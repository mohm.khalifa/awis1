<h3>Tastenkürzel</h3>
Die Anwendung AWIS basiert auf einem Browser. Je nach eingesetztem Browser ergeben sich einige besondere
Funktionalitäten. Auf dieser Seite werden die Besonderheiten Ihres Browsers erläutert.<br><br>

<?php


echo '<table border=0 width=100%>';
echo '<tr><td valign=top id=FeldBez>Browserversion:</td>';
if(strpos($HTTP_USER_AGENT,'MSIE'))
{
	$Version = intval(substr($HTTP_USER_AGENT, strpos($HTTP_USER_AGENT,'MSIE')+5,4));
	echo '<td>Microsoft Internetexplorer, Version ' . $Version . '.';
}
else
{
 	echo '<td>'. $HTTP_USER_AGENT . '</td></tr>';
}



echo '<tr><td valign=top id=FeldBez>Sprachkürzel:</td>';
echo '<td>'. $HTTP_ACCEPT_LANGUAGE . '</td></tr>';

echo  '</table>';
echo '<hr>';

if(strpos($HTTP_USER_AGENT, 'MSIE'))
{
	echo 'Folgende Funktionen werden in Ihrem Microsoft Internet Explorer unterstützt:<br><br>
	
	<b>Unterstrichene Buchstaben</b><br><br>
	In Ihrem Browser werden Tastenkürzel für Eingabemasken und Registerblätter unterstützt. Die Tastenkürzel
	werden durch einen unterstrichenen Buchstaben gekennzeichnet, z.B. Art<u>i</u>kel. Hier ist das I das Tastenkürzel.
	Um die Kürzel zu nutzen, muss die ALT-Taste (links neben der Leertaste) festgehalten und dann der unterstrichene
	Buchstabe gedrückt werden. In dem Beispiel also ALT und I.<br><br>
	
	';

}
















?>