<html>
<head>
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - Filialinformationen</title>
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<?
require_once("register.inc.php");
require_once("db.inc.php"); // DB-Befehle
require_once("sicherheit.inc.php");
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) . ">";
?>
</head>

<body>
<?
global $awisRSZeilen;
global $AWISBenutzer;

$con = awislogon();
if(awisBenutzerRecht($con,100)==0)
{
     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
     die("Keine ausreichenden Rechte!");
}

if($con == FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

if(!isset($_GET['FilID']))
{
	awisLogoff($con);
	die("<span class=HinweisText>Diese Seite kann nicht direkt aufgerufen werden</span>");
}

switch($_GET['Info'])
{
	case 'Adresse':
		$SQL = "SELECT * FROM Filialen ";
		$SQL .= " WHERE FIL_ID=" . $_GET['FilID'];
		
		$rsFIL = awisOpenRecordset($con, $SQL);
		if($awisRSZeilen>0)
		{
			echo "<br>" . $rsFIL['FIL_BEZ'][0];
			echo '<br>' . $rsFIL['FIL_STRASSE'][0];
			echo '<br><br>' . $rsFIL['FIL_PLZ'][0] . ' ' . $rsFIL['FIL_ORT'][0];
		}
		break;
	case 'AdresseMitGF':
		$SQL = "SELECT * FROM Filialen ";
		$SQL .= " INNER JOIN FilialInfos ON FIL_ID = FIF_FIL_ID";
		$SQL .= " INNER JOIN Personal ON FIF_WERT = PER_Nr";
		$SQL .= " WHERE FIL_ID=" . $_GET['FilID'];
		$SQL .= " AND FIF_FIT_ID=74";
		
		$rsFIL = awisOpenRecordset($con, $SQL);
		if($awisRSZeilen>0)
		{
			echo "<br>" . $rsFIL['FIL_BEZ'][0];
			echo "<br>" . ($rsFIL['PER_ANR_ID'][0]==1?'Herr ':'Frau ');
			echo $rsFIL['PER_VORNAME'][0]." " . $rsFIL['PER_NACHNAME'][0];
			echo '<br>' . $rsFIL['FIL_STRASSE'][0];
			echo '<br><br>' . $rsFIL['FIL_PLZ'][0] . ' ' . $rsFIL['FIL_ORT'][0];
		}
		break;
}



awisLogoff($con);
?>
</body>
</html>