<?php
global $AWISCursorPosition;		// Zum Cursor setzen
//global $AWIS_KEY1;
//global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KundenKfzZuordnungen','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Werkzeuge = new awisWerkzeuge();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht13000 = $AWISBenutzer->HatDasRecht(13000);
	if($Recht13000==0)
	{
	    awisEreignis(3,1000,'KundenKfzZuordnungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KundenKfzZuordnungen'));

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{	
		$Param = array();
		$Param['KKZ_INFOFELD_1'] = $_POST['sucKKZ_INFOFELD_1'];
		$Param['KKZ_VERTRAGSNUMMER'] = $_POST['sucKKZ_VERTRAGSNUMMER'];
		$Param['KKZ_REFERENZFELD'] = $_POST['sucKKZ_REFERENZFELD'];
		$Param['KKZ_LIEFERANT'] = $_POST['sucKKZ_LIEFERANT'];
		$Param['KKZ_STATUS'] = $_POST['sucKKZ_STATUS'];
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_KundenKfzZuordnungen",serialize($Param));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./kundenkfzzuordnungen_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KundenKfzZuordnungen'));
	}
	//elseif(isset($_GET['KKZ_REFERENZFELD']))
	//{
	//	$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['KKZ_REFERENZFELD']);
	//	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KundenKfzZuordnungen'));
	//}
	elseif(isset($_GET['KKZ_REFERENZFELD']))
	{
		$Param['KKZ_INFOFELD_1'] = '';
		$Param['KKZ_VERTRAGSNUMMER'] = '';
		$Param['KKZ_REFERENZFELD'] = $_GET['KKZ_REFERENZFELD'];
		$Param['KKZ_LIEFERANT'] = '';
		$Param['KKZ_STATUS'] = '';
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_KundenKfzZuordnungen",serialize($Param));		
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_KundenKfzZuordnungen',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		//$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY INFOFELD';
			$Param['ORDER']='INFOFELD';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
	{
		$RemoteDB = '@COM_DE.ATU.DE';
	}
	else 
	{
		$RemoteDB = '@COM_TEST_DE.ATU.DE';		
	}
	
	$SQL= 'SELECT KKZ_VERTRAGSNUMMER, KKZ_REFERENZFELD, KKZ_STATUS, KKZ_LIEFERANT, INFOFELD, KKZ_KEY, KKC_VERTRAGSNUMMER ';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM (';		
	$SQL.= 'SELECT KKZ_VERTRAGSNUMMER, KKZ_REFERENZFELD, KKZ_STATUS, KKZ_LIEFERANT, ';	
	$SQL .= ' kkz_infofeld_1,';
	$SQL .= ' kkz_infofeld_1 ||';
	$SQL .= " decode(kkz_infofeld_2, null, '', ' | ' || kkz_infofeld_2) ||";
	$SQL .= " decode(kkz_infofeld_3, null, '', ' | ' || kkz_infofeld_3) ||";
	$SQL .= " decode(kkz_infofeld_4, null, '', ' | ' || kkz_infofeld_4) ||";
	$SQL .= " decode(kkz_infofeld_5, null, '', ' | ' || kkz_infofeld_5) ||";
	$SQL .= " decode(kkz_infofeld_6, null, '', ' | ' || kkz_infofeld_6) ||";
	$SQL .= " decode(kkz_infofeld_7, null, '', ' | ' || kkz_infofeld_7) ||";
	$SQL .= " decode(kkz_infofeld_8, null, '', ' | ' || kkz_infofeld_8) ||";
	$SQL .= " decode(kkz_infofeld_9, null, '', ' | ' || kkz_infofeld_9) as INFOFELD, KKZ_KEY, ";
	$SQL .= " (SELECT LISTAGG(KKC_VERTRAGSNUMMER, ',') WITHIN GROUP (ORDER BY KKC_VERTRAGSNUMMER)"; 
    $SQL .= " FROM EXPERIAN_ATU.EXP_KUNDEN_KFZ_ZUORDNUNG_CLONE".$RemoteDB;
    $SQL .= " WHERE KKC_KKZ_KEY=KKZ_KEY) AS KKC_VERTRAGSNUMMER";  
	$SQL .= " FROM EXPERIAN_ATU.EXP_KUNDEN_KFZ_ZUORDNUNG".$RemoteDB.") k";		
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

//	if($AWIS_KEY1<=0)
//	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUR',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
//	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST);
$Form->DebugAusgabe(1,$SQL);
	
	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsKKZ = $DB->RecordsetOeffnen($SQL);
	
	$AWISBenutzer->ParameterSchreiben('Formular_KundenKfzZuordnungen',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmKundenKfzZuordnungen action=./kundenkfzzuordnungen_Main.php?cmdAktion=Details method=POST>';

	if($rsKKZ->EOF())		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsKKZ->AnzahlDatensaetze()>=1)						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();


		//$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Form->ZeileStart('font-size:11pt');

		$Link = './kundenkfzzuordnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KKZ_LIEFERANT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KKZ_LIEFERANT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_LIEFERANT'],120,'',$Link);				
		$Link = './kundenkfzzuordnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');		
		$Link .= '&Sort=KKZ_VERTRAGSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KKZ_VERTRAGSNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_VERTRAGSNUMMER'],130,'',$Link);
		$Link = './kundenkfzzuordnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KKZ_REFERENZFELD'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KKZ_REFERENZFELD'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_REFERENZFELD'],100,'',$Link);
		$Link = './kundenkfzzuordnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KKC_VERTRAGSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KKC_VERTRAGSNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzZuordnungen']['KKC_VERTRAGSNUMMER'],200,'',$Link);
		$Link = './kundenkfzzuordnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=INFOFELD'.((isset($_GET['Sort']) AND ($_GET['Sort']=='INFOFELD'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_INFOFELD_1'],600,'',$Link);				
						
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsKKZ->EOF())
		{
			//$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
			$Form->ZeileStart('font-size:11pt');		

			$Form->Erstelle_ListenFeld('KKZ_LIEFERANT',$rsKKZ->FeldInhalt('KKZ_LIEFERANT'),12,120,false,($DS%2),'','','T');			
			$Form->Erstelle_ListenFeld('KKZ_VERTRAGSNUMMER_'.$rsKKZ->FeldInhalt('KKZ_KEY'),$rsKKZ->FeldInhalt('KKZ_VERTRAGSNUMMER'),12,130,($Recht13000&2),($DS%2),'','','Z','');
			$Form->Erstelle_ListenFeld('KKZ_REFERENZFELD',$rsKKZ->FeldInhalt('KKZ_REFERENZFELD'),12,100,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('KKC_VERTRAGSNUMMER_'.$rsKKZ->FeldInhalt('KKZ_KEY'),$rsKKZ->FeldInhalt('KKC_VERTRAGSNUMMER'),30,200,($Recht13000&2),($DS%2),'','','T','','Vertragsnummer - Klone als Komma getrennte Liste');
			$Form->Erstelle_ListenFeld('KKZ_INFOFELD',$rsKKZ->FeldInhalt('INFOFELD'),12,600,false,($DS%2),'','','T','L');
					
			$Form->ZeileEnde();

			$rsKKZ->DSWeiter();
			$DS++;
		}

		$Link = './kundenkfzzuordnungen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht13000&2)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201010181123");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201010181124");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	//global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';
	
	if(isset($Param['KKZ_INFOFELD_1']) AND $Param['KKZ_INFOFELD_1']!='')
	{
		//$Bedingung .= ' AND UPPER(KKZ_INFOFELD_1)' . $DB->LIKEoderIst($Param['KKZ_INFOFELD_1'],  awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ' AND UPPER(INFOFELD)' . $DB->LIKEoderIst($Param['KKZ_INFOFELD_1'],  awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	
	if(isset($Param['KKZ_VERTRAGSNUMMER']) AND $Param['KKZ_VERTRAGSNUMMER']!='')
	{
		$Bedingung .= ' AND KKZ_VERTRAGSNUMMER = ' . $DB->FeldInhaltFormat('Z',$Param['KKZ_VERTRAGSNUMMER']) . ' ';
	}	
	
	if(isset($Param['KKZ_REFERENZFELD']) AND $Param['KKZ_REFERENZFELD']!='')
	{
		//$Bedingung .= ' AND KKZ_REFERENZFELD = ' . $DB->FeldInhaltFormat('T',$Param['KKZ_REFERENZFELD']) . ' ';
		$Bedingung .= ' AND KKZ_REFERENZFELD ' . $DB->LikeOderIst($Param['KKZ_REFERENZFELD']). ' ';
	}
	
	if(isset($Param['KKZ_LIEFERANT']) AND $Param['KKZ_LIEFERANT']!='')
	{
		$Bedingung .= ' AND KKZ_LIEFERANT = ' . $DB->FeldInhaltFormat('T',$Param['KKZ_LIEFERANT']) . ' ';
	}

	if(isset($Param['KKZ_STATUS']) AND $Param['KKZ_STATUS']!='')
	{
		$Bedingung .= ' AND KKZ_STATUS = ' . $DB->FeldInhaltFormat('Z',$Param['KKZ_STATUS']) . ' ';
	}
	
	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>