<?php
try {
    global $Funktionen;
    if(!isset($_GET["cmdAktion"]) or $_GET["cmdAktion"] != "Pflege") {

        $SQL = 'SELECT count(*) AS ANZAHL FROM AZUBIPROJEKTETEILNEHMER WHERE AZT_PERSNR = ' . $Funktionen->DB->WertSetzen('AZT', 'Z', $_POST['txtAZT_PERSNR']);
        $SQL .= " AND AZT_AZB_STUFE = " . $Funktionen->DB->WertSetzen('AZT', 'Z', $_POST['txtAZB_STUFE']);
        $rsCheck = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('AZT', true));

        $SQL = 'SELECT * FROM v_personal_komplett WHERE PERSNR = ' . $Funktionen->DB->WertSetzen('AZT', 'T', $_POST['txtAZT_PERSNR']);
        $rsPersNrCheck = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('AZT'));

        $SQL = " SELECT FIL_ID FROM v_filialen_aktuell WHERE FIL_ID = " . $Funktionen->DB->WertSetzen('AZT', 'T', $_POST['txtAZT_FILNR']);
        $rsFILCheck = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('AZT', true));

        $Funktionen->Form->ZeileStart();
        if ($rsPersNrCheck->AnzahlDatensaetze() <= 0) {
            $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_KEINEPERSNR'], awisFormular::HINWEISTEXT_FEHLER);
        } elseif ($rsCheck->FeldInhalt('ANZAHL') > 0) {
            $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_BEREITSTEILGENOMMEN'], awisFormular::HINWEISTEXT_FEHLER);
        } elseif ((isset($_POST['txtAZT_EMAIL'])) and ($_POST['txtAZT_EMAIL'] <> "") and (!$Funktionen->awisMailer->checkEmail($_POST['txtAZT_EMAIL'], awisMailer::PRUEFE_LOGIK))) {
            $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_EMAIL_FALSCH'], awisFormular::HINWEISTEXT_FEHLER);
        } elseif ($rsFILCheck->AnzahlDatensaetze() <= 0) {
            $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_FIL_FALSCH'], awisFormular::HINWEISTEXT_FEHLER);
        } else {
            $Funktionen->DB->TransaktionBegin();

            $SQL = 'INSERT INTO AZUBIPROJEKTETEILNEHMER (AZT_AZB_STUFE, AZT_NAME, AZT_VORNAME, AZT_PERSNR, AZT_FILNR, AZT_TSHIRT_GROESSE, AZT_TEL_NR, AZT_EMAIL, AZT_USER, AZT_USERDAT) VALUES (';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'Z', $_POST['txtAZB_STUFE']) . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'T', $_POST['txtAZT_NAME']) . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'T', $_POST['txtAZT_VORNAME']) . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'Z', $_POST['txtAZT_PERSNR']) . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'Z', $_POST['txtAZT_FILNR']) . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'T', $_POST['txtAZT_TSHIRT_GROESSE']) . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'T', $_POST['txtAZT_TEL_NR']) . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'T', isset($_POST['txtAZT_EMAIL']) ? ($_POST['txtAZT_EMAIL']) : '') . ', ';
            $SQL .= $Funktionen->DB->WertSetzen('AZT', 'T', $Funktionen->AWISBenutzer->BenutzerName()) . ', ';
            $SQL .= 'sysdate)';

            $Funktionen->DB->Ausfuehren($SQL, 'AZT', true, $Funktionen->DB->Bindevariablen('AZT'));

            $SQL = " SELECT seq_azt_key.currval FROM dual";
            $rsAZTKEY = $Funktionen->DB->RecordSetOeffnen($SQL);
            $Funktionen->AWIS_KEY1 = $rsAZTKEY->FeldInhalt("CURRVAL");

            $SQL = "SELECT t.*, b.AZB_BEREICHNAME, lpad(AZT_FILNR,4,0) || FUNC_EMAILENDUNG_FILIALE(AZT_FILNR) AS FIL_MAIL_KOMPLETT FROM AZUBIPROJEKTETEILNEHMER t ";
            $SQL .= " LEFT JOIN AZUBIPROJEKTEBEREICHE b ";
            $SQL .= " ON t.AZT_AZB_STUFE = b.AZB_STUFE ";
            $SQL .= $Funktionen->BedingungErstellen();
            $rsAZT = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen("AZT", true));

            $Funktionen->MailVersenden("AZT_ANMELDEBESTAETIGUNG", $rsAZT->FeldInhalt('FIL_MAIL_KOMPLETT') . (($rsAZT->FeldInhalt('AZT_EMAIL') <> "") ? (";" . $rsAZT->FeldInhalt('AZT_EMAIL')) : ("")), "AZT_STANDART_ABSENDER", awisMailer::FORMAT_HTML);

            $Funktionen->Form->Hinweistext(str_replace('#AZB_BEREICHNAME#', $rsAZT->FeldInhalt('AZB_BEREICHNAME'), $Funktionen->AWISSprachKonserven['AZT']['AZT_ERFOLGREICHTEILGENOMMEN']), awisFormular::HINWEISTEXT_OK);
            $Funktionen->DB->TransaktionCommit();
        }
        $Funktionen->Form->ZeileEnde();
    } elseif(isset($_GET["cmdAktion"]) and $_GET["cmdAktion"] == "Pflege") {
        if(isset($_POST["txtAZB_KEY"]) and $_POST["txtAZB_KEY"] != -1){

            $Aenderung = "";
            if($_POST["oldAZB_BEREICHNAME"] != $_POST["txtAZB_BEREICHNAME"]){
                $Aenderung .= ", AZB_BEREICHNAME = ".$Funktionen->DB->WertSetzen("AZB",'T',$_POST["txtAZB_BEREICHNAME"]);
            }

            if($_POST["oldAZB_STATUS"] != $_POST["txtAZB_STATUS"]){
                $Aenderung .= ", AZB_STATUS = ".$Funktionen->DB->WertSetzen("AZB",'T',$_POST["txtAZB_STATUS"]);
            }

            if($_POST["oldAZB_INFOTEXT"] != $_POST["txtAZB_INFOTEXT"]){


                $Aenderung .= ", AZB_INFOTEXT = ".$Funktionen->DB->WertSetzen("AZB",'C',$_POST["txtAZB_INFOTEXT"]);

            }


            if($Aenderung != ""){
                $SQL = "UPDATE AZUBIPROJEKTEBEREICHE set ";
                $SQL .= substr($Aenderung,2);
                $SQL .= " where AZB_KEY =".$Funktionen->DB->WertSetzen("AZB","Z",$_POST["txtAZB_KEY"]);

                $Funktionen->DB->Ausfuehren($SQL,'',true, $Funktionen->DB->Bindevariablen("AZB"));
            }

            $Funktionen->AWIS_KEY1 = $_POST["txtAZB_KEY"];

            $Funktionen->Form->ZeileStart();
            $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZB']['AZB_SPEICHERFOLG'], awisFormular::HINWEISTEXT_OK);
            $Funktionen->Form->ZeileEnde();

        } if(isset($_POST["txtAZB_KEY"]) and $_POST["txtAZB_KEY"] == -1) {

            $SQL = "Select max(AZB_STUFE) as GROESSTE_STUFE from AZUBIPROJEKTEBEREICHE";
            $rsSTUFE = $Funktionen->DB->RecordSetOeffnen($SQL);

            $SQL = "";
            $SQL .= "INSERT INTO AZUBIPROJEKTEBEREICHE ";
            $SQL .= " (";
            $SQL .= "   AZB_STUFE";
            $SQL .= ",  AZB_BEREICHNAME";
            $SQL .= ",  AZB_INFOTEXT";
            $SQL .= ",  AZB_STATUS";
            $SQL .= ",  AZB_USER";
            $SQL .= ",  AZB_USERDAT";
            $SQL .= " ) ";
            $SQL .= " VALUES ";
            $SQL .= " (";
            $SQL .= "   ".$Funktionen->DB->WertSetzen("AZB","Z",($rsSTUFE->AnzahlDatensaetze()==1)?($rsSTUFE->FeldInhalt('GROESSTE_STUFE')+1):(1));
            $SQL .= ",  ".$Funktionen->DB->WertSetzen("AZB","T",$_POST["txtAZB_BEREICHNAME"]);
            $SQL .= ",  ".$Funktionen->DB->WertSetzen("AZB","C",$_POST["txtAZB_INFOTEXT"]);
            $SQL .= ",  ".$Funktionen->DB->WertSetzen("AZB","T",$_POST["txtAZB_STATUS"]);
            $SQL .= ",  ".$Funktionen->DB->WertSetzen("AZB", "T", $Funktionen->AWISBenutzer->BenutzerName());
            $SQL .= ",  SYSDATE";
            $SQL .= ")";

            $Funktionen->DB->Ausfuehren($SQL,'',true,$Funktionen->DB->Bindevariablen("AZB"));


            $SQL = "Select max(AZB_KEY) as GROESSTER_KEY from AZUBIPROJEKTEBEREICHE";
            $rsKEY = $Funktionen->DB->RecordSetOeffnen($SQL);

            $Funktionen->AWIS_KEY1 = $rsKEY->FeldInhalt("GROESSTER_KEY");

            $Funktionen->Form->ZeileStart();
            $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZB']['AZB_DSNEUERFOLG'], awisFormular::HINWEISTEXT_OK);
            $Funktionen->Form->ZeileEnde();
        }

    }
} catch (awisException $ex) {
    if($Funktionen->DB->TransaktionAktiv()) {
        $Funktionen->DB->TransaktionRollback();
    }
    $Funktionen->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Funktionen->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    if($Funktionen->DB->TransaktionAktiv()) {
        $Funktionen->DB->TransaktionRollback();
    }
    $Funktionen->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}

?>