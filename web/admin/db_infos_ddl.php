<html>
<head>
	<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $tab_breite;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 10);

if(($RechteStufe&4)!=4)
{
	awisEreignis(3,1000,'Journal',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
	die("Keine ausreichenden Rechte!");
}

$awis_link = "";
$db = awisDBServer();
/*
if (!empty($_GET["txtDB"]))
{
	$awis_link = "@" . strtolower($_GET["txtDB"]) . ".atu.de";
	$db = strtolower($_GET["txtDB"]);
}
else
{
	$awis_link = "@" . strtolower(awisDBServer()) . ".atu.de";
	$db = strtolower(awisDBServer());
}
*/
$rsResult = awisOpenRecordset($con,'SELECT * from mcp.datenbanken');
$rsResultZeilen = $awisRSZeilen;

echo "<br><h1 id=SeitenTitel>DDL-History";
if (!empty($_GET["txtDB"]))
{
	echo " von Datenbank: " . $_GET["txtDB"];
}
echo  "</h1><br>";

echo '<form name=frmSuche method=get action=' . $_SERVER["PHP_SELF"] . '>';
echo '<input type="hidden" name="cmdAktion" value="DDL_History">';

echo '<table border=1>';
echo '<tr><td id=FeldBez colspan=4>';
echo '<h3>Datenbanken</h3></td></tr>';
echo '<tr>';
echo '<td id=FeldBez><u>D</u>atenbank</td>';
echo '<td colspan=3><input name=txtDB readonly accesskey=d value="'.awisDBServer().'"></td>';
// Keine Auswahl mehr, SK 14.02.2008
/*

echo '<td colspan=3><select accesskey=d name="txtDB">';


for($DBNr=0;$DBNr<$rsResultZeilen;$DBNr++)
{
	echo '<option value="' . $rsResult['DBK_NAME'][$DBNr] . '"';
	if ($db == strtolower($rsResult['DBK_NAME'][$DBNr]))
	{
		echo ' selected="selected"';
	}
	echo '>' . $rsResult['DBK_NAME'][$DBNr] . '</option>';
}

echo '</select></td>';
*/
echo '</tr>';
echo '</table>';
echo "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";

echo "<br>";
echo "<table border=0><tr>";
echo "<td>Datum von:</td><td><input type='text' name=txt_DatVon size=12></td>";
echo "&nbsp;<td>Datum bis:</td><td><input type='text' name=txt_DatBis size=12></td>";
echo "</tr></table>";

echo "<hr>";

if(!isset($_GET['txt_DatVon']) || $_GET['txt_DatVon']=='' )
{
	$start='trunc(sysdate)';
}else 
{
	$start="to_date('".awis_PruefeDatum($_GET['txt_DatVon'].' 00:00:00',1,0)."','DD.MM.RRRR HH24:MI:SS')";
}
if (!isset($_GET['txt_DatBis']) || $_GET['txt_DatBis']=='')
{
	$ende='sysdate';
}else 
{
	$ende="to_date('".awis_PruefeDatum($_GET['txt_DatBis'].' 23:59:59',1,0)."','DD.MM.RRRR HH24:MI:SS')";
}

$SQL="SELECT rowidtochar(ROWID) as RID, USER_NAME, DDL_DATE, ";
$SQL.="DDL_TYPE, OBJECT_TYPE, OWNER, OBJECT_NAME, INSTANCE, SQL_STMT ";
$SQL.="FROM DDL_LOG".$awis_link." WHERE DDL_DATE>=".$start." AND DDL_DATE<=".$ende;
$SQL.=" ORDER BY DDL_DATE DESC";

//awis_debug(1,$SQL,$_REQUEST);

awisExecute($con,"ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI.SS'");

$rsDDL=awisOpenRecordset($con,$SQL);
$rsDDLZeilen = $awisRSZeilen;

awisExecute($con,"ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR'");

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift('',30);
awis_FORM_Erstelle_Liste_Ueberschrift('User',100);
awis_FORM_Erstelle_Liste_Ueberschrift('Date',170);
awis_FORM_Erstelle_Liste_Ueberschrift('Typ',100);
awis_FORM_Erstelle_Liste_Ueberschrift('Object',150);
//awis_FORM_Erstelle_Liste_Ueberschrift('Owner',100);
awis_FORM_Erstelle_Liste_Ueberschrift('Name',350);
awis_FORM_Erstelle_Liste_Ueberschrift('Instance',250);
awis_FORM_ZeileEnde();

for($i=0;$i<$rsDDLZeilen;$i++)
{
	$Icons = array();
	$Icons[] = array('edit','./db_infos_Main.php?cmdAktion=DDL_History&txtDB='.$db.'&Show_DDL='.$rsDDL['RID'][$i]);
	
	
	if (isset($_GET['Show_DDL']) && $rsDDL['RID'][$i] == $_GET['Show_DDL'])
	{
		awis_FORM_Trennzeile();
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel('User: ',40,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('User',$rsDDL['USER_NAME'][$i],0,150,false,'font-size: 75%;','','','T','L','DDL-User');
		awis_FORM_Erstelle_TextLabel('Datum: ',50,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('Date',$rsDDL['DDL_DATE'][$i],0,150,false,'font-size: 75%;','','','T','L','DDL-Datum/Zeit');
		awis_FORM_Erstelle_TextLabel('Instance: ',60,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('Instance',$rsDDL['INSTANCE'][$i],0,150,false,'font-size: 75%;','','','T','L','Instance');
		awis_FORM_ZeileEnde();
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel('DDL-Typ: ',60,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('Typ',$rsDDL['DDL_TYPE'][$i],0,150,false,'font-size: 75%;','','','T','L','DDL-Typ');
		awis_FORM_Erstelle_TextLabel('Object-Typ: ',75,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('Object',$rsDDL['OBJECT_TYPE'][$i],0,150,false,'font-size: 75%;','','','T','L','DDL-Object');
		awis_FORM_Erstelle_TextLabel('Object-Owner: ',95,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('Owner',$rsDDL['OWNER'][$i],0,150,false,'font-size: 75%;','','','T','L','Object-Owner');
		awis_FORM_Erstelle_TextLabel('Object-Name: ',90,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('Name',$rsDDL['OBJECT_NAME'][$i],0,150,false,'font-size: 75%;','','','T','L','Object-Name');
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel('SQL-Statment: ',100,'font-size: 75%;');
		awis_FORM_Erstelle_TextFeld('SQL',$rsDDL['SQL_STMT'][$i],0,600,false,'font-size: 75%;','','','T','L','DDL-SQL');
		awis_FORM_ZeileEnde();
		
		awis_FORM_Trennzeile();
		
	}
	else 
	{
	
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListeIcons($Icons,30,($i%2));
		awis_FORM_Erstelle_ListenFeld('User',$rsDDL['USER_NAME'][$i],'',100,false,($i%2),'','','T','L','DDL-User');
		awis_FORM_Erstelle_ListenFeld('Date',$rsDDL['DDL_DATE'][$i],'',170,false,($i%2),'','','T','L','DDL-Datum/Zeit');
		awis_FORM_Erstelle_ListenFeld('Typ',$rsDDL['DDL_TYPE'][$i],'',100,false,($i%2),'','','T','L','DDL-Typ');
		awis_FORM_Erstelle_ListenFeld('Object',$rsDDL['OBJECT_TYPE'][$i],'',150,false,($i%2),'','','T','L','DDL-Object');
		//awis_FORM_Erstelle_ListenFeld('Owner',$rsDDL['OWNER'][$i],'',100,false,($i%2),'','','T','L','DDL-Owner');
		awis_FORM_Erstelle_ListenFeld('Name',$rsDDL['OBJECT_NAME'][$i],'',350,false,($i%2),'','','T','L','DDL-Name');
		awis_FORM_Erstelle_ListenFeld('Instance',$rsDDL['INSTANCE'][$i],'',250,false,($i%2),'','','T','L','DDL-Instance');
		awis_FORM_ZeileEnde();
	}
}

awis_FORM_FormularEnde();

echo "</form>";
awislogoff($con);
?>
</body>
</html>