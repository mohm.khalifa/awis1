<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUB','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','ZusammenfassungBestellungen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);		// Artikelstamm
	$Recht10002 = $AWISBenutzer->HatDasRecht(10002);		// Bestellungen
	if(($Recht10001&32)==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZUB_BESTELLDATUM DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT Zukaufbestellungen.*, ZWA_WANR';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukaufbestellungen ';
	$SQL .= ' LEFT OUTER JOIN ZukaufWerkstattAuftraege ON ZUB_KEY = ZWA_ZUB_KEY';
	$SQL .= ' WHERE ZUB_ZLA_KEY=0'.$AWIS_KEY1;

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUKKAEUFE',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsZUB = $DB->RecordsetOeffnen($SQL);
	$Form->Formular_Start();

	if($rsZUB->AnzahlDatensaetze()>0)
	{
		$Form->ZeileStart();
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZUB_BESTELLDATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_BESTELLDATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'],120,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZUB_FIL_ID'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_FIL_ID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_FIL_ID'],80,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZUB_BESTELLMENGE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_BESTELLMENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZUB_EINHEITENPROPREIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_EINHEITENPROPREIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_EINHEITENPROPREIS'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZUB_PREISBRUTTO'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_PREISBRUTTO'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'],200,'',$Link);
		if(($Recht10002&32)==32)
		{
			$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&SSort=ZUB_PREISANGELIEFERT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUB_PREISANGELIEFERT'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISANGELIEFERT'],200,'',$Link);
		}
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZUB->EOF())
		{
			$Form->ZeileStart();
			$Link='/zukauf/zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$rsZUB->FeldInhalt('ZUB_KEY');
			$Form->Erstelle_ListenFeld('ZUB_BESTELLDATUM',$rsZUB->FeldInhalt('ZUB_BESTELLDATUM'),0,120,false,($DS%2),'',$Link,'D');
			$Link='/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsZUB->FeldInhalt('ZUB_FIL_ID');	// TODO: Sp�ter auf die Hersteller
			$Form->Erstelle_ListenFeld('ZUB_FIL_ID',$rsZUB->FeldInhalt('ZUB_FIL_ID'),0,80,false,($DS%2),'',$Link,'T','L');
			$Link = '';
			$Style='';
			if(stripos($rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG'),'ALTTEILWERT')!==false)
			{
				$Style='color:#F00FFF;font-weight:bold;';
			}
			$Form->Erstelle_ListenFeld('ZUB_BESTELLMENGE',$rsZUB->FeldInhalt('ZUB_BESTELLMENGE'),0,150,false,($DS%2),$Style,$Link,'Z','L',$AWISSprachKonserven['Wort']['Altteilwert']);
			$Form->Erstelle_ListenFeld('ZUB_EINHEITENPROPREIS',$rsZUB->FeldInhalt('ZUB_EINHEITENPROPREIS'),0,150,false,($DS%2),'',$Link,'Z','L');
			$Form->Erstelle_ListenFeld('ZUB_PREISBRUTTO',$rsZUB->FeldInhalt('ZUB_PREISBRUTTO'),0,200,false,($DS%2),'',$Link,'N2','L');
			if(($Recht10002&32)==32)
			{
				$Form->Erstelle_ListenFeld('ZUB_PREISANGELIEFERT',$rsZUB->FeldInhalt('ZUB_PREISANGELIEFERT'),0,200,false,($DS%2),'',$Link,'N2','L');
			}


			if(stripos($rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG'),'ALTTEILWERT')===false)
			{
				$Menge += $rsZUB->FeldInhalt('ZUB_BESTELLMENGE');
				$Vorgaenge++;
			}

			$Form->ZeileEnde();

			$rsZUB->DSWeiter();
			$DS++;
		}

		// Summen ermitteln
		$SQL = 'SELECT SUM(ZUB_BESTELLMENGE) AS MENGE, COUNT(*) AS VORGAENGE';
		$SQL .= ' FROM Zukaufbestellungen ';
		$SQL .= ' WHERE ZUB_ZLA_KEY=0'.$AWIS_KEY1;
		$SQL .= " AND ZUB_ARTIKELBEZEICHNUNG <> 'ALTTEILWERT'";
		$rsSumme = $DB->RecordSetOeffnen($SQL);

		$Form->ZeileStart();
		$Text = $AWISSprachKonserven['Wort']['ZusammenfassungBestellungen'];
		$Text = str_replace('#MENGE#',$rsSumme->FeldInhalt('MENGE'),$Text);
		$Text = str_replace('#VORGAENGE#',$rsSumme->FeldInhalt('VORGAENGE'),$Text);
		$Form->Erstelle_ListenFeld('FILLER1',$Text,0,920,false,($DS%2),'','','');
		$Form->ZeileEnde();

		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Kaeufe';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}


	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181311");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181309");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>