<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class versvermittlungen_funktionen
{
    public $AWIS_KEY1;
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht9800;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeuge;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular(3);
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht9800 = $this->AWISBenutzer->HatDasRecht(9800);
        $this->_EditRecht = (($this->Recht9800 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_VBA'));
        $this->AWISWerkzeuge = new awisWerkzeuge();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('VBA', '%');
        $TextKonserven[] = array('VVE', '%');
        $TextKonserven[] = array('VVS', '%');
        $TextKonserven[] = array('AST', 'AST_ATUNR');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_export');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Dateiname');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'Ja');
        $TextKonserven[] = array('Wort', 'Nein');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineRechte');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_versvermittlungen');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_VBA', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht9800 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'VBA'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function BedingungErstellen()
    {
        $Bedingung = '';

        if ($this->AWIS_KEY1 != 0) {
            $Bedingung .= ' AND VBA_KEY = ' . $this->DB->WertSetzen('VBA', 'N0', $this->AWIS_KEY1);
            return $Bedingung;
        } else {
            $Bedingung .= 'AND ((VBA_KEEPALIVE is null or VBA_KEEPALIVE <= sysdate - 19*(1/24/60/60))';
            $Bedingung .= 'OR VBA_KEEPALIVE_USER ' . $this->DB->LikeOderIst($this->AWISBenutzer->BenutzerName(), 0, 'VBA').')';

        }

        if (isset($this->Param['VVE_KUERZEL']) and $this->Param['VVE_KUERZEL'] != '') {
            $Bedingung .= ' AND VVE.VVE_KUERZEL ' . $this->DB->LikeOderIst($this->Param['VVE_KUERZEL'],awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_KFZKZ']) and $this->Param['VBA_KFZKZ'] != '') {
            $Bedingung .= ' AND LARTNRKOMP(VBA_KFZKZ) ' . $this->DB->LikeOderIst(awisWerkzeuge::ArtNrKomprimiert($this->Param['VBA_KFZKZ'],array(ord('*'),ord('%'))), awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_NAME1']) and $this->Param['VBA_NAME1'] != '') {
            $Bedingung .= ' AND VBA_NAME1 ' . $this->DB->LikeOderIst($this->Param['VBA_NAME1'], awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_NAME3']) and $this->Param['VBA_NAME3'] != '') {
            $Bedingung .= ' AND VBA_NAME3 ' . $this->DB->LikeOderIst($this->Param['VBA_NAME3'], awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_VSNR']) and $this->Param['VBA_VSNR'] != '') {
            $Bedingung .= ' AND VBA_VSNR ' . $this->DB->LikeOderIst($this->Param['VBA_VSNR'], awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_SNR']) and $this->Param['VBA_SNR'] != '') {
            $Bedingung .= ' AND VBA_SNR ' . $this->DB->LikeOderIst($this->Param['VBA_SNR'], awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_VNPLZ']) and $this->Param['VBA_VNPLZ'] != '') {
            $Bedingung .= ' AND VBA_VNPLZ ' . $this->DB->LikeOderIst($this->Param['VBA_VNPLZ'], awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_VNORT']) and $this->Param['VBA_VNORT'] != '') {
            $Bedingung .= ' AND VBA_VNORT <= ' . $this->DB->LikeOderIst($this->Param['VBA_VNPLZ'], awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_ORDNUNGSNR']) and $this->Param['VBA_ORDNUNGSNR'] != '') {
            $Bedingung .= ' AND VBA_ORDNUNGSNR ' . $this->DB->LikeOderIst($this->Param['VBA_ORDNUNGSNR'], awisDatenbank::AWIS_LIKE_UPPER, 'VBA');
        }

        if (isset($this->Param['VBA_AXZ']) and $this->Param['VBA_AXZ'] == 0) {
            $Bedingung .= ' AND VBA_AX_VORGANGNR is null';
        } elseif (isset($this->Param['VBA_AXZ']) and $this->Param['VBA_AXZ'] == 1) {
            $Bedingung .= ' AND VBA_AX_VORGANGNR is not null';
        } elseif (isset($this->Param['VBA_AXZ']) and $this->Param['VBA_AXZ'] == 2) {
            $Bedingung .= '';
        }

        return $Bedingung;
    }

    public function DatumKorrekt ($datum) {
        $DatetimeAktDatum = strtotime(strftime('%Y-%m-%d'));
        $DatetimeDatum = strtotime($datum);
        return $DatetimeDatum <= $DatetimeAktDatum;
    }


}