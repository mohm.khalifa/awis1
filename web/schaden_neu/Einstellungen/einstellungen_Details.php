<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Fehler;


try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	
		
 	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht40001 = $AWISBenutzer->HatDasRecht(40001);
	
	
	
	
	if($Recht40001==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./einstellungen_speichern.php');
	}
	
	$SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Schad_Schnellsuche');
	$Frames = unserialize($AWISBenutzer->ParameterLesen('Schad_Frame'));
	
	
	$Form->SchreibeHTMLCode('<form name="frmEinstellung" action="./einstellungen_Main.php?cmdAktion=Einstellungen" method=POST  enctype="multipart/form-data">');
	$Form->Formular_Start();	

	if(($Recht40001&2)==2)
	{	
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_SCHNELLSUCHE'] . ':', 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SCHNELLSUCHE'] . ':',320);
		$Form->Erstelle_Checkbox('CHK_SCHNELLSUCHE',$SchnellSucheFeld,65,true,1);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();
	}
	
	$Form->ZeileStart();
	$Form->Trennzeile('O');
	$Form->ZeileEnde();
	
	
	if(($Recht40001&4)==4)
	{
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_FRAME'] . ':', 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();
		
		
		//a:3:{s:3:"KEY";i:0;s:5:"WHERE";s:0:"";s:5:"ORDER";s:19:" ORDER BY FFR_FILID";}
		
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FRAME1'] . ':',320);
		$Form->Erstelle_Checkbox('CHK_FRAME1',$Frames[0],65,true,1);
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FRAME2'] . ':',320);
		$Form->Erstelle_Checkbox('CHK_FRAME2',$Frames[1],65,true,1);
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FRAME3'] . ':',320);
		$Form->Erstelle_Checkbox('CHK_FRAME3',$Frames[2],65,true,1);
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FRAME4'] . ':',320);
		$Form->Erstelle_Checkbox('CHK_FRAME4',$Frames[3],65,true,1);
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FRAME5'] . ':',320);
		$Form->Erstelle_Checkbox('CHK_FRAME5',$Frames[4],65,true,1);
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
	}
	

	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht40001&2)==2 or ($Recht40001&4)==4)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	
	
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}



?>