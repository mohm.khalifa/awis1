<?php
require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

class operating_funktionen
{
    public $AWISSprachkonserven;
    public $DB;
    public $AWISBenutzer;
    public $Form;
    public $Recht;

    public function __construct()
    {
        $this->Form = new awisFormular();
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();

        $TextKonserven = array();
        $TextKonserven[]=array('Wort','lbl_zurueck');
        $TextKonserven[]=array('Fehler','err_keineRechte');
        $TextKonserven[]=array('TITEL','tit_MobiDataOperating');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('SEH', 'SEH_HOTELNAME');
        $TextKonserven[] = array('MDI', 'lst_MDI_STATUS');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'KeineDatenVorhanden');
        $TextKonserven[] = array('SEH', 'SEH_HOTELNAME');
        $TextKonserven[] = array('MDI', 'lst_MDI_STATUS');
        $TextKonserven[] = array('MDI', 'MDI_NAME');
        $TextKonserven[] = array('MDI', 'MDI_STATUS');
        $TextKonserven[] = array('MDI', 'MDI_START');
        $TextKonserven[] = array('MDI', 'MDI_ENDE');
        $TextKonserven[] = array('MDI', 'MDI_DATUM');
        $TextKonserven[] = array('MDI', 'MDI_DATEI_DE');
        $TextKonserven[] = array('MDI', 'MDI_DATEI_AT');
        $TextKonserven[] = array('MDI', 'MDI_DATEI_CH');

        $this->AWISSprachkonserven = $this->Form->LadeTexte($TextKonserven);
        $this->Recht = $this->AWISBenutzer->HatDasRecht(3128);

        if ($this->Recht == 0) {
            $this->Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', 1, "200907281613");
            die('Keine Rechte');
        }
    }

    //***************************************
    // Bedingung zusammenbauen
    //***************************************
    public function _BedingungErstellen($Param)
    {
        global $AWIS_KEY1;

        $Bedingung = '';

        if (floatval($AWIS_KEY1) != 0) {
            $Bedingung .= 'AND SEP_KEY = ' . $AWIS_KEY1;

            return $Bedingung;
        }

        if (isset($Param['MDI_NAME']) AND $Param['MDI_NAME'] != '') {
            $Bedingung .= 'AND MDJ_KEY in (' . implode(',', $Param['MDI_NAME']) . ') ';
        }

        if (isset($Param['MDI_STATUS']) AND $Param['MDI_STATUS'] != '') {
            $Bedingung .= 'AND MDI_STATUS ' . $this->DB->LikeOderIst($Param['MDI_STATUS']) . ' ';
        }

        if (isset($Param['MDI_DATUM']) AND $Param['MDI_DATUM'] != '') {
            $Bedingung .= 'AND MDI_DATUM ' . $this->DB->LikeOderIst($Param['MDI_DATUM']) . ' ';
        }

        if (isset($Param['MDI_START']) AND $Param['MDI_START'] != '') {
            $Bedingung .= 'AND MDI_START ' . $this->DB->LikeOderIst($Param['MDI_START']) . ' ';
        }

        if (isset($Param['MDI_ENDE']) AND $Param['MDI_ENDE'] != '') {
            $Bedingung .= 'AND MDI_DATUM ' . $this->DB->LikeOderIst($Param['MDI_DATUM']) . ' ';
        }

        if (isset($Param['MDI_DATEI_DE']) AND $Param['MDI_DATEI_DE'] != '') {
            $Bedingung .= 'AND MDI_DATEI_DE ' . $this->DB->LikeOderIst($Param['MDI_DATEI_DE']) . ' ';
        }

        if (isset($Param['MDI_DATEI_AT']) AND $Param['MDI_DATEI_AT'] != '') {
            $Bedingung .= 'AND MDI_DATEI_AT ' . $this->DB->LikeOderIst($Param['MDI_DATEI_AT']) . ' ';
        }

        if (isset($Param['MDI_DATEI_CH']) AND $Param['MDI_DATEI_CH'] != '') {
            $Bedingung .= 'AND MDI_DATEI_CH ' . $this->DB->LikeOderIst($Param['MDI_DATEI_CH']) . ' ';
        }

        return $Bedingung;
    }
}
