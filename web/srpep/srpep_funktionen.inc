<?php
require_once 'awisFormular.inc';
class srpep_funktionen
{
    private $_AWISBenutzer;

    private $_DB;

    private $_Form;

    private $AWISSprachkonserven;

    /**
     * srpep_funktionen constructor.
     *
     * @param awisBenutzer $awisBenutzer
     */
    function __construct(awisBenutzer $awisBenutzer,awisDatenbank $DB)
    {
       $this->_AWISBenutzer = $awisBenutzer;
       $this->_DB = $DB;
       $this->_Form = new awisFormular();
        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[]=array('WTE','%');
        $this->AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    /**
     * Übergangslösung wegen Gebietsreform. Liefert entweder das Datum aus dem Programmparameter oder das aktuelle Datum
     * @return bool|false|string
     * @throws awisException
     */
    public function GueltigBis($OperatorFuerParam = '='){

        $GueltigBis = date('d.m.Y');
        $ParamGueltigBis = $this->_AWISBenutzer->ParameterLesen('SmartRepairGueltigBis',true);
        if($ParamGueltigBis != '-1'){
            return  $OperatorFuerParam . $this->_DB->FeldInhaltFormat('DU',$ParamGueltigBis) ;
        }
        return  ' >= ' . $this->_DB->FeldInhaltFormat('D',$GueltigBis);

    }

    const STATUS_OK_NICHTS_GEAENDERT = 1;
    const STATUS_OK_GEANDERT = 2;
    const STATUS_ERROR_UNBEKANNT = 0;
    const STATUS_ERROR_PERSNR = -1;

    public function UpdateFRZ($PersNrNeu, $FilId){
        //Wenn nichts kommt, Zuordnung deaktivieren
        if($PersNrNeu == ''){
            $SQL = 'update FILIALEBENENROLLENZUORDNUNGEN ';
            $SQL .= 'set FRZ_GUELTIGBIS = sysdate - 1 ';
            $SQL .= 'where FRZ_FER_KEY = 39 and FRZ_XTN_KUERZEL = \'FIL\'';
            $SQL .= ' and FRZ_XXX_KEY = ' . $this->_DB->WertSetzen('FRZ','N0',$FilId);

            $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('FRZ'));

            return self::STATUS_ERROR_PERSNR;
        }
        //Schauen, ob wir die neue Personalnummer kennen
        $SQL = 'select KON_KEY from Kontakte where KON_STATUS = \'A\' and KON_PER_NR = ' . $this->_DB->WertSetzen('KON','T',$PersNrNeu);
        $rsKon = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('KON'));
        $KON_KEY = $rsKon->FeldInhalt('KON_KEY');

        if($KON_KEY == ''){
            return self::STATUS_ERROR_PERSNR;
        }

        //Aktuellen Wert ermitteln
        $SQL = 'select FRZ_KON_KEY, KON_PER_NR , FRZ_KEY from FILIALEBENENROLLENZUORDNUNGEN ';
        $SQL .= ' inner join KONTAKTE on KON_KEY = FRZ_KON_KEY ';
        $SQL .= ' where FRZ_XTN_KUERZEL = \'FIL\' and FRZ_XXX_KEY = ' . $this->_DB->WertSetzen('FRZ','Z',$FilId);
        $SQL .= ' AND FRZ_GUELTIGAB <= trunc(sysdate) and FRZ_GUELTIGBIS >= sysdate ';
        $SQL .= ' AND FRZ_FER_KEY = 39 ';
        $rsFRZ = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FRZ'));

        $this->_Form->DebugAusgabe($this->_DB->LetzterSQL());

        if($rsFRZ->FeldInhalt('KON_PER_NR')!= $PersNrNeu){

            $this->_Form->DebugAusgabe('Filiale: ' . $FilId . ' von ' . $rsFRZ->FeldInhalt('KON_PER_NR') . ' nach ' . $PersNrNeu);
            //Alten auslaufen lassen
            $SQL = 'update FILIALEBENENROLLENZUORDNUNGEN set FRZ_GUELTIGBIS = sysdate - 1 where FRZ_XXX_KEY = ' . $this->_DB->WertSetzen('FRZ','Z',$FilId);
            $SQL .= ' and FRZ_XTN_KUERZEL = \'FIL\' and FRZ_FER_KEY = 39 ';
            $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('FRZ'));

            $this->_Form->DebugAusgabe($this->_DB->LetzterSQL());

            //Neuen anlegen
            $SQL  ='insert into FILIALEBENENROLLENZUORDNUNGEN (';
            $SQL .='     FRZ_XXX_KEY,';
            $SQL .='     FRZ_XTN_KUERZEL,';
            $SQL .='     FRZ_FER_KEY,';
            $SQL .='     FRZ_KON_KEY,';
            $SQL .='     FRZ_GUELTIGAB,';
            $SQL .='     FRZ_GUELTIGBIS,';
            $SQL .='     FRZ_BEMERKUNG,';
            $SQL .='     FRZ_USER,';
            $SQL .='     FRZ_USERDAT';
            $SQL .=' ) values (';

            $SQL .= $this->_DB->WertSetzen('FRZ','T',$FilId);
            $SQL .=' , ' . $this->_DB->WertSetzen('FRZ','T','FIL');
            $SQL .=' , ' . $this->_DB->WertSetzen('FRZ','Z',39);
            $SQL .=' , ' . $this->_DB->WertSetzen('FRZ','Z',$KON_KEY);
            $SQL .=' , trunc(sysdate) ';
            $SQL .=' , ' . $this->_DB->WertSetzen('FRZ','T','31.12.2030');
            $SQL .=' , ' . $this->_DB->WertSetzen('FRZ','T','');
            $SQL .=' , ' . $this->_DB->WertSetzen('FRZ','T',$this->_AWISBenutzer->BenutzerName());
            $SQL .=' , sysdate';
            $SQL .=' )';

            $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('FRZ'));
            $this->_Form->DebugAusgabe($this->_DB->LetzterSQL());
            return self::STATUS_OK_GEANDERT;
        }else{
            return self::STATUS_OK_NICHTS_GEAENDERT;
        }

        return self::STATUS_ERROR_UNBEKANNT;
    }

    public function FilialenDesUsers(){
        $SQL  ='SELECT';
        $SQL .='     FRZ_XXX_KEY AS FIL_ID';
        $SQL .=' FROM';
        $SQL .='     FILIALEBENENROLLENZUORDNUNGEN';
        $SQL .=' WHERE';
        $SQL .='     FRZ_FER_KEY = 39';
        $SQL .='     AND FRZ_GUELTIGAB <= SYSDATE';
        $SQL .='     AND FRZ_GUELTIGBIS >= SYSDATE';
        $SQL .='     AND FRZ_XTN_KUERZEL = \'FIL\'';
        $SQL .='     AND FRZ_KON_KEY = ' . $this->_DB->WertSetzen('XX1','N0',$this->_AWISBenutzer->BenutzerKontaktKEY());
        $SQL .= ' ORDER BY 1 ';

        $rsFil = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('XX1'));
        $Filialen = array();
        while(!$rsFil->EOF()){
            $Filialen[] = $rsFil->FeldInhalt('FIL_ID');
            $rsFil->DSWeiter();
        }

        return $Filialen;
    }

    public function ErstelleAuftragdetailInfofeld($WTE_KEY,$WTE_KUNDENFZVON, $WTE_KUNDENFZBIS){
        $SQL = 'SELECT *';
        $SQL .= ' FROM WerkstatttermineDetails';
        $SQL .= ' WHERE WTD_WTE_KEY = '.$this->_DB->WertSetzen('WTD','Z',$WTE_KEY);
        $SQL .= ' ORDER BY WTD_POSITION';

        $rsWTD = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('WTD'));

        $ToolTipp = '<table>';

        while(!$rsWTD->EOF())
        {
            $ToolTipp .= '<tr><td>'.$rsWTD->FeldInhalt('WTD_AST_ATUNR').'';
            $ToolTipp .= '</td><td>'.str_replace('"','',$rsWTD->FeldInhalt('WTD_BEZEICHNUNG'));
            if($rsWTD->FeldInhalt('WTD_HINWEIS')!='')
            {
                $ToolTipp .= '<br>'.str_replace('"','',$rsWTD->FeldInhalt('WTD_HINWEIS'));
            }
            $ToolTipp .= '</td><td align=right>'.$this->_Form->Format('T',$this->_DB->FeldInhaltFormat('N2',$rsWTD->FeldInhalt('WTD_VK'))*$rsWTD->FeldInhalt('WTD_MENGE'));
            $ToolTipp .= '&nbsp;&nbsp;';

            $ToolTipp .= '</td></tr>';
            $rsWTD->DSWeiter();
        }

        // Kundenfahrzeuganlieferung anzeigen
        $ToolTipp.='<tr><td colspan=\'4\'>'.$this->AWISSprachkonserven['WTE']['WTE_KUNDENFZVON'].': ';
        $ToolTipp.=$this->_Form->Format('DU',$WTE_KUNDENFZVON,false).' - '.$this->_Form->Format('DU',$WTE_KUNDENFZBIS,false).'</td></tr>';
        $ToolTipp.='</table>';
        $ToolTipp = addslashes($ToolTipp);

        echo '<img class="Auftraginfo" border=0 width=16 height=16 src="/bilder/info.png" title="'.$ToolTipp.'" >';

        echo '<script> 
                    $(".Auftraginfo").tooltipster({ contentAsHTML: true,
                                                      interactive: true}); 
             </script>';
    }
}