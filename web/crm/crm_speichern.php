<?php
require_once('awisDokumente.php');
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('CAD','CAD_%');
$TextKonserven[]=array('CGK','CGK_%');

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if(isset($_POST['txtCAD_KEY']))
{
	$AWIS_KEY1 = $_POST['txtCAD_KEY'];
	awis_Debug(1,$_POST);

	if(intval($_POST['txtCAD_KEY'])===0)		// Neue Adresse
	{
			// daten auf Vollst�ndigkeit pr�fen
		$Fehler = '';
		$Pflichtfelder = array('CAD_NAME1','CAD_STRASSE','CAD_PLZ','CAD_ORT','CAD_TELEFON');
		
		// Pflichtfelder ermitteln welche von FA ueber Maske festgelegt wurden und an Pflichtfelder-Array anhaengen
		$SQL = 'SELECT DISTINCT cpf_pflichtfeld ';
		$SQL .= 'FROM crmpflichtfelder ';
		$SQL .= "WHERE cpf_ranking = " .awis_FeldInhaltFormat('T',$_POST['txtCAD_RANKING'],true). " ";
		$SQL .= "OR cpf_kon_key = " . awis_FeldInhaltFormat('Z',$_POST['txtCAD_KON_KEY'],true);
		
		$rsCPF = awisOpenRecordset($con,$SQL);
		$rsCPFZeilen = $awisRSZeilen;
		
		for($CPFZeile=0;$CPFZeile<$rsCPFZeilen;$CPFZeile++)
		{
			$Pflichtfelder[] = $rsCPF['CPF_PFLICHTFELD'][$CPFZeile];
		}
		
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['CAD'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$SQL = 'INSERT INTO CRMAdressen';
		$SQL .= '(CAD_ANREDE,CAD_NAME1, CAD_NAME2';
		$SQL .= ',CAD_STRASSE, CAD_HAUSNUMMER, CAD_STATUS, CAD_RANKING';
		$SQL .= ',CAD_LAN_CODE, CAD_PLZ, CAD_ORT, CAD_BEMERKUNG, CAD_KON_KEY';
		$SQL .= ',CAD_TELEFON,CAD_WEBSEITE, CAD_EMAIL, CAD_CGB_KEY, CAD_TELEFAX, CAD_ORTSTEIL';
		$SQL .= ',CAD_USER, CAD_USERDAT';
		$SQL .= ')VALUES ('.awis_FeldInhaltFormat('Z',$_POST['txtCAD_ANREDE'],false);
		$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCAD_NAME1']),false);
		$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCAD_NAME2']),true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCAD_STRASSE']),true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCAD_HAUSNUMMER']),true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_STATUS'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_RANKING'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_LAN_CODE'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_PLZ'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCAD_ORT']),true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',(isset($_POST['txtCAD_BEMERKUNG'])?$_POST['txtCAD_BEMERKUNG']:''),true);
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCAD_KON_KEY'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('TZ',$_POST['txtCAD_TELEFON'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_WEBSEITE'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_EMAIL'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCAD_CGB_KEY'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('TZ',$_POST['txtCAD_TELEFAX'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_ORTSTEIL'],true);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'NEU/1');
			die();
		}
		$SQL = 'SELECT seq_CAD_KEY.CurrVal AS KEY FROM DUAL';
		$rsKey = awisOpenRecordset($con,$SQL);
		$AWIS_KEY1=$rsKey['KEY'][0];
		awis_BenutzerParameterSpeichern($con, "AktuellerCAD" , $AWISBenutzer->BenutzerName() ,$rsKey['KEY'][0]);
		
		// Infofelder speichern
		$InfoFelder = array();
		$InfoFelder[4010] = $_POST['txtINFO_Vertragsstatus'];
		
		foreach($InfoFelder as $key => $value)
		{
			$BindeVariablen=array();
			$BindeVariablen['var_N0_cin_cit_id'] = $key;
			$BindeVariablen['var_T_cin_wert'] = $value;
			$BindeVariablen['var_N0_cin_cad_key'] = $AWIS_KEY1;
			$BindeVariablen['var_T_user'] = $AWISBenutzer->BenutzerName();
		
			$SQL = "INSERT INTO crminfos ";
			$SQL .= "(cin_cad_key,cin_cit_id,cin_wert,cin_user,cin_userdat) values ";
			$SQL .= "(:var_N0_cin_cad_key,:var_N0_cin_cit_id,:var_T_cin_wert,:var_T_user,sysdate)";
		
			if(awisExecute($con, $SQL, true, false, 0 , $BindeVariablen)===false)
			{
				awisErrorMailLink('CRMAdressenAP',2,'Fehler beim Speichern',$SQL);
				awisLogoff($con);
				die();
			}
		}
	}
    elseif(isset($_POST['cmdUpdBest_x']))
    {
        // durch OPAL Update wird Updatekenner gesetzt der durch diesen Button
        // wieder zuruekgesetzt wird --> CRMInfo-Eintrag loeschen

        $BindeVariablen=array();
		$BindeVariablen['var_N0_cad_key']='0'.$_POST['txtCAD_KEY'];

        $SQL = 'DELETE crminfos ';
        $SQL .= 'WHERE cin_cit_id = 4004 ';
        $SQL .= 'AND cin_cad_key=:var_N0_cad_key';

        if(awisExecute($con, $SQL, true, false, 0 , $BindeVariablen)===false)
        {
            awisErrorMailLink('CRMAdressen',1,'Fehler beim Best�tigen',$SQL);
        }
    }
	else 					// ge�nderter CRMAdressen
	{
		$Pflichtfelder = array();
		$Fehler = '';
		
		// Pflichtfelder ermitteln welche von FA ueber Maske festgelegt wurden und an Pflichtfelder-Array anhaengen
		$SQL = 'SELECT DISTINCT cpf_pflichtfeld ';
		$SQL .= 'FROM crmpflichtfelder ';
		$SQL .= "WHERE cpf_ranking = " .awis_FeldInhaltFormat('T',$_POST['txtCAD_RANKING'],true). " ";
		$SQL .= "OR cpf_kon_key = " . awis_FeldInhaltFormat('Z',$_POST['txtCAD_KON_KEY'],true);
		
		$rsCPF = awisOpenRecordset($con,$SQL);
		$rsCPFZeilen = $awisRSZeilen;
		
		for($CPFZeile=0;$CPFZeile<$rsCPFZeilen;$CPFZeile++)
		{
			$Pflichtfelder[] = $rsCPF['CPF_PFLICHTFELD'][$CPFZeile];
		}
		
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['CAD'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
				
		$Felder = explode(';',awis_NameInArray($_POST, 'txtCAD',1,1));
		$FehlerListe = array();
		$UpdateFelder = '';

		awis_BenutzerParameterSpeichern($con, "AktuellerCAD" , $AWISBenutzer->BenutzerName() ,$_POST['txtCAD_KEY']);
		$BindeVariablen=array();
		$BindeVariablen['var_N0_cad_key']='0'.$_POST['txtCAD_KEY'];
		$rsCAD = awisOpenRecordset($con,'SELECT * FROM CRMAdressen WHERE cad_key=:var_N0_cad_key',true,false,$BindeVariablen);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
				// Alten und neuen Wert umformatieren!!
				switch ($FeldName)
				{
					case 'CAD_TELEFON':
						$WertNeu=awis_FeldInhaltFormat('TZ',$_POST[$Feld],true);
						break;
					case 'CAD_RANKING':	// Keine ::Bitte waehlen:: Eintraege
					case 'CAD_KON_KEY':
						if($_POST[$Feld]==-1)
						{
							$_POST[$Feld]='';
							$WertNeu='';
						}
						$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
						break;
					default:
						$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				}
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCAD[$FeldName][0],true);
		//echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCAD['CAD_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$BindeVariablen=array();
			$BindeVariablen['var_N0_cad_key']='0'.$_POST['txtCAD_KEY'];
			$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
			
			$SQL = 'UPDATE CRMAdressen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', cad_user=:var_T_user';
			$SQL .= ', cad_userdat=sysdate';
			$SQL .= ' WHERE cad_key=:var_N0_cad_key';
awis_Debug(1,$SQL,$_POST );
			if(awisExecute($con, $SQL, true, false, 0 , $BindeVariablen)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}

		$AWIS_KEY1=$_POST['txtCAD_KEY'];
	//awis_Debug(1,$FeldListe,$SQL,$Felder);

		// Infofelder (z.B. Provision) updaten
		$InfoFelder = array();
		$InfoFelder[4010] = (isset($_POST['txtINFO_Provision']) ? 'on' : 'off');
		
		foreach($InfoFelder as $key => $value)
		{
			$BindeVariablen=array();
			$BindeVariablen['var_N0_cin_cit_id'] = $key;
			$BindeVariablen['var_T_cin_wert'] = $value;
			$BindeVariablen['var_N0_cin_cad_key'] = $AWIS_KEY1;
			$BindeVariablen['var_T_user'] = $AWISBenutzer->BenutzerName();
			
			// pruefen ob Info-DS vorhanden ist
			$SQL = "SELECT cin_wert ";
			$SQL .= "FROM crminfos ";
			$SQL .= "WHERE cin_cad_key = :var_N0_cin_cad_key ";
			$SQL .= "AND cin_cit_id = :var_N0_cin_cit_id";
			
			$rsCIN = awisOpenRecordset($con,$SQL,'','', $BindeVariablen);
			$rsCINZeilen = $awisRSZeilen;
			
			if($rsCINZeilen > 0 && $InfoFelder[4010] == 'off')
			{
				$SQL = "DELETE ";
				$SQL .= "FROM crminfos ";
				$SQL .= "WHERE cin_cad_key = " . $AWIS_KEY1;
				$SQL .= " AND cin_cit_id = " . $key;
				
				if(awisExecute($con, $SQL, true, false,0,'')===false)
				{
					awisErrorMailLink('CRMAdressenAP',2,'Fehler beim Speichern',$SQL);
					awisLogoff($con);
					die();
				}
			}
			elseif($rsCINZeilen == 0 && $InfoFelder[4010] == 'on')
			{
				$SQL = "INSERT INTO crminfos ";
				$SQL .= "(cin_cad_key,cin_cit_id,cin_wert,cin_user,cin_userdat) values ";
				$SQL .= "(:var_N0_cin_cad_key,:var_N0_cin_cit_id,:var_T_cin_wert,:var_T_user,sysdate)";
				
				if(awisExecute($con, $SQL, true, false, 0 , $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMAdressenAP',2,'Fehler beim Speichern',$SQL);
					awisLogoff($con);
					die();
				}
			}
			
			
		}

	}

	//*************************************************************************
	//** Kontakttypen f�r einen Ansprechpartner
	//*************************************************************************
	$Felder = awis_NameInArray($_POST, 'txtCAK_',1,1);
	if($Felder!='')
	{
		$AWIS_KEY2=$_POST['txtCAP_KEY'];
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('Wort','geaendert_auf');
		$TextKonserven[]=array('CAK','CAK_%');
		$TextKonserven[]=array('Meldung','DSVeraendert');
		$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
		$FeldListe='';
awis_Debug(1,$_POST);
		if(isset($_POST['txtCAK_KEY']) AND $_POST['txtCAK_KEY']==0)	// Neuer Kontakt
		{
			$BindeVariablen=array();
			$BindeVariablen['var_N0_cak_cap_key']=intval('0'.$_POST['txtCAP_KEY']);
			$BindeVariablen['var_N0_cak_kot_key']='0'.$_POST['txtCAK_KOT_KEY'];
			$BindeVariablen['var_T_cak_wert']=$_POST['txtCAK_WERT'];
			$BindeVariablen['var_T_cak_bemerkung']=$_POST['txtCAK_BEMERKUNG'];
			$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
			
			$SQL = 'INSERT INTO CRMAnsprechpartnerKontakte';
			$SQL .= '(CAK_CAP_KEY, CAK_KOT_KEY, CAK_WERT, CAK_BEMERKUNG';
			$SQL .= ',CAK_User, CAK_UserDat)';
			$SQL .= 'VALUES(';
			$SQL .= ':var_N0_cak_cap_key';
			$SQL .= ', :var_N0_cak_kot_key';
			$SQL .= ', :var_T_cak_wert';
			$SQL .= ', :var_T_cak_bemerkung';
			$SQL .= ', :var_T_user';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL,true,false,0,$BindeVariablen))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'NEU/1');
				die();
			}
		}
		else 	//Update
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$BindeVariablen=array();
			$BindeVariablen['var_N0_cak_key']='0'.$_POST['txtCAK_KEY'];
			$rsCAK = awisOpenRecordset($con,'SELECT * FROM CRMAnsprechpartnerKontakte WHERE CAK_key=:var_N0_cak_key',true,false,$BindeVariablen);
		//awis_Debug(1,$_POST,$Felder);
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
					$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
					$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCAK[$FeldName][0],true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsCAK['CAK_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
			}
			elseif($FeldListe!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_cak_key']='0'.$_POST['txtCAK_KEY'];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMAnsprechpartnerKontakte SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', CAK_user=:var_T_user';
				$SQL .= ', CAK_userdat=sysdate';
				$SQL .= ' WHERE CAK_key=:var_N0_cak_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
				}
			}

		}

	}

	//*************************************************************************
	//** Ansprechpartner
	//*************************************************************************
	$Felder = awis_NameInArray($_POST, 'txtCAP_',1,1);
	if($Felder!='')
	{
		$AWIS_KEY2=$_POST['txtCAP_KEY'];
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('Wort','geaendert_auf');
		$TextKonserven[]=array('CAP','CAP_%');
		$TextKonserven[]=array('Meldung','DSVeraendert');
		$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
		$FeldListe='';
		if($_POST['txtCAP_KEY']=='0')
		{
			$Fehler = '';
			$SQL = 'INSERT INTO CRMAnsprechpartner';
			$SQL .= '(CAP_CAD_KEY, CAP_ANREDE, CAP_NACHNAME, CAP_VORNAME, CAP_BEMERKUNG, CAP_FUNKTION';
			$SQL .= ',CAP_RANKING,CAP_GEBURTSTAG, CAP_AKTIONEN, CAP_MITARBEITERSEIT, CAP_MITARBEITERBIS';
			$SQL .= ',CAP_USER, CAP_USERDAT';
			$SQL .= ')VALUES ('.$AWIS_KEY1;
			$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCAP_ANREDE'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAP_NACHNAME'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAP_VORNAME'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAP_BEMERKUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAP_FUNKTION'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCAP_RANKING'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtCAP_GEBURTSTAG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCAP_AKTIONEN'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtCAP_MITARBEITERSEIT'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtCAP_MITARBEITERBIS'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'NEU/1');
				die();
			}
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$BindeVariablen=array();
			$BindeVariablen['var_N0_cap_key']='0'.$_POST['txtCAP_KEY'];
			$rsCAP = awisOpenRecordset($con,'SELECT * FROM CRMAnsprechpartner WHERE CAP_key=:var_N0_cap_key',true,false,$BindeVariablen);
		//awis_Debug(1,$_POST,$Felder);
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
					$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
					$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCAP[$FeldName][0],true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsCAP['CAP_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
			}
			elseif($FeldListe!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_cap_key']='0'.$_POST['txtCAP_KEY'];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMAnsprechpartner SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', CAP_user=:var_T_user';
				$SQL .= ', CAP_userdat=sysdate';
				$SQL .= ' WHERE CAP_key=:var_N0_cap_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
				}
			}

		}
	}

	//*************************************************************************
	//** Adressen-Aktionen
	//*************************************************************************
	$Felder = awis_NameInArray($_POST, 'txtCKO_',1,1);
	if($Felder!='')
	{
		$AWIS_KEY2=$_POST['txtCKO_KEY'];
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('Wort','geaendert_auf');
		$TextKonserven[]=array('CKO','CKO_%');
		$TextKonserven[]=array('Meldung','DSVeraendert');
		$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
		$FeldListe='';
		$SQL = '';

		if($_POST['txtCKO_KEY']=='0')
		{
			if($_POST['txtCKO_CAT_KEY']!='')
			{
				$Fehler = '';
				$SQL = 'INSERT INTO CRMAktionen';
				$SQL .= '(CKO_XXX_KEY, CKO_CAT_KEY, CKO_BEMERKUNG, CKO_LETZTEAKTION';
				$SQL .= ',CKO_USER, CKO_USERDAT';
				$SQL .= ')VALUES ('.$AWIS_KEY1;
				$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCKO_CAT_KEY'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKO_BEMERKUNG'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtCKO_LETZTEAKTION'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				if(!awisExecute($con,$SQL))
				{
					awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200710021750');
					die();
				}
			}

		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$BindeVariablen=array();
			$BindeVariablen['var_N0_cko_key']='0'.$_POST['txtCKO_KEY'];
			$rsCKO = awisOpenRecordset($con,'SELECT * FROM CRMAktionen WHERE CKO_key=:var_N0_cko_key',true,false,$BindeVariablen);
		//awis_Debug(1,$_POST,$Felder);
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
					$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
					$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKO[$FeldName][0],true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsCKO['CKO_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
			}
			elseif($FeldListe!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_cko_key']='0'.$_POST['txtCKO_KEY'];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMAktionen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', CKO_user=:var_T_user';
				$SQL .= ', CKO_userdat=sysdate';
				$SQL .= ' WHERE CKO_key=:var_N0_cko_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
				}
			}

		}

		$AWIS_KEY2=0;		// Zur�ck in die Liste
	}

	//*************************************************************************
	//** Adressen-Wiedervorlagen
	//*************************************************************************
	$Felder = awis_NameInArray($_POST, 'txtCWV_',1,1);
	if($Felder!='')
	{
		$AWIS_KEY2=$_POST['txtCWV_KEY'];
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('Wort','geaendert_auf');
		$TextKonserven[]=array('CWV','CWV_%');
		$TextKonserven[]=array('Meldung','DSVeraendert');
		$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
		$FeldListe='';
		$SQL = '';

		if($_POST['txtCWV_KEY']=='0')
		{
			if($_POST['txtCWV_KON_KEY']!='')	// Nur, wenn ein Kontakt angegeben ist
			{
				$Fehler = '';
				$SQL = 'INSERT INTO CRMWiedervorlagen';
				$SQL .= '(CWV_CAD_KEY, CWV_KON_KEY, CWV_BEMERKUNG, CWV_DATUM';
				$SQL .= ',CWV_USER, CWV_USERDAT';
				$SQL .= ')VALUES ('.$AWIS_KEY1;
				$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCWV_KON_KEY'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCWV_BEMERKUNG'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtCWV_DATUM'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				if(!awisExecute($con,$SQL))
				{
					awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200710021750');
					die();
				}
			}

		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$BindeVariablen=array();
			$BindeVariablen['var_N0_cwv_key']='0'.$_POST['txtCWV_KEY'];
			$rsCWV = awisOpenRecordset($con,'SELECT * FROM CRMWiedervorlagen WHERE CWV_key=:var_N0_cwv_key',true,false,$BindeVariablen);
		//awis_Debug(1,$_POST,$Felder);
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
					$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
					$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCWV[$FeldName][0],true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsCWV['CWV_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
			}
			elseif($FeldListe!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_cwv_key']='0'.$_POST['txtCWV_KEY'];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMWiedervorlagen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', CWV_user=:var_T_user';
				$SQL .= ', CWV_userdat=sysdate';
				$SQL .= ' WHERE CWV_key=:var_N0_cwv_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
				}
			}

		}
	}


	//*************************************************************************
	//** Adressen-Kontakte
	//*************************************************************************
	$Felder = awis_NameInArray($_POST, 'txtCKT_',1,1);
	if($Felder!='')
	{
		$AWIS_KEY2=$_POST['txtCKT_KEY'];
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('Wort','geaendert_auf');
		$TextKonserven[]=array('CKT','CKT_%');
		$TextKonserven[]=array('Meldung','DSVeraendert');
		$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
		$FeldListe='';
		$SQL = '';

		if($_POST['txtCKT_KEY']=='0')
		{
			$Speichern = true;
			if($_POST['txtCKT_KON_KEY']=='')	// Nur, wenn ein Kontakt angegeben ist
			{
				$Speichern = false;
			}

			if($Speichern)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO CRMKontakte';
				$SQL .= '(CKT_CAD_KEY, CKT_KON_KEY, CKT_BEMERKUNG, CKT_DATUM';
				$SQL .= ', CKT_CKA_KEY, CKT_CKE_KEY, CKT_CAP_KEY';
				$SQL .= ',CKT_USER, CKT_USERDAT';
				$SQL .= ')VALUES ('.$AWIS_KEY1;
				$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCKT_KON_KEY'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKT_BEMERKUNG'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtCKT_DATUM'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCKT_CKA_KEY'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCKT_CKE_KEY'],true);
				$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCKT_CAP_KEY'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				if(!awisExecute($con,$SQL))
				{
					awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200710021750');
					die();
				}
			}


		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$BindeVariablen=array();
			$BindeVariablen['var_N0_ckt_key']='0'.$_POST['txtCKT_KEY'];
			$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMKontakte WHERE CKT_key=:var_N0_ckt_key',true,false,$BindeVariablen);
		//awis_Debug(1,$_POST,$Felder);
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
					$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
					$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsCKT['CKT_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
			}
			elseif($FeldListe!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_ckt_key']='0'.$_POST['txtCKT_KEY'];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMKontakte SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', CKT_user=:var_T_user';
				$SQL .= ', CKT_userdat=sysdate';
				$SQL .= ' WHERE CKT_key=:var_N0_ckt_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
				}
			}

		}
	}

	//*************************************************************************
	//** Adressen-Vertragsstatus
	//*************************************************************************
	$Felder = awis_NameInArray($_POST, 'txtCAQ_',1,1);
	if($Felder!='')
	{
		$AWIS_KEY2=$_POST['txtCAQ_KEY'];
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('Wort','geaendert_auf');
		$TextKonserven[]=array('CAQ','CAQ_%');
		$TextKonserven[]=array('Meldung','DSVeraendert');
		$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
		$FeldListe='';
		$SQL = '';

		if($_POST['txtCAQ_KEY']=='0')
		{

		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$BindeVariablen=array();
			$BindeVariablen['var_N0_caq_key']='0'.$_POST['txtCAQ_KEY'];			
			$rsCAQ = awisOpenRecordset($con,'SELECT * FROM CRMAKQUISESTAND WHERE CAQ_key=:var_N0_caq_key',true,false,$BindeVariablen);
//		awis_Debug(1,$_POST,$Felder);
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);

				// MUss numerisch sein, wg. Grosskundendaten!
				if($FeldName=='CAQ_VERTRAGSNUMMER')
				{
					$_POST['txtCAQ_VERTRAGSNUMMER']=floatval($_POST['txtCAQ_VERTRAGSNUMMER']);
					
					if($_POST['txtCAQ_VERTRAGSNUMMER'] != 0)
					{
						$BindeVariablen=array();
						$BindeVariablen['var_N0_caq_cad_key'] = $rsCAQ['CAQ_CAD_KEY'][0];
						$SQL = "SELECT cad_ranking, cad_status FROM crmadressen WHERE cad_key =:var_N0_caq_cad_key";
						$rsCADPruef = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
					}
				}

				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
					$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
					$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCAQ[$FeldName][0],true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsCAQ['CAQ_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
			}
			elseif($FeldListe!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_caq_key']='0'.$_POST['txtCAQ_KEY'];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMAKQUISESTAND SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', CAQ_user=:var_T_user';
				$SQL .= ', CAQ_userdat=sysdate';
				$SQL .= ' WHERE CAQ_key=:var_N0_caq_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
				}
								
				// bei Kleinflotte (Ranking = 1) soll mit Pflege der Vertragsnummer der Status von
				// Interessent (Status = I) auf Kunde (Status = K) geaendert werden
				if(isset($rsCADPruef) && $rsCADPruef['CAD_STATUS'][0] == 'I' && $rsCADPruef['CAD_RANKING'][0] == 1)
				{					
					$BindeVariablen=array();
					$BindeVariablen['var_N0_caq_cad_key'] = $rsCAQ['CAQ_CAD_KEY'][0];
					
					$SQL = "UPDATE crmadressen ";
					$SQL .= "SET cad_status = 'K' ";
					$SQL .= "WHERE cad_key =:var_N0_caq_cad_key";
					
					if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
					{
						awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
					}	
				}
			}
awis_Debug(1,$SQL);
		}
	}

	//*************************************************************************
	//** Adressen-Infos
	//*************************************************************************
	$Felder = awis_NameInArray($_POST, 'txtCIN_',1,1);
	if($Felder!='' && !isset($_POST['cmdUpdBest_x']))
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('Wort','geaendert_auf');
		$TextKonserven[]=array('CIN','CIN_%');
		$TextKonserven[]=array('Meldung','DSVeraendert');
		$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
		$FeldListe='';
		$SQL = '';

		$Felder = explode(';',awis_NameInArray($_POST, 'txtCIN_WERT_',1,1));
		foreach($Felder as $Feld)
		{
			$FeldTeile = explode('_',$Feld);	// Format: CIN_WERT_<CIT_ID>_<CIN_KEY>
			if($FeldTeile[3]=='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_cin_cad_key']=$AWIS_KEY1;
				$BindeVariablen['var_N0_cin_cit_id']=$FeldTeile[2];
				if ($_POST[$Feld]!='') {
					$BindeVariablen['var_T_cin_wert']=$_POST[$Feld];
				}
				
				$SQL = 'SELECT CIN_KEY FROM CRMInfos';
				$SQL .= ' WHERE CIN_CAD_KEY=:var_N0_cin_cad_key';
				$SQL .= ' AND CIN_CIT_ID=:var_N0_cin_cit_id';
				$SQL .= ' AND CIN_WERT ' . ($_POST[$Feld]==''?'IS NULL':'= :var_T_cin_wert');
				$rsDaten = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
				if(!isset($rsDaten['CIN_KEY'][0]) OR (isset($rsDaten['CIN_KEY'][0]) AND $rsDaten['CIN_KEY'][0]==''))
				{
					$Fehler = '';
					$BindeVariablen=array();
					$BindeVariablen['var_T_cin_cad_key']=$AWIS_KEY1;
					$BindeVariablen['var_T_cin_cit_id']=$FeldTeile[2];
					$BindeVariablen['var_T_cin_wert']=$_POST[$Feld];
					$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
					
					$SQL = 'INSERT INTO CRMInfos';
					$SQL .= '(CIN_CAD_KEY, CIN_CIT_ID, CIN_WERT';
					$SQL .= ',CIN_USER, CIN_USERDAT';
					$SQL .= ')VALUES (:var_T_cin_cad_key';
					$SQL .= ',:var_T_cin_cit_id';
					$SQL .= ',:var_T_cin_wert';
					$SQL .= ',:var_T_user';
					$SQL .= ',SYSDATE';
					$SQL .= ')';
					if(!awisExecute($con,$SQL,true,false,0,$BindeVariablen))
					{
						awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200711180813');
						die();
					}
				}
			}
			else 					// ge�nderter Datensatz
			{
				$FehlerListe = array();
				$UpdateFelder = '';
				$BindeVariablen=array();
				$BindeVariablen['var_N0_cin_key']=$FeldTeile[3];
				$BindeVariablen['var_T_cin_wert']=$_POST[$Feld];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMInfos SET';
				$SQL .= ' CIN_WERT = :var_T_cin_wert';
				$SQL .= ', CIN_user=:var_T_user';
				$SQL .= ', CIN_userdat=sysdate';
				$SQL .= ' WHERE CIN_key=:var_N0_cin_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200711180814');
				}
			}

		}
	}

	//*****************************************************
	// Dokumente speichern
	//*****************************************************
	$Felder = awis_NameInArray($_POST, 'txtDOC_',1,1);
	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$DOC = new awisDokumente();
		$Zuordnungen = array('CAD'=>$_POST['txtCAD_KEY']);
		$AWIS_KEY2 = $DOC->DokumentSpeichern($_POST['txtDOC_KEY'],'DOC',$_POST['txtDOC_BEZEICHNUNG'],$_POST['txtDOC_DATUM'],$_POST['txtDOC_BEMERKUNG'],$AWISBenutzer->BenutzerID(),$Zuordnungen);
	}



	//****************************************************************
	// Potentialseite
	//
	// Es gibt nur Updates, keine Insert, da alle Daten �ber eine SP
	// erzeugt werden. Es sind immer alle Datens�tze vorhanden!!
	//****************************************************************

	if(isset($_POST['txtCPO_KEY']))
	{
	//awis_Debug(1,$_POST);
		// Teil1: das Potential selber
		$Felder = awis_NameInArray($_POST, 'txtCPO_',1,1);
		if($Felder!='')
		{
			$AWIS_KEY2=$_POST['txtCPO_KEY'];
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('Wort','geaendert_auf');
			$TextKonserven[]=array('CPO','CPO_%');
			$TextKonserven[]=array('Meldung','DSVeraendert');
			$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
			$FeldListe='';
			$SQL = '';

			$FehlerListe = array();
			$UpdateFelder = '';

			$BindeVariablen=array();
			$BindeVariablen['var_N0_cpo_key']='0'.$_POST['txtCPO_KEY'];
			$rsCPO = awisOpenRecordset($con,'SELECT * FROM CRMPotential WHERE CPO_key=:var_N0_cpo_key',true,false,$BindeVariablen);
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
					$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
					$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCPO[$FeldName][0],true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsCPO['CPO_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
			}
			elseif($FeldListe!='')
			{
				$BindeVariablen=array();
				$BindeVariablen['var_N0_cpo_key']='0'.$_POST['txtCPO_KEY'];
				$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
				
				$SQL = 'UPDATE CRMPotential SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', CPO_user=:var_T_user';
				$SQL .= ', CPO_userdat=sysdate';
				$SQL .= ' WHERE CPO_key=:var_N0_cpo_key';
				if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
				{
					awisErrorMailLink('CRMPotential',1,'Fehler beim Speichern',$SQL);
				}
			}

		}	// Ende Potentialseite

		//****************************
		// Teil 2: Fahrzeuge
		//****************************
		$Felder = awis_NameInArray($_POST, 'txtCFZ_ANZAHL',1,1);
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$SQL = '';

			foreach($Felder AS $Feld)
			{
				$Key =intval(substr($Feld,strlen('txtCFZ_ANZAHL_')));
				if($_POST['txtCFZ_ANZAHL_'.$Key] != $_POST['oldCFZ_ANZAHL_'.$Key] OR $_POST['txtCFZ_ANTEIL_'.$Key] != $_POST['oldCFZ_ANTEIL_'.$Key])
				{
					$BindeVariablen=array();
					$BindeVariablen['var_N0_cfz_anzahl']='0'.$_POST['txtCFZ_ANZAHL_'.$Key];
					$BindeVariablen['var_N0_cfz_anteil']='0'.$_POST['txtCFZ_ANTEIL_'.$Key];
					$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
					$BindeVariablen['var_N0_cfz_key']='0'.$Key;
					
					$SQL = 'UPDATE CRMFahrzeuge ';
					$SQL .= 'SET CFZ_ANZAHL=:var_N0_cfz_anzahl';
					$SQL .= ', CFZ_ANTEIL=:var_N0_cfz_anteil';
					$SQL .= ', CFZ_User=:var_T_user';
					$SQL .= ', CFZ_USERDAT = SYSDATE';
					$SQL .= ' WHERE CFZ_KEY=:var_N0_cfz_key';
					if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
					{
						awisErrorMailLink('CRMPotential',1,'Fehler beim Speichern',$SQL);
					}
				}
			}
		}


		//****************************
		// Teil 3:Potentialdetails
		//****************************
		$Felder = awis_NameInArray($_POST, 'txtCPD_STATUS_',1,1);
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$SQL = '';

			foreach($Felder AS $Feld)
			{
				$Key =intval(substr($Feld,strlen('txtCPD_STATUS_')));
				if($_POST['txtCPD_STATUS_'.$Key] != $_POST['oldCPD_STATUS_'.$Key])
				{
					$BindeVariablen=array();
					$BindeVariablen['var_N0_cpd_status']='0'.$_POST['txtCPD_STATUS_'.$Key];
					$BindeVariablen['var_T_user']=$AWISBenutzer->BenutzerName();
					$BindeVariablen['var_N0_cpd_key']='0'.$Key;
					
					$SQL = 'UPDATE CRMPotentialDetails ';
					$SQL .= 'SET CPD_STATUS=:var_N0_cpd_status';
					$SQL .= ', CPD_User=:var_T_user';
					$SQL .= ', CPD_USERDAT = SYSDATE';
					$SQL .= ' WHERE CPD_KEY=:var_N0_cpd_key';
					if(awisExecute($con, $SQL, true, false, 0, $BindeVariablen)===false)
					{
						awisErrorMailLink('CRMPotential',1,'Fehler beim Speichern',$SQL);
					}
				}
			}
		}
	}



	// Aktuellen CAD speichern
	awis_BenutzerParameterSpeichern($con, "AktuellerCAD" , $AWISBenutzer->BenutzerName() ,$AWIS_KEY1);

}

//*************************************************************************
//** Konditionsblattversand
//*************************************************************************
$Felder = awis_NameInArray($_POST, 'txtCKV_',1,1);
if($Felder!='') {
    $AWIS_KEY2 = $_POST['txtCKV_KEY'];
    $Felder = explode(';', $Felder);

    if (isset($_POST['txtCKV_KEY']) AND $_POST['txtCKV_KEY'] == 0) {
        $BindeVariablen = [];
        $BindeVariablen['var_T_ckv_name'] = $_POST['txtCKV_NAME'];
        $BindeVariablen['var_N0_ckv_quelle'] = $_POST['txtCKV_QUELLE'];
        $BindeVariablen['var_DU_ckv_datum'] = $_POST['txtCKV_DATUM'];
        $BindeVariablen['var_N0_ckv_status'] = 0;

        $SQL = 'INSERT INTO CRMKONDBLATTVERSAND';
        $SQL .= ' (CKV_NAME, CKV_QUELLE, CKV_DATUM, CKV_STATUS)';
        $SQL .= ' VALUES(';
        $SQL .= ':var_T_ckv_name,';
        $SQL .= ':var_N0_ckv_quelle,';
        $SQL .= ':var_DU_ckv_datum,';
        $SQL .= ':var_N0_ckv_status';
        $SQL .= ')';

        if (!awisExecute($con, $SQL, true, false, 0, $BindeVariablen)) {
            awisErrorMailLink('crmadressen_speichern.php', 1, $awisDBError['message'], 'NEU/1');
            die();
        }
    } else {
        $FehlerListe = [];
        $UpdateFelder = '';

        $BindeVariablen = [];
        $BindeVariablen['var_N0_ckv_key'] = '0' . $_POST['txtCKV_KEY'];
        $rsCKV = awisOpenRecordset($con, 'SELECT * FROM CRMKONDBLATTVERSAND WHERE CKV_key=:var_N0_ckv_key', true, false, $BindeVariablen);

        $FeldListe = '';
        foreach ($Felder AS $Feld) {
            $FeldName = substr($Feld, 3);
            if (isset($_POST['old' . $FeldName])) {
                // Alten und neuen Wert umformatieren!!
                $WertNeu = awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'], $_POST[$Feld], true);
                $WertAlt = awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'], $_POST['old' . $FeldName], true);
                $WertDB = awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'], $rsCKV[$FeldName][0], true);
                if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                    if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                        $FehlerListe[] =
                            [
                                $FeldName,
                                $WertAlt,
                                $WertDB
                            ];
                    } else {
                        $FeldListe .= ', ' . $FeldName . '=';

                        if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                        {
                            $FeldListe .= ' null';
                        } else {
                            $FeldListe .= $WertNeu;
                        }
                    }
                }
            }
        }

        if (count($FehlerListe) > 0) {
            $Meldung = str_replace('%1', $rsCAK['CAK_USER'][0], $TXT_Speichern['Meldung']['DSVeraendert']);
            foreach ($FehlerListe AS $Fehler) {
                $Meldung .= '<br>&nbsp;' . $Fehler[0] . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
            }
            awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
        } elseif ($FeldListe != '') {
            $BindeVariablen = [];
            $BindeVariablen['var_N0_ckv_key'] = '0' . $_POST['txtCKV_KEY'];
            $BindeVariablen['var_T_user'] = $AWISBenutzer->BenutzerName();

            $SQL = 'UPDATE CRMKONDBLATTVERSAND SET';
            $SQL .= substr($FeldListe, 1);
            $SQL .= ', CKV_user=:var_T_user';
            $SQL .= ', CKV_userdat=sysdate';
            $SQL .= ' WHERE CKV_key=:var_N0_ckv_key';
            if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                awisErrorMailLink('CRMAdressen', 1, 'Fehler beim Speichern', $SQL);
            }
        }
    }
}

//*************************************************************************
//*************************************************************************
//*************************************************************************
//** Konditionsmodelle
//*************************************************************************
//*************************************************************************
//*************************************************************************
$Felder = awis_NameInArray($_POST, 'txtCKM_',1,1);
if($Felder!='')
{
	$AWIS_KEY1=$_POST['txtCKM_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CKT','CKM_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';

	if($_POST['txtCKM_KEY']=='0')
	{
		$Fehler = '';
		$SQL = 'INSERT INTO CRMKonditionsModelle';
		$SQL .= '(CKM_BEZEICHNUNG, CKM_BEMERKUNG, CKM_BEZEICHNUNG_OPAL, CKM_BASIS, CKM_STATUS';
		$SQL .= ', CKM_BEREICH, CKM_EINSATZSTUFE, CKM_KONDBLATT';
		$SQL .= ',CKM_USER, CKM_USERDAT';
		$SQL .= ')VALUES (';
		$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtCKM_BEZEICHNUNG'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKM_BEMERKUNG'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKM_BEZEICHNUNG_OPAL'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKM_BASIS'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKM_STATUS'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtCKM_BEREICH'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtCKM_EINSATZSTUFE'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtCKM_KONDBLATT'],false);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200716111140');
			die();
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$BindeVariablen=array();
		$BindeVariablen['var_N0_ckm_key']=$_POST['txtCKM_KEY'];
		$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMKonditionsModelle WHERE CKM_key=:var_N0_ckm_key',true,false,$BindeVariablen);
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCKT['CKM_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE CRMKonditionsModelle SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', CKM_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', CKM_userdat=sysdate';
			$SQL .= ' WHERE CKM_key=0' . $_POST['txtCKM_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}



$Felder = awis_NameInArray($_POST, 'txtCKD_',1,1);
if(!empty($Felder))
{
	$AWIS_KEY2=$_POST['txtCKD_KEY'];
	$AWIS_KEY1=$_POST['txtCKD_CKM_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('CKT','CKD_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';

	if($_POST['txtCKD_KEY']=='0')
	{
		if($_POST['txtCKD_BEZEICHNUNG']!='')
		{
			$Fehler = '';
			$SQL = 'INSERT INTO CRMKonditionsModellDetails';
			$SQL .= '(CKD_BEZEICHNUNG, CKD_BEMERKUNG, CKD_BEREICH, CKD_STATUS';
			$SQL .= ', CKD_TEXT, CKD_CKM_KEY';
			if(isset($_POST['txtCKD_TEXT2']))
			{
				$SQL .= ', CKD_TEXT2';
			}
			$SQL .= ',CKD_USER, CKD_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtCKD_BEZEICHNUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKD_BEMERKUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKD_BEREICH'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKD_STATUS'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKD_TEXT'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCKD_CKM_KEY'],true);
			if(isset($_POST['txtCKD_TEXT2']))
			{
				$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCKD_TEXT2'],true);
			}
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200716111140');
				die();
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsCKT = awisOpenRecordset($con,'SELECT * FROM CRMKonditionsModellDetails WHERE CKD_key=' . $_POST['txtCKD_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsCKT[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsCKT['CKD_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE CRMKonditionsModellDetails SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', CKD_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', CKD_userdat=sysdate';
			$SQL .= ' WHERE CKD_key=0' . $_POST['txtCKD_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}


//**************************************************************
// Potential-Vorgaben
//**************************************************************
$Felder = awis_NameInArray($_POST, 'txtCPV_BETRAG_',1,1);
if($Felder!='')
{
	$SQL = '';
	$Felder = explode(';',$Felder);
	foreach($Felder AS $Feld)
	{
		$FeldInfo = explode("_",$Feld);		// Format: txtCPV_BETRAG_100_100
		$CLBKEY = $FeldInfo[2];
		$CFTKEY = $FeldInfo[3];

		$SQL = 'SELECT * ';
		$SQL .= ' FROM CRMPotentialVorgaben';
		$SQL .= ' WHERE CPV_CPO_KEY =0'.$_POST['txtCPO_KEY'];
		$SQL .= ' AND CPV_CLB_ID=0'.$CLBKEY;
		$SQL .= ' AND CPV_CFT_ID=0'.$CFTKEY;
		$rsCPV = awisOpenRecordset($con,$SQL);
		if($awisRSZeilen==0)
		{
			$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.substr($Feld,3)],true);
			if($WertAlt!=$WertNeu)
			{
				$SQL = 'INSERT INTO CRMPotentialVorgaben';
				$SQL .= '(CPV_CPO_KEY, CPV_CLB_ID, CPV_CFT_ID, CPV_BETRAG';
				$SQL .= ',CPV_USER, CPV_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . awis_FeldInhaltFormat('Z',$_POST['txtCPO_KEY'],true);
				$SQL .= ', ' . awis_FeldInhaltFormat('Z',$CLBKEY,true);
				$SQL .= ', ' . awis_FeldInhaltFormat('Z',$CFTKEY,true);
				$SQL .= ',' . awis_FeldInhaltFormat('N2',$_POST[$Feld],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				if(!awisExecute($con,$SQL))
				{
					awis_Debug(1,$SQL);
					awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200712131110');
					die();
				}
			}
		}
		else
		{
			$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.substr($Feld,3)],true);
			if($WertNeu=='null')
			{
				$SQL = 'DELETE CRMPotentialVorgaben';
				$SQL .= ' WHERE CPV_KEY = 0'.$rsCPV['CPV_KEY'][0];
				$SQL .= '';
				if(!awisExecute($con,$SQL))
				{
					awis_Debug(1,$SQL);
					awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200712131120');
					die();
				}
			}
			elseif($WertAlt!=$WertNeu)
			{
				$SQL = 'UPDATE CRMPotentialVorgaben';
				$SQL .= ' SET CPV_BETRAG='. awis_FeldInhaltFormat('N2',$_POST[$Feld],true);
				$SQL .= ',CPV_USER = \'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',CPV_USERDAT = SYSDATE';
				$SQL .= ' WHERE CPV_KEY = 0'.$rsCPV['CPV_KEY'][0];
				$SQL .= '';
				if(!awisExecute($con,$SQL))
				{
					awis_Debug(1,$SQL);
					awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'200712131120');
					die();
				}
			}
		}
	}
}

//**************************************************************
// Vertragsdaten �bernehmen
// �nderung: Neue Felder und AP automatisch anlegen
//**************************************************************
if(isset($_POST['txtCGK_KEY']))
{
	$AWIS_KEY1 = 0;
	awis_Debug(1,$_POST);

	// Daten auf Vollst�ndigkeit pr�fen
	$Fehler = '';
	$Pflichtfelder = array('CGK_STRASSE','CGK_PLZ','CGK_ORT');
	foreach($Pflichtfelder AS $Pflichtfeld)
	{
		if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
		{
			$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['CGK'][$Pflichtfeld].'<br>';
		}
	}
//awis_Debug(1,$_POST);
	// Eines von beiden muss gef�llt sein
	if($_POST['txtCGK_NACHNAME']=='' AND $_POST['txtCGK_FIRMA1']=='')
	{
		$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['CGK']['CGK_FIRMA1'].'<br>';
	}
		// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		die('<span class=HinweisText>'.$Fehler.'</span>');
	}

	// Betreuername ermitteln und eintragen
	$SQL = 'SELECT CGK_BETREUER';
	$SQL .= ' FROM CRMGewerbekunden';
	$SQL .= ' WHERE CGK_BETREUER_ID = '.awis_FeldInhaltFormat('N0', $_POST['txtBETREUER_ID'],false);
	$rsCAD = awisOpenRecordset($con,$SQL);
	$Text = explode('(',$rsCAD['CGK_BETREUER'][0]);
	$PER_NR = intval($Text[1]);

	$SQL = 'INSERT INTO CRMAdressen';
	$SQL .= '(CAD_NAME1, CAD_NAME2 ';
	$SQL .= ',CAD_STRASSE, CAD_HAUSNUMMER, CAD_STATUS ';
	$SQL .= ',CAD_PLZ, CAD_ORT';
	$SQL .= ',CAD_TELEFON, CAD_CGB_KEY';
	$SQL .= ', CAD_KON_KEY, CAD_RANKING';
	if(isset($_POST['txtinverbinfo']))
	{
		$SQL .= ', CAD_VERBUNDHINWEIS';
	}
    $SQL .= ', CAD_EMAIL, CAD_TELEFAX';
	$SQL .= ',CAD_USER, CAD_USERDAT';
	$SQL .= ')VALUES (';

	if($_POST['txtCGK_FIRMA1']=='')			// Bei Mitarbeitern zu einer Firma gibt es keinen Firmennamen
	{
		$SQL .= ' ' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_NACHNAME']),false);
		$SQL .= ', ' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_VORNAME']),false);
		$Ranking = 12;		// MA
	}
	else
	{
		$SQL .= ' ' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_FIRMA1']),false);
		if ($_POST['txtCGK_FIRMA3']!= '')
		{
			$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_FIRMA2']). ' '.trim($_POST['txtCGK_FIRMA3']),true);
		}
		else
		{
			$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_FIRMA2']),true);
		}
		$Ranking = 'null';
	}
    
    // bei EuroShell-Kunden soll direkt das Ranking EuroShell ausgewaehlt sein
    if($_POST['txtCGK_ACCOUNT_TY'] == 20) // 20 = EuroShell
    {
        $Ranking = 15; // EuroShell
    }

	$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_STRASSE']),true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_HAUSNR']),true);
	$SQL .= ',' . awis_FeldInhaltFormat('T','K',true);
	//$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_RANKING'],true);
	//$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_LAN_CODE'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCGK_PLZ'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',trim($_POST['txtCGK_ORT']),true);
	//$SQL .= ',' . awis_FeldInhaltFormat('T',(isset($_POST['txtCAD_BEMERKUNG'])?$_POST['txtCAD_BEMERKUNG']:''),true);
	//$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtCAD_KON_KEY'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('TZ',$_POST['txtCGK_PHONE_H'],true);
	//$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_WEBSEITE'],true);
	//$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_EMAIL'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('N0',19,true);
	//$SQL .= ',' . awis_FeldInhaltFormat('TZ',$_POST['txtCAD_TELEFAX'],true);
	//$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCAD_ORTSTEIL'],true);

	$SQL .= ', (SELECT KON_KEY FROM KONTAKTE WHERE KON_PER_NR = 0'.$PER_NR.')';
	$SQL .= ', '.$Ranking;

	if(isset($_POST['txtinverbinfo']))
	{
		$SQL .= ', '.awis_FeldInhaltFormat('T',$_POST['txtCGK_CUSTOMERGROUP']);
	}

    $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCGK_EMAIL'],true);
    $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCGK_FAX'],true);
	$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
	$SQL .= ',SYSDATE';
	$SQL .= ')';

	awis_Debug(1,$SQL);

	if(!awisExecute($con,$SQL))
	{
		awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'NEU/1');
		die();
	}

	$SQL = 'SELECT seq_CAD_KEY.CurrVal AS KEY FROM DUAL';
	$rsKey = awisOpenRecordset($con,$SQL);
	$AWIS_KEY1=$rsKey['KEY'][0];
	awis_BenutzerParameterSpeichern($con, "AktuellerCAD" , $AWISBenutzer->BenutzerName() ,$rsKey['KEY'][0]);

	$SQL = 'INSERT  INTO CRMAkquiseStand';
	$SQL .= ' (CAQ_CAD_KEY, CAQ_STATUS, CAQ_VERTRAGSNUMMER, CAQ_USER, CAQ_USERDAT)';
	$SQL .= ' VALUES('.$AWIS_KEY1.', 0, '.awis_FeldInhaltFormat('Z',$_POST['txtCGK_ACCOUNT_ID'],true);
	$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
	$SQL .= ',SYSDATE';
	$SQL .= ')';
	if(awisExecute($con, $SQL)===false)
	{
		awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
		awisLogoff($con);
		die();
	}

    // Infofelder (Vertragsstatus/Vertragssperren/Kartensperren/Poststatus schreiben

    $InfoFelder = array();
    $InfoFelder[4005] = $_POST['txtINFO_Vertragsstatus'];
    $InfoFelder[4006] = $_POST['txtINFO_Kartensperren'];
    $InfoFelder[4007] = $_POST['txtINFO_Poststatus'];
    $InfoFelder[4008] = $_POST['txtINFO_Vertragssperren'];
    
    foreach($InfoFelder as $key => $value)
    {
        $BindeVariablen=array();
		$BindeVariablen['var_N0_cin_cit_id'] = $key;
        $BindeVariablen['var_T_cin_wert'] = $value;
        $BindeVariablen['var_N0_cin_cad_key'] = $AWIS_KEY1;
        $BindeVariablen['var_T_user'] = $AWISBenutzer->BenutzerName();

        $SQL = "INSERT INTO crminfos ";
        $SQL .= "(cin_cad_key,cin_cit_id,cin_wert,cin_user,cin_userdat) values ";
        $SQL .= "(:var_N0_cin_cad_key,:var_N0_cin_cit_id,:var_T_cin_wert,:var_T_user,sysdate)";

        if(awisExecute($con, $SQL, true, false, 0 , $BindeVariablen)===false)
        {
            awisErrorMailLink('CRMAdressenAP',2,'Fehler beim Speichern',$SQL);
            awisLogoff($con);
            die();
        }
    }
    
	// Kunde auch als Ansprechpartner
	if(isset($_POST['txtalsap']))
	{
		if($_POST['txtCGK_NACHNAME']!='' AND $_POST['txtCGK_VORNAME']!='')
		{
			$SQL = 'INSERT INTO CRMANSPRECHPARTNER';
			$SQL .= '(CAP_CAD_KEY,CAP_VORNAME,CAP_NACHNAME,CAP_GEBURTSTAG';
			$SQL .= ',CAP_USER,CAP_USERDAT)';
			$SQL .= ' VALUES (';
			$SQL .= ' '.$AWIS_KEY1;
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCGK_VORNAME'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCGK_NACHNAME'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtCGK_BIRTH_DT'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMAdressenAP',2,'Fehler beim Speichern',$SQL);
				awisLogoff($con);
				die();
			}

		}
	}
}

//********************************************************************
// Export der Reifenkonditionen durchfuehren
//********************************************************************
if (isset($_POST['txtAktion'])) {

    if ($_POST['txtAktion'] == 1) {
        // Export der Reifendaten
        $SQL = "SELECT CKD_KEY, CKM_BEZEICHNUNG, CKD_BEZEICHNUNG, CKD_BEMERKUNG, CKD_TEXT, CKD_TEXT2 ";
        $SQL .= "FROM CRMKONDITIONSMODELLE INNER JOIN CRMKONDITIONSMODELLDETAILS ";
        $SQL .= "ON CKM_KEY = CKD_CKM_KEY ";
        $SQL .= "WHERE CKM_BEREICH = 3 ";
        $SQL .= "ORDER BY CKM_BEZEICHNUNG, CKD_BEZEICHNUNG, CKD_BEMERKUNG";

        $rsCKD = awisOpenRecordset($con,$SQL);
        $rsCKDZeilen = $awisRSZeilen;

        $Pfad = '/daten/web' . $_POST['txtPFAD'];

        // alte Daten loeschen
        array_map('unlink', glob($Pfad . '*_export_crm_reifenkond.csv'));

        $Dateiname = $_POST['txtDATEINAME'];

        $fpExport = fopen($Pfad . $Dateiname,'w');

        $Ueberschrift = 'CKD_KEY;CKM_BEZEICHNUNG;CKD_BEZEICHNUNG;CKD_BEMERKUNG;CKD_TEXT;CKD_TEXT2';
        fputs($fpExport,$Ueberschrift . chr(13) . chr(10));

        for($CKDZeile=0;$CKDZeile<$rsCKDZeilen;$CKDZeile++)
        {
            $Zeile = '';
            $Zeile .= $rsCKD['CKD_KEY'][$CKDZeile] . ';';
            $Zeile .= $rsCKD['CKM_BEZEICHNUNG'][$CKDZeile] . ';';
            $Zeile .= $rsCKD['CKD_BEZEICHNUNG'][$CKDZeile] . ';';
            $Zeile .= $rsCKD['CKD_BEMERKUNG'][$CKDZeile] . ';';
            $Zeile .= $rsCKD['CKD_TEXT'][$CKDZeile] . ';';
            $Zeile .= $rsCKD['CKD_TEXT2'][$CKDZeile];

            fputs($fpExport,$Zeile . chr(13) . chr(10));
        }

        fclose($fpExport);
    }

}

//********************************************************************
// B2B KV-Regeln
//********************************************************************
$Felder = awis_NameInArray($_POST, 'txtCBR_',1,1);
if(!empty($Felder))
{
    $AWIS_KEY1 = $_POST['txtCAD_KEY'];
    $Felder = explode(';', $Felder);

    if($_POST['CBR_KEY'] == '0')
    {
        $SQL = 'INSERT INTO CRMB2BKVREGELN';
        $SQL .= ' (';
        $SQL .= '   CBR_CAD_KEY,';
        $SQL .= '   CBR_REGELAKTIV,';
        $SQL .= '   CBR_BETRAG,';
        $SQL .= '   CBR_KMSTAND,';
        $SQL .= '   CBR_FAHRZEUGALTER,';
        $SQL .= '   CBR_MAILEMPFAENGER,';
        $SQL .= '   CBR_USER,';
        $SQL .= '   CBR_USERDAT,';
        $SQL .= '   CBR_AUFTRAGBEZOGEN';
        $SQL .= ' )';
        $SQL .= ' VALUES';
        $SQL .= ' (';
        $SQL .= ' '.$AWIS_KEY1;
        $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCBR_REGELAKTIV'],true);
        $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCBR_BETRAG'],true);
        $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCBR_KMSTAND'],true);
        $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCBR_FAHRZEUGALTER'],true);
        $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCBR_MAILEMPFAENGER'],true);
        $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
        $SQL .= ',SYSDATE';
        $SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtCBR_AUFTRAGBEZOGEN'],true);
        $SQL .= ' )';

        if(!awisExecute($con,$SQL))
        {
            awisErrorMailLink('crmadressen_speichern.php',1,$awisDBError['message'],'201802161606');
            die();
        }
    }
    else
    {
        $SQL = 'UPDATE CRMB2BKVREGELN SET';
        $SQL .= ' CBR_REGELAKTIV = ' . awis_FeldInhaltFormat('T',$_POST['txtCBR_REGELAKTIV'],true) . ',';
        $SQL .= ' CBR_BETRAG = ' . awis_FeldInhaltFormat('T',$_POST['txtCBR_BETRAG'],true) . ',';
        $SQL .= ' CBR_KMSTAND = ' . awis_FeldInhaltFormat('T',$_POST['txtCBR_KMSTAND'],true) . ',';
        $SQL .= ' CBR_FAHRZEUGALTER = ' . awis_FeldInhaltFormat('T',$_POST['txtCBR_FAHRZEUGALTER'],true) . ',';
        $SQL .= ' CBR_MAILEMPFAENGER = ' . awis_FeldInhaltFormat('T',$_POST['txtCBR_MAILEMPFAENGER'],true) . ',';
        $SQL .= ' CBR_AUFTRAGBEZOGEN = ' . awis_FeldInhaltFormat('T',$_POST['txtCBR_AUFTRAGBEZOGEN'],true) . ',';
        $SQL .= ' CBR_USER = \'' . $AWISBenutzer->BenutzerName() . '\',';
        $SQL .= ' CBR_USERDAT = sysdate';
        $SQL .= ' WHERE CBR_KEY =0' . $_POST['CBR_KEY'];

        if(awisExecute($con, $SQL)===false)
        {
            awisErrorMailLink('CRMAdressen',1,'Fehler beim Speichern',$SQL);
        }

    }
}


//awis_Debug(1,$_POST);
?>