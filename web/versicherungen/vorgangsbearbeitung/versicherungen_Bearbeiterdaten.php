<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('V_VERS_FORM_BEARBDATEN', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    $SQL = 'SELECT filnr,wanr,status,datum,zeit,verkaeuferart,name ';
    $SQL .= 'FROM v_vers_form_bearbdaten v, versvorgangsstatus vv ';
    $SQL .= 'WHERE v.filnr = vv.vvs_filid AND ';
    $SQL .= 'v.wanr = vv.vvs_wanr AND ';
    $SQL .= 'vv.vvs_key = ' . $Param;

    $rsBearbDat = $DB->RecordSetOeffnen($SQL);

    $Form->SchreibeHTMLCode('<form name=frmVersicherungenBearbeitungsdaten action=./versicherungen_Details.php?cmdAktion=Details&Seite=Positionsdaten' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

    $EditRecht = $Recht9500 & 2;

    $Form->ZeileStart();

    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_BEARBDATEN']['FILNR'], 120, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_BEARBDATEN']['WANR'], 250, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_BEARBDATEN']['STATUS'], 80, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_BEARBDATEN']['DATUM'], 200, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_BEARBDATEN']['ZEIT'], 120, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_BEARBDATEN']['VERKAEUFERART'], 120, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_BEARBDATEN']['NAME'], 80, '', '');

    $Form->ZeileEnde();

    $PosZeile = 0;

    while(!$rsBearbDat->EOF())
    {
        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('FILNR', $rsBearbDat->FeldInhalt('FILNR'), 0, 120, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('WANR', $rsBearbDat->FeldInhalt('WANR'), 0, 250, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('STATUS', $rsBearbDat->FeldInhalt('STATUS'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('DATUM', $rsBearbDat->FeldInhalt('DATUM'), 0, 200, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('ZEIT', $rsBearbDat->FeldInhalt('ZEIT'), 0, 120, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('VERKAEUFERART', $rsBearbDat->FeldInhalt('VERKAEUFERART'), 0, 120, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('NAME', $rsBearbDat->FeldInhalt('NAME'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->ZeileEnde();

        $rsBearbDat->DSWeiter();
        $PosZeile++;
    }
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241147");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
