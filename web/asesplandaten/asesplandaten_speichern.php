<?php
require_once 'awisTeamplanAPI.php';
require_once 'awisPlanningReceiverAPI.php';
global $APD;
global $AWIS_KEY1;
try
{
    $MeldungHinweis = '';
    $PersNrn = array();

    $DatumVon = $_POST['txtAPD_DATUM_VON'];
    $DatumBis = $_POST['txtAPD_DATUM_BIS'];
    $ClientNo = $_POST['txtAPD_CLIENT_NO'];

    //Personalnummern zu Array
    if(isset($_POST['txtAPD_PERS_NR']) and $_POST['txtAPD_PERS_NR'] != ''){
        $PersNrn = explode(',',trim($_POST['txtAPD_PERS_NR']));
    }

    $TeamplanAPI = new awisTeamplanAPI();

    //F�r die Filialnummern das zugeh�rige Personal ermitteln
    if(isset($_POST['txtAPD_FIL_ID'])){
        foreach ($_POST['txtAPD_FIL_ID'] as $FilId){
            $TeamplanErg = $TeamplanAPI->AnwesenheitenEinerFiliale($FilId, $DatumVon, $DatumVon);

            if($TeamplanErg){
                foreach ($TeamplanErg as $PersNr => $Tage){
                    $PersNrn[] = $PersNr;
                }
            }else{
                $MeldungHinweis .= sprintf('F�r die Filiale %u konnte kein Personal ermittelt werden.<br>',$FilId);
            }
        }
    }

    $Erg = false;
    if(count($PersNrn) > 0){
        $awisPresenceAPI = new awisPlanningReceiverAPI();
        $Erg = $awisPresenceAPI->SyncRequest($PersNrn,$ClientNo,$DatumVon,$DatumBis);
    }

    if($Erg){
        $APD->Form->ZeileStart();
        $APD->Form->Hinweistext(sprintf($APD->AWISSprachKonserven['APD']['APD_REQUEST_OK'],count($PersNrn)),awisFormular::HINWEISTEXT_OK);
        $APD->Form->ZeileEnde();
    }
    if ($MeldungHinweis) {
        $APD->Form->ZeileStart();
        $APD->Form->Hinweistext($MeldungHinweis,awisFormular::HINWEISTEXT_WARNUNG);
        $APD->Form->ZeileEnde();
    }


}  catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $APD->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>