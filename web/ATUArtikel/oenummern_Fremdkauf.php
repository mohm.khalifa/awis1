<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $con;
	global $AWISBenutzer;

	$Rechte_OEN = awisBenutzerRecht($con,400);

		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $AWISBenutzer->BenutzerName()));
	$SQL = 'SELECT * FROM Fremdkaeufe WHERE ';
	$SQL .= " FRK_OEN_SUCHNUMMER " . awisLIKEoderIST($Artikel[1],0,0,1,0) . "";
	$SQL .= " ORDER BY FRK_DATUM DESC";
	$rsOEP = awisOpenRecordset($con, $SQL);
	if($rsOEP==FALSE)
	{
		awisErrorMailLink("oenummern_Fremdkauf.php", 2, $awisDBFehler['message']);
	}
	$rsOEPZeilen=$awisRSZeilen;

	$AnzFK=0;
	$AnzFKGesamt=0;
	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		$Dat = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+1);
		if($Dat > time())
		{
			$AnzFK+=$rsOEP['FRK_MENGE'][$OEPZeile];
		}
		$AnzFKGesamt+=$rsOEP['FRK_MENGE'][$OEPZeile];
	}
	echo 'Fremdkšufe in den letzten 12 Monaten: ' . $AnzFK . '. Gesamtmenge: ' . $AnzFKGesamt;

	echo "<table id=DatenTabelle width=100% border=1>";
	echo "<td id=FeldBez>Filiale</td>";
	echo "<td id=FeldBez>Datum</td>";
	echo "<td id=FeldBez>Lfd.Nr</td>";
	echo "<td id=FeldBez>WstNr</td>";
	echo "<td id=FeldBez>KBA-Nr</td>";
	echo "<td id=FeldBez>Menge</td>";
	echo "<td id=FeldBez>Grund</td>";
	echo "<td id=FeldBez>OE-Nr</td>";
	echo "<td id=FeldBez>Artikel</td>";
	echo "<td id=FeldBez>Preis</td>";
	echo "<td id=FeldBez>Rabatt</td>";
	echo "</tr>";

	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		echo '<tr>';
		echo '<td><a href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' .$rsOEP['FRK_FIL_ID'][$OEPZeile].'>'	. $rsOEP['FRK_FIL_ID'][$OEPZeile] . '</a></td>';
		echo '<td>'	. $rsOEP['FRK_DATUM'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_LFDNR'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_WSTNR'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_KBA'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_MENGE'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_GRUND'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_OEN_NUMMER'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_ARTBEZ'][$OEPZeile] . '</td>';
		echo '<td align=right>'	. awis_format($rsOEP['FRK_PREIS'][$OEPZeile],'Currency') . '</td>';
		echo '<td align=right>'	. $rsOEP['FRK_RABATT'][$OEPZeile] . '%</td>';
		echo '</tr>';
	}

	echo "</table>";
	unset($rsOEP);

?>