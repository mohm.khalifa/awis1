<?php

global $con;

$RechteStufe = awisBenutzerRecht($con, 21);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Rechtebersicht: Programmpunkte',$AWISBenutzer->BenutzerName(),'','','');
    awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}
	print "<table width=100%";

	print "<tr><td width=100>Programmpunkt</td><td>";

		$SQL = "SELECT * FROM Rechte ORDER BY XRC_Recht";
		
		$rsBenutzer = awisOpenRecordset($con, $SQL);
		$rsBenutzerAnz = $awisRSZeilen;
		
		print "<select name=txtRechteID onchange='document.frmRechte.submit();'>";
		if(!isset($_POST["txtRechteID"]))
		{
			print "<option selected value=0>Bitte w�hlen...</option>";
		}
		for($i=0;$i<$rsBenutzerAnz;$i++)
		{
			print "<option value=" . $rsBenutzer["XRC_ID"][$i];
			if(isset($_POST["txtRechteID"]) AND $rsBenutzer["XRC_ID"][$i] == $_POST["txtRechteID"])
			{
				print " selected ";
			}
			print ">" . $rsBenutzer["XRC_RECHT"][$i] . "</option>";
		}

		print "</select>";
        print "</td></tr>";
    if(isset($_POST["txtRechteID"]))
    {
        print "<tr><td width=100>Rechtestufe</td><td>";
        $SQL = "SELECT * FROM Rechtestufen where XRS_XRC_ID =0" . $_POST["txtRechteID"];
        $SQL .= " ORDER BY XRS_BIT";

        $rsBenutzer = awisOpenRecordset($con, $SQL);
        $rsBenutzerAnz = $awisRSZeilen;

        print "<select name=txtXRS_BIT onchange='document.frmRechte.submit();'>";

            print "<option  value=-1>Bitte w�hlen...</option>";

        for($i=0;$i<$rsBenutzerAnz;$i++)
        {
            print "<option value=" . $rsBenutzer["XRS_BIT"][$i];
            if(isset($_POST["txtXRS_BIT"]) AND $rsBenutzer["XRS_BIT"][$i] == $_POST["txtXRS_BIT"])
            {
                print " selected ";
            }
            print ">" . $rsBenutzer["XRS_BIT"][$i] .' - '. $rsBenutzer["XRS_BESCHREIBUNG"][$i] . "</option>";
        }

        print "</select>";
        print "</td></tr>";
    }


	print "</table>";

	if(isset($_POST["txtRechteID"]))
	{
		$RechteStufeBenutzerVerwaltung = awisBenutzerRecht($con, 1);

		print "<hr><table border=0 width=100%><tr><td valign=top>";		// F�r 2-spaltige Ausgabe

		print "<h3>Benutzer<h3>";
		$SQL = "SELECT * FROM V_ACCOUNTRECHTE WHERE XRC_ID=" . $_POST["txtRechteID"] ;
		if(isset($_POST["txtXRS_BIT"]) and $_POST["txtXRS_BIT"] >= 0){
		    $Wert = pow(2,$_POST["txtXRS_BIT"]);
		    $SQL .= ' AND BITAND(XBA_STUFE,'.$Wert.') =' . $Wert;
        }
        $SQL .=  " ORDER BY XBL_LOGIN";
        
		$rsBenutzerRechte = awisOpenRecordset($con, $SQL);
		print "<table border=1 width=390>";
		print "<tr><td id=FeldBez>Benutzer</td><td id=FeldBez>Rechte-ID</td><td id=FeldBez>Rechte-Stufe</td></tr>";
		for($i=0;$i<$awisRSZeilen;$i++)
		{
			print "<tr>";
			if($RechteStufeBenutzerVerwaltung>0)
			{
				print "<td width=200><a href=./rechte_Main.php?cmdAktion=Benutzerrechte&txtBenutzerID=" . $rsBenutzerRechte["XBN_KEY"][$i] . ">" . $rsBenutzerRechte["XBL_LOGIN"][$i] . "</a></td>";
			}
			else
			{
				print "<td width=200>" . $rsBenutzerRechte["XBL_LOGIN"][$i] . "</td>";
			}
			
			print "<td width=80>" . $rsBenutzerRechte["XRC_ID"][$i] . "</td>";
			print "<td width=100>" . $rsBenutzerRechte["XBA_STUFE"][$i] . "</td>";
			print "</tr>";
		
		}
		print "</table>";

		print "</td><td  valign=top>";		// 2. Spalte

		print "<h3>Gruppen<h3>";
		$SQL = "select  DISTINCT XBB_KEY, XBB_BEZ, XBA_XRC_ID, XBA_STUFE from BenutzerACLS, BenutzerGruppenBez ";
		$SQL .= "WHERE XBA_XXX_KEY = XBB_KEY AND XBA_XRC_ID=0" . $_POST["txtRechteID"];

        if(isset($_POST["txtXRS_BIT"]) and $_POST["txtXRS_BIT"] >= 0){
            $Wert = pow(2,$_POST["txtXRS_BIT"]);
            $SQL .= ' AND BITAND(XBA_STUFE,'.$Wert.') =' . $Wert;
        }

        $SQL .=  " ORDER BY XBB_BEZ";

		$rsBenutzerRechte = awisOpenRecordset($con, $SQL);
		print "<table border=1 width=390>";
		print "<tr><td id=FeldBez>Gruppe</td><td id=FeldBez>Rechte-ID</td><td id=FeldBez>Rechte-Stufe</td></tr>";
		for($i=0;$i<$awisRSZeilen;$i++)
		{
			print "<tr>";
			if($RechteStufeBenutzerVerwaltung>0)
			{
				
				print "<td width=200><a target='_blank' href=/admin/benutzerundgruppen/gruppenverwaltung_Main.php?cmdAktion=Details&XBB_KEY=" . $rsBenutzerRechte["XBB_KEY"][$i] . ">" . $rsBenutzerRechte["XBB_BEZ"][$i] . "</a></td>";
			}
			else
			{
				print "<td width=200>" . $rsBenutzerRechte["XBB_BEZ"][$i] . "</td>";
			}
			print "<td width=80>" . $rsBenutzerRechte["XBA_XRC_ID"][$i] . "</td>";
			print "<td width=100>" . $rsBenutzerRechte["XBA_STUFE"][$i] . "</td>";
			print "</tr>";
		
		}
		print "</table>";

		print "</td></tr></table>";
	}

?>