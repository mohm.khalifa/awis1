<?php

require_once("register.inc.php");			// AWIS Benutzerparameter u.a.
require_once("db.inc.php");					// DB AWIS


//Variablen f�r Tabellennamen DB2

$db2Aachen=1;	//Wenn 1, dann Aachen, ansonsten (0) kklx05su90

if($db2Aachen)
{
	$tabAdressen = "CLS.ADRESSEN";
	$tabKunden = "CLS.KUNDEN";
	$tabLand = "CLS.LAND";
	$tabDTA_Positionen ="CLS.DTA_POSITIONEN";
	$tabKalender = "CLS.KALENDER";
}
else
{
	$tabAdressen = "ADRESSEN";
	$tabKunden = "KUNDEN";
	$tabLand = "LAND";
	$tabDTA_Positionen ="DTA_POSITIONEN";
	$tabKalender = "KALENDER";
}

//Verbindungsaufbau zu Datenbanken

//Oracle

function oraverbindung()
{
	if($_ENV["HOSTNAME"]=='atlx22su91') //Testserver
		$hdlVerbindung=@ocilogon("card","gis","AWIS03");
	else
		$hdlVerbindung=@ocilogon("card","gis","AWISRAC.ATU.DE");	//Produktivserver
	return $hdlVerbindung;
}

//DB2

function db2verbindung()
{
	global $db2Aachen;	//Wenn 1, dann Aachen, ansonsten (0) kklx05su90
	
	/*if($db2Aachen)
	{
		$hdlVerbindung=odbc_connect("CLS_340","atu","queen");	//Aachen
	}
	else
	{*/
		$hdlVerbindung=@odbc_connect("ATUCLS1","db2inst1","db2");	//kklx05su90
	//}

	if(!$hdlVerbindung)
		die("Keine Verbindung zur DB2-Datenbank ".$strInstanz."<br>".$php_errormsg);		//Schalter track_errors muss in php.ini eingeschaltet sein
		//fehler("Neuer","Neuer1");
	
	return $hdlVerbindung;
}


//Alte Version
/*function db2verbindung($strInstanz,$strBenutzer,$strPasswort)
{
	$hdlVerbindung=@odbc_connect($strInstanz,$strBenutzer,$strPasswort);
	if(!$hdlVerbindung)
		die("Keine Verbindung zur DB2-Datenbank ".$strInstanz."<br>".$php_errormsg);		//Schalter track_errors muss in php.ini eingeschaltet sein
		//fehler("Neuer","Neuer1");
	
	return $hdlVerbindung;
}

function oraverbindung($strBenutzer,$strPasswort,$strInstanz)
{
	$hdlVerbindung=@ocilogon($strBenutzer,$strPasswort,$strInstanz);
	return $hdlVerbindung;
}*/


//Leitet je nach Datenbankart die Abfrage an eine entsprechende Unterabfrage weiter
//wird f�r $intKeinCommit 1 �bergeben, so wird auch kein Commit ausgef�hrt

function abfrage($strDatenbank,$strSqlstatement,$intKeinCommit=0)
{
	global $hdlOraVerbindung;
	global $hdlDb2Verbindung;
	
	global $dblDatenbankzeit;
	
	if(isset($dblDatenbankzeit))
		$dblStartzeit = mikrozeit();
		
	$arrErgebnis;		//erh�lt assoziatives Array in Form $arr["zeilen"],["trefferarray"],["spaltennamen"],["abfragenart"] zugewiesen

	switch($strDatenbank)
	{
		case "db2":
			if(!isset($hdlDb2Verbindung))
				$hdlDb2Verbindung=db2verbindung();
			$arrErgebnis=db2abfrage($hdlDb2Verbindung,$strSqlstatement,$intKeinCommit);
			break;
		case "ora":
			if(!isset($hdlOraVerbindung))
				$hdlOraVerbindung=oraverbindung();
			$arrErgebnis=oraabfrage($hdlOraVerbindung,$strSqlstatement,$intKeinCommit);
			break;
		default:
			fehler("Datenbankart ".$strDatenbank." ist nicht vorhanden!");
	}
	
	if(isset($dblDatenbankzeit))
		$dblDatenbankzeit+=(mikrozeit()-$dblStartzeit);
	
	return $arrErgebnis;
}

// F�hrt ein �bergebenes SQL-Statement f�r best. Oracle DB aus und liefert zur�ck:
// Array in Form: $arr["zeilen"],["trefferarray"],["spaltennamen"],["abfragenart"] (trefferarray + spaltennamen nur bei SELECT)

function oraabfrage($hdlOraVerbindung,$strSql,$intKeinCommit)
{
	$varRueckgabe=array();	//["zeilen"],["trefferarray"],["spaltennamen"],["abfragenart"]
	$intZeilen=0;			//Anzahl betroffener Zeilen der Abfrage
	
	//ermittelt Abfragenart: SELECT, INSERT, UPDATE oder DELETE
	$strAbfragenart=trim(strtoupper(substr(trim($strSql),0,strcspn(trim($strSql),chr(9).chr(10).chr(12).chr(13).chr(32)))));
	
	//Sql-Anweisung parsen, im Fehlerfall (tritt offensichtlich nie ein) Aufruf der Funktion fehler()
	$hdlOraAnweisung=@ociparse($hdlOraVerbindung,$strSql) or fehler("ORA","Parsefehler: ".$strSql,$strSql);
	
	//geparste Sql-Anweisung ausf�hren, im Fehlerfall Aufruf der Funktion orafehler()
	@ociexecute($hdlOraAnweisung,OCI_DEFAULT) or orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strSql);		//OCI_DEFAULT => ocicommit muss ausgef�hrt werden
	
	switch($strAbfragenart)
	{
		case "SELECT":
			$intZeilen=@ocifetchstatement($hdlOraAnweisung,$arrSelect);
			
			if($intZeilen)
			{
				$varRueckgabe["trefferarray"]=$arrSelect;
			}
			
			$intSpalten=@ocinumcols($hdlOraAnweisung) or orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strSql);	//Spaltenanzahl ermitteln
			
			$varRueckgabe["spaltennamen"]=array();
			
			for($i=1;$i<=$intSpalten;$i++)
			{
				$varRueckgabe["spaltennamen"][$i-1]=@ocicolumnname($hdlOraAnweisung,$i) or orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strSql); //Spaltennamen ermitteln
			}
			
		default:
			$intZeilen=@ocirowcount($hdlOraAnweisung);	//R�ckgabe der Anzahl betroffener Zeilen bei INSERT, UPDATE, DELETE
			
			if(!$intKeinCommit)
			{
				@ocicommit($hdlOraVerbindung) or orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strSql);
			}
	}
		
	@ocifreestatement($hdlOraAnweisung);		//Ressourcen freigeben
		
	$varRueckgabe["zeilen"]=$intZeilen;
	$varRueckgabe["abfragenart"]=$strAbfragenart;
	
	return $varRueckgabe;
}


// F�hrt ein �bergebenes SQL-Statement f�r best. DB2 DB aus und liefert zur�ck:
// Array in Form: $arr["zeilen"],["trefferarray"],["spaltennamen"],["abfragenart"] (trefferarray + spaltennamen nur bei SELECT)


function db2abfrage($hdlDb2Verbindung,$strSql,$intKeinCommit)
{
	$varRueckgabe=array();	//["zeilen"],["trefferarray"],["spaltennamen"],["abfragenart"]
	$intZeilen=0;			//Anzahl betroffener Zeilen der Abfrage
	
	@odbc_autocommit($hdlDb2Verbindung,FALSE) or fehler("DB2",odbc_errormsg(),$strSql);		//Autocommit abschalten
		
	//ermittelt Abfragenart: SELECT, INSERT, UPDATE oder DELETE
	$strAbfragenart=strtoupper(substr(trim($strSql),0,strpos(trim($strSql)," ")));
	
	$hdlDb2Anweisung = @odbc_exec($hdlDb2Verbindung,$strSql) or fehler("DB2",odbc_errormsg(),$strSql);	//entspricht oci_parse + oci_execute
		
	if($strAbfragenart=='SELECT')
	{
		$arrSelect = array();
		$intZeilen = 0;
		
		while($arrSpalten=@odbc_fetch_array($hdlDb2Anweisung))		//Aufbau eines 2-dimensionalen Arrays mit den Datensatztreffern
		{														//in der Form $arr["Feldname"][Zeilennr] = Wert der Zeilennr
			foreach($arrSpalten as $index => $wert)
			{
				if(!is_array($arrSelect[$index]))
				{
					$arrSelect[$index]=array();
				}
			
				$arrSelect[$index][$intZeilen]=$wert;
			}
			$intZeilen++;
		}
		$varRueckgabe["trefferarray"]=$arrSelect;									//R�ckgabe eines Arrays
		
		$intSpalten=@odbc_num_fields($hdlDb2Anweisung) or fehler("DB2",odbc_errormsg(),$strSql);	//Spaltenanzahl ermitteln
			
		$varRueckgabe["spaltennamen"]=array();
		
		for($i=1;$i<=$intSpalten;$i++)
		{
			$varRueckgabe["spaltennamen"][$i-1]=@odbc_field_name($hdlDb2Anweisung,$i) or fehler("DB2",odbc_errormsg(),$strSql); //Spaltennamen ermitteln
		}		
	}
	else
	{
		$intZeilen = @odbc_num_rows($hdlDb2Anweisung);		//R�ckgabe der Anzahl betroffener Zeilen
		
		if(!$intKeinCommit)
		{
			@odbc_commit($hdlDb2Verbindung) or fehler("DB2",odbc_errormsg(),$strSql);		//commit ausf�hren
		}
	}
	
	$varRueckgabe["zeilen"]=$intZeilen;
	$varRueckgabe["abfragenart"]=$strAbfragenart;
	
	return $varRueckgabe;
}

//Als erster Parameter muss zwingend ein Prozeduraufruf (inkl. Bindevariablen z.B. :rueckgabe1,:rueckgabe2 usw.) �bergeben werden
//Zweiter Parameter: assoziatives Array mit Name der Bindvariable (index) und Gr��e des zu reservierenden Speicherplatzes (wert)
//z.B $arrParameter=array("rueckgabe1"=>100,"rueckgabe2"=>50);


function oracleprozedur($strProz,$arrParameter)
{
	global $hdlOraVerbindung;
	
	//Sql-Anweisung parsen, im Fehlerfall (tritt offensichtlich nie ein) Aufruf der Funktion fehler()
	$hdlOraAnweisung=@ociparse($hdlOraVerbindung,"begin ".$strProz.";end;") or fehler("ORA","Parsefehler: ".$strProz,$strProz);
	
	
	//Bind-Variablen generieren
	foreach($arrParameter as $index => $wert)
	{
		@ocibindbyname($hdlOraAnweisung,":".$index,${$index},$wert) 
		or orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strProz.": Bindfehler mit Variable $".$index." (L�nge: ".$wert.")");
	}
	
	//geparste Sql-Anweisung ausf�hren, im Fehlerfall Aufruf der Funktion orafehler()
	@ociexecute($hdlOraAnweisung) or orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strProz);		//ocicommit innerhalb Prozedur
	
	@ocifreestatement($hdlOraAnweisung);		//Ressourcen freigeben
	
	$arrRueck=array();
	
	//Werte der Bind-Variablen Array $arrRueck zuweisen
	
	foreach($arrParameter as $index => $wert)
	{
		$arrRueck[$index]=${$index};
	}
	return $arrRueck;	
}












//Liest Oracle-Fehler aus und leitet Fehlertext an die Funktion fehler() weiter

function orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strSql='')
{
	@ocirollback($hdlOraVerbindung);
	$arrFehler = @ocierror($hdlOraAnweisung);
	@ocifreestatement($hdlOraAnweisung);
	fehler("ORA",$arrFehler["message"],$strSql);
}


//**********************************************************************************************************************
//		Verschiedene Funktionen f�r Datenbank
//**********************************************************************************************************************


//Daten einer Select-Abfrage als HTML-Tabelle ausgeben


function selecttabelle($strDatenbank,$hdlVerbindung,$strSqlstatement)
{
	$strAbfragenart=strtoupper(substr(trim($strSqlstatement),0,strpos(trim($strSqlstatement)," ")));
	
	$strAbfragenart=='SELECT' or fehler("DB","F�r die Funktion 'selecttabelle()' ist eine SELECT-Abfrage erforderlich!",$strSqlstatement);
	
	$arrRueckgabe=abfrage($strDatenbank,$hdlVerbindung,$strSqlstatement);

	echo "<table border=\"1\" align=\"center\"><tr>";
	
	for($k=0;$k<count($arrRueckgabe["spaltennamen"]);$k++)
	{
		echo "<th>".$arrRueckgabe["spaltennamen"][$k]."</th>";
	}
	
	echo "</tr>";
	
	for($i=0;$i<$arrRueckgabe["zeilen"];$i++)
	{
		echo "<tr>";
		for($k=0;$k<count($arrRueckgabe["spaltennamen"]);$k++)
		{
			echo "<td>";
			if($arrRueckgabe["trefferarray"][$arrRueckgabe["spaltennamen"][$k]][$i]!='')
				echo $arrRueckgabe["trefferarray"][$arrRueckgabe["spaltennamen"][$k]][$i];
			else
				echo "&nbsp;";
			echo "</td>";
		}
		echo "</tr>";
	}

	echo "</table>";
}


//Liefert den Wert eines Feldes nach einer Abfrage, �bergeben wird R�ckgabearray, Feldname
//optional: Zeilennr (Default: 0), intHtmlTab (ob R�ckgabewert f�r HTML-Tabelle bestimmt ist) Default: 0


function feldwert($arrRueck,$strFeldname,$intZeile=0,$intHtmlTab=0)
{
	$varWert=$arrRueck["trefferarray"][$strFeldname][$intZeile];
	
	if($intHtmlTab&&empty($varWert))
	{
		return "&nbsp;";
	}
	else
	{
		return $varWert;
	}
}

//Ermittelt die Anzahl Datens�tze, welche ein bestimmtes sql-Statement liefert

function anzahlds($strDB,$strSql)
{
	$strSqlAnzahl = "select count(*) anzahl ".strstr($strSql,"from");
	
	if(strpos($strSqlAnzahl,"order by"))
		$strSqlAnzahl = substr($strSqlAnzahl,0,strpos($strSqlAnzahl,"order by"));
	
	$arrAnzahl = abfrage($strDB,$strSqlAnzahl);

	if($arrAnzahl["zeilen"])
		$intAnzDs = $arrAnzahl["trefferarray"]["ANZAHL"][0];
	else
		$intAnzDs = 0;
	
	return $intAnzDs;
}

//Ermittelt den AWIS-BenutzerParameter bei �bergabe der $strParameterBez (z.B. 'BildschirmBreite')

function benutzerparameter($strParameterBez,$strBenutzer='PHP_AUTH_USER')
{
	global $hdlOraVerbindung;
	
	if(!isset($hdlOraVerbindung))
		$hdlOraVerbindung=oraverbindung();
	
	//$hdlOraVerbindung=@ocilogon("card","gis","AWIS26");
	
	if($strBenutzer=='PHP_AUTH_USER')
	{
		global $PHP_AUTH_USER;
		$strBenutzer=$PHP_AUTH_USER;
	}
	
	return awis_BenutzerParameter($hdlOraVerbindung,$strParameterBez,$strBenutzer);
}



?>

