<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RAV','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht631 = $AWISBenutzer->HatDasRecht(631);
	if($Recht631==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GSL'));
	if(empty($Param))
	{
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']=' GSL_SUCHNAME ASC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['GSL_LIEFERANTENART'] = $_POST['sucGSL_LIEFERANTENART'];
		$Param['GSL_BRANCHE'] = $_POST['sucGSL_BRANCHE'];
		$Param['GSL_AKTIV'] = $_POST['sucGSL_AKTIV'];
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']=' GSL_SUCHNAME ASC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_GSL",serialize($Param));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./lieferantenadr_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['GSL_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['GSL_KEY']);
	}
	elseif($AWIS_KEY1==0) 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='GSL_SUCHNAME ASC';
			$AWISBenutzer->ParameterSchreiben('Formular_GSL',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//Speichernbutton oder Klick aufs F�hnchen
	if(isset($_GET['Aktiv']))
	{
		include('./lieferantenadr_speichern.php');
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY GSL_SUCHNAME ASC';
			$Param['ORDER']='GSL_SUCHNAME ASC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT GSL_KEY,GSL_SUCHNAME,GSL_FIRMA,GSB_BRANCHE,GSL_LIEFERANTENART,LIEFERANTENART,GSL_AKTIV,GSL_USER,GSL_USERDAT,GSL_ANREDE,GSL_TITEL,GSL_VORNAME,GSL_ANSPRECHPARTNER,GSL_STRASSE,GSL_PLZ,GSL_ORT,GSL_BRANCHENSCHL,GSL_TELNR_FIRMA,GSL_TELNR_PRIVAT,GSL_MOBIL,GSL_FAXNR,GSL_MAIL';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr FROM(';
	$SQL .= 'SELECT GSL_KEY,GSL_SUCHNAME,GSL_FIRMA,GSB_BRANCHE,GSL_LIEFERANTENART,func_textkonserve(\'lst_LIEFERANTENART\',\'RAV\',\'DE\',GSL_LIEFERANTENART) as LIEFERANTENART,GSL_AKTIV,GSL_USER,GSL_USERDAT,GSL_ANREDE,GSL_TITEL,GSL_VORNAME,GSL_ANSPRECHPARTNER,GSL_STRASSE,GSL_PLZ,GSL_ORT,GSL_BRANCHENSCHL,GSL_TELNR_FIRMA,GSL_TELNR_PRIVAT,GSL_MOBIL,GSL_FAXNR,GSL_MAIL';

	$SQL .= ' FROM GSVLIEFERANTEN ';
	$SQL .= ' LEFT JOIN GSVBRANCHEN ON GSL_BRANCHENSCHL = GSB_KEY )';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_GSL',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('GSL', false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsRAV = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('GSL', true));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmLieferantenAdr action=./lieferantenadr_Main.php?cmdAktion=Details method=POST enctype="multipart/form-data">');

	if($rsRAV->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Param['BLOCK']=1;		// Zur�ck setzen, falls es einfach den Block nicht mehr gibt.
	}
	elseif($rsRAV->AnzahlDatensaetze()>1 AND !isset($_GET['GSL_KEY']))						// Liste anzeigen
	{
		$FeldBreiten = array();
		$FeldBreiten['Firma'] = 350;
		$FeldBreiten['Branche'] = 250;
		$FeldBreiten['Lieferantenart'] = 150;
		$FeldBreiten['Aktiv'] = 20;
		$DetailAnsicht = false;
		$Form->Formular_Start();
		$Param['KEY']='';

		$Form->ZeileStart();
		$Link = './lieferantenadr_Main.php?cmdAktion=Details&Sort=GSL_SUCHNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSL_SUCHNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_FIRMA'], $FeldBreiten['Firma'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './lieferantenadr_Main.php?cmdAktion=Details&Sort=GSB_BRANCHE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSB_BRANCHE'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_BRANCHE'], $FeldBreiten['Branche'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
		$Link = './lieferantenadr_Main.php?cmdAktion=Details&Sort=LIEFERANTENART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIEFERANTENART'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_LIEFERANTENART'], $FeldBreiten['Lieferantenart'], '', $Link);
		$Link = './lieferantenadr_Main.php?cmdAktion=Details&Sort=GSL_AKTIV'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSL_AKTIV'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$TTT = $AWISSprachKonserven['RAV']['ttt_UEBAKTIV'];
		$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['Aktiv'], '',$Link,$TTT);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsRAV->EOF())
		{
			$Form->ZeileStart();
			$Link = './lieferantenadr_Main.php?cmdAktion=Details&GSL_KEY='.$rsRAV->FeldInhalt('GSL_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $rsRAV->FeldInhalt('GSL_FIRMA');
			$Form->Erstelle_ListenFeld('GSL_FIRMA', $rsRAV->FeldInhalt('GSL_SUCHNAME'), 0, $FeldBreiten['Firma'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
			$TTT =  $rsRAV->FeldInhalt('GSB_BRANCHE');
			$Form->Erstelle_ListenFeld('GSB_BRANCHE',$rsRAV->FeldInhalt('GSB_BRANCHE'), 0, $FeldBreiten['Branche'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT = $rsRAV->FeldInhalt('LIEFERANTENART');
			$Form->Erstelle_ListenFeld('GSL_LIEFERANTENART',$rsRAV->FeldInhalt('LIEFERANTENART'), 0, $FeldBreiten['Lieferantenart'], false, ($DS%2),'','', 'T', 'L', $TTT);
			$LinkAktiv =  './lieferantenadr_Main.php?cmdAktion=Details&button='.$rsRAV->FeldInhalt('GSL_KEY').'&Aktiv='.$rsRAV->FeldInhalt('GSL_AKTIV');
			$IconsArray = '';
			if ($rsRAV->FeldInhalt('GSL_AKTIV') == 1)
			{
				$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['RAV']['ttt_Aktiv']);
			}
			else
			{
				$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['RAV']['ttt_Inaktiv']);
			}
			$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['Aktiv'] , ($DS%2));

			$Form->ZeileEnde();

			$rsRAV->DSWeiter();
			$DS++;
		}

		$Link = './lieferantenadr_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			
	else								
	{
		$DetailAnsicht = true;

		if($AWIS_KEY1 == '') {
			$AWIS_KEY1 = $rsRAV->FeldInhalt('GSL_KEY');
		}
		$Form->Erstelle_HiddenFeld('GSL_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./lieferantenadr_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRAV->FeldInhalt('GSL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRAV->FeldInhalt('GSL_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_SUCHNAME'].':',200);
		$Form->Erstelle_TextFeld('!GSL_SUCHNAME',$rsRAV->FeldInhalt('GSL_SUCHNAME'),30,200,true);
		$AWISCursorPosition = 'txtGSL_SUCHNAME';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FIRMA'].':',200,'','');
		$Form->Erstelle_Textarea('!GSL_FIRMA',$rsRAV->FeldInhalt('GSL_FIRMA'),60,50,3,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANREDE'].':',200);
		$SQL = 'SELECT ANR_ID, ANR_ANREDE';
		$SQL .= ' FROM ANREDEN';
		$SQL .= ' WHERE BITAND(ANR_BIT,6) > 0';
		$SQL .= ' ORDER BY ANR_ANREDE';
		$Form->Erstelle_SelectFeld('GSL_ANREDE',$rsRAV->FeldInhalt('GSL_ANREDE'),200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TITEL'].':',200);
		$Form->Erstelle_TextFeld('GSL_TITEL',$rsRAV->FeldInhalt('GSL_TITEL'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VORNAME'].':',200);
		$Form->Erstelle_TextFeld('GSL_VORNAME',$rsRAV->FeldInhalt('GSL_VORNAME'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSPRECHPARTNER'].':',200);
		$Form->Erstelle_TextFeld('GSL_ANSPRECHPARTNER',$rsRAV->FeldInhalt('GSL_ANSPRECHPARTNER'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'].':',200);
		$Form->Erstelle_TextFeld('!GSL_STRASSE',$rsRAV->FeldInhalt('GSL_STRASSE'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PLZORT'].':',200);
		$Form->Erstelle_TextFeld('!GSL_PLZ',$rsRAV->FeldInhalt('GSL_PLZ'),2,53,true,'','','','T','','','',5);
		$Form->Erstelle_TextFeld('!GSL_ORT',$rsRAV->FeldInhalt('GSL_ORT'),18,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_BRANCHENSCHL'].':',200);
		$SQL = 'SELECT GSB_KEY, GSB_BRANCHE';
		$SQL .= ' FROM GSVBRANCHEN';
		$SQL .= ' ORDER BY GSB_BRANCHE';
		$Form->Erstelle_SelectFeld('GSL_BRANCHEN',$rsRAV->FeldInhalt('GSL_BRANCHENSCHL'),200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_FIRMA'].':',200);
		$Form->Erstelle_TextFeld('GSL_TELNR_FIRMA',$rsRAV->FeldInhalt('GSL_TELNR_FIRMA'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_PRIVAT'].':',200);
		$Form->Erstelle_TextFeld('GSL_TELNR_PRIVAT',$rsRAV->FeldInhalt('GSL_TELNR_PRIVAT'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_MOBIL'].':',200);
		$Form->Erstelle_TextFeld('GSL_TELNR_MOBIL',$rsRAV->FeldInhalt('GSL_MOBIL'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FAXNR'].':',200);
		$Form->Erstelle_TextFeld('GSL_FAXNR',$rsRAV->FeldInhalt('GSL_FAXNR'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EMAIL'].':',200);
		$Form->Erstelle_TextFeld('GSL_EMAIL',$rsRAV->FeldInhalt('GSL_MAIL'),30,200,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LIEFERANTENART'].':',200);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_LIEFERANTENART']);
		$Form->Erstelle_SelectFeld('!GSL_LIEFERANTENART',$rsRAV->FeldInhalt('GSL_LIEFERANTENART'),150,true,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();

		if($AWIS_KEY1 != '-1') {
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_AKTIV'] . ':', 200);
			$Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
			$Form->Erstelle_SelectFeld('GSL_AKTIV', $rsRAV->FeldInhalt('GSL_AKTIV'), 150, true, '', '', '', '', '', $Daten);
			$Form->ZeileEnde();
		}
		$Form->Formular_Ende();

	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if($DetailAnsicht) {
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	} else {
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	$AWISBenutzer->ParameterSchreiben('Formular_XFT',serialize($Param));
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201615");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201614");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND GSL_KEY = '.$DB->WertSetzen('GSL', 'N0',$AWIS_KEY1).' ';
		return $Bedingung;
	}

	if(isset($Param['GSL_LIEFERANTENART']) AND $Param['GSL_LIEFERANTENART']!='')
	{
		$Bedingung .= ' AND GSL_LIEFERANTENART ' . $DB->LIKEoderIST($Param['GSL_LIEFERANTENART']). '';
	}

	if(isset($Param['GSL_BRANCHE']) AND $Param['GSL_BRANCHE']!='')
	{
		$Bedingung .= ' AND GSL_BRANCHENSCHL ' . $DB->LIKEoderIST($Param['GSL_BRANCHE']). '';
	}
	if(isset($Param['GSL_AKTIV']) AND $Param['GSL_AKTIV']!='')
	{
		$Bedingung .= ' AND GSL_AKTIV ' . $DB->LIKEoderIST($Param['GSL_AKTIV']). '';
	}

	return $Bedingung;
}
?>