<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';


class textkonserven_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht604;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeug;

    private $_VerfuegbareSprachen = null;
    private $_VerfuegbareSprachenSF = null;


    function __construct($Benutzer='')
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht604 = $this->AWISBenutzer->HatDasRecht(604);
        $this->_EditRecht = (($this->Recht604 & 0) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_XTX'));
        $this->AWISWerkzeug = new awisWerkzeuge();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('Textkonserven', '%');
        $TextKonserven[] = array('XTX', '%');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_XTX', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht604 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'XTX'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }


    public function VerfuegbareSprachenArray($SelectFeld = true){

        if($this->_VerfuegbareSprachen == null){
            $SQL  ='select';
            $SQL .='     replace(COLUMN_NAME,\'XTX_TEXT_\',\'\') as SPRACHE';
            $SQL .=' from';
            $SQL .='     sys.ALL_TAB_COLUMNS';
            $SQL .=' where';
            $SQL .='     owner = user';
            $SQL .='     and upper(TABLE_NAME) = upper(\'TEXTKONSERVEN\')';
            $SQL .='     and upper(COLUMN_NAME) like \'XTX_TEXT_%\'';
            $SQL .=' order by';
            $SQL .='    1';

            $rsXTX = $this->DB->RecordSetOeffnen($SQL);

            while(!$rsXTX->EOF()){
                $Sprache = $rsXTX->FeldInhalt('SPRACHE');
                $Beschreibung = isset($this->AWISSprachKonserven['XTX']["XTX_TEXT_$Sprache"])?$this->AWISSprachKonserven['XTX']["XTX_TEXT_$Sprache"]:$Sprache;
                $this->_VerfuegbareSprachen[$Sprache] = $Beschreibung;
                $this->_VerfuegbareSprachenSF[] = $Sprache.'~'.$Beschreibung;
                $rsXTX->DSWeiter();
            }
        }
        if($SelectFeld){
            return $this->_VerfuegbareSprachenSF;
        }else{
            return $this->_VerfuegbareSprachen;
        }

    }

    public function TextkonservenSelectfeldSQL(){
        $SQL  ='select distinct';
        $SQL .='     XTX_BEREICH,';
        $SQL .='     coalesce(B.XTN_TABELLENNAME,XTX_BEREICH) as BESCHREIBUNG';
        $SQL .=' from';
        $SQL .='     TEXTKONSERVEN a';
        $SQL .='     left join TABELLENKUERZEL B on a.XTX_BEREICH = B.XTN_KUERZEL';
        $SQL .=' order by 2';
        return $SQL;
    }


}