<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Absatz</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>

</head>
<body>

<?php
global $_POST;
global $cmdAktion;

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();

if(!(awisHatDasRecht(200)))		// AbsatzSuche (Reifen)
{
	awisEreignis(3,1000,'Benutzerverwaltung',$_SERVER['$PHP_AUTH_USER'],'','','');
	die("Keine ausreichenden Rechte!");
}

	//***********************************************
	//Eingabemaske aufbauen
	//***********************************************

if (isset($_REQUEST['cmdAktion']))
{
	$cmdAktion=$_REQUEST['cmdAktion'];
}	
	
awis_RegisterErstellen(53, $con, $cmdAktion);

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/auswertungen/auswertungen_Main.php';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=absatz','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

if($_SERVER['PHP_AUTH_USER']=='entwick')
{
 include("debug_info.php");
}

awislogoff($con);

?>

</body>
</html>
