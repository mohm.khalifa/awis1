<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('SEP','SEP_%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','Dateiname');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keingueltigesDatum');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht4201 = $AWISBenutzer->HatDasRecht(4201);
if($Recht4201==0)
{
	$Form->Fehler_KeineRechte();    
}


if(!isset($_POST['cmd_Import_x']))
{
	$Form->SchreibeHTMLCode('<form name=frmImport method=post action=./seminarplan_Main.php?cmdAktion=Import enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'].':',150);
	$Form->Erstelle_DateiUpload('Importdatei',300,30,20000000);
	$Form->ZeileEnde();

	$Form->Formular_Ende();
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image','cmd_Import','','/bilder/cmd_weiter.png',$AWISSprachKonserven['Wort']['lbl_weiter'],'W');
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form');
}
else 
{
	if(isset($_FILES['Importdatei']))
	{		
		if(($fd = fopen($_FILES['Importdatei']['tmp_name'],'r'))!==false)
		{			
			$Importfunktion = "import_set";
			
			$Erg = $Importfunktion($fd);
			
			$Form->SchreibeHTMLCode("Statusreport AWIS<br>");
			
			$Form->SchreibeHTMLCode("Datum\t".date('d.m.Y')."<br>");
			
			if(is_array($Erg))
			{
				$Form->SchreibeHTMLCode("<br>");
				$Form->SchreibeHTMLCode("<b>FEHLERMELDUNGEN</b><br>");
				foreach ($Erg['Fehlermeldung'] AS $Fehlermeldung)
				{
					$Form->SchreibeHTMLCode('-FEHLER- '.$Fehlermeldung."<br>");
				}
				
				$Form->SchreibeHTMLCode("<br>");
				$Form->SchreibeHTMLCode("<b>MELDUNGEN</b><br>");
				foreach ($Erg['Meldungen'] AS $Meldung)
				{
					$Form->SchreibeHTMLCode($Meldung."<br>");
				}
			}
			
			$Link='./seminarplan_Main.php?cmdAktion=Suche';				
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png');
			$Form->SchaltflaechenEnde();				
		}
	}
}


function import_sep($fd)
{
	global $AWISBenutzer;
	
	$Form = new awisFormular();
		
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array(), 'Fehlermeldung'=>array(), 'Anforderung'=>'');	
	
	$Zeile = fgets($fd);
	$Zeile = trim(str_replace('"','',$Zeile));	
	
	$Ueberschriften = explode(";",$Zeile);	
	$Ueberschriften = array_flip($Ueberschriften);			
			
	if(!isset($Ueberschriften['SEMINAR']) or !isset($Ueberschriften['BESCHREIBUNG']) 
		or !isset($Ueberschriften['TRAINING']) or !isset($Ueberschriften['VON'])
		or !isset($Ueberschriften['BIS']) or !isset($Ueberschriften['FREIEPLAETZE'])
		or !isset($Ueberschriften['RAUM']) or !isset($Ueberschriften['ORT'])
		//or !isset($Ueberschriften['ZIELGRUPPE']) 
		or !isset($Ueberschriften['TRAINER']))
	{		
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]='Falsche �berschrift';
		
		return $Erg;
	}		
		
	//Alle Seminare auf Status 2 setzen
	$SQL = ' UPDATE SEMINARPLAN SET SEP_STATUS=2';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',908100912,$SQL,2);					
		die();		
	}
		
	//Alle R�ume auf Status 2 setzen
	$SQL = ' UPDATE SEMINARRAEUME SET SER_STATUS=2';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',004191334,$SQL,2);					
		die();		
	}
				
	//Alle Regionen auf Status 2 setzen
	$SQL = ' UPDATE SEMINARREGIONEN SET SRG_STATUS=2';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',004191335,$SQL,2);					
		die();		
	}
	
	$Zeile = fgets($fd);	
	$Zeile = trim(str_replace('"','',$Zeile));	

	$DS = 1;	
	while(!feof($fd))
	{
		$MeldungRaum='';
		$MeldungRegion='';
		
		$Daten = explode(";",$Zeile);		
		if($Daten[$Ueberschriften['SEMINAR']]!='')
		{			
			if($Daten[$Ueberschriften['RAUM']]!='')
			{							
				//Raum ermitteln
				$SQL = ' SELECT * ';
				$SQL .= ' FROM SEMINARRAEUME ';
				$SQL .= ' WHERE SER_RAUM = '.trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RAUM']]));			
				
				//$Form->DebugAusgabe(1,$SQL);		
					
				$rsSER = $DB->RecordSetOeffnen($SQL);				
								
				//Neuen Raum anlegen
				if ($rsSER->AnzahlDatensaetze()==0)
				{
					$SQL = 'INSERT INTO SEMINARRAEUME';
					$SQL .= '(SER_RAUM, SER_STATUS, SER_USER, SER_USERDAT';									
					$SQL .= ')VALUES (';
					$SQL .= ' ' . trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['RAUM']])?$Daten[$Ueberschriften['RAUM']]:''),true));													
					$SQL .= ',1';
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';									
					$SQL .= ',SYSDATE';
					$SQL .= ')';										
					
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben des Seminarraums '.$Daten[$Ueberschriften['RAUM']];					
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;				
					}
					else 
					{
						$MeldungRaum = 'Der Raum '.$Daten[$Ueberschriften['RAUM']].' wurde erfolgreich angelegt.';					
					}
					
					$SQL = 'SELECT seq_SER_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$SERKEY=$rsKey->FeldInhalt('KEY');															
				}
				//Raum vorhanden
				else if ($rsSER->AnzahlDatensaetze()==1)
				{
					$SERKEY=$rsSER->FeldInhalt('SER_KEY');															
					
					//Region auf Status 1 setzen
					$SQL = ' UPDATE SEMINARRAEUME SET SER_STATUS=1 WHERE SER_KEY = '.$DB->FeldInhaltFormat('Z',$SERKEY);
					if($DB->Ausfuehren($SQL)===false)
					{				
						throw new awisException('Fehler beim Speichern',004191336,$SQL,2);					
						die();		
					}					
				}
				else 
				{
					echo 'Mehr als ein Raum vorhanden!!!';
					die();
				}
			}
			else 
			{
				$SERKEY=0;
			}			
						
			//Region ermitteln (nur wenn Raum <> Region)
			if ((trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ORT']])) != '') AND
			    (trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ORT']])) <> trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RAUM']]))))
			{							
				$SQL = ' SELECT * ';
				$SQL .= ' FROM SEMINARREGIONEN ';
				$SQL .= ' WHERE SRG_REGION = '.trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ORT']]));			
				
				$rsSRG = $DB->RecordSetOeffnen($SQL);
								
				//Neuen Raum anlegen
				if ($rsSRG->AnzahlDatensaetze()==0)
				{
					$SQL = 'INSERT INTO SEMINARREGIONEN';
					$SQL .= '(SRG_REGION, SRG_STATUS, SRG_USER, SRG_USERDAT';									
					$SQL .= ')VALUES (';
					$SQL .= ' ' . trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ORT']])?$Daten[$Ueberschriften['ORT']]:''),true));													
					$SQL .= ',1';
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';				
					$SQL .= ',SYSDATE';
					$SQL .= ')';				
					
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben der Seminarregion '.$Daten[$Ueberschriften['ORT']];					
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;				
					}
					else 
					{
						$MeldungRegion = $Daten[$Ueberschriften['ORT']].' wurde erfolgreich angelegt.';					
					}
					
					$SQL = 'SELECT seq_SRG_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$SRGKEY=$rsKey->FeldInhalt('KEY');															
				}
				//Raum vorhanden
				else if ($rsSRG->AnzahlDatensaetze()==1)
				{
					$SRGKEY=$rsSRG->FeldInhalt('SRG_KEY');									

					//Region auf Status 1 setzen
					$SQL = ' UPDATE SEMINARREGIONEN SET SRG_STATUS=1 WHERE SRG_KEY = '.$DB->FeldInhaltFormat('Z',$SRGKEY);
					if($DB->Ausfuehren($SQL)===false)
					{				
						throw new awisException('Fehler beim Speichern',004191337,$SQL,2);					
						die();		
					}				
				}
				else 
				{
					echo 'Mehr als eine Region vorhanden!!!';
					die();
				}
			}
			else 
			{
				$SRGKEY=0;
			}	
						
			$SQL = ' SELECT * ';
			$SQL .= ' FROM SEMINARPLAN ';
			$SQL .= ' WHERE SEP_SEMINAR = '.trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['SEMINAR']]));			
			$SQL .= ' AND SEP_TRAINING = '.trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['TRAINING']]));						
						
			$rsSEP = $DB->RecordSetOeffnen($SQL);
			
			//Neues Seminar anlegen
			if($rsSEP->AnzahlDatensaetze()==0)
			{			
				$SQL = 'INSERT INTO SEMINARPLAN';
				$SQL .= '(SEP_SEMINAR, SEP_BESCHREIBUNG, SEP_TRAINING, SEP_VON, SEP_BIS,';
				$SQL .= ' SEP_FREIEPLAETZE, SEP_SER_KEY, SEP_SRG_KEY, SEP_TRAINER, SEP_STATUS, SEP_USER, SEP_USERDAT';					
				$SQL .= ')VALUES (';
				$SQL .= '' . trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['SEMINAR']],true));
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['BESCHREIBUNG']])?$Daten[$Ueberschriften['BESCHREIBUNG']]:''),true));
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['TRAINING']],true));
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('D',(isset($Daten[$Ueberschriften['VON']])?$Daten[$Ueberschriften['VON']]:''),true));
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('D',(isset($Daten[$Ueberschriften['BIS']])?$Daten[$Ueberschriften['BIS']]:''),true));
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('Z',(isset($Daten[$Ueberschriften['FREIEPLAETZE']])?$Daten[$Ueberschriften['FREIEPLAETZE']]:''),true));				
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('Z',$SERKEY,true));				
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('Z',$SRGKEY,true));				
				//$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ZIELGRUPPE']])?$Daten[$Ueberschriften['ZIELGRUPPE']]:''),true));					
				$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['TRAINER']])?$Daten[$Ueberschriften['TRAINER']]:''),true));					
				$SQL .= ',1';
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';				
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				//$Form->DebugAusgabe(1,$SQL);
				//die();
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Erg['Code']=5;
					$Erg['Zeile'] = $DS;
					$Erg['Fehlermeldung'][]='Fehler beim Schreiben des Seminars '.$Daten[$Ueberschriften['SEMINAR']];
					
					$Zeile = fgets($fd);	
					$Zeile = trim($Zeile);
					$DS++;
					continue;				
				}
				else 
				{
					$Erg['Meldungen'][]='Zeile '.$DS.': Das Seminar mit der Trainingsnummer '.$Daten[$Ueberschriften['TRAINING']].' wurde erfolgreich angelegt. '.$MeldungRaum.' '.$MeldungRegion;
				}
				
				$SQL = 'SELECT seq_SEP_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$KEY2=$rsKey->FeldInhalt('KEY');											
			}
			//Vorhandenes Seminar updaten
			else if ($rsSEP->AnzahlDatensaetze()==1)
			{
				$KEY2=$rsSEP->FeldInhalt('SEP_KEY');											
				
				$SQL = 'UPDATE SEMINARPLAN SET';
				$SQL .= ' SEP_BESCHREIBUNG = '.trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['BESCHREIBUNG']])?$Daten[$Ueberschriften['BESCHREIBUNG']]:''),true));
				$SQL .= ', SEP_VON = '.trim($DB->FeldInhaltFormat('D',(isset($Daten[$Ueberschriften['VON']])?$Daten[$Ueberschriften['VON']]:''),true));
				$SQL .= ', SEP_BIS = '.trim($DB->FeldInhaltFormat('D',(isset($Daten[$Ueberschriften['BIS']])?$Daten[$Ueberschriften['BIS']]:''),true));
				$SQL .= ', SEP_FREIEPLAETZE = '.trim($DB->FeldInhaltFormat('Z',(isset($Daten[$Ueberschriften['FREIEPLAETZE']])?$Daten[$Ueberschriften['FREIEPLAETZE']]:''),true));
				$SQL .= ', SEP_SER_KEY = '.($DB->FeldInhaltFormat('Z',$SERKEY,true));				
				$SQL .= ', SEP_SRG_KEY = '.($DB->FeldInhaltFormat('Z',$SRGKEY,true));				
				//$SQL .= ', SEP_ZIELGRUPPE = '.trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ZIELGRUPPE']])?$Daten[$Ueberschriften['ZIELGRUPPE']]:''),true));
				$SQL .= ', SEP_TRAINER = '.trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['TRAINER']])?$Daten[$Ueberschriften['TRAINER']]:''),true));
				$SQL .= ', SEP_STATUS = 1';
				$SQL .= ', SEP_USER = \'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ', SEP_USERDAT = SYSDATE';
				$SQL .= ' WHERE SEP_KEY ='.$KEY2;
				
				//$Form->DebugAusgabe(1,$SQL);
				//die();
				
				if($DB->Ausfuehren($SQL)===false)
				{
					$Erg['Code']=5;
					$Erg['Zeile'] = $DS;
					$Erg['Fehlermeldung'][]='Fehler beim Schreiben des Seminars '.$Daten[$Ueberschriften['SEMINAR']];
					
					$Zeile = fgets($fd);	
					$Zeile = trim($Zeile);
					$DS++;
					continue;
				}
				else 
				{
					$Erg['Meldungen'][]='Zeile '.$DS.': Das Seminar mit der Trainingsnummer '.$Daten[$Ueberschriften['TRAINING']].' wurde erfolgreich aktualisiert.';
				}
			}
			else 
			{
				echo 'Mehr als ein Seminar vorhanden!!!!';
				die();
			}
		}
		
		$Zeile = fgets($fd);	
		$Zeile = trim(str_replace('"','',$Zeile));	
		$DS++;
	}
	
	//Alle nicht vorhandenen Seminare auf Status 3 (inaktiv) setzen
	$SQL = ' UPDATE SEMINARPLAN SET SEP_STATUS=3 WHERE SEP_STATUS=2';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',908100912,$SQL,2);					
		die();		
	}
	
	//Alle nicht vorhandenen R�ume auf Status 3 (inaktiv) setzen
	$SQL = ' UPDATE SEMINARRAEUME SET SER_STATUS=3 WHERE SER_STATUS=2';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',004191338,$SQL,2);					
		die();		
	}
	
	//Alle nicht vorhandenen Regionen auf Status 3 (inaktiv) setzen
	$SQL = ' UPDATE SEMINARREGIONEN SET SRG_STATUS=3 WHERE SRG_STATUS=2';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',004191339,$SQL,2);					
		die();		
	}
	
	return $Erg;
}


function import_set($fd)
{
	global $AWISBenutzer;
	
	$Form = new awisFormular();
		
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Erg = array('Code'=>0, 'Letzte Meldung'=>'', 'Zeile'=>0, 'Meldungen'=>array(), 'Fehlermeldung'=>array(), 'Anforderung'=>'');	
	
	//Alle Mitarbeiter auf Status 2 setzen
	$SQL = 'UPDATE SEMINARTEILNEHMER SET SET_STATUSTEILNEHMER = 2 ';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',004121422,$SQL,2);					
		die();		
	}
		
	$Zeile = fgets($fd);	
	$Zeile = trim(str_replace('"','',$Zeile));	
	
	$Ueberschriften = explode(";",$Zeile);	
	$Ueberschriften = array_flip($Ueberschriften);	
			
	if(!isset($Ueberschriften['ID']) or !isset($Ueberschriften['NACHNAME']) 
		or !isset($Ueberschriften['VORNAME']) or !isset($Ueberschriften['STATUS'])
		or !isset($Ueberschriften['FILIALNR']) or !isset($Ueberschriften['FILIALE'])
		or !isset($Ueberschriften['SEMINAR']) or !isset($Ueberschriften['TRAINING'])
		or !isset($Ueberschriften['ANGEMELDETAM']))		
	{		
		$Erg['Code']=1;
		$Erg['Fehlermeldung'][]='Falsche �berschrift';
		
		return $Erg;
	}					
		
	$Zeile = fgets($fd);
	$Zeile = trim(str_replace('"','',$Zeile));	

	$DS = 1;	
	while(!feof($fd))
	{		
		$Daten = explode(";",$Zeile);		
		if($Daten[$Ueberschriften['SEMINAR']]!='' AND $Daten[$Ueberschriften['TRAINING']]!='')
		{			
			$SQL = ' SELECT * ';
			$SQL .= ' FROM SEMINARPLAN ';
			$SQL .= ' WHERE SEP_SEMINAR = '.trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['SEMINAR']]));			
			$SQL .= ' AND SEP_TRAINING = '.trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['TRAINING']]));						
			
			//$Form->DebugAusgabe(1,$SQL);
			//die();			
			
			$rsSEP = $DB->RecordSetOeffnen($SQL);
			
			//Seminar vorhanden
			if($rsSEP->AnzahlDatensaetze()==1)
			{								
				//Pr�fen, ob Mitarbeiter schon zu dem Seminar zugeordnet ist
				$SQL = 'SELECT *';
				$SQL.= ' FROM SEMINARTEILNEHMER';
				$SQL.= ' WHERE SET_SEP_KEY = '.$DB->FeldInhaltFormat('Z',$rsSEP->FeldInhalt('SEP_KEY'));
				$SQL.= ' AND SET_PER_NR = '.trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ID']]));						;
				
				$rsSET = $DB->RecordSetOeffnen($SQL);
				
				//$Form->DebugAusgabe(1,$SQL);
				//die();
				
				if ($rsSET->AnzahlDatensaetze()==0)
				{			
					//Seminarteilnehmer hinzuf�gen						
					$SQL = 'INSERT INTO SEMINARTEILNEHMER';
					$SQL .= '(SET_SEP_KEY, SET_PER_NR, SET_NACHNAME, SET_VORNAME, SET_ANMELDETAG, SET_STATUS,';
					$SQL .= ' SET_FIL_NR, SET_FILIALE, SET_STATUSTEILNEHMER, SET_USER, SET_USERDAT ';				
					$SQL .= ')VALUES (';
					$SQL .= '' . ($DB->FeldInhaltFormat('Z',$rsSEP->FeldInhalt('SEP_KEY'),true));
					$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ID']])?$Daten[$Ueberschriften['ID']]:''),true));
					$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['NACHNAME']],true));
					$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['VORNAME']],true));
					$SQL .= ' ,' . trim($DB->FeldInhaltFormat('D',$Daten[$Ueberschriften['ANGEMELDETAM']],true));
					$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['STATUS']],true));
					$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['FILIALNR']],true));
					$SQL .= ' ,' . trim($DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['FILIALE']],true));								
					$SQL .= ',1';
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';				
					$SQL .= ',SYSDATE';
					$SQL .= ')';
				
					//$Form->DebugAusgabe(1,$SQL);
					//die();
					
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben der Seminarteilnehmer '.$Daten[$Ueberschriften['ID']];
						//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;
					}				
					else 
					{
						$Erg['Meldungen'][]='Zeile '.$DS.': Der Teilnehmer mit der Personalnummer '.$Daten[$Ueberschriften['ID']].' wurde erfolgreich angelegt.';
					}
				}
				else if ($rsSET->AnzahlDatensaetze()==1)
				{
					$KEY2=$rsSET->FeldInhalt('SET_KEY');											
					
					$SQL = 'UPDATE SEMINARTEILNEHMER SET';
					$SQL .= ' SET_NACHNAME = '.trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['NACHNAME']])?$Daten[$Ueberschriften['NACHNAME']]:''),true));
					$SQL .= ', SET_VORNAME = '.trim($DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['VORNAME']])?$Daten[$Ueberschriften['VORNAME']]:''),true));
					$SQL .= ', SET_ANMELDETAG = '.trim($DB->FeldInhaltFormat('D',(isset($Daten[$Ueberschriften['ANGEMELDETAM']])?$Daten[$Ueberschriften['ANGEMELDETAM']]:''),true));
					$SQL .= ', SET_STATUS = '.trim($DB->FeldInhaltFormat('D',(isset($Daten[$Ueberschriften['STATUS']])?$Daten[$Ueberschriften['STATUS']]:''),true));
					$SQL .= ', SET_FIL_NR = '.trim($DB->FeldInhaltFormat('Z',(isset($Daten[$Ueberschriften['FILIALNR']])?$Daten[$Ueberschriften['FILIALNR']]:''),true));										
					$SQL .= ', SET_STATUSTEILNEHMER = 1';
					$SQL .= ', SET_USER = \'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ', SET_USERDAT = SYSDATE';
					$SQL .= ' WHERE SET_KEY ='.$KEY2;
				
					//$Form->DebugAusgabe(1,$SQL);
					//die();
					
					if($DB->Ausfuehren($SQL)===false)
					{
						$Erg['Code']=5;
						$Erg['Zeile'] = $DS;
						$Erg['Fehlermeldung'][]='Fehler beim Schreiben des Seminars '.$Daten[$Ueberschriften['SEMINAR']];
						//$Erg['Letzte Meldung']='Fehler beim Schreiben der ATUNR '.$Daten[$Ueberschriften['Bezeichnung']];
						
						$Zeile = fgets($fd);	
						$Zeile = trim($Zeile);
						$DS++;
						continue;					
					}
					else 
					{
						$Erg['Meldungen'][]='Zeile '.$DS.': Der Teilnehmer mit der Personalnummer '.$Daten[$Ueberschriften['ID']].' wurde erfolgreich aktualisiert.';
					}
				}
				else 
				{
					echo 'Seminarteilnehmer mehrfach vorhanden!!!';
					die();
				}
			}
			else 
			{
				echo 'Seminar nicht oder mehrfach vorhanden!!!';
				die();
			}								
		}
		
		$Zeile = fgets($fd);			
		$Zeile = trim(str_replace('"','',$Zeile));	
		$DS++;
	}
	
	//Alle nicht vorhandenen Seminare auf Status 3 (inaktiv) setzen
	$SQL = ' UPDATE SEMINARTEILNEHMER SET SET_STATUSTEILNEHMER=3 WHERE SET_STATUSTEILNEHMER=2';
	if($DB->Ausfuehren($SQL)===false)
	{				
		throw new awisException('Fehler beim Speichern',004121440,$SQL,2);					
		die();		
	}
	
	return $Erg;
}

?>

