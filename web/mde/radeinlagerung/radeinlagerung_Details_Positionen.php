<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FIL_ID;
global $LFDNR;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('REL','%');	
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','ttt_PositionHinzufuegen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Wort','Rueckfuehrungstyp');
	$TextKonserven[]=array('Wort','Gesamtbestand');
	$TextKonserven[]=array('Wort','Urspruengliche');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$AWISSprache = $AWISBenutzer->BenutzerSprache();

	if($AWISSprache=='')
	{
		$AWISSprache='DE';
	}

	$Recht3602 = $AWISBenutzer->HatDasRecht(3602);
	if($Recht3602==0)
	{
		$Form->Fehler_KeineRechte();
	}
			
	//$Param = unserialize($AWISBenutzer->ParameterLesen('LKKSuche'));
	$Params = unserialize($AWISBenutzer->ParameterLesen('RELPos'));
		
	//********************************************************
	// Daten suchen
	//********************************************************	
	$SQL = 'SELECT distinct REL.*';
	$SQL .= ', row_number() over (order by REL_NR) AS ZeilenNr';
	$SQL .=' FROM Raedereinlagerungen REL';	
	$SQL .=' WHERE REL_FIL_ID=0'.$FIL_ID;			
	$SQL .=' AND REL_LFDNR=0'.$LFDNR;			
	$SQL .= ' ORDER BY REL_NR';
	
	$Form->DebugAusgabe(1,$SQL);
	
	$Block = 1;
	
	//if(isset($_REQUEST['Block']) AND !isset($_POST['cmdSpeichern_x']) AND !isset($_POST['cmdAbschliessen_x']))
	if(isset($_REQUEST['Block']) AND !isset($_POST['cmdAbschliessen_x']))
	{				
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);		
		$Params['BLOCKPOS']=$Block;	
		$AWISBenutzer->ParameterSchreiben('RELPos',serialize($Params));
	}
	elseif(isset($Param['BLOCKPOS']) AND $Param['BLOCKPOS']!='')
	{
		$Block = intval($Params['BLOCKPOS']);
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//var_dump($SQL,$Param['BLOCKPOS'], $_POST);
	$Form->DebugAusgabe(1,$SQL);
	$rsREL = $DB->RecordSetOeffnen($SQL);

	if($rsREL->AnzahlDatensaetze()>=1)						// Liste anzeigen
	{	
		$Form->Formular_Start();		
			
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_NR'],120);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_DATUMSCAN'],120);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_MENGE'],80);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_BESCHAEDIGT'],120);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_BEMERKUNG'],230);		
		if(($Recht3602 & 8) == 8)
		{
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_MENGEIST'],100);
		}
		$Form->ZeileEnde();										

		$RELZeile=0;
		while(!$rsREL->EOF())
		{						
			//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
			if ((($Recht3602&2)==2) AND $DB->FeldInhaltFormat('NO',$rsREL->FeldInhalt('REL_STATUS'))<10)
			{
				$EditModus=true;					
			}
			else
			{
				$EditModus=false;				
			}
						
			$Form->ZeileStart();						
			$Form->Erstelle_ListenFeld('REL_NR',$rsREL->FeldInhalt('REL_NR'),10,120,false,($RELZeile%2),'','','T','L');
			$Form->Erstelle_ListenFeld('REL_DATUMSCAN',$rsREL->FeldInhalt('REL_DATUMSCAN'),10,120,false,($RELZeile%2),'','','D','L');
			$Form->Erstelle_ListenFeld('REL_MENGE_'.$rsREL->FeldInhalt('REL_KEY'),$rsREL->FeldInhalt('REL_MENGE'),7,80,$EditModus,($RELZeile%2),'','','T','L');
			$Form->Erstelle_Checkbox('REL_BESCHAEDIGT_VL_'.$rsREL->FeldInhalt('REL_KEY'), $rsREL->Feldinhalt('REL_BESCHAEDIGT_VL'),30,$EditModus,'on','','Beschädigung vorne links');
			$Form->Erstelle_Checkbox('REL_BESCHAEDIGT_VR_'.$rsREL->FeldInhalt('REL_KEY'), $rsREL->Feldinhalt('REL_BESCHAEDIGT_VR'),30,$EditModus,'on','','Beschädigung vorne rechts');
			$Form->Erstelle_Checkbox('REL_BESCHAEDIGT_HL_'.$rsREL->FeldInhalt('REL_KEY'), $rsREL->Feldinhalt('REL_BESCHAEDIGT_HL'),30,$EditModus,'on','','Beschädigung hinten links');
			$Form->Erstelle_Checkbox('REL_BESCHAEDIGT_HR_'.$rsREL->FeldInhalt('REL_KEY'), $rsREL->Feldinhalt('REL_BESCHAEDIGT_HR'),30,$EditModus,'on','','Beschädigung hinten rechts');
			$Form->Erstelle_ListenFeld('REL_BEMERKUNG_'.$rsREL->FeldInhalt('REL_KEY'),$rsREL->FeldInhalt('REL_BEMERKUNG'),32,230,$EditModus,($RELZeile%2),'','','T','L');		
			if(($Recht3602 & 8) == 8)
			{
				$Form->Erstelle_ListenFeld('REL_MENGESCAN',$rsREL->FeldInhalt('REL_MENGESCAN'),10,100,false,($RELZeile%2),'','','T');				
			}
			$Form->ZeileEnde();
	
			$rsREL->DSWeiter();
			$RELZeile++;
		}		

		$Link = './radeinlagerung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'&REL_FIL_ID='.$FIL_ID.'&REL_LFDNR='.$LFDNR;
		//$Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'&LKK_KEY='.$AWIS_KEY1.(isset($_POST['txtAlleAnzeigen'])||isset($_GET['ALLE'])?'&ALLE=1':'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
			
		$Form->Formular_Ende();

	}			// Eine einzelne Adresse	
}

catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>