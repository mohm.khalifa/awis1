<?php
global $AWIS_KEY1;

require_once('awisFilialen.inc');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	
    $TextKonserven = array();
    
    $TextKonserven[]=array('ADK','*');
    $TextKonserven[]=array('ADP','*');
    $TextKonserven[]=array('ADS','*');
    
    $TextKonserven[]=array('FIL','FIL_ID');
    $TextKonserven[]=array('FIL','FIL_GEBIET');
    $TextKonserven[]=array('FIL','FIL_BEZ');
    $TextKonserven[]=array('FIB','FIB_BESTAND');
    
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','Soll');
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_AdrAuswertungen'));
	
	if (isset($_GET['cmdAktion']) AND ($_GET['cmdAktion']=='CSV'))
	{
		$Trenner=";";
	
		@ob_clean();
	
		//header("Cache-Control: no-cache, must-revalidate");
		//header("Expires: 01 Jan 2000");
		header('Pragma: public');
		header('Cache-Control: max-age=0');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="datenexport.csv"');
	
		//********************************************************
		// Bedingung erstellen
		//********************************************************
		$BindeVariablen = array();
		$Bedingung = _ExportBedingungErstellen($Param, $BindeVariablen);
			
		//*****************************************************************
		// Sortierung aufbauen
		//*****************************************************************
		if (isset($Param['ORDER']) and $Param['ORDER'] != '')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ADS_EWCCODE ASC';
		}
	
		$SQL  = 'SELECT';
		$SQL .= ' DAT.*,';
        $SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILE';
		$SQL .= ' FROM ';
		$SQL .= ' (';
		$SQL .= ' SELECT ';
        $SQL .= ' ADS_LAGACODE,';
        $SQL .= ' ADS_LAGABEZ,';
        $SQL .= ' ADS_EWCCODE,';
        $SQL .= ' ADS_EWCBEZ,';
        $SQL .= ' SUM(ADP_MENGE) AS ADP_MENGE,';
        $SQL .= ' ROUND(SUM(ADP_GEWICHT)) AS ADP_GEWICHT';
        if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
        {
            $SQL .= ' ,ADK_LAGERKZ';
        }
        $SQL .= ' FROM ADRKOPF adk';
        $SQL .= ' INNER JOIN ADRPOS adp';
        $SQL .= ' ON adp.adp_adk_key=adk.adk_key';
        $SQL .= ' INNER JOIN ADRSTAMM ads';
        $SQL .= ' ON ads.ads_key=adp.adp_ads_key';
		if($Bedingung!='')
		{
			$SQL .= ' WHERE ' . substr($Bedingung,4);
		}		
        $SQL .= ' GROUP BY';
        $SQL .= ' ADS_LAGACODE,';
        $SQL .= ' ADS_LAGABEZ,';
        $SQL .= ' ADS_EWCCODE,';
        $SQL .= ' ADS_EWCBEZ';
        if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
        {
            $SQL .= ' ,ADK_LAGERKZ';
        }
		$SQL .= ' ) DAT';
		$SQL .= ' ORDER BY ' .$ORDERBY;
							
		// Blockgr��e festlegen
		$Blockgroesse = 5000; 
	
		for($i=1;$i<999;$i++)
		{
			$BindeVariablen['var_N0_ZEILE_VON'] = $Form->Format('Z',($i-1)*$Blockgroesse,false);
			$BindeVariablen['var_N0_ZEILE_BIS'] = $Form->Format('Z',($i)*$Blockgroesse,false) ;
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZEILE >= :var_N0_ZEILE_VON AND ZEILE < :var_N0_ZEILE_BIS';
			
			$rsDaten = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	
			if($i==1)		// Beim ersten Mal den Kopf schreiben
			{
				$Felder = $rsDaten->SpaltenNamen();
				
				$Zeile  = $AWISSprachKonserven['ADS']['ADS_EWCBEZ'];
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['ADP']['ADP_GEWICHT'];
				$Zeile .= "\n";
				
				echo $Zeile;
			}
	
			while(!$rsDaten->EOF())
			{
                $Zeile  = $rsDaten->FeldInhalt('ADS_EWCBEZ');
				$Zeile .= $Trenner;
                $Zeile .= $rsDaten->FeldInhalt('ADP_GEWICHT');
				$Zeile .= "\n";
				
				echo $Zeile;
				
				$rsDaten->DSWeiter();
			}
	
			if($rsDaten->AnzahlDatensaetze()==0)
			{
				break;
			}
		}
	}
	elseif (isset($_GET['cmdAktion']) AND ($_GET['cmdAktion']=='Erzeuger'))
	{
		$Trenner=";";
	
		@ob_clean();
	
		//header("Cache-Control: no-cache, must-revalidate");
		//header("Expires: 01 Jan 2000");
		header('Pragma: public');
		header('Cache-Control: max-age=0');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="datenexport.csv"');
	
		$ORDERBY = ' XX1_FIL_ID ASC';
	
		$SQL  = 'SELECT';
		$SQL .= ' DAT.*,';
        $SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILE';
		$SQL .= ' FROM ';
		$SQL .= ' (';
		$SQL .= " SELECT XX1_FIL_ID, NVL(FIF_WERT,'----------') AS FIF_WERT";
		$SQL .= ' FROM V_FILIALPFAD';
		$SQL .= ' INNER JOIN FILIALEN';
		$SQL .= ' ON XX1_FIL_ID=FIL_ID';
		$SQL .= ' INNER JOIN FILIALINFOS';
		$SQL .= ' ON XX1_FIL_ID=FIF_FIL_ID';
		$SQL .= ' WHERE FIF_FIT_ID=911';
		$SQL .= " AND FIL_LAN_WWSKENN='BRD'";
		$SQL .= ' ) DAT';
		$SQL .= ' ORDER BY ' .$ORDERBY;
							
		// Blockgr��e festlegen
		$Blockgroesse = 5000; 
	
		for($i=1;$i<999;$i++)
		{
			$BindeVariablen['var_N0_ZEILE_VON'] = $Form->Format('Z',($i-1)*$Blockgroesse,false);
			$BindeVariablen['var_N0_ZEILE_BIS'] = $Form->Format('Z',($i)*$Blockgroesse,false) ;
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZEILE >= :var_N0_ZEILE_VON AND ZEILE < :var_N0_ZEILE_BIS';
			
			$rsDaten = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	
			if($i==1)		// Beim ersten Mal den Kopf schreiben
			{
				$Felder = $rsDaten->SpaltenNamen();
				
				$Zeile  = $AWISSprachKonserven['FIL']['FIL_ID'];
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['ADK']['ADK_ERZEUGERNUMMER'];
				$Zeile .= "\n";
				
				echo $Zeile;
			}
	
			while(!$rsDaten->EOF())
			{
                $Zeile  = $rsDaten->FeldInhalt('XX1_FIL_ID');
				$Zeile .= $Trenner;
                $Zeile .= $rsDaten->FeldInhalt('FIF_WERT');
				$Zeile .= "\n";
				
				echo $Zeile;
				
				$rsDaten->DSWeiter();
			}
	
			if($rsDaten->AnzahlDatensaetze()==0)
			{
				break;
			}
		}
	}
}

catch (Exception $ex)
{
	die($ex->getMessage());
}

function _ExportBedingungErstellen($Param, &$BindeVariablen)
{
	global $Form;
	global $AWISBenutzer;
	global $DB;
	global $Recht4303;
	
	$Bedingung = '';
	if(isset($Param['ADK_FIL_ID']) AND $Param['ADK_FIL_ID']!='')
	{
		$Bedingung .= ' AND ADK_FIL_ID = :var_N0_ADK_FIL_ID';
		$BindeVariablen['var_N0_ADK_FIL_ID'] = $Form->Format('N0',$Param['ADK_FIL_ID'],false);
	}
	
	if(isset($Param['BUL_ID']) AND $Param['BUL_ID']!='0')
	{
	    $Bedingung .= ' AND adk.adk_fil_id IN (SELECT FIF_FIL_ID FROM FILIALINFOS WHERE FIF_FIT_ID=65 AND FIF_WERT='.$Form->Format('T',$Param['BUL_ID'],false).' AND FIF_IMQ_ID=8)';
	}
	
	if(isset($Param['DATUM_VOM']) AND $Param['DATUM_VOM']!='')
	{
		$Bedingung .= ' AND TRUNC(adk_datumabschluss) >= :var_D_DATUM_VOM';
		$BindeVariablen['var_D_DATUM_VOM'] = $Form->Format('D',$Param['DATUM_VOM'],false);
	}
	
	if(isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS']!='')
	{
		$Bedingung .= ' AND TRUNC(adk_datumabschluss) <= :var_D_DATUM_BIS';
		$BindeVariablen['var_D_DATUM_BIS'] = $Form->Format('D',$Param['DATUM_BIS'],false);
	}

	if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
	{
		$Bedingung .= ' AND ADK_LAGERKZ = :var_T_ADK_LAGERKZ';
		$BindeVariablen['var_T_ADK_LAGERKZ'] = $Form->Format('T',$Param['ADK_LAGERKZ'],false);
	}
	
	if(isset($Param['ALTOEL']) AND $Param['ALTOEL']=='')
	{
		$Bedingung .= " AND ADS_EWCCODE <> '130205*'";
	}
	
	if(isset($Param['ALTOEL']) AND $Param['ALTOEL']!='')
	{
		$Bedingung .= " AND ADS_EWCCODE = '130205*'";
		$Bedingung .= ' AND ADS_ADRNR = :var_T_ADS_ADRNR';
		$BindeVariablen['var_T_ADS_ADRNR'] = $Form->Format('Z',$Param['ALTOEL'],false);
	}
		
	return $Bedingung;
}

?>

