<?php
global $Param;
global $awisRSInfo;
global $awisDBError;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamte Bereich
{
	$Tabelle = 'FEB';
	$Key=$_POST['txtFEB_KEY'];
	$FEB_KEY=$_POST['txtFEB_KEY'];

	$Felder=array();

	$Felder[]=array(awis_TextKonserve($con,'FEB_BEZEICHNUNG','FEB',$AWISSprache),$_POST['txtFEB_BEZEICHNUNG']);
}

elseif((isset($_GET['Del']) OR isset($_GET['DelFRV'])) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'Rollenzuordnungen':
			$Tabelle = 'FRZ';
			$Key=$_GET['Del'];
	
			$SQL = 'SELECT FRZ_GUELTIGAB,FRZ_GUELTIGBIS,FRZ_XXX_KEY,FER_BEZEICHNUNG,KON_NAME1';
			$SQL .= ' FROM Filialebenenrollenzuordnungen';
			$SQL .= ' INNER JOIN Kontakte ON FRZ_KON_KEY = KON_KEY';
			$SQL .= ' INNER JOIN Filialebenenrollen ON FRZ_FER_KEY = FER_KEY';
			$SQL .=' WHERE FRZ_KEY=0'.$Key;
			$SQL .= ' AND FRZ_XTN_KUERZEL = \'FEB\'';
			
			$rsDaten = awisOpenRecordset($con, $SQL);
			$Felder=array();
			$Felder[]=array(awis_TextKonserve($con,'KON_NAME1','KON',$AWISSprache),$rsDaten['KON_NAME1'][0]);
			$Felder[]=array(awis_TextKonserve($con,'FER_BEZEICHNUNG','FER',$AWISSprache),$rsDaten['FER_BEZEICHNUNG'][0]);
			$Felder[]=array(awis_TextKonserve($con,'FRZ_GUELTIGAB','FRZ',$AWISSprache),$rsDaten['FRZ_GUELTIGAB'][0]);
			$Felder[]=array(awis_TextKonserve($con,'FRZ_GUELTIGBIS','FRZ',$AWISSprache),$rsDaten['FRZ_GUELTIGBIS'][0]);
			$FEB_KEY = $rsDaten['FRZ_XXX_KEY'][0];
			break;
		case 'Vertreter':
				$Tabelle = 'FRV';
				$Key=$_GET['DelFRV'];
				
				$_GET['Seite'] = 'Rollenzuordnungen';
				
				//https://awis-entwick.server.atu.de/filialebenen/filialebenen_Main.php?cmdAktion=Details&Seite=Rollenzuordnungen&FRZ_KEY=33024
				
				$SQL = 'SELECT * FROM FILIALEBENENROLLENVERTRETER ';
				$SQL .= 'INNER JOIN KONTAKTE ON KON_KEY = FRV_KON_KEY ';
				$SQL .= 'INNER JOIN FILIALEBENENROLLENZUORDNUNGEN ON FRZ_KEY = FRV_FRZ_KEY ';
				$SQL .= 'WHERE FRV_KEY='.$Key;
					
				$rsDaten = awisOpenRecordset($con, $SQL);
				$Felder=array();
				$Felder[]=array(awis_TextKonserve($con,'KON_NAME','KON',$AWISSprache),$rsDaten['KON_NAME1'][0].', '.$rsDaten['KON_NAME2'][0]);
				$Felder[]=array(awis_TextKonserve($con,'FRV_GUELTIGAB','FRV',$AWISSprache),$rsDaten['FRV_DATUMVON'][0]);
				$Felder[]=array(awis_TextKonserve($con,'FRV_GUELTIGBIS','FRV',$AWISSprache),$rsDaten['FRV_DATUMBIS'][0]);
				$FEB_KEY = $rsDaten['FRZ_XXX_KEY'][0];
				break;
		case 'Filialzuordnungen':			
			if (isset($_GET['Unterseite']) and $_GET['Unterseite']=='FilialzuordnungenRollen')
			{
				$Tabelle = 'FRZFIL';
				$Key=$_GET['Del'];
		
				$SQL = 'SELECT FRZ_GUELTIGAB,FRZ_GUELTIGBIS,FRZ_XXX_KEY,FER_BEZEICHNUNG,KON_NAME1';
				$SQL .= ' FROM Filialebenenrollenzuordnungen';
				$SQL .= ' INNER JOIN Kontakte ON FRZ_KON_KEY = KON_KEY';
				$SQL .= ' INNER JOIN Filialebenenrollen ON FRZ_FER_KEY = FER_KEY';
				$SQL .=' WHERE FRZ_KEY=0'.$Key;
				$SQL .= ' AND FRZ_XTN_KUERZEL = \'FIL\'';
				
				$rsDaten = awisOpenRecordset($con, $SQL);
				$Felder=array();
				$Felder[]=array(awis_TextKonserve($con,'KON_NAME1','KON',$AWISSprache),$rsDaten['KON_NAME1'][0]);
				$Felder[]=array(awis_TextKonserve($con,'FER_BEZEICHNUNG','FER',$AWISSprache),$rsDaten['FER_BEZEICHNUNG'][0]);
				$Felder[]=array(awis_TextKonserve($con,'FRZ_GUELTIGAB','FRZ',$AWISSprache),$rsDaten['FRZ_GUELTIGAB'][0]);
				$Felder[]=array(awis_TextKonserve($con,'FRZ_GUELTIGBIS','FRZ',$AWISSprache),$rsDaten['FRZ_GUELTIGBIS'][0]);
								
				$FEB_KEY = $_GET['FEB_KEY'];
				//$FIL_KEY = $rsDaten['FRZ_XXX_KEY'][0];
				break;				
			}			
			else 
			{
				$Tabelle = 'FEZ';
				$Key=$_GET['Del'];
		
				$SQL = 'SELECT FEZ_GUELTIGAB,FEZ_GUELTIGBIS,FEZ_FIL_ID,FEZ_FEB_KEY';
				$SQL .= ' FROM FILIALEBENENZUORDNUNGEN';
				$SQL .= ' INNER JOIN Filialen ON FEZ_FIL_ID = FIL_ID';
				$SQL .=' WHERE FEZ_KEY=0'.$Key;
	
				$rsDaten = awisOpenRecordset($con, $SQL);
				$Felder=array();
				$Felder[]=array(awis_TextKonserve($con,'FIL_ID','FIL',$AWISSprache),$rsDaten['FEZ_FIL_ID'][0]);
				$Felder[]=array(awis_TextKonserve($con,'FEZ_GUELTIGAB','FEZ',$AWISSprache),$rsDaten['FEZ_GUELTIGAB'][0]);
				$Felder[]=array(awis_TextKonserve($con,'FEZ_GUELTIGBIS','FEZ',$AWISSprache),$rsDaten['FEZ_GUELTIGBIS'][0]);
				$FEB_KEY = $rsDaten['FEZ_FEB_KEY'][0];
				break;				
			}			
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'FEB':
			$SQL = 'DELETE FROM filialebenenrollen WHERE FEB_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			break;
		case 'FRZ':
			$SQL = 'DELETE FROM filialebenenrollenzuordnungen WHERE FRZ_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=$_POST['txtFEB_KEY'];
			break;
		case 'FRZFIL':
			$SQL = 'DELETE FROM filialebenenrollenzuordnungen WHERE FRZ_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=$_POST['txtFEB_KEY'];
			break;			
		case 'FEZ':
			$SQL = 'DELETE FROM FILIALEBENENZUORDNUNGEN WHERE FEZ_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=$_POST['txtFEB_KEY'];
			break;
		case 'FRV':
			$SQL = 'DELETE FROM FILIALEBENENROLLENVERTRETER WHERE FRV_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=$_POST['txtFEB_KEY'];
			break;
		default:
			break;
	}

	if($SQL !='')
	{
		if(awisExecute($con,$SQL)==false)
		{
			awisErrorMailLink('filialebenen_stammdaten_loeschen',1,$awisDBError['messages'],'');
		}
		awis_BenutzerParameterSpeichern($con, "AktuellerFilialBereich" , $AWISBenutzer->BenutzerName(), $AWIS_KEY1);
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

	echo '<form name=frmLoeschen action=./filialebenen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').($Tabelle == 'FRV'?'&FRZ_KEY='.$rsDaten['FRV_FRZ_KEY'][0]:'').' method=post>';
	echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': <b>'.$Feld[1].'</b>';
	}

	echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
	echo '<input type=hidden name=txtKey value="'.$Key.'">';		
	echo '<input type=hidden name=txtFEB_KEY value="'.$FEB_KEY.'">';	

	echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
	echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';

	echo '</form>';
	awisLogoff($con);
	die();
}
?>