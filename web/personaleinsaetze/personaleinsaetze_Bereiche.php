<?php
/**
 * Bereichsverwaltung f�r die Personaleins�tze
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200809161558
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PEB','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4501 = $AWISBenutzer->HatDasRecht(4501);
	if($Recht4501==0)
	{
		$Form->Fehler_KeineRechte();
	}

	//********************************************************
	// Parameter verarbeiten
	//********************************************************
	if(isset($_POST['cmdDSZurueck_x']))
	{
		$SQL = 'SELECT PEB_KEY FROM (SELECT PEB_KEY ';
		$SQL .= ' FROM PersEinsBereiche';
		$SQL .= ' LEFT JOIN PersEinsBereicheMitglieder ON PBM_PEB_KEY = PEB_KEY';
		$SQL .= ' WHERE PEB_BEZEICHNUNG < '.$DB->FeldInhaltFormat('T',$_POST['txtPEB_BEZEICHNUNG']);
		if(($Recht4501&16)==0)		// Nur den eigenen Bereich anzeigen
		{
			$Bedingung .= ' AND (PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
			$Bedingung .= ' AND PBR_PBZ_KEY = 6)';
		}
		$SQL .= ' ORDER BY PEB_BEZEICHNUNG DESC';
		$SQL .= ') WHERE ROWNUM = 1';
		$rsPEB = $DB->RecordSetOeffnen($SQL);
		if(!$rsPEB->EOF())
		{
			$AWIS_KEY1=$rsPEB->FeldInhalt('PEB_KEY');
		}
		else
		{
			$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPEB_KEY']);
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
		$SQL = 'SELECT PEB_KEY FROM (SELECT PEB_KEY ';
		$SQL .= ' FROM PersEinsBereiche';
		$SQL .= ' LEFT JOIN PersEinsBereicheMitglieder ON PBM_PEB_KEY = PEB_KEY';
		$SQL .= ' WHERE PEB_BEZEICHNUNG > '.$DB->FeldInhaltFormat('T',$_POST['txtPEB_BEZEICHNUNG']);
		if(($Recht4501&16)==0)		// Nur den eigenen Bereich anzeigen
		{
			$Bedingung .= ' AND (PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
		}
		$SQL .= ' ORDER BY PEB_BEZEICHNUNG ASC';
		$SQL .= ') WHERE ROWNUM = 1';
		$rsPEB = $DB->RecordSetOeffnen($SQL);
		if(!$rsPEB->EOF())
		{
			$AWIS_KEY1=$rsPEB->FeldInhalt('PEB_KEY');
		}
		else
		{
			$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPEB_KEY']);
		}
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./personaleinsaetze_Bereiche_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./personaleinsaetze_Bereiche_speichern.php');
		$Form->DebugAusgabe(1,$AWIS_KEY1);
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['PEB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PEB_KEY']);
	}
	elseif(isset($_POST['txtPEB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPEB_KEY']);
	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEB'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
		}

		if(isset($_GET['PEBListe']))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY PEB_Bezeichnung';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	$SQL = 'SELECT DISTINCT PersEinsBereiche.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM PersEinsBereiche';

	$Bedingung='';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PEB_KEY = '.floatval($AWIS_KEY1);
	}

	if(($Recht4501&16)==0)		// Nur den eigenen Bereich anzeigen
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM PersEinsBereicheMitglieder WHERE PBM_PEB_KEY = PEB_KEY';
		$Bedingung .= ' AND PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
	}

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1, $SQL);

	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		}
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}
	$rsPEB = $DB->RecordSetOeffnen($SQL);
$Form->DebugAusgabe(1, $SQL, $ZeilenProSeite );

	if($rsPEB->AnzahlDatensaetze()>1 AND $AWIS_KEY1==0)						// Liste anzeigen
	{
		$Form->SchreibeHTMLCode('<form name="frmPersEinsBereiche" action="./personaleinsaetze_Main.php?cmdAktion=Bereiche" method="POST"  enctype="multipart/form-data">');

		$Form->Formular_Start();

		$Form->ZeileStart();
		$Link = './personaleinsaetze_Main.php?cmdAktion=Bereiche'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=PEB_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEB_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEB']['PEB_BEZEICHNUNG'],250,'',$Link);
		$Link = './personaleinsaetze_Main.php?cmdAktion=Bereiche'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=PEB_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEB_BEMERKUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEB']['PEB_BEMERKUNG'],550,'',$Link);
		$Form->ZeileEnde();


		$PEBZeile=0;
		while(!$rsPEB->EOF())
		{
			$Form->ZeileStart();
			$Link = './personaleinsaetze_Main.php?cmdAktion=Bereiche&PEB_KEY='.$rsPEB->FeldInhalt('PEB_KEY').'';
			$Form->Erstelle_ListenFeld('PEB_BEZEICHNUNG',$rsPEB->FeldInhalt('PEB_BEZEICHNUNG'),0,250,false,($PEBZeile%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('PEB_BEMERKUNG',$rsPEB->FeldInhalt('PEB_BEMERKUNG'),0,550,false,($PEBZeile%2),'','');
			$Form->ZeileEnde();

			$rsPEB->DSWeiter();
			$PEBZeile++;
		}

		$Link = './personaleinsaetze_Main.php?cmdAktion=Bereiche';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();

		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		if(($Recht4501&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}
		$Form->SchaltflaechenEnde();

		$Form->SchreibeHTMLCode('</form>');
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();
		$Form->SchreibeHTMLCode('<form name=frmPersEinsBereiche action=./personaleinsaetze_Main.php?cmdAktion=Bereiche method=POST  enctype="multipart/form-data">');

		$AWIS_KEY1 = $rsPEB->FeldInhalt('PEB_KEY');

		$Param['KEY']=$AWIS_KEY1;

		$Form->Erstelle_HiddenFeld('PEB_KEY',$AWIS_KEY1);

		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./personaleinsaetze_Main.php?cmdAktion=Bereiche&PEBListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPEB->FeldInhalt('PEB_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsPEB->FeldInhalt('PEB_USERDAT')));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht4501&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEB']['PEB_BEZEICHNUNG'].':',150);
		$Form->Erstelle_TextFeld('PEB_BEZEICHNUNG',$rsPEB->FeldInhalt('PEB_BEZEICHNUNG'),50,350,$EditRecht);
		$AWISCursorPosition='txtPEB_BEZEICHNUNG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEB']['PEB_BEMERKUNG'].':',150);
		$Form->Erstelle_TextFeld('PEB_BEMERKUNG',$rsPEB->FeldInhalt('PEB_BEMERKUNG'),50,350,$EditRecht);
		$Form->ZeileEnde();

		$Form->Formular_Ende();

		if(!isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=0)
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Mitglieder'));
	//		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';
			$SubReg = new awisRegister(4501);
			$SubReg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:'Mitglieder'));
		}


		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();

		$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

		if(($Recht4501&6)!=0)		//
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		}

		if(($Recht4501&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}
		if(($Recht4501&8)==8 AND !isset($_POST['cmdDSNeu_x']))
		{
			$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
		}

		if(!isset($_POST['cmdDSNeu_x']))
		{
			$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
			$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');
		}

		$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=bereiche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_Hilfe'], 'H');

		$Form->SchaltflaechenEnde();

		$Form->SchreibeHTMLCode('</form>');
	}

	$AWISBenutzer->ParameterSchreiben('Formular_PEB',serialize($Param));
	//$Form->DebugAusgabe(1, $Param, $Bedingung, $rsPEB, $_POST, $rsPEB, $SQL, $AWISSprache);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>