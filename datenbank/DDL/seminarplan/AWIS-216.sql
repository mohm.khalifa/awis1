SET DEFINE OFF;
UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Folgende Platzhalter werden ersetzt: <br>
<br><br>
<b>#Vorname Nachname#</b> Vorname und Nachname des Teinehmers<br>
<b>#Seminartitel#</b> Titel des Seminars<br>
<b>#Datumvon#</b> Beginn des Seminars<br>
<b>#Datumbis#</b> Ende des Seminars<br>
<b>#KW#</b> Kalenderwoche des Seminars<br>
<b>#Schulungsort#</b> Ort (z.B. A.T.U Academy) des Seminars<br>
<b>#Region#</b> Region des Teilnehmers<br>
<b>#SET_KEY#</b> Key zum Datensatz (F�r Links)<br>
<b>#Strasse#</b> Stra�e des Seminars<br>
<b>#Ort#</b> Ort des Seminars<br>
<b>#PLZ#</b> Postleitzahl des Seminars<br>
<b>#LINK#</b> Link zur AWIS-Umgebung (Entwick, Staging, Prod) <br>
<b>#LINKAFB#</b> Link zur Anfahrtsbeschreibung <br>
<b>#LINKLZB#</b> Link zum Lernzielblatt <br>
<b>#LINKTH#</b> Link zum Hotel des Teilnehmers<br>
<br>

Leerzeilen m�ssen �ber das HTML-Tag "&lt;br&gt;" eingef�gt werden. ' WHERE XTX_KENNUNG = 'SEP_MAILERSETZUNG' AND XTX_BEREICH = 'SEP';

SET DEFINE OFF;
UPDATE MAILVERSANDTEXTE SET MVT_TEXT = '<div style="font-family: Arial; font-size: 10pt"> Sehr geehrte Damen und Herren, <br> <br>

anbei erhalten Sie die Reservierungsbest�tigung des Hotels zu Ihrem Seminar #Seminartitel# vom #Datumvon# bis #Datumbis# #Schulungsort#. Wir haben Ihnen gem�� Ihrer R�ckmeldung ein Zimmer mit Fr�hst�ck reserviert. <br> <br>

Wir bitten Sie diese Daten zu pr�fen. #LINKTH# <br> <br>

Sollte eine �nderung des Zeitraums notwendig sein oder Sie nicht am Seminar teilnehmen, bitten wir Sie dies direkt �ber AWIS - Seminarplan zu best�tigen oder nochmals anzupassen. <br> <br>

Sonntagsanreisen, die kurzfristig nicht angetreten werden k�nnen, bitten wir selbstst�ndig im Hotel abzusagen um hier "No-Show-Geb�hren" zu vermeiden. <br> <br>

F�r Fragen stehen wir Ihnen gerne zur Verf�gung. <br>
</div>' WHERE MVT_BEREICH = 'SET_HOTEL_BESTAETIGUNG';

SET DEFINE OFF;
UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = '#LINK#/seminarplan/seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&HOTEL_SET_KEY=#SEH_SET_KEY#' WHERE XTX_KENNUNG = 'mail_Link' AND XTX_BEREICH = 'SEH';

ALTER table SEMINARHOTEL ADD SEH_HOTELVON DATE;
ALTER table SEMINARHOTEL ADD SEH_HOTELBIS DATE;
ALTER table SEMINARHOTEL ADD SEH_ZUSATZINFO VARCHAR(4000);

COMMENT ON COLUMN SEMINARHOTEL.SEH_HOTELVON IS 'Datum und Zeit von dem das Hotel gebucht ist';
COMMENT ON COLUMN SEMINARHOTEL.SEH_HOTELBIS IS 'Datum und Zeit bis dem das Hotel gebucht ist';
COMMENT ON COLUMN SEMINARHOTEL.SEH_ZUSATZINFO IS 'Freitextfeld f�r Zusatzinfos';

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) VALUES ('SEH_HOTELVON','SEH','TextLabel f�r HotelVon','Reserviert von:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) VALUES ('SEH_HOTELBIS','SEH','TextLabel f�r HotelBis','Reserviert bis:');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) VALUES ('SEH_ZUSATZINFO','SEH','TextLabel f�r die Zusatzinfos','Zusatzinfos:');
SET DEFINE OFF;
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) VALUES ('SEH_FEHLERMAILINFOTEXT','SEH','Der Fehlertext, der als Infotext ausgegeben wird, wenn keine Email ermittelt wurde.','F�r den Teilnehmer #SEH_TEILNEHMER# konnte keine Emailadresse ermittelt werden. <br> Bitte manuell erfassen: ');