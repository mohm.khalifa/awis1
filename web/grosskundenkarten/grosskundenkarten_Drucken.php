<?php
global $Funktionen;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {

    if(isset($_POST['txtKUNDEN'])){
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Hinweistext($Funktionen->AWISSprachkonserven['ERM']['ERM_HINWEIS_ALSGEDRUCKTMARKIERT'],awisFormular::HINWEISTEXT_OK);
        $Funktionen->Form->ZeileEnde();

        //COM-Server-Update
        $SQL = 'UPDATE '.$Funktionen->holeKartenTabelle().' SET GEDRUCKT = 2, DRUCK_DATUM = sysdate';
        $SQL .= ' WHERE KUNDEN_NR IN (' . implode(',', $_POST["txtKUNDEN"]) . ') AND GEDRUCKT = 1';

        $Funktionen->DB->Ausfuehren($SQL, 'COM', true, $Funktionen->DB->Bindevariablen('COM', true));
    }
    $Funktionen->Form->Formular_Start();
    $Funktionen->Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./grosskundenkarten_Main.php?cmdAktion=Drucken>");

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['LBL_LAENDER'], 200);
    $Funktionen->Form->AuswahlSelectFeld('LAND', isset($_REQUEST['txtLAND'])?$_REQUEST['txtLAND']:'', 250, true, '', 'lst_ERM_Drucken', 'box1', ['txtLAND','sucSuchFeld'], '', '', '', explode('|', $Funktionen->AWISSprachkonserven['ERM']['LST_LAENDER_DRUCK']), 'onchange="lst_ERM_Drucken(this);"');

    $SQL  ='select * from (';
    $SQL  .='select';
    $SQL .='     ZAHL_ART,';
    $SQL .='     c.LAN_LAND,';
    $SQL .='     count(*) as Anz';
    $SQL .=' from';
    $SQL .= $Funktionen->holeKartenTabelle() .' a';
    $SQL .='     inner join'.$Funktionen->holeDestTabelle().' B on a.KUNDEN_NR = B.KUNDEN_NR';
    $SQL .='     inner join LAENDER c on B.GKB_STAAT = c.LAN_ATU_STAAT_NR';
    $SQL .=' where';
    $SQL .='     GEDRUCKT = 1';
    $SQL .= '    AND (a.NAME3 <> \'Mana Ara\' ';
    $SQL .= '    OR a.NAME3 is null) '; //Nur Suche nach 'nicht Mana Ara'
    $SQL .=' group by';
    $SQL .='     ZAHL_ART,';
    $SQL .='     LAN_LAND';
    $SQL .=') union (';
    $SQL .='select';
    $SQL .='    \'MA\',';
    $SQL .='    c.LAN_LAND,';
    $SQL .='    count(*) as Anz';
    $SQL .=' from';
    $SQL .= $Funktionen->holeKartenTabelle() .' a';
    $SQL .='     inner join'.$Funktionen->holeDestTabelle().' B on a.KUNDEN_NR = B.KUNDEN_NR';
    $SQL .='     inner join LAENDER c on B.GKB_STAAT = c.LAN_ATU_STAAT_NR';
    $SQL .=' where';
    $SQL .='     GEDRUCKT = 1 ';
    $SQL .=' and';
    $SQL .='    a.NAME3 = \'Mana Ara\'';
    $SQL .=' group by';
    $SQL .='     ZAHL_ART,';
    $SQL .='     LAN_LAND';
    $SQL .=')';

    $rsCount = $Funktionen->DB->RecordSetOeffnen($SQL);
    
    $Zahlarten['K'] = 'Unbar';
    $Zahlarten['G'] = 'Bar';
    $Zahlarten['MA'] = 'Mana Ara';

    while (!$rsCount->EOF()){

        $ZahlartText = isset($Zahlarten[$rsCount->FeldInhalt('ZAHL_ART')])?$Zahlarten[$rsCount->FeldInhalt('ZAHL_ART')]:$rsCount->FeldInhalt('ZAHL_ART');
        $Ausgabe = $rsCount->FeldInhalt('LAN_LAND').'(' .$ZahlartText . '): ' . $rsCount->FeldInhalt('ANZ');
        $Funktionen->Form->Erstelle_TextLabel($Ausgabe,strlen($Ausgabe)*10);
        $rsCount->DSWeiter();
    }

    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();

    $Funktionen->Form->Erstelle_TextLabel('Suche' .':',200);
    $Funktionen->Form->AuswahlBox('SuchFeld','box1','zielfeld','ERM_Drucken','txtLAND',100,10,isset($_REQUEST['sucSuchFeld'])?$_REQUEST['sucSuchFeld']:'','T',true,'','','','','','','','1');
    $Funktionen->Form->ZeileEnde();

    $Script = '<script>$( document ).ready(function(){ lst_ERM_Drucken(this) })</script>';
    $Funktionen->Form->SchreibeHTMLCode($Script);

    $Funktionen->Form->AuswahlBoxHuelle('box1');

    $Schaltflaechen = array(array('/bilder/cmd_weiter.png', 'cmdGedruckt', 'script', '','','print_ok'),array('/bilder/cmd_mann_laeuft.png', 'cmdGedruckt', 'script', '','','print_leave'),array('/bilder/cmd_dsloeschen.png', 'cmdGedruckt', 'script', '','','print_nok'));

    $Funktionen->Form->PopupDialog($Funktionen->AWISSprachkonserven['ERM']['ERM_POPUP_DRUCKBESTAETIGEN'],$Funktionen->AWISSprachkonserven['ERM']['ERM_POPUP_BITTEBESTAETIGEN'],$Schaltflaechen,awisFormular::POPUP_WARNUNG,'2');

    $Funktionen->Form->Formular_Ende();

    $Funktionen->Form->SchaltflaechenStart();
    $Funktionen->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_zurueck'], 'Z');


    $Funktionen->Form->Schaltflaeche('script', 'cmdPDF_IE', '', '/bilder/cmd_pdf.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_drucken'], 'W');


    $Funktionen->Form->SchaltflaechenEnde();

    $Funktionen->Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->DebugAusgabe(1, $ex->getSQL());
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Funktionen->Form instanceof awisFormular) {

        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}