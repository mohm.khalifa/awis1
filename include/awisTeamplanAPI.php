<?php
require_once 'awisWerkzeuge.inc';
require_once 'awisRest.inc';
require_once 'awisFormular.inc';
require_once 'awisBusinessLoggingPepAPI.php';
class awisTeamplanAPI
{

    const URL_EMPLOYEE_PRESENCE = 'https://dsp.atu.de/s105-employee-presence/v1';
    const URL_EMPLOYEE_PRESENCE_TEST = 'https://dst.atu.de/s105-employee-presence/v1';

    const URL_EMPLOYEE_MASTER_DATA = 'https://dsp.atu.de/s107-employee-master-data/v1/master-data/awis';
    const URL_EMPLOYEE_MASTER_DATA_TEST = 'https://dst.atu.de/s107-employee-master-data/v1/master-data/awis';

    const URL_EMPLOYEE_MASTER_DATA_LIST = 'https://dsp.atu.de/s107-employee-master-data/v1/master-data/awis';
    const URL_EMPLOYEE_MASTER_DATA_LIST_TEST = 'https://dst.atu.de/s107-employee-master-data/v1/master-data/awis';


    private $_URL_EMPLOYEE_PRESENCE = '';
    private $_URL_EMPLOYEE_MASTER_DATA = '';
    private $_URL_EMPLOYEE_MASTER_DATA_LIST = '';
    private $_awisRest;
    private $_awisWerkzeuge;
    private $_Form;
    private $awisBusinessLoggingAPI;
    private $aktuelleUniqueProcessId;

    function __construct()
    {
        $this->_awisRest = new awisRest('awisTeamplanAPI');
        $this->_awisWerkzeuge = new awisWerkzeuge();
        $this->_Form = new awisFormular();
        $this->initUrls();
        $this->awisBusinessLoggingAPI = new AWISBusinessLoggingPEPAPI();
    }

    private function initUrls(){
        $Level = $this->_awisWerkzeuge->awisLevel();

        //Wenn nicht Prod
        if($Level != awisWerkzeuge::AWIS_LEVEL_PRODUKTIV and $Level != awisWerkzeuge::AWIS_LEVEL_SHUTTLE){
            $this->_URL_EMPLOYEE_PRESENCE = self::URL_EMPLOYEE_PRESENCE_TEST;
            $this->_URL_EMPLOYEE_MASTER_DATA = self::URL_EMPLOYEE_MASTER_DATA_TEST;
            $this->_URL_EMPLOYEE_MASTER_DATA_LIST = self::URL_EMPLOYEE_MASTER_DATA_LIST_TEST;
        }else{
            $this->_URL_EMPLOYEE_PRESENCE = self::URL_EMPLOYEE_PRESENCE;
            $this->_URL_EMPLOYEE_MASTER_DATA = self::URL_EMPLOYEE_MASTER_DATA;
            $this->_URL_EMPLOYEE_MASTER_DATA_LIST = self::URL_EMPLOYEE_MASTER_DATA_LIST;
        }
    }

    public function AnwesenheitenEinerFiliale($FIL_ID, $DatumVon, $DatumBis){
        $Parameter = array();
        $Parameter['storeId'] = $FIL_ID;
        $Parameter['dateFrom'] = $this->DatumsFormat($DatumVon);
        $Parameter['dateTo'] = $this->DatumsFormat($DatumBis);;

        $Erg = $this->_awisRest->erstelleRequest($this->_URL_EMPLOYEE_PRESENCE . '/branchData','GET','JSON',$Parameter);

        if($Erg){
            return $this->_Presence2Array($Erg,$DatumVon, $DatumBis);
        }

        return false;
    }

    public function AnwesenheitenEinesMitarbeiters($PersNr, $DatumVon, $DatumBis){
        $Parameter = array();
        $Parameter['employeeId'] = $PersNr;
        $Parameter['dateFrom'] = $this->DatumsFormat($DatumVon);
        $Parameter['dateTo'] = $this->DatumsFormat($DatumBis);;

        $Erg = $this->_awisRest->erstelleRequest($this->_URL_EMPLOYEE_PRESENCE.'/presence','GET','JSON',$Parameter);

        return $Erg['data'];
    }

    private function _Presence2Array($EmployeePresenceArray, $DatumVon, $DatumBis){
        $Ma = array();
        $DatumVon = strtotime(explode('T',$DatumVon)[0]);
        $DatumBis = strtotime(explode('T',$DatumBis)[0]);

        foreach ($EmployeePresenceArray as $Presence){

            $PersNr = $Presence['employeeId'];

            $DatumAnwesenheit =date('Y-m-d',strtotime($Presence['date']));
            $tmpDatumVon = $DatumVon;
            while ($tmpDatumVon <= $DatumBis){
                if(strtotime($DatumAnwesenheit) == $tmpDatumVon ){
                    $Ma[$PersNr][$tmpDatumVon] = ['present' => $Presence['present'], 'presenceCode' => $Presence['presenceCode']];
                }

                $tmpDatumVon = $tmpDatumVon + (1*60*60*24);
            }
        }

        return $Ma;

    }

    public function MasterDataService($PersNr){
        $Parameter = array();
        $Parameter['employeeId'] = $PersNr;

        $Erg = $this->_awisRest->erstelleRequest($this->_URL_EMPLOYEE_MASTER_DATA,'GET','JSON',$Parameter);

        return $Erg;
    }

    public function MasterDataListService(){

        $BusinessKey = 'awis_import_' . date('ymdHis');
        $this->aktuelleUniqueProcessId = $this->awisBusinessLoggingAPI->ErstelleLogheader("AWIS-Request Employee-Masterdata",$BusinessKey);

        $this->awisBusinessLoggingAPI->ErstelleLogeintrag($this->aktuelleUniqueProcessId,'Get Employeedatalist','OK','Start Request',200);

        $Parameter = array();
        $Parameter['uniqueProcessId'] = $this->aktuelleUniqueProcessId;

        $Erg = $this->_awisRest->erstelleRequest($this->_URL_EMPLOYEE_MASTER_DATA_LIST,'GET','JSON',$Parameter);

        if(isset($Erg['masterDataAwisExportModels']) and count($Erg['masterDataAwisExportModels']) > 0){
            $this->awisBusinessLoggingAPI->ErstelleLogeintrag($this->aktuelleUniqueProcessId,'Get Employeedatalist','OK','Response proceeded. Count: ' . count($Erg['masterDataAwisExportModels']),200);
            return $Erg['masterDataAwisExportModels'];
        }else{
            $this->awisBusinessLoggingAPI->ErstelleLogeintrag($this->aktuelleUniqueProcessId,'Get Employeedatalist','ERROR','Response: ' . json_encode($Erg) ,200);
            return false;
        }

    }

    private function DatumsFormat($Datum){
        $Formatiert = $this->_Form->Format('DI',$Datum);

        if($Formatiert and strpos($Formatiert,'T')){
            return explode('T',$Formatiert)[0];
        }

        return $Datum;
    }

}