<html>
<head>

<title>Awis 1.0 - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
//var_dump($_POST);

global $awisRSZeilen;
global $DSNr;

$Zeit = time();

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,120);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Dienstleistungspreis',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$rsDPR = awisOpenRecordset($con,'SELECT MAX(DPR_UserDat) AS Datum FROM DienstleistungsPreise');
$LetzterDatensatz = $rsDPR['DATUM'][0];
unset($rsDPR);

if(isset($_POST['txtBezeichnung']))
{
	$Params[0] = $_POST['txtAuswahlSpeichern'];
	$Params[1] = (isset($_POST['txtPreis_DE'])?$_POST['txtPreis_DE']:'');
	$Params[2] = (isset($_POST['txtPreis_AT'])?$_POST['txtPreis_AT']:'');
	$Params[3] = $_POST['txtBezeichnung'];
	
	awis_BenutzerParameterSpeichern($con, "Dienstleistunspreis_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
}
else
{
	$Params = explode(';',awis_BenutzerParameter($con, "Dienstleistunspreis_Suche", $AWISBenutzer->BenutzerName()));
}

echo '<form name=frmSuche method=post action=./dienstleistungspreise_Main.php>';

echo '<table width=100% border=1><tr><td id=RegisterInhalt>';	// Tabelle f�r die Farbe
echo '<h3>Dienstleistungspreise suchen</h3></td></tr>';
echo "<tr><td id=RegisterInhalt align=right><font size=2>Daten vom $LetzterDatensatz</font></td></tr>";
echo '<tr><td id=RegisterInhalt>';

echo '<br><table>';

echo '<tr>';
echo '<td>Preis f�r Deutschland</td>';
echo '<td><input tabindex=20 type=checkbox name=txtPreis_DE value=on' . ($Params[0]=='on'?($Params[1]=='on'?' checked':''):'') .'></td>';
echo '</tr>';
echo '<tr>';
echo '<td>Preis f�r �sterreich</td>';
echo '<td><input tabindex=21 type=checkbox name=txtPreis_AT value=on' . ($Params[0]=='on'?($Params[2]=='on'?' checked':''):'') .'></td>';
echo '</tr>';

echo '<tr>';
echo '<td colspan=2><hr></td>';
echo '</tr>';

echo '<tr>';
echo '<td>D<u>i</u>enstleistung</td>';
echo "<td><input accesskey=I tabindex=25 size=30 type=text name=txtBezeichnung value=\"" . ($Params[0]=='on'?$Params[3]:'') ."\"></td>";
echo '</tr>';

echo '<tr>';
echo '<td>S<u>o</u>rtierung</td>';
echo '<td><select name=txtSort>';
echo '<option ' . ($_POST['txtSort']=='DPR_Bezeichnung'?'Selected':'') . ' value=DPR_Bezeichnung>Bezeichnung</option>';
echo '<option ' . ($_POST['txtSort']=='DPR_Nummer'?'Selected':'') . ' value=DPR_Nummer>Nummer</option>';
echo '</select></td>';
echo '</tr>';

echo '<tr>';
echo '<td colspan=2><hr></td>';
echo '</tr>';

print "<tr><td>Auswahl <u>s</u>peichern</td><td><input tabindex=50 type=checkbox value=on " . ($Params[0]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=11></td></tr>";

echo '</table>';

print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo '&nbsp;&nbsp;&nbsp;<b style="color:red">ROT = Artikel gesperrt</b>';
echo '</td>';
echo '</tr></table>';


/***********************************************************************************
* 	Daten anzeigen
***********************************************************************************/
	
if(isset($_POST['txtBezeichnung']))
{
	$Params[0] = $_POST['txtAuswahlSpeichern'];
	$Params[1] = (isset($_POST['txtPreis_DE'])?$_POST['txtPreis_DE']:'');
	$Params[2] = (isset($_POST['txtPreis_AT'])?$_POST['txtPreis_AT']:'');
	$Params[3] = $_POST['txtBezeichnung'];

	// ############### SQL-Statment geandert durch CA am 15.03.06 ############################
	
	//$SQL = 'SELECT * FROM DienstleistungsPreise';
	
	/*$SQL = "select DPR_NUMMER, DPR_BEZEICHNUNG, DPR_VK, DPF_FIL_ID, DPF_VK from Dienstleistungspreise, DienstleistungspreiseFilialen
			WHERE (Dienstleistungspreise.DPR_NUMMER = DienstleistungspreiseFilialen.DPF_DPR_NUMMER
			AND DPR_KENN <> 'S'
			AND ((DienstleistungspreiseFilialen.DPF_FIL_ID = 1 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'O') OR
			(DienstleistungspreiseFilialen.DPF_FIL_ID = 231 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)='O')))";
	*/
	$SQL = "select DPR_NUMMER, DPR_BEZEICHNUNG, DPR_VK, DPR_KENN, DPF_FIL_ID, DPF_VK from Dienstleistungspreise, DienstleistungspreiseFilialen
			WHERE (Dienstleistungspreise.DPR_NUMMER = DienstleistungspreiseFilialen.DPF_DPR_NUMMER
			)";
	
	//AND (DPR_KENN IS NULL OR DPR_KENN <> 'S') CA - 05.11.07 entfernt => Gesperrte werden Rot angezeigt
	
	$Bedingung = '';
	If($Params[1]!='on' OR $Params[2]!='on')
	{
		If($Params[1]=='on')	// Deutsche Artikel
		{
			//$Bedingung .= " AND DPR_Nummer NOT LIKE 'O%'";
			$Bedingung .= " AND (DienstleistungspreiseFilialen.DPF_FIL_ID = 1 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'O')";
			$Bedingung .= " AND (DienstleistungspreiseFilialen.DPF_FIL_ID = 1 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'C')";
		}
		elseIf($Params[2]=='on')	// �sterreichische Artikel
		{
			//$Bedingung .= " AND DPR_Nummer LIKE 'O%'";
			$Bedingung .= " AND (DienstleistungspreiseFilialen.DPF_FIL_ID = 235 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'O')";
			$Bedingung .= " AND (DienstleistungspreiseFilialen.DPF_FIL_ID = 235 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'C')";
		}
		else 
		{
			$Bedingung .= " AND ((DienstleistungspreiseFilialen.DPF_FIL_ID = 1 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'O')";
			$Bedingung .= " OR (DienstleistungspreiseFilialen.DPF_FIL_ID = 235 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'O'))";
			$Bedingung .= " AND ((DienstleistungspreiseFilialen.DPF_FIL_ID = 1 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'C')";
			$Bedingung .= " OR (DienstleistungspreiseFilialen.DPF_FIL_ID = 235 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'C'))";
		}
	}
	else{ // Hinzugefuegt am 15.03.06 CA
		$Bedingung .= " AND ((DienstleistungspreiseFilialen.DPF_FIL_ID = 1 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'O')";
		$Bedingung .= " OR (DienstleistungspreiseFilialen.DPF_FIL_ID = 235 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'O'))";
		$Bedingung .= " AND ((DienstleistungspreiseFilialen.DPF_FIL_ID = 1 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'C')";
		$Bedingung .= " OR (DienstleistungspreiseFilialen.DPF_FIL_ID = 235 AND SUBSTR(Dienstleistungspreise.DPR_NUMMER,1,1)<>'C'))";
	}
	If($Params[3]!='')	// Bezeichnung
	{
		$Bedingung .= " AND (DPR_Bezeichnung " . awisLIKEoderIST('%' . $Params[3] . '%',true,false,false,false);
		$Bedingung .= " OR DPR_Nummer " . awisLIKEoderIST('%' . $Params[3] . '%',true,false,false,false);
		$Bedingung .= ")";
	}

	$SQL .= ($Bedingung!=''?' AND ' . substr($Bedingung,4):'');
	
	$SQL .= ' ORDER BY ' . $_POST['txtSort'];
	
	awis_Debug(1,$SQL);

	$rsDPR = awisOpenRecordset($con, $SQL)	;
	$rsDPRZeilen = $awisRSZeilen;
	
	echo '<table width=100%>';
	echo '<colspec><col width=100><col width=*><col width=90><col width=70></colspec>';
	echo '<tr><td id=FeldBez>';	// Tabelle f�r die Farbe
	echo 'Nummer</td>';
	echo '<td id=FeldBez>Dienstleistung</td>';
	echo '<td id=FeldBez>Preis</td><td id=FeldBez>Referenz</td><tr>';

	for($DSNr=0;$DSNr<$rsDPRZeilen;$DSNr++)
	{
		if ($rsDPR['DPR_KENN'][$DSNr]!='S')
		{
			$font_color='style="color:black"';
		}else{
			$font_color='style="color:red"';
		}	
		
		
		echo '<tr>';
		echo '<td ' . (($DSNr%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . ' '.$font_color.'>';
		echo $rsDPR['DPR_NUMMER'][$DSNr];
		echo '</td>';

		echo '<td ' . (($DSNr%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . ' '.$font_color.'>';
		echo $rsDPR['DPR_BEZEICHNUNG'][$DSNr];
		echo '</td>';

		echo '<td align=right ' . (($DSNr%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . ' '.$font_color.'>';
		if($rsDPR['DPF_VK'][$DSNr]==0)
		{
			echo '--';
		}
		else
		{
			echo awis_format($rsDPR['DPF_VK'][$DSNr],'Currency');
		}
		echo '</td>';

		echo '<td align=right ' . (($DSNr%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . ' '.$font_color.'>';
		echo $rsDPR['DPF_FIL_ID'][$DSNr];
		echo '</td>';
		
		echo '<tr>';		

	}

	echo '</table>';

}

echo '</form>';
$Zeit = time()-$Zeit;
if($DSNr>0)
{
	echo "<br><font size=1>$DSNr Zeilen in " . $Zeit . " Sekunden gefunden.</font>";
}

print "<br><hr><input tabindex=500 accesskey=z type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
print "&nbsp;<input tabindex=501 type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=dienstleistungspreise','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awislogoff($con);



// Nicht, wenn Hilfe ge�ffnet wird
if(!isset($_POST['cmdHilfe_x']))
{
	echo '<Script Language=JavaScript>';
	print "document.getElementsByName(\"txtBezeichnung\")[0].focus();";
	echo '</Script>';
}

?>
</body>
</html>

