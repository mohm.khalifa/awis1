<?php

require_once 'grosskundenkarten_funktionen.php';

try {
    $Funktionen = new grosskundenkarten_funktionen();

    $SQL = 'UPDATE ' . $Funktionen->holeKartenTabelle() . ' SET GEDRUCKT = 2, DRUCK_DATUM = sysdate';
    $SQL .= ' WHERE KUNDEN_NR IN (' . $Funktionen->DB->WertSetzen('ERM', 'Z', $_REQUEST['KDNR']) . ') AND GEDRUCKT = 1';
    
    $Funktionen->DB->Ausfuehren($SQL, '', true, $Funktionen->DB->Bindevariablen('ERM'));

    echo "ok";
} catch (awisException $e) {
    echo "Fehler";
} catch (Exception $e) {
    echo "Fehler";
}