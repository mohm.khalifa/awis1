<?php
require_once 'schnittstellendaten_funktionen.inc';

try
{
    $VGD = new schnittstellendaten_funktionen();
    $Speichern = isset($VGD->Param['SPEICHERN']) && $VGD->Param['SPEICHERN'] == 'on'?true:false;

    $VGD->RechteMeldung(0);//Anzeigen Recht
    echo "<form name=frmSuche method=post action=./schnittstellendaten_Main.php?cmdAktion=Details>";

    $VGD->Form->Formular_Start();

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_KFZ_KZ'].':',190);
    $VGD->Form->Erstelle_TextFeld('*VGD_KFZ_KZ',($Speichern?$VGD->Param['VGD_KFZ_KZ']:''),10,110,true);
    $VGD->Form->ZeileEnde();

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['Wort']['Quelle'] . ':', 190);
    if (($VGD->Recht69000 & 8) == 8) {
        $Daten = explode('|', $VGD->AWISSprachKonserven['Liste']['lst_Deckungsquellen']);
    } else {
        $Daten = explode('|', $VGD->AWISSprachKonserven['Liste']['lst_Deckungsquellen']);
        unset($Daten[1]); // Direktvermittlungen d�rften nicht bearbeitet werden
    }

    $VGD->Form->Erstelle_SelectFeld('*VGD_QUELLE', ($Speichern?$VGD->Param['VGD_QUELLE']:''), 400, true, '', '', '', '', '', $Daten);
    $VGD->Form->ZeileEnde();

    // Auswahl kann gespeichert werden
    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 190);
    $VGD->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $VGD->Form->ZeileEnde();

    $VGD->Form->Formular_Ende();

    $VGD->Form->SchaltflaechenStart();
    $VGD->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$VGD->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
    $VGD->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $VGD->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $VGD->Form->SchaltflaechenEnde();

    $VGD->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
    if($VGD->Form instanceof awisFormular)
    {
        $VGD->Form->DebugAusgabe(1, $ex->getSQL());
        $VGD->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
    }
    else
    {
        $VGD->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($VGD->Form instanceof awisFormular)
    {
        $VGD->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>