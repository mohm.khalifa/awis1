<?php
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;

try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_SCH"));
    
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('SCH', '%');
    $TextKonserven[]=array('Wort', 'lbl_zurueck');
    $TextKonserven[]=array('Wort', 'lbl_suche');
    $TextKonserven[]=array('Wort', 'lbl_trefferliste');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Fehler', 'err_keineDaten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht25040=$AWISBenutzer->HatDasRecht(25040);
                                         //                            _Speichern.php>";
    echo "<form name=frmDetails method=post action=./schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht>";

    if($Recht25040 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    $Fehler = '';
    if(isset($_POST['cmdSuche_x']))
    {
        $Param = array();
        $Param['KEY']=0;			

        $Param['SCH_Key']=$_POST['sucSCH_Key'];	
        $Param['TUR_Key']=$_POST['sucTUR_Key'];	
        $Param['KON_Key']=$_POST['sucKON_Key'];	
        
        $Param['SCH_Historie']=(isset($_POST['sucSCH_Historie'])?'on':'');
        $Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

        $Param['ORDERBY']='';
        $Param['BLOCK']='';
        $Param['KEY']=0;	
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('schluesselzentrale_Speichern.php');
    }
    elseif (isset($_POST['cmdDSNeu_x']))
    {
        $Param['SCH_Key'] = '';
        $Param['TUR_Key'] = '';	    
        $Param['KON_Key'] = '';
        
        $AWIS_KEY1 = -1;
    }
    if(!isset($_GET['Sort']))
    {        
        if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
        {
                $ORDERBY = $Param['ORDERBY'];
        }
        else
        {
            if (isset($Param['KON_Key']))
            {
                $ORDERBY = ' SCH_Bezeichnung';
            }
            elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1)
            {
                $ORDERBY = ' KON_Name1';
            }
        }		
    }
    else
    {
        $ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
    }
    
    $Form->SchreibeHTMLCode('<form name=frmschluesselzentral_Details action=./schluesselzentrale_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
    
    //Infozeile mit Daten zur letzten �nderung durch einen Mitarbeiter und Button zur�ck zur Suchseite
    $Felder = array();
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./schluesselzentrale_Main.php?cmdAktion=Suche' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=T title=' . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0? $rsUNF->FeldInhalt('UNF_USER'): $AWISBenutzer->BenutzerName()));	// bei Edit => User aus DB; bei New => aktUser 
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0? $rsUNF->FeldInhalt('UNF_USERDAT'): date('d.m.Y H:i:s')));			// bei Edit => Dat aus DB; bei New => aktDat
    $Form->InfoZeile($Felder, '');
    
    if ($Param['KON_Key']!=0)
    {
        $SQL = 'SELECT DISTINCT sch.SCH_Bezeichnung as BEZ, sgr.SGR_Bezeichnung, tur.TUR_Bezeichnung, ';
        $SQL.= '\'Bauteil \' ||tur.TUR_Bauteil || \', Stockwerk \' || tur.TUR_Stockwerk || \', B�ro \' || tur.TUR_Raum, mas.MAS_Ausgabedatum';
        if ($Param['SCH_Historie']== 'on')
        {
            $SQL.= ', mas.MAS_Rueckgabedatum, mas.MAS_Verlustdatum';
        }
        $SQL.= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
        $SQL.= ' FROM SchlSchluessel sch';
        $SQL.= ' Join SchlSchluesselgruppen sgr on sch.SCH_SGR_Key = sgr.SGR_Key';
        $SQL.= ' Join SchlTueren tur on sch.SCH_TUR_Key = tur.TUR_Key';
        $SQL.= ' Join SchlMitarbeiterSchluessel mas on mas.MAS_SCH_Key = sch.SCH_Key';
        $SQL.= ' Join Kontakte kon on mas.MAS_KON_Key = kon.KON_Key';
        $SQL.= ' WHERE kon.KON_Key = '.($Param['KON_Key']);
        if (!$Param['SCH_Historie']== 'on')
        {
            $SQL.=' AND mas.MAS_Verlustdatum is null ' ;
            $SQL.=' AND mas.MAS_Rueckgabedatum is null ';
        }
        
        $SQL_Mitarbeiter = 'SELECT KON_NAME1 || \', \' || KON_NAME2 AS Mitarbeiter FROM Kontakte kon, SchlMitarbeiterSchluessel mas ';
        $SQL_Mitarbeiter.= 'WHERE kon.KON_Key = mas.MAS_KON_Key AND kon.KON_Key = '.($Param['KON_Key']);

        $SQL_Abteilung = 'SELECT KAB_Abteilung FROM KontakteAbteilungen kab, Kontakte kon, KontakteAbteilungenZuordnungen kza ';
        $SQL_Abteilung.= 'WHERE kza.KZA_KON_Key = kon.KON_Key AND kza.KZA_KAB_Key = kab.KAB_Key AND KZA_KAB_Key != 55 AND ';
        $SQL_Abteilung.= 'kon.KON_Key = '.($Param['KON_Key']);
        
        $SQL_Telefon = 'SELECT KKO_WERT FROM Kontakte kon, Kontaktekommunikation kko ';
        $SQL_Telefon.= 'WHERE kon.KON_Key = kko.KKO_KON_Key AND kko.KKO_KOT_Key = 9 AND kon.KON_Key = '.($Param['KON_Key']);
    }
    elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1)
    {
        $SQL = 'SELECT kon.KON_NAME1, kon.KON_NAME2, mas.MAS_Ausgabedatum, kab.KAB_Abteilung, mas.MAS_Bemerkung, kko.KKO_WERT';
        if ($Param['SCH_Historie']== 'on')
        {
            $SQL.= ', mas.MAS_Rueckgabedatum, mas.MAS_Verlustdatum ';
        }
        $SQL.= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
        $SQL.= ' FROM SchlMitarbeiterSchluessel mas Join Kontakte kon on mas.MAS_KON_Key = kon.KON_Key';
        $SQL.= ' Join SchlSchluessel sch on mas.MAS_SCH_Key = sch.SCH_Key';
        $SQL.= ' Join SchlSchluesselGruppen sgr on sch.SCH_SGR_Key = sgr.SGR_Key';
        $SQL.= ' Join SchlTueren tur on sch.SCH_TUR_Key = tur.Tur_Key';
        $SQL.= ' Join KontakteAbteilungenZuordnungen kza on kza.KZA_KON_Key = kon.KON_Key';
        $SQL.= ' Join KontakteAbteilungen kab on kza.KZA_KAB_KEY = kab.KAB_Key';
        $SQL.= ' Join Kontaktekommunikation kko on kon.KON_Key = kko.KKO_KON_Key';
        $SQL.= ' WHERE KZA_KAB_Key != 55';
        $SQL.= ' AND kko.KKO_KOT_Key = 9';
        if (!$Param['SCH_Historie']== 'on')
        {
            $SQL.=' AND mas.MAS_Verlustdatum is null ' ;
            $SQL.=' AND mas.MAS_Rueckgabedatum is null ';
        }

        if (is_numeric($Param['SCH_Key'])==1)
        {
            $SQL.= ' AND sch.SCH_Key = '.$Param['SCH_Key'];
        }
        elseif (is_numeric($Param['TUR_Key'])==1)
        {
            $SQL.= ' AND sch.SCH_TUR_Key = '.$Param['TUR_Key'];
        }    

        $SQLReserve = 'Select sch.SCH_Anzahl - count(mas.MAS_SCH_Key) AS Reserve';
        $SQLReserve.= ' FROM SchlMitarbeiterSchluessel mas JOIN SchlSchluessel sch on mas.MAS_SCH_Key = sch.SCH_Key AND ';
        if (is_numeric($Param['SCH_Key'])==1)
        {
            $SQLReserve.= ' sch.SCH_Key = '.($Param['SCH_Key']);
        }
        elseif (is_numeric($Param['TUR_Key'])==1)
        {
            $SQLReserve.= '  sch.SCH_TUR_Key = '.($Param['TUR_Key']);
        }
        $SQLReserve.=' AND mas.MAS_Rueckgabedatum is null ' ;
        $SQLReserve.= ' GROUP BY sch.SCH_Anzahl';
        
        $SQLVerlust = 'SELECT count(mas.MAS_SCH_Key) AS Verlust';
        $SQLVerlust.= ' FROM SchlMitarbeiterSchluessel mas JOIN SchlSchluessel sch on mas.MAS_SCH_Key = sch.SCH_Key';
        $SQLVerlust.= ' WHERE mas.MAS_Verlustdatum is not null';
        $SQLVerlust.= ' AND mas.MAS_Ausgabedatum is not null';
        $SQLVerlust.= ' AND mas.MAS_Rueckgabedatum is null AND ';
        if (is_numeric($Param['SCH_Key'])==1)
        {
            $SQLVerlust.= ' sch.SCH_Key = '.($Param['SCH_Key']);
        }
        elseif (is_numeric($Param['TUR_Key'])==1)
        {
            $SQLVerlust.= '  sch.SCH_TUR_Key = '.($Param['TUR_Key']);
        }
        
        $SQL_Sch = 'SELECT sch.SCH_Bezeichnung, sgr.SGR_Bezeichnung, tur.TUR_Bezeichnung, ';
        $SQL_Sch.= 'tur.TUR_Bauteil, tur.TUR_Stockwerk, tur.TUR_Raum, sch.SCH_Anzahl ';
        $SQL_Sch.= 'FROM SchlSchluessel sch Join SchlSchluesselgruppen sgr on sch.SCH_SGR_Key = sgr.SGR_Key ';
        $SQL_Sch.= '                        Join SchlTueren tur on sch.SCH_TUR_Key = tur.TUR_Key ';
        $SQL_Sch.= '                        Join SchlMitarbeiterSchluessel mas on mas.MAS_SCH_Key = sch.SCH_Key ';
        $SQL_Sch.= '                        Join Kontakte kon on mas.MAS_KON_Key = kon.KON_Key ';
        $SQL_Sch.= 'WHERE ';
        if (!$Param['SCH_Historie']== 'on')
        {
            $SQL_Sch.=' mas.MAS_Verlustdatum is null AND ' ;
            $SQL_Sch.=' mas.MAS_Rueckgabedatum is null AND ';
        }
        if (is_numeric($Param['SCH_Key'])==1)
        {
            $SQL_Sch.= 'sch.SCH_Key = '.($Param['SCH_Key']);
        }
        elseif (is_numeric($Param['TUR_Key'])==1)
        {
            $SQL_Sch.= ' sch.SCH_TUR_Key = '.($Param['TUR_Key']);
        } 
        
        $SQL_Details = 'SELECT sch.SCH_Bezeichnung, sgr.SGR_Bezeichnung, tur.TUR_Bezeichnung, ';
        $SQL_Details.= 'tur.TUR_Bauteil, tur.TUR_Stockwerk, tur.TUR_Raum, sch.SCH_Anzahl ';
        $SQL_Details.= 'FROM SchlSchluessel sch Join SchlSchluesselgruppen sgr on sch.SCH_SGR_Key = sgr.SGR_Key ';
        $SQL_Details.= '                        Join SchlTueren tur on sch.SCH_TUR_Key = tur.TUR_Key ';
        $SQL_Details.= '                        Join SchlMitarbeiterSchluessel mas on mas.MAS_SCH_Key = sch.SCH_Key ';
        $SQL_Details.= '                        Join Kontakte kon on mas.MAS_KON_Key = kon.KON_Key WHERE ';
        if (is_numeric($Param['SCH_Key'])==1)
        {
            $SQL_Details.= 'sch.SCH_Key = '.($Param['SCH_Key']);
        }
        elseif (is_numeric($Param['TUR_Key'])==1)
        {
            $SQL_Details.= ' sch.SCH_TUR_Key = '.($Param['TUR_Key']);
        } 
 
        $SQLAusgabe = 'SELECT count(mas.MAS_SCH_Key) As Ausgegeben';
        $SQLAusgabe.= ' FROM SchlMitarbeiterSchluessel mas JOIN SchlSchluessel sch on mas.MAS_SCH_Key = sch.SCH_Key AND ';
        $SQLAusgabe.=' mas.MAS_Verlustdatum is null AND ' ;
        $SQLAusgabe.=' mas.MAS_Rueckgabedatum is null AND ';
        if (is_numeric($Param['SCH_Key'])==1)
        {
            $SQLAusgabe.= 'sch.SCH_Key = '.($Param['SCH_Key']);
        }
        elseif (is_numeric($Param['TUR_Key'])==1)
        {
            $SQLAusgabe.= ' sch.SCH_TUR_Key = '.($Param['TUR_Key']);
        } 
        
        $rsSchl = $DB->RecordSetOeffnen($SQL_Sch);
        $rsReserve = $DB->RecordSetOeffnen($SQLReserve);
        $rsVerlust = $DB->RecordSetOeffnen($SQLVerlust);
        $rsAusgabe = $DB->RecordSetOeffnen($SQLAusgabe);  
        $rsDetails = $DB->RecordSetOeffnen($SQL_Details);
    }
    elseif (isset($_POST['cmdDSNeu_x']))   
    {
        
    }
    else
    {
        // Wenn kein Parameter ausgew�hlt wurde
        $Fehler .= $AWISSprachKonserven['Fehler']['err_keineDaten'].'<br>';
        die('<span class=HinweisText>'.$Fehler.'</span>');
        
    }
    if($Param['KON_Key']!=0)
    {
        // AWIS-Framework Seitenumschalterei bei Listen - Standardvorgehensweise
        $Block = 1;
        $StartZeile = '';
        $MaxDS = '';
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

        $SQL = 'SELECT * FROM ('.$SQL.') WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr <'.($StartZeile+$ZeilenProSeite);

        $rsPEI  = $DB->RecordSetOeffnen($SQL);
        $rsMitarbeiter = $DB->RecordSetOeffnen($SQL_Mitarbeiter);
        $rsAbteilung = $DB->RecordSetOeffnen($SQL_Abteilung);
        $rsTelefon = $DB->RecordSetOeffnen($SQL_Telefon);
        $Form->Formular_Start();

        // Zum Bl�ttern in den Daten
        if (isset($_REQUEST['Block']))
        {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        }

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Mitarbeiter'].':',100);
        $Form->Erstelle_TextFeld('Mitarbeiter', $rsMitarbeiter->Feldinhalt('1'), '', 300);
        $Form->Erstelle_HiddenFeld('KON_Key', $Param['KON_Key']);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Abteilung'].':',100);
        $Form->Erstelle_TextFeld('Abteilung', $rsAbteilung->FeldInhalt('1'), '', 300);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Telefon'].':',100);
        $Form->Erstelle_TextFeld('Telefon', '- '.$rsTelefon->FeldInhalt('1'), '', 300);
        $Form->ZeileEnde();
        
        $Form->Trennzeile();
        
        //Erstelle Unterregister
        $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
        $SubReg = new awisRegister(25001);
        $SubReg->ZeichneRegister($RegisterSeite);  
        
        //Schaltfl�chen
        $Form->SchaltflaechenStart();

        if ($Recht25040 >= 0)
        {
             // Zur�ck zum Men�
            $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

            //neuen DS anlegen
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['SCH']['SCH_Anlegen']);
        }
        $Form->SchaltflaechenEnde();
        }

    elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1)
    {
        // AWIS-Framework Seitenumschalterei bei Listen - Standardvorgehensweise
        $Block = 1;
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

        $SQL = 'SELECT * FROM ('.$SQL.') WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr <'.($StartZeile+$ZeilenProSeite);
                
        $rsPEI  = $DB->RecordSetOeffnen($SQL);
        $Form->Formular_Start();

        // Zum Bl�ttern in den Daten
        if (isset($_REQUEST['Block']))
        {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        }
        $BreiteTL_vorne = 120;
        $BreiteTL_hinten = 100;
        $BreiteTF_vorne = 300;
        $BreiteTF_hinten = 15;
        
        $EditRecht = true;
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_SchluesselNr'].':',$BreiteTL_vorne);
        
        $Form->Erstelle_TextFeld('SchluesselNr', $rsDetails->Feldinhalt('1'), 40, $BreiteTF_vorne, $EditRecht,'','','', $Format='T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Anzahl'].':',$BreiteTL_hinten);
        $Form->Erstelle_TextFeld('Anzahl', $rsDetails->Feldinhalt('7'), 1, $BreiteTF_hinten, $EditRecht,'','','', $Format='N0');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Schliessgruppe'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('Schliessgruppe', $rsDetails->FeldInhalt('2'), 40, $BreiteTF_vorne, $EditRecht,'','','', $Format='T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Ausgegeben'].':',$BreiteTL_hinten);
        $Form->Erstelle_TextFeld('Ausgegeben', $rsAusgabe->Feldinhalt('1'), '', $BreiteTF_hinten);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Tuer'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('TuerBezeichnung', $rsDetails->FeldInhalt('3'), 40, $BreiteTF_vorne, $EditRecht,'','','', $Format='T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Verlust'].':',$BreiteTL_hinten);
        $Form->Erstelle_TextFeld('Tuer', $rsVerlust->FeldInhalt('1'), '', $BreiteTF_hinten);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_TuerBemerkung'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('TuerBemerkung', 'Bauteil '.$rsDetails->FeldInhalt('4').', Stockwerk '.$rsDetails->FeldInhalt('5').', B�ro '.$rsDetails->FeldInhalt('6'), 40, $BreiteTF_vorne, true,'','','', $Format='T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Reserve'].':',$BreiteTL_hinten);
        $Form->Erstelle_TextFeld('Reserve', $rsReserve->FeldInhalt('1'), '', $BreiteTF_hinten);
        $Form->Erstelle_HiddenFeld('TuerBauteil', $rsDetails->FeldInhalt('4'));
        $Form->Erstelle_HiddenFeld('TuerStockwerk', $rsDetails->FeldInhalt('5'));
        $Form->Erstelle_HiddenFeld('TuerBuero', $rsDetails->FeldInhalt('6'));
        $Form->ZeileEnde();
        
        $Form->Trennzeile();
        
        //Erstelle Unterregister
        $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
        $SubReg = new awisRegister(25001);
        $SubReg->ZeichneRegister($RegisterSeite);  
        
        //Schaltfl�chen
        $Form->SchaltflaechenStart();

        if ($Recht25040 >= 0)
        {
             // Zur�ck zum Men�
            $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

            //neuen DS anlegen
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['SCH']['SCH_Anlegen']);
            
            //�nderung speichern
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['SCH']['SCH_Speichern']);
        }
        $Form->SchaltflaechenEnde();
    }
    elseif (isset($_POST['cmdDSNeu_x']))   
    {
        $BreiteTL_vorne = 120;
        $BreiteTL_hinten = 100;
        $BreiteTF_vorne = 300;
        $BreiteTF_hinten = 15;

        $EditRecht = true;
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_SchluesselNr'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('SchluesselNr', '', '20', 100, $EditRecht,'','','', $Format='T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Schliessgruppe'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('Schliessgruppe', '', '20', 100, $EditRecht,'','','', $Format='T');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Tuer'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('TuerBezeichnung', '', '50', 100, $EditRecht,'','','', $Format='T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Bauteil'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('TuerBauteil', '', '50', 100, $EditRecht,'','','', $Format='T');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Stockwerk'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('TuerStockwerk', '', '50', 100, $EditRecht,'','','', $Format='T');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Buero'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('TuerBuero', '', '50', 100, $EditRecht,'','','', $Format='T');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Anzahl'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('Anzahl', '', '20', 100, $EditRecht,'','','', $Format='N0');
        $Form->ZeileEnde();
        
        $Form->SchaltflaechenStart();

        if ($Recht25040 >= 0)
        {
             // Zur�ck zum Men�
            $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

            //DS speichern
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['SCH']['SCH_Speichern']);
        }
        $Form->SchaltflaechenEnde();
    }
    else
    {
        echo var_dump($_POST);
    }

    $Form->SchreibeHTMLCode("<form name=frmDetails_Listenansicht method=post action='./schluesselzentrale_Details_Bearbeiten.php'>");
    
    $Form->SchreibeHTMLCode('</form>');
    $Form->SetzeCursor($AWISCursorPosition);
   
    $AWISBenutzer->ParameterSchreiben("Formular_SCH",serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>