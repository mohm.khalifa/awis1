<?php

// feste Parameter:
//
// alle:
//
// text 					(Beschriftung, Vorgabetext)
// hoehe
// breite
//
// Listenfeld (select):
//
// optionsarray				(Array f�r optionen)
// optionstextfeld			(Feldname des Optionarrays)
// optionsidfeld			(Feldname des Optionsarrays)
// optionsvorauwsahlwert	(Wert des Kriteriums f�r Vorauswahl)
// optionsvorauswahlfeld	(Feldname f�r Kriteriumsvergleich bei Vorauswahl)


function element($arrParam)		//�bergeben wird ein assoziatives Array mit den Attributen f�r das HTML-Element
{
	if(isset($arrParam["type"]))	//Pr�ft ob Parameter type �bergeben worden ist
	{
		$strType = $arrParam["type"];
		unset($arrParam["type"]);		//Element type aus Array l�schen
	}
	
	if(isset($arrParam["breite"]))	//Wenn der Parameter breite �bergeben worden ist
	{
		$arrParam["style"].="width:".$arrParam["breite"].";";
		unset($arrParam["breite"]);		//Element breite aus Array l�schen
	}
	
	if(isset($arrParam["hoehe"]))	//Wenn der Parameter hoehe �bergeben worden ist
	{
		$arrParam["style"].="height:".$arrParam["hoehe"].";";
		unset($arrParam["hoehe"]);		//Element hoehe aus Array l�schen
	}
	
	switch($strType)
	{
		case "button":		$strRueckgabe = button($arrParam);
		break;
		case "text":		$strRueckgabe = text($arrParam);
		break;
		case "textarea":	$strRueckgabe = textarea($arrParam);
		break;
		case "select":		$strRueckgabe = select($arrParam);
		break;
	}
		
	return $strRueckgabe;
}

function button($arrParam)	//erzeugt html-Code f�r einen Button
{
	$strAusgabe="<button type=\"button\"";
	
	if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
	{
		$strBeschriftung = $arrParam["text"];
		unset($arrParam["text"]);
	}
	
	foreach($arrParam as $strIndex => $strWert)
	{
		$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
	}
	
	$strAusgabe.=">".$strBeschriftung."</button>";
	
	return $strAusgabe;
}

function text($arrParam)		//erzeugt html-Code f�r ein Textfeld
{
	$strAusgabe="<input type=\"text\"";
	
	if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
	{
		$arrParam["value"] = $arrParam["text"];
		unset($arrParam["text"]);
	}	
	
	foreach($arrParam as $strIndex => $strWert)
	{
		$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
	}
	
	$strAusgabe.=">";
	
	return $strAusgabe;
}

function textarea($arrParam)	//erzeugt html-Code f�r eine Textarea
{
	$strAusgabe="<textarea";
	
	if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
	{
		$strText = $arrParam["text"];
		unset($arrParam["text"]);
	}	
	
	foreach($arrParam as $strIndex => $strWert)
	{
		$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
	}
	
	$strAusgabe.=">".$strText."</textarea>";
	
	return $strAusgabe;
}

function select($arrParam)	//erzeugt html-Code f�r eine Auswahlliste
{
	if(isset($arrParam["readOnly"]))	//Wenn der Parameter readOnly �bergeben worden ist
	{
		unset($arrParam["readOnly"]);
		
		$strAusgabe="<select";
		
		if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
		{
			unset($arrParam["text"]);
		}
		
		if(isset($arrParam["optionsarray"]))	//Wenn der Parameter optionsarray �bergeben worden ist
		{
			$arrOptionsarray = $arrParam["optionsarray"];
			unset($arrParam["optionsarray"]);
		}	
		
		if(isset($arrParam["optionstextfeld"]))	//Wenn der Parameter optionstextfeld �bergeben worden ist
		{
			$strOptionstextfeld = $arrParam["optionstextfeld"];
			unset($arrParam["optionstextfeld"]);
		}	
		
		if(isset($arrParam["optionswertfeld"]))	//Wenn der Parameter optionsidfeld �bergeben worden ist
		{
			$strOptionswertfeld = $arrParam["optionswertfeld"];
			unset($arrParam["optionswertfeld"]);
		}
		
		if(isset($arrParam["optionsvorauswahlwert"]))	//Wenn der Parameter optionsvorauswahlwert �bergeben worden ist
		{
			$strOptionsvorauswahlwert = $arrParam["optionsvorauswahlwert"];
			unset($arrParam["optionsvorauswahlwert"]);
		}	
		
		if(isset($arrParam["optionsvorauswahlfeld"]))	//Wenn der Parameter optionsvorauswahlfeld �bergeben worden ist
		{
			$strOptionsvorauswahlfeld = $arrParam["optionsvorauswahlfeld"];
			unset($arrParam["optionsvorauswahlfeld"]);
		}		
		
		foreach($arrParam as $strIndex => $strWert)
		{
			$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
		}
		
		$strAusgabe.=">";
	
		for($i=0;$i<count($arrOptionsarray[$strOptionstextfeld]);$i++)
		{
			if(isset($strOptionsvorauswahlwert)&&$arrOptionsarray[$strOptionsvorauswahlfeld][$i]==$strOptionsvorauswahlwert)
			{
				$strAusgabe.="<option";
				
				if(isset($strOptionswertfeld))
				{
					$strAusgabe.=" value=\"".$arrOptionsarray[$strOptionswertfeld][$i]."\" selected";
				}
				
				$strAusgabe.=">".$arrOptionsarray[$strOptionstextfeld][$i]."</option>";
			}
		}
		$strAusgabe.="</select>";
	}
	else
	{
		$strAusgabe="<select";
		
		if(isset($arrParam["text"]))	//Wenn der Parameter text �bergeben worden ist
		{
			unset($arrParam["text"]);
		}
		
		if(isset($arrParam["optionsarray"]))	//Wenn der Parameter optionsarray �bergeben worden ist
		{
			$arrOptionsarray = $arrParam["optionsarray"];
			unset($arrParam["optionsarray"]);
		}	
		
		if(isset($arrParam["optionstextfeld"]))	//Wenn der Parameter optionstextfeld �bergeben worden ist
		{
			$strOptionstextfeld = $arrParam["optionstextfeld"];
			unset($arrParam["optionstextfeld"]);
		}	
		
		if(isset($arrParam["optionswertfeld"]))	//Wenn der Parameter optionsidfeld �bergeben worden ist
		{
			$strOptionswertfeld = $arrParam["optionswertfeld"];
			unset($arrParam["optionswertfeld"]);
		}
		
		if(isset($arrParam["optionsvorauswahlwert"]))	//Wenn der Parameter optionsvorauswahlwert �bergeben worden ist
		{
			$strOptionsvorauswahlwert = $arrParam["optionsvorauswahlwert"];
			unset($arrParam["optionsvorauswahlwert"]);
		}	
		
		if(isset($arrParam["optionsvorauswahlfeld"]))	//Wenn der Parameter optionsvorauswahlfeld �bergeben worden ist
		{
			$strOptionsvorauswahlfeld = $arrParam["optionsvorauswahlfeld"];
			unset($arrParam["optionsvorauswahlfeld"]);
		}		
		
		if(isset($arrParam["keineleereoption"]))	//Wenn der Parameter keineleereoption �bergeben worden ist
		{
			$intKeineleereOption = 1;
			unset($arrParam["keineleereoption"]);
		}		
		
		foreach($arrParam as $strIndex => $strWert)
		{
			$strAusgabe.=" ".$strIndex."=\"".$strWert."\"";
		}
		
		$strAusgabe.=">";
	
	
		if(!$intKeineleereOption)
		{
			//Leeres Auswahlfeld einf�gen
			
			$strAusgabe.="<option";
			
			if(isset($strOptionswertfeld))
			{
				$strAusgabe.=" value=\"-1\"";
			
			}
			$strAusgabe.="></option>";
		}
		
		for($i=0;$i<count($arrOptionsarray[$strOptionstextfeld]);$i++)
		{
			$strAusgabe.="<option";
			
			if(isset($strOptionswertfeld))
			{
				$strAusgabe.=" value=\"".$arrOptionsarray[$strOptionswertfeld][$i]."\"";
			}
			
			if(isset($strOptionsvorauswahlwert)&&$arrOptionsarray[$strOptionsvorauswahlfeld][$i]==$strOptionsvorauswahlwert)
			{
				$strAusgabe.=" selected";
			}
			
			$strAusgabe.=">".$arrOptionsarray[$strOptionstextfeld][$i]."</option>";		
		}
		$strAusgabe.="</select>";
	}
	
	return $strAusgabe;
}




function aufloesungermitteln($seite)
{
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";

	echo "<html>";
	echo "<head>";
	echo "<title>Objekt</title>";
	
	echo "<script language=\"JavaScript\">";
	
	echo "function bildschirmdaten()";
	echo "{";
		echo "location.href='".$seite."?aufl='+screen.width;";
	echo "}";
	echo "</script>";
		
	echo "</head>";

	echo "<body onload=\"bildschirmdaten();\"></body>";
}


/*Beispiel f�r das Erstellen eines Auswahlfeldes

$arr=abfrage("db2",$hdlDb2Verbindung,'select * from land');

$arrLand = $arr["trefferarray"];


	
	echo element(array(
	"type"=>"select",
	"name"=>"knopf",
	"style"=>"border-color:red;",
	"breite"=>"300px",
	"hoehe"=>"50px",
	"optionsarray"=>$arrLand,
	"optionstextfeld"=>"Name",
	"optionswertfeld"=>"Land_Nr",
	"optionsvorauswahlwert"=>"39",
	"optionsvorauswahlfeld"=>"Land_Nr"
	));
	
*/

//Wandelt einen Double-Wert in einen Eurobetrag um (z.B. 15,3 in 15,30 �

function eurobetrag($dblBetrag)
{
	return number_format(str_replace(",",".",$dblBetrag),2,',','.')." �";
}

//Erzeugt den Java-Script-Code f�r modales Fenster


//Zum Erzeugen des Fensters m�ssen im aufrufenden Fenster folgende Zeilen eingef�gt werden:

//var parameter = "strAbrDatum="+datum+"&strKundennr="+kundennr;	(Javascript-Zeile zur �bergabe von Parametern an die Datei)
			
//<?php echo dialog("dateiname.php"); ? > (entspricht der aufzurufenden Datei im Dialogfenster)

//Parameter

//$intDialogbreite		(Breite des Dialogfensters)
//$intDialoghoehe		(H�he des Dialogfensters)
//$intDialogstatus		(Statuszeile)
//$intDialoghelp		(Hilfefragezeichen)
//$intDialogscroll		(Scrolleisten)
//$intNavigationhoehe	(H�he der Navigationsleiste in einem Frameset)

//werden in Parameterdatei dialog_dateiname.php im Include-Verzeichnis card/dialog/ gesetzt

function dialog($strDateiname)
{
	global $intSchriftgroesse;
	global $intScrollanzahl;
	
	//Parameter aus Parameterdatei dialog_... (falls vorhanden) einbinden
	
	$strDatei = "card/dialog/dialog_".$strDateiname;
		
	if(file_exists(realpath("/daten/include/".$strDatei)))
	{
		require($strDatei);
	}
	
	//Wenn die Parameter nicht vorhanden sind, werden Standardparameter verwendet
	
	if(!isset($intDialogbreite))
		$intDialogbreite=300;
	
	if(!isset($intDialoghoehe))
		$intDialoghoehe=300;
	
	if(!isset($intDialogstatus))
		$intDialogstatus=0;
		
	if(!isset($intDialoghelp))
		$intDialoghelp=0;
		
	if(!isset($intDialogscroll))
		$intDialogscroll=0;
		
	if(!isset($intNavigationshoehe))
		$intNavigationshoehe=100;
		
	if(!isset($intScrollanzahl))
		$intScrollanzahl = 10;
	

	$strRueckgabe = "var rueckgabe = unescape(window.showModalDialog(\"".$strDateiname."?\"+parameter+\"&intNavigationshoehe=".
	$intNavigationshoehe."&intScrollanzahl=".$intScrollanzahl."\",window,\"dialogHeight:".
	$intDialoghoehe."px;	dialogWidth:".$intDialogbreite."px;status:".$intDialogstatus.";help:".
	$intDialoghelp.";scroll:".$intDialogscroll."\"));";
	return $strRueckgabe;
}

//Liefert bei �bergabe eines Datums in der Form 25.12.03 ein Datum in der Form 2003-12-25 00:00:00.000000 zur�ck

function db2datum($datDatum)
{
	$arrDatum = explode(".",$datDatum);
	
	$strDatum =	strftime("%Y-%m-%d",mktime(0,0,0,$arrDatum[1],$arrDatum[0],$arrDatum[2])).' 00:00:00.000000';
	return $strDatum;
}

//Liefert bei �bergabe eines Datums in der Form 2003-12-25 00:00:00.000000 ein Datum in der in der Form 25.12.03 zur�ck

function db2datumzurueck($datDatum)
{
	$arrDatum1 = explode(" ",trim($datDatum));
	
	$arrDatum2 = explode("-",trim($arrDatum1[0]));
	
	$datDatum = strftime("%d.%m.%y",mktime(0,0,0,(integer)$arrDatum2[1],(integer)$arrDatum2[2],$arrDatum2[0]));
	
	return $datDatum;
}

//Ersetzt das Komma eines Betrages durch einen Punkt

function punktersetztkomma($strBetrag)
{
	return str_replace(",",".",$strBetrag);
}

//Ersetzt den Punkt eines Betrages durch ein Komma

function kommaersetztpunkt($strBetrag)
{
	return str_replace(".",",",$strBetrag);
}


?>