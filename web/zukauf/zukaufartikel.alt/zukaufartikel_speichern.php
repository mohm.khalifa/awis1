<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('ZLA','ZLA_%');

$AWIS_KEY1 = $_POST['txtZLA_KEY'];

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if(intval($_POST['txtZLA_KEY'])===0)		// Neuer Artikel
{

		// daten auf Vollst�ndigkeit pr�fen
	$Fehler = '';
	$Pflichtfelder = array();
	foreach($Pflichtfelder AS $Pflichtfeld)
	{
		if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
		{
			$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZLA'][$Pflichtfeld].'<br>';
		}
	}
		// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		die('<span class=HinweisText>'.$Fehler.'</span>');
	}
	$SQL = 'INSERT INTO ZukaufLieferantenArtikel';
	$SQL .= '(';
	$SQL .= 'ZLA_ARTIKELNUMMER,ZLA_ZLI_KEY,ZLA_ZLH_KEY,ZLA_BEZEICHNUNG,ZLA_WG1,ZLA_WG2';
	$SQL .= ',ZLA_WG3,ZLA_MENGENEINHEIT,ZLA_WAEHRUNG,ZLA_MWS_ID,ZLA_PREISDATUM,ZLA_VK,ZLA_EK';
	$SQL .= ',ZLA_RABATT1,ZLA_RABATT2,ZLA_EINHEITENPROVK,ZLA_GUELTIGAB,ZLA_GUELTIGBIS,ZLA_AST_ATUNR,ZLA_STATUS';
	$SQL .= ',ZLA_USER,ZLA_USERDAT';
	$SQL .= ')VALUES ('.awis_FeldInhaltFormat('T',$_POST['txtZLA_ARTIKELNUMMER'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZLA_ZLI_KEY'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZLA_ZLH_KEY'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_BEZEICHNUNG'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_WG1'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_WG2'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_WG3'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_MENGENEINHEIT'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_WAEHRUNG'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZLA_MWS_ID'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtZLA_PREISDATUM'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('N2',$_POST['txtZLA_VK'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('N2',$_POST['txtZLA_EK'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('N2',$_POST['txtZLA_RABATT1'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('N2',$_POST['txtZLA_RABATT2'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_EINHEITENPROVK'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtZLA_GUELTIGAB'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtZLA_GUELTIGBIS'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_AST_ATUNR'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZLA_STATUS'],true);
	$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
	$SQL .= ',SYSDATE';
	$SQL .= ')';
	if(!awisExecute($con,$SQL))
	{
		awisErrorMailLink('zukaufartikel_speichern.php',1,$awisDBError['message'],'200709101821');
		die();
	}
	$SQL = 'SELECT seq_ZLA_KEY.CurrVal AS KEY FROM DUAL';
	$rsKey = awisOpenRecordset($con,$SQL);
	$AWIS_KEY1=$rsKey['KEY'][0];
	awis_BenutzerParameterSpeichern($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName() ,$rsKey['KEY'][0]);
}
else 					// ge�nderter 
{
	$Felder = explode(';',awis_NameInArray($_POST, 'txtZLA',1,1));
	$FehlerListe = array();
	$UpdateFelder = '';

	awis_BenutzerParameterSpeichern($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName() ,$_POST['txtZLA_KEY']);
	$rsZLA = awisOpenRecordset($con,'SELECT * FROM Zukauflieferantenartikel WHERE ZLA_key=' . $_POST['txtZLA_KEY'] . '');
	$FeldListe = '';
	foreach($Felder AS $Feld)
	{
		$FeldName = substr($Feld,3);
		if(isset($_POST['old'.$FeldName]))
		{
			// Alten und neuen Wert umformatieren!!
			switch ($FeldName)
			{
				default:
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			}
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
			$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsZLA[$FeldName][0],true);
	//echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
			if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
			{
				if($WertAlt != $WertDB AND $WertDB!='null')
				{
					$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
				}
				else
				{
					$FeldListe .= ', '.$FeldName.'=';

					if($_POST[$Feld]=='')	// Leere Felder immer als NULL
					{
						$FeldListe.=' null';
					}
					else
					{
						$FeldListe.=$WertNeu;
					}
				}
			}
		}
	}

	if(count($FehlerListe)>0)
	{
		$Meldung = str_replace('%1',$rsZLA['ZLA_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
		foreach($FehlerListe AS $Fehler)
		{
			$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
		}
		awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
	}
	elseif($FeldListe!='')
	{
		$SQL = 'UPDATE ZukaufLieferantenArtikel SET';
		$SQL .= substr($FeldListe,1);
		$SQL .= ', ZLA_user=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', ZLA_userdat=sysdate';
		$SQL .= ' WHERE ZLA_key=0' . $_POST['txtZLA_KEY'] . '';
		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('ZukaufLieferantenArtikel',1,'Fehler beim Speichern',$SQL);
		}
	}
	$AWIS_KEY1=$_POST['txtZLA_KEY'];
}


//*******************************************************************
//* Zusatzids speichern
//*******************************************************************
$Felder = explode(';',awis_NameInArray($_POST, 'txtZAI_ID_',1,1));
if(count($Felder)>0)
{
	awis_Debug(1,$_POST);
	foreach($Felder AS $Feld)
	{
		if(awis_FeldInhaltFormat('T',$_POST[$Feld])!==awis_FeldInhaltFormat('T',$_POST['old'.substr($Feld,3)]))
		{
			$ID = explode('_',$Feld);
			if($ID[2]==0)
			{
				$SQL = 'INSERT INTO ZukaufLieferantenArtikelIDs';
				$SQL .= '(ZAI_ZLA_KEY,ZAI_AID_NR,ZAI_ID,ZAI_USER,ZAI_USERDAT,ZAI_IMQ_ID)';
				$SQL .= 'VALUES(';
				$SQL .= ''.$AWIS_KEY1;
				$SQL .= ', 4 '; // ATUNR
				$SQL .= ', '.awis_FeldInhaltFormat('T',$_POST[$Feld]);		
				$SQL .= ', '.awis_FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
				$SQL .= ', sysdate';
				$SQL .= ', 4';	// Importquelle (manuell)
				$SQL .= ')';
				
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('ZukaufLieferantenArtikel',1,'Fehler beim Speichern',$SQL);
				}				
			}
			else 
			{
				if($_POST[$Feld]==='')
				{
					$SQL = 'DELETE FROM ZukaufLieferantenArtikelIds';
					$SQL .= ' WHERE ZAI_KEY = 0'.awis_FeldInhaltFormat('N0',$ID[2]);
				}
				else
				{
					$SQL = 'UPDATE ZukaufLieferantenArtikelIds';
					$SQL .= ' SET ZAI_ID = '.awis_FeldInhaltFormat('T',$_POST[$Feld]);
					$SQL .= ' , ZAI_USER = '.awis_FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ' , ZAI_UserDat = sysdate';
					$SQL .= ' , ZAI_IMQ_ID = 4';
					$SQL .= ' WHERE ZAI_KEY = 0'.awis_FeldInhaltFormat('N0',$ID[2]);
				}
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('ZukaufLieferantenArtikel',1,'Fehler beim Speichern',$SQL);
				}				
			}
		}
	}
}









// Aktuellen ZLA speichern
awis_BenutzerParameterSpeichern($con, "AktuellerZLA" , $AWISBenutzer->BenutzerName() ,$AWIS_KEY1);
?>