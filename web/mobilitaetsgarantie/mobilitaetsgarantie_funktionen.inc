<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class mobilitaetsgarantie_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht56000;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeug;


    function __construct($Benutzer='')
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht56000 = $this->AWISBenutzer->HatDasRecht(56000);
        $this->_EditRecht = (($this->Recht56000 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_MOB'));
        $this->AWISWerkzeug = new awisWerkzeuge();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('MOB', '%');
        $TextKonserven[] = array('AST', 'AST_ATUNR');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Dateiname');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_Mobilitaetsgarantie');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_MOB', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht56000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'MOB'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }



    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';

        if ($AWIS_KEY1 != 0) {
            $Bedingung .= ' AND MOB_KEY = ' . $this->DB->WertSetzen('MOB', 'N0', $AWIS_KEY1);
            return $Bedingung;
        }

        if (isset($this->Param['MOB_VERS_NUMMER']) and $this->Param['MOB_VERS_NUMMER'] != '') {
            $Bedingung .= ' AND MOB_VERS_NUMMER ' . $this->DB->LikeOderIst($this->Param['MOB_VERS_NUMMER'], awisDatenbank::AWIS_LIKE_UPPER, 'MOB');
        }

        if (isset($this->Param['MOB_NAME']) and $this->Param['MOB_NAME'] != '') {
            $Bedingung .= ' AND (';
            $Bedingung .= ' upper(MOB_NAME1) ' . $this->DB->LikeOderIst($this->Param['MOB_NAME'], awisDatenbank::AWIS_LIKE_UPPER, 'MOB');
            $Bedingung .= ' OR upper(MOB_NAME2) ' . $this->DB->LikeOderIst($this->Param['MOB_NAME'], awisDatenbank::AWIS_LIKE_UPPER, 'MOB');
            $Bedingung .= ' OR upper(MOB_NAME3) ' . $this->DB->LikeOderIst($this->Param['MOB_NAME'], awisDatenbank::AWIS_LIKE_UPPER, 'MOB');
            $Bedingung .= ' ) ';
        }

        if (isset($this->Param['MOB_VORGANGSNUMMER']) and $this->Param['MOB_VORGANGSNUMMER'] != '') {
            $Bedingung .= ' AND MOB_VORGANGSNUMMER ' . $this->DB->LikeOderIst($this->Param['MOB_VORGANGSNUMMER'], awisDatenbank::AWIS_LIKE_UPPER, 'MOB');
        }

        if (isset($this->Param['MOB_KFZ_KENNZEICHEN']) and $this->Param['MOB_KFZ_KENNZEICHEN'] != '') {
            $Bedingung .= ' AND MOB_KFZ_KENNZEICHEN_KOMP ' . $this->DB->LikeOderIst(awisWerkzeuge::ArtNrKomprimiert($this->Param['MOB_KFZ_KENNZEICHEN'],array('*','%')), awisDatenbank::AWIS_LIKE_UPPER, 'MOB');
        }

        if (isset($this->Param['MOB_FIL_ID']) and $this->Param['MOB_FIL_ID'] != '') {
            $Bedingung .= ' AND MOB_FIL_ID ' . $this->DB->LikeOderIst($this->Param['MOB_FIL_ID'], awisDatenbank::AWIS_LIKE_UPPER, 'MOB');
        }

        if (isset($this->Param['MOB_DATUM_VON']) and $this->Param['MOB_DATUM_VON'] != '') {
            $Bedingung .= ' AND MOB_VERS_GUELTIG_AB <= ' . $this->DB->WertSetzen('MOB', 'D', $this->Param['MOB_DATUM_VON']);
        }

        if (isset($this->Param['MOB_DATUM_BIS']) and $this->Param['MOB_DATUM_BIS'] != '') {
            $Bedingung .= ' AND MOB_VERS_GUELTIG_BIS >= ' . $this->DB->WertSetzen('MOB', 'D', $this->Param['MOB_DATUM_BIS']);
        }

        return $Bedingung;
    }


}