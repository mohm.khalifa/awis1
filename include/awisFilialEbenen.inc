<?php
//require_once('db.inc.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');


class awisFilialEbenen
{
	/**
	 * Fehler: Kein g�ltiger FEB_KEY angegeben
	 *
	 */
	const ERR_KEIN_FEB_KEY = 1;

	/**
	 * Datensatz mit den Filialebenen-Infos
	 *
	 * @var awisRecordset
	 */
	protected $_rsFEB=0;

	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB;

	/**
	 * AWIS-Benutzerobjekt
	 *
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer;

	public function __construct($FEB_KEY=0)
	{
		$this->_AWISBenutzer = awisBenutzer::Init();
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();

		$BindeVariablen=array();
		$BindeVariablen['var_N0_feb_key']=intval('0'.$FEB_KEY);
		
		$SQL = 'SELECT *';
		$SQL .= ' FROM FilialEbenen ';
		$SQL .= ' WHERE FEB_KEY = :var_N0_feb_key';

		$this->_rsFEB = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
	}


	/**
	 * Feld mit den Pfad-Namen
	 *
	 */
	const FILIALEBENELISTE_FELD_PFAD = 'PFAD';
	/**
	 * Feld mit den Pfad-Keys
	 *
	 */
	const FILIALEBENELISTE_FELD_PFADKEY = 'PFAD_KEY';

	/**
	 * Ergebnis wird als ARRAY zur�ckgeliefert
	 *
	 */
	const FILIALEBENELISTE_ERGTYP_ARRAY = 'array';

	/**
	 * Ergebnis wird als ARRAY zur�ckgeliefert
	 *
	 */
	const FILIALEBENELISTE_ERGTYP_XML = 'xml';

	/**
	 * Ergebnis wird als ARRAY f�r AWIS-SELECT Felder zur�ckgeliefert
	 *
	 */
	const FILIALEBENELISTE_ERGTYP_SELECT = 'select';

	/**
	 * Liefert eine Liste mit g�ltigen Filialebenen
	 *
	 * @param string [optional] $Zeitpunkt
	 * @param string [optional] $Feld
	 * @param string [optional] $ErgebnisTyp
	 * @return mixed
	 */
	public function FilialEbenenListe($Zeitpunkt='', $Feld=self::FILIALEBENELISTE_FELD_PFAD, $ErgebnisTyp=self::FILIALEBENELISTE_ERGTYP_ARRAY)
	{
		if($Zeitpunkt=='')
		{
			$Zeitpunkt=' TRUNC(SYSDATE) ';
		}
		else
		{
			$Zeitpunkt = $this->_DB->FeldInhaltFormat('D',$Zeitpunkt,true);
		}
		$SQL = 'select distinct feb_key, '.$Feld;
		$SQL .= ' FROM';
 		$SQL .= ' (';
		$SQL .= " select sys_connect_by_path(feb_bezeichnung,'/') as pfad,";
		$SQL .= " sys_connect_by_path(feb_key,'/') as pfad_key,";
		$SQL .= ' level stufe, feb_ebene, feb_key, feb_feb_key';
		$SQL .= ' , connect_by_root feb_key feb_key_root';
		$SQL .= ' from filialebenen';
		$SQL .= ' connect by prior feb_key = feb_feb_key';
		$SQL .= ' ) Stufen';
 		$SQL .= ' INNER JOIN (select feb_key feb_key1, FEB_GUELTIGAB, FEB_GUELTIGBIS, feb_bezeichnung from filialebenen) FEB ON Stufen.feb_key = FEB.feb_key1';
		$SQL .= '  where stufe = feb_ebene';
		$SQL .= ' AND FEB_GUELTIGAB <= '.$Zeitpunkt.' AND FEB_GUELTIGBIS >='.$Zeitpunkt.'';
		$SQL .= ' order by 2';

		$rsFEB = $this->_DB->RecordSetOeffnen($SQL);
		switch ($ErgebnisTyp)
		{
			case self::FILIALEBENELISTE_ERGTYP_ARRAY:
				return $rsFEB->Tabelle();
				break;
			case self::FILIALEBENELISTE_ERGTYP_XML:
				return $rsFEB->xml();
				break;
			case self::FILIALEBENELISTE_ERGTYP_SELECT:
				$Daten = array();
				while(!$rsFEB->EOF())
				{
					$Daten[] = $rsFEB->FeldInhalt(1).'~'.$rsFEB->FeldInhalt(2);
					$rsFEB->DSWeiter();
				}
				return $Daten;
				break;
			case 'SQL':
				return $SQL;
		}

		return true;
	}

	/**
	 * Liefert eine Liste aller Filialen, die unter einer bestimmten Ebene angeordnet sind
	 *
	 * @param int $FEB_KEY
	 * @param string [optional] $Zeitpunkt
	 * @param string [optional] $ErgebnisTyp
	 * @return mixed
	 */
	public function FilialenEinerEbene($FEB_KEY, $Zeitpunkt='',$ErgebnisTyp=self::FILIALEBENELISTE_ERGTYP_ARRAY)
	{
		if($Zeitpunkt=='')
		{
			$Zeitpunkt=' TRUNC(SYSDATE) ';
		}
		else
		{
			$Zeitpunkt = $this->_DB->FeldInhaltFormat('D',$Zeitpunkt,true);
		}
		$SQL = 'SELECT FEZ_FIL_ID AS XX1_FIL_ID, FEZ_FIL_ID || \' - \' || FIL_BEZ';
		$SQL .= ' FROM';
		$SQL .= ' (';
		$SQL .= ' SELECT XX.*, max(stufe) over (partition by fez_fil_id ORDER BY fez_fil_id) AS Nr';
		$SQL .= ' FROM';
		$SQL .= ' (';
 		$SQL .= ' select stufen.*, fez_fil_id, feb_bezeichnung , FEB_GUELTIGAB, FEB_GUELTIGBIS, FEZ_GUELTIGAB, FEZ_GUELTIGBIS';
 		$SQL .= ' from';
 		$SQL .= ' (';
   		$SQL .= " select sys_connect_by_path(feb_bezeichnung,'/') as pfad,";
   		$SQL .= " sys_connect_by_path(feb_key,'/') as pfad_key,";
   		$SQL .= ' level stufe, feb_ebene, feb_key, feb_feb_key';
   		$SQL .= ' , connect_by_root feb_key feb_key_root';
   		$SQL .= ' from filialebenen';
   		$SQL .= ' connect by prior feb_key = feb_feb_key';
 		$SQL .= ' ) Stufen';
 		$SQL .= ' LEFT OUTER JOIN (select fez_feb_key, FEZ_GUELTIGAB, FEZ_GUELTIGBIS, fez_fil_id from filialebenenzuordnungen) FEZ ON Stufen.feb_key = FEZ.fez_feb_key';
 		$SQL .= ' INNER JOIN (select feb_key feb_key1, FEB_GUELTIGAB, FEB_GUELTIGBIS, feb_bezeichnung from filialebenen) FEB ON Stufen.feb_key = FEB.feb_key1';
		$SQL .= ' ) XX';
		$SQL .= ' WHERE FEB_GUELTIGAB <= TRUNC(SYSDATE) AND FEB_GUELTIGBIS >= TRUNC(SYSDATE)';
		$SQL .= ' AND FEZ_GUELTIGAB <= TRUNC(SYSDATE) AND FEZ_GUELTIGBIS >= TRUNC(SYSDATE)';
		$SQL .= ($FEB_KEY==0?'':' AND \'/\'||PFAD_KEY||\'/\' LIKE \'%/'.$FEB_KEY.'/%\'');

		$SQL .= ' ) FILPFAD';
		$SQL .= ' INNER JOIN Filialen ON FEZ_FIL_ID = FIL_ID';
		$SQL .= ' WHERE STUFE = Nr AND FEZ_FIL_ID IS NOT NULL';

		$rsFEB = $this->_DB->RecordSetOeffnen($SQL);
		switch ($ErgebnisTyp)
		{
			case self::FILIALEBENELISTE_ERGTYP_ARRAY:
				return $rsFEB->Tabelle();
				break;
			case self::FILIALEBENELISTE_ERGTYP_XML:
				return $rsFEB->xml();
				break;
			case self::FILIALEBENELISTE_ERGTYP_SELECT:
				$Daten = array();
				while(!$rsFEB->EOF())
				{
					$Daten[] = $rsFEB->FeldInhalt(1).'~'.$rsFEB->FeldInhalt(2);
					$rsFEB->DSWeiter();
				}
				return $Daten;
				break;
			case 'SQL':
				return $SQL;
		}

		return true;
	}


	public function ZeigeVererbteRollen()
	{
		global $awisRSZeilen;

		$Ebene = $this->_rsFEB->FeldInhalt('FEB_EBENE');

		$FEB_KEY = $this->_rsFEB->FeldInhalt('FEB_FEB_KEY');
		$Rollen = array();
		$RollenAnz = 0;

		while($FEB_KEY!='')
		{
			$BindeVariablen=array();
			$BindeVariablen['var_N0_feb_key']=intval('0'.$FEB_KEY);
			
			$SQL = 'SELECT FEB_KEY, FEB_FEB_KEY, FER_KEY, FRZ_KEY, FER_KEY, FER_BEZEICHNUNG, FEB_BEZEICHNUNG, ';
			$SQL .= 'FRZ_GUELTIGAB, FRZ_GUELTIGBIS, KON_KEY, KON_NAME1, KON_NAME2';
			$SQL .= ' FROM Filialebenen';
			$SQL .= ' LEFT OUTER JOIN FILIALEBENENROLLENZUORDNUNGEN ON FRZ_XXX_KEY = FEB_KEY AND FRZ_XTN_KUERZEL = \'FEB\' AND ((FRZ_GUELTIGAB <= TRUNC(SYSDATE) AND FRZ_GUELTIGBIS >= TRUNC(SYSDATE)) OR FRZ_GUELTIGAB IS NULL)';
			$SQL .= ' LEFT OUTER JOIN Filialebenenrollen ON FRZ_FER_KEY = FER_KEY';
			$SQL .= ' LEFT OUTER JOIN Kontakte ON FRZ_KON_KEY = KON_KEY';
			$SQL .= ' WHERE FEB_KEY = :var_N0_feb_key';
			$SQL .= ' AND (FEB_GUELTIGAB <= TRUNC(SYSDATE) AND FEB_GUELTIGBIS >= TRUNC(SYSDATE))';
			//$SQL .= ' ORDER BY FRZ_GUELTIGAB DESC';
//awis_Debug(1,$SQL);
			//$rsFRZ = awisOpenRecordset($this->_con,$SQL);
			$rsFRZ = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
			if($rsFRZ->EOF())
			{
				break;
			}

			if(array_key_exists($rsFRZ->FeldInhalt('FER_KEY'),$Rollen)===false AND $rsFRZ->FeldInhalt('FRZ_KEY')!==null)
			{
				$Rollen[$rsFRZ->FeldInhalt('FER_KEY')]=array('FEB_KEY'=>$rsFRZ->FeldInhalt('FEB_KEY'),
													'FEB_BEZEICHNUNG'=>$rsFRZ->FeldInhalt('FEB_BEZEICHNUNG'),
													'FER_BEZEICHNUNG'=>$rsFRZ->FeldInhalt('FER_BEZEICHNUNG'),
													'FRZ_GUELTIGAB'=>$rsFRZ->FeldInhalt('FRZ_GUELTIGAB'),
													'FRZ_GUELTIGBIS'=>$rsFRZ->FeldInhalt('FRZ_GUELTIGBIS'),
													'KON_KEY'=>$rsFRZ->FeldInhalt('KON_KEY'),
													'KON_NAME'=>$rsFRZ->FeldInhalt('KON_NAME1').', '.$rsFRZ->FeldInhalt('KON_NAME2')
													);
			}
			// N�chste Ebene
			$FEB_KEY = $rsFRZ->FeldInhalt('FEB_FEB_KEY');
		}

		return $Rollen;
	}


}



?>