<?php
//require_once 'awisDatenbank.inc';
//require_once 'awisBenutzer.inc';
//require_once 'awisFormular.inc';
require_once 'awisMailer.inc';
//require_once 'awisBerichte.inc';

class kdtelefonie_funktionen
{
	/* werden im Konstruktor initialisiert */
	private $_Form;						// lokale Instanz der Klasse awisFormular
	private $_DB;						// lokale Instanz der DB-Klasse
	private $_CurPos;					// lokale Instanz der CursorPos
	private $_SprachKonserven;          // lokale Instanz von $AWISSprachKonserven
	//private $_Param;					// Parameter-Array => alle Felder benutzten Werte zu den Feldern sind hier drin
	
	const _LabelBreite = 200;			// Standard-Breite der Label (links vor Wert)
    const _UniqueDelimiter = '|*~*|';   // eindeutiges Trennzeichen (z.B. f�r gruppierte DropDown-Felder)
    const _SuchenInTGD = 1;
    const _SuchenInTKD = 2;
	
	public function __construct()
	{
		// globalen Klassenobjekte reinziehen
		global $Form;
		global $DB;
		global $AWISSprachKonserven;
		global $AWISCursorPosition;

		// globalen Klassenobjekte �ber lokale Instanzen verf�gbar machen
		$this->_Form = $Form;
		$this->_DB = $DB;
		$this->_CurPos = $AWISCursorPosition;
        
		// Textkonserven laden
		$TextKonserven = array();
		$TextKonserven[]=array('KTA','%');
		$TextKonserven[]=array('KT1','%');
		$TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
		$this->_SprachKonserven = $Form->LadeTexte($TextKonserven);		
	}
	public function MailSenden($Key, &$Benutzer)
	{
		// wird ben�tigt, da Bericht hier direkt aufgerufen wird
		//require_once('/daten/web/berichte/ber_InkassoAnhang.inc');
	
		try
		{
			$Mail = new awisMailer($this->_DB, $Benutzer);                  // Mailer-Klasse instanzieren
			$Werkzeuge = new awisWerkzeuge();
			$Absender = 'noreply@de.atu.eu';                     //Absender der Mails TODO
			$Empfaenger = '';                                               // Empf�nger der Mails => wird dynamisch in Schleife belegt
			$EmpfaengerCC = '';                      // CC-Empf�nger
			$Betreff = $this->_SprachKonserven['KTA']['mail_Betreff'];      // Betreff der Mail
	
			/*************************************************************************
			 * Hier wird der Mailtext zusammengeschustert
			*************************************************************************/
			$Text = '';
			$Text .= $this->_SprachKonserven['KTA']['mail_Anrede'] . ',';
			$Text .= "\n\r\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock01'];
			$Text .= "\n\r\n\r";
			
			$SQL = 'SELECT a.*,ANR_ANREDE,KTT_KRITERIUM,KTT_SORTIERUNG,KTT_KTF_ID,FIL_BEZ,FIL_ORT,KTK_TEXTKONSERVE,KTF_ID,KTF_BEZEICHNUNG,KTZ_WERT,KTZ_BEMERKUNG,KTZ_KRITERIUM,ky.bez as typ,km.bez as Modell,kh.bez as Hersteller,to_char(a.KTA_EZ_DATUM,\'DD.MM.YYYY\') as EZ_DATUM';
			$SQL .= ' FROM KDTELEFONIEAUFTRAG a';
			$SQL .= ' INNER JOIN KDTELEFONIEKATALOG on KTK_id = KTA_KTK_ID';
			$SQL .= ' INNER JOIN KDTELEFONIEFRAGEN on KTK_id = KTF_KTK_KEY';
			$SQL .= ' INNER JOIN KDTELEFONIEAUFTRAGSZUORD on KTZ_KTF_ID = KTF_ID AND KTZ_KTA_KEY='.$Key;
			$SQL .= ' INNER JOIN FILIALEN on KTA_FIL_ID = FIL_ID';
			$SQL .= ' INNER JOIN KFZ_TYP KY on KTA_KTYPNR = ktypnr';
			$SQL .= ' INNER JOIN KFZ_MODELL KM on ky.kmodnr = km.kmodnr';
			$SQL .= ' INNER JOIN KFZ_HERST KH on km.khernr = kh.khernr';
			$SQL .= ' INNER JOIN ANREDEN on ANR_ID = KTA_ANREDE';
			$SQL .= ' INNER JOIN KDTELEFONIEKRITERIEN on KTF_ID = KTT_KTF_ID';
			$SQL .= ' WHERE KTF_STATUS=\'A\'';
			$SQL .= ' AND kta_key ='.$Key;
			$SQL .= ' ORDER BY KTF_SORTIERUNG,KTT_SORTIERUNG';
			//var_dump($SQL);
			$rsMail = $this->_DB->RecordSetOeffnen($SQL);	
			$FilOrt = $rsMail->FeldInhalt('FIL_ORT');
			$SQL = 'SELECT *';
			$SQL .= ' FROM V_FILIALEBENENROLLEN_AKTUELL';
			$SQL .= ' INNER JOIN KONTAKTE on XX1_KON_KEY = KON_KEY';
			$SQL .= ' WHERE XX1_FIL_ID ='.$rsMail->FeldInhalt('KTA_FIL_ID');
			$SQL .= ' AND xx1_FER_KEY in (23,25)';
			$SQL .= ' ORDER BY XX1_FER_KEY';
			$rsFil = $this->_DB->RecordSetOeffnen($SQL);
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock02'].': ' . $rsMail->FeldInhalt('ANR_ANREDE') . ' '. $rsMail->FeldInhalt('KTA_TITEL') . ' '.  $rsMail->FeldInhalt('KTA_NACHNAME') . ' '.  $rsMail->FeldInhalt('KTA_VORNAME');
			$Text .= "\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock03'].': ' . $rsMail->FeldInhalt('KTA_TELEFON');
			$Text .= "\n\r\n\r";
			$Fil = 0;
			while (!$rsFil->EOF())
			{
				if ($rsFil->FeldInhalt('XX1_FER_KEY') == 23)
				{
					$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock12'].': ' . $rsFil->FeldInhalt('KON_NAME1').', '.$rsFil->FeldInhalt('KON_NAME2');
					$Text .= "\n\r";
				}
				else
				{
					$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock13'].': ' . $rsFil->FeldInhalt('KON_NAME1').', '.$rsFil->FeldInhalt('KON_NAME2');
					$Text .= "\n\r";
				}
				$rsFil->DSWeiter();
			}
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock07'].': ' . $rsMail->FeldInhalt('FIL_BEZ');
			$Text .= "\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock08'].': ' . $rsMail->FeldInhalt('KTA_FIL_ID');
			$Text .= "\n\r\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock09'].': ' . $rsMail->FeldInhalt('HERSTELLER');
			$Text .= "\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock11'].': ' . $rsMail->FeldInhalt('MODELL') . ' ' . $rsMail->FeldInhalt('TYP');
			$Text .= "\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock04'].': ' . $rsMail->FeldInhalt('KTA_KFZ_KZ');
			$Text .= "\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock05'].': ' . $rsMail->FeldInhalt('EZ_DATUM');
			$Text .= "\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock06'].': ' . $rsMail->FeldInhalt('KTA_VORGANGSNR');
			$Text .= "\n\r\n\r";
			$Text .= $this->_SprachKonserven['KTA']['mail_TextBlock10'];
			$Text .= "\n\r";
			$Text .= '-------------------------------------------';
			$Text .= "\n\r\n\r";
			$i = 1;
			$FragenId = 0;
			while (!$rsMail->EOF())
			{
				If($FragenId <> $rsMail->FeldInhalt('KTF_ID'))
				{
					$Text .= $i .". " .$this->_SprachKonserven[$rsMail->FeldInhalt('KTK_TEXTKONSERVE')][$rsMail->FeldInhalt('KTF_BEZEICHNUNG')].': ' . ($rsMail->FeldInhalt('KTZ_WERT') == 1 ? 'Ja' : 'Nein ');
					$Text .= "\n\r\n\r";
					$Text .= $this->_SprachKonserven['KTA']['BEMERKUNG'] . ': ' .  $rsMail->FeldInhalt('KTZ_BEMERKUNG');
					$Text .= "\n\r\n\r";
					$FragenId = $rsMail->FeldInhalt('KTF_ID');
				}
				$Text .= $rsMail->FeldInhalt('KTT_KRITERIUM') . ': ' . (substr($rsMail->FeldInhalt('KTZ_KRITERIUM'),$rsMail->FeldInhalt('KTT_SORTIERUNG')-1,1) == '1' ? 'X' : '');
				$Text .= "\n\r";
				$i++;
                $rsMail->DSWeiter();
			}
			$Text = str_replace('$FIL_ORT$',$FilOrt, $Text);
			//$Text .= $this->_SprachKonserven['TAG']['mail_Signatur01'];
			//$Text .= "\n\r\n\r";
			//$Text .= $this->_SprachKonserven['TAG']['mail_Signatur02'];
			//$Text .= "\n\r";
			//$Text .= $this->_SprachKonserven['TAG']['mail_Signatur03'];
			//$Text .= "\n\r\n\r";
			//$Text .= $this->_SprachKonserven['TAG']['mail_Signatur04'];
			/*************************************************************************/                               
	
				if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
				{
					//$Empfaenger = str_pad($rsMail->FeldInhalt('TGD_FIL_ID'), 4, '0', STR_PAD_LEFT) . '@de.atu.eu';
					$Empfaenger = 'Hermine.Spachtholz@de.atu.eu';
				}
				else
				{
					$Empfaenger = 'tobias.schaeffler@de.atu.eu';
					$EmpfaengerCC = 'tobias.schaeffler@de.atu.eu';
				}
				$Mail->LoescheAdressListe();
				$Mail->DebugLevel(0);
				$Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
				$Mail->AdressListe(awisMailer::TYP_CC, $EmpfaengerCC, false, false);
				$Mail->Absender($Absender);
				$Mail->Betreff($Betreff);
				$Mail->Text($Text, awisMailer::FORMAT_TEXT, true);
				$Mail->SetzeBezug('KTA', $Key);
				//$Mail->AnhaengeLoeschen();
				//$Mail->Anhaenge($Verzeichnis . $DateiRumpf . $rsMail->FeldInhalt('TGD_FIL_ID') . '.pdf', $Anhangname);
				//$Mail->MailSenden();
				$Mail->MailInWarteschlange();
	
				// in Array merken und nachher ausgeben lassen
				//$HinweisText[] = 'Bericht "' . $Verzeichnis . $DateiRumpf . $rsMail->FeldInhalt('TGD_FIL_ID') . '.pdf" erstellt und als "' . $Anhangname . '" an "' . $Empfaenger . '" versendet.';
			   // $Mail->VerarbeiteWarteschlange();
		}
		catch(Exception $ex)
		{
			$this->_Form->Fehler_Anzeigen('INTERN', $ex->getMessage(),'MELDEN',6,'201210170000');
		}
	}
	
	public function MailWarteschlange(&$Benutzer)
	{
		// wird ben�tigt, da Bericht hier direkt aufgerufen wird
		//require_once('/daten/web/berichte/ber_InkassoAnhang.inc');
	
		try
		{
			$Mail = new awisMailer($this->_DB, $Benutzer);                  // Mailer-Klasse instanzieren
			$Werkzeuge = new awisWerkzeuge();
			$Mail->VerarbeiteWarteschlange();
		}
		catch(Exception $ex)
		{
			$this->_Form->Fehler_Anzeigen('INTERN', $ex->getMessage(),'MELDEN',6,'201210170000');
		}
	}
}