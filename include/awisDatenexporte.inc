<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisMailer.inc';
require_once 'awisCSVSpooler.inc';

class awisDatenexporte
{
    /**
     * awisDatenbank Verbindung
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * Formular
     * @var awisFormular
     */
    private $_Form;
    /**
     * AWIS Benutzer
     * @var awisBenutzer
     */
    private $_AWISBenutzer;
    /**
     * Aktueller Debuglevel f�r Ausgaben
     * @var int
     */
    private $_DebugLevel = 0;
    /**
     * Informationen zu den laufenden Datenexporten
     * @var array
     */
    private $_LaufendeJobs = array();
    /**
     * Mail-Objekt
     * @var awisMailer
     */
    private $_Mailer = null;
    /**
     * Werkzeug
     * @var awisWerkzeuge
     */
    private $_WerkZeugObj = null;
    /**
     * PID Datei f�r die Steuerung
     * @var string
     */
    private $_PID_Datei = '/daten/jobs/pid/awisDatenexporte.pid';
    /**
     * Eigene Prozess-Id
     * @var unknown
     */
    private $_PID = 0;

    /**
     * Initialisierung der Klasse
     *
     * @param awisDatenbank $DB
     * @param awisBenutzer  $AWISBenutzer
     */
    public function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
    {
        $this->_DB = $DB;
        $this->_AWISBenutzer = $AWISBenutzer;
        $this->_WerkZeugObj = new awisWerkzeuge();
        $this->_Mailer = new awisMailer($DB, $AWISBenutzer);

        $this->_Mailer->Absender('awis@de.atu.eu');
        $this->_Form = new awisFormular();
    }

    /**
     * Beenden des Moduls
     */
    public function __destruct()
    {
        @unlink($this->_PID_Datei);
    }

    /**
     * Starten den Export einer Datenafrage
     *
     */
    public function StarteExporte($Anzahl = 1)
    {
        if (is_file($this->_PID_Datei)) {
            if ($this->WatchDog() == true) {
                return false;           // Anderer Scheduler
            }
        }

        // PID Dtatei erzeugen um anzuzeigen, dass was getan wird
        $this->_PID = getmypid();
        $this->_DebugAusgabe('Erzeuge eine PID (' . $this->_PID . ')...', 9);
        file_put_contents($this->_PID_Datei, $this->_PID);

        $SQL = 'select XDZ_KEY, XDZ_XAB_KEY, XBL_LOGIN, XDZ_DATEINAME';
        $SQL .= ' FROM EXPORTABFRAGENZEITPUNKTE';
        $SQL .= ' INNER JOIN BENUTZERLOGINS ON XBL_XBN_KEY = XDZ_XBN_KEY';
        $SQL .= ' WHERE XDZ_NAECHSTERSTART <= SYSDATE';
        $SQL .= ' AND ROWNUM <=  ' . $this->_DB->WertSetzen('XDZ','N0',$Anzahl);
        $SQL .= ' ORDER BY XDZ_PRIORITAET DESC, XDZ_NAECHSTERSTART';
        $rsXDZ = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('XDZ'));

        if ($rsXDZ->EOF()) {
            $this->_DebugAusgabe('Keine Exporte fuer die Verarbeitung', 1);

            return 0;
        }

        while (!$rsXDZ->EOF()) {
            $this->_DebugAusgabe('Starte Zeitplan ' . $rsXDZ->FeldInhalt('XDZ_KEY') . '...', 5);

            $XAB_KEY = $rsXDZ->FeldInhalt('XDZ_XAB_KEY');
            $XDZ_KEY = $rsXDZ->FeldInhalt('XDZ_KEY');
            $BenutzerLogin = $rsXDZ->FeldInhalt('XBL_LOGIN');

            $SQL = 'SELECT XAB_KEY, XAB_XDE_KEY, XAB_BEZEICHNUNG, XAB_UEBERSCHRIFT, XAB_TRENNZEICHEN_CSV';
            $SQL .= ' FROM EXPORTABFRAGEN';
            $SQL .= ' WHERE XAB_KEY = ' . $this->_DB->WertSetzen('XAB', 'N0', $XAB_KEY);

            $rsXAB = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('XAB'));

            $Trenner = $rsXAB->FeldInhalt('XAB_TRENNZEICHEN_CSV');

            //Nachfolgende Zeilen sehen komisch aus, doch leider gibt es laut Internet keine bessere L�sung.
            //--> Trennzeichen brauchen doppelte Hochkommas. Wenn man \t direkt von der Datenbank nehmen w�rde, w�rde auch \t exportiert
            // werden und kein Tab. (Doppelte Hochkommas werden nochmal geparst)
            switch ($Trenner) {
                case '\t':
                    $Trenner = "\t";
                    break;
                case 'Standard':
                    $Trenner = ";";
                    break;
                default:
                    $Trenner = $Trenner;
            }

            $ExportSQL = $this->ErstelleAbfrage($rsXAB->FeldInhalt('XAB_XDE_KEY'), $XAB_KEY, $this->_AWISBenutzer->BenutzerSprache(), false, false, $Trenner);

            // Leerzeilen entfernen
            $ExportSQL = str_replace("\r", '', $ExportSQL);

            $this->_DebugAusgabe('Export SQL ' . $ExportSQL, 99);

            // Tempor�re Datei
            $TEMPDatei = '/tmp/awis_datenexport_' . $XDZ_KEY . '.csv';

            // Falls noch eine alte Auftragsdatei steht, die erst mal l�schen
            clearstatcache();
            if (is_file($TEMPDatei)) {
                $this->_DebugAusgabe('L�sche die alten Relikte', 99);
                unlink($TEMPDatei);
            }

            // Felder der Abfrage als �berschrift ermitteln und in die CSV schreiben
            $FeldListe = $this->ErstelleAbfrage($rsXAB->FeldInhalt('XAB_XDE_KEY'), $XAB_KEY, $this->_AWISBenutzer->BenutzerSprache(), true);

            $UeberschriftenTyp = $rsXAB->FeldInhalt('XAB_UEBERSCHRIFT');

            //Feldbezeichnungen benutzen
            if ($UeberschriftenTyp == 0 or $UeberschriftenTyp == 2) {
                $this->_DebugAusgabe('Benutze die Feldnamen als �berschriften', 99);
                $Ueberschrift = true;
            } elseif ($UeberschriftenTyp == 3) {
                $this->_DebugAusgabe('Benutze Textkonserven als �berschriften', 99);
                //Alle Felder !!des Datenexports!! allgemein holen
                $SQL = 'SELECT XDF_TEXTKONSERVE,XDF_FELDNAME,XDE_BEZEICHNUNG,XDF_TEXTKONSERVE,XDF_DATENTYP';
                $SQL .= ' FROM ExportDatenFelder';
                $SQL .= ' INNER JOIN ExportDaten ON XDE_KEY = XDF_XDE_KEY';
                $SQL .= ' WHERE XDF_XDE_KEY =' . $this->_DB->WertSetzen('XDF', 'N0', $rsXAB->FeldInhalt('XAB_XDE_KEY'));
                $SQL .= ' ORDER BY XDF_SORTIERUNG';
                $rsXDF = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('XDF', true));

                //Alle Relevanten Felder !!der Abfrage!! in Array, und Schl�ssel mit Werten tauschen
                $AbfrageFelder = explode(',', $FeldListe);
                $AbfrageFelder = array_flip($AbfrageFelder);
                $Ueberschrift = [];
                while (!$rsXDF->EOF()) {
                    if (isset($AbfrageFelder[$rsXDF->FeldInhalt('XDF_FELDNAME')])) //Wenn das Feld in der vom User gebauten Abfrage enthalten ist
                    {
                        if ($rsXDF->FeldInhalt('XDF_TEXTKONSERVE') != '') {
                            $FeldKonserve = explode(':', $rsXDF->FeldInhalt('XDF_TEXTKONSERVE'));
                            $Label = $this->_Form->LadeTexte(array(array($FeldKonserve[0], $FeldKonserve[1])), $this->_AWISBenutzer->BenutzerSprache());
                            $Ueberschrift[] .= $Label[$FeldKonserve[0]][$FeldKonserve[1]];
                        } else {
                            $Label =
                                $this->_Form->LadeTexte(
                                    array(array(substr($rsXDF->FeldInhalt('XDF_FELDNAME'), 0, 3), $rsXDF->FeldInhalt('XDF_FELDNAME'))),
                                    $this->_AWISBenutzer->BenutzerSprache()
                                );
                            $Ueberschrift[] .= $Label[substr($rsXDF->FeldInhalt('XDF_FELDNAME'), 0, 3)][$rsXDF->FeldInhalt('XDF_FELDNAME')];
                        }
                    }

                    $rsXDF->DSWeiter();
                }
            } else //Keine �berschrift
            {
                $this->_DebugAusgabe('Keine �berschriften gew�nscht', 99);
                $Ueberschrift = false;
            }


            try {
                $CSV = new awisCSVSpooler($this->_DB);
                $CSV->Trennzeichen($Trenner);

                $this->_DebugAusgabe("Starte Export in $TEMPDatei ", 0);
                $CSV->StarteExport($ExportSQL,$TEMPDatei,[],$Ueberschrift);


                $ExportBenutzer = awisBenutzer::Init($BenutzerLogin);
                $EMail = $ExportBenutzer->EMailAdresse();

                $this->_DebugAusgabe('Auftrag war von ' . $ExportBenutzer->BenutzerName() . '/' . $EMail . '/' . $rsXDZ->FeldInhalt('XDZ_BENACHRICHTIGUNG'), 6);

                if ($EMail != '' AND ($rsXDZ->FeldInhalt('XDZ_BENACHRICHTIGUNG') == 2 OR $rsXDZ->FeldInhalt('XDZ_BENACHRICHTIGUNG') == 3)) {
                    $this->_DebugAusgabe('Mail wird gesendet', 3);
                    $this->_Mailer->LoescheAdressListe();
                    $this->_Mailer->Betreff('Datenexport erfolgreich');
                    $this->_Mailer->SetzeVersandPrioritaet(30);
                    $this->_Mailer->SetzeBezug('XDZ', $rsXDZ->FeldInhalt('XDZ_KEY'));
                    $this->_Mailer->AdressListe(awisMailer::TYP_TO, $EMail, false, awisMailer::PRUEFAKTION_KEINE);
                    $this->_Mailer->Text('Datenexport ohne Fehler abgeschlossen.', awisMailer::FORMAT_HTML, true);
                    $this->_Mailer->Prioritaet(awisMailer::PRIO_NORMAL);
                    $this->_Mailer->MailInWarteschlange();
                }

                $BasisPfad = '/daten/web/dokumente/datenexport/';

                $BasisPfad .= $rsXDZ->FeldInhalt('XDZ_XAB_KEY');
                if (!is_dir($BasisPfad)) {
                    mkdir($BasisPfad, 0770);
                }

                // Dateiname
                $DateiName = $rsXDZ->FeldInhalt('XDZ_DATEINAME');

                $DateiName = str_replace('$NAM', $rsXAB->FeldInhalt('XAB_BEZEICHNUNG'), $DateiName);
                $DateiName = str_replace('$TAG', date('d'), $DateiName);
                $DateiName = str_replace('$MON', date('m'), $DateiName);
                $DateiName = str_replace('$JAH', date('Y'), $DateiName);
                $DateiName = str_replace('$STD', date('H'), $DateiName);
                $DateiName = str_replace('$MIN', date('i'), $DateiName);
                $DateiName = $BasisPfad . '/' . $DateiName;

                // Datei umkopieren ins Ziellaufwerk
                $this->_DebugAusgabe('Datei von  ' . $TEMPDatei . ' nach ' . $DateiName . ' kopieren...', 3);
                copy($TEMPDatei, $DateiName);

                //Tempdatei l�schen
                unlink($TEMPDatei);

                // N�chsten Termin setzen
                $this->NeuenTerminSetzen($XDZ_KEY);

            } catch (Exception $e) {
                if ($EMail != '' AND ($rsXDZ->FeldInhalt('XDZ_BENACHRICHTIGUNG') == 2 OR $rsXDZ->FeldInhalt('XDZ_BENACHRICHTIGUNG') == 4)) {
                    $this->_DebugAusgabe('Mail wird gesendet', 3);

                    $this->_Mailer->LoescheAdressListe();
                    $this->_Mailer->SetzeBezug('XDZ', $rsXDZ->FeldInhalt('XDZ_KEY'));
                    $this->_Mailer->SetzeVersandPrioritaet(30);
                    $this->_Mailer->Betreff('Datenexport fehlerhaft');
                    $this->_Mailer->AdressListe(awisMailer::TYP_TO, $EMail, false, awisMailer::PRUEFAKTION_KEINE);
                    $this->_Mailer->Text('Datenexport konnte nicht ausgefuehrt werden.', awisMailer::FORMAT_HTML, true);
                    $this->_Mailer->Prioritaet(awisMailer::PRIO_HOCH);
                    $this->_Mailer->MailInWarteschlange();
                }
            }
            $rsXDZ->DSWeiter();
        }
    }

    /**
     * Setzt den neuen Starttermin f�r einen Export
     *
     * @param int $XDZ_KEY
     */
    public function NeuenTerminSetzen($XDZ_KEY)
    {
        $SQL = 'SELECT EXPORTABFRAGENZEITPUNKTE.*';
        $SQL .= ' FROM EXPORTABFRAGENZEITPUNKTE';
        $SQL .= ' WHERE XDZ_KEY = ' . $this->_DB->WertSetzen('XDZ', 'N0', $XDZ_KEY);

        $rsXDZ = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('XDZ'));

        $this->_DebugAusgabe('Neuen Terminplan fuer ' . $XDZ_KEY . ' setzen...', 7);

        if ($rsXDZ->EOF()) {
            die('Fehler beim Terminplan setzen');
        }

        $StartNeu = strtotime($rsXDZ->Feldinhalt('XDZ_NAECHSTERSTART'));

        if ($StartNeu < 0) {
            $StartNeu = time();
        }

        switch ((int)$rsXDZ->FeldInhalt('XDZ_STANDARDINTERVALL')) {
            case 0:            // Einmaliger Import
                $this->_DebugAusgabe('War ein einmaliger Export.', 7);

                $SQL = 'UPDATE EXPORTABFRAGENZEITPUNKTE';
                $SQL .= ' SET XDZ_NAECHSTERSTART = NULL';
                $SQL .= ' WHERE XDZ_KEY = ' . $this->_DB->WertSetzen('XDZ', 'N0', $XDZ_KEY);

                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('XDZ'));
                break;

            case 1:            // T�glicher Export
                $this->_DebugAusgabe('Taeglicher Export.', 7);

                while ($StartNeu < time()) {
                    $StartNeu += (60 * $rsXDZ->Feldinhalt('XDZ_INTERVALL'));
                }

                $StartZeit = date('H:i:s', $StartNeu);
                if ($StartZeit == '00:00:00') {
                    $StartZeit == '00:00:01';
                }

                $Tag = 0;
                if (strtotime($StartZeit) < strtotime($rsXDZ->FeldInhalt('XDZ_ZEITVON'))) {
                    $StartZeit = $rsXDZ->Feldinhalt('XDZ_ZEITVON');
                } elseif (strtotime($StartZeit) > strtotime($rsXDZ->FeldInhalt('XDZ_ZEITBIS'))) {
                    $Tag += 1;
                    $StartZeit = $rsXDZ->Feldinhalt('XDZ_ZEITVON');
                }
                $StartNeu = date('d.m.Y', ($StartNeu + ($Tag * (60 * 60 * 24)))) . ' ' . $StartZeit;
                // Wochentage ber�cksichtigen
                $TageArray = explode(';', $rsXDZ->Feldinhalt('XDZ_SERVICETAGE'));
                $i = 0;
                $StartNeu = strtotime($StartNeu);

                while (array_search(date('w', $StartNeu), $TageArray) === false) {
                    // Einen Tag weiter, fr�hester Termin!
                    $StartNeu = strtotime(date('d.m.Y', ($StartNeu + (60 * 60 * 24))) . ' ' . $StartZeit);
                    if ($i++ > 10) {
                        break;
                    }
                }

                $this->_DebugAusgabe('Naechster Termin:' . date('d.m.Y H:i:s', $StartNeu), 7);

                $SQL = 'UPDATE EXPORTABFRAGENZEITPUNKTE';
                $SQL .= ' SET XDZ_NAECHSTERSTART = ' . $this->_DB->WertSetzen('XDZ', 'DU', date('d.m.Y H:i:s', $StartNeu));
                $SQL .= ' WHERE XDZ_KEY = ' . $this->_DB->WertSetzen('XDZ', 'N0', $XDZ_KEY);

                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('XDZ'));

                break;
            case 2:
                $this->_DebugAusgabe('Woechentlicher Export.', 7);

                while ($StartNeu < time()) {
                    $StartNeu += (60 * $rsXDZ->Feldinhalt('XDZ_INTERVALL'));
                }

                $StartZeit = date('H:i:s', $StartNeu);
                if ($StartZeit == '00:00:00') {
                    $StartZeit == '00:00:01';
                }

                $Tag = 0;
                if (strtotime($StartZeit) < strtotime($rsXDZ->FeldInhalt('XDZ_ZEITVON'))) {
                    $StartZeit = $rsXDZ->Feldinhalt('XDZ_ZEITVON');
                } elseif (strtotime($StartZeit) > strtotime($rsXDZ->FeldInhalt('XDZ_ZEITBIS'))) {
                    $Tag += 1;
                    $StartZeit = $rsXDZ->Feldinhalt('XDZ_ZEITVON');
                }

                $StartNeu = date('d.m.Y', ($StartNeu + ($Tag * (60 * 60 * 24)))) . ' ' . $StartZeit;
                // Wochentage ber�cksichtigen
                $TageArray = explode(';', $rsXDZ->Feldinhalt('XDZ_SERVICETAGE'));
                $i = 0;
                $StartNeu = strtotime($StartNeu);

                while (array_search(date('w', $StartNeu), $TageArray) === false) {
                    // Einen Tag weiter, fr�hester Termin!
                    $StartNeu = strtotime(date('d.m.Y', ($StartNeu + (60 * 60 * 24))) . ' ' . $StartZeit);
                    if ($i++ > 10) {
                        break;
                    }
                }

                $this->_DebugAusgabe('Naechster Termin:' . date('d.m.Y H:i:s', $StartNeu), 7);

                $SQL = 'UPDATE EXPORTABFRAGENZEITPUNKTE';
                $SQL .= ' SET XDZ_NAECHSTERSTART = ' . $this->_DB->WertSetzen('XDZ', 'DU', date('d.m.Y H:i:s', $StartNeu));
                $SQL .= ' WHERE XDZ_KEY = ' . $this->_DB->WertSetzen('XDZ', 'N0', $XDZ_KEY);

                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('XDZ'));

                break;
        }
    }

    /**
     * Liest oder setzt den Debuglevel
     *
     * @param int $NeuerLevel
     *
     * @return int
     */
    public function DebugLevel($NeuerLevel = null)
    {
        if (!is_null($NeuerLevel)) {
            $this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0', $NeuerLevel);
        }

        return $this->_DebugLevel;
    }

    /**
     * Erzeugt die SQL Anweisung aus den Bedingungen
     *
     * @param awisDatenbank $DB
     * @param int           $XDE_KEY
     * @param int           $XAB_KEY
     * @param string        $SQLPlusTrennzeichen
     *
     * @return string
     */
    function ErstelleAbfrage($XDE_KEY, $XAB_KEY = 0, $Sprache = 'DE', $NurFelder = false, $EinFeld = false, $SQLPlusTrennzeichen = ';')
    {
        // Basisdaten zur Abfrage laden
        $SQL = 'SELECT *';
        $SQL .= ' FROM ExportDaten ';
        $SQL .= ' LEFT OUTER JOIN ExportAbfragen ON XAB_XDE_KEY = XDE_KEY AND XAB_KEY=' . $this->_DB->WertSetzen('XAB', 'N0', $XAB_KEY);
        $SQL .= ' WHERE ROWNUM = 1 AND XDE_KEY=' . $this->_DB->WertSetzen('XAB', 'N0', $XDE_KEY);
        $rsXAB = $this->_DB->RecordsetOeffnen($SQL, $this->_DB->Bindevariablen('XAB'));

        // Jetzt alle Felder, die in der Auswahl stehen
        $SQL = 'SELECT *';
        $SQL .= ' FROM ExportDatenFelder ';
        $SQL .= ' WHERE XDF_XDE_KEY =' . $this->_DB->WertSetzen('XDF', 'N0', $XDE_KEY);
        $SQL .= ' ORDER BY XDF_SORTIERUNG';

        $rsXAK = $this->_DB->RecordsetOeffnen($SQL, $this->_DB->Bindevariablen('XDF'));

        $rsXAKZeilen = $rsXAK->AnzahlDatensaetze();
        $ParameterListe = array();

        $BASISSQL = $rsXAB->FeldInhalt('XDE_SQL');            // Basis Abfrage

        $FeldNamen = '';
        // Hier stehen alle Felder, die ausgegeben werden sollen
        $Felder = explode(',', $rsXAB->FeldInhalt('XAB_FELDLISTE'));

        if (count($Felder) == 0 OR $Felder[0] == '') {
            $SELECT = ' ROWNUM AS XXX_ROWNUM';    // Pseudospalte ausgeben
        } else {
            $SELECT = '';
            foreach ($Felder AS $Feld) {
                $rsXAK->DSErster();
                while (!$rsXAK->EOF()) {
                    if ($EinFeld)        // Liefert alle Spalten in einem Feld => SQLPLUS SPOOL
                    {
                        if ($rsXAK->FeldInhalt('XDF_KEY') == $Feld) {
                            if ($rsXAK->FeldInhalt('XDF_AUSDRUCK') == '')            // Wenn kein Ausdruck angegeben wurde, das Feld direkt nehmen
                            {
                                switch ($rsXAK->FeldInhalt('XDF_DATENTYP')) {
                                    case 'D---':
                                        $SELECT .= " || '$SQLPlusTrennzeichen' ||  TO_CHAR(" . $rsXAK->FeldInhalt('XDF_FELDNAME') . ",'DD.MM.YYYY')  ";
                                        break;
                                    default:
                                        $SELECT .= " || '$SQLPlusTrennzeichen' || " . $rsXAK->FeldInhalt('XDF_FELDNAME');
                                        break;
                                }
                            } else {
                                switch ($rsXAK->FeldInhalt('XDF_DATENTYP')) {
                                    case 'D---':
                                        $SELECT .= " || '$SQLPlusTrennzeichen' ||  TO_CHAR(" . $rsXAK->FeldInhalt('XDF_AUSDRUCK') . ",'DD.MM.YYYY') ";
                                        break;
                                    default:
                                        $SELECT .= " || '$SQLPlusTrennzeichen' ||  " . $rsXAK->FeldInhalt('XDF_AUSDRUCK');
                                        break;
                                }
                            }
                            break;
                        }
                    } else {
                        if ($rsXAK->FeldInhalt('XDF_KEY') == $Feld) {
                            $FeldNamen .= ',' . $rsXAK->FeldInhalt('XDF_FELDNAME');

                            if ($rsXAK->FeldInhalt('XDF_AUSDRUCK') == '')            // Wenn kein Ausdruck angegeben wurde, das Feld direkt nehmen
                            {
                                $SELECT .= ', ' . $rsXAK->FeldInhalt('XDF_FELDNAME');
                            } else {
                                $SELECT .= ', ' . $rsXAK->FeldInhalt('XDF_AUSDRUCK') . ' AS ' . $rsXAK->FeldInhalt('XDF_FELDNAME');
                            }
                            break;
                        }
                    }

                    $rsXAK->DSWeiter();
                }
            }
        }

        if ($NurFelder) {
            return substr($FeldNamen, 1);
        }
        if ($EinFeld) {        // Liefert alle Spalten in einem Feld => SQLPLUS SPOOL
            $SubStrStart = strlen($SQLPlusTrennzeichen);
            $SubStrStart += 9; //Pipes und Leerzeichen
            $SELECT = substr($SELECT, $SubStrStart); //Alle Zeichen die vor dem ersten Datenfeld kommen wegschneiden.
            $SELECT .= ' as EXPORTFELD ';
        }

        $rsXAK->DSErster();
        while (!$rsXAK->EOF()) {
            if (substr($rsXAK->FeldInhalt('XDF_FELDNAME'), 0, 2) == '$$') {
                $ParameterListe[$rsXAK->FeldInhalt('XDF_FELDNAME')] = $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), '', false);
            }
            $rsXAK->DSWeiter();
        }

        // Bedingung
        $SQL = 'SELECT *';
        $SQL .= ' FROM ExportAbfragenKriterien ';
        $SQL .= ' INNER JOIN ExportDatenFelder ON XAK_XDF_KEY = XDF_KEY';
        $SQL .= ' WHERE XAK_XAB_KEY = ' . $this->_DB->WertSetzen('XAK', 'N0', $XAB_KEY);
        $SQL .= ' ORDER BY XAK_GRUPPE, XAK_KEY';
        $rsXAK = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('XAK'));
        $rsXAKZeilen = $rsXAK->AnzahlDatensaetze();

        $Bedingung = '';
        $LetzteGruppe = 0;

        while (!$rsXAK->EOF()) {
            if (substr($rsXAK->FeldInhalt('XDF_FELDNAME'), 0, 2) == '$$')    // Pflichtparameter
            {
                $ParameterListe[$rsXAK->FeldInhalt('XDF_FELDNAME')] =
                    $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')), true);
            } else {
                // Neue Gruppe-> Klammern setzen
                if ($LetzteGruppe != $rsXAK->FeldInhalt('XAK_GRUPPE')) {
                    $Bedingung .= ($Bedingung == '' ? '' : ')');
                    $LetzteGruppe = $rsXAK->FeldInhalt('XAK_GRUPPE');
                    $Bedingung .= sprintf('%4s', $rsXAK->FeldInhalt('XAK_BEDINGUNGGRUPPE')) . ' (';
                } else {
                    $Bedingung .= sprintf('%4s', $rsXAK->FeldInhalt('XAK_BEDINGUNGGRUPPE')) . ' ';
                }
                // Feldname hinzufuegen
                $Bedingung .= $rsXAK->FeldInhalt('XDF_FELDNAME');

                // Bedingung hinzuf�gen
                switch ($rsXAK->FeldInhalt('XAK_VERKNUEPFUNG')) {
                    case 1:            // =
                        $Bedingung .= ' = ' . $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))) . '';
                        break;
                    case 2:            // >
                        $Bedingung .= ' > ' . $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))) . '';
                        break;
                    case 3:            // <
                        $Bedingung .= ' < ' . $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))) . '';
                        break;
                    case 4:            // >=
                        $Bedingung .= ' >= ' . $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))) . '';
                        break;
                    case 5:            // <=
                        $Bedingung .= ' <= ' . $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))) . '';
                        break;
                    case 6:            // <>
                        $Bedingung .= ' <> ' . $this->_DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'), $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))) . '';
                        break;
                    case 7:            // LEER
                        $Bedingung .= ' IS NULL';
                        break;
                    case 8:            // NICHT LEER
                        $Bedingung .= ' IS NOT NULL';
                        break;
                    case 9:            // Beginnt mit
                        $Bedingung .= ' LIKE \'' . $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')) . '%\'';
                        break;
                    case 10:            // Endet mit
                        $Bedingung .= ' LIKE \'%' . $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')) . '\'';
                        break;
                    case 11:            // Enth�lt
                        $Bedingung .= ' LIKE \'%' . $this->_ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')) . '%\'';
                        break;
                }
            }

            $rsXAK->DSWeiter();
        }
        $Bedingung .= ($Bedingung == '' ? '' : ')');

        $Erg = ' SELECT ' . substr($SELECT, 1);
        $Erg .= ' FROM (';
        $Erg .= $BASISSQL;
        $Erg .= ')';
        if ($Bedingung != '') {
            $Erg .= ' WHERE ' . substr($Bedingung, 4);
        }

        // Alle festen Parameter ersetzen
        foreach ($ParameterListe AS $Parameter => $Wert) {
            $Erg = str_replace($Parameter, $Wert, $Erg);
        }

        return $Erg;
    }

    /**
     * Ersetzt Parameter durch Ihre Werte
     *
     * @param string $Parameter
     */
    private function _ErsetzeParameter($Parameter)
    {
        $Erg = $Parameter;

        if (substr($Parameter, 0, 1) == '$') {
            switch (strtoupper(substr($Parameter, 1))) {
                case 'DATUM':
                    $Erg = date('d.m.Y');
                    break;
                case 'MONAT':
                    $Erg = date('m');
                    break;
                case 'TAG':
                    $Erg = date('d');
                    break;
                case 'TAGE':
                    $Erg = date('z');
                    break;
                case 'JAHR':
                    $Erg = date('Y');
                    break;
                case 'MONATSERSTER':
                    $Erg = date('d.m.Y', mktime(0, 0, 0, date('m'), 1, date('Y')));
                    break;
                case 'MONATSLETZTER':
                    $Erg = date('d.m.Y', mktime(0, 0, 0, date('m') + 1, 0, date('Y')));
                    break;
                case 'BENUTZER':
                    require_once('awisBenutzer.inc');
                    $Benutzer = awisBenutzer::Init();
                    $Erg = $Benutzer->BenutzerName();
                    break;
                case 'BENUTZERID':
                    require_once('awisBenutzer.inc');
                    $Benutzer = awisBenutzer::Init();
                    $Erg = $Benutzer->BenutzerID();
                    break;
                default:
                    $Erg = '';
            }
        }

        return $Erg;
    }

    /**
     * Erzeugt eine Ausgabe f�r Debugzwecke
     *
     * @param string $Text       Test f�r die Ausgabe
     * @param int    $DebugLevel Debuglevel, ab dem die Ausgabe erfolgen soll
     * @param string $Kanal      STDOUT oder STDERR
     */
    private function _DebugAusgabe($Text, $DebugLevel, $Kanal = STDOUT)
    {
        if ($this->_DebugLevel >= $DebugLevel) {
            $Ausgabe = date('c');
            $Ausgabe .= ';' . getenv('HOST') . ';DatenExport' . ';' . $Text . PHP_EOL;

            fwrite($Kanal, $Ausgabe);
        }
    }

    /**
     * Funktion pr�ft, ob der Jobscheduler aktiv ist
     * Liefert FALSE, wenn der Prozess noch l�uft, TRUE wenn er nicht l�uft oder beendet wurde
     */
    public function Watchdog()
    {
        if (!is_file($this->_PID_Datei)) {
            return false;
        }
        $PID = file_get_contents($this->_PID_Datei);
        if ($PID == $this->_PID) {
            $this->_DebugAusgabe('Ich laufe noch...', 3);

            return false;
        }

        $Meldung = system('ps -j ' . $PID . ' | grep ' . $PID . ' -c');
        $this->_DebugAusgabe('Pruefe Job ' . $PID . ' :' . $Meldung, 5);
        if ($Meldung == '0') {
            $this->_DebugAusgabe('Prozess nicht vorhanden.', 5);
            unlink($this->_PID_Datei);

            return false;
        } else {
            $this->_DebugAusgabe('  Anderer Scheduler laeuft auf diesem Knoten.', 5);
        }

        return true;
    }

    /***
     * L�scht alte Dateien nach einer gewissen Zeit
     */
    public function LoescheAlteDateien()
    {

        $SQL = 'select distinct XAB_KEY, XAB_XBN_KEY, XBL_LOGIN from exportabfragen ';
        $SQL .= ' inner join BENUTZER on XAB_XBN_KEY = XBN_KEY ';
        $SQL .= ' inner join benutzerlogins on XBL_XBN_KEY = XBN_KEY ';

        $rsXAB = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsXAB->EOF()) {
            $Pfad = '/daten/web/dokumente/datenexport/';
            $Pfad .= $rsXAB->FeldInhalt('XAB_KEY') . '/';

            if (!is_dir($Pfad)) {
                $rsXAB->DSWeiter();
                continue;
            }

            $Ordner = opendir($Pfad);

            try {
                $ExportNutzer = awisBenutzer::Init($rsXAB->FeldInhalt('XBL_LOGIN'));

                $MaxAlter = $ExportNutzer->ParameterLesen('DatenexporteDownloadsLoeschZeitraum', true);
            } catch (Exception $e) {
                //Ausgetretener oder gel�schter User
                $MaxAlter = 30;
            }

            while (($ExportDatei = readdir($Ordner)) !== false) {
                if ($ExportDatei != '.' && $ExportDatei != '..') {
                    $ExportDateiAlter = time() - filemtime($Pfad . $ExportDatei);
                    $ExportDateiAlter = round($ExportDateiAlter / 86400);

                    if ($ExportDateiAlter > $MaxAlter) {
                        $this->_DebugAusgabe(
                            'L�sche Exportdatei: ' . $Pfad . $ExportDatei . ' da sie ' . $ExportDateiAlter .
                            ' Tage alt ist. Das maximale Alter f�r den User: ' . $rsXAB->FeldInhalt('XBL_LOGIN') .
                            ' ber�gt ' . $MaxAlter
                            ,
                            1
                        );
                        unlink($Pfad . $ExportDatei);
                    }
                }
            }
            $rsXAB->DSWeiter();
        }
    }

}

?>
