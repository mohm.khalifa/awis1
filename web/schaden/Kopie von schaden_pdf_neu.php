<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
//require_once("mc_table.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $DBServer;

//require_once("register.inc.php");
//require_once("db.inc.php");
//require_once("sicherheit.inc.php");
require_once 'fpdi.php';

clearstatcache();

include ("ATU_Header.php");

$DBServer = awisDBServer();

$conSchad = awisAdminLogon($DBServer, "schaddev09", "schad");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3300);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Schaden-PDF',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug(1,$_REQUEST);

if ($_REQUEST['BEARB_NR']!='' && ($RechteStufe&64)==64)
{
	/***************************************
	* Hole Daten per SQL
	***************************************/
	
	//$SQL="SELECT * FROM SCHAEDEN_NEW@SCHAD.ATU.DE, SACHBEARBEITER@SCHAD.ATU.DE AA, EINGANGPER@SCHAD.ATU.DE BB";
	//$SQL.=" WHERE BEARBEITER=AA.ID(+) AND EINGANGPER=BB.ID(+) AND BEARBEITUNGSNR='".$_REQUEST['BEARB_NR']."'";
	
	$SQL="SELECT SCH.BEARBEITUNGSNR, SCH.EINGABEAM, SCH.FILNR, SCH.GESCHLECHT, SCH.KFZ_KENNZ,";
	$SQL.=" SCH.KUNDENNAME, SCH.PLZ, SCH.ORT, SCH.STRASSE, SCH.TELEFON, SCH.TELEFON2,";
	$SQL.=" SCH.FAX, SCH.WANR, SCH.PAN, SCH.EMAIL, SCH.VORNAME, SCH.TERMIN_KUNDE_1,";
	$SQL.=" SCH.TERMIN_KUNDE_2, SCH.TERMIN_KUNDE_WER_1, SCH.TERMIN_KUNDE_WER_2, SCH.EINGABEDURCH_FIL,";
	$SQL.=" SB.SBNAME as SACHBEARBEITER, EP.MITTEL as EINGANG, KK.WERT as KONTAKT,";
	$SQL.=" IGBL.WERT as INFOGBL, KEN.KENNUNG_BEZEICHNUNG as KENBEZEICHNUNG, AUF.AUFTRAGSART_ATU as AUFTRAGSART,";
	$SQL.=" SGR.GRUND as SCHADENSGRUND, BES.SCHLAGWORT as BESCHAEDIGT, AU.WERT as AUSFALLURSACHE,";
	$SQL.=" STA.BESCHREIBUNG as STAND, to_clob(SCH.FEHLERBESCHREIBUNG) as FEHLERBESCHREIBUNG";
	//$SQL.=" FROM SCHAEDEN_NEW@SCHAD.ATU.DE SCH";			
	$SQL.=" FROM SCHAEDEN_NEW SCH";			
	//$SQL.=" LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE SB ON SB.ID = BEARBEITER";
	$SQL.=" LEFT JOIN SACHBEARBEITER SB ON SB.ID = BEARBEITER";
	//$SQL.=" LEFT JOIN EINGANGPER@SCHAD.ATU.DE EP ON EP.ID = EINGANGPER";
	$SQL.=" LEFT JOIN EINGANGPER EP ON EP.ID = EINGANGPER";
	//$SQL.=" LEFT JOIN KONTAKT_KUNDE@SCHAD.ATU.DE KK ON KK.ID = KONTAKT_KUNDE";
	$SQL.=" LEFT JOIN KONTAKT_KUNDE KK ON KK.ID = KONTAKT_KUNDE";
	//$SQL.=" LEFT JOIN INFO_AN_GBL@SCHAD.ATU.DE IGBL ON IGBL.ID = INFO_AN_GBL";
	$SQL.=" LEFT JOIN INFO_AN_GBL IGBL ON IGBL.ID = INFO_AN_GBL";
	//$SQL.=" LEFT JOIN KENNUNGEN@SCHAD.ATU.DE KEN ON KEN.KENNUNG = SCH.KENNUNG";
	$SQL.=" LEFT JOIN KENNUNGEN KEN ON KEN.KENNUNG = SCH.KENNUNG";
	//$SQL.=" LEFT JOIN AUFTRAGSARTEN@SCHAD.ATU.DE AUF ON ART_ID = AUFTRAGSART_ATU_NEU";
	$SQL.=" LEFT JOIN AUFTRAGSARTEN AUF ON ART_ID = AUFTRAGSART_ATU_NEU";
	//$SQL.=" LEFT JOIN SCHADENSGRUND@SCHAD.ATU.DE SGR ON SGR.ID = SCHADENSGRUND";
	$SQL.=" LEFT JOIN SCHADENSGRUND SGR ON SGR.ID = SCHADENSGRUND";
	//$SQL.=" LEFT JOIN BESCHAEDIGT@SCHAD.ATU.DE BES ON BES.BID = SCH.BID";
	$SQL.=" LEFT JOIN BESCHAEDIGT BES ON BES.BID = SCH.BID";
	//$SQL.=" LEFT JOIN AUSFALLURSACHE@SCHAD.ATU.DE AU ON AU.ID = AUSFALLURSACHE";
	$SQL.=" LEFT JOIN AUSFALLURSACHE AU ON AU.ID = AUSFALLURSACHE";
	//$SQL.=" LEFT JOIN STAND@SCHAD.ATU.DE STA ON STANDID = STAND";
	$SQL.=" LEFT JOIN STAND STA ON STANDID = STAND";
	$SQL.=" WHERE BEARBEITUNGSNR='".$_REQUEST['BEARB_NR']."'";	
	
	awis_Debug(1,$SQL);
	
	$rsResult = awisOpenRecordset($conSchad,$SQL);
	$rsResultZeilen = $awisRSZeilen;
	
	
	//, SACHBEARBEITER@SCHAD.ATU.DE AA, EINGANGPER@SCHAD.ATU.DE BB";
	//$SQL.=" WHERE BEARBEITER=AA.ID(+) AND EINGANGPER=BB.ID(+) AND BEARBEITUNGSNR='".$_REQUEST['BEARB_NR']."'";	

	if($rsResultZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Keine Daten f�r Bearbeitungs-Nr.: ".$_REQUEST['BEARB_NR']." gefunden!</span></center>");
	}
	else
	{
		$BEARBEITER=$rsResult['SACHBEARBEITER'][0];
		$BEARBEITUNGSNR=$rsResult['BEARBEITUNGSNR'][0];
		$EINGABEAM=$rsResult['EINGABEAM'][0];
		$EINGANGPER=$rsResult['EINGANG'][0];
		$FEHLERBESCHREIBUNG=$rsResult['FEHLERBESCHREIBUNG'][0];
//		$FEHLERBESCHREIBUNG=preg_replace('/[\r\n]+/',chr(13).chr(10),$rsResult['FEHLERBESCHREIBUNG'][0]);
		$FEHLERBESCHREIBUNG=preg_replace('/[\r\n]+/',' ',$rsResult['FEHLERBESCHREIBUNG'][0]);
		$FILNR=$rsResult['FILNR'][0];
		$GESCHLECHT=$rsResult['GESCHLECHT'][0];
		$KFZ_KENNZ=$rsResult['KFZ_KENNZ'][0];
		$KUNDENNAME=$rsResult['KUNDENNAME'][0];
		$PLZ=$rsResult['PLZ'][0];
		$ORT=$rsResult['ORT'][0];
		$STRASSE=$rsResult['STRASSE'][0];
		$TELEFON=$rsResult['TELEFON'][0];
		$TELEFON2=$rsResult['TELEFON2'][0];
		$FAX=$rsResult['FAX'][0];
		$WANR=$rsResult['WANR'][0];
		$PAN=$rsResult['PAN'][0];
		$EMAIL=$rsResult['EMAIL'][0];
		$VORNAME=$rsResult['VORNAME'][0];
		$KONTAKT_KUNDE=$rsResult['KONTAKT'][0];
		$INFOANGBL=$rsResult['INFOGBL'][0];
		$KENNUNG=$rsResult['KENBEZEICHNUNG'][0];
		$AUFTRAGSART=$rsResult['AUFTRAGSART'][0];
		$SCHADENSGRUND=$rsResult['SCHADENSGRUND'][0];
		$BESCHAEDIGT=$rsResult['BESCHAEDIGT'][0];
		$AUSFALLURSACHE=$rsResult['AUSFALLURSACHE'][0];
		$TERMINKUNDE1=$rsResult['TERMIN_KUNDE_1'][0];
		$TERMINKUNDE2=$rsResult['TERMIN_KUNDE_2'][0];
		$TERMINKUNDEWER1=$rsResult['TERMIN_KUNDE_WER_1'][0];
		$TERMINKUNDEWER2=$rsResult['TERMIN_KUNDE_WER_2'][0];
		$STAND=$rsResult['STAND'][0];
		$EINGABEDURCH=$rsResult['EINGABEDURCH_FIL'][0];
	}	

		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
		
		define('FPDF_FONTPATH','font/');
		$pdf = new fpdi('p','mm','a4');		
		$pdf->Open();		
		//$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
		$pdf->setSourceFile('/daten/web/dokumente/vorlagen/PDF_klein_Logo_farbig_2008.pdf');	
		$ATULogo = $pdf->ImportPage(1);				
		
		$Zeile=0;
		$Seitenrand=15;
		
		NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true);
		
		$LinkerRand=$Seitenrand;
			
		$Y_Wert=16;
				
		$Y_Wert=$Y_Wert+5;			
		
		$Y_Wert=$Y_Wert+18;
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(130,6,"Kundendaten",1,0,'L',1);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(50,6,"Bearbeitungsdaten",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+6;
		$pdf->SetFont('Arial','',9);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Name:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$KUNDENNAME,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"WA-Nr.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(35,6,$WANR,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Bearb-Nr.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$BEARBEITUNGSNR,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Vorname :",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$VORNAME,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"Fz.-Kennz.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(35,6,$KFZ_KENNZ,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Sachbearb.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$BEARBEITER,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Stra�e :",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$STRASSE,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"ATU-Card:",0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Filial-Nr.:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$FILNR,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"PLZ / Ort :",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$PLZ." ".$ORT,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,$PAN,0,0,'L',0);
	
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Erfasst am:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$EINGABEAM,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Tel. 1:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$TELEFON,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"FAX:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(35,6,$FAX,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(35,6,"Eingang per:",0,0,'L',0);
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(35,6,$EINGANGPER,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Tel. 2:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$TELEFON2,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"E-Mail:",0,0,'L',0);
		$pdf->setXY($LinkerRand+16,$Y_Wert);
		$pdf->cell(35,6,$EMAIL,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+6;
		
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,"Beschwerde / Kommentar",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+7;
		$pdf->SetFont('Arial','',9);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->MultiCell(180,4,$FEHLERBESCHREIBUNG,0,'N',0);
		
		//$bla=$pdf->getStringWidth($FEHLERBESCHREIBUNG);
		//$Hoehe=round($bla/46+0.4,0);
		//$Y_Wert=$Y_Wert+$Hoehe+6;
		
		$pdf->Ln();
		$Y_Wert=$pdf->GetY();
		
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,"Beschwerdebearbeitung / R�ckmeldung Filiale",1,0,'L',1);
		
		$Y_Wert=$Y_Wert+12;
						
		$pdf->SetFont('Arial','',9);
		$Y_Wert=$Y_Wert-4;	
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Kontakt mit Kunde:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$KONTAKT_KUNDE,0,0,'L',0);

		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Termin mit Kunden am:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$TERMINKUNDE1,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(45,6,"Wer:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(45,6,$TERMINKUNDEWER1,0,0,'L',0);

		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Termin mit Kunden am:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$TERMINKUNDE2,0,0,'L',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(35,6,"Wer:",0,0,'L',0);
		$pdf->setXY($LinkerRand+88,$Y_Wert);
		$pdf->cell(35,6,$TERMINKUNDEWER2,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Info an GBL:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$INFOANGBL,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Kennung:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$KENNUNG,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Urspr. Auftrag:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$AUFTRAGSART,0,0,'L',0);

		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Woran gearbeitet:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$SCHADENSGRUND,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Was besch�digt:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$BESCHAEDIGT,0,0,'L',0);
		
		if ($AUSFALLURSACHE!='')
		{
			$Y_Wert=$Y_Wert+4;
		
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(35,6,"Ausfallursache:",0,0,'L',0);
			$pdf->setXY($LinkerRand+35,$Y_Wert);
			$pdf->cell(35,6,$AUSFALLURSACHE,0,0,'L',0);
		}
		
//		$Y_Wert=$Y_Wert+4;
		
//		$pdf->setXY($LinkerRand,$Y_Wert);
//		$pdf->cell(35,6,"Bearbeitet durch:",0,0,'L',0);
//		$pdf->setXY($LinkerRand+35,$Y_Wert);
//		$pdf->cell(35,6,$EINGABEDURCH,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+4;
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(35,6,"Bearbeitungsstand:",0,0,'L',0);
		$pdf->setXY($LinkerRand+35,$Y_Wert);
		$pdf->cell(35,6,$STAND,0,0,'L',0);
			
		$Y_Wert=$Y_Wert+10;
		
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(180,6,"Diagnose / Ergebnis und Besprochene Ma�name",1,0,'L',1);		

		//$SQL = "SELECT B.BEARBNRNEU, substr(B.BEMERKUNGEN,1,2000) as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
		$SQL = "SELECT B.BEARBNRNEU, to_clob(B.BEMERKUNGEN) as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW B";
		//$SQL .= " WHERE BEARBNRNEU=(SELECT BEARBNRNEU FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='". $BEARBEITUNGSNR ."') ORDER BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";
		$SQL .= " WHERE BEARBNRNEU=(SELECT BEARBNRNEU FROM SCHAEDEN_NEW WHERE BEARBEITUNGSNR='". $BEARBEITUNGSNR ."') ORDER BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";
		//$SQL = "SELECT B.BEARBNRNEU, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
		//$SQL = "SELECT B.BEARBNRNEU, B.DATUM, B.BEMERKUNGEN FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE B";
		//$SQL .= " WHERE BEARBNRNEU=".intval($rsSchad['BEARBNRNEU'][0]);	
		
		//$SQL = "SELECT * FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE ";
		//$SQL .= "WHERE BEARBNRNEU=(SELECT BEARBNRNEU FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='". $BEARBEITUNGSNR ."') ORDER BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";
		
		$rsSchadInfo = awisOpenRecordset($conSchad,$SQL,true,true);
		$rsSchadInfoZeilen = $awisRSZeilen;		
		
		$Y_Wert=$Y_Wert+7;
		$pdf->SetFont('Arial','B',9);
		
		$pdf->setXY($LinkerRand,$Y_Wert);
		
		$pdf->setX($LinkerRand);
		$pdf->cell(20,5,'Datum',1,0,'L',0);			
		$pdf->setX($LinkerRand+20);
		$pdf->cell(35,5,'Eintrag',1,0,'L',0);
		$pdf->setX($LinkerRand+55);
		$pdf->cell(15,5,'Betrag',1,0,'L',0);
		$pdf->setX($LinkerRand+70);
		$pdf->cell(20,5,'Zahlungsart',1,0,'L',0);
		$pdf->setX($LinkerRand+90);			
		$pdf->MultiCell(90,5,'Diagnose / Ergebnis',1);
		$pdf->Ln(1);		
		$pdf->SetFont('Arial','',9);
    					
		$Y_Wert2=$Y_Wert;
			
		for ($i=0;$i<$rsSchadInfoZeilen;$i++)
		{
			$bla=$pdf->getStringWidth($rsSchadInfo['BEMERKUNG'][$i]);
			$Hoehe=round($bla/46+0.4,0);
			$Y_Wert2=$Y_Wert2+$Hoehe;
			
			if ($Y_Wert2 > 280)
			{
				$Y_Wert=16;
				$Y_Wert=$Y_Wert+10;				
				
				$Zeile=0;
				$Seitenrand=15;
		
				NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true);
				$pdf->setXY($LinkerRand,$Y_Wert);				
			}
			
			
			$pdf->setX($LinkerRand);
			$pdf->cell(20,5,$rsSchadInfo['DATUM'][$i],0,0,'L',0);			
			$pdf->setX($LinkerRand+20);
			$pdf->cell(35,5,$rsSchadInfo['EINTRAG'][$i],0,0,'L',0);
			$pdf->setX($LinkerRand+55);
			$pdf->cell(15,5,$rsSchadInfo['BETRAG'][$i],0,0,'L',0);
			$pdf->setX($LinkerRand+70);
			$pdf->cell(20,5,$rsSchadInfo['ZAHLUNGSART'][$i],0,0,'L',0);
			$pdf->setX($LinkerRand+90);			
			$pdf->MultiCell(90,5,$rsSchadInfo['BEMERKUNG'][$i],0);
			$pdf->Ln(1);								        	        	
			$Y_Wert2=$pdf->GetY();
		}
		
		
		//$pdf->SetFont('Arial','B',12);
		//$pdf->setXY($LinkerRand,$Y_Wert);
		//$pdf->MultiCell(180,6,$Protokolltext4,1,'C',0);
		
		/*** Protokoll - Zusatz Beginn ***/
		/*$pdf->addpage();
		$pdf->SetAutoPageBreak(true,0);
		$pdf->useTemplate($ATULogo,$Seitenrand+170,4,20);
		$pdf->useTemplate($Template);
		$pdf->SetFont('Arial','',10);
		$pdf->setXY(25,10);
		$pdf->cell(50,8,'Bearbeitungsnummer: 070909999',0,0,'L',0);
		$pdf->setXY(90,10);
		$pdf->cell(50,8,'Kundenname: Max Mustermann',0,0,'L',0);*/
		/*** Protokoll - Zusatz Ende ***/
		
		$DateiName = awis_UserExportDateiName('.pdf');
		$DateiNameLink = pathinfo($DateiName);
		$DateiNameLink = '/export/' . $DateiNameLink['basename'];
		$pdf->saveas($DateiName);
		
		echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
		echo "<hr><a href=./schaden_Main.php><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
		
}	
	
/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Christian Argauer
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  bool �berschrift
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand)
{
	static $Seite;
	
	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,0,0);
	//$pdf->useTemplate($ATULogo,$LinkerRand+160,4,20);		// Logo einbauen
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
	
	
	$pdf->setXY($LinkerRand,10);					// Cursor setzen
	$pdf->SetFont('Arial','B',14);				// Schrift setzen
	
	// Ueberschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(180,6,"ATU Qualit�tsmanagement - Beschwerdeprotokoll -",0,0,'C',0);
	
//	$pdf->setXY($LinkerRand,283);				// Cursor setzen
//	$pdf->SetFont('Arial','',6);				// Schrift setzen
//	$pdf->cell(180,6,"Stand: " . date('d.m.Y'),0,0,'C',0);

	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=10;
	
	$Zeile = 22;
}	
	
	
?>
</body>
</html>
