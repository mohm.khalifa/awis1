<?php
global $CursorFeld;
global $AWISBenutzer;
global $con;
global $awisRSZeilen;

$AWISSprache = 'DE';
$CursorFeld='';

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

$Recht3610 = awisBenutzerRecht($con,3610);
 if ($Recht3610 == 0)
 {
     awisEreignis(3, 1000, 'ATU TV', $AWISBenutzer->BenutzerName(), '', '', '');
     die("Keine ausreichenden Rechte!");
 }
//awis_debug(1,$_REQUEST);

echo "<br>";

// Das Formular wird nur ge�ffnet, wenn nicht der 'Speicher' - Button gedr�ckt wurde

if (!isset ($_POST['cmdSpeichern_x']))
{
	
// Hier wird das Formular zur Einschr�nkung der Auswertung ge�ffnet

echo "<form name=frmAuswertungen method=post action=./atutv_Main.php?cmdAktion=Auswertungen>";

/**********************************************
* * Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('<b>Bitte w&auml;hlen Sie den Zeitraum der Auswertung aus:</b>',500);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('<b>(Datumsformat: tt.mm.yyyy z.B. 18.08.2007)</b><br><br>',500);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('<b>Zeitraum vom:</b>',150);
awis_FORM_Erstelle_TextFeld('datumvom','',10,110,true,'');
$CursorFeld='datumvom';
awis_FORM_Erstelle_TextLabel('<b>bis:</b>',50);
awis_FORM_Erstelle_TextFeld('datumbis','',10,10,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('<br><br>',200);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('<b>Bitte w&auml;hlen Sie den Bereich f&uuml;r die Auswertung aus:</b><br><br>',600);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
$SQL = 'SELECT ATT_KEY, ATT_BEZEICHNUNG FROM ATUTVFEEDBACKTYPEN ';
$SQL .= ' ORDER BY ATT_KEY';
$Auswahl ='ALLE';
awis_FORM_Erstelle_SelectFeld('bereich','',300,true,$con, $SQL,'0~' .$Auswahl,'', 'ATT_BEZEICHNUNG','','','');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('<br><br>',200);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('<b>Um ihre Auswertung anzuzeigen, dr&uuml;cken Sie bitte hier:<b/>',500);
echo "&nbsp;<input tabindex=91 accesskey=s type=image src=/bilder/eingabe_ok.png title='lbl_speichern (ALT+S)' name=cmdSpeichern>";
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();

}

// Wenn der 'Speicher' - Button gedr�ckt wurde, werden folgende Aktionen ausgef�hrt

if (isset ($_POST['cmdSpeichern_x']))
{
		$Bereich = 'Alle';
		$SQL = '';

		$SQL = 'SELECT a.tvf_att_id, a.tvf_text, b.att_bezeichnung, a.tvf_dat, b.att_key';
		$SQL .= ' FROM atutvfeedback a, atutvfeedbacktypen b';
		$SQL .= ' WHERE a.tvf_att_id = b.att_key';
	
		if ($_POST ['txtbereich'] == '1' )
		{
			$SQL .= ' AND b.att_key = 1';
			$Bereich = 'Positives Feedback';
		}
		elseif ($_POST ['txtbereich'] == '2' )
		{
			$SQL .= ' AND b.att_key = 2';
			$Bereich = 'Negatives Feedback';
		}
		elseif ($_POST ['txtbereich'] == '3' )
		{
			$SQL .= ' AND b.att_key= 3';
			$Bereich = 'Interessantes Thema';
		}
		elseif ($_POST ['txtbereich'] == '4' )
		{
			$SQL .= ' AND b.att_key = 4';
			$Bereich = 'Vermisse Thema';
		}
		elseif ($_POST ['txtbereich'] == '5' )
		{
			$SQL .= ' AND b.att_key = 5';
			$Bereich = 'Sonstiges';
		}

		if (($_POST ['txtdatumvom'] !== '') and ($_POST ['txtdatumbis'] == ''))
		{
			$SQL .= " AND trunc(a.tvf_dat) >= to_date('".$_POST['txtdatumvom']."','dd.mm.yyyy') AND trunc(a.tvf_dat) <= sysdate";
		}
		elseif (($_POST ['txtdatumvom'] == '') and ($_POST ['txtdatumbis'] !== ''))
		{
			$SQL .= " AND trunc(a.tvf_dat) <= to_date('".$_POST['txtdatumbis']."','dd.mm.yyyy')";
		}
		elseif (($_POST ['txtdatumvom'] !== '') and ($_POST ['txtdatumbis'] !== ''))
		{
			$SQL .= " AND trunc(a.tvf_dat) >= to_date('".$_POST['txtdatumvom']."','dd.mm.yyyy') AND trunc(a.tvf_dat) <= to_date('".$_POST['txtdatumbis']."','dd.mm.yyyy')";	
		}

	
			$SQL .= ' ORDER BY a.tvf_dat';
			//awis_debug(1,$SQL);


			$rsATV = awisOpenRecordset($con, $SQL);
			//awis_debug(1,$rsATV);
			$rsATVZeilen = $awisRSZeilen;
			$StartZeile = 0;



	awis_FORM_FormularStart();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('<b>Um wieder zur Auswahlbeschr&auml;nkung der Auswertung zur&uuml;ckzukommen, dr&uuml;cken Sie bitte hier:<b/>',850);
	echo "&nbsp;<input type=image name=cmdZurueck accesskey=n src=/bilder/radierer.png onclick=location.href='./atutv_Main.php?cmdAktion=Auswertungen'>";		
	awis_FORM_ZeileEnde();

	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('<b>Sie haben folgenden Bereich ausgew&auml;hlt:</b><br><br>',380);
	awis_FORM_Erstelle_TextFeld('bereichsanzeige','"'. $Bereich . '"', 300, 300);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift('DATUM',120,'','','');
	awis_FORM_Erstelle_Liste_Ueberschrift('TEXT',1000,'','','');
	awis_FORM_ZeileEnde();
	
	for($i=0;$i<$rsATVZeilen;$i++)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('DATUM',$rsATV['TVF_DAT'][$i],0,120,false,'','','','D');
		awis_FORM_Erstelle_ListenFeld('TEXT',$rsATV['TVF_TEXT'][$i],0,1000,false,'','','','T');
		
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('<br><br>',200);
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('Dr&uuml;cken sie bitte auf \'Datei &ouml;ffnen\' um das Excel-Format anzuzeigen!' ,700);
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
	
	//Tabelle auf Excel portieren
	
		$DateiName = awis_UserExportDateiName('.csv');
		$fd = fopen($DateiName,'w' );
	
		$Zeile ='';
		$Zeile .= 'Bereich:  ' .$Bereich ;
		$Zeile .= "\n";
		fputs($fd, $Zeile);
		
		$Zeile ='';
		$Zeile .= 'Datum;Text';
		$Zeile .= "\n";
		fputs($fd, $Zeile);
	
			for($i=0;$i<$rsATVZeilen;$i++)
			{
					$Zeile='';
					$Zeile .= $rsATV['TVF_DAT'][$i].';'.$rsATV['TVF_TEXT'][$i]."\n";
					fputs($fd, $Zeile);
			}
	//$Zeile ='';
	
		fclose($fd);
	
		$DateiName = pathinfo($DateiName);
		echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
		
		if($CursorFeld!='')
		{
			echo '<Script Language=JavaScript>';
			echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
			echo '</Script>';
		}
}	
	





?>