<?php

/**
 * Klasse zum Erstellen von Auswertungen (DeTeFleet) 
 * ************************************************************************
 * Parameter :
 * --- keine ---
 * R�ckgabeparameter:
 * --- keine ---
 * ***********************************************************************
 * Erstellt:
 * @author 26.10.2011 Stefan Oppl
 * @version 1.0
 * �nderungen:
 * 21.02.2013 OP : Methode fuer Export einer Materialnr/ATUNr Zuordnung
 *                 fuer EdiPlan
 * ***********************************************************************
 */

require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'fileCSV.inc.php';

class deteFleet_Auswertung
{
	
	/**
	 * Datenbankobjekt
	 * @var object
	 */
	private $_DB;
	
	/**
	 * AWIS-Benutzerobjekt
	 * @var object
	 */
	private $_AWISBenutzer;
	
	/**
	 * 
	 * Auswertungdatum als string im
	 * Format: "yyyymm"
	 * @var string
	 */
	private $_AuswertDatum;

    /**
     * Pfad unter dem die Auswertungen gespeichert werden sollen
     *
     * @var string
     */
	private $_AuswertPfad = '/daten/daten/teams/AWIS-Transfer/09-ATU-Card/01-DeTeFleet/';
	
	
	/**
	 * Benoetigte Objekte erstellen und Datum setzen
	 */
	public function __construct($auswertDatum = '')
	{
		$this->_AWISBenutzer = awisBenutzer::Init();
  		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
  		$this->_DB->Oeffnen();
  		
        if($auswertDatum == '')
        {
            $this->setAuswertDatum();
        }
        else
        {
            $this->_AuswertDatum = $auswertDatum;
        }
  	}
	
	/**
	 * Setzt das Auswertungsdatum. Dieses wird aus
	 * der Tabelle GKPREISLISTE selektiert.
	 */
	private function setAuswertDatum() 
	{
		$SQL = 'SELECT gkp_monat ';
		$SQL .= 'FROM gkpreisliste ';
		$SQL .= 'WHERE gkp_key = ';
		$SQL .= '(select max(gkp_key) as gkp_key ';
		$SQL .= 'FROM gkpreisliste)';
		
		$rsMonat = $this->_DB->RecordSetOeffnen($SQL);
		
		$this->_AuswertDatum = $rsMonat->FeldInhalt('GKP_MONAT');
	}
	
	/**
	 * Liefert das Auswertungsdatum zurueck
	 * @return string Auwertungsdatum (yyyymm)
	 */
	public function getAuswertDatum()
	{
		return $this->_AuswertDatum;
	}
	
	/**
	 * Erstellt die Auswertung "MatNr mit Preis" 
	 * und exportiert diese als CSV
	 */
	public function exportMatZuordnung() 
	{	
		/* Daten fuer Material-Zuordnungsdatei festlegen */
		$SQL = '';
		
		$SQL = 'SELECT gkz_auto as auto, ';
		$SQL .= 'gkz_materialnr as Materialnr, ';
		$SQL .= 'gkz_kurztext as Kurztext_org, ';
		$SQL .= 'gkz_preis as Preis, ';
		$SQL .= 'gkz_bemerkung as Bem, ';
		$SQL .= 'gkz_breite as BREITE, ';
		$SQL .= 'gkz_querschnitt as QUERSCHNITT, ';
		$SQL .= 'gkz_bauart as BAUART, ';
		$SQL .= 'gkz_innendurchmesser as INNENDM, ';
		$SQL .= 'gkz_loadindex1 as LOAD1, ';
		$SQL .= 'gkz_loadindex2 as LOAD2, ';
		$SQL .= 'gkz_speedindex as SP, ';
		$SQL .= 'gkz_seal || gkz_rof as "S+R", ';
		$SQL .= 'gkz_gws as GWS, ';
		$SQL .= 'gkz_rx as RX, ';
		$SQL .= 'gkz_wg as WG_Felgen_Kappen, ';
		$SQL .= 'gkz_sg as SG_Felgen_Kappen ';
		$SQL .= 'FROM gkpreisliste gkp ';
		$SQL .= 'INNER JOIN gkpreislistezuord gkz ';
		$SQL .= 'ON gkp.gkp_key = gkz.gkz_gkp_key ';
		$SQL .= "WHERE gkp.gkp_monat = '" . $this->_AuswertDatum . "'";
		
		$rsMatZuOrd = $this->_DB->RecordSetOeffnen($SQL);
		
		/* Datei oeffnen (schreibend) und Daten schreiben (Material-Zuordnungsdatei) */
		$fp = fopen($this->_AuswertPfad . 'DeTeFleet_MatZuord_' . $this->_AuswertDatum . '.csv', 'w');
		
		/*Ueberschrift in Datei schreiben*/
		$Zeile = 'Auto;Materialnr;Kurztext_org;Preis;Bem;Breite;Querschnitt;';
		$Zeile .= 'Bauart;Innendm;Load1;Load2;SP;S+R;GWS;RX;WG_Felgen_Kappen;';
		$Zeile .= 'SG_Felgen_Kappen';
		
		fwrite($fp, $Zeile.chr(13).chr(10));
		
		while(!$rsMatZuOrd->EOF())
		{	
			$Zeile = '';
			$Zeile .= $rsMatZuOrd->FeldInhalt('AUTO').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('MATERIALNR').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('KURZTEXT_ORG').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('PREIS').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('BEM').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('BREITE').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('QUERSCHNITT').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('BAUART').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('INNENDM').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('LOAD1').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('LOAD2').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('SP').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('S+R').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('GWS').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('RX').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('WG_FELGEN_KAPPEN').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('SG_FELGEN_KAPPEN').';';
			
			fwrite($fp, $Zeile.chr(13).chr(10));

			$rsMatZuOrd->DSWeiter();
		}
		
		fclose($fp);
	}
	
	/**
	 * Erstellt die Auswertung MatNr und alle ATU-NR
	 */
	public function exportMatZuordnung2()
	{
		$SQL = 'select b.gkz_kurztext as Kurztext, ';
		$SQL .= 'b.gkz_preis as Preis, ';
		$SQL .= 'b.gkz_bemerkung as Bem, ';
		$SQL .= 'c.gka_ast_atunr as ARTNR, ';
		$SQL .= 'd.ast_bezeichnungww, ';
		$SQL .= "'' as ArtBezK, ";
		$SQL .= 'e.wug_wgr_id as WG, ';
		$SQL .= 'e.wug_id as SG, ';
		$SQL .= 'b.gkz_materialnr as Materialnr, ';
		$SQL .= 'b.gkz_kurztext ';
		$SQL .= 'from gkpreisliste a inner join gkpreislistezuord b ';
		$SQL .= 'on a.gkp_key = b.gkz_gkp_key ';
		$SQL .= 'left join gkatunrzuord c ';
		$SQL .= 'on b.gkz_key = c.gka_gkz_key ';
		$SQL .= 'left join artikelstamm d ';
		$SQL .= 'on c.gka_ast_atunr = d.ast_atunr ';
		$SQL .= 'left join warenuntergruppen e ';
		$SQL .= 'on d.ast_wug_key = e.wug_key ';
		$SQL .= "where a.gkp_monat = '" . $this->_AuswertDatum . "'";
		
		$rsMatZuOrd = $this->_DB->RecordSetOeffnen($SQL);
		
		/* Datei oeffnen (schreibend) und Daten schreiben (Material-Zuordnungsdatei) */
		$fp = fopen($this->_AuswertPfad . 'DeTeFleet_MatZuord_' . $this->_AuswertDatum . '_2.csv', 'w');
		
		/*Ueberschrift in Datei schreiben*/
		$Zeile = 'Kurztext;Preis;Bem;ARTNR;Bezeichnungww;ArtBezK;WG;';
		$Zeile .= 'SG;Materialnr;Kurztext';
		
		fwrite($fp, $Zeile.chr(13).chr(10));
		
		while(!$rsMatZuOrd->EOF())
		{	
			$Zeile = '';
			$Zeile .= $rsMatZuOrd->FeldInhalt('KURZTEXT').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('PREIS').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('BEM').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('ARTNR').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('AST_BEZEICHNUNGWW').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('ARTBEZK').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('WG').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('SG').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('MATERIALNR').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('GKZ_KURZTEXT').';';
			
			fwrite($fp, $Zeile.chr(13).chr(10));

			$rsMatZuOrd->DSWeiter();
		}
		
		fclose($fp);
	}

    /**
	 * Erstellt die Exportdatei fuer Ediplan (ATUNr + Materialnr)
     * Es soll alles exportiert werden bis auf
     * - Stahlfelgen
     * - Alufelgen
     * - Radzierblenden/Radkappen
     * 13.08.2013 OP: Auswertungsdatum als optionaler Parameter
     * 				  Wird gebraucht um evtl. ben�tigte �ltere
     * 				  Zuordnungslisten zu erstellen (in Prod-Umgebung)
	 */
	public function exportMatZuordnungEdiPlan($auswertDatum='')
	{
		if($auswertDatum == '')
		{
			$auswertDatum = $this->_AuswertDatum;
		}

        $SQL = 'select ';
		$SQL .= 'c.gka_ast_atunr as ARTNR, ';
		$SQL .= 'b.gkz_materialnr as Materialnr ';
		$SQL .= 'from gkpreisliste a inner join gkpreislistezuord b ';
		$SQL .= 'on a.gkp_key = b.gkz_gkp_key ';
		$SQL .= 'left join gkatunrzuord c ';
		$SQL .= 'on b.gkz_key = c.gka_gkz_key ';
		$SQL .= "where a.gkp_monat = '" . $auswertDatum . "' ";
        $SQL .= "and gkz_kurztext not like '%Stahlfelgen%' ";
        $SQL .= "and gkz_kurztext not like '%Radkappen%' ";
        $SQL .= "and gkz_kurztext not like '%Leichtmetallfelgen%' ";
        $SQL .= "order by artnr";
		
		$rsMatZuOrd = $this->_DB->RecordSetOeffnen($SQL);

		/* Datei oeffnen (schreibend) und Daten schreiben (Material-Zuordnungsdatei) */
		$fp = fopen($this->_AuswertPfad . 'tmp_DeTeFleet_MatZuordEdiPlan_' . $auswertDatum . '.csv', 'w');
		
		/*Ueberschrift in Datei schreiben*/
		$Zeile = 'ARTNR;Materialnr';
		
		fwrite($fp, $Zeile.chr(13).chr(10));

		while(!$rsMatZuOrd->EOF())
		{
			$Zeile = '';
			$Zeile .= $rsMatZuOrd->FeldInhalt('ARTNR').';';
			$Zeile .= $rsMatZuOrd->FeldInhalt('MATERIALNR');
			
			fwrite($fp, $Zeile.chr(13).chr(10));

			$rsMatZuOrd->DSWeiter();
		}

		fclose($fp);
	}
}

?>