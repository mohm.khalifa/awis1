<html>
<head>
<meta http-equiv="content-type" content="text/html"; charset="windows-1252">
</head>
<?php

global $AWISWebSrv;

require_once("operating_AWIS_config.php");

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
include ("awisHeader.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	if ($AWISBenutzer->HatDasRecht(3124) == 0 || $AWISBenutzer->HatDasRecht(3124)&32 !== 32 )
	{
	    $Form->Fehler_KeineRechte();
	}


$WWWPath=$AWISWebSrv.'/admin/appserver/showfile_log.php?file=';

if (!empty($_REQUEST["file"])) {
	
	if ($_REQUEST["file"]=="/" || substr($_REQUEST["file"],0,1)=="/") 
	{
		print "<h1>Kein Zugriff auf ROOT-Verzeichnis erlaubt</h1>";
	  	exit (0);	
	}
	elseif (substr($_REQUEST["file"],0,3)=="../" || substr($_REQUEST["file"],0,2)=="..")	
	{
		print "<h1>Pfadwechsel nicht erlaubt.</h1>";
	  	exit (0);	
	}		
	
  print "<h1>". $AWISAppSrv . ' - ' . basename ($_REQUEST["file"]) ."</h1>";
  $LogFile=$_REQUEST["file"];
  $myfile = @fopen ($LogFile, "r");
  if ($myfile == FALSE) {
    print "<h2>Kann Datei nicht &ouml;ffnen.</h2>";

	$FileArr=glob("$LogFile*");

	if (!empty($FileArr))
	{
		echo "<b>Alternative Dateien:</b><p>";
		foreach($FileArr as $LogFileArr)
		{
			echo "<a href=".$WWWPath.$LogFileArr." target=_blank>".$LogFileArr."</a>";
		}
	}
    exit (0);
  }
} else {
  print "<h1>Keine Datei angegeben</h1>";
  exit (0);
}

$dir_back=substr($_REQUEST["file"],0,strripos($_REQUEST["file"],"/"));
if($dir_back!="/")
{
        echo "<h2><a href=./showdir_log.php?dir=$dir_back>Verzeichnis nach oben</a></h2>";
}

$FilNr="";
if (stristr(basename ($_REQUEST["file"]),'TXNDAT')!==FALSE)
{
 $FilNr=explode('-',basename ($_REQUEST["file"]));
 $FilNr=$FilNr[3];		
}

if (stristr(basename ($_REQUEST["file"]),'DW')!==FALSE)
{
 $FilNr=explode('-',basename ($_REQUEST["file"]));
 $FilNr=$FilNr[4];		
}

if ($FilNr!='')
{
 echo "<h2>Filiale: $FilNr</h2>";
}

if (!empty($_REQUEST["search"]))
{
$search=explode(",",$_REQUEST["search"]);
$search_text=htmlspecialchars($_REQUEST["search"]);
echo "<h2>Anzeige ab Suchtext: >>".$_REQUEST["search"]."<<<h2>";
}
else
{
$search="";
$search_text="";
}

echo "<form name='frmSuche' method=get action=./showfile_log.php>";
echo "<input name=search type=text size=50 value='".$search_text."'> * <input type=submit value=Suchen>";
echo "<h5><b>* Suchhinweis: Leere Suche zeigt komplette Datei an.";
echo " | Mehrere Suchwörter durch Komma trennen => Ab 1.ten Worttreffer wird angezeigt. (Bsp.: Tabelle,Satz,erfolgreich)</b><h5>";
echo "<input name=file type=hidden value='".$_REQUEST["file"]."'>";
echo "</form>";

print "<table border=\"0\">";

$line_nr = 0;
$line_match=0;

while (!feof ($myfile)) {
$grepmatch=array();
$cnt=0;
 if (!empty($search))
 {
  $line = fgets ($myfile, 4096);
  for($i=0;$i<count($search);$i++)
  {	
    if($line_match==0)
    {		
  	if(stristr($line,$search[$i])!== FALSE)
	{
         ereg('\ \/.*',$line, $grepmatch);
          if (!empty($grepmatch))
          {
                  $filestr=trim($grepmatch[0]).'*';
                  $ret = exec('ls '.$filestr);
                  if ($ret!='')
                  {
                        $line=str_replace($grepmatch[0],"<a href=".$WWWPath."".$ret." target=_blank>".$grepmatch[0]."</a>",$line,$cnt);
                  }
          }

 	 #$line_match=1;
	 $line_nr++;
   	 print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
          if ($cnt>0){
                print "<td><code>". $line ."</code></td></tr>";
          }
          else{
                print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
          }
	 break;
  	}
    }else
    {
        ereg('\ \/.*',$line, $grepmatch);
          if (!empty($grepmatch))
          {
                  $filestr=trim($grepmatch[0]).'*';
                  $ret = exec('ls '.$filestr);
                  if ($ret!='')
                  {
                        $line=str_replace($grepmatch[0],"<a href=".$WWWPath."".$ret." target=_blank>".$grepmatch[0]."</a>",$line,$cnt);
                  }
          }

	 $line_nr++;
         print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
          if ($cnt>0){
                print "<td><code>". $line ."</code></td></tr>";
          }
          else{
                print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
          }
	 break;	
    }
  }
 }
 else
 {	
  $line = fgets ($myfile, 4096);
  $line_nr++;
  ereg('\ \/.*',$line, $grepmatch);
  if (!empty($grepmatch))
  {
          $filestr=trim($grepmatch[0]).'*';
          $ret = exec('ls '.$filestr);
          if ($ret!='')
          {
                $line=str_replace($grepmatch[0],"<a href=".$WWWPath."".$ret." target=_blank>".$grepmatch[0]."</a>",$line,$cnt);
          }
  }

  print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
  if ($cnt>0){
        print "<td><code>". $line ."</code></td></tr>";
  }
  else{
        print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
  }
 }
}
fclose($myfile);
print "</table>";

}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</html>
