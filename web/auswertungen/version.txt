#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Auswertungen
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
1.00.12;div. Anpassungen;28.03.2008;<a href=mailto:Thomas.Riedl@de.atu.eu>Thomas Riedl</a>
1.00.11;Auswertung Lieferkontrolle hinzugef�gt;21.01.2008;<a href=mailto:Thomas.Riedl@de.atu.eu>Thomas Riedl</a>
1.00.10;Erweiterung bei der Suchmaske f�r Fremk�ufe;04.08.2006;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.09;Nachgefragte Artikel aufgenommen;03.03.2005;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.08;Fehler bei Fremdkaufauswertung beseitigt;10.02.2005;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.07;Umschl�sselung bei Fremdkaufauswertung f�r ATU-Nr eingef�gt;23.11.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.06;Exportdateinamen angeglichen;07.09.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.05;Fehler bei Fremdkaufsuche beseitigt;31.08.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.04;Bei Filialpreisen Sortierung hinzugef�gt;26.05.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.03;Auswertungen f�r Cityboards;16.04.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.02;Auswertung f�r Filialpreise;15.01.2004;<a href=mailto:Entwick>Sacha Kerres</a>
1.00.01;Erste Version;13.01.2004;<a href=mailto:Entwick>Sacha Kerres</a>

