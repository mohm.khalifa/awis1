<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class reifen_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht52000;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht52000 = $this->AWISBenutzer->HatDasRecht(52000);
        $this->_EditRecht = (($this->Recht52000 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_REX'));

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('REX', '%');
        $TextKonserven[] = array('AST', 'AST_ATUNR');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_ReklamationenReifen');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_REX', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht52000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'REX'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function erstelleATUNrInfos($ATU_NR, $Hersteller = '',$LAR_NR = '')
    {
        $SQL = 'SELECT AST.AST_ATUNR, AST.AST_BEZEICHNUNGWW, RST.RST_LIE_NR, REH.REH_BEZEICHNUNG, RST.RST_LARTNR';
        $SQL .= ' FROM REIFENSTAMM RST';
        $SQL .= ' INNER JOIN V_REIFENHERSTELLER REH';
        $SQL .= ' ON REH.REH_KEY = RST.RST_REH_KEY';
        $SQL .= ' INNER JOIN REIFENPFLEGE RRP';
        $SQL .= ' ON RRP.RRP_RST_KEY = RST.RST_KEY';
        $SQL .= ' INNER JOIN ARTIKELSTAMM AST';
        $SQL .= ' ON RRP.RRP_ATUNR = AST.AST_ATUNR ';

        $SQL .= ' WHERE AST_ATUNR =' . $this->DB->WertSetzen('AST', 'TU', $ATU_NR);

        $rsAst = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('AST'));

        $this->Form->ZeileStart();
        $Style = '';
        $Text = $rsAst->FeldInhalt('AST_BEZEICHNUNGWW');
        if ($rsAst->EOF()) {
            $Text = $this->Form->LadeTextBaustein('REX', 'REX_ERR_UNBEKANNTE_ATUNR', $this->AWISBenutzer->BenutzerSprache());
            $Style = 'font-weight: bold; color: #FF0000';
        }
        $this->Form->Erstelle_TextLabel($this->Form->LadeTextBaustein('AST', 'AST_BEZEICHNUNGWW') . ':', 170, $Style);
        $this->Form->Erstelle_TextLabel($Text, 500, $Style);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();

        if($Hersteller==''){
            $Hersteller = $rsAst->FeldInhalt('REH_BEZEICHNUNG');
        }
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['REX']['REX_HERSTELLER'] . ':', 170);
        $SQL = 'select distinct reh_bezeichnung, reh_bezeichnung from V_REIFENHERSTELLER order by 1 asc';
        $this->Form->Erstelle_SelectFeld('~!REX_HERSTELLER', $Hersteller, '', $this->_EditRecht, $SQL);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();

        if($LAR_NR==''){
            $LAR_NR = $rsAst->FeldInhalt('RST_LARTNR');
        }
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['REX']['REX_LAR_NR'] . ':', 170);
        $this->Form->Erstelle_TextFeld('REX_LAR_NR',$LAR_NR,12,150,$this->_EditRecht);
        $this->Form->ZeileEnde();

    }

    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';

        if ($AWIS_KEY1 != 0) {
            $Bedingung .= ' AND REX_KEY = ' . $this->DB->WertSetzen('REX', 'N0', $AWIS_KEY1);

            return $Bedingung;
        }

        if (isset($this->Param['REX_AST_ATUNR']) and $this->Param['REX_AST_ATUNR'] != '') {
            $Bedingung .= ' AND REX_AST_ATUNR ' . $this->DB->LikeOderIst($this->Param['REX_AST_ATUNR'], awisDatenbank::AWIS_LIKE_UPPER, 'REX');
        }

        if (isset($this->Param['REX_WERK']) and $this->Param['REX_WERK'] != '') {
            $Bedingung .= ' AND REX_WERK ' . $this->DB->LikeOderIst($this->Param['REX_WERK'], awisDatenbank::AWIS_LIKE_UPPER, 'REX');
        }

        if (isset($this->Param['REX_REKLAMATIONSDATUM_VON']) and $this->Param['REX_REKLAMATIONSDATUM_VON'] != '') {
            $Bedingung .= ' AND REX_REKLAMATIONSDATUM <= ' . $this->DB->WertSetzen('REX', 'D', $this->Param['REX_REKLAMATIONSDATUM_VON']);
        }

        if (isset($this->Param['REX_REKLAMATIONSDATUM_BIS']) and $this->Param['REX_REKLAMATIONSDATUM_BIS'] != '') {
            $Bedingung .= ' AND REX_REKLAMATIONSDATUM >= ' . $this->DB->WertSetzen('REX', 'D', $this->Param['REX_REKLAMATIONSDATUM_VON']);
        }

        return $Bedingung;
    }


}