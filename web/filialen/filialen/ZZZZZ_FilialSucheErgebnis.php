<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Ereignisanzeige</title>
<link rel=stylesheet type=text/css href=/ATU.css>
</head>
<body>
<? 

require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("ATU_Header.php");	// Kopfzeile

if(!(awisHatDasRecht(100)))
{
	awisEreignis(3,1000,'Benutzerverwaltung',"$PHP_AUTH_USER",'','','');
	die("Keine ausreichenden Rechte!");
}

$con = awisLogon();
if($con)
{
	if($txtFilnr<>"")
	{
		$Bedingung = " FI_ID=" . $txtFilnr;
	}
	If($txtBez<>"")
	{
		If($Bedingung=="")
		{
			$Bedingung = " UPPER(FI_Bez) " . awisLIKEoderIST($txtBez);
		}
		else 
		{
			$Bedingung = " AND UPPER(FI_Bez) " . awisLIKEoderIST($txtBez);
		}
	}

	if($txtPlz)
	{
		If($Bedingung=="")
		{
			$Bedingung = "FI_Plz " . awisLIKEoderIST($txtPlz);
		}
		else 
		{
			$Bedingung = " AND FI_Plz " . awisLIKEoderIST($txtPlz);
		}
	}
		
	If($txtOrt<>"")
	{
		If($Bedingung=="")
		{
			$Bedingung = "UPPER(FI_Ort) " . awisLIKEoderIST($txtOrt);
		}
		else 
		{
			$Bedingung = " AND UPPER(FI_Ort) " . awisLIKEoderIST($txtOrt);
		}
	}

	$rsFilialen = awisOpenRecordset($con, "SELECT * FROM Filialen WHERE " . $Bedingung);
	$rsFilialenZeilen = $awisRSZeilen;

	$AktZeile = 1;		// Akt. Zeile und Spalte 
	$AktSpalte = 1;
	
	If($rsFilialenZeilen==0)
	{
		print "Es wurde keine Filiale gefunden.<br><br>";
		print "<a href=./FilialSuche.php>Neue Suche</a>";
	}		// Keine Filiale gefunden
	elseif($rsFilialenZeilen==1)
	{
		print "<h1 id=SeitenTitel>Filialdetails f�r die Filiale " . $rsFilialen["FI_ID"][0] . " - " . $rsFilialen["FI_BEZ"][0] . "</h1><hr>";

		$rsWebMaske = awisOpenRecordset($con, "SELECT * From V_BenutzerMasken WHERE BN_Key=" .  awisBenutzerID() . " AND XM_ID=1");
		$rsWebMaskeZeilen = $awisRSZeilen;
		
		print "<table border=0><tr>"	 	;
		
	 		// Jeden Datenblock ausgeben
	 	for($Feld=0;$Feld<$rsWebMaskeZeilen;$Feld++)
		{
			print "<td valign=top>";
			if($AktZeile==$rsWebMaske["ZEILE"][$Feld] && $AktSpalte==$rsWebMaske["SPALTE"][$Feld])
			{
				switch($rsWebMaske["XF_KEY"][$Feld])
				{
					case '1':		//Anschriftsblock
						print "<table border=" . $rsWebMaske["RAHMEN"][$Feld] . " Width=". $rsWebMaske["BLOCKBREITE"][$Feld] .">";
						print "<tr><td id=FeldFett>" . $rsFilialen["FI_BEZ"][0] . "</td></tr>";
						print "<tr><td id=FeldFett>" . $rsFilialen["FI_STRASSE"][0] . "</td></tr>";
						print "<tr><td id=FeldFett>" . $rsFilialen["FI_PLZ"][0] . " " . $rsFilialen["FI_ORT"][0] . "</td></tr>";
						print "</table>";
						break;
					default:		
								// Zeile und Spalte festlegen
								
						$FTIds = $rsWebMaske["XF_INFOLISTE"][$Feld];		// Auszugebende Info-Felder
				
						print "<table border=" . $rsWebMaske["RAHMEN"][$Feld] . " Width=". $rsWebMaske["BLOCKBREITE"][$Feld] .">";
						If($rsWebMaske["UEBERSCHRIFT"][$Feld]==1)		// �berschrift oben
						{
							print "<tr><td ID=FeldBez colspan=2>" . $rsWebMaske["XF_FELDBEZEICHNUNG"][$Feld] . "</td></tr>";
						}
						
						If($rsWebMaske["IQID"][$Feld]==0)
						{
							$rsFilInfos = awisOpenRecordset($con, "SELECT * FROM V_FilialInfos WHERE FI_ID=" . $rsFilialen["FI_ID"][0] . " AND FT_ID IN(" . $FTIds . ")");
						}
						else
						{
						$rsFilInfos = awisOpenRecordset($con, "SELECT * FROM V_FilialInfos WHERE FI_ID=" . $rsFilialen["FI_ID"][0] . " AND FT_ID IN(" . $FTIds . ") AND FF_IQ_ID=". $rsWebMaske["IQID"][$Feld]);
						}
						$rsFilInfosZeilen = $awisRSZeilen;
						
						for($DS=0;$DS<$rsFilInfosZeilen;$DS++)
						{
							If($rsWebMaske["XF_SQL"][$Feld]!="")
							{
								$rsPE = awisOpenRecordSet($con, str_replace("%1",$rsFilInfos["FF_WERT"][$DS],$rsWebMaske["XF_SQL"][$Feld]));
								$Wert = $rsPE["PENAME"][0];
							}
							else
							{
								$Wert = $rsFilInfos["FF_WERT"][$DS];
							}
							
							If($rsWebMaske["XF_FELDBEZ"][$Feld]==1)		// Feldbezeichnung ausgeben
							{
								
								print "<tr><td>" . $rsFilInfos["FT_INFORMATION"][$DS] . "</td><td>$Wert</td></tr>";
							}
							else										// Ohne Feldbezeichnung
							{
								print "<tr><td>&nbsp;</td><td> "  . $rsFilInfos["FF_WERT"][$DS] . "</td></tr>";
							}
						}
					
						unset($rsFilInfos);
						print "</table>";
					
						unset($FTIds);
						break;
				}
				print "</td>";
			}
			else			// Leere Spalte/Zeile
			{
				print "&nbsp;</td>";	
				$Feld--;			
			}

			If($rsWebMaske["XM_SPALTEN"][$Feld]<++$AktSpalte)
			{
				$AktSpalte=1;
				$AktZeile++;
				print "</tr></table><table border=0><tr>";
			}

		}			// Alle Felder(Bl�cke)

					// Eine Spalte weiter positionieren
									

		Print "</tr></table>";		// Rahmen
		
		print "<hr><a href=./FilialSuche.php>Neue Suche</a>";
		
	}		// Eine Filiale gefunden, gleich zum Detail
	else
	{
		print "<table border=1 width=100%><tr><th width=70 id=FeldBez>FilialNr</th><th id=FeldBez>Bezeichnung</th><th id=FeldBez>Stra�e</th><th id=FeldBez>Plz</th><th id=FeldBez>Ort</th></tr>";
	
		for($DS=0;$DS<$rsFilialenZeilen;$DS++)
		{	
			print "<tr>";
			print "<td><a href=./FilialSucheErgebnis.php?txtFilnr=" . $rsFilialen["FI_ID"][$DS] . ">" . $rsFilialen["FI_ID"][$DS] . "</td>";
			print "<td>" . $rsFilialen["FI_BEZ"][$DS] . "</td>";
			print "<td>" . $rsFilialen["FI_STRASSE"][$DS] . "</td>";
			print "<td>" . $rsFilialen["FI_PLZ"][$DS] . "</td>";
			print "<td>" . $rsFilialen["FI_ORT"][$DS] . "</td>";
			print "</tr>";

		}
		print"</table>";
		print "Es wurden " . $DS . " Filiale(n) gefunden.<br><br>";
		print "<a href=./FilialSuche.php>Neue Suche</a>";

	}	//Mehrere Filialen gefunden
	
	awisLogoff($con);
}
else 
{
	awisEreignis(3,1010,'Filialinformation',"Logon",'','','','');
	awisErrorMailLink("FilialSucheErgebnis",1);
}	// Verbindung aufgebaut?



?>
</Body>
</HTML>
