#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Stammdaten
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Target=_self
ErrorMail=Shuttle%20Sepp

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.00.05;Sortierung der Bereiche in den Textkonserven;29.04.2008;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.00.04;Anzeigen des Landes im Lieferantenstammblatt;25.10.2007;<a href=mailto:christian.argauer@de.atu.eu>Christian Argauer</a>
0.00.03;Anzeige, wenn Lieferanten nicht (mehr) existieren;23.03.2004;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.00.02;Hersteller;27.02.2004;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.00.01;Lieferanten;09.12.2003;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>

