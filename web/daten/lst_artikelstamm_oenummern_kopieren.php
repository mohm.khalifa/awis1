<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisBenutzer.inc';

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Benutzer = awisBenutzer::Init();
$Form = new awisFormular();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('OEN', '%');
$TextKonserven[] = array('LIE', 'LIE_NAME1');
$TextKonserven[] = array('HER', 'HER_BEZEICHNUNG');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$JaNeinFeld = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);

$SQL  ='SELECT';
$SQL .='     *';
$SQL .=' FROM';
$SQL .='     TEILEINFOS';
$SQL .='     INNER JOIN OENUMMERN ON TEI_KEY2 = OEN_KEY';
$SQL .='     INNER JOIN HERSTELLER ON OEN_HER_ID = HER_ID';
$SQL .=' WHERE';
$SQL .='     TEI_ITY_ID1 = \'AST\'';
$SQL .='     AND TEI_ITY_ID2 = \'OEN\'';
$SQL .='     AND TEI_WERT1 = ' . $DB->WertSetzen('TEI','TU',$_REQUEST['sucATUNrOENr']);

$rsOEN = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('TEI'));

$Link = '';

$FeldBreiten['CHECKBOX'] = 20;
$FeldBreiten['OEN_NUMMER'] = 200;
$FeldBreiten['HER_BEZEICHNUNG'] = 190;
$FeldBreiten['HER_BEZEICHNUNG_LAENGE'] = 15;
$FeldBreiten['OEN_BEMERKUNG'] = 350;
$FeldBreiten['OEN_BEMERKUNG_LAENGE'] = 60;

?>

<script>

    $('#txtCOPY_ALL').change(function(){
        $('.oe_copy').each(function(e,i){
           $(i).find('input[type=checkbox]').prop('checked', $('#txtCOPY_ALL').prop('checked'));
        });
    });

</script>


<?php

$Form->ZeileStart();
$Form->Erstelle_ListenCheckbox('COPY_ALL', 'off', $FeldBreiten['CHECKBOX'], true, 'on', '', '', '', '',-2);
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_NUMMER'], $FeldBreiten['OEN_NUMMER'], '', $Link);
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HER']['HER_BEZEICHNUNG'], $FeldBreiten['HER_BEZEICHNUNG'], '', $Link);
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_MUSTER'], 75, '', $Link);
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_BEMERKUNG'], $FeldBreiten['OEN_BEMERKUNG'], '', $Link);
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_USER'], 100, '', $Link);
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_USERDAT'], 112, '', $Link);
$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_AKTNUMMER'], 68, '', $Link);
$Form->ZeileEnde();
$DS = 0;
while(!$rsOEN->EOF()){
    $DS++;
    $Form->ZeileStart();
    $Form->Erstelle_ListenCheckbox('COPY_OE_TEI_KEY_' . $rsOEN->FeldInhalt('TEI_KEY'), 'off', $FeldBreiten['CHECKBOX'], true, 'on', '', '', '', 'oe_copy',($DS % 2));

    $Form->Erstelle_ListenFeld('*OEN_NUMMER', $rsOEN->FeldInhalt('OEN_NUMMER'), 10, $FeldBreiten['OEN_NUMMER'], false, ($DS % 2), '', '', 'T', 'L',
                               $rsOEN->FeldInhalt('OEN_NUMMER'));

    $Text = (strlen($rsOEN->FeldInhalt('HER_BEZEICHNUNG')) > $FeldBreiten['HER_BEZEICHNUNG_LAENGE'] - 2?substr($rsOEN->FeldInhalt('HER_BEZEICHNUNG'), 0, $FeldBreiten['HER_BEZEICHNUNG_LAENGE'] - 2) . '...':$rsOEN->FeldInhalt('HER_BEZEICHNUNG'));
    $Form->Erstelle_ListenFeld('*OEN_HER_ID', $Text, 10, $FeldBreiten['HER_BEZEICHNUNG'], false, ($DS % 2), '', '', 'T', 'L', $rsOEN->FeldInhalt('HER_BEZEICHNUNG'));

    $Muster = '--';
    foreach ($JaNeinFeld AS $JaNeinAnzeige) {
        if (substr($JaNeinAnzeige, 0, 1) == $rsOEN->FeldInhalt('OEN_MUSTER')) {
            $Muster = substr($JaNeinAnzeige, 2);
            break;
        }
    }

    $Form->Erstelle_ListenFeld('*OEN_MUSTER', $Muster, 10, 75, false, ($DS % 2), '', '', 'T', 'L', $AWISSprachKonserven['OEN']['ttt_OEN_MUSTER']);

    $Text = (strlen($rsOEN->FeldInhalt('OEN_BEMERKUNG')) > $FeldBreiten['OEN_BEMERKUNG_LAENGE'] - 2?substr($rsOEN->FeldInhalt('OEN_BEMERKUNG'), 0, $FeldBreiten['OEN_BEMERKUNG_LAENGE'] - 2) . '...':$rsOEN->FeldInhalt('OEN_BEMERKUNG'));
    $Form->Erstelle_ListenFeld('*OEN_BEMERKUNG', $Text, 10, $FeldBreiten['OEN_BEMERKUNG'], false, ($DS % 2), '', '', 'T', 'L', $rsOEN->FeldInhalt('OEN_BEMERKUNG'));

    $Link = '';
    $Form->Erstelle_ListenFeld('*OEN_USER', $rsOEN->FeldInhalt('TEI_USER'), 10, 100, false, ($DS % 2), '', $Link, 'T', 'L', $rsOEN->FeldInhalt('OEN_USER'));
    $Form->Erstelle_ListenFeld('*OEN_USERDAT', $rsOEN->FeldInhalt('TEI_USERDAT'), 10, 92, false, ($DS % 2), '', '', 'D', 'L',
                               $Form->Format('D', $rsOEN->FeldInhalt('OEN_USERDAT')));

    $Form->Erstelle_ListenCheckbox('OEN_AKTNUMMER_' . $rsOEN->FeldInhalt('OEN_KEY'), $rsOEN->FeldInhalt('OEN_AKTNUMMER'), 20, false, 1, '', '', '', '',($DS % 2));

    $Form->ZeileEnde();


    $rsOEN->DSWeiter();
}