<?php
require_once('/daten/include/awisZukauf_WUETSCHNER.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;
    $WANummer = '';
    $Abfrage = 'Bestellung';
    $DebugModus = false;

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--testsystem':
                            $Testsystem = true;
                            break;
                    case '--wanummer':
                            $WANummer = $argv[++$i];
                            break;
                    case '--jobid':
                            $JobId = $argv[++$i];
                            break;
                    case '--starttag':
                            $StartTag = $argv[++$i];
                            break;
                    case '--endtag':
                            $EndTag = $argv[++$i];
                            break;
                    case '--abfrage':
                            $Abfrage = $argv[++$i];
                            break;
                    case '--debugmodus':
                        $DebugModus = true;
                        break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --testsystem                       Daten aus dem Testsystem abfragen.'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                            echo '  --wanummer  <Nr>                   WANummer fuer die Abfrage.'.PHP_EOL;
                            echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.'.PHP_EOL;
                            echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.'.PHP_EOL;
                            echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung, Lieferschein oder Rechnung.'.PHP_EOL;

                            echo PHP_EOL;
                            die();
            }
    }

    $AWISUser = awisBenutzer::init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $ZUK = new awisZukauf_WUETSCHNER($DebugLevel,$Testsystem);

    if($DebugModus){
        $ZUK->DebugModus();
    }

    foreach ($ZUK->SubLieferanten() as $LKZ) {
        $ZUK->InitSubLieferant($LKZ);

        switch ($Abfrage) {
            case 'Bestellung':
                if ($ZUK->LeseBestellungen($WANummer, $StartTag, $EndTag, ($WANummer != '' ? 'A' : '')) > 0) {
                    $ZUK->SpeichereBestellungen();
                }
                break;
            case 'Lieferschein':
                if ($ZUK->LeseLieferscheine($StartTag, $EndTag, '', $WANummer, '', ($WANummer != '' ? true : false)) > 0) {
                    $ZUK->SpeichereLieferscheine();
                }
                break;
            case 'Rechnung':
                if ($ZUK->LeseRechnungen($StartTag, $EndTag, '', $WANummer, '', true, 'R') > 0) {
                    $ZUK->SpeichereRechnungen();
                }
                if ($ZUK->LeseRechnungen($StartTag, $EndTag, '', $WANummer, '', true, 'G') > 0) {
                    $ZUK->SpeichereRechnungen();
                }

                break;
        }
    }
}
catch (awisException $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'SQL: '.$ex->getSQL().PHP_EOL;
}
catch (Exception $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
}
?>

