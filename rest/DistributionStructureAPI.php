<?php

require_once 'awisDistributionCenter.php';
try{

    require_once 'awisDatenbank.inc';
    require_once 'awisBenutzer.inc';
    require_once 'awisRest.inc';

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Benutzer = awisBenutzer::Init('awis_jobs');

    $awisREST = new awisRest('DistributionStructureAPI');
    $awisREST->verarbeiteRequest();

    $Request = $awisREST->getRequest();

    $DistributionStructure = new stdClass();

    //Standard FEB_KEY (ATU INT)
    $FEB_KEY = array();
    //Standard Rollenkeys
    $FER_KEYs = array();
    //Standardmaessig keine Rollen ausliefern
    $SalesForce = false;


    //Bestimmtes Datum?
    $Datum ='';
    if(isset($Request['DATE']['WERT'])){
        $Datum = $Request['DATE']['WERT'];
    }

    //Eine spezielle Filialebene?
    if(isset($Request['DISTRIBUTION_KEY']['WERT'])){
        $XTN_KUERZEL = 'FEB';
        if(isset($Request['DISTRIBUTION_LEVEL']) and $Request['DISTRIBUTION_LEVEL']['WERT'] == 4){
            $XTN_KUERZEL = 'FIL';
        }
        $FEB_KEY = array(array('XXX_KEY'=>$Request['DISTRIBUTION_KEY']['WERT'],'XTN_KUERZEL'=> $XTN_KUERZEL));
    }elseif(isset($Request['USER_KEY']['WERT']) or isset($Request['USER_NAME']['WERT'])){ //Kommt ein Userkey? Schauen was er ist und evtl. erst Tiefer einsteigen
        $SQL  ='select a.*, B.FER_BEZEICHNUNG,c.KON_NAME1, c.KON_NAME2, c.KON_PER_NR, e.XBL_LOGIN, XBN_KEY from FILIALEBENENROLLENZUORDNUNGEN a';
        $SQL .=' inner join FILIALEBENENROLLEN B';
        $SQL .=' on a.FRZ_FER_KEY = B.FER_KEY ';
        $SQL .=' inner join KONTAKTE c';
        $SQL .=' on c.KON_KEY = a.FRZ_KON_KEY';
        $SQL .=' inner join BENUTZER d ';
        $SQL .=' on d.XBN_KON_KEY = c.KON_KEY';
        $SQL .=' inner join BENUTZERLOGINS e';
        $SQL .=' on e.XBL_XBN_KEY = d.XBN_KEY';


        if($Datum!=''){
            $SQL .=' where FRZ_GUELTIGAB <= '.$DB->WertSetzen('FRZ','D',$Datum);
            $SQL .=' and FRZ_GUELTIGBIS  >= ' . $DB->WertSetzen('FRZ','D',$Datum);
        }else{
            $SQL .=' where FRZ_GUELTIGAB <= sysdate';
            $SQL .=' and FRZ_GUELTIGBIS  >= sysdate';
        }

        if(isset($Request['USER_KEY']['WERT'])){
            $SQL .= ' AND XBN_KEY = ' .$DB->WertSetzen('FRZ','Z',$Request['USER_KEY']['WERT']);
        }else{
            $SQL .= ' AND XBL_LOGIN = ' .$DB->WertSetzen('FRZ','TU',$Request['USER_NAME']['WERT']);
        }
        $SQL .= ' AND FER_KEY in (1,2,3,4,23,25) ';
        $rsFRZ = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FRZ'));

        $rsFRZ->AnzahlDatensaetze()==0?$FEB_KEY[]=array('XXX_KEY'=>0,'XTN_KUERZEL'=>'FEB'):''; //Nichts gefunden, dann nix.

        while(!$rsFRZ->EOF()){
            $Key = array();
            $Key['XXX_KEY'] = $rsFRZ->FeldInhalt('FRZ_XXX_KEY');
            $Key['XTN_KUERZEL'] = $rsFRZ->FeldInhalt('FRZ_XTN_KUERZEL');
            $FEB_KEY[] = $Key;
            $rsFRZ->DSWeiter();
        }

        unset($rsFRZ);

    }


    //Kein FEB_KEY gesetzt? Dann ATU hernehmen
    count($FEB_KEY)==0?$FEB_KEY[]=array('XXX_KEY'=>368,'XTN_KUERZEL'=>'FEB'):'';


    $DistributionCenter = new awisDistributionCenter($DB, $FEB_KEY);

    //Uebergeordnete ermitteln?
    $Childs = (isset($Request['CHILDS']['WERT']) and $Request['CHILDS']['WERT'] = 1);
    //Untergeordnete ermitteln?
    $Parents = (isset($Request['PARENTS']['WERT']) and $Request['PARENTS']['WERT'] = 1);
    //Personal ermitteln?
    if((isset($Request['SALESFORCE']['WERT']) and $Request['SALESFORCE']['WERT'] = 1)){
        $DistributionCenter->withSalesForce();
        if(isset($Request['ROLE_KEY']['WERT'])){

            if(strpos($Request['ROLE_KEY']['WERT'],',')!==false){
                $FER_KEYs = explode(',',$Request['ROLE_KEY']['WERT']);
            }
            $FER_KEYs = array($Request['ROLE_KEY']['WERT']);
        }
    }

    if(isset($Request['ASTREE']['WERT']) and $Request['ASTREE']['WERT'] == 1 ){
        $DistributionCenter->asTree();
        $DistributionCenter->SalesForceAsChildren();
    }

    $Response = $DistributionCenter->getDistributionCenter($Parents,$Childs,$FER_KEYs,$Datum);

    $awisREST->sendeResponse(array($Response),awisRest::AntwortTyp_JSON);

}

catch(awisException $e){
    var_dump($e);
}catch (Exception $e){
    var_dump($e);
}