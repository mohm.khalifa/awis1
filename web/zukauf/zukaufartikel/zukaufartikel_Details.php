<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('ZLH','ZLH_BEZEICHNUNG');
	$TextKonserven[]=array('ZLH','ZLH_KUERZEL');
	$TextKonserven[]=array('ZUP','ZUP_AKTION');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('AST','AST_VK');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);
	if($Recht10001==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['ZLA_ARTIKELNUMMER'] = $_POST['sucZLA_ARTIKELNUMMER'];
		$Param['ZLA_BEZEICHNUNG'] = $_POST['sucZLA_BEZEICHNUNG'];
		$Param['LIE_NR'] = $_POST['sucLIE_NR'];
		$Param['ZLA_ZLH_KEY'] = $_POST['sucZLA_ZLH_KEY'];
		$Param['ZSZ_SORTIMENT'] = $_POST['sucZSZ_SORTIMENT'];
		$Param['ZLA_AST_ATUNR'] = $_POST['sucZLA_AST_ATUNR'];
		$Param['ZAI_ID'] = $_POST['sucNummer'];
		$Param['ZLA_TAUSCHTEIL'] = $_POST['sucZLA_TAUSCHTEIL'];
		$Param['ZAL_BESTELLNUMMER'] = $_POST['sucZAL_BESTELLNUMMER'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ZLA",serialize($Param));
		$AWISBenutzer->ParameterSchreiben('AktuellerZLA',serialize(''));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./zukaufartikel_loeschen.php');
		if($AWIS_KEY1==0)
		{
			$Param = $AWISBenutzer->ParameterLesen("Formular_ZLA");
		}
		else
		{
			$Param = $AWIS_KEY1;
		}
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufartikel_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZLA'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZLA'));
	}
	elseif(isset($_GET['ZLA_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZLA_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZLA'));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZLA'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_ZLA',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZLA_ARTIKELNUMMER';
			$Param['ORDER']='ZLA_ARTIKELNUMMER';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT DAT1.* ,row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM (';
	$SQL .= 'SELECT DISTINCT ZUKAUFARTIKEL.*, ZLH_KUERZEL, ZLH_BEZEICHNUNG';
	$SQL .= ' FROM ZUKAUFARTIKEL';
	$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
	if($Param['ZLA_ARTIKELNUMMER']!='')
	{
		//$ArtNr = awisWerkzeuge::ArtNrKomprimiert($Param['ZLA_ARTIKELNUMMER'],array(ord('*'),ord('_')));
		//$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZLA_KEY = ZAL_ZLA_KEY AND ZAL_BESTELLNUMMER ' . $DB->LIKEoderIST($ArtNr) . '';
		//$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZLA_KEY = ZAL_ZLA_KEY AND ZAL_BESTELLNUMMER ' . $DB->LIKEoderIST($Param['ZLA_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . '';
	}

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	$SQL .= ')DAT1 ';

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZLA',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= ' '.$ORDERBY;

//$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$Form->DebugAusgabe(1,$SQL);
	$rsZLA = $DB->RecordsetOeffnen($SQL);

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmZukaufartikel action=./zukaufartikel_Main.php?cmdAktion=Details method=POST>';

	if($rsZLA->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZLA->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$AWISBenutzer->ParameterSchreiben('Formular_ZLA',serialize($Param));

		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');

		$Link .= '&Sort=ZLA_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLA_ARTIKELNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'],200,'',$Link);
		$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],200,'',$Link);
		$Link .= '&Sort=ZLA_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLA_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'],400,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZLA->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
			$Link = './zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY=0'.$rsZLA->FeldInhalt('ZLA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZLA_ARTIKELNUMMER',$rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER'),0,200,false,($DS%2),'',$Link);
			$Link='';	// TODO: Sp�ter auf die Hersteller
			$Form->Erstelle_ListenFeld('ZLH_KUERZEL',$rsZLA->FeldInhalt('ZLH_KUERZEL'),0,200,false,($DS%2),'',$Link,'T','L',$rsZLA->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Form->Erstelle_ListenFeld('ZLA_BEZEICHNUNG',$rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'),0,400,false,($DS%2),'',$Link);
			$Form->ZeileEnde();

			$rsZLA->DSWeiter();
			$DS++;
		}

		$Link = './zukaufartikel_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZLA->FeldInhalt('ZLA_KEY');

		$AWISBenutzer->ParameterSchreiben("AktuellerZLA",serialize($AWIS_KEY1));
		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_ZLA',serialize($Param));

		$Form->Erstelle_HiddenFeld('ZLA_KEY', $AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufartikel_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZLA->FeldInhalt('ZLA_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZLA->FeldInhalt('ZLA_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10001&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10001&6);
		}

			// Nummer und Hersteller
		$Form->ZeileStart();

		if($rsZLA->FeldInhalt('ZLA_KEY')!='')
		{
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'].':',180);
    		$SQL = 'SELECT COUNT(*) AS DUBLETTEN';
    		$SQL .= ' FROM Zukaufartikel ';
    		$SQL .= ' INNER JOIN Zukauflieferantenhersteller ON ZLA_ZLH_KEY = ZLH_KEY';
    		$SQL .= ' WHERE ZLA_KEY<>'.$AWIS_KEY1;
    		$SQL .= ' AND ZLA_ARTIKELNUMMER = (SELECT ZLA_ARTIKELNUMMER ';
    		$SQL .= '              FROM Zukaufartikel WHERE zla_key = '.$AWIS_KEY1.')';
    		$rsDATEN = $DB->RecordSetOeffnen($SQL);
    		if($rsDATEN->FeldInhalt('DUBLETTEN')>0)
    		{
    			$Form->Erstelle_Bild('href', 'dubletten', './zukaufartikel_Main.php?cmdAktion=Details&Seite=Dubletten&ZLA_KEY='.$AWIS_KEY1, '/bilder/icon_fehler.png',str_replace('$1', $rsDATEN->FeldInhalt('DUBLETTEN'), $AWISSprachKonserven['ZLA']['DUBLETTENJA']),'',18,18,20);
    		}
    		else
    		{
    			$Form->Erstelle_Bild('--', 'dubletten', '', '/bilder/icon_ok.png',$AWISSprachKonserven['ZLA']['DUBLETTENNEIN'],'',18,18,20);
    		}

		}
        else
        {
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'].':',200);
        }
		$Form->Erstelle_TextFeld('!ZLA_ARTIKELNUMMER',$rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER'),25,370,$EditRecht,'','','','','','','',50);
		$AWISCursorPosition='txtZLA_ARTIKELNUMMER';

		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AID_NR'].':',170);
		$SQL = 'SELECT AID_NR, AID_BEZEICHNUNG ';
		$SQL .= ' FROM ARTIKELSTAMMIDTYPEN';
		$SQL .= ' WHERE AID_NR IN (5,2)';     // TecDoc und OENummer
		$SQL .= ' ORDER BY AID_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('ZLA_AID_NR',$rsZLA->FeldInhalt('ZLA_AID_NR'),'200:190',$EditRecht,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
		
		// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLH_KEY'].':',200);
		$SQL = 'SELECT ZLH_KEY, ZLH_KUERZEL || \' - \' || ZLH_BEZEICHNUNG ';
		$SQL .= ' FROM ZUKAUFLIEFERANTENHERSTELLER';
		$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('!ZLA_ZLH_KEY',$rsZLA->FeldInhalt('ZLA_ZLH_KEY'),'370:360',$EditRecht,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);


		$Form->ZeileEnde();

			// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('!ZLA_BEZEICHNUNG',$rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'),55,370,$EditRecht);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_TAUSCHTEIL'].':',170);
		$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
		$Form->Erstelle_SelectFeld('ZLA_TAUSCHTEIL',$rsZLA->FeldInhalt('ZLA_TAUSCHTEIL'),200,$EditRecht,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_GUELTIGAB'].':',200);
		$Form->Erstelle_TextFeld('ZLA_GUELTIGAB',$rsZLA->FeldInhalt('ZLA_GUELTIGAB'),10,150,$EditRecht,'','','','D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_GUELTIGBIS'].':',100);
		$Form->Erstelle_TextFeld('ZLA_GUELTIGBIS',$rsZLA->FeldInhalt('ZLA_GUELTIGBIS'),10,120,$EditRecht,'','','','D');
		$KateogrieAlt = explode("|",$AWISSprachKonserven['Liste']['lst_AktivInaktiv']);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_STATUS'].':',170);
		$Form->Erstelle_SelectFeld('ZLA_STATUS',$rsZLA->FeldInhalt('ZLA_STATUS'),200,$EditRecht,'','','','','',$KateogrieAlt);
		$Form->ZeileEnde();

		$Form->Trennzeile('L');

		// ATU Nummer mit Preiswirkung
		$Form->ZeileStart();
		$Link = '';
		if($rsZLA->FeldInhalt('ZLA_AST_ATUNR')!='')
		{
			$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsZLA->FeldInhalt('ZLA_AST_ATUNR').'&cmdAktion=ArtikelInfos';
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'',$Link);
		$Form->Erstelle_TextFeld('ZLA_AST_ATUNR',$rsZLA->FeldInhalt('ZLA_AST_ATUNR'),10,150,($Recht10001&64),'','','','T');
		if($rsZLA->FeldInhalt('ZLA_AST_ATUNR')!='')
		{
			$SQL = 'SELECT AST_VK ';
			$SQL .= ' FROM Artikelstamm';
			$SQL .= ' WHERE AST_ATUNR = \''.$rsZLA->FeldInhalt('ZLA_AST_ATUNR').'\'';
			$rsAST = $DB->RecordSetOeffnen($SQL);
			if(!$rsAST->EOF())
			{
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'].':',100);
				$Form->Erstelle_TextFeld('*AST_VK',$rsAST->FeldInhalt('AST_VK'),10,120,false,'','','','N2');

				$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_PRUEFSTATUS'].':',170);
				$Pruefung = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
				$Form->Erstelle_SelectFeld('ZLA_PRUEFSTATUS',($AWIS_KEY1===0?'0':$rsZLA->FeldInhalt('ZLA_PRUEFSTATUS')),200,$EditRecht,'','','','','',$Pruefung);
			}
		}
		else
		{
		    $SQL = 'SELECT DISTINCT TEI_SUCH1, TEI_KEY1';
		    $SQL .= ' FROM Lieferantenartikel';
		    $SQL .= ' INNER JOIN Teileinfos ON TEI_ITY_ID1 = '.$DB->WertSetzen('TEI', 'T', 'AST');
		    $SQL .= ' AND TEI_KEY2 = LAR_KEY AND TEI_ITY_ID2 = '.$DB->WertSetzen('TEI', 'T', 'LAR');
		    $SQL .= ' WHERE LAR_SUCH_LARTNR = ASCIIWORT('.$DB->WertSetzen('TEI', 'T', $rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER')).')';
		    $rsTEI = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TEI'));
		    //$Form->DebugAusgabe(1,$DB->LetzterSQL());
		    while(!$rsTEI->EOF())
		    {
		        $Link ='/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY='.$rsTEI->FeldInhalt('TEI_KEY1');
                $Form->Erstelle_TextLabel($rsTEI->FeldInhalt('TEI_SUCH1'),80,'',$Link);

		        $rsTEI->DSWeiter();
		    }
		}
		$Form->ZeileEnde();

		// ATU Nummer ohne Preiswirkung
		$SQL = 'SELECT * FROM zukauflieferantenartikelids';
		$SQL .= ' WHERE zai_zla_key = 0'.$rsZLA->FeldInhalt('ZLA_KEY');
		$SQL .= ' AND zai_aid_nr = 4';		// "weitere ATU Nummer!"
		$rsZAI = $DB->RecordsetOeffnen($SQL);

		$Form->ZeileStart();
		$Link = '';
		if($rsZAI->FeldInhalt('ZAI_ID')!='')
		{
			$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsZAI->FeldInhalt('ZAI_ID').'&cmdAktion=ArtikelInfos';
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZUSATZATUNR'].':',200,'',$Link);
		$Form->Erstelle_TextFeld('ZAI_ID_'.$rsZAI->FeldInhalt('ZAI_KEY').'_4',$rsZAI->FeldInhalt('ZAI_ID'),10,150,($Recht10001&(2^7)),'','','','T');

		// ATU Nummer �hnlicher Artikel
		$SQL = 'SELECT * FROM zukauflieferantenartikelids';
		$SQL .= ' WHERE zai_zla_key = 0'.$rsZLA->FeldInhalt('ZLA_KEY');
		$SQL .= ' AND zai_aid_nr = 7';
		$rsZAI = $DB->RecordsetOeffnen($SQL);

		$Link = '';
		if($rsZAI->FeldInhalt('ZAI_ID')!='')
		{
			$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsZAI->FeldInhalt('ZAI_ID').'&cmdAktion=ArtikelInfos';
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['AEHNLICHEATUNR'].':',200,'',$Link);
		$Form->Erstelle_TextFeld('ZAI_ID_'.$rsZAI->FeldInhalt('ZAI_KEY').'_7',$rsZAI->FeldInhalt('ZAI_ID'),10,200,($Recht10001&(2^7)),'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ATUNRBEMERKUNG'].':',200,'');
		$Form->Erstelle_TextFeld('ZLA_ATUNRBEMERKUNG',$rsZLA->FeldInhalt('ZLA_ATUNRBEMERKUNG'),80,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$SQL = 'SELECT * FROM (';
		$SQL .= 'SELECT ZUKAUFANPASSUNGENPROTOKOLL.*';
		$SQL .= ',row_number() over (ORDER BY ZUP_KEY DESC) AS ZeilenNr';
		$SQL .= ' FROM ZUKAUFANPASSUNGENPROTOKOLL';
		$SQL .= ' INNER JOIN ZUKAUFARTIKEL ON ZUP_XXX_KEY = ZLA_KEY';
		$SQL .= ' WHERE ZUP_XTN_KUERZEL ='.$DB->WertSetzen('ZUP', 'T','ZLA');
		$SQL .= ' AND ZUP_XXX_KEY ='.$DB->WertSetzen('ZUP', 'T',$AWIS_KEY1);
		$SQL .= ' ) DATEN WHERE ZeilenNr = 1';
		$rsZUP = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZUP'));
		if(!$rsZUP->EOF())
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUP']['ZUP_AKTION'].':',200,'');
		    $Form->Erstelle_TextLabel($Form->Format('D',$rsZUP->FeldInhalt('ZUP_USERDAT')).', '.$rsZUP->FeldInhalt('ZUP_AKTION'),600,'');
		    $Form->ZeileEnde();
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_SORTIMENT'].':',200,'');
		$SQL = "select ast_atunr, ast_atunr as anzeige from artikelstamm where ast_atunr like 'ZUK___'";
		$Form->Erstelle_SelectFeld('ZLA_SORTIMENT',$rsZLA->FeldInhalt('ZLA_SORTIMENT'),200,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();

		$Form->Formular_Ende();


		if(!isset($_POST['cmdDSNeu_x']))
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Lieferanten'));
			echo '<input type=hidden name=Seite value='.$DB->FeldInhaltFormat('T',$RegisterSeite,'DB',false).'>';

			$Reg = new awisRegister(10002);
			$Reg->ZeichneRegister($RegisterSeite);
		}
	}

	//awis_Debug(1, $Param, $Bedingung, $rsZLA, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht10001&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ZLA_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['ZLA_ARTIKELNUMMER']) AND $Param['ZLA_ARTIKELNUMMER']!='')
	{
		$ArtNr = awisWerkzeuge::ArtNrKomprimiert($Param['ZLA_ARTIKELNUMMER'],array(ord('*'),ord('_')));
		$Bedingung .= ' AND ((ZLA_ARTIKELNUMMER) ' . $DB->LIKEoderIST($ArtNr) . ' ';
//		$Bedingung .= ' AND ((ZLA_ARTIKELNUMMER) ' . $DB->LIKEoderIST($Param['ZLA_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';

		//$Bedingung .= ' OR EXISTS(SELECT * FROM ZUKAUFARTIKELLIEFERANTEN WHERE (ZAL_BESTELLNUMMER) ' . $DB->LIKEoderIST($Param['ZLA_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ' AND ZAL_ZLA_KEY = ZLA_KEY)';
		//$Bedingung .= ' OR ZAL_KEY IS NOT NULL';
		$Bedingung .= ')';
	}
	if(isset($Param['ZLA_BEZEICHNUNG']) AND $Param['ZLA_BEZEICHNUNG']!='')
	{
		$Bedingung .= ' AND (UPPER(ZLA_BEZEICHNUNG) ' . $DB->LIKEoderIST($Param['ZLA_BEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['LIE_NR']) AND $Param['LIE_NR']>0)
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM ZUKAUFARTIKELLIEFERANTEN WHERE ZAL_ZLA_KEY = ZLA_KEY AND ZAL_LIE_NR = ' . $DB->FeldInhaltFormat('T',$Param['LIE_NR']) . ') ';
	}

	if(isset($Param['ZAL_BESTELLNUMMER']) AND $Param['ZAL_BESTELLNUMMER']!='')
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM ZUKAUFARTIKELLIEFERANTEN WHERE ZAL_ZLA_KEY = ZLA_KEY AND ZAL_BESTELLNUMMER = ' . $DB->FeldInhaltFormat('T',$Param['ZAL_BESTELLNUMMER']) . ') ';
	}
	if(isset($Param['ZLA_ZLH_KEY']) AND $Param['ZLA_ZLH_KEY']>0)
	{
		$Bedingung .= ' AND ZLA_ZLH_KEY = ' . intval($Param['ZLA_ZLH_KEY']) . ' ';
	}
	if(isset($Param['ZLA_ZSZ_KEY']) AND $Param['ZLA_ZSZ_KEY']>0)
	{
		$Bedingung .= ' AND ZLA_ZSZ_KEY= ' . intval($Param['ZLA_ZSZ_KEY']) . ' ';
	}
	if(isset($Param['ZLA_AST_ATUNR']) AND $Param['ZLA_AST_ATUNR']!='')
	{
		$Bedingung .= ' AND ((ZLA_AST_ATUNR) ' . $DB->LIKEoderIST($Param['ZLA_AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['ZAI_ID']) AND $Param['ZAI_ID']!='')
	{
		$Bedingung .= ' AND ZLA_KEY IN (SELECT ZAI_ZLA_KEY FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE UPPER(ZAI_ID) ' . $DB->LIKEoderIST($Param['ZAI_ID'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}
	if(isset($Param['ZLA_TAUSCHTEIL']) AND $Param['ZLA_TAUSCHTEIL']>0)
	{
		$Bedingung .= ' AND ZLA_TAUSCHTEIL = ' . intval($Param['ZLA_TAUSCHTEIL']) . ' ';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>