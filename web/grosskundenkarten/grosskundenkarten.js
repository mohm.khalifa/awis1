$(document).ready(function () {
    var zuDrucken = [];
    var count = 0;
    $('#cmdPDF_IE').on('click', function () {
        $('.kdcheck:checkbox:checked').each(function () {
            zuDrucken.push($(this).attr('data-kdnr'));

            //Erste PDF gleich selbst ausl�sen
            if (count == 0) {
                naechstePDF();
            }
            count = count + 1;
        });
    });

    function naechstePDF() {
        console.log(zuDrucken);
        if( typeof zuDrucken[0] !== 'undefined'){
            createPDF(zuDrucken[0]);
        }
    }

    function createPDF(KDNR) {
        $.get('grosskundenkarten_pdf_handler.php?KDNR=' + KDNR, {}).done(function (retVal) {
            if(retVal != "queued" && (retVal == '1' || retVal == '2')){
                switch(Number(retVal)){
                    case 1: alert('Es ist ein Fehler bei der Erzeugung der PDF-Datei aufgetreten. Aktueller Druckstatus: 1 - Drucker aktuell in Verwendung');
                            break;
                    case 2: alert('Es ist ein Fehler bei der Erzeugung der PDF-Datei aufgetreten. Aktueller Druckstatus: 2 - PDF-Druck bereit');
                            break;
                    default: alert('Es ist ein Fehler bei der Erzeugung der PDF-Datei aufgetreten. Druckstatus konnte nicht ermittelt werden.');
                            break;
                }
            }


            $("#fenster_2").find("div.fensterInhalt").html('Wurde(n) die Karte(n) mit der ' +
                '<br>Kundennummer: <b> ' + KDNR + '</b><br>' +
                ' erfolgreich gedruckt?');
            $('#print_ok').attr('KDNR', KDNR);
            $('#print_nok').attr('KDNR', KDNR);
            DialogPopup("2");



        });
    }


    $('#print_ok').click(function () {
        $.get('grosskundenkarten_pdf_status.php?KDNR=' + $(this).attr('kdnr'), {}).done(function (retVal) {
            if (retVal == "ok") {
                DialogPopup_schliessen("2");
                zuDrucken.shift();
                setTimeout(naechstePDF, 3000);

            } else {
                alert('Beim �ndern des Status der Karte ist ein Fehler aufgetreten.');
            }
        });
    });

    $('#print_nok').click(function () {
        DialogPopup_schliessen("2");
        naechstePDF();
    });

    $('#print_leave').click(function () {
        window.location.reload(false);
    });

});

