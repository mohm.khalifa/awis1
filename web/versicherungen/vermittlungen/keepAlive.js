function executeQuery() {

    var URL = window.location.protocol + '//' + window.location.host + '/daten/versvermittlungen_keepAlive.php';

    URL += '?VBA_KEY=' + getKey();
    $.ajax({
        url: URL,
        success: function (data) {
            //Return hier auswerten.
        }
    });
    setTimeout(executeQuery, 10000); // Nach dem ersten lauf, alle 10 Sekunden wieder aufrufen
}

$(document).ready(function () {
    // Nach 10 Sekunden das erste mal aufrufen
    setTimeout(executeQuery, 10000);
});

function getKey() {
    var VDB_KEYS = '';
    VDB_KEYS = $('#txtVBA_KEY').val();
    return VDB_KEYS;
}
