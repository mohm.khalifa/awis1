<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FIK', '%');
    $TextKonserven[] = array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Wort','lbl_trefferliste');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht5001 = $AWISBenutzer->HatDasRecht(5001);
    $Recht5020 = $AWISBenutzer->HatDasRecht(5020);        // Inhalte der Seite
    if (($Recht5001 & 8) == 0) {
        $Form->Fehler_KeineRechte();
        die();
    }

    if(isset($_GET['FIK_KEY']) and $_GET['FIK_KEY'] == 0) {

        $SQL ='SELECT FIK_KEY, FIK_BEZEICHNUNG';
        $SQL .=' FROM FILIALKOMPETENZENTYPEN';
        $SQL .=' WHERE FIK_KEY not in';
        $SQL .=' (select FIF_WERT from filialinfos where FIF_FIL_ID = '.$DB->WertSetzen('FIK', 'N0', $AWIS_KEY1).' and FIF_FIT_ID = 920)';

        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialinfos_Main.php?cmdAktion=Details&Seite=Filialkompetenzen accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Form->InfoZeile($Felder,'');

        if ($DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('FIK',false)) > 0) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel('Kompetenz:',150);
            $Form->Erstelle_SelectFeld('FIK_BEZ','',300,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',null,'','',$DB->Bindevariablen('FIK'));
            $Form->ZeileEnde();
        } else {
            $Form->Hinweistext('Dieser Filiale wurden bereits alle möglichen Kompetenzen zugeordnet.', 1);
        }

    } else {

        $SQL = 'SELECT *';
        $SQL .= ' FROM FILIALINFOS';
        $SQL .= ' INNER JOIN FILIALKOMPETENZENTYPEN';
        $SQL .= ' ON FILIALINFOS.FIF_WERT = FILIALKOMPETENZENTYPEN.FIK_KEY';
        $SQL .= ' WHERE FIF_FIT_ID = 920';
        $SQL .= ' AND FIF_FIL_ID = ' . $DB->WertSetzen('FIK', 'N0', $AWIS_KEY1).' and FIF_FIT_ID = 920';

        $rsFIK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FIK'));

        $Form->ZeileStart();
        if (($Recht5020 & 2) == 2)	// Ändernrecht
        {
            $Icons[] = array('new', './filialinfos_Main.php?cmdAktion=Details&Seite=Filialkompetenzen&FIK_KEY=0');
            $Form->Erstelle_ListeIcons($Icons,38,-1);
        }
        $Form->Erstelle_Liste_Ueberschrift('Filialkompetenzen', 500);
        $Form->ZeileEnde();

        $DS = 0;

        if ($rsFIK->AnzahlDatensaetze() > 0)
        {
            while (!$rsFIK->EOF()) {
                $HG = ($DS % 2);

                $Form->ZeileStart();
                $Icons = array();

                if (($Recht5020 & 2) == 2)	// Ändernrecht
                {
                    $Icons[] = array('delete','./filialinfos_Main.php?cmdAktion=Details&Seite=Filialkompetenzen&Del='.$rsFIK->FeldInhalt('FIF_KEY'));
                    $Form->Erstelle_ListeIcons($Icons,38,($DS%2));
                }
                $Form->Erstelle_ListenFeld('FIK_BEZ', $rsFIK->FeldInhalt('FIK_BEZEICHNUNG'), '', 500, false, $HG);
                $Form->ZeileEnde();

                $rsFIK->DSWeiter();
                $DS++;
            }
        } else {
            $Form->Hinweistext($AWISSprachKonserven['FIK']['FIK_KEINDS'], 1);
        }
    }
}
catch (Exception $ex)
{

}
?>
