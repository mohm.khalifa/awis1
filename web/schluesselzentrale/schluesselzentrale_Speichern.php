<?php
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;

try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('SCH', '%');
    $TextKonserven[]=array('Wort', 'lbl_zurueck');
    $TextKonserven[]=array('Wort', 'lbl_suche');
    $TextKonserven[]=array('Wort', 'lbl_trefferliste');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Fehler', 'err_keineDaten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

  //  echo "<form name=frmSpeichern method=post action=./schluesselzentrale_Details_Listenansicht.php>";

    $Recht25040=$AWISBenutzer->HatDasRecht(25040);
    if($Recht25040 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
//var_dump($_POST);   
    if(isset($_POST['cmdSuche_x']))
    {
        $Param = array();
        $Param['KEY']=0;			

        $Param['SCH_Key']=$_POST['sucSCH_Key'];	
        $Param['TUR_Key']=$_POST['sucTUR_Key'];	
        $Param['KON_Key']=$_POST['sucKON_Key'];	
                
        $Param['SCH_Historie']=(isset($_POST['sucSCH_Historie'])?'on':'');
        $Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

        $Param['ORDERBY']='';
        $Param['BLOCK']='';
        $Param['KEY']=0;	
        
        $AWISBenutzer->ParameterSchreiben("Formular_SCH",serialize($Param));
    }
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_SCH"));
    
    
    $MAS_Key = '';

    if(isset($_POST['sucKON_Key']))	
    {
        $KON_Key = $_POST['sucKON_Key'];

        // Neuen DS einf�gen
        $SQLAnlegen = 'INSERT INTO SchlMitarbeiterSchluessel (MAS_KON_Key, MAS_SCH_Key, MAS_Ausgabedatum, MAS_Rueckgabedatum,';
        $SQLAnlegen.= ' MAS_Verlustdatum, MAS_Bemerkung) VALUES(';
        $SQLAnlegen.= $DB->FeldInhaltFormat('N0', $_POST['sucKON_Key'], true) . ", ";//nix txt!!!Selectfeld!!!
        $SQLAnlegen.= $DB->FeldInhaltFormat('N0', $_POST['txtSCH_Key'], true) . ", ";
        $SQLAnlegen.= $DB->FeldInhaltFormat('D', $_POST['txtAusgabedatum'], true) . ", ";
        $SQLAnlegen.= $DB->FeldInhaltFormat('D', $_POST['txtRueckgabedatum'], true) . ", ";
        $SQLAnlegen.= $DB->FeldInhaltFormat('D', $_POST['txtVerlustdatum'], true) . ", ";
        $SQLAnlegen.= $DB->FeldInhaltFormat('T', $_POST['txtBemerkung'], true);
        $SQLAnlegen.= ' )';
        if($DB->Ausfuehren($SQLAnlegen)===false)
        {
            $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201212051504');
        }
    }

    elseif(isset($_POST['txtMAS_Key'])or isset($_POST['txtKON_Key']))
    {
        //bestehenden DS bearbeiten
        $SQLSpeichern = 'Update SchlMitarbeiterSchluessel mas SET  ';
        if($_POST['txtRueckgabedatum']!= '')
        {
            $SQLSpeichern.= 'mas.MAS_Rueckgabedatum = '.$DB->FeldInhaltFormat('D', $_POST['txtRueckgabedatum'], true) . ",";
        }
        if($_POST['txtVerlustdatum']!= '')
        {
            $SQLSpeichern.= 'mas.MAS_Verlustdatum ='.$DB->FeldInhaltFormat('D', $_POST['txtVerlustdatum'], true) . ",";
        }
        if($_POST['txtBemerkung']!= $_POST['oldBemerkung'])
        {
            $SQLSpeichern.= 'mas.MAS_Bemerkung ='. $DB->FeldInhaltFormat('T', $_POST['txtBemerkung'], true). " ";
        } 
        
        //wenn das letzte Zeichen von $SQLSpeichern ein ',' ist, dann das letzte Zeichen wegschneiden
        if($SQLSpeichern{strlen($SQLSpeichern)-1}==',')
        {
            $SQLSpeichern = substr($SQLSpeichern,0,-1);
        }
        if(isset($_POST['txtMAS_Key']))
        {
            $SQLSpeichern.= ' WHERE mas.MAS_Key = '.$_POST['txtMAS_Key'];
        }
        elseif(isset($_POST['txtKON_Key']))
        {
            $SQLSpeichern.= ' WHERE mas.MAS_KON_Key = '.$_POST['txtKON_Key'];
        }
        if($DB->Ausfuehren($SQLSpeichern)===false)
        {
            $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201212051504');
        }
    }
    else
    {
        //Wenn Schluesselgruppe noch nicht vorhanden ist, dann einf�gen
        $SQL = 'SELECT count(SGR_Key) AS Anzahl FROM SchlSchluesselgruppen WHERE SGR_Bezeichnung like \''.$_POST['txtSchliessgruppe'].'\'';
        $rsPEI  = $DB->RecordSetOeffnen($SQL);
        $Test = $rsPEI->FeldInhalt('1');
        if($Test == 0)
        {
            $SQL_SGR_Insert= 'INSERT INTO SchlSchluesselgruppen (SGR_BEZEICHNUNG) Values(';
            $SQL_SGR_Insert.= $DB->FeldInhaltFormat('T', $_POST['txtSchliessgruppe'], true).')';
            if($DB->Ausfuehren($SQL_SGR_Insert)===false)
            {
                $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201212051504');
            }
        }
        else
        {
            //echo 'Schl�sselgruppe schon vorhanden!!';
        }
        //SGR_Key auslesen
        $SQL_SGR = 'SELECT DISTINCT SGR_Key FROM SchlSchluesselgruppen WHERE SGR_Bezeichnung like \''.$_POST['txtSchliessgruppe'].'\'';
        $rsPEI  = $DB->RecordSetOeffnen($SQL_SGR);
        $SGR_Key = $rsPEI->FeldInhalt('1');   
        //Wenn T�r noch  nicht angelegt ist alle Werte einf�gen       
        $SQL = 'SELECT count(TUR_Key) AS Anzahl FROM SchlTueren WHERE TUR_Bezeichnung like \''.$_POST['txtTuerBezeichnung'].'\'';
        $SQL.= ' AND TUR_Bauteil like \''.$_POST['txtTuerBauteil'].'\'';
        $SQL.= ' AND TUR_Stockwerk like \''.$_POST['txtTuerStockwerk'].'\'';
        $SQL.= ' AND TUR_Raum like \''.$_POST['txtTuerBuero'].'\'';
        $rsPEI  = $DB->RecordSetOeffnen($SQL);
        $Test = $rsPEI->FeldInhalt('1');        
        if($Test == 0)
        {
            $SQL_TUR_Insert= 'INSERT INTO SchlTueren (TUR_BEZEICHNUNG';
            if($_POST['txtTuerBauteil']!='')
            {
                $SQL_TUR_Insert.= ', TUR_Bauteil';
            }
            if($_POST['txtTuerStockwerk']!='')
            {
                $SQL_TUR_Insert.= ', TUR_Stockwerk';
            }
            if($_POST['txtTuerBuero']!='')
            {
                $SQL_TUR_Insert.= ', TUR_Raum';
            }
            $SQL_TUR_Insert.= ') Values(';
            $SQL_TUR_Insert.= $DB->FeldInhaltFormat('T', $_POST['txtTuerBezeichnung'], true).' ,';
            $SQL_TUR_Insert.= $DB->FeldInhaltFormat('T', $_POST['txtTuerBauteil'], true).' ,';
            $SQL_TUR_Insert.= $DB->FeldInhaltFormat('T', $_POST['txtTuerStockwerk'], true).' ,';
            $SQL_TUR_Insert.= $DB->FeldInhaltFormat('T', $_POST['txtTuerBuero'], true).')';
            if($DB->Ausfuehren($SQL_TUR_Insert)===false)
            {
                $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201212051504');
            }
        }
        else
        {
            //echo 'T�r schon vorhanden!!';
        }
        //TUR_Key auslesen
        $SQL_TUR = 'SELECT DISTINCT TUR_Key FROM SchlTueren WHERE TUR_Bezeichnung like \''.$_POST['txtTuerBezeichnung'].'\'';
        $rs_TUR  = $DB->RecordSetOeffnen($SQL_TUR);
        $TUR_Key = $rs_TUR->FeldInhalt('1');
       
        //Zum Schluss DS in Tabelle SchlSchluessel anlegen und die PK als FK eintragen
        //Wenn Schluessel noch nicht vorhanden ist, dann einf�gen
        $SQL = 'SELECT count(SGR_Key) AS Anzahl FROM SchlSchluesselgruppen WHERE SGR_Bezeichnung like \''.$_POST['txtSchliessgruppe'].'\'';
        $rsPEI  = $DB->RecordSetOeffnen($SQL);
        $Test = $rsPEI->FeldInhalt('1');
        if($Test == 0)
        {
            $SQL_SGR_Insert= 'INSERT INTO SchlSchluesselgruppen (SGR_BEZEICHNUNG) Values(';
            $SQL_SGR_Insert.= $DB->FeldInhaltFormat('T', $_POST['txtSchliessgruppe'], true).')';
            if($DB->Ausfuehren($SQL_SGR_Insert)===false)
            {
                $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201212051504');
            }
        }
        else
        {
            //echo 'Schl�sselgruppe schon vorhanden!!';
        }
        $SQL = 'SELECT count(SCH_Key) AS Anzahl FROM SchlSchluessel WHERE SCH_Bezeichnung like \''.$_POST['txtSchluesselNr'].'\'';
        $rsPEI  = $DB->RecordSetOeffnen($SQL);
        $Test = $rsPEI->FeldInhalt('1');
        if($Test == 0)
        {
            $SQLAnlegen = 'INSERT INTO SchlSchluessel (SCH_Bezeichnung, SCH_Anzahl, SCH_SGR_Key, SCH_TUR_Key) VALUES(';
            $SQLAnlegen.= $DB->FeldInhaltFormat('T', $_POST['txtSchluesselNr'], true) . ", ";
            $SQLAnlegen.= $DB->FeldInhaltFormat('N0', $_POST['txtAnzahl'], true) . ", ";
            $SQLAnlegen.= $SGR_Key. ", ";
            $SQLAnlegen.= $TUR_Key. ")";
            if($DB->Ausfuehren($SQLAnlegen)===false)
            {
                $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201212051504');
            }
        }
    }
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>