<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SVT','%');
	$TextKonserven[]=array('XDI','%');
	$TextKonserven[]=array('Liste','SVT_BENACHRICHTIGUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Wort','EANPruefung%');
	$TextKonserven[]=array('Wort','FSNummer%');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISWerkzeuge = new awisWerkzeuge();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19000 = $AWISBenutzer->HatDasRecht(19000);
	if(($Recht19000)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$SVTParam = unserialize($AWISBenutzer->ParameterLesen('Formular_SVT_P'));

	if(!isset($SVTParam['BLOCK']) OR $SVTParam['SVB_KEY']!=$AWIS_KEY1)
	{
		$SVTParam['BLOCK']=1;
		$SVTParam['KEY']='';
		$SVTParam['SVB_KEY']=$AWIS_KEY1;
	}

	if(!isset($_GET['SSort']))
	{
		if(isset($SVTParam['ORDERBY']) AND $SVTParam['ORDERBY']!='')
		{
			$ORDERBY = ' ORDER BY '.$SVTParam['ORDERBY'];
		}
		else
		{
			$ORDERBY = ' ORDER BY SVT_DATUMAB DESC';
		}
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		
		$SVTParam['ORDERBY']=$OrderBy;
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT SVCTeilnehmer.*, Importprotokoll.*';
	$SQL .= ', (SELECT COUNT(*) FROM SVCLeistungen WHERE SVL_SVT_KEY = SVT_KEY) AS ANZLEISTUNGEN';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ', CASE WHEN SVT_DATUMBIS >= SYSDATE THEN 0 ELSE 1 END AS ABGELAUFEN';
	$SQL .= ' FROM SVCTeilnehmer ';
	$SQL .= ' LEFT OUTER JOIN Importprotokoll ON SVT_XDI_KEY = XDI_KEY';
	$SQL .= ' WHERE SVT_SVB_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['ALLE']))
	{
		$SVTParam['alle_anzeigen'] = intval($_GET['ALLE']);
	}
	if(isset($SVTParam['alle_anzeigen']) AND $SVTParam['alle_anzeigen']==0)
	{
		$SQL .= ' AND SVT_DATUMBIS >= SYSDATE';
	}
	

	if(isset($_GET['SVTListe']))
	{
	    $AWIS_KEY2=0;
	}
	elseif(isset($_GET['SVT_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['SVT_KEY']);
	}
	elseif($AWIS_KEY2=='')
	{
		if($SVTParam['SVB_KEY']==$AWIS_KEY1)
		{
			$AWIS_KEY2 = $SVTParam['KEY'];
		}
		else
		{
			$SVTParam['BLOCK']=1;
			$SVTParam['KEY']='';
			$AWIS_KEY2=0;
		}
	}

	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND SVT_KEY = 0'.$AWIS_KEY2;
		$_GET['SVT_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}

	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$SVTParam['BLOCK']=$Block;
		}
		elseif(isset($SVTParam['BLOCK']))
		{
			$Block=$SVTParam['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$rsSVT = $DB->RecordsetOeffnen($SQL);

    $Form->Formular_Start();

	if($rsSVT->AnzahlDatensaetze()>1 OR isset($_GET['SVTListe']) OR !isset($_GET['SVT_KEY']))
	{
		$Form->ZeileStart();
		$Icons=array();
		if((intval($Recht19000)&6)!=0)
		{
			$Icons[] = array('new','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVB_KEY='.$AWIS_KEY1.'&SVT_KEY=-1');
  		}

		if(isset($SVTParam['alle_anzeigen']) AND $SVTParam['alle_anzeigen']==0)
		{
			$Icons[] = array('filteraus','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVB_KEY='.$AWIS_KEY1.'&ALLE=1','f',$AWISSprachKonserven['SVT']['ttt_InaktiveEinblenden']);
  		}
		else
		{
			$Icons[] = array('filterein','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVB_KEY='.$AWIS_KEY1.'&ALLE=0','f',$AWISSprachKonserven['SVT']['ttt_InaktiveAusblenden']);
  		}

  		$Form->Erstelle_ListeIcons($Icons,38,-1);

  		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SVT_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SVT_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['SVT_BEZEICHNUNG'],350,'',$Link);

  		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SVT_KENNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SVT_KENNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['SVT_KENNUNG'],150,'',$Link);

  		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SVT_DATUMAB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SVT_DATUMAB'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['SVT_DATUMAB'],100,'',$Link);

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['ANZLEISTUNGEN'],100,'','');

		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$SVTParam['KEY']='';
		$SVTParam['SVB_KEY']=$AWIS_KEY1;

		$DS=0;
		while(!$rsSVT->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht19000&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVB_KEY='.$AWIS_KEY1.'&SVT_KEY='.$rsSVT->FeldInhalt('SVT_KEY'));
			}
			if(intval($Recht19000&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVB_KEY='.$AWIS_KEY1.'&Del='.$rsSVT->FeldInhalt('SVT_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Style = ($rsSVT->FeldInhalt('ABGELAUFEN')==1?'font-style:italic;color:#FF0000;':'');
			$Form->Erstelle_ListenFeld('*SVT_BEZEICHNUNG',$rsSVT->FeldInhalt('SVT_BEZEICHNUNG'),20,350,false,($DS%2),$Style,'','T');
			$Form->Erstelle_ListenFeld('*SVT_KENNUNG',$rsSVT->FeldInhalt('SVT_KENNUNG'),20,150,false,($DS%2),$Style,'','T');
			$Form->Erstelle_ListenFeld('*SVT_DATUMAB',$rsSVT->FeldInhalt('SVT_DATUMAB'),20,100,false,($DS%2),$Style,'','T');
			$Form->Erstelle_ListenFeld('*ANZLEISTUNGEN',$rsSVT->FeldInhalt('ANZLEISTUNGEN'),20,100,false,($DS%2),$Style,'','N0');
            if($rsSVT->FeldInhalt('SVT_BEMERKUNG')!='')
            {
                $Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsSVT->FeldInhalt('SVT_BEMERKUNG'));
            }
			$Form->ZeileEnde();

			$rsSVT->DSWeiter();
			$DS++;
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsSVT->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht19000&6);
		$AWIS_KEY2 = $rsSVT->FeldInhalt('SVT_KEY');
		$SVTParam['KEY']=$AWIS_KEY2;
		$SVTParam['SVB_KEY']=$AWIS_KEY1;

		$Form->Erstelle_HiddenFeld('SVT_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('SVT_SVB_KEY',$AWIS_KEY1);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVTListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVT->FeldInhalt('SVT_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVT->FeldInhalt('SVT_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('SVT_BEZEICHNUNG',$rsSVT->FeldInhalt('SVT_BEZEICHNUNG'),50,500,$EditModus,'','','','T');
		if($EditModus)
		{
			$AWISCursorPosition='txtSVT_BEZEICHNUNG';
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		if($rsSVT->FeldInhalt('SVT_KENNUNG')!='')
		{
			$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_KENNUNG'].':',184);
			$OK=true;
			$Kennung = $rsSVT->FeldInhalt('SVT_KENNUNG');
			if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_EAN,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
			{
				if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_EUROSA,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
				{
					if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_EUKARTE,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
					{
						if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_GRAU,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
						{
							if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_DDR,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
							{
								$OK=false;
							}
						}
					}
				}
			}
			else
			{
				$Form->Erstelle_Bild('image', 'OK', '', '/bilder/icon_ok.png',$AWISSprachKonserven['Wort']['EANPruefung_OK'],'',16,16);
				$OK=false;
			}
				
			if($OK)
			{
				$Form->Erstelle_Bild('image', 'OK', '', '/bilder/icon_ok.png',$AWISSprachKonserven['Wort']['FSNummer_OK'],'',16,16);
			}
			else
			{
				$Form->Erstelle_Bild('image', 'ERR', '', '/bilder/icon_warnung.png',$AWISSprachKonserven['Wort']['FSNummer_FALSCH'],'',16,16);
			}

		}
		else
		{
			$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_KENNUNG'].':',200);
		}
		$Form->Erstelle_TextFeld('!SVT_KENNUNG',$rsSVT->FeldInhalt('SVT_KENNUNG'),22,300,$EditModus,'','','','T','','','',13);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_DATUMAB'].':',200);
		$Form->Erstelle_TextFeld('!SVT_DATUMAB',$rsSVT->FeldInhalt('SVT_DATUMAB'),10,100,$EditModus,'','','','D','','',date('d.m.Y'));
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_DATUMBIS'].':',200);
		$Form->Erstelle_TextFeld('!SVT_DATUMBIS',$rsSVT->FeldInhalt('SVT_DATUMBIS'),10,100,$EditModus,'','','','D','','','31.12.2030');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_BENACHRICHTIGUNG'].':',200);
		$Methoden = explode("|",$AWISSprachKonserven['Liste']['SVT_BENACHRICHTIGUNG']);
		foreach($Methoden AS $Methode)
		{
			$Methode = explode('~',$Methode);

			$Wert = ((int)$rsSVT->FeldInhalt('SVT_BENACHRICHTIGUNG')&(Int)$Methode[0]);

			$Form->Erstelle_Checkbox('BENACHRICHTIGUNG_'.$Methode[0],($Wert?'on':''), 50, $EditModus,'on');
			$Form->Erstelle_TextLabel($Methode[1],200);
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('&nbsp;',200);
		}

		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_MAILADRESSE'].':',200);
		$Form->Erstelle_TextFeld('SVT_MAILADRESSE',$rsSVT->FeldInhalt('SVT_MAILADRESSE'),80,600,$EditModus,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_SMSNUMMER'].':',200);
		$Form->Erstelle_TextFeld('SVT_SMSNUMMER',$rsSVT->FeldInhalt('SVT_SMSNUMMER'),20,350,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_ANREDE'].':',200);
		$Form->Erstelle_TextFeld('SVT_ANREDE',$rsSVT->FeldInhalt('SVT_ANREDE'),10,350,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_NAME'].':',200);
		$Form->Erstelle_TextFeld('SVT_NAME',$rsSVT->FeldInhalt('SVT_NAME'),50,330,$EditModus);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_VORNAME'].':',160);
		$Form->Erstelle_TextFeld('SVT_VORNAME',$rsSVT->FeldInhalt('SVT_VORNAME'),50,330,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_ADRESSZUSATZ'].':',200);
		$Form->Erstelle_TextFeld('SVT_ADRESSZUSATZ',$rsSVT->FeldInhalt('SVT_ADRESSZUSATZ'),50,330,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_STRASSE'].':',200);
		$Form->Erstelle_TextFeld('SVT_STRASSE',$rsSVT->FeldInhalt('SVT_STRASSE'),50,330,$EditModus);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_HAUSNUMMER'].':',160);
		$Form->Erstelle_TextFeld('SVT_HAUSNUMMER',$rsSVT->FeldInhalt('SVT_HAUSNUMMER'),10,100,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_LAN_CODE'].':',200,'');
		$SQL = 'select lan_code, lan_land ';
		$SQL .= ' FROM laender';
		$SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('SVT_LAN_CODE',$rsSVT->FeldInhalt('SVT_LAN_CODE'),'200:190',$EditModus,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_PLZ'].':',200);
		$Form->Erstelle_TextFeld('SVT_PLZ',$rsSVT->FeldInhalt('SVT_PLZ'),10,330,$EditModus);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_ORT'].':',160);
		$Form->Erstelle_TextFeld('SVT_ORT',$rsSVT->FeldInhalt('SVT_ORT'),50,100,$EditModus);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_BEMERKUNG'].':',200);
		$Form->Erstelle_TextArea('SVT_BEMERKUNG',$rsSVT->FeldInhalt('SVT_BEMERKUNG'),800,99,5,$EditModus);
		$Form->ZeileEnde();

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XDI']['XDI_DATUM'].':',200);
		$Form->Erstelle_TextFeld('*XDI_DATUM',$rsSVT->FeldInhalt('XDI_DATUM'),10,300,false);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XDI']['XDI_DATEINAME'].':',200);
		$Form->Erstelle_TextFeld('*XDI_DATEINAME',$rsSVT->FeldInhalt('XDI_DATEINAME'),50,400,false);
		$Form->ZeileEnde();

		$Form->Formular_Ende();

		if($rsSVT->FeldInhalt('SVT_KEY')!='')
		{
			$RegSVT = new awisRegister(19030);
			$RegSVT->ZeichneRegister(isset($_GET['Unterseite'])?$_GET['Unterseite']:'');
		}
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}
	$AWISBenutzer->ParameterSchreiben('Formular_SVT_P',serialize($SVTParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>