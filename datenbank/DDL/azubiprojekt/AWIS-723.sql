INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('AZT','Tabelle','AzubiprojekteTeilnehmer','02.08.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('AZB','Tabelle','AzubiprojekteBereiche','02.08.2018');
commit;

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
  65000,
  3,
  'Projektebereiche pflegen'
);
commit;

INSERT INTO webmaskenregister (
  xrg_id,
  xrg_xwm_id,
  xrg_bezeichnung,
  xrg_beschriftung,
  xrg_aktion,
  xrg_seite,
  xrg_bemerkung,
  xrg_xrc_id,
  xrg_rechtestufe,
  xrg_sortierung,
  xrg_neuehilfe
) VALUES (
  650003,
  65000,
  'Pflege',
  'Pflege',
  './azubiprojekte_Main.php?cmdAktion=Pflege',
  './azubiprojekte_Pflege.php',
  'Azubiprojekte Pflegemaske',
  65000,
  8,
  4,
  1
);
commit;


INSERT INTO textkonserven (
  xtx_kennung,
  xtx_bereich,
  xtx_bemerkung,
  xtx_text_de,
  xtx_userdat
) VALUES (
  'reg_650003',
  'Register',
  'Register zu den Azubiprojekten',
  'Pflege',
  '09.08.2018'
);
commit;


INSERT INTO textkonserven (
  xtx_kennung,
  xtx_bereich,
  xtx_bemerkung,
  xtx_text_de,
  xtx_userdat
) VALUES (
  'ttt_650003',
  'Register',
  'Register',
  'Pflege',
  '09.08.2018'
);
commit;


INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('AZB_AKTIVINAKTIV','AZB','A~Aktiv|I~Inaktiv','09.08.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('AZB_INFOTEXT','AZB','Infotext','09.08.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('AZB_DSNEUERFOLG','AZB','Der Themenblock wurde erfolgreich angelegt.','09.08.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('AZB_SPEICHERFOLG','AZB','Ihre Änderungen wurden erfolgereich gespeichert.','09.08.2018');
commit;

Create TABLE ZZZ_Meyer_AZUBIPROJEKTTEXT ( KEY int, INFOTEXT CLOB);
commit;

INSERT INTO ZZZ_Meyer_AZUBIPROJEKTTEXT (KEY, INFOTEXT)
  SELECT AZB_KEY, AZB_INFOTEXT FROM AZUBIPROJEKTEBEREICHE;
commit;

alter table AZUBIPROJEKTEBEREICHE drop COLUMN AZB_INFOTEXT;
commit;

alter table AZUBIPROJEKTEBEREICHE add AZB_INFOTEXT CLOB;
commit;

update AZUBIPROJEKTEBEREICHE a set azb_infotext = (select INFOTEXT from ZZZ_Meyer_AZUBIPROJEKTTEXT b where a.AZB_KEY = b.KEY);
commit;

drop TABLE ZZZ_Meyer_AZUBIPROJEKTTEXT;
commit;

INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('AZB_HTMLERKLAERUNG','AZB','Folgende Textbearbeitungs-Tags werden unterstützt: <br>
<br>
<b>&lt;br&gt;</b> Zeilenumbruch<br>
<b>&lt;b&gt; Text &lt;/b&gt;</b> Text in der Mitte wird fett geschrieben','09.08.2018');
commit;

