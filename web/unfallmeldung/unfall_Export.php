<?php

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();

$Recht21080 = $AWISBenutzer->HatDasRecht(21080);

if ($Recht21080 == 0) {
    $Form->Fehler_KeineRechte();
}

$TextKonserven = array();
$TextKonserven[] = array('UNF', 'UNF_EXPORT_INFO');
$TextKonserven[] = array('Wort', 'lbl_export');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Form->SchreibeHTMLCode('<form name=frmUnfallExport action="./unfall_Main.php?cmdAktion=Export" method="POST" enctype="multipart/form-data">');

$Form->Hinweistext($AWISSprachKonserven['UNF']['UNF_EXPORT_INFO'],6, 'width: auto');

$Form->SchaltflaechenStart();
// Zur�ck zum Men�
$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
$Form->Schaltflaeche('image', 'cmdSenden', '', '/bilder/cmd_export_xls.png', $AWISSprachKonserven['Wort']['lbl_export'], 'Z');

$Form->SchaltflaechenEnde();

$Form->SchreibeHTMLCode('</form>');