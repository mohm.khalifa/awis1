<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FelderNew;
global $FelderDel;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BEW','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht39002 = $AWISBenutzer->HatDasRecht(39002);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_HRT'));
	
	$Fehler = 0;
	$Save = 0;
	$FehlerFest = false;
	$NeuAnlage = 0;	
	
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1); 

	if(isset($_GET['aktiv'])) {

		$SQL = "UPDATE hrtaetigkeiten SET hrt_aktiv =" . $DB->WertSetzen('HRT', 'N0', $_GET['aktiv']);
		$SQL .= " WHERE hrt_key = " . $DB->WertSetzen('HRT', 'N0', $_GET['button']);
		$DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('HRT', true));
	}

	if(isset($_GET['ausbild'])) {

		$SQL = "UPDATE hrtaetigkeiten SET hrt_ausbild =" . $DB->WertSetzen('HRT', 'N0', $_GET['ausbild']);
		$SQL .= " WHERE hrt_key = " . $DB->WertSetzen('HRT', 'N0', $_GET['button']);
		$DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('HRT', true));
	}

	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'HRT_SUCH';
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht39002==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_GET['HRT_KEY']))
	{
		$AWIS_KEY1 = $_GET['HRT_KEY'];
	}
	if(isset($_POST['txtHRT_KEY']) and !isset($_POST['cmdSpeichern_x']) or (isset($_POST['cmdHinzufuegen_x'])))
	{
		$AWIS_KEY1 = $_POST['txtHRT_KEY'];
	}
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{	
		$Param['HRT_FIRMIERUNG'] = 	$Form->Format('N0',$_POST['sucBEW_FIRMIERUNG'],true);
		$Param['HRT_TAETIGKEITEN']= $Form->Format('N0',$_POST['sucBEW_TAETIGKEITEN'],true);
		$Param['HRT_AKTIV']= $Form->Format('N0',$_POST['sucBEW_AKTIV'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./taetigkeiten_speichern.php');
		
		if($Fehler == 0)
		{
			if($NeuAnlage == 0)
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['BEW']['MSG_SPEICHERN']);
				$Form->ZeileEnde();
			}
			else 
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['BEW']['MSG_NEUANLAGE']);
				$Form->ZeileEnde();
			}
		}
		else 
		{
			if($Fehler == 2)
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['BEW']['MSG_NEUANLAGEEMPTY']);
				$Form->ZeileEnde();
			}
		}
	}
	elseif($FelderDel != '' or isset($_POST['cmdLoeschenOK']))
	{
		include('./taetigkeiten_loeschen.php');
	}
	if($FelderNew != '')
	{
		include('./taetigkeiten_speichern.php');
	}

	/** Test-SQL Eskalationen **/
	
	$SQL = ' select HRT_KEY,HRT_SUCH,HRT_AKTIV,HRT_AUSBILD,HRT_USER,HRT_USERDAT,FIRM,row_number() OVER ( ';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	$SQL .= 'select distinct HRT_KEY,HRT_SUCH,HRT_AKTIV,HRT_AUSBILD,HRT_USER,HRT_USERDAT,FIRM FROM(';
	$SQL .= ' select HRT_KEY,HRT_SUCH,HRT_AKTIV,HRT_AUSBILD,HRT_USER,HRT_USERDAT,(';
    $SQL .= ' select listagg((select hrf_firmierung from hrfirmierung where hrf_key = htz_firmierung and hrf_aktiv = 1),\',\') within group (order by htz_firmierung) "list" ';
  	$SQL .=	' from hrtaetzuord where htz_hrt_key = hrt_key) as FIRM ';
	$SQL .=	' from hrtaetigkeiten ';
	$SQL .= ' left join hrtaetzuord on hrt_key = htz_hrt_key ';

	
	

	
	
	if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '')
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		//if ($Bedingung != '' and (!isset($_POST['cmdHinzufuegen_x'])) and (!isset($_POST['cmdDSNeu_x'])) and !isset($_GET['HRT_KEY']) and $NeuAnlage != 1 and !isset($_POST['txtHRT_KEY']))
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}

	if($AWIS_KEY1 != '')
	{
		if($NeuAnlage == 1 or isset($_GET['HRT_KEY']) or $AWIS_KEY1 <> '')
		{
			$SQL .= ' WHERE hrt_key='.$AWIS_KEY1;
		}
		else 
		{
			$SQL .= ' AND hrt_key='.$AWIS_KEY1;
		}
		
	
	}
	
	$SQL .= '))';
	
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	
	if(($NeuAnlage != 1 and !isset($_GET['HRT_KEY']) and $FelderNew == false) or $Fehler == 1)
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsHRT = $DB->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['Bezeichnung'] = 600;
	$FeldBreiten['Firmierung'] = 400;
	$FeldBreiten['Aktiv'] = 60;
	$FeldBreiten['Ausbildung'] = 135;
	
	$Form->SchreibeHTMLCode('<form name="frmHRTDetails" action="./taetigkeiten_Main.php?cmdAktion=Stammdaten" method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	if (($rsHRT->AnzahlDatensaetze() > 1))
	{
		$Form->ZeileStart();
		$Link = './taetigkeiten_Main.php?cmdAktion=Stammdaten&Sort=HRT_SUCH'.((isset($_GET['Sort']) AND ($_GET['Sort']=='HRT_SUCH'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['HRT_BEZEICHNUNG'], $FeldBreiten['Bezeichnung'], '', $Link);

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['HRT_FIRMIERUNG'], $FeldBreiten['Firmierung'], '','');

		$Link = './taetigkeiten_Main.php?cmdAktion=Stammdaten&Sort=HRT_AKTIV'.((isset($_GET['Sort']) AND ($_GET['Sort']=='HRT_AKTIV'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['HRT_AKTIVSUCH'], $FeldBreiten['Aktiv'], '', $Link,$AWISSprachKonserven['BEW']['ttt_UEBAKTIV']);

		$Link = './taetigkeiten_Main.php?cmdAktion=Stammdaten&Sort=HRT_AUSBILD'.((isset($_GET['Sort']) AND ($_GET['Sort']=='HRT_AUSBILD'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['HRT_AUSBILDUNG'], $FeldBreiten['Ausbildung'], '', $Link);

		$Form->ZeileEnde();
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsHRT->EOF())
		{
			$Form->ZeileStart('','Zeile"ID="Zeile_'.$rsHRT->FeldInhalt('HRT_KEY'));
			$Link = './taetigkeiten_Main.php?cmdAktion=Stammdaten&HRT_KEY=0'.$rsHRT->FeldInhalt('HRT_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $rsHRT->FeldInhalt('HRT_SUCH');
			$Form->Erstelle_ListenFeld('HRT_SUCH', $rsHRT->FeldInhalt('HRT_SUCH'), 0, $FeldBreiten['Bezeichnung'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
			$TTT =  $rsHRT->FeldInhalt('FIRM');
			$Form->Erstelle_ListenFeld('HRT_FIRMIERUNG',$rsHRT->FeldInhalt('FIRM'), 0, $FeldBreiten['Firmierung'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$LinkAktiv = '';
			$LinkAzub = '';
			if (($Recht39002&2) == 2)
			{
				if ($rsHRT->FeldInhalt('HRT_AKTIV') == '1')
				{
					$LinkAktiv .=  './taetigkeiten_Main.php?cmdAktion=Stammdaten&button='.$rsHRT->FeldInhalt('HRT_KEY').'&aktiv=0';
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BEW']['ttt_Aktiv']);
				}
				else
				{
					$LinkAktiv .=  './taetigkeiten_Main.php?cmdAktion=Stammdaten&button='.$rsHRT->FeldInhalt('HRT_KEY').'&aktiv=1';
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BEW']['ttt_Inaktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['Aktiv'] , ($DS%2));

				$IconsArray = '';

				if ($rsHRT->FeldInhalt('HRT_AUSBILD') == '1') {
					$LinkAzub .= './taetigkeiten_Main.php?cmdAktion=Stammdaten&button='.$rsHRT->FeldInhalt('HRT_KEY').'&ausbild=0';
					$IconsArray[] = array("flagge_gruen", $LinkAzub, "", $AWISSprachKonserven['BEW']['ttt_Ausbildung']);//$AWISSprachKonserven['BEW']['ttt_Aktiv']);
				}
				else
				{
					$LinkAzub .= './taetigkeiten_Main.php?cmdAktion=Stammdaten&button='.$rsHRT->FeldInhalt('HRT_KEY').'&ausbild=1';
					$IconsArray[] = array("flagge_rot", $LinkAzub, "", $AWISSprachKonserven['BEW']['ttt_keineAusbildung']);//$AWISSprachKonserven['BEW']['ttt_Inaktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['Ausbildung'] , ($DS%2));
				$IconsArray = '';
			}


			$Form->ZeileEnde();
			$DS++;
			$rsHRT->DSWeiter();
		}
		$Link = './taetigkeiten_Main.php?cmdAktion=Stammdaten';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	elseif(($rsHRT->AnzahlDatensaetze() == 1) or $AWIS_KEY1 == '-1')
	{
		if($AWIS_KEY1 == '-1')
		{
			// Infozeile zusammenbauen
			$Felder = array();
			//$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
			$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href="./taetigkeiten_Main.php?cmdAktion=Suche" accesskey=S title=' . $AWISSprachKonserven['BEW']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
			$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['BEW']['ZurueckSuche']);
			$Form->InfoZeile($Felder, '');
		}
		else
		{
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./taetigkeiten_Main.php?cmdAktion=Stammdaten accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHRT->FeldInhalt('HRT_USER'));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHRT->FeldInhalt('HRT_USERDAT'));
			$Form->InfoZeile($Felder,'');
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['HRT_BEZEICHNUNG'] . ':',140);
		$Form->Erstelle_TextFeld('HRT_BEZEICHNUNG',(isset($_REQUEST['txtHRT_BEZEICHNUNG']) and !isset($_POST['cmdSpeichern_x']))?$_REQUEST['txtHRT_BEZEICHNUNG']:$rsHRT->FeldInhalt('HRT_SUCH'), 65,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
			
		if ($AWIS_KEY1 != '-1')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['HRT_BEZEICHNUNG'] . ':',140);
			$Form->Erstelle_Checkbox('CHK_AKTIV',$FelderNew == '' ? (isset($_REQUEST['txtCHK_AKTIV']) and !isset($_POST['cmdSpeichern_x'])) ?$_REQUEST['txtCHK_AKTIV']:$rsHRT->FeldInhalt('HRT_AKTIV') : (isset($_REQUEST['txtCHK_AKTIV'])  and !isset($_POST['cmdSpeichern_x'])) ? 1 : 0,40,true,1);
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['HRT_AUSBILDUNG'] . ':',140);
			$Form->Erstelle_Checkbox('CHK_AUSBILD',$FelderNew == '' ? (isset($_REQUEST['txtCHK_AUSBILD']) and !isset($_POST['cmdSpeichern_x'])) ?$_REQUEST['txtCHK_AUSBILD']:$rsHRT->FeldInhalt('HRT_AUSBILD') : (isset($_REQUEST['txtCHK_AUSBILD'])  and !isset($_POST['cmdSpeichern_x'])) ? 1 : 0,40,true,1);
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Trennzeile('L');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			$FeldBreiten = array();
			$FeldBreiten['Blank'] = 20;
			$FeldBreiten['Firmierung'] = 530;
				
			
			if($AWIS_KEY1 == '')
			{
				$AWIS_KEY1 = $rsHRT->FeldInhalt('HRT_KEY');
			}
			
			$SQL ='select htz_key,hrf_key,hrf_firmierung as FIRM from hrtaetzuord ';
			$SQL .= 'inner join hrfirmierung on hrf_key = htz_firmierung and hrf_aktiv = 1 ';
			$SQL .=' where htz_hrt_key ='.$AWIS_KEY1;
			$SQL .=' order by firm';
				
			$rsZuord = $DB->RecordSetOeffnen($SQL);	
			
			
			
			$Form->ZeileStart();
			// �berschrift der Listenansicht mit Sortierungslink: Filiale
			//$Form->Schaltflaeche('image', 'cmdHinzufuegen','', '/bilder/icon_add.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'W','',13,13);
			$SQL2 ='select hrf_key,hrf_firmierung from hrfirmierung ';
			$SQL2 .=' left join hrtaetzuord on htz_firmierung = hrf_key';
			$SQL2 .=' and htz_hrt_key ='.$AWIS_KEY1;
			$SQL2 .=' where htz_key is null';
			$SQL2 .=' and hrf_aktiv = 1';
			$SQL2 .=' order by hrf_firmierung';
				
			$rsList = $DB->RecordSetOeffnen($SQL2);
			
	
			$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
			//$Link = './taetigkeiten_Main.php?cmdAktion=Stammdaten&HRT_KEY='.$AWIS_KEY1.'&Sort2=FIRM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIRM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['HRT_FIRMIERUNG'], $FeldBreiten['Firmierung'],'', '');
			$Form->ZeileEnde();
			$DS = 0;	// f�r Hintergrundfarbumschaltung
				
			$Form->Erstelle_HiddenFeld('Seite','Taetigkeiten');
			
			while(! $rsZuord->EOF())
			{
				$Form->ZeileStart();
				$Icons = array();
				//$Icons[] = array('delete','"./taetigkeiten_Main.php?cmdAktion=Stammdaten&Del='.$rsZuord->FeldInhalt('HTZ_KEY').'&HRT_KEY='.$AWIS_KEY1.'" onmouseover="urlanpassen(this);"');
				$Icons[] = array('delete','POST~'.$rsZuord->FeldInhalt('HTZ_KEY'));
				$Form->Erstelle_ListeIcons($Icons,20,($DS%2));
				$TTT = $rsZuord->FeldInhalt('FIRM');
				$Form->Erstelle_ListenFeld('HRT_FIRMIERUNG',$rsZuord->FeldInhalt('FIRM'), 0, $FeldBreiten['Firmierung'], false, ($DS%2),'','', 'T', 'L', $TTT);
				$Form->ZeileEnde();
				$DS++;
				$rsZuord->DSWeiter();
			}	
			
			if($rsList->AnzahlDatensaetze() > 0)
			{
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array('add','POST~'.$AWIS_KEY1);
				$Form->Erstelle_ListeIcons($Icons,20,($DS%2));
				$Form->Erstelle_SelectFeld('HRT_FIRM',isset($_REQUEST['txtHRT_FIRM'])?$_REQUEST['txtHRT_FIRM']:'0','300:535',true,$SQL2,'','','','','','','',array(),'font-size:14px;height:22px');
				$Form->ZeileEnde();
			}
		}
		
		
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
	$Form->Erstelle_HiddenFeld('HRT_KEY',$AWIS_KEY1);
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht39002&4)==4 and ($AWIS_KEY1 != '-1' and $rsHRT->AnzahlDatensaetze() != 1))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht39002&2)==2 and ($AWIS_KEY1 == '-1' or $rsHRT->AnzahlDatensaetze() == 1))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_HRT',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	if(isset($Param['HRT_TAETIGKEITEN']) AND $Param['HRT_TAETIGKEITEN']!='')
	{
		$Bedingung .= 'AND HRT_KEY ' . $DB->LikeOderIst($Param['HRT_TAETIGKEITEN']) . ' ';
	}
	if(isset($Param['HRT_AKTIV']) AND $Param['HRT_AKTIV']!='')
	{
		$Bedingung .= 'AND HRT_AKTIV ' . $DB->LikeOderIst($Param['HRT_AKTIV']) . ' ';
	}
	if(isset($Param['HRT_FIRMIERUNG']) AND $Param['HRT_FIRMIERUNG']!='')
	{
		if($Param['HRT_FIRMIERUNG'] != '-1')
		{
			$Bedingung .= 'AND HTZ_FIRMIERUNG ' . $DB->LikeOderIst($Param['HRT_FIRMIERUNG']) . ' ';
		}
		else 
		{
			$Bedingung .= 'AND HTZ_FIRMIERUNG is null';
		}
	}	
	return $Bedingung;
}

?>