<?
global $CursorFeld;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$_SERVER['PHP_AUTH_USER']);
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('MBW','txt_SucheName');
$TextKonserven[]=array('MBW','lbl_SucheName');
$TextKonserven[]=array('MBW','txt_SucheOrt');
$TextKonserven[]=array('MBW','txt_SucheFiliale');
$TextKonserven[]=array('MBW','txt_SuchePLZ');
$TextKonserven[]=array('MBW','txt_Entfernung');
$TextKonserven[]=array('MBW','txt_SucheStrasse');
$TextKonserven[]=array('MBW','ttt_Entfernung');
$TextKonserven[]=array('MBW','MBW_%');
$TextKonserven[]=array('MBH','MBH_HER_ID');
$TextKonserven[]=array('MPG','MPG_MGT_ID');
$TextKonserven[]=array('MIU','MIU_MUT_KEY');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht3010=awisBenutzerRecht($con,3010);

echo "<br>";

echo "<form name=frmSuche method=post action=./mitbewerber_Main.php?cmdAktion=Details>";


	// Alle Parameter auslesen
	//	Es wird aber nicht alles eingeblendet!
$TelSuche = explode(";",awis_BenutzerParameter($con, "MitbewerberSuche", $_SERVER['PHP_AUTH_USER']));

/**********************************************
* * Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['txt_SucheName'].':',150);
awis_FORM_Erstelle_TextFeld('*SuchName','',20,200,true);
$CursorFeld='sucSuchName';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['txt_SuchePLZ'].':',150);
awis_FORM_Erstelle_TextFeld('*SuchPLZ','',10,0,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['txt_SucheOrt'].':',150);
awis_FORM_Erstelle_TextFeld('*SuchOrt','',10,200,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['txt_SucheFiliale'].':',150);
awis_FORM_Erstelle_TextFeld('*SuchFiliale','',20,200,true,'');
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['txt_Entfernung'].':',150);
awis_FORM_Erstelle_TextFeld('*Entfernung','',10,200,true,'','','','T','L',$TXT_TextBaustein['MBW']['ttt_Entfernung']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['txt_SucheStrasse'].':',150);
awis_FORM_Erstelle_TextFeld('*SucheStrasse','',30,280,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBH']['MBH_HER_ID'],150,'');
$SQL = 'SELECT HER_ID,HER_BEZEICHNUNG FROM Hersteller WHERE BITAND(HER_VERWENDUNG,1)=1 ';
$SQL .= ' ORDER BY HER_BEZEICHNUNG';
awis_FORM_Erstelle_SelectFeld('*HerstellerID','',300,true,$con,$SQL,'0~'.$TXT_TextBaustein['Wort']['txt_BitteWaehlen'],'','HER_BEZEICHNUNG','','','');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MIU']['MIU_MUT_KEY'],150,'');
$SQL = 'SELECT MUT_KEY,MUT_Unternehmenstyp FROM Mitbewerbutypentypen ';
$SQL .= ' ORDER BY MUT_Unternehmenstyp';
awis_FORM_Erstelle_SelectFeld('*UTyp','',300,true,$con,$SQL,'0~'.$TXT_TextBaustein['Wort']['txt_BitteWaehlen'],'','MUT_UNTERNEHMENSTYP','','','');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MPG']['MPG_MGT_ID'],150,'');
$SQL = 'SELECT MGT_ID,MGT_GRUPPE FROM Mitbewerbprodgruppentypen ';
$SQL .= ' ORDER BY MGT_GRUPPE';
awis_FORM_Erstelle_SelectFeld('*Produkte','',300,true,$con,$SQL,'0~'.$TXT_TextBaustein['Wort']['txt_BitteWaehlen'],'','MGT_GRUPPE','','','');
awis_FORM_ZeileEnde();


awis_FORM_FormularEnde();

echo "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png name=cmdSuche value=\"Aktualisieren\">";
if(($Recht3010&4) == 4)		// Hinzufügen erlaubt?
{
	echo "&nbsp;<input type=image name=cmdDSNeu accesskey=n title='".$TXT_TextBaustein['Wort']['lbl_hinzufuegen']."' src=/bilder/plus.png name=cmdDSNeu>";// onclick=location.href='./mitbewerber_Main.php?cmdAktion=Liste&txtADRKey=-1&DSNeu=True'>";
}

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}

?>

