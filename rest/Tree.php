
<?php
try{
    require_once 'awisDITTreeAPI.php';
    require_once 'awisDatenbank.inc';
    require_once 'awisRest.inc';
    $DB = awisDatenbank::NeueVerbindung('AWIS_PROD');
    $DB->Oeffnen();
    $awisREST = new awisRest('DistributionStructureAPI');
    $awisREST->verarbeiteRequest();

    $Request = $awisREST->getRequest();

    $DITTree = new awisDITTreeAPI();


    $awisREST->sendeResponse(array($DITTree->getTree()),awisRest::AntwortTyp_JSON);


}catch (Exception $e){
    echo $e->getMessage();
}



