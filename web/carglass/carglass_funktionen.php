<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

class carglass_funktionen
{
    public $Form;
    public $DB;
    public $SQL;
    public $AWISBenutzer;
    public $Recht75000;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeuge;
    public $UserAgent;
    private $FILNR;
    private $BoolFiliale;
    private $Script;
    private $ScriptFunktion;
    private $Link;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular(3);
        $this->Recht75000 = $this->AWISBenutzer->HatDasRecht(75000);
        $this->AWISWerkzeuge = new awisWerkzeuge();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('CAG', '%');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Land');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_CAG');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht75000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'CAG'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }


    /*
     * Pr�ft ob der aktuelle Browser IE ist und gibt einen boolean zur�ck
     *
     * */
    public function CheckIEBrowser (){

        $this->UserAgent = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');

        if (preg_match('~MSIE|Internet Explorer~i', $this->UserAgent) || (strpos($this->UserAgent, 'Trident/7.0') !== false && strpos($this->UserAgent, 'rv:11.0') !== false)) {
            //INTERNET EXPLORER

            return true;

        }
        else{
            //anderer Browser

            return false;

        }
    }


    /*
     * Insert in Tabelle CarglassKooperation
     */
    public function InsertCarglassKopf(){

        $this->FILNR=$this->FilialNummer();

        //Insert SQL
        $this->SQL = 'INSERT INTO CARGLASSKOOPERATION (';
        $this->SQL .= 'CAG_FILNR, CAG_KLICK, CAG_USER, CAG_USERDAT) ';
        $this->SQL .= 'VALUES ';
        $this->SQL .= '('. $this->DB->WertSetzen('CAG', 'T', $this->FILNR) . ', ';
        $this->SQL .= 'SYSDATE, ';
        $this->SQL .= ''. $this->DB->WertSetzen('CAG', 'T', $this->AWISBenutzer->BenutzerName()) . ', ';
        $this->SQL .= 'SYSDATE ';
        $this->SQL .= ')';

        $this->DB->Ausfuehren($this->SQL, 'CAG', true, $this->DB->Bindevariablen('CAG'));
    }

    /*
     * Weiterleitung zu Carglass nach x sec
     */
    public function Weiterleitung()
    {
        $this->Link=$this->AWISBenutzer->ParameterLesen('Carglass Link');
        $this->FILNR=$this->FilialNummer();
        $this->Link=$this->Link.$this->FILNR;

        //10000 = 10 sec, 5000 = 5 sec
        $this->Script = "<script>".PHP_EOL;
        $this->ScriptFunktion = "function callback() {".PHP_EOL;
        $this->ScriptFunktion .= "window.location.replace(\"".$this->Link."\");".PHP_EOL;
        $this->ScriptFunktion .= "}".PHP_EOL;
        $this->Script .= $this->ScriptFunktion;
        $this->Script .= "window.setTimeout(callback, 5000);".PHP_EOL;
        $this->Script .= "</script>";

        echo $this->Script;
    }


    /*
     * Filialnummer �bergeben, Unterscheidung ob Filiale oder nicht.
     * Wenn Filiale, �bergabe der Filialnummer
     * Anderer User, Standard Filiale wird �bergeben z.B. 999
     */
    public function FilialNummer()
    {
        //Pr�fen, ob der angemeldete Benutzer eine Filiale ist
        $this->BoolFiliale=$this->AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_BOOLEAN);

        if ($this->BoolFiliale==false)
        {
            $this->FILNR=$this->AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
            $this->FILNR=ltrim($this->FILNR, '0');

            return $this->FILNR;

        }else{
            //KEINE Filialnummer hinterlegt

            return $this->FILNR='999';

        }
    }

}