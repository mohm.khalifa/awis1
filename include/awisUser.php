<?php
require_once('db.inc.php');

class awisUser
{
	/**
	 * Benutzername
	 *
	 * @var string
	 */
	private $_UserName = '';
	
	/**
	 * Dom�ne (Windows), in der Benutzer ist
	 *
	 * @var string
	 */
	private $_UserDomain = '';
	
	/**
	 * Interne BenutzerID (XBN_KEY)
	 *
	 * @var int
	 */
	var $_UserID = 0;
	
	
	/**
	 * Initialisierung der Klasse
	 *
	 * @param string $LoginName
	 */
	public function __construct($LoginName='')
	{
		if($LoginName==='')
		{
			if(isset($_SERVER["REMOTE_USER"]))
			{
				$LoginName = strtolower($_SERVER["REMOTE_USER"]);
			}
			elseif(isset($_SERVER['PHP_AUTH_USER']))
			{
				$LoginName = strtolower($_SERVER["PHP_AUTH_USER"]);
			}
		}
		
		if(strpos($LoginName,'@')===false)		// Alte Anmeldung
		{
			$this->_UserName = $LoginName;	
			$this->_UserDomain = '';
		}
		else 
		{
			$LoginName = explode('@',$LoginName);
			$this->_UserName = $LoginName[0];
			$this->_UserDomain = $LoginName[1];
		}
	}
	
	/**
	 * Liefert den aktuellen Benutzernamen
	 *
	 * @param int $Format (1=Login, 2=Login@Dom�ne)
	 * @return string
	 */
	public function BenutzerName($Format = 1)
	{
		if($Format==1)
		{
			return $this->_UserName;
		}
		
		return $this->_UserName . '@' . $this->_UserDomain;
	}
	
	/**
	 * Funktion liefert den Kontakt-KEY des aktuellen Benutzers
	 *
	 * @return int
	 */
	public function BenutzerKontaktKEY()
	{
		$_con = awisLogon();
		
//		$SQL = 'SELECT XBN_KON_KEY';
//		$SQL .= ' FROM Benutzer ';
//		$SQL .= ' INNER JOIN BenutzerLogins ON XBN_KEY = XBL_XBN_KEY';
////		$SQL .= ' WHERE UPPER(XBL_LOGIN)=\''.strtoupper($this->BenutzerName()).'\'';
//		$SQL .= ' WHERE UPPER(XBL_LOGIN)=:var_T_XBL_LOGIN';

		$SQL = 'SELECT XBN_KON_KEY';
		$SQL .= ' FROM MV_BENUTZER_LOGIN ';
		$SQL .= ' WHERE UPPER(XBL_LOGIN)=:var_T_XBL_LOGIN';

		$BindeVariablen = array();
		$BindeVariablen['var_T_XBL_LOGIN'] = strtoupper($this->BenutzerName());
		
		$rsXBN = awisOpenRecordset($_con, $SQL,false,false,$BindeVariablen);
		awisLogoff($_con);
		
		if(isset($rsXBN['XBN_KON_KEY'][0]))
		{
			return $rsXBN['XBN_KON_KEY'][0];
		}
		
		return 0;
	}
	/**
	 * Funktion liefert den eindeutigen Benutzer-Key
	 * Tritt ein Fehler auf, wird 0 geliefert
	 * 
	 * @return int
	 */
	public function BenutzerID()
	{
		$_con = awisLogon();
		
//		$SQL = 'SELECT XBN_KEY';
//		$SQL .= ' FROM Benutzer ';
//		$SQL .= ' INNER JOIN BenutzerLogins ON XBN_KEY = XBL_XBN_KEY';
////	$SQL .= ' WHERE UPPER(XBL_LOGIN)=\''.strtoupper($this->BenutzerName()).'\'';
//		$SQL .= ' WHERE UPPER(XBL_LOGIN)=:var_T_XBL_LOGIN';

		$SQL = 'SELECT XBN_KEY';
		$SQL .= ' FROM MV_BENUTZER_LOGIN ';
		$SQL .= ' WHERE UPPER(XBL_LOGIN)=:var_T_XBL_LOGIN';
		
		$BindeVariablen = array();
		$BindeVariablen['var_T_XBL_LOGIN'] = strtoupper($this->BenutzerName());
		
		$rsXBN = awisOpenRecordset($_con, $SQL,false,false,$BindeVariablen);
		
		awisLogoff($_con);
		
		if(isset($rsXBN['XBN_KEY'][0]))
		{
			return $rsXBN['XBN_KEY'][0];
		}
		
		return 0;
	}
	
	/**
	 * FilialZugriff
	 * Liefert true zurueck, wenn der Benutzer keinen Zugriff auf eine Filiale hat
	 * 
	 * @param int $Filiale - Filialid
	 * @param resource $con - Datenbankverbindung
	 */
	public function FilialZugriff($Filiale, $con=false)
	{
		if($con===false)
		{
			$con = awisLogon();		// Macht eine eigene DB-Verbindung auf
		}
//		$SQL = 'SELECT XBN_FILIALEN FROM Benutzer ';
//		$SQL .= ' INNER JOIN BenutzerLogins ON XBN_KEY = XBL_XBN_KEY ';
////	$SQL .= ' WHERE UPPER(XBL_LOGIN) = UPPER(\''.$this->_UserName.'\')';
//		$SQL .= ' WHERE UPPER(XBL_LOGIN)=:var_T_XBL_LOGIN';
		
		$SQL = 'SELECT XBN_FILIALEN FROM MV_BENUTZER_LOGIN ';
		$SQL .= ' WHERE UPPER(XBL_LOGIN)=:var_T_XBL_LOGIN';
		
		$BindeVariablen = array();
		$BindeVariablen['var_T_XBL_LOGIN'] = strtoupper($this->BenutzerName());
		
		$rsXBN = awisOpenRecordset($con, $SQL,false,false,$BindeVariablen);
		
		if($rsXBN['XBN_FILIALEN'][0]=='' OR $rsXBN['XBN_FILIALEN'][0]=='0')
		{
			return false;
		}
		else
		{
			$Liste = explode(',',$rsXBN['XBN_FILIALEN'][0]);
			foreach($Liste AS $FilId)
			{
				if($FilId == $Filiale)
				{
					return true;
				}
			}
		}
	
		return false;
	}
}
?>