<?php
global $con;
global $RechteStufe;
global $awisRSZeilen;
global $awisDBFehler;


if($RechteStufe==0)
{
	awisLogoff($con);
	die();
}

$comcon = awisLogonComCluster('DE',false);

if(isset($_GET['Freigabe']) && $_GET['Freigabe']=='1')
{
	$BindeVariablen=array();
	$BindeVariablen['var_N0_id_fplb']=$_GET['FPLB'];
	$BindeVariablen['var_N0_pts_pst_id']=intval(40);
	
	$SQL = 'SELECT BEZEICHNUNG, PST_ID, PST_STATUS, PTS_USERDAT,';
	$SQL .= ' NVL(PTS_TRANSAKTIONSID,0) AS PTS_TRANSAKTIONSID, NVL(PTS_ID_FPLB,0) AS PTS_ID_FPLB, HERSTELLERPREISLISTE,';
	$SQL .= ' NVL(PTS_ZUSATZ_TYP,0) AS PTS_ZUSATZ_TYP, NVL(PTS_ZUSATZ_TID,0) AS PTS_ZUSATZ_TID';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS ';
	$SQL .= ' LEFT OUTER JOIN EXPGW.EXPGW_EXPORT ON PTS_TRANSAKTIONSID=TRANSAKTIONSID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTS_PST_ID = PST_ID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB ON ID_FPLB = PTS_ID_FPLB';
	$SQL .= ' WHERE PTS_ID_FPLB=:var_N0_id_fplb AND PTS_PST_ID=:var_N0_pts_pst_id';
	$SQL .= ' ORDER BY  PTS_TRANSAKTIONSID DESC';

	$rsPLI = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
	$rsPLIZeilen = $awisRSZeilen;
	
	if($rsPLIZeilen>0)
	{
		$TID=$rsPLI['PTS_TRANSAKTIONSID'][0];
		$PREISLISTE=$rsPLI['PTS_ID_FPLB'][0];
		$PTS_ZUSATZ_TYP=$rsPLI['PTS_ZUSATZ_TYP'][0];
		$PTS_ZUSATZ_TID=$rsPLI['PTS_ZUSATZ_TID'][0];
	
		
		$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_CREA_FREIGABE_JOB('.$TID.','.$PREISLISTE.','.$PTS_ZUSATZ_TYP.','.$PTS_ZUSATZ_TID.'); END;';
	
		if(awisExecute($comcon, $SQL)===false)
		{
			awisErrorMailLink('08091334-preislisten_Uebersicht.php',3,$awisDBFehler['message']);
			die();
		}
	}
}

if(($RechteStufe&8)==8)		// Rechte f�r ALLE Preislisten?
{
	$SQL = 'SELECT *';
	$SQL .= ' FROM EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB WHERE STATUS=1';
	$SQL .= ' ORDER BY BEZEICHNUNG';
}
else						// Einzelne Preislisten
{
	$SQL = 'SELECT *';
	$SQL .= ' FROM EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB';
	$SQL .= ' INNER JOIN  EXPERIAN_ATU.PLP_PreisListenBenutzer ON ID_FPLB=PUL_ID_FPLB AND PUL_XBN_KEY='.awisBenutzerID();
	$SQL .= '  WHERE STATUS=1';
	$SQL .= ' ORDER BY BEZEICHNUNG';
}

$rsPLI = awisOpenRecordset($comcon,$SQL);
$rsPLIZeilen = $awisRSZeilen;
//awis_Debug(1,$rsPLI);

echo '<form name=frmPreislisten action=./preislisten_Main.php?cmdAktion=Details method=POST>';

echo '<table class=Datentabelle border=1 width=100%>';

echo '<tr><td class=FeldBez>ID</td>';
echo '<td class=FeldBez>Bezeichnung</td>';
echo '<td class=FeldBez>Typ</td>';
echo '<td class=FeldBez>Status</td>';
echo '<td class=FeldBez>Status�nderung</td>';
echo '<td class=FeldBez>G�ltig vom</td>';
echo '<td class=FeldBez>G�ltig bis</td>';
echo '<td class=FeldBez>Transaktion</td>';
echo '<td class=FeldBez>Aktion</td>';

echo '</tr>';
for($Zeile=0;$Zeile<$rsPLIZeilen;$Zeile++)
{

	echo '<tr>';

	echo '<td id=TabellenZeileWeiss>'. $rsPLI['ID_FPLB'][$Zeile] . '</td>';
	echo '<td id=TabellenZeileWeiss><b>'. $rsPLI['BEZEICHNUNG'][$Zeile] . '</b></td>';


	// Zeit anzeigen!
	awisExecute($comcon, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'");

	//**************************************
	// Zu �bertragende Version
	//**************************************
	echo '<td id=TabellenZeileWeiss title="Status der neu einzuspielenden Liste">NEU</td>';

	$BindeVariablen=array();
	$BindeVariablen['var_N0_id_fplb']=$rsPLI['ID_FPLB'][$Zeile];
	
	$SQL = 'SELECT PST_ID, PST_STATUS, PTS_USERDAT, PTS_TRANSAKTIONSID, PST_BEMERKUNG, ARBEITSSTATUS, PTS_ID_FPLB, PTS_BEMERKUNG';
	$SQL .= ' ,(SELECT /*+ INDEX(PLP_FESTPREISLISTEN IDX_PLT_TRANSID) */ MAX(PLT_GUELTIG_VON) FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_TRANSAKTIONSID=PLP.PTS_TRANSAKTIONSID) AS GueltigVon';
	$SQL .= ' ,(SELECT /*+ INDEX(PLP_FESTPREISLISTEN IDX_PLT_TRANSID) */ MAX(PLT_GUELTIG_BIS) FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_TRANSAKTIONSID=PLP.PTS_TRANSAKTIONSID) AS GueltigBis';
	$SQL .= ', PTS_ZUSATZ_TID';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS PLP';
	$SQL .= ' LEFT OUTER JOIN EXPGW.EXPGW_EXPORT ON PTS_TRANSAKTIONSID=TRANSAKTIONSID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTS_PST_ID = PST_ID';
	$SQL .= ' AND PTS_ID_FPLB=:var_N0_id_fplb';
	$SQL .= " WHERE (PTS_PST_ID<>42 AND ARBEITSSTATUS<>'T')";
	$SQL .= ' ORDER BY  PTS_TRANSAKTIONSID DESC';
/*
	$SQL = "select * from(";
	$SQL .= " with aa as (select pst_id, pst_status, pts_userdat, pts_transaktionsid, pst_bemerkung,";
    $SQL .= " arbeitsstatus, pts_id_fplb, pts_bemerkung,pts_zusatz_tid";
    $SQL .= " from experian_atu.plp_transaktionsstatus pts,";
    $SQL .= " experian_atu.plp_statusarten pst,";
    $SQL .= " expgw.expgw_export ee";
    $SQL .= " where pts.pts_pst_id = pst.pst_id";
    $SQL .= " and pts.pts_transaktionsid = ee.transaktionsid";
    $SQL .= " and pts.pts_id_fplb = ".$rsPLI['ID_FPLB'][$Zeile];
    $SQL .= " and pts.pts_pst_id <> 42";
    $SQL .= " and ee.arbeitsstatus <> 'T'";
    $SQL .= " ),";
    $SQL .= " bb as (SELECT fpl.plt_transaktionsid, MAX (fpl.plt_gueltig_von) AS gueltigvon,MAX (fpl.plt_gueltig_bis)AS gueltigbis";
    $SQL .= " FROM experian_atu.plp_festpreislisten fpl , aa";
    $SQL .= " where fpl.plt_transaktionsid = aa.pts_transaktionsid";
    $SQL .= " group by fpl.plt_transaktionsid)";
	$SQL .= " SELECT   aa.*, bb.*";
    $SQL .= " FROM aa, bb";
    $SQL .= " where bb.plt_transaktionsid(+) = aa.pts_transaktionsid";
    $SQL .= " )";
	$SQL .= " ORDER BY pts_transaktionsid DESC";
*/
//awis_Debug(1,$SQL);
	$rsPTS = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
	
	echo '<td  id=TabellenZeileWeiss title=\''.(isset($rsPTS['PST_BEMERKUNG'][0])?$rsPTS['PST_BEMERKUNG'][0]:'').'\'>';
	//echo ($rsPTS['PST_STATUS'][0]==''?'::keine Daten geladen::':($rsPTS['PST_STATUS'][0]=='Offen'?'<span style="background-color:yellow;color:green;">Offen</span>':($rsPTS['PST_ID'][0]>=90?'<font color=red>':'').$rsPTS['PST_STATUS'][0].($rsPTS['PST_ID'][0]>=90?'</font>':''))) . '</td>';
	echo (isset($rsPTS['PST_STATUS'][0]) && $rsPTS['PST_STATUS'][0]==''?'::keine Daten geladen::':(isset($rsPTS['PST_STATUS'][0]) && $rsPTS['PST_STATUS'][0]=='Offen'?'<span style="background-color:yellow;color:green;">Offen</span>':(isset($rsPTS['PST_ID'][0]) && $rsPTS['PST_ID'][0]>=90?'<font color=red>':'').(isset($rsPTS['PST_STATUS'][0])?$rsPTS['PST_STATUS'][0]:'').(isset($rsPTS['PST_ID'][0]) && $rsPTS['PST_ID'][0]>=90?'</font>':''))) . '</td>';
	echo '<td id=TabellenZeileWeiss>' . (isset($rsPTS['PTS_USERDAT'][0])?$rsPTS['PTS_USERDAT'][0]:'') . '</td>';
	echo '<td id=TabellenZeileWeiss>' . (isset($rsPTS['GUELTIGVON'][0])?substr($rsPTS['GUELTIGVON'][0],0,10):'') . '</td>';
	echo '<td id=TabellenZeileWeiss>' . (isset($rsPTS['GUELTIGBIS'][0])?substr($rsPTS['GUELTIGBIS'][0],0,10):'') . '</td>';
	echo '<td id=TabellenZeileWeiss title=\''.(isset($rsPTS['PST_BEMERKUNG'][0])?$rsPTS['PST_BEMERKUNG'][0]:'').'\'>' . (isset($rsPTS['PTS_TRANSAKTIONSID'][0])?$rsPTS['PTS_TRANSAKTIONSID'][0]:'') . '</td>';

	echo '<td id=TabellenZeileWeiss>';
	switch (isset($rsPTS['PST_ID'][0])?$rsPTS['PST_ID'][0]:999)
	{
		case 0:			// Import l�uft
			echo '&nbsp;';
			break;
		case 1:			// Offen
		case 10:		// Pr�fung l�uft
		case 11:		// Pr�fung unvollst�ndig
			print "&nbsp;<img title='Pr�efung starten' src=/bilder/schraubenschluessel.png name=cmdPruefung accesskey=� onclick=window.open('./preislisten_PruefungDurchfuehren.php?TID=" . $rsPTS['PTS_TRANSAKTIONSID'][0] . "','Preislistenpr�fung','toolbar=no,menubar=no,dependent=yes,resizable=yes,status=no,scrollbars=yes,fullscreen=yes');>";
			break;
		case 15:		// Pr�fung abgeschlossen
			print "&nbsp;<input type=image title='Details untersuchen' src=/bilder/sc_suche2.png name=cmdDetails_" . $rsPLI['ID_FPLB'][$Zeile] . ">";
			print "&nbsp;<img title='Pr�efung starten' src=/bilder/schraubenschluessel.png name=cmdPruefung accesskey=� onclick=window.open('./preislisten_PruefungDurchfuehren.php?TID=" . $rsPTS['PTS_TRANSAKTIONSID'][0] . "','Preislistenpr�fung','toolbar=no,menubar=no,dependent=yes,resizable=yes,status=no,scrollbars=yes,fullscreen=yes');>";
			break;
		case 95:		// Gesperrt
		case 40:		// Freigabe
			print "&nbsp;<input type=image title='Details untersuchen' src=/bilder/sc_suche2.png name=cmdDetails_" . $rsPLI['ID_FPLB'][$Zeile] . ">";
			//print "&nbsp;<img title='Daten �bertragen' src=/bilder/sc_rakete.png name=cmdDatenSenden_" . $rsPTS['PTS_ID_FPLB'][$Zeile] . " onclick=window.open('./preislisten_DatenUebertragen.php?FPLB=" . $rsPTS['PTS_ID_FPLB'][0] . "','Preislistenpr�fung','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');>";
			//print "&nbsp;<input type=image title='Daten �bertragen' src=/bilder/sc_rakete.png name=cmdDatenSenden>";
			echo '<a href="./preislisten_Main.php?cmdAktion=Uebersicht&Freigabe=1&FPLB='. $rsPTS['PTS_ID_FPLB'][0].'"><img border=0 name=PL_Freigabe_FPLB_ID src=/bilder/sc_rakete.png name=cmdDatenSenden></a>';
			break;
		case 42:		// Liste ist weg
			echo '&nbsp;';
			break;
	}
	echo '</td></tr>';
	if(isset($rsPTS['PTS_ZUSATZ_TID'][0]) AND $rsPTS['PTS_ZUSATZ_TID'][0]!='')
	{
		echo '<tr><td colspan=3>&nbsp;</td>';
		echo '<td id=TabellenZeileGrau>Artikelzusatzliste</td>';
		$BindeVariablen=array();
		$BindeVariablen['var_tid']=$rsPTS['PTS_ZUSATZ_TID'][0];
		
		$SQL = ' SELECT *';
		$SQL .= ' FROM EXPGW.EXPGW_EXPORT';
		$SQL .= ' WHERE TRANSAKTIONSID=:var_tid'; // Die vorherige Version!!
		$rsZTID = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		echo '<td colspan=5 id=TabellenZeileGrau>' . DFUE_Status($rsZTID['ARBEITSSTATUS'][0]) . '</td>';
		echo '</tr>';
	}

flush();

	//**************************************
	// Aktuelle Preisliste anzeigen
	//**************************************

	$BindeVariablen=array();
	$BindeVariablen['var_N0_id_fplb']=$rsPLI['ID_FPLB'][$Zeile];
	$BindeVariablen['var_tid']=(isset($rsPTS['PTS_TRANSAKTIONSID'][0])?$rsPTS['PTS_TRANSAKTIONSID'][0]:0);
	$BindeVariablen['var_T_pfp_parameter']='ABLAUFWARNUNG';
	
	$SQL = ' SELECT *';
	$SQL .= ' FROM EXPGW.EXPGW_EXPORT EE';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS ON TRANSAKTIONSID = PTS_TRANSAKTIONSID';
	$SQL .= ' AND PTS_ID_FPLB=:var_N0_id_fplb';
	$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPARAMETER ON PFP_ID_FPLB = PTS_ID_FPLB AND PFP_Parameter=:var_T_pfp_parameter';
//	$SQL .= ' WHERE TRANSAKTIONSID<>:var_tid'; // Die vorherige Version!!
	$SQL .= ' WHERE NOT EXISTS (SELECT * FROM (SELECT :var_tid AS TID FROM DUAL) BB WHERE EE.TRANSAKTIONSID=BB.TID)';
	$SQL .= ' ORDER BY TRANSAKTIONSID DESC';
/*
	$SQL = "SELECT * from (";
	$SQL .= " SELECT EE.*, PTS.*, PP.*, row_number() over (order by ee.transaktionsid desc) as rnum";
	$SQL .= " FROM EXPGW.EXPGW_EXPORT EE";
	$SQL .= " INNER JOIN EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS PTS ON (EE.TRANSAKTIONSID = PTS.PTS_TRANSAKTIONSID";
	$SQL .= " AND PTS.PTS_ID_FPLB=".$rsPLI['ID_FPLB'][$Zeile]." and EE.TRANSAKTIONSID<>0" . (isset($rsPTS['PTS_TRANSAKTIONSID'][0])?$rsPTS['PTS_TRANSAKTIONSID'][0]:0).")";
	$SQL .= " LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPARAMETER PP ON (PP.PFP_ID_FPLB = PTS.PTS_ID_FPLB AND PP.PFP_Parameter = 'ABLAUFWARNUNG')";
	$SQL .= " ) where rnum=1";		
*/	
	$rsPTS = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);

	if($awisRSZeilen>0)
	{
		echo '<tr>';
		echo '<tr><td colspan=2>&nbsp;</td>';
		echo '<td id=TabellenZeileGrau title="Status der zuletzt eingespielten Liste">AKT</td>';
		echo '<td id=TabellenZeileGrau>' . DFUE_Status($rsPTS['ARBEITSSTATUS'][0]) . '</td>';
		echo '<td id=TabellenZeileGrau>' . $rsPTS['PTS_USERDAT'][0] . '</td>';
		echo '<td id=TabellenZeileGrau>' . substr($rsPTS['PTS_GUELTIG_VON'][0],0,10) . '</td>';
		$AblaufWarnung = mktime(0,0,0,substr($rsPTS['PTS_GUELTIG_BIS'][0],3,2),substr($rsPTS['PTS_GUELTIG_BIS'][0]-$rsPTS['PFP_WERT'][0],0,2),substr($rsPTS['PTS_GUELTIG_BIS'][0],6,4));
		$Ablauf = mktime(0,0,0,substr($rsPTS['PTS_GUELTIG_BIS'][0],3,2),substr($rsPTS['PTS_GUELTIG_BIS'][0],0,2),substr($rsPTS['PTS_GUELTIG_BIS'][0],6,4));
	//awis_Debug(1,$AblaufWarnung,$Ablauf,date('d.m.Y',$AblaufWarnung),date('d.m.Y',$Ablauf));
		if(time()>=$AblaufWarnung)
		{
			echo '<td id=TabellenZeileGrau><span class=HinweisText style="text-decoration:blink;">' . substr($rsPTS['PTS_GUELTIG_BIS'][0],0,10) . '</span></td>';
		}
		else
		{
			echo '<td id=TabellenZeileGrau>' . substr($rsPTS['PTS_GUELTIG_BIS'][0],0,10) . '</td>';
		}
		echo '<td id=TabellenZeileGrau title=\''.$rsPTS['PTS_BEMERKUNG'][0].'\'>' . $rsPTS['PTS_TRANSAKTIONSID'][0] . '</td>';
		echo '<td id=TabellenZeileGrau>&nbsp;</td>';
		
		if($rsPTS['PTS_ZUSATZ_TID'][0]!='')
		{
			echo '<tr><td colspan=3>&nbsp;</td>';
			echo '<td id=TabellenZeileGrau>Artikelzusatzliste</td>';
			
			$BindeVariablen=array();
			$BindeVariablen['var_tid']=$rsPTS['PTS_ZUSATZ_TID'][0];
			
			$SQL = ' SELECT *';
			$SQL .= ' FROM EXPGW.EXPGW_EXPORT';
			$SQL .= ' WHERE TRANSAKTIONSID=:var_tid'; // Die vorherige Version!!
			$rsZTID = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
			echo '<td colspan=5 id=TabellenZeileGrau>' . DFUE_Status($rsZTID['ARBEITSSTATUS'][0]) . '</td>';
			echo '</tr>';
		}
	}

	echo '</tr>';


	echo '<tr><td colspan=9 height=4 bgcolor=#AAAABB></td></tr>';

flush();

}

echo '</table>';
echo '</form>';


//awisLogoff($comcon);


function DFUE_Status($Status)
{
	switch ($Status) {
		case 'C':
			return 'Transaktion erzeugt.';
		case 'L':
			return 'Neue Daten geladen.';
		case 'M':
			return 'Experian informiert.';
		case 'T':
			return '<font color=green>Datentransfer abgeschlossen.</font>';
		case 'E':
			return '<font color=red>Fehler.</font>';
		case 'F':
			return '<font color=red>Fehler.</font>';
		default:
			return '::unbekannt::';
	}
}
?>