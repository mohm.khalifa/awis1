<?php
global $AWISCursorPosition;
global $AWISBenutzer;


$Form = new awisFormular();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('REL','%');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','Status');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht3602=$AWISBenutzer->HatDasRecht(3602);

echo "<form name=frmSuche method=post action=./radeinlagerung_Main.php?cmdAktion=Details>";

/**********************************************
* * Eingabemaske
***********************************************/
$Param = unserialize($AWISBenutzer->ParameterLesen('RELSuche'));

$Form->Formular_Start();

$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
$FilZugriffListe=explode(',',$FilZugriff);

$AWISCursorPosition='sucREL_LFDNR';
$Form->ZeileStart();

if(count($FilZugriffListe)>1)
{
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REL']['REL_FIL_ID'].':',180);
	$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
	$SQL .= ' FROM Filialen ';
	$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
	$SQL .= ' ORDER BY FIL_BEZ';
	$Form->Erstelle_SelectFeld('*REL_FIL_ID','',200,true,$SQL);
	$AWISCursorPosition='sucRELFIL_ID';
}
elseif($FilZugriff!='')
{
	$FIL_ID=$FilZugriff;	
	//$Form->Erstelle_HiddenFeld('*LKK_FIL_ID',$FIL_ID);
	//$Form->Erstelle_TextLabel($FIL_ID,200);
	echo '<input type=hidden name="sucREL_FIL_ID" value="'.$FIL_ID.'">';
}
else
{
	$Form->Erstelle_TextLabel($AWISSprachKonserven['REL']['REL_FIL_ID'].':',180);
	$Form->Erstelle_TextFeld('*REL_FIL_ID',($Param['SPEICHERN']=='on'?$Param['REL_FIL_ID']:''),20,200,true);		
	$AWISCursorPosition='sucREL_FIL_ID';
}
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Status'].':',180);
$StatusText = explode("|",$AWISSprachKonserven['REL']['lst_Radeinlagerungstatus']);
$Form->Erstelle_SelectFeld('*REL_STATUS',($Param['SPEICHERN']=='on'?$Param['REL_STATUS']:''),200,true,'','0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$StatusText,'');
//$Form->Erstelle_TextFeld('*REL_STATUS',($Param['SPEICHERN']=='on'?$Param['REL_STATUS']:''),20,200,true);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['REL']['REL_LFDNR'].':',180);
$Form->Erstelle_TextFeld('*REL_LFDNR',($Param['SPEICHERN']=='on'?$Param['REL_LFDNR']:''),20,200,true);
$Form->ZeileEnde();	

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['REL']['REL_NR'].':',180);
$Form->Erstelle_TextFeld('*REL_NR',($Param['SPEICHERN']=='on'?$Param['REL_NR']:''),20,200,true,'','','','T');
$Form->ZeileEnde();	

//Auswahl speichern
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
$Form->ZeileEnde();

$Form->Formular_Ende();

$Form->SchaltflaechenStart();

$Form->Schaltflaeche('href','cmd_zurueck','../../filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

$Form->SchaltflaechenEnde();

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}
?>
