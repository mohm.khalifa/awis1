<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('PNL','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Meldung','KennwoerterStimmenNichtUeberein');
$TextKonserven[]=array('Meldung','PersonalNrNichtGueltig');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);

	$AWIS_KEY1 = $_POST['txtPNL_KEY'];
	//***********************************************************************************
	//** Portalnewsletter
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPNL_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$AWIS_KEY1 = $_POST['txtPNL_KEY'];

		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler='';
		$rsPNL = $DB->RecordSetOeffnen('SELECT * FROM Portalnewsletter WHERE PNL_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));
		if($rsPNL->EOF())
		{
			$PflichtfelderTest = array('PNL_BEZEICHNUNG','PNL_DATUM','PNL_PLT_KEY');
			$Pflichtfelder= array();
			foreach($PflichtfelderTest as $Feld)
			{
				if(isset($_POST['txt'.$Feld]))
				{
					$Pflichtfelder[] = $Feld;
				}
			}
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PNL'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$SQL = 'INSERT INTO Portalnewsletter';
			$SQL .= '(PNL_DATUM,PNL_BETREFF,PNL_BEMERKUNGINTERN, PNL_PLT_KEY, PNL_STATUS';
            $SQL .= ', PNL_EINLEITUNGSTEXT,PNL_EINLEITUNGBILD';
			$SQL .= ',PNL_USER,PNL_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('D',$_POST['txtPNL_DATUM'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNL_BETREFF'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNL_BEMERKUNGINTERN'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPNL_PLT_KEY'],true);
			$SQL .= ',1';
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNL_EINLEITUNGSTEXT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNL_EINLEITUNGBILD'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200909151906);
			}
			$SQL = 'SELECT seq_PNL_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY1=$rsKey->Feldinhalt('KEY');
            require_once('awis_portalnewsletter.inc');
            $NewsLetter = new awis_PortalNewsletter($AWISBenutzer->BenutzerName());
            $NewsLetter->ImportiereAktuelleInfos($AWIS_KEY1);
		}
		else
		{
			$PflichtfelderTest = array('PNL_BEZEICHNUNG','PNL_DATUM','PNL_PLT_KEY');
			$Pflichtfelder= array();
			foreach($PflichtfelderTest as $Feld)
			{
				if(isset($_POST['txt'.$Feld]))
				{
					$Pflichtfelder[] = $Feld;
				}
			}

			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PNL'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}

			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]) AND substr($FeldName,0,20)!='PNL_BENUTZERKENNWORT')
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPNL->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPNL->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPNL->FeldInfo($FeldName,'TypKZ'),$rsPNL->FeldInhalt($FeldName),true);

					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPNL->FeldInhalt('PNL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Portalnewsletter SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PNL_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PNL_userdat=sysdate';
				$SQL .= ' WHERE PNL_key=0' . $_POST['txtPNL_KEY'] . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',1010101048,$SQL,2);
				}
			}
		}
	}


	$Felder = $Form->NameInArray($_POST,'txtPNT_KEY_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
    if(!empty($Felder))
    {
        $Keys = array();
        foreach($Felder AS $Feld)
        {
            $Keys[] = substr($Feld,11);
        }

        foreach($Keys as $Key)
        {
            $rsMFI = $DB->RecordSetOeffnen('SELECT * FROM Portalnewslettertexte WHERE PNT_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['txtPNT_KEY_'.$Key],false));

            if($rsMFI->EOF())		// Neue Zuordnung
            {
                $Fehler = '';
                    // Daten auf Vollst�ndigkeit pr�fen
                $Pflichtfelder = array();

                foreach($Pflichtfelder AS $Pflichtfeld)
                {
                    if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
                    {
                        $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['AST'][$Pflichtfeld].'<br>';
                    }
                }


                    // Wurden Fehler entdeckt? => Speichern abbrechen
                if($Fehler!='')
                {
                    die('<span class=HinweisText>'.$Fehler.'</span>');
                }

                $SQL = 'INSERT INTO Portalnewslettertexte';
                $SQL .= '(PNT_PNL_KEY, PNT_BEREICH, PNT_POSITION, PNT_UEBERSCHRIFT, PNT_LINK, PNT_TEXT';
                $SQL .= ', PNT_VERSENDEN';
                $SQL .= ',PNT_USER, PNT_USERDAT)';
                $SQL .= 'VALUES (';
                $SQL .= '' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNT_BEREICH_'.$Key],false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPNT_POSITION_'.$Key],false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNT_UEBERSCHRIFT_'.$Key],false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNT_LINK_'.$Key],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPNT_TEXT_'.$Key],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPNT_VERSENDEN_'.$Key],false);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('PNT speichern',909031332,$SQL,4);
                }
                $SQL = 'SELECT seq_PNT_KEY.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $AWIS_KEY2=$rsKey->Feldinhalt('KEY');

            }
            else 					// ge�nderter Artikel
            {
                $FehlerListe = array();
                $UpdateFelder = '';

                $Felder = $Form->NameInArray($_POST,'/txt.*_'.$Key.'/',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_REGEXP);

                $FeldListe = '';
                foreach($Felder AS $Feld)
                {
                    $FeldName = substr($Feld,3);
                    if(isset($_POST['old'.$FeldName]))
                    {
                        $TabellenFeld = substr($FeldName,0,strlen($FeldName)-strlen((string)$Key)-1);

                        $WertNeu=$DB->FeldInhaltFormat($rsMFI->FeldInfo($TabellenFeld,'TypKZ'),$_POST[$Feld],true);
                        $WertAlt=$DB->FeldInhaltFormat($rsMFI->FeldInfo($TabellenFeld,'TypKZ'),$_POST['old'.$FeldName],true);
                        $WertDB=$DB->FeldInhaltFormat($rsMFI->FeldInfo($TabellenFeld,'TypKZ'),$rsMFI->FeldInhalt($TabellenFeld),true);
                        if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($TabellenFeld)==7 AND substr($TabellenFeld,-4,4)=='_KEY'))
                        {
                            if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
                            {
                                $FehlerListe[] = array($TabellenFeld,$WertAlt,$WertDB);
                            }
                            else
                            {
                                $FeldListe .= ', '.$TabellenFeld.'=';

                                if($_POST[$Feld]=='')	// Leere Felder immer als NULL
                                {
                                    $FeldListe.=' null';
                                }
                                else
                                {
                                    $FeldListe.=$WertNeu;
                                }
                            }
                        }
                    }
                }

                if(count($FehlerListe)>0)
                {
                    $Meldung = str_replace('%1',$rsMFI->FeldInhalt('PNT_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
                    foreach($FehlerListe AS $Fehler)
                    {
                        $FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
                        $Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
                    }
                    $Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
                    die();
                }
                elseif($FeldListe!='')
                {
                    $SQL = 'UPDATE Portalnewslettertexte SET';
                    $SQL .= substr($FeldListe,1);
                    $SQL .= ', PNT_user=\''.$AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ', PNT_userdat=sysdate';
                    $SQL .= ' WHERE PNT_KEY=0' . $_POST['txtPNT_KEY_'.$Key] . '';
                    if($DB->Ausfuehren($SQL)===false)
                    {
                        throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),909131431,$SQL,2);
                    }
                }
                $AWIS_KEY2=$_POST['txtPNT_KEY_'.$Key];
            }
        }
    }


}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>