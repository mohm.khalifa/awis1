<?php

global $VGD;
global $AWIS_KEY1;

try {
    $VGD->Form->DebugAusgabe(1, $_POST);

    if($VGD->Param['QUELLE'] == 1) { // VERSGDVDECKUNGSABFRAGEN
        $VGD->RechteMeldung(2);
        $SQL  ='UPDATE VERSGDVDECKUNGSABFRAGEN';
        $SQL .=' SET';
        $SQL .='   VGD_SB                   = '.$VGD->DB->WertSetzen('VGD','T',$_POST['txtVGD_SB']);
        $SQL .=' WHERE VGD_KEY              = '.$VGD->DB->WertSetzen('VGD','N0',$AWIS_KEY1,false);

        $VGD->DB->Ausfuehren($SQL, '', true, $VGD->DB->Bindevariablen('VGD'));
        $VGD->Aenderungshistorie->SchreibeAenderung('VGD',$AWIS_KEY1,'VGD_SB',$_POST['oldVGD_SB'],$_POST['txtVGD_SB'],'',$VGD->AWISBenutzer->BenutzerName(),strftime('%d.%m.%Y %H:%M:%S'));
        $Meldung = $VGD->AWISSprachKonserven['VGD']['VGD_UPDATE_OK'];
    } else { // VERSBEAUFTRAGUNGEN (DIREKTVERMITTLUNGEN)
        $VGD->RechteMeldung(2);

        $SQL = 'select VBA_AUFTRAGSBESCHREIBUNG';
        $SQL .= ' from VERSBEAUFTRAGUNGEN';
        $SQL .= ' where VBA_KEY = '.$VGD->DB->WertSetzen('VBA','N0',$AWIS_KEY1,false);
        $rsVBA = $VGD->DB->RecordsetOeffnen($SQL, $VGD->DB->Bindevariablen('VBA', true));

        $auftragsbeschreibungDB = $rsVBA->FeldInhalt('VBA_AUFTRAGSBESCHREIBUNG');
        $sb = str_pad($_POST['txtVGD_SB'], 4, '0', STR_PAD_LEFT);

        $auftragsbeschreibungNeu = substr($auftragsbeschreibungDB,0,71) . $sb;

        $VBA_STATUS = ($_POST['txtVBA_STATUS'] == 'CLOSED'?$_POST['txtVBA_STATUS']:null);

        $SQL  ='UPDATE VERSBEAUFTRAGUNGEN';
        $SQL .=' SET';
        $SQL .='   VBA_AUFTRAGSBESCHREIBUNG   = '.$VGD->DB->WertSetzen('VGD','T',$auftragsbeschreibungNeu,false);
        $SQL .=',  VBA_VORSTABZUGKZ           = '.$VGD->DB->WertSetzen('VGD','N0',$_POST['txtVBA_VORSTABZUGKZ'],false);
        $SQL .=',  VBA_STATUS                 = '.$VGD->DB->WertSetzen('VGD', 'T',$VBA_STATUS, true);
        $SQL .=' WHERE VBA_KEY                = '.$VGD->DB->WertSetzen('VGD','N0',$AWIS_KEY1,false);

        $VGD->DB->Ausfuehren($SQL, '', true, $VGD->DB->Bindevariablen('VGD'));
        $Meldung = $VGD->AWISSprachKonserven['VGD']['VGD_UPDATE_OK'];
    }
    $VGD->Aenderungshistorie->SchreibeAenderung('VBA',$AWIS_KEY1,'SB',intval($_POST['oldVGD_SB']),intval($_POST['txtVGD_SB']),'',$VGD->AWISBenutzer->BenutzerName(),strftime('%d.%m.%Y %H:%M:%S'));
    $VGD->Aenderungshistorie->SchreibeAenderung('VBA',$AWIS_KEY1, 'STATUS',$_POST['oldVBA_STATUS'], $_POST['txtVBA_STATUS'], '', $VGD->AWISBenutzer->BenutzerName(),strftime('%d.%m.%Y %H:%M:%S'));
    $VGD->Aenderungshistorie->SchreibeAenderungenAusPOST($AWIS_KEY1, $_POST, array('VBA_VORSTABZUGKZ'));
    if($Meldung){
        $VGD->Form->Hinweistext($Meldung,awisFormular::HINWEISTEXT_OK);
    }

} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $VGD->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $VGD->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $VGD->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}


?>