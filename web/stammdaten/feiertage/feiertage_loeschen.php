<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtXFT_KEY']))
		{
			$Tabelle = 'XFT';
			$Key=$_POST['txtXFT_KEY'];

			$SQL = 'SELECT XFT_KEY, XFT_BEZEICHNUNG, XFT_TAG';
			$SQL .= ' FROM feiertage ';
			$SQL .= ' WHERE XFT_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$XFTKey = $rsDaten->FeldInhalt('XFT_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('XFT','XFT_BEZEICHNUNG'),$rsDaten->FeldInhalt('XFT_BEZEICHNUNG'));
			$Felder[]=array($Form->LadeTextBaustein('XFT','XFT_TAG'),$Form->Format('D',$rsDaten->FeldInhalt('XFT_TAG')));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				case 'Rueckgaben':
					$Tabelle = 'ZRU';
					$Key=$_GET['Del'];
					

					$SQL = 'SELECT ZRU_KEY, ZRU_XFT_KEY, ZRU_DATUM, ZRU_MENGE';
					$SQL .= ' FROM Zukaufrueckgaben ';
					$SQL .= ' WHERE ZRU_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);
					
					$XFTKey=$rsDaten->FeldInhalt('ZRU_XFT_KEY');
					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('ZRU','ZRU_DATUM'),$rsDaten->FeldInhalt('ZRU_DATUM'));
					$Felder[]=array($Form->LadeTextBaustein('ZRU','ZRU_MENGE'),$rsDaten->FeldInhalt('ZRU_MENGE'));
					break;
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'XFT':
				$SQL = 'DELETE FROM Feiertage WHERE XFT_key=0'.$_POST['txtKey'];
				$AWIS_KEY1='';
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('feiertage_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./feiertage_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('XFTKey',$XFTKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>