<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('AST', 'AST_ATUNR');
$TextKonserven[] = array('AST', 'AST_USERDAT');
$TextKonserven[] = array('AST', 'AST_BEZEICHNUNGWW');
$TextKonserven[] = array('WUG', 'WUG_WGR_ID');

try {
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_453_1'));

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht458 = $AWISBenutzer->HatDasRecht(458);        // Recht f�r Lieferantenartikel
    if (($Recht458 & 1) == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if (isset($_GET['Sort'])) {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
    } elseif (isset($Param['ORDERBY'])) {
        $ORDERBY = $Param['ORDERBY'];
    } else {
        $ORDERBY = ' ORDER BY AST_USERDAT DESC';
    }

    $SQL = 'SELECT ARTIKELSTAMM.*, LOA_BEZEICHNUNGWW, WUG_WGR_ID';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM ARTIKELSTAMM ';
    $SQL .= ' LEFT OUTER JOIN LOESCHARTIKEL ON AST_ATUNR = LOA_AST_ATUNR';
    $SQL .= ' INNER JOIN WARENUNTERGRUPPEN ON AST_WUG_KEY = WUG_KEY';

    $Bedingung = ' AND AST_USER = \'XXX\'';

    $WGRS = '';
    if (isset($_POST['txtWGR_ID'])) {
        $WGRS = explode(',', trim($_POST['txtWGR_ID']));
        if ($_POST['txtWGR_ID'] != $Param['WGR_ID']) {
            $Param['BLOCK'] = 1;
            $_REQUEST['Block'] = 1;
        }
    } elseif ($Param['WGR_ID'] != '') {
        $WGRS = explode(',', $Param['WGR_ID']);
    }

    if (is_array($WGRS) AND $WGRS[0] != '') {
        $Bedingung .= ' AND WUG_WGR_ID IN (\'' . (implode("','", $WGRS)) . '\')';
        $WGRS = implode(',', $WGRS);
    }

    $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

    // Zum Bl�ttern in den Daten
    $Block = 1;
    if (isset($_REQUEST['Block'])) {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    } elseif (isset($Param['BLOCK'])) {
        $Block = $Param['BLOCK'];
    }

    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
    if ($MaxDS === false) {
        $SQL = 'SELECT 1 FROM DUAL';
    }

    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);

    $Param['BLOCK'] = $Block;
    $Param['ORDERBY'] = $ORDERBY;
    $Param['WGR_ID'] = $WGRS;

    $AWISBenutzer->ParameterSchreiben('Formular_453_1', serialize($Param));

    $rsAPR = $DB->RecordSetOeffnen($SQL);

    $Form->SchreibeHTMLCode('<form name=frmGeloeschte action=./artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Geloeschte method=POST  enctype="multipart/form-data">');

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['WUG']['WUG_WGR_ID'] . ':', 100);
    $Form->Erstelle_TextFeld('WGR_ID', $Param['WGR_ID'], 30, 300, true);
    $AWISCursorPosition = 'txtWGR_ID';
    $Form->ZeileEnde();

    if ($rsAPR->EOF()) {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    } else {
        $Form->ZeileStart();

        if (($Recht458 & 4)) {
            $Icons[] = array('new', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen&APR_KEY=0', 'g', $AWISSprachKonserven['Wort']['lbl_hinzufuegen']);
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Geloeschte';
        $Link .= '&Sort=AST_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_ATUNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_ATUNR'], 100, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Geloeschte';
        $Link .= '&Sort=AST_USERDAT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_USERDAT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_USERDAT'], 190, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Geloeschte';
        $Link .= '&Sort=LOA_BEZEICHNUNGWW' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LOA_BEZEICHNUNGWW'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'], 450, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Geloeschte';
        $Link .= '&Sort=WUG_WGR_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'WUG_WGR_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WUG']['WUG_WGR_ID'], 100, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsAPR->EOF()) {
            $Form->ZeileStart();

            $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=0' . $rsAPR->FeldInhalt('AST_KEY');
            $Form->Erstelle_ListenFeld('#AST_ATUNR', $rsAPR->FeldInhalt('AST_ATUNR'), 10, 100, false, ($DS % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('#AST_USERDAT', $rsAPR->FeldInhalt('AST_USERDAT'), 10, 190, false, ($DS % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('#LOA_BEZEICHNUNGWW', $rsAPR->FeldInhalt('LOA_BEZEICHNUNGWW'), 10, 450, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#WUG_WGR_ID', $rsAPR->FeldInhalt('WUG_WGR_ID'), 10, 100, false, ($DS % 2), '', '', 'T');

            $Form->ZeileEnde();

            $rsAPR->DSWeiter();
            $DS++;
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Geloeschte';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }

    echo '</div>'; //PG: Verzweiflungs-DIV, da ich den Fehler nicht finde.
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Form->Schaltflaeche('script', 'cmdHilfe',
        "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=werkzeuge&Seite=" . (isset($_GET['Seite'])?$_GET['Seite']:'Geloeschte') . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');",
        '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060003");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "20081��61223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>