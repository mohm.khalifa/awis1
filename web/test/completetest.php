<?php
	/*
	 *	This is a rather extensive example to show you, what's all possible.
	 *	On my notebook ( a PentiumII w/ 366MHz ) this script takes app. 6 sec
	 *	for processing and a few more seconds for rendering in the PDF viewer.
	 *	For getting an impression, what certain objects do, just comment all 
	 *	others out, maybe have a look at the debug-output...you know the drill ;-)
	 *
	 *	If you have major problems with this example, please use the
	 *	forum on the sourceforge.net-projectsite (see README) or send me
	 *	an email, but I would like to ask you to use the forum, I usually go
	 *	there at least once a day.
	 *	Have fun!
	 */

	// If you experience timeouts running this script, uncomment this
	// command to extend the execution-time.
	// set_time_limit( 0 );

	// First include the wrapper. Every other class is included there
	include( "pc4p_init.inc" );

	// Uncomment this define, to get a tree-view of your PDF. WARNING!!!
	// This view can get HUGE!!!!!
	// define( "PC4PDEBUG", 1 );

	// Uncomment this define, to get a grid on your document. You may check the
	// positioning with this feature. The number you enter will be taken as grid-width
	// define( "PC4PGRID", 5 );

	// This function may look a bit awkward, but with GDLib < 2.0 the functionality is
	// somewhat limited
	function draw_smilie()
	{
		$im = ImageCreate( 50, 50 );
		$white = ImageColorAllocate ($im, 255, 255, 255);
		$yellow = ImageColorAllocate ($im, 255, 204, 0);
		$black = ImageColorAllocate ($im, 0, 0, 0);
		ImageArc( $im, 25, 25, 45, 45, 0, 360, $black );
		ImageArc( $im, 25, 25, 42, 42, 0, 360, $black );
		ImageArc( $im, 17, 17, 2, 2, 0, 360, $black );
		ImageArc( $im, 33, 17, 2, 2, 0, 360, $black );
		ImageFill( $im, 25, 25, $yellow );
		ImageArc( $im, 17, 17, 5, 5, 0, 360, $black );
		ImageArc( $im, 33, 17, 5, 5, 0, 360, $black );
		ImageArc( $im, 25, 25, 30, 30, 0, 180, $black );
		ImageArc( $im, 25, 25, 28, 28, 0, 180, $black );
		return $im;
	}

	// Create yourself a PDF...
	$PDF = &pc4p_create_pdf( array( "Author" => "Alexander Wirtz", "Title" => "PC4P-Test" ) );

	// The following parameters have to be set, prior to the first creation of a page:
	// Set a different version of Acrobat...here version 5
	$PDF->pc4p_set_compatibility( "1.4" );

	// Set your serial for PDFlib
	// $PDF->pc4p_set_serial( "I-Have-No-Serial" );

	// Use a temporary file instead of creating the PDF in the memory...better for large files
	// $PDF->pc4p_set_tempfile( "/tmp" );
	// $PDF->pc4p_set_tempfile( "C:/WINDOWS/TEMP" );

	// Set for maximum compression
	$PDF->pc4p_set_compression( 9 );

	// "Normal" display, usually in a plugin
	$PDF->pc4p_set_savepdf( "inline", "pc4p.pdf" );
	// Force download of the File
	// $PDF->pc4p_set_savepdf( "attached", "pc4p.pdf" );
	// Save the file in your filesystem
	$PDF->pc4p_set_savepdf( "drive", "pc4p.pdf" );

	// ...and within this PDF a page with a DIN A4-format
	$Page1 = &pc4p_create_page( $PDF, "a4" );
	$Page1->pc4p_set_newpage( 1 );
	$bookmark1 = $Page1->pc4p_add_bookmark( "The example for PC4P", 0, 1 );
	$bookmark2 = $Page1->pc4p_add_bookmark( "First Page", $bookmark1, 1 );

	// Pagenumber for the page
	$pagenum = &pc4p_create_object( $Page1->header, "pagenumber" );
	$pagenum->pc4p_set_text( "p. %P" );
	$pagenum->pc4p_set_alignment( "center" );

	// The first object on this page is a table...
	$table1 = &pc4p_create_object( $Page1, "table");
	// and it has a 5x2 matrix with certain names.
	$table1->pc4p_create_tablematrix( 5, 2, array( "col1", "col2", "col3", "col4", "col5" ), array( "row1", "row2" ) );
		// Now I define 2 text-objects in the (col1, row1)-cell...
		$text1a = &pc4p_create_object( $table1->cell[ "col1" ][ "row1" ] , "text" );
		$text1a->pc4p_set_text( "bla1a" );
		$text1b = &pc4p_create_object( $table1->cell[ "col1" ][ "row1" ] , "text" );
		$text1b->pc4p_set_text( "bla1b" );
		// ...and lots of other text-objects...
		$text2 = &pc4p_create_object( $table1->cell[ "col2" ][ "row1" ] , "text" );
		$text2->pc4p_set_text( "This is an example for wordwrapping, colored text and alignment set to \"justify\"" );
		$text2->pc4p_set_textcolor( "maroon" );
		$text2->pc4p_set_alignment( "justify" );
		$text2->pc4p_set_weblink( "http://www.pc4p.net/", "", 0 );
		 // This is an example for memory-created images. You have to have the GDLib installed for this
		if( extension_loaded( "gd" ) && function_exists( "pdf_open_memory_image" ) ) {
			$image1 = &pc4p_create_object( $table1->cell[ "col3" ][ "row1" ] , "image" );
			$image1->pc4p_set_image( draw_smilie() );
		} else {
			// Sorry, no real smilie :-/
			$text2 = &pc4p_create_object( $table1->cell[ "col3" ][ "row1" ] , "text" );
			$text2->pc4p_set_text( ":-)" );
		}
		$text4 = &pc4p_create_object( $table1->cell[ "col4" ][ "row1" ] , "text" );
		$text4->pc4p_set_text( "bla4" );

		// ...here is an image in the table, which I rotate by 215.6 degrees
		$image2 = &pc4p_create_object( $table1->cell[ "col5" ][ "row1" ], "image" );
		$image2->pc4p_set_image( "completetest.jpg" );
		$image2->pc4p_set_rot( 215.6 );
		$image2->pc4p_set_imagescale( 1.5 );
		$image2->pc4p_set_weblink( "http://www.pdflib.de/", "dashed", 2, "red" );

		// More text-objects
		$text5 = &pc4p_create_object( $table1->cell[ "col1" ][ "row2" ] , "text" );
		$text5->pc4p_set_text( "averylongwordjusttoshowyouthateventhisidioticthingiswrappedtoo" );
		$text5->pc4p_set_underline();
		$text6 = &pc4p_create_object( $table1->cell[ "col2" ][ "row2" ] , "text" );
		$text6->pc4p_set_text( "bla6" );
		// A second table within the first, filled with text-objects
		// created in a box, to give the table a background color

		$box1 = &pc4p_create_object( $table1->cell[ "col3" ][ "row2"] , "box" );
		$box1->pc4p_set_margin( 0 );
		$box1->pc4p_set_boxborder( array( "top", "bottom", "left", "right" ), "none" );
		$box1->pc4p_set_fillcolor( "lime" );

		$table2 = &pc4p_create_object( $box1 , "table" );
		$table2->pc4p_create_tablematrix( 3 );
			$text7 = &pc4p_create_object( $table2->cell[ 0 ][ 0 ] , "text" );
			$text7->pc4p_set_text( "bla7" );
			$text8 = &pc4p_create_object( $table2->cell[ 1 ][ 0 ] , "text" );
			$text8->pc4p_set_alignment( "center" );
			$text8->pc4p_set_text( "bla8" );
			$text9 = &pc4p_create_object( $table2->cell[ 2 ][ 0 ] , "text" );
			$text9->pc4p_set_alignment( "center" );
			$text9->pc4p_set_text( "bla9" );
		$table2->pc4p_add_tablerow();
			$text10 = &pc4p_create_object( $table2->cell[ 0 ][ 1 ] , "text" );
			$text10->pc4p_set_text( "bla10" );
			$text11 = &pc4p_create_object( $table2->cell[ 1 ][ 1 ] , "text" );
			$text11->pc4p_set_alignment( "center" );
			$text11->pc4p_set_text( "bla11" );
			$text12 = &pc4p_create_object( $table2->cell[ 2 ][ 1 ] , "text" );
			$text12->pc4p_set_text( "bla12" );
		$table2->pc4p_add_tablerow();
			$text13 = &pc4p_create_object( $table2->cell[ 0 ][ 2 ] , "text" );
			$text13->pc4p_set_text( "bla13" );
			$text14 = &pc4p_create_object( $table2->cell[ 1 ][ 2 ] , "text" );
			$text14->pc4p_set_alignment( "center" );
			$text14->pc4p_set_text( "bla14" );
			$text15 = &pc4p_create_object( $table2->cell[ 2 ][ 2 ] , "text" );
			$text15->pc4p_set_text( "bla15" );
		// Now I set the borders for this table. I do this _after_ the table is complete!!!
		$table2->pc4p_set_tableborder( "all", "single" , 2.0 );
		$table2->pc4p_set_tablefillcolor( "aqua" );

		$table2->pc4p_set_cellborder( $table2->cell[ 0 ][ 0 ], array( "top", "left" ), "double", 2.0 );
		$table2->pc4p_set_cellborder( $table2->cell[ 1 ][ 0 ], "top", "double", 2.0 );
		$table2->pc4p_set_cellborder( $table2->cell[ 2 ][ 0 ], array( "top", "right" ), "double", 2.0 );
		$table2->pc4p_set_cellborder( $table2->cell[ 0 ][ 2 ], array( "bottom", "left" ), "double", 2.0 );
		$table2->pc4p_set_cellborder( $table2->cell[ 1 ][ 2 ], "bottom", "double", 2.0 );
		$table2->pc4p_set_cellborder( $table2->cell[ 2 ][ 2 ], array( "bottom", "right" ), "double", 2.0 );
		$table2->pc4p_set_cellborder( $table2->cell[ 0 ][ 1 ], "left", "double", 2.0 );
		$table2->pc4p_set_cellborder( $table2->cell[ 2 ][ 1 ], "all", "double", 2.0 );

		$text11 = &pc4p_create_object( $table1->cell[ "col4" ][ "row2" ] , "text" );
		$text11->pc4p_set_alignment( "right");
		$text11->pc4p_set_text( "bla16" );
		$text12 = &pc4p_create_object( $table1->cell[ "col5" ][ "row2" ] , "text" );
		$text12->pc4p_set_text( "bla17" );

	// A line with a predefined width
	$line = &pc4p_create_object( $Page1, "line" );
	$line->pc4p_set_linestyle( "double" );
	$line->pc4p_set_width( "65%" );
	$line->pc4p_set_linewidth( 3.5 );
	$line->pc4p_set_alignment( "center" );
	$line->pc4p_set_linecolor( "red" );
	$line->pc4p_set_margin( array( "top" => 20, "bottom" => 20, "left" => 10, "right" => 10 ) );

	// And a third table...
	$table3 = &pc4p_create_object( $Page1, "table" );
	$table3->pc4p_create_tablematrix( 2 );
	$table3->pc4p_set_width( 400 );

	// ...with a tableheader...
	$table3->pc4p_set_tableheader( 0 );
	$table4 = &pc4p_create_object( $table3->cell[ 0 ][ 0 ] , "table" );
	$table4->pc4p_create_tablematrix( 2, 1 );
	$table4->pc4p_set_cellfillcolor( $table4->cell[ 0 ][ 0 ], "lime" );
	$table4->pc4p_set_cellfillcolor( $table4->cell[ 1 ][ 0 ], "#008080" );
		$text1 = &pc4p_create_object( $table4->cell[ 0 ][ 0 ] , "text" );
		$text1->pc4p_set_alignment( "center" );
		$text1->pc4p_set_text( "This" );
		$text2 = &pc4p_create_object( $table4->cell[ 1 ][ 0 ] , "text" );
		$text2->pc4p_set_alignment( "center" );
		$text2->pc4p_set_text( "is" );

	$text3 = &pc4p_create_object( $table3->cell[ 1 ][ 0 ] , "text" );
	$text3->pc4p_set_alignment( "center" );
	$text3->pc4p_set_text( "the table-header" );
	// ...and lots of rows!
	for( $i = 1; $i < 200; $i++) {
		$table3->pc4p_add_tablerow();
			$text1 = &pc4p_create_object(  $table3->cell[ 0 ][ $i ], "text" );
			$text1->pc4p_set_alignment( "right" );
			$text1->pc4p_set_text( "Pagebreak within " );
			$text2 = &pc4p_create_object(  $table3->cell[ 1 ][ $i ], "text" );
			$text2->pc4p_set_alignment( "left" );
			$text2->pc4p_set_text( " a table - ".$i );
	}
	$table3->pc4p_set_tableborder();

	// As last object, I add a box
	$Page2 = &pc4p_create_page( $PDF, "a4" );
	$bookmark3 = $Page2->pc4p_add_bookmark( "Last Page", $bookmark1, 1 );

	// Pagenumber for the second page
	$pagenum = &pc4p_create_object( $Page2->header, "pagenumber" );
	$pagenum->pc4p_set_alignment( "center" );
	$pagenum->pc4p_set_text( "p. %P" );

	$box2 = &pc4p_create_object( $Page2, "box" );
	$box2->pc4p_set_margin( array( "top" => 340, "bottom" => 340, "left" => 100, "right" => 100 ) );
	$box2->pc4p_set_boxborder( "all", "double", 3.5 );
	$box2->pc4p_set_boxbordercolor( "all", "blue" );
	$box2->pc4p_set_fillcolor( "yellow" );
	$box2->pc4p_set_alignment( "center" );
		// with some text in it
		$text13 = &pc4p_create_object( $box2, "text" );
		$text13->pc4p_set_font( "Times-Bold", 15 );
		$text13->pc4p_set_alignment( "center" );
		$text13->pc4p_set_rendering( 1 );
		$text13->pc4p_set_textcolor( "green" );
		// Attention!!! Manual linefeeds!!! Wohoo!!!
		$text13->pc4p_set_text( "Okay, here we\nhave some text in a box with large margins to proove, that pagebreak functions\n\nproperly.\n\n" );
		
	// Now I draw the PDF and (hopefully) get the output
	$PDF->pc4p_draw();
?>
