<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular


$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();

$Form = new awisFormular();


$TextKonserven = array();
$TextKonserven[]=array('XDE','%');
$TextKonserven[]=array('XAB','%');
$TextKonserven[]=array('TITEL','%');

$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Liste','lst_ALLE_0');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_weiter');


$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Zahlungsmittelmatrix'].'</title>';

?>

</head>
<body>
<?php

try
{
	
	$Fehler = false;
	
	$_POST['txtSQL'] = isset($_POST['txtSQL'])?str_replace(';', '',$_POST['txtSQL']):'';
	
	if (isset($_POST['cmdSpeichern_y']) or isset($_POST['cmdWeiter_x']))
	{
		if ($_POST['txtSQL'] == '')
		{
			$Form->Hinweistext('SQL Eingeben!',1);
		}
	}
	
	if(isset($_POST['cmdSpeichern_y']))
	{
		$InsertExportDaten  ='INSERT';
		$InsertExportDaten .=' INTO EXPORTDATEN';
		$InsertExportDaten .='   (';
		$InsertExportDaten .='     XDE_XRC_ID,';
		$InsertExportDaten .='     XDE_STUFE,';
		$InsertExportDaten .='     XDE_BEZEICHNUNG,';
		$InsertExportDaten .='     XDE_BESCHREIBUNG,';
		$InsertExportDaten .='     XDE_SQL,';
		$InsertExportDaten .='     XDE_STATUS,';
		$InsertExportDaten .='     XDE_USER,';
		$InsertExportDaten .='     XDE_USERDAT';
		$InsertExportDaten .='   )';
		$InsertExportDaten .='   VALUES';
		$InsertExportDaten .='   (';
		$InsertExportDaten .=' ' . $DB->WertSetzen('XDE', 'N0', $_POST['sucXRC_ID']).' ,';
		$InsertExportDaten .=' ' . $DB->WertSetzen('XDE', 'N0', $_POST['sucXRS_BIT']).' ,';
		$InsertExportDaten .=' ' . $DB->WertSetzen('XDE', 'T', $_POST['txtBezeichnung']).' ,';
		$InsertExportDaten .=' ' . $DB->WertSetzen('XDE', 'T', $_POST['txtBeschreibung']).' ,';
		$InsertExportDaten .=' ' . $DB->WertSetzen('XDE', 'T', $_POST['txtSQL']).' ,';
		$InsertExportDaten .=' \'A\' ,';
		$InsertExportDaten .=' ' . $DB->WertSetzen('XDE', 'N0',$AWISBenutzer->BenutzerName()).' ,';
		$InsertExportDaten .=' sysdate ';
		$InsertExportDaten .='   )';
        $DB->TransaktionBegin();
		$DB->Ausfuehren($InsertExportDaten,'', true, $DB->Bindevariablen('XDE',true));
		$SQLXDEKEY = 'select seq_XDE_KEY.currval as key  from dual';
		$rsXDEKEY = $DB->RecordSetOeffnen($SQLXDEKEY);
		
		
	}

	
	$Form->Formular_Start();
	$Form->SchreibeHTMLCode("<form name=frmGenerator method=post action=./datenexporte_Main.php?cmdAktion=Generator>");
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Export Kopf: ', 220,'font-size: 15px; font-weight: bold');
	$Form->Trennzeile('T');
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Bezeichnung', 120);
	$Form->Erstelle_TextFeld('!Bezeichnung', isset($_POST['txtBezeichnung'])?$_POST['txtBezeichnung']:'', 150, 150,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Beschreibung', 120);
	$Form->Erstelle_TextFeld('!Beschreibung', isset($_POST['txtBeschreibung'])?$_POST['txtBeschreibung']:'', 150, 150,true);
	$Form->ZeileEnde();
	
	 
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Recht',120);
	$SQLRechte = 'Select XRC_ID, XRC_ID || \' - \' || XRC_RECHT from Rechte order by XRC_RECHT asc';
	$Form->Erstelle_SelectFeld('~!*XRC_ID', isset($_POST['sucXRC_ID'])?$_POST['sucXRC_ID']:'', 700, true,$SQLRechte, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '','','','','',array());
	$Form->ZeileEnde();
	
	$filter='***XRS_Daten;sucXRC_ID';
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Rechtestufe',100);
	
	$Form->Erstelle_SelectFeld('!*XRS_BIT', isset($_POST['sucXRS_BIT'])?$_POST['sucXRS_BIT']:'', 400, true,$filter,isset($_POST['sucXRS_BIT'])?$_POST['sucXRS_BIT']:$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array());
	$Form->ZeileEnde();

	if(isset($_POST['txtSQL']) and $_POST['txtSQL']!='')
	{
		$Form->FormularBereichStart();
		$Form->FormularBereichInhaltStart('SQL');
	}
	
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('SQL: ', 120);
	$Form->Erstelle_Textarea('!SQL', isset($_POST['txtSQL'])?$_POST['txtSQL']:'', 600, 300, 4,true);
	$Form->ZeileEnde();
	
	if(isset($_POST) and $_POST['txtSQL']!='')
	{
		$Form->FormularBereichEnde();
		$Form->FormularBereichInhaltEnde();
	}

	
	if (isset($_POST['txtSQL']) and  $_POST['txtSQL']!='')
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Extrahierte Felder: ', 220,'font-size: 15px; font-weight: bold');
		$Form->Trennzeile('T');
		$Form->ZeileEnde();
		
		$rsFelder = $DB->RecordSetOeffnen('select * from ( ' . $_POST['txtSQL']. ') daten where rownum <1');
		
		$ExportDatenFelder = array();

        //Code f�r "Alle anchecken"
        $Script = '
            <script>
            $( document ).ready(function() {
                $("#txtcheckPflicht").change(function() {
                 $(".Pflicht_chk").prop(\'checked\',  $("#txtcheckPflicht").prop(\'checked\'));
                });  
                
                $("#txtcheckFilter").change(function() {
                 $(".Filter_chk").prop(\'checked\',  $("#txtcheckFilter").prop(\'checked\'));
                });  
            });  
            </script>
			
			
			';

        echo $Script;

		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Feldname', 200);
		$Form->Erstelle_Liste_Ueberschrift('Typ', 100);
		$Form->Erstelle_Liste_Ueberschrift('Feldgroesse', 150);
		$Form->Erstelle_Liste_Ueberschrift('FeldSpaltenbreite', 150);
		$Form->Erstelle_Liste_Ueberschrift('Sort', 50);
		$Form->Erstelle_Liste_Ueberschrift('FeldKonserve', 300);
		$Form->Erstelle_Liste_Ueberschrift('Datenquellle', 300);
		$Form->Erstelle_Liste_Ueberschrift('Ausdruck', 300);
		$Form->Erstelle_Liste_Ueberschrift('Pflicht', 50);
		$Form->Erstelle_Liste_Ueberschrift('Filterfeld', 100);
		
		$Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift('', 200);
        $Form->Erstelle_Liste_Ueberschrift('', 100);
        $Form->Erstelle_Liste_Ueberschrift('', 150);
        $Form->Erstelle_Liste_Ueberschrift('', 150);
        $Form->Erstelle_Liste_Ueberschrift('', 50);
        $Form->Erstelle_Liste_Ueberschrift('', 300);
        $Form->Erstelle_Liste_Ueberschrift('', 300);
        $Form->Erstelle_Liste_Ueberschrift('', 300);
        ob_start(); //Checkbox in Buffer zeichnen
        $Form->Erstelle_Checkbox('checkPflicht', 0, 20,true,1,'','','');
        $Inhalt = ob_get_clean();
        $Form->Erstelle_Liste_Ueberschrift('',18);
        $Form->Erstelle_Liste_Ueberschrift($Inhalt,25);
        ob_start(); //Checkbox in Buffer zeichnen
        $Form->Erstelle_Checkbox('checkFilter', 0, 20,true,1,'','','');
        $Inhalt = ob_get_clean();
        $Form->Erstelle_Liste_Ueberschrift('',25);
        $Form->Erstelle_Liste_Ueberschrift($Inhalt,82);

        $Form->ZeileEnde();
	;
		for ($i = 1; $i <= $rsFelder->AnzahlSpalten(); $i++)
		{
			$Feldname  = $rsFelder->FeldInfo($i, awisRecordset::FELDINFO_NAME);
			//Feste Felder - Nicht editierbar
			$ExportDatenFelder[$Feldname]['Feldname'] = $Feldname;
			$ExportDatenFelder[$Feldname]['Feldtyp'] =$rsFelder->Feldinfo($i, awisRecordset::FELDINFO_FORMAT);
				
			//nicht automatisch generierbare, aber editierbare Felder kommen immer aus der Maske:
			$ExportDatenFelder[$Feldname]['XDF_PFLICHTFELD'] = isset($_POST['txt'.$Feldname.'_' .'XDF_PFLICHTFELD'])?$_POST['txt'.$Feldname.'_' .'XDF_PFLICHTFELD']:'0';
            $ExportDatenFelder[$Feldname]['XDF_FILTERFELD'] = isset($_POST['txt'.$Feldname.'_' .'XDF_FILTERFELD'])?$_POST['txt'.$Feldname.'_' .'XDF_FILTERFELD']:'0';

            $ExportDatenFelder[$Feldname]['XDF_DATENQUELLE'] = isset($_POST['txt'.$Feldname.'_' .'XDF_DATENQUELLE'])?$_POST['txt'.$Feldname.'_' .'XDF_DATENQUELLE']:'0';
            $ExportDatenFelder[$Feldname]['XDF_AUSDRUCK'] = isset($_POST['txt'.$Feldname.'_' .'XDF_AUSDRUCK'])?$_POST['txt'.$Feldname.'_' .'XDF_AUSDRUCK']:'';

			//automatisch generierbare, Editierbare Felder - Wenn Post da ist, dann POST, wenn nicht, den Inhalt der DB:
			$ExportDatenFelder[$Feldname]['Feldgroesse']  = isset($_POST['txt'.$Feldname.'_' .'Feldgroesse'])?$_POST['txt'.$Feldname.'_' .'Feldgroesse']:$rsFelder->FeldInfo($i, awisRecordset::FELDINFO_GROESSE);
			$ExportDatenFelder[$Feldname]['FeldSpaltenbreite']  = isset($_POST['txt'.$Feldname.'_' .'FeldSpaltenbreite'])?$_POST['txt'.$Feldname.'_' .'FeldSpaltenbreite']:$rsFelder->FeldInfo($i, awisRecordset::FELDINFO_GROESSE)*15;
			if($ExportDatenFelder[$Feldname]['Feldgroesse'] > 15){
                $ExportDatenFelder[$Feldname]['Feldgroesse'] = 15;
                $ExportDatenFelder[$Feldname]['FeldSpaltenbreite'] = 15 * 15;
            }
            $ExportDatenFelder[$Feldname]['FeldSortierung']  = isset($_POST['txt'.$Feldname.'_' .'FeldSortierung'])?$_POST['txt'.$Feldname.'_' .'FeldSortierung']: $i;

            $KonservenHG ='';
			//Textkonserven sind bissi spezieller
			if (isset($_POST['txt'.$Feldname.'_' .'FeldKonserve']) and $_POST['txt'.$Feldname.'_' .'FeldKonserve'] != '::FEHLT::' and $_POST['txt'.$Feldname.'_' .'FeldKonserve'] != '')
			{
				$ExportDatenFelder[$Feldname]['FeldKonserve'] = $_POST['txt'.$Feldname.'_' .'FeldKonserve'];
			}
			else
			{
				//Schauen, ob es die Textkonserve gibt:
				$Bereich = explode('_',$Feldname)[0];
				$Kennung = str_replace($Bereich, '', $Feldname);
				$SQLKonserve = 'Select * from Textkonserven where XTX_BEREICH = ' .$DB->WertSetzen('SQL', 'T', $Bereich);
				$SQLKonserve .= ' and XTX_KENNUNG = ' .$DB->WertSetzen('SQL', 'T', $Feldname);
			
				if ($DB->ErmittleZeilenAnzahl($SQLKonserve,$DB->Bindevariablen('SQL',true))>0)
				{
					$ExportDatenFelder[$Feldname]['FeldKonserve'] = $Bereich . ':'. $Bereich . $Kennung;
				}
				else
				{
					$ExportDatenFelder[$Feldname]['FeldKonserve'] = '::FEHLT::';
					$Fehler = true;
					$KonservenHG = 'background-color: #F78181; color: #ffffff';
				}
			
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
			}
			
			
			//Listenfelder gleich hinmalen:
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('Feldname', $ExportDatenFelder[$Feldname]['Feldname'], 200, 200,false,$i%2);
			$Form->Erstelle_ListenFeld('Feldtyp',$ExportDatenFelder[$Feldname]['Feldtyp'], 100, 100,false,$i%2);		
			$Form->Erstelle_ListenFeld( $ExportDatenFelder[$Feldname]['Feldname'].'_Feldgroesse', $ExportDatenFelder[$Feldname]['Feldgroesse'], 150, 150,true,$i%2,'');
			$Form->Erstelle_ListenFeld( $ExportDatenFelder[$Feldname]['Feldname'].'_FeldSpaltenbreite',$ExportDatenFelder[$Feldname]['FeldSpaltenbreite'],150, 150, true,$i%2);
			$Form->Erstelle_ListenFeld( $ExportDatenFelder[$Feldname]['Feldname'].'_FeldSortierung',$ExportDatenFelder[$Feldname]['FeldSortierung'], 50, 50,true,$i%2);
			$Form->Erstelle_ListenFeld( $ExportDatenFelder[$Feldname]['Feldname'].'_FeldKonserve',$ExportDatenFelder[$Feldname]['FeldKonserve'], 300, 300,true,$i%2,'','','T','L','',$KonservenHG);
			$Form->Erstelle_ListenFeld( $ExportDatenFelder[$Feldname]['Feldname'].'_XDF_AUSDRUCK',$ExportDatenFelder[$Feldname]['XDF_AUSDRUCK'], 300, 300,true,$i%2);
			$Form->Erstelle_ListenFeld( $ExportDatenFelder[$Feldname]['Feldname'].'_XDF_DATENQUELLE',$ExportDatenFelder[$Feldname]['XDF_DATENQUELLE'], 300, 300,true,$i%2);
			$Form->Erstelle_ListenCheckbox($ExportDatenFelder[$Feldname]['Feldname'].'_XDF_PFLICHTFELD', $ExportDatenFelder[$Feldname]['XDF_PFLICHTFELD'].'_XDF_PFLICHTFELD', 50,true,1,'','','','',$i%2,true,'Pflicht_chk');
			$Form->Erstelle_ListenCheckbox($ExportDatenFelder[$Feldname]['Feldname'].'_XDF_FILTERFELD', $ExportDatenFelder[$Feldname]['XDF_FILTERFELD'].'_XDF_FILTERFELD', 50,true,1,'','','','',$i%2,true,'Filter_chk');
			
			
			$Form->ZeileEnde();
				
			if(isset($_POST['cmdSpeichern_y']))
			{
				$InsertXDF  ='INSERT';
				$InsertXDF .=' INTO EXPORTDATENFELDER';
				$InsertXDF .='   (';
				$InsertXDF .='     XDF_XDE_KEY,';
				$InsertXDF .='     XDF_FELDNAME,';
				$InsertXDF .='     XDF_DATENTYP,';
				$InsertXDF .='     XDF_FELDLAENGE,';
				$InsertXDF .='     XDF_SPALTENBREITE,';
				$InsertXDF .='     XDF_SORTIERUNG,';
				$InsertXDF .='     XDF_USER,';
				$InsertXDF .='     XDF_USERDAT,';
				$InsertXDF .='     XDF_TEXTKONSERVE,';
				$InsertXDF .='     XDF_AUSDRUCK,';
				$InsertXDF .='     XDF_PFLICHTFELD,';
				$InsertXDF .='     XDF_FILTERFELD';
				$InsertXDF .='   )';
				$InsertXDF .='   VALUES';
				$InsertXDF .='   (';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'N0', $rsXDEKEY->FeldInhalt('KEY')).' ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['Feldname']).' ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['Feldtyp']).' ,';
					
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['Feldgroesse']).' ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['FeldSpaltenbreite']).' ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['FeldSortierung']).' ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $AWISBenutzer->BenutzerName()).' ,';
				$InsertXDF .=' sysdate ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['FeldKonserve']).', ';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['XDF_AUSDRUCK']).' ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['XDF_PFLICHTFELD']).' ,';
				$InsertXDF .=' ' . $DB->WertSetzen('XDF', 'T', $ExportDatenFelder[$Feldname]['XDF_FILTERFELD']).'';
				$InsertXDF .='   )';
				$DB->Ausfuehren($InsertXDF,'',true,$DB->Bindevariablen('XDF',true));
			}
				
		}
	
		
	}
	
	//Wenn ich bis dahin gekommen bin, war alles gut --> Commit
	if(isset($_POST['cmdSpeichern_y']))
	{
		$DB->TransaktionCommit();
	}
	
	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	$Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'S');
	if(!$Fehler and isseT($_POST['cmdWeiter_x']))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	
	if($AWISCursorPosition!=='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		
		echo $DB->LetzterSQL();
		echo '<br>';
		echo '<br>';
		echo '<br>';
		echo '<br>';
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"20160509");
		
					
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</body>
</html>