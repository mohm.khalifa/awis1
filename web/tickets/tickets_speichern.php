<?php
require_once 'awisOT.inc';
global $AWIS_KEY1;
global $AWIS_KEY2;


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$OTObj = new awisOT($AWISBenutzer);
	$Form = new awisFormular();
//$Form->DebugAusgabe(1,$_POST);
    $TXT_Speichern = $Form->LadeTexte($TextKonserven);
    
    $Felder = $Form->NameInArray($_POST, 'txtOTF_', awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
    if(!empty($Felder))
    {
        $SQL = 'SELECT * FROM OTFormulare WHERE OFO_KEY = :var_N0_OFO_KEY';
        $Bindevariablen = array('var_N0_OFO_KEY'=>$_POST['txtOFO_KEY']);
        $rsOFO = $DB->RecordSetOeffnen($SQL,$Bindevariablen);
//var_dump($_POST);
        $Ticket = array();
	    $Pflichtfelder = array();
	    
	    // Beschreibung, die aus unterschiedlichen Feldern zusammengebaut wird
	    $Description = array();	
	    $VORNACHNAME = array();

        foreach($Felder as $Feld)
        {
            $FeldTeile = explode('_',$Feld);        // Beispiel: txtOTF_WERT_4_T, bei chk-Box txtOFW_WERT_4_T_<OptionNr>

            $SQL = 'SELECT * FROM OTFelder ';
            $SQL .= ' INNER JOIN OTFormularFelder ON OTF_KEY = OFF_OTF_KEY';
            $SQL .= ' WHERE OTF_KEY = :var_N0_OTF_KEY ';
            $SQL .= ' AND OTF_STATUS = \'A\'';
            $SQL .= ' ORDER BY OFF_SORTIERUNG';        // Damit die Funktionen auf vorhandene Werte zugreifen k�nnen
            $Bindevariablen = array('var_N0_OTF_KEY'=>$FeldTeile[2]);
            $rsOTF = $DB->RecordSetOeffnen($SQL,$Bindevariablen);
            if($rsOTF->EOF())
            {
                continue;
            }            
            $FeldFormat = $rsOTF->FeldInhalt('OTF_FORMAT');
            
//echo $rsOTF->FeldInhalt('OTF_KEY').'-'.$rsOTF->FeldInhalt('OTF_BEZEICHNUNG').'/'.$rsOTF->FeldInhalt('OTF_OTFUNKTION').'-'.$FeldTeile[2].'<br>';
            // Wird eine Funktion verwendet, um die Daten zu ermitteln?
            if($rsOTF->FeldInhalt('OTF_OTFUNKTION')!='')        // Muss der Wert umgebaut werden?
            {
                $Funktion = explode('~',$rsOTF->FeldInhalt('OTF_OTFUNKTION'));
                switch ($Funktion[0])
                {
                	case 'DESCRIPTION':		// Allgemeine Beschreibung mit Feldern f�llen
                		if(!isset($Funktion[1]) OR $Funktion[1]=='00' OR $Funktion[1]=='')		// Zeile des Feldes => Letzter Eintrag in einer Kette
                		{
                			$Beschreibung = '';
                			foreach ($Description AS $Text)
                			{
                				$Beschreibung .= $Text;
                			}
	                		$Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $Beschreibung;
                		}
                		else
                		{
                			if(isset($FeldTeile[3]))		// Wird bei den Checkboxen verwendet. Dann alles zusammenbauen
                			{
                				$Optionen='';
                				$BasisFeldName = $FeldTeile[0].'_'.$FeldTeile[1].'_'.$FeldTeile[2].'_';
                				for($i=0;$i<999;$i++)
                				{
                					if(isset($_POST[$BasisFeldName.$i]))
                					{
                						$Optionen .= ', '. $_POST[$BasisFeldName.$i];
                					}
                				}
                				$Description[$Funktion[1]]=$rsOTF->FeldInhalt('OTF_BEZEICHNUNG').': '.substr($Optionen,1)."\r\n\r\n";
                			}
                			else
                			{
                				// Standard-Text-Feld
                				$Description[$Funktion[1]]=$rsOTF->FeldInhalt('OTF_BEZEICHNUNG').': '.$_POST[$Feld]."\r\n\r\n";
                			}
                		}
                		break;
                	case 'VORNACHNAME':		// Allgemeine Beschreibung mit Feldern f�llen
                		if(!isset($Funktion[1]) OR $Funktion[1]=='00' OR $Funktion[1]=='')		// Zeile des Feldes => Letzter Eintrag in einer Kette
                		{
                			$Beschreibung = '';
                			$VORNACHNAME[$Funktion[1]]=$_POST[$Feld];
                			foreach ($VORNACHNAME AS $Text)
                			{
                				$Beschreibung .= ' '.$Text;
                			}
	                		$Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = trim($Beschreibung);
                		}
                        else
                		{
                            $VORNACHNAME[$Funktion[1]]=$_POST[$Feld];
                        }
                		break;
                    case 'BENUTZERNAME':        // AWIS-Benutzername
                        $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $AWISBenutzer->BenutzerName();
                        break;
                    case 'MITARBEITERNAME':
                        $SQL = 'SELECT COALESCE(KON_NAME1 || \', \' || KON_NAME2, XBN_VOLLERNAME,'.$DB->WertSetzen('__XBN__', 'T', $AWISBenutzer->BenutzerName()).') AS KONNAME';
                        $SQL .= ' FROM BENUTZER ';
                        $SQL .= ' LEFT OUTER JOIN KONTAKTE ON XBN_KON_KEY = KON_KEY';
                        $SQL .= ' WHERE XBN_KEY = '.$DB->WertSetzen('__XBN__', 'N0', $AWISBenutzer->BenutzerID());
                    
                        $rsXBN = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('__XBN__'));
                        $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $rsXBN->FeldInhalt('KONNAME');
                        break;
                    case 'PERSONKEY':        // Key aus dem Feld (KEY~PERSNR)
                        if(isset($_POST['txtOTF_WERT_17_T']) AND $_POST['txtOTF_WERT_17_T']!='')
                        {
                            $Daten = explode('~',$_POST['txtOTF_WERT_17_T']);
                            $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $Daten[0];
                        }
                        break;
                    case 'PERSONPERSNR':        // PersNr aus dem Feld (KEY~PERSNR)
                        if(isset($_POST['txtOTF_WERT_17_T']) AND $_POST['txtOTF_WERT_17_T']!='')
                        {
                            $Daten = explode('~',$_POST['txtOTF_WERT_17_T']);
                            $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $Daten[1];
                        }
                        elseif(isset($_POST['txtOTF_WERT_31_T']) AND $_POST['txtOTF_WERT_31_T']!='')		// Hilfstabelle aus AWIS
                        {
                            $SQL = 'SELECT OTP_PERSNR FROM OTPersonen WHERE OTP_ID = '.$_POST['txtOTF_WERT_31_T'];
                            $rsOTP = $DB->RecordSetOeffnen($SQL);
                            $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $rsOTP->FeldInhalt('OTP_PERSNR');
                        }
                        break;
                    case 'REFTOUSER':
                        $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = explode('~',$_POST[$Feld]);
                    case 'KONNAME':
                        $SQL = 'SELECT KON_NAME1 || COALESCE(\', \' || KON_NAME2,\'\') AS Mitarbeiter';
                        $SQL .= ' FROM Kontakte';
                        $SQL .= ' WHERE KON_KEY = :var_N0_kon_key';
                        $Bindevariablen=array();
                        $Bindevariablen['var_N0_kon_key']=$_POST[$Feld];
                        $rsDaten = $DB->RecordSetOeffnen($SQL,$Bindevariablen);
                        $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $rsDaten->FeldInhalt('MITARBEITER');
                        break;
                    case 'KONMAIL':
                        if(isset($_POST[$Funktion[1]]))
                        {
                            $SQL = 'SELECT KKO_WERT AS Mail';
                            $SQL .= ' FROM Kontaktekommunikation';
                            $SQL .= ' WHERE KKO_KON_KEY = :var_N0_kon_key AND KKO_KOT_KEY = 7';
                            $Bindevariablen=array();
                            $Bindevariablen['var_N0_kon_key']=$_POST[$Funktion[1]];
                            $rsDaten = $DB->RecordSetOeffnen($SQL,$Bindevariablen);
                            $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $rsDaten->FeldInhalt('MAIL');
                        }
                        break;
                    case 'KONKOSTENSTELLE':		// Kostenstelle f�r einen Kontakt
                        $SQL = 'SELECT KOSTENSTELLE';
                        $SQL .= ' FROM Kontakte';
                        $SQL .= ' INNER JOIN PERSONAL_KOMPLETT ON KON_PER_NR = PERSNR';
                        $SQL .= ' WHERE KON_KEY = :var_N0_kon_key';
                        $Bindevariablen=array();
                        $Bindevariablen['var_N0_kon_key']=$_POST[$Funktion[1]];
                        $rsDaten = $DB->RecordSetOeffnen($SQL,$Bindevariablen);
                        $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $rsDaten->FeldInhalt('KOSTENSTELLE');
                        break;
                    case 'STRING':      // Erzeugt einen Text aus POST Variablen und beliebigen Texten
                        // STRING~txtOTF_WERT_108~, ~txtOTF_WERT_109
                        $Erg = '';
                        for($i=1;;$i++)
                        {
                            if(!isset($Funktion[$i]))
                            {
                                break;
                            }
                            if(substr($Funktion[$i],0,3)=='txt')        // Maskenfeld
                            {
                                $Erg .= isset($_POST[$Funktion[$i]])?$_POST[$Funktion[$i]]:'?????';
                            }
                            else                                        // Beliebiger Text 
                            {
                                $Erg .= $Funktion[$i];
                            }
                        }

                        // Sonderfall bei zusammengesetzten Feldern: nie mit <,> beginnen, falls ein Feld fehlt
                        if(substr($Erg,0,2)==', ')
                        {
                            $Erg = substr($Erg,2);
                        }
                        $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $Erg;
                }
            }
            elseif($rsOTF->FeldInhalt('OTF_OTTYP')!='-' AND $_POST[$Feld]!='')
            {
                $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')] = $Form->Format($FeldFormat,$_POST[$Feld]);
                $Form->DebugAusgabe(3,$rsOTF->FeldInhalt('OTF_OTFELD'), $Feld, $_POST[$Feld], $Ticket);
            }
            
            //******************************************************
            // Variablen-Ersetzungen
            // z.B. $FORMULAR$ - $OTF_KEY-StringVal-43$ - $DATUM$
            //******************************************************
            if(isset($Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')]))
            {
	            $Text = $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')];
	            
	            $Text = str_replace('$FORMULAR$',$rsOFO->FeldInhalt('OFO_BEZEICHNUNG'),$Text);
	            $Text = str_replace('$DATUM$',$Form->Format('D',date('d.m.Y')),$Text);
	            $Pos = strpos($Text,'$OTF_KEY');
	            if($Pos!==false)
	            {
	                $TextErsatz='';
	            	$Feld = substr($Text,$Pos+1,30);
	            	$Pos = strpos($Feld,"$",8);
	            	$Feld = substr($Feld,0,$Pos);
	            	$FeldTeile = explode('-',$Feld);
					switch($FeldTeile[2])
					{
						case 43:		// Angefordert f�r...
						    if(isset($_POST['txtOTF_WERT_'.$FeldTeile[2].'_'.$FeldTeile[1]]))
						    {
    							$SQL = 'SELECT OTP_BEZEICHNUNG';
    							$SQL .= ' FROM OTPersonen';
    							$SQL .= ' WHERE OTP_ID = :var_N0_OTP_ID';
    							$Bindevariablen=array();
    							$Bindevariablen['var_N0_OTP_ID']=$DB->FeldInhaltFormat('N0',$_POST['txtOTF_WERT_'.$FeldTeile[2].'_'.$FeldTeile[1]]);
    							$rsOTP = $DB->RecordSetOeffnen($SQL,$Bindevariablen);						
    							$TextErsatz = $rsOTP->FeldInhalt('OTP_BEZEICHNUNG');
						    }
							break;
						default:
						    if(strstr($FeldTeile[2],'|')!==false)
					        {
					            $Felder = explode('|',$FeldTeile[2]);
					            foreach($Felder AS $ErsatzFeld)
					            {
					                if(is_numeric($ErsatzFeld))
					                {
                                        if(isset($_POST['txtOTF_WERT_'.$ErsatzFeld]) AND $_POST['txtOTF_WERT_'.$ErsatzFeld]!='')
                                        {
                                           $TextErsatz = $_POST['txtOTF_WERT_'.$ErsatzFeld];
                                           break;
                                        }
					                }
					                else 
					                {
					                    $TextErsatz = $Ticket[$FeldTeile[1]][$ErsatzFeld];					                    
					                }
					            }
					        }
							elseif(is_numeric($FeldTeile[2]))
						    {
						        $TextErsatz = $_POST['txtOTF_WERT_'.$FeldTeile[2]];
						    }
					        else 
							{
							    $TextErsatz = $Ticket[$FeldTeile[1]][$FeldTeile[2]];
							}
					}	            
					$Text = str_replace('$'.$Feld.'$',$TextErsatz,$Text);
	            }
	            
	            if(isset($_POST['txtTitel']))
	            {
	               $Text = str_replace('$TITEL$',$_POST['txtTitel'],$Text);
	            }
	            $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')]=$Text;
            }
        }

        if(file_exists('/daten/web/tickets/off_'.$rsOFO->FeldInhalt('OFO_KENNUNG').'_speichern.php'))
        {
            $DetailBlock = '';
            $DetailBlock110 = '';
            $DetailBlockPP = '';

            include '/daten/web/tickets/off_'.$rsOFO->FeldInhalt('OFO_KENNUNG').'_speichern.php';
            
            $Form->DebugAusgabe(3,$DetailBlock);
            $Ticket['StringVal']['Description'] = $DetailBlock;

            $Ticket['StringVal']['Benachrichtigung_an'] = $DetailBlock110;
            if($DetailBlockPP != '') {
                $Ticket['StringVal']['workflowAction'] = $DetailBlockPP;
            }
            // Sonderfall Formular 1, wenn eine Filale gew�hlt wurde
            if(isset($_POST['txtOTF_WERT_110']) AND $_POST['txtOTF_WERT_110']!='' AND $_POST['txtOTF_WERT_110']!='~')
            {
                $Ticket['StringVal']['Mitarbeiter_Freitext']='Filiale '.$_POST['txtOTF_WERT_110'];
            }
        }

        if(!empty($Pflichtfelder))
        {
            $Fehler = '<b>'.$TXT_Speichern['Fehler']['err_KeinWert'].'</b><br><br>';
    		foreach($Pflichtfelder AS $Pflichtfeld)
    		{
   				$Fehler .= $Pflichtfeld.'<br>';
    		}
  			die('<span class=HinweisText>'.$Fehler.'</span>');
        }
        //*******************************************************************************************
        // Dateianh�nge separat betrachten
        //*******************************************************************************************
        if(isset($_FILES) AND !empty($_FILES))
        {
            foreach($_FILES as $Feld=>$Inhalt)
            {
                if($Inhalt['tmp_name']!='')
                {
                    $FeldTeile = explode('_',$Feld);        // Beispiel: txtOTF_WERT_4_T

                    $SQL = 'SELECT * FROM OTFelder ';
                    $SQL .= ' INNER JOIN OTFormularFelder ON OTF_KEY = OFF_OTF_KEY';
                    $SQL .= ' WHERE OTF_KEY = :var_N0_OTF_KEY ';
                    $SQL .= ' AND OTF_STATUS = \'A\'';
                    $SQL .= ' ORDER BY OFF_SORTIERUNG';        // Damit die Funktionen auf vorhandene Werte zugreifen k�nnen
                    $Bindevariablen = array('var_N0_OTF_KEY'=>$FeldTeile[2]);
                    $rsOTF = $DB->RecordSetOeffnen($SQL,$Bindevariablen);

                    $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')]['name']=$Inhalt['name'];
                    $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')]['typ']=$Inhalt['type'];
                    $Ticket[$rsOTF->FeldInhalt('OTF_OTTYP')][$rsOTF->FeldInhalt('OTF_OTFELD')]['dateiname']=$Inhalt['tmp_name'];
                }
            }
        }
        $Form->DebugAusgabe(3,$Ticket,$_POST);      
        $OTObj->DebugLevel(0);
        $TicketID = $OTObj->ErzeugeTicket($rsOFO->FeldInhalt('OFO_OTPFAD'), $Ticket);
        if($TicketID==-1)
        {
            throw new Exception('Fehler beim Anlegen des Tickets',201607121006);
        }
        else
        {
            $OTObj = new awisOT($AWISBenutzer);
            $TicketDaten = $OTObj->ObjektListe($rsOFO->FeldInhalt('OFO_OTPFAD'), '#'.$TicketID, explode(',',$rsOFO->FeldInhalt('OFO_ANZEIGEFELDER')),1);
            
            $TicketNummer='';
            foreach($TicketDaten AS $Ticketfelder)
            {
                foreach($Ticketfelder AS $Ticketfeld=>$Feldinhalt)
                {
                    if($Ticketfeld=='Number')
                    {
                        $TicketNummer = 'A-'.str_pad($Feldinhalt,6,'0',STR_PAD_LEFT);       // Ticket Nummer immer 6 stellig auff�llen, um identische Anzeige zu OT zu bekommen
                    }
                }
            }
            // Bei erfolgreicher Speicherug wir die Ticket-ID im AWIS gespeichert-> kann man nachschauen!
            $SQL = 'INSERT INTO OTTickets';
            $SQL .= '(OTT_OFO_KEY,OTT_BETREFF,OTT_TICKETID,OTT_TICKETNR,OTT_XBN_KEY,OTT_USER,OTT_USERDAT)';
            $SQL .= ' VALUES(';
            $SQL .= ' '.$rsOFO->FeldInhalt('OFO_KEY');
            $SQL .= ','.$DB->FeldInhaltFormat('T',(isset($Ticket['StringVal']['Title'])?$Ticket['StringVal']['Title']:'XXX'));
            $SQL .= ','.$DB->FeldInhaltFormat('T',$TicketID);
            $SQL .= ','.$DB->FeldInhaltFormat('T',$TicketNummer);
            $SQL .= ','.$DB->FeldInhaltFormat('N0',$AWISBenutzer->BenutzerID());
            $SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
            $SQL .= ',SYSDATE)';
			if($DB->Ausfuehren($SQL)===false) {
				throw new awisException('Fehler',201111241029,$SQL,awisException::AWIS_ERR_SYSTEM);
			} else {
			    $AWIS_KEY1 = $DB->RecordSetOeffnen('SELECT SEQ_OTT_KEY.currval from dual')->FeldInhalt(1);
			    $AWIS_KEY2 = $TicketNummer;
			}
        }
    }
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler','Problem 0502121735: '.$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler','Problem 0502121734: '.$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>