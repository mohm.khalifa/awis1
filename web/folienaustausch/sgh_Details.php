<?php
global $AWISCursorPosition; // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DETAILANSICHTSGH;
global $DETAILANSICHTKASSENDATEN;
global $FLAGKEINEAUSWAHL;
global $FLAGBETRAGSKUERZUNG;
require_once ('awisDatenbank.inc');
require_once ('awisFormular.inc');
require_once ('awisFilialen.inc');

try {
	// Textkonserven laden
	$TextKonserven = array ();
	$TextKonserven [] = array ('SGD', '*' );
	$TextKonserven [] = array ('SGK', '*' );
	$TextKonserven [] = array ('Wort', 'lbl_weiter' );
	$TextKonserven [] = array ('Wort', 'lbl_speichern' );
	$TextKonserven [] = array ('Wort', 'lbl_zurueck' );
	$TextKonserven [] = array ('Wort', 'lbl_hilfe' );
	$TextKonserven [] = array ('Wort', 'lbl_trefferliste' );
	$TextKonserven [] = array ('Wort', 'lbl_aendern' );
	$TextKonserven [] = array ('Wort', 'lbl_hinzufuegen' );
	$TextKonserven [] = array ('Wort', 'lbl_loeschen' );
	$TextKonserven [] = array ('Liste', 'lst_JaNein' );
	$TextKonserven [] = array ('Fehler', 'err_keineDaten' );
	$TextKonserven [] = array ('Fehler', 'err_keineRechte' );
	$TextKonserven [] = array ('Fehler', 'err_keineDatenbank' );
	
	$Form = new awisFormular ();
	$AWISBenutzer = awisBenutzer::Init ();
	$DB = awisDatenbank::NeueVerbindung ( 'AWIS' );
	$DB->Oeffnen ();
	
	$AWISSprachKonserven = $Form->LadeTexte ( $TextKonserven );
	$Recht9600 = $AWISBenutzer->HatDasRecht ( 9600 );
	
	if ($Recht9600 == 0) {
		$DB->ProtokollEintrag ( awisDatenbank::EREIGNIS_FEHLER, 1000, $AWISBenutzer->BenutzerName (), 'Folienaustausch - Modul', $AWISBenutzer->BenutzerName () );
		echo "<span class=HinweisText>" . $AWISSprachKonserven ['Fehler'] ['err_keineRechte'] . "</span>";
		die ();
	}
	
	//$Form->DebugAusgabe(1,$_POST);
	

	$Form->SchreibeHTMLCode ( "<form name=frmWeiter method=post action=./sgh_Main.php?cmdAktion=Details>" );
	$Form->Formular_Start ();
	
	$DetailAnsichtSGH = false;
	$DetailAnsichtKassendaten = false;
	$AnzahlDatensaetze = 0;
	
	if (isset ( $_POST ['cmdSpeichern_x'] )) {
		include './sgh_speichern.php';
		
		$TextKonserven = array ();
		$TextKonserven [] = array ('SGD', '*' );
		$TextKonserven [] = array ('SGK', '*' );
		$TextKonserven [] = array ('Wort', 'lbl_weiter' );
		$TextKonserven [] = array ('Wort', 'lbl_speichern' );
		$TextKonserven [] = array ('Wort', 'lbl_zurueck' );
		$TextKonserven [] = array ('Wort', 'lbl_hilfe' );
		$TextKonserven [] = array ('Wort', 'lbl_trefferliste' );
		$TextKonserven [] = array ('Wort', 'lbl_aendern' );
		$TextKonserven [] = array ('Wort', 'lbl_hinzufuegen' );
		$TextKonserven [] = array ('Wort', 'lbl_loeschen' );
		$TextKonserven [] = array ('Liste', 'lst_JaNein' );
		$TextKonserven [] = array ('Fehler', 'err_keineDaten' );
		$TextKonserven [] = array ('Fehler', 'err_keineRechte' );
		$TextKonserven [] = array ('Fehler', 'err_keineDatenbank' );
		
		if ($FLAGKEINEAUSWAHL == true and $FLAGBETRAGSKUERZUNG == false and $DETAILANSICHTKASSENDATEN == false) {
			$Form->Hinweistext ( 'ES wurde kein Kassendatensatz ausgewählt', 1 );
			$Form->ZeileEnde ();
			$Form->ZeileStart ();
			
			$DetailAnsichtSGH = false;
			$DetailAnsichtKassendaten = true;
			$flag = true;
			
			$Key = $_POST ["txtKDSKEY"];
			
			$AWISSprachKonserven = $Form->LadeTexte ( $TextKonserven );
		} elseif ($FLAGKEINEAUSWAHL == true and $FLAGBETRAGSKUERZUNG == false and $DETAILANSICHTKASSENDATEN == true) {
			$DetailAnsichtSGH = false;
			$DetailAnsichtKassendaten = true;
			$flag = true;
			
			$Key = $_POST ["txtKDSKEY"];
			
			$AWISSprachKonserven = $Form->LadeTexte ( $TextKonserven );
		} elseif ($FLAGBETRAGSKUERZUNG == true) {
			
			$DetailAnsichtSGH = false;
			$DetailAnsichtKassendaten = false;
			$FLAGBETRAGSKUERZUNG == true;
			$flag = true;
			
			$AWISSprachKonserven = $Form->LadeTexte ( $TextKonserven );
		
		} else {
			if ($FLAGBETRAGSKUERZUNG != true) {
				$DetailAnsichtSGH = true;
				$DetailAnsichtKassendaten = false;
				$AWISSprachKonserven = $Form->LadeTexte ( $TextKonserven );
			}
		
		}
		$Form->DebugAusgabe ( 1, $_POST );
	
	}
	
	if (isset ( $_POST ['cmdWeiter_x'] )) {
		$flag = false;
		
		foreach ( $_POST as $key => $value ) {
			if ($_POST [$key] == 'on') {
				$Felder = explode ( ';', $Form->NameInArray ( $_POST, 'txtAuswahl_', 1, 1 ) );
				$Form->DebugAusgabe ( 1, $Felder );
				$ZeichenLaenge = strlen ( $Felder [0] );
				
				$ZeichenLaenge - 10;
				
				$Key = substr ( $Felder [0], 11, $ZeichenLaenge );
				
				$DetailAnsichtSGH = false;
				$DetailAnsichtKassendaten = true;
				$flag = true;
			} else {
				if ($flag == true) {
				
				} else {
					$flag = false;
				}
			}
		}
		
		if ($flag == false) {
			foreach ( $_POST as $key => $value ) {
				$_POST [$key] = '';
			}
			$DetailAnsichtSGH = false;
			$DetailAnsichtKassendaten = false;
		}
	
	}
	
	if (! empty ( $_POST )) {
		if ($_POST ['txtSGD_RNRSGH'] != '' && $DetailAnsichtKassendaten == false && $FLAGBETRAGSKUERZUNG == false) {
			$SQL = "Select * from SGHDIENSTLEISTER ";
			$SQL .= " WHERE SGD_RNRSGH=" . $Form->Format ( 'T', $_POST ['txtSGD_RNRSGH'] );
			$SQL .= " ORDER BY SGD_FILNR, SGD_WANR";
			
			//echo $SQL;
			

			$rsSGD = $DB->RecordSetOeffnen ( $SQL );
			
			if ($rsSGD->AnzahlDatensaetze () > 0) {
				$DetailAnsichtSGH = true;
				$Param ['SGDRNRSGH'] = $_POST ['txtSGD_RNRSGH'];
				
				//$Form->DebugAusgabe(1,$Param);
				$AWISBenutzer->ParameterSchreiben ( "SGHABGLEICH", serialize ( $Param ) );
				
				$AnzahlDatensaetze = $rsSGD->AnzahlDatensaetze ();
				$Form->Erstelle_HiddenFeld ( 'AnzahlDatensaetze', $AnzahlDatensaetze );
			
			} else {
				$DetailAnsichtSGH = false;
			}
		}
	} else {
	
	}
	
	if ($DetailAnsichtSGH == false && $DetailAnsichtKassendaten == false && $FLAGBETRAGSKUERZUNG == false) {
		$Form->Trennzeile ( 'O' );
		$Form->Erstelle_TextLabel ( "Rechnungsnr:", 120 );
		$Form->Erstelle_TextFeld ( 'SGD_RNRSGH', '', '', 500, true );
	
	} elseif ($DetailAnsichtSGH == true) {
		
		$Form->Trennzeile ( 'O' );
		$Form->Erstelle_TextLabel ( "Rechnungsnr:", 120 );
		$Form->Erstelle_TextFeld ( 'SGD_RNRSGH', $rsSGD->FeldInhalt ( "SGD_RNRSGH" ), '', 500 );
		$Form->Erstelle_HiddenFeld ( 'SGD_RNRSGH', $_POST ['txtSGD_RNRSGH'] );
		$Form->Trennzeile ( 'O' );
		
		$Form->ZeileStart ();
		$Form->Erstelle_TextLabel ( '', 20 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDKEY'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDRNRSGH'], 130 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDFILNR'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDWANR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDEINBAUDATUM'], 120 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDATUNR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDMENGE'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDBETRAG'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDKFZKENNZ'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDMAPPE'], 150 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDLIEFERANTENNR'], 100 );
		
		$DS = 0;
		
		while ( ! $rsSGD->EOF () ) {
			
			$Form->ZeileStart ();
			$Form->Erstelle_Checkbox ( 'Auswahl_' . $rsSGD->FeldInhalt ( 'SGD_KEY' ), 'off', 20, true, 'on', '' );
			$Form->Erstelle_ListenFeld ( 'SGD_KEY', $rsSGD->FeldInhalt ( 'SGD_KEY' ), 10, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_RNRSGH', $rsSGD->FeldInhalt ( 'SGD_RNRSGH' ), 16, 130, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_FILNR', $rsSGD->FeldInhalt ( 'SGD_FILNR' ), 4, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_WANR', $rsSGD->FeldInhalt ( 'SGD_WANR' ), 10, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_EINBAUDATUM', substr ( $rsSGD->FeldInhalt ( 'SGD_EINBAUDATUM' ), 0, 10 ), 14, 120, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_ATUNR', $rsSGD->FeldInhalt ( 'SGD_ATUNR' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_MENGE', $rsSGD->FeldInhalt ( 'SGD_MENGE' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_BETRAG', $Form->Format ('N2',$rsSGD->FeldInhalt('SGD_BETRAG')), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_KFZKENNZ', $rsSGD->FeldInhalt ( 'SGD_KFZKENNZ' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_MAPPE', $rsSGD->FeldInhalt ( 'SGD_MAPPE' ), 14, 150, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_LIEFERANTENNR', $rsSGD->FeldInhalt ( 'SGD_LIEFERANTENNR' ), 14, 100, false, ($DS % 2) );
			$Form->ZeileEnde ();
			
			$DS ++;
			$rsSGD->DSWeiter ();
		}
	
	} elseif ($DetailAnsichtKassendaten == true && $DetailAnsichtSGH == false && $FLAGBETRAGSKUERZUNG == false) {
		$SQL = "Select * from SGHDIENSTLEISTER WHERE SGD_KEY=" . $DB->FeldInhaltFormat ( 'NO', $Key );
		$rsDienstleister = $DB->RecordSetOeffnen ( $SQL );
		
		$Form->Erstelle_TextLabel ( 'Ausgewählter SGHDatensatz', 250 );
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		$Form->Erstelle_TextLabel ( '', 20 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDKEY'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDRNRSGH'], 130 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDFILNR'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDWANR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDEINBAUDATUM'], 120 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDATUNR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDMENGE'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDBETRAG'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDKFZKENNZ'], 100 );
		
		$DS = 0;
		
		while ( ! $rsDienstleister->EOF () ) {
			$Form->ZeileStart ();
			
			$Form->Erstelle_TextLabel ( '', 20 );
			$Form->Erstelle_ListenFeld ( 'SGD_KEY', $rsDienstleister->FeldInhalt ( 'SGD_KEY' ), 10, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_RNRSGH', $rsDienstleister->FeldInhalt ( 'SGD_RNRSGH' ), 16, 130, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_FILNR', $rsDienstleister->FeldInhalt ( 'SGD_FILNR' ), 4, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_WANR', $rsDienstleister->FeldInhalt ( 'SGD_WANR' ), 10, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_EINBAUDATUM', substr ( $rsDienstleister->FeldInhalt ( 'SGD_EINBAUDATUM' ), 0, 10 ), 14, 120, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_ATUNR', $rsDienstleister->FeldInhalt ( 'SGD_ATUNR' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_MENGE', $rsDienstleister->FeldInhalt ( 'SGD_MENGE' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_BETRAG', $Form->Format ( 'N2', $rsDienstleister->FeldInhalt ( 'SGD_BETRAG' ) ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_KFZKENNZ', $rsDienstleister->FeldInhalt ( 'SGD_KFZKENNZ' ), 14, 100, false, ($DS % 2) );
			
			$Form->ZeileEnde ();
			$DS ++;
			$rsDienstleister->DSWeiter ();
		}
		
		$rsDienstleister->DSErster ();
		
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		
		$Form->Erstelle_TextLabel ( 'Auswahl Kassendatensatz', 200 );
		
		$SQL = "Select * from SGHKASSENDATEN ";
		$SQL .= " WHERE SGK_FILID=" . $DB->FeldInhaltFormat ( 'NO', $rsDienstleister->FeldInhalt ( 'SGD_FILNR' ) );
		$SQL .= " ORDER BY SGK_WANR";
		
		//echo $SQL;
		

		$rsKassendaten = $DB->RecordSetOeffnen ( $SQL );
		
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		
		$Form->Erstelle_TextLabel ( '', 20 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKKEY'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKFILNR'], 60 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKDATUM'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKZEIT'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKBSA'], 60 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKWANR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDWAGENDAT'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKARTNR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKMENGE'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtATUEK'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKUMSATZ'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKKFZKENNZ'], 100 );
		
		while ( ! $rsKassendaten->EOF () ) {
			$Form->ZeileStart ();
			
			$SQL  = 'SELECT *';
			$SQL .= ' FROM SGHARTPFLEGE';
			$SQL .= ' WHERE SSD_ARTNR = ' .$DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ARTNR'));
			$SQL .= " AND TRUNC(TO_DATE('" .$rsKassendaten->FeldInhalt('SGK_WAGENDAT') ."','DD.MM.YYYY HH24:MI:SS'))";
			$SQL .= ' >= TRUNC(SSD_GUELTIGAB)';
			$SQL .= " AND TRUNC(TO_DATE('" .$rsKassendaten->FeldInhalt('SGK_WAGENDAT') ."','DD.MM.YYYY HH24:MI:SS'))";
			$SQL .= ' <= TRUNC(SSD_GUELTIGBIS)';
			
			//$Form->DebugAusgabe(1,$SQL);
			
			$rsArtNr = $DB->RecordSetOeffnen ( $SQL );
			
			$SGHEK = 0.0;
			$ATUVKLIST = 0.0;
			$ATUEK = 0.0;
			$ATUVK = 0.0;
			
			$SGHEK = $rsDienstleister->FeldInhalt ( 'SGD_BETRAG' );
			$ATUEK = $rsArtNr->FeldInhalt ( 'SSD_EK' );
			
			$ATUVK = $rsKassendaten->FeldInhalt ( 'SGK_UMSATZ' );
			$ATUVKLIST = $rsArtNr->FeldInhalt ( 'SSD_VK' );
			
			$Form->Erstelle_Checkbox ( 'Auswahl_' . $rsKassendaten->FeldInhalt ( 'SGK_KEY' ), 'off', 20, true, 'on', '' );
			$Form->Erstelle_ListenFeld ( 'SGK_KEY', $rsKassendaten->FeldInhalt ( 'SGK_KEY' ), 10, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_FILNR', $rsKassendaten->FeldInhalt ( 'SGK_FILID' ), 16, 60, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_DATUM', substr ( $rsKassendaten->FeldInhalt ( 'SGK_DATUM' ), 0, 10 ), 4, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_ZEIT', $rsKassendaten->FeldInhalt ( 'SGK_ZEIT' ), 10, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_BSA', $rsKassendaten->FeldInhalt ( 'SGK_BSA' ), 14, 60, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_WANR', $rsKassendaten->FeldInhalt ( 'SGK_WANR' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_WAGENDAT', substr ( $rsKassendaten->FeldInhalt ( 'SGK_WAGENDAT' ), 0, 10), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_ARTNR', $rsKassendaten->FeldInhalt ( 'SGK_ARTNR' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_MENGE', $rsKassendaten->FeldInhalt ( 'SGK_MENGE' ), 14, 100, false, ($DS % 2) );
			
			if ($SGHEK == $ATUEK) {
				//Formatiere Grün
				$Form->Erstelle_ListenFeld ( 'SGK_ATUEK', $Form->Format ( 'N2', $rsArtNr->FeldInhalt ( 'SSD_EK' ) ), 14, 50, false, ($DS % 2), "color:green",'','T','R' );
			} else {
				$Form->Erstelle_ListenFeld ( 'SGK_ATUEK', $Form->Format ( 'N2', $rsArtNr->FeldInhalt ( 'SSD_EK' ) ), 14, 50, false, ($DS % 2), "color:red",'','T','R' );
			}
			if ($ATUVK == $ATUVKLIST) {
				$Form->Erstelle_ListenFeld ( 'SGK_UMSATZ', $Form->Format ( 'N2', $rsKassendaten->FeldInhalt ( 'SGK_UMSATZ' )), 14, 100, false, ($DS % 2), "color:green",'','T','R' );
				$Form->Erstelle_ListenFeld('FILLER', ' ', 14, 40, false, ($DS % 2));
			} else {
				$Form->Erstelle_ListenFeld ( 'SGK_UMSATZ', $Form->Format ( 'N2', $rsKassendaten->FeldInhalt ( 'SGK_UMSATZ' )), 14, 100, false, ($DS % 2), "color:red",'','T','R' );
				$Form->Erstelle_ListenFeld('FILLER', ' ', 14, 40, false, ($DS % 2));
			}
			$Form->Erstelle_ListenFeld ( 'SGK_KFZKENNZ', $rsKassendaten->FeldInhalt ( 'SGK_KFZKENNZ' ), 14, 100, false, ($DS % 2) );
			
			$Form->ZeileEnde ();
			$DS ++;
			
			$rsKassendaten->DSWeiter ();
		}
		
		//Auswahl Grund:
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		$Form->Erstelle_TextLabel ( 'Abgleichsgrund', 200 );
		$Form->Trennzeile ( 'O' );
		$SQLGRUENDE = "Select SGG_KEY,SGG_GRUENDE from SGHABGLEICHSGRUENDE WHERE SGG_KUERZEL <> 'A'";
		
		$Form->Erstelle_SelectFeld ( 'Abgleichsgrund', '', 70, True, $SQLGRUENDE, '', '', '1', '', '', '' );
		$Form->Trennzeile ( 'O' );
		
		$Form->ZeileStart ();
		$Form->Erstelle_Checkbox ( 'AuswahlBegruendung', 'off', 20, true, 'on', '' );
		$Form->Erstelle_TextLabel ( 'Begruendung forcieren', 200 );
		$Form->ZeileEnde ();
		$Form->Trennzeile ( 'O' );
		$Form->ZeileStart ();
		$Form->Erstelle_Textarea ( 'Begruendung', '', 500, 113, 7, True );
		$Form->ZeileEnde ();
	
	} elseif ($FLAGBETRAGSKUERZUNG == true) {
		$SQL = "Select * from SGHDIENSTLEISTER WHERE SGD_KEY=" . $DB->FeldInhaltFormat ( 'NO', $_POST ['txtKDSKEY'] );
		$rsDienstleister = $DB->RecordSetOeffnen ( $SQL );
		
		$Form->Erstelle_TextLabel ( 'Ausgewählter SGHDatensatz', 250 );
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDKEY'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDRNRSGH'], 130 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDFILNR'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDWANR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDEINBAUDATUM'], 120 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDATUNR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDMENGE'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDBETRAG'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGD'] ['txtSGDKFZKENNZ'], 100 );
		
		$DS = 0;
		
		while ( ! $rsDienstleister->EOF () ) {
			$Form->ZeileStart ();
			
			$Form->Erstelle_ListenFeld ( 'SGD_KEY', $rsDienstleister->FeldInhalt ( 'SGD_KEY' ), 10, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_RNRSGH', $rsDienstleister->FeldInhalt ( 'SGD_RNRSGH' ), 16, 130, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_FILNR', $rsDienstleister->FeldInhalt ( 'SGD_FILNR' ), 4, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_WANR', $rsDienstleister->FeldInhalt ( 'SGD_WANR' ), 10, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_EINBAUDATUM', substr ( $rsDienstleister->FeldInhalt ( 'SGD_EINBAUDATUM' ), 0, 10 ), 14, 120, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_ATUNR', $rsDienstleister->FeldInhalt ( 'SGD_ATUNR' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_MENGE', $rsDienstleister->FeldInhalt ( 'SGD_MENGE' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_BETRAG', $rsDienstleister->FeldInhalt ( 'SGD_BETRAG' ), 14, 100, true, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGD_KFZKENNZ', $rsDienstleister->FeldInhalt ( 'SGD_KFZKENNZ' ), 14, 100, false, ($DS % 2) );
			
			$Form->ZeileEnde ();
			$DS ++;
			$rsDienstleister->DSWeiter ();
		}
		
		$rsDienstleister->DSErster ();
		
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		
		$Form->Erstelle_TextLabel ( 'Auswahl Kassendatensatz', 200 );
		
		$SQL = "Select * from SGHKASSENDATEN WHERE SGK_KEY=" . $DB->FeldInhaltFormat ( 'NO', $Key );
		
		//echo $SQL;
		

		$rsKassendaten = $DB->RecordSetOeffnen ( $SQL );
		
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKKEY'], 50 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKFILNR'], 60 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKDATUM'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKZEIT'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKBSA'], 60 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKWANR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKWAGENDAT'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKARTNR'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKMENGE'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKUMSATZ'], 100 );
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['SGK'] ['txtSGKKFZKENNZ'], 100 );
		
		while ( ! $rsKassendaten->EOF () ) {
			$Form->ZeileStart ();
			
			$SQL  = 'SELECT *';
			$SQL .= ' FROM SGHARTPFLEGE';
			$SQL .= ' WHERE SSD_ARTNR = ' .$DB->FeldInhaltFormat('T',$rsKassendaten->FeldInhalt('SGK_ARTNR'));
			$SQL .= " AND TRUNC(TO_DATE('" .$rsKassendaten->FeldInhalt('SGK_WAGENDAT') ."','DD.MM.YYYY HH24:MI:SS'))";
			$SQL .= ' >= TRUNC(SSD_GUELTIGAB)';
			$SQL .= " AND TRUNC(TO_DATE('" .$rsKassendaten->FeldInhalt('SGK_WAGENDAT') ."','DD.MM.YYYY HH24:MI:SS'))";
			$SQL .= ' <= TRUNC(SSD_GUELTIGBIS)';
						
			//$Form->DebugAusgabe(1,$SQL);
			
			$rsArtNr = $DB->RecordSetOeffnen ( $SQL );
			
			$SGHEK = 0.0;
			$ATUVKLIST = 0.0;
			$ATUEK = 0.0;
			$ATUVK = 0.0;
			
			$SGHEK = $rsDienstleister->FeldInhalt ( 'SGD_BETRAG' );
			$ATUEK = $rsArtNr->FeldInhalt ( 'SSD_EK' );
			
			$ATUVK = $rsKassendaten->FeldInhalt ( 'SGK_UMSATZ' );
			$ATUVKLIST = $rsArtNr->FeldInhalt ( 'SSD_VK' );
			
			$Form->Erstelle_ListenFeld ( 'SGK_KEY', $rsKassendaten->FeldInhalt ( 'SGK_KEY' ), 10, 50, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_FILNR', $rsKassendaten->FeldInhalt ( 'SGK_FILID' ), 16, 60, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_DATUM', substr ( $rsKassendaten->FeldInhalt ( 'SGK_DATUM' ), 0, 10 ), 4, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_ZEIT', $rsKassendaten->FeldInhalt ( 'SGK_ZEIT' ), 10, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_BSA', $rsKassendaten->FeldInhalt ( 'SGK_BSA' ), 14, 60, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_WANR', $rsKassendaten->FeldInhalt ( 'SGK_WANR' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_WANR', substr ( $rsKassendaten->FeldInhalt ( 'SGK_WAGENDAT' ), 0, 10), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_ARTNR', $rsKassendaten->FeldInhalt ( 'SGK_ARTNR' ), 14, 100, false, ($DS % 2) );
			$Form->Erstelle_ListenFeld ( 'SGK_MENGE', $rsKassendaten->FeldInhalt ( 'SGK_MENGE' ), 14, 100, false, ($DS % 2) );
			
			if ($SGHEK == $ATUEK) {
				$Form->Erstelle_ListenFeld ( 'SGK_ATUEK', $Form->Format ( 'N2', $rsArtNr->FeldInhalt ( 'SSD_EK' ) ), 14, 100, false, ($DS % 2), "color:green",'','T','R' );
			} else {
				$Form->Erstelle_ListenFeld ( 'SGK_ATUEK', $Form->Format ( 'N2', $rsArtNr->FeldInhalt ( 'SSD_EK' ) ), 14, 100, false, ($DS % 2), "color:red",'','T','R' );
			}
			
			if ($ATUVK == $ATUVKLIST) {
				$Form->Erstelle_ListenFeld ( 'SGK_UMSATZ', $Form->Format ( 'N2', $rsKassendaten->FeldInhalt ( 'SGK_UMSATZ' )), 14, 100, false, ($DS % 2), "color:green",'','T','R' );
				$Form->Erstelle_ListenFeld('FILLER', ' ', 14, 40, false, ($DS % 2));
			} else {
				$Form->Erstelle_ListenFeld ( 'SGK_UMSATZ', $Form->Format ( 'N2', $rsKassendaten->FeldInhalt ( 'SGK_UMSATZ' )), 14, 100, false, ($DS % 2), "color:red",'','T','R' );
				$Form->Erstelle_ListenFeld('FILLER', ' ', 14, 40, false, ($DS % 2));
			}
			$Form->Erstelle_ListenFeld ( 'SGK_KFZKENNZ', $rsKassendaten->FeldInhalt ( 'SGK_KFZKENNZ' ), 14, 100, false, ($DS % 2) );
			
			$Form->ZeileEnde ();
			$DS ++;
			
			$rsKassendaten->DSWeiter ();
		}
		$Form->ZeileStart ();
		$Form->Trennzeile ( 'O' );
		$Form->Erstelle_TextLabel ( 'Abgleichsgrund:', 200 );
		$Form->Trennzeile ( 'O' );
		echo "<span class=HinweisText>Betragskürzung</span>";
		
		$Form->ZeileStart ();
		
		if (isset ( $_POST ['txtAuswahlBegruendung'] )) {
			if ($_POST ['txtAuswahlBegruendung'] == 'on') {
				$Form->Erstelle_Checkbox ( 'AuswahlBegruendung', 'on', 20, true, 'on', '' );
			} else {
				$Form->Erstelle_Checkbox ( 'AuswahlBegruendung', 'off', 20, true, 'on', '' );
			}
		} else {
			$Form->Erstelle_Checkbox ( 'AuswahlBegruendung', 'off', 20, true, 'on', '' );
		}
		
		$Form->Erstelle_TextLabel ( 'Begruendung forcieren', 200 );
		$Form->ZeileEnde ();
		$Form->Trennzeile ( 'O' );
		$Form->ZeileStart ();
		
		if (isset ( $_POST ['txtBegruendung'] )) {
			if ($_POST ['txtBegruendung'] != "" && $_POST ['txtAuswahlBegruendung'] == 'on') {
				$Form->Erstelle_Textarea ( 'Begruendung', $_POST ['txtBegruendung'], 500, 113, 7, True );
			} else {
				$Form->Erstelle_Textarea ( 'Begruendung', '', 500, 113, 7, True );
			}
		} else {
			$Form->Erstelle_Textarea ( 'Begruendung', '', 500, 113, 7, True );
		}
		$Form->ZeileEnde ();
	
	}
	
	$Form->Trennzeile ( 'O' );
	$Form->Formular_Ende ();
	
	$Form->SchaltflaechenStart ();
	
	$Form->Schaltflaeche ( 'href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven ['Wort'] ['lbl_zurueck'], 'Z' );
	
	if (($Recht9600 & 2) != 0) {
		//$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if ($DetailAnsichtSGH == false && $DetailAnsichtKassendaten == false && $FLAGBETRAGSKUERZUNG != true) {
		$Form->Schaltflaeche ( 'image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven ['Wort'] ['lbl_weiter'], 'W' );
	} elseif ($DetailAnsichtSGH == true && $DetailAnsichtKassendaten == false && $FLAGBETRAGSKUERZUNG != true) {
		$Form->Schaltflaeche ( 'image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven ['Wort'] ['lbl_weiter'], 'W' );
		$Form->Erstelle_HiddenFeld ( 'DetailAnsicht', True );
	
	} elseif ($DetailAnsichtKassendaten == True && $FLAGBETRAGSKUERZUNG != true) {
		$Form->Schaltflaeche ( 'image', 'cmdZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven ['Wort'] ['lbl_zurueck'], 'Z' );
		$Form->Erstelle_HiddenFeld ( 'SGD_RNRSGH', $_POST ['txtSGD_RNRSGH'] );
		$Form->Schaltflaeche ( 'image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven ['Wort'] ['lbl_speichern'], 'S' );
		$Form->Erstelle_HiddenFeld ( 'KDSKEY', $Key );
	} elseif ($DetailAnsichtSGH == false && $DetailAnsichtKassendaten == false && $FLAGBETRAGSKUERZUNG == true) {
		$Form->Schaltflaeche ( 'image', 'cmdZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven ['Wort'] ['lbl_zurueck'], 'Z' );
		$Form->Erstelle_HiddenFeld ( 'SGD_RNRSGH', $_POST ['txtSGD_RNRSGH'] );
		$Form->Schaltflaeche ( 'image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven ['Wort'] ['lbl_speichern'], 'S' );
		$Form->Erstelle_HiddenFeld ( 'KDSKEY', $Key );
		$Form->Erstelle_HiddenFeld ( 'SGHKEY', $_POST ['txtKDSKEY'] );
		$Form->Erstelle_Hiddenfeld ( 'AbgleichsgrundSpeichern', 8 );
	}
	
	$Form->SchaltflaechenEnde ();
	
	$Form->SchreibeHTMLCode ( '</form>' );
	
	if ($AWISCursorPosition != '') {
		$Form->SchreibeHTMLCode ( '<Script Language=JavaScript>' );
		$Form->SchreibeHTMLCode ( "document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();" );
		$Form->SchreibeHTMLCode ( '</Script>' );
	}
} catch ( Exception $ex ) {
	if ($Form instanceof awisFormular) {
		$Form->Fehler_Anzeigen ( 'INTERN', $ex->getMessage (), 'MELDEN', 6, "200812180922" );
	} else {
		echo 'allg. Fehler:' . $ex->getMessage ();
	}
}

?>