<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PAF','%');
	$TextKonserven[]=array('PFA','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8005 = $AWISBenutzer->HatDasRecht(8005);
	if($Recht8005==0)
	{
	    awisEreignis(3,1000,'Zukauflieferscheine',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_8005'));
	$Filialen = $AWISBenutzer->FilialZugriff('',awisBenutzer::FILIALZUGRIFF_STRING);
	if($Filialen == '' AND isset($_GET['FIL_ID']))
	{
		$Filialen = $_GET['FIL_ID'];
	}
	elseif($Filialen == '' AND isset($_POST['txtFIL_ID']))
	{
		$Filialen = $_POST['txtFIL_ID'];
	}

	if(empty($Param))
	{
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='PAF_DATUM DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./preiserfassung_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_8005'));
	}
	elseif(isset($_GET['PAF_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PAF_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_8005'));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{		
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='PAF_DATUM DESC';
			$AWISBenutzer->ParameterSchreiben('Formular_8005',serialize($Param));
		}
		
		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		if(isset($_GET['Seite']))
		{
			$AWIS_KEY1=$Param['KEY'];
		}
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY PAF_DATUM DESC';
			$Param['ORDER']='PAF_DATUM DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$Bedingung .= ' AND PAF_STATUS = 10';		// Nur die offenen Abfragen
	if($Filialen!='')			// F�r die Filialen einschr�nken
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM PreisAbfragenPreise WHERE PFA_FIL_ID IN ('.$Filialen.') AND PFA_PAF_KEY = PAF_KEY)';
	}

	$SQL = 'SELECT Preisabfragen.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Preisabfragen';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_8005',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsPAF = $DB->RecordsetOeffnen($SQL);

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmPreisabfragen action=./preiserfassung_Main.php?cmdAktion=Preisabfragen method=POST enctype="multipart/form-data">');

	if($rsPAF->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Param['BLOCK']=1;
	}
	elseif($rsPAF->AnzahlDatensaetze()>1 AND !isset($_GET['PAF_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './preiserfassung_Main.php?cmdAktion=Preisabfragen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PAF_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PAF_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAF']['PAF_DATUM'],130,'',$Link);
		$Link = './preiserfassung_Main.php?cmdAktion=Preisabfragen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PAF_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PAF_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAF']['PAF_BEZEICHNUNG'],390,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsPAF->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './preiserfassung_Main.php?cmdAktion=Preisabfragen&PAF_KEY=0'.$rsPAF->FeldInhalt('PAF_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('#PAF_DATUM',$rsPAF->FeldInhalt('PAF_DATUM'),0,130,false,($DS%2),'',$Link,'D');
			$Form->Erstelle_ListenFeld('#PAF_BEZEICHNUNG',$rsPAF->FeldInhalt('PAF_BEZEICHNUNG'),0,395,false,($DS%2),'',$Link);

			$Form->ZeileEnde();

			$rsPAF->DSWeiter();
			$DS++;
		}

		$Link = './preiserfassung_Main.php?cmdAktion=Preisabfragen';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Abfrage
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsPAF->FeldInhalt('PAF_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_8005',serialize($Param));

		$Form->Erstelle_HiddenFeld('PAF_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./preiserfassung_Main.php?cmdAktion=Preisabfragen&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPAF->FeldInhalt('PAF_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPAF->FeldInhalt('PAF_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht8005&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht8005&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAF']['PAF_DATUM'].':',200);
		$Form->Erstelle_TextFeld('PAF_DATUM',$rsPAF->FeldInhalt('PAF_DATUM'),10,200,false,'','','','D');
		$AWISCursorPosition = 'txtPAF_DATUM';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAF']['PAF_BEZEICHNUNG'].':',200,'','');
		$Form->Erstelle_TextFeld('PAF_BEZEICHNUNG',$rsPAF->FeldInhalt('PAF_BEZEICHNUNG'),60,300,false);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAF']['PAF_BEMERKUNG'].':',200,'','');
		$Form->Erstelle_Textarea('PAF_BEMERKUNG',$rsPAF->FeldInhalt('PAF_BEMERKUNG'),800,100,5,false);
		$Form->ZeileEnde();


		if($rsPAF->FeldInhalt('PAF_KEY')!='')
		{
			$SQL = 'SELECT count(*) as ANZ';
			$SQL .= ' FROM Preisabfragenpreise';
			$SQL .= ' WHERE PFA_PAF_KEY = '.$AWIS_KEY1;
			$rsPFA = $DB->RecordSetOeffnen($SQL);
			if($rsPFA->FeldInhalt('ANZ')==0)
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PAF']['PAF_PREISDATEINAME'].':',200,'','');
				$Form->Erstelle_DateiUpload('PREISDATEI',300,30,10000000,'');
				$Form->ZeileEnde();
			}
			else
			{
				$SQL = 'SELECT count(*) AS GESAMT';
				$SQL .= ', SUM(CASE WHEN (PFA_ERMITTLUNGSSTATUS=1 AND PFA_PREIS IS NULL) THEN 1 ELSE 0 END) AS OFFEN';
				$SQL .= ' FROM PreisAbfragenPreise ';
				$SQL .= ' WHERE PFA_PAF_KEY = 0'.$AWIS_KEY1;
				if($Filialen != '')
				{
					$SQL .= ' AND PFA_FIL_ID IN ('.$Filialen.')';
				}
				$rsPFA = $DB->RecordSetOeffnen($SQL);
				$DS = 0;
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PFA']['PFA_GESAMT'].':',200);
				$Form->Erstelle_TextFeld('#GESAMT',$rsPFA->FeldInhalt('GESAMT'),10,300);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PFA']['PFA_OFFEN'].':',200);
				$Form->Erstelle_TextFeld('#FIL',$rsPFA->FeldInhalt('OFFEN'),10,300);
				$Form->ZeileEnde();
			}
		}

		$Form->Formular_Ende();

		//
		if($rsPAF->FeldInhalt('PAF_KEY')!='')
		{
			$Reg = new awisRegister(8006);
			$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
		}
	}

	//awis_Debug(1, $Param, $Bedingung, $rsPAF, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht8005&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht8005&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('href', 'cmdDrucken', './preiserfassung_ausdruck.php?FIL_ID='.$AWIS_KEY2.'&PAF_KEY='.$AWIS_KEY1, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'D');
	}

	if(($Recht8005&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	$AWISBenutzer->ParameterSchreiben('Formular_8005',serialize($Param));
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201615");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201614");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PAF_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['PAF_BEZEICHNUNG']) AND $Param['PAF_BEZEICHNUNG']!='')
	{
		$Bedingung .= ' AND (PAF_BEZEICHNUNG ' . $DB->LIKEoderIST($Param['PAF_BEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER) . '';
		$Bedingung .= ' OR ZLA_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['PAF_BEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['DATUMVOM']) AND $Param['DATUMVOM']!='')
	{
		$Bedingung .= ' AND PAF_DATUM >= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMVOM']) . ' ';
	}
	if(isset($Param['DATUMBIS']) AND $Param['DATUMBIS']!='')
	{
		$Bedingung .= ' AND PAF_DATUM <= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMBIS']) . ' ';
	}

	if(isset($Param['AST_ATUNR']) AND $Param['AST_ATUNR']!='')
	{
		$Bedingung .= ' AND (ZLA_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}

	return $Bedingung;
}
?>