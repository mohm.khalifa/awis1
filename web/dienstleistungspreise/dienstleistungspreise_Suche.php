<?php
/**
 * Suchmaske f�r die Auswahl Dienstleistungspreise
 *
 * @author Henry Ott
 * @copyright ATU
 * @version 20120725
 *
 *
 */
global $CursorFeld;
global $AWISBenutzer;


try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('DPR','%');
	$TextKonserven[]=array('DPF','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_AuswahlSpeichern');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht120=$AWISBenutzer->HatDasRecht(120);
	if($Recht120==0)
	{
	    awisEreignis(3,1000,'MBW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./dienstleistungspreise_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Dienstleistungspreise'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['DPR']['DPR_BEZEICHNUNG'].':',190);
	$Form->Erstelle_TextFeld('*DPR_BEZEICHNUNG',($Param['SPEICHERN']=='on'?$Param['DPR_BEZEICHNUNG']:''),25,200,true);
	$AWISCursorPosition='sucDPR_BEZEICHNUNG';
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['DPR']['DPR_NUMMER'].':',190);
	$Form->Erstelle_TextFeld('*DPR_NUMMER',($Param['SPEICHERN']=='on'?$Param['DPR_NUMMER']:''),25,200,true);
	$AWISCursorPosition='sucDPR_NUMMER';
	$Form->ZeileEnde();
	
	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['DPR']['txt_PreisAuswahl'].':',500);
	$Form->ZeileEnde();
	
	$Filialen = explode(';',$AWISBenutzer->ParameterLesen('DienstleistungsPreisFilialen'));
	foreach($Filialen AS $Filiale)
	{
		$FilialTeile = explode('=',$Filiale);
		$SQL = 'SELECT * FROM Laender WHERE LAN_CODE='.$DB->FeldInhaltFormat('T',$FilialTeile[0]);
		$rsLAN = $DB->RecordSetOeffnen($SQL);
		if(!$rsLAN->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($rsLAN->FeldInhalt('LAN_LAND').':',190);
			$Form->Erstelle_Checkbox('*LAND_'.$FilialTeile[0],($Param['SPEICHERN']=='on'?$Param['LAND_'.$FilialTeile[0]]:''),200,true);
			$Form->ZeileEnde();	
		}
	}

	$Form->Trennzeile();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['DPR']['txt_Inaktiv'].':',190);
	$Form->Erstelle_Checkbox('*Inaktiv',($Param['SPEICHERN']=='on'?isset($Param['INAKTIV'])?$Param['INAKTIV']:'':''),200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['DPR']['txt_Gesperrt'].':',190);
	$Form->Erstelle_Checkbox('*Gesperrt',($Param['SPEICHERN']=='on'?isset($Param['GESPERRT'])?$Param['GESPERRT']:'':''),200,true);
	$Form->ZeileEnde();
	
	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?$Param['SPEICHERN']:''),200,true);
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();
	
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->SchaltflaechenEnde();
	$Form->SetzeCursor ( $AWISCursorPosition );
	$Form->SchreibeHTMLCode ('</form>');
}

catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221210");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221211");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>