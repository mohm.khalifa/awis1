<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('MBW','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','INET_AdressenSuche');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht3020 = $AWISBenutzer->HatDasRecht(3020);
	if($Recht3020==0)
	{
	    awisEreignis(3,1000,'MBW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_MBW'));
	$INET_LINK_Telefon =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Telefon');
	$INET_LINK_Karte =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);



	$DetailAnsicht=false;
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['MBW_NAME1'] = $_POST['sucMBW_NAME1'];
		$Param['MBW_STRASSE'] = $_POST['sucMBW_STRASSE'];
		$Param['MBW_PLZ'] = $_POST['sucMBW_PLZ'];
		$Param['MBW_ORT'] = $_POST['sucMBW_ORT'];
		$Param['MBW_LAN_CODE'] = $_POST['sucMBW_LAN_CODE'];
		$Param['MFI_FIL_ID'] = $_POST['sucMFI_FIL_ID'];
		$Param['MFI_ENTFERNUNG'] = $_POST['sucMFI_ENTFERNUNG'];
		$Param['MBW_MBK_KEY'] = $_POST['sucMBW_MBK_KEY'];
		$Param['MBW_MUT_KEY'] = $_POST['txtMBW_MUT_KEY'];		// wg. AJAX Suche

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_MBW",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./mitbewerb_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./mitbewerb_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$AWISBenutzer->ParameterSchreiben('Formular_MBW',serialize($Param));
	}
	elseif(isset($_GET['MBW_KEY']))
	{
		$AWIS_KEY1 = $Form->Format('N0',$_GET['MBW_KEY']);
		$Param['KEY'] = ($AWIS_KEY1<0?'':$AWIS_KEY1);
		$Param['WHERE']='';
		$Param['ORDER']='';
		$AWISBenutzer->ParameterSchreiben('Formular_MBW',serialize($Param));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_MBW',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}
		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************

	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY MBW_NAME1, MBW_NAME2';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	$Param['ORDER']=$ORDERBY;

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT mitbewerber.*';
	$SQL .= ', LAN_CODE, LAN_LAND, MUT_UNTERNEHMENSTYP';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM mitbewerber';
	$SQL .= ' INNER JOIN Laender ON MBW_LAN_CODE = LAN_CODE';
	$SQL .= ' LEFT OUTER JOIN MITBEWERBUTYPENTYPEN ON MBW_MUT_KEY = MUT_KEY';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
//$Form->DebugAusgabe(1,$SQL,$Param);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_MBW',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsMBW = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_MBW',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmmitbewerbs action=./mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').' method=POST>';

	if($rsMBW->EOF() AND !isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=-1)		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsMBW->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
		if($BildschirmBreite<1024)
		{
			$Breiten['MBW_NAME1']=200;
			$Breiten['MBW_ORT']=200;
			$Breiten['MBW_STRASSE']=200;
			$Breiten['MBW_HAUSNUMMER']=50;
		}
		elseif($BildschirmBreite<1280)
		{
			$Breiten['MBW_NAME1']=250;
			$Breiten['MBW_ORT']=250;
			$Breiten['MBW_STRASSE']=250;
			$Breiten['MBW_HAUSNUMMER']=50;
		}
		else
		{
			$Breiten['MBW_NAME1']=300;
			$Breiten['MBW_ORT']=250;
			$Breiten['MBW_STRASSE']=250;
			$Breiten['MBW_HAUSNUMMER']=50;
		}

		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=MBW_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_NAME1'],$Breiten['MBW_NAME1'],'',$Link);
		$Link = './mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=MBW_ORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_ORT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_ORT'],$Breiten['MBW_ORT'],'',$Link);
		$Link = './mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=MBW_STRASSE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_STRASSE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_STRASSE'],$Breiten['MBW_STRASSE'],'',$Link);
		$Link = './mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=MBW_HAUSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_HAUSNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MBW']['MBW_HAUSNUMMER'],$Breiten['MBW_HAUSNUMMER'],'',$Link);
		$Form->Erstelle_Liste_Ueberschrift('',16);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsMBW->EOF())
		{
			$Form->ZeileStart();
			$Link = './mitbewerb_Main.php?cmdAktion=Details&MBW_KEY=0'.$rsMBW->FeldInhalt('MBW_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');

			$Text = (strlen($rsMBW->FeldInhalt('MBW_NAME1'))>(int)($Breiten['MBW_NAME1']/$ListenSchriftFaktor)?substr($rsMBW->FeldInhalt('MBW_NAME1'),0,(int)($Breiten['MBW_NAME1']/$ListenSchriftFaktor)-2).'..':$rsMBW->FeldInhalt('MBW_NAME1'));
			$Form->Erstelle_ListenFeld('MBW_NAME1',$Text,0,$Breiten['MBW_NAME1'],false,($DS%2),'',$Link,'T','L',$rsMBW->FeldInhalt('MBW_NAME1'));

			$Text = (strlen($rsMBW->FeldInhalt('MBW_ORT'))>(int)($Breiten['MBW_ORT']/$ListenSchriftFaktor)?substr($rsMBW->FeldInhalt('MBW_ORT'),0,(int)($Breiten['MBW_NAME1']/$ListenSchriftFaktor)-2).'..':$rsMBW->FeldInhalt('MBW_ORT'));
			$Form->Erstelle_ListenFeld('MBW_ORT',$Text,0,$Breiten['MBW_ORT'],false,($DS%2),'','','','',$rsMBW->FeldInhalt('MBW_ORT'));

			$Text = (strlen($rsMBW->FeldInhalt('MBW_STRASSE'))>(int)($Breiten['MBW_STRASSE']/$ListenSchriftFaktor)?substr($rsMBW->FeldInhalt('MBW_STRASSE'),0,(int)($Breiten['MBW_NAME1']/$ListenSchriftFaktor)-2).'..':$rsMBW->FeldInhalt('MBW_STRASSE'));
			$Form->Erstelle_ListenFeld('MBW_STRASSE',$Text,0,$Breiten['MBW_STRASSE'],false,($DS%2),'','','','',$rsMBW->FeldInhalt('MBW_STRASSE'));

			$Text = (strlen($rsMBW->FeldInhalt('MBW_HAUSNUMMER'))>(int)($Breiten['MBW_HAUSNUMMER']/$ListenSchriftFaktor)?substr($rsMBW->FeldInhalt('MBW_HAUSNUMMER'),0,(int)($Breiten['MBW_NAME1']/$ListenSchriftFaktor)-2).'..':$rsMBW->FeldInhalt('MBW_HAUSNUMMER'));
			$Form->Erstelle_ListenFeld('MBW_HAUSNUMMER',$Text,0,$Breiten['MBW_HAUSNUMMER'],false,($DS%2),'','','','',$rsMBW->FeldInhalt('MBW_HAUSNUMMER'));

			$Parameter = array('$NAME1'=>urlencode($rsMBW->FeldInhalt('MBW_NAME1')),
						   '$NAME2'=>urlencode($rsMBW->FeldInhalt('MBW_NAME2')),
						   '$STRASSE'=>urlencode($rsMBW->FeldInhalt('MBW_STRASSE')),
						   '$HAUSNUMMER'=>urlencode($rsMBW->FeldInhalt('MBW_HAUSNUMMER')),
						   '$PLZ'=>urlencode($rsMBW->FeldInhalt('MBW_PLZ')),
						   '$ORT'=>urlencode($rsMBW->FeldInhalt('MBW_ORT')),
						   '$LAN_CODE'=>urlencode($rsMBW->FeldInhalt('MBW_LAN_CODE'))
						   );
			$Link = strtr($INET_LINK_Karte,$Parameter);
			$Form->Erstelle_ListenBild('href','MAPS',$Link,'/bilder/icon_globus.png',$AWISSprachKonserven['Wort']['INET_AdressenSuche'],($DS%2),'');

/*			// Link f�r die Web-Seite
			$Link = $rsMBW->FeldInhalt('MBW_WEBSEITE');
			if($Link!='')
			{
				$Form->Erstelle_ListenBild('href','ONLINE',$Link,'/bilder/icon_globus_hell.png',$AWISSprachKonserven['MBW']['MBW_WEBSEITE'],($DS%2),'');
			}
*/
			// Link f�r den Shop
			$Link = $rsMBW->FeldInhalt('MBW_ONLINESHOP');
			if($Link!='')
			{
				$Form->Erstelle_ListenBild('href','SHOP',$Link,'/bilder/icon_einkaufswagen.png',$AWISSprachKonserven['MBW']['MBW_ONLINESHOP'],($DS%2),'');
			}

			$Form->ZeileEnde();

			$rsMBW->DSWeiter();
			$DS++;
		}

		$Link = './mitbewerb_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsMBW->FeldInhalt('MBW_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_MBW',serialize($Param));

		echo '<input type=hidden name=txtMBW_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];


			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./mitbewerb_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsMBW->FeldInhalt('MBW_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsMBW->FeldInhalt('MBW_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht3020&2)!=0);

		// �berschrift
		$Form->Erstelle_HiddenFeld('MBW_KEY',$rsMBW->FeldInhalt('MBW_KEY'));

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_NAME1'].':',150);
		$Form->Erstelle_TextFeld('MBW_NAME1',$rsMBW->FeldInhalt('MBW_NAME1'),50,500,$EditRecht,'','','','T');
		$AWISCursorPosition = 'txtMBW_NAME1';
		$Form->ZeileEnde();

		/*
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_NAME2'].':',150);
		$Form->Erstelle_TextFeld('MBW_NAME2',$rsMBW->FeldInhalt('MBW_NAME2'),50,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();
		*/

		$Form->ZeileStart();

		$Parameter = array('$NAME1'=>urlencode($rsMBW->FeldInhalt('MBW_NAME1')),
					   '$NAME2'=>urlencode($rsMBW->FeldInhalt('MBW_NAME2')),
					   '$STRASSE'=>urlencode($rsMBW->FeldInhalt('MBW_STRASSE')),
					   '$HAUSNUMMER'=>urlencode($rsMBW->FeldInhalt('MBW_HAUSNUMMER')),
					   '$PLZ'=>urlencode($rsMBW->FeldInhalt('MBW_PLZ')),
					   '$ORT'=>urlencode($rsMBW->FeldInhalt('MBW_ORT')),
					   '$LAN_CODE'=>urlencode($rsMBW->FeldInhalt('MBW_LAN_CODE'))
					   );
		$Link = strtr($INET_LINK_Karte,$Parameter);



		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_STRASSE'].'/'.$AWISSprachKonserven['MBW']['MBW_HAUSNUMMER'].':',150);
		$Form->Erstelle_TextFeld('MBW_STRASSE',$rsMBW->FeldInhalt('MBW_STRASSE'),40,0,$EditRecht,'','','','T');
		$Form->Erstelle_TextFeld('MBW_HAUSNUMMER',$rsMBW->FeldInhalt('MBW_HAUSNUMMER'),5,90,$EditRecht,'','',$Link,'T');

		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_LAN_CODE'].'/'.$AWISSprachKonserven['MBW']['MBW_PLZ'].'/'.$AWISSprachKonserven['MBW']['MBW_ORT'].':',150);
		$SQL = 'SELECT LAN_CODE, LAN_LAND';
		$SQL .= ' FROM Laender';
		$SQL .= ' ORDER BY LAN_LAND';
		$Form->Erstelle_SelectFeld('MBW_LAN_CODE',$rsMBW->FeldInhalt('MBW_LAN_CODE'),0,$EditRecht,$SQL,'',$AWISBenutzer->BenutzerSprache(),'','','-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->Erstelle_TextFeld('MBW_PLZ',$rsMBW->FeldInhalt('MBW_PLZ'),6,0,$EditRecht,'','','','T');
		$Form->Erstelle_TextFeld('MBW_ORT',$rsMBW->FeldInhalt('MBW_ORT'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_MBK_KEY'].':',150);
		$SQL = 'SELECT MBK_KEY, MBK_KLASSIFIZIERUNG';
		$SQL .= ' FROM MITBEWERBERKLASSIFIZIERUNGEN';
		$SQL .= ' ORDER BY MBK_SORTIERUNG, MBK_KLASSIFIZIERUNG';
		//$SQL .= ' ORDER BY nlssort(MBK_KLASSIFIZIERUNG,\'NLS_SORT=BINARY\')';
		$Java = 'onchange="document.getElementsByName(\'txtMBW_MUT_KEY\')[0].value=\'\';document.getElementsByName(\'txtMBW_MUT_KEY\')[0].focus();"';
		$Form->Erstelle_SelectFeld('MBW_MBK_KEY',$rsMBW->FeldInhalt('MBW_MBK_KEY'),220,$EditRecht,$SQL,'','','','',array('~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']),$Java);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_MUT_KEY'].':',130);
		$AktuelleDaten = ($rsMBW->FeldInhalt('MBW_MUT_KEY')==''?array('~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']):array($rsMBW->FeldInhalt('MBW_MUT_KEY').'~'.$rsMBW->FeldInhalt('MUT_UNTERNEHMENSTYP')));
		$Form->Erstelle_SelectFeld('MBW_MUT_KEY',$rsMBW->FeldInhalt('MBW_MUT_KEY'),'200:180',$EditRecht,'***MUT_Daten;txtMBW_MUT_KEY;MBK_KEY=*txtMBW_MBK_KEY&WERT='.$rsMBW->FeldInhalt('MBW_MUT_KEY'),'','','','',$AktuelleDaten);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_HINWEIS'].':',100);
		$Form->Erstelle_TextFeld('MBW_HINWEIS',$rsMBW->FeldInhalt('MBW_HINWEIS'),20,200,$EditRecht,'','','','T','L',$AWISSprachKonserven['MBW']['ttt_MBW_HINWEIS'],'',50);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Link = strtr($INET_LINK_Telefon,$Parameter);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_TELEFON'].':',150,'');
		$Form->Erstelle_TextFeld('MBW_TELEFON',$rsMBW->FeldInhalt('MBW_TELEFON'),20,200,$EditRecht,'','',$Link,'T');
		$Form->ZeileEnde();

		/*
		$Form->ZeileStart();
		$Link = ($rsMBW->FeldInhalt('MBW_WEBSEITE')==''?'':$rsMBW->FeldInhalt('MBW_WEBSEITE'));
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_WEBSEITE'].':',150);
		$Form->Erstelle_TextFeld('MBW_WEBSEITE',$rsMBW->FeldInhalt('MBW_WEBSEITE'),60,500,$EditRecht,'','',$Link,'T');
		$Form->ZeileEnde();
		*/

		$Form->ZeileStart();
		$Link = ($rsMBW->FeldInhalt('MBW_ONLINESHOP')==''?'':$rsMBW->FeldInhalt('MBW_ONLINESHOP'));
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_ONLINESHOP'].':',150,'',$Link);
		$Form->Erstelle_TextFeld('MBW_ONLINESHOP',$rsMBW->FeldInhalt('MBW_ONLINESHOP'),60,500,$EditRecht,'','',$Link,'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_BEMERKUNG'].':',150);
		$Form->Erstelle_Textarea('MBW_BEMERKUNG',$rsMBW->FeldInhalt('MBW_BEMERKUNG'),500,70,3,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->Trennzeile('L');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_PRUEFUNGDURCH'].':',150);
		$Form->Erstelle_TextFeld('MBW_PRUEFUNGDURCH',$rsMBW->FeldInhalt('MBW_PRUEFUNGDURCH'),30,270,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_PRUEFUNGAM'].':',100);
		$Form->Erstelle_TextFeld('MBW_PRUEFUNGAM',$rsMBW->FeldInhalt('MBW_PRUEFUNGAM'),10,120,$EditRecht,'','','','D');
		$Form->ZeileEnde();

		$Form->Formular_Ende();


		if($rsMBW->FeldInhalt('MBW_KEY')!='')
		{
			$Reg = new awisRegister(3035);
			$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
		}

	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht3020&6)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if((($Recht3020&4)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	if((($Recht3020&8)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
	}

	//$Form->Schaltflaeche('href', 'cmdDrucken', './mitbewerb_Drucken_Liste.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');
/*
	$HilfeSeite='';
	if($DetailAnsicht)
	{
		if($AWIS_KEY1>0)
		{
			$HilfeSeite=(isset($_GET['Seite'])?$_GET['Seite']:'Filialzuordnungen');
		}

		if($HilfeSeite=='Filialzuordnungen')
		{
			if($AWIS_KEY2<0)
			{
				$HilfeSeite='FilialzuordnungenNeu';
			}
		}
	}
	else
	{
			$HilfeSeite='Liste';
	}
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=mitbewerb&Aktion=details&Seite=".$HilfeSeite."','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
*/
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		@file_put_contents('/tmp/mitbewerb_Details.err',$AWISBenutzer->BenutzerName()."\n".serialize($SQL)."\n\n",FILE_APPEND);
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND MBW_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['MBW_NAME1']) AND $Param['MBW_NAME1']!='')
	{
		$Bedingung .= ' AND (UPPER(MBW_NAME1) ' . $DB->LIKEoderIST('*'.$Param['MBW_NAME1'].'*',awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['MBW_STRASSE']) AND $Param['MBW_STRASSE']!='')
	{
		$Bedingung .= ' AND (UPPER(MBW_STRASSE) ' . $DB->LIKEoderIST($Param['MBW_STRASSE'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['MBW_ORT']) AND $Param['MBW_ORT']!='')
	{
		$Bedingung .= ' AND UPPER(MBW_ORT) ' . $DB->LIKEoderIST($Param['MBW_ORT'],awisDatenbank::AWIS_LIKE_UPPER) . '';
	}

	if(isset($Param['MBW_PLZ']) AND $Param['MBW_PLZ']!='')
	{
		$Bedingung .= ' AND MBW_PLZ ' . $DB->LIKEoderIST($Param['MBW_PLZ'],awisDatenbank::AWIS_LIKE_UPPER) . '';
	}

	if(isset($Param['MBW_LAN_CODE']) AND $Param['MBW_LAN_CODE']!='0')
	{
		$Bedingung .= ' AND (MBW_LAN_CODE ' . $DB->LIKEoderIST($Param['MBW_LAN_CODE'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['MFI_FIL_ID']) AND $Param['MFI_FIL_ID']!='')
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM MitbewerberFilialen WHERE MFI_MBW_KEY = MBW_KEY AND MFI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['MFI_FIL_ID']).')';
	}

	if(isset($Param['MFI_ENTFERNUNG']) AND $Param['MFI_ENTFERNUNG']!='')
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM MitbewerberFilialen WHERE MFI_MBW_KEY = MBW_KEY AND MFI_ENTFERNUNG <= '.$DB->FeldInhaltFormat('N2',$Param['MFI_ENTFERNUNG']).')';
	}

	if(isset($Param['MBW_MBK_KEY']) AND $Param['MBW_MBK_KEY']!='')
	{
		$Bedingung .= ' AND MBW_MBK_KEY = '.$DB->FeldInhaltFormat('N0',$Param['MBW_MBK_KEY']).' ';
	}

	if(isset($Param['MBW_MUT_KEY']) AND $Param['MBW_MUT_KEY']!='')
	{
		$Bedingung .= ' AND MBW_MUT_KEY = '.$DB->FeldInhaltFormat('N0',$Param['MBW_MUT_KEY']).' ';
	}

	return $Bedingung;
}
?>