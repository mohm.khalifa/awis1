<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Funktionen;

try {

    if (isset($_POST['cmdSpeichern_x']) or isset($_POST['cmdLoeschen_x'])) {
        include 'grosskundenkarten_speichern.php';
    }

    if (isset($_GET['KARTE'])) {
        $Funktionen->Form->Hinweistext($Funktionen->AWISSprachkonserven['ERM']['ERM_HINWEIS_ERNEUTFREIGEGEBEN'],awisFormular::HINWEISTEXT_OK);
        include 'grosskundenkarten_speichern.php';
    }

    if (isset($_GET['KUNDEN_NR'])) {
        $AWIS_KEY1 = $_GET['KUNDEN_NR'];
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
    }

    $Param = array();
    $Param = unserialize($Funktionen->AWISBenutzer->ParameterLesen('Formular_ERM'));
    $Param['Block'] = 1;

    if (isset($_POST['cmdSuche_x'])) {
        $Param['ERZ_KUNDEN_NR'] = $_POST['sucERZ_KUNDEN_NR'];
        $Param['ERM_NAME'] = $_POST['sucERM_NAME'];
        $Param['LAND'] = $_POST['sucLAND'];
        $Param['ART'] = $_POST['sucART'];
        $Param['DRUCKSTATUS'] = $_POST['sucERM_DRUCKSTATUS'];
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    }

    $Block = 1;
    if (isset($_REQUEST['Block'])) {
        $Block = $Funktionen->Form->Format('N0', $_REQUEST['Block'], false);
    } else {
        $Block = $Param['Block'];
    }

    $Funktionen->AWISBenutzer->ParameterSchreiben('Formular_ERM', serialize($Param));

    if ($AWIS_KEY1 != '') {
        $Param['ERZ_KUNDEN_NR'] = $AWIS_KEY1;
    }

    $SQL = SQLZusammenbauen($Param,$Funktionen);

    $MaxDS = 1;
    $ZeilenProSeite = 1;
    $ZeilenProSeite = $Funktionen->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $Funktionen->DB->ErmittleZeilenAnzahl($SQL, $Funktionen->DB->Bindevariablen('ERM', false));
    if (!isset($AWIS_KEY1) OR $AWIS_KEY1 == '') {
        $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
    }
    $rsERM = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('ERM'));

    $Funktionen->Form->DebugAusgabe(1,$Funktionen->DB->LetzterSQL());
    if ($rsERM->AnzahlDatensaetze() == 1) {
        $AWIS_KEY1 = $rsERM->FeldInhalt('KUNDEN_NR');
    }

    // Spaltenbreiten f�r Listenansicht
    $FeldBreiten = array();
    $FeldBreiten['KUNDEN_NR'] = 80;
    $FeldBreiten['NAMEN'] = 220;
    $FeldBreiten['ERM_NR'] = 100;
    $FeldBreiten['ERM_MODELLNAME'] = 120;
    $FeldBreiten['Werte'] = 200;

    $Funktionen->Form->Formular_Start();
    $Funktionen->Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./grosskundenkarten_Main.php?cmdAktion=Zuordnung>");

    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./grosskundenkarten_Main.php?cmdAktion=Zuordnung accesskey=T title='" . $Funktionen->AWISSprachkonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );

    if (isset($AWIS_KEY1) and $AWIS_KEY1 != '') {
        $Funktionen->Form->InfoZeile($Felder, '');

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachkonserven['ERM']['ERZ_KUNDEN_NR'], $FeldBreiten['Werte']);
        if ($AWIS_KEY1 != '-1') {
            $Funktionen->Form->Erstelle_TextLabel($AWIS_KEY1, $FeldBreiten['Werte']);
        }
        $Funktionen->Form->AuswahlBox('!KUNDEN_NR', 'box1', '', 'ERM_Daten', '', $FeldBreiten['Werte'], $FeldBreiten['Werte'] / 10, isset($_POST['sucKUNDEN_NR'])?$_POST['sucKUNDEN_NR']:$rsERM->FeldInhalt('KUNDEN_NR'), 'T', true, '', '', '', 0, '', '', '', 0, ($AWIS_KEY1 != '-1'?'display: none':''), 'autocomplete=off ');
        $Funktionen->Form->ZeileEnde();
        $Funktionen->Form->AuswahlBoxHuelle('box1', '', '', '');

        $Script = '<script>$(window).load(function(){ $( "#sucKUNDEN_NR" ).change(); })</script>';
        $Funktionen->Form->SchreibeHTMLCode($Script);

        // hier liste mit gedruckten Karten
        $SQL = 'SELECT * ';
        $SQL .= 'FROM '.$Funktionen->holeKartenTabelle().' ';
        $SQL .= 'WHERE KUNDEN_NR = ' . $Funktionen->DB->WertSetzen('KARTEN', 'Z', $AWIS_KEY1);
        $SQL .= ' AND GEDRUCKT = ' . $Funktionen->DB->WertSetzen('KARTEN', 'Z', 2);
        $SQL .= ' ORDER BY DRUCK_DATUM DESC';

        $rsKarten = $Funktionen->DB->RecordSetOeffnen($SQL,$Funktionen->DB->Bindevariablen('KARTEN'));

        $Funktionen->Form->Trennzeile('L');

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['KARTEN_NR'], 140);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['EAN_NR'], 140);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['DRUCK_DATUM'], 140);
        $Funktionen->Form->ZeileEnde();

        $DS = 2;
        while (!$rsKarten->EOF()) {
            $Icons = array();
            $Icons[] = array(
                'delete',
                './grosskundenkarten_Main.php?cmdAktion=Zuordnung&KUNDEN_NR=' . $AWIS_KEY1 . '&KARTE=' . $rsKarten->FeldInhalt('KARTEN_NR')
            );

            $Funktionen->Form->ZeileStart();
            $Funktionen->Form->Erstelle_ListeIcons($Icons, 20);
            $Funktionen->Form->Erstelle_ListenFeld('KARTEN_NR', $rsKarten->FeldInhalt('KARTEN_NR'), 120, 120, false, ($DS % 2));
            $Funktionen->Form->Erstelle_ListenFeld('EAN_NR', $rsKarten->FeldInhalt('EAN_NR'), 140, 140, false, ($DS % 2));
            $Funktionen->Form->Erstelle_ListenFeld('DRUCK_DATUM', $rsKarten->FeldInhalt('DRUCK_DATUM'), 140, 140, false, ($DS % 2));
            $Funktionen->Form->ZeileEnde();
            $DS++;
            $rsKarten->DSWeiter();
        }

    } elseif (($rsERM->AnzahlDatensaetze() > 1)) {
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERZ_KUNDEN_NR'], $FeldBreiten['KUNDEN_NR']);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['NAME1'], $FeldBreiten['NAMEN']);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['NAME2'], $FeldBreiten['NAMEN']);
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['NAME3'], $FeldBreiten['NAMEN']);
        if(grosskundenkarten_funktionen::RABATTMODELLE_AKTIV){
            $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERM_NR'], $FeldBreiten['ERM_NR']);
            $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachkonserven['ERM']['ERM_MODELLNAME'], $FeldBreiten['ERM_MODELLNAME']);
        }

        $Funktionen->Form->ZeileEnde();

        $DS = 2;
        while (!$rsERM->EOF()) {
            $Funktionen->Form->ZeileStart();
            $Link = './grosskundenkarten_Main.php?cmdAktion=Zuordnung&KUNDEN_NR=' . $rsERM->FeldInhalt('KUNDEN_NR');
            $Funktionen->Form->Erstelle_ListenFeld('ERZ_KUNDEN_NR', $rsERM->FeldInhalt('KUNDEN_NR'), $FeldBreiten['KUNDEN_NR'], $FeldBreiten['KUNDEN_NR'], false, ($DS % 2), '', $Link);
            $Funktionen->Form->Erstelle_ListenFeld('NAME1', $rsERM->FeldInhalt('NAME1'), $FeldBreiten['NAMEN'], $FeldBreiten['NAMEN'], false, ($DS % 2), '');
            $Funktionen->Form->Erstelle_ListenFeld('NAME2', $rsERM->FeldInhalt('NAME2'), $FeldBreiten['NAMEN'], $FeldBreiten['NAMEN'], false, ($DS % 2), '');
            $Funktionen->Form->Erstelle_ListenFeld('NAME3', $rsERM->FeldInhalt('NAME3'), $FeldBreiten['NAMEN'], $FeldBreiten['NAMEN'], false, ($DS % 2), '');
            if(grosskundenkarten_funktionen::RABATTMODELLE_AKTIV){
                $Link = 'grosskundenkarten_Main.php?cmdAktion=Modelle&ERM_NR=' . $rsERM->FeldInhalt('ERZ_ERM_NR');
                $Funktionen->Form->Erstelle_ListenFeld('ERZ_ERM_NR', $rsERM->FeldInhalt('ERZ_ERM_NR'), $FeldBreiten['ERM_NR'], $FeldBreiten['ERM_NR'], false, ($DS % 2), '', $Link);
                $Funktionen->Form->Erstelle_ListenFeld('ERM_MODELLNAME', $rsERM->FeldInhalt('ERM_MODELLNAME'), $FeldBreiten['ERM_MODELLNAME'], $FeldBreiten['ERM_MODELLNAME'], false, ($DS % 2));
            }
            $Funktionen->Form->ZeileEnde();

            $DS++;
            $rsERM->DSWeiter();
        }

        $Link = './grosskundenkarten_Main.php?cmdAktion=Zuordnung';
        $Funktionen->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else {
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Hinweistext($Funktionen->AWISSprachkonserven['Fehler']['err_keineDaten']);
        $Funktionen->Form->ZeileEnde();
    }

    $Funktionen->Form->Formular_Ende();

    $Funktionen->Form->SchaltflaechenStart();
    $Funktionen->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_zurueck'], 'Z');

    if(grosskundenkarten_funktionen::RABATTMODELLE_AKTIV){
        if ($AWIS_KEY1 != '') {
            $Funktionen->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_speichern'], 'S');
            if ($AWIS_KEY1 > 0) {
                $Funktionen->Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_loeschen'], 'L', '', 27, 27, '', true);
            }
        }


        $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachkonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Funktionen->Form->SchaltflaechenEnde();

    $Funktionen->Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->DebugAusgabe(1, $ex->getSQL());
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {

    if ($Funktionen->Form instanceof awisFormular) {

        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

function SQLZusammenbauen($Param,$Funktionen)
{

    $SQL = 'SELECT DISTINCT NAME1, NAME2, NAME3, KUNDEN_NR, ERZ_ERM_NR, ERM_MODELLNAME , ERZ_KUNDEN_NR, GKB_STAAT, row_number() OVER (ORDER BY KUNDEN_NR DESC) AS ZeilenNr ';
    $SQL .= 'FROM ';
    $SQL .= '(SELECT DISTINCT KND.NAME1, KND.NAME2, KND.NAME3, KND.KUNDEN_NR, ERZ_ERM_NR, ERM_MODELLNAME , ERZ_KUNDEN_NR, GKB_STAAT, GEDRUCKT, ZAHL_ART FROM EXPRABATTZUORDNUNGEN ERZ ';
    $SQL .= 'LEFT JOIN EXPRABATTMODELLE ERM ON ERZ.ERZ_ERM_NR = ERM_NR ';
    $SQL .= 'RIGHT JOIN '.$Funktionen->holeKartenTabelle().' KND ON ERZ_KUNDEN_NR = KND.KUNDEN_NR ';
    $SQL .= 'LEFT JOIN '.$Funktionen->holeDestTabelle().' LAND ON LAND.KUNDEN_NR = KND.KUNDEN_NR) ';

    $Bedingung = '';
    if (isset($Param['ERZ_KUNDEN_NR']) and $Param['ERZ_KUNDEN_NR'] != '') {
        $Bedingung .= 'AND KUNDEN_NR = ' . $Funktionen->DB->WertSetzen('ERM', 'T', $Param['ERZ_KUNDEN_NR']);
    }

    if (isset($Param['ERM_NAME']) and $Param['ERM_NAME'] != '') {
        $Bedingung .= ' AND NAME1||NAME2||NAME3 LIKE ' . $Funktionen->DB->WertSetzen('ERM', 'T', '%' . $Param['ERM_NAME'] . '%');
    }


    if (isset($Param['LAND']) and $Param['LAND'] != '') {
        switch ($Param['LAND']) {
            case '-1':
                break;
            case '0':
                $Bedingung .= ' AND GKB_STAAT IS NULL';
                break;
            default:
                $Bedingung .= ' AND GKB_STAAT = ' . $Funktionen->DB->WertSetzen('ERM', 'T', $Param['LAND']);
        }
    }

    if (isset($Param['ART']) and $Param['ART'] != '') {
        switch ($Param['ART']) {
            case '-1':
                break;
            case '1':
                $Bedingung .= ' AND ZAHL_ART = ' . $Funktionen->DB->WertSetzen('ERM', 'T', 'K');
                break;
            case '2':
                $Bedingung .= ' AND ZAHL_ART = ' . $Funktionen->DB->WertSetzen('ERM', 'T', 'G');
                $Bedingung .= ' AND ( NAME3 <> ' . $Funktionen->DB->WertSetzen('ERM', 'T', 'Mana Ara') . ' ';
                $Bedingung .= ' OR NAME3 is null) '; //Nur Suche nach 'nicht Mana Ara' -> keine
                break;
            case '3':
                $Bedingung .= ' AND NAME3 = ' . $Funktionen->DB->WertSetzen('ERM', 'T', 'Mana Ara');
                break;
            default:
                break;
        }
    }

    if(isset($Param['ERM_DRUCKSTATUS'])){
        switch ($Param['ERM_DRUCKSTATUS']){
            case 1:
                $Bedingung .= ' AND GEDRUCKT = 1';
                break;
            default:
                break;
        }
    }

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $SQL .= ' ORDER BY KUNDEN_NR DESC';

    return $SQL;
}