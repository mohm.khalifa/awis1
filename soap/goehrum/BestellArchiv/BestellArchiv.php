<?php

class BestellArchiv extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'AuftraegeProTag' => '\\AuftraegeProTag',
      'AuftraegeProTagResponse' => '\\AuftraegeProTagResponse',
      'AuftragsListe' => '\\AuftragsListe',
      'ArrayOfPartnerBestellBestaetigungV3' => '\\ArrayOfPartnerBestellBestaetigungV3',
      'PartnerBestellBestaetigungV3' => '\\PartnerBestellBestaetigungV3',
      'ArrayOfBestaetigungPosition' => '\\ArrayOfBestaetigungPosition',
      'BestaetigungPosition' => '\\BestaetigungPosition',
      'Serialize' => '\\Serialize',
      'OrderLieferadresse' => '\\OrderLieferadresse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        die();
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param AuftraegeProTag $parameters
     * @return AuftraegeProTagResponse
     */
    public function AuftraegeProTag(AuftraegeProTag $parameters)
    {
      return $this->__soapCall('AuftraegeProTag', array($parameters));
    }

}
