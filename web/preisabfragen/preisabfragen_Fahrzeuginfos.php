<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PFI','%');
	$TextKonserven[]=array('KFZ','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8003 = $AWISBenutzer->HatDasRecht(8003);
	if($Recht8003==0)
	{
	    awisEreignis(3,1000,'Preisermittlung - ',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PFI'));
	if(empty($Param))
	{
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='PFI_BEZEICHNUNG';
	}

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['PFI_BEZEICHNUNG'] = $_POST['sucPFI_BEZEICHNUNG'];
		$Param['FIL_ID'] = $_POST['sucFIL_ID'];
		$Param['DATUMBIS'] = $_POST['sucDATUMBIS'];
		$Param['DATUMVOM'] = $_POST['sucDATUMVOM'];
		$Param['AST_ATUNR'] = $_POST['sucAST_ATUNR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='PFI_BEZEICHNUNG DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_PFI",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./preisabfragen_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./preisabfragen_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PFI'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PFI'));
	}
	elseif(isset($_GET['PFI_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PFI_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PFI'));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='PFI_BEZEICHNUNG DESC';
			$AWISBenutzer->ParameterSchreiben('Formular_PFI',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY PFI_BEZEICHNUNG DESC';
			$Param['ORDER']='PFI_BEZEICHNUNG DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT Preisabfragenfahrzeuginfos.*';
	$SQL .= ', KFZ_HERSTELLER, KFZ_MODELL, KFZ_TYP';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Preisabfragenfahrzeuginfos';
	$SQL .= ' LEFT OUTER JOIN v_KFZSTAMM KFZ ON PFI_KTYPNR = KFZ_TYPNR';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_PFI',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsPFI = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_PFI',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmPreisabfragenfahrzeuginfos action=./preisabfragen_Main.php?cmdAktion=Fahrzeuginfos method=POST enctype="multipart/form-data">');

	if($rsPFI->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsPFI->AnzahlDatensaetze()>1 AND !isset($_GET['PFI_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './preisabfragen_Main.php?cmdAktion=Fahrzeuginfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PFI_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PFI_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFI']['PFI_BEZEICHNUNG'],250,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Fahrzeuginfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KFZ_HERSTELLER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KFZ_HERSTELLER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KFZ']['KFZ_HERSTELLER'],200,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Fahrzeuginfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KFZ_MODELL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KFZ_MODELL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KFZ']['KFZ_MODELL'],250,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Fahrzeuginfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PFI_BAUJAHR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PFI_BAUJAHR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFI']['PFI_BAUJAHR'],100,'',$Link);
		$Link = './preisabfragen_Main.php?cmdAktion=Fahrzeuginfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PFI_KMSTAND'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PFI_KMSTAND'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFI']['PFI_KMSTAND'],110,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsPFI->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './preisabfragen_Main.php?cmdAktion=Fahrzeuginfos&PFI_KEY=0'.$rsPFI->FeldInhalt('PFI_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('#PFI_BEZEICHNUNG',$rsPFI->FeldInhalt('PFI_BEZEICHNUNG'),0,250,false,($DS%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('#KFZ_HERSTELLER',$rsPFI->FeldInhalt('KFZ_HERSTELLER'),0,200,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('#KFZ_MODELL',$rsPFI->FeldInhalt('KFZ_MODELL'),0,250,false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('#PFI_BAUJAHR',$rsPFI->FeldInhalt('PFI_BAUJAHR'),0,100,false,($DS%2),'','','T','Z');
			$Form->Erstelle_ListenFeld('#PFI_KMSTAND',$rsPFI->FeldInhalt('PFI_KMSTAND'),0,90,false,($DS%2),'','','N0T','R');
			if($rsPFI->FeldInhalt('PFI_BEMERKUNG')!='')
			{
				$Form->Erstelle_ListenBild('ohne', '', '', '/bilder/icon_info.png',$rsPFI->FeldInhalt('PFI_BEMERKUNG'),($DS%2));
			}
			$Form->ZeileEnde();

			$rsPFI->DSWeiter();
			$DS++;
		}

		$Link = './preisabfragen_Main.php?cmdAktion=Fahrzeuginfos';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsPFI->FeldInhalt('PFI_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_PFI',serialize($Param));

		$Form->Erstelle_HiddenFeld('PFI_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./preisabfragen_Main.php?cmdAktion=Fahrzeuginfos&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPFI->FeldInhalt('PFI_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPFI->FeldInhalt('PFI_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht8003&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht8003&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PFI']['PFI_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('PFI_BEZEICHNUNG',$rsPFI->FeldInhalt('PFI_BEZEICHNUNG'),50,200,$EditRecht,'','','','T');
		$AWISCursorPosition = 'txtPFI_BEZEICHNUNG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PFI']['PFI_KTYPNR'].':',200);
		$Form->Erstelle_TextFeld('PFI_KTYPNR',$rsPFI->FeldInhalt('PFI_KTYPNR'),10,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		if($rsPFI->FeldInhalt('PFI_KTYPNR')!='')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('',200);
			$Form->Erstelle_TextFeld('*HERSTELLER',$rsPFI->FeldInhalt('KFZ_HERSTELLER'),50,500,false,'','','','T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('',200);
			$Form->Erstelle_TextFeld('*MODELL',$rsPFI->FeldInhalt('KFZ_MODELL'),50,500,false,'','','','T');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('',200);
			$Form->Erstelle_TextFeld('*TYP',$rsPFI->FeldInhalt('KFZ_TYP'),50,500,false,'','','','T');
			$Form->ZeileEnde();

		}		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PFI']['PFI_BAUJAHR'].':',200);
		$Form->Erstelle_TextFeld('PFI_BAUJAHR',$rsPFI->FeldInhalt('PFI_BAUJAHR'),8,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PFI']['PFI_KMSTAND'].':',200);
		$Form->Erstelle_TextFeld('PFI_KMSTAND',$rsPFI->FeldInhalt('PFI_KMSTAND'),8,200,$EditRecht,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PFI']['PFI_BEMERKUNG'].':',200,'','');
		$Form->Erstelle_Textarea('PFI_BEMERKUNG',$rsPFI->FeldInhalt('PFI_BEMERKUNG'),800,100,5,$EditRecht);
		$Form->ZeileEnde();

		/*
		if($rsPFI->FeldInhalt('PFI_KEY')!='')
		{
			$Reg = new awisRegister(8002);
			$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
		}*/
	}

	//awis_Debug(1, $Param, $Bedingung, $rsPFI, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht8003&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht8003&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	if(($Recht8003&8) == 8 AND $DetailAnsicht)		// L�schen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}

	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201615");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201614");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PFI_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	return $Bedingung;
}
?>