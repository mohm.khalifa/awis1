<?php

require_once 'awisMailer.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 27.09.2016
 * Time: 15:23
 */
class awisSmartKatExport
{
    //F�r Import + Export
    private $_AWISBenutzer;
    private $_DB;
    private $_ProtokollDB;
    private $_DebugLevel = 99;

    private $_EMAIL_Infos = array('shuttle@de.atu.eu');
    private $_DebugText = '';

    //F�r Export
    private $_SQLDatei = '';


    function __construct($Benutzer)
    {
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        //Eigene Datenbankverbindung f�r das Protokoll, da der Import in einer eigenen Transaktion l�uft
        $this->_ProtokollDB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_ProtokollDB->Oeffnen();
    }

    function __destruct()
    {
        echo $this->_DebugText;
    }

    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }


    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText .= date('d.m.Y H:i:s');
            $this->_DebugText .= ': ' . $Typ;
            $this->_DebugText .= ': ' . $Text;
            $this->_DebugText .= PHP_EOL;
        }
    }


    /**
     * Exportiert
     */
    public function ExportArtikel()
    {
        $this->_SQLDatei = '/daten/include/SQLspool/smartkat_lieferanten_export.sql';
        $this->_StarteExport();
    }

    public function ExportAlternativartikel(){
        $this->_SQLDatei = '/daten/include/SQLspool/smartkat_alternativartikel_export.sql';
        $this->_StarteExport();
    }

    //Alternativ Artikel Apollo
    public function ExportAltArtiApollo(){
        $this->_SQLDatei = '/daten/include/SQLspool/smartkat_altartiapollo_export.sql';
        $this->_StarteExport();
    }

    private function _StarteExport(){
        $Befehl = $this->_AWISBenutzer->ParameterLesen('SQLPLUS');
        $Kennwort = $this->_AWISBenutzer->ParameterLesen('AWIS_PW');

        switch (awisWerkzeuge::awisLevel()) {
            case awisWerkzeuge::AWIS_LEVEL_ENTWICK :
            case awisWerkzeuge::AWIS_LEVEL_STAGING :
                $Befehl .= '  -S AWIS/' . $Kennwort . '@AWISSTAG_SH @' . $this->_SQLDatei;
                break;
            case awisWerkzeuge::AWIS_LEVEL_SHUTTLE :
            case awisWerkzeuge::AWIS_LEVEL_PRODUKTIV :
                $Befehl .= '  -S AWIS/' . $Kennwort . '@AWISPROD_SH @' . $this->_SQLDatei;
                break;
        }

        system($Befehl);
    }
}