<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20090220
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('MBW','%');
	$TextKonserven[]=array('MFI','MFI_FIL_ID');
	$TextKonserven[]=array('MFI','MFI_ENTFERNUNG');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht3020=$AWISBenutzer->HatDasRecht(3020);
	if($Recht3020==0)
	{
	    awisEreignis(3,1000,'MBW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./mitbewerb_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_MBW'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();


	// Name des Mitbewerbers
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_NAME1'].':',190);
	$Form->Erstelle_TextFeld('*MBW_NAME1',($Param['SPEICHERN']=='on'?$Param['MBW_NAME1']:''),25,200,true);
	$AWISCursorPosition='sucMBW_NAME1';
	$Form->ZeileEnde();

	// Strasse
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_STRASSE'].':',190);
	$Form->Erstelle_TextFeld('*MBW_STRASSE',($Param['SPEICHERN']=='on'?$Param['MBW_STRASSE']:''),25,200,true);
	$Form->ZeileEnde();

	// Ort
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_ORT'].':',190);
	$Form->Erstelle_TextFeld('*MBW_ORT',($Param['SPEICHERN']=='on'?$Param['MBW_ORT']:''),25,200,true);
	$Form->ZeileEnde();

	// Postleitzahl
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_PLZ'].':',190);
	$Form->Erstelle_TextFeld('*MBW_PLZ',($Param['SPEICHERN']=='on'?$Param['MBW_PLZ']:''),5,200,true);
	$Form->ZeileEnde();

	// Land
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_LAN_CODE'].':',190);
	$SQL = "SELECT LAN_CODE, LAN_LAND FROM LAENDER ORDER BY LAN_LAND";
	$Form->Erstelle_SelectFeld('*MBW_LAN_CODE',($Param['SPEICHERN']=='on'?$Param['MBW_LAN_CODE']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'0');
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_MBK_KEY'].':',190);
	$SQL = 'SELECT MBK_KEY, MBK_KLASSIFIZIERUNG';
	$SQL .= ' FROM MITBEWERBERKLASSIFIZIERUNGEN';
	$SQL .= ' ORDER BY MBK_SORTIERUNG, MBK_KLASSIFIZIERUNG';
	//$SQL .= ' ORDER BY nlssort(MBK_KLASSIFIZIERUNG,\'NLS_SORT=BINARY\')';
	$Java = 'onchange="document.getElementsByName(\'txtMBW_MUT_KEY\')[0].value=\'\';document.getElementsByName(\'txtMBW_MUT_KEY\')[0].focus();"';
	$Form->Erstelle_SelectFeld('*MBW_MBK_KEY',($Param['SPEICHERN']=='on'?$Param['MBW_MBK_KEY']:'0'),220,true,$SQL,'','','','',array('~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']));
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBW']['MBW_MUT_KEY'].':',170);
	$AktuelleDaten = array('~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->Erstelle_SelectFeld('MBW_MUT_KEY',($Param['SPEICHERN']=='on'?$Param['MBW_MUT_KEY']:'0'),200,true,'***MUT_Daten;txtMBW_MUT_KEY;MBK_KEY=*sucMBW_MBK_KEY&WERT=','','','','',$AktuelleDaten);
	$Form->ZeileEnde();


	// Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MFI']['MFI_FIL_ID'].':',190);
	$Form->Erstelle_TextFeld('*MFI_FIL_ID',($Param['SPEICHERN']=='on'?$Param['MFI_FIL_ID']:''),5,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MFI']['MFI_ENTFERNUNG'].':',190);
	$Form->Erstelle_TextFeld('*MFI_ENTFERNUNG',($Param['SPEICHERN']=='on'?$Param['MFI_ENTFERNUNG']:''),5,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	if(($Recht3020&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	/*
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=mitbewerb&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
	*/
	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301833");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301834");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>