<?php
/**
 * awisMDEArtikelstamm.inc
*
* Export der MDE Artikelstammdaten
*
* @author Patrick Gebhardt
* @copyright ATU - Auto-Teile-Unger
* @version 20100419
*
*/
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awisMDEArtikelstammProtokoll
{

	/**
	 * Array f�r das JobProtokoll
	 * @var DebugProtokoll = array();
	 */
	private $_DebugProtokoll;

	private $_Werkzeug;
	


	/**
	 * Emailbenachrichtigung
	 * @var string
	 */
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');

	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
		
		$this->_Werkzeug = new awisWerkzeuge();
		$this->_Werkzeug->EMail($this->_EMAIL_Infos,'OK MDEArtkelstammexport BEGINNT','',1,'','awis@de.atu.eu');
            

	}
	
	public function __destruct()
	{
		//var_dump($this->_DebugProtokoll);
		$this->_Werkzeug->EMail($this->_EMAIL_Infos,'OK MDEArtkelstammexport BEENDET','<html>'.$this->_array2Html($this->_DebugProtokoll).'</html>',1,'','awis@de.atu.eu');
	}

	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-999)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $NeuerLevel;
		}

		return $this->_DebugLevel;
	}

	/**
	 * Generiert einen DebugEintrag
	 * @param unknown $Text
	 * @param number $Level
	 * @param string $Typ
	 */
	public function debugAusgabe($Text, $Level=1, $Typ = 'Debug')
	{

		if ($Level>= $this->DebugLevel())
		{
			$this->_DebugProtokoll[]['Datum'] = date('d.m.Y');
			$this->_DebugProtokoll[]['Typ'] = $Typ;
			$this->_DebugProtokoll[]['Text'] = $Text;

		}

	}
	
	/**
	 * Erstellt aus einem Array eine HTML Tabelle...
	 * @param array() $array
	 * @param string $table
	 * @return HTMLstring
	 */
	private function _array2Html($array, $table = true)
	{
		$out = '';
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				if (!isset($tableHeader)) {
					$tableHeader =
					'<th>' .
					implode('</th><th>', array_keys($value)) .
					'</th>';
				}
				array_keys($value);
				$out .= '<tr>';
				$out .= $this->_array2Html($value, false);
				$out .= '</tr>';
			} else {
				$out .= "<td>$value</td>";
			}
		}
	
		if ($table) {
			return '<table>' . $tableHeader . $out . '</table>';
		} else {
			return $out;
		}
	}
	
}



?>
