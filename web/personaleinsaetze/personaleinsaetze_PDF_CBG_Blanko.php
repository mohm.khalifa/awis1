<?php

//Blanko -> Formular VisitCheck
require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('CBT','%');
$TextKonserven[]=array('CBG','%');
$TextKonserven[]=array('CV','%');

$AWISBenutzer = awisBenutzer::Init();

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Ausdruck = new awisAusdruck('P','A4','');
$Ausdruck->NeueSeite(0,1);

if ($AWISBenutzer->BenutzerSprache()=='CZ')
{
	$Ausdruck->_pdf->ZeichenKodierung('CP1250');
	$Schriftart = 'arialcz';
}
else 
{
	$Schriftart = 'arial';
}

$Ausdruck->_pdf->AddFont($Schriftart,'','');

function Auswertung_Felder($Ausdruck,$ID,$x1,$y1,$x1_2,$y1_2,$FeldBreite = 5)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	global $Schriftart;
	
	$Schritt = 3;
	
	$SQL =  'Select * from Personaleinsberichtszuord inner join PERSONALEINSBERICHTSFELDER ON PBF_ID = PEZ_PBF_ID '; 
	$SQL .= 'where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;
	
	$rsAuswertung = $DB->RecordSetOeffnen($SQL);
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1,$y1,$x2 = ($x1  + $Schritt),$y2 = ($y1 + $Schritt));
      $Ausdruck->_pdf->Line($x2, $y1, $x1, $y2);
      $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	}
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1_2,$y1_2,$x2 = ($x1_2  + $Schritt),$y2 = ($y1_2 + $Schritt));
      $Ausdruck->_pdf->Line($x2, $y1_2, $x1_2, $y2);
      $Ausdruck->_pdf->SetLineWidth(0); 	
	}
	else
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	}
	
	if($rsAuswertung->FeldInhalt('PBF_BEMERKUNG') == '')
	{
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6);
	  $Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,35)."\n".substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),35,35);
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$Text,'LTRB',0,'L',0);
	  	
	}
	else 
	{
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6);	
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$rsAuswertung->FeldInhalt('PBF_BEMERKUNG').':'.$rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),'LTRB','L',1);  	
	}
}

$Spalte = 15;
$Zeile = 5;

if($_GET['PEI_KEY'] == 0)
{
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch GBL: Typ B','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 20.08.2012','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.0','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.1','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['CBT']['CBT_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['CBT']['CBT_DATUM'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,'','B',0,'C',0);
$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,'A. '.$AWISSprachKonserven['CBG']['CBG_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_A1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_A2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',1);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'3. '.$AWISSprachKonserven['CBG']['CBG_A3'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',1);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_A4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBG']['CBG_A5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'B. '.$AWISSprachKonserven['CBG']['CBG_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_B1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'Anz. Leerhaken','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_B2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;
$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'3. '.$AWISSprachKonserven['CBG']['CBG_B3'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_B4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'Ger�teanz.','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBG']['CBG_B5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'Ger�teanz.','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'6. '.$AWISSprachKonserven['CBG']['CBG_B6'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'C. '.$AWISSprachKonserven['CBG']['CBG_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_C1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBG']['CBG_C2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_C3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_C4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'D. '.$AWISSprachKonserven['CBG']['CBG_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_D1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_D2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_D3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'E. '.$AWISSprachKonserven['CBG']['CBG_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_E1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_E2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['CBG']['CBG_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_F1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_F2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_F3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'G. '.$AWISSprachKonserven['CBG']['CBG_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_G1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBG']['CBG_G2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_G3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_G4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'H. '.$AWISSprachKonserven['CBG']['CBG_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_H1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, $Zeile+1, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, $Zeile+1, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=30;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Datum:','T',0,'C',0);

$Spalte+=40;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift WL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL:','T',0,'C',0);



}
elseif($_GET['PEI_KEY'] != 0)
{

$SQL = 	'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS '; 
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= 'WHERE PEZ_PEI_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEI_KEY']);
	
$rsBericht = $DB->RecordSetOeffnen($SQL);	

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch GBL: Typ B','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 22.08.2012','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.0','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.1','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['CBT']['CBT_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,$rsBericht->FeldInhalt('PEI_FIL_ID'),'B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITVON'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITBIS'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['CBT']['CBT_DATUM'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,substr($rsBericht->FeldInhalt('PEI_DATUM'),0,10),'B',0,'C',0);
$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,'A. '.$AWISSprachKonserven['CBG']['CBG_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_A1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,151, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_A2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,152, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'3. '.$AWISSprachKonserven['CBG']['CBG_A3'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,153, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_A4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,154, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBG']['CBG_A5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,155, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'B. '.$AWISSprachKonserven['CBG']['CBG_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_B1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,157, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_B2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,158, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;
$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'3. '.$AWISSprachKonserven['CBG']['CBG_B3'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,159, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_B4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,160, 128.5, $Zeile+1,143, $Zeile+1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBG']['CBG_B5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,161, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'6. '.$AWISSprachKonserven['CBG']['CBG_B6'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,162, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'C. '.$AWISSprachKonserven['CBG']['CBG_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_C1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,164, 128.5, $Zeile+1,143, $Zeile+1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBG']['CBG_C2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,165, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_C3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,166, 128.5, $Zeile+1,143, $Zeile+1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_C4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,167, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'D. '.$AWISSprachKonserven['CBG']['CBG_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_D1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,169, 128.5, $Zeile+1,143, $Zeile+1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_D2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,170, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_D3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,171, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'E. '.$AWISSprachKonserven['CBG']['CBG_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_E1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,173, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_E2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,174, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['CBG']['CBG_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_F1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,176, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['CBG']['CBG_F2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,177, 128.5, $Zeile+1,143, $Zeile+1, 8);
$Spalte-=110;
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_F3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,178, 128.5, $Zeile+1, 143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'G. '.$AWISSprachKonserven['CBG']['CBG_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_G1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,180, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBG']['CBG_G2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,181, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBG']['CBG_G3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,182, 128.5, $Zeile+1,143, $Zeile+1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBG']['CBG_G4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,183, 128.5, $Zeile+1,143, $Zeile+1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'H. '.$AWISSprachKonserven['CBG']['CBG_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBG']['CBG_H1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck, 185, 128.5, $Zeile+1, 143, $Zeile+1);

$Zeile+=16;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Datum:','T',0,'C',0);

$Spalte+=40;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift WL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL:','T',0,'C',0);

$Zeile+=5;
$Spalte-=140;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";

$rsGesamt = $DB->RecordSetOeffnen($SQL);

$Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');

echo "Gesamtanzahl:".$Gesamtanzahl;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= "AND PEZ_WERT = '0'";

$GesamtFalsch = $DB->RecordSetOeffnen($SQL);

$Falsch = $GesamtFalsch->FeldInhalt('ANZ');

echo "Falsch".$Falsch;

$Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) *100); 

if($Prozentsatz >= 90 && $Prozentsatz <= 100)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=10;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GR�N)','',0,'R',0);
	
}
elseif($Prozentsatz >= 80 && $Prozentsatz <= 90)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=10;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GELB)','',0,'R',0);
	
}
elseif($Prozentsatz < 80)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	//$Ausdruck->_pdf->cell2(180,4,'Hallo','LTRB',0,'L',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=10;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(180,5,'(ROT)','',0,'C',0);
	//$Ausdruck->_pdf->Image($file, $x, $y,$w,$h);
}
$Zeile+=4;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(180,5,'Erf�llungsgrad: min. 75 % der Felder, Erf�llungsrating: 100% - 90% = Gr�n, 89% - 80% = Gelb, < 80% = Rot   ','',0,'C',0);
	
}


$Ausdruck->Anzeigen();


?>