<?php
require_once('awisDatenbank.inc');
/**
 * Klasse zum Verwalten und Anzeigen von Kontakten
 *
 */
class awisKontakt
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB;

	/**
	 * Datensatz mit dem Kontakt
	 *
	 * @var awisRecordset
	 */
	private $_rsKON = null;

	/**
	 * Aktuelle Kontakt-ID
	 *
	 * @var int
	 */
	private $_KON_KEY = 0;

	public function __construct($KON_KEY, $Exception=true)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();

		$BindeVariablen=array();
		$BindeVariablen['var_N0_kon_key']=intval('0'.$KON_KEY);
		
		$SQL = 'SELECT *';
		$SQL .= ' FROM Kontakte';
		$SQL .= ' LEFT OUTER JOIN Benutzer ON KON_KEY = XBN_KON_KEY';
		$SQL .= ' WHERE KON_KEY = :var_N0_kon_key';

		$this->_rsKON = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);

		if($this->_rsKON->EOF() AND $Exception)
		{
			throw new awisException('Kein passender Kontakt gefunden',1,$SQL,1);
		}

		$this->_KON_KEY = $this->_rsKON->FeldInhalt('KON_KEY');
	}

	/**
	 * Liefert die aktuelle KON_KEY oder setzt einen neuen Kontakt
	 *
	 * @param int $NeuerKontaktKEY
	 * @return int
	 */
	public function KontaktKEY($NeuerKontaktKEY=0)
	{
		if($NeuerKontaktKEY!==0)
		{
			$this->__construct($NeuerKontaktKEY,false);
		}

		return $this->_KON_KEY;
	}

	public function KontaktInfo($Info)
	{

		$BindeVariablen=array();
		$BindeVariablen['var_N0_kon_key']=intval('0'.$this->_rsKON->FeldInhalt('KON_KEY'));
		$SQL = 'SELECT *';
		$SQL .= ' FROM KontakteKommunikation ';
		$SQL .= ' WHERE KKO_KON_KEY = :var_N0_kon_key';
		if(is_array($Info))
		{
			$Erg = '';
			foreach($Info AS &$Eintrag)
			{
				$Eintrag = $this->_DB->FeldInhaltFormat('N0',$Eintrag);
			}
			$SQL .= ' AND KKO_KOT_KEY IN('.implode(',',$Info).')';
			$rsKKO = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
			while(!$rsKKO->EOF())
			{
				$Erg .= ', '.$rsKKO->FeldInhalt('KKO_WERT');
				$rsKKO->DSWeiter();
			}
			return substr($Erg,2);

		}
		else
		{
			$BindeVariablen['var_N0_kot_key']=intval('0'.$Info);
			$SQL .= ' AND KKO_KOT_KEY = :var_N0_kot_key';
		}
		$rsKKO = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
		return $rsKKO->FeldInhalt('KKO_WERT');
	}
}




?>