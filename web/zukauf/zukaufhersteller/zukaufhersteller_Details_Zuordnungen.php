<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZHZ','%');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10012 = $AWISBenutzer->HatDasRecht(10012);		// Artikelstamm
	if(($Recht10012&16)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZHZ_LIE_NR DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT ZUKAUFHERSTELLERZUORDNUNGEN.*, LIE_NAME1, LIE_NAME2';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZUKAUFHERSTELLERZUORDNUNGEN ';
	$SQL .= ' INNER JOIN Lieferanten ON ZHZ_LIE_NR = LIE_NR';
	$SQL .= ' WHERE ZHZ_ZLH_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['Edit']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['Edit']);
		$SQL .= ' AND ZHZ_KEY = 0'.$AWIS_KEY2;
	}



	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsZHZ = $DB->RecordsetOeffnen($SQL);
	$Form->Formular_Start();

	if(!isset($_GET['Edit']))
	{
		$Form->ZeileStart();
		if((intval($Recht10012)&6)!=0)
		{
			$Icons[] = array('new','./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Zuordnungen&ZLH_KEY='.$AWIS_KEY1.'&Edit=-1');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
  		}

  		$Link = './zukaufhersteller_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZHZ_LIE_NR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZHZ_LIE_NR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZHZ']['ZHZ_LIE_NR'],200,'',$Link);
  		$Link = './zukaufhersteller_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=LIE_NAME1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='LIE_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'],290,'',$Link);
  		$Link = './zukaufhersteller_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZHZ_SORTIERUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZHZ_SORTIERUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZHZ']['ZHZ_SORTIERUNG'],150,'',$Link);
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;
		$Recht605 = $AWISBenutzer->HatDasRecht(605);		// Lieferantenstammdaten
		$Link = '';
		$DS=0;
		while(!$rsZHZ->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht10012&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Zuordnungen&ZLH_KEY='.$AWIS_KEY1.'&Edit='.$rsZHZ->FeldInhalt('ZHZ_KEY'));
			}
			if(intval($Recht10012&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Zuordnungen&ZLH_KEY='.$AWIS_KEY1.'&Del='.$rsZHZ->FeldInhalt('ZHZ_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			if($Recht605>0)
			{
				$Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR='.$rsZHZ->FeldInhalt('ZHZ_LIE_NR');
			}
			$Form->Erstelle_ListenFeld('*ZHZ_LIE_NR',$rsZHZ->FeldInhalt('ZHZ_LIE_NR'),20,200,false,($DS%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('*LIE_NAME1',substr($rsZHZ->FeldInhalt('LIE_NAME1'),0,20),20,290,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZHZ_SORTIERUNG',$rsZHZ->FeldInhalt('ZHZ_SORTIERUNG'),20,150,false,($DS%2),'','','T');

			$Form->ZeileEnde();

			$rsZHZ->DSWeiter();
			$DS++;
		}


		$Link = './zukaufhersteller_Main.php?cmdAktion=Details&Seite=Historie';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsZHZ->AnzahlDatensaetze()==1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht10012&6);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Zuordnungen&SListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZHZ->FeldInhalt('ZHZ_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZHZ->FeldInhalt('ZHZ_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->Erstelle_HiddenFeld('ZHZ_KEY',$rsZHZ->Feldinhalt('ZHZ_KEY'));
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZHZ']['ZHZ_LIE_NR'].':',200);
		$SQL = 'SELECT LIE_NR, LIE_NAME1 || COALESCE(\', \' || LIE_NAME2, \'\') || \'(\' || LIE_NR || \')\' AS Lieferant';
		$SQL .= ' FROM Lieferanten';
		$SQL .= ' WHERE (NOT EXISTS(SELECT * FROM lieferanteninfos WHERE lin_lie_nr = lie_nr AND lin_ity_key = 1)';
		$SQL .= ' AND NOT EXISTS(SELECT * FROM zukaufherstellercodes WHERE zhk_zlh_key = '.$AWIS_KEY1.' AND zhk_lie_nr = lie_nr)';
		$SQL .= ' AND NOT EXISTS (SELECT * FROM ZUKAUFHERSTELLERZUORDNUNGEN WHERE ZHZ_LIE_NR = LIE_NR AND ZHZ_ZLH_KEY = '.$AWIS_KEY1.')';
		$SQL .= ' ) OR LIE_Nr = \''.$rsZHZ->FeldInhalt('ZHZ_LIE_NR').'\'';
		$SQL .= ' ORDER BY LIE_NAME1, LIE_NAME2';

		$AWISCursorPosition = 'txtZHZ_LIE_NR';
		$DB->SetzeBindevariable('LIE', 'var_T_ZHZ_LIE_NR', $rsZHZ->FeldInhalt('ZHZ_LIE_NR'));
		$Form->Erstelle_SelectFeld('!ZHZ_LIE_NR',$rsZHZ->FeldInhalt('ZHZ_LIE_NR'),300,$EditModus,$SQL,'', '', '', '', '', '', '');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZHZ']['ZHZ_SORTIERUNG'].':',200);
		$Form->Erstelle_TextFeld('!ZHZ_SORTIERUNG',$rsZHZ->FeldInhalt('ZHZ_SORTIERUNG'),3,300,$EditModus,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZHZ']['ZHZ_BEMERKUNG'].':',200);
		$Form->Erstelle_TextFeld('ZHZ_BEMERKUNG',$rsZHZ->FeldInhalt('ZHZ_BEMERKUNG'),80,800,$EditModus,'','','','T');
		$Form->ZeileEnde();

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>