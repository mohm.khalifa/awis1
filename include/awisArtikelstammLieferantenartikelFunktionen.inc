<?php
require_once 'awisFormular.inc';
require_once 'awisDatenbank.inc';
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 04.11.2016
 * Time: 16:23
 */
class awisArtikelstammLieferantenartikelFunktionen
{

    private $_Form;
    private $_DB;

    function __construct()
    {
        $this->_Form = new awisFormular();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
    }

    public function TecDocHinweis($LieNr){

        $SQL = 'Select LIN_KEY from lieferanteninfos where lin_ity_key = 317 and lin_lie_nr = ' .$this->_DB->WertSetzen('LIN','T',$LieNr);

        if($this->_DB->ErmittleZeilenAnzahl($SQL,$this->_DB->Bindevariablen('LIN'))==0){
            $this->_Form->ZeileStart();
            $Text = str_replace('#LINK#','',$this->_Form->LadeTextBaustein('LAR', 'LAR_HINWEIS_KEIN_TECDOC'));
            $this->_Form->Hinweistext($Text,awisFormular::HINWEISTEXT_HINWEIS,'font-weight:bolder');
            $this->_Form->ZeileEnde();
        }


    }

}