<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

$TextKonserven = array();
$TextKonserven[]=array('XBT','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

try
{
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht1 = $AWISBenutzer->HatDasRecht(1);
	if($Recht1==0)
	{
	    awisEreignis(3,1000,'BenutzerOptionen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}


	$Form->Formular_Start();

	$EditRecht=(($Recht1&128)!=0);

	$SQL = 'SELECT *';
	$SQL .= ' FROM BenutzerOptionenTypen';
	$SQL .= ' LEFT OUTER JOIN BenutzerOptionen ON XBT_ID = XBO_XBT_ID AND XBO_XBN_KEY='.$AWIS_KEY1;
	$SQL .= ' WHERE XBT_STATUS<>\'I\'';
	$SQL .= ' ORDER BY XBT_VERWENDUNG, XBT_SORTIERUNG';
	$rsXBO = $DB->RecordSetOeffnen($SQL);

	$LetzterBereich = '';
	while(!$rsXBO->EOF())
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($rsXBO->FeldInhalt('XBT_BEZEICHNUNG').':',200);
		$EditModus=True;
		if(($AWISBenutzer->HatDasRecht($rsXBO->FeldInhalt('XBT_XRC_ID'))&$rsXBO->FeldInhalt('XBT_STUFE'))==0)
		{
			$EditModus=false;
		}

		if($rsXBO->FeldInhalt('XBT_DATENQUELLE')!='')
		{
			switch(substr($rsXBO->FeldInhalt('XBT_DATENQUELLE'),0,3))
			{
				case 'TXT':
					$Felder = explode(':',$rsXBO->FeldInhalt('XBT_DATENQUELLE'));
					$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
					$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
					$Form->Erstelle_SelectFeld('XBO_WERT_'.$rsXBO->FeldInhalt('XBT_ID').'_'.$rsXBO->FeldInhalt('XBO_KEY').'_'.$rsXBO->FeldInhalt('XBT_FORMAT'),$rsXBO->FeldInhalt('XBO_WERT'),$rsXBO->FeldInhalt('XBT_BREITE'),$EditModus,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
					break;
				case 'SQL':
					$Felder = explode(':',$rsXBO->FeldInhalt('XBT_DATENQUELLE'));
					$Form->Erstelle_SelectFeld('XBO_WERT_'.$rsXBO->FeldInhalt('XBT_ID').'_'.$rsXBO->FeldInhalt('XBO_KEY').'_'.$rsXBO->FeldInhalt('XBT_FORMAT'),$rsXBO->FeldInhalt('XBO_WERT'),$rsXBO->FeldInhalt('XBT_BREITE'),$EditModus,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
					break;
				default:
					$Form->Erstelle_TextFeld('XBO_WERT_'.$rsXBO->FeldInhalt('XBT_ID').'_'.$rsXBO->FeldInhalt('XBO_KEY').'_'.$rsXBO->FeldInhalt('XBT_FORMAT'),$rsXBO->FeldInhalt('XBO_WERT'),$rsXBO->FeldInhalt('XBT_ZEICHEN'),$rsXBO->FeldInhalt('XBT_BREITE'),$EditModus,'','','',$rsXBO->FeldInhalt('XBT_FORMAT'),'','',$rsXBO->FeldInhalt('XBT_DATENQUELLE'));
					break;
			}
		}
		else
		{
			$Form->Erstelle_TextFeld('XBO_WERT_'.$rsXBO->FeldInhalt('XBT_ID').'_'.$rsXBO->FeldInhalt('XBO_KEY').'_'.$rsXBO->FeldInhalt('XBT_FORMAT'),$rsXBO->FeldInhalt('XBO_WERT'),$rsXBO->FeldInhalt('XBT_ZEICHEN'),$rsXBO->FeldInhalt('XBT_BREITE'),$EditRecht,'','','',$rsXBO->FeldInhalt('XBT_FORMAT'));
		}
		$Form->ZeileEnde();
		$rsXBO->DSWeiter();
	}

	$Form->Formular_Ende();
}
catch (Exception $ex)
{
	if(is_object($Form))
	{
		$Form->Formular_Start();
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'WIEDERHOLEN',0);
		$Form->Formular_Ende();
	}
}
?>