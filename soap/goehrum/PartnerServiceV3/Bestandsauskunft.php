<?php

class Bestandsauskunft
{

    /**
     * @var SuchAnfrage $ArtikelAnfrage
     */
    protected $ArtikelAnfrage = null;

    /**
     * @param SuchAnfrage $ArtikelAnfrage
     */
    public function __construct($ArtikelAnfrage)
    {
      $this->ArtikelAnfrage = $ArtikelAnfrage;
    }

    /**
     * @return SuchAnfrage
     */
    public function getArtikelAnfrage()
    {
      return $this->ArtikelAnfrage;
    }

    /**
     * @param SuchAnfrage $ArtikelAnfrage
     * @return Bestandsauskunft
     */
    public function setArtikelAnfrage($ArtikelAnfrage)
    {
      $this->ArtikelAnfrage = $ArtikelAnfrage;
      return $this;
    }

}
