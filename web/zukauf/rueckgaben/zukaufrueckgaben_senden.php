<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('ZRU','*');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10050 = $AWISBenutzer->HatDasRecht(10050);
	if($Recht10050==0)
	{
	    awisEreignis(3,1000,'Zukaufrueckgaben',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	// Die R�ckgabe laden, um alle Daten zu bekommen
	$ZRU_KEY = $DB->FeldInhaltFormat('N0',$_GET['ZRU_KEY'],false);
	$SQL = 'SELECT Zukaufrueckgaben.*,ZUL_LIE_NR';
	$SQL .= ' FROM Zukaufrueckgaben ';
	$SQL .= ' INNER JOIN Zukauflieferscheine ON ZRU_ZUL_KEY = ZUL_KEY ';
	$SQL .= ' WHERE ZRU_KEY=:var_N0_zru_key';

	$BindeVariablen = array();
	$BindeVariablen['var_N0_zru_key']=$ZRU_KEY;
	$rsZRU = $DB->RecordSetOeffnen($SQL,$BindeVariablen);

	// Je nach Lieferant andere Aktivit�ten ausl�sen
	switch($rsZRU->FeldInhalt('ZUL_LIE_NR'))
	{
	    case '8252':		// Werthenbach -> E-Mail
	    case '2620':		// Birner zum Testen
	    case '8164':		// W�tschner zum TEST
	        include 'zukaufrueckgaben_mailsenden_8252.php';
	        break;
	}
	// Bericht manuell laden
	require_once '/daten/web/berichte/ber_ZukaufRueckgaben.inc';
	
	$BerObj = new ber_ZukaufRueckgaben($AWISBenutzer, $DB, 1);
	$Parameter['ZRU_KEY']="=~".$ZRU_KEY;
	$BerObj->init($Parameter);
	$BerObj->ErzeugePDF();
	
	$SQL = 'UPDATE Zukuafrueckgaben';
	$SQL .= ' SET ZRU_STATUS = 9';
	$SQL .= ' WHERE ZRU_KEY = :var_N0_ZRU_KEY';
	$DB->SetzeBindevariablen('ZUR','var_N0_ZRU_KEY',$ZRU_KEY);
	$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('ZUR'));
	
	
	die();	// Ende erzwingen wegen der Berichtsausgabe
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201229051011");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>