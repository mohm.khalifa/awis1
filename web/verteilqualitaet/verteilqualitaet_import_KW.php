<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);
date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class VERTEILQUALITAETIMPORT
{

private static $instance = NULL;
private $_DB;            
protected $_AWISBenutzer;
private $flagIsSunday = false;	
private $dirPath = '../../../daten/daten/pccommon/Beierl/FILPEPIS';	
private $importFile = '';
private $WerbeFilPers = array();
protected $aktZeile;
protected $aktKW = '';
protected $zuImportierendeKW = '';
protected $KWDateien = array();
protected $KWDeuDateien = array();
protected $KWOesDateien = array();


 public function __construct() {
  $this->_AWISBenutzer = awisBenutzer::Init();
  $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
  $this->_DB->Oeffnen();
}

  
public function getInstance() {
   if(self::$instance === NULL) {
      self::$instance = new self;
   }

   return self::$instance;
}

private function __clone() {}
  


public function import()
{
	$this->isSunday();

	if($this->flagIsSunday == true)
	{
		$this->identifyFile();	
		
		$this->delWERBEFILPERS();
		echo "LOAD --> FILES(0)\n";
		$this->loadFile();
		$this->bereinigeDS();
		
	}
	else 
	{
		echo "KEIN RUN EXIT-->CODE(0)";		
	}
	
}

private function loadFile()
{
	$fd = null;
	
	echo "LOADFILE";

	echo "PFAD_DIR".$this->dirPath;
	//echo "Importdatei".$this->importFile;
	
	var_dump($this->KWDateien);
	
	"ANZAHL:".count($this->KWDateien);
	
	for($i=0;$i<count($this->KWDateien);$i++)
	{
		echo $this->importFile = $this->KWDateien[$i];
		
    	if(($fd = fopen($this->dirPath."/".$this->importFile,'r')) === false) {
      		echo "Fehler beim �ffnen der Datei";      
    	}
    	else {
		   while(! feof($fd)) {
                $this->aktZeile = fgets($fd);
                $this->WerbeFilPers = explode(';',$this->aktZeile);
				$this->safeDS();
            }
            fclose($fd);   	 
    	}
	}
		
}

private function delWERBEFILPERS()
{
	$SQL = "TRUNCATE TABLE WERBEFILPERS";
	
 	if($this->_DB->Ausfuehren($SQL)===false) {
    }
}

public function bereinigeDS()
{
	echo "Bereinige";
	
	$SQL = 'Select DISTINCT WPF_KUERZEL FROM WERBEFILTERPERS';
	
	$rsKuerzel = $this->_DB->RecordSetOeffnen($SQL);
	
	while(!$rsKuerzel->EOF())
	{
		$SQL = 'Delete from WERBEFILPERS WHERE WFP_PERSNR IN  ';
		$SQL .= '(Select WFP_PERSNR FROM WERBEFILPERS WHERE ';
		$SQL .= ' WFP_MO ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_DI ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));	
		$SQL .= ' AND WFP_MI ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_DO ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_FR ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_SA ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' )';
		
		if($this->_DB->Ausfuehren($SQL)===false) {
    	}
		
		$rsKuerzel->DSWeiter();
	}
	
	//$SQL = 'Delete from WERBEFILPERS WHERE WFP_PERSNR IN  ';
	$SQL = 'Select WFP_FILNR,WFP_PERSNR FROM WERBEFILPERS WHERE ';
	$SQL .= ' WFP_MO is null ';
	$SQL .= ' AND WFP_DI is null ';	
	$SQL .= ' AND WFP_MI is null ';
	$SQL .= ' AND WFP_DO is null ';
	$SQL .= ' AND WFP_FR is null ';
	$SQL .= ' AND WFP_SA is null ';
	//$SQL .= ' )';
	
	$rsLoesche = $this->_DB->RecordSetOeffnen($SQL);
	
	while(!$rsLoesche->EOF())
	{
		$SQL = 'Delete from WERBEFILPERS WHERE WFP_FILNR='.$this->_DB->FeldInhaltFormat('NO',$rsLoesche->FeldInhalt('WFP_FILNR'));
		$SQL .= ' AND WFP_PERSNR='.$this->_DB->FeldInhaltFormat('T',$rsLoesche->FeldInhalt('WFP_PERSNR'));
		
		if($this->_DB->Ausfuehren($SQL)===false) {
    	}
		
		$rsLoesche->DSWeiter();
	}
	
	if($this->_DB->Ausfuehren($SQL)===false) {
    }
	
}


private function safeDS()
{
	$SQL = "INSERT INTO WERBEFILPERS (WFP_FILNR,WFP_PERSNR,WFP_NAME,WFP_MO,";
    $SQL .= "WFP_DI,WFP_MI,WFP_DO,WFP_FR,WFP_SA,";
    $SQL .= "WFP_USER,WFP_USERDAT) VALUES (";
    $SQL .= ' ' . $this->_DB->FeldInhaltFormat('NO',$this->WerbeFilPers[0],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[1],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[2],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[3],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[4],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[5],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[6],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[7],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[8],false);
    $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
    $SQL .= ',SYSDATE';
    $SQL .= ')';
	
     if($this->_DB->Ausfuehren($SQL)===false) {
     }

    array_splice($this->WerbeFilPers,0);
    
}



private function identifyFile()
{

		$iDeu=0;
		$iOes=0;
		
		if ($handle = opendir($this->dirPath)) {
    	while (false !== ($file = readdir($handle))) {
    		
    		if ($file != "." && $file != "..") {
        		
    			$landKZ = substr($file,6,3);
    			echo "Land:".$landKZ."\n";
    			
        		$kw = substr($file,14,2)."\n";		
        		
        		echo $file."\n";
        		echo "KW:".$kw;
        		echo "ErwartetKW:".$this->zuImportierendeKW."\n";
        		
        		echo "TYPKW:".gettype($kw)."\n";
        		echo "TYPAN:".gettype($this->zuImportierendeKW)."\n";
        		
        		if((intval($kw) == intval($this->zuImportierendeKW)) && $landKZ == 'deu')
        		{
        			echo "OK-ARRAY:".$this->KWDeuDateien[$iDeu] = $file;
        			$iDeu++;
        			echo "I-WERT:".$iDeu;
        		}
        		elseif((intval($kw) == intval($this->zuImportierendeKW)) && $landKZ == 'oes')
        		{
        			echo "OK-ARRAY:".$this->KWOesDateien[$iOes] = $file;
        			$iOes++;
        			echo "I-WERT:".$iOes;
        		}
        		else
        		{
        		}
    	}
       }
       closedir($handle);
    }

    echo "DeutscheDateien:";
    var_dump($this->KWDeuDateien);
    
    echo "Oesterreichische Dateien:";
    var_dump($this->KWOesDateien);
    
    $MaxDeuDate = '';
    $DeuDateiName = '';
    
    for($i=0;$i<count($this->KWDeuDateien);$i++)
    {
    	$aktDateiDatum = substr($this->KWDeuDateien[$i],17,6);
    	
    	if($i == 0)
    	{
    		$MaxDeuDate = $aktDateiDatum;
    		$DeuDateiName = $this->KWDeuDateien[$i];
    	}
    	elseif($MaxDeuDate < $aktDateiDatum)
    	{
    		$MaxDeuDate = $aktDateiDatum;
    		$DeuDateiName = $this->KWDeuDateien[$i];
    	}
    }
    
    $this->KWDateien[0] = $DeuDateiName;
    
    
    $MaxOesDate = '';
    $OesDateiName='';
    
	for($i=0;$i<count($this->KWOesDateien);$i++)
    {
    	$aktDateiDatum = substr($this->KWOesDateien[$i],17,6);
    	
    	if($i == 0)
    	{
    		$MaxOesDate = $aktDateiDatum;
    		$OesDateiName = $this->KWOesDateien[$i];
    	}
    	elseif($MaxOesDate < $aktDateiDatum)
    	{
    		$MaxOesDate = $aktDateiDatum;
    		$OesDateiName = $this->KWOesDateien[$i];
    	}
    }
    
    $this->KWDateien[1] = $OesDateiName;

    //Ermittlung abgeschlossen
    
    echo "--------------------";
    var_dump($this->KWDateien);
    
} 


private function isSunday()
{
	if(date('D') == 'Tue')
	{
		$this->flagIsSunday = true;
		
		$this->aktKW = date('W');
		echo "Kalenderwoche:".$this->aktKW."\n";
		$this->zuImportierendeKW = $this->aktKW;
		$this->zuImportierendeKW = '0'.$this->zuImportierendeKW;
		
		echo "zuImportierendeKW".$this->zuImportierendeKW;
		
	}
	else 
	{
		$this->flagIsSunday = false;		
	}
}



}

?>