<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZAI','%');
	$TextKonserven[]=array('AID','AID_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);		// Artikelstamm
	if(($Recht10001&1)==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY AID_BEZEICHNUNG, ZAI_ID';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT ZukaufLieferantenArtikelIds.*, AID_BEZEICHNUNG';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZukaufLieferantenArtikelIds ';
	$SQL .= ' INNER JOIN ArtikelStammIdTypen ON AID_NR = ZAI_AID_NR';
	$SQL .= ' WHERE ZAI_ZLA_KEY=0'.$AWIS_KEY1;

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZAI',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsZAI = $DB->RecordSetOeffnen($SQL);

//$Form->DebugAusgabe(1,$SQL);
	$Form->Formular_Start();

	if(!$rsZAI->EOF())
	{
		$Form->Formular_Start();

		$Form->ZeileStart();
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZAI_ID'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAI_ID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAI']['ZAI_ID'],250,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=AID_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='AID_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AID']['AID_BEZEICHNUNG'],250,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZAI_USER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAI_USER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAI']['ZAI_USER'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&SSort=ZAI_USERDAT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAI_USERDAT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAI']['ZAI_USERDAT'],100,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZAI->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('#ZAI_ID',$rsZAI->FeldInhalt('ZAI_ID'),0,250,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('#AID_BEZEICHNUNG',$rsZAI->FeldInhalt('AID_BEZEICHNUNG'),0,250,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('#ZAI_USER',$rsZAI->FeldInhalt('ZAI_USER'),0,150,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('#ZAI_USERDAT',$rsZAI->FeldInhalt('ZAI_USERDAT'),0,100,false,($DS%2),'','','D');
			$Form->ZeileEnde();

			$rsZAI->DSWeiter();
			$DS++;
		}

		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=IDs';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}

	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181311");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181309");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

?>