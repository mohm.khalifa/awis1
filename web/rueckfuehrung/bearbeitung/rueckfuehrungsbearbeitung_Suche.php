<?php
global $AWISCursorPosition;
global $AWISBenutzer;


$Form = new awisFormular();


// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('RFK','%');
$TextKonserven[]=array('RFF','%');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','txt_OhneZuordnung');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Status');
$TextKonserven[]=array('RFS','lst_RFS_STATUS');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht3920=$AWISBenutzer->HatDasRecht(3920);

echo "<br>";

echo "<form name=frmSuche method=post action=./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details>";


/**********************************************
* * Eingabemaske
***********************************************/
$Param = unserialize($AWISBenutzer->ParameterLesen('RFKSuche'));

$Form->Formular_Start();

//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_FIL_ID'].':',210);
if($UserFilialen!='')
{	
	$Form->Erstelle_TextFeld('*RFK_FIL_ID',$UserFilialen,20,200,false,'','','','T');
	echo '<input type=hidden name="sucRFK_FIL_ID" value="'.$UserFilialen.'">';
}
else 
{
	$Form->Erstelle_TextFeld('*RFK_FIL_ID',($Param['SPEICHERN']=='on'?$Param['RFK_FIL_ID']:''),20,200,true);	
	$AWISCursorPosition='sucRFK_FIL_ID';
}
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_RUECKFUEHRUNGSNR'].':',210);
$Form->Erstelle_TextFeld('*RFK_RUECKFUEHRUNGSNR',($Param['SPEICHERN']=='on'?$Param['RFK_RUECKFUEHRUNGSNR']:''),20,200,true);
$Form->ZeileEnde();	

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Status'].':',210);
//$SQL = 'SELECT RFS_KEY, RFS_BEZEICHNUNG FROM RueckFuehrungsStatus WHERE RFS_STATUS in (30,31,32,33) ORDER BY RFS_BEZEICHNUNG';
$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFS_STATUS']);
$Form->Erstelle_SelectFeld('*RFS_STATUS',($Param['SPEICHERN']=='on'?$Param['RFS_STATUS']:''),100,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$StatusText,'');
//$Form->Erstelle_SelectFeld('*RFK_RFS_KEY',($Param['SPEICHERN']=='on'?$Param['RFK_RFS_KEY']:''),100,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_BOXART'].':',210);
$StatusText = explode("|",$AWISSprachKonserven['RFK']['lst_RFK_BOXART']);
$Form->Erstelle_SelectFeld('*RFK_BOXART',($Param['SPEICHERN']=='on'?$Param['RFK_BOXART']:''),200,true,'','0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$StatusText,'');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_BOXEAN'].':',210);
$Form->Erstelle_TextFeld('*RFK_BOXEAN',($Param['SPEICHERN']=='on'?$Param['RFK_BOXEAN']:''),20,200,true);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_AST_ATUNR'].':',210);
$Form->Erstelle_TextFeld('*RFF_AST_ATUNR',($Param['SPEICHERN']=='on'?$Param['RFF_AST_ATUNR']:''),20,200,true);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_DATUMABSCHLUSSFIL'].':',210);
$Form->Erstelle_TextFeld('*RFK_DATUMABSCHLUSSFIL','',20,200,true,'','','','D');
$Form->ZeileEnde();
	
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_DATUMABSCHLUSSZEN'].':',210);
$Form->Erstelle_TextFeld('*RFK_DATUMABSCHLUSSZEN','',20,200,true,'','','','D');
$Form->ZeileEnde();

if (($Recht3920&32)==32)
{
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_BEMERKUNGZENTRALE'].':',210);
	$Form->Erstelle_TextFeld('*RFF_BEMERKUNGZENTRALE',($Param['SPEICHERN']=='on'?$Param['RFF_BEMERKUNGZENTRALE']:''),20,200,true);
	$Form->ZeileEnde();
}

/*
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['NEGATIVBESTANDFILIALE'].':',200);
$Form->Erstelle_Checkbox('*NegativBestandFiliale',($Param['SPEICHERN']=='on'?$Param['NEGATIVBESTANDFILIALE']:''),20,true,'on');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['NEGATIVBESTANDZENTRALE'].':',200);
$Form->Erstelle_Checkbox('*NegativBestandZentrale',($Param['SPEICHERN']=='on'?$Param['NEGATIVBESTANDZENTRALE']:''),20,true,'on');
$Form->ZeileEnde();
*/
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',210);
$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
$Form->ZeileEnde();

$Form->Formular_Ende();

$Form->SchaltflaechenStart();

$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

if(($Recht3920&4) == 4)		// Hinzuf�gen erlaubt?
{
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=rueckfuehrungsbearbeitung&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

$Form->SchaltflaechenEnde();

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}
?>
