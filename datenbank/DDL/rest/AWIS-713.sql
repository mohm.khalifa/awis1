create table ACTIVEDIRECTORYVERTEILER (
  ADV_SURNAME VARCHAR2(300),
  ADV_MAIL VARCHAR2(300)
);

commit;


INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-A.T.U-Academy-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-AG-Mobil-Center-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Autoglas-Teamleiter-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-alle-Mail-User-Filiale@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-alle-Mail-User-Zentrale@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-alle-Mail-User-Zentrale-AT@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-alle-Mail-User-Zentrale-CH@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Alle-Mail-User-Zentrale-DE@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-AQM-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Ausland-CH-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Aussendienst-GL-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Aussendienst-WL-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Autoglas-Monteure-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-AX-Filiale-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-AX-Projektteam-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-AX-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Bestellwesen-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-BR-Filialinfo-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-BR-K11-OG2-5548-04-Personal-BVW@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-BR-Zentrale-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-CallCenter-QualityComm@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Controlling-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Debitorenmanagement-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-E-Commerce-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-AT-ALLE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-CH-ALLE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-DE-ALLE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-GL-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-GL-CH-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-GL-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Filiale-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Filiale-CH-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Filiale-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-Personal-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-Personal-CH-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-Personal-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-WL-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-WL-CH-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FIL-WL-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Finanzbuchhaltung-Verteiler@ads.atu.de');

commit;

INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Flotten-Grosskunden-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Freigestellte-BR-Weiden@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-FSRMReports@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Fuehrungskraefte-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Fuhrpark-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBL-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBL-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBL-Nord-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBL-Ost-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBL-Suedost-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBL-Suedwest-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBL-West-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBR-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBV-Praemie-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-GBV-Urlaub-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('AT','Global-Hausmeister-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('DE','Global-Hausmeister-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Human-Resources-Business-Partner-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('IT','Global-IT-6300-Leitung@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6311-Infrastruktur-Operations@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6312-Service-Desk@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6321-Filialsysteme@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6322-DWH-Operating@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6323-Projektmanagement@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6324-Digital-Development-Weiden@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6325-Business-Support@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6330-IT-Controlling@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-6340-Digital-Development-Berlin@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-ALL@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-DWH-Verteiler@de.atu.eu');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-Operating@de.atu.eu');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-Stoerung-Alle-Filialen-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-IT-Stoerung-Neues-Filialsystem-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Kommissionierplan-Logistik-Weiden-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Kompetenzfilialen-Autogas-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Kostenstellenabschluss-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-LL-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Neues-Filialsystem-UHD-Report@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Orga-SR-BR-Pflege@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-RB-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Reach-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Rechnungspruefung-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','global-revision-verteiler@ads.atu.de');

commit;

INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','RGZ-Deutschland@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-RL-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Rollout-Neues-Filialsystem@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Rollout-Neues-Filialsystem-WWS@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-RPL-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('SAP-Infrastr.-Vert.','sap.infrastruktur@de.atu.eu');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('SAP-Teamleiter-Vert.	','sap.teamleiter@de.atu.eu');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('SAP-Team-Vert.	','sap.team@de.atu.eu');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Sonstige-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Smartrepair-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Stoerung-Filiale-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Sysstate-Microsoft-Events-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-TGH-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-TKDL-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-TKDL-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-TK-Stoerung-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-User-PCCommon-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Vertrieb-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Werl-Alle-Filialen-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Wartungstechniker-AT-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Wartungstechniker-DE-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Zahlungsverkehr-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Zentrale-Verteiler@ads.atu.de');
INSERT INTO activedirectoryverteiler ( adv_surname,adv_mail ) VALUES ('','Global-Zentrale-Weiden-Verteiler@ads.atu.de');


commit;