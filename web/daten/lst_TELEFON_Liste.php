<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['suc_telBereich']))
{
    die('##PARAM##');
}

try 
{
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();



    // Rechte f�r die einzelnen Stufen
    $Recht150 = $AWISBenutzer->HatDasRecht(150);
    $TelefonverzeichnisStufe = $AWISBenutzer->HatDasRecht(151);
    
    $Suchbegriff = $_GET['suc_telBereich'];
    
    $Nachname = '';
    $Telefon = '';
    $EMail = '';
    
    if(is_numeric(trim($Suchbegriff)))
    {
        $Telefon = trim($Suchbegriff);
    }
    elseif(strpos($Suchbegriff, '@')!==false)
    {
        $EMail = trim($Suchbegriff);
    }
    else
    {
        $Nachname = trim($Suchbegriff);
    }    
    
    
    $SQL = 'SELECT KON_NAME1 || COALESCE(\', \' || KON_NAME2,\'\') AS KON_NAME';
    $SQL .= ', (SELECT MAX(KKO_WERT) FROM KONTAKTEKOMMUNIKATION WHERE KKO_KON_KEY =KON_KEY AND KKO_KOT_KEY = 9) AS DURCHWAHL';          // Durchwahl
    $SQL .= ', (SELECT MAX(KKO_WERT) FROM KONTAKTEKOMMUNIKATION WHERE KKO_KON_KEY =KON_KEY AND KKO_KOT_KEY = 2) AS FIRMENTELEFON';      // Firmennummer
    $SQL .= ', (SELECT MAX(KKO_WERT) FROM KONTAKTEKOMMUNIKATION WHERE KKO_KON_KEY =KON_KEY AND KKO_KOT_KEY = 7) AS EMAIL';      
    $SQL .= ', (SELECT listagg(KAB_ABTEILUNG,\',\') within group(order by KAB_KEY) FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE KZA_KON_KEY= KON_KEY) AS ABTEILUNGEN';
    $SQL .= ' FROM KONTAKTE';
    
    $Bedingung = '';
    
    if($Nachname!='')
    {
        $Vorname = '';
        $Nachname = $Suchbegriff;

        if(strpos($Suchbegriff, ','))
        {
            $Daten = explode(',', $Suchbegriff);
            $Nachname = $Daten[0];
            $Vorname = $Daten[1];
        }
        
        if(strpos($Nachname,' ')!==false)
        {
            $Nachnamen = explode(' ',$Nachname);            
        }
        else 
        {
            $Nachnamen = array($Nachname);
        }
        
        if($Vorname!='' AND strpos($Vorname,' ')!==false)
        {
            $Vornamen = explode(' ',$Vorname);            
        }
        else 
        {
            $Vornamen = array($Vorname);
        }
        
        $UnterBedingung = '';
        foreach($Nachnamen AS $Nachname)
        {
            // Vor und Nachname in beiden Felder suchen
            $UnterBedingung  .= ' OR  (REGEXP_LIKE(KON_NAME1, '.$DB->WertSetzen('KON','T', $Nachname).',\'i\')';
            $VornameBedingung = '';
            
            if($Vornamen!='')
            {
                $VornameBedingung='';
                foreach($Vornamen AS $Vorname)
                {
                    if($Vorname!='')
                    {
                        $VornameBedingung .= ' OR  REGEXP_LIKE(KON_NAME2, '.$DB->WertSetzen('KON','T', $Vorname).',\'i\')';
                    }
                }
                if($VornameBedingung!='')
                {
                    $UnterBedingung .= ' AND ('.substr($VornameBedingung, 4).')';
                }
            }
            $UnterBedingung  .= ')';
        }
        $Bedingung .= ' AND ('.substr($UnterBedingung,4);
        
        // in Abteilungen, wenn ein einfacher Suchbegriff gew�hlt wird
        if(count($Vornamen)<=1 AND count($Nachnamen)<=1 AND strpos($Suchbegriff,',')===false)
        {
            $Bedingung .= ' OR ';
            $Bedingung .= ' EXISTS(SELECT * FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE KZA_KON_KEY= KON_KEY AND REGEXP_LIKE(KAB_ABTEILUNG, '.$DB->WertSetzen('KON','T', $Nachname).',\'i\'))';
        }

        $Bedingung .= ' )';
        
    }
    elseif($Telefon!='')
    {
        $Bedingung .= ' AND (REGEXP_LIKE((SELECT MAX(KKO_WERT) FROM KONTAKTEKOMMUNIKATION WHERE KKO_KON_KEY =KON_KEY AND KKO_KOT_KEY = 9), '.$DB->WertSetzen('KON','T', $Telefon).',\'i\')';
        $Bedingung .= '  OR REGEXP_LIKE((SELECT MAX(KKO_WERT) FROM KONTAKTEKOMMUNIKATION WHERE KKO_KON_KEY =KON_KEY AND KKO_KOT_KEY = 2), '.$DB->WertSetzen('KON','T', $Telefon).',\'i\')';
        $Bedingung .= ' )';
    }
    elseif($EMail!='')
    {
        $Bedingung .= ' AND (REGEXP_LIKE((SELECT MAX(KKO_WERT) FROM KONTAKTEKOMMUNIKATION WHERE KKO_KON_KEY =KON_KEY AND KKO_KOT_KEY = 7), '.$DB->WertSetzen('KON','T', $EMail).',\'i\')';
        $Bedingung .= ' )';
    }
    else 
    {
        $Bedingung .= ' AND (REGEXP_LIKE(KON_NAME1, '.$DB->WertSetzen('KON','T', $Suchbegriff).',\'i\')';
        $Bedingung .= '  OR REGEXP_LIKE(KON_NAME2, '.$DB->WertSetzen('KON','T', $Suchbegriff).',\'i\'))';
    }
    
    $Bedingung .= ' AND KON_STATUS = \'A\'';

    //Filialliste abfragen
    if (($Recht150&64)==64){
        $Bedingung .= ' AND BITAND(KON_LISTEN,2)>0';
    }
    
    if($Bedingung!='')
    {
        $SQL .= ' WHERE '.substr($Bedingung, 4);
    }
    
    
    
    $rsKON = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('KON'));
    $DS=0;
    
    while(!$rsKON->EOF())
    {
        //echo '<div class="telBereichTreffer">';
        
        $DS=1;
        
        $Form->ZeileStart('font-weight:bold;margin-top:5px;background-color:white;');
        $Form->Erstelle_ListenFeld('KON_NAME', $rsKON->FeldInhalt('KON_NAME'), 0, 540, false, ($DS%2),'font-size:14px;');
        //echo '<div>'.$rsKON->FeldInhalt('KON_NAME').'</div>';
        $Form->ZeileEnde();
        
        if($rsKON->FeldInhalt('ABTEILUNGEN')!='')
        {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('TELEFON', '', 0, 10, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', 'Abteilung:', 0, 130, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', $rsKON->FeldInhalt('ABTEILUNGEN'), 0, 400, false, ($DS%2));
            //echo '&nbsp;&nbsp;<strong>Durchwahl:</strong>'.$rsKON->FeldInhalt('DURCHWAHL');
            $Form->ZeileEnde();
        }
        
        if($rsKON->FeldInhalt('DURCHWAHL')!='')
        {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('TELEFON', '', 0, 10, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', 'Durchwahl:', 0, 130, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', $rsKON->FeldInhalt('DURCHWAHL'), 0, 400, false, ($DS%2));
            //echo '&nbsp;&nbsp;<strong>Durchwahl:</strong>'.$rsKON->FeldInhalt('DURCHWAHL');
            $Form->ZeileEnde();
        }
        
        if($rsKON->FeldInhalt('FIRMENTELEFON')!='')
        {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('TELEFON', '', 0, 10, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', 'Telefon Gesch&auml;ft:', 0, 130, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', $rsKON->FeldInhalt('FIRMENTELEFON'), 0, 400, false, ($DS%2));
            $Form->ZeileEnde();
        }
        
        if($rsKON->FeldInhalt('EMAIL')!='')
        {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('TELEFON', '', 0, 10, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', 'E-Mail:', 0, 130, false, ($DS%2));
            $Form->Erstelle_ListenFeld('TELEFON', $rsKON->FeldInhalt('EMAIL'), 0, 400, false, ($DS%2),'','mailto:'.$rsKON->FeldInhalt('EMAIL'));
            $Form->ZeileEnde();
        }
        
        //echo '</div>';
        
        $rsKON->DSWeiter();
        $DS++;
    }
    
   // echo $DB->LetzterSQL();
}
catch(Exception $ex)
{
    echo 'Mmmmmmmist...<br>'.$ex->getMessage();
}
?>