<?php
require_once 'artikelstamm_Werkzeuge_SmartKat_funktionen.php';
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SK;

try {
    $SK = new artikelstamm_Werkzeuge_SmartKat_funktionen();

    if (isset($_POST['cmdSpeichern_x'])) {
        include "artikelstamm_Werkzeuge_SmartKat_Speichern.php";
    }

    $Step = $SK->Form->NameInArray($_POST, 'cmdStep_', 0, 1);
    if ($Step !== false) {
        $Step = explode('_', $Step)[1];
    } else {
        $Step = 1;
    }

    $SK->Form->SchreibeHTMLCode('<form name=frmFreieNummern action=./artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=SmartKatExport method=POST  enctype="multipart/form-data">');
    if (file_exists("artikelstamm_Werkzeuge_SmartKat_Step$Step.php")) {
        include "artikelstamm_Werkzeuge_SmartKat_Step$Step.php";
    } else {
        throw new Exception('Unerwarteter Zugriff.' . serialize($_REQUEST), '-201701171130');
    }

    $SK->Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($SK->Form instanceof awisFormular) {
        $SK->Form->DebugAusgabe(1, $ex->getSQL());
        $SK->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201701171128");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($SK->Form instanceof awisFormular) {
        $SK->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201701171129");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>