<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

/**
 * Klasse awisOT
 * Stellt Methoden zur Verf�gung, um OmniTracker anzusteuern
 * @author Sacha Kerres
 * @copyright ATU
 * @version 11.10.2011
 *
 */
class awisOT
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;

	/**
	 * AWIS Werkzeugobjekt
	 *
	 * @var awisWerkzeuge
	 */
	private $_AWISWerkzeug = null;

	/**
	 * AWIS Benutzer
	 *
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;

	/**
	 * Server-URL
	 * @var string
	 */
	private $_OTServer = '';
	private $_OTServerPort = 5085;
	private $_OTServerUser = '';
	private $_OTServerPass = '';
    private $_OTWSDL = '';

    /**
     * Debuglevel fuer Ausgaben
     * @var int
     */
    private $_DebugLevel = 0;
	/**
	 * SOAP Client f�r den Zugriff
	 * @var SoapClient
	 */
	private $_SOAP_Client = null;

	/**
	 * Formularobjekt zum Formatieren, etc
	 * @var awisFormular
	 */
	private $_Form = null;
	/**
	 * Konstruktor
	 *
	 * @param string $Benutzer
	 */
	public function __construct($AWISBenutzer)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISWerkzeug = new awisWerkzeuge();
		$this->_AWISBenutzer = $AWISBenutzer;
        $this->_Form = new awisFormular();
		$this->_OTServer = $this->_AWISBenutzer->ParameterLesen('OT-Server');
		$this->_OTServerPort = $this->_AWISBenutzer->ParameterLesen('OT-ServerPort');
		$this->_OTServerUser = $this->_AWISBenutzer->ParameterLesen('OT-ServerUser');
		$this->_OTServerPass = $this->_AWISBenutzer->ParameterLesen('OT-ServerPass');
		$this->_OTWSDL = $this->_AWISBenutzer->ParameterLesen('OT-WSDLPfad');

		$WSDL = 'http://'.$this->_OTServer.':'.$this->_OTServerPort.$this->_OTWSDL;
        //$WSDL='http://svappwi033.ads.atu.de/OtWS/v1.asmx?WSDL';
		$Params = array('trace'=>1						// Protokoll
    		, 'exceptions'=> 1
    		, 'soap_version'=>SOAP_1_2
		);

		$this->_SOAP_Client = new SoapClient($WSDL, $Params);
	}

	/**
	 * Testet die Verbindung zum OmniTracker
	 */
	public function VerbindungsTest($Pfad)
	{
		try
		{
			$OTKlasse = new stdClass();
			$OTKlasse->folderPath=$Pfad;
			$OTKlasse->recursive=false;
			$OTKlasse->maximumRecords=1;
			$OTKlasse->ObjectIDs = new stdClass();
			$OTKlasse->ObjectIDs->objectIDs = '1';
			
			$Param = new stdClass();
			$Param->Get = $OTKlasse;
			//$Param->Get->ObjectIDs->objectIDs = '1';
			$Erg = $this->_SOAP_Client->GetObjectList($Param);
			
			if($this->_DebugLevel>1)
			{
				echo 'Teste den Dienst im Pfad '.$Pfad.' ...'.PHP_EOL;
			}
			
			if(isset($Erg->GetObjectListResult))    // richtiges Objekt
			{
				return true;
			}
		}
		catch(SoapFault $ex)
		{
			return false;
		}
		catch(Exception $ex)
		{
			if($this->_DebugLevel>1)
			{
				var_dump($ex->getMessage());
			}
		}
		return false;
	}
	/**
	 * Setzt oder liest den aktuellen Debuglevel
	 * @param (optionl) int $NeuerLevel
	 */
	public function DebugLevel($NeuerLevel = null)
	{
	    if(!is_null($NeuerLevel))
	    {
	        $this->_DebugLevel=(int)$NeuerLevel;
	    }

	    return $this->_DebugLevel;
	}

	/**
	 * Erzeugt ein Ticket im OmniTracker
	 */
	public function ErzeugeTicket($Pfad, array $TicketDaten)
	{
        $OTKlasse = new stdClass();
        $OTKlasse->folderPath=$Pfad;        // Zielpfad in OT Syntax

        $Param = new stdClass();
        $Param->Object = $OTKlasse;

        if(isset($TicketDaten['StringVal']))
        {
	        foreach($TicketDaten['StringVal'] AS $TicketFeld=>$TicketFeldInhalt)
	        {
	            $Feld = new stdClass();
	            $Feld->name = $TicketFeld;
	            $Feld->_ = utf8_encode($TicketFeldInhalt);
	
	            $Param->Object->StringVal[] = $Feld;
	        }
        }
                
        if(isset($TicketDaten['DateTimeVal']))
        {
	        foreach($TicketDaten['DateTimeVal'] AS $TicketFeld=>$TicketFeldInhalt)
	        {
	            $Feld = new stdClass();
	            $Feld->name = $TicketFeld;
	            $Feld->_ = $this->_Form->Format('DI',$TicketFeldInhalt);        // Datum nach ISO
	
	            $Param->Object->DateTimeVal[] = $Feld;
	        }
        }
        
        if(isset($TicketDaten['ShortIntVal']))
        {
            foreach($TicketDaten['ShortIntVal'] AS $TicketFeld=>$TicketFeldInhalt)
            {
                $Feld = new stdClass();
                $Feld->name = $TicketFeld;
                $Feld->_ = $TicketFeldInhalt;

                $Param->Object->ShortIntVal[] = $Feld;
            }
        }
        if(isset($TicketDaten['ReferenceVal']))
        {
            foreach($TicketDaten['ReferenceVal'] AS $TicketFeld=>$TicketFeldInhalt)
            {
                $Feld = new stdClass();
                $Feld->name = $TicketFeld;
                $Feld->objectId = $TicketFeldInhalt;

                $Param->Object->ReferenceVal[] = $Feld;
            }
        }

        // TODO: Geht so nicht, mal andere Varianten auch versuchen!
        if(isset($TicketDaten['ReferenceToUserVal']))
        {
            $AttAnz=0;
            foreach($TicketDaten['ReferenceToUserVal'] AS $TicketFeld=>$TicketFeldInhalt)
            {
/*
 				// Variante 1
                $Feld = new stdClass();
                $Feld->name = $TicketFeld;
                $Feld->type = $TicketFeldInhalt[0];
                $Feld->value = $TicketFeldInhalt[1];

                $Param->Object->ReferenceToUserVal[] = $Feld;

				// Variante 2
                $Param->Object->ReferenceToUserVal[$AttAnz]->name = $TicketFeld;
                $Param->Object->ReferenceToUserVal[$AttAnz]->type = $TicketFeldInhalt[0];
                $Param->Object->ReferenceToUserVal[$AttAnz]->value = $TicketFeldInhalt[1];
                $AttAnz++;

*/
            }
        }
        if(isset($TicketDaten['ReferenceListVal']))
        {
            foreach($TicketDaten['ReferenceListVal'] AS $TicketFeld=>$TicketFeldInhalt)
            {
                $Feld = new stdClass();
                $Feld->name = $TicketFeld;
                $Feld->objectIds = $TicketFeldInhalt;

                $Param->Object->ReferenceListVal[] = $Feld;
            }
        }
        if(isset($TicketDaten['AddedAttachmentsVal']))
        {
            $AttAnz = 0;
            foreach($TicketDaten['AddedAttachmentsVal'] AS $TicketFeld=>$TicketFeldInhalt)
            {
                $Param->Object->AddedAttachmentsVal = new stdClass();
                $Param->Object->AddedAttachmentsVal->name = $TicketFeld;
                $Param->Object->AddedAttachmentsVal->Attachments = new stdClass();
                $Param->Object->AddedAttachmentsVal->Attachments->AddedAttachment[$AttAnz] = new stdClass();
                $Param->Object->AddedAttachmentsVal->Attachments->AddedAttachment[$AttAnz]->description = 'Anhang';
                $Param->Object->AddedAttachmentsVal->Attachments->AddedAttachment[$AttAnz]->link = false;
                $Param->Object->AddedAttachmentsVal->Attachments->AddedAttachment[$AttAnz]->name = utf8_encode($TicketFeldInhalt['name']);
                $Param->Object->AddedAttachmentsVal->Attachments->AddedAttachment[$AttAnz]->_ = file_get_contents($TicketFeldInhalt['dateiname']);

                $AttAnz++;
            }
        }

        if($this->_DebugLevel>2)
        {
            echo 'Erzeuge neues Ticket...'.PHP_EOL;
            if($this->_DebugLevel>90)
            {
                var_dump($Param);
            }
        }
        $Erg = $this->_SOAP_Client->AddObject($Param);
        if($this->_DebugLevel>90)
        {
            echo 'Antwort:'.PHP_EOL;
            var_dump($Erg);
        }
	//["AddObjectResult"]=> object(stdClass)#25 (2) { ["success"]=> bool(true) ["objectId"]=> int(3198665) }

        switch((string)$Erg->AddObjectResult->success)
        {
            case true:
                $TicketID = (string)$Erg->AddObjectResult->objectId;
                $this->_Form->Formular_Start();
                $this->_Form->ZeileStart();
                $this->_Form->Hinweistext('Das Ticket wurde erzeugt.', awisFormular::HINWEISTEXT_OK);
                $this->_Form->ZeileEnde();
                $this->_Form->Formular_Ende();
                break;
            case false:
                $TicketID = -1;
                $this->_Form->Formular_Start();
                $this->_Form->ZeileStart();
                $this->_Form->Erstelle_TextLabel((string)'PROBLEM: 0402121745: '.utf8_decode($Erg->AddObjectResult->errorMsg),800,'Hinweis');
                $this->_Form->ZeileEnde();
                $this->_Form->ZeileStart();
                $this->_Form->Erstelle_TextLabel('Das Ticket wurde nicht erzeugt.',800,'Hinweis');
                $this->_Form->ZeileEnde();

                $this->_Form->DebugAusgabe(1,htmlentities(serialize($Param)));
                
                $this->_Form->Formular_Ende();
                
                require_once 'awisMailer.inc';
                $MailObj = new awisMailer($this->_DB, $this->_AWISBenutzer);
                $MailObj->Betreff('Problem beim Ticket anlegen von '.$this->_AWISBenutzer->BenutzerName());
                $MailObj->AdressListe(awisMailer::TYP_TO,'shuttle@de.atu.eu',false,awisMailer::PRUEFAKTION_KEINE);
                $MailObj->Text('Fehler beim Speichern des Tickets:'.serialize($Param).PHP_EOL.PHP_EOL.'Fehlermeldung: '.$Erg->AddObjectResult->errorMsg);
                $MailObj->MailSenden();
                
                break;
        }
        
        return $TicketID;
	}

	/**
     * Liest eine Liste mit Tickets aus
     * @param string $Pfad
     */
	public function ObjektListe($Pfad, $OrdnerFilter='', array $Felder, $MaxEintraege=99)
	{
	    $RueckgabeListe = array();
	    $RueckgabeAnz = 0;
//	    $this->_DebugLevel=90;
        $OTKlasse = new stdClass();
        $OTKlasse->folderPath=$Pfad;
        $OTKlasse->recursive=true;
        $OTKlasse->maximumRecords=$MaxEintraege;

        if(substr($OrdnerFilter,0,1)=='#')            // Suche nach einem Ticket mit einer ID
        {
            $Param = new stdClass();
            $Param->Get = $OTKlasse;
            $Param->Get->ObjectIDs = new stdClass();
            $Param->Get->ObjectIDs->objectIDs = substr($OrdnerFilter,1);
        }
        elseif(substr($OrdnerFilter,0,1)=='*')        // Filtersuche: TODO geht noch nicht, raussuchen, warum
        {
            $Param = new stdClass();
            $Param->Get = $OTKlasse;

            $Filter = explode('#',substr($OrdnerFilter,1));

            $FilterParam = new stdClass();
            $FilterParam->name = $Filter[0];
            foreach($Filter AS $FilterEintrag)
            {
                $FilterElement = explode('~', $FilterEintrag);
                switch ($FilterElement[0])
                {
                    case 'StringVal':
                        $FilterParamElement = new stdClass();
                        $FilterParamElement->name = $FilterElement[1];
                        $FilterParamElement->_ = $FilterElement[2].'*';
                        $FilterParam->StringVal[] = $FilterParamElement;
                        break;
                }
            }
            $Param->Get->Filter = $FilterParam;
            /*$Param->Get->Filter->name = $Filter[0];
            $Param->Get->Filter->StringVal[0]->name = $FilterElement[1];
            $Param->Get->Filter->StringVal[0]->_ = $FilterElement[2].'*';
*/
        }
        else        // Suche nach einem Filternamen
        {
            $Filter = new stdClass();
            $Filter->name = $OrdnerFilter;
            $Param = new stdClass();
            $Param->Get = $OTKlasse;
            $Param->Get->Filter = $Filter;
        }

        foreach($Felder as $Feld)
        {
            $Param->Get->RequiredField[]=$Feld;
        }

        if($this->_DebugLevel>50)
        {
            var_dump($Param);
        }

        $Erg = $this->_SOAP_Client->GetObjectList($Param);

        if($this->_DebugLevel>1)
        {
            echo 'Frage Tickets aus '.$Pfad.' ab...'.PHP_EOL;
        }

        if(isset($Erg->GetObjectListResult->success))    // richtiges Objekt
        {
            if($this->_DebugLevel>8)
            {
                echo '  -> Abfrage ok'.PHP_EOL;
            }
            if($this->_DebugLevel>50)
            {
                var_dump($Erg);
            }

            if($Erg->GetObjectListResult->success==true)
            {
                if($this->_DebugLevel>8)
                {
                    echo '    -> Daten ok'.PHP_EOL;
                }

                // Pr�fen, ob eine Liste kommt, wenn nichzt umbauen wg. foreach()
                if(!is_array($Erg->GetObjectListResult->Object))
                {
                    $Tickets=array($Erg->GetObjectListResult->Object);
                }
                else
                {
                    $Tickets=$Erg->GetObjectListResult->Object;
                }

                foreach($Tickets AS $Ticket)
                {
                    if($this->_DebugLevel>8)
                    {
                        echo '-------------------------'.PHP_EOL.'Ticket-ID:'.$Ticket->id.PHP_EOL;
                    }

                    $RueckgabeListe[$RueckgabeAnz]['id'] = $Ticket->id;

                    if(isset($Ticket->StringVal))
                    {
                        if(!is_array($Ticket->StringVal))
                        {
                            $Ticket->StringVal = array($Ticket->StringVal);
                        }
                        foreach($Ticket->StringVal as $Felder)
                        {
                            $RueckgabeListe[$RueckgabeAnz][$Felder->name]=utf8_decode($Felder->_);
                            if($this->_DebugLevel>40)
                            {
                                echo $Felder->name.' : '.utf8_decode($Felder->_).PHP_EOL;
                            }
                        }
                    }
                    if(isset($Ticket->NullVal))
                    {
                        if(!is_array($Ticket->NullVal))
                        {
                            $Ticket->NullVal = array($Ticket->NullVal);
                        }
                        foreach($Ticket->NullVal as $Felder)
                        {
                            $RueckgabeListe[$RueckgabeAnz][$Felder->name]='';
                            if($this->_DebugLevel>40)
                            {
                                echo $Felder->name.' : NULL'.PHP_EOL;
                            }
                        }
                    }
                    if(isset($Ticket->LongIntVal))
                    {
                        if(!is_array($Ticket->LongIntVal))
                        {
                            $Ticket->LongIntVal = array($Ticket->LongIntVal);
                        }
                        foreach($Ticket->LongIntVal as $Felder)
                        {
                            $RueckgabeListe[$RueckgabeAnz][$Felder->name]=$Felder->_;
                            if($this->_DebugLevel>40)
                            {
                                echo $Felder->name.' : '.$Felder->_.PHP_EOL;
                            }
                        }
                    }
                    if(isset($Ticket->DateTimeVal))
                    {
                        if(!is_array($Ticket->DateTimeVal))
                        {
                            $Ticket->DateTimeVal = array($Ticket->DateTimeVal);
                        }
                        foreach($Ticket->DateTimeVal as $Felder)
                        {
                            $RueckgabeListe[$RueckgabeAnz][$Felder->name]=$this->_Form->PruefeDatum(substr($Felder->_,0,10),true);
                            if($this->_DebugLevel>40)
                            {
                                echo $Felder->name.' : '.$Felder->_.PHP_EOL;
                            }
                        }
                    }

                    if(isset($Ticket->ReferenceVal))
                    {
                        if(!is_array($Ticket->ReferenceVal))
                        {
                            $Ticket->ReferenceVal = array($Ticket->ReferenceVal);
                        }
                        foreach($Ticket->ReferenceVal as $Felder)
                        {
                            $RueckgabeListe[$RueckgabeAnz][$Felder->name]=$Felder->objectId;
                            if($this->_DebugLevel>40)
                            {
                                echo $Felder->name.' : '.$Felder->_.PHP_EOL;
                            }
                        }
                    }


                    // TODO ab hier testen!
                    if(isset($Ticket->ReferenceToUserVal))
                    {
                        if(!is_array($Ticket->ReferenceToUserVal))
                        {
                            $Ticket->ReferenceToUserVal = array($Ticket->ReferenceToUserVal);
                        }
                        foreach($Ticket->ReferenceToUserVal as $Felder)
                        {
                            $RueckgabeListe[$RueckgabeAnz][$Felder->name]=$Felder->Value;
                            if($this->_DebugLevel>40)
                            {
                                echo $Felder->name.' : '.$Felder->type.' : '.$Felder->Value.PHP_EOL;
                            }
                        }
                    }

                    $RueckgabeAnz++;
                }
            }
            else    // Fehler beim Zugriff
            {

            }

            return $RueckgabeListe;
        }
        else
        {
            throw new Exception('Kein Zugriff auf den Server', 201110121948);
        }
	}
	
	/**
	 * Importiert Daten aus dem OmniTracker
	 */
	public function ImportiereStammdaten()
	{
		$Pfad = '/daten/daten/pcedv11/Daten/Omnitracker';
		
		// Kategorien
		$fp = fopen($Pfad.'/OTCategory.csv','r');
		if($this->_DebugLevel>10)
		{
			echo 'Oeffne '.$Pfad.'/OTCategory.csv'.PHP_EOL;
		}
		if($fp!==false)
		{
			//**
			// Aufbau :
			// Unique_Id;Object_Name;Beschreibung;Titel;AWIS_Formular_String
			// 542317;"KAT-0056: Kategorien\Sonstige";;"Sonstige";"001/004/003"
			//**
			
			$Ueberschrift = fgetcsv($fp,0,";",'"');
			if(substr($Ueberschrift[0],-9,9)=='Unique_Id')
			{
				$Ueberschrift[0]='Unique_Id';
			}
			$Ueberschrift = array_flip($Ueberschrift);
			
			if($this->_DebugLevel>10)
			{
				var_dump($Ueberschrift);
			}
				
			if(isset($Ueberschrift['Unique_Id']))
			{
				$SQL = 'SELECT OFO_KEY, OFO_KENNUNG';
				$SQL .= ' FROM OTFormulare';
				$SQL .= ' WHERE OFO_KENNUNG IS NOT NULL';
				$rsOFO = $this->_DB->RecordSetOeffnen($SQL);
				$FormularIDs=array();
				while(!$rsOFO->EOF())
				{
					$FormularIDs[$rsOFO->FeldInhalt('OFO_KENNUNG')]=$rsOFO->FeldInhalt('OFO_KEY');
					$rsOFO->DSWeiter();	
				}
				
				$this->_DB->TransaktionBegin();
				$this->_DB->Ausfuehren('DELETE FROM OTKATEGORIEN');

				$Daten = fgetcsv($fp,0,";",'"');
				while(!feof($fp))
				{
					$Formulare = explode("/",$Daten[$Ueberschrift['AWIS_Formular_String']]);
	
					foreach($Formulare AS $Formular)
					{
						if(isset($FormularIDs[$Formular]))
						{
							if($this->_DebugLevel>10)
							{
								echo '  Fuege Daten ein..'.PHP_EOL;
							}
							$SQL = 'INSERT INTO OTKATEGORIEN(';
							$SQL .= 'OTK_BEZEICHNUNG,OTK_BEMERKUNG,OTK_OFO_KEY,OTK_ID';
							$SQL .= ',OTK_USER,OTK_USERDAT';
							$SQL .= ')VALUES (';
							$SQL .= ' '.$this->_DB->FeldInhaltFormat('T',utf8_decode($Daten[$Ueberschrift['Titel']]));
							$SQL .= ' ,'.$this->_DB->FeldInhaltFormat('T',utf8_decode($Daten[$Ueberschrift['Beschreibung']]));
							$SQL .= ' ,'.$this->_DB->FeldInhaltFormat('N0',$FormularIDs[$Formular]);
							$SQL .= ' ,'.$this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschrift['Unique_Id']]);
							$SQL .= ' ,'.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
							$SQL .= ' ,SYSDATE)';
							
							if($this->_DebugLevel>10)
							{
								echo '  -->'.$SQL.PHP_EOL;
							}
							$this->_DB->Ausfuehren($SQL);
						}
					}
					$Daten = fgetcsv($fp,0,";",'"');
				}
				$this->_DB->TransaktionCommit();
			}
		}

		// Personen
		$fp = fopen($Pfad.'/OTPersons.csv','r');
		if($this->_DebugLevel>10)
		{
			echo 'Oeffne '.$Pfad.'/OTPersons.csv'.PHP_EOL;
		}
		if($fp!==false)
		{
			//**
			// Aufbau :
			// Object_Name;Unique_Id;Vorname;Nachname;Persnr;Telefon;Email;Abteilung;Standort_ATU_StandortID
			// "Buchholz Sebastian, +49(671)7964788";546438;"Sebastian";"Buchholz";"979319";"+49(671)7964788";"0103@de.atu.eu";;"103-F"
			//**
			
			$Ueberschrift = fgetcsv($fp,0,";",'"');
			if(substr($Ueberschrift[0],-11,11)=='Object_Name')
			{
				$Ueberschrift[0]='Object_Name';
			}
			$Ueberschrift = array_flip($Ueberschrift);
			
			if($this->_DebugLevel>10)
			{
				var_dump($Ueberschrift);
			}
				
			if(isset($Ueberschrift['Object_Name']))
			{
				$this->_DB->TransaktionBegin();
				$this->_DB->Ausfuehren('DELETE FROM OTPERSONEN');

				$Daten = fgetcsv($fp,0,";",'"');
				
				while(!feof($fp))
				{
					if($this->_DebugLevel>10)
					{
						echo '  Fuege Daten ein..'.PHP_EOL;
					}
					$SQL = 'INSERT INTO OTPERSONEN';
					$SQL .= '(OTP_ID,OTP_BEZEICHNUNG,OTP_PERSNR';
					$SQL .= ' ,OTP_USER,OPT_USERDAT) VALUES (';
					$SQL .= ' '.$this->_DB->FeldInhaltFormat('T',utf8_decode($Daten[$Ueberschrift['Unique_Id']]));
					$SQL .= ' ,'.$this->_DB->FeldInhaltFormat('T',utf8_decode($Daten[$Ueberschrift['Object_Name']]));
					$SQL .= ' ,'.$this->_DB->FeldInhaltFormat('N0',$Daten[$Ueberschrift['Persnr']],false);
					$SQL .= ' ,'.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
					$SQL .= ' ,SYSDATE)';
					
					if($this->_DebugLevel>10)
					{
						echo '  -->'.$SQL.PHP_EOL;
					}
					$this->_DB->Ausfuehren($SQL);
					$Daten = fgetcsv($fp,0,";",'"');
				}
				$this->_DB->TransaktionCommit();
			}
		}
	}
}
?>