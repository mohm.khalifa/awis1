<?php

// Variablen
$Bedingung = '';
$SQL = '';
global $awisRSZeilen;
global $AWISBenutzer;
// DB Init
$con = awisLogon();

$Recht251 = awisBenutzerRecht($con, 251);
if($Recht251==0)
{
    awisEreignis(3,1000,'Reifen',$AWISBenutzer->BenutzerName(),'','','');
	die("Keine ausreichenden Rechte!");
}

If(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='')
{
	$Params = "";
	$Params .= $_POST['txtBreite'];						// 0
	$Params .= ";" . $_POST['txtQSchnitt'];				// 1
	$Params .= ";" . $_POST['txtDMesser'];					// 2
	$Params .= ";" . $_POST['txtLLKW'];					// 3
	$Params .= ";" . $_POST['txtLIndex'];					// 4
	$Params .= ";" . $_POST['txtLIndex2'];					// 5
	$Params .= ";" . $_POST['txtSpeedIndex'];				// 6
	$Params .= ";" . $_POST['txtMaxVK'];					// 7
	$Params .= ";" . $_POST['txtATUNr'];					// 8
	$Params .= ";" . $_POST['txtBez'];						// 9
	$Params .= ";" . (isset($_POST['txtSort'])?$_POST['txtSort']:'');					// 10
	$Params .= ";" . $_POST['txtTyp'];						// 11
	$Params .= ";" . (isset($_POST['txtCCSort'])?$_POST['txtCCSort']:''); // 12
	$Params .= ";" . (isset($_POST['txtReifSpd'])?$_POST['txtReifSpd']:'');	// 13
	$Params .= ";" . (isset($_POST['txtAuswahlSpeichern'])?$_POST['txtAuswahlSpeichern']:''); // Alle gespeicherten Parameter wieder anzeigen
	$Params .= ";" . $_POST['txtHersteller'];				// 15
	$Params .= ";" . (isset($_POST['txtKBListe'])?$_POST['txtKBListe']:'');					// 16
	$Params .= ";" . (isset($_POST['txtHerstellerZuerst'])?$_POST['txtHerstellerZuerst']:'');		// 17
	$Params .= ";" . (isset($_POST['txtLAN_CODE'])?$_POST['txtLAN_CODE']:''); // 18
	$Params .= ";" . (isset($_POST['txtFahrzeuge'])?$_POST['txtFahrzeuge']:'');				// 19

	awis_BenutzerParameterSpeichern($con, "ReifenSuche", $AWISBenutzer->BenutzerName(), $Params);
	$Params = explode(";",$Params);
}
else		// die gespeicherten Werte
{
	$Params = explode(";",awis_BenutzerParameter($con,"ReifenSuche",$AWISBenutzer->BenutzerName()));
}


$SQL = 'SELECT ';
$SQL .= " KFZ_KBANR.KBANRC AS KBANR, KFZ_TYP.BEZ AS TYP, KFZ_TYP.BJVON, KFZ_TYP.BJBIS, KFZ_TYP.PS,";
$SQL .= " KFZ_MODELL.BEZ AS MODELL, KFZ_HERST.BEZ AS HERSTELLER,";
$SQL .= " ReifenFahrzeuge.*, KFZ_HERST.KHERNR";
$SQL .= " from KFZ_KBANR INNER JOIN KFZ_TYP ON KFZ_KBANR.KTYPNR=KFZ_TYP.KTYPNR";
$SQL .= "   INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR=KFZ_MODELL.KMODNR";
$SQL .= "   INNER JOIN KFZ_HERST ON KFZ_MODELL.KHERNR=KFZ_HERST.KHERNR"; 
$SQL .= "   INNER JOIN ReifenFahrzeuge ON KFZ_KBANR.KBANRC=(ReifenFahrzeuge.REF_KBH_ID || ReifenFahrzeuge.REF_KBT_ID) ";

$Bedingung = '';
$BedingungText = '';

if($Params[0]!='')		// Breite
{
	$Bedingung .= " AND REF_Breite = " . $Params[0];
	$BedingungText .= 'Breite: ' . $Params[0];
}

if($Params[1]!='')
{
	$Bedingung .= " AND REF_Querschnitt = '" . $Params[1] . "'";
	$BedingungText .= ', Querschnitt: ' . $Params[1];
}

if($Params[2]!='')			// DMesser
{
	$Bedingung .= " AND REF_InnenDurchmesser = " . $Params[2];
	$BedingungText .= ', Innendurchmesser:'.$Params[2];
}

if($Params[4]!='')
{
	$Bedingung .= " AND REF_LoadIndex = " . $Params[4];
	$BedingungText .= ', Load Index:'.$Params[4];
}
if($Params[5]!='')
{
	$Bedingung .= " AND REF_LoadIndex2 = " . $Params[5];
	$BedingungText .= ', Load Index 2:'.$Params[5];
}
if($Params[6]!='#-#')
{
	$Bedingung .= " AND REF_SPI_SYMBOL = '" . $Params[6] . "'";
	$BedingungText .= ', Speed Index:'.$Params[6];
}


if(isset($_POST['txtFahrzeugZiffern']) AND $_POST['txtFahrzeugZiffern']<3)
{
	if(isset($_POST['txtFahrzeugZiffern']) AND $_POST['txtFahrzeugZiffern']==0)
	{
		$Bedingung .= " AND REF_ZIFFER IN (20,21)";
	}
	elseif(isset($_POST['txtFahrzeugZiffern']) AND $_POST['txtFahrzeugZiffern']==1)
	{
		$Bedingung .= " AND REF_ZIFFER IN (22,23)";
	}
	elseif(isset($_POST['txtFahrzeugZiffern']) AND $_POST['txtFahrzeugZiffern']==2)
	{
		$Bedingung .= " AND REF_ZIFFER = 33";
	}
}

$SQL .= ($Bedingung!=''?' WHERE ' . substr($Bedingung,4):' ORDER BY REF_KBH_ID');


$rs = awisOpenRecordset($con, $SQL);
$rsZeilen = $awisRSZeilen;
awis_Debug(1,$awisRSZeilen,$SQL);

/********************************************************************
*
* Zusammenfassung der Daten
*
********************************************************************/

$FahrzeugListe = array();
$Zulassungen=array();
$GesAnzahl=0;
for($i=0;$i<$rsZeilen;$i++)
{
	$Fahrzeug = $rs['HERSTELLER'][$i].'~'.$rs['MODELL'][$i].'~'.$rs['TYP'][$i].'~'.substr($rs['BJVON'][$i],0,2).'/'.substr($rs['BJVON'][$i],2).'~'.($rs['BJBIS'][$i]=='000000'?'':substr($rs['BJBIS'][$i],0,2).'/'.substr($rs['BJBIS'][$i],2)).'~'.$rs['REF_KBH_ID'][$i].'~'.$rs['REF_KBT_ID'][$i].'';
	if(array_search($Fahrzeug ,$FahrzeugListe)===false)
	{
		$rsZulassungen = awisOpenRecordset($con, "SELECT ANZ FROM KFZ_SNAP_ARTIKELVKN4 WHERE HER='".$rs['REF_KBH_ID'][$i]."' AND TYP='".$rs['REF_KBT_ID'][$i]."'");
		$Zulassungen[$rs['REF_KBH_ID'][$i]][$rs['REF_KBT_ID'][$i]] = (isset($rsZulassungen['ANZ'][0])?$rsZulassungen['ANZ'][0]:'0');
		$GesAnzahl += (isset($rsZulassungen['ANZ'][0])?$rsZulassungen['ANZ'][0]:'0');
		unset($rsZulassungen);
		
		array_push($FahrzeugListe,$Fahrzeug);
	}
}

echo '<h2>Gefundene Fahrzeuge</h2>';
echo 'Suckriterien:'.$BedingungText;

sort($FahrzeugListe);

echo '<table border=1>';
$i=0;

// Daten auch als Datei zur Verf�gung stellen
$DateiName = awis_UserExportDateiName('.csv');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];

$fp = fopen($DateiName,'w' );
if($fp===false)
{
	awis_Debug($DateiName);
}
fputs($fp,"Hersteller;Modell;Typ;BJ von;BJ bis;Ziff 2;Ziff 3;Zulassungen;\n");

echo '<tr>';
echo '<td id=FeldBez>Hersteller</td>';
echo '<td id=FeldBez>Modell</td>';
echo '<td id=FeldBez>Typ</td>';
echo '<td id=FeldBez>BJ von</td>';
echo '<td id=FeldBez>BJ bis</td>';
echo '<td id=FeldBez>Ziff 2</td>';
echo '<td id=FeldBez>Ziff 3</td>';
echo '<td id=FeldBez>Zulassungen</td>';
echo '</tr>';

while($FahrzeugListe[$i]!='')
{
	$Spalten = explode('~',$FahrzeugListe[$i++]);
	
	echo '<tr>';
	for($j=0;$j<count($Spalten);$j++)
	{
		echo '<td>' . $Spalten[$j] . '</td>';
		fputs($fp, $Spalten[$j].';');
	}
	echo '<td align=right>' . awis_format($Zulassungen[$Spalten[5]][$Spalten[6]],'Standardzahl#0') . '</td>';
	fputs($fp, awis_format($Zulassungen[$Spalten[5]][$Spalten[6]],'Standardzahl#0').';');
	echo '</tr>';

	fputs($fp,"\n");

}
echo '<tr><td align=right colspan=' . ($j) . '>Summe</td><td align=right>' . awis_format($GesAnzahl,'Standardzahl#0') . '</td></tr>';
echo '</table>';

echo '<br><font size=2>Es wurden ' . $i . ' Fahrzeuge gefunden.</font>';

fclose($fp);
echo "<br><a href=$DateiNameLink>CSV-Datei �ffnen</a>";

awisLogoff($con);
?>