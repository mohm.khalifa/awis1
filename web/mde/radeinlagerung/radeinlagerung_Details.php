<?php
/**
 * Bearbeitung der R�ckf�hrungen durch die Filiale bzw. Zentrale
 * 
 * @author Thomas Riedl
 * @copyright ATU Auto Teile Unger
 * @version 200809291144
 */

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FIL_ID;
global $LFDNR;

try 
{	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('REL','%');	
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AlleAnzeigen');	
	$TextKonserven[]=array('Wort','Abschliessen');
	$TextKonserven[]=array('Wort','Abbrechen');
	$TextKonserven[]=array('Wort','Abgeschlossen');
	$TextKonserven[]=array('Wort','NichtAbgeschlossen');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','Zwangsabschluss');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
		
	$Recht3602 = $AWISBenutzer->HatDasRecht(3602);
	if($Recht3602==0)
	{
		$Form->Fehler_KeineRechte();
	}
			
	//********************************************************
	// Parameter ?
	//********************************************************
	$Param = array_fill(0,15,'');		
	//var_dump($_POST);
	if(isset($_POST['cmdSuche_x']))
	{
		$Param['KEY']=0;			// Key
		
		$Param['REL_FIL_ID']=$_POST['sucREL_FIL_ID'];	
		$Param['REL_LFDNR']=$_POST['sucREL_LFDNR'];	
		$Param['REL_NR']=$_POST['sucREL_NR'];			
		$Param['REL_STATUS']=$_POST['sucREL_STATUS'];			
		
		$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
	
		$Param['ORDERBY']='';
		$Param['BLOCK']='';
		$Params['BLOCKPOS']='';		
		$Param['KEY']=0;
	
		$AWISBenutzer->ParameterSchreiben("RELSuche",serialize($Param));
		$AWISBenutzer->ParameterSchreiben("RELPos",serialize($Params));
		$AWISBenutzer->ParameterSchreiben("AktuellerREL",0);					
	}					
	elseif(isset($_POST['cmd_Abschliessen_x']))
	{		
		include('./radeinlagerung_speichern.php');
		include('./radeinlagerung_abschliessen.php');
		$Params['BLOCKPOS']='';	
		$AWISBenutzer->ParameterSchreiben("RELPos",serialize($Params));
	}
	elseif(isset($_POST['cmdAbschliessenOK']))
	{	
		include('./radeinlagerung_abschliessen.php');
		$Params['BLOCKPOS']='';	
		$AWISBenutzer->ParameterSchreiben("RELPos",serialize($Params));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{	
		include('./radeinlagerung_speichern.php');
		//$Params['BLOCKPOS']='';	
		//$AWISBenutzer->ParameterSchreiben("RELPos",serialize($Params));
	}	
	elseif(isset($_GET['REL_FIL_ID']) AND isset($_GET['REL_LFDNR']))
	{
		$FIL_ID = $DB->FeldInhaltFormat('N0',$_GET['REL_FIL_ID']);
		$LFDNR = $DB->FeldInhaltFormat('N0',$_GET['REL_LFDNR']);
	}	
	elseif(isset($_POST['txtREL_FIL_ID']) AND isset($_POST['REL_LFDNR']))
	{
		$FIL_ID = $DB->FeldInhaltFormat('N0',$_POST['txtREL_FIL_ID']);
		$LFDNR = $DB->FeldInhaltFormat('N0',$_POST['REL_LFDNR']);
	}		
	else 		// Letzte Lieferkontrolle suchen
	{
		$Params['BLOCKPOS']='';	
		$AWISBenutzer->ParameterSchreiben("RELPos",serialize($Params['BLOCKPOS']));
				
		if(!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block']) and !isset($_GET['BlockPos']))
		{									
			//$AWIS_KEY1 = unserialize($AWISBenutzer->ParameterLesen('AktuellerREL' , false));				
			$ParamAkt = unserialize($AWISBenutzer->ParameterLesen('AktuellerREL'));
			$FIL_ID=$ParamAkt['FIL_ID'];
			$LFDNR=$ParamAkt['LFDNR'];
						
			if (intval($FIL_ID) <= 0 or intval($LFDNR) <= 0)
			{
				$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
				if ($UserFilialen != '')
				{
					$Param['REL_FIL_ID']=$UserFilialen;	
					$AWISBenutzer->ParameterSchreiben("AktuellerREL",0);
					$AWISBenutzer->ParameterSchreiben("RELSuche",serialize($Param));
				}
			}
		}		
		
		if($Param=='' OR $Param=='0')
		{			
			$AWISBenutzer->ParameterSchreiben("AktuellerREL",0);			
		}								
	}
		
	$Param = unserialize($AWISBenutzer->ParameterLesen("RELSuche"));	
	
	$Form->DebugAusgabe(1,$Param);
	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************
	if(!isset($_GET['Sort']))
	{
		if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
		{
			$ORDERBY = $Param['ORDERBY'];
		}
		else
		{
			$ORDERBY = ' REL_LFDNR DESC';
		}		
	}
	else
	{
		$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	
	//********************************************************
	// Daten suchen
	//********************************************************	
	$SQL= 'SELECT DISTINCT REL_LFDNR, REL_FIL_ID, REL_STATUS';
	if($FIL_ID<=0 AND $LFDNR <=0)
	{
		$SQL.= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
	}				
	$SQL.= ' FROM';
	$SQL.= '(';
	$SQL.= 'SELECT DISTINCT REL_LFDNR, REL_FIL_ID, REL_STATUS';	
	$SQL.= ' FROM';
	$SQL.=' (';		
	$SQL.= 'SELECT DISTINCT REL_LFDNR, REL_FIL_ID';
	if (isset($Param['REL_NR']) AND $Param['REL_NR'] != '')
	{
		$SQL.= ', REL_NR';
	}
	$SQL .= ' ,(SELECT MIN(REL_STATUS) FROM RAEDEREINLAGERUNGEN WHERE RAEDEREINLAGERUNGEN.REL_LFDNR=REL.REL_LFDNR AND RAEDEREINLAGERUNGEN.REL_FIL_ID = REL.REL_FIL_ID) AS REL_STATUS';		
	$SQL.=' FROM Raedereinlagerungen REL';		
	if($Bedingung!='')
	{
		$SQL.= ' WHERE ' . substr($Bedingung,4);
	}
	$SQL.=' )';				
	$SQL.=' )';				

	$SQL.= ' ORDER BY '.$ORDERBY;
	$Form->DebugAusgabe(1,$SQL,$Param);				

	//************************************************
	// Aktuellen Datenblock festlegen
	//************************************************
	$Block = 1;
	//if (!isset($_GET['Liste']))
	//{
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		}
		elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='' AND !isset($_GET['Liste']))
		{
			$Block = intval($Param['BLOCK']);
		}
	//}
	
	//************************************************
	// Zeilen begrenzen
	//************************************************
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		
	//*****************************************************************
	// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
	//*****************************************************************	
	if($FIL_ID<=0 AND $LFDNR<=0)
	{
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	
	$rsREL = $DB->RecordSetOeffnen($SQL);
	
	//*****************************************************************
	// Aktuelle Parameter sichern
	//*****************************************************************		
	$Param['ORDERBY']=$ORDERBY;
	//$Param['KEY']=$AWIS_KEY1;
	$Param['FIL_ID']=$FIL_ID;
	$Param['LFDNR']=$LFDNR;
	$Param['BLOCK']=$Block;
	$AWISBenutzer->ParameterSchreiben("RELSuche",serialize($Param));
	
	$Form->SchreibeHTMLCode('<form name=frmRadeinlagerung action=./radeinlagerung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
	$Form->DebugAusgabe(1,$SQL);	

	//Pr�fen ob User eine Filiale ist
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
		
	//Wenn eine Fililale angemeldet ist, dann gleich speichern
	if(count($FilZugriffListe)==1 and $FilZugriff != '')
	{
		$FIL_ID=$FilZugriff;	
	}
	
	//********************************************************
	// Daten anzeigen
	//********************************************************	
	if($rsREL->EOF())		// Keine Meldung bei neuen Datens�tzen!
	{
		echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
	}
	elseif(($rsREL->AnzahlDatensaetze()>1) or (isset($_GET['Liste'])))						// Liste anzeigen
	{	
		$Form->Formular_Start();
	
		$Form->ZeileStart();
		$Link = './radeinlagerung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=REL_LFDNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='REL_LFDNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_LFDNR'],200,'',$Link);
		
		if(count($FilZugriffListe)>1 or $FilZugriff == '')
		{			
			$Link = './radeinlagerung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=REL_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='REL_FIL_ID'))?'~':'');			
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_FIL_ID'],100,'',$Link);			
		}			
				
		$Link = './radeinlagerung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=REL_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='REL_STATUS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REL']['REL_STATUS'],200,'',$Link);						
		$Form->ZeileEnde();
		
		$RELZeile=0;
		
		while(!$rsREL->EOF())
		{
			$Form->ZeileStart();
			$Link = './radeinlagerung_Main.php?cmdAktion=Details&REL_FIL_ID='.$rsREL->FeldInhalt('REL_FIL_ID').'&REL_LFDNR='.$rsREL->FeldInhalt('REL_LFDNR');
			$Form->Erstelle_ListenFeld('REL_LFDNR',$rsREL->FeldInhalt('REL_LFDNR'),0,200,false,($RELZeile%2),'',$Link,'T');
			
			if(count($FilZugriffListe)>1 or $FilZugriff == '')
			{			
				$Form->Erstelle_ListenFeld('REL_FIL_ID',$rsREL->FeldInhalt('REL_FIL_ID'),0,100,false,($RELZeile%2),'','','T');				
			}
			
			$Status= (($rsREL->FeldInhalt('REL_STATUS')>=10)?$AWISSprachKonserven['Wort']['Abgeschlossen']:$AWISSprachKonserven['Wort']['NichtAbgeschlossen']);						
			$Form->Erstelle_ListenFeld('REL_STATUS',$Status,0,200,false,($RELZeile%2),'','','T');		
			/*						
			$LKKStatus='';
			$StatusText = explode("|",$AWISSprachKonserven['LIK']['lst_Lieferkontrollenstatus']);			
			foreach ($StatusText as $Status)
			{
				$LKStatus = explode("~",$Status);								
				if ($LKStatus[0]==$rsLKK->FeldInhalt('LKK_STATUS'))
				{
					$LKKStatus = $LKStatus[1];					
				}				
			}			
			$Form->Erstelle_ListenFeld('LKK_STATUS',$LKKStatus,0,200,false,($LKKZeile%2),'','','T','L','');		
			*/			
			
			$Form->ZeileEnde();
	
			$rsREL->DSWeiter();
			$RELZeile++;
		}
	
		$Link = './radeinlagerung_Main.php?cmdAktion=Details&Liste=True&'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');		
	
		$Form->Formular_Ende();
	
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','../../filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');				
		$Form->SchaltflaechenEnde();	
	}			
	else										// Eine einzelne Lieferkontrolle
	{
		$Form->SchreibeHTMLCode('<form name=frmRadeinlagerung action=./radeinlagerung_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');	
		
		//$AWIS_KEY1 = $rsREL->FeldInhalt('REL_FIL_ID').'_'.$rsREL->FeldInhalt('REL_LFDNR');					
		$FIL_ID = $rsREL->FeldInhalt('REL_FIL_ID');
		$LFDNR = $rsREL->FeldInhalt('REL_LFDNR');
		
		$ParamAkt=array();
		$ParamAkt['FIL_ID']=$FIL_ID;
		$ParamAkt['LFDNR']=$LFDNR;
		
		$AWISBenutzer->ParameterSchreiben("AktuellerREL",serialize($ParamAkt));	
				
		//$Form->Erstelle_HiddenFeld('LKK_KEY',$AWIS_KEY1);			
		$Form->Erstelle_HiddenFeld('REL_FIL_ID',$rsREL->FeldInhalt('REL_FIL_ID'));
		$Form->Erstelle_HiddenFeld('REL_LFDNR',$rsREL->FeldInhalt('REL_LFDNR'));
				
		$Form->Formular_Start();	
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];	
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./radeinlagerung_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");		
		//$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKK->FeldInhalt('LKK_USER'));
		//$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKK->FeldInhalt('LKK_USERDAT'));		
		$Form->InfoZeile($Felder,'');
				
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['REL']['REL_LFDNR'].':',180);
		$Form->Erstelle_TextFeld('REL_LFDNR',$rsREL->FeldInhalt('REL_LFDNR'),50,300,false);		
		$Form->ZeileEnde();	
		
		if(count($FilZugriffListe)>1 or $FilZugriff == '')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['REL']['REL_FIL_ID'].':',180);
			$Form->Erstelle_TextFeld('REL_FIL_ID',$rsREL->FeldInhalt('REL_FIL_ID'),50,300,false);			
			$Form->ZeileEnde();	
		}
							
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['REL']['REL_STATUS'].':',180);
		$Status= (($rsREL->FeldInhalt('REL_STATUS')>=10)?$AWISSprachKonserven['Wort']['Abgeschlossen']:$AWISSprachKonserven['Wort']['NichtAbgeschlossen']);						
		$Form->Erstelle_TextFeld('REL_STATUS',$Status,50,200,false,'','','','T','L','');						
		$Form->ZeileEnde();			
		
		$Form->Trennzeile();
				
		$Reg = new awisRegister(3624);
		$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));				
			
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();	
		
		$Form->Schaltflaeche('href','cmd_zurueck','../../filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');		
		
		if((($Recht3602 & 2)==2))// and $rsLKK->FeldInhalt('LKK_STATUS')=='O') or ($Recht3600 & 4)==4)		
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
			//If ($rsLKK->FeldInhalt('LKK_STATUS')=='O')
			//{
				$Form->Schaltflaeche('image', 'cmd_Abschliessen','','/bilder/cmd_korb_runter.png', $AWISSprachKonserven['REL']['LieferscheinAbschliessen'],'A');
			//}
		}		
		
		$Form->Schaltflaeche('href', 'cmd_DruckLieferschein','./radeinlagerung_pdf.php?REL_FIL_ID='.$FIL_ID.'&REL_LFDNR='.$LFDNR,'/bilder/cmd_pdf.png', $AWISSprachKonserven['REL']['DruckLieferschein'],'.');								
		
		$Form->SchaltflaechenEnde();
			
		$Form->SchreibeHTMLCode('</form>');
	}
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}	
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031126");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031127");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
	
/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $FIL_ID;
	global $LFDNR;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';	
	
	if(floatval($LFDNR) != 0 AND floatval($FIL_ID) != 0)
	{
		//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
		$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
				
		// Beschr�nkung auf eine Filiale?
		if ($UserFilialen != '')
		{
			$Bedingung.= 'AND REL_FIL_ID IN ('.$UserFilialen.') ';
		}
		
		$Bedingung.= 'AND REL_FIL_ID = '.$FIL_ID;
		$Bedingung.= 'AND REL_LFDNR = '.$LFDNR;
		return $Bedingung;
	}	
				
	if(isset($Param['REL_FIL_ID']) AND $Param['REL_FIL_ID']!='')
	{	
		//$Bedingung .= 'AND RFK_FIL_ID in ( ' . $DB->FeldInhaltFormat('N0',$Param['RFK_FIL_ID']) . ') ';			
		//$Bedingung .= 'AND LIK_FIL_ID in ( ' . $Param['LIK_FIL_ID'] . ') ';			
		$Bedingung .= 'AND REL_FIL_ID = ' . $DB->FeldInhaltFormat('N0',$Param['REL_FIL_ID']) . ' ';			
	}	
	
	if(isset($Param['REL_LFDNR']) AND $Param['REL_LFDNR']!='')
	{
		$Bedingung .= 'AND REL_LFDNR = ' . $DB->FeldInhaltFormat('N0',$Param['REL_LFDNR']) . ' ';			
	}		
		
	if(isset($Param['REL_STATUS']) AND $Param['REL_STATUS']=='O')
	{
		$Bedingung .= 'AND REL_STATUS < 10';					
	}		
	
	if(isset($Param['REL_STATUS']) AND $Param['REL_STATUS']=='A')
	{
		$Bedingung .= 'AND REL_STATUS >= 10';					
	}			
	
	if(isset($Param['REL_NR']) AND $Param['REL_NR']!='')
	{				
		$Bedingung .= 'AND REL_NR ' . $DB->LikeOderIst($Param['REL_NR'],0) . ' ';			
		//$Bedingung .= 'AND LKK_KEY in (select lik_lkk_key from lieferkontrollen where LIK_AST_ATUNR ' . $DB->LikeOderIst($Param['LIK_AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ') ';			
	}					
	
	return $Bedingung;
}

?>