<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $ABE;

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
if (isset($_GET['ABF_KEY'])) {
    $AWIS_KEY1 = $_GET['ABF_KEY'];
} elseif (isset($_POST['txtABF_KEY'])) {
    $AWIS_KEY1 = $_POST['txtABF_KEY'];
} elseif (isset($_POST['cmdDSNeu_x'])) {
    $AWIS_KEY1 = -1;
}
$ABE->Param['KEY'] = $AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $ABE->Param['ABF_FAHRGESTELLNR'] = $ABE->Form->Format('T', $_POST['sucFAHRGESTELLNR'], true);
    $ABE->Param['ABF_MOTORNR'] = $ABE->Form->Format('T', $_POST['sucMOTORNR'], true);
    $ABE->Param['ABF_ATU_NR'] = $ABE->Form->Format('T', $_POST['sucATU_NR'], true);
    $ABE->Param['ABF_DRUCK'] = isset($_POST['sucDRUCK'])?'on':'';

    $ABE->Param['KEY'] = '';
    $ABE->Param['WHERE'] = '';
    $ABE->Param['ORDER'] = '';
    $ABE->Param['BLOCK'] = 1;
    $ABE->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
} elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) { //Irgendwas mit l�schen?
    include('./reifen_loeschen.php');
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include 'abe_speichern.php';
} else { //User hat den Reiter gewechselt.
    $AWIS_KEY1 = $ABE->Param['KEY'];
}

//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************
if (!isset($_GET['Sort'])) {
    if (isset($ABE->Param['ORDER']) AND $ABE->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $ABE->Param['ORDER'];
    } else {
        $ABE->Param['ORDER'] = 'ABF_FAHRGESTELLNR DESC nulls last';
        $ORDERBY =  ' ORDER BY ' . $ABE->Param['ORDER'];
    }
} else {
    $ABE->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $ABE->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $ABE->BedingungErstellen();

$SQL = 'SELECT ';
$SQL .= ' ABF_KEY';
$SQL .= ' ,ABF_FAHRGESTELLNR';
$SQL .= ' ,ABF_MOTORNR';
$SQL .= ' ,ABF_ATU_NR';
$SQL .= ' ,ABF_FARBE';
$SQL .= ' ,AST_BEZEICHNUNG';
$SQL .= ' ,AST_BEZEICHNUNGWW';
$SQL .= ' ,ausstehend.ABL_WARTEND';
$SQL .= ' ,count(gesamt.ABL_KEY) as DRUCKANZ';
$SQL .= ' ,row_number() OVER (';
$SQL .= '  order by ' . $ABE->Param['ORDER'] . ') as ZEILENNR';
$SQL .= ' FROM ABEFAHRZEUGE';
$SQL .= ' LEFT JOIN ABELIEFERSCHEINE gesamt';
$SQL .= ' ON ABF_KEY = gesamt.ABL_ABF_KEY';
$SQL .= ' LEFT JOIN ABELIEFERSCHEINE ausstehend';
$SQL .= ' ON ABF_KEY = ausstehend.ABL_ABF_KEY and ausstehend.ABL_WARTEND = 1';
$SQL .= ' LEFT JOIN ARTIKELSTAMM';
$SQL .= ' ON ABF_ATU_NR = AST_ATUNR';

if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

$SQL .= ' GROUP BY ABF_KEY, ABF_FAHRGESTELLNR, ABF_MOTORNR, ABF_ATU_NR, ABF_FARBE, AST_BEZEICHNUNG, AST_BEZEICHNUNGWW, ausstehend.ABL_WARTEND';

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($AWIS_KEY1 == '') { //Liste?
    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $ABE->Form->Format('N0', $_REQUEST['Block'], false);
        $ABE->Param['BLOCK'] = $Block;
    } elseif (isset($ABE->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $ABE->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $ABE->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $ABE->DB->ErmittleZeilenAnzahl($SQL, $ABE->DB->Bindevariablen('ABF', false));
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $ABE->DB->WertSetzen('ABF', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $ABE->DB->WertSetzen('ABF', 'N0', ($StartZeile + $ZeilenProSeite));
}

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsABF = $ABE->DB->RecordsetOeffnen($SQL, $ABE->DB->Bindevariablen('ABF', true));
$ABE->Form->DebugAusgabe(1, $ABE->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$ABE->Form->SchreibeHTMLCode('<form name="frmVVWDetails" action="./abe_Main.php?cmdAktion=Details' . ' " method=POST  enctype="multipart/form-data">');
$ABE->Form->Formular_Start();

if($rsABF->EOF() and $AWIS_KEY1 == ''){ //Nichts gefunden
    $ABE->Form->Hinweistext($ABE->AWISSprachKonserven['Fehler']['err_keineDaten']);
}elseif (($rsABF->AnzahlDatensaetze() > 1)) { //Liste
    $FeldBreiten = array();
    $FeldBreiten['ABF_FAHRGESTELLNR'] = 220;
    $FeldBreiten['ABF_MOTORNR'] = 180;
    $FeldBreiten['ABF_ATU_NR'] = 120;
    $FeldBreiten['ABF_DRUCK'] = 90;

    $ABE->Form->ZeileStart();
    $Link = './abe_Main.php?cmdAktion=Details&Sort=ABF_FAHRGESTELLNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ABF_FAHRGESTELLNR'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
    $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_FAHRGESTELLNR'], $FeldBreiten['ABF_FAHRGESTELLNR'], '', $Link);

    $Link = './abe_Main.php?cmdAktion=Details&Sort=ABF_MOTORNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ABF_MOTORNR'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
    $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_MOTORNR'], $FeldBreiten['ABF_MOTORNR'], '', $Link);

    $Link = './abe_Main.php?cmdAktion=Details&Sort=ABF_ATU_NR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ABF_ATU_NR'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
    $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_ATU_NR'], $FeldBreiten['ABF_ATU_NR'], '', $Link);

    $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_DRUCK'], $FeldBreiten['ABF_DRUCK'], '', '');

    $ABE->Form->ZeileEnde();
    $DS = 0;    // f�r Hintergrundfarbumschaltung
    while (!$rsABF->EOF()) {
        $ABE->Form->ZeileStart('', 'Zeile"ID="Zeile_' . $rsABF->FeldInhalt('ABF_KEY'));

        $Link = './abe_Main.php?cmdAktion=Details&ABF_KEY=' . $rsABF->FeldInhalt('ABF_KEY') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $TTT = $rsABF->FeldInhalt('ABF_FAHRGESTELLNR');
        $ABE->Form->Erstelle_ListenFeld('ABF_FAHRGESTELLNR', $rsABF->FeldInhalt('ABF_FAHRGESTELLNR'), 0, $FeldBreiten['ABF_FAHRGESTELLNR'], false, ($DS % 2), '', $Link, 'T', 'L',
            $TTT);

        $TTT = $rsABF->FeldInhalt('ABF_MOTORNR');
        $ABE->Form->Erstelle_ListenFeld('ABF_MOTORNR', $rsABF->FeldInhalt('ABF_MOTORNR'), 0, $FeldBreiten['ABF_MOTORNR'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

        $TTT = $rsABF->FeldInhalt('ABF_ATU_NR');
        $ABE->Form->Erstelle_ListenFeld('ABF_ATU_NR', $rsABF->FeldInhalt('ABF_ATU_NR'), 0, $FeldBreiten['ABF_ATU_NR'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

        if ($rsABF->FeldInhalt('ABL_WARTEND') == '1') {
            $IconsArray[] = array("flagge_orange", $Link, "", $ABE->AWISSprachKonserven['ABF']['ABF_ZUDRUCK']);
        } elseif ($rsABF->FeldInhalt('DRUCKANZ') > 0) {
            $IconsArray[] = array("flagge_gruen", $Link, "", $ABE->AWISSprachKonserven['ABF']['ABF_DRUCK']);
        } else {
            $IconsArray[] = array("flagge_rot", $Link, "", $ABE->AWISSprachKonserven['ABF']['ABF_NICHTDRUCK']);
        }
        $ABE->Form->Erstelle_ListeIcons($IconsArray, $FeldBreiten['ABF_DRUCK'], ($DS % 2));
        $IconsArray = [];

        $ABE->Form->ZeileEnde();
        $DS++;
        $rsABF->DSWeiter();
    }
    $Link = './abe_Main.php?cmdAktion=Details';
    $ABE->Form->SchreibeHTMLCode('<form name="frmDetails" action="' . $Link . '" method=POST  enctype="multipart/form-data">');
    $ABE->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    $ABE->Form->SchreibeHTMLCode('</form>');
} else { //Einer oder ein neuer.
    $AWIS_KEY1 = $rsABF->FeldInhalt('ABF_KEY');
    $ABE->Form->Erstelle_HiddenFeld('ABF_KEY', $AWIS_KEY1);
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./abe_Main.php?cmdAktion=Details accesskey=T title='" . $ABE->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $ABE->Form->InfoZeile($Felder, '');
    $LabelBreite = 200;
    $FeldBreite = 260;

    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABF_FAHRGESTELLNR'] . ':', $LabelBreite);
    $ABE->Form->Erstelle_TextFeld('ABF_FAHRGESTELLNR', $rsABF->FeldInhalt('ABF_FAHRGESTELLNR'), 65, $FeldBreite, false);
    $ABE->Form->ZeileEnde();

    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABF_MOTORNR'] . ':', $LabelBreite);
    $ABE->Form->Erstelle_TextFeld('ABF_MOTORNR', $rsABF->FeldInhalt('ABF_MOTORNR'), 65, $FeldBreite, false);
    $ABE->Form->ZeileEnde();

    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABF_ATU_NR'] . ':', $LabelBreite);
    $ABE->Form->Erstelle_TextFeld('ABF_ATU_NR', $rsABF->FeldInhalt('ABF_ATU_NR'), 65, $FeldBreite, false);
    $ABE->Form->ZeileEnde();

    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABF_ATU_NR'] . ':', $LabelBreite);
    $Inhalt = ($rsABF->FeldInhalt('AST_BEZEICHNUNGWW', 'T') != ''?$rsABF->FeldInhalt('AST_BEZEICHNUNGWW', 'T'):$rsABF->FeldInhalt('AST_BEZEICHNUNG', 'T'));
    $ABE->Form->Erstelle_TextFeld('BEZEICHNUNG', $Inhalt, 65, $FeldBreite, false);
    $ABE->Form->ZeileEnde();

    //Checkbox f�r die Warteschlange; Toggelt die anderen Felder
    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABF_ZUDRUCK'] . ':', $LabelBreite);
    $ABE->Form->Erstelle_Checkbox('ABL_WARTEND', ($rsABF->FeldInhalt('ABL_WARTEND') == 1?'on':''), $FeldBreite, true, 'on', '', '',
        'onclick="switchDisplay()" onload="switchDisplay()"');
    $ABE->Form->ZeileEnde();

    $SQL1 = 'SELECT * FROM ABELIEFERSCHEINE ';
    $SQL1 .= ' WHERE ABL_ABF_KEY = ' . $ABE->DB->WertSetzen('ABL1', 'Z', $rsABF->FeldInhalt('ABF_KEY'));
    $SQL1 .= ' AND ABL_XBN_KEY = ' . $ABE->DB->WertSetzen('ABL1', 'Z', $ABE->AWISBenutzer->BenutzerID());
    $SQL1 .= ' AND ABL_WARTEND = 1';

    $SQL0 = 'SELECT * FROM ABELIEFERSCHEINE ';
    $SQL0 .= ' WHERE ABL_ABF_KEY = ' . $ABE->DB->WertSetzen('ABL0', 'Z', $rsABF->FeldInhalt('ABF_KEY'));
    $SQL0 .= ' AND ABL_WARTEND = 0';
    $SQL0 .= ' ORDER BY ABL_DRUCKDATUM DESC';

    $rsABL1 = $ABE->DB->RecordSetOeffnen($SQL1, $ABE->DB->Bindevariablen('ABL1', true));
    $rsABL0 = $ABE->DB->RecordSetOeffnen($SQL0, $ABE->DB->Bindevariablen('ABL0', true));

    //Warteschlangen-Block, Sichtbarkeit durch Script getoggelt
    echo '<div id="druckinfos" style="width: 100%">';
    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABL_VON'] . ':', $LabelBreite);
    $ABE->Form->Erstelle_TextFeld('!ABL_VON', ($rsABL1->FeldInhalt('ABL_VON') != ''?$rsABL1->FeldInhalt('ABL_VON'):($rsABL0->FeldInhalt('ABL_NACH'))), 65, $FeldBreite, true);
    $ABE->Form->ZeileEnde();
    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABL_NACH'] . ':', $LabelBreite);
    $ABE->Form->Erstelle_TextFeld('!ABL_NACH', ($rsABL1->FeldInhalt('ABL_NACH') != ''?$rsABL1->FeldInhalt('ABL_NACH'):''), 65, $FeldBreite, true);
    $ABE->Form->ZeileEnde();
    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABF']['ABL_BEMERKUNG'] . ':', $LabelBreite);
    $ABE->Form->Erstelle_Textarea('!ABL_BEMERKUNG', ($rsABL1->FeldInhalt('ABL_BEMERKUNG') != ''?$rsABL1->FeldInhalt('ABL_BEMERKUNG'):''), $FeldBreite, 30, 1, true);
    $ABE->Form->ZeileEnde();
    $ABE->Form->SchreibeHTMLCode('</div>');

    if ($rsABL0->AnzahlDatensaetze() > 0) {
        $FeldBreiten = array();
        $FeldBreiten['ABL_DRUCKDATUM'] = 220;
        $FeldBreiten['ABL_VON'] = 180;
        $FeldBreiten['ABL_NACH'] = 180;
        $FeldBreiten['ABL_BEMERKUNG'] = 180;

        $ABE->Form->ZeileStart();
        $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABL_DRUCKDATUM'], $FeldBreiten['ABL_DRUCKDATUM']);
        $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABL_VON'], $FeldBreiten['ABL_VON']);
        $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABL_NACH'], $FeldBreiten['ABL_NACH']);
        $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABL_BEMERKUNG'], $FeldBreiten['ABL_BEMERKUNG']);
        $ABE->Form->ZeileEnde();

        while (!$rsABL0->EOF()) {
            $ABE->Form->ZeileStart();
            $ABE->Form->Erstelle_ListenFeld('ABL_DRUCKDATUM', $rsABL0->FeldInhalt('ABL_DRUCKDATUM'), $FeldBreiten['ABL_DRUCKDATUM'], $FeldBreiten['ABL_DRUCKDATUM']);
            $ABE->Form->Erstelle_ListenFeld('ABL_VON', $rsABL0->FeldInhalt('ABL_VON'), $FeldBreiten['ABL_VON'], $FeldBreiten['ABL_VON']);
            $ABE->Form->Erstelle_ListenFeld('ABL_NACH', $rsABL0->FeldInhalt('ABL_NACH'), $FeldBreiten['ABL_NACH'], $FeldBreiten['ABL_NACH']);
            $ABE->Form->Erstelle_ListenFeld('ABL_BEMERKUNG', $rsABL0->FeldInhalt('ABL_BEMERKUNG'), $FeldBreiten['ABL_BEMERKUNG'], $FeldBreiten['ABL_BEMERKUNG']);
            $ABE->Form->ZeileEnde();

            $rsABL0->DSWeiter();
        }
    }
}

//Toggelt die Sichtbarkeit der Warteschlangen-Felder
$Script = 'function switchDisplay() {
                    var checked = $(\'#txtABL_WARTEND\').prop(\'checked\');
                    if (checked == true) {
                        $(\'#druckinfos\').show();
                        $(\'#txtABL_VON\').prop(\'required\', \'required\');
                        $(\'#txtABL_NACH\').prop(\'required\', \'required\');
                        $(\'#txtABL_BEMERKUNG\').prop(\'required\', \'required\');
                    } else if (checked == false) {
                        $(\'#druckinfos\').hide();
                        $(\'#txtABL_VON\').removeAttr(\'required\');
                        $(\'#txtABL_NACH\').removeAttr(\'required\');
                        $(\'#txtABL_BEMERKUNG\').removeAttr(\'required\');
                    }
                }
                
                onload = switchDisplay();';
$ABE->Form->SchreibeHTMLCode("<script>$Script</script>");
$ABE->Form->Formular_Ende();
$ABE->Form->SchaltflaechenStart();
$ABE->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $ABE->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
$rsDS = $ABE->DB->RecordSetOeffnen('SELECT * FROM ABELIEFERSCHEINE WHERE ABL_WARTEND = 1 AND ABL_XBN_KEY = ' . $ABE->AWISBenutzer->BenutzerID());
if ($rsABF->AnzahlDatensaetze() == 1) {
    $ABE->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $ABE->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}

$ABE->Form->SchaltflaechenEnde();
$ABE->Form->SchreibeHTMLCode('</form>');

?>