<?php
require_once 'awisDistributionSalesForce.php';
class awisDistributionCenter
{
     /**
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * FEB_KEY der Ebene
     * @var string
     */
    private $_FEB_KEYs;

    private $_withSalesForce = false;

    private $_SalesForceAsChildren = false;

    private $_asTree = false;

    private $_MaxTiefe = 0;

    function __construct($DB,array $FEB_KEYs)
    {

        $this->_DB = $DB;
        $this->_FEB_KEYs = $FEB_KEYs;
    }

    public function withSalesForce(){
        $this->_withSalesForce = true;
    }

    public function SalesForceAsChildren(){
        $this->_SalesForceAsChildren = true;
    }


    public function asTree(){
        $this->_asTree = true;
    }

    private function _maxTiefe($MaxEbene){
        if($MaxEbene>$this->_MaxTiefe){
            $this->_MaxTiefe = $MaxEbene;
        }
    }

    public function getDistributionCenter($SucheParents, $SucheChilds,array $FER_KEYs = array(), $ValidDate = ''){

        $Return = array();

        if(count($FER_KEYs)>0 and $FER_KEYs[0]!=''){
            foreach($FER_KEYs as $FER_KEY){
                switch ($FER_KEY){
                    case 23:
                        $this->_maxTiefe(3);
                        break;
                    case 25:
                        $this->_maxTiefe(4);
                        break;
                    case 1: //GL
                    case 2: //WL
                    case 3: //Filaccount
                        $this->_MaxTiefe(5);
                        break;
                    case 4: //DIT Zentraluser
                        $this->_MaxTiefe(5);
                    default:
                        $this->_MaxTiefe(9999999);
                }
            }
        }else{
            $this->_MaxTiefe(9999999);
        }

        foreach($this->_FEB_KEYs as $FEB_KEY){

            switch($FEB_KEY['XTN_KUERZEL']){

                case 'FEB':
                    $SQL = 'select * from FILIALEBENEN';
                    $SQL .= ' WHERE FEB_KEY = ' . $this->_DB->WertSetzen('FEB','Z',$FEB_KEY['XXX_KEY']);

                    if($ValidDate!=''){
                        $SQL .=' and FEB_GUELTIGAB <= '.$this->_DB->WertSetzen('FEB','D',$ValidDate);
                        $SQL .=' and FEB_GUELTIGBIS  >= ' . $this->_DB->WertSetzen('FEB','D',$ValidDate);
                    }else{
                        $SQL .=' and FEB_GUELTIGAB <= sysdate';
                        $SQL .=' and FEB_GUELTIGBIS  >= sysdate';
                    }

                    $SQL .=' order by FEB_EBENE asc,FEB_BEZEICHNUNG asc';

                    break;

                case 'FIL':
                    $SQL  ='select';
                    $SQL .='     FIL_ID as FEB_KEY,';
                    $SQL .='     FIL_BEZ as FEB_BEZEICHNUNG,';
                    $SQL .='     5 as FEB_EBENE,';
                    $SQL .='     \'Filiale\' as FEB_BEMERKUNG,';
                    $SQL .='     \'01.01.1970\' as FEB_GUELTIGAB,';
                    $SQL .='     \'31.12.2030\' as FEB_GUELTIGBIS,';
                    $SQL .='     \'1\' as FEB_ANWENDUNG,';
                    $SQL .='     GBL_FEB_KEY as FEB_FEB_KEY';
                    $SQL .=' from';
                    $SQL .='     V_FILIALEN_AKTUELL';
                    $SQL .= ' WHERE FIL_ID =' .$this->_DB->WertSetzen('FEB','Z',$FEB_KEY['XXX_KEY']);
                    break;
                default:
                    var_dump($FEB_KEY);
            }

            $rsFilialebenen = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FEB'));
            
            $DistObj = new stdClass();

            $DistObj->text = $rsFilialebenen->FeldInhalt('FEB_BEZEICHNUNG');
            $DistObj->Level = $rsFilialebenen->FeldInhalt('FEB_EBENE');
            if(!$this->_asTree){
                $DistObj->key = $rsFilialebenen->FeldInhalt('FEB_KEY');
                $DistObj->Description = $rsFilialebenen->FeldInhalt('FEB_BEMERKUNG');
                $DistObj->validFrom = $rsFilialebenen->FeldInhalt('FEB_GUELTIGAB');
                $DistObj->validTo = $rsFilialebenen->FeldInhalt('FEB_GUELTIGBIS');
                $DistObj->Section = $rsFilialebenen->FeldInhalt('FEB_ANWENDUNG');
            }else{
                $DistObj->key = 'dummy';
            }

            //Uebergeordnete Ebenen suchen
            $Parents = array();
            if($SucheParents){
                if($rsFilialebenen->FeldInhalt('FEB_FEB_KEY')!=''){
                    $DistributionCenter = new awisDistributionCenter($this->_DB, array(array('XTN_KUERZEL' =>'FEB', 'XXX_KEY' => $rsFilialebenen->FeldInhalt('FEB_FEB_KEY'))));
                    $this->_withSalesForce?$DistributionCenter->withSalesForce():'';
                    $this->_SalesForceAsChildren?$DistributionCenter->SalesForceAsChildren():'';
                    $this->_asTree?$DistributionCenter->asTree():'';
                    $DistributionCenter->_maxTiefe($this->_MaxTiefe);
                    $Parents[] = $DistributionCenter->getDistributionCenter(true,false, $FER_KEYs,$ValidDate);
                }
            }

            if(count($Parents)>0){
                $DistObj->Parents = $Parents;
            }else{
                unset($DistObj->Parents);
            }
            //Untergeordnete Ebenen ermitteln
            $Childs = array();

            if($SucheChilds and $DistObj->Level < $this->_MaxTiefe ){

                $SQL = 'select * from FILIALEBENEN';
                $SQL .= ' WHERE FEB_FEB_KEY = ' . $this->_DB->WertSetzen('FEB','Z',$FEB_KEY['XXX_KEY']);

                if($ValidDate!=''){
                    $SQL .=' and FEB_GUELTIGAB <= '.$this->_DB->WertSetzen('FEB','D',$ValidDate);
                    $SQL .=' and FEB_GUELTIGBIS  >= ' . $this->_DB->WertSetzen('FEB','D',$ValidDate);
                }else{
                    $SQL .=' and FEB_GUELTIGAB <= sysdate';
                    $SQL .=' and FEB_GUELTIGBIS  >= sysdate';
                }

                $SQL .=' order by FEB_EBENE asc,FEB_BEZEICHNUNG asc';

                $rsChilds = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FEB'));

                while(!$rsChilds->EOF()){
                    $DistributionCenter = new awisDistributionCenter($this->_DB, array(array('XTN_KUERZEL' =>'FEB', 'XXX_KEY' =>$rsChilds->FeldInhalt('FEB_KEY'))));
                    $this->_withSalesForce?$DistributionCenter->withSalesForce():'';
                    $this->_SalesForceAsChildren?$DistributionCenter->SalesForceAsChildren():'';
                    $DistributionCenter->_maxTiefe($this->_MaxTiefe);
                    $this->_asTree?$DistributionCenter->asTree():'';
                    $Childs[] = $DistributionCenter->getDistributionCenter(false,true,$FER_KEYs,$ValidDate);
                    $rsChilds->DSWeiter();
                }
            }


            if($this->_withSalesForce){
                //Personal dieser Ebene ermitteln
                $DistributionSalesForce = new awisDistributionSalesForce($this->_DB, $FEB_KEY['XTN_KUERZEL'], $FEB_KEY['XXX_KEY']);
                if(!$this->_SalesForceAsChildren){
                    $DistObj->SalesForce = $DistributionSalesForce->getSalesForce($ValidDate,$FER_KEYs);
                }else{
                    foreach($DistributionSalesForce->getSalesForce($ValidDate,$FER_KEYs) as $Child){
                        $Childs[] = $Child;
                    }
                }
            }


            if(count($Childs)>0){
                $DistObj->children = $Childs;
            }else{
                unset($DistObj->children);
            }


            if($this->_MaxTiefe>4 and $FEB_KEY['XTN_KUERZEL']!='FIL'){

                //Filialen dieser Ebene ermitteln
                $Stores = array();
                $SQL = 'select * from FILIALEBENENZUORDNUNGEN ';
                $SQL .= ' INNER JOIN FILIALEN ON FEZ_FIL_ID = FIL_ID ';

                if($FEB_KEY['XTN_KUERZEL']=='FIL'){
                    $SQL .= ' WHERE FEZ_FIL_ID = ' . $this->_DB->WertSetzen('FEZ','Z',$FEB_KEY['XXX_KEY']);
                }elseif($FEB_KEY['XTN_KUERZEL'] == 'FEB'){
                    $SQL .= ' WHERE FEZ_FEB_KEY = ' . $this->_DB->WertSetzen('FEZ','Z',$FEB_KEY['XXX_KEY']);
                }

                if($ValidDate!=''){
                    $SQL .=' and FEZ_GUELTIGAB <= '.$this->_DB->WertSetzen('FEZ','D',$ValidDate);
                    $SQL .=' and FEZ_GUELTIGBIS  >= ' . $this->_DB->WertSetzen('FEZ','D',$ValidDate);
                }else{
                    $SQL .=' and FEZ_GUELTIGAB <= sysdate';
                    $SQL .=' and FEZ_GUELTIGBIS  >= sysdate';
                }

                $SQL .= ' order by FEZ_FIL_ID asc';

                $rsFEZ = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FEZ'));;
                while(!$rsFEZ->EOF()){
                    $Store = new stdClass();


                    if($this->_asTree){
                        $Store->key = 'dummy';
                        $Store->text = $rsFEZ->FeldInhalt('FEZ_FIL_ID');
                    }else{
                        $Store->key = $rsFEZ->FeldInhalt('FEZ_FIL_ID');
                        $Store->text = $rsFEZ->FeldInhalt('FIL_BEZ');
                    }

                    $Store->Level = 5;
                    $Store->Description = 'Filiale';
                    $Store->validFrom = '01.01.1970';
                    $Store->validTo = '01.01.2170';
                    $Store->Section = 1;

                    $DistributionSalesForce = new awisDistributionSalesForce($this->_DB, 'FIL', $rsFEZ->FeldInhalt('FEZ_FIL_ID'));
                    if(!$this->_SalesForceAsChildren){
                        $Store->SalesForce = $DistributionSalesForce->getSalesForce($ValidDate,array(1));

                    }else{
                        $Store->children = $DistributionSalesForce->getSalesForce($ValidDate,array(1));
                    }


                    $Stores[] = $Store;
                    $rsFEZ->DSWeiter();
                }
                if(count($Stores)>0){
                    $DistObj->children = $Stores;

                }
            }

            $Return[] = $DistObj;

        }

       return $Return[0];
    }



}