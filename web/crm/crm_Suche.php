<?php
global $CursorFeld;
global $AWISBenutzer;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Liste','lst_Auswahl_ALLE_OHNE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('CAK','CAK_KOT_KEY');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht3700=awisBenutzerRecht($con,3700);

echo "<br>";

echo "<form name=frmSuche method=post action=./crm_Main.php?cmdAktion=Details>";


	// Alle Parameter auslesen
	//	Es wird aber nicht alles eingeblendet!
$TelSuche = explode(";",awis_BenutzerParameter($con, "CRMSuche", $AWISBenutzer->BenutzerName()));

/**********************************************
* * Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheName'].':',190);
awis_FORM_Erstelle_TextFeld('*SuchName','',20,200,true);
$CursorFeld='sucSuchName';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SuchePLZ'].':',190);
awis_FORM_Erstelle_TextFeld('*SuchPLZ','',10,0,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheOrt'].':',190);
awis_FORM_Erstelle_TextFeld('*SuchOrt','',10,200,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_LAN_CODE'].':',190);
$SQL = 'SELECT LAN_CODE, LAN_LAND FROM Laender ORDER BY LAN_LAND';
awis_FORM_Erstelle_SelectFeld('*CAD_LAN_CODE','',-40,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheStrasse'].':',190);
awis_FORM_Erstelle_TextFeld('*SucheStrasse','',30,280,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheBetreuer'].':',190);
/*$SQL = 'SELECT KON_KEY, KON_NAME1 || \', \' || KON_Name2 AS Name FROM Kontakte';
$SQL .= ' INNER JOIN KontakteAbteilungenZuordnungen ON KON_KEY = KZA_KON_KEY';
$SQL.=' WHERE KZA_KAB_KEY=268';
$SQL .= ' ORDER BY KON_NAME1 , KON_NAME2';
*/

/*
$SQL = "select DISTINCT * FROM (SELECT  kon_key, kon_name1 || ', ' || COALESCE(kon_name2,'') AS kontaktName";
$SQL .= ' FROM kontakte';
$SQL .= ' INNER JOIN benutzer B on kon_key = B.xbn_kon_key';// and xbn_status=\'A\'';
$SQL .= ' LEFT OUTER JOIN V_ACCOUNTRECHTE R on B.xbn_key = R.xbn_key';
$SQL .= ' WHERE R.XRC_ID = 3704 AND BITAND(XBA_STUFE,16)=16 and kon_status=\'A\'';
$SQL .= ' OR EXISTS(SELECT * FROM CRMADressen WHERE CAD_KON_KEY = KON_KEY)';
$SQL .= ' )';
$SQL .= ' ORDER BY kontaktName';
*/


$SQL  ='select';
$SQL .='   *';
$SQL .=' from';
$SQL .='   (';
$SQL .='     select distinct';
$SQL .='       KON.KON_KEY,';
$SQL .='       KON.KON_NAME1';
$SQL .='       || \', \'';
$SQL .='       || coalesce(KON.KON_NAME2,\'\') as KONTAKTNAME';
$SQL .='     from';
$SQL .='       CRMADRESSEN a';
$SQL .='     left join';
$SQL .='       (';
$SQL .='         select distinct';
$SQL .='           *';
$SQL .='         from';
$SQL .='           (';
$SQL .='             select';
$SQL .='               KON_KEY,';
$SQL .='               KON_NAME1';
$SQL .='               || \', \'';
$SQL .='               || coalesce(KON_NAME2,\'\') as KONTAKTNAME';
$SQL .='             from';
$SQL .='               KONTAKTE';
$SQL .='             inner join BENUTZER B';
$SQL .='             on';
$SQL .='               KON_KEY = B.XBN_KON_KEY';
$SQL .='             left outer join V_ACCOUNTRECHTE R';
$SQL .='             on';
$SQL .='               B.XBN_KEY = R.XBN_KEY';
$SQL .='             where';
$SQL .='               R.XRC_ID              = 3704';
$SQL .='             and BITAND(XBA_STUFE,16)=16';
$SQL .='             and KON_STATUS          =\'A\'';
$SQL .='           )';
$SQL .='         order by';
$SQL .='           KONTAKTNAME';
$SQL .='       )';
$SQL .='       GIBTS on a.CAD_KON_KEY = GIBTS.KON_KEY';
$SQL .='     left join KONTAKTE KON';
$SQL .='     on';
$SQL .='       KON.KON_KEY = CAD_KON_KEY';
$SQL .='     where';
$SQL .='       GIBTS.KON_KEY is null';
$SQL .='     union';
$SQL .='     select distinct';
$SQL .='       *';
$SQL .='     from';
$SQL .='       (';
$SQL .='         select';
$SQL .='           KON_KEY,';
$SQL .='           KON_NAME1';
$SQL .='           || \', \'';
$SQL .='           || coalesce(KON_NAME2,\'\') as KONTAKTNAME';
$SQL .='         from';
$SQL .='           KONTAKTE';
$SQL .='         inner join BENUTZER B';
$SQL .='         on';
$SQL .='           KON_KEY = B.XBN_KON_KEY';
$SQL .='         left outer join V_ACCOUNTRECHTE R';
$SQL .='         on';
$SQL .='           B.XBN_KEY = R.XBN_KEY';
$SQL .='         where';
$SQL .='           R.XRC_ID              = 3704';
$SQL .='         and BITAND(XBA_STUFE,16)=16';
$SQL .='         and KON_STATUS          =\'A\'';
$SQL .='       )';
$SQL .='   )';
$SQL .=' where';
$SQL .='   KON_KEY is not null';
$SQL .=' order by';
$SQL .='   KONTAKTNAME';



$KateogrieAlt = explode("|",$AWISSprachKonserven['Liste']['lst_Auswahl_ALLE_OHNE']);
awis_FORM_Erstelle_SelectFeld('*SucheBetreuer','',200,true,$con,$SQL,'','','','',$KateogrieAlt);
awis_FORM_ZeileEnde();


awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_CGB_KEY'].':',190);
$SQL = "select CGB_KEY, CGB_Gebiet";
$SQL .= ' FROM CRMGebiete';
$SQL .= ' ORDER BY CGB_Gebiet';
awis_FORM_Erstelle_SelectFeld('*SucheGebiet','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();


awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheVertrag'].':',190);
awis_FORM_Erstelle_TextFeld('*SucheVertrag','',10,200,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheKontaktWert'].':',190);
awis_FORM_Erstelle_TextFeld('*CAK_WERT','',10,200,true,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_STATUS'].':',190);
$KateogrieAlt = explode("|",$AWISSprachKonserven['CAD']['lst_CAD_STATUS']);
awis_FORM_Erstelle_SelectFeld('*CAD_STATUS','',130,true,$con,'','~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'A','','',$KateogrieAlt,'');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_IMQ_ID'].':',190);
$SQL = 'SELECT IMQ_ID, IMQ_IMPORTQUELLE FROM Importquellen ';
$SQL .= ' WHERE EXISTS(SELECT * FROM CRMADRESSEN WHERE CAD_IMQ_ID = IMQ_ID)';
$SQL .= ' ORDER BY 2';
awis_FORM_Erstelle_SelectFeld('*CAD_IMQ_ID','',-40,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CIT_3006'].':',190);
$SQL = 'SELECT DISTINCT CIN_WERT AS KEY, CIN_WERT';
$SQL .= ' FROM CRMINFOS ';
$SQL .= ' WHERE CIN_CIT_ID = 3006';
$SQL .= ' ORDER BY 1';
awis_FORM_Erstelle_SelectFeld('*CIT_3006','',-40,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_RANKING'].':',190);
$KateogrieAlt = explode("|",$AWISSprachKonserven['CAD']['lst_CAD_RANKING']);
awis_FORM_Erstelle_SelectFeld('*CAD_RANKING','',200,true,$con,'','~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$KateogrieAlt,'');
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();


awis_FORM_SchaltflaechenStart();
	// Zur�ck zum Men�
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Recht3700&4) == 4)		// Hinzuf�gen erlaubt?
{
	awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

awis_FORM_SchaltflaechenEnde();


if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>