<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?

require_once('db.inc.php');
require_once('register.inc.php');
require_once('awisAusdruck.php');
require_once('awis_forms.inc.php');

global $awisRSZeilen;

$AWISBenutzer = new awisUser();

$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');

$con = awisLogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName());

$RechteStufe = awisBenutzerRecht($con,3600);	// MDE Lieferkontrolle
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Lieferschein-PDF', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug(1,$_GET);

$TXT_Baustein = array();
$TXT_Baustein[]=array('Ausdruck','txtHinweisVertraulich');
$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);

if ($_REQUEST['LKK_LIEFERSCHEINNR']!='' && $_REQUEST['LKK_FIL_ID']!= '' ) 
{
	$FILIALE = intval($_REQUEST['LKK_FIL_ID']);
	
	/***************************************
	* Hole Daten per SQL
	***************************************/	
	$SQL = 'SELECT DISTINCT LIK_FIL_ID, LIK_LieferscheinNr, LIK_AST_ATUNR, LIK_MENGESOLL, LIK_DATUMKOMM, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, AST_BEZEICHNUNGWW';
	$SQL .=' FROM Lieferkontrollen LIK';
	$SQL .= ' LEFT JOIN ARTIKELSTAMM ON AST_ATUNR = LIK_AST_ATUNR';
	$SQL .= ' LEFT JOIN FILIALEN ON FIL_ID = LIK_FIL_ID';
	$SQL .=' WHERE LIK_FIL_ID=0'.intval($_REQUEST['LKK_FIL_ID']);
	$SQL .= ' AND LIK_LieferscheinNr=0'.intval($_REQUEST['LKK_LIEFERSCHEINNR']);
	$SQL .= ' ORDER BY LIK_AST_ATUNR ASC';
		
	awis_Debug(1,$SQL);	
	
	$rsLIK = awisOpenRecordset($con,$SQL);
	$rsLIKZeilen = $awisRSZeilen;
	
	if($rsLIKZeilen==0)		// Keine Daten
	{
		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Lieferschein');
		$Ausdruck->NeueSeite(0,1);		// Mit Hintergrund
		$Ausdruck->_pdf->SetFont('Arial','',20);
		$Ausdruck->_pdf->SetXY(50,50);
		$Ausdruck->_pdf->Cell(100,5,'Keine Daten zum Drucken!',0,0,'L',0);

		$Ausdruck->Anzeigen();
		die();
	}	
	else 
	{
		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
		$Zeile=0;
		$Seitenrand=15;
		$Seite = 1;
		
		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Lieferschein');
		$Ausdruck->NeueSeite(0,1);				
		$Ausdruck->_pdf->SetFillColor(210,210,210);			
		
		// Fu�zeile
		$Ausdruck->_pdf->SetFont('Arial','',8);
		$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
		$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
		$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
		$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$TXT_Baustein['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
		$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
		$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
				
		$LinkerRand=$Seitenrand;		
		$Y_Wert=10;
				
		$LSNR = $rsLIK['LIK_LIEFERSCHEINNR'][0];
		$Kommissionierung = $rsLIK['LIK_DATUMKOMM'][0];
		$Lager = $rsLIK['LIK_QUELLE'][0];
		$Filiale = $rsLIK['LIK_FIL_ID'][0];
		$FilBez = $rsLIK['FIL_BEZ'][0];
		$FilStrasse = $rsLIK['FIL_STRASSE'][0];
		$FilOrt = $rsLIK['FIL_ORT'][0];
		$FilPLZ = $rsLIK['FIL_PLZ'][0];
				
		$Ausdruck->_pdf->SetFont('Arial','B',14);
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(40,6,"LIEFERSCHEIN",0,0,'L',0);
		
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->setXY($LinkerRand+65,$Y_Wert);
		$Ausdruck->_pdf->cell(15,6,'Nr.: ',0,0,'L',0);						
		$Ausdruck->_pdf->cell(30,6,$LSNR,0,0,'L',0);						
		$Y_Wert=$Y_Wert+5;		
		$Ausdruck->_pdf->setXY($LinkerRand+65,$Y_Wert);
		$Ausdruck->_pdf->cell(15,6,'Datum: ',0,0,'L',0);						
		$Ausdruck->_pdf->cell(30,6,$Kommissionierung,0,0,'L',0);						
		
		$Y_Wert=$Y_Wert+5;		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);				
		$Ausdruck->_pdf->cell(40,6,$FilBez,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;				
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(40,6,$FilStrasse,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(40,6,$FilPLZ.' '.$FilOrt,0,0,'L',0);
		
		//Listen�berschriften			
		$Y_Wert=$Y_Wert+10;		
		$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
		$Ausdruck->_pdf->cell(15,6,"ATUNR",1,0,'L',1);				
		
		$Ausdruck->_pdf->setXY($LinkerRand+15,$Y_Wert);
		$Ausdruck->_pdf->cell(90,6,"Artikelbezeichnung",1,0,'L',1);		
		
		$Ausdruck->_pdf->setXY($LinkerRand+105,$Y_Wert);
		$Ausdruck->_pdf->cell(50,6,"EAN-Nummer",1,0,'L',1);		
		
		$Ausdruck->_pdf->setXY($LinkerRand+155,$Y_Wert);
		$Ausdruck->_pdf->cell(15,6,"Menge",1,0,'L',1);														
		
		for($LIKZeile=0;$LIKZeile<$rsLIKZeilen;$LIKZeile++)
		{
			//Neue Seite beginnen			
			if ($Y_Wert > 270)
			{
				$Y_Wert=10;				
				$Seite = $Seite + 1;
				
				$Ausdruck->NeueSeite(0,1);
				
				$Ausdruck->_pdf->SetFont('Arial','B',14);
				$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
				$Ausdruck->_pdf->cell(40,6,"LIEFERSCHEIN",0,0,'L',0);
				
				$Ausdruck->_pdf->SetFont('Arial','B',10);
				$Ausdruck->_pdf->setXY($LinkerRand+65,$Y_Wert);				
				$Ausdruck->_pdf->cell(15,6,'Nr.: ',0,0,'L',0);						
				$Ausdruck->_pdf->cell(30,6,$LSNR,0,0,'L',0);						
				$Y_Wert=$Y_Wert+5;		
				$Ausdruck->_pdf->setXY($LinkerRand+65,$Y_Wert);
				$Ausdruck->_pdf->cell(15,6,'Datum: ',0,0,'L',0);						
				$Ausdruck->_pdf->cell(30,6,$Kommissionierung,0,0,'L',0);						
												
				// Fu�zeile
				$Ausdruck->_pdf->SetFont('Arial','',8);
				$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
				$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
				$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
				$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$TXT_Baustein['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
				$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
				$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
															
				//Listen�berschrifen
				$Ausdruck->_pdf->SetFont('Arial','B',10);
				$Y_Wert=$Y_Wert+10;		
				$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
				$Ausdruck->_pdf->cell(15,6,"ATUNR",1,0,'L',1);						
				
				$Ausdruck->_pdf->setXY($LinkerRand+15,$Y_Wert);
				$Ausdruck->_pdf->cell(90,6,"Artikelbezeichnung",1,0,'L',1);				
				
				$Ausdruck->_pdf->setXY($LinkerRand+105,$Y_Wert);
				$Ausdruck->_pdf->cell(50,6,"EAN-Nummer",1,0,'L',1);		
				
				$Ausdruck->_pdf->setXY($LinkerRand+155,$Y_Wert);
				$Ausdruck->_pdf->cell(15,6,"Menge",1,0,'L',1);								
			}

			$ATUNR=$rsLIK['LIK_AST_ATUNR'][$LIKZeile];						
			$SOLLMENGE=$rsLIK['LIK_MENGESOLL'][$LIKZeile];
			$BEZEICHNUNG=$rsLIK['AST_BEZEICHNUNGWW'][$LIKZeile];			
			
			//HAUPTEAN ermitteln
			$BindeVariablen=array();
			$BindeVariablen['var_T_ast_atunr']=$rsLIK['LIK_AST_ATUNR'][$LIKZeile];
			$BindeVariablen['var_N0_asi_ait_id']=250;
		
			$SQL = 'SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = :var_T_ast_atunr AND ASI_AIT_ID = :var_N0_asi_ait_id';
			$rsASI = awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
			$rsASIZeilen = $awisRSZeilen;
			$EAN=$rsASI['ASI_WERT'][0];			
			
			
			$Y_Wert=$Y_Wert+6;
			$Ausdruck->_pdf->SetFont('Arial','',9);
			$Ausdruck->_pdf->setXY($LinkerRand,$Y_Wert);
			$Ausdruck->_pdf->cell(15,6,$ATUNR,1,0,'L',0);	
			
			$Ausdruck->_pdf->setXY($LinkerRand+15,$Y_Wert);
			$Ausdruck->_pdf->cell(90,6,$BEZEICHNUNG,1,0,'L',0);
			
			$Ausdruck->_pdf->setXY($LinkerRand+105,$Y_Wert);
			$Ausdruck->_pdf->cell(50,6,$EAN,1,0,'L',0);
						
			$Ausdruck->_pdf->setXY($LinkerRand+155,$Y_Wert);
			$Ausdruck->_pdf->cell(15,6,$SOLLMENGE,1,0,'R',0);								
		}		
	}	
	
	$Ausdruck->Anzeigen();
}

?>
</body>
</html>
