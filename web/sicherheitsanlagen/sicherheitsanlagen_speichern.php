<?php

global $AWIS_KEY1;
global $AWIS_KEY2;
global $BMV;

try {
    $BMV->Form->DebugAusgabe(1, 'speichern', $_POST);
    $BMV->Form->DebugAusgabe(1, 'speichern', $_GET);

    $Meldung = '';
    $Datumsfelder = array('txtBMR_REDATUM' );
    foreach ($Datumsfelder as $Feld){
        if(isset($_POST[$Feld])){
            if(!$BMV->Form->PruefeDatum($_POST[$Feld],false,false,false,false)){
                $Meldung .= $BMV->AWISSprachKonserven['BMV']['BMV_ERR_DATUM'] . $BMV->AWISSprachKonserven[substr($Feld,3,3)][substr($Feld,3)] . '<br>';
            }
        }
    }
    if ($Meldung == '') {
        if ($AWIS_KEY1 == -1) { // neuer Vertrag

            $SQL = 'insert';
            $SQL .= ' into bmavertraege';
            $SQL .= ' (';
            $SQL .= '    BMV_VERTRAGSNR,';
            $SQL .= '    BMV_MIETE,';
            $SQL .= '    BMV_MIETFREI,';
            $SQL .= '    BMV_TNNL,';
            $SQL .= '    BMV_KENNUNG,';
            $SQL .= '    BMV_LAUFZEITENDE,';
            $SQL .= '    BMV_BMK_KEY,';
            if (isset($_POST['txtBMV_FIL_ID'])) {
                $SQL .= '    BMV_FIL_ID,';
            }
            if (isset($_POST['txtBMV_ZENTRALBEREICH'])) {
                $SQL .= '    BMV_ZENTRALBEREICH,';
            }
            $SQL .= '    BMV_USER,';
            $SQL .= '    BMV_USERDAT';
            $SQL .= ' )';
            $SQL .= 'values ';
            $SQL .= '(';
            $SQL .= $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_VERTRAGSNR']) . ',';
            $SQL .= $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_MIETE']) . ',';
            $SQL .= $BMV->DB->WertSetzen('BMV', 'D', $_POST['txtBMV_MIETFREI']) . ',';
            $SQL .= $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_TNNL']) . ',';
            $SQL .= $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_KENNUNG']) . ',';
            $SQL .= $BMV->DB->WertSetzen('BMV', 'D', $_POST['txtBMV_LAUFZEITENDE']) . ',';
            $SQL .= $BMV->DB->WertSetzen('BMV', 'N0', $_POST['txtBMV_BMK_KEY']) . ',';
            if (isset($_POST['txtBMV_FIL_ID'])) {
                $SQL .= $BMV->DB->WertSetzen('BMV', 'N0', $_POST['txtBMV_FIL_ID']) . ',';
            }
            if (isset($_POST['txtBMV_ZENTRALBEREICH'])) {
                $SQL .= $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_ZENTRALBEREICH']) . ',';
            }
            $SQL .= $BMV->DB->WertSetzen('BMV', 'T', $BMV->AWISBenutzer->BenutzerName()) . ',';
            $SQL .= 'sysdate';
            $SQL .= ')';

            $BMV->DB->Ausfuehren($SQL, '', '', $BMV->DB->Bindevariablen('BMV'));
            $BMV->Form->DebugAusgabe(1, $BMV->DB->LetzterSQL());

            $AWIS_KEY1 = 'select seq_bmv_key.currval from dual';
            $AWIS_KEY1 = $BMV->DB->RecordSetOeffnen($AWIS_KEY1)->FeldInhalt(1);
            $Meldung .= $BMV->AWISSprachKonserven['BMV']['BMV_SPEICHERN_OK'];
        } elseif ($AWIS_KEY1 > 0 and $AWIS_KEY2 == '') { // geaenderter Vertrag keine Aenderungen bei den Rechnungen

            $SQL = 'update bmavertraege ';
            $SQL .= 'set ';

            if ($_POST['txtBMK_BEREICH'] == 'FIL') {
                $SQL .= 'bmv_fil_id = ' . $BMV->DB->WertSetzen('BMV', 'N0', $_POST['txtBMV_FIL_ID']) . ',';
            } else {
                $SQL .= 'bmv_zentralbereich = ' . $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_ZENTRALBEREICH']) . ',';
            }
            $SQL .= 'bmv_vertragsnr = ' . $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_VERTRAGSNR']) . ',';
            $SQL .= 'bmv_miete = ' . $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_MIETE']) . ',';
            $SQL .= 'bmv_mietfrei = ' . $BMV->DB->WertSetzen('BMV', 'D', $_POST['txtBMV_MIETFREI']) . ',';
            $SQL .= 'bmv_tnnl = ' . $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_TNNL']) . ',';
            $SQL .= 'bmv_kennung = ' . $BMV->DB->WertSetzen('BMV', 'T', $_POST['txtBMV_KENNUNG']) . ',';
            $SQL .= 'bmv_laufzeitende = ' . $BMV->DB->WertSetzen('BMV', 'D', $_POST['txtBMV_LAUFZEITENDE']) . ',';
            $SQL .= 'bmv_bmk_key = ' . $BMV->DB->WertSetzen('BMV', 'N0', $_POST['txtBMV_BMK_KEY']) . ',';
            $SQL .= 'bmv_user = ' . $BMV->DB->WertSetzen('BMV', 'T', $BMV->AWISBenutzer->BenutzerName()) . ',';
            $SQL .= 'bmv_userdat = sysdate';
            $SQL .= ' where bmv_key = ' . $BMV->DB->WertSetzen('BMV', 'N0', $_POST['txtBMV_KEY']);

            $BMV->DB->Ausfuehren($SQL, '', '', $BMV->DB->Bindevariablen('BMV'));
            $Meldung .= $BMV->AWISSprachKonserven['BMV']['BMV_SPEICHERN_OK'];
        } elseif ($AWIS_KEY1 > 0 AND $AWIS_KEY2 == -1) { // neue Rechnung

            $Param['BMR_REDATUM'] = $_POST['txtBMR_REDATUM'];
            $Param['BMR_RENUMMER'] = $_POST['txtBMR_RENUMMER'];
            $Param['BMR_TEXTFELD'] = $_POST['txtBMR_TEXTFELD'];
            $Param['BMR_NETTO'] = $_POST['txtBMR_NETTO'];

            $BMV->AWISBenutzer->ParameterSchreiben('Rechnung_Sicherheitsanlagen', serialize($Param));

            if ($Meldung == '') {
                $SQL = 'insert';
                $SQL .= ' into bmarechnungen';
                $SQL .= ' (';
                $SQL .= '    BMR_BMV_KEY,';
                $SQL .= '    BMR_REDATUM,';
                $SQL .= '    BMR_RENUMMER,';
                $SQL .= '    BMR_TEXTFELD,';
                $SQL .= '    BMR_NETTO,';
                $SQL .= '    BMR_USER,';
                $SQL .= '    BMR_USERDAT';
                $SQL .= ' ) ';
                $SQL .= 'values ';
                $SQL .= '(';
                $SQL .= $BMV->DB->WertSetzen('BMR', 'N0', $_POST['txtBMV_KEY']) . ',';
                $SQL .= $BMV->DB->WertSetzen('BMR', 'D', $_POST['txtBMR_REDATUM']) . ',';
                $SQL .= $BMV->DB->WertSetzen('BMR', 'T', $_POST['txtBMR_RENUMMER']) . ',';
                $SQL .= $BMV->DB->WertSetzen('BMR', 'T', $_POST['txtBMR_TEXTFELD']) . ',';
                $SQL .= $BMV->DB->WertSetzen('BMR', 'T', $_POST['txtBMR_NETTO']) . ',';
                $SQL .= $BMV->DB->WertSetzen('BMR', 'T', $BMV->AWISBenutzer->BenutzerName()) . ',';
                $SQL .= 'sysdate';
                $SQL .= ')';

                $BMV->DB->Ausfuehren($SQL, '', '', $BMV->DB->Bindevariablen('BMR'));

                $AWIS_KEY2 = ''; //Nach anlage einer Rechnung, gleich in die Liste springen
                $Meldung .= $BMV->AWISSprachKonserven['BMV']['BMV_SPEICHERN_OK'];
                $BMV->Form->DebugAusgabe(1, $BMV->DB->LetzterSQL());
            }
        } elseif ($AWIS_KEY1 > 0 and $AWIS_KEY2 > 0) { // geanderte Rechnung

            $Param['BMR_REDATUM'] = $_POST['txtBMR_REDATUM'];
            $Param['BMR_RENUMMER'] = $_POST['txtBMR_RENUMMER'];
            $Param['BMR_TEXTFELD'] = $_POST['txtBMR_TEXTFELD'];
            $Param['BMR_NETTO'] = $_POST['txtBMR_NETTO'];

            $BMV->AWISBenutzer->ParameterSchreiben('Rechnung_Sicherheitsanlagen', serialize($Param));
            
            $SQL = 'update BMARECHNUNGEN set';
            $SQL .= ' bmr_redatum = ' . $BMV->DB->WertSetzen('BMR', 'D', $_POST['txtBMR_REDATUM']);
            $SQL .= ', bmr_renummer = ' . $BMV->DB->WertSetzen('BMR', 'T', $_POST['txtBMR_RENUMMER']);
            $SQL .= ', bmr_textfeld = ' . $BMV->DB->WertSetzen('BMR', 'T', $_POST['txtBMR_TEXTFELD']);
            $SQL .= ', bmr_netto = ' . $BMV->DB->WertSetzen('BMR', 'T', $_POST['txtBMR_NETTO']);
            $SQL .= ', bmr_user = ' . $BMV->DB->WertSetzen('BMR', 'T', $BMV->AWISBenutzer->BenutzerName());
            $SQL .= ', bmr_userdat = sysdate';
            $SQL .= ' where bmr_key = ' . $AWIS_KEY2;

            $BMV->DB->Ausfuehren($SQL, '', '', $BMV->DB->Bindevariablen('BMR'));
            $BMV->Form->DebugAusgabe(1, $BMV->DB->LetzterSQL());
            $Meldung .= $BMV->AWISSprachKonserven['BMV']['BMV_SPEICHERN_OK'];
        }
    }
    if($Meldung!=''){
        $BMV->Form->ZeileStart();
        $BMV->Form->Hinweistext($Meldung);
        $BMV->Form->ZeileEnde();
    }
} catch (awisException $ex) {
    $BMV->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $BMV->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $BMV->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}