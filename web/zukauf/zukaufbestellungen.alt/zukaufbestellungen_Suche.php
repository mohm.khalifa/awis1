<?
global $CursorFeld;
global $AWISBenutzer;
global $con;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZUB','%');
$TextKonserven[]=array('AST','AST_ATUNR');
$TextKonserven[]=array('ZSZ','ZSZ_SORTIMENT');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','Datum%');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht10003=awisBenutzerRecht($con,10003);

echo "<br>";
echo "<form name=frmSuche method=post action=./zukaufbestellungen_Main.php?cmdAktion=Details>";

/**********************************************
* Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'].':',190);
awis_FORM_Erstelle_TextFeld('*ZUB_ARTIKELNUMMER','',20,200,true);
$CursorFeld='sucZUB_ARTIKELNUMMER';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELBEZEICHNUNG'].':',190);
awis_FORM_Erstelle_TextFeld('*ZUB_ARTIKELBEZEICHNUNG','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLI_KEY'].':',190);
$SQL = 'SELECT ZLI_KEY, ZLI_BEZEICHNUNG FROM ZUKAUFLIEFERANTEN';
$SQL .= ' ORDER BY ZLI_BEZEICHNUNG';
awis_FORM_Erstelle_SelectFeld('*ZUB_ZLI_KEY','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLH_KEY'].':',190);
$SQL = 'SELECT ZLH_KUERZEL, ZLH_KUERZEL || \' - \' || ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
awis_FORM_Erstelle_SelectFeld('*ZUB_ZLH_KEY','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'].':',190);
$SQL = 'SELECT DISTINCT ZSZ_SORTIMENT FROM ZUKAUFSORTIMENTZUORDNUNGEN';
$SQL .= ' ORDER BY ZSZ_SORTIMENT';
awis_FORM_Erstelle_SelectFeld('*ZSZ_SORTIMENT','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ATUNR'].':',190);
awis_FORM_Erstelle_TextFeld('*AST_ATUNR','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_FIL_ID'].':',190);
awis_FORM_Erstelle_TextFeld('*ZUB_FIL_ID','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANR'].':',190);
awis_FORM_Erstelle_TextFeld('*ZUB_WANR','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
awis_FORM_Erstelle_TextFeld('*DATUMVOM','',20,200,true);
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',190);
awis_FORM_Erstelle_TextFeld('*DATUMBIS','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();


awis_FORM_SchaltflaechenStart();
	// Zur�ck zum Men�
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Recht10003&4) == 4)		// Hinzuf�gen erlaubt?
{
	awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

awis_FORM_SchaltflaechenEnde();


if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>