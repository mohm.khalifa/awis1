<?php
/**
 * Suche Stammdatenpflege GDV-Nachrichtentypen
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607080950
 * @todo
 */
global $AWISBenutzer;
global $AWISCursorPosition;


try
{
	$TextKonserven = array();
	$TextKonserven[]=array('GDV','*');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','AlleAnzeigen');

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht44001 = $AWISBenutzer->HatDasRecht(44001);

	if($Recht44001==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$Form->SchreibeHTMLCode('<form name="frmNachrichtentypen" action="nachrichtentypen_Main.php?cmdAktion=Details" method="POST"  >');
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GDV_NT'));

	$Form->Formular_Start();
	/**
	 *  Versicherung
	 */
	/* Daten beschaffen */
	$SQL  ='SELECT ';
	$SQL .=' distinct VVE_VERSNR,VVE_BEZEICHNUNG ';
	$SQL .=' FROM ';
	$SQL .='versnachrichtentypen ';
	$SQL .=' inner join versversicherungen on vnt_versnr = vve_versnr';
	$SQL .=' order by VVE_BEZEICHNUNG ';


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'].':',200);
	$AWISCursorPosition= 'sucVNT_VERSICHERUNG';
	$Form->Erstelle_SelectFeld('*VNT_VERSICHERUNG',($Param['SPEICHERN']=='on'?$Param['VNT_VERSICHERUNG']:''),150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'','','','',array(),'');
	$Form->ZeileEnde();

	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();
	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','/gdvService/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>