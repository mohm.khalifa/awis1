<?php
require_once 'gutscheinaktion_funktionen.inc';

try
{
    $GUT = new gutscheinaktion_funktionen();
    $Speichern = isset($GUT->Param['SPEICHERN']) && $GUT->Param['SPEICHERN'] == 'on'?true:false;

    $GUT->RechteMeldung(0);//Anzeigen Recht
	echo "<form name=frmSuche method=post action=./gutscheinaktion_Main.php?cmdAktion=Details>";

	$GUT->Form->Formular_Start();

    if($GUT->AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING )!= ''){
        $Filialen = $GUT->AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_ARRAY);
        $Daten = array();
        foreach ($Filialen as $Fil){
            $Daten[] = $Fil . '~' .$Fil;
        }
        $GUT->Form->ZeileStart();
        $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_FIL_ID'].':',190);
        $GUT->Form->Erstelle_SelectFeld('*GUT_FIL_ID',($Speichern?$GUT->Param['GUT_FIL_ID']:''),'105:110',true,'','','','','',$Daten);
        $GUT->Form->ZeileEnde();
    }else{
        $GUT->Form->ZeileStart();
        $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_FIL_ID'].':',190);
        $GUT->Form->Erstelle_TextFeld('*GUT_FIL_ID',($Speichern?$GUT->Param['GUT_FIL_ID']:''),15,150,true);
        $GUT->Form->ZeileEnde();
    }



	$GUT->Form->ZeileStart();
	$GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_VORGANGSNUMMER'].':',190);
	$GUT->Form->Erstelle_TextFeld('*GUT_VORGANGSNUMMER',($Speichern?$GUT->Param['GUT_VORGANGSNUMMER']:''),15,150,true);
	$GUT->AWISCursorPosition='sucGUT_VORGANGSNUMMER';
	$GUT->Form->ZeileEnde();

	$GUT->Form->ZeileStart();
	$GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_GUTSCHEINNR'].':',190);
	$GUT->Form->Erstelle_TextFeld('*GUT_GUTSCHEINNR',($Speichern?$GUT->Param['GUT_GUTSCHEINNR']:''),15,150,true);
    $GUT->Form->ZeileEnde();


    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_VORNAME'].':',190);
    $GUT->Form->Erstelle_TextFeld('*GUT_VORNAME',($Speichern?$GUT->Param['GUT_VORNAME']:''),15,150,true,'','','','T');
    $GUT->Form->ZeileEnde();

    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_NACHNAME'].':',190);
    $GUT->Form->Erstelle_TextFeld('*GUT_NACHNAME',($Speichern?$GUT->Param['GUT_NACHNAME']:''),15,150,true,'','','','T');
    $GUT->Form->ZeileEnde();

    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['GUT']['GUT_STATUS'].':',190);
    $Daten = $GUT->AWISSprachKonserven['GUT']['GUT_LST_STATUS'];
    $Daten = explode('|',$Daten);
    $GUT->Form->Erstelle_SelectFeld('*GUT_STATUS',($Speichern?$GUT->Param['GUT_STATUS']:''),'150:150',true,'',$GUT->OptionBitteWaehlen,'','','',$Daten);
    $GUT->Form->ZeileEnde();





    // Auswahl kann gespeichert werden
    $GUT->Form->ZeileStart();
    $GUT->Form->Erstelle_TextLabel($GUT->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $GUT->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $GUT->Form->ZeileEnde();

	$GUT->Form->Formular_Ende();

	$GUT->Form->SchaltflaechenStart();
	$GUT->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$GUT->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$GUT->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $GUT->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($GUT->Recht55000&2) == 2){
		$GUT->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $GUT->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$GUT->Form->SchaltflaechenEnde();

	$GUT->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($GUT->Form instanceof awisFormular)
	{
		$GUT->Form->DebugAusgabe(1, $ex->getSQL());
		$GUT->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$GUT->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($GUT->Form instanceof awisFormular)
	{
		$GUT->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>