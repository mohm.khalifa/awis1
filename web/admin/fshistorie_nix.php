<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - FS-Historie</title>
<link rel=stylesheet type=text/css href=/ATU.css>
</head>

<body>
<? 

require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("ATU_Header.php");	// Kopfzeile

if(!(awisHatDasRecht(10)))
{
	awisEreignis(3,1000,'Benutzerverwaltung',"$PHP_AUTH_USER",'','','');
	die("Keine ausreichenden Rechte!");
}

print "<p>�bersicht �ber die Filesysteme: </p>";

$admcon = awisAdminLogon();

if($admcon)
{
	$SQL = "SELECT RN_NAME, FS_FILESYSTEM, FS_MOUNTPOINT, FS_UEBERWACHEN, FS_ANZEIGEN, FS_ID FROM V_FILESYSTEME ";
	$SQL = $SQL."WHERE UPPER(RN_NAME) = '".strtoupper($rname)."'";
	$rsFS = awisOpenRecordset($admcon, $SQL);
	$rsFSZeilen = $awisRSZeilen;
	
	$anzahlfs = 0;
	for($i=0; $i<$rsFSZeilen; $i++)
	{
		if ($rsFS["FS_ANZEIGEN"][$i]==1)
		{
			$anzahlfs=$anzahlfs + 1;
		}
	}
	
	print "<p>Ich habe ".$rsFSZeilen." Zeilen gefunden!</p>";
	print "<p>Davon sind ".$anzahlfs." Zeilen anzuzeigen!</p>";
	

	//**********************************
	//* Bild erzeugen
	//**********************************
	
	$breite = 300;
	$hoehe = 250;
	$beschriftungfontgroesse = 3;
	$beschriftunghoehe = imagefontheight($beschriftungfontgroesse);
	$ueberschriftfontgroesse = 5;
	$ueberschrifthoehe = imagefontheight($ueberschriftfontgroesse);
	$balkenbreite = 30;
	$linkerrand = 0;
	
	$img = imagecreate($breite, $hoehe);
	$vcolor = imagecolorallocate($img, 192, 192, 192);	//Grau
	$bcolor = imagecolorallocate($img, 0, 0, 0);	//Schwarz
	$freicol = imagecolorallocate($img, 0, 255, 0);	//Gr�n
	$vollcol = imagecolorallocate($img, 255, 0, 0);	//Rot
	imagerectangle($img, 0, 0, $breite - 1, $hoehe - 1, $bcolor);
	imagerectangle($img, 2, 2, $breite - 3, $hoehe - 3, $bcolor);

	// �berschrift
	$lage = ($breite - imagefontwidth($ueberschriftfontgroesse) * strlen($rname)) / 2;
	imagestring($img, $ueberschriftfontgroesse, $lage, $ueberschrifthoehe, $rname, $bcolor);


	//**********************************
	//* Angaben weiter auswerten
	//**********************************
	
	while(list(,$lines)=each($output))
	{
		print ($lines."<br>\n");
		$zeile = ereg_replace("[ ]{2,}", " ", $lines);
		$felder = explode(" ", $zeile);
		

		//**********************************
		//* Daten in DB buchen
		//**********************************
	
		//Abgleich DB - DF
		$ds = array_search($felder[0], $rsFS["FS_FILESYSTEM"]);

		if (($rsFS["FS_UEBERWACHEN"][$ds]==1) and ($felder[2] != "Used"))
		{
			$SQL = "Insert into Filesystemhistorie values (".$rsFS["FS_ID"][$ds].", ".$felder[1].", ".$felder[3].", sysdate)";
			print "<br>".$SQL."<br>";
			awisExecute($admcon, $SQL);
		}



		if($ds > -1)
		{
			print "In ".$ds." gefunden!<br>";
			if($rsFS["FS_ANZEIGEN"][$ds] == 0)
			{
				continue;
			}
			print_r ($rsFS["FS_ANZEIGEN"][$ds]);
		}
		else
		{
			if($felder[2] = "Used")
			{
				print "�berschriften!<br><br>";
				continue;
			}
			print "Filesystem fehlt!<br>";
		}
		
//		while(list(,$feld)=each($felder))
//		{
//				print ($feld."<br>\n");
//		}



		//**********************************
		//* Bild weitermalen
		//**********************************
	

		// Seitlichen Versatz berechnen
		if($linkerrand == 0)
		{
			$linkerrand = 25;
		}
		else
		{
			$linkerrand = $linkerrand + 150;
		}


		// ermitteln der Grafikmitte
		$beschriftungbreite1 = imagefontwidth($beschriftungfontgroesse) * strlen($felder[5]);
		$beschriftungbreite2 = imagefontwidth($beschriftungfontgroesse) * strlen($felder[0]);
		$mitte = abs($beschriftungbreite1 - $beschriftungbreite2) / 2 + $linkerrand;

		// Mountpoint
		$text = $felder[5];
		imagestring($img, $beschriftungfontgroesse, $mitte - $beschriftungbreite1 / 2, $hoehe - 2 * $beschriftunghoehe, $text, $bcolor);

		// Filesystem
		$text = $felder[0];
		imagestring($img, $beschriftungfontgroesse, $mitte - $beschriftungbreite2 / 2, $hoehe - 4 * $beschriftunghoehe, $text, $bcolor);

		// Balkengrafik
		$geshoehe = $hoehe - 5 * $beschriftunghoehe - 3 * $ueberschrifthoehe;
		$hoehefrei = $geshoehe * $felder[3] / $felder[1];
		$hoehevoll = $geshoehe * $felder[2] / $felder[1];
		$unten = $hoehe - 5 * $beschriftunghoehe;
		imagerectangle($img, $mitte - $balkenbreite / 2, $unten, $mitte + $balkenbreite / 2, $unten - $hoehevoll, $vollcol);
		if($hoehevoll > 1)
		{
			imagefill($img, $mitte, $unten - 1, $vollcol);
		}
		imagerectangle($img, $mitte - $balkenbreite / 2, $unten - $hoehevoll, $mitte + $balkenbreite / 2, $unten - $geshoehe, $freicol);
		imagefill($img, $mitte, $geshoehe + 1, $freicol);


		// Prozentangabe
		$text = $felder[4];
		if($hoehefrei * 1.5 > $beschriftunghoehe)
		{
			// In das Freifeld schreiben
			imagestring($img, $beschriftungfontgroesse, $mitte - imagefontwidth($beschriftungfontgroesse) * strlen($text) / 2, 45, $text, $bcolor);
		
		}
		else
		{
			// Neben das Freifeld schreiben
			imagestring($img, $beschriftungfontgroesse, $mitte + $balkenbreite / 2 + 5, 45, $text, $bcolor);
		}

	}	//Bild fertig!

	imagepng($img, "test.png");
	imagedestroy($img);
	
	print "Fehler: ";
	print_r($fehler);

	print "<hr>";
	
	awisLogoff($admcon);
	
} // If($admcon)	
?>

<img src="test.png" width="300" height="250" alt="Nix is mit test.png">
</BODY>
</HTML>
