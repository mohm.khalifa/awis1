<?php

class PartnerBestellungV3
{

    /**
     * @var int $Kundennummer
     */
    protected $Kundennummer = null;

    /**
     * @var int $Bestellnummer
     */
    protected $Bestellnummer = null;

    /**
     * @var \DateTime $Bestelldatum
     */
    protected $Bestelldatum = null;

    /**
     * @var string $BestellReferenz
     */
    protected $BestellReferenz = null;

    /**
     * @var Versandart $Versand
     */
    protected $Versand = null;

    /**
     * @var ArrayOfBestellPosition $Positionen
     */
    protected $Positionen = null;

    /**
     * @var OrderLieferadresse $LieferAdresse
     */
    protected $LieferAdresse = null;

    /**
     * @param int $Kundennummer
     * @param int $Bestellnummer
     * @param \DateTime $Bestelldatum
     * @param Versandart $Versand
     */
    public function __construct($Kundennummer, $Bestellnummer, \DateTime $Bestelldatum, $Versand)
    {
      $this->Kundennummer = $Kundennummer;
      $this->Bestellnummer = $Bestellnummer;
      $this->Bestelldatum = $Bestelldatum->format(\DateTime::ATOM);
      $this->Versand = $Versand;
    }

    /**
     * @return int
     */
    public function getKundennummer()
    {
      return $this->Kundennummer;
    }

    /**
     * @param int $Kundennummer
     * @return PartnerBestellungV3
     */
    public function setKundennummer($Kundennummer)
    {
      $this->Kundennummer = $Kundennummer;
      return $this;
    }

    /**
     * @return int
     */
    public function getBestellnummer()
    {
      return $this->Bestellnummer;
    }

    /**
     * @param int $Bestellnummer
     * @return PartnerBestellungV3
     */
    public function setBestellnummer($Bestellnummer)
    {
      $this->Bestellnummer = $Bestellnummer;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBestelldatum()
    {
      if ($this->Bestelldatum == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Bestelldatum);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Bestelldatum
     * @return PartnerBestellungV3
     */
    public function setBestelldatum(\DateTime $Bestelldatum)
    {
      $this->Bestelldatum = $Bestelldatum->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getBestellReferenz()
    {
      return $this->BestellReferenz;
    }

    /**
     * @param string $BestellReferenz
     * @return PartnerBestellungV3
     */
    public function setBestellReferenz($BestellReferenz)
    {
      $this->BestellReferenz = $BestellReferenz;
      return $this;
    }

    /**
     * @return Versandart
     */
    public function getVersand()
    {
      return $this->Versand;
    }

    /**
     * @param Versandart $Versand
     * @return PartnerBestellungV3
     */
    public function setVersand($Versand)
    {
      $this->Versand = $Versand;
      return $this;
    }

    /**
     * @return ArrayOfBestellPosition
     */
    public function getPositionen()
    {
      return $this->Positionen;
    }

    /**
     * @param ArrayOfBestellPosition $Positionen
     * @return PartnerBestellungV3
     */
    public function setPositionen($Positionen)
    {
      $this->Positionen = $Positionen;
      return $this;
    }

    /**
     * @return OrderLieferadresse
     */
    public function getLieferAdresse()
    {
      return $this->LieferAdresse;
    }

    /**
     * @param OrderLieferadresse $LieferAdresse
     * @return PartnerBestellungV3
     */
    public function setLieferAdresse($LieferAdresse)
    {
      $this->LieferAdresse = $LieferAdresse;
      return $this;
    }

}
