<?php
/**
 * artikelstamm_speichern.php
 *
 * Speichert die Daten im Modul artikelstamm
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   20080419
 * @todo      -Keine-
 *
 * �nderungen
 * Wer        Wann        Was
 * ---------------------------------------------------------------
 */
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    $TextKonserven = array();
    $TextKonserven[] = array('Fehler', 'err_KeinWert');
    $TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
    $TextKonserven[] = array('Fehler', 'err_EingabeWiederholen');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zuordnen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'geaendert_von');
    $TextKonserven[] = array('Wort', 'geaendert_auf');
    $TextKonserven[] = array('Wort', 'DoppelterDatensatz');
    $TextKonserven[] = array('Meldung', 'DSVeraendert');
    $TextKonserven[] = array('Meldung', 'ATUNR_Mind_Laenge');
    $TextKonserven[] = array('Meldung', 'DatensatzVorhanden');
    $TextKonserven[] = array('AST', 'AST%');
    $TextKonserven[] = array('LAR', 'wrd_DoppelteArtikel');
    $TextKonserven[] = array('OEN', 'wrd_DoppelteArtikel');
    $TextKonserven[] = array('OEN', 'OEN_%');
    $TextKonserven[] = array('HER', 'HER_BEZEICHNUNG');
    $TextKonserven[] = array('LAR', 'wrd_ZugeordnetZu');
    $TextKonserven[] = array('LAR', 'wrd_FrageZuordnenAnlegen');
    $TextKonserven[] = array('OEN', 'wrd_FrageZuordnenAnlegen');
    $TextKonserven[] = array('LAR', 'wrd_ArtikelNeuAnlegen');
    $TextKonserven[] = array('LAR', 'LAR_%');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $TXT_Speichern = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $Form->DebugAusgabe(1, $_POST);

    $ATUNR = strtoupper((isset($_POST['txtAST_ATUNR'])?$_POST['txtAST_ATUNR']:''));

    if (isset($_POST['txtAST_ATUNR'])) {
        $_POST['txtAST_ATUNR'] = strtoupper($_POST['txtAST_ATUNR']);
    }

    if (isset($_POST['txtAST_KEY'])) {
        $AWIS_KEY1 = $_POST['txtAST_KEY'];

        if (intval($_POST['txtAST_KEY']) === 0)        // Neuer Artikel
        {
            $Fehler = '';
            $ATUNR = strtoupper($_POST['txtAST_ATUNR_ZEICHEN'] . substr($_POST['txtAST_ATUNR'], 0, 7));        // Maximal 7 Zeichen verwenden
            switch ($_POST['txtAST_ATUNR_ZEICHEN']) {
                case '@':
                    if (strlen($ATUNR) < 5) {
                        $Fehler .= $TXT_Speichern['Meldung']['ATUNR_Mind_Laenge'];
                    }

                    // Pr�fen, ob es den Datensatz schon gibt
                    $BindeVariablen = array();
                    $BindeVariablen['var_T_ast_atunr'] = $ATUNR;

                    $SQL = 'SELECT AST_KEY FROM Artikelstamm WHERE AST_ATUNR = :var_T_ast_atunr';
                    $rsDaten = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                    $awisRSZeilen = $rsDaten->AnzahlDatensaetze();
                    $rsDaten = $rsDaten->Tabelle();

                    if ($awisRSZeilen > 0) {
                        $Fehler .= $TXT_Speichern['Meldung']['DatensatzVorhanden'] . '<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Key=' . $rsDaten['AST_KEY'][0] . '>' . $ATUNR . '</a>';
                    }
                    break;
                case '�';
                    $Kennung = $AWISBenutzer->ParameterLesen('DefaultKennungAutoArtikelnr', true);
                    $SQL = 'SELECT * FROM (SELECT AST_ATUNR FROM ARTIKELSTAMM WHERE SUBSTR(AST_ATUNR,1,' . strlen($Kennung) . ')=\'' . $Kennung . '\' ORDER BY nlssort(ast_atunr,\'NLS_SORT=BINARY\') DESC) WHERE ROWNUM = 1';

                    $rsKey = $DB->RecordSetOeffnen($SQL);
                    $rsKey = $rsKey->Tabelle();
                    if (!isset($rsKey['AST_ATUNR'][0]) OR (isset($rsKey['AST_ATUNR'][0]) AND $rsKey['AST_ATUNR'][0] == '')) {
                        $ATUNR = $Kennung . '0001';
                    } else {
                        $ATUNR = ++$rsKey['AST_ATUNR'][0];
                    }
                    echo $ATUNR;
                    break;
                default:
                    // Es k�nnen auch normale ATU Nummern gespeichert werden
                    $ATUNR = strtoupper(substr($_POST['txtAST_ATUNR'], 0, 8));
            }

            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = $ATUNR;

            $SQL = 'SELECT *';
            $SQL .= ' FROM Artikelstamm WHERE AST_ATUNR = :var_T_ast_atunr';
            $rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
            $awisRSZeilen = $rsAST->AnzahlDatensaetze();
            $rsAST = $rsAST->Tabelle();

            if ($awisRSZeilen > 0) {
                $Form->Formular_Start();
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($TXT_Speichern['Wort']['DoppelterDatensatz'], 0);
                $Form->ZeileEnde();
                $Form->Formular_Ende();
            }

            // Daten auf Vollst�ndigkeit pr�fen
            $Pflichtfelder = array();
            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['AST'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $WUG_KEY = $AWISBenutzer->ParameterLesen('DefaultWUG_Key_NeueArtikel');

            $SQL = 'INSERT INTO Artikelstamm';
            $SQL .= '(AST_ATUNR, AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG';
            $SQL .= ',AST_WUG_KEY, AST_IMQ_ID, AST_USER, AST_USERDAT';
            $SQL .= ')VALUES (';
            $SQL .= ' ' . $DB->FeldInhaltFormat('TU', $ATUNR, false);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtAST_BEZEICHNUNG'], false);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtAST_KENNUNGVORSCHLAG'], false);
            $SQL .= ',' . $DB->FeldInhaltFormat('N0', $WUG_KEY);
            $SQL .= ',4';
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            $Form->DebugAusgabe(1, $SQL);
            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('ast_speichern', 9010151819, $SQL, 2);
            }
            $SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
            $rsKey = $DB->RecordSetOeffnen($SQL);
            $AWIS_KEY1 = $rsKey->FeldInhalt('KEY');
            $AWISBenutzer->ParameterSchreiben("AktuellerAST", $rsKey->FeldInhalt('KEY'));
        } else                    // ge�nderter Artikel
        {
            $Felder = explode(';', $Form->NameInArray($_POST, 'txtAST_', 1, 1));
            $FehlerListe = array();
            $UpdateFelder = '';

            $AWISBenutzer->ParameterSchreiben("AktuellerAST", $_POST['txtAST_KEY']);
            $BindeVariablen = array();
            $BindeVariablen['var_N0_ast_key'] = $_POST['txtAST_KEY'];

            $rsAST = $DB->RecordSetOeffnen('SELECT * FROM Artikelstamm WHERE ast_key=:var_N0_ast_key', $BindeVariablen);
            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $rsAST->FeldInhalt($FeldName), true);
                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsAST->FeldInhalt('AST_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
                die();
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE Artikelstamm SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', AST_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', AST_userdat=sysdate';
                $SQL .= ' WHERE AST_key=0' . $_POST['txtAST_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException($Form->LadeTextBaustein('FEHLER', 'SpeicherFehler'), 200901151754, $SQL, 2);
                }
            }

            $AWIS_KEY1 = $_POST['txtAST_KEY'];
        }
    }

    //***********************************************************************************
    //** Lieferantenartikel speichern und in der Teileinfos zuordnen
    //***********************************************************************************
    if ((isset($_POST['txtLAR_KEY']) or isset($_POST['ico_add_Lart_x'])) and (@$_POST['txtLAR_LARTNR'] != '' or isset($_POST['txtLAR_KEY'])) and $_POST['txtLAR_LIE_NR'] != '') {
        if (intval($_POST['txtLAR_KEY']) === 0)        // Neuer Artikel
        {
            // daten auf Vollst�ndigkeit pr�fen
            $_POST['txtLAR_APP_ID'] = (@$_POST['txtLAR_APP_ID'] < 0?0:@$_POST['txtLAR_APP_ID']);

            $Fehler = '';
            $Pflichtfelder = array('LAR_LARTNR', 'LAR_LIE_NR');
            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LAR'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $LARSpeichern = true;        // Datensatz speichern
            $LARZuordnen = true;        // LAR der zuordnen

            if (isset($_POST['bestaetigung'])) {
                $LARSpeichern = isset($_POST['cmdSpeichern_x']);
                $Zuordnungen = $Form->NameInArray($_POST, 'cmdZuordnen_');
                $LARZuordnen = ($Zuordnungen != '');        // Soll ein LAR zugeordnet werden?

                $AWIS_KEY2 = 0;
                if ($LARZuordnen) {
                    $Key = explode('_', $Zuordnungen);
                    $AWIS_KEY2 = intval($Key[1]);
                }
            } else {
                // Zuerst nach �hnlichen Artikeln suchen und dem Benutzer anzeigen
                // Es muss entschieden werden, ob einer Verkn�pfung oder eine Neuanlage machen m�chte
                $BindeVariablen = array();
                $BindeVariablen['var_T_lar_lartnr'] = strtoupper($_POST['txtLAR_LARTNR']);
                $BindeVariablen['var_T_lar_lie_nr'] = $_POST['txtLAR_LIE_NR'];

                $SQL = 'SELECT LAR_KEY, LAR_BEMERKUNGEN, LAR_LARTNR, LAR_USER, LAR_USERDAT,TEI_ITY_ID1,TEI_KEY1,TEI_WERT1,TEI_USER, TEI_USERDAT ';
                $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 1 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=TEI_KEY1 AND ROWNUM=1) AS ALI_WERT_1';    // Bezeichnung
                $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 2 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=TEI_KEY1 AND ROWNUM=1) AS ALI_WERT_2';    // Reklamationskennung
                $SQL .= ' FROM Lieferantenartikel';
                $SQL .= ' LEFT OUTER JOIN teileinfos ON LAR_KEY = TEI_KEY2';
                $SQL .= ' WHERE LAR_SUCH_LARTNR=asciiwort(:var_T_lar_lartnr)';
                $SQL .= ' AND LAR_LIE_NR=:var_T_lar_lie_nr';
                $rsLAR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

                $rsLARZeilen = $rsLAR->AnzahlDatensaetze();
                $rsLAR = $rsLAR->Tabelle();

                if ($rsLARZeilen > 0) {
                    echo '<form name=frmArtikelstamm method=post action=./artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'Block=' . $_GET['Block']:'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . '>';
                    echo '<input name=txtLAR_KEY_ZUORDNUNG type=hidden value="' . $rsLAR['LAR_KEY'][0] . '">';
                    echo '<input name=txtAST_KEY type=hidden value="' . $_POST['txtAST_KEY'] . '">';
                    echo '<input name=txtAST_ATUNR type=hidden value="' . $ATUNR . '">';

                    // Alle Teileinfofelder speichern
                    $InfoFelder = $Form->NameInArray($_POST, 'txtTII_', 1, 0);
                    if ($InfoFelder != '') {
                        $InfoFelder = explode(',', $InfoFelder);
                        foreach ($InfoFelder AS $InfoFeld) {
                            echo '<input type="hidden" name="' . $InfoFeld . '" value="' . $_POST[$InfoFeld] . '"';
                            echo '<input type="hidden" name="' . str_replace('txt', 'old', $InfoFeld) . '" value="' . $_POST[str_replace('txt', 'old', $InfoFeld)] . '"';
                        }
                    }
                    $Form->Formular_Start();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['wrd_DoppelteArtikel'], 0, 'Hinweis');
                    $Form->ZeileEnde();
                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel(str_replace('$1', $ATUNR, $TXT_Speichern['LAR']['wrd_FrageZuordnenAnlegen']), 0, '');
                    $Form->ZeileEnde();

                    $LetzterLAR_KEY = 0;
                    $SchonVorhanden = false;        // Ist der EXAKTE LartNr schon vorhanden?->Kann kann nicht mehr gespeichert werden
                    for ($LARZeile = 0; $LARZeile < $rsLARZeilen; $LARZeile++) {
                        if ($LetzterLAR_KEY != $rsLAR['LAR_KEY'][$LARZeile]) {
                            $LetzterLAR_KEY = $rsLAR['LAR_KEY'][$LARZeile];

                            $Form->ZeileStart();
                            $Form->Schaltflaeche('image', 'cmdZuordnen_' . $rsLAR['LAR_KEY'][$LARZeile], '', '/bilder/cmd_korb_runter.png', $TXT_Speichern['Wort']['lbl_zuordnen']);
                            $Form->ZeileEnde();

                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_LARTNR'] . ':', 150);
                            $Form->Erstelle_TextFeld('*LAR', $rsLAR['LAR_LARTNR'][$LARZeile], 1, 200, false);
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_BEZWW'] . ':', 150);
                            $Form->Erstelle_TextFeld('*LAR', $rsLAR['ALI_WERT_1'][$LARZeile], 1, 200, false);
                            $Form->ZeileEnde();
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_BEMERKUNGEN'] . ':', 150);
                            $Form->Erstelle_Textarea('*LAR', $rsLAR['LAR_BEMERKUNGEN'][$LARZeile], 200, 80, 3, false);
                            $Form->ZeileEnde();
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_USER'] . ':', 150);
                            $Form->Erstelle_Textarea('*LAR', $rsLAR['TEI_USER'][$LARZeile], 200, 80, 3, false);
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_USERDAT'] . ':', 150);
                            $Form->Erstelle_Textarea('*LAR', $rsLAR['TEI_USERDAT'][$LARZeile], 200, 80, 3, false);
                            $Form->ZeileEnde();

                            if (strcasecmp($_POST['txtLAR_LARTNR'], $rsLAR['LAR_LARTNR'][$LARZeile]) == 0) {
                                $SchonVorhanden = true;
                            }
                        }

                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['wrd_ZugeordnetZu'] . ':', 0, '');
                        $Form->ZeileEnde();

                        $Form->ZeileStart();
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_ITY_ID1'][$LARZeile], 1, 50, false);
                        switch ($rsLAR['TEI_ITY_ID1'][$LARZeile]) {
                            case 'AST':
                                $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&AST_KEY=0' . $rsLAR['TEI_KEY1'][$LARZeile];
                                break;
                            default:
                                $Link = '';
                                break;
                        }
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_WERT1'][$LARZeile], 1, 200, false, '', '', $Link);
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_USER'][$LARZeile], 1, 200, false, '', '', '');
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_USERDAT'][$LARZeile], 1, 200, false, '', '', '', 'D');
                        $Form->ZeileEnde();
                    }

                    $Form->Trennzeile('D');
                    if (!$SchonVorhanden)            // Artikel kann auch gespeichert werden
                    {
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel(str_replace('$1', $_POST['txtLAR_LARTNR'], $TXT_Speichern['LAR']['wrd_ArtikelNeuAnlegen']), 0);
                        $Form->ZeileEnde();
                    }
                    $Form->Formular_Ende();

                    $Form->SchaltflaechenStart();
                    if (!$SchonVorhanden) {
                        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $TXT_Speichern['Wort']['lbl_speichern'], 'S');
                    }
                    $Form->Schaltflaeche('image', 'cmdZurueck', '', '/bilder/cmd_zurueck.png', $TXT_Speichern['Wort']['lbl_zurueck'], 'O');
                    $Form->SchaltflaechenEnde();

                    // Alle Felder sichern
                    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLAR_', 1, 1));
                    foreach ($Felder AS $Feld) {
                        echo '<input name=' . $Feld . ' type=hidden value="' . $_POST[$Feld] . '">';
                    }

                    echo '<input name=bestaetigung value=1 type=hidden>';
                    echo '</form>';

                    die();
                }        // Gibt es den Artikel schon in �hlicher Form?
            }
            if ($LARSpeichern) {
                $SQL = 'INSERT INTO Lieferantenartikel';
                $SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_ALTERNATIVARTIKEL,LAR_BEMERKUNGEN,LAR_AUSLAUFARTIKEL';
                $SQL .= ',LAR_PROBLEMARTIKEL,LAR_PROBLEMBESCHREIBUNG,LAR_GEPRUEFT,LAR_APP_ID,LAR_MUSTER,LAR_BEZWW';
                $SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_REKLAMATIONSKENNUNG,LAR_SUCH_LARTNR,LAR_LARTID';
                $SQL .= ',LAR_USER, LAR_USERDAT';
                $SQL .= ')VALUES (';
                $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtLAR_LARTNR'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAR_LIE_NR'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', @$_POST['txtLAR_ALTERNATIVARTIKEL'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', @$_POST['txtLAR_BEMERKUNGEN'], true);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', @$_POST['txtLAR_AUSLAUFARTIKEL'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', @$_POST['txtLAR_PROBLEMARTIKEL'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', @$_POST['txtLAR_PROBLEMBESCHREIBUNG'], true);
                if (isset($_POST['txtLAR_GEPRUEFT']) and $_POST['txtLAR_GEPRUEFT'] != '') {
                    $SQL .= ',' . $DB->FeldInhaltFormat('D', $_POST['txtLAR_GEPRUEFT'], false);
                } else {
                    $SQL .= ',' . $DB->FeldInhaltFormat('T', '', false);
                }
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', @$_POST['txtLAR_APP_ID'], true);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', @$_POST['txtLAR_MUSTER'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', @$_POST['txtLAR_BEZWW'], false);
                $SQL .= ',0';
                $SQL .= ',4';        // Importquelle
                $SQL .= ',' . $DB->FeldInhaltFormat('T', @$_POST['txtLAR_REKLAMATIONSKENNUNG'], false);
                $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $_POST['txtLAR_LARTNR'], false) . ')';
                $SQL .= ',' . $DB->FeldInhaltFormat('T', @$_POST['txtLAR_LARTID'], false) . '';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151818, $SQL, 2);
                }
                $SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $AWIS_KEY2 = $rsKey->FeldInhalt('KEY');
            }

            if ($LARZuordnen AND $AWIS_KEY2 > 0) {
                $SQL = 'INSERT INTO TeileInfos';
                $SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
                $SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
                $SQL .= 'VALUES(';
                $SQL .= ' \'AST\'';
                $SQL .= ',' . $AWIS_KEY1;
                $SQL .= ',suchwort(\'' . $ATUNR . '\')';
                $SQL .= ',\'' . $ATUNR . '\'';
                $SQL .= ',\'LAR\'';
                $SQL .= ',' . $AWIS_KEY2;
                $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $_POST['txtLAR_LARTNR']) . ')';
                $SQL .= ',\'' . $_POST['txtLAR_LARTNR'] . '\'';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151801, $SQL, 2);
                }
                $SQL = 'SELECT seq_TEI_Key.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $TEIKEY = $rsKey->FeldInhalt('KEY');

                // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Artikel
                // nicht durch den Import gel�scht werden kann
                // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                $SQL = 'UPDATE Artikelstamm SET';
                $SQL .= ' AST_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
                $SQL .= ', AST_userdat=sysdate';
                $SQL .= ' WHERE AST_key=0' . $AWIS_KEY1 . ' AND BITAND(AST_IMQ_ID,4)<>4';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151802, $SQL, 2);
                }

                // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Lieferantenrtikel
                // nicht durch den Import gel�scht werden kann
                // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                if ($LARSpeichern AND ($rsKey->FeldInhalt('KEY') != '' OR $AWIS_KEY2 != $rsKey->FeldInhalt('KEY'))) {
                    $SQL = 'UPDATE Lieferantenartikel SET';
                    $SQL .= ' LAR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ', LAR_IMQ_ID = CASE WHEN BITAND(LAR_IMQ_ID,4)=4 THEN LAR_IMQ_ID ELSE LAR_IMQ_ID+4 END';
                    $SQL .= ', LAR_userdat=sysdate';
                    $SQL .= ' WHERE LAR_key=0' . $AWIS_KEY2 . ' AND BITAND(LAR_IMQ_ID,4)<>4';
                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901151803, $SQL, 2);
                    }
                }
            }

            $AWIS_KEY2 = 0;    // Wieder l�schen, damit in der Oberfl�che eine Listendarstellung erreicht werden kann

        } else        // zu�ndernder Artikel
        {
            $Felder = explode(';', $Form->NameInArray($_POST, 'txtLAR_', 1, 1));
            $FehlerListe = array();
            $UpdateFelder = '';
            $FeldListe = '';

            $BindeVariablen = array();
            $BindeVariablen['var_N0_lar_key'] = $_POST['txtLAR_KEY'];

            $SQL = 'SELECT * FROM Lieferantenartikel WHERE lar_key=:var_N0_lar_key';
            $rsLAR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
            $awisRSZeilen = $rsLAR->AnzahlDatensaetze();
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsLAR->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsLAR->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsLAR->FeldInfo($FeldName, 'TypKZ'), $rsLAR->FeldInhalt($FeldName), true);
                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsLAR->FeldInhalt('LAR_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
                die();
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE Lieferantenartikel SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', LAR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', LAR_userdat=sysdate';
                $SQL .= ' WHERE LAR_key=0' . $_POST['txtLAR_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151804, $SQL, 2);
                }
            }
            $AWIS_KEY2 = 0;
        }
    }

    //***********************************************************************************
    //** WGR Vorschlag speichern --> Artikelstamminfo
    //***********************************************************************************

    if(isset($_POST['txtWGR_ID_Vorschlag'])){

        $SQL  ='merge into ARTIKELSTAMMINFOS DEST using';
        $SQL .=' (select ' . $DB->WertSetzen('ASI','T',$_POST['txtAST_ATUNR']);
        $SQL .='  as AST_ATUNR, ';
        $SQL .=' 400 as ASI_AIT_ID';
        $SQL .= ' from DUAL';
        $SQL .=' ) SRC on (DEST.ASI_AST_ATUNR = SRC.AST_ATUNR and DEST.ASI_AIT_ID = SRC.ASI_AIT_ID)';
        $SQL .=' when matched then';
        $SQL .='   update';
        $SQL .='   set ASI_WERT  = ' .$DB->WertSetzen('ASI','T',$_POST['txtWGR_ID_Vorschlag']);
        $SQL .='     , ASI_USER    = ' .$DB->WertSetzen('ASI','T',$AWISBenutzer->BenutzerName());
        $SQL .='     , ASI_USERDAT = sysdate when not matched then';
        $SQL .='   insert';
        $SQL .='     (';
        $SQL .='       ASI_AST_ATUNR,';
        $SQL .='       ASI_AIT_ID,';
        $SQL .='       ASI_WERT,';
        $SQL .='       ASI_IMQ_ID,';
        $SQL .='       ASI_USER,';
        $SQL .='       ASI_USERDAT';
        $SQL .='     )';
        $SQL .='     values';
        $SQL .='     (';
        $SQL .='       SRC.AST_ATUNR,';
        $SQL .='       SRC.ASI_AIT_ID,';
        $SQL .= $DB->WertSetzen('ASI','T',$_POST['txtWGR_ID_Vorschlag']);
        $SQL .=',       4,';
        $SQL .= $DB->WertSetzen('ASI','T',$AWISBenutzer->BenutzerName());
        $SQL .=',       sysdate';
        $SQL .='     )';

        $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ASI'));

    }


    //***********************************************************************************
    //** WUG Vorschlag speichern --> Artikelstamminfo
    //***********************************************************************************

    if(isset($_POST['txtWUG_ID_Vorschlag'])){

        $SQL  ='merge into ARTIKELSTAMMINFOS DEST using';
        $SQL .=' (select ' . $DB->WertSetzen('ASI','T',$_POST['txtAST_ATUNR']);
        $SQL .='  as AST_ATUNR, ';
        $SQL .=' 401 as ASI_AIT_ID';
        $SQL .= ' from DUAL';
        $SQL .=' ) SRC on (DEST.ASI_AST_ATUNR = SRC.AST_ATUNR and DEST.ASI_AIT_ID = SRC.ASI_AIT_ID)';
        $SQL .=' when matched then';
        $SQL .='   update';
        $SQL .='   set ASI_WERT  = ' .$DB->WertSetzen('ASI','T',$_POST['txtWUG_ID_Vorschlag']);
        $SQL .='     , ASI_USER    = ' .$DB->WertSetzen('ASI','T',$AWISBenutzer->BenutzerName());
        $SQL .='     , ASI_USERDAT = sysdate when not matched then';
        $SQL .='   insert';
        $SQL .='     (';
        $SQL .='       ASI_AST_ATUNR,';
        $SQL .='       ASI_AIT_ID,';
        $SQL .='       ASI_WERT,';
        $SQL .='       ASI_IMQ_ID,';
        $SQL .='       ASI_USER,';
        $SQL .='       ASI_USERDAT';
        $SQL .='     )';
        $SQL .='     values';
        $SQL .='     (';
        $SQL .='       SRC.AST_ATUNR,';
        $SQL .='       SRC.ASI_AIT_ID,';
        $SQL .= $DB->WertSetzen('ASI','T',$_POST['txtWUG_ID_Vorschlag']);
        $SQL .=',       4,';
        $SQL .= $DB->WertSetzen('ASI','T',$AWISBenutzer->BenutzerName());
        $SQL .=',       sysdate';
        $SQL .='     )';

        $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ASI'));

    }


    //***********************************************************************************
    //** Alternativartikel (als Komma-getrennte Liste)
    //***********************************************************************************

    if (isset($_POST['txtALTERNATIVARTIKEL'])) {
        $SQL = '';
        if (strcasecmp($_POST['txtALTERNATIVARTIKEL'], $_POST['oldALTERNATIVARTIKEL']) != 0) {
            $SQL = 'DELETE FROM Artikelstamminfos';
            $SQL .= ' WHERE ASI_AIT_ID = 120';
            $SQL .= ' AND ASI_AST_ATUNR = ' . $DB->FeldInhaltFormat('T', $ATUNR, true) . ' ';
            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 901151819, $SQL, 2);
            }

            $Werte = explode(',', $_POST['txtALTERNATIVARTIKEL']);
            foreach ($Werte AS $Wert) {
                $SQL = 'INSERT INTO Artikelstamminfos(ASI_WERT, ASI_AST_ATUNR,ASI_IMQ_ID, ASI_AIT_ID, ASI_USER, ASI_USERDAT)VALUES(';
                $SQL .= ' ' . $DB->FeldInhaltFormat('T', trim($Wert), true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $ATUNR, true);
                $SQL .= ',4';        // Manuel erfasst
                $SQL .= ',120';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE) ';

                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151805, $SQL, 2);
                }
            }
        }
    }
    //***********************************************************************************
    //** Artikelstamm-Infos speichern ung l�schen
    //***********************************************************************************
    $Felder = explode(';', $Form->NameInArray($_POST, 'txtASI_', 1, 1));
    if (count($Felder) > 0 AND $Felder[0] != '' AND $ATUNR != '') {
        foreach ($Felder AS $Feld) {
            $FeldTeile = explode('_', $Feld);
            $SQL = '';

            if ($_POST[$Feld] == '' AND $FeldTeile[2] > 0)            // Zum l�schen
            {
                // Leere Felder werden gel�scht, um nicht zu viele sinnlose Datens�tze zu halten
                $SQL .= 'DELETE FROM Artikelstamminfos';
                $SQL .= ' WHERE ASI_KEY = 0' . $FeldTeile[2];
                $SQL .= ' AND ASI_AIT_ID = 0' . $FeldTeile[1] . '';
                $SQL .= ' AND ASI_AST_ATUNR = ' . $DB->FeldInhaltFormat('T', $ATUNR, true);
            } elseif ($FeldTeile[2] > 0)                            // Aktualisieren
            {
                // Nur schreiben, wenn sich die Felder ge�ndert haben sollten!
                if ($DB->FeldInhaltFormat('T', $_POST[$Feld], true) != $DB->FeldInhaltFormat('T', $_POST['old' . substr($Feld, 3)], true)) {
                    $SQL .= 'UPDATE Artikelstamminfos';
                    $SQL .= ' SET ASI_WERT=' . $DB->FeldInhaltFormat('T', $_POST[$Feld], true);
                    $SQL .= ', ASI_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ', ASI_USERDAT=SYSDATE';
                    $SQL .= ' WHERE ASI_KEY=0' . $FeldTeile[2];
                    $SQL .= ' AND ASI_AST_ATUNR = ' . $DB->FeldInhaltFormat('T', $ATUNR, true);
                    $SQL .= ' AND ASI_AIT_ID = 0' . $FeldTeile[1] . '';
                }
            } elseif ($_POST[$Feld] !== '' AND $FeldTeile[2] == 0)                            // Neu anlegen
            {
                $SQL .= 'INSERT INTO Artikelstamminfos(ASI_WERT, ASI_AST_ATUNR,ASI_IMQ_ID, ASI_AIT_ID, ASI_USER, ASI_USERDAT)VALUES(';
                $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST[$Feld], true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $ATUNR, true);
                $SQL .= ',4';        // Manuel erfasst
                $SQL .= ',' . $FeldTeile[1];
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE)';
            }

            if ($SQL != '') {
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151807, $SQL, 2);
                }

                // Beim �ndern einer Option muss der "Besitzer" gewechselt werden, damit der Artikel
                // nicht durch den Import gel�scht werden kann
                // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                $SQL = 'UPDATE Artikelstamm SET';
                $SQL .= ' AST_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
                $SQL .= ', AST_userdat=sysdate';
                $SQL .= ' WHERE AST_key=0' . $_POST['txtAST_KEY'] . ' AND BITAND(AST_IMQ_ID,4)<>4';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151808, $SQL, 2);
                }
            }
        }
    }

    //***********************************************************************************
    //** LieferantenartikelInfos speichern
    //***********************************************************************************
    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLAI_', 1, 1));

    if (count($Felder) > 0 AND $Felder[0] != '') {
        foreach ($Felder AS $Feld) {
            $SQL = '';
            $FeldTeile = explode('_', $Feld);

            if ($_POST[$Feld] == '' AND $FeldTeile[2] > 0)            // Zum l�schen
            {
                // Leere Felder werden gel�scht, um nicht zu viele sinnlose Datens�tze zu halten
                $SQL .= 'DELETE FROM LieferantenArtikelInfos';
                $SQL .= ' WHERE LAI_KEY = 0' . $FeldTeile[2];
                $SQL .= ' AND LAI_LIT_ID = 0' . $FeldTeile[1] . '';
                $SQL .= ' AND LAI_LAR_KEY = 0' . $_POST['txtLAR_KEY'];
            } elseif ($FeldTeile[2] > 0)                            // Aktualisieren
            {
                // Nur schreiben, wenn sich die Felder ge�ndert haben sollten!
                if ($DB->FeldInhaltFormat('T', $_POST[$Feld], true) != $DB->FeldInhaltFormat('T', $_POST['old' . substr($Feld, 3)], true)) {
                    $SQL .= 'UPDATE LieferantenArtikelInfos';
                    $SQL .= ' SET LAI_WERT=' . $DB->FeldInhaltFormat('T', $_POST[$Feld], true);
                    $SQL .= ', LAI_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ', LAI_USERDAT=SYSDATE';
                    $SQL .= ' WHERE LAI_KEY=0' . $FeldTeile[2];
                    $SQL .= ' AND LAI_LAR_KEY = 0' . $_POST['txtLAR_KEY'];
                    $SQL .= ' AND LAI_LIT_ID = 0' . $FeldTeile[1] . '';
                }
            } elseif ($_POST[$Feld] !== '0' AND $FeldTeile[2] == 0)                            // Neu anlegen
            {
                $SQL .= 'INSERT INTO LieferantenArtikelInfos(LAI_WERT, LAI_LAR_KEY, LAI_IMQ_ID, LAI_LIT_ID, LAI_USER, LAI_USERDAT)VALUES(';
                $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST[$Feld], true);
                $SQL .= ',' . $DB->FeldInhaltFormat('N', $_POST['txtLAR_KEY'], true);
                $SQL .= ',4';        // Manuel erfasst
                $SQL .= ',' . $FeldTeile[1];
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE)';
            }
            //?????????????EVENTUELL ZUORDNUNG NOCH �NDERN LAR_IMQ_ID????????????????
            if ($SQL != '') {
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 010211005, $SQL, 2);
                }
            }
        }
    }

    //***********************************************************************************
    //** Smartkatexportfeld in Teileinfos speichern
    // ** Keine L�schabfrage usw..
    //***********************************************************************************
    $SK_Export = $Form->NameInArray($_POST, 'oldLARTNRSMARTKATEXPORT_', 2, 1); //olds betrachten, da diese immer kommen. txts kommen nur, wenn ein Haken drin ist.

    //Alle Checkboxen durchgehen
    foreach ($SK_Export as $SK) {
        $Old = $_POST[$SK];
        $SK = 'txt' . substr($SK, 3);
        $TEI_KEY = explode('_', $SK)[1];

        //Wenn der TEI_KEY in der Checkbox 0 ist, kommt man aus der Neuanlage.
        if($TEI_KEY == '0' ){
            $Old = 0;

            if(isset($_POST['txtLAR_LARTNR']) and $_POST['txtLAR_LARTNR'] != ''
                and isset($_POST['txtLAR_LIE_NR']) and $_POST['txtLAR_LIE_NR'] != ''){
                $TEI_KEY = $DB->RecordSetOeffnen('select seq_tei_key.currval from dual')->FeldInhalt(1);
            }


        }
        if (isset($_POST[$SK])) { //Kam das Feld als "txt"? --> txtLARTNRSMARTKATEXPORT_{LAR_KEY}
            $Neu = $_POST[$SK];
        } else {
            $Neu = 0;//Kam nicht als POST --> Kein Haken in der Checkbox
        }

        if ($Old != $Neu) { //Ge�ndert?

            $SQL  ='merge INTO teileinfosinfos dest USING';
            $SQL .=' (SELECT tei_key';
            $SQL .=' FROM teileinfos a';
            $SQL .=' INNER JOIN';
            $SQL .='   (SELECT tei_key1,';
            $SQL .='     tei_key2';
            $SQL .='   FROM teileinfos';
            $SQL .='   WHERE tei_key   = '. $DB->WertSetzen('TEI','T',$TEI_KEY);
            $SQL .='   AND tei_ity_id1 = \'AST\'';
            $SQL .='   AND tei_ity_id2 = \'LAR\'';
            $SQL .='   ) b';
            $SQL .=' ON B.TEI_KEY1         = a.TEI_KEY1';
            $SQL .=' AND B.TEI_KEY2        = a.TEI_KEY2';
            $SQL .=' AND tei_ity_id1       = \'AST\'';
            $SQL .=' AND tei_ity_id2       = \'LAR\'';
            $SQL .=' ) src ON (src.tei_key = dest.tii_tei_key AND dest.tii_ity_key = 318)';
            $SQL .=' WHEN matched THEN';
            $SQL .='   UPDATE';
            $SQL .='   SET dest.tii_wert  = '. $DB->WertSetzen('TEI','T',$Neu);
            $SQL .=',     dest.tii_user    = '. $DB->WertSetzen('TEI','T',$AWISBenutzer->BenutzerName());
            $SQL .=',     dest.tii_userdat = sysdate WHEN NOT matched THEN';
            $SQL .='   INSERT';
            $SQL .='     (';
            $SQL .='       tii_tei_key,';
            $SQL .='       tii_wert,';
            $SQL .='       tii_user,';
            $SQL .='       tii_userdat,';
            $SQL .='       tii_ity_key';
            $SQL .='     )';
            $SQL .='     VALUES';
            $SQL .='     (';
            $SQL .='       src.tei_key';
            $SQL .=',       '. $DB->WertSetzen('TEI','T',$Neu);
            $SQL .=',       '. $DB->WertSetzen('TEI','T',$AWISBenutzer->BenutzerName());
            $SQL .=',       sysdate';
            $SQL .=',       318';
            $SQL .='     )';


            if ($SQL != '') {
                $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('TEI', true));
            }
        }
    }

    //***********************************************************************************
    //** OE-Nummern speichern und zuordnen
    //***********************************************************************************
    if (isset($_GET['Seite']) and $_GET['Seite'] == 'OENummern') {
        $HakenFelder = $Form->NameInArray($_POST, 'oldOEN_AKTNUMMER_', 2, 1);
        foreach ($HakenFelder as $HakenFeld) {
            $OEN_KEY = explode('_', $HakenFeld)[2];
            $Wert = 0;
            if (isset($_POST['txtOEN_AKTNUMMER_' . $OEN_KEY])) {
                $Wert = 1;
            }

            if ($_POST[$HakenFeld] != $Wert) {
                $SQL = 'Update OENUMMERN set OEN_AKTNUMMER = ' . $DB->WertSetzen('OEN', 'Z', $Wert);
                $SQL .= ' WHERE OEN_KEY = ' . $DB->WertSetzen('OEN', 'Z', $OEN_KEY);
                $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('OEN'));
            }
        }
    }

    if (isset($_POST['txtOEN_KEY']) and (isset($_POST['txtOEN_NUMMER']) and $_POST['txtOEN_NUMMER'] != '')) {
        if (intval($_POST['txtOEN_KEY']) === 0)        // Neuer Artikel
        {
            // daten auf Vollst�ndigkeit pr�fen
            $Fehler = '';
            $Pflichtfelder = array('OEN_NUMMER', 'OEN_HER_ID');
            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['OEN'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $OENSpeichern = true;        // Datensatz speichern
            $OENZuordnen = true;        // OEN der zuordnen

            if (isset($_POST['bestaetigung'])) {
                $OENSpeichern = isset($_POST['cmdSpeichern_x']);
                $Zuordnungen = $Form->NameInArray($_POST, 'cmdZuordnen_');
                $OENZuordnen = ($Zuordnungen != '');        // Soll ein OEN zugeordnet werden?
                $AWIS_KEY2 = 0;
                if ($OENZuordnen) {
                    $Key = explode('_', $Zuordnungen);
                    $AWIS_KEY2 = intval($Key[1]);
                }
            } else {
                $BindeVariablen = array();
                $BindeVariablen['var_T_oen_nummer'] = $_POST['txtOEN_NUMMER'];
                $BindeVariablen['var_N0_oen_her_id'] = $_POST['txtOEN_HER_ID'];

                $SQL = 'SELECT OEN_KEY, OEN_NUMMER, OEN_HER_ID, OEN_BEMERKUNG, OEN_USER, OEN_USERDAT, HER_BEZEICHNUNG, TEI_KEY1, TEI_ITY_ID1, TEI_WERT1, TEI_KEY2, TEI_USER, TEI_USERDAT';
                $SQL .= ' FROM OENummern';
                $SQL .= ' LEFT OUTER JOIN teileinfos ON OEN_KEY = TEI_KEY2';
                $SQL .= ' LEFT OUTER JOIN Hersteller ON OEN_HER_ID = HER_ID';
                $SQL .= ' WHERE OEN_SUCHNUMMER=asciiwortoe(:var_T_oen_nummer)';
                $SQL .= ' AND OEN_HER_ID=:var_N0_oen_her_id';

                $rsOEN = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                $rsOENZeilen = $rsOEN->AnzahlDatensaetze();
                $rsOEN = $rsOEN->Tabelle();

                if ($rsOENZeilen > 0) {
                    echo '<form name=frmArtikelstamm method=post action=./artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'Block=' . $_GET['Block']:'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . '>';
                    echo '<input name=txtOEN_KEY_ZUORDNUNG type=hidden value="' . $rsOEN['OEN_KEY'][0] . '">';
                    echo '<input name=txtAST_KEY type=hidden value="' . $_POST['txtAST_KEY'] . '">';
                    echo '<input name=txtAST_ATUNR type=hidden value="' . $ATUNR . '">';
                    echo '<input name=txtLiefArtKey type=hidden value="' . (isset($_POST['txtLAR_KEY'])?$_POST['txtLAR_KEY']:'') . '">';
                    echo '<input name=txtLiefArtNr type=hidden value="' . (isset($_POST['txtLAR_LARTNR'])?$_POST['txtLAR_LARTNR']:'') . '">';

                    $Form->Formular_Start();

                    // Alle Teileinfofelder speichern
                    $InfoFelder = explode(',', $Form->NameInArray($_POST, 'txtTII_', 1, 0));
                    foreach ($InfoFelder AS $InfoFeld) {
                        echo '<input type="hidden" name="' . $InfoFeld . '" value="' . $_POST[$InfoFeld] . '"';
                        echo '<input type="hidden" name="' . str_replace('txt', 'old', $InfoFeld) . '" value="' . $_POST[str_replace('txt', 'old', $InfoFeld)] . '"';
                    }
                    $Form->Trennzeile('O');
                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($TXT_Speichern['OEN']['wrd_DoppelteArtikel'], 0, 'Hinweis');
                    $Form->ZeileEnde();
                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel(str_replace('$1', $ATUNR, $TXT_Speichern['OEN']['wrd_FrageZuordnenAnlegen']), 0, '');
                    $Form->ZeileEnde();

                    $LetzterOEN_KEY = 0;
                    $SchonVorhanden = false;        // Ist der EXAKTE LartNr schon vorhanden?->Kann kann nicht mehr gespeichert werden
                    for ($OENZeile = 0; $OENZeile < $rsOENZeilen; $OENZeile++) {
                        if ($LetzterOEN_KEY != $rsOEN['OEN_KEY'][$OENZeile]) {
                            $Form->Trennzeile();
                            $LetzterOEN_KEY = $rsOEN['OEN_KEY'][$OENZeile];


                            $Form->Schaltflaeche('image', 'cmdZuordnen_' . $rsOEN['OEN_KEY'][$OENZeile], '', '/bilder/cmd_korb_runter.png', $TXT_Speichern['Wort']['lbl_zuordnen']);


                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['OEN']['OEN_NUMMER'] . ':', 150);
                            $Form->Erstelle_TextFeld('*OEN', $rsOEN['OEN_NUMMER'][$OENZeile], 1, 200, false);
                            $Form->Erstelle_TextLabel($TXT_Speichern['OEN']['OEN_HER_ID'] . ':', 150);
                            $Form->Erstelle_TextFeld('*OEN', $rsOEN['OEN_HER_ID'][$OENZeile], 1, 200, false);
                            $Form->ZeileEnde();
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['OEN']['OEN_BEMERKUNG'] . ':', 150);
                            $Form->Erstelle_Textarea('*OEN', $rsOEN['OEN_BEMERKUNG'][$OENZeile], 200, 80, 3, false);
                            $Form->ZeileEnde();
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['OEN']['OEN_USER'] . ':', 150);
                            $Form->Erstelle_Textarea('*OEN', $rsOEN['TEI_USER'][$OENZeile], 200, 80, 3, false);
                            $Form->Erstelle_TextLabel($TXT_Speichern['OEN']['OEN_USERDAT'] . ':', 150);
                            $Form->Erstelle_Textarea('*OEN', $rsOEN['TEI_USERDAT'][$OENZeile], 200, 80, 3, false);
                            $Form->ZeileEnde();

                            if (strcasecmp($_POST['txtOEN_NUMMER'], $rsOEN['OEN_NUMMER'][$OENZeile]) == 0) {
                                $SchonVorhanden = true;
                            }
                        }

                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['wrd_ZugeordnetZu'] . ':', 0, '');
                        $Form->ZeileEnde();

                        $Form->ZeileStart();
                        $Form->Erstelle_TextFeld('*OEN', $rsOEN['TEI_ITY_ID1'][$OENZeile], 1, 50, false);
                        switch ($rsOEN['TEI_ITY_ID1'][$OENZeile]) {
                            case 'AST':
                                $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&AST_KEY=0' . $rsOEN['TEI_KEY1'][$OENZeile];
                                break;
                            case 'LAR':
                                $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&AST_KEY=' . $_POST['txtAST_KEY'] . '&LAR_KEY=0' . $rsOEN['TEI_KEY1'][$OENZeile] . '&OEN_KEY=0' . $rsOEN['TEI_KEY2'][$OENZeile];
                                break;
                            default:
                                $Link = '';
                                break;
                        }
                        $Form->Erstelle_TextFeld('*OEN', $rsOEN['TEI_WERT1'][$OENZeile], 1, 200, false, '', '', $Link);
                        $Form->Erstelle_TextFeld('*OEN', $rsOEN['TEI_USER'][$OENZeile], 1, 200, false, '', '', '');
                        $Form->Erstelle_TextFeld('*OEN', $rsOEN['TEI_USERDAT'][$OENZeile], 1, 200, false, '', '', '', 'D');
                        $Form->ZeileEnde();
                    }

                    $Form->Trennzeile('D');
                    if (!$SchonVorhanden)            // Artikel kann auch gespeichert werden
                    {
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel(str_replace('$1', $_POST['txtOEN_NUMMER'], $TXT_Speichern['OEN']['wrd_ArtikelNeuAnlegen']), 0);
                        $Form->ZeileEnde();
                    }
                    $Form->Formular_Ende();

                    $Form->SchaltflaechenStart();
                    if (!$SchonVorhanden) {
                        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $TXT_Speichern['Wort']['lbl_speichern'], 'S');
                    }
                    $Form->Schaltflaeche('image', 'cmdZurueck', '', '/bilder/cmd_zurueck.png', $TXT_Speichern['Wort']['lbl_zurueck'], 'O');
                    $Form->SchaltflaechenEnde();

                    // Alle Felder sichern
                    $Felder = explode(';', $Form->NameInArray($_POST, 'txtOEN_', 1, 1));
                    foreach ($Felder AS $Feld) {
                        echo '<input name=' . $Feld . ' type=hidden value="' . $_POST[$Feld] . '">';
                    }
                    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLAR_', 1));
                    foreach ($Felder AS $Feld) {
                        if ($Feld != '') {
                            echo '<input name=' . $Feld . ' type=hidden value="' . $_POST[$Feld] . '">';
                        }
                    }

                    echo '<input name=bestaetigung value=1 type=hidden>';
                    echo '</form>';

                    die();
                }        // Gibt es den Artikel schon?
            }

            if ($OENSpeichern) {
                $SQL = 'INSERT INTO OENummern';
                $SQL .= '(OEN_NUMMER, OEN_HER_ID';
                $SQL .= ',OEN_MUSTER,OEN_BEMERKUNG,OEN_SUCHNUMMER';
                $SQL .= ',OEN_USER, OEN_USERDAT, OEN_AKTNUMMER';
                $SQL .= ')VALUES (';
                $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtOEN_NUMMER'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtOEN_HER_ID'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', @$_POST['txtOEN_MUSTER'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', @$_POST['txtOEN_BEMERKUNG'], true);
                $SQL .= ',asciiwortoe(' . $DB->FeldInhaltFormat('T', $_POST['txtOEN_NUMMER'], false) . ')';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', @$_POST['txtOEN_AKTNUMMER'], false);
                $SQL .= ')';

                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151809, $SQL, 2);
                }
                $SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $AWIS_KEY2 = $rsKey->FeldInhalt('KEY');
                $OENKEY = $AWIS_KEY2;
            }

            if ($_GET['Seite'] == 'OENummern')        // OENummern zu einer ATU Nummer
            {
                if ($OENZuordnen AND $AWIS_KEY2 > 0) {
                    $SQL = 'INSERT INTO TeileInfos';
                    $SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
                    $SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
                    $SQL .= 'VALUES(';
                    $SQL .= ' \'AST\'';
                    $SQL .= ',' . $AWIS_KEY1;
                    $SQL .= ',suchwort(\'' . $ATUNR . '\')';
                    $SQL .= ',\'' . $ATUNR . '\'';
                    $SQL .= ',\'OEN\'';
                    $SQL .= ',' . $AWIS_KEY2;
                    $SQL .= ',asciiwortoe(\'' . $_POST['txtOEN_NUMMER'] . '\')';
                    $SQL .= ',\'' . $_POST['txtOEN_NUMMER'] . '\'';
                    $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';

                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901151810, $SQL, 2);
                    }
                    $SQL = 'SELECT seq_TEI_Key.CurrVal AS KEY FROM DUAL';
                    $rsKey = $DB->RecordSetOeffnen($SQL);
                    $TEIKEY = $rsKey->FeldInhalt('KEY');

                    // Beim �ndern einer Option muss der "Besitzer" gewechselt werden, damit der Artikel
                    // nicht durch den Import gel�scht werden kann
                    // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                    $SQL = 'UPDATE Artikelstamm SET';
                    $SQL .= ' AST_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
                    $SQL .= ', AST_userdat=sysdate';
                    $SQL .= ' WHERE AST_key=0' . $_POST['txtAST_KEY'] . ' AND BITAND(AST_IMQ_ID,4)<>4';

                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901151811, $SQL, 2);
                    }
                }
                $AWIS_KEY2 = 0;        // Wieder l�schen, um eine Listendarstellung in der Oberfl�che zu erzwingen
            } elseif ($_GET['Seite'] == 'Lieferantenartikel')    // OENummer an eine Lieferantenartikelnummer
            {
                if ($OENZuordnen AND $AWIS_KEY2 > 0) {
                    if (!isset($_POST['txtLAR_LARTNR'])) {
                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_lar_key'] = $_POST['txtLAR_KEY'];

                        $SQL = "SELECT LAR_LARTNR FROM LIEFERANTENARTIKEL WHERE LAR_KEY=:var_N0_lar_key";
                        $rsLAR_LARTNR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                        $txtLAR_LARTNR = $rsLAR_LARTNR->FeldInhalt('LAR_LARTNR');
                    } else {
                        $txtLAR_LARTNR = $_POST['txtLAR_LARTNR'];
                    }

                    $SQL = 'INSERT INTO TeileInfos';
                    $SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
                    $SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
                    $SQL .= 'VALUES(';
                    $SQL .= ' \'LAR\'';
                    $SQL .= ',' . $_POST['txtLAR_KEY'];
                    $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $txtLAR_LARTNR) . ')';
                    $SQL .= ',\'' . $txtLAR_LARTNR . '\'';
                    $SQL .= ',\'OEN\'';
                    $SQL .= ',' . $AWIS_KEY2;
                    $SQL .= ',asciiwortoe(\'' . $_POST['txtOEN_NUMMER'] . '\')';
                    $SQL .= ',\'' . $_POST['txtOEN_NUMMER'] . '\'';
                    $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';

                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901151812, $SQL, 2);
                    }
                    $SQL = 'SELECT seq_TEI_Key.CurrVal AS KEY FROM DUAL';
                    $rsKey = $DB->RecordSetOeffnen($SQL);
                    $TEIKEY = $rsKey->FeldInhalt('KEY');

                    // Beim �ndern einer Option muss der "Besitzer" gewechselt werden, damit der Artikel
                    // nicht durch den Import gel�scht werden kann
                    // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                    $SQL = 'UPDATE Lieferantenartikel SET';
                    $SQL .= ' LAR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ', LAR_IMQ_ID = CASE WHEN BITAND(LAR_IMQ_ID,4)=4 THEN LAR_IMQ_ID ELSE LAR_IMQ_ID+4 END';
                    $SQL .= ', LAR_userdat=sysdate';
                    $SQL .= ' WHERE LAR_key=0' . $_POST['txtLAR_KEY'] . ' AND BITAND(LAR_IMQ_ID,4)<>4';

                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901151813, $SQL, 2);
                    }
                }
                $AWIS_KEY2 = 0;	// Wieder auf den Lieferantenartikel setzen, um Detailansicht zu erzwingen
            }
        } else        // zu�ndernder Artikel
        {
            $Felder = explode(';', $Form->NameInArray($_POST, 'txtOEN_', 1, 1));
            $FehlerListe = array();
            $UpdateFelder = '';

            $BindeVariablen = array();
            $BindeVariablen['var_N0_oen_key'] = $_POST['txtOEN_KEY'];

            $SQL = 'SELECT * FROM OENummern WHERE OEN_key=:var_N0_oen_key';
            $rsOEN = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);

                if ($FeldName != 'OEN_AKTNUMMER') {
                    if (isset($_POST['old' . $FeldName])) {
                        // Alten und neuen Wert umformatieren!!
                        $WertNeu = $DB->FeldInhaltFormat($rsOEN->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                        $WertAlt = $DB->FeldInhaltFormat($rsOEN->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                        $WertDB = $DB->FeldInhaltFormat($rsOEN->FeldInfo($FeldName, 'TypKZ'), $rsOEN->FeldInhalt($FeldName), true);
                        if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                            if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                                $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                            } else {
                                $FeldListe .= ', ' . $FeldName . '=';

                                if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                                {
                                    $FeldListe .= ' null';
                                } else {
                                    $FeldListe .= $WertNeu;
                                }
                            }
                        }
                    }
                }
            }
            $Form->DebugAusgabe(1, 'Fehlerliste-Anz: ' . count($FehlerListe));
            $Form->DebugAusgabe(1, 'FeldListe: ' . $FeldListe);
            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsOEN->FeldInhalt('OEN_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
                die();
            }
            else {
                $SQL = 'UPDATE OENummern SET ';
                $SQL .= ($FeldListe != ''?substr($FeldListe,
                        1) . ', ':'');        //Zeile mit $Feldliste (wird weiter oben zusammengebaut), inkl. ", " nur mit in den SQL, wenn Variable nicht leer ist
                $SQL .= 'OEN_user=\'' . $AWISBenutzer->BenutzerName() . '\', ';
                $SQL .= 'OEN_userdat=sysdate, ';
                $SQL .= 'OEN_AKTNUMMER = ' . (isset($_POST['txtOEN_AKTNUMMER'])?'1':'0');        //�ber tern�ren Operator und nicht �ber Feldliste gel�st, weil wenn unchecked => keine POST-Variable verf�gbar
                $SQL .= ' WHERE OEN_key=0' . $_POST['txtOEN_KEY'] . '';
                $Form->DebugAusgabe(1, $SQL);
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151814, $SQL, 2);
                }
            }
            $AWIS_KEY2 = 0;// Wieder l�schen, um eine Listendarstellung in der Oberfl�che zu erzwingen
        }
    }


    $OE_Kopier_Keys =  $Zuordnungen = $Form->NameInArray($_POST, 'txtCOPY_OE_TEI_KEY',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
    if(count($OE_Kopier_Keys)>0){

        $SQL  ='MERGE INTO teileinfos dest';
        $SQL .=' USING (';
        $SQL .='           SELECT';
        $SQL .='               *';
        $SQL .='           FROM';
        $SQL .='               teileinfos';
        $SQL .='           WHERE';
        $SQL .='               tei_key IN (';
        $SQL .='                   0';

        foreach ($OE_Kopier_Keys as $Kopier_TEI_KEY){
            $TEI_KEY = explode('txtCOPY_OE_TEI_KEY_',$Kopier_TEI_KEY)[1];
            $SQL .= ', ' . $DB->WertSetzen('TEI','N0',$TEI_KEY);
        }

        $SQL .='               )';
        $SQL .='       )';
        $SQL .=' src ON ( dest.tei_key2 = src.tei_key2';
        $SQL .='          AND dest.tei_key1 = ' . $DB->WertSetzen('TEI','N0',$AWIS_KEY1). ' )';
        $SQL .=' WHEN NOT MATCHED THEN';
        $SQL .=' INSERT (tei_ity_id1,';
        $SQL .='     tei_key1,';
        $SQL .='     tei_such1,';
        $SQL .='     tei_wert1,';
        $SQL .='     tei_ity_id2,';
        $SQL .='     tei_key2,';
        $SQL .='     tei_such2,';
        $SQL .='     tei_wert2,';
        $SQL .='     tei_user,';
        $SQL .='     tei_userdat )';
        $SQL .=' VALUES';
        $SQL .='     ( src.tei_ity_id1, ';
        $SQL .=  $DB->WertSetzen('TEI','N0',$AWIS_KEY1);
        $SQL .=',     (select ast_atunr from artikelstamm where ast_key = '. $DB->WertSetzen('TEI','N0',$AWIS_KEY1) .')';
        $SQL .=',     (select ast_atunr from artikelstamm where ast_key = '. $DB->WertSetzen('TEI','N0',$AWIS_KEY1) .')';
        $SQL .=',       src.tei_ity_id2,';
        $SQL .='       src.tei_key2,';
        $SQL .='       src.tei_such2,';
        $SQL .='       src.tei_wert2,';
        $SQL .='     \'EDV(Kopie)\',';
        $SQL .='       SYSDATE )';
        
        $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('TEI'));

    }

    //***********************************************************************************
    //** Lieferantenartikelset-Teil speichern und in der Teileinfos zuordnen
    //***********************************************************************************
    if (isset($_POST['txtLAS_KEY'])) {
        if (intval($_POST['txtLAS_KEY']) === 0)        // Neuer Artikel
        {
            // daten auf Vollst�ndigkeit pr�fen
            $_POST['txtLAS_APP_ID'] = ($_POST['txtLAS_APP_ID'] < 0?0:$_POST['txtLAS_APP_ID']);

            $Fehler = '';
            $Pflichtfelder = array('LAS_LARTNR', 'LAS_LIE_NR');
            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['LAR'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $LARSpeichern = true;        // Datensatz speichern
            $LARZuordnen = true;        // LAR der zuordnen

            if (isset($_POST['bestaetigung'])) {
                $LARSpeichern = isset($_POST['cmdSpeichern_x']);
                $Zuordnungen = $Form->NameInArray($_POST, 'cmdZuordnen_');
                $LARZuordnen = ($Zuordnungen != '');        // Soll ein LAR zugeordnet werden?
                $LAS_KEY = 0;
                if ($LARZuordnen) {
                    $Key = explode('_', $Zuordnungen);
                    $LAS_KEY = intval($Key[1]);
                }
            } else {
                // Zuerst nach �hnlichen Artikeln suchen und dem Benutzer anzeigen
                // Es muss entschieden werden, ob einer Verkn�pfung oder eine Neuanlage machen m�chte
                $BindeVariablen = array();
                $BindeVariablen['var_T_las_lartnr'] = $_POST['txtLAS_LARTNR'];
                $BindeVariablen['var_T_las_lie_nr'] = $_POST['txtLAS_LIE_NR'];

                $SQL = 'SELECT LAR_KEY, LAR_BEMERKUNGEN, LAR_LARTNR, LAR_USER, LAR_USERDAT,TEI_ITY_ID1,TEI_KEY1,TEI_WERT1,TEI_USER, TEI_USERDAT ';
                $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 1 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=TEI_KEY1 AND ROWNUM=1) AS ALI_WERT_1';    // Bezeichnung
                $SQL .= ', (SELECT ALI_WERT FROM AstLarInfos WHERE ALI_ALT_ID = 2 AND ALI_LAR_KEY = LAR_KEY AND ALI_AST_KEY=TEI_KEY1 AND ROWNUM=1) AS ALI_WERT_2';    // Reklamationskennung
                $SQL .= ' FROM Lieferantenartikel';
                $SQL .= ' LEFT OUTER JOIN teileinfos ON LAR_KEY = TEI_KEY2';
                $SQL .= ' WHERE LAR_SUCH_LARTNR=asciiwort(:var_T_las_lartnr)';
                $SQL .= ' AND LAR_LIE_NR=:var_T_las_lie_nr';
                $rsLAR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                $rsLARZeilen = $rsLAR->AnzahlDatensaetze();
                $rsLAR = $rsLAR->Tabelle();

                if ($rsLARZeilen > 0) {
                    echo '<form name=frmArtikelstamm method=post action=./artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'Block=' . $_GET['Block']:'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . '>';
                    echo '<input name=txtLAS_KEY type=hidden value="' . $_POST['txtLAS_KEY'] . '">';
                    echo '<input name=txtLAS_KEY_ZUORDNUNG type=hidden value="' . $rsLAR['LAR_KEY'][0] . '">';
                    echo '<input name=txtAST_KEY type=hidden value="' . $_POST['txtAST_KEY'] . '">';
                    echo '<input name=txtAST_ATUNR type=hidden value="' . $ATUNR . '">';
                    echo '<input name=txtParentKEY type=hidden value="' . $_POST['txtLAR_KEY'] . '">';
                    echo '<input name=txtParentNR type=hidden value="' . (isset($_POST['txtLAR_LARTNR'])?$_POST['txtLAR_LARTNR']:'') . '">';
                    $Form->Formular_Start();

                    // Alle Teileinfofelder speichern
                    $InfoFelder = explode(',', $Form->NameInArray($_POST, 'txtTII_', 1, 0));
                    foreach ($InfoFelder AS $InfoFeld) {
                        echo '<input type="text" name="' . $InfoFeld . '" value="' . $_POST[$InfoFeld] . '"';
                        echo '<input type="text" name="' . str_replace('txt', 'old', $InfoFeld) . '" value="' . $_POST[str_replace('txt', 'old', $InfoFeld)] . '"';
                    }

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['wrd_DoppelteArtikel'], 0, 'Hinweis');
                    $Form->ZeileEnde();
                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel(str_replace('$1', $ATUNR, $TXT_Speichern['LAR']['wrd_FrageZuordnenAnlegen']), 0, '');
                    $Form->ZeileEnde();

                    $LetzterLAS_KEY = 0;
                    $SchonVorhanden = false;        // Ist der EXAKTE LartNr schon vorhanden?->Kann kann nicht mehr gespeichert werden
                    for ($LARZeile = 0; $LARZeile < $rsLARZeilen; $LARZeile++) {
                        if ($LetzterLAS_KEY != $rsLAR['LAR_KEY'][$LARZeile]) {
                            $Form->Trennzeile();
                            $LetzterLAS_KEY = $rsLAR['LAR_KEY'][$LARZeile];

                            $Form->SchaltflaechenStart();
                            $Form->Schaltflaeche('image', 'cmdZuordnen_' . $rsLAR['LAR_KEY'][$LARZeile], '', '/bilder/cmd_korb_runter.png', $TXT_Speichern['Wort']['lbl_zuordnen']);
                            $Form->SchaltflaechenEnde();

                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_LARTNR'] . ':', 150);
                            $Form->Erstelle_TextFeld('*LAR', $rsLAR['LAR_LARTNR'][$LARZeile], 1, 200, false);
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_BEZWW'] . ':', 150);
                            $Form->Erstelle_TextFeld('*LAR', $rsLAR['ALI_WERT_1'][$LARZeile], 1, 200, false);
                            $Form->ZeileEnde();
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_BEMERKUNGEN'] . ':', 150);
                            $Form->Erstelle_Textarea('*LAR', $rsLAR['LAR_BEMERKUNGEN'][$LARZeile], 200, 80, 3, false);
                            $Form->ZeileEnde();
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_USER'] . ':', 150);
                            $Form->Erstelle_Textarea('*LAR', $rsLAR['TEI_USER'][$LARZeile], 200, 80, 3, false);
                            $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['LAR_USERDAT'] . ':', 150);
                            $Form->Erstelle_Textarea('*LAR', $rsLAR['TEI_USERDAT'][$LARZeile], 200, 80, 3, false);
                            $Form->ZeileEnde();

                            if (strcasecmp($_POST['txtLAS_LARTNR'], $rsLAR['LAR_LARTNR'][$LARZeile]) == 0) {
                                $SchonVorhanden = true;
                            }
                        }

                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($TXT_Speichern['LAR']['wrd_ZugeordnetZu'] . ':', 0, '');
                        $Form->ZeileEnde();

                        $Form->ZeileStart();
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_ITY_ID1'][$LARZeile], 1, 50, false);
                        switch ($rsLAR['TEI_ITY_ID1'][$LARZeile]) {
                            case 'AST':
                                $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&AST_KEY=0' . $rsLAR['TEI_KEY1'][$LARZeile];
                                break;
                            default:
                                $Link = '';
                                break;
                        }
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_WERT1'][$LARZeile], 1, 200, false, '', '', $Link);
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_USER'][$LARZeile], 1, 200, false, '', '', '');
                        $Form->Erstelle_TextFeld('*LAR', $rsLAR['TEI_USERDAT'][$LARZeile], 1, 200, false, '', '', '', 'D');
                        $Form->ZeileEnde();
                    }

                    $Form->Trennzeile('D');
                    if (!$SchonVorhanden)            // Artikel kann auch gespeichert werden
                    {
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel(str_replace('$1', $_POST['txtLAS_LARTNR'], $TXT_Speichern['LAR']['wrd_ArtikelNeuAnlegen']), 0);
                        $Form->ZeileEnde();
                    }
                    $Form->Formular_Ende();

                    $Form->SchaltflaechenStart();
                    if (!$SchonVorhanden) {
                        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $TXT_Speichern['Wort']['lbl_speichern'], 'S');
                    }
                    $Form->Schaltflaeche('image', 'cmdZurueck', '', '/bilder/cmd_zurueck.png', $TXT_Speichern['Wort']['lbl_zurueck'], 'O');
                    $Form->SchaltflaechenEnde();

                    // Alle Felder sichern
                    $Felder = explode(';', $Form->NameInArray($_POST, 'txtLAS_', 1, 1));
                    foreach ($Felder AS $Feld) {
                        echo '<input name=' . $Feld . ' type=hidden value="' . $_POST[$Feld] . '">';
                    }

                    echo '<input name=bestaetigung value=1 type=hidden>';
                    echo '</form>';

                    die();
                }        // Gibt es den Artikel schon in �hlicher Form?
            }

            if ($LARSpeichern) {
                $SQL = 'INSERT INTO Lieferantenartikel';
                $SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_ALTERNATIVARTIKEL,LAR_BEMERKUNGEN,LAR_AUSLAUFARTIKEL';
                $SQL .= ',LAR_PROBLEMARTIKEL,LAR_PROBLEMBESCHREIBUNG,LAR_GEPRUEFT,LAR_APP_ID,LAR_MUSTER,LAR_BEZWW';
                $SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_REKLAMATIONSKENNUNG,LAR_SUCH_LARTNR,LAR_LARTID';
                $SQL .= ',LAR_USER, LAR_USERDAT';
                $SQL .= ')VALUES (';
                $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_LARTNR'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_LIE_NR'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLAS_ALTERNATIVARTIKEL'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_BEMERKUNGEN'], true);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLAS_AUSLAUFARTIKEL'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLAS_PROBLEMARTIKEL'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_PROBLEMBESCHREIBUNG'], true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_GEPRUEFT'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLAS_APP_ID'], true);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtLAS_MUSTER'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_BEZWW'], false);
                $SQL .= ',0';
                $SQL .= ',4';        // Importquelle
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_REKLAMATIONSKENNUNG'], false);
                $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $_POST['txtLAS_LARTNR'], false) . ')';
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtLAS_LARTID'], false) . '';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                awisFormular::DebugAusgabe(1, $_POST, $SQL);
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 9011511815, $SQL, 2);
                }
                $SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $LAS_KEY = $rsKey->FeldInhalt('KEY');
            }

            if ($LARZuordnen AND $LAS_KEY > 0) {
                if (!isset($_POST['txtLAR_LARTNR'])) {
                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_lar_key'] = $_POST['txtLAS_KEY'];

                    $SQL = "SELECT LAR_LARTNR FROM LIEFERANTENARTIKEL WHERE LAR_KEY=:var_N0_lar_key";
                    $rsLAR_LARTNR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                    $txtLAR_LARTNR = $rsLAR_LARTNR->FeldInhalt('LAR_LARTNR');
                } else {
                    $txtLAR_LARTNR = $_POST['txtLAR_LARTNR'];
                }

                $SQL = 'INSERT INTO TeileInfos';
                $SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
                $SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
                $SQL .= 'VALUES(';
                $SQL .= ' \'LAR\'';
                $SQL .= ',' . $LAS_KEY;
                $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $_POST['txtLAS_LARTNR']) . ')';
                $SQL .= ',\'' . $_POST['txtLAS_LARTNR'] . '\'';
                $SQL .= ',\'LAS\'';
                $SQL .= ',' . (isset($_POST['txtParentKEY'])?$_POST['txtParentKEY']:$_POST['txtLAR_KEY']);
                $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', (isset($_POST['txtParentNR'])?$_POST['txtParentNR']:$txtLAR_LARTNR)) . ')';
                $SQL .= ',\'' . (isset($_POST['txtParentNR'])?$_POST['txtParentNR']:$txtLAR_LARTNR) . '\'';
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                $Form->DebugAusgabe(1, $SQL);
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151816, $SQL, 2);
                }
                $SQL = 'SELECT seq_TEI_Key.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $TEIKEY = $rsKey->FeldInhalt('KEY');

                // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Artikel
                // nicht durch den Import gel�scht werden kann
                // User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
                if (isset($rsKey) AND ($rsKey->FeldInhalt('KEY') != '' OR $AWIS_KEY2 != $rsKey->FeldInhalt('KEY'))) {
                    $SQL = 'UPDATE Lieferantenartikel SET';
                    $SQL .= ' LAR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ', LAR_IMQ_ID = CASE WHEN BITAND(LAR_IMQ_ID,4)=4 THEN LAR_IMQ_ID ELSE LAR_IMQ_ID+4 END';
                    $SQL .= ', LAR_userdat=sysdate';
                    $SQL .= ' WHERE LAR_key=0' . $AWIS_KEY2 . ' AND BITAND(LAR_IMQ_ID,4)<>4';
                    if ($DB->Ausfuehren($SQL) === false) {
                        throw new awisException('Fehler beim Speichern', 901151816, $SQL, 2);
                    }
                }
            }

            $AWIS_KEY2 = 0;    // Wieder l�schen, damit in der Oberfl�che eine Listendarstellung erreicht werden kann

        } else        // zu�ndernder Artikel
        {
            $Felder = explode(';', $Form->NameInArray($_POST, 'txtLAS_', 1, 1));
            $FehlerListe = array();
            $UpdateFelder = '';

            $BindeVariablen = array();
            $BindeVariablen['var_N0_lar_key'] = $_POST['txtLAS_KEY'];

            $SQL = 'SELECT LAR_KEY, LAR_LARTNR, LAR_LIE_NR, LAR_ALTERNATIVARTIKEL, LAR_ALTELIEFNR, ';
            $SQL .= 'LAR_BEMERKUNGEN, LAR_AUSLAUFARTIKEL, LAR_PROBLEMARTIKEL, ';
            $SQL .= 'LAR_PROBLEMBESCHREIBUNG, LAR_GEPRUEFT, LAR_APP_ID, LAR_MUSTER, LAR_BEZWW, LAR_BEKANNTWW, ';
            $SQL .= 'LAR_IMQ_ID, LAR_REKLAMATIONSKENNUNG, LAR_SUCH_LARTNR, LAR_USER, LAR_USERDAT, LAR_LARTID ';
            $SQL .= 'FROM Lieferantenartikel WHERE lar_key=:var_N0_lar_key';
            $rsLAR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $FeldNameDB = 'LAR_' . substr($FeldName, 4);
                    $WertNeu = $DB->FeldInhaltFormat($rsLAR->FeldInfo($FeldNameDB, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsLAR->FeldInfo($FeldNameDB, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsLAR->FeldInfo($FeldNameDB, 'TypKZ'), $rsLAR->FeldInhalt($FeldNameDB), true);
                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsLAR->FeldInhalt('LAR_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
                die();
            } elseif ($FeldListe != '') {
                $FeldListe = str_replace('LAS_', 'LAR_', $FeldListe);

                $SQL = 'UPDATE Lieferantenartikel SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', LAR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', LAR_userdat=sysdate';
                $SQL .= ' WHERE LAR_key=0' . $_POST['txtLAS_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151817, $SQL, 2);
                }
            }
            $AWIS_KEY2 = 0;
        }
    }

    //**************************************************************************
    //**************************************************************************
    //* Artikelpruefpersonal
    //**************************************************************************
    //**************************************************************************
    if (isset($_POST['txtAPP_ID'])) {
        $AWIS_KEY1 = $_POST['txtAPP_ID'];

        if (intval($_POST['txtAPP_ID']) === 0)        // Neuer Artikel
        {
            $Fehler = '';
            // Daten auf Vollst�ndigkeit pr�fen
            $Pflichtfelder = array('APP_NAME', 'APP_AKTIVBIS');

            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['AST'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $SQL = 'INSERT INTO Artikelpruefpersonal';
            $SQL .= '(APP_ID, APP_NAME, APP_AKTIVBIS';
            $SQL .= ',APP_KON_KEY, APP_USER, APP_USERDAT)';
            if ($DB->FeldInhaltFormat('N0', $_POST['txtAPP_ID'], false) == 0) {
                $SQL .= ' SELECT MAX(APP_ID)+1 AS ID ';
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtAPP_NAME'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtAPP_AKTIVBIS'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtAPP_KON_KEY'], false);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ' FROM Artikelpruefpersonal';
            } else {
                $SQL .= 'VALUES (';
                $SQL .= '' . $DB->FeldInhaltFormat('N0', $_POST['txtAPP_ID'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtAPP_NAME'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtAPP_AKTIVBIS'], false);
                $SQL .= ',' . $DB->FeldInhaltFormat('N0', $_POST['txtAPP_KON_KEY'], false);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
            }
            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('app_speichern', 9010151819, $SQL, 4);
                $Form->DebugAusgabe(1, $SQL);
            }
            $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_POST['txtAPP_ID']);
        } else                    // ge�nderter Artikel
        {
            $Felder = explode(';', $Form->NameInArray($_POST, 'txtAPP_', 1, 1));
            $FehlerListe = array();
            $UpdateFelder = '';

            $AWISBenutzer->ParameterSchreiben("AktuellerAST", $_POST['txtAPP_ID']);

            $BindeVariablen = array();
            $BindeVariablen['var_N0_app_id'] = $_POST['txtAPP_ID'];

            $rsAST = $DB->RecordSetOeffnen('SELECT * FROM Artikelpruefpersonal WHERE APP_ID=:var_N0_app_id', $BindeVariablen);
            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $rsAST->FeldInhalt($FeldName), true);
                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsAST->FeldInhalt('APP_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
                die();
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE Artikelpruefpersonal SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', APP_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', APP_userdat=sysdate';
                $SQL .= ' WHERE APP_ID=0' . $_POST['txtAPP_ID'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException($Form->LadeTextBaustein('FEHLER', 'SpeicherFehler'), 200901151754, $SQL, 2);
                }
            }

            $AWIS_KEY1 = $_POST['txtAPP_ID'];
        }
    }

    //**************************************************************************
    //**************************************************************************
    //* Artikelpruefungen
    //**************************************************************************
    //**************************************************************************
    if (isset($_POST['txtAPR_KEY'])) {
        $AWIS_KEY2 = $_POST['txtAPR_KEY'];

        if ($DB->FeldInhaltFormat('N0', $_POST['txtAPR_KEY'], false) == 0)        // Neuer Artikel
        {
            $Fehler = '';
            // Daten auf Vollst�ndigkeit pr�fen
            $Pflichtfelder = array('APR_LAR_KEY', 'APR_FEHLERBESCHREIBUNG');

            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['AST'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $BindeVariablen = array();
            $BindeVariablen['var_N0_lar_key'] = $_POST["txtAPR_LAR_KEY"];

            $SQL = 'SELECT LAR_LARTNR, LAR_LIE_NR FROM Lieferantenartikel WHERE LAR_KEY = :var_N0_lar_key';
            $rsLAR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

            $SQL = 'INSERT INTO ARTIKELPRUEFUNGEN (';
            $SQL .= ' APR_AST_ATUNR, APR_LAR_KEY,';
            $SQL .= ' APR_LAR_LARTNR, APR_LIE_NR, APR_FEHLERBESCHREIBUNG, ';
            $SQL .= ' APR_MENGE_N, APR_PRUEFER_N, APR_DATUM_N, APR_ERGEBNIS_N, APR_MASSNAHME_N, ';
            $SQL .= ' APR_MENGE_L, APR_PRUEFER_L, APR_DATUM_L, APR_ERGEBNIS_L, APR_MASSNAHME_L, ';
            $SQL .= ' APR_FILIALBESTAND, APR_MASSNAHME, ';
            $SQL .= ' APR_BEARBEITER, APR_BEARBEITUNGSTAG, APR_MELDUNGDURCH, ';
            $SQL .= ' APR_LIEFINFORMIERT, APR_LIEFINFORMIERTAM,  ';
            $SQL .= ' APR_MASSNAHME_FILIALEN,';
            $SQL .= ' APR_WEITERGELEITETAM, APR_USER,';
            $SQL .= ' APR_USERDAT) ';
            $SQL .= ' VALUES (';

            $SQL .= " " . $DB->FeldInhaltFormat('T', $_POST["txtAST_ATUNR"]);
            $SQL .= ', ' . $DB->FeldInhaltFormat('N0', $_POST["txtAPR_LAR_KEY"]);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $rsLAR->FeldInhalt('LAR_LARTNR'), true);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $rsLAR->FeldInhalt('LAR_LIE_NR'), true);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_FEHLERBESCHREIBUNG"]);

            $SQL .= ", " . (isset($_POST["txtAPR_MENGE_N"])?$DB->FeldInhaltFormat('N0', $_POST["txtAPR_MENGE_N"]):'0');
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_PRUEFER_N"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('D', $_POST["txtAPR_DATUM_N"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_ERGEBNIS_N"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_MASSNAHME_N"], true);

            $SQL .= ", " . (isset($_POST["txtAPR_MENGE_L"])?$DB->FeldInhaltFormat('N0', $_POST["txtAPR_MENGE_L"]):'0');
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_PRUEFER_L"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('D', $_POST["txtAPR_DATUM_L"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_ERGEBNIS_L"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_MASSNAHME_L"], true);

            $SQL .= ", " . (isset($_POST["txtAPR_FILIALBESTAND"])?$DB->FeldInhaltFormat('N0', $_POST["txtAPR_FILIALBESTAND"]):'null');
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_MASSNAHME"], true);

            $SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
            $SQL .= ", SYSDATE";
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_MELDUNGDURCH"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_LIEFINFORMIERT"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('D', $_POST["txtAPR_LIEFINFORMIERTAM"], true);

            $SQL .= ", " . $DB->FeldInhaltFormat('T', $_POST["txtAPR_MASSNAHME_FILIALEN"], true);
            $SQL .= ", " . $DB->FeldInhaltFormat('D', $_POST["txtAPR_WEITERGELEITETAM"], true);
            $SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
            $SQL .= ", SYSDATE)";

            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('apr_speichern', 9010151819, $SQL, 4);
                $Form->DebugAusgabe(1, $SQL);
            } else {
                $SQL = 'SELECT seq_apr_key.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $AWIS_KEY2 = $rsKey->FeldInhalt('KEY');
            }
        } else                    // ge�nderter Artikel
        {
            $Felder = explode(';', $Form->NameInArray($_POST, 'txtAPR_', 1, 1));
            $FehlerListe = array();
            $UpdateFelder = '';

            $AWISBenutzer->ParameterSchreiben("AktuellerAST", $_POST['txtAPR_KEY']);

            $BindeVariablen = array();
            $BindeVariablen['var_N0_apr_key'] = $_POST['txtAPR_KEY'];
            $rsAST = $DB->RecordSetOeffnen('SELECT * FROM Artikelpruefungen WHERE APR_KEY=:var_N0_apr_key', $BindeVariablen);
            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsAST->FeldInfo($FeldName, 'TypKZ'), $rsAST->FeldInhalt($FeldName), true);
                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsAST->FeldInhalt('APR_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
                die();
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE Artikelpruefungen SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', APR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', APR_userdat=sysdate';
                $SQL .= ' WHERE APR_KEY=0' . $_POST['txtAPR_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException($Form->LadeTextBaustein('FEHLER', 'SpeicherFehler'), 200901151754, $SQL, 2);
                }
            }

            $AWIS_KEY2 = $_POST['txtAPR_KEY'];        }

        $Liste = str_replace('txtAIS_', '', $Form->NameInArray($_POST, 'txtAIS_', 1));
        if ($Liste != '') {
            $SQL = 'MERGE INTO ARTIKELPRUEFUNGENMELDUNGEN USING DUAL ON (APM_APR_KEY=0' . $AWIS_KEY2 . ')';
            $SQL .= " WHEN MATCHED THEN UPDATE SET APM_BENACHRICHTIGUNGEN='" . $Liste . "', APM_USER='" . $AWISBenutzer->BenutzerName() . "', APM_USERDAT=SYSDATE";
            $SQL .= " WHEN NOT MATCHED THEN INSERT (APM_APR_KEY, APM_BENACHRICHTIGUNGEN, APM_USER, APM_USERDAT)";
            $SQL .= " VALUES( " . $AWIS_KEY2 . ", '" . $Liste . "', '" . $AWISBenutzer->BenutzerName() . "', SYSDATE)";

            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException($Form->LadeTextBaustein('FEHLER', 'SpeicherFehler'), 200901221821, $SQL, 2);
            }
        }
    }

    //***********************************************************************************
    //** Gebrauchsummern speichern und zuordnen
    //***********************************************************************************
    if (isset($_POST['txtGNR_KEY'])) {
        if (intval($_POST['txtGNR_KEY']) === 0)        // Neuer Artikel
        {
            // daten auf Vollst�ndigkeit pr�fen
            $Fehler = '';
            $Pflichtfelder = array('GNR_NUMMER');
            foreach ($Pflichtfelder AS $Pflichtfeld) {
                if ($_POST['txt' . $Pflichtfeld] == '')    // Name muss angegeben werden
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['GNR'][$Pflichtfeld] . '<br>';
                }
            }
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if ($Fehler != '') {
                die('<span class=HinweisText>' . $Fehler . '</span>');
            }

            $SQL = 'INSERT INTO Gebrauchsnummern';
            $SQL .= '(GNR_NUMMER, GNR_BEMERKUNG';
            $SQL .= ',GNR_USER, GNR_USERDAT';
            $SQL .= ')VALUES (';
            $SQL .= ' ' . $DB->FeldInhaltFormat('T', $_POST['txtGNR_NUMMER'], false);
            $SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtGNR_BEMERKUNG'], true);
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 901151809, $SQL, 2);
            }
            $SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
            $rsKey = $DB->RecordSetOeffnen($SQL);
            $GNRKEY = $rsKey->FeldInhalt('KEY');

            if (!isset($_POST['txtLAR_LARTNR'])) {
                $BindeVariablen = array();
                $BindeVariablen['var_N0_lar_key'] = $_POST['txtLAR_KEY'];

                $SQL = "SELECT LAR_LARTNR FROM LIEFERANTENARTIKEL WHERE LAR_KEY=:var_N0_lar_key";
                $rsLAR_LARTNR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                $txtLAR_LARTNR = $rsLAR_LARTNR->FeldInhalt('LAR_LARTNR');
            } else {
                $txtLAR_LARTNR = $_POST['txtLAR_LARTNR'];
            }

            $SQL = 'INSERT INTO TeileInfos';
            $SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
            $SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
            $SQL .= 'VALUES(';
            $SQL .= ' \'LAR\'';
            $SQL .= ',' . $_POST['txtLAR_KEY'];
            $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $txtLAR_LARTNR) . ')';
            $SQL .= ',\'' . $txtLAR_LARTNR . '\'';
            $SQL .= ',\'GNR\'';
            $SQL .= ',' . $GNRKEY;
            $SQL .= ',asciiwort(' . $DB->FeldInhaltFormat('TU', $_POST['txtGNR_NUMMER']) . ')';
            $SQL .= ',\'' . $_POST['txtGNR_NUMMER'] . '\'';
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 901151812, $SQL, 2);
            }
            $SQL = 'SELECT seq_TEI_Key.CurrVal AS KEY FROM DUAL';
            $rsKey = $DB->RecordSetOeffnen($SQL);
            $TEIKEY = $rsKey->FeldInhalt('KEY');

            awisFormular::DebugAusgabe(1, $SQL, $txtLAR_LARTNR, $DB->FeldInhaltFormat('TU', $txtLAR_LARTNR));
        } else        // zu�ndernder Artikel
        {
            $Felder = explode(';', $Form->NameInArray($_POST, 'txtGNR_', 1, 1));
            $FehlerListe = array();
            $UpdateFelder = '';

            $BindeVariablen = array();
            $BindeVariablen['var_N0_gnr_key'] = $_POST['txtGNR_KEY'];

            $SQL = 'SELECT * FROM Gebrauchsnummern WHERE GNR_key=:var_N0_gnr_key';
            $rsGNR = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
            $FeldListe = '';
            foreach ($Felder AS $Feld) {
                $FeldName = substr($Feld, 3);
                if (isset($_POST['old' . $FeldName])) {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu = $DB->FeldInhaltFormat($rsGNR->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
                    $WertAlt = $DB->FeldInhaltFormat($rsGNR->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
                    $WertDB = $DB->FeldInhaltFormat($rsGNR->FeldInfo($FeldName, 'TypKZ'), $rsGNR->FeldInhalt($FeldName), true);
                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY')) {
                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null') {
                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                        } else {
                            $FeldListe .= ', ' . $FeldName . '=';

                            if ($_POST[$Feld] == '')    // Leere Felder immer als NULL
                            {
                                $FeldListe .= ' null';
                            } else {
                                $FeldListe .= $WertNeu;
                            }
                        }
                    }
                }
            }

            if (count($FehlerListe) > 0) {
                $Meldung = str_replace('%1', $rsGNR->FeldInhalt('GNR_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
                foreach ($FehlerListe AS $Fehler) {
                    $FeldName = $Form->LadeTextBaustein(substr($Fehler[0], 0, 3), $Fehler[0]);
                    $Meldung .= '<br>&nbsp;' . $FeldName . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                }
                $Form->Fehler_Anzeigen('DSVeraendert', $Meldung, 'EingabeWiederholen', -1);
                die();
            } elseif ($FeldListe != '') {
                $SQL = 'UPDATE Gebrauchsnummern SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', GNR_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', GNR_userdat=sysdate';
                $SQL .= ' WHERE GNR_key=0' . $_POST['txtGNR_KEY'] . '';
                if ($DB->Ausfuehren($SQL) === false) {
                    throw new awisException('Fehler beim Speichern', 901151814, $SQL, 2);
                }
            }
            $AWIS_KEY2 = 0;// Wieder l�schen, um eine Listendarstellung in der Oberfl�che zu erzwingen
        }
    }

    //**************************************************************************************
    //* TeileinfosInfos
    //**************************************************************************************
    $InfoFelder = $Form->NameInArray($_POST, 'txtTII_', 1, 0);
    if (sizeof($InfoFelder[0]) > 0) {
        $InfoFelder = explode(';', $InfoFelder);
        $Form->DebugAusgabe(1, $InfoFelder, $AWIS_KEY2);
        $Form->DebugAusgabe(1, $_POST);
        foreach ($InfoFelder AS $InfoFeldText) {
            $InfoFeld = explode('_', $InfoFeldText);
            $ITY_KEY = $InfoFeld[1];
            if ($InfoFeld[3] == '') {
                switch ($ITY_KEY) {
                    case 5:            // OE-ATU - Bemerkung
                        $TEI_KEY = $TEIKEY;
                        break;
                }
            } else {
                $TEI_KEY = floatval($InfoFeld[3]);
            }
            if ($TEI_KEY === 0) {
                die('Keine ID');
            }

            $TII_KEY = ($InfoFeld[2] == ''?0:$InfoFeld[2]);

            if ($TII_KEY == 0) {
                $SQL = 'INSERT INTO teileinfosinfos';
                $SQL .= '(TII_TEI_KEY,TII_ITY_KEY,TII_WERT,TII_USER,TII_USERDAT)';
                $SQL .= ' VALUES(';
                $SQL .= ' ' . $TEI_KEY;
                $SQL .= ', ' . $ITY_KEY;
                $SQL .= ', ' . $DB->FeldInhaltFormat($InfoFeld[4], $_POST[$InfoFeldText]);
                $SQL .= ', ' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
                $SQL .= ',SYSDATE)';
            } else {
                $SQL = 'UPDATE teileinfosinfos';
                $SQL .= ' SET tii_wert = ' . $DB->FeldInhaltFormat($InfoFeld[4], $_POST[$InfoFeldText]);
                $SQL .= ', tii_user = ' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
                $SQL .= ', tii_userdat = SYSDATE';
                $SQL .= ' WHERE tii_key = 0' . $TII_KEY;
            }

            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 901151814, $SQL, 2);
            }
        }
    }

    //**************************************************************************************
    //* Artikelalternativen
    //**************************************************************************************
    if (isset($_POST['txtAAL_ALTERNATIV_AST_ATUNR'])) {
        $SQL = 'INSERT';
        $SQL .= ' INTO ARTIKELALTERNATIVEN';
        $SQL .= '   (';
        $SQL .= '     AAL_AST_ATUNR,';
        $SQL .= '     AAL_ALTERNATIV_AST_ATUNR,';
        $SQL .= '     AAL_USER,';
        $SQL .= '     AAL_USERDAT';
        $SQL .= '   )';
        //Wird per Select gemacht, damit bei einer beidseitigen Alternative das gegenst�ck in einem Insert abgehandelt werden kann.
        $SQL .= ' SELECT ';
        $SQL .= $DB->WertSetzen('AAL', 'T', $_POST['txtAST_ATUNR']);
        $SQL .= ',     ' . $DB->WertSetzen('AAL', 'T', $_POST['txtAAL_ALTERNATIV_AST_ATUNR']);
        $SQL .= ',     ' . $DB->WertSetzen('AAL', 'T', $AWISBenutzer->BenutzerName());
        $SQL .= ',     sysdate';
        $SQL .= ' FROM DUAL ';
        if (isset($_POST['txtAAL_BEIDSEITIG'])) { //Beidseitige alternative?
            $SQL .= ' UNION SELECT ';
            $SQL .= $DB->WertSetzen('AAL', 'T', $_POST['txtAAL_ALTERNATIV_AST_ATUNR']);
            $SQL .= ',     ' . $DB->WertSetzen('AAL', 'T', $_POST['txtAST_ATUNR']);
            $SQL .= ',     ' . $DB->WertSetzen('AAL', 'T', $AWISBenutzer->BenutzerName());
            $SQL .= ',     sysdate';
            $SQL .= ' FROM DUAL ';
        }
        try {
            $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('AAL'));
            $Form->Hinweistext($Form->LadeTextBaustein('AAL', 'AAL_GESPEICHERT', $AWISBenutzer->BenutzerSprache()));
        } catch (awisException $e) {
            if (strpos($e->getMessage(), 'UID_AAL_AST_ATUNR') !== false) {
                $Form->Hinweistext($Form->LadeTextBaustein('Fehler', 'err_AlternativeBereitsVorhanden', $AWISBenutzer->BenutzerSprache()));
            }
        }
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>