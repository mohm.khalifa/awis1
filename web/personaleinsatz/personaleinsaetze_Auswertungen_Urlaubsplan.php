<?php

/***************************************
* Urlaubsplan
***************************************/
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');


global $AWIS_KEY1;

try 
{
	$AWISBenutzer = awisBenutzer::Init();	
	$Form = new awisFormular();
	
	$TextKonserven = array();				
	$TextKonserven[]=array('Ausdruck','txt_KeineDatenGefunden');	
	$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
	$TextKonserven[]=array('Wort','wrd_Stand');	
	$TextKonserven[]=array('Wort','Seite');
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht4505 = $AWISBenutzer->HatDasRecht(4505);
	if($Recht4505==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI_Auswertung'));					
		
	
	$Farbe[3][0]=200;	// Urlaub
	$Farbe[3][1]=0;
	$Farbe[3][2]=0;
	$Farbe[16][0]=255;	// Urlaub Neu
	$Farbe[16][1]=255;
	$Farbe[16][2]=0;
	$Farbe[17][0]=100;	// Urlaub Alt
	$Farbe[17][1]=200;
	$Farbe[17][2]=200;
	$Farbe[18][0]=200;	// Urlaub Plan
	$Farbe[18][1]=200;
	$Farbe[18][2]=200;
	$Farbe[19][0]=0;	// Überstunden
	$Farbe[19][1]=100;
	$Farbe[19][2]=0;
	$Farbe[21][0]=255;	// Überstunden
	$Farbe[21][1]=165;
	$Farbe[21][2]=000;

	
	//Mitarbeiter
	$SQL = "SELECT DISTINCT xbn_key, xbn_name ";
	$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
	$SQL .= "       FROM Perseinsbereichemitglieder";
	$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
	$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
	$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
	$SQL .= " GROUP BY PEB_KEY) Bereiche";
	//	$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
	// 04.06.2009 SK
	$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key ";
	$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGAB END <= SYSDATE ';
	$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGBIS END >= SYSDATE ';
	$SQL .= " INNER JOIN Benutzer ON pbm_xbn_key = xbn_key";
	$SQL .= ' ORDER BY 2';

	$rsMIT = $DB->RecordSetOeffnen($SQL);
	$rsMITZeilen = $rsMIT->AnzahlDatensaetze();
	
	$Mitarbeiter=array();
	
	while(!$rsMIT->EOF())
	{
		$Mitarbeiter[$rsMIT->FeldInhalt('XBN_KEY')]=array('Anzeige'=>0,'Name'=>$rsMIT->FeldInhalt('XBN_NAME'));
		$rsMIT->DSWeiter();
	}
	//$Form->DebugAusgabe(1,$SQL,$Mitarbeiter);
	//die();
		
		
	//$SQL = "select MIT_BEZEICHNUNG, MIT_KEY";
	//$SQL .= ' FROM Mitarbeiter ';
	//$SQL .= ' WHERE ';
	//$SQL .= " MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";
	//$SQL .= ' ORDER BY MIT_BEZEICHNUNG';
	/*
	$rsMIT = awisOpenRecordset($con, $SQL);
	$rsMITZeilen = $awisRSZeilen;
	$Mitarbeiter=array();
	for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
	{
		$Mitarbeiter[$rsMIT['MIT_KEY'][$MITZeile]]=array('Anzeige'=>0,'Name'=>$rsMIT['MIT_BEZEICHNUNG'][$MITZeile]);
	}
	*/
	
	$SQL = "SELECT xbn_name AS Mitarbeiter, XBN_KEY, PEA_ABKUERZUNG, PEI_DATUM, PEA_KEY, PEA_BEZEICHNUNG,";				 		
	$SQL .= " to_char(PEI_DATUM,'RRRR-MM') AS Monat ";	
	//$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';			
	$SQL .= ' FROM Personaleinsaetze';
	$SQL .= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';	
	$SQL .= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	$SQL .= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY'; //AND BITAND(PEA_AUSWERTUNG,1)=1';		
	$SQL .= ' WHERE PEA_KEY in (3,16,17,18,19,21)';
		
	//$SQL .= " MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";

	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$SQL .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$SQL .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}
	
	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$SQL .= ' AND PEI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID'],false);
	}
	
	// Einschränken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
		if($Param['PBM_XBN_KEY']==0)
		{
			$SQL .= " AND EXISTS (SELECT *";
			$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$SQL .= "       FROM Perseinsbereichemitglieder";
			$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			$SQL .= " GROUP BY PEB_KEY) Bereiche";
			$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			$SQL .= ' WHERE pbm_xbn_key = PEI_XBN_KEY';
			$SQL .= ' )';
		}
		else
		{
			$SQL .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY'],false);
		}
	}
	
	if(isset($Param['FER_KEY']) AND $Param['FER_KEY']!='0')
	{

		$SQL .= ' AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN';
		$SQL .= ' WHERE xx1_fer_key =0'.$DB->FeldInhaltFormat('N0',$Param['FER_KEY'],false);
		$SQL .= ' AND xx1_kon_key = KON_KEY)';
		/*
		  $Bedingung = " AND EXISTS (SELECT *
		  FROM (
		  SELECT KON_KEY, XX1_FER_KEY, FEB_GUELTIGAB, FRZ_GUELTIGAB, FEZ_GUELTIGAB, FER_GUELTIGAB,
		  FEB_GUELTIGBIS, FRZ_GUELTIGBIS, FEZ_GUELTIGBIS, FER_GUELTIGBIS
		  FROM v_FilialEbenenRollen
		  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY
		  INNER JOIN FilialEbenen ON XX1_FEB_KEY = FEB_KEY
		  INNER JOIN FilialEbenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY
		  INNER JOIN FilialEbenenZuordnungen ON XX1_FEZ_KEY = FEZ_KEY
		  INNER JOIN FilialEbenenRollen ON XX1_FER_KEY = FER_KEY
		  INNER JOIN FilialEbenenRollenBereiche ON FER_FRB_KEY = FRB_KEY
		  WHERE XX1_FER_KEY =".$Param['FER_KEY']."
		  ORDER BY XX1_STUFE, FRZ_GUELTIGBIS, FRZ_USERDAT DESC
		  ) Daten WHERE KON_KEY = XBN_KON_KEY
		  AND FEB_GUELTIGAB <= PEI_DATUM AND FEB_GUELTIGBIS >= PEI_DATUM
		  AND FRZ_GUELTIGAB <= PEI_DATUM AND FRZ_GUELTIGBIS >= PEI_DATUM
		  AND FEZ_GUELTIGAB <= PEI_DATUM AND FEZ_GUELTIGBIS >= PEI_DATUM
		  AND FER_GUELTIGAB <= PEI_DATUM AND FER_GUELTIGBIS >= PEI_DATUM
		  ) ";
		*/
	}
/*		
	if($_POST['txtMIT_MTA_ID']!='-1')
	{
		$SQL .= " AND MIT_MTA_ID=" . $_POST['txtMIT_MTA_ID'] . "";
	}
	
	if(($RechteStufe&2)==2)	// Einschränkung Technik-Übersicht
	{
		$SQL .= ' AND MIT_MTA_ID IN (30,40,50)';
	}
*/	
	$SQL .= " ";
	$SQL .= " ORDER BY to_char(PEI_DATUM,'RRRR-MM'), XBN_NAME, XBN_KEY, PEI_DATUM";

	//awis_Debug(1,$SQL);

	$rsPEI = $DB->RecordSetOeffnen($SQL);
	$rsPEIZeilen = $rsPEI->AnzahlDatensaetze();
	
	$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');

	$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Personaleinsatz');

	//$Ausdruck->_pdf->SetAutoPageBreak(true,2);
	$Ausdruck->_pdf->SetAutoPageBreak(false,'');
	//$pdf->SetAuthor($AWISBenutzer->BenutzerName());
	
	if($rsPEIZeilen==0)
	{
		$Ausdruck->NeueSeite(0,1);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_KeineDatenGefunden'],0,0,'C',0);		
	}
		
	$RasterZeile=9999;
	$LetzterXBN_KEY = '';
	$LetzterMonat = '';
	$Legende = array();
	$NeueZeile=false;
	//$MaxTag='';
		
	while(!$rsPEI->EOF())
	{		
		$NeueZeile=false;
		if($LetzterMonat!=$rsPEI->FeldInhalt('MONAT') OR $RasterZeile>180)
		{			
			if($rsPEI->DSNummer()!=0)
			{
				/*
				foreach($Mitarbeiter AS $Eintrag)
				{
					if($Eintrag['Anzeige']==0)
					{
						if ($RasterZeile>180)
						{
							$Ausdruck->NeueSeite(0,1);
							
							$Ausdruck->_pdf->SetFont('Arial','',6);
							$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
					
							$Ausdruck->_pdf->setXY(10,12);
							$Ausdruck->_pdf->SetFont('Arial','',6);
							$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);							
					
							$RasterZeile = 10;
							$Ausdruck->_pdf->SetFont('Arial','',20);
							$Ausdruck->_pdf->text(10,$RasterZeile,"Urlaubstage " . $rsPEI->FeldInhalt('MONAT'));
							$Ausdruck->_RasterZeile = 25;
								
							$RasterZeile = 30;
							$Ausdruck->_pdf->SetFillColor(240,240,240);
							$Ausdruck->_pdf->SetDrawColor(0,0,0);
							$Ausdruck->_pdf->setXY(10,$RasterZeile);
						
							$Ausdruck->_pdf->SetFont('Arial','',10);
							$Ausdruck->_pdf->cell(80,5,'Mitarbeitername',1,0,'L',1);			
							$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
							$MaxTag = date('d',(mktime(0,0,0,date('m',$Datum)+1,0,date('Y',$Datum))));
							
							for($Tag = 1;$Tag<=$MaxTag;$Tag++)
							{
								$Ausdruck->_pdf->cell(5,5,$Tag,1,0,'C',1);	
							}
							
						}
						$RasterZeile+=5;
						
						$Ausdruck->_pdf->SetFillColor(240,240,240);
						$Ausdruck->_pdf->SetDrawColor(0,0,0);
						$Ausdruck->_pdf->setXY(10,$RasterZeile);
				
						$Ausdruck->_pdf->SetFont('Arial','',10);
						$Ausdruck->_pdf->cell(80,5,$Eintrag['Name'],1,0,'L',1);
						for($Tag = 1;$Tag<=$MaxTag;$Tag++)
						{
							$Ausdruck->_pdf->cell(5,5,'',1,0,'C',0);	
						}
					}
				}
				*/
				
				$Ausdruck->_pdf->SetXY(15,20);
				$Ausdruck->_pdf->SetDrawColor(0,0,0);
				$Ausdruck->_pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
				$Legende = array();
			}
			
			$Ausdruck->NeueSeite(0,1);
				
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
	
			$Ausdruck->_pdf->setXY(10,12);
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);							
	
			$RasterZeile = 10;
			$Ausdruck->_pdf->SetFont('Arial','',20);
			$Ausdruck->_pdf->text(10,$RasterZeile,"Urlaubstage " . $rsPEI->FeldInhalt('MONAT'));
			$Ausdruck->_RasterZeile = 25;
			
			$RasterZeile = 30;
			$Ausdruck->_pdf->SetFillColor(240,240,240);
			$Ausdruck->_pdf->SetDrawColor(0,0,0);
			$Ausdruck->_pdf->setXY(10,$RasterZeile);
	
			$Ausdruck->_pdf->SetFont('Arial','',10);
			$Ausdruck->_pdf->cell(80,5,'Mitarbeitername',1,0,'L',1);			
			$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
			$MaxTag = date('d',(mktime(0,0,0,date('m',$Datum)+1,0,date('Y',$Datum))));
			
			for($Tag = 1;$Tag<=$MaxTag;$Tag++)
			{
				$Ausdruck->_pdf->cell(5,5,$Tag,1,0,'C',1);	
			}
			$LetzterMonat=$rsPEI->FeldInhalt('MONAT');			
					
			$NeueZeile=true;
	
			// Alle Eintraege zurücksetzen
			foreach($Mitarbeiter AS $Eintrag=>$Werte)
			{
				$Mitarbeiter[$Eintrag]['Anzeige']=0;
			}
		}
		
		
		// Neuer Mitarbeiter -> neue Zeile
		if($LetzterXBN_KEY!=$rsPEI->FeldInhalt('XBN_KEY') or $NeueZeile==true)
		{
			$RasterZeile += 5;
			
			$Ausdruck->_pdf->SetFillColor(240,240,240);
			$Ausdruck->_pdf->SetDrawColor(0,0,0);
			$Ausdruck->_pdf->setXY(10,$RasterZeile);
	
			$Ausdruck->_pdf->SetFont('Arial','',10);
			$Ausdruck->_pdf->cell(80,5,$rsPEI->FeldInhalt('MITARBEITER'),1,0,'L',1);
			$Mitarbeiter[$rsPEI->FeldInhalt('XBN_KEY')]['Anzeige']=1;
	
			for($Tag = 1;$Tag<=$MaxTag;$Tag++)
			{
				$Ausdruck->_pdf->cell(5,5,'',1,0,'C',0);	
			}
			
			$LetzterXBN_KEY=$rsPEI->FeldInhalt('XBN_KEY');
		}
		
				
		$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
		$Tag = date('d',$Datum);
					
		$Ausdruck->_pdf->setXY(90+(5*($Tag-1)),$RasterZeile);
		$Ausdruck->_pdf->SetFillColor($Farbe[$rsPEI->FeldInhalt('PEA_KEY')][0],$Farbe[$rsPEI->FeldInhalt('PEA_KEY')][1],$Farbe[$rsPEI->FeldInhalt('PEA_KEY')][2]);
		$Ausdruck->_pdf->SetFont('Arial','',4);
		$Ausdruck->_pdf->cell(5,5,$rsPEI->FeldInhalt('PEA_ABKUERZUNG'),1,0,'C',1);			
		//$Ausdruck->_pdf->cell(5,5,$Tag,1,0,'C',1);
		
		if(array_search($rsPEI->FeldInhalt('PEA_ABKUERZUNG').' - ' .$rsPEI->FeldInhalt('PEA_BEZEICHNUNG'),$Legende)===false)
		{
			$Legende[$rsPEI->FeldInhalt('PEA_ABKUERZUNG')]=$rsPEI->FeldInhalt('PEA_ABKUERZUNG') . ' - ' .$rsPEI->FeldInhalt('PEA_BEZEICHNUNG');
		}			
		$rsPEI->DSWeiter();
	}
			
	if($rsPEI->DSNummer()>0)
	{
		foreach($Mitarbeiter AS $Eintrag)
		{
			if($Eintrag['Anzeige']==0)
			{
				if ($RasterZeile>180)
				{
					$Ausdruck->NeueSeite(0,1);
					
					$Ausdruck->_pdf->SetFont('Arial','',6);
					$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
			
					$Ausdruck->_pdf->setXY(10,12);
					$Ausdruck->_pdf->SetFont('Arial','',6);
					$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);							
			
					$RasterZeile = 10;
					$Ausdruck->_pdf->SetFont('Arial','',20);
					$Ausdruck->_pdf->text(10,$RasterZeile,"Urlaubstage " . $rsPEI->FeldInhalt('MONAT'));
					$Ausdruck->_RasterZeile = 25;
						
					$RasterZeile = 30;
					$Ausdruck->_pdf->SetFillColor(240,240,240);
					$Ausdruck->_pdf->SetDrawColor(0,0,0);
					$Ausdruck->_pdf->setXY(10,$RasterZeile);
				
					$Ausdruck->_pdf->SetFont('Arial','',10);
					$Ausdruck->_pdf->cell(80,5,'Mitarbeitername',1,0,'L',1);			
					$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
					$MaxTag = date('d',(mktime(0,0,0,date('m',$Datum)+1,0,date('Y',$Datum))));
					
					for($Tag = 1;$Tag<=$MaxTag;$Tag++)
					{
						$Ausdruck->_pdf->cell(5,5,$Tag,1,0,'C',1);	
					}
					
				}
				
				$RasterZeile+=5;
				
				$Ausdruck->_pdf->SetFillColor(240,240,240);
				$Ausdruck->_pdf->SetDrawColor(0,0,0);
				$Ausdruck->_pdf->setXY(10,$RasterZeile);
		
				$Ausdruck->_pdf->SetFont('Arial','',10);
				$Ausdruck->_pdf->cell(80,5,$Eintrag['Name'],1,0,'L',1);
				for($Tag = 1;$Tag<=$MaxTag;$Tag++)
				{
					$Ausdruck->_pdf->cell(5,5,'',1,0,'C',0);					
				}					
			}
		}
		
		$Ausdruck->_pdf->SetXY(15,20);
		$Ausdruck->_pdf->SetDrawColor(0,0,0);
		$Ausdruck->_pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
		$Legende = array();
		
		$Ausdruck->_pdf->SetXY(15,20);
		$Ausdruck->_pdf->SetDrawColor(0,0,0);
		$Ausdruck->_pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
	}
	
	$Ausdruck->Anzeigen();	
}
			
	
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}

?>	