<?php
global $con;
global $AWISBenutzer;
global $awisRSZeilen;
global $AWISSprache;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $CursorFeld;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('FBR','FBR_%');
$TextKonserven[]=array('FBR','lst_FBR_RECHT');
$TextKonserven[]=array('XBN','XBN_VOLLERNAME');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht3801 = awisBenutzerRecht($con,3801,$AWISBenutzer->BenutzerName());
if($Recht3801==0)
{
    awisEreignis(3,1000,'CRM-AP',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$EditRecht = ($Recht3801&6);

$SQL = 'SELECT *';
$SQL .= ' FROM filialebenenrollenberzugriffe';
$SQL .= ' INNER JOIN Benutzer ON FBR_XBN_KEY = XBN_KEY';
$SQL .= ' WHERE FBR_FRB_KEY=0'.$AWIS_KEY1;

if(isset($_GET['FBR_KEY']))
{
	$SQL .= ' AND FBR_KEY=0'.intval($_GET['FBR_KEY']);
}
if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
{
	$SQL .= ' AND FBR_KEY=0'.intval($AWIS_KEY2);
}
if(!isset($_GET['FBRSort']))
{
	$SQL .= ' ORDER BY XBN_VOLLERNAME';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['FBRSort']);
}

$rsFBR = awisOpenRecordset($con,$SQL);
$rsFBRZeilen = $awisRSZeilen;

//awis_Debug(1,$SQL,$AWIS_KEY2,$AWIS_KEY1);
if($rsFBRZeilen > 1 or isset($_GET['FBRListe']))					// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();

	if($EditRecht>0)
	{
		$Icons[] = array('new','./filialebenen_Main.php?cmdAktion=Bereiche&Seite=Mitglieder&FBR_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,36,-1);
	}

	$Link = './filialebenen_Main.php?cmdAktion=Bereiche&Seite=Mitglieder';
	$Link .= '&FBRSort=XBN_VOLLERNAME'.((isset($_GET['FBRSort']) AND ($_GET['FBRSort']=='XBN_VOLLERNAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBN']['XBN_VOLLERNAME'],350,'',$Link);

	awis_FORM_ZeileEnde();

	for($FBRZeile=0;$FBRZeile<$rsFBRZeilen;$FBRZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();

		if($EditRecht>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./filialebenen_Main.php?cmdAktion=Bereiche&Seite=Mitglieder&FBR_KEY='.$rsFBR['FBR_KEY'][$FBRZeile]);
			$Icons[] = array('delete','./filialebenen_Main.php?cmdAktion=Bereiche&Seite=Mitglieder&Del='.$rsFBR['FBR_KEY'][$FBRZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,36,($FBRZeile%2));

		awis_FORM_Erstelle_ListenFeld('XBN_VOLLERNAME',$rsFBR['XBN_VOLLERNAME'][$FBRZeile],20,350,false,($FBRZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtFBR_KEY type=hidden value=0'.(isset($rsFBR['FBR_KEY'][0])?$rsFBR['FBR_KEY'][0]:'').'>';
	$AWIS_KEY2 = (isset($rsFBR['FBR_KEY'][0])?$rsFBR['FBR_KEY'][0]:'');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Bereiche&Seite=Mitglieder&FBRListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsFBR['FBR_KEY'][0])?$rsFBR['FBR_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsFBR['FBR_KEY'][0])?$rsFBR['FBR_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['XBN']['XBN_VOLLERNAME'].':',150);
	$SQL = 'SELECT XBN_KEY, XBN_VOLLERNAME';
	$SQL .= ' FROM Benutzer';
	$SQL .= ' WHERE UPPER(XBN_NAME) NOT LIKE \'FIL%\'';
	$SQL .= ' ORDER BY XBN_VOLLERNAME';
	awis_FORM_Erstelle_SelectFeld('FBR_XBN_KEY',isset($rsFBR['FBR_XBN_KEY'][0])?$rsFBR['FBR_XBN_KEY'][0]:'',200,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$CursorFeld='txtFBR_XBN_KEY';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FBR']['FBR_RECHT'].':',150);
	$KateogrieAlt = explode("|",$AWISSprachKonserven['FBR']['lst_FBR_RECHT']);
	awis_FORM_Erstelle_SelectFeld('FBR_RECHT',(!isset($rsFBR['FBR_RECHT'][0])?'':$rsFBR['FBR_RECHT'][0]),130,$EditRecht,$con,'','','0','','',$KateogrieAlt,'');
	awis_FORM_ZeileEnde();


	awis_FORM_FormularEnde();
}
?>