<?php
/**
 * Diese Datei ist f�r das Drucken von Standard-Berichten zust�ndig. Es �bernimmt die Rechteverwaltung, das Laden der Berichte
 * und das Generieren der PDFs.
 * 
 * Folgende Pflicht-Parameter sind notwendig:
 * 
 *  XRE	= ID des Berichts (steht in der Tabelle Berichte)
 *  ID  = BASE64 kodierte Parameterliste
 *  	  Aufbau: <PARAMETER>=<Verleichsoperator>~<Wert>&<PARAMETER>...
 *        Vergleichsoperator muss URLENCODE durchlaufen.
 *        
 *         Beispiel:
 *         		Hier werden 2 Parameter zusammen gebaut und anschlie�en �bergeben 
 *         		FIL_ID.urlencode('=~').'90&DATUM'.urlencode('>=~').'01.01.2012'
 *         
 * @author Sacha Kerres
 * @version 20120215
 *         
 */
require_once('awisDatenbank.inc');			// Datenbankfunktionen
require_once('awisFormular.inc');			// Datenbankfunktionen

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_zurueck');
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$SQL = 'SELECT * ';
$SQL .= ' FROM berichte';
$SQL .= ' WHERE xre_key = :var_N0_xre_key';
$SQL .= ' AND xre_status = \'A\'';
$BindeVariablen=array();
$BindeVariablen['var_N0_xre_key']=intval($_GET['XRE']);

$rsXRE = $DB->RecordSetOeffnen($SQL,$BindeVariablen);

if($rsXRE->EOF())
{
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$Recht = $AWISBenutzer->HatDasRecht($rsXRE->FeldInhalt('XRE_XRC_ID'));

if(($Recht & $rsXRE->FeldInhalt('XRE_STUFE'))==0)
{
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$Klasse = $rsXRE->FeldInhalt('XRE_KLASSENNAME');
require_once('/daten/web/berichte/'. $Klasse. '.inc');

try
{
    $Bericht = new $Klasse($AWISBenutzer, $DB, intval($_GET['XRE']));
   
	$Param = array();
	if(isset($_GET['ID']))                  // Parameter kommen base64 kodiert
	{
		// Parameter werden durch & getrennt
		$ParameterListe = explode('&',base64_decode($_GET['ID']));
        foreach($ParameterListe AS $Parameter)
		{
			// Jeder Parameter hat das Format <Param>=<Operator>~<Wert>
			$Parameter = explode('=',$Parameter);
			$Param[$Parameter[0]]=(isset($Parameter[1])?urldecode($Parameter[1]):'');
		}
	}
	else
	{
		die();
	}
	$Bericht->init($Param);
	$Bericht->ErzeugePDF();
}
catch (Exception $ex)
{
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	$Form->DebugAusgabe(1,$ex->getFile().' '.$ex->getLine());
	die();
}
?>                                