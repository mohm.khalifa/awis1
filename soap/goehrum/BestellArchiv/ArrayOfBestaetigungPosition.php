<?php

class ArrayOfBestaetigungPosition implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BestaetigungPosition[] $BestaetigungPosition
     */
    protected $BestaetigungPosition = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BestaetigungPosition[]
     */
    public function getBestaetigungPosition()
    {
      return $this->BestaetigungPosition;
    }

    /**
     * @param BestaetigungPosition[] $BestaetigungPosition
     * @return ArrayOfBestaetigungPosition
     */
    public function setBestaetigungPosition(array $BestaetigungPosition = null)
    {
      $this->BestaetigungPosition = $BestaetigungPosition;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BestaetigungPosition[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BestaetigungPosition
     */
    public function offsetGet($offset)
    {
      return $this->BestaetigungPosition[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BestaetigungPosition $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BestaetigungPosition[] = $value;
      } else {
        $this->BestaetigungPosition[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BestaetigungPosition[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BestaetigungPosition Return the current element
     */
    public function current()
    {
      return current($this->BestaetigungPosition);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BestaetigungPosition);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BestaetigungPosition);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BestaetigungPosition);
    }

    /**
     * Countable implementation
     *
     * @return BestaetigungPosition Return count of elements
     */
    public function count()
    {
      return count($this->BestaetigungPosition);
    }

}
