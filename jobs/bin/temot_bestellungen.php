<?php
require_once('awisJobVerwaltung.php');
require_once('db.inc.php');
require_once('awisTemot.php');
require_once("awis_forms.inc.php");

global $awisRSZeilen;
global $argv;

try 
{
	$con = awisLogon();

	if(isset($argv[1]))
	{
		$Firma = $argv[1];
	}

	if($Firma=='PV')
	{
		$Job = new awisJobProtokoll(2, $con, 'Starte PV - Import.');
	}
	elseif($Firma=='TROST')
	{
		$Job = new awisJobProtokoll(3, $con, 'Starte TROST - Import.');
	}
	else 
	{
		throw new Exception('Ungueltige Firma!',200710231321);
	}
	
 	$Temot = new awisTemot(1,$Firma);
	 	// Alle noch offnen Daten lesen
	$Temot->LeseBestellungen('','','','','','');

	$SQL = 'SELECT * FROM Zukaufbestellungen ';
 	$SQL .= ' WHERE ZUB_ZLI_KEY=1 AND ZUB_ExterneID IS NULL';
 	$rsZUB = awisOpenRecordset($con, $SQL);
 	$rsZUBZeilen = $awisRSZeilen;
 	echo $rsZUBZeilen . 'offene gefunden.'.PHP_EOL;
 	for($rsZeile=0;$rsZeile<$rsZUBZeilen;$rsZeile++)
 	{
 		//$Filiale,$WANr,$DatumVom,$DatumBis,'',$AlleLesen
 		echo ' --> Suche nach '.$rsZUB['ZUB_WANR'][$rsZeile]. ' der Filiale '.$rsZUB['ZUB_FIL_ID'][$rsZeile].'.'.PHP_EOL;
 		$Filiale = sprintf('%04d',$rsZUB['ZUB_FIL_ID'][$rsZeile]);
 		$Temot->LeseBestellungen($Filiale,$rsZUB['ZUB_WANR'][$rsZeile],'','','','A',true);	
 	}

 	$SQL = 'SELECT * FROM ZukaufSonderanfragen ';
 	$SQL .= ' WHERE ZSA_FIRMA=\''.$Firma.'\'';
 	$rsZSA = awisOpenRecordset($con, $SQL);
 	$rsZSAZeilen = $awisRSZeilen;
 	echo $rsZSAZeilen . 'offene Sonderanfragen.'.PHP_EOL;
 	for($rsZeile=0;$rsZeile<$rsZSAZeilen;$rsZeile++)
 	{
 		//$Filiale,$WANr,$DatumVom,$DatumBis,'',$AlleLesen
 		echo ' --> Sonderanfragen nach '.$rsZSA['ZSA_WANR'][$rsZeile].PHP_EOL;
 		$Temot->LeseBestellungen('',$rsZSA['ZSA_WANR'][$rsZeile],'','','','A',false);
 		$SQL = 'DELETE FROM ZukaufSonderanfragen WHERE ZSA_WANR='.awis_FeldInhaltFormat('T',$rsZSA['ZSA_WANR'][$rsZeile]);
 		awisExecute($con, $SQL);	
 	}

 	$Job->SchreibeLog(1,0,'Ende des Jobs');
}
catch (Exception $ex)
{
	echo $ex->getMessage().PHP_EOL;
 	$Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.awisFeldFormat('T',$ex->getMessage()));
}

?>
