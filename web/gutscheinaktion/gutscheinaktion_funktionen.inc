<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class gutscheinaktion_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht55000;
    public $EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht55000 = $this->AWISBenutzer->HatDasRecht(55000);
        $this->EditRecht = (($this->Recht55000 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_GUT'));

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('GUT', '%');
        $TextKonserven[] = array('BLZ', '%');
        $TextKonserven[] = array('AST', 'AST_ATUNR');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('Fehler', 'err_keineRechte');
        $TextKonserven[] = array('TITEL', 'tit_Gutscheinaktion');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }


    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_GUT', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    //Sperrt das Editieren, wenn man nicht das recht hat, bereits erfasste zu bearbeiten.
    public function EditSperren(){
        global $AWIS_KEY1;

        if(($this->Recht55000&4)!=4 and $AWIS_KEY1>0){
            $this->EditRecht = false;
        }
    }

    public function RechteMeldung($Bit = 1)
    {
        if (($this->Recht55000 & $Bit) !== $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'GUT'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function erstelleIBANInfos($IBAN, $LKZ, $GUT_KEY)
    {
        global $AWIS_KEY1;
        $AWIS_KEY1 = $GUT_KEY;
        $this->EditSperren();
        if($IBAN < 10){
            return;
        }

        //�bergebene Iban ist ohne L�nderkennung. Die ersten zwei stellen sind eine Pr�fziffer. Danach kommt die 8 Stellige BLZ
        $BLZ = substr($IBAN,2,8);

        $LKZ = strtoupper($LKZ);
        $SQL  ='SELECT ';
        $SQL .='   BLZ_BANKLEITZAHL,';
        $SQL .='   BLZ_BIC,';
        $SQL .='   BLZ_POSTLEITZAHL,';
        $SQL .='   BLZ_BEZEICHNUNG,';
        $SQL .='   BLZ_BEZEICHNUNG_KURZ';
        $SQL .=' FROM BANKLEITZAHLENIBAN ';
        $SQL .=' where ';
        $SQL .= '  BLZ_BANKLEITZAHL = ' .$this->DB->WertSetzen('BLZ','T',$BLZ);
        //F�r ausl�ndische Banken haben wir keine Daten. --> SQL darf nichts liefern
        if($LKZ!='DE'){
            $SQL .= ' AND 1 = 0';
        }

        $rsBIC = $this->DB->RecordSetOeffnen($SQL,$this->DB->Bindevariablen('BLZ'));

        $Edit = $this->EditRecht;
        if($rsBIC->AnzahlDatensaetze()!=1 and $LKZ == 'DE'){
            $this->Form->ZeileStart();
            $this->Form->Hinweistext($this->AWISSprachKonserven['GUT']['GUT_ERR_IBAN']);
            $this->Form->ZeileEnde();
            $Edit = true;
        }elseif($this->Form->PruefeIban($LKZ.$IBAN) == false){
            $this->Form->ZeileStart();
            $this->Form->Hinweistext($this->AWISSprachKonserven['GUT']['GUT_ERR_IBAN']);
            $this->Form->ZeileEnde();
            $Edit = true;
        }elseif($LKZ!='DE'){ //Bei Ausl�ndern muss die Eingabe offen bleiben
            $Edit = true;
        }

        if($this->EditRecht == false){
            $Edit = false;
        }

        $SQL  ='SELECT *';
        $SQL .=' FROM GUTSCHEINAKTION ';
        $SQL .=' ';
        $SQL .=' WHERE GUT_IBAN =' . $this->DB->WertSetzen('CHK','T',$IBAN);
        $SQL .=' and upper(GUT_IBAN_LAND) = '. $this->DB->WertSetzen('CHK','T',$LKZ);
        $SQL .= ' and GUT_CREA_DAT >= sysdate -3';

        $Anz = $this->DB->ErmittleZeilenAnzahl($SQL,$this->DB->Bindevariablen('CHK'));

        //Wenn es bei der Neuanlage diese IBAN schon gibt, Hinweis anzeigen.
        //Wenn es 2 oder mehr gibt, dann Zeig den Hinweis immer an, nicht nur bei neuanlage.
        if(($Anz == 1 and $AWIS_KEY1 == -1) or  ($Anz>=2)){
            $this->Form->ZeileStart();
            $this->Form->Hinweistext($this->AWISSprachKonserven['GUT']['GUT_HINWEIS_IBAN'] . $Anz,awisFormular::HINWEISTEXT_HINWEIS,'color: #000000');
            $this->Form->ZeileEnde();
        }



        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BLZ']['BLZ_BEZEICHNUNG'] . ':', 170);
        $this->Form->Erstelle_TextFeld('!BLZ_BEZEICHNUNG', ((isset($_GET['txthiddenBLZ_BEZEICHNUNG']) and $_GET['txthiddenBLZ_BEZEICHNUNG']!='') ?$_GET['txthiddenBLZ_BEZEICHNUNG']:$rsBIC->FeldOderPOST('BLZ_BEZEICHNUNG')),62, 435, $Edit, '', '', '', 'T');
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BLZ']['BLZ_BIC'] . ':', 170);
        $this->Form->Erstelle_TextFeld('!BLZ_BIC', ((isset($_GET['txthiddenBLZ_BIC'])and $_GET['txthiddenBLZ_BIC']!='')?$_GET['txthiddenBLZ_BIC']:$rsBIC->FeldOderPOST('BLZ_BIC')),12, 150, $Edit, '', '', '', 'T');
        $this->Form->ZeileEnde();


    }

    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';
        //"gel�schte" immer raus
        $Bedingung .= ' AND GUT_STATUS != 5 ';
        if ($AWIS_KEY1 != 0) {
            $Bedingung .= ' AND GUT_KEY = ' . $this->DB->WertSetzen('GUT', 'N0', $AWIS_KEY1);

            return $Bedingung;
        }


        if (isset($this->Param['GUT_VORGANGSNUMMER']) and $this->Param['GUT_VORGANGSNUMMER'] != '') {
            $Bedingung .= ' AND GUT_VORGANGSNUMMER ' . $this->DB->LikeOderIst($this->Param['GUT_VORGANGSNUMMER'], awisDatenbank::AWIS_LIKE_UPPER, 'GUT');
        }

        if (isset($this->Param['GUT_GUTSCHEINNR']) and $this->Param['GUT_GUTSCHEINNR'] != '') {
            $Bedingung .= ' AND GUT_GUTSCHEINNR ' . $this->DB->LikeOderIst($this->Param['GUT_GUTSCHEINNR'], awisDatenbank::AWIS_LIKE_UPPER, 'GUT');
        }

        if (isset($this->Param['GUT_STATUS']) and $this->Param['GUT_STATUS'] != '') {

            if( $this->Param['GUT_STATUS'] == 2){
                $Bedingung .= ' AND GUT_STATUS in(1,2) ';
            }else{
                $Bedingung .= ' AND GUT_STATUS = ' . $this->DB->WertSetzen('GUT', 'T',$this->Param['GUT_STATUS']);

            }
        }


        if (isset($this->Param['GUT_NACHNAME']) and $this->Param['GUT_NACHNAME'] != '') {
            $Bedingung .= ' AND upper(GUT_NACHNAME) ' . $this->DB->LikeOderIst($this->Param['GUT_NACHNAME'], awisDatenbank::AWIS_LIKE_UPPER, 'GUT');
        }

        if (isset($this->Param['GUT_VORNAME']) and $this->Param['GUT_VORNAME'] != '') {
            $Bedingung .= ' AND upper(GUT_VORNAME) ' . $this->DB->LikeOderIst($this->Param['GUT_VORNAME'], awisDatenbank::AWIS_LIKE_UPPER, 'GUT');
        }


        if (isset($this->Param['GUT_FIL_ID']) and $this->Param['GUT_FIL_ID'] != '') {
            $Bedingung .= ' AND GUT_FIL_ID = ' . $this->DB->WertSetzen('GUT', 'T', $this->Param['GUT_FIL_ID']);
        }

        return $Bedingung;
    }


}