<?php
//**************************************************************
// Liefert die Lieferantenartikle im Zukauf f�r einen 
// angegebenen Lieferant und Hersteller
// Parameter: ARTNUMMER=a&LIE_NR=2620&ZLH_KEY=283
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['LIE_NR']))
{
	die('##PARAM##');
}
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// Auswahlliste in Abh�ngigkeit der Rechte anzeigen
$SQL = 'SELECT ZAL_KEY AS KEY, ZLA_ARTIKELNUMMER || \' - \' || ZAL_BEZEICHNUNG AS ANZEIGE';
$SQL .= ' FROM ZUKAUFARTIKELLIEFERANTEN';
$SQL .= ' INNER JOIN ZUKAUFARTIKEL ON ZAL_ZLA_KEY = ZLA_KEY';
$SQL .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLA_ZLH_KEY = ZLH_KEY';
$SQL .= ' WHERE ZAL_LIE_NR = :var_T_LIE_NR';
if(is_numeric($_GET['ZLH_KEY']))
{
	$SQL .= ' AND ZLH_TECDOCID = :var_N0_ZLH_KEY';
	$DB->SetzeBindevariable('ZLA', 'var_N0_ZLH_KEY', $_GET['ZLH_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
}
else
{
	$SQL .= ' AND EXISTS(SELECT * FROM ZUKAUFHERSTELLERCODES WHERE ZLH_KEY = ZHK_ZLH_KEY AND ZHK_CODE = :var_T_ZHK_CODE)';
	$DB->SetzeBindevariable('ZLA', 'var_T_ZHK_CODE', $_GET['ZLH_KEY'], awisDatenbank::VAR_TYP_TEXT);
}
$SQL .= ' AND (ZLA_ARTIKELNUMMER LIKE :var_T_ARTNUMMER';
$SQL .= '  OR ZAL_BESTELLNUMMER LIKE :var_T_ARTNUMMER)';
$SQL .= ' ORDER BY ZLA_ARTIKELNUMMER, ZAL_BEZEICHNUNG';


$DB->SetzeBindevariable('ZLA', 'var_T_LIE_NR', $_GET['LIE_NR'], awisDatenbank::VAR_TYP_TEXT);
$DB->SetzeBindevariable('ZLA', 'var_T_ARTNUMMER', strtoupper($_GET['ARTNUMMER']), awisDatenbank::VAR_TYP_TEXT);

$rsPEB = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZLA'));
echo $DB->LetzterSQL();

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen

while(!$rsPEB->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE')===false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsPEB->FeldInhalt('KEY').'">'.$rsPEB->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsPEB->FeldInhalt('KEY').'#+#'.$rsPEB->FeldInhalt('ANZEIGE').'';
	}

	$rsPEB->DSWeiter();
}
?>