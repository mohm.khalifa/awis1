<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('FAB','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_keinPersonal');
$TextKonserven[]=array('Fehler','err_keinegueltigeATUNR');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_zurueck');


try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	$Form->DebugAusgabe(1,'speichern',$_POST);
	$Form->DebugAusgabe(1,'speichern',$_GET);

	//***********************************************
	// Reifenpflege
	//***********************************************
	if(isset($_POST['txtFAB_KEY']))
	{
		if($_POST['txtFAB_KEY']=='')
		{
			$AWIS_KEY1=-1;
		}
		else
		{
			$AWIS_KEY1=$_POST['txtFAB_KEY'];
		}
		
		$Felder = $Form->NameInArray($_POST, 'txtFAB_',1,1);
		
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
				
			if ($AWIS_KEY1==-1) //Neuer Fachbetrieb
			{
				$Felder = $Form->NameInArray($_POST, 'txtFAB_',1,1);
			
				//Daten auf Vollst�ndigkeit pr�fen
				$Fehler = '';
				$Pflichtfelder = array('FAB_BEREICH','FAB_NAME1','FAB_LAN_CODE','FAB_PLZ','FAB_ORT');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FAB'][$Pflichtfeld].'<br>';
					}
				}
					
				// Wurden Fehler entdeckt? => Speichern abbrechen
				if($Fehler!='')
				{
					$Form->ZeileStart();
					$Form->Hinweistext($Fehler);
					$Form->ZeileEnde();
						
					$Link = './fachbetriebe_Main.php?cmdAktion=Details&FAB_KEY='.$AWIS_KEY1;
					$Form->SchaltflaechenStart();
					$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
					$Form->SchaltflaechenEnde();
						
					die();
				}
			
				$Speichern = true;
			
				if(isset($_POST['bestaetigung']))
				{
					$Speichern=isset($_POST['cmdSpeichern_x']);
				}

				if ($Speichern)
				{
					$SQL  = ' INSERT INTO FACHBETRIEBE';
					$SQL .= ' (';
					$SQL .= ' FAB_NAME1,';
					$SQL .= ' FAB_NAME2,';
					$SQL .= ' FAB_ANSPRECHP1,';
					$SQL .= ' FAB_STRASSE,';
					$SQL .= ' FAB_LAN_CODE,';
					$SQL .= ' FAB_PLZ,';
					$SQL .= ' FAB_ORT,';
					$SQL .= ' FAB_TELEFON,';
					$SQL .= ' FAB_TELEFAX,';
					$SQL .= ' FAB_MOBILNUMMER,';
					$SQL .= ' FAB_EMAIL,';
					$SQL .= ' FAB_BEREICH,';
					$SQL .= ' FAB_USER,';
					$SQL .= ' FAB_USERDAT';
					$SQL .= ' ) VALUES (';
					$SQL .= ' '.$DB->FeldInhaltFormat('T',$_POST['txtFAB_NAME1'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_NAME2'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_ANSPRECHP1'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_STRASSE'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_LAN_CODE'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_PLZ'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_ORT'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_TELEFON'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_TELEFAX'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_MOBILNUMMER'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_EMAIL'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtFAB_BEREICH'],false);
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ' ,SYSDATE)';

					//$Form->DebugAusgabe(1,$SQL);
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
					
					$SQL = 'SELECT seq_FAB_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
						
				}
					
			} //end Neuer Hersteller
			else //ge�nderter Hersteller
			{
				$FehlerListe = array();
				$UpdateFelder = '';
					
				$rsFAB= $DB->RecordSetOeffnen('SELECT * FROM fachbetriebe WHERE FAB_key=' . $_POST['txtFAB_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
			
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsFAB->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsFAB->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsFAB->FeldInfo($FeldName,'TypKZ'),$rsFAB->FeldInhalt($FeldName),true);
						//echo $WertAlt. '/'. $WertNeu . '<br>';
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
			
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
					
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsFAB->FeldInhalt('FAB_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE fachbetriebe SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', FAB_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', FAB_userdat=sysdate';
					$SQL .= ' WHERE FAB_key=0' . $_POST['txtFAB_KEY'] . '';
			
					$DB->Ausfuehren($SQL,'',false);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
				}
			}//End ge�nderter Hersteller
		}//end if($Felder!='')
	}//end if(isset($_POST['txtFAB_KEY']))
}//end Try		

catch (awisException $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}	
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}	
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>