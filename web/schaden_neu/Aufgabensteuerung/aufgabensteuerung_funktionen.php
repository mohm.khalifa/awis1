<?php


/**
 *  Erstellt das Ergebnis des Ajax f�r die Wiedervorlagen Verteilung
 *
 * @param $Wert integer Sachbearbeiter ID, des zu Vertretenden 
 */
function erstelleAjaxMitWV($Wert)
{
    global $AWISCursorPosition;
    global $AWISBenutzer;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $SpeichernOK;
    
    try
    {
        $DS=0;
    
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
    
        $DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
        $DBSCHAD->Oeffnen();
    
    
        $AWISBenutzer = awisBenutzer::Init();
        $Form = new awisFormular();
    
        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[]=array('BES','%');
        $TextKonserven[]=array('SBV','%');
        $TextKonserven[]=array('Wort','lbl_speichern');
        $TextKonserven[]=array('Wort','lbl_suche');
        $TextKonserven[]=array('Wort','AuswahlSpeichern');
        $TextKonserven[]=array('Wort','lbl_hinzufuegen');
        $TextKonserven[]=array('Wort','txt_BitteWaehlen');
        $TextKonserven[]=array('Wort','lbl_trefferliste');
        $TextKonserven[]=array('Wort','wrd_Filiale');
        $TextKonserven[]=array('Fehler','err_keineDaten');
        $TextKonserven[]=array('Liste','lst_OffenMass');
        $TextKonserven[]=array('Wort','PDFErzeugen');
        $TextKonserven[]=array('Wort','DatumVom');
        $TextKonserven[]=array('Wort','Datum');
        $TextKonserven[]=array('FIL','FIL_GEBIET');
        $TextKonserven[]=array('Liste','lst_ALLE_0');
        $TextKonserven[]=array('Liste','lst_JaNein');
        $TextKonserven[]=array('Fehler','err_keineDaten');
    
        $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
        $Param = array();
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SBV'));
        $Recht40016 = $AWISBenutzer->HatDasRecht(40016);
        
        if(!isset($Param['ORDERWV']) or $Param['ORDERWV'] == '')
        {
            $Param['ORDERWV'] = 'WV_DATUM';
        }
        
        if (isset($_GET['SortWV']) and $_GET['SortWV'] != '')
        {		// wenn GET-Sort, dann nach diesen Feld sortieren
            $Param['ORDERWV'] = str_replace('~',' DESC ', $_GET['SortWV']);
        }
        
        $SQLWV  ='SELECT';
        $SQLWV .='   WV_ID WV_ID,';
        $SQLWV .='   WV_DATUM WV_DATUM,';
        $SQLWV .='   WV_GRUND WV_GRUND,';
        $SQLWV .='   ID ID,';
        $SQLWV .='   ART ART,';
        $SQLWV .='   BEARBNRNEU BEARBNRNEU,';
        $SQLWV .='   BEARBEITUNGSNR BEARBEITUNGSNR,';
        $SQLWV .='   BEARBEITUNGSNRFIL BEARBEITUNGSNRFIL,';
        $SQLWV .='   SCHADENSJAHR SCHADENSJAHR,';
        $SQLWV .='   FILNR FILNR,';
        $SQLWV .='   LFDNR LFDNR,';
        $SQLWV .='   AUFTRAGSDATUM AUFTRAGSDATUM,';
        $SQLWV .='   WANR WANR,';
        $SQLWV .='   BONNR BONNR,';
        $SQLWV .='   KUNDENNAME KUNDENNAME,';
        $SQLWV .='   VORNAME VORNAME,';
        $SQLWV .='   TYP TYP,';
        $SQLWV .='   AUFTRAGSART_ATU AUFTRAGSART_ATU,';
        $SQLWV .='   AUFTRAGSART_ATU_NEU AUFTRAGSART_ATU_NEU,';
        $SQLWV .='   EINGABEAM EINGABEAM,';
        $SQLWV .='   KENNUNG KENNUNG,';
        $SQLWV .='   BEZEICHNUNG BEZEICHNUNG,';
        $SQLWV .='   ANWALTSSACHE ANWALTSSACHE,';
        $SQLWV .='   STAND STAND,';
        $SQLWV .='   BID BID,';
        $SQLWV .='   VORGANGSNR VORGANGSNR,';
        $SQLWV .='   ANTRAGART ANTRAGART,';
        $SQLWV .='   EINGABEDURCH EINGABEDURCH,';
        $SQLWV .='   SCHADENSGRUND SCHADENSGRUND,';
        $SQLWV .='   GROSSKDNR GROSSKDNR,';
        $SQLWV .='   GEWICHTUNG GEWICHTUNG,';
        $SQLWV .='   ZUSATZINFO ZUSATZINFO,';
        $SQLWV .='   BEMINTERN BEMINTERN,';
        $SQLWV .='   WERT';
        $SQLWV .= ' ,row_number() OVER ( ';
        $SQLWV .= ' order by ' . $Param['ORDERWV'];
        $SQLWV .= ') AS ZeilenNr from( ';
        $SQLWV .='SELECT';
        $SQLWV .='   WV.WV_ID WV_ID,';
        $SQLWV .='   WV.WV_DATUM WV_DATUM,';
        $SQLWV .='   WV.WV_GRUND WV_GRUND,';
        $SQLWV .='   SB.ID ID,';
        $SQLWV .='   SCHAD.ART ART,';
        $SQLWV .='   SCHAD.BEARBNRNEU BEARBNRNEU,';
        $SQLWV .='   SCHAD.BEARBEITUNGSNR BEARBEITUNGSNR,';
        $SQLWV .='   SCHAD.BEARBEITUNGSNRFIL BEARBEITUNGSNRFIL,';
        $SQLWV .='   SCHAD.SCHADENSJAHR SCHADENSJAHR,';
        $SQLWV .='   SCHAD.FILNR FILNR,';
        $SQLWV .='   SCHAD.LFDNR LFDNR,';
        $SQLWV .='   SCHAD.AUFTRAGSDATUM AUFTRAGSDATUM,';
        $SQLWV .='   SCHAD.WANR WANR,';
        $SQLWV .='   SCHAD.BONNR BONNR,';
        $SQLWV .='   SCHAD.KUNDENNAME KUNDENNAME,';
        $SQLWV .='   SCHAD.VORNAME VORNAME,';
        $SQLWV .='   SCHAD.TYP TYP,';
        $SQLWV .='   SCHAD.AUFTRAGSART_ATU AUFTRAGSART_ATU,';
        $SQLWV .='   SCHAD.AUFTRAGSART_ATU_NEU AUFTRAGSART_ATU_NEU,';
        $SQLWV .='   SCHAD.EINGABEAM EINGABEAM,';
        $SQLWV .='   SCHAD.KENNUNG KENNUNG,';
        $SQLWV .='   SCHAD.BEZEICHNUNG BEZEICHNUNG,';
        $SQLWV .='   SCHAD.ANWALTSSACHE ANWALTSSACHE,';
        $SQLWV .='   SCHAD.STAND STAND,';
        $SQLWV .='   SCHAD.BID BID,';
        $SQLWV .='   SCHAD.VORGANGSNR VORGANGSNR,';
        $SQLWV .='   SCHAD.ANTRAGART ANTRAGART,';
        $SQLWV .='   SCHAD.EINGABEDURCH EINGABEDURCH,';
        $SQLWV .='   SCHAD.SCHADENSGRUND SCHADENSGRUND,';
        $SQLWV .='   SCHAD.GROSSKDNR GROSSKDNR,';
        $SQLWV .='   SCHAD.GEWICHTUNG GEWICHTUNG,';
        $SQLWV .='   SCHAD.ZUSATZINFO ZUSATZINFO,';
        $SQLWV .='   SCHAD.BEMINTERN BEMINTERN,';
        $SQLWV .='   AA.WERT';
        $SQLWV .=' FROM';
        $SQLWV .='   WIEDERVORLAGEN_NEW WV';
        $SQLWV .=' LEFT JOIN SACHBEARBEITER SB';
        $SQLWV .=' ON';
        $SQLWV .='   WV.WV_AN = SB.SBNAME';
        $SQLWV .=' INNER JOIN SCHAEDEN_NEW SCHAD';
        $SQLWV .=' ON';
        $SQLWV .='   SCHAD.BEARBEITUNGSNR = WV.BEARBEITUNGSNR';
        $SQLWV .=' LEFT JOIN ANTRAGART AA';
        $SQLWV .=' ON';
        $SQLWV .='   SCHAD.ANTRAGART = AA.ID';
        $SQLWV .=' WHERE';
        $SQLWV .='   SB.ID = '.$DB->WertSetzen('SBV', 'N0', $Wert);
        $SQLWV .=')';
        $Form->DebugAusgabe(1,$SQLWV);
         
        $rsVorgaenge = $DBSCHAD->RecordSetOeffnen($SQLWV,$DB->Bindevariablen('SBV',true));
    
        $Form->DebugAusgabe(1,$SQLWV);
       
        $Fehler = '';
        if($rsVorgaenge->AnzahlDatensaetze() == 0)
        {

            $Form->Erstelle_TextLabel('keine Vorg�nge mit einer WV f�r den User gefunden.', 500,'font-weight:bolder');
        }
        elseif ($Wert!='')
        {
            $FeldBreiten = array();
            $FeldBreiten['BEARBEITUNG'] = 120;
            $FeldBreiten['EINGABEAM'] = 90;
            $FeldBreiten['AART'] = 150;
            $FeldBreiten['KUNDE'] = 200;
            //$FeldBreiten['SCHADBESCH'] = 50;
            $FeldBreiten['WVGRUND'] = 405;
            $FeldBreiten['FILNR'] = 50;
            $FeldBreiten['DATUM'] = 110;
            $FeldBreiten['ICON'] = 15;
            
            if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
            {
            	$FeldBreiten['ICON'] += 5;
            }
           
            
            $count = 0;
            $Gesamtbreite = '';
            foreach ($FeldBreiten as $value)
            {
            	$Gesamtbreite += $value;
            	$count++;
            }
            if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
            {
            	$Gesamtbreite += ($count * 2.4);
            }
             
            //Alle Checkboxen in eine Variable, damit man diese dann alle Anhaken kann.
            $CheckBoxen = '';
            while(! $rsVorgaenge->EOF())
            {
                $CheckBoxen .= 'WV_'.$rsVorgaenge->FeldInhalt('WV_ID') .'|';
                $rsVorgaenge->DSWeiter();
            }
            $rsVorgaenge->DSErster(); 
             
            $Form->ZeileStart();
            // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
            //$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
            // �berschrift der Listenansicht mit Sortierungslink: Filial;
            $Form->Erstelle_Checkbox('checkAlle', false, $FeldBreiten['ICON'],true,true,'','','onClick="CheckAlle(\''.$CheckBoxen.'\')"');
            
            $Link = './aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung&SortWV=BEARBEITUNGSNR'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='BEARBEITUNGSNR'))?'~':'').'&Inhaber='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEARB'], $FeldBreiten['BEARBEITUNG'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung&SortWV=EINGABEAM'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='EINGABEAM'))?'~':'').'&Inhaber='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_EINGABEAM'], $FeldBreiten['EINGABEAM'], '', $Link);
            $Link = './aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung&SortWV=WERT'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='WERT'))?'~':'').'&Inhaber='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_AART'], $FeldBreiten['AART'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung&SortWV=FILNR'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='FILNR'))?'~':'').'&Inhaber='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_FILNR'], $FeldBreiten['FILNR'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
            $Link = './aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung&SortWV=KUNDENNAME'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='KUNDENNAME'))?'~':'').'&Inhaber='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KUNDE'], $FeldBreiten['KUNDE'], '', $Link);
            $Link = './aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung&SortWV=WV_DATUM'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='WV_DATUM'))?'~':'').'&Inhaber='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_WVDATUM'], $FeldBreiten['DATUM'], '', $Link);
            $Link = './aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung&SortWV=WV_GRUND'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='WV_GRUND'))?'~':'').'&Inhaber='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_WVGRUND'], $FeldBreiten['WVGRUND'] + ($rsVorgaenge->AnzahlDatensaetze() > 11?28:0), '', $Link);
             
            $Form->ZeileEnde();
            
            
            if($rsVorgaenge->AnzahlDatensaetze() > 11)
            {
            	//$Form->ScrollBereichStart('scrKomVert', 200,1202,true,false);
            	$Form->SchreibeHTMLCode('<div id=scrKomVert style="width: 1202px; height: 200px; overflow-y: scroll;">');	
            }
  
          
            while(! $rsVorgaenge->EOF())
            {
                $Form->ZeileStart();
                	
                	
                if($rsVorgaenge->FeldInhalt('AKTEGESCHLOSSENAM') <> '')
                {
                    $Hintergrund = 3;
                }
                else
                {
                    $Hintergrund = ($DS%2);
                }
                $Form->Erstelle_Checkbox('WV_'.$rsVorgaenge->FeldInhalt('WV_ID').'', isset($_POST['txtWV_'.$rsVorgaenge->FeldInhalt('WV_ID')]), 20,true,true,'','');
                $TTT = $rsVorgaenge->FeldInhalt('BEARBEITUNGSNR');
                
                $Form->Erstelle_ListenFeld('BES_BEARBEITUNG', $rsVorgaenge->FeldInhalt('BEARBEITUNGSNR'), 0, $FeldBreiten['BEARBEITUNG'], false, $Hintergrund, '','', 'T', 'L', $TTT);
                
                $TTT =  $Form->Format('D',$rsVorgaenge->FeldInhalt('EINGABEAM'));
                $Form->Erstelle_ListenFeld('BES_EINGABEAM',$Form->Format('D',$rsVorgaenge->FeldInhalt('EINGABEAM')), 0, $FeldBreiten['EINGABEAM'], false, $Hintergrund, '','', 'T', 'L', $TTT);
                
                $TTT =  $rsVorgaenge->FeldInhalt('WERT');
                $Form->Erstelle_ListenFeld('BES_ART',$rsVorgaenge->FeldInhalt('WERT'), 0, $FeldBreiten['AART'], false, $Hintergrund, '','', 'T', 'L', $TTT);
                	
                $TTT =  $rsVorgaenge->FeldInhalt('FILNR');
                $Form->Erstelle_ListenFeld('BES_FILNR',$rsVorgaenge->FeldInhalt('FILNR'), 0, $FeldBreiten['FILNR'], false, $Hintergrund, '','', 'T', 'L', $TTT);
    
                $TTT = $rsVorgaenge->FeldInhalt('KUNDENNAME').' '.$rsVorgaenge->FeldInhalt('VORNAME');
                $Form->Erstelle_ListenFeld('BES_KUNDE',$rsVorgaenge->FeldInhalt('KUNDENNAME').' '.$rsVorgaenge->FeldInhalt('VORNAME'), 0, $FeldBreiten['KUNDE'], false, $Hintergrund,'','', 'T', 'L', $TTT);
    
                $TTT = $rsVorgaenge->FeldInhalt('WV_DATUM');
                $Form->Erstelle_ListenFeld('BES_WVDATUM',$Form->Format('D',$rsVorgaenge->FeldInhalt('WV_DATUM')), 0, $FeldBreiten['DATUM'], false, ($DS%2), '','', 'T', 'L', $TTT);
    
                $TTT =  $rsVorgaenge->FeldInhalt('WV_GRUND');
                $Form->Erstelle_ListenFeld('BES_WVGRUND',strlen($rsVorgaenge->FeldInhalt('WV_GRUND')) > 70 ? substr($rsVorgaenge->FeldInhalt('WV_GRUND'),0,69).'...':$rsVorgaenge->FeldInhalt('WV_GRUND'), 0, $FeldBreiten['WVGRUND'], false, ($DS%2), '','', 'T', 'L', $TTT);
    
                	
                $Form->ZeileEnde();
                $DS++;
                $rsVorgaenge->DSWeiter();
            }
            
           	if($rsVorgaenge->AnzahlDatensaetze() > 11)
            {
            	//$Form->ScrollBereichEnde();
            	$Form->SchreibeHTMLCode('</div>') ;
           	}
            
            
            
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].': '.$rsVorgaenge->AnzahlDatensaetze(), $Gesamtbreite+5+ ($rsVorgaenge->AnzahlDatensaetze() > 11?28:0), 'font-weight:bolder;');
            //$Link = './versarten_Main.php?cmdAktion=Details';
            //$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
            $Form->ZeileEnde();

            
            $Form->ZeileStart();
            $SQLWVWeiterAn  ='SELECT';
            $SQLWVWeiterAn .='   ID ,';
            $SQLWVWeiterAn .='   SBNAME  ||\' \'  || SBVORNAME  ||\'(\'  ||SBKUERZEL  ||\')\'';
            $SQLWVWeiterAn .=' FROM';
            $SQLWVWeiterAn .='   SACHBEARBEITER';
            $SQLWVWeiterAn .=' WHERE';
            $SQLWVWeiterAn .='   AUSGESCHIEDEN = 0 order by 2';
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SBV']['SBV_UMSCHREIBEN_AUF'].': ', 280, '');
    
            $Form->Erstelle_SelectFeld('SBV_WV_WEITER_AN', ((!$SpeichernOK and isset($_POST['txtSBV_WV_WEITER_AN']))?$_POST['txtSBV_WV_WEITER_AN']:''), 100,true,$SQLWVWeiterAn,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'',null,'','',array(),'','SCHAD');
    
            $Form->ZeileEnde();
        }
    
    }
    catch (awisException $ex)
    {
        if($Form instanceof awisFormular)
        {
            $Form->DebugAusgabe(1, $ex->getSQL());
            $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
            echo $DB->LetzterSQL();
        }
        else
        {
            echo 'AWIS-Fehler:'.$ex->getMessage();
            echo $DB->LetzterSQL();
        }
    }
    catch (Exception $ex)
    {
        if($Form instanceof awisFormular)
        {
    
            $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
            echo $DB->LetzterSQL();
        }
        else
        {
            echo 'allg. Fehler:'.$ex->getMessage();
            echo $DB->LetzterSQL();
        }
    }
}


/**
 *  Erstellt das Ergebnis des Ajax f�r die Vorgangsverteilung 
 * 
 * @param $Wert integer Sachbearbeiter ID, des zu Vertretenden 
 */
function erstelleAjaxOhneWV($Wert)
{
    global $AWISCursorPosition;
    global $AWISBenutzer;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $SpeichernOK;
    global $Recht40016;
    global $DatenSB;
    global $SQLZuVertreten;
    try
    {
        $DS=0;

        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();

        $DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
        $DBSCHAD->Oeffnen();


        $AWISBenutzer = awisBenutzer::Init();
        $Form = new awisFormular();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[]=array('BES','%');
        $TextKonserven[]=array('SBV','%');
        $TextKonserven[]=array('Wort','lbl_speichern');
        $TextKonserven[]=array('Wort','lbl_suche');
        $TextKonserven[]=array('Wort','AuswahlSpeichern');
        $TextKonserven[]=array('Wort','lbl_hinzufuegen');
        $TextKonserven[]=array('Wort','txt_BitteWaehlen');
        $TextKonserven[]=array('Wort','lbl_trefferliste');
        $TextKonserven[]=array('Wort','wrd_Filiale');
        $TextKonserven[]=array('Fehler','err_keineDaten');
        $TextKonserven[]=array('Liste','lst_OffenMass');
        $TextKonserven[]=array('Wort','PDFErzeugen');
        $TextKonserven[]=array('Wort','DatumVom');
        $TextKonserven[]=array('Wort','Datum');
        $TextKonserven[]=array('FIL','FIL_GEBIET');
        $TextKonserven[]=array('Liste','lst_ALLE_0');
        $TextKonserven[]=array('Liste','lst_JaNein');
        $TextKonserven[]=array('Fehler','err_keineDaten');


        $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
        $Param = array();
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SBV'));

        if(!isset($Param['ORDERoWV']) or $Param['ORDERoWV'] == '')
        {
            $Param['ORDERoWV'] = 'AUFTRAGSDATUM';
        }

        if (isset($_GET['SortOWV']) and $_GET['SortOWV'] != '')
        {		// wenn GET-Sort, dann nach diesen Feld sortieren
            $Param['ORDERoWV'] = str_replace('~',' DESC ', $_GET['SortOWV']);
        }
        
        $SQLoVW  ='SELECT';
        $SQLoVW .='   ART ,';
        $SQLoVW .='   BEARBNRNEU ,';
        $SQLoVW .='   BEARBEITUNGSNR ,';
        $SQLoVW .='   BEARBEITUNGSNRFIL ,';
        $SQLoVW .='   SCHADENSJAHR ,';
        $SQLoVW .='   FILNR ,';
        $SQLoVW .='   LFDNR ,';
        $SQLoVW .='   AUFTRAGSDATUM ,';
        $SQLoVW .='   WANR ,';
        $SQLoVW .='   BONNR ,';
        $SQLoVW .='   DATUMREGULIERUNG ,';
        $SQLoVW .='   BONNRREGULIERUNG ,';
        $SQLoVW .='   VERK�UFER ,';
        $SQLoVW .='   DATUMEREIGNIS ,';
        $SQLoVW .='   BEARBEITER ,';
        $SQLoVW .='   REKLASCHEINNR ,';
        $SQLoVW .='   REKLASCHEINVAX ,';
        $SQLoVW .='   GESCHLECHT ,';
        $SQLoVW .='   ANREDE ,';
        $SQLoVW .='   KUNDENNAME ,';
        $SQLoVW .='   VORNAME ,';
        $SQLoVW .='   STRASSE ,';
        $SQLoVW .='   PLZ ,';
        $SQLoVW .='   ORT ,';
        $SQLoVW .='   TELEFON ,';
        $SQLoVW .='   KFZ_KENNZ ,';
        $SQLoVW .='   ERSTZULASSUNG ,';
        $SQLoVW .='   FABRIKAT ,';
        $SQLoVW .='   TYP ,';
        $SQLoVW .='   KM ,';
        $SQLoVW .='   AUFTRAGSART_ATU ,';
        $SQLoVW .='   AUFTRAGSART_ATU_NEU ,';
        $SQLoVW .='   FEHLERBESCHREIBUNG ,';
        $SQLoVW .='   MECHANIKER ,';
        $SQLoVW .='   DIAGNOSE_WERKSTATTLEITER ,';
        $SQLoVW .='   DIAGNOSE_TKDL ,';
        $SQLoVW .='   WEITERGELEITET_HERSTELLER ,';
        $SQLoVW .='   BEMHERSTELLER ,';
        $SQLoVW .='   WEITERGELEITETVERSICHERUNG ,';
        $SQLoVW .='   ATUNR ,';
        $SQLoVW .='   LIEFERANT ,';
        $SQLoVW .='   AKTEGESCHLOSSENAM ,';
        $SQLoVW .='   EINGABEAM ,';
        $SQLoVW .='   EINREICHER ,';
        $SQLoVW .='   ORTEREIGNIS ,';
        $SQLoVW .='   FORDERUNG ,';
        $SQLoVW .='   KENNUNG ,';
        $SQLoVW .='   BEZEICHNUNG ,';
        $SQLoVW .='   ANWALTSSACHE ,';
        $SQLoVW .='   AKTENZEICHENRA ,';
        $SQLoVW .='   RAFORDERUNG ,';
        $SQLoVW .='   AKTENZEITENGERICHT ,';
        $SQLoVW .='   STANDBEIGERICHT ,';
        $SQLoVW .='   STAND ,';
        $SQLoVW .='   BID ,';
        $SQLoVW .='   LIEFARTNR ,';
        $SQLoVW .='   VORGANGSNR ,';
        $SQLoVW .='   KAUFDATUM ,';
        $SQLoVW .='   ANTRAGART ,';
        $SQLoVW .='   EINGABEDURCH ,';
        $SQLoVW .='   KBANR ,';
        $SQLoVW .='   SCHADENSGRUND ,';
        $SQLoVW .='   VERURS_FILIALE ,';
        $SQLoVW .='   BESICHTIGT ,';
        $SQLoVW .='   TELEFON2 ,';
        $SQLoVW .='   EMAIL ,';
        $SQLoVW .='   FAX ,';
        $SQLoVW .='   PAN ,';
        $SQLoVW .='   EINGANGPER ,';
        $SQLoVW .='   TERMIN_KUNDE_1 ,';
        $SQLoVW .='   TERMIN_KUNDE_2 ,';
        $SQLoVW .='   TERMIN_KUNDE_WER_1 ,';
        $SQLoVW .='   TERMIN_KUNDE_WER_2 ,';
        $SQLoVW .='   KONTAKT_KUNDE ,';
        $SQLoVW .='   INFO_AN_GBL ,';
        $SQLoVW .='   AUSFALLURSACHE ,';
        $SQLoVW .='   EINGABEDURCH_FIL ,';
        $SQLoVW .='   EINGABEDURCH_FILIALE ,';
        $SQLoVW .='   EINGABEDURCH_FIL_DATUM ,';
        $SQLoVW .='   VORGANG_FIL_GEDRUCKT ,';
        $SQLoVW .='   EINGABEDURCH_FILPERSNR ,';
        $SQLoVW .='   GROSSKDNR ,';
        $SQLoVW .='   GEWICHTUNG ,';
        $SQLoVW .='   ZUSATZINFO ,';
        $SQLoVW .='   KFZTYP ,';
        $SQLoVW .='   FGLIEFERANT ,';
        $SQLoVW .='   FGMARKE ,';
        $SQLoVW .='   BEMINTERN';
        $SQLoVW .='   WERT,aawert';
        $SQLoVW .= ' ,row_number() OVER ( ';
        $SQLoVW .= ' order by ' . $Param['ORDERoWV'];
        $SQLoVW .= ') AS ZeilenNr from( ';
        $SQLoVW .='   (';
        $SQLoVW .='     SELECT';
        $SQLoVW .='       schad.*, aa.WERT as aawert';
        $SQLoVW .='     FROM';
        $SQLoVW .='       schaeden_new schad';
        $SQLoVW .='     LEFT JOIN wiedervorlagen_new wv';
        $SQLoVW .='     ON';
        $SQLoVW .='       SCHAD.BEARBEITUNGSNR = WV.BEARBEITUNGSNR';
        $SQLoVW .=' LEFT JOIN ANTRAGART AA';
        $SQLoVW .=' ON';
        $SQLoVW .='   SCHAD.ANTRAGART = AA.ID';
        $SQLoVW .='     WHERE';
        $SQLoVW .='       WV.WV_ID        IS NULL and aktegeschlossenam is null';
        $SQLoVW .='     AND eingabeam >= \'01.01.2014\'';
        $SQLoVW .='   ))';
        $SQLoVW .=' WHERE BEARBEITER = '.$DBSCHAD->WertSetzen('SBV', 'N0', $Wert);
        
        $rsVorgaenge = $DBSCHAD->RecordSetOeffnen($SQLoVW,$DBSCHAD->Bindevariablen('SBV',true));

        $Form->DebugAusgabe(1,$SQLoVW);

        $Fehler = '';
        if($rsVorgaenge->AnzahlDatensaetze() == 0)
        {
            $Form->Erstelle_TextLabel('keine Vorg�nge ohne WV f�r den User gefunden.', 500,'font-weight:bolder');
        }
        elseif ($Wert!='')
        {
            $FeldBreiten = array();
            $FeldBreiten['BEARBEITUNG'] = 130;
            $FeldBreiten['EINGABEAM'] = 90;
            $FeldBreiten['AART'] = 230;
            $FeldBreiten['KUNDE'] = 200;
            //$FeldBreiten['SCHADBESCH'] = 50;
            //$FeldBreiten['WVGRUND'] = 405;
            $FeldBreiten['FILNR'] = 50;
            //$FeldBreiten['DATUM'] = 110;
            
            $FeldBreiten['ICON'] = 15;
            
            if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
            {
            	$FeldBreiten['ICON'] += 5;
            }
             
            
            $count = 0;
            $Gesamtbreite = '';
            foreach ($FeldBreiten as $value)
            {
            	$Gesamtbreite += $value;
            	$count++;
            }
            if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
            {
            	$Gesamtbreite += ($count * 2);
            }
            
            //Alle Checkboxen in eine Variable, damit man diese dann alle Anhaken kann.
            $CheckBoxen = '';
            while(! $rsVorgaenge->EOF())
            {
                $CheckBoxen .= 'oWV_'.$rsVorgaenge->FeldInhalt('BEARBEITUNGSNR') .'|';
                $rsVorgaenge->DSWeiter();
            }
            $rsVorgaenge->DSErster();
             
           
            
            $Form->ZeileStart();
            // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
            //$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
            // �berschrift der Listenansicht mit Sortierungslink: Filial;
            $Form->Erstelle_Checkbox('checkAlle', false, $FeldBreiten['ICON'],true,true,'','','onClick="CheckAlle(\''.$CheckBoxen.'\')"');
            
            //$Form->Erstelle_Liste_Ueberschrift('', 15);
            $Link = './aufgabensteuerung_Main.php?cmdAktion=VorgangsVertretung&SortOWV=BEARBEITUNGSNR'.((isset($_GET['SortOWV']) AND ($_GET['SortOWV']=='BEARBEITUNGSNR'))?'~':'').'&InhaberOWV='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEARB'], $FeldBreiten['BEARBEITUNG'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './aufgabensteuerung_Main.php?cmdAktion=VorgangsVertretung&SortOWV=EINGABEAM'.((isset($_GET['SortWV']) AND ($_GET['SortWV']=='EINGABEAM'))?'~':'').'&InhaberOWV='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_EINGABEAM'], $FeldBreiten['EINGABEAM'], '', $Link);
            $Link = './aufgabensteuerung_Main.php?cmdAktion=VorgangsVertretung&SortOWV=AAWERT'.((isset($_GET['SortOWV']) AND ($_GET['SortOWV']=='AAWERT'))?'~':'').'&InhaberOWV='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_AART'], $FeldBreiten['AART'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './aufgabensteuerung_Main.php?cmdAktion=VorgangsVertretung&SortOWV=FILNR'.((isset($_GET['SortOWV']) AND ($_GET['SortOWV']=='FILNR'))?'~':'').'&InhaberOWV='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_FILNR'], $FeldBreiten['FILNR'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
            $Link = './aufgabensteuerung_Main.php?cmdAktion=VorgangsVertretung&SortOWV=KUNDENNAME'.((isset($_GET['SortOWV']) AND ($_GET['SortOWV']=='KUNDENNAME'))?'~':'').'&InhaberOWV='.$Wert;
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KUNDE'], $FeldBreiten['KUNDE']+ ($rsVorgaenge->AnzahlDatensaetze() > 11?25:0), '', $Link);
            
            $Form->ZeileEnde();
            
            $Form->ZeileStart();
            
            if($rsVorgaenge->AnzahlDatensaetze() > 11)
            {
            	$Form->SchreibeHTMLCode('<div id=scrKomVert style="width: 765px; height: 200px;  overflow-y: scroll;">');
            }
            
            
            
            while(! $rsVorgaenge->EOF())
            {
                $Form->ZeileStart();
                 
                if($rsVorgaenge->FeldInhalt('AKTEGESCHLOSSENAM') <> '')
                {
                    $Hintergrund = 3;
                }
                else
                {
                    $Hintergrund = ($DS%2);
                }
                 
                $Form->Erstelle_Checkbox('oWV_'.$rsVorgaenge->FeldInhalt('BEARBEITUNGSNR'), isset($_POST['txtoWV_'.$rsVorgaenge->FeldInhalt('BEARBEITUNGSNR')]), 20,true,true);
                $TTT = $rsVorgaenge->FeldInhalt('BEARBEITUNGSNR');
                $Form->Erstelle_ListenFeld('BES_BEARBEITUNG', $rsVorgaenge->FeldInhalt('BEARBEITUNGSNR'), 0, $FeldBreiten['BEARBEITUNG'], false, $Hintergrund, '','', 'T', 'L', $TTT);
                
                $TTT =  $Form->Format('D',$rsVorgaenge->FeldInhalt('EINGABEAM'));
                $Form->Erstelle_ListenFeld('BES_EINGABEAM',$Form->Format('D',$rsVorgaenge->FeldInhalt('EINGABEAM')), 0, $FeldBreiten['EINGABEAM'], false, $Hintergrund, '','', 'T', 'L', $TTT);
                
                
                $TTT =  $rsVorgaenge->FeldInhalt('AAWERT');
                $Form->Erstelle_ListenFeld('BES_ART',$rsVorgaenge->FeldInhalt('AAWERT'), 0, $FeldBreiten['AART'], false, $Hintergrund, '','', 'T', 'L', $TTT);
                 
                $TTT =  $rsVorgaenge->FeldInhalt('FILNR');
                $Form->Erstelle_ListenFeld('BES_FILNR',$rsVorgaenge->FeldInhalt('FILNR'), 0, $FeldBreiten['FILNR'], false, $Hintergrund, '','', 'T', 'L', $TTT);

                $TTT = $rsVorgaenge->FeldInhalt('KUNDENNAME').' '.$rsVorgaenge->FeldInhalt('VORNAME');
                $Form->Erstelle_ListenFeld('BES_KUNDE',$rsVorgaenge->FeldInhalt('KUNDENNAME').' '.$rsVorgaenge->FeldInhalt('VORNAME'), 0, $FeldBreiten['KUNDE'], false, $Hintergrund,'','', 'T', 'L', $TTT);

                
                $Form->ZeileEnde();
                $DS++;
                $rsVorgaenge->DSWeiter();
            }
            
            
            
            if($rsVorgaenge->AnzahlDatensaetze() > 11)
            {
            	$Form->SchreibeHTMLCode('</div>') ;
            }
            
            $Form->ZeileEnde();
            
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].': '.$rsVorgaenge->AnzahlDatensaetze(), $Gesamtbreite+5+($rsVorgaenge->AnzahlDatensaetze() > 11?25:0), 'font-weight:bolder;');
            //$Link = './versarten_Main.php?cmdAktion=Details';
            //$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
            $Form->ZeileEnde();

           
            $Form->ZeileStart();
            $SQLWVWeiterAn  ='SELECT';
            $SQLWVWeiterAn .='   ID ,';
            $SQLWVWeiterAn .='   SBNAME  ||\' \'  || SBVORNAME  ||\'(\'  ||SBKUERZEL  ||\')\'';
            $SQLWVWeiterAn .=' FROM';
            $SQLWVWeiterAn .='   SACHBEARBEITER';
            $SQLWVWeiterAn .=' WHERE';
            $SQLWVWeiterAn .='   AUSGESCHIEDEN = 0 ';
            
            $Recht40016 = $AWISBenutzer->HatDasRecht(40016);
            
            if (($Recht40016&32) != 32)
			{
				$SQLWVWeiterAn .= ' and ID '.$DBSCHAD->LikeOderIst($Wert);
			}
            $SQLWVWeiterAn .= ' order by 2';
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SBV']['SBV_UMSCHREIBEN_AUF'].': ', 280, '');
			
            $Form->Erstelle_SelectFeld('SBV_oWV_WEITER_AN', ((!$SpeichernOK and isset($_POST['txtSBV_WV_WEITER_AN']))?$_POST['txtSBV_WV_WEITER_AN']:''), 100,true,$SQLWVWeiterAn,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'',null,'','',array(),'','SCHAD');
			
            $Form->ZeileEnde();
        }

    }
    catch (awisException $ex)
    {
        if($Form instanceof awisFormular)
        {
            $Form->DebugAusgabe(1, $ex->getSQL());
            $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
            echo $DB->LetzterSQL();
        }
        else
        {
            echo 'AWIS-Fehler:'.$ex->getMessage();
            echo $DB->LetzterSQL();
        }
    }
    catch (Exception $ex)
    {
        if($Form instanceof awisFormular)
        {

            $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
            echo $DB->LetzterSQL();
        }
        else
        {
            echo 'allg. Fehler:'.$ex->getMessage();
            echo $DB->LetzterSQL();
        }
    }
}

?>