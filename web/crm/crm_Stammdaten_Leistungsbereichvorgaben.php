<?php
global $con;
global $Recht3708;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CLV','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3708 = awisBenutzerRecht($con,3708,$AWISBenutzer->BenutzerName());
if($Recht3708==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
$Bedingung = '';

//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdDSZurueck_x']))
{
	$SQL = 'SELECT CLV_KEY FROM (SELECT CLV_KEY FROM CRMLeistungsbereichvorgaben';
	$SQL .= ' WHERE  (CLV_CLB_ID || \'#\' || CLV_CFT_ID) < \''.(awis_FeldInhaltFormat('Z',$_POST['txtCLV_CLB_ID'])).'#'.(awis_FeldInhaltFormat('Z',$_POST['txtCLV_CFT_ID'])).'\'';
	$SQL .= ' ORDER BY CLV_CLB_ID DESC, CLV_CFT_ID DESC';	
	$SQL .= ') WHERE ROWNUM = 1';
	
	$rsCLV = awisOpenRecordset($con, $SQL);
	if(isset($rsCLV['CLV_KEY'][0]))
	{
		$AWIS_KEY2=$rsCLV['CLV_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCLV_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$SQL = 'SELECT CLV_KEY FROM (SELECT CLV_KEY FROM CRMLeistungsbereichvorgaben';
	$SQL .= ' WHERE  (CLV_CLB_ID || \'#\' || CLV_CFT_ID) > \''.(awis_FeldInhaltFormat('Z',$_POST['txtCLV_CLB_ID'])).'#'.(awis_FeldInhaltFormat('Z',$_POST['txtCLV_CFT_ID'])).'\'';
	$SQL .= ' ORDER BY CLV_CLB_ID ASC, CLV_CFT_ID ASC';	
	$SQL .= ') WHERE ROWNUM = 1';
	$rsCLV = awisOpenRecordset($con, $SQL);

	if(isset($rsCLV['CLV_KEY'][0]))
	{
		$AWIS_KEY2=$rsCLV['CLV_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCLV_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./crm_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./crm_speichern_stammdaten.php');
}
elseif(isset($_GET['CLV_KEY']))
{
	$Bedingung .= ' AND CLV_KEY='.awis_FeldInhaltFormat('Z',$_GET['CLV_KEY']).'';
}
elseif(isset($_POST['txtCLV_KEY']))
{
	$Bedingung .= ' AND CLV_KEY='.awis_FeldInhaltFormat('Z',$_POST['CLV_KEY']).'';
}

if($AWIS_KEY2>0)
{
	$Bedingung .= ' AND CLV_KEY='.awis_FeldInhaltFormat('Z',$AWIS_KEY2).'';
}

//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM CRMLeistungsbereichvorgaben';
$SQL .= ' INNER JOIN CRMLeistungsBereiche ON CLV_CLB_ID = CLB_ID AND CLB_STATUS = \'A\'';
$SQL .= ' INNER JOIN CRMFahrzeugTypen ON CLV_CFT_ID = CFT_ID AND CFT_STATUS = \'A\'';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY CLV_CLB_ID, CLV_CFT_ID';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

$rsCLV = awisOpenRecordset($con, $SQL);
$rsCLVZeilen = $awisRSZeilen;
awis_Debug(1,$_POST,$_GET, $SQL);
//********************************************************
// Daten anzeigen
//********************************************************
if($rsCLVZeilen==0 AND !isset($_GET['CLV_KEY']))		// Keine Meldung bei neuen Datensätzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden CRMLeistungsbereichvorgaben gefunden.</span>';
}
elseif($rsCLVZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Leistungsbereichvorgaben'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CLV_CLB_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CLV_CLB_ID'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CLV']['CLV_CLB_ID'],492,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Leistungsbereichvorgaben'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CLV_CFT_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CLV_CFT_ID'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CLV']['CLV_CFT_ID'],250,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Leistungsbereichvorgaben'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CLV_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CLV_BETRAG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CLV']['CLV_BETRAG'],90,'',$Link);
	awis_FORM_ZeileEnde();

	for($CLVZeile=0;$CLVZeile<$rsCLVZeilen;$CLVZeile++)
	{
		awis_FORM_ZeileStart();
		
		$Icons = array();
		if(($Recht3708&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Stammdaten&Seite=Leistungsbereichvorgaben&CLV_KEY='.$rsCLV['CLV_KEY'][$CLVZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CLVZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('CLV_CLB_ID',$rsCLV['CLB_BEZEICHNUNG'][$CLVZeile],0,450,false,($CLVZeile%2));
		awis_FORM_Erstelle_ListenFeld('CLV_CFT_ID',$rsCLV['CFT_BEZEICHNUNG'][$CLVZeile],0,250,false,($CLVZeile%2));
		awis_FORM_Erstelle_ListenFeld('CLV_BETRAG',$rsCLV['CLV_BETRAG'][$CLVZeile],0,90,false,($CLVZeile%2),'','','N2','R');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmCRMLeistungsbereichvorgaben action=./crm_Main.php?cmdAktion=Stammdaten&Seite=Leistungsbereichvorgaben method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsCLV['CLV_KEY'][0])?$rsCLV['CLV_KEY'][0]:0);

	echo '<input type=hidden name=txtCLV_KEY value='.$AWIS_KEY1. '>';
	echo '<input type=hidden name=txtCLV_CLB_ID value='.$rsCLV['CLV_CLB_ID'][0]. '>';
	echo '<input type=hidden name=txtCLV_CFT_ID value='.$rsCLV['CLV_CFT_ID'][0]. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Stammdaten&Seite=Leistungsbereichvorgaben&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCLV['CLV_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCLV['CLV_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$TabellenKuerzel='CLV';
	$EditRecht=(($Recht3708&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CLV']['CLV_CLB_ID'].':',150);
	awis_FORM_Erstelle_TextFeld('#CLV_CLB_ID',$rsCLV['CLB_BEZEICHNUNG'][0],50,350,false);
	$CursorFeld='txtCLV_CLB_ID';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CLV']['CLV_CFT_ID'].':',150);
	awis_FORM_Erstelle_TextFeld('#CLV_CFT_ID',$rsCLV['CFT_BEZEICHNUNG'][0],50,350,false);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CLV']['CLV_BETRAG'].':',150);
	awis_FORM_Erstelle_TextFeld('CLV_BETRAG',$rsCLV['CLV_BETRAG'][0],50,350,$EditRecht,'','','','N2');
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();

	//***************************************
	// Schaltflächen für dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3708&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>