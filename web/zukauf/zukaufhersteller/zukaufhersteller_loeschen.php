<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtZLH_KEY']))
		{
			$Tabelle = 'ZLH';
			$Key=$_POST['txtZLH_KEY'];

			$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG, ZLH_KUERZEL';
			$SQL .= ' FROM ZukauflieferantenHersteller ';
			$SQL .= ' WHERE ZLH_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$ZLHKey = $rsDaten->FeldInhalt('ZLH_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('ZLH','ZLH_BEZEICHNUNG'),$rsDaten->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Felder[]=array($Form->LadeTextBaustein('ZLH','ZLH_KUERZEL'),$rsDaten->FeldInhalt('ZLH_KUERZEL'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				case 'Lieferanten':
					$Tabelle = 'ZHK';
					$Key=$_GET['Del'];
					$ZLHKey=$_GET['ZLH_KEY'];

					$SQL = 'SELECT ZHK_KEY, ZHK_LIE_NR, ZHK_CODE';
					$SQL .= ' FROM Zukaufherstellercodes ';
					$SQL .= ' WHERE ZHK_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('ZHK','ZHK_LIE_NR'),$rsDaten->FeldInhalt('ZHK_LIE_NR'));
					$Felder[]=array($Form->LadeTextBaustein('ZHK','ZHK_CODE'),$rsDaten->FeldInhalt('ZHK_CODE'));
					break;
				case 'Zuordnungen':
					$Tabelle = 'ZHZ';
					$Key=$_GET['Del'];
					$ZLHKey=$_GET['ZLH_KEY'];

					$SQL = 'SELECT ZHZ_KEY, ZHZ_LIE_NR';
					$SQL .= ' FROM ZUKAUFHERSTELLERZUORDNUNGEN ';
					$SQL .= ' WHERE ZHZ_KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('ZHZ','ZHZ_LIE_NR'),$rsDaten->FeldInhalt('ZHZ_LIE_NR'));
					break;
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'ZLH':
				$SQL = 'DELETE FROM Zukauflieferantenhersteller WHERE ZLH_key=0'.$_POST['txtKey'];
				$AWIS_KEY1='';
				break;
			case 'ZHK':
				$SQL = 'DELETE FROM Zukaufherstellercodes WHERE ZHK_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtZLHKey'];
				break;
			case 'ZHZ':
				$SQL = 'DELETE FROM ZUKAUFHERSTELLERZUORDNUNGEN WHERE ZHZ_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtZLHKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('zukaufhersteller_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./zukaufhersteller_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('ZLHKey',$ZLHKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>