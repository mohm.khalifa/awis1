<html>
<head>
<link rel="SHORTCUT ICON" href="http://atlx22su81/favicon.ico">
<title>Redaktionssystem</title>
</head>

<body>
<?php
require_once("db.inc.php");		// DB-Befehle
require_once("register.inc.php");
require_once("sicherheit.inc.php");
require_once("awisFormular.inc");
global $AWISBenutzer;
								// Benutzerdefinierte CSS Datei
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";


include ("ATU_Header.php");	// Kopfzeile
$con = awisLogon();

print "<table border=0 width=100%><tr><td><h1 id=SeitenTitel>Redaktionssystem</h1></td><td align=right>Anmeldename: " . $AWISBenutzer->BenutzerName() . "</td></tr></table>";
// ?berschrift der Tabelle
print "<table border=0 width=100%><tr><th align=left id=FeldBez>Aktion</th><th align=left id=FeldBez>Beschreibung</th></tr>";
$RechteStufe = awisBenutzerRecht($con,803);
if(($RechteStufe&2)==2)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_pflege&form=auftrag_freigabe>Auftrag_Freigabe</a></td><td>Freigabe von Auftr?gen.</td></tr>";
}
elseif(($RechteStufe&1)==1)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_sel&form=auftrag_freigabe>Auftrag_Freigabe</a></td><td>Freigabe von Auftr?gen.</td></tr>";
}

$RechteStufe = awisBenutzerRecht($con,802);
if(($RechteStufe&2)==2)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_pflege&form=berechtigungspflege>Berechtigungspflege</a></td><td>Pflege der Berechtigungen / Hinweise in den Filialsystemen.</td></tr>";
}
elseif(($RechteStufe&1)==1)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_sel&form=berechtigungspflege>Berechtigungspflege</a></td><td>Berechtigungen / Hinweise in den Filialsystemen anzeigen.</td></tr>";
}

$RechteStufe = awisBenutzerRecht($con,804);
if(($RechteStufe&2)==2)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_pflege&form=cls_berechtigung>CLS_Berechtigung</a></td><td>Berechtigungspflege ATU-Card Kunden.</td></tr>";
}
elseif(($RechteStufe&1)==1)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_sel&form=cls_berechtigung>CLS_Berechtigung</a></td><td>Berechtigungen der ATU-Card Kunden anzeigen.</td></tr>";
}

$RechteStufe = awisBenutzerRecht($con,801);
if(($RechteStufe&2)==2)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_pflege&form=grosskunde>Gro�kundenstammpflege</a></td><td>Pflege der Gro�kunden in den Filialsystemen.</td></tr>";
}
elseif(($RechteStufe&1)==1)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=atucard_sel&form=grosskunde>Gro�kundenstammpflege</a></td><td>Gro�kunden in den Filialsystemen anzeigen.</td></tr>";
}

if(awisDBServer()=='AWIS3')
{
$RechteStufe = awisBenutzerRecht($con,805);
if(($RechteStufe&1)==1)
{
	print "<tr><td><a href=./grosskundenkarten.php>Gro�kundenkarten</a></t><td>Erzeugen der Druck-Datei f&uuml;r Grosskundenkarten.</td></tr>";
}
}

$RechteStufe = awisBenutzerRecht($con,806);
if($RechteStufe>0)
{
	print "<tr><td><a href=http://filsysrac1.filsys.atu.de:7782/forms90/f90servlet?config=rechdup>Rechnungsduplikate</a></td><td>Erstellen von Rechnungsduplikaten.</td></tr>";
}
print "</table>";	//Ende Hauptmen?
print "<br><hr>";
// print "Achtung! Nach dem Umzug auf den neuen Server m&uuml;ssen evtl. vorhandene Short-Cuts angepasst werden!";

print "<br><input type=image alt=Zur?ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";

?>

</body>
</html>

