<?php
/**
 * Formular: 07 - Softwareanforderung
 * @author Sacha Kerres
 *
 */

$TextKonserven = array();
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'TTT_*');
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'txt_*');
$TextFormular = $Form->LadeTexte($TextKonserven);
$ToolTipps = $Form->LadeTexte($TextKonserven, $AWISBenutzer->BenutzerSprache());


//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

$SQL = ' select ADE_MAIL, ADE_SURNAME || COALESCE(\', \'||ADE_GIVENNAME,\'\') AS KON_NAME from ACTIVEDIRECTORYEXPORT ';

$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_benachrichtigungan'], 299,'','','');
$Form->Erstelle_MehrfachSelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn',isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn']:array(),'450:450',true,$SQL,'','','','','');
$Form->ZeileEnde();


//**************************************************
// Titel
//**************************************************
$Form->ZeileStart();
$Form->Erstelle_TextLabel('Titel', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_TITEL']);
$Form->Erstelle_TextFeld('!Titel', (isset($_POST['txtTitel'])?$_POST['txtTitel']:''), 65, 650, true,'','','','T','L','','',200,'','','');
$Form->ZeileEnde();

//**************************************************
// 
//**************************************************
$Form->ZeileStart('','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_istzustand'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_IST']);
$Form->Erstelle_Textarea('IstZustand', (isset($_POST['txtIstZustand'])?$_POST['txtIstZustand']:''), 650, 65, 5, true, '', '', '', '');
$Form->ZeileEnde();
$Form->ZeileStart('','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_sollzustand'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_SOLL']);
$Form->Erstelle_Textarea('!SollZustand', (isset($_POST['txtSollZustand'])?$_POST['txtSollZustand']:''), 650, 65, 5, true, '', '', '', '');
$Form->ZeileEnde();
$Form->ZeileStart('','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_loesungsvorschlag'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_NUTZEN']);
$Form->Erstelle_Textarea('Loesungsvorschlag', (isset($_POST['txtLoesungsvorschlag'])?$_POST['txtLoesungsvorschlag']:''), 650, 65, 5, true, '', '', '', '');
$Form->ZeileEnde();
$Form->ZeileStart('','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_nutzen_vorteil'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_NUTZEN']);
$Form->Erstelle_Textarea('NutzenVorteil', (isset($_POST['txtNutzenVorteil'])?$_POST['txtNutzenVorteil']:''), 650, 65, 5, true, '', '', '', '');
$Form->ZeileEnde();

//**************************************************
// Bemerkungen
//**************************************************
$Block='900';

$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_allgbemerkungen'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_BEMERKUNGEN_ALLG']);
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']:''), 600, 60, 3, true);
$Form->ZeileEnde();
?>