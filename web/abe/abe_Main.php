<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <?php
    global $REX;
    $Version = 3;
    require_once 'abe_funktionen.inc';
    $ABE = new abe_funktionen();
    echo "<link rel=stylesheet type=text/css href=" . $ABE->AWISBenutzer->CSSDatei($Version) . ">";
    echo '<title>' . $ABE->AWISSprachKonserven['TITEL']['tit_ABE'] . '</title>';
    ?>
</head>
<body>
<?php
if(isset($_POST['cmdPDF_x'])){
    $_GET['XRE'] = 65;
    $_GET['ID'] = 'bla';

    require_once '/daten/web/berichte/drucken.php';
}
include("awisHeader$Version.inc");    // Kopfzeile
try {



    $Register = new awisRegister(51000);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    $ABE->Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($ABE->Form instanceof awisFormular) {
        $ABE->Form->DebugAusgabe(1, $ex->getSQL());
        $ABE->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {

    if ($ABE->Form instanceof awisFormular) {

        $ABE->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
</body>
</html>