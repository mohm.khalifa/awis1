<?php
/**
 * Sendet den Newsletter als E-Mail
 */
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awis_portalnewsletter.inc';

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PortalNewsletter'));

$NL = new awis_PortalNewsletter();

$SQL = 'SELECT * FROM PORTALNEWSLETTER';
$SQL .= ' WHERE PNL_STATUS IN (3,5)';           // 3 = Test, 5=Prodiktiv
$SQL .= ' ORDER BY PNL_KEY';
$rsPNL = $DB->RecordSetOeffnen($SQL);

while(!$rsPNL->EOF())
{
    file_put_contents('/var/log/portalnewsletter.log', '******************************'.PHP_EOL, FILE_APPEND);
    file_put_contents('/var/log/portalnewsletter.log', 'Portalnewsletter versenden iim Modus '.$rsPNL->FeldInhalt('PNL_STATUS').PHP_EOL, FILE_APPEND);
    file_put_contents('/var/log/portalnewsletter.log', 'Zeitpunkt: '.date('c').PHP_EOL, FILE_APPEND);
    file_put_contents('/var/log/portalnewsletter.log', '******************************'.PHP_EOL, FILE_APPEND);

    $Daten = $NL->NewsletterText($rsPNL->FeldInhalt('PNL_KEY'));

    $EMail = new awisWerkzeuge();
    $Empfaenger = array();
    if($rsPNL->FeldInhalt('PNL_STATUS')==3)         // TEST
    {
        $Empfaenger[] = 'shuttle@de.atu.eu';
    }
    else
    {
        // Empf�nger aus der Datenbank ziehen
        $Empfaenger[] = 'shuttle@de.atu.eu';
    }

    $Erg = $EMail->EMail('noreply@atu-portal.de', 'ATU Newsletter', utf8_encode($Daten), 2, 'noreply@atu-portal.de', 'noreply@atu-portal.de', 1, null,$Empfaenger, 2, array('server'=>'mail.ads.atu.de','user'=>base64_encode('awis_jobs'),'kennwort'=>base64_encode('')));
    file_put_contents('/var/log/portalnewsletter.log', $Erg.PHP_EOL, FILE_APPEND);

    if($rsPNL->FeldInhalt('PNL_STATUS')==3)         // TEST
    {
        $SQL = 'UPDATE PORTALNEWSLETTER';
        $SQL .= ' SET PNL_STATUS = 1';
        $SQL .= ' WHERE PNL_KEY = 0'.$rsPNL->FeldInhalt('PNL_KEY');

        $DB->Ausfuehren($SQL);
    }

    $rsPNL->DSWeiter();
}
?>
