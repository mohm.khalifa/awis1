<?php

/**
 * Klasse fuer den Import von Artikel (Crossing)
 * *************************************************************************************************************
 * Parameter :
 * --- keine ---
 * R�ckgabeparameter:
 * --- keine ---
 * *************************************************************************************************************
 * Erstellt:
 * @author 23.07.2009 TR
 * @version 1.0
 * �nderungen:
 * 30.06.2011 OP @version 1.1 : Mailverteiler ge�ndert (Riedl, Beierl raus daf�r Ott und Bannert rein).
 * 19.06.2012 HO @version 1.2 : function Ausfuehren mit Paramter Exception=True ge�ndert
 * 14.04.2016 PG @version 1.3 : auf Jobliste umgebaut.. 
 * *************************************************************************************************************
*/
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awisArtikelstammImport
{
	/**
	* Datenbankverbindung
	*
	* @var awisDatenbank
	*/
	private $_DB = '';


	/**
	* Debugmodus aktiv/inaktiv
	* Hiermit werden Ausgaben fuer die Kommandozeile erzeugt.
	*
	* @var int
	*/
	private $_Debug=0;			// Debug Infos ausgeben
	
	/**
	 * DebugLevel 
	 * Hiermit werden Ausgaben fuer die Kommandozeile erzeugt.
	 *
	 * @var int
	 */
	private $_DebugLevel=0;			// Debug Infos ausgeben
	


	/**
	 * Aktueller Benutzer
	 *
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer;

	/**
	 * Empf�nger f�r Mails
	 *
	 * @var string
	 */
	
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');

	public function __construct($Benutzer, $DebugLevel)
	{
		
		if ($DebugLevel > 0)
		{
			$this->_Debug = 1;		// Debugmodus fuer Ausgaben setzen 
			$this->_DebugLevel = $DebugLevel;
		}
		
		//$this->_AWISBenutzer = new awisUser($LoginName);

		//Datenbank �ffnen
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
	    $this->_DB->Oeffnen();

	    $this->_AWISWerkzeug = new awisWerkzeuge();

	    $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
	}


	public function ImportArtikelTest()
	{
		// TODO: Konvertierung des � Zeichens in ASCII??
		$Kennung = '�'.substr($this->_AWISBenutzer->ParameterLesen('DefaultKennungAutoArtikelnr'),-1,1);
		//$Kennung = utf8_decode($this->_AWISBenutzer->ParameterLesen('DefaultKennungAutoArtikelnr'));

		$SQL = 'SELECT * FROM (SELECT AST_ATUNR FROM ARTIKELSTAMM ';
		$SQL .= ' WHERE SUBSTR(AST_ATUNR,1,'.strlen($Kennung).')=\''.$Kennung.'\'';
		$SQL .= ' ORDER BY nlssort(ast_atunr,\'NLS_SORT=BINARY\') DESC) WHERE ROWNUM = 1';
		
		$rsKey = $this->_DB->RecordSetOeffnen($SQL);				
		$rsKey = $rsKey->Tabelle();

		echo $SQL.PHP_EOL;				
		var_dump($rsKey);

		if(!isset($rsKey['AST_ATUNR'][0]) OR (isset($rsKey['AST_ATUNR'][0]) AND $rsKey['AST_ATUNR'][0]==''))
		{
			$ATUNR=$Kennung.'0001';
		}
		else
		{
			$ATUNR=++$rsKey['AST_ATUNR'][0];
		}
		
		echo $ATUNR.PHP_EOL;
	}
	
	/**
	* Funktion ImportArtikel
	*
	*/
	public function ImportArtikel()
	{
		try
		{
	    	//Pr�fen, ob Dateien was zum imporieren vorhanden ist
	    	$SQL = 'SELECT DISTINCT CIK_KEY, CIK_STATUS, CIK_IMPORTTYP FROM CROSSINGIMPORTKOPF WHERE CIK_STATUS=1 ORDER BY CIK_KEY';
	    	$rsCIDStatus = $this->_DB->RecordSetOeffnen($SQL);
			//echo $rsCIDStatus->AnzahlDatensaetze();

			//Wenn Daten vorhanden sind, kann die Zuordnung beginnen
			if ($rsCIDStatus->AnzahlDatensaetze()>0)
			{
				awisWerkzeuge::EMail($this->_EMAIL_Infos,'ARTIKELSTAMMIMPORT BEGINNT','',2,'','awis@de.atu.eu');
				
				if ($this->_DebugLevel >= 10)
				{
					echo "CIK_KEY ".$rsCIDStatus->FeldInhalt('CIK_KEY')." ist dran.." .  PHP_EOL;
				}
				
				//Status auf l�uft setzen
				$SQL = 'UPDATE CROSSINGIMPORTKOPF SET CIK_STATUS = 2 WHERE CIK_KEY = '.$rsCIDStatus->FeldInhalt('CIK_KEY');
				$this->_DB->Ausfuehren($SQL,'',true);

		    	$SQL = 'SELECT DISTINCT CID_LAR_LARTNR, CID_LIE_NR FROM CROSSINGIMPORTDATEIEN WHERE CID_CIK_KEY = '.$rsCIDStatus->FeldInhalt('CIK_KEY') . ' ORDER BY CID_LAR_LARTNR';
				$rsCID = $this->_DB->RecordSetOeffnen($SQL);

				//Alle Lieferantenartikel durchgehen und die OENR dazu suchen
				while(!$rsCID->EOF())
				{
					//Alle OENR zu diesem LARTNR suchen
					$SQL = 'SELECT DISTINCT CID_LAR_LARTNR, CID_LIE_NR, CID_OEN_OENR, CID_OEN_HER_ID, CID_ZEILE, CID_BEMERKUNG FROM CROSSINGIMPORTDATEIEN WHERE ';
					$SQL.= " CID_CIK_KEY = ".$rsCIDStatus->FeldInhalt('CIK_KEY') . " AND CID_LAR_LARTNR = '".$rsCID->Feldinhalt('CID_LAR_LARTNR') ."' AND CID_LIE_NR = '".$rsCID->Feldinhalt('CID_LIE_NR') ."' ORDER BY CID_ZEILE";

					$rsCIDOEN = $this->_DB->RecordSetOeffnen($SQL);

					//Leere OENR?????
					$Zuordnung=false;
					$ZuordnungPseudoOENR=false;
					while(!$rsCIDOEN->EOF() and $Zuordnung==false)
					{
						//Pr�fen, ob OENR in AWIS vorhanden ist (kein @ oder � Artikel)
						$SQL = ' SELECT * ';
						$SQL .= ' FROM Artikelstamm';
						$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
						$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
						$SQL .= " INNER JOIN TeileInfos ON TEI_KEY1 = AST_KEY AND TEI_ITY_ID2='OEN'";
                        $SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsCIDOEN->Feldinhalt('CID_OEN_OENR'), awisDatenbank::AWIS_LIKE_SUCHOE) . "";

						$SQL .= " AND AST_ATUNR not like '@%' and AST_ATUNR not like '�%' and AST_ATUNR not like '�%'";
																		
						$rsOEN = $this->_DB->RecordSetOeffnen($SQL);

						//Wenn OENR in AWIS bei einem Artikel vorhanden ist
						if($rsOEN->AnzahlDatensaetze()>0)
						{
							$Zuordnung = true;							
							$OEN_ASTKEY = $rsOEN->FeldInhalt('AST_KEY');
							$OEN_AST_ATUNR = $rsOEN->FeldInhalt('AST_ATUNR');
							$OEN_OENNR = $rsCIDOEN->Feldinhalt('CID_OEN_OENR');
						}
						
						$Bemerkung = $rsCIDOEN->FeldInhalt('CID_BEMERKUNG');
						$rsCIDOEN->DSWeiter();
					}

					//Wenn keine OENR bei einem Artikel hinterlegt ist (kein @ oder � Artikel)
					//Pr�fen ob OENR bei einem @ oder � Artikel hinterlegt ist
					if ($Zuordnung==false)				
					{
						$rsCIDOEN->DSErster();
						while(!$rsCIDOEN->EOF() and $Zuordnung==false)
						{						
							//Pr�fen, ob OENR in AWIS vorhanden ist 
							$SQL = ' SELECT * ';
							$SQL .= ' FROM Artikelstamm';
							$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
							$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
							$SQL .= " INNER JOIN TeileInfos ON TEI_KEY1 = AST_KEY AND TEI_ITY_ID2='OEN'";
							$SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsCIDOEN->Feldinhalt('CID_OEN_OENR'), awisDatenbank::AWIS_LIKE_SUCHOE) . "";
												
							$rsOEN = $this->_DB->RecordSetOeffnen($SQL);
	
							//Wenn OENR in AWIS bei einem Artikel vorhanden ist
							if($rsOEN->AnzahlDatensaetze()>0)
							{
								$Zuordnung = true;
								$ZuordnungPseudoOENR = true;
								$OEN_ASTKEY = $rsOEN->FeldInhalt('AST_KEY');
								$OEN_AST_ATUNR = $rsOEN->FeldInhalt('AST_ATUNR');
								$OEN_OENNR = $rsCIDOEN->Feldinhalt('CID_OEN_OENR');
							}
							
							$Bemerkung = $rsCIDOEN->FeldInhalt('CID_BEMERKUNG');
							$rsCIDOEN->DSWeiter();
						}												
					}
					
					$ZuordnungPseudo=false;
					
					//Wenn OENR in AWIS bei einem Artikel vorhanden ist
					if ($Zuordnung == true)
					{																							
						//Pr�fen, ob die einzutragende LIEFARTNR schon bei einem Artikel (keine Pseudonummer) hinterlegt ist
						$SQL = ' SELECT * ';
						$SQL .= ' FROM Artikelstamm';
						$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
						$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
						$SQL .= ' INNER JOIN TeileInfos ON (AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
                        if($rsCIDStatus->FeldInhalt('CIK_IMPORTTYP')==2){
                            $SQL .= " AND (TEI_WERT2) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR')) . ")";
                        }else{
                            $SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER) . ")";
                        }
						$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
						$SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " .$this->_DB->FeldInhaltFormat('Z',$rsCID->Feldinhalt('CID_LIE_NR')). ")";
						$SQL .= ' WHERE AST_IMQ_ID <> 2';
						$SQL .= " AND AST_ATUNR not like '@%' and AST_ATUNR not like '�%' and AST_ATUNR not like '�%'";

						$rsLAR = $this->_DB->RecordSetOeffnen($SQL);																		
						
						//Wenn LAR nicht in AWIS oder bei einem @ bzw. � Artikel bzw. Artikel mit Kennung WWS vorhanden ist
						if($rsLAR->AnzahlDatensaetze()==0)
						{
							//Pr�fen, ob die einzutragende LIEFARTNR schon bei einem Artikel (Pseudonummer) hinterlegt ist
							$SQL = ' SELECT * ';
							$SQL .= ' FROM Artikelstamm';
							$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
							$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
							$SQL .= ' INNER JOIN TeileInfos ON (AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
                            if($rsCIDStatus->FeldInhalt('CIK_IMPORTTYP')==2){
                                $SQL .= " AND (TEI_WERT2) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR')) . ")";
                            }else{
                                $SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER) . ")";
                            }
							$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
							$SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " .$this->_DB->FeldInhaltFormat('Z',$rsCID->Feldinhalt('CID_LIE_NR')). ")";
							$SQL .= ' WHERE AST_IMQ_ID <> 2';
							$SQL .= " AND (AST_ATUNR like '@%' OR AST_ATUNR like '�%'or AST_ATUNR like '�%')";
	
							$rsLARPseudo = $this->_DB->RecordSetOeffnen($SQL);
						
							if ($rsLARPseudo->AnzahlDatensaetze()>0)
							{
								//LARTNR nur bei einem Pseudoartikel zugeordnet
								$ZuordnungPseudo=true;
							}							
							
							$Meldung='';
							$LAR_AST_ATUNR='';
							$Aktion='';
							
							//OENR nur bei Pseudo zugeordnet und LARTNR bereits bei Pseudo zugeordnet
							if ($ZuordnungPseudoOENR == true and $ZuordnungPseudo == true) //Nicht mehr zuordnen
							{
								$Meldung.= 'LARTNR wird nicht zugeordnet. LARTNR bereits bei '.$rsLARPseudo->FeldInhalt('AST_ATUNR').' zugeordnet.';
								$LAR_AST_ATUNR=$rsLARPseudo->FeldInhalt('AST_ATUNR');
							}							
							else 
							{
								if ($ZuordnungPseudoOENR == false and $ZuordnungPseudo == true) //Zuordnen
								{
									$Meldung.= 'LARTNR wird zugeordnet. LARTNR ist aber bereits bei '.$rsLARPseudo->FeldInhalt('AST_ATUNR').' zugeordnet.';
									$LAR_AST_ATUNR=$rsLARPseudo->FeldInhalt('AST_ATUNR');
								}
								
								//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
								$SQL = ' SELECT * ';
								$SQL .= ' FROM LIEFERANTENARTIKEL ';
                                if($rsCIDStatus->FeldInhalt('CIK_IMPORTTYP')==2){
                                    $SQL .= " WHERE (LAR_LARTNR) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR'));
                                }else{
                                    $SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
                                }
								$SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " .$this->_DB->FeldInhaltFormat('Z',$rsCID->Feldinhalt('CID_LIE_NR'));
	
								$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);
	
								//Wenn LIEFARTNR + LIEFNR nicht in AWIS, dann LIEFARTNR anlegen
								if($rsLARTNR->AnzahlDatensaetze()==0)
								{
									//neue LiefArtNr anlegen
									$SQL = 'INSERT INTO Lieferantenartikel';
									$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
									$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
									$SQL .= ',LAR_USER, LAR_USERDAT';
									$SQL .= ')VALUES (';
									$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsCID->Feldinhalt('CID_LAR_LARTNR'),false);
									$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T',$rsCID->Feldinhalt('CID_LIE_NR'),false).',4,0)';
									$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$Bemerkung,true);
									$SQL .= ',0';
									$SQL .= ',4';		// Importquelle
									$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsCID->Feldinhalt('CID_LAR_LARTNR')).')';
                                    $SQL .= ',\'Import\'';
									$SQL .= ',SYSDATE';
									$SQL .= ')';
	
									$this->_DB->Ausfuehren($SQL,'',true);
	
									$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
									$rsKey = $this->_DB->RecordSetOeffnen($SQL);
									$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
									$MELDUNGLIEFARTNR = $rsCID->Feldinhalt('CID_LAR_LARTNR').' angelegt';
								}
								else
								{
									$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
	
									// Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Lieferantenrtikel
									// nicht durch den Import gel�scht werden kann
									// User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
									$SQL = 'UPDATE Lieferantenartikel SET';
                                    $SQL .= ' LAR_user=\'Import\'';
									$SQL .= ', LAR_IMQ_ID = CASE WHEN BITAND(LAR_IMQ_ID,4)=4 THEN LAR_IMQ_ID ELSE LAR_IMQ_ID+4 END';
									$SQL .= ', LAR_userdat=sysdate';
									$SQL .= ' WHERE LAR_key=0' . $AWIS_KEY2 . ' AND BITAND(LAR_IMQ_ID,4)<>4';
	
									$this->_DB->Ausfuehren($SQL,'',true);

									$SQL = 'SELECT LAR_USER FROM LIEFERANTENARTIKEL WHERE LAR_KEY = ' . $this->_DB->WertSetzen('LAR','Z',$AWIS_KEY2);
									$Besitzer = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('LAR'));
									if(strtolower($Besitzer->FeldInhalt(1)) != 'import'){
                                        $MELDUNGLIEFARTNR = $rsCID->Feldinhalt('CID_LAR_LARTNR').' vorhanden. Besitzer auf Import gewechselt. Vorher: '.$Besitzer->FeldInhalt(1);
                                    }
								}
	
								//Pr�fen, ob ATUNR und LIEFARTNR evtl. schon zugeordnet sind
								$SQL = ' SELECT * ';
								$SQL .= ' FROM TeileInfos ';
								$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_KEY1 = '.$OEN_ASTKEY;
								$SQL .= ' AND TEI_ITY_ID2 = \'LAR\' AND TEI_KEY2 = '.$AWIS_KEY2;
	
								$rsTei = $this->_DB->RecordSetOeffnen($SQL);
	
								//LiefArtNr der ATUNR zuordnen
								if($rsTei->AnzahlDatensaetze()==0)
								{
									$SQL = 'INSERT INTO TeileInfos';
									$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
									$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
									$SQL .= 'VALUES(';
									$SQL .= ' \'AST\'';
									$SQL .= ','.$OEN_ASTKEY;
									$SQL .= ',suchwort(\''.$OEN_AST_ATUNR.'\')';
									$SQL .= ',\''.$OEN_AST_ATUNR.'\'';
									$SQL .= ',\'LAR\'';
									$SQL .= ','.$AWIS_KEY2;
									$SQL .= ',asciiwort('.$this->_DB->FeldInhaltFormat('TU',$rsCID->Feldinhalt('CID_LAR_LARTNR'),false).')';
									$SQL .= ',\''.$rsCID->Feldinhalt('CID_LAR_LARTNR').'\'';
                                    $SQL .= ',\'Import\'';
									$SQL .= ',SYSDATE';
									$SQL .= ')';
	
									$this->_DB->Ausfuehren($SQL,'',true);

                                     //Zu der eben angelegten AST<-->LART Zuordnung den SK Export setzen:
                                    $TEI_KEY = $this->_DB->RecordSetOeffnen('Select SEQ_TEI_KEY.currval from dual')->FeldInhalt(1);

                                    $Merge = 'merge INTO TEILEINFOSINFOS a USING';
                                    $Merge .= ' (';
                                    $Merge .= 'Select distinct '.$TEI_KEY.' as TEI_KEY, \'1\' as TII_WERT  from dual';
                                    $Merge .= ' ) src ON (src.TEI_KEY = a.TII_TEI_KEY and TII_ITY_KEY = 318)';
                                    $Merge .= ' WHEN matched THEN';
                                    $Merge .= '   UPDATE SET a.TII_WERT = src.TII_WERT ';
                                    $Merge .= ' WHEN NOT matched THEN';
                                    $Merge .= '   INSERT';
                                    $Merge .= '     (';
                                    $Merge .= '       TII_TEI_KEY,';
                                    $Merge .= '       TII_WERT,';
                                    $Merge .= '       tii_ity_key,';
                                    $Merge .= '       Tii_user,';
                                    $Merge .= '       Tii_userdat';
                                    $Merge .= '     )';
                                    $Merge .= '     VALUES';
                                    $Merge .= '     (';
                                    $Merge .= '       src.TEI_KEY,';
                                    $Merge .=  "'1'"; //Haken soll Standardm#�ig gesetzt sein
                                    $Merge .= ',       318,';
                                    $Merge .= "'Import'";
                                    $Merge .= ',       sysdate';
                                    $Merge .= '     )';

                                    $this->_DB->Ausfuehren($Merge, '', true, $this->_DB->Bindevariablen('SMK'));


                                    // Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Artikel
									// nicht durch den Import gel�scht werden kann
									// User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)
									$SQL = 'UPDATE Artikelstamm SET';
                                    $SQL .= ' AST_user=\'Import\'';
									$SQL .= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
									$SQL .= ', AST_userdat=sysdate';
									$SQL .= ' WHERE AST_key=0' . $OEN_ASTKEY. ' AND BITAND(AST_IMQ_ID,4)<>4';
	
									$this->_DB->Ausfuehren($SQL,'',true);

									$MELDUNGZUORDNUNG = $rsCID->Feldinhalt('CID_LAR_LARTNR').' wurde den Artikel '. $OEN_AST_ATUNR.' zugeordnet.';
									$Aktion='x';
	
								}
								else
								{
									$MELDUNGZUORDNUNG = 'Zuordnung zu Artikel '. $OEN_AST_ATUNR.' vorhanden.';
								}
								$Meldung.= 'LIEFARTNR '.$MELDUNGLIEFARTNR.' + '.$MELDUNGZUORDNUNG;								
							}
							//Zum Ersten DS von dieser LARTNR springen, damit Eintrag in Protokoll erfolgt.
							$rsCIDOEN->DSErster();

							while (!$rsCIDOEN->EOF())
							{
								$Zeile = 'Zeile '.$rsCIDOEN->FeldInhalt('CID_ZEILE').', '.$rsCIDOEN->FeldInhalt('CID_LIE_NR').', '.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').', '.$rsCIDOEN->FeldInhalt('CID_OEN_OENR');

								$MELDUNGIMPORT='OENR '.$OEN_OENNR. ' vorhanden + Zuordnung zu Artikel '. $OEN_AST_ATUNR. ' vorhanden. '.$Meldung;
								$SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_ZEILENINHALT, CIP_MELDUNG, CIP_AST_ATUNR_OEN, CIP_AST_ATUNR, CIP_LAR_LARTNR, CIP_AKTION, CIP_USER, CIP_USERDAT)';
								$SQL .= 'VALUES(';
								//$SQL .= ''.$CIK_KEY;
								$SQL .= ''.$rsCIDStatus->FeldInhalt('CIK_KEY');
								$SQL .= ','.$rsCIDOEN->FeldInhalt('CID_ZEILE');
								$SQL .= ',\''.$Zeile.'\'';
								$SQL .= ',\''.$MELDUNGIMPORT.'\'';
								$SQL .= ',\''.$OEN_AST_ATUNR.'\'';
								$SQL .= ',\''.$LAR_AST_ATUNR.'\'';
								$SQL .= ',\''.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').'\'';
								$SQL .= ',\''.$Aktion.'\'';
								$SQL .= ',\'Import\'';
								$SQL .= ',SYSDATE';
								$SQL .= ')';

								$this->_DB->Ausfuehren($SQL,'',true);

								$rsCIDOEN->DSWeiter();
							}							
						}
						else
						{
							$rsCIDOEN->DSErster();

							while (!$rsCIDOEN->EOF())
							{
								$Zeile = 'Zeile '.$rsCIDOEN->FeldInhalt('CID_ZEILE').', '.$rsCIDOEN->FeldInhalt('CID_LIE_NR').', '.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').', '.$rsCIDOEN->FeldInhalt('CID_OEN_OENR');
								//$Erg['Meldungen'][]=$Zeile. ':'. 'OENR '.$OEN_OENNR. ' vorhanden + Zuordnung zu Artikel '. $OEN_AST_ATUNR. ' vorhanden. LIEFARTNR '.$rsCID->FeldInhalt('CID_LAR_LARTNR').' vorhanden + Zuordnung zu Artikel '. $rsLAR->FeldInhalt('AST_ATUNR'). ' vorhanden.';

								$MELDUNGIMPORT='OENR '.$OEN_OENNR. ' vorhanden + Zuordnung zu Artikel '. $OEN_AST_ATUNR. ' vorhanden. LIEFARTNR '.$rsCID->FeldInhalt('CID_LAR_LARTNR').' vorhanden + Zuordnung zu Artikel '. $rsLAR->FeldInhalt('AST_ATUNR'). ' vorhanden.';
								$SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_ZEILENINHALT, CIP_MELDUNG, CIP_AST_ATUNR_OEN, CIP_AST_ATUNR, CIP_LAR_LARTNR, CIP_USER, CIP_USERDAT)';
								$SQL .= 'VALUES(';
								$SQL .= ''.$rsCIDStatus->FeldInhalt('CIK_KEY');
								$SQL .= ','.$rsCIDOEN->FeldInhalt('CID_ZEILE');
								$SQL .= ',\''.$Zeile.'\'';
								$SQL .= ',\''.$MELDUNGIMPORT.'\'';
								$SQL .= ',\''.$OEN_AST_ATUNR.'\'';
								$SQL .= ',\''.$rsLAR->FeldInhalt('AST_ATUNR').'\'';
								$SQL .= ',\''.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').'\'';
								//$SQL .= ',\''.$AWISBenutzer->BenutzerName().'\'';
								$SQL .= ',\'Import\'';
								$SQL .= ',SYSDATE';
								$SQL .= ')';

								$this->_DB->Ausfuehren($SQL,'',true);

								$rsCIDOEN->DSWeiter();
							}
						}
					}
					else
					{
						$Aktion='';
						
						//Zur ersten OENR von dieser Lieferantenartikelnummer springen
						$rsCIDOEN->DSErster();

						//Pr�fen, ob einzutragende LIEFARTNR schon bei einem Artikel hinterlegt ist
						$SQL = ' SELECT * ';
						$SQL .= ' FROM Artikelstamm';
						//$SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \''.$AWISBenutzer->BenutzerSprache().'\'';
						$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
						$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
						$SQL .= ' INNER JOIN TeileInfos ON (AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
                        if($rsCIDStatus->FeldInhalt('CIK_IMPORTTYP')==2){
                            $SQL .= " AND (TEI_WERT2) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR')) . ")";
                        }else{
                            $SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER ) . ")";
                        }

						$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
						$SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " .$this->_DB->FeldInhaltFormat('Z',$rsCID->Feldinhalt('CID_LIE_NR')). ")";
						$SQL .= ' WHERE AST_IMQ_ID <> 2';

						$rsLAR = $this->_DB->RecordSetOeffnen($SQL);

						//Wenn LAR nicht in AWIS
						if($rsLAR->AnzahlDatensaetze()==0)
						{
							$Aktion='x';
							
							$Kennung = $this->_AWISBenutzer->ParameterLesen('DefaultKennungAutoArtikelnr');

                            $SQL  ='select ast_atunr ';
                            $SQL .=' from ( ';
                            $SQL .='    select AST_ATUNR, ';
                            $SQL .='        regexp_substr(zahlen,\'[1-9]{1,}[0-9]{0,}\') as zahlenohnenull, ';
                            $SQL .='        regexp_substr(teilwert,\'[1-9]{1,}[0-9]{0,}\') as teilwertohnenull, ';
                            $SQL .='        teilwert, ';
                            $SQL .='        teilwert2 ';
                            $SQL .='    from ( ';
                            $SQL .='        select AST_ATUNR, ';
                            $SQL .='            substr(teilwert,(length(teilwert2))+1, 100) as zahlen, ';
                            $SQL .='            teilwert2, ';
                            $SQL .='            teilwert ';
                            $SQL .='        from ( ';
                            $SQL .='            select regexp_substr(teilwert,\'[A-Z]{1,}\') as teilwert2, ';
                            $SQL .='                teilwert, ';
                            $SQL .='                AST_ATUNR ';
                            $SQL .='            from ( ';
                            $SQL .='                SELECT SUBSTR(AST_ATUNR,LENGTH(\''.$Kennung.'\')+1,100) as teilwert, ';
                            $SQL .='                    AST_ATUNR ';
                            $SQL .='                FROM ARTIKELSTAMM ';
                            $SQL .='                WHERE SUBSTR(AST_ATUNR,1,LENGTH(\''.$Kennung.'\')) = \''.$Kennung.'\' ';
                            $SQL .='            ) ';
                            $SQL .='        ) ';
                            $SQL .='    ) ';
                            $SQL .='    ORDER BY teilwert2 asc, ';
                            $SQL .='        length(zahlenohnenull) desc, ';
                            $SQL .='        zahlenohnenull desc, ';
                            $SQL .='        length(teilwert) desc, ';
                            $SQL .='        teilwert desc ';
                            $SQL .=') ';
                            $SQL .='where ROWNUM = 1 ';

							$rsKey = $this->_DB->RecordSetOeffnen($SQL);
							$rsKey = $rsKey->Tabelle();
                            $ATUNR = $rsKey['AST_ATUNR'][0];

                            //letzte ATU Nummer pruefen und zerlegen..
                            if (preg_match("[\d]", $ATUNR,$Treffer)) {
                                $ATUNrPraefix = substr($ATUNR,0,strpos($ATUNR,$Treffer[0])); //Alles vor der ersten Zahl Bsp. �T
                                $ATUNrLfdNr = substr($ATUNR,strpos($ATUNR,$Treffer[0]));//Alles ab der ersten Zahl, Bsp. 9888

                                $ATUNR = str_pad($ATUNrPraefix . ++$ATUNrLfdNr,6,'0');
                            }else{
                                throw new Exception('ATU-Nummer konnte nicht ermittelt werden. => ' . $ATUNR,201805141155);
                            }

							
							$WUG_KEY = $this->_AWISBenutzer->ParameterLesen('DefaultWUG_Key_NeueArtikel');

							//neue ATU-Pseudonummer anlegen
							$SQL = 'INSERT INTO Artikelstamm';
							$SQL .= '(AST_ATUNR';
							$SQL .= ',AST_WUG_KEY, AST_IMQ_ID, AST_USER, AST_USERDAT';
							$SQL .= ') VALUES (';
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat('TU',$ATUNR,false);
							$SQL .= ','.$this->_DB->FeldInhaltFormat('N0',$WUG_KEY);
							$SQL .= ',4';
                            $SQL .= ',\'Import\'';
							$SQL .= ',SYSDATE';
							$SQL .= ')';

							$this->_DB->Ausfuehren($SQL,'',true);

							$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
							$rsKey = $this->_DB->RecordSetOeffnen($SQL);
							$AWIS_KEY1=$rsKey->FeldInhalt('KEY');

							//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
							$SQL = ' SELECT * ';
							$SQL .= ' FROM LIEFERANTENARTIKEL ';
                            if($rsCIDStatus->FeldInhalt('CIK_IMPORTTYP')==2){
                                $SQL .= " WHERE (LAR_LARTNR) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR'));
                            }else{
                                $SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsCID->Feldinhalt('CID_LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
                            }
							$SQL .= " AND (TO_NUMBER(LAR_LIE_NR)) = " .$this->_DB->FeldInhaltFormat('Z',$rsCID->Feldinhalt('CID_LIE_NR'));

							$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

							//Wenn LIEFARTNR + LIEFNR nicht in AWIS
							if($rsLARTNR->AnzahlDatensaetze()==0)
							{
								//neue LiefArtNr anlegen
								$SQL = 'INSERT INTO Lieferantenartikel';
								$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
								$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
								$SQL .= ',LAR_USER, LAR_USERDAT';
								$SQL .= ')VALUES (';
								$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsCID->Feldinhalt('CID_LAR_LARTNR'),false);
								$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T',$rsCID->Feldinhalt('CID_LIE_NR'),false).',4,0)';
								$SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$Bemerkung,true);
								$SQL .= ',0';
								$SQL .= ',4';		// Importquelle
								$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsCID->Feldinhalt('CID_LAR_LARTNR'),false).')';
                                $SQL .= ',\'Import\'';
								$SQL .= ',SYSDATE';
								$SQL .= ')';

								$this->_DB->Ausfuehren($SQL,'',true);

								$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
								$rsKey = $this->_DB->RecordSetOeffnen($SQL);
								$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

								$MELDUNGLIEFARTNR = $rsCID->Feldinhalt('CID_LAR_LARTNR').' angelegt.';
							}
							else
							{
								$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

								$MELDUNGLIEFARTNR = $rsCID->Feldinhalt('CID_LAR_LARTNR').' vorhanden.';
							}

							//Eintrag in Teileinfos ATUNR -> LARTNR
							$SQL = 'INSERT INTO TeileInfos';
							$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
							$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
							$SQL .= 'VALUES(';
							$SQL .= ' \'AST\'';
							$SQL .= ','.$AWIS_KEY1;
							$SQL .= ',suchwort(\''.$ATUNR.'\')';
							$SQL .= ',\''.$ATUNR.'\'';
							$SQL .= ',\'LAR\'';
							$SQL .= ','.$AWIS_KEY2;
							$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsCID->Feldinhalt('CID_LAR_LARTNR'),false).')';
							$SQL .= ',\''.$rsCID->Feldinhalt('CID_LAR_LARTNR').'\'';
							$SQL .= ',\'Import\'';

							$SQL .= ',SYSDATE';
							$SQL .= ')';

							$this->_DB->Ausfuehren($SQL,'',true);

                            //Zu der eben angelegten AST<-->LART Zuordnung den SK Export setzen:
                            $TEI_KEY = $this->_DB->RecordSetOeffnen('Select SEQ_TEI_KEY.currval from dual')->FeldInhalt(1);

                            $Merge = 'merge INTO TEILEINFOSINFOS a USING';
                            $Merge .= ' (';
                            $Merge .= 'Select distinct '.$TEI_KEY.' as TEI_KEY, \'1\' as TII_WERT  from dual';
                            $Merge .= ' ) src ON (src.TEI_KEY = a.TII_TEI_KEY and TII_ITY_KEY = 318)';
                            $Merge .= ' WHEN matched THEN';
                            $Merge .= '   UPDATE SET a.TII_WERT = src.TII_WERT ';
                            $Merge .= ' WHEN NOT matched THEN';
                            $Merge .= '   INSERT';
                            $Merge .= '     (';
                            $Merge .= '       TII_TEI_KEY,';
                            $Merge .= '       TII_WERT,';
                            $Merge .= '       tii_ity_key,';
                            $Merge .= '       Tii_user,';
                            $Merge .= '       Tii_userdat';
                            $Merge .= '     )';
                            $Merge .= '     VALUES';
                            $Merge .= '     (';
                            $Merge .= '       src.TEI_KEY,';
                            $Merge .=  "'1'"; //Haken soll Standardm#�ig gesetzt sein
                            $Merge .= ',       318,';
                            $Merge .= "'Import'";
                            $Merge .= ',       sysdate';
                            $Merge .= '     )';

                            $this->_DB->Ausfuehren($Merge, '', true, $this->_DB->Bindevariablen('SMK'));


                            //alle OENR zu dieser LARTNR zuordnen???
							while(!$rsCIDOEN->EOF())
							{
								//Pr�fen, ob OENR bereits in Tabelle OENUMMERN vorhanden (keine Zuordnung ?)
								$SQL = ' SELECT * ';
								$SQL .= ' FROM OENUMMERN ';
								$SQL .= " WHERE (OEN_SUCHNUMMER) " . $this->_DB->LIKEoderIST($rsCIDOEN->Feldinhalt('CID_OEN_OENR'), awisDatenbank::AWIS_LIKE_SUCHOE);
                                if($rsCIDStatus->FeldInhalt('CIK_IMPORTTYP')==2){
                                    $SQL .= " AND OEN_HER_ID = ".$rsCIDOEN->Feldinhalt('CID_OEN_HER_ID');
                                }

								$rsOENR = $this->_DB->RecordSetOeffnen($SQL);

								//Wenn OENR nicht in AWIS
								if($rsOENR->AnzahlDatensaetze()==0)
								{
									//neue OENR anlegen
									$SQL = 'INSERT INTO OENummern';
									$SQL .= '(OEN_NUMMER, OEN_HER_ID';
									$SQL .= ',OEN_MUSTER,OEN_SUCHNUMMER';
									$SQL .= ',OEN_USER, OEN_USERDAT';
									$SQL .= ')VALUES (';
									$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',str_replace('�', '', $rsCIDOEN->Feldinhalt('CID_OEN_OENR')),false); //Weiches Trennzeichen/Soft Hyphen muss ersetzt werden
									$SQL .= ', (select HEI_HER_ID from herstellerinfos where HEI_WERT1=' . $this->_DB->FeldInhaltFormat('N0',$rsCIDOEN->Feldinhalt('CID_OEN_HER_ID'),false).')';
									$SQL .= ',0';
									$SQL .= ',asciiwortoe(' . $this->_DB->FeldInhaltFormat('T',$rsCIDOEN->Feldinhalt('CID_OEN_OENR'),false).')';
                                    $SQL .= ',\'Import\'';
									$SQL .= ',SYSDATE';
									$SQL .= ')';

									$this->_DB->Ausfuehren($SQL,'',true);

									$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
									$rsKey = $this->_DB->RecordSetOeffnen($SQL);
									$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

									$MELDUNGOENR = $rsCIDOEN->Feldinhalt('CID_OEN_OENR').' angelegt.';
								}
								else
								{
									$AWIS_KEY2=$rsOENR->FeldInhalt('OEN_KEY');

									$MELDUNGOENR = $rsCIDOEN->Feldinhalt('CID_OEN_OENR').' vorhanden.';
								}

								//Pr�fen, ob OENR auch schon bei dem Artikel zugeordnet ist
								$SQL = ' SELECT * ';
								$SQL.= ' FROM OENUMMERN ';
								$SQL.= ' INNER JOIN TEILEINFOS ON TEI_KEY1 = '.$AWIS_KEY1;
								$SQL.= ' AND TEI_ITY_ID1 = \'AST\' AND TEI_ITY_ID2 = \'OEN\' AND TEI_KEY2 = '.$AWIS_KEY2;
								$SQL .= " WHERE (OEN_SUCHNUMMER) " . $this->_DB->LIKEoderIST($rsCIDOEN->Feldinhalt('CID_OEN_OENR'), awisDatenbank::AWIS_LIKE_SUCHOE);

								$rsTEI = $this->_DB->RecordSetOeffnen($SQL);
								
								if ($rsTEI->AnzahlDatensaetze()==0)
								{
									//Eintrag in Teileinfos ATUNR -> OENR
									$SQL = 'INSERT INTO TeileInfos';
									$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
									$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
									$SQL .= 'VALUES(';
									$SQL .= ' \'AST\'';
									$SQL .= ','.$AWIS_KEY1;
									$SQL .= ',suchwort(\''.$ATUNR.'\')';
									$SQL .= ',\''.$ATUNR.'\'';
									$SQL .= ',\'OEN\'';
									$SQL .= ','.$AWIS_KEY2;
									$SQL .= ',asciiwortoe(\''.$rsCIDOEN->Feldinhalt('CID_OEN_OENR').'\')';
									$SQL .= ',\''.$rsCIDOEN->Feldinhalt('CID_OEN_OENR').'\'';
									$SQL .= ',\'Import\'';
									$SQL .= ',SYSDATE';
									$SQL .= ')';
	
									$this->_DB->Ausfuehren($SQL,'',true);
								}

								$Zeile = 'Zeile '.$rsCIDOEN->FeldInhalt('CID_ZEILE').', '.$rsCIDOEN->FeldInhalt('CID_LIE_NR').', '.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').', '.$rsCIDOEN->FeldInhalt('CID_OEN_OENR');

								$MELDUNGIMPORT='Neuer Artikel: '.$ATUNR .', LIEFARTNR '.$MELDUNGLIEFARTNR.' OENR '.$MELDUNGOENR;
								$SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_ZEILENINHALT, CIP_MELDUNG, CIP_AST_ATUNR_OEN, CIP_AST_ATUNR, CIP_LAR_LARTNR, CIP_AKTION, CIP_USER, CIP_USERDAT)';
								$SQL .= 'VALUES(';
								$SQL .= ''.$rsCIDStatus->FeldInhalt('CIK_KEY');
								$SQL .= ','.$rsCIDOEN->FeldInhalt('CID_ZEILE');
								$SQL .= ',\''.$Zeile.'\'';
								$SQL .= ',\''.$MELDUNGIMPORT.'\'';
								$SQL .= ',\''.$ATUNR.'\'';
								$SQL .= ',\''.$ATUNR.'\'';
								$SQL .= ',\''.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').'\'';
								$SQL .= ',\''.$Aktion.'\'';
								$SQL .= ',\'Import\'';
								$SQL .= ',SYSDATE';
								$SQL .= ')';

								$this->_DB->Ausfuehren($SQL,'',true);

								$rsCIDOEN->DSWeiter();
							}
						}
						else
						{
							while (!$rsCIDOEN->EOF())
							{
								$Zeile = 'Zeile '.$rsCIDOEN->FeldInhalt('CID_ZEILE').', '.$rsCIDOEN->FeldInhalt('CID_LIE_NR').', '.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').', '.$rsCIDOEN->FeldInhalt('CID_OEN_OENR');

								$MELDUNGIMPORT='OENR '.$rsCIDOEN->Feldinhalt('CID_OEN_OENR'). ' nicht vorhanden. LIEFARTNR '.$rsCID->Feldinhalt('CID_LAR_LARTNR').' vorhanden + Zuordnung zu Artikel '. $rsLAR->FeldInhalt('AST_ATUNR'). ' vorhanden.';
								$SQL = 'INSERT INTO CROSSINGIMPORTPOS (CIP_CIK_KEY, CIP_ZEILE, CIP_ZEILENINHALT, CIP_MELDUNG, CIP_AST_ATUNR, CIP_LAR_LARTNR, CIP_USER, CIP_USERDAT)';
								$SQL .= 'VALUES(';
								$SQL .= ''.$rsCIDStatus->FeldInhalt('CIK_KEY');
								$SQL .= ','.$rsCIDOEN->FeldInhalt('CID_ZEILE');
								$SQL .= ',\''.$Zeile.'\'';
								$SQL .= ',\''.$MELDUNGIMPORT.'\'';
								$SQL .= ',\''.$rsLAR->FeldInhalt('AST_ATUNR').'\'';
								$SQL .= ',\''.$rsCIDOEN->FeldInhalt('CID_LAR_LARTNR').'\'';
                                $SQL .= ',\'Import\'';
								$SQL .= ',SYSDATE';
								$SQL .= ')';

								$this->_DB->Ausfuehren($SQL,'',true);

								$rsCIDOEN->DSWeiter();
							}
						}
					}
					$rsCID->DSWeiter();
				}

				//Status auf beendet setzen
				$SQL = 'UPDATE CROSSINGIMPORTKOPF SET CIK_STATUS = 3 WHERE CIK_KEY = '.$rsCIDStatus->FeldInhalt('CIK_KEY');
				$this->_DB->Ausfuehren($SQL,'',true);
				awisWerkzeuge::EMail($this->_EMAIL_Infos,'ARTIKELSTAMMIMPORT BEENDET','',2,'','awis@de.atu.eu');
			}
			else 
			{
				if ($this->_DebugLevel >= 10)
				{
					echo "Keine Crossingimportdatei mit Status 1 gefunden. " . PHP_EOL;
				}
			}
		}
		catch (Exception $ex)
		{
			throw new Exception('Fehler beim Import von Artikel: '.$ex->getMessage().' - '.$ex->getCode() .' Vorgang: ' .(isset($MELDUNGIMPORT)?$MELDUNGIMPORT:$SQL),12);
		}
	}
	
	//LUCAS
	public function import_artikel_lucas()
	{
		$SQL = 'SELECT * FROM XXX_LUCAS WHERE ZEILE > 2511 ORDER BY ZEILE';
		
		$rsLucas = $this->_DB->RecordSetOeffnen($SQL);

		while(!$rsLucas->EOF())
		{
			//Pr�fen ob zu der Lucas - Nummer schon eine ATU-Nr (Nur EM%, @% bzw. �%) zugeordnet ist
			$SQL = ' SELECT DISTINCT AST_ATUNR, AST_KEY ';
			$SQL .= ' FROM Artikelstamm';
			$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
			$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
			$SQL .= ' INNER JOIN TeileInfos ON AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
			$SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('LUCAS'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
			$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
			$SQL .= " AND LAR_LIE_NR = '0831')";
			$SQL .= " WHERE AST_ATUNR LIKE 'EM%' or AST_ATUNR LIKE '@%' or AST_ATUNR LIKE '�%'";
			
			$rsAST_Lucas = $this->_DB->RecordSetOeffnen($SQL);			
			
			//Wenn LAR - Lucas nicht in AWIS
			if($rsAST_Lucas->AnzahlDatensaetze()==0)			
			{
				// Pr�fen, ob TRW - Nummer schon eine ATU-Nr (Nur EM%, @% bzw. �%) zugeordnet ist				
				$SQL = ' SELECT DISTINCT AST_ATUNR, AST_KEY ';
				$SQL .= ' FROM Artikelstamm';
				$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
				$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
				$SQL .= ' INNER JOIN TeileInfos ON AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
				$SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('TRW'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER );
				$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
				$SQL .= " AND LAR_LIE_NR = '0831')";
				$SQL .= " WHERE AST_ATUNR LIKE 'EM%' or AST_ATUNR LIKE '@%' or AST_ATUNR LIKE '�%'";
				
				$rsAST_TRW = $this->_DB->RecordSetOeffnen($SQL);
				
				if ($rsAST_TRW->AnzahlDatensaetze()==0)
				{
					//Neuen Artikel anlegen
					//TRW - Nr. + Lucas - Nr. + Gabriel - Nr.
					
    				$Kennung = utf8_decode($this->_AWISBenutzer->ParameterLesen('DefaultKennungAutoArtikelnr'));

					$SQL = 'SELECT * FROM (SELECT AST_ATUNR FROM ARTIKELSTAMM ';
					$SQL .= ' WHERE SUBSTR(AST_ATUNR,1,'.strlen($Kennung).')=\''.$Kennung.'\'';
					$SQL .= ' ORDER BY nlssort(ast_atunr,\'NLS_SORT=BINARY\') DESC) WHERE ROWNUM = 1';
					$rsKey = $this->_DB->RecordSetOeffnen($SQL);
					$rsKey = $rsKey->Tabelle();					

					if(!isset($rsKey['AST_ATUNR'][0]) OR (isset($rsKey['AST_ATUNR'][0]) AND $rsKey['AST_ATUNR'][0]==''))
					{
						$ATUNR=$Kennung.'0001';
					}
					else
					{
						$ATUNR=++$rsKey['AST_ATUNR'][0];
					}
							
					$WUG_KEY = $this->_AWISBenutzer->ParameterLesen('DefaultWUG_Key_NeueArtikel');

					//neue ATU-Pseudonummer anlegen
					$SQL = 'INSERT INTO Artikelstamm';
					$SQL .= '(AST_ATUNR, AST_BEZEICHNUNG';
					$SQL .= ',AST_WUG_KEY, AST_IMQ_ID, AST_USER, AST_USERDAT';
					$SQL .= ') VALUES (';
					$SQL .= ' ' . $this->_DB->FeldInhaltFormat('TU',$ATUNR,false);
					$SQL.= ' ,'.$this->_DB->FeldInhaltFormat('T',$rsLucas->FeldInhalt('GAS_OEL').', '.$rsLucas->FeldInhalt('VORNE_HINTEN'));
					$SQL .= ','.$this->_DB->FeldInhaltFormat('N0',$WUG_KEY);
					$SQL .= ',4';
					$SQL .= ',\'Import\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);

					$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
					$rsKey = $this->_DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');

					//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
					$SQL = ' SELECT * ';
					$SQL .= ' FROM LIEFERANTENARTIKEL ';
					$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('LUCAS'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
					$SQL .= " AND (LAR_LIE_NR) = '0831'";

					$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

					//Wenn LIEFARTNR + LIEFNR nicht in AWIS
					if($rsLARTNR->AnzahlDatensaetze()==0)
					{
						//neue LiefArtNr anlegen
						$SQL = 'INSERT INTO Lieferantenartikel';
						$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
						$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
						$SQL .= ',LAR_USER, LAR_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('LUCAS'),false);
						$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','0831',false).',4,0)';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
						$SQL .= ',0';
						$SQL .= ',4';		// Importquelle
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
						$SQL .= ',\'Import\'';								
						$SQL .= ',SYSDATE';
						$SQL .= ')';

						$this->_DB->Ausfuehren($SQL,'',true);
						
						$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' angelegt.';
					}
					else
					{
						$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' vorhanden.';
					}
					
					//Eintrag in Teileinfos ATUNR -> LARTNR
					$SQL = 'INSERT INTO TeileInfos';
					$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
					$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
					$SQL .= 'VALUES(';
					$SQL .= ' \'AST\'';
					$SQL .= ','.$AWIS_KEY1;
					$SQL .= ',suchwort(\''.$ATUNR.'\')';
					$SQL .= ',\''.$ATUNR.'\'';
					$SQL .= ',\'LAR\'';
					$SQL .= ','.$AWIS_KEY2;
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
					$SQL .= ',\''.$rsLucas->Feldinhalt('LUCAS').'\'';
					$SQL .= ',\'Import\'';							
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);

					//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
					$SQL = ' SELECT * ';
					$SQL .= ' FROM LIEFERANTENARTIKEL ';
					$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('TRW'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
					$SQL .= " AND (LAR_LIE_NR) = '0831'";

					$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

					//Wenn LIEFARTNR + LIEFNR nicht in AWIS
					if($rsLARTNR->AnzahlDatensaetze()==0)
					{
						//neue LiefArtNr anlegen
						$SQL = 'INSERT INTO Lieferantenartikel';
						$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
						$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
						$SQL .= ',LAR_USER, LAR_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('TRW'),false);
						$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','0831',false).',4,0)';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
						$SQL .= ',0';
						$SQL .= ',4';		// Importquelle
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('TRW'),false).')';
						$SQL .= ',\'Import\'';								
						$SQL .= ',SYSDATE';
						$SQL .= ')';

						$this->_DB->Ausfuehren($SQL,'',true);
						
						$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('TRW').' angelegt.';
					}
					else
					{
						$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('TRW').' vorhanden.';
					}
					
					//Eintrag in Teileinfos ATUNR -> LARTNR
					$SQL = 'INSERT INTO TeileInfos';
					$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
					$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
					$SQL .= 'VALUES(';
					$SQL .= ' \'AST\'';
					$SQL .= ','.$AWIS_KEY1;
					$SQL .= ',suchwort(\''.$ATUNR.'\')';
					$SQL .= ',\''.$ATUNR.'\'';
					$SQL .= ',\'LAR\'';
					$SQL .= ','.$AWIS_KEY2;
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('TRW'),false).')';
					$SQL .= ',\''.$rsLucas->Feldinhalt('TRW').'\'';
					$SQL .= ',\'Import\'';							
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);
					
					//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
					$SQL = ' SELECT * ';
					$SQL .= ' FROM LIEFERANTENARTIKEL ';
					$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('GABRIEL'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
					$SQL .= " AND (LAR_LIE_NR) = '9146'";

					$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

					//Wenn LIEFARTNR + LIEFNR nicht in AWIS
					if($rsLARTNR->AnzahlDatensaetze()==0)
					{
						//neue LiefArtNr anlegen
						$SQL = 'INSERT INTO Lieferantenartikel';
						$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
						$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
						$SQL .= ',LAR_USER, LAR_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('GABRIEL'),false);
						$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','9146',false).',4,0)';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
						$SQL .= ',0';
						$SQL .= ',4';		// Importquelle
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
						$SQL .= ',\'Import\'';								
						$SQL .= ',SYSDATE';
						$SQL .= ')';

						$this->_DB->Ausfuehren($SQL,'',true);
						
						$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' angelegt.';
					}
					else
					{
						$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' vorhanden.';
					}
					
					//Eintrag in Teileinfos ATUNR -> LARTNR
					$SQL = 'INSERT INTO TeileInfos';
					$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
					$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
					$SQL .= 'VALUES(';
					$SQL .= ' \'AST\'';
					$SQL .= ','.$AWIS_KEY1;
					$SQL .= ',suchwort(\''.$ATUNR.'\')';
					$SQL .= ',\''.$ATUNR.'\'';
					$SQL .= ',\'LAR\'';
					$SQL .= ','.$AWIS_KEY2;
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
					$SQL .= ',\''.$rsLucas->Feldinhalt('GABRIEL').'\'';
					$SQL .= ',\'Import\'';							
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);
					
					if ($rsLucas->Feldinhalt('OENR')!='')
					{
						$OENR = explode(',',$rsLucas->Feldinhalt('OENR'));					
						foreach ($OENR as $OE)
						{
							$OE = trim($OE);
										
							//Pr�fen, ob OENR bereits in Tabelle OENUMMERN vorhanden (keine Zuordnung ?)
							$SQL = ' SELECT * ';
							$SQL .= ' FROM OENUMMERN ';
							$SQL .= " WHERE (OEN_SUCHNUMMER) " . $this->_DB->LIKEoderIST($OE, awisDatenbank::AWIS_LIKE_SUCHOE);
	
							$rsOENR = $this->_DB->RecordSetOeffnen($SQL);
	
							//Wenn OENR nicht in AWIS
							if($rsOENR->AnzahlDatensaetze()==0)
							{
								//neue OENR anlegen
								$SQL = 'INSERT INTO OENummern';
								$SQL .= '(OEN_NUMMER, OEN_HER_ID';
								$SQL .= ',OEN_MUSTER,OEN_SUCHNUMMER';
								$SQL .= ',OEN_USER, OEN_USERDAT';
								$SQL .= ')VALUES (';
								$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',str_replace('�', '',$OE),false);//Weiches Trennzeichen/Soft Hyphen muss ersetzt werden
								$SQL .= ',0';
								$SQL .= ',0';
								$SQL .= ',asciiwortoe(' . $this->_DB->FeldInhaltFormat('T',$OE,false).')';
								$SQL .= ',\'Import\'';							
								$SQL .= ',SYSDATE';
								$SQL .= ')';
	
								$this->_DB->Ausfuehren($SQL,'',true);

								$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
								$rsKey = $this->_DB->RecordSetOeffnen($SQL);
								$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
								$MELDUNGOENR = $OE.' angelegt.';
							}
							else
							{
								$AWIS_KEY2=$rsOENR->FeldInhalt('OEN_KEY');
	
								$MELDUNGOENR = $OE.' vorhanden.';
							}
	
							//Eintrag in Teileinfos ATUNR -> OENR
							$SQL = 'INSERT INTO TeileInfos';
							$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
							$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
							$SQL .= 'VALUES(';
							$SQL .= ' \'AST\'';
							$SQL .= ','.$AWIS_KEY1;
							$SQL .= ',suchwort(\''.$ATUNR.'\')';
							$SQL .= ',\''.$ATUNR.'\'';
							$SQL .= ',\'OEN\'';
							$SQL .= ','.$AWIS_KEY2;
							$SQL .= ',asciiwortoe(\''.$OE.'\')';
							$SQL .= ',\''.$OE.'\'';
							$SQL .= ',\'Import\'';						
							$SQL .= ',SYSDATE';
							$SQL .= ')';
	
							$this->_DB->Ausfuehren($SQL,'',true);
						}
					}
					$Meldung = 'Zeile: '.$rsLucas->FeldInhalt('ZEILE'). ' Neuer Artikel: '.$ATUNR;
				}
				else
				{	
					$ATUNRGesamt='';
					
					if ($rsAST_TRW->AnzahlDatensaetze()>1)
					{
						//Info an Katalogabteilung
						$Meldung = 'Zeile: '.$rsLucas->FeldInhalt('ZEILE'). ' TRW bei mehreren Artikeln zugeordnet !';
					}
					else 
					{
						//Info an Katalogabteilung
						$Meldung = 'Zeile: '.$rsLucas->FeldInhalt('ZEILE'). ' TRW bei einem Artikel zugeordnet !';
					}
					
					//Alle Artikel durchgehen
					while (!$rsAST_TRW->EOF())
					{																	
						$AWIS_KEY1 = $rsAST_TRW->FeldInhalt('AST_KEY');
						$ATUNR = $rsAST_TRW->FeldInhalt('AST_ATUNR');
						
						// Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Artikel
						// nicht durch den Import gel�scht werden kann
						// User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)						
						// Bezeichnung Katalog updaten			
						$SQL = 'Update Artikelstamm set ';
						$SQL.= ' AST_BEZEICHNUNG = '.$this->_DB->FeldInhaltFormat('T',$rsLucas->FeldInhalt('GAS_OEL').', '.$rsLucas->FeldInhalt('VORNE_HINTEN'));
						$SQL.= ', AST_user=\'Import\'';
						$SQL.= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
						$SQL.= ', AST_userdat=sysdate';
						$SQL.= ' WHERE AST_KEY = '.$AWIS_KEY1;
						
						$this->_DB->Ausfuehren($SQL,'',true);
								
						//Lucas - Nummer bei diesem Artikel zuordnen						
						//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
						$SQL = ' SELECT * ';
						$SQL .= ' FROM LIEFERANTENARTIKEL ';
						$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('LUCAS'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
						$SQL .= " AND (LAR_LIE_NR) = '0831'";
	
						$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);
	
						//Wenn LIEFARTNR + LIEFNR nicht in AWIS
						if($rsLARTNR->AnzahlDatensaetze()==0)
						{
							//neue LiefArtNr anlegen
							$SQL = 'INSERT INTO Lieferantenartikel';
							$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
							$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
							$SQL .= ',LAR_USER, LAR_USERDAT';
							$SQL .= ')VALUES (';
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('LUCAS'),false);
							$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','0831',false).',4,0)';
							$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
							$SQL .= ',0';
							$SQL .= ',4';		// Importquelle
							$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
							$SQL .= ',\'Import\'';								
							$SQL .= ',SYSDATE';
							$SQL .= ')';
	
							$this->_DB->Ausfuehren($SQL,'',true);
							
							$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
							$rsKey = $this->_DB->RecordSetOeffnen($SQL);
							$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
							$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' angelegt.';
						}
						else
						{
							$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
	
							$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' vorhanden.';
						}
						
						//Eintrag in Teileinfos ATUNR -> LARTNR
						$SQL = 'INSERT INTO TeileInfos';
						$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
						$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
						$SQL .= 'VALUES(';
						$SQL .= ' \'AST\'';
						$SQL .= ','.$AWIS_KEY1;
						$SQL .= ',suchwort(\''.$ATUNR.'\')';
						$SQL .= ',\''.$ATUNR.'\'';
						$SQL .= ',\'LAR\'';
						$SQL .= ','.$AWIS_KEY2;
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
						$SQL .= ',\''.$rsLucas->Feldinhalt('LUCAS').'\'';
						$SQL .= ',\'Import\'';							
						$SQL .= ',SYSDATE';
						$SQL .= ')';
	
						$this->_DB->Ausfuehren($SQL,'',true);
												
						//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
						$SQL = ' SELECT * ';
						$SQL .= ' FROM LIEFERANTENARTIKEL ';
						$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('GABRIEL'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
						$SQL .= " AND (LAR_LIE_NR) = '9146'";
	
						$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);
	
						//Wenn LIEFARTNR + LIEFNR nicht in AWIS
						if($rsLARTNR->AnzahlDatensaetze()==0)
						{
							//neue LiefArtNr anlegen
							$SQL = 'INSERT INTO Lieferantenartikel';
							$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
							$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
							$SQL .= ',LAR_USER, LAR_USERDAT';
							$SQL .= ')VALUES (';
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('GABRIEL'),false);
							$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','9146',false).',4,0)';
							$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
							$SQL .= ',0';
							$SQL .= ',4';		// Importquelle
							$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
							$SQL .= ',\'Import\'';								
							$SQL .= ',SYSDATE';
							$SQL .= ')';
	
							$this->_DB->Ausfuehren($SQL,'',true);
							
							$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
							$rsKey = $this->_DB->RecordSetOeffnen($SQL);
							$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
							$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' angelegt.';
						}
						else
						{
							$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
	
							$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' vorhanden.';
						}
						
						//Pr�fen, ob schon zugeordnet
						$SQL = ' SELECT * ';
						$SQL .= ' FROM TEILEINFOS ';
						$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_ITY_ID2 = \'LAR\'';
						$SQL .= " AND TEI_KEY1 = ".$AWIS_KEY1." AND TEI_KEY2 = ".$AWIS_KEY2;
					
						$rsTEI = $this->_DB->RecordSetOeffnen($SQL);
								
						if ($rsTEI->AnzahlDatensaetze()==0)
						{											
							//Eintrag in Teileinfos ATUNR -> LARTNR
							$SQL = 'INSERT INTO TeileInfos';
							$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
							$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
							$SQL .= 'VALUES(';
							$SQL .= ' \'AST\'';
							$SQL .= ','.$AWIS_KEY1;
							$SQL .= ',suchwort(\''.$ATUNR.'\')';
							$SQL .= ',\''.$ATUNR.'\'';
							$SQL .= ',\'LAR\'';
							$SQL .= ','.$AWIS_KEY2;
							$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
							$SQL .= ',\''.$rsLucas->Feldinhalt('GABRIEL').'\'';
							$SQL .= ',\'Import\'';							
							$SQL .= ',SYSDATE';
							$SQL .= ')';
		
							$this->_DB->Ausfuehren($SQL,'',true);
						}
						
						if ($rsLucas->Feldinhalt('OENR')!='')
						{
							
							$OENR = explode(',',$rsLucas->Feldinhalt('OENR'));
							
							foreach ($OENR as $OE)
							{
								$OE = trim($OE);
											
								//Pr�fen, ob OENR bereits in Tabelle OENUMMERN vorhanden (keine Zuordnung ?)
								$SQL = ' SELECT * ';
								$SQL .= ' FROM OENUMMERN ';
								$SQL .= " WHERE (OEN_SUCHNUMMER) " . $this->_DB->LIKEoderIST($OE, awisDatenbank::AWIS_LIKE_SUCHOE);
		
								$rsOENR = $this->_DB->RecordSetOeffnen($SQL);
		
								//Wenn OENR nicht in AWIS
								if($rsOENR->AnzahlDatensaetze()==0)
								{
									//neue OENR anlegen
									$SQL = 'INSERT INTO OENummern';
									$SQL .= '(OEN_NUMMER, OEN_HER_ID';
									$SQL .= ',OEN_MUSTER,OEN_SUCHNUMMER';
									$SQL .= ',OEN_USER, OEN_USERDAT';
									$SQL .= ')VALUES (';
									$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',str_replace('�', '',$OE),false);//Weiches Trennzeichen/Soft Hyphen muss ersetzt werden
									$SQL .= ',0';
									$SQL .= ',0';
									$SQL .= ',asciiwortoe(' . $this->_DB->FeldInhaltFormat('T',$OE,false).')';
									$SQL .= ',\'Import\'';							
									$SQL .= ',SYSDATE';
									$SQL .= ')';
		
									$this->_DB->Ausfuehren($SQL,'',true);

									$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
									$rsKey = $this->_DB->RecordSetOeffnen($SQL);
									$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
		
									$MELDUNGOENR = $OE.' angelegt.';
								}
								else
								{
									$AWIS_KEY2=$rsOENR->FeldInhalt('OEN_KEY');
		
									$MELDUNGOENR = $OE.' vorhanden.';
								}
		
								//Pr�fen, ob schon zugeordnet
								$SQL = ' SELECT * ';
								$SQL .= ' FROM TEILEINFOS ';
								$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_ITY_ID2 = \'OEN\'';
								$SQL .= " AND TEI_KEY1 = ".$AWIS_KEY1." AND TEI_KEY2 = ".$AWIS_KEY2;
		
								$rsTEI = $this->_DB->RecordSetOeffnen($SQL);
								
								if ($rsTEI->AnzahlDatensaetze()==0)
								{
									//Eintrag in Teileinfos ATUNR -> OENR
									$SQL = 'INSERT INTO TeileInfos';
									$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
									$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
									$SQL .= 'VALUES(';
									$SQL .= ' \'AST\'';
									$SQL .= ','.$AWIS_KEY1;
									$SQL .= ',suchwort(\''.$ATUNR.'\')';
									$SQL .= ',\''.$ATUNR.'\'';
									$SQL .= ',\'OEN\'';
									$SQL .= ','.$AWIS_KEY2;
									$SQL .= ',asciiwortoe(\''.$OE.'\')';
									$SQL .= ',\''.$OE.'\'';
									$SQL .= ',\'Import\'';						
									$SQL .= ',SYSDATE';
									$SQL .= ')';
			
									$this->_DB->Ausfuehren($SQL,'',true);
								}
							}
						}
						$ATUNRGesamt.= ' | '.$ATUNR;
						$rsAST_TRW->DSWeiter();
					}
					
					$Meldung.= $ATUNRGesamt.' Lucas / Gabriel / OENR wurden zugeordnet';
				}																			
			}			
			else
			{
				if ($rsAST_Lucas->AnzahlDatensaetze()>1)
				{
					//Info an Katalogabteilung
					$Meldung = 'Zeile: '.$rsLucas->FeldInhalt('ZEILE'). ' Lucas bei mehreren Artikeln zugeordnet !';
				}
				else 
				{
					//Info an Katalogabteilung
					$Meldung = 'Zeile: '.$rsLucas->FeldInhalt('ZEILE'). ' Lucas bei einem Artikel zugeordnet !';
				}				
				
				$ATUNRGesamt='';
				
				while (!$rsAST_Lucas->EOF())
				{
					$AWIS_KEY1 = $rsAST_Lucas->FeldInhalt('AST_KEY');
					$ATUNR = $rsAST_Lucas->FeldInhalt('AST_ATUNR');
					
					// Beim �ndern einer Zuordnung muss der "Besitzer" gewechselt werden, damit der Artikel
					// nicht durch den Import gel�scht werden kann
					// User wird gesetzt, um die XXX Kennung durch Import-L�schung zu �berschreiben (falls vorhanden)						
					// Bezeichnung Katalog updaten			
					$SQL = 'Update Artikelstamm set ';
					$SQL.= ' AST_BEZEICHNUNG = '.$this->_DB->FeldInhaltFormat('T',$rsLucas->FeldInhalt('GAS_OEL').', '.$rsLucas->FeldInhalt('VORNE_HINTEN'));
					$SQL.= ', AST_user=\'Import\'';
					$SQL.= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
					$SQL.= ', AST_userdat=sysdate';
					$SQL.= ' WHERE AST_KEY = '.$AWIS_KEY1;
					
					$this->_DB->Ausfuehren($SQL,'',true);
														
					//Pr�fen ob TRW - Nummer bei einem anderen EM - Artikel zugeordnet ist, wenn ja, dann nicht mehr zuordnen					
					//Pr�fen ob TRW - Nummer auch schon zu dem Artikel zugeordnet ist
					$SQL = ' SELECT DISTINCT AST_ATUNR ';
					$SQL .= ' FROM Artikelstamm';
					$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
					$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
					$SQL .= ' INNER JOIN TeileInfos ON AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
					$SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('TRW'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER );
					$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
					$SQL .= " AND LAR_LIE_NR = '0831')";
					$SQL .= " WHERE AST_ATUNR = '".$rsAST_Lucas->FeldInhalt('AST_ATUNR')."'";
					
					$rsAST_TRW2 = $this->_DB->RecordSetOeffnen($SQL);
					
					if ($rsAST_TRW2->AnzahlDatensaetze()==0)
					{
						//Pr�fen ob TRW bei einem Anderen Artikel zugeordnet ist, wenn nein dann zuordnen (au�er 9...)
						$SQL = ' SELECT DISTINCT AST_ATUNR ';
						$SQL .= ' FROM Artikelstamm';
						$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
						$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
						$SQL .= ' INNER JOIN TeileInfos ON AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
						$SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('TRW'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER );
						$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
						$SQL .= " AND LAR_LIE_NR = '0831')";
						$SQL .= " WHERE AST_ATUNR <> '".$rsAST_Lucas->FeldInhalt('AST_ATUNR')."'";					
						$SQL.= " AND AST_ATUNR NOT LIKE '9%'";
						//wenn bei Artikel mit 9 --> dann trotzdem
						
						$rsAST_TRW3 = $this->_DB->RecordSetOeffnen($SQL);
						
						if ($rsAST_TRW3->AnzahlDatensaetze()==0)
						{
							//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
							$SQL = ' SELECT * ';
							$SQL .= ' FROM LIEFERANTENARTIKEL ';
							$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('TRW'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
							$SQL .= " AND (LAR_LIE_NR) = '0831'";
		
							$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);
	
							//Wenn LIEFARTNR + LIEFNR nicht in AWIS
							if($rsLARTNR->AnzahlDatensaetze()==0)
							{
								//neue LiefArtNr anlegen
								$SQL = 'INSERT INTO Lieferantenartikel';
								$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
								$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
								$SQL .= ',LAR_USER, LAR_USERDAT';
								$SQL .= ')VALUES (';
								$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('TRW'),false);
								$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','0831',false).',4,0)';
								$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
								$SQL .= ',0';
								$SQL .= ',4';		// Importquelle
								$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('TRW'),false).')';
								$SQL .= ',\'Import\'';								
								$SQL .= ',SYSDATE';
								$SQL .= ')';
	
								$this->_DB->Ausfuehren($SQL,'',true);
							
								$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
								$rsKey = $this->_DB->RecordSetOeffnen($SQL);
								$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
		
								$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('TRW').' angelegt.';
							}
							else
							{
								$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
		
								$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('TRW').' vorhanden.';
							}
						
							//Eintrag in Teileinfos ATUNR -> LARTNR
							$SQL = 'INSERT INTO TeileInfos';
							$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
							$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
							$SQL .= 'VALUES(';
							$SQL .= ' \'AST\'';
							$SQL .= ','.$AWIS_KEY1;
							$SQL .= ',suchwort(\''.$ATUNR.'\')';
							$SQL .= ',\''.$ATUNR.'\'';
							$SQL .= ',\'LAR\'';
							$SQL .= ','.$AWIS_KEY2;
							$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('TRW'),false).')';
							$SQL .= ',\''.$rsLucas->Feldinhalt('TRW').'\'';
							$SQL .= ',\'Import\'';							
							$SQL .= ',SYSDATE';
							$SQL .= ')';
		
							$this->_DB->Ausfuehren($SQL,'',true);

							//Gabriel auch Zuordnen
							//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
							$SQL = ' SELECT * ';
							$SQL .= ' FROM LIEFERANTENARTIKEL ';
							$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('GABRIEL'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
							$SQL .= " AND (LAR_LIE_NR) = '9146'";
			
							$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);
			
							//Wenn LIEFARTNR + LIEFNR nicht in AWIS
							if($rsLARTNR->AnzahlDatensaetze()==0)
							{
								//neue LiefArtNr anlegen
								$SQL = 'INSERT INTO Lieferantenartikel';
								$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
								$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
								$SQL .= ',LAR_USER, LAR_USERDAT';
								$SQL .= ')VALUES (';
								$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('GABRIEL'),false);
								$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','9146',false).',4,0)';
								$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
								$SQL .= ',0';
								$SQL .= ',4';		// Importquelle
								$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
								$SQL .= ',\'Import\'';								
								$SQL .= ',SYSDATE';
								$SQL .= ')';
			
								$this->_DB->Ausfuehren($SQL,'',true);
								
								$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
								$rsKey = $this->_DB->RecordSetOeffnen($SQL);
								$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			
								$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' angelegt.';
							}
							else
							{
								$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
			
								$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' vorhanden.';
							}
									
							//Pr�fen, ob schon zugeordnet
							$SQL = ' SELECT * ';
							$SQL .= ' FROM TEILEINFOS ';
							$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_ITY_ID2 = \'LAR\'';
							$SQL .= " AND TEI_KEY1 = ".$AWIS_KEY1." AND TEI_KEY2 = ".$AWIS_KEY2;
						
							$rsTEI = $this->_DB->RecordSetOeffnen($SQL);
									
							if ($rsTEI->AnzahlDatensaetze()==0)
							{					
								//Eintrag in Teileinfos ATUNR -> LARTNR
								$SQL = 'INSERT INTO TeileInfos';
								$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
								$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
								$SQL .= 'VALUES(';
								$SQL .= ' \'AST\'';
								$SQL .= ','.$AWIS_KEY1;
								$SQL .= ',suchwort(\''.$ATUNR.'\')';
								$SQL .= ',\''.$ATUNR.'\'';
								$SQL .= ',\'LAR\'';
								$SQL .= ','.$AWIS_KEY2;
								$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
								$SQL .= ',\''.$rsLucas->Feldinhalt('GABRIEL').'\'';
								$SQL .= ',\'Import\'';							
								$SQL .= ',SYSDATE';
								$SQL .= ')';
				
								$this->_DB->Ausfuehren($SQL,'',true);
							}

							if ($rsLucas->Feldinhalt('OENR')!='')
							{
							
								$OENR = explode(',',$rsLucas->Feldinhalt('OENR'));
	
								foreach ($OENR as $OE)
								{
									$OE = trim($OE);
												
									//Pr�fen, ob OENR bereits in Tabelle OENUMMERN vorhanden (keine Zuordnung ?)
									$SQL = ' SELECT * ';
									$SQL .= ' FROM OENUMMERN ';
									$SQL .= " WHERE (OEN_SUCHNUMMER) " . $this->_DB->LIKEoderIST($OE, awisDatenbank::AWIS_LIKE_SUCHOE);
			
									$rsOENR = $this->_DB->RecordSetOeffnen($SQL);
				
									//Wenn OENR nicht in AWIS
									if($rsOENR->AnzahlDatensaetze()==0)
									{
										//neue OENR anlegen
										$SQL = 'INSERT INTO OENummern';
										$SQL .= '(OEN_NUMMER, OEN_HER_ID';
										$SQL .= ',OEN_MUSTER,OEN_SUCHNUMMER';
										$SQL .= ',OEN_USER, OEN_USERDAT';
										$SQL .= ')VALUES (';
										$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',str_replace('�', '',$OE),false);//Weiches Trennzeichen/Soft Hyphen muss ersetzt werden
										$SQL .= ',0';
										$SQL .= ',0';
										$SQL .= ',asciiwortoe(' . $this->_DB->FeldInhaltFormat('T',$OE,false).')';
										$SQL .= ',\'Import\'';							
										$SQL .= ',SYSDATE';
										$SQL .= ')';
			
										$this->_DB->Ausfuehren($SQL,'',true);

										$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
										$rsKey = $this->_DB->RecordSetOeffnen($SQL);
										$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			
										$MELDUNGOENR = $OE.' angelegt.';
									}
									else
									{
										$AWIS_KEY2=$rsOENR->FeldInhalt('OEN_KEY');
			
										$MELDUNGOENR = $OE.' vorhanden.';
									}
				
									//Pr�fen, ob schon zugeordnet
									$SQL = ' SELECT * ';
									$SQL .= ' FROM TEILEINFOS ';
									$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_ITY_ID2 = \'OEN\'';
									$SQL .= " AND TEI_KEY1 = ".$AWIS_KEY1." AND TEI_KEY2 = ".$AWIS_KEY2;
				
									$rsTEI = $this->_DB->RecordSetOeffnen($SQL);
										
									if ($rsTEI->AnzahlDatensaetze()==0)
									{
										//Eintrag in Teileinfos ATUNR -> OENR
										$SQL = 'INSERT INTO TeileInfos';
										$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
										$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
										$SQL .= 'VALUES(';
										$SQL .= ' \'AST\'';
										$SQL .= ','.$AWIS_KEY1;
										$SQL .= ',suchwort(\''.$ATUNR.'\')';
										$SQL .= ',\''.$ATUNR.'\'';
										$SQL .= ',\'OEN\'';
										$SQL .= ','.$AWIS_KEY2;
										$SQL .= ',asciiwortoe(\''.$OE.'\')';
										$SQL .= ',\''.$OE.'\'';
										$SQL .= ',\'Import\'';						
										$SQL .= ',SYSDATE';
										$SQL .= ')';
				
										$this->_DB->Ausfuehren($SQL,'',true);
									}
								}
							}
							
							$Meldung.= ' TRW / Gabriel / OENR wurden zugeordnet';
													
						}
						else 
						{
							//TRW - Nummer ist bereits bei einer anderen ATU - Nummer zugeordnet
							$Meldung.=' TRW ist bereits bei einer anderen ATU - Nummer '.$rsAST_TRW3->FeldInhalt('AST_ATUNR').' zugeordnet';
						}
					}		
					else 
					{
						//TRW - Nummer ist auch bereits bei dieser ATU - Nummer zugeordnet
						$Meldung.=' TRW ist auch bereits bei ATUNR '.$ATUNR.' zugeordnet';
					}					
					
					//Gabriel auch Zuordnen
					//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
					$SQL = ' SELECT * ';
					$SQL .= ' FROM LIEFERANTENARTIKEL ';
					$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('GABRIEL'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
					$SQL .= " AND (LAR_LIE_NR) = '9146'";
	
					$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);
	
					//Wenn LIEFARTNR + LIEFNR nicht in AWIS
					if($rsLARTNR->AnzahlDatensaetze()==0)
					{
						//neue LiefArtNr anlegen
						$SQL = 'INSERT INTO Lieferantenartikel';
						$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
						$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
						$SQL .= ',LAR_USER, LAR_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('GABRIEL'),false);
						$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','9146',false).',4,0)';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
						$SQL .= ',0';
						$SQL .= ',4';		// Importquelle
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
						$SQL .= ',\'Import\'';								
						$SQL .= ',SYSDATE';
						$SQL .= ')';
	
						$this->_DB->Ausfuehren($SQL,'',true);
						
						$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' angelegt.';
					}
					else
					{
						$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
	
						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' vorhanden.';
					}
					

					//Pr�fen, ob schon zugeordnet
					$SQL = ' SELECT * ';
					$SQL .= ' FROM TEILEINFOS ';
					$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_ITY_ID2 = \'LAR\'';
					$SQL .= ' AND TEI_KEY1 = '.$AWIS_KEY1.' AND TEI_KEY2 = '.$AWIS_KEY2;
				
					$rsTEI = $this->_DB->RecordSetOeffnen($SQL);
							
					if ($rsTEI->AnzahlDatensaetze()==0)
					{					
						//Eintrag in Teileinfos ATUNR -> LARTNR
						$SQL = 'INSERT INTO TeileInfos';
						$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
						$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
						$SQL .= 'VALUES(';
						$SQL .= ' \'AST\'';
						$SQL .= ','.$AWIS_KEY1;
						$SQL .= ',suchwort(\''.$ATUNR.'\')';
						$SQL .= ',\''.$ATUNR.'\'';
						$SQL .= ',\'LAR\'';
						$SQL .= ','.$AWIS_KEY2;
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
						$SQL .= ',\''.$rsLucas->Feldinhalt('GABRIEL').'\'';
						$SQL .= ',\'Import\'';							
						$SQL .= ',SYSDATE';
						$SQL .= ')';
		
						$this->_DB->Ausfuehren($SQL,'',true);
					}
					
					if ($rsLucas->Feldinhalt('OENR')!='')
					{
						$OENR = explode(',',$rsLucas->Feldinhalt('OENR'));
						
						foreach ($OENR as $OE)
						{
							$OE = trim($OE);
										
							//Pr�fen, ob OENR bereits in Tabelle OENUMMERN vorhanden (keine Zuordnung ?)
							$SQL = ' SELECT * ';
							$SQL .= ' FROM OENUMMERN ';
							$SQL .= " WHERE (OEN_SUCHNUMMER) " . $this->_DB->LIKEoderIST($OE, awisDatenbank::AWIS_LIKE_SUCHOE);
	
							$rsOENR = $this->_DB->RecordSetOeffnen($SQL);
		
							//Wenn OENR nicht in AWIS
							if($rsOENR->AnzahlDatensaetze()==0)
							{
								//neue OENR anlegen
								$SQL = 'INSERT INTO OENummern';
								$SQL .= '(OEN_NUMMER, OEN_HER_ID';
								$SQL .= ',OEN_MUSTER,OEN_SUCHNUMMER';
								$SQL .= ',OEN_USER, OEN_USERDAT';
								$SQL .= ')VALUES (';
								$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',str_replace('�', '',$OE),false);//Weiches Trennzeichen/Soft Hyphen muss ersetzt werden
								$SQL .= ',0';
								$SQL .= ',0';
								$SQL .= ',asciiwortoe(' . $this->_DB->FeldInhaltFormat('T',$OE,false).')';
								$SQL .= ',\'Import\'';							
								$SQL .= ',SYSDATE';
								$SQL .= ')';
	
								$this->_DB->Ausfuehren($SQL,'',true);

								$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
								$rsKey = $this->_DB->RecordSetOeffnen($SQL);
								$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
								$MELDUNGOENR = $OE.' angelegt.';
							}
							else
							{
								$AWIS_KEY2=$rsOENR->FeldInhalt('OEN_KEY');
	
								$MELDUNGOENR = $OE.' vorhanden.';
							}
		
							//Pr�fen, ob schon zugeordnet
							$SQL = ' SELECT * ';
							$SQL .= ' FROM TEILEINFOS ';
							$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND TEI_ITY_ID2 = \'OEN\'';
							$SQL .= " AND TEI_KEY1 = ".$AWIS_KEY1." AND TEI_KEY2 = ".$AWIS_KEY2;
		
							$rsTEI = $this->_DB->RecordSetOeffnen($SQL);
								
							if ($rsTEI->AnzahlDatensaetze()==0)
							{
								//Eintrag in Teileinfos ATUNR -> OENR
								$SQL = 'INSERT INTO TeileInfos';
								$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
								$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
								$SQL .= 'VALUES(';
								$SQL .= ' \'AST\'';
								$SQL .= ','.$AWIS_KEY1;
								$SQL .= ',suchwort(\''.$ATUNR.'\')';
								$SQL .= ',\''.$ATUNR.'\'';
								$SQL .= ',\'OEN\'';
								$SQL .= ','.$AWIS_KEY2;
								$SQL .= ',asciiwortoe(\''.$OE.'\')';
								$SQL .= ',\''.$OE.'\'';
								$SQL .= ',\'Import\'';						
								$SQL .= ',SYSDATE';
								$SQL .= ')';
		
								$this->_DB->Ausfuehren($SQL,'',true);
							}
						}
					}
					$Meldung.= ' Gabriel / OENR wurden zugeordnet';
					$rsAST_Lucas->DSWeiter();
				}
			}
			
			$SQL = 'UPDATE XXX_LUCAS SET MELDUNG = \''.$Meldung.'\' where zeile = '.$rsLucas->FeldInhalt('ZEILE');
			
			$this->_DB->Ausfuehren($SQL,'',true);
			
			$rsLucas->DSWeiter();
		}
	}
	
	public function import_artikel_lucas_2()
	{
		$SQL = 'SELECT * FROM ARTIKELSTAMM WHERE AST_ATUNR LIKE \'�I%\' and ast_bezeichnung is null order by ast_atunr';
		$rsAST = $this->_DB->RecordSetOeffnen($SQL);
		
		while (!$rsAST->EOF())	
		{
			$SQL = 'SELECT * FROM XXX_LUCAS WHERE MELDUNG LIKE \'%Neuer Artikel: '.$rsAST->FeldInhalt('AST_ATUNR').'\'';
			echo $SQL.PHP_EOL;

			$rsLucas = $this->_DB->RecordSetOeffnen($SQL);
			
			if ($rsLucas->AnzahlDatensaetze()==0)
			{
				echo 'Fehler bei '.$rsAST->FeldInhalt('AST_ATUNR');
			}
			else 
			{
				$SQL = 'update artikelstamm set ';
				$SQL.= ' AST_BEZEICHNUNG = '.$this->_DB->FeldInhaltFormat('T',$rsLucas->FeldInhalt('GAS_OEL').', '.$rsLucas->FeldInhalt('VORNE_HINTEN'));
				$SQL.= ' where ast_key='.$rsAST->FeldInhalt('AST_KEY');
				
				$this->_DB->Ausfuehren($SQL,'',true);
			}
			
			$rsAST->DSWeiter();
		}
	}
	
	public function import_artikel_lucas_3()
	{
		$SQL = 'select *';
		$SQL.= ' from xxx_lucas_2';
		$SQL.= ' inner join lieferantenartikel on lar_such_lartnr = lucas and lar_lie_nr = \'0831\'';
		$SQL.= ' inner join teileinfos on tei_key2=lar_key and tei_ity_id1=\'AST\' and tei_ity_id2=\'LAR\'';
		$SQL.= ' inner join artikelstamm on ast_key = tei_key1';
		$SQL.= ' where ast_key is not null and ast_atunr not like \'EM%\'';
		$SQL.= ' order by zeile';

		$rsAST = $this->_DB->RecordSetOeffnen($SQL);
		echo $SQL;
		die();
		while (!$rsAST->EOF())	
		{
			//Artikelnummer umschreiben
			$SQL = 'SELECT MAX(substr(AST_ATUNR,3))+1 as ATUNR FROM ARTIKELSTAMM WHERE AST_ATUNR LIKE \'EM1%\'';
			
			$rsATUNR = $this->_DB->RecordSetOeffnen($SQL);
			
			if ($rsATUNR->FeldInhalt('ATUNR')=='')
			{
				$ATUNR = 'EM1000';				
			}
			else 
			{
				$ATUNR = 'EM'.$rsATUNR->FeldInhalt('ATUNR');
			}
			
			$SQL = 'update artikelstamm set ast_kennungvorschlag = \'S\',';
			$SQL.= ' AST_ATUNR = \''.$ATUNR.'\'';
			$SQL.= ', AST_user=\'Import\'';
			$SQL.= ', AST_IMQ_ID = CASE WHEN BITAND(AST_IMQ_ID,4)=4 THEN AST_IMQ_ID ELSE AST_IMQ_ID+4 END';
			$SQL.= ', AST_userdat=sysdate';			
			$SQL.= ' where ast_key='.$rsAST->FeldInhalt('AST_KEY');
			
			$this->_DB->Ausfuehren($SQL,'',true);
			
			$SQL = 'DELETE FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = \''.$ATUNR.'\' AND ASI_AIT_ID in (60,100)';
			
			//echo $SQL.'<br>';		
			
			$this->_DB->Ausfuehren($SQL,'',true);
			
			$SQL = 'INSERT INTO ARTIKELSTAMMINFOS (ASI_AST_ATUNR, ASI_AIT_ID, ASI_WERT, ASI_IMQ_ID, ASI_USER, ASI_USERDAT) VALUES (';
			$SQL.= ' \''.$ATUNR .'\', 60, \'1\', 4, \'Import\', sysdate)';
			//echo $SQL.'<br>';		
			
			$this->_DB->Ausfuehren($SQL,'',true);
						
			$SQL = 'INSERT INTO ARTIKELSTAMMINFOS (ASI_AST_ATUNR, ASI_AIT_ID, ASI_WERT, ASI_IMQ_ID, ASI_USER, ASI_USERDAT) VALUES (';
			$SQL.= ' \''.$ATUNR .'\', 100, \'17.02.2011\', 4, \'Import\', sysdate)';
			//echo $SQL.'<br>';		
			
			$this->_DB->Ausfuehren($SQL,'',true);
									
			$rsAST->DSWeiter();
		}
	}
	
	public function import_artikel_lucas_4()
	{
		$SQL = 'SELECT * FROM XXX_LUCAS ORDER BY ZEILE';
		
		$rsLucas = $this->_DB->RecordSetOeffnen($SQL);

		while(!$rsLucas->EOF())
		{
			// Pr�fen, ob TRW - Nummer schon eine EM ATU-Nr (Nur EM%, @% bzw. �%) zugeordnet ist				
			$SQL = ' SELECT DISTINCT AST_ATUNR, AST_KEY ';
			$SQL .= ' FROM Artikelstamm';
			$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
			$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
			$SQL .= ' INNER JOIN TeileInfos ON AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
			$SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('TRW'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER );
			$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
			$SQL .= " AND LAR_LIE_NR = '0831')";
			$SQL .= " WHERE AST_ATUNR LIKE 'EM%'";
			
			$rsAST_TRW = $this->_DB->RecordSetOeffnen($SQL);
				
			if ($rsAST_TRW->AnzahlDatensaetze()>0)
			{
				$AWIS_KEY1 = $rsAST_TRW->FeldInhalt('AST_KEY');
				$ATUNR = $rsAST_TRW->FeldInhalt('AST_ATUNR');
				
				//Pr�fen, ob schone eine Lucas - Nummer schon zu der ATU - Nr zugeordnet ist
				$SQL = ' SELECT DISTINCT AST_ATUNR, AST_KEY ';
				$SQL .= ' FROM Artikelstamm';
				$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
				$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
				$SQL .= ' INNER JOIN TeileInfos ON AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
				$SQL .= " AND (TEI_SUCH2) LIKE 'L%'";
				$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
				$SQL .= " AND LAR_LIE_NR = '0831')";
				$SQL .= " WHERE AST_ATUNR = '".$rsAST_TRW->FeldInhalt('AST_ATUNR')."'";
			
				$rsAST_Lucas = $this->_DB->RecordSetOeffnen($SQL);	
				
				if ($rsAST_Lucas->AnzahlDatensaetze()==0)
				{					
					//Lucas - Nr hier zuordnen
					//Gabriel - Nr hier zuordnen
					//OENummer nicht!!!
					
					//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
					$SQL = ' SELECT * ';
					$SQL .= ' FROM LIEFERANTENARTIKEL ';
					$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('LUCAS'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
					$SQL .= " AND (LAR_LIE_NR) = '0831'";
	
					$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

					//Wenn LIEFARTNR + LIEFNR nicht in AWIS
					if($rsLARTNR->AnzahlDatensaetze()==0)
					{
						//neue LiefArtNr anlegen
						$SQL = 'INSERT INTO Lieferantenartikel';
						$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
						$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
						$SQL .= ',LAR_USER, LAR_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('LUCAS'),false);
						$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','0831',false).',4,0)';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
						$SQL .= ',0';
						$SQL .= ',4';		// Importquelle
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
						$SQL .= ',\'Import\'';								
						$SQL .= ',SYSDATE';
						$SQL .= ')';
	
						$this->_DB->Ausfuehren($SQL,'',true);
												
						$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' angelegt.';
					}
					else
					{
						$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
	
						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' vorhanden.';
					}
				
					//Eintrag in Teileinfos ATUNR -> LARTNR
					$SQL = 'INSERT INTO TeileInfos';
					$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
					$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
					$SQL .= 'VALUES(';
					$SQL .= ' \'AST\'';
					$SQL .= ','.$AWIS_KEY1;
					$SQL .= ',suchwort(\''.$ATUNR.'\')';
					$SQL .= ',\''.$ATUNR.'\'';
					$SQL .= ',\'LAR\'';
					$SQL .= ','.$AWIS_KEY2;
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
					$SQL .= ',\''.$rsLucas->Feldinhalt('LUCAS').'\'';
					$SQL .= ',\'Import\'';							
					$SQL .= ',SYSDATE';
					$SQL .= ')';
	
					$this->_DB->Ausfuehren($SQL,'',true);
													
					//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
					$SQL = ' SELECT * ';
					$SQL .= ' FROM LIEFERANTENARTIKEL ';
					$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('GABRIEL'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
					$SQL .= " AND (LAR_LIE_NR) = '9146'";
	
					$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);
	
					//Wenn LIEFARTNR + LIEFNR nicht in AWIS
					if($rsLARTNR->AnzahlDatensaetze()==0)
					{
						//neue LiefArtNr anlegen
						$SQL = 'INSERT INTO Lieferantenartikel';
						$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
						$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
						$SQL .= ',LAR_USER, LAR_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('GABRIEL'),false);
						$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','9146',false).',4,0)';
						$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
						$SQL .= ',0';
						$SQL .= ',4';		// Importquelle
						$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
						$SQL .= ',\'Import\'';								
						$SQL .= ',SYSDATE';
						$SQL .= ')';
	
						$this->_DB->Ausfuehren($SQL,'',true);
												
						$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
	
						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' angelegt.';
					}
					else
					{
						$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');
	
						$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' vorhanden.';
					}
				
					//Eintrag in Teileinfos ATUNR -> LARTNR
					$SQL = 'INSERT INTO TeileInfos';
					$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
					$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
					$SQL .= 'VALUES(';
					$SQL .= ' \'AST\'';
					$SQL .= ','.$AWIS_KEY1;
					$SQL .= ',suchwort(\''.$ATUNR.'\')';
					$SQL .= ',\''.$ATUNR.'\'';
					$SQL .= ',\'LAR\'';
					$SQL .= ','.$AWIS_KEY2;
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
					$SQL .= ',\''.$rsLucas->Feldinhalt('GABRIEL').'\'';
					$SQL .= ',\'Import\'';							
					$SQL .= ',SYSDATE';
					$SQL .= ')';
	
					$this->_DB->Ausfuehren($SQL,'',true);
					
					$Meldung= ' TRW vorhanden bei ATUNR '.$ATUNR.' Lucas und Gabriel wurden zugeordnet';
					
					$SQL = 'UPDATE XXX_LUCAS SET MELDUNG = \''.$Meldung.'\' where zeile = '.$rsLucas->FeldInhalt('ZEILE');
					
					$this->_DB->Ausfuehren($SQL,'',true);

				}
			}
			
			$rsLucas->DSWeiter();
		}
	}
	
	public function import_artikel_lucas_5()
	{	
		//2.Schritt
		//Pr�fen ob zu der Lucas - Nummer schon eine ATU-Nr (Nur EM%, @% bzw. �%) zugeordnet ist
		$SQL = 'SELECT * FROM XXX_LUCAS ORDER BY ZEILE';
		
		$rsLucas = $this->_DB->RecordSetOeffnen($SQL);
		
		while(!$rsLucas->EOF())
		{		
			$SQL = ' SELECT DISTINCT AST_ATUNR, AST_KEY ';
			$SQL .= ' FROM Artikelstamm';
			$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
			$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
			$SQL .= ' INNER JOIN TeileInfos ON AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';
			$SQL .= " AND (TEI_SUCH2) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('LUCAS'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
			$SQL .= ' INNER JOIN Lieferantenartikel ON (TEI_KEY2 = LAR_KEY';
			$SQL .= " AND LAR_LIE_NR = '0831')";
			$SQL .= " WHERE AST_ATUNR LIKE 'EM%' or AST_ATUNR LIKE '@%' or AST_ATUNR LIKE '�%'";
			
			$rsAST_Lucas = $this->_DB->RecordSetOeffnen($SQL);			
			
			if ($rsAST_Lucas->AnzahlDatensaetze()==0)
			{
				//Neuen Artikel + TRW + LUCAS + GABRIEL + OENR			
				$Kennung = utf8_decode($this->_AWISBenutzer->ParameterLesen('DefaultKennungAutoArtikelnr'));

				$SQL = 'SELECT * FROM (SELECT AST_ATUNR FROM ARTIKELSTAMM ';
				$SQL .= ' WHERE SUBSTR(AST_ATUNR,1,'.strlen($Kennung).')=\''.$Kennung.'\'';
				$SQL .= ' ORDER BY nlssort(ast_atunr,\'NLS_SORT=BINARY\') DESC) WHERE ROWNUM = 1';
				$rsKey = $this->_DB->RecordSetOeffnen($SQL);
				$rsKey = $rsKey->Tabelle();					

				if(!isset($rsKey['AST_ATUNR'][0]) OR (isset($rsKey['AST_ATUNR'][0]) AND $rsKey['AST_ATUNR'][0]==''))
				{
					$ATUNR=$Kennung.'0001';
				}
				else
				{
				    $Teil1Kennung = substr($rsKey['AST_ATUNR'][0],0,1);
				    $Teil2Kennung = substr($rsKey['AST_ATUNR'][0],1);
					$ATUNR=$Teil1Kennung . ++$Teil2Kennung;
				}
						
				$WUG_KEY = $this->_AWISBenutzer->ParameterLesen('DefaultWUG_Key_NeueArtikel');

				//neue ATU-Pseudonummer anlegen
				$SQL = 'INSERT INTO Artikelstamm';
				$SQL .= '(AST_ATUNR, AST_BEZEICHNUNG';
				$SQL .= ',AST_WUG_KEY, AST_IMQ_ID, AST_USER, AST_USERDAT';
				$SQL .= ') VALUES (';
				$SQL .= ' ' . $this->_DB->FeldInhaltFormat('TU',$ATUNR,false);
				$SQL.= ' ,'.$this->_DB->FeldInhaltFormat('T',$rsLucas->FeldInhalt('GAS_OEL').', '.$rsLucas->FeldInhalt('VORNE_HINTEN'));
				$SQL .= ','.$this->_DB->FeldInhaltFormat('N0',$WUG_KEY);
				$SQL .= ',4';
				$SQL .= ',\'Import\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$this->_DB->Ausfuehren($SQL,'',true);
				
				$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
				$rsKey = $this->_DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');

				//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
				$SQL = ' SELECT * ';
				$SQL .= ' FROM LIEFERANTENARTIKEL ';
				$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('LUCAS'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
				$SQL .= " AND (LAR_LIE_NR) = '0831'";

				$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

				//Wenn LIEFARTNR + LIEFNR nicht in AWIS
				if($rsLARTNR->AnzahlDatensaetze()==0)
				{
					//neue LiefArtNr anlegen
					$SQL = 'INSERT INTO Lieferantenartikel';
					$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
					$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
					$SQL .= ',LAR_USER, LAR_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('LUCAS'),false);
					$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','0831',false).',4,0)';
					$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
					$SQL .= ',0';
					$SQL .= ',4';		// Importquelle
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
					$SQL .= ',\'Import\'';								
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);
										
					$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
					$rsKey = $this->_DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

					$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' angelegt.';
				}
				else
				{
					$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

					$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('LUCAS').' vorhanden.';
				}
				
				//Eintrag in Teileinfos ATUNR -> LARTNR
				$SQL = 'INSERT INTO TeileInfos';
				$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
				$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
				$SQL .= 'VALUES(';
				$SQL .= ' \'AST\'';
				$SQL .= ','.$AWIS_KEY1;
				$SQL .= ',suchwort(\''.$ATUNR.'\')';
				$SQL .= ',\''.$ATUNR.'\'';
				$SQL .= ',\'LAR\'';
				$SQL .= ','.$AWIS_KEY2;
				$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('LUCAS'),false).')';
				$SQL .= ',\''.$rsLucas->Feldinhalt('LUCAS').'\'';
				$SQL .= ',\'Import\'';							
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$this->_DB->Ausfuehren($SQL,'',true);
				
				//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
				$SQL = ' SELECT * ';
				$SQL .= ' FROM LIEFERANTENARTIKEL ';
				$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('TRW'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
				$SQL .= " AND (LAR_LIE_NR) = '0831'";

				$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

				//Wenn LIEFARTNR + LIEFNR nicht in AWIS
				if($rsLARTNR->AnzahlDatensaetze()==0)
				{
					//neue LiefArtNr anlegen
					$SQL = 'INSERT INTO Lieferantenartikel';
					$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
					$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
					$SQL .= ',LAR_USER, LAR_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('TRW'),false);
					$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','0831',false).',4,0)';
					$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
					$SQL .= ',0';
					$SQL .= ',4';		// Importquelle
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('TRW'),false).')';
					$SQL .= ',\'Import\'';								
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);
										
					$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
					$rsKey = $this->_DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

					$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('TRW').' angelegt.';
				}
				else
				{
					$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

					$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('TRW').' vorhanden.';
				}
				
				//Eintrag in Teileinfos ATUNR -> LARTNR
				$SQL = 'INSERT INTO TeileInfos';
				$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
				$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
				$SQL .= 'VALUES(';
				$SQL .= ' \'AST\'';
				$SQL .= ','.$AWIS_KEY1;
				$SQL .= ',suchwort(\''.$ATUNR.'\')';
				$SQL .= ',\''.$ATUNR.'\'';
				$SQL .= ',\'LAR\'';
				$SQL .= ','.$AWIS_KEY2;
				$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('TRW'),false).')';
				$SQL .= ',\''.$rsLucas->Feldinhalt('TRW').'\'';
				$SQL .= ',\'Import\'';							
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$this->_DB->Ausfuehren($SQL,'',true);
								
				//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
				$SQL = ' SELECT * ';
				$SQL .= ' FROM LIEFERANTENARTIKEL ';
				$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsLucas->Feldinhalt('GABRIEL'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
				$SQL .= " AND (LAR_LIE_NR) = '9146'";

				$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

				//Wenn LIEFARTNR + LIEFNR nicht in AWIS
				if($rsLARTNR->AnzahlDatensaetze()==0)
				{
					//neue LiefArtNr anlegen
					$SQL = 'INSERT INTO Lieferantenartikel';
					$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
					$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
					$SQL .= ',LAR_USER, LAR_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsLucas->Feldinhalt('GABRIEL'),false);
					$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T','9146',false).',4,0)';
					$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 15.02.2011 (EDV)',true);
					$SQL .= ',0';
					$SQL .= ',4';		// Importquelle
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
					$SQL .= ',\'Import\'';								
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);
										
					$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
					$rsKey = $this->_DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

					$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' angelegt.';
				}
				else
				{
					$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

					$MELDUNGLIEFARTNR = $rsLucas->Feldinhalt('GABRIEL').' vorhanden.';
				}
				
				//Eintrag in Teileinfos ATUNR -> LARTNR
				$SQL = 'INSERT INTO TeileInfos';
				$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
				$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
				$SQL .= 'VALUES(';
				$SQL .= ' \'AST\'';
				$SQL .= ','.$AWIS_KEY1;
				$SQL .= ',suchwort(\''.$ATUNR.'\')';
				$SQL .= ',\''.$ATUNR.'\'';
				$SQL .= ',\'LAR\'';
				$SQL .= ','.$AWIS_KEY2;
				$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsLucas->Feldinhalt('GABRIEL'),false).')';
				$SQL .= ',\''.$rsLucas->Feldinhalt('GABRIEL').'\'';
				$SQL .= ',\'Import\'';							
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$this->_DB->Ausfuehren($SQL,'',true);
								
				if ($rsLucas->Feldinhalt('OENR')!='')
				{
					$OENR = explode(',',$rsLucas->Feldinhalt('OENR'));					
					foreach ($OENR as $OE)
					{
						$OE = trim($OE);
									
						//Pr�fen, ob OENR bereits in Tabelle OENUMMERN vorhanden (keine Zuordnung ?)
						$SQL = ' SELECT * ';
						$SQL .= ' FROM OENUMMERN ';
						$SQL .= " WHERE (OEN_SUCHNUMMER) " . $this->_DB->LIKEoderIST($OE, awisDatenbank::AWIS_LIKE_SUCHOE);

						$rsOENR = $this->_DB->RecordSetOeffnen($SQL);

						//Wenn OENR nicht in AWIS
						if($rsOENR->AnzahlDatensaetze()==0)
						{
							//neue OENR anlegen
							$SQL = 'INSERT INTO OENummern';
							$SQL .= '(OEN_NUMMER, OEN_HER_ID';
							$SQL .= ',OEN_MUSTER,OEN_SUCHNUMMER';
							$SQL .= ',OEN_USER, OEN_USERDAT';
							$SQL .= ')VALUES (';
							$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',str_replace('�', '',$OE),false);//Weiches Trennzeichen/Soft Hyphen muss ersetzt werden
							$SQL .= ',0';
							$SQL .= ',0';
							$SQL .= ',asciiwortoe(' . $this->_DB->FeldInhaltFormat('T',$OE,false).')';
							$SQL .= ',\'Import\'';							
							$SQL .= ',SYSDATE';
							$SQL .= ')';

							$this->_DB->Ausfuehren($SQL,'',true);

							$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
							$rsKey = $this->_DB->RecordSetOeffnen($SQL);
							$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

							$MELDUNGOENR = $OE.' angelegt.';
						}
						else
						{
							$AWIS_KEY2=$rsOENR->FeldInhalt('OEN_KEY');

							$MELDUNGOENR = $OE.' vorhanden.';
						}

						//Eintrag in Teileinfos ATUNR -> OENR
						$SQL = 'INSERT INTO TeileInfos';
						$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
						$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
						$SQL .= 'VALUES(';
						$SQL .= ' \'AST\'';
						$SQL .= ','.$AWIS_KEY1;
						$SQL .= ',suchwort(\''.$ATUNR.'\')';
						$SQL .= ',\''.$ATUNR.'\'';
						$SQL .= ',\'OEN\'';
						$SQL .= ','.$AWIS_KEY2;
						$SQL .= ',asciiwortoe(\''.$OE.'\')';
						$SQL .= ',\''.$OE.'\'';
						$SQL .= ',\'Import\'';						
						$SQL .= ',SYSDATE';
						$SQL .= ')';

						$this->_DB->Ausfuehren($SQL,'',true);

					}
				}
				$Meldung = 'Zeile: '.$rsLucas->FeldInhalt('ZEILE'). ' Neuer Artikel: '.$ATUNR;								
					
				$SQL = 'UPDATE XXX_LUCAS SET MELDUNG = \''.$Meldung.'\' where zeile = '.$rsLucas->FeldInhalt('ZEILE');
				
				$this->_DB->Ausfuehren($SQL,'',true);

			}
			else 
			{
				$Meldung= 'Zeile: '.$rsLucas->FeldInhalt('ZEILE'). ' Lucas vorhanden bei ATUNR '.$rsAST_Lucas->FeldInhalt('AST_ATUNR');
				
				$SQL = 'UPDATE XXX_LUCAS SET MELDUNG = \''.$Meldung.'\' where zeile = '.$rsLucas->FeldInhalt('ZEILE');
				
				$this->_DB->Ausfuehren($SQL,'',true);

			}
			
			$rsLucas->DSWeiter();
		}
	}

	public function import_vi()
	{
		//Daten selektieren
		$SQL = 'SELECT * FROM XXX_TEST_2000 order by zeile';
		
		$rsVI=$this->_DB->RecordSetOeffnen($SQL);
		
		while (!$rsVI->EOF())
		{					
			$WUG_KEY = $this->_AWISBenutzer->ParameterLesen('DefaultWUG_Key_NeueArtikel');

			$SQL = 'SELECT * FROM ARTIKELSTAMM WHERE AST_ATUNR = '.$this->_DB->FeldInhaltFormat('T',$rsVI->Feldinhalt('ATUNR'),false);
			
			$rsAST = $this->_DB->RecordSetOeffnen($SQL);
			
			if ($rsAST->AnzahlDatensaetze()==0)
			{			
				//neue ATU-Pseudonummer anlegen
				$SQL = 'INSERT INTO Artikelstamm';
				$SQL .= '(AST_ATUNR, AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG';
				$SQL .= ',AST_WUG_KEY, AST_IMQ_ID, AST_USER, AST_USERDAT';
				$SQL .= ') VALUES (';
				$SQL .= ' ' . $this->_DB->FeldInhaltFormat('TU',$rsVI->Feldinhalt('ATUNR'),false);
				$SQL.= ' ,'.$this->_DB->FeldInhaltFormat('T',$rsVI->Feldinhalt('BEZEICHNUNG'));
				$SQL.= ' ,'.$this->_DB->FeldInhaltFormat('T',$rsVI->Feldinhalt('KENNUNG'));
				$SQL .= ','.$this->_DB->FeldInhaltFormat('N0',$WUG_KEY);
				$SQL .= ',4';
				$SQL .= ',\'Import\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$this->_DB->Ausfuehren($SQL,'',true);
				
				$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
				$rsKey = $this->_DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
				
				
				$SQL = 'INSERT INTO ARTIKELSTAMMINFOS (ASI_AST_ATUNR, ASI_AIT_ID, ASI_WERT, ASI_IMQ_ID, ASI_USER, ASI_USERDAT) VALUES (';
				$SQL.= ' '.$this->_DB->FeldInhaltFormat('TU',$rsVI->Feldinhalt('ATUNR')) .', ';
				$SQL.= ' 60, ';
				$SQL.= ' '.$this->_DB->FeldInhaltFormat('T',$rsVI->Feldinhalt('VPE')) .', ';
				$SQL.= '4, \'Import\', sysdate)';
				
				$this->_DB->Ausfuehren($SQL,'',true);
								
				$SQL = 'INSERT INTO ARTIKELSTAMMINFOS (ASI_AST_ATUNR, ASI_AIT_ID, ASI_WERT, ASI_IMQ_ID, ASI_USER, ASI_USERDAT) VALUES (';
				$SQL.= ' '.$this->_DB->FeldInhaltFormat('TU',$rsVI->Feldinhalt('ATUNR')) .', ';
				$SQL.= ' 100, ';
				$SQL.= ' '.$this->_DB->FeldInhaltFormat('D',$rsVI->Feldinhalt('EINKAUFAM')) .', ';
				$SQL.= '4, \'Import\', sysdate)';
				
				$this->_DB->Ausfuehren($SQL,'',true);
				
				//Pr�fen, ob LIEFARTNR + LIEFNR bereits in Tabelle Lieferantenartikel vorhanden (keine Zuordnung ?)
				$SQL = ' SELECT * ';
				$SQL .= ' FROM LIEFERANTENARTIKEL ';
				$SQL .= " WHERE (LAR_SUCH_LARTNR) " . $this->_DB->LIKEoderIST($rsVI->Feldinhalt('LIEFARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
				$SQL .= " AND (LAR_LIE_NR) = ".$this->_DB->FeldInhaltFormat('T',$rsVI->Feldinhalt('LIEFNR'));

				$rsLARTNR = $this->_DB->RecordSetOeffnen($SQL);

				//Wenn LIEFARTNR + LIEFNR nicht in AWIS
				if($rsLARTNR->AnzahlDatensaetze()==0)
				{
					//neue LiefArtNr anlegen
					$SQL = 'INSERT INTO Lieferantenartikel';
					$SQL .= '(LAR_LARTNR, LAR_LIE_NR,LAR_BEMERKUNGEN';
					$SQL .= ',LAR_BEKANNTWW,LAR_IMQ_ID,LAR_SUCH_LARTNR';
					$SQL .= ',LAR_USER, LAR_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $this->_DB->FeldInhaltFormat('T',$rsVI->Feldinhalt('LIEFARTNR'),false);
					$SQL .= ', lpad('.$this->_DB->FeldInhaltFormat('T',$rsVI->Feldinhalt('LIEFNR')).',4,0)';
					$SQL .= ',' . $this->_DB->FeldInhaltFormat('T','Import 02.08.2011 (EDV) laut Liste von Hr. Schwartz',true);
					$SQL .= ',0';
					$SQL .= ',4';		// Importquelle
					$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsVI->Feldinhalt('LIEFARTNR'),false).')';
					$SQL .= ',\'schwartz_r\'';								
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$this->_DB->Ausfuehren($SQL,'',true);
										
					$SQL = 'SELECT seq_global_key.CurrVal AS KEY FROM DUAL';
					$rsKey = $this->_DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

					$MELDUNGLIEFARTNR = $rsVI->Feldinhalt('LIEFARTNR').' angelegt.';
				}
				else
				{
					$AWIS_KEY2=$rsLARTNR->FeldInhalt('LAR_KEY');

					$MELDUNGLIEFARTNR = $rsVI->Feldinhalt('LIEFARTNR').' vorhanden.';
				}
				
				//Eintrag in Teileinfos ATUNR -> LARTNR
				$SQL = 'INSERT INTO TeileInfos';
				$SQL .= '(TEI_ITY_ID1,TEI_KEY1,TEI_SUCH1,TEI_WERT1';
				$SQL .= ',TEI_ITY_ID2,TEI_KEY2,TEI_SUCH2,TEI_WERT2,TEI_USER,TEI_USERDAT)';
				$SQL .= 'VALUES(';
				$SQL .= ' \'AST\'';
				$SQL .= ','.$AWIS_KEY1;
				$SQL .= ',suchwort('.$this->_DB->FeldInhaltFormat('TU',$rsVI->Feldinhalt('ATUNR')).')';
				$SQL .= ','.$this->_DB->FeldInhaltFormat('TU',$rsVI->Feldinhalt('ATUNR'));
				$SQL .= ',\'LAR\'';
				$SQL .= ','.$AWIS_KEY2;
				$SQL .= ',asciiwort(' . $this->_DB->FeldInhaltFormat('TU',$rsVI->Feldinhalt('LIEFARTNR'),false).')';
				$SQL .= ',\''.$rsVI->Feldinhalt('LIEFARTNR').'\'';
				$SQL .= ',\'schwartz_r\'';							
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$this->_DB->Ausfuehren($SQL,'',true);
								
				echo 'Zeile: '.$rsVI->FeldInhalt('ZEILE').': Neuer Artikel '.$rsVI->Feldinhalt('ATUNR').'. Lieferatenartikel '.$MELDUNGLIEFARTNR.'<br>';			
			}
			
			else 
			{
				echo 'Zeile: '.$rsVI->FeldInhalt('ZEILE').': FEHLER: Artikel '.$rsVI->Feldinhalt('ATUNR').' bereits vorhanden!<br>';			
			}
			
			$rsVI->DSWeiter();
		}
	}	
}

?>