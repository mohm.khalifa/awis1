<?php
require_once('awisKontakt.inc');
/**
 * Bereichsverwaltung f�r die Personaleins�tze
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200809161558
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PPR','%');
	$TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
	$TextKonserven[]=array('XBN','XBN_NAME');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AnzahlEintraege');
	$TextKonserven[]=array('Wort','AnzahlFehler');
	$TextKonserven[]=array('Wort','AktuellerStand');
	$TextKonserven[]=array('Wort','FehlerMeldung');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Liste','PPR_STATUS');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4501 = $AWISBenutzer->HatDasRecht(4501);
	if($Recht4501==0)
	{
		$Form->Fehler_KeineRechte();
	}


	//********************************************************
	// Parameter verarbeiten
	//********************************************************
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./personaleinsaetze_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PPR'));
	}
	elseif(isset($_POST['cmdWeiter_x']))
	{
		$AWIS_KEY1 = 0;
	}
	elseif(isset($_GET['PPR_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PPR_KEY']);
	}
	elseif(isset($_POST['txtPPR_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPPR_KEY']);
	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PPR'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_PPR',serialize($Param));
		}

		if(isset($_GET['PPRListe']))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY PPR_TAG DESC';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	if((!isset($_POST['sucPEB_KEY']) OR isset($_GET['PPRListe'])))
	{
		$Form->SchreibeHTMLCode('<form name="frmPersEinsPruefungen" action="./personaleinsaetze_Main.php?cmdAktion=Probleme" method="POST"  enctype="multipart/form-data">');

		$Form->Formular_Start();

		//******************************
		// Bearbeitungsbereich
		//******************************
		$SQL = 'SELECT peb_key, peb_bezeichnung';
		$SQL .= ' FROM Perseinsbereichemitglieder ';
		$SQL .= ' INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY';
		$SQL .= ' INNER JOIN PersEinsBereicheRechteVergabe ON PBM_KEY = PBR_PBM_KEY AND PBR_PBZ_KEY = 9';
		$SQL .= ' LEFT OUTER JOIN Benutzer ON pbm_xbn_key = xbn_key';
		$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
		//Perseins�tze Rechte pr�fen
		$SQL .= ' AND PBM_GUELTIGAB <= SYSDATE';
		$SQL .= ' AND PBM_GUELTIGBIS >= SYSDATE';
		$SQL .= ' ORDER BY peb_bezeichnung';
		$rsPEB = $DB->RecordSetOeffnen($SQL);
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEB']['PEB_BEZEICHNUNG'].':',190);
		$Form->Erstelle_SelectFeld('*PEB_KEY','',100,true,$SQL,'','','','','','');
		$AWISCursorPosition = 'sucPEB_KEY';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PPR']['PPR_TAG'].':',190);
		$Form->Erstelle_TextFeld('*PPR_TAG',date('d.m.Y'),10,350,true,'','','','D');
		$Form->ZeileEnde();

		$Form->Trennzeile();

		// Aktueller Stand
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['AktuellerStand'],192+254+452);
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEB']['PEB_BEZEICHNUNG'],190);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PPR']['PPR_STATUS'],250);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PPR']['PPR_BEMERKUNG'],450);
		$Form->ZeileEnde();

		$SQL = 'SELECT peb_key, peb_bezeichnung, ppr_status, ppr_bemerkung';
		$SQL .= ' FROM Perseinsbereichemitglieder ';
		$SQL .= ' INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY';
		$SQL .= ' INNER JOIN PersEinsBereicheRechteVergabe ON PBM_KEY = PBR_PBM_KEY AND PBR_PBZ_KEY = 9';
		$SQL .= ' LEFT OUTER JOIN PersEinsPruefungen ON PPR_PEB_KEY = PEB_KEY AND PPR_TAG = '.$DB->FeldInhaltFormat('D',date('d.m.Y'),false);
		$SQL .= ' LEFT OUTER JOIN Benutzer ON pbm_xbn_key = xbn_key';
		$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
		//Perseinsaetze Rechte pr�fen
		$SQL .= ' AND PBM_GUELTIGAB <= SYSDATE';
		$SQL .= ' AND PBM_GUELTIGBIS >= SYSDATE';
		$SQL .= ' ORDER BY peb_bezeichnung';
		$rsPEB = $DB->RecordSetOeffnen($SQL);
		while(!$rsPEB->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($rsPEB->FeldInhalt('PEB_BEZEICHNUNG').':',190);
			$Daten = $Form->WerteListe($AWISSprachKonserven['Liste']['PPR_STATUS'],$rsPEB->FeldInhalt('PPR_STATUS'));
			$Form->Erstelle_TextFeld('*PPR_STATUS',$Daten,10,250,false,'','','','T');
			$Form->Erstelle_TextFeld('*PPR_BEMERKUNG',$rsPEB->FeldInhalt('PPR_BEMERKUNG'),10,450,false,'','','','T');
			$Form->ZeileEnde();


			$rsPEB->DSWeiter();
		}


		$Form->Formular_Ende();

		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=probleme','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_Hilfe'], 'H');
		$Form->SchaltflaechenEnde();
	}
	else
	{
		//**************************************
		// Rechte des Benutzers ermitteln
		//**************************************
		$SQL = 'SELECT DISTINCT PBR_PBZ_KEY, PBM_PEB_KEY ';
		$SQL .= ' FROM PerseinsbereicheRechteVergabe';
		$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBR_PBM_KEY = PBM_KEY';
		$SQL .= ' INNER JOIN benutzer ON PBM_XBN_KEY = XBN_KEY AND XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
		//Perseinsaetze Rechte pr�fen
		$SQL .= ' WHERE PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
		$SQL .= ' AND PBM_PEB_KEY ='.$DB->FeldInhaltFormat('N0',$_POST['sucPEB_KEY'],false);
		$SQL .= ' AND (XBN_STATUS = \'A\' OR XBN_KEY = 0'.$AWISBenutzer->BenutzerID().')';
		$SQL .= ' ORDER BY PBR_PBZ_KEY ';
		$rsPBR = $DB->RecordSetOeffnen($SQL);
		$RechtPEI = 0;
		while(!$rsPBR->EOF())
		{
			$RechtPEI = $RechtPEI  | pow(2,($rsPBR->FeldInhalt('PBR_PBZ_KEY')-1));
			$rsPBR->DSWeiter();
		}

		//****************************************************
		// Details zu einem Tag anzeigen
		//****************************************************

			// evtl. fehlende Eintraege anlegen
		_ErzeugePruefungen($DB->FeldInhaltFormat('D',(isset($_POST['sucPPR_TAG'])?$_POST['sucPPR_TAG']:date('d.m.Y'))));

		$SQL = 'SELECT DISTINCT PersEinsPruefungen.*';
		$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
		$SQL .= ' FROM PersEinsPruefungen';
		$SQL .= ' INNER JOIN PersEinsBereiche ON PPR_PEB_KEY = PEB_KEY';

		$Bedingung='';

		if(isset($_POST['sucPEB_KEY']))
		{
			$Bedingung .= ' AND PPR_PEB_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['sucPEB_KEY'],false);
			if($_POST['sucPPR_TAG']!='')
			{
				$Bedingung .= ' AND PPR_TAG = '.$DB->FeldInhaltFormat('D',$_POST['sucPPR_TAG'],false);
			}
			$AWIS_KEY1=0;
		}
		else
		{
			if($AWIS_KEY1!=0)
			{
				$Bedingung.= ' AND PPR_KEY = '.floatval($AWIS_KEY1);
			}
		}
		if(($Recht4501&16)==0)		// Nur den eigenen Bereich anzeigen
		{
			$Bedingung .= ' AND EXISTS(SELECT * FROM PersEinsBereicheMitglieder WHERE PBM_PEB_KEY = PEB_KEY';
			$Bedingung .= ' AND PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
		}

		if($Bedingung!='')
		{
			$SQL .= ' WHERE ' . substr($Bedingung,4);
		}
		$SQL .= $ORDERBY;

		if($AWIS_KEY1<=0)
		{
			// Zum Bl�ttern in den Daten
			$Block = 1;
			if(isset($_REQUEST['Block']))
			{
				$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			}
			$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

			$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
			$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
		//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
		}
		else
		{
			$MaxDS = 1;
			$ZeilenProSeite=1;
			$Block = 1;
		}
		$rsPPR = $DB->RecordSetOeffnen($SQL);


$Form->DebugAusgabe(1, $SQL);

		$Form->Formular_Start();
		$Form->SchreibeHTMLCode('<form name=frmPersEinsPruefungen action=./personaleinsaetze_Main.php?cmdAktion=Probleme method=POST  enctype="multipart/form-data">');

		$AWIS_KEY1 = $rsPPR->FeldInhalt('PPR_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_PPR',serialize($Param));

		$Form->Erstelle_HiddenFeld('PPR_KEY',$AWIS_KEY1);

		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./personaleinsaetze_Main.php?cmdAktion=Probleme&PPRListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPPR->FeldInhalt('PPR_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsPPR->FeldInhalt('PPR_USERDAT')));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht4501&2)!=0);
		$EditRecht = ($RechtPEI&1024);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PPR']['PPR_TAG'].':',150);
		$Form->Erstelle_TextFeld('PPR_TAG',$rsPPR->FeldInhalt('PPR_TAG'),10,350,$EditRecht,'','','','D');
		$AWISCursorPosition='txtPPR_TAG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PPR']['PPR_BEMERKUNG'].':',150);
		$Form->Erstelle_TextFeld('PPR_BEMERKUNG',$rsPPR->FeldInhalt('PPR_BEMERKUNG'),100,350,$EditRecht);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PPR']['PPR_STATUS'].':',150);
		$Form->Erstelle_SelectFeld('PPR_STATUS',$rsPPR->FeldInhalt('PPR_STATUS'),200,$EditRecht,'','','','','',explode(';',$AWISSprachKonserven['Liste']['PPR_STATUS']));
		$Form->ZeileEnde();

		//***********************************************************
		// Daten LIVE testen
		//***********************************************************

		$SQL = 'SELECT XTZ_ID, XTZ_TAGESZEIT FROM TagesZeiten ORDER BY XTZ_ID';
		$rsXTZ = $DB->RecordSetOeffnen($SQL);
		$TagesZeiten=array();
		while(!$rsXTZ->EOF())
		{
			$TagesZeiten[$rsXTZ->FeldInhalt('XTZ_ID')]=$rsXTZ->FeldInhalt('XTZ_TAGESZEIT');
			$rsXTZ->DSWeiter();
		}

		$Form->Trennzeile();

		$AnzProbleme=0;
		$DS=0;
		$SQL = 'SELECT PBM_XBN_KEY, XBN_NAME, XBN_VORNAME, XBN_KON_KEY';
		$SQL .= ' FROM PerseinsbereicheMitglieder';
		$SQL .= ' LEFT OUTER JOIN PersEinsBereicheRechteVergabe ON PBM_KEY = PBR_PBM_KEY AND PBR_PBZ_KEY = 8';
		$SQL .= ' INNER JOIN Benutzer ON PBM_XBN_KEY = XBN_KEY';
		$SQL .= ' WHERE PBM_PEB_KEY = '.$DB->FeldInhaltFormat('N0',$_POST['sucPEB_KEY'],false);
		$SQL .= ' AND PBR_KEY IS NULL';
		//Perseinsaetze Rechte pr�fen
		$SQL .= ' AND PBM_GUELTIGAB <= '.$DB->FeldInhaltFormat('D',$rsPPR->FeldInhalt('PPR_TAG'));
		$SQL .= ' AND PBM_GUELTIGBIS >= '.$DB->FeldInhaltFormat('D',$rsPPR->FeldInhalt('PPR_TAG'));
		$SQL .= ' ORDER BY XBN_NAME, XBN_VORNAME';
		$rsXBN = $DB->RecordSetOeffnen($SQL);
$Form->DebugAusgabe(1, $SQL);

		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBN']['XBN_NAME'],200);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['AnzahlEintraege'],150);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['AnzahlFehler'],150);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['FehlerMeldung'],500);
		$Form->ZeileEnde();


		// Alle Mitglieder pr�fen
		$Kontakt = new awisKontakt($rsXBN->FeldInhalt('XBN_KON_KEY'),false);
		$AnzProblemeGesamt=0;

		while(!$rsXBN->EOF())
		{
			$Form->ZeileStart();
			$Kontakt->KontaktKEY($rsXBN->FeldInhalt('XBN_KON_KEY'));
			$EMail = $Kontakt->KontaktInfo(7);
			$Link=($EMail==''?'':'mailto:'.$EMail);
			$Infos = $Kontakt->KontaktInfo(6);
			$Form->Erstelle_TextLabel($rsXBN->FeldInhalt('XBN_NAME'),200,'',$Link,$Infos);


			$SQL = 'SELECT *';
			$SQL .= ' FROM Personaleinsaetze';
			$SQL .= ' WHERE PEI_XBN_KEY = '.$rsXBN->FeldInhalt('PBM_XBN_KEY');
			$SQL .= ' AND PEI_DATUM = '.$DB->FeldInhaltFormat('D',$rsPPR->FeldInhalt('PPR_TAG'));
			$SQL .= ' AND (PEI_PLANUNGSTYP is null or PEI_PLANUNGSTYP = 2)';			
			$SQL .= ' ORDER BY PEI_VONXTZ_ID';

			$rsDaten = $DB->RecordSetOeffnen($SQL);
			$Link = './personaleinsaetze_Main.php?cmdAktion=Details&PEI_XBN_KEY='.$rsXBN->FeldInhalt('PBM_XBN_KEY').'&PEI_DATUM='.$rsPPR->FeldInhalt('PPR_TAG');
			$Form->Erstelle_TextLabel($rsDaten->AnzahlDatensaetze(),150,'',$Link);

			$StartZeit = 0;
			$EndeZeit = 0;
			$LetztesEnde = 0;
			$ProblemMeldung = '';
			$AnzProblemeGesamt+=$AnzProbleme;
			$AnzProbleme=0;

			while(!$rsDaten->EOF())
			{
				if($StartZeit == 0)
				{
					$StartZeit = $rsDaten->FeldInhalt('PEI_VONXTZ_ID');
				}
				if($EndeZeit<$rsDaten->FeldInhalt('PEI_BISXTZ_ID'))			// Gr��te Zeit
				{
					$EndeZeit = $rsDaten->FeldInhalt('PEI_BISXTZ_ID');
				}


					//*************************
					// �berschneidungen testen
					//*************************
				if($LetztesEnde > $rsDaten->FeldInhalt('PEI_VONXTZ_ID'))
				{
					$ProblemMeldung .= '<br>�berschneidung. Ende: '	. $TagesZeiten[$LetztesEnde] . '. n�chster Beginn: ' . $TagesZeiten[$rsDaten->FeldInhalt('PEI_VONXTZ_ID')] . '.';
					$AnzProbleme++;
				}

					//*************************
					// L�cken testen
					//*************************
				if($LetztesEnde!=0)
				{
					if($rsDaten->FeldInhalt('PEI_VONXTZ_ID') != $LetztesEnde)
					{
						$ProblemMeldung .= '<br>Keine Angabe zwischen '	. $TagesZeiten[$LetztesEnde] . ' und ' . $TagesZeiten[$rsDaten->FeldInhalt('PEI_VONXTZ_ID')] . '.';
						$AnzProbleme++;
					}
				}
				$LetztesEnde = $rsDaten->FeldInhalt('PEI_BISXTZ_ID');

				$rsDaten->DSWeiter();
			}

			// Arbeitszeit pr�fen
			$Zeit = (($EndeZeit - $StartZeit)/5) * 30;
			if(($Zeit/60)<8)
			{
				$ProblemMeldung .= '<br>Arbeitszeit betr�gt nur ' . ($Zeit/60) .' Stunden';
				$AnzProbleme++;
			}

			$Form->Erstelle_TextLabel($AnzProbleme,150,($AnzProbleme==0?'':awisFormular::FORMAT_HINWEIS));
			$Form->Erstelle_TextLabel(substr($ProblemMeldung,4),500);

			$Form->ZeileEnde();

			$rsXBN->DSWeiter();

/*
		$SQL = "SELECT * FROM MitarbeiterEinsaetze ";
		$SQL .= " WHERE MEI_MIT_KEY=" . $rsMIT['MIT_KEY'][$MITZeile];
		$SQL .= " AND MEI_TAG='" . awis_format($_POST['txtMEP_TAG'], 'OracleDatum') . "'";

		$SQL .= " ORDER BY MEI_VONXTZ_ID";

		$rsMEI = awisOpenRecordset($con, $SQL);
		$rsMEIZeilen = $awisRSZeilen;
		$ProblemMeldung='';

		if($rsMEIZeilen>0)		// Es sind Daten da
		{
			$StartID = 0;	// Kleinste Zeit (gesamt)
			$EndeID = 0;	// Gr��te Zeit (gesamt)
			$LetztesEnde = 0;
			$ProblemMeldung = '';		// Beschreibung f�r die L�cke
			for($MEIZeile=0;$MEIZeile<$rsMEIZeilen;$MEIZeile++)
			{
				if($StartID==0)			// Kleinste Zeit
				{
					$StartID = $rsDaten->FeldInhalt('PEI_VONXTZ_ID');
				}

				if($EndeID<$rsDaten->FeldInhalt('PEI_BISXTZ_ID'))			// Gr��te Zeit
				{
					$EndeID = $rsDaten->FeldInhalt('PEI_BISXTZ_ID');
				}

					//*************************
					// �berschneidungen testen
					//*************************
				if($LetztesEnde > $rsDaten->FeldInhalt('PEI_VONXTZ_ID'))
				{
					$ProblemMeldung .= '<br>�berschneidung. Ende: '	. $TagesZeiten[$LetztesEnde] . '. n�chster Beginn: ' . $TagesZeiten[$rsDaten->FeldInhalt('PEI_VONXTZ_ID')] . '.';
					$AnzProbleme++;
				}

					//*************************
					// L�cken testen
					//*************************
				if($LetztesEnde!=0)
				{
					if($rsDaten->FeldInhalt('PEI_VONXTZ_ID') != $LetztesEnde)
					{
						$ProblemMeldung .= '<br>Keine Angabe zwischen '	. $TagesZeiten[$LetztesEnde] . ' und ' . $TagesZeiten[$rsDaten->FeldInhalt('PEI_VONXTZ_ID')] . '.';
						$AnzProbleme++;
					}
				}
				$LetztesEnde = $rsDaten->FeldInhalt('PEI_BISXTZ_ID');

						// Filiale ausf�llen
				if($rsMEI['MEI_MEA_KEY'][$MEIZeile]==8 AND $rsMEI['MEI_FIL_ID'][$MEIZeile]=='')		// Filiale
				{
					$ProblemMeldung .= '<br>Keine Filialnummer eingetragen (ab ' . $TagesZeiten[$rsDaten->FeldInhalt('PEI_VONXTZ_ID')] . ')';
					$AnzProbleme++;
				}
			}

			$Zeit = (($EndeID - $StartID)/5) * 30;
			if(($Zeit/60)<8)
			{
				$ProblemMeldung .= '<br>Arbeitszeit betr�gt nur ' . ($Zeit/60) . ' Stunden';
				$AnzProbleme++;
			}

		}

		if($ProblemMeldung!='')
		{
			echo '<tr><td class=FeldBez colspan=2>';
			if(($RechteStufe1300&4)==4)
			{
				echo '<a href=./perseins_Main.php?cmdAktion=Einsaetze&Tag=' . awis_format($_POST['txtMEP_TAG'], 'OracleDatum') . '&MIT_KEY=' . $rsMIT['MIT_KEY'][$MITZeile] . '>';
				echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile] . ' (' . $rsMIT['REZ_BEZEICHNUNG'][$MITZeile] . ')';
				echo '</a>';
				echo '</td></tr>';
			}
			else
			{
				echo $rsMIT['MIT_BEZEICHNUNG'][$MITZeile] . ' (' . $rsMIT['REZ_BEZEICHNUNG'][$MITZeile] . ')</td></tr>';
			}

			echo '<tr><td>';
			echo substr($ProblemMeldung,4);
			echo '</td></tr>';
			$AnzProbleme++;
		}
*/
			$DS++;
		}	// Ende alle Mitarbeiter

		if($AnzProblemeGesamt==0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('Es wurden '.$DS.' Eintr�ge gepr�ft.',500,'Hinweis');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PPR']['VollstaendigErfasst'],500,'Hinweis');
			$Form->ZeileEnde();
		}
		$Form->Formular_Ende();

		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();

		$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

		if($RechtPEI&1024)		//
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		}

		$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=bereiche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_Hilfe'], 'H');

		$Form->SchaltflaechenEnde();


	}
	//$Form->DebugAusgabe(1, $Param, $Bedingung, $rsPPR, $_POST, $rsPPR, $SQL, $AWISSprache);

	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Erzeugt alle notwendigen Eintragungen f�r die Personaleinsatzpr�fungen
 *
 * @param string $Datum (mit TO_DATE()!!)
 * @return boolean
 */
function _ErzeugePruefungen($Datum)
{
	global $DB;

	$SQL = "insert into
			Perseinspruefungen
			(ppr_tag, ppr_peb_key, ppr_status)
			select xka_tag, peb_key, status from
			(select xka_tag, peb_key, 'O' as status
			from Kalender
			cross join perseinsbereiche
			WHERE xka_tag = ".$Datum.") Daten
			 left outer join perseinspruefungen on ppr_tag = ".$Datum." and peb_key = ppr_peb_key and ppr_tag = xka_tag
			 where ppr_key is null";

	return $DB->Ausfuehren($SQL);
}
?>