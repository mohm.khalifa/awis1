<?php
require_once 'awisBerichte.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

global $AWISBenutzer;

class ber_Qualitaetsmaengel
	extends awisBerichte
{
	private $pdf_vorlage = '';
	private $Zeilenmerker = '';
	private $Spaltenmerker = '';
	private $letzte_positon = '';
	private $Summe = 0.00;
	
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
		$TextKonserven = array();
		$TextKonserven[]=array('QMM','%');
		$TextKonserven[]=array('Wort','lbl_zurueck');
		$TextKonserven[]=array('Wort','lbl_speichern');
		$TextKonserven[]=array('Wort','lbl_hinzufuegen');
		$TextKonserven[]=array('Wort','txt_BitteWaehlen');
		$TextKonserven[]=array('Wort','lbl_trefferliste');
		$TextKonserven[]=array('Fehler','err_keineDaten');
		$TextKonserven[]=array('Liste','lst_OffenMass');
		$TextKonserven[] = array('Ausdruck', 'txt_HinweisFormularIntern');
		$TextKonserven[] = array('Wort', 'Seite');
		
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		
		$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_QMM'));
		$Recht34000 = 0;
		//$Recht34000 = $this->_AWISBenutzer->HatDasRecht(34000);
		
		
		//Selektiert die Kriterien
		/*
		$SQL = 'SELECT *';
		$SQL .= 'FROM QMKRITERIEN';
		*/
		
		//Selektiert die Pl�ne
		$SQL = 'SELECT *';
		$SQL .= 'FROM QMPLAENE';
		$SQL .= ' INNER JOIN QMSTATUS ON QMP_STATUS = QMS_KEY';
		$SQL .= ' WHERE QMP_KEY='.$this->_DB->FeldInhaltFormat('T',$Parameter['QMP_KEY']);
		
		$this->_rsDetails = $this->_DB->RecordSetOeffnen($SQL);
		
		$SQL = 'SELECT distinct qmk_lang,qmk_key ';
		$SQL .= 'FROM QMKRITERIEN';
		$SQL .= ' INNER JOIN QMKRITERIENGUELTIG ON QMK_KEY = QMG_QMK_KEY';
		$SQL .= ' WHERE QMK_AKTIV = 1 ';
		$SQL .= ' AND QMG_GUELTIGAB <=\''.$this->_Form->Format('D',$this->_rsDetails->FeldInhalt('QMP_ERHEBDAT')).'\'';
		$SQL .= ' AND QMG_GUELTIGBIS >=\''.$this->_Form->Format('D',$this->_rsDetails->FeldInhalt('QMP_ERHEBDAT')).'\'';
		$SQL .= ' order by qmk_lang ';
		
		$this->_rsKrit = $this->_DB->RecordSetOeffnen($SQL);
		
		//Filialinformationen
		$SQL = 'Select * from( ';
		$SQL .='(SELECT distinct FIF_FIL_ID,FIL_BEZ,PER_NACHNAME || \', \' || PER_VORNAME as Name,FIF_FIT_ID';
		$SQL .='       FROM PERSONAL';
		$SQL .='       INNER JOIN FILIALINFOS ON FIF_WERT = PER_NR ';
		$SQL .='       INNER JOIN filialinfostypen2 ON FIT_ID = 74';
		$SQL .='       INNER JOIN FILIALEN ON FIL_ID = FIF_FIL_ID ';
		$SQL .=' WHERE FIF_FIT_ID = 74 AND FIF_IMQ_ID = 8 AND FIL_LAN_WWSKENN = \'BRD\'';
		$SQL .=' )';
		$SQL .=' UNION';
		$SQL .=' (';
		$SQL .=' SELECT distinct FIF_FIL_ID,FIL_BEZ,PER_NACHNAME || \', \' || PER_VORNAME as Name,FIF_FIT_ID';
		$SQL .='       FROM PERSONAL';
		$SQL .='       INNER JOIN FILIALINFOS ON FIF_WERT = PER_NR ';
		$SQL .='       INNER JOIN filialinfostypen2 ON FIT_ID = 76';
		$SQL .='       INNER JOIN FILIALEN ON FIL_ID = FIF_FIL_ID ';
		$SQL .=' WHERE FIF_FIT_ID = 76 AND FIF_IMQ_ID = 8 AND FIL_LAN_WWSKENN = \'BRD\'';
		$SQL .=' ))';
		$SQL .=' WHERE FIF_FIL_ID='.$this->_rsDetails->FeldInhalt('QMP_FIL_ID');
			
		$this->_rsFil = $this->_DB->RecordSetOeffnen($SQL);
			
		$this->_FIL_NAME = '';
		$this->_GL = '';
		$this->_WL = '';
		while(! $this->_rsFil->EOF())
		{
			if($this->_rsFil->FeldInhalt('FIF_FIT_ID') == 74)
			{
				$this->_GL = $this->_rsFil->FeldInhalt('NAME');
			}
			elseif($this->_rsFil->FeldInhalt('FIF_FIT_ID') == 76)
			{
				$this->_WL = $this->_rsFil->FeldInhalt('NAME');
			}
			$this->_rsFil->DSWeiter();
		}
			
		$SQL2 = 'Select * from Filialen where FIL_ID='.$this->_rsDetails->FeldInhalt('QMP_FIL_ID');
		$this->_rsFil2 = $this->_DB->RecordSetOeffnen($SQL2);
		$this->_FIL_NAME = $this->_rsFil2->FeldInhalt('FIL_BEZ');
		
		$SQL = 'SELECT case when qmz_key is not null then 1 else 0 end as checked,qmz_frist,qmm_bezeichnung,qmz_bemerkung,qms_bezeichnung,qmz_key,qmz_erfuellt,qms_folgestatus';
		$SQL .= ' FROM QMMASSNAHMENZUORD ';
		$SQL .= ' INNER JOIN QMMASSNAHMEN on qmz_qmm_key = qmm_key ';
		$SQL .= ' INNER JOIN QMSTATUS on qmz_status = qms_key ';
		$SQL .= '  where qmz_qmp_key='.$this->_DB->FeldInhaltFormat('T',$Parameter['QMP_KEY']);
		$SQL .= ' ORDER BY QMZ_KEY';
			
		$this->_rsMass = $this->_DB->RecordSetOeffnen($SQL);
		
		//personalnummer
		$SQL = 'SELECT Name || \',\' || Vorname as Name ';
		$SQL .= ' from personal_komplett where persnr =\''.$this->_rsDetails->FeldInhalt('QMP_PERSNR').'\'';
		$SQL .= ' and (datum_eintritt <= sysdate) and (datum_austritt is null or datum_austritt >= sysdate)';
		
		$this->_rsPers = $this->_DB->RecordSetOeffnen($SQL);
		
		// Daten f�r den Bericht laden
       	$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eigentliches PDF erzeugen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		//$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		
		$this->AliasNbPages();
		$this->SetAutoPageBreak(true,50);
		
		// Startpositionen setzen
		
		//$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->NeueSeite();
		

		//$this->SetXY($this->_Spalte, $this->_Zeile);
		//Zeichne Kriterien
		$Zeile = 0;
		while(!$this->_rsKrit->EOF()) //Werte in PDF Drucken
		{
			if($this->_rsDetails->FeldInhalt('QMP_QMK_KEY') == $this->_rsKrit->FeldInhalt('QMK_KEY'))
			{
				$Check = 1;
			}
			else
			{
				$Check = 0;
			}
			
			if($Check == 1)
			{
				$this->Cell(5, $this->_Parameter['Zeilenhoehe'], 'X', 0, 0, 'L', 0);
			}
			else 
			{
				$this->Cell(5, $this->_Parameter['Zeilenhoehe'], '_', 0, 0, 'L', 0);
			}
			$this->Cell(75, $this->_Parameter['Zeilenhoehe'], $this->_rsKrit->FeldInhalt('QMK_LANG'), 0, 0, 'L', 0);
			
			if($Zeile == 1)
			{
				$this->_Spalte = $this->_Parameter['StartPosWerte'];
				$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
				$this->SetXY($this->_Spalte, $this->_Zeile);
				$Zeile = 0;
			}
			else 
			{
				$Zeile++;
				
			}
			$this->_rsKrit->DSWeiter();
		}
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		//Erhebdat + Ergebnis
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['ERHEBDAT'] . ':'));
		
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D',$this->_rsDetails->FeldInhalt('QMP_ERHEBDAT')), 0, 0, 'L', 0);
		
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_ERGEBNIS'] . ':'), 0, 0, 'L', 0);
		
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$QMM_ERGEBNIS = $this->_rsDetails->FeldInhalt('QMP_QMK_KEY') != 'BeschwerdeQ'?$this->_Form->Format('N2',$this->_rsDetails->FeldInhalt('QMP_ERGEBNIS')).' %':$this->_Form->Format('N8',$this->_rsDetails->FeldInhalt('QMP_ERGEBNIS'));
		$this->Cell(10, $this->_Parameter['Zeilenhoehe'], $QMM_ERGEBNIS, 0, 0, 'L', 0);
		
		//Zwischenlinie
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(self::Seitenbreite()-($this->_Parameter['LinkerRand']*2), $this->_Parameter['Zeilenhoehe'], '' , 'B', 0, 'L', 0);
		
		//FILNR + FIL-Bez 
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_FIL_NR'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsDetails->FeldInhalt('QMP_FIL_ID'), 0, 0, 'L', 0);
		
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_FIL_ID'] . ':'), 0, 0, 'L', 0);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(10, $this->_Parameter['Zeilenhoehe'], $this->_FIL_NAME, 0, 0, 'L', 0);
		
		//GL, WL
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_GL'] . ':'));
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_GL, 0, 0, 'L', 0);
		
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_WL'] . ':'), 0, 0, 'L', 0);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(10, $this->_Parameter['Zeilenhoehe'], $this->_WL, 0, 0, 'L', 0);
		
		//Zwischenlinie
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(self::Seitenbreite()-($this->_Parameter['LinkerRand']*2), $this->_Parameter['Zeilenhoehe'], '' , 'B', 0, 'L', 0);
		
		//Feststellung
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		//$this->_Spalte = $this->GetY();
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_FESTSTELLUNG'] . ':'));
				
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell(150, $this->_Parameter['Zeilenhoehe'], $this->_rsDetails->FeldInhalt('QMP_FESTSTELLUNG'));
		
		//Ma�nahmenueberschriften
		$this->_Zeile = $this->GetY()+ $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		
		
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(30, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_MASSNAHMEN'] . ':'));

		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$this->Cell($this->_Parameter['BreiteMassnahme'], $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L', 0);
		$this->Cell(5, $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L', 0);
		$this->Cell($this->_Parameter['BreiteBeschreibung'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_BESCHREIBUNG']), 0, 0, 'L', 0);
		$this->Cell($this->_Parameter['BreiteFrist'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_FRIST']), 0, 0, 'L', 0);
		$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_Status']), 0, 0, 'L', 0);
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$EndeX = $this->_Parameter['LinkerRand'];
		//Ma�nahmenpositionen
		while(! $this->_rsMass->EOF())
		{
			$this->_Zeile = $this->GetY();
			$EndeX = self::Seitenbreite() - $this->_Parameter['LinkerRand'];
			$this->Line($this->_Parameter['LinkerRand'],$this->GetY(),$EndeX,$this->GetY());
			$Oben = $this->GetY();
			
			$Bezeichnung = strlen($this->_rsMass->FeldInhalt('QMM_BEZEICHNUNG')) >= 25? substr($this->_rsMass->FeldInhalt('QMM_BEZEICHNUNG'),0,25).'...':$this->_rsMass->FeldInhalt('QMM_BEZEICHNUNG');
			//$Beschreibung = strlen($this->_rsMass->FeldInhalt('QMZ_BEMERKUNG')) >= 40? substr($this->_rsMass->FeldInhalt('QMZ_BEMERKUNG'),0,40).'...':$this->_rsMass->FeldInhalt('QMZ_BEMERKUNG');
			
			$this->SetFont($this->_Parameter['Schriftart'], '', 7);
			
			$this->Cell($this->_Parameter['BreiteMassnahme'], $this->_Parameter['Zeilenhoehe'], 'X    ' . $Bezeichnung, 0, 0, 'L', 0);
			$this->Cell(5, $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L', 0);
			
			$this->MultiCell($this->_Parameter['BreiteBeschreibung'], $this->_Parameter['Zeilenhoehe'], $this->_rsMass->FeldInhalt('QMZ_BEMERKUNG'));

			$EndeMulitCell = $this->GetY();
	
			$this->_Spalte = 125;
			$this->SetXY($this->_Spalte, $this->_Zeile);
			
			$this->SetFont($this->_Parameter['Schriftart'], 'B', 7);
			$this->Cell($this->_Parameter['BreiteFrist'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D', $this->_rsMass->FeldInhalt('QMZ_FRIST')),0, 0, 'L', 0);
			$this->SetFont($this->_Parameter['Schriftart'], '', 7);
			$this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsMass->FeldInhalt('QMS_BEZEICHNUNG'), 0, 0, 'L', 0);
			
			
			$this->_Zeile = $EndeMulitCell;
			$this->_Spalte = $this->_Parameter['StartPosWerte'];
			
			$this->SetXY($this->_Spalte, $this->_Zeile);
			
			$this->_rsMass->DSWeiter();
		}
		$this->Line($this->_Parameter['LinkerRand'],$this->GetY(),$EndeX,$this->GetY());
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$this->Cell(25, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['QMM_PERSNR'] . ':'), 0, 0, 'L', 0);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->Cell(25, $this->_Parameter['Zeilenhoehe'], $this->_rsDetails->FeldInhalt('QMP_PERSNR'), 0, 0, 'L', 0);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->Cell(40, $this->_Parameter['Zeilenhoehe'], $this->_rsPers->FeldInhalt('NAME'), 0, 0, 'L', 0);
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$EndeX = self::Seitenbreite() - $this->_Parameter['LinkerRand'];

		
		//Datum und Unterschrift
		/**
		 * Textkonserven f�r Datum und Unterschrift f�r Produktiv erweitern.
		 */
		
		$Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_QMM'));
		$Recht34000 = $this->_AWISBenutzer->HatDasRecht(34000);
		
		
		if (($this->_AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='' or ($Recht34000&2)==2)
		{
			$this->Line($this->_Parameter['LinkerRand'],$this->GetY(),$EndeX,$this->GetY());
			
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			$this->_Spalte = $this->_Parameter['StartPosWerte'];
			$this->SetXY($this->_Spalte, $this->_Zeile);
			
			$this->Cell(15, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T','Datum'. ':'), 0, 0, 'L', 0);
			$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
			$this->Cell(25, $this->_Parameter['Zeilenhoehe'],'_______________', 0, 0, 'L', 0);
			$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
			$this->Cell(40, $this->_Parameter['Zeilenhoehe'], $this->_rsPers->FeldInhalt('NAME'), 0, 0, 'L', 0);
			
			$this->Cell(22, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('T','Unterschrift'. ':'), 0, 0, 'L', 0);
			$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
			$this->Cell(25, $this->_Parameter['Zeilenhoehe'],'_____________________', 0, 0, 'L', 0);
			$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
			$this->Cell(40, $this->_Parameter['Zeilenhoehe'], $this->_rsPers->FeldInhalt('NAME'), 0, 0, 'L', 0);
			
			$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
			$this->_Spalte = $this->_Parameter['StartPosWerte'];
			$this->SetXY($this->_Spalte, $this->_Zeile);
		}

		
		$this->FertigStellen($Ausgabeart);        
	}
	
	
	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
	    $this->AddPage();
	}
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		//$this->HauptrahmenAnfangY = $this->_Parameter['StartUeberschrift']+$this->_Parameter['Zeilenhoehe'];
		$this->HauptrahmenAnfangY = $this->_Parameter['Seite1-Oben'];
		$this->HauptrahmenEndeY = $this->GetY();
		$this->HauptrahmenEndeX = self::Seitenbreite() - $this->_Parameter['LinkerRand'];
		
		//Linker Rand
		$this->Line($this->_Parameter['LinkerRand'],$this->HauptrahmenEndeY,$this->_Parameter['LinkerRand'],$this->HauptrahmenAnfangY);
				
		//Rechter Rand
		$this->Line($this->HauptrahmenEndeX,$this->HauptrahmenEndeY,$this->HauptrahmenEndeX,$this->HauptrahmenAnfangY);
		
		//Unterer Rand
		$this->Line($this->_Parameter['LinkerRand'],$this->HauptrahmenEndeY,$this->HauptrahmenEndeX,$this->HauptrahmenEndeY);
	
		$this->SetFont('Arial','',6);
		
		$this->SetXY(0,self::SeitenHoehe()-5);
		$this->Cell(self::Seitenbreite(),$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Ausdruck']['txt_HinweisFormularIntern']),0,0,'C','');
		
		$this->SetXY(self::SeitenBreite()-50,self::SeitenHoehe()-5);
		$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
		 
	
	
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		if($this->_BerichtsVorlage!='')
		{
			$pdf_vorlage = $this->ImportPage(1);		// Erste Seite aus der Vorlage importieren
			$this->useTemplate($pdf_vorlage);
		}
		$this->_Zeile = $this->_Parameter['SeitenzaehlerOben'];
		$this->_Spalte = $this->_Parameter['SeitenzaehlerRechts'];
		$this->SetFont($this->_Parameter['Schriftart'], '', 8);
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$this->Cell(10, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo().' von {nb}',0 ,0 ,'L');
		
		
		$this->_Zeile = $this->_Parameter['Seite1-Oben']+ $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['StartPosWerte'];
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		//$this->Cell(10, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo().' von {nb}',0 ,0 ,'L');
		//$this->Cell(50, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['QMM']['UEBERSCHRIFT_QMM']));
	
		/*
		if($this->PageNo()!=1)
		{
			//$this->_Zeile = $this->_Zeile + $this->_Parameter['Zeilenhoehe'];
			$this->_Zeile = $this->GetY();
			$this->SetXY($this->_Spalte, $this->_Zeile);
		}
		*/
		
	}
	
	public function FertigStellen($Ausgabeart)
	{
	    //*********************************************************************
	    // Berichtsende -> Zur�ckliefern
	    //*********************************************************************
	    switch($Ausgabeart)
	    {
	        case 1:			// PDF im extra Fenster -> Download
	            $this->Output($this->_BerichtsName.'.pdf','D');
	            break;
	        case 2:			// Standard-Stream
	            $this->Output($this->_BerichtsName.'.pdf','I');
	            break;
	        case 3:			// Als String zur�ckliefern
	            return($this->Output($this->_BerichtsName.'.pdf','S'));
	            break;
	        case 4:         // Als Datei speichern
	            $this->Output($this->_Dateiname,'F');
	            break;
	    }
	}
	
}

?>