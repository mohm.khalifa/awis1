<?php
/**
 * awis_seminarplan.inc
 * 
 * Diese Klasse ist f�r den Import der Seminare bzw. Seminarteilnehmer zust�ndig
 * 
 * @author Thomas Riedl
 * @copyright ATU - Auto-Teile-Unger
 * @version 20100419
 * 
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awis_einkaufsgesellschaften
{
	/**
	 * Datenbankverbindung
	 * 
	 * @var awisDatenbank
	 */
	private $_DB = null;
	
	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	
	/**
	 * Debuglevel f�r Ausgaben an die Konsole
	 * @var int (0=Keine, 9=Alles)
	 */
	private $_DebugLevel = 0;
		
	/**
	 * Importdateiname der Termine
	 * @var string
	 */	
	private $_ExportDateiPfad = '/daten/daten/pcedv11/Awis/EKG/';
		
	/**
	 * Emailbenachrichtigung
	 * @var string
	 */
	private $_ExportDateiName = 'wws_export_ekg_beweg';
	
	/**
	 * Emailbenachrichtigung
	 * @var string
	 */
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');
	
	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);		
	}
	
	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);	
		}
		
		return $this->_DebugLevel;
	}
	
	
    public function ExportBewegDat()
    {
        try 
        {
            $Werkzeug = new awisWerkzeuge();
            
            $Werkzeug->EMail($this->_EMAIL_Infos,'OK EKG_BEWEGDAT_EXPORT BEGINNT','',1,'','awis@de.atu.eu');
            
            if($this->_DebugLevel>5)
            {
                echo date('d.m.Y') . 'EKG_BEWEGDAT_EXPORT BEGINNT'.PHP_EOL;
            }
          
            $Protokoll['ExportierteDS']=0;
            $Protokoll['KeineZamiNR']=0; //Anzahl DS, wo AWIS keine Zaminr kennt. 
            $UnbekannteZamiNr[0]=''; //Array, Werte in Klartext wo AWIS die Zaminr nicht kennt. 
            $Protokoll['ZamiAufUnbekannt']=0;  //Anzahl DS, wo der Comserver keine Zahlungsart geliefert hat.           
            $AnzahlOffeneTransaktionsID = 0; 
           
            //Merken wieviel offene Transaktionen vorhanden sind f�r den Fall, 
            //dass es eine Nachladung am Comserver gibt und ich am Ende zuviele Update.
            $SQLOffeneTransaktionen = 'select * from expgw_export_awis@com_test_de.atu.de';
            $SQLOffeneTransaktionen .=' WHERE';
            $SQLOffeneTransaktionen .=' arbeitsstatus = \'M\'';            
            $AnzahlOffeneTransaktionsID = $this->_DB->ErmittleZeilenAnzahl($SQLOffeneTransaktionen);
            
            if ($AnzahlOffeneTransaktionsID == 0)
            {
            	if($this->_DebugLevel>50)
            	{
            		$Werkzeug->EMail($this->_EMAIL_Infos,'WARNING EKG_BEWEGDAT_EXPORT','Verschiebe n�chsten Start um eine Minute' . (date("d.m.Y H:i")+ 2 ),1,'','awis@de.atu.eu');
            	}
            	

            	if($this->_DebugLevel>5)
            	{
            		echo date('d.m.Y') . 'Keine Transaktionen offen. Verschiebe Start um eine Minute.' . (date("d.m.Y H:i") + 2 ) .PHP_EOL;
            	}
            	
            	
            	$date = new DateTime(time());
            	$date->add(new DateInterval('2M'));
            	echo $date->format('d.m.Y H:i');
            	
            
            	$Update = 'Update Jobliste set XJO_NAECHSTERSTART = ' .$this->_DB->WertSetzen('JOB', 'T', ($date->format('d.m.Y H:i'))). ' where XJO_KEY='. $this->_DB->WertSetzen('JOB', 'T', 58);
            	
            	$this->_DB->Ausfuehren($Update,'',true,$this->_DB->Bindevariablen('JOB',true));
            	
            	exit;
            	
            	
            }
          
            
            $Form = new awisFormular();
          
            $SQL  ='SELECT';
            $SQL .='   Artikelnr';
            $SQL .='   ||\';\'';
            $SQL .='   || fil_id';
            $SQL .='   ||\';\'';
            $SQL .='   || SUM(menge)';
            $SQL .='   ||\';\'';
            $SQL .='   || bontyp';
            $SQL .='   ||\';\'';
            $SQL .='   || kassentag';
            $SQL .='   ||\';\'';
            $SQL .='   || transaktionsid';
            $SQL .='   ||\';\'';
            $SQL .='   || zmm_zaminr';
            $SQL .='   ||\';\'';
            $SQL .='   || zmm_rechnungstext ';
            $SQL .='   ||\';\' ';
            $SQL .='   || artikelbez';
            $SQL .='   || \';\'';
            $SQL .='   || bonzeit ';
            $SQL .='   || \';\'';           
            $SQL .='   || ZMM_SKONTO ';
            $SQL .='   || \';\'';           
            $SQL .='   || ZMM_ZAHLUNGSZIEL ';
            $SQL .='   || \';\'';          
            $SQL .='   || bsa AS Daten,';
            $SQL .='   Artikelnr,fil_id, bsa, kassentag, bonzeit, bondatum, transaktionsid,  zmm_zaminr,';
            $SQL .='   nvl(PRUEF_ZAMINR,0) as PRUEF_ZAMINR';
            $SQL .=' FROM';
            $SQL .='   (';
            $SQL .='     SELECT';
            $SQL .='       A.artikelnr,';
            $SQL .='       A.artikelbez,';
            $SQL .='       A.bsa,';
            $SQL .='       A.fil_id,';
            $SQL .='       menge,';
            $SQL .='       A.bontyp,';
            $SQL .='       A.kassentag,';
            $SQL .='       A.transaktionsid,';
            $SQL .='       A.bonzeit,';
            $SQL .='       a.bondatum,';
            $SQL .='       b.zmm_rechnungstext,';
            $SQL .='       b.zmm_zaminr, b.zmm_zahlungsziel, b.zmm_skonto, ';
            $SQL .='       b.PRUEF_ZAMINR';
            $SQL .='     FROM';
            $SQL .='       v_awis_a5_ekg@com_de.atu.de A';
            $SQL .='     LEFT JOIN v_ekg_com_min_zami b';
            $SQL .='     ON';
            $SQL .='       A.fil_id           = b.fil_id';
            $SQL .='     AND A.bondatum       = b.bondatum';
            $SQL .='     AND A.bonzeit        = b.bonzeit';
            $SQL .='     AND A.bsa            = b.bsa';
            $SQL .='     AND A.transaktionsid = b.transaktionsid';
            $SQL .='     WHERE';
            $SQL .='       A.transaktionsid IN';
            $SQL .='       (';
            $SQL .='         SELECT';
            $SQL .='           transaktionsid';
            $SQL .='         FROM';
            $SQL .='           expgw_export_awis@com_test_de.atu.de';
            $SQL .='         WHERE';
            $SQL .='           arbeitsstatus = \'M\'';
            $SQL .='       )';
            $SQL .='   )';
            $SQL .=' GROUP BY';
            $SQL .='   artikelnr,';
            $SQL .='   Artikelnr,';
            $SQL .='   fil_id,';
            $SQL .='   bontyp,';
            $SQL .='   kassentag,';
            $SQL .='   transaktionsid,';
            $SQL .='   zmm_zaminr,';
            $SQL .='   PRUEF_ZAMINR,';
            $SQL .='   zmm_rechnungstext,';
            $SQL .='   artikelbez,';
            $SQL .='   bsa, zmm_zahlungsziel, zmm_skonto, ';
            $SQL .='   bonzeit, BONDATUM';
            
            
            if($this->_DebugLevel>50)
            {
                echo date('d.m.Y') . 'F�hre SQL aus: '. $SQL .PHP_EOL;
            }
            
            $rsBewegDat =$this->_DB->RecordSetOeffnen($SQL);
            
            $AnzahlExportDatensaetze = $rsBewegDat->AnzahlDatensaetze();
            
            if($this->_DebugLevel>50)
            {
                echo date('d.m.Y') . 'SQL Abfrage ausgef�hrt. ' .PHP_EOL;
                echo date('d.m.Y') . '�ffne Datei zum schreiben: ' .$this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp' .PHP_EOL;
            }
             
            //Temp Datei erstellen. Damit die WAWI nicht drauf zugreift...
            $Datei = fopen($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp',"a");
            
            while(!$rsBewegDat->EOF())
            {  
            	$Export = false;
            	
            	//Wenn AWIS Die Zahlungsart kennt 
                if(($rsBewegDat->FeldInhalt('ZMM_ZAMINR')!= '')) 
                {
                	if($this->_DebugLevel>90)
                	{
                		echo date('d.m.Y') . 'AWIS kennt diese Zahlungsart f�r diesen Datensatz.  ' .  $rsBewegDat->FeldInhalt('DATEN') .PHP_EOL;
                	}
                	
                	$Export = true;
                }
                elseif(($rsBewegDat->FeldInhalt('ZMM_ZAMINR')== '' AND $rsBewegDat->FeldInhalt('PRUEF_ZAMINR')== '0'))//oder vom Comserver urspr�nglich mal NULL (per NVL aber 0) kommt,
                {
                	if($this->_DebugLevel>90)
                	{
                		echo date('d.m.Y') . 'Keine Zahlungsart am Comserver hinterlegt. --> Unbekannt.  ' .  $rsBewegDat->FeldInhalt('DATEN') .PHP_EOL;
                	}	
                	++$Protokoll['ZamiAufUnbekannt'];
                	$Export = true;               	
                }
                else //AWIS kennt von diesem DS die Zahlungsart nicht, der Comserver hat aber eine Zahlungsart geliefert
                {
                    ++$Protokoll['KeineZamiNR'];
                    if($this->_DebugLevel>10)
                    {
                        echo date('d.m.Y') . 'Keine im AWIS gepflegte ZamiNR f�r Datensatz:  ' . $rsBewegDat->FeldInhalt('DATEN') .PHP_EOL;
                    }
                    $UnbekannteZamiNr[$Protokoll['KeineZamiNR']] =  $rsBewegDat->FeldInhalt('PRUEF_ZAMINR');
                    $Export = false;
                }
                
                //Nachdem alle Pr�fungen gemacht wurden, muss hier ein true ankommen. Wenn ja, schreib den DS in die Datei.
                if($Export = true)
                {
                	$Zeile = $rsBewegDat->FeldInhalt('DATEN').chr(13).chr(10);
                	if($this->_DebugLevel>90)
                	{
                		echo date('d.m.Y') . 'Exportiere Zeile:  ' . $Zeile .PHP_EOL;
                	}
                	fwrite($Datei, $Zeile);
                	++$Protokoll['ExportierteDS'];
                }
                
                $rsBewegDat->DSWeiter();
            }
            fclose($Datei);
          
            
            //Wenn es f�r einen DS keine ZamiNR gibt (Ausnahme wenn Comserver auch keine Daten hat, dann trotzdem), darf garnichts exportiert werden.
            if ($Protokoll['KeineZamiNR'] > 0)
            {
                if($this->_DebugLevel>10)
                {
                    echo date('d.m.Y') . 'Es war eine unbekannte ZamiNR dabei. Datei auf .errOK umbennenen  ' .PHP_EOL;
                }
            
                //Datei auf Error umbennen.
                rename($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp', $this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.errOK');
                 
                $TextKeineZami = 'Folgende Zahlungsmittel sind unbekannt. Bitte umgehend im AWIS anlegen. Anschlie�end muss der Job vom AWIS-Team wiederholt werden.';
                //Doppelte Werte aus dem Array schmei�en..
                $UnbekannteZamiNr = array_unique($UnbekannteZamiNr);
                foreach($UnbekannteZamiNr AS $ZamiNr)
                {
                    $TextKeineZami .= $ZamiNr . ', '.chr(10).chr(13);
                }
            
                $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR EKG_BEWEGDAT_EXPORT - UNBEKANNTE ZAMI',$TextKeineZami,1,'','awis@de.atu.eu');
            }
            else //Keine unbekannten Zamis
            {
            	//Schauen ob die Datei so viele Datens�tze hat, wie urspr�nglich ermittelt. 
                if(count(file($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp')) == $AnzahlExportDatensaetze )
                {
                	//Schauen ob immer noch genauso viele Transaktionen auf "offenen" stehen wie vor dem Export.
                	if ($AnzahlOffeneTransaktionsID == $this->_DB->ErmittleZeilenAnzahl($SQLOffeneTransaktionen))
                	{
                		if($this->_DebugLevel>10)
                		{
                			echo date('d.m.Y') . 'COM-Journal schreiben...  ' .PHP_EOL;
                		}
                	
                		//COM-Journaltabelle schreiben
                		$SQL = 'update expgw_export_awis@com_test_de.ATU.DE set ARBEITSSTATUS = \'T\', TRANSFER_DAT = sysdate where ARBEITSSTATUS = \'M\'';
                		$this->_DB->Ausfuehren($SQL);
                	
                		//Da DB->Ausfuehren() KEINEN R�ckgabewert liefert, nachschauen ob der Update wirklich funktioniert hat.
                		if ($this->_DB->ErmittleZeilenAnzahl($SQLOffeneTransaktionen) == 0)
                		{
                			//Auf .txt umbennen, damit WAWI holen kann
                			rename($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp', $this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.txt');
                			$Text= 'Exportierte Datens�tze '.$Protokoll['ExportierteDS'].chr(10);
                			$Werkzeug->EMail($this->_EMAIL_Infos,'OK EKG_BEWEGDAT_EXPORT',$Text,1,'','awis@de.atu.eu');
                	
                			if($this->_DebugLevel>10)
                			{
                				echo date('d.m.Y') . 'Job erfolgreich abgeschlossen... Datei nach .txt umbennen.   ' .PHP_EOL;
                			}
                		}
                		else{
                			$Text = 'Bei der Verarbeitung trat ein Fehler auf. Folgender Update verursachte wohl Probleme: ' . PHP_EOL ;
                			$Text .= $SQLOffeneTransaktionen. PHP_EOL ;
                			$Text .= 'Es wurde KEINE Datei an die Warenwirtschaft �bermittelt!';
                			$Werkzeug->EMail($this->_EMAIL_Infos,'Error ERROR_BEWEGDAT_EXPORT',$Text,1,'','awis@de.atu.eu');
                		}
                	}
                	else //Es stehen mehr Transaktionen auf offen wie bei Exportstart.
                	{
                		$Text = 'COM Server hat w�hrend des Exports neue Daten bereitsgestellt. Export abgebrochen. Job muss wiederholt werden. ';
                		$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR EKG_BEWEGDAT_EXPORT',$Text,1,'','awis@de.atu.eu');
                	}
                }
                else 
                {
                	if($this->_DebugLevel>10)
                	{
                		echo date('d.m.Y') . ' FEHLER! Es wurden WENIGER Datens�tze in die Textdatei geschrieben, wie urspr�nglich ermittelt. ' .PHP_EOL;
                	}
                	$Text  = 'FEHLER! Es wurden WENIGER Datens�tze in die Textdatei geschrieben, wie urspr�nglich ermittelt. ' . PHP_EOL;
                	$Text .= 'Urspr�nglich Ermittelt: ' . $AnzahlExportDatensaetze . PHP_EOL;
                	$Text .= 'Tats�chlich geschrieben ' . count(file($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp'))  . PHP_EOL;
                	        	
                	$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR EKG_BEWEGDAT_EXPORT',$Text,1,'','awis@de.atu.eu');
                	
                	//Datei auf Error umbennen.
                	rename($this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.temp', $this->_ExportDateiPfad.$this->_ExportDateiName.date("Ymd").'.errOK');    	
                }                         
            }           
        } 
        catch (awisException $ex)
        {
            echo $this->_DB->LetzterSQL().PHP_EOL;      
            echo 'AWIS-Fehler:'.$ex->getMessage().PHP_EOL;            
        }
        catch (Exception $ex)
        {
            echo 'allg. Fehler:'.$ex->getMessage().PHP_EOL;            
        }
        
    }
}



?>
