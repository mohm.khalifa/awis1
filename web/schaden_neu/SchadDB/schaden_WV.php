<?php
/**
 * Schadensdatenbank Wiedervorlagenreiter
 *
 * @author Tobias Schäffler
 * @copyright ATU Auto Teile Unger
 * @version 201511201247
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $rsErgebnis;
global $AWISSprachKonserven;
global $meldung;
global $Fehler;

try
{
	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Seite = 1;
	
	
	$script = "<script type='text/javascript'>
	    	      	
	function Wechsel() {
			
			if(document.getElementById('SELECT').style.display == 'none')
			{
				document.getElementById('AREA').style.display = 'none';
				document.getElementById('SELECT').style.display = 'block';
			}
			else
			{
				document.getElementById('AREA').style.display = 'block';
				document.getElementById('SELECT').style.display = 'none';
			}	
		}
	function Uebergabe(obj){
		var checkText = obj.options[obj.selectedIndex].text;
		if(checkText == '::bitte wählen::')
		{
			checkText = '';
		}
		document.getElementsByName('txtBES_WVAN')[0].value = checkText;
		document.getElementById('AREA').style.display = 'block';
		document.getElementById('SELECT').style.display = 'none';
	}
	
	</script>";
	
	echo $script;
	
	
	$FeldBreiten = array();
	$FeldBreiten['DATUM'] = 110;
	$FeldBreiten['WVAN'] = 150;
	$FeldBreiten['SCHADEN_NR'] = 100;
	$FeldBreiten['SCHADBESCH'] = 50;
	$FeldBreiten['WVGRUND'] = 600;
	
	$SQL=	'select WV_ID,WV_DATUM,BEARBEITUNGSNR,WV_GRUND,WV_AN,SCHADEN_BESCHWERDE,WV_USER,WV_USERDAT, ';	
	$SQL .='row_number() OVER (';
	$SQL .= ' order by WV_ID';
	$SQL .= ') AS ZeilenNr from( ' ;
	$SQL .= 'select * from WIEDERVORLAGEN_NEW ';	
	$SQL .= ' where BEARBEITUNGSNR ='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
	
	if(isset($_GET['ID']) or $AWIS_KEY2 != '')
	{
		if(isset($_GET['ID']))
		{
			$SQL .= ' and WV_ID ='.$_GET['ID'];
		}
		else 
		{
			$SQL .= ' and WV_ID ='.$AWIS_KEY2;
		}
	}
	$SQL .= ')';
		
		
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	
	$Recht40006 = $AWISBenutzer->HatDasRecht(40006);
	
	
	if($Recht40006==0)
	{
		$Form->Fehler_KeineRechte();
		die;
	}
	
	
	// Zum Blättern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
			
	}
		
		
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	$rsWV = $DBSCHAD->RecordSetOeffnen($SQL);

	if(!isset($_GET['ID']) and $AWIS_KEY2 == '')
	{
		if($meldung != '')
		{
			$Form->Hinweistext($meldung);
		}
		else
		{
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
		$Form->ZeileStart('width:100%;');
		if(($Recht40006&8)==8)
		{
			$Icons = array();
			$Icons[] = array('new','./schaden_Main.php?cmdAktion=Details&Seite=WV&ID=-1'.'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'],'g');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}
		elseif((($Recht40006&2)==2 or ($Recht40006&4)==4))
		{
			$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
		}	
			
			
		$Link= '';
		// Überschrift der Listenansicht mit Sortierungslink: Filiale
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_WVDATUM'], $FeldBreiten['DATUM'], '', $Link);
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_LANG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_LANG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_WVAN'], $FeldBreiten['WVAN'], '', $Link);
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_AKTIV,QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_AKTIV,QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_SCHADENNR'], $FeldBreiten['SCHADEN_NR'], '', '');
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGAB'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['SCHADBESCH'], '', $Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_WVGRUND'], $FeldBreiten['WVGRUND'], '', $Link);
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGBIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->ZeileEnde();
			
	
		
		$DS = 0;
		while(! $rsWV->EOF())
		{
				
			$Form->ZeileStart('width:100%');
			if((($Recht40006&2)==2 or ($Recht40006&4)==4))
			{
				$Icons = array();
				if(($Recht40006&2)==2)
				{
					$Icons[] = array('edit','./schaden_Main.php?cmdAktion=Details&Seite=WV&ID='.$rsWV->FeldInhalt('WV_ID').'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR']);
				}
				if(($Recht40006&4)==4)
				{
					$Icons[] = array('delete','./schaden_Main.php?cmdAktion=Details&Seite=WV&Del='.$rsWV->FeldInhalt('WV_ID').'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR']);
				}
				$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			}
			else 
			{
				if(($Recht40006&8)==8)
				{
					$Form->Erstelle_ListenFeld('BES_BLANK','', 0, $FeldBreiten['Blank'], false, ($DS%2), '','', 'T', 'L','');
				}
			}
			$TTT = $rsWV->FeldInhalt('WV_DATUM');
			$Form->Erstelle_ListenFeld('BES_WVDATUM',$Form->Format('D',$rsWV->FeldInhalt('WV_DATUM')), 0, $FeldBreiten['DATUM'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsWV->FeldInhalt('WV_AN');
			$Form->Erstelle_ListenFeld('BES_WVAN',$rsWV->FeldInhalt('WV_AN'), 0, $FeldBreiten['WVAN'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsWV->FeldInhalt('BEARBEITUNGSNR');
			$Form->Erstelle_ListenFeld('BES_BEARBEITUNGSNR',$rsWV->FeldInhalt('BEARBEITUNGSNR'), 0, $FeldBreiten['SCHADEN_NR'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsWV->FeldInhalt('SCHADEN_BESCHWERDE');
			$Form->Erstelle_ListenFeld('BES_SCHADBESCH',$rsWV->FeldInhalt('SCHADEN_BESCHWERDE'), 0, $FeldBreiten['SCHADBESCH'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsWV->FeldInhalt('WV_GRUND');
			$Form->Erstelle_ListenFeld('BES_WVGRUND',strlen($rsWV->FeldInhalt('WV_GRUND')) > 70 ? substr($rsWV->FeldInhalt('WV_GRUND'),0,69).'...':$rsWV->FeldInhalt('WV_GRUND'), 0, $FeldBreiten['WVGRUND'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsWV->DSWeiter();
					
		}
			
			
		$Link = './schaden_Main.php?cmdAktion=Details&Seite='.$_GET['Seite'].'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'];
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	else 
	{
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./schaden_Main.php?cmdAktion=Details&Seite=WV&BEARBEITUNGSNR=".$_GET['BEARBEITUNGSNR']." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsWV->FeldInhalt('WV_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsWV->FeldInhalt('WV_USERDAT'));
		$Form->ZeileStart();
		$Form->InfoZeile($Felder,'');
		$Form->ZeileEnde();
		
		$FehlerText = '';
		if(($Fehler&1) == 1)
		{
			$FehlerText .= $AWISSprachKonserven['BES']['FEHLER_DATUM'];
		}
		if($FehlerText != '')
		{
			$Form->Hinweistext($FehlerText);
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WVDATUM'] . ':',160);
		$Form->Erstelle_TextFeld('!BES_DATUM',(isset($_POST['txtBES_WVDATUM'])?$_POST['txtBES_WVDATUM']:($rsWV->FeldInhalt('WV_DATUM') == ''?date('d.m.Y',time()+86400):$rsWV->FeldInhalt('WV_DATUM'))), 10, 223,true, '','', '','D', 'L','','',10);
		$Form->ZeileEnde();

		$AWISCursorPosition = 'txtBES_DATUM';

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WVAN'] . ':',160);
		$SQL ='select SBNAME,SBNAME as NAME from SACHBEARBEITER where ((sbart = 1) or (sbart = 0 and AWIS_USER is not null)) and ausgeschieden = 0 order by SBART,SBNAME';
		$Form->Erstelle_SelectFeld('!BES_WVAN',isset($_POST['txtBES_WVAN'])?$_POST['txtBES_WVAN']:$rsWV->FeldInhalt('WV_AN'),110,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SCHADENNR'] . ':',160);
		$Form->Erstelle_TextFeld('BES_SCHADENSNR',isset($_POST['txtBES_SCHADENSNR'])?$_POST['txtBES_SCHADENSNR']:$Form->Format('T',$rsErgebnis->FeldInhalt('BEARBEITUNGSNR')), 10, 95,false, '','text-align:right', '','T', 'L');
		$Form->ZeileEnde();
		
		if(isset($_POST['txtBES_SCHADENSNR']))
		{
			$SCHADENNR = $_POST['txtBES_SCHADENSNR']; 
		}
		else if( $rsWV->FeldInhalt('BEARBEITUNGSNR') != '')
		{
			$SCHADENNR = $rsWV->FeldInhalt('BEARBEITUNGSNR');
		}
		else 
		{
			$SCHADENNR = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		}
		$Form->Erstelle_HiddenFeld('SCHADENSNR',$SCHADENNR);
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ART'] . ':',160);
		$Form->Erstelle_TextFeld('BES_ART',isset($_POST['txtBES_ART'])?$_POST['txtBES_ART']:$rsWV->FeldInhalt('SCHADEN_BESCHWERDE')!=''?$rsWV->FeldInhalt('SCHADEN_BESCHWERDE'):'S', 10, 95,false, '','', '','T', 'L');
		$Form->ZeileEnde();
		
		if(isset($_POST['txtBES_ART']))
		{
			$ART = $_POST['txtBES_ART'];
		}
		else if($rsWV->FeldInhalt('SCHADEN_BESCHWERDE') !='')
		{
			$ART = $rsWV->FeldInhalt('SCHADEN_BESCHWERDE');
		}
		else 
		{
			$ART = 'S';
		}
		$Form->Erstelle_HiddenFeld('ART',$ART);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WVGRUND'] . ':',160);
		$Form->Erstelle_Textarea('!BES_WVGRUND',(isset($_POST['txtBES_WVGRUND'])?$_POST['txtBES_WVGRUND']:($rsWV->FeldInhalt('WV_GRUND')!=''?$rsWV->FeldInhalt('WV_GRUND'):'o.E.')),750,90,3,true);
		$Form->ZeileEnde();

		if(isset($_GET['ID']))
		{
			$KEY =	$_GET['ID']; 
		}
		else 
		{
			$KEY = $AWIS_KEY2;
		}
		$Form->Erstelle_HiddenFeld('ID',$KEY);
		
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
	}
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>