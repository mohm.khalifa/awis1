<?php
/**
 * Erzeugt die SQL Anweisung aus den Bedingungen
 *
 * @param awisDatenbank $DB
 * @param int $XDEKEY
 * @param int $XABKEY
 * @return string
 */
//function ErstelleAbfrage($con, $XDEKEY, $XABKEY=0, $Sprache='DE')
function ErstelleAbfrage(awisDatenbank $DB, $XDEKEY, $XABKEY=0, $Sprache='DE')
{
	// Basisdaten zur Abfrage laden
	$SQL = 'SELECT *';
	$SQL .= ' FROM ExportDaten ';
	$SQL .= ' LEFT OUTER JOIN ExportAbfragen ON XAB_XDE_KEY = XDE_KEY AND XAB_KEY='.$DB->WertSetzen('XAB','N0',$XABKEY);
	$SQL .= ' WHERE ROWNUM = 1 AND XDE_KEY='.$DB->WertSetzen('XAB','N0',$XDEKEY);
	$rsXAB = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('XAB'));

	// Jetzt alle Felder, die in der Auswahl stehen
	$SQL = 'SELECT *';
	$SQL .= ' FROM ExportDatenFelder ';
	$SQL .= ' WHERE XDF_XDE_KEY ='.$DB->WertSetzen('XDF','N0',$XDEKEY);

	$rsXAK = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('XDF'));
	
	$rsXAKZeilen = $rsXAK->AnzahlDatensaetze();
	$ParameterListe = array();
	
	$BASISSQL = $rsXAB->FeldInhalt('XDE_SQL');			// Basis Abfrage
	
	// Hier stehen alle Felder, die ausgegeben werden sollen
	$Felder = explode(',',$rsXAB->FeldInhalt('XAB_FELDLISTE'));
	if(count($Felder)==0 OR $Felder[0]=='')
	{
		$SELECT = ' ROWNUM AS XXX_ROWNUM';	// Pseudospalte ausgeben
	}
	else
	{
		$SELECT = '';
		foreach ($Felder AS $Feld)
		{			
			$rsXAK->DSErster();
			while(!$rsXAK->EOF())
			{
				if($rsXAK->FeldInhalt('XDF_KEY')==$Feld)
				{
					if($rsXAK->FeldInhalt('XDF_AUSDRUCK')=='')			// Wenn kein Ausdruck angegeben wurde, das Feld direkt nehmen
					{
						$SELECT .= ', '.$rsXAK->FeldInhalt('XDF_FELDNAME');
					}
					else 
					{
						$SELECT .= ', '.$rsXAK->FeldInhalt('XDF_AUSDRUCK').' AS '.$rsXAK->FeldInhalt('XDF_FELDNAME'); 
					}
					break;
				}
				
				$rsXAK->DSWeiter();
			}
		}
	}

	$rsXAK->DSErster();
	while(!$rsXAK->EOF())
	{
		if(substr($rsXAK->FeldInhalt('XDF_FELDNAME'),0,2)=='$$')
		{
			$ParameterListe[$rsXAK->FeldInhalt('XDF_FELDNAME')] = $DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),'',false);
		}
		
		$rsXAK->DSWeiter();
	}

	
	// Bedingung
	$SQL = 'SELECT *';
	$SQL .= ' FROM ExportAbfragenKriterien ';
	$SQL .= ' INNER JOIN ExportDatenFelder ON XAK_XDF_KEY = XDF_KEY';
	$SQL .= ' WHERE XAK_XAB_KEY = '.$DB->WertSetzen('XAK','N0', $XABKEY);
	$SQL .= ' ORDER BY XAK_GRUPPE, XAK_KEY';
	$rsXAK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('XAK'));
	$rsXAKZeilen = $rsXAK->AnzahlDatensaetze();

	$Bedingung = '';
	$LetzteGruppe = 0;
	
	//for($i=0;$i<$rsXAKZeilen;$i++)
	while(!$rsXAK->EOF())
	{
		if(substr($rsXAK->FeldInhalt('XDF_FELDNAME'),0,2)=='$$')	// Pflichtparameter
		{
			$ParameterListe[$rsXAK->FeldInhalt('XDF_FELDNAME')] = $DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')),true);
		}
		else
		{
			// Neue Gruppe-> Klammern setzen
			if($LetzteGruppe != $rsXAK->FeldInhalt('XAK_GRUPPE'))
			{
				$Bedingung .= ($Bedingung==''?'':')');
				$LetzteGruppe = $rsXAK->FeldInhalt('XAK_GRUPPE');
				$Bedingung .= sprintf('%4s',$rsXAK->FeldInhalt('XAK_BEDINGUNGGRUPPE')).' (';
			}
			else
			{
				$Bedingung .= sprintf('%4s',$rsXAK->FeldInhalt('XAK_BEDINGUNGGRUPPE')).' ';
			}
			// Feldname hinzufuegen
			$Bedingung.=$rsXAK->FeldInhalt('XDF_FELDNAME');
	
			// Bedingung hinzuf�gen
			switch ($rsXAK->FeldInhalt('XAK_VERKNUEPFUNG'))
			{
				case 1:			// =
					$Bedingung .= ' = '.$DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))).'';
					break;
				case 2:			// >
					$Bedingung .= ' > '.$DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))).'';
					break;
				case 3:			// <
					$Bedingung .= ' < '.$DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))).'';
					break;
				case 4:			// >=
					$Bedingung .= ' >= '.$DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))).'';
					break;
				case 5:			// <=
					$Bedingung .= ' <= '.$DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))).'';
					break;
				case 6:			// <>
					$Bedingung .= ' <> '.$DB->FeldInhaltFormat($rsXAK->FeldInhalt('XDF_DATENTYP'),ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'))).'';
					break;
				case 7:			// LEER
					$Bedingung .= ' IS NULL';
					break;
				case 8:			// NICHT LEER
					$Bedingung .= ' IS NOT NULL';
					break;
				case 9:			// Beginnt mit
					$Bedingung .= ' LIKE \''.ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')).'%\'';
					break;
				case 10:			// Endet mit
					$Bedingung .= ' LIKE \'%'.ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')).'\'';
					break;
				case 11:			// Enth�lt
					$Bedingung .= ' LIKE \'%'.ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT')).'%\'';
					break;
			}
		}
		
		$rsXAK->DSWeiter();
	}
	$Bedingung .= ($Bedingung==''?'':')');
	
	$Erg = ' SELECT '.substr($SELECT,1);
	$Erg .= ' FROM (';
	$Erg .= $BASISSQL;
	$Erg .= ')';
	if($Bedingung!='')
	{
		$Erg .= ' WHERE '.substr($Bedingung,4);
	}

	// Alle festen Parameter ersetzen
	foreach($ParameterListe AS $Parameter=>$Wert)
	{
		$Erg = str_replace($Parameter,$Wert,$Erg);
	}
	
	// TODO: So umbauen
	/*
	$Erg = str_replace('$$DATUM$$', $DB->FeldInhaltFormat('D',date('d.m.Y')), $Erg);
	$Erg = str_replace('$$MONATSERSTER$$', $DB->FeldInhaltFormat('D',date('d.m.Y',mktime(0,0,0,date('m'),1,date('Y')))), $Erg);
	$Erg = str_replace('$$MONATSLETZTER$$', $DB->FeldInhaltFormat('D',date('d.m.Y',mktime(0,0,0,date('m')+1,0,date('Y')))), $Erg);
	*/
	
	
	return $Erg;
}

/**
 * Ersetzt Parameter durch Ihre Werte
 * @param string $Parameter
 */
function ErsetzeParameter($Parameter)
{
	$Erg = $Parameter;

	if(substr($Parameter,0,1)=='$')
	{
		switch(strtoupper(substr($Parameter,1)))
		{
			case 'DATUM':
				$Erg = date('d.m.Y');
				break;
			case 'MONAT':
				$Erg = date('m');
				break;
			case 'TAG':
				$Erg = date('d');
				break;
			case 'TAGE':
				$Erg = date('z');
				break;
			case 'JAHR':
				$Erg = date('Y');
				break;
			case 'MONATSERSTER':
				$Erg = date('d.m.Y',mktime(0,0,0,date('m'),1,date('Y')));
				break;
			case 'MONATSLETZTER':
				$Erg = date('d.m.Y',mktime(0,0,0,date('m')+1,0,date('Y')));
				break;
			case 'BENUTZER':
				require_once('awisBenutzer.inc');
				$Benutzer = awisBenutzer::Init();
				$Erg = $Benutzer->BenutzerName();
				break;
			case 'BENUTZERID':
				require_once('awisBenutzer.inc');
				$Benutzer = awisBenutzer::Init();
				$Erg = $Benutzer->BenutzerID();
				break;
			default:
				$Erg = '';
		}
	}
	
	
	return $Erg;
}
?>