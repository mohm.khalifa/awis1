<?php

global $_POST;
global $_GET;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

define('FPDF_FONTPATH','font/');
require 'fpdi.php';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung möglich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con, 1300);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleinsätze',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

$Params = explode(";",awis_BenutzerParameter($con, "Personaleinsatz_Suche", $AWISBenutzer->BenutzerName()));


$RZListeStufe = awisBenutzerRecht($con, 1302);		// Einschränkungen nach RZ
$RZListe = explode(";",awis_BenutzerParameter($con, "Mitarbeiter_RZListe_Rechte", $AWISBenutzer->BenutzerName()));
$RZUserListe = '';
for($i=0;$i<16;$i++)
{
	if(($RZListeStufe&pow(2,$i))==pow(2,$i))
	{
		$RZUserListe .= ';' . $RZListe[$i+1];
	}
}
$RZUserListe .= ';';


$SQL = 'SELECT * ';
$SQL .= ' FROM MitarbeiterEinsaetze';
$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
$SQL .= ' INNER JOIN Mitarbeiter ON MEI_MIT_KEY = MIT_KEY';
$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';

$Bedingung = '';
if($Params[1]!='0')		//MIT_KEY
{
	$Bedingung .= ' AND MEI_MIT_KEY=0' . $Params[1];
}
if($Params[2]!='')		//txtDatumVom
{
	$Bedingung .= " AND MEI_TAG>=TO_DATE('" . $Params[2] . "','DD.MM.RRRR')";
}
if($Params[3]!='')		//txtDatumBis
{
	$Bedingung .= " AND MEI_TAG<=TO_DATE('" . $Params[3] . "','DD.MM.RRRR')";
}
if($Params[4]!='')		//txtFIL_ID
{
	$Bedingung .= " AND MEI_FIL_ID=" . $Params[4] . "";
}
if($Params[5]!='0' AND $Params[5]!='')		//Regionalzentren
{
	$Bedingung .= " AND MIT_REZ_ID=" . $Params[5] . "";
}
if($Params[6]!='-1' AND $Params[6]!='')		//Regionalzentren
{
	$Bedingung .= " AND MEI_MEA_KEY=" . $Params[6] . "";
}
if($Params[7]!='-1' AND $Params[7]!='')		//Tätigkeit
{
	$Bedingung .= " AND MIT_MTA_ID=" . $Params[7] . "";
}
$Bedingung .= " AND MIT_BEREICH='" . $Params[8] . "'";
if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

$SQL .= ' ORDER BY MEI_TAG, MIT_REZ_ID, MIT_BEZEICHNUNG, MEI_VonXTZ_ID';

awis_Debug(1,$SQL);

/*
	$Params[0] = $_POST['txtAuswahlSpeichern'];
	$Params[1] = $_POST['txtMIT_KEY'];
	$Params[2] = $_POST['txtDatumVom'];
	$Params[3] = $_POST['txtDatumBis'];
	$Params[4] = $_POST['txtFIL_ID'];
	$Params[5] = $_POST['txtMIT_REZ_ID'];
	$Params[6] = $_POST['txtMEI_MEA_KEY'];
*/

$rsMEI = awisOpenRecordset($con, $SQL );
$rsMEIZeilen = $awisRSZeilen;

	// Tageszeiten
$rsXTZ = awisOpenRecordset($con, 'SELECT * FROM TagesZeiten');
$TagesZeiten=array();
for($i=0;$i<$awisRSZeilen;$i++)
{
	$TagesZeiten[$rsXTZ['XTZ_ID'][$i]]=$rsXTZ['XTZ_TAGESZEIT'][$i];
}
unset($rsXTZ);

/*************************************
* PDF aufbauen
*************************************/
$pdf = new fpdi('l','mm','a4');
$pdf->open();
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);

$RasterZeile=9999;
$LetzterMitarbeiter = '';
$LetztesDatum = '';
$LetztesREZ='';

if($rsMEIZeilen==0)
{
	$pdf->addpage();
	$pdf->useTemplate($ATULogo,270,4,20);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
}

for($i=0;$i<$rsMEIZeilen;$i++)
{
		// Nur Mitarbeiter aus einem Regionalzentrum anzeigen
	if($RZUserListe!=';')
	{
		if(strstr($RZUserListe,';'.$rsMEI['MIT_REZ_ID'][$i].';')=='')
		{
			continue;
		}
	}

	if($LetzterMitarbeiter!=$rsMEI['MIT_KEY'][$i])
	{
				// Neuer Tag?
		$Ausgabe = '';
		if($LetztesDatum!=$rsMEI['MEI_TAG'][$i] OR $LetztesREZ!=$rsMEI['MIT_REZ_ID'][$i])
		{
			$LetztesDatum=$rsMEI['MEI_TAG'][$i];
			$LetztesREZ=$rsMEI['MIT_REZ_ID'][$i];
			$Ausgabe .= $rsMEI['MEI_TAG'][$i];
			$Ausgabe .= ' ' . $rsMEI['REZ_BEZEICHNUNG'][$i];
			$RasterZeile = 9999;
		}

			// Neue Seite beginnen
		if($RasterZeile > 190)
		{
			$pdf->addpage();
			$pdf->useTemplate($ATULogo,270,4,20);
			$pdf->SetFont('Arial','',6);
			$pdf->cell(270,6,'Diese Liste ist nur für interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

			$pdf->setXY(270,12);
			$pdf->SetFont('Arial','',6);
			$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);

			$RasterZeile = 10;
			$pdf->SetFont('Arial','',20);
			$pdf->text(10,$RasterZeile,"Mitarbeitereinsätze");
//			$pdf->image('../bilder/atulogo_neu.png',240,5);
			$RasterZeile = 40;
			$pdf->SetFont('Arial','',10);

			$pdf->SetFillColor(240,240,240);
			$pdf->SetDrawColor(0,0,0);
			$pdf->setXY(10,$RasterZeile-10);

			$pdf->cell(80,6,'Mitarbeiter',1,0,'C',1);

			for($Std=90;$Std<=240;$Std+=10)
			{
				$pdf->setXY(($Std*1.2)-30,$RasterZeile-10);
				$pdf->SetDrawColor(0,0,0);
				$pdf->cell(12,6,$TagesZeiten[$Std],1,0,'C',1);

				$pdf->SetDrawColor(230,230,230);
				$pdf->line(($Std*1.2)-30,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);
			}
			$pdf->SetDrawColor(230,230,230);
			$pdf->line(($Std*1.2)-930,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);

		}


		if($Ausgabe != '')		// Tages/REZ Überschrift
		{
			$pdf->setXY(10,$RasterZeile-3);
			$pdf->SetFont('Arial','',12);
			$pdf->cell(260,6,$Ausgabe,0,0,'L',1);
			$RasterZeile+=8;
		}

				// Neuer Mitarbeiter
		$SQL = "SELECT KKO_KURZWAHL FROM KontakteKommunikation WHERE KKO_KOT_Key =6 AND KKO_KON_Key=0" . $rsMEI['MIT_KON_KEY'][$i] . "";
		$rsKKO = awisOpenRecordset($con, $SQL);

		$pdf->SetDrawColor(230,230,230);
		$pdf->SetFont('Arial','',12);
		$pdf->text(12,$RasterZeile,$rsMEI['MIT_BEZEICHNUNG'][$i] . ', ' . (isset($rsKKO['KKO_KURZWAHL'][0])?$rsKKO['KKO_KURZWAHL'][0]:''));
//		$pdf->line(220,$RasterZeile+4,290,$RasterZeile+4);
		$RasterZeile += 6;
		$LetzterMitarbeiter=$rsMEI['MIT_KEY'][$i];
		unset($rsKKO);
	}	// Neuer Mitarbeiter
	elseif($LetztesDatum!=$rsMEI['MEI_TAG'][$i])		// gleiche Daten an einem anderen TAG
	{
		$RasterZeile+=6;
		if($RasterZeile > 190)
		{
			$pdf->addpage();
			$pdf->SetFont('Arial','',6);
			$pdf->cell(270,6,'Diese Liste ist nur für interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

			$RasterZeile = 10;
			$pdf->SetFont('Arial','',20);
			$pdf->text(10,$RasterZeile,"Mitarbeitereinsätze");
			$pdf->image('../bilder/atulogo_neu.png',240,5);
			$RasterZeile = 40;
			$pdf->SetFont('Arial','',10);

			$pdf->SetFillColor(240,240,240);
			$pdf->SetDrawColor(0,0,0);
			$pdf->setXY(10,$RasterZeile-10);

			$pdf->cell(80,6,'Mitarbeiter',1,0,'C',1);

			for($Std=90;$Std<=240;$Std+=10)
			{
				$pdf->setXY(($Std*1.2)-30,$RasterZeile-10);
				$pdf->SetDrawColor(0,0,0);
				$pdf->cell(12,6,$TagesZeiten[$Std],1,0,'C',1);

				$pdf->SetDrawColor(230,230,230);
				$pdf->line(($Std*1.2)-30,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);
			}
			$pdf->SetDrawColor(230,230,230);
			$pdf->line(($Std*1.2)-30,$RasterZeile-3,($Std*1.2)-30,$RasterZeile+148);
			$RasterZeile = 50;

		}

		$pdf->SetFont('Arial','',6);
		$pdf->setXY(64,$RasterZeile-10);
		$pdf->cell(10,6,$rsMEI['MEI_TAG'][$i],0,0,'R',0);
		$LetztesDatum=$rsMEI['MEI_TAG'][$i];
	}

			// Einträge schreiben
	$pdf->SetFont('Arial','',6);
	$pdf->SetDrawColor(0,0,0);

	$PosVon = ($rsMEI['MEI_VONXTZ_ID'][$i]*1.2)-30;
	$PosBis = ($rsMEI['MEI_BISXTZ_ID'][$i]*1.2)-30;

	$pdf->setXY($PosVon,$RasterZeile-10);

	$Text = $rsMEI['MEA_BEZEICHNUNG'][$i];
	if($rsMEI['MEA_KEY'][$i]==8)		// Filiale
	{
		$Text = '' . $rsMEI['MEI_FIL_ID'][$i];
		$pdf->SetFillColor(250,250,250);
	}
	elseif($rsMEI['MEA_KEY'][$i]==9)		// Auto
	{
		$Text = '';
		$pdf->SetFillColor(230,230,230);
	}
	else
	{
		$pdf->SetFillColor(255,255,255);
	}
	$Text .= ' ' . $rsMEI['MEI_BEMERKUNG'][$i];
	$pdf->cell(($PosBis -$PosVon),6,$Text,1,0,'C',1);

}		// Ende for

/**************************************
* Datei speichern
**************************************/

$DateiName = awis_UserExportDateiName('.pdf');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];
//$pdf->saveas($DateiName);
$pdf->Output($DateiName,'F');
echo "<br><a target=_new href=$DateiNameLink>PDF Datei öffnen</a>";
echo "<br><a href=./perseins_Main.php?cmdAktion=Einsaetze>Zurück</a>";

awislogoff($con);
?>