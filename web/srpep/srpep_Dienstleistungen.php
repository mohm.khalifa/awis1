<?php
/**
 * Passwort zur�cksetzen
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20091008
 *
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFilialen.inc');
global $AWISCursorPosition;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$AWISBenutzer = awisBenutzer::Init();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('DPR','%');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','HinweisZuruecksetzen');
	$TextKonserven[]=array('Wort','FehlerNichtGefunden');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht11003=$AWISBenutzer->HatDasRecht(11003);		// Rechte des Mitarbeiters
	if($Recht11003==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	if(isset($_POST['cmdSpeichern_x']))
	{
		include './srpep_speichern.php';
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./srpep_Main.php?cmdAktion=Dienstleistungen>");
	$Form->Formular_Start();

	$SQL = 'SELECT XHW_KEY, XHW_WERTTXT, COALESCE(DPR_BEZEICHNUNG,\'??????????\') AS DPR_BEZEICHNUNG';
	$SQL .= ' FROM Hilfswerte';
	$SQL .= ' LEFT OUTER JOIN Dienstleistungspreise ON XHW_WERTTXT = DPR_NUMMER';
	$SQL .= ' WHERE XHW_BEREICH = \'SRPLANUNG\'';
	$SQL .= ' ORDER BY 2';
	$rsXHW = $DB->RecordSetOeffnen($SQL);

	$EditModus = ($Recht11003&6);
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DPR']['DPR_NUMMER'],120,'','','Tooltipptext');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DPR']['DPR_BEZEICHNUNG'],450);
	$Form->ZeileEnde();

	$DS=0;
	$AWISCursorPosition = '';
	while(!$rsXHW->EOF())
	{
		//$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Form->ZeileStart();
		$Form->Erstelle_ListenFeld('XHW_WERTTXT_'.$rsXHW->FeldInhalt('XHW_KEY'),$rsXHW->FeldInhalt('XHW_WERTTXT'),8,120,$EditModus,($DS%2),'','','T');
		if($EditModus AND $AWISCursorPosition=='')
		{
			$AWISCursorPosition = 'txtXHW_WERTTXT_'.$rsXHW->FeldInhalt('XHW_KEY');
		}
		$Form->Erstelle_ListenFeld('#DPR_BEZEICHNUNG',$rsXHW->FeldInhalt('DPR_BEZEICHNUNG'),0,450,false,($DS%2),'','','T');
		$Form->ZeileEnde();

		$rsXHW->DSWeiter();
		$DS++;
	}
	if(($Recht11003&4)==4)
	{
		$Form->ZeileStart();
		$Form->Erstelle_ListenFeld('XHW_WERTTXT_0',$rsXHW->FeldInhalt('XHW_WERTTXT'),8,120,$EditModus,($DS%2),'','','T');
		$Form->Erstelle_ListenFeld('#DPR_BEZEICHNUNG','',0,450,false,($DS%2),'','','T');
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if((($Recht11003&4)==4))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200912171119");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200912171120");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>
