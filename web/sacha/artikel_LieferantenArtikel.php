<?php
	global $PHP_AUTH_USER;
	global $HTTP_GET_VARS;
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

	$con=awisLogon();	
	$Rechte_LAR = awisBenutzerRecht($con,402);	
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $PHP_AUTH_USER));

	
	$SQL = "SELECT * ";
	$SQL .= " FROM AWIS.LieferantenArtikel, AWIS.Lieferanten, AWIS.ArtikelPruefPersonal, AWIS.TeileInfos WHERE APP_ID(+) = LAR_APP_ID AND LIE_NR(+) = LAR_LIE_NR AND TEI_KEY2 = LAR_KEY AND TEI_KEY1=0" . $Artikel[0] . "";
	
	if(!isset($HTTP_GET_VARS['LARSort']))
	{
		$SQL .= " ORDER BY LIE_NR, LAR_LARTNR";
	}
	else
	{
		$SQL .= " ORDER BY " . $HTTP_GET_VARS['LARSort'] . ", LAR_LARTNR";
	}

	$rsLiefArt = awisOpenRecordset($con, $SQL);
	if($rsLiefArt==FALSE)
	{
		awisErrorMailLink("artikel_LieferantenArtikel.php", 2, $awisDBFehler['message']);
	}
	$rsLiefartZeilen=$awisRSZeilen;

	$MitReklInfo = awis_BenutzerParameter($con,"ReklamationsInfoLieferanten" , $PHP_AUTH_USER );
	
	print "<table id=DatenTabelle width=100% border=1>";
	print "<tr><td id=FeldBez></td>";
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LAR_LARTNR&Key=" . $Artikel[0] . "><u>L</u>iefArtNr</a></td>";
	if($MitReklInfo)
	{
		print "<td id=FeldBez>Info</td>";
	}
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LAR_LIE_NR&Key=" . $Artikel[0] . ">LiefNr</a></td>";	
	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=LIE_NAME1&Key=" . $Artikel[0] . ">Lieferant</a></td>";	
	print '<td id=FeldBez>Teileinfo</td>';
//	print "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=LieferantenArtikel&LARSort=APP_NAME&Key=" . $Artikel[0] . ">Pr�fer</a></td>";	
	print "<td id=FeldBez>Zuord.</td>";
	print "<td id=FeldBez>Rekl.</td>";
	print "</tr>";

			// Hinzuf�gen von Datens�tzen
	if(($Rechte_LAR & 2)==2)
	{
		print "<tr>";
		
		// Lieferantenartikel
		print "<td></td>";
		print "<td><input accesskey=l name=txtLAR_LARTNR_0></td>";
		if($MitReklInfo)
		{
			print "<td>&nbsp;</td>";
		}

		print "<td><input name=txtLAR_LIE_NR size=6 onchange=document.getElementsByName(\"txtLAR_LIE_NR2\")[0].value=''> oder </td>";
		print "<td><select name=txtLAR_LIE_NR2 onchange=document.getElementsByName(\"txtLAR_LIE_NR\")[0].value=document.getElementsByName(\"txtLAR_LIE_NR2\")[0].value;>";
		
		$rsLieferanten = awisOpenRecordset($con, "SELECT * FROM AWIS.Lieferanten ORDER BY LIE_Name1");
		$rsLieferantenZeilen = $awisRSZeilen;
		print "<option value=0>Bitte w�hlen...</option>";
		
		for($LiefNr=0;$LiefNr<$rsLieferantenZeilen;$LiefNr++)
		{
			print "<option value=" . $rsLieferanten["LIE_NR"][$LiefNr] . ">" . $rsLieferanten["LIE_NAME1"][$LiefNr] . "</option>";
		}
		unset($rsLieferanten);
		
		print "</select></td>";


		print "<td>/</td>";			// Pr�fer
		print "<td>/</td>";
		print "<td>/</td>";			// Reklamation
		
		print "</tr>";
	
	}

	for($LiefArt=0;$LiefArt<$rsLiefartZeilen;$LiefArt++)
	{
		print "<tr>";

		$ZweitArtikel = +1;	
			// Zwei gleiche Artikel, Nur die Benutzer sind in der falschen Reihenfolge
		if($rsLiefArt["LAR_LARTNR"][$LiefArt]==$rsLiefArt["LAR_LARTNR"][$LiefArt+1] AND
			$rsLiefArt['LAR_LIE_NR'][$LiefArt]==$rsLiefArt['LAR_LIE_NR'][$LiefArt+1] AND
			$rsLiefArt['TEI_USER'][$LiefArt]=='WWS')
		{
			$LiefArt++;		
			$ZweitArtikel = -1;
		}		
			
			
		if(($Rechte_LAR & 2)==2 AND strtoupper($rsLiefArt["TEI_USER"][$LiefArt])!='WWS')
		{
			print "<td><input name=cmdLARLoeschen_" . $rsLiefArt["TEI_KEY"][$LiefArt] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\" onmouseover=\"window.status='Zuordnung l�schen';return true;\"></td>";
		}
		else
		{
			print "<td></td>";
		}

			// 1. Spalte -> LieferantenArtikelNummer
		print "<td nowrap ><a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsLiefArt["LAR_KEY"][$LiefArt] . " onmouseover=\"window.status='Lieferantenartikel';return true;\" onfocus=\"window.status='Lieferantenartikel';return true;\" onmouseout=window.status='' >" . $rsLiefArt["LAR_LARTNR"][$LiefArt] . "</a></td>";
//		print '<td>' . $rsLiefArt["LAR_LARTNR"][$LiefArt] . '</td>';

		if($MitReklInfo)
		{
			print "<td align=center valign=center>";
			$DateiName = realpath('../dokumente/lieferanten/reklamationen') . '/lief_rekl_' . $rsLiefArt['LAR_LIE_NR'][$LiefArt] . '.pdf';
			if(file_exists($DateiName))
			{
				print "<a href=../dokumente/lieferanten/reklamationen/lief_rekl_" . $rsLiefArt['LAR_LIE_NR'][$LiefArt] . ".pdf><img border=0 src=/bilder/pdf.png alt=Anleitung onmouseover=\"window.status='Reklamation';return true;\" onmouseout=\"window.status='';return true;\" onFocus=\"window.status='Reklamation';return true;\"></a>";
			}
			print "</td>";
		}
			// 2. Spalte -> LieferantenNummer
		echo "<td><a href=/stammdaten/lieferanten.php?ASTKEY=" . $Artikel[0] . "&Zurueck=artikel_LIEF&LIENR=" . $rsLiefArt["LAR_LIE_NR"][$LiefArt] . ">" . $rsLiefArt["LAR_LIE_NR"][$LiefArt] . "</a></td>";

			// 3. Spalte -> LieferantenName
		if($rsLiefArt["LIE_NAME1"][$LiefArt]=='')
		{
			echo '<td>::unbekannt::</td>';
		}
		else
		{
			echo '<td>' . $rsLiefArt["LIE_NAME1"][$LiefArt] . '</td>';
		}
			// 3a. Spalte
			
		$Anzeige ='';
		if($rsLiefArt["LAR_ALTERNATIVARTIKEL"][$LiefArt]!=0)
		{
			$Anzeige = ', Akt';
		}
		if($rsLiefArt["LAR_AUSLAUFARTIKEL"][$LiefArt]!=0)
		{
			$Anzeige .= ', Ausl';
		}
		if($rsLiefArt["LAR_ALTELIEFNR"][$LiefArt]!=0)
		{
			$Anzeige .= ', Alt';
		}
		if($rsLiefArt["LAR_PROBLEMARTIKEL"][$LiefArt]!=0)
		{
			$Anzeige .= ', Prob';
		}
		if($rsLiefArt["LAR_GEPRUEFT"][$LiefArt]!=0)
		{
			$Anzeige .= ', Gepr';
		}
		if($rsLiefArt["LAR_MUSTER"][$LiefArt]!=0)
		{
			$Anzeige .= ', M';
		}
		if($rsLiefArt["LAR_BEMERKUNGEN"][$LiefArt].''!='')
		{
			$Anzeige .= ', B';
		}
		echo "<td>" . substr($Anzeige,2) . "</td>";

			// 4. Spalte -> Pr�fer
//		echo "<td>" . substr($rsLiefArt["APP_NAME"][$LiefArt],0,5) . "</td>";

			// 5. Spalte -> Zuordnung (mit Link)
		echo "<td>";
				// Zuordnung aus Crossing
		echo "<a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsLiefArt["LAR_KEY"][$LiefArt] . " onmouseover=\"window.status='Lieferantenartikel';return true;\" onfocus=\"window.status='Lieferantenartikel';return true;\" onmouseout=window.status='' >";
		echo $rsLiefArt["TEI_USER"][$LiefArt] . "</a>";

				// Zuordnung aus WWS
		if($rsLiefArt["LAR_LARTNR"][$LiefArt]==$rsLiefArt["LAR_LARTNR"][$LiefArt+$ZweitArtikel] AND
			$rsLiefArt['LAR_LIE_NR'][$LiefArt]==$rsLiefArt['LAR_LIE_NR'][$LiefArt+$ZweitArtikel])
		{
			echo "/<a href=./artikel_Main.php?cmdAktion=Lieferantenartikel&LARKEY=" . $rsLiefArt["LAR_KEY"][$LiefArt+$ZweitArtikel] . " onmouseover=\"window.status='Lieferantenartikel';return true;\" onfocus=\"window.status='Lieferantenartikel';return true;\" onmouseout=window.status='' >";
			echo $rsLiefArt["TEI_USER"][$LiefArt+$ZweitArtikel] . "</a>";
			if($ZweitArtikel ==1)
			{
				$LiefArt++;
			}	
		}
		echo "</td>";

			// 6. Spalte
		print "<td>" . ($rsLiefArt["LAR_REKLAMATIONSKENNUNG"][$LiefArt]==''?'- -':"<font color=#FF0000>" . $rsLiefArt["LAR_REKLAMATIONSKENNUNG"][$LiefArt]) . "</font></td>";
		
		print "</tr>";
		
		// Gebrauchsnummern anzeigen
		if($HTTP_GET_VARS["GNr"]=='Ja')		
		{
			$rsGNr = awisOpenRecordset($con, "SELECT * FROM AWIS.Gebrauchsnummern, AWIS.TEILEINFOS WHERE GNR_KEY = TEI_KEY2 AND TEI_KEY1 = " . $rsLiefArt["LAR_KEY"][$LiefArt]);
			$rsGNRZeilen = $awisRSZeilen;
			
			if($rsGNRZeilen>0)
			{
 				print "<tr><td></td><td colspan=4>";
				print "<table border=1><cols><col width=180></col><col width=150></col><col width=305></col><col width=110></col><col width=120></col></cols><tr>";
				for($GNr=0;$GNr<$rsGNRZeilen;$GNr++)
				{
					print "<td>Gebrauchsnummer:</td>";
					print "<td><input name=txtGNR_KEY value=" . $rsGNr["GNR_KEY"][$GNr] . "><input name=txtGNR_NUMMER size=20 value=\"" . $rsGNr["GNR_NUMMER"][$GNr] . "\">";
					print "</td>";
					print "<td><input name=txtGNR_BEMERKUNG size=43 value=\"" . $rsGNr["GNR_BEMERKUNG"][$GNr]. "\"></td>";
					print "<td>" . $rsGNr["GNR_USER"][$GNr]. "</td>";
					print "<td>" . $rsGNr["GNR_USERDAT"][$GNr]. "</td>";
				}
				print "</tr></table>";
				print "</td></tr>";
			}
			else
			{
				print "\n<!--Keine Gebrauchsnummern gefunden-->\n";
			}
					
			unset($rsGNr);
		}
	}


	print "</table>";
	
	unset($rsLiefArt);
?>