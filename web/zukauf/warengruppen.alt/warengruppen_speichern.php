<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('ZSZ','ZSZ_%');

$AWIS_KEY1 = $_POST['txtZSZ_KEY'];

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if(intval($_POST['txtZSZ_KEY'])===0)		// Neue Zuordnung
{
		// daten auf Vollst�ndigkeit pr�fen
	$Fehler = '';
	$Pflichtfelder = array();
	foreach($Pflichtfelder AS $Pflichtfeld)
	{
		if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
		{
			$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZSZ'][$Pflichtfeld].'<br>';
		}
	}
		// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		die('<span class=HinweisText>'.$Fehler.'</span>');
	}
	$SQL = 'INSERT INTO ZUKAUFSORTIMENTZUORDNUNGEN';
	$SQL .= '(';
	$SQL .= 'ZSZ_SORTIMENT,ZSZ_WG1,ZSZ_WG2,ZSZ_WG3,ZSZ_ZLI_KEY,ZSZ_UNTERLIEFERANT';
	$SQL .= ',ZSZ_USER,ZSZ_USERDAT';
	$SQL .= ')VALUES ('.awis_FeldInhaltFormat('T',$_POST['txtZSZ_SORTIMENT'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZSZ_WG1'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZSZ_WG2'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZSZ_WG3'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtZSZ_ZLI_KEY'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtZSZ_UNTERLIEFERANT'],true);
	$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
	$SQL .= ',SYSDATE';
	$SQL .= ')';
	if(!awisExecute($con,$SQL))
	{
		awisErrorMailLink('200806261024',1,$awisDBError['message'],'');
		die();
	}
	$SQL = 'SELECT seq_ZSZ_KEY.CurrVal AS KEY FROM DUAL';
	$rsKey = awisOpenRecordset($con,$SQL);
	$AWIS_KEY1=$rsKey['KEY'][0];
	awis_BenutzerParameterSpeichern($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName() ,$rsKey['KEY'][0]);
}
else 					// ge�nderter 
{
	$Felder = explode(';',awis_NameInArray($_POST, 'txtZSZ',1,1));
	$FehlerListe = array();
	$UpdateFelder = '';

	awis_BenutzerParameterSpeichern($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName() ,$_POST['txtZSZ_KEY']);
	$rsZSZ = awisOpenRecordset($con,'SELECT * FROM ZUKAUFSORTIMENTZUORDNUNGEN WHERE ZSZ_key=' . $_POST['txtZSZ_KEY'] . '');
	$FeldListe = '';
	foreach($Felder AS $Feld)
	{
		$FeldName = substr($Feld,3);
		if(isset($_POST['old'.$FeldName]))
		{
			// Alten und neuen Wert umformatieren!!
			switch ($FeldName)
			{
				default:
					$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			}
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
			$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsZSZ[$FeldName][0],true);
	//echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
			if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
			{
				if($WertAlt != $WertDB AND $WertDB!='null')
				{
					$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
				}
				else
				{
					$FeldListe .= ', '.$FeldName.'=';

					if($_POST[$Feld]=='')	// Leere Felder immer als NULL
					{
						$FeldListe.=' null';
					}
					else
					{
						$FeldListe.=$WertNeu;
					}
				}
			}
		}
	}

	if(count($FehlerListe)>0)
	{
		$Meldung = str_replace('%1',$rsZSZ['ZSZ_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
		foreach($FehlerListe AS $Fehler)
		{
			$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
		}
		awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
	}
	elseif($FeldListe!='')
	{
		$SQL = 'UPDATE ZUKAUFSORTIMENTZUORDNUNGEN SET';
		$SQL .= substr($FeldListe,1);
		$SQL .= ', ZSZ_user=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', ZSZ_userdat=sysdate';
		$SQL .= ' WHERE ZSZ_key=0' . $_POST['txtZSZ_KEY'] . '';
		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('ZUKAUFSORTIMENTZUORDNUNGEN',1,'Fehler beim Speichern',$SQL);
		}
	}
	$AWIS_KEY1=$_POST['txtZSZ_KEY'];
}

// Aktuellen ZSZ speichern
awis_BenutzerParameterSpeichern($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName() ,$AWIS_KEY1);
?>