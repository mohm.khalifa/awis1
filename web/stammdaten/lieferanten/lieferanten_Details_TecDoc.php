<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LIN', '%');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();

    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht616 = $AWISBenutzer->HatDasRecht(616);            // Recht f�r die TecDocNren

    if (isset($_GET['LINKEY'])) {
        $AWIS_KEY2 = $_GET['LINKEY'];
    }

    if ($Recht616 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $FeldBreiten['NeuEdit'] = 40;
    $FeldBreiten['TecDocNr'] = 100;
    $FeldBreiten['USER'] = 150;
    $FeldBreiten['USERDAT'] = 150;

    $SQL = "SELECT *";
    $SQL .= ' FROM Informationstypen';
    $SQL .= ' WHERE ITY_KEY = 317';
    $SQL .= ' AND ITY_Status = \'A\'';
    $SQL .= ' ORDER BY ITY_Sortierung';

    $rsITY = $DB->RecordSetOeffnen($SQL);

    $SQL = 'SELECT *';
    $SQL .= ' FROM LieferantenInfos ';
    $SQL .= ' WHERE LIN_ITY_KEY = 317';
    $SQL .= ' AND LIN_LIE_NR = ' . $AWIS_KEY1;

    if (isset($_GET['LINKEY'])) {
        $SQL .= ' AND LIN_KEY = ' . $DB->FeldInhaltFormat('N0', $_GET['LINKEY'], false);
    }

    if (isset($_GET['SortLNo'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SortLNo']);
    } else {
        $SQL .= ' ORDER BY LIN_USERDAT DESC';
    }

    $Form->DebugAusgabe(1, $SQL);

    $rsLIN = $DB->RecordSetOeffnen($SQL);

    // Liste anzeigen
    if ($rsLIN->AnzahlDatensaetze() > 1 or isset($_GET['Liste']) or !isset($_GET['LINKEY']))                    // Liste anzeigen
    {
        $Form->Formular_Start();
        $Form->ZeileStart();

        $Icons = array();
        if (($Recht616 & 4) != 0) {
            $Icons[] = array('new', './lieferanten_Main.php?cmdAktion=Details&Seite=TecDoc&LINKEY=0');
        }
        $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['NeuEdit'], -2);
        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=TecDocNr';
        $Sort = '&SortLNo=LIN_WERT' . ((isset($_GET['SortLNo']) AND ($_GET['SortLNo'] == 'LIN_WERT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIN']['LIN_TecDocNr'], $FeldBreiten['TecDocNr'], '', $Link . $Sort);
        $Sort = '&SortLNo=LIN_USER' . ((isset($_GET['SortLNo']) AND ($_GET['SortLNo'] == 'LIN_USER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIN']['LIN_USER'], $FeldBreiten['USER'], '', $Link . $Sort);
        $Sort = '&SortLNo=LIN_USERDAT' . ((isset($_GET['SortLNo']) AND ($_GET['SortLNo'] == 'LIN_USERDAT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIN']['LIN_USERDAT'], $FeldBreiten['USERDAT'], '', $Link);

        $Form->ZeileEnde();

        $DS = 0;

        while (!$rsLIN->EOF()) {
            $Form->ZeileStart();
            $Icons = array();

            // Daten �ndern
            if (($Recht616 & 2) != 0) {
                $Icons[] = array('edit', './lieferanten_Main.php?cmdAktion=Details&Seite=TecDoc&LINKEY=' . $rsLIN->FeldInhalt('LIN_KEY'));
            }
            // Daten l�schen
            if (($Recht616 & 8) == 8) {
                $Icons[] = array(
                    'delete',
                    './lieferanten_Main.php?cmdAktion=Details&Seite=TecDoc&LINKEY=' . $rsLIN->FeldInhalt('LIN_KEY') . '&Del=' . $rsLIN->FeldInhalt('LIN_KEY')
                );
            }
            $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['NeuEdit'], ($DS % 2));

            $Form->Erstelle_ListenFeld('*LIN_WERT', $rsLIN->FeldInhalt('LIN_WERT'), 10, $FeldBreiten['TecDocNr'], false, ($DS % 2), '', '', 'T', 'L',
                $rsLIN->FeldInhalt('LIN_WERT'));
            $Form->Erstelle_ListenFeld('*LIN_USER', $rsLIN->FeldInhalt('LIN_USER'), 10, $FeldBreiten['USER'], false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('*LIN_USERDAT', $rsLIN->FeldInhalt('LIN_USERDAT'), 10, $FeldBreiten['USERDAT'], false, ($DS % 2), '', '', 'D');

            $Form->ZeileEnde();
            $DS++;

            $rsLIN->DSWeiter();
        }
    } else        // Einer oder keiner (= Neu)
    {
        $Form->Formular_Start();
        $AWIS_KEY2 = ($rsLIN->FeldInhalt('LIN_KEY') != ''?$rsLIN->FeldInhalt('LIN_KEY'):'0');

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./lieferanten_Main.php?cmdAktion=Details&Seite=TecDoc accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLIN->FeldInhalt('LIN_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLIN->FeldInhalt('LIN_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();

        $Form->DebugAusgabe(1, $AWIS_KEY2);

        $EditModus = (($Recht616 & 2) == 2);

        $ItyName = 'LIN_WERT' . '_' . $rsITY->FeldInhalt('ITY_KEY') . '_' . $rsLIN->FeldInhalt('LIN_KEY') . '_' . $rsITY->FeldInhalt('ITY_FORMAT');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIN']['LIN_TecDocNr'] . ':', 120);
        $Form->Erstelle_TextFeld($ItyName, $rsLIN->FeldInhalt('LIN_WERT'), $rsITY->FeldInhalt('ITY_ZEICHEN'), $rsITY->FeldInhalt('ITY_BREITE'), $EditModus);
        $AWISCursorPosition = ($EditModus?'txt' . $ItyName:$AWISCursorPosition);
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060008");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812061223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>