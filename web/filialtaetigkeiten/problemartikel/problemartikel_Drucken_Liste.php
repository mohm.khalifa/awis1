<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();


	$Vorlagen = array('BriefpapierATU_DE_Seite_2.pdf');
	$Form = new awisFormular();

	$RechteStufe = $AWISBenutzer->HatDasRecht(6000);
	if($RechteStufe==0)
	{
		$Form->DebugAusgabe(1,$AWISBenutzer->BenutzerName());
		$Form->Fehler_KeineRechte();
	}

	$TextKonserven=array();
	$TextKonserven[]=array('PRA','PRA%');
	$TextKonserven[]=array('PRA','tit_AusdruckUebersicht');
	$TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	$TextKonserven[]=array('PAB','PAB_ISTBESTAND_kurz');
	$TextKonserven[]=array('FIL','wrdFiliale');
	$TextKonserven[]=array('Wort','wrd_Stand');
	$TextKonserven[]=array('Wort','Summe');
	$TextKonserven[]=array('Wort','AlleFilialen');
	$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,$AWISSprachKonserven['PRA']['tit_AusdruckUebersicht']);

	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	if($FilZugriff=='' AND isset($_GET['FIL_ID']))
	{
		$FilZugriff=$DB->FeldInhaltFormat('N0',$_GET['FIL_ID']);
	}
	$FilZugriffListe=explode(',',$FilZugriff);


	$SQL = 'SELECT DISTINCT PRA_KEY, PRA_AST_ATUNR, AST_BEZEICHNUNGWW, SUM(COALESCE(FIB_BESTAND,0)) OVER(PARTITION BY PRA_KEY) AS FIB_BESTAND';
	$SQL .= ' FROM problemartikel';
	$SQL .= ' INNER JOIN artikelstamm ON PRA_AST_ATUNR = AST_ATUNR';
	$SQL .= ' LEFT OUTER JOIN FilialBestand ON PRA_AST_ATUNR = FIB_AST_ATUNR';
	if($FilZugriff!='')
	{
		$SQL .= '    AND FIB_FIL_ID IN('.$FilZugriff.')';
	}
	$SQL .= ' WHERE PRA_STATUS = \'A\'';
	$SQL .= ' AND PRA_KATEGORIE = 0'.$DB->FeldInhaltFormat('N0',$_GET['KAT'],false);
	$SQL .= ' ORDER BY AST_BEZEICHNUNGWW';
	$rsPRA = $DB->RecordSetOeffnen($SQL);

	/*************************************
	* PDF aufbauen
	*************************************/

	$RasterZeile=9999;
	$Summe=0;

	if($rsPRA->EOF())
	{
		$Ausdruck->NeueSeite(0,1);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_KeineDatenGefunden'],0,0,'C',0);
	}

	while(!$rsPRA->EOF())
	{
		if($RasterZeile > 250)
		{
			$Ausdruck->NeueSeite(0,1);

			$Ausdruck->_pdf->setXY(10,10);
			$Ausdruck->_pdf->SetFont('Arial','',20);
			$Ueberschrift = $AWISSprachKonserven['PRA']['tit_AusdruckUebersicht'];
			if($FilZugriff!='')
			{
				$Ueberschrift .= ' - ' . $AWISSprachKonserven['FIL']['wrdFiliale'].' '.$FilZugriff;
			}
			else
			{
				$Ueberschrift .= ' - '.$AWISSprachKonserven['Wort']['AlleFilialen'];
			}
			$Ausdruck->_pdf->cell(170,6,$Ueberschrift,0,0,'L',0);

			$Ausdruck->_pdf->setXY(10,18);
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] .' '. date('d.m.Y'),0,0,'L',0);

			$Ausdruck->_pdf->setXY(10,280);
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(190,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);


			$RasterZeile = 40;
			$Ausdruck->_pdf->setXY(10,$RasterZeile);
			$Ausdruck->_pdf->SetFillColor(10,10,10);
			$Ausdruck->_pdf->SetTextColor(255,255,255);
			$Ausdruck->_pdf->cell(30,5,$AWISSprachKonserven['PRA']['PRA_AST_ATUNR'],1,0,'L',1);
			$Ausdruck->_pdf->cell(130,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);
			$Ausdruck->_pdf->cell(30,5,$AWISSprachKonserven['PAB']['PAB_ISTBESTAND_kurz'],1,0,'L',1);
			$RasterZeile+=5;
			$Ausdruck->_pdf->SetTextColor(0,0,0);

		}

				// Eintr�ge schreiben
		$Ausdruck->_pdf->SetFont('Arial','',14);
		$Ausdruck->_pdf->SetDrawColor(0,0,0);
		$SQL =' SELECT SUM(PAB_ISTBESTAND) AS PAB_ISTBESTAND FROM (';
		$SQL .= ' SELECT  PAB_ISTBESTAND, PAB_PRA_KEY';
		$SQL .= ' , ROW_NUMBER() OVER (PARTITION BY PAB_FIL_ID ORDER BY PAB_DATUM DESC) AS ZeilenNr';
		$SQL .= ' FROM ProblemArtikelBestand';
		$SQL .= ' WHERE PAB_PRA_KEY = 0'.$rsPRA->FeldInhalt('PRA_KEY');
		if($FilZugriff!='')
		{
			$SQL .= ' AND PAB_FIL_ID IN ('.$FilZugriff.')';
		}
		$SQL .= ') DATEN WHERE ZeilenNr = 1';
		$rsPAB = $DB->RecordSetOeffnen($SQL);
																															// oder wenn filbest nix -> erledigt
		//if(!$rsPAB->EOF() AND (($rsPAB->FeldInhalt('PAB_ISTBESTAND')>0) OR ($rsPAB->FeldInhalt('PAB_ISTBESTAND')===''))) // AND $rsPRA->FeldInhalt('FIB_BESTAND')>0)))
		if(!$rsPAB->EOF() AND (($rsPAB->FeldInhalt('PAB_ISTBESTAND')>0) OR ($rsPAB->FeldInhalt('PAB_ISTBESTAND')==='' AND $rsPRA->FeldInhalt('FIB_BESTAND')>0)))
		{
			$Ausdruck->_pdf->setXY(10,$RasterZeile);

			$Ausdruck->_pdf->cell(30,8,$rsPRA->FeldInhalt('PRA_AST_ATUNR'),1,0,'L',0);
			$Ausdruck->_pdf->cell(130,8,$rsPRA->FeldInhalt('AST_BEZEICHNUNGWW'),1,0,'L',0);
			if($FilZugriff!='')
			{
				$Ausdruck->_pdf->cell(10,8,$Form->Format('N0',$rsPAB->FeldInhalt('PAB_ISTBESTAND')),1,0,'R',0);
				$Ausdruck->_pdf->cell(20,8,'',1,0,'L',0);
			}
			else
			{
				$Ausdruck->_pdf->cell(30,8,$Form->Format('N0T',$rsPAB->FeldInhalt('PAB_ISTBESTAND')),1,0,'R',0);

			}

			$Summe += $Form->Format('N0',$rsPAB->FeldInhalt('PAB_ISTBESTAND'),false);

			$RasterZeile+=8;
		}
		$rsPRA->DSWeiter();
	}

	if($FilZugriff=='')
	{
		$Ausdruck->_pdf->setXY(10+30,$RasterZeile);
		$Ausdruck->_pdf->cell(130,8,$AWISSprachKonserven['Wort']['Summe'].':',0,0,'R',0);
		$Ausdruck->_pdf->cell(30,8,$Form->Format('N0T',$Summe),0,0,'R',0);
	}

	/**************************************
	* Datei speichern
	**************************************/

	$Ausdruck->Anzeigen();
}
catch (awisException $ex)
{
	$Form->DebugAusgabe(1,$SQL);
	$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',1,200810231232);
}
catch (Exception $ex)
{

}
?>