<?php
global $con;
global $Recht3708;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CKE','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3708 = awisBenutzerRecht($con,3708,$AWISBenutzer->BenutzerName());
if($Recht3708==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
$Bedingung = '';

//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdDSZurueck_x']))
{
	$SQL = 'SELECT CKE_KEY FROM (SELECT CKE_KEY FROM CRMKontaktergebnisse';
	$SQL .= ' WHERE  UPPER(CKE_BEZEICHNUNG) < '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtCKE_BEZEICHNUNG']));
	$SQL .= ' ORDER BY CKE_BEZEICHNUNG DESC';	
	$SQL .= ') WHERE ROWNUM = 1';
	
	$rsCKE = awisOpenRecordset($con, $SQL);
	if(isset($rsCKE['CKE_KEY'][0]))
	{
		$AWIS_KEY2=$rsCKE['CKE_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCKE_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$SQL = 'SELECT CKE_KEY FROM (SELECT CKE_KEY FROM CRMKontaktergebnisse';
	$SQL .= ' WHERE  UPPER(CKE_BEZEICHNUNG) > '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtCKE_BEZEICHNUNG']));
	$SQL .= ' ORDER BY CKE_BEZEICHNUNG ASC';	
	$SQL .= ') WHERE ROWNUM = 1';
	$rsCKE = awisOpenRecordset($con, $SQL);

	if(isset($rsCKE['CKE_KEY'][0]))
	{
		$AWIS_KEY2=$rsCKE['CKE_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCKE_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./crm_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./crm_speichern_stammdaten.php');
}
elseif(isset($_GET['CKE_KEY']))
{
	$Bedingung .= ' AND CKE_KEY='.awis_FeldInhaltFormat('Z',$_GET['CKE_KEY']).'';
}
elseif(isset($_POST['txtCKE_KEY']))
{
	$Bedingung .= ' AND CKE_KEY='.awis_FeldInhaltFormat('Z',$_POST['CKE_KEY']).'';
}

if($AWIS_KEY2>0)
{
	$Bedingung .= ' AND CKE_KEY='.awis_FeldInhaltFormat('Z',$AWIS_KEY2).'';
}

//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM CRMKontaktergebnisse';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY CKE_BEZEICHNUNG';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

$rsCKE = awisOpenRecordset($con, $SQL);
$rsCKEZeilen = $awisRSZeilen;
awis_Debug(1,$_POST,$_GET, $SQL);
//********************************************************
// Daten anzeigen
//********************************************************
if($rsCKEZeilen==0 AND !isset($_GET['CKE_KEY']))		// Keine Meldung bei neuen Datensätzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden CRMKontaktergebnisse gefunden.</span>';
}
elseif($rsCKEZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	if(($Recht3708&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktergebnisse&CKE_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktergebnisse'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CKE_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CKE_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKE']['CKE_BEZEICHNUNG'],450,'',$Link);
	awis_FORM_ZeileEnde();

	for($CKEZeile=0;$CKEZeile<$rsCKEZeilen;$CKEZeile++)
	{
		awis_FORM_ZeileStart();
		
		$Icons = array();
		if(($Recht3708&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktergebnisse&CKE_KEY='.$rsCKE['CKE_KEY'][$CKEZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktergebnisse&Del='.$rsCKE['CKE_KEY'][$CKEZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CKEZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('CKE_BEZEICHNUNG',$rsCKE['CKE_BEZEICHNUNG'][$CKEZeile],0,450,false,($CKEZeile%2));
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmCRMKontaktergebnisse action=./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktergebnisse method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsCKE['CKE_KEY'][0])?$rsCKE['CKE_KEY'][0]:0);

	echo '<input type=hidden name=txtCKE_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktergebnisse&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCKE['CKE_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCKE['CKE_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$TabellenKuerzel='CKE';
	$EditRecht=(($Recht3708&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKE']['CKE_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('CKE_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsCKE['CKE_BEZEICHNUNG'][0]),50,350,$EditRecht);
	$CursorFeld='txtCKE_BEZEICHNUNG';
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();


	if(!isset($_POST['cmdDSNeu_x']))
	{
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Infos'));
		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';

		awis_RegisterErstellen(3701, $con, $RegisterSeite);
	}
	
	//***************************************
	// Schaltflächen für dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3708&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>