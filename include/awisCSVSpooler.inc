<?php

/**
 * Class awisCSVSpooler
 *
 * Klasse zum exportieren gro�er Datenmengen. Es wird nicht awisRecordset verwendet, da man ansonsten den PHP-Speicher sprengt.
 */
class awisCSVSpooler
{

    /**
     * @var string Trennzeichen
     */
    private $_Trennzeichen = ';';

    /**
     * @var awisDatenbank
     */
    public $DB;

    /**
     * @var LetzterSQL zum debuggen
     */
    private $_LetzterSQL;

    /**
     * @var string Linefeed
     */
    private $_LF = PHP_EOL;

    /**
     * @var string Textbegrenzungszeichen
     */
    private $_Textbegrenzer = '';

    /**
     * awisCSVSpooler constructor.
     *
     * @param awisDatenbank $DB
     */
    function __construct(awisDatenbank $DB)
    {
        $this->DB = $DB;
    }

    /**
     * @param               $SQL
     * @param               $ExportDatei mit Pfad
     * @param array         $Bindevariablen
     * @param array/bool    $Ueberschriften true/false oder array mit eigenen �berschriften
     * @param bool          $Ueberschreiben
     *
     * @throws Exception
     */
    public function StarteExport($SQL,$ExportDatei,$Bindevariablen=[],$Ueberschriften=true, $Ueberschreiben = false){

        if(!$Ueberschreiben and is_file($ExportDatei)){
            throw new Exception('Datei schon vorhanden','201909051402');
        }elseif ($Ueberschreiben and is_file($ExportDatei)){
            unlink($ExportDatei);
        }

        $Datei = fopen($ExportDatei,'w');

        $cmd = OCI_Parse($this->DB->getCon(), $SQL);

        //$Wert kann nicht verwendet werden, da oci_bind_by_name den Wert als Referenz �bergeben bekommt.
        foreach ($Bindevariablen as $Name => $Wert){
            $Format = explode('_',$Name)[1];

            switch ($Format) {
                case awisDatenbank::VAR_TYP_FLOAT :
                    $Erg = oci_bind_by_name($cmd, $Name, $Bindevariablen[$Name], -1, SQLT_FLT);
                    break;
                case awisDatenbank::VAR_TYP_GANZEZAHL:
                    $Erg = oci_bind_by_name($cmd, $Name, $Bindevariablen[$Name], -1, SQLT_INT);
                    break;
                case awisDatenbank::VAR_TYP_TEXT:
                    $Erg = oci_bind_by_name($cmd, $Name, $Bindevariablen[$Name], -1, SQLT_CHR);
                    break;
                case awisDatenbank::VAR_TYP_DATUM :
                case awisDatenbank::VAR_TYP_DATUMZEIT :
                    $Erg = oci_bind_by_name($cmd, $Name, $Bindevariablen[$Name]);
                    break;
                case awisDatenbank::VAR_TYP_DIREKT:
                default:
                    $Erg = oci_bind_by_name($cmd, $Name, $Bindevariablen[$Name]);
            }

            $SQL = str_replace(':' . $Name, (is_null($Wert)?'null':$Wert), $SQL);

            if(!$Erg){
                throw new Exception('Fehler beim Bindevariablenersetzen im CSVSpooler','20190905130212');
            }
        }

        $this->_LetzterSQL = $SQL;

        if (@OCIExecute($cmd) === true) {

            if($Ueberschriften === true){
                $AnzSpalten = oci_num_fields($cmd);
                $Header = '';
                for ($i = 1; $i <= $AnzSpalten; $i++) {
                    $Header  .= $this->_Textbegrenzer . oci_field_name($cmd, $i) . $this->_Textbegrenzer . $this->_Trennzeichen;
                }
                fwrite($Datei,$Header . $this->_LF);

            }elseif(is_array($Ueberschriften)){
                $Header = '';
                foreach ($Ueberschriften as $Ueber){
                    $Header .= $this->_Textbegrenzer . $Ueber . $this->_Textbegrenzer . $this->_Trennzeichen;
                }
                fwrite($Datei,$Header.$this->_LF);
            }

            while($DS = oci_fetch_assoc($cmd)){
                $Zeile = '';
                foreach ($DS as $Key => $Val){
                    $Zeile .= str_replace([$this->_Trennzeichen,chr(10),chr(13),$this->_Textbegrenzer],' ', $Val) . $this->_Trennzeichen;
                }

                //Letztes Trennzeichen wieder weg
                $Zeile = substr($Zeile,0,strlen($Zeile)-strlen($this->_Trennzeichen)) . $this->_LF;

                fwrite($Datei,$Zeile);
            }
            fclose($Datei);
        }else{
            throw new Exception('CSVSpooler: ' . oci_error($cmd)['message'],'201909051323');
        }

        OCIFreeStatement($cmd);

    }

    /**
     * @return LetzterSQL
     */
    public function LetzterSQL(){
        return $this->_LetzterSQL;
    }

    /**
     * @param string $Trenner
     */
    public function Trennzeichen($Trenner=';'){
        $this->_Trennzeichen = $Trenner;
    }

    /**
     * @param $LF
     */
    public function LineFeed($LF){
        $this->_LF = $LF;
    }

    /**
     * @param $Zeichen
     */
    public function Textbegrenzungszeichen($Zeichen){
        $this->Textbegrenzer($Zeichen);
    }


}