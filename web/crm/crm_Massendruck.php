<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CRM','wrd_ZusatzText%');
$TextKonserven[]=array('CRM','wrd_AktionSchreiben');
$TextKonserven[]=array('CAD','txt_SucheBetreuer%');
$TextKonserven[]=array('CAD','CAD_LAN_CODE');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','txt_Konditionsmodell');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Liste','CKD_KONDITIONSBLATT_REIFENGRUPPE');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3714 = awisBenutzerRecht($con,3714,$AWISBenutzer->BenutzerName());
if($Recht3714==0)
{
    awisEreignis(3,1000,'CRM-Massendruck',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


//********************************************************************
//* Daten speichern
//********************************************************************
$Felder = explode(';',awis_NameInArray($_POST,'txtXHW_',1));
if(sizeof($Felder)>1)
{
	foreach ($Felder as $Feld)
	{
		$Key = substr($Feld,7);

		if(strcmp(awis_FeldInhaltFormat('T',$_POST[$Feld]),awis_FeldInhaltFormat('T',$_POST['old'.substr($Feld,3)]))!=0)
		{

			$SQL = 'UPDATE Hilfswerte';
			$SQL .= ' SET XHW_WERTTXT='.awis_FeldInhaltFormat('T',$_POST[$Feld]);
			$SQL .= ' ,XHW_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ' ,XHW_USERDAT = SYSDATE';
			$SQL .= ' WHERE XHW_KEY = 0'.$Key;

			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('CRMKonditionen',1,'Fehler beim Speichern',$SQL);
			}
		}
	}

	$Param = array();
	$Param['KON_KEY']=$_POST['sucKON_KEY'];
	$Param['CKM_KEY']=$_POST['sucCKM_KEY'];
	$Param['LAN_CODE']=$_POST['sucLAN_CODE'];
	$Param['AktionSchreiben']=(isset($_POST['sucAktionSchreiben'])?$_POST['sucAktionSchreiben']:'');
	//Wegen Druck für sonstige Konditionsblätter
	//$Param['Sonstiges']=(isset($_POST['sucSonstiges'])?$_POST['sucSonstiges']:'');
	$Param['CKD_KONDITIONSBLATT_REIFENGRUPPE']=(isset($_POST['sucCKD_KONDITIONSBLATT_REIFENGRUPPE'])?$_POST['sucCKD_KONDITIONSBLATT_REIFENGRUPPE']:1);
	$Param['EMAIL']=(isset($_POST['sucEMAIL'])?$_POST['sucEMAIL']:'');


	awis_BenutzerParameterSpeichern($con, "Formular_CRM_Massendruck", $AWISBenutzer->BenutzerName() , $Param);
}

$Param = awis_BenutzerParameter($con, "Formular_CRM_Massendruck",$AWISBenutzer->BenutzerName());

echo '<form name=frmMassendruck method=post action=./crm_Main.php?cmdAktion=Massendruck>';

awis_FORM_FormularStart();
$Speichern = false;

if(($Recht3714&2)!=0)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('Konditionsblatt als PDF',200,'','./crm_DruckKonditionsblatt.php?Druckart=2');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('Konditionsblatt als CSV',200,'','./crm_DruckKonditionsblatt.php?Druckart=3');
	awis_FORM_ZeileEnde();

	awis_FORM_Trennzeile();

	$SQL = 'SELECT XHW_KEY, XHW_WERTTXT FROM Hilfswerte WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID=\'1\'';
	$rsText = awisOpenRecordset($con,$SQL);
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CRM']['wrd_ZusatzTextSommer'].':',250);
	awis_FORM_Erstelle_TextFeld('XHW_'.$rsText['XHW_KEY'][0],$rsText['XHW_WERTTXT'][0],30,300,true);
	awis_FORM_ZeileEnde();

	$SQL = 'SELECT XHW_KEY,XHW_WERTTXT FROM Hilfswerte WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID=\'2\'';
	$rsText = awisOpenRecordset($con,$SQL);
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CRM']['wrd_ZusatzTextWinter'].':',250);
	awis_FORM_Erstelle_TextFeld('XHW_'.$rsText['XHW_KEY'][0],$rsText['XHW_WERTTXT'][0],30,300,true);
	awis_FORM_ZeileEnde();


	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_LAN_CODE'].':',250);
	$SQL = 'select DISTINCT cad_lan_code, lan_land';
	$SQL .= ' FROM CRMAdressen';
	$SQL .= ' INNER JOIN laender ON lan_code = cad_lan_code';
	$SQL .= ' ORDER BY 2';
	awis_FORM_Erstelle_SelectFeld('*LAN_CODE',$Param['LAN_CODE'],250,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheBetreuer'].':',250);
	$SQL = "select DISTINCT * FROM (SELECT  kon_key, kon_name1 || ', ' || COALESCE(kon_name2,'') AS kontaktName";
	$SQL .= ' FROM kontakte';
	$SQL .= ' INNER JOIN benutzer B on kon_key = B.xbn_kon_key';// and xbn_status=\'A\'';
	$SQL .= ' INNER JOIN V_ACCOUNTRECHTE R on B.xbn_key = R.xbn_key';
	$SQL .= ' WHERE R.XRC_ID = 3704 AND BITAND(XBA_STUFE,16)=16 and kon_status=\'A\'';
	$SQL .= ' AND EXISTS(SELECT * FROM CRMADRESSEN ';
	$SQL .= ' INNER JOIN crmAktionen ON cad_key = cko_xxx_key AND cko_cat_key = 6';		// Aktion: Konditionsblätter
	$SQL .= ' WHERE CAD_KON_KEY = KON_KEY)';
	$SQL .= ' )';
	$SQL .= ' ORDER BY kontaktName';
	awis_FORM_Erstelle_SelectFeld('*KON_KEY',$Param['KON_KEY'],250,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_Konditionsmodell'].':',250);
	$SQL="select KEY2, Text ";
	$SQL.="from ( ";
	$SQL.="select KEY2, 'WG: ' || WG || ', DL: ' || DL || ', RH: ' || RH || ', Anzahl:' || ANZAHL as Text, Anzahl, 1 as Art ";
	$SQL.="FROM v_crm_konditionen ";
	$SQL.="WHERE ANZAHL > 50 ";
	$SQL.="UNION ";
	$SQL.="SELECT '999999999' as KEY2, 'Sonstige, Anzahl:' || SUM(ANZAHL) as Text, SUM(ANZAHL) as Anzahl, 2 as Art ";
	$SQL.="FROM v_crm_konditionen ";
	$SQL.="WHERE ANZAHL <= 50) ";
	$SQL.="order by Art asc, Anzahl desc ";
	//awis_Debug(1,$SQL);
	awis_FORM_Erstelle_SelectFeld('*CKM_KEY',$Param['CKM_KEY'],250,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	awis_FORM_ZeileEnde();

	awis_FORM_Trennzeile();

	/**/
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('Sonstiges:',250);
	$StatusText = explode("|",$AWISSprachKonserven['Liste']['CKD_KONDITIONSBLATT_REIFENGRUPPE']);
	awis_FORM_Erstelle_SelectFeld('*CKD_KONDITIONSBLATT_REIFENGRUPPE',(isset($Param['CKD_KONDITIONSBLATT_REIFENGRUPPE'])?$Param['CKD_KONDITIONSBLATT_REIFENGRUPPE']:''),130,true,$con,'','','A','','',$StatusText);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('E-Mail:',250);
	awis_FORM_Erstelle_TextFeld('*EMAIL',$Param['EMAIL'],50,500,true);
	awis_FORM_ZeileEnde();
	/**/

	awis_FORM_Trennzeile();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CRM']['wrd_AktionSchreiben'].':',250);
	awis_FORM_Erstelle_Checkbox('*AktionSchreiben',$Param['AktionSchreiben'],200,true,'on');
	awis_FORM_ZeileEnde();



	$Speichern = true;
}

if($Speichern==true)
{
	awis_FORM_Trennzeile();

	awis_FORM_ZeileStart();
	awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	awis_FORM_ZeileEnde();
}

awis_FORM_FormularEnde();
echo '</form>';
?>