<?php
/**
 * Verf�egbarkeiten verwalten
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20091008
 *
 *
 */
require_once('awisFilialen.inc');

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SRFunktionen;

// Standardwert f�r die Anwesenheit
$Anwesenheit['BRD']=array('weg'=>96,'da'=>115);

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$AWISBenutzer = awisBenutzer::Init();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KOV','%');
	$TextKonserven[]=array('Wort','Wochentag_%_Kurz');
	$TextKonserven[]=array('FEB','FEB_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','HinweisZuruecksetzen');
	$TextKonserven[]=array('Wort','FehlerNichtGefunden');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht11002=$AWISBenutzer->HatDasRecht(11002);		// Rechte des Mitarbeiters
	if($Recht11002==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SRVerfuegbarkeiten'));
	if(!isset($Param['FEB_KEY']))
	{
		$Param['FEB_KEY']='';
		$Param['KOV_WOCHE']=date('W');
		$Param['KOV_JAHR']=date('Y');
	}

	if(isset($_POST['sucFEB_KEY']))
	{
		$Param['FEB_KEY']=$_POST['sucFEB_KEY'];
		$Param['KOV_WOCHE']=$_POST['sucKOV_WOCHE'];
		$Param['KOV_JAHR']=$_POST['sucKOV_JAHR'];
	}
	
	
	if(isset($_POST['cmdSpeichern_x']))
	{
		include './srpep_speichern.php';
	}
	$Speichern = false;
$Form->DebugAusgabe(1,$_GET,$_POST);

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./srpep_Main.php?cmdAktion=Verfuegbarkeiten>");
	$Form->Formular_Start();

	// Auswahl des Gebiets, f�r das geplant werden soll

	if(($Recht11002&2)!=0)
	{
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
		$SQL .= ' FROM Filialebenen';
		$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
		$SQL .= ' AND EXISTS';
		$SQL .= '  (select DISTINCT XX1_FEB_KEY';
		$SQL .= '  FROM V_FILIALEBENENROLLEN';
		$SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate)';
		$SQL .= '  WHERE XX1_FER_KEY = 39';         // 39=SMART Repair Mitarbeiter
		$SQL .= '  AND  XX1_FEB_KEY = FEB_KEY)';
		$SQL .= ' ORDER BY 2';
	}
	elseif(($Recht11002&1)!=0)			// Nur die eigenen (GBL, TKDL, ...)
	{
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
		$SQL .= ' FROM Filialebenen';
		$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
		$SQL .= ' AND EXISTS';
		$SQL .= '  (select DISTINCT XX1_FEB_KEY';
		$SQL .= '  FROM V_FILIALEBENENROLLEN';
		$SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
		$SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate)';
		$SQL .= '  WHERE XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();
		$SQL .= '  AND  XX1_FEB_KEY = FEB_KEY';
		$SQL .= ')';
		$SQL .= ' ORDER BY 2';
	}

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FEB']['FEB_BEZEICHNUNG'].':',200);
	$Form->Erstelle_SelectFeld('*FEB_KEY',(isset($_POST['sucFEB_KEY'])?$_POST['sucFEB_KEY']:$Param['FEB_KEY']),100,true,$SQL);
	if(isset($Param['FEB_KEY']) AND $Param['FEB_KEY']>0)
	{
		// GBL f�r die Ebene anzeigen
		$SQL = 'SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, FER_BEZEICHNUNG';
		$SQL .= ' FROM Kontakte';
		$SQL .= ' INNER JOIN V_FILIALEBENENROLLEN ON XX1_KON_KEY = KON_KEY';
		$SQL .= ' INNER JOIN FILIALEBENENROLLEN ON XX1_FER_KEY = FER_KEY';
		$SQL .= ' INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate)';
		$SQL .= ' WHERE XX1_FER_KEY = 25';	// GBL 
		$SQL .= ' AND XX1_FEB_KEY = '.$Param['FEB_KEY'];
		$SQL .= ' ORDER BY KON_NAME1, KON_NAME2';
		$rsKON = $DB->RecordSetOeffnen($SQL);

		$Form->Erstelle_TextLabel($rsKON->FeldInhalt('FER_BEZEICHNUNG').': '.$rsKON->FeldInhalt('KON_NAME1').' '.$rsKON->FeldInhalt('KON_NAME2'),200);
		$Param['GBL']=$rsKON->FeldInhalt('KON_KEY');
	}
	$Form->ZeileEnde();


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KOV']['KOV_WOCHE'].':',200);
	$Daten = array();
	for($i=1;$i<=53;$i++)
	{
		$Daten[] = $i.'~'.$i;
	}
	$Form->Erstelle_SelectFeld('*KOV_WOCHE',(isset($_POST['sucKOV_WOCHE'])?$_POST['sucKOV_WOCHE']:$Param['KOV_WOCHE']),200,true,'','',date('W'),'','',$Daten);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KOV']['KOV_JAHR'].':',200);
	$Daten = array();
	for($i=2009;$i<=date('Y')+1;$i++)
	{
		$Daten[] = $i.'~'.$i;
	}
	$Form->Erstelle_SelectFeld('*KOV_JAHR',(isset($_POST['sucKOV_JAHR'])?$_POST['sucKOV_JAHR']:$Param['KOV_JAHR']),200,true,'','',date('Y'),'','',$Daten);
	$Form->ZeileEnde();

	// Details zur aktuellen Auswahl anzeigen
	if(isset($_POST['sucFEB_KEY']) OR isset($_GET['WTG_KEY']) OR isset($_GET['SListe']) OR isset($_GET['Del']))
	{
		$Form->Trennzeile();
		$DatumVom = date('d.m.Y',strtotime($Param['KOV_JAHR'].'-W'.str_pad($Param['KOV_WOCHE'],2,'0',STR_PAD_LEFT)));
		$DatumBis = date('d.m.Y',strtotime($Param['KOV_JAHR'].'-W'.str_pad(($Param['KOV_WOCHE']),2,'0',STR_PAD_LEFT).' +6 day'));

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Verf�gbarkeit '.$DatumVom.' - '.$DatumBis,600);
		$Form->ZeileEnde();

		$Form->Trennzeile('O');

		$SQL = 'SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2';
		$SQL .= ', COALESCE(PIF_WERT,\'90\') AS STAMMFILIALE';
		$SQL .= ' FROM Kontakte';
		$SQL .= ' INNER JOIN V_FILIALEBENENROLLEN ON XX1_KON_KEY = KON_KEY';
		$SQL .= ' INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS)  >= trunc(sysdate)'; //Nur die aktuell Zugeordneten anzeigen (TR 16.04.2010)
		$SQL .= ' LEFT OUTER JOIN PERSONALINFOS ON KON_PER_NR = PIF_PER_NR AND PIF_ITY_KEY = 11';	// Stammfiliale
		$SQL .= ' WHERE XX1_FER_KEY = 39';				// Smart Repair Mitarbeiter
		$SQL .= ' AND XX1_FEB_KEY = '.$Param['FEB_KEY'];
		$SQL .= ' ORDER BY KON_NAME1, KON_NAME2';
		$rsKON = $DB->RecordSetOeffnen($SQL);

		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KOV']['KOV_KON_KEY'],200);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_Montag_Kurz'],75);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_Dienstag_Kurz'],75);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_Mittwoch_Kurz'],75);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_Donnerstag_Kurz'],75);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_Freitag_Kurz'],75);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_Samstag_Kurz'],75);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Wochentag_Sonntag_Kurz'],75);
		$Form->ZeileEnde();

		$Land = 'BRD';		// Standardland

		// Auswahlliste f�r die Gebietszuordnung der Mitarbeiter als Array bauen
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG ';
		$SQL .= ' FROM Filialebenen ';
		$SQL .= ' WHERE (trunc(FEB_GUELTIGAB) <= :var_D_GUELITG_BIS AND trunc(FEB_GUELTIGBIS) >= :var_D_GUELITG_BIS ';
		$SQL .= ' OR trunc(FEB_GUELTIGAB) <= :var_D_GUELITG_AB AND trunc(FEB_GUELTIGBIS) >= :var_D_GUELITG_AB )';
		$SQL .= ' AND EXISTS (select DISTINCT XX1_FEB_KEY ';
		$SQL .= ' FROM V_FILIALEBENENROLLEN ';
		$SQL .= ' INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\' ';
		$SQL .= ' INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND FRZ_FER_KEY = 39 AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE) ';
		$SQL .= ' WHERE XX1_FEB_KEY = FEB_KEY) ';
		$SQL .= ' ORDER BY 2';
		$DB->SetzeBindevariable('FEB', 'var_D_GUELITG_AB', $DatumVom, awisDatenbank::VAR_TYP_DATUM);
		$DB->SetzeBindevariable('FEB', 'var_D_GUELITG_BIS', $DatumBis, awisDatenbank::VAR_TYP_DATUM);
		$rsFEB = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FEB'));
		$FEBDaten = array();
		$FEBDaten[] = '~- -';			// Standardwert ohne Ausleihe
		while(!$rsFEB->EOF())
		{
			$FEBDaten[] = $rsFEB->FeldInhalt('FEB_KEY').'~'.$rsFEB->FeldInhalt('FEB_BEZEICHNUNG');
			$rsFEB->DSWeiter();
		}
		
		while(!$rsKON->EOF())
		{
			$FILObj = new awisFilialen($DB->FeldInhaltFormat('N0',$rsKON->FeldInhalt('STAMMFILIALE'),true));
			$Land = $FILObj->FilialInfo('FIL_LAN_WWSKENN');

			$Feitertage = $FILObj->Feiertage($DatumVom,$DatumBis);
			foreach($Feitertage AS $Tag)
			{
				$Tage[date('w',$Form->PruefeDatum($Tag['Datum'],false,false,true))]=$Tag['Datum'].' - '.$Tag['Bezeichnung'];
			}

			$SQL = 'SELECT *';
			$SQL .= ' FROM KontakteVerfuegbarkeiten ';
			$SQL .= ' WHERE KOV_KON_KEY = '.$rsKON->FeldInhalt('KON_KEY');
			$SQL .= ' AND KOV_JAHR = '.$Param['KOV_JAHR'];
			$SQL .= ' AND KOV_WOCHE = '.$Param['KOV_WOCHE'];
			$SQL .= ' AND KOV_BEREICH = 1';			// Smart Repair Bereich
			$rsKOV = $DB->RecordSetOeffnen($SQL);
			if($rsKOV->EOF())
			{
				// Erst die Sondergebiete f�r den Mitarbeiter in der Woche ermitteln.
				$SQL = 'SELECT WTG_DATUMVOM, WTG_DATUMBIS, WTG_FEB_KEY';
				$SQL .= ' FROM WERKSTATTTERMINEAUSLEIHE';
				$SQL .= ' WHERE WTG_KON_KEY = :var_N0_WTG_KON_KEY';
				$SQL .= ' AND ((WTG_DATUMVOM <= :var_D_VOM AND WTG_DATUMBIS >= :var_D_VOM)';
				$SQL .= ' OR (WTG_DATUMVOM <= :var_D_BIS AND WTG_DATUMBIS >= :var_D_BIS)';
				$SQL .= ' OR (WTG_DATUMVOM >= :var_D_VOM AND WTG_DATUMBIS <= :var_D_BIS))';
				$SQL .= ' ORDER BY WTG_DATUMVOM';
				$DB->SetzeBindevariable('WTG', 'var_N0_WTG_KON_KEY', $rsKON->FeldInhalt('KON_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
				$DB->SetzeBindevariable('WTG', 'var_D_VOM', $DatumVom, awisDatenbank::VAR_TYP_DATUM);
				$DB->SetzeBindevariable('WTG', 'var_D_BIS', $DatumBis, awisDatenbank::VAR_TYP_DATUM);
				$rsWTG = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('WTG'));

				$Tage = array();
				while(!$rsWTG->EOF())
				{
					$StartTag = $Form->PruefeDatum($rsWTG->FeldInhalt('WTG_DATUMVOM'),false,false,true);
					if($StartTag < $Form->PruefeDatum($DatumVom,false,false,true))
					{
						$StartTag = $Form->PruefeDatum($DatumVom,false,false,true);
					}
					$EndTag = $Form->PruefeDatum($rsWTG->FeldInhalt('WTG_DATUMBIS'),false,false,true);
					if($EndTag > $Form->PruefeDatum($DatumBis,false,false,true))
					{
						$EndTag = $Form->PruefeDatum($DatumBis,false,false,true);
					}
					
					$StartTag = date('N',$StartTag);
					$EndTag = date('N',$EndTag);
					
					for($Tag = $StartTag; $Tag<=$EndTag; $Tag++)
					{
						$Tage[$Tag]=$rsWTG->FeldInhalt('WTG_FEB_KEY');
					}
					$rsWTG->DSWeiter();
				}


				$SQL = 'INSERT INTO KontakteVerfuegbarkeiten';
				$SQL .= '(KOV_KON_KEY, KOV_JAHR, KOV_WOCHE';
				$SQL .= ',KOV_PEK_KEY_MONTAG, KOV_PEK_KEY_DIENSTAG, KOV_PEK_KEY_MITTWOCH';
				$SQL .= ',KOV_PEK_KEY_DONNERSTAG,KOV_PEK_KEY_FREITAG,KOV_PEK_KEY_SAMSTAG,KOV_PEK_KEY_SONNTAG';
				$SQL .= ',KOV_FEB_KEY_MONTAG, KOV_FEB_KEY_DIENSTAG, KOV_FEB_KEY_MITTWOCH';
				$SQL .= ',KOV_FEB_KEY_DONNERSTAG,KOV_FEB_KEY_FREITAG,KOV_FEB_KEY_SAMSTAG,KOV_FEB_KEY_SONNTAG)';
				$SQL .= ' VALUES( ';
				$SQL .= ' '.$rsKON->FeldInhalt('KON_KEY');
				$SQL .= ','.$_POST['sucKOV_JAHR'];
				$SQL .= ','.$_POST['sucKOV_WOCHE'];
				$SQL .= ',\''.(isset($Tage[1])?$Anwesenheit[$Land]['weg']:$Anwesenheit[$Land]['da']).'\'';
				$SQL .= ',\''.(isset($Tage[2])?$Anwesenheit[$Land]['weg']:$Anwesenheit[$Land]['da']).'\'';
				$SQL .= ',\''.(isset($Tage[3])?$Anwesenheit[$Land]['weg']:$Anwesenheit[$Land]['da']).'\'';
				$SQL .= ',\''.(isset($Tage[4])?$Anwesenheit[$Land]['weg']:$Anwesenheit[$Land]['da']).'\'';
				$SQL .= ',\''.(isset($Tage[5])?$Anwesenheit[$Land]['weg']:$Anwesenheit[$Land]['da']).'\'';
				$SQL .= ',\''.$Anwesenheit[$Land]['weg'].'\'';	// Sa
				$SQL .= ',\''.$Anwesenheit[$Land]['weg'].'\'';	// So
				$SQL .= ','.(isset($Tage[1])?$DB->FeldInhaltFormat('N0',$Tage[1]):'null');	// Montag
				$SQL .= ','.(isset($Tage[2])?$DB->FeldInhaltFormat('N0',$Tage[2]):'null');
				$SQL .= ','.(isset($Tage[3])?$DB->FeldInhaltFormat('N0',$Tage[3]):'null');
				$SQL .= ','.(isset($Tage[4])?$DB->FeldInhaltFormat('N0',$Tage[4]):'null');
				$SQL .= ','.(isset($Tage[5])?$DB->FeldInhaltFormat('N0',$Tage[5]):'null');
				$SQL .= ','.(isset($Tage[6])?$DB->FeldInhaltFormat('N0',$Tage[6]):'null');
				$SQL .= ','.(isset($Tage[7])?$DB->FeldInhaltFormat('N0',$Tage[7]):'null');
				$SQL .= ')';
				if($DB->Ausfuehren($SQL)!==false)
				{
					$SQL = 'SELECT seq_KOV_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$rsKOV = $DB->RecordSetOeffnen('SELECT * FROM KontakteVerfuegbarkeiten WHERE KOV_KEY = '.$DB->WertSetzen('KOV', 'N0', $rsKey->FeldInhalt('KEY')),$DB->Bindevariablen('KOV'));
					
				}
			}
			$AWIS_KEY1 = $rsKOV->FeldInhalt('KOV_KEY');
			
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($rsKON->FeldInhalt('KON_NAME1').' ('.$rsKON->FeldInhalt('STAMMFILIALE').')',200,'','',$FILObj->FilialInfo('FIL_BEZ'));

			// Pro Mitarbeiter ermitteln, ob er an einem Tag schon verliehen wurde
			$SQL = 'SELECT COUNT(*) AS ANZ, WTP_TAG FROM WERKSTATTTERMINEPLANUNGEN ';
			$SQL .= ' WHERE WTP_KON_KEY = '.$DB->WertSetzen('WTP', 'N0', $rsKOV->FeldInhalt('KOV_KON_KEY'));
			$SQL .= ' AND WTP_AUSLEIHE = 1';
			$SQL .= ' AND TO_CHAR(WTP_TAG,\'IW-RRRR\') = '.$DB->WertSetzen('WTP', 'T', $Param['KOV_WOCHE'].'-'.$Param['KOV_JAHR']);
			$SQL .= ' GROUP BY WTP_TAG';
			$rsWTP = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('WTP'));
			$Ausleihe=array();
			while(!$rsWTP->EOF())
			{
				$Ausleihe[date('N',strtotime($rsWTP->FeldInhalt('WTP_TAG')))]=$rsWTP->FeldInhalt('ANZ');
				$rsWTP->DSWeiter();
			}

				// Liste mit allen sinnvollen K�rzeln
			$SQL = 'SELECT PEK_KEY, PEK_ID FROM PKUERZEL ';
			$SQL .= ' WHERE PEK_LAN_WWS_KENN = \''.$Land.'\' ';
			$SQL .= ' AND BITAND(PEK_VERWENDUNG,1)=1';
			$SQL .= ' ORDER BY PEK_TEXT';

			$Form->Erstelle_SelectFeld('KOV_PEK_KEY_MONTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_PEK_KEY_MONTAG'),'75:70',true,$SQL,'','','','','','',(isset($Tage[1])?$Tage[1]:''));
			$Form->Erstelle_SelectFeld('KOV_PEK_KEY_DIENSTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_PEK_KEY_DIENSTAG'),'75:70',true,$SQL,'','','','','','',(isset($Tage[2])?$Tage[2]:''));
			$Form->Erstelle_SelectFeld('KOV_PEK_KEY_MITTWOCH_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_PEK_KEY_MITTWOCH'),'75:70',true,$SQL,'','','','','','',(isset($Tage[3])?$Tage[3]:''));
			$Form->Erstelle_SelectFeld('KOV_PEK_KEY_DONNERSTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_PEK_KEY_DONNERSTAG'),'75:70',true,$SQL,'','','','','','',(isset($Tage[4])?$Tage[4]:''));
			$Form->Erstelle_SelectFeld('KOV_PEK_KEY_FREITAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_PEK_KEY_FREITAG'),'75:70',true,$SQL,'','','','','','',(isset($Tage[5])?$Tage[5]:''));
			$Form->Erstelle_SelectFeld('KOV_PEK_KEY_SAMSTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_PEK_KEY_SAMSTAG'),'75:70',true,$SQL,'','','','','','',(isset($Tage[6])?$Tage[6]:''));
			$Form->Erstelle_SelectFeld('KOV_PEK_KEY_SONNTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_PEK_KEY_SONNTAG'),'75:70',true,$SQL,'','','','','','',(isset($Tage[7])?$Tage[7]:''));
			$Form->ZeileEnde();

			// Gebiete, in denen der Mitarbeiter t�tig ist

				
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('Ausleihe:',200,'text-align:right;','','');
			$Form->Erstelle_SelectFeld('KOV_FEB_KEY_MONTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_FEB_KEY_MONTAG'),'75:70',!isset($Ausleihe[1]),null,'','','','',$FEBDaten,'',(isset($Ausleihe[1])?'Anzahl Auftr&auml;ge: '.$Ausleihe[1]:''));
			$Form->Erstelle_SelectFeld('KOV_FEB_KEY_DIENSTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_FEB_KEY_DIENSTAG'),'75:70',!isset($Ausleihe[2]),null,'','','','',$FEBDaten,'',(isset($Ausleihe[2])?'Anzahl Auftr&auml;ge: '.$Ausleihe[2]:''));
			$Form->Erstelle_SelectFeld('KOV_FEB_KEY_MITTWOCH_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_FEB_KEY_MITTWOCH'),'75:70',!isset($Ausleihe[3]),null,'','','','',$FEBDaten,'',(isset($Ausleihe[3])?'Anzahl Auftr&auml;ge: '.$Ausleihe[3]:''));
			$Form->Erstelle_SelectFeld('KOV_FEB_KEY_DONNERSTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_FEB_KEY_DONNERSTAG'),'75:70',!isset($Ausleihe[4]),null,'','','','',$FEBDaten,'',(isset($Ausleihe[4])?'Anzahl Auftr&auml;ge: '.$Ausleihe[4]:''));
			$Form->Erstelle_SelectFeld('KOV_FEB_KEY_FREITAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_FEB_KEY_FREITAG'),'75:70',!isset($Ausleihe[5]),null,'','','','',$FEBDaten,'',(isset($Ausleihe[5])?'Anzahl Auftr&auml;ge: '.$Ausleihe[5]:''));
			$Form->Erstelle_SelectFeld('KOV_FEB_KEY_SAMSTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_FEB_KEY_SAMSTAG'),'75:70',!isset($Ausleihe[6]),null,'','','','',$FEBDaten,'',(isset($Ausleihe[6])?'Anzahl Auftr&auml;ge: '.$Ausleihe[6]:''));
			$Form->Erstelle_SelectFeld('KOV_FEB_KEY_SONNTAG_'.$rsKOV->FeldInhalt('KOV_KEY'),$rsKOV->FeldInhalt('KOV_FEB_KEY_SONNTAG'),'75:70',!isset($Ausleihe[7]),null,'','','','',$FEBDaten,'',(isset($Ausleihe[7])?'Anzahl Auftr&auml;ge: '.$Ausleihe[7]:''));

				
			$Form->ZeileEnde();
			
			$Form->Trennzeile();
			
			$rsKON->DSWeiter();
		}
		
		//********************************************************************************************
		// Jetzt alle Mitarbeiter, die an das aktuelle Gebiet verliehen wurden
		// Teil 1: Kurzausleihe
		//********************************************************************************************
		
		$SQL = 'SELECT KON_NAME1';
		$SQL .= ',KOV_FEB_KEY_MONTAG,KOV_FEB_KEY_DIENSTAG,KOV_FEB_KEY_MITTWOCH,KOV_FEB_KEY_DONNERSTAG,KOV_FEB_KEY_FREITAG,KOV_FEB_KEY_SAMSTAG,KOV_FEB_KEY_SONNTAG';
		
		$SQL .= ', (SELECT PEK_ID FROM PKUERZEL WHERE PEK_KEY = KOV_PEK_KEY_MONTAG) AS PEK_MONTAG';
		$SQL .= ', (SELECT PEK_ID FROM PKUERZEL WHERE PEK_KEY = KOV_PEK_KEY_DIENSTAG) AS PEK_DIENSTAG';
		$SQL .= ', (SELECT PEK_ID FROM PKUERZEL WHERE PEK_KEY = KOV_PEK_KEY_MITTWOCH) AS PEK_MITTWOCH';
		$SQL .= ', (SELECT PEK_ID FROM PKUERZEL WHERE PEK_KEY = KOV_PEK_KEY_DONNERSTAG) AS PEK_DONNERSTAG';
		$SQL .= ', (SELECT PEK_ID FROM PKUERZEL WHERE PEK_KEY = KOV_PEK_KEY_FREITAG) AS PEK_FREITAG';
		$SQL .= ', (SELECT PEK_ID FROM PKUERZEL WHERE PEK_KEY = KOV_PEK_KEY_SAMSTAG) AS PEK_SAMSTAG';
		$SQL .= ', (SELECT PEK_ID FROM PKUERZEL WHERE PEK_KEY = KOV_PEK_KEY_SONNTAG) AS PEK_SONNTAG';

		$SQL .= ', COALESCE(PIF_WERT,\'90\') AS STAMMFILIALE';
		$SQL .= ' FROM KontakteVerfuegbarkeiten ';
		$SQL .= ' INNER JOIN Kontakte ON KOV_KON_KEY = KON_KEY ';
		$SQL .= ' LEFT OUTER JOIN PERSONALINFOS ON KON_PER_NR = PIF_PER_NR AND PIF_ITY_KEY = 11';	// Stammfiliale
		$SQL .= ' WHERE (KOV_FEB_KEY_MONTAG = '.$DB->WertSetzen('KOV', 'N0', $Param['FEB_KEY']);
		$SQL .= ' 	OR KOV_FEB_KEY_DIENSTAG = '.$DB->WertSetzen('KOV', 'N0', $Param['FEB_KEY']);
		$SQL .= ' 	OR KOV_FEB_KEY_MITTWOCH = '.$DB->WertSetzen('KOV', 'N0', $Param['FEB_KEY']);
		$SQL .= ' 	OR KOV_FEB_KEY_DONNERSTAG = '.$DB->WertSetzen('KOV', 'N0', $Param['FEB_KEY']);
		$SQL .= ' 	OR KOV_FEB_KEY_FREITAG = '.$DB->WertSetzen('KOV', 'N0', $Param['FEB_KEY']);
		$SQL .= ' 	OR KOV_FEB_KEY_SAMSTAG = '.$DB->WertSetzen('KOV', 'N0', $Param['FEB_KEY']);
		$SQL .= ' 	OR KOV_FEB_KEY_SONNTAG = '.$DB->WertSetzen('KOV', 'N0', $Param['FEB_KEY']);
		$SQL .= ' )AND KOV_JAHR = '.$DB->WertSetzen('KOV', 'N0', $Param['KOV_JAHR']);
		$SQL .= ' AND KOV_WOCHE = '.$DB->WertSetzen('KOV', 'N0', $Param['KOV_WOCHE']);
		$SQL .= ' AND KOV_BEREICH = 1';			// Smart Repair Bereich
		$rsKOV = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('KOV'));
		
		if(!$rsKOV->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('In diesem Gebiet verf�gbare Mitarbeiter', 800);
			$Form->ZeileEnde();
			$Form->Trennzeile();
		}
		
		while(!$rsKOV->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($rsKOV->FeldInhalt('KON_NAME1').' ('.$rsKOV->FeldInhalt('STAMMFILIALE').')',200);
				
			$Form->Erstelle_TextLabel(($rsKOV->FeldInhalt('KOV_FEB_KEY_MONTAG')==$Param['FEB_KEY']?$rsKOV->FeldInhalt('PEK_MONTAG'):'- -'), 75,'text-align:center');
			$Form->Erstelle_TextLabel(($rsKOV->FeldInhalt('KOV_FEB_KEY_DIENSTAG')==$Param['FEB_KEY']?$rsKOV->FeldInhalt('PEK_DIENSTAG'):'- -'), 75,'text-align:center');
			$Form->Erstelle_TextLabel(($rsKOV->FeldInhalt('KOV_FEB_KEY_MITTWOCH')==$Param['FEB_KEY']?$rsKOV->FeldInhalt('PEK_MITTWOCH'):'- -'), 75,'text-align:center');
			$Form->Erstelle_TextLabel(($rsKOV->FeldInhalt('KOV_FEB_KEY_DONNERSTAG')==$Param['FEB_KEY']?$rsKOV->FeldInhalt('PEK_DONNERSTAG'):'- -'), 75,'text-align:center');
			$Form->Erstelle_TextLabel(($rsKOV->FeldInhalt('KOV_FEB_KEY_FREITAG')==$Param['FEB_KEY']?$rsKOV->FeldInhalt('PEK_FREITAG'):'- -'), 75,'text-align:center');
			$Form->Erstelle_TextLabel(($rsKOV->FeldInhalt('KOV_FEB_KEY_SAMSTAG')==$Param['FEB_KEY']?$rsKOV->FeldInhalt('PEK_SAMSTAG'):'- -'), 75,'text-align:center');
			$Form->Erstelle_TextLabel(($rsKOV->FeldInhalt('KOV_FEB_KEY_SONNTAG')==$Param['FEB_KEY']?$rsKOV->FeldInhalt('PEK_SONNTAG'):'- -'), 75,'text-align:center');

			$Form->ZeileEnde();
			
			$rsKOV->DSWeiter();
		}		
		
/*
		//
		// Legende zeigen?
		//
		$Form->Trennzeile('O');

		$SQL = 'SELECT PEK_ID, PEK_TEXT FROM PKUERZEL ';
		$SQL .= ' WHERE PEK_LAN_WWS_KENN = \'BRD\'';
		$SQL .= ' AND BITAND(PEK_VERWENDUNG,1)=1';
		$SQL .= ' ORDER BY PEK_KEY';
		$rsPEK = $DB->RecordSetOeffnen($SQL);
		while(!$rsPEK->EOF())
		{
			$Form->ZeileStart('font-size:smaller');
			$Form->Erstelle_ListenFeld('A',$rsPEK->FeldInhalt('PEK_ID'),0,50,false);
			$Form->Erstelle_ListenFeld('A',$rsPEK->FeldInhalt('PEK_TEXT'),0,350,false);
			$Form->ZeileEnde();
			$rsPEK->DSWeiter();

		}
		*/
	
		//Pr�fen, ob Mitarbeiter zu der ausgew�hlten Filiale GBL ist --> Wenn ja, dann Speichernbutton anzeigen
		$Speichern = false;
		$SQL = 'SELECT FRZ_XXX_KEY FROM FILIALEBENENROLLENZUORDNUNGEN WHERE FRZ_FER_KEY = 25 ';
		$SQL.= ' AND FRZ_XTN_KUERZEL = \'FEB\' AND FRZ_XXX_KEY = '.$Param['FEB_KEY'];
		$SQL.= ' AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate)';
		$SQL.= ' AND FRZ_KON_KEY = '.$DB->FeldInhaltFormat('Z',$AWISBenutzer->BenutzerKontaktKEY());

		$rsFRZ = $DB->RecordSetOeffnen($SQL);
		
		if ($rsFRZ->AnzahlDatensaetze()>0)
		{
			$Speichern = true;
		}

		//**************************************************************************************************
		//* Langzeitausleihe
		//**************************************************************************************************
		// Parameter werden von der Folgeseite gelesen
		$Form->Formular_Ende();
		
		$AWISBenutzer->ParameterSchreiben('Formular_SRVerfuegbarkeiten', serialize($Param));
		
		$Reg = new awisRegister(11010);
		$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
	}
	else
	{
		$Form->Formular_Ende();
	}

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if(((($Recht11002&4)==4) AND isset($Param['FEB_KEY'])) or $Speichern == true)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200912171119");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200912171120");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>
