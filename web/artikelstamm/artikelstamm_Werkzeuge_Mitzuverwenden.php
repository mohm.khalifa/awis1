<?php

// Textkonserven laden
$TextKonserven = [];
$TextKonserven[] = ['Wort', 'lbl_speichern'];
$TextKonserven[] = ['Wort', 'lbl_suche'];
$TextKonserven[] = ['Wort', 'lbl_zurueck'];
$TextKonserven[] = ['Fehler', 'err_keineRechte'];
$TextKonserven[] = ['Fehler', 'err_keineDaten'];
$TextKonserven[] = ['AST', '%'];

try {
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht458 = $AWISBenutzer->HatDasRecht(458);        // Recht f�r Lieferantenartikel
    if (($Recht458 & 16) == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if (isset($_POST['cmdSpeichern_x'])) {
        $Felder = $Form->NameInArray($_POST, 'txtMITZUVERWENDEN_', 2, 1);

        foreach ($Felder as $Feld) {
            $ATUNRBase64 = explode('_', $Feld)[1];
            $ATUNr = base64_decode($ATUNRBase64);
            $Wert = $_POST[$Feld];

            if ($ATUNr != '') {
                $DB->TransaktionBegin();

                $SQL = 'delete from artikelstamminfos where asi_ait_id in (410,411) and asi_ast_atunr = ' . $DB->WertSetzen('ASI', 'T', $ATUNr);
                $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('ASI'));

                if($Wert == ''){
                    $DB->TransaktionCommit();
                    continue;
                }

                $Wert = str_replace(',', ';', $Wert);

                if (strpos($Wert, ';') !== false) {
                    $Mitzuverwenden = explode(';', $Wert);
                } else {
                    $Mitzuverwenden = [$Wert];
                }

                foreach ($Mitzuverwenden as $MZF) {
                    $SQL = 'INSERT INTO artikelstamminfos (';
                    $SQL .= '     asi_ast_atunr,';
                    $SQL .= '     asi_ait_id,';
                    $SQL .= '     asi_wert,';
                    $SQL .= '     asi_user,';
                    $SQL .= '     asi_imq_id,';
                    $SQL .= '     asi_userdat';
                    $SQL .= ' ) VALUES (';
                    $SQL .= $DB->WertSetzen('ASI', 'T', $ATUNr);
                    $SQL .= ', 410 ';
                    $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $MZF);
                    $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $AWISBenutzer->BenutzerName());
                    $SQL .= ',     4,';
                    $SQL .= '     sysdate';
                    $SQL .= ' )';

                    $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('ASI'));
                }

                $Hersteller = $_POST['txtHERST_MITZUVERWENDEN_'.$ATUNRBase64]!=''?$_POST['txtHERST_MITZUVERWENDEN_'.$ATUNRBase64]:'mitzuverwenden';

                $SQL = 'INSERT INTO artikelstamminfos (';
                $SQL .= '     asi_ast_atunr,';
                $SQL .= '     asi_ait_id,';
                $SQL .= '     asi_wert,';
                $SQL .= '     asi_user,';
                $SQL .= '     asi_imq_id,';
                $SQL .= '     asi_userdat';
                $SQL .= ' ) VALUES (';
                $SQL .= $DB->WertSetzen('ASI', 'T', $ATUNr);
                $SQL .= ', 411 ';
                $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $Hersteller);
                $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $AWISBenutzer->BenutzerName());
                $SQL .= ',     4,';
                $SQL .= '     sysdate';
                $SQL .= ' )';

                $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('ASI'));

                $DB->TransaktionCommit();
            }
        }

        $Felder = $Form->NameInArray($_POST, 'txtGEBRAUCHSNUMMER_', 2, 1);

        foreach ($Felder as $Feld) {
            $ATUNRBase64 = explode('_', $Feld)[1];
            $ATUNr = base64_decode($ATUNRBase64);
            $Wert = $_POST[$Feld];
            if ($ATUNr != '') {
                $DB->TransaktionBegin();

                $SQL = 'delete from artikelstamminfos where asi_ait_id in (420,421) and asi_ast_atunr = ' . $DB->WertSetzen('ASI', 'T', $ATUNr);
                $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('ASI'));
                $Wert = str_replace(',', ';', $Wert);

                if($Wert == ''){
                    continue;
                }

                if (strpos($Wert, ';') !== false) {
                    $Gebrauchsnummern = explode(';', $Wert);
                } else {
                    $Gebrauchsnummern = [$Wert];
                }

                foreach ($Gebrauchsnummern as $Gebrauchsnummer) {
                    $SQL = 'INSERT INTO artikelstamminfos (';
                    $SQL .= '     asi_ast_atunr,';
                    $SQL .= '     asi_ait_id,';
                    $SQL .= '     asi_wert,';
                    $SQL .= '     asi_user,';
                    $SQL .= '     asi_imq_id,';
                    $SQL .= '     asi_userdat';
                    $SQL .= ' ) VALUES (';
                    $SQL .= $DB->WertSetzen('ASI', 'T', $ATUNr);
                    $SQL .= ', 420 ';
                    $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $Gebrauchsnummer);
                    $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $AWISBenutzer->BenutzerName());
                    $SQL .= ',     4,';
                    $SQL .= '     sysdate';
                    $SQL .= ' )';

                    $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('ASI'));
                }

                $Hersteller = $_POST['txtHERST_GEBRAUCHSNUMMER_'.$ATUNRBase64]!=''?$_POST['txtHERST_GEBRAUCHSNUMMER_'.$ATUNRBase64]:'Gebrauchsnummer';

                $SQL = 'INSERT INTO artikelstamminfos (';
                $SQL .= '     asi_ast_atunr,';
                $SQL .= '     asi_ait_id,';
                $SQL .= '     asi_wert,';
                $SQL .= '     asi_user,';
                $SQL .= '     asi_imq_id,';
                $SQL .= '     asi_userdat';
                $SQL .= ' ) VALUES (';
                $SQL .= $DB->WertSetzen('ASI', 'T', $ATUNr);
                $SQL .= ', 421 ';
                $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $Hersteller);
                $SQL .= ', ' . $DB->WertSetzen('ASI', 'T', $AWISBenutzer->BenutzerName());
                $SQL .= ',     4,';
                $SQL .= '     sysdate';
                $SQL .= ' )';

                $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('ASI'));

                $DB->TransaktionCommit();
            }
        }
    }

    if (isset($_GET['Sort'])) {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
    } elseif (isset($Param['ORDERBY'])) {
        $ORDERBY = $Param['ORDERBY'];
    } else {
        $ORDERBY = ' ORDER BY AST_USERDAT DESC';
    }

    if (isset($_POST['sucAST_ATUNR'])) {
        $Param['AST_ATUNR'] = $_POST['sucAST_ATUNR'];
        $Param['Block'] = 1;
    } else {
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_453_4', true));
    }

    if (isset($_REQUEST['Block'])) {
        $Param['Block'] = $_REQUEST['Block'];
    }


    $SQL = 'SELECT ARTIKELSTAMM.* ';
    $SQL .= ', (select listagg(asi_wert,\';\') within group (order by asi_wert) from artikelstamminfos where asi_ast_atunr = ast_atunr  and ASI_AIT_ID = 410 ) as ASI_MITZUVERWENDEN ';
    $SQL .= ', (select listagg(asi_wert,\';\') within group (order by asi_wert) from artikelstamminfos where asi_ast_atunr = ast_atunr  and ASI_AIT_ID = 411 ) as ASI_MITZUVERWENDEN_HERSTELLER ';
    $SQL .= ', (select listagg(asi_wert,\';\') within group (order by asi_wert) from artikelstamminfos where asi_ast_atunr = ast_atunr  and ASI_AIT_ID = 420 ) as ASI_GEBRAUCHSNUMMER ';
    $SQL .= ', (select listagg(asi_wert,\';\') within group (order by asi_wert) from artikelstamminfos where asi_ast_atunr = ast_atunr  and ASI_AIT_ID = 421 ) as ASI_GEBRAUCHSNUMMER_HERSTELLER ';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM ARTIKELSTAMM ';

    if (isset($Param['AST_ATUNR']) and $Param['AST_ATUNR'] != '') {
        $SQL .= 'WHERE AST_ATUNR ' . $DB->LikeOderIst($Param['AST_ATUNR'], awisDatenbank::AWIS_LIKE_UPPER, 'AST');
    }else{
        $SQL .= ' WHERE AST_ATUNR IN ( ';
        $SQL .= ' SELECT distinct ASI_AST_ATUNR FROM ARTIKELSTAMMINFOS ';
        $SQL .= ' WHERE ASI_AIT_ID in (410,411,420,421)';
        $SQL .= ')';
    }


    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Param['Block'] - 1) * $ZeilenProSeite) + 1;

    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('AST', false));

    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);

    $Param['ORDERBY'] = $ORDERBY;

    $AWISBenutzer->ParameterSchreiben('Formular_453_4', serialize($Param));

    $rsAPR = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('AST', false));

    $Form->Formular_Start();

    $Form->SchreibeHTMLCode('<form name=frmMitzuverwendenSUC action=./artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Mitzuverwenden method=POST  enctype="multipart/form-data">');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ATUNR'] . ':', 100);
    $Form->Erstelle_TextFeld('*AST_ATUNR', $Param['AST_ATUNR'], 30, 300, true);
    $AWISCursorPosition = 'sucAST_ATUNR';
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Form->ZeileEnde();

    $Form->SchreibeHTMLCode('</form>');

    $Form->SchreibeHTMLCode('<form id=frmMitzuverwenden name=frmMitzuverwenden action=./artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Mitzuverwenden method=POST  enctype="multipart/form-data">');

    if ($rsAPR->EOF()) {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    } else {
        $Form->ZeileStart();
        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Mitzuverwenden';
        $Link .= '&Sort=AST_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_ATUNR')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_ATUNR'], 100, '', $Link);

        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Mitzuverwenden';
        $Link .= '&Sort=AST_BEZEICHNUNGWW' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_BEZEICHNUNGWW')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'], 450, '', $Link);

        $Link = '';
        $Form->Erstelle_Liste_Ueberschrift('', 20, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_MITZUVERWENDEN'], 160, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['ASI_MITZUVERWENDEN_HERSTELLER'], 200, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift('', 20, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['ASI_GEBRAUCHSNUMMER'], 160, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['ASI_GEBRAUCHSNUMMER_HERSTELLER'], 160, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsAPR->EOF()) {
            $Form->ZeileStart();
            $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=0' . $rsAPR->FeldInhalt('AST_KEY');
            $Form->Erstelle_ListenFeld('AST_ATUNR', $rsAPR->FeldInhalt('AST_ATUNR'), 10, 100, false, ($DS % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('AST_BEZEICHNUNGWW', $rsAPR->FeldInhalt('AST_BEZEICHNUNGWW'), 10, 450, false, ($DS % 2), '', '', 'T');
            $Icons = array(array( 'delete', ''));
            $Form->Erstelle_ListeIcons($Icons,16,($DS % 2),'','','loeschfeld="'.'txtMITZUVERWENDEN_' . base64_encode($rsAPR->FeldInhalt('AST_ATUNR')).'"' );
            $Form->Erstelle_ListenFeld('MITZUVERWENDEN_' . base64_encode($rsAPR->FeldInhalt('AST_ATUNR')), $rsAPR->FeldInhalt('ASI_MITZUVERWENDEN'), 20, 160, true, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('HERST_MITZUVERWENDEN_' . base64_encode($rsAPR->FeldInhalt('AST_ATUNR')), $rsAPR->FeldInhalt('ASI_MITZUVERWENDEN_HERSTELLER'), 20, 200, true, ($DS % 2), '', '', 'T');

            $Icons = array(array( 'delete', ''));
            $Form->Erstelle_ListeIcons($Icons,16,($DS % 2),'','','loeschfeld="'.'txtGEBRAUCHSNUMMER_' . base64_encode($rsAPR->FeldInhalt('AST_ATUNR')).'"' );
            $Form->Erstelle_ListenFeld('GEBRAUCHSNUMMER_' . base64_encode($rsAPR->FeldInhalt('AST_ATUNR')), $rsAPR->FeldInhalt('ASI_GEBRAUCHSNUMMER'), 20, 160, true, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('HERST_GEBRAUCHSNUMMER_' . base64_encode($rsAPR->FeldInhalt('AST_ATUNR')), $rsAPR->FeldInhalt('ASI_GEBRAUCHSNUMMER_HERSTELLER'), 20, 160, true, ($DS % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsAPR->DSWeiter();
            $DS++;
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=Mitzuverwenden';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Param['Block'], '');
    }

    ?>

    <script>
        $('.ListenIcon').on('click',function(){
          if($(this).attr('loeschfeld') != null){
              $('#'+$(this).attr('loeschfeld')).val('');
              $('#cmdSpeichern').click();
          }
        });

    </script>
    <?php

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060003");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "20081��61223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>