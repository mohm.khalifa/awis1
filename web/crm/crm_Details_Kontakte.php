<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('CKT','CKT_%');
$TextKonserven[]=array('CAP','CAP_NACHNAME');
$TextKonserven[]=array('CAP','CAP_VORNAME');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3704 = awisBenutzerRecht($con,3704,$AWISBenutzer->BenutzerName());
if($Recht3704==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


awis_FORM_FormularStart();

$EditRecht=(($Recht3704&6)!=0);

//***********************************
//Kontakte anzeigen
//***********************************

$SQL = 'SELECT CRMKontakte.*';
$SQL .= " , COALESCE(CAP_NACHNAME || ', ','::ausgeschieden::') || COALESCE(CAP_VORNAME,'') AS CAPNAME";
$SQL .= ' , CKA_BEZEICHNUNG, CKE_BEZEICHNUNG ';
//$SQL .= ' ,CAK_WERT AS MailAdresse';
$SQL .= " , KON_NAME1 || ', ' || COALESCE(KON_NAME2,'') AS KONNAME";
$SQL .= ' FROM CRMKontakte ';
$SQL .= ' INNER JOIN CRMKontaktArten ON CKA_KEY = CKT_CKA_KEY';
$SQL .= ' INNER JOIN CRMKontaktErgebnisse ON CKE_KEY = CKT_CKE_KEY';
$SQL .= ' LEFT OUTER JOIN CRMAnsprechpartner ON CAP_KEY = CKT_CAP_KEY';
//$SQL .= ' LEFT OUTER JOIN CRMAnsprechpartnerKontakte ON CAK_KOT_KEY = 7 AND CAK_CAP_KEY = CAP_KEY';
$SQL .= ' LEFT OUTER JOIN Kontakte ON KON_KEY = CKT_KON_KEY';
$SQL .= ' WHERE CKT_CAD_KEY=0'.$AWIS_KEY1;

if(isset($_GET['CKTKEY']))
{
	$SQL .= ' AND CKT_KEY=0'.intval($_GET['CKTKEY']);
}
if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
{
	$SQL .= ' AND CKT_KEY=0'.intval($AWIS_KEY2);
}

if(!isset($_GET['CKTSort']))
{
	$SQL .= ' ORDER BY CKT_DATUM DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CKTSort']);
}
$rsCKT = awisOpenRecordset($con,$SQL);
$rsCKTZeilen = $awisRSZeilen;
awis_Debug(1,$SQL);
if($rsCKTZeilen > 1 or isset($_GET['CKTListe']))					// Liste anzeigen
{
	awis_FORM_ZeileStart();

	if(($Recht3704&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Details&Seite=Kontakte&CKTKEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Kontakte';
	$Link .= '&CKTSort=CKT_DATUM'.((isset($_GET['CKTSort']) AND ($_GET['CKTSort']=='CKT_DATUM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKT']['CKT_DATUM'],100,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Kontakte';
	$Link .= '&CKTSort=CAPNAME'.((isset($_GET['CKTSort']) AND ($_GET['CKTSort']=='CAP_NAME1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKT']['CKT_CAP_KEY'],250,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Kontakte';
	$Link .= '&CKTSort=CKA_BEZEICHNUNG'.((isset($_GET['CKTSort']) AND ($_GET['CKTSort']=='CKA_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKT']['CKT_CKA_KEY'],200,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Kontakte';
	$Link .= '&CKTSort=CKE_BEZEICHNUNG'.((isset($_GET['CKTSort']) AND ($_GET['CKTSort']=='CKE_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKT']['CKT_CKE_KEY'],200,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Kontakte';
	$Link .= '&CKTSort=KONNAME'.((isset($_GET['CKTSort']) AND ($_GET['CKTSort']=='KONNAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKT']['CKT_KON_KEY'],200,'',$Link);


	awis_FORM_ZeileEnde();

	for($CKTZeile=0;$CKTZeile<$rsCKTZeilen;$CKTZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3704&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Details&Seite=Kontakte&CKTKEY='.$rsCKT['CKT_KEY'][$CKTZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Details&Seite=Kontakte&Del='.$rsCKT['CKT_KEY'][$CKTZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CKTZeile%2));

		awis_FORM_Erstelle_ListenFeld('#CKT_DATUM',$rsCKT['CKT_DATUM'][$CKTZeile],20,100,false,($CKTZeile%2),'','','T');
		$SQL = 'SELECT CAK_Wert FROM CRMAnsprechpartnerKontakte WHERE CAK_KOT_KEY = 7 AND CAK_CAP_KEY = '.awisFeldFormat('N0',$rsCKT['CKT_CAP_KEY'][$CKTZeile]);
		$rsCAK = awisOpenRecordset($con,$SQL);
		$Link = '';
		for($i=0;$i<$awisRSZeilen;$i++)
		{
			$Link .= ';'.$rsCAK['CAK_WERT'][$i];
		}
		$Link = ($Link != ''?'mailto:'.substr($Link,1):'');
		awis_FORM_Erstelle_ListenFeld('#CAPNAME',$rsCKT['CAPNAME'][$CKTZeile],20,250,false,($CKTZeile%2),'',$Link,'T');
		awis_FORM_Erstelle_ListenFeld('#CKA_BEZEICHNUNG',$rsCKT['CKA_BEZEICHNUNG'][$CKTZeile],20,200,false,($CKTZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CKE_BEZEICHNUNG',$rsCKT['CKE_BEZEICHNUNG'][$CKTZeile],20,200,false,($CKTZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#KONNAME',$rsCKT['KONNAME'][$CKTZeile],20,200,false,($CKTZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}
	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtCKT_KEY type=hidden value=0'.(isset($rsCKT['CKT_KEY'][0])?$rsCKT['CKT_KEY'][0]:'').'>';
	echo '<input name=txtCKT_CAD_KEY type=hidden value=0'.$AWIS_KEY1.'>';

	$AWIS_KEY2 = (isset($rsCKT['CKT_KEY'][0])?$rsCKT['CKT_KEY'][0]:'0');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Details&Seite=Kontakte&CKTListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsCKT['CKT_KEY'][0])?$rsCKT['CKT_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsCKT['CKT_KEY'][0])?$rsCKT['CKT_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKT']['CKT_DATUM'].'',150);
	awis_FORM_Erstelle_TextFeld('CKT_DATUM',(isset($rsCKT['CKT_DATUM'][0])?$rsCKT['CKT_DATUM'][0]:''),10,200,($Recht3704&6),'','','','D','','',awis_PruefeDatum(date('d.m.Y')));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKT']['CKT_KON_KEY'].'',150);
	$SQL = "select DISTINCT * FROM (SELECT  kon_key, kon_name1 || ', ' || COALESCE(kon_name2,'') AS kontaktName";
	$SQL .= ' FROM kontakte';
	$SQL .= ' INNER JOIN benutzer B on kon_key = B.xbn_kon_key';// and xbn_status=\'A\'';
	$SQL .= ' INNER JOIN V_ACCOUNTRECHTE R on B.xbn_key = R.xbn_key';
	$SQL .= ' WHERE (R.XRC_ID = 3704 AND BITAND(XBA_STUFE,16)=16 and kon_status=\'A\')';
	$SQL .= (isset($rsCKT['CKT_KON_KEY'][0])?' OR B.XBN_KEY = 0'.$rsCKT['CKT_KON_KEY'][0]:'');		// wenn er schon mal vergeben wurde, lassen
	$SQL .= ') ORDER BY kontaktname';
	awis_FORM_Erstelle_SelectFeld('CKT_KON_KEY',($AWIS_KEY2==0?'':$rsCKT['CKT_KON_KEY'][0]),200,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKT']['CKT_CKA_KEY'].'',150);
	$SQL = "select CKA_KEY, CKA_BEZEICHNUNG ";
	$SQL .= ' FROM CRMkontaktArten';
	$SQL .= ' ORDER BY CKA_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('CKT_CKA_KEY',($AWIS_KEY2==0?'':$rsCKT['CKT_CKA_KEY'][0]),200,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKT']['CKT_CAP_KEY'].'',150);
	$SQL = "select CAP_KEY, CAP_Nachname || ', ' || COALESCE(CAP_VORNAME,'') AS APName ";
	$SQL .= ' FROM CRMAnsprechpartner';
	$SQL .= ' WHERE CAP_CAD_KEY=0'.$AWIS_KEY1;
	$SQL .= ' ORDER BY CAP_NACHNAME, CAP_VORNAME';
	awis_FORM_Erstelle_SelectFeld('CKT_CAP_KEY',($AWIS_KEY2==0?'':$rsCKT['CKT_CAP_KEY'][0]),200,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKT']['CKT_CKE_KEY'].'',150);
	$SQL = "select CKE_KEY, CKE_BEZEICHNUNG ";
	$SQL .= ' FROM CRMkontaktErgebnisse';
	$SQL .= ' ORDER BY CKE_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('CKT_CKE_KEY',($AWIS_KEY2==0?'':$rsCKT['CKT_CKE_KEY'][0]),200,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKT']['CKT_BEMERKUNG'].'',150);
	awis_FORM_Erstelle_Textarea('CKT_BEMERKUNG',($AWIS_KEY2==0?'':$rsCKT['CKT_BEMERKUNG'][0]),500,100,5,$EditRecht,'');
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
}
?>