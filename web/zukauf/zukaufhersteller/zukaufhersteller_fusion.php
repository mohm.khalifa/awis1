<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_ZukaufHersteller');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new awisFormular();
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_ZukaufHersteller'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader.inc");	// Kopfzeile

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Form->Formular_Start();
	$Form->SchreibeHTMLCode('<form name=frmFusion action=./zukaufhersteller_fusion.php method=POST>');
	
	
	if(isset($_POST['cmdSpeichern_x']))
	{
		// Umsetzen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Beginne Umsetzung...',200,'Hinweis');
		$Form->ZeileEnde();
	
		$DB->TransaktionBegin();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Rechnungen.',200,'');
		$Form->ZeileEnde();
	
		$SQL = 'UPDATE Zukaufrechnungen';
		$SQL .= ' SET ZUR_ZLH_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZUR_ZLH_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Lieferscheine.',200,'');
		$Form->ZeileEnde();
	
		$SQL = 'UPDATE Zukauflieferscheine';
		$SQL .= ' SET ZUL_ZLH_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZUL_ZLH_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);
	
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Zukauflieferantenartikelarchiv.',200,'');
		$Form->ZeileEnde();
	
		$SQL = 'UPDATE ZUKAUFLIEFERANTENARTIKELARCHIV';
		$SQL .= ' SET ZAH_ZLH_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZAH_ZLH_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Zukaufherstellercodes.',200,'');
		$Form->ZeileEnde();
	
		$SQL = 'UPDATE Zukaufherstellercodes';
		$SQL .= ' SET ZHK_ZLH_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZHK_ZLH_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Zukaufartikel.',200,'');
		$Form->ZeileEnde();
	
		$SQL = 'UPDATE Zukaufartikel';
		$SQL .= ' SET ZLA_ZLH_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZLA_ZLH_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Hersteller l�schen.',200,'');
		$Form->ZeileEnde();
	
		$SQL = 'DELETE FROM Zukauflieferantenhersteller';
		$SQL .= ' WHERE ZLH_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);
	
		$DB->TransaktionCommit();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Umsetzung beendet...',200,'Hinweis');
		$Form->ZeileEnde();
	}
	
	$Form->ZeileStart();

	// Hier alle Hersteller, die mehrfach vorkommen k�nnen
	$Form->Erstelle_TextLabel('Zu l�schen:',200);
	
	$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG || \' - \' || ZLH_KUERZEL || \' - \' || CAST(count(*) AS varchar(10)) AS Anzeige';
	$SQL .= ' FROM Zukauflieferantenhersteller';
	$SQL .= ' INNER JOIN zukaufartikel ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ' WHERE ZLH_KEY > 0';
	//$SQL .= ' AND (SELECT COUNT(*) FROM Zukauflieferantenhersteller ZLH WHERE ZLH.ZLH_KUERZEL = Zukauflieferantenhersteller.ZLH_KUERZEL) > 1';
	$SQL .= ' GROUP BY ZLH_KEY, ZLH_BEZEICHNUNG, ZLH_KUERZEL';
	$SQL .= ' ORDER BY nlssort(ZLH_BEZEICHNUNG,\'NLS_SORT=BINARY\')';
	
	$Form->Erstelle_SelectFeld('loeschen','',200,true,$SQL);
	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	
	$Form->Erstelle_TextLabel('Zuordnen zu:',200);
	
	$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG || \' - \' || ZLH_KUERZEL || \' - \' || CAST(count(*) AS varchar(10)) AS Anzeige';
	$SQL .= ' FROM Zukauflieferantenhersteller';
	$SQL .= ' INNER JOIN zukaufartikel ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ' WHERE ZLH_KEY > 0';
	$SQL .= ' GROUP BY ZLH_KEY, ZLH_BEZEICHNUNG, ZLH_KUERZEL';
	$SQL .= ' ORDER BY nlssort(ZLH_BEZEICHNUNG,\'NLS_SORT=BINARY\')';
	
	$Form->Erstelle_SelectFeld('ziel',(isset($_POST['txtziel'])?$_POST['txtziel']:''),200,true,$SQL);
	
	$Form->ZeileEnde();

	
/*	
	$Form->ZeileStart();
	
	$Form->Erstelle_TextLabel('NUR diesen Lieferanten:',200);
	
	$SQL = 'SELECT LIE_NR, LIE_NAME1';
	$SQL .= ' FROM lieferanten';
	$SQL .= ' WHERE EXISTS(SELECT * FROM LIEFERANTENINFOS WHERE lin_lie_nr = lie_nr AND lin_ity_key = 1 AND lin_wert = 1';	
	$Form->Erstelle_SelectFeld('LIE_NR',(isset($_POST['txtLIE_NR'])?$_POST['txtLIE_NR']:''),200,true,$SQL);
	
	$Form->ZeileEnde();
*/

	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	
	$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	$Form->SchaltflaechenEnde();
	
	$Form->SchreibeHTMLCode('</form>');
}
catch(exception $ex)
{
	echo ' Fehler:'.$ex->getMessage();
	
	if(isset($DB) AND $DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
}
