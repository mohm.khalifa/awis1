<?php
require_once('/daten/include/awisZukauf_WUETSCHNER.inc');
require_once('/daten/include/awisJobVerwaltung.inc');


try
{
    $Job = new awisJobProtokoll(13, 'Starte Wuetschner - Lieferscheinimport.');

    $AWISBenutzer = awisBenutzer::Init('awis_jobs');
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $rsZUL = $DB->RecordSetOeffnen('SELECT MAX(ZUL_DATUM) AS LETZTER FROM Zukauflieferscheine WHERE ZUL_LIE_NR = \'8164\'');
    
    $Letzter = $Form->PruefeDatum($rsZUL->FeldInhalt('LETZTER'),0,0,1);
    $StartTag = date('d.m.Y',$Letzter);
    
    $ZUK = new awisZukauf_WUETSCHNER((isset($argv[1])?(int)$argv[1]:0),2);
    $Job->SchreibeLog(3,1,'Suche offene Lieferscheine...');

    while($Letzter < time())
    {
    	$Job->SchreibeLog(3,1,'Hole Daten vom '.date('d.m.Y',$Letzter));
    	if($ZUK->LeseLieferscheine(date('d.m.Y',$Letzter),date('d.m.Y',$Letzter),'','','',true)!==false)
	    {
	        $ZUK->SpeichereLieferscheine();
	    }
	    else
	    {
	    	$Job->SchreibeLog(3,9,'Fehler beim Zugriff auf die Daten. Breche ab.');
	    	die('Fehler beim Zugriff auf die Daten');
	    }
	    
	    $Letzter += (60*60*24);		// Einen Tag weiter
    }
   $Job->SchreibeLog(9,1,'Import ohne Fehler abgeschlossen.');
}
catch (Exception $ex)
{
	echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
	if(is_object($Job))
	{
		$Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
	}
	if(trim($ex->getMessage())=='Gateway Error')
	{
		if(awisWerkzeuge::EMail(array('zukauf@de.atu.eu','shuttle@de.atu.eu'),'ATU- SOAP Lieferscheinschnittstelle','Der Server ist nicht erreichbar. Fehlermeldung war: Gateway Error.',3)===false)
		{
			echo '  --> Fehler bei EXTERN E-Mail';
		}
	}
	elseif(trim($ex->getMessage())=='Error Fetching http headers')
	{
		if(awisWerkzeuge::EMail(array('zukauf@de.atu.eu','shuttle@de.atu.eu'),'ATU- SOAP Lieferscheinschnittstelle','Der Server ist nicht erreichbar. Fehlermeldung war: Error Fetching http headers.',3)===false)
		{
			echo '  --> Fehler bei EXTERN E-Mail';
		}
	}
}
?>
