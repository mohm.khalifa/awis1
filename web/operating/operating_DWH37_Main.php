<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();

$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
//echo '<title>'.$AWISSprachKonserven['TITEL']['tit_SRPEP'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();
/*
	if($AWISBenutzer->HatDasRecht(11000)==0)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"1010111020");
		die();
	}
*/	
	echo "<APPLET code='joperatingapplet/AppletMain.class' codebase='http://svdbslx037.server.atu.de/joperating/' archive='JOperating.jar' width=1250 height=750>";
	//echo "<param name='user' value='Applet-User'>";
	echo "<param name='user' value='".$AWISBenutzer->BenutzerName()."'>";
	echo "</APPLET>";
	
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"1010111010");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}
?>
</body>
</html>