<?php
global $AWISBenutzer;
global $con;
global $awisRSZeilen;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $CursorPosition;
global $AWISSprache;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('ASI', '%');
    $TextKonserven[] = array('AWS', '%');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'wrd_Abbildung');
    $TextKonserven[] = array('Wort', 'wrd_Anwendungstellen');
    $TextKonserven[] = array('Wort', 'wrd_Stueckliste');
    $TextKonserven[] = array('Wort', 'wrd_ZugeordneteFahrzeuge');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'txt_KeineEKATInfo');
    $TextKonserven[] = array('Wort', 'txt_KeinStuecklistenEintrag');
    $TextKonserven[] = array('Wort', 'txt_KeinEKATZusatzText');
    $TextKonserven[] = array('Wort', 'txt_KeineEKATInfo');
    $TextKonserven[] = array('Wort', 'txt_KeineAnwendungsstellen');
    $TextKonserven[] = array('Wort', 'txt_KeineAbbildung');
    $TextKonserven[] = array('Wort', 'txt_KeineZugeordnetenFahrzeuge');
    $TextKonserven[] = array('Wort', 'txt_ZulassungenGesamt');
    $TextKonserven[] = array('Wort', 'NummerKurz');

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MitReklInfo = $AWISBenutzer->ParameterLesen('ReklamationsInfoLieferanten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht400 = $AWISBenutzer->HatDasRecht(400);            // Recht f�r die Lieferantenartikel
    if ($Recht400 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $BindeVariablen = array();
    $BindeVariablen['var_N0_ast_key'] = intval('0' . $AWIS_KEY1);

    $SQL = 'SELECT AST_ATUNR FROM Artikelstamm WHERE AST_KEY = :var_N0_ast_key';
    $rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    $AST_ATUNR = $rsAST->FeldInhalt('AST_ATUNR');

    //********************************************************************
    //* Basisinformationen anzeigen
    //********************************************************************
    $BindeVariablen = array();
    $BindeVariablen['var_N0_ast_key'] = intval('0' . $AWIS_KEY1);

    $SQL = 'select AIT_INFORMATION AS ASI_EKAT_INFO, ASI_WERT AS ASI_EKAT_INFOWERT, AIT_ID';
    $SQL .= ' FROM v_ekat_artikelstamminfos ';
    $SQL .= ' INNER JOIN v_ekat_artikelstamminfotypen ON ASI_AIT_ID = AIT_ID';
    $SQL .= ' INNER JOIN artikelstamm ON ASI_AST_ATUNR = AST_ATUNR';
    $SQL .= ' WHERE  AST_KEY = :var_N0_ast_key';
    $SQL .= ' AND AIT_ID <9999990';
    $SQL .= ' ORDER BY ASI_SORTIERUNG, ASI_WERT';

    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ASI']['ASI_EKAT_INFO'], 250);
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ASI']['ASI_EKAT_INFOWERT'], 350);
    $Form->ZeileEnde();

    $rsAIT = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    if (!$rsAIT->EOF()) {
        $DS = 0;
        $LetzteASIKEY = 0;

        // Basisinfos anzeigen
        while (!$rsAIT->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('*ASI_EKAT_INFO', $rsAIT->FeldInhalt('ASI_EKAT_INFO'), 10, 250, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('*ASI_EKAT_INFOWERT', $rsAIT->FeldInhalt('ASI_EKAT_INFOWERT'), 10, 350, false, ($DS % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsAIT->DSWeiter();
            $DS++;
        }
    } else {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_KeineEKATInfo'], 1024);
        $Form->ZeileEnde();
    }

    //********************************************************************
    //* Zusatztexte
    //********************************************************************

    $BindeVariablen = array();
    $BindeVariablen['var_T_ast_atunr'] = $AST_ATUNR;

    $SQL = "SELECT TEXT FROM ARTIKEL_TEXTE ";
    $SQL .= " WHERE ATU_NR=:var_T_ast_atunr";

    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ASI']['ASI_EKAT_ZUSATZTEXT'], 604);
    $Form->ZeileEnde();
    $rsDaten = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    if (!$rsDaten->EOF()) {

        $DS = 0;
        $LetzteASIKEY = 0;

        // Basisinfos anzeigen
        while (!$rsDaten->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_Textarea('*TEXT', $rsDaten->FeldInhalt('TEXT'), 604, 0, 5, false, ($DS % 2), '', '', 'T');
            $Form->ZeileEnde();
            $rsDaten->DSWeiter();
        }
    } else {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_KeinEKATZusatzText'], 1024);
        $Form->ZeileEnde();
    }

    //**************************************
    // Anwendungsstellen
    //**************************************

    $BindeVariablen = array();
    $BindeVariablen['var_T_ast_atunr'] = $AST_ATUNR;

    $SQL = "select DISTINCT AWS_ID, AWS_BEZEICHNUNG ";
    $SQL .= " FROM V_EKAT_ANWENDUNGSSTELLEN";
    $SQL .= " INNER JOIN  V_EKAT_AST_ANWENDUNGSSTELLEN ON AWS_ID = AAS_AWS_ID";
    $SQL .= " WHERE AAS_AST_ATUNR=:var_T_ast_atunr";

    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_ID'], 100);
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_BEZEICHNUNG'], 500);
    $Form->ZeileEnde();
    $rsDaten = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    if (!$rsDaten->EOF()) {
        $DS = 0;
        $LetzteASIKEY = 0;

        // Basisinfos anzeigen
        while (!$rsDaten->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('*AWS_ID', $rsDaten->FeldInhalt('AWS_ID'), 10, 100, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('*AWS_BEZEICHNUNG', $rsDaten->FeldInhalt('AWS_BEZEICHNUNG'), 10, 500, false, ($DS % 2), '', '', 'T');
            $Form->ZeileEnde();
            $rsDaten->DSWeiter();
        }
    } else {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_KeineAnwendungsstellen'], 1024);
        $Form->ZeileEnde();
    }

    //**************************************
    // Abbildung
    //**************************************
    $BindeVariablen = array();
    $BindeVariablen['var_T_ast_atunr'] = $AST_ATUNR;

    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['wrd_Abbildung'], 604);
    $Form->ZeileEnde();
    $Form->ZeileStart();
    $SQL = 'SELECT bildname FROM BILDER_INFO WHERE atu_nr = :var_T_ast_atunr';
    $rsDaten = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    if ($rsDaten->EOF()) {
        // Bei St�cklisten steht die ATU Nummer im bildername - Feld!
        $SQL = 'SELECT bildname FROM BILDER_INFO WHERE bildname = :var_T_ast_atunr';
        $rsDaten = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    }

    if (!$rsDaten->EOF()) {
        $Form->SchreibeHTMLCode('<img src=./artikelstamm_bild.php?ATUNR=' . $AST_ATUNR . '>');
    } else {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_KeineAbbildung'], 1024);
        $Form->ZeileEnde();
    }
    $Form->ZeileEnde();

    //**************************************
    //* St�ckliste
    //**************************************
    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['wrd_Stueckliste'], 600);
    $Form->ZeileEnde();

    $BindeVariablen = array();
    $BindeVariablen['var_N0_ast_key'] = intval('0' . $AWIS_KEY1);

    $SQL = "select AIT_INFORMATION AS ASI_EKAT_INFO, ASI_WERT AS ASI_EKAT_INFOWERT, AIT_ID";
    $SQL .= ' FROM v_ekat_artikelstamminfos ';
    $SQL .= ' INNER JOIN v_ekat_artikelstamminfotypen ON ASI_AIT_ID = AIT_ID';
    $SQL .= ' INNER JOIN artikelstamm ON ASI_AST_ATUNR = AST_ATUNR';
    $SQL .= " WHERE  AST_KEY = :var_N0_ast_key";
    $SQL .= ' AND AIT_ID = 9999998';
    $SQL .= " ORDER BY ASI_SORTIERUNG, ASI_WERT";
    $rsAIT = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    $Form->ZeileStart();
    $StueckListe = false;
    if (!$rsAIT->EOF()) {
        $StueckListe = true;
        $DS = 0;
        while (!$rsAIT->EOF()) {
            $Form->Erstelle_ListenFeld('*ASI_EKAT_INFOWERT', $rsAIT->FeldInhalt('ASI_EKAT_INFOWERT'), 10, 85, false, ($DS % 2), '', '', 'T');
            $rsAIT->DSWeiter();
            $DS++;
        }
    } else {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_KeinStuecklistenEintrag'], 1024);
        $Form->ZeileEnde();
    }

    $Form->ZeileEnde();

    //*******************************************
    // Anwendungen in den Fahrzeugen
    //*******************************************

    if (($Recht400 & 64) == 64) {

        $BindeVariablen = array();

        $BindeVariablen['var_T_ast_atunr'] = $AST_ATUNR;

        // Daten f�r direkt zugeordnete Fahrzeuge
        $SQL = "SELECT DISTINCT KFZ_KBANR.KBANRC AS KBANR, KFZ_HERST.BEZ AS HERSTELLER, KFZ_MODELL.BEZ AS MODELL,
					KFZ_TYP.BEZ AS TYP, KFZ_TYP.BJVON AS BJVON, KFZ_TYP.BJBIS AS BJBIS, KFZ_HERST.KHERNR,
					KFZ_SNAP_ARTIKELVKN4.ANZ AS zulassungen, ARTIKEL_KTYPNR_AWST.AWSTNR as  baugruppe,
					KFZ_TYP.KTYPNR AS KTYPNR";
        $SQL .= " FROM ARTIKEL_KTYPNR_AWST
					LEFT JOIN KFZ_TYP ON ARTIKEL_KTYPNR_AWST.KTYPNR=KFZ_TYP.KTYPNR
					LEFT OUTER JOIN KFZ_KBANR ON KFZ_TYP.KTYPNR = KFZ_KBANR.KTYPNR
					LEFT JOIN KFZ_MODELL ON KFZ_TYP.KMODNR=KFZ_MODELL.KMODNR
					LEFT JOIN KFZ_HERST ON KFZ_MODELL.KHERNR=KFZ_HERST.KHERNR
					LEFT OUTER JOIN KFZ_SNAP_ARTIKELVKN4 ON (KFZ_SNAP_ARTIKELVKN4.HER || KFZ_SNAP_ARTIKELVKN4.TYP)= KFZ_KBANR.KBANRC";
        $SQL .= " WHERE ARTIKEL_KTYPNR_AWST.ATU_NR = :var_T_ast_atunr";

        // Bei St�cklisten zus�tzlich suchen
        if ($StueckListe) {
            $BindeVariablen['var_N0_ast_key'] = intval('0' . $AWIS_KEY1);
            $SQL .= ' UNION ';
            $SQL .= "SELECT DISTINCT KBANR, HERSTELLER, MODELL,
					TYP, BJVON, BJBIS, KHERNR,
					zulassungen, baugruppe,
					KTYPNR FROM (";
            $SQL .= " SELECT DISTINCT KFZ_KBANR.KBANRC AS KBANR, KFZ_HERST.BEZ AS HERSTELLER, KFZ_MODELL.BEZ AS MODELL,
					KFZ_TYP.BEZ AS TYP, KFZ_TYP.BJVON AS BJVON, KFZ_TYP.BJBIS AS BJBIS, KFZ_HERST.KHERNR,
					KFZ_SNAP_ARTIKELVKN4.ANZ AS zulassungen, ARTIKEL_KTYPNR_AWST.AWSTNR as  baugruppe,
					KFZ_TYP.KTYPNR AS KTYPNR";
            $SQL .= ', ASI_WERT';
            $SQL .= " FROM ARTIKEL_KTYPNR_AWST
						LEFT JOIN KFZ_TYP ON ARTIKEL_KTYPNR_AWST.KTYPNR=KFZ_TYP.KTYPNR
						LEFT OUTER JOIN KFZ_KBANR ON KFZ_TYP.KTYPNR = KFZ_KBANR.KTYPNR
						LEFT JOIN KFZ_MODELL ON KFZ_TYP.KMODNR=KFZ_MODELL.KMODNR
						LEFT JOIN KFZ_HERST ON KFZ_MODELL.KHERNR=KFZ_HERST.KHERNR
						LEFT OUTER JOIN KFZ_SNAP_ARTIKELVKN4 ON (KFZ_SNAP_ARTIKELVKN4.HER || KFZ_SNAP_ARTIKELVKN4.TYP)= KFZ_KBANR.KBANRC";
            $SQL .= ' INNER JOIN (';
            $SQL .= "select ASI_WERT";
            $SQL .= ' FROM v_ekat_artikelstamminfos ';
            $SQL .= ' INNER JOIN v_ekat_artikelstamminfotypen ON ASI_AIT_ID = AIT_ID';
            $SQL .= ' INNER JOIN artikelstamm ON ASI_AST_ATUNR = AST_ATUNR';
            $SQL .= " WHERE  AST_KEY = :var_N0_ast_key";
            $SQL .= ' AND AIT_ID = 9999998';
            $SQL .= ') ON ARTIKEL_KTYPNR_AWST.ATU_NR = ASI_WERT)';
        }
        $SQL .= " ORDER BY HERSTELLER, MODELL, TYP";
        $rsAWS = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
        $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

        if ($BildschirmBreite < 1024) {
            $FeldBreiten['AWS_HERSTELLER'] = 200;        // Wei bei voller Breite, weil 2-zeilig
            $FeldBreiten['ZEICHEN_HERSTELLER'] = 22;
            $FeldBreiten['AWS_MODELL'] = 230;
            $FeldBreiten['ZEICHEN_MODELL'] = 20;
            $FeldBreiten['AWS_TYP'] = 270;
            $FeldBreiten['ZEICHEN_TYP'] = 34;
            $FeldBreiten['UEBERSCHRIFT'] = 738;
        } elseif ($BildschirmBreite < 1280) {
            $FeldBreiten['AWS_HERSTELLER'] = 110;
            $FeldBreiten['ZEICHEN_HERSTELLER'] = 12;
            $FeldBreiten['AWS_MODELL'] = 160;
            $FeldBreiten['ZEICHEN_MODELL'] = 20;
            $FeldBreiten['AWS_TYP'] = 200;
            $FeldBreiten['ZEICHEN_TYP'] = 21;
            $FeldBreiten['UEBERSCHRIFT'] = 958;
        } else {
            $FeldBreiten['AWS_HERSTELLER'] = 190;
            $FeldBreiten['ZEICHEN_HERSTELLER'] = 16;
            $FeldBreiten['AWS_MODELL'] = 180;
            $FeldBreiten['ZEICHEN_MODELL'] = 20;
            $FeldBreiten['AWS_TYP'] = 240;
            $FeldBreiten['ZEICHEN_TYP'] = 25;
            $FeldBreiten['UEBERSCHRIFT'] = 1182;
        }

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['wrd_ZugeordneteFahrzeuge'], $FeldBreiten['UEBERSCHRIFT'], 'text-align:center;');
        $Form->ZeileEnde();

        if ($rsAWS->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_KeineZugeordnetenFahrzeuge'], $FeldBreiten['UEBERSCHRIFT']);
            $Form->ZeileEnde();
        } else {
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_HERSTELLER'], $FeldBreiten['AWS_HERSTELLER']);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_MODELL'], $FeldBreiten['AWS_MODELL']);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_TYP'], $FeldBreiten['AWS_TYP']);
            if ($BildschirmBreite < 1024) {
                $Form->ZeileEnde();
                $Form->ZeileStart();
                $Form->Erstelle_ListenFeld('#FILLER', '', 0, 120, false, ($DS % 2));
            }
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_BJVON'], 74);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_BJBIS'], 74);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_ZULASSUNGEN'], 89);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_BAUGRUPPE'], 89);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_KTYPNR'], 77);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AWS']['AWS_KBANR'], 77);
            if ($rsAIT->AnzahlDatensaetze() != 0) {
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['NummerKurz'], 85);
            }
            $Form->ZeileEnde();

            $KBANummern = array();            // Zulassungen nur f�r KBA Nummern z�hlen
            $AnzZulassungen = 0;
            $DS = 0;
            while (!$rsAWS->EOF()) {
                $Form->ZeileStart();
                $Text = (strlen($rsAWS->FeldInhalt('HERSTELLER')) > $FeldBreiten['ZEICHEN_HERSTELLER']?substr($rsAWS->FeldInhalt('HERSTELLER'), 0,
                        $FeldBreiten['ZEICHEN_HERSTELLER'] - 2) . '...':$rsAWS->FeldInhalt('HERSTELLER'));

                $Form->Erstelle_ListenFeld('#AWS_HERSTELLER', $Text, 0, $FeldBreiten['AWS_HERSTELLER'], false, ($DS % 2), '', '', 'T', 'L', $rsAWS->FeldInhalt('HERSTELLER'));
                $Form->Erstelle_ListenFeld('#AWS_MODELL', (strlen($rsAWS->FeldInhalt('MODELL')) > $FeldBreiten['ZEICHEN_MODELL']?substr($rsAWS->FeldInhalt('MODELL'), 0,
                        $FeldBreiten['ZEICHEN_MODELL'] - 2) . '...':$rsAWS->FeldInhalt('MODELL')), 0, $FeldBreiten['AWS_MODELL'], false, ($DS % 2), '', '', 'TS', 'L',
                    $rsAWS->FeldInhalt('MODELL'));
                $Form->Erstelle_ListenFeld('#AWS_TYP', (strlen($rsAWS->FeldInhalt('TYP')) > $FeldBreiten['ZEICHEN_TYP']?substr($rsAWS->FeldInhalt('TYP'), 0,
                        $FeldBreiten['ZEICHEN_TYP'] - 2) . '...':$rsAWS->FeldInhalt('TYP')), 0, $FeldBreiten['AWS_TYP'], false, ($DS % 2), '', '', 'T', 'L',
                    $rsAWS->FeldInhalt('TYP'));
                if ($BildschirmBreite < 1024) {
                    $Form->ZeileEnde();
                    $Form->ZeileStart();
                    $Form->Erstelle_ListenFeld('#FILLER', '', 0, 120, false, ($DS % 2));
                }
                $Form->Erstelle_ListenFeld('#AWS_BJVON', $rsAWS->FeldInhalt('BJVON'), 0, 74, false, ($DS % 2));
                $Form->Erstelle_ListenFeld('#AWS_BJBIS', $rsAWS->FeldInhalt('BJBIS'), 0, 74, false, ($DS % 2));
                $Form->Erstelle_ListenFeld('#AWS_ZULASSUNGEN', $rsAWS->FeldInhalt('ZULASSUNGEN'), 0, 89, false, ($DS % 2), '', '', 'N0T');
                $Form->Erstelle_ListenFeld('#AWS_BAUGRUPPE', $rsAWS->FeldInhalt('BAUGRUPPE'), 0, 89, false, ($DS % 2));
                $Form->Erstelle_ListenFeld('#AWS_KTYPNR', $rsAWS->FeldInhalt('KTYPNR'), 0, 77, false, ($DS % 2));
                $Form->Erstelle_ListenFeld('#AWS_KBANR', $rsAWS->FeldInhalt('KBANR'), 0, 77, false, ($DS % 2));
                if ($rsAIT->AnzahlDatensaetze() != 0) {
                    $Form->Erstelle_ListenFeld('#ASI_WERT', $rsAWS->FeldInhalt('ASI_WERT'), 0, 85, false, ($DS % 2));
                }

                if (array_search($rsAWS->FeldInhalt('KBANR'), $KBANummern) === false) {
                    $KBANummern[] = $rsAWS->FeldInhalt('KBANR');
                    $AnzZulassungen += $rsAWS->FeldInhalt('ZULASSUNGEN');
                }
                $Form->ZeileEnde();

                $rsAWS->DSWeiter();
                $DS++;
            }

            $Form->Trennzeile();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_ZulassungenGesamt'] . ': ' . $Form->Format('N0T', $AnzZulassungen, false), 800);
            $Form->ZeileEnde();
            unset($rsAWS);
        }
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060001");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812061223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>