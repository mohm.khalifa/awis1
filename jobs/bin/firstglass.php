<?php

require_once("/daten/include/firstglas_Shuttle.php");
require_once("/daten/include/awisJobVerwaltung.inc");

ini_set('max_execution_time', 0);

date_default_timezone_set('UTC');
//setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'deu_deu');

try 
{
    $Job = new awisJobProtokoll(8, 'Starte FirstglassVerarbeitung');
    
    $Shuttle = new firstglasShuttle();
    $Shuttle->pruefeDWHGeladen();
    $Shuttle->Shuttle();
    
}
catch (Exception $ex)
{	
	echo PHP_EOL.'Firstglass: Fehler: '.$ex->getMessage().PHP_EOL;
		
	if(is_object($Job))
	 {
           $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
         }
}







?>
