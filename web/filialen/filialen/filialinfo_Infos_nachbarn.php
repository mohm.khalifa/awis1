<?php
global $con;
global $AWISSprache;
global $AWISBenutzer;
	
	//*******************************************
	// Nachbarfilialen
	//*******************************************
$TXT_Baustein = array();
$TXT_Baustein[]=array('FIL','FIL_%');
$TXT_Baustein[]=array('Wort','LetzteAenderung');
$TXT_Baustein[]=array('Wort','Eroeffnung');
$TXT_Baustein[]=array('Wort','Umzug');
$TXT_Baustein[]=array('Wort','lbl_weiter');
$TXT_Baustein[]=array('Wort','lbl_speichern');

$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);


$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
$FIL_ID = $FIL_ID[1];
$rsFiliale = awisOpenRecordset($con,'SELECT * FROM Filialen WHERE FIL_ID=0'.$FIL_ID);

//awis_Debug(1,$rsFiliale,$FIL_ID);

// Wegen Importustellung 7-stellige PLZ
if(strpos($rsFiliale['FIL_PLZ'][0],';')!==false)
{
	$rsFiliale['FIL_PLZ'][0]=substr($rsFiliale['FIL_PLZ'][0],0,5);
}
$Link = 'http://filialsuche.server.atu.de/filialsuche?plz=' . $rsFiliale['FIL_PLZ'][0] . '&lkz=D&anzahl=' . (awis_BenutzerParameter($con, 'FilialNachbarn', $AWISBenutzer->BenutzerName())+1);


$Antwort = file_get_contents($Link);

if($Antwort!==false)
{
	$p = xml_parser_create();
	xml_parse_into_struct($p, $Antwort, $vals, $index);
	xml_parser_free($p);
	$Nr = 0;
	$FilListe='';
	foreach($vals as $Daten)
	{
		if($Daten['tag']=='NR')
		{
			$Filialen['NR'][++$Nr]=$Daten['value'];
			$FilListe .= ',' . $Daten['value'];
		}
		elseif($Daten['tag']=='NAME')
		{
			$Filialen['NAME'][$Nr]=$Daten['value'];
		}
		elseif($Daten['tag']=='PLZ')
		{
			$Filialen['PLZ'][$Nr]=$Daten['value'];
		}
		elseif($Daten['tag']=='STRASSE')
		{
			$Filialen['STRASSE'][$Nr]=$Daten['value'];
		}
		elseif($Daten['tag']=='ORT')
		{
			$Filialen['ORT'][$Nr]=$Daten['value'];
		}
		elseif($Daten['tag']=='NEUEROEFFNUNG')
		{
			$Filialen['NEUEROEFFNUNG'][$Nr]=isset($Daten['value'])?$Daten['value']:'';
		}
		elseif($Daten['tag']=='UMZUG')
		{
			$Filialen['UMZUG'][$Nr]=isset($Daten['value'])?$Daten['value']:'';
		}
	}

	echo '<table border=1>';
	echo '<tr><td id=FeldBez>'.$TXT_Baustein['FIL']['FIL_ID'].'</td>';
	echo '<td id=FeldBez>'.$TXT_Baustein['FIL']['FIL_BEZ'].'</td>';
	echo '<td id=FeldBez>'.$TXT_Baustein['FIL']['FIL_STRASSE'].'</td>';
	echo '<td id=FeldBez>'.$TXT_Baustein['FIL']['FIL_PLZ'].'</td>';
	echo '<td id=FeldBez>'.$TXT_Baustein['FIL']['FIL_ORT'].'</td>';
	echo '<td id=FeldBez>'.$TXT_Baustein['Wort']['Eroeffnung'].'</td>';
	echo '<td id=FeldBez>'.$TXT_Baustein['Wort']['Umzug'].'</td>';
	echo '<td id=FeldBez>'.$TXT_Baustein['FIL']['FIL_LAGERKZ'].'</td></tr>';

	//if(strtotime($ErOeffDat) 
	for($nbFil=1;$nbFil<=$Nr;$nbFil++)
	{
		if($Filialen['NEUEROEFFNUNG'][$nbFil]=='')
		{
			$ErOeffDat='';
		}
		else
		{
			$ErOeffDat = explode('.',$Filialen['NEUEROEFFNUNG'][$nbFil]);
			$ErOeffDat = strtotime($ErOeffDat[1].'/'.$ErOeffDat[0].'/'.$ErOeffDat[2]);
			if($ErOeffDat < time())
			{
				$ErOeffDat='';
			}
			else
			{
				$ErOeffDat = strftime('%d.%m.%Y');
			}
		}


		if($Filialen['NR'][$nbFil]!=$rsFiliale['FIL_ID'][0])
		{
			echo '<tr><td><a href=./filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $Filialen['NR'][$nbFil] . '>' . $Filialen['NR'][$nbFil] . '</a></td>';
			echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['NAME'][$nbFil] . ($ErOeffDat==''?'':'</i>'). '</td>';
			echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['STRASSE'][$nbFil] . ($ErOeffDat==''?'':'</i>').'</td>';
			echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['PLZ'][$nbFil] . ($ErOeffDat==''?'':'</i>').'</td>';
			echo '<td>' . ($ErOeffDat==''?'':'<i>').$Filialen['ORT'][$nbFil] . ($ErOeffDat==''?'':'</i>').'</td>';
			echo '<td ' . ($Filialen['NEUEROEFFNUNG'][$nbFil]==''?'':"title='Seit " . $Filialen['NEUEROEFFNUNG'][$nbFil] . "'") . '>' . ($ErOeffDat==''?'offen':'ab <b>'.$Filialen['NEUEROEFFNUNG'][$nbFil].'</b>') . '</td>';
			echo '<td>' . ($Filialen['UMZUG'][$nbFil]==''?'nein':'<b>'.$Filialen['UMZUG'][$nbFil].'</b>') . '</td>';
			$rsFILLager = awisOpenRecordset($con,'SELECT FIL_LAGERKZ from Filialen WHERE FIL_ID=0'.$Filialen['NR'][$nbFil]);
			echo '<td>' . ($ErOeffDat==''?'':'<i>').$rsFILLager['FIL_LAGERKZ'][0] . ($ErOeffDat==''?'':'</i>'). '</td>';			
			echo '<tr>';
		}
	}
	echo '</table>';
}// Ende Nachbarn



	
?>