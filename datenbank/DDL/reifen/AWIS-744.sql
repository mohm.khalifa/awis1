INSERT INTO reifeninfostypen (rnt_id,rnt_information,rnt_bemerkung,rnt_sortierung) VALUES (14,'Wintertauglichkeit','Gibt an, ob Reifen garantiert Wintertauglichkeit', 204);
INSERT INTO reifeninfostypen (rnt_id,rnt_information,rnt_bemerkung,rnt_sortierung) VALUES (30,'INFO','Infofeld', 300);

ALTER TABLE REIFENSTAMM ADD RST_WINTERTAUGLICHKEIT VARCHAR2(2);
ALTER TABLE REIFENSTAMM ADD RST_INFO VARCHAR2(15);

INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('RST_WINTERTAUGLICHKEIT','RST','Wintertauglichkeit','21.08.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('RST_INFO','RST','Info','21.08.2018');
INSERT INTO textkonserven (xtx_kennung,xtx_bereich,xtx_text_de,xtx_userdat) VALUES ('lst_WINTERTAUGLICHKEIT','RST','X~X| ~-','21.08.2018');
