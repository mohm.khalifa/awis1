<?php
require_once 'awisXML.inc';
require_once 'awisDatenbank.inc';

class awisRest
{

    private $_XMLRoot = 'root';
    private $_XMLVersion = '1.0';
    private $httpVersion = "HTTP/1.1";
    private $_ServiceName;
    private $_DebugLevel = 0;
    private $_Debug = 0;
    private $_REQUEST;
    private $_LetzterStatusCode;

    public function __construct($ServiceName = 'Default')
    {
        $this->_ServiceName = $ServiceName;
    }

    public function __destruct()
    {
        if ($this->_DebugLevel >= 1) {
            echo $this->_Debug;
        }
    }

    /**
     * Liefert die RequestMethode
     */
    public function getRequestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function verarbeiteRequest()
    {
        foreach ($_GET as $Key => $Wert) {
            $Key = strtoupper($Key);
            $this->_REQUEST[$Key]['TYP'] = 'GET';
            $this->_REQUEST[$Key]['WERT'] = rawurldecode($Wert);
        }

        foreach ($_POST as $Key => $Wert) {
            $Num = '';
            if (isset($this->_REQUEST[$Key]['WERT'])) //Evtl, war der Post ja schon ein GET..
            {
                $Num = '2';
            }
            $Key = strtoupper($Key);
            $this->_REQUEST[$Key . $Num]['TYP'] = 'POST';
            $this->_REQUEST[$Key . $Num]['WERT'] = $Wert;
        }
    }

    /**
     * Liefert den Wert eines Requestparameters
     *
     * @return $Key = '' dann, array() mit [KEY][WERT] und [KEY][TYP]
     * @return $Key != '' dann, string, Wert aus Request
     * @param string $Key KEY_AUS_REQUEST o. ''
     * @param string $Typ POST o. GET o. ''
     */
    public function getRequest($Key = '', $Typ = '')
    {
        $Typ = strtoupper($Typ);
        $Key = strtoupper($Key);

        if ($Key !== '') {
            if ((isset($this->_REQUEST[$Key]['WERT']) and $Typ === '') or (isset($this->_REQUEST[$Key]['WERT']) and $Typ !== '' and $this->_REQUEST[$Key]['TYP'] == $Typ)) {
                return $this->_REQUEST[$Key]['WERT'];
            } else {
                return false;
            }
        } else {
            return $this->_REQUEST;
        }
    }

    const AntwortTyp_JSON = 'application/json';
    const AntwortTyp_HTML = 'text/html';
    const AntwortTyp_XML = 'application/xml';

    /**
     * Gibt die Daten zur�ck
     *
     * @param unknown $Daten awisRecordset o. Array()
     */
    public function sendeResponse($Daten, $AntwortTyp = '', $StatusCode = 0)
    {

        //Schauen ob die Daten evtl. ein awisRecordset sind und ggf. konvertieren..
        if (is_a($Daten, 'awisRecordset')) {
            $Daten = $this->_RecordsetToArray($Daten);
        }

        if($StatusCode == 0){
            //Keine Daten?-->404 not found
            if (empty($Daten)) {
                $StatusCode = 404;
                $Daten = array('error' => 'No DATA found!');
            } else {
                $StatusCode = 200;
            }
        }

        if ($AntwortTyp == '') {
            $AntwortTyp = @$_SERVER['HTTP_ACCEPT'];
        }

        $this->_setzeHTTPHeader($AntwortTyp, $StatusCode);
        //je nachdem was angefordert wurde, reagieren..
        if (strpos($AntwortTyp, self::AntwortTyp_JSON) !== false) {
            $response = $this->_generiereJson($Daten);
            echo $response;
        } elseif (strpos($AntwortTyp, self::AntwortTyp_HTML) !== false) {
            $response = $this->_generiereHtml($Daten);
            echo $response;
        } elseif (strpos($AntwortTyp, self::AntwortTyp_XML) !== false) {
            $response = $this->_generiereXML($Daten);
            echo $response;
        } else {
            $response = $this->_generiereJson($Daten);
            echo $response;
            throw new awisException("Unbekannter REST Antworttyp: $AntwortTyp  
					von " . $_SERVER['REMOTE_ADDR'] . " 
					bei Service: " . $this->_ServiceName, '201605171831', '', awisException::AWIS_ERR_SYSTEM);
        }
    }

    /**
     * Gibt die Daten als XML zur�ck
     *
     * @param $Daten
     * @return mixed
     */
    private function _generiereXML($Daten)
    {
        $awisXML = new awisXML();
        $awisXML->setzeXMLRoot($this->_XMLRoot);
        $awisXML->setzeXMLVersion($this->_XMLVersion);

        return $awisXML->ArrayZuXML($Daten);
    }

    /**
     * Gibt die Daten (zum debuggen) in einer HTML Tabelle aus
     *
     * @param unknown $Daten
     * TODO: geht nicht, deswegen erstmal XML Ausgabe
     */
    private function _generiereHtml($Daten)
    {

        $htmlResponse = $this->_generiereXML($Daten);
        // 		$htmlResponse = "<table border='1'>";
        // 		if (!is_array($Daten))
        // 		{
        // 			$htmlResponse .= "<tr><td>". $Daten. "</td></tr>";
        // 		}
        // 		else
        // 		{
        // 			foreach($Daten as $key=>$value)
        // 			{

        // 				$htmlResponse .= "<tr><td>". $key. "</td><td>". $value. "</td></tr>";
        // 			}
        // 		}

        // 		$htmlResponse .= "</table>";
        return $htmlResponse;
    }

    /**
     * Gibt die Daten als Json zur�ck
     *
     * @param unknown $Daten
     */
    private function _generiereJson($Daten,$UTF8 = false)
    {
        $jsonResponse = json_encode($Daten);
        if ($jsonResponse === false) {
            if(!$UTF8){
                $Daten = awisWerkzeuge::UTF8Code($Daten);
                return $this->_generiereJson($Daten,true);
            }
            $this->_DebugAusgabe('Fehler beim JsonDecode', 10, __LINE__);
        } else {

            $this->_DebugAusgabe("JsonDecode Ergebnis: $jsonResponse", 99, __LINE__);
            return $jsonResponse;
        }
        return $jsonResponse;
    }

    function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                $this->utf8_encode_deep($value);
            }

            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));

            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
    }

    /**
     * setzt Das Debuglevel
     *
     * @param unknown $Level
     */
    public function setzeDebugLevel($Level)
    {
        $this->_DebugLevel = $Level;
    }

    /**
     * Erzeugt eine formatierte Debugausgabe
     *
     * @param string $Text  Debugtext
     * @param number $Level Ben�tigtes Level
     * @param system $Zeile Aufrufede Zeile. = __LINE__
     */
    private function _DebugAusgabe($Text, $Level = 50, $Zeile = __LINE__)
    {
        if ($Level >= $this->_DebugLevel) {
            $this->_Debug .= date('d.m.Y H:i:s') . ' Z' . $Zeile . ' - ' . $Text . PHP_EOL;
        }
    }

    //TODO: Recordset->Tabelle() verwernden
    private function _RecordsetToArray($awisRecordset)
    {
        $this->_DebugAusgabe("Konvertiere Recordset zu Array", 99, __LINE__);
        try {
            $Felder = array();

            $Felder = $awisRecordset->SpaltenNamen();
            $Daten = array();
            $i = 0;
            while (!$awisRecordset->EOF()) {
                foreach ($Felder as $Feld) {
                    $Daten[$i][$Feld] = $awisRecordset->FeldInhalt($Feld);
                }
                $i++;
                $awisRecordset->DSWeiter();
            }
            return $Daten;
        } catch (Exception $e) {
            $this->_DebugAusgabe("Fehler beim Recordset zu Array konvertieren $e->getMessage()", 10, __LINE__);
        }
    }

    /**
     * setzt die HTTP header
     *
     * @param unknown $contentType
     * @param unknown $StatusCode
     */
    private function _setzeHTTPHeader($contentType, $StatusCode)
    {
        $this->_DebugAusgabe("Setze HTTP Header $contentType Status: $StatusCode", 99, __LINE__);
        $statusMessage = $this->getHttpStatusMessage($StatusCode);

        header($this->httpVersion . " " . $StatusCode . " " . $statusMessage);
        header("Content-Type:" . $contentType);
    }

    /**
     * Liefert die Statusmessage zu einem Statuscode
     *
     * @param unknown $StatusCode
     */
    public function getHttpStatusMessage($StatusCode)
    {
        $httpStatus = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
        );

        return ($httpStatus[$StatusCode])?$httpStatus[$StatusCode]:$httpStatus[500];
    }

    /**
     * Sendet einen Request zu einem Webservice
     *
     * @param string $URI
     * @param string $Methode
     * @param string $AnfrageTyp
     * @param array  $Parameter
     * @throws Exception
     */
    public function erstelleRequest($URI, $Methode = 'GET', $AnfrageTyp = 'JSON', $Parameter = array(), $ZusatzHeader = array(), $HeaderErsetzen = false)
    {
        try {
            $this->_DebugAusgabe("Hole Daten von $URI mit Methode $Methode als $AnfrageTyp", 99, __LINE__);

            $ContentType = 'application/x-www-form-urlencoded';

            if ($Methode == 'POST') {
                $POST = http_build_query($Parameter);
            } elseif($Methode == 'JSONPOST'){
                $Methode = 'POST';
                $POST = json_encode($Parameter);
                $ContentType = 'application/json';
            }else {
                $POST = '';
                $GETs = strpos($URI,'?')===false?'?':'&';
                foreach (array_keys($Parameter) as $Key) {
                    $GETs .= $Key . '=' . $Parameter[$Key] . '&';
                }
                $GETs = substr($GETs,0,-1);
                $URI .= rawurldecode($GETs);
            }

            $header = array(
                "Accept-language" => "en",
                "HTTP_ACCEPT"     => "application/$AnfrageTyp",
                "Content-Type"    => $ContentType,
            );

            if (is_array($ZusatzHeader) and count($ZusatzHeader) > 0) {
                if ($HeaderErsetzen == true) {
                    $header = $ZusatzHeader;
                } else {
                    $header = array_merge($header, $ZusatzHeader);
                }
            }

            $opts = array(
                'http' => array(
                    'method'  => "$Methode",
                    'header'  => $this->zerlegeHeader($header),
                    'content' => $POST,
                ),
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                )
            );


            $context = stream_context_create($opts);


            // �ffnen der Datei mit den oben definierten HTTP-Headern
            $Antwort = @file_get_contents($URI, false, $context);

            preg_match('{HTTP\/\S*\s(\d{3})}', $http_response_header[0], $match);

            $this->_LetzterStatusCode = $match[1];
            if ($Antwort === false) {
                $this->_DebugAusgabe("Fehler bei file_get_contents", 10, __LINE__);
            }

            if ($AnfrageTyp == 'XML') {
                $this->_DebugAusgabe("Erzeuge Array aus XML", 99, __LINE__);
                $awisXML = new awisXML();
                $Antwort = $awisXML->XMLzuArray($Antwort);
            } elseif ($AnfrageTyp == 'JSON') {
                $this->_DebugAusgabe("Erzeuge Array aus JSON", 99, __LINE__);
                $Antwort = mb_convert_encoding($Antwort, 'HTML-ENTITIES', 'utf-8');
                $Antwort = json_decode($Antwort, true);
            } else {
                $this->_DebugAusgabe("Unbekannter RESTAnfragetyp:  $AnfrageTyp", 99, __LINE__);
                throw new awisException('Unbekannter REST Anfragetyp', '201605171925', '', awisException::AWIS_ERR_SYSTEM);
            }

            return $Antwort;
        } catch (Exception $e) {
            $this->_DebugAusgabe("Fehler beim Daten holen:  $e->getMessage()", 10, __LINE__);
        }
    }

    public function letzterStatusCode(){
        return $this->_LetzterStatusCode;
    }
    /**
     * Setzt das �u�erste XML Tag
     *
     * @param unknown $Name
     */
    public function setzeXMLRoot($Name)
    {
        $this->_XMLRoot = $Name;
    }

    /**
     * Setzt die XML Verion
     *
     * @param unknown $Version
     */
    public function setzeXMLVersion($Version)
    {
        $this->_XMLVersion = $Version;
    }

    /**
     * Zerlegt den Header f�r die �bertragung per file_get_contents
     *
     * @param $HeaderArray
     * @return string
     */
    private function zerlegeHeader($HeaderArray)
    {
        $Header = [];

        foreach ($HeaderArray as $key => $value) {
            $Header[] = $key . ': ' . $value;
        }

        return implode("\r\n", $Header);
    }
}


?>