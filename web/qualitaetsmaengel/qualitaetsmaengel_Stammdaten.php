<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Fehler;

try
{	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('QMM','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	$Fehler = 0;

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	if($Recht34000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_POST['cmdSpeichern_x']))
	{
		if(isset($_POST['txtQMK_SCHLUESSEL']) or isset($_POST['txtQMK_LANG']))
		{
			if(!$_POST['txtQMK_SCHLUESSEL'])
			{
				$_GET['QMK_KEY'] = $_POST['txtQMK_KEY'];
				$Fehler=$Fehler+1;
			}
			if(!$_POST['txtQMK_LANG'])
			{
				$_GET['QMK_KEY'] = $_POST['txtQMK_KEY'];
				$Fehler=$Fehler+2;
			}
		}
		if(isset($_POST['txtQMG_GUELTIGAB']) or isset($_POST['txtQMG_GUELTIGBIS']))
		{
			if($_POST['txtQMG_GUELTIGAB'] == '')
			{
				$Fehler=$Fehler+8;
				$_GET['QMK_KEY'] = $_POST['txtQMK_KEY'];
				$_GET['QMG_KEY'] = $_POST['txtQMG_KEY'];
			}
			if($_POST['txtQMG_GUELTIGBIS'] == '')
			{
				$Fehler=$Fehler+16;
				$_GET['QMK_KEY'] = $_POST['txtQMK_KEY'];
				$_GET['QMG_KEY'] = $_POST['txtQMG_KEY'];
			}
			if($_POST['txtQMG_GUELTIGBIS'] != '' and strtotime($_POST['txtQMG_GUELTIGBIS']) < strtotime(date('d.m.Y',time())))
			{
				$Fehler=$Fehler+32;
				$_GET['QMK_KEY'] = $_POST['txtQMK_KEY'];
				$_GET['QMG_KEY'] = $_POST['txtQMG_KEY'];
			}
			
			if(($_POST['txtQMG_GUELTIGBIS'] != '' and $_POST['txtQMG_GUELTIGAB'] != '') and (strtotime($_POST['txtQMG_GUELTIGBIS']) < strtotime($_POST['txtQMG_GUELTIGAB'])))
			{
				$Fehler=$Fehler+64;
				$_GET['QMK_KEY'] = $_POST['txtQMK_KEY'];
				$_GET['QMG_KEY'] = $_POST['txtQMG_KEY'];
			}
		}
		if($Fehler == 0)
		{
			include('./qualitaetsmaengel_speichern.php');
			if($Fehler <> 0)
			{
				$_GET['QMK_KEY'] = $_POST['txtQMK_KEY'];
			}
		}
	}
	
	elseif(isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./qualitaetsmaengel_loeschen.php');
	}
	

	if(isset($_GET['QMK_KEY']) and !isset($_GET['QMG_KEY']))
	{
		$Form->SchreibeHTMLCode('<form name="frmQMGStammdaten" action="./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite=Kriterien&Unterseite=Gueltigkeiten" method=POST  enctype="multipart/form-data">');
	}
	else if(isset($_GET['QMG_KEY']))
	{
		$Form->SchreibeHTMLCode('<form name="frmQMGStammdaten" action="././qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite=Kriterien&Unterseite=Gueltigkeiten&QMK_KEY='.$_GET['QMK_KEY'].'" method=POST  enctype="multipart/form-data">');
	}
	else if($_GET['Seite'] == 'Massnahmen')
	{
		$Form->SchreibeHTMLCode('<form name="frmQMMStammdaten" action="./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite=Massnahmen" method=POST  enctype="multipart/form-data">');
	}
	else if($_GET['Seite'] == 'Kriterien')
	{
		$Form->SchreibeHTMLCode('<form name="frmQMMStammdaten" action="./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite=Kriterien" method=POST  enctype="multipart/form-data">');
	}
			
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Kriterien'));
			
		$RegDet = new awisRegister(34010);
		$RegDet->ZeichneRegister($RegisterSeite);		
		
   
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');	
	if (isset($_GET['QMK_KEY']) or isset($_GET['QMM_KEY']))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->SchreibeHTMLCode('</form>');
	
	$Form->SchaltflaechenEnde();

	
	
	
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}



?>