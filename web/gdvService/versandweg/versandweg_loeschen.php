<?php
/**
 * L�schen Stammdatenpflege GDV-Versandweg
 *
 * @author Sch�ffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607061358
 * @todo
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Tabelle= '';

if(isset($_GET['Del']) or isset($_POST['cmdLoeschen_x'])) {
	switch ($_GET['cmdAktion']) {
		case 'Details':
			$Tabelle = 'VVW';
			$Key = isset($_GET['Del'])?$_GET['Del']:$_POST['txtKey'];

			$SQL = '     SELECT';
			$SQL .= '       VVW_KEY,';
			$SQL .= '       VVW_VERSNR,';
			$SQL .= '       VVE_BEZEICHNUNG,';
			$SQL .= '       VVW_RELEASE,';
			$SQL .= '       VVW_USER,';
			$SQL .= '       VVW_USERDAT';
			$SQL .= '     FROM';
			$SQL .= '       versversandweg';
			$SQL .= '    inner join versversicherungen on versversandweg.VVW_VERSNR = VVE_VERSNR';
			$SQL .= ' WHERE VVW_KEY=' . $Key;
			$rsDaten = $DB->RecordSetOeffnen($SQL);

			/**
			 * Werte f�r Frage 'Wirklich l�schen?' holen
			 **/
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_VERSICHERUNG'),$rsDaten->FeldInhalt('VVE_BEZEICHNUNG'));
			$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_RELEASE'),$rsDaten->FeldInhalt('VVW_RELEASE') == 0?'Mailweg':$rsDaten->FeldInhalt('VVW_RELEASE'));
			break;
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// L�schen durchf�hren wenn auf Frage 'Wirklich l�schen?' ok
{
	switch ($_POST['txtTabelle'])
	{
		case 'VVW':
			$SQL = 'DELETE FROM VERSVERSANDWEG WHERE VVW_key='.$_POST['txtKey']; // Versandweg l�schen
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
			if($_POST['txtArt'] == 'p'){
				$Param['VVW_VERSICHERUNG'] =  '';
				$Param['VVW_RELEASE']= '';
				$Param['SPEICHERN']='';
				$AWISBenutzer->ParameterSchreiben('Formular_GDV_VA',serialize($Param));
			}
			break;
	}
}
if($Tabelle!='') {

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	switch ($_GET['cmdAktion']) {
		case 'Details':
			$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./versandweg_Main.php?cmdAktion=Details method=post>');
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);    // Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
			$Form->Trennzeile('O');

			foreach ($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0] . ':', 200);
				$Form->Erstelle_TextFeld('Feld', $Feld[1], 100, 500, false);
				$Form->ZeileEnde();
			}
			/**
			 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
			 **/
			$Form->Erstelle_HiddenFeld('Key', $Key);
			$Form->Erstelle_HiddenFeld('Tabelle', $Tabelle);
			$Form->Erstelle_HiddenFeld('Art',isset($_GET['Del'])?'g':'p');
			$Form->Trennzeile();

			/**
			 * L�schen ja/nein
			 **/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '');
			$Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '');
			$Form->ZeileEnde();
			$Form->SchreibeHTMLCode('</form>');
			$Form->Formular_Ende();
			die();
			break;
	}
}
?>