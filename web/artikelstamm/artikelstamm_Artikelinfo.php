<?php
/**
 * artikelstamm_ArtikelInfo.php
 *
 * Zentrale �bersichtsseite f�r den Artikelstamm
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto-Teile-Unger
 *
 * @version   200815031634
 *
 *
 */
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
global $EditModus;
global $DetailAnsicht;

require_once('awisATUArtikel.inc');

try {
    $AWISBenutzer = awisBenutzer::Init();

    //Englische Benutzersprache bekommen deutsche Preise usw.
    $BenutzerSprache = strtoupper($AWISBenutzer->BenutzerSprache());
    if($BenutzerSprache == 'EN' or $BenutzerSprache == 'FR' ){
        $BenutzerSprache = 'DE';
    }

    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $MAX_TREFFER = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
    $BenutzerFilialen = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    if ($BildschirmBreite < 1024) {
        $FeldBreiten['AST_BEZEICHNUNGWW'] = 100;
        $FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] = 10;
    } elseif ($BildschirmBreite < 1280) {
        $FeldBreiten['AST_BEZEICHNUNGWW'] = 200;
        $FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] = 20;
    } else {
        $FeldBreiten['AST_BEZEICHNUNGWW'] = 400;
        $FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] = 40;
    }

    $EditModus = false;
    $DetailAnsicht = false;
    $KatalogArtikel = false;        // Zeigt an, wenn es ein Katalogartikel ist

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('RIN', '%');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'txt_AnzahlDSGefunden');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Wort', 'wrd_AusWWGeloescht');
    $TextKonserven[] = array('Wort', 'FREMDKAUF12');
    $TextKonserven[] = array('Wort', 'FREMDKAUF24');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('FIB', 'FIB_BESTAND');
    $TextKonserven[] = array('WGR', 'WGR_ID');
    $TextKonserven[] = array('LAR', 'LAR_LIE_NR');
    $TextKonserven[] = array('WUG', 'WUG_ID');
    $TextKonserven[] = array('WGR', 'ttt_WGR_ID');
    $TextKonserven[] = array('WGR', 'ttt_AST_KENN2');
    $TextKonserven[] = array('LIE', 'LIE_NR');

    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $Recht452 = $AWISBenutzer->HatDasRecht(452);            // Recht f�r die Lieferantenartikel
    $Recht451 = $AWISBenutzer->HatDasRecht(451);            // Recht f�r die Artikelinhalte
    $Recht450 = $AWISBenutzer->HatDasRecht(450);            // Recht f�r die Basisfunktionen
    if ($Recht450 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    //***********************************************
    //* Standardwerte setzen
    //***********************************************

    $AWIS_KEY1 = -1;        // Artikelstamm-Key, der angezeigt werden soll

    $AIT_VK['IT'] = 53;    // Artikelstamminfos fuer die Preise (ladesabhaengig!)
    $AIT_VK['NL'] = 52;
    $AIT_VK['CH'] = 54;
    $AIT_VK['CZ'] = 51;
    $AIT_VK['AT'] = 50;

    $SQL = 'SELECT * FROM laender WHERE lan_code = \'' . $BenutzerSprache . '\'';
    $rsUSERLAND = $DB->RecordSetOeffnen($SQL);

    $AWISWerkzeug = new awisWerkzeuge();
    //***********************************************
    //* Aktionen auswerten
    //***********************************************

    // Datensatz speichern oder eine Zuordnung speichern
    $Zuordnungen = $Form->NameInArray($_POST, 'cmdZuordnen_');

    if ((isset($_POST['sucASTATUNR']) AND $_POST['sucASTATUNR'] != ''))    // Schellsuche-Feld
    {
        if ($_POST['sucASTATUNR'] != '') {
            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = strtoupper($_POST['sucASTATUNR']);

            $SQL = 'SELECT AST_KEY FROM Artikelstamm WHERE AST_ATUNR = :var_T_ast_atunr';
            $rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
            if (!$rsAST->EOF()) {
                $AWIS_KEY1 = $rsAST->FeldInhalt('AST_KEY');
            }
        }
    } elseif (isset($_POST['cmdSuche_x']) OR (!isset($_POST['cmdDSNeu_x']) AND ((isset($_POST['sucAST_ATUNR']) AND $_POST['sucAST_ATUNR'] != ''))))    // Neue Suche gestartet
    {
        $Param = array_fill(0, 20, '');

        $Param[0] = isset($_POST['sucAuswahlSpeichern'])?$_POST['sucAuswahlSpeichern']:'off';

        $Param[1] = (isset($_POST['sucAST_ATUNR'])?trim($_POST['sucAST_ATUNR']):'');
        $Param[2] = isset($_POST['sucAST_BEZEICHNUNG'])?trim($_POST['sucAST_BEZEICHNUNG']):'';
        $Param[3] = isset($_POST['sucKOMMENTAR'])?trim($_POST['sucKOMMENTAR']):'';
        $Param[4] = isset($_POST['sucWARENGRUPPE'])?$_POST['sucWARENGRUPPE']:'-1';
        $Param[5] = isset($_POST['sucAST_WUG_KEY'])?$_POST['sucAST_WUG_KEY']:'-1';

        $Param[6] = isset($_POST['sucNummer'])?trim($_POST['sucNummer']):'';
        $Param[7] = isset($_POST['sucExakteSuche'])?$_POST['sucExakteSuche']:'';
        $Param[8] = isset($_POST['sucEANNummer'])?$_POST['sucEANNummer']:'';
        $Param[9] = isset($_POST['sucGebrauchsNummer'])?$_POST['sucGebrauchsNummer']:'';
        $Param[10] = isset($_POST['sucLieferantenArtikelNummer'])?$_POST['sucLieferantenArtikelNummer']:'';
        $Param[11] = isset($_POST['sucLieferantenSet'])?$_POST['sucLieferantenSet']:'';
        $Param[12] = isset($_POST['sucOENummer'])?$_POST['sucOENummer']:'';
        $Param[13] = isset($_POST['sucBeliebigeNummer'])?$_POST['sucBeliebigeNummer']:'';
        $Param[14] = isset($_POST['sucOEPreiseZeigen'])?$_POST['sucOEPreiseZeigen']:'';
        $Param[15] = isset($_POST['sucFremdkaufZeigen'])?$_POST['sucFremdkaufZeigen']:'';
        $Param[16] = isset($_POST['sucFIL_ID'])?$_POST['sucFIL_ID']:'';
        $Param[20] = isset($_POST['sucOnlineShopNummer'])?$_POST['sucOnlineShopNummer']:'';

        $AWISBenutzer->ParameterSchreiben('ArtikelStammSuche', implode(';', $Param));
        $AWISBenutzer->ParameterSchreiben('ArtikelStammSucheFiliale', $Param[16]);

        // Einen direkt suchen aund anspringen
        if (isset($_POST['sucAST_ATUNR']) AND $_POST['sucAST_ATUNR'] != '' AND !strpos($_POST['sucAST_ATUNR'], '*')) {
            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = strtoupper($_POST['sucAST_ATUNR']);

            $SQL = 'SELECT AST_KEY FROM Artikelstamm WHERE AST_ATUNR = :var_T_ast_atunr';

            $rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

            if ($rsAST->FeldInhalt('AST_KEY')) {
                $AWIS_KEY1 = $rsAST->FeldInhalt('AST_KEY');
            }
        }

        //Wenn kein Suchkriterium angegeben wurde --> Fehlermeldung
        if ($Param[1] == '' and $Param[2] == '' and $Param[3] == '' and $Param[4] == '-1' and $Param[5] == '-1' and $Param[6] == '') {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['AST']['txt_KeinSuchkriterium'], awisFormular::HINWEISTEXT_HINWEIS);
            $Form->ZeileEnde();

            $Form->SchaltflaechenStart();
            $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
            $Form->SchaltflaechenEnde();
            die();
        }
    } elseif (isset($_POST['cmdSpeichern_x']) OR $Zuordnungen != '' or isset($_POST['ico_add_Lart_x']) or isset($_POST['ico_add_OEN_x'])) {
        include './artikelstamm_speichern.php';
    } elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
        include './artikelstamm_loeschen.php';
    } elseif (isset($_GET['LartPopUpDel'])) {
        include './artikelstamm_Lieferantenartikel_loeschen.php';
    } elseif (isset($_GET['OENNummerPopUpDel'])) {
        include './artikelstamm_OENummern_loeschen.php';
    } elseif (isset($_POST['cmdLoeschenAbbrechen']))    // Wenn der Artikel nicht gel�scht werden soll
    {
        $AWIS_KEY1 = $_POST['txtASTKey'];
    } elseif (isset($_GET['Key']))            // AST_KEY wurde angegeben (alte Syntax)
    {
        $AWIS_KEY1 = floatval($_GET['Key']);
    } elseif (isset($_GET['AST_KEY']))            // AST_KEY wurde angegeben (neue Syntax)
    {
        $AWIS_KEY1 = floatval($_GET['AST_KEY']);
    } elseif (isset($_GET['AST_ATUNR']))            // AST_KEY wurde angegeben (neue Syntax)
    {
        $AWIS_KEY1 = 0;
        $BindeVariablen = array();
        $BindeVariablen['var_T_ast_atunr'] = strtoupper($_GET['AST_ATUNR']);

        $SQL = 'SELECT AST_KEY FROM Artikelstamm WHERE AST_ATUNR = :var_T_ast_atunr';
        $rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
        if (!$rsAST->EOF()) {
            $AWIS_KEY1 = $rsAST->FeldInhalt('AST_KEY');
        }
    } elseif (isset($_POST['cmdDSWeiter_x']))            // N�chster Datensatz
    {
        $AWIS_KEY1 = floatval($_POST['txtAST_KEY']);    // Letzter Datensatz

        $SQL = 'SELECT * FROM (SELECT ast_key FROM artikelstamm WHERE ast_atunr >' . $DB->FeldInhaltFormat('T', $_POST['txtAST_ATUNR'],
                                                                                                           false) . ' ORDER BY nlssort(ast_atunr,\'NLS_SORT=BINARY\')) WHERE ROWNUM = 1';
        $rsAST = $DB->RecordSetOeffnen($SQL);
        if (!$rsAST->EOF()) {
            $AWIS_KEY1 = $rsAST->FeldInhalt('AST_KEY');
        }
    } elseif (isset($_POST['cmdDSZurueck_x']))            // N�chster Datensatz
    {
        $AWIS_KEY1 = floatval($_POST['txtAST_KEY']);    // Letzter Datensatz

        $SQL = 'SELECT * FROM (SELECT ast_key FROM artikelstamm WHERE ast_atunr <' . $DB->FeldInhaltFormat('T', $_POST['txtAST_ATUNR'],
                                                                                                           false) . ' ORDER BY nlssort(ast_atunr,\'NLS_SORT=BINARY\') DESC) WHERE ROWNUM = 1';
        $rsAST = $DB->RecordSetOeffnen($SQL);
        if (!$rsAST->EOF()) {
            $AWIS_KEY1 = $rsAST->FeldInhalt('AST_KEY');
        }
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = 0;            // Neuen Artikel anlegen
    }

    // Beim Bl�tztern aktuelle Parameter laden
    if (!isset($_GET['Seite']) AND (isset($_GET['Block']) OR isset($_GET['Sort']))) {
        $Param = explode(';', $AWISBenutzer->ParameterLesen('ArtikelStammSuche'));
    }

    if (!isset($Param) AND $AWIS_KEY1 == -1)        // Keine Auswahl
    {
        if (!isset($_GET['Liste'])) {
            $AWIS_KEY1 = $AWISBenutzer->ParameterLesen('AktuellerAST');
        }
        $Param = explode(';', $AWISBenutzer->ParameterLesen('ArtikelStammSuche'));
        $Param[16] = '';
    }

    $Param[16] = $AWISBenutzer->ParameterLesen('ArtikelStammSucheFiliale');
    //Pr�fen, ob der angemeldete Benutzer eine Filiale ist
    $UserFilialen = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
    if (!isset($Param[16])) {
        if ($UserFilialen != '') {
            $Param[16] = $UserFilialen;
        }
    }
    $LAN_CODE = '';
    if (isset($Param[16]) and $Param[16] != '' and $Param[16] != '0') {

        $BindeVariablen = array();
        $BindeVariablen['var_N0_fil_id'] = $Param[16];

        //Land zu der eingegebenen Filiale ermitteln
        $SQL = 'SELECT *';
        $SQL .= ' FROM FILIALEN';
        $SQL .= ' INNER JOIN LAENDER ON LAN_WWSKENN = FIL_LAN_WWSKENN';
        $SQL .= ' WHERE FIL_ID = :var_N0_fil_id';

        $rsLAN = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
        if ($rsLAN->AnzahlDatensaetze() == 0) {
            $Form->Hinweistext('Ung�ltige Filiale!');
            die();
        } else {
            $LAN_CODE = $rsLAN->FeldInhalt('LAN_CODE');
        }
    }

    //*************************************************************************************
    // Daten suchen
    //*************************************************************************************
    $Bedingung = '';
    $SuchMethode = 1;            // 1= Suche nach ATU Artikel, 2=Suche nach OE, etc.

    // Falls ein einzelner Key angegeben wurde -> als Parameter behandeln, da Einschr�nkungen kommen k�nnen
    if ($AWIS_KEY1 != -1) {
        $Bedingung .= ' AND AST_KEY = 0' . floatval($AWIS_KEY1);
    } else {
        if (!isset($Param))            // Keine Parameter angegeben
        {
            $Param = array_fill(0, 20, '');
        } else {
            if ($Param[1] != '')        // ATU Nummer
            {
                $Bedingung .= ' AND AST_ATUNR ' . $DB->LikeOderIst(strtoupper($Param[1]));
            }
            if ($Param[2] != '')        // Bezeichnung
            {
                $Bedingung .= ' AND (COALESCE(UPPER(ASP_BEZEICHNUNG),UPPER(AST_BEZEICHNUNGWW)) ' . $DB->LIKEoderIST($Param[2], awisDatenbank::AWIS_LIKE_UPPER);
                $Bedingung .= ' OR UPPER(AST_BEZEICHNUNG) ' . $DB->LIKEoderIST($Param[2], awisDatenbank::AWIS_LIKE_UPPER);
                $Bedingung .= ')';
            }
            if ($Param[3] != '')        // Kommentar
            {
                $Kommentare = '110,111';

                $Bedingung .= ' AND EXISTS(';
                $Bedingung .= ' SELECT * FROM Artikelstamminfos WHERE ASI_AIT_ID IN (0' . $Kommentare . ') AND ASI_AST_ATUNR = ArtikelStamm.AST_ATUNR AND UPPER(ASI_WERT) ' . $DB->LIKEoderIST($Param[3],
                                                                                                                                                                                               awisDatenbank::AWIS_LIKE_UPPER) . '';
                $Bedingung .= ')';
            }
            if ($Param[4] != '-1')        // Warengruppe
            {
                $Bedingung .= ' AND WUG_WGR_ID = ' . $DB->FeldInhaltFormat('T', $Param[4]);
            }
            if ($Param[5] != '-1')        // Warenuntergruppe
            {
                $Bedingung .= ' AND AST_WUG_KEY = ' . $DB->FeldInhaltFormat('N0', $Param[5]);
            }

            if ($Param[6] != '') {
                $SuchMethode = 2;        // Suche nach einer sonstigen Nummer
                $Bedingung = '';        // Alle Bedingungen von oben l�schen
            }
        }    // Ende: Gibt es Parameter?
    }

    //*********************************************
    //* Sonderrechte und Einschr�nkungen beachten
    //*********************************************
    if (($Recht451 & 1) == 0)    // Nur Artikel aus der Warenwirtschaft anzeigen
    {
        $Bedingung .= " AND BITAND(AST_IMQ_ID,2)=2";
    }
    if (($Recht452 & 16) == 0) // Sonderartikel anzeigen
    {
        $Bedingung .= " AND AST_SUCH2 IS NULL";
    }

    //$Form->DebugAusgabe(1,$_POST,$Bedingung,$AWIS_KEY1,$SQL);

    $EditModus = ($Recht450 & 6);        // Bearbeitungsmodus f�r den Artikel

    echo '<form name=frmArtikelstamm method=post action=./artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . '>';

    //*************************************************
    if ($AWIS_KEY1 != -1)            // Einen Datensatz anzeigen
    {                            //*************************************************
        $DetailAnsicht = true;    // Schaltfl�chen zum Speichern, etc. anzeigen

        $SQL = 'SELECT Artikelstamm.*, WGR_ID, WGR_BEZEICHNUNG, WUG_ID, WUG_BEZEICHNUNG ';
        $SQL .= ', (SELECT tei_key FROM Teileinfos WHERE (tei_key1=AST_KEY OR tei_key2=AST_KEY) AND ROWNUM<=1) AS AnzTEI';
        $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS ASP_BEZEICHNUNGWW';
        $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
        if ($LAN_CODE != '') {
            if ($LAN_CODE == 'DE') {
                $SQL .= ', AST_VK';
            } else {
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$LAN_CODE] . ') AS AST_VK';
            }
        } else {
            if ($BenutzerSprache == 'DE') {
                $SQL .= ', AST_VK';
            } else {
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND ASI_ait_id=' . $AIT_VK[$BenutzerSprache] . ') AS AST_VK';
            }
        }
        $SQL .= ' FROM Artikelstamm';
        $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';
        $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
        $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
        $SQL .= ' WHERE ' . substr($Bedingung, 4);

        $rsAST = $DB->RecordSetOeffnen($SQL);
        $Form->DebugAusgabe(1, $SQL);
        $ArtikelLoeschbar = ($EditModus AND ($rsAST->FeldInhalt('ANZTEI') . '') == '' AND (floatval($rsAST->FeldInhalt('AST_IMQ_ID')) & 6) == 4);

        if ($EditModus) {
            $AWISCursorPosition = 'txtAST_BEZEICHNUNG';
        }
        // Schaltflaechen OBEN und UNTEN anbieten
        if ($IconPosition == '1') {
            $Form->SchaltflaechenStart();

            $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

            $Recht453 = $AWISBenutzer->HatDasRecht(453);            // Recht f�r die Kommentare bearbeiten

            $EditModusKommentare = false;
            if ((isset($_GET['Seite']) AND $_GET['Seite'] == 'Kommentare') AND isset($_GET['ASIKEY']) AND ((($Recht453 & 4) == 4) OR (($Recht453 & 32) == 32) OR (($Recht453 & 256) == 256))) {
                $EditModusKommentare = true;
            }

            $EditModusLieferantenArtikel = false;
            $Recht452 = $AWISBenutzer->HatDasRecht(452);            // Recht f�r die Lieferantenartikel bearbeiten
            if ((isset($_GET['Seite']) AND $_GET['Seite'] == 'Lieferantenartikel' AND isset($_GET['LAR_KEY']) AND (($Recht452 & 4096) == 4096))) {
                $EditModusLieferantenArtikel = true;
            }

            $Recht407 = $AWISBenutzer->HatDasRecht(407);            // Recht f�r die Pr�fungen bearbeiten

            $EditModusPruefungen = false;
            $AnzPruefungen = 0;
            if ((isset($_GET['Seite']) AND $_GET['Seite'] == 'Pruefungen')) {
                $BindeVariablen = array();
                $BindeVariablen['var_N0_ast_key'] = '0' . $AWIS_KEY1;
                $SQL = 'SELECT *';
                $SQL .= ' FROM ARTIKELPRUEFUNGEN ';
                $SQL .= ' WHERE APR_AST_ATUNR = (SELECT AST_ATUNR FROM ARTIKELSTAMM WHERE AST_KEY = :var_N0_ast_key)';

                $rsPruefuengen = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                $AnzPruefungen = $rsPruefuengen->AnzahlDatensaetze();
                if (($Recht407 > 3) and (isset($_GET['APR_KEY']) or $AnzPruefungen > 0) and !isset($_GET['APRListe'])) {
                    $EditModusPruefungen = true;
                }
            }

            if (($EditModus or $EditModusKommentare or $EditModusPruefungen or $EditModusLieferantenArtikel) AND $DetailAnsicht) {
                $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
            }

            if (($Recht450 & 4) == 4)        // Bearbeitungsmodus f�r den Artikel
            {
                $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
            }

            if ($DetailAnsicht) {
                $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
                $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');

                if ($ArtikelLoeschbar) {
                    $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
                }
            }

            if (isset($_GET['Seite']) AND $_GET['Seite'] == 'Pruefungen' AND ($AWIS_KEY2 != '' OR isset($_GET['APR_KEY']) or $AnzPruefungen > 0) AND !isset($_GET['APRListe'])) {
                $Form->Schaltflaeche('href', 'cmdDruckenPruefungen', './artikelstamm_drucken_Pruefungen.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'],
                                     '');
            }

            $Form->Schaltflaeche('script', 'cmdHilfe',
                                 "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=artikelinfo&Seite=" . (isset($_GET['Seite'])?$_GET['Seite']:'Lieferantenartikel') . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');",
                                 '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

            // Suchfeld f�r die ATU Nummer
            if ($SchnellSucheFeld) {
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txt_Schnellsuche'] . ':', 150, 'text-align:right', '', $AWISSprachKonserven['AST']['ttt_Schnellsuche']);
                $Form->Erstelle_TextFeld('*ASTATUNR', '', 10, 0, true, '', '', '', 'T', 'L', '', '', 8, '', 'f');
                $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
            }
            $Form->SchaltflaechenEnde();
        }
        $Form->Formular_Start();

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo" . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'') . "" . (isset($_GET['Sort'])?'&Sort=' . $_GET['Sort']:'') . "&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAST->FeldInhalt('AST_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAST->FeldInhalt('AST_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $rsAST->FeldInhalt('AST_KEY'), false);
        echo '<input type=hidden name=txtAST_KEY value=' . $AWIS_KEY1 . '>';
        echo '<input type=hidden name=txtAST_IMQ_ID value=' . ($rsAST->FeldInhalt('AST_IMQ_ID') != ''?floatval($rsAST->FeldInhalt('AST_IMQ_ID') | 4):4) . '>';
        echo '<input type=hidden name=oldAST_IMQ_ID value=' . ($rsAST->FeldInhalt('AST_IMQ_ID') != ''?$rsAST->FeldInhalt('AST_IMQ_ID'):0) . '>';
        $AWISBenutzer->ParameterSchreiben('AktuellerAST', $AWIS_KEY1);

        // Zeile 1
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ATUNR'] . ':', 150);
        $EditATUNR = false;
        if ($rsAST->FeldInhalt('AST_ATUNR') == '' OR substr($rsAST->FeldInhalt('AST_ATUNR'), 0, 1) == '@' OR substr($rsAST->FeldInhalt('AST_ATUNR'), 0,
                                                                                                                    1) == '�' OR $rsAST->FeldInhalt('AST_IMQ_ID') == 4
        ) {
            $KatalogArtikel = true;
            $EditATUNR = $EditModus;        // Nur neue oder manuelle ATU Nummern d�rfen bearbeitet werden
            $AWISCursorPosition = 'txtAST_ATUNR';
            if ($rsAST->FeldInhalt('AST_ATUNR') == '' AND $EditATUNR) {
                $AWISCursorPosition = 'txtAST_ATUNR_ZEICHEN';
                $Default = '@';
                if (isset($_GET['AST_ATUNR'])) {
                    $Default = '';
                }
                $Form->Erstelle_SelectFeld('AST_ATUNR_ZEICHEN', $Default, 50, true, '', '', '', '', '', array('@~@', '�~�', '~'));
            }
            //�ber Werkzeuge->FreieNummern gekommen? Dann ausgew�hlte ATU-NR Vorblenden.
            if (isset($_GET['AST_ATUNR'])) {
                $ATUNr = $_GET['AST_ATUNR'];
            } else {
                $ATUNr = $rsAST->FeldInhalt('AST_ATUNR');
            }
            $Form->Erstelle_TextFeld('AST_ATUNR', $ATUNr, 10, 100, $EditATUNR, '', '', '', 'T', 'L', '', '', 8);

        } else {
            echo '<input name=txtAST_ATUNR type=hidden value=' . $rsAST->FeldInhalt('AST_ATUNR') . '>';
            $Form->Erstelle_TextFeld('AST_ATUNR', $rsAST->FeldInhalt('AST_ATUNR'), 10, 100, $EditATUNR, ($rsAST->FeldInhalt('AST_USER') == 'XXX'?'color:#FF0000':''), '', '', 'T',
                                     'L', '', '', 8);
        }

        //******************************************************************************************************************
        // Wenn das ATU-NR Feld nicht bearbeitbar ist, ein HiddenFeld um speichern zu erm�glichen
        // 11.11.2014, SK
        //******************************************************************************************************************
        if ($EditATUNR === false) {
            $Form->Erstelle_HiddenFeld('AST_ATUNR', $rsAST->FeldInhalt('AST_ATUNR'));
        }

        if ($rsAST->FeldInhalt('AST_ATUNR') != '') {
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'] . ':', 160);
            $Loeschhinweis = '';        // Bei aus dem WWS gel�schten Artikel das Datum anzeigen
            $StyleGeloescht = '';
            if ($rsAST->FeldInhalt('LOESCHDATUM') != '') {
                $StyleGeloescht = ';color:#FF0000';
                $Loeschhinweis = str_replace('$1', $rsAST->FeldInhalt('LOESCHDATUM'), $AWISSprachKonserven['AST']['wrd_AusWWGeloescht']);
            }
            $Text = $rsAST->FeldInhalt('AST_BEZEICHNUNGWW');
            $ToolTipp = $Text;
            $Format = '';
            if ($Text == '') {
                $Text = $rsAST->FeldInhalt('AST_BEZEICHNUNG');
                $ToolTipp = $Text;
                $Format = 'font-style:italic' . $StyleGeloescht;
            }
            $Form->Erstelle_TextFeld('*AST_BEZEICHNUNGWW', ($Loeschhinweis != ''?$Loeschhinweis:$Text), 30, $FeldBreiten['AST_BEZEICHNUNGWW'], false, $StyleGeloescht, '', '', 'T',
                                     'L', ($Loeschhinweis != $ToolTipp?$Text:''), '', 35);

            $WGR_LL = '';
            $WUG_LL = '';

            //Noch keine WGR von WaWi? Dann kann ein Vorschlag gemacht werden (Artikelstamminfos)
            if($rsAST->FeldInhalt('WGR_ID')=='' or $rsAST->FeldInhalt('WGR_ID')=='00'){

                if(isset($_POST['cmdSpeichern_x'])){

                    $DebugSQL  ='   select ASI_WERT';
                    $DebugSQL .='   from ARTIKELSTAMMINFOS';
                    $DebugSQL .='   where ASI_AIT_ID  = 400' ;
                    $DebugSQL .='   and ASI_AST_ATUNR = '. $DB->WertSetzen('WUG','T',$rsAST->FeldInhalt('AST_ATUNR'));

                    if($DB->ErmittleZeilenAnzahl($DebugSQL,$DB->Bindevariablen('WUG')) > 1){
                        foreach($_REQUEST as $item => $value){
                            $Debug[utf8_encode($item)] = utf8_encode($value);
                        }
                        $Fehlercode = time();
                        $AWISWerkzeug->EMail(['shuttle@de.atu.eu'],'Artikelstamminfofehler: ' . $Fehlercode,$AWISBenutzer->BenutzerName() .PHP_EOL . json_encode($Debug));
                        $Mail = '<a href=mailto:shuttle@de.atu.eu?Subject='.urlencode('Artikelstammfehler: ' . $Fehlercode ).'&Body=' . urlencode($Meldung);
                        $Mail .= '>Fehler Melden</a>';
                        $Form->PopupDialog('Fehler erzeugt','Beim Speichern ist ein Fehler aufgetreten, welcher zur Sperrung des Artikels gef�hrt h�tte. Die Daten wurden trotzdem gespeichert! <br>
                            Bitte melden Sie diesen Fehler und beschreiben Sie kurz, was Sie Getan haben, bevor diese Meldung erschien. <br><br>Vielen Dank<br><br> Ihr AWIS-Team' .  $Mail,[['/bilder/cmd_dsloeschen.png','close','close','close']],awisFormular::POPUP_FEHLER,'555');
                        $Form->PopupDialogAutoload('555');
                    }

                }


                $SQL  ='select';
                $SQL .='   (select ASI_WERT';
                $SQL .='   from ARTIKELSTAMMINFOS';
                $SQL .='   where ASI_AIT_ID  = 400' ;
                $SQL .='   and ASI_AST_ATUNR = '. $DB->WertSetzen('WUG','T',$rsAST->FeldInhalt('AST_ATUNR'));
                $SQL .='   and rownum = 1) as ASI_WGR_ID ,';
                $SQL .='   (select ASI_WERT';
                $SQL .='   from ARTIKELSTAMMINFOS';
                $SQL .='   where ASI_AIT_ID  = 401';
                $SQL .='   and ASI_AST_ATUNR = '. $DB->WertSetzen('WUG','T',$rsAST->FeldInhalt('AST_ATUNR'));
                $SQL .='   and rownum = 1) as ASI_WUG_ID';
                $SQL .=' from DUAL';

                $rsASI = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('WUG',false));
                $WGR_LL = $rsASI->FeldInhalt('ASI_WGR_ID');
                $WUG_LL = $rsASI->FeldInhalt('ASI_WUG_ID');
                $Form->Erstelle_TextLabel($AWISSprachKonserven['WGR']['WGR_ID'] . ':', 50);
                $Form->Erstelle_TextFeld('WGR_ID_Vorschlag',$WGR_LL , 10, 130, $EditModus, '', '', '', 'T', 'L','','03');

                $Form->Erstelle_TextLabel($AWISSprachKonserven['WUG']['WUG_ID'] . ':', 50);
                $Form->Erstelle_TextFeld('WUG_ID_Vorschlag', $WUG_LL, 10, 40, $EditModus, '', '', '', 'T', 'L');
            }else{
                $Form->Erstelle_TextLabel($AWISSprachKonserven['WGR']['WGR_ID'] . ':', 50);
                $Form->Erstelle_TextFeld('*WGR_ID', $rsAST->FeldInhalt('WGR_ID'), 10, 30, false, '', '', '', 'T', 'L', $rsAST->FeldInhalt('WGR_BEZEICHNUNG'));

                $Form->Erstelle_TextLabel($AWISSprachKonserven['WUG']['WUG_ID'] . ':', 50);
                $Form->Erstelle_TextFeld('*WUG_ID', $rsAST->FeldInhalt('WUG_ID'), 10, 40, false, '', '', '', 'T', 'L', $rsAST->FeldInhalt('WUG_BEZEICHNUNG'));

                $Form->Erstelle_HiddenFeld('WGR_ID_Vorschlag', $rsAST->FeldInhalt('WGR_ID'));
                $Form->Erstelle_HiddenFeld('WUG_ID_Vorschlag', $rsAST->FeldInhalt('WUG_ID'));
                $WGR_LL = $rsAST->FeldInhalt('WGR_ID');
                $WUG_LL = $rsAST->FeldInhalt('WUG_ID');
            }


            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_KENNUNG'] . ':', 70);
            $Form->Erstelle_TextFeld('*AST_KENNUNG', $rsAST->FeldInhalt('AST_KENNUNG'), 10, 30, false, '', '', '', 'T', 'L');

            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 70, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '');    // KENN2
            $WertL = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 71, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '');    // KENN2_L
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_KENNUNG2'] . ':', 70, '', '', $AWISSprachKonserven['AST']['ttt_AST_KENN2']);
            $Form->Erstelle_TextFeld('*AST_KENN2', ($Wert != '' || $WertL != ''?$Wert . '/' . $WertL:''), 10, 30, false, '', '', '', 'T', 'L');
        }
        $Form->ZeileEnde();

        if ($rsAST->FeldInhalt('AST_ATUNR') != '') {
            //***************************
            // Zeile 2 wenn Reifen
            //***************************
            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = $rsAST->FeldInhalt('AST_ATUNR');

            // Reifeninfos WETGRIP, ROLLRES, NOISECL, NOISEPE laden
            $SQL = 'SELECT ROLLRES, WETGRIP, NOISECL, NOISEPE';
            $SQL .= ' FROM V_REIFENLABEL';
            $SQL .= ' WHERE AST_ATUNR=:var_T_ast_atunr';

            $rsRIN = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

            if (!$rsRIN->EOF()) {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['RIN']['txt_ROLLRES'] . ':', 150);
                $Form->Erstelle_TextFeld('*ROLLRES', $rsRIN->FeldInhalt('ROLLRES'), 2, 100, false, '', '', '', 'T', 'L');
                $Form->Erstelle_TextLabel($AWISSprachKonserven['RIN']['txt_NOISECL'] . ':', 160);
                $Form->Erstelle_TextFeld('*NOISECL', $rsRIN->FeldInhalt('NOISECL'), 2, 100, false, '', '', '', 'T', 'L');
                $Form->ZeileEnde();
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['RIN']['txt_WETGRIP'] . ':', 150);
                $Form->Erstelle_TextFeld('*WETGRIP', $rsRIN->FeldInhalt('WETGRIP'), 2, 100, false, '', '', '', 'T', 'L');
                $Form->Erstelle_TextLabel($AWISSprachKonserven['RIN']['txt_NOISEPE'] . ':', 160);
                $Form->Erstelle_TextFeld('*NOISEPE', $rsRIN->FeldInhalt('NOISEPE'), 2, 100, false, '', '', '', 'T', 'L');
                $Form->ZeileEnde();
            }
        }

        if ($rsAST->FeldInhalt('AST_ATUNR') != '') {
            //***************************
            // Zeile 2 wenn keine Reifen, sonst Zeile 3 ;-)
            //***************************
            $Form->ZeileStart();

            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = $rsAST->FeldInhalt('AST_ATUNR');

            // Fahrzeuge
            $SQL = "SELECT HERSTELLER FROM V_ARTIKEL_FAHRZEUGE ";
            $SQL .= " WHERE ATU_NR=:var_T_ast_atunr";
            $SQL .= " ORDER BY 1";
            $Form->DebugAusgabe(1, $SQL);
            $rsKFZ = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
            $Fahrzeuge = '';
            while (!$rsKFZ->EOF()) {
                $Fahrzeuge .= ', ' . substr($rsKFZ->FeldInhalt('HERSTELLER'), 0, 4);
                $rsKFZ->DSWeiter();
            }
            unset($rsKFZ);

            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtFahrzeuge'] . ':', 150);
            $Form->Erstelle_TextFeld('*FAHRZEUGE', substr($Fahrzeuge, 2), 10, 1000, false, '', '', '', 'T', 'L');

            $Form->ZeileEnde();

            $Form->ZeileStart();

            //VK, je nach Filiale
            $VK = $rsAST->FeldInhalt('AST_VK');

            if ($Param[16] != "") {

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsAST->FeldInhalt('AST_ATUNR');
                $BindeVariablen['var_N0_fil_id'] = $Param[16];

                //Filialindividueller VK
                $rsVKFil = $DB->RecordSetOeffnen("SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=:var_T_ast_atunr AND FIB_FIL_ID=:var_N0_fil_id", $BindeVariablen);
                $rsVKAnz = $rsVKFil->AnzahlDatensaetze();

                if ($rsVKAnz > 0) {
                    $VK = $rsVKFil->FeldInhalt('FIB_VK');
                }
            }

            // VK, je nach Land bzw. Filiale
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'] . ':', 150);

            if ($LAN_CODE != '') {
                $Form->Erstelle_TextFeld('*AST_VK', $Form->Format('N2', $VK, $BenutzerSprache) . ' ' . $rsLAN->FeldInhalt('LAN_WAE_CODE'), 10, 110, false, '', '',
                                         '', 'T', 'L');
            } else {
                $Form->Erstelle_TextFeld('*AST_VK', $Form->Format('N2', $VK, $BenutzerSprache) . ' ' . $rsUSERLAND->FeldInhalt('LAN_WAE_CODE'), 10, 110, false, '',
                                         '', '', 'T', 'L');
            }

            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 190, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);

            // Altteilwert
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtAltteilwert'] . ':', 130);
            $SQL = 'SELECT ATW_BETRAG';
            $SQL .= ' FROM Altteilwerte';
            $SQL .= ' WHERE ATW_KENNUNG=\'' . $Wert[0]['Wert'] . '\'';
            if ($LAN_CODE != '') {
                $SQL .= ' AND ATW_LAN_CODE=\'' . $rsLAN->FeldInhalt('LAN_WAE_CODE') . '\'';
            } else {
                $SQL .= ' AND ATW_LAN_CODE=\'' . $BenutzerSprache . '\'';
            }
            $rsATW = $DB->RecordSetOeffnen($SQL);
            if ($rsATW->FeldInhalt('ATW_BETRAG') != '') {
                $Wert = $rsATW->FeldInhalt('ATW_BETRAG');
            } else {
                $Wert = 0;
            }

            if ($LAN_CODE != '') {
                $Form->Erstelle_TextFeld('*Altteilwert', $Form->Format('N2', $Wert, $rsLAN->FeldInhalt('LAN_CODE')) . ' ' . $rsLAN->FeldInhalt('LAN_WAE_CODE'), 10, 80, false, '',
                                         '', '', 'T', 'L');
            } else {
                $Form->Erstelle_TextFeld('*Altteilwert', $Form->Format('N2', $Wert, $BenutzerSprache) . ' ' . $rsUSERLAND->FeldInhalt('LAN_WAE_CODE'), 10, 80,
                                         false, '', '', '', 'T', 'L');
            }
            $Form->Erstelle_TextFeld('*Fueller1', '', 0, 20, false);

            // Hauptlieferant
            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 40, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtHauptlieferant'] . ':', 140);
            $Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $Wert[0]['Wert'];
            $Form->Erstelle_TextFeld('*Lieferant', $Wert[0]['Wert'], 10, 90, false, '', '', $Link, 'T', 'L');

            // Lieferantenartikelnummer
            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 41, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtLiefArtNr'] . ':', 150);
            $Link = '';
            $Form->Erstelle_TextFeld('*LiefArtNr', $Wert[0]['Wert'], 10, 0, false, '', '', $Link, 'T', 'L');

            $Form->ZeileEnde();

            $Form->ZeileStart();

            // Zeile 6 -> Abs�tze / Reklamationen / Bestand
            if ($Recht451 & 8) {
                $AbsWeiden = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 30, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Absatz Weiden
                $AbsWerl = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 31, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Absatz Werl
                $ToolTipp = 'Weiden: ' . $AbsWeiden . ' / Werl: ' . $AbsWerl;
                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 38, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Absatz Gesamt
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtAbsatzLfdJahr'] . ':', 150);
                $Form->Erstelle_TextFeld('*AbsatzLfd', $Wert, 10, 110, false, '', '', '', 'N0', 'L', $ToolTipp);
            }
            if ($Recht451 & 16) {
                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 130, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Reklamationen lfd. Jahr
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtReklamationenLfdJahr'] . ':', 130);
                $Form->Erstelle_TextFeld('*RelLfdJahr', ($Wert == ''?0:$Wert), 10, 100, false, '', '', '', 'N0', 'L', '');
            }
            if ($Recht451 & 2) {
                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 10, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Bestand Weiden
                $WertRes = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 13, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Bestand Weiden
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtBestandWeiden'] . ':', 140);
                $BestN = ($Wert == ''?0:$Wert - ($WertRes == ''?0:$WertRes));
                $Form->Erstelle_TextFeld('*BestandN', ($BestN == 0?'0':$BestN), 10, 90, false, '', '', '', 'NO', 'L', '');

                if ($Recht451 & 256) {
                    // R�ckstand Weiden
                    $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 76, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtBestellrueckstandWeiden'] . ':', 150, '', '',
                                              $AWISSprachKonserven['AST']['tttBestellrueckstandWeiden']);
                    $Form->Erstelle_TextFeld('*RueckstandN', ($Wert == ''?0:$Wert), 10, 100, false, '', '', '', 'N0', 'L', '');
                }

                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 12, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Bestand Filialen

                $Link = '../ATUArtikel/filialbestand.php?ATUNR=' . $rsAST->FeldInhalt("AST_ATUNR") . '&NurBestand=1&Zurueck=ArtikelInfosNeu&ASTKEY=' . $rsAST->FeldInhalt("AST_KEY");

                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtBestandFilialen'] . ':', 150);
                $BestF = ($Wert == ''?0:$Wert);

                $ToolTipp = '';
                if ($Recht451 & 128) {
                    $BindeVariablen = array();
                    $BindeVariablen['var_T_ast_atunr'] = $rsAST->FeldInhalt("AST_ATUNR");
                    $SQL = 'SELECT SUM(FIB_BESTAND) as Bestand, DECODE(FIL_LAGERKZ,\'N\',\'Weiden\',\'L\',\'Werl\',\'\') as LAGER';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " INNER JOIN FILIALEN ON FIL_ID = FIB_FIL_ID AND FIL_LAGERKZ IN ('L','N')";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = :var_T_ast_atunr";
                    $SQL .= " GROUP BY FIL_LAGERKZ ";
                    $rsFIB = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

                    $ToolTipp = '';
                    while (!$rsFIB->EOF()) {
                        $ToolTipp .= ' / ' . $rsFIB->FeldInhalt('LAGER') . ': ' . $rsFIB->FeldInhalt('BESTAND');
                        $rsFIB->DSWeiter();
                    }
                }

                $Form->Erstelle_TextFeld('*BestandF', ($BestF == 0?'0':$BestF), 10, 0, false, '', '', $Link, 'N0', 'L', substr($ToolTipp, 3));
            }

            $Form->ZeileEnde();

            $Form->ZeileStart();

            // Zeile 6 -> Abs�tze
            if ($Recht451 & 8) {
                $AbsWeiden = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 33, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Absatz Vorjahr Weiden
                $AbsWerl = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 34, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Absatz Vorjahr Werl
                $ToolTipp = 'Weiden: ' . $AbsWeiden . ' / Werl: ' . $AbsWerl;
                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 37, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Absatz Vorjahr Gesamt
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtAbsatzVorjahr'] . ':', 150);
                $Form->Erstelle_TextFeld('*AbsatzVorjahr', ($Wert == ''?0:$Wert), 10, 110, false, '', '', '', 'N0', 'L', $ToolTipp);
            }
            // Reklamationen
            if ($Recht451 & 16) {
                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 131, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Reklamationen lfd. Jahr
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtReklamationenVorJahr'] . ':', 130);
                $Form->Erstelle_TextFeld('*RelVorJahr', ($Wert == ''?0:$Wert), 10, 100, false, '', '', '', 'N0', 'L', '');
            }

            if ($Recht451 & 2) {
                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 11, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Bestand Werl
                $WertRes = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 14, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');    // Bestand Weiden
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtBestandWerl'] . ':', 140);
                $BestL = ($Wert == ''?0:$Wert - ($WertRes == ''?0:$WertRes));
                $Form->Erstelle_TextFeld('*BestandL', ($BestL == 0?'0':$BestL), 10, 90, false, '', '', '', 'N0', 'L', '');

                if ($Recht451 & 256) {
                    // R�ckstand Werl
                    $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 77, $rsAST->FeldInhalt('AST_ATUNR'), 'N0', '');
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtBestellrueckstandWerl'] . ':', 150, '', '', $AWISSprachKonserven['AST']['tttBestellrueckstandWerl']);
                    $Form->Erstelle_TextFeld('*RueckstandL', ($Wert == ''?0:$Wert), 10, 100, false, '', '', '', 'N0', 'L', '');
                }

                if ($BenutzerFilialen != '') {
                    $BindeVariablen = array();
                    $BindeVariablen['var_T_ast_atunr'] = $rsAST->FeldInhalt("AST_ATUNR");
                    $FilCount = count(explode(',', $BenutzerFilialen));

                    $SQL = 'SELECT FIB_BESTAND, FIB_FIL_ID';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = :var_T_ast_atunr";
                    if ($FilCount === 1) {
                        $BindeVariablen['var_N0_fil_id'] = $BenutzerFilialen;
                        $SQL .= " AND FIB_FIL_ID = :var_N0_fil_id";
                    } else {
                        $SQL .= " AND FIB_FIL_ID IN(" . $BenutzerFilialen . ")";
                    }
                    $rsFIB = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

                    $Anzeige = '';
                    $ToolTipp = $AWISSprachKonserven['Wort']['wrd_Filiale'] . ' ';
                    while (!$rsFIB->EOF()) {
                        $Anzeige .= '/' . $rsFIB->FeldInhalt('FIB_BESTAND');
                        $ToolTipp .= $rsFIB->FeldInhalt('FIB_FIL_ID');
                        $rsFIB->DSWeiter();
                    }
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtBestandEigen'] . ':', 150);
                    $Form->Erstelle_TextFeld('*BestandEigen', substr($Anzeige, 1), 0, 0, false, '', '', '', 'T', 'L', $ToolTipp);
                }
            }

            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Trennzeile();
            $Form->ZeileEnde();
        }    // Nicht bei neuen ATU Artikeln

        //*****************************************
        // Kataloginformationen
        //*****************************************
        if ($Recht451 & 1) {
            // Zeile 3
            $Form->ZeileStart();

            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNG'] . ':', 150);
            $Form->Erstelle_TextFeld('AST_BEZEICHNUNG', $rsAST->FeldInhalt('AST_BEZEICHNUNG'), 60, 505, $EditModus, '', '', '', 'T', '', '', '', 100);

            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_LIEBLINGSLIEFERANT'] . ':', 65);

            $SQL  ='SELECT ';
            $SQL .='   WGI_LIE_NR,';
            $SQL .='   WGI_LIE_NR_WERTIGKEIT,';
            $SQL .='   WGI_LIE_NAME,';
            $SQL .='   ZUORD.LAR_LIE_NR as ZUGEORDNETE_LIE_NR';
            $SQL .=' FROM V_WARENGRUPPEN_LIE_NR';
            $SQL .=' LEFT JOIN (';
            $SQL .=' select distinct lar_lie_nr from teileinfos tei';
            $SQL .=' inner join LIEFERANTENARTIKEL lar';
            $SQL .=' on lar.lar_key = tei_key2';
            $SQL .=' where tei_key1 =  '  .$DB->WertSetzen('WGI','T',$AWIS_KEY1);
            $SQL .= ' and tei_ity_id1 = \'AST\' and tei_ity_id2 = \'LAR\') zuord';
            $SQL .=' on zuord.lar_lie_nr = wgi_lie_nr';
            $SQL .=' WHERE WGI_WGR_ID = ' .$DB->WertSetzen('WGI','T',$WGR_LL);
            $SQL .=' and (WGI_WUG_ID is null or WGI_WUG_ID = '  .$DB->WertSetzen('WGI','T',$WUG_LL);
            $SQL .=' ) ORDER BY WGI_LIE_NR ASC';
            $rsWGI = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('WGI'));
            while(!$rsWGI->EOF()){
                if($rsWGI->FeldInhalt('ZUGEORDNETE_LIE_NR')){ //Bereits ein LART mit diesem LIEF? Dann keinen Link zur Neuanlage
                    $Link = '';
                }else{
                    $Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&LAR_KEY=0&txtLAR_LIE_NR='.$rsWGI->FeldInhalt('WGI_LIE_NR');
                }
                $Form->Erstelle_TextLabel($rsWGI->FeldInhalt('WGI_LIE_NR'),40,'',$Link,$rsWGI->FeldInhalt('WGI_LIE_NAME'));
                $rsWGI->DSWeiter();
            }



            $Form->ZeileEnde();

            // Zeile 4

            $Form->ZeileStart();

            // Alternativartikel
            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 120, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', false);

            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtAlternativArtikel'] . ':', 150);
            // CA bzw. CB als weiteren Alternativartikel betrachten!
            $Breite = 154;
            if (substr($rsAST->FeldInhalt('AST_ATUNR'), 0, 2) == 'CA' OR substr($rsAST->FeldInhalt('AST_ATUNR'), 0, 2) == 'CB') {
                $SuchArt = (substr($rsAST->FeldInhalt('AST_ATUNR'), 0, 2) == 'CA'?'CB' . substr($rsAST->FeldInhalt('AST_ATUNR'), 2):'CA' . substr($rsAST->FeldInhalt('AST_ATUNR'),
                                                                                                                                                  2));

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $SuchArt;

                $rsCACB = $DB->RecordSetOeffnen("SELECT AST_KEY, AST_ATUNR FROM AWIS.ArtikelStamm WHERE AST_ATUNR =:var_T_ast_atunr", $BindeVariablen);
                if ($rsCACB->FeldInhalt('AST_ATUNR') != '') {
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=0' . $rsCACB->FeldInhalt('AST_KEY');
                    $Form->Erstelle_TextLabel('' . $rsCACB->FeldInhalt('AST_ATUNR') . '', 75, '', $Link, $AWISSprachKonserven['AST']['ttt_CACBArtikel']);
                    $Breite = 85;
                }
                unset($rsCACB);
            }

            $Form->Erstelle_TextFeld('ALTERNATIVARTIKEL', $Wert, ($Breite == 154?20:9), $Breite, $EditModus, '', '', '', 'T', 'L');

            // Zubeh�rliste
            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 121, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtZubehoer'] . ':', 150, 'text-align:right');
            $Form->Erstelle_TextFeld('ASI_121_' . $Wert[0]['Key'], $Wert[0]['Wert'], 20, 160, $EditModus, '', '', '', 'T', 'L');

            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 61, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtMusterVorhanden'] . ':', 160, 'text-align:right');
            $JaNeinFeld = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
            $Form->Erstelle_SelectFeld('ASI_61_' . $Wert[0]['Key'], $Wert[0]['Wert'], 60, $EditModus, '', '', '0', '', '', $JaNeinFeld);

            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_KENNUNGVORSCHLAG'] . ':', 160, 'text-align:right');
            $Form->Erstelle_TextFeld('AST_KENNUNGVORSCHLAG', $rsAST->FeldInhalt('AST_KENNUNGVORSCHLAG'), 2, 30, $EditModus, '', '', '', 'T', 'L');

            $Form->ZeileEnde();
        }

        // Zeile 5

        $Form->ZeileStart();

        // Verpackungseinheiten Katalog und WWS
        $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 60, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VPE'] . ':', 150, '', '', $AWISSprachKonserven['AST']['ttt_AST_VPE']);
        $Form->Erstelle_TextFeld('ASI_60_' . $Wert[0]['Key'], $Wert[0]['Wert'], 2, 50, $EditModus, '', '', '', 'T', 'L');

        $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 191, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', false);
        $Form->Erstelle_TextLabel((isset($Wert[0])?'/ ' . $Wert[0]:''), 114);

        if ($Recht451 & 4) {
            echo '<script>' . PHP_EOL;
            echo 'function aktuellesDatum(obj){ ' . PHP_EOL;
            echo 'var date = new Date();';
            echo ' var heute = new Date();';
            echo ' obj.value =  heute.getDate()+\'.\'+(\'0\' + (heute.getMonth()+1)).slice(-2)+\'.\' + heute.getFullYear(); ' . PHP_EOL;
            echo ' $(txtMeldungDurch).val( PP_WERT);';
            echo '}' . PHP_EOL;
            echo '</script>' . PHP_EOL;

            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 100, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtMeldungAnEinkauf'] . ':', 150, 'text-align:right');
            $Form->Erstelle_TextFeld('ASI_100_' . $Wert[0]['Key'], $Wert[0]['Wert'], 10, 160, $EditModus, '', '', '', 'D', 'L', '', '', '', 'ondblClick="aktuellesDatum(this);"',
                                     '', 'autocomplete="off"');
            $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 101, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);

            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txtMeldungDurch'] . ':', 160, 'text-align:right');
            if ($EditModus) {
                $SQL = 'SELECT APP_ID, APP_NAME FROM Artikelpruefpersonal';
                $SQL .= ' WHERE (app_id = 0' . ($Wert[0]['Wert'] == ''?'0':$Wert[0]['Wert']) . ' OR app_aktivbis > SYSDATE)';
                $SQL .= ' ORDER BY app_name';
                $OptWert = $Wert[0]['Wert'];

                //"Meldung durch" wurde noch nie gespeichert?
                if ($Wert[0]['Wert'] == 0) {
                    //Dann schauen, ob der angemeldete User ein ArtikelPruefer ist
                    $SQLVorschlag = ' SELECT APP_ID from Artikelpruefpersonal where APP_KON_KEY =  ' . $DB->WertSetzen('APP', 'Z', '0' . $AWISBenutzer->BenutzerKontaktKEY());
                    $OptWert = $DB->RecordSetOeffnen($SQLVorschlag, $DB->Bindevariablen('APP'))->FeldInhalt(1);
                    echo '<script>';
                    echo 'var PP_WERT = "' . $OptWert . '";' . PHP_EOL;
                    echo 'var txtMeldungDurch = "#txtASI_101_' . $Wert[0]['Key'] . '";' . PHP_EOL;
                    echo '</script>';
                }
                $Form->Erstelle_SelectFeld('ASI_101_' . $Wert[0]['Key'], $Wert[0]['Wert'], 0, $EditModus, $SQL, '0~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
            } else {
                $BindeVariablen = array();
                $BindeVariablen['var_N0_app_id'] = '0' . ($Wert[0]['Wert'] == ''?'0':$Wert[0]['Wert']);
                $SQL = 'SELECT APP_ID, APP_NAME FROM Artikelpruefpersonal';
                $SQL .= ' WHERE app_id = :var_N0_app_id';
                $SQL .= ' ORDER BY app_name';
                $rsHILF = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
                $Form->Erstelle_TextLabel($rsHILF->FeldInhalt('APP_NAME'), 160, '');
            }
        }
        $Form->ZeileEnde();


        if($AWIS_KEY1 > 0){
            $Form->ZeileStart();
            if ($AWISBenutzer->HatDasRecht(10060) > 0) {

                // Verpackungseinheiten Katalog und WWS
                $Wert = $AWISWerkzeug->InfoFeldwert($DB, 'AST', 300, $rsAST->FeldInhalt('AST_ATUNR'), 'T', '', true);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_PREISEINHEITZUK'] . ':', 150, '', '', $AWISSprachKonserven['AST']['ttt_AST_PREISEINHEITZUK']);
                $Form->Erstelle_TextFeld('ASI_300_' . $Wert[0]['Key'], $Wert[0]['Wert'], 2, 60, $EditModus, '', '', '', 'T', 'L');
            }
            $Form->Erstelle_TextLabel($Form->LadeTextBaustein('AAL', 'AAL_ALTERNATIV_AST_ATUNR') . ':', 150);
            $Alternativen = 'Select AAL_AST_ATUNR, listagg(AAL_ALTERNATIV_AST_ATUNR,\', \') within group(order by 1)  as Alternativen from ARTIKELALTERNATIVEN ';
            $Alternativen .= ' WHERE AAL_AST_ATUNR =  ' . $DB->WertSetzen('AAL', 'T', $rsAST->FeldInhalt('AST_ATUNR'), false);
            $Alternativen .= ' group by AAL_AST_ATUNR ';
            $Alternativen = $DB->RecordSetOeffnen($Alternativen, $DB->Bindevariablen('AAL', true));
            $Form->Erstelle_TextFeld('alternativen', $Alternativen->FeldInhalt(2), 2, 500, false, '', '', '', 'T', 'L');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($Form->LadeTextBaustein('AAL', 'AAL_ALTERNATIVE_VON') . ':', 150);

            $Alternativen = 'Select listagg(AAL_AST_ATUNR,\', \') within group(order by 1)  as Alternativen from ARTIKELALTERNATIVEN ';
            $Alternativen .= ' WHERE AAL_ALTERNATIV_AST_ATUNR =  ' . $DB->WertSetzen('AAL', 'T', $rsAST->FeldInhalt('AST_ATUNR'), false);
            $Alternativen .= ' group by AAL_ALTERNATIV_AST_ATUNR ';
            $Alternativen = $DB->RecordSetOeffnen($Alternativen, $DB->Bindevariablen('AAL', true));

            $Form->Erstelle_TextFeld('alternativen', $Alternativen->FeldInhalt(1), 2, 500, false, '', '', '', 'T', 'L');


            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_MITZUVERWENDEN'] . ':', 150);

            $Alternativen = 'Select listagg(asi_wert,\', \') within group(order by 1)  as mitzuverwenden from artikelstamminfos ';
            $Alternativen .= ' WHERE asi_ait_id = 410 and asi_ast_atunr =  ' . $DB->WertSetzen('AAL', 'T', $rsAST->FeldInhalt('AST_ATUNR'), false);
            $Alternativen .= ' group by asi_ast_atunr ';
            $Alternativen = $DB->RecordSetOeffnen($Alternativen, $DB->Bindevariablen('AAL', true));
            $Form->Erstelle_TextFeld('mitzuverwenden', $Alternativen->FeldInhalt(1), 2, 500, false, '', '', '', 'T', 'L');

            $Form->ZeileEnde();
        }


        $Form->Formular_Ende();

        if ($rsAST->FeldInhalt('AST_KEY') != '') {
            $Register = new awisRegister(451);
            $Register->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:'Lieferantenartikel'));
        }
    } else        // Listen anzeigen
    {
        //**********************************************************************
        // Liste anzeigen
        //**********************************************************************

        if ($SuchMethode == 1)            // Bei der Suche nach einer ATU-Nummer diese Daten anzeigen
        {
            $SQL = 'SELECT AST_KEY, AST_ATUNR,  AST_IMQ_ID, AST_BEZEICHNUNG ';
            $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
            $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW, WGR_ID';
            if ($LAN_CODE != '') {
                if ($LAN_CODE == 'DE') {
                    $SQL .= ', COALESCE(AST_VK,0) AS AST_VK';
                } else {
                    $SQL .= ',COALESCE((SELECT TO_NUMBER(ASI_WERT) FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$LAN_CODE] . '),0) AS AST_VK';
                }
            } else {
                if ($BenutzerSprache == 'DE') {
                    $SQL .= ', COALESCE(AST_VK,0) AS AST_VK';
                } else {
                    $SQL .= ',COALESCE((SELECT TO_NUMBER(ASI_WERT) FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$BenutzerSprache] . '),0) AS AST_VK';
                }
            }

            // Lagerbestand
            $SQL .= ',COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (10,11)),0) -';
            $SQL .= ' COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (13,14)),0) AS AST_GESBEST';
            // Gesamt-Filialbestand
            $SQL .= ',COALESCE(TO_NUMBER((SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=12)),0) AS AST_FILBESTD';

            if (($Recht451 & 2) == 2) {
                $SQL .= ',(SELECT SUM(FIB_BESTAND)AS FIB_BESTAND ';
                $SQL .= " FROM FilialBestand";
                $SQL .= " WHERE ";
                $SQL .= " FIB_AST_ATUNR = artikelstamm.ast_atunr";
                if ($BenutzerFilialen != '') {
                    $SQL .= " AND FIB_FIL_ID IN(0" . $BenutzerFilialen . ")";
                }
                $SQL .= " ) AS FIB_BESTAND";
            }
            $SQL .= ',(select  listagg(HERSTELLER,\',\')  within group (order by HERSTELLER)  from V_ARTIKEL_FAHRZEUGE fzg WHERE fzg.ATU_NR = AST_ATUNR group by ATU_NR) as HERSTELLER';
            $SQL .= ' FROM Artikelstamm';
            $SQL .= ' INNER JOIN WARENUNTERGRUPPEN ON ast_wug_key = wug_key';
            $SQL .= ' INNER JOIN WARENGRUPPEN ON wug_wgr_id = wgr_id';
            $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';

            if ($Bedingung != '') {
                $SQL .= ' WHERE ' . substr($Bedingung, 4);
            }

            if (isset($_GET['Sort'])) {
                $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
            } else {
                $SQL .= ' ORDER BY AST_ATUNR';
            }

            $Form->Formular_Start();

            $rsAST = $DB->RecordSetOeffnen($SQL);
            $Form->DebugAusgabe(1,$SQL);
            if ($rsAST->AnzahlDatensaetze() > $MAX_TREFFER) {
                $Form->ZeileStart();
                $Text = str_replace('%1', $MAX_TREFFER, $AWISSprachKonserven['AST']['txt_ZuVieleTreffer']);
                $Form->Hinweistext($Text, awisFormular::HINWEISTEXT_HINWEIS);
                $Form->ZeileEnde();
                $Form->Trennzeile('');
            } else {

                // Pr�fen, ob ein Artikel gefunden wurde. Wenn nicht in St�cklisten suchen
                if ($rsAST->EOF()) {
                    $SQL = 'SELECT DISTINCT PARTNR AS AST_ATUNR, AST_BEZEICHNUNGWW, STCKL_NR, AST_KEY, DPR_BEZEICHNUNG ';
                    $SQL .= ' FROM STUECKLISTE ';
                    $SQL .= ' LEFT OUTER JOIN ARTIKELSTAMM ON PARTNR = AST_ATUNR';
                    $SQL .= ' LEFT OUTER JOIN DIENSTLEISTUNGSPREISE ON PARTNR = DPR_NUMMER';
                    $SQL .= ' WHERE UPPER(STCKL_NR)' . $DB->LikeOderIst($Param[1], true);
                    $SQL .= ' ORDER BY STCKL_NR, PARTNR';

                    $ASTZeile = 0;
                    $rsSTU = $DB->RecordSetOeffnen($SQL);
                    if (!$rsSTU->EOF()) {
                        $Form->ZeileStart();
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_ATUNR'], 100);
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'], 400);
                        $Form->ZeileEnde();

                        $LetzteStueckListe = '';
                        while (!$rsSTU->EOF()) {
                            if ($LetzteStueckListe != $rsSTU->FeldInhalt('STCKL_NR')) {
                                $Form->ZeileStart();
                                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['Stueckliste'] . ': ' . $rsSTU->FeldInhalt('STCKL_NR'), 504);
                                $Form->ZeileEnde();

                                $LetzteStueckListe = $rsSTU->FeldInhalt('STCKL_NR');
                            }

                            $Form->ZeileStart();

                            $Link = '';
                            if ($rsSTU->FeldInhalt('AST_KEY') != '') {
                                $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=' . $rsSTU->FeldInhalt('AST_KEY');
                            }
                            $Form->Erstelle_ListenFeld('*AST_ATUNR', $rsSTU->FeldInhalt('AST_ATUNR'), 0, 100, false, ($ASTZeile % 2), '', $Link, 'T', 'L');
                            $Form->Erstelle_ListenFeld('*AST_BEZEICHNUNGWW',
                                ($rsSTU->FeldInhalt('AST_BEZEICHNUNGWW') != ''?$rsSTU->FeldInhalt('AST_BEZEICHNUNGWW'):$rsSTU->FeldInhalt('DPR_BEZEICHNUNG')), 0, 400, false,
                                ($ASTZeile % 2), '', '', 'T', 'L');

                            $Form->ZeileEnde();

                            $rsSTU->DSWeiter();
                            $ASTZeile++;
                        }
                    }
                } else {

                    $StartZeile = 0;
                    if (isset($_GET['Block'])) {
                        $StartZeile = intval($_GET['Block']) * $MaxDSAnzahl;
                    }

                    //** �berschrift f�r die Artikeltreffer
                    $Form->ZeileStart();
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
                    $Link .= '&Sort=AST_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_ATUNR'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_ATUNR'], 100, '', $Link);
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['txtFahrzeuge'], 200, '', '');
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
                    $Link .= '&Sort=AST_BEZEICHNUNGWW' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_BEZEICHNUNGWW'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'], $FeldBreiten['AST_BEZEICHNUNGWW'], '', $Link);
                    if (($Recht451 & 1) == 1)    // Nur Artikel aus der Warenwirtschaft anzeigen
                    {
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['ImWWS'], 50, '', '', $AWISSprachKonserven['AST']['ttt_ImWWS']);
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['ImKatalog'], 50, '', '', $AWISSprachKonserven['AST']['ttt_ImKatalog']);
                    }

                    $Link = '';
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_VK'], 142, 'text-align:center', $Link, $AWISSprachKonserven['AST']['ttt_AST_VK']);
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
                    $Link .= '&Sort=AST_GESBEST' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_GESBEST'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_GESBEST'], 70, '', $Link, $AWISSprachKonserven['AST']['ttt_AST_GESBEST']);
                    if (($Recht451 & 2) == 2)    // Filialbestand anzeigen
                    {
                        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
                        $Link .= '&Sort=AST_FILBESTD' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_FILBESTD'))?'~':'');
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_FILBESTD'], 70, '', $Link,
                                                           str_replace('$1', $BenutzerFilialen, $AWISSprachKonserven['AST']['ttt_AST_FILBESTD']));
                    }
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WGR']['WGR_ID'], 60, '', $Link, $AWISSprachKonserven['WGR']['ttt_WGR_ID']);
                    if ($Param[15] == 'on')        // Fremdk�ufe
                    {
                        $Artikel = new awisATUArtikel();
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['FREMDKAUF12'], 50);
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['FREMDKAUF24'], 50);
                    }

                    $Form->ZeileEnde();

                    // Liste mit ATU Artikel anzeigen
                    $ASTZeile = 0;
                    while (!$rsAST->EOF()){
                        $StyleGeloescht = '';
                        $Loeschhinweis = '';
                        if ($rsAST->FeldInhalt('LOESCHDATUM') != '') {
                            $StyleGeloescht = ';color:#FF0000';
                            $Loeschhinweis = str_replace('$1', $rsAST->FeldInhalt('LOESCHDATUM'), $AWISSprachKonserven['AST']['wrd_AusWWGeloescht']);
                        }

                        // Schriftgr��e festlegen
                        $Form->ZeileStart($ListenSchriftGroesse == 0?'':'font-size:' . intval($ListenSchriftGroesse) . 'pt');

                        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=' . $rsAST->FeldInhalt('AST_KEY');
                        $Form->Erstelle_ListenFeld('*AST_ATUNR', $rsAST->FeldInhalt('AST_ATUNR'), 0, 100, false, ($ASTZeile % 2), '', $Link, 'T', 'L', $Loeschhinweis);
                        $Hersteller =  $rsAST->FeldInhalt('HERSTELLER');
                        $Hersteller = strlen($Hersteller)>20?substr($Hersteller,0,18).'...':$Hersteller;
                        $Form->Erstelle_ListenFeld('*HERSTELLER', $Hersteller, 0, 200, false, ($ASTZeile % 2), '', '', 'T', 'L', $rsAST->FeldInhalt('HERSTELLER'));

                        $Text = $rsAST->FeldInhalt('AST_BEZEICHNUNGWW');
                        $Format = '';
                        if ($Text == '') {
                            $Text = $rsAST->FeldInhalt('AST_BEZEICHNUNG');
                            $Format = 'font-style:italic' . $StyleGeloescht;
                        }
                        if ($FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] < strlen($Text)) {
                            $Text = substr($Text, 0, $FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] - 2) . '...';
                        }

                        $Form->Erstelle_ListenFeld('*AST_BEZEICHNUNGWW', ($Loeschhinweis != ''?$Loeschhinweis:$Text), 0, $FeldBreiten['AST_BEZEICHNUNGWW'], false, ($ASTZeile % 2),
                                                   $Format, '', 'T', 'L', ($Loeschhinweis != ''?$Text:''));

                        if (($Recht451 & 1) == 1)    // Nur Artikel aus der Warenwirtschaft anzeigen
                        {
                            $Form->Erstelle_ListenFeld('*WWS', (($rsAST->FeldInhalt('AST_IMQ_ID') & 2) == 2?'X':''), 0, 50, false, ($ASTZeile % 2),
                                (($rsAST->FeldInhalt('AST_IMQ_ID') & 2) == 2?'':'background-color:#FF0000'), '', 'T', 'Z');
                            $Form->Erstelle_ListenFeld('*WWS', (($rsAST->FeldInhalt('AST_IMQ_ID') & 4) == 4?'X':''), 0, 50, false, ($ASTZeile % 2),
                                (($rsAST->FeldInhalt('AST_IMQ_ID') & 4) == 4?'':'background-color:#FF0000'), '', 'T', 'Z');
                        }

                        //VK, je nach Filiale
                        $VK = $rsAST->FeldInhalt('AST_VK');

                        if ($Param[16] != "") {
                            $BindeVariablen = array();
                            $BindeVariablen['var_T_ast_atunr'] = $rsAST->FeldInhalt('AST_ATUNR');
                            $BindeVariablen['var_N0_fil_id'] = $Param[16];
                            //eventuelle AST_VK hernehmen??
                            $rsVKFil = $DB->RecordSetOeffnen("SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=:var_T_ast_atunr AND FIB_FIL_ID=:var_N0_fil_id",
                                                             $BindeVariablen);
                            $rsVKAnz = $rsVKFil->AnzahlDatensaetze();

                            if ($rsVKAnz > 0) {
                                $VK = $rsVKFil->FeldInhalt('FIB_VK');
                            }
                        }

                        $Form->Erstelle_ListenFeld('*AST_VK', $VK, 0, 100, false, ($ASTZeile % 2), '', '', 'N2', 'R', $AWISSprachKonserven['AST']['ttt_AST_VK']);

                        if ($LAN_CODE != '') {
                            $Form->Erstelle_ListenFeld('*WAEHRUNG', $rsLAN->FeldInhalt('LAN_WAE_CODE'), 0, 40, false, ($ASTZeile % 2), '', '', 'T', 'L');
                        } else {
                            $Form->Erstelle_ListenFeld('*WAEHRUNG', $rsUSERLAND->FeldInhalt('LAN_WAE_CODE'), 0, 40, false, ($ASTZeile % 2), '', '', 'T', 'L');
                        }
                        $Form->Erstelle_ListenFeld('*AST_GESBEST', $rsAST->FeldInhalt('AST_GESBEST'), 0, 70, false, ($ASTZeile % 2), '', '', 'T', 'R');
                        if (($Recht451 & 2) == 2)    // Filialbestand anzeigen
                        {
                            $Link = '/ATUArtikel/filialbestand.php?ATUNR=' . $rsAST->FeldInhalt('AST_ATUNR') . '&NurBestand=1&Zurueck=ArtikelListeNeu';
                            $Form->Erstelle_ListenFeld('*FIB_BESTAND', $rsAST->FeldInhalt('FIB_BESTAND'), 0, 70, false, ($ASTZeile % 2), '', $Link, 'N0', 'R');
                        }
                        $Form->Erstelle_ListenFeld('*WGR_ID', $rsAST->FeldInhalt('WGR_ID'), 0, 60, false, ($ASTZeile % 2), '', '', 'T', 'R');

                        if ($Param[15] == 'on')        // Fremdk�ufe
                        {
                            $Artikel->LadeArtikel($rsAST->FeldInhalt('AST_ATUNR'));
                            $FremdKauf = $Artikel->Fremdkaeufe();
                            $Form->Erstelle_ListenFeld('*FK12', $FremdKauf[12], 5, 50, false, ($ASTZeile % 2), '', '', 'N0', 'R');
                            $Form->Erstelle_ListenFeld('*FK24', $FremdKauf[24], 5, 50, false, ($ASTZeile % 2), '', '', 'N0', 'R');
                        }

                        $Form->ZeileEnde();
                        $rsAST->DSWeiter();
                        $ASTZeile++;
                    }
                }
            }

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel(str_replace('$1', $rsAST->AnzahlDatensaetze(), $AWISSprachKonserven['Wort']['txt_AnzahlDSGefunden']), 500);
            $Form->ZeileEnde();
        }            // Ende : Suche nach einer ATU Nummer
        //******************************************
        elseif ($SuchMethode == 2)                    // Suche nach einer anderen Nummer
            //******************************************
        {
            $DSAnz = 0;

            $rsArtikelTreffer = array();

            if ($Param[13] != '')        // Suche nach beliebiger Nummer -> es wird nach allem gesucht
            {
                $Param[8] = 1;
                $Param[9] = 1;
                $Param[10] = 1;
                $Param[11] = 1;
                $Param[12] = 1;
                $Param[20] = 1;
            }

            if ($Param[8] != '')        // Suche nach EAN Nummer
            {
                $SQL = "SELECT DISTINCT AST_ATUNR, AST_KEY, AST_BEZEICHNUNG, AST_IMQ_ID, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'EAN' AS FUNDSTELLE, 'EAN' AS FUNDZUSATZ, TEI_WERT2 AS TREFFER ";
                $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
                $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW';
                if ($LAN_CODE != '') {
                    if ($LAN_CODE == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$LAN_CODE] . ') AS AST_VK';
                    }
                } else {
                    if ($BenutzerSprache == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$BenutzerSprache] . ') AS AST_VK';
                    }
                }
                // Lagerbestand
                $SQL .= ',COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (10,11)),0) -';
                $SQL .= ' COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (13,14)),0) AS AST_GESBEST';
                // Gesamt-Filialbestand
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=12) AS AST_FILBESTD';
                if (($Recht451 & 2) == 2) {
                    $SQL .= ',(SELECT SUM(FIB_BESTAND)AS FIB_BESTAND ';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = artikelstamm.ast_atunr";
                    if ($BenutzerFilialen != '') {
                        $SQL .= " AND FIB_FIL_ID IN(0" . $BenutzerFilialen . ")";
                    }
                    $SQL .= " ) AS FIB_BESTAND";
                }
                $SQL .= ',(select  listagg(HERSTELLER,\',\')  within group (order by HERSTELLER)  from V_ARTIKEL_FAHRZEUGE fzg WHERE fzg.ATU_NR = AST_ATUNR group by ATU_NR) as HERSTELLER';
                $SQL .= ' FROM Artikelstamm';
                $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';
                $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
                $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
                $SQL .= " INNER JOIN TeileInfos ON TEI_KEY1 = AST_KEY AND (TEI_ITY_ID2 = 'EAN' ";
                $SQL .= " AND (TEI_WERT2) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_UPPER) . ")";
                $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');
                $SQL .= " ORDER BY TEI_WERT2";        //Singer, 16.12.2009

                $rsArtikel = $DB->RecordSetOeffnen($SQL);
                if ($rsArtikel == false) {
                    awisErrorMailLink("200803271806-001", 2, $awisDBFehler);
                    die();
                }
                $rsArtikelZeilen = $rsArtikel->AnzahlDatensaetze();
                $DSAnz += $rsArtikel->AnzahlDatensaetze();
                $rsArtikelTreffer = array_merge_recursive($rsArtikelTreffer, $rsArtikel->Tabelle());
            }

            if ($Param[9] != '')        // Suche nach Gebarauchsnummer
            {
                $SQL = "SELECT DISTINCT AST_ATUNR, AST_KEY, AST_BEZEICHNUNG, AST_IMQ_ID, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'GNR' AS FUNDSTELLE, 'GNR' AS FUNDZUSATZ, T2.TEI_WERT2 AS TREFFER ";
                $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
                $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW';
                if ($LAN_CODE != '') {
                    if ($LAN_CODE == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$LAN_CODE] . ') AS AST_VK';
                    }
                } else {
                    if ($BenutzerSprache == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$BenutzerSprache] . ') AS AST_VK';
                    }
                }
                // Lagerbestand
                $SQL .= ',COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (10,11)),0) -';
                $SQL .= ' COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (13,14)),0) AS AST_GESBEST';
                // Gesamt-Filialbestand
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=12) AS AST_FILBESTD';
                if (($Recht451 & 2) == 2) {
                    $SQL .= ',(SELECT SUM(FIB_BESTAND)AS FIB_BESTAND ';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = artikelstamm.ast_atunr";
                    if ($BenutzerFilialen != '') {
                        $SQL .= " AND FIB_FIL_ID IN(0" . $BenutzerFilialen . ")";
                    }
                    $SQL .= " ) AS FIB_BESTAND";
                }
                $SQL .= ',(select  listagg(HERSTELLER,\',\')  within group (order by HERSTELLER)  from V_ARTIKEL_FAHRZEUGE fzg WHERE fzg.ATU_NR = AST_ATUNR group by ATU_NR) as HERSTELLER';
                $SQL .= ' FROM Artikelstamm';
                $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';
                $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
                $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
                $SQL .= ' INNER JOIN TeileInfos T1 ON T1.TEI_KEY1 = AST_KEY ';
                $SQL .= ' INNER JOIN TeileInfos T2 ON T1.TEI_KEY2 = T2.TEI_KEY1 AND T2.TEI_ITY_ID2=\'GNR\'';
                if ($Param[7] != 'on')            // NICHT Exakte suche
                {
                    $SQL .= " AND (T2.TEI_SUCH2) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER) . "";
                } else {
                    $SQL .= ' AND UPPER(T2.TEI_WERT2) ' . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_UPPER);
                }

                $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');
                $SQL .= " ORDER BY T2.TEI_WERT2";


                $rsArtikel = $DB->RecordSetOeffnen($SQL);
                if ($rsArtikel == false) {
                    awisErrorMailLink("200803271807", 2, $awisDBFehler);
                    die();
                }
                $rsArtikelZeilen = $rsArtikel->AnzahlDatensaetze();
                $DSAnz += $rsArtikel->AnzahlDatensaetze();
                $rsArtikelTreffer = array_merge_recursive($rsArtikelTreffer, $rsArtikel->Tabelle());
            }

            if ($Param[10] != '')        // Suche nach Lieferantenartikelnummer
            {
                $MitProblemartikeln = $AWISBenutzer->ParameterLesen("Lieferantenartikel: Problemartikel anzeigen");

                $SQL = 'SELECT AST_ATUNR	,AST_KEY ,AST_BEZEICHNUNG,AST_IMQ_ID,WUG_WGR_ID,WUG_ID,WGR_ID,WGR_BEZEICHNUNG,FUNDSTELLE,FUNDZUSATZ,TREFFER,LOESCHDATUM,AST_BEZEICHNUNGWW,AST_VK,AST_GESBEST,AST_FILBESTD,FIB_BESTAND,AST_SUCH2, HERSTELLER';
                $SQL .= ' FROM (';
                $SQL .= "SELECT DISTINCT AST_ATUNR, AST_KEY, AST_BEZEICHNUNG, AST_IMQ_ID, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'LAR, " . $AWISSprachKonserven['LAR']['LAR_LIE_NR'] . " '|| LAR_LIE_NR AS FUNDSTELLE, LAR_LIE_NR AS FUNDZUSATZ, TEI_WERT2 AS TREFFER, AST_SUCH2 ";
                $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
                $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW';
                if ($LAN_CODE != '') {
                    if ($LAN_CODE == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$LAN_CODE] . ') AS AST_VK';
                    }
                } else {
                    if ($BenutzerSprache == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$BenutzerSprache] . ') AS AST_VK';
                    }
                }
                if (($Recht452 & 128) == 0) {
                    $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=40) AS HAUPTLIEFERANT	';
                }
                // Lagerbestand
                $SQL .= ',COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (10,11)),0) -';
                $SQL .= ' COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (13,14)),0) AS AST_GESBEST';
                // Gesamt-Filialbestand
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=12) AS AST_FILBESTD';
                if (($Recht451 & 2) == 2) {
                    $SQL .= ',(SELECT SUM(FIB_BESTAND)AS FIB_BESTAND ';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = artikelstamm.ast_atunr";
                    if ($BenutzerFilialen != '') {
                        $SQL .= " AND FIB_FIL_ID IN(0" . $BenutzerFilialen . ")";
                    }
                    $SQL .= " ) AS FIB_BESTAND";
                }
                $SQL .= ', LAR_LIE_NR';            // Zum Ermitteln des Hauptlieferanten
                $SQL .= ',(select  listagg(HERSTELLER,\',\')  within group (order by HERSTELLER)  from V_ARTIKEL_FAHRZEUGE fzg WHERE fzg.ATU_NR = AST_ATUNR group by ATU_NR) as HERSTELLER';
                $SQL .= ' FROM Artikelstamm';
                $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';
                $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
                $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
                $SQL .= ' INNER JOIN TeileInfos ON (AST_KEY = TEI_KEY1 AND TEI_ITY_ID2=\'LAR\'';

                if ($Param[7] != 'on')        // Exakte oder angepasste Suche
                {
                    if ($Recht452 & 16)        // Auch Artikel mit Reklamationskennung anzeigem
                    {
                        $SQL .= " AND (TEI_SUCH2) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER) . ")";
                    } else                    // Nur Artikel ohne Reklamationskennung
                    {
                        $SQL .= " AND EXISTS(select * from lieferantenartikel where LAR_KEY = TEI_KEY2 AND LAR_SUCH_LARTNR " . $DB->LIKEoderIST($Param[6],
                                                                                                                                                awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER) . ' AND LAR_REKLAMATIONSKENNUNG is null)';
                        $SQL .= ')';
                    }
                } else {
                    if ($Recht452 & 16)        // Auch Artikel mit Reklamationskennung anzeigem
                    {
                        $SQL .= " AND UPPER(TEI_WERT2) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_UPPER) . ")";
                    } else                    // Nur Artikel ohne Reklamationskennung
                    {
                        $SQL .= " AND EXISTS(select * from lieferantenartikel where LAR_KEY = TEI_KEY2 AND LAR_LARTNR " . $DB->LIKEoderIST($Param[6],
                                                                                                                                           awisDatenbank::AWIS_LIKE_UPPER) . ' AND LAR_REKLAMATIONSKENNUNG is null))';
                    }
                }

                $SQL .= ' INNER JOIN LieferantenArtikel ON TEI_KEY2 = LAR_KEY';


                $SQL .= ')';
                $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

                if (($Recht452 & 128) == 0) {
                    $SQL .= ($Bedingung != ''?' AND ':' WHERE ') . ' HAUPTLIEFERANT = LAR_LIE_NR';
                }

                $SQL .= $Bedingung . " ORDER BY TREFFER";

                $rsArtikel = $DB->RecordSetOeffnen($SQL);
                if ($rsArtikel == false) {
                    awisErrorMailLink("200803271951", 2, $awisDBFehler);
                    die();
                }
                $rsArtikelZeilen = $rsArtikel->AnzahlDatensaetze();
                $DSAnz += $rsArtikel->AnzahlDatensaetze();
                $rsArtikelTreffer = array_merge_recursive($rsArtikelTreffer, $rsArtikel->Tabelle());
            }

            //**********************************************************
            //* Lieferantenartikelset
            //**********************************************************
            if ($Param[11] != '')        // Suche nach Lieferantensets
            {
                $Bedingung1 = '';
                $SQL = 'SELECT AST_ATUNR	,AST_KEY ,AST_BEZEICHNUNG,AST_IMQ_ID,WUG_WGR_ID,WUG_ID,WGR_ID,WGR_BEZEICHNUNG,FUNDSTELLE,FUNDZUSATZ,TREFFER,LOESCHDATUM,AST_BEZEICHNUNGWW,AST_VK,AST_GESBEST,AST_FILBESTD,FIB_BESTAND';
                $SQL .= ' FROM (';
                $SQL .= "SELECT DISTINCT AST_ATUNR, AST_KEY, AST_BEZEICHNUNG, AST_IMQ_ID, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'LAS' AS FUNDSTELLE, 'LAS' AS FUNDZUSATZ, T2.TEI_WERT2 AS TREFFER ";
                $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
                $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW';
                if ($LAN_CODE != '') {
                    if ($LAN_CODE == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$LAN_CODE] . ') AS AST_VK';
                    }
                } else {
                    if ($BenutzerSprache == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$BenutzerSprache] . ') AS AST_VK';
                    }
                }
                if (($Recht452 & 128) == 0) {
                    $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=40) AS HAUPTLIEFERANT	';
                }
                // Lagerbestand
                $SQL .= ',COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (10,11)),0) -';
                $SQL .= ' COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (13,14)),0) AS AST_GESBEST';
                // Gesamt-Filialbestand
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=12) AS AST_FILBESTD';
                if (($Recht451 & 2) == 2) {
                    $SQL .= ',(SELECT SUM(FIB_BESTAND)AS FIB_BESTAND ';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = artikelstamm.ast_atunr";
                    if ($BenutzerFilialen != '') {
                        $SQL .= " AND FIB_FIL_ID IN(0" . $BenutzerFilialen . ")";
                    }
                    $SQL .= " ) AS FIB_BESTAND";
                }
                $SQL .= ', LAR_LIE_NR';
                $SQL .= ' FROM Artikelstamm';
                $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';
                $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
                $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
                $SQL .= " INNER JOIN TeileInfos T1 ON T1.TEI_KEY1 = AST_KEY AND T1.TEI_ITY_ID1 = 'AST' AND T1.TEI_ITY_ID2='LAR'";
                $SQL .= " INNER JOIN TeileInfos T2 ON T2.TEI_KEY2 = T1.TEI_KEY2 AND T2.TEI_ITY_ID2 = 'LAS'";
                $SQL .= " INNER JOIN LieferantenArtikel ON T2.TEI_KEY1 = LAR_KEY";

                if ($Param[7] != 'on')        // Exakte oder angepasste Suche
                {
                    if ($Recht452 & 16)        // Auch Artikel mit Reklamationskennung anzeigem
                    {
                        $Bedingung1 .= " AND (T2.TEI_SUCH1) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER) . "";
                    } else                    // Nur Artikel ohne Reklamationskennung
                    {
                        $Bedingung1 .= " AND EXISTS(select * from lieferantenartikel where LAR_KEY = T2.TEI_KEY1 AND LAR_SUCH_LARTNR " . $DB->LIKEoderIST($Param[6],
                                                                                                                                                          awisDatenbank::AWIS_LIKE_ASCIIWORT + awisDatenbank::AWIS_LIKE_UPPER) . ' AND LAR_REKLAMATIONSKENNUNG is null)';
                    }
                } else {
                    if ($Recht452 & 16)        // Auch Artikel mit Reklamationskennung anzeigem
                    {
                        $Bedingung1 .= " AND UPPER(T2.TEI_WERT1) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_UPPER) . "";
                    } else                    // Nur Artikel ohne Reklamationskennung
                    {
                        $Bedingung1 .= " AND EXISTS(select * from lieferantenartikel where LAR_KEY = T2.TEI_KEY1 AND LAR_LARTNR " . $DB->LIKEoderIST($Param[6],
                                                                                                                                                     awisDatenbank::AWIS_LIKE_UPPER) . ' AND LAR_REKLAMATIONSKENNUNG is null)';
                    }
                }

                $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4) . ' ' . $Bedingung1:($Bedingung1 == ''?'':' WHERE ' . substr($Bedingung1, 4)));
                $SQL .= ')';
                if (($Recht452 & 128) == 0) {
                    $SQL .= ' WHERE HAUPTLIEFERANT = LAR_LIE_NR';
                }
                $SQL .= " ORDER BY TREFFER";

                $rsArtikel = $DB->RecordSetOeffnen($SQL);
                if ($rsArtikel == false) {
                    awisErrorMailLink("200804011000", 2, $awisDBFehler);
                    die();
                }
                $rsArtikelZeilen = $rsArtikel->AnzahlDatensaetze();
                $DSAnz += $rsArtikel->AnzahlDatensaetze();
                $rsArtikelTreffer = array_merge_recursive($rsArtikelTreffer, $rsArtikel->Tabelle());
            }

            //**********************************************************
            //* OE-Nummern
            //**********************************************************
            if ($Param[12] != '')        // Suche nach OE-Nummern
            {
                $SQL = "SELECT DISTINCT AST_ATUNR, AST_KEY, AST_BEZEICHNUNG, AST_IMQ_ID, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'OEN' AS FUNDSTELLE, 'OEN' AS FUNDZUSATZ, TEI_WERT2 AS TREFFER ";
                $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
                $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW';
                if ($LAN_CODE != '') {
                    if ($LAN_CODE == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$LAN_CODE] . ') AS AST_VK';
                    }
                } else {
                    if ($BenutzerSprache == 'DE') {
                        $SQL .= ', AST_VK';
                    } else {
                        $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$BenutzerSprache] . ') AS AST_VK';
                    }
                }
                // Lagerbestand
                $SQL .= ',COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (10,11)),0) -';
                $SQL .= ' COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (13,14)),0) AS AST_GESBEST';
                // Gesamt-Filialbestand
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=12) AS AST_FILBESTD';
                if (($Recht451 & 2) == 2) {
                    $SQL .= ',(SELECT SUM(FIB_BESTAND)AS FIB_BESTAND ';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = artikelstamm.ast_atunr";
                    if ($BenutzerFilialen != '') {
                        $SQL .= " AND FIB_FIL_ID IN(0" . $BenutzerFilialen . ")";
                    }
                    $SQL .= " ) AS FIB_BESTAND";
                }
                $SQL .= ',(select  listagg(HERSTELLER,\',\')  within group (order by HERSTELLER)  from V_ARTIKEL_FAHRZEUGE fzg WHERE fzg.ATU_NR = AST_ATUNR group by ATU_NR) as HERSTELLER';
                $SQL .= ' FROM Artikelstamm';
                $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';
                $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
                $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
                $SQL .= " INNER JOIN TeileInfos ON TEI_KEY1 = AST_KEY AND TEI_ITY_ID2='OEN'";

                if ($Param[7] != 'on')        // Exakte oder angepasste Suche
                {
                    $SQL .= " AND (TEI_SUCH2) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_SUCHOE) . "";
                } else {
                    $SQL .= " AND UPPER(TEI_WERT2) " . $DB->LIKEoderIST($Param[6], awisDatenbank::AWIS_LIKE_UPPER) . "";
                }

                $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');
                $SQL .= " ORDER BY TEI_WERT2";

                $rsArtikel = $DB->RecordSetOeffnen($SQL);
                if ($rsArtikel == false) {
                    awisErrorMailLink("200804011325", 2, $awisDBFehler);
                    die();
                }
                $rsArtikelZeilen = $rsArtikel->AnzahlDatensaetze();
                $DSAnz += $rsArtikel->AnzahlDatensaetze();
                $rsArtikelTreffer = array_merge_recursive($rsArtikelTreffer, $rsArtikel->Tabelle());
            }

            //**********************************************************
            //* Online-Shop-Nummern
            //**********************************************************
            if ($Param[20] != '' and ((ctype_digit($Param[6]) == true) or strtoupper(substr($Param[6], 0, 3)) == 'PSD'))        // Suche nach OnlineShopNummern
            {
                $SQL = "SELECT DISTINCT AST_ATUNR, AST_KEY, AST_BEZEICHNUNG, AST_IMQ_ID, WUG_WGR_ID, WUG_ID, WGR_ID, WGR_BEZEICHNUNG, 'ONL' AS FUNDSTELLE, 'ONL' AS FUNDZUSATZ, '" . $Param[6] . "' AS TREFFER ";
                $SQL .= ', CASE WHEN BITAND(AST_IMQ_ID,2)=2 THEN NULL ELSE (SELECT MAX(LOA_LOESCHDATUM) FROM Loeschartikel WHERE LOA_AST_ATUNR = AST_ATUNR) END AS LOESCHDATUM';
                $SQL .= ', COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS AST_BEZEICHNUNGWW';
                if ($BenutzerSprache == 'DE') {
                    $SQL .= ', AST_VK';
                } else {
                    $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=' . $AIT_VK[$BenutzerSprache] . ') AS AST_VK';
                }
                // Lagerbestand
                $SQL .= ',COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (10,11)),0) -';
                $SQL .= ' COALESCE((SELECT SUM(TO_NUMBER(ASI_WERT,\'9999999.99\')) AS AST_GESBEST FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id IN (13,14)),0) AS AST_GESBEST';
                // Gesamt-Filialbestand
                $SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id=12) AS AST_FILBESTD';
                if (($Recht451 & 2) == 2) {
                    $SQL .= ',(SELECT SUM(FIB_BESTAND)AS FIB_BESTAND ';
                    $SQL .= " FROM FilialBestand";
                    $SQL .= " WHERE ";
                    $SQL .= " FIB_AST_ATUNR = artikelstamm.ast_atunr";
                    if ($BenutzerFilialen != '') {
                        $SQL .= " AND FIB_FIL_ID IN(0" . $BenutzerFilialen . ")";
                    }
                    $SQL .= " ) AS FIB_BESTAND";
                }
                $SQL .= ',(select  listagg(HERSTELLER,\',\')  within group (order by HERSTELLER)  from V_ARTIKEL_FAHRZEUGE fzg WHERE fzg.ATU_NR = AST_ATUNR group by ATU_NR) as HERSTELLER';
                $SQL .= ' FROM Artikelstamm';
                $SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \'' . $BenutzerSprache . '\'';
                $SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
                $SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
                $Bedingung = 'AND decodeatunr(' . strtoupper($DB->FeldInhaltFormat('T', str_replace('.', '', $Param[6]))) . ') = AST_ATUNR';

                $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

                $rsArtikel = $DB->RecordSetOeffnen($SQL);
                if ($rsArtikel == false) {
                    awisErrorMailLink("200804011325", 2, $awisDBFehler);
                    die();
                }
                $rsArtikelZeilen = $rsArtikel->AnzahlDatensaetze();
                $DSAnz += $rsArtikel->AnzahlDatensaetze();
                $rsArtikelTreffer = array_merge_recursive($rsArtikelTreffer, $rsArtikel->Tabelle());
            }

            if ($DSAnz > $MAX_TREFFER) {
                $Form->ZeileStart();
                $Text = str_replace('%1', $MAX_TREFFER, $AWISSprachKonserven['AST']['txt_ZuVieleTreffer']);
                $Form->Hinweistext($Text, awisFormular::HINWEISTEXT_HINWEIS);
                $Form->ZeileEnde();
                $Form->Trennzeile('');
            } else {
                //**************************************
                // Daten anzeigen
                //**************************************
                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['txt_Treffer'], 200);
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['txtFahrzeuge'],200);
                if(($Recht451 & 64) == 64){
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['txt_Treffer'], 54);
                }
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_ATUNR'], 100);
                if ($SuchMethode == 2) {
                    $Link = '';
                } else {
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
                    $Link .= '&Sort=AST_BEZEICHNUNGWW' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AST_BEZEICHNUNGWW'))?'~':'');
                }

                if ($FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] < strlen($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'])) {
                    $Text = substr($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'], 0, $FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] - 2) . '...';
                } else {
                    $Text = $AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'];
                }
                $Form->Erstelle_Liste_Ueberschrift($Text, $FeldBreiten['AST_BEZEICHNUNGWW'], '', $Link);
                if (($Recht451 & 1) == 1)    // Nur Artikel aus der Warenwirtschaft anzeigen
                {
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['ImWWS'], 50, '', '', $AWISSprachKonserven['AST']['ttt_ImWWS']);
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['ImKatalog'], 50, '', '', $AWISSprachKonserven['AST']['ttt_ImKatalog']);
                }

                if ($SuchMethode == 2) {
                    $Link = '';
                } else {
                    $Link = '';
                }

                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_VK'], 122, 'text-align:center', $Link, $AWISSprachKonserven['AST']['ttt_AST_VK']);
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_GESBEST'], 70, '', $Link, $AWISSprachKonserven['AST']['ttt_AST_GESBEST']);
                if (($Recht451 & 2) == 2)    // Filialbestand anzeigen
                {
                    if ($SuchMethode == 2) {
                        $Link = '';
                    } else {
                        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
                        $Link .= '&Sort=FIB_BESTAND' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FIB_BESTAND'))?'~':'');
                    }
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AST']['AST_FILBESTD'], 70, '', $Link,
                                                       str_replace('$1', $BenutzerFilialen, $AWISSprachKonserven['AST']['ttt_AST_FILBESTD']));
                }
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WGR']['WGR_ID'], 60, '', $Link, $AWISSprachKonserven['WGR']['ttt_WGR_ID']);

                $Form->ZeileEnde();
                for ($DS = 0; $DS < $DSAnz; $DS++) {
                    $Form->ZeileStart($ListenSchriftGroesse == 0?'':'font-size:' . intval($ListenSchriftGroesse) . 'pt');

                    $Form->Erstelle_ListenFeld('*TREFFER', $rsArtikelTreffer['TREFFER'][$DS], 0, 200, false, ($DS % 2), '', '', 'T', 'L', $rsArtikelTreffer['FUNDSTELLE'][$DS]);
                    $Hersteller =  $rsArtikelTreffer['HERSTELLER'][$DS];
                    $Hersteller = strlen($Hersteller)>20?substr($Hersteller,0,18).'...':$Hersteller;
                    $Form->Erstelle_ListenFeld('*HERSTELLER', $Hersteller, 0, 200, false, ($DS % 2), '', '', 'T', 'L', $rsArtikelTreffer['HERSTELLER'][$DS]);

                    if (($Recht451 & 64) != 0) {
                        $Form->Erstelle_ListenFeld('*FUNDZUSATZ', $rsArtikelTreffer['FUNDZUSATZ'][$DS], 0, 50, false, ($DS % 2), '', '', 'T', 'L');
                    }
                    $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=' . $rsArtikelTreffer['AST_KEY'][$DS] . '';
                    $StyleGeloescht = '';
                    $Loeschhinweis = '';
                    if ($rsArtikelTreffer['LOESCHDATUM'][$DS] != '') {
                        $StyleGeloescht = ';color:#FF0000';
                        $Loeschhinweis = str_replace('$1', $rsArtikelTreffer['LOESCHDATUM'][$DS], $AWISSprachKonserven['AST']['wrd_AusWWGeloescht']);
                    }
                    $Form->Erstelle_ListenFeld('*AST_ATUNR', $rsArtikelTreffer['AST_ATUNR'][$DS], 0, 100, false, ($DS % 2), '', $Link, '', 'L', '');

                    $Text = $rsArtikelTreffer['AST_BEZEICHNUNGWW'][$DS];
                    $ToolTipp = $Text;
                    $Format = '';
                    if ($Text == '') {
                        $Text = $rsArtikelTreffer['AST_BEZEICHNUNG'][$DS];
                        $ToolTipp = $Text;
                        $Format = 'font-style:italic' . $StyleGeloescht;
                    }
                    if ($FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] < strlen($Text)) {
                        $Text = substr($Text, 0, $FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN'] - 2) . '...';
                    }
                    $Form->Erstelle_ListenFeld('*AST_BEZEICHNUNGWW', ($Loeschhinweis == ''?$Text:$Loeschhinweis), 0, $FeldBreiten['AST_BEZEICHNUNGWW'], false, ($DS % 2), $Format,
                                               '', 'T', 'L', ($Loeschhinweis == ''?$ToolTipp:$Text));

                    if (($Recht451 & 1) == 1)    // Nur Artikel aus der Warenwirtschaft anzeigen
                    {
                        $Form->Erstelle_ListenFeld('*WWS', (($rsArtikelTreffer['AST_IMQ_ID'][$DS] & 2) == 2?'X':''), 0, 50, false, ($DS % 2),
                            (($rsArtikelTreffer['AST_IMQ_ID'][$DS] & 2) == 2?'':'background-color:#FF0000'), '', 'T', 'Z');
                        $Form->Erstelle_ListenFeld('*WWS', (($rsArtikelTreffer['AST_IMQ_ID'][$DS] & 4) == 4?'X':''), 0, 50, false, ($DS % 2),
                            (($rsArtikelTreffer['AST_IMQ_ID'][$DS] & 4) == 4?'':'background-color:#FF0000'), '', 'T', 'Z');
                    }

                    //VK, je nach Filiale
                    $VK = $rsArtikelTreffer['AST_VK'][$DS];

                    if ($Param[16] != "") {
                        $BindeVariablen = array();
                        $BindeVariablen['var_T_ast_atunr'] = $rsArtikelTreffer['AST_ATUNR'][$DS];
                        $BindeVariablen['var_N0_fil_id'] = $Param[16];
                        //eventuelle AST_VK hernehmen??
                        $rsVKFil = $DB->RecordSetOeffnen("SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=:var_T_ast_atunr AND FIB_FIL_ID=:var_N0_fil_id", $BindeVariablen);
                        $rsVKAnz = $rsVKFil->AnzahlDatensaetze();

                        if ($rsVKAnz > 0) {
                            $VK = $rsVKFil->FeldInhalt('FIB_VK');
                        }
                    }

                    $Form->Erstelle_ListenFeld('*AST_VK', $VK, 0, 80, false, ($DS % 2), '', '', 'N2', 'R', $AWISSprachKonserven['AST']['ttt_AST_VK']);

                    if ($LAN_CODE != '') {
                        $Form->Erstelle_ListenFeld('*WAEHRUNG', $rsLAN->FeldInhalt('LAN_WAE_CODE'), 0, 40, false, ($DS % 2), '', '', 'T', 'L');
                    } else {
                        $Form->Erstelle_ListenFeld('*WAEHRUNG', $rsUSERLAND->FeldInhalt('LAN_WAE_CODE'), 0, 40, false, ($DS % 2), '', '', 'T', 'L');
                    }
                    $Form->Erstelle_ListenFeld('*AST_GESBEST', $rsArtikelTreffer['AST_GESBEST'][$DS], 0, 70, false, ($DS % 2), '', '', 'T', 'R');
                    if (($Recht451 & 2) == 2)    // Filialbestand anzeigen
                    {
                        $Link = '/ATUArtikel/filialbestand.php?ATUNR=' . $rsArtikelTreffer['AST_ATUNR'][$DS] . '&NurBestand=1&Zurueck=ArtikelListeNeu';
                        $Form->Erstelle_ListenFeld('*FIB_BESTAND', $rsArtikelTreffer['FIB_BESTAND'][$DS], 0, 70, false, ($DS % 2), '', $Link, 'N0', 'R');
                    }
                    $Form->Erstelle_ListenFeld('*WGR_ID', $rsArtikelTreffer['WGR_ID'][$DS], 0, 60, false, ($DS % 2), '', '', 'T', 'R');
                    $Form->ZeileEnde();
                }
            }

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel(str_replace('$1', $DSAnz, $AWISSprachKonserven['Wort']['txt_AnzahlDSGefunden']), 500);
            $Form->ZeileEnde();
        }        // Ende: Suchmethode 2 (Weitere Nummern)
    }    // Ende: Listen anzeigen

    $Form->Formular_Ende();

    // Die Schaltfl�chen k�nnen oben oder unten angezeigt werden
    if ($IconPosition == '0') {
        $Form->SchaltflaechenStart();

        $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        $Recht453 = $AWISBenutzer->HatDasRecht(453);            // Recht f�r die Kommentare bearbeiten

        $EditModusKommentare = false;
        if ((isset($_GET['Seite']) AND $_GET['Seite'] == 'Kommentare' AND $AWIS_KEY2 != '') AND ((($Recht453 & 4) == 4) OR (($Recht453 & 32) == 32) OR (($Recht453 & 256) == 256))) {
            $EditModusKommentare = true;
        }

        $EditModusLieferantenArtikel = false;
        $Recht452 = $AWISBenutzer->HatDasRecht(452);            // Recht f�r die Lieferantenartikel bearbeiten
        if ((isset($_GET['Seite']) AND $_GET['Seite'] == 'Lieferantenartikel' AND $AWIS_KEY2 != '' AND (($Recht452 & 4096) == 4096))) {
            $EditModusLieferantenArtikel = true;
        }

        if (($EditModus or $EditModusKommentare or $EditModusLieferantenArtikel) AND $DetailAnsicht) {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }

        if (($Recht450 & 4) == 4)        // Bearbeitungsmodus f�r den Artikel
        {
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }

        if ($DetailAnsicht) {
            $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
            $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');

            if ($ArtikelLoeschbar) {
                $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
            }
        }

        if (isset($_GET['Seite']) AND $_GET['Seite'] == 'Pruefungen' AND $AWIS_KEY2 != '') {
            $Form->Schaltflaeche('href', 'cmdDruckenPruefungen', './artikelstamm_drucken_Pruefungen.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], '');
        }

        $Form->Schaltflaeche('script', 'cmdHilfe',
                             "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=artikelinfo&Seite=" . (isset($_GET['Seite'])?$_GET['Seite']:'Lieferantenartikel') . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');",
                             '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

        // Suchfeld f�r die ATU Nummer
        if ($SchnellSucheFeld) {
            $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txt_Schnellsuche'] . ':', 150, 'text-align:right', '', $AWISSprachKonserven['AST']['ttt_Schnellsuche']);
            $Form->Erstelle_TextFeld('*ASTATUNR', '', 10, 0, true, '', '', '', 'T', 'L', '', '', 8, '', 'f');
            $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
        }

        $Form->SchaltflaechenEnde();
    }

    $Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $DB->ProtokollEintrag(awisDatenbank::EREIGNIS_FEHLER, 1200, $AWISBenutzer->BenutzerName(), $ex->getMessage(), $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060009");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812061223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
