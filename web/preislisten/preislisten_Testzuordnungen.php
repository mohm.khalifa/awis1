<?php
global $con;
global $RechteStufe;
global $awisDBError;
global $awisRSZeilen;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con,2001);		// Preislistenadministration
if($RechteStufe==0)
{
	awisLogoff($con);
	die();
}



if(($RechteStufe&8)!=8)		// Rechte f�r Zuordnungen der Tests
{
    awisEreignis(3,1000,'Preislistenpr�fungen',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$comcon = awisLogonComCluster('DE',false);

//awis_Debug(1,$_POST);
// Daten l�schen


//*************************************
// Test hinzuf�gen
//*************************************

if(isset($_POST['txtPLP_PTA_ID_0'])AND $_POST['txtPLP_PTA_ID_0']!=0)
{
	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
	$SQL .= '(PLP_ID_FPLB,PLP_PTA_ID,PLP_STATUS,PLP_REIHENFOLGE,PLP_USER,PLP_USERDAT)';
	$SQL .= 'VALUES(';
	$SQL .= $_POST['txtPLP_ID_FPLB'];
	$SQL .= ', '.$_POST['txtPLP_PTA_ID_0'];
	$SQL .= ', 1';
	$SQL .= ', 100';
	$SQL .= ', \''.$AWISBenutzer->BenutzerName().'\'';
	$SQL .= ', SYSDATE';
	$SQL .= ')';

	if(!awisExecute($comcon,$SQL))
	{
		awisErrorMailLink('30092259-Testzuordnungen',1,'Fehler neu anlegen',$awisDBError['message']);
	}
}

//*************************************
// Daten �ndern
//*************************************

$Vars = explode(';',awis_NameInArray($_POST,'txtPLP_BEMERKUNG',1));
foreach($Vars AS $Feld)
{
	$Nr = intval(substr($Feld,17));
	$Felder = '';

	if((isset($_POST['txtPLP_BEMERKUNG_'.$Nr])?$_POST['txtPLP_BEMERKUNG_'.$Nr]:'') <> (isset($_POST['oldPLP_BEMERKUNG_'.$Nr])?$_POST['oldPLP_BEMERKUNG_'.$Nr]:''))
	{
		$Felder .= ',PLP_BEMERKUNG=\'' . $_POST['txtPLP_BEMERKUNG_'.$Nr] . '\'';
	}
	if((isset($_POST['txtPLP_STATUS_'.$Nr])?$_POST['txtPLP_STATUS_'.$Nr]:'') <> (isset($_POST['oldPLP_STATUS_'.$Nr])?$_POST['oldPLP_STATUS_'.$Nr]:''))
	{
		$Felder .= ',PLP_STATUS=' . $_POST['txtPLP_STATUS_'.$Nr] . '';
	}
	if((isset($_POST['txtPLP_REIHENFOLGE_'.$Nr])?$_POST['txtPLP_REIHENFOLGE_'.$Nr]:'') <> (isset($_POST['oldPLP_REIHENFOLGE_'.$Nr])?$_POST['oldPLP_REIHENFOLGE_'.$Nr]:''))
	{
		$Felder .= ',PLP_REIHENFOLGE=' . $_POST['txtPLP_REIHENFOLGE_'.$Nr] . '';
	}


	if($Felder!='')
	{
		$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
		$SQL .= ' SET ' . substr($Felder,1);
		$SQL .= ', PLP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', PLP_USERDAT=SYSDATE';
		$SQL .= ' WHERE PLP_KEY=0'.$Nr;

		if(!awisExecute($comcon,$SQL))
		{
			awisErrorMailLink('30092259-Testzuordnungen',1,'Fehler beim Speichern',$awisDBError['message']);
		}
	}
}


//*************************************
// Daten l�schen
//*************************************

$Loeschen = awis_NameInArray($_POST,'cmdDEL_',0);
if($Loeschen!='')
{
	$Nr = intval(substr($Loeschen,7));

	$SQL = 'DELETE FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
	$SQL .= ' WHERE PLP_Key='.$Nr;

	if(!awisExecute($comcon,$SQL))
	{
		awisErrorMailLink('30092325-Testzuordnungen',1,'Fehler beim L�schen',$awisDBError['message']);
	}

}

echo '<form name=frmTestZuordnungen method=POST>';

//*********************************************
// Preislisten - Auswahl
//*********************************************

$SQL = 'SELECT * FROM EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB';
if(isset($_POST['txtID_FPLB']))
{
	$SQL .= ' WHERE ID_FPLB=0' . $_POST['txtID_FPLB'];
}
$SQL .= ' ORDER BY ID_FPLB';

$rsPLI = awisOpenRecordset($comcon,$SQL);
$rsPLIZeilen = $awisRSZeilen;

if(!isset($_POST['txtID_FPLB']))
{
	echo '<table border=0>';
	echo '<tr><td>Preisliste</td><td><select name=txtID_FPLB onchange=form.submit();>';
	echo '<option value=0>::Bitte w�hlen::</option>';
	for($PLIZeile=0;$PLIZeile<$rsPLIZeilen;$PLIZeile++)
	{
		echo '<option ';

		echo ' value=' . $rsPLI['ID_FPLB'][$PLIZeile] . '>';
		echo $rsPLI['BEZEICHNUNG'][$PLIZeile];
		echo '</option>';
	}

	echo '</select></td></tr>';
	echo '</table>';
}
else 		// Preisliste gew�hlt
{
	echo '<input type=hidden name=txtID_FPLB value='.$_POST['txtID_FPLB'].'>';
	echo '<h3>Testzuordnung '. $rsPLI['BEZEICHNUNG'][0] . '</h3><hr>';

	$SQL = 'SELECT * FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTEN ON PTA_ID=PLP_PTA_ID';
	$SQL .= ' WHERE PLP_ID_FPLB='.$_POST['txtID_FPLB'];
	$SQL .= ' ORDER BY PLP_REIHENFOLGE';

	$rsPLP = awisOpenRecordset($comcon,$SQL);
	$rsPLPZeilen = $awisRSZeilen;

	echo '<input name=txtPLP_ID_FPLB type=hidden value=' . $_POST['txtID_FPLB'] . '>';

	echo '<table border=1 class=DatenTabelle>';

	echo '<tr><td class=FeldBez>Testart</td>';
	echo '<td class=FeldBez>Status</td>';
	echo '<td class=FeldBez>Bemerkung</td>';
	echo '<td class=FeldBez>Reihenfolge</td>';
	echo '</tr>';

	for($PLPZeile=0;$PLPZeile<$rsPLPZeilen;$PLPZeile++)
	{
		echo '<tr>';

		echo '<td>';
		echo '' . $rsPLP['PTA_BEZEICHNUNG'][$PLPZeile] . '';
		echo '</td>';


		echo '<td>';
		echo '<input type=hidden name=oldPLP_STATUS_' . $rsPLP['PLP_KEY'][$PLPZeile] . ' value='.$rsPLP['PLP_STATUS'][$PLPZeile].'>';
		echo '<select name=txtPLP_STATUS_' . $rsPLP['PLP_KEY'][$PLPZeile] . '>';
		echo '<option value=0 ' . ($rsPLP['PLP_STATUS'][$PLPZeile]==0?' selected':'').'>Inaktiv</option>';
		echo '<option value=1 ' . ($rsPLP['PLP_STATUS'][$PLPZeile]==1?' selected':'').'>Aktiv</option>';
		echo '</select>';
		echo '</td>';

		echo '<td>';
		echo '<input type=hidden name=oldPLP_BEMERKUNG_' . $rsPLP['PLP_KEY'][$PLPZeile] . ' value=\'' . $rsPLP['PLP_BEMERKUNG'][$PLPZeile] . '\'>';
		echo '<input type=text size=100 name=txtPLP_BEMERKUNG_' . $rsPLP['PLP_KEY'][$PLPZeile] . ' value=\'' . $rsPLP['PLP_BEMERKUNG'][$PLPZeile] . '\'>';
		echo '</td>';

		echo '<td>';
		echo '<input type=hidden name=oldPLP_REIHENFOLGE_' . $rsPLP['PLP_KEY'][$PLPZeile] . ' value=\'' . $rsPLP['PLP_REIHENFOLGE'][$PLPZeile] . '\'>';
		echo '<input type=text size=5 name=txtPLP_REIHENFOLGE_' . $rsPLP['PLP_KEY'][$PLPZeile] . ' value=\'' . $rsPLP['PLP_REIHENFOLGE'][$PLPZeile] . '\'>';
		echo '</td>';

		echo '<td>';
		echo "<input type=image name=cmdDEL_" . $rsPLP['PLP_KEY'][$PLPZeile] . " src=/bilder/muelleimer.png title='Testzuordnung l�schen'>";
		echo '</td>';

		echo '</tr>';
	}

	echo '<tr>';


	//*************************
	// Neuer Test
	//*************************
	echo '<td><select name=txtPLP_PTA_ID_0>';
	echo '<option value=0>::Neuer Test::</option>';
	$rsPTA = awisOpenRecordset($comcon,'SELECT PTA_ID,PTA_BEZEICHNUNG FROM EXPERIAN_ATU.PLP_TESTARTEN WHERE PTA_ID NOT IN (SELECT PLP_PTA_ID FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN WHERE PLP_ID_FPLB=0'.$rsPLP['PLP_ID_FPLB'][0].')');
	$rsPTAZeilen = $awisRSZeilen;
	for($PTAZeile=0;$PTAZeile<$rsPTAZeilen;$PTAZeile++)
	{
		echo '<option ';
		echo ' value='.$rsPTA['PTA_ID'][$PTAZeile].'>';
		echo $rsPTA['PTA_BEZEICHNUNG'][$PTAZeile];
		echo '</option>';
	}
	echo '</select></td>';

	echo '</tr>';

	echo '</table>';


	echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo "&nbsp;<img src=/bilder/NeueListe.png accesskey='T' alt='�bersicht (Alt+T)' onclick=location.href='./preislisten_Main.php?cmdAktion=Testzuordnungen';>";
}

echo '</form>';

awisLogoff($con);
awisLogoff($comcon);

?>