<?php
global $con;
global $Recht10008;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZSZ','%');
$TextKonserven[]=array('ZLI','ZLI_BEZEICHNUNG');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','AktuellesSortiment');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10008 = awisBenutzerRecht($con,10008,$AWISBenutzer->BenutzerName());
if($Recht10008==0)
{
    awisEreignis(3,1000,'ZSZ',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdSuche_x']))
{
//awis_Debug(1,$_POST);
	$Param = '';
	$Param .= ';'.$_POST['sucZSZ_SORTIMENT'];
	$Param .= ';'.$_POST['sucZSZ_WG1'];
	$Param .= ';'.$_POST['sucZSZ_WG2'];
	$Param .= ';'.$_POST['sucZSZ_WG3'];
	$Param .= ';'.$_POST['sucZSZ_ZLI_KEY'];

	awis_BenutzerParameterSpeichern($con, "ZukaufZSZSuche" , $AWISBenutzer->BenutzerName() , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName() , '');
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	$Param = ';;;;;;;;;';
	include('./warengruppen_loeschen.php');
	if($AWIS_KEY1==0)
	{
		$Param = awis_BenutzerParameter($con, "ZukaufZSZSuche" , $AWISBenutzer->BenutzerName());
	}
	else
	{
		$Param = $AWIS_KEY1;
	}
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	$Param = ';;;';
	include('./warengruppen_speichern.php');
	$Param = awis_BenutzerParameter($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName());
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$Param = '-1;;;';
}
elseif(isset($_GET['ZSZ_KEY']))
{
	$Param = ''.$_GET['ZSZ_KEY'].';;;';		// Nur den Key speiechern
}
elseif(isset($_GET['WG1']))
{
	$Param = '';
	$Param .= ';';
	$Param .= ';'.$_GET['WG1'];
	$Param .= ';'.$_GET['WG2'];
	$Param .= ';'.$_GET['WG3'];
	$Param .= ';';

}
elseif(isset($_POST['txtZSZ_KEY']))
{
	$Param = ''.$_POST['txtZSZ_KEY'].';;;';		// Nur den Key speiechern
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	$Param='';
	if(!isset($_GET['Liste']))
	{
		$Param = awis_BenutzerParameter($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName());
	}
	else
	{
		awis_BenutzerParameterSpeichern($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName() , '');
	}
	if($Param=='' OR $Param=='0')
	{
		$Param = awis_BenutzerParameter($con, 'ZukaufZSZSuche', $AWISBenutzer->BenutzerName());
	}
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = '';
$Param = explode(';',$Param);

// Name angegeben?
if($Param[0]!='')		// Key
{
	$Bedingung .= 'AND ZSZ_Key = ' . intval($Param[0]) . ' ';
}
if(isset($Param[1]) AND $Param[1]!='')
{
	$Bedingung .= 'AND (UPPER(ZSZ_SORTIMENT) ' . awisLIKEoderIST($Param[1].'',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[2]) AND $Param[2]!='')
{
	$Bedingung .= 'AND (UPPER(ZSZ_WG1) ' . awisLIKEoderIST($Param[2].'',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[3]) AND $Param[3]!='')
{
	$Bedingung .= 'AND (UPPER(ZSZ_WG2) ' . awisLIKEoderIST($Param[3].'',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[4]) AND $Param[4]!='')
{
	$Bedingung .= 'AND (UPPER(ZSZ_WG3) ' . awisLIKEoderIST($Param[4].'',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[5]) AND $Param[5]>0)
{
	$Bedingung .= 'AND ZSZ_ZLI_KEY = ' . floatval($Param[5]) . ' ';
}

$SQL = 'SELECT Zukaufsortimentzuordnungen.*,ZLI_BEZEICHNUNG';
$SQL .= ' FROM Zukaufsortimentzuordnungen';
$SQL .= ' LEFT OUTER JOIN Zukauflieferanten ON ZSZ_ZLI_KEY = ZLI_KEY';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY ZSZ_SORTIMENT';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
awis_Debug(1,$SQL);
$rsZSZ = awisOpenRecordset($con, $SQL);
$rsZSZZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
if($rsZSZZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>'.$AWISSprachKonserven['Fehler']['err_keineDaten'].'</span>';
}
elseif($rsZSZZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './warengruppen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');

	$Link .= '&Sort=ZSZ_SORTIMENT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_SORTIMENT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'],200,'',$Link);
	$Link .= '&Sort=ZSZ_WG1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_WG1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_WG1'],100,'',$Link);
	$Link .= '&Sort=ZSZ_WG2'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_WG2'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_WG2'],100,'',$Link);
	$Link .= '&Sort=ZSZ_WG3'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_WG3'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_WG3'],100,'',$Link);
	$Link .= '&Sort=ZLI_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLI_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLI']['ZLI_BEZEICHNUNG'],300,'',$Link);
	$Link .= '&Sort=ZSZ_UNTERLIEFERANT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSZ_UNTERLIEFERANT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSZ']['ZSZ_UNTERLIEFERANT'],50,'',$Link);
	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZSZZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZSZZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./warengruppen_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	for($ZSZZeile=$StartZeile;$ZSZZeile<$rsZSZZeilen and $ZSZZeile<$StartZeile+$MaxDSAnzahl;$ZSZZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = './warengruppen_Main.php?cmdAktion=Details&ZSZ_KEY=0'.$rsZSZ['ZSZ_KEY'][$ZSZZeile].(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
		awis_FORM_Erstelle_ListenFeld('ZSZ_SORTIMENT',$rsZSZ['ZSZ_SORTIMENT'][$ZSZZeile],0,200,false,($ZSZZeile%2),'',$Link);
		$Link='';	// TODO: Sp�ter auf die Hersteller
		awis_FORM_Erstelle_ListenFeld('ZSZ_WG1',$rsZSZ['ZSZ_WG1'][$ZSZZeile],0,100,false,($ZSZZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('ZSZ_WG2',$rsZSZ['ZSZ_WG2'][$ZSZZeile],0,100,false,($ZSZZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('ZSZ_WG3',$rsZSZ['ZSZ_WG3'][$ZSZZeile],0,100,false,($ZSZZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('ZLI_BEZEICHNUNG',$rsZSZ['ZLI_BEZEICHNUNG'][$ZSZZeile],0,300,false,($ZSZZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('ZSZ_UNTERLIEFERANT',$rsZSZ['ZSZ_UNTERLIEFERANT'][$ZSZZeile],0,50,false,($ZSZZeile%2),'',$Link);
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
	awis_FORM_SchaltflaechenStart();
	if(($Recht10008&4) == 4)		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	awis_FORM_SchaltflaechenEnde();

}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmZSZAdressen action=./warengruppen_Main.php?cmdAktion=Details method=POST>';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsZSZ['ZSZ_KEY'][0])?$rsZSZ['ZSZ_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuellerZSZ" , $AWISBenutzer->BenutzerName() , $AWIS_KEY1);

	echo '<input type=hidden name=txtZSZ_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./warengruppen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZSZ['ZSZ_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZSZ['ZSZ_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$EditRecht=(($Recht10008&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=($Recht10008&6);
	}


	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_ZLI_KEY'].':',200);
	if($EditRecht)
	{
		$CursorFeld='txtZSZ_ZLI_KEY';
	}
	$SQL = 'SELECT ZLI_KEY, ZLI_BEZEICHNUNG FROM ZUKAUFLIEFERANTEN';
	$SQL .= ' ORDER BY ZLI_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ZSZ_ZLI_KEY',($AWIS_KEY1===0?'':$rsZSZ['ZSZ_ZLI_KEY'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_UNTERLIEFERANT'].':',200);
	awis_FORM_Erstelle_SelectFeld('ZSZ_UNTERLIEFERANT',($AWIS_KEY1===0?'':$rsZSZ['ZSZ_UNTERLIEFERANT'][0]),200,$EditRecht,$con,'','','','','',array('P~PV','T~TROST'));
	awis_FORM_ZeileEnde();

		// Nummer und Hersteller
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'].':',200);
	awis_FORM_Erstelle_TextFeld('ZSZ_SORTIMENT',($AWIS_KEY1===0?'':$rsZSZ['ZSZ_SORTIMENT'][0]),20,200,$EditRecht);
	awis_FORM_ZeileEnde();

		// Warengruppen
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG1'].':',200);
	awis_FORM_Erstelle_TextFeld('ZSZ_WG1',($AWIS_KEY1===0?'':$rsZSZ['ZSZ_WG1'][0]),10,100,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG2'].':',170);
	awis_FORM_Erstelle_TextFeld('ZSZ_WG2',($AWIS_KEY1===0?'':$rsZSZ['ZSZ_WG2'][0]),10,100,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG3'].':',170);
	awis_FORM_Erstelle_TextFeld('ZSZ_WG3',($AWIS_KEY1===0?'':$rsZSZ['ZSZ_WG3'][0]),10,100,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht10008&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if(($Recht10008&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht10008&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}
	awis_FORM_SchaltflaechenEnde();


	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsZSZ, $_POST, $rsAZG, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}