<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $DB;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('DPR','%');
	$TextKonserven[]=array('DPF','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_UngueltigeLaenderauswahl');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht120 = $AWISBenutzer->HatDasRecht(120);

	if(($Recht120)==0)
	{
	    awisEreignis(3,1000,'MBW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	//$Form->DebugAusgabe(1,$_GET);
	$Form->DebugAusgabe(1,$_POST);
	
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
	$Form->SchreibeHTMLCode('<form name="frmDienstleistungspreiseDetails" action="./dienstleistungspreise_Main.php?cmdAktion=Details" method="POST">');
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Dienstleistungspreise'));

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['DPR_BEZEICHNUNG'] = $_POST['sucDPR_BEZEICHNUNG'];
		$Param['DPR_NUMMER'] = $_POST['sucDPR_NUMMER'];
		$Param['INAKTIV'] = isset($_POST['sucInaktiv'])?$_POST['sucInaktiv']:'off';
		$Param['GESPERRT'] = isset($_POST['sucGesperrt'])?$_POST['sucGesperrt']:'off';
		$Param['BEDINGUNG'] = '';
		
		$Filialen = explode(';',$AWISBenutzer->ParameterLesen('DienstleistungsPreisFilialen'));
		$Bedingung  = '';
		foreach($Filialen AS $Filiale)
		{
			$FilialTeile = explode('=',$Filiale);
			$Param['LAND_'.$FilialTeile[0]]=isset($_POST['sucLAND_'.$FilialTeile[0]])?$_POST['sucLAND_'.$FilialTeile[0]]:'off';
			if ($Param['LAND_'.$FilialTeile[0]]=='on')
			{
    			$SQL = 'SELECT * FROM Laender WHERE LAN_CODE='.$DB->FeldInhaltFormat('T',$FilialTeile[0]);
    			$rsLAN = $DB->RecordSetOeffnen($SQL);
    			if(!$rsLAN->EOF())
    			{
					//L�nderkennzeichen f�r Ausgabe speichern
    			    $Param['LKZ_'.$FilialTeile[1]] = $FilialTeile[0];
    			    //W�hrung f�r Ausgabe speichern 
					$Param['WAE_'.$FilialTeile[1]] = $rsLAN->FeldInhalt('LAN_WAE_CODE'); 
    			    //Aktivkennung f�r Ausgabe speichern 
					$Param['AKTIV_'.$FilialTeile[1]] = 'DPR_AKTIV'.$FilialTeile[2]; 
					
				    $Bedingung .= ' OR (';
					$Bedingung .= ' DPF_FIL_ID='.$FilialTeile[1];
					if ($Param['INAKTIV']=='off')
					{
					    $Bedingung .= ' AND DPR_AKTIV'.$FilialTeile[2]."='J'";
					}
					$Bedingung .= ')';
    			}
			}
		}

		$Param['BEDINGUNG'] = $Bedingung;
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_Dienstleistungspreise",serialize($Param));

		if ($Param['BEDINGUNG'] == '')
		{
			echo '<span class=HinweisText>'.$AWISSprachKonserven['Fehler']['err_UngueltigeLaenderauswahl'].'</span>';
			$Form->SchaltflaechenStart();	
			$Form->Schaltflaeche('href','cmd_zurueck','./dienstleistungspreise_Main.php?cmdAktion=Suche','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			$Form->SchaltflaechenEnde();
			die();
		}
		
//$Form->DebugAusgabe(1,$Param);
//die();
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_Dienstleistungspreise',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}	

	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************
	if (!isset($_GET['Sort']))
	{
		if (isset($Param['ORDER']) and $Param['ORDER'] != '')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			if(isset($Param['DPR_BEZEICHNUNG']) AND $Param['DPR_BEZEICHNUNG']!='')
			{
				$ORDERBY = ' DPR_BEZEICHNUNG, DPR_NUMMER, DPF_FIL_ID';
			}
			elseif(isset($Param['DPR_NUMMER']) AND $Param['DPR_NUMMER']!='')
			{
				$ORDERBY = ' DPR_NUMMER, DPR_BEZEICHNUNG, DPF_FIL_ID';
			}
			else
			{
				$ORDERBY = ' DPR_NUMMER, DPR_BEZEICHNUNG, DPF_FIL_ID';
			}
		}
	}
	else
	{
		$ORDERBY = ' ' . str_replace ( '~', ' DESC ', $_GET ['Sort'] );
	}
	$Param['ORDER'] = $ORDERBY;
	$AWISBenutzer->ParameterSchreiben('Formular_Dienstleistungspreise',serialize($Param));	
	$Form->DebugAusgabe(1,$Param);
	
	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$BindeVariablen = array();
	$Bedingung = _BedingungErstellen($Param);
	
	$SQL  = 'SELECT DPR_NUMMER, DPR_BEZEICHNUNG, DPR_VK, DPR_KENN, DPF_FIL_ID, DPF_VK,';
	$SQL .= ' DPR_AKTIV1, DPR_AKTIV2, DPR_AKTIV3, DPR_AKTIV4, DPR_AKTIV5, DPR_AKTIV6,';
	$SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILENNR';
	$SQL .= ' FROM Dienstleistungspreise';
	$SQL .= ' INNER JOIN DienstleistungspreiseFilialen';
	$SQL .= ' ON DPR_NUMMER = DPF_DPR_NUMMER';
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}		
	$SQL .= ' ORDER BY ' .$ORDERBY;
	
	$Form->DebugAusgabe(1,$SQL);
//die();	
	//*****************************************************************
	// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll 
	//*****************************************************************
	if(($AWIS_KEY1<=0))
	{
		//************************************************
		// Aktuellen Datenblock festlegen
		//************************************************
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']) and ($Param['BLOCK'] != ''))
		{
			$Block=intval($Param['BLOCK']);
		}
	
		//************************************************
		// Zeilen begrenzen
		//************************************************
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);

		$BindeVariablen['var_N0_ZEILE_VON'] = $Form->Format('Z',$StartZeile,false);
		$BindeVariablen['var_N0_ZEILE_BIS'] = $Form->Format('Z',$StartZeile+$ZeilenProSeite,false) ;
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr >= :var_N0_ZEILE_VON AND  ZeilenNr < :var_N0_ZEILE_BIS';
		//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}
		
	$Form->DebugAusgabe(1,'Bindevariablen:', $BindeVariablen);
	$Form->DebugAusgabe(1,$SQL);
	
	$rsDienst = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	if($rsDienst->EOF())
	{
		echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
	} 
	elseif (($rsDienst->AnzahlDatensaetze()>0) or (isset($_REQUEST['Liste']))) // Liste anzeigen
	{
		$Form->Formular_Start();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['DPR']['txt_DienstleistungGesperrtFarbe'].':',500,'color:red');
		$Form->ZeileEnde();
		
		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		
		$Link = './dienstleistungspreise_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=DPR_NUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='DPR_NUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['DPR']['DPR_NUMMER'], 80, '', $Link );

		$Link = './dienstleistungspreise_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=DPR_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='DPR_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['DPR']['DPR_BEZEICHNUNG'], 400, '', $Link );
		
		$Form->Erstelle_Liste_Ueberschrift ($AWISSprachKonserven ['DPF']['DPF_VK'], 100, '', '', '', 'C');
		$Form->Erstelle_Liste_Ueberschrift ($AWISSprachKonserven ['DPF']['txt_Referenzfiliale'], 100, '', '', '', 'R');
		$Form->Erstelle_Liste_Ueberschrift ($AWISSprachKonserven ['DPR']['txt_Land'], 60, '', '', '', 'C');
		$Form->Erstelle_Liste_Ueberschrift ($AWISSprachKonserven ['DPR']['DPR_AKTIV'], 60, '', '', '', 'C');
		
		$Form->ZeileEnde ();

		$DS = 0;
		while (!$rsDienst->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
			$style = ($rsDienst->FeldInhalt('DPR_KENN')=='S'?'color:red':'');

			$Form->Erstelle_ListenFeld('DPR_NUMMER',$rsDienst->FeldInhalt('DPR_NUMMER'),0,80,false,($DS%2),$style,'','T','L');
			$Form->Erstelle_ListenFeld('DPR_BEZEICHNUNG',$rsDienst->FeldInhalt('DPR_BEZEICHNUNG'),0,400,false,($DS%2),$style,'','T','L');
			$Form->Erstelle_ListenFeld('DPF_VK',$Form->Format('N2',$rsDienst->FeldInhalt('DPF_VK')).' '.$Param['WAE_'.$rsDienst->FeldInhalt('DPF_FIL_ID')],0,100,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('DPF_FIL_ID',$rsDienst->FeldInhalt('DPF_FIL_ID'),0,100,false,($DS%2),'','','T','R');
			$Form->Erstelle_ListenFeld('LAN_CODE',$Param['LKZ_'.$rsDienst->FeldInhalt('DPF_FIL_ID')],0,60,false,($DS%2),'','','T','C');
			
			$Form->Erstelle_ListenFeld('DPR_AKTIV',$rsDienst->FeldInhalt($Param['AKTIV_'.$rsDienst->FeldInhalt('DPF_FIL_ID')]),0,60,false,($DS%2),'','','T','C');
			$Form->ZeileEnde();
			
			$DS ++;

			$rsDienst->DSWeiter();
		}
				
		$Link = './dienstleistungspreise_Main.php?cmdAktion=Details&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile ($MaxDS, $ZeilenProSeite, $Link, $Block, '');

		$Form->Formular_Ende();
	}	
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	
	$Form->SchaltflaechenStart();	
	//$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('href','cmd_zurueck','./dienstleistungspreise_Main.php?cmdAktion=Suche','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->SchaltflaechenEnde();

	$AWISBenutzer->ParameterSchreiben('Formular_Dienstleistungspreise',serialize($Param));
	
	$Form->SetzeCursor ($AWISCursorPosition );
	$Form->SchreibeHTMLCode ('</form>');
	die();
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201207301122");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

function _BedingungErstellen($Param)
{
	global $Form;
	global $AWISBenutzer;
	global $DB;
	
	$Bedingung = '';
	if(isset($Param['DPR_BEZEICHNUNG']) AND $Param['DPR_BEZEICHNUNG']!='')
	{
		$Bedingung .= ' AND UPPER(DPR_BEZEICHNUNG)' .$DB->LikeOderIst($Param['DPR_BEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER);
	}
	if(isset($Param['DPR_NUMMER']) AND $Param['DPR_NUMMER']!='')
	{
		$Bedingung .= ' AND UPPER(DPR_NUMMER)' .$DB->LikeOderIst($Param['DPR_NUMMER'],awisDatenbank::AWIS_LIKE_UPPER);
	}
	if(isset($Param['GESPERRT']) AND ($Param['GESPERRT']=='on'))
	{
	}
	else
	{
   	    $Bedingung .= " AND DPR_KENN<>'S' ";
	}
	if(isset($Param['BEDINGUNG']) AND $Param['BEDINGUNG']!='')
	{
		$Bedingung .= 'AND ('.substr($Param['BEDINGUNG'],3).')';
	}
	
	$Bedingung .= " AND (SUBSTR(DPR_NUMMER,1,1)<>'O')";
	$Bedingung .= " AND (SUBSTR(DPR_NUMMER,1,1)<>'C')";

	return $Bedingung;
}
?>