<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUB','%');
	$TextKonserven[]=array('ZLH','%');
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('ZWA','%');
	$TextKonserven[]=array('AST','%');
	$TextKonserven[]=array('ZLB','txtOnlineBestellung');
	$TextKonserven[]=array('ZLB','ZLB_VERSANDART');
	$TextKonserven[]=array('ZAP','ZAP_EK');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Wort','txt_EK_Aktuell');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

    $awisWerkzeug = new awisWerkzeuge();
	// TODO: Zum testen wg. Gro�-/Klein
//	$DB->Ausfuehren("ALTER SESSION SET NLS_COMP=LINGUISTIC");
//	$DB->Ausfuehren("ALTER SESSION SET NLS_SORT=BINARY_CI");


    if(isset($_GET['Fehlermelden']) and $_GET['Fehlermelden'] == true){
        $Mailtext = 'Link zum Datensatz: <a href="https://awis.server.atu.de/zukauf/zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=' . $_GET['ZUB_KEY'] .'></a><br>';
        $Mailtext .= 'Fehlertext: ' . $_POST['txtFehlerbeschreibung'];
        $awisWerkzeug->EMail(['shuttle@de.atu.eu'],'Zukauf-Fehler-Meldung ' . $AWISBenutzer->BenutzerName(),$Mailtext,2,'','shuttle@de.atu.eu',0,null,null,2);
    }

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10003 = $AWISBenutzer->HatDasRecht(10003);
	if ($Recht10003==0) {
	    awisEreignis(3,1000,'Zukaufbestellungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}


	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['ZUB_ARTIKELNUMMER'] = $_POST['sucZUB_ARTIKELNUMMER'];
		$Param['ZUB_ARTIKELBEZEICHNUNG'] = $_POST['sucZUB_ARTIKELBEZEICHNUNG'];
		$Param['ZUB_FIL_ID'] = $_POST['sucZUB_FIL_ID'];
		$Param['ZUB_LIE_NR'] = $_POST['sucZUB_LIE_NR'];
		if($Param['ZUB_LIE_NR']=='99999')
		{
		    $Param['ZUB_LIE_NR']='0255';
		    $Param['LKD_ZUSATZ1']='SAP';
		}
		else 
		{
		    $Param['LKD_ZUSATZ1']='';
		}
		$Param['ZUB_ZLH_KEY'] = $_POST['sucZUB_ZLH_KEY'];
		$Param['ZSZ_SORTIMENT'] = $_POST['sucZSZ_SORTIMENT'];
		$Param['DATUMBIS'] = $_POST['sucDATUMBIS'];
		$Param['DATUMVOM'] = $_POST['sucDATUMVOM'];
		$Param['AST_ATUNR'] = $_POST['sucAST_ATUNR'];
		$Param['ZUB_EXTERNEID'] = $_POST['sucZUB_EXTERNEID'];
		$Param['ZUB_WANR'] = $_POST['sucZUB_WANR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ZUB",serialize($Param));
		$AWISBenutzer->ParameterSchreiben('AktuellerZUB',serialize(''));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./zukaufbestellungen_loeschen.php');
		if($AWIS_KEY1==0)
		{
			$Param = $AWISBenutzer->ParameterLesen("Formular_ZUB");
		}
		else
		{
			$Param = $AWIS_KEY1;
		}
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufbestellungen_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUB'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUB'));
	}
	elseif(isset($_GET['ZUB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZUB_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUB'));
	}
	elseif(isset($_GET['ZUB_WANR']))
	{
		$Param = array();
		$Param['ZUB_ARTIKELNUMMER'] = '';
		$Param['ZUB_FIL_ID'] = '';
		$Param['ZUB_LIE_NR'] = '';
		$Param['ZUB_ZLH_KEY'] = '';
		$Param['ZSZ_SORTIMENT'] = '';
		$Param['DATUMBIS'] = '';
		$Param['DATUMVOM'] = '';
		$Param['AST_ATUNR'] = '';
		$Param['ZUB_EXTERNEID'] = '';
		$Param['ZUB_WANR'] = $_GET['ZUB_WANR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ZUB",serialize($Param));
		$AWISBenutzer->ParameterSchreiben('AktuellerZUB',serialize(''));

	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUB'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_ZUB',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZUB_BESTELLDATUM DESC';
			$Param['ORDER']='ZUB_BESTELLDATUM DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$BindeVariablen = array();
	$Bedingung = _BedingungErstellen($Param, $BindeVariablen);

	$SQL = 'SELECT Zukaufbestellungen.*, ZLH_KUERZEL, ZLH_BEZEICHNUNG, ZLA_AST_ATUNR, FIL_BEZ, LIE_NAME1, ZHK_CODE, LAN_CODE';
	//$SQL .= ', ZWA_VK, ZWA_MENGE';
	$SQL .= ',(SELECT TEMOT_NR FROM TEMOT_BESTELL@FILSYS.ATU.DE FILSYS WHERE BESTELL_KEY = ZUB_KEY ) AS TEMOT_NR';
	$SQL .= ', (SELECT COUNT(*) FROM Zukauflieferscheine WHERE ZUL_ZUB_KEY = ZUB_KEY) AS AnzLieferscheine';
	$SQL .= ', (SELECT SUM(ZUL_LIEFERMENGE) FROM Zukauflieferscheine WHERE ZUL_ZUB_KEY = ZUB_KEY AND ZUL_LIEFERMENGE < 0) AS Rueckgaben';
	$SQL .= ', (SELECT ZAI_ID FROM ZUKAUFLIEFERANTENARTIKELIDS WHERE ZAI_ZLA_KEY = ZLA_KEY AND ZAI_AID_NR = 4) AS ATUNR_OHNE';
	$SQL .= ', ZAP_EK, ZAP_KEY';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ", ZLB_KEY, CASE WHEN ZLB_VERSANDART IS NULL THEN '- -' ELSE FUNC_TEXTKONSERVE('ZLB_VERSANDART','Liste','".$AWISBenutzer->BenutzerSprache()."',ZLB_VERSANDART) END AS ZLB_VERSANDART, ZLB_STATUSTEXT, ZLB_USERDAT";
	$SQL .= ', (SELECT SUM(ZWA_MENGE) FROM ZUKAUFWERKSTATTAUFTRAEGE WHERE ZWA_ZUB_KEY = ZUB_KEY) AS ZWA_MENGE';
	$SQL .= ' FROM Zukaufbestellungen';
	$SQL .= ' LEFT OUTER JOIN ZukaufArtikel ON ZUB_ZLA_KEY = ZLA_KEY';
	$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ' LEFT OUTER JOIN ZukaufArtikellieferantenPreise ON ZAP_KEY = ZUB_ZAP_KEY';
	//$SQL .= ' LEFT OUTER JOIN TEMOT_BESTELL@FILSYS.ATU.DE FILSYS ON BESTELL_KEY = ZUB_KEY';
	//$SQL .= ' LEFT OUTER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
	$SQL .= ' LEFT OUTER JOIN FILIALEN ON FIL_ID = ZUB_FIL_ID';
	$SQL .= ' LEFT OUTER JOIN LAENDER ON FIL_LAN_WWSKENN = LAN_WWSKENN';
	$SQL .= ' LEFT OUTER JOIN LIEFERANTEN ON ZUB_LIE_NR = LIE_NR';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFLIEFERANTENBESTELLUNGEN ON ZUB_KEY = ZLB_ZUB_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFHERSTELLERCODES ON ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = ZUB_LIE_NR AND ZUB_HERSTELLER = ZHK_CODE';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	$Form->DebugAusgabe(1,$SQL);

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUB',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;


	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsZUB = $DB->RecordsetOeffnen($SQL, $BindeVariablen);

	$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);
	
	$AWISBenutzer->ParameterSchreiben('Formular_ZUB',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmZukaufBestellungen action=./zukaufbestellungen_Main.php?cmdAktion=Details method=POST>';

	if($rsZUB->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZUB->AnzahlDatensaetze()>1 AND !isset($_GET['ZUB_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_BESTELLDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_BESTELLDATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'],165,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_WANR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_WANR'],130,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_BESTELLMENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_BESTELLMENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'],60,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_ARTIKELNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'],190,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],60,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_ARTIKELBEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_ARTIKELBEZEICHNUNG'],350,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_PREISBRUTTO'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_PREISBRUTTO'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'],80,'text-align:right;',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],55,'','',$AWISSprachKonserven['ZUB']['ttt_ZUBSTATUS']);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_LIE_NR'],60);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZUB->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=0'.$rsZUB->FeldInhalt('ZUB_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZUB_BESTELLDATUM',$rsZUB->FeldInhalt('ZUB_BESTELLDATUM'),0,165,false,($DS%2),'',$Link,'DU');

			if($rsZUB->FeldInhalt('ZUB_WANRKORR')=='')
			{
			    if(strlen($rsZUB->FeldInhalt('ZUB_WANR'))==14)
			    {
			        $WAKorrekt =  $awisWerkzeug->BerechnePruefziffer($rsZUB->FeldInhalt('ZUB_WANR'),awisWerkzeuge::PRUEFZIIFER_TYP_AXBESTELLNR,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN);
			    }
			    else 
			    {
    				$WAKorrekt =  $awisWerkzeug->BerechnePruefziffer($rsZUB->FeldInhalt('ZUB_WANR'),awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN);
    				
			    }
			    $Form->Erstelle_ListenFeld('ZUB_WANR',(strlen($rsZUB->FeldInhalt('ZUB_WANR'))==14?'*':'').$rsZUB->FeldInhalt('ZUB_WANR'),0,130,false,($WAKorrekt==false?2:($DS%2)),'','','T');
			}
			else
			{
				$WAKorrekt =  $awisWerkzeug->BerechnePruefziffer($rsZUB->FeldInhalt('ZUB_WANRKORR'),awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN);
				$Form->Erstelle_ListenFeld('ZUB_WANR',$rsZUB->FeldInhalt('ZUB_WANRKORR'),0,130,false,($WAKorrekt==false?2:($DS%2)),($WAKorrekt==false?'color:white;':'color:red;'),'','T','',$rsZUB->FeldInhalt('ZUB_WANR'));
			}
			$Form->Erstelle_ListenFeld('ZUB_BESTELLMENGE',$rsZUB->FeldInhalt('ZUB_BESTELLMENGE'),0,60,false,($DS%2),'','','T','L');
			$Link = '../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUB->FeldInhalt('ZUB_ZLA_KEY');
			$Form->Erstelle_ListenFeld('ZUB_ARTIKELNUMMER',$rsZUB->FeldInhalt('ZUB_ARTIKELNUMMER'),0,190,false,($DS%2),'',$Link);
			$Link='';	// TODO: Sp�ter auf die Hersteller
			$Form->Erstelle_ListenFeld('ZLH_KUERZEL',$rsZUB->FeldInhalt('ZLH_KUERZEL'),0,60,false,($DS%2),'',$Link,'T','L',$rsZUB->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Form->Erstelle_ListenFeld('ZUB_ARTIKELBEZEICHNUNG',$rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG'),0,350,false,($DS%2),($rsZUB->FeldInhalt('ZUB_SORTIMENT')=='ZUKNEU'?'color:red;text-decoration:blink;':''),$Link,'T','L');
			$Form->Erstelle_ListenFeld('ZUB_PREISBRUTTO',$rsZUB->FeldInhalt('ZUB_PREISBRUTTO'),0,80,false,($DS%2),'',$Link,'N2','R');

			//****************************************
			// Status
			//****************************************
			// Abweichungen berechnen
			$Status='';
			$TippText = '';

			if($rsZUB->FeldInhalt('ZUB_PREISANGELIEFERT')!='')
			{
				$PreisAngeliefert = $DB->FeldInhaltFormat('N2',$rsZUB->FeldInhalt('ZUB_PREISANGELIEFERT'))/$DB->FeldInhaltFormat('N2',$rsZUB->FeldInhalt('ZUB_EINHEITENPROPREIS'));
				$Abweichung = ($DB->FeldInhaltFormat('N2',$rsZUB->FeldInhalt('ZUB_PREISBRUTTO')) - $PreisAngeliefert)/$PreisAngeliefert;
				if(abs($Abweichung) > 0.3)
				{
					$Status .= 'P';
					$TippText .= "/Abweichung >30%";
				}
				elseif(abs($Abweichung) > 0.2)
				{
					$Status .= 'p';
					$TippText .= "/Abweichung >20%";
				}
			}
			// Gibt es einen ATU Artikel
			if($rsZUB->FeldInhalt('ZLA_AST_ATUNR')!='')
			{
				$Status .= 'A';
				$TippText .= "/ATU Nummer bekannt";
			}
			elseif($rsZUB->FeldInhalt('ATUNR_OHNE')!='')
			{
				$Status .= 'a';
				$TippText .= "/ATU Nummer ohne Preiseinflu&szlig;";
			}
			// Ist die Bestellung bei den Filialen zu sehen?
			if($rsZUB->FeldInhalt('TEMOT_NR')=='')
			{
				$Status .= '!';
				$TippText .= "/Ist nicht in den Filialen";
			}
			// Ist die Bestellung abgerechnet?
			if($rsZUB->FeldInhalt('ZWA_MENGE')!='')
			{
			    if($rsZUB->FeldInhalt('ZWA_MENGE')>$rsZUB->FeldInhalt('ZUB_BESTELLMENGE'))
			    {
			        $Status .= 'b';
			        $TippText .= "/".$rsZUB->FeldInhalt('ZWA_MENGE').' Stueck verkauft';
			    }
			    else 
			    {
    				$Status .= 'B';
    				$TippText .= "/".$rsZUB->FeldInhalt('ZWA_MENGE').' Stueck verkauft';
			    }
			}
			if($rsZUB->FeldInhalt('RUECKGABEN')<0)
			{
				if($rsZUB->FeldInhalt('ZUB_BESTELLMENGE')>abs($rsZUB->FeldInhalt('RUECKGABEN')))
				{
					$Status .= 'r';
					$TippText .= "/".abs($rsZUB->FeldInhalt('RUECKGABEN')).' Stueck zurueck';
				}
				else
				{
					$Status .= 'R';
					$TippText .= "/ Komplett zurueck";
				}
			}
			if($rsZUB->FeldInhalt('ANZLIEFERSCHEINE')>0)
			{
				$Status .= 'L';
				$TippText .= "/".$rsZUB->FeldInhalt('ANZLIEFERSCHEINE').' Lieferschein(e) vorhanden';
			}

			$Form->Erstelle_ListenFeld('STATUS',$Status,0,55,false,($DS%2),'','','T','',substr($TippText,1));

			$Form->Erstelle_ListenFeld('!ZUB_LIE_NR',$rsZUB->FeldInhalt('ZUB_LIE_NR'),0,60,false,($DS%2),'',$Link,'T','R',$rsZUB->FeldInhalt('LIE_NAME1'));

			$Form->ZeileEnde();

			$rsZUB->DSWeiter();
			$DS++;
		}

		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZUB->FeldInhalt('ZUB_KEY');

		$AWISBenutzer->ParameterSchreiben("AktuellerZUB",serialize($AWIS_KEY1));
		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_ZUB',serialize($Param));

		echo '<input type=hidden name=txtZUB_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufbestellungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUB->FeldInhalt('ZUB_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUB->FeldInhalt('ZUB_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10003&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10003&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_LIE_NR'].':',200);
		if($EditRecht)
		{
			$AWISCursorPosition='txtZUB_LIE_NR';
		}
		$SQL = 'SELECT LIE_NR, LIE_NAME1 ';
		$SQL .= ' FROM LIEFERANTEN';
		$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('ZUB_LIE_NR',$rsZUB->FeldInhalt('ZUB_LIE_NR'),'350:350',$EditRecht,$SQL);




        $Form->Schaltflaeche('script','cmdFehlermelden',$Form->PopupOeffnen(420),'/bilder/cmd_antenne.png','Fehler melden');

        ob_start();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Fehlerbeschreibung: ', 100);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_Textarea('Fehlerbeschreibung','',250,50,9,true);
        $Form->ZeileEnde();

        $Inhalt = ob_get_clean();

        $Schaltflaechen = [
            ['/bilder/cmd_dsloeschen.png','close','close','close'],
            ['/bilder/cmd_weiter.png','submit','script','','','cmdFehlermeldenSend']
        ];

        $Script = '$(document).ready(function(){';
        $Script .= '$("#cmdFehlermeldenSend").click(function(){';
        $Script .= '   $(\'form[name="frmZukaufBestellungen"]\').attr("action","./zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$AWIS_KEY1.'&Fehlermelden=true");';
        $Script .= '   $(\'form[name="frmZukaufBestellungen"]\').submit();';
        $Script .= '});';
        $Script .= '});';

        echo '<script>'.$Script.'</script>';
        
        $Form->PopupDialog('Fehler melden.',$Inhalt ,$Schaltflaechen,awisFormular::POPUP_WARNUNG,'420');

      	$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_EXTERNEID'].':',200);
		$Form->Erstelle_TextFeld('ZUB_EXTERNEID',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_EXTERNEID')),50,600,$rsZUB->FeldInhalt('ZUB_KEY')=='','','','','T');
		$Form->ZeileEnde();

			// Bestellinformationen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'].':',200);
		$Form->Erstelle_TextFeld('!ZUB_BESTELLDATUM',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_BESTELLDATUM')),17,200,$EditRecht,'','','','DU');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_FIL_ID'].':',190);
		$Form->Erstelle_TextFeld('!ZUB_FIL_ID',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_FIL_ID')),5,200,$EditRecht,'','','','T');
		$Link = ($rsZUB->FeldInhalt('ZUB_FIL_ID')==''?'':'/filialinfos/filialinfos_Main.php?cmdAktion=Details&Seite=Zukauflieferanten&FIL_ID='.$rsZUB->FeldInhalt('ZUB_FIL_ID'));
		$Form->Erstelle_TextFeld('#FIL_BEZ',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('FIL_BEZ')),20,200,false,'','',$Link,'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANR'].':',200);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_WANR='.$rsZUB->FeldInhalt('ZUB_WANR');
	 	$Form->Erstelle_TextFeld('ZUB_WANR',$rsZUB->FeldInhalt('ZUB_WANR'),20,200,false,'','',$Link,'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANRKORR'].':',190);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_WANR='.$rsZUB->FeldInhalt('ZUB_WANRKORR');
		$Form->Erstelle_TextFeld(($rsZUB->FeldInhalt('ZUB_KEY')!=''?'':'!').'ZUB_WANRKORR',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_WANRKORR')),20,200,$EditRecht,'','',$Link,'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_VERSANDARTBEZ'].':',200);
		$Form->Erstelle_TextFeld('ZUB_VERSANDARTBEZ',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_VERSANDARTBEZ')),20,200,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WAEHRUNG'].':',190);
		$Form->Erstelle_TextFeld('ZUB_WAEHRUNG',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_WAEHRUNG')),10,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'].':',200);
		$Form->Erstelle_TextFeld('!ZUB_BESTELLMENGE',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_BESTELLMENGE')),7,100,$EditRecht,'','','','N3');
		$Form->Erstelle_TextLabel(($rsZUB->FeldInhalt('ZWA_MENGE')==''?'- -':'/'.$rsZUB->FeldInhalt('ZWA_MENGE')),30,($rsZUB->FeldInhalt('ZWA_MENGE')!=$rsZUB->FeldInhalt('ZUB_BESTELLMENGE')?'':'background:#3DB745'));
		$Form->Erstelle_TextLabel('',70);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_MENGENEINHEITEN'].':',190);
		$Form->Erstelle_TextFeld('ZUB_MENGENEINHEITEN',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_MENGENEINHEITEN')),10,200,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_EINHEITENPROPREIS'].':',190);
		$Form->Erstelle_TextFeld('!ZUB_EINHEITENPROPREIS',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_EINHEITENPROPREIS')),10,200,$EditRecht,'','','','T','','',1);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'].':',200);
		$Form->Erstelle_TextFeld('!ZUB_PREISBRUTTO',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_PREISBRUTTO')),10,200,$EditRecht,'','','','N2');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_PREISANGELIEFERT'].':',190);
		$Form->Erstelle_TextFeld('ZUB_PREISANGELIEFERT',$rsZUB->FeldInhalt('ZUB_PREISANGELIEFERT'),10,200,$EditRecht,'','','','N2');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ALTTEILWERT'].':',190);
		$Form->Erstelle_TextFeld('ZUB_ALTTEILWERT',$rsZUB->FeldInhalt('ZUB_ALTTEILWERT'),10,200,$EditRecht,'','','','N2');
		$Form->ZeileEnde();
		

		if($rsZUB->FeldInhalt('ZUB_ZUSATZBETRAG')<>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZUSATZBETRAG'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_ZUSATZBETRAG',$rsZUB->FeldInhalt('ZUB_ZUSATZBETRAG'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZUSATZBETRAGTYP'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_ZUSATZBETRAGTYP',$rsZUB->FeldInhalt('ZUB_ZUSATZBETRAGTYP'),20,200,$EditRecht,'','','','T');
		    $Form->ZeileEnde();
		}
		
		if($rsZUB->FeldInhalt('ZUB_UMWELTPAUSCHALEMENGE')>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_UMWELTPAUSCHALE'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_UMWELTPAUSCHALE',$rsZUB->FeldInhalt('ZUB_UMWELTPAUSCHALE'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_UMWELTPAUSCHALEMENGE'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_UMWELTPAUSCHALEMENGE',$rsZUB->FeldInhalt('ZUB_UMWELTPAUSCHALEMENGE'),5,100,$EditRecht,'','','','Nx');
		    $Form->ZeileEnde();
		}
		
		if($rsZUB->FeldInhalt('ZUB_WIEDEREINLAGERUNGMENGE')>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WIEDEREINLAGERUNG'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_WIEDEREINLAGERUNG',$rsZUB->FeldInhalt('ZUB_WIEDEREINLAGERUNG'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WIEDEREINLAGERUNGMENGE'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_WIEDEREINLAGERUNGMENGE',$rsZUB->FeldInhalt('ZUB_WIEDEREINLAGERUNGMENGE'),5,100,$EditRecht,'','','','Nx');
		    $Form->ZeileEnde();
		}
		
		if($rsZUB->FeldInhalt('ZUB_SONSTIGEMENGE')>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_SONSTIGEKOSTEN'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_SONSTIGEKOSTEN',$rsZUB->FeldInhalt('ZUB_SONSTIGEKOSTEN'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_SONSTIGEMENGE'].':',200);
		    $Form->Erstelle_TextFeld('ZUB_SONSTIGEMENGE',$rsZUB->FeldInhalt('ZUB_SONSTIGEMENGE'),5,100,$EditRecht,'','','','Nx');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_SONSTIGEBEZEICHNUNG'].':',100);
		    $Form->Erstelle_TextFeld('ZUB_SONSTIGEBEZEICHNUNG',$rsZUB->FeldInhalt('ZUB_SONSTIGEBEZEICHNUNG'),36,200,$EditRecht,'','','','T');
		    $Form->ZeileEnde();
		}
		
		
		// EK nicht allen anzeigen
		if($Recht10003&32 != 0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_EK'].':',200);
			$Form->Erstelle_TextFeld('ZUB_EK',$rsZUB->FeldInhalt('ZUB_EK'),10,200,$EditRecht,'','','','N2');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_EK_Aktuell'].':',190);
			if($rsZUB->FeldInhalt('ZAP_EK')=='')
			{
			    $Form->Erstelle_TextFeld('EKHINWEIS','- -',10,200,false,'','','','T');
			}
			else
			{
                $Form->Erstelle_TextFeld('ZAP_EK',$rsZUB->FeldInhalt('ZAP_EK'),10,200,false,'','','','N2');
			}
			$Form->ZeileEnde();
		}		

		// Onlinebestellung anzeigen, wenn was existiert
		if($Recht10003&64 != 0 AND $rsZUB->FeldInhalt('ZLB_VERSANDART')!='')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLB']['txtOnlineBestellung'].':',200);
			$Form->Erstelle_TextFeld('#ZLB_USERDAT',$rsZUB->FeldInhalt('ZLB_USERDAT'),10,200,false,'','','','DU');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLB']['ZLB_VERSANDART'].':',190);
			$Form->Erstelle_TextFeld('#ZLB_VERSANDART',$rsZUB->FeldInhalt('ZLB_VERSANDART'),10,200,false,'','','','T');
			$Form->ZeileEnde();
		}		
		
		if($rsZUB->FeldInhalt('ZUB_ZLA_KEY')=='')
		{
				// Nummer und Hersteller
			$Form->ZeileStart();
			$Link = ($rsZUB->FeldInhalt('ZUB_ZLA_KEY')=='')?'':'../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUB->FeldInhalt('ZUB_ZLA_KEY');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLH_KEY'].':',180);
			if($EditRecht)
			{
				$AktuelleDaten = ($AWIS_KEY1===0?'':array($rsZUB->FeldInhalt('ZHK_CODE').'~'.$rsZUB->FeldInhalt('ZLH_KUERZEL').' - '.$rsZUB->FeldInhalt('ZLH_BEZEICHNUNG')));
				$Form->Erstelle_SelectFeld('!ZUB_HERSTELLER',$rsZUB->FeldInhalt('ZUB_HERSTELLER'),"400:360",$EditRecht,'***ZLH_Daten;txtZUB_HERSTELLER;LIE_NR=*txtZUB_LIE_NR&WERT='.($rsZUB->FeldInhalt('ZUB_HERSTELLER')).'','','','','',$AktuelleDaten);
			}
			else
			{
				$Form->Erstelle_TextFeld('HERSTELLER',$rsZUB->FeldInhalt('ZHK_CODE').' - '.$rsZUB->FeldInhalt('ZLH_BEZEICHNUNG'),20,400,false);
			}
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'].':',190,'',$Link);
			$Form->Erstelle_TextFeld('*ARTNUMMER',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_ARTIKELNUMMER')),20,180,$EditRecht,'','background:#50FF50');
			$Form->Erstelle_SelectFeld('ZUB_ZAL_KEY',$rsZUB->FeldInhalt('ZUB_ZAL_KEY'),"220:180",$EditRecht,'***ZLA_Daten;txtZUB_ZAL_KEY;ARTNUMMER=*sucARTNUMMER&LIE_NR=*txtZUB_LIE_NR&ZLH_KEY=*txtZUB_HERSTELLER','','','','',$AktuelleDaten);
		}
		else
		{
			// Nummer und Hersteller
			$Form->ZeileStart();
			$Link = ($rsZUB->FeldInhalt('ZUB_ZLA_KEY')=='')?'':'../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUB->FeldInhalt('ZUB_ZLA_KEY');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'].':',200,'',$Link);
			$Form->Erstelle_TextFeld('ZUB_ARTIKELNUMMER',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_ARTIKELNUMMER')),20,200,$EditRecht);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLH_KEY'].':',170);
			if($EditRecht)
			{
				$AktuelleDaten = ($AWIS_KEY1===0?'':array($rsZUB->FeldInhalt('ZHK_CODE').'~'.$rsZUB->FeldInhalt('ZLH_KUERZEL').' - '.$rsZUB->FeldInhalt('ZLH_BEZEICHNUNG')));
				$Form->Erstelle_SelectFeld('ZUB_HERSTELLER',$rsZUB->FeldInhalt('ZUB_HERSTELLER'),"400:360",$EditRecht,'***ZLH_Daten;txtZUB_HERSTELLER;LIE_NR=*txtZUB_LIE_NR&WERT='.($rsZUB->FeldInhalt('ZUB_HERSTELLER')).'','','','','',$AktuelleDaten);
			}
			else
			{
				$Form->Erstelle_TextFeld('HERSTELLER',$rsZUB->FeldInhalt('ZHK_CODE').' - '.$rsZUB->FeldInhalt('ZLH_BEZEICHNUNG'),20,400,false);
			}
		}
		$Form->ZeileEnde();

			// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELBEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('!ZUB_ARTIKELBEZEICHNUNG',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG')),50,500,$EditRecht);
		$Form->ZeileEnde();

			// ATU Daten
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_SORTIMENT'].':',200);
		// Direkt aus dem ATU Artikel, da die hier angelegt sein m�ssen (FILSYS)
		$SQL = "SELECT AST_ATUNR, AST_ATUNR AS ANZEIGE FROM ARTIKELSTAMM WHERE AST_ATUNR LIKE 'ZUK___'";
		$Form->Erstelle_SelectFeld('ZUB_SORTIMENT',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_SORTIMENT')),200,$EditRecht,$SQL);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_TAUSCHTEIL'].':',190);
		$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
		$Form->Erstelle_SelectFeld('ZUB_TAUSCHTEIL',$rsZUB->FeldInhalt('ZUB_TAUSCHTEIL'),200,$EditRecht,'','','0','','',$Daten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_AUFTRAGSNREXTERN'].':',200);
		$Form->Erstelle_TextFeld('ZUB_AUFTRAGSNREXTERN',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_AUFTRAGSNREXTERN')),50,200,$EditRecht);
		$Form->ZeileEnde();


				// Abschluss
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ABSCHLUSSART'].':',200);
		$Liste = explode("|",$AWISSprachKonserven['ZUB']['lst_ZUB_ABSCHLUSSART']);
		$Form->Erstelle_SelectFeld('ZUB_ABSCHLUSSART',$rsZUB->FeldInhalt('ZUB_ABSCHLUSSART'),"400:380",$EditRecht,'','','1','','',$Liste,'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ABSCHLUSSGRUND'].':',200);
		$Form->Erstelle_TextFeld('ZUB_ABSCHLUSSGRUND',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_ABSCHLUSSGRUND')),50,400,$EditRecht);
		$Form->ZeileEnde();

		if($rsZUB->FeldInhalt('ZLA_AST_ATUNR')!='')
		{
			$Form->Trennzeile();
			$LAN_CODE = $rsZUB->FeldInhalt('LAN_CODE');
			if($LAN_CODE=='')
			{
				$LAN_CODE = 'DE';
			}

			if($LAN_CODE=='DE')
			{
				$SQL = 'SELECT AST_VK, AST_ATUNR, AST_BEZEICHNUNGWW';
				$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS VP';
				$SQL .= ', (SELECT ATW_BETRAG FROM ArtikelStammInfos ';
				$SQL .= '  	INNER JOIN ALTTEILWERTE ON ATW_LAN_CODE = :var_T_LAN_CODE AND ATW_KENNUNG = ASI_WERT';
				$SQL .= '     WHERE ASI_AIT_ID=190 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS ATW_BETRAG';
				$SQL .= ',(SELECT ATW_BETRAG FROM Altteilwerte WHERE ATW_KENNUNG = (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND asi_ast_atunr=:var_T_ASI_AST_ATUNR AND asi_ait_id=190) AND ATW_LAN_CODE=:var_T_LAN_CODE) AS AST_ALTTEILWERT';
				$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND  asi_ast_atunr=:var_T_ASI_AST_ATUNR AND asi_ait_id=191) AS AST_VPE';
				$SQL .= ' FROM Artikelstamm ';
				$SQL .= ' WHERE AST_ATUNR = :var_T_ASI_AST_ATUNR';
			}
			else
			{
				$SQL = 'SELECT AST_ATUNR, AST_BEZEICHNUNGWW';
				$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=:var_T_ASI_AIT_ID AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS AST_VK';
				$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS VP';
				$SQL .= ', (SELECT ATW_BETRAG FROM ArtikelStammInfos ';
				$SQL .= '  	INNER JOIN ALTTEILWERTE ON ATW_LAN_CODE = :var_T_LAN_CODE AND ATW_KENNUNG = ASI_WERT';
				$SQL .= '     WHERE ASI_AIT_ID=190 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS ATW_BETRAG';
				$SQL .= ',(SELECT ATW_BETRAG FROM Altteilwerte WHERE ATW_KENNUNG = (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND asi_ast_atunr=:var_T_ASI_AST_ATUNR AND asi_ait_id=190) AND ATW_LAN_CODE=:var_T_LAN_CODE) AS AST_ALTTEILWERT';
				$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND  asi_ast_atunr=:var_T_ASI_AST_ATUNR AND asi_ait_id=191) AS AST_VPE';
				$SQL .= ' FROM ZukaufArtikel ';
				$SQL .= ' INNER JOIN Artikelstamm ON ZLA_AST_ATUNR = AST_ATUNR';
				$SQL .= ' WHERE AST_ATUNR = :var_T_ASI_AST_ATUNR';
			}

			$DB->SetzeBindevariable('AST', ':var_T_LAN_CODE', $LAN_CODE);
			$DB->SetzeBindevariable('AST', ':var_T_ASI_AST_ATUNR', $rsZUB->FeldInhalt('ZLA_AST_ATUNR'));

			switch($LAN_CODE)
			{
				case 'DE':
					break;
				case 'AT':
					$DB->SetzeBindevariable('AST', 'var_T_ASI_AIT_ID', 50);
					break;
				case 'IT':
					$DB->SetzeBindevariable('AST', 'var_T_ASI_AIT_ID', 53);
					break;
				case 'CZ':
					$DB->SetzeBindevariable('AST', 'var_T_ASI_AIT_ID', 51);
					break;
				case 'CH':
					$DB->SetzeBindevariable('AST', 'var_T_ASI_AIT_ID', 54);
					break;
				case 'NL':
					$DB->SetzeBindevariable('AST', 'var_T_ASI_AIT_ID', 52);
					break;
			}

			$rsAST = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('AST'));

			if(!$rsAST->EOF())
			{
				$Form->ZeileStart();
				$Link = '/artikelstamm/artikelstamm_Main.php?AST_ATUNR='.urlencode($rsAST->FeldInhalt('AST_ATUNR')).'&cmdAktion=Artikelinfo';
				$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'');
				$Form->Erstelle_TextFeld('AST_ATUNR',$rsAST->FeldInhalt('AST_ATUNR'),20,200,false,'','',$Link);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':',200);
				$Form->Erstelle_TextFeld('AST_BEZEICHNUNGWW',$rsAST->FeldInhalt('AST_BEZEICHNUNGWW'),20,500,false);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'].' - '.$LAN_CODE.':',200);
				$Form->Erstelle_TextFeld('AST_VK',$rsAST->FeldInhalt('AST_VK'),20,200,false,'','','','N2');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ALTTEILWERT'].':',200);
				$Form->Erstelle_TextFeld('AST_ALTTEILWERT',$rsAST->FeldInhalt('AST_ALTTEILWERT'),20,200,false,'','','','N2');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VPE'].':',200);
				$Form->Erstelle_TextFeld('AST_VPE',$rsAST->FeldInhalt('AST_VPE'),20,200,false,'','','','N0');
				$Form->ZeileEnde();
			}
		}

        if($rsZUB->FeldInhalt('ZUB_KEY')!='')
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_GEPRUEFT'].':',200);
            if($rsZUB->FeldInhalt('ZUB_GEPRUEFT')=='')
            {
                $Form->Erstelle_Checkbox('GEPRUEFT', '', 50,$EditRecht);
            }
            else
            {
                $Form->Erstelle_TextLabel($rsZUB->FeldInhalt('ZUB_GEPRUEFT'),400);
            }
            $Form->ZeileEnde();
        }

        $Form->Formular_Ende();

		if($AWIS_KEY1>0)
		{
			$Reg = new awisRegister(10006);
			$Reg->ZeichneRegister(isset($_GET['Seite'])?$_GET['Seite']:'Lieferscheine');

		}
	}

	//awis_Debug(1, $Param, $Bedingung, $rsZUB, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht10003&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht10003&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param, &$Bindevariablen)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Form = new awisFormular();
	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		//$Bedingung.= ' AND ZUB_KEY = '.floatval($AWIS_KEY1);
		$Bedingung .= ' AND ZUB_KEY = :var_N0_zub_key ';
		$Bindevariablen['var_N0_zub_key'] = $AWIS_KEY1;
		return $Bedingung;
	}
//var_dump($Param);

	if(isset($Param['ZUB_ARTIKELNUMMER']) AND $Param['ZUB_ARTIKELNUMMER']!='')
	{
		//$Bedingung .= ' AND (ZUB_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUB_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ')';

		if(strpos($Param['ZUB_ARTIKELNUMMER'],'*')===false)
		{
			$Bedingung .= ' AND (ZUB_ARTIKELNUMMER) = LARTNRKOMP(:var_T_zub_artikelnummer)';
			$Bindevariablen['var_T_zub_artikelnummer'] = '\''.$Param['ZUB_ARTIKELNUMMER'].'\'';
		}
		else
		{
			$ParameterWert = str_replace('\'', '', trim($DB->LIKEoderIST($Param['ZUB_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER)));
			$ParameterWert= explode(' ', $ParameterWert);
			//$Bedingung .= ' AND UPPER(ZUB_ARTIKELNUMMER) '.$ParameterWert[0].' :var_T_zub_artikelnummer';
			$Bedingung .= ' AND (ZUB_ARTIKELNUMMER) '.$ParameterWert[0].' :var_T_zub_artikelnummer';
			array_shift($ParameterWert);
			$Bindevariablen['var_T_zub_artikelnummer'] = implode(' ', $ParameterWert);
		}
	}

	if(isset($Param['ZUB_ARTIKELBEZEICHNUNG']) AND $Param['ZUB_ARTIKELBEZEICHNUNG']!='')
	{
		//$Bedingung .= ' AND UPPER(ZUB_ARTIKELBEZEICHNUNG) ' . $DB->LIKEoderIST($Param['ZUB_ARTIKELBEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER) . '';

		$ParameterWert = str_replace('\'', '', trim($DB->LIKEoderIST($Param['ZUB_ARTIKELBEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER)));
		$ParameterWert = explode(' ', $ParameterWert);
		//$Bedingung .= ' AND UPPER(ZUB_ARTIKELBEZEICHNUNG) '.$ParameterWert[0].' :var_T_zub_artikelbezeichnung';
		$Bedingung .= ' AND (ZUB_ARTIKELBEZEICHNUNG) '.$ParameterWert[0].' :var_T_zub_artikelbezeichnung';
		array_shift($ParameterWert);
		$Bindevariablen['var_T_zub_artikelbezeichnung'] = implode(' ', $ParameterWert);
	}

	if(isset($Param['ZUB_ZLH_KEY']) AND $Param['ZUB_ZLH_KEY']>0)
	{
		$Bedingung .= ' AND EXISTS (SELECT * FROM ZUKAUFHERSTELLERCODES';
		$Bedingung .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLH_KEY = ZHK_ZLH_KEY';
		$Bedingung .= ' WHERE ZLH_KEY = ' . $DB->FeldInhaltFormat('N0',$Param['ZUB_ZLH_KEY']);
		$Bedingung .= ' AND ZHK_CODE = ZUB_HERSTELLER';
		$Bedingung .= ' )';
	}
	if(isset($Param['ZUB_FIL_ID']) AND $Param['ZUB_FIL_ID']!='')
	{
		//$Bedingung .= ' AND ZUB_FIL_ID = ' . $Param['ZUB_FIL_ID'] . ' ';
		$Bindevariablen['var_N0_zub_fil_id']=$Param['ZUB_FIL_ID'];
		$Bedingung .= ' AND ZUB_FIL_ID = :var_N0_zub_fil_id ';
	}

	if(isset($Param['ZUB_LIE_NR']) AND $Param['ZUB_LIE_NR']!='')
	{
		//$Bedingung .= ' AND ZUB_LIE_NR = ' . $Param['ZUB_LIE_NR'] . ' ';
		$Bindevariablen['var_T_zub_lie_nr']=$Param['ZUB_LIE_NR'];
		$Bedingung .= ' AND ZUB_LIE_NR = :var_T_zub_lie_nr ';
	}

	if(isset($Param['ZSZ_SORTIMENT']) AND $Param['ZSZ_SORTIMENT']!='')
	{
		//$Bedingung .= ' AND ZUB_SORTIMENT = ' . $DB->FeldInhaltFormat('T',$Param['ZSZ_SORTIMENT']) . ' ';
		$Bindevariablen['var_T_ZUB_SORTIMENT']=$Param['ZSZ_SORTIMENT'];
		$Bedingung .= ' AND ZUB_SORTIMENT = :var_T_ZUB_SORTIMENT ';
	}

	if(isset($Param['DATUMVOM']) AND $Param['DATUMVOM']!='')
	{
		//$Bedingung .= ' AND ZUB_BESTELLDATUM >= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMVOM']) . ' ';
		$Bindevariablen['var_D_datumvom']=$Form->Format('DU',$Param['DATUMVOM']);
		$Bedingung .= ' AND ZUB_BESTELLDATUM >= :var_D_datumvom ';
	}
	if(isset($Param['DATUMBIS']) AND $Param['DATUMBIS']!='')
	{
		//$Bedingung .= ' AND ZUB_BESTELLDATUM <= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMBIS']) . ' ';
		$Bindevariablen['var_D_datumbis']=$Form->Format('DU',$Param['DATUMBIS']);
		$Bedingung .= ' AND ZUB_BESTELLDATUM <= :var_D_datumbis ';
	}
	if(isset($Param['ZUB_EXTERNEID']) AND $Param['ZUB_EXTERNEID']!='')
	{
		//$Bedingung .= ' AND ZUB_EXTERNEID ' . $DB->LIKEoderIST($Param['ZUB_EXTERNEID'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bindevariablen['var_T_zub_externeid']=$Form->Format('TU',$Param['ZUB_EXTERNEID']);
		$Bedingung .= ' AND ZUB_EXTERNEID = :var_T_zub_externeid ';
	}
	if(isset($Param['ZUB_WANR']) AND $Param['ZUB_WANR']!='')
	{
		//$Bedingung .= ' AND (ZUB_WANR ' . $DB->LIKEoderIST($Param['ZUB_WANR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		//$Bedingung .= ' OR ZUB_WANRKORR ' . $DB->LIKEoderIST($Param['ZUB_WANR'],awisDatenbank::AWIS_LIKE_UPPER) . ') ';
		$ParameterWert = str_replace('\'','',trim($DB->LIKEoderIST($Param['ZUB_WANR'],awisDatenbank::AWIS_LIKE_UPPER)));
		$ParameterWert = explode(' ', $ParameterWert);
		$Bedingung .= ' AND (ZUB_WANR  '.$ParameterWert[0].' :var_T_zub_wanr';
		$Bedingung .= ' OR ZUB_WANRKORR '.$ParameterWert[0].' :var_T_zub_wanr ) ';
		array_shift($ParameterWert);
		$Bindevariablen['var_T_zub_wanr'] = implode(' ', $ParameterWert);

	}

	if(isset($Param['ZUB_ZSZ_KEY']) AND $Param['ZUB_ZSZ_KEY']>0)
	{
		//$Bedingung .= ' AND ZUB_ZSZ_KEY= ' . intval($Param['ZUB_ZSZ_KEY']) . ' ';
		$Bedingung .= ' AND ZUB_ZSZ_KEY= :var_N0_zub_zsz_key ';
		$Bindevariablen['var_N0_zub_zsz_key']=$Param['ZUB_ZSZ_KEY'];
	}
	if(isset($Param['AST_ATUNR']) AND $Param['AST_ATUNR']!='')
	{
		//$Bedingung .= ' AND (ZAL_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ' AND (ZLA_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['ZUB_TAUSCHTEIL']) AND $Param['ZUB_TAUSCHTEIL']>0)
	{
		$Bedingung .= ' AND ZUB_TAUSCHTEIL = ' . intval($Param['ZUB_TAUSCHTEIL']) . ' ';
	}

	// Zusatzpr�fung f�r die SAP Filialen von TROST. Kann sp�ter wieder entfernt werden
	if(isset($Param['LKD_ZUSATZ1']) AND $Param['LKD_ZUSATZ1']!='')
	{
	    $Bindevariablen['var_T_lkd_zusatz1']=$Form->Format('T',$Param['LKD_ZUSATZ1']);
	    $Bedingung .= ' AND EXISTS(SELECT * FROM LIEFERANTENKUNDENNUMMERN WHERE LKD_FIL_ID = ZUB_FIL_ID AND LKD_ZUSATZ1 = :var_T_lkd_zusatz1)';
	}
	

	$Param['WHERE']=$Bedingung;
	return $Bedingung;
}
?>