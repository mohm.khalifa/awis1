<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

class filialkompetenzen_funktionen {

    /**
     * @var awisFormular
     */
    public $Form;

    /**
     * @var awisDatenbank
     */
    public $DB;

    /**
     * @var awisBenutzer
     */
    public $AWISBenutzer;

    /**
     * Recht f�r die Maske
     *
     * @var int
     */
    public $Recht660;

    /**
     * AWISSprachKonserven
     *
     * @var array
     */
    public $AWISSprachKonserven;

    /**
     * CursorPosition der Seite
     *
     * @var String
     */
    public $AWISCursorPosition;

    /**
     * Datensatzkey
     *
     * @var string
     */
    public $AWIS_KEY1;

    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->Recht660 = $this->AWISBenutzer->HatDasRecht(660);

        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_FIK'));

        $TextKonserven = array();
        $TextKonserven[]=array('FIK','*');
        $TextKonserven[]=array('FIL','*');
        $TextKonserven[]=array('TITEL','tit_FILIALKOMPETENZEN');
        $TextKonserven[]=array('Wort','lbl_zurueck');
        $TextKonserven[]=array('Wort','lbl_speichern');
        $TextKonserven[]=array('Wort','lbl_hinzufuegen');
        $TextKonserven[]=array('Wort','lbl_loeschen');
        $TextKonserven[]=array('Wort','lbl_trefferliste');
        $TextKonserven[]=array('Wort','ttt_PositionHinzufuegen');
        $TextKonserven[]=array('Wort','ttt_PositionLoeschen');
        $TextKonserven[]=array('Wort','Achtung');
        $TextKonserven[]=array('Wort','lbl_suche');
        $TextKonserven[]=array('Wort','AuswahlSpeichern');
        $TextKonserven[]=array('Fehler','err_keineRechte');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven,$this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->Form->SetzeCursor($this->AWISCursorPosition);
        $this->AWISBenutzer->ParameterSchreiben('Formular_FIK', serialize($this->Param));
    }

    /**
     * Pr�ft die Rechte
     *
     * @param int $Bit
     */
    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht660 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'FIK'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function LadeFilialenHinzufuegen(){
        //Versteckt die Mehrfachselectfelder bei der Einzel-DS-Ansicht
        //bzw. zeigt sie an beim Dr�cken des Hinzuf�gen-Buttons
        $Script = "";
        $Script .= "<script>".PHP_EOL;
        $Script .= "$(document).ready(function(){".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= " $('#FIK_KOMPETENZFILIALEN_DIV').hide();".PHP_EOL;
        $Script .= " $('#FIK_KOMPETENZFILIALEN_ADD').click(function(){".PHP_EOL;
        $Script .= "  OeffneFiliale();".PHP_EOL;
        $Script .= " })".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= " function OeffneFiliale(){".PHP_EOL;
        $Script .= "  $('#FIK_KOMPETENZFILIALEN_DIV').show();".PHP_EOL;
        $Script .= " }".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "});".PHP_EOL;
        $Script .= "</script>";
        echo $Script;
    }
}