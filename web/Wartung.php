<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try 
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	$AWISBenutzer = awisBenutzer::Init();
	// --> Des muss ins AWIS-Formular als abfrage 
	//$AWISBenutzer->BenutzerOption(1);
	//
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZZZ','%');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$AWISSprachKonserven = awisFormular::LadeTexte($TextKonserven);
echo '<title>Wartung</title>';
?>
</head>
<body>
<?php
//include ("awisHeader.inc");	// Kopfzeile

try 
{
	$Form = new awisFormular();
    $Form->Formular_Start();
    
    $Form->ZeileStart();
    $WartungsBildHTML = '<img src="/bilder/Wartung.bmp" alt="wartung" height="520" width="1200">';
    $Form->SchreibeHTMLCode($WartungsBildHTML);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $SQL = 'SELECT XWM_WARTUNGSENDE FROM WEBMASKEN WHERE XWM_ID  = ' . $RegisterID ;
    $WebMaskenTbl = $DB->RecordSetOeffnen($SQL);
    
    If ($WebMaskenTbl->FeldInhalt('XWM_WARTUNGSENDE') != '')
    {
        $wartungstext = Str_Replace('#Uhrzeit#', $WebMaskenTbl->FeldInhalt('XWM_WARTUNGSENDE'), $AWISSprachKonserven['ZZZ']['ZZZ_WARTUNG_BESCHREIBUNG']);
    }
    else
    {
        $wartungstext = $AWISSprachKonserven['ZZZ']['ZZZ_WARTUNG_BESCHREIBUNG_ENDLOS'];
    }
    
    $Form->Erstelle_TextLabel($wartungstext, '1280', 'text-align:center');
    $Form->ZeileEnde();
    
    $Form->Formular_Ende();
	
}
catch (Exception $ex)
{
	echo  $ex->getMessage();
}
?>
</body>
</html>