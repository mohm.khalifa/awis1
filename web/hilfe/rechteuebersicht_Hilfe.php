<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>

<title></title>
</head>

<body>

<h2>AWIS - Rechte�bersicht</h2>

In dieser Maske kann gepr�ft werden, welche Rechte ein Anwender hat und 
welche Programmpunkte von welchem Mitarbeiter ausgef�hrt werden d�rfen.
Im Register <b>:Benutzerrechte:</b> kann ein Mitarbeiter ausgew�hlt werden. Sobald die
Auswahl best�tigt wurde, werden die Rechte angezeigt. Im Register <b>:Programmpunkte:</b>
werden analog die einzelnen Programmpunkte ausgew�hlt.

Zu den einzelnen Rechten werden die internen Rechte-IDs und die Rechtestufe angezeigt.
Je nach Rechtestufe werden mehr Rechte f�r den Programmpunkt hinterlegt. 

<br><br>Weitere Informationen
zu den Rechtestufen finden Sie in der Hilfe zu <a href=./rechte_Hilfe.php>Rechten</a> .

</body>

</html>
