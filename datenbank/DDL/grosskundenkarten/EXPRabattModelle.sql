/***************************************************************************
* Tabellendefinitionen : EXPRabattModelle
* Erstellt am : 14.12.2016 16:08:01
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE TABLE EXPRabattModelle(
ERM_KEY NUMBER(10,0) CONSTRAINT CNN_ERM_KEY NOT NULL,
ERM_NR NUMBER(10,0),
ERM_ERD_KEY VARCHAR2(50),
ERM_RABATTSATZ VARCHAR2(50),
CONSTRAINT PK_ERM_KEY PRIMARY KEY(ERM_KEY) USING Index TABLESPACE INDX) 
;


/***************************************************************************
* Kommentare : EXPRabattModelle
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

COMMENT ON COLUMN EXPRabattModelle.ERM_KEY IS 'KEY';
COMMENT ON COLUMN EXPRabattModelle.ERM_NR IS 'ModelNr';
COMMENT ON COLUMN EXPRabattModelle.ERM_ERD_KEY IS 'Rabattdetail-Key';
COMMENT ON COLUMN EXPRabattModelle.ERM_RABATTSATZ IS 'Rabattsatz';


/***************************************************************************
* Indexdefinitionen : EXPRabattModelle
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE INDEX IDX_ERM_ID ON EXPRabattModelle( ERM_NR) TABLESPACE INDX
;

/***************************************************************************
* Sequenz erstellen : EXPRabattModelle
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE SEQUENCE SEQ_ERM_KEY INCREMENT BY 1 START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trg_seq_ERM_KEY
     BEFORE INSERT ON EXPRabattModelle
     REFERENCING NEW AS NEW OLD AS OLD
     FOR EACH ROW
       BEGIN
         :NEW.ERM_KEY := seq_ERM_KEY.NEXTVAL ;
     END;
/ 


/***************************************************************************
* FOREIGN KEYS
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Tabelle dokumentieren
* Erstellt am : 14.12.2016 16:08:01
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DELETE FROM TabellenKuerzel WHERE XTN_KUERZEL='ERM';

INSERT INTO TabellenKuerzel(XTN_KUERZEL, XTN_TABELLENNAME, XTN_DATENQUELLE)
 VALUES('ERM', 'EXPRabattModelle', 'ORACLE')
;
DELETE FROM Textkonserven WHERE XTX_KENNUNG='ERM' AND XTX_BEREICH='Tabelle';

INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE)
 VALUES('ERM','Tabelle', 'EXPRabattModelle', 'EXPRabattModelle')
;


/***************************************************************************
* Ende des Skripts EXPRabattModelle
****************************************************************************
* Fertig am : 14.12.2016 16:08:01
***************************************************************************/

COMMIT;
