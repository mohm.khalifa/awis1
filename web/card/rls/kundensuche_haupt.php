<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
?>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
?>
	top.dialogArguments.parent.card_ladezeit.startzeit();	//Zeitmessung
<?php } ?>
</script>
<html>
<head>
<title>Kunden suchen</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript">
</script>


<?php 

//Rechnungen ermitteln

$strDb = "ora";

$strSql='select Kunden_Nr,Name,Strasse,PLZ,Ort from 
(select Kunden_Nr,Firma || \' \' || Abteilung as Name,Strasse,PLZ,Ort from ADRESSEN_SNAP where ';

if($intGrossklein)
{
	$strSql.='Firma like \'';
	if(!$intTextanfang)
		$strSql.='%';
	$strSql.=$strKundenname;
}
else
{
	$strSql.='upper(Firma) like \'';
	if(!$intTextanfang)
		$strSql.='%';
	$strSql.=strtoupper($strKundenname);
}

$strSql.='%\' and Adress_Art=\'01\' and Firma is not null
union
select Kunden_Nr,Name || \' \' || Vorname as Name,Strasse,PLZ,Ort from ADRESSEN_SNAP where ';

if($intGrossklein)
{
	$strSql.='Name like \'';
	if(!$intTextanfang)
		$strSql.='%';
	$strSql.=$strKundenname;
}
else
{
	$strSql.='upper(Name) like \'';
	if(!$intTextanfang)
		$strSql.='%';
	$strSql.=strtoupper($strKundenname);
}

$strSql.='%\' and Adress_Art=\'01\' and Firma is null)
order by Name';

$strQueryString = "strKundenname=".$strKundenname."&intGrossklein=".$intGrossklein."&intTextanfang=".$intTextanfang;

include("card/card_blaettern_haupt.inc.php");	//Bl�ttern 

?>
<script language="JavaScript">
function init()
{
	<?php
	if($intZeitmessung)
	{
	?>
		top.dialogArguments.parent.card_ladezeit.endezeit();	//Zeitmessung
	<?php } ?>
		
	document.ondblclick = zoom;		//Zoomfenster
}

//Wird bei Doppelklick auf das Feld mit der Rechnungsnr ge�ffnet, d.h. �bermittelt die Rechnungsnr nach card_haupt.php

function auswahlkdnr(strKunden_Nr)
{
	<?php
	if($intZeitmessung)
	{
	?>
		top.dialogArguments.parent.card_ladezeit.sendezeit("Auswahl Liste Kundensuche");	//Zeitmessung
	<?php } ?>
		
	parent.window.returnValue = strKunden_Nr;
	parent.self.close();
}
</script>

</head>
<body onload="init();" style="overflow:hidden;">
<table cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr><td align="center" valign="top">
<br>
Suche nach: <span class="titel"><?php echo $strKundenname; ?></span>&nbsp;
<?php
	if($intGrossklein||$intTextanfang)
	{
		echo "(";
		if($intGrossklein&&$intTextanfang)
			echo "G/K+TA";
			elseif($intGrossklein)
				echo "G/K";
				else
					echo "TA";
		echo ")";
	}
?>
<br>
Datens&auml;tze: <span class="titel"><?php echo $strPosition; ?></span><br><br>
<?php 
$arrKunden = abfrage("ora",$strSqlScroll); 

if($arrKunden["trefferarray"])
{
?>
	<table cellspacing="0" cellpadding="0">
	<tr>
	<td>&nbsp;</td><td class="titel">Name:</td><td class="titel">Strasse:</td><td class="titel">PLZ:</td><td class="titel">Ort:</td>
	</tr>
	<?php
		$arrHilf = $arrKunden["trefferarray"];
		
		for($i=0;$i<$arrKunden["zeilen"];$i++)
		{
			echo "<tr>";
			echo "<td><img src=\"/bilder/eingabe_ok.png\" class=\"quadrat\" alt=\"Datensatz ausw&auml;hlen\" onclick=\"auswahlkdnr('".$arrHilf["KUNDEN_NR"][$i]."');\"></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"name\" value=\"".$arrHilf["NAME"][$i]."\" maxlength=\"50\" readonly></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"strasse\" value=\"".$arrHilf["STRASSE"][$i]."\" readonly></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"plz\" value=\"".$arrHilf["PLZ"][$i]."\" readonly></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"ort\" value=\"".$arrHilf["ORT"][$i]."\" readonly></td>";
			echo "</tr>";
		}
	?>
	</table>
<?php
}
else
{
	echo "<span class=\"titel\">keine Datens&auml;tze gefunden!</span>";
}
?>
</td></tr></table>
</body>
</html>
<?php 
$intDialog = 1;
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>