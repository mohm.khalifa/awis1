/***************************************************************************
* Tabellendefinitionen : EXPEANSwitch
* Erstellt am : 14.12.2016 16:07:55
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE TABLE EXPEANSwitch(
EES_KEY NUMBER(10,0) CONSTRAINT CNN_EES_KEY NOT NULL,
EES_EANID NUMBER(10,0) DEFAULT '0',
EES_EANCODE VARCHAR2(50),
EES_BILDPFAD VARCHAR2(50),
CONSTRAINT PK_EES_KEY PRIMARY KEY(EES_KEY) USING Index TABLESPACE INDX) 
;


/***************************************************************************
* Kommentare : EXPEANSwitch
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

COMMENT ON COLUMN EXPEANSwitch.EES_KEY IS 'KEY';


/***************************************************************************
* Indexdefinitionen : EXPEANSwitch
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE INDEX IDX_EES_EANCODE ON EXPEANSwitch( EES_EANCODE) TABLESPACE INDX
;

/***************************************************************************
* Sequenz erstellen : EXPEANSwitch
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE SEQUENCE SEQ_EES_KEY INCREMENT BY 1 START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trg_seq_EES_KEY
     BEFORE INSERT ON EXPEANSwitch
     REFERENCING NEW AS NEW OLD AS OLD
     FOR EACH ROW
       BEGIN
         :NEW.EES_KEY := seq_EES_KEY.NEXTVAL ;
     END;
/ 


/***************************************************************************
* FOREIGN KEYS
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Tabelle dokumentieren
* Erstellt am : 14.12.2016 16:07:55
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DELETE FROM TabellenKuerzel WHERE XTN_KUERZEL='EES';

INSERT INTO TabellenKuerzel(XTN_KUERZEL, XTN_TABELLENNAME, XTN_DATENQUELLE)
 VALUES('EES', 'EXPEANSwitch', 'ORACLE')
;
DELETE FROM Textkonserven WHERE XTX_KENNUNG='EES' AND XTX_BEREICH='Tabelle';

INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE)
 VALUES('EES','Tabelle', 'EXPEANSwitch', 'EXPEANSwitch')
;


/***************************************************************************
* Ende des Skripts EXPEANSwitch
****************************************************************************
* Fertig am : 14.12.2016 16:07:55
***************************************************************************/

COMMIT;
