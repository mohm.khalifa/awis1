<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Preislistenpr�fungen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body bgcolor="#EEEEEF">
<?php
global $awisRSZeilen;
global $awisDBFehler;

$con = awislogon();

$RechteStufe = awisBenutzerRecht($con,2000);
if(($RechteStufe&64)!=64)
{
	awisEreignis(3,1000,'Preislistenpr�fungen',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

$comcon = awisLogonComCluster('DE',false);
awisExecute($comcon, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'");

echo '<h2>Daten �bertragen</h2>';


// �bersicht zeigen und Best�tigen lassen
if(!isset($_POST['cmdStart_x']))
{
	$SQL = 'SELECT BEZEICHNUNG, PST_ID, PST_STATUS, PTS_USERDAT,';
	$SQL .= ' NVL(PTS_TRANSAKTIONSID,0) AS PTS_TRANSAKTIONSID, NVL(PTS_ID_FPLB,0) AS PTS_ID_FPLB, HERSTELLERPREISLISTE,';
	$SQL .= ' NVL(PTS_ZUSATZ_TYP,0) AS PTS_ZUSATZ_TYP, NVL(PTS_ZUSATZ_TID,0) AS PTS_ZUSATZ_TID';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS ';
	$SQL .= ' LEFT OUTER JOIN EXPGW.EXPGW_EXPORT ON PTS_TRANSAKTIONSID=TRANSAKTIONSID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTS_PST_ID = PST_ID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB ON ID_FPLB = PTS_ID_FPLB';
	$SQL .= ' WHERE PTS_ID_FPLB='.$_GET['FPLB'].'';
	$SQL .= ' ORDER BY  PTS_TRANSAKTIONSID DESC';

	$rsPLI = awisOpenRecordset($comcon,$SQL);
	//awis_Debug(1,$rsPLI);

	echo '<form name=frmBestaetigung action=./preislisten_DatenUebertragen.php method=POST>';

	echo '<input type=hidden name=txtTID value=' . $rsPLI['PTS_TRANSAKTIONSID'][0] . '>';
	echo '<input type=hidden name=txtPREISLISTE value=' . $rsPLI['PTS_ID_FPLB'][0] . '>';
	echo '<input type=hidden name=txtHERSTELLER value=' . $rsPLI['HERSTELLERPREISLISTE'][0] . '>';
	echo '<input type=hidden name=txtPTS_ZUSATZ_TYP value=' . $rsPLI['PTS_ZUSATZ_TYP'][0] . '>';
	echo '<input type=hidden name=txtPTS_ZUSATZ_TID value=' . $rsPLI['PTS_ZUSATZ_TID'][0] . '>';

	echo '<table border=1 width=100% ID=DatenTabelle>';
	echo '<tr>';
	echo '<td class=FeldBez width=200>Preisliste:</td><td>' . $rsPLI['BEZEICHNUNG'][0] . '(' . $rsPLI['PTS_ID_FPLB'][0] . ')</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td class=FeldBez>Transaktion:</td><td>' . $rsPLI['PTS_TRANSAKTIONSID'][0] . '</td>';
	echo '</tr>';

	if($rsPLI['PTS_ZUSATZ_TID'][0]!='0')
	{
		echo '<tr>';
		echo '<td class=FeldBez>Transaktion Zusatzpreisliste:</td><td>' . $rsPLI['PTS_ZUSATZ_TID'][0] . '</td>';
		echo '</tr>';
	}
	echo '<tr>';
	echo '<td class=FeldBez>Status:</td><td>' . $rsPLI['PST_STATUS'][0] . '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td class=FeldBez>Letzte Status�nderung:</td><td>' . $rsPLI['PTS_USERDAT'][0] . '</td>';
	echo '</tr>';

	echo '</table>';

	echo "<br><hr><input type=image title=Zur�ck alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=window.close();>";
	echo "&nbsp;<input type=image title='Daten �bertragen' src=/bilder/sc_rakete.png name=cmdStart accesskey=p >";

	echo '</form>';
}
else
{
	echo '<br>Alte Daten werden gel�scht...';
	ob_flush();
	flush();

	/*
	$SQL = 'DELETE FROM EXPERIAN_ATU.EXP_FESTPREISLISTEN_TAB';
	$SQL .= ' WHERE TRANSAKTIONSID=' . $_POST['txtTID'];
	$SQL .= ' AND ID_FPLB=' . $_POST['txtPREISLISTE'];
	*/
	
	$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_DELETE_PREISLISTE('.$_POST['txtTID'].', '.$_POST['txtPREISLISTE'].'); END;';

	if(awisExecute($comcon, $SQL)===false)
	{
		awisErrorMailLink('08091334-preislisten_DatenUebertragen.php',3,$awisDBFehler['message']);
		die();
	}

	echo '<br>Neue Preisliste wird �bertragen...';
	ob_flush();
	flush();

	$SQL = ' SELECT count(*) AS ANZ';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN ';
	$SQL .= ' WHERE PLT_TRANSAKTIONSID=' . $_POST['txtTID'];
	$SQL .= ' AND PLT_ID_FPLB=' . $_POST['txtPREISLISTE'];
	$SQL .= ' AND PLT_Version=0';			// Nur um WIRKLICH sicher zu sein!

	$rsDaten = awisOpenRecordset($comcon, $SQL);
	$Anzahl_Soll=$rsDaten['ANZ'][0];
	echo '<br>&nbsp;&nbsp;&nbsp;Es werden ' . $rsDaten['ANZ'][0] . ' Datens�tze �bertragen...';
	$DatenAnz = $rsDaten['ANZ'][0];
	ob_flush();
	flush();

	/*
	$SQL = ' INSERT INTO EXPERIAN_ATU.EXP_FESTPREISLISTEN_TAB';
	$SQL .= ' SELECT PLT_ID_FPLB, PLT_HERSTELLERPREISLISTE, PLT_ARTNR, PLT_PREIS, PLT_GUELTIG_VON, PLT_GUELTIG_BIS, PLT_IMPDAT, PLT_TRANSAKTIONSID, PLT_IMD_ID';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN ';
	$SQL .= ' WHERE PLT_TRANSAKTIONSID=' . $_POST['txtTID'];
	$SQL .= ' AND PLT_ID_FPLB=' . $_POST['txtPREISLISTE'];
	$SQL .= ' AND PLT_Version=0';			// Nur um WIRKLICH sicher zu sein!
	*/
	
	$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_INSERT_PREISLISTE('.$_POST['txtTID'].', '.$_POST['txtPREISLISTE'].'); END;';
	
	if(awisExecute($comcon, $SQL)===false)
	{
		awisErrorMailLink('08091335-preislisten_DatenUebertragen.php',3,$awisDBFehler['message']);
		die();
	}

	$SQL = ' SELECT count(*) AS ANZ';
	$SQL .= ' FROM EXPERIAN_ATU.EXP_FESTPREISLISTEN_TAB ';
	$SQL .= ' WHERE TRANSAKTIONSID=' . $_POST['txtTID'];
	$SQL .= ' AND ID_FPLB=' . $_POST['txtPREISLISTE'];

	$rsDaten = awisOpenRecordset($comcon, $SQL);
	$Anzahl_Ist=$rsDaten['ANZ'][0];
	echo '<br>&nbsp;&nbsp;&nbsp;Es wurden ' . $rsDaten['ANZ'][0] . ' Datens�tze erstellt.';
	$DatenAnz = $rsDaten['ANZ'][0];
	ob_flush();
	flush();
	
		// Zusatzpreisliste vorhanden?
	if($_POST['txtPTS_ZUSATZ_TYP']=='1')
	{
		echo '<br>Neue Artikelzusatzdaten (Typ 1) werden �bertragen...';
		ob_flush();
		flush();

		echo '<br>&nbsp;&nbsp;&nbsp;Alte Daten werden gel�scht...';
		ob_flush();
		flush();

		$SQL = 'DELETE FROM EXPERIAN_ATU.EXP_ARTIKEL_ZUSATZ_TAB';
		//$SQL .= ' WHERE TRANSAKTIONSID=' . $_POST['txtTID'];
		$SQL .= ' WHERE TRANSAKTIONSID=' . $_POST['txtPTS_ZUSATZ_TID'];

		if(awisExecute($comcon, $SQL)===false)
		{
			awisErrorMailLink('08091434-preislisten_DatenUebertragen.php',3,$awisDBFehler['message']);
			die();
		}

		echo '<br>&nbsp;&nbsp;&nbsp;Neue Daten �bertragen...';
		ob_flush();
		flush();

		$SQL = ' SELECT count(*) AS ANZ';
		$SQL .= ' FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ ';
		$SQL .= ' WHERE PAZ_TRANSAKTIONSID=' . $_POST['txtTID'];

		$rsDaten = awisOpenRecordset($comcon, $SQL);
		echo '<br>&nbsp;&nbsp;&nbsp;Es werden ' . $rsDaten['ANZ'][0] . ' Datens�tze �bertragen...';
		$DatenAnz .= ' (' . $rsDaten['ANZ'][0] . ')';
		ob_flush();
		flush();


		$SQL = ' INSERT INTO EXPERIAN_ATU.EXP_ARTIKEL_ZUSATZ_TAB';
		$SQL .= ' SELECT PAZ_ARTNR,PAZ_HERSTELLER,PAZ_HERSTELLER_LANG,PAZ_RG,PAZ_ZUSINF_RG';
		$SQL .= ' ,PAZ_TYP1,PAZ_TYP2, PAZ_TYP3,PAZ_WINSOM,PAZ_BREITE,PAZ_QUERSCHNITT,PAZ_BAUART,PAZ_INNENDM';
		$SQL .= ' ,PAZ_LLKW,PAZ_LOAD1,PAZ_LOAD2,PAZ_SP,PAZ_RF,PAZ_ZUSBEM,PAZ_IMPDAT,PAZ_USERDAT';
		//$SQL .= 'PAZ_TRANSAKTIONSID';
			// TID der Zusatzliste verwenden f�r die �bertragung (SK, 28.3.07)
		$SQL .= ',' . $_POST['txtPTS_ZUSATZ_TID'];
		$SQL .= ', PAZ_IMD_ID,PAZ_BEZEICHNUNG,PAZ_GWS,PAZ_SEAL,PAZ_ROF,PAZ_RX,PAZ_ES,PAZ_ROLLRES, PAZ_WETGRIP, PAZ_NOISEPE, PAZ_NOISECL';
		$SQL .= ' FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ';
		$SQL .= ' WHERE PAZ_TRANSAKTIONSID=' . $_POST['txtTID'];
		$SQL .= ' AND PAZ_Version=0';			// Nur um WIRKLICH sicher zu sein!

		if(awisExecute($comcon, $SQL)===false)
		{
			awisErrorMailLink('08091335-preislisten_DatenUebertragen.php',3,$awisDBFehler['message']);
			die();
		}
		
			// Status der Zusatzpreisliste �ndern (SK, 28.3.07)
		echo '<br>DF�-Status Zusatzpreisliste �ndern...';
		ob_flush();
		flush();
		$SQL = 'UPDATE EXPGW.EXPGW_EXPORT';
		$SQL .=  ' SET ARBEITSSTATUS=\'L\'';
		$SQL .= ' WHERE TRANSAKTIONSID=' . $_POST['txtPTS_ZUSATZ_TID'];
	
		if(awisExecute($comcon, $SQL)===false)
		{
			awisErrorMailLink('28031139-preislisten_DatenUebertragen.php',3,$awisDBFehler['message']);
			die();
		}
	
		
		
	}

	if ($Anzahl_Soll==$Anzahl_Ist)
	{
		echo '<br>Transationsstatus �ndern...';
		ob_flush();
		flush();
		/*$SQL = 'UPDATE EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS';
		$SQL .=  ' SET PTS_PST_ID = 42, PTS_BEMERKUNG=\'Es wurden ' . $DatenAnz . ' Daten �bertragen.\'';
		$SQL .= ' WHERE PTS_TRANSAKTIONSID=' . $_POST['txtTID'];
	
		if(awisExecute($comcon, $SQL)===false)
		{
			awisErrorMailLink('08091456-preislisten_DatenUebertragen.php',3,$awisDBFehler['message']);
			die();
		}
	
	
		echo '<br>DF�-Status �ndern...';
		ob_flush();
		flush();
		$SQL = 'UPDATE EXPGW.EXPGW_EXPORT';
		$SQL .=  ' SET ARBEITSSTATUS=\'L\'';
		$SQL .= ' WHERE TRANSAKTIONSID=' . $_POST['txtTID'];
		*/
		$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_FREIGABE_PREISLISTE('.$_POST['txtTID'].', \''.$DatenAnz.'\'); END;';	
		
		if(awisExecute($comcon, $SQL)===false)
		{
			awisErrorMailLink('08091458-preislisten_DatenUebertragen.php',3,$awisDBFehler['message']);
			die();
		}
		echo '<br>DF�-Status �ndern...';
		ob_flush();
		flush();
	
		echo '<br><br><br>�bertragung abgeschlossen.<br>';
		echo "<br><hr><input type=image title='Zur�ck und aktualisieren' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=opener.location.reload();window.close();>";
	}else{
			echo '<br><br><br>Die Daten wurden nicht vollst&auml;ndig &uuml;bertragen. Starten sie die &Uuml;bertragung erneut.<br>';
			echo "<br><hr><input type=image title='Zur�ck und aktualisieren' alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=opener.location.reload();window.close();>";
	}
}



awisLogoff($comcon);
awisLogoff($con);
?>