<?php
	global $HTTP_POST_VARS;
	global $HTTP_GET_VARS;

	require_once("db.inc.php");		// DB-Befehle

	$con = awislogon();

	$rs = awisOpenRecordset($con, $HTTP_GET_VARS["SQL"]);
	$rsZeilen = $awisRSZeilen;

	print "<h3>SQL Anweisung</h3><hr>";
	print "" . $HTTP_GET_VARS["SQL"];

	print "<hr><br>$rsZeilen Zeilen gefunden.<br>";
	print "<br><b>Felder</b><br>";
	$rs1 = array_keys($rs);
	for($i=0;$i<sizeof($rs1);$i++)
	{
		print "<br>" . $rs1[$i];
	}


	/*********************************************************
	* 
	* XML Datei schreiben
	* 
	*********************************************************/
	
	$fd = fopen("./Daten.xml","w");

	fwrite($fd,"<?xml version=\"1.0\" encoding=\"Windows-1252\" ?>");
	fwrite($fd,"\n <Datensaetze>");
	for($j=0;$j<$rsZeilen;$j++)
	{
		fwrite($fd,"\n  <Datensatz>");
		for($i=0;$i<sizeof($rs1);$i++)
		{
			fwrite($fd,"\n    <" . $rs1[$i] . ">" . $rs[$rs1[$i]][$j]);
			fwrite($fd,"</" . $rs1[$i] . ">");
		}
		fwrite($fd,"\n</Datensatz>");
	}	
	fwrite($fd,"\n</Datensaetze>");	
	fclose($fd)	;

	print "<hr><br><a href=./Daten.xml>XML-Datei</a>";
	
	awisLogoff($con)	;
?>