<?php

//Blanko -> Formular VisitCheck
require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('CBT','%');
$TextKonserven[]=array('CV','%');

$AWISBenutzer = awisBenutzer::Init();

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Ausdruck = new awisAusdruck('P','A4','');
$Ausdruck->NeueSeite(0,1);

if ($AWISBenutzer->BenutzerSprache()=='CZ')
{
	$Ausdruck->_pdf->ZeichenKodierung('CP1250');
	$Schriftart = 'arialcz';
}
else 
{
	$Schriftart = 'arial';
}

$Ausdruck->_pdf->AddFont($Schriftart,'','');

function Auswertung_Felder($Ausdruck,$ID,$x1,$y1,$x1_2,$y1_2,$FeldBreite = 5)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	global $Schriftart;
	
	$Schritt = 3;
	
	$SQL =  'Select * from Personaleinsberichtszuord inner join PERSONALEINSBERICHTSFELDER ON PBF_ID = PEZ_PBF_ID '; 
	$SQL .= 'where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;
	
	$rsAuswertung = $DB->RecordSetOeffnen($SQL);
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1,$y1,$x2 = ($x1  + $Schritt),$y2 = ($y1 + $Schritt));
      $Ausdruck->_pdf->Line($x2, $y1, $x1, $y2);
      $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	}
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1_2,$y1_2,$x2 = ($x1_2  + $Schritt),$y2 = ($y1_2 + $Schritt));
      $Ausdruck->_pdf->Line($x2, $y1_2, $x1_2, $y2);
      $Ausdruck->_pdf->SetLineWidth(0); 	
	}
	else
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	}
	if($rsAuswertung->FeldInhalt('PBF_BEMERKUNG') == '')
	{
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6);
	  $Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,35)."\n".substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),35,35);
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$Text,'LTRB',0,'L',0);
	}
	else 
	{
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6); 	
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$rsAuswertung->FeldInhalt('PBF_BEMERKUNG').':'.$rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),'LTRB','L',1);  	
	}
}

$Spalte = 15;
$Zeile = 5;



if($_GET['PEI_KEY'] == 0)
{
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch TKDL: Typ B','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 07.02.2011','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['CBT']['CBT_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['CBT']['CBT_DATUM'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,'','B',0,'C',0);
$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,'A. '.$AWISSprachKonserven['CBT']['CBT_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_A1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 62, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 62, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_A2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 67, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 67, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_A3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 72, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 72, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBT']['CBT_A4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 77, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 77, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'B. '.$AWISSprachKonserven['CBT']['CBT_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_B1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 87, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 87, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_B2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 92, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 92, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'C. '.$AWISSprachKonserven['CBT']['CBT_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_C1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 102, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 102, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_C2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 107, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 107, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_C3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 112, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 112, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'D. '.$AWISSprachKonserven['CBT']['CBT_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_D1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 122, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 122, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_D2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 127, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 127, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_D3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 132, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 132, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBT']['CBT_D4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 137, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 137, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBT']['CBT_D5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 142, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 142, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'E. '.$AWISSprachKonserven['CBT']['CBT_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_E1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 152, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 152, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_E2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 157, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 157, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['CBT']['CBT_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_F1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 167, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 167, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_F2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 172, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 172, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_F3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 177, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 177, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'G. '.$AWISSprachKonserven['CBT']['CBT_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_H1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 187, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 187, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_H2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 192, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 192, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);

//$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_A1'],'TRB','L',0);
//$Ausdruck->_pdf->Multicell(5,8,'2.','TLB','C',0);
$Ausdruck->_pdf->Multicell(110,4,'3. '.$AWISSprachKonserven['CBT']['CBT_H3'],'TLB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 198.5, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 198.5, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',1);

$Spalte-=110;
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBT']['CBT_H4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 205, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 205, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'Quote:','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBT']['CBT_H5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 210, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 210, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['CBT']['CBT_H6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 215, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 215, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'H. '.$AWISSprachKonserven['CBT']['CBT_I'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_I1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 225, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 225, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_I2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 230, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 230, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_I3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 235, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 235, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'I. '.$AWISSprachKonserven['CBT']['CBT_J'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_J1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 245, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 245, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'J. '.$AWISSprachKonserven['CBT']['CBT_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_G1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 255, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 255, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=30;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Datum:','T',0,'C',0);

$Spalte+=40;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift WL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift TKDL:','T',0,'C',0);



}
elseif($_GET['PEI_KEY'] != 0)
{

$SQL = 	'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS '; 
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= 'WHERE PEZ_PEI_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEI_KEY']);
	
$rsBericht = $DB->RecordSetOeffnen($SQL);	

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Coaching - Besuch TKDL: Typ B','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 1','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 07.02.2011','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['CBT']['CBT_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,$rsBericht->FeldInhalt('PEI_FIL_ID'),'B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITVON'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['CBT']['CBT_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITBIS'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['CBT']['CBT_DATUM'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,substr($rsBericht->FeldInhalt('PEI_DATUM'),0,10),'B',0,'C',0);
$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,'A. '.$AWISSprachKonserven['CBT']['CBT_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_A1'],'LTRB',0,'L',0);


Auswertung_Felder($Ausdruck,101, 128.5, 62,143, 62);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_A2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,102, 128.5, 67,143, 67);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_A3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,103, 128.5, 72,143, 72);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBT']['CBT_A4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,104, 128.5, 77,143, 77);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'B. '.$AWISSprachKonserven['CBT']['CBT_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_B1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,106, 128.5, 87,143, 87);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_B2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,107, 128.5, 92,143, 92);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'C. '.$AWISSprachKonserven['CBT']['CBT_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_C1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,109, 128.5, 102,143, 102);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_C2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,110, 128.5, 107,143, 107);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_C3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,111, 128.5, 112,143, 112);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'D. '.$AWISSprachKonserven['CBT']['CBT_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_D1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,113, 128.5, 122,143, 122);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_D2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,115, 128.5, 127,143, 127);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_D3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,116, 128.5, 132,143, 132);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBT']['CBT_D4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,117, 128.5, 137,143, 137);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBT']['CBT_D5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,118, 128.5, 142,143, 142);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'E. '.$AWISSprachKonserven['CBT']['CBT_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_E1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,120, 128.5, 152,143, 152);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_E2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,121, 128.5, 157,143, 157);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['CBT']['CBT_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_F1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,123, 128.5, 167,143, 167);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_F2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,124, 128.5, 172,143, 172);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_F3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,125, 128.5, 177,143, 177);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'G. '.$AWISSprachKonserven['CBT']['CBT_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_H1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,127, 128.5, 187,143, 187);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_H2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,128, 128.5, 192,143, 192);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);

//$Ausdruck->_pdf->Multicell(70,4,$AWISSprachKonserven['CV']['CV_A1'],'TRB','L',0);
//$Ausdruck->_pdf->Multicell(5,8,'2.','TLB','C',0);
$Ausdruck->_pdf->Multicell(110,4,'3. '.$AWISSprachKonserven['CBT']['CBT_H3'],'TLB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,129, 128.5, 198.5,143, 198.5,8);

$Spalte-=110;
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['CBT']['CBT_H4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,130, 128.5, 205,143, 205);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['CBT']['CBT_H5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,131, 128.5, 210,143, 210);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['CBT']['CBT_H6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,132, 128.5, 215,143, 215);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'H. '.$AWISSprachKonserven['CBT']['CBT_I'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_I1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,134, 128.5, 225,143, 225);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['CBT']['CBT_I2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,135, 128.5, 230,143, 230);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['CBT']['CBT_I3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,136, 128.5, 235,143, 235);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'I. '.$AWISSprachKonserven['CBT']['CBT_J'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_J1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,138, 128.5, 245,143, 245);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'J. '.$AWISSprachKonserven['CBT']['CBT_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['CBT']['CBT_G1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,140, 128.5, 255,143, 255);

$Zeile+=15;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Datum:','T',0,'C',0);

$Spalte+=40;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift WL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift TKDL:','T',0,'C',0);

$Zeile+=5;
$Spalte-=140;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";

$rsGesamt = $DB->RecordSetOeffnen($SQL);

$Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');

echo "Gesamtanzahl:".$Gesamtanzahl;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= "AND PEZ_WERT = '0'";

$GesamtFalsch = $DB->RecordSetOeffnen($SQL);

$Falsch = $GesamtFalsch->FeldInhalt('ANZ');

echo "Falsch".$Falsch;

$Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) *100); 

if($Prozentsatz >= 90 && $Prozentsatz <= 100)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GR�N)','',0,'R',0);
	
}
elseif($Prozentsatz >= 80 && $Prozentsatz <= 90)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GELB)','',0,'R',0);
	
}
elseif($Prozentsatz < 80)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	//$Ausdruck->_pdf->cell2(180,4,'Hallo','LTRB',0,'L',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=13;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(180,5,'(ROT)','',0,'C',0);
	//$Ausdruck->_pdf->Image($file, $x, $y,$w,$h);
}
$Zeile+=4;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(180,5,'Erf�llungsgrad: min. 75 % der Felder, Erf�llungsrating: 100% - 90% = Gr�n, 89% - 80% = Gelb, < 80% = Rot   ','',0,'C',0);
	


}


$Ausdruck->Anzeigen();


?>