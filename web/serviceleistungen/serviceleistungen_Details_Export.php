<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Jahr');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19003 = $AWISBenutzer->HatDasRecht(19003);
	if($Recht19003==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	$Form->Formular_Start();


    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Jahr'].':', 250);
    for($Jahr=2011;$Jahr<=date('Y');$Jahr++)
    {
    	//$Daten[] = array($Jahr, $Jahr); // Hmm ?
    	$Daten[$Jahr] = array($Jahr);
    }
	$Form->Erstelle_SelectFeld('jahr', date('Y'), '100:90', true, null, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
    $AWISCursorPosition='txtjahr';
    $Form->ZeileEnde();

    $Form->Formular_Ende();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (Exception $ex)
{

}
?>