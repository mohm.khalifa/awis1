<?php

/**
 * Klasse fuer Informationen rund um AWIS
 * 
 * @author Sacha Kerres
 * @version 20070802
 * @copyright ATU Auto Teile Unger
 *
 */
class awisINFO
{
	/**
	 * Pfad zu der Konfigurationsdatei
	 *
	 */
	CONST AWIS_INI_PFAD = '/daten/conf/';

	/**
	 * Objekt initialisieren
	 * 
	 * Es wird gepr�ft, ob das Konfigurationsverzeichnis existiert.
	 *
	 */
	public function __construct()
	{
		if(!is_dir(self::AWIS_INI_PFAD))
		{
			throw new Exception('Verzeichnis mit AWIS Konfiguration ist nicht vorhanden',1);
		}
		
		
		$LogFacility = self::LeseAWISParameter('awis.conf','SYSLOG_FACILITY');
		if(version_compare(PHP_VERSION,'5.4','<')){
            define_syslog_variables();
        }
		openlog("AWIS", LOG_CONS | LOG_PID | LOG_ODELAY, constant($LogFacility));
	}

	/**
	 * Objekt aufraeumen
	 *
	 */
	public function __destruct()
	{
		closelog();
	}
	/**
	 * Einen Parameter aus dem AWIS System lesen
	 *
	 * @param string $INIQuelle (Dateiname oder 'DB')
	 * @param string $Parameter
	 * @return string
	 * @version 20070802
	 * @author Sacha Kerres
	 */
	public function LeseAWISParameter($INIQuelle, $Parameter)
	{
		$Erg = '';

				// Pruefen, ob es eine Datei mit dem Namen gibt
		if(is_file(self::AWIS_INI_PFAD . $INIQuelle))
		{
			$Zeilen = file(self::AWIS_INI_PFAD . $INIQuelle);
			foreach($Zeilen AS $Zeile)				// Alle Zeilen einlesen
			{
				$Zeile = trim($Zeile);
				if(substr($Zeile,0,1)!=='#')	// Kommentarzeile?
				{
					$Inhalt = explode('=',$Zeile);
					if(sizeof($Inhalt)==2)			// Zwei Eintraege  PARAM=WERT
					{
						if(strcasecmp(trim($Inhalt[0]),$Parameter)==0)
						{
							$Erg = trim($Inhalt[1]);// Wert zurueckliefern
						}
					}
				}
			}
		}
		
		return $Erg;
	}
	
	/**
	 * E-Mail senden
	 *
	 * @param array $TO
	 * @param string $Betreff
	 * @param string $Text
	 * @param int $Level (1=niedrig, 2=normal, 3=wichtig)
	 * @return boolean
	 * @version 200708071927
	 * @author Sacha Kerres
	 */
	public function EMail($TO,$Betreff,$Text,$Level=2)
	{
		require_once 'awisDatenbank.inc';

		$Werkzeuge = new awisWerkzeuge();

        if(!is_array($TO))
        {
            $TO = array($TO);
        }
		return $Werkzeuge->EMail($TO,$Betreff,$Text,$Level,'','awis@de.atu.eu');

	}
	
	
	/**
	 * Eintrag in Syslog schreiben
	 *
	 * @param int $Prioritaet
	 * @param string $Meldung
	 * @author Sacha Kerres
	 * @version 200710090920
	 * 
	 */
	public function SyslogEintrag($Prioritaet, $Meldung)
	{
		syslog($Prioritaet, $Meldung);
	}
}




?>
