<?php
/**
 * Pr�fmodul: 	XXX
 * Zweck:		Dieses Modul pr�ft XXX
 *
 * Parameter:
 *
 *
 */

global $con;					// Verbindung zum AWIS Server
global $concom;					// Verbindung zum COM Server
global $PreisListe;				// Preislisten-ID
global $TransaktionsID;			// Transaktions-ID
global $rsPTA;					// Testarten
global $PTAZeile;				// Aktuelle Zeile in den Testarten-RS
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

//*****************************************************************************
// Parameter lesen
//*****************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY';
$SQL .= ' WHERE PLP_ID_FPLB=' . $PreisListe;
$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

$Params=array();

$rsPAR = awisOpenRecordset($comcon,$SQL);
$rsPARZeilen = $awisRSZeilen;
if($rsPARZeilen>0)
{
	for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
	{
		$Params[$rsPAR['PTP_PARAMETER'][$PARZeile]]=($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]);
	}
}

//*****************************
// Historie schreiben
//*****************************
$SQL = ' INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' (PPH_PLP_KEY,PPH_TRANSAKTIONSID,PPH_BEGINN,PPH_USER,PPH_STATUS)';
$SQL .= ' VALUES(' . $rsPTA['PLP_KEY'][$PTAZeile] . '';
$SQL .= ', ' . $TransaktionsID ;
$SQL .= ', SYSDATE';
$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
$SQL .= ', 0';
$SQL .= ')';

if(!awisExecute($comcon,$SQL))
{
	echo '<span class=HinweisTex>Fehler: '. $SQL . '</span>';
}
$rsPPH = awisOpenRecordset($comcon,'SELECT MAX(PPH_Key) AS KEY FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $TransaktionsID . ' AND PPH_User=\'' . $AWISBenutzer->BenutzerName() . '\'');
$PPHKey = $rsPPH['KEY'][0];
unset($rsPPH);

$TestErgebnis = 1;	// Erfolg als Standard
$AnzAuff=0;			// Anzahl der Auff�lligkeiten

//###############################################################################
//###############################################################################
//########################   B E G I N N   T E S T S ############################
//###############################################################################
//###############################################################################


//*****************************************************************************
// TEST 1
//*****************************************************************************


$SQL = 'SELECT ';
$SQL .= ' FROM ';
$SQL .= ' WHERE';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>: XXX</b><br>';
for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['XXX'][$TSTZeile];

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'XXX: ' . $rsTST['XXX'][$TSTZeile] . '\'' ;
	$SQL .= ', \'1;2;3;4\'';
	$SQL .= ', 1)';
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	//TODO: Testergebnis
	$TestErgebnis = '??';
}

flush();
unset($rsTST);








if($TestErgebnis==1)
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: Keine Fehler gefunden. Bewertung: <font color=green>I.O.</font></div>';
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $AnzAuff . ' Fehler gefunden. Bewertung: <font color=red>Fehler.</font></div>';
}


//###############################################################################
//###############################################################################
//##########################   E N D E   T E S T S ##############################
//###############################################################################
//###############################################################################


//*********************************************
// Historie aktualisieren
//*********************************************
$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' SET PPH_ENDE = SYSDATE, PPH_STATUS = '.$TestErgebnis;
$SQL .= ' WHERE PPH_Key = ' . $PPHKey;
if(!awisExecute($comcon,$SQL))
{
	awisErrorMailLink('PreislistePr�fung',1,$awisDBFehler);
}


unset($rsTST);

echo '<hr>';

?>