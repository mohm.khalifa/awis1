<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PRA','%');
	$TextKonserven[]=array('PAB','%');
	$TextKonserven[]=array('PRI','%');
	$TextKonserven[]=array('FIB','FIB_BESTAND');
	$TextKonserven[]=array('FIL','FIL_ID');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','lbl_drucken');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht6000 = $AWISBenutzer->HatDasRecht(6000);
	if($Recht6000==0)
	{
	    awisEreignis(3,1000,'PRA',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$DetailAnsicht=false;
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./problemartikel_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./problemartikel_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}



	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
	$FIL_ID = (isset($_POST['txtFIL_ID'])?$_POST['txtFIL_ID']:'');
	if($FIL_ID=='')
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ProblemArtikel_1'));
		$FIL_ID=$Param['FIL_ID'];
	}
	else
	{
		$Param['FIL_ID']=$FIL_ID;
		$AWISBenutzer->ParameterSchreiben('Formular_ProblemArtikel_1',serialize($Param));
	}

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmProblemartikel action=./problemartikel_Main.php?cmdAktion=ReifenOhneS'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST>');

	$Form->Formular_Start();

	$EditModus = ($Recht6000&6);

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',150);
	if(count($FilZugriffListe)>1)
	{
		$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
		$SQL .= ' FROM Filialen ';
		$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
		$SQL .= ' ORDER BY FIL_BEZ';
		$Form->Erstelle_SelectFeld('FIL_ID',$FIL_ID,150,$EditModus,$SQL);
		$AWISCursorPosition='txtFIL_ID';
	}
	elseif($FilZugriff!='')
	{
		$FIL_ID=$FilZugriff;
		$Form->Erstelle_HiddenFeld('FIL_ID',$FIL_ID);
		$Form->Erstelle_TextLabel($FIL_ID,150);
		$Param['FIL_ID']=$FIL_ID;
		$AWISBenutzer->ParameterSchreiben('Formular_ProblemArtikel_1',serialize($Param));
	}
	else
	{
		$Form->Erstelle_TextFeld('FIL_ID',$FIL_ID,4,180,$EditModus,'','','','T','L','','',10);
		$AWISCursorPosition='txtFIL_ID';
	}
	$Form->ZeileEnde();

	$Form->Formular_Ende();


	$Reg = new awisRegister(6001);
	$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));

	/*
	//********************************************************
	// Daten suchen
	//********************************************************

	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
	$FIL_ID = (isset($_POST['txtFIL_ID'])?$_POST['txtFIL_ID']:'');

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmProblemartikel action=./problemartikel_Main.php?cmdAktion=ReifenOhneS'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST>');

	$Form->Formular_Start();

	$EditModus = ($Recht6000&6);

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',150);
	if(count($FilZugriffListe)>1)
	{
		$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
		$SQL .= ' FROM Filialen ';
		$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
		$SQL .= ' ORDER BY FIL_BEZ';
		$Form->Erstelle_SelectFeld('FIL_ID',$FIL_ID,150,$EditModus,$SQL);
		$AWISCursorPosition='txtFIL_ID';
	}
	elseif($FilZugriff!='')
	{
		$FIL_ID=$FilZugriff;
		$Form->Erstelle_HiddenFeld('FIL_ID',$FIL_ID);
		$Form->Erstelle_TextLabel($FIL_ID,150);
	}
	else
	{
		$Form->Erstelle_TextFeld('FIL_ID',$FIL_ID,4,180,$EditModus,'','','','T','L','','',10);
		$AWISCursorPosition='txtFIL_ID';
	}
	$Form->ZeileEnde();



	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PRA']['NeueATUNR'].':',150);
	$Form->Erstelle_TextFeld('PRA_AST_ATUNR','',8,100,$EditModus,'','','','T');
	$AWISCursorPosition = ($AWISCursorPosition==''?'txtPRA_AST_ATUNR':$AWISCursorPosition);

	$Form->Erstelle_HiddenFeld('PRA_KATEGORIE',1);
	$Form->ZeileEnde();

	$Form->Trennzeile();


	$SQL = 'SELECT * FROM (';
	$SQL .= 'SELECT problemartikel.*, PAB_ISTBESTAND, PAB_BEMERKUNG, PAB_DATUM, FIB_BESTAND, AST_ATUNR, AST_BEZEICHNUNGWW';
	$SQL .= ' , CASE WHEN PAB_ISTBESTAND IS NULL THEN 1 WHEN PAB_ISTBESTAND > 0 THEN 2 ELSE 3 END AS SORT';
	$SQL .= ', row_number() over(partition by PRA_KEY ORDER BY PAB_DATUM DESC) as Zeile';
	$SQL .= ', CASE WHEN PAB_DATUM = TRUNC(SYSDATE) THEN PAB_KEY ELSE 0 END AS PAB_KEY';
	$SQL .= ' FROM problemartikel';
	$SQL .= ' INNER JOIN artikelstamm ON PRA_AST_ATUNR = AST_ATUNR';
	$SQL .= ' INNER JOIN FilialBestand ON FIB_FIL_ID = '.$DB->FeldInhaltFormat('N0',$FIL_ID,false).' AND FIB_AST_ATUNR = AST_ATUNR';
	$SQL .= ' LEFT OUTER JOIN ProblemArtikelBestand ON PAB_PRA_KEY = PRA_KEY AND PAB_FIL_ID = '.$DB->FeldInhaltFormat('N0',$FIL_ID,false).'';
	$SQL .= ' WHERE FIB_FIL_ID = '.$DB->FeldInhaltFormat('N0',$FIL_ID,false);
	$SQL .= ' AND PRA_KATEGORIE = 1';
	//$SQL .= ' ORDER BY AST_ATUNR';
	$SQL .= ' ) DATEN ';
	$SQL .= ' WHERE Zeile=1';
	$SQL .= ' ORDER BY SORT, AST_ATUNR';
$Form->DebugAusgabe(1,$SQL);
	$rsPRA = $DB->RecordsetOeffnen($SQL);

	$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRA']['PRA_AST_ATUNR'],150);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIB']['FIB_BESTAND'],120);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAB']['PAB_ISTBESTAND'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAB']['PAB_BEMERKUNG'],300);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAB']['PAB_DATUM'],100);
	$Form->ZeileEnde();

	$DS=0;
	$Link= '';
	$LetzteGruppe=0;

	while(!$rsPRA->EOF())
	{
		if($LetzteGruppe!=$rsPRA->FeldInhalt('SORT'))
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['PAB']['txt_PAB_HINWEIS_'.$rsPRA->FeldInhalt('SORT')],2);
			$Form->ZeileEnde();

			$LetzteGruppe=$rsPRA->FeldInhalt('SORT');
		}


		$Form->ZeileStart();
		$Form->Erstelle_ListenFeld('#PRA_AST_ATUNR',$rsPRA->FeldInhalt('PRA_AST_ATUNR'),0,150,false,($DS%2),'',$Link,'T','L',$rsPRA->FeldInhalt('AST_BEZEICHNUNGWW'));

		$AltBestand=$rsPRA->FeldInhalt('PAB_ISTBESTAND');
		$AltBemerkung=$rsPRA->FeldInhalt('PAB_BEMERKUNG');
		$AltStand = $rsPRA->FeldInhalt('PAB_DATUM');
		if($rsPRA->FeldInhalt('PAB_ISTBESTAND')=='')
		{
			$SQL = 'SELECT PAB_ISTBESTAND,PAB_BEMERKUNG, PAB_DATUM';
			$SQL .= ' FROM ProblemartikelBestand';
			$SQL .= ' WHERE PAB_PRA_KEY = '.$rsPRA->FeldInhalt('PRA_KEY');
			$SQL .= ' AND PAB_FIL_ID = '.$FIL_ID;
			$SQL .= ' ORDER BY PAB_DATUM DESC';
			$rsPAB = $DB->RecordSetOeffnen($SQL);
			$AltBestand = $rsPAB->FeldInhalt('PAB_ISTBESTAND');
			$AltBemerkung = $rsPAB->FeldInhalt('PAB_BEMERKUNG');
			$AltStand = $rsPAB->FeldInhalt('PAB_DATUM');
		}
		$Form->Erstelle_ListenFeld('#FIBBESTAND_'.$rsPRA->FeldInhalt('PRA_KEY'),$rsPRA->FeldInhalt('FIB_BESTAND'),5,120,false,($DS%2),'','','N0','L');
		$Form->Erstelle_HiddenFeld('PAB_FIBBESTAND_'.$rsPRA->FeldInhalt('PRA_KEY'),$rsPRA->FeldInhalt('FIB_BESTAND'));
		$Form->Erstelle_ListenFeld('PAB_ISTBESTAND_'.$rsPRA->FeldInhalt('PRA_KEY').'_'.$rsPRA->FeldInhalt('PAB_KEY'),$AltBestand,5,100,$EditModus,($DS%2),'','','T','L');
		$Form->Erstelle_ListenFeld('PAB_BEMERKUNG_'.$rsPRA->FeldInhalt('PRA_KEY').'_'.$rsPRA->FeldInhalt('PAB_KEY'),$AltBemerkung,30,300,$EditModus,($DS%2),'','','T','L');
		$Form->Erstelle_ListenFeld('#STAND',$AltStand,5,100,false,($DS%2),'','','D','L');

		$Form->ZeileEnde();

		$rsPRA->DSWeiter();
		$DS++;
	}

	$Form->Formular_Ende();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht6000&6)!=0))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->Schaltflaeche('href', 'cmdDrucken', './problemartikel_Drucken_Liste.php?KAT=1'.(($FilZugriff=='' AND $FIL_ID!='')?'&FIL_ID='.$FIL_ID:''), '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}

	*/
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>