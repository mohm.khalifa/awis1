<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_keinPersonal');
$TextKonserven[]=array('Fehler','err_ATUNRBereitsVorhanden');
$TextKonserven[]=array('Fehler','err_keinegueltigeATUNR');
$TextKonserven[]=array('Fehler','err_RueckfuehrungAnlegen');
$TextKonserven[]=array('Fehler','err_EingabeGroesserNull');
$TextKonserven[]=array('Fehler','err_RueckfuehrungLagerwareschein');
$TextKonserven[]=array('Fehler','err_RueckfuehrungWarenbestaende');
$TextKonserven[]=array('Fehler','err_WertBereitsVorhanden');
$TextKonserven[]=array('Fehler','err_EingabeZuGross');
$TextKonserven[]=array('Fehler','err_EingabeWiederholen');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('RFK','RFK_%');
$TextKonserven[]=array('RFF','RFF_%');
$TextKonserven[]=array('Wort','lbl_weiter');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	//***********************************************
	// R�ckf�hrungsbearbeitung
	//***********************************************
	$Form->DebugAusgabe(1,$_POST);
	
	if(isset($_POST['txtRFK_KEY']))
	{
		$AWIS_KEY1=$_POST['txtRFK_KEY'];
		
		$Felder = $Form->NameInArray($_POST, 'txtRFK_',1,1);
					
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('RFK','RFK_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
			$FeldListe='';
			$SQL = '';
			
			if(floatval($_POST['txtRFK_KEY'])==0)		//Neue R�ckf�hrung
			{
				//Daten auf Vollst�ndigkeit pr�fen
				$Fehler = '';			
				$Pflichtfelder = array('RFK_FIL_ID');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if($_POST['txt'.$Pflichtfeld]=='-1')	// Filiale muss angegeben werden
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFK'][$Pflichtfeld].'<br>';
					}
				}
				
				$DB->SetzeBindevariable('RFK', 'var_N0_rfk_fil_id', '0'.$_POST['txtRFK_FIL_ID'], awisDatenbank::VAR_TYP_GANZEZAHL);
				
				$SQL = 'select RFK_KEY FROM RUECKFUEHRUNGSFILIALMENGENKOPF ';
				$SQL .= 'left join RUECKFUEHRUNGSSTATUS on RFS_KEY = RFK_RFS_KEY '; 			
				$SQL .= 'WHERE RFK_FIL_ID = :var_N0_rfk_fil_id AND RFS_STATUS < 31';
			
				$rsRFKFil = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFK'));
				if ($rsRFKFil->AnzahlDatensaetze()>=1)
				{
						$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungAnlegen'].' ('.$TXT_Speichern['RFK'][$Pflichtfeld].' '.$_POST['txtRFK_FIL_ID'].')<br>';
				}				
								
				// Wurden Fehler entdeckt? => Speichern abbrechen
				if($Fehler!='')
				{
					$Form->ZeileStart();
					$Form->Hinweistext($Fehler);
					$Form->ZeileEnde();
					
					$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Suche';
					$Form->SchaltflaechenStart();
					$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
					$Form->SchaltflaechenEnde();									

					die();
					//die('<span class=HinweisText>'.$Fehler.'</span>');
					
					$Speichern = false;
				}
				else 
				{
					$Speichern = true;
				}
				
				if($Speichern == true)
				{
					$rsRFS = $DB->RecordSetOeffnen('SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=30');
					$Status = $rsRFS->FeldInhalt('RFS_KEY');
					
					$DB->SetzeBindevariable('RFK', 'var_N0_rfk_fil_id', '0'.$_POST['txtRFK_FIL_ID'], awisDatenbank::VAR_TYP_GANZEZAHL);
					$SQL='SELECT NVL(MAX(RFK_LFDNR),0) as MAXLFDNR FROM RUECKFUEHRUNGSFILIALMENGENKOPF WHERE RFK_FIL_ID=:var_N0_rfk_fil_id';
					
					$rsLFDNR = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFK'));
					$LFDNR = $DB->FeldInhaltFormat('Z',$rsLFDNR->FeldInhalt('MAXLFDNR'))+1;
					
					
					$DB->SetzeBindevariable('RFK', 'var_N0_rfk_fil_id', '0'.$_POST['txtRFK_FIL_ID'], awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFK', 'var_N0_rfk_lfdnr', $LFDNR, awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFK', 'var_N0_rfk_persnrfiliale', (isset($_POST['txtRFK_PERSNRFILIALE'])?$_POST['txtRFK_PERSNRFILIALE']:''), awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFK', 'var_N0_rfk_persnrzentrale', (isset($_POST['txtRFK_PERSNRZENTRALE'])?$_POST['txtRFK_PERSNRZENTRALE']:''), awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('RFK', 'var_T_rfk_bemerkungfiliale', (isset($_POST['txtRFK_BEMERKUNGFILIALE'])?$_POST['txtRFK_BEMERKUNGFILIALE']:''), awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('RFK', 'var_T_rfk_bemerkungzentrale', (isset($_POST['txtRFK_BEMERKUNGZENTRALE'])?$_POST['txtRFK_BEMERKUNGZENTRALE']:''), awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('RFK', 'var_T_rfk_boxart', (isset($_POST['txtRFK_BOXART'])?$_POST['txtRFK_BOXART']:''), awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('RFK', 'var_T_rfk_boxean', (isset($_POST['txtRFK_BOXEAN'])?$_POST['txtRFK_BOXEAN']:''), awisDatenbank::VAR_TYP_TEXT);
					$DB->SetzeBindevariable('RFK', 'var_NO_rfk_rfs_key', $Status, awisDatenbank::VAR_TYP_GANZEZAHL );
					$DB->SetzeBindevariable('RFK', 'var_T_rfk_user', $AWISBenutzer->BenutzerName(), awisDatenbank::VAR_TYP_TEXT);
					
					$Fehler = '';
					$SQL = 'INSERT INTO RueckFuehrungsFilialMengenKopf ';
					$SQL .= '(RFK_FIL_ID, RFK_LFDNR, RFK_PERSNRFILIALE, RFK_PERSNRZENTRALE ';
					$SQL .= ',RFK_BEMERKUNGFILIALE,RFK_BEMERKUNGZENTRALE ';
					$SQL .= ',RFK_BOXART, RFK_BOXEAN, RFK_RFS_KEY ';
					$SQL .= ',RFK_USER, RFK_USERDAT) ';
					$SQL .= 'VALUES (:var_N0_rfk_fil_id, :var_N0_rfk_lfdnr, :var_N0_rfk_persnrfiliale, :var_N0_rfk_persnrzentrale';
					$SQL .= ',:var_T_rfk_bemerkungfiliale, :var_T_rfk_bemerkungzentrale';
					$SQL .= ',:var_T_rfk_boxart, :var_T_rfk_boxean, :var_NO_rfk_rfs_key';
					$SQL .= ',:var_T_rfk_user, SYSDATE)';
					
					if($DB->Ausfuehren($SQL,'',false, $DB->Bindevariablen('RFK'))===false)
					{
						awisErrorMailLink('rueckfuehrungsbearbeitung_speichern.php',1,$awisDBError['message'],'809101509');
						die();
					}
					$SQL = 'SELECT seq_RFK_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
				}
			}
			else 					// ge�nderte R�ckf�hrungskopf
			{
				$Felder = explode(';',$Form->NameInArray($_POST, 'txtRFK_',1,1));			
				$FehlerListe = array();
				$UpdateFelder = '';
				$Fehler = '';
				
				$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', '0'.$_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
				
				$SQL='SELECT * FROM RueckFuehrungsFilialmengenKopf WHERE RFK_key=:var_N0_rfk_key';
				
				$rsRFK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFK'));
							
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						switch ($FeldName)
						{
							default:							
								$WertNeu=$DB->FeldInhaltFormat($rsRFK->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST[$Feld],true);	
								break;
						}					

						$WertAlt=$DB->FeldInhaltFormat($rsRFK->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsRFK->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsRFK->FeldInhalt($FeldName),true);

						switch (substr($rsRFK->FeldInfo($FeldName,'TypKZ'),0,1))				
						{
							case 'T':
								$varName=':var_T_'.strtolower($FeldName);
								$varTyp=awisDatenbank::VAR_TYP_TEXT;
								break;
							case 'N':
								$varName=':var_N0_'.strtolower($FeldName);
								$varTyp=awisDatenbank::VAR_TYP_GANZEZAHL;
								break;
							default:
								$varName=':var_'.strtolower($FeldName);
								$varTyp=awisDatenbank::VAR_TYP_UNBEKANNT;							
								break;
						}
						
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
								
								if (($FeldName == 'RFK_PERSNRFILIALE' and  $_POST['txt'.$FeldName] != '') or 
									($FeldName == 'RFK_PERSNRZENTRALE' and  $_POST['txt'.$FeldName] != ''))
								{
									$DB->SetzeBindevariable('PER', 'var_N0_persnr', $_POST['txt'.$FeldName], awisDatenbank::VAR_TYP_GANZEZAHL);
									
									$SQL = 'SELECT NAME, VORNAME, ABTEILUNG, PERSNR ';
									$SQL .= 'FROM V_PERSONAL_KOMPLETT ';
									$SQL .= 'WHERE (DATUM_EINTRITT <= SYSDATE AND ';
									$SQL .= '(DATUM_AUSTRITT > SYSDATE OR DATUM_AUSTRITT IS NULL)) ';
									$SQL .= 'AND PERSNR = :var_N0_persnr';
								
									$rsRFF = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('PER'));
									
									if ($rsRFF->AnzahlDatensaetze()==0)
									{										
										$Fehler .= $TXT_Speichern['Fehler']['err_keinPersonal'].'<br>';							
									}

													// Wurden Fehler entdeckt? => Speichern abbrechen
									if($Fehler!='')
									{
										$Form->ZeileStart();
										$Form->Hinweistext($Fehler);
										$Form->ZeileEnde();
										
										$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$_POST['txtRFK_KEY'];
										$Form->SchaltflaechenStart();
										$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
										$Form->SchaltflaechenEnde();				
										die();										
									}									
								}			
		
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$varName;
									$DB->SetzeBindevariable('UPD', $varName, $_POST[$Feld], $varTyp);																				
								}
							}
						}
					}
				}
				
				
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsRFK->FeldInhalt('RFK_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
						//$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$DB->SetzeBindevariable('UPD', 'var_N0_rfk_key', '0'.$_POST['txtRFK_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
					$DB->SetzeBindevariable('UPD', 'var_T_rfk_user', $AWISBenutzer->BenutzerName(), awisDatenbank::VAR_TYP_TEXT);
					
					$SQL = 'UPDATE RueckFuehrungsFilialMengenKopf SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', RFK_user=:var_T_rfk_user';
					$SQL .= ', RFK_userdat=sysdate';
					$SQL .= ' WHERE RFK_key=:var_N0_rfk_key';
					
					if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('UPD'))===false)
					{
						$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'809121042');
						throw new awisException('Fehler beim Speichern',809101511,$SQL,2);
					}
				}
			}
		}
	}
	
	//***********************************************************************************
	//** Rueckfuehrungsfilialmengenpositionen speichern
	//***********************************************************************************
	if(isset($_POST['txtRFF_KEY']))
	{
		if(intval($_POST['txtRFF_KEY'])==0)		// Neue Position
		{
			$Fehler = '';
			
			$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			//Status holen
			$SQL = 'SELECT RFS_STATUS';
			$SQL .= ' FROM RueckFuehrungsFilialmengenKopf ';			
			$SQL .= ' INNER JOIN RueckFuehrungsStatus on RFS_KEY = RFK_RFS_KEY';
			$SQL .= ' WHERE RFK_KEY = :var_N0_rfk_key';
			
			$rsRFS = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFK'));
						
			// Daten auf Vollst�ndigkeit pr�fen
			$Pflichtfelder = array('RFF_AST_ATUNR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF'][$Pflichtfeld].'<br>';
				}
			}

			if($rsRFS->FeldInhalt('RFS_STATUS')>=31)
			{
				// Daten auf Vollst�ndigkeit pr�fen
				$Pflichtfelder = array('RFF_RUECKLIEFKZ');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if($_POST['txt'.$Pflichtfeld]=='')	
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF'][$Pflichtfeld].'<br>';
					}
				}
			}
			//Pr�fen, ob diese ATUNR in dieser R�ckf�hrung bereits vorhanden ist
			
			$DB->SetzeBindevariable('RFF', 'var_T_rff_ast_atunr', strtoupper($_POST['txtRFF_AST_ATUNR']), awisDatenbank::VAR_TYP_TEXT);
			$DB->SetzeBindevariable('RFF', 'var_T_rff_rueckliefkz', (isset($_POST['txtRFF_RUECKLIEFKZ'])?$_POST['txtRFF_RUECKLIEFKZ']:' '), awisDatenbank::VAR_TYP_TEXT);
			$DB->SetzeBindevariable('RFF', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$SQL = 'SELECT RFF_KEY';
			$SQL .= ' FROM RueckFuehrungsFilialmengenKopf ';
			$SQL .= ' LEFT JOIN RueckFuehrungsFilialmengenpos on RFF_RFK_KEY = RFK_KEY';			
			$SQL .= ' WHERE RFF_AST_ATUNR = :var_T_rff_ast_atunr';
			$SQL .= ' AND RFF_RUECKLIEFKZ = :var_T_rff_rueckliefkz';
			$SQL .= ' AND RFK_KEY = :var_N0_rfk_key';
			
			$rsRFF = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFF'));
			
			if ($rsRFF->AnzahlDatensaetze()>0)
			{
				$DB->SetzeBindevariable('RFT', 'var_T_rft_kennung', (isset($_POST['txtRFF_RUECKLIEFKZ'])?$_POST['txtRFF_RUECKLIEFKZ']:' '), awisDatenbank::VAR_TYP_TEXT);

				$SQL='SELECT RFT_BEZEICHNUNG FROM RUECKFUEHRUNGSTYP WHERE RFT_KENNUNG = :var_T_rft_kennung';

				$rsRFT = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFT'));

				$Fehler .= $TXT_Speichern['Fehler']['err_WertBereitsVorhanden'].' ATUNR=>"'.$_POST['txtRFF_AST_ATUNR'].'" | RUECKLIEFERUNGSTYP=>"'.$rsRFT->FeldInhalt('RFT_BEZEICHNUNG').'"<br>';
			}					
			
			//Wenn ATUNR eingegeben wurde, pr�fen ob diese eine g�ltige ist
			if ($_POST['txtRFF_AST_ATUNR']!='')
			{
				/* ho,20110124
				$SQL = 'SELECT *';
				$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR = \''.$_POST['txtRFF_AST_ATUNR'].'\' AND BITAND(AST_IMQ_ID,2)=2';			
				*/ 
				
				$DB->SetzeBindevariable('AST', 'var_T_ast_atunr', strtoupper($_POST['txtRFF_AST_ATUNR']), awisDatenbank::VAR_TYP_TEXT);
				
				$SQL = 'SELECT ast.*, wug.wug_wgr_id, wug.wug_id' ;
				$SQL .= ' FROM artikelstamm ast';
				$SQL .= ' LEFT JOIN warenuntergruppen wug';
				$SQL .= ' ON ast.ast_wug_key = wug.wug_key';
				$SQL .= ' WHERE ast.ast_atunr = :var_T_ast_atunr AND BITAND(AST_IMQ_ID,2)=2';
				
				$rsAST = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('AST'));
				if($rsAST->AnzahlDatensaetze()==0)
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_keinegueltigeATUNR'].':'.$_POST['txtRFF_AST_ATUNR'].'<br>';
				}
				else //ho,20110124
				{
					$TParam = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelSonderMeld'));
					foreach ($TParam as $Suche)
					{
						if (substr(strtoupper($rsAST->FeldInhalt('AST_ATUNR')), 0, strlen($Suche)) == strtoupper($Suche))
						{
							$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungWarenbestaende'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
							break;
						}
					}
					//ho,20130713 A006881/A008720
					$TParam = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelBadWG',true));
					foreach ($TParam as $WGSort)
					{
						$WGSort = explode('~',$WGSort);
						if (isset($WGSort[0]) AND $WGSort[0]!='')//WG
						{
							if (isset($WGSort[1]) AND  $WGSort[1]!='')//SORT
							{
								if ($rsAST->FeldInhalt('WUG_WGR_ID')==$WGSort[0] AND $rsAST->FeldInhalt('WUG_ID') == $WGSort[1])
								{
									$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
									break;
								}
							}
							else
							{
								if ($rsAST->FeldInhalt('WUG_WGR_ID')==$WGSort[0])
								{
									$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
									break;
								}
							}
						}
						elseif (isset($WGSort[1]) AND $WGSort[1]!='')//SORT
						{
							if ($rsAST->FeldInhalt('WUG_ID')==$WGSort[1])
							{
								$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
								break;
							}
						}
					}
					
					$TParam = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelBadArtikel',true));
					foreach ($TParam as $Artikelbereich)
					{
						$Artikelbereich = explode('-',$Artikelbereich);
						if ($rsAST->FeldInhalt('AST_ATUNR') >= $Artikelbereich[0] AND $rsAST->FeldInhalt('AST_ATUNR') <= $Artikelbereich[1])
						{
							$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
							break;
						}
					}
					//ho,20130713 Ende
				}
			}
			
			if ($DB->FeldInhaltFormat('Z', $rsRFS->FeldInhalt('RFS_STATUS'))==30) //R�ckf�hrung offen
			{
				//Mengefiliale muss mit angegeben werden und muss gr��er als 0 sein
				// Daten auf Vollst�ndigkeit pr�fen
				if($_POST['txtRFF_MENGEFILIALE']=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF']['RFF_MENGEFILIALE'].'<br>';
				}
				//Wenn Mengefiliale eingegeben wurde, pr�fen ob diese > 0 ist
				elseif ($_POST['txtRFF_MENGEFILIALE']<=0)
				{							
					$Fehler .= $TXT_Speichern['RFF'][$Pflichtfeld].': '.$TXT_Speichern['Fehler']['err_EingabeGroesserNull'].'<br>';
				}
			}
			
			if ($DB->FeldInhaltFormat('Z', $rsRFS->FeldInhalt('RFS_STATUS'))==31) //R�ckf�hrung offen
			{
				//Mengezentrale muss mit angegeben werden und muss gr��er gleich 0 sein
				// Daten auf Vollst�ndigkeit pr�fen
				if($_POST['txtRFF_MENGEZENTRALE']=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF']['RFF_MENGEZENTRALE'].'<br>';
				}
				//Wenn Mengefiliale eingegeben wurde, pr�fen ob diese >= 0 ist
				elseif ($_POST['txtRFF_MENGEZENTRALE']<0)
				{							
					$Fehler .= $TXT_Speichern['RFF']['RFF_MENGEZENTRALE'].': '.$TXT_Speichern['Fehler']['err_EingabeGroesserNull'].'<br>';
				}
			}			
			
			// Pr�fung der RUECKLIEFKZ und der dazugeh�rigen Pflichtbeschreibung
			$DB->SetzeBindevariable('RFT', 'var_T_rft_kennung', (isset($_POST['txtRFF_RUECKLIEFKZ'])?$_POST['txtRFF_RUECKLIEFKZ']:' '), awisDatenbank::VAR_TYP_TEXT);
			
			$SQL='SELECT RFT_WWS_BEM_ERFORDERLICH, RFT_WWS_BEM_MAX_LAENGE';
			$SQL.=' FROM AWIS.RUECKFUEHRUNGSTYP';
			$SQL.=' WHERE RFT_KENNUNG = :var_T_rft_kennung';
			
			$rsRFT = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFT'));
			if(intval($rsRFT->FeldInhalt('RFT_WWS_BEM_ERFORDERLICH'))==1)
			{
				$Pflichtfelder = array('RFF_BEMERKUNGZENTRALE');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if(trim($_POST['txt'.$Pflichtfeld])=='')	
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF'][$Pflichtfeld].'<br>';
					}
					
					/* 20140425-CA: Auf Anforderung FA im AWIS ausgeschalten. G�ltig nur noch in WWS-Bestand SST
					if(strlen($_POST['txt'.$Pflichtfeld])>$rsRFT->FeldInhalt('RFT_WWS_BEM_MAX_LAENGE'))
					{
						$AKTLEN=strlen($_POST['txt'.$Pflichtfeld]);
						$MAXLEN=$rsRFT->FeldInhalt('RFT_WWS_BEM_MAX_LAENGE');
						
						$Ausgabe=$TXT_Speichern['Fehler']['err_EingabeZuGross'].' - '.$TXT_Speichern['RFF'][$Pflichtfeld].'<br>';
						$Ausgabe=str_replace('$AKTLEN',$AKTLEN,$Ausgabe);
						$Ausgabe=str_replace('$MAXLEN',$MAXLEN,$Ausgabe);
						
						$Fehler .= $Ausgabe;
					}
					*/
				}
			}
			
			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->ZeileStart();
				$Form->Hinweistext($Fehler);
				$Form->ZeileEnde();
				$Speichern=false;				
				
				$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();				
				die();				
				
				//die('<span class=HinweisText>'.$Fehler.'</span>');				
			}
			else 
			{
				$Speichern = true;
			}
			
			if ($Speichern == true)
			{
				
				$DB->SetzeBindevariable('RFF', 'var_N0_rff_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
				$DB->SetzeBindevariable('RFF', 'var_T_rff_ast_atunr', (isset($_POST['txtRFF_AST_ATUNR'])?strtoupper($_POST['txtRFF_AST_ATUNR']):''), awisDatenbank::VAR_TYP_TEXT);
				$DB->SetzeBindevariable('RFF', 'var_N0_rff_mengefiliale', (isset($_POST['txtRFF_MENGEFILIALE'])?$_POST['txtRFF_MENGEFILIALE']:''), awisDatenbank::VAR_TYP_GANZEZAHL);
				$DB->SetzeBindevariable('RFF', 'var_N0_rff_mengezentrale', (isset($_POST['txtRFF_MENGEZENTRALE'])?$_POST['txtRFF_MENGEZENTRALE']:''), awisDatenbank::VAR_TYP_GANZEZAHL);
				$DB->SetzeBindevariable('RFF', 'var_T_rff_bemerkungzentrale', (isset($_POST['txtRFF_BEMERKUNGZENTRALE'])?trim($_POST['txtRFF_BEMERKUNGZENTRALE']):''), awisDatenbank::VAR_TYP_TEXT);
				$DB->SetzeBindevariable('RFF', 'var_T_rff_rueckliefkz', (isset($_POST['txtRFF_RUECKLIEFKZ'])?$_POST['txtRFF_RUECKLIEFKZ']:' '), awisDatenbank::VAR_TYP_TEXT);
				$DB->SetzeBindevariable('RFF', 'var_T_rff_user', $AWISBenutzer->BenutzerName(), awisDatenbank::VAR_TYP_TEXT);
				
				$SQL = 'INSERT INTO RueckFuehrungsFilialmengenPos';
				$SQL .= '(RFF_RFK_KEY, RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE';
				$SQL .= ', RFF_BEMERKUNGZENTRALE, RFF_RUECKLIEFKZ, RFF_USER, RFF_USERDAT)';
				$SQL .= 'VALUES (:var_N0_rff_rfk_key, :var_T_rff_ast_atunr, :var_N0_rff_mengefiliale, :var_N0_rff_mengezentrale,';
				$SQL .= ':var_T_rff_bemerkungzentrale, :var_T_rff_rueckliefkz, :var_T_rff_user, SYSDATE)';
				
				if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFF'))===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'2008809121132');
				}
				$AWIS_KEY2=0;//Liste zeigen
			}
		}
		else		// zu�ndernde Position
		{
			$Felder = explode(';',$Form->NameInArray($_POST, 'txtRFF_',1,1));
			$FehlerListe = array();
			$UpdateFelder = '';
			
			$DB->SetzeBindevariable('RFF', 'var_N0_rff_key', '0'.$_POST['txtRFF_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
			
			$SQL='SELECT * FROM RueckFuehrungsFilialmengenpos WHERE RFF_KEY = :var_N0_rff_key';
			
			$rsRFF = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFF'));

			$FeldListe = '';
			
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);				
				
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					//echo '##'.$awisRSInfoName[$FeldName]['TypKZ'].'<br>';
					switch ($FeldName)
					{
						default:
							$WertNeu=$DB->FeldInhaltFormat($rsRFF->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
							break;
					}
					
					$WertAlt=$DB->FeldInhaltFormat($rsRFF->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsRFF->FeldInfo($FeldName,'TypKZ'),$rsRFF->FeldInhalt($FeldName),true);
					
					switch (substr($rsRFF->FeldInfo($FeldName,'TypKZ'),0,1))				
					{
						case 'T':
							$varName=':var_T_'.strtolower($FeldName);
							$varTyp=awisDatenbank::VAR_TYP_TEXT;
							break;
						case 'N':
							$varName=':var_N0_'.strtolower($FeldName);
							$varTyp=awisDatenbank::VAR_TYP_GANZEZAHL;
							break;
						default:
							$varName=':var_'.strtolower($FeldName);
							$varTyp=awisDatenbank::VAR_TYP_UNBEKANNT;							
							break;
					}
					
					//ATUNR Pr�fen							
					if ($FeldName == 'RFF_AST_ATUNR')
					{
						// Daten auf Vollst�ndigkeit pr�fen
						$Pflichtfelder = array('RFF_AST_ATUNR');

						foreach($Pflichtfelder AS $Pflichtfeld)
						{
							if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
							{
								$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF'][$Pflichtfeld].'<br>';
							}
						}
						
									// Wurden Fehler entdeckt? => Speichern abbrechen
						if($Fehler!='')
						{
							//die('<span class=HinweisText>'.$Fehler.'</span>');
							$Form->ZeileStart();
							$Form->Hinweistext($Fehler);
							$Form->ZeileEnde();
							
							$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
							$Form->SchaltflaechenStart();
							$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
							$Form->SchaltflaechenEnde();				
							die();
							
						}
														
					}	
					
					if ($FeldName == 'RFF_AST_ATUNR' and  $_POST['txt'.$FeldName] != '')
					{								
						$DB->SetzeBindevariable('RFF', 'var_T_rff_ast_atunr', strtoupper($_POST['txt'.$FeldName]), awisDatenbank::VAR_TYP_TEXT);
						$DB->SetzeBindevariable('RFF', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
						$DB->SetzeBindevariable('RFF', 'var_N0_rff_key', '0'.$_POST['txtRFF_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
						
						//Pr�fen, ob diese ATUNR in dieser R�ckf�hrung bereits vorhanden ist
						$SQL = 'SELECT RFF_KEY, NVL(RFF_RUECKLIEFKZ,\' \') AS RFF_RUECKLIEFKZ';
						$SQL .= ' FROM RueckFuehrungsFilialmengenKopf ';
						$SQL .= ' LEFT JOIN RueckFuehrungsFilialmengenpos on RFF_RFK_KEY = RFK_KEY';			
						$SQL .= ' WHERE RFF_AST_ATUNR = :var_T_rff_ast_atunr';
						$SQL .= ' AND RFK_KEY = :var_N0_rfk_key';
						$SQL .= ' AND RFF_KEY <> :var_N0_rff_key';
						
						$Form->DebugAusgabe(1,$SQL);
						$Form->DebugAusgabe(1,$DB->Bindevariablen('RFF',false));
						
						$rsRFFSuch = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFF'));
						
						if ($rsRFFSuch->AnzahlDatensaetze()>0)
						{
							while(!$rsRFFSuch->EOF())
							{
								if($rsRFFSuch->FeldInhalt('RFF_RUECKLIEFKZ')==(isset($_POST['txtRFF_RUECKLIEFKZ'])?$_POST['txtRFF_RUECKLIEFKZ']:' '))
								{
									$DB->SetzeBindevariable('RFT', 'var_T_rft_kennung', (isset($_POST['txtRFF_RUECKLIEFKZ'])?$_POST['txtRFF_RUECKLIEFKZ']:' '), awisDatenbank::VAR_TYP_TEXT);
									
									$SQL='SELECT RFT_BEZEICHNUNG FROM RUECKFUEHRUNGSTYP WHERE RFT_KENNUNG = :var_T_rft_kennung';
					
									$rsRFT = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFT'));
					
									$Fehler .= $TXT_Speichern['Fehler']['err_WertBereitsVorhanden'].' ATUNR=>"'.$_POST['txtRFF_AST_ATUNR'].'" | RUECKLIEFERUNGSTYP=>"'.$rsRFT->FeldInhalt('RFT_BEZEICHNUNG').'"<br>';
								}
								$rsRFFSuch->DSWeiter();
							}
						}													
											
						//Wenn ATUNR eingegeben wurde, pr�fen ob diese eine g�ltige ist
						if ($_POST['txt'.$FeldName]!='')
						{
							//ho,20120130
							//$SQL = 'SELECT *';
							//$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR = \''.$_POST['txt'.$FeldName].'\' AND BITAND(AST_IMQ_ID,2)=2';			

							$DB->SetzeBindevariable('AST', 'var_T_ast_atunr', strtoupper($_POST['txt'.$FeldName]), awisDatenbank::VAR_TYP_TEXT);
						
							$SQL = 'SELECT ast.*, wug.wug_wgr_id, wug.wug_id' ;
							$SQL .= ' FROM artikelstamm ast';
							$SQL .= ' LEFT JOIN warenuntergruppen wug';
							$SQL .= ' ON ast.ast_wug_key = wug.wug_key';
							$SQL .= ' WHERE ast.ast_atunr = :var_T_ast_atunr AND BITAND(AST_IMQ_ID,2)=2';
							
							$rsAST = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('AST'));
							if($rsAST->AnzahlDatensaetze()==0)
							{
								$Fehler .= $TXT_Speichern['Fehler']['err_keinegueltigeATUNR'].':'.$_POST['txtRFF_AST_ATUNR'].'<br>';
							}
							else //ho,20110124
							{
								$TParam = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelSonderMeld'));
								foreach ($TParam as $Suche)
								{
									if (substr(strtoupper($rsAST->FeldInhalt('AST_ATUNR')), 0, strlen($Suche)) == strtoupper($Suche))
									{
										$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungWarenbestaende'].':'.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
										break;
									}
								}
								//ho,20130713 A006881/A008720
								$TParam = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelBadWG',true));
								foreach ($TParam as $WGSort)
								{
									$WGSort = explode('~',$WGSort);
									if (isset($WGSort[0]) AND $WGSort[0]!='')//WG
									{
										if (isset($WGSort[1]) AND  $WGSort[1]!='')//SORT
										{
											if ($rsAST->FeldInhalt('WUG_WGR_ID')==$WGSort[0] AND $rsAST->FeldInhalt('WUG_ID') == $WGSort[1])
											{
												$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
												break;
											}
										}
										else
										{
											if ($rsAST->FeldInhalt('WUG_WGR_ID')==$WGSort[0])
											{
												$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
												break;
											}
										}
									}
									elseif (isset($WGSort[1]) AND $WGSort[1]!='')//SORT
									{
										if ($rsAST->FeldInhalt('WUG_ID')==$WGSort[1])
										{
											$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
											break;
										}
									}
								}
								
								$TParam = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelBadArtikel',true));
								foreach ($TParam as $Artikelbereich)
								{
									$Artikelbereich = explode('-',$Artikelbereich);
									if ($rsAST->FeldInhalt('AST_ATUNR') >= $Artikelbereich[0] AND $rsAST->FeldInhalt('AST_ATUNR') <= $Artikelbereich[1])
									{
										$Fehler .= $TXT_Speichern['Fehler']['err_RueckfuehrungLagerwareschein'].': '.$rsAST->FeldInhalt('AST_ATUNR').'<br>';
										break;
									}
								}
								//ho,20130713 Ende
							}
						}
					}
					
					// Pr�fung der RUECKLIEFKZ und der dazugeh�rigen Pflichtbeschreibung
					$DB->SetzeBindevariable('RFT', 'var_T_rft_kennung', (isset($_POST['txtRFF_RUECKLIEFKZ'])?$_POST['txtRFF_RUECKLIEFKZ']:' '), awisDatenbank::VAR_TYP_TEXT);
					
					$SQL='SELECT RFT_WWS_BEM_ERFORDERLICH, RFT_WWS_BEM_MAX_LAENGE';
					$SQL.=' FROM AWIS.RUECKFUEHRUNGSTYP';
					$SQL.=' WHERE RFT_KENNUNG = :var_T_rft_kennung';
					
					$rsRFT = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFT'));
					if(intval($rsRFT->FeldInhalt('RFT_WWS_BEM_ERFORDERLICH'))==1)
					{
						$Pflichtfelder = array('RFF_BEMERKUNGZENTRALE');
						foreach($Pflichtfelder AS $Pflichtfeld)
						{
							if(trim($_POST['txt'.$Pflichtfeld])=='')	
							{
								$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF'][$Pflichtfeld].'<br>';
							}
							/* 20140425-CA: Auf Anforderung FA im AWIS ausgeschalten. G�ltig nur noch in WWS-Bestand SST
							if(strlen($_POST['txt'.$Pflichtfeld])>$rsRFT->FeldInhalt('RFT_WWS_BEM_MAX_LAENGE'))
							{
								$AKTLEN=strlen($_POST['txt'.$Pflichtfeld]);
								$MAXLEN=$rsRFT->FeldInhalt('RFT_WWS_BEM_MAX_LAENGE');
								
								$Ausgabe=$TXT_Speichern['Fehler']['err_EingabeZuGross'].' - '.$TXT_Speichern['RFF'][$Pflichtfeld].'<br>';
								$Ausgabe=str_replace('$AKTLEN',$AKTLEN,$Ausgabe);
								$Ausgabe=str_replace('$MAXLEN',$MAXLEN,$Ausgabe);
								
								$Fehler .= $Ausgabe;
							}*/
						}
					}

					// Wurden Fehler entdeckt? => Speichern abbrechen
					if($Fehler!='')
					{
						//die('<span class=HinweisText>'.$Fehler.'</span>');
						$Form->ZeileStart();
						$Form->Hinweistext($Fehler);
						$Form->ZeileEnde();
					
						$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
						$Form->SchaltflaechenStart();									
						$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
						$Form->SchaltflaechenEnde();				
						die();									
					}
					
					//echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{												
							$FeldListe .= ', '.$FeldName.'=';							
							
							
							$DB->SetzeBindevariable('RFK', 'var_N0_rfk_key', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
							
							//Status holen
							$SQL = 'SELECT RFS_STATUS';
							$SQL .= ' FROM RueckFuehrungsFilialmengenKopf ';			
							$SQL .= ' INNER JOIN RueckFuehrungsStatus on RFS_KEY = RFK_RFS_KEY';
							$SQL .= ' WHERE RFK_KEY = :var_N0_rfk_key';
							
							$rsRFS = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFK'));
			
							if ($DB->FeldInhaltFormat('Z', $rsRFS->FeldInhalt('RFS_STATUS'))==30) //R�ckf�hrung offen
							{
    							//Mengefiliale muss mit angegeben werden und muss gr��er als 0 sein
    							// Daten auf Vollst�ndigkeit pr�fen
								if($_POST['txtRFF_MENGEFILIALE']=='')	// Name muss angegeben werden
								{
									$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF']['RFF_MENGEFILIALE'].'<br>';
								}
								
								//Wenn Mengefiliale eingegeben wurde, pr�fen ob diese > 0 ist
								elseif ($_POST['txtRFF_MENGEFILIALE']<=0)
								{																	
									$Fehler .= $TXT_Speichern['RFF']['RFF_MENGEFILIALE'].': '.$TXT_Speichern['Fehler']['err_EingabeGroesserNull'].'<br>';										
								}				
							}

							if ($DB->FeldInhaltFormat('Z', $rsRFS->FeldInhalt('RFS_STATUS'))==31) //R�ckf�hrung offen
							{
    							//Mengefiliale muss mit angegeben werden und muss gr��er als 0 sein
    							// Daten auf Vollst�ndigkeit pr�fen
								if($_POST['txtRFF_MENGEZENTRALE']=='')	// Name muss angegeben werden
								{
									$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFF']['RFF_MENGEZENTRALE'].'<br>';
								}
								
								//Wenn Mengefiliale eingegeben wurde, pr�fen ob diese >= 0 ist
								elseif ($_POST['txtRFF_MENGEZENTRALE']<0)
								{																	
									$Fehler .= $TXT_Speichern['RFF']['RFF_MENGEZENTRALE'].': '.$TXT_Speichern['Fehler']['err_EingabeGroesserNull'].'<br>';										
								}				
							}

							// Wurden Fehler entdeckt? => Speichern abbrechen
							if($Fehler!='')
							{
								//die('<span class=HinweisText>'.$Fehler.'</span>');
								$Form->ZeileStart();
								$Form->Hinweistext($Fehler);
								$Form->ZeileEnde();
							
								$Link='./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$AWIS_KEY1;
								$Form->SchaltflaechenStart();									
								$Form->Schaltflaeche('href','cmdAnforderung',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
								$Form->SchaltflaechenEnde();				
								die();									
							}								
							
							
							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$varName;
								
								if ($FeldName == 'RFF_AST_ATUNR')
								{
									$DB->SetzeBindevariable('UPD', $varName, strtoupper($_POST[$Feld]), $varTyp);											
								}else
								{
									$DB->SetzeBindevariable('UPD', $varName, $_POST[$Feld], $varTyp);											
								}
							}
						}
					}
				}
			}			
			
			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsRFF->FeldInhalt('RFF_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
					$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen(1, $Meldung, 809121138, $TXT_Speichern['Fehler']['err_EingabeWiederholen']);
			}
			elseif($FeldListe!='')
			{
				$DB->SetzeBindevariable('UPD', 'var_N0_rff_key', $_POST['txtRFF_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
				$DB->SetzeBindevariable('UPD', 'var_T_rff_user', $AWISBenutzer->BenutzerName(), awisDatenbank::VAR_TYP_TEXT);
				
				$SQL = 'UPDATE RueckFuehrungsFilialmengenpos SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', RFF_user=:var_T_rff_user';
				$SQL .= ', RFF_userdat=sysdate';
				$SQL .= ' WHERE RFF_KEY = :var_N0_rff_key';				

				if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('UPD'))===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200809121140');
				}
			}
			$AWIS_KEY2 = 0;//Liste zeigen
		}
	}

	if(isset($_POST['cmdAdmin_x'])){
        $SQL = 'UPDATE RueckFuehrungsFilialmengenpos SET';
        $SQL .= '  RFF_user = ' . $DB->WertSetzen('RFF','T', $AWISBenutzer->BenutzerName());

        if(isset($_POST['txtadmin_RFT_AENDERN']) and $_POST['txtadmin_RFT_AENDERN'] != ''){
            if($_POST['txtadmin_RFT_AENDERN'] == ' '){
                $SQL .= ", RFF_RUECKLIEFKZ = ' ' ";
            }else{
                $SQL .= ', RFF_RUECKLIEFKZ = ' . $DB->WertSetzen('RFF','T',$_POST['txtadmin_RFT_AENDERN']);
            }
        }

        if(isset($_POST['txtadmin_RFF_MENGEZENTRALE']) and $_POST['txtadmin_RFF_MENGEZENTRALE'] == '1'){
            $SQL .= ' , RFF_MENGEZENTRALE = RFF_MENGEFILIALE';
        }
        $SQL .= ' , RFF_userdat = sysdate';
        $SQL .= ' WHERE RFF_RFK_KEY = ' . $DB->WertSetzen('RFF','T', $_POST['txtRFK_KEY']);;
        $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFF'));



    }

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);	
}
catch (Exception $ex)
{
	
}
?>