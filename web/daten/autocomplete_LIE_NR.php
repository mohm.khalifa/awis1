<?php
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 01.12.2016
 * Time: 19:17
 */

require_once 'awisRest.inc';

$awisRest = new awisRest();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$SQL = 'Select * from (';
$SQL .= 'select lie_nr as key, ';

$WGRRelevant = (isset($_GET['txtWGR_ID_Vorschlag']) and $_GET['txtWGR_ID_Vorschlag'] != '');

if($WGRRelevant){
    $SQL  .='CASE';
    $SQL .='       WHEN';
    $SQL  .=' (SELECT WGI_WERT from  WARENGRUPPENINFOS wgi';
    $SQL .='   WHERE wgi_wert        = lie_nr';
    $SQL .='   AND wgi_wgt_id     = 2';
    $SQL .='   AND (wgi_wgr_id IS NULL';
    $SQL .='   OR wgi_wgr_id      = ' . $DB->WertSetzen('LIE','T',$_GET['txtWGR_ID_Vorschlag']);
    $SQL .=' )  AND (wgi_wug_id   IS NULL';
    $SQL .='   OR wgi_wug_id      = ' . $DB->WertSetzen('LIE','T',$_GET['txtWUG_ID_Vorschlag']);;

    $SQL .= ' ))  IS NOT NULL';
    $SQL .='       THEN \'$ - \'';
    $SQL .='       ELSE \'\'';
    $SQL .='     END';
    $SQL .='     ||';
}

$SQL .= 'lie_nr||\' \'||LIE_NAME1 as value from lieferanten ';

$SQL .= 'order by 1 asc';

$SQL .= ') WHERE upper(value) ' . $DB->LikeOderIst('%' . $_REQUEST['term'] . '%', awisDatenbank::AWIS_LIKE_UPPER, 'LIE');

$rs = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('LIE'));

$daten = array();

while (!$rs->EOF()) {

    $Value = utf8_encode($rs->FeldInhalt(1));
    $Label = utf8_encode($rs->FeldInhalt(2));

    $daten[] = array(
        'value' => $Value,
        'label' => $Label
    );

    $rs->DSWeiter();
}

$awisRest->sendeResponse($daten, 'application/json');
