<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;
global $Params;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$Gruppenrecht = awisBenutzerRecht($con, 1701);
		//  1=externe Abrechnungen
		//	2=Zeitarbeitsfirmen
		//	4=externe Reinigungsfirmen
		//  8=Sonderausweise
		//  16=Sonstige Filialen
		//  32=Besucher/Lieferanten
		//  64=PDF erstellen
		//  128=Zeitarbeitsfirmen Ausland
		//  256=externe Abrechnungen 2
		
if($Gruppenrecht==0)
{
    awisEreignis(3,1000,'Sonderausweis_Gruppenrecht',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Gruppenrechte!</span>");
}

if(isset($_REQUEST['Reset']) && $_REQUEST['Reset']!=TRUE)
{
	$Params=explode(';',awis_BenutzerParameter($con, "Sonderausweis_Suche", $AWISBenutzer->BenutzerName()));
}

awis_Debug(1,$Params);

echo "<form name=frmSonderausweis method=post action=./sonderausweis_Main.php?cmdAktion=Liste>";

// Tabelle aufbauen
echo "<table id=DatenTabelle><tr>";
echo "<tr><td width=200><b>Personalnummer</b></td>";
echo "<td><input type=text name=txtPersonalnummer size=20 value='".$Params[0]."'></td>";
echo "</tr>";
echo "<tr><td width=200><b>EAN - Nummer</b></td>";
echo "<td><input type=text name=txtEAN size=20 value='".$Params[2]."'></td>";
echo "</tr>";
echo "<tr><td width=200><b>Filial - Nummer</b></td>";
echo "<td><input type=text name=txtFIL_ID size=5 value='".$Params[4]."'></td>";
echo "</tr>";
echo "<tr><td width=200><b>Firma</b></td>";
echo "<td><input type=text name=txtFirma size=35 value='".$Params[3]."'></td>";
echo "</tr>";
echo "<tr><td width=200><b>Name / Vorname</b></td>";
echo "<td><input type=text name=txtName size=30 value='".$Params[1]."'>&nbsp;<input type=text name=txtVorname size=30 value='".$Params[15]."'></td>";
echo "</tr>";
echo "<tr><td width=200><b>Stra�e</b></td>";
echo "<td><input type=text name=txtStra�e size=35 value='".$Params[5]."'></td>";
echo "</tr>";
echo "<tr><td width=200><b>PLZ / Ort</b></td>";
echo "<td><input type=text name=txtPLZ size=6 value='".$Params[6]."'>&nbsp;<input type=text name=txtOrt size=35 value='".$Params[7]."'></td>";
echo "</tr>";
echo "<tr>";
echo "<td width=200><b>Bereich</b></td><td><select name=txtBereich>";
	echo "<option value=''>Bitte w�hlen...</option>";
		$rsBereich = awisOpenRecordset($con, "SELECT DISTINCT(SAW_BEREICH) FROM SONDERAUSWEISE");
		$rsBereichZeilen = $awisRSZeilen;
		
		for($BereichZeile=0;$BereichZeile<$rsBereichZeilen;$BereichZeile++)
		{
			if($Params[8]==$rsBereich['SAW_BEREICH'][$BereichZeile])
			{
				echo "<option value=" . $rsBereich['SAW_BEREICH'][$BereichZeile] . " selected='selected'>" . $rsBereich['SAW_BEREICH'][$BereichZeile] . "</option>";
			}
			else 
			{
				echo "<option value=" . $rsBereich['SAW_BEREICH'][$BereichZeile] . ">" . $rsBereich['SAW_BEREICH'][$BereichZeile] . "</option>";
			}			
		}
	
	echo "</select></td>";
echo "</tr>";
echo "<tr>";
echo "<td width=200><b>Ausweisgruppe</b></td>";
echo "<td><select name=txtAWGruppe>";
echo "<option value=''>Bitte w�hlen...</option>";
	$rsAWGruppe = awisOpenRecordset($con, "SELECT * FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT ORDER BY SAG_KEY");
	$rsAWGruppeZeilen = $awisRSZeilen;
	
	for($AWGruppeZeile=0;$AWGruppeZeile<$rsAWGruppeZeilen;$AWGruppeZeile++)
	{
		if($Params[9]==$rsAWGruppe['SAG_KEY'][$AWGruppeZeile])
		{
			echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " selected='selected' title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][$AWGruppeZeile]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][$AWGruppeZeile]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
		}
		else
		{
			echo "<option value=" . $rsAWGruppe['SAG_KEY'][$AWGruppeZeile] . " title='AW-Nr.:".$rsAWGruppe['SAG_NUMMERNKREISVON'][$AWGruppeZeile]." - ".$rsAWGruppe['SAG_NUMMERNKREISBIS'][$AWGruppeZeile]."'>" . $rsAWGruppe['SAG_GRUPPE'][$AWGruppeZeile] . "</option>";
		}
	}
echo "</select></td>";
echo "</tr>";

echo "<tr>";
echo "<td width=200><b>T&auml;tigkeit</b></td>";
echo "<td><select name=txtTaetigkeit>";
echo "<option value=''>Bitte w�hlen...</option>";
	$rsTaetigkeit = awisOpenRecordset($con, "SELECT DISTINCT(SAW_TAETIGKEIT) AS TAETIGKEIT FROM SONDERAUSWEISE WHERE SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,".$Gruppenrecht.")=SAG_RECHTEBIT) ORDER BY 1 NULLS FIRST");
	$rsTaetigkeitZeilen = $awisRSZeilen;

	for($TaetigkeitZeile=0;$TaetigkeitZeile<$rsTaetigkeitZeilen;$TaetigkeitZeile++)
	{
		if($Params[10]==$rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile])
		{
			echo "<option value=" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . " selected='selected'>" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
		}
		else
		{
			echo "<option value=" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . ">" . $rsTaetigkeit['TAETIGKEIT'][$TaetigkeitZeile] . "</option>";
		}
	}
		
echo "</select>";
echo "</tr>";
echo "<tr><td width=200><b>Eintritt / Austritt</b></td>";
echo "<td><input type=text name=txtEintritt size=10 value='".$Params[11]."'>&nbsp;<input type=text name=txtAustritt size=10 value='".$Params[12]."'></td>";
echo "</tr>";
echo "<tr><td width=200><b>G&uuml;ltig von / G&uuml;ltig bis</b></td>";
echo "<td><input type=text name=txtGueltigVon size=10 value='".$Params[13]."'>&nbsp;<input type=text name=txtGueltigBis size=10 value='".$Params[14]."'></td>";
echo "</tr>";

echo "</table>";

print "<hr><br>&nbsp;<input accesskey=w tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten (Alt+w)' name=cmdSuche value=Aktualisieren>";
print "&nbsp;<img accesskey=r tabindex=99 src=/bilder/radierer.png alt='Formularinhalt l�schen (Alt+r)' name=cmdReset onclick=location.href='./sonderausweis_Main.php?Reset=True';>";

print "</form>";

// Cursor in das erste Feld setzen
print "<Script Language=JavaScript>";
print "document.getElementsByName(\"txtPersonalnummer\")[0].focus();";
echo '</SCRIPT>';

?>
</body>
</html>