<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisTeamplanAPI.php';

class awisTeamplanImport
{

    private $_DB;

    private $_PET_IDs;

    private $_LAN_CODES;

    private $_DebugLevel;

    private $_FEB_FEB;

    private $_Werkzeuge;

    private $_Benutzer;

    private $_EMAIL = 'awis@de.atu.de';

    function __construct($Benutzer = 'awis_jobs', $DebugLevel)
    {
        $this->_DebugLevel = $DebugLevel;
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();

        $this->_Benutzer = awisBenutzer::Init($Benutzer);

        $this->_Werkzeuge =  new awisWerkzeuge();

        $this->initPET_IDs();
        $this->initLAN();
        $this->_InitAsesFilialebenen();;
        $this->initFEBFEB();
    }

    private function _InitAsesFilialebenen()
    {
        $SQL = 'update ASESFILIALEBENEN set AFE_STATUS = 2';
        $this->_DB->Ausfuehren($SQL);
    }

    public function starteImport($Filialebenen = false)
    {
        $awisTeamplanAPI = new awisTeamplanAPI();

        $MasterDataList = $awisTeamplanAPI->MasterDataListService();

        //    $this->_Log('Habe folgende Daten erhalten: ' . json_encode($MasterDataList));

        $i = 0;
        if ($MasterDataList) {
            $AnzahlMa = count($MasterDataList);
            foreach ($MasterDataList as $MaDaten) {
                $i++;
                $this->_Log("Verarbeite $i von $AnzahlMa (" . round(($i / $AnzahlMa) * 100, 2) . '%)');

                $PerNr = $this->WertDecoden($MaDaten['employeeId']);
                $Vorname = $this->WertDecoden($MaDaten['firstName']);
                $Nachname = $this->WertDecoden($MaDaten['namePrefix']);
                $Nachname .= $this->WertDecoden($Nachname != '' ? ' ' : '');
                $Nachname .= $this->WertDecoden($MaDaten['lastName']);
                $Geschlecht = $this->WertDecoden($MaDaten['gender']);

                switch ($this->WertDecoden(strtoupper($Geschlecht[0]))) {
                    case 'M':
                        $ANR_ID = 1;
                        break;
                    default:
                        $ANR_ID = 2;
                        break;
                }
                $Geburtsdatum = $this->WertDecoden($MaDaten['dateOfBirth']);
                $LanCode = 'DE';
                $LAN_WWS_KENN = $this->WWS_KENN($LanCode);
                $Eintrittsdatum = $this->WertDecoden($MaDaten['entryDate']);
                $Austrittsdatum = $this->WertDecoden($MaDaten['leaveDate']);
                $Stammfiliale = $this->WertDecoden($MaDaten['storeId']);
                $Stammfiliale = $Stammfiliale != '' ? $Stammfiliale : 0;
                $Taetigkeit = $this->WertDecoden($MaDaten['occupation']);
                $PET_ID = $this->_holePET_ID($Taetigkeit, $LAN_WWS_KENN);
                $Bezirk = $this->WertDecoden($MaDaten['domain']);
                $Regionalzenter = $this->WertDecoden($MaDaten['regionalCenter']);
                $BetriebsleiterPersNr = $this->WertDecoden($MaDaten['factoryManager']);
                $FilialleiterPersNr = $this->PersNrExtrahieren($MaDaten['shopManager']);
                $WerkstattleiterPersNr = $this->PersNrExtrahieren($MaDaten['repairShopManager']);
                $BezirksleiterPersNr = $this->PersNrExtrahieren($MaDaten['areaManager']);
                $RegionalleiterPersNr = $this->PersNrExtrahieren($MaDaten['regionalManager']);
                $DistrictContraction = $this->WertDecoden($MaDaten['districtContraction']);
                $Kostenstelle = $this->WertDecoden($MaDaten['costCenterId']);
                $PerNrAlt = $this->WertDecoden($MaDaten['employeeIdOld']);
                $Bereich = $this->WertDecoden($MaDaten['activityBranch']);
                $GiftCardNumber = $this->WertDecoden($MaDaten['giftCardNumber']);
                $PLZ = $this->WertDecoden($MaDaten['zipcode']);
                $Ort = $this->WertDecoden($MaDaten['city']);
                $Strasse = $this->WertDecoden($MaDaten['fullStreet']); //Strasse mit Hausnummer + Adresszusatz (bspw. Dr. Kilianstrasse 2b)

                $this->_mergePersonal($PerNr, $Stammfiliale, $ANR_ID, $Nachname, $Vorname, $Eintrittsdatum, $Austrittsdatum, $Geburtsdatum, $PET_ID, $LAN_WWS_KENN,$PLZ,$Ort,$Strasse);
                $this->_mergePersonal_komplett($PerNr, $PerNrAlt, $Stammfiliale, $Nachname, $Vorname, $Taetigkeit, $Bereich, $Kostenstelle, $GiftCardNumber, $Eintrittsdatum, $Austrittsdatum, $Geburtsdatum, $LanCode,$PLZ,$Ort,$Strasse);

                if ($Filialebenen){
                    switch ($PerNr) {
                        case $BetriebsleiterPersNr:
                        case $FilialleiterPersNr:
                            $this->_MergeAsesFilialebenen($PerNr, 1, $Stammfiliale, 'FIL');
                            break;
                        case $WerkstattleiterPersNr:
                            $this->_MergeAsesFilialebenen($PerNr, 2, $Stammfiliale, 'FIL');
                            break;
                        case $BezirksleiterPersNr:
                            if ($FEB_KEY = $this->_ErmittleFEB_KEY($Bezirk)) {
                                $this->_MergeAsesFilialebenen($PerNr, 25, $FEB_KEY, 'FEB');
                            }
                    }
                }
            }
        }
        if($Filialebenen){
            $this->_DeleteAsesFilialebenen();
            $this->AsesFilialebenen2Filialebnenen();
        }
    }

    private function AsesFilialebenen2Filialebnenen()
    {
        $this->_DB->TransaktionBegin();

        //Alle relevanten Zuordnungen auf Zwischenstatus 2(=Nicht beim aktuellen Import dabei) setzen
        $SQL = ' update FILIALEBENENROLLENZUORDNUNGEN set FRZ_JOB_STATUS = 2  ';
        $SQL .= ' where FRZ_FER_KEY in (select FER_KEY from FILIALEBENENROLLEN where FER_MAX_ANZ > 0) ';

        $this->_DB->Ausfuehren($SQL, '', true);

        $SQL = 'select * from ASESFILIALEBENEN';
        $SQL .= ' inner join FILIALEBENENROLLEN on AFE_FER_KEY = FER_KEY';
        $SQL .= ' where FER_MAX_ANZ > 0'; //Rollen die f�r Job geeignet sind

        $rsAFE = $this->_DB->RecordSetOeffnen($SQL);

        $ErfolgreicheDS = 0;

        while (!$rsAFE->EOF()) {
            var_dump($rsAFE->FeldInhalt('AFE_PER_NR'));
            //KON_KEY Ermitteln
            $SQL = 'select KON_KEY from KONTAKTE where KON_PER_NR = ' . $this->_DB->WertSetzen('FRZ', 'T', $rsAFE->FeldInhalt('AFE_PER_NR'));
            $SQL .= ' and KON_STATUS in (\'A\',\'V\')';

            $rsKON = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FRZ'));

            //Kein KON_KEY gefunden => Schlecht
            if ($rsKON->AnzahlDatensaetze() == 0) {
                $this->_Werkzeuge->EMail($this->_EMAIL, 'Warning ASES-Import: KON_KEY konnte nicht ermittelt werden',
                                         'Personalnummer ' . $rsAFE->FeldInhalt('AFE_PER_NR') , 2, '', 'awis@de.atu.eu');

                var_dump('PersNr fehlt');
                //Die Kontakte aus dem AD haben keine Personalnummer -.-
                $rsAFE->DSWeiter();
                continue;
            }


            $SQL = 'merge into FILIALEBENENROLLENZUORDNUNGEN DEST';
            $SQL .= ' using (';
            $SQL .= '           select ';
            $SQL .= $this->_DB->WertSetzen('FRZ', 'N0', $rsAFE->FeldInhalt('AFE_XXX_KEY')) . ' as FRZ_XXX_KEY, ';
            $SQL .= $this->_DB->WertSetzen('FRZ', 'T', $rsAFE->FeldInhalt('AFE_XTN_KUERZEL')) . ' as FRZ_XTN_KUERZEL, ';
            $SQL .= $this->_DB->WertSetzen('FRZ', 'N0', $rsAFE->FeldInhalt('AFE_FER_KEY')) . ' as FRZ_FER_KEY, ';
            $SQL .= $this->_DB->WertSetzen('FRZ', 'N0', $rsKON->FeldInhalt('KON_KEY')) . ' as FRZ_KON_KEY ';
            $SQL .= '           from';
            $SQL .= '               DUAL';
            $SQL .= '       )';
            $SQL .= ' SRC on ( SRC.FRZ_XXX_KEY = DEST.FRZ_XXX_KEY';
            $SQL .= '          and SRC.FRZ_XTN_KUERZEL = DEST.FRZ_XTN_KUERZEL';
            $SQL .= '          and SRC.FRZ_FER_KEY = DEST.FRZ_FER_KEY';
            $SQL .= '          and SRC.FRZ_KON_KEY = DEST.FRZ_KON_KEY and DEST.FRZ_GUELTIGAB <= SYSDATE';
            $SQL .= '          and DEST.FRZ_GUELTIGBIS >= SYSDATE )';
            $SQL .= ' when matched then update';
            $SQL .= ' set DEST.FRZ_USERDAT = SYSDATE,';
            $SQL .= '     DEST.FRZ_USER = \'ASES-Import\',';
            $SQL .= '     DEST.FRZ_JOB_STATUS = 1';
            $SQL .= ' when not matched then';
            $SQL .= ' insert (';
            $SQL .= '     FRZ_XXX_KEY,';
            $SQL .= '     FRZ_XTN_KUERZEL,';
            $SQL .= '     FRZ_FER_KEY,';
            $SQL .= '     FRZ_KON_KEY,';
            $SQL .= '     FRZ_GUELTIGAB,';
            $SQL .= '     FRZ_GUELTIGBIS,';
            $SQL .= '     FRZ_BEMERKUNG,';
            $SQL .= '     FRZ_USER,';
            $SQL .= '     FRZ_USERDAT,';
            $SQL .= '     FRZ_JOB_STATUS )';
            $SQL .= ' values';
            $SQL .= '     ( SRC.FRZ_XXX_KEY,';
            $SQL .= '       SRC.FRZ_XTN_KUERZEL,';
            $SQL .= '       SRC.FRZ_FER_KEY,';
            $SQL .= '       SRC.FRZ_KON_KEY,';
            $SQL .= '     trunc(SYSDATE),';
            $SQL .= '     \'31.12.2030\',';
            $SQL .= '     \'ASES-Import\',';
            $SQL .= '     \'ASES-Import\',';
            $SQL .= '       SYSDATE,';
            $SQL .= '     1 )';


            $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('FRZ'));

            $ErfolgreicheDS++;
            $rsAFE->DSWeiter();
        }


        $Schwellwert = $this->_Benutzer->ParameterLesen('ASES_EBENEN_SCHWELLWERT');

        var_dump($ErfolgreicheDS);
        //Schwellwert erreicht?
        if($ErfolgreicheDS >= $Schwellwert){

            $SQL = 'update FILIALEBENENROLLENZUORDNUNGEN ';
            $SQL .= ' set FRZ_GUELTIGBIS = trunc(sysdate)-1 ,';
            $SQL .= ' FRZ_STATUS = 3 ,'; //Ausgelaufen durch Job
            $SQL .= ' FRZ_USER = \'ASES-Import\', ';
            $SQL .= ' FRZ_USERDAT = sysdate ';
            $SQL .= ' WHERE FRZ_STATUS = 2 '; //Alle die nicht mehr gekommen sind

            $this->_DB->Ausfuehren($SQL,'',true);

            $this->_DB->TransaktionCommit();
        }else{
            echo __LINE__ . '<br>';
            $this->_DB->TransaktionRollback();
            $this->_Werkzeuge->EMail($this->_EMAIL, 'ERROR ASES-Import: Schwellwert nicht erreicht. Rolle zur�ck.',
                                     'Habe keine ' .$Schwellwert. ' Datens�tze auf die Ebenen gebracht. Bitte pr�fen und ggf. Schwellwert (XPP_BEZEICHNUNG=ASES_EBENEN_SCHWELLWERT) korrigieren. Habe alles zur�ckgerollt. ', 2, '', 'awis@de.atu.eu');
        }


        //Gibt es auf einer Ebene zuviele Rollen?
        $SQL  ='select  * from (';
        $SQL .='         select';
        $SQL .='             FRZ_XXX_KEY,';
        $SQL .='             FRZ_XTN_KUERZEL,';
        $SQL .='             FRZ_FER_KEY,';
        $SQL .='             FER_MAX_ANZ,';
        $SQL .='             FER_BEZEICHNUNG,';
        $SQL .='             count(*) as ANZ';
        $SQL .='         from';
        $SQL .='             FILIALEBENENROLLENZUORDNUNGEN';
        $SQL .='             inner join FILIALEBENENROLLEN on FER_KEY = FRZ_FER_KEY';
        $SQL .='         where';
        $SQL .='             FRZ_GUELTIGAB <= SYSDATE';
        $SQL .='             and FRZ_GUELTIGBIS >= SYSDATE';
        $SQL .='             and FER_MAX_ANZ > 0';
        $SQL .='         group by';
        $SQL .='             FRZ_XXX_KEY,';
        $SQL .='             FRZ_XTN_KUERZEL,';
        $SQL .='             FRZ_FER_KEY,';
        $SQL .='             FER_MAX_ANZ,';
        $SQL .='             FER_BEZEICHNUNG';
        $SQL .='     )';
        $SQL .=' where';
        $SQL .='     FER_MAX_ANZ < ANZ';

        $rsFRZ = $this->_DB->RecordSetOeffnen($SQL);
        $Text = '';
        while(!$rsFRZ->EOF()){

            $Text .= $rsFRZ->FeldInhalt('FER_BEZEICHNUNG') . ' auf XTN ' .$rsFRZ->FeldInhalt('FRZ_XTN_KUERZEL') . ' XXX ' .  $rsFRZ->FeldInhalt('FRZ_XXX_KEY');
                ': Aktuell vorhanden: ' . $rsFRZ->FeldInhalt('ANZ') . ' Erlaubt: ' . $rsFRZ->FeldInhalt('FER_MAX_ANZ') . PHP_EOL;

            $rsFRZ->DSWeiter();
        }

        if($Text){
            $this->_Werkzeuge->EMail($this->_EMAIL, 'Warning ASES-Import: Zu viele Rollen auf Ebene ',
                                     'Habe keine ' .$Schwellwert. ' Datens�tze auf die Ebenen gebracht. Bitte pr�fen und ggf. Schwellwert (XPP_BEZEICHNUNG=ASES_EBENEN_SCHWELLWERT) korrigieren. Habe alles zur�ckgerollt. ', 2, '', 'awis@de.atu.eu');

        }
    }

    /**
     * Ermittelt den FEB_KEY f�r die Ebene und legt sie ggf. an
     *
     * @param $Ebenenbezeichnung
     *
     * @return string
     */
    private function _ErmittleFEB_KEY($Ebenenbezeichnung)
    {
        
        if(isset($this->_FEB_FEB[$Ebenenbezeichnung])){
            return $this->_FEB_FEB[$Ebenenbezeichnung]['FEB_KEY'];
        }
        
        $SQL = 'select';
        $SQL .= '     FEB_KEY';
        $SQL .= ' from';
        $SQL .= '     FILIALEBENEN';
        $SQL .= ' where';
        $SQL .= '     FEB_GUELTIGAB <= SYSDATE';
        $SQL .= '     and FEB_GUELTIGBIS >= SYSDATE';
        $SQL .= '     and FEB_BEZEICHNUNG = ' . $this->_DB->WertSetzen('FEB', 'T', $Ebenenbezeichnung);

        $rsFEB = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FEB'));

        $FEB_KEY = '';

        if ($rsFEB->AnzahlDatensaetze() == 0) {
            //Ebene nicht gefunden, nachschauen, ob die Ebene evtl angelegt werden kann, Aufgrund der Anfangsbuchstaben der Ebenen
            foreach ($this->_FEB_FEB as $FEB_FEB) {
                $Substring = $FEB_FEB['FEB_ASESJOB_SUBTR'];
                if (substr($Ebenenbezeichnung, 0, strlen($Substring)) == $Substring) {
                    $FEB_FEB_KEY = $FEB_FEB['FEB_KEY'];
                    $FEB_EBENE = $FEB_FEB['FEB_EBENE'] + 1;

                    $FEB_KEY = $this->_FEB_Anlegen($Ebenenbezeichnung, $FEB_EBENE, $FEB_FEB_KEY, 'Teamplanimport');
                }
            }
        } else {
            //FEB_KEY gefunden
            $FEB_KEY = $rsFEB->FeldInhalt('FEB_KEY');
        }

        return $FEB_KEY;
    }

    /**
     *
     * Legt eine Filialebene an und liefert den Key zur�ck
     *
     * @param $FEB_BEZEICHNUNG
     * @param $FEB_EBENE
     * @param $FEB_FEB_KEY
     * @param $FEB_BEMERKUNG
     *
     * @return string
     */
    private function _FEB_Anlegen($FEB_BEZEICHNUNG, $FEB_EBENE, $FEB_FEB_KEY, $FEB_BEMERKUNG)
    {
        $SQL = 'insert into FILIALEBENEN (';
        $SQL .= '     FEB_BEZEICHNUNG,';
        $SQL .= '     FEB_EBENE,';
        $SQL .= '     FEB_FEB_KEY,';
        $SQL .= '     FEB_GUELTIGAB,';
        $SQL .= '     FEB_GUELTIGBIS,';
        $SQL .= '     FEB_BEMERKUNG,';
        $SQL .= '     FEB_USER,';
        $SQL .= '     FEB_USERDAT';
        $SQL .= '     ';
        $SQL .= ' ) values (';
        $SQL .= $this->_DB->WertSetzen('FEB', 'T', $FEB_BEZEICHNUNG) . ' , ';
        $SQL .= $this->_DB->WertSetzen('FEB', 'T', $FEB_EBENE) . ' , ';
        $SQL .= $this->_DB->WertSetzen('FEB', 'T', $FEB_FEB_KEY) . ' , ';
        $SQL .= '     trunc(SYSDATE),';
        $SQL .= '     \'31.12.2030\', ';
        $SQL .= $this->_DB->WertSetzen('FEB', 'T', $FEB_BEMERKUNG) . ' , ';
        $SQL .= '     \'ASES-Import\',';
        $SQL .= '     SYSDATE';
        $SQL .= ' )';

        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('FEB'));

        $SQL = 'select SEQ_FEB_KEY.currval from dual';
        $rsKey = $this->_DB->RecordSetOeffnen($SQL);

        return $rsKey->FeldInhalt(1);
    }

    /**
     * Merged einen Datensatz in die Tabelle AsesFilialebenen
     *
     * @param $PER_NR
     * @param $FER_KEY
     * @param $XXX_KEY
     * @param $XTN_KUERZEL
     */
    private function _MergeAsesFilialebenen($PER_NR, $FER_KEY, $XXX_KEY, $XTN_KUERZEL)
    {
        $this->_Log("Lege $PER_NR als FER_KEY $FER_KEY auf $XTN_KUERZEL $XXX_KEY an");

        $SQL = 'merge into ASESFILIALEBENEN DEST';
        $SQL .= ' using (';
        $SQL .= '           select';
        $SQL .= $this->_DB->WertSetzen('AFE', 'T', $PER_NR) . ' as PER_NR,';
        $SQL .= $this->_DB->WertSetzen('AFE', 'N0', $FER_KEY) . ' as FER_KEY,';
        $SQL .= $this->_DB->WertSetzen('AFE', 'N0', $XXX_KEY) . ' as XXX_KEY,';
        $SQL .= $this->_DB->WertSetzen('AFE', 'T', $XTN_KUERZEL) . ' as XTN_KUERZEL,';
        $SQL .= '               \'ASES-Import\' as IMPORTUSER,';
        $SQL .= '               SYSDATE as USERDAT';
        $SQL .= '           from';
        $SQL .= '               DUAL';
        $SQL .= '       )';
        $SQL .= ' SRC on ( AFE_PER_NR = PER_NR';
        $SQL .= '          and AFE_FER_KEY = FER_KEY';
        $SQL .= '          and AFE_XXX_KEY = XXX_KEY';
        $SQL .= '          and AFE_XTN_KUERZEL = XTN_KUERZEL )';
        $SQL .= ' when matched then update';
        $SQL .= ' set AFE_USERDAT = USERDAT,';
        $SQL .= '     AFE_USER = IMPORTUSER,';
        $SQL .= '     AFE_STATUS = 1';
        $SQL .= ' when not matched then';
        $SQL .= ' insert (';
        $SQL .= '     AFE_PER_NR,';
        $SQL .= '     AFE_FER_KEY,';
        $SQL .= '     AFE_XXX_KEY,';
        $SQL .= '     AFE_XTN_KUERZEL,';
        $SQL .= '     AFE_USER,';
        $SQL .= '     AFE_USERDAT,';
        $SQL .= '     AFE_STATUS )';
        $SQL .= ' values';
        $SQL .= '     ( PER_NR,';
        $SQL .= '       FER_KEY, XXX_KEY,';
        $SQL .= '       XTN_KUERZEL,';
        $SQL .= '       IMPORTUSER,';
        $SQL .= '       USERDAT,';
        $SQL .= '     1 )';

        $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('AFE'));
    }

    /**
     * L�scht alle inaktiven ASESFILIALEBENEN
     */
    private function _DeleteAsesFilialebenen()
    {
        $SQL = 'delete from ASESFILIALEBENEN where AFE_STATUS = 2';
        $this->_DB->Ausfuehren($SQL);
    }

    /**
     * Liefert die T�tigkeits-ID einer T�tigkeit
     *
     * @param $Taetigkeit
     * @param $LAN_WWS_KENN
     *
     * @return mixed
     */
    private function _holePET_ID($Taetigkeit, $LAN_WWS_KENN)
    {
        if (!isset($this->_PET_IDs[$LAN_WWS_KENN][md5($Taetigkeit)])) {
            $this->_insertPET($Taetigkeit, $LAN_WWS_KENN);
        }

        return $this->_PET_IDs[$LAN_WWS_KENN][md5($Taetigkeit)];
    }

    /**
     * Legt eine T�tigkeit an
     *
     * @param $Taetigkeit
     * @param $LAN_WWS_KENN
     */
    private function _insertPET($Taetigkeit, $LAN_WWS_KENN)
    {

        $this->_Log('Lege T�tigkeit an: ' . $Taetigkeit . ' WWS_KENN: ' . $LAN_WWS_KENN, 1);

        $SQL = 'INSERT INTO PERSONALTAETIGKEITEN (';
        $SQL .= '     PET_ID,';
        $SQL .= '     PET_TAETIGKEIT,';
        $SQL .= '     PET_LAN_WWS_KENN,';
        $SQL .= '     PET_USER,';
        $SQL .= '     PET_USERDAT';
        $SQL .= ' ) VALUES (';
        $SQL .= '     SEQ_GLOBAL_KEY.NEXTVAL,';
        $SQL .= $this->_DB->WertSetzen('PET', 'T', $Taetigkeit) . ' , ';
        $SQL .= $this->_DB->WertSetzen('PET', 'T', $LAN_WWS_KENN) . ' , ';
        $SQL .= '     \'Teamplan\',';
        $SQL .= '     SYSDATE';
        $SQL .= ' )';

        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('PET'));

        $this->initPET_IDs();
    }

    /**
     * Liefert die WWS Kennung eines LAN_CODES
     *
     * @param $LAN_CODE
     *
     * @return string
     */
    private function WWS_KENN($LAN_CODE)
    {
        if (!isset($this->_LAN_CODES[$LAN_CODE])) {
            return 'BRD';
        }

        return $this->_LAN_CODES[$LAN_CODE];
    }

    /**
     * Initialisiert das Laender Array
     */
    private function initLAN()
    {

        $this->_Log('initLAN', 999);
        $SQL = 'select LAN_CODE, LAN_WWSKENN from LAENDER where LAN_WWSKENN is not null';

        $rsLAN = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsLAN->EOF()) {
            $this->_LAN_CODES[$rsLAN->FeldInhalt('LAN_CODE')] = $rsLAN->FeldInhalt('LAN_WWSKENN');
            $rsLAN->DSWeiter();
        }
    }

    /**
     * Initialisiert das Taetigkeits Array
     */
    private function initPET_IDs()
    {
        $this->_Log('initPET_IDs ', 999);
        $SQL = 'select';
        $SQL .= '     *';
        $SQL .= ' from';
        $SQL .= '     PERSONALTAETIGKEITEN';
        $SQL .= ' where';
        $SQL .= '     PET_USER = \'Teamplan\'';

        $rsPET = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsPET->EOF()) {
            $this->_PET_IDs[$rsPET->FeldInhalt('PET_LAN_WWS_KENN')][md5($rsPET->FeldInhalt('PET_TAETIGKEIT'))] = $rsPET->FeldInhalt('PET_ID');

            $rsPET->DSWeiter();
        }
    }

    /**
     * Mergt einen Datensatz in die Personaltabelle
     *
     * @param $PerNr
     * @param $Stammfiliale
     * @param $ANR_ID
     * @param $Nachname
     * @param $Vorname
     * @param $Eintrittsdatum
     * @param $Austrittsdatum
     * @param $Geburtsdatum
     * @param $PET_ID
     * @param $LanCode
     * @param $PLZ
     * @param $Ort
     * @param $Strasse
     */
    private function _mergePersonal($PerNr, $Stammfiliale, $ANR_ID, $Nachname, $Vorname, $Eintrittsdatum, $Austrittsdatum, $Geburtsdatum, $PET_ID, $LanCode,$PLZ,$Ort,$Strasse)
    {

        $this->_Log('Merge Mitarbeiter: ' . $PerNr, 999);
        $SQL = 'merge into PERSONAL DEST';
        $SQL .= ' using (';
        $SQL .= '           select';
        $SQL .= '  ' . $this->_DB->WertSetzen('PER', 'T', $PerNr) . ' AS PER_NR';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Stammfiliale) . ' AS PER_FIL_ID';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', 10) . ' AS PER_ABRECHNUNGSKREIS';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $ANR_ID) . ' AS PER_ANR_ID';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Nachname) . ' AS PER_NACHNAME';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Vorname) . ' AS PER_VORNAME';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'D', $Eintrittsdatum) . ' AS PER_EINTRITT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'D', $Austrittsdatum) . ' AS PER_AUSTRITTGEPLANT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'D', $Austrittsdatum) . ' AS PER_AUSTRITT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'D', $Geburtsdatum) . ' AS PER_GEBURTSDATUM';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $PET_ID) . ' AS PER_PET_ID';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $LanCode) . ' AS PER_LAN_WWS_KENN';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $PLZ) . ' AS PER_PLZ';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Ort) . ' AS PER_ORT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Strasse) . ' AS PER_STRASSE';

        $SQL .= '           from';
        $SQL .= '               DUAL';
        $SQL .= '       )';
        $SQL .= ' SRC on ( DEST.PER_NR = SRC.PER_NR )';
        $SQL .= ' when matched then update';
        $SQL .= ' set PER_FIL_ID = SRC.PER_FIL_ID,';
        $SQL .= '     PER_ABRECHNUNGSKREIS = SRC.PER_ABRECHNUNGSKREIS,';
        $SQL .= '     PER_ANR_ID = SRC.PER_ANR_ID,';
        $SQL .= '     PER_NACHNAME = SRC.PER_NACHNAME,';
        $SQL .= '     PER_VORNAME = SRC.PER_VORNAME,';
        $SQL .= '     PER_EINTRITT = SRC.PER_EINTRITT,';
        $SQL .= '     PER_AUSTRITTGEPLANT = SRC.PER_AUSTRITTGEPLANT,';
        $SQL .= '     PER_AUSTRITT = SRC.PER_AUSTRITT,';
        $SQL .= '     PER_GEBURTSDATUM= SRC.PER_GEBURTSDATUM,';
        $SQL .= '     PER_PET_ID = SRC.PER_PET_ID,';
        $SQL .= '     PER_LAN_WWS_KENN = SRC.PER_LAN_WWS_KENN,';
        $SQL .= '     PER_USER = \'Teamplan\',';
        $SQL .= '     PER_USERDAT = SYSDATE,';
        $SQL .= '     PER_PLZ = SRC.PER_PLZ,';
        $SQL .= '     PER_ORT = SRC.PER_ORT,';
        $SQL .= '     PER_STRASSE = SRC.PER_STRASSE';
        $SQL .= ' when not matched then';
        $SQL .= ' insert (';
        $SQL .= '     PER_NR,';
        $SQL .= '     PER_FIL_ID,';
        $SQL .= '     PER_ABRECHNUNGSKREIS,';
        $SQL .= '     PER_ANR_ID,';
        $SQL .= '     PER_NACHNAME,';
        $SQL .= '     PER_VORNAME,';
        $SQL .= '     PER_EINTRITT,';
        $SQL .= '     PER_AUSTRITTGEPLANT,';
        $SQL .= '     PER_AUSTRITT,';
        $SQL .= '     PER_GEBURTSDATUM,';
        $SQL .= '     PER_PET_ID,';
        $SQL .= '     PER_LAN_WWS_KENN,';
        $SQL .= '     PER_USER,';
        $SQL .= '     PER_USERDAT,';
        $SQL .= '     PER_PLZ,';
        $SQL .= '     PER_ORT,';
        $SQL .= '     PER_STRASSE )';
        $SQL .= ' values';
        $SQL .= '     ( SRC.PER_NR,';
        $SQL .= '       SRC.PER_FIL_ID,';
        $SQL .= '       SRC.PER_ABRECHNUNGSKREIS,';
        $SQL .= '       SRC.PER_ANR_ID,';
        $SQL .= '       SRC.PER_NACHNAME,';
        $SQL .= '       SRC.PER_VORNAME,';
        $SQL .= '       SRC.PER_EINTRITT,';
        $SQL .= '       SRC.PER_AUSTRITTGEPLANT,';
        $SQL .= '       SRC.PER_AUSTRITT,';
        $SQL .= '       SRC.PER_GEBURTSDATUM,';
        $SQL .= '       SRC.PER_PET_ID,';
        $SQL .= '       SRC.PER_LAN_WWS_KENN,';
        $SQL .= '     \'Teamplan\',';
        $SQL .= '       SYSDATE,';
        $SQL .= '       SRC.PER_PLZ,';
        $SQL .= '       SRC.PER_ORT,';
        $SQL .= '       SRC.PER_STRASSE )';

        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('PER'));
    }

    /**
     * Merged einen Datensatz in die Personal_komplett Tabelle
     *
     * @param $PerNr
     * @param $PerNrAlt
     * @param $Stammfiliale
     * @param $Nachname
     * @param $Vorname
     * @param $Taetigkeit
     * @param $Bereich
     * @param $Kostenstelle
     * @param $GiftCardNumber
     * @param $Eintrittsdatum
     * @param $Austrittsdatum
     * @param $Geburtsdatum
     * @param $LanCode
     * @param $PLZ
     * @param $Ort
     * @param $Strasse
     */
    private function _mergePersonal_komplett($PerNr, $PerNrAlt, $Stammfiliale, $Nachname, $Vorname, $Taetigkeit, $Bereich, $Kostenstelle, $GiftCardNumber, $Eintrittsdatum, $Austrittsdatum, $Geburtsdatum, $LanCode,$PLZ,$Ort,$Strasse)
    {
        $SQL = 'merge into PERSONAL_KOMPLETT DEST';
        $SQL .= ' using (';
        $SQL .= '           select';
        $SQL .= '  ' . $this->_DB->WertSetzen('PER', 'T', $PerNr) . ' AS PER_NR';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Stammfiliale) . ' AS PER_FIL_ID';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Nachname) . ' AS PER_NACHNAME';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Vorname) . ' AS PER_VORNAME';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'D', $Eintrittsdatum) . ' AS PER_EINTRITT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'D', $Austrittsdatum) . ' AS PER_AUSTRITT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'D', $Geburtsdatum) . ' AS PER_GEBURTSDATUM';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $LanCode) . ' AS PER_LAN_CODE';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', substr($Taetigkeit, 0, 25)) . ' AS PER_TAETIGKEIT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Kostenstelle) . ' AS PER_KOSTENSTELLE';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $PerNrAlt) . ' AS PER_NR_ALT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Bereich) . ' AS PER_BEREICH';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $GiftCardNumber) . ' AS PER_CARDNUMBER';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $PLZ) . ' AS PER_PLZ';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Ort) . ' AS PER_ORT';
        $SQL .= ' , ' . $this->_DB->WertSetzen('PER', 'T', $Strasse) . ' AS PER_STRASSE';
        $SQL .= '           from';
        $SQL .= '               DUAL';
        $SQL .= '       )';
        $SQL .= ' SRC on ( DEST.persnr = SRC.PER_NR )';
        $SQL .= ' when matched then update';
        $SQL .= ' set ';
        $SQL .= '     ABTEILUNG = SRC.PER_FIL_ID,';
        $SQL .= '      PERSNR_ALT = SRC.PER_NR_ALT,';
        $SQL .= '      NAME = SRC.PER_NACHNAME,';
        $SQL .= '      VORNAME = SRC.PER_VORNAME,';
        $SQL .= '      GEB_DATUM = SRC.PER_GEBURTSDATUM,';
        $SQL .= '      LAND_KZ = SRC.PER_LAN_CODE,';
        $SQL .= '      DATUM_EINTRITT = SRC.PER_EINTRITT,';
        $SQL .= '      DATUM_AUSTRITT = SRC.PER_AUSTRITT,';
        $SQL .= '      TAETIGKEIT = SRC.PER_TAETIGKEIT,';
        $SQL .= '      BEREICH = SRC.PER_BEREICH,';
        $SQL .= '      KOSTENSTELLE = SRC.PER_KOSTENSTELLE,';
        $SQL .= '      CARDNUMBER = SRC.PER_CARDNUMBER,';
        $SQL .= '      PLZ = SRC.PER_PLZ,';
        $SQL .= '      ORT = SRC.PER_ORT,';
        $SQL .= '      STRASSE = SRC.PER_STRASSE';
        $SQL .= ' when not matched then';
        $SQL .= ' insert (';
        $SQL .= '     PERSNR,';
        $SQL .= '     PERSNR_ALT,';
        $SQL .= '     NAME,';
        $SQL .= '     VORNAME,';
        $SQL .= '     GEB_DATUM,';
        $SQL .= '     LAND_KZ,';
        $SQL .= '     DATUM_EINTRITT,';
        $SQL .= '     DATUM_AUSTRITT,';
        $SQL .= '     TAETIGKEIT,';
        $SQL .= '     BEREICH,';
        $SQL .= '     ABTEILUNG,';
        $SQL .= '     KOSTENSTELLE,';
        $SQL .= '     CARDNUMBER,';
        $SQL .= '     PLZ,';
        $SQL .= '     ORT,';
        $SQL .= '     STRASSE';
        $SQL .= '  )';
        $SQL .= ' values';
        $SQL .= '     ( ';
        $SQL .= '       SRC.PER_NR,';
        $SQL .= '       SRC.PER_NR_ALT,';
        $SQL .= '       SRC.PER_NACHNAME,';
        $SQL .= '       SRC.PER_VORNAME,';
        $SQL .= '       SRC.PER_GEBURTSDATUM,';
        $SQL .= '       SRC.PER_LAN_CODE,';
        $SQL .= '       SRC.PER_EINTRITT,';
        $SQL .= '       SRC.PER_AUSTRITT,';
        $SQL .= '       SRC.PER_TAETIGKEIT,';
        $SQL .= '       SRC.PER_BEREICH,';
        $SQL .= '       SRC.PER_FIL_ID,';
        $SQL .= '       SRC.PER_KOSTENSTELLE,';
        $SQL .= '       SRC.PER_CARDNUMBER,';
        $SQL .= '       SRC.PER_PLZ,';
        $SQL .= '       SRC.PER_ORT,';
        $SQL .= '       SRC.PER_STRASSE';
        $SQL .= '     )';

        try {

            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('PER'));
        } catch (awisException $e) {
            var_dump($e->getMessage());
            var_dump($e->getSQL());
            die;
        }
    }

    /**
     * Initialisiert das FEBFEB-Array f�r die untergeordneten Ebenen
     */
    private function initFEBFEB()
    {
        $SQL = 'select * from FILIALEBENEN ';
        $SQL .= ' where FEB_GUELTIGAB <= sysdate and FEB_GUELTIGBIS >= sysdate ';
        $SQL .= ' and FEB_ASESJOB_SUBSTR is not null ';

        $rsFEB = $this->_DB->RecordSetOeffnen($SQL);

        while (!$rsFEB->EOF()) {

            $FEB_FEB = ['FEB_KEY' => $rsFEB->FeldInhalt('FEB_KEY'),
                        'FEB_EBENE' => $rsFEB->FeldInhalt('FEB_EBENE'),
                        'FEB_ASESJOB_SUBSTR' => $rsFEB->FeldInhalt('FEB_ASESJOB_SUBSTR')
            ];

            $this->_FEB_FEB[$rsFEB->FeldInhalt('FEB_BEZEICHNUNG')] = $FEB_FEB;

            $rsFEB->DSWeiter();
        }
    }

    /**
     * In manchen Feldern steht die Kostenstelle mit in der Personalnummer. Diese muss extrahiert werden
     *
     * @param $Wert
     *
     * @return string
     */
    private function PersNrExtrahieren($Wert)
    {
        $Wert = $this->WertDecoden($Wert);

        if (strpos($Wert, ',') !== false) {
            $Wert = trim(explode(',', $Wert)[1]);
        }

        return $Wert;
    }

    /**
     * Decoded den Wert in ein f�r AWIS lesbares Format
     *
     * @param $Wert
     *
     * @return string
     */
    private function WertDecoden($Wert)
    {
        return utf8_decode(html_entity_decode($Wert));
    }

    /**
     * Schreibt einen Logeintrag
     *
     * @param     $Msg
     * @param int $Lvl
     */
    private function _Log($Msg, $Lvl = 1)
    {
        if ($this->_DebugLevel >= $Lvl) {
            echo date('c') . ' ' . $Msg . PHP_EOL;
        }
    }

}