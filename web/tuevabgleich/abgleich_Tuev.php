<?php
require_once('abgleich_funktionen.inc');
/**
 * Seite fuer Tuev Datensaetze Abgleich
 *
 * @author    Tobias Schaeffler
 * @copyright ATU Auto Teile Unger
 * @version   201610061342
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TAG','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','PDFErzeugen');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht23010 = $AWISBenutzer->HatDasRecht(23010);
	if($Recht23010 == 0) {
	    $Form->Fehler_KeineRechte();
	}
    
    $AWIS_KEYs1 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TGD_KEY)
    $AWIS_KEYs2 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TKD_KEY)    
    $AWIS_KEYs1[0] = (isset($_GET['TGD_KEY'])? $_GET['TGD_KEY']: 0);    // wenn �ber GET (direkter Klick vom Benutzer auf href)
    $AWIS_KEYs2[0] = 0;
    if (isset($_GET['TGD_KEYs']))        {                                 // wenn �ber GET ein ganzes quasi-Array mitkommt (z.B. beim umsortieren der Zuordnungen)

        $AWIS_KEYs1 = explode(awis_abgleich_funktionen::_UniqueDelimiter, $_GET['TGD_KEYs']);
    }
    
    if ($AWIS_KEYs1[0] <= 0)     {                                        // wenn 0-Index leer,

        foreach ($_POST as $key => $value)                              // dann alle POST-�bergaben durchlaufen
        {
            if (strpos($key, 'txtTGD_CHK_') === 0)    {                  // wenn an 1. Position (0. Index), 'txtTGD_CHK_' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merken
                if ($AWIS_KEYs1[0] <= 0)      {                          // wenn 0. Index vom Array noch unbelegt, dann hier hin schreiben
                    $AWIS_KEYs1[0] = $value;
                } else   {                                               // ansonsten hinzuf�gen
                    $AWIS_KEYs1[] = $value;
                }
            }
        }
    }

    foreach ($_POST as $key => $value)                              // alle POST-�bergaben durchlaufen
    {
        if (strpos($key, 'txtTKD_CHK_') === 0)  {                    // wenn an 1. Position (0. Index), 'txtTKD_CHK_' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merken
            if ($AWIS_KEYs2[0] <= 0)      {                          // wenn 0. Index vom Array noch unbelegt, dann hier hin schreiben
                $AWIS_KEYs2[0] = $value;
            } else   {                                               // ansonsten hinzuf�gen
                $AWIS_KEYs2[] = $value;
            }            
        }
    }
        
    $TAG = new awis_abgleich_funktionen();
    $AnsichtSuche = false;
    $AnsichtAuswahl = false;
    $AnsichtZuordnung = false;
    
    $Gespeichert = false;                   // Flag, ob gespeichert wurde (=> wichtig, damit die Parameter behalten werden => siehe $SuchParam['SPEICHERN'])

    if (isset($_POST['cmdSuche_x'])) {
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdDSWeiter_x']) and $AWIS_KEYs1[0] > 0) {
        $AnsichtZuordnung = true;
    }
    elseif (isset($_POST['cmdDSZurueck_x'])) {
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdMail_x'])) {
        $MailSenden = true;
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdSpeichern_B'])
         or isset($_POST['cmdSpeichern_K'])
         or isset($_POST['cmdSpeichern_N'])   
         or isset($_POST['cmdSpeichern_S'])
         or isset($_POST['cmdSpeichern_V'])
    	 or isset($_POST['cmdSpeichern_SK'])
    	 or isset($_POST['cmdSpeichern_W'])) {
        $AbgleichArt = 'XXX';       // vorbelegen mit ung�ltig => nicht speichern
        $Bemerkung = '';
        if (isset($_POST['cmdSpeichern_B']) 
        and (count($AWIS_KEYs2) <= 2 
        and count($AWIS_KEYs2) >= 1)      
        and ($AWIS_KEYs2[0] != 0)                               // wenn 1 Kassendatensatz (laut AWIS_KEY2) verf�gbar ist, gegen nicht 0 pr�fen, da das erste Arrayelement so vorbelegt wird
        )       // max. 2 Kassendatens�tze und keine leere Begr�ndung
        {
            $AbgleichArt = 'B';
            $Bemerkung = $_POST['txtTAG_BEMERKUNG_UNTEN'];
        }
        if (isset($_POST['cmdSpeichern_K']) 
        and ($AWIS_KEYs2[0] != 0)                           // wenn 1 Kassendatensatz (laut AWIS_KEY2) verf�gbar ist, gegen nicht 0 pr�fen, da das erste Arrayelement so vorbelegt wird
        and (count($AWIS_KEYs2) == 1))                      // nur ein Kassendatensatz
        {
            $AbgleichArt = 'K';
            $Bemerkung = $_POST['txtTAG_BEMERKUNG_UNTEN'];
        }
        if (isset($_POST['cmdSpeichern_SK'])
        and (count($AWIS_KEYs2) <= 2 
        and count($AWIS_KEYs2) >= 1)      
        and ($AWIS_KEYs2[0] != 0))                         
        {
        	$AbgleichArt = 'S';
        	$Bemerkung = $_POST['txtTAG_BEMERKUNG_UNTEN'];
        }
        
        if (isset($_POST['cmdSpeichern_N']) and ($AWIS_KEYs2[0] == 0) and (!empty($_POST['txtTAG_BEMERKUNG_OBEN'])))   {   // keine leere Begr�ndung
            $AbgleichArt = 'N';
            $Bemerkung = $_POST['txtTAG_BEMERKUNG_OBEN'];
        }
        if (isset($_POST['cmdSpeichern_S']) and ($AWIS_KEYs2[0] == 0) and (!empty($_POST['txtTAG_BEMERKUNG_OBEN'])))   {   // keine leere Begr�ndung
            $AbgleichArt = 'S';
            $Bemerkung = $_POST['txtTAG_BEMERKUNG_OBEN'];
        }
        if (isset($_POST['cmdSpeichern_V']) and ($AWIS_KEYs2[0] == 0) and (!empty($_POST['txtTAG_BEMERKUNG_OBEN'])))   {   // keine leere Begr�ndung
            $AbgleichArt = 'V';
            $Bemerkung = $_POST['txtTAG_BEMERKUNG_OBEN'];
        }
        if (isset($_POST['cmdSpeichern_W']) and ($AWIS_KEYs2[0] == 0) and (!empty($_POST['txtTAG_BEMERKUNG_OBEN'])))   {   // keine leere Begr�ndung
        	$AbgleichArt = 'W';
        	$Bemerkung = $_POST['txtTAG_BEMERKUNG_OBEN'];
        }
        

        if ($AbgleichArt != 'XXX')    {      // auf ungleich 'XXX' pr�fen, da $AbgleichArt so vorbelegt wurden
            foreach ($AWIS_KEYs1 as $key => $TGDvalue)
            {
                foreach ($AWIS_KEYs2 as $key => $TKDvalue)
                {
                    $TAG->AbgleichSpeichern($TGDvalue, $TKDvalue, $AbgleichArt, $Bemerkung, $AWISBenutzer->BenutzerName(1));
                }
            }
            $Form->Hinweistext($AWISSprachKonserven['TAG']['DatenGespeichert'], 1);
            $SQL =  'select * from tuevgesellschaftendaten ' .
            		'where  tgd_abgeglichen = 0';

            if($_POST['txtSFilId'] != '' and $_POST['txtSFilId'] != '::bitte w�hlen::') {
            	$SQL .=' and tgd_fil_id = :var_N0_fil_id';
            	$DB->SetzeBindevariable('TGD', 'var_N0_fil_id', $_POST['txtSFilId'] , awisDatenbank::VAR_TYP_GANZEZAHL);
            }
            if ($_POST['txtBelegNr'] != '' and $_POST['txtBelegNr'] != '::bitte w�hlen::') {
            	$SQL .=' and tgd_belegnr =  :var_T_belegnr';
            	$DB->SetzeBindevariable('TGD', 'var_T_belegnr', $_POST['txtBelegNr'] , awisDatenbank::VAR_TYP_TEXT);
            	$SQL .=' and tgd_belegdatum =  :var_DU_belegDatum';
            	$DB->SetzeBindevariable('TGD', 'var_DU_belegDatum', $_POST['txtBelegDatum'] , awisDatenbank::VAR_TYP_DATUMZEIT);
            }
			            		
            $rsSuche = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('TGD'));
            if ($rsSuche->AnzahlDatensaetze() > 0) {
            	$AnsichtAuswahl = true;
            } else {
            	$AnsichtSuche = true;
            }
            $Gespeichert = true;
        } else {
            $Form->Hinweistext($AWISSprachKonserven['TAG']['FalscheAnzahlDaten'].' '.$AWISSprachKonserven['TAG']['oder'].' '.$AWISSprachKonserven['TAG']['KeineBemerkung'], 1);
            $AnsichtZuordnung = true;
        }
    } else {
        if (isset($_REQUEST['txtANSICHT']))   {      // wegen Block
        	switch ($_REQUEST['txtANSICHT'])
            {
                case 'Auswahl':     $AnsichtAuswahl = true;     break;
                case 'Zuordnung':   $AnsichtZuordnung = true;   break;
                default: break;
            }
        }
        elseif ($AWIS_KEYs1[0] > 0) {
        	$AnsichtZuordnung = true;
        } else {
        	$AnsichtSuche = true;
        }
    }
    
$Form->DebugAusgabe(1, 'AnsichtSuche: ' . $AnsichtSuche, 'AnsichtAuswahl: ' . $AnsichtAuswahl, 'AnsichtZuordnung: ' . $AnsichtZuordnung);        
$Form->DebugAusgabe(1, $AWIS_KEYs1, $AWIS_KEYs2);
$Form->DebugAusgabe(1, $_REQUEST);    

	/**********************************************
	* Benutzerparameter
	***********************************************/
    $SuchParam = array();
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_TGD')) < 8)) {
        $SuchParam['BelegNr'] = '';
        $SuchParam['BelegDatum'] = '';
        $SuchParam['DatumVon'] = '';
        $SuchParam['DatumBis'] = '';
        $SuchParam['FilId'] = '';
        $SuchParam['TuevNr'] = '';
        $SuchParam['TuevArtKey'] = 0;
        $SuchParam['Kfz'] = '';
        $SuchParam['Name'] = '';
        $SuchParam['PreisVon'] = '';
        $SuchParam['PreisBis'] = '';
        $SuchParam['PassendeKfz'] = 'off';
        $SuchParam['Nachverfolgung'] = 'off';
        $SuchParam['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_TGD", serialize($SuchParam));
	}
    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_TGD'));
	if ($AnsichtAuswahl and !isset($_GET['TGDSort'])) {
            if(isset($_POST['sucTAG_BELEGNR']) and $_POST['sucTAG_BELEGNR'] != '' and $_POST['sucTAG_BELEGNR'] != '::bitte w�hlen::') {
				$Daten = explode('|',$_POST['sucTAG_BELEGNR']);
			} else {
				$Daten[0] = '';
				$Daten[1] = '';
			}
		$SuchParam['BelegNr'] = (isset($_POST['sucTAG_BELEGNR'])? $Daten[0]: $SuchParam['BelegNr']);
		$SuchParam['BelegDatum'] = (isset($_POST['sucTAG_BELEGNR'])? $Daten[1]: $SuchParam['BelegDatum']);
		$SuchParam['BelegDatumVon'] = (isset($_POST['sucTAG_BELEGDATUMVON'])? $_POST['sucTAG_BELEGDATUMVON']: $SuchParam['BelegDatumVon']);
        $SuchParam['BelegDatumBis'] = (isset($_POST['sucTAG_BELEGDATUMBIS'])? $_POST['sucTAG_BELEGDATUMBIS']: $SuchParam['BelegDatumBis']);
        $SuchParam['DatumVon'] = (isset($_POST['sucTAG_DATUMVON'])? $_POST['sucTAG_DATUMVON']: $SuchParam['DatumVon']);
        $SuchParam['DatumBis'] = (isset($_POST['sucTAG_DATUMBIS'])? $_POST['sucTAG_DATUMBIS']: $SuchParam['DatumBis']);
        $SuchParam['FilId'] = (isset($_POST['sucTAG_FILNR'])? $_POST['sucTAG_FILNR']: $SuchParam['FilId']);
        $SuchParam['TuevNr'] = (isset($_POST['sucTAG_TUEVNR'])? $_POST['sucTAG_TUEVNR']: $SuchParam['TuevNr']);
        $SuchParam['TuevArtKey'] = (isset($_POST['sucTAG_TUEVARTKEY'])? $_POST['sucTAG_TUEVARTKEY']: $SuchParam['TuevArtKey']);
        $SuchParam['Kfz'] = (isset($_POST['sucTAG_KFZKENNZ'])? $_POST['sucTAG_KFZKENNZ']: $SuchParam['Kfz']);
        $SuchParam['Name'] = (isset($_POST['sucTAG_NAME'])? $_POST['sucTAG_NAME']: $SuchParam['Name']);
        $SuchParam['PreisVon'] = (isset($_POST['sucTAG_PREISVON'])? $_POST['sucTAG_PREISVON']: $SuchParam['PreisVon']);
        $SuchParam['PreisBis'] = (isset($_POST['sucTAG_PREISBIS'])? $_POST['sucTAG_PREISBIS']: $SuchParam['PreisBis']);
        If(!isset($_POST['cmdDSZurueck_x']) and !$Gespeichert) {
            $SuchParam['PassendeKfz'] = (isset($_POST['sucTAG_PASSENDEKFZ'])?'on':'off');
        }
        $SuchParam['Nachverfolgung'] = (isset($_POST['sucTAG_NACHVERFOLGUNG'])? 'on': $SuchParam['Nachverfolgung']);
        If(!isset($_POST['cmdDSZurueck_x']) and !$Gespeichert) {
        	$SuchParam['SPEICHERN'] = (isset($_POST['sucTAG_AUSWAHLSPEICHERN'])? 'on': 'off');
        }
        $AWISBenutzer->ParameterSchreiben("Formular_TGD", serialize($SuchParam));	        
	}
	/**********************************************
	* Formular
	***********************************************/
    $Form->SchreibeHTMLCode('<form name=frmKasse action=./abgleich_Main.php?cmdAktion=Tuev method=POST enctype="multipart/form-data">');
    $Form->Formular_Start();
    $Form->DebugAusgabe(1, $SuchParam);

    if ($AnsichtSuche or $AnsichtAuswahl) {
        $TAG->Label_Filter($AnsichtSuche);
        $TAG->Feld_SelectBelegNr($SuchParam, $AnsichtSuche, false, false);
        $TAG->Feld_SelectFilId($SuchParam, $AnsichtSuche, 1);
        $TAG->Feld_CheckPassendeKfz($SuchParam, $AnsichtSuche);
        $TAG->Feld_AuswahlSpeichern($SuchParam, $AnsichtSuche);
    }
    
    if ($AnsichtAuswahl or $AnsichtZuordnung) {
        $Form->Trennzeile('L');
        $Sort= '';
        if(isset($_GET['TGDSort'])) {
        	$Sort = $_GET['TGDSort'];
        }
        elseif(isset($_REQUEST['txtTGDSort'])) {
        	$Sort = $_REQUEST['txtTGDSort'];
        }
		// Infozeile zusammenbauen
		$Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Tuev accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['TAG']['ZurueckSuche']);
		$Form->InfoZeile($Felder, '');
        
        if ($AnsichtAuswahl) {
            $SQL = 'select tgd.*, tue.tue_bez, fil.fil_bez, tua.tua_bez, ' .
                   'case when tgd.tgd_duplikat = 1 ' .
                       'then \'X\' else null end as duplikat, ' .
                   'row_number() OVER (';            
            if ($Sort != '') {		// wenn GET-Sort, dann nach diesen Feld sortieren
                $SQL .= ' order by ' . str_replace('~',' DESC ', $Sort);
            } else {		// sonst pauschal nach Pr�fdatum
                $SQL .= ' order by tgd_pruefdatum';
            }
            $SQL .= ') AS ZeilenNr ' . 
                    'from tuevgesellschaftendaten tgd ' .
                    'left join tuevorganisationen tue on tgd.tgd_tuevnr = tue.tue_id ' .
                    'left join filialen fil on tgd.tgd_fil_id = fil.fil_id ' .
                    'left join tuevarten tua on tua.tua_id = tgd.tgd_tuevart ' .
                    'left join tuevabgleich tag on tag.tag_tgd_key = tgd.tgd_key';
                    'and tag.tag_taa_key = (select taa_key from tuevabgleichart ' .
                    'where taa_kennung = \'V\') ' .
                    'and tag.tag_tgd_key not in (select sub_tag.tag_tgd_key from tuevabgleich sub_tag ' .            // diese Bedingung schmei�t alle mitgenommenen (Abgleichart = N) wieder raus, die nachtr�glich noch anders abgeglichen wurden
                                               'where sub_tag.tag_taa_key in (select taa_key from tuevabgleichart ' .
                                                                             'where taa_kennung != \'V\'))';
            $Bedingung = ') or (tgd_abgeglichen = 0';                  // dann verODERn, geklammert 
            $Bedingung .= $TAG->ErstelleTGDBedingung($SuchParam);       // mit dem Rest
            if ($Bedingung != '') {
                $SQL .= ' WHERE ' . substr($Bedingung, 4) . ')';
            }
            $MaxDS = 1;
            $ZeilenProSeite=1;
            $Block = 1;
            // Zum Bl�ttern in den Daten
            if (isset($_REQUEST['Block'])) {
                if (! isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x'])) {
                    $Block = $Form->Format('N0', $_REQUEST['Block'], false);
                }
            }
            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
            $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
            $Form->DebugAusgabe(1, $SQL);
            $rsTGD = $DB->RecordSetOeffnen($SQL);
            
            // Spaltenbreiten f�r Listenansicht
            $FeldBreiten = array();
            $FeldBreiten['ChkBox'] = 16;
            $FeldBreiten['BelegNr'] = 140;
            $FeldBreiten['BelegDatum'] = 100;
            $FeldBreiten['Betrag'] = 60;
            $FeldBreiten['TuevNr'] = 60;
            $FeldBreiten['FilNr'] = 70;
            $FeldBreiten['PruefDatum'] = 100;
            $FeldBreiten['Kfz'] = 140;
            $FeldBreiten['Name'] = 180;
            $FeldBreiten['TuevArt'] = 230;
            $FeldBreiten['Nachverfolgung'] = 20;
            $Gesamtbreite = 0;
            if($rsTGD->AnzahlDatensaetze() > 12) {
                $FeldBreiten['Balken'] = 17;
            } else {
                $Gesamtbreite += 3;
            }
            foreach ($FeldBreiten as $value)
            {
                $Gesamtbreite += $value + 2.7;
            }
            
            $Form->ZeileStart();
            // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
            $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
            // �berschrift der Listenansicht mit Sortierungslink: BelegDatum
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort=TGD_PRUEFDATUM'.((isset($_GET['TGDSort']) AND ($_GET['TGDSort']=='TGD_PRUEFDATUM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Datum'], $FeldBreiten['PruefDatum'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort=TGD_KFZKENNZ'.((isset($_GET['TGDSort']) AND ($_GET['TGDSort']=='TGD_KFZKENNZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['KfzKennz'], $FeldBreiten['Kfz'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Nachname
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort=TGD_NACHNAME'.((isset($_GET['TGDSort']) AND ($_GET['TGDSort']=='TGD_NACHNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Name'], $FeldBreiten['Name'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort=TGD_PREIS'.((isset($_GET['TGDSort']) AND ($_GET['TGDSort']=='TGD_PREIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Preis'], $FeldBreiten['Betrag'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: T�V-Art
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort=TGD_TUEVART'.((isset($_GET['TGDSort']) AND ($_GET['TGDSort']=='TGD_TUEVART'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevArt'], $FeldBreiten['TuevArt'], '', $Link);            
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort=TGD_FIL_ID'.((isset($_GET['TGDSort']) AND ($_GET['TGDSort']=='TGD_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['FilId'], $FeldBreiten['FilNr'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Nachverfolgung
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort=TGD_DUPLIKAT'.((isset($_GET['TGDSort']) AND ($_GET['TGDSort']=='TGD_DUPLIKAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $TTT = $AWISSprachKonserven['TAG']['Nachverfolgung'];
            $Form->Erstelle_Liste_Ueberschrift('D', $FeldBreiten['Nachverfolgung'], '', $Link,$TTT);
            if($rsTGD->AnzahlDatensaetze() > 12) {
                $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Balken']);
            }
            $Form->ZeileEnde();
            if($rsTGD->AnzahlDatensaetze() > 12) {
            	$Form->ZeileStart();
                $Form->ScrollBereichStart('scrTuevOffen',250,829,true,false);
            }
            $DS = 0;	// f�r Hintergrundfarbumschaltung
            while(! $rsTGD->EOF())
            {
            	$Form->ZeileStart();
                $Link = ((($Recht23010 & 2) == 2)? './abgleich_Main.php?cmdAktion=Tuev&TGD_KEY=0'.$rsTGD->FeldInhalt('TGD_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:''): '');
                $Form->Erstelle_Checkbox('TGD_CHK_' . $rsTGD->FeldInhalt('TGD_KEY'), '', $FeldBreiten['ChkBox'], true, $rsTGD->FeldInhalt('TGD_KEY'));                
                $TTT = $Form->Format('D', $rsTGD->FeldInhalt('TGD_PRUEFDATUM'));
                $Form->Erstelle_ListenFeld('TGD_PRUEFDATUM', $Form->Format('D', $rsTGD->FeldInhalt('TGD_PRUEFDATUM')), 0, $FeldBreiten['PruefDatum'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTGD->FeldInhalt('TGD_KFZKENNZ') . ' (' . $rsTGD->FeldInhalt('TGD_NACHNAME') . ')';
                $Form->Erstelle_ListenFeld('TGD_KFZKENNZ', $rsTGD->FeldInhalt('TGD_KFZKENNZ'), 0, $FeldBreiten['Kfz'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTGD->FeldInhalt('TGD_NACHNAME') . ' (' . $rsTGD->FeldInhalt('TGD_KFZKENNZ') . ')';
                $Form->Erstelle_ListenFeld('TGD_NACHNAME', $rsTGD->FeldInhalt('TGD_NACHNAME'), 0, $FeldBreiten['Name'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $Form->Format('N2', $rsTGD->FeldInhalt('TGD_PREIS'));
                $Form->Erstelle_ListenFeld('TGD_PREIS', $Form->Format('N2', $rsTGD->FeldInhalt('TGD_PREIS')), 0, $FeldBreiten['Betrag'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                //$TTT = $rsTGD->FeldInhalt('TGD_BELEGNR') . ' (' . $Form->Format('D', $rsTGD->FeldInhalt('TGD_BELEGDATUM')) . ')';
                //$Form->Erstelle_ListenFeld('TGD_BELEGNR', $rsTGD->FeldInhalt('TGD_BELEGNR'), 0, $FeldBreiten['BelegNr'], false, ($DS%2),'', $Link, 'T', 'L', $TTT);
                //$TTT = $rsTGD->FeldInhalt('TGD_TUEVNR') . ' - ' . $rsTGD->FeldInhalt('TUE_BEZ');
                //$Form->Erstelle_ListenFeld('TGD_TUEVNR', $rsTGD->FeldInhalt('TGD_TUEVNR'), 0, $FeldBreiten['TuevNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);                             
                //$TTT = $Form->Format('D', $rsTGD->FeldInhalt('TGD_BELEGDATUM')) . ' (' . $rsTGD->FeldInhalt('TGD_BELEGNR') . ')';
                //$Form->Erstelle_ListenFeld('TGD_BELEGDATUM', $Form->Format('D', $rsTGD->FeldInhalt('TGD_BELEGDATUM')), 0, $FeldBreiten['BelegDatum'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTGD->FeldInhalt('TUA_BEZ') . ' (' . $rsTGD->FeldInhalt('TUA_BEZ') . ')';
                $Form->Erstelle_ListenFeld('TGD_TUEVART', $rsTGD->FeldInhalt('TUA_BEZ'), 0, $FeldBreiten['TuevArt'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsTGD->FeldInhalt('TGD_FIL_ID') . ' - ' . $rsTGD->FeldInhalt('FIL_BEZ');
                $Form->Erstelle_ListenFeld('TGD_FIL_ID', $rsTGD->FeldInhalt('TGD_FIL_ID'), 0, $FeldBreiten['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $AWISSprachKonserven['TAG']['Nachverfolgung'];
                $Form->Erstelle_ListenFeld('TGD_DUPLIKAT', $rsTGD->FeldInhalt('DUPLIKAT'), 0, $FeldBreiten['Nachverfolgung']-4, false, ($DS%2), '', '', 'T', 'L', $TTT);
                $Form->ZeileEnde();
                $DS++;
                $rsTGD->DSWeiter();                
            }
            if($rsTGD->AnzahlDatensaetze() > 12) {
            	$Form->ScrollBereichEnde();
                $Form->ZeileEnde();
            }
            $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['TAG']['SummeGesamtDS'].':', $Gesamtbreite-544, 'font-weight:bolder;');
            $Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsTGD->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), 208, 'font-weight:bolder;','','','R');
            $Form->ZeileEnde();
            $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Auswahl&TGDSort='.$Sort;        // f�r N�chste/Vorherige Seite schalten (href im Framwork) (=> mit txt damit immer die gleiche REQUEST-Var ankommt, wegen HiddenFeld)
            $Form->Erstelle_HiddenFeld('ANSICHT', 'Auswahl');                       // f�r direkte Auswahl der Seite
            $Form->Erstelle_HiddenFeld('TGDSort', $Sort);
        }
        elseif ($AnsichtZuordnung) {
        	$SQL =  'SELECT tgd.*, tue.tue_bez, fil.fil_bez, tua.tua_bez ' .
                    'FROM tuevgesellschaftendaten tgd ' .
                    'LEFT JOIN tuevorganisationen tue ON tgd.tgd_tuevnr = tue.tue_id ' .
                    'LEFT JOIN filialen fil ON tgd.tgd_fil_id = fil.fil_id ' .
                    'LEFT JOIN tuevarten tua ON tua.tua_id = tgd.tgd_tuevart ';
                            
			if 	(count($AWIS_KEYs1)==1) {
				$DB->SetzeBindevariable('TGD', 'var_N0_tgd_key', $AWIS_KEYs1[0], awisDatenbank::VAR_TYP_GANZEZAHL);
				$SQL.='WHERE tgd.tgd_key = :var_N0_tgd_key';
			} else {
		        $SQL.='WHERE tgd.tgd_key in (';
        		$i=0;
	           	foreach ($AWIS_KEYs1 as $key => $value)
		        {
	                ++$i;
	            	$DB->SetzeBindevariable('TGD', 'var_N0_tgd_key_'.$i, $value, awisDatenbank::VAR_TYP_GANZEZAHL);
	            	$SQL .=':var_N0_tgd_key_'.$i.', ';                	        	
	            }           
	            $SQL = substr($SQL, 0, -2);
	            $SQL .= ')';
        	}
	        $Form->DebugAusgabe(1, $SQL);        
            $rsTGD = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TGD'));
            
            $AnzahlGewaehlteDatensaetze = $rsTGD->AnzahlDatensaetze();
            $TAG->Label_Abgleich($AnzahlGewaehlteDatensaetze);
            
            $TopErgebnissKennz = array();
            $TopErgebnissFilId = array();
            $TopErgebnissDatum = array();
            $TopErgebnissName = array();
            $TGDMerker = 0;
            
            while(! $rsTGD->EOF())
            {
                $TAG->Felder_AbgleichTGD($rsTGD);
                
                $TGDMerker = $rsTGD->FeldInhalt('TGD_KEY');
                $TopErgebnissKennz[] = $rsTGD->FeldInhalt('TGD_KFZKENNZ');
                $TopErgebnissFilId[] = $rsTGD->FeldInhalt('TGD_FIL_ID');
                $TopErgebnissDatum[] = $rsTGD->FeldInhalt('TGD_PRUEFDATUM');
                $TopErgebnissName[] = $rsTGD->FeldInhalt('TGD_NACHNAME');
                
                $rsTGD->DSWeiter();
                
                if (! $rsTGD->EOF()) {
                    $Form->Trennzeile('O');
                }
            }                

            if ($AnzahlGewaehlteDatensaetze == 1)  {     // nur wenn 1 T�V-Datensatz gew�hlt wurde, k�nnen folgende drei Abgleicharten gew�hlt werden
                $Form->Trennzeile('O');
                $TAG->Feld_Bemerkung(1, $TGDMerker);
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('', awis_abgleich_funktionen::_LabelBreite);
                $Form->Schaltflaeche('submit', 'cmdSpeichern_N', '', '', $AWISSprachKonserven['TAG']['MAAbnahmeNichtErfolgt'], '');
                $Form->Schaltflaeche('submit', 'cmdSpeichern_S', '', '', $AWISSprachKonserven['TAG']['MASonstiges'], '');
                $Form->Schaltflaeche('submit', 'cmdSpeichern_V', '', '', $AWISSprachKonserven['TAG']['MANachverfolgung'], '');
                $Form->Schaltflaeche('submit', 'cmdSpeichern_W', '', '', $AWISSprachKonserven['TAG']['MAKWeiterberechnung'], ''		);
                $Form->ZeileEnde();
                
            }
            
            if ($AnzahlGewaehlteDatensaetze > 2)  {  // zu viele T�V-Datens�tze f�r Abgleich ausgew�hlt

                $Form->ZeileStart();
                $Form->Hinweistext($AWISSprachKonserven['TAG']['ZuVieleDaten'], 1);
                $Form->ZeileEnde();
            }
            else {
            	$Sort= '';
            	if(isset($_GET['TKDSort'])) {
            		$Sort = $_GET['TKDSort'];
            	}
            	elseif(isset($_REQUEST['txtTKDSort'])) {
            		$Sort = $_REQUEST['txtTKDSort'];
            	}
            	if(!empty($TopErgebnissDatum)) {
                	if(count($TopErgebnissDatum)<2) {
                		$SucDatum = $TopErgebnissDatum[0];
                	} else {
                		if($TopErgebnissDatum[0] <= $TopErgebnissDatum[1]) {
                			$SucDatum = $TopErgebnissDatum[0]; 
                		} else {
                			$SucDatum = $TopErgebnissDatum[1];
                		}
                	}                	
                } else {
                	$SucDatum = $_REQUEST['txtPruefDatum'];
                }
                $DB->SetzeBindevariable('TKD', 'var_tkd_datumzeit', $SucDatum, awisDatenbank::VAR_TYP_DATUM);
                
            	$Form->Trennzeile('L');
                $SQL2 = 'SELECT tkd.*,tua_bez, '.
						'case when tkd.tkd_duplikat = 1 ' .
                        'then \'X\' else null end as duplikat ' .
                		'FROM tuevkassendaten tkd';
                $SQL2 .= ' left join tuevarten on tua_id = tkd_tuevart';
                
                if(!empty($TopErgebnissFilId)) {
                	$SQL2 .= ' where tkd_fil_id in(';
                	$SucFilID='';
                	$i=0;
                	foreach ($TopErgebnissFilId as $wert)
                	{
						++$i;
		            	$DB->SetzeBindevariable('TKD', 'var_N0_fil_id_'.$i, $wert, awisDatenbank::VAR_TYP_GANZEZAHL );
		            	$SQL2 .=':var_N0_fil_id_'.$i.', ';                	        	
                		$SucFilID .= ',' . $wert;
                	}
                
                	$SucFilID = substr($SucFilID,1);
                	
		            $SQL2 = substr($SQL2, 0, -2);
		            $SQL2 .= ')';
                } else {
                	$SucFilID = $_REQUEST['txtFilId'];
                	$DB->SetzeBindevariable('TKD', 'var_N0_fil_id', $SucFilID, awisDatenbank::VAR_TYP_GANZEZAHL);
                	$SQL2 .= ' where tkd_fil_id = :var_N0_fil_id';
                }
                
                $SQL2 .= ' and tkd_datumzeit + 30 >= :var_tkd_datumzeit';
                $SQL2 .= ' and tkd_abgeglichen = 0';
                $SQL =  'select sub.*, row_number() OVER (';            
                if ($Sort != '') {		// wenn GET-Sort, dann nach diesen Feld sortieren
                    $SQL .= 'order by ' . str_replace('~',' DESC ', $Sort);
                } else {		// sonst pauschal nach erster Spalte => ergibt die richtige Treffersortierung (generiert in ErstelleTopErgebnissSQL())
                    $SQL .= 'order by tkd_datumzeit';
                }
                $SQL .= ') AS ZeilenNr from (';
                $SQL .= $SQL2;
                $SQL .= ') sub';
                $MaxDS = 1;
                $ZeilenProSeite=1;
                $Block = 1;
                // Zum Bl�ttern in den Daten
                if (isset($_REQUEST['Block'])) {
                    $Block = $Form->Format('N0', $_REQUEST['Block'], false);
                }
                $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
                $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
                $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('TKD', false));
                $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
                $Form->DebugAusgabe(1, $SQL);
                $Form->DebugAusgabe(1, $DB->Bindevariablen('TKD', false));
                $rsTKD = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TKD'));

                // Spaltenbreiten f�r Listenansicht
                $FeldBreiten = array();
                $FeldBreiten['ChkBox'] = 20;
                $FeldBreiten['Betrag'] = 60;
                $FeldBreiten['TuevNr'] = 60;
                $FeldBreiten['FilNr'] = 70;
                $FeldBreiten['KasseDatum'] = 90;
                $FeldBreiten['Kfz'] = 140;
                $FeldBreiten['Name'] = 200;
                $FeldBreiten['TuevArt'] = 250;
                $FeldBreiten['Nachverfolgung']=20;
                $Gesamtbreite = 0;
                if($rsTKD->AnzahlDatensaetze() > 12) {
                    $FeldBreiten['Balken'] = 17;
                } else {
                    $Gesamtbreite += 2;
                }
                foreach ($FeldBreiten as $value)
                {
                    $Gesamtbreite += $value + 2.4;
                }

                $AWIS_KEYs1String = implode(awis_abgleich_funktionen::_UniqueDelimiter, $AWIS_KEYs1);       // alle Anzeigen verpacken und an Link mit dran h�ngen
                $Form->ZeileStart();
                // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
                $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
                // �berschrift der Listenansicht mit Sortierungslink: Kassendatum
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_DATUMZEIT'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_DATUMZEIT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Datum'], $FeldBreiten['KasseDatum'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_KFZKENNZ'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_KFZKENNZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['KfzKennz'], $FeldBreiten['Kfz'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: Name
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_NAME'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_NAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Name'], $FeldBreiten['Name'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_BETRAG'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_BETRAG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['Preis'], $FeldBreiten['Betrag'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: T�V-Art
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_TUEVART'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_TUEVART'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevArt'], $FeldBreiten['TuevArt'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: FilNr
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_FIL_ID'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['FilId'], $FeldBreiten['FilNr'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: T�V-DUPLIKAT
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_DUPLIKAT'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_DUPLIKAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift('D', $FeldBreiten['Nachverfolgung'], '', $Link);
                if($rsTKD->AnzahlDatensaetze() > 12) {
                    $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Balken']);
                }
                $Form->ZeileEnde();
                if($rsTKD->AnzahlDatensaetze() > 12) {
                    $Form->ZeileStart();
                    $Form->ScrollBereichStart('scrKasse',250,864,true,false);
                }
                $DS = 0;	// f�r Hintergrundfarbumschaltung
                while(! $rsTKD->EOF())
                {
                    $Form->ZeileStart();

                    $Form->Erstelle_Checkbox('TKD_CHK_' . $rsTKD->FeldInhalt('TKD_KEY'), '', $FeldBreiten['ChkBox'], true, $rsTKD->FeldInhalt('TKD_KEY'));
                    $TTT = $Form->Format('DU', $rsTKD->FeldInhalt('TKD_DATUMZEIT'));
                    $Form->Erstelle_ListenFeld('TKD_DATUMZEIT', $Form->Format('D', $rsTKD->FeldInhalt('TKD_DATUMZEIT')), 0, $FeldBreiten['KasseDatum'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $rsTKD->FeldInhalt('TKD_KFZKENNZ') . ' (' . $rsTKD->FeldInhalt('TKD_NAME') . ')';
                    $Form->Erstelle_ListenFeld('TKD_KFZKENNZ', $rsTKD->FeldInhalt('TKD_KFZKENNZ'), 0, $FeldBreiten['Kfz'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $rsTKD->FeldInhalt('TKD_NAME') . ' (' . $rsTKD->FeldInhalt('TKD_KFZKENNZ') . ')';
                    $Form->Erstelle_ListenFeld('TKD_NAME', $rsTKD->FeldInhalt('TKD_NAME'), 0, $FeldBreiten['Name'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $Form->Format('N2', $rsTKD->FeldInhalt('TKD_BETRAG'));
                    $Form->Erstelle_ListenFeld('TKD_PREIS', $Form->Format('N2', $rsTKD->FeldInhalt('TKD_BETRAG')), 0, $FeldBreiten['Betrag'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $rsTKD->FeldInhalt('TKD_TUEVART') . ' (' . $rsTKD->FeldInhalt('TUA_BEZ') . ')';
                    $Form->Erstelle_ListenFeld('TKD_TUEVART',$rsTKD->FeldInhalt('TUA_BEZ'), 0, $FeldBreiten['TuevArt']-4, false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $rsTKD->FeldInhalt('TKD_FIL_ID') . ' - ' . $rsTKD->FeldInhalt('FIL_BEZ');
                    $Form->Erstelle_ListenFeld('TKD_FIL_ID', $rsTKD->FeldInhalt('TKD_FIL_ID'), 0, $FeldBreiten['FilNr'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $rsTKD->FeldInhalt('DUPLIKAT');
                    $Form->Erstelle_ListenFeld('TKD_DUPLIKAT',$rsTKD->FeldInhalt('DUPLIKAT'), 0, $FeldBreiten['Nachverfolgung'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $Form->ZeileEnde();
                    $DS++;
                    $rsTKD->DSWeiter();                
                }
                if($rsTKD->AnzahlDatensaetze() > 12) {
                    $Form->ScrollBereichEnde();
                    $Form->ZeileEnde();
                }
                $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['TAG']['SummeGesamtDS'].':', $Gesamtbreite-349, 'font-weight:bolder;');
                $Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsTKD->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), 262, 'font-weight:bolder;','','','R');
                $Form->ZeileEnde();           
                foreach ($AWIS_KEYs1 as $key => $value)
                {
                    $Form->Erstelle_HiddenFeld('TGD_CHK_' . $value, $value);    // AWIS_KEYs1 f�r n�chste Seite merken
                } 
          
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&txtFilId='.$SucFilID.'&TKDSort='.$Sort;      // f�r N�chste/Vorherige Seite schalten (href im Framwork) (=> mit txt damit immer die gleiche REQUEST-Var ankommt, wegen HiddenFeld)
                $Form->Erstelle_HiddenFeld('ANSICHT', 'Zuordnung');
                $Form->Erstelle_HiddenFeld('FilId', $SucFilID);                       // f�r direkte Auswahl der Seite
                $Form->Erstelle_HiddenFeld('BelegNr', $SuchParam['BelegNr']);
                $Form->Erstelle_HiddenFeld('BelegDatum', $SuchParam['BelegDatum']);
                $Form->Erstelle_HiddenFeld('SFilId', $SuchParam['FilId']);
                $Form->Erstelle_HiddenFeld('TKDSort', $Sort);

                $Form->Trennzeile('L');

                //$TAG->Feld_SelectAbgleicharten();
                $TAG->Feld_Bemerkung();
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('', awis_abgleich_funktionen::_LabelBreite);            
                $Form->Schaltflaeche('submit', 'cmdSpeichern_B', '', '', $AWISSprachKonserven['TAG']['MABetragsdifferenz'], '');
                if ($AnzahlGewaehlteDatensaetze == 1)   {    // Kennzeichen falsch, nur wenn ein T�V-Datensatz ausgew�hlt wurde

                    $Form->Schaltflaeche('submit', 'cmdSpeichern_K', '', '', $AWISSprachKonserven['TAG']['MAKennzeichenFalsch'], '');
                }
                $Form->Schaltflaeche('submit', 'cmdSpeichern_SK', '', '', $AWISSprachKonserven['TAG']['MASonstiges'], '');
                $Form->ZeileEnde();
            }
        }
    }

	$Form->Formular_Ende();	    
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht23010&1) == 1)
    and ($AnsichtSuche)) {
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}
	if ((($Recht23010&2) == 2)
    and ($AnsichtAuswahl)) {
        $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'N');
    }
    // Mail an Filialen schicken
    if ((($Recht23010&2) == 2)
    and ($AnsichtAuswahl)) {
        $SuchParam['SuchenInKey'] = awis_abgleich_funktionen::_SuchenInTGD;             // Schalter auf TGD-Tabelle setzen
        $SuchParamString = base64_encode($TAG->ErstellePDFBedingung($SuchParam));       // Parameter-Array verschl�sseln und in String f�r GET-�bergabe konvertieren
        // Mail-Button => �ber neues Fenster ausf�hren, Da beim Speichern des PDFs das Stylsheet verloren geht. Datei "abgleich_mail.php" triggert das PDF-Erstellen und Mail versenden
        $Form->Schaltflaeche('href', 'cmdMail', 'abgleich_mail.php?ID='.$SuchParamString, '/bilder/cmd_mail.png', $AWISSprachKonserven['TAG']['MailSenden'], 'M', '', 27, 27, '_blank');
    }
	if ((($Recht23010&2) == 2)
    and ($AnsichtAuswahl)) {
        $SuchParam['SuchenInKey'] = awis_abgleich_funktionen::_SuchenInTGD;             // Schalter auf TGD-Tabelle setzen
	    $LinkPDF = '/berichte/drucken.php?XRE=35&ID='.base64_encode($TAG->ErstellePDFBedingung($SuchParam));
	    $Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
	}        

    if ((($Recht23010&2) == 2) 
    and ($AnsichtZuordnung)) {
        $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'N');
    }
    
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
    
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular) {
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	} else {
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular) {
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	} else {
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>