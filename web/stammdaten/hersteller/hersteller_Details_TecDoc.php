<?php
global $HER;
try {
     //********************************************************
    // Parameter ?
    //********************************************************
    if (isset($_GET['HEI_KEY'])) {
        $HER->AWIS_KEY2 = $_GET['HEI_KEY'];
    } elseif (isset($_POST['txtHEI_KEY'])) {
        $HER->AWIS_KEY2 = $_POST['txtHEI_KEY'];
    } else {
        $HER->AWIS_KEY2 = '';
    }

    if (!$HER->AWIS_KEY1 > 0) {
        if (!isset($Param['HEI_KEY'])) {
            $Param['HEI_KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = '';
        }

        if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
            $Param['HEI_KEY'] = 0;
        }
    }
    if(isset($_GET['Del'])){
        $HER->AWIS_KEY2 = '';
    }

    //*********************************************************
    //* Sortierung
    //*********************************************************

    if (!isset($_GET['Sort2'])) {
        if (isset($Param['ORDER2']) AND $Param['ORDER2'] != '') {
            $ORDERBY = ' ORDER BY ' . $Param['ORDER2'];
        } else {
            $ORDERBY = ' ORDER BY HEI_WERT1 asc';
            $Param['ORDER2'] = 'HEI_WERT1 asc';
        }
    } else {
        $Param['ORDER2'] = str_replace('~', ' DESC ', $_GET['Sort2']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDER2'];
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $BindeVariablen = array();

    $SQL = 'SELECT HERSTELLERINFOS.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM HERSTELLERINFOS';
    $SQL .= ' WHERE HEI_ITY_KEY1 = 473 ';
    if($HER->AWIS_KEY2!=''){
        $SQL .= 'AND HEI_KEY = '.$HER->DB->WertSetzen('HEI','Z',$HER->AWIS_KEY2);
    }
    elseif ($HER->AWIS_KEY1 > 0) {
        $SQL .= ' AND HEI_HER_ID = '.$HER->DB->WertSetzen('HEI','N0',$HER->AWIS_KEY1);
    }
    $HER->Form->DebugAusgabe(1, $SQL);

    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($HER->AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $HER->Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $HER->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $HER->DB->ErmittleZeilenAnzahl($SQL, $HER->DB->Bindevariablen('HEI', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $HER->DB->WertSetzen('HEI', 'N0', $StartZeile) . ' AND  ZeilenNr<' . $HER->DB->WertSetzen('HEI', 'N0',
                ($StartZeile + $ZeilenProSeite));
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $rsHER = $HER->DB->RecordsetOeffnen($SQL, $HER->DB->Bindevariablen('HEI', true));

    $HER->Form->DebugAusgabe(1, $HER->DB->LetzterSQL(), $Param, $_GET, $_POST, $HER->AWIS_KEY1);


    //********************************************************
    // Daten anzeigen
    //********************************************************
    echo '<form name=frmhersteller  method=POST>';
    $DetailAnsicht = false;

    if ($HER->AWIS_KEY2=='')                        // Liste anzeigen
    {

        $SpaltenBreite = array();
        $SpaltenBreite['HER_TECDOCNR'] = 100;
        $SpaltenBreite['HER_TECDOCBEZ'] = 400;

        $HER->Form->ZeileStart();
        if(($HER->Recht602&4)==4){
            $Icons = array();
            $Icons[] = array('new','./hersteller_Main.php?cmdAktion=Details&Seite=TecDoc&HER_ID='.$HER->AWIS_KEY1.'&HEI_KEY=-1');
            $HER->Form->Erstelle_ListeIcons($Icons,19,-2);

        }
        $Link = './hersteller_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort2=HEI_WERT1' . ((isset($_GET['Sort2']) AND ($_GET['Sort2'] == 'HEI_WERT1'))?'~':'');
        $HER->Form->Erstelle_Liste_Ueberschrift($HER->AWISSprachKonserven['HER']['HER_TECDOCNR'], $SpaltenBreite['HER_TECDOCNR'], '', $Link);
        $Link = './hersteller_Main.php?cmdAktion=Details' . (isset($_GET['Block2'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort2=HEI_WERT2' . ((isset($_GET['Sort2']) AND ($_GET['Sort2'] == 'HEI_WERT2'))?'~':'');
        $HER->Form->Erstelle_Liste_Ueberschrift($HER->AWISSprachKonserven['HER']['HER_TECDOCBEZ'], $SpaltenBreite['HER_TECDOCBEZ'], '', $Link);
        $HER->Form->ZeileEnde();

        $DS = 0;
        while (!$rsHER->EOF()) {
            $HER->Form->ZeileStart();
            if(($HER->Recht602&8)==8){
               $HER->Form->Erstelle_ListenLoeschPopUp('./hersteller_Main.php?Del=true&HER_ID='.$HER->AWIS_KEY1.'&cmdAktion=Details&Seite=TecDoc&HEI_KEY='.$rsHER->FeldInhalt('HEI_KEY'),$rsHER->FeldInhalt('HEI_KEY'),19,'Wirklich l�schen?',$rsHER->FeldInhalt('HEI_WERT1'),($DS%2));
            }
            $Link = './hersteller_Main.php?cmdAktion=Details&HER_ID='.$HER->AWIS_KEY1.'&HEI_KEY=' . $rsHER->FeldInhalt('HEI_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $HER->Form->Erstelle_ListenFeld('HER_TECDOCNR', $rsHER->FeldInhalt('HEI_WERT1'), 0, $SpaltenBreite['HER_TECDOCNR'], false, ($DS % 2), '', $Link, 'T');
            $HER->Form->Erstelle_ListenFeld('HER_TECDOCBEZ', $rsHER->FeldInhalt('HEI_WERT2'), 0, $SpaltenBreite['HER_TECDOCBEZ'], false, ($DS % 2), '', '', 'T', 'L');
            $HER->Form->ZeileEnde();

            $rsHER->DSWeiter();
            $DS++;
        }

        $Link = './hersteller_Main.php?cmdAktion=Details';
        $HER->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else {//Ein DS
        $DetailAnsicht = true;
        $HER->AWIS_KEY2 = $rsHER->FeldInhalt('HEI_KEY');
        $Aendern = (($HER->Recht602&2)==2);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./hersteller_Main.php?cmdAktion=Details&HER_ID=$HER->AWIS_KEY1&Seite=TecDoc&Liste2=True accesskey=T title='" . $HER->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsHER->FeldInhalt('HEI_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsHER->FeldInhalt('HEI_USERDAT'));
        $HER->Form->InfoZeile($Felder, '');

        $HER->Form->ZeileStart();
        $HER->Form->Erstelle_TextLabel($HER->AWISSprachKonserven['HER']['HER_TECDOCNR'] . ':', 170);
        $HER->Form->Erstelle_TextFeld('!HER_TECDOCNR', $rsHER->FeldInhalt('HEI_WERT1'), 10, 600, $Aendern, '', '', '', 'T');
        $HER->Form->ZeileEnde();

        $HER->Form->ZeileStart();
        $HER->Form->Erstelle_TextLabel($HER->AWISSprachKonserven['HER']['HER_TECDOCBEZ'] . ':', 170);
        $HER->Form->Erstelle_TextFeld('!HER_TECDOCBEZ', $rsHER->FeldInhalt('HEI_WERT2'), 10, 600, $Aendern, '', '', '', 'T');
        $HER->Form->ZeileEnde();

    }
} catch (awisException $ex) {
    if ($HER->Form instanceof awisFormular) {
        $HER->Form->DebugAusgabe(1, $ex->getSQL());
        $HER->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $HER->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($HER->Form instanceof awisFormular) {
        $HER->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>