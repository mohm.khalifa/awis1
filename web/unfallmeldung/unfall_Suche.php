<?php
// require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('UNF','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht21010 = $AWISBenutzer->HatDasRecht(21010);
	$Recht21020 = $AWISBenutzer->HatDasRecht(21020);
	if($Recht21010 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
    
	
	/**********************************************
	* Benutzerparameter
	***********************************************/	
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_UNF')) < 8))
	{
        $SuchParam['SUCH_UNF_BereichKey'] = '';
        $SuchParam['SUCH_UNF_FilNr'] = '';
        $SuchParam['SUCH_UNF_PersNr'] = '';
        $SuchParam['SUCH_UNF_Name'] = '';
        $SuchParam['SUCH_UNF_ArtKey'] = 0;
        $SuchParam['SUCH_UNF_DatumVon'] = '';
        $SuchParam['SUCH_UNF_DatumBis'] = '';
        $SuchParam['SUCH_UNF_FILABGESCHL']= 'off';
        $SuchParam['SUCH_UNF_BESCHAEFTIGUNG']= '';
        $SuchParam['SUCH_UNF_ORT']= '';
        $SuchParam['SUCH_UNF_URSACHE']= '';
        $SuchParam['SUCH_UNF_URSACHESPEZ']= '';
        $SuchParam['SUCH_UNF_AKTENZEICHEN']= '';
        $SuchParam['SUCH_UNF_BAGATELL']= '';
        $SuchParam['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_UNF", serialize($SuchParam));
	}
    else
    {
	    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_UNF'));
       	if (! isset($SuchParam['SPEICHERN']))
    	{
    		$SuchParam['SPEICHERN'] = 'off';
            $SuchParam['SUCH_UNF_BereichKey'] = '';
            $SuchParam['SUCH_UNF_FilNr'] = '';
            $SuchParam['SUCH_UNF_PersNr'] = '';
            $SuchParam['SUCH_UNF_Name'] = '';
            $SuchParam['SUCH_UNF_ArtKey'] = 0;
            $SuchParam['SUCH_UNF_DatumVon'] = '';
            $SuchParam['SUCH_UNF_DatumBis'] = '';
            $SuchParam['SUCH_UNF_FILABGESCHL']= 'off';
            $SuchParam['SUCH_UNF_BESCHAEFTIGUNG']= '';
            $SuchParam['SUCH_UNF_ORT']= '';
            $SuchParam['SUCH_UNF_URSACHE']= '';
            $SuchParam['SUCH_UNF_URSACHESPEZ']= '';
            $SuchParam['SUCH_UNF_AKTENZEICHEN']= '';            
            $SuchParam['SUCH_UNF_BAGATELL']= '';           
            $SuchParam['SPEICHERN']= 'off';
    	    $AWISBenutzer->ParameterSchreiben("Formular_UNF", serialize($SuchParam));    		
    	}
	}    
    
$Form->DebugAusgabe(1, $SuchParam);
    
    $Form->SchreibeHTMLCode('<form name=frmUnfallDetails action=./unfall_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();
    
    // Zeile: Beschriftungs und Suchfeld: Unfallbereich.
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
	{
		$Form->ZeileStart();
	    $SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' . 
	            'FROM unfallmeldunginfotypen ' .
	            'WHERE uft_bereichid  = 1 ' .       // ID für Unfallbereich        
	            'AND uft_ueberschrift = 0 ' .		// nur Stammdatentypen, keine Stammdatentypen-Überschriften
	            'AND uft_status       = 1 ' .		// nur aktive Datensätze
	            'ORDER BY uft_kennung, uft_information';    	
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEREICH'] . ':', 200);
	    $Form->Erstelle_SelectFeld('*UNF_BEREICHKEY', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_BereichKey']: ''), 150, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	    $AWISCursorPosition = 'sucUNF_BEREICHKEY';
	    $Form->ZeileEnde();
	}
	else
	{
		echo '<input type=hidden name="sucUNF_BEREICHKEY" value="'.$SuchParam['SUCH_UNF_BereichKey'].'">';
		//$AWISCursorPosition = 'sucUNF_BEREICHKEY';
	}
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
	{
		$Form->ZeileStart();
		$SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' .
				'FROM unfallmeldunginfotypen ' .
				'WHERE uft_bereichid  = 2 ' .       // ID für Beschäftigungsart
				'AND uft_ueberschrift = 0 ' .		// nur Stammdatentypen, keine Stammdatentypen-Überschriften
				'AND uft_status       = 1 ' .		// nur aktive Datensätze
				'ORDER BY uft_kennung, uft_information';
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BESCHAEFTIGUNG'] . ':', 200);
		$Form->Erstelle_SelectFeld('*UNF_BESCHAEFTIGUNG', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_BESCHAEFTIGUNG']: ''), 150, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
	}
	else
	{
		echo '<input type=hidden name="sucUNF_BESCHAEFTIGUNG" value="'.$SuchParam['SUCH_UNF_BESCHAEFTIGUNG'].'">';
		//$AWISCursorPosition = 'sucUNF_BEREICHKEY';
	}
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
	{
		$Form->ZeileStart();
		$SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' .
				'FROM unfallmeldunginfotypen ' .
				'WHERE uft_bereichid  = 7 ' .       // ID für Beschäftigungsart
				'AND uft_ueberschrift = 0 ' .		// nur Stammdatentypen, keine Stammdatentypen-Überschriften
				'AND uft_status       = 1 ' .		// nur aktive Datensätze
				'ORDER BY uft_kennung, uft_information';
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_ORT'] . ':', 200);
		$Form->Erstelle_SelectFeld('*UNF_ORT', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_ORT']: ''), '150:150', true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
	}
	else
	{
		echo '<input type=hidden name="sucUNF_ORT" value="'.$SuchParam['SUCH_UNF_ORT'].'">';
		//$AWISCursorPosition = 'sucUNF_BEREICHKEY';
	}
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
	{
		$Form->ZeileStart();
		$SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' .
				'FROM unfallmeldunginfotypen ' .
				'WHERE uft_bereichid  = 8 ' .       // ID für Beschäftigungsart
				'AND uft_ueberschrift = 0 ' .		// nur Stammdatentypen, keine Stammdatentypen-Überschriften
				'AND uft_status       = 1 ' .		// nur aktive Datensätze
				'ORDER BY uft_kennung, uft_information';
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['URSACHE'] . ':', 200);
		$Form->Erstelle_SelectFeld('*UNF_URSACHE', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_URSACHE']: ''), 150, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
	}
	else
	{
		echo '<input type=hidden name="sucUNF_URSACHE" value="'.$SuchParam['SUCH_UNF_URSACHE'].'">';
		//$AWISCursorPosition = 'sucUNF_BEREICHKEY';
	}
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
	{
		$Form->ZeileStart();
		$SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' .
				'FROM unfallmeldunginfotypen ' .
				'WHERE uft_bereichid  = 11 ' .       // ID für Beschäftigungsart
				'AND uft_ueberschrift = 0 ' .		// nur Stammdatentypen, keine Stammdatentypen-Überschriften
				'AND uft_status       = 1 ' .		// nur aktive Datensätze
				'ORDER BY uft_kennung, uft_information';
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['URSACHESPEZ'] . ':', 200);
		$Form->Erstelle_SelectFeld('*UNF_URSACHESPEZ', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_URSACHESPEZ']: ''), 150, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
	}
	else
	{
		echo '<input type=hidden name="sucUNF_URSACHESPEZ" value="'.$SuchParam['SUCH_UNF_URSACHESPEZ'].'">';
		//$AWISCursorPosition = 'sucUNF_BEREICHKEY';
	}


    if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING)) == '')
    {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_VERTRIEBSBEZIRK'] . ':', 200);
        $SQL  ='SELECT';
        $SQL .='     FEB_KEY,';
        $SQL .='     FEB_BEZEICHNUNG';
        $SQL .=' FROM';
        $SQL .='     FILIALEBENEN';
        $SQL .=' WHERE';
        $SQL .='     FEB_GUELTIGAB <= SYSDATE';
        $SQL .='     AND FEB_GUELTIGBIS >= SYSDATE';
        $SQL .='     AND FEB_EBENE = 4';
        $SQL .=' ORDER BY FEB_BEZEICHNUNG ASC';
        $Form->Erstelle_SelectFeld('*UNF_VERTRIEBSBEZIRK', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_VERTRIEBSBEZIRK']: ''),250,true,$SQL, '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();
    }

    // Zeile: Beschriftungs und Suchfeld: FilNr
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_FIL_ID'] . ':', 200);
    if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))<>'')
    {
    	$Form->Erstelle_TextLabel(intval($FilZugriff),200);
    	echo '<input type=hidden name="sucUNF_FILNR" value="'.$FilZugriff.'">';
    }
    else
    {
    	$Form->Erstelle_TextFeld('*UNF_FILNR', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_FilNr']: ''), 6, 150, true, '', '', '', 'T', 'L');
    }
    $Form->ZeileEnde();
    // Zeile: Beschriftungs und Suchfeld: PersNr
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_PERSNR'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_PERSNR', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_PersNr']: ''), 6, 150, true, '', '', '', 'T', 'L');
    $Form->ZeileEnde();	    
    if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))<>'')
    {
    	$AWISCursorPosition = 'sucUNF_PERSNR';
    }
    // Zeile: Beschriftungs und Suchfeld: Name    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_NAME'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_NAME', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_Name']: ''), 30, 150, true, '', '', '', 'T', 'L');
    $Form->ZeileEnde();
    if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
    {
        $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_AKTENZEICHEN'] . ':', 200);
	    $Form->Erstelle_TextFeld('*UNF_AKTENZEICHEN', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_AKTENZEICHEN']: ''), 30, 150, true, '', '', '', 'T', 'L');
	    $Form->ZeileEnde();
    }
    else 
    {
    	echo '<input type=hidden name="sucUNF_AKTENZEICHEN" value="'.$SuchParam['SUCH_UNF_AKTENZEICHEN'].'">';
    }
    // Zeile: Beschriftungs und Suchfeld: Unfallart.
    $Form->ZeileStart();
    $SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' . 
            'FROM unfallmeldunginfotypen ' .
            'WHERE uft_bereichid  = 3 ' .       // ID für Unfallart
            'AND uft_ueberschrift = 0 ' .		// nur Stammdatentypen, keine Stammdatentypen-Überschriften
            'AND uft_status       = 1 ' .		// nur aktive Datensätze
            'ORDER BY uft_kennung, uft_information';    	
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['ART'] . ':', 200);
    $Form->Erstelle_SelectFeld('*UNF_ARTKEY', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_ArtKey']: ''), 150, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
    $Form->ZeileEnde();
    // Zeile: Beschriftungs und Suchauswahlfeld: Datum von
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['VON'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DATUMVON', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_DatumVon']: ''), 13, 170, true, '', '', '', 'D', 'L');
    $Form->ZeileEnde();
    // Zeile: Beschriftungs und Suchauswahlfeld: Datum bis
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['BIS'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DATUMBIS', ($SuchParam['SPEICHERN'] == 'on'? $SuchParam['SUCH_UNF_DatumBis']: ''), 13, 170, true, '', '', '', 'D', 'L');
    $Form->ZeileEnde();
    // Zeile: Beschriftungs und Checkbox: Auswahl speichern
    
    if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
    {
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['FILABGESCHLOSSEN'] . ':', 200);
    	$Form->Erstelle_Checkbox('*UNF_FILABGESCHLOSSEN', ($SuchParam['SPEICHERN'] == 'on'?$SuchParam['SUCH_UNF_FILABGESCHL']: ''), 50, true, 'on');
    	$Form->ZeileEnde();
    	
    	//PG 26.06.2015 --> Bagatellunfalle (Arzt nein) 
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_BAGATELL'] . ':', 200);
    	$Form->Erstelle_Checkbox('*UNF_BAGATELL', ($SuchParam['SPEICHERN'] == 'on'?$SuchParam['SUCH_UNF_BAGATELL']: ''), 50 , true, 'on');
    	$Form->ZeileEnde();
    	 
    }
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['AUSWAHLSPEICHERN'] . ':', 200);
    $Form->Erstelle_Checkbox('*UNF_AUSWAHLSPEICHERN', ($SuchParam['SPEICHERN'] == 'on'? 'on': ''), 50 , true, 'on');
    $Form->ZeileEnde();

	$Form->Formular_Ende();	    
    
	//************************************************************
	//* Schaltflächen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zurück zum Menü
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if(($Recht21010&1) == 1)
	{	
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}    
    
	if (($Recht21020 & 4) != 0)
	{	// wenn Recht zum Anlegen
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>