<?php
/**
 * �bersicht �ber den Import, etc anzeigen
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20091008
 *
 *
 */
require_once('awisFilialen.inc');

global $AWISCursorPosition;
global $SRFunktionen;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$AWISBenutzer = awisBenutzer::Init();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('WTE','%');
	$TextKonserven[]=array('WPO','%');
	$TextKonserven[]=array('FEZ','FEZ_FEB_KEY');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','FehlerNichtGefunden');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht11000=$AWISBenutzer->HatDasRecht(11000);		// Rechte des Mitarbeiters
	if($Recht11000==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	if(isset($_POST['cmdSpeichern_x']))
	{
		include './srpep_speichern.php';
	}


	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./srpep_Main.php?cmdAktion=Uebersicht>");
	$Form->Formular_Start();

	// Auswahl des Gebiets, f�r das geplant werden soll

	$SQL = 'SELECT MAX(XDI_USERDAT) AS LETZTER_IMPORT';
	$SQL .= ' FROM Importprotokoll';
	$SQL .= ' WHERE XDI_BEREICH = \'WTE\'';
	$rsXDI = $DB->RecordSetOeffnen($SQL);

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['WTE']['txt_LetzterImport'].':',180);
	$Form->Erstelle_TextLabel($Form->Format('DU',$rsXDI->FeldInhalt('LETZTER_IMPORT')),200);
	$Form->ZeileEnde();

	$Form->Trennzeile('L');

    $Filialen = [];
    if(($Recht11000&2)!=2){
        // Ermitteln, welche Filialen man sehen darf
        $Filialen = $SRFunktionen->FilialenDesUsers();
    }

	// Neue Version ab 05.01.2011
	$SQL = 'SELECT WTE_FIL_ID, WPO_TERMINVON, SUM(ANZPOS) AS ANZPOS';
	$SQL.= ', feb_bezeichnung';
	$SQL.= ', SUM(ANZGEPLANT) AS ANZGEPLANT, SUM(ANZWA) AS ANZWA, SUM(ANZWAUNGEPLANT) AS ANZWAUNGEPLANT';
	$SQL.= ', SUM(WPO_ZEITMIN) AS WPO_ZEITMIN, SUM(WPO_VK) AS WPO_VK';
	$SQL.= ' FROM (SELECT DATEN.*';
	$SQL.= ' , (SELECT COUNT(*) FROM WERKSTATTTERMINEPOSITIONEN INNER JOIN HILFSWERTE ON WPO_AST_ATUNR = XHW_WERTTXT AND XHW_BEREICH = \'SRPLANUNG\' WHERE WPO_WTE_KEY = WTE_KEY AND WPO_IMPORTSTATUS <> \'X\') AS ANZPOS';
	$SQL.= ' , (SELECT COUNT(*) FROM WERKSTATTTERMINEPOSITIONEN INNER JOIN WERKSTATTTERMINEPLANUNGEN ON WTP_WPO_KEY = WPO_KEY AND WTP_STATUS < 8   INNER JOIN HILFSWERTE ON WPO_AST_ATUNR = XHW_WERTTXT AND XHW_BEREICH = \'SRPLANUNG\' WHERE WPO_WTE_KEY = WTE_KEY AND WPO_IMPORTSTATUS <> \'X\') AS ANZGEPLANT';
	$SQL.= ' , (SELECT COUNT(DISTINCT WPO_WTE_KEY) FROM WERKSTATTTERMINEPOSITIONEN INNER JOIN HILFSWERTE ON WPO_AST_ATUNR = XHW_WERTTXT AND XHW_BEREICH = \'SRPLANUNG\' WHERE WPO_WTE_KEY = WTE_KEY AND WPO_IMPORTSTATUS <> \'X\') AS ANZWA';
	$SQL.= ' , (SELECT COUNT(DISTINCT WPO_WTE_KEY) FROM WERKSTATTTERMINEPOSITIONEN LEFT OUTER JOIN WERKSTATTTERMINEPLANUNGEN ON WTP_WPO_KEY = WPO_KEY AND WTP_STATUS < 8 INNER JOIN HILFSWERTE ON WPO_AST_ATUNR = XHW_WERTTXT AND XHW_BEREICH = \'SRPLANUNG\' WHERE WPO_WTE_KEY = WTE_KEY AND WPO_IMPORTSTATUS <> \'X\' AND WPO_WTE_KEY = WTE_KEY AND WTP_KEY IS NULL) AS ANZWAUNGEPLANT';
	$SQL.= ' FROM (';
	$SQL.= ' ';
	$SQL.= ' SELECT WTE_FIL_ID,TRUNC(WPO_TERMINVON) WPO_TERMINVON, SUM(WPO_ZEITMIN) AS WPO_ZEITMIN, SUM(WPO_VK) AS WPO_VK, WTE_KEY, WTE_WANR';
	$SQL.= ' FROM AWIS.WERKSTATTTERMINEPOSITIONEN';
	$SQL.= ' INNER JOIN WERKSTATTTERMINE ON WPO_WTE_KEY = WTE_KEY';
	$SQL.= ' WHERE WTE_STATUS < 8';            // Nicht die gel�schten Termine
	$SQL.= ' AND WPO_IMPORTSTATUS <> \'X\'';   // und keine gel�schten Positionen
	$SQL.= ' GROUP BY TRUNC(WPO_TERMINVON), WTE_FIL_ID, WTE_KEY, WTE_WANR';
	$SQL.= ' ) DATEN';
	$SQL.= ' WHERE WPO_TERMINVON<=TRUNC(SYSDATE+14)';
	$SQL.= ' AND WPO_TERMINVON >=TRUNC(SYSDATE)';
    $SQL.= ' ) DATEN2';
    $SQL .= ' INNER JOIN Filialebenenzuordnungen ON fez_fil_id = wte_fil_id AND fez_gueltigab <= SYSDATE AND fez_gueltigbis '  . $SRFunktionen->GueltigBis();
    $SQL .= ' INNER JOIN Filialebenen ON fez_feb_key = feb_key AND feb_gueltigab <= SYSDATE AND feb_gueltigbis '  . $SRFunktionen->GueltigBis();

	if(count($Filialen) > 0){
	    $SQL .= ' WHERE WTE_FIL_ID in(' . implode(',',$Filialen) . ' ) ';
    }
    $SQL.= ' GROUP BY FEB_BEZEICHNUNG, WTE_FIL_ID, WPO_TERMINVON';
	$SQL.= ' ORDER BY FEB_BEZEICHNUNG, WPO_TERMINVON, WTE_FIL_ID';
	$Form->DebugAusgabe(1,$SQL);

    $rsWTE = $DB->RecordSetOeffnen($SQL);

    $Form->DebugAusgabe(1,$DB->LetzterSQL());


	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['WTE_FIL_ID'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['txt_SummeAuftraege'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['txt_SummeZuPlanen'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_TERMINVON'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_ZEITMIN'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_VK'],100);
	$Form->ZeileEnde();

	$DS=0;
	$LetztesGebiet = '';

	$tmpFilialen = array_flip($Filialen);
	
	while(!$rsWTE->EOF())
	{
		$Link = './srpep_Main.php?cmdAktion=Planung&FIL_ID='.$rsWTE->FeldInhalt('WTE_FIL_ID').'&TAG='.$Form->Format('D',$rsWTE->FeldInhalt('WTE_KUNDENFZVON'));

		if($LetztesGebiet != $rsWTE->FeldInhalt('FEB_BEZEICHNUNG'))
		{
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEZ']['FEZ_FEB_KEY'].': '.$rsWTE->FeldInhalt('FEB_BEZEICHNUNG'),620);
			$Form->ZeileEnde();
		
			$LetztesGebiet = $rsWTE->FeldInhalt('FEB_BEZEICHNUNG');
		}

		if(isset($tmpFilialen[$rsWTE->FeldInhalt('WTE_FIL_ID')])){
		    unset($tmpFilialen[$rsWTE->FeldInhalt('WTE_FIL_ID')]);
        }

		$Form->ZeileStart();
		$Form->Erstelle_ListenFeld('#FIL',$rsWTE->FeldInhalt('WTE_FIL_ID'),10,100, false, ($DS%2),'','','N0');
		$Form->Erstelle_ListenFeld('#ANZ',$rsWTE->FeldInhalt('ANZWA'),10,100, false, ($DS%2),'','','N0');
		$Form->Erstelle_ListenFeld('#OFFEN',$rsWTE->FeldInhalt('ANZWAUNGEPLANT'),10,100, false, ($DS%2),'','','N0');

		$Recht11004 = $AWISBenutzer->HatDasRecht(11004);
		$Link = '';

		if($Recht11004 != 0){
            $Link = './srpep_Main.php?cmdAktion=Planung&FIL_ID='.$rsWTE->FeldInhalt('WTE_FIL_ID').'&TAG='.$Form->Format('D',$rsWTE->FeldInhalt('WPO_TERMINVON'));
        }
		$Form->Erstelle_ListenFeld('#WPO_TERMINVON',$rsWTE->FeldInhalt('WPO_TERMINVON'),10,100, false, ($DS%2),'',$Link,'D');
		$Form->Erstelle_ListenFeld('#WPO_ZEITMIN',($rsWTE->FeldInhalt('WPO_ZEITMIN')/60),10,100,false,($DS%2),'','','N1','R');
		$Form->Erstelle_ListenFeld('#WPO_VK',$rsWTE->FeldInhalt('WPO_VK'),10,100,false,($DS%2),'','','N0','R');
		$Form->ZeileEnde();

		$rsWTE->DSWeiter();
		$DS++;
	}

    $tmpFilialen = array_flip($tmpFilialen);
    sort($tmpFilialen);
	foreach ($tmpFilialen as $Fil){
        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('FIL_ID',$Fil,10,100,false,($DS%2));
        $Form->Erstelle_ListenFeld('Beschreibung','-- keine Auftr�ge --' ,50,520,false,($DS%2),'','','T','C');
        $Form->ZeileEnde();
        $DS++;
    }


	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if((($Recht11000&4)==4) AND isset($_POST['sucFEB_KEY']))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200912171119");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200912171120");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}