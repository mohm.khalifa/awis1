<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Thomas Riedl
 * @copyright ATU
 * @version 20090915
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ADK','%');	
	$TextKonserven[]=array('Wort','Auswahl_ALLE');	
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4300=$AWISBenutzer->HatDasRecht(4300);
	if($Recht4300==0)
	{
	    awisEreignis(3,1000,'ADK',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./adr_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ADK'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();
	
	$AWISCursorPosition='sucADK_STATUS';
	
	//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
	$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	// Filiale	
	if($UserFilialen!='')
	{	
		//$Form->Erstelle_TextFeld('*ADK_FIL_ID',$UserFilialen,20,200,false,'','','','T');
		echo '<input type=hidden name="sucADK_FIL_ID" value="'.$UserFilialen.'">';
	}
	else 
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_FIL_ID'].':',190);
		$Form->Erstelle_TextFeld('*ADK_FIL_ID',($Param['SPEICHERN']=='on'?$Param['ADK_FIL_ID']:''),20,200,true);	
		$AWISCursorPosition='sucADK_FIL_ID';
		$Form->ZeileEnde();
	}
				
	// Status
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_STATUS'].':',190);
	$Status = explode("|",$AWISSprachKonserven['ADK']['lst_ADK_STATUS']);	
	$Form->Erstelle_SelectFeld('*ADK_STATUS',($Param['SPEICHERN']=='on'?$Param['ADK_STATUS']:''),150,true,'','0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Status);		
	$Form->ZeileEnde();

	// Vorgangsnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_VORGANGNR'].':',190);
	$Form->Erstelle_TextFeld('*ADK_VORGANGNR',($Param['SPEICHERN']=='on'?$Param['ADK_VORGANGNR']:''),25,200,true);	
	$Form->ZeileEnde();

	// Erstellungsdatum
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_DATUMERSTELLUNG'].':',190);
	$Form->Erstelle_TextFeld('*ADK_DATUMERSTELLUNG',($Param['SPEICHERN']=='on'?$Param['ADK_DATUMERSTELLUNG']:''),25,250,true,'','','','D');	
	$Form->ZeileEnde();

	// Abschlussdatum
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_DATUMABSCHLUSS'].':',190);
	$Form->Erstelle_TextFeld('*ADK_DATUMABSCHLUSS',($Param['SPEICHERN']=='on'?$Param['ADK_DATUMABSCHLUSS']:''),25,250,true,'','','','D');	
	$Form->ZeileEnde();
		
	//Auswahl speichern
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();		
	
	//Pr�fen ob User eine Filiale ist
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
	
	//Wenn eine Fililale angemeldet ist, dann gleich speichern
	if(count($FilZugriffListe)==1 and $FilZugriff != '')
	{
		$FIL_ID=$FilZugriff;
		$Form->Erstelle_HiddenFeld('ADK_FIL_ID',$FIL_ID);			
		$Form->Erstelle_HiddenFeld('ADK_KEY',0);									
	}
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht4300&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
		
	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909150911");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909150912");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>