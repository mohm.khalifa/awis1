<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>
</head>

<body>
<?
global $PHP_AUTH_USER;			// USER Kennung


include ("ATU_Header.php");	// Kopfzeile

if(!(awisHatDasRecht(100) || $REMOTE_ADDR=="125.0.3.20"))
{
    awisEreignis(3,1000,'Filialen',"$PHP_AUTH_USER",'','','');
    die("Keine ausreichenden Rechte!");
}

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

print "<form name=frmFilialInfo action=./filialinfo_Main.php?" . $QUERY_STRING . ">";

	/************************************************
	* 
	* Daten speichern
	* 
	************************************************/

if(strpos($QUERY_STRING,"cmdSpeichern"))		// Nur wenn Speichernknopf aktiv ist
{
	if((awisBenutzerRecht($con,110)&2)==2)	// Speichern Filialpreise
	{
		global $txtStundenSatz;				// Neuer Wert

		$SQL = "BEGIN AWIS.AENDERN_FILIALINFOS($FILID,200,0" . abs(str_replace(",",".",$txtStundenSatz)) . ",'$PHP_AUTH_USER'); END;";
		awisExecute($con, $SQL);
	}

	
}	// Ende Speichern


if(strpos($QUERY_STRING,"cmdReset"))
{
	awis_BenutzerParameterSpeichern($con, "FilialSuche", $PHP_AUTH_USER,'');
	$cmdAktion = '';
}


/************************************************
* Daten anzeigen
************************************************/

$SpeichernButton = False;

awis_RegisterErstellen(1, $con);

print "<br><hr><img alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";

if($SpeichernButton)
{
	print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./filialinfo_Main.php?" . $QUERY_STRING . "&Speichern=True'>";

}

print "<input type=hidden name=cmdAktion value=Filialinfos>";
print "</form>";

//include "debug_info.php";
awislogoff($con);

?>
</body>
</html>

