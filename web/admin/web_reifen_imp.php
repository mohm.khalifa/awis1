<?
require_once("sicherheit.inc.php");
global $AWISBenutzer;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 10);

if(($RechteStufe&8)!=8)
{
    awisEreignis(3,1000,'Web-Reifenimport',"$AWISBenutzer->BenutzerName()",'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

echo "Es werden alle Reifendateien neu importiert!<br><br>";
    //************************************
    // LoginName l�schen
    //************************************
if($_POST['cmdReifimp_x'])
{
	$Erg = system("/bin/rm /daten/web/logs/web_reifen_imp.log");
}

if(file_exists('/daten/web/logs/web_reifen_imp.log'))
{
	echo 'Der letzte Reifenimport wurde durchgef�hrt. Kontrollieren Sie Ihr Ergebnis.';
	echo '<br>Verwenden Sie die Schaltfl�che um eine neuen Import zu starten!';
	print "<br><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./index.php'>";

	echo '<hr><form name=frmReifimp method=post action=./web_reifen_imp.php>';
	echo "<input type=image alt='Neuer Import' src=/bilder/verteilen.png name=cmdReifimp onclick=location.href='./web_reifen_imp.php'>";

	echo '<table border=0 width=100%>';
	echo '<tr><td><b>Zeile</b></td><td><b>Protokoll</b></td></tr>';
	
	$LogDatei = file('/daten/web/logs/web_reifen_imp.log');
	foreach($LogDatei as $ZeileNr => $ZeilenInhalt)
	{
		echo '<tr>';
		echo '<td width=50 align=right>' . $ZeileNr . ':</td>';
		
		If(strstr($ZeilenInhalt,'CONFLICT') OR strstr($ZeilenInhalt,'<-?->'))
		{
			echo '<td ><font color=#FF0000><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		elseif(strstr($ZeilenInhalt,"local") AND strstr($ZeilenInhalt,"ap-"))
		{
			echo '<td ><font size=4 color=#0000FF><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		elseif(strstr($ZeilenInhalt,'new file --') OR strstr($ZeilenInhalt,'changed  --'))
		{
			echo '<td ><font size=3 color=#00CC00><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		elseif(strstr($ZeilenInhalt,'deleted ')!='')
		{
			echo '<td ><font size=3 color=#FFFF00><b>' . $ZeilenInhalt . '</b></font></td>';
		}
		else
		{
			echo '<td >' . $ZeilenInhalt . '</td>';
		}
		echo '</tr>';	
	}
	echo '</table></form>';
}
elseif(file_exists('/daten/web/logs/web_reifen_imp.tmp'))
{
	echo 'Der Import l�uft gerade. Versuchen Sie es sp�ter wieder.';
	echo '<br>Verwenden Sie die Schaltfl�che um zu aktualisieren!';
	echo "<br><input type=image alt='Aktualisieren' src=/bilder/verteilen.png name=cmdReifimp onclick=location.href='./web_reifen_imp.php'>";
}
else
{
	echo 'Der Import wurde um ' . date("H:i:s") . ' Uhr in Auftrag gegeben. Wird in den n�chsten Minuten ausgef�hrt.';
	echo '<br>Verwenden Sie die Schaltfl�che um zu aktualisieren!';
	echo "<br><input type=image alt='Aktualisieren' src=/bilder/verteilen.png name=cmdReifimp onclick=location.href='./web_reifen_imp.php'>";
}

print "<hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./index.php'>";

awislogoff($con);
?>