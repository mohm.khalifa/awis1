<?php
/**
 * Suchmaske f�r Kassendifferenzen
 *
 * @author Patrick Gebhardt
 * @copyright ATU
 * @version 201110
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS'); 
	$DB->Oeffnen();
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KDI','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht36000=$AWISBenutzer->HatDasRecht(36000);
	if($Recht36000==0)
	{
	    $Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./kassendifferenzen_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Kassendifferenzen'));
	
	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	if(!isset($Param['KDI_HISTORISCH']))
	{
	    $Param['KDI_HISTORISCH'] = 'off';
	}

    $Form->Formular_Start();

    if($AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING )!= ''){
        $Filialen = $AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_ARRAY);
        $Daten = array();
        $FilialZugriffAnzahl = 0;
        foreach ($Filialen as $Fil){
            $FilialZugriffAnzahl++;
            $Daten[] = $Fil . '~' .$Fil;
        }
        if($FilialZugriffAnzahl==1){
            $Standard = $AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING );
            $Form->Erstelle_HiddenFeld('KDI_FILID',$Standard);
        }
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_FILID'].':',190);
        $Form->Erstelle_SelectFeld('*KDI_FILID',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on' ? $Param['KDI_FILID']:(isset($Standard)?$Standard:'')),'105:110',($FilialZugriffAnzahl>1?true:false ),'','','','','',$Daten);
        $Form->ZeileEnde();
    }else{
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_FILID'].':',190);
        $Form->Erstelle_TextFeld('*KDI_FILID',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['KDI_FILID']:''),15,150,true);
        $Form->ZeileEnde();
    }


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_DATUM_VON'].':',190);
	$Form->Erstelle_TextFeld('*KDI_DATUM_VON',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['KDI_DATUM_VON']:''),10,130,true,'','','','D');
	
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_DATUM_BIS'].':',130);
	$Form->Erstelle_TextFeld('*KDI_DATUM_BIS',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on' &&$Param['SPEICHERN']<>'' ?$Param['KDI_DATUM_BIS']:''),10,200,true,'','','','D');
	$Form->ZeileEnde();
	
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_KATEGORIE'].':',190);
	$KateogrieAlt = explode("|",$AWISSprachKonserven['KDI']['LST_KDI_KATEGORIE']);
	$Form->Erstelle_SelectFeld('*KDI_KATEGORIE',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['KDI_KATEGORIE']:''),359,true,'',$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','',$KateogrieAlt);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KDI']['KDI_HISTORISCH'].':',190);
    $Form->Erstelle_Checkbox('*Historie',($Param['SPEICHERN']=='on'&&$Param['KDI_HISTORISCH'] == 'on'?'on':''),30,true,'on','',$AWISSprachKonserven['KDI']['KDI_HISTORISCH']);
	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	
	if(($Recht36000&2) == 2)		// Hinzuf�gen erlaubt?
	{
	    $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>