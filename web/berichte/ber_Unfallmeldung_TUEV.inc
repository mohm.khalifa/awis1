<?php
require_once 'awisBerichte.inc';

class ber_Unfallmeldung_TUEV
    extends awisBerichte
{
    private $pdf_vorlage = '';
    private $letzte_positon = '';

    /**
     * Prozedur zum Initialisieren des Berichts
     *
     * @see awisBerichte::init()
     */
    public function init(array $Parameter)
    {

        // Daten f�r den Bericht laden
        $SQL = "";
        $SQL .= "SELECT * FROM v_unfallmeldung";

        // Alle Parameter auswerten
        // Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
        //			Beispiel: $Parameter['KEY_ZRU_KEY']="=~27"
        $Bedingung = "";
        if (isset($Parameter['UNF_KEY'])) {
            $Param = explode('~', $Parameter['UNF_KEY']);
            $Bedingung .= " AND unf_key " . self::_VergleichsOperatorPruefen($Param[0]) . " " . $this->_DB->FeldInhaltFormat('T', $Param[1], false);
        }

        if ($Bedingung != "") {
            $SQL .= " WHERE " . substr($Bedingung, 4);
        }

        // Daten laden
        $this->_rec = $this->_DB->RecordSetOeffnen($SQL);

        // Alle Textbausteine laden
        $TextKonserven[] = array('UNF', '*');
        $TextKonserven[] = array('Fehler', 'err_KeineDaten');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    /**
     * Eigentliches PDF erzeugen
     *
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {
        // PDF Attribute setzen
        $this->SetTitle($this->_BerichtsName);
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->AliasNbPages();

        $this->ErstelleTUEVStammBlatt01();

        $this->FertigStellen($Ausgabeart);
    }

    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage();
    }

    /**
     * Automatische Fu�zeile
     *
     * @see TCPDF::Footer()
     */
    public function Footer()
    {
    }

    /**
     * Automatische Kopfzeile
     *
     * @see TCPDF::Header()
     */
    public function Header()
    {
        //$this->pdf_vorlage = $this->ImportPage(1);
        //$this->useTemplate($pdf_vorlage);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Image('/daten/web/bilder/ATU_Deutschland_farbe.PNG', $this->_Parameter['AbstandLogo'], $this->_Zeile, 591 / 20, 276 / 20);
        $this->Image('/daten/web/dokumente/vorlagen/tuev_vorlage.gif', 188, $this->_Zeile, 55 / 5, 53 / 5);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 0.25;
        $this->_Spalte = $this->_Parameter['LinkerRand'] * 6;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText('<b>' . $this->_AWISSprachkonserven['UNF']['PDF_DOC02UEBERSCHRIFT'] . '</b>', $this->_Parameter['Zeilenhoehe'],
            $this->_Parameter['UeberschriftGross'], $this->_Parameter['Schriftart']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3.5;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText('<b>' . $this->_AWISSprachkonserven['UNF']['PDF_ARBSICH'] . '</b>', $this->_Parameter['Zeilenhoehe'], $this->_Parameter['UeberschriftGross'] / 2,
            $this->_Parameter['Schriftart']);
        $pos = $this->_Spalte + 1;
        $this->Line($pos, $this->_Zeile + 3, $this->_Parameter['AbstandH'] * 5.25, $this->_Zeile + 3);
    }

    public function FertigStellen($Ausgabeart)
    {
        //*********************************************************************
        // Berichtsende -> Zur�ckliefern
        //*********************************************************************
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zur�ckliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:         // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    protected function EditierBreite()
    {
        return self::SeitenBreite() - (self::RECHTERRAND + self::LINKERRAND);
    }


    private function ErstelleTUEVStammBlatt01()
    {
        // Startpositionen setzen
        $this->_Zeile = $this->_Parameter['Seite1-Oben'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];

        $this->NeueSeite();
        $this->Aussenrahmen();
        $this->Rahmen();
        $this->Werte();
        //$this->Ueberschrift();
        //$this->LieferantNr();
        //$this->Lieferantanschrift();
        //$this->ReklAnschrift();
        //$this->Zusatzinfos();
    }

    private function Aussenrahmen()
    {
        // Rahmen zeichnen
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 5, $this->_Parameter['Zeilenhoehe'] * 51, '', 1, 0, 'L');
    }

    private function Rahmen()
    {
        $this->ZeichneZeile1();
        $this->ZeichneZeile2();
        $this->ZeichneZeile3();
        $this->ZeichneZeile4();
        $this->ZeichneZeile5();
        $this->ZeichneZeile6();
        $this->ZeichneZeile7();
        $this->ZeichneZeile8();
        $this->ZeichneZeile9();
        $this->ZeichneZeile10();
        $this->ZeichneZeile11();
    }

    private function Werte()
    {
        $this->FuelleZeile1();
        $this->FuelleZeile2();
        $this->FuelleZeile3();
        $this->FuelleZeile4();
        $this->FuelleZeile5();
        $this->FuelleZeile6();
        $this->FuelleZeile7();
        $this->FuelleZeile8();
        $this->FuelleZeile9();
        $this->FuelleZeile10();
        $this->FuelleZeile11();
        $this->FuelleZeile12();
    }

    private function ZeichneZeile1()
    {
        $this->letzte_position = $this->_Zeile;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 9, '', 1, 0, 'L');
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 9, '', 1, 0, 'L');
    }

    private function FuelleZeile1()
    {
        $this->_Zeile = $this->letzte_position;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_FILNR'] . ':', 0, 0, 'L');
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('UNF_FIL_ID'), 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_FILADR'] . ':', 0, 0, 'L');
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $pos = strpos($this->_rec->FeldInhalt('UNF_FILIALEORT'), ';');
        $Filiale = substr($this->_rec->FeldInhalt('UNF_FILIALEORT'), 0, $pos);
        $Rest = substr($this->_rec->FeldInhalt('UNF_FILIALEORT'), $pos + 1);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $Filiale, 0, 0, 'L');
        $pos = strpos($Rest, ';');
        $PLZ = substr($Rest, 0, $pos);
        $Rest = substr($Rest, $pos + 1);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'] * 2.7;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $PLZ, 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'] * 2.7;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $Rest, 0, 0, 'L');
        $this->_Zeile = $this->letzte_position;
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText('<i><u><b>' . $this->_AWISSprachkonserven['UNF']['PDF_BERAN'] . ':</b></u></i> ', $this->_Parameter['Zeilenhoehe'],
            $this->_Parameter['SchriftgroesseKlein'] - 1, $this->_Parameter['Schriftart']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText('<b>' . $this->_AWISSprachkonserven['UNF']['PDF_ABTEILUNG'] . '</b>', $this->_Parameter['Zeilenhoehe'],
            $this->_Parameter['SchriftgroesseKlein'] - 1, $this->_Parameter['Schriftart']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText($this->_AWISSprachkonserven['UNF']['PDF_TEL_ARB'], $this->_Parameter['Zeilenhoehe'], $this->_Parameter['SchriftgroesseKlein'] - 1,
            $this->_Parameter['Schriftart']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText($this->_AWISSprachkonserven['UNF']['PDF_EMAIL_ARB'], $this->_Parameter['Zeilenhoehe'], $this->_Parameter['SchriftgroesseKlein'] - 1,
            $this->_Parameter['Schriftart']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText('<b>' . $this->_AWISSprachkonserven['UNF']['PDF_TUEV_ANSCH'] . '</b>', $this->_Parameter['Zeilenhoehe'],
            $this->_Parameter['SchriftgroesseKlein'] - 1, $this->_Parameter['Schriftart']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText($this->_AWISSprachkonserven['UNF']['PDF_TUEV_FAX'], $this->_Parameter['Zeilenhoehe'], $this->_Parameter['SchriftgroesseKlein'] - 1,
            $this->_Parameter['Schriftart']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SchreibeSonderText($this->_AWISSprachkonserven['UNF']['PDF_TUEV_EMAIL'], $this->_Parameter['Zeilenhoehe'], $this->_Parameter['SchriftgroesseKlein'] - 1,
            $this->_Parameter['Schriftart']);
    }

    private function ZeichneZeile2()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 9;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
    }

    private function FuelleZeile2()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_VERLETZTER'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('UNF_NAME') . ',' . $this->_rec->FeldInhalt('UNF_VORNAME'), 0, 0, 'L');

        $this->_Zeile = $this->letzte_position;
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNFTAG'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D', $this->_rec->FeldInhalt('UNF_DATUM')), 0, 0, 'L');
    }

    private function ZeichneZeile3()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
    }

    private function FuelleZeile3()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_PERSNR'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('UNF_PERSNR'), 0, 0, 'L');
    }

    private function ZeichneZeile4()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
        $this->Cell($this->_Parameter['AbstandH'] * 2.5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
    }

    private function FuelleZeile4()
    {
        $RectHoehe = 3;
        $RectLaenge = 3;
        $RectZeile = 0;
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNTAM'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D', $this->_rec->FeldInhalt('UNF_PRUEFUNGAM')), 0, 0, 'L');

        $this->_Zeile = $this->letzte_position;
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.78;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_MIT'] . ':', 0, 0, 'L');

        $this->_Spalte = $this->_Spalte + 11;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $RectZeile = $this->_Zeile + ($this->_Parameter['Zeilenhoehe'] - $RectHoehe) / 2;
        $this->Rect($this->_Spalte, $RectZeile, $RectLaenge, $RectHoehe);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        if ($this->_rec->FeldInhalt('UNF_PRUEFUNGMITOPFER') == 1) {
            $this->Line($this->_Spalte, $RectZeile, $this->_Spalte + $RectLaenge, $RectZeile + $RectHoehe);
            $this->Line($this->_Spalte + $RectLaenge, $RectZeile, $this->_Spalte, $RectZeile + $RectHoehe);
        }
        $this->_Spalte = $this->_Spalte + $RectLaenge + 1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['UNF_HERGANGVERSICHERTER'], 0, 0, 'L');

        $this->_Spalte = $this->_Spalte + 22;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $RectZeile = $this->_Zeile + ($this->_Parameter['Zeilenhoehe'] - $RectHoehe) / 2;
        $this->Rect($this->_Spalte, $RectZeile, $RectLaenge, $RectHoehe);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        if ($this->_rec->FeldInhalt('UNF_PRUEFUNGMITGL') == 1) {
            $this->Line($this->_Spalte, $RectZeile, $this->_Spalte + $RectLaenge, $RectZeile + $RectHoehe);
            $this->Line($this->_Spalte + $RectLaenge, $RectZeile, $this->_Spalte, $RectZeile + $RectHoehe);
        }
        $this->_Spalte = $this->_Spalte + $RectLaenge + 1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['UNF_PRUEFUNGMITGL'], 0, 0, 'L');

        $this->_Spalte = $this->_Spalte + 10;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $RectZeile = $this->_Zeile + ($this->_Parameter['Zeilenhoehe'] - $RectHoehe) / 2;
        $this->Rect($this->_Spalte, $RectZeile, $RectLaenge, $RectHoehe);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        if ($this->_rec->FeldInhalt('UNF_PRUEFUNGMITWL') == 1) {
            $this->Line($this->_Spalte, $RectZeile, $this->_Spalte + $RectLaenge, $RectZeile + $RectHoehe);
            $this->Line($this->_Spalte + $RectLaenge, $RectZeile, $this->_Spalte, $RectZeile + $RectHoehe);
        }
        $this->_Spalte = $this->_Spalte + $RectLaenge + 1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['UNF_PRUEFUNGMITWL'], 0, 0, 'L');
    }

    private function ZeichneZeile5()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 5, $this->_Parameter['Zeilenhoehe'] * 1.5, '', 1, 0, 'L');
    }

    private function FuelleZeile5()
    {
        $RectHoehe = 3;
        $RectLaenge = 3;
        $RectZeile = 0;
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_VEREINBAR'] . ':', 0, 0, 'L');

        $this->_Spalte = $this->_Parameter['LinkerRand'] * 11;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $RectZeile = $this->_Zeile + ($this->_Parameter['Zeilenhoehe'] - $RectHoehe) / 2;
        $this->Rect($this->_Spalte, $RectZeile, $RectLaenge, $RectHoehe);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        if ($this->_rec->FeldInhalt('UNF_VEREINBAR') == 1) {
            $this->Line($this->_Spalte, $RectZeile, $this->_Spalte + $RectLaenge, $RectZeile + $RectHoehe);
            $this->Line($this->_Spalte + $RectLaenge, $RectZeile, $this->_Spalte, $RectZeile + $RectHoehe);
        }
        $this->_Spalte = $this->_Spalte + $RectLaenge + 1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_JA'], 0, 0, 'L');

        $this->_Spalte = $this->_Spalte + 18;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $RectZeile = $this->_Zeile + ($this->_Parameter['Zeilenhoehe'] - $RectHoehe) / 2;
        $this->Rect($this->_Spalte, $RectZeile, $RectLaenge, $RectHoehe);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        if ($this->_rec->FeldInhalt('UNF_VEREINBAR') == 0) {
            $this->Line($this->_Spalte, $RectZeile, $this->_Spalte + $RectLaenge, $RectZeile + $RectHoehe);
            $this->Line($this->_Spalte + $RectLaenge, $RectZeile, $this->_Spalte, $RectZeile + $RectHoehe);
        }
        $this->_Spalte = $this->_Spalte + $RectLaenge + 1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_NEIN'], 0, 0, 'L');
    }

    private function ZeichneZeile6()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 5, $this->_Parameter['Zeilenhoehe'] * 16, '', 1, 0, 'L');
    }

    private function FuelleZeile6()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNFSCHILD'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        //$MZeile = preg_replace("/[\n]/", '', $this->_rec->FeldInhalt('UNF_HERGANG'));
        //$MZeile = preg_replace("/[\r]/", '', $MZeile);
        $MZeile = $this->_rec->FeldInhalt('UNF_HERGANG');
        //Inhalt der Multicell begrenzen und sauber abschneiden.
        if (strlen($MZeile) > 1520) {
            $MZeile = substr($MZeile, 0, 1520);
            $ipos = strrpos($MZeile, ' ');
            if ($ipos != 0) {
                $MZeile = substr($MZeile, 0, $ipos);
            }
            $MZeile .= "...";
        }
        $this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe'] * 0.5, $MZeile, 0, 'L', 0, 1);
    }

    private function ZeichneZeile7()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 16;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 5, $this->_Parameter['Zeilenhoehe'] * 2, '', 1, 0, 'L');
    }

    private function FuelleZeile7()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 14;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNFGEGENSTAND'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('UFT_URSACHESPEZ'), 0, 0, 'L');
    }

    private function ZeichneZeile8()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
    }

    private function FuelleZeile8()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_BENPSA'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('UNF_BENUTZTEPSA'), 0, 0, 'L');
    }

    private function ZeichneZeile9()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 5, $this->_Parameter['Zeilenhoehe'] * 1.5, '', 1, 0, 'L');
    }

    private function FuelleZeile9()
    {
        $RectHoehe = 3;
        $RectLaenge = 3;
        $RectZeile = 0;
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['UNF_LETZTEUNTERWEISUNGAM'] . ':', 0, 0, 'L');

        $this->_Spalte = $this->_Parameter['LinkerRand'] * 8;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Parameter['AbstandH'] / 4, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('UNF_DATUMTAETTUEV'), 0, 0, 'L');
    }

    private function ZeichneZeile10()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 5, $this->_Parameter['Zeilenhoehe'] * 2.5, '', 1, 0, 'L');
    }

    private function FuelleZeile10()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNFURSACHE'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('UFT_URSACHETUEV'), 0, 0, 'L');
    }

    private function ZeichneZeile11()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'] * 2.1, $this->_Parameter['Zeilenhoehe'] * 6, '', 1, 0, 'L');
        $this->Cell($this->_Parameter['AbstandH'] * 2.9, $this->_Parameter['Zeilenhoehe'] * 6, '', 1, 0, 'L');
    }

    private function FuelleZeile11()
    {
        $RectHoehe = 3;
        $RectLaenge = 3;
        $RectZeile = 0;
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNFUNT'], 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNFUNT2'] . ':', 0, 0, 'L');
        $this->_Spalte = $this->_Parameter['LinkerRand'] * 3;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $RectZeile = $this->_Zeile + ($this->_Parameter['Zeilenhoehe'] - $RectHoehe) / 2;
        $this->Rect($this->_Spalte, $RectZeile, $RectLaenge, $RectHoehe);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);

        if ($this->_rec->FeldInhalt('UNF_UNTERWEISUNG') == 1) {
            $this->Line($this->_Spalte, $RectZeile, $this->_Spalte + $RectLaenge, $RectZeile + $RectHoehe);
            $this->Line($this->_Spalte + $RectLaenge, $RectZeile, $this->_Spalte, $RectZeile + $RectHoehe);
        }
        $this->_Spalte = $this->_Spalte + $RectLaenge + 1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_JA'], 0, 0, 'L');

        $this->_Spalte = $this->_Spalte + 18;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $RectZeile = $this->_Zeile + ($this->_Parameter['Zeilenhoehe'] - $RectHoehe) / 2;
        $this->Rect($this->_Spalte, $RectZeile, $RectLaenge, $RectHoehe);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        if ($this->_rec->FeldInhalt('UNF_UNTERWEISUNG') == 0) {
            $this->Line($this->_Spalte, $RectZeile, $this->_Spalte + $RectLaenge, $RectZeile + $RectHoehe);
            $this->Line($this->_Spalte + $RectLaenge, $RectZeile, $this->_Spalte, $RectZeile + $RectHoehe);
        }
        $this->_Spalte = $this->_Spalte + $RectLaenge + 1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_NEIN'], 0, 0, 'L');
        $this->_Zeile = $this->letzte_position;
        $this->_Spalte = $this->_Parameter['AbstandH'] * 2.37;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['AbstandJa'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['UNF']['PDF_UNFMASS'] . ':', 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart2'], '', $this->_Parameter['SchriftgroesseKlein']);
        $MZeile = preg_replace("/[\n]/", '', $this->_rec->FeldInhalt('UNF_UNFALLVERHUETUNG'));
        //Inhalt der Multicell begrenzen und sauber abschneiden.
        if (strlen($MZeile) > 310) {
            $MZeile = substr($MZeile, 0, 310);
            $ipos = strrpos($MZeile, ' ');
            if ($ipos != 0) {
                $MZeile = substr($MZeile, 0, $ipos);
            }
            $MZeile .= "...";
        }
        $this->MultiCell($this->_Parameter['AbstandH'] * 2.89, $this->_Parameter['Zeilenhoehe'], $MZeile, 0, 'L', 0, 1);
    }

    private function FuelleZeile12()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 5;
        $this->letzte_position = $this->_Zeile;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->Cell($this->_Parameter['AbstandH'] / 2, $this->_Parameter['Zeilenhoehe'],
            $this->_AWISSprachkonserven['UNF']['PDF_DATUM'] . ', ' . $this->_AWISSprachkonserven['UNF']['PDF_UNTERSCHRIFT'] . ':', 0, 0, 'L');
    }
}

?>