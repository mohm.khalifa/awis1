<?php

require_once 'awisDistributionCenter.php';
try{
    require_once 'awisDatenbank.inc';
    require_once 'awisBenutzer.inc';
    require_once 'awisRest.inc';


    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Benutzer = awisBenutzer::Init('awis_jobs');

    $awisREST = new awisRest('DistributionStructureAPI');
    $awisREST->verarbeiteRequest();

    $Request = $awisREST->getRequest();

    $DistributionStructure = new stdClass();

    $SQL  ='select FRZ_KEY, FRZ_FER_KEY';
    $SQL .=' from  FILIALEBENENROLLENZUORDNUNGEN B';

    $Where = '';

   if(isset($Request['SALESFORCE_KEY']['WERT'])){ //Filialrollenzuordnung?
       $Where .= ' AND FRZ_KEY = ' . $DB->WertSetzen('FEB','Z',$Request['SALESFORCE_KEY']['WERT']);
    }elseif(isset($Request['EMPLOYEEID']['WERT'])){ //Personalnummer
       $SQL .= ' inner join KONTAKTE c';
       $SQL .= '  on c.KON_KEY = B.FRZ_KON_KEY';
       $Where .= ' AND KON_PER_NR = ' . $DB->WertSetzen('FEB','T',$Request['EMPLOYEEID']['WERT']);
    }elseif(isset($Request['USERNAME']['WERT'])){ //Username
       $SQL .= ' inner join KONTAKTE c';
       $SQL .= '  on c.KON_KEY = B.FRZ_KON_KEY';
       $SQL .= ' inner join BENUTZER d';
       $SQL .= ' on d.XBN_KON_KEY= c.KON_KEY ';
       $SQL .= ' inner join BENUTZERLOGINS e';
       $SQL .= ' on e.XBL_XBN_KEY = d.XBN_KEY';
       $Where .= ' AND XBL_LOGIN = ' . $DB->WertSetzen('FEB','TU',$Request['USERNAME']['WERT']);
    }elseif(isset($Request['USER_KEY']['WERT'])){ //Username
       $SQL .= ' inner join KONTAKTE c';
       $SQL .= '  on c.KON_KEY = B.FRZ_KON_KEY and c.KON_STATUS <> \'I\' ';
       $SQL .= ' inner join BENUTZER d';
       $SQL .= ' on d.XBN_KON_KEY= c.KON_KEY ';
       $Where .= ' AND XBN_KEY = ' . $DB->WertSetzen('FEB','TU',$Request['USER_KEY']['WERT']);
   }else{
        $awisREST->sendeResponse('Missing required field',awisRest::AntwortTyp_JSON,500);
        die;
   }

    //Gueltig von:
    if(isset($Request['DATE'])){
        $Where .=' and FRZ_GUELTIGAB <= '.$DB->WertSetzen('FEB','D',$Request['DATE']['WERT']);
        $Where .=' and FRZ_GUELTIGBIS >= '.$DB->WertSetzen('FEB','D',$Request['DATE']['WERT']);
        $Date = $Request['DATE']['WERT'];
    }else{
        $Where .=' and FRZ_GUELTIGAB <= sysdate';
        $Where .=' and FRZ_GUELTIGBIS >= sysdate';
        $Date = date('c');
    }


    if ($Where != '') {
        $Where = ' WHERE ' . substr($Where, 4);
    }


    $SQL .= $Where;

    $rsFEB = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FEB'));

    if($rsFEB->AnzahlDatensaetze()==0){
        $awisREST->sendeResponse('no Data found',awisRest::AntwortTyp_JSON,404);
        die;
    }

    $SalesForce = new awisDistributionSalesForce($DB, 'FRZ', $rsFEB->FeldInhalt('FRZ_KEY'));


    $Response = $SalesForce->getSalesForce($Date,array($rsFEB->FeldInhalt('FRZ_FER_KEY')));

    $awisREST->sendeResponse($Response,awisRest::AntwortTyp_JSON);

}

catch(awisException $e){
    var_dump($e->getSQL());
}catch (Exception $e){
    var_dump($e);
}