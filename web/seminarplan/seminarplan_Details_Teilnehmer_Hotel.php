<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_KEY3;

/*
 * Hotelinformation hinterlegen f�r:
 */
$EditRecht = (($Recht4200 & 524288) == 524288) ? true : false;

$Felder = array();
$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./seminarplan_Main.php?cmdAktion=Details&SEP_KEY=" . $AWIS_KEY1 . " accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
$Form->InfoZeile($Felder, '');

if($rsSET->FeldInhalt('SET_HOTEL') == 0 and $rsSET->FeldInhalt('SET_TEILNAHME') == 1){
    $Form->Hinweistext($AWISSprachKonserven['SEH']['SEH_HINWEIS_KEINHOTEL'],awisFormular::HINWEISTEXT_WARNUNG);
}elseif($rsSET->FeldInhalt('SET_TEILNAHME') == 0 and $rsSET->FeldInhalt('SET_TEILNAHME') != '' ){
    $Form->Hinweistext($AWISSprachKonserven['SEH']['SEH_HINWEIS_KEINEBESTAETIGUNG'],awisFormular::HINWEISTEXT_WARNUNG);
}
if($EditRecht) {
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_INFOHINTERLEGEN'], 200, 'font-weight: bold; font-size: 12px');
    $Form->ZeileEnde();
}

$Form->ZeileStart();
$Form->Erstelle_TextLabel($rsSET->FeldInhalt('SET_VORNAME') . ' ' . $rsSET->FeldInhalt('SET_NACHNAME') .' ('. $rsSET->FeldInhalt('SET_PER_NR') .') ' . $rsSET->FeldInhalt('SET_FIL_NR'),500);
$Form->ZeileEnde();

if($EditRecht) {

    $Form->Trennzeile('O');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_UNDFUER'], 180, 'font-weight: bold; font-size: 12px');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $SQL = "SELECT SET_KEY, SET_NACHNAME  || ' ' || SET_VORNAME || ' (' || SET_PER_NR || ') ' || SET_FIL_NR || case when SET_HOTEL = 1 then ' => Hotel ja' else case when SET_HOTEL = 0 then ' => Hotel nein' else ' => Hotel ?' end end from SEMINARTEILNEHMER ";
    $SQL .= ' LEFT JOIN SEMINARHOTELZUORD ';
    $SQL .= ' ON SET_KEY = SHZ_SET_KEY  ';
    $SQL .= ' LEFT JOIN SEMINARHOTEL';
    $SQL .= ' ON SEH_KEY = SHZ_SEH_KEY';
    $SQL .= ' where SET_SEP_KEY = ' . $DB->WertSetzen('SET', 'Z', $AWIS_KEY1);
    $SQL .= ' AND SET_KEY <> ' . $DB->WertSetzen('SET', 'Z', $AWIS_KEY2);
    $SQL .= ' order by SET_HOTEL desc nulls last, SET_NACHNAME ';
    $Form->Erstelle_MehrfachSelectFeld('ZUSATZ_SHZ_SET_KEY', isset($_POST['txtZUSATZ_SHZ_SET_KEY']) ? $_POST['txtZUSATZ_SHZ_SET_KEY'] : array(), '571:571', true, $SQL, '', '', '', '', '', '', '', $DB->Bindevariablen('SET'), '', 'AWIS', '', '', 0);
    $Form->ZeileEnde();
}

/*
 * Hoteldaten
 */

$SQL = 'Select * from Seminarhotel';
$SQL .= ' left join SEMINARHOTELZUORD';
$SQL .= ' on Seminarhotel.SEH_KEY = SEMINARHOTELZUORD.SHZ_SEH_KEY';
$SQL .= ' WHERE SHZ_SET_KEY = '.$DB->WertSetzen('SEH','N0',$_GET['HOTEL_SET_KEY']);

$rsSEH = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SEH'));
$AWIS_KEY3 = $rsSEH->FeldInhalt('SEH_KEY');
$Form->Trennzeile('L');

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOTELDATEN'], 100, 'font-weight: bold; font-size: 12px');
if($rsSEH->FeldOderPOST('SEH_STRASSE')!=''){
    $MapsParameter = $rsSEH->FeldOderPOST('SEH_STRASSE') . ' ' . $rsSEH->FeldOderPOST('SEH_HAUSNR') . ', ' .  $rsSEH->FeldOderPOST('SEH_PLZ');
    $MapsParameter = urlencode(utf8_encode($MapsParameter));
    echo '<a target="_blanktarget="_blank" href="https://www.google.com/maps/dir/'.$MapsParameter .'">'.$AWISSprachKonserven['SEH']['SEH_GOOGLEMAPS'].'</a>';
}
$Form->ZeileEnde();

/*
 * Infobox mit Teilnehmern, die im gleichen Hotel untergebracht sind.
 */
if (isset($AWIS_KEY3) and $AWIS_KEY3 > 0) {
    $SQL = 'select SEMINARTEILNEHMER.SET_NACHNAME, SEMINARTEILNEHMER.SET_VORNAME from SEMINARHOTELZUORD';
    $SQL .= ' inner join SEMINARTEILNEHMER';
    $SQL .= ' on SEMINARHOTELZUORD.SHZ_SET_KEY = SEMINARTEILNEHMER.SET_KEY';
    $SQL .= ' where SEMINARHOTELZUORD.SHZ_SEP_KEY = '.$AWIS_KEY1;
    $SQL .= ' and SEMINARTEILNEHMER.SET_KEY != '.$AWIS_KEY2;
    $SQL .= ' and SEMINARHOTELZUORD.SHZ_SEH_KEY = '.$AWIS_KEY3;
    $SQL .= ' AND SET_STATUSTEILNEHMER != 3';

    $rsSET = $DB->RecordSetOeffnen($SQL);


    $DS = 0;
    $person = '';

    while (!$rsSET->EOF()) {
        $person .= $rsSET->FeldInhalt('SET_NACHNAME').' '.$rsSET->FeldInhalt('SET_VORNAME').', ';
        $rsSET->DSWeiter();
        $DS++;
    }

    $person = substr($person, 0, -2);

    if ($rsSET->AnzahlDatensaetze() > 0) {
        $Form->Hinweistext($AWISSprachKonserven['SEH']['SEH_GLEICHES_HOTEL'].$person,awisFormular::HINWEISTEXT_WARNUNG);
    }
}

$Form->Formular_Start();

$Form->Erstelle_HiddenFeld('SHZ_SET_KEY', $_GET['HOTEL_SET_KEY']);

$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

$SQL = 'SELECT *';
$SQL .= ' FROM SEMINARHOTEL';
$SQL .= ' INNER JOIN SEMINARHOTELZUORD';
$SQL .= ' ON SEMINARHOTEL.SEH_KEY = SEMINARHOTELZUORD.SHZ_SEH_KEY';
$SQL .= ' WHERE SHZ_SEP_KEY = '. $AWIS_KEY1;
$SQL .= ' AND SHZ_SET_KEY = '.$AWIS_KEY2;
$rsSEH = $DB->RecordSetOeffnen($SQL);

$SQL = "SELECT SEH_KEY, SEH_NAME || ' (' || SEH_ORT || ', ' || SEH_STRASSE || ' ' || SEH_HAUSNR || ')' FROM SEMINARHOTEL WHERE SEH_STATUS = 'A'";
if($rsSEH->FeldInhalt('SHZ_SEH_KEY')){
    $SQL .= " or SEH_KEY = ".$rsSEH->FeldInhalt('SHZ_SEH_KEY');
}

if($EditRecht) {
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('', 100);
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_NEUESHOTEL'], 200, '', '/seminarplan/seminarplan_Main.php?cmdAktion=Hotels');
    $Form->ZeileEnde();
}
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOTELNAME'], 100);
if(isset($_POST['del_relation_x'])) {
    include './seminarplan_loeschen.php';
}
if($EditRecht or $rsSEH->AnzahlDatensaetze() >= 1) {
    $Form->AuswahlSelectFeld('~SHZ_HOTEL', $rsSEH->FeldOderPOST('SEH_KEY', 'N0', true), '', $EditRecht, $SQL, 'lst_SEH_HOTEL', 'box1', array('txtSHZ_HOTEL'), $OptionBitteWaehlen, '', '');
    $Form->Erstelle_HiddenFeld('SEH_LAN_CODE', $rsSEH->FeldOderPOST('SEH_LAN_CODE', 'T', true));
    if(!$EditRecht){ //Falls Hotel gefunden aber kein EditRecht (damit Hotelinfos angezeigt werden)
        $Form->SchreibeHTMLCode('<input type=hidden class="InputText" value="'.$rsSEH->FeldInhalt('SHZ_SEH_KEY').'" name="txtSHZ_HOTEL" id="txtSHZ_HOTEL" oninput="lst_SEH_HOTEL(this);" style="" onchange="setzeFarbe(\'txtSHZ_HOTEL\');">');
    }
} else {
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_KEINHOTEL'], 200);
}
if($EditRecht) {
    $Form->Schaltflaeche('image', 'del_relation', '', '/bilder/icon_delete.png', 'entfernen', '', '', 20, 20, '', true);
}
$Form->ZeileEnde();

echo '<script> ';
echo ' lst_SEH_HOTEL(this); ' . PHP_EOL;
echo ' $(".custom-combobox-input").width("500px");' . PHP_EOL;
echo '</script>';

$Form->AuswahlBoxHuelle('box1','AuswahlListe','','');

$SQL = 'SELECT a.*, b.SET_HOTEL_VON,b.SET_HOTEL_BIS FROM SEMINARHOTELZUORD a';
$SQL.= ' right join seminarteilnehmer b';
$SQL.= ' ON b.SET_KEY = a.SHZ_SET_KEY';
$SQL.= ' WHERE b.SET_KEY = '.$DB->WertSetzen('SHZ', 'N0', $AWIS_KEY2, true);
$rsSEH = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SHZ'));

$Form->Trennzeile('O');

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOTELVON'], 100);
$Form->Erstelle_TextFeld('SHZ_HOTELVON', ($rsSEH->Feldinhalt('SHZ_HOTELVON','D')<>"")?($rsSEH->Feldinhalt('SHZ_HOTELVON','D')):($rsSEH->Feldinhalt('SET_HOTEL_VON','D')),20,350, $EditRecht,'','','','D','L','',$rsSEH->Feldinhalt('SHZ_HOTELVON','D'));
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOTELBIS'], 100);
$Form->Erstelle_TextFeld('SHZ_HOTELBIS', ($rsSEH->Feldinhalt('SHZ_HOTELBIS','D')<>"")?($rsSEH->Feldinhalt('SHZ_HOTELBIS','D')):($rsSEH->Feldinhalt('SET_HOTEL_BIS','D')),20,350, $EditRecht,'','','','D','L','',$rsSEH->Feldinhalt('SHZ_HOTELBIS','D'));
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_ZUSATZINFO'], 100);
$Form->Erstelle_Textarea('SHZ_ZUSATZINFO', $rsSEH->Feldinhalt('SHZ_ZUSATZINFO'),800,113,5,$EditRecht);
$Form->ZeileEnde();


$SQL = 'SELECT Dokumente.*, xbn_name FROM Dokumente';
$SQL .= ' INNER JOIN dokumentZuordnungen ON doc_key = doz_doc_key';
$SQL .= ' INNER JOIN Benutzer ON doc_xbn_key = xbn_key';
$SQL .= ' WHERE doz_xxx_key=' . $DB->WertSetzen('DOZ', 'Z', $AWIS_KEY2);
$SQL .= ' AND doz_xxx_kuerzel=\'SEH\'';

$SQL .= ' ORDER BY doc_datum desc';

$rsDOC = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('DOZ'));

//Liste Mit Dokumenten nur malen, wenn es Dokumente gibt
if($rsDOC->AnzahlDatensaetze()>0){

    $Form->Trennzeile('L');

    $Form->ZeileStart();
    $Link = "./bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=" . $AWIS_KEY1 . "&BWH_KEY=" . $AWIS_KEY2 . "&Dokumente=1";
    $Link .= '&DOCSort=DOC_BEZEICHNUNG' . ((isset($_GET['DOCSort']) AND ($_GET['DOCSort'] == 'DOC_BEZEICHNUNG')) ? '~' : '');
    $Link .= '#Dokumente';
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'], 550, '', $Link);
    $Form->Erstelle_Liste_Ueberschrift('', 30, '', $Link);
    $Form->ZeileEnde();
}
$DS = 0;

while (!$rsDOC->EOF()) {
    $Form->ZeileStart();
    $Icons = array();

    if ($EditRecht) {
        $Form->Erstelle_ListenLoeschPopUp('./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SEP_KEY='.$AWIS_KEY1.'&HOTEL_SET_KEY='.$AWIS_KEY2.'&DocDel=' . $rsDOC->FeldInhalt('DOC_KEY'), $rsDOC->FeldInhalt('DOC_KEY'), 20, '', '', ($DS % 2));
    }

    $Form->Erstelle_ListenFeld('#DOC_BEZEICHNUNG', $rsDOC->FeldInhalt('DOC_BEZEICHNUNG'), 20, 550, false, ($DS % 2),
        '', '', 'T');

    $Link = '/dokumentanzeigen.php?doctab=SEH&dockey=' . $rsDOC->FeldInhalt('DOC_KEY') . '';

  
    $Form->Erstelle_ListenBild('href', 'DOC', $Link, '/bilder/dateioeffnen_klein.png', $AWISSprachKonserven['Wort']['DateiOeffnen'],  ($DS % 2), '', 17, 17, 18);
    
    $Form->ZeileEnde();

    $rsDOC->DSWeiter();
    $DS++;
}

if($EditRecht) {
    $Form->Trennzeile('L');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_NEUEDATHOCH'], 150, 'font-weight: bold; font-size: 12px');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'] . ':', 150);
    $Form->Erstelle_TextFeld('DOC_BEZEICHNUNG', ($rsDOC->FeldInhalt('DOC_BEZEICHNUNG')<>"")?$rsDOC->FeldInhalt('DOC_BEZEICHNUNG'):$AWISSprachKonserven['SEH']['SEH_HOTELINFO'], 60, 600, true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_DateiUpload('DOC', 800, 65);
    $Form->ZeileEnde();
}