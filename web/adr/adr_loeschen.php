<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschenRueckfuehrung');
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
{
	$Tabelle = 'ADK';
	$Key=$_POST['txtADK_KEY'];
	$ADKKey=$_POST['txtADK_KEY'];

	$AWIS_KEY1 = $Key;

	$Felder=array();
	$Felder[]=array($Form->LadeTextBaustein('ADK','ADK_VORGANGNR'),$_POST['txtADK_VORGANGNR']);	
}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'Abfallstamm':
			$Tabelle = 'ADS';
			$Key=$_GET['Del'];

			$SQL = 'SELECT ADS_LAGACODE, ADS_LAGABEZ, ADS_ADRNR, ADS_KEY';
			$SQL .= ' FROM ADRStamm ';
			$SQL .= ' WHERE ADS_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			//$RFKKey = $rsDaten->FeldInhalt('RFF_RFK_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('ADS','ADS_ADRNR'),$rsDaten->FeldInhalt('ADS_ADRNR'));
			$Felder[]=array($Form->LadeTextBaustein('ADS','ADS_LAGACODE'),$rsDaten->FeldInhalt('ADS_LAGACODE'));
			$Felder[]=array($Form->LadeTextBaustein('ADS','ADS_LAGABEZ'),$rsDaten->FeldInhalt('ADS_LAGABEZ'));			
			break;
	case 'Gasflaschen':
			$Tabelle = 'ADS';
			$Key=$_GET['Del'];

			$SQL = 'SELECT ADS_LAGACODE, ADS_LAGABEZ, ADS_ADRNR, ADS_KEY';
			$SQL .= ' FROM ADRStamm ';
			$SQL .= ' WHERE ADS_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			//$RFKKey = $rsDaten->FeldInhalt('RFF_RFK_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('ADS','ADS_ADRNR'),$rsDaten->FeldInhalt('ADS_ADRNR'));
			$Felder[]=array($Form->LadeTextBaustein('ADS','ADS_LAGACODE'),$rsDaten->FeldInhalt('ADS_LAGACODE'));
			$Felder[]=array($Form->LadeTextBaustein('ADS','ADS_LAGABEZ'),$rsDaten->FeldInhalt('ADS_LAGABEZ'));			
			break;			
		case 'Texte':
			$Tabelle = 'ADX';
			$Key=$_GET['Del'];

			$SQL = 'SELECT ADX_ADRART, ADX_ADRBLOCK, ADX_ADRSORT, ADX_ADRBEZ';
			$SQL .= ' FROM ADRTexte ';
			$SQL .= ' WHERE ADX_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			//$RFKKey = $rsDaten->FeldInhalt('RFF_RFK_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('ADX','ADX_ADRART'),$rsDaten->FeldInhalt('ADX_ADRART'));
			$Felder[]=array($Form->LadeTextBaustein('ADX','ADX_ADRBLOCK'),$rsDaten->FeldInhalt('ADX_ADRBLOCK'));
			$Felder[]=array($Form->LadeTextBaustein('ADX','ADX_ADRSORT'),$rsDaten->FeldInhalt('ADX_ADRSORT'));			
			$Felder[]=array($Form->LadeTextBaustein('ADX','ADX_ADRBEZ'),$rsDaten->FeldInhalt('ADX_ADRBEZ'));			
			break;			
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{
	$SQL = '';
	switch ($_POST['txtTabelle'])
	{
		case 'ADK':
			$SQL = 'DELETE FROM ADRKopf WHERE ADK_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			break;
		case 'ADS':
			$SQL = 'DELETE FROM ADRStamm WHERE ADS_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			break;					
		case 'ADX':
			$SQL = 'DELETE FROM ADRTexte WHERE ADX_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			break;					
		default:
			break;
	}

	if($SQL !='')
	{
		if($DB->Ausfuehren($SQL)===false)
		{
			awisErrorMailLink('adr_loeschen_1',1,$awisDBError['messages'],'');
		}
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

	$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./adr_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

	$Form->Formular_Start();
	$Form->ZeileStart();
	
	
	$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
	
	$Form->ZeileEnde();

	foreach($Felder AS $Feld)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Feld[0].':',200);
		$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		$Form->ZeileEnde();
	}

	//$Form->Erstelle_HiddenFeld('RFKKey',$RFKKey);
	$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
	$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
	$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
	$Form->ZeileEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>