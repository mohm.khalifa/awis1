<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('RRP','Importdatei');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Form->DebugAusgabe(1, $_POST);
	$Form->DebugAusgabe(1, $_GET);
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15000 = $AWISBenutzer->HatDasRecht(15000);
	if($Recht15000==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	$Form->SchreibeHTMLCode('<form name="frmReifenpflegeImport" action="./reifenpflege_Main.php?cmdAktion=Import" method="POST"  enctype="multipart/form-data">');
	$Form->Formular_Start();
		
	if (isset($_POST['cmdImport_x']))
	{
		//********************************
		// Datenimport
		//********************************
		if(isset($_FILES['IMPORT']['name']))
		{
			include 'reifenpflege_importieren.php';
		}

	    $Form->Formular_Ende();				
		
		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    	$Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
	    $Form->SchaltflaechenEnde();
	}
	else
	{
		if(!empty($_FILES))
		{
			if(($fd = fopen($_FILES['IMPORT']['tmp_name'],'r'))!==false)
			{
				$Zeile = fgets($fd);
			}
			else
			{
				$Form->ZeileStart();
				$Form->Hinweistext(str_replace('$1','CSV',$AWISSprachKonserven['FEHLER']['err_UngueltigesUploadDateiFormat']),'Warnung','');
				$Form->ZeileEnde();
			}
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['Importdatei'].':', 120);
		$Form->Erstelle_DateiUpload('IMPORT',300,30,10000000,'');
		$AWISCursorPosition='IMPORT';
		$Form->ZeileEnde();
		
	    $Form->Formular_Ende();				
		
		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	    if (($Recht15000&32)!=0)
	    {
	    	$Form->Schaltflaeche('image', 'cmdImport', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
	    }
	    $Form->SchaltflaechenEnde();
	    
	}	
	
    
    $Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (Exception $ex)
{

}

?>