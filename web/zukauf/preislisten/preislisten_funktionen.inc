<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class preislisten_funktionen
{
    public $Form;
    public $DB;
    public $DBi;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht10070;
    public $Recht10071;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeug;


    function __construct($Benutzer='')
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DBi = awisDatenbank::NeueVerbindung('AWIS_IMPORT');
        $this->DB->Oeffnen();
        $this->DBi->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht10070 = $this->AWISBenutzer->HatDasRecht(10070);
        $this->Recht10071 = $this->AWISBenutzer->HatDasRecht(10071);
        $this->_EditRecht = (($this->Recht10070 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_ZPL'));
        $this->AWISWerkzeug = new awisWerkzeuge();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('ZPL', '%');
        $TextKonserven[] = array('ZPP', '%');
        $TextKonserven[] = array('AST', 'AST_ATUNR');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'BedingungHinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Dateiname');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_Mobilitaetsgarantie');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_ZPL', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht10070 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'ZPL'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }



    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';

        if ($AWIS_KEY1 != 0) {
            $Bedingung .= ' AND ZPL_KEY = ' . $this->DBi->WertSetzen('ZPL', 'N0', $AWIS_KEY1);
            return $Bedingung;
        }

        if (isset($this->Param['ZPL_BEZEICHNUNG']) and $this->Param['ZPL_BEZEICHNUNG'] != '') {
            $Bedingung .= ' AND ZPL_BEZEICHNUNG ' . $this->DBi->LikeOderIst($this->Param['ZPL_BEZEICHNUNG'], awisDatenbank::AWIS_LIKE_UPPER, 'ZPL');
        }

        if (isset($this->Param['ZPL_LIE_NR']) and $this->Param['ZPL_LIE_NR'] != '') {
            $Bedingung .= ' AND ZPL_LIE_NR <= ' . $this->DBi->WertSetzen('ZPL', 'T', $this->Param['ZPL_LIE_NR']);
        }



        return $Bedingung;
    }

    public function BedingungErstellenPruefProgs(){
        global $AWIS_KEY2;
        $Bedingung = '';


        return $Bedingung;
    }

    public function getZukaufpreislistenstatusText($ZPS_ID){
        $SQL = 'select ZPS_TEXT from ZUKAUFPREISLISTENSTATUS where ZPS_ID = '.$this->DBi->WertSetzen('ZPS','Z',$ZPS_ID);

        return $this->DBi->RecordSetOeffnen($SQL,$this->DBi->Bindevariablen('ZPS'))->FeldInhalt(1);
    }


    public function getZukaufpreislistenProtokoll($ZPL_KEY){
        $SQL = 'select ZPL_PROTOKOLL from ZUKAUFPREISLISTEN where ZPL_KEY = '.$this->DBi->WertSetzen('ZPS','Z',$ZPL_KEY);

        return $this->DBi->RecordSetOeffnen($SQL,$this->DBi->Bindevariablen('ZPS'))->FeldInhalt(1);
    }

    public function addZukaufpreislistenProtokoll($ZPL_KEY,$ZPS_KEY, $StatusSchreiben = false){
        $Eintrag = $this->getZukaufpreislistenProtokoll($ZPL_KEY);
        $Eintrag .= PHP_EOL . '[' . date('d.m.Y H:i:s') . ']'. " - " . $this->getZukaufpreislistenstatusText($ZPS_KEY);
        $SQL = 'Update ZUKAUFPREISLISTEN set ZPL_PROTOKOLL = ' . $this->DBi->WertSetzen('ZPL','T',$Eintrag);
        $SQL .= ',ZPL_USER = ' .$this->DBi->WertSetzen('ZPL','T',$this->AWISBenutzer->BenutzerName());
        if($StatusSchreiben){
            $SQL .= ',ZPL_STATUS = ' .$this->DBi->WertSetzen('ZPL','Z',$ZPS_KEY);
        }
        $SQL .= ',ZPL_USERDAT = sysdate';
        $SQL .= ' WHERE ZPL_KEY = ' . $this->DBi->WertSetzen('ZPL','Z',$ZPL_KEY);

        $this->DBi->Ausfuehren($SQL,'',true,$this->DBi->Bindevariablen('ZPL'));

    }


}