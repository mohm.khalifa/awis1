<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LGM', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Zugriffsrechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Liste', 'lst_JaNein');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht605 = $AWISBenutzer->HatDasRecht(605);
    if (($Recht605 & 128) == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Form->DebugAusgabe(1, $Recht605);
    // Daten ermitteln
    $SQL = 'SELECT *';
    $SQL .= ' FROM LIEFERANTENAPZUGRIFFSGRPBEN ';
    $SQL .= ' LEFT OUTER JOIN Benutzer ON lgm_xbn_key = xbn_key';
    $SQL .= ' WHERE lgm_lzg_key = 0' . $AWIS_KEY1;
    if (isset($_GET['LGM_KEY'])) {
        $AWIS_KEY2 = $DB->FeldInhaltFormat('N0', $_GET['LGM_KEY']);
        $SQL .= ' AND LGM_KEY = ' . $AWIS_KEY2;
    }
    $SQL .= ' ORDER BY xbn_name, xbn_vorname';

    $rsLGM = $DB->RecordSetOeffnen($SQL);

    if ($rsLGM->AnzahlDatensaetze() > 1 OR $AWIS_KEY2 == 0)                        // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();

        if ((intval($Recht605) & 128) != 0) {
            $Icons[] = array('new', './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&Seite=Mitarbeiter&LZG_KEY=' . $AWIS_KEY1 . '&LGM_KEY=-1');
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&Seite=Mitarbeiter' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=LGM_XBN_KEY' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LGM_XBN_KEY'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LGM']['LGM_XBN_KEY'], 350, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&Seite=Mitarbeiter' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&Sort=LGM_AENDERN' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LGM_AENDERN'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LGM']['LGM_AENDERN'], 150, '', $Link);
        $Form->ZeileEnde();

        $JaNeinListe = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
        foreach ($JaNeinListe as $ID => $Wert) {
            $Wert = explode('~', $Wert);
            $JaNeinAnzeige[$Wert[0]] = $Wert[1];
        }
        $DS = 0;
        while (!$rsLGM->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            if (intval($Recht605 & 128) > 0)    // Ändernrecht
            {
                $Icons[] = array(
                    'edit',
                    './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&Seite=Mitarbeiter&LZG_KEY=' . $AWIS_KEY1 . '&LGM_KEY=' . $rsLGM->FeldInhalt('LGM_KEY')
                );
                $Icons[] = array('delete', './lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&Seite=Mitarbeiter&LZG_KEY=' . $AWIS_KEY1 . '&Del=' . $rsLGM->FeldInhalt('LGM_KEY'));
            }
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));
            $Form->Erstelle_ListenFeld('XBN_NAME', $rsLGM->FeldInhalt('XBN_NAME') . ' ' . $rsLGM->FeldInhalt('XBN_VORNAME'), 0, 350, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('LGM_AENDERN', $JaNeinAnzeige[$rsLGM->FeldInhalt('LGM_AENDERN')], 0, 150, false, ($DS % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsLGM->DSWeiter();
            $DS++;
        }

        $Form->Formular_Ende();
    }            // Eine einzelne Adresse
    else                                        // Eine einzelne oder neue Adresse
    {
        $Form->Formular_Start();

        $AWIS_KEY2 = $rsLGM->FeldInhalt('LGM_KEY');

        $Form->Erstelle_HiddenFeld('LGM_KEY', $AWIS_KEY2);
        $Form->Erstelle_HiddenFeld('LGM_LZG_KEY', $AWIS_KEY1);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a class=BilderLink href=./lieferanten_Main.php?cmdAktion=ZugriffsgruppenAP&LGMListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLGM->FeldInhalt('LGM_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLGM->FeldInhalt('LGM_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht605 & 128) != 0);

        if ($AWIS_KEY1 == 0) {
            $EditRecht = true;
        }

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LGM']['LGM_XBN_KEY'] . ':', 150);
        $SQL = 'SELECT xbn_key, xbn_name || \' \' || xbn_vorname AS xbnname ';
        $SQL .= ' FROM Benutzer';
        $SQL .= ' WHERE LOWER(xbn_name) not like \'fil%\'';
        $SQL .= ' AND xbn_status = \'A\'';
        $SQL .= ' ORDER BY xbn_name, xbn_vorname';

        $Form->Erstelle_SelectFeld('LGM_XBN_KEY', $rsLGM->FeldInhalt('LGM_XBN_KEY'), 50, $EditRecht, $SQL, '0~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '');
        $AWISCursorPosition = 'txtLGM_XBN_KEY';
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LGM']['LGM_AENDERN'] . ':', 150);
        $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
        $Form->Erstelle_SelectFeld('LGM_AENDERN', $rsLGM->FeldInhalt('LGM_AENDERN'), 200, $EditRecht, '', '', '', '', '', $Daten);
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('PROBLEM', $ex->getMessage(), 'MELDEN', 3, 200809111043);
} catch (Exception $ex) {
    echo 'allg. Fehler:' . $ex->getMessage();
}
?>