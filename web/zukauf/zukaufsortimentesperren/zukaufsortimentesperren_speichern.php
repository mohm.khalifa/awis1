<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	//***********************************************************************************
	//** Bestellungen �ndern / anlegen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZSS_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('ZSS','ZSS_%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtZSS_KEY'];

		$SQL = 'SELECT * FROM ZUKAUFSORTIMENTESPERREN WHERE ZSS_KEY = '.$DB->WertSetzen('ZSS','T',$AWIS_KEY1);
		$rsZSS = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ZSS'));
		if($rsZSS->EOF())
		{
			$Fehler = '';
			$Pflichtfelder = array('ZSS_DATUMVOM','ZSS_DATUMBIS');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZSS'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$SQL = 'INSERT INTO ZUKAUFSORTIMENTESPERREN';
			$SQL .= '(ZSS_ZLH_KEY,';
            $SQL .= ' ZSS_SORTIMENT,';
            $SQL .= ' ZSS_DATUMVOM, ';
            $SQL .= ' ZSS_DATUMBIS,';
			$SQL .= ' ZSS_USER, ';
            $SQL .= ' ZSS_USERDAT,';
            if(isset($_POST['txtZSS_XXX_KEY']) and $_POST['txtZSS_XXX_KEY'] != '') {
                $SQL .= ' ZSS_XXX_KEY,';
                $SQL .= ' ZSS_XTN_KUERZEL,';
            }
            $SQL .= ' ZSS_LIE_NR,';
            $SQL .= ' ZSS_ZAL_BESTELLNUMMER,';
            $SQL .= ' ZSS_BEZEICHNUNG';
			$SQL .= ')VALUES(';

			$SQL .= ' ' . $DB->WertSetzen('ZSS','T',$_POST['txtZSS_ZLH_KEY'],true);
			$SQL .= ',' . $DB->WertSetzen('ZSS','T',$_POST['txtZSS_SORTIMENT'],true);
			$SQL .= ',' . $DB->WertSetzen('ZSS','D',$_POST['txtZSS_DATUMVOM'],true);
			$SQL .= ',' . $DB->WertSetzen('ZSS','D',$_POST['txtZSS_DATUMBIS'],true);		// in das richtige Feld speichern!!
			$SQL .= ',' . $DB->WertSetzen('ZSS','T',$AWISBenutzer->BenutzerName());
			$SQL .= ',SYSDATE';
            if(isset($_POST['txtZSS_XXX_KEY']) and $_POST['txtZSS_XXX_KEY'] != '') {
                $SQL .= ',' . $DB->WertSetzen('ZSS', 'T', $_POST['txtZSS_XXX_KEY'], true);
                $SQL .= ',' . $DB->WertSetzen('ZSS', 'T', $_POST['txtZSS_XTN_KUERZEL'], true);
            }
            $SQL .= ',' . $DB->WertSetzen('ZSS','T',$_POST['txtZSS_LIE_NR'],true);
            $SQL .= ','.$DB->WertSetzen('ZSS','T',$_POST['txtZSS_ZAL_BESTELLNUMMER'],true);
            $SQL .= ',' . $DB->WertSetzen('ZSS','T',$_POST['txtZSS_BEZEICHNUNG'],true);
			$SQL .= ')';

			$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ZSS'));

			$SQL = 'SELECT seq_ZSS_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$ZSS_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$ZSS_KEY;
		}
		else
		{
                $SQL  ='UPDATE ZUKAUFSORTIMENTESPERREN';
                $SQL .=' SET ';
                $SQL .='  ZSS_ZLH_KEY     = '. $DB->WertSetzen('ZSS','T',$_POST['txtZSS_ZLH_KEY'],true);
                $SQL .=' , ZSS_SORTIMENT   = '. $DB->WertSetzen('ZSS','T',$_POST['txtZSS_SORTIMENT'],true);
                $SQL .=' , ZSS_DATUMVOM    = '. $DB->WertSetzen('ZSS','D',$_POST['txtZSS_DATUMVOM'],true);
                $SQL .=' , ZSS_DATUMBIS    = '. $DB->WertSetzen('ZSS','D',$_POST['txtZSS_DATUMBIS'],true);
                $SQL .=' , ZSS_USER        = '. $DB->WertSetzen('ZSS','T',$AWISBenutzer->BenutzerName(),true);
                $SQL .=' , ZSS_USERDAT     = sysdate';
                if(isset($_POST['txtZSS_XTN_KUERZEL']) and $_POST['txtZSS_XTN_KUERZEL'] != '') {
                    $SQL .=' , ZSS_XXX_KEY     = '. $DB->WertSetzen('ZSS','T',$_POST['txtZSS_XXX_KEY'],true);
                    $SQL .=' , ZSS_XTN_KUERZEL = '. $DB->WertSetzen('ZSS','T',$_POST['txtZSS_XTN_KUERZEL'],true);
                }
                else{
                    $SQL .=' , ZSS_XXX_KEY     = null';
                    $SQL .=' , ZSS_XTN_KUERZEL = null';
                }
                $SQL .=' , ZSS_LIE_NR      = '. $DB->WertSetzen('ZSS','T',$_POST['txtZSS_LIE_NR'],true);
                $SQL .=' , ZSS_ZAL_BESTELLNUMMER     = '. $DB->WertSetzen('ZSS','T',$_POST['txtZSS_ZAL_BESTELLNUMMER'],true);
                $SQL .=' , ZSS_BEZEICHNUNG = '. $DB->WertSetzen('ZSS','T',$_POST['txtZSS_BEZEICHNUNG'],true);
                $SQL .=' WHERE ZSS_KEY       = '. $DB->WertSetzen('ZSS','N0',$AWIS_KEY1,true);

                $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ZSS'));
		}
	}
}
catch (awisException $ex)
{
    $Information = '<br>Zeile: ' .$ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() .'<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
	$Form->Fehler_Anzeigen('SpeicherFehler',$Information,'WIEDERHOLEN',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
    $Information = 'Zeile: ' .$ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $Form->Fehler_Anzeigen('SpeicherFehler',$Information,'WIEDERHOLEN',-2);
}
?>