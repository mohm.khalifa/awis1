<?php
/**
 * L�schen von Informationen auf der Filialinfo
 *
 * @author Sacha Kerres
 * @version 20110
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','VerbindungMFILoesen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$AWIS_KEY1 = 0;

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

	$Frage = $TXT_AdrLoeschen['Wort']['WirklichLoeschen'];

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtPAF_KEY']))
		{
			$Tabelle = 'PAF';
			$Key=$_POST['txtPAF_KEY'];
			$PAFKey=$_POST['txtPAF_KEY'];
	
			$AWIS_KEY1 = $Key;
	
			$Felder=array();
	
			$Felder[]=array($Form->LadeTextBaustein('PAF','PAF_DATUM'),$_POST['txtPAF_DATUM']);
			$Felder[]=array($Form->LadeTextBaustein('PAF','PAF_BEZEICHNUNG'),$_POST['txtPAF_BEZEICHNUNG']);
		}
		elseif(isset($_POST['txtPFI_KEY']))
		{
			$Tabelle = 'PFI';
			$Key=$_POST['txtPFI_KEY'];
	
			$AWIS_KEY1 = $Key;
	
			$Felder=array();
	
			$Felder[]=array($Form->LadeTextBaustein('PFI','PFI_BEZEICHNUNG'),$_POST['txtPFI_BEZEICHNUNG']);
			$Felder[]=array($Form->LadeTextBaustein('PFI','PFI_BAUJAHR'),$_POST['txtPFI_BAUJAHR']);
			$Felder[]=array($Form->LadeTextBaustein('PFI','PFI_KMSTAND'),$_POST['txtPFI_KMSTAND']);
		}
		elseif(isset($_POST['txtPFG_KEY']))
		{
			$Tabelle = 'PFG';
			$Key=$_POST['txtPFG_KEY'];
	
			$AWIS_KEY1 = $Key;
	
			$Felder=array();
	
			$Felder[]=array($Form->LadeTextBaustein('PFG','PFG_BEZEICHNUNG'),$_POST['txtPFG_BEZEICHNUNG']);
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];

					$SQL = 'SELECT ';
					$SQL .= ' FROM ';
					$SQL .= ' WHERE ..._KEY=0'.$Key;
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'','',$AWISSprache),$rsDaten->FeldInhalt(''));
					break;
			}
		}
		else
		{
			switch($_GET['Seite'])
			{
				case 'Artikel':
					$Tabelle = 'PAA';
					$Key=$_GET['Del'];

					$SQL = 'SELECT PAA_AST_ATUNR, PAA_PAP_KEY';
					$SQL .= ' FROM PREISABFRAGENDATENARTIKEL';
					$SQL .= ' WHERE PAA_KEY=0'.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$AWIS_KEY1 = $rsDaten->FeldInhalt('PAA_PAP_KEY');
					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('PAA','PAA_AST_ATUNR'),$rsDaten->FeldInhalt('PAA_AST_ATUNR'));

					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'PAF':
				$SQL = 'DELETE FROM Preisabfragen WHERE PAF_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			case 'PFI':
				$SQL = 'DELETE FROM PREISABFRAGENFAHRZEUGINFOS WHERE PFI_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			case 'PFG':
				$SQL = 'DELETE FROM PREISABFRAGENGRUPPEN WHERE PFG_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=0;
				break;
			case 'PAA':
				$SQL = 'DELETE FROM PREISABFRAGENDATENARTIKEL WHERE PAA_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtAWIS_KEY1'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('preisabfragen_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{
		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./preisabfragen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($Frage);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);
		$Form->Erstelle_HiddenFeld('AWIS_KEY1',$AWIS_KEY1);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>