<?php
/**
 * Pr�fmodul: 	5
 * Zweck:		Dieses Modul pr�ft die Herstellerpreislisten (Zusatz)
 *
 * Parameter:	FELDLISTE
 *
 *
 */

global $con;					// Verbindung zum AWIS Server
global $comcon;					// Verbindung zum COM Server
global $PreisListe;				// Preislisten-ID
global $TransaktionsID;			// Transaktions-ID
global $rsPTA;					// Testarten
global $PTAZeile;				// Aktuelle Zeile in den Testarten-RS
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;
global $BindeVariablenAusgabe;

set_time_limit(0);

//*****************************************************************************
// Parameter lesen
//*****************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY AND PLP_KEY = PPP_PLP_KEY';
$SQL .= ' WHERE PLP_ID_FPLB=' . $PreisListe;
$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

$Params=array();

$rsPAR = awisOpenRecordset($comcon,$SQL);
$rsPARZeilen = $awisRSZeilen;
if($rsPARZeilen>0)
{
	for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
	{
		$Params[$rsPAR['PTP_PARAMETER'][$PARZeile]]=($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]);
	}
}

//*****************************
// Historie schreiben
//*****************************
$SQL = ' INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' (PPH_PLP_KEY,PPH_TRANSAKTIONSID,PPH_BEGINN,PPH_USER,PPH_STATUS)';
$SQL .= ' VALUES(' . $rsPTA['PLP_KEY'][$PTAZeile] . '';
$SQL .= ', ' . $TransaktionsID ;
$SQL .= ', SYSDATE';
$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
$SQL .= ', 0';
$SQL .= ')';

if(!awisExecute($comcon,$SQL))
{
	echo '<span class=HinweisTex>Fehler: '. $SQL . '</span>';
}
$rsPPH = awisOpenRecordset($comcon,'SELECT MAX(PPH_Key) AS KEY FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $TransaktionsID . ' AND PPH_User=\'' . $AWISBenutzer->BenutzerName() . '\'');
$PPHKey = $rsPPH['KEY'][0];
unset($rsPPH);

$TestErgebnis = 1;	// Erfolg als Standard
$AnzAuff=0;			// Anzahl der Auff�lligkeiten

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);

//###############################################################################
//###############################################################################
//########################   B E G I N N   T E S T S ############################
//###############################################################################
//###############################################################################

$FeldListe = explode(',',strtoupper($Params['FELDLISTE']));
$FeldListeNamenAKT = '';
$FeldListeNamenNEU = '';
$FeldListeNamenAKTDef = '';
$FeldListeNamenNEUDef = '';

foreach($FeldListe AS $Feld)
{
	$FeldListeNamenAKT .= ',  AKT_' . $Feld;
	$FeldListeNamenNEU .= ',  NEU_' . $Feld;
	$FeldListeNamenAKTDef .= ', PAZ_' . $Feld . ' AS AKT_' . $Feld;
	$FeldListeNamenNEUDef .= ', PAZ_' . $Feld . ' AS NEU_' . $Feld;
}

//*****************************************************************************
// TEST 1
//*****************************************************************************

set_time_limit(0);

// Transaktions-ID der aktuellen Daten suchen
$SQL = 'SELECT PLT_TRANSAKTIONSID FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_ID_FPLB=' . $PreisListe . ' AND PLT_VERSION=1 AND ROWNUM=1';
$rsTST = awisOpenRecordset($comcon,$SQL);
$TransaktionsIDAKT = $rsTST['PLT_TRANSAKTIONSID'][0];

$SQL = "BEGIN EXPERIAN_ATU.LOESCHETABELLE ('PLP_AUFFAELLIGKEITEN', 1); END;";

if(!awisExecute($comcon,$SQL,true,false,1))
{
	echo '<br><span class=HinweisText>Fehler bei DROP der TABLE: PLP_AUFFAELLIGKEITEN>'.$SQL.'</span><br>';
	flush();
}

$SQL =  'CREATE TABLE PLP_AUFFAELLIGKEITEN AS ';
$SQL .= ' SELECT AKT.PAZ_IMPDAT AS AKT_IMPDAT, AKT.PAZ_ARTNR AS AKT_ARTNR, NEU.PAZ_ARTNR AS NEU_ARTNR ' . $FeldListeNamenAKT . $FeldListeNamenNEU;
$SQL .= ' FROM ';
$SQL .= ' (SELECT PAZ_IMPDAT, PAZ_ARTNR ' . $FeldListeNamenAKTDef;
$SQL .= '   FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ WHERE PAZ_Version=1 AND PAZ_TRANSAKTIONSID=' . $TransaktionsIDAKT . ') AKT';
$SQL .= ' RIGHT OUTER JOIN ';
$SQL .= ' (SELECT PAZ_ARTNR ' . $FeldListeNamenNEUDef;
$SQL .= '   FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ WHERE PAZ_Version=0 AND PAZ_TRANSAKTIONSID=' . $TransaktionsID . ') NEU';
$SQL .= ' ON NEU.PAZ_ARTNR = AKT.PAZ_ARTNR';

if(!awisExecute($comcon,$SQL,true,false,1))
{
	echo '<br><span class=HinweisText>Fehler bei erstellen der TABLE: PLP_AUFFAELLIGKEITEN>'.$SQL.'</span><br>';
	flush();
}

$SQL = "GRANT SELECT ON PLP_AUFFAELLIGKEITEN TO EXPERIAN_ATU WITH GRANT OPTION";

awisExecute($comcon,$SQL,true,false,1);

/*
$SQL = 'SELECT * FROM PLP_AUFFAELLIGKEITEN WHERE AKT_ARTNR IS NOT NULL';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>: Sonderkonditionsfelder ver�ndert?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;Pr�fe ' . $rsTSTZeilen . ' Datens�tze.<br>';
flush();

for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	if($rsTST['AKT_ARTNR'][$TSTZeile] != '')	// Gab es den Artikel schon in der letzten Liste?
	{
		foreach($FeldListe AS $Feld)
		{
			if($rsTST['AKT_'.$Feld][$TSTZeile] != $rsTST['NEU_'.$Feld][$TSTZeile])
			{
				//echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $Feld . ' wurde f�r ' . $rsTST['NEU_ARTNR'][$TSTZeile] . ' von \'' . $rsTST['AKT_' . $Feld][$TSTZeile] . '\' auf \'' . $rsTST['NEU_' . $Feld][$TSTZeile] . '\' ge�ndert. -><font color=red>Fehler</font>' ;
				//flush();

				$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN,PAU_FELD)';
				$SQL .= ' VALUES(' . $PPHKey;
				$SQL .= ', \'Sonderkonditionsfeld ' . $Feld . ' wurde f�r ' . $rsTST['NEU_ARTNR'][$TSTZeile] . ' von ' . $rsTST['AKT_' . $Feld][$TSTZeile] . ' auf ' . $rsTST['NEU_' . $Feld][$TSTZeile] . ' ge�ndert.\'' ;
				$SQL .= ', 1';
				$SQL .= ', \'' . $rsTST['NEU_ARTNR'][$TSTZeile] . '\'' ;
				$SQL .= ', \'' . $Feld . '\')' ;
				if(!awisExecute($comcon,$SQL))
				{
					echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
				}
				$TestErgebnis = 2;
				$AnzAuff++;
			}
		}
	}
	else
	{
		echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Artikel ' . $rsTST['NEU_ARTNR'][$TSTZeile] . ' wurde neu in die Liste aufgenommen. Wird ignoriert' ;	
	}
}

*/

$BindeVariablenAusgabe=array();
$BindeVariablen=array();
$BindeVariablen[':var_N0_tst_cnt'][6]=0;

$SQL="BEGIN :var_N0_tst_cnt := EXPERIAN_ATU.P_PREISLISTE.FUNC_PLP_MOD_5_T_1(".$PPHKey.",'".strtoupper($Params['FELDLISTE'])."'); END;";

awisExecute($comcon,$SQL,true,false,0,$BindeVariablen);

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>: Sonderkonditionsfelder ver�ndert?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;Pr�fe ' . $BindeVariablenAusgabe[':var_N0_tst_cnt'] . ' Datens�tze.<br>';

if ($BindeVariablenAusgabe[':var_N0_tst_cnt']>0)
{
	$TestErgebnis = 2;
	$AnzAuff=$AnzAuff+$BindeVariablenAusgabe[':var_N0_tst_cnt'];
}	

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);

//*****************************************************************************
// TEST 2
//*****************************************************************************

set_time_limit(0);

/*
$SQL = ' SELECT PAZ.*, PLT.*';
$SQL .= ' FROM ';
$SQL .= ' (SELECT PAZ_ARTNR ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ WHERE PAZ_Version=0 AND PAZ_TRANSAKTIONSID=' . $TransaktionsID . ') PAZ';
$SQL .= ' RIGHT OUTER JOIN ';
$SQL .= ' (SELECT PLT_ARTNR ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_TRANSAKTIONSID=' . $TransaktionsID . ') PLT';
$SQL .= ' ON PAZ_ARTNR = PLT_ARTNR';
$SQL .= ' WHERE PAZ.PAZ_ARTNR IS NULL';

//awis_Debug(1,$SQL);
$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 2</b>: Alle Artikel aus Festpreisliste auch in der Zusatztabelle</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTSTZeilen . ' fehlende Datens�tze in der Zusatz-Liste .<br>';
flush();

for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	//echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile] . ' wurde nicht in der Zusatzliste gefunden. -><font color=red>Fehler</font>' ;
	//flush();

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'' . $rsTST['PLT_ARTNR'][$TSTZeile] . ' wurde nicht in der Zusatzliste gefunden.\'' ;
	$SQL .= ', 2';
	$SQL .= ', \'' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\')' ;
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	$TestErgebnis = 2;
	$AnzAuff++;
}

flush();
unset($rsTST);
*/

$BindeVariablenAusgabe=array();
$BindeVariablen=array();
$BindeVariablen[':var_N0_tst_cnt'][6]=0;

$SQL='BEGIN :var_N0_tst_cnt := EXPERIAN_ATU.P_PREISLISTE.FUNC_PLP_MOD_5_T_2('.$TransaktionsID.','.$PPHKey.'); END;';

awisExecute($comcon,$SQL,true,false,0,$BindeVariablen);

echo '<br><br>&nbsp;&nbsp;<b>TEST 2</b>: Alle Artikel aus Festpreisliste auch in der Zusatztabelle</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;' . $BindeVariablenAusgabe[':var_N0_tst_cnt'] . ' fehlende Datens�tze in der Zusatz-Liste .<br>';

if ($BindeVariablenAusgabe[':var_N0_tst_cnt']>0)
{
	$TestErgebnis = 2;
	$AnzAuff=$AnzAuff+$BindeVariablenAusgabe[':var_N0_tst_cnt'];
}	

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);

//*****************************************************************************
// TEST 3
//*****************************************************************************

set_time_limit(0);

/*
$SQL = ' SELECT PAZ.*, PLT.*';
$SQL .= ' FROM ';
$SQL .= ' (SELECT PAZ_ARTNR ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ WHERE PAZ_Version=0 AND PAZ_TRANSAKTIONSID=' . $TransaktionsID . ') PAZ';
$SQL .= ' LEFT OUTER JOIN ';
$SQL .= ' (SELECT PLT_ARTNR ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_TRANSAKTIONSID=' . $TransaktionsID . ') PLT';
$SQL .= ' ON PAZ_ARTNR = PLT_ARTNR';
$SQL .= ' WHERE PLT.PLT_ARTNR IS NULL';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 3</b>: Alle Artikel aus der Zusatztabelle auch in der Festpreisliste?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTSTZeilen . ' fehlende Datens�tze in der Festpreis-Liste .<br>';
flush();

for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	//echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PAZ_ARTNR'][$TSTZeile] . ' wurde nicht in der Festpreisliste gefunden. -><font color=red>Fehler</font>' ;
	//flush();

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'' . $rsTST['PAZ_ARTNR'][$TSTZeile] . ' wurde nicht in der Festpreisliste gefunden.\'' ;
	$SQL .= ', 3';
	$SQL .= ', \'' . $rsTST['PAZ_ARTNR'][$TSTZeile] . '\')' ;
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	$TestErgebnis = 2;
	$AnzAuff++;
}

flush();
unset($rsTST);
*/

$BindeVariablenAusgabe=array();
$BindeVariablen=array();
$BindeVariablen[':var_N0_tst_cnt'][6]=0;

$SQL='BEGIN :var_N0_tst_cnt := EXPERIAN_ATU.P_PREISLISTE.FUNC_PLP_MOD_5_T_3('.$TransaktionsID.','.$PPHKey.'); END;';

awisExecute($comcon,$SQL,true,false,0,$BindeVariablen);

echo '<br><br>&nbsp;&nbsp;<b>TEST 3</b>: Alle Artikel aus der Zusatztabelle auch in der Festpreisliste?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;' . $BindeVariablenAusgabe[':var_N0_tst_cnt'] . ' fehlende Datens�tze in der Festpreis-Liste .<br>';

if ($BindeVariablenAusgabe[':var_N0_tst_cnt']>0)
{
	$TestErgebnis = 2;
	$AnzAuff=$AnzAuff+$BindeVariablenAusgabe[':var_N0_tst_cnt'];
}	

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);

//*****************************************************************************
// TEST 4
//*****************************************************************************

set_time_limit(0);

$FeldListe = explode(',',$Params['PFLICHTFELDER']);
$FeldListeNamenAKT = '';
$FeldListeNamenNEU = '';
$FeldListeNamenAKTDef = '';
$FeldListeNamenNEUDef = '';
$WHEREListe='';

foreach($FeldListe AS $Feld)
{
	$FeldListeNamenAKT .= ',  AKT_' . $Feld;
	$FeldListeNamenNEU .= ',  NEU_' . $Feld;
	$FeldListeNamenAKTDef .= ', PAZ_' . $Feld . ' AS AKT_' . $Feld;
	$FeldListeNamenNEUDef .= ', PAZ_' . $Feld . ' AS NEU_' . $Feld;
	$WHEREListe .= ' OR NVL(AKT_' . $Feld.',\' \')<>NVL(NEU_' . $Feld.',\' \')';
}

$SQL = "BEGIN EXPERIAN_ATU.LOESCHETABELLE('PLP_AUFFAELLIGKEITEN', 1); END;";

if(!awisExecute($comcon,$SQL,true,false,1))
{
	echo '<br><span class=HinweisText>Fehler bei DROP der TABLE: PLP_AUFFAELLIGKEITEN>'.$SQL.'</span><br>';
	flush();
}

$SQL =  'CREATE TABLE PLP_AUFFAELLIGKEITEN AS ';
$SQL .= ' SELECT AKT.PAZ_ARTNR AS AKT_ARTNR, NEU.PAZ_ARTNR AS NEU_ARTNR ' . $FeldListeNamenAKT . $FeldListeNamenNEU;
$SQL .= ' FROM ';
$SQL .= ' (SELECT PAZ_ARTNR ' . $FeldListeNamenAKTDef;
$SQL .= '   FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ WHERE PAZ_Version=1 AND PAZ_TRANSAKTIONSID=' . $TransaktionsIDAKT . ') AKT';
$SQL .= ' FULL OUTER JOIN ';
$SQL .= ' (SELECT PAZ_ARTNR ' . $FeldListeNamenNEUDef;
$SQL .= '   FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ WHERE PAZ_Version=0 AND PAZ_TRANSAKTIONSID=' . $TransaktionsID . ') NEU';
$SQL .= ' ON NEU.PAZ_ARTNR = AKT.PAZ_ARTNR';
$SQL .= ' WHERE ('.substr($WHEREListe,3) . ')';

if(!awisExecute($comcon,$SQL,true,false,1))
{
	echo '<br><span class=HinweisText>Fehler bei erstellen der TABLE: AWIS.PLP_AUFFAELLIGKEITEN>'.$SQL.'</span><br>';
	flush();	
}

$SQL = "GRANT SELECT ON PLP_AUFFAELLIGKEITEN TO EXPERIAN_ATU WITH GRANT OPTION";

awisExecute($comcon,$SQL,true,false,1);
/*
$SQL = 'SELECT * FROM PLP_AUFFAELLIGKEITEN WHERE NEU_ARTNR IS NOT NULL';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

//awis_Debug(1,$SQL,$rsTST);
echo '<br><br>&nbsp;&nbsp;<b>TEST 4</b>: Alle Pflichtfelder gef�llt?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;Pr�fe ' . $rsTSTZeilen . ' Datens�tze.<br>';
flush();

for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	if($rsTST['NEU_ARTNR'][$TSTZeile] != '')	// Artikel vorhanden
	{
		$FeldListeXML='';
		foreach($FeldListe AS $Feld)
		{
			$FeldListeXML .= '<'.$Feld.'>'.$rsTST['NEU_'.$Feld][$TSTZeile].'</'.$Feld.'>';
			if($rsTST['NEU_'.$Feld][$TSTZeile]=='')
			{
				//echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Artikel: '. $rsTST['NEU_ARTNR'][$TSTZeile].': ' . $Feld . ' ist nicht gef�llt. -><font color=red>Fehler</font>' ;
				//flush();

				$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN,PAU_FELD)';
				$SQL .= ' VALUES(' . $PPHKey;
				$SQL .= ', \'Pflichtfeld ' . $Feld . ' wurde bei ' . $rsTST['NEU_ARTNR'][$TSTZeile] . ' nicht ausgef�llt.\'' ;
				$SQL .= ', 4';
				$SQL .= ', \'' . $rsTST['NEU_ARTNR'][$TSTZeile] . '\'' ;
				$SQL .= ', \'' . $Feld . '\')' ;
				if(!awisExecute($comcon,$SQL))
				{
					echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
				}
				$TestErgebnis = 2;
				$AnzAuff++;
			}
		}
	}
}

flush();
unset($rsTST);
*/


$BindeVariablenAusgabe=array();
$BindeVariablen=array();
$BindeVariablen[':var_N0_tst_cnt'][6]=0;

$SQL="BEGIN :var_N0_tst_cnt := EXPERIAN_ATU.P_PREISLISTE.FUNC_PLP_MOD_5_T_4(".$PPHKey.",'".strtoupper($Params['PFLICHTFELDER'])."'); END;";

awisExecute($comcon,$SQL,true,false,0,$BindeVariablen);

echo '<br><br>&nbsp;&nbsp;<b>TEST 4</b>: Alle Pflichtfelder gef�llt?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;Pr�fe ' . $BindeVariablenAusgabe[':var_N0_tst_cnt'] . ' Datens�tze.<br>';

if ($BindeVariablenAusgabe[':var_N0_tst_cnt']>0)
{
	$TestErgebnis = 2;
	$AnzAuff=$AnzAuff+$BindeVariablenAusgabe[':var_N0_tst_cnt'];
}	

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);

//*****************************************************************************
// TEST 5
//*****************************************************************************

set_time_limit(0);

$BindeVariablenAusgabe=array();
$BindeVariablen=array();
$BindeVariablen[':var_N0_tst_cnt'][6]=0;

$SQL='BEGIN :var_N0_tst_cnt := EXPERIAN_ATU.P_PREISLISTE.FUNC_PLP_MOD_5_T_5('.$PPHKey.'); END;';

awisExecute($comcon,$SQL,true,false,0,$BindeVariablen);

echo '<br><br>&nbsp;&nbsp;<b>TEST 5</b>: Ge�nderte Artikel in Bausatz?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;' . $BindeVariablenAusgabe[':var_N0_tst_cnt'] . ' ge�nderte Artikel in Bausatz.<br>';

if ($BindeVariablenAusgabe[':var_N0_tst_cnt']>0)
{
	$TestErgebnis = 2;
	$AnzAuff=$AnzAuff+$BindeVariablenAusgabe[':var_N0_tst_cnt'];
}	

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);


if($TestErgebnis==1)
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: Keine Fehler gefunden. Bewertung: <font color=green>I.O.</font></div>';
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $AnzAuff . ' Fehler gefunden. Bewertung: <font color=red>Fehler.</font></div>';
}


//###############################################################################
//###############################################################################
//##########################   E N D E   T E S T S ##############################
//###############################################################################
//###############################################################################


//*********************************************
// Historie aktualisieren
//*********************************************
$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' SET PPH_ENDE = SYSDATE, PPH_STATUS = '.$TestErgebnis;
$SQL .= ' WHERE PPH_Key = ' . $PPHKey;
if(!awisExecute($comcon,$SQL))
{
	awisErrorMailLink('PreislistePr�fung',1,$awisDBFehler);
	flush();
}


unset($rsTST);

echo '<hr>';

?>