<?php
/**
 * Formular: 05 - Austritt Mitarbeiter
 * @author Sacha Kerres
 *
 */
$FormularKennung = '005';
$DetailBlock = '';
$DetailBlock110 = '';

$DetailFelder = $Form->NameInArray($_POST, 'txtDETAILS', 2, 0);
$Form->DebugAusgabe(3,$DetailFelder);
$OT_EOL = "\r\n";


//****************************
// Account sperren ab
//****************************

$Block='100';
$Daten='';

if(isset($_POST['txtAccountSperrenAb']) AND $_POST['txtAccountSperrenAb']>=1)
{
    $DetailBlock .= $OT_EOL.'Account sperren ab:'.$OT_EOL;
    
    // '1~sofort','2~Austrittsdatum','3~exaktes Datum'
    switch($_POST['txtAccountSperrenAb'])
    {
        case 1:     // sofort
            $DetailBlock .= '  sofort'.$OT_EOL;
            break;
        case 2:     // Austrittsdatum
            $DetailBlock .= '  Austrittsdatum'.$OT_EOL;
            break;
        case 3:     // Austrittsdatum
            $DetailBlock .= '  exaktes Datum: '.$Form->Format('D',$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-AccountSperrenAb']).$OT_EOL;
            break;
    }
}




//****************************
// Pers. Laufwerk
//****************************

$Block='60';
$Daten='';

if(isset($_POST['txtPersLaufwerk']) AND $_POST['txtPersLaufwerk']>=0)
{
    $DetailBlock .= $OT_EOL.'Daten des pers�nlichen Laufwerks:'.$OT_EOL;
    
    // '1~sofort','2~Austrittsdatum','3~exaktes Datum'
    switch($_POST['txtPersLaufwerk'])
    {
        case 0:     // sofort
            $DetailBlock .= '  l�schen'.$OT_EOL;
            break;
        default:
            $DetailBlock .= '  �bertragen an: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn'].$OT_EOL;
            $DetailBlock .= '  �bertragen ab: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb'].$OT_EOL;
            break;
    }
}




//****************************
// E-Mails des Mitrabeiters
//****************************

$Block='70';
$Daten='';

if(isset($_POST['txtEMails']) AND $_POST['txtEMails']>=0)
{
    $DetailBlock .= $OT_EOL.'Emails des Mitarbeiters: l�schen / �bertragen:'.$OT_EOL;
    
    switch($_POST['txtEMails'])
    {
        case 0:     // sofort
            $DetailBlock .= '  l�schen'.$OT_EOL;
            break;
        default:
            $DetailBlock .= '  �bertragen an: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn'].$OT_EOL;
            $DetailBlock .= '  �bertragen ab: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb'].$OT_EOL;
            break;
    }
}

//****************************
// Weiterleitung der E--Mails des Mitarbeiters
//****************************

$Block='80';
$Daten='';

if(isset($_POST['txtEMailWeiter']) AND $_POST['txtEMailWeiter']>=1)
{
    $DetailBlock .= $OT_EOL.'Weiterleitung eingehender Emails:'.$OT_EOL;
    $DetailBlock .= '  Weiterleitung an: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn'].$OT_EOL;
    $DetailBlock .= '  Weiterleitung ab: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb'].$OT_EOL;
    $DetailBlock .= '  F�r die Dauer von Tagen: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenDauer'].$OT_EOL;
}

//****************************
// Weiterleitung der E--Mails des Mitarbeiters
//****************************

$Block='90';
$Daten='';

if(isset($_POST['txtGoogleDriveuebertragen']) AND $_POST['txtGoogleDriveuebertragen']>=1)
{
    $DetailBlock .= $OT_EOL.'�bertragung von Google Drive Daten:'.$OT_EOL;
    $DetailBlock .= '  Weiterleitung an: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn'].$OT_EOL;
    $DetailBlock .= '  Weiterleitung ab: '.$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb'].$OT_EOL;
}


//****************************
// HW vorhanden
//****************************
if(isset($_POST['txtHWVorhanden']) AND $_POST['txtHWVorhanden']>=1)
{
    $DetailBlock .= $OT_EOL.'Hardware vorhanden: '.($_POST['txtHWVorhanden']==1?'Ja':'Nein').$OT_EOL;
}

//*********************************************************
//* Bemerkung
//*********************************************************
$Block='900';
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']!='')
{
    $DetailBlock .= $OT_EOL.'Allgemeine Hinweise:'.$OT_EOL;
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'].$OT_EOL;
}


//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']!='') {

    $Namelist = '';
    foreach($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn'] as $KEY)
    {
        $Namelist .= ';'.$KEY;
    }
    $Namelist = substr($Namelist,1);
    $DetailBlock110 .= $Namelist;

    $DetailBlock .= $OT_EOL.'Benachrichtigung an:'.$OT_EOL;
    $DetailBlock .= $Namelist. $OT_EOL;
}


?>