<?php
require_once('/daten/include/awisARUA.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $Testsystem = false;
    $Typ = 'LS';
    $Lieferant = '0255';
    $Level = null;

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--jobid':
                            $JobId = $argv[++$i];
                            break;
                    case '--lieferant':
                            $Lieferant = $argv[++$i];
                            break;
                    case '--level':
                            $Level = $argv[++$i];
                            break;
                    case '--typ':
                            $Typ = $argv[++$i];
                            if($Typ!='RE' AND $Typ!='LS')
                            {
                                echo 'Falscher Typ.'.PHP_EOL;
                                die();
                            }
                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --level <Level>                    AWIS Level (ENTW,STAG,PROD).'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                            echo '  --lieferant  <ARUA-Lieferant>      Lieferant laut ARUA Kennung.'.PHP_EOL;
                            echo '  --typ  <Dateityp>                  Dateityp, Werte: LS, RE.'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }
    }


    if($JobId!=0)
    {
        //$Job = new awisJobProtokoll($JobId, 'Starte KSM - Import.');
    }

    $ARUA = new awisARUA($Benutzer);

    $ARUA->DebugLevel($DebugLevel);
    $ARUA->ImportiereDateien($Lieferant, $Typ, $Level);

    if($JobId!=0)
    {
        //$Job->SchreibeLog(1,0,'Ende des Jobs.');
    }
}
catch (Exception $ex)
{
    echo $ex->getMessage().PHP_EOL;
    if($JobId!=0)
    {
        //$Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
    }
}
?>

