<?php

/**
 * Importklasse fuer den DeTeFleet-Job.
 * Ebenfalls enthalten die Pruefung ob der Job laufen darf (hier gibt es
 * Abhaengigkeiten).
 * ************************************************************************
 * Parameter :
 * --- keine ---
 * Rueckgabeparameter:
 * --- keine ---
 * ***********************************************************************
 * Erstellt:
 * 01.05.2011 TR
 * Aenderungen:
 * dd.mm.yyyy XX :
 * ***********************************************************************
 * OFFENE PUNKTE:
 * Der Job muss am Monatsletzten laufen. Urspruengliche Programmierung
 * sah Joblauf am 5.ten des Folgemonats vor --> muss geaendert werden
 */

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('detefleet_funktionen.php');

ini_set('max_execution_time', 0);

date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class detefleet_Import
{
	
    private $_Funktionen;
    private $_DB;
    protected $_AWISBenutzer;
    protected $pfadDETEFLEET = '/daten/daten/DETEFLEET/';
    protected $dateiName ='DETEFLEET.csv';
    protected $kundenID;
    protected $gueltigAb;
    protected $gueltigBis;
    protected $detefleetDaten = array();
    protected $aktZeile;
    protected $flagImport = false;
    protected $preislisteID = '';
    private $_Werkzeug;
    private $monatGueltigkeit = '';
    
    /*
     * Konstruktor
     */
    public function __construct()
    {
        $this->_AWISBenutzer = awisBenutzer::Init('oppl_s');
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Werkzeug = new awisWerkzeuge();
        $this->_Funktionen = new detefleet_funktionen();
        $this->_Funktionen->DebugLevel(999);
    }

    /**
     * am 01.04. und am 01.10. wird eine neue KB-Preisliste erstellt. Erst wenn diese erstellt wurde
     * darf die DTFS-Preisliste erstellt werden. Diese Methode checkt ob fuer den neuen DTFS-Preislisten-Zeitraum
     * eine gueltige KB-Preisliste vorliegt.
     */
    protected function checkKBPreisListe () {

        $SQL = 'select * from';
        $SQL.= ' ( select plp.*';
        $SQL.= ' ,(select arbeitsstatus from EXPGW.EXPGW_EXPORT@COM_DE.ATU.DE where PTS_TRANSAKTIONSID=TRANSAKTIONSID) as Status';
        $SQL.= ' ,(select arbeitsstatus from EXPGW.EXPGW_EXPORT@COM_DE.ATU.DE where PTS_ZUSATZ_TID=TRANSAKTIONSID) as Status_Zusatz';
        $SQL.= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS@COM_DE.ATU.DE PLP';
        $SQL.= ' where pts_id_fplb = 2 and pts_gueltig_von <= add_months(sysdate,1) and pts_gueltig_bis >= add_months(sysdate,1))';
        $SQL.= ' where Status = \'T\' and Status_Zusatz = \'T\'';

        $rsPLP = $this->_DB->RecordSetOeffnen($SQL);

        if ($rsPLP->AnzahlDatensaetze()>0) {
            $this->flagImport = true;
        } else {
            $this->flagImport = false;
        }
    }

    /**
     * prueft ob eine DTFS-Preisliste erstellt werden darf.
     */
    public function pruefeStart()
    {
        $this->flagImport = false;

        $tag = date('d');

        $aktuellesMonat = date('m');
        $this->_Funktionen->debugAusgabe('Aktuelles Monat: ' . $aktuellesMonat);
        $this->_Funktionen->debugAusgabe('Aktueller Tag: ' . $tag);

        // nur zum Monatswechsel schauen ob Preisliste bereits erstellt wurde
        if($tag >= 26 || $tag <= 4) {

            $monatPreisliste = str_pad(($tag<=4?$aktuellesMonat:($tag>=26?$aktuellesMonat+1:'')),2,'0', STR_PAD_LEFT);
            if($monatPreisliste == '13') {
                $monatPreisliste = 1;
            }
            // wenn die Preisliste fuer Januar erstellt wird und wir Ende Dezember haben (also Tag >= 26) dann auf das Jahr 1 addieren
            $jahrPreisliste = ($monatPreisliste=='01'&&$tag>=26?date('Y')+1:date('Y'));

            // pruefen ob schon eine Preisliste erstellt wurde fuer die naechste Periode
            $sql = "  select *";
            $sql .= " from gkpreisliste";
            $sql .= " where gkp_monat = " . $this->_DB->WertSetzen('GKP','T', $jahrPreisliste.$monatPreisliste);

            $rsGKP = $this->_DB->RecordSetOeffnen($sql, $this->_DB->Bindevariablen('GKP'));

            if($rsGKP->AnzahlDatensaetze() == 0) {
                $this->flagImport = true;
                $this->gueltigAb = '06.' . $monatPreisliste . '.' . $jahrPreisliste;
                $this->gueltigBis = '05.' . str_pad(intval($monatPreisliste) +1,2, '0', STR_PAD_LEFT) . '.' . $jahrPreisliste;
                $this->monatGueltigkeit = $jahrPreisliste . $monatPreisliste;

                // pruefen ob eine gueltige KB-Preisliste vorhanden ist.
                $this->checkKBPreisListe();
            } else {
                // keine Preisliste erstellen da bereits eine erstellt wurde
                $this->flagImport = false;
            }

        } else {
            // keine Preisliste erstellen da noch keine Monatswechsel ist
            $this->flagImport = false;
            $this->_Funktionen->debugAusgabe('keine Preisliste erstellen da nohc kein Monatswechsel ist');
        }
    }

    /**
     * Startet den Import und die Erstellung der Preisliste
     */
    public function starteImport() {
        $this->setGKKunde(1);
        $this->importDETEFLEET();
    }

    /**
     * Setzt die Kunden-Nummer
     * @param $kundenID
     */
    public function setGKKunde($kundenID) {
        $this->kundenID = $kundenID;
    }

    /**
     * Getter fuer das ImportFlag
     * @return bool
     */
    public function getFlagImport() {
        return $this->flagImport;
    }

    /**
     * Getter fuer die Preislisten - ID
     * @return string
     */
    public function getPreislisteID() {
        return $this->preislisteID;
    }

    /**
     * Importiert die DTFS-Preislisten-Grunddatei
     */
    public function importDETEFLEET() {
        $SQL = 'INSERT INTO GKPREISLISTE (GKP_GKD_KEY,GKP_GUELTIGAB,GKP_GUELTIGBIS,GKP_MONAT,GKP_USER,GKP_USERDAT) VALUES (';
        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('NO',$this->kundenID,false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->gueltigAb,false);
        $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU',$this->gueltigBis,false);
        $SQL .= ',TO_CHAR('.$this->monatGueltigkeit.')';
        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
        $SQL .= ',SYSDATE';
        $SQL .= ')';

        $this->_DB->Ausfuehren($SQL);
        
        $SQL='SELECT MAX(GKP_KEY) AS GKZ_KEY FROM GKPREISLISTE';

        $rsGKZKEY = $this->_DB->RecordSetOeffnen($SQL);

        $GKZKEY = $rsGKZKEY->FeldInhalt('GKZ_KEY');

        $this->preislisteID = $GKZKEY;

        $fd = null;

        if(($fd = fopen($this->pfadDETEFLEET.$this->dateiName,'r')) === false)
        {
            $this->_Funktionen->debugAusgabe('Fehler beim Oeffnen der Datei');
        }
        else
        {
            while(! feof($fd))
            {
                $this->aktZeile = fgets($fd);
                $this->detefleetDaten = explode(';',$this->aktZeile);

                $SQL = 'INSERT INTO GKPREISLISTEZUORD (Gkz_Gkp_Key, Gkz_Auto, Gkz_Materialnr, GKZ_KURZTEXT,GKZ_PREIS, GKZ_BEMERKUNG,GKZ_USER,GKZ_USERDAT) ';
                $SQL .= ' VALUES (';
                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('NO',$GKZKEY,false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->detefleetDaten[0],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO',$this->detefleetDaten[1],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',str_replace('"','',$this->detefleetDaten[2]),false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2',$this->detefleetDaten[3],false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->detefleetDaten[4],false);
                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                $this->_DB->Ausfuehren($SQL);
            }
            fclose($fd);
        }
    }
}

?>