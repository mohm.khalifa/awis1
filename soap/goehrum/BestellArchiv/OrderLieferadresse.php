<?php

class OrderLieferadresse
{

    /**
     * @var int $AdressNummer
     */
    protected $AdressNummer = null;

    /**
     * @var string $Name1
     */
    protected $Name1 = null;

    /**
     * @var string $Name2
     */
    protected $Name2 = null;

    /**
     * @var string $Land
     */
    protected $Land = null;

    /**
     * @var string $PLZ
     */
    protected $PLZ = null;

    /**
     * @var string $Ort
     */
    protected $Ort = null;

    /**
     * @var string $Strasse
     */
    protected $Strasse = null;

    /**
     * @var string $eMail
     */
    protected $eMail = null;

    /**
     * @param int $AdressNummer
     */
    public function __construct($AdressNummer)
    {
      $this->AdressNummer = $AdressNummer;
    }

    /**
     * @return int
     */
    public function getAdressNummer()
    {
      return $this->AdressNummer;
    }

    /**
     * @param int $AdressNummer
     * @return OrderLieferadresse
     */
    public function setAdressNummer($AdressNummer)
    {
      $this->AdressNummer = $AdressNummer;
      return $this;
    }

    /**
     * @return string
     */
    public function getName1()
    {
      return $this->Name1;
    }

    /**
     * @param string $Name1
     * @return OrderLieferadresse
     */
    public function setName1($Name1)
    {
      $this->Name1 = $Name1;
      return $this;
    }

    /**
     * @return string
     */
    public function getName2()
    {
      return $this->Name2;
    }

    /**
     * @param string $Name2
     * @return OrderLieferadresse
     */
    public function setName2($Name2)
    {
      $this->Name2 = $Name2;
      return $this;
    }

    /**
     * @return string
     */
    public function getLand()
    {
      return $this->Land;
    }

    /**
     * @param string $Land
     * @return OrderLieferadresse
     */
    public function setLand($Land)
    {
      $this->Land = $Land;
      return $this;
    }

    /**
     * @return string
     */
    public function getPLZ()
    {
      return $this->PLZ;
    }

    /**
     * @param string $PLZ
     * @return OrderLieferadresse
     */
    public function setPLZ($PLZ)
    {
      $this->PLZ = $PLZ;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrt()
    {
      return $this->Ort;
    }

    /**
     * @param string $Ort
     * @return OrderLieferadresse
     */
    public function setOrt($Ort)
    {
      $this->Ort = $Ort;
      return $this;
    }

    /**
     * @return string
     */
    public function getStrasse()
    {
      return $this->Strasse;
    }

    /**
     * @param string $Strasse
     * @return OrderLieferadresse
     */
    public function setStrasse($Strasse)
    {
      $this->Strasse = $Strasse;
      return $this;
    }

    /**
     * @return string
     */
    public function getEMail()
    {
      return $this->eMail;
    }

    /**
     * @param string $eMail
     * @return OrderLieferadresse
     */
    public function setEMail($eMail)
    {
      $this->eMail = $eMail;
      return $this;
    }

}
