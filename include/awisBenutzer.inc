<?php
require_once('awisDatenbank.inc');

class awisBenutzer
{
	/**
	 * Rueckgabetyp BOOLEAN fuer die Funktion FilialZugriff
	 *
	 */
	const FILIALZUGRIFF_BOOLEAN = 'B';
	/**
	 * Rueckgabetyp STRING (Komma getrennte Liste) fuer die Funktion FilialZugriff
	 *
	 */
	const FILIALZUGRIFF_STRING = 'S';
	/**
	 * Rueckgabetyp ARRAY fuer die Funktion FilialZugriff
	 *
	 */
	const FILIALZUGRIFF_ARRAY = 'A';

	/**
	 * Rueckgabewert f�r den Benutzernamen: Login
	 *
	 */
	const BENUTZERNAME_LOGIN = 1;
	/**	 * Rueckgabewert f�r den Benutzernamen: Login mit Dom�ne
	 *
	 */
	const BENUTZERNAME_LOGINDOMAIN = 2;
	/**
	 * Rueckgabewert f�r den Benutzernamen: Nachname und Vorname
	 *
	 */
	const BENUTZERNAME_NAMEVORNAME = 3;

	/**
	 * Benutzername
	 *
	 * @var string
	 */
	private $_UserName = '';

	/**
	 * Benutzername im Format NACHNAME VORNAME
	 *
	 * @var string
	 */
	private $_UserVollerName = '';
	/**
	 * Dom�ne (Windows), in der Benutzer ist
	 *
	 * @var string
	 */
	private $_UserDomain = '';

	/**
	 * Interne BenutzerID (XBN_KEY)
	 *
	 * @var int
	 */
	private $_UserID = 0;

	/**
	 * Aktuelle Sprache f�r den Benutzer
	 *
	 * @var string
	 */
	private $_UserSprache = 'DE';

	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;


	/**
	 * Rechte(-cache) des Benutzers
	 *
	 * @var array
	 */
	private $_UserRechte = array();

	/**
	 * KontaktID des Benutzers
	 * @var int
	 */
	private $_KON_KEY = 0;
	/**
	 * Instanz der Klasse
	 *
	 * @var awisBenutzer
	 */
	static $_Instanz = null;


	/**
	 * Instanz der Klasse
	 *
	 * @param string $LoginName
	 * @return awisBenutzer
	 */
	public static function Init($LoginName = '')
	{
	    if($LoginName=='' AND count(self::$_Instanz)>0)
	    {
	        $X = array_slice(self::$_Instanz,0,1,true);
	        $Name = array_keys($X);
	        
	        return self::$_Instanz[$Name[0]];
	    }
	    
	    
		if(!isset(self::$_Instanz[$LoginName]))
		{
			//self::$_Instanz[$LoginName] = new awisBenutzer($LoginName);
			$User = new awisBenutzer($LoginName);
			self::$_Instanz[$User->BenutzerName(1)]=$User;
			return self::$_Instanz[$User->BenutzerName(1)];
		}

		return self::$_Instanz[$LoginName];
	}

	/**
	 * Initialisierung der Klasse
	 *
	 * @param string $LoginName
	 */
	protected function __construct($LoginName='')
	{
		if($LoginName==='')
		{
			if(isset($_SERVER["REMOTE_USER"]))
			{
				$LoginName = strtolower($_SERVER["REMOTE_USER"]);
			}
			elseif(isset($_SERVER['PHP_AUTH_USER']))
			{
				$LoginName = strtolower($_SERVER["PHP_AUTH_USER"]);
			}
		}

		if(strpos($LoginName,'@')===false)		// Alte Anmeldung
		{
			$this->_UserName = $LoginName;
			$this->_UserDomain = '';
		}
		else
		{
			$LoginName = explode('@',$LoginName);
			$this->_UserName = $LoginName[0];
			$this->_UserDomain = $LoginName[1];
		}

		$this->_DB = awisDatenbank::NeueVerbindung('AWIS','awisDatenbankORA');
		$this->_DB->Oeffnen();
		

		// Aktiven Benutzer suchen
		$SQL = 'SELECT XBN_KEY, XBN_NAME, XBN_VORNAME';
		$SQL .= ' FROM MV_BENUTZER_LOGIN ';
		$SQL .= ' WHERE XBL_LOGIN=:var_T_xbl_login';
		//$SQL .= ' WHERE XBL_LOGIN=\'KERRES_S\'';
		
		$SQL .= ' AND XBN_STATUS=\'A\'';
		$BindeVariablen=array();
		$BindeVariablen['var_T_xbl_login']=strtoupper($this->_UserName);
		$rsXBN = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

		
		if($rsXBN->FeldInhalt('XBN_KEY')!='')
		{
			$this->_UserID = $rsXBN->FeldInhalt('XBN_KEY');
			$this->_UserVollerName = trim($rsXBN->FeldInhalt('XBN_NAME').' '.$rsXBN->FeldInhalt('XBN_VORNAME'));
		}
        else        // Benutzer ist nicht aktiv oder hat keinen g�ltigen Login-Namen 
		{
		    throw new Exception('Kein aktiver User: '.(is_array($LoginName)?$LoginName[0]:$LoginName),201502031915);
		}
        
		
		//*************************************************
		// Alle Rechte des Benutzers laden
		// TODO: muss in die View rein!
		//*************************************************
		$SQL = "SELECT XRC_ID, XBA_STUFE FROM AWIS.v_AccountRechte ";
		$SQL .= " WHERE XBL_LOGIN=".$this->_DB->WertSetzen('XRC', 'TU', $this->_UserName);
		$SQL .= " UNION SELECT XRC_ID, XBA_STUFE FROM AWIS.v_AccountRechte ";
		$SQL .= " WHERE XBL_LOGIN=".$this->_DB->WertSetzen('XRC', 'TU', $this->_UserName);
		$SQL .= " ORDER BY XRC_ID";

		$rsXRC = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('XRC'));
		while(!$rsXRC->EOF())
		{
			if(!isset($this->_UserRechte[$rsXRC->FeldInhalt('XRC_ID')]))
			{
				$this->_UserRechte[$rsXRC->FeldInhalt('XRC_ID')]=0;
			}
			$this->_UserRechte[$rsXRC->FeldInhalt('XRC_ID')] |= $rsXRC->FeldInhalt('XBA_STUFE');
			$rsXRC->DSWeiter();
		}

		// Sprache richtig setzen
		$this->_UserSprache = $this->ParameterLesen('AnzeigeSprache');
	}

	/**
    * Liefert die Haupt-EMail Adresse des Benutzers
    * @return string
    */
    public function EMailAdresse($XBN_KEY=null)
    {
		$SQL = 'SELECT KKO_WERT ';
		$SQL .= ' FROM KONTAKTEKOMMUNIKATION ';
		$SQL .= ' INNER JOIN Benutzer ON XBN_KON_KEY = KKO_KON_KEY';
		$SQL .= ' WHERE KKO_KOT_KEY = 7';
		if(is_null($XBN_KEY))
		{
            $SQL .= ' AND XBN_KEY = '.$this->_DB->WertSetzen('KKO', 'N0', $this->_UserID);
		}
		else
		{
		    $SQL .= ' AND XBN_KEY = '.$this->_DB->WertSetzen('KKO', 'N0', $XBN_KEY);
		}
		$rsDaten = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('KKO'));
		if($rsDaten->EOF())
		{
		    // Pr�fen, ob es eine Filiale ist
		    $SQL = 'SELECT XBN_NAME, LAN_CODE, FIL_ID';
		    $SQL .= ' FROM Benutzer';
		    $SQL .= ' INNER JOIN Filialen ON FIL_ID = CAST(XBN_FILIALEN AS numeric(10,0))';
		    $SQL .= ' INNER JOIN LAENDER ON FIL_LAN_WWSKENN = LAN_WWSKENN';
		    $SQL .= ' WHERE XBN_KEY = '.$this->_DB->WertSetzen('XBN', 'N0', $this->_UserID);
		    
		    $rsDaten = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('XBN'));
		    
		    if($rsDaten->EOF())
		    {
                return 'shuttle@de.atu.eu';
		    }
		    elseif(substr(strtolower($rsDaten->FeldInhalt('XBN_NAME')),0,4)=='fil-')
		    {
		        $FilName = explode('-',$rsDaten->FeldInhalt('XBN_NAME'));
		        if(isset($FilName[1]))
		        {
		            // Format: $FIL_ID~P0L4$$@$$LAN_CODE~L$$.atu.eu
		            return strtolower(str_pad($rsDaten->FeldInhalt('FIL_ID'),4,'0',STR_PAD_LEFT).'@'.$rsDaten->FeldInhalt('LAN_CODE').'.atu.eu');
		        }
		        else
		        {
		            return 'shuttle@de.atu.eu';
		        }
		    }
		}
		return $rsDaten->FeldInhalt('KKO_WERT');
	}

	/**
	 * Liefert den aktuellen Benutzernamen
	 *
	 * @param int $Format (1=Login, 2=Login@Dom�ne, ...)
	 * @return string
	 */
	public function BenutzerName($Format = self::BENUTZERNAME_LOGIN)
	{
		switch($Format)
		{
			case self::BENUTZERNAME_LOGIN:
				return $this->_UserName;
			case self::BENUTZERNAME_NAMEVORNAME:
				return $this->_UserVollerName;
			case self::BENUTZERNAME_LOGINDOMAIN:
				return $this->_UserName.'@'.$this->_UserDomain;
		}

		return $this->_UserName . '@' . $this->_UserDomain;
	}


	/**
	 * Funktion liefert den Kontakt-KEY des aktuellen Benutzers
	 *
	 * @return int
	 */
	public function BenutzerKontaktKEY()
	{
	    if($this->_KON_KEY!=0)
	    {
	        return $this->_KON_KEY;
	    }

//	    $SQL = 'SELECT XBN_KON_KEY';
//		$SQL .= ' FROM Benutzer ';
//		$SQL .= ' INNER JOIN BenutzerLogins ON XBN_KEY = XBL_XBN_KEY';
//		$SQL .= ' WHERE XBL_LOGIN=:var_xbl_login';
		
		$SQL = 'SELECT XBN_KON_KEY';
		$SQL .= ' FROM MV_BENUTZER_LOGIN ';
		$SQL .= ' WHERE XBL_LOGIN=:var_xbl_login';
	    $BindeVariablen=array();
		$BindeVariablen['var_xbl_login']=strtoupper($this->_UserName);

		$rsXBN = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

		if($rsXBN->FeldInhalt('XBN_KON_KEY')!='')
		{
		    $this->_KON_KEY = $rsXBN->FeldInhalt('XBN_KON_KEY');
			return $this->_KON_KEY;
		}

		return 0;
	}
	/**
	 * Funktion liefert den eindeutigen Benutzer-Key
	 * Tritt ein Fehler auf, wird 0 geliefert
	 *
	 * @return int
	 */
	public function BenutzerID()
	{
		return $this->_UserID;
	}

	/**
	 * FilialZugriff
	 * Liefert true zurueck, wenn der Benutzer keinen Zugriff auf eine Filiale hat
	 *
	 * @param int $Filiale - Filialid (nur bei Rueckgabe FILIALZUGRIFF_BOOLEAN)
	 * @param string Rueckgabe (Boolean, Filialiste, Array)
	 */
	public function FilialZugriff($Filiale, $Rueckgabe = self::FILIALZUGRIFF_BOOLEAN)
	{
//		$SQL = 'SELECT XBN_FILIALEN FROM Benutzer ';
//		$SQL .= ' INNER JOIN BenutzerLogins ON XBN_KEY = XBL_XBN_KEY ';
//		$SQL .= ' WHERE XBL_LOGIN=:var_xbl_login';
		
		$SQL = 'SELECT XBN_FILIALEN FROM MV_BENUTZER_LOGIN ';
		$SQL .= ' WHERE XBL_LOGIN=:var_xbl_login';
	    $BindeVariablen=array();
		$BindeVariablen['var_xbl_login']=strtoupper($this->_UserName);

		$rsXBN = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		if($rsXBN->FeldInhalt('XBN_FILIALEN')=='' OR $rsXBN->FeldInhalt('XBN_FILIALEN')=='0')
		{
			switch($Rueckgabe)
			{
				case self::FILIALZUGRIFF_BOOLEAN:
					return true;
				case self::FILIALZUGRIFF_STRING:
					return '';
				case self::FILIALZUGRIFF_ARRAY:
					return array();
			}
		}
		else
		{
			$Liste = explode(',',$rsXBN->FeldInhalt('XBN_FILIALEN'));
			switch($Rueckgabe)
			{
				case self::FILIALZUGRIFF_BOOLEAN:
					foreach($Liste AS $FilId)
					{
						if($FilId == $Filiale)
						{
							return true;
						}
					}
					return false;
				case self::FILIALZUGRIFF_STRING:
					return (string)$rsXBN->FeldInhalt('XBN_FILIALEN');
				case self::FILIALZUGRIFF_ARRAY:
					return $Liste;
			}
		}

		return false;
	}

	/**
	 * Liefert oder setzt die aktuelle Sprache des Benutzers
	 *
	 * @param string [optional] $NeueSprache
	 * @return string
	 */
	public function BenutzerSprache($NeueSprache='')
	{
		if($NeueSprache=='')
		{
			return $this->_UserSprache;
		}

		$this->ParameterSchreiben('AnzeigeSprache',$NeueSprache);
		$this->_UserSprache = $NeueSprache;

		return $this->_UserSprache;
	}

	/**
	 * Liefert einen Programmparameter
	 *
	 * @param string $ParameterName
	 * @return string
	 */
	public function ParameterLesen($ParameterName, $NeuLesen=false)
	{
	    if(session_id()=='')
	    {
	    	session_start();
	    }
	    $Kodiert = 0;

	    $SQL = "SELECT DISTINCT XBP_WERT, XBL_LOGIN, XPP_ID, XPP_KODIERT from AWIS.V_Benutzerparameter";
		$SQL .= " where XBN_KEY = :var_N0_xbn_key";
		//$SQL .= " where XBN_KEY = 0".(int)$this->_UserID;
	    $SQL .= " and XPP_BEZEICHNUNG = :var_T_xpp_bezeichnung";
	    //$SQL .= " and XPP_BEZEICHNUNG = ".$this->_DB->FeldInhaltFormat('T',(string)$ParameterName);
	    $SQL .= " UNION ALL SELECT DISTINCT XPP_DEFAULT, 'ZZZZZZZZZZ', XPP_ID, XPP_KODIERT from AWIS.ProgrammParameter";
	    $SQL .= " WHERE XPP_BEZEICHNUNG = :var_T_xpp_bezeichnung";
	    //$SQL .= " WHERE XPP_BEZEICHNUNG = ".$this->_DB->FeldInhaltFormat('T',(string)$ParameterName);
	    $SQL .= " ORDER BY XBL_LOGIN";

	    $BindeVariablen=array();
		$BindeVariablen['var_N0_xbn_key']=(int)$this->_UserID;
		$BindeVariablen['var_T_xpp_bezeichnung']=(string)$ParameterName;

	    $rsParameter = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
	    if(!$rsParameter->EOF())
	    {
	    	// Gibt es den Parameter als Session?
	    	// --> wenn ja, den Session-Wert und nicht den DB Wert liefern
	    	if(isset($_SESSION['XBP_'.$rsParameter->FeldInhalt("XPP_ID")]) AND !$NeuLesen)
	    	{
	    		//awis_Debug(1,$_SESSION['XBP_'.$rsParameter["XPP_ID"][0]],$_SESSION,$rsParameter["XBP_WERT"][0],session_id(),'Session !!');
		    	$ParameterWert = $_SESSION['XBP_'.$rsParameter->FeldInhalt("XPP_ID")];
//	    		return $ParameterWert;
	    	}
			else
			{
	    		$ParameterWert = $rsParameter->FeldInhalt("XBP_WERT");
		    	$_SESSION['XBP_'.$rsParameter->FeldInhalt("XPP_ID")]=$ParameterWert;
			}
	    	$Kodiert = (int)$rsParameter->FeldInhalt('XPP_KODIERT');

//	        return $ParameterWert;
	    }
	    else
	    {
	        throw new awisException('Kein Parameter mit dem Namen "'.$ParameterName.'".',1080717,$SQL,awisException::AWIS_ERR_SYSTEM);
	    }

	    return ($Kodiert==1?base64_decode($ParameterWert):$ParameterWert);
	}

	/**
	 * Benutzerparameter speichern
	 *
	 * @param string $ParameterName
	 * @param string $NeuerWert
	 * @param int [optional] $BenutzerID
	 */
	public function ParameterSchreiben($ParameterName, $NeuerWert, $BenutzerID=0)
	{
		if($BenutzerID==0)
		{
			$BenutzerID=$this->_UserID;
		}
	    //********************************************
	    // Session
	    //********************************************

	    if(session_id()=='')
	    {
	    	session_start();
	    }

	    $Kodiert = 0;
		if(is_numeric($ParameterName))
		{
			$PPID = $ParameterName;
		}
		else
		{
			$SQL = "SELECT XPP_ID, XPP_UEBERSCHREIBBAR, XPP_KODIERT ";
			$SQL .= " FROM AWIS.ProgrammParameter ";
			$SQL .= " WHERE XPP_Bezeichnung=:var_T_xpp_bezeichnung";

			$BindeVariablen=array();
			$BindeVariablen['var_T_xpp_bezeichnung']= $ParameterName;

		    $rsParameter = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		    if($rsParameter->EOF())
		    {
		    	throw new Exception('Parameter '.$ParameterName.' exsistiert nicht!',10807171731);
		    }
		    if($rsParameter->FeldInhalt('XPP_UEBERSCHREIBBAR')==0)
		    {
		    	throw new Exception('Parameter '.$ParameterName.' darf nicht ueberschrieben werden!',10807171732);
		    }
		    $PPID = $rsParameter->FeldInhalt('XPP_ID');
		    $Kodiert = $rsParameter->FeldInhalt('XPP_KODIERT');
		}


	    $BindeVariablen=array();
		$BindeVariablen['var_N0_xpp_id']= $PPID;
		$BindeVariablen['var_xbp_xbn_key']= $BenutzerID;

		$SQL = "SELECT DISTINCT XBP_Wert, XPP_KODIERT, XBL_LOGIN from V_Benutzerparameter";
	    $SQL .= " WHERE (XBP_XBN_KEY = :var_xbp_xbn_key )";
	    $SQL .= " AND XPP_ID = :var_N0_xpp_id";
	    $SQL .= " AND XBL_LOGIN <> 'ZZZZZZZZ'";

	    $rsParameter = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

	    // Als Base64 codierten Text speichern, damit alle Sonderzeichen m�glich sind
	    $NeuerWert = ($Kodiert==1?base64_encode($NeuerWert):$NeuerWert);

	    $BindeVariablen=array();
		$BindeVariablen['var_xbp_wert']=$NeuerWert;
		$BindeVariablen['var_N0_xpp_id']=$PPID;
		$BindeVariablen['var_N0_xbn_key']=$BenutzerID;

	    if(!$rsParameter->EOF())
	    {
			$SQL = "UPDATE AWIS.BenutzerParameter SET XBP_WERT = :var_xbp_wert WHERE XBP_XPP_ID=:var_N0_xpp_id";
			$SQL = $SQL . " AND XBP_XBN_KEY=:var_N0_xbn_key";
	    }
	    else
	    {
			$SQL = "INSERT INTO AWIS.BenutzerParameter(XBP_WERT, XBP_XPP_ID, XBP_XBN_Key)";
			$SQL = $SQL . " VALUES(:var_xbp_wert, :var_N0_xpp_id, :var_N0_xbn_key)";
	    }
		$this->_DB->Ausfuehren($SQL,'',false,$BindeVariablen);

		if($BenutzerID == $this->_UserID)
		{
			// Parameter in der Session �ndern / anlegen
			$_SESSION['XBP_'.$PPID]=$NeuerWert;
		}
	}

	/**
	 * Liefert die aktuelle CSS Datei f�r den aktuellen Benutzer
	 *
	 * @return string
	 */
	public function CSSDatei($Version='')
	{
		$Schema = $this->ParameterLesen('Farbschema',true);
		if($Schema=='_')
		{
			$Schema='';
		}
		if($Version!='')
		{
		    $Version .= '/';
		}
		if(is_file("/daten/web/css/".$Version. strtolower($this->_UserName) . ".css"))
		{
		   return "/css/".$Version . strtolower($this->_UserName) . ".css";
		}
		elseif(is_file('/daten/web/css/'.$Version.'awis_'.$Schema.'.css'))
		{
		   return '/css/'.$Version.'awis_'.$Schema.'.css';
		}
		else
		{
		   return "/css/".$Version."awis.css";
		}
	}

	/**
	 * Liefert die Rechtestufe zu einem Recht des Benutzers
	 *
	 * @param int $RechteID
	 * @return int
	 */
	public function HatDasRecht($RechteID)
	{
		if(isset($this->_UserRechte[$RechteID]))
		{
			return $this->_UserRechte[$RechteID];
		}
		return 0;
	}

	/**
	 * Liest oder setzt eine Benutzeroption
	 *
	 * @param mixed $Option
	 */
	public function BenutzerOption($Option)
	{
		$SQL = 'SELECT XBO_WERT, XBT_FORMAT';
		$SQL .= ' FROM BENUTZEROPTIONEN';
		$SQL .= ' INNER JOIN BenutzerOptionenTypen ON XBT_ID = XBO_XBT_ID';
		$SQL .= ' WHERE XBO_XBN_KEY = :var_N0_XBO_XBN_KEY';

		$BindeVariablen=array();
		$BindeVariablen['var_N0_XBO_XBN_KEY']=$this->_UserID;
		if(is_numeric($Option))
		{
			$SQL .= ' AND XBT_ID = :var_N0_xbt_id';
			$BindeVariablen['var_N0_xbt_id']= $this->_DB->FeldInhaltFormat('N0',$Option,false);
		}
		else
		{
			$SQL .= ' AND XBT_BEZEICHNUNG = :var_T_xbt_bezeichnung';
			$BindeVariablen['var_T_xbt_bezeichnung']= $this->_DB->FeldInhaltFormat('T',$Option,false);
		}

		$rsXBT = $this->_DB->RecordsetOeffnen($SQL,$BindeVariablen);
		if($rsXBT->EOF())
		{
			return false;
		}

		return $this->_DB->FeldInhaltFormat($rsXBT->FeldInhalt('XBT_FORMAT'),$rsXBT->FeldInhalt('XBO_WERT'),true);
	}

	/**
	 * Liefert die Personalnummer eines Benuztzers
	 */
	public function PersonalNummer()
	{
        $SQL = 'SELECT KON_PER_NR ';
		$SQL .= ' FROM KONTAKTE';
		$SQL .= ' INNER JOIN Benutzer ON XBN_KON_KEY = KON_KEY';
		$SQL .= ' WHERE XBN_KEY = :var_N0_xbn_key';
	    $BindeVariablen=array();
		$BindeVariablen['var_N0_xbn_key']=$this->_UserID;

		$rsDaten = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		if($rsDaten->EOF())
		{
			return '';
		}
		return $rsDaten->FeldInhalt('KON_PER_NR');
	}

	/**
	 * Liefert die Kostenstelle eines Benutzers
	 * @param (optional) int	BenutzerKey (0=Aktueller Benutzer)
	 */
	public function Kostenstelle($XBN_KEY=0)
	{
        $SQL = 'SELECT KOSTENSTELLE ';
		$SQL .= ' FROM KONTAKTE';
		$SQL .= ' INNER JOIN Benutzer ON XBN_KON_KEY = KON_KEY';
		$SQL .= ' INNER JOIN Personal_Komplett ON KON_PER_NR = PERSNR';
		$SQL .= ' WHERE XBN_KEY = :var_N0_xbn_key';
	    $BindeVariablen=array();
		$BindeVariablen['var_N0_xbn_key']=($XBN_KEY==0?$this->_UserID:$XBN_KEY);

		$rsDaten = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
		if($rsDaten->EOF())
		{
			return '';
		}
		return $rsDaten->FeldInhalt('KOSTENSTELLE');
	}

	/**
	 * Liefert zu einem Benutzer die Kontaktinformation (falls eine Zuordnung vorhanden ist, sonst false)
	 *
	 * @param int $KOT_KEY
	 * @return mixed
	 */
	public function KontaktInfo($KOT_KEY)
	{
        if($this->BenutzerKontaktKEY()!=0)
        {
            $SQL = 'SELECT KKO_WERT ';
            $SQL .= ' FROM KontakteKommunikation';
            $SQL .= ' WHERE KKO_KON_KEY = :var_N0_kko_kon_key AND KKO_KOT_KEY = :var_N0_kko_kot_key';
            $BindeVariablen=array();
            $BindeVariablen['var_N0_kko_kon_key']=$this->_KON_KEY;
            $BindeVariablen['var_N0_kko_kot_key']=$KOT_KEY;

            $rsKKO = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

            return $rsKKO->FeldInhalt('KKO_WERT');
        }

        return false;
	}

}
?>