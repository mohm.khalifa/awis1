<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PAF','lst_PAF_GRUPPEN');
	$TextKonserven[]=array('PAP','PAP_BEZEICHNUNG');
	$TextKonserven[]=array('MBK','MBK_KLASSIFIZIERUNG');
	$TextKonserven[]=array('MRG','MRG_BEZEICHNUNG');
	$TextKonserven[]=array('MUT','MUT_HER_ID');
	$TextKonserven[]=array('MFR','lst_MFR_RANKING');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','bis');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AlleHersteller');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8001 = $AWISBenutzer->HatDasRecht(8001);
	if($Recht8001==0)
	{
	    awisEreignis(3,1000,'Preisabfragen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->Formular_Start();

	if(isset($_POST['cmdExport_x']))
	{
		require_once('awisPreisabfragen.inc');
		require_once('awisMailer.inc');

		$X = new awisPreisabfragen($AWISBenutzer->BenutzerName(1));
		$X->DebugLevel(0);
		$Klassifizierungen = $Form->NameInArray($_POST, 'txtXXX_MBK_',2);
		foreach($Klassifizierungen AS $Klassifizierung)
		{
			if($_POST[$Klassifizierung]!='')
			{
				$FeldTeile = explode('_',$Klassifizierung);
				$X->SetzeKlassifizierung($FeldTeile[2],$_POST[$Klassifizierung]);
			}
		}

		$X->SetzeFilialGruppe($_POST['txtXXX_GRUPPE']);

		$Rankings = $Form->NameInArray($_POST, 'txtXXX_MRG_VON',2);
		foreach($Rankings AS $Ranking)
		{
			if($_POST[$Ranking]!='')
			{
				$FeldTeile = explode('_',$Ranking);
				$X->SetzeRanking($FeldTeile[3],$_POST[$Ranking],$_POST[str_replace('VON','BIS',$Ranking)]);
			}
		}
		if($_POST['txtHER_ID']!='')
		{
			$X->SetzeHersteller($_POST['txtHER_ID']);
		}

		$X->ErstelleMitbewerberListe($_POST['txtXXX_PAP_KEY']);

		$DateiName = '/tmp/Datenabfrage_'.$_POST['txtXXX_PAP_KEY'].'.csv';

		$Mail = new awisMailer($DB, $AWISBenutzer);
		$Mail->Betreff('Datenselektion Preisabfragen');
		$Mail->Absender('awis@de.atu.eu');
		$Mail->AdressListe(awisMailer::TYP_TO,$AWISBenutzer->EMailAdresse(),false,awisMailer::PRUEFAKTION_KEINE);
		$Mail->Text('Anbei Ihre ausgew&auml;hlten Daten.',awisMailer::FORMAT_HTML);
		$Mail->Anhaenge($DateiName, 'Datenselektion.csv');
		if($Mail->MailSenden()===false)
		{
			$Mail->MailInWarteschlange($AWISBenutzer);
		}
		unlink($DateiName);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Daten werden per E-Mail gesendet.', 500, 'Hinweis');
		$Form->ZeileEnde();
	}

	$Form->SchreibeHTMLCode('<form name=frmPreisabfragen action=./preisabfragen_Main.php?cmdAktion=Datenselektion method=POST enctype="multipart/form-data">');

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;

	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PAP']['PAP_BEZEICHNUNG'].':',200);
	$SQL = 'SELECT PAP_KEY, PAP_BEZEICHNUNG';
	$SQL .= ' FROM PREISABFRAGENARTIKELPAKETE';
	$SQL .= ' ORDER BY 2';
	$Form->Erstelle_SelectFeld('XXX_PAP_KEY','','400:390',true,$SQL);
	$AWISCursorPosition = 'txtXXX_PAP_KEY';
	$Form->ZeileEnde();


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MBK']['MBK_KLASSIFIZIERUNG'], 800, 'Ueberschrift');
	$Form->ZeileEnde();


	$SQL = 'SELECT MBK_KEY, MBK_KLASSIFIZIERUNG, MBK_KENNUNG';
	$SQL .= ' FROM MITBEWERBERKLASSIFIZIERUNGEN';
	$SQL .= ' ORDER BY MBK_SORTIERUNG, MBK_KLASSIFIZIERUNG';
	$rsMBK = $DB->RecordSetOeffnen($SQL);
	while(!$rsMBK->EOF())
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($rsMBK->FeldInhalt('MBK_KLASSIFIZIERUNG').':',200);
		$Form->Erstelle_TextFeld('XXX_MBK_'.$rsMBK->FeldInhalt('MBK_KEY'),'',10,150,true,'','','','N0');

		if($rsMBK->FeldInhalt('MBK_KEY')==28)
		{
			$Form->Erstelle_TextLabel($AWISSprachKonserven['MUT']['MUT_HER_ID'].':',100);
			$SQL = 'SELECT HER_ID, HER_BEZEICHNUNG';
			$SQL .= ' FROM HERSTELLER';
			$SQL .= ' INNER JOIN MITBEWERBUTYPENTYPEN ON HER_ID = MUT_HER_ID';
			$Form->Erstelle_SelectFeld('HER_ID','','200:190',true, $SQL,'~'.$AWISSprachKonserven['Wort']['AlleHersteller']);
		}

		$Form->ZeileEnde();

		$rsMBK->DSWeiter();
	}

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['MRG']['MRG_BEZEICHNUNG'], 800, 'Ueberschrift');
	$Form->ZeileEnde();


	$SQL = 'SELECT MRG_KEY, MRG_BEZEICHNUNG';
	$SQL .= ' FROM MITBEWERBERRANKINGGRUPPEN';
	$SQL .= ' ORDER BY MRG_BEZEICHNUNG';
	$rsMBK = $DB->RecordSetOeffnen($SQL);
	while(!$rsMBK->EOF())
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($rsMBK->FeldInhalt('MRG_BEZEICHNUNG').':',200);
		//$Form->Erstelle_TextFeld('XXX_MRG_VON_'.$rsMBK->FeldInhalt('MRG_KEY'),'',10,100,true,'','','','N0');
		$Daten = explode("|",$AWISSprachKonserven['MFR']['lst_MFR_RANKING']);
		$Form->Erstelle_SelectFeld('XXX_MRG_VON_'.$rsMBK->FeldInhalt('MRG_KEY'),'','200:190',true,null,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',1,'',$Daten,'','');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['bis'].':',50);
		//$Form->Erstelle_TextFeld('XXX_MRG_BIS_'.$rsMBK->FeldInhalt('MRG_KEY'),'',10,100,true,'','','','N0');
		$Form->Erstelle_SelectFeld('XXX_MRG_BIS_'.$rsMBK->FeldInhalt('MRG_KEY'),'','200:190',true,null,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',1,'',$Daten,'','');
		$Form->ZeileEnde();

		$rsMBK->DSWeiter();
	}

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PAP']['PAP_BEZEICHNUNG'].':',200);
/*
	$SQL = 'SELECT PAP_KEY, PAP_BEZEICHNUNG';
	$SQL .= ' FROM PREISABFRAGENARTIKELPAKETE';
	$SQL .= ' ORDER BY 2';

	$Gruppen = explode("|",$AWISSprachKonserven['PAF']['lst_PAF_GRUPPEN']);
	$Form->Erstelle_SelectFeld('XXX_GRUPPE',1,'100:90',true,'','','','','',$Gruppen,'');
*/
	$SQL = 'SELECT DISTINCT FIF_WERT AS KEY, FIF_WERT AS ANZEIGE';
	$SQL .= ' FROM FILIALINFOS';
	$SQL .= ' WHERE FIF_FIT_ID = 150';
	$SQL .= ' ORDER BY 2';
	$Form->Erstelle_SelectFeld('XXX_GRUPPE','','200:190',true, $SQL);


	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdExport', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export'], '');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201615");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201614");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>