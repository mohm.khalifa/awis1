<?php
global $AWISCursorPosition;
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KundenKfzSuche','%');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht13001 = $AWISBenutzer->HatDasRecht(13001);
	if($Recht13001==0)
	{
	    awisEreignis(3,1000,'KundenKfzSuche',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	echo "<form name=frmSuche method=post action=./kundenkfzsuche_Main.php?cmdAktion=Details>";

	/**********************************************
	* Eingabemaske
	***********************************************/
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KundenKfzSuche'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzSuche']['KFZ'].':',170);
	$Form->Erstelle_TextFeld('*KFZ',($Param['SPEICHERN']=='on'?$Param['KFZ']:''),20,200,true);
	$AWISCursorPosition='sucKFZ';
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzSuche']['VERTRAGSNUMMER'].':',170);
	$Form->Erstelle_TextFeld('*VERTRAGSNUMMER',($Param['SPEICHERN']=='on'?$Param['VERTRAGSNUMMER']:''),20,200,true);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzSuche']['LEASINGNUMMER'].':',170);
	$Form->Erstelle_TextFeld('*LEASINGNUMMER',($Param['SPEICHERN']=='on'?$Param['LEASINGNUMMER']:''),20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzSuche']['FAHRER_ID'].':',170);
	$Form->Erstelle_TextFeld('*FAHRER_ID',($Param['SPEICHERN']=='on'?$Param['FAHRER_ID']:''), 20, 200, true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KundenKfzSuche']['IMPDAT'].':',170);
	$Form->Erstelle_TextFeld('*IMPDAT',($Param['SPEICHERN']=='on'?$Param['IMPDAT']:''),20,200,true,'','','','D');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',170);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();


	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071619");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071620");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>