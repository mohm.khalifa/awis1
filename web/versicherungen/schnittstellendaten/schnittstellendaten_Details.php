<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $VGD;

$DetailAnsicht = false;
$POSTsBenutzen = true;

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$AWIS_KEY1 = '';
if (isset($_GET['KEY'])) {
    $AWIS_KEY1 = $_GET['KEY'];
} elseif (isset($_POST['txtKEY'])) {
    $AWIS_KEY1 = $_POST['txtKEY'];
}

if (isset($_POST['cmdDSNeu_x'])) { //Neuanlage?
    $AWIS_KEY1 = -1;
    $_POST = array(); //Falls in nem bestehenden Datensatz auf "Neu" geklickt wurde, muss der POST geleert werden, da diese dann in die Felder geschrieben werden
}
if($AWIS_KEY1!=''){
    $VGD->Param['KEY'] = $AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..
}

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $VGD->Param = array();
    $VGD->Param['VGD_KFZ_KZ'] = $_POST['sucVGD_KFZ_KZ'];
    $VGD->Param['QUELLE'] = $_POST['sucVGD_QUELLE'];

    $VGD->Param['KEY'] = '';
    $VGD->Param['WHERE'] = '';
    $VGD->Param['ORDER'] = '';
    $VGD->Param['BLOCK'] = 1;
    $VGD->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    $POSTsBenutzen = false;
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include('./schnittstellendaten_speichern.php');
}elseif(isset($_GET['Liste'])){
    //Liste anzeigen
    $AWIS_KEY1 = '';
}
else{ //User hat den Reiter gewechselt.
    $AWIS_KEY1 = $VGD->Param['KEY'];
}

//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************

if (!isset($_GET['Sort'])) {
    if (isset($VGD->Param['ORDER']) AND $VGD->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $VGD->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY ERSTELLDATUM DESC';
        $VGD->Param['ORDER'] = ' ERSTELLDATUM DESC ';
    }
} else {
    $VGD->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $VGD->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $VGD->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************
$SQL = 'select';
$SQL .= '   KEY';
$SQL .= ',  DATEINAME';
$SQL .= ',  ORDNUNGSNR';
$SQL .= ',  ERSTELLDATUM';
$SQL .= ',  AX_VORGANGNR';
$SQL .= ',  KFZ_KZ';
$SQL .= ',  SB';
$SQL .= ',  USER_';
$SQL .= ',  USERDAT';
$SQL .= ',  (';
$SQL .= '       select b.vve_bezeichnung';
$SQL .= '       from';
$SQL .= '       (';
$SQL .= '           select vve_bezeichnung, vve_vunummer_kurz';
$SQL .= '           from versversicherungen';
$SQL .= '           union';
$SQL .= '           select vve_bezeichnung, to_char(vdv_vu_nummer)';
$SQL .= '           from versdirektvermittlungref inner join versversicherungen';
$SQL .= '           on vdv_vers_nr = vve_versnr';
$SQL .= '       ) b';
$SQL .= '       where a.VUNUMMER = b.VVE_VUNUMMER_KURZ';
$SQL .= '       and rownum = 1';
$SQL .= '   ) as VERS_BEZ';
$SQL .= ',  VUNUMMER';
$SQL .= ',  VORSTABZUGKZ';
$SQL .= ',  QUELLTABELLE';
$SQL .= ',  STATUS';
$SQL .= ', row_number() over (' . $ORDERBY . ') as ZeilenNr';
$SQL .= ' from';
$SQL .= ' (';
$SQL .= ' select ';
$SQL .= '   VGD_KEY as KEY';
$SQL .= ',  \'\' as DATEINAME';
$SQL .= ',  VGD_CASEID as ORDNUNGSNR';
$SQL .= ',  VGD_USERDAT as ERSTELLDATUM';
$SQL .= ',  \'\' as AX_VORGANGNR';
$SQL .= ',  VGD_KFZ_KZ as KFZ_KZ';
$SQL .= ',  to_char(VGD_SB) as SB';
$SQL .= ',  VGD_USER as USER_';
$SQL .= ',  VGD_USERDAT as USERDAT';
$SQL .= ',  VGD_VERS_VUNR as VUNUMMER';
$SQL .= ',  -1 as VORSTABZUGKZ';
$SQL .= ',  \'VGD\' as QUELLTABELLE';
$SQL .= ', \' \' as STATUS';
$SQL .= ' from VERSGDVDECKUNGSABFRAGEN ';
$SQL .= ' union all';
$SQL .= ' select ';
$SQL .= '   VBA_KEY as KEY';
$SQL .= ',  VBA_DATEINAME as DATEINAME';
$SQL .= ',  VBA_ORDNUNGSNR as ORDNUNGSNR';
$SQL .= ',  VBA_ERSTELLDATUM as ERSTELLDATUM';
$SQL .= ',  to_char(VBA_AX_VORGANGNR) as AX_VORGANGNR';
$SQL .= ',  VBA_KFZKZ as KFZ_KZ';
$SQL .= ',  substr(VBA_AUFTRAGSBESCHREIBUNG, 72, 4) as SB';
$SQL .= ',  VBA_USER as USER_';
$SQL .= ',  VBA_USERDAT as USERDAT';
$SQL .= ',  VBA_VUNUMMER as VUNUMMER';
$SQL .= ',  VBA_VORSTABZUGKZ as VORSTABZUGKZ';
$SQL .= ',  \'VBA\' as QUELLTABELLE';
$SQL .= ',  CASE WHEN VBA_STATUS IS NULL THEN \'OPEN\' ELSE VBA_STATUS END as STATUS';
$SQL .= ' from VERSBEAUFTRAGUNGEN';
$SQL .= ') a';

if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($AWIS_KEY1 == '') { //Liste?
    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $VGD->Form->Format('N0', $_REQUEST['Block'], false);
        $VGD->Param['BLOCK'] = $Block;
    } elseif (isset($VGD->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $VGD->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $VGD->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $VGD->DB->ErmittleZeilenAnzahl($SQL, $VGD->DB->Bindevariablen('VGD', false));
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $VGD->DB->WertSetzen('VGD', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $VGD->DB->WertSetzen('VGD', 'N0', ($StartZeile + $ZeilenProSeite));
}

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsVGD = $VGD->DB->RecordsetOeffnen($SQL, $VGD->DB->Bindevariablen('VGD', true));
$VGD->Form->DebugAusgabe(1, $VGD->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$VGD->Form->Formular_Start();
echo '<form name=frmschnittstellendaten action=./schnittstellendaten_Main.php?cmdAktion=Details method=POST>';

if ($rsVGD->EOF() and $AWIS_KEY1 == '') { //Nichts gefunden!
    $VGD->Form->Hinweistext($VGD->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsVGD->AnzahlDatensaetze() > 1) {// Liste

    $FeldBreiten = array();
    $FeldBreiten['VGD_ERSTELLDATUM'] = 160;
    $FeldBreiten['VGD_DATEINAME'] = 250;
    $FeldBreiten['VGD_AX_VORGANGNR'] = 180;
    $FeldBreiten['VGD_ORDNUNGSNR'] = 250;
    $FeldBreiten['VGD_KFZ_KZ'] = 150;
    $FeldBreiten['VGD_SB'] = 150;
    $FeldBreiten['VGD_VERSBEZ'] = 250;

    $VGD->Form->ZeileStart();
    $Link = './schnittstellendaten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=ERSTELLDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ERSTELLDATUM'))?'~':'');
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['VGD']['VGD_ERSTELLDATUM'], $FeldBreiten['VGD_ERSTELLDATUM'], '', $Link);

    $Link = './schnittstellendaten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=DATEINAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'DATEINAME'))?'~':'');
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['VGD']['VGD_DATEINAME'], $FeldBreiten['VGD_DATEINAME'], '', $Link);

    $Link = './schnittstellendaten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=AX_VORGANGNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'AX_VORGANGNR'))?'~':'');
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['VGD']['VGD_AX_VORGANGNR'], $FeldBreiten['VGD_AX_VORGANGNR'], '', $Link);

    $Link = './schnittstellendaten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=ORDNUNGSNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ORDNUNGSNR'))?'~':'');
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['VGD']['VGD_ORDNUNGSNR'], $FeldBreiten['VGD_ORDNUNGSNR'], '', $Link);

    $Link = './schnittstellendaten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=KFZ_KZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'KFZ_KZ'))?'~':'');
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['VGD']['VGD_KFZ_KZ'], $FeldBreiten['VGD_KFZ_KZ'], '', $Link);

    $Link = './schnittstellendaten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=SB' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'SB'))?'~':'');
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['VGD']['VGD_SB'], $FeldBreiten['VGD_SB'], '', $Link, '', 'C');

    $Link = './schnittstellendaten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=VERS_BEZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VERS_BEZ'))?'~':'');
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['VGD']['VGD_VERSBEZ'], $FeldBreiten['VGD_VERSBEZ'], '', $Link);

    $VGD->Form->ZeileEnde();

    $DS = 0;
    while (!$rsVGD->EOF()) {
        $HG = ($DS % 2);
        $VGD->Form->ZeileStart();
        $Link = './schnittstellendaten_Main.php?cmdAktion=Details&KEY=0' . $rsVGD->FeldInhalt('KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $VGD->Form->Erstelle_ListenFeld('VGD_ERSTELLDATUM', $rsVGD->FeldInhalt('ERSTELLDATUM'), 0, $FeldBreiten['VGD_ERSTELLDATUM'], false, $HG, '', $Link, 'T', 'L',$rsVGD->FeldInhalt('ERSTELLDATUM'));
        $VGD->Form->Erstelle_ListenFeld('VGD_DATEINAME', $rsVGD->FeldInhalt('DATEINAME'), 0, $FeldBreiten['VGD_DATEINAME'], false, $HG, '', '', 'T', 'L',$rsVGD->FeldInhalt('DATEINAME'));
        $VGD->Form->Erstelle_ListenFeld('VGD_AX_VORGANGNR', $rsVGD->FeldInhalt('AX_VORGANGNR'), 0, $FeldBreiten['VGD_AX_VORGANGNR'], false, $HG, '', '', 'T', 'L',$rsVGD->FeldInhalt('AX_VORGANGNR'));
        $VGD->Form->Erstelle_ListenFeld('VGD_ORDNUNGSNR', $rsVGD->FeldInhalt('ORDNUNGSNR'), 0, $FeldBreiten['VGD_ORDNUNGSNR'], false, $HG, '', '', 'T', 'L',$rsVGD->FeldInhalt('ORDNUNGSNR'));
        $VGD->Form->Erstelle_ListenFeld('VGD_KFZ_KZ', $rsVGD->FeldInhalt('KFZ_KZ'), 0, $FeldBreiten['VGD_KFZ_KZ'], false, $HG, '', '', 'T', 'L',$rsVGD->FeldInhalt('KFZ_KZ'));
        $VGD->Form->Erstelle_ListenFeld('VGD_SB', $rsVGD->FeldInhalt('SB'), 0, $FeldBreiten['VGD_SB'], false, $HG, '', '', 'N0', 'C',$rsVGD->FeldInhalt('SB'));
        $VGD->Form->Erstelle_ListenFeld('VGD_VERSBEZ', $rsVGD->FeldInhalt('VERS_BEZ'), 0, $FeldBreiten['VGD_VERSBEZ'], false, $HG, '', '', 'T', 'L',$rsVGD->FeldInhalt('VERS_BEZ'));

        $VGD->Form->ZeileEnde();

        $rsVGD->DSWeiter();
        $DS++;
    }

    $Link = './schnittstellendaten_Main.php?cmdAktion=Details';
    $VGD->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
} else { //Ein Datensatz
    if($rsVGD->FeldInhalt('KEY')!=''){
        $AWIS_KEY1 = $rsVGD->FeldInhalt('KEY');
    }

    $VGD->Form->Erstelle_HiddenFeld('KEY', $AWIS_KEY1);

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./schnittstellendaten_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $VGD->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVGD->FeldInhalt('USER_'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVGD->FeldInhalt('USERDAT'));
    $VGD->Form->InfoZeile($Felder, '');

    $EditRecht = (($VGD->Recht69000 & 2) != 0);

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_KFZ_KZ'] . ':', 180);
    $VGD->Form->Erstelle_TextFeld('VGD_KFZ_KZ', $rsVGD->FeldOderPOST('KFZ_KZ','',$POSTsBenutzen), 15, 150, false, '', '', '', 'T', '', '');
    $VGD->Form->ZeileEnde();

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_SB'] . ':', 180);
    $Daten = explode("|", $VGD->AWISSprachKonserven['VBA']['VBA_LST_SB']);
    $VGD->Form->Erstelle_SelectFeld('VGD_SB', $rsVGD->FeldOderPOST('SB','',$POSTsBenutzen), '100', $EditRecht, '', '', '', '', '', $Daten, '', '', [], 'height:22px');
    $VGD->Form->ZeileEnde();

    // bei einer Direktvermittlung kann neben der SB auch das Kennzeichen (Vorsteuerabzugsberechtigt) geaendert werden
    if($VGD->Param['QUELLE'] == 2) {// Direktvermittlungen
        $VGD->Form->ZeileStart();
        $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_VORSTABZUG'] . ':', 180);
        $Daten = explode("|", '0~nein|1~ja');
        $VGD->Form->Erstelle_SelectFeld('VBA_VORSTABZUGKZ', $rsVGD->FeldOderPOST('VORSTABZUGKZ', 'T', $POSTsBenutzen), '100', $EditRecht, '', '', '', '', '', $Daten, '', '', [], 'height:22px');
        $VGD->Form->ZeileEnde();
    }

    // bei einer Direktvermittlung kann neben der SB & Vorsteuerabzugsberechtigung auch der Status der Vermittlung ge�ndert werden (auf CLOSED)
    if($VGD->Param['QUELLE'] == 2) {// Direktvermittlungen
        $VGD->Form->ZeileStart();
        $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VBA']['VBA_STATUS'] . ':', 180);
        $Daten = explode("|", 'OPEN~Offen|CLOSED~Geschlossen');
        $VBA_STATUS = $rsVGD->FeldOderPOST('STATUS', 'T', $POSTsBenutzen);
        $VGD->Form->Erstelle_SelectFeld('VBA_STATUS', $VBA_STATUS, '150', $EditRecht, '', '','', '', '', $Daten, '', '', [], 'height:22px');
        $VGD->Form->ZeileEnde();
    }

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_ORDNUNGSNR'] . ':', 180);
    $VGD->Form->Erstelle_TextFeld('VGD_ORDNUNGSNR', $rsVGD->FeldOderPOST('ORDNUNGSNR','',$POSTsBenutzen), 50, 350, false, '', '', '', 'T', '', '');
    $VGD->Form->ZeileEnde();

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_AX_VORGANGNR'] . ':', 180);
    $VGD->Form->Erstelle_TextFeld('VGD_AX_VORGANGNR', $rsVGD->FeldOderPOST('AX_VORGANGNR','',$POSTsBenutzen), 15, 150, false, '', '', '', 'T', '', '');
    $VGD->Form->ZeileEnde();

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_DATEINAME'] . ':', 180);
    $VGD->Form->Erstelle_TextFeld('VGD_DATEINAME', $rsVGD->FeldOderPOST('DATEINAME','',$POSTsBenutzen), 50, 350, false, '', '', '', 'T', '', '');
    $VGD->Form->ZeileEnde();

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_ERSTELLDATUM'] . ':', 180);
    $VGD->Form->Erstelle_TextFeld('VGD_ERSTELLDATUM', $rsVGD->FeldOderPOST('ERSTELLDATUM','',$POSTsBenutzen), 25, 250, false, '', '', '', 'T', '', '');
    $VGD->Form->ZeileEnde();

    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_TextLabel($VGD->AWISSprachKonserven['VGD']['VGD_VERSBEZ'] . ':', 180);
    $VGD->Form->Erstelle_TextFeld('VGD_VERSBEZ', $rsVGD->FeldOderPOST('VERS_BEZ','',$POSTsBenutzen), 40, 350, false, '', '', '', 'T', '', '');
    $VGD->Form->ZeileEnde();

    //*********************************************************
    //* Aenderungshistorie anzeigen wenn eine vorhanden
    //*********************************************************

    $rsXAH = $VGD->Aenderungshistorie->LeseAenderungen($rsVGD->FeldInhalt('QUELLTABELLE'),$AWIS_KEY1);

    $FeldBreitenHistorie = array();
    $FeldBreitenHistorie['XAH_FELD'] = 160;
    $FeldBreitenHistorie['XAH_WERTALT'] = 250;
    $FeldBreitenHistorie['XAH_WERTNEU'] = 180;
    $FeldBreitenHistorie['XAH_USER'] = 250;
    $FeldBreitenHistorie['XAH_USERDAT'] = 150;

    $VGD->Form->FormularBereichStart();
    $VGD->Form->FormularBereichInhaltStart('�nderungshistorie',false);
    $VGD->Form->ZeileStart();
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['XAH']['XAH_FELD'], $FeldBreitenHistorie['XAH_FELD']);
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['XAH']['XAH_WERTALT'], $FeldBreitenHistorie['XAH_WERTALT']);
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['XAH']['XAH_WERTNEU'], $FeldBreitenHistorie['XAH_WERTNEU']);
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['XAH']['XAH_USER'], $FeldBreitenHistorie['XAH_USER']);
    $VGD->Form->Erstelle_Liste_Ueberschrift($VGD->AWISSprachKonserven['XAH']['XAH_USERDAT'], $FeldBreitenHistorie['XAH_USERDAT']);

    $VGD->Form->ZeileEnde();

    $DS = 0;
    while (!$rsXAH->EOF()) {
        $HG = ($DS % 2);
        $VGD->Form->ZeileStart();
        $VGD->Form->Erstelle_ListenFeld('XAH_FELD', $rsXAH->FeldInhalt('XAH_FELD'), 0, $FeldBreitenHistorie['XAH_FELD'], false, $HG, '', '', 'T', 'L',$rsXAH->FeldInhalt('XAH_FELD'));
        if($rsXAH->FeldInhalt('XAH_FELD') == 'VBA_VORSTABZUGKZ') {
            $Daten = array('0' => 'Nein', '1' => 'Ja');
            $VGD->Form->Erstelle_ListenFeld('XAH_WERTALT', $Daten[$rsXAH->FeldInhalt('XAH_WERTALT')], 0, $FeldBreitenHistorie['XAH_WERTALT'], false, $HG, '', '', 'T', 'L',$Daten[$rsXAH->FeldInhalt('XAH_WERTALT')]);
            $VGD->Form->Erstelle_ListenFeld('XAH_WERTNEU', $Daten[$rsXAH->FeldInhalt('XAH_WERTNEU')], 0, $FeldBreitenHistorie['XAH_WERTNEU'], false, $HG, '', '', 'T', 'L',$Daten[$rsXAH->FeldInhalt('XAH_WERTNEU')]);

        } else {
            $VGD->Form->Erstelle_ListenFeld('XAH_WERTALT', $rsXAH->FeldInhalt('XAH_WERTALT'), 0, $FeldBreitenHistorie['XAH_WERTALT'], false, $HG, '', '', 'T', 'L',$rsXAH->FeldInhalt('XAH_WERTALT'));
            $VGD->Form->Erstelle_ListenFeld('XAH_WERTNEU', $rsXAH->FeldInhalt('XAH_WERTNEU'), 0, $FeldBreitenHistorie['XAH_WERTNEU'], false, $HG, '', '', 'T', 'L',$rsXAH->FeldInhalt('XAH_WERTNEU'));
        }
        $VGD->Form->Erstelle_ListenFeld('XAH_USER', $rsXAH->FeldInhalt('XAH_USER'), 0, $FeldBreitenHistorie['XAH_USER'], false, $HG, '', '', 'T', 'L',$rsXAH->FeldInhalt('XAH_USER'));
        $VGD->Form->Erstelle_ListenFeld('XAH_USERDAT', $rsXAH->FeldInhalt('XAH_USERDAT'), 0, $FeldBreitenHistorie['XAH_USERDAT'], false, $HG, '', '', 'DU', 'L',$rsXAH->FeldInhalt('XAH_USERDAT'));

        $VGD->Form->ZeileEnde();

        $rsXAH->DSWeiter();
        $DS++;
    }

    $VGD->Form->FormularBereichInhaltEnde();
    $VGD->Form->FormularBereichEnde();

}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$VGD->Form->Formular_Ende();
$VGD->Form->SchaltflaechenStart();

$VGD->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $VGD->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

if (($VGD->Recht69000 & 2) ==2 AND $AWIS_KEY1!='') { //Speichern erlaubt + in einen Datensatz?
    $VGD->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $VGD->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}

$VGD->Form->SchaltflaechenEnde();

$VGD->Form->SchreibeHTMLCode('</form>');

?>