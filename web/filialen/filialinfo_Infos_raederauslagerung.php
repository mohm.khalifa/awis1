<?php
//********************************************************************************************
// Raedereinlagerung
//********************************************************************************************
//
// Dieses Modul ermöglicht die Anzeige und das Bearbeiten der Raederauslagerungen für 
// eine Filiale
//
//********************************************************************************************
// Autor: Thomas Riedl
// Datum: 18.03.2008
//********************************************************************************************
// Änderungen:
// WER                   WANN      WAS
//
//
//********************************************************************************************
require_once("awis_forms.inc.php");

global $con;
global $awisRSZeilen;
global $AWISSprache;
global $AWISBenutzer;
global $SpeichernButton;

$TXT_Baustein = array();

$TXT_Baustein[]=array('RAL','RAL_%');
$TXT_Baustein[]=array('RAL','HinweisAbgeschlossen');
$TXT_Baustein[]=array('RAL','WirklichAbschliessen');
$TXT_Baustein[]=array('RAL','LieferscheinAbschliessen');
$TXT_Baustein[]=array('Wort','Abgeschlossen');
$TXT_Baustein[]=array('Wort','NichtAbgeschlossen');
$TXT_Baustein[]=array('Wort','PDFErzeugen');
$TXT_Baustein[]=array('Wort','KeineBerechtigungRaederauslagerungen');
$TXT_Baustein[]=array('Wort','Zurueck');
$TXT_Baustein[]=array('Wort','Ja');
$TXT_Baustein[]=array('Wort','Nein');
$TXT_Baustein[]=array('Wort','HinweisSpeichern');

$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);

// Aktueller Lieferschein?
$LFDNR = (isset($_GET['LFDNR'])?$_GET['LFDNR']:(isset($_POST['txtRAL_LFDNR'])?$_POST['txtRAL_LFDNR']:''));
$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
$FIL_ID = $FIL_ID[1];

$RechteStufe = awisBenutzerRecht($con,3603);	// MDE Raederauslagerung
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");     
}

$SpeichernButton = False;

if(($RechteStufe & 2)==2 or ($RechteStufe & 4)==4)
{
	$SpeichernButton = True;
}


// Beschränkung auf eine Filiale?
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);

if($UserFilialen!='' AND intval($FIL_ID) <> intval($UserFilialen))
{
	awis_FORM_Hinweistext($TXT_Baustein['Wort']['KeineBerechtigungRaederauslagerungen'],1);
	die;
}

if(isset($_POST['cmdSpeichern_x']))
{	
	include './filialinfo_speichern.php';
}
elseif(isset($_POST['cmdAbschliessen_x']))
{
	include './filialinfo_speichern.php';	
	
	$SQL = 'SELECT MIN(RAL_STATUS) AS STATUS';
	$SQL .=' FROM RAEDERAUSLAGERUNGEN';
	$SQL .=' WHERE RAL_FIL_ID=0'.intval($FIL_ID);
	$SQL .= ' AND RAL_LFDNR=0'.intval($LFDNR);
	
	awis_Debug(1,$SQL);
	
	$rsRAL=awisOpenRecordset($con,$SQL);
	$rsRALZeilen = $awisRSZeilen;
	
	if ($rsRAL['STATUS'][0]>=10)
	{
		echo '<span class=HinweisText>'.$TXT_Baustein['RAL']['HinweisAbgeschlossen'].'</span>';
		echo "<br><br>";		
		
		echo '<form name=frmBestaetigen action=./filialinfo_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>';
	
		echo '<input type=submit name=cmdAbschliessenAbbrechen style="width:50px" value='.$TXT_Baustein['Wort']['Zurueck'].'>';
	
		echo '<input type=hidden name=txtRAL_LFDNR value='.$LFDNR.'>';
		echo '</form>';		
	}
	else 
	{
		echo '<form name=frmBestaetigen action=./filialinfo_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>';
		echo '<span class=HinweisText>'.$TXT_Baustein['RAL']['WirklichAbschliessen'].'</span>';
	
		echo '<br><input type=submit name=cmdAbschliessenOK style="width:50px" value='.$TXT_Baustein['Wort']['Ja'].'>';
		echo '&nbsp;<input type=submit name=cmdAbschliessenAbbrechen style="width:50px" value='.$TXT_Baustein['Wort']['Nein'].'>';
	
		echo '<input type=hidden name=txtRAL_LFDNR value='.$LFDNR.'>';
		echo '</form>';
	}
	
	awisLogoff($con);
	die();
}
elseif(isset($_POST['cmdAbschliessenOK']))
{
	$SQL = 'UPDATE RAEDERAUSLAGERUNGEN';
	$SQL .= ' SET RAL_Status=12';
	$SQL .= ' , RAL_User='.awis_FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
	$SQL .= ' , RAL_UserDat=SYSDATE';
	$SQL .= ' , RAL_DatumAbschluss=SYSDATE';
	$SQL .= ' WHERE RAL_FIL_ID='.$FIL_ID;
	$SQL .= ' AND RAL_LFDNR=0'.intval($_POST['txtRAL_LFDNR']);
	
	if(awisExecute($con, $SQL)===false)
	{
		awisErrorMailLink('Raederauslagerungen abschliessen',1,'Fehler beim Speichern',$SQL);
	}			
}

if($LFDNR!='')
{
	$SQL = 'SELECT *';
	$SQL .=' FROM RAEDERAUSLAGERUNGEN RAL';
	$SQL .=' WHERE RAL_FIL_ID=0'.intval($FIL_ID);
	$SQL .= ' AND RAL_LFDNR=0'.intval($LFDNR);
	$SQL .= ' ORDER BY RAL_NR ASC';		
}
else 
{
	$SQL = 'SELECT DISTINCT RAL_LFDNR, RAL_STATUSUEBERTRAGEN';
	$SQL .= ' ,(SELECT MIN(RAL_STATUS) AS STATUS FROM RAEDERAUSLAGERUNGEN WHERE RAEDERAUSLAGERUNGEN.RAL_LFDNR=RAL.RAL_LFDNR AND RAEDERAUSLAGERUNGEN.RAL_FIL_ID = RAL.RAL_FIL_ID) AS RAL_STATUS';	
	$SQL .=' FROM RAEDERAUSLAGERUNGEN RAL';
	$SQL .=' WHERE RAL_FIL_ID=0'.intval($FIL_ID);
	$SQL .=' ORDER BY 1 DESC';
}

$rsRAL=awisOpenRecordset($con,$SQL);
$rsRALZeilen = $awisRSZeilen;

awis_FORM_FormularStart();

if($LFDNR=='')			// Übersicht zeigen
{
	awis_FORM_ZeileStart();	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_LFDNR'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_STATUS'],200);
	awis_FORM_ZeileEnde();
	
	for($RALZeile=0;$RALZeile<$rsRALZeilen;$RALZeile++)
	{
		awis_FORM_ZeileStart();					
		$Link='./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=MDE&Unterseite=Raederauslagerung&LFDNR='.$rsRAL['RAL_LFDNR'][$RALZeile].'';
		awis_FORM_Erstelle_ListenFeld('RAL_LFDNR',$rsRAL['RAL_LFDNR'][$RALZeile],20,200,false,($RALZeile%2),'',$Link,'T');
		$Status= (($rsRAL['RAL_STATUS'][$RALZeile]>=10)?$TXT_Baustein['Wort']['Abgeschlossen']:$TXT_Baustein['Wort']['NichtAbgeschlossen']);						
		awis_FORM_Erstelle_ListenFeld('RAL_STATUS',$Status,20,200,false,($RALZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	} 
}
else 						// Details anzeigen
{		
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['RAL']['RAL_LFDNR'].':',150);
	awis_FORM_Erstelle_TextFeld('RAL_LFDNR',$rsRAL['RAL_LFDNR'][0],0,200,false);
	echo '<input type=hidden name=txtRAL_LFDNR value=0'.$rsRAL['RAL_LFDNR'][0].'>';
	awis_FORM_ZeileEnde();		
	
	if(($RechteStufe & 2)==2)
	{
		echo "&nbsp;<input name=cmdAbschliessen type=image border=0 src=/bilder/bestaetigen.png title='".$TXT_Baustein['RAL']['LieferscheinAbschliessen']."'>";						
	}	
	
	if ($rsRAL['RAL_STATUS'][0]>=10)
	{
		$PDF_Link='./filialinfo_Infos_raederauslagerung_pdf.php?RAL_LFDNR='.$rsRAL['RAL_LFDNR'][0].'&RAL_FIL_ID='.$FIL_ID; 
		echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='".$TXT_Baustein['Wort']['PDFErzeugen']."'></a>";
	}
	
	//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
	if((($RechteStufe&2)!=0) AND $rsRALZeilen > 0)
	{
		echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;";
		echo '<span class=Hinweistext>'.$TXT_Baustein['Wort']['HinweisSpeichern'].'</span>';
	}

	awis_FORM_Trennzeile();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_NR'],120);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_DATUMSCAN'],120);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_MENGE'],100);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_BESCHAEDIGT'],120);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_BEMERKUNG'],300);	

	if(($RechteStufe & 8)==8)		
	{
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['RAL']['RAL_MENGEIST'],130);	
	}
	
	awis_FORM_ZeileEnde();		
	
	for($RALZeile=0;$RALZeile<$rsRALZeilen;$RALZeile++)
	{
		//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
		//if((($RechteStufe&2)!=0) AND $rsLIK['LIK_STATUS'][$LIKZeile]>=10 AND (($RechteStufe&4)!=4))
		if(((($RechteStufe&2)==2) AND $rsRAL['RAL_STATUS'][$RALZeile]<10))
		{
			$EditModus=true;
		}
		else
		{
			$EditModus=false;
		}
	
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('RAL_NR',$rsRAL['RAL_NR'][$RALZeile],10,120,false,($RALZeile%2),'','','T','L');
		awis_FORM_Erstelle_ListenFeld('RAL_DATUMSCAN',$rsRAL['RAL_DATUMSCAN'][$RALZeile],10,120,false,($RALZeile%2),'','','D','L');
		awis_FORM_Erstelle_ListenFeld('RAL_MENGE_'.$rsRAL['RAL_KEY'][$RALZeile],$rsRAL['RAL_MENGE'][$RALZeile],10,100,$EditModus,($RALZeile%2),'','','N','L');
		awis_FORM_Erstelle_Checkbox('RAL_BESCHAEDIGT_VL_'.$rsRAL['RAL_KEY'][$RALZeile], $rsRAL['RAL_BESCHAEDIGT_VL'][$RALZeile],30,$EditModus,'on','','Beschädigung vorne links');
		awis_FORM_Erstelle_Checkbox('RAL_BESCHAEDIGT_VR_'.$rsRAL['RAL_KEY'][$RALZeile], $rsRAL['RAL_BESCHAEDIGT_VR'][$RALZeile],30,$EditModus,'on','','Beschädigung vorne rechts');
		awis_FORM_Erstelle_Checkbox('RAL_BESCHAEDIGT_HL_'.$rsRAL['RAL_KEY'][$RALZeile], $rsRAL['RAL_BESCHAEDIGT_HL'][$RALZeile],30,$EditModus,'on','','Beschädigung hinten links');
		awis_FORM_Erstelle_Checkbox('RAL_BESCHAEDIGT_HR_'.$rsRAL['RAL_KEY'][$RALZeile], $rsRAL['RAL_BESCHAEDIGT_HR'][$RALZeile],30,$EditModus,'on','','Beschädigung hinten rechts');
		awis_FORM_Erstelle_ListenFeld('RAL_BEMERKUNG_'.$rsRAL['RAL_KEY'][$RALZeile],$rsRAL['RAL_BEMERKUNG'][$RALZeile],40,300,$EditModus,($RALZeile%2),'','','T','L');

		if(($RechteStufe & 8)==8)			
		{
			awis_FORM_Erstelle_ListenFeld('RAL_MENGESCAN',$rsRAL['RAL_MENGESCAN'][$RALZeile],10,130,false,($RALZeile%2),'','','N','L');
		}
		
		awis_FORM_ZeileEnde();
	}
}
	
awis_FORM_FormularEnde();

?>