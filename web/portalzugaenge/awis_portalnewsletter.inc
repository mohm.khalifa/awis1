<?php
/**
 * awis_portalzugaenge.inc
 *
 * Diese Klasse steuert die Portalzug�nge im AWIS und im Portal
 * Diese Datei muss im Produktivsystem und im PHP Shuttle identisch sein
 *
 * @author Sacha Kerres
 * @copyright ATU - Auto-Teile-Unger
 * @version 20091216
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awis_PortalNewsletter
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;

	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;

	/**
	 * Debuglevel f�r Ausgaben an die Konsole
	 * @var int (0=Keine, 9=Alles)
	 */
	private $_DebugLevel = 0;

	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);



	}

    /**
     * Erzeugt einen Text f�r einen Newsletter
     * 
     * @param int $PNL_KEY Newsletter-Key
     */
    public function NewsletterText($PNL_KEY)
    {
        $NewsLetterText = '';
        $NewsLetterQuelle = '';

        $SQL = 'SELECT *';
        $SQL .= ' FROM Portalnewsletter ';
        $SQL .= ' INNER JOIN Portalnewslettertypen ON PNL_PLT_KEY = PLT_KEY';
        $SQL .= ' WHERE PNL_KEY = 0'.$PNL_KEY;

        $rsPNL = $this->_DB->RecordSetOeffnen($SQL);
        if($rsPNL->EOF())
        {
            return false;
        }

        // Originaldatei auslesen
        $NewsLetterQuelle = ($rsPNL->FeldInhalt('PLT_VORLAGE'));
        $Bereiche = explode(',',$rsPNL->FeldInhalt('PLT_BEREICHE'));

        foreach($Bereiche as $Bereich)
        {
            $PosStart = strpos($NewsLetterQuelle,'<!-- '.strtoupper($Bereich).'_BLOCK_START-->');

            if($PosStart!==false)
            {
                $PosEnde = strpos($NewsLetterQuelle,'<!-- '.strtoupper($Bereich).'_BLOCK_ENDE-->');
                // Vorlage f�r einen Beitrag
                $TextBlock = substr($NewsLetterQuelle,$PosStart,$PosEnde-$PosStart);
                $SQL = 'SELECT *';
                $SQL .= ' FROM PortalnewsletterTexte';
                $SQL .= ' WHERE PNT_PNL_KEY = 0'.$PNL_KEY;
                $SQL .= ' AND PNT_BEREICH = '.$this->_DB->FeldInhaltFormat('T',$Bereich);
                $SQL .= ' AND PNT_VERSENDEN = 1';
                $rsPLT = $this->_DB->RecordSetOeffnen($SQL);
                $Block = '';

                while(!$rsPLT->EOF())
                {
                    $BlockBeitrag = str_replace('$'.strtoupper($Bereich).'_KOPF',$rsPLT->FeldInhalt('PNT_UEBERSCHRIFT'),$TextBlock);
                    $Link = 'https://atu-portal.de/index.php/login.html';

                    switch($Bereich)
                    {
                        case 'Beitrag':
                            $ZielLink = 'index.php?news/'.$rsPLT->FeldInhalt('PNT_IMPORTID').'.html';
                            break;
                        case 'Forum':
                            $ZielLink = 'index.php?forum/topic.html?id='.$rsPLT->FeldInhalt('PNT_IMPORTID');
                            break;
                        case 'Frage':
                            $ZielLink = 'index.php?component/dfcontact/';
                            break;
                        default:
                            $ZielLink = 'index.php';
                            break;
                    }
                    $Link .= '?return='.base64_encode($ZielLink);

                    $BlockBeitrag = str_replace('$'.strtoupper($Bereich).'_LINK', $Link, $BlockBeitrag);
                    $BlockBeitrag = str_replace('$'.strtoupper($Bereich).'_TEXT', $rsPLT->FeldInhalt('PNT_TEXT'), $BlockBeitrag);

                    $Block .= $BlockBeitrag;
                    //echo htmlspecialchars($BlockBeitrag);
                    $rsPLT->DSWeiter();
                }

                $TextStart = substr($NewsLetterQuelle,0,$PosStart);
                $TextStart .= $Block;
                $TextStart .= substr($NewsLetterQuelle,$PosEnde);

                $NewsLetterQuelle = $TextStart;
            }
        }


        $NewsLetterQuelle = str_replace('$EINLEITUNGTEXT',$rsPNL->FeldInhalt('PNL_EINLEITUNGSTEXT'),$NewsLetterQuelle);
        if($rsPNL->FeldInhalt('PNL_EINLEITUNGBILD')!='')
        {
            $Bild= '<img border=0 src="images/stories/'.$rsPNL->FeldInhalt('PNL_EINLEITUNGBILD').'">';
        }
        else
        {
            $Bild = '&nbsp;';
        }
        $NewsLetterQuelle = str_replace('$EINLEITUNGBILD',$Bild,$NewsLetterQuelle);
        $Form = new awisFormular();

        $Datum = $Form->Format('DMJ',$rsPNL->FeldInhalt('PNL_DATUM'));
        $NewsLetterQuelle = str_replace('$MONAT_JAHR',$Datum,$NewsLetterQuelle);

        // Bilderlink ersetzen
        $NewsLetterQuelle = str_replace('class="floatleft"','Style="float:left;margin-top:0pt;margin-right:10px;margin-bottom: 10px;margin-left: 0pt;"',$NewsLetterQuelle);
        $NewsLetterQuelle = str_replace('images/stories','https://atu-portal.de/img',$NewsLetterQuelle);

        return $NewsLetterQuelle;
    }

	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);
		}

		return $this->_DebugLevel;
	}

    /**
     * Importiert f�r den aktuellen Newsletter (neue) Infos
     * @param int $PNL_KEY  Newsletter
     */
    public function ImportiereAktuelleInfos($PNL_KEY)
    {
        $SQL = 'DELETE ';
        $SQL .= ' FROM PortalNewsletterTexte';
        $SQL .= ' WHERE PNT_PNL_KEY = '.$this->_DB->FeldInhaltFormat('N0',$PNL_KEY);

        $this->_DB->Ausfuehren($SQL);


        $SQL =' INSERT INTO PORTALNEWSLETTERTEXTE';
        $SQL .= '(PNT_PNL_KEY,PNT_BEREICH,PNT_POSITION,PNT_UEBERSCHRIFT,PNT_LINK,PNT_TEXT,PNT_VERSENDEN,PNT_BILDNAME,PNT_IMPORTID,PNT_INFOS,PNT_USER,PNT_USERDAT)';
        $SQL .= ' SELECT '.$this->_DB->FeldInhaltFormat('N0',$PNL_KEY);
        $SQL .= ' , PNI_BEREICH, 50, PNI_TITEL, null, PNI_TEXT, 1, null, PNI_IMPORTID, PNI_XML';
        $SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
        $SQL .= ', SYSDATE';
        $SQL .= ' FROM PortalnewsletterInfos';
        $this->_DB->Ausfuehren($SQL);

    }
}