<?php
// Gantt hour example
include ("../jpgraph.php");
include ("../jpgraph_gantt.php");
include ("../jpg-config.inc");
//DEFINE("LOCALE","de_DE");

$dateLocale = new DateLocale(); 
// Use Swedish locale 
$dateLocale->Set('de_DE'); 

$graph = new GanttGraph(600,300,"auto",1);
$graph->SetMarginColor('blue:1.7');
$graph->SetColor('white');

$graph->SetBackgroundGradient('navy','white',GRAD_HOR,BGRAD_MARGIN);
$graph->scale->hour->SetBackgroundColor('lightyellow:1.5');
$graph->scale->hour->SetFont(FF_FONT1);
$graph->scale->day->SetBackgroundColor('lightyellow:1.5');
$graph->scale->day->SetFont(FF_FONT1,FS_BOLD);

$graph->title->Set("Beispiel über Stunden mit scale");
$graph->title->SetColor('white');
$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);

$graph->ShowHeaders(GANTT_HDAY | GANTT_HHOUR);

$graph->scale->week->SetStyle(WEEKSTYLE_FIRSTDAY);
$graph->scale->week->SetFont(FF_FONT1);
$graph->scale->hour->SetIntervall(3);

$graph->scale->hour->SetStyle(HOURSTYLE_HM24);
$graph->scale->day->SetStyle(DAYSTYLE_SHORTDAYDATE3);

$data = array(
    array(0,"  Blafasel 1", "011126 04:00","011126 14:00"),
    array(1,"  Blubber 2", "011126 10:00","011126 18:00"),
    array(2,"  Linie 3", "011126","011127 10:00"),
    array(3,"  Neu", "011126 11:00","011127 01:00")
);


for($i=0; $i<count($data); ++$i) {
	$bar = new GanttBar($data[$i][0],$data[$i][1],$data[$i][2],$data[$i][3],"[5%]",10);
	if( count($data[$i])>4 )
		$bar->title->SetFont($data[$i][4],$data[$i][5],$data[$i][6]);
	$bar->SetPattern(BAND_RDIAG,"yellow");
	$bar->SetFillColor("red");
	$graph->Add($bar);
}

$graph->Stroke();



?>


