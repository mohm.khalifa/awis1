<?php
require_once 'awisBerichte.inc';
require_once('abgleich_funktionen.inc');

class ber_InkassoAnhang
	extends awisBerichte
{
    
    private $KompletteBreite = 0;
    private $debug = '';

    /**
     * Setzt den Speicherpfad f�r AusgabeArt 4
     * @param type $DateiPfad  kompletter Pfad absoluter Pfad mit Dateiname und Dateiendung
     */
    public function SetzeDateipfad($DateiPfad)
    {
        $this->_Dateiname = $DateiPfad;
    }    
    
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
        $this->KompletteBreite = self::SeitenBreite() - (self::RECHTERRAND + self::LINKERRAND);
        
        $TAG = new awis_abgleich_funktionen();
		// Daten f�r den Bericht laden
        // gleicher SQL, wie in Hauptmaske (beim Aufruf des Berichtes wird aber immer nur nach einer Filiale gefiltert => wird vor dem Aufruf als Parameter gesetzt)
        $SQL =  'select tgd.*, tue.tue_bez, fil.fil_bez, tua.tua_bez ' .
                'from tuevgesellschaftendaten tgd ' .
                'left join tuevorganisationen tue on tgd.tgd_tuevnr = tue.tue_id ' .
                'left join filialen fil on tgd.tgd_fil_id = fil.fil_id ' .
                'left join tuevarten tua on tua.tua_id = tgd.tgd_tuevart ' .
                'left join tuevabgleich tag on tag.tag_tgd_key = tgd.tgd_key';
        /*
        $Bedingung = ' and (tgd_abgeglichen = 1 ' .                // diese Bedingung nimmt abgeglichene Datens�zte mit, deren Abgleichsart = N (Nachverfolgung) ist
                           'and tag.tag_taa_key = (select taa_key from tuevabgleichart ' .
                                                  'where taa_kennung = \'V\') ' . 
                           'and tag.tag_tgd_key not in (select sub_tag.tag_tgd_key from tuevabgleich sub_tag ' .            // diese Bedingung schmei�t alle mitgenommenen (Abgleichart = N) wieder raus, die nachtr�glich noch anders abgeglichen wurden
                                                       'where sub_tag.tag_taa_key in (select taa_key from tuevabgleichart ' . 
         */                                                                            'where taa_kennung != \'V\'))';
        // Alle Parameter auswerten
        // Format $Parameter[<Name>]=<Wert>
        //			Beispiel: $Parameter['TGD_FIL_ID']="60"
        // Dieses Format kann direkt von der Funktion "ErstelleTAGBedingung()" verarbeitet werden,
        //      solange die Bezeichnungen des assoziatifen Arrays passen (siehe Parameternamen in Klasse oder php-File die Suchparameter)            
        //$Bedingung .= $TAG->ErstelleTGDBedingung($Parameter);       // Bedingung erstellen 2x => f�r den ersten Teile ODER und f�r den zweiten Teil der Gesamtbedingung
    	
        $Bedingung = 'and tgd_abgeglichen = 0';                  // dann verODERn, geklammert 
        $Bedingung .= $TAG->ErstelleTGDBedingung($Parameter);       // mit dem Rest
        if ($Bedingung != '')
        {
            $SQL .= ' WHERE ' . substr($Bedingung, 4);
        }
        $SQL .= ' order by tgd.tgd_fil_id';
$this->debug = $SQL;
		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL);
 
		// Alle Textbausteine laden
		$TextKonserven[]=array('TAG','*');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eiegntliches PDF erzegen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 4)
	{
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->AliasNbPages();
		$this->SetFillColor(192, 192, 192);
		
		$this->NeueSeite();
        
		$this->_Zeile = $this->_Parameter['LinkerRand'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
//$this->Write($this->_Parameter['Zeilenhoehe'], $this->debug);

        $DSfuerModulo = 1;                                      // Z�hler der Datens�tze f�r Modulo-IF
        $Seitenzahl = 1;                                        // Z�hler der Seiten insgesamt
        $DSproSeite = $this->_Parameter['DSproErsteSeite'];     // Parameter f�r Datens�tze f�r die erste Seite merken (jede weitere Seite, einen Datensatz mehr, weil �berschriftKopf nicht mit drauf ist)
        
        $this->ErstelleUeberschrift();                          // �berschrift und ATU-Logo
        $this->ErstelleKopf();                                  // �berschrift f�r Belegnummer ... (pro Seite)

        while (!$this->_rec->EOF())
        {
            $this->ErstelleDS();                                // Bereich f�r einen Datensatz hinpinseln
            $this->ErstelleAusfuellVorlage();                   // Bereich f�r auszuf�llenden Dinge hinpinseln
            
            $this->_Zeile += $this->_Parameter['Zeilenhoehe']/4;        // kleiner Offset f�r Linie
            $this->SetLineWidth(0.5);                                       
            $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);
            $this->_Zeile += $this->_Parameter['Zeilenhoehe']/4;            

            if (($DSfuerModulo%$DSproSeite) == 0)                             // wenn Anzahl der Datens�tze MODULO Datens�tze pro Seite
            {
                $this->ErstelleFuss($Seitenzahl);                   // Fusszeile
                $Seitenzahl++;                                      // Seitenzahl inkrementieren

                $this->NeueSeite();                                 // neue Seite und X/Y-Cursor-Positionen wieder auf Anfang
                $this->_Zeile = $this->_Parameter['LinkerRand'];
                $this->_Spalte = $this->_Parameter['LinkerRand'];
                $this->ErstelleUeberschrift();
                $this->ErstelleKopf();                              // �berschrift f�r Belegnummer ... (pro Seite)
                
                /*
                if ($DSproSeite == $this->_Parameter['DSproErsteSeite'])    // das passiert nur beim ersten Durchlauf (wegen �berschrift ein DS weniger auf 1. Seite)
                {
                    $DSproSeite++;                                          // inkrementieren, damit auf den Folgeseiten ein DS mehr angezeigt wird
                    $DSfuerModulo++;                                        // DS-Z�hler inkrementieren (bescheissen), damit beim n�chsten Schleifendurchlauf das IF nicht anschl�gt (=> sonst w�rde auf der 2. Seite nur ein Datensatz angezeigt werden)
                }
                */
            }            
            
            $this->_rec->DSWeiter();
            $DSfuerModulo++;
        }
        $this->ErstelleFuss($Seitenzahl);                   // Fusszeile (weil f�r letzte, bzw. wenn nur 1 Seite das if in der oberen Schliefe nicht anschl�gt)

//$this->Write($this->_Parameter['Zeilenhoehe'], $this->debug);
        $this->FertigStellen($Ausgabeart);        
	}

    private function ErstelleUeberschrift()
    {
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseUeberschrift']);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'] * 3, $this->_AWISSprachkonserven['TAG']['PDF_OffeneInkasso'], 0, 0, 'C', false);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $BildBreite = 143 / 4;      // die 143 ist die echte Breite des Images (bei beiden geteilt durch X, damit man auf eine unverzerrte kleinere Gr��e kommt)
        $BildHoehe = 48 / 4;        // die 48 ist die echte H�he des Images (bei beiden geteilt durch X, damit man auf eine unverzerrte kleinere Gr��e kommt)
        $this->Image('/daten/web/bilder/atulogo_neu.png', 245, $this->_Zeile, $BildBreite, $BildHoehe);
        $this->_Zeile += $BildHoehe + $this->_Parameter['Zeilenhoehe'];        
    }
    
    private function ErstelleKopf()
    {
        $this->SetLineWidth(0.75);
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']/4;
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Parameter['Spalte01'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_BelegNr'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte01'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte02'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_FilNr'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte02'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte03'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_PruefDatum'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte03'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte04'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_KfzKennz'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte04'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte05'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Preis'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte05'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte06'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Name'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte06'];

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte07'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Tuev'], 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte07'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte08'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_TuevArt'], 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        
        $this->SetLineWidth(0.5);
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']/4;
    }
    
    private function ErstelleDS()
    {
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Parameter['Spalte01'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('TGD_BELEGNR'), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte01'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte02'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('TGD_FIL_ID'), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte02'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte03'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D', $this->_rec->FeldInhalt('TGD_PRUEFDATUM')), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte03'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte04'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('TGD_KFZKENNZ'), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte04'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte05'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N2', $this->_rec->FeldInhalt('TGD_PREIS')) . ' EUR', 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte05'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte06'], $this->_Parameter['Zeilenhoehe'], substr($this->_rec->FeldInhalt('TGD_NACHNAME'),0,19), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte06'];

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte07'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('TUE_BEZ'), 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte07'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte08'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('TUA_BEZ'), 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
    }
    
    private function ErstelleAusfuellVorlage()
    {
        $this->SetLineWidth(0.25);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte01'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_WaNr'].'__________________', 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte01'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte02'] + $this->_Parameter['Spalte03'] + $this->_Parameter['Spalte04'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Abkassiert'].'________________', 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte02'] + $this->_Parameter['Spalte03'] + $this->_Parameter['Spalte04'];

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte05'] + $this->_Parameter['Spalte06'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Gesamtbetrag'].':______________', 0, 0, 'L', false);
        $this->_Spalte += $this->_Parameter['Spalte05'] + $this->_Parameter['Spalte06'];
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['Spalte07'] + $this->_Parameter['Spalte08'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_FaxBearbeitet'].'_____________________', 0, 0, 'L', false);
        
        
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Rect($this->_Spalte+1.5, $this->_Zeile+0.75,2.5,2.5);
        $this->_Spalte = $this->_Parameter['LinkerRand']+4;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_KD'], 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand']+50;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Rect($this->_Spalte+1.5, $this->_Zeile+0.75,2.5,2.5);
        $this->_Spalte = $this->_Spalte + 4;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_WA'], 0, 0, 'L', false);
        $this->_Spalte = $this->_Parameter['LinkerRand']+115;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Rect($this->_Spalte+1.5, $this->_Zeile+0.75,2.5,2.5);
        $this->_Spalte = $this->_Spalte + 4;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_POS'], 0, 0, 'L', false);
        
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
        
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->KompletteBreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Bemerkung'].'___________________________________________________________________________________', 0, 0, 'L', false);
        
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
        
    }
    
    private function ErstelleFuss($Seite)
    {
        $rs = $this->_DB->RecordSetOeffnen('select to_char(sysdate, \'DAY DD. MONTH YYYY\') as datum from dual');
        $Datum = $rs->FeldInhalt('DATUM');
        
        $this->SetLineWidth(0.25);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        
        $this->_Zeile = $this->_Parameter['Seite-Unten'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        
        $this->Line($this->_Spalte, $this->_Zeile, $this->_Spalte + $this->KompletteBreite, $this->_Zeile);        
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']/2;

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $Datum);
        
        $this->_Spalte = $this->_Parameter['RechteSeite'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['TAG']['PDF_Seite'] . ' ' . $Seite);
    }
    
	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
		$this->AddPage();
	}
		
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
                /*
		// Seitennummer schreiben
		$this->SetXY(15,self::SeitenHoehe()-5);
		$this->SetFont('Arial','',7);
		$this->Cell(10,4,$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');
		$this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
		$this->Cell(30,4,$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
                */
	}

	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{              
	    //$this->pdf_vorlage = $this->ImportPage(1);
	    //$this->useTemplate($pdf_vorlage);
		/*
		$Zeile = $this->_Parameter['Kopfzeile1-Oben'];
		$Spalte = $this->_Spalte;

		// Berichtsparameter anzeigen
		$this->SetXY($Spalte,$Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Ueberschrift-Groesse']);
		$this->Cell(30, 4, $this->_AWISSprachkonserven['PVS']['pdf_BvbKopf'] ,0 ,0 ,'L' ,'');

		$Zeile+=5;

		// Trennlinie
		$this->SetXY($Spalte,$Zeile);
		$this->Line($this->_Spalte,$Zeile,self::SeitenBreite()-self::RECHTERRAND,$Zeile);
		$Zeile+=2;
                */
	}	

    public function FertigStellen($Ausgabeart)        
    {
        //*********************************************************************
		// Berichtsende -> Zur�ckliefern
		//*********************************************************************
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zur�ckliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
    }
    
}

?>