<?php
require_once 'awisSeminarplanEmail.php';

/**
 * Created by PhpStorm.
 * User: patrickgebhardt
 * Date: 10.07.17
 * Time: 17:10
 */
class awisSeminarplanEinladungsemail extends awisSeminarplanEmail
{
    /**
     * Standardabsender der Email
     */
    const Absender = 'noreply@de.atu.eu';

    /**
     * awisSeminarplanEinladungsemail constructor.
     * @param $SET_KEY
     */
    function __construct($SET_KEY)
    {
        parent::__construct($SET_KEY);

        //Text holen, Betreff und Empf�nger setzen
        $SQL = 'select';
        $SQL .= '     sort,';
        $SQL .= '     SEM_KEY,';
        $SQL .= '     SEM_SEP_SEMINAR,';
        $SQL .= '     SEM_MVT_BEREICH,';
        $SQL .= '     SEM_ANHAENGE,';
        $SQL .= '     SEM_ABSENDER,';
        $SQL .= '     SEM_EMPFAENGER,';
        $SQL .= '     SEM_EMPFAENGERCC,';
        $SQL .= '     SEM_EMPFAENGERBCC,';
        $SQL .= '     SEM_USER,';
        $SQL .= '     SEM_USERDAT';
        $SQL .= ' from';
        $SQL .= '     (';
        //Alle Emails holen, die f�r diesen Teilneumer in Frage kommen
        $SQL .= '         select';
        $SQL .= '             UTL_MATCH.EDIT_DISTANCE_SIMILARITY(';
        $SQL .= '                 SEM_SEP_SEMINAR,';
        $SQL .= '                 (';
        $SQL .= '                     (select';
        $SQL .= '                         SEP_SEMINAR';
        $SQL .= '                     from';
        $SQL .= '                         SEMINARTEILNEHMER a';
        $SQL .= '                         inner join SEMINARPLAN B on a.SET_SEP_KEY = B.SEP_KEY';
        $SQL .= '                     where';
        $SQL .= '                         SET_KEY = ' . $this->_DB->WertSetzen('SEM', 'Z', $SET_KEY);
        $SQL .= '                     )';
        $SQL .= '                 )';
        $SQL .= '             ) as sort,'; //Umso n�her die Zuordnung am Seminar ist, umso weiter steht sie oben. Beispiel: Seminar: 2012-12-123 Zuordnungen in der Datenbank: 2012-; 2012-12 und 2012-12-123. Somit entsehen folgende Sortwerte: 50,70,100. Der mit 100 wird verwendet
        $SQL .= '             SEM_KEY,';
        $SQL .= '             SEM_SEP_SEMINAR,';
        $SQL .= '             SEM_MVT_BEREICH,';
        $SQL .= '             SEM_ANHAENGE,';
        $SQL .= '             SEM_ABSENDER,';
        $SQL .= '             SEM_EMPFAENGER,';
        $SQL .= '             SEM_EMPFAENGERCC,';
        $SQL .= '             SEM_EMPFAENGERBCC,';
        $SQL .= '             SEM_USER,';
        $SQL .= '             SEM_USERDAT';
        $SQL .= '         from';
        $SQL .= '             SEMINAREMAILS';
        $SQL .= '         where';
        $SQL .= '             (';
        $SQL .= '                 select';
        $SQL .= '                     SEP_SEMINAR';
        $SQL .= '                 from';
        $SQL .= '                     SEMINARTEILNEHMER a';
        $SQL .= '                     inner join SEMINARPLAN B on a.SET_SEP_KEY = B.SEP_KEY';
        $SQL .= '                 where';
        $SQL .= '                     SET_KEY = ' . $this->_DB->WertSetzen('SEM', 'Z', $SET_KEY);;
        $SQL .= '             ) like SEM_SEP_SEMINAR || \'%\'';
        $SQL .= '         union';
        //Als Fallback den Standardtext mitholen
        $SQL .= '         select';
        $SQL .= '             -1 as sort,'; //Bekommt -1 und steht somit ganz unten
        $SQL .= '             SEM_KEY,';
        $SQL .= '             SEM_SEP_SEMINAR,';
        $SQL .= '             SEM_MVT_BEREICH,';
        $SQL .= '             SEM_ANHAENGE,';
        $SQL .= '             SEM_ABSENDER,';
        $SQL .= '             SEM_EMPFAENGER,';
        $SQL .= '             SEM_EMPFAENGERCC,';
        $SQL .= '             SEM_EMPFAENGERBCC,';
        $SQL .= '             SEM_USER,';
        $SQL .= '             SEM_USERDAT';
        $SQL .= '         from';
        $SQL .= '             SEMINAREMAILS';
        $SQL .= '         where';
        $SQL .= '             SEM_SEP_SEMINAR = \'*\'';
        $SQL .= '     )';
        $SQL .= ' order by';
        $SQL .= '     sort desc';

        $rsMail = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('SEM'));
        $Gefunden = false;
        while (!$rsMail->EOF() and !$Gefunden) {
            //Text und Betreff holen:
            $SQL = 'SELECT MVT_TEXT, MVT_BETREFF from MAILVERSANDTEXTE WHERE MVT_BEREICH = ' . $this->_DB->WertSetzen('MVT', 'T', $rsMail->FeldInhalt('SEM_MVT_BEREICH'));
            $rsText = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('MVT'));

            if ($rsText->AnzahlDatensaetze() == 0) {
                $this->_Werkzeuge->EMail(array('shuttle@de.atu.eu'), 'WARNUNG - F�r das Seminar ' . $rsMail->FeldInhalt('SEM_SEP_SEMINAR') . ' gibt es keinen g�ltigen Mailtext!',
                    'F�r das oben genannte Seminar wurde ein Mailtext gepflegt. Dieser konnte jedoch nicht gefunden werden. Versuche einen anderen Mailtext zu finden der passt... ', 2, '', 'awis@de.atu.eu');
            } else {
                $this->Text($rsText->FeldInhalt('MVT_TEXT'));
                $this->Betreff($rsText->FeldInhalt('MVT_BETREFF'));
                $Gefunden = true;
                continue;
            }
            $rsMail->DSWeiter();
        }

        //Alle Empf�nger setzen
        foreach ($this->EmpfaengerAufloesen($rsMail->FeldInhalt('SEM_EMPFAENGER')) as $Emp) {
            parent::Adressliste($Emp, 'TO');
        }
        foreach ($this->EmpfaengerAufloesen($rsMail->FeldInhalt('SEM_EMPFAENGERCC')) as $Emp) {
            parent::Adressliste($Emp, 'CC');
        }
        foreach ($this->EmpfaengerAufloesen($rsMail->FeldInhalt('SEM_EMPFAENGERBCC')) as $Emp) {
            parent::Adressliste($Emp, 'BCC');
        }

        //Absender setzen
        $this->Absender(self::Absender);

        //Alle Anhn�nge setzen die dazu geh�ren!
        foreach ($this->AnhaengeAufloesen($rsMail->FeldInhalt('SEM_ANHAENGE')) as $Anhang) {
            $this->_Mailer->Anhaenge($Anhang[0], $Anhang[1]);
        }

    }

    /**
     * versendet die Email
     *
     * @return bool
     */
    public function Senden()
    {
        return parent::Senden();
    }

    /**
     * Email wurde �ber die Maske versendet
     */
    const EMAIL_STATUS_MANUELL = 2;
    /**
     * Email wurde �ber den Job versendet
     */
    const EMAIL_STATUS_JOB = 1;

    /**
     * Protokolliert, dass die Email versendet wurde
     * @param int $Status
     */
    public function ProtokolliereEmailversand($Status = 1)
    {
        $SQL = 'update SEMINARTEILNEHMER ';
        $SQL .= '         set SET_STATUSEMAIL = ' . $this->_DB->WertSetzen('SET', 'Z', $Status);
        $SQL .= ' ,            SET_EMAILVERSANDZEITPUNKT = SYSDATE,';
        $SQL .= '             SET_TEILNAHME = -1';
        $SQL .= '         where SET_KEY = ' . $this->_DB->WertSetzen('SET', 'Z', $this->_SET_KEY);

        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('SET'));
    }

}