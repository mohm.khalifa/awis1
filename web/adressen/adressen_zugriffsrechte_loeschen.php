<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISSprache;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

try 
{
    require_once('awisDokumente.php');
    
    $TextKonserven=array();
    $TextKonserven[]=array('Wort','WirklichLoeschen');
    $TextKonserven[]=array('Wort','Ja');
    $TextKonserven[]=array('Wort','Nein');
    $TextKonserven[]=array('AZG','%');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Tabelle= '';

    if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamte Adresse
    {
        $Tabelle = 'AZG';
        $Key=$_POST['txtAZG_KEY'];
    
        $Felder=array();
        $Felder[]=array($Form->LadeTextBaustein('AZG_BEZEICHNUNG','AZG',$AWISSprache),$_POST['txtAZG_BEZEICHNUNG']);
    }
    elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
    {
        switch($_GET['Seite'])
        {
        	case 'Mitglieder':
        	    $Tabelle = 'AZM';
        	    $Key=$_GET['Del'];
    
        	    $rsDaten = $DB->RecordSetOeffnen('SELECT AZM_KEY,AZM_AZG_KEY,XBN_VOLLERNAME FROM AdressenZugriffsGrpMitglieder INNER JOIN Benutzer ON AZM_XBN_KEY = XBN_KEY WHERE AZM_KEY=0'.$Key);
        	    $Felder=array();
        	    $Felder[]=array($Form->LadeTextBaustein('XBN_VOLLERNAME','XBN',$AWISSprache),$rsDaten->FeldInhalt('XBN_VOLLERNAME'));
        	    $AZG_KEY = $rsDaten->FeldInhalt('AZM_AZG_KEY');
        	    break;
        }
    }
    elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
    {
        $SQL = '';
        switch ($_POST['txtTabelle']) {
        	case 'AZG':
        	    $SQL = 'DELETE FROM AdressenZugriffsGruppen WHERE azg_key=0'.$_POST['txtKey'];
        	    $AWIS_KEY1=0;
        	    break;
        	default:
        	    break;
        	case 'AZM':
        	    $SQL = 'DELETE FROM AdressenZugriffsGrpMitglieder WHERE azm_key=0'.$_POST['txtKey'];
        	    $AWIS_KEY1 = $_POST['txtAZG_KEY'];
        	    break;
        	default:
        	    break;
        }
        if($SQL !='')
        {
            if($DB->Ausfuehren($SQL)===false)
            {
                throw new awisException('Fehler beim L�schen',201403241042,$SQL,awisException::AWIS_ERR_SYSTEM);
            }
        }
    }
    if($Tabelle!='')
    {
        $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven, $AWISSprache);
        echo '<form name=frmLoeschen action=./adressen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
        echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';
    
        foreach($Felder AS $Feld)
        {
            echo '<br>'.$Feld[0].': '.$Feld[1];
        }
    
        echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
        echo '<input type=hidden name=txtKey value="'.$Key.'">';
        echo '<input type=hidden name=txtAZG_KEY value="'.$AZG_KEY.'">';
    
        echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
        echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';
    
        echo '</form>';
        $DB->Schliessen();
        die();
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>