<? 

function fs_balkengrafik($fs, $rname)
{

		$beschriftungfontgroesse = 3;
		$beschriftunghoehe = imagefontheight($beschriftungfontgroesse);
		$ueberschriftfontgroesse = 5;
		$ueberschrifthoehe = imagefontheight($ueberschriftfontgroesse);
		$balkenbreite = 30;
		$linkerrand = 0;
		$aussenabstand = 10;
		$zwischenabstand = 5;

		//**********************************
		//* Array auswerten
		//**********************************
	
		$anzfs = count($fs);
		$minbreite = 150;
		
		while(list(,$lines)=each($fs))
		{
			$zeile = ereg_replace("[ ]{2,}", " ", $lines);
			$felder = explode(" ", $zeile);
			$beschriftungbreite1 = imagefontwidth($beschriftungfontgroesse) * strlen($felder[5]);
			$beschriftungbreite2 = imagefontwidth($beschriftungfontgroesse) * strlen($felder[0]);
			$minbreite = max($minbreite, $beschriftungbreite1, $beschriftungbreite2);

		}	
		
		$minbreite = $minbreite * $anzfs;
		$minbreite = $minbreite + (2 * $aussenabstand) + (($anzfs - 1) * $zwischenabstand);

		//**********************************
		//* Bild erzeugen
		//**********************************
	
		$breite = max($minbreite, 300);
		$hoehe = 250;
	
		$img = imagecreate($breite, $hoehe);
		$vcolor = imagecolorallocate($img, 192, 192, 192);	//Grau
		$bcolor = imagecolorallocate($img, 0, 0, 0);	//Schwarz
		$freicol = imagecolorallocate($img, 0, 255, 0);	//Gr�n
		$vollcol = imagecolorallocate($img, 255, 0, 0);	//Rot
		imagerectangle($img, 0, 0, $breite - 1, $hoehe - 1, $bcolor);
		imagerectangle($img, 2, 2, $breite - 3, $hoehe - 3, $bcolor);

		// �berschrift
		$lage = ($breite - imagefontwidth($ueberschriftfontgroesse) * strlen($rname)) / 2;
		imagestring($img, $ueberschriftfontgroesse, $lage, $ueberschrifthoehe, $rname, $bcolor);
	

		//**********************************
		//* Angaben weiter auswerten
		//**********************************
	
		reset($fs);
		while(list(,$lines)=each($fs))
		{
			$zeile = ereg_replace("[ ]{2,}", " ", $lines);
			$felder = explode(" ", $zeile);
			


			//**********************************
			//* Bild weitermalen
			//**********************************
	

			// Seitlichen Versatz berechnen
			if($linkerrand == 0)
			{
				$linkerrand = 25;
			}
			else
			{
				$linkerrand = $linkerrand + 150;
			}


			$felder[0] = trim($felder[0]);
			$felder[5] = trim($felder[5]);

			// ermitteln der Grafikmitte
			$beschriftungbreite1 = imagefontwidth($beschriftungfontgroesse) * strlen($felder[5]);
			$beschriftungbreite2 = imagefontwidth($beschriftungfontgroesse) * strlen($felder[0]);
			$mitte = abs($beschriftungbreite1 - $beschriftungbreite2) / 2 + $linkerrand;

			// Mountpoint
			$text = $felder[5];
			imagestring($img, $beschriftungfontgroesse, $mitte - $beschriftungbreite1 / 2, $hoehe - 2 * $beschriftunghoehe, $text, $bcolor);

			// Filesystem
			$text = $felder[0];
			imagestring($img, $beschriftungfontgroesse, $mitte - $beschriftungbreite2 / 2, $hoehe - 4 * $beschriftunghoehe, $text, $bcolor);

			// Balkengrafik
			$geshoehe = $hoehe - 5 * $beschriftunghoehe - 3 * $ueberschrifthoehe;
			$hoehefrei = $geshoehe * $felder[3] / $felder[1];
			$hoehevoll = $geshoehe * $felder[2] / $felder[1];
			$unten = $hoehe - 5 * $beschriftunghoehe;
			imagerectangle($img, $mitte - $balkenbreite / 2, $unten, $mitte + $balkenbreite / 2, $unten - $hoehevoll, $vollcol);
			if($hoehevoll > 1)
			{
				imagefill($img, $mitte, $unten - 1, $vollcol);
			}
			imagerectangle($img, $mitte - $balkenbreite / 2, $unten - $hoehevoll, $mitte + $balkenbreite / 2, $unten - $geshoehe, $freicol);
			imagefill($img, $mitte, $unten - $hoehevoll - 1, $freicol);


			// Prozentangabe
			$text = $felder[4];
			if($hoehefrei * 1.5 > $beschriftunghoehe)
			{
				// In das Freifeld schreiben
				imagestring($img, $beschriftungfontgroesse, $mitte - imagefontwidth($beschriftungfontgroesse) * strlen($text) / 2, 45, $text, $bcolor);
		
			}
			else
			{
				// Neben das Freifeld schreiben
				imagestring($img, $beschriftungfontgroesse, $mitte + $balkenbreite / 2 + 5, 45, $text, $bcolor);
			}

		}	//Bild fertig!

		imagepng($img, "/daten/web/admin/dateien/".$rname.".png");
		imagedestroy($img);
	
		//Rechte setzen
		chmod("/daten/web/admin/dateien/".$rname.".png", 0666);
	
}  // End Function



function fs_verlauf($rsFsh, $rsStat)
{

	//**********************************
	//* Vorgaben
	//**********************************
	
	$beschriftungfontgroesse = 3;
	$beschriftunghoehe = imagefontheight($beschriftungfontgroesse);
	$ueberschriftfontgroesse = 5;
	$ueberschrifthoehe = imagefontheight($ueberschriftfontgroesse);
	$breite = 450;
	$hoehe = 250;
	$linkerrand = 40;
	$rechterrand = 100;
	$untererrand = 30;

	//Breiteste Beschriftung in der Legende suchen
	for ($i = 0; $i < count($rsStat["FS_ID"]); $i++)
	{
		$rechterrand = max($rechterrand, imagefontwidth($beschriftungfontgroesse) * strlen($rsStat["FS_MOUNTPOINT"][$i]));
	}
	$rechterrand = $rechterrand + 20; 			//Abstand zu den Linien

	$img = imagecreate($breite, $hoehe);
	$vcolor = imagecolorallocate($img, 192, 192, 192);	//Grau
	$bcolor = imagecolorallocate($img, 0, 0, 0);		//Schwarz
	$col_gruen = imagecolorallocate($img, 0, 255, 0);	//Gr�n
	$col_rot = imagecolorallocate($img, 255, 0, 0);		//Rot
	$col_blau = imagecolorallocate($img, 0, 0, 255);	//Blau
	$col_gelb = imagecolorallocate($img, 255, 255, 0);	//Gelb
	$col_orange = imagecolorallocate($img, 255, 153, 0);	//Oranje
	$col_purpur = imagecolorallocate($img, 204, 0, 153);	//Lila
	$colArray[] = $col_rot;
	$colArray[] = $col_gelb;
	$colArray[] = $col_gruen;
	$colArray[] = $col_blau;
	$colArray[] = $col_purpur;
	$colArray[] = $col_orange;
	$colArray[] = $bcolor;

	// Rand
	imagerectangle($img, 0, 0, $breite - 1, $hoehe - 1, $bcolor);
	imagerectangle($img, 2, 2, $breite - 3, $hoehe - 3, $bcolor);

	//Koordinatensystem
	$x0 = (integer)$linkerrand;
	$y0 = (integer)($hoehe - $untererrand);
	$x1 = (integer)($breite - $rechterrand);
	$y1 = (integer)($ueberschrifthoehe * 3);
	imageline($img, $x0, $y0, $x1, $y0, $bcolor);	//X-Achse
	imageline($img, $x0, $y0, $x0, $y1, $bcolor);	//Y-Achse
	imagedashedline($img, $x0, $y1, $x1, $y1, $bcolor);	//Rand oben
	imagedashedline($img, $x1, $y0, $x1, $y1, $bcolor);	//Rand hinten

	//**********************************
	//* Rechnernamen suchen
	//**********************************
	
	$ds = array_search($rsFsh["FS_ID"][0], $rsStat["FS_ID"]);

	$rname = $rsStat["RN_NAME"][$ds];
	$anz = $rsStat["ANZ"][$ds];

	// �berschrift
	$lage = ($breite - imagefontwidth($ueberschriftfontgroesse) * strlen($rname)) / 2;
	imagestring($img, $ueberschriftfontgroesse, $lage, $ueberschrifthoehe, $rname, $bcolor);

	$startdatum = $rsFsh["DATUM"][0];
	$lagex = $x0 - (imagefontwidth($beschriftungfontgroesse) * strlen($startdatum)) / 2; 
	imagestring($img, $beschriftungfontgroesse, $lagex, $y0 + 1, $startdatum, $bcolor);

	$enddatum = $rsFsh["DATUM"][$anz - 1];
	$lagex = $x1 - (imagefontwidth($beschriftungfontgroesse) * strlen($enddatum)) / 2; 
	imagestring($img, $beschriftungfontgroesse, $lagex, $y0 + 1, $enddatum, $bcolor);
	
	//Bemassung
	$text = "100%";
	$lagex = $x0 - (imagefontwidth($beschriftungfontgroesse) * strlen($text)) - 1; 
	$lagey = $y1 - $beschriftunghoehe / 2; 
	imagestring($img, $beschriftungfontgroesse, $lagex, $lagey, $text, $bcolor);
	
	
	//Filesysteme
	$punkt = 0;					//Z�hlvariable f�r die einzelnen Punkte einer Linie
	$fs = 0;					//Z�hlvariable f�r die Filesysteme (Linien)

	for($i=0; $i < count($rsFsh["FS_ID"]); $i++)
	{
		if ($punkt == 0)			// 0 -> Es beginnt ein neues FS
		{
			$ds = array_search($rsFsh["FS_ID"][$i], $rsStat["FS_ID"]);

			$rname = $rsStat["RN_NAME"][$ds];
			$fsystem = $rsStat["FS_FILESYSTEM"][$ds];
			$mpoint = $rsStat["FS_MOUNTPOINT"][$ds];
			$anz = $rsStat["ANZ"][$ds];
			$maxges = $rsStat["MAXGES"][$ds];
			$he = ($y0 - $y1) / $maxges;			//H�heneinheit zum Berechnen im Koordinatensystem
			$le = ($x1 - $x0) / ($anz - 1);		//L�ngeneinheiten zum Berechnen im Koordinatensystem

			// Legende schreiben
			$lagex = $x1 + 4;				// + 4 sind etwas Abstand von der Linie
			$lagey = $y1 + (($beschriftunghoehe + 1) * ($fs)); 
			imagestring($img, $beschriftungfontgroesse, $lagex, $lagey, $mpoint, $colArray[$fs]);
		}
		

		if ($punkt < $anz - 1)							// - 1 da f�r 5 Punkte nur 4 Striche
		{
			$xsp = $x0 + $punkt * $le;					//Lage des Startpunktes
			$ysp = $y0 - ($he * $rsFsh["FH_FREI"][$i]);
			$xep = $x0 + ($punkt + 1) * $le;				//Lage des Endpunktes
			$yep = $y0 - ($he * $rsFsh["FH_FREI"][$i + 1]);
			imageline($img, $xsp, $ysp, $xep, $yep, $colArray[$fs]);	//Ausgabe
		}
		$punkt++;

		if ($rsFsh["FS_ID"][$i] != $rsFsh["FS_ID"][$i + 1])
		{
			$punkt = 0;
			$fs++;
		}

	}

	imagepng($img, "/daten/web/admin/dateien/".$rname."_fsh.png");
	imagedestroy($img);
	
	//Rechte setzen
	chmod("/daten/web/admin/dateien/".$rname."_fsh.png", 0666);

}  // End Function

function awis_DB_Bild_Anzeigen ($SQLStmt,$Breite=300,$Hoehe=200)
{

$con = awisLogon();

$rsBild=awisBilder($con,$SQLStmt,true,false);

//$font='Arial';
//$text='Kein Bild vorhanden';

if ($rsBild!=NULL)
{
	header("Content-type: image/png");	
	$img=imagecreatefromstring($rsBild);

	// START - Aenderung zur Bildanpassung an Gr��enangabe in DB - CA 18.02.09
	
	$width=imagesx($img);
	$height=imagesy($img);

	$ratio=$width/$height;
	
	$newwidth = $Breite;
	$newheight = $Hoehe;
	
	if($newwidth/$newheight > $ratio)
	{
		$newwidth=$newheight*$ratio;	
	}
	else 
	{
		$newheight=$newwidth/$ratio;
	}
	
	// Load
	$img_new = imagecreatetruecolor($newwidth, $newheight);
	// Resize
	imagecopyresized($img_new, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	imagepng($img_new);
	imagedestroy($img_new);
	
	// ENDE - Aenderung zur Bildanpassung an Gr��enangabe in DB
	
//	imagepng($img);
//	imagedestroy($img);
}
else{
/*	$img_new=imagecreatetruecolor($Breite,$Hoehe);
	$white=imagecolorallocate($img_new,255,255,255);
	$white=imagecolorallocate($img_new,0,0,0);
	imagefilledrectangle($img_new,0,0,400,300,$white);
	imagettftext($img_new,12,50,50,50,$black,$font,$text);
	*/
	echo "<p><p><center><img src='/bilder/warnung.gif'></img></center><p><p>";
	echo "<center><b><h2>Kein Bild vorhanden</h></b></center>";
}

awisLogoff($con);
}

?>