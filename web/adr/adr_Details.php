<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ADK','%');
	$TextKonserven[]=array('ADS','%');
	$TextKonserven[]=array('ADP','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_LadelisteDrucken');
	$TextKonserven[]=array('Wort','lbl_LieferscheinDrucken');
	$TextKonserven[]=array('Wort','Abschliessen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Status = '';
	$NeuFiliale=false;
		
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht4300 = $AWISBenutzer->HatDasRecht(4300);
	if($Recht4300==0)
	{
	    awisEreignis(3,1000,'ADP',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ADK'));	
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);

	$DetailAnsicht=false;
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['ADK_FIL_ID'] = $_POST['sucADK_FIL_ID'];		
		$Param['ADK_VORGANGNR'] = $_POST['sucADK_VORGANGNR'];
		$Param['ADK_DATUMERSTELLUNG'] = $_POST['sucADK_DATUMERSTELLUNG'];
		$Param['ADK_DATUMABSCHLUSS'] = $_POST['sucADK_DATUMABSCHLUSS'];
		$Param['ADK_STATUS'] = $_POST['sucADK_STATUS'];		

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ADK",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./adr_loeschen.php');
	}
	elseif(isset($_POST['cmd_Abschliessen_x']))
	{
		include('./adr_abschliessen.php');
	}
	elseif(isset($_POST['cmdAbschliessenOK']))
	{		
		include('./adr_abschliessen.php');
	}	
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./adr_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		
		if (isset($_POST['txtADK_FIL_ID']))
		{
			$NeuFiliale=true;
			include('./adr_speichern.php');
		}				
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$AWISBenutzer->ParameterSchreiben('Formular_ADK',serialize($Param));				
	}
	elseif(isset($_GET['ADK_KEY']))
	{
		$AWIS_KEY1 = $Form->Format('N0',$_GET['ADK_KEY']);
		$Param['KEY'] = ($AWIS_KEY1<0?'':$AWIS_KEY1);
		$Param['WHERE']='';
		$Param['ORDER']='';
		$AWISBenutzer->ParameterSchreiben('Formular_ADK',serialize($Param));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_ADK',serialize($Param));			
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;			
		}
		
		$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
		if ($UserFilialen != '')
		{
			$Param['ADK_FIL_ID']=$UserFilialen;	
			$AWISBenutzer->ParameterSchreiben("Formular_ADK",serialize($Param));			
		}		
		
		$AWIS_KEY1=$Param['KEY'];
		
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ADK_FIL_ID ASC, ADK_DATUMERSTELLUNG DESC';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	$Param['ORDER']=$ORDERBY;

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT adrkopf.*';	
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM adrkopf';	
	$SQL .= ' WHERE ADK_ART = 1';	

	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}
	
	$Form->DebugAusgabe(1,$SQL,$Param);	
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ADK',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsADK = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_ADK',serialize($Param));	
	
	//Pr�fen ob User eine Filiale ist
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
		
	//Wenn eine Fililale angemeldet ist, dann gleich speichern
	if(count($FilZugriffListe)==1 and $FilZugriff != '')
	{
		$FIL_ID=$FilZugriff;
		$Form->Erstelle_HiddenFeld('ADK_FIL_ID',$FIL_ID);			
		$Form->Erstelle_HiddenFeld('ADK_KEY',$AWIS_KEY1);											
	}
		
	//********************************************************
	// Daten anzeigen
	//********************************************************	
	echo '<form name=frmadr action=./adr_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').' method=POST>';

	if($rsADK->EOF() AND !isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=-1)		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsADK->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');				
		$Breiten['ADK_FIL_ID']=80;
		$Breiten['ADK_VORGANGNR']=150;		
		$Breiten['ADK_STATUS']=150;
		$Breiten['ADK_DATUMERSTELLUNG']=170;
		$Breiten['ADK_DATUMABSCHLUSS']=170;					

		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');		
		$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ADK_VORGANGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADK_VORGANGNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_VORGANGNR'],$Breiten['ADK_VORGANGNR'],'',$Link);
	
		if(count($FilZugriffListe)>1 or $FilZugriff == '')
		{			
			$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&Sort=ADK_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADK_FIL_ID'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_FIL_ID'],$Breiten['ADK_FIL_ID'],'',$Link);
		}	
		
		$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ADK_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADK_STATUS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_STATUS'],$Breiten['ADK_STATUS'],'',$Link);
		$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ADK_DATUMERSTELLUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADK_DATUMERSTELLUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_DATUMERSTELLUNG'],$Breiten['ADK_DATUMERSTELLUNG'],'',$Link);
		$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ADK_DATUMABSCHLUSS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADK_DATUMABSCHLUSS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_DATUMABSCHLUSS'],$Breiten['ADK_DATUMABSCHLUSS'],'',$Link);		
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsADK->EOF())
		{
			$Form->ZeileStart();
			
			$Link = './adr_Main.php?cmdAktion=Details&ADK_KEY=0'.$rsADK->FeldInhalt('ADK_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');

			$Text = (strlen($rsADK->FeldInhalt('ADK_VORGANGNR'))>(int)($Breiten['ADK_VORGANGNR']/$ListenSchriftFaktor)?substr($rsADK->FeldInhalt('ADK_VORGANGNR'),0,(int)($Breiten['ADK_VORGANGNR']/$ListenSchriftFaktor)-2).'..':$rsADK->FeldInhalt('ADK_VORGANGNR'));
			$Form->Erstelle_ListenFeld('ADK_VORGANGNR',$Text,0,$Breiten['ADK_VORGANGNR'],false,($DS%2),'',$Link,'','',$rsADK->FeldInhalt('ADK_VORGANGNR'));
			
			if(count($FilZugriffListe)>1 or $FilZugriff == '')
			{
				$Text = (strlen($rsADK->FeldInhalt('ADK_FIL_ID'))>(int)($Breiten['ADK_FIL_ID']/$ListenSchriftFaktor)?substr($rsADK->FeldInhalt('ADK_FIL_ID'),0,(int)($Breiten['ADK_FIL_ID']/$ListenSchriftFaktor)-2).'..':$rsADK->FeldInhalt('ADK_FIL_ID'));
				$Form->Erstelle_ListenFeld('ADK_FIL_ID',$Text,0,$Breiten['ADK_FIL_ID'],false,($DS%2),'','','T','L',$rsADK->FeldInhalt('ADK_FIL_ID'));
			}
											
			$ADKStatus='';
			$KateogrieAlt = explode("|",$AWISSprachKonserven['ADK']['lst_ADK_STATUS']);			
			foreach ($KateogrieAlt as $Status)
			{
				$ADStatus = explode("~",$Status);								
				if ($ADStatus[0]==$rsADK->FeldInhalt('ADK_STATUS'))
				{
					$ADKStatus = $ADStatus[1];					
				}				
			}
			$Text = (strlen($ADKStatus)>(int)($Breiten['ADK_STATUS']/$ListenSchriftFaktor)?substr($ADKStatus,0,(int)($Breiten['ADK_STATUS']/$ListenSchriftFaktor)-2).'..':$ADKStatus);
			$Form->Erstelle_ListenFeld('ADK_STATUS',$Text,0,$Breiten['ADK_STATUS'],false,($DS%2),'','','','',$ADKStatus);											
						
			//$Text = (strlen($rsADK->FeldInhalt('ADK_DATUMERSTELLUNG'))>(int)($Breiten['ADK_DATUMERSTELLUNG']/$ListenSchriftFaktor)?substr($rsADK->FeldInhalt('ADK_DATUMERSTELLUNG'),0,(int)($Breiten['ADK_DATUMERSTELLUNG']/$ListenSchriftFaktor)-2).'..':$rsADK->FeldInhalt('ADK_DATUMERSTELLUNG'));
			$Form->Erstelle_ListenFeld('ADK_DATUMERSTELLUNG',$rsADK->FeldInhalt('ADK_DATUMERSTELLUNG'),0,$Breiten['ADK_DATUMERSTELLUNG'],false,($DS%2),'','','D','',$rsADK->FeldInhalt('ADK_DATUMERSTELLUNG'));
			
			//$Text = (strlen($rsADK->FeldInhalt('ADK_DATUMABSCHLUSS'))>(int)($Breiten['ADK_DATUMABSCHLUSS']/$ListenSchriftFaktor)?substr($rsADK->FeldInhalt('ADK_DATUMABSCHLUSS'),0,(int)($Breiten['ADK_DATUMABSCHLUSS']/$ListenSchriftFaktor)-2).'..':$rsADK->FeldInhalt('ADK_DATUMABSCHLUSS'));
			$Form->Erstelle_ListenFeld('ADK_DATUMABSCHLUSS',$rsADK->FeldInhalt('ADK_DATUMABSCHLUSS'),0,$Breiten['ADK_DATUMABSCHLUSS'],false,($DS%2),'','','D','',$rsADK->FeldInhalt('ADK_DATUMABSCHLUSS'));
			
			$Form->ZeileEnde();

			$rsADK->DSWeiter();
			$DS++;
		}

		$Link = './adr_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsADK->FeldInhalt('ADK_KEY');
		
		$Status = $rsADK->FeldInhalt('ADK_STATUS');
		$Form->DebugAusgabe(1,$Status);
		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_ADK',serialize($Param));

		echo '<input type=hidden name=txtADK_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adr_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsADK->FeldInhalt('ADK_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsADK->FeldInhalt('ADK_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht4300&2)!=0);		
		
		$EditRechtNeu = false;
		If ($EditRecht == true and $AWIS_KEY1 == 0)
		{
			$EditRechtNeu = true;
		}
		
		// �berschrift
		$Form->Erstelle_HiddenFeld('ADK_KEY',$rsADK->FeldInhalt('ADK_KEY'));		
		
		$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
		$FilZugriffListe=explode(',',$FilZugriff);
						
		if(count($FilZugriffListe)>1)
		{
			$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
			$SQL .= ' FROM Filialen ';
			$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
			$SQL .= ' ORDER BY FIL_BEZ';
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_FIL_ID'].':',150);
			$Form->Erstelle_SelectFeld('ADK_FIL_ID',($AWIS_KEY1==0?'':$rsADK->FeldInhalt('ADK_FIL_ID')),200,$EditRechtNeu,$SQL,$OptionBitteWaehlen);
			$AWISCursorPosition='txtADK_FIL_ID';
			$Form->ZeileEnde();
		}
		elseif($FilZugriff!='')
		{
			$FIL_ID=$FilZugriff;
			$Form->Erstelle_HiddenFeld('ADK_FIL_ID',($AWIS_KEY1==0?$FIL_ID:$rsADK->FeldInhalt('ADK_FIL_ID')));
			//$Form->Erstelle_TextLabel($FIL_ID,200);
		}
		else
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_FIL_ID'].':',150);
			$Form->Erstelle_TextFeld('ADK_FIL_ID',($AWIS_KEY1==0?'':$rsADK->FeldInhalt('ADK_FIL_ID')),4,200,$EditRechtNeu,'','','','T','L','','',10);
			$AWISCursorPosition='txtADK_FIL_ID';
			$Form->ZeileEnde();
		}					

		$Form->ZeileStart();	
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_VORGANGNR'].':',150);
		$Form->Erstelle_TextFeld('ADK_VORGANGNR',$rsADK->FeldInhalt('ADK_VORGANGNR'),20,200,false,'','','','T');		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_DATUMERSTELLUNG'].':',150);
		$Form->Erstelle_TextFeld('ADK_DATUMERSTELLUNG',$rsADK->FeldInhalt('ADK_DATUMERSTELLUNG'),20,200,false,'','','','D');						
		$Form->ZeileEnde();
				
		if ($AWIS_KEY1!=0)
		{
			$Form->Erstelle_HiddenFeld('ADK_FIL_ID',$rsADK->FeldInhalt('ADK_FIL_ID'));
			$Form->Erstelle_HiddenFeld('ADK_VORGANGNR',$rsADK->FeldInhalt('ADK_VORGANGNR'));
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_STATUS'].':',150);
		$KateogrieAlt = explode("|",$AWISSprachKonserven['ADK']['lst_ADK_STATUS']);
		$Form->Erstelle_SelectFeld('ADK_STATUS',$rsADK->FeldInhalt('ADK_STATUS'),200,false,'','','','','',$KateogrieAlt,'');				
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_DATUMABSCHLUSS'].':',150);
		$Form->Erstelle_TextFeld('ADK_DATUMABSCHLUSS',$rsADK->FeldInhalt('ADK_DATUMABSCHLUSS'),20,200,false,'','','','D');		
		$Form->ZeileEnde();

		$Form->Trennzeile('L');
		
		if (!isset($_POST['cmdDSNeu_x']) OR $NeuFiliale==true)
		{	
			//*********************************************************
			//* Sortierung
			//*********************************************************
			if(!isset($_GET['SSort']))
			{
				$ORDERBY = ' ORDER BY ADS_LAGABEZ';				
			}
			else
			{
				$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);				
			}
								
			$SQL = 'SELECT ADRST.*, ADRPOS.*';		
			$SQL .= ' FROM ADRSTAMM ADRST ';	
			if ($Status == 'A')
			{
				$SQL .= ' INNER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY AND ADP_ADK_KEY ='.$AWIS_KEY1;	
			}
			else 
			{
				$SQL .= ' LEFT OUTER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY AND ADP_ADK_KEY ='.$AWIS_KEY1;	
			}
			$SQL .= ' WHERE ADS_ADRART='.$DB->FeldInhaltFormat('T',$rsADK->FeldInhalt('ADK_ART'));						
			
			$SQL .= $ORDERBY;	
			
			$rsADP = $DB->RecordsetOeffnen($SQL);
			
			$Form->ZeileStart();
			$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&SSort=ADS_LAGABEZ'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ADS_LAGABEZ'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_LAGABEZ'],300,'',$Link);			
			$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&SSort=ADS_BEHTYP'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ADS_BEHTYP'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_BEHTYP'],100,'',$Link);			
			$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&SSort=ADP_MENGE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ADP_MENGE'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADP']['ADP_MENGE'],100,'',$Link);						
			$Link = './adr_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Link .= '&SSort=ADP_GEWICHT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ADP_GEWICHT'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADP']['ADP_GEWICHT'],108,'',$Link);						
			$Form->ZeileEnde();
	
			$DS=0;
			while(!$rsADP->EOF())
			{
				//$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');								
				$Form->ZeileStart();								
				$Form->Erstelle_ListenFeld('ADS_LAGABEZ',$rsADP->FeldInhalt('ADS_LAGABEZ'),0,300,false,($DS%2),'','','T','L',$rsADP->FeldInhalt('ADS_EWCCODE').' - '.$rsADP->FeldInhalt('ADS_EWCBEZ'));
				$Form->Erstelle_ListenFeld('ADS_BEHTYP',$rsADP->FeldInhalt('ADS_BEHTYP'),0,100,false,($DS%2));
				$AendernRecht=(($Status=='O') OR (($Recht4300&32)!=0));							
				$Form->Erstelle_ListenFeld('ADP_MENGE_'.$rsADP->FeldInhalt('ADS_KEY').'_'.$rsADP->FeldInhalt('ADP_KEY'),$rsADP->FeldInhalt('ADP_MENGE'),5,100,$AendernRecht,($DS%2),'','','T','L','');				
				//$Form->Erstelle_ListenFeld('ADP_MENGE_'.$rsADP->FeldInhalt('ADS_KEY').'_'.$rsADP->FeldInhalt('ADP_KEY'),$rsADP->FeldInhalt('ADP_MENGE'),5,100,($Status=='O'?true:false),($DS%2),'','','T','L','');				
				$Form->Erstelle_ListenFeld('ADP_GEWICHT',($rsADP->FeldInhalt('ADP_GEWICHT')!=''?$Form->Format('N2',$rsADP->FeldInhalt('ADP_GEWICHT')).' kg':''),0,100,false,($DS%2),'','','T');								
				$Form->Erstelle_ListenFeld('XXX','',0,0,false,($DS%2));								
				$Form->ZeileEnde();
				
				$rsADP->DSWeiter();
				$DS++;
			}
		}		
		$Form->Formular_Ende();		
	
	}
		
	//Pr�fen ob User eine Filiale ist
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
	
	//Wenn eine Fililale angemeldet ist, dann gleich speichern
	if(count($FilZugriffListe)==1 and $FilZugriff != '')
	{
		$FIL_ID=$FilZugriff;
		$Form->Erstelle_HiddenFeld('ADK_FIL_ID',$FIL_ID);			
		$Form->Erstelle_HiddenFeld('ADK_KEY',$AWIS_KEY1);											
	}
		
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht4300&6)!=0  AND $DetailAnsicht AND ($Status == 'O' or $AWIS_KEY1==0)) OR ($Recht4300&32)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if((($Recht4300&4)!=0  AND $DetailAnsicht==false))
	{					
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	if((($Recht4300&8)!=0  AND $DetailAnsicht  AND $Status == 'O'))
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
	}
	
	if((($Recht4300&16)!=0  AND $DetailAnsicht AND $Status == 'O'))
	{		
		$SQL = 'SELECT *';
		$SQL .=' FROM ADRPOS ADP';
		$SQL .=' WHERE ADP_ADK_KEY='.intval($AWIS_KEY1);	
	
		$rsADP=$DB->RecordSetOeffnen($SQL);		
			
		if ($rsADP->AnzahlDatensaetze()>0)
		{				
				$Form->Schaltflaeche('image', 'cmd_Abschliessen','','/bilder/cmd_korb_runter.png', $AWISSprachKonserven['Wort']['Abschliessen'],'A');			
		}					
	}
	
	//if ($DetailAnsicht==true)
	//{
	//	$Form->Schaltflaeche('href', 'cmd_DruckLadeListe','./adr_ladeliste_pdf.php?ADK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_notizblock.png', $AWISSprachKonserven['Wort']['lbl_LadelisteDrucken'],'.');		
	//}
	
	if(($Recht4300&6)!=0  AND $DetailAnsicht AND $Status == 'A')
	{
		if ($rsADK->FeldInhalt('ADK_ART')==1)
		{
			$Form->Schaltflaeche('href', 'cmd_DruckLieferschein','./adr_lieferschein_1_pdf.php?ADK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_LieferscheinDrucken'],'.');
		}
		else 
		{
			$Form->Schaltflaeche('href', 'cmd_DruckLieferschein','./adr_lieferschein_3_pdf.php?ADK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_LieferscheinDrucken'],'.');
		}
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151032");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151033");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ADK_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['ADK_FIL_ID']) AND $Param['ADK_FIL_ID']!='')
	{
		$Bedingung .= ' AND (ADK_FIL_ID =' .$DB->FeldInhaltFormat('N0',$Param['ADK_FIL_ID']).')';
	}
	
	if(isset($Param['ADK_STATUS']) AND $Param['ADK_STATUS']!='0')
	{
		$Bedingung .= ' AND (ADK_STATUS ' .$DB->LikeOderIst($Param['ADK_STATUS'],awisDatenbank::AWIS_LIKE_UPPER ).')';				
	}
	
	if(isset($Param['ADK_VORGANGNR']) AND $Param['ADK_VORGANGNR']!='')
	{
		if(strlen($Param['ADK_VORGANGNR'])==13)
		{
			$Bedingung .= ' AND (ADK_VORGANGNR =' .substr($DB->FeldInhaltFormat('N0',$Param['ADK_VORGANGNR']),9,3).')';
		}
		else 
		{
			$Bedingung .= ' AND (ADK_VORGANGNR =' .$DB->FeldInhaltFormat('N0',$Param['ADK_VORGANGNR']).')';
		}
	}
	
	if(isset($Param['ADK_DATUMERSTELLUNG']) AND $Param['ADK_DATUMERSTELLUNG']!='')		
	{
		$Bedingung .= "AND to_date(to_char(ADK_DATUMERSTELLUNG,'DD.MM.RRRR'),'DD.MM.RRRR') = " . $DB->FeldInhaltFormat('D',$Param['ADK_DATUMERSTELLUNG']) . ' ';
	}
		
	if(isset($Param['ADK_DATUMABSCHLUSS']) AND $Param['ADK_DATUMABSCHLUSS']!='')		
	{
		$Bedingung .= "AND to_date(to_char(ADK_DATUMABSCHLUSS,'DD.MM.RRRR'),'DD.MM.RRRR') = " . $DB->FeldInhaltFormat('D',$Param['ADK_DATUMABSCHLUSS']) . ' ';
	}		

	return $Bedingung;
}
?>