<?php

/*********
* Abteilungsliste
****************/

global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con,153);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Listen Telefonverzeichnis',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}
require_once('fpdf.php');
require_once 'fpdi.php';

/***************************************
* Neue PDF Datei erstellen
***************************************/
define('FPDF_FONTPATH','font/');
$pdf = new fpdi('p','mm','a4');
$pdf->open();
// ATU Logo als Hintergrundbild laden
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);

/***************************************
*
* Daten laden
*
***************************************/

$Bedingung='';
		// Daten suchen
if($_POST['txtKategorie']!='-1')
{
	$Bedingung .= ' AND KON_KKA_KEY=0' . $_POST['txtKategorie'];
}
if($_POST['txtBereich']!='-1')
{
	$Bedingung .= ' AND KON_KBE_KEY=0' . $_POST['txtBereich'];
}
if($_POST['txtGebaeude']!='-1')
{
	$Bedingung .= ' AND KON_GEB_ID=0' . $_POST['txtGebaeude'];
}

if($_POST['txtAbteilung']!='-1')
{
	$Bedingung .= ' AND KON_KEY IN (SELECT KZA_KON_KEY FROM KontakteAbteilungenZuordnungen WHERE KZA_KAB_KEY=0' . $_POST['txtAbteilung'] . ')';
}

	// Filialliste
if(isset($_POST['chkFilialen']))
{
	$Bedingung .= " AND BITAND(KON_LISTEN,2)=2";
}
	// �ffentliche Daten
if(isset($_POST['chkOeffentliche']))
{
	$Bedingung .= " AND BITAND(KON_LISTEN,1)=1";
}

$SQL = "SELECT  DISTINCT GEB_ID, GEB_Bezeichnung";
$SQL .= " FROM KontakteAbteilungenZuordnungen INNER JOIN Kontakte ON KZA_KON_KEY = KON_KEY";
$SQL .= "      INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY";
$SQL .= "      LEFT OUTER JOIN GEBAEUDE ON KON_GEB_ID = GEB_ID";
if($Bedingung!='')
{
	$SQL .= " WHERE " . substr($Bedingung,4) . "";
}
$SQL .= " ORDER BY GEB_ID";
		// Osrtliste aufbauen
$rsGebaeude=awisOpenRecordset($con,$SQL);
$rsGebaeudeZeilen = $awisRSZeilen;
$OrtsListe = 'Orte: ';
for($i=0;$i<$rsGebaeudeZeilen;$i++)
{
	$OrtsListe .= $rsGebaeude["GEB_ID"][$i] . "=" . $rsGebaeude["GEB_BEZEICHNUNG"][$i] . " // ";
}

$Bedingung .= ' AND KON_STATUS = \'A\'';

$SQL = "SELECT DISTINCT KAB_KEY, KAB_ABTEILUNG, KON_KEY, KON_NAME1, KON_NAME2, GEB_ID, KON_ZUSTAENDIGKEIT, KZA_BEMERKUNG, KZA_LISTENHINWEIS";
$SQL .= " FROM KontakteAbteilungenZuordnungen INNER JOIN Kontakte ON KZA_KON_KEY = KON_KEY";
$SQL .= "      INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY";
$SQL .= "      LEFT OUTER JOIN GEBAEUDE ON KON_GEB_ID = GEB_ID";
if($Bedingung!='')
{
	$SQL .= " WHERE " . substr($Bedingung,4) . "";
}
$SQL .= " ORDER BY KAB_ABTEILUNG, KON_NAME1, KON_NAME2";

$rsKON = awisOpenRecordset($con,$SQL);
$rsKONZeilen = $awisRSZeilen;

if($rsKONZeilen==0)			// Keine Daten gefunden
{
	$LinkerRand = 15;
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,150,4,20);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(170,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->SetFont('Arial','',5);					// Schrift setzen
				// �berschrift
	$pdf->MultiCell(190,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
}
else 						// Daten gefunden
{
	$NeueSeite = true;
	$Zeile = 0;
	$LinkerRand = 15;
	$LetzterBlock = '';

	NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand,$OrtsListe);

	for($KONZeile=0;$KONZeile<$rsKONZeilen;$KONZeile++)
	{
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);				// Schrift setzen

		$ZeilenHoehe = 1;	// Eine Zeile hoch



			// Daten sammeln und H�hen berechnen
		$AusgabeZus = $rsKON['KON_ZUSTAENDIGKEIT'][$KONZeile];
		$foo = $pdf->GetStringWidth($AusgabeZus);
		$ZeilenHoeheZus = round(($foo/58)+0.5,0);			// H�he nach der Textausgabe steuern
		$ZeilenHoeheZus=($ZeilenHoeheZus==0?1:$ZeilenHoeheZus);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheZus)?$ZeilenHoeheZus:$ZeilenHoehe;

		$AusgabeTel = '';
		$rsKonTel = awisOpenRecordset($con, "SELECT KKO_KON_KEY, KKO_WERT FROM KONTAKTEKOMMUNIKATION WHERE KKO_KOT_KEY=9 AND KKO_KON_KEY=0" . $rsKON['KON_KEY'][$KONZeile]);
		for($ZeilenHoeheTel=0;$ZeilenHoeheTel<$awisRSZeilen;$ZeilenHoeheTel++)
		{
			$AusgabeTel .= "\n " . $rsKonTel['KKO_WERT'][$ZeilenHoeheTel];
		}
		$ZeilenHoeheTel=($ZeilenHoeheTel==0?1:$ZeilenHoeheTel);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheTel)?$ZeilenHoeheTel:$ZeilenHoehe;


		$AusgabeFax = '';
		$rsKonTel = awisOpenRecordset($con, "SELECT KKO_KON_KEY, KKO_WERT FROM KONTAKTEKOMMUNIKATION WHERE KKO_KOT_KEY=10 AND KKO_KON_KEY=0" . $rsKON['KON_KEY'][$KONZeile]);
		for($ZeilenHoeheFax=0;$ZeilenHoeheFax<$awisRSZeilen;$ZeilenHoeheFax++)
		{
			$AusgabeFax .= "\n " . $rsKonTel['KKO_WERT'][$ZeilenHoeheFax];
		}
		$ZeilenHoeheFax=($ZeilenHoeheFax==0?1:$ZeilenHoeheFax);			// Mindesth�he ist 1
		$ZeilenHoehe = ($ZeilenHoehe<$ZeilenHoeheFax)?$ZeilenHoeheFax:$ZeilenHoehe;



		if($LetzterBlock!=$rsKON['KAB_ABTEILUNG'][$KONZeile])
		{
			if($Zeile > (275-($ZeilenHoehe*4)))		// Seitenumbruch n�tig?
			{
				NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand,$OrtsListe);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',8);				// Schrift setzen
			}
			$pdf->setXY($LinkerRand,$Zeile);							// Cursor setzen
			$pdf->SetFont('Arial','B',12);
			$pdf->MultiCell(190,5,$rsKON['KAB_ABTEILUNG'][$KONZeile],1,'L',1);
			$Zeile+=5;
			$LetzterBlock=$rsKON['KAB_ABTEILUNG'][$KONZeile];
			$pdf->SetFont('Arial','',8);				// Schrift setzen
		}

		if($Zeile > (280-($ZeilenHoehe*4)))		// Seitenumbruch n�tig?
		{
			//$NeueSeite=true;
			NeueSeite($pdf,$Zeile,$ATULogo,$LinkerRand,$OrtsListe);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetFont('Arial','',8);				// Schrift setzen
		}

		// Daten ausgeben
		$pdf->setXY($LinkerRand,$Zeile);							// Cursor setzen
		$foo = $rsKON['KON_NAME1'][$KONZeile] . ($rsKON['KON_NAME2'][$KONZeile]==''?'':', '.$rsKON['KON_NAME2'][$KONZeile]);
		$pdf->MultiCell(90,$ZeilenHoehe*4,$foo,1,'L',1);

		$pdf->setXY($LinkerRand+90,$Zeile);							// Zust�ndigkeit
		$pdf->MultiCell(60,($ZeilenHoehe-$ZeilenHoeheZus+1)*4,$AusgabeZus,1,'L',1);

		$pdf->setXY($LinkerRand+150,$Zeile);						// Ort
		$pdf->MultiCell(10,$ZeilenHoehe*4,$rsKON['GEB_ID'][$KONZeile],1,'C',1);

		$pdf->setXY($LinkerRand+160,$Zeile);						// Tel
		$pdf->MultiCell(15,($ZeilenHoehe-$ZeilenHoeheTel+1)*4,substr($AusgabeTel,2),1,'C',1);

		$pdf->setXY($LinkerRand+175,$Zeile);						// Fax
		$pdf->Multicell(15,($ZeilenHoehe-$ZeilenHoeheFax+1)*4,substr($AusgabeFax,2),1,'C',1);


		$Zeile += $ZeilenHoehe*4;


	}	// Datens�tze

	$pdf->setXY($LinkerRand,285);					// Cursor setzen
	$pdf->SetFont('Arial','',6);					// Schrift setzen
			// �berschrift
	$pdf->MultiCell(160,3,$OrtsListe,0,'L',0);
	$pdf->setXY(190,285);					// Cursor setzen
	$pdf->Cell(40,3,'Seite ' . $pdf->PageNo(),0,0,'L',0);
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->SetFont('Arial','',5);					// Schrift setzen
				// �berschrift
	$pdf->MultiCell(190,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);


}	// Ende Datenaufbereitung


/***************************************
* Abschluss und Datei speichern
***************************************/

$DateiName = awis_UserExportDateiName('.pdf');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];
//$pdf->saveas($DateiName);
$pdf->Output($DateiName,'F');
echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";

echo "<br><br><a href=./telefon_Main.php?cmdAktion=Ausdrucke>Neue Suche</a>";


/**
*
* Funktion erzeugt eine neue Seite mit �berschriften
*
* @author Sacha Kerres
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  string OrtsListe
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,$OrtsListe)
{
	if($pdf->PageNo() > 0)		// Fu�zeile?
	{
		$pdf->setXY($LinkerRand,285);					// Cursor setzen
		$pdf->SetFont('Arial','',6);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(160,3,$OrtsListe,0,'L',0);
		$pdf->setXY(190,285);					// Cursor setzen
		$pdf->Cell(40,3,'Seite ' . $pdf->PageNo(),0,0,'L',0);

		$pdf->setXY($LinkerRand,290);					// Cursor setzen
		$pdf->SetFont('Arial','',5);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(190,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);

	}

	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,185,4,20);		// Logo einbauen

	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','',14);				// Schrift setzen
				// �berschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(190,6,"Telefonliste ATU - Zentrale Weiden nach Abteilungen",0,0,'C',0);

	$pdf->setXY($LinkerRand,10);				// Cursor setzen
	$pdf->SetFont('Arial','',8);				// Schrift setzen
	$pdf->cell(190,6,"Stand per " . date('d.m.Y'),0,0,'C',0);


	// �berschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->setXY($LinkerRand,18);				// Cursor setzen
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	$pdf->cell(90,6,"Mitarbeiter/-in",1,0,'C',1);

	$pdf->setXY(105,18);							// Cursor setzen
	$pdf->cell(60,6,"Zust�ndigkeit",1,0,'C',1);

	$pdf->setXY(165,18);						// Cursor setzen
	$pdf->cell(10,6,"Ort",1,0,'C',1);

	$pdf->setXY(175,18);						// Cursor setzen
	$pdf->cell(15,6,"Tel",1,0,'C',1);

	$pdf->setXY(190,18);						// Cursor setzen
	$pdf->cell(15,6,"Fax",1,0,'C',1);

	$Zeile = 24;
}

?>