<?php
require_once('/daten/include/awisZukauf_HESS.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;
    $WANummer = '';
    $Abfrage = 'Bestellung';
    $AllesLesen = false;
    $DebugModus = false;

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--testsystem':
                            $Testsystem = true;
                            break;
                    case '--wanummer':
                            $WANummer = $argv[++$i];
	 	            $AllesLesen = true;
                            break;
                    case '--starttag':
                            $StartTag = $argv[++$i];
                            break;
                    case '--endtag':
                            $EndTag = $argv[++$i];
	 	            $AllesLesen = true;
                            break;
                    case '--abfrage':
                            $Abfrage = $argv[++$i];
	 	            $AllesLesen = true;
                            break;
                    case '--debugmodus':
                        $DebugModus = true;
                        break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --testsystem                       Daten aus dem Testsystem abfragen.'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                            echo '  --wanummer  <Nr>                   WANummer fuer die Abfrage.'.PHP_EOL;
                            echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.'.PHP_EOL;
                            echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.'.PHP_EOL;
                            echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung, Lieferschein oder Rechnung.'.PHP_EOL;

                            echo PHP_EOL;
                            die();
            }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    
    $ZUK = new awisZukauf_HESS($DebugLevel,$Testsystem);

    if($DebugModus){
        $ZUK->DebugModus();
    }

    if($StartTag != '')
    {
        $StartTag = date('d.m.y',$Form->PruefeDatum($StartTag,false,false,true));
        $EndTag = date('d.m.y',$Form->PruefeDatum($EndTag,false,false,true));
    }


    switch($Abfrage)
    {
        case 'Bestellung':
            echo 'Lese Bestellungen...'.PHP_EOL;

            if($StartTag!='')
            {
                $StartTag = $Form->PruefeDatum($StartTag,false,false,true);
                $EndTag = $Form->PruefeDatum($EndTag,false,false,true);
                for($Tag=$StartTag;$Tag<=$EndTag;$Tag+=(60*60*24))
                {
		    echo '  rufe Tag '.date('d.m.y',$Tag).' ab....'.PHP_EOL;
                    if($ZUK->LeseBestellungen($WANummer, date('d.m.y',$Tag), date('d.m.y',$Tag),($AllesLesen!=''?'A':''),'')>0)
                    {
                            $ZUK->SpeichereBestellungen();
                    }
                }
            }
            else
            {
                if($ZUK->LeseBestellungen($WANummer, '', '',($WANummer!=''?'A':''),'')>0)
                {
                        $ZUK->SpeichereBestellungen();
                }
            }
            break;
        case 'Lieferschein':
            if($StartTag!='')
            {
                $StartTag = $Form->PruefeDatum($StartTag,false,false,true);
                $EndTag = $Form->PruefeDatum($EndTag,false,false,true);

                for($Tag=$StartTag;$Tag<=$EndTag;$Tag+=(60*60*24))
                {
                    if($ZUK->LeseLieferscheine(date('d.m.y',$Tag),date('d.m.y',$Tag),'',$WANummer,'',($WANummer!=''?'A':''))>0)
                    {
                        $ZUK->SpeichereLieferscheine();
                    }
                }
            }
            else
            {
                    if($ZUK->LeseLieferscheine('','','',$WANummer,'',($WANummer!=''?'A':''))>0)
                    {
                        $ZUK->SpeichereLieferscheine();
                    }
            }
            break;
        case 'Rechnung':

            if($StartTag != '')
            {
                $StartTag = $Form->PruefeDatum($StartTag,false,false,true);
                $EndTag = $Form->PruefeDatum($EndTag,false,false,true);

                for($Tag=$StartTag;$Tag<=$EndTag;$Tag+=(60*60*24))
                {
                    if($ZUK->LeseRechnungen(date('d.m.y',$Tag),date('d.m.y',$Tag),'',$WANummer,'',($WANummer!=''?'A':''))>0)
                    {
                        $ZUK->SpeichereRechnungen();
                    }
               }
            }
            else
            {
                if($ZUK->LeseRechnungen('','','',$WANummer,'',($WANummer!=''?'A':''))>0)
                {
                    $ZUK->SpeichereRechnungen();
                }
            }
            break;

	default:
	    echo 'Unbekannter Typ '.PHP_EOL;
	    break;
    }
}
catch (awisException $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'SQL: '.$ex->getSQL().PHP_EOL;
}
catch (Exception $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
}
?>
