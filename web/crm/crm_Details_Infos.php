<?php
global $AWISBenutzer;
global $AWIS_KEY1;

$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('IMQ','IMQ_IMPORTQUELLE');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3700 = awisBenutzerRecht($con,3700,$AWISBenutzer->BenutzerName());
if($Recht3700==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//$AWIS_KEY1 = awis_BenutzerParameter($con, "AktuellerCAD" , $AWISBenutzer->BenutzerName());

$SQL = 'SELECT CRMAdressen.*, IMQ_IMPORTQUELLE';
$SQL .= ' FROM CRMAdressen';
$SQL .= ' LEFT OUTER JOIN Importquellen ON CAD_IMQ_ID = IMQ_ID';
$SQL .= ' WHERE CAD_KEY=:var_N0_CAD_KEY';
$BindeVariablen=array();
$BindeVariablen['var_N0_CAD_KEY']=$AWIS_KEY1;
$rsCAD = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);

if(isset($rsCAD['CAD_KEY'][0]))
{
	awis_FORM_FormularStart();

	$EditRecht=(($Recht3700&2)!=0);

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_STEUERNUMMER'].':',150);
	awis_FORM_Erstelle_TextFeld('CAD_STEUERNUMMER',$rsCAD['CAD_STEUERNUMMER'][0],20,200,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_USTIDNR'].':',150);
	awis_FORM_Erstelle_TextFeld('CAD_USTIDNR',$rsCAD['CAD_USTIDNR'][0],20,200,$EditRecht);
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_HANDELSREGISTER'].':',150);
	awis_FORM_Erstelle_TextFeld('CAD_HANDELSREGISTER',$rsCAD['CAD_HANDELSREGISTER'][0],20,200,$EditRecht);
	awis_FORM_ZeileEnde();
	
	/* Auf Wunsch FA ausblenden
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_MITARBEITER'].':',150);
	awis_FORM_Erstelle_TextFeld('CAD_MITARBEITER',$rsCAD['CAD_MITARBEITER'][0],10,200,$EditRecht);
	awis_FORM_ZeileEnde();
	*/
	
	$SQL = 'SELECT *';
	$SQL .= ' FROM CRMInfoTypen';
	$SQL .= ' LEFT OUTER JOIN CRMInfos ON CIT_ID = CIN_CIT_ID AND CIN_CAD_KEY=:var_N0_CIN_CAD_KEY';
	$SQL .= ' WHERE CIT_SEITE = \'Infos\' AND CIT_STATUS=\'A\'';
	$SQL .= ' AND CIT_ID <> 104';
	$SQL .= ' ORDER BY CIT_BEREICH, CIT_SORTIERUNG';
	$BindeVariablen=array();
	$BindeVariablen['var_N0_CIN_CAD_KEY']=$AWIS_KEY1;
	$rsCIN = awisOpenRecordset($con, $SQL,'','',$BindeVariablen);
	$rsCINZeilen = $awisRSZeilen;
	
	$LetzterBereich = '';
	for($CINZeile=0;$CINZeile<$rsCINZeilen;$CINZeile++)
	{
		if($LetzterBereich!=$rsCIN['CIT_BEREICH'][$CINZeile])
		{
			awis_FORM_Trennzeile();
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_Liste_Ueberschrift($rsCIN['CIT_BEREICH'][$CINZeile],0,'','');
			awis_FORM_ZeileEnde();
			$LetzterBereich=$rsCIN['CIT_BEREICH'][$CINZeile];
		}
		awis_FORM_ZeileStart();
		echo '<input type=hidden name=txtCIN_KEY value=0'.$rsCIN['CIN_KEY'][$CINZeile].'>';
		awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][$CINZeile].':',200);
		if($rsCIN['CIT_DATENQUELLE'][$CINZeile]!='')
		{
			switch(substr($rsCIN['CIT_DATENQUELLE'][$CINZeile],0,3))
			{
				case 'TXT':
					$Felder = explode(':',$rsCIN['CIT_DATENQUELLE'][$CINZeile]);
					$Daten = awis_LadeTextKonserven($con, array(array($Felder[1],$Felder[2])), $AWISSprache);
					$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
					awis_FORM_Erstelle_SelectFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,$con,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
					break;
				case 'SQL':
					$Felder = explode(':',$rsCIN['CIT_DATENQUELLE'][$CINZeile]);
					awis_FORM_Erstelle_SelectFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,$con,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
					break;
				default:
					awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_ZEICHEN'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,'','','',$rsCIN['CIT_FORMAT'][$CINZeile],'','',$rsCIN['CIT_DATENQUELLE'][$CINZeile]);
					break;
			}
			
		}
		else 
		{
			
			awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_ZEICHEN'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditRecht,'','','',$rsCIN['CIT_FORMAT'][$CINZeile]);
		}
		awis_FORM_ZeileEnde();
	}
	
	
	
	
	
			// Importquelle bei nicht manuell erfassten Daten anzeigen
	if($rsCAD['IMQ_IMPORTQUELLE'][0]!=4)
	{
		awis_FORM_Trennzeile();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['IMQ']['IMQ_IMPORTQUELLE'].':',200);
		awis_FORM_Erstelle_TextFeld('*IMQ_IMPORTQUELLE',$rsCAD['IMQ_IMPORTQUELLE'][0],10,400,false);
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_IMPORTID'].':',150);
		awis_FORM_Erstelle_TextFeld('*CAD_IMPORTID',$rsCAD['CAD_IMPORTID'][0],10,200,false);
		awis_FORM_ZeileEnde();
		
		
		$SQL = 'SELECT *';
		$SQL .= ' FROM CRMInfoTypen';
		//$SQL .= ' LEFT OUTER JOIN CRMInfos ON CIT_ID = CIN_CIT_ID AND CIN_CAD_KEY='.$AWIS_KEY1;
		$SQL .= ' INNER JOIN CRMInfos ON CIT_ID = CIN_CIT_ID AND CIN_CAD_KEY=:var_N0_CIN_CAD_KEY';
		$SQL .= ' WHERE CIT_SEITE = \'Infos\' AND CIT_STATUS=\'A\'';
		//nur Importquelle
		$SQL .= ' AND CIT_ID = 104';
		$SQL .= ' ORDER BY CIT_BEREICH, CIT_SORTIERUNG';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_CIN_CAD_KEY']=$AWIS_KEY1;
		$rsCIN = awisOpenRecordset($con, $SQL,'','',$BindeVariablen);
		$rsCINZeilen = $awisRSZeilen;
					
		for($CINZeile=0;$CINZeile<$rsCINZeilen;$CINZeile++)
		{			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][$CINZeile].':',200);
			awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_ZEICHEN'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],false,'','','',$rsCIN['CIT_FORMAT'][$CINZeile]);
			awis_FORM_ZeileEnde();
		}

		
		$SQL = 'SELECT XDI_DATEINAME, XDI_USER, XDI_USERDAT';
		$SQL .= ' FROM IMPORTPROTOKOLL';
		$SQL .= ' INNER JOIN CRMINFOS ON CIN_CIT_ID = 3005 AND CAST(CIN_WERT AS NUMBER(10,0))= XDI_KEY AND CIN_CAD_KEY = :var_N0_CIN_CAD_KEY';
		$SQL .= ' ORDER BY 3';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_CIN_CAD_KEY']=$AWIS_KEY1;
		$rsCIN = awisOpenRecordset($con, $SQL,'','',$BindeVariablen);
		$rsCINZeilen = $awisRSZeilen;
					
		for($CINZeile=0;$CINZeile<$rsCINZeilen;$CINZeile++)
		{			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($rsCIN['XDI_USERDAT'][$CINZeile],200);
			awis_FORM_Erstelle_TextLabel($rsCIN['XDI_USER'][$CINZeile],200);
			awis_FORM_Erstelle_TextLabel($rsCIN['XDI_DATEINAME'][$CINZeile],200);
			awis_FORM_ZeileEnde();
		}
	}
}
?>