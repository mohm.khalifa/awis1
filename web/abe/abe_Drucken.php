<?php
global $AWIS_KEY1;
global $ABE;
try {
    
    $ABE->Form->Formular_Start();
    echo '<form name=frmabe action=./abe_Main.php?cmdAktion=Drucken method=POST>';
    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABE']['ABE_DRUCKTYPEN'].': ',170);
    $Daten = explode('|',$ABE->AWISSprachKonserven['ABE']['ABE_LST_DRUCKTYPEN']);
    $ABE->Form->Erstelle_SelectFeld('Drucktyp',isset($_POST['txtDrucktyp'])?$_POST['txtDrucktyp']:'','100:100',true,'','','','','',$Daten,'onchange="filter();"');
    $ABE->Form->ZeileEnde();

    $Script = '
        <script>
        $(document).ready(function(){
            filter();
        });
   
        function filter(){
            if($("#txtDrucktyp option:selected").val() == 1){
                $("#filter").hide();
            }else{
                $("#filter").show();
            }
            
        }
        
        //Alle anchecken..
        $( document ).ready(function() {
            $("#txtcheckAlle").change(function() {
                $(".anzeigen_chk").prop(\'checked\',  $("#txtcheckAlle").prop(\'checked\'));
            });  
        });  
        </script>
    ';

    echo $Script;



    $ABE->Form->SchreibeHTMLCode('<div id="filter">');


    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['Wort']['DatumVom'].':',170);
    $ABE->Form->Erstelle_TextFeld('ABE_DRUCKDATUM_VON',isset($_POST['txtABE_DRUCKDATUM_VON'])?$_POST['txtABE_DRUCKDATUM_VON']:'',10,150,true,'','','','D','','',date('d')-1 . '.'. date('m') . '.'. date('Y'));
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['Wort']['DatumBis'].':',100);
    $ABE->Form->Erstelle_TextFeld('ABE_DRUCKDATUM_BIS',isset($_POST['txtABE_DRUCKDATUM_BIS'])?$_POST['txtABE_DRUCKDATUM_BIS']:'',10,150,true,'','','','D','','',date('d.m.Y'));
    $ABE->Form->ZeileEnde();

    if(($ABE->Recht51000&16)==16){ //Auftr�ge anderer User mit drucken?
        $ABE->Form->ZeileStart();
        $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABE']['ABE_AUFTRAEGE_ANDERER'].':',170);
        $ABE->Form->Erstelle_Checkbox('AndereUser',isset($_POST['txtAndereUser'])?'1':0,20,true,1);
        $ABE->Form->ZeileEnde();
    }

    $ABE->Form->ZeileStart();
    $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABE']['ABE_BEREITS_GEDRUCKTE'].':',170);
    $ABE->Form->Erstelle_Checkbox('BereitsGedruckte',isset($_POST['txtBereitsGedruckte'])?'1':0,20,true,1);
    $ABE->Form->ZeileEnde();

    $ABE->Form->SchreibeHTMLCode('</div>');
    $Drucken = false;
    if(isset($_POST['txtDrucktyp'])){
        $ABE->Form->ZeileStart();
        $ABE->Form->Trennzeile('L');
        $ABE->Form->ZeileEnde();

        $ABE->Form->ZeileStart();
        $ABE->Form->Erstelle_TextLabel($ABE->AWISSprachKonserven['ABE']['ABE_ZU_DRUCKEN'],200,'font-weight: bold; font-size: 15px');
        $ABE->Form->ZeileEnde();

        $SQL  ='SELECT ABL.ABL_KEY ABL_KEY,';
        $SQL .='   ABL.ABL_ABF_KEY ABL_ABF_KEY,';
        $SQL .='   ABL.ABL_DRUCKDATUM ABL_DRUCKDATUM,';
        $SQL .='   ABL.ABL_VON ABL_VON,';
        $SQL .='   ABL.ABL_NACH ABL_NACH,';
        $SQL .='   ABL.ABL_BEMERKUNG ABL_BEMERKUNG,';
        $SQL .='   ABL.ABL_XBN_KEY ABL_XBN_KEY,';
        $SQL .='   ABL.ABL_WARTEND ABL_WARTEND,';
        $SQL .='   ABF.ABF_KEY ABF_KEY,';
        $SQL .='   ABF.ABF_FAHRGESTELLNR ABF_FAHRGESTELLNR,';
        $SQL .='   ABF.ABF_MOTORNR ABF_MOTORNR,';
        $SQL .='   ABF.ABF_ATU_NR ABF_ATU_NR,';
        $SQL .='   ABF.ABF_FARBE ABF_FARBE';
        $SQL .=' FROM ABELIEFERSCHEINE ABL';
        $SQL .=' INNER JOIN ABEFAHRZEUGE ABF';
        $SQL .=' ON ABF_KEY = ABL_ABF_KEY';

        if($_POST['txtDrucktyp'] == 1){ //Alle nicht gedruckten, des aktuellen Users
            $SQL .= ' WHERE ABL_WARTEND = 1 ';
            $SQL .= ' AND ABL_XBN_KEY = ' . $ABE->DB->WertSetzen('ABE','N0',$ABE->AWISBenutzer->BenutzerID());
        }elseif($_POST['txtDrucktyp'] == 2){ //Weitere Druckoptionen
            $SQL .= ' WHERE ((ABL_DRUCKDATUM >= ' . $ABE->DB->WertSetzen('ABE','D',$_POST['txtABE_DRUCKDATUM_VON']);
            $SQL .= ' and ABL_DRUCKDATUM <= ' . $ABE->DB->WertSetzen('ABE','D',$_POST['txtABE_DRUCKDATUM_BIS']);
            $SQL .=' ) or ABL_DRUCKDATUM is null) ';

            if(!isset($_POST['txtAndereUser'])){ //Wenn die Checkbox nicht kommt, dann immer auf den aktuellen User einschr�nken
                $SQL .= ' AND ABL_XBN_KEY = ' . $ABE->DB->WertSetzen('ABE','N0',$ABE->AWISBenutzer->BenutzerID());
            }

            if(!isset($_POST['txtBereitsGedruckte'])){ //Wenn die Checkbox nicht kommt, dann nur nichtgedruckte
                $SQL .= ' and ABL_WARTEND = 1 ';
            }
        }
        $SQL .= ' ORDER BY ABF_FAHRGESTELLNR';

        $rsABE = $ABE->DB->RecordSetOeffnen($SQL,$ABE->DB->Bindevariablen('ABE'));

        if($rsABE->EOF()){
            $ABE->Form->ZeileStart();
            $ABE->Form->Hinweistext($ABE->AWISSprachKonserven['Fehler']['err_keineDaten']);
            $ABE->Form->ZeileEnde();
        }else{
            $FeldBreiten = array();
            $FeldBreiten['ABF_FAHRGESTELLNR'] = 220;
            $FeldBreiten['ABF_MOTORNR'] = 180;
            $FeldBreiten['ABF_ATU_NR'] = 120;
            $FeldBreiten['ABF_DRUCK'] = 90;
            $Link = '';
            $ABE->Form->ZeileStart();
            ob_start();
            $ABE->Form->Erstelle_Checkbox('checkAlle', 1, 20,true,1,'','','');
            $Inhalt = ob_get_clean();
            $ABE->Form->Erstelle_Liste_Ueberschrift($Inhalt,20,'padding-left: 5px;padding-top: 2px;');
             $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_FAHRGESTELLNR'], $FeldBreiten['ABF_FAHRGESTELLNR'], '', $Link);

           $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_MOTORNR'], $FeldBreiten['ABF_MOTORNR'], '', $Link);

            $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_ATU_NR'], $FeldBreiten['ABF_ATU_NR'], '', $Link);

            $ABE->Form->Erstelle_Liste_Ueberschrift($ABE->AWISSprachKonserven['ABF']['ABF_DRUCK'], $FeldBreiten['ABF_DRUCK'], '', '');

            $ABE->Form->ZeileEnde();
            $DS = 0;    // f�r Hintergrundfarbumschaltung
            while (!$rsABE->EOF()) {
                $ABE->Form->ZeileStart('', 'Zeile"ID="Zeile_' . $rsABE->FeldInhalt('ABF_KEY'));
                $ABE->Form->Erstelle_ListenCheckbox('DruckKey[]',((isset($_POST['txtDruckKey']) and in_array($rsABE->FeldInhalt('ABL_KEY'),$_POST['txtDruckKey']))?$rsABE->FeldInhalt('ABL_KEY'):''),20,true,$rsABE->FeldInhalt('ABL_KEY'),'','','','',($DS % 2),false,'','anzeigen_chk');

                $Link = './abe_Main.php?cmdAktion=Details&ABF_KEY=' . $rsABE->FeldInhalt('ABF_KEY') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
                $TTT = $rsABE->FeldInhalt('ABF_FAHRGESTELLNR');
                $ABE->Form->Erstelle_ListenFeld('ABF_FAHRGESTELLNR', $rsABE->FeldInhalt('ABF_FAHRGESTELLNR'), 0, $FeldBreiten['ABF_FAHRGESTELLNR'], false, ($DS % 2), '', $Link, 'T', 'L',
                    $TTT);

                $TTT = $rsABE->FeldInhalt('ABF_MOTORNR');
                $ABE->Form->Erstelle_ListenFeld('ABF_MOTORNR', $rsABE->FeldInhalt('ABF_MOTORNR'), 0, $FeldBreiten['ABF_MOTORNR'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                $TTT = $rsABE->FeldInhalt('ABF_ATU_NR');
                $ABE->Form->Erstelle_ListenFeld('ABF_ATU_NR', $rsABE->FeldInhalt('ABF_ATU_NR'), 0, $FeldBreiten['ABF_ATU_NR'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

                if ($rsABE->FeldInhalt('ABL_WARTEND') == '1') {
                    $IconsArray[] = array("flagge_orange", $Link, "", $ABE->AWISSprachKonserven['ABF']['ABF_ZUDRUCK']);
                } elseif ($rsABE->FeldInhalt('DRUCKANZ') > 0) {
                    $IconsArray[] = array("flagge_gruen", $Link, "", $ABE->AWISSprachKonserven['ABF']['ABF_DRUCK']);
                } else {
                    $IconsArray[] = array("flagge_rot", $Link, "", $ABE->AWISSprachKonserven['ABF']['ABF_NICHTDRUCK']);
                }
                $ABE->Form->Erstelle_ListeIcons($IconsArray, $FeldBreiten['ABF_DRUCK'], ($DS % 2));
                $IconsArray = [];

                $ABE->Form->ZeileEnde();
                $DS++;
                $rsABE->DSWeiter();
            }
            $Drucken = true;
        }
    }

    $ABE->Form->Formular_Ende();
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $ABE->Form->SchaltflaechenStart();
    $ABE->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $ABE->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        $ABE->Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $ABE->AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    if($Drucken){
        $ABE->Form->Schaltflaeche('image', 'cmdPDF', '', '/bilder/cmd_PDF.png', $ABE->AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
    }
    $ABE->Form->SchaltflaechenEnde();

    $ABE->Form->SchreibeHTMLCode('</form>');

} catch (awisException $ex) {
    if ($ABE->Form instanceof awisFormular) {
        $ABE->Form->DebugAusgabe(1, $ex->getSQL());
        $ABE->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $ABE->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($ABE->Form instanceof awisFormular) {
        $ABE->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>