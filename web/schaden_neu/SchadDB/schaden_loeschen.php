<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('SCHAD');
$DB->Oeffnen();

$Tabelle= '';

if(isset($_GET['Del'])) // Wenn rotes X bei einer Massnahme gedr�ckt
{
		switch($_GET['Seite'])
		{
	    	case 'Konto':
				$Tabelle = 'ZAHLUNGEN';
				$Key=$_GET['Del'];
		
				$SQL  = 'select ZKEY,DATUM,BETRAG,b.BUCHUNGSART,VERWENDUNGSZWECK,c.ART,d.STANDBEM,c.SCHADENSNR,a.ZUSATZINFO ';
				$SQL .= 'from zahlungen a';
				$SQL .= ' inner join buchungsarten b on a.bid=b.bid ';
				$SQL .= ' left join vers_new c on a.zuvsid = c.VS_ID ';
				$SQL .= ' left join stand d on c.STANDID = d.STANDID and ORT = \'VERS\'';
				
				$SQL .= ' WHERE zkey='.$Key;
				$SQL .= ' and b.VERALTET = 0';
				
				$rsDaten = $DB->RecordSetOeffnen($SQL);
			
				/**
				 * Werte f�r Frage 'Wirklich l�schen?' holen
				**/
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_DATUM'),$Form->Format('D',$rsDaten->FeldInhalt('DATUM')));	
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_BUCHUNGSART'),$rsDaten->FeldInhalt('BUCHUNGSART'));		
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_VERWENDUNGSZWECK'),$rsDaten->FeldInhalt('VERWENDUNGSZWECK'));
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_ART'),$rsDaten->FeldInhalt('ART'));
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_STATUS'),$rsDaten->FeldInhalt('STANDBEM'));
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_VERSNR'),$rsDaten->FeldInhalt('SCHADENSNR'));
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_ZUSATZINFO'),$rsDaten->FeldInhalt('ZUSATZINFO'));
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_BETRAG'),$Form->Format('N2',$rsDaten->FeldInhalt('BETRAG')).'&euro;');
			break;
			
			case 'Info':
				$Tabelle = 'BEARBEITUNGSSTAND_NEW';
				$Key=$_GET['Del'];
			
				$SQL  = 'select * from BEARBEITUNGSSTAND_NEW ';	
				$SQL .= ' where ID ='.$Key;
				
				$rsDaten = $DB->RecordSetOeffnen($SQL);
					
				/**
				 * Werte f�r Frage 'Wirklich l�schen?' holen
				**/
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_DATUM'),$Form->Format('D',$rsDaten->FeldInhalt('DATUM')));
				$Felder[]=array($Form->LadeTextBaustein('BES','BEMERKUNGEN'),strlen($rsDaten->FeldInhalt('BEMERKUNGEN')) > 50 ? substr($rsDaten->FeldInhalt('BEMERKUNGEN'),0,49).'...':$rsDaten->FeldInhalt('BEMERKUNGEN'));
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_BETRAG').' EUR',$Form->Format('N2',$rsDaten->FeldInhalt('BETRAG')).($rsDaten->FeldInhalt('BETRAG')== ''?'':'&euro;'));
				$Felder[]=array($Form->LadeTextBaustein('BES','BES_ZAHLUNGSART'),$rsDaten->FeldInhalt('ZAHLUNGSART'));
				break;
		
				case 'WV':
					$Tabelle = 'WIEDERVORLAGEN_NEW';
					$Key=$_GET['Del'];
						
					$SQL  = 'select * from WIEDERVORLAGEN_NEW ';
					$SQL .= ' where WV_ID ='.$Key;
				
					$rsDaten = $DB->RecordSetOeffnen($SQL);
						
					/**
					 * Werte f�r Frage 'Wirklich l�schen?' holen
					**/
					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('BES','BES_WVDATUM'),$Form->Format('D',$rsDaten->FeldInhalt('WV_DATUM')));
					$Felder[]=array($Form->LadeTextBaustein('BES','BES_WVAN'),$Form->Format('T',$rsDaten->FeldInhalt('WV_AN')));
					$Felder[]=array($Form->LadeTextBaustein('BES','BES_SCHADENNR'),$Form->Format('T',$rsDaten->FeldInhalt('BEARBEITUNGSNR')));
					$Felder[]=array($Form->LadeTextBaustein('BES','BES_WVGRUND'),strlen($rsDaten->FeldInhalt('WV_GRUND')) > 50 ? substr($rsDaten->FeldInhalt('WV_GRUND'),0,49).'...':$rsDaten->FeldInhalt('WV_GRUND'));
				break;
				
		}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren wenn auf Frage 'Wirklich l�schen?' ok
{
	switch ($_POST['txtTabelle'])
	{
		case 'ZAHLUNGEN':
			
			
			$SQL = 'DELETE FROM ZAHLUNGEN WHERE ZKEY='.$_POST['txtKey']; // Zahlungsdatensatz l�schen
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		break;

		case 'BEARBEITUNGSSTAND_NEW':
				
		
			$SQL = 'DELETE FROM  BEARBEITUNGSSTAND_NEW WHERE ID='.$_POST['txtKey']; // Zahlungsdatensatz l�schen
				
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}

		break;
		
		case 'WIEDERVORLAGEN_NEW':
		
		
			$SQL = 'DELETE FROM WIEDERVORLAGEN_NEW WHERE WV_ID='.$_POST['txtKey']; // Zahlungsdatensatz l�schen
		
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		
			break;
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['Seite'])
	{
		case 'Konto':
			$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./schaden_Main.php?cmdAktion=Details&Seite=Konto&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'].' method=post>');
		
			$Form->Formular_Start();
			
			$Form->ZeileStart();		
			$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
		
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
			 **/
			$Form->Erstelle_HiddenFeld('Key',$_GET['Del']);
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		
			$Form->Trennzeile();
		
			/**
			 * L�schen ja/nein
			 **/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],''); 
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();
		
			$Form->SchreibeHTMLCode('</form>');
		
			$Form->Formular_Ende();
		
			die();
		break;
		
		case 'Info':
			$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./schaden_Main.php?cmdAktion=Details&Seite=Info&BEARBNEU='.$_GET['BEARBNEU'].'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'].' method=post>');
		
			$Form->Formular_Start();
				
			$Form->ZeileStart();
			$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
		
			$Form->Trennzeile('O');
				
			foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
				
			/**
			 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
			 **/
			$Form->Erstelle_HiddenFeld('Key',$_GET['Del']);
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		
			$Form->Trennzeile();
		
			/**
			 * L�schen ja/nein
			**/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();
		
			$Form->SchreibeHTMLCode('</form>');
		
			$Form->Formular_Ende();
		
			die();
			break;
			
			case 'WV':
				$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./schaden_Main.php?cmdAktion=Details&Seite=WV'.'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'].' method=post>');
			
				$Form->Formular_Start();
			
				$Form->ZeileStart();
				$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
				$Form->ZeileEnde();
			
				$Form->Trennzeile('O');
			
				foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
				{
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($Feld[0].':',200);
					$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
					$Form->ZeileEnde();
				}
			
				/**
				 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
				 **/
				$Form->Erstelle_HiddenFeld('Key',$_GET['Del']);
				$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
			
				$Form->Trennzeile();
			
				/**
				 * L�schen ja/nein
				**/
				$Form->ZeileStart();
				$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
				$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
				$Form->ZeileEnde();
			
				$Form->SchreibeHTMLCode('</form>');
			
				$Form->Formular_Ende();
			
				die();
				break;
	
	}
}

?>