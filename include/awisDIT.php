<?php
require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisRest.inc';

class awisDIT
{
    /**
     * AwisBenutzerObj
     *
     * @var awisBenutzer
     */
    protected $_AWISBenutzer;

    /**
     * AWIS-DB
     *
     * @var awisDatenbank
     */
    protected $_DB;

    /**
     * AWIS-REST
     *
     * @var awisRest
     */
    protected $_awisRest;

    /**
     * Tabellenk�rzel als Kennung
     *
     * @var String
     */
    protected $_XTN_Kuerzel;


    function __construct(awisBenutzer $AWISBenutzer,awisDatenbank $DB, $XTN_Kuerzel)
    {
        $this->_AWISBenutzer = $AWISBenutzer;
        $this->_DB = $DB;
        $this->_XTN_Kuerzel = $XTN_Kuerzel;

        $this->_awisRest = new awisRest('DIT_'.$XTN_Kuerzel);
    }

    /**
     * Schreibt die aktuellen Daten in die Log-Tabelle
     * (Titel, Datum, etc)
     *
     * @param $TaskID int
     * @param $LogWerte array (titel, description, publishingDate, maturity, priority, maxAcknowledgementLvl, created_by, recipient)
     * @param $XXX_KEY int
     *
     * @return bool
     */
    private function Log($TaskID, $LogWerte = array(), $XXX_KEY = 0)
    {
        try {
            $SQL = "SELECT dil_status FROM DITAPILOG WHERE dil_taskid = " . $this->_DB->WertSetzen('DIL', 'Z', $TaskID, false);
            $rsDIL = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('DIL'));
            if ($rsDIL->AnzahlDatensaetze() < 1 and isset($LogWerte['title'])) {
                $SQL = "INSERT INTO DITAPILOG ";
                $SQL .= " (DIL_TASKID, DIL_TITEL, DIL_BESCHREIBUNG, DIL_VEROEFFENTLICHUNGSDATUM, DIL_ENDDATUM, DIL_PRIORITAET, DIL_MAX_EMPFAENGER, DIL_ERSTELLT_VON, DIL_STATUS, DIL_XTN_KUERZEL, DIL_USER, DIL_USERDAT ";
                if($XXX_KEY <> 0) {
                    $SQL .= " , DIL_XXX_KEY";
                }
                $SQL .= " ) VALUES ";
                $SQL .= " ( ";
                $SQL .= " " . $this->_DB->WertSetzen('DIL', 'Z', $TaskID, false);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'T', $LogWerte['title'], true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'T', $LogWerte['description'], true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'D', $LogWerte['publishingDate'], true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'D', $LogWerte['maturity'], true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'Z', $LogWerte['priority'], true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'Z', $LogWerte['maxAcknowledgementLvl'], true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'Z', $LogWerte['created_by'], true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'Z', 1, true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'T', $this->_XTN_Kuerzel, true);
                $SQL .= ", " . $this->_DB->WertSetzen('DIL', 'T', $this->_AWISBenutzer->BenutzerName(), false);
                $SQL .= ", sysdate";
                if($XXX_KEY <> 0) {
                    $SQL .= ", ".$this->_DB->WertSetzen('DIL','Z',$XXX_KEY,true);
                }
                $SQL .= " ) ";

                $this->_DB->Ausfuehren($SQL, 'DIL', true, $this->_DB->Bindevariablen('DIL'));

                $SQL = "SELECT DIL_KEY FROM DITAPILOG WHERE dil_taskid = " . $this->_DB->WertSetzen('DIL', 'Z', $TaskID, false);
                $rsDIL = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('DIL'));

                foreach ((json_decode($LogWerte['recipient'])) as $EmpfaengerID) {
                    $SQL = "INSERT INTO DITAPILOGEMPF ";
                    $SQL .= " (DIE_DIL_KEY, DIE_EMPFAENGER, DIE_USER, DIE_USERDAT) ";
                    $SQL .= " VALUES ";
                    $SQL .= " ( ";
                    $SQL .= " " . $this->_DB->WertSetzen('DIE', 'Z', $rsDIL->FeldInhalt('DIL_KEY'), false);
                    $SQL .= ", " . $this->_DB->WertSetzen('DIE', 'Z', $EmpfaengerID->ID, false);
                    $SQL .= ", " . $this->_DB->WertSetzen('DIE', 'T', $this->_AWISBenutzer->BenutzerName(), false);
                    $SQL .= ", sysdate";
                    $SQL .= " ) ";
                    $this->_DB->Ausfuehren($SQL, 'DIE', true, $this->_DB->Bindevariablen('DIE'));
                }

            } elseif ($rsDIL->FeldInhalt('DIL_STATUS') == 1) {
                $SQL = "UPDATE DITAPILOG SET";
                $SQL .= " dil_status = " . $this->_DB->WertSetzen('DIL', 'Z', 2, true);
                $SQL .= " where dil_taskid = " . $this->_DB->WertSetzen('DIL', 'Z', $TaskID, false);
                $this->_DB->Ausfuehren($SQL, 'DIL', true, $this->_DB->Bindevariablen('DIL'));
            } else {
                return false;
            }

        } catch (awisException $e) {
            $this->_DB->BindevariablenLoeschen('DIL');
            return false;
        }
        return true;
    }

    /**
     * Erstellt den Task mit den mitgegebenen Daten
     *
     * @param $Titel string
     * @param $EmpfaengerXBN string|int|array
     * @param $Beschreibung string
     * @param $Veroeffentlichungsdatum string
     * @param $Enddatum string
     * @param $Prioritaet int 1 (niedrig) - 3 (hoch)
     * @param $Max_Empfaenger int
     * @param $ErstellerXBN int
     * @param $XTN_KEY int
     *
     * @return bool Gibt zur�ck, ob alles korrekt durchgelaufen ist oder nicht
     */
    public function ErstelleTask($Titel,$EmpfaengerXBN, $Beschreibung = "", $Veroeffentlichungsdatum = "", $Enddatum = "", $Prioritaet = 1, $Max_Empfaenger = 0, $ErstellerXBN = 0, $XTN_KEY = 0)
    {
        if(is_array($EmpfaengerXBN)) {
            $Empfaenger = array();
            foreach ($EmpfaengerXBN as $EmpfaengerID) {
                $Klasse = new stdClass();
                $Klasse->ID = $EmpfaengerID;
                $Empfaenger[] = $Klasse;
            }
        } else {
            $Empfaenger = array();
            $Klasse = new stdClass();
            $Klasse->ID = $EmpfaengerXBN;
            $Empfaenger[] = $Klasse;
        }

        $Werte = array();
        $Werte['title'] = $Titel;
        $Werte['description'] = $Beschreibung;
        $Werte['publishingDate'] = date('Y-m-d H:i:s',strtotime($Veroeffentlichungsdatum));
        $Werte['maturity'] = date('Y-m-d H:i:s',strtotime($Enddatum));
        $Werte['priority'] = $Prioritaet;
        $Werte['maxAcknowledgementLvl'] = $Max_Empfaenger;
        $Werte['created_by'] = $ErstellerXBN;
        $Werte['store_option'] = 1;
        $Werte['recipient'] = json_encode($Empfaenger);

        $TaskID = $this->_awisRest->erstelleRequest($this->_AWISBenutzer->ParameterLesen('DITAPI_URL_Erstellen'),'POST','JSON',$Werte);
        $this->Log($TaskID, $Werte, $XTN_KEY);

        return $TaskID;
    }

    /**
     * Gibt den XBN_KEY vom Filialleiter zur�ck
     *
     * @param $FIL_ID int
     *
     * @return int
     */
    public function holeFilialXBN($FIL_ID){

        $SQL = "select XBN_KEY from benutzer where XBN_KON_KEY in ( ";
        $SQL .= " select FRZ_KON_KEY from FILIALEBENENROLLENZUORDNUNGEN ";
        $SQL .= " where FRZ_XTN_KUERZEL = 'FIL' and FRZ_FER_KEY = 1 and FRZ_XXX_KEY = ".$this->_DB->WertSetzen('DIT','Z',$FIL_ID);
        $SQL .= " and frz_gueltigbis > sysdate and frz_gueltigab < sysdate"; //Pr�fen ob g�ltig und zur Sicherheit den Aktuellsten nehmen falls es mehrere gibt
        $SQL .= " )";

        $rsFRZ = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('DIT'));

        if($rsFRZ->AnzahlDatensaetze() > 0) {
            return $rsFRZ->FeldInhalt('XBN_KEY');
        } else{
            return 0;
        }
    }

    /**
     * Gibt den kompletten Task zur�ck mit dieser ID
     *
     * @param $TASK_ID int
     *
     * @return int
     */
    public function holeTask($TASK_ID){
        $Response = $this->_awisRest->erstelleRequest($this->_AWISBenutzer->ParameterLesen('DITAPI_URL_GETTASK').'/'.$TASK_ID, 'GET');
        return $Response;
    }

    /**
     * Schlie�t den Task mit dieser ID ( -> Abgeschlossen/Done )
     *
     * @param $TASK_ID int
     *
     * @return int
     */
    public function schliesseTaskAb($TASK_ID){
        $Response = $this->_awisRest->erstelleRequest($this->_AWISBenutzer->ParameterLesen('DITAPI_URL_Abschliessen').'/'.$TASK_ID, 'POST');
        if($Response == "ok"){
            $this->Log($TASK_ID);
        }
        return $Response;
    }

    /**
     * Schlie�t den Task mit dieser ID ( -> Storniert/Cancel )
     *
     * @param $TaskID int
     *
     * @return bool
     */
    public function StorniereTask($TaskID)
    {
        try {
            $Werte = array();
            $Werte['task_id'] = $TaskID;

            $Response = $this->_awisRest->erstelleRequest($this->_AWISBenutzer->ParameterLesen('DITAPI_URL_Stornieren'), 'POST', 'JSON', $Werte);

            if($Response == 'ok') {
                $this->Log($TaskID);
                return $Response;
            } else{
                return $Response;
            }

        }catch (awisException $e){
            return "error";
        }
    }

    /**
     * Updated in der AWIS DITAPILOG-Tabelle den Status
     *
     * @param $TaskID int
     *
     * @return bool
     */
    public function updateTaskStatus($TaskID)
    {
        return $this->Log($TaskID);
    }
}