<?php
/**
 * Suchmaske f�r die Auswahl einer Warengruppe
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20090305
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZSZ','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_JaNein');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht10008=$AWISBenutzer->HatDasRecht(10008);		// Rechte des Mitarbeiters

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./warengruppen_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('ZukaufZSZSuche'));
	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();

	// Sortiment
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'].':',190);
	$Form->Erstelle_TextFeld('*ZSZ_SORTIMENT',($Param['SPEICHERN']=='on'?$Param['ZSZ_SORTIMENT']:''),30,200,true);
	$AWISCursorPosition='sucZSZ_SORTIMENT';
	$Form->ZeileEnde();

	// Warengruppe
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG1'].':',190);
	$Form->Erstelle_TextFeld('*ZSZ_WG1',($Param['SPEICHERN']=='on'?$Param['ZSZ_WG1']:''),10,200,true);
	$Form->ZeileEnde();

	// Warengruppe
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG2'].':',190);
	$Form->Erstelle_TextFeld('*ZSZ_WG2',($Param['SPEICHERN']=='on'?$Param['ZSZ_WG2']:''),10,200,true);
	$Form->ZeileEnde();

	// Warengruppe
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_WG3'].':',190);
	$Form->Erstelle_TextFeld('*ZSZ_WG3',($Param['SPEICHERN']=='on'?$Param['ZSZ_WG3']:''),10,200,true);
	$Form->ZeileEnde();

	// Lieferant
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_LIE_NR'].':',190);
	$SQL = 'SELECT LIE_NR, LIE_NAME1 FROM Lieferanten';
	$SQL .= ' WHERE EXISTS( SELECT * FROM ZukaufArtikelLieferanten WHERE LIE_NR = ZAL_LIE_NR)';
	$SQL .= ' ORDER BY LIE_NAME1';
	$Form->Erstelle_SelectFeld('*ZSZ_LIE_NR',($Param['SPEICHERN']=='on'?$Param['ZSZ_LIE_NR']:''),200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();


	$Form->Trennzeile();

	// Auswahl kann gesZLAchert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();


	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht10008&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=zukaufartikel&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180836");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180825");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>