<?php
global $awisRSInfoName;
global $awisDBError;
global $AWIS_KEY1;
global $RUECKFUEHRUNGSTYP;
global $AWISCursorPosition;
try
{
	$TextKonserven = array();
	$TextKonserven[]=array('RFA','RFA_%');
	$TextKonserven[]=array('RFA','lst_RFA_%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Rueckfuehrung%');
	$TextKonserven[]=array('Wort','lbl_aktualisieren');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Gesamt');
	$TextKonserven[]=array('Wort','Offen');
	$TextKonserven[]=array('Wort','Erledigt');
	$TextKonserven[]=array('Wort','Fehler');
	$TextKonserven[]=array('Wort','Jobname');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','BereitsBearbeitet');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('RFS','lst_RFA_RFS_STATUS');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht3900 = $AWISBenutzer->HatDasRecht(3900);
	if($Recht3900==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$Recht3910= $AWISBenutzer->HatDasRecht(3910);
	if($Recht3910==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);

	//********************************************************
	// Parameter ?
	//********************************************************
	
	$Param = array_fill(0,10,'');	
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array_fill(0,10,'');
		$Param[0]= $_POST['sucRFA_DATUM'];
		$Param[1]= $_POST['sucAST_ATUNR'];
		//$Param[2]= (isset($_POST['sucRFS_KEY'])?$_POST['sucRFS_KEY']:'');
		$Param[2]= (isset($_POST['sucRFS_STATUS'])?$_POST['sucRFS_STATUS']:'');
		$Param[3]= $_POST['txtRFM_FIL_ID'];
		$Param[4]= $_POST['sucRFA_NR'];
		if(($Recht3910&32)==32)
		{
			$Param[5]= $_POST['sucRFC_Wert'];
		}
		else{
			$Param[5]= $_POST['txtRFC_Wert'];
		}		
		$Param['ORDERBY']='';
		$Param['BLOCK']='';
		$Param['KEY']=0;
		$AWISBenutzer->ParameterSchreiben('RFASuche' , serialize($Param));
		$AWISBenutzer->ParameterSchreiben('AktuellerRFA', serialize(''));
	}

	elseif(isset($_POST['cmdDSZurueck_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('RFASuche', true));
		
		$Bedingung = _BedingungErstellen($Param);
		$Bedingung .= ' AND RFA_DATUM < ' . $DB->WertSetzen('RFA','D',$_POST['txtRFA_DATUM'], false);
		$Bedingung .= ' OR (RFA_DATUM = '.$DB->WertSetzen('RFA','D',$_POST['txtRFA_DATUM'],false);
		$Bedingung .= ' AND RFA_LFDNR < '.$DB->WertSetzen('RFA','Z',$_POST['txtRFA_LFDNR'],false).')';

		$SQL = 'SELECT * FROM ';
		$SQL .= '(  SELECT RFA_KEY FROM RueckfuehrungsAnforderungen';
		$SQL .= '   WHERE '.substr($Bedingung,4);
		$SQL .= '   ORDER BY RFA_DATUM DESC, RFA_LFDNR DESC';
		$SQL .= ') WHERE ROWNUM = 1';


		
		$rsRFA = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFA',true));
        $Form->DebugAusgabe(1,$DB->LetzterSQL());
		if($rsRFA->FeldInhalt('RFA_KEY')!='')
		{
			$AWIS_KEY1=$rsRFA->FeldInhalt('RFA_KEY');
		}
		else
		{
			$AWIS_KEY1 = $_POST['txtRFA_KEY'];
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('RFASuche', true));
		
		$Bedingung = _BedingungErstellen($Param);
		$Bedingung .= ' AND RFA_DATUM > '.$DB->WertSetzen('RFA','D',$_POST['txtRFA_DATUM'],false);
		$Bedingung .= ' OR (RFA_DATUM = '.$DB->WertSetzen('RFA','D',$_POST['txtRFA_DATUM'],false);
		$Bedingung .= ' AND RFA_LFDNR > '.$DB->WertSetzen('RFA','Z',$_POST['txtRFA_LFDNR'],false).')';

		$SQL = 'SELECT RFA_KEY FROM (SELECT RFA_KEY FROM RueckfuehrungsAnforderungen';
		$SQL .= ' WHERE '.substr($Bedingung,4);
		$SQL .= ' ORDER BY RFA_DATUM ASC, RFA_LFDNR ASC';
		$SQL .= ') WHERE ROWNUM = 1';

		$rsRFA = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFA'));
        $Form->DebugAusgabe(1,$DB->LetzterSQL());
		if($rsRFA->FeldInhalt('RFA_KEY')!='')
		{
			$AWIS_KEY1=$rsRFA->FeldInhalt('RFA_KEY');
		}
		else
		{
			$AWIS_KEY1 = $_POST['txtRFA_KEY'];
		}
	}

	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./rueckfuehrung_loeschen.php');
		if($AWIS_KEY1==0)
		{
			$Param = unserialize($AWISBenutzer->ParameterLesen('RFASuche', false));
		}
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./rueckfuehrung_speichern.php');
		if(($Recht3910&32)==32)
		{
			$Param = unserialize($AWISBenutzer->ParameterLesen('RFASuche', false));
		}
		else{
			$Param = unserialize($AWISBenutzer->ParameterLesen('AktuellerRFA', false));
		}
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = -1;
	}
	elseif(isset($_GET['RFA_KEY']))
	{
		$AWIS_KEY1 = floatval($_GET['RFA_KEY']);		// Nur den Key speiechern
	}
	elseif(isset($_POST['txtRFA_KEY']))
	{
		$AWIS_KEY1 = floatval($_POST['txtRFA_KEY']);		// Nur den Key speiechern
	}
	else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
	{		
		if(!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block']))
		{
			$AWIS_KEY1 = unserialize($AWISBenutzer->ParameterLesen('AktuellerRFA' , false));

			if (intval($AWIS_KEY1) <= 0)		
			{
				$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
				
				if ($UserFilialen != '')
				{					
					$Param[3]=$UserFilialen;	
					$AWISBenutzer->ParameterSchreiben("AktuellerRFA",0);
					$AWISBenutzer->ParameterSchreiben("RFASuche",serialize($Param));
				}			
			}
		}			
		else
		{
			//$AWISBenutzer->ParameterSchreiben('AktuellerRFA',serialize(''));
			$AWISBenutzer->ParameterSchreiben('RFA_POS_Blaettern',serialize(0));
		}
		
		if($Param=='' OR $Param=='0')	
		{						
			$AWISBenutzer->ParameterSchreiben("AktuellerRFA",0);			
		}		
	}
	$Param = unserialize($AWISBenutzer->ParameterLesen("RFASuche"));

    $Form->DebugAusgabe(1,$Param);
	//********************************************************
	// Anforderung - Erstellung
	//********************************************************

    if(isset($_POST['cmdErinnerung_x'])){
        $SQL='BEGIN P_MDE.PROC_MAIL_ERINNERUNG ( '.$DB->WertSetzen('EXE','N0',$_POST['txtRFA_KEY']).','.$DB->WertSetzen('EXE','N0',$_POST['txtRFA_LFDNR']).'); COMMIT; END;';
        $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('EXE'));
    }


	if(isset($_GET['RFA_KEY']) && isset($_GET['Anforderung']) && ($Recht3910&4)==4)
	{
		

		
		$SQL = 'SELECT RFA_NR, RFS_STATUS FROM RUECKFUEHRUNGSANFORDERUNGEN JOIN RUECKFUEHRUNGSSTATUS ON (RFA_RFS_KEY=RFS_KEY)';
		$SQL.= ' WHERE RFA_KEY='.$DB->WertSetzen('RFA','N0',intval('0'.$_GET['RFA_KEY']));
		
		$rsRFAStatus = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFA'));
		$rsRFAStatusZeilen = $rsRFAStatus->AnzahlDatensaetze();
		
		if($rsRFAStatus->FeldInhalt('RFS_STATUS')>='19')
		{
			//R�ckf�hrungskennzeichen mit speichern
			$SQL = 'UPDATE RUECKFUEHRUNGSANFORDERUNGENPOS';
			$SQL.= ' SET RFP_RUECKLIEFKZ = (select rueckliefkz from v_artikel_rueckliefkz where ast_atunr = rfp_ast_atunr and gueltig=0)';
			$SQL.= ' WHERE RFP_RFA_key='.$DB->WertSetzen('RFA','N0',intval('0'.$_GET['RFA_KEY']) );
			

			
			if($DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('RFA'))===false)
			{
				throw new awisException('Fehler beim Abschliessen','009021047',$SQL,2);
			}
            $Form->DebugAusgabe(1,$SQL);
			$SQL='BEGIN P_MDE.PROC_CREA_FIL_RF_ANFORDERUNG ( '.$_GET['Anforderung'] .', '. $_GET['RFA_KEY'].'); COMMIT; END;';

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim ausf�hren der Simulation',200810221634,$SQL,2);
			}
			
			$Form->Hinweistext('Anforderung: '.$rsRFAStatus->FeldInhalt('RFA_NUMMER').' - Filialen werden informiert!');
			$Form->Erstelle_HiddenFeld('EMailVersand',1);
		}
		else {
			$Form->Hinweistext('Anforderung: '.$rsRFAStatus->FeldInhalt('RFA_NUMMER').' im falschem Status!!!');	
		}
	}

	//********************************************************
	// Anforderung - Erstellung - Mail-�berwachung
	//********************************************************
	
	if(isset($_POST['txtEMailVersand'])) 
	{
		$MailVersand=$_POST['txtEMailVersand'];
	}
	elseif(isset($_GET['txtEMailVersand']))
	{
		$MailVersand=$_GET['txtEMailVersand'];
	}else{
		if($AWIS_KEY1<>0 || isset($_POST['cmdEMailVersand']))
		{

			$SQL='SELECT COUNT(*) AS RFE_MAILVERSAND FROM V_RUECKLIEFERUNG_MAILVERSAND WHERE RFE_RFA_KEY=' .$DB->WertSetzen('RFE','N0',intval('0'.$AWIS_KEY1));
            $SQL .=' AND RFE_GESAMT<>RFE_ERLEDIGT';
			$rsRFA_Mail = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFE'));

            $Form->DebugAusgabe(1,$SQL);

            if($rsRFA_Mail->FeldInhalt('RFE_MAILVERSAND')<>0)
			{
				$MailVersand=1;
			}
			else{
				$MailVersand=0;
			}	
		}
		else{
			$MailVersand=0;
		}	
	}
	
	if($MailVersand==1)
	{
			$SQL='SELECT * FROM V_RUECKLIEFERUNG_MAILVERSAND WHERE RFE_RFA_KEY='.$DB->WertSetzen('RFE','N0',intval('0'.$AWIS_KEY1));
			$SQL .= ' AND RFE_GESAMT<>RFE_ERLEDIGT';

			$rsRFA_Mail = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFE'));
            $Form->DebugAusgabe(1,$DB->LetzterSQL());
			$rsRFA_MailZeilen = $rsRFA_Mail->AnzahlDatensaetze();
			
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Gesamt'],100);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Offen'],100);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Erledigt'],100);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Fehler'],100);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Jobname'],300);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],200);
			$Form->ZeileEnde();
			
			for($rsRFA_MailZeile=0;$rsRFA_MailZeile<$rsRFA_MailZeilen;$rsRFA_MailZeile++)
			{
				$Form->ZeileStart();
				$Form->Erstelle_ListenFeld('RFE_GESAMT',$rsRFA_Mail->FeldInhalt('RFE_GESAMT'),0,100,false,($rsRFA_MailZeile%2));
				$Form->Erstelle_ListenFeld('RFE_OFFEN',$rsRFA_Mail->FeldInhalt('RFE_OFFEN'),0,100,false,($rsRFA_MailZeile%2));
				$Form->Erstelle_ListenFeld('RFE_ERLEDIGT',$rsRFA_Mail->FeldInhalt('RFE_ERLEDIGT'),0,100,false,($rsRFA_MailZeile%2));
				$Form->Erstelle_ListenFeld('RFE_FEHLER',$rsRFA_Mail->FeldInhalt('RFE_FEHLER'),0,100,false,($rsRFA_MailZeile%2));
				$Form->Erstelle_ListenFeld('RFE_JOB_NAME',$rsRFA_Mail->FeldInhalt('RFE_JOB_NAME'),0,300,false,($rsRFA_MailZeile%2));
				$Form->Erstelle_ListenFeld('RFE_JOB_STATUS',$rsRFA_Mail->FeldInhalt('RFE_JOB_STATUS'),0,200,false,($rsRFA_MailZeile%2));
				if($rsRFA_Mail->FeldInhalt('RFE_JOB_STATUS')=='SUCCEEDED' AND ($rsRFA_Mail->FeldInhalt('RFE_OFFEN')!=0 OR $rsRFA_Mail->FeldInhalt('RFE_FEHLER')!=0))
				{
					if(($Recht3910&4)==4)
					{

						$SQL='SELECT RFA_TYP FROM RUECKFUEHRUNGSANFORDERUNGEN WHERE RFA_KEY='.$DB->WertSetzen('RFA','N0',$AWIS_KEY1);
						$rsRFA_Typ=$DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFA'));
						$RUECKFUEHRUNGSTYP = $rsRFA_Typ->FeldInhalt('RFA_TYP');
						$Form->SchreibeHTMLCode('<form name="frmRueckfuehrung" action="./rueckfuehrung_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');
						$Form->Schaltflaeche('href', 'cmdAnforderung', './rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$AWIS_KEY1.'&Anforderung='.$RUECKFUEHRUNGSTYP, '/bilder/cmd_antenne.png', $AWISSprachKonserven['Wort']['lbl_ReuckfuehrungAnforderung'], '');
						$Form->SchreibeHTMLCode('</form>');
					}
				}
				    
				$Form->ZeileEnde();
				$rsRFA_Mail->DSWeiter();
			}

			$Form->SchreibeHTMLCode('<form name="frmRueckfuehrung" action="./rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$AWIS_KEY1.'" method="POST"  enctype="multipart/form-data">');
			$Form->ZeileStart();
			$Form->Schaltflaeche('image', 'cmdEMailVersand', '', '/bilder/cmd_mann_laeuft.png', $AWISSprachKonserven['Wort']['lbl_aktualisieren']);
			$Form->ZeileEnde();
			$Form->SchreibeHTMLCode('</form>');
			
			$Form->Trennzeile('D');
	}
	
	
	//********************************************************
	// Anforderung - Simulation
	//********************************************************

	if(isset($_GET['RFA_KEY']) && isset($_GET['Simulation']) && ($Recht3910&2)==2)
	{
		$SQL='SELECT COUNT(*) AS FIL_COUNT FROM RUECKFUEHRUNGSANFFILIALEN WHERE RFL_RFA_KEY='.$DB->WertSetzen('RFA','N0',intval('0'.$_GET['RFA_KEY']));
		
		$rsRFL_COUNT = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFA'));
		$rsRFL_COUNTZeilen = $rsRFL_COUNT->AnzahlDatensaetze();

		$SQL='SELECT RFP_AST_ATUNR FROM RUECKFUEHRUNGSANFORDERUNGENPOS, ';
		$SQL.='(SELECT * FROM V_ARTIKEL_RUECKLIEFKZ WHERE GUELTIG=0) ';
		$SQL.='WHERE RFP_AST_ATUNR=AST_ATUNR(+) ';
		$SQL.='AND RUECKLIEFKZ IS NULL AND RFP_RFA_KEY='.$DB->WertSetzen('RFP','N0',intval('0'.$_GET['RFA_KEY']));
		
		$rsRFA_ATUNR = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFP'));
		$rsRFA_ATUNRZeilen = $rsRFA_ATUNR->AnzahlDatensaetze();
		
		if($rsRFL_COUNT->FeldInhalt('FIL_COUNT')==0 && $_GET['Simulation']!=2)
		{
			$Form->Hinweistext('Es wurde keine Filiale ausgew�hlt!!!');
		}
		elseif($rsRFA_ATUNRZeilen==0)
		{
			//Userdat neu setzen
            $SQL = 'UPDATE ARTIKELSTAMMINFOS SET';
			$SQL.= ' ASI_USER = ' .$DB->WertSetzen('RFA','T',$AWISBenutzer->BenutzerName(1));
			$SQL.= ', ASI_USERDAT=SYSDATE';
			$SQL.= ' WHERE ASI_AIT_ID = 122 ';
			$SQL.= ' AND ASI_AST_ATUNR IN (SELECT RFP_AST_ATUNR FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_RFA_KEY='.$DB->WertSetzen('RFA','N0', intval('0'.$_GET['RFA_KEY'])).')';


			if($DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('RFA'))===false)
			{
				throw new awisException('Fehler beim ausf�hren der Simulation',1005101346,$SQL,2);
			}
            $Form->DebugAusgabe(1,$DB->LetzterSQL());
			$SQL='BEGIN P_MDE.PROC_SIMU_FIL_RF_ANFORDERUNG ( '. $_GET['Simulation'].', '.$_GET['RFA_KEY'].' , 0); COMMIT; END;';

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim ausf�hren der Simulation',200809181403,$SQL,2);
			}
		}
		else {
			$Form->Hinweistext($rsRFA_ATUNRZeilen.' Artikel ohne R�ckf�hrungstyp!!!');
			$Artikel='';
			for($i=0;$i<$rsRFA_ATUNRZeilen;$i++)
			{
				if ($rsRFA_ATUNRZeilen>1)
				{
					$Artikel.=$rsRFA_ATUNR->FeldInhalt('RFP_AST_ATUNR').', ';
					$rsRFA_ATUNR->DSWeiter();
				}else
				{
					$Artikel.=$rsRFA_ATUNR->FeldInhalt('RFP_AST_ATUNR');
				}
			}
		
			$Form->ZeileStart();
				//$Form->Erstelle_Textarea('RFP_AST_ATUNR_RFT',$Artikel,800,1,strlen($Artikel)/800,false);
				$Form->Hinweistext($Artikel);
			$Form->ZeileEnde();
		}
	}

	
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	if(!isset($_GET['Sort']))
	{
		if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
		{
			$ORDERBY = $Param['ORDERBY'];
		}
		else 
		{
			$ORDERBY = ' ORDER BY RFA_DATUM DESC, RFA_LFDNR DESC nulls last';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']). ' nulls last';
	}		

	$SQL = 'SELECT DISTINCT RFA_KEY, RFA_NR, RFA_DATUM, RFA_BEMERKUNG, RFA_RFS_KEY, RFA_TYP, RFA_USER, RFA_USERDAT, RFA_LFDNR, RFS.RFS_BEZEICHNUNG, RFS.RFS_STATUS';
	if(($Recht3910&32)==32 && isset($Param[3]) && $Param[3]!='')
	{
		$SQL .= ' , RFC.RFC_WERT ';
	}
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM RUECKFUEHRUNGSANFORDERUNGEN RFA';
	$SQL .= ' LEFT JOIN RUECKFUEHRUNGSSTATUS RFS ON (RFA.RFA_RFS_KEY=RFS.RFS_KEY)';
	if(($Recht3910&32)==32 && isset($Param[3]) && $Param[3]!='')
	{
		$SQL .= ' LEFT JOIN RUECKFUEHRUNGSCHECKS RFC ON (RFA.RFA_KEY=RFC.RFC_RFA_KEY AND RFC.RFC_FIL_ID IN (';
        $In = explode(',',$Param[3]);
        foreach ($In as $Wert){
            $SQL .= $DB->WertSetzen('RFA','N0',$Wert) . ',';
        }
        $SQL .= $DB->WertSetzen('RFA','N0',-1);
        $SQL .='))';
	}
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,3);
	}
	$SQL .= ' GROUP BY RFA_KEY, RFA_NR, RFA_DATUM, RFA_BEMERKUNG, RFA_RFS_KEY, RFA_TYP, RFA_USER, RFA_USERDAT, RFA_LFDNR, RFS.RFS_BEZEICHNUNG, RFS.RFS_STATUS';
	if(($Recht3910&32)==32 && isset($Param[3]) && $Param[3]!='')
	{
		$SQL .= ' , RFC.RFC_WERT ';
	}
	$SQL .= $ORDERBY;
	
	//************************************************
	// Aktuellen Datenblock festlegen
	//************************************************
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
	}
	elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
	{
		$Block = intval($Param['BLOCK']);
	}

	//************************************************
	// Zeilen begrenzen
	//************************************************
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('RFA',false));
		
	//*****************************************************************
	// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
	//*****************************************************************
	if($AWIS_KEY1<=0)
	{
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	$rsRFA = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFA'));
	$rsRFAZeilen = $rsRFA->AnzahlDatensaetze();
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    //*****************************************************************
	// Aktuelle Parameter sichern
	//*****************************************************************	
	$Param['ORDERBY']=$ORDERBY;
	$Param['KEY']=$AWIS_KEY1;
	$Param['BLOCK']=$Block;
	$AWISBenutzer->ParameterSchreiben("RFASuche",serialize($Param));


	$Form->SchreibeHTMLCode('<form name=frmRueckfuehrung action=./rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

	//********************************************************
	// Daten anzeigen
	//********************************************************
	if($rsRFAZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->SchreibeHTMLCode('<span class=HinweisText>'.$AWISSprachKonserven['Fehler']['err_keineDaten'].'</span>');
	}
	elseif($rsRFAZeilen>1 OR ($rsRFAZeilen==1 AND ($Recht3910&16)!=16))						// Liste anzeigen
	{
		$Form->Formular_Start();

		$Form->ZeileStart();
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=RFA_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFA_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_DATUM'],130,'',$Link);
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=RFA_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFA_BEMERKUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_BEMERKUNG'],210,'',$Link);
		if(($Recht3910&32)!=32)
		{
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFA_TYP'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFA_TYP'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_TYP'],100,'',$Link);
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFS_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFS_STATUS'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],400,'',$Link);
		}
		else{
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFS_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFS_STATUS'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],250,'',$Link);
		}
		if(($Recht3910&8)==8)
		{
			$Form->Erstelle_Liste_Ueberschrift('&nbsp;',22);			
			if(($Recht3910&32)==32)
			{
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=RFC_WERT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFC_WERT'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['BereitsBearbeitet'],125,'',$Link);
			}
						
		}			
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsRFA->EOF())
		{
			$Form->ZeileStart();
			if(($Recht3910&16)==16)
			{	
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$rsRFA->FeldInhalt('RFA_KEY').'';
			}
			else{
				$Link ='';
			}
			
			$ANFNR = $Form->Format('D',$rsRFA->FeldInhalt('RFA_DATUM')).' - '.str_pad($Form->Format('T',$rsRFA->FeldInhalt('RFA_LFDNR')), 3, "0", STR_PAD_LEFT);			
			$Form->Erstelle_ListenFeld('RFA_DATUM',$ANFNR,0,130,false,($DS%2),'',$Link,'T','L');
			$Form->Erstelle_ListenFeld('RFA_BEMERKUNG',$rsRFA->FeldInhalt('RFA_BEMERKUNG'),0,210,false,($DS%2),'','','T','L');
			if(($Recht3910&32)!=32)
			{
				$Form->Erstelle_ListenFeld('RFA_TYP',($rsRFA->FeldInhalt('RFA_TYP')=='1'?'4090':'4096'),0,100,false,($DS%2),'','','T','L');
				$RueckStatus='';
				$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFA_RFS_STATUS']);			
				foreach ($StatusText as $Status)
				{
					$RFStatus = explode("~",$Status);				
					if (intval($RFStatus[0])==intval($rsRFA->FeldInhalt('RFS_STATUS')))
					{
						$RueckStatus = $RFStatus[1];					
					}				
				}
				$Form->Erstelle_ListenFeld('RFS_STATUS',$RueckStatus,0,400,false,($DS%2),'','','T','L','');
			}
			else
			{
				$RueckStatus='';
				$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFA_RFS_STATUS']);			
				foreach ($StatusText as $Status)
				{
					$RFStatus = explode("~",$Status);				
					if (intval($RFStatus[0])==intval($rsRFA->FeldInhalt('RFS_STATUS')))
					{
						$RueckStatus = $RFStatus[1];					
					}				
				}
				$Form->Erstelle_ListenFeld('RFS_STATUS',$RueckStatus,0,250,false,($DS%2),'','','T','L','');
			}
			
			if(($Recht3910&8)==8)
			{
				$SQL = 'SELECT RFS_STATUS FROM RUECKFUEHRUNGSANFORDERUNGEN JOIN RUECKFUEHRUNGSSTATUS ON (RFA_RFS_KEY=RFS_KEY)';
				$SQL.= ' WHERE RFA_KEY='.$DB->WertSetzen('RFA','N0',intval('0'.$rsRFA->FeldInhalt('RFA_KEY')));
				
				$rsRFAStatus = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFA'));
				$rsRFAStatusZeilen = $rsRFAStatus->AnzahlDatensaetze();
				
				if($DB->FeldInhaltFormat('Z',$rsRFAStatus->FeldInhalt('RFS_STATUS'))>=19)
				{
					$Link='/rueckfuehrung/anforderung/rueckfuehrung_PDF.php?RFA_KEY='.$rsRFA->FeldInhalt('RFA_KEY');
					$Form->Erstelle_ListenBild('href','PDF',$Link,'/bilder/pdf.png','',($DS%2),'',22,22);					
				}
				else{
					$Form->Erstelle_ListenFeld('','',22,22,false,($DS%2),'','','T');					
				}
				
				if(($Recht3910&32)==32)
				{
					$Form->Erstelle_Checkbox('RFC_Wert'.'_'.$rsRFA->FeldInhalt('RFA_KEY'),($rsRFA->FeldInhalt('RFC_WERT')=='on'?$rsRFA->FeldInhalt('RFC_WERT'):'off'),100,(($Recht3900&2)!=0));
					$Form->Erstelle_HiddenFeld('RFC_LISTE',1);	
				}
				
			}
			$Form->ZeileEnde();

			$rsRFA->DSWeiter();
			$DS++;
		}
		
		$Link = './rueckfuehrung_Main.php?cmdAktion=Details&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		
		$Form->Formular_Ende();
		
		//Schaltfl�chen
		$Form->SchreibeHTMLCode('<form name="frmRueckfuehrung" action="./rueckfuehrung_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');				
		$Form->SchaltflaechenStart();			
		$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		
		if(($Recht3910&32)==32)
		{
			if(($Recht3900&(2+4))!==0)
			{				
				$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');							
			}
		}
		
		$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=rueckfuehrungsaufforderung&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
		
		$Form->SchaltflaechenEnde();					
		$Form->SchreibeHTMLCode('</form>');

	}			// Ein einzelner Datensatz
	else
	{
		if(($Recht3910&32)==32 && $AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING)!='')
		{
			$Form->Formular_Start();

			$Form->ZeileStart();
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFA_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFA_DATUM'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_DATUM'],100,'',$Link);
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFA_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFA_BEMERKUNG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_BEMERKUNG'],300,'',$Link);
			$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFS_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFS_STATUS'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],300,'',$Link);
			if(($Recht3910&8)==8)
			{
				$Form->Erstelle_Liste_Ueberschrift('&nbsp;',22);
				if(($Recht3910&32)==32)
				{
					$Link = './rueckfuehrung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
					$Link .= '&Sort=RFC_WERT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFC_WERT'))?'~':'');
					$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['BereitsBearbeitet'],150,'',$Link);
				}
			}	
			$Form->ZeileEnde();
	
			$Form->ZeileStart();
			if(($Recht3910&16)==16)
			{	
				$Link = './rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$rsRFA->FeldInhalt('RFA_KEY').'';
			}
			else{
				$Link ='';
			}
			$Form->Erstelle_ListenFeld('RFA_DATUM',$rsRFA->FeldInhalt('RFA_DATUM'),0,100,false,'','',$Link,'D','L');
			$Form->Erstelle_ListenFeld('RFA_BEMERKUNG',$rsRFA->FeldInhalt('RFA_BEMERKUNG'),0,300,false,'','','','T','L');

			$RueckStatus='';
			$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFA_RFS_STATUS']);			
			foreach ($StatusText as $Status)
			{
				$RFStatus = explode("~",$Status);				
				if (intval($RFStatus[0])==intval($rsRFA->FeldInhalt('RFS_STATUS')))
				{
					$RueckStatus = $RFStatus[1];					
				}				
			}
			$Form->Erstelle_ListenFeld('RFS_STATUS',$RueckStatus,0,300,false,'','','','T','L','');											
			
			if(($Recht3910&8)==8)
			{

				$SQL = 'SELECT RFS_STATUS FROM RUECKFUEHRUNGSANFORDERUNGEN JOIN RUECKFUEHRUNGSSTATUS ON (RFA_RFS_KEY=RFS_KEY)';
				$SQL.= ' WHERE RFA_KEY='.$DB->WertSetzen('RFA','N0', intval('0'.$rsRFA->FeldInhalt('RFA_KEY')));
				
				$rsRFAStatus = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFA'));
				$rsRFAStatusZeilen = $rsRFAStatus->AnzahlDatensaetze();
				
				if($rsRFAStatus->FeldInhalt('RFS_STATUS')>='19')
				{
					$Link='/rueckfuehrung/anforderung/rueckfuehrung_PDF.php?RFA_KEY='.$rsRFA->FeldInhalt('RFA_KEY');
					$Form->Erstelle_ListenBild('href','PDF',$Link,'/bilder/pdf.png','','','',22,22);
				}
				else{
					$Form->Erstelle_ListenFeld('','',22,22,false,'','','','T');
				}
				if(($Recht3910&32)==32)
				{
					$Form->Erstelle_Checkbox('RFC_Wert'.'_'.$rsRFA->FeldInhalt('RFA_KEY'),($rsRFA->FeldInhalt('RFC_WERT')=='on'?$rsRFA->FeldInhalt('RFC_WERT'):'off'),150,(($Recht3900&2)!=0));	
					$Form->Erstelle_HiddenFeld('RFC_LISTE',1);	
				}
			}
			$Form->ZeileEnde();
	
			$Form->Formular_Ende();
			
			if(($Recht3910&32)==32)
			{
				if(($Recht3900&(2+4))!==0)
				{
					$Form->SchreibeHTMLCode('<form name="frmRueckfuehrung" action="./rueckfuehrung_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');
					
					$Form->SchaltflaechenStart();
					$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
					$Form->SchaltflaechenEnde();
					$Form->SchreibeHTMLCode('</form>');
				}
			}
		}
		
		if(($Recht3910&16)==16)
		{
			$Form->SchreibeHTMLCode('<form name="frmRueckfuehrung" action="./rueckfuehrung_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');
	
			$AWIS_KEY1 = $rsRFA->FeldInhalt('RFA_KEY');
			$RUECKFUEHRUNGSTYP = $rsRFA->FeldInhalt('RFA_TYP');
	
			$AWISBenutzer->ParameterSchreiben('AktuellerRFA', serialize($AWIS_KEY1));
			$Form->SchreibeHTMLCode('<input type="hidden" name="txtRFA_KEY" value="'.$AWIS_KEY1. '">');
	
			$Form->Formular_Start();
	
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrung_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsRFA->FeldInhalt('RFA_USER')));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsRFA->FeldInhalt('RFA_USERDAT')));
			$Form->InfoZeile($Felder,'');
	
			$EditRecht=(($Recht3900&2)!=0);
	
			if($AWIS_KEY1==0)
			{
				$EditRecht=true;
			}
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFA']['RFA_DATUM'].':',150);
			$Form->Erstelle_TextFeld('RFA_DATUM',($AWIS_KEY1===0?'':$rsRFA->Feldinhalt('RFA_DATUM')),10,150,$EditRecht,'','','','D','L');

			if ($AWIS_KEY1 != 0)
			{
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFA']['RFA_LFDNR'].':'.str_pad($Form->Format('T',$rsRFA->Feldinhalt('RFA_LFDNR')), 3, '0', STR_PAD_LEFT),30);				
				$Form->Erstelle_HiddenFeld('RFA_LFDNR',$rsRFA->Feldinhalt('RFA_LFDNR'));
			}

			$Form->ZeileEnde();

			if($AWIS_KEY1>0){
			    $AWISCursorPosition = 'txtRFA_BEMERKUNG';
            }else{
                $AWISCursorPosition='txtRFA_DATUM';
            }
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFA']['RFA_BEMERKUNG'].':',150);
            $Form->Erstelle_TextFeld('RFA_BEMERKUNG',($AWIS_KEY1===0?'':$rsRFA->Feldinhalt('RFA_BEMERKUNG')),120,350,$EditRecht,'','','','T','L');

			$Form->ZeileEnde();
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFA']['RFA_TYP'].':',150);
			$Texte = explode("|",$AWISSprachKonserven['RFA']['lst_RFA_TYP']);
			$Form->Erstelle_SelectFeld('RFA_TYP',($AWIS_KEY1===0?'':$rsRFA->Feldinhalt('RFA_TYP')),100,$EditRecht,'','','','','',$Texte,'','');
            $Form->ZeileEnde();

	
			
			$RueckStatus='';
			$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFA_RFS_STATUS']);			
			foreach ($StatusText as $Status)
			{
				$RFStatus = explode("~",$Status);				
				if (intval($RFStatus[0])==intval($rsRFA->FeldInhalt('RFS_STATUS')))
				{
					$RueckStatus = $RFStatus[1];
				}				
			}			
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Status'].':',150);

            if(($Recht3910&64)==64){
                //Wenn die R�ckf�hrung auf Anforderung steht, soll ein "Erinnerungsbutton" angezeigt werden.
                //Mit diesem, kann man Filialen, die die R�ckf�hrung noch nicht bearbeitet haben, erneut per
                //Email erinnern
                if($rsRFA->FeldInhalt('RFS_STATUS')==22){
                    $ErSQL  = 'SELECT RFC_FIL_ID from RUECKFUEHRUNGSCHECKS ';
                    $ErSQL  .= ' where RFC_RFA_KEY = '.$DB->WertSetzen('ERI','N0',$AWIS_KEY1);
                    $ErSQL  .= ' and RFC_WERT != \'on\' ';

                    $rsERI = $DB->RecordSetOeffnen($ErSQL,$DB->Bindevariablen('ERI'));
                    if($rsERI->AnzahlDatensaetze()>0){
                        $Form->Schaltflaeche('script','Erinnerung',$Form->PopupOeffnen(2),'/bilder/cmd_mail.png','Erinnerung','','',20,20,'',true);
                        ob_start();
                        $Form->ZeileStart('width: 440px');
                        $Form->Erstelle_Liste_Ueberschrift('Filialen',420);;
                        $Form->ZeileEnde();
                        $DS = 1;
                        $ZeileStart = true;
                        $ZeileEnde = false;
                        while(!$rsERI->EOF()){
                            if($ZeileStart){
                                $Form->ZeileStart('width: 440px');
                                $ZeileStart = false;
                                $ZeileEnde = true;
                            }
                            $Form->Erstelle_ListenFeld('Fil',$rsERI->FeldInhalt(1),5,70);
                            if(($DS%6)==0 and $DS != 0) {
                                $Form->ZeileEnde();
                                $ZeileStart = true;
                                $ZeileEnde = false;
                            }
                            $DS++;
                            $rsERI->DSWeiter();
                        }
                        if($ZeileEnde){
                            $Form->ZeileEnde();
                        }
                        $PopUp = ob_get_clean();

                        $Form->PopupDialog('Filialen erneut benachrichtigen',$PopUp,array(array('/bilder/cmd_dsloeschen.png','','close',''),array('/bilder/cmd_weiter.png','cmdErinnerung','post','')),awisFormular::POPUP_INFO,2);
                    }
                }
            }

			$Form->Erstelle_TextFeld('RFS_STATUS',($AWIS_KEY1===0?'':$RueckStatus),50,100,false,'','','','T','L');

            if(($Recht3910&64)==64) {

                if($rsRFA->FeldInhalt('RFS_STATUS')==22) {
                    $SQL = 'SELECT trunc(MVS_EINGESTELLT) as MVS_EINGESTELLT, count(*) as ANZ FROM mailversand';
                    $SQL .= ' WHERE MVS_XTN_KUERZEL = \'RFA\'';
                    $SQL .= ' AND MVS_XXX_KEY ='.$DB->WertSetzen('RFA','N0',$AWIS_KEY1);
                    $SQL .= ' GROUP BY trunc(mvs_eingestellt) order by 1';

                    $rsStatusErinnerung = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RFA'));

                    if($rsStatusErinnerung->AnzahlDatensaetze()>0){
                        $Form->Schaltflaeche('script','StatEr',$Form->PopupOeffnen(3),'/bilder/cmd_uhr.png','StatEr','','',20,20,'',true);

                        ob_start();
                        $Form->ZeileStart();
                        $Form->Erstelle_Liste_Ueberschrift('Zuletzt gesendete Nachrichten am...',350);
                        $Form->Erstelle_Liste_Ueberschrift('Anzahl',70);
                        $Form->ZeileEnde();
                        $DS=1;
                        while(!$rsStatusErinnerung->EOF() and $DS <= 5){
                            $Form->ZeileStart('width:440px');
                            $Form->Erstelle_ListenFeld('Zeit',$rsStatusErinnerung->FeldInhalt('MVS_EINGESTELLT','D'),17,350);
                            $Form->Erstelle_ListenFeld('Anzahl',$rsStatusErinnerung->FeldInhalt('ANZ','N0'),4,70);
                            $rsStatusErinnerung->DSWeiter();
                            $DS++;
                            $Form->ZeileEnde();
                        }

                        $PopUp2 = ob_get_clean();
                        $Form->PopupDialog('Letzte Benachrichtigungen',$PopUp2,array(array('/bilder/cmd_dsloeschen.png','','close','')),awisFormular::POPUP_INFO,3);
                    }
                }
            }

            $Form->ZeileEnde();


			$Form->Formular_Ende();

            if(!isset($_POST['cmdDSNeu_x']))
			{
				$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Positionen'));
				$Register= new awisRegister(3902);
				$Register->ZeichneRegister($RegisterSeite);
			}
	
	
			//***************************************
			// Schaltfl�chen f�r dieses Register
			//***************************************
			$Form->SchaltflaechenStart();
			
			$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

			$SQL = 'SELECT RFS_STATUS FROM RUECKFUEHRUNGSANFORDERUNGEN JOIN RUECKFUEHRUNGSSTATUS ON (RFA_RFS_KEY=RFS_KEY)';
			$SQL.= ' WHERE RFA_KEY='.$DB->WertSetzen('RFA','N0',intval('0'.$AWIS_KEY1));
			
			$rsRFAStatus = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RFA'));
			$rsRFAStatusZeilen = $rsRFAStatus->AnzahlDatensaetze();

            if(($Recht3900&(2+4))!==0)
			{
				if($rsRFAStatus->FeldInhalt('RFS_STATUS')<'20')
				{
					$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
				}
			}
			if(($Recht3900&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
			{
				$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
			}
			if(($Recht3900&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
			{
				if($rsRFAStatus->FeldInhalt('RFS_STATUS')<'20')
				{
					$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
				}
			}
	
			$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
			$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');


			if($rsRFAStatus->FeldInhalt('RFS_STATUS')<'20')
			{
				if(($Recht3910&2)==2)
				{
					$Form->Schaltflaeche('href', 'cmdSimulation', './rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$AWIS_KEY1.'&Simulation='.$RUECKFUEHRUNGSTYP, '/bilder/cmd_statistik.png', $AWISSprachKonserven['Wort']['lbl_RueckfuehrungSimulation'], '');
				}	
			}
			
			if($rsRFAStatus->FeldInhalt('RFS_STATUS')>='19')
			{
				if(($Recht3910&8)==8)
				{
					$Form->Schaltflaeche('href', 'cmdPDF', './rueckfuehrung_PDF.php?RFA_KEY='.$AWIS_KEY1, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_RueckfuehrungListe'], '');
				}
				
				if($rsRFAStatus->FeldInhalt('RFS_STATUS')<'22' && ($Recht3910&4)==4)
				{
					$Form->Schaltflaeche('href', 'cmdAnforderung', './rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$AWIS_KEY1.'&Anforderung='.$RUECKFUEHRUNGSTYP, '/bilder/cmd_antenne.png', $AWISSprachKonserven['Wort']['lbl_RueckfuehrungAnforderung'], '');
				}
			}					
	
			$Form->SchaltflaechenEnde();
	
			$Form->SchreibeHTMLCode('</form>');
		}
	}

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode('document.getElementsByName(\''.$AWISCursorPosition.'\')[0].focus();');
		$Form->SchreibeHTMLCode('</Script>');
	}

}
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	
	$AWISBenutzer = awisBenutzer::Init();
	
	$Recht3910= $AWISBenutzer->HatDasRecht(3910);

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Bedingung = '';

	if($AWIS_KEY1 <> 0)
	{
		$Bedingung = 'AND RFA_KEY = '.$AWIS_KEY1;
		return $Bedingung;
	}

	if(isset($Param[0]) && $Param[0]!='')		// Datum
	{
		$Bedingung .= 'AND RFA_DATUM = ' . $DB->WertSetzen('RFA','D',$Param[0]) . ' ';
	}

	if(isset($Param[1]) && $Param[1]!='')		// Artikel
	{
		$Bedingung .= 'AND RFA_KEY IN (SELECT DISTINCT RFP_RFA_KEY FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_AST_ATUNR'.$DB->LikeOderIst($Param[1],awisDatenbank::AWIS_LIKE_UPPER ).')';
	}
	
	if(isset($Param[2]) && $Param[2]!='')		// Status
	{
		$Bedingung .= 'AND (RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS =' . $DB->WertSetzen('RFA','NO',$Param[2]);
        $Bedingung .= ') OR RFA_KEY IN (SELECT DISTINCT RFA_KEY FROM RUECKFUEHRUNGSANFORDERUNGEN, RUECKFUEHRUNGSANFORDERUNGENPOS, RUECKFUEHRUNGSANFPOSFILMENGEN ' ;
        $Bedingung .= ' WHERE RFA_KEY=RFP_RFA_KEY AND RFP_KEY=RFM_RFP_KEY AND (RFP_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS =';
        $Bedingung .= $DB->WertSetzen('RFA','N0',$Param[2]);
        $Bedingung .=' ) OR RFM_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS =';
        $Bedingung .= $DB->WertSetzen('RFA','N0',$Param[2]).'))))';
	}
	
	if(isset($Param[3]) && $Param[3]!='')		// Filiale
	{
        $Bedingung .= 'AND RFA_KEY IN (SELECT DISTINCT RFP_RFA_KEY FROM RUECKFUEHRUNGSANFORDERUNGENPOS JOIN RUECKFUEHRUNGSANFPOSFILMENGEN ON (RFP_KEY=RFM_RFP_KEY) WHERE RFM_FIL_ID IN (';
        $In = explode(',',$Param[3]);
        foreach ($In as $Wert){
            $Bedingung .= $DB->WertSetzen('RFA','N0',$Wert) . ',';
        }
        $Bedingung .= $DB->WertSetzen('RFA','N0',-1);
		if(($Recht3910&32)==32)
		{
			if(isset($Param[1]) && $Param[1]!='')
			{
                $Bedingung .= ') AND RFP_AST_ATUNR'.$DB->LikeOderIst($Param[1],awisDatenbank::AWIS_LIKE_UPPER ).') ';
			}
			else
			{
				$Bedingung .= ' )) ';
			}
			$Bedingung .= ' AND RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=22) ';
		}
		else
		{
			if(isset($Param[1]) && $Param[1]!='')
			{
				$Bedingung .= ')  AND RFP_AST_ATUNR'.$DB->LikeOderIst($Param[1],awisDatenbank::AWIS_LIKE_UPPER ).') ';
			}
		}
	}
	
	if(isset($Param[4]) && $Param[4]!='')		// Anforderungs-Nummer
	{
		$poslfdnr = strpos($Param[4],'-');
		
		if ($poslfdnr==false)
		{
			$Bedingung .= "AND to_char(RFA_NR)" . $DB->LikeOderIst($Param[4]) . " ";
		}
		else 
		{		
			$Bedingung .= "AND to_char(RFA_NR || '-' || LPAD(RFA_LFDNR, 3, 0))" . $DB->LikeOderIst($Param[4]) . " ";
		}
	}

	//if(isset($Param[5]) && $Param[5]!='-1' && ($Recht3910&32)==32) 		// RFA_Check: Filiale Anforderung bearbeitet
	if(isset($Param[5]) && $Param[5]!='-1' && $Param[5]!='' && ($Recht3910&32)==32) 		// RFA_Check: Filiale Anforderung bearbeitet
	{
		if ($Param[5]=='off')
		{
			$Bedingung .= 'AND (RFC_WERT ' . $DB->LikeOderIst($Param[5]) . '  or RFC_WERT is null) ';
		}
		else 
		{
			$Bedingung .= 'AND RFC_WERT ' . $DB->LikeOderIst($Param[5]) . ' ';
		}
	}
	
	return $Bedingung;
}
?>