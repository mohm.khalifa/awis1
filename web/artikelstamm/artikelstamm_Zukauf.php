<?php
/**
 * Zukauf
 *
 * Wird nicht in den Filialen angezeigt => keine Bildschirmbreitenanpassung
 *
 * @author    Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version   200901
 *
 */
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('ZUB', '%');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'Altteilwert');
$TextKonserven[] = array('Wort', 'AktuellesSortiment');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'KeineDatenVorhanden');
$TextKonserven[] = array('Wort', 'ZusammenfassungBestellungen');
$TextKonserven[] = array('Liste', 'lst_AktivInaktiv');
$TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Fehler', 'err_keineDatenbank');

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht457 = $AWISBenutzer->HatDasRecht(457);            // Recht f�r die Lieferantenartikel
    if ($Recht457 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if (isset($_GET['SSort'])) {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SSort']);
    } else {
        $ORDERBY = ' ORDER BY ZUB_BESTELLDATUM DESC, ZUB_FIL_ID';
    }

    $BindeVariablen = array();
    $BindeVariablen['var_N0_ast_key'] = '0' . $AWIS_KEY1;

    $SQL = 'SELECT Zukaufbestellungen.*, ZWA_WANR';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM Zukaufbestellungen ';
    $SQL .= ' LEFT OUTER JOIN ZukaufWerkstattAuftraege ON ZUB_KEY = ZWA_ZUB_KEY';
    $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENARTIKELIDS ON ZUB_ZLA_KEY = ZAI_ZLA_KEY AND ZAI_AID_NR = 4';
    $SQL .= ' INNER JOIN Artikelstamm ON AST_ATUNR = ZAI_ID AND AST_KEY = :var_N0_ast_key';

    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
    // Zum Bl�ttern in den Daten
    $Block = 1;
    if (isset($_REQUEST['Block'])) {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    }
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;

    $SQL .= $ORDERBY;

    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);

    $rsZUB = $DB->RecordsetOeffnen($SQL, $BindeVariablen);
    $Form->Formular_Start();

    if ($rsZUB->AnzahlDatensaetze() > 0) {
        $Form->ZeileStart();
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Zukauf' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=ZUB_BESTELLDATUM' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'ZUB_BESTELLDATUM'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'], 120, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Zukauf' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=ZUB_FIL_ID' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'ZUB_FIL_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_FIL_ID'], 80, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Zukauf' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=ZUB_BESTELLMENGE' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'ZUB_BESTELLMENGE'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'], 150, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Zukauf' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=ZUB_EINHEITENPROPREIS' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'ZUB_EINHEITENPROPREIS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_EINHEITENPROPREIS'], 150, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Zukauf' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&SSort=ZUB_PREISBRUTTO' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'ZUB_PREISBRUTTO'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'], 200, '', $Link);
        if (($Recht457 & 32) == 32) {
            $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Zukauf' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
            $Link .= '&SSort=ZUB_PREISANGELIEFERT' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'ZUB_PREISANGELIEFERT'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISANGELIEFERT'], 200, '', $Link);
        }
        $Form->ZeileEnde();

        // Blockweise
        $StartZeile = 0;
        if (isset($_GET['Block'])) {
            $StartZeile = intval($_GET['Block']) * $MaxDSAnzahl;
        }

        $Menge = 0;
        $Vorgaenge = 0;
        $Mengen = array();

        $DS = 0;
        while (!$rsZUB->EOF()) {
            $Form->ZeileStart();
            $Link = '';
            if (($Recht457 & 2) != 0) {
                $Link = '/zukauf/zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=' . $rsZUB->FeldInhalt('ZUB_KEY');
            }
            $Form->Erstelle_ListenFeld('ZUB_BESTELLDATUM', $rsZUB->FeldInhalt('ZUB_BESTELLDATUM'), 0, 120, false, ($DS % 2), '', $Link, 'D');
            $Link = '/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsZUB->FeldInhalt('ZUB_FIL_ID');    // TODO: Sp�ter auf die Hersteller
            $Form->Erstelle_ListenFeld('ZUB_FIL_ID', $rsZUB->FeldInhalt('ZUB_FIL_ID'), 0, 80, false, ($DS % 2), '', $Link, 'T', 'L');
            $Link = '';
            $Style = '';
            if (stripos($rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG'), 'ALTTEILWERT') !== false) {
                $Style = 'color:#F00FFF;font-weight:bold;';
            }
            $Form->Erstelle_ListenFeld('ZUB_BESTELLMENGE', $rsZUB->FeldInhalt('ZUB_BESTELLMENGE'), 0, 150, false, ($DS % 2), $Style, $Link, 'Z', 'L',
                $AWISSprachKonserven['Wort']['Altteilwert']);
            $Form->Erstelle_ListenFeld('ZUB_EINHEITENPROPREIS', $rsZUB->FeldInhalt('ZUB_EINHEITENPROPREIS'), 0, 150, false, ($DS % 2), '', $Link, 'Z', 'L');
            $Form->Erstelle_ListenFeld('ZUB_PREISBRUTTO', $rsZUB->FeldInhalt('ZUB_PREISBRUTTO'), 0, 200, false, ($DS % 2), '', $Link, 'N2', 'L');
            if (($Recht457 & 32) == 32) {
                $Form->Erstelle_ListenFeld('ZUB_PREISANGELIEFERT', $rsZUB->FeldInhalt('ZUB_PREISANGELIEFERT'), 0, 200, false, ($DS % 2), '', $Link, 'N2', 'L');
            }

            if (stripos($rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG'), 'ALTTEILWERT') === false) {
                $Menge += $rsZUB->FeldInhalt('ZUB_BESTELLMENGE');
                $Vorgaenge++;
                if (!isset($Mengen[date('Y', $Form->PruefeDatum($rsZUB->FeldInhalt('ZUB_BESTELLDATUM'), false, false, true))])) {
                    $Mengen[date('Y', $Form->PruefeDatum($rsZUB->FeldInhalt('ZUB_BESTELLDATUM'), false, false, true))] = 0;
                }
                $Mengen[date('Y', $Form->PruefeDatum($rsZUB->FeldInhalt('ZUB_BESTELLDATUM'), false, false, true))] += $rsZUB->FeldInhalt('ZUB_BESTELLMENGE');
            }

            $Form->ZeileEnde();

            $rsZUB->DSWeiter();
            $DS++;
        }

        // Summen ermitteln
        $BindeVariablen = array();
        $BindeVariablen['var_N0_ast_key'] = '0' . $AWIS_KEY1;

        $SQL = 'SELECT SUM(ZUB_BESTELLMENGE) AS MENGE, COUNT(*) AS VORGAENGE';
        $SQL .= ' FROM Zukaufbestellungen ';
        $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENARTIKELIDS ON ZUB_ZLA_KEY = ZAI_ZLA_KEY AND ZAI_AID_NR = 4';
        $SQL .= ' INNER JOIN Artikelstamm ON AST_ATUNR = ZAI_ID AND AST_KEY = :var_N0_ast_key';
        $SQL .= " WHERE ZUB_ARTIKELBEZEICHNUNG <> 'ALTTEILWERT'";
        $rsSumme = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

        $Form->Trennzeile();

        $Form->ZeileStart();
        $Text = $AWISSprachKonserven['Wort']['ZusammenfassungBestellungen'];
        $Text = str_replace('#MENGE#', $rsSumme->FeldInhalt('MENGE'), $Text);
        $Text = str_replace('#VORGAENGE#', $rsSumme->FeldInhalt('VORGAENGE'), $Text);
        $Form->Erstelle_TextLabel($Text, 0);
        $Form->ZeileEnde();

        $BindeVariablen = array();
        $BindeVariablen['var_N0_ast_key'] = '0' . $AWIS_KEY1;

        $SQL = 'SELECT SUM(ZUB_BESTELLMENGE) AS MENGE, COUNT(*) AS VORGAENGE, TO_CHAR(ZUB_BESTELLDATUM,\'YYYY\') AS JAHR';
        $SQL .= ' FROM Zukaufbestellungen ';
        $SQL .= ' INNER JOIN ZUKAUFLIEFERANTENARTIKELIDS ON ZUB_ZLA_KEY = ZAI_ZLA_KEY AND ZAI_AID_NR = 4';
        $SQL .= ' INNER JOIN Artikelstamm ON AST_ATUNR = ZAI_ID AND AST_KEY = :var_N0_ast_key';
        $SQL .= " WHERE ZUB_ARTIKELBEZEICHNUNG <> 'ALTTEILWERT'";
        $SQL .= ' GROUP BY TO_CHAR(ZUB_BESTELLDATUM,\'YYYY\')';
        $SQL .= ' ORDER BY 3 DESC';
        $rsSumme = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
        $Jahressummen = '';
        $DS = 0;
        while (!$rsSumme->EOF()) {
            $Jahressummen .= ', ' . $rsSumme->FeldInhalt('JAHR') . ':' . $rsSumme->FeldInhalt('MENGE') . '/' . $rsSumme->FeldInhalt('VORGAENGE');
            $rsSumme->DSWeiter();
            if (++$DS > 5) {
                break;
            }
        }
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel(substr($Jahressummen, 2), 0);
        $Form->ZeileEnde();

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Zukauf';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else {
        $Form->Erstelle_TextFeld('#Hinweis', $AWISSprachKonserven['Wort']['KeineDatenVorhanden'], 20, 500);
    }

    $Form->Formular_Ende();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201320");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201321");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>