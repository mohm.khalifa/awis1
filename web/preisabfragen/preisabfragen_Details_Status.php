<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PFA','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AlleSetzen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8001= $AWISBenutzer->HatDasRecht(8001);		// Herstelle
	if(($Recht8001&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY PFA_FIL_ID DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT count(*) AS GESAMT, PFA_FIL_ID';
	$SQL .= ', SUM(CASE WHEN PFA_ERMITTLUNGSSTATUS=1 AND PFA_PREIS IS NULL THEN 1 ELSE 0 END) AS OFFEN';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM PreisAbfragenPreise ';
	$SQL .= ' WHERE PFA_PAF_KEY = 0'.$AWIS_KEY1;
	$SQL .= ' GROUP BY PFA_FIL_ID';

	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		$Param['BLOCK']=$Block;
		//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
	}
	elseif(isset($Param['BLOCK']))
	{
		$Block=$Param['BLOCK'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

	$SQL .= $ORDERBY;

	$rsPFA = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1,$SQL);
	$Param['FIL_ID']=$AWIS_KEY1;

	$Form->Formular_Start();
	$DS=0;
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_FIL_ID'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_GESAMT'],100);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PFA']['PFA_OFFEN'],100);
	$Form->ZeileEnde();

	while(!$rsPFA->EOF())
	{
		$Form->ZeileStart();
		$Link = '/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsPFA->FeldInhalt('PFA_FIL_ID');
		$Form->Erstelle_ListenFeld('#FIL',$rsPFA->FeldInhalt('PFA_FIL_ID'),10,100,false,($DS%2),'',$Link);
		$Form->Erstelle_ListenFeld('#FIL',$rsPFA->FeldInhalt('GESAMT'),10,100,false,($DS%2));
		$Form->Erstelle_ListenFeld('#FIL',$rsPFA->FeldInhalt('OFFEN'),10,100,false,($DS%2));
		$Form->ZeileEnde();
		$rsPFA->DSWeiter();
		$DS++;
	}
	$Link = './preisabfragen_Main.php?cmdAktion=Details&PAF_KEY='.$AWIS_KEY1;
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201016061422");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201016061421");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>