<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?
//********************************************************************************************
// Lieferkontrolle
//********************************************************************************************
//
// Dieses Modul erstellt eine PDF - Datei für Rädereinlagerungen der Filialen
//
//********************************************************************************************
// Autor: Thomas Riedl
// Datum: 25.03.2008
//********************************************************************************************
// Änderungen:
// WER                   WANN      WAS
//
//
//********************************************************************************************
global $AWISBenutzer;

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

?>
</head>

<body>
<?
global $con;
global $awisRSZeilen;
global $awisDBFehler;

require_once 'fpdi.php';

clearstatcache();

include ("ATU_Header.php");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung möglich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,3602);	// MDE Raedereinlagerung
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Raedereinlagerung-PDF', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug(1,$_GET);


if ($_REQUEST['REL_LFDNR']!='' && $_REQUEST['REL_FIL_ID']!= '' ) // && ($RechteStufe&16)==16)
{
	$FILIALE = intval($_REQUEST['REL_FIL_ID']);
	
	/***************************************
	* Hole Daten per SQL
	***************************************/
	
	$SQL = 'SELECT *';
	$SQL .=' FROM Raedereinlagerungen REL';
	$SQL .=' WHERE REL_FIL_ID=0'.intval($_REQUEST['REL_FIL_ID']);
	$SQL .= ' AND REL_LFDNR=0'.intval($_REQUEST['REL_LFDNR']);
	$SQL .= ' ORDER BY REL_NR ASC';
		
	awis_Debug(1,$SQL);
	
	$rsREL = awisOpenRecordset($con,$SQL);
	$rsRELZeilen = $awisRSZeilen;
	
	if($rsRELZeilen==0)		// Keine Daten
	{
		die("<center><span class=HinweisText>Keine Daten gefunden!</span></center>");
	}
	else 
	{
		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
	
		define('FPDF_FONTPATH','font/');
		$pdf = new fpdi('p','mm','a4');
		$pdf->open();
		$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
		$ATULogo = $pdf->ImportPage(1);				
		
		$Zeile=0;
		$Seitenrand=15;
		$Seite = 1;
		
		NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true,$FILIALE,$Seite);
				
		$LinkerRand=$Seitenrand;
			
		$Y_Wert=16;
		
		$LFDNR = $rsREL['REL_LFDNR'][0];		
		
		$Y_Wert=$Y_Wert+5;
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,"Lieferschein-Nr:",0,0,'L',0);
		
		$pdf->setXY($LinkerRand+30,$Y_Wert);
		$pdf->cell(40,6,$LFDNR,0,0,'L',0);		
		
		$Y_Wert=$Y_Wert+10;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,12,"Einlagerungsnr.",1,0,'L',1);
		
		$pdf->setXY($LinkerRand+30,$Y_Wert);
		$pdf->cell(30,12,"Scandatum",1,0,'L',1);		
		
		$pdf->setXY($LinkerRand+60,$Y_Wert);
		$pdf->cell(30,12,"Menge",1,0,'L',1);						
		
		$pdf->setXY($LinkerRand+90,$Y_Wert);
		$pdf->cell(40,6,"Beschädigt",1,0,'C',1);						
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(50,12,"Bemerkung",1,0,'L',1);						
		
		$Y_Wert=$Y_Wert+6;
		
		$pdf->setXY($LinkerRand+90,$Y_Wert);
		$pdf->cell(10,6,"VL",1,0,'C',1);						
		
		$pdf->setXY($LinkerRand+100,$Y_Wert);
		$pdf->cell(10,6,"VR",1,0,'C',1);						
		
		$pdf->setXY($LinkerRand+110,$Y_Wert);
		$pdf->cell(10,6,"HL",1,0,'C',1);						
		
		$pdf->setXY($LinkerRand+120,$Y_Wert);
		$pdf->cell(10,6,"HR",1,0,'C',1);						
		
		
		for($RELZeile=0;$RELZeile<$rsRELZeilen;$RELZeile++)
		{
			if ($Y_Wert > 270)
			{
				$Y_Wert=16;
				$Y_Wert=$Y_Wert+10;
				$Seite = $Seite + 1;
				
				NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,true,$FILIALE,$Seite);
				
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(30,12,"Einlagerungsnr.",1,0,'L',1);
				
				$pdf->setXY($LinkerRand+30,$Y_Wert);
				$pdf->cell(30,12,"Scandatum",1,0,'L',1);				
				
				$pdf->setXY($LinkerRand+60,$Y_Wert);
				$pdf->cell(30,12,"Menge",1,0,'L',1);				
				
				$pdf->setXY($LinkerRand+90,$Y_Wert);
				$pdf->cell(30,6,"Beschädigt",1,0,'C',1);						
		
				$pdf->setXY($LinkerRand+130,$Y_Wert);
				$pdf->cell(50,12,"Bemerkung",1,0,'L',1);		

				$Y_Wert=$Y_Wert+6;
				
				$pdf->setXY($LinkerRand+90,$Y_Wert);
				$pdf->cell(10,6,"VL",1,0,'C',1);						
				
				$pdf->setXY($LinkerRand+100,$Y_Wert);
				$pdf->cell(10,6,"VR",1,0,'C',1);						
				
				$pdf->setXY($LinkerRand+110,$Y_Wert);
				$pdf->cell(10,6,"HL",1,0,'C',1);						
				
				$pdf->setXY($LinkerRand+120,$Y_Wert);
				$pdf->cell(10,6,"HR",1,0,'C',1);									
			}
			
			$EINLAGERUNGSNR=$rsREL['REL_NR'][$RELZeile];
			$SCANDATUM=$rsREL['REL_DATUMSCAN'][$RELZeile];
			$MENGE=$rsREL['REL_MENGE'][$RELZeile];
			$VL=$rsREL['REL_BESCHAEDIGT_VL'][$RELZeile];
			$VR=$rsREL['REL_BESCHAEDIGT_VR'][$RELZeile];
			$HL=$rsREL['REL_BESCHAEDIGT_HL'][$RELZeile];
			$HR=$rsREL['REL_BESCHAEDIGT_HR'][$RELZeile];
			$BEMERKUNG=$rsREL['REL_BEMERKUNG'][$RELZeile];
			
			$Y_Wert=$Y_Wert+6;
			$pdf->SetFont('Arial','',9);
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(30,6,$EINLAGERUNGSNR,1,0,'L',0);	
			
			$pdf->setXY($LinkerRand+30,$Y_Wert);
			$pdf->cell(30,6,$SCANDATUM,1,0,'L',0);
			
			$pdf->setXY($LinkerRand+60,$Y_Wert);
			$pdf->cell(30,6,$MENGE,1,0,'L',0);			
			
			$pdf->setXY($LinkerRand+90,$Y_Wert);
			$pdf->cell(10,6,(isset($VL) && strtoupper($VL)=='ON'?'X':''),1,0,'C',0);						
			
			$pdf->setXY($LinkerRand+100,$Y_Wert);
			$pdf->cell(10,6,(isset($VR) && strtoupper($VR)=='ON'?'X':''),1,0,'C',0);						
			
			$pdf->setXY($LinkerRand+110,$Y_Wert);
			$pdf->cell(10,6,(isset($HL) && strtoupper($HL)=='ON'?'X':''),1,0,'C',0);						
			
			$pdf->setXY($LinkerRand+120,$Y_Wert);
			$pdf->cell(10,6,(isset($HR) && strtoupper($HR)=='ON'?'X':''),1,0,'C',0);						
		
			$pdf->setXY($LinkerRand+130,$Y_Wert);
			$pdf->cell(50,6,$BEMERKUNG,1,0,'L',0);						
		}		
	}	
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->saveas($DateiName);
	
	echo "<br><a target=_new href=$DateiNameLink>PDF Datei öffnen</a><p>";
	echo "<hr><a href=./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=MDE&Unterseite=Raedereinlagerung><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
}	
	
/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Thomas Riedl
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param  bool Überschrift
* @param string Datum
* @param int Seite
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,$Ueberschrift,$FILIALE,$Seite)
{
	static $Seite;
	
	$pdf->addpage();									// Neue Seite hinzufügen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,$LinkerRand+160,4,20);		// Logo einbauen
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,290);					// Cursor setzen
	$pdf->Cell(180,3,'Seite '. $Seite,0,0,'C',0);
	//$pdf->Cell(180,3,'Dieses Formular ist nur für interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
		
	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','B',14);				// Schrift setzen
	
	// Ueberschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(180,6,"Lieferschein zur Rädereinlagerung von Filiale: ".$FILIALE,0,0,'C',0);
	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	//$Y_Wert=10;
	$Zeile = 22;
}	
	
?>
</body>
</html>
