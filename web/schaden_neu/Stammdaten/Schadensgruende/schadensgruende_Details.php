<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht40013 = $AWISBenutzer->HatDasRecht(40013);
	$EditRecht = ($Recht40013&2) == 2;
	$HinzuRecht = ($Recht40013&4) == 4;
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SCHAD_SGR'));
	
	$Fehler = 0;
	$Save = 0;
	$FehlerFest = false;
	$NeuAnlage = 0;	
	
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1);
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	
	
	if (isset($_GET['button']))
	{
		$Aktiv = '';
		if(isset($_GET['VERALTET']))
		{
			if($_GET['VERALTET'] == '1')
			{
				$Aktiv = 0;
			}
			else
			{
				$Aktiv = 1;
			}
			$Feld = 'VERALTET';
		}
		else if(isset($_GET['FILIALEN']))
		{
				if($_GET['FILIALEN'] == '1')
				{
					$Aktiv = 0;
				}
				else
				{
					$Aktiv = 1;
				}
				$Feld = 'SICHTBAR_FILIALEN';
		}
		$SQL = "UPDATE SCHADENSGRUND ";
		$SQL .="SET ".$Feld."=".$Aktiv;
		$SQL .= ',  GRUND_USER=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ',  GRUND_USERDAT=SYSDATE';
		$SQL .=" WHERE ID = " .$_GET['button'];
		$DBSCHAD->Ausfuehren($SQL);
	}

	
    if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
    {
		$Param['ORDER'] = 'GRUND';
 	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht40013==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	
	if(isset($_GET['ID']))
	{
		$AWIS_KEY1 = $_GET['ID'];
	}
	
	if(isset($_POST['txtID']))
	{
		$AWIS_KEY1 = $_POST['txtID'];
	}
	
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{	
		$Param['BES_GRUND'] = 	$Form->Format('T',$_POST['sucBES_GRUND'],true);
		$Param['BES_VERALTET']= $Form->Format('N0',$_POST['sucBES_VERALTET'],true);
		$Param['BES_FILIALEN']= $Form->Format('N0',$_POST['sucBES_FILIALEN'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
		
	//Speichernbutton oder Klick aufs F�hnchen
	if(isset($_POST['cmdSpeichern_x']))
	{
		$Fehler = 0;
		$SQL  = 'select * ';
		$SQL .=	' from schadensgrund where upper(GRUND)= upper(\''.$_POST['txtBES_GRUND'].'\')';
		
		$rsCheck = $DBSCHAD->RecordSetOeffnen($SQL);
		
		if($rsCheck->AnzahlDatensaetze() > 0 and $AWIS_KEY1 == '-1')
		{
			$Fehler += 1;
		}
		else if($AWIS_KEY1 != '-1')
		{
			if($rsCheck->AnzahlDatensaetze() > 0 and $rsCheck->FeldInhalt('ID') != $AWIS_KEY1)
			{
				$Fehler += 1;
			}
		}
		if($Fehler == 0)
		{
			include('./schadengruende_speichern.php');
			$AWIS_KEY1 = '';
		}
		else 
		{
			$Form->Hinweistext('Schadensgrund "'.$rsCheck->FeldInhalt('GRUND').'" bereits in der Datenbank vorhanden.');
		}
	}

	/* Daten beschaffen */
	
	$SQL = ' select ID,GRUND,VERALTET,SICHTBAR_FILIALEN,GRUND_USER, GRUND_USERDAT, row_number() OVER ( ';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	
	$SQL .= ' select ID,GRUND,VERALTET,NVL(SICHTBAR_FILIALEN,\'0\') as SICHTBAR_FILIALEN,GRUND_USER, GRUND_USERDAT';
	$SQL .= ' from( ';
	$SQL .= 'select * ';
	$SQL .=	' from schadensgrund)) ';

	
	if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '' or $SpeichernOK)
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		//if ($Bedingung != '' and (!isset($_POST['cmdHinzufuegen_x'])) and (!isset($_POST['cmdDSNeu_x'])) and !isset($_GET['HRT_KEY']) and $NeuAnlage != 1 and !isset($_POST['txtHRT_KEY']))
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}

	if($AWIS_KEY1 != '')
	{
		if($AWIS_KEY1 <> '')
		{
			$SQL .= ' WHERE ID='.$AWIS_KEY1;
		}
		else 
		{
			$SQL .= ' AND ID='.$AWIS_KEY1;
		}
		
	}
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	
	if(!isset($_GET['ID']) and $AWIS_KEY1 == '')
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsSGR = $DBSCHAD->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['BES_GRUND'] = 500;
	$FeldBreiten['BES_VERALTET'] = 20;
	$FeldBreiten['BES_FILIALEN'] = 20;
	
	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 2.5);
	}
	
	$Form->SchreibeHTMLCode('<form name="frmSGRDetails" action="./schadensgruende_Main.php?cmdAktion=Details " method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	if (($rsSGR->AnzahlDatensaetze() > 1))
	{
		$Form->ZeileStart();
		// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
	    $Link = './schadensgruende_Main.php?cmdAktion=Details&Sort=GRUND'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GRUND'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');

		
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_GRUND'], $FeldBreiten['BES_GRUND'], '', $Link);
		
		
		if (($Recht40013&2) == 2)
		{
			$Link = './schadensgruende_Main.php?cmdAktion=Details&Sort=VERALTET'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VERALTET'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBAKTIV'];
			$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['BES_VERALTET'], '',$Link,$TTT);
		}
		
		if (($Recht40013&2) == 2)
		{
			$Link = './schadensgruende_Main.php?cmdAktion=Details&Sort=SICHTBAR_FILIALEN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SICHTBAR_FILIALEN'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBFILIALEN'];
			$Form->Erstelle_Liste_Ueberschrift('S', $FeldBreiten['BES_FILIALEN'], '',$Link,$TTT);
		}
		/*
		if(($Recht34000&8)==8)
		{
			// �berschrift der Listenansicht: History
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_HIST'], $FeldBreiten['Hist'], '', '');
		}*/
		$Form->ZeileEnde();
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsSGR->EOF())
		{
			$Form->ZeileStart();
			if ($EditRecht)
			{
			    //Link zum Datensatz
			    $Link = './schadensgruende_Main.php?cmdAktion=Details&ID='.$rsSGR->FeldInhalt('ID') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			}
			else 
			{
				$Link = '';
			}
			$TTT = $rsSGR->FeldInhalt('GRUND');
			
			$Form->Erstelle_ListenFeld('BES_GRUND', $rsSGR->FeldInhalt('GRUND'), 0, $FeldBreiten['BES_GRUND'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
			$Link = '';
				
			
			$LinkAktiv = '';
			if (($Recht40013&2) == 2)
			{
				$LinkAktiv .=  './schadensgruende_Main.php?cmdAktion=Details&button='.$rsSGR->FeldInhalt('ID').'&VERALTET='.$rsSGR->FeldInhalt('VERALTET');
				if ($rsSGR->FeldInhalt('VERALTET') == '1')
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Inaktiv']);
				}
				else
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Aktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['BES_VERALTET'] , ($DS%2));
				$IconsArray = '';
			}
			
			$LinkAktiv = '';
			if (($Recht40013&2) == 2)
			{
				$LinkAktiv .=  './schadensgruende_Main.php?cmdAktion=Details&button='.$rsSGR->FeldInhalt('ID').'&FILIALEN='.$rsSGR->FeldInhalt('SICHTBAR_FILIALEN');
				if ($rsSGR->FeldInhalt('SICHTBAR_FILIALEN') != '1')
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_NSichtbar']);
				}
				else
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Sichtbar']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['BES_FILIALEN'] , ($DS%2));
				$IconsArray = '';
			}
			
			$Form->ZeileEnde();
			$DS++;
			$rsSGR->DSWeiter();
		}
		
		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsSGR->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './schadensgruende_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
		
		//$Link = './schadensgruende_Main.php?cmdAktion=Details';
		//$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	elseif(($rsSGR->AnzahlDatensaetze() == 1) or $AWIS_KEY1 == '-1')
	{
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./schadensgruende_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSGR->FeldInhalt('GRUND_USER'));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSGR->FeldInhalt('GRUND_USERDAT'));
			$Form->InfoZeile($Felder,'');

        
			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GRUND'] . ':',140);
		$Form->Erstelle_TextFeld('!BES_GRUND', isset($_POST['txtBES_GRUND'])?$_POST['txtBES_GRUND']:$rsSGR->FeldInhalt('GRUND'), 90, 90,$AWIS_KEY1 == '-1'?true:$EditRecht);
		$Form->ZeileEnde();
		
		$AWISCursorPosition = 'txtBES_GRUND';
			
		if (isset($_POST['txtBES_GRUND']))
		{
			if (isset($_POST['txtCHK_AKTIV']))
			{
				$CHK_AKT = 1;
			}
			else
			{
				$CHK_AKT = 0;
			}
			if (isset($_POST['txtCHK_FILIALEN']))
			{
				$CHK_FIL = 1;
			}
			else
			{
				$CHK_FIL = 0;
			}
		
		}
		else if($AWIS_KEY1 == '-1')
		{
			$CHK_AKT = 1;
			$CHK_FIL = 1;
		}
		else
		{
			$CHK_AKT =$rsSGR->FeldInhalt('VERALTET')==1?0:1;
			$CHK_FIL =$rsSGR->FeldInhalt('SICHTBAR_FILIALEN')==1?1:0;
		}
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVSUCH'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIV',$CHK_AKT,40,true,1);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SGR_AKTIVFIL'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_FILIALEN',$CHK_FIL,40,true,1);
		$Form->ZeileEnde();
		
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
    $Form->Formular_Ende();
	
    $Form->Erstelle_HiddenFeld('ID',$AWIS_KEY1);
    
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if(($Recht40013&2)==2 and ($rsSGR->AnzahlDatensaetze() == 1 or $AWIS_KEY1 == '-1'))//Speichern wenn Recht und 1(Detailansicht) oder kein (Neuanlage) Datensatz gefunden wurden
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	elseif(($Recht40013&4)==4)
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SCHAD_SGR',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	if(isset($Param['BES_GRUND']) AND $Param['BES_GRUND']!='')
	{
		$Bedingung .= 'AND UPPER(GRUND) ' . $DB->LikeOderIst($Param['BES_GRUND'], awisDatenbank::AWIS_LIKE_UPPER);
	}
	if(isset($Param['BES_VERALTET']) AND $Param['BES_VERALTET']!='')
	{
		$Bedingung .= 'AND VERALTET ' . $DB->LikeOderIst($Param['BES_VERALTET']) . ' ';
	}
	if(isset($Param['BES_FILIALEN']) AND $Param['BES_FILIALEN']!='')
	{
		if($Param['BES_FILIALEN'] == '0')
		{
			$Bedingung .= 'AND (SICHTBAR_FILIALEN ' . $DB->LikeOderIst($Param['BES_FILIALEN']) . ' or SICHTBAR_FILIALEN is null) ';
		}
		else 
		{
			$Bedingung .= 'AND SICHTBAR_FILIALEN ' . $DB->LikeOderIst($Param['BES_FILIALEN']) . ' ';
		}
	}
	
	return $Bedingung;
}

?>