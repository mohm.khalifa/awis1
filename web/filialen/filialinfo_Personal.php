<?php
// Code gepr�ft, Sacha Kerres, 22.07.2005
 global $SpeichernButton; // Anzeige eines Speichern-Knopfs
 global $con;
 global $awisRSZeilen;
 global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 115);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Filialen: Personal',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

 
$SQL = "SELECT * FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ";

$FILID=(isset($_REQUEST['FIL_ID'])?$_REQUEST['FIL_ID']:'');

// Alle Filialen anzeigen
If(!isset($_GET["Liste"]) AND $FILID == '')
{
	$foo = awis_BenutzerParameter($con, "_HILFSPARAM", strtoupper($AWISBenutzer->BenutzerName()));
	if(substr($foo,0,7)=="FIL_ID=")
	{
		$FILID=substr($foo,7,99);
	}
}

// Bedingung ausf�llen -> ID �bernehmen

if($FILID != '')
{
    $SQL .= " AND ( FIL_ID=0" . $FILID . ")";
}
else
{
 	$Params = explode(";", awis_BenutzerParameter($con, "FilialSuche", $AWISBenutzer->BenutzerName));
    if($Params[0] != '')
    {
        $Bedingung .= "AND FIL_ID " . awisLIKEoderIST($Params[0], 0);
    }
     if($Params[1] != '')
    {
    	$Bedingung .= "AND UPPER(FIL_BEZ) " . awisLIKEoderIST("%" . $Params[1] . "%", 1, 0, 1);
    }
     if($Params[2] != '')
    {
    	$Bedingung .= "AND UPPER(FIL_ORT) " . awisLIKEoderIST('%'.$Params[2].'%', 1, 0, 1);
    }
    if($Params[3] != '')
    {
        $Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($Params[3], 0);
	}
	if($Params[5]!='')
	{
		$Bedingung .= "AND FIL_ID IN (SELECT DISTINCT PER_FIL_ID FROM PERSONAL WHERE SUCHWORT(PER_NACHNAME) " . awisLIKEoderIST("%" . $Params[5]. "%",1,0,1) . ")";		
	}
    
    if($Bedingung != '')
    {
        $SQL .= " AND ( " . substr($Bedingung, 4) . ")";
    }
}

$rsFiliale = awisOpenRecordset($con, $SQL);
$rsFilialeAnz = $awisRSZeilen;

If($rsFilialeAnz > 1) 
{ // Liste gefunden -> Liste anzeigen
 	print "<table border=0 width=100%>";
    for($i = 0;$i < $rsFilialeAnz;$i++)
    {
         print "<tr><td><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Personal&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
         print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
         print "<td>" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td>" . $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td><tr>";
    }
    print "</table>";
}
elseIf($rsFilialeAnz == 0) 
{
	print "<p class=HinweisText>Es konnte kein Personal f�r die gesuchte Filiale gefunden werden</p>";
}
else 
{ // Eine Filiale

	// Aktuelle Filiale speichern
	awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "FIL_ID=" . $rsFiliale["FIL_ID"][0]);	
	     /**
         * Daten anzeigen
         */

	
	//Ausblenden von 2 Personen, da diese im Projekt neue Filialsysteme sind
	//Herr Freitag (PERNR 105356) und Herr Weber (PERNR 106873)
	//TR 31.01.08
	        
        // Daten f�r die Infoseite sammeln
	$SQL = "SELECT *
				FROM PERSONAL, PERSONALEINSATZPLANUNG, 
				(SELECT * FROM PERSONALTAETIGKEITEN WHERE PET_USER=(SELECT DISTINCT PER_USER FROM PERSONAL WHERE PER_FIL_ID=" . ($rsFiliale["FIL_ID"][0]==''?'0':$rsFiliale["FIL_ID"][0]) . "))
				 WHERE PER_Nr = PEP_PER_Nr AND PET_ID = PER_PET_ID
				  AND PEP_FIL_ID = " . ($rsFiliale["FIL_ID"][0]==''?'0':$rsFiliale["FIL_ID"][0]) . "
				  AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)
				  AND PER_NR <> 105356 AND PER_NR <> 106873
				  AND PEP_JAHR = " . date("Y",time()) . " AND PEP_KALENDERWOCHE = " . date("W",time());

	if(isset($_GET["Sortierung"]) AND $_GET["Sortierung"]=="Name")
	{
		$SQL .= " ORDER BY PER_NACHNAME, PER_VORNAME";
	}
	else
	{
		$SQL .= " ORDER BY PET_Sortierung, PER_NACHNAME, PER_VORNAME";
	}
	
	//awis_Debug(1,$SQL);
	
	$rsFilialPersonal = awisOpenRecordset($con, $SQL);
	$rsFilialPersonalAnz = $awisRSZeilen;

		// Liste der K�rzel mit Anzeigestufe
	$rsPersKuerzel = awisOpenRecordset($con, "SELECT * FROM PKUERZEL WHERE PEK_USER=(SELECT DISTINCT PER_USER FROM PERSONAL WHERE PER_FIL_ID=" . ($rsFiliale["FIL_ID"][0]==''?'0':$rsFiliale["FIL_ID"][0]) . ")");
	$rsPersKuerzelZeilen = $awisRSZeilen;
	for($i=0;$i<$rsPersKuerzelZeilen;$i++)
	{
		$rsPersKuerzel = array_merge($rsPersKuerzel, array($rsPersKuerzel["PEK_ID"][$i] => $rsPersKuerzel["PEK_ANZEIGESTUFE"][$i]));
	}
	
	$alternativ=0;
	
	if ($rsFilialPersonalAnz==0)		// Bei Ausland �ber die Sonderausweise
	{
		$alternativ=1;	
		
		$SQL="SELECT saw_name as per_nachname, saw_vorname as per_vorname, saw_taetigkeit as pet_taetigkeit from sonderausweise where saw_austritt is null and saw_fil_id=" . ($rsFiliale["FIL_ID"][0]==''?'0':$rsFiliale["FIL_ID"][0]);
		
		if(isset($_GET["Sortierung"]) AND $_GET["Sortierung"]=="Name")
		{
			$SQL .= " ORDER BY PER_NACHNAME, PER_VORNAME";
		}
		else
		{
			$SQL .= " ORDER BY PET_TAETIGKEIT";
		}

		$rsFilialPersonal = awisOpenRecordset($con, $SQL);
		$rsFilialPersonalAnz = $awisRSZeilen;
	}
	
     // �berschrift schreiben
	print "<table width=100% border=0><tr><td align=left>";
    print "<span class=Ueberschrift>Filiale " . $rsFiliale["FIL_ID"][0] . "</span>";

    print "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Personal&Liste=True';></td></tr></table>";
    print "<hr>";
    
	print "<table width=100%>";
	print "<tr><td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Personal&FIL_ID=" . $rsFiliale["FIL_ID"][0] . "&Sortierung=Name>Mitarbeiter</a></td>";
	print "<td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Personal&FIL_ID=" . $rsFiliale["FIL_ID"][0] . "&Sortierung=Taetigkeit>T�tigkeit</a></td><td id=FeldBez>Mo</td><td id=FeldBez>Di</td><td id=FeldBez>Mi</td><td id=FeldBez>Do</td><td id=FeldBez>Fr</td><td id=FeldBez>Sa</td><td id=FeldBez>So</td><tr>";		
	for($i=0;$i<$rsFilialPersonalAnz;$i++)
	{
		print "<tr>";
	
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialPersonal["PER_NACHNAME"][$i] . ", " . $rsFilialPersonal["PER_VORNAME"][$i] . "</td>";		
		if($alternativ==1)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awisDecrypt($con, $rsFilialPersonal["PET_TAETIGKEIT"][$i]) . "</td>";		
		}
		else
		{	 
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialPersonal["PET_TAETIGKEIT"][$i] . "</td>";		
		}
		
		//Wurde zum Test mal eingef�gt TR 290308
		/*
		if ($i==0)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">8.15 - 8.45 Uhr</td>";		
		}
		elseif ($i==1)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">8.00 - 8.30 Uhr</td>";		
		}
		elseif ($i==2)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">8.45 - 9.15 Uhr</td>";		
		}
		elseif ($i==3)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">9.15 - 9.45 Uhr</td>";		
		}
		elseif ($i==4)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">10.15 - 10.45 Uhr</td>";		
		}
		elseif ($i==5)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."></td>";		
		}
		elseif ($i==6)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">11.15 - 11.45 Uhr</td>";		
		}
		elseif ($i==7)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">9.30 - 10.00 Uhr</td>";		
		}
		elseif ($i==8)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">10.15 - 10.45 Uhr</td>";		
		}
		elseif ($i==9)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">12.15 - 12.45 Uhr</td>";		
		}
		elseif ($i==10)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">11.00 - 11.30 Uhr</td>";		
		}
		elseif ($i==11)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">11.45 - 12.15 Uhr</td>";		
		}
		elseif ($i==12)
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">13.15 - 13.45 Uhr</td>";		
		}
		else 
		{
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."></td>";		
		}
		*/
		
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (isset($rsFilialPersonal["PEP_PEK_ID_MONTAG"][$i])?(((isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MONTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MONTAG"][$i]]:0) & $RechteStufe)==(isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MONTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MONTAG"][$i]]:0)?$rsFilialPersonal["PEP_PEK_ID_MONTAG"][$i]:"--"):'--') . "</td>";		
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (isset($rsFilialPersonal["PEP_PEK_ID_DIENSTAG"][$i])?(((isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DIENSTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DIENSTAG"][$i]]:0) & $RechteStufe)==(isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DIENSTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DIENSTAG"][$i]]:0)?$rsFilialPersonal["PEP_PEK_ID_DIENSTAG"][$i]:"--"):'--') . "</td>";		
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (isset($rsFilialPersonal["PEP_PEK_ID_MITTWOCH"][$i])?(((isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MITTWOCH"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MITTWOCH"][$i]]:0) & $RechteStufe)==(isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MITTWOCH"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_MITTWOCH"][$i]]:0)?$rsFilialPersonal["PEP_PEK_ID_MITTWOCH"][$i]:"--"):'--') . "</td>";		
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (isset($rsFilialPersonal["PEP_PEK_ID_DONNERSTAG"][$i])?(((isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DONNERSTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DONNERSTAG"][$i]]:0) & $RechteStufe)==(isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DONNERSTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_DONNERSTAG"][$i]]:0)?$rsFilialPersonal["PEP_PEK_ID_DONNERSTAG"][$i]:"--"):'--') . "</td>";		
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (isset($rsFilialPersonal["PEP_PEK_ID_FREITAG"][$i])?(((isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_FREITAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_FREITAG"][$i]]:0) & $RechteStufe)==(isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_FREITAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_FREITAG"][$i]]:0)?$rsFilialPersonal["PEP_PEK_ID_FREITAG"][$i]:"--"):'--') . "</td>";		
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (isset($rsFilialPersonal["PEP_PEK_ID_SAMSTAG"][$i])?(((isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SAMSTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SAMSTAG"][$i]]:0) & $RechteStufe)==(isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SAMSTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SAMSTAG"][$i]]:0)?$rsFilialPersonal["PEP_PEK_ID_SAMSTAG"][$i]:"--"):'--') . "</td>";		
		print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (isset($rsFilialPersonal["PEP_PEK_ID_SONNTAG"][$i])?(((isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SONNTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SONNTAG"][$i]]:0) & $RechteStufe)==(isset($rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SONNTAG"][$i]])?$rsPersKuerzel[$rsFilialPersonal["PEP_PEK_ID_SONNTAG"][$i]]:0)?$rsFilialPersonal["PEP_PEK_ID_SONNTAG"][$i]:"--"):'--') . "</td>";		
		print "</tr>";
	}
    print "</table>";
    
	print "</tr></table>";


}	// Auswahl oder Details

?>