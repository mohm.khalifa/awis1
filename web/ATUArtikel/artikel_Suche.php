<?
// Konstanten

$sk_BreiteErsteSpalte = 250;		// Breite f�r die erste Splate in der Suchmaske

// Variablen
global $awisRSZeilen;
global $awisDBFehler;
global $con;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 400);
if($RechteStufe==0)
{
	awisEreignis(3,1000,'ATU-Artikel: Suchen',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
	die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$BearbeitungsStufe = awisBenutzerRecht($con, 401);
// lokale Variablen

// Ende Variablen

// Auswahl zur�cksetzen

if(isset($_GET["Reset"]) AND $_GET["Reset"]=='True')
{
//TODO: Haken speichern
	$Param =explode(";",awis_BenutzerParameter($con, "ArtikelSuche", $AWISBenutzer->BenutzerName()));
	$Param[1]='';
	$Param[2]='';
	$Param[3]='';
	$Param[10]='';
	awis_BenutzerParameterSpeichern($con,"ArtikelSuche",$AWISBenutzer->BenutzerName(),implode(";",$Param));
}

echo "<form name=frmSuche method=post action=./artikel_Main.php>";//?cmdAktion=ArtikelInfos>";

echo "<table id=SuchMaske>";

$ArtSuche = explode(";",awis_BenutzerParameter($con, "ArtikelSuche", $AWISBenutzer->BenutzerName()));

// ATU Nummer
echo "<tr><td width=$sk_BreiteErsteSpalte>ATU Artikel-<u>N</u>ummer</td><td><input name=txtAST_ATUNR accesskey=n tabindex=1 size=10 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[1]:'') . "\" onchange='ClearFeld(2);'></td></tr>";
// Artikelbezeichnung
echo "<tr><td width=$sk_BreiteErsteSpalte>Artikel-Bezei<u>c</u>hnung</td><td><input name=txtAST_BEZEICHNUNG accesskey=c tabindex=2 size=30 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[2]:'') . "\"></td></tr>";
echo "<tr><td width=$sk_BreiteErsteSpalte>Kommentar</td><td><input name=txtKommentar tabindex=3 size=30 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[10]:'') . "\"></td></tr>";


// Warengruppe
echo '<tr><td>Warengruppe</td><td><select name=txtWGR_KEY>';
echo '<option value=-1>Bitte w�hlen...</option>';
$rsWGR = awisOpenRecordset($con, 'SELECT * FROM WARENGRUPPEN ORDER BY WGR_Bezeichnung');
$rsWGRZeilen = $awisRSZeilen;
for($WGRZeile=0;$WGRZeile<$rsWGRZeilen;$WGRZeile++)
{
	echo '<option ';
	if($ArtSuche[0]=='on')
	{
		if($rsWGR['WGR_ID'][$WGRZeile]==$ArtSuche[13])
		{
			echo ' selected ';
		}
	}
	echo ' value=' . $rsWGR['WGR_ID'][$WGRZeile] . '>' . $rsWGR['WGR_BEZEICHNUNG'][$WGRZeile] . '</option>';
}
echo '</select>';
unset($rsWGR);


// Warenuntergruppen
$rsWUG = awisOpenRecordset($con, 'SELECT * FROM WARENGRUPPEN INNER JOIN WARENUNTERGRUPPEN ON WGR_ID = WUG_WGR_ID WHERE WUG_KEY > 0 ORDER BY WGR_Bezeichnung, WUG_Bezeichnung');
$rsWUGZeilen = $awisRSZeilen;

echo '<tr><td>Warenuntergruppe</td><td><select name=txtWUG_KEY>';
echo '<option value=0>Bitte w�hlen...</option>';

$LetzteGrp = '';
for($WUGZeile=0;$WUGZeile<$rsWUGZeilen;$WUGZeile++)
{
	if($LetzteGrp!=$rsWUG['WGR_BEZEICHNUNG'][$WUGZeile])
	{
		if($LetzteGrp=='')
		{
			echo '</optgroup>';
		}
		echo "<optgroup label='" . $rsWUG['WGR_BEZEICHNUNG'][$WUGZeile]. "'>";
		$LetzteGrp=$rsWUG['WGR_BEZEICHNUNG'][$WUGZeile];
	}
	echo '<option ';
	if($ArtSuche[0]=='on')
	{
		if($rsWUG['WUG_KEY'][$WUGZeile]==$ArtSuche[12])
		{
			echo ' selected ';
		}
	}
	echo ' value=' . $rsWUG['WUG_KEY'][$WUGZeile] . '>' . $rsWUG['WUG_BEZEICHNUNG'][$WUGZeile] . '</option>';
}
echo '</optgroup>';
echo '</select></td></tr>';

unset($rsWUG);


// Auswahl Speichern
echo "<tr><td colspan=2><hr></td></tr>";

echo "<tr><td width=$sk_BreiteErsteSpalte>Zu suchende N<u>u</u>mmer</td><td><input name=txtTEI_SUCH accesskey=u tabindex=10 size=30 value=\"" . ($ArtSuche[0]=='on'?$ArtSuche[3]:'') . "\" onchange='ClearFeld(1);';>";
echo "<input type=checkbox name=txtTEI_SUCH_Exakt accesskey=� tabindex=20 value='on'  " . ($ArtSuche[0]=='on'?($ArtSuche[7]=='on'?'checked':''):'') . "> exakte <u>�</u>bereinstimmung";

echo "</td></tr>";
echo "<tr><td colspan=2>Nummer ist eine</td></tr>";

echo "<tr><td width=$sk_BreiteErsteSpalte>�<u>E</u>AN-Nummer</td><td><input type=checkbox name=txtTEI_SUCH_EAN accesskey=e tabindex=20 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[4]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
echo "<tr><td width=$sk_BreiteErsteSpalte>�<u>G</u>ebrauchsnummer</td><td><input type=checkbox name=txtTEI_SUCH_GNR accesskey=g tabindex=21 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[5]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
echo "<tr><td width=$sk_BreiteErsteSpalte>�<u>L</u>ieferanten-Artikel-Nummer</td><td><input type=checkbox name=txtTEI_SUCH_LAR accesskey=l tabindex=22 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[6]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
echo "<tr><td width=$sk_BreiteErsteSpalte>�Lieferan<u>t</u>en-Set</td><td><input type=checkbox name=txtTEI_SUCH_LAS accesskey=t tabindex=23 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[8]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
echo "<tr><td width=$sk_BreiteErsteSpalte>�OE Nu<u>m</u>mer</td><td><input type=checkbox name=txtTEI_SUCH_OEN accesskey=m tabindex=24 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[9]=='on'?'checked':''):'') . " onclick=BeliebigeNummerAus();></td></tr>";
// Sondersuche: Sucht nach einer beliebigen Nummer
echo "<tr><td width=$sk_BreiteErsteSpalte>�Bel<u>i</u>ebige Nummer</td><td><input type=checkbox name=txtTEI_SUCH_XXX accesskey=m tabindex=25 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[11]=='on'?'checked':''):'') . " onclick=\"SetzeBeliebigeNummer();\"></td></tr>";

if(($OEPRechte = awisBenutzerRecht($con, 403)&4)==4)
{
	echo "<tr><td colspan=2><hr></td></tr>";
	echo "<tr><td width=$sk_BreiteErsteSpalte><span title='Zeigt immer die OE Preise in der �bersicht an'>OE-<u>P</u>reise immer zeigen</span></td><td><input type=checkbox name=txtOEPreise accesskey=p tabindex=30 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[14]=='on'?'checked':''):'') . "></td></tr>";
}

// Fremdkauf in der �bersicht anzeigen
echo "<tr><td width=$sk_BreiteErsteSpalte><span title='Zeigt die Fremdk�ufe bei der Artikel�bersicht an'>Fremdkaufanzeige</span></td><td><input type=checkbox name=txtFKAnzeige accesskey=k tabindex=31 value='on' " . ($ArtSuche[0]=='on'?($ArtSuche[15]=='on'?'checked':''):'') . "></td></tr>";


echo "<tr><td colspan=2><hr></td></tr>";
echo "<tr><td width=$sk_BreiteErsteSpalte>Auswahl <u>s</u>peichern:</td>";
echo "<td><input type=checkbox value=on " . ($ArtSuche[0]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=55></td></tr>";

echo "<tr><td>Anzuzeigendes Reg<u>i</u>ster:</td>";

echo "<td><select tabindex=56 accesskey=i name=cmdAktion>";
echo "<option selected Value=ArtikelInfos>Artikelinformationen</option>";
echo "<option Value=EANArtikel>EAN Artikel</option>";
echo "<option Value=OENummern>OE Nummern</option>";
echo "<option Value=Lieferantenartikel>Lieferantenartikel</option>";

echo "</select></td></tr>";

echo '</tr><td>Maximale Datensatzanzahl:</td>';
echo '<td><a href=/BenutzerParameter.php>' . awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName()) . '</a> pro Bereich</td>';
echo "</tr>";

echo "</table>";

echo "<hr>";
echo "<br>&nbsp;<input accesskey=w tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten (Alt+w)' name=cmdSuche value=Aktualisieren".awis_StatusZeile('Suche starte (Alt+w).','').">";
echo "&nbsp;<a border=0 title='Formular l�schen (Alt+r)' accesskey=r href=./artikel_Main.php?Reset=True><img border=0 tabindex=99 src=/bilder/radierer.png alt='Formularinhalt l�schen (Alt+r)' name=cmdReset ".awis_StatusZeile('Formularinhalt l�schen (Alt+r).','')."></a>";
if(($BearbeitungsStufe&4)==4)		// Neuen Artikel hinzuf�gen
{
	echo "&nbsp;<a title='Neuer Artikel (Alt+n)' accesskey=n href=./artikel_Main.php?cmdAktion=ArtikelInfos&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'".awis_StatusZeile('Neuen Artikel hinzuf�gen (Alt+n).','')."></a>";
}

echo "</form>";
// Cursor in das erste Feld setzen
echo "<Script Language=JavaScript>";
echo "document.getElementsByName(\"txtAST_ATUNR\")[0].focus();";

echo 'function SetzeBeliebigeNummer()';
echo '{';
echo "  if(document.getElementsByName(\"txtTEI_SUCH_XXX\")[0].checked==true)";
echo '{';
echo "    document.getElementsByName(\"txtTEI_SUCH_EAN\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_GNR\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_LAR\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_LAS\")[0].checked=false;";
echo "    document.getElementsByName(\"txtTEI_SUCH_OEN\")[0].checked=false;";
echo "    document.getElementsByName(\"cmdAktion\")[0].value=\"ArtikelInfos\";";
echo '}';
echo "  return(0)";
echo '}';

echo 'function BeliebigeNummerAus()';
echo '{';
echo "  document.getElementsByName(\"txtTEI_SUCH_XXX\")[0].checked=false;";
echo "  return(0)";
echo '}';

echo 'function ClearFeld(Feld)';
echo '{';
echo 'if (Feld == 2) {';
echo "    document.getElementsByName(\"txtTEI_SUCH\")[0].value='';";
echo '} else {';
echo "    document.getElementsByName(\"txtAST_ATUNR\")[0].value='';";
echo '}';
echo '  return(0)';
echo '}';

echo "</Script>";

?>