<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
global $EditModus;
global $DetailAnsicht;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('APR', '%');
$TextKonserven[] = array('LIE', 'LIE_NAME1');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

try {
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht407 = $AWISBenutzer->HatDasRecht(407);        // Recht f�r Lieferantenartikel
    if ($Recht407 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $BindeVariablen = array();
    $BindeVariablen['var_N0_ast_key'] = '0' . $AWIS_KEY1;

    $SQL = 'SELECT AST_ATUNR FROM Artikelstamm WHERE AST_KEY = :var_N0_ast_key';
    $rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
    $AST_ATUNR = $rsAST->FeldInhalt('AST_ATUNR');

    $Bedingung = ' AND APR_AST_ATUNR = \'' . $AST_ATUNR . '\'';

    $SQL = 'SELECT ARTIKELPRUEFUNGEN.*, LIE_NAME1, LAR_LARTNR';
    $SQL .= ' FROM ARTIKELPRUEFUNGEN ';
    $SQL .= ' LEFT OUTER JOIN Lieferanten ON APR_LIE_NR = LIE_NR';
    $SQL .= ' LEFT OUTER JOIN Lieferantenartikel ON LAR_KEY = APR_LAR_KEY';

    if (isset($_GET['APR_KEY'])) {
        $AWIS_KEY2 = $DB->FeldInhaltFormat('Z', $_GET['APR_KEY'], false);
        $Bedingung .= ' AND APR_KEY = ' . $AWIS_KEY2;
    }
    if (isset($_POST['txtAPRKey'])) {
        $AWIS_KEY2 = $DB->FeldInhaltFormat('Z', $_POST['txtAPRKey'], false);
        $Bedingung .= ' AND APR_KEY = ' . $AWIS_KEY2;
    }
    if ($AWIS_KEY2 != 0) {
        $Bedingung .= ' AND APR_KEY = 0' . $AWIS_KEY2;
    }

    $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

    if (isset($_GET['APRSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['APRSort']);
    } else {
        $SQL .= ' ORDER BY APR_BEARBEITUNGSTAG';
    }

    $rsAPR = $DB->RecordSetOeffnen($SQL);

    if (($rsAPR->AnzahlDatensaetze() > 1 or isset($_GET['APRListe'])) OR ($rsAPR->EOF() AND $AWIS_KEY2 == 0))                    // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();

        if (($Recht407 & 1024)) {
            $Icons[] = array('new', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen&APR_KEY=-1', 'g', $AWISSprachKonserven['Wort']['lbl_hinzufuegen']);
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen';
        $Link .= '&APRSort=APR_BEARBEITUNGSTAG' . ((isset($_GET['APRSort']) AND ($_GET['APRSort'] == 'APR_BEARBEITUNGSTAG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_BEARBEITUNGSTAG'], 100, '', $Link);

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen';
        $Link .= '&APRSort=APR_LAR_LARTNR' . ((isset($_GET['APRSort']) AND ($_GET['APRSort'] == 'APR_LAR_LARTNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_LAR_LARTNR'], 190, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen';
        $Link .= '&APRSort=LIE_NAME1' . ((isset($_GET['APRSort']) AND ($_GET['APRSort'] == 'LIE_NAME1'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'], 250, '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_LIE_NR'], 100);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_FEHLERBESCHREIBUNG'], 500);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsAPR->EOF()) {
            $Form->ZeileStart();

            $Icons = array();
            if (intval($Recht407 & 2) > 0)    // �ndernrecht (Details anzeigen)
            {
                $Icons[] = array('edit', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen&APR_KEY=' . $rsAPR->FeldInhalt('APR_KEY'));
            }
            if (intval($Recht407 & 512) > 0)    // L�schen
            {
                $Icons[] = array('delete', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen&Del=' . $rsAPR->FeldInhalt('APR_KEY') . '&Key=' . $AWIS_KEY1);
            }
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));

            $Form->Erstelle_ListenFeld('#APR_BEARBEITUNGSTAG', $rsAPR->FeldInhalt('APR_BEARBEITUNGSTAG'), 10, 100, false, ($DS % 2), '', '', 'D');
            $Form->Erstelle_ListenFeld('#APR_LAR_LARTNR', ($rsAPR->FeldInhalt('APR_LAR_KEY') == ''?$rsAPR->FeldInhalt('APR_LAR_LARTNR'):$rsAPR->FeldInhalt('LAR_LARTNR')), 10, 190,
                false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#LIE_NAME1', $rsAPR->FeldInhalt('LIE_NAME1'), 10, 250, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#APR_LIE_NR', $rsAPR->FeldInhalt('APR_LIE_NR'), 10, 100, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#APR_FEHLERBESCHREIBUNG', (strlen($rsAPR->FeldInhalt('APR_FEHLERBESCHREIBUNG')) > 60?substr($rsAPR->FeldInhalt('APR_FEHLERBESCHREIBUNG'), 0,
                    57) . '...':$rsAPR->FeldInhalt('APR_FEHLERBESCHREIBUNG')), 10, 500, false, ($DS % 2), '', '', 'T', '', $rsAPR->FeldInhalt('APR_FEHLERBESCHREIBUNG'));

            $Form->ZeileEnde();

            $rsAPR->DSWeiter();
            $DS++;
        }
        $Form->Formular_Ende();
    } else        // Einer oder keiner
    {
        $Form->Formular_Start();

        echo '<input name=txtAPR_KEY type=hidden value=0' . ($rsAPR->FeldInhalt('APR_KEY') != ''?$rsAPR->FeldInhalt('APR_KEY'):'') . '>';
        // Importquelle muss bei der �nderung unbedingt eine 4 bekommen!

        $AWIS_KEY2 = $rsAPR->FeldInhalt('APR_KEY');
        $AWISBenutzer->ParameterSchreiben('AktuellerAPR', $AWIS_KEY2);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Pruefungen&APRListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAPR->FeldInhalt('APR_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAPR->FeldInhalt('APR_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditModus = ($Recht407 & 6);
        $EditModus2 = ($Recht407 & 64);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_LAR_LARTNR'] . ':', 150);
        $AWISCursorPosition = ($EditModus2?'txtAPR_LAR_LARTNR':$AWISCursorPosition);
        $Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . ($rsAPR->FeldInhalt('APR_LIE_NR'));
        $SQL = "SELECT DISTINCT LAR_KEY, LAR_LARTNR || ' (' || LIE_NR || ' - ' || LIE_NAME1 || ')' AS anzeige ";
        $SQL .= " FROM AWIS.LieferantenArtikel INNER JOIN AWIS.Lieferanten ON LAR_LIE_NR = LIE_NR";
        $SQL .= " INNER JOIN AWIS.TeileInfos ON LAR_KEY = TEI_KEY2 WHERE TEI_KEY1='" . $AWIS_KEY1 . "'";
        $SQL .= ' ORDER BY 2';
        $Form->Erstelle_SelectFeld('APR_LAR_KEY', $rsAPR->FeldInhalt('APR_LAR_KEY'), 0, $EditModus2, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        if ($rsAPR->FeldInhalt('APR_KEY') != '' AND $rsAPR->FeldInhalt('APR_LAR_KEY') == '') {
            $Form->Erstelle_TextFeld('APR_LAR_LARTNR', ' (' . $rsAPR->FeldInhalt('APR_LAR_LARTNR') . ')', 0, 200, false, '', '', '', 'T');
        }
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_FEHLERBESCHREIBUNG'] . ':', 150);
        $Form->Erstelle_Textarea('APR_FEHLERBESCHREIBUNG', $rsAPR->FeldInhalt('APR_FEHLERBESCHREIBUNG'), 700, 120, 4, ($EditModus2));
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['Zentrum_1'] . '', 400, 'Ueberschrift');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['Zentrum_2'] . '', 400, 'Ueberschrift');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['Filialbestaende'] . '', 350, 'Ueberschrift');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Bestand = 0;
        if ($rsAPR->FeldInhalt('APR_KEY') == '') {
            $BindeVariablen = array();
            $BindeVariablen['var_T_asi_ast_atunr'] = $AST_ATUNR;
            $BindeVariablen['var_N0_asi_ait_id'] = 10;
            $rsASI = $DB->RecordSetOeffnen("SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AST_ATUNR=:var_T_asi_ast_atunr AND ASI_AIT_ID=:var_N0_asi_ait_id", $BindeVariablen);
            $Bestand = $rsASI->FeldInhalt('ASI_WERT');            // Wert aus dem aktuellen Bestand
        }
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_MENGE_N'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_MENGE_N', $rsAPR->FeldInhalt('APR_MENGE_N'), 2, 300, ($Recht407 & 4), '', '', '', 'N0', 'L', '', $Bestand);
        $Bestand = 0;
        if ($rsAPR->FeldInhalt('APR_KEY') == '') {
            $BindeVariablen = array();
            $BindeVariablen['var_T_asi_ast_atunr'] = $AST_ATUNR;
            $BindeVariablen['var_N0_asi_ait_id'] = 11;
            $rsASI = $DB->RecordSetOeffnen("SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AST_ATUNR=:var_T_asi_ast_atunr AND ASI_AIT_ID=:var_N0_asi_ait_id", $BindeVariablen);
            $Bestand = $rsASI->FeldInhalt('ASI_WERT');            // Wert aus dem aktuellen Bestand
        }
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_MENGE_L'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_MENGE_L', $rsAPR->FeldInhalt('APR_MENGE_L'), 2, 300, ($Recht407 & 8), '', '', '', 'N0', 'L', '', $Bestand);
        if ($rsAPR->FeldInhalt('APR_KEY') == '') {
            $BindeVariablen = array();
            $BindeVariablen['var_T_asi_ast_atunr'] = $AST_ATUNR;
            $BindeVariablen['var_N0_asi_ait_id'] = 12;
            $rsASI = $DB->RecordSetOeffnen("SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AST_ATUNR=:var_T_asi_ast_atunr AND ASI_AIT_ID=:var_N0_asi_ait_id", $BindeVariablen);
            $Bestand = $rsASI->FeldInhalt('ASI_WERT');            // Wert aus dem aktuellen Bestand
        }
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_FILIALBESTAND'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_FILIALBESTAND', $rsAPR->FeldInhalt('APR_FILIALBESTAND'), 2, 250, ($Recht407 & 16), '', '', '', 'N0', 'L', '', $Bestand);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_PRUEFER_N'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_PRUEFER_N', $rsAPR->FeldInhalt('APR_PRUEFER_N'), 30, 300, ($Recht407 & 4), '', '', '', 'T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_PRUEFER_L'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_PRUEFER_L', $rsAPR->FeldInhalt('APR_PRUEFER_L'), 30, 300, ($Recht407 & 8), '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_DATUM_N'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_DATUM_N', $rsAPR->FeldInhalt('APR_DATUM_N'), 10, 300, ($Recht407 & 4), '', '', '', 'D');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_DATUM_L'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_DATUM_L', $rsAPR->FeldInhalt('APR_DATUM_L'), 10, 300, ($Recht407 & 8), '', '', '', 'D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_ERGEBNIS_N'] . ':', 100);
        $Form->Erstelle_Textarea('APR_ERGEBNIS_N', $rsAPR->FeldInhalt('APR_ERGEBNIS_N'), 300, 30, 2, ($Recht407 & 4), '', '', '', 'N0');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_ERGEBNIS_L'] . ':', 100);
        $Form->Erstelle_Textarea('APR_ERGEBNIS_L', $rsAPR->FeldInhalt('APR_ERGEBNIS_L'), 300, 30, 2, ($Recht407 & 8), '', '', '', 'N0');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_MASSNAHME_N'] . ':', 100);
        $Form->Erstelle_Textarea('APR_MASSNAHME_N', $rsAPR->FeldInhalt('APR_MASSNAHME_N'), 300, 30, 2, ($Recht407 & 128), '', '', '', 'N0');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_MASSNAHME_L'] . ':', 100);
        $Form->Erstelle_Textarea('APR_MASSNAHME_L', $rsAPR->FeldInhalt('APR_MASSNAHME_L'), 300, 30, 2, ($Recht407 & 256), '', '', '', 'N0');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_MASSNAHME_FILIALEN'] . ':', 100);
        $Form->Erstelle_Textarea('APR_MASSNAHME_FILIALEN', $rsAPR->FeldInhalt('APR_MASSNAHME_FILIALEN'), 250, 30, 2, ($Recht407 & 16), '', '', '', 'N0');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Trennzeile('L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_BEARBEITER'] . ':', 160);
        $Form->Erstelle_TextFeld('APR_BEARBEITER', $rsAPR->FeldInhalt('APR_BEARBEITER'), 30, 240, false, '', '', '', 'T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_BEARBEITUNGSTAG'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_BEARBEITUNGSTAG', $rsAPR->FeldInhalt('APR_BEARBEITUNGSTAG'), 30, 300, false, '', '', '', 'D');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_MELDUNGDURCH'] . ':', 103);
        $Form->Erstelle_TextFeld('APR_MELDUNGDURCH', $rsAPR->FeldInhalt('APR_MELDUNGDURCH'), 30, 300, ($Recht407 & 32), '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_LIEFINFORMIERT'] . ':', 160);
        $Form->Erstelle_TextFeld('APR_LIEFINFORMIERT', $rsAPR->FeldInhalt('APR_LIEFINFORMIERT'), 30, 240, ($Recht407 & 64), '', '', '', 'T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_LIEFINFORMIERTAM'] . ':', 100);
        $Form->Erstelle_TextFeld('APR_LIEFINFORMIERTAM', $rsAPR->FeldInhalt('APR_LIEFINFORMIERTAM'), 10, 140, ($Recht407 & 64), '', '', '', 'D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_MASSNAHME'] . ':', 160);
        $Form->Erstelle_TextFeld('APR_MASSNAHME', $rsAPR->FeldInhalt('APR_MASSNAHME'), 100, 600, ($Recht407 & 64), '', '', '', 'T');
        $Form->ZeileEnde();

        //**********************************************************************
        //* Benachrichtungen f�r Abteilungen
        //**********************************************************************

        $BindeVariablen = array();
        $BindeVariablen['var_N0_apm_apr_key'] = '0' . $rsAPR->FeldInhalt('APR_KEY');

        $SQL = 'SELECT * FROM ArtikelPruefungenMeldungen ';
        $SQL .= ' WHERE APM_APR_KEY=:var_N0_apm_apr_key';

        $rsAPM = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
        $Benachrichtigungen = explode(";", $rsAPM->FeldInhalt('APM_BENACHRICHTIGUNGEN'));

        $SQL = 'SELECT AIS_KEY, AIS_Bezeichnung, AIS_Status FROM ArtikelPruefungenInfoStellen';
        $rsAIS = $DB->RecordSetOeffnen($SQL);

        $DS = 0;
        while (!$rsAIS->EOF()) {
            if (($DS % 4) == 0) {
                $Form->ZeileStart();
            }

            $Form->Erstelle_Checkbox('AIS_' . $rsAIS->FeldInhalt('AIS_KEY'), (array_search($rsAIS->FeldInhalt('AIS_KEY'), $Benachrichtigungen) === false?'':'on'), 40,
                ($Recht407 & 32), 'on');
            $Form->Erstelle_TextLabel($rsAIS->FeldInhalt('AIS_BEZEICHNUNG'), 250);

            if (((++$DS) % 4) == 0) {
                $Form->ZeileEnde();
            }
            $rsAIS->DSWeiter();
        }

        $Form->Trennzeile('L');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['APR']['APR_WEITERGELEITETAM'] . ':', 190);
        $Form->Erstelle_TextFeld('APR_WEITERGELEITETAM', $rsAPR->FeldInhalt('APR_WEITERGELEITETAM'), 10, 140, ($Recht407 & 32), '', '', '', 'D');
        $Form->ZeileEnde();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060002");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "20081��61223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>