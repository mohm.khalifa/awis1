<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUR','%');
	$TextKonserven[]=array('ZUB','ZUB_EK');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht10003 = $AWISBenutzer->HatDasRecht(10003);
	if($Recht10003==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZUR_KEY';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
	}


	// Daten ermitteln
	$SQL = 'SELECT zukaufrechnungen.*';
	$SQL .= ' , ZUR_EKPR * (1-(ZUR_RABATT1/100.0)) * (1-(ZUR_RABATT2/100.0)) * (1-(ZUR_RABATT3/100.0)) AS EK';
	$SQL .= ' , ZAL_BEZEICHNUNG';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM zukaufrechnungen ';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAL_KEY = ZUR_ZAL_KEY';
	$SQL .= ' WHERE ZUR_ZUL_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);

	$AWIS_KEY2=0;
	if(isset($_GET['ZUR_KEY']))
	{
		$AWIS_KEY2=$DB->FeldInhaltFormat('N0',$_GET['ZUR_KEY']);
		$SQL .= ' AND ZUR_KEY = '.$AWIS_KEY2;
	}

	$SQL .= $ORDERBY;


	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUR',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$rsZUR = $DB->RecordSetOeffnen($SQL);

	if($rsZUR->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0)						// Liste anzeigen
	{
		$Form->Formular_Start();

		$Form->ZeileStart();

		if((intval($Recht10003)&4)!=0)
		{
			$Icons = array();
			$Form->Erstelle_ListeIcons($Icons,58,-1);
  		}

		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUR_DATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUR_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_DATUM'],130,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUR_EXTERNEID'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUR_EXTERNEID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_EXTERNEID'],250,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUR_LIEFERSCHEINNUMMER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUR_LIEFERSCHEINNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_LIEFERSCHEINNUMMER'],150,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUR_MENGE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUR_MENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_MENGE'],150,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUR_VKPR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUR_VKPR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_VKPR'],150,'',$Link);
		if(($Recht10003&32)==32)
		{
			$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUR_EKPR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUR_EKPR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_EK'],150,'',$Link);
		}

		$Form->ZeileEnde();

		$PEBZeile=0;
		$DS=0;
		while(!$rsZUR->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht10003&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','../zukaufrechnungen/zukaufrechnungen_Main.php?cmdAktion=Details&ZUR_KEY='.$rsZUR->FeldInhalt('ZUR_KEY'));
			}
			if($rsZUR->FeldInhalt('ZUR_EMAIL')!='')	// EMail vorhanden?
			{
				$Icons[] = array('mail','mailto:'.$rsZUR->FeldInhalt('ZUR_EMAIL'));
			}
			$Form->Erstelle_ListeIcons($Icons,58,($DS%2));

			$Form->Erstelle_ListenFeld('#ZUR_DATUM',$rsZUR->FeldInhalt('ZUR_DATUM'),0,130,false,($PEBZeile%2),'','','D');
			$Link = '../zukaufrechnungen/zukaufrechnungen_Main.php?cmdAktion=Details&ZUR_KEY='.$rsZUR->FeldInhalt('ZUR_KEY');
			$Form->Erstelle_ListenFeld('#ZUR_EXTERNEID',$rsZUR->FeldInhalt('ZUR_EXTERNEID'),0,250,false,($PEBZeile%2),'','','T','','');
			$Form->Erstelle_ListenFeld('#ZUR_LIEFERSCHEINNUMMER',$rsZUR->FeldInhalt('ZUR_LIEFERSCHEINNUMMER'),0,150,false,($PEBZeile%2),'','','T','','');
			$Form->Erstelle_ListenFeld('#ZUR_MENGE',$rsZUR->FeldInhalt('ZUR_MENGE'),0,150,false,($PEBZeile%2),'','','Nx','','');
			$Form->Erstelle_ListenFeld('#ZUR_VKPR',$rsZUR->FeldInhalt('ZUR_VKPR'),0,150,false,($PEBZeile%2),'','','N2');
			if(($Recht10003&32)==32)
			{
				$Form->Erstelle_ListenFeld('#ZUR_EK',$rsZUR->FeldInhalt('EK'),0,150,false,($PEBZeile%2),'','','N2');
			}
			$Form->ZeileEnde();

			$rsZUR->DSWeiter();
			$PEBZeile++;
		}

		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&ZUB_KEY='.$AWIS_KEY1.''.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();

		$AWIS_KEY2 = $rsZUR->FeldInhalt('ZUR_KEY');

		$Form->Erstelle_HiddenFeld('ZUR_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('ZUR_ZUB_KEY',$AWIS_KEY1);

					// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./zukauflieferscheine_Main.php?cmdAktion=Details&ZURListe=1&Seite=Lieferscheine accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUR->FeldInhalt('ZUR_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUR->FeldInhalt('ZUR_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10003&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_DATUM'].':',150);
		$Form->Erstelle_TextFeld('ZUR_DATUM',$rsZUR->FeldInhalt('ZUR_DATUM'),10,300,$EditRecht,'','','','D');
		$AWISCursorPosition='txtZUR_DATUM';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EXTERNEID'].':',150);
		$Form->Erstelle_TextFeld('ZUR_EXTERNEID',$rsZUR->FeldInhalt('ZUR_EXTERNEID'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_BESTELLID'].':',150);
		$Form->Erstelle_TextFeld('ZUR_BESTELLID',$rsZUR->FeldInhalt('ZUR_BESTELLID'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_VKPR'].':',150);
		$Form->Erstelle_TextFeld('ZUR_VKPR',$rsZUR->FeldInhalt('ZUR_VKPR'),10,300,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WANR'].':',150);
		$Form->Erstelle_TextFeld('ZUR_WANR',$rsZUR->FeldInhalt('ZUR_WANR'),12,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARTIKELNUMMER'].':',150);
		$Form->Erstelle_TextFeld('ZUR_ARTIKELNUMMER',$rsZUR->FeldInhalt('ZUR_ARTIKELNUMMER'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARTIKELBEZEICHNUNG'].':',150);
		$Form->Erstelle_TextFeld('ZUR_ARTIKELBEZEICHNUNG',$rsZUR->FeldInhalt('ZUR_ARTIKELBEZEICHNUNG'),50,900,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_HERSTELLER'].':',150);
		$Form->Erstelle_TextFeld('ZUR_HERSTELLER',$rsZUR->FeldInhalt('ZUR_HERSTELLER'),10,150,$EditRecht,'','','','T');
		$Form->ZeileEnde();




		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_MENGE'].':',150);
		$Form->Erstelle_TextFeld('ZUR_MENGE',$rsZUR->FeldInhalt('ZUR_MENGE'),10,150,$EditRecht,'','','','N3');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EINHEITENPROPREIS'].':',150);
		$Form->Erstelle_TextFeld('ZUR_EINHEITENPROPREIS',$rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS'),10,150,$EditRecht,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WAEHRUNG'].':',150);
		$Form->Erstelle_TextFeld('ZUR_WAEHRUNG',$rsZUR->FeldInhalt('ZUR_WAEHRUNG'),3,150,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_VKPR'].':',150);
		$Form->Erstelle_TextFeld('ZUR_VKPR',$rsZUR->FeldInhalt('ZUR_VKPR'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EKPR'].':',150);
		$Form->Erstelle_TextFeld('ZUR_EKPR',$rsZUR->FeldInhalt('ZUR_EKPR'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RABATT1'].':',150);
		$Form->Erstelle_TextFeld('ZUR_RABATT1',$rsZUR->FeldInhalt('ZUR_RABATT1'),10,150,$EditRecht,'','','','N4');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RABATT2'].':',150);
		$Form->Erstelle_TextFeld('ZUR_RABATT2',$rsZUR->FeldInhalt('ZUR_RABATT2'),10,150,$EditRecht,'','','','N4');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_VK'].':',150);
		$Form->Erstelle_TextFeld('ZUR_VK',$rsZUR->FeldInhalt('ZUR_VK'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EK'].':',150);
		$Form->Erstelle_TextFeld('ZUR_EK',$rsZUR->FeldInhalt('ZUR_EK'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

				// Abschluss
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ABSCHLUSSART'].':',150);
		$Liste = explode("|",$AWISSprachKonserven['ZUR']['lst_ZUR_ABSCHLUSSART']);
		$Form->Erstelle_SelectFeld('ZUR_ABSCHLUSSART',$rsZUR->FeldInhalt('ZUR_ABSCHLUSSART'),200,$EditRecht,'','','1','','',$Liste,'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ABSCHLUSSGRUND'].':',150);
		$Form->Erstelle_TextFeld('ZUR_ABSCHLUSSGRUND',$rsZUR->FeldInhalt('ZUR_ABSCHLUSSGRUND'),60,900,$EditRecht,'','','','N2');
		$Form->ZeileEnde();


//ZUR_ZAL_KEY
/*
ZUR_MWS_ID
ZUR_ABSCHLUSSART

ZUR_FIL_ID
ZUR_ZUSATZINFOS
*/

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,200908131432);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>