<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 14.10.2016
 * Time: 11:00
 */
class abe_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht51000;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht51000 = $this->AWISBenutzer->HatDasRecht(51000);
        $this->_EditRecht = (($this->Recht51000 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_ABE'));

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('ABE', '%');
        $TextKonserven[] = array('ABF', '%');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_ABE');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_ABE', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht51000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'ABE'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }


    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';
        if($AWIS_KEY1>0){
            $Bedingung .= ' and ABF_KEY=' . $this->DB->WertSetzen('ABF','N0',$AWIS_KEY1) ;
            return $Bedingung;
        }

        if (isset($this->Param['ABF_FAHRGESTELLNR']) AND $this->Param['ABF_FAHRGESTELLNR'] != '') {
            $Bedingung .= 'AND ABF_FAHRGESTELLNR ' . $this->DB->LikeOderIst('*'.$this->Param['ABF_FAHRGESTELLNR']) . ' ';
        }
        if (isset($this->Param['ABF_MOTORNR']) AND $this->Param['ABF_MOTORNR'] != '') {
            $Bedingung .= 'AND ABF_MOTORNR ' . $this->DB->LikeOderIst('*'.$this->Param['ABF_MOTORNR']) . ' ';
        }
        if (isset($this->Param['ABF_ATU_NR']) AND $this->Param['ABF_ATU_NR'] != '') {
            $Bedingung .= 'AND ABF_ATU_NR ' . $this->DB->LikeOderIst($this->Param['ABF_ATU_NR']) . ' ';
        }


        return $Bedingung;

    }


}