<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

$intBestAend=$intSonderkonditionenZZBestDsAendern;		//Recht wird in card_rechte.inc.php gesetzt

if(isset($intSkz_kunden_nr))	//Wenn Neuanlage/�nderung
{
	if(isset($dblSkz_Skonto))
	{
		if($dblSkz_Skonto=='')
		{
			$dblSkz_Skonto='null';
		}
		else
		{
			$dblSkz_Skonto=str_replace(",",".",$dblSkz_Skonto);
		}
	}

	if($intSkz_kunden_nr=='')	//Neuanlage
	{
		$strSql="insert into SONDERKONDITIONEN_ZAHLUNGSZIEL 
		(SKZ_KUNDEN_NR, SKZ_ZAHLUNGSZIEL_IN_TAGEN, SKZ_SKONTO, SKZ_BEMERKUNG, SKZ_AEND_BENUTZER, SKZ_AEND_DATUM, SKZ_LOESCH)
		values('$strSkz_Kunden_Nr',$intSkz_Zahlungsziel_In_Tagen,$dblSkz_Skonto,'$strSkz_Bemerkung',
		'$strSkz_Aend_Benutzer',sysdate,0)"; 
		abfrage("ora",$strSql);
		
		$strBlock = "SKZ_KUNDEN_NR='".$strSkz_Kunden_Nr."'";
		unset($intZaehler);
	}
	else				//�nderung	bzw. L�schen
	{
		$strSql="update SONDERKONDITIONEN_ZAHLUNGSZIEL set ".
		(isset($strSkz_Kunden_Nr)?"SKZ_KUNDEN_NR='".$strSkz_Kunden_Nr."',":"").
		(isset($intSkz_Zahlungsziel_In_Tagen)?"SKZ_ZAHLUNGSZIEL_IN_TAGEN=".$intSkz_Zahlungsziel_In_Tagen.",":"").
		(isset($dblSkz_Skonto)?"SKZ_SKONTO=".$dblSkz_Skonto.",":"").
		(isset($strSkz_Bemerkung)?"SKZ_BEMERKUNG='".$strSkz_Bemerkung."',":"").
		"skz_aend_benutzer='".$strSkz_Aend_Benutzer."',skz_aend_datum=sysdate";
		if($intSkz_loesch)
		{
			$strSql.=",skz_loesch=".$intSkz_loesch;
		}
		else
		{
			$strBlock = "SKZ_KUNDEN_NR='".$intSkz_kunden_nr."'";
			unset($intZaehler);
		}
		
		$strSql.=" where SKZ_KUNDEN_NR='".$intSkz_kunden_nr."'";
		
		abfrage("ora",$strSql);
	}
}

?>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
	echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.startzeit();	//Zeitmessung
<?php } ?>
</script>
<html>
<head>
<title>Sonderkonditionen Zahlungsziele</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript">
</script>
<?php



$intScrollanzahl = 10;

$strDb = "ora";

$strSql = "select SKZ_KUNDEN_NR, SKZ_ZAHLUNGSZIEL_IN_TAGEN, SKZ_SKONTO, SKZ_BEMERKUNG from sonderkonditionen_zahlungsziel where SKZ_LOESCH=0 order by SKZ_KUNDEN_NR";
				
include("card/card_blaettern_haupt.inc.php");	//Bl�ttern

$arrSonderkond = abfrage("ora",$strSqlScroll);
			
$intSonderkondZeilen = $arrSonderkond["zeilen"];
			
$arrSonderkond = $arrSonderkond["trefferarray"];


?>
<script language="javascript">

function init()
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.endezeit();	//Zeitmessung
	<?php } ?>
	
	document.ondblclick = zoom;		//Zoomfenster
}

function aenderungzeilerest()  //Wird aus Funktion aenderungzeile() in card_allgmeinefunktionen.js aufgerufen
{
	document.getElementById('neu').setAttribute("onclick","");		//Funktion onclick auf Button neuer Dastensatz ausschalten
	
	document.getElementById('but'+window.event.srcElement.id.slice(3)).style.display='inline';		//Speicher-Button wird sichtbar
}

function aendsend(parameter)
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Sonderkonditionen Zahlungsziel �ndern");	//Zeitmessung
	<?php } ?>
		
	var obj = window.event.srcElement;
	
	var kundennr = obj.id.slice(3);
		
	var knrobj = document.getElementById("knr"+kundennr);	
	var firobj = document.getElementById("fir"+kundennr);
	var zazobj = document.getElementById("zaz"+kundennr);
	var sktobj = document.getElementById("skt"+kundennr);
	var bemobj = document.getElementById("bem"+kundennr);
	
	var knrwert = knrobj.value;
	var firwert = firobj.value;
	var zazwert = zazobj.value;
	var sktwert = sktobj.value;
	var bemwert = bemobj.value;
	
	if(knrwert.search(/^[0-9]{8}$/)==-1)	//Pr�fung ob Kundennr (8-stellig) eingegeben worden ist
	{
		alert(unescape("Geben Sie bitte eine korrekte Kunden-Nr (8-stellig) an!"));
		knrobj.select();
		return;
	}
		
	if(zazwert.search(/^[0-9]*$/)==-1||zazwert<1)	//Pr�fung ob Ganzzahl in Feld Tage
	{
		alert(unescape("Geben Sie bitte eine korrekte Zahl f%FCr das Feld Tage ein!"));
		zazobj.select();
		return;
	}
	
	if(sktwert.search(/^[0-9]*(,[0-9]{0,2})?$/)==-1||sktwert>100)
	{
		alert(unescape("Geben Sie bitte eine korrekten Wert (z.B. 2,5) f%FCr das Feld Skonto ein!"));
		sktobj.select();
		return;
	}
	
	var skz_kunden_nr=knrwert;
	var skz_zahlungsziel_in_tagen=zazwert;
	var skz_skonto = sktwert;
	var skz_bemerkung = bemwert;
	var akt_kunden_nr = kundennr;
	
	location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+"&strSkz_Aend_Benutzer=<?php echo $PHP_AUTH_USER; ?>&strSkz_Kunden_Nr="+knrwert
	+"&intSkz_Zahlungsziel_In_Tagen="+zazwert+"&dblSkz_Skonto="+sktwert+"&strSkz_Bemerkung="+bemwert+"&intSkz_kunden_nr="+kundennr;
}



//Wird aufgerufen wenn neuer Datensatz angef�gt werden soll

function neu()
{
	<?php
	if($intBestAend)
		echo "aenderungzeile('aenderungzeilerest');";	//nicht betroffene Felder ausgrauen
	else
		echo "document.getElementById('but'+window.event.srcElement.id.slice(3)).style.display='inline';";		//Speicher-Button wird sichtbar
	?>
	
	window.event.srcElement.style.display = "none";		//Button neu ausblenden
	
	document.getElementById("knr").style.display = "inline";
	
	var obj = document.getElementsByName("neuzeile");
	
	for(i=0;i<obj.length;i++)
	{
		obj[i].style.visibility = "visible";
	}
	
	document.getElementById("knr").focus();		//Fokus auf Kundennr setzen
}

function loeschen()
{
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Sonderkonditionen Zahlungsziel l�schen");	//Zeitmessung
	<?php } ?>

	var obj = window.event.srcElement;
	
	var kundennr = obj.id.slice(3);
		
	var firobj = document.getElementById("fir"+kundennr).value;
	var zazobj = document.getElementById("zaz"+kundennr).value;
	var sktobj = document.getElementById("skt"+kundennr).value;
		
	var rueck = confirm(unescape("Soll der Datensatz\n\nKunden-Nr: "+kundennr+"\nFirma: "+firobj+"\nTage: "+zazobj+"\nSkonto: "+sktobj+"\n\nwirklich gel%F6scht werden?"));
	
	if(rueck)
	{
		location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+"&strSkz_Aend_Benutzer=<?php echo $PHP_AUTH_USER; ?>&intSkz_kunden_nr="+kundennr+"&intSkz_loesch=1&intZaehler=<?php echo $intZaehler;?>";
	}
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<table cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr><td align="center" valign="top">
<?php 
if($intSonderkondZeilen)
{
?>
	<br>
	Datens&auml;tze: <span class="titel"><?php echo $strPosition; ?></span><br><br>
	<table cellspacing="0" cellpadding="0">
	<tr>
	<td class="titel">Kunden-Nr:</td><td class="titel">Firma:</td><td class="titel">Tage:</td><td class="titel">Skonto:</td><td class="titel">Bemerkung:</td><td>&nbsp;</td>
	</tr>
	<?php
		for($i=0;$i<$intSonderkondZeilen;$i++)
		{
			//Firma ermitteln
			
			$strSql='select a."Kunden_Art",b."Firma",b."Abteilung",b."Titel",b."Name",b."Vorname",b."Geschlecht"
					from '.$tabKunden.' as a left outer join '.$tabAdressen.' as b
					on a."Kunden_Nr"=b."Kunden_Nr" and b."Adress_Art"=\'01\'
					where a."Kunden_Nr"=\''.$arrSonderkond["SKZ_KUNDEN_NR"][$i].'\'';
			
			$arrAdressen=abfrage("db2",$strSql);
			
			$strFirma='';
			
			
			
			if($arrAdressen["zeilen"])
			{
				$arrAdressen=$arrAdressen["trefferarray"];
				$strAdressfeld = '';
				
				if(trim($arrAdressen["Kunden_Art"][0])=='PR')
				{
					if(trim($arrAdressen["Firma"][0])!='')
						$strAdressfeld.=trim($arrAdressen["Firma"][0])." ";
						
					if(trim($arrAdressen["Abteilung"][0])!='')
						$strAdressfeld.=trim($arrAdressen["Abteilung"][0]);
				}
				
				$strAdressfeld=trim($strAdressfeld);
				
				//$strGeschlecht=trim($arrAdressen["Geschlecht"][0]);
				
				if(trim($arrAdressen["Vorname"][0])!=''||trim($arrAdressen["Name"][0])!='')
				{
					if(trim($arrAdressen["Firma"][0])!=''||trim($arrAdressen["Abteilung"][0])!='')
						$strAdressfeld.=" / ";
				
					/*if($strGeschlecht=='1')
						$strAdressfeld.="Herrn ".$strAnredeEnde;
					elseif($strGeschlecht=='2')
						$strAdressfeld.="Frau ".$strAnredeEnde;
					
					if(trim($arrAdressen["Titel"][0])!='')
						$strAdressfeld.=trim($arrAdressen["Titel"][0])." ";*/
					
					if(trim($arrAdressen["Name"][0])!='')
						$strAdressfeld.=trim($arrAdressen["Name"][0])." ";
					
					if(trim($arrAdressen["Vorname"][0])!='')
						$strAdressfeld.=trim($arrAdressen["Vorname"][0]);
				}
				
				$strFirma=$strAdressfeld;
			}
		
			echo "<tr>";
			
			$intKundenNr = $arrSonderkond["SKZ_KUNDEN_NR"][$i];
			
			echo "<td class=\"text\"><input type=\"text\" class=\"kundennr\" name=\"grau\" id=\"knr".$intKundenNr.
			"\" value=\"".$arrSonderkond["SKZ_KUNDEN_NR"][$i]."\" maxlength=\"8\"";
			if(!$intBestAend)
				echo " readOnly";
			else
				echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
			echo "></td>";
			
			echo "<td class=\"text\"><input type=\"text\" class=\"firma\" name=\"grau\" id=\"fir".$intKundenNr.
			"\" value=\"".$strFirma."\" readOnly></td>";
			
			echo "<td class=\"text\"><input type=\"text\" class=\"zahlungsziel\" name=\"grau\" id=\"zaz".$intKundenNr.
			"\" value=\"".$arrSonderkond["SKZ_ZAHLUNGSZIEL_IN_TAGEN"][$i]."\" maxlength=\"3\"";
			if(!$intBestAend)
				echo " readOnly";
			else
				echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
			echo "></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"skonto\" name=\"grau\" id=\"skt".$intKundenNr.
			"\" value=\"".$arrSonderkond["SKZ_SKONTO"][$i]."\" maxlength=\"5\"";
			if(!$intBestAend)
				echo " readOnly";
			else
				echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
			echo "></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"bemerkung\" name=\"grau\" id=\"bem".$intKundenNr.
			"\" value=\"".$arrSonderkond["SKZ_BEMERKUNG"][$i]."\" maxlength=\"50\"";
			if(!$intBestAend)
				echo " readOnly";
			else
				echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
			echo "></td>";
			echo "<td><input type=\"image\" src=\"/bilder/diskette.png\" accesskey=\"S\" class=\"speibut\" id=\"but".$intKundenNr.
			"\" alt=\"speichern (Alt+S)\" onclick=\"aendsend();\"><input type=\"image\" src=\"/bilder/muelleimer.png\" accesskey=\"L\" name=\"loeschen\" class=\"quadrat\" id=\"loe".$intKundenNr.
			"\" alt=\"l&ouml;schen (Alt+L)\" onclick=\"loeschen();\"></td>";
			
			echo "</tr>";
		}
	?>
	<tr>
	<td class="text" id="td_dat"><input type="image" src="/bilder/plus.png" id="neu" class="quadrat" alt="neuer Datensatz" onclick="neu();"><input type="text" class="kundennr" name="neuzeile" id="knr" maxlength="8" tabindex="1"></td>
	<td class="text"><input type="text" class="firma" name="neuzeile" id="fir" readOnly></td>
	<td class="text"><input type="text" class="zahlungsziel" name="neuzeile" id="zaz" maxlength="3" tabindex="2"></td>
	<td class="text"><input type="text" class="skonto" name="neuzeile" id="skt" maxlength="5" tabindex="3"></td>
	<td class="text"><input type="text" class="bemerkung" name="neuzeile" id="bem" maxlength="50" tabindex="4"></td>
	<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="speibut" name="neuzeile" id="but" alt="speichern (Alt+S)" onclick="aendsend();"></td>
	</tr>
	</table>
<?php
}
?>
</td></tr></table>
</body>
</html>
<?php 
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>