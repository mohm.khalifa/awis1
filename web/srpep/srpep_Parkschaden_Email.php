<?php
require_once 'awisMailer.inc';
try
{
//    $AWISBenutzer = awisBenutzer::Init();
//    $DB = awisDatenbank::NeueVerbindung('AWIS');
//    $DB->Oeffnen();
    
    $Mail = new awisMailer($DB, $AWISBenutzer);          // Mailer-Klasse instanzieren
    $Werkzeuge = new awisWerkzeuge();
    $Absender = 'noreply@de.atu.eu';                     // Absender der Mails
    
    $EmpfaengerBCC = '';
    $EmpfaengerCC = '';
    //Versendet die Parkschadenemail. --> Wird inkludiert in srpep_speichern.php im Block f�r Parksch�den. 
   
//    var_dump ($rsZustaendigkeit->FeldInhalt('LL_MAIL'));
    $MailErsetzen = array('');
    if($MailTyp == 'LL') //Leiterlackmail, wenn Servicecenter abschlie�t. 
    {
        
        if ($rsZustaendigkeit->FeldInhalt('LL_MAIL')!= '')
        {
            $Empfaenger = $rsZustaendigkeit->FeldInhalt('LL_MAIL');
        }
        elseif ($rsZustaendigkeit->FeldInhalt('GBL_MAIL')!= '')
        {
            $Empfaenger = $rsZustaendigkeit->FeldInhalt('GBL_MAIL');
        }
        else 
        {
            $Empfaenger = 'shuttle@de.atu.eu';
        }
        $MVTBereich = 'PKS_LEITERLACK';
        $MVTVersNR = '';
        $MailErsetzen = array('PKS_KEY');
        
    }
    else //Versicherungsemails
    {
        //Erstmal Mail holen
    
        //var_dump ($_REQUEST);
        $MVTVersNR =  ($rsPKS->FeldInhalt('PKS_VERSNR')!=''?$rsPKS->FeldInhalt('PKS_VERSNR'):(isset($_POST['txtPKS_VERSNR'])?$_POST['txtPKS_VERSNR']:0));
        $SQLVersEmpfaenger  ='SELECT VVE_EMAIL FROM VERSVERSICHERUNGEN';
        $SQLVersEmpfaenger .=' WHERE VVE_VERSNR = '.$DB->WertSetzen('VVE', 'N0', $MVTVersNR);
        
        $rsVersEmpfaenger = $DB->RecordSetOeffnen($SQLVersEmpfaenger,$DB->Bindevariablen('VVE',true));
        $Empfaenger = $rsVersEmpfaenger->FeldInhalt('VVE_EMAIL');
        //Dann MVT Bereich festlegen
        if($MailTyp == 'S') //Versicherungsmail wenn Leiterlack storniert.
        {
            $MVTBereich = 'PKS_VERSICHERUNGSTORNO_';
        }
        elseif($MailTyp == 'A') //Versicherungsmail wenn Leiterlack den Vorgang nicht annimmt.
        {
            $MVTBereich = 'PKS_VERSICHERUNGSABSAGE_';   
        }
        require_once 'awisDokument.inc';
        require_once('/daten/web/berichte/ber_Srpep_EmailVersicherung.inc');
        $BerObj = new ber_Srpep_EmailVersicherung($AWISBenutzer, $DB, 51);
        
        $DokNummer = 'Versicherungsemail Parkschaden';
        $DokNummer .= '(' . $AWIS_KEY1 . ' ' . strftime('%Y%m') . ')';
        $DokType = 0;
        $DokDatum = date('d.m.Y');
        $DokBemerkung = 'Versicherungsmail Parkschaden f�r PKS Key: ' . $AWIS_KEY1;
        $Dateierweiterung = 'pdf';
        $Zuordnungen = array();
        $Zuordnungen[] = array('PKS',$AWIS_KEY1);
        $DokObj = new awisDokument($DB, $AWISBenutzer);	 
        $DokInfos = $DokObj->ErzeugeDokumentenPfad($DokNummer, $DokType, $DokDatum , $DokBemerkung, $Dateierweiterung, $Zuordnungen);
        
        $Parameter['DATEINAME']=$DokInfos['pfad'];
        $Parameter['PKS_KEY']=$rsPKS->FeldInhalt('PKS_KEY');
        
        $BerObj->init($Parameter);
        $BerObj->ErzeugePDF(4);
       
    }
        
    
    //Mailtext holen. Sollte es eine Versicherungsemail sein, wird die Versicherungsspezifische geholt
    $SQL  ='SELECT';
    $SQL .='   MVT_BETREFF,';
    $SQL .='   MVT_TEXT';
    $SQL .=' FROM';
    $SQL .='   Mailversandtexte';
    $SQL .=' WHERE';
    $SQL .='   MVT_BEREICH = :var_MVT_BEREICH';

    //Bereich der Email setzen, Wenn eine Versicherungsnummer gesetzt ist, dann anh�ngen..
 
    $DB->SetzeBindevariable('MVT', 'var_MVT_BEREICH' , $MVTBereich . ($MVTVersNR));   
    $rsMail = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('MVT',true));
    //Schauen ob es f�r diese Versicherung einen Mailtext gibt, wenn nicht nehm den Standardtext
    if ($rsMail->AnzahlDatensaetze() < 1)
    {
        $DB->SetzeBindevariable('MVT', 'var_MVT_BEREICH' , $MVTBereich.'0');
        $rsMail = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('MVT',true));
    }
    
    $Debugbetreff = 'An: ' . $Empfaenger . ' ';
    
    $Betreff = $rsMail->FeldInhalt('MVT_BETREFF');
    $Text = $rsMail->FeldInhalt('MVT_TEXT');
    
   
    $SQL = 'Select * from parkschaeden where pks_key='.$SEQ;
    $rsMailPKS = $DB->RecordSetOeffnen($SQL);

     foreach($MailErsetzen AS $Ersetzfeld)
    {
    	$Text = str_replace('#'.$Ersetzfeld.'#',$rsMailPKS->FeldInhalt($Ersetzfeld), $Text);
    }
       
    //Empf�nger �berb�geln, wenn nicht produktiv. 
    if ($Werkzeuge->awisLevel() != awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
    {     
        $Betreff = $Debugbetreff . $rsMail->FeldInhalt('MVT_BETREFF');
        //$Empfaenger = 'patrick.gebhardt@de.atu.eu';
        $Empfaenger = 'tobias.schaeffler@de.atu.eu';
    }
     
    $Mail->AnhaengeLoeschen();
    $Mail->LoescheAdressListe();
    $Mail->DebugLevel(0);
    $Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
    $Mail->AdressListe(awisMailer::TYP_CC, $EmpfaengerCC, false, false);
    $Mail->AdressListe(awisMailer::TYP_BCC, $EmpfaengerBCC, false, false);
    $Mail->Absender($Absender);
    $Mail->Betreff($Betreff);
    $Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
    $Mail->SetzeBezug('PKS',$rsMailPKS->FeldInhalt('PKS_KEY')); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
    $Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
    
    if (isset($DokInfos['pfad']) and $DokInfos['pfad'] != '')
    {
        $Mail->Anhaenge($DokInfos['pfad'],'Parkschadendokumentation.pdf',true);	
    }
    
    if ($Werkzeuge->awisLevel() != awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
    {
    	//$Mail->MailInWarteschlange();
    	$Mail->MailSenden(); 
        $Form->Hinweistext($AWISSprachKonserven['PKS']['PKS_EMAIL']);
    }
    else 
    {
        $Mail->MailInWarteschlange();
        $Form->Hinweistext($AWISSprachKonserven['PKS']['PKS_EMAIL']);
    }       
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}

?>