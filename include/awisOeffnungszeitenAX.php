<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisCSVSpooler.inc';
require_once 'awisWerkzeuge.inc';

class awisOeffnungszeitenAX
{
    private $DB;

    private $AWISBenutzer;

    private $DebugLevel;

    private $Exportpfad = '';

    private $Werkzeuge;

    const PFAD = '/win/40_INTERFACE/AWIS-Axapta/';

    const TZ = ';';

    public function __construct($Benutzer)
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();

        $this->Werkzeuge = new awisWerkzeuge();

        $Dateiname = 'openingtimes_' . date('YmdHis') . '.csv';

        $this->Exportpfad .= self::PFAD . $Dateiname;
    }

    public function ExportOeffnungszeiten()
    {
        $this->Log('Hole alle aktuellen Filialen aus Datenbank.', 1);
        
        $SQL = ' select FOZ_ID, FZP_GUELTIG_AB, FZP_FIL_ID from FILIALENOEFFNUNGSZEITENPFLEGE ';
        $SQL .= ' inner join FILIALENOEFFNUNGSZEITENMODELLE ';
        $SQL .= ' on FZP_FOZ_KEY = FOZ_KEY ';
        $SQL .= ' where trunc(FZP_USERDAT) = trunc(sysdate - 1)';

        $rsFZP = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('FZP'));

        if($rsFZP->AnzahlDatensaetze() > 0){
            $Datei = fopen($this->Exportpfad . '.tmp', 'w');
        }else{
            return true;
        }

        while (!$rsFZP->EOF()){
            $Zeile = $rsFZP->FeldInhalt('FZP_FIL_ID') . self::TZ;
            $Zeile .= $rsFZP->FeldInhalt('FOZ_ID') . self::TZ;
            $Zeile .= $rsFZP->FeldInhalt('FZP_GUELTIG_AB', 'D');
            $Zeile .= "\r\n";

            fwrite($Datei,$Zeile);

            $rsFZP->DSWeiter();
        }

        fclose($Datei);

        $this->Log('Stelle Datei bereit..', 1);
        if (is_file($this->Exportpfad)) {
            $this->Log('Alte Datei vorhanden, l�sche..', 1);
            unlink($this->Exportpfad);
        }

        $this->Log('Bennene Tempdatei um.', 1);
        rename($this->Exportpfad . '.tmp', $this->Exportpfad);
        $this->Log('Habe alles erledigt.', 1);
    }

    public function DebugLevel($Level)
    {
        $this->DebugLevel = $Level;
    }

    public function Log($Text, $Level)
    {
        if ($this->DebugLevel >= $Level) {
            echo date('c') . ' ' . $Text . PHP_EOL;
        }
    }

}