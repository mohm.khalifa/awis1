<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RAV','%');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht631 = $AWISBenutzer->HatDasRecht(630);
	if($Recht631==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GSL'));

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./lieferantenadr_Main.php?cmdAktion=Details>");

	/**********************************************
	* Eingabemaske
	***********************************************/

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LIEFERANTENART'].':',150);
	$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_LIEFERANTENART']);
	$Form->Erstelle_SelectFeld('*GSL_LIEFERANTENART',($Param['SPEICHERN']=='on'?$Param['GSL_LIEFERANTENART']:''),150,true,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);

	$AWISCursorPosition='sucRAV_LIEFERANTENART';
	$Form->ZeileEnde();

	$SQL =  'select gsb_key,gsb_branche';
	$SQL .= ' from gsvbranchen';
	$SQL .= ' order by gsb_branche asc';

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_BRANCHE'].':',150);
	$Form->Erstelle_SelectFeld('*GSL_BRANCHE',($Param['SPEICHERN']=='on'?$Param['GSL_BRANCHE']:''),150,true,$SQL,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_AKTIV'] . ':',150);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	$Form->Erstelle_SelectFeld('*GSL_AKTIV',($Param['SPEICHERN']=='on'?$Param['GSL_AKTIV']:''),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
	$Form->ZeileEnde();

	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',150);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();

	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	$Form->SchaltflaechenEnde();
	$Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201431");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201432");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>