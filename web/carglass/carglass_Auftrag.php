<?php
global $CAG;
try
{
    $CAG->RechteMeldung(0);//Anzeigen Recht
    echo "<form name=frmSuche method=post action=./carglass_Main.php>";

    $CAG->Form->Formular_Start();
    $CAG->Form->Trennzeile('O');


    //Ausgabe in welchem Browser man sich befindet
    $BrowserIE = $CAG->CheckIEBrowser();
//    $BrowserIE = true;
    if ($BrowserIE == FALSE) {
        //Anderer Browser als IE
        $CAG->Form->ZeileStart();
        $CAG->Form->Erstelle_TextLabel($CAG->AWISSprachKonserven['CAG']['CAG_HINWEIS'],250);
        $CAG->Form->ZeileEnde();

        //1. Insert in Tabelle CarglassKooperation
        $CAG->InsertCarglassKopf();

        //2. Counter Java Skript 10 Sekunden
        //3. Weiterleitung an Link
        $CAG->Weiterleitung();



        $CAG->Form->ZeileStart();
        $CAG->Form->Erstelle_TextLabel($CAG->AWISSprachKonserven['CAG']['CAG_WEITERLEITUNG'],250,'Hinweis');
        $CAG->Form->ZeileEnde();
        $CAG->Form->Trennzeile('O');
    }
    else{
        //IE
        $CAG->Form->ZeileStart();
        $CAG->Form->Erstelle_TextLabel($CAG->AWISSprachKonserven['CAG']['CAG_ACHTUNGBROWSER'],250);
        $CAG->Form->ZeileEnde();
    }

    $CAG->Form->Trennzeile('O');
    $CAG->Form->Formular_Ende();

    $CAG->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($CAG->Form instanceof awisFormular)
	{
        $CAG->Form->DebugAusgabe(1, $ex->getSQL());
        $CAG->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"202104081620");
	}
	else
	{
        $CAG->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($CAG->Form instanceof awisFormular)
	{
        $CAG->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"202104081619");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>