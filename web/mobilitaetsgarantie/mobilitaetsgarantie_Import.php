<?php
global $AWIS_KEY1;
global $MOB;
require_once 'awisDiagramm.inc';
require_once 'awis_mobilitaetsgarantien.inc';

try {

    if (isset($_FILES['Importdatei'])) {

        $Import = new awis_mobilitaetsgarantien(true);
        $Import->MD5Check = false;
        $Erg = $Import->ImportAusMaske($_FILES['Importdatei']['tmp_name'],$_FILES['Importdatei']['name']);

        //Fehlerausgabe
        if ($Erg ===true) {
            $MOB->Form->ZeileStart();
            $MOB->Form->Hinweistext($MOB->AWISSprachKonserven['MOB']['MOB_IMPORT_OK'],awisFormular::HINWEISTEXT_OK);
            $MOB->Form->ZeileEnde();
        } else{
            $MOB->Form->ZeileStart();
            $MOB->Form->Hinweistext($MOB->AWISSprachKonserven['MOB'][$Erg],awisFormular::HINWEISTEXT_FEHLER);
            $MOB->Form->ZeileEnde();
        }
    }


    $MOB->Form->Formular_Start();
    echo '<form name=frmmobilitaetsgarantie action=./mobilitaetsgarantie_Main.php?cmdAktion=Import method=POST  enctype="multipart/form-data">';


    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel('Import:',170,'font-weight: bold; font-size:15px');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['Wort']['Dateiname'] . ':', 150);
    $MOB->Form->Erstelle_DateiUpload('Importdatei', 600, 30,2000000,'','.csv,.xls,.xlsx');
    $MOB->Form->ZeileEnde();


    $MOB->Form->Formular_Ende();
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $MOB->Form->SchaltflaechenStart();
    $MOB->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $MOB->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        $MOB->Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $MOB->AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    $MOB->Form->SchaltflaechenEnde();

    $MOB->Form->SchreibeHTMLCode('</form>');

} catch (awisException $ex) {
    if ($MOB->Form instanceof awisFormular) {
        $MOB->Form->DebugAusgabe(1, $ex->getSQL());
        $MOB->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $MOB->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($MOB->Form instanceof awisFormular) {
        $MOB->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>