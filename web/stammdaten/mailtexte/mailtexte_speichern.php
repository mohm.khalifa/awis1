<?php
global $MVT;
try {

    $Meldung = '';

    if($MVT->AWIS_KEY1 == '-1'){ //Neuanlage

        $MVT->RechteMeldung(4);
        $SQL  ='INSERT INTO MAILVERSANDTEXTE';
        $SQL .=' (';
        $SQL .=' MVT_BEREICH,';
        $SQL .=' MVT_SPRACHE,';
        $SQL .=' MVT_BETREFF,';
        $SQL .=' MVT_TEXT,';
        $SQL .=' MVT_GUELTIGAB,';
        $SQL .=' MVT_USER,';
        $SQL .=' MVT_USERDAT,';
        $SQL .=' MVT_GRUPPE,';
        $SQL .=' MVT_XRC_ID,';
        $SQL .=' MVT_RECHTESTUFE_ANZEIGEN,';
        $SQL .=' MVT_RECHTESTUFE_BEARBEITEN';
        $SQL .=' )';
        $SQL .=' VALUES';
        $SQL .=' (';
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_BEREICH']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_SPRACHE']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_BETREFF']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_TEXT']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','D',$_POST['txtMVT_GUELTIGAB']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$MVT->AWISBenutzer->BenutzerName());
        $SQL .= ',sysdate';
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_GRUPPE']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['sucXRC_ID']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['sucXRS_BIT_ANZ']);
        $SQL .= ','. $MVT->DB->WertSetzen('MVT','T',$_POST['sucXRS_BIT_BEARB']);
        $SQL .=' )';

        $MVT->DB->Ausfuehren($SQL,'',true,$MVT->DB->Bindevariablen('MVT'));

        $Meldung = $MVT->AWISSprachKonserven['Meldung']['InsertOK'];

    }else{ //Bearbeiten
        $MVT->RechteMeldung(2);

        $SQL  ='UPDATE MAILVERSANDTEXTE SET  ';
        $SQL .='   MVT_BEREICH                = '. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_BEREICH']);
        $SQL .=' , MVT_SPRACHE                = '. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_SPRACHE']);
        $SQL .=' , MVT_BETREFF                = '. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_BETREFF']);
        $SQL .=' , MVT_TEXT                   = '. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_TEXT']);
        $SQL .=' , MVT_GUELTIGAB              = '. $MVT->DB->WertSetzen('MVT','D',$_POST['txtMVT_GUELTIGAB']);
        $SQL .=' , MVT_USER                   = '. $MVT->DB->WertSetzen('MVT','T',$MVT->AWISBenutzer->BenutzerName());
        $SQL .=' , MVT_USERDAT                = sysdate ';
        $SQL .=' , MVT_GRUPPE                 = '. $MVT->DB->WertSetzen('MVT','T',$_POST['txtMVT_GRUPPE']);
        $SQL .=' WHERE MVT_KEY = '.  $MVT->DB->WertSetzen('MVT','Z',$MVT->AWIS_KEY1);


        $MVT->DB->Ausfuehren($SQL,'',true,$MVT->DB->Bindevariablen('MVT'));
        $Meldung = $MVT->AWISSprachKonserven['Meldung']['UpdateOK'];
    }


    if ($Meldung != '') {
        $MVT->Form->Hinweistext($Meldung,awisFormular::HINWEISTEXT_OK);
    }
} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    $Information .= '<br>SQL: ' . $ex->getSQL() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $MVT->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $MVT->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    $Information .= 'SQL: ' . $ex->getSQL();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $MVT->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>