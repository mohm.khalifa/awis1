<?php
/**
 * L�schen von Informationen auf der Filialinfo
 *
 * @author Sacha Kerres
 * @version 20090813
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');


try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

//$Form->DebugAusgabe(1,$_POST);
	$Tabelle= '';

	if(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		switch($_GET['Seite'])
		{
			case 'Mitbewerb':
				$Tabelle = 'MFI';
				$Key=$_GET['Del'];

				$SQL = 'SELECT MFI_KEY, MFI_FIL_ID, MBW_NAME1 ';
				$SQL .= ' FROM MitbewerberFilialen';
				$SQL .= ' INNER JOIN Mitbewerber ON MFI_MBW_KEY = MBW_KEY';
				$SQL .= ' WHERE MFI_KEY=0'.$Key;

				$rsDaten = $DB->RecordsetOeffnen($SQL);

				$FILKey = $rsDaten->FeldInhalt('MFI_FIL_ID');

				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('MBW','MBW_NAME1'),$rsDaten->FeldInhalt('MBW_NAME1'));
				$Felder[]=array($Form->LadeTextBaustein('MBW','MBW_STRASSE'),$rsDaten->FeldInhalt('MBW_STRASSE'));
				break;
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'MFI':
				$SQL = 'DELETE FROM MitbewerberFilialen WHERE mfi_key=0'.$_POST['txtKey'].' AND MFI_FIL_ID = '.$_POST['txtFILKey'];
				$AWIS_KEY1=$_POST['txtFILKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('filialinfos_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{

		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./filialinfo_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('FILKey',$FILKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>