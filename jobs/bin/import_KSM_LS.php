<?php
require_once('/daten/include/awisZukauf_KSM.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = 'awis_jobs';
    
    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    
	$ZUK = new awisZukauf_KSM((isset($argv[1])?$argv[1]:0),false);

	if($ZUK->LeseLieferscheine((isset($argv[2])?$argv[2]:''),'','',(isset($argv[2])?'A':''))>0)
	{
		$ZUK->SpeichereLieferscheine();
	}
}
catch (Exception $ex)
{
    echo $ex->getMessage().PHP_EOL;
}
?>
