<?php
require_once 'awisAenderungshistorie.inc';
/**
 * Klasse fuer Werkzeuge rund um AWIS
 *
 * @author    Sacha Kerres
 * @version   200807
 * @copyright ATU Auto Teile Unger
 *
 */
class awisWerkzeuge
{
    /**
     * Pfad zu der Konfigurationsdatei
     *
     */
    CONST AWIS_INI_PFAD = '/daten/conf/';

    /**
     * Objekt initialisieren
     *
     * Es wird gepr�ft, ob das Konfigurationsverzeichnis existiert.
     *
     */
    public function __construct()
    {
        if (!is_dir(self::AWIS_INI_PFAD)) {
            throw new Exception('Verzeichnis mit AWIS Konfiguration ist nicht vorhanden', 1);
        }
    }

    /**
     * Objekt aufraeumen
     *
     */
    public function __destruct()
    {
    }

    /**
     * Liefert eine komprimierte Artikelnummer
     *
     * @param string $ArtNr        Originale Artikelnummer
     * @param array  $ZeichenListe Liste mit ASCII-Codes, die nicht gefiltert werden sollen
     */
    public static function ArtNrKomprimiert($ArtNr, $ZeichenListe = array())
    {
        $ArtNr = strtoupper($ArtNr);
        $Erg = '';
        for ($i = 0; $i < strlen($ArtNr); $i++) {
            $Chr = ord($ArtNr[$i]);
            if (($Chr >= 48 AND $Chr < 58) OR ($Chr >= 65 AND $Chr < 91) OR (array_search($Chr,
                                                                                          $ZeichenListe) !== false)
            ) {
                $Erg .= $ArtNr[$i];
            }
        }

        return $Erg;
    }

    /**
     * Einen Parameter aus dem AWIS System lesen
     *
     * @param string $INIQuelle (Dateiname oder 'DB')
     * @param string $Parameter
     * @return string
     * @version 20070802
     * @author  Sacha Kerres
     */
    public function LeseAWISParameter($INIQuelle, $Parameter)
    {
        $Erg = '';

        // Pruefen, ob es eine Datei mit dem Namen gibt
        if (is_file(self::AWIS_INI_PFAD . $INIQuelle)) {
            $Zeilen = file(self::AWIS_INI_PFAD . $INIQuelle);
            foreach ($Zeilen AS $Zeile)                // Alle Zeilen einlesen
            {
                $Zeile = trim($Zeile);
                if (strpbrk(substr(trim($Zeile), 0, 1), '#;') === false)    // Kommentarzeile?
                {
                    $Inhalt = explode('=', $Zeile);
                    if (sizeof($Inhalt) == 2)            // Zwei Eintraege  PARAM=WERT
                    {
                        if (strcasecmp(trim($Inhalt[0]), $Parameter) == 0) {
                            $Erg = trim($Inhalt[1]);// Wert zurueckliefern
                        }
                    }
                }
            }
        }

        return $Erg;
    }

    /**
     * E-Mail senden
     *
     * @param array  $TO
     * @param string $Betreff
     * @param string $Text
     * @param int    $Level        (1=niedrig, 2=normal, 3=wichtig)
     * @param string $ReplyTo
     * @param string $Absender
     * @param int    $RueckgabeTyp (0=Boolean, 1=Text)
     * @param array  $CC
     * @param array  $BCC
     * @param int    $Textart      1=ASCII, 2=HTML
     * @param array  $Serverdaten  Array mit Anmeldedaten an SMTP Server
     * @return mixed
     * @version 200708071927
     * @author  Sacha Kerres
     */
    public function EMail(
        $TO,
        $Betreff,
        $Text,
        $Level = 2,
        $ReplyTo = '',
        $Absender = '',
        $RueckgabeTyp = 0,
        $CC = null,
        $BCC = null,
        $TextArt = 1,
        $ServerDaten = null
    )
    {
        require_once 'awisMailer.inc';

        $AWISBenutzer = awisBenutzer::Init('awis_jobs');
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
        $Mail = new awisMailer($DB, $AWISBenutzer);

        $Mail->AnhaengeLoeschen();
        $Mail->LoescheAdressListe();
        $Mail->DebugLevel($Level);
        if(is_array($TO)){
            foreach($TO as $Empf){
                $Mail->AdressListe(awisMailer::TYP_TO, $Empf, false, false);
            }
        }else{
            $Mail->AdressListe(awisMailer::TYP_TO, $TO, false, false);
        }
        if($CC !== null) {
            $Mail->AdressListe(awisMailer::TYP_CC, $CC, false, false);
        }
        if ($BCC !== null) {
            $Mail->AdressListe(awisMailer::TYP_BCC, $BCC, false, false);
        }
        if ($Absender == '') {
            $Absender = 'awis' . strval(($this->awisLevel() == awisWerkzeuge::AWIS_LEVEL_SHUTTLE) ? (awisWerkzeuge::AWIS_LEVEL_SHUTTLE) : (($this->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV) ? (awisWerkzeuge::AWIS_LEVEL_PRODUKTIV) : (awisWerkzeuge::AWIS_LEVEL_ENTWICK))) . '@de.atu.eu';
        }
        $Mail->Absender($Absender);
        $Mail->Betreff($Betreff);
        $Mail->Text($Text,(($TextArt == 2)?(awisMailer::FORMAT_HTML):(awisMailer::FORMAT_TEXT)));
        if($ServerDaten !== null) {
            $Mail->SMTP_Authentifizierung($ServerDaten['user'],$ServerDaten['kennwort']);
        }
        $Mail->MailSenden();
    }


    /**
     * Eintrag in Syslog schreiben
     *
     * @param int    $Prioritaet
     * @param string $Meldung
     * @author  Sacha Kerres
     * @version 200710090920
     *
     */
    public function SyslogEintrag($Prioritaet, $Meldung)
    {
        $LogFacility = self::LeseAWISParameter('awis.conf', 'SYSLOG_FACILITY');
        //define_syslog_variables();
        openlog("AWIS", LOG_CONS | LOG_PID | LOG_ODELAY, constant($LogFacility));

        syslog($Prioritaet, $Meldung);

        closelog();
    }


    /**
     * Liefert einen Wert aus einer Infotabelle
     *
     * @param awisDatenbank $DB
     * @param string        $Tabelle
     * @param int           $InfoKey
     * @param int           $TabKey
     * @param string        $Typ    (nach awis_Format())
     * @param int           $IMQ_ID
     * @param boolean       $MitKey (Tabelle mit Keys liefern)
     * @return mixed
     */
    public function InfoFeldwert(
        awisDatenbank $DB,
        $Tabelle,
        $InfoKey,
        $TabKey,
        $Typ = 'T',
        $IMQ_ID = '',
        $MitKey = false
    ) {
        global $awisRSZeilen;
        global $AWISSprache;

        if ($MitKey) {
            $Erg = array(array('Key' => 0, 'Wert' => ''));    // Defaultarray ist leer
            $Elemente = 0;
        } else {
            $Erg = '';
        }

        switch ($Tabelle) {
            case 'AST':

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $TabKey;
                $BindeVariablen['var_N0_asi_ait_id'] = $DB->FeldInhaltFormat('N0', $InfoKey);

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= ' WHERE ASI_AST_ATUNR = :var_T_ast_atunr';
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                if ($IMQ_ID != '') {
                    $BindeVariablen['var_N0_asi_imq_id'] = $DB->FeldInhaltFormat('N0', $IMQ_ID);
                    $SQL .= ' AND BITAND(ASI_IMQ_ID,:var_N0_asi_imq_id) <> 0';
                }
                $rs = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

                $Form = new awisFormular();
                while (!$rs->EOF()) {
                    $Wert = $Form->Format($Typ, ($rs->FeldInhalt('ASI_WERT') != ''?$rs->FeldInhalt('ASI_WERT'):''));
                    if ($MitKey) {
                        $Erg[$Elemente++] = array(
                            'Key' => ($rs->FeldInhalt('ASI_KEY') != ''?$rs->FeldInhalt('ASI_KEY'):'0'),
                            'Wert' => $Wert
                        );
                    } else {
                        $Erg .= ($Erg == ''?'':',') . $Wert;
                    }
                    $rs->DSWeiter();
                }

                break;
            default:
                $Erg = '##TAB##';
        }

        return $Erg;
    }


    /**
     * Pr�fziffer einer WA-Nummer f�r den Zukauf
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_WERKSTATTAUFTRAG = 'WA';
    /**
     * Bestellnummer aus AX
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_AXBESTELLNR = 'AXB';
    /**
     * Pr�fziffer f�r eine 13-stellig EAN
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_EAN = 'EAN';
    /**
     * Pr�fziffer f�r eine 17-stellig Fahrzeugidentifikationsnummer
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_FIN = 'FIN';
    /**
     * Pr�fziffer f�r den neuen F�hrerschein als Kreditkarte
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_FS_EUKARTE = 'FSEUK';
    /**
     * Pr�fziffer f�r den rosa F�hrerschein
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_FS_EUROSA = 'FSROSA';
    /**
     * Pr�fziffer f�r den alten, grauen F�hrerschein
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_FS_GRAU = 'FSGRAU';
    /**
     * P�rfung, auf DDR F�hrerschein
     *
     * @var string
     */
    const PRUEFZIIFER_TYP_FS_DDR = 'FSDDR';
    /**
     * Gew�nschte Aktion der Pruefziffermethode: Pr�fen
     *
     * @var int
     */
    const PRUEFZIIFER_ERG_PRUEFEN = 1;
    /**
     * Gew�nschte Aktion der Pruefziffermethode: Pr�fziffer berechnen
     *
     * @var int
     */
    const PRUEFZIIFER_ERG_ERMITTELN = 2;

    /**
     * Pr�ft oder berechnet eine Pruefziifer
     *
     * @param string $Wert      Zahl oder Text
     * @param string $Verfahren Nach Kontante PRUEFZIIFER_TYP_*
     * @param int    $Ergebnis  Aktion, die durchgef�hrt werden soll PRUEFZIIFER_ERG_*, (1=Pruefung, 2=Ermittlung Pr�fziffer)
     */
    public static function BerechnePruefziffer(
        $Wert,
        $Verfahren = self::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,
        $Ergebnis = self::PRUEFZIIFER_ERG_PRUEFEN
    ) {
        switch ($Verfahren) {
            case self::PRUEFZIIFER_TYP_FIN :
                $ZeichenWerte = array(
                    '0' => 0,
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    'A' => 1,
                    'B' => 2,
                    'C' => 3,
                    'D' => 4,
                    'E' => 5,
                    'F' => 6,
                    'G' => 7,
                    'H' => 8,
                    'I' => 9,
                    'J' => 1,
                    'K' => 2,
                    'L' => 3,
                    'M' => 4,
                    'N' => 5,
                    'O' => 0,
                    'P' => 7,
                    'Q' => 8,
                    'R' => 9,
                    'S' => 2,
                    'T' => 3,
                    'U' => 4,
                    'V' => 5,
                    'W' => 6,
                    'X' => 7,
                    'Y' => 8,
                    'Z' => 9
                );
                $Gewichte = array(
                    17 => 9,
                    16 => 8,
                    15 => 7,
                    14 => 6,
                    13 => 5,
                    12 => 4,
                    11 => 3,
                    10 => 2,
                    9 => 10,
                    8 => 9,
                    7 => 8,
                    6 => 7,
                    5 => 6,
                    4 => 5,
                    3 => 4,
                    2 => 3,
                    1 => 2
                );
                $PSumme = 0;
                $Wert = strrev($Wert);
                for ($i = strlen($Wert); $i > 0; $i--) {
                    $PSumme += ($ZeichenWerte[$Wert[$i - 1]] * $Gewichte[$i]);
                }
                $PZ = ($PSumme % 11);

                return $PZ;        // Kann nicht in der Nummer gepr�ft werden. Muss extern mit der PZ verglichen werden
                break;
            case self::PRUEFZIIFER_TYP_EAN :
                $Wert = str_pad($Wert, 13, '0', STR_PAD_LEFT);        // Sauber f�llen
                $PSumme = 0;
                for ($i = 13 - 2; $i >= 0; $i--) {
                    // Es d�rfen nur Zahlen vorkommen
                    if (!is_numeric($Wert[$i]) AND $Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                        return false;
                    }
                    $PSumme += $Wert[$i] * (1 + (($i % 2)?2:0));
                }
                $PZ = 10 - ($PSumme % 10);
                if ($PZ == 10) {
                    $PZ = 0;
                }
                if ($PZ == $Wert[12]) {
                    if ($Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                        return true;
                    } else {
                        return $Wert . $PZ;
                    }
                }

                return false;
                break;
            case self::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG :
                if ($Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                    if ($Wert === '' OR strlen($Wert) != 11 OR (substr($Wert, 0, 4) == '0000')) {
                        return false;
                    }
                    $PruefWert = 0;
                    $Wert = str_pad($Wert, 11, '0', STR_PAD_LEFT);
                    $PruefZiffer = $Wert[10];
                    $Wert = substr($Wert, 0, 10);

                    for ($i = 9; $i >= 0; $i--) {
                        $PruefWert += ((($i % 2) == 1)?3:1) * $Wert[$i];
                    }
                    $PruefWert = 10 - (($PruefWert % 10) == 0?10:($PruefWert % 10));

                    return ($PruefWert == $PruefZiffer);
                } else {
                    $PruefWert = 0;
                    $Wert = str_pad($Wert, 10, '0', STR_PAD_LEFT);
                    for ($i = 9; $i >= 0; $i--) {
                        $PruefWert += (($i % 2)?3:1) * $Wert[$i];
                    }
                    //$PruefWert = ($PruefWert%10);
                    $PruefWert = 10 - (($PruefWert % 10) == 0?10:($PruefWert % 10));

                    return ($Wert . $PruefWert);
                }
                break;
            case self::PRUEFZIIFER_TYP_AXBESTELLNR :
                if ($Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                    if ($Wert === '' OR strlen($Wert) != 14 OR (substr($Wert, 0, 3) != '200')) {
                        return false;
                    }
                    $PruefWert = 0;
                    $Wert = str_pad($Wert, 14, '0', STR_PAD_LEFT);
                    $PruefZiffer = $Wert[13];

                    $Wert = substr($Wert, 0, 14);

                    for ($i = 0; $i <= 12; $i++) {
                        $PruefWert += (($i % 2) == 0?3:1) * $Wert[$i];
                    }
                    $PruefWert = 10 - ($PruefWert % 10);
                    if ($PruefWert == 10) {
                        $PruefWert = 0;
                    }

                    return ($PruefWert == $PruefZiffer);
                } else {
                    $PruefWert = 0;
                    $Wert = str_pad($Wert, 14, '0', STR_PAD_LEFT);
                    for ($i = 0; $i <= 12; $i++) {
                        $PruefWert += (($i % 2) == 0?3:1) * $Wert[$i];
                    }
                    //$PruefWert = ($PruefWert%10);
                    $PruefWert = 10 - ($PruefWert % 10);
                    if ($PruefWert == 10) {
                        $PruefWert = 0;
                    }

                    return ($Wert . $PruefWert);
                }
                break;
            case self::PRUEFZIIFER_TYP_FS_EUKARTE :            // EU-F�hrerschein-Karte
                if ($Wert === '' OR strlen($Wert) < 9 OR strlen($Wert) > 11) {
                    return false;
                }
                $Wert = str_pad($Wert, 11, '0', STR_PAD_RIGHT);            // Auf 11 auff�llen
                $PruefWert = 0;
                // Aufbau: Xaaabbbbbc (X=Buchstabe + aaa => Beh�rdenschl�ssel, bbbbb=>Laufende Nummer (00001-ZZZZZZ), c=>F�hrerschein-LfdNummer (1-Z))
                $ErlaubteZeichen = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $Produkte = 0;
                $Zeichen = strrev(substr($Wert, 0, 10));
                for ($i = 9; $i > 0; $i--) {
                    $Produkte += $i * (strpos($ErlaubteZeichen, $Zeichen[$i]));
                    //echo 'Rechne '.$i * (strpos($ErlaubteZeichen, $Zeichen[$i])).'<br>';
                }
                //echo $Produkte.' = '.($Produkte%11).'<br>';
                $PruefZiffer = $Produkte % 11;
                if ($PruefZiffer == 10) {
                    $PruefZiffer = 'X';
                }

                if ($Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                    return ($PruefZiffer == $Wert[9]);
                } else {
                    return (substr($Wert, 0, 9) . $PruefZiffer . $Wert[10]);
                }
                break;
            case self::PRUEFZIIFER_TYP_FS_GRAU :
                /*
				 * Der graue F�hrerschein wurde von 1909 bis 1985 ausgestellt
				 * (F�hrerscheinnummer 4-6stellige Listennummer / 2stelliges Ausstellungsjahr; Bsp: 6138/72;
				 * die Listennummer bezieht sich auf die Liste der Ausstellungsbeh�rde: Landratsamt, Stadt
				 * , dort dann das Verkehrsamt und/oder die Zulassungsstelle - einmalige Sichtpr�fung!!!)
				 */
                $Werte = explode('/', $Wert);
                if (!isset($Werte[1]))    // Trennstrich?
                {
                    if (strlen($Wert) < 6 OR strlen($Wert) > 8) {
                        return false;
                    }

                    if (substr($Wert, -2, 2) > 85 or substr($Wert, -2, 2) < 19) {
                        return false;
                    }
                }
                if ($Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                    return true;
                } else {
                    return $Wert;
                }
                break;
            case self::PRUEFZIIFER_TYP_FS_EUROSA :
                /*
				*	Der rosafarbene F�hrerschein (EG-Version) wurde von April 1986 bis 1998 ausgestellt
				*   (F�hrerscheinnummer wie beim grauen F�hrerschein)
				 */
                $Werte = explode('/', $Wert);
                if (!isset($Werte[1]))    // Trennstrich?
                {
                    if (strlen($Wert) < 6 OR strlen($Wert) > 8) {
                        return false;
                    }

                    if (substr($Wert, -2, 2) > 98 or substr($Wert, -2, 2) < 86) {
                        return false;
                    }
                }
                if ($Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                    return true;
                } else {
                    return $Wert;
                }
                break;
            case self::PRUEFZIIFER_TYP_FS_DDR :
                /*
				*	Der rosafarbene DDR-F�hrerschein wurde bis zur Wende ausgestellt.
				*  (F�hrerscheinnummer Buchstabe+7stellige Nummer; Bsp.: D 7681420 - einmalige Sichtpr�fung!!!).
				 */
                if (is_numeric($Wert[0]))    // Erstes Zeichen ein Buchstabe
                {
                    return false;
                }
                if (strlen($Wert) != 8) {
                    return false;
                }

                if ($Ergebnis == self::PRUEFZIIFER_ERG_PRUEFEN) {
                    return true;
                } else {
                    return $Wert;
                }
                break;
            default:
                return false;
        }
    }


    /**
     * Wandelt einen Text in korrekte Gro�-Kleinschreibung um
     *
     * @param string $Text         (Zu �ndernder Text)
     * @param string $Zeichenliste (Zeichenliste, nach denen gro� geschrieben werden soll)
     * @return string
     */
    public function awisGrossKlein($Text, $ZeichenListe = " /.-;&+()[]\"'\t")
    {
        $Erg = strtolower($Text);
        $VorgaegerZeichen = ' ';

        for ($i = 0; $i < strlen($Erg); $i++) {
            if (strpbrk($VorgaegerZeichen, $ZeichenListe)) {
                $Erg[$i] = strtoupper($Erg[$i]);
            }

            $VorgaegerZeichen = $Erg[$i];
        }

        return $Erg;
    }

    /**
     * AWIS l�uft auf dem Entwicklungssystem
     *
     */
    const AWIS_LEVEL_ENTWICK = 'ENTW';
    /**
     * AWIS l�uft im Testsystem
     *
     */
    const AWIS_LEVEL_STAGING = 'STAG';
    /**
     * AWISl�uft auf dem Produktivsystem
     *
     */
    const AWIS_LEVEL_PRODUKTIV = 'PROD';
    /**
     * AWIS l�uft auf dem produktiven Shuttle
     *
     */
    const AWIS_LEVEL_SHUTTLE = 'SHUT';
    /**
     * AWIS l�uft auf einem unbekannten Systemlevel
     *
     */
    const AWIS_LEVEL_UNBEKANNT = '****';

    /**
     * Gibt zur�ck, auf welchem System AWIS l�uft
     *
     * @return String
     */
    public function awisLevel()
    {
        $Level = self::LeseAWISParameter('awis.conf', 'AWIS_LEVEL');

        switch ($Level) {
            case 'ENTW':
                $Erg = self::AWIS_LEVEL_ENTWICK;
                break;
            case 'STAG':
                $Erg = self::AWIS_LEVEL_STAGING;
                break;
            case 'PROD':
                $Erg = self::AWIS_LEVEL_PRODUKTIV;
                break;
            case 'SHUT':
                $Erg = self::AWIS_LEVEL_SHUTTLE;
                break;
            default:
                $Erg = self::AWIS_LEVEL_UNBEKANNT;
                self::EMail(array('shuttle@de.atu.eu'), 'awis.conf',
                            'Bitte den Parameter AWIS_LEVEL in der Datei awis.conf pflegen!', 3, 'shuttle@de.atu.eu');
        }

        return $Erg;
    }

    const HILFSWERT_TYP_TEXT = 'T';
    const HILFSWERT_TYP_ZAHL = 'N';
    const HILFSWERT_TYP_DATUM = 'D';
    const HILFSWERT_TYP_DATUMZEIT = 'DU';

    /**
     * Liest oder schreibt einen Wert in die Hilfswert-Tabelle
     *
     * @param string $Bereich
     * @param string $ID
     * @param string $Typ
     * @param string $NeuerWert
     * @return mixed
     */
    public function Hilfswert($Bereich, $ID, $Typ = self::HILFSWERT_TYP_TEXT, $NeuerWert = null)
    {
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();

        $SQL = 'SELECT XHW_KEY, ';
        switch ($Typ) {
            case self::HILFSWERT_TYP_ZAHL :
                $SQL .= ' XHW_WERTNUM';
                break;
            case self::HILFSWERT_TYP_DATUM :
            case self::HILFSWERT_TYP_DATUMZEIT :
                $SQL .= ' XHW_WERTDAT';
                break;
            case self::HILFSWERT_TYP_TEXT :
            default:
                $SQL .= ' XHW_WERTTXT';
                break;
        }
        $SQL .= ' AS WERT';
        $SQL .= ' FROM Hilfswerte';
        $SQL .= ' WHERE xhw_bereich = :var_T_xhw_bereich';
        $SQL .= ' AND xhw_id = :var_T_xhw_id';
        //$SQL .= ' AND xhw_userdat >= :var_D_xhw_dat';
        $DB->SetzeBindevariable('XHW', 'var_T_xhw_bereich', $Bereich, awisDatenbank::VAR_TYP_TEXT);
        $DB->SetzeBindevariable('XHW', 'var_T_xhw_id', $ID, awisDatenbank::VAR_TYP_TEXT);
        //$DB->SetzeBindevariable('XHW', 'var_D_xhw_dat', date('d.m.Y'), awisDatenbank::VAR_TYP_DATUMZEIT);
        $rsXHW = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('XHW'));

        if (is_null($NeuerWert)) {
            return $rsXHW->FeldInhalt('WERT');
        }

        $AWISBenutzer = awisBenutzer::Init();

        if ($rsXHW->FeldInhalt('XHW_KEY') == '') {
            $Format = $Typ;
            $SQL = 'INSERT INTO HILFSWERTE(';
            switch ($Typ) {
                case self::HILFSWERT_TYP_ZAHL :
                    $SQL .= ' XHW_WERTNUM';
                    $Format = 'N8';
                    break;
                case self::HILFSWERT_TYP_DATUM :
                case self::HILFSWERT_TYP_DATUMZEIT :
                    $SQL .= ' XHW_WERTDAT';
                    $Typ = self::HILFSWERT_TYP_DATUM;
                    break;
                case self::HILFSWERT_TYP_TEXT :
                default:
                    $SQL .= ' XHW_WERTTXT';
                    break;
            }
            $SQL .= ', XHW_BEREICH, XHW_FELDTYP, XHW_ID, XHW_USER, XHW_USERDAT)';
            $SQL .= ' VALUES(';
            $SQL .= ' ' . $DB->FeldInhaltFormat($Format, $NeuerWert);
            $SQL .= ' , ' . $DB->FeldInhaltFormat('T', $Bereich);
            $SQL .= ' , ' . $DB->FeldInhaltFormat('T', $Typ);
            $SQL .= ' , ' . $DB->FeldInhaltFormat('T', $ID);
            $SQL .= ' , ' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
            $SQL .= ' ,SYSDATE)';
        } else {
            $SQL = 'UPDATE HILFSWERTE SET ';
            switch ($Typ) {
                case self::HILFSWERT_TYP_ZAHL :
                    $SQL .= ' XHW_WERTNUM=' . $DB->FeldInhaltFormat('N8', $NeuerWert);
                    break;
                case self::HILFSWERT_TYP_DATUM :
                    $SQL .= ' XHW_WERTDAT=' . $DB->FeldInhaltFormat($Typ, $NeuerWert);
                    break;
                case self::HILFSWERT_TYP_TEXT :
                default:
                    $SQL .= ' XHW_WERTTXT=' . $DB->FeldInhaltFormat($Typ, $NeuerWert);
                    break;
            }
            $SQL .= ' , XHW_USER= ' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName());
            $SQL .= ' , XHW_USERDAT = SYSDATE';
            $SQL .= ' WHERE XHW_KEY= ' . $rsXHW->FeldInhalt('XHW_KEY');
        }
        $DB->Ausfuehren($SQL, '');

        return $NeuerWert;
    }

    /**
     * Zerlegt eine Strasse nach Strasse und Hausnummer und liefer ein Array mit Strasse und Hausnummer
     *
     * @param string $Strasse
     * @return array
     */
    public static function ZerlegeStrasse($Strasse)
    {
        $Erg = Array();

        $Len = strlen($Strasse);

        // Zun�chst eine Zahl suchen
        for ($i = 0; $i < $Len; $i++) {
            if (is_numeric($Strasse[$i]) === true) {
                break;
            }
        }

        $Erg[0] = substr($Strasse, 0, $i - 1);
        $Erg[1] = substr($Strasse, $i);

        return $Erg;
    }


    /**
     * F�hrt einen Betriebssystembefehl aus.
     * R�ckgabe ist ein Array mit [Ergebnis](0=Ok, <>0 Fehler vom Befehl, FALSE, wenn der Befehl nicht ausgf�hrt werden konnte),
     * [Ausgabe] (stdout) und [Fehler] (stderr) Meldungen
     *
     * @param string $Befehl   Auszuf�hrender Befehl
     * @param string $Laufzeit Maximale Laufzeit in Sekunden
     * @return array
     */
    public function StarteBefehl($Befehl, $Laufzeit = 600)
    {
        $P = array(
            0 => array('pipe', 'r'),
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );

        $Prozess = proc_open($Befehl, $P, $pipes, null, $_ENV);

        if (is_resource($Prozess)) {
            stream_set_blocking($pipes[2], 0);
            $PInfo = proc_get_status($Prozess);
            $StartZeitpunkt = time();

            $Antwort = $Fehler = '';

            while ($PInfo['running'] === true) {
                sleep(2);
                $PInfo = proc_get_status($Prozess);

                if (time() > $StartZeitpunkt + $Laufzeit) {
                    $Erg1 = proc_terminate($Prozess, 15);

                    return array('Ergebnis' => false, 'Ausgabe' => $Antwort, 'Fehler' => 'Timeout');
                }
            }
            //var_dump($PInfo);

            $Antwort = stream_get_contents($pipes[1]);
            $Fehler = stream_get_contents($pipes[2]);

            $Erg1 = proc_close($Prozess);

            return array('Ergebnis' => $PInfo['exitcode'], 'Ausgabe' => $Antwort, 'Fehler' => $Fehler);
        }

        return array('Ergebnis' => false, 'Ausgabe' => null, 'Fehler' => 'Fehler bei Prozesserstellung');
    }

    /**
     * Konvertiert $input von/nach zu UTF8
     *
     * @param mixed $input          The Input (Array/Object/String-Mix)
     * @param bool  $b_encode           enocde or decode?
     * @param bool  $b_entity_replace   New parameter to define, whether its ok to replace entities.
     *                                  -> There is barely no reason to set this to FALSE except it does not work or takes too much time, no errors found, yet.
     * @return mixed    The de-/encoded Object-/Array-/String- value.
     */
    static function UTF8Code($input, $b_encode = TRUE, $b_entity_replace = TRUE)
    {
        if (is_string($input))
        {
            if($b_encode)
            {
                $input = utf8_encode($input);

                //return Entities to UTF8 characters
                //important for interfaces to blackbox-pages to send the correct UTF8-Characters and not Entities.
                if($b_entity_replace)
                {
                    $input = html_entity_decode($input, ENT_NOQUOTES/* | ENT_HTML5*/, 'UTF-8'); //ENT_HTML5 is a PHP 5.4 Parameter.
                }
            }
            else
            {
                //Replace NON-ISO Characters with their Entities to stop setting them to '?'-Characters.
                if($b_entity_replace)
                {
                    $input = preg_replace("/([\304-\337])([\200-\277])/e", "'&#'.((ord('\\1')-192)*64+(ord('\\2')-128)).';'", $input);
                }

                $input = utf8_decode($input);
            }
        }
        elseif (is_array($input))
        {
            foreach ($input as &$value)
            {
                $value = self::UTF8Code($value, $b_encode, $b_entity_replace);
            }
        }
        elseif (is_object($input))
        {
            $vars = array_keys(get_object_vars($input));

            if(get_class($input) == 'SimpleXMLElement')
            {
                //DOES NOT WORK!
                return '';
            }

            foreach ($vars as $var)
            {
                $input->$var = self::UTF8Code($input->$var, $b_encode, $b_entity_replace);
            }
        }

        return $input;
    }

}
