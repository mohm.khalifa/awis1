<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';
	$Form->DebugAusgabe(2,$_POST);
	
	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		$Tabelle = 'XBN';
		$Key=$_POST['txtXBV_KEY'];
		$XBNKey=$_POST['txtXBV_XBN_KEY'];
		
		$AWIS_KEY1 = $Key;

		$Felder=array();

		$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_NAME'),$_POST['txtXBN_NAME']);
		$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_VORNAME'),$_POST['txtXBN_VORNAME']);
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];

					$SQL = 'SELECT ';
					$SQL .= ' FROM ';
					$SQL .= ' WHERE ..._KEY=0'.$Key;
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'','',$AWISSprache),$rsDaten->FeldInhalt(''));
					break;
			}
		}
		else
		{
			switch($_GET['Seite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];

					$SQL = '';
					$SQL .= ' WHERE '.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$XBNKey = $rsDaten->FeldInhalt('');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('XXX',''),$rsDaten->FeldInhalt(''));
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'XBV':
				$SQL = 'DELETE FROM BenutzerVertretungen WHERE xbv_key=0'.$_POST['txtKey'];
				$AWIS_KEY1='';
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('benutzervertretung_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{

		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./benutzervertretung_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('XBNKey',$XBNKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>