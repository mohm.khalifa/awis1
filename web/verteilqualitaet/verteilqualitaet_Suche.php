<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20090220
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('WEB','%');
	$TextKonserven[]=array('WBE','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Liste','lst_AlleOffenErledigt');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_ALLE_0');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht14000=$AWISBenutzer->HatDasRecht(14000);		// Rechte des Mitarbeiters

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./verteilqualitaet_Main.php?cmdAktion=Details>");

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();

	// Filialnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['WEB']['WERBEMEDIUM'].':',190);
	$Form->Erstelle_TextFeld('*WERBEMEDIUM','',30,100,true);
	$AWISCursorPosition='sucFIL_ID';
	$Form->ZeileEnde();

	// Name der Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['DATVON'].':',190);
	$Form->Erstelle_TextFeld('*DATVON','',10,200,true,'','','','D','L','',$Form->PruefeDatum(date('d.m.Y')),10);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['WBE']['DATBIS'].':',120);
	$Form->Erstelle_TextFeld('*DATBIS','',10,200,true,'','','','D','L','',$Form->PruefeDatum(date('d.m.Y')),10);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_AlleOffenErledigt']);
	//var_dump($Daten);
	//$Form->Erstelle_SelectFeld('*Status',70,true,'','','1','','',$Daten);
	//$Form->Erstelle_SelectFeld('*Status',70,'1','','','1','','',$Daten);
	$Form->ZeileEnde();

	
	
	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht14000&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=filialinfos&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301833");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906301834");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>