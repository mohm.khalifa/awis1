<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');


try
{
	$TextKonserven = array();
	$TextKonserven[]=array('QMM','*');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','lbl_export');

	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	//var_dump ($_POST);
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	if($Recht34000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_QMM_Export'));
		//var_dump(date('d.m.Y G:i:s ',time()));

		//$Form->SchreibeHTMLCode('<form name=frmExport action=./qualitaetsmaengel_Main.php?cmdAktion=Auswertungen method=POST enctype="multipart/form-data">');
		$Form->SchreibeHTMLCode('<form name=frmExport action=./qualitaetsmaengel_exportieren.php method=POST enctype="multipart/form-data">');
		$Form->Formular_Start();
		
		// Datumsbereich festlegen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',200);
		$Form->Erstelle_TextFeld('*QMP_ERHEBDAT_VON',($Param['SPEICHERN']=='on'?$Param['QMP_VOM']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('01.01.Y'))),10);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',200);
		$Form->Erstelle_TextFeld('*QMP_ERHEBDAT_BIS',($Param['SPEICHERN']=='on'?$Param['QMP_BIS']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('31.12.Y'))),10);
		$Form->ZeileEnde();
	
		/**
		 * Filialfeld
		 **/
		
		$SQL ='select feb_key,feb_bezeichnung from v_filialebenenrollen_aktuell';
		$SQL .=' inner join Filialebenen on xx1_feb_key = feb_key';
		
		if($AWISBenutzer->BenutzerKontaktKEY() != 0 or ($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='' and ($Recht34000&2)==2) // Wenn GL
			{
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
				$Form->Erstelle_TextFeld('QMM_FIL_ID', $FilZugriff, 1,150, false, '', '', '', 'T', 'L','');
				$Form->Erstelle_HiddenFeld('QMM_FIL_ID',$FilZugriff);
				$AWISCursorPosition = 'txtQMM_FIL_ID';
				$Form->ZeileEnde();
			}
			elseif(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='' and ($Recht34000&2)==2) // Wenn Recht f�r GL aber keine Filiale
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
				$Form->Erstelle_TextFeld('*QMM_FIL_ID',($Param['SPEICHERN']=='on'?$Param['QMM_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
				$AWISCursorPosition = 'sucQMM_FIL_ID';
				$Form->ZeileEnde();
			}
			elseif(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='' and ($Recht34000&8)==8) // Wenn Admin
			{
				$SQL .=' group by feb_key,feb_bezeichnung';
				$SQL .=' order by feb_bezeichnung';
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);
				$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
				$Form->Erstelle_TextFeld('*QMM_FIL_ID',($Param['SPEICHERN']=='on'?$Param['QMM_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
				$AWISCursorPosition = 'sucQMM_FIL_ID';
				$Form->ZeileEnde();
				
				
			
			}
			elseif(($Recht34000&4)==4) // Wenn GBL oder RL oder RGZ
			{
				/*
				$SQL2 ='select xx1_fil_id,xx1_fil_id as fil_bezeichnung from v_filialebenenrollen_aktuell';
				$SQL2 .=' inner join Filialebenen on xx1_feb_key = feb_key';
				$SQL2 .=' where xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
				$SQL2 .=' group by xx1_fil_id ';
				$SQL2 .=' order by xx1_fil_id ';
				*/
				
				//Nur f�r GBL/RL/RGZ und ihre Vertreter 14.01.2015 ST
				$SQL .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
				$SQL .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'and xx1_fer_key in (23,25,57))';
				$SQL .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
				$SQL .=' and trunc(frv_datumvon) <= trunc(sysdate)';
				$SQL .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57))';
				$SQL .=' group by feb_key,feb_bezeichnung';
				$SQL .=' order by feb_bezeichnung';
				
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);
				$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',180);
			
				$Form->Erstelle_SelectFeld('*QMM_FIL_ID',$Param['QMM_FIL_ID'], 400, true, '***QMM_Daten;sucQMM_FIL_ID;Gebiet=*sucFIL_GEBIET&ZUSATZ='.$AWISSprachKonserven['Liste']['lst_ALLE_0'].'&KON='.$AWISBenutzer->BenutzerKontaktKEY(),(($Param['QMM_FIL_ID'] == '0' or $Param['SPEICHERN']=='')?$AWISSprachKonserven['Liste']['lst_ALLE_0']: $Param['QMM_FIL_ID'].'~'.$Param['QMM_FIL_ID']),'', '', '');
				//$Form->Erstelle_SelectFeld('*QMM_FIL_ID',($Param['SPEICHERN']=='on'?$Param['QMM_FIL_ID']:'0'),400,true,$SQL2,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
				$Form->ZeileEnde();
				
				//$Form->Erstelle_TextFeld('*QMM_FIL_ID',($Param['SPEICHERN']=='on'?$Param['QMM_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
				//$AWISCursorPosition = 'sucQMM_FIL_ID';
				//$Form->ZeileEnde();
				
			
				
			}
					
			/**
			 * Kriteriumsfeld
			 */
	
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_KRITERIEN'].':',200);
			
			$SQL = 'SELECT QMK_KEY,QMK_LANG'; // Alle aktiven Kriterien holen
			$SQL .= ' FROM QMKRITERIEN';
			$SQL .= ' WHERE QMK_AKTIV = 1';
			$SQL .= ' ORDER BY QMK_LANG';
				
			$Form->Erstelle_SelectFeld('*QMK_KRITERIUM',($Param['SPEICHERN']=='on'?$Param['QMM_QMK_KEY']:'0'),400,true,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			$Form->ZeileEnde();
			
			
				
			/**
			 *  Statusfeld
			 */
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_Status'].':',200);
				
			$SQL = 'SELECT QMS_KEY,QMS_BEZEICHNUNG'; // Alle Status f�r den Kopf holen
			$SQL .= ' FROM QMSTATUS';
			$SQL .= ' WHERE QMS_MASSNAHME = 0';
			$SQL .= ' ORDER BY QMS_KEY';
		
			$Form->Erstelle_SelectFeld('*QMM_STATUS',($Param['SPEICHERN']=='on'?$Param['QMM_STATUS']:'0'),400,true,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			$Form->ZeileEnde();
			
			
			// Auswahl kann gespeichert werden
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
			$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
			$Form->ZeileEnde();

		}
		else 
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['QMM']['ERR_KONTAKT']);
			$Form->ZeileEnde();
		}
		
	
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		if($AWISBenutzer->BenutzerKontaktKEY() != 0 or ($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$Form->Schaltflaeche('image', 'cmdExportXLSX', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export']);
		}
		$Form->SchaltflaechenEnde();

		$Form->SetzeCursor($AWISCursorPosition);
			
		$Form->SchreibeHTMLCode('</form>');
		
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>