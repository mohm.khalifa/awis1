<?php
require_once('awisDeployment.inc');

try {

    $Benutzer = '';
    $DebugLevel = 0;
    $Ziel = '';

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--ziel':
                $Ziel = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo '  --ignore  <Wert>                   Welche pr�fungen sollen deaktiviert werden: zeit .' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    if($Ziel!=''){
        $Deploy = new awisDeployment($Benutzer);
        $Deploy->Ziel($Ziel);
        $Deploy->DeployDDLDML();
        $Deploy->DeployMergeScripte();
    }else{
        throw new Exception('Kein Ziel angegeben',-99999);
    }


} catch (Exception $ex) {
    echo PHP_EOL . 'Datenbankdeployment: Fehler: ' . $ex->getMessage() . PHP_EOL;

    $AWISWerkzeug = new awisWerkzeuge();
    $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'), 'Datenbankdeployment-Fehler', 'Fehler :' . $ex->getMessage(), 3, '', 'awis@de.atu.eu');

    echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
    echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
    echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
    echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}

?>