<?php
//@ob_clean();

echo "<html>";
echo "<head>";
echo '<meta http-equiv="cache-control" content="no-cache">';
echo '<meta http-equiv="cache-control" content="no-store">';
echo '<meta http-equiv="cache-control" content="must-revalidate">';
echo '<meta http-equiv="cache-control" content="post-check=0, pre-check=0, false">';
echo '<meta http-equiv="pragma" content="no-cache">';
echo "<title>Awis - ATU webbasierendes Informationssystem</title>";

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen

global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;
global $PerNr;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

echo "</head>";
echo "<body>";



$RechteStufe = awisBenutzerRecht($con, 1600);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Personalverkauf',$AWISBenutzer->BenutzerName(),'','','');
   die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//awis_Debug(1,$_REQUEST);
//awis_Debug(1,$Params);


// Export aller Filiale
if(isset($_POST['cmdExport_Alle_x']))
{
	$DateiName = awis_UserExportDateiName('.csv');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
		
	$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
	
	$SQL_EXPORT=str_replace("''","'",$Params[8]);
	
	awis_Debug(1,$DateiName);
	awis_Debug(1,$SQL_EXPORT);
	
	$rsPersvkExport = awisOpenRecordset($con,$SQL_EXPORT,true,true);
	$rsPersvkExportZeilen = $awisRSZeilen;
	
	$SQL_EXPORT_UEBERSCHRIFT=str_replace("''","'",$Params[12]);
	
	//awis_Debug(1,$DateiName);
	awis_Debug(1,$SQL_EXPORT_UEBERSCHRIFT);
	
	$rsPersvkExportUeberschrift = awisOpenRecordset($con,$SQL_EXPORT_UEBERSCHRIFT,true,true);
	$rsPersvkExportUeberschriftZeilen = $awisRSZeilen;
	
	$fd = fopen($DateiName,'w+');
	if($fd==FALSE)
	{
		die($DateiName);
	}
	
	fputs($fd, "Personalkauf\n");
	
	fputs($fd, "PER-NR;Name;Menge-Gesamt;VK-Gesamt;Rabatt/Ware;Nachlass/Dienstl.;Gesamt\n");
	for($DSNr=0;$DSNr<$rsPersvkExportUeberschriftZeilen;$DSNr++)
	{
		fputs($fd,$rsPersvkExportUeberschrift['PEV_PERSONALNUMMER'][$DSNr] . ';' . $rsPersvkExportUeberschrift['PEV_KAEUFERNAME'][$DSNr] . ';' . $rsPersvkExportUeberschrift['MENGE'][$DSNr] . ';' . $rsPersvkExportUeberschrift['VK'][$DSNr] . ';' . $rsPersvkExportUeberschrift['RABATT'][$DSNr] . ';' . $rsPersvkExportUeberschrift['MANRABATT'][$DSNr] .';' . $rsPersvkExportUeberschrift['RABATTGESAMT'][$DSNr]);
		fputs($fd, "\n");
	}
	
	fputs($fd, "\n\n");
	
	fputs($fd, "Filiale;Datum;Zeit;BSA;Artikel-Nr;Menge;VK;VK-Gesamt;Rabatt;Nachlass;Umsatz;WA-NR;BON-NR\n");
	for($DSNr=0;$DSNr<$rsPersvkExportZeilen;$DSNr++)
	{
		fputs($fd,$rsPersvkExport['PEV_FIL_ID'][$DSNr] . ';' . $rsPersvkExport['PEV_KAUFDATUM'][$DSNr] . ';' . $rsPersvkExport['PEV_KAUFZEIT'][$DSNr] . ';' . $rsPersvkExport['PEV_BSA'][$DSNr] . ';' . $rsPersvkExport['PEV_AST_ATUNR'][$DSNr] . ';' . $rsPersvkExport['PEV_MENGE'][$DSNr] . ';' . $rsPersvkExport['PEV_VK'][$DSNr] . ';' . $rsPersvkExport['VK_GES'][$DSNr] . ';' . $rsPersvkExport['PEV_PERSRABATT'][$DSNr] . ';' . $rsPersvkExport['PEV_MANRABATT'][$DSNr] . ';' . $rsPersvkExport['PEV_UMSATZ'][$DSNr] . ';' . $rsPersvkExport['PEV_WANR'][$DSNr] . ';"' . $rsPersvkExport['PEV_BONNR'][$DSNr] . '"');
		fputs($fd, "\n");
	}
	fclose($fd);
	
	echo '<br><a href='.$DateiNameLink.' target="_blank">CSV Datei �ffnen</a>';
	echo '<br><br>&nbsp;<img src=/bilder/zurueck.png onclick=history.back();><br>';
}

// Export einer einzelnen Filiale
if(isset($_POST['cmdExport_Filiale_x']))
{
	$DateiName = awis_UserExportDateiName('.csv');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
		
	$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
	
	$SQL_EXPORT=str_replace("''","'",$Params[8]);
	
	awis_Debug(1,$DateiName);
	awis_Debug(1,$SQL_EXPORT);
	
	$rsPersvkExport = awisOpenRecordset($con,$SQL_EXPORT,true,true);
	$rsPersvkExportZeilen = $awisRSZeilen;		
	
	$SQL_EXPORT_UEBERSCHRIFT="SELECT personalverkaeufe.pev_personalnummer, personalverkaeufe.pev_kaeufername, ";		  
	$SQL_EXPORT_UEBERSCHRIFT.="sum(personalverkaeufe.pev_menge) AS menge, sum((personalverkaeufe.pev_vk*personalverkaeufe.pev_menge)) as vk, ";
	$SQL_EXPORT_UEBERSCHRIFT.="sum(personalverkaeufe.pev_persrabatt) as rabatt, ";
	$SQL_EXPORT_UEBERSCHRIFT.= "sum(nvl(personalverkaeufe.pev_manrabatt,0)) as manrabatt, ";
	$SQL_EXPORT_UEBERSCHRIFT.= "sum(nvl(personalverkaeufe.pev_rabatt,0)) as maschinellerrabatt, ";
	$SQL_EXPORT_UEBERSCHRIFT.= "sum(nvl(personalverkaeufe.pev_persrabatt,0)+nvl(personalverkaeufe.pev_manrabatt,0)) as rabattgesamt ";
	$SQL_EXPORT_UEBERSCHRIFT.="FROM personalverkaeufe ";
	$SQL_EXPORT_UEBERSCHRIFT.="WHERE (personalverkaeufe.pev_personalnummer=".$Params[6].") ";
	$SQL_EXPORT_UEBERSCHRIFT.="AND (personalverkaeufe.pev_fil_id=".$Params[7].") ";
	
	If($Params[0]==1)
	{	
		if($Params[5]=='')
		{
			$SQL_EXPORT_UEBERSCHRIFT.="AND personalverkaeufe.pev_kaufdatum=to_date('".$Params[4]."','DD.MM.RR') ";
		}
		else
		{
			$SQL_EXPORT_UEBERSCHRIFT.="AND (personalverkaeufe.pev_kaufdatum>=to_date('".$Params[4]."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('".$Params[5]."','DD.MM.RR')) ";
		}
	}
	
	If($Params[9]!='')
	{
		$SucheArtbez = $Params[9];
		
	   	if(substr($Params[9],-1,1)!='*' AND substr($Params[9],-1,1)!='%')
    	{
    		//$Params[9] .='%';
    		$SucheArtbez .='%';
    	}
    	
    	if(substr($Params[9],1,1)!='*' AND substr($Params[9],1,1)!='%')
    	{
    		$SucheArtbez = '%'.$SucheArtbez;
    	}
    	
		$SQL_EXPORT_UEBERSCHRIFT.="AND ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($SucheArtbez,TRUE,FALSE,FALSE,0).")"; 		
	}
	
	If($Params[11]!='')
	{
		$SQL_EXPORT_UEBERSCHRIFT.="AND ( upper(personalverkaeufe.pev_ast_atunr) ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0).")"; 		
	}
	
	$SQL_EXPORT_UEBERSCHRIFT.="GROUP BY personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername";			

	//awis_Debug(1,$DateiName);
	awis_Debug(1,$SQL_EXPORT_UEBERSCHRIFT);
	
	$rsPersvkExportUeberschrift = awisOpenRecordset($con,$SQL_EXPORT_UEBERSCHRIFT,true,true);
	$rsPersvkExportUeberschriftZeilen = $awisRSZeilen;
	
	$fd = fopen($DateiName,'w');
	if($fd==FALSE)
	{
		die($DateiName);
	}
	
	fputs($fd, "Personalkauf\n");
	
	fputs($fd, "PER-NR;Name;Menge-Gesamt;VK-Gesamt;Rabatt/Ware;Nachlass/Dienstl.;Gesamt\n");
	for($DSNr=0;$DSNr<$rsPersvkExportUeberschriftZeilen;$DSNr++)
	{
		fputs($fd,$rsPersvkExportUeberschrift['PEV_PERSONALNUMMER'][$DSNr] . ';' . $rsPersvkExportUeberschrift['PEV_KAEUFERNAME'][$DSNr] . ';' . $rsPersvkExportUeberschrift['MENGE'][$DSNr] . ';' . $rsPersvkExportUeberschrift['VK'][$DSNr] . ';' . $rsPersvkExportUeberschrift['RABATT'][$DSNr] . ';' . $rsPersvkExportUeberschrift['MANRABATT'][$DSNr] . ';' . $rsPersvkExportUeberschrift['RABATTGESAMT'][$DSNr]);
		fputs($fd, "\n");
	}
	
	fputs($fd, "\n\n");
	
	awis_Debug(1,$rsPersvkExportZeilen);
	fputs($fd, "Filiale;Datum;Zeit;BSA;Artikel-Nr;Menge;VK;VK-Gesamt;Rabatt;Nachlass;Umsatz;WA-NR;BON-NR\n");
	for($DSNr=0; $DSNr < $rsPersvkExportZeilen; $DSNr++)
	{
		fputs($fd,$rsPersvkExport['FIL_ID'][$DSNr] . ';' . $rsPersvkExport['PEV_KAUFDATUM'][$DSNr] . ';' . $rsPersvkExport['PEV_KAUFZEIT'][$DSNr] . ';' . $rsPersvkExport['PEV_BSA'][$DSNr] . ';' . $rsPersvkExport['PEV_AST_ATUNR'][$DSNr] . ';' . $rsPersvkExport['PEV_MENGE'][$DSNr] . ';' . $rsPersvkExport['PEV_VK'][$DSNr] . ';' . $rsPersvkExport['VK_GES'][$DSNr] . ';' . $rsPersvkExport['PEV_PERSRABATT'][$DSNr] . ';' . $rsPersvkExport['PEV_MANRABATT'][$DSNr] . ';' . $rsPersvkExport['PEV_UMSATZ'][$DSNr] . ';' . $rsPersvkExport['PEV_WANR'][$DSNr] . ';"' . $rsPersvkExport['PEV_BONNR'][$DSNr] . '"');
		fputs($fd, "\n");
	}
	fclose($fd);
	
	echo '<br><a href='.$DateiNameLink.' target="_blank">CSV Datei �ffnen</a>';
	echo '<br><br>&nbsp;<img src=/bilder/zurueck.png onclick=history.back();><br>';
}


function check_date($date)
{
	$matches='';
    $format = '/^(\d{1,2})\.(\d{1,2})\.(\d{2})$/';
	if(preg_match($format, $date, $matches)) {
        return checkdate($matches[2], $matches[1], $matches[3]);
    }
    else return false;
}

$PerNr='';

if(isset($_REQUEST['txtPERNR']))
{
	$PerNr=$_REQUEST['txtPERNR'];
}

$FilId='';

if(isset($_REQUEST['txtFILID']))
{	
	$FilId=$_REQUEST['txtFILID'];
}

$RowId='';

if(isset($_REQUEST['Edit']))
{	
	$RowId=$_REQUEST['Edit'];
}

$anzeigen=false;

if(isset($_POST['cmdSpeichern_x'])) //Speichern mit Fallunterschied ob INSERT oder UPDATE
{
	$SQL="SELECT * FROM PERSONALVERKAEUFE WHERE rowid=chartorowid('".$_REQUEST['txtROWID']."')";
	$rsPersvk = awisOpenRecordset($con,$SQL);
	$rsPersvkZeilen = $awisRSZeilen;
	
	$SQL='';
	$txtHinweis='';
	
	if($rsPersvkZeilen==0)		// Keine Daten
	{
			awislogoff($con);
			die("<span class=HinweisText>Datensatz wurde gel�scht!</span>");
	}
	
	if(isset($_REQUEST['txtPEV_Ausgeschieden']))
	{	
		$SQL.=", PEV_AUSGESCHIEDEN=1";
	}
	else
	{
		$SQL.=", PEV_AUSGESCHIEDEN=0";
	}
	
	if(isset($_REQUEST['txtPEV_Fehler']))
	{	
		$SQL.=", PEV_FEHLER=1";
	}
	else
	{
		$SQL.=", PEV_FEHLER=0";
	}
	
	if(isset($_REQUEST['txtPEV_Kontrolliert']))
	{	
		$SQL.=", PEV_KONTROLLIERT=1, PEV_KONTROLLDATUM=SYSDATE";
	}
	else
	{
		/*if($_REQUEST['txtPEV_KONTROLLDATUM']!='')
		{
			if(check_date($_REQUEST['txtPEV_KONTROLLDATUM'])
			{
				$SQL.=", PEV_KONTROLLIERT=1, PEV_KONTROLLDATUM=to_date('".$_REQUEST['txtPEV_KONTROLLDATUM']."','DD.MM.YY')";
			}
			else
			{
				$SQL.=", PEV_KONTROLLIERT=1, PEV_KONTROLLDATUM=SYSDATE";	
			}
				
		}
		else
		{*/
			$SQL.=", PEV_KONTROLLIERT=0, PEV_KONTROLLDATUM=''";
		/*}*/
	}
	
	if(is_numeric($_POST['txtPEV_Menge']))
	{
		if($_POST['txtPEV_Menge'] != $_POST['txtPEV_Menge_old'])
		{
				if($rsPersvk['PEV_MENGE'][0] != $_POST['txtPEV_Menge_old'])
				{
						$txtHinweis.=',Menge von '.$_POST['txtPEV_Menge_old']. ' auf '.$rsPersvk['PEV_MENGE'][0];
				}
				else
				{
						$SQL.=',PEV_MENGE='.$_POST['txtPEV_Menge'];
				}
		}
	}
	else
	{
			$txtHinweis.=',Eingabe Menge : '.$_POST['txtPEV_Menge']. ' ist keine numerische Zahl';
	}
	
	if(awis_format($_POST['txtPEV_VK'],'Standardzahl') != $_POST['txtPEV_VK_old'])
	{
			if($rsPersvk['PEV_VK'][0] != $_POST['txtPEV_VK_old'])
			{
					$txtHinweis.=',VK von '.$_POST['txtPEV_VK_old']. ' auf '.$rsPersvk['PEV_VK'][0];
			}
			else
			{
					$SQL.=',PEV_VK='.str_replace(",",".",awis_format($_POST['txtPEV_VK'],'Standardzahl'));
			}
	}
	
	if(awis_format($_POST['txtPEV_Persrabatt'],'Standardzahl') != $_POST['txtPEV_Persrabatt_old'])
	{
			if($rsPersvk['PEV_PERSRABATT'][0] != $_POST['txtPEV_Persrabatt_old'])
			{
					$txtHinweis.=',Personalrabatt von '.$_POST['txtPEV_Persrabatt_old']. ' auf '.$rsPersvk['PEV_PERSRABATT'][0];
			}
			else
			{
					$SQL.=',PEV_PERSRABATT='.str_replace(",",".",awis_format($_POST['txtPEV_Persrabatt'],'Standardzahl'));
			}
	}
	
	if(strlen($_POST['txtPEV_Sonderbestellung'])==1 || strlen($_POST['txtPEV_Sonderbestellung']==0))
	{
		if($_POST['txtPEV_Sonderbestellung'] != $_POST['txtPEV_Sonderbestellung_old'])
		{
				if($rsPersvk['PEV_SONDERBESTELLUNG'][0] != $_POST['txtPEV_Sonderbestellung_old'])
				{
						$txtHinweis.=',Sonderbestellung von '.$_POST['txtPEV_Sonderbestellung_old']. ' auf '.$rsPersvk['PEV_Sonderbestellung'][0];
				}
				else
				{
						$SQL.=",PEV_Sonderbestellung='".$_POST['txtPEV_Sonderbestellung']."'";
				}
		}
	}
	else
	{
			$txtHinweis.=',Eingabe Sonderbestellung : '.$_POST['txtPEV_Sonderbestellung']. ' ist l�nger als 1 Zeichen';
	}
	
	if($_POST['txtPEV_BEMERKUNG'] != urldecode($_POST['txtPEV_BEMERKUNG_old']))
	{
		if($rsPersvk['PEV_BEMERKUNG'][0] != urldecode($_POST['txtPEV_BEMERKUNG_old']))
		{
				$txtHinweis.=',BEMERKUNG von '.$_POST['txtPEV_BEMERKUNG_old']. ' auf '.$rsPersvk['PEV_BEMERKUNG'][0];
		}
		else
		{
				$SQL.=',PEV_BEMERKUNG=\''.substr($_POST['txtPEV_BEMERKUNG'],0,1000).'\'';
		}
	}
	
	if($txtHinweis=='' && $SQL!='')
	{
			$SQL.=',PEV_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL.=',PEV_USERDAT=SYSDATE';
			$SQL="UPDATE PERSONALVERKAEUFE SET " .substr($SQL,1)." WHERE ROWID=chartorowid('".$_REQUEST['txtROWID']."')";
			
			if(awisExecute($con, $SQL)===FALSE)
			{
				awisErrorMailLink("persvk_Detail.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}
			
			$anzeigen=true;
	}
	elseif($txtHinweis!='')
	{
			echo $txtHinweis;
			awislogoff($con);
			die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsPersvk['PEV_USER'][0] ." ge�ndert !</span>");
	}
	
}
elseif($RowId!='')
{
	$SQL="select * from personalverkaeufe where rowid=chartorowid('".$RowId."')";
	
	$rsPersvk = awisOpenRecordset($con,$SQL);
	$rsPersvkZeilen = $awisRSZeilen;
	
	if($rsPersvkZeilen==0)		// Keine Daten
	{
		echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
	}
	else
	{
		$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
		
		echo '<form name=frmDetail method=post action=./persvk_Main.php?cmdAktion=Detail>';
		
		echo "<table  width=100% id=DatenTabelle border=1><tr>";
		echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>Filiale</td>";
		echo "<td id=TabellenZeileGrau width=80%><a title='Filial-Details anzeigen' href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=" . $rsPersvk['PEV_FIL_ID'][0] . "&Zurueck=../personalverkauf/persvk_Main.php?cmdAktion=Detail~~1~~Edit=".$RowId.">" . $rsPersvk['PEV_FIL_ID'][0] . "</a></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>Kaufdatum</td>";
		echo "<td id=TabellenZeileGrau width=80%>".$rsPersvk['PEV_KAUFDATUM'][0]." ".substr($rsPersvk['PEV_KAUFZEIT'][0],0,2) . ':' . substr($rsPersvk['PEV_KAUFZEIT'][0],2,2) . ':' . substr($rsPersvk['PEV_KAUFZEIT'][0],4,2) . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>BSA</td>";
		echo "<td id=TabellenZeileGrau width=80%>".$rsPersvk['PEV_BSA'][0]."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>WA - NR</td>";
		echo "<td id=TabellenZeileGrau width=80%>".$rsPersvk['PEV_WANR'][0]."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>BON - NR</td>";
		echo "<td id=TabellenZeileGrau width=80%>".$rsPersvk['PEV_BONNR'][0]."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>BON - Position</td>";
		echo "<td id=TabellenZeileGrau width=80%>".$rsPersvk['PEV_POS'][0]."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>Mitarbeiter</td>";
		echo "<td id=TabellenZeileGrau width=80%>".$rsPersvk['PEV_PERSONALNUMMER'][0]." - ".$rsPersvk['PEV_KAEUFERNAME'][0]."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>Artikel-Nr</td>";
		echo "<td id=TabellenZeileGrau width=80%><a title='Artikel-Details anzeigen' href=../ATUArtikel/artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=" . $rsPersvk['PEV_AST_ATUNR'][0] . "&Zurueck=../personalverkauf/persvk_Main.php?cmdAktion=Detail~~1~~Edit=".$RowId.">" . $rsPersvk['PEV_AST_ATUNR'][0] . "</a></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>Artikel-Bezeichnung</td>";
		echo "<td id=TabellenZeileGrau width=80%>".$rsPersvk['PEV_ARTIKELBEZEICHNUNG'][0]."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Menge</td>";
		echo "<td id=TabellenZeileGrau><input name=txtPEV_Menge size=10 value='" . $rsPersvk['PEV_MENGE'][0] . "'><input type=hidden name=txtPEV_Menge_old  value='" . $rsPersvk['PEV_MENGE'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Verkaufspreis</td>";
		echo "<td id=TabellenZeileGrau><input name=txtPEV_VK size=10 value='" . awis_format($rsPersvk['PEV_VK'][0],'Currency') . "'><input type=hidden name=txtPEV_VK_old  value='" . $rsPersvk['PEV_VK'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Personalrabatt</td>";
		echo "<td id=TabellenZeileGrau><input name=txtPEV_Persrabatt size=10 value='" . awis_format($rsPersvk['PEV_PERSRABATT'][0],'Currency') . "'><input type=hidden name=txtPEV_Persrabatt_old  value='" . $rsPersvk['PEV_PERSRABATT'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Manueller Nachlass</td>";
		echo "<td id=TabellenZeileGrau width=80%>". awis_format($rsPersvk['PEV_MANRABATT'][0],'Currency')."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Umsatz</td>";
		echo "<td id=TabellenZeileGrau width=80%>". awis_format($rsPersvk['PEV_UMSATZ'][0],'Currency')."</td>";
		echo "</tr>";		
		echo "<tr>";
		echo "<td id=FeldBez>Sonderbestellung</td>";
		echo "<td id=TabellenZeileGrau><input name=txtPEV_Sonderbestellung size=1 value='" . $rsPersvk['PEV_SONDERBESTELLUNG'][0] . "'><input type=hidden name=txtPEV_Sonderbestellung_old  value='" . $rsPersvk['PEV_SONDERBESTELLUNG'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Ausgeschieden</td>";
		echo "<td id=TabellenZeileGrau><input type=checkbox name=txtPEV_Ausgeschieden " . ($rsPersvk['PEV_AUSGESCHIEDEN'][0]!=0?"checked='checked'":"") . "><input type=hidden name=txtPEV_Ausgeschieden_old  value='" . $rsPersvk['PEV_AUSGESCHIEDEN'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Fehler</td>";
		echo "<td id=TabellenZeileGrau><input type=checkbox name=txtPEV_Fehler " . ($rsPersvk['PEV_FEHLER'][0]!=0?"checked='checked'":"") . "><input type=hidden name=txtPEV_Fehler_old  value='" . $rsPersvk['PEV_FEHLER'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Kontrolliert</td>";
		echo "<td id=TabellenZeileGrau><input type=checkbox name=txtPEV_Kontrolliert " . ($rsPersvk['PEV_KONTROLLIERT'][0]!=0||$rsPersvk['PEV_KONTROLLDATUM'][0]!=''?"checked='checked'":"") . "><input type=hidden name=txtPEV_Kontrolliert_old  value='" . $rsPersvk['PEV_KONTROLLIERT'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Kontrolldatum</td>";
		echo "<td id=TabellenZeileGrau><input name=txtPEV_Kontrolldatum value='" . $rsPersvk['PEV_KONTROLLDATUM'][0] . "'><input type=hidden name=txtPEV_Kontrolldatum_old  value='" . $rsPersvk['PEV_KONTROLLDATUM'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez valign=top>Bemerkung</td>";
		echo "<td id=TabellenZeileGrau><textarea name=txtPEV_BEMERKUNG cols=50 rows=5>" . $rsPersvk['PEV_BEMERKUNG'][0] . "</textarea></td><input type=hidden name=txtPEV_BEMERKUNG_old  value='" . $rsPersvk['PEV_BEMERKUNG'][0] . "'></td>";
		echo "</tr>";
		echo "<tr>";
			echo "<td colspan=2 align=left>Erstellt von User " . $rsPersvk['PEV_USER'][0] . " am " . $rsPersvk['PEV_USERDAT'][0] . "</td>";
		echo "</tr>";
		echo "</table>";
		
		echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
		echo "<input type=hidden name=txtPERNR value=".$Params[6].">";
		echo "<input type=hidden name=txtFILID value=".$Params[7].">";
		echo "<input type=hidden name=txtROWID value=".$RowId.">";
		echo "</form>";
	}
	
}
if($PerNr!='' || isset($_POST['cmdAnzeigenAlle_x']) || isset($_POST['cmdAnzeigenFilialen_x']) || $anzeigen)
{
	$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
	
	//Alle Artikel suchen, die �fter als 10 mal im Jahr gekauft wurden
	$SQL = "SELECT pev_ast_atunr, TO_CHAR(pev_kaufdatum, 'YYYY'), sum(pev_menge) ";
	$SQL.= "FROM personalverkaeufe ";
	$SQL.= "WHERE pev_personalnummer=".($PerNr==''?$Params[6]:$PerNr);
	$SQL.= "GROUP BY pev_ast_atunr, TO_CHAR(pev_kaufdatum, 'YYYY') ";
	$SQL.= "HAVING sum(pev_menge) > 10 ";
	
	$rsPersvkArtikelsuche = awisOpenRecordset($con,$SQL,true,true);
	$rsPersvkArtikelsucheZeilen = $awisRSZeilen;
	
	echo "<form name=frmNummern method=post action=./persvk_Main.php?cmdAktion=Suche>";
		echo "<input align=right type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'>";
	echo "</form>";		

	echo '<form name=frmDetail method=post action=./persvk_Main.php?cmdAktion=Detail>';
	
	$SQL="SELECT personalverkaeufe.pev_personalnummer, personalverkaeufe.pev_kaeufername, ";		  
	$SQL.="sum(personalverkaeufe.pev_menge) AS menge, sum((personalverkaeufe.pev_vk*personalverkaeufe.pev_menge)) as vk, ";
	$SQL.= "sum(personalverkaeufe.pev_persrabatt) as rabatt, sum(nvl(personalverkaeufe.pev_manrabatt,0)) as manrabatt, ";
	$SQL.= "sum(nvl(personalverkaeufe.pev_rabatt,0)) as maschinellerrabatt, ";
	$SQL.= "sum(nvl(personalverkaeufe.pev_persrabatt,0)+nvl(personalverkaeufe.pev_manrabatt,0)) as rabattgesamt ";
	$SQL.="FROM personalverkaeufe ";
	$SQL.="WHERE (personalverkaeufe.pev_personalnummer=".($PerNr==''?$Params[6]:$PerNr).") ";
	
	If($Params[0]==1)
	{	
		if($Params[5]=='')
		{
			$SQL.="AND personalverkaeufe.pev_kaufdatum=to_date('".$Params[4]."','DD.MM.RR') ";
		}
		else
		{
			$SQL.="AND (personalverkaeufe.pev_kaufdatum>=to_date('".$Params[4]."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('".$Params[5]."','DD.MM.RR')) ";
		}
	}
	
	If($Params[9]!='')
	{
		$SucheArtbez = $Params[9];
		
	   	if(substr($Params[9],-1,1)!='*' AND substr($Params[9],-1,1)!='%')
    	{
    		//$Params[9] .='%';
    		$SucheArtbez .='%';
    	}
    	
    	if(substr($Params[9],1,1)!='*' AND substr($Params[9],1,1)!='%')
    	{
    		$SucheArtbez = '%'.$SucheArtbez;
    	}
    	
		$SQL.="AND ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($SucheArtbez,TRUE,FALSE,FALSE,0).")"; 		
	}
	
	If($Params[11]!='')
	{
		$SQL.="AND ( upper(personalverkaeufe.pev_ast_atunr) ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0).")"; 		
	}
	
	If($Params[3]!='')
	{
		$SQL.="AND ( personalverkaeufe.pev_fil_id=". $Params[3] .") "; 		
	}
	
	
	$SQL.="GROUP BY personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername";
	
	//awis_Debug('entwick',0,false,$SQL);
	//awis_Debug('entwick',0,false,$Params);
	
	$rsPersvksuche = awisOpenRecordset($con,$SQL,true,true);
	$rsPersvksucheZeilen = $awisRSZeilen;
	
	// �berschrift aufbauen
	echo "<table id=DatenTabelle border=1><tr>";					
	echo "<td id=FeldBez>Personal-Nummer</td>";
	echo "<td id=FeldBez>Personal-K&auml;ufername</td>";
	echo "<td id=FeldBez>Menge-Gesamt</td>";
	echo "<td id=FeldBez>VK-Gesamt</td>";
	echo "<td id=FeldBez>Rabatt</td>";
	echo "<td id=FeldBez>Nachlass</td>";
	echo "<td id=FeldBez>Rabatt-Gesamt</td>";
		
	echo '</tr>';
		
	for($i=0;$i<$rsPersvksucheZeilen;$i++)
	{	
		echo '<tr>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_PERSONALNUMMER'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_KAEUFERNAME'][$i] . '</td>';
		echo '<td align=right ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['MENGE'][$i] . '</td>';
		echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["VK"][$i],'Currency') . "</td>";
		echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["RABATT"][$i],'Currency') . "</td>";
		echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["MANRABATT"][$i],'Currency') . "</td>";
		echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["RABATTGESAMT"][$i],'Currency') . "</td>";
		echo '</tr>';
	}
	print "</table>";
	
	$Params[12]="";
	//$Params[7]=str_replace("'","''",$Params[7]);
	$Params[12]=str_replace("'","''",$SQL);
	awis_Debug(1,$Params);
	awis_BenutzerParameterSpeichern($con, "Persvk_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));		
	
	echo "<br><input type=image title='Alle Anzeigen' src=/bilder/sc_lupe.png name=cmdAnzeigenAlle>";
	echo "&nbsp;<input type=image title='Nach Filialen anzeigen' src=/bilder/tabelle.png name=cmdAnzeigenFilialen>";
	echo "</form>";
	
	/*******************************************
	Ausgabe aller Datens�tze pro Personalmnummer
	*******************************************/
	
	if(isset($_POST['cmdAnzeigenAlle_x']) || (isset($_REQUEST['All']) && $_REQUEST['All']==TRUE))
	{
		$SQL="SELECT rowidtochar(rowid) as ID, personalverkaeufe.pev_fil_id, personalverkaeufe.pev_kaufdatum, personalverkaeufe.pev_kaufzeit, personalverkaeufe.pev_bsa,personalverkaeufe.pev_ast_atunr, ";
		$SQL.="personalverkaeufe.pev_menge, personalverkaeufe.pev_vk, (personalverkaeufe.pev_menge*personalverkaeufe.pev_vk) AS VK_GES , personalverkaeufe.pev_persrabatt, ";
		$SQL.="personalverkaeufe.pev_ausgeschieden, personalverkaeufe.pev_kontrolliert, personalverkaeufe.pev_kontrolldatum, ";
		$SQL.="personalverkaeufe.pev_fehler, personalverkaeufe.pev_sonderbestellung, personalverkaeufe.pev_pos, personalverkaeufe.pev_artikelbezeichnung, ";
		$SQL.="personalverkaeufe.pev_manrabatt, personalverkaeufe.pev_wanr, personalverkaeufe.pev_bonnr, ";
		$SQL.="personalverkaeufe.pev_umsatz, personalverkaeufe.pev_rabatt ";
		$SQL.="FROM personalverkaeufe ";
		$SQL.="WHERE (personalverkaeufe.pev_personalnummer=".$Params[6].") ";
		
		If($Params[0]==1)
		{	
			if($Params[5]=='')
			{
				$SQL.="AND personalverkaeufe.pev_kaufdatum=to_date('".$Params[4]."','DD.MM.RR') ";
			}
			else
			{
				$SQL.="AND (personalverkaeufe.pev_kaufdatum>=to_date('".$Params[4]."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('".$Params[5]."','DD.MM.RR')) ";
			}
		}
		
		If($Params[9]!='')
		{
			$SucheArtbez = $Params[9];
		
	   		if(substr($Params[9],-1,1)!='*' AND substr($Params[9],-1,1)!='%')
    		{
    			//$Params[9] .='%';
    			$SucheArtbez .='%';
    		}
    		
    		if(substr($Params[9],1,1)!='*' AND substr($Params[9],1,1)!='%')
    		{	
    			$SucheArtbez = '%'.$SucheArtbez;
    		}
    	
			$SQL.="AND ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($SucheArtbez,TRUE,FALSE,FALSE,0).")"; 						
		}

		If($Params[11]!='')
		{
			$SQL.="AND ( upper(personalverkaeufe.pev_ast_atunr) ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0).")"; 		
		}
		
		If($Params[3]!='')
		{
			$SQL.="AND ( personalverkaeufe.pev_fil_id=". $Params[3] .") "; 		
		}	
		
		$SQL.="ORDER BY personalverkaeufe.pev_kaufdatum ASC, personalverkaeufe.pev_kaufzeit ASC, personalverkaeufe.pev_pos ASC, personalverkaeufe.pev_fil_id ASC";
		
		$rsPersvkdetail = awisOpenRecordset($con,$SQL,true,true);
		$rsPersvkdetailZeilen = $awisRSZeilen;
			
		//awis_Debug('entwick',0,false,$SQL);
		
		$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
					
		$rsPersvksuche = awisOpenRecordset($con,$SQL,true,true);
		$rsPersvksucheZeilen = $awisRSZeilen;
		
echo '<form name=frmDetail method=post action=./persvk_Main.php?cmdAktion=Detail>';		
		echo "<table width=100% id=DatenTabelle border=1><tr>";			
		echo "<td id=FeldBez>Filiale</td>";
		echo "<td id=FeldBez>Datum</td>";
		echo "<td id=FeldBez>Zeit</td>";
		//echo "<td id=FeldBez>BSA</td>";
		echo "<td id=FeldBez>Artikel-Nr</td>";
		echo "<td id=FeldBez>Artikel-Bezeichnung</td>";
		echo "<td id=FeldBez>Menge</td>";
		echo "<td id=FeldBez>VK</td>";
		echo "<td id=FeldBez>VK-Gesamt</td>";
		echo "<td id=FeldBez>Rabatt</td>";
		echo "<td id=FeldBez>Nachlass</td>";
		echo "<td id=FeldBez>Umsatz</td>";
		echo "<td id=FeldBez>WA-NR</td>";
		echo "<td id=FeldBez>BON-NR</td>";
		
		//echo "<td id=FeldBez>Ausgeschieden</td>";
		//echo "<td id=FeldBez>Kontrolliert</td>";
		//echo "<td id=FeldBez>Kontrolldatum</td>";
		//echo "<td id=FeldBez>Fehler</td>";
		//echo "<td id=FeldBez>Sonderbestellung</td>";
		//echo "<td id=FeldBez>Position</td>";
		//echo "<td id=FeldBez><a title=\'Daten exportieren\' href=../personalverkauf/persvk_Main.php?cmdAktion=Detail&Export=Gesamt&PersNr=".$Params[6]."><img border=0 src=/bilder/diagramm.png name=cmdExport title='Daten Export'></td>";
		echo "<td id=FeldBez><input type=image src=/bilder/diagramm.png name=cmdExport_Alle title='Daten Export'>";
		$PDF_Link='./persvk_pdf.php?Export=Alle';
		echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a></td>";
		echo '</tr>';
echo '</form>';
		
		for($a=0;$a<$rsPersvksucheZeilen;$a++)
		{	
			//if($a<$AwisBenuPara)
			//{	
				echo '<tr>';
				echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_FIL_ID'][$a] . '</td>';
				echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_KAUFDATUM'][$a] . '</td>';
				echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . substr($rsPersvksuche['PEV_KAUFZEIT'][$a],0,2) . ':' . substr($rsPersvksuche['PEV_KAUFZEIT'][$a],2,2) . ':' . substr($rsPersvksuche['PEV_KAUFZEIT'][$a],4,2) . '</td>';
				//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_BSA'][$a] . '</td>';
				echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title=\'Artikel-Details anzeigen\' href=../ATUArtikel/artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $rsPersvksuche['PEV_AST_ATUNR'][$a] . '&Zurueck=../personalverkauf/persvk_Main.php?cmdAktion=Detail~~1~~txtPERNR='.($PerNr==''?$Params[6]:$PerNr).'~~1~~All=TRUE>' . $rsPersvkdetail['PEV_AST_ATUNR'][$a] . '</a></td>';
				
				//Pr�fen, ober der Artikel �fter als 10 x im Jahr gekauft wurde
				$MERKATUNR = "";
				
				if ($rsPersvkArtikelsucheZeilen > 0)
				{
					for($i=0; $i<$rsPersvkArtikelsucheZeilen;$i++)
					{
						if($rsPersvkArtikelsuche['PEV_AST_ATUNR'][$i] == $rsPersvksuche['PEV_AST_ATUNR'][$a])
						{
							$MERKATUNR = $rsPersvksuche['PEV_AST_ATUNR'][$a];
						}
					}
				}
				
				if($MERKATUNR == $rsPersvksuche['PEV_AST_ATUNR'][$a])
				{
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><font color=red>' . $rsPersvksuche['PEV_ARTIKELBEZEICHNUNG'][$a] . '</td></font>';
				}
				else 
				{
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_ARTIKELBEZEICHNUNG'][$a] . '</td>';
				}
				//awis_Debug(1,substr($rsPersvksuche['PEV_MENGE'][$a],0,1));
				//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_ARTIKELBEZEICHNUNG'][$a] . '</td>';
				echo '<td align=right ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_MENGE'][$a] . '</td>';
				echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["PEV_VK"][$a],'Currency') . "</td>";
				echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["VK_GES"][$a],'Currency') . "</td>";
				echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["PEV_PERSRABATT"][$a],'Currency') . "</td>";
				echo "<td align=right " . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . awis_format($rsPersvkdetail["PEV_MANRABATT"][$a],'Currency') . "</td>";
				echo "<td align=right " . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . awis_format($rsPersvkdetail["PEV_UMSATZ"][$a],'Currency') . "</td>";
				echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_WANR'][$a] . '</td>';
				echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_BONNR'][$a] . '</td>';
				//if($rsPersvksuche['PEV_KONTROLLIERT'][$a]>0||$rsPersvksuche['PEV_KONTROLLDATUM'][$a]!='')
				//{
				//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><img border=0 src=/bilder/button_ok_trans.png name=OK_Button title="OK"></td>';
					//echo '<td id=TabellenZeileGruen>' . $rsPersvkdetail['PEV_KONTROLLIERT'][$a] . '</td>';
				//}
				//else
				//{
				//	echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'></td>';
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_KONTROLLIERT'][$a] . '</td>';
				//}
				//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_KONTROLLDATUM'][$a] . '</td>';
				//if($rsPersvksuche['PEV_FEHLER'][$a]>0&&$rsPersvksuche['PEV_KONTROLLIERT'][$a]==0)
				//{
				//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><img border=0 src=/bilder/button_cancel_trans.png name=OK_Button title="OK"></td>';
					//echo '<td id=TabellenZeileGelb>' . $rsPersvkdetail['PEV_FEHLER'][$a] . '</td>';
				//}
				//elseif($rsPersvksuche['PEV_FEHLER'][$a]>0&&$rsPersvksuche['PEV_KONTROLLIERT'][$a]>0)
				//{
				//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><img border=0 src=/bilder/button_ok_trans.png name=OK_Button title="OK"></td>';
				//}
				//else
				//{
				//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'></td>';
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_FEHLER'][$a] . '</td>';
				//}
				//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_SONDERBESTELLUNG'][$a] . '</td>';
				//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_POS'][$a] . '</td>';
				echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title=\'Bearbeiten\' href=../personalverkauf/persvk_Main.php?cmdAktion=Detail&Edit='. urlencode($rsPersvkdetail['ID'][$a]) .'><img border=0 src=/bilder/aendern.png name=cmdAnzeigen title="Anzeigen"></a></td>';
				echo '</tr>';
			//}
		}
		print "</table>";
		
		
		
		//if($a<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
		//{
			echo '<font size=2>Es wurden ' . $a . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
		//}
		//else
		//{
		//	echo '<font size=2>Es wurden ' . $a . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
		//}

		$Params[8]="";
		$Params[7]=str_replace("'","''",$Params[7]);
		$Params[8]=str_replace("'","''",$SQL);
		awis_Debug(1,$Params);
		awis_BenutzerParameterSpeichern($con, "Persvk_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));		
	}
	/*******************************************
	Ausgabe aller Datens�tze pro Filiale
	*******************************************/
	else
	{
	$SQL="SELECT personalverkaeufe.pev_fil_id, personalverkaeufe.pev_personalnummer, ";
	$SQL.="personalverkaeufe.pev_kaeufername, ";		  
	$SQL.="sum(personalverkaeufe.pev_menge) AS menge, ";
	$SQL.="sum((personalverkaeufe.pev_vk*personalverkaeufe.pev_menge)) as vk, ";	
	$SQL.= "sum(personalverkaeufe.pev_persrabatt) as rabatt, ";
	$SQL.= "sum(nvl(personalverkaeufe.pev_manrabatt,0)) as manrabatt, ";
	$SQL.= "sum(nvl(personalverkaeufe.pev_rabatt,0)) as maschinellerrabatt, ";
	$SQL.= "sum(nvl(personalverkaeufe.pev_persrabatt,0)+nvl(personalverkaeufe.pev_manrabatt,0)) as rabattgesamt ";
	$SQL.="FROM personalverkaeufe ";
	$SQL.="WHERE (personalverkaeufe.pev_personalnummer=".($PerNr==''?$Params[6]:$PerNr).") ";
	
	If($Params[0]==1)
	{	
		if($Params[5]=='')
		{
			$SQL.="AND personalverkaeufe.pev_kaufdatum=to_date('".$Params[4]."','DD.MM.RR') ";
		}
		else
		{
			$SQL.="AND (personalverkaeufe.pev_kaufdatum>=to_date('".$Params[4]."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('".$Params[5]."','DD.MM.RR')) ";
		}
	}
	
	If($Params[9]!='')
	{
		$SucheArtbez = $Params[9];
		
	   	if(substr($Params[9],-1,1)!='*' AND substr($Params[9],-1,1)!='%')
    	{
    		//$Params[9] .='%';
    		$SucheArtbez .='%';
    	}
    	
    	if(substr($Params[9],1,1)!='*' AND substr($Params[9],1,1)!='%')
    	{
    		$SucheArtbez = '%'.$SucheArtbez;
    	}
    	
		$SQL.="AND ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($SucheArtbez,TRUE,FALSE,FALSE,0).")"; 		
	}
	
	If($Params[11]!='')
	{
		$SQL.="AND ( upper(personalverkaeufe.pev_ast_atunr) ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0).")"; 		
	}
	
	If($Params[3]!='')
	{
		$SQL.="AND ( personalverkaeufe.pev_fil_id=". $Params[3] .") "; 		
	}
	
	
	$SQL.="GROUP BY personalverkaeufe.pev_fil_id, personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername ";
	$SQL.="ORDER BY personalverkaeufe.pev_fil_id ASC";
		
	//awis_Debug('entwick',0,false,$SQL);
	
	$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
				
	$rsPersvksuche = awisOpenRecordset($con,$SQL,true,true);
	$rsPersvksucheZeilen = $awisRSZeilen;
	
	// �berschrift aufbauen
	echo "<table width=100% id=DatenTabelle border=1><tr>";
	echo "<td id=FeldBez></td>";
	echo "<td id=FeldBez>Filial-Nummer</td>";					
	echo "<td id=FeldBez>Personal-Nummer</td>";
	echo "<td id=FeldBez>Personal-K&auml;ufername</td>";
	echo "<td id=FeldBez>Menge-Gesamt</td>";
	echo "<td id=FeldBez>VK-Gesamt</td>";
	echo "<td id=FeldBez>Rabatt</td>";
	echo "<td id=FeldBez>Nachlass</td>";
	echo "<td id=FeldBez>Rabatt-Gesamt</td>";
	echo '</tr>';
		
	for($i=0;$i<$rsPersvksucheZeilen;$i++)
	{	
		//if($i<$AwisBenuPara)
		//{
			echo '<tr>';
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=./persvk_Main.php?cmdAktion=Detail&txtPERNR=' . ($PerNr==''?$Params[6]:$PerNr) . '&txtFILID='.$rsPersvksuche['PEV_FIL_ID'][$i].'><img border=0 src=/bilder/info.png name=cmdAnzeigen title="Anzeigen"></a></td>';
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title=\'Filial-Details anzeigen\' href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsPersvksuche['PEV_FIL_ID'][$i] . '&Zurueck=../personalverkauf/persvk_Main.php?cmdAktion=Detail~~1~~txtPERNR='.$PerNr.'>' . $rsPersvksuche['PEV_FIL_ID'][$i] . '</a></td>';
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_PERSONALNUMMER'][$i] . '</td>';
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_KAEUFERNAME'][$i] . '</td>';
			echo '<td align=right ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['MENGE'][$i] . '</td>';
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["VK"][$i],'Currency') . "</td>";
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["RABATT"][$i],'Currency') . "</td>";
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["MANRABATT"][$i],'Currency') . "</td>";
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["RABATTGESAMT"][$i],'Currency') . "</td>";
			echo '</tr>';
		//}
		
		if($FilId!='' && $rsPersvksuche['PEV_FIL_ID'][$i]==$FilId)
		{	
			$SQL="SELECT rowidtochar(rowid) as ID, ".$FilId." as FIL_ID,personalverkaeufe.pev_kaufdatum, personalverkaeufe.pev_kaufzeit, personalverkaeufe.pev_bsa,personalverkaeufe.pev_ast_atunr, ";
			$SQL.="personalverkaeufe.pev_menge, personalverkaeufe.pev_vk, (personalverkaeufe.pev_menge*personalverkaeufe.pev_vk) AS VK_GES , personalverkaeufe.pev_persrabatt, ";
			$SQL.="personalverkaeufe.pev_ausgeschieden, personalverkaeufe.pev_kontrolliert, personalverkaeufe.pev_kontrolldatum, ";
			$SQL.="personalverkaeufe.pev_fehler, personalverkaeufe.pev_sonderbestellung, personalverkaeufe.pev_pos, personalverkaeufe.pev_artikelbezeichnung, ";
			$SQL.="personalverkaeufe.pev_manrabatt, personalverkaeufe.pev_wanr, personalverkaeufe.pev_bonnr, ";
			$SQL.="personalverkaeufe.pev_umsatz, personalverkaeufe.pev_rabatt, personalverkaeufe.pev_bausatz ";
			$SQL.="FROM personalverkaeufe ";
			$SQL.="WHERE (personalverkaeufe.pev_personalnummer=".$PerNr.") and (personalverkaeufe.pev_fil_id=".$FilId.") ";
			
			If($Params[0]==1)
			{	
				if($Params[5]=='')
				{
					$SQL.="AND personalverkaeufe.pev_kaufdatum=to_date('".$Params[4]."','DD.MM.RR') ";
				}
				else
				{
					$SQL.="AND (personalverkaeufe.pev_kaufdatum>=to_date('".$Params[4]."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('".$Params[5]."','DD.MM.RR')) ";
				}
			}
			
			If($Params[9]!='')
			{
				$SucheArtbez = $Params[9];
				
			   	if(substr($Params[9],-1,1)!='*' AND substr($Params[9],-1,1)!='%')
		    	{
		    		//$Params[9] .='%';
		    		$SucheArtbez .='%';
		    	}
		    	
		    	if(substr($Params[9],1,1)!='*' AND substr($Params[9],1,1)!='%')
    			{
    				$SucheArtbez = '%'.$SucheArtbez;
    			}
		    	
				$SQL.="AND ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($SucheArtbez,TRUE,FALSE,FALSE,0).")"; 		
			}
			
			If($Params[11]!='')
			{
				$SQL.="AND ( upper(personalverkaeufe.pev_ast_atunr) ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0).")"; 		
			}
			
			$SQL.="ORDER BY personalverkaeufe.pev_kaufdatum ASC, personalverkaeufe.pev_kaufzeit ASC, personalverkaeufe.pev_pos ASC";
			
			$rsPersvkdetail = awisOpenRecordset($con,$SQL,true,true);
			$rsPersvkdetailZeilen = $awisRSZeilen;
			
echo '<form name=frmDetail method=post action=./persvk_Main.php?cmdAktion=Detail>';		
			echo "<table width=100% id=DatenTabelle border=1><tr>";			
			echo "<td id=FeldBez>Datum</td>";
			echo "<td id=FeldBez>Zeit</td>";
			//echo "<td id=FeldBez>BSA</td>";
			echo "<td id=FeldBez>Artikel-Nr</td>";
			echo "<td id=FeldBez>Artikel-Bezeichnung</td>";
			echo "<td id=FeldBez>Menge</td>";
			echo "<td id=FeldBez>VK</td>";
			echo "<td id=FeldBez>VK-Gesamt</td>";
			echo "<td id=FeldBez>Rabatt</td>";
			echo "<td id=FeldBez>Nachlass</td>";
			echo "<td id=FeldBez>Umsatz</td>";
			echo "<td id=FeldBez>WA-NR</td>";
			echo "<td id=FeldBez>BON-NR</td>";
						
			//echo "<td id=FeldBez>Ausgeschieden</td>";
			//echo "<td id=FeldBez>Kontrolliert</td>";
			//echo "<td id=FeldBez>Kontrolldatum</td>";
			//echo "<td id=FeldBez>Fehler</td>";
			//echo "<td id=FeldBez>Sonderbestellung</td>";
			//echo "<td id=FeldBez>Position</td>";
			echo "<td id=FeldBez><input type=image src=/bilder/diagramm.png name=cmdExport_Filiale title='Daten Export'>";
			$PDF_Link='./persvk_pdf.php?Export=Filiale';
			echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a></td>";
			echo '</tr>';
echo '</form>';
			
			for($a=0;$a<$rsPersvkdetailZeilen;$a++)
			{	
				//if($a<$AwisBenuPara)
				//{	
					echo '<tr>';
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_KAUFDATUM'][$a] . '</td>';
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . substr($rsPersvkdetail['PEV_KAUFZEIT'][$a],0,2) . ':' . substr($rsPersvkdetail['PEV_KAUFZEIT'][$a],2,2) . ':' . substr($rsPersvkdetail['PEV_KAUFZEIT'][$a],4,2) . '</td>';
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_BSA'][$a] . '</td>';								
					
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title=\'Artikel-Details anzeigen\' href=../ATUArtikel/artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $rsPersvkdetail['PEV_AST_ATUNR'][$a] . '&Zurueck=../personalverkauf/persvk_Main.php?cmdAktion=Detail~~1~~txtPERNR='.$PerNr.'~~1~~txtFILID='.$FilId.'>' . $rsPersvkdetail['PEV_AST_ATUNR'][$a] . '</a></td>';
									
					
					//Pr�fen, ober der Artikel �fter als 10 x im Jahr gekauft wurde
					$MERKATUNR = "";
										
					if ($rsPersvkArtikelsucheZeilen > 0)
					{			
						for($i=0;$i<$rsPersvkArtikelsucheZeilen;$i++)
						{
							if($rsPersvkArtikelsuche['PEV_AST_ATUNR'][$i] == $rsPersvkdetail['PEV_AST_ATUNR'][$a])
							{
								$MERKATUNR = $rsPersvkdetail['PEV_AST_ATUNR'][$a];
							}
						}
					}
										
					$Bausatz='';
					if ($rsPersvkdetail['PEV_BAUSATZ'][$a] != '')
					{
						$Bausatz = $rsPersvkdetail['PEV_BAUSATZ'][$a];
					}
					
					if($MERKATUNR == $rsPersvkdetail['PEV_AST_ATUNR'][$a])
					{
						echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><font color=red>' . ($Bausatz!=''?'<font color = blue>('.$Bausatz.')</font> ':'').$rsPersvkdetail['PEV_ARTIKELBEZEICHNUNG'][$a] . '</td></font>';
					}
					else 
					{
						echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . ($Bausatz!=''?'<font color=blue>('.$Bausatz.')</font> ':'').$rsPersvkdetail['PEV_ARTIKELBEZEICHNUNG'][$a] . '</td>';
					}				
					
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_ARTIKELBEZEICHNUNG'][$a] . '</td>';
					echo '<td align=right ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_MENGE'][$a] . '</td>';
					
					//TR 03.04.08
					//echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (substr($rsPersvkdetail['PEV_MENGE'][$a],0,1)<>'-'?awis_format($rsPersvkdetail["PEV_VK"][$a],'Currency'):"0") . "</td>";
					//echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . (substr($rsPersvkdetail['PEV_MENGE'][$a],0,1)<>'-'?awis_format($rsPersvkdetail["VK_GES"][$a],'Currency'):"0") . "</td>";
					
					echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvkdetail["PEV_VK"][$a],'Currency') . "</td>";
					echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvkdetail["VK_GES"][$a],'Currency'). "</td>";
					
					echo "<td align=right " . (($a%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvkdetail["PEV_PERSRABATT"][$a],'Currency') . "</td>";
					echo "<td align=right " . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . awis_format($rsPersvkdetail["PEV_MANRABATT"][$a],'Currency') . "</td>";
					echo "<td align=right " . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . awis_format($rsPersvkdetail["PEV_UMSATZ"][$a],'Currency') . "</td>";
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_WANR'][$a] . '</td>';
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_BONNR'][$a] . '</td>';
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_AUSGESCHIEDEN'][$a] . '</td>';
					//if($rsPersvkdetail['PEV_KONTROLLIERT'][$a]>0||$rsPersvkdetail['PEV_KONTROLLDATUM'][$a]!='')
					//{
					//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><img border=0 src=/bilder/button_ok_trans.png name=OK_Button title="OK"></td>';
						//echo '<td id=TabellenZeileGruen>' . $rsPersvkdetail['PEV_KONTROLLIERT'][$a] . '</td>';
					//}
					//else
					//{
					//	echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'></td>';
						//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_KONTROLLIERT'][$a] . '</td>';
					//}
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_KONTROLLDATUM'][$a] . '</td>';
					//if($rsPersvkdetail['PEV_FEHLER'][$a]>0&&$rsPersvkdetail['PEV_KONTROLLIERT'][$a]==0)
					//{
					//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><img border=0 src=/bilder/button_cancel_trans.png name=OK_Button title="OK"></td>';
						//echo '<td id=TabellenZeileGelb>' . $rsPersvkdetail['PEV_FEHLER'][$a] . '</td>';
					//}
					//elseif($rsPersvkdetail['PEV_FEHLER'][$a]>0&&$rsPersvkdetail['PEV_KONTROLLIERT'][$a]>0)
					//{
					//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><img border=0 src=/bilder/button_ok_trans.png name=OK_Button title="OK"></td>';
					//}
					//else
					//{
					//	echo '<td align=center ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'></td>';
						//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_FEHLER'][$a] . '</td>';
					//}
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_SONDERBESTELLUNG'][$a] . '</td>';
					//echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvkdetail['PEV_POS'][$a] . '</td>';
					echo '<td ' . (($a%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a title=\'Bearbeiten\' href=../personalverkauf/persvk_Main.php?cmdAktion=Detail&Edit='. urlencode($rsPersvkdetail['ID'][$a]) .'><img border=0 src=/bilder/aendern.png name=cmdAnzeigen title="Anzeigen"></a></td>';
					echo '</tr>';
				//}
			}
	
			print "</table>";
			
			//if($a<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
			//{
				echo '<font size=2>Es wurden ' . $a . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
			//}
			//else
			//{
			//	echo '<font size=2>Es wurden ' . $a . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
			//}
			
			if($i+1<$rsPersvksucheZeilen)
			{
			echo "<table width=100% id=DatenTabelle border=1><tr>";
			echo "<td id=FeldBez></td>";
			echo "<td id=FeldBez>Filial-Nummer</td>";					
			echo "<td id=FeldBez>Personal-Nummer</td>";
			echo "<td id=FeldBez>Personal-K&auml;ufername</td>";
			echo "<td id=FeldBez>Menge-Gesamt</td>";
			echo "<td id=FeldBez>VK-Gesamt</td>";
			echo "<td id=FeldBez>Rabatt</td>";
			echo "<td id=FeldBez>Nachlass</td>";
			echo "<td id=FeldBez>Rabatt-Gesamt</td>";	
			echo '</tr>';
			}
		}
	}
	print "</table>";
	
	if($PerNr!='')
	{
		$Params[6]=$PerNr;
	}
	if($FilId!='')
	{
		$Params[7]=$FilId;
	}
	
	$Params[8]="";
	$Params[7]=str_replace("'","''",$Params[7]);
	$Params[8]=str_replace("'","''",$SQL);
	awis_Debug(1,$Params);
	awis_BenutzerParameterSpeichern($con, "Persvk_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
	}
	
	echo "<hr>";
	
	//if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
	//{
		echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
	//}
	//else
	//{
	//	echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
	//}
	
}
//else
//{
//	die("<span class=HinweisText>Kein Suchkriterium angegeben!</span>");
//}

echo "</body>";
echo "</html>";
?>