<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $DB;
global $AWISSprache;
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try 
{
    require_once('awisDatenbank.inc');
    require_once('awisFormular.inc');
    
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('AZG','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht3001 = $AWISBenutzer->HatDasRecht(3001);

    if($Recht3001==0)
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
    }
    
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_AdressenZugriffsGruppe'));
    $Form->DebugAusgabe(1,$Param);

    //********************************************************
    // Parameter ?
    //********************************************************
    if(isset($_POST['cmdSuche_x']))
    {
        $Param = array();
        $Param['NAME'] = $_POST['sucSuchName'];		// 1
        $Param['KONTAKT'] = $_POST['sucSuchKontakt'];		// 2
        $Param['AZG_AZG_KEY'] = $_POST['sucAZG_AZG_KEY'];		// 3
        $Param['AZG_ORT'] = $_POST['sucAZG_ORT'];			// 4
        
        // Allgemeine Parameter
        $Param['KEY']='';
        $Param['WHERE']='';
        $Param['ORDER']='';
        $Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
    }
    elseif(isset($_POST['cmdDSZurueck_x']))
    {
        $AWIS_KEY1 = $_POST['txtAZG_KEY'];
    
        $SQL = 'SELECT AZG_KEY FROM (SELECT AZG_KEY ';
        $SQL .= ' FROM AdressenZugriffsGruppen';
        $SQL .= ' INNER JOIN KontakteAbteilungen ON AZG_KAB_KEY = KAB_KEY';
        $SQL .= ' INNER JOIN KontakteAbteilungenZuordnungen ON KZA_KAB_KEY = KAB_KEY';
        $SQL .= ' WHERE (KZA_KON_KEY = 0'.$AWISBenutzer->BenutzerKontaktKEY() . ')';
        $SQL .= ' AND AZG_KEY <> 1';
        $SQL .= ' ORDER BY AZG_BEZEICHNUNG DESC';
        $SQL .= ') WHERE ROWNUM = 1';

        $rsAZG = $DB->RecordSetOeffnen($SQL);
        $Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);
        if(!$rsAZG->EOF())
        {
            $AWIS_KEY1=$rsAZG->FeldInhalt('AZG_KEY');
        }
    }
    elseif(isset($_POST['cmdDSWeiter_x']))
    {
        $AWIS_KEY1 = $_POST['txtAZG_KEY'];
        $SQL = 'SELECT AZG_KEY FROM (SELECT AZG_KEY ';
        $SQL .= ' FROM AdressenZugriffsGruppen';
        $SQL .= ' INNER JOIN KontakteAbteilungen ON AZG_KAB_KEY = KAB_KEY';
        $SQL .= ' INNER JOIN KontakteAbteilungenZuordnungen ON KZA_KAB_KEY = KAB_KEY';
        $SQL .= ' WHERE (KZA_KON_KEY = 0'.$AWISBenutzer->BenutzerKontaktKEY() . ')';
        $SQL .= ' AND AZG_KEY <> 1';
        $SQL .= ' ORDER BY AZG_BEZEICHNUNG ASC';
        $SQL .= ') WHERE ROWNUM = 1';
        
        $rsAZG = $DB->RecordSetOeffnen($SQL);
        $Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);
        if(!$rsAZG->EOF())
        {
            $AWIS_KEY1=$rsAZG->FeldInhalt('AZG_KEY');
        }
    }
    elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
        include('./adressen_zugriffsrechte_loeschen.php');
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('./adressen_zugriffsrechte_speichern.php');
    }
    elseif(isset($_POST['cmdDSNeu_x']))
    {
		$AWIS_KEY1 = $Param['KEY']='';
        $Param['WHERE']='';
        $Param['ORDER']='';
    }
    elseif(isset($_GET['AZG_KEY']))
    {
		$AWIS_KEY1 = $Param['KEY']=(int)$_GET['AZG_KEY'];
        $Param['WHERE']='';
        $Param['ORDER']='';
    }
    elseif(isset($_POST['txtAZG_KEY']))
    {
		$AWIS_KEY1 = $Param['KEY']=(int)$_POST['txtAZG_KEY'];
        $Param['WHERE']='';
        $Param['ORDER']='';
    }
    else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
    {
    	if(!isset($Param['KEY']))
        {
            $AWIS_KEY1 = $Param['KEY']='';
            $Param['WHERE']='';
            $Param['ORDER']='';
        }
        
        if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
        {
            $Param['KEY']=0;
        }
        $AWIS_KEY1=$Param['KEY'];
        $Form->DebugAusgabe(1,$Param,$AWIS_KEY1);
    }
    
    //*********************************************************
    //* Sortierung
    //*********************************************************
    if(!isset($_GET['Sort']))
    {
        if($Param['ORDER']!='')
        {
            $ORDERBY = 'ORDER BY '.$Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' ORDER BY AZG_BEZEICHNUNG DESC';
            $Param['ORDER']='AZG_BEZEICHNUNG DESC';
        }
    }
    else
    {
        $Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
        $ORDERBY = ' ORDER BY '.$Param['ORDER'];
    }
    
    //********************************************************
    // Daten suchen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);
    
    $SQL = 'SELECT DISTINCT AdressenZugriffsGruppen.*';
    $SQL .= ' FROM AdressenZugriffsGruppen';
    $SQL .= ' INNER JOIN KontakteAbteilungen ON AZG_KAB_KEY = KAB_KEY';
    $SQL .= ' INNER JOIN KontakteAbteilungenZuordnungen ON KZA_KAB_KEY = KAB_KEY';
    $SQL .= ' WHERE  AZG_KEY <> 1';
    if(($Recht3001&16)==0)
    {
        $SQL .= ' AND (KZA_KON_KEY = 0'.$AWISBenutzer->BenutzerKontaktKEY() . ')';
    }
    
    if($Bedingung!='')
    {
        $SQL .= $Bedingung;
    }
    
    if(!isset($_GET['Sort']))
    {
        $SQL .= ' ORDER BY AZG_Bezeichnung';
    }
    else
    {
        $SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
    }

    // Zeilen begrenzen
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $rsAZG = $DB->RecordSetOeffnen($SQL);
    $rsAZGZeilen = $rsAZG->AnzahlDatensaetze();
   
    //********************************************************
    // Daten anzeigen
    //********************************************************
    if($rsAZGZeilen>1)						// Liste anzeigen
    {
        echo '<form name=frmAdressenZugriffsGruppen action=./adressen_Main.php?cmdAktion=Zugriffsrechte method=POST  enctype="multipart/form-data">';
    
        $Form->Formular_Start();
        $Param['KEY']=0;
    
        $Form->ZeileStart();
        $Link = './adressen_Main.php?cmdAktion=Zugriffsrechte'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=AZG_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AZG_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AZG']['AZG_BEZEICHNUNG'],350,'',$Link);
        $Link = './adressen_Main.php?cmdAktion=Zugriffsrechte'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=AZG_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AZG_BEMERKUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AZG']['AZG_BEMERKUNG'],550,'',$Link);
        $Form->ZeileEnde();
    
        // Blockweise
        $StartZeile=0;
        if(isset($_GET['Block']))
        {
            $StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
        }
        // Seitenweises blaettern
        if($rsAZGZeilen>$MaxDSAnzahl)
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');
    
            for($i=0;$i<($rsAZGZeilen/$MaxDSAnzahl);$i++)
            {
                if($i!=($StartZeile/$MaxDSAnzahl))
                {
    				$Text = '&nbsp;<a href=./adressen_Main.php?cmdAktion=Zugriffsrechte&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
                    $Form->Erstelle_TextLabel($Text,30,'');
                }
                else
                {
                    $Text = '&nbsp;'.($i+1).'';
                    $Form->Erstelle_TextLabel($Text,30,'');
                }
            }
            $Form->ZeileEnde();
        }
        for($AZGZeile=$StartZeile;$AZGZeile<$rsAZGZeilen and $AZGZeile<$StartZeile+$MaxDSAnzahl;$AZGZeile++)
	    {
		      $Form->ZeileStart();
    		  $Link = './adressen_Main.php?cmdAktion=Zugriffsrechte&AZG_KEY='.$rsAZG->FeldInhalt('AZG_KEY').'';
    		  $Form->Erstelle_ListenFeld('AZG_BEZEICHNUNG',$rsAZG->FeldInhalt('AZG_BEZEICHNUNG'),0,350,false,($AZGZeile%2),'',$Link);
    		  $Form->Erstelle_ListenFeld('AZG_BEMERKUNG',$rsAZG->FeldInhalt('AZG_BEMERKUNG'),0,550,false,($AZGZeile%2),'',$Link);
    		  $Form->ZeileEnde();
    		  $rsAZG->DSWeiter();
        }
        $Form->Formular_Ende();
    
    //***************************************
    // Schaltflaechen fuer dieses Register
    //***************************************
        $Form->SchaltflaechenStart();
        
        if(($Recht3001&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
    	{
    	    $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }
        $Form->SchaltflaechenEnde();
        echo '</form>';
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
	    $Form->Formular_Start();
	    echo '<form name=frmAdressenZugriffsGruppen action=./adressen_Main.php?cmdAktion=Zugriffsrechte method=POST  enctype="multipart/form-data">';
	    $Param['KEY']= $AWIS_KEY1 = $rsAZG->FeldInhalt('AZG_KEY')!=''?$rsAZG->FeldInhalt('AZG_KEY'):0;
	    $AWISBenutzer->ParameterSchreiben("AktuelleZugriffsGruppe" , $AWIS_KEY1);
	    echo '<input type=hidden name=txtAZG_KEY value='.$AWIS_KEY1. '>';
	    
	    $Form->Formular_Start();
	    $OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	    
	    // Infozeile zusammenbauen
	    $Felder = array();
	    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Zugriffsrechte&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAZG->FeldInhalt('AZG_USER')));
	    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsAZG->FeldInhalt('AZG_USERDAT')));
	    $Form->InfoZeile($Felder,'');
	    
	    $EditRecht=(($Recht3001&2)!=0);
	        
	    if($AWIS_KEY1==0)
	    {
	       $EditRecht=true;
	    }
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['AZG']['AZG_BEZEICHNUNG'].':',150);
	    $Form->Erstelle_TextFeld('AZG_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsAZG->FeldInhalt('AZG_BEZEICHNUNG')),50,350,$EditRecht);
	    $CursorFeld='txtAZG_BEZEICHNUNG';
	    $Form->ZeileEnde();
	    
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['AZG']['AZG_KAB_KEY'].':',150);
	    $SQL = ' SELECT DISTINCT KAB_KEY, KAB_ABTEILUNG';
	    $SQL .= ' FROM KontakteAbteilungen';
	    $SQL .= ' INNER JOIN KontakteAbteilungenZuordnungen ON KZA_KAB_KEY = KAB_KEY';
	        	   
	    if(($Recht3001&16)==0)
	    {
	      $SQL .= ' WHERE (KZA_KON_KEY = 0'.$AWISBenutzer->BenutzerKontaktKEY() . ' OR KAB_KEY = 0'.$rsAZG->FeldInhalt('AZG_KAB_KEY')?$rsAZG->FeldInhalt('AZG_KAB_KEY'):''.')';
	    }
	    $SQL .= ' ORDER BY KAB_ABTEILUNG';
	    $Form->Erstelle_SelectFeld('AZG_KAB_KEY',($AWIS_KEY1===0?'':$rsAZG->FeldInhalt('AZG_KAB_KEY')),350,$EditRecht,$SQL);
	    $Form->ZeileEnde();
	    
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['AZG']['AZG_BEMERKUNG'].':',150);
	    $Form->Erstelle_TextFeld('AZG_BEMERKUNG',($AWIS_KEY1===0?'':$rsAZG->FeldInhalt('AZG_BEMERKUNG')),50,350,$EditRecht);
	    $Form->ZeileEnde();
	    
	    $Form->Formular_Ende();
	    
	    if(!isset($_POST['cmdDSNeu_x']))
	    {
	      $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Mitglieder'));
	       //		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';
	      
	      $Reg = new awisRegister(3003);
	      $Reg->ZeichneRegister($RegisterSeite);
	    }
	    //***************************************
	    // Schaltflaechen fuer dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	    if(($Recht3001&6)!=0)		//
	    {
	        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	    }
	    if(($Recht3001&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzufuegen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	    if(($Recht3001&8)==8 AND !isset($_POST['cmdDSNeu_x']))
	    {
	        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	    }
	    if(!isset($_POST['cmdDSNeu_x']))
	    {
	        $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
			$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
	    }
	    $Form->SchaltflaechenEnde();
	    echo '</form>';
	}

	$AWISBenutzer->ParameterSchreiben('Formular_AdressenZugriffsGruppe',serialize($Param));
	$Form->SetzeCursor($AWISCursorPosition);
    $Form->DebugAusgabe(1,$Param);
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWISBenutzer;
    global $AWISCursorPosition;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $DB;
	
	$Bedingung = '';
	
	if($AWIS_KEY1!=0)
	{
	    $Bedingung.= ' AND AZG_KEY = '. floatval($AWIS_KEY1);
	    return $Bedingung;
	}
	if(isset($Param['NAME']) AND $Param['NAME']!='')			// Name
	{
		$Bedingung .= 'AND (UPPER(AZG_Bezeichnung) ' . $DB->LIKEoderIST($Param['NAME'].'%',1) . ' ';
		$Bedingung .= ')';
	}
	return $Bedingung;
}