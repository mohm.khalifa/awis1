<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZSS','%');
	$TextKonserven[]=array('AST','AST_ATUNR');
	$TextKonserven[]=array('ZSZ','ZSZ_SORTIMENT');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Datum%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10065 = $AWISBenutzer->HatDasRecht(10065);
	if($Recht10065==0)
	{
	    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	echo "<br>";
	echo "<form name=frmSuche method=post action=./zukaufsortimentesperren_Main.php?cmdAktion=Details>";

	/**********************************************
	* Eingabemaske
	***********************************************/

	$Form->Formular_Start();


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_ZLH_KEY'].':',190);
	$SQL = 'SELECT ZLH_KEY, ZLH_KUERZEL || \' - \' || ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*ZSS_ZLH_KEY','',200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$AWISCursorPosition='sucZSS_ARTIKELNUMMER';
	
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_SORTIMENT'].':',190);
	$SQL = 'SELECT DISTINCT ZSZ_SORTIMENT,ZSZ_SORTIMENT AS ANZEIGE FROM ZUKAUFSORTIMENTZUORDNUNGEN';
	$SQL .= ' ORDER BY ZSZ_SORTIMENT';
	$Form->Erstelle_SelectFeld('*ZSS_SORTIMENT','',200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht10065&4) == 4)		// Hinzufügen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>