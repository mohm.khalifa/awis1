<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=WIN1252">
        <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
        <meta http-equiv="author" content="ATU">
        <?php
        require_once('awisDatenbank.inc');
        require_once('awisBenutzer.inc');
        require_once('awisFormular.inc');
        include_once('awisFirstglasImport.inc.php');

        global $AWISCursorPosition;
        global $AWIS_KEY1;
        global $AWIS_KEY2;


        try {
        // Textkonserven laden
            $TextKonserven = array();
            $TextKonserven[]=array('FFR','%');
            $TextKonserven[]=array('FIL','%');
            $TextKonserven[]=array('Wort','lbl_weiter');
            $TextKonserven[]=array('Wort','lbl_speichern');
            $TextKonserven[]=array('Wort','lbl_trefferliste');
            $TextKonserven[]=array('Wort','lbl_aendern');
            $TextKonserven[]=array('Wort','lbl_hinzufuegen');
            $TextKonserven[]=array('Wort','lbl_loeschen');
            $TextKonserven[]=array('Wort','lbl_zurueck');
            $TextKonserven[]=array('Wort','lbl_DSZurueck');
            $TextKonserven[]=array('Wort','lbl_DSWeiter');
            $TextKonserven[]=array('Wort','lbl_Hilfe');
            $TextKonserven[]=array('Wort','lbl_hilfe');
            $TextKonserven[]=array('Wort','lbl_suche');
            $TextKonserven[]=array('Wort','Seite');
            $TextKonserven[]=array('Wort','txt_BitteWaehlen');
            $TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
            $TextKonserven[]=array('Fehler','err_keineRechte');
            $TextKonserven[]=array('Fehler','err_keineDaten');
            $TextKonserven[]=array('Wort','Status');
            $TextKonserven[]=array('Wort','AlleAnzeigen');
            $TextKonserven[]=array('Wort','lbl_RueckfuehrungsscheinDrucken');
            $TextKonserven[]=array('Wort','lbl_RueckfuehrungsdifferenzenDrucken');
            $TextKonserven[]=array('Wort','lbl_RueckfuehrungslisteDrucken');
            $TextKonserven[]=array('Wort','KeineBerechtigungRueckfuehrung');
            $TextKonserven[]=array('Wort','AlleAnzeigen');
            $TextKonserven[]=array('Wort','Abschliessen');
            $TextKonserven[]=array('Wort','Abbrechen');
            $TextKonserven[]=array('Wort','Abschlussdatum');
            $TextKonserven[]=array('RFS','lst_RFS_STATUS');
            $TextKonserven[]=array('Wort','Text');
            $TextKonserven[]=array('Wort','FFR_UNBEARBEITET');
            $TextKonserven[]=array('Wort','FFR_BEARBEITET');

            $Form = new awisFormular();
            $AWISBenutzer = awisBenutzer::Init();
            $DB = awisDatenbank::NeueVerbindung('AWIS');
            $DB->Oeffnen();

            $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

            $Recht9100 = $AWISBenutzer->HatDasRecht(9100);
            if($Recht9100==0) {
                $Form->Fehler_KeineRechte();
            }

            $Form->DebugAusgabe(1,$_POST);
            //$Form->DebugAusgabe(1,$_REQUEST);

            $AWIS_KEY1 = -1;
            $FLAG_DETAIL = 0;

            $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./firstglas_Main.php?cmdAktion=Differenzen>");

            $Form->Formular_Start();

            if(!isset($_POST['suc_FILID']))
            {
                $Param['FIL_ID'] ='*';
            }
            else
            {
                $Param['FIL_ID'] = $_POST['suc_FILID'];
            }
            $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasRueckfuehrung'));

            //var_dump($Param);

            if(isset($_GET['FFR_KEY'])) {
                $AWIS_KEY1 = $_GET['FFR_KEY'];
            }
            elseif(isset($_REQUEST['txtFFR_KEY'])) {
                $AWIS_KEY1 = $_REQUEST['txtFFR_KEY'];
            }

            if(isset($_POST['cmdSpeichern_x'])) {
                include './firstglas_rueckfuehrung_speichern.php';
            }

            if(!isset($_GET['Sort'])) {
                $ORDERBY = ' ORDER BY FFR_DIFFERENZDATUM ASC';
            }
            else {
                $ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
            }

            $Param['FIL_ID']=(isset($_POST['sucFIL_ID'])?$_POST['sucFIL_ID']:$Param['FIL_ID']);
            $Param['ORDERBY']=$ORDERBY;
            $Param['BLOCK']=(isset($_REQUEST['Block'])?$_REQUEST['Block']:1);
            $Form->DebugAusgabe(1,$Param);



            $FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

            if($FilZugriff!='') {
                if(strpos($FilZugriff,',')!==false) {
                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',240);
                    $SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
                    $SQL .= ' FROM Filialen ';
                    $SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
                    $SQL .= ' ORDER BY FIL_BEZ';
                    $Form->Erstelle_SelectFeld('*FIL_ID',$Param['FIL_ID'],150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
                    $Form->ZeileEnde();
                    $AWISCursorPosition='sucFIL_ID';
                }
                elseif($Param['FIL_ID']=='') {
                    $Param['FIL_ID']=$FilZugriff;
                }
            }
            else {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',240);
                $Form->Erstelle_TextFeld('*FIL_ID',$Param['FIL_ID'],10,180,true,'','','','T','L','','',10);
                $AWISCursorPosition='sucFIL_ID';
                $Form->ZeileEnde();


                //VorgangNr
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'].':',240);
                //$Form->Erstelle_TextFeld('*FFR_VORGANGNR',$Param['VorgangNr'],10,180,true,'','','','T','L','','',10);
                $Form->Erstelle_TextFeld('*FFR_VORGANGNR','',10,250,true,'','','','T','L','','',13);
                $Form->ZeileEnde();


            //$Form->ZeileEnde();
            //$AWISCursorPosition='sucFIL_ID';

            }
            //Alle anzeigen mit (*)

            if(@$Param['FIL_ID']=='*' && @$_POST['sucFFR_VORGANGNR'] == "" && @!isset($_GET['FFR_KEY'])) {

                $SQL = 'SELECT FGRUECKFUEHRUNG.*';
                $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                $SQL .= ' FROM FGRUECKFUEHRUNG';
                $SQL .= ' WHERE FFR_STATUS <> 2';
                $SQL .= ' '.$ORDERBY;

                $Block = 1;

                //echo $SQL;

            /*
            if($AWIS_KEY1<=0) {
                 // Zum Bl�ttern in den Daten
                //echo "Bl�tter - Funktion";
            $Block = 1;
            if(isset($_REQUEST['Block'])) {
                $Block=$Form->Format('N0',$_REQUEST['Block'],false);
                $Param['Block']=$Block;
                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));
            }
            elseif(isset($Param['Block'])) {
                $Block=$Param['Block'];
            }

            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

            $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
            echo $SQL;
        }
        else {
            $MaxDS = 1;
            $ZeilenProSeite=1;
            $Block = 1;
        }
        */
                if(isset($_REQUEST['Block'])) {
                    $Block=$Form->Format('N0',$_REQUEST['Block'],false);
                    $Param['BLOCK']=$Block;
                    $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));
                }
                elseif(isset($Param['BLOCK'])) {
                    $Block=$Param['BLOCK'];
                }

                $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

                $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
                $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
                $SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

                $rsFFR = $DB->RecordSetOeffnen($SQL);
                //$Form->DebugAusgabe(1,$rsFFR);
                        
                if($rsFFR->EOF()) {
                     print "Keine Datensaetze gefunden";
                }
                else
                {
                    $Form->ZeileStart();
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_KEY'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KEY'],60,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_FILID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_FILID'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_FILID'],130,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_WANR'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_WANR'],100,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_AEMBETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_AEMBETRAG'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_VORGANGNR'],200,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_VORGANGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_VORGANGNR'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'],150,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_KASSENBETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_KASSENBETRAG'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'],120,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_DIFFERENZDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_DIFFERENZDATUM'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZDATUM'],120,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_DIFFERENZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_DIFFERENZ'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'],100,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_STATUS'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_STATUS'],100,'',$Link);

                    $Form->ZeileEnde();

                    $DS=0;
                    while(!$rsFFR->EOF()) {
                        $Form->ZeileStart();
                        $Link = './firstglas_Main.php?cmdAktion=Differenzen&FFR_KEY='.$rsFFR->FeldInhalt('FFR_KEY').'';
                        $Form->Erstelle_ListenFeld('*FFR_KEY',$rsFFR->FeldInhalt('FFR_KEY'),0,60,false,($DS%2),'',$Link,'NO');
                        $Form->Erstelle_ListenFeld('*FFR_FILID',$rsFFR->FeldInhalt('FFR_FILID'),0,130,false,($DS%2),'','','NO');
                        $Form->Erstelle_ListenFeld('*FFR_WANR',$rsFFR->FeldInhalt('FFR_WANR'),0,100,false,($DS%2),'','','T');
                        $Form->Erstelle_ListenFeld('*FFR_VORGANGNR',$rsFFR->FeldInhalt('FFR_VORGANGNR'),0,200,false,($DS%2),'','','T');
                        $Form->Erstelle_ListenFeld('*FFR_AEMBETRAG',$rsFFR->FeldInhalt('FFR_AEMBETRAG'),0,150,false,($DS%2),'','','N2');
                        $Form->Erstelle_ListenFeld('*FFR_KASSENBETRAG',$rsFFR->FeldInhalt('FFR_KASSENBETRAG'),0,120,false,($DS%2),'','','N2');
                        $Form->Erstelle_ListenFeld('*FFR_DIFFERENZDATUM',substr($rsFFR->FeldInhalt('FFR_DIFFERENZDATUM'),0,10),0,120,false,($DS%2),'','','DU');
                        $Form->Erstelle_ListenFeld('*FFR_DIFFERENZ',$rsFFR->FeldInhalt('FFR_DIFFERENZ'),0,100,false,($DS%2),'','','N2');
                        $Status = $rsFFR->FeldInhalt('FFR_STATUS');

                        if($Status == 1)
                        {
                           $Nachricht = "bearbeitet";
                           $Form->Erstelle_ListenFeld('',$Nachricht,0,100,false,($DS%2),'color:00FF00','','T');
                        }
                        else
                        {
                           $Nachricht = "unbearbeitet";
                           $Form->Erstelle_ListenFeld('',$Nachricht,0,100,false,($DS%2),'color:FF4500','','T');
                        }



                        $Form->ZeileEnde();

                        $rsFFR->DSWeiter();
                        $DS++;
                    }

                    $Form->Formular_Ende();

                    $Link = './firstglas_Main.php?cmdAktion=Differenzen';
                    $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
                    /*
                    $Form->SchaltflaechenStart();
                    $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
                    $Form->SchaltflaechenEnde();
                    */
                }
                
            }
            elseif($Param['FIL_ID']!='' && $AWIS_KEY1 == -1 && @$_POST['sucFFR_VORGANGNR'] == "" && !isset($_GET['FFR_KEY'])) {

                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));

                $Form->Trennzeile();

                $SQL = 'SELECT FGRUECKFUEHRUNG.*';
                $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                $SQL .= ' FROM FGRUECKFUEHRUNG';
                $SQL .= ' WHERE FFR_FILID = 0'.$DB->FeldInhaltFormat('N0',$Param['FIL_ID']);
                $SQL .= ' AND FFR_STATUS <> 2';
                $SQL .= ' '.$ORDERBY;

                $Block = 1;
                /*
                if(isset($_REQUEST['Block'])) {
                    $Block=$Form->Format('N0',$_REQUEST['Block'],false);
                    $Param['BLOCK']=$Block;
                    $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));
                }
                elseif(isset($Param['BLOCK'])) {
                    $Block=$Param['BLOCK'];
                }
                */
                $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

                $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
                $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
                $SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

                //echo $SQL;

                $rsFFR = $DB->RecordSetOeffnen($SQL);
                //$Form->DebugAusgabe(1,$rsFFR);

                if($rsFFR->EOF()) {
                    $Form->ZeileStart();
                    //$Form->Hinweistext($AWISSprachKonserven['FFR']['err_KeineBestellungen']);
                    echo "Keine Datensaetze gefunden";
                    $Form->ZeileEnde();
                }
                else {
                    $Form->ZeileStart();
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_KEY'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KEY'],60,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_FILID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_FILID'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_FILID'],130,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_WANR'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_WANR'],100,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_AEMBETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_AEMBETRAG'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_VORGANGNR'],200,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_VORGANGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_VORGANGNR'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'],150,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_KASSENBETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_KASSENBETRAG'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'],120,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_DIFFERENZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_DIFFERENZ'))?'~':'');
                    $Link .= '&Sort=FFR_DIFFERENZDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_DIFFERENZDATUM'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZDATUM'],120,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'],100,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_STATUS'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_STATUS'],100,'',$Link);

                    $Form->ZeileEnde();

                    $DS=0;
                    while(!$rsFFR->EOF()) {
                        $Form->ZeileStart();
                        $Link = './firstglas_Main.php?cmdAktion=Differenzen&FFR_KEY='.$rsFFR->FeldInhalt('FFR_KEY').'';
                        $Form->Erstelle_ListenFeld('*FFR_KEY',$rsFFR->FeldInhalt('FFR_KEY'),0,60,false,($DS%2),'',$Link,'NO');
                        $Form->Erstelle_ListenFeld('*FFR_FILID',$rsFFR->FeldInhalt('FFR_FILID'),0,130,false,($DS%2),'','','NO');
                        $Form->Erstelle_ListenFeld('*FFR_WANR',$rsFFR->FeldInhalt('FFR_WANR'),0,100,false,($DS%2),'','','T');
                        $Form->Erstelle_ListenFeld('*FFR_VORGANGNR',$rsFFR->FeldInhalt('FFR_VORGANGNR'),0,200,false,($DS%2),'','','T');
                        $Form->Erstelle_ListenFeld('*FFR_AEMBETRAG',$rsFFR->FeldInhalt('FFR_AEMBETRAG'),0,150,false,($DS%2),'','','N2');
                        $Form->Erstelle_ListenFeld('*FFR_KASSENBETRAG',$rsFFR->FeldInhalt('FFR_KASSENBETRAG'),0,120,false,($DS%2),'','','N2');
                        $Form->Erstelle_ListenFeld('*FFR_DIFFERENZDATUM',substr($rsFFR->FeldInhalt('FFR_DIFFERENZDATUM'),0,10),0,120,false,($DS%2),'','','DU');
                        $Form->Erstelle_ListenFeld('*FFR_DIFFERENZ',$rsFFR->FeldInhalt('FFR_DIFFERENZ'),0,100,false,($DS%2),'','','N2');
                        $Status = $rsFFR->FeldInhalt('FFR_STATUS');

                        if($Status == 1)
                        {
                          $Nachricht = "bearbeitet";
                        }
                        else
                        {
                          $Nachricht = "unbearbeitet";
                        }
                        $Form->Erstelle_ListenFeld('FFR_BEARBEITUNGSSTATUS',$Nachricht,0,100,false,($DS%2),'','','T');


                        $Form->ZeileEnde();

                        $rsFFR->DSWeiter();
                        $DS++;
                    }

                    $Form->Formular_Ende();

                    $Link = './firstglas_Main.php?cmdAktion=Differenzen';
                    $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
                    /*
                    $Form->SchaltflaechenStart();
                    $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
                    $Form->SchaltflaechenEnde();
                    */
                }
                
            }
            elseif(isset($_POST['sucFFR_VORGANGNR']) != "" && $AWIS_KEY1 == -1 && !isset($_GET['FFR_KEY'])) {

                //echo "Hallo";

                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));

                $Form->Trennzeile();

                if($Param['FIL_ID'] == '*' && $_POST['sucFFR_VORGANGNR'] != "") {
                    //echo "HALLO_NEU";
                    $SQL = 'SELECT FGRUECKFUEHRUNG.*';
                    $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                    $SQL .= ' FROM FGRUECKFUEHRUNG';
                    $SQL .= ' WHERE FFR_VORGANGNR = '.$DB->FeldInhaltFormat('T',$_POST['sucFFR_VORGANGNR']);
                    $SQL .= ' AND FFR_STATUS <> 2';
                    $SQL .= ' '.$ORDERBY;
                }
                else {

                    if($Param['FIL_ID'] != '' && $_POST['sucFFR_VORGANGNR'] != "")
                    {
                        $SQL = 'SELECT FGRUECKFUEHRUNG.*';
                        $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                        $SQL .= ' FROM FGRUECKFUEHRUNG';
                        $SQL .= ' WHERE FFR_FILID = 0'.$DB->FeldInhaltFormat('N0',$Param['FIL_ID']);
                        $SQL .= ' AND FFR_VORGANGNR = '.$DB->FeldInhaltFormat('N0',$_POST['sucFFR_VORGANGNR']);
                        $SQL .= ' AND FFR_STATUS <> 2';
                        $SQL .= ' '.$ORDERBY;

                    }
                    else
                    {
                        if($Param['FIL_ID'] == '' || $Param['FIL_ID'] == '*' && $_POST['sucFFR_VORGANGNR'] != "")
                        {
                        $SQL = 'SELECT FGRUECKFUEHRUNG.*';
                        $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                        $SQL .= ' FROM FGRUECKFUEHRUNG';
                        $SQL .= ' WHERE FFR_VORGANGNR = '.$DB->FeldInhaltFormat('T',$_POST['sucFFR_VORGANGNR']);
                        $SQL .= ' AND FFR_STATUS <> 2';
                        $SQL .= ' '.$ORDERBY;

                        //echo $SQL;

                        }



                    }

                }

                $Block = 1;

                if(isset($_REQUEST['Block'])) {
                    $Block=$Form->Format('N0',$_REQUEST['Block'],false);
                    $Param['BLOCK']=$Block;
                    $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));
                }
                elseif(isset($Param['BLOCK'])) {
                    $Block=$Param['BLOCK'];
                }

                $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

                $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
                $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
                $SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

                $rsFFR = $DB->RecordSetOeffnen($SQL);
                //$Form->DebugAusgabe(1,$rsFFR);

                if($rsFFR->EOF()) {
                    $Form->ZeileStart();

                    echo "Keine Datensaetze gefunden";
                    $Form->ZeileEnde();
                }
                else {
                   $Form->ZeileStart();
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_KEY'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KEY'],60,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_FILID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_FILID'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_FILID'],130,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_WANR'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_WANR'],100,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_AEMBETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_AEMBETRAG'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_VORGANGNR'],200,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_VORGANGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_VORGANGNR'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'],150,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_KASSENBETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_KASSENBETRAG'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'],120,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_DIFFERENZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_DIFFERENZ'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZDATUM'],120,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_DIFFERENZDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_DIFFERENZDATUM'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'],100,'',$Link);
                    $Link = './firstglas_Main.php?cmdAktion=Differenzen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
                    $Link .= '&Sort=FFR_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FFR_STATUS'))?'~':'');
                    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_STATUS'],100,'',$Link);
                    $Form->ZeileEnde();

                    $DS=0;
                    while(!$rsFFR->EOF()) {
                        $Form->ZeileStart();
                        $Link = './firstglas_Main.php?cmdAktion=Differenzen&FFR_KEY='.$rsFFR->FeldInhalt('FFR_KEY').'';
                        $Form->Erstelle_ListenFeld('*FFR_KEY',$rsFFR->FeldInhalt('FFR_KEY'),0,60,false,($DS%2),'',$Link,'NO');
                        $Form->Erstelle_ListenFeld('*FFR_FILID',$rsFFR->FeldInhalt('FFR_FILID'),0,130,false,($DS%2),'','','NO');
                        $Form->Erstelle_ListenFeld('*FFR_WANR',$rsFFR->FeldInhalt('FFR_WANR'),0,100,false,($DS%2),'','','T');
                        $Form->Erstelle_ListenFeld('*FFR_VORGANGNR',$rsFFR->FeldInhalt('FFR_VORGANGNR'),0,200,false,($DS%2),'','','T');
                        $Form->Erstelle_ListenFeld('*FFR_AEMBETRAG',$rsFFR->FeldInhalt('FFR_AEMBETRAG'),0,150,false,($DS%2),'','','N2');
                        $Form->Erstelle_ListenFeld('*FFR_KASSENBETRAG',$rsFFR->FeldInhalt('FFR_KASSENBETRAG'),0,120,false,($DS%2),'','','N2');
                        $Form->Erstelle_ListenFeld('*FFR_DIFFERENZDATUM',substr($rsFFR->FeldInhalt('FFR_DIFFERENZDATUM'),0,10),0,120,false,($DS%2),'','','DU');
                        $Form->Erstelle_ListenFeld('*FFR_DIFFERENZ',$rsFFR->FeldInhalt('FFR_DIFFERENZ'),0,100,false,($DS%2),'','','N2');
                        $Status = $rsFFR->FeldInhalt('FFR_STATUS');

                        if($Status == 1)
                        {
                          $Nachricht = "bearbeitet";
                        }
                        else
                        {
                          $Nachricht = "unbearbeitet";
                        }
                        $Form->Erstelle_ListenFeld('FFR_BEARBEITUNGSSTATUS',$Nachricht,0,100,false,($DS%2),'','','T');



                        $Form->ZeileEnde();

                        $rsFFR->DSWeiter();
                        $DS++;
                    }

                    $Form->Formular_Ende();

                    $Link = './firstglas_Main.php?cmdAktion=Differenzen';
                    $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
                    /*
                    $Form->SchaltflaechenStart();
                    $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
                    $Form->SchaltflaechenEnde();
                    */
                }
                
            }
            else {

                $SQL = 'Select * from FGRUECKFUEHRUNG WHERE FFR_KEY='.$DB->FeldInhaltFormat('NO', $AWIS_KEY1);

                $rsFFR = $DB->RecordSetOeffnen($SQL);

                if ($AWIS_KEY1 !=-1) {
                    $AWIS_KEY1 = $rsFFR->FeldInhalt('FFR_KEY');
                    $Param['KEY']=$AWIS_KEY1;
                    $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));
                    $Form->Erstelle_HiddenFeld('FFR_KEY',$AWIS_KEY1);
                }
                else {
                    $AWIS_KEY1 = -1;
                    $AWIS_KEY2 = -1;
                }

                // Infozeile zusammenbauen

                $Felder = array();
                $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./firstglas_Main.php?cmdAktion=Differenzen&FFRListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
                $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$AWISBenutzer->BenutzerName());
                $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',date('Ymd H:i:s')));
                $Form->InfoZeile($Felder,'');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KEY'].':',150);
                $Form->Erstelle_TextFeld('FFR_KEY',$rsFFR->FeldInhalt('FFR_KEY'),20,50,false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_FILID'].':',150);
                $Form->Erstelle_TextFeld('FFR_FILID',$rsFFR->FeldInhalt('FFR_FILID'),10,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_WANR'].':',150);
                $Form->Erstelle_TextFeld('FFR_WANR',$rsFFR->FeldInhalt('FFR_WANR'),5,100,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'].':',150);
                $Form->Erstelle_TextFeld('FFR_VORGANGNR',$rsFFR->FeldInhalt('FFR_VORGANGNR'),50,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KFZKENNZ'].':',150);
                $Form->Erstelle_TextFeld('FFR_KFZKENNZ',$rsFFR->FeldInhalt('FFR_KFZKENNZ'),10,200,false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'].':',150);
                $Form->Erstelle_TextFeld('FFR_AEMBETRAG',$rsFFR->FeldInhalt('FFR_AEMBETRAG'),7,100,false,'','','','N2');
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'].':',150);
                $Form->Erstelle_TextFeld('FFR_KASSENBETRAG',$rsFFR->FeldInhalt('FFR_KASSENBETRAG'),30,100,false,'','','','N2');
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'].':',150);
                $Form->Erstelle_TextFeld('FFR_DIFFERENZ',$rsFFR->FeldInhalt('FFR_DIFFERENZ'),30,100,false,'','','','N2');
                $Form->ZeileEnde();

                $Form->Trennzeile('O');
                
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZDATUM'].':',150);
                $Form->Erstelle_TextFeld('FFR_DIFFERENZ',$rsFFR->FeldInhalt('FFR_DIFFERENZDATUM'),100,200,false,'','','','DU');
                $Form->ZeileEnde();

                $Form->Trennzeile('O');
                
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORDEFTEXT'].':',150);
                $Form->Erstelle_TextFeld("Begruendung", $rsFFR->FeldInhalt('FFR_VORDEFTEXT'), 400, 400);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift('Begr�ndung', 250);
                $Form->Erstelle_Textarea('FFR_BEGRUENDUNG', $rsFFR->FeldInhalt('FFR_BEGRUENDUNG'), 1000, 90, 4, True);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $FLAG_DETAIL = 1;

                $Form->SchaltflaechenStart();
                // Zur�ck zum Men�
                $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                if(($Recht9100&4)==4) {
                    $Form->Erstelle_HiddenFeld('FFR_KEY', $rsFFR->FeldInhalt('FFR_KEY'));
                    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
                }

                $Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/','','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
                $Form->SchaltflaechenEnde();
                $Form->SchreibeHTMLCode('</form>');
            }

            if($FLAG_DETAIL == 0)
            {
                $Form->SchaltflaechenStart();
                $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
                $Form->SchaltflaechenEnde();

                $Form->SchreibeHTMLCode('</form>');
            }
            $Form->Formular_Ende();



        }
        catch (Exception $ex)
        {
            die($ex->getMessage());
        }

        ?>
    </body>
</html>