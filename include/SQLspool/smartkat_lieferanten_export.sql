set pagesize 0;
set heading off;
set feedback off;
set verify off;
set term off;
set trimspool on;
set linesize 32676;

spool /win/applicationdata2/AWIS/06-Smartkat/awis_export.csv REPLACE;
select
  EArtNr || ';' ||
  EinspNr|| ';' ||
  HArtNr || ';' ||
  OEArtNr from v_smartkat_export
;



spool off;
exit;
