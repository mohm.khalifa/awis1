<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $_GET;
global $_POST;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;
global $NeuAngelegt;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<?php
/****************************************************************************************************
* 
* 	HilfsUndBetriebsstoffe
* 
* 	Pflege der HilfsUndBetriebsstoffe
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt 2004
* 
****************************************************************************************************/

$Rechtestufe = awisBenutzerRecht($con, 1100);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		//  8=L�schen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'HilfsUndBetriebsstoffe',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//var_dump($_POST);
echo '<form name=frmHBG method=post action=./hibe_Main.php?cmdAktion=HBST>';

/**************************************************************************
* Aktuelle Zeile l�schen
**************************************************************************/

if(isset($_POST['cmdLoeschen_x']))
{
//	var_dump($_POST);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM HILFSUNDBETRIEBSSTOFFE WHERE HBS_KEY=0" . $_POST["txtHGB_KEY_old"];
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			var_dump($SQL);
			
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Die Gruppe wurde erfolgreich gel�scht<br><br><a href=./hibe_Main.php?cmdAktion=HBST&Liste=True>Weiter</a>');
	}
	elseif(empty($_POST["cmdLoeschAbbruch"]))
	{
		print "<form name=frmHBG method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtHGB_KEY_old value=" . $_POST["txtHBS_KEY_old"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Hilfs- und Betriebsstoffgruppe l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}

if(isset($_POST['cmdSpeichern_x']))
{
	$SQL = '';
	$NeuAngelegt=0;
	
	if($_POST['txtHBS_KEY_old']=='')
	{
		$SQL = "INSERT INTO AWIS.HILFSUNDBETRIEBSSTOFFE ";
		$SQL .= "(HBS_HBT_KEY, HBS_SERIENNUMMER, HBS_INVENTARNUMMER, HBS_BAUJAHR, HBS_BEMERKUNG, HBS_AUSGEMUSTERT, HBS_USER, HBS_USERDAT)";
		$SQL .= "VALUES('" . $_POST['txtHBS_HBT_KEY'] . "'";
		$SQL .= ",'" . $_POST['txtHBS_SERIENNUMMER'] . "'";
		$SQL .= ",'" . $_POST['txtHBS_INVENTARNUMMER'] . "'";
		$SQL .= ",'" . $_POST['txtHBS_BAUJAHR'] . "'";
		$SQL .= ",'" . $_POST['txtHBS_BEMERKUNG'] . "'";
		$SQL .= ",'" . $_POST['txtHBS_AUSGEMUSTERT'] . "'";
		$SQL .= ",'" . $AWISBenutzer->BenutzerName() . "'";
		$SQL .= ",SYSDATE)";

		$NeuAngelegt=1;
	}
	else
	{
		$Aenderungen = '';
		If($_POST['txtHBS_HBT_KEY'] != $_POST['txtHBS_HBT_KEY_old'])
		{
			$Aenderungen = ", HBS_HBT_KEY = " . $_POST['txtHBS_HBT_KEY'] . "";
		}
		If($_POST['txtHBS_SERIENNUMMER'] != $_POST['txtHBS_SERIENNUMMER_old'])
		{
			$Aenderungen = ", HBS_SERIENNUMMER = '" . $_POST['txtHBS_SERIENNUMMER'] . "'";
		}
		If($_POST['txtHBS_INVENTARNUMMER'] != $_POST['txtHBS_INVENTARNUMMER_old'])
		{
			$Aenderungen = ", HBS_INVENTARNUMMER = '" . $_POST['txtHBS_INVENTARNUMMER'] . "'";
		}
		If($_POST['txtHBS_BAUJAHR'].'-' != $_POST['txtHBS_BAUJAHR_old'].'-')
		{
			$Aenderungen .= ", HBS_BAUJAHR = '" . $_POST['txtHBS_BAUJAHR'] . "'";
		}
		If($_POST['txtHBS_BEMERKUNG'].'-' != $_POST['txtHBS_BEMERKUNG_old'].'-')
		{
			$Aenderungen .= ", HBS_BEMERKUNG = '" . $_POST['txtHBS_BEMERKUNG'] . "'";
		}
		If($_POST['txtHBS_AUSGEMUSTERT'].'-' != $_POST['txtHBS_AUSGEMUSTERT_old'].'-')
		{
			$Aenderungen .= ", HBS_AUSGEMUSTERT = '" . $_POST['txtHBS_AUSGEMUSTERT'] . "'";
		}

		If($Aenderungen!='')
		{
			$SQL = "UPDATE HilfsUndBetriebsstoffe SET " . substr($Aenderungen,2);
			$SQL .= ", HBS_USER='" . $AWISBenutzer->BenutzerName() . "', HBS_USERDAT=SYSDATE";
			$SQL .= " WHERE HBS_KEY=0" . $_POST['txtHBS_KEY_old'];
		}
	}	// Neu oder �ndern

	if($SQL != '')
	{
		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}	
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

if(!isset($_POST['cmdSuchen']))
{
	
	echo '<table border=1 width=100%>';	
	
	echo '<tr><td width=200>Serien-Nr:</td><td><input name=txtSerienNr type=text value=' . (isset($_POST['txtSerienNr'])?$_POST['txtSerienNr']:''). '></td></tr>';
	echo '<tr><td width=200>Inventar-Nr:</td><td><input name=txtInventarNr type=text value=' . (isset($_POST['txtInventarNr'])?$_POST['txtInventarNr']:''). '></td></tr>';
	echo '<tr><td width=200>Typ:</td><td><input name=txtTyp type=text value=' . (isset($_POST['txtTyp'])?$_POST['txtTyp']:''). '></td></tr>';	
	echo "<tr><td colspan=2><input accesskey=w tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten (Alt+w)' name=cmdSuche value=Aktualisieren></td></tr>";	
	echo '</table>';
}

	//**************************************************
	// Kein spezieller angegeben -> suchen
	//**************************************************
	
		// 
if((!empty($_POST['txtSerienNr']) || !empty($_POST['txtInventarNr']) || !empty($_POST['txtTyp'])) AND ((!isset($_GET['HBS_KEY']) and !isset($_POST['txtHBS_KEY'])) or isset($_POST['cmdTrefferListe_x']) or $NeuAngelegt==1))
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffe, HILFSUNDBETRIEBSSTOFFTYPEN WHERE HBS_HBT_KEY=HBT_KEY";

	if($_POST['txtSerienNr']!='')
	{
		$SQL .= ' AND HBS_SERIENNUMMER ' . awisLIKEoderIST($_POST['txtSerienNr']);
	}
	if($_POST['txtInventarNr']!='')
	{
		$SQL .= ' AND HBS_INVENTARNUMMER ' . awisLIKEoderIST($_POST['txtInventarNr']);
	}
	if($_POST['txtTyp']!='')
	{
		$SQL .= ' AND HBT_BEZEICHNUNG ' . awisLIKEoderIST($_POST['txtTyp']);
	}

	if(isset($_GET['Sort']))
	{
		$SQL .= " ORDER BY " . $_GET['Sort'];
	}
	else
	{
			// Wenn keine Einschr�nkung gegeben wurde, nach dem USERDAT sortieren!
		$SQL .= " ORDER BY HBS_SERIENNUMMER";
	}

}
			// Letzte Artikel anzeigen
elseif((empty($_POST['txtSerienNr']) && empty($_POST['txtInventarNr']) && empty($_POST['txtTyp'])) AND ((!isset($_GET['HBS_KEY']) and !isset($_POST['txtHBS_KEY'])) or isset($_POST['cmdTrefferListe_x']) or $NeuAngelegt==1))
{
		$SQL = "SELECT * FROM (SELECT * FROM HilfsUndBetriebsstoffe, HILFSUNDBETRIEBSSTOFFTYPEN WHERE HBS_HBT_KEY=HBT_KEY";
		$SQL .= " ORDER BY HBS_USERDAT DESC)";
		$SQL .= ' WHERE ROWNUM<=20';	
}
else		// Einen anzeigen
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffe, HILFSUNDBETRIEBSSTOFFTYPEN WHERE HBS_HBT_KEY=HBT_KEY AND HBS_KEY=0". (isset($_GET['HBS_KEY'])?$_GET['HBS_KEY']:'') . (isset($_POST['txtHBS_KEY'])?$_POST['txtHBS_KEY']:'') . "";
}
awis_Debug(1,$SQL);
//$rsHBG ='';

if(!isset($_GET['Hinzufuegen']))
{
	$rsHBG = awisOpenRecordset($con, $SQL);
	$rsHBGZeilen = $awisRSZeilen;
}
else
{
	$rsHBGZeilen=-1;
}


	//**************************************************
	// Daten ausgeben
	//**************************************************
	
if($rsHBGZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<tr><td width=100 id=FeldBez><a href=./hibe_Main.php?cmdAktion=HBST&Sort=HBS_SERIENNUMMER>Serien-Nr</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=HBST&Sort=HBS_INVENTARNUMMER>Inventarnummer</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=HBST&Sort=HBT_BEZEICHNUNG>Typ</a></td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsHBGZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./hibe_Main.php?cmdAktion=HBST&HBS_KEY=" . $rsHBG['HBS_KEY'][$Zeile] . ">" . $rsHBG['HBS_SERIENNUMMER'][$Zeile] . "</a></td>";
		echo "<td id=DatenFeld>" . $rsHBG['HBS_INVENTARNUMMER'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" . $rsHBG['HBT_BEZEICHNUNG'][$Zeile] . "</td>";
		
		echo '</tr>';
	}
	
	echo '</table>';
}
else					// einzelnen hersteller anzeigen
{

	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Hilfs- und Betriebsstoffe</h3></td>';
	echo '<td><font size=1>' . (isset($rsHBG['HBS_USER'][0])?$rsHBG['HBS_USER'][0]:'') . '<br>' . (isset($rsHBG['HBS_USERDAT'][0])?$rsHBG['HBS_USERDAT'][0]:'') . '</font></td>';

    echo "<td width=40 backcolor=#000000 align=right><input name=cmdTrefferListe type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./hibe_Main.php?cmdAktion=HBST&Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=*></colgroup>';

		// KEY
	echo '<tr>';
	echo '<td id=FeldBez>ID</td><td>';
	echo '' . (isset($rsHBG['HBS_KEY'][0])?$rsHBG['HBS_KEY'][0]:'') . '';
	echo '<input type=hidden name=txtHBS_KEY_old value=' . (isset($rsHBG['HBS_KEY'][0])?$rsHBG['HBS_KEY'][0]:'') . '></td>';
	echo '</tr>';


	echo '<tr>';
	echo '<td id=FeldBez>Typ</td><td>';
	echo '<select name=txtHBS_HBT_KEY>';
	$SQL = "SELECT * FROM HILFSUNDBETRIEBSSTOFFTYPEN ORDER BY HBT_BEZEICHNUNG";
	$rsHBT = awisOpenRecordset($con, $SQL);
	$rsHBTZeilen = $awisRSZeilen;
	for($HBTZeile=0;$HBTZeile<$rsHBTZeilen;$HBTZeile++)
	{
		echo '<option value=' . $rsHBT['HBT_KEY'][$HBTZeile];
		if(isset($rsHBG['HBS_HBT_KEY'][0]) AND $rsHBG['HBS_HBT_KEY'][0] == $rsHBT['HBT_KEY'][$HBTZeile])
		{
			echo ' selected ';
		}
		echo '>' . $rsHBT['HBT_BEZEICHNUNG'][$HBTZeile] . '';
		echo '</option>';
	}
	
	echo '</select>';
	echo '<input type=hidden name=txtHBS_HBT_KEY_old value=' . (isset($rsHBG['HBS_HBT_KEY'][0])?$rsHBG['HBS_HBT_KEY'][0]:'') . '></td>';
	echo '</tr>';

			// Zeile 
	echo '<tr>';
	echo '<td id=FeldBez>Seriennummer</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBS_SERIENNUMMER size=40 value='" . (isset($rsHBG['HBS_SERIENNUMMER'][0])?$rsHBG['HBS_SERIENNUMMER'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBS_SERIENNUMMER_old value='" . (isset($rsHBG['HBS_SERIENNUMMER'][0])?$rsHBG['HBS_SERIENNUMMER'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBG['HBS_SERIENNUMMER'][0])?$rsHBG['HBS_SERIENNUMMER'][0]:'') . '</td>';
	}
	echo '</tr>';

	echo '<tr>';
	echo '<td id=FeldBez>Inventarnummer</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBS_INVENTARNUMMER size=40 value='" . (isset($rsHBG['HBS_INVENTARNUMMER'][0])?$rsHBG['HBS_INVENTARNUMMER'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBS_INVENTARNUMMER_old value='" . (isset($rsHBG['HBS_INVENTARNUMMER'][0])?$rsHBG['HBS_INVENTARNUMMER'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBG['HBS_INVENTARNUMMER'][0])?$rsHBG['HBS_INVENTARNUMMER'][0]:'') . '</td>';
	}
	echo '</tr>';

	echo '<tr>';
	echo '<td id=FeldBez>Baujahr</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBS_BAUJAHR size=6 value='" . (!isset($rsHBG['HBS_BAUJAHR'][0]) || $rsHBG['HBS_BAUJAHR'][0]==''?date('Y'):$rsHBG['HBS_BAUJAHR'][0]) . "'>";
		echo "<input type=hidden name=txtHBS_BAUJAHR_old value='" . (isset($rsHBG['HBS_BAUJAHR'][0])?$rsHBG['HBS_BAUJAHR'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBG['HBS_BAUJAHR'][0])?$rsHBG['HBS_BAUJAHR'][0]:'') . '</td>';
	}
	echo '</tr>';

				// Zeile 3
	echo '<tr>';
	echo '<td id=FeldBez>Bemerkung</td>';
	echo '<td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<input type=hidden name=txtHBS_BEMERKUNG_old value='" . (isset($rsHBG['HBS_BEMERKUNG'][0])?$rsHBG['HBS_BEMERKUNG'][0]:'') . "'>";

		echo '<textarea name=txtHBS_BEMERKUNG cols=60 rows=5>';
		echo (isset($rsHBG['HBS_BEMERKUNG'][0])?$rsHBG['HBS_BEMERKUNG'][0]:'') . "</textarea>";
	}
	else
	{
		echo (isset($rsHBG['HBS_BEMERKUNG'][0])?$rsHBG['HBS_BEMERKUNG'][0]:'');
	}
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td id=FeldBez>Ausgemustert am</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBS_AUSGEMUSTERT size=12 value='" . (isset($rsHBG['HBS_AUSGEMUSTERT'][0])?$rsHBG['HBS_AUSGEMUSTERT'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBS_AUSGEMUSTERT_old value='" . (isset($rsHBG['HBS_AUSGEMUSTERT'][0])?$rsHBG['HBS_AUSGEMUSTERT'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBG['HBS_AUSGEMUSTERT'][0])?$rsHBG['HBS_AUSGEMUSTERT'][0]:'') . '</td>';
	}
	echo '</tr>';

	
	
	echo '</table>';
	
	
	/********************************************************
	* Historie und andere Infos der Maschinen anzeigen
	********************************************************/
	
	if(isset($rsHBG['HBS_KEY'][0]) AND $rsHBG['HBS_KEY'][0]!='')
	{
		awis_BenutzerParameterSpeichern($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName(),$rsHBG['HBS_KEY'][0]);
		awis_RegisterErstellen(1150, $con,'Historie');
	}
	
	
	
	/********************************************************
	*
	********************************************************/

	if($Rechtestufe>1)
	{
		if(($Rechtestufe&8)==8)
		{
			print " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen>";
		}
//		print "<input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	}

	echo '</form>';

}

if(!isset($_GET['Hinzufuegen']))
{
	if(($Rechtestufe&4)==4)
	{
		echo "<a accesskey=n href=./hibe_Main.php?cmdAktion=HBST&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}
}

echo '</form>';

if($rsHBGZeilen == 1)	// Liste anzeigen
{
				// Cursor in das erste Feld setzen
	print "<Script Language=JavaScript>";
	print "document.getElementsByName(\"txtHBS_HBT_KEY\")[0].focus();";
	print "</Script>";
}

// Cursor in das erste Feld setzen
echo "<Script Language=JavaScript>";
echo "document.getElementsByName(\"txtSerienNr\")[0].focus();";
echo "</Script>";

?>