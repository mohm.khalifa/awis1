<?php

if(isset($_GET['cmdAktion']) and $_GET['cmdAktion'] == 'Export' and isset($_POST['cmdSenden_x']))
{
    include('./unfall_exportieren.php');
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

$Version = 3;
try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei($Version) .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Unfallmeldung');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Unfallmeldung'].'</title>';
?>
</head>
<body>

<?php
include ("awisHeader$Version.inc");	// Kopfzeile
echo '<script type="application/javascript" src="unfallmeldung.js" href="unfallmeldung.js"></script>';
try
{
	$Form = new awisFormular();

	if ($AWISBenutzer->HatDasRecht(21000) == 0)
	{
	    $Form->Fehler_KeineRechte();
	}

	$Register = new awisRegister(21000);
	$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}
?>
</body>
</html>