<?php
/**
 * Pr�fmodul: 	006
 * Zweck:		Dieses Modul pr�ft ob in der Preisliste 0-er Preise vorhanden sind.
 *
 * Parameter:
 *
 *
 */

global $con;					// Verbindung zum AWIS Server
global $comcon;					// Verbindung zum COM Server
global $PreisListe;				// Preislisten-ID
global $TransaktionsID;			// Transaktions-ID
global $rsPTA;					// Testarten
global $PTAZeile;				// Aktuelle Zeile in den Testarten-RS
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;
global $BindeVariablenAusgabe;

set_time_limit(0);

//*****************************************************************************
// Parameter lesen
//*****************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY AND PLP_KEY = PPP_PLP_KEY';
$SQL .= ' WHERE PLP_ID_FPLB=' . $PreisListe;
$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

$Params=array();

$rsPAR = awisOpenRecordset($comcon,$SQL);
$rsPARZeilen = $awisRSZeilen;
if($rsPARZeilen>0)
{
    for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
    {
        $Params[$rsPAR['PTP_PARAMETER'][$PARZeile]]=($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]);
    }
}

//*****************************
// Historie schreiben
//*****************************
$SQL = ' INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' (PPH_PLP_KEY,PPH_TRANSAKTIONSID,PPH_BEGINN,PPH_USER,PPH_STATUS)';
$SQL .= ' VALUES(' . $rsPTA['PLP_KEY'][$PTAZeile] . '';
$SQL .= ', ' . $TransaktionsID ;
$SQL .= ', SYSDATE';
$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
$SQL .= ', 0';
$SQL .= ')';

if(!awisExecute($comcon,$SQL))
{
    echo '<span class=HinweisTex>Fehler: '. $SQL . '</span>';
}
$rsPPH = awisOpenRecordset($comcon,'SELECT MAX(PPH_Key) AS KEY FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $TransaktionsID . ' AND PPH_User=\'' . $AWISBenutzer->BenutzerName() . '\'');
$PPHKey = $rsPPH['KEY'][0];
unset($rsPPH);

$TestErgebnis = 1;	// Erfolg als Standard
$AnzAuff=0;			// Anzahl der Auff�lligkeiten

echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);

//###############################################################################
//###############################################################################
//########################   B E G I N N   T E S T S ############################
//###############################################################################
//###############################################################################


//*****************************************************************************
// TEST 1
//*****************************************************************************

set_time_limit(0);


$BindeVariablenAusgabe=array();
$BindeVariablen=array();
$BindeVariablen[':var_N0_tst_cnt'][6]=0;

$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_PLP_MOD_6_T_1('.$TransaktionsID.','.$PPHKey.'); END;';

awisExecute($comcon,$SQL,true,false,0);

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>:0er Preislistenpr�fung</b><br>';


echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);


//*****************************************************************************
// TEST 2
//*****************************************************************************

set_time_limit(0);


$BindeVariablenAusgabe=array();
$BindeVariablen=array();


$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_PLP_MOD_6_T_2('.$TransaktionsID.','.$PPHKey.'); END;';

awisExecute($comcon,$SQL,true,false,0);

echo '<br><br>&nbsp;&nbsp;<b>TEST 2</b>:0er Preislistenpr�fung</b><br>';


echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);


//*****************************************************************************
// TEST 3
//*****************************************************************************

set_time_limit(0);


$BindeVariablenAusgabe=array();
$BindeVariablen=array();


$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_PLP_MOD_6_T_3('.$TransaktionsID.','.$PPHKey.'); END;';

awisExecute($comcon,$SQL,true,false,0,$BindeVariablen);

echo '<br><br>&nbsp;&nbsp;<b>TEST 3</b>:0er Preislistenpr�fung</b><br>';


echo str_repeat(" ",256)."<pre>";
ob_flush();
flush();
usleep(50000);



//###############################################################################
//###############################################################################
//##########################   E N D E   T E S T S ##############################
//###############################################################################
//###############################################################################

/*
//*********************************************
// Historie aktualisieren
//*********************************************
$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' SET PPH_ENDE = SYSDATE, PPH_STATUS = '.$TestErgebnis;
$SQL .= ' WHERE PPH_Key = ' . $PPHKey;
if(!awisExecute($comcon,$SQL))
{
    awisErrorMailLink('PreislistePr�fung',1,$awisDBFehler);
    flush();
}


unset($rsTST);
*/
echo '<hr>';

?>