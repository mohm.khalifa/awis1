<?php
global $AWISSprache;
global $AWISBenutzer;
global $con;
global $awisRSZeilen;

$TXT_Baustein = array();
$TXT_Baustein[]=array('FIL','FIF_%');
$TXT_Baustein[]=array('Wort','Wochentag_%');
$TXT_Baustein[]=array('Wort','Hinweis_KeinePruefgesellschaft');
$TXT_Baustein[]=array('Wort','TUEV_Termine');
$TXT_Baustein[]=array('PRG','PRG_%');
$TXT_Baustein[]=array('PRB','PRB_%');
$TXT_Baustein[]=array('PRF','PRF_%');
$TXT_Baustein[]=array('FIT','FITID_800');
$TXT_Baustein[]=array('Wort','Preis');
$TXT_Baustein[]=array('Wort','Wegbeschreibung');
$TXT_Baustein[]=array('Wort','LetzteAenderung');
$TXT_Baustein[]=array('Wort','lbl_weiter');
$TXT_Baustein[]=array('Wort','lbl_speichern');
$TXT_Baustein[]=array('Wort','keine');

$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);


$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
$FIL_ID = $FIL_ID[1];

$SQL = "SELECT filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
	       filialinfostypen.fit_information, filialinfostypen.fit_sortierung,
   			filialinfos.fif_wert, fit_id, FIF_KEY, FIF_IMQ_ID, FIF_FIL_ID, FIF_USER,  FIF_USERDAT
			FROM filialinfos, filialinfostypen, filialinfostypengruppen, importquellen
			 WHERE ((filialinfostypen.fit_id = filialinfos.fif_fit_id)
			 	AND importquellen.IMQ_ID = filialinfos.FIF_IMQ_ID
    			AND (filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id)
				AND FIF_FIL_ID=0" . $FIL_ID . ")
			ORDER BY IMQ_PRIORITAET";

$rsFilialInfo = awisOpenRecordset($con, $SQL);


$DatenBearbeitung = awisBenutzerRecht($con,118);	// Filialdaten-Bearbeitung

//awis_Debug(1,$SQL,$rsFilialInfo,$rsFiliale);
	//*******************************************
	// T�V-Termine
	//*******************************************
$RechteStufeTUEV = awisBenutzerRecht($con,111);	// TUEV-Infos
echo "<span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['TUEV_Termine']."</span></br>";
if(($RechteStufeTUEV & 8)!=8)
{
	$Ausgabe = "";
	$Erg = SucheFilialInfo($rsFilialInfo, 41, "FIT_ID");		// T?V-Termin Montag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe = ", <span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Montag']."</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}

	$Erg = SucheFilialInfo($rsFilialInfo, 42, "FIT_ID");		// T?V-Termin Dienstag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Dienstag']."</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 43, "FIT_ID");		// T?V-Termin Mittwoch
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Mittwoch']."</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 44, "FIT_ID");		// T?V-Termin Donnerstag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Donnerstag']."</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 45, "FIT_ID");		// T?V-Termin Freitag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Freitag']."</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}
	$Erg = SucheFilialInfo($rsFilialInfo, 46, "FIT_ID");		// T?V-Termin Samstag
	if($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
		$Ausgabe .= ", <span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Samstag']."</span> " . $rsFilialInfo["FIF_WERT"][$Erg];
	}

	If($Ausgabe)
	{
			echo "<span class=DatenFeldNormal>" . substr($Ausgabe,2) . "</span><br>";
	}
	else
	{
		echo "<span class=DatenFeldNormal>".$TXT_Baustein['Wort']['keine']."</span><br>";
	}
}
else		// Daten bearbeiten
{

	echo "<table border=0><tr>";
	$Erg = SucheFilialInfo($rsFilialInfo, 41, "FIT_ID");
	echo "<td><input type=hidden name=txtTUEVMontag_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Montag']."</span></td><td><input title='".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' size=16 name=txtTUEVMontag value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'></td>";

	$Erg = SucheFilialInfo($rsFilialInfo, 42, "FIT_ID");
	echo "<td><input type=hidden name=txtTUEVDienstag_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Dienstag']."</span></td><td><input title='".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' size=16 name=txtTUEVDienstag value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'></td>";

	$Erg = SucheFilialInfo($rsFilialInfo, 43, "FIT_ID");
	echo "<td><input type=hidden name=txtTUEVMittwoch_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Mittwoch']."</span></td><td><input title='".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' size=16 name=txtTUEVMittwoch value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'></td>";

	echo '</tr><tr>';
	$Erg = SucheFilialInfo($rsFilialInfo, 44, "FIT_ID");
	echo "<td><input type=hidden name=txtTUEVDonnerstag_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Donnerstag']."</span></td><td><input title='".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' size=16 name=txtTUEVDonnerstag value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'></td>";

	$Erg = SucheFilialInfo($rsFilialInfo, 45, "FIT_ID");
	echo "<td><input type=hidden name=txtTUEVFreitag_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Freitag']."</span></td><td><input title='".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' size=16 name=txtTUEVFreitag value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'></td>";

	$Erg = SucheFilialInfo($rsFilialInfo, 46, "FIT_ID");
	echo "<td><input type=hidden name=txtTUEVSamstag_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wochentag_Samstag']."</span></td><td><input title='".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "' size=16 name=txtTUEVSamstag value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'></td>";

	echo "<tr></table>";

	$SpeichernButton = True;
}

	// T?V - Preise

if(($RechteStufeTUEV & 2)==2)			// Mehr Informationen
{
	$SQL = "SELECT * FROM PruefGesellschaften, PruefArten, PruefFilialen, PruefBemerkungen";
	$SQL .= " WHERE PRF_PRG_ID = PRG_ID AND PRF_PRA_ID = PRA_ID";
	$SQL .= " AND (PRF_FIL_ID=PRB_FIL_ID(+) AND PRF_PRG_ID=PRB_PRG_ID(+))";
	$SQL .= " AND PRF_FIL_ID = 0" . $FIL_ID;
	$SQL .= " AND PRA_ID = 1";

	$rsTUEVInfos = awisOpenRecordset($con, $SQL);
	$rsTUEVInfosZeilen = $awisRSZeilen;
	if($rsTUEVInfosZeilen==0)
	{
		echo '<br>'.$TXT_Baustein['Wort']['Hinweis_KeinePruefgesellschaft'].'<br>';
	}
	else
	{
		echo "<br><table width=100% border=0><tr>";
		echo "<td id=FeldBez>".$TXT_Baustein['PRG']['PRG_ID']."</td>";
		echo "<td id=FeldBez>".$TXT_Baustein['PRG']['PRG_BEZEICHNUNG']."</td>";
//		echo "<td id=FeldBez>Untersuchung</td>";
		echo "<td id=FeldBez>".$TXT_Baustein['PRF']['PRF_PREIS']."</td>";
//		echo "<td id=FeldBez>Nachlass</td><td id=FeldBez>Sonderpreis</td>";
		echo "<td id=FeldBez>".$TXT_Baustein['PRB']['PRB_BEMERKUNG']."</td></tr>";
		for($j=0;$j<$rsTUEVInfosZeilen;$j++)
		{
			echo "<tr>";
			echo "<td valign=top>" . $rsTUEVInfos["PRG_ID"][$j] . "</td>";
			echo "<td valign=top>" . $rsTUEVInfos["PRG_BEZEICHNUNG"][$j] . "</td>";
//			echo "<td valign=top>" . $rsTUEVInfos["TUA_BEZ"][$j] . "</td>";
			echo "<td valign=top>" . awis_format($rsTUEVInfos["PRF_PREIS"][$j],"Currency") . "</td>";
//			echo "<td valign=top>" . $rsTUEVInfos["FTU_NACHLASS"][$j] . "</td>";
//			echo "<td valign=top>" . $rsTUEVInfos["FTU_SONDERPREIS"][$j] . "</td>";
			if(($RechteStufeTUEV & 4)==4)
			{
				$ZeichenAnz = count_chars($rsTUEVInfos["PRB_BEMERKUNG"][$j]);
				$ZeichenAnz = $ZeichenAnz[10]+1;

				echo "<td><textarea title=\"".$TXT_Baustein['Wort']['LetzteAenderung'].": " . $rsTUEVInfos["PRB_USER"][$j] .  ", " . $rsTUEVInfos["PRB_USERDAT"][$j] .  "\" name=txtPRB_BEMERKUNG_$j cols=60 rows=$ZeichenAnz>" . $rsTUEVInfos["PRB_BEMERKUNG"][$j] . "</textarea>";
				echo "<input type=hidden name=txtPRA_ID_$j value='" . $rsTUEVInfos["PRF_FIL_ID"][$j] . "~" . $rsTUEVInfos["PRF_PRG_ID"][$j]. "~" . $rsTUEVInfos["PRF_PRA_ID"][$j] . "'>";
				echo "</td>";
			}
			else
			{
				echo "<td valign=top>" . $rsTUEVInfos["PRB_BEMERKUNG"][$j] . "</td>";
			}

			echo "</tr>";
		}
		echo "</table><hr>";
	}	// Daten vorhanden?
	unset($rsTUEVInfos);
	unset($rsTUEVInfosZeilen);

	$SpeichernButton = True;
}
elseif(($RechteStufeTUEV & 1)==1)		// Nur Zusammenfassung
{
	echo "<table border=0>";

	$SQL = "SELECT MIN(PRF_Preis) AS Preis_MIN, MAX(PRF_Preis) AS Preis_MAX FROM PruefFilialen";
	$SQL .= " WHERE PRF_FIL_ID = 0" . $FIL_ID;
	$SQL .= " AND PRF_PRA_ID = 1";
	$rsPreise = awisOpenRecordset($con,$SQL );
	echo "<tr><td width=100><span class=DatenFeldNormal>Preis f&uuml;r HU:</td><td>";
	$Ausgabe = $rsPreise["PREIS_MIN"][0];
	if($Ausgabe <> $rsPreise["PREIS_MAX"][0] and $rsPreise["PREIS_MAX"][0]!=0)
	{
		echo awis_format($rsPreise["PREIS_MIN"][0],'Currency') . " - " . awis_format($rsPreise["PREIS_MAX"][0],'Currency',',');
	}
	else
	{
		echo awis_format($rsPreise["PREIS_MIN"][0],'Currency');
	}
	/*
	$Ausgabe="";
	$Erg = SucheFilialInfo($rsFilialInfo, 201, "FIT_ID");		// Min T?V Preis
	$Ausgabe = $rsFilialInfo["FIF_WERT"][$Erg];
	echo "<tr><td width=100><span class=DatenFeldNormal>Preis f&uuml;r HU:</td><td>" . awis_format($rsFilialInfo["FIF_WERT"][$Erg],'Currency',',');
	$Erg = SucheFilialInfo($rsFilialInfo, 202, "FIT_ID");		// Max T?V Preis

	if($Ausgabe <> $rsFilialInfo["FIF_WERT"][$Erg])
	{
		echo " - " . awis_format($rsFilialInfo["FIF_WERT"][$Erg],'Currency',',');
	}
	*/
	echo "</span></td></tr></table>";
}


	//*******************************************
	// Allg. Informationen
	//*******************************************

echo "<br><span class=DatenFeldNormalFett>".$TXT_Baustein['FIT']['FITID_800']."</span></br>";
$Erg = SucheFilialInfo($rsFilialInfo, 800, "FIT_ID");		// Allg. Infos
if(($DatenBearbeitung & 1)!=1)
{
	If($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
			echo "<span class=DatenFeldNormal>" . nl2br($rsFilialInfo["FIF_WERT"][$Erg]) . "</span>";
	}
	else
	{
		echo "<span class=DatenFeldNormal>&nbsp;</span>";
	}
}
else
{
	echo "<input type=hidden name=txtAllgInfos_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<textarea title=\"".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "\" name=txtAllgInfos rows=5 cols=80>" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "</textarea>";
	$SpeichernButton = True;
}



	//*******************************************
	// Wegbeschreibung
	//*******************************************

echo "<br><span class=DatenFeldNormalFett>".$TXT_Baustein['Wort']['Wegbeschreibung']."</span></br>";
$Erg = SucheFilialInfo($rsFilialInfo, 47, "FIT_ID");		// Wegbeschreibung
if(($DatenBearbeitung & 1)!=1)
{
	If($rsFilialInfo["FIF_WERT"][$Erg]!="")
	{
			echo "<span class=DatenFeldNormal>" . nl2br($rsFilialInfo["FIF_WERT"][$Erg]) . "</span>";
	}
	else
	{
		echo "<span class=DatenFeldNormal>keine gefunden.</span>";
	}
}
else
{
	echo "<input type=hidden name=txtWegBeschreibung_old value='" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "'>";
	echo "<textarea title=\"".$TXT_Baustein['Wort']['LetzteAenderung'].": " . (!empty($rsFilialInfo["FIF_USER"][$Erg])?$rsFilialInfo["FIF_USER"][$Erg]:'') .  ", " . (!empty($rsFilialInfo["FIF_USERDAT"][$Erg])?$rsFilialInfo["FIF_USERDAT"][$Erg]:'') .  "\" name=txtWegBeschreibung rows=5 cols=80>" . (!empty($rsFilialInfo["FIF_WERT"][$Erg])?$rsFilialInfo["FIF_WERT"][$Erg]:'') . "</textarea>";
	$SpeichernButton = True;
}
?>