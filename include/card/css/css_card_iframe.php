<?php
switch($intSchriftgroesse)
{
	case 12:	//Werte f�r die Breite der Spalten im I-Frame (werden �ber css zugewiesen)

				$intIfr_rgnr=140;					//Breite der Spalte Rechnungs-Nr im I-Frame (entspricht Spalte Beschriftung Adressdaten in der Hauptseite)
				$intIfr_betr=90;					//Breite der Spalte Betrag im I-Frame
				$intIfr_dat=65;						//Breite der Spalte Datum im I-Frame (entspricht Spalte Textfelder Mahndaten in der Hauptseite)
				$intIfr_buart=300;					//Breite der Spalte Buchungsart im I-Frame
				$intIfr_ktoaz=100;					//Breite der Spalte Kontoauszug im I-Frame
				$intIfr_hoehe=137;					//Hoehe des Iframes
				break;
				
	case 16:	//Werte f�r die Breite der Spalten im I-Frame (werden �ber css zugewiesen)

				$intIfr_rgnr=190;					//Breite der Spalte Rechnungs-Nr im I-Frame (entspricht Spalte Beschriftung Adressdaten in der Hauptseite)
				$intIfr_betr=110;					//Breite der Spalte Betrag im I-Frame
				$intIfr_dat=80;						//Breite der Spalte Datum im I-Frame (entspricht Spalte Textfelder Mahndaten in der Hauptseite)
				$intIfr_buart=390;					//Breite der Spalte Buchungsart im I-Frame
				$intIfr_ktoaz=120;					//Breite der Spalte Kontoauszug im I-Frame
				$intIfr_hoehe=245;					//Hoehe des Iframes
				break;
			
	default:	//Werte f�r die Breite der Spalten im I-Frame (werden �ber css zugewiesen)

				$intIfr_rgnr=170;					//Breite der Spalte Rechnungs-Nr im I-Frame (entspricht Spalte Beschriftung Adressdaten in der Hauptseite)
				$intIfr_betr=100;					//Breite der Spalte Betrag im I-Frame
				$intIfr_dat=70;						//Breite der Spalte Datum im I-Frame (entspricht Spalte Textfelder Mahndaten in der Hauptseite)
				$intIfr_buart=340;					//Breite der Spalte Buchungsart im I-Frame
				$intIfr_ktoaz=110;					//Breite der Spalte Kontoauszug im I-Frame
				$intIfr_hoehe=90;					//Hoehe des Iframes
}

if(strrchr($PHP_SELF,'/')!='/card_haupt.php')
{
?>



/* Werte f�r I-Frame card_iframe.php */
body									{	background-color:<?php echo $strZellhintergrund; ?>; }
										
.ifr_rgnr								{	width:<?php echo $intIfr_rgnr; ?>px;
											font-weight:bold; }
.ifr_betr								{	width:<?php echo $intIfr_betr; ?>px; 
											text-align:right; }
.ifr_dat								{	width:<?php echo $intIfr_dat; ?>px; }
.ifr_buart								{	width:<?php echo $intIfr_buart; ?>px; }
.ifr_ktoaz								{	width:<?php echo $intIfr_ktoaz; ?>px; }

.ifr_speibut							{	height:<?php echo $intElementhoehe; ?>;
											width:<?php echo $intElementhoehe; ?>;
											display:none; }

#but									{	height:<?php echo $intElementhoehe; ?>;		/*Button neuer Datensatz*/
											width:<?php echo $intElementhoehe; ?>; }

#rnr									{	display:none; }			/*Felder neue Zeile in IFrame sind versteckt*/
#bet,#dat,#bua,#kaz,#but				{	visibility:hidden; }		

<?php
}
?>
