<?php
/**
 * Suchmaske f�r die Auswahl eines Datenexportes
 *
 * @author    Sacha Kerres
 * @copyright ATU
 * @version   201405
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('XDE', 'XDE_%');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'GespeicherteAbfrage');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Liste', 'Bedingungen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht4001 = $AWISBenutzer->HatDasRecht(4001);
    if ($Recht4001 == 0) {
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $Form->SchreibeHTMLCode('<form name="frmSuche" method="post" action="./datenexporte_Main.php?cmdAktion=Details">');

    /**********************************************
     * * Eingabemaske
     ***********************************************/
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Datenexporte'));

    if (!isset($Param['SPEICHERN'])) {
        $Param['SPEICHERN'] = 'off';
    }

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['XDE']['XDE_BEZEICHNUNG'] . ':', 190);
    $SQL = 'SELECT DISTINCT XDE_KEY, XDE_Bezeichnung ';
    $SQL .= ' FROM ExportDaten ';
    $SQL .= ' INNER JOIN V_ACCOUNTRECHTE ON XDE_XRC_ID = XBA_XRC_ID AND BITAND(XBA_STUFE,XDE_STUFE)=XDE_STUFE';
    $SQL .= ' WHERE XBL_LOGIN = ' . $DB->WertSetzen('XDE', 'T', strtoupper($AWISBenutzer->BenutzerName())) . '';
    $SQL .= ' AND XDE_STATUS = \'A\'';
    $SQL .= ' ORDER BY XDE_Bezeichnung';
    $Form->Erstelle_SelectFeld('*XDE_KEY', ($Param['SPEICHERN'] == 'on'?$Param['XDE_KEY']:''), '350:340', true, $SQL,
        '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '', '', $DB->Bindevariablen('XDE'), '');
    $AWISCursorPosition = 'suxXDE_KEY';
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['GespeicherteAbfrage'] . ':', 190);
    $SQL = 'SELECT a.XAB_KEY, c.XDE_BEZEICHNUNG || \' - \' || a.XAB_BEZEICHNUNG AS Anzeige';
    $SQL .= ' FROM ExportAbfragen a INNER JOIN';
    $SQL .= ' (';
    $SQL .= '   SELECT XAB_KEY, XAB_XBN_KEY';
    $SQL .= '   FROM EXPORTABFRAGEN';
    $SQL .= '   WHERE XAB_XBN_KEY = ' . $DB->WertSetzen('XAB','N0', $AWISBenutzer->BenutzerID());
    $SQL .= '   UNION ALL';
    $SQL .= '   SELECT XAN_XAB_KEY, XAN_XBN_KEY';
    $SQL .= '   FROM EXPORTABFRAGENBENUTZER';
    $SQL .= '   WHERE XAN_XBN_KEY = ' . $DB->WertSetzen('XAB','N0', $AWISBenutzer->BenutzerID());
    $SQL .= ' ) b';
    $SQL .= ' ON a.XAB_KEY = b.XAB_KEY';
    $SQL .= ' INNER JOIN EXPORTDATEN c';
    $SQL .= ' ON a.XAB_XDE_KEY = c.XDE_KEY';
    $SQL .= ' ORDER BY c.XDE_BEZEICHNUNG, a.XAB_BEZEICHNUNG';
    $Form->Erstelle_SelectFeld('*XAB_KEY', ($Param['SPEICHERN'] == 'on'?$Param['XAB_KEY']:''), '350:340', true, $SQL,
        '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '', '', $DB->Bindevariablen('XAB'),
        '' . '', 'AWIS', '', true);
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png',
        $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'],
        'W');
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);

    $Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201110221210");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201110221211");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>