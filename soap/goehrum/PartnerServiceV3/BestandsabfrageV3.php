<?php

class BestandsabfrageV3 extends Serialize
{

    /**
     * @var string $EinspeiserNr
     */
    protected $EinspeiserNr = null;

    /**
     * @var string $EinspeiserArtikelNr
     */
    protected $EinspeiserArtikelNr = null;

    /**
     * @var int $AnfrageMenge
     */
    protected $AnfrageMenge = null;

    /**
     * @var int $Zentrallager
     */
    protected $Zentrallager = null;

    /**
     * @var int $Filiale
     */
    protected $Filiale = null;

    /**
     * @var string $MEH
     */
    protected $MEH = null;

    /**
     * @var int $VPE
     */
    protected $VPE = null;

    /**
     * @var boolean $IstVerfuegbar
     */
    protected $IstVerfuegbar = null;

    /**
     * @var int $PreisEH
     */
    protected $PreisEH = null;

    /**
     * @var float $Nettopreis
     */
    protected $Nettopreis = null;

    /**
     * @var float $Bruttopreis
     */
    protected $Bruttopreis = null;

    /**
     * @var string $Infotext
     */
    protected $Infotext = null;

    /**
     * @param int $AnfrageMenge
     * @param int $Zentrallager
     * @param int $Filiale
     * @param int $VPE
     * @param boolean $IstVerfuegbar
     * @param int $PreisEH
     * @param float $Nettopreis
     * @param float $Bruttopreis
     */
    public function __construct($AnfrageMenge, $Zentrallager, $Filiale, $VPE, $IstVerfuegbar, $PreisEH, $Nettopreis, $Bruttopreis)
    {
      $this->AnfrageMenge = $AnfrageMenge;
      $this->Zentrallager = $Zentrallager;
      $this->Filiale = $Filiale;
      $this->VPE = $VPE;
      $this->IstVerfuegbar = $IstVerfuegbar;
      $this->PreisEH = $PreisEH;
      $this->Nettopreis = $Nettopreis;
      $this->Bruttopreis = $Bruttopreis;
    }

    /**
     * @return string
     */
    public function getEinspeiserNr()
    {
      return $this->EinspeiserNr;
    }

    /**
     * @param string $EinspeiserNr
     * @return BestandsabfrageV3
     */
    public function setEinspeiserNr($EinspeiserNr)
    {
      $this->EinspeiserNr = $EinspeiserNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getEinspeiserArtikelNr()
    {
      return $this->EinspeiserArtikelNr;
    }

    /**
     * @param string $EinspeiserArtikelNr
     * @return BestandsabfrageV3
     */
    public function setEinspeiserArtikelNr($EinspeiserArtikelNr)
    {
      $this->EinspeiserArtikelNr = $EinspeiserArtikelNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getAnfrageMenge()
    {
      return $this->AnfrageMenge;
    }

    /**
     * @param int $AnfrageMenge
     * @return BestandsabfrageV3
     */
    public function setAnfrageMenge($AnfrageMenge)
    {
      $this->AnfrageMenge = $AnfrageMenge;
      return $this;
    }

    /**
     * @return int
     */
    public function getZentrallager()
    {
      return $this->Zentrallager;
    }

    /**
     * @param int $Zentrallager
     * @return BestandsabfrageV3
     */
    public function setZentrallager($Zentrallager)
    {
      $this->Zentrallager = $Zentrallager;
      return $this;
    }

    /**
     * @return int
     */
    public function getFiliale()
    {
      return $this->Filiale;
    }

    /**
     * @param int $Filiale
     * @return BestandsabfrageV3
     */
    public function setFiliale($Filiale)
    {
      $this->Filiale = $Filiale;
      return $this;
    }

    /**
     * @return string
     */
    public function getMEH()
    {
      return $this->MEH;
    }

    /**
     * @param string $MEH
     * @return BestandsabfrageV3
     */
    public function setMEH($MEH)
    {
      $this->MEH = $MEH;
      return $this;
    }

    /**
     * @return int
     */
    public function getVPE()
    {
      return $this->VPE;
    }

    /**
     * @param int $VPE
     * @return BestandsabfrageV3
     */
    public function setVPE($VPE)
    {
      $this->VPE = $VPE;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIstVerfuegbar()
    {
      return $this->IstVerfuegbar;
    }

    /**
     * @param boolean $IstVerfuegbar
     * @return BestandsabfrageV3
     */
    public function setIstVerfuegbar($IstVerfuegbar)
    {
      $this->IstVerfuegbar = $IstVerfuegbar;
      return $this;
    }

    /**
     * @return int
     */
    public function getPreisEH()
    {
      return $this->PreisEH;
    }

    /**
     * @param int $PreisEH
     * @return BestandsabfrageV3
     */
    public function setPreisEH($PreisEH)
    {
      $this->PreisEH = $PreisEH;
      return $this;
    }

    /**
     * @return float
     */
    public function getNettopreis()
    {
      return $this->Nettopreis;
    }

    /**
     * @param float $Nettopreis
     * @return BestandsabfrageV3
     */
    public function setNettopreis($Nettopreis)
    {
      $this->Nettopreis = $Nettopreis;
      return $this;
    }

    /**
     * @return float
     */
    public function getBruttopreis()
    {
      return $this->Bruttopreis;
    }

    /**
     * @param float $Bruttopreis
     * @return BestandsabfrageV3
     */
    public function setBruttopreis($Bruttopreis)
    {
      $this->Bruttopreis = $Bruttopreis;
      return $this;
    }

    /**
     * @return string
     */
    public function getInfotext()
    {
      return $this->Infotext;
    }

    /**
     * @param string $Infotext
     * @return BestandsabfrageV3
     */
    public function setInfotext($Infotext)
    {
      $this->Infotext = $Infotext;
      return $this;
    }

}
