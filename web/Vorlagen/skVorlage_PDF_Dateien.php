<?
global $con;
global $awisRSZeilen;
global $awisDBFehler;

require_once 'fpdi.php';

/***************************************
* Neue PDF Datei erstellen
***************************************/
define('FPDF_FONTPATH','font/');
$pdf = new fpdi('l','mm','a4');
$pdf->open();
// ATU Logo als Hintergrundbild laden
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);				

/***************************************
*
* Daten laden
*
***************************************/

$SQL = '';

$rsXXX = awisOpenRecordset($con,$SQL);
$rsXXXZeilen = $awisRSZeilen;


if($rsXXXZeilen==0)
{
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,270,4,20);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
}
else 		// Daten gefunden
{
	
	NeueSeite($pdf,$Zeile,$ATULogo,15);
	
	
	
}	// Ende Datenaufbereitung


/***************************************
* Abschluss und Datei speichern
***************************************/

$DateiName = awis_UserExportDateiName('.pdf');
$DateiNameLink = pathinfo($DateiName);
$DateiNameLink = '/export/' . $DateiNameLink['basename'];
$pdf->saveas($DateiName);
echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a>";

/**
*
* Funktion erzeugt eine neue Seite mit �berschriften
*
* @author Sacha Kerres
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand)
{
	static $Seite;
	
	if($Seite > 0)		// Fu�zeile?
	{
		$pdf->setXY($LinkerRand,285);					// Cursor setzen
		$pdf->SetFont('Arial','',6);					// Schrift setzen
		$pdf->setXY(190,285);					// Cursor setzen
		$pdf->Cell(170,3,'Seite ' . $Seite,0,0,'L',0);

		$pdf->setXY($LinkerRand,290);					// Cursor setzen
		$pdf->SetFont('Arial','',3);					// Schrift setzen
					// �berschrift
		$pdf->MultiCell(160,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,'C',0);
	}
	$Seite++;
	
	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,185,4,20);		// Logo einbauen

	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','',14);				// Schrift setzen
				// �berschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(190,6,"ATU",0,0,'C',0);
	
	$pdf->setXY($LinkerRand,10);				// Cursor setzen
	$pdf->SetFont('Arial','',6);				// Schrift setzen
	$pdf->cell(190,6,"Stand: " . date('d.m.Y'),0,0,'C',0);

	
	// �berschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen

		// Spalten�berschriften
	$pdf->setXY($LinkerRand,16);				// Cursor setzen
	$pdf->cell(90,6,"Spalte1",1,0,'C',1);
	
	$pdf->setXY(105,16);						// Cursor setzen
	$pdf->cell(60,6,"Spalte2",1,0,'C',1);
	
	$pdf->setXY(165,16);						// Cursor setzen
	$pdf->cell(10,6,"Spalte3",1,0,'C',1);
	
	$Zeile = 22;
}
?>