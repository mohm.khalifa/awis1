<?php
require_once 'awis_portalzugaenge.inc';
require_once 'awisJobVerwaltung.inc';

try
{
	$Job = new awisJobProtokoll(12,'Starte Abgleich.');
	$Portal = new awis_Portalzugaenge('awis_jobs');
	
	$Job->SchreibeLog(awisJobProtokoll::TYP_MELDUNG,0,'Suche Daten');	

	$Portal->DebugLevel(10);

// Neue Benutzer von PEPIS holen
// -> ausgeschiedene Sperren
	$Portal->PersonalAbgleich();
	$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,0,'Abgleich abgeschlossen.');	
	
}
catch (Exception $ex)
{
	if(isset($Job))
	{
		$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,3,'Fehler:'.$ex->getMessage());
	}
}
?>
