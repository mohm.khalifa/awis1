<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("phpCheckTime.class.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $_POST;
global $_GET;
global $awisDBFehler;
global $AOOZeile;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,505);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Auswertungen Atunr ohne Oenr',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}
	// Wurde ein g�ltiger Parameter angegeben?
		// -> sofort Liste anzeigen
$ParAktiv=((isset($_GET['cmd_Aktion']) AND $_GET['cmd_Aktion']=='Liste') 
		   or (isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!=''));

$objLaufZeit = new phpCheckTime;             // Neues phpCheckTime Objekt
$objLaufZeit->start();      			// Startzeit

awis_RegisterErstellen(52, $con, ($ParAktiv?'Liste':''));

$objLaufZeit->stop();      			// Endzeit

if($AOOZeile == 0){
	printf( "Ergebnis in %1.3f Sekunden ermittelt", $objLaufZeit->getTime());
} else {
	printf( "%d Datens&auml;tze in %1.3f Sekunden gefunden", $AOOZeile, $objLaufZeit->getTime());
}

if(!isset($_GET['ZurueckLink']) or $_GET['ZurueckLink']=='')
{
	if($ParAktiv)
	{
		print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=history.back();>";
	}
	else
	{
		print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./auswertungen_Main.php';>";
	}	
}
else
{
	print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . $_GET['ZurueckLink'] . "';>";
}
print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

?>
</body>
</html>

