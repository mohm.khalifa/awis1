SET DEFINE OFF;

INSERT INTO WEBMASKENREGISTER (
  XRG_ID,
  XRG_XWM_ID,
  XRG_BEZEICHNUNG,
  XRG_BESCHRIFTUNG,
  XRG_AKTION,
  XRG_SEITE,
  XRG_BEMERKUNG,
  XRG_XRC_ID,
  XRG_RECHTESTUFE,
  XRG_SORTIERUNG
) VALUES (
  50004,
  5000,
  'Tourenplan',
  'Tourenplan',
  './filialinfos_Main.php?cmdAktion=Tourenplan',
  './filialinfos_Tourenplan.php',
  'Uploadseite f�r die Tourenplaene',
  5000,
  512,
  4
);

COMMIT;

INSERT INTO RECHTESTUFEN (
  XRS_XRC_ID,
  XRS_BIT,
  XRS_BESCHREIBUNG
) VALUES (
  5000,
  9,
  'Tourenpl�ne uploaden'
);

COMMIT;

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_USERDAT) VALUES ('reg_50004','Register','Registerbeschriftung','<u>T</u>ourenplan',to_date('03.01.18 11:31:45','DD.MM.RR HH24:MI:SS'));

COMMIT;
