<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('BAF','%');
	$TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	$TextKonserven[]=array('Wort','vom');	
	$TextKonserven[]=array('Wort','Filialbestand');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht3601 = $AWISBenutzer->HatDasRecht(3601);
	if($Recht3601==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');

	if (isset($_GET['BAF_FIL_ID'])) 
	{
		$FILIALE = intval($_GET['BAF_FIL_ID']);		
	}	
	
	$Spalte = 20;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Bestandsabfrage');
	
	$Zeile=_Erzeuge_Seite_Bestandsabfrage($FILIALE);
	
	/***************************************
	* Hole Daten per SQL
	***************************************/	
	$SQL = 'SELECT BAF_AST_ATUNR, AST_BEZEICHNUNGWW, BAF_DATUMSCAN, BAF_MENGESCAN, FIB_BESTAND, RUF_REGUNDFACHNR, AST_VK' ; 
	$SQL .= ' FROM BESTANDSABFRAGEN';
	$SQL .= ' INNER JOIN ARTIKELSTAMM ON AST_ATUNR = BAF_AST_ATUNR';
	$SQL .= ' LEFT JOIN FILIALBESTAND ON FIB_AST_ATUNR = BAF_AST_ATUNR AND FIB_FIL_ID = BAF_FIL_ID';
	$SQL .= ' LEFT JOIN REGALUNDFACHNUMMER ON RUF_AST_ATUNR = BAF_AST_ATUNR AND RUF_FIL_ID = BAF_FIL_ID AND RUF_AKTIV = 1';
	$SQL .= ' WHERE BAF_FIL_ID=0'.$FILIALE;
	$SQL .= ' ORDER BY BAF_AST_ATUNR ASC';
		
	$Form->DebugAusgabe(1,$SQL);
	
	$rsBAF = $DB->RecordSetOeffnen($SQL);
	$rsBAFZeilen = $rsBAF->AnzahlDatensaetze();
	
	if($rsBAFZeilen==0)		// Keine Daten
	{
		$Form->Hinweistext('Keine Daten gefunden!');
		die();
	}		
		
	for($BAFZeile=0;$BAFZeile<$rsBAFZeilen;$BAFZeile++)
	{	
		if(($Ausdruck->SeitenHoehe()-20)<$Zeile)
		{
			$Zeile=_Erzeuge_Seite_Bestandsabfrage($FILIALE);
		}			
		
		$Ausdruck->_pdf->SetFont('Arial','',8);		
		
		$Ausdruck->_pdf->SetFont('Arial','',9);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(15,6,$rsBAF->FeldInhalt('BAF_AST_ATUNR'),1,0,'L',0);	
			
		$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
		$Ausdruck->_pdf->cell(40,6,substr($rsBAF->FeldInhalt('AST_BEZEICHNUNGWW'),0,15),1,0,'L',0);			
						
		$Ausdruck->_pdf->setXY($Spalte+55,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$rsBAF->FeldInhalt('FIB_BESTAND'),1,0,'R',0);			
		
		$Ausdruck->_pdf->setXY($Spalte+80,$Zeile);
		$Ausdruck->_pdf->cell(22,6,$rsBAF->FeldInhalt('BAF_MENGESCAN'),1,0,'R',0);			
		
		$Ausdruck->_pdf->setXY($Spalte+102,$Zeile);
		$Ausdruck->_pdf->cell(20,6,$Form->Format('N2',$rsBAF->FeldInhalt('AST_VK')),1,0,'R',0);			
		
		$Ausdruck->_pdf->setXY($Spalte+122,$Zeile);
		$Ausdruck->_pdf->cell(60,6,$rsBAF->FeldInhalt('RUF_REGUNDFACHNR'),1,0,'R',0);						
		
		$Zeile+=6;
	
		$rsBAF->DSWeiter();
	}	
	
	$Ausdruck->Anzeigen();	
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


function _Erzeuge_Seite_Bestandsabfrage($FILIALE)
{
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
	
	if ($Seite == 1)
	{			
		$SQL = 'SELECT DISTINCT BAF_FIL_ID, MAX(BAF_DATUMSCAN) as DATUMSCAN';
		$SQL .=' FROM Bestandsabfragen BAF';		
		$SQL .=' WHERE BAF_FIL_ID=0'.$FILIALE;
		$SQL .=' GROUP BY BAF_FIL_ID';		
		
		$rsBAFKopf = $DB->RecordSetOeffnen($SQL);							
									
		$Ausdruck->_pdf->SetFont('Arial','B',14);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['BAF']['Bestandsabfrage'].' '.$AWISSprachKonserven['Wort']['vom'].': '.$Form->Format('D', $rsBAFKopf->FeldInhalt('DATUMSCAN')),0,0,'L',0);		
		$Ausdruck->_pdf->SetXY($Spalte+40,$Zeile);												
	}
	else 
	{
		$Zeile+=5;
	}
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);		
	$Zeile=$Zeile+10;		
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	$Ausdruck->_pdf->cell(15,6,$AWISSprachKonserven['BAF']['BAF_AST_ATUNR'],1,0,'L',1);				
		
	$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
	$Ausdruck->_pdf->cell(45,6,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
		
	$Ausdruck->_pdf->setXY($Spalte+55,$Zeile);
	$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['Wort']['Filialbestand'],1,0,'L',1);		
		
	$Ausdruck->_pdf->setXY($Spalte+80,$Zeile);
	$Ausdruck->_pdf->cell(22,6,$AWISSprachKonserven['BAF']['BAF_MENGESCAN'],1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+102,$Zeile);
	$Ausdruck->_pdf->cell(20,6,$AWISSprachKonserven['BAF']['BAF_VK'],1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+122,$Zeile);
	$Ausdruck->_pdf->cell(60,6,$AWISSprachKonserven['BAF']['BAF_REGALUNDFACHNUMMER'],1,0,'L',1);		
				
	$Zeile+=6;	
	
	return $Zeile;
}
?>

