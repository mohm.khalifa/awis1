ALTER TABLE KONTAKTEABTEILUNGEN ADD KAB_BEMERKUNG VARCHAR2(200);
ALTER TABLE KONTAKTEABTEILUNGEN ADD KAB_USER VARCHAR2(50);
ALTER TABLE KONTAKTEABTEILUNGEN ADD KAB_USERDAT DATE;

UPDATE kontakteabteilungen set kab_user = 'AWIS', KAB_USERDAT = '01.01.1970';

Insert into rechte (xrc_id, xrc_recht) values (650, 'Stammdaten: Abteilungsbearbeitung');

insert into rechtestufen (xrs_xrc_id, xrs_bit, xrs_beschreibung) values (650, 0, 'Anzeigen');
insert into rechtestufen (xrs_xrc_id, xrs_bit, xrs_beschreibung) values (650, 1, 'Abteilungen bearbeiten');
insert into rechtestufen (xrs_xrc_id, xrs_bit, xrs_beschreibung) values (650, 2, 'Abteilungen erstellen');
insert into rechtestufen (xrs_xrc_id, xrs_bit, xrs_beschreibung) values (650, 3, 'Abteilungen l�schen');

INSERT INTO webmenuepunkte (xmp_key, xmp_xme_key,xmp_bezeichnung,xmp_xrc_id,xmp_stufe,xmp_gruppenid,xmp_sortierung,xmp_link,xmp_user,xmp_userdat) VALUES ( 237,10,'Abteilungsbearbeitung',650,0,2,10,'/stammdaten/abteilungsbearbeitung/abteilungsbearbeitung_Main.php?cmdAktion=Suche','Andr� Meyer',Sysdate);

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('GRUPPE_10_2','MENUE','Stammdaten','Telefonverzeichnis');

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('MENUE_237_TEXT','MENUE','Menuepunktueberschrift','Abteilungsbearbeitung');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('MENUE_237_HINWEIS','MENUE','Menuepunkthinweis','Abteilungsbearbeitung');

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('tit_ABTEILUNGSBEARBEITUNG','TITEL','Titel f�r die Abteilungsbearbeitung','Abteilungsbearbeitung');

INSERT INTO webmasken (xwm_id,xwm_bezeichnung,xwm_bemerkung,xwm_user,xwm_userdat,xwm_status) VALUES (650,'Abteilungsbearbeitung','Abteilungsbearbeitung', 'Andr� Meyer',Sysdate,'A');

INSERT INTO webmaskenregister (xrg_id,xrg_xwm_id,xrg_bezeichnung,xrg_beschriftung,xrg_aktion,xrg_seite,xrg_bemerkung,xrg_xrc_id,xrg_rechtestufe,xrg_sortierung,xrg_neuehilfe) VALUES (6500,650, 'Suche', 'Suche','./abteilungsbearbeitung_Main.php?cmdAktion=Suche','abteilungsbearbeitung_Suche.php','Suche',650,0,1,0);
INSERT INTO webmaskenregister (xrg_id,xrg_xwm_id,xrg_bezeichnung,xrg_beschriftung,xrg_aktion,xrg_seite,xrg_bemerkung,xrg_xrc_id,xrg_rechtestufe,xrg_sortierung,xrg_neuehilfe) VALUES (6501,650, 'Details', 'Details','./abteilungsbearbeitung_Main.php?cmdAktion=Details','abteilungsbearbeitung_Details.php','Details',650,0,2,0);

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('reg_6500','Register','Register zu Abteilungsbearbeitung','Suche');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('reg_6501','Register','Register zu Abteilungsbearbeitung','Details');

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('ttt_6500','Register','Tooltipptext','Suche');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('ttt_6501','Register','Tooltipptext','Details');

INSERT INTO programmparameter (xpp_id,xpp_bezeichnung,xpp_default,xpp_ueberschreibbar,xpp_bemerkung,xpp_kodiert,xpp_datentyp) VALUES (650,'Formular_KAB',';',255,'Suchparameter bei der Abteilungsbearbeitung',0,'T');

---------------
-- Daten zur Anzeige innerhalb der Men�punkte
---------------

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('KAB_BEMERKUNG','KAB','Bermerkungsfeld bei den Abteilungen','Bemerkung');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('KAB_ABTEILUNGSMITGLIED','KAB','','Mitarbeiter in dieser Abteilung');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('KAB_ABTEILUNGSBETREUERIT','KAB','','IT-Betreuung zu dieser Abteilung');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('KAB_KEINDS','KAB','','Es wurde kein Datensatz zu Ihrer Suche gefunden.');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('KAB_POPUP_TEXT','KAB','Text des PopUps in Abteilungsbearbeitung','Sollen alle untergeordneten Datens�tze gel�scht werden?');

SET DEFINE OFF;
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('KAB_POPUP_LOESCHEN','KAB','','Es sind noch #ANZ_BETR# IT-Betreuer und #ANZ_MIT# Mitarbeiter auf dieser Abteilung gespeichert. Diese Verbindungen werden beim L�schen entfernt. Sind Sie sich sicher, dass die Abteilung gel�scht werden soll?');
