<?php
require_once 'awisMailer.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 19.12.2016
 * Time: 10:24
 */
class awisDeployment
{
    /**
     * AWISBenutzer Objekt
     * @var awisBenutzer
     */
    private $_AWISBenutzer;

    /**
     * @var int Debug Level
     */
    private $_DebugLevel = 99;

    /**
     * @var string DebugText
     */
    private $_DebugText='';

    /**
     * Quelldatenbankobjekt
     * @var awisDatenbank
     */
    private $_QuellDB;

    /**
     * Zieldatenbankobjekt
     * @var awisDatenbank
     */
    private $_ZielDB;

    /**
     * Ziel f�r SQLPlus
     * @var
     */
    private $_Ziel;


    /**
     * @var string Ordner wohin Bamboo auscheckt
     */
    private $_VerteilungsOrdner = '/verteilung/';

    /**
     * ConfName der Entwicklungsdatenbank
     */
    const AWIS_ENTWICK = 'AWISENTWICK_DE';
    /**
     * ConfName der Stagingdatenbank
     */
    const AWIS_STAG = 'AWISSTAG_DE';
    /**
     * ConfName der Proddatenbank
     */
    const AWIS_PROD = 'AWISPROD_DE';



    function __construct($Benutzer)
    {
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);

    }

    function __destruct()
    {
        echo $this->_DebugText;
    }

    /**
     * Legt das Ziel (und somit auch die Quelle) fest.
     * @param $Ziel
     */
    public function Ziel($Ziel){
        $this->_Ziel = $Ziel;
        switch ($Ziel){
            case self::AWIS_ENTWICK:
                $this->_QuellDB = awisDatenbank::NeueVerbindung(self::AWIS_ENTWICK);
                $this->_VerteilungsOrdner .= 'ENTWICK/';
                break;
            case self::AWIS_STAG:
                $this->_QuellDB = awisDatenbank::NeueVerbindung(self::AWIS_ENTWICK);
                $this->_VerteilungsOrdner .= 'STAG/';
                break;
            case self::AWIS_PROD:
                $this->_QuellDB = awisDatenbank::NeueVerbindung(self::AWIS_STAG);
                $this->_VerteilungsOrdner .= 'PROD/';
                break;
        }
        $this->_VerteilungsOrdner .= 'datenbank/';
        $this->_ZielDB = awisDatenbank::NeueVerbindung($Ziel);
        $this->_ZielDB->Oeffnen();
    }


    /**
     * Deployt die DDL und DML SQL Scrupte
     */
    public function DeployDDLDML(){
        $Dateien = $this->_ermittleDateien($this->_VerteilungsOrdner,array('sql'));

        foreach ($Dateien as $Datei){
            if($this->_PruefeDDLProtokoll($Datei)){
                $this->_DateiAusfuehren($Datei,$this->_ZielDB);
                $this->_SchreibeDDLProtokoll($Datei);
            }
        }
    }

    /**
     * Ermittelt die Dateien eines Ordners inklusive Unterordner
     *
     * @param       $Pfad
     * @param array $ErlaubteDateiendungen
     * @return array
     */
    private function _ermittleDateien($Pfad, array $ErlaubteDateiendungen){
        //Alle Erlaubten Dateiendungen uppern
        $tmpDateiendungen = array();
        foreach ($ErlaubteDateiendungen as $Endung){
            $tmpDateiendungen[] = mb_strtoupper($Endung);
        }
        $ErlaubteDateiendungen = $tmpDateiendungen;
        unset($tmpDateiendungen);

        $Pfad = realpath($Pfad);

        $Dateien = array();
        $QuellOrdner = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($Pfad), RecursiveIteratorIterator::SELF_FIRST);
        foreach($QuellOrdner as $Element => $ElementObjekt){
            //Dateiendung ermitteln und uppern
            $Dateiendung = explode('.',$Element);
            $Dateiendung = $Dateiendung[count($Dateiendung)-1];
            $Dateiendung = mb_strtoupper($Dateiendung);
            if(in_array($Dateiendung,$ErlaubteDateiendungen)){ //Interessiert mich diese Datei?
                $Dateien[] = $Element;
            }
        }
        return $Dateien;
    }

    /**
     * Ermittelt und deployt die Mergescripte
     */
    public function DeployMergeScripte(){
        $MergeOrdner = $this->_VerteilungsOrdner . 'WiederholendeMergeScripte/';

        $Dateien = $this->_ermittleDateien($MergeOrdner,array('sql'));
        foreach ($Dateien as $Datei){
            $this->_DateiAusfuehren($Datei,$this->_QuellDB);
        }
    }

    /**
     * F�hrt die SQL Datei aus
     * @param $Datei
     * @param awisDatenbank $DB
     */
    private function _DateiAusfuehren($Datei,awisDatenbank $DB){

        $Inhat = file_get_contents($Datei);

        if($Inhat!=''){

            $TmpDatei = '/tmp/depoyment.sql';

            if(file_exists($TmpDatei)){
                unlink($TmpDatei);
            }

            $Inhat .= PHP_EOL . ' exit; '; //SQl Script beenden

            file_put_contents($TmpDatei,$Inhat);

            // Befehlszeile f�r den Export
            $Kennwort = $this->_AWISBenutzer->ParameterLesen('AWIS_PW');
            $Befehl = 'exit | ';
            $Befehl .= $this->_AWISBenutzer->ParameterLesen('SQLPLUS');
            $Befehl .= '  -S AWIS/'.$Kennwort.'@'.$this->_Ziel.' @'.$TmpDatei;

            $RetVal = '';
            system($Befehl,$RetVal);

            if($RetVal){
                $Ausgabe = date('c');
                $Ausgabe .= 'Datei: ' . $Datei . 'ausgef�hrt. ';
                $Ausgabe .= $RetVal . ' ';
                echo $Ausgabe . PHP_EOL;
            }
            else{
                $Ausgabe = date('c');
                $Ausgabe .= '; ERROR; SQL Plus konnte nicht ausgef�hrt werden. ';
                $Ausgabe .= $RetVal . ' ';
                echo $Ausgabe . PHP_EOL;
            }
        }
    }


    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Schreibt die Pr�fsumme der Datei ins Protokoll, damit sie das n�chste mal nicht mehr ausgef�hrt wird
     * @param $Dateiname
     */
    private function _SchreibeDDLProtokoll($Dateiname){

        $SQL  ='INSERT';
        $SQL .=' INTO AWIS_DDL_DEPLOYMENT';
        $SQL .='   (';
        $SQL .='     XDD_DATUM,';
        $SQL .='     XDD_SHA1,';
        $SQL .='     XDD_DATEINAME,';
        $SQL .='     XDD_STATUS,';
        $SQL .='     XDD_MELDUNG';
        $SQL .='   )';
        $SQL .='   VALUES';
        $SQL .='   (';
        $SQL .='     sysdate,';
        $SQL .=   $this->_ZielDB->WertSetzen('XDD','T',sha1_file($Dateiname));
        $SQL .=' ,' . $this->_ZielDB->WertSetzen('XDD','T',$Dateiname);
        $SQL .='  ,   1,';
        $SQL .=' \'OK\'';
        $SQL .='   )';

        $this->_ZielDB->Ausfuehren($SQL,'',true,$this->_ZielDB->Bindevariablen('XDD'));
    }

    /**
     * Pr�ft ob die Datei schon da war
     * @param $Dateiname
     * @return bool
     */
    private function _PruefeDDLProtokoll($Dateiname){
        $SQL  ='SELECT ';
        $SQL .='   XDD_KEY,';
        $SQL .='   XDD_DATUM,';
        $SQL .='   XDD_SHA1,';
        $SQL .='   XDD_DATEINAME,';
        $SQL .='   XDD_STATUS,';
        $SQL .='   XDD_MELDUNG';
        $SQL .=' FROM ';
        $SQL .='   AWIS_DDL_DEPLOYMENT ';
        $SQL .=' WHERE XDD_SHA1 = '.$this->_ZielDB->WertSetzen('XDD','T',sha1_file($Dateiname));

        $rsXDI = $this->_ZielDB->RecordSetOeffnen($SQL,$this->_ZielDB->Bindevariablen('XDD'));

        if($rsXDI->AnzahlDatensaetze()>0){
            return false;
        }else{
            return true;
        }
    }


    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText .= date('d.m.Y H:i:s');
            $this->_DebugText .=  ': ' . $Typ;
            $this->_DebugText .=  ': ' . $Text;
            $this->_DebugText .=  PHP_EOL;
        }
    }

}


