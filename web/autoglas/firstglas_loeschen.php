<?php

/**
 * Maske zum loeschen von Kassendaten
 * ************************************************************************
 * Parameter :
 * --- keine ---
 * R�ckgabeparameter:
 * --- keine ---
 * ***********************************************************************
 * Erstellt:
 * @author ??.08.2009 Beierl Christian
 * @version 1.0
 * �nderungen:
 * 30.09.2011 OP : Kassendaten sollen beim loeschen Zukuenftig nicht mehr
 *                 aus der Tabelle rausgenommen und in Loesch-Tabelle verschoben
 *                 werden sondern nur noch als Geloescht markiert werden. Daher
 *                 folgende �nderung:
 *                 Insert in FKASSENDATEN_GELOESCHT Tabelle auskommentiert
 *                 + Delete in FKASSENDATEN auskommentiert daf�r einen
 *                 Update der FKASSENDATEN Feld FKA_STATUS --> geloescht-Flag
 * ***********************************************************************
 */
global $AWIS_KEY1;

$TextKonserven = array();
$TextKonserven[] = array('Wort', 'WirklichLoeschen');
$TextKonserven[] = array('Wort', 'Ja');
$TextKonserven[] = array('Wort', 'Nein');

try
{
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    //$Form->DebugAusgabe(1,$_REQUEST);

    $Form->Formular_Start();

    if (isset($_GET['Del']))
    {
        $Tabelle = 'FKA';
        $Key = $_GET['Del'];

        $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

        $SQL = "SELECT * from FKASSENDATEN WHERE FKA_KEY=" . $DB->FeldInhaltFormat('NO', $Key);
        $rsDelKassenDaten = $DB->RecordSetOeffnen($SQL);

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KEY'], 60, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KENNUNG'], 66, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_FILID'], 50, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_DATUM'], 100, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_UHRZEIT'], 90, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BSA'], 50, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_WANR'], 80, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_AEMNR'], 130, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_ATUNR'], 80, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_MENGE'], 60, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BETRAG'], 70, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'], 100, '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BEMERKUNG'], 100, '');
        $Form->ZeileEnde();

        $FGKZeile = 0;
        $DS = 0;

        While (!$rsDelKassenDaten->EOF())
        {
            $Form->Erstelle_ListenFeld('FKA_KEY', $rsDelKassenDaten->FeldInhalt('FKA_KEY'), 0, 60, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_KENNUNG', $rsDelKassenDaten->FeldInhalt('FKA_KENNUNG'), 0, 66, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_FILID', $rsDelKassenDaten->FeldInhalt('FKA_FILID'), 0, 50, false, ($FGKZeile % 2), '', '');
            $DATUM = str_replace("00:00:00", "", $rsDelKassenDaten->FeldInhalt('FKA_DATUM'));
            $Form->Erstelle_ListenFeld('FKA_DATUM', $DATUM, 0, 100, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_UHRZEIT', $rsDelKassenDaten->FeldInhalt('FKA_UHRZEIT'), 0, 90, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_BSA', $rsDelKassenDaten->FeldInhalt('FKA_BSA'), 0, 50, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_WANR', $rsDelKassenDaten->FeldInhalt('FKA_WANR'), 0, 80, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_AEMNR', $rsDelKassenDaten->FeldInhalt('FKA_AEMNR'), 13, 130, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_ATUNR', $rsDelKassenDaten->FeldInhalt('FKA_ATUNR'), 0, 80, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_MENGE', $rsDelKassenDaten->FeldInhalt('FKA_MENGE'), 0, 60, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_BETRAG', $rsDelKassenDaten->FeldInhalt('FKA_BETRAG'), 0, 70, false, ($FGKZeile % 2), 'font-weight:bold', '', 'N2');
            $Form->Erstelle_ListenFeld('FKA_KFZKENNZ', $rsDelKassenDaten->FeldInhalt('FKA_KFZKENNZ'), 0, 100, false, ($FGKZeile % 2), '', '');
            $Form->Erstelle_ListenFeld('FKA_BEMERKUNG', $rsDelKassenDaten->FeldInhalt('FKA_BEMERKUNG'), 15, 100, false, ($FGKZeile % 2), '', '');
            $Form->ZeileEnde();

            $FGKZeile++;
            $rsDelKassenDaten->DSWeiter();
        }

        $Form->SchreibeHTMLCode('<form name=frmLoeschen action=./autoglas_Main.php?cmdAktion=' . $_GET['cmdAktion'] . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . ' method=post>');

        $Form->Formular_Start();
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
        $Form->ZeileEnde();

        $Form->Erstelle_HiddenFeld('FGK_KEY', $_REQUEST['txtFGK_KEY']);
        $Form->Erstelle_HiddenFeld('FKA_KEY', $Key);


        $Form->ZeileStart();
        $Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '');
        $Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '');

        $Form->ZeileEnde();

        $Form->SchreibeHTMLCode('</form>');

        $Form->Formular_Ende();

        die();
    }
    elseif ($_REQUEST['cmdLoeschenOK'])
    {
        /*
          $SQL = "SELECT * from FKASSENDATEN WHERE FKA_KEY=".$DB->FeldInhaltFormat('NO', $_REQUEST['txtFKA_KEY']);

          $rsDelKassenDaten = $DB->RecordSetOeffnen($SQL);
          //----------FKASSENDATEN_GELOESCHT-------------
          //---------------------------------------------

          if($rsDelKassenDaten->AnzahlDatensaetze() != 0)
          {
          $SQL = "INSERT INTO FKASSENDATEN_GELOESCHT (FKG_KEY,FKG_KENNUNG,FKG_FILID,";
          $SQL .= "FKG_DATUM,FKG_UHRZEIT,FKG_BSA,FKG_WANR,FKG_AEMNR,";
          $SQL .= "FKG_ATUNR,FKG_MENGE,FKG_BETRAG,FKG_KFZKENNZ,FKG_BEMERKUNG,FKG_IMP_KEY,FKG_STATUS,FKG_USER,FKG_USERDAT";
          $SQL .= ") VALUES (";
          $SQL .= ' ' . $rsDelKassenDaten->FeldInhalt('FKA_KEY');
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_KENNUNG'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('NO',$rsDelKassenDaten->FeldInhalt('FKA_FILID'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('DU',$rsDelKassenDaten->FeldInhalt('FKA_DATUM'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_UHRZEIT'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_BSA'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_WANR'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_AEMNR'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_ATUNR'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('N2',$rsDelKassenDaten->FeldInhalt('FKA_MENGE'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('N2',$rsDelKassenDaten->FeldInhalt('FKA_BETRAG'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_KFZKENNZ'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('T',$rsDelKassenDaten->FeldInhalt('FKA_BEMERKUNG'),false);
          $SQL .= ',' . $DB->FeldInhaltFormat('NO',0,false);
          $SQL .= ',' . $DB->FeldInhaltFormat('NO',0,True);
          $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
          $SQL .= ',SYSDATE';
          $SQL .= ')';

          $DB->Ausfuehren($SQL);


          } */

        //$SQL = "Delete from FKASSENDATEN WHERE FKA_KEY=".$DB->FeldInhaltFormat('NO',  $_REQUEST['txtFKA_KEY']);
        // Kassendatensatz als gel�scht markieren
        $SQL = 'UPDATE FKASSENDATEN SET FKA_STATUS = 1 WHERE FKA_KEY=' . $DB->FeldInhaltFormat('NO', $_REQUEST['txtFKA_KEY']);

        $DB->Ausfuehren($SQL);
    }
}
catch (awisException $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>
