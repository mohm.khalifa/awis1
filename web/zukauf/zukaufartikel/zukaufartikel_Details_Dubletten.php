<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('ZLH','%');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','ZusammenfassungBestellungen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);		// Artikelstamm
	if(($Recht10001&256)==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZLA_ARTIKELNUMMER DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT ZLA_KEY, ZLA_ARTIKELNUMMER, ZLA_BEZEICHNUNG';
	$SQL .= ', ZLA_USER, ZLA_USERDAT';
	$SQL .= ', ZLH_BEZEICHNUNG, ZLH_KUERZEL';
	$SQL .= ', ROW_NUMBER() OVER(ORDER BY ZLA_KEY) AS ZEILENNR';
	$SQL .= ', (SELECT COUNT(*) FROM Zukaufbestellungen WHERE ZUB_ZLA_KEY = ZLA_KEY) AS ANZ';
	$SQL .= ' FROM Zukaufartikel ';
	$SQL .= ' INNER JOIN Zukauflieferantenhersteller ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ' WHERE ZLA_KEY<>'.$AWIS_KEY1;
	$SQL .= ' AND ZLA_ARTIKELNUMMER = (SELECT ZLA_ARTIKELNUMMER ';
	$SQL .= '              FROM Zukaufartikel WHERE zla_key = '.$AWIS_KEY1.')';

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$Form->DebugAusgabe(1,$SQL);
	$rsZLA = $DB->RecordsetOeffnen($SQL);
	$Form->Formular_Start();

	if($rsZLA->AnzahlDatensaetze()>0)
	{
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['DUBLETTE_AUFLOESEN_KURZ'],80);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZLA_USERDAT'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZLA_USERDAT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_USERDAT'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZLA_USER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZLA_USER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_USER'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZLA_ARTIKELNUMMER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZLA_ARTIKELNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'],200,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZLA_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZLA_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'],180,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZLH_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZLH_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_BEZEICHNUNG'],250,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ANZAHL'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ANZAHL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['wrd_AnzahlDSZeilen'],100,'',$Link);

		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZLA->EOF())
		{
			$Form->ZeileStart();

			$Form->Erstelle_Checkbox('DUBL_'.$rsZLA->FeldInhalt('ZLA_KEY'), 'off', 80,true, 'on','',$AWISSprachKonserven['ZLA']['ttt_DublettenAufloesen']);
			$Form->Erstelle_ListenFeld('*ZLA_USERDAT',$rsZLA->FeldInhalt('ZLA_USERDAT'),20,150,false,($DS%2),'','','DU');
			$Form->Erstelle_ListenFeld('*ZLA_USER',$rsZLA->FeldInhalt('ZLA_USER'),20,150,false,($DS%2),'','','T');
			$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Dubletten&ZLA_KEY='.$rsZLA->FeldInhalt('ZLA_KEY');
			$Form->Erstelle_ListenFeld('*ZLA_ARTIKELNUMMER',$rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER'),20,200,false,($DS%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('*ZLA_BEZEICHNUNG',$rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'),20,180,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZLH_BEZEICHNUNG',$rsZLA->FeldInhalt('ZLH_BEZEICHNUNG'),20,250,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ANZAHL',$rsZLA->FeldInhalt('ANZAHL'),20,100,false,($DS%2),'','','NxT');

			$Form->ZeileEnde();

			$rsZLA->DSWeiter();
			$DS++;
		}


		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Dubletten';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}


	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>