<?php
require_once 'awisBerichte.inc';
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBarcode.inc');

class ber_Gutschein
    extends awisBerichte
{
    private $PosLiefVorhanden = true;
    private $PosLiefSeite1 = true;
    private $_Gutschein = true;
    private $MaxSeitenAnzahl = 0;
    private $Barcode;
    private $Art='';
    private $_rsLieferschein;
    private $_rsPositionen;
    private $_Gesamtbetrag=0;
    private $_Gesamtanzahl=0;

    /**
     * Prozedur zum Initialisieren des Berichts
     *
     * @see awisBerichte::init()
     */
    public function SetzeDateipfad($DateiPfad)
    {
        $this->_Dateiname = $DateiPfad;
    }

    public function init(array $Parameter)
    {
        global $AWIS_KEY1;
        $AWIS_KEY1 = $_GET['GSK_AUFTRAGNR'];
        $this->Art = $_GET['ART'];
        $this->Barcode= new awisBarcode($this);

        $TextKonserven[] = array('Fehler', 'err_KeineDaten');
        $TextKonserven[] = array('Liste', 'lst_ALLE_0');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Soll');
        $TextKonserven[] = array('Wort', 'Anschrift_ATU_Handel');
        $TextKonserven[] = array('Wort', 'PersoenlHaftGesell_ATU_Handel');
        $TextKonserven[] = array('Wort', 'Geschaeftsfuehrer_ATU_Handel');
        $TextKonserven[] = array('Wort', 'Bankverb_ATU_Handel');
        $TextKonserven[] = array('FGK', 'txt_BruttoOEM');
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');
        $TextKonserven[] = array('Ausdruck', 'txt_HinweisAusdruckIntern');
        $TextKonserven[] = array('GSD', '*');
        $TextKonserven[] = array('GKU', '*');
        $TextKonserven[] = array('Wort', 'Datum');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);

        //Daten beschaffen
        $SQL  ='SELECT gskopf.GSK_KEY';
        $SQL .=', gskopf.GSK_AUFTRAGNR';
        $SQL .=', gskopf.GSK_KST';
        $SQL .=', gku.GKU_NAME1';
        $SQL .=', gku.GKU_NAME2';
        $SQL .=', gku.GKU_KONZERN';
        $SQL .=', gku.GKU_STRASSE';
        $SQL .=', gku.GKU_HN';
        $SQL .=', gku.GKU_PLZ';
        $SQL .=', gku.GKU_ORT';
        $SQL .=', gskopf.gsk_gku_key';
        $SQL .=', gskopf.GSK_STATUS';
        $SQL .=', gku.GKU_ANREDE';
        $SQL .=', gku.GKU_TITEL';
        $SQL .=', gku.GKU_TELEFON';
        $SQL .=', gku.GKU_EMAIL';
        $SQL .=', gku.GKU_MOBILTELEFON';
        $SQL .=', gku.GKU_FAX';
        $SQL .=', gku.GKU_LAN_CODE';
        $SQL .=', gku.GKU_POSTFACH';
        $SQL .=', gku.GKU_POSTFACHPLZ';
        $SQL .=', gskopf.GSK_AUFTRAGGEBER';
        $SQL .=', gskopf.GSK_GSV_KEY';
        $SQL .=', gsvor.GSV_BEZEICHNUNG';
        $SQL .=', gsvor.GSV_PDFVORLAGE';
        $SQL .=', gskopf.GSK_KUNDE_INTERN';
        $SQL .=', gku.GKU_USTID';
        $SQL .=', gskopf.gsk_freitext';
        $SQL .=', pers.NAME';
        $SQL .=', pers.VORNAME';
        $SQL .=', lan.LAN_LAND';
        $SQL .=' FROM GUTSCHEINKOPFDATEN gskopf';
        $SQL .=' LEFT JOIN GUTSCHEINKUNDEEXTERN gku on gskopf.GSK_GKU_KEY = gku.gku_KEY';
        $SQL .=' LEFT JOIN GUTSCHEINVORLAGE gsvor on gskopf.GSK_GSV_KEY = gsvor.GSV_KEY';
        $SQL .=' LEFT JOIN LAENDER lan on gsvor.GSV_LAND = lan.LAN_ATU_STAAT_NR';
        $SQL .=' LEFT JOIN PERSONAL_KOMPLETT pers on gskopf.GSK_AUFTRAGGEBER = pers.PERSNR';
        $SQL .= ' WHERE gskopf.GSK_AUFTRAGNR = \'' . $AWIS_KEY1 . '\'';
        $SQL .=' Group by gskopf.GSK_KEY';
        $SQL .=', gskopf.GSK_AUFTRAGNR';
        $SQL .=', gskopf.GSK_KST';
        $SQL .=', gku.GKU_NAME1';
        $SQL .=', gku.GKU_NAME2';
        $SQL .=', gku.GKU_KONZERN';
        $SQL .=', gku.GKU_STRASSE';
        $SQL .=', gku.GKU_HN';
        $SQL .=', gku.GKU_PLZ';
        $SQL .=', gku.GKU_ORT';
        $SQL .=', gskopf.gsk_gku_key';
        $SQL .=', gskopf.GSK_STATUS';
        $SQL .=', gku.GKU_ANREDE';
        $SQL .=', gku.GKU_TITEL';
        $SQL .=', gku.GKU_TELEFON';
        $SQL .=', gku.GKU_EMAIL';
        $SQL .=', gku.GKU_MOBILTELEFON';
        $SQL .=', gku.GKU_FAX';
        $SQL .=', gku.GKU_POSTFACH';
        $SQL .=', gku.GKU_POSTFACHPLZ';
        $SQL .=', gskopf.GSK_AUFTRAGGEBER';
        $SQL .=', gskopf.GSK_GSV_KEY';
        $SQL .=', gsvor.GSV_BEZEICHNUNG';
        $SQL .=', gsvor.GSV_PDFVORLAGE';
        $SQL .=', gskopf.GSK_AUFTRAGGEBER';
        $SQL .=', gskopf.GSK_KUNDE_INTERN';
        $SQL .=', gskopf.gsk_freitext';
        $SQL .=', pers.NAME';
        $SQL .=', pers.VORNAME';
        $SQL .=', gku.GKU_LAN_CODE';
        $SQL .=', gku.GKU_USTID';
        $SQL .=', lan.LAN_LAND';
        $SQL .=' ORDER BY gskopf.GSK_KEY desc';

        $this->_rsLieferschein=$this->_DB->RecordSetOeffnen($SQL);

        $SQL  ='SELECT gs.GPD_POSITION,';
        $SQL .=' gs.GPD_AUFTRAGNR,';
        $SQL .=' gs.GPD_GUTSCHEINNR,';
        $SQL .=' gs.GPD_PRUEFZ,';
        $SQL .=' gs.GPD_KOMMENTAR,';
        $SQL .=' gs.GPD_DATUM,';
        $SQL .=' gs.GPD_UNIT,';
        $SQL .=' gs.GPD_BUCHART,';
        $SQL .=' pos.GSP_BETRAG';
        $SQL .=' FROM GUTSCHEINE gs';
        $SQL .=' inner join GUTSCHEINPOSDATEN pos on gs.GPD_AUFTRAGNR = pos.GSP_AUFTRAGNR';
        $SQL .=' and gs.GPD_POSITION = pos.GSP_POSITION';
        $SQL .=' WHERE pos.GSP_AUFTRAGNR=\'' . $AWIS_KEY1 . '\'';
        $SQL .=' AND gs.GPD_BUCHART <> \'S\'';
        $SQL .=' GROUP BY gs.GPD_POSITION,';
        $SQL .=' gs.GPD_AUFTRAGNR,';
        $SQL .=' gs.GPD_GUTSCHEINNR,';
        $SQL .=' gs.GPD_PRUEFZ,';
        $SQL .=' gs.GPD_KOMMENTAR,';
        $SQL .=' gs.GPD_DATUM,';
        $SQL .=' gs.GPD_UNIT,';
        $SQL .=' gs.GPD_BUCHART,';
        $SQL .=' pos.GSP_BETRAG';
        $SQL .=' ORDER BY gs.GPD_POSITION asc';

        $this->_rsPositionen=$this->_DB->RecordSetOeffnen($SQL);
    }
    
    /**
     * Eigentliches PDF erzeugen
     *
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {
        // PDF Attribute setzen
        $this->SetTitle('Gutscheindruck');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins($this->_Parameter['LinkerRand'], 40, $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->AliasNbPages();
        $this->_BerichtsVorlage = $this->_rsLieferschein->FeldInhalt('GSV_PDFVORLAGE');

        if(file_exists($this->_BerichtsVorlage))
        {
            $this->setSourceFile($this->_BerichtsVorlage);
        }
        else
        {
            $this->_BerichtsVorlage='';
        }

        if($this->Art=='Gutschein')
        {
            if($this->_BerichtsVorlage=='/daten/web/dokumente/vorlagen/Gutschein_ZEISS_500.pdf')
            {
                //ZEISS Gutscheine drucken
                $this->SetAutoPageBreak(true,5);
                $this->GutscheinZEISSDrucken();
            }
            else
            {
                //PDF-Gutscheine
                $this->SetAutoPageBreak(true, 5);
                $this->GutscheinDrucken();
            }
        }
        else
        {
            //Lieferschein als letzte Seite anf�gen
            $this->SetAutoPageBreak(true, 40);
            $this->_Gutschein=false;
            $this->NeueSeite();
            $this->LieferscheinKopf();
            $this->LieferscheinAdressdaten();
            $this->LieferscheinPositionsdaten();
            $this->LieferscheinBetragsblock();
            $this->LieferscheinAnzahlblock();
            $this->LieferscheinUnterschrift();
        }


        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName. '.pdf', 'I');
                break;
            case 3:            // Als String zur�ckliefern
                return ($this->Output($this->_BerichtsName. '.pdf', 'S'));
                break;
            case 4:         // Als Datei speichern
                $this->Output($this->_Dateiname. '.pdf', 'F');
                break;
        }
    }
    
    private function GutscheinDrucken()
    {
        $this->_Gutschein=true;
        
        while (!$this->_rsPositionen->EOF())
        {
            $this->NeueSeite();

            $this->_Zeile = $this->_Parameter['GSBetrag'];
            $this->_Spalte = $this->_Parameter['LinkerRandGS'];
            //Gutscheinbetrag
            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse'] + 20);
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteGSBetrag'],$this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N2',$this->_rsPositionen->FeldInhalt('GSP_BETRAG')), 0, 0, 'L', 0);
            $this->_Spalte+=59;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteGSBetrag'],$this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->FeldInhalt('GPD_UNIT'), 0, 0, 'R', 0);
            
            //Gutscheincode
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
            $this->_Zeile = $this->_Parameter['GSCode'];
            $this->_Spalte = $this->_Parameter['LinkerRandGS'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Barcode->Code128($this->_Spalte,$this->_Zeile,$this->_rsPositionen->FeldInhalt('GPD_GUTSCHEINNR') . $this->_rsPositionen->FeldInhalt('GPD_PRUEFZ'),55,12);
            $this->_Zeile = $this->_Parameter['GSNr'];
            $this->_Spalte = $this->_Parameter['LinkerRandGS'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteGSCode'],$this->_Parameter['ZeilenhoeheBarcode'], $this->_rsPositionen->FeldInhalt('GPD_GUTSCHEINNR') . ' ' . $this->_rsPositionen->FeldInhalt('GPD_PRUEFZ'), 0, 0, 'C', 0);

            $this->_rsPositionen->DSWeiter();
        }
    }

    private function GutscheinZEISSDrucken()
    {
        $this->_Gutschein=true;

        while (!$this->_rsPositionen->EOF())
        {
            $this->NeueSeite();

            $this->_Zeile = $this->_Parameter['GSBetrag'];
            $this->_Spalte = $this->_Parameter['LinkerRandGS'] +5;
            //Gutscheinbetrag
            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse'] + 20);
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->_Spalte+=35;
            $this->SetXY($this->_Spalte, $this->_Zeile);

            //Gutscheincode
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
            $this->_Zeile = $this->_Parameter['GSCode'];
            $this->_Spalte = $this->_Parameter['LinkerRandGS'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Barcode->Code128($this->_Spalte,$this->_Zeile,$this->_rsPositionen->FeldInhalt('GPD_GUTSCHEINNR') . $this->_rsPositionen->FeldInhalt('GPD_PRUEFZ'),55,12);
            $this->_Zeile = $this->_Parameter['GSNr'];
            $this->_Spalte = $this->_Parameter['LinkerRandGS'] ;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteGSCode'],$this->_Parameter['ZeilenhoeheBarcode'], $this->_rsPositionen->FeldInhalt('GPD_GUTSCHEINNR') . ' ' . $this->_rsPositionen->FeldInhalt('GPD_PRUEFZ'), 0, 0, 'C', 0);

            $this->_rsPositionen->DSWeiter();
        }
    }

    private function LieferscheinKopf()
    {
        $this->_Zeile = $this->_Parameter['ObererRand'];
        $this->_Spalte = $this->_Parameter['LinkerRand'] ;

        //Auftragsnummer
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GSD']['GSD_AUFTRAGNUMMER'] . ':', 0, 0, 'L', 0);
        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsLieferschein->FeldInhalt('GSK_AUFTRAGNR'), 0, 0, 'L', 0);

        //Erstelldatum der GS; PDF Erzeugung
        $this->_Spalte = $this->_Parameter['LinkerRand'] ;
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GSD']['GSD_PDFERSTELLUNG'] . ':', 0, 0, 'L', 0);
        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], date('d.m.Y'), 0, 0, 'L', 0);

        //Kostenstelle
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GSD']['GSD_KOSTENSTELLE'] . ':', 0, 0, 'L', 0);
        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsLieferschein->FeldInhalt('GSK_KST'), 0, 0, 'L', 0);

        //Land wo der Gutschein g�ltig ist
        $this->_Spalte = $this->_Parameter['LinkerRand'] ;
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GKU']['GKU_LAN_CODE'] . ':', 0, 0, 'L', 0);
        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsLieferschein->FeldInhalt('LAN_LAND'), 0, 0, 'L', 0);
        
        $this->_Zeile += 5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 30;
    }

    private function LieferscheinPosUeberschrift()
    {
        if($this->PosLiefSeite1 == true)
        {
            $this->_Zeile = $this->_Parameter['PositionRand'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->PosLiefSeite1=false;
        }
        else
        {
            $this->_Zeile = 50;
            $this->_Spalte = $this->_Parameter['LinkerRand'];
        }

        $Zellenbreite = self::Seitenbreite() - (2 * $this->_Parameter['LinkerRand']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], '', 1, 0, 'C', 0);

        //�berschrift Positionen
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreitePos'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GSD']['GSD_POSITIONEN'], 0, 0, 'C', 0);

        $this->_Spalte += $this->_Parameter['BreitePos'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteGS'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GSD']['GSD_GUTSCHEINNUMMER'], 0, 0, 'R', 0);

        $this->_Spalte += $this->_Parameter['BreiteGS'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteBetrag'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GSD']['GSD_BETRAG'], 0, 0, 'R', 0);
    }

    private function LieferscheinAdressdaten()
    {
        $this->_Zeile = $this->_Parameter['AdresseRand'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $Zellenbreite =  self::Seitenbreite() - $this->_Parameter['LinkerRand'];

        //Anschrift ATU
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], 'ATU Auto-Teile-Unger GmbH Co. KG, Postfach 2554, 92615 Weiden i.d.Opf', 0, 0, 'L', 0); //TODO

        //Anschrift des Kunden; Unterscheidung zwischen Kunde intern/extern
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseHervorgehoben']);
        
        switch ($this->_rsLieferschein->FeldInhalt('GKU_ANREDE'))
        {
            case 1:
                $Anrede = 'Herr';
            case 2:
                $Anrede = 'Frau';
            case 3:
                $Anrede = 'Firma';
        }
        
        //if Kunde Extern gef�llt => Extern; Ansonsten Intern
        $Kunde=$this->_Form->Format('N0',$this->_rsLieferschein->FeldInhalt('GSK_GKU_KEY'));

        if($Kunde<>'')
        {
            $this->_Zeile += $this->_Parameter['Zeilenhoehe']+ 2;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsLieferschein->FeldInhalt('GKU_KONZERN'), 0, 0, 'L', 0);

            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $Anrede . ' ' . $this->_rsLieferschein->FeldInhalt('GKU_TITEL'), 0, 0, 'L', 0);

            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsLieferschein->FeldInhalt('GKU_NAME1') . ' ' . $this->_rsLieferschein->FeldInhalt('GKU_NAME2'), 0, 0, 'L', 0);

            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], ($this->_rsLieferschein->FeldInhalt('GKU_STRASSE') . ' ' . $this->_rsLieferschein->FeldInhalt('GKU_HN'))?($this->_rsLieferschein->FeldInhalt('GKU_STRASSE') . ' ' . $this->_rsLieferschein->FeldInhalt('GKU_HN')):'Postfach ' . $this->_rsLieferschein->FeldInhalt('GKU_POSTFACH'), 0, 0, 'L', 0);

            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'],
            $this->_rsLieferschein->FeldInhalt('GKU_LAN_CODE') . ' - ' . $this->_rsLieferschein->FeldInhalt('GKU_PLZ') . ' ' . $this->_rsLieferschein->FeldInhalt('GKU_ORT'), 0, 0, 'L', 0);

            $this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GKU']['GKU_USTID']. ': ' .$this->_rsLieferschein->FeldInhalt('GKU_USTID'), 0, 0, 'L', 0);
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        }
        else
        {
            $this->_Zeile += $this->_Parameter['Zeilenhoehe']+ 2;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsLieferschein->FeldInhalt('GSK_KUNDE_INTERN'), 0, 0, 'L', 0);
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        }
    }

    private function LieferscheinPositionsdaten()
    {
        $this->_Gesamtbetrag = 0;
        $this->_Gesamtanzahl =0;
        $this->_Zeile = $this->_Parameter['PositionRand'];
        $this->_rsPositionen->DSErster();
        
        while (!$this->_rsPositionen->EOF())
        {
            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreitePos'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->DSNummer()+1, 0, 0, 'C', 0);

            $this->_Spalte += $this->_Parameter['BreitePos'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteGS'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->FeldInhalt('GPD_GUTSCHEINNR') . '|' . $this->_rsPositionen->FeldInhalt('GPD_PRUEFZ'), 0, 0, 'R', 0);

            $this->_Spalte += $this->_Parameter['BreiteGS'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteBetrag'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->FeldInhalt('GSP_BETRAG','N2'), 0, 0, 'R', 0);

            $this->_Gesamtbetrag += str_replace(',','.',$this->_rsPositionen->FeldInhalt('GSP_BETRAG','N2'));

            $this->_rsPositionen->DSWeiter();
            $this->_Zeile = $this->GetY();
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->_Gesamtanzahl+=1;
        }
        $this->PosLiefVorhanden = false;
    }

    private function LieferscheinBetragsblock()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Line($this->_Spalte,$this->_Zeile,self::Seitenbreite() - $this->_Parameter['LinkerRand'],$this->_Zeile);

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetFillColor(209, 209, 209);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(28, $this->_Parameter['Zeilenhoehe'] + 2, $this->_AWISSprachkonserven['GSD']['GSD_BETRAGGES'] . ':', '', 0, 'L', true);

        $this->SetFillColor(209, 209, 209);
        $this->_Spalte = $this->_Parameter['LinkerRand'] + ($this->_Parameter['BreiteBetragGes']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(22, $this->_Parameter['Zeilenhoehe'] + 2, $this->_Form->Format('N2',$this->_Gesamtbetrag), '', 0, 'R', true);
    }

    private function LieferscheinAnzahlblock()
    {
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        //GESAMTZahlbetrag NETTO
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetFillColor(209, 209, 209);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(28, $this->_Parameter['Zeilenhoehe'] + 2, $this->_AWISSprachkonserven['GSD']['GSD_ANZAHLGESAMT'] . ':', '', 0, 'L', true);

        $this->SetFillColor(209, 209, 209);
        $this->_Spalte = $this->_Parameter['LinkerRand'] + ($this->_Parameter['BreiteAnzahlGes']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(22, $this->_Parameter['Zeilenhoehe'] + 2, $this->_Gesamtanzahl, '', 0, 'R', true);
    }

    private function LieferscheinUnterschrift()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe']*5;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Line($this->_Spalte,$this->_Zeile,self::Seitenbreite() - $this->_Parameter['LinkerRand'],$this->_Zeile);
        $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['Wort']['Datum'], 0, 0, 'C', 0);

        $this->_Spalte += $this->_Parameter['BreiteEmpfangUnterschrift'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteEmpfangUnterschrift'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['GSD']['GSD_UNTERSCHRIFT'], 0, 0, 'R', 0);
    }
    
    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage();
    }

    /**
     * Automatische Fu�zeile
     *
     * @see TCPDF::Footer()
     */
    public function Footer()
    {
        if($this->_Gutschein==false)
        {
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->_Zeile = $this->_Parameter['FussRand'];

            $BreiteFussZeile = (self::Seitenbreite() - ($this->_Parameter['LinkerRand'])) / 3;

            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_AWISSprachkonserven['Wort']['Anschrift_ATU_Handel'], 0, 'L', 0);

            $this->_Spalte += $BreiteFussZeile;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_AWISSprachkonserven['Wort']['PersoenlHaftGesell_ATU_Handel'], 0, 'L', 0);

            $this->_Zeile = $this->GetY() + 2;
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_AWISSprachkonserven['Wort']['Geschaeftsfuehrer_ATU_Handel'], 0, 'L', 0);

            $this->_Spalte += $BreiteFussZeile;
            $this->_Zeile = $this->_Parameter['FussRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_AWISSprachkonserven['Wort']['Bankverb_ATU_Handel'], 0, 'L', 0);
        }
    }
    
    /**
     * Automatische Kopfzeile
     *
     * @see TCPDF::Header()
     */
    public function Header()
    {
        if($this->_Gutschein==true)
        {
            if ($this->_BerichtsVorlage != '')
            {
                $pdf_vorlage = $this->ImportPage(1);        // $this->PageNo() == 1:1?2; Wenn PageNo = 1 Dann soll die Vorlage Seite 1 herangezogen werden
                                                            // Wenn PageNo = 2 Dann soll die Vorlage Seite 2 benutzt werden; Seite 2 ist von vielen Vorlagen eine leere Seite => AutoPageBreak
                $this->useTemplate($pdf_vorlage);
            }

            //Druck Gutscheine
            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse'] + 10);
            $this->_Zeile = 15;
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
        }
        else
        {
            //Druck Lieferschein
            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse'] + 20);
            $this->_Zeile = 15;
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell(80, $this->_Parameter['Zeilenhoehe'], 'Lieferschein', 0, 0, 'L', 0);

            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
            $this->_Zeile = 30;
            $this->_Spalte = $this->_Parameter['LinkerRand'] + 150;

            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell(80, $this->_Parameter['Zeilenhoehe'], 'Seite: ' . $this->PageNo() . ' von {nb}', 0, 0, 'L', 0);

            if ($this->PosLiefVorhanden == true)
            {
                $this->LieferscheinPosUeberschrift();
                $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            }
            else
            {
                $this->_Zeile = 40;
            }

            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->MaxSeitenAnzahl++;
        }
    }
}

?>