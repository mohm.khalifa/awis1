INSERT INTO rechte ( xrc_id,xrc_recht ) VALUES (660, 'Stammdaten: Filialkompetenzen');

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
  660,
  0,
  'Kompetenzen anzeigen'
);

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
  660,
  1,
  'Kompetenzen bearbeiten'
);

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
  660,
  2,
  'Kompetenzen hinzuf�gen'
);

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
  660,
  3,
  'Kompetenzen l�schen'
);

Commit;


INSERT INTO webmasken (
    xwm_id,
    xwm_bezeichnung,
    xwm_bemerkung,
    xwm_user,
    xwm_userdat,
    xwm_status
) VALUES (
    660,
    'Filialkompetenzen',
    'Filialkompetenz-Stammdaten',
    'Meyer Andre',
    '25.10.2018',
    'A'
);

commit;

INSERT INTO webmenuepunkte (
    xmp_key,
    xmp_xme_key,
    xmp_bezeichnung,
    xmp_xrc_id,
    xmp_stufe,
    xmp_gruppenid,
    xmp_sortierung,
    xmp_link,
    xmp_user,
    xmp_userdat,
    xmp_gueltigab,
    xmp_gueltigbis
) VALUES (
    257,
    10,
    'Filialkompetenzen-Stammdaten',
    660,
    0,
    0,
    90,
    '/stammdaten/filialkompetenzen/filialkompetenzen_Main.php?cmdAktion=Suche',
    'Andre Meyer',
    '25.10.2018',
    '25.10.2018',
    '31.12.2030'
);

commit;


INSERT INTO webmaskenregister (xrg_id,xrg_xwm_id,xrg_bezeichnung,xrg_beschriftung,xrg_aktion,xrg_seite,xrg_bemerkung,xrg_xrc_id,xrg_rechtestufe,xrg_sortierung,xrg_neuehilfe) VALUES (6600,660, 'Suche', 'Suche','./filialkompetenzen_Main.php?cmdAktion=Suche','filialkompetenzen_Suche.php','Suche',660,0,1,0);
INSERT INTO webmaskenregister (xrg_id,xrg_xwm_id,xrg_bezeichnung,xrg_beschriftung,xrg_aktion,xrg_seite,xrg_bemerkung,xrg_xrc_id,xrg_rechtestufe,xrg_sortierung,xrg_neuehilfe) VALUES (6601,660, 'Details', 'Details','./filialkompetenzen_Main.php?cmdAktion=Details','filialkompetenzen_Details.php','Details',660,0,2,0);

commit;

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('MENUE_241_TEXT','MENUE','Menuepunktueberschrift','Filialkompetenzen');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('MENUE_241_HINWEIS','MENUE','Menuepunkthinweis','Filialkompetenzen');

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('tit_FILIALKOMPETENZEN','TITEL','Titel f�r das Filialkompetenzen-Stammdaten-Modul','Filialkompetenzen');

commit;

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('reg_6600','Register','Register zu Abteilungsbearbeitung','Suche');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('reg_6601','Register','Register zu Abteilungsbearbeitung','Details');

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('ttt_6600','Register','Tooltipptext','Suche');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('ttt_6601','Register','Tooltipptext','Details');

INSERT INTO programmparameter (xpp_id,xpp_bezeichnung,xpp_default,xpp_ueberschreibbar,xpp_bemerkung,xpp_kodiert,xpp_datentyp) VALUES (660,'Formular_FIK',';',255,'Suchparameter bei dem Filialkompetenzen-Stammdaten-Modul',0,'T');

commit;

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('FIK_BESCHREIBUNG','FIK','Beschreibungen bei den Filialkompetenzen','Beschreibung');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('FIK_BEZEICHNUNG','FIK','','Filialkompetenzen');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('FIK_KEINDS','FIK','','Es wurden kein Datensatz gefunden.');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('FIK_KOMPETENZFILIALEN','FIK','','Filialen mit dieser Kompetenz');

commit;

INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('FIK_KOMPETENZFILIALEN_ADD','FIK','','Hinzuf�gen von weiteren Filialen');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('FIK_POPUP_LOESCHEN','FIK','','Es sind noch #ANZ_FIL# Filiale(n) mit dieser Kompetenz gespeichert. Diese Verbindungen werden beim L�schen entfernt. Sind Sie sich sicher, dass die Kompetenz gel�scht werden soll?');
INSERT INTO TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE) values ('FIK_POPUP_TEXT','FIK','Ueberschrift des PopUps in Filialkompetenzen','Sollen alle untergeordneten Datens�tze gel�scht werden?');

commit;
