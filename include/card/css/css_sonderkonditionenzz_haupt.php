/* css_sonderkonditionenzz_haupt.php */


<?php
switch($intSchriftgroesse)
{
	case 12:	//Werte f�r die Breite der Spalten

				$intKundennr=80;				//Breite der Spalte Kundennr
				$intFirma=290;					//Breite der Spalte Firma
				$intZahlungsziel=40;			//Breite der Spalte Zahlungsziel / Tage
				$intSkonto=55;					//Breite der Spalte Skonto
				$intBemerkung=270;				//Breite der Spalte Bemerkung
				break;				
	case 16:	//Werte f�r die Breite der Spalten

				$intKundennr=105;				//Breite der Spalte Kundennr
				$intFirma=370;					//Breite der Spalte Firma
				$intZahlungsziel=55;			//Breite der Spalte Zahlungsziel / Tage
				$intSkonto=70;					//Breite der Spalte Skonto
				$intBemerkung=350;				//Breite der Spalte Bemerkung
				break;
			
	default:	//Werte f�r die Breite der Spalten

				$intKundennr=80;				//Breite der Spalte Kundennr
				$intFirma=80;					//Breite der Spalte Firma
				$intZahlungsziel=80;			//Breite der Spalte Zahlungsziel / Tage
				$intSkonto=80;					//Breite der Spalte Skonto
				$intBemerkung=80;				//Breite der Spalte Bemerkung
}
?>


.speibut							{	height:<?php echo $intElementhoehe; ?>;
										width:<?php echo $intElementhoehe; ?>;
										display:none; }
												
.text								{	vertical-align:top;	}

.titel								{	font-weight:bold; }
td.titel							{	text-align:center; }

.kundennr							{	width:<?php echo $intKundennr; ?>px; 
										text-align:center;	}

.firma								{	width:<?php echo $intFirma; ?>px; }

.zahlungsziel						{	width:<?php echo $intZahlungsziel; ?>px; 
										text-align:center;	}

.skonto								{	width:<?php echo $intSkonto; ?>px;
										text-align:center;	}

.bemerkung							{	width:<?php echo $intBemerkung; ?>px; }

#knr								{	display:none; }			/*Felder neue Zeile sind versteckt*/
#fir,#zaz,#skt,#bem					{	visibility:hidden; }
