<? 
require_once("config.inc.php");		// DB-Befehle

function startElement($parser, $name, $attrs) {
global $x_aktArray;
global $x_aktElement;
global $x_array_namen;
global $x_debug;

	if ($x_aktElement != 'nix') {
		if (($x_debug & 8) == 8) {
			echo "<hr>Neues Array $x_aktElement<hr>";
		}
		$x_aktArray = strtolower($x_aktElement) . "_array";
		${$x_aktArray} = array();
		$x_array_namen[] = $x_aktArray;
	}
	if (($x_debug & 4) == 4) {
		echo "&lt;$name&gt;";
	}
	$x_aktElement = $name;
}

function endElement($parser, $name) {
global $x_aktArray;
global $x_aktElement;
global ${$x_aktArray};
global $x_array_namen;
global $x_debug;

	if (strcmp($x_aktArray, strtolower($name) . "_array") == 0) {
		if (($x_debug & 8) == 8) {
		    echo "<hr>Ende Array $name (aktArray = $x_aktArray)<br>";
		}
		array_pop($x_array_namen);
		$tmpArrayName = end($x_array_namen);
global ${$tmpArrayName};
		${$tmpArrayName}[substr(strtoupper($x_aktArray), 0, strlen($x_aktArray) - 6)] = ${$x_aktArray};
		$x_aktArray = end($x_array_namen);
	}
	$x_aktElement = 'nix';
	if (($x_debug & 4) == 4) {
		echo "&lt;/$name&gt;<br>";
	}
}

function chardata($parser, $data){
global $x_aktArray;
global ${$x_aktArray};
global $x_aktElement;
global $x_debug;

	$data = chop($data);
	if ($x_aktElement != 'nix' && strlen($data) > 0) {
		if (($x_debug & 1) == 1) {
			echo $x_aktElement . ": " . $data . "<br>";
		}
		if (($x_debug & 4) == 4) {
			echo $data;
		}
		${$x_aktArray}[$x_aktElement] = $data;
	}
}

	
function xml_to_array($fp){
global $x_aktArray;
global $x_aktElement;
global $x_aktArray;
global ${$x_aktArray};
global $x_debug;
	
	$x_aktElement = 'nix';
	$xml_parser = xml_parser_create();
	xml_set_element_handler($xml_parser, "startElement", "endElement");
	xml_set_character_data_handler($xml_parser, "chardata");
	
	foreach ($fp as $data) {
		if (($x_debug & 16) == 16) {
			print $data . "<br>";
		}
		if (!xml_parse($xml_parser, $data)) {
			sprintf("XML-Fehler %s auf Zeile %d",
				xml_error_string(xml_get_error_code($xml_parser)),
				xml_get_current_line_number($xml_parser));
			print "Mist";
			print xml_error_string(xml_get_error_code($xml_parser));
		}
	}
	
	xml_parser_free($xml_parser);
	if (($x_debug & 2) == 2) {
		print_r (${$x_aktArray});
	}
	return ${$x_aktArray};
}

?>
