<?php
ob_start();  //Einschalten der Pufferung von Ausgaben

require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

//Funktionen

//Gibt Firmenname zur�ck, bei �bergabe der Kundennr

function firma_ermitteln($strKundennr)
{
	global $hdlDb2Verbindung;
	global $tabKunden;
	global $tabAdressen;
	
	//Firma ermitteln
	$strSql='select a."Kunden_Art",b."Firma",b."Abteilung",b."Titel",b."Name",b."Vorname",b."Geschlecht"
			from '.$tabKunden.' as a left outer join '.$tabAdressen.' as b
			on a."Kunden_Nr"=b."Kunden_Nr" and b."Adress_Art"=\'01\'
			where a."Kunden_Nr"=\''.$strKundennr.'\'';
	
	$arrAdressen=abfrage("db2",$strSql);
	
	$strFirma='';
	
	if($arrAdressen["zeilen"])
	{
		$arrAdressen=$arrAdressen["trefferarray"];
		$strAdressfeld = '';
		
		if(trim($arrAdressen["Kunden_Art"][0])=='PR')
		{
			if(trim($arrAdressen["Firma"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Firma"][0])." ";
				
			if(trim($arrAdressen["Abteilung"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Abteilung"][0]);
		}
		
		$strAdressfeld=trim($strAdressfeld);
		
		//$strGeschlecht=trim($arrAdressen["Geschlecht"][0]);
		
		if(trim($arrAdressen["Vorname"][0])!=''||trim($arrAdressen["Name"][0])!='')
		{
			if(trim($arrAdressen["Firma"][0])!=''||trim($arrAdressen["Abteilung"][0])!='')
				$strAdressfeld.=" / ";
		
			/*if($strGeschlecht=='1')
				$strAdressfeld.="Herrn ".$strAnredeEnde;
			elseif($strGeschlecht=='2')
				$strAdressfeld.="Frau ".$strAnredeEnde;
			
			if(trim($arrAdressen["Titel"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Titel"][0])." ";*/
			
			if(trim($arrAdressen["Name"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Name"][0])." ";
			
			if(trim($arrAdressen["Vorname"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Vorname"][0]);
		}
		
		$strFirma=$strAdressfeld;
	}
	return $strFirma;
}

//Ende Funktionen




if(!isset($strAbrZeitraum))
{
?>
<html>
<head>
	<title>Zahlungsjournal</title>
	<?php include("card/css/css_card_master.php"); ?>
	<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
	<script language="JavaScript">
	
	function init()		//Aufruf �ber <body onload="init();">
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
		document.ondblclick = zoom;
		
		var obj=document.getElementById('AbrZeitr');
		
		standsetzen(obj);
	}
		
	//Setzt Betrag in Feld Stand
	function standsetzen(objekt)
	{
		var strBetrag=objekt.options[objekt.selectedIndex].value;
		
		
		if(strBetrag>=0)
			document.getElementById("stand").style.color='black';
		else
			document.getElementById("stand").style.color='red';
		
		document.getElementById("stand").value=eurobetrag(strBetrag);
	}
	
	//Wenn Abrechnungszeitraum ge�ndert wird
	
	function datumswechsel()
	{
		var obj = window.event.srcElement;
		
		standsetzen(obj);
	}
	
	//Bei Klick auf Checkbox offen / alle
	
	function wechsel(strId)
	{
		document.getElementById(strId).click();
				
		if(document.getElementById('alle').checked)
		{
			document.getElementById('AbrZeitr').style.display='none';
			document.getElementById('AbrZeitrAlle').style.display='inline';
			var obj=document.getElementById('AbrZeitrAlle');
		}
		else
		{
			document.getElementById('AbrZeitr').style.display='inline';
			document.getElementById('AbrZeitrAlle').style.display='none';
			var obj=document.getElementById('AbrZeitr');
		}
		standsetzen(obj);
	}
	
	//Bei Klick auf Button PDF erstellen
	function pdf_erstellen()
	{
		//Datumspr�fung
		var datObj = document.getElementById("datum");
		
		var strDatum ='';
		
		if(datObj.value!='')
		{
			if(!datumspruefung(datObj))
				return;
			else
				strDatum = '&strDatum='+datObj.value;	
		}
	
		//Werte abfragen
					
		if(document.getElementById('AbrZeitrAlle').style.display=='none')
			var obj=document.getElementById('AbrZeitr');
		else
			var obj=document.getElementById('AbrZeitrAlle');
			
		//Abrechnungszeitraum
		var strAbrZeitraum=obj.options[obj.selectedIndex].text;
		
		if(document.getElementById('sortdat').checked)
			strSort='sortdat';
		else
			strSort='sortkun';
		
		self.location.href="<?php echo $PHP_SELF;?>?strAbrZeitraum="+strAbrZeitraum+"&strSort="+strSort+strDatum;
	}
	
	</script>
</head>
<body onload="init();">
<?php
$strSql="select vzj_abrechnungszeitraum zeitraum,sum(vzj_betrag) summe from view_zahlungsjournal group by vzj_abrechnungszeitraum order by vzj_abrechnungszeitraum";
	
$arrAbrZeitrDat = abfrage("ora",$strSql);
$intZeilen = $arrAbrZeitrDat["zeilen"];
$arrAbrZeitrDat = $arrAbrZeitrDat["trefferarray"];

$strAbrZeitrDat='';
$strAbrZeitrDatAlle='';

for($i=0;$i<$intZeilen;$i++)
{
	$dblBetrag=(double)punktersetztkomma($arrAbrZeitrDat["SUMME"][$i]);
	
	$strDatum=$arrAbrZeitrDat["ZEITRAUM"][$i];
	
	$strOption="<option value=\"".$dblBetrag."\"";
	
	$strAbrZeitrDatAlle.=$strOption;

	if($dblBetrag!=0)
	{
		$strAbrZeitrDat.=$strOption." style=\"color:red;\">".$strDatum."</option>";
		$strAbrZeitrDatAlle.=" style=\"color:red;\">".$strDatum."</option>";
	}
	else
	{
		$strAbrZeitrDatAlle.=">".$strDatum."</option>";
	}
}

?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="100%">
<tr><td align="center">

	<table border="0" cellpadding="0" cellspacing="0" align="center" id="tabelleaussen">
		<tr><td colspan="3" class="fett" align="center">Zahlungsjournal</td></tr>
		<tr><td colspan="3" class="unten">&nbsp;</td></tr>
		<tr>
			<td class="fett">Abrechnungszeitraum</td>
			<td rowspan="2" class="rechts">&nbsp;</td>
			<td align="center">
			<select id="AbrZeitr" onchange="datumswechsel();">
			<?php echo $strAbrZeitrDat; ?>
			</select>
			<select id="AbrZeitrAlle" onchange="datumswechsel();" style="display:none;">
			<?php echo $strAbrZeitrDatAlle; ?>
			</select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="center" class="fett">offen&nbsp;<input type="checkbox" id="offen" onclick="wechsel('alle');" checked>&nbsp;
			alle&nbsp;<input type="checkbox" id="alle" onclick="wechsel('offen');">
			</td>
		</tr>
		<tr>
			<td colspan="3" class="obenunten">&nbsp;</td>
		</tr>
		<tr>
		<td class="fett">Buchungsdatum</td>
		<td>&nbsp;</td>
		<td align="center"><input type="text" id="datum" style="text-align:left;"></td>
		</tr>
		<tr>
			<td colspan="3" class="obenunten">&nbsp;</td>
		</tr>
		<tr>
		<td rowspan="2" class="fett">Sortierung</td>
		<td rowspan="2">&nbsp;</td>
		<td class="fett" valign="top"><input type="checkbox" id="sortdat" onclick="document.getElementById('sortkun').click();" checked>&nbsp;Datum</td>
		</tr>
		<tr><td class="fett" valign="middle"><input type="checkbox" id="sortkun" onclick="document.getElementById('sortdat').click();">&nbsp;Kundenname</td></tr>
		<tr>
			<td colspan="3" class="obenunten">&nbsp;</td>
		</tr>
		<tr>
		<td class="fett">Stand</td>
		<td>&nbsp;</td>
		<td align="center"><input type="text" id="stand" readOnly style="text-align:right;"></td>
		</tr>
		
		<tr>
			<td colspan="3" class="oben">&nbsp;</td>
		</tr>
		<tr><td colspan="3" align="center"><button type="button" onclick="pdf_erstellen();">PDF erstellen</button></td></tr>
	</table>
</td></tr>
</table>
</body>
</html>

<?php

flush();		//Ausgabe-Puffer leeren
}
else
{
	$strSql="select sum(vzj_betrag) summe from view_zahlungsjournal where 
	to_date(vzj_abrechnungszeitraum,'DD.MM.RR')='".$strAbrZeitraum."'
	and vzj_betragart=19 and vzj_tabelle=0";
	
	$arrSummeZeitraum = abfrage("ora",$strSql);
	$intZeilen = $arrSummeZeitraum["zeilen"];
	$arrSummeZeitraum = $arrSummeZeitraum["trefferarray"];
	
	
	$strSummeRechnungen=eurobetrag($arrSummeZeitraum["SUMME"][0]);
	$strRechnungsSumme="Rechnungssumme: ".$strSummeRechnungen;
	
	$arrDat=explode(".",$strAbrZeitraum);
	
	setlocale("LC_TIME","de_DE");
	$strAbrDat=strftime("%B %Y",mktime(0,0,0,(integer)$arrDat[1],(integer)$arrDat[0],(integer)$arrDat[2]))." (".$strAbrZeitraum.")";
	
	$strOrderBy='';
	
	if($strSort=='sortdat')
	{
		$strOrderBy=' order by VZJ_DATUM,VZJ_KUNDEN_NR,VZJ_RNR,VZJ_BETRAG';
	}
	else
	{
		$strOrderBy=' order by VZJ_KUNDEN_NR,VZJ_DATUM,VZJ_RNR,VZJ_BETRAG';
	}
	
	$strSql="select VZJ_RNR, VZJ_KUNDEN_NR, VZJ_DATUM, VZJ_TEXT, VZJ_BETRAG, VZJ_KONTOAUSZUGSDATUM
	from view_zahlungsjournal where 
	to_date(vzj_abrechnungszeitraum,'DD.MM.RR')='".$strAbrZeitraum."'
	and (vzj_betragart!=19 and vzj_tabelle=0 or vzj_tabelle=1)";
	
	if(isset($strDatum)&&$strDatum!='')
	{
		$strSql.=" and vzj_datum='".$strDatum."'";
		$strAbrDatKonstr=$strAbrDat." / Buchungsdatum: ".$strDatum;
	}
	else
		$strAbrDatKonstr=$strAbrDat;
		
	$strSql.=$strOrderBy;
		
	$arrZahlJournal = abfrage("ora",$strSql);
	$intZeilen = $arrZahlJournal["zeilen"];
	$arrZahlJournal = $arrZahlJournal["trefferarray"];
	
		
	require_once("card/card_pdf_zahlungsjournal.inc.php");	// PDF
	
	ob_end_clean();	//Verwirft den Inhalt des Ausgabe-Puffers und deaktiviert die Pufferung
		
	$dblSeitenrandLinks=10;
	$dblSeitenrandOben=10;
	$dblSeitenrandRechts=10;
	$dblSeitenrandUnten=10;
	
	
	
	
	$pdf=new PDF_ZJ('L','mm','A4',0,0,$dblSeitenrandLinks,$dblSeitenrandOben,$dblSeitenrandRechts,$dblSeitenrandUnten,$strAbrDatKonstr,$strRechnungsSumme);	//A4-Seite im Querformat mit Seitenrand 10 mm
	
	if($strSort=='sortdat')
	{
		$arrFirma=array();
	
		for($i=0;$i<$intZeilen;$i++)
		{
			//Firma ermitteln
			
			$strKundennr=$arrZahlJournal["VZJ_KUNDEN_NR"][$i];
			
			if(empty($arrFirma[$strKundennr]))
			{
				$strFirma=firma_ermitteln($strKundennr);
				$arrFirma[$strKundennr]=$strFirma;
			}
			else
			{
				$strFirma=$arrFirma[$strKundennr];
			}
			
			$strBuchText=$arrZahlJournal["VZJ_TEXT"][$i];
			
			if($arrZahlJournal["VZJ_KONTOAUSZUGSDATUM"][$i]!='')
				$strBuchText.=", lt. Kto-Ausz. v. ".$arrZahlJournal["VZJ_KONTOAUSZUGSDATUM"][$i];
						
			//Datenzeile 
				
			$dblSchriftgroesse=10.5;
			$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
			$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
			
			$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array($arrZahlJournal["VZJ_DATUM"][$i],$strFirma,eurobetrag($arrZahlJournal["VZJ_BETRAG"][$i]),
				$strBuchText,$arrZahlJournal["VZJ_KUNDEN_NR"][$i],$arrZahlJournal["VZJ_RNR"][$i]),
				array(1,1,1,1,1,1),$pdf->arrKopfPos,array('L','L','R','L','L','L'),array(1,1,1,1,1,1),array('L','L','L','L','L','L'));
		}
	}
	else
	{
		$arrFirma=array();
		$arrFirmaGroKl=array();
		$arrKundenNr=array();
		$arrTrans=array("�"=>"AE","�"=>"OE","�"=>"UE");
			
		for($i=0;$i<$intZeilen;$i++)
		{		
			$strKundennr=$arrZahlJournal["VZJ_KUNDEN_NR"][$i];
		
			if(empty($arrFirma[$strKundennr]))
			{
				$strFirma=firma_ermitteln($strKundennr);
				
				$arrFirmaGroKl[$strKundennr]=$strFirma;
				$arrFirma[$strKundennr]=strtr(strtoupper($strFirma),$arrTrans);
			}
			else
			{
				$strFirma=$arrFirmaGroKl[$strKundennr];
			}
			
			if(empty($arrKundenNr[$strKundennr]))
			{
				$arrKundenNr[$strKundennr]=array();
			}
							
			$arrKundenNr[$strKundennr][]=array($arrZahlJournal["VZJ_DATUM"][$i],$arrZahlJournal["VZJ_BETRAG"][$i],
				$arrZahlJournal["VZJ_TEXT"][$i],$arrZahlJournal["VZJ_KUNDEN_NR"][$i],$arrZahlJournal["VZJ_RNR"][$i],$arrZahlJournal["VZJ_KONTOAUSZUGSDATUM"][$i],
				$strFirma);
		}
		
		asort($arrFirma,SORT_STRING);
		
		foreach($arrFirma as $index => $strFirma)
		{
			$arrZwischen=$arrKundenNr[$index];
			
			for($i=0;$i<count($arrZwischen);$i++)
			{
				$strBuchText=$arrZwischen[$i][2];
			
				if($arrZwischen[$i][5]!='')
					$strBuchText.=", lt. Kto-Ausz. v. ".$arrZwischen[$i][5];
			
				$dblSchriftgroesse=10.5;
				$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
				$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
			
				$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array($arrZwischen[$i][0],$arrZwischen[$i][6],eurobetrag($arrZwischen[$i][1]),
				$strBuchText,$arrZwischen[$i][3],$arrZwischen[$i][4]),
				array(1,1,1,1,1,1),$pdf->arrKopfPos,array('L','L','R','L','L','L'),array(1,1,1,1,1,1),array('L','L','L','L','L','L'));
			
			}
		}
	}		//Ende else
	
	
	$strSql="select nvl(summe_ze,0) summe_ze,nvl(summe_rg,0) summe_rg,nvl(summe_mg,0) summe_mg ,nvl(summe_rg,0)+nvl(summe_mg,0) summe_gesamt from
			(select sum(VZJ_BETRAG) summe_rg
			from view_zahlungsjournal where 
			to_date(vzj_abrechnungszeitraum,'DD.MM.RR')='".$strAbrZeitraum."'
			and vzj_tabelle=0),
			(select sum(VZJ_BETRAG) summe_mg
			from view_zahlungsjournal where 
			to_date(vzj_abrechnungszeitraum,'DD.MM.RR')='".$strAbrZeitraum."'
			and vzj_tabelle=1),
			(select sum(VZJ_BETRAG) summe_ze
			from view_zahlungsjournal where 
			to_date(vzj_abrechnungszeitraum,'DD.MM.RR')='".$strAbrZeitraum."'
			and vzj_tabelle=0 and vzj_betragart!=19)";
	
	$arrSummen = abfrage("ora",$strSql);
	$intZeilen = $arrSummen["zeilen"];
	$arrSummen = $arrSummen["trefferarray"];
	
	$dblSchriftgroesse=12;
	$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
	$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
	
	//Zeile Summe Rechnungen
	
		$aktY=$pdf->GetY();
	
	if($aktY>=150)
	{
		$pdf->intTitelzeile=0;
		$pdf->AddPage();
	}
	
	$pdf->Ln($dblZeilenhoehe); //Zeilenumbruch
	
	$aktX=$pdf->GetX();
	$aktY=$pdf->GetY();
	
	$pdf->SetLineWidth($pdf->dblLinienStaerke);
	
	
	
	$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',0);
			
	$pdf->SetXY($aktX,$aktY);
	
	$pdf->SetLineWidth(0.4);
	
	$aktX=$pdf->GetX();
	$aktY=$pdf->GetY();
	
	$pdf->Cell(0,$dblZeilenhoehe,"",'T',1,'L',0);
			
	$pdf->SetXY($aktX,$aktY);
	
	$strHeute=strftime("%d.%m.%y");
	
	$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Rechnungssumme ".$strAbrDat,"",$strSummeRechnungen,
				"","",""),
				array(0,0,0,0,0,0),$pdf->arrKopfPos,array('L','L','R','L','L','L'));
	
	
	//Zeile Summe Zahlungseing�nge (ohne Mahngeb�hren)
	
	$pdf->SetLineWidth($pdf->dblLinienStaerke);
	
	$aktX=$pdf->GetX();
	$aktY=$pdf->GetY();
	
	$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',0);
	
	$pdf->SetXY($aktX,$aktY);
	
	$pdf->SetLineWidth(0.4);
	
	$aktX=$pdf->GetX();
	$aktY=$pdf->GetY();
	
	$pdf->Cell(0,$dblZeilenhoehe,"",'B',1,'L',0);
			
	$pdf->SetXY($aktX,$aktY);
	
	$strHeute=strftime("%d.%m.%y");
	
	$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Summe Zahlungseing�nge (ohne Mahnungen)","",eurobetrag($arrSummen["SUMME_ZE"][0]),
				"","",""),
				array(0,0,0,0,0,0),$pdf->arrKopfPos,array('L','L','R','L','L','L'));
	
	$pdf->SetLineWidth(0.4);
	
	//Zeile offener Rechnungsbetrag
	
	$aktX=$pdf->GetX();
	$aktY=$pdf->GetY();
	
	$pdf->SetFillColor(192,192,192);			
	$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',1);
			
	$pdf->SetXY($aktX,$aktY);
	
	$strHeute=strftime("%d.%m.%y");
	
	$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Offener Rechnungsbetrag","",eurobetrag($arrSummen["SUMME_RG"][0]),
				"","",""),
				array(0,0,0,0,0,0),$pdf->arrKopfPos,array('L','L','R','L','L','L'));
				
	//Zeile offene Mahngeb�hren
	
	$pdf->Ln($dblZeilenhoehe); //Zeilenumbruch
	
	$aktX=$pdf->GetX();
	$aktY=$pdf->GetY();
	
	$pdf->SetFillColor(192,192,192);
	$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',1);
			
	$pdf->SetXY($aktX,$aktY);
	
	$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Offene Mahngeb�hren","",eurobetrag($arrSummen["SUMME_MG"][0]),
				"","",""),
				array(0,0,0,0,0,0),$pdf->arrKopfPos,array('L','L','R','L','L','L'));
				
	//Zeile offener Restbetrag
	
	$pdf->Ln($dblZeilenhoehe); //Zeilenumbruch
	
	$aktX=$pdf->GetX();
	$aktY=$pdf->GetY();
	
	$pdf->SetFillColor(192,192,192);
	$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',1);
			
	$pdf->SetXY($aktX,$aktY);
	
	$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Offener Restbetrag","",eurobetrag($arrSummen["SUMME_GESAMT"][0]),
				"","",""),
				array(0,0,0,0,0,0),$pdf->arrKopfPos,array('L','L','R','L','L','L'));
				
	$pdf->SetDisplayMode('fullwidth','continuous');
	
	$pdf->Output();
}	//Ende else



?>