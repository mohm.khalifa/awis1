<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['sucRRP_ATUNR']))
{
	var_dump($_GET);
	die();
}

try
{
	$DS=0;

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$Suchbegriffe = explode(',',$_GET['sucRRP_ATUNR']);
	
	$SQL = 'SELECT';
	$SQL .= ' RRP_KEY,';
	$SQL .= ' RRP_ATUNR';
	$SQL .= ' FROM REIFENPFLEGE';
	$SQL .= ' WHERE RRP_AST_KEY IS NOT NULL';
	$SQL .=	' AND RRP_ATUNR '.$DB->LikeOderIst($Suchbegriffe[0].'*',awisDatenbank::AWIS_LIKE_UPPER);

	$rsDaten = $DB->RecordSetOeffnen($SQL);
var_dump($DB->LetzterSQL());	

	$FelderBreiten=array(100);
	$FelderAusrichtung=array('L');
	$Felder=array('RRP_ATUNR');
	
	$TextKonserven[]=array('RRP','RRP*');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$i=0;
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift('',20);
	foreach($Felder AS $Feld)
	{
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP'][$Feld], $FelderBreiten[$i++]);
	}
	$Form->ZeileEnde();
	
	while(!$rsDaten->EOF())
	{
		$Form->ZeileStart('','Zeile'.$DS);
		echo '<div class='.(($DS%2)==0?'ListenFeldHell':'ListenFeldDunkel').'  style="width: 20px;">';
		echo '<input type=radio name="'.$_GET['Feldname'].'" value="'.$rsDaten->FeldInhalt('RST_KEY').'"';
		if($rsDaten->AnzahlDatensaetze()==1)
		{
			echo ' checked';
		}

		echo ' onchange="sucrst_suche_ZeileMarkieren('.$DS.')"';

		echo '></div>';

		$i=0;
		foreach($Felder AS $Feld)
		{
			echo '<div class='.(($DS%2)==0?'ListenFeldHell':'ListenFeldDunkel').' style="width:'.$FelderBreiten[$i].'px;';

			switch($FelderAusrichtung[$i])
			{
				case 'L':
					echo 'text-align:left;';
					break;
				case 'R':
					echo 'text-align:right;';
					break;
				case 'Z':
					echo 'text-align:center;';
					break;
				default:
					echo 'text-align:right;';
					break;
			}

			echo '">';
			echo $rsDaten->FeldInhalt($Felder[$i]);
			$i++;
			echo '</div>';
		}

		$DS++;
		$Form->ZeileEnde();

		$rsDaten->DSWeiter();
	}
		
}
catch (Exception $ex)
{
	$Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>