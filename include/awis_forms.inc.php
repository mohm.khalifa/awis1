<?php
/**
*
*	Formular Funktionen
*	Dieses Modul beinhaltet alle Funktionen, die f�r die (neue) Formulargestaltung
* 	der AWIS Oberfl�che verwendet werden.
*
* @author 		Sacha Kerres
* @copyright 	ATU
* @version  	Dez 2006
* @todo 		Es kommen noch weitere Funktionen hinzu
*********************************************************************************************/
/**
 * Diese Variable wird f�r das Setzen des Cursors �ber Seitengrenzen hinweg verwendet
 * Der Inhalt der Variable wird in den _details.php Seiten am Ende ausgelesen
 */
$CursorFeld='';
/**
 *  KEY-Feld zur Weitergabe von Keys in den Formularen
 */
$AWIS_KEY1 = '';
/**
 *  KEY-Feld zur Weitergabe von Keys in den Formularen
 */
$AWIS_KEY2 = '';
/**
 *  Aktuelle Sprache
 *  Die Variable wird druch awis_Header.php gesetzt
 */
$AWISSprache = '';


/**
 * Erstellt ein DIV Objekt f�r ein Eingabeformular
 * Muss aufgerufen werden, bevor eine awis_FORM Funktion verwendet wird
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version  20070501
 * @param string $Style (CSS Style)
 */
function awis_FORM_FormularStart($Style='')
{
	if($Style=='')
	{
		echo '<div class=EingabeFormular>';
	}
	else
	{
		echo '<div class=EingabeFormular style="'.$Style.'">';
	}
}

/**
 * Schlie�t ein Eingabeformular ab, das mit awis_FORM_Formularstart() ge�ffnet wurde
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20070501
 *
 */
function awis_FORM_FormularEnde()
{
	echo '</div>';
}


/**
 * Erstellt ein neues DIV Element f�r eine Bildschirmzeile in einem AWIS-Formular
 *
 * @param string $Style (CSS-Style)
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20070501

 */
function awis_FORM_ZeileStart($Style='')
{
	if($Style=='')
	{
		echo '<div class=EingabeZeile>';
	}
	else
	{
		echo '<div class=EingabeZeile style="'.$Style.'">';
	}
}

/**
 * Schlie�t eine Bildschirmzeile in einem AWIS Formular
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20070501
 *
 */
function awis_FORM_ZeileEnde()
{
	echo '</div>';
}


/**
 * Schreibt eine Meldung in ein AWIS Formular
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20070501
 * @param string $Text
 * @param int $Modus
 * @param string $Style
 */
function awis_FORM_Hinweistext($Text, $Modus=1, $Style='')
{
	echo '<div';

	switch ($Modus)
	{
		case 1:
			echo ' class=HinweisText';
			break;
	}

	if ($Style!='')
	{
		echo ' style=\''.$Style.'\'>';
		echo $Text;
		echo '</div>';
	}
	else
	{
		echo ' >';
		echo $Text;
		echo '</div>';
	}
}

/**
 * Eingabefeld
 *
 * @author Sacha Kerres
 * @version 20070315
 * @package awis_form
 * @param string $Name
 * @param string $Wert
 * @param int $Zeichen
 * @param int $Breite
 * @param boolean $AendernRecht
 * @param string $Style
 * @param string $InputStyle
 * @param string $Link
 * @param string $Format
 * @param string $Ausrichtung
 * @param string $ToolTippText
 * @param mixed $DefaultWert
 * @param int $MaxZeichen
 * @param string $Java
 * @param string $TastenKuerzel
 */
function awis_FORM_Erstelle_TextFeld($Name, $Wert, $Zeichen, $Breite,$AendernRecht=false,$Style='',$InputStyle='',$Link='', $Format='T',$Ausrichtung='L',$ToolTippText='',$DefaultWert='',$MaxZeichen=0,$Java = '', $Tastenkuerzel='')
{
//	$DIVStyle='';

	switch ($Format[0])
	{
		case 'D':
			if($Wert==null)
			{
				$Wert='';
			}
			else
			{
				if(isset($Format[1]) AND $Format[1]=='U')
				{
					$Wert = awis_PruefeDatum($Wert,true);
				}
				else
				{
					$Wert = awis_PruefeDatum($Wert,false);
				}
			}
			break;
		case 'N':
			if($Wert!=='')		// Nur, wenn was angegeben wurde.
			{
				$Wert = str_replace(',','.',str_replace('.',',',$Wert));

				if(isset($Format[1]))
				{
					$Wert = number_format($Wert,substr($Format,1),',','');
				}
				else
				{
					$Wert = number_format($Wert,2,',','');
				}
			}

			if($Ausrichtung=='')
			{
				if($AendernRecht)		// Inputbox formatieren
				{
					$InputStyle .= ';text-align:right;';
				}
				else
				{
					$Style .= ';text-align:right;';
				}
			}
			break;
		case 'W':
			$Wert = str_replace(',','.',str_replace('.',',',$Wert));
			if($Wert!='')
			{
				$Wert = number_format($Wert,2,',','.');
			}
			if($Ausrichtung=='')
			{
				if($AendernRecht)		// Inputbox formatieren
				{
					$InputStyle .= ';text-align:right;';
				}
				else
				{
					$Style .= ';text-align:right;';
				}
			}
			break;
		case 'T':		// Text
			$Wert = htmlspecialchars($Wert,ENT_QUOTES,'ISO-8859-15');
			break;
		default:
			break;
	}

	switch ($Ausrichtung)
	{
		case 'R':
			$Style .= ';text-align:right;';
			break;
		case 'L':
			$Style .= ';text-align:left;';
			break;
		case 'Z':
			$Style .= ';text-align:center;';
			break;
	}

	echo '<div class=EingabeFeld ';
	if($ToolTippText!='')
	{
		echo 'title="'.$ToolTippText.'"';
	}

	if($Breite==0)
	{
		echo ' style="'.$Style.'">';
	}
	elseif($Breite<0)
	{
		echo ' style="min-width:'.(-$Breite).';'.$Style.'">';
	}
	else
	{
		echo ' style="width:'.$Breite.';'.$Style.'">';
	}

	if($AendernRecht)
	{
		if($Name[0]=='*')			// Such-Feld
		{
			$Name = substr($Name,1);
			echo '<input class="InputText" name="suc'.$Name.'" type="text" size="'.$Zeichen.'"';
			echo ($InputStyle!=''?' style="'.$InputStyle.';"':'');			
			echo ($Tastenkuerzel==''?'':' accesskey="'.$Tastenkuerzel.'"');
			if($MaxZeichen>0)
			{
				echo ' maxlength="'.intval($MaxZeichen).'"';
			}

			if($Java!='')
			{
				echo ' '.$Java.' ';
			}

			echo ' value="'.$Wert.'">';
		}
		elseif($Name[0]=='#')		// Kein OLD bauen
		{
			$Name = substr($Name,1);
			echo '<input class=InputText name=txt'.$Name.' type=text size='.$Zeichen;
			echo ($Tastenkuerzel==''?'':' accesskey="'.$Tastenkuerzel.'"');
			if($MaxZeichen>0)
			{
				echo ' maxlength='.intval($MaxZeichen);
			}
			echo ' value=\''.$Wert.'\'>';
		}
		else 						// Normales Feld mit TXT und OLD
		{
			echo '<input name=old'.$Name.' type=hidden value=\''.$Wert.'\'>';
			if($Wert=='' AND $DefaultWert!='')
			{
				echo '<input class=InputText name=txt'.$Name.' type=text size='.$Zeichen;
				echo ($Tastenkuerzel==''?'':' accesskey="'.$Tastenkuerzel.'"');
				if($MaxZeichen>0)
				{
					echo ' maxlength='.intval($MaxZeichen);
				}
				if($Java!='')
				{
					echo ' '.$Java.' ';
				}
				echo ' value =\''.$DefaultWert.'\'';
			}
			else
			{
				echo '<input class=InputText name=txt'.$Name.' type=text size='.$Zeichen;
				echo ($Tastenkuerzel==''?'':' accesskey="'.$Tastenkuerzel.'"');
				if($MaxZeichen>0)
				{
					echo ' maxlength='.intval($MaxZeichen);
				}
				if($Java!='')
				{
					echo ' '.$Java.' ';
				}
				echo ' value=\''.$Wert.'\'';
			}
			echo ($InputStyle!=''?'style="'.$InputStyle.';"':'');
			echo '>';
		}
	}
	else
	{
		echo '' . ($Wert==''?'&nbsp;':$Wert) . '';
	}

	if($Link!='')
	{
		echo '<a href='.$Link.'><img class=EingabeFeldLink src=/bilder/ico_link.png></a>';
	}

	echo '</div>';
}

/**
 * Erstellt eine CHECKBOX
 *
 * @param string $Name
 * @param string $Wert
 * @param int $Breite
 * @param boolean $AendernRecht
 * @param string $JaWert
 * @param string $Style
 */
function awis_FORM_Erstelle_Checkbox($Name, $Wert, $Breite, $AendernRecht=false, $JaWert='on', $Style='', $ToolTippText = '')
{
	if($Breite==0)
	{
		echo '<div class=EingabeFeld style="'.$Style.'"';
	}
	elseif($Breite<0)
	{
		echo '<div class=EingabeFeld style="min-width:'.(-$Breite).';'.$Style.'"';
	}
	else
	{
		echo '<div class=EingabeFeld style="width:'.$Breite.';'.$Style.'"';
	}

	if($ToolTippText!='')
	{
		echo ' title="'.$ToolTippText.'"';
	}

	echo '>';
	if($AendernRecht)
	{
		if(substr($Name,0,1)!='*')
		{
			echo '<input type=hidden value="'.($Wert==$JaWert?$JaWert:'').'" name=old'.$Name.'>';
			echo '<input type=checkbox value="'.$JaWert.'" name=txt'.$Name;
		}
		else
		{
			echo '<input type=checkbox value="'.$JaWert.'" name=suc'.substr($Name,1);
		}
//		echo '<input type=checkbox value="'.$JaWert.'" name=txt'.$Name;
		if($Wert == $JaWert)
		{
			echo ' checked ';
		}

		echo '>';
	}
	else 		// Nur lesen
	{
		if($Wert == $JaWert)
		{
			echo 'X';
		}
		else
		{
			echo '_';
		}
	}

	echo '</div>';
}
/**
 * SELECT Feld erstellen
 *
 * @param string $Name
 * @param mixed $Wert
 * @param integer $Breite
 * @param boolean $AendernRecht
 * @param ressource $con
 * @param string $SQL
 * @param string $OptionBitteWaehlen
 * @param string $OptionDefault
 * @param integer $AnzeigeSpalte
 * @param string $Style
 * @param array $Zusatzoptionen
 * @param string $Script
 */
function awis_FORM_Erstelle_SelectFeld($Name, $Wert, $Breite=0, $AendernRecht=false, $con, $SQL, $OptionBitteWaehlen='', $OptionDefault='', $AnzeigeSpalte='', $Style='', $Zusatzoptionen=null, $Script='',$ToolTippText='')
{
	global $awisRSInfo;		// Datensatzinfos
	global $awisRSZeilen;
	global $awisRSSpalten;
	global $AWISSprache;

	if($Breite==0)
	{
		echo '<div class=EingabeFeld style="'.$Style.'"';
	}
	elseif($Breite<0)
	{
		echo '<div class=EingabeFeld style="min-width:'.(-$Breite).';'.$Style.'"';
	}
	else
	{
		echo '<div class=EingabeFeld style="width:'.$Breite.';'.$Style.'"';
	}

	if($ToolTippText!='')
	{
		echo ' title="'.$ToolTippText.'"';
	}

	echo '>';		// Ende DIV


//awis_Debug(1,$Zusatzoptionen);
	if($AendernRecht)
	{
		//echo '<input class=InputText name=txt'.$Name.' type=text size='.$Breite.' value=\''.$Wert.'\'>';
		$SuchFeld = false;
		if($Name[0]=='*')	// Suchfeld-> Kein old
		{
			$SuchFeld=true;
			$Name = substr($Name,1);
		}

		if(substr($SQL,0,3)=='***' OR substr($SQL,0,3)=='*F*')		// Daten suchen (AJAX)
		{
			$ZielFeld = '';
			$Daten = explode(";",substr($SQL,3));
			$Seite = $Daten[0];
			$QuellFeld = $Daten[1];
			$Param = (isset($Daten[2])?$Daten[2]:'');
			$WertDefault = (isset($Daten[3])?$Daten[3]:'');
			$TextDefault = (isset($Daten[4])?$Daten[4]:'');
			$FeldName = 'txt'.$Name;

			echo "\n";
			echo '<script type=text/javascript>';
			echo 'var Req'.$FeldName.'=null;';
			echo 'function lade_'.$FeldName.'(QuellFeld, ZielFeld)';
			echo '{';
			echo 'try {';
			echo '    Req'.$FeldName.' = new XMLHttpRequest();';
			echo '    } catch(e) {';
			echo '         if (!Req'.$FeldName.') {
    						try {
        					Req'.$FeldName.'  = new ActiveXObject("Msxml2.XMLHTTP");
							} catch(e) {
        						try {
            					Req'.$FeldName.'  = new ActiveXObject("Microsoft.XMLHTTP");
        						} catch(e) {
            						Req'.$FeldName.'  = null;
        						}
    						}
        				}
				}';
			echo '    Req'.$FeldName.'.onreadystatechange=callBack_'.$FeldName.';';

			$ParamListe = explode('&',$Param);
//echo '</script>';			
			
			$Param = '';
			foreach($ParamListe AS $Parameter)
			{
				$ParamPaar = explode('=',$Parameter);
				if(isset($ParamPaar[1]))
				{
					$Param .= '&'.trim($ParamPaar[0]).'=';
					if(substr($ParamPaar[1],0,1)=='*')
					{
						$Param .= '"+document.getElementsByName("'.substr($ParamPaar[1],1).'")[0].value+"';
					}
					else
					{
						$Param .= $ParamPaar[1];
					}
				}
			}
//var_dump($Param);die();			
			if(substr($SQL,0,3)=='*F*')		// Daten suchen mit Filter
			{
				echo '    Req'.$FeldName.'.open("Get","/daten/opt_'.$Seite.'.php?'.substr($Param,1).'&WERT="+document.getElementsByName("'.$QuellFeld.'")[0].value+"&FILTER="+document.getElementsByName("suc'.substr($QuellFeld,3).'")[0].value+"",true);';
			}
			else
			{
				echo '    Req'.$FeldName.'.open("Get","/daten/opt_'.$Seite.'.php?'.substr($Param,1).'&WERT="+document.getElementsByName("'.$QuellFeld.'")[0].value+"",true);';
			}

			echo '    Req'.$FeldName.'.setRequestHeader("Content-Type","application/x-www-form-urlencoded");';
			echo '    Req'.$FeldName.'.send("");}';
//			echo '}';

			echo 'function callBack_'.$FeldName.'()';
			echo '{';
			echo '    if(Req'.$FeldName.'.readyState==4)';
			echo '	  {';
			echo '     var Feld = document.getElementsByName("'.$FeldName.'")[0];';
			echo '     var Optionen = "";';

			if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
			{
				//************************************
				// Code f�r Firefox
				//************************************
				if($WertDefault!='')
				{
					echo '     Optionen += \'<option value="'.$WertDefault.'">'.$TextDefault.'</option>\';';
				}
				echo '     Feld.innerHTML = Optionen + Req'.$FeldName.'.responseText.split("#~#")[1];';

			}
			else
			{
				//************************************
				// Code f�r den IE
				//************************************
				echo '		var SelectDaten = Req'.$FeldName.'.responseText.split("#~#")[1];';
				echo '		var Daten = SelectDaten.split("#+#");';
				echo '		document.getElementsByName("'.$FeldName.'")[0].options.length=0;';
				echo '		for(var i=1; i < Daten.length; i++)
							{
			   					var oOption = document.createElement("OPTION");
			   					oOption.value=Daten[i];
		 						i++;
			   					oOption.text=Daten[i];
			   					document.getElementsByName("'.$FeldName.'")[0].add(oOption);
							}';
			}
			// Defaultwert setzen
			//echo '     document.getElementsByName("'.$FeldName.'")[0].value= Req'.$FeldName.'.responseText.split("#~#")[0];';
			echo '          if(Req'.$FeldName.'.responseText.split("#~#")[0]!="")';
			echo '     		{document.getElementsByName("'.$FeldName.'")[0].value= Req'.$FeldName.'.responseText.split("#~#")[0];}';
				
			echo '    }';
			echo '}';
			echo '</script>';
			echo "\n";

			$Script .= " \n".' onFocus="lade_'.$FeldName.'(document.getElementsByName(\''.$QuellFeld.'\')[0].value,\''.$ZielFeld.'\');"';
		}

		if(!$SuchFeld)		// Bei Suchfeldern kein OLD
		{
			echo '<input name=old'.$Name.' type=hidden value=\''.$Wert.'\'>';
			echo '<select name=txt'.$Name.' '.$Script.'>';
		}
		else
		{
			echo '<select name=suc'.$Name.' '.$Script.'>';
		}

		if($OptionBitteWaehlen!='')		// Standardwert
		{
			$Option = explode('~',$OptionBitteWaehlen);
			echo '<option ';
			if($Wert=='')
			{
				echo ' selected ';
			}
			echo ' value="'.$Option[0].'">';
			echo (isset($Option[1])?$Option[1]:$Option[0]);
			echo '</option>';
		}

			// Zusatzoptionen, die nicht aus einer Tabelle kommen, sondern als Array
			// Format: a) array(array(Wert,Anzeige),array(Wert,Anzeige)...)
			//         b) array("Wert~Anzeige","Wert~Anzeige",...)
		if($Zusatzoptionen!==null AND $Zusatzoptionen !='')
		{
					// Format b)  ==> umbauen
			if(strpos($Zusatzoptionen[0],'~')!==false)
			{
				$Optionen = array();
				foreach($Zusatzoptionen AS $Option)
				{
					$Optionen[] = explode('~',$Option);
				}
				$Zusatzoptionen = $Optionen;
			}

			foreach($Zusatzoptionen as $Option)
			{
				echo '<option '.(isset($Option[2])?$Option[2]:'').' ';
				if($Option[0] == $Wert OR ($Wert=='' AND $Option[0]==$OptionDefault))
				{
					echo ' selected ';
				}
				echo ' value="'.$Option[0].'">';
				echo (isset($Option[1])?$Option[1]:$Option[0]);
				echo '</option>';
			}
		}

		if($SQL!='')
		{
			if(substr($SQL,0,3)=='***' OR substr($SQL,0,3)=='*F*')		// Daten suchen (AJAX)
			{
			}
			else
			{
				$rsDaten = awisOpenRecordset($con,$SQL);
				$LetzteGruppe='';
				for($i=0;$i<$awisRSZeilen;$i++)
				{
					if($awisRSSpalten==3)
					{
						if($LetzteGruppe!=$rsDaten[$awisRSInfo[3]['Name']][$i])
						{
							if($LetzteGruppe!='')
							{
								echo '</optgroup>';
							}
							echo "<optgroup label='" . $rsDaten[$awisRSInfo[3]['Name']][$i]. "'>";
							$LetzteGruppe=$rsDaten[$awisRSInfo[3]['Name']][$i];
						}
					}

					echo '<option value="'.$rsDaten[$awisRSInfo[1]['Name']][$i].'"';
					if(($rsDaten[$awisRSInfo[1]['Name']][$i] == $OptionDefault AND $Wert=='') OR $rsDaten[$awisRSInfo[1]['Name']][$i] == $Wert)
					{
						echo ' selected ';
					}
					echo '>'.$rsDaten[(isset($awisRSInfo[2]['Name'])?$awisRSInfo[2]['Name']:$awisRSInfo[1]['Name'])][$i].'</option>';
				}
				if($LetzteGruppe!='')
				{
					echo '</optgroup>';
				}
			}
		}
		echo '</select>';
	}
	else		// Schreibgesch�tzt
	{
			// passenden Datensatz suchen
		if($SQL!='')
		{	
			$rsDaten = awisOpenRecordset($con,$SQL);
			$rsDatenZeilen = $awisRSZeilen;
			if($AnzeigeSpalte=='')			// soll eine spezielle Spalte verwendet werden?
			{
				$Spalte = $awisRSInfo[2]['Name'];		// def: immer die zweite
			}
			else
			{
				$Spalte = $AnzeigeSpalte;
			}

			for($i=0;$i<$rsDatenZeilen;$i++)
			{
				if($rsDaten[$awisRSInfo[1]['Name']][$i] == $Wert)
				{
					echo '' . ($rsDaten[$Spalte][$i]==''?'&nbsp;':$rsDaten[$Spalte][$i]);
					break;
				}
			}
		}
		if(is_array($Zusatzoptionen))
		{
					// Format b)  ==> umbauen
			if(strpos($Zusatzoptionen[0],'~')!==false)
			{
				$Optionen = array();
				foreach($Zusatzoptionen AS $Option)
				{
					$Optionen[] = explode('~',$Option);
				}
				$Zusatzoptionen = $Optionen;
			}

			foreach($Zusatzoptionen as $Option)
			{
				if($Option[0] == $Wert)
				{
					echo $Option[1];
				}
			}
		}

	}
	echo '</div>';

}


/**
 * Erstellt ein TEEXTAREA Feld
 *
 * @param string $Name
 * @param string $Wert
 * @param int $Spalten
 * @param int $Zeilen
 * @param boolean $AendernRecht
 */
function awis_FORM_Erstelle_Textarea($Name, $Wert, $Breite, $Spalten, $Zeilen,$AendernRecht=false,$Style='')
{
	if($Breite > 0)
	{
		echo '<div class=EingabeFeldTextarea style="width:'.$Breite.';'.$Style.'">';
	}
	else
	{
		echo '<div class=EingabeFeldTextarea style="'.$Style.'">';
	}

	if($AendernRecht)
	{
		echo '<textarea cols='.$Spalten.' rows='.$Zeilen.' name=txt'.$Name.'>'.$Wert.'</textarea>';
		echo '<input name=old'.$Name.' type=hidden value=\''.stripslashes($Wert).'\'>';
	}
	else
	{
		echo '' . ($Wert==''?'&nbsp;':$Wert);
	}
	echo '</div>';
}

/**
 * Erzeugt ein Beschreibungsfeld für ein Element
 *
 * @param string $Bezeichnung
 * @param int $Breite
 * @param string $Style (DIV Style)
 * @param string $Link
 * @param string $ToolTippText
 */
function awis_FORM_Erstelle_TextLabel($Bezeichnung, $Breite, $Style='',$Link='',$ToolTippText='')
{
	switch ($Style)
	{
		case 'Hinweis':
			$Style = 'color:#FF0000;background-color:#FFFF00';
			break;
	}
	if($Breite > 0)
	{
		echo '<div class=EingabeLabel style="width:'.$Breite.';'.$Style.'"';
	}
	else
	{
		echo '<div class=EingabeLabel style="'.$Style.'"';
	}

	if($ToolTippText!='')
	{
		echo ' title="'.$ToolTippText.'"';
	}
	echo '>';

	if($Link!='')
	{
		echo '<a href='.$Link.'>';
	}
	echo '' . ($Bezeichnung==''?'&nbsp;':$Bezeichnung);
	if($Link!='')
	{
		echo '</a>';
	}
	if($ToolTippText!='')
	{
		echo '<img title="'.$ToolTippText.'" style="height:3;width:3;vertical-align:top" src=/bilder/hinweispunkt.png>';
	}
	echo '</div>';
}

/**
 * �berschrift
 *
 * @param string $Bezeichnung
 * @param int $Breite
 * @param string $Style
 * @param string $Link
 * @param string $ToolTipText
 */
function awis_FORM_Erstelle_Liste_Ueberschrift($Bezeichnung, $Breite,$Style='',$Link='',$ToolTipText='')
{
	if($Breite > 0)
	{
		echo '<div class=EingabeUeberschrift style="width:'.$Breite.';'.$Style.'" ';
	}
	else
	{
		echo '<div class=EingabeUeberschrift style="'.$Style.'" ';
	}

	if($ToolTipText!='')
	{
		echo ' title="'.$ToolTipText.'"';
	}
	echo '>';

	if($Link=='')
	{
		echo '' . ($Bezeichnung==''?'&nbsp;':$Bezeichnung);
	}
	else
	{
		echo '<a href='.$Link.'>';
		echo '' . ($Bezeichnung==''?'&nbsp;':$Bezeichnung);
		echo '</a>';
	}
	echo '</div>';
}

/**
 * Erstellt ein Icon in einer Liste
 *
 * @param string $Typ (href, image, script)
 * @param string $Name
 * @param string $Link
 * @param string $Bilddatei
 * @param string $Titel
 * @param int $Hintergrund (0=Dunkel, 1=Hell)
 * @param string $Style
 * @param int $Breite
 * @param int $Hoehe
 */
function awis_FORM_Erstelle_ListenBild($Typ, $Name, $Link, $Bilddatei, $Titel='', $Hintergrund=0, $Style='', $Breite=16, $Hoehe=16, $FeldBreite=0, $Ausrichtung='L')
{
	if($Hintergrund==0)
	{
		$Class = 'ListenFeldDunkel';
	}
	else
	{
		$Class = 'ListenFeldHell';
	}
	if($FeldBreite==0)
	{
		$FeldBreite = $Breite;
	}

	switch ($Ausrichtung)
	{
		case 'L':
			$Style.='text-align:left;';
			break;
		case 'C':	// Zentriert
			$Style.='text-align:center;';
			break;
		case 'R':	// rechtsb�ndig
			$Style.='text-align:right;';
			break;
	}

	echo '<div class='.$Class.' style="width:'.$FeldBreite.(isset($Style)?';'.$Style.';"':'"').'>';



	switch ($Typ)
	{
		case 'href':
			echo '<a name='.$Name.' href="'.$Link.'"';

			if($Titel!='')
			{
			 	echo ' title="'.$Titel.'" ';
			}

			echo '><img border=0 height='.$Hoehe.' width='.$Breite.' src="'.$Bilddatei.'">';
			echo '</a>';
			break;
		case 'image':
			echo '<input name='.$Name.' type=image';

			if($Titel!='')
			{
			 	echo ' title="'.$Titel.'" ';
			}

			echo ' border=0 height='.$Hoehe.' width='.$Breite.' src="'.$Bilddatei.'">';
			break;
		case 'script':
			echo '<a name='.$Name;

			if($Titel!='')
			{
			 	echo ' title="'.$Titel.'" ';
			 	echo ' alt="'.$Titel.'" ';
			}

			echo $Link;		// Das Skript

			echo '>';
			echo '<img border=0 height='.$Hoehe.' width='.$Breite.' src="'.$Bilddatei.'">';
			echo '</a>';
			break;
	}

	echo '</div>';
}

/**
 * Ein Listenfeld erstellen
 *
 * @param string $Name
 * @param mixed $Wert
 * @param int $Zeichen
 * @param int $Breite
 * @param boolean $AendernRecht
 * @param int $Hintergrund
 * @param string $Style
 * @param string $Link
 */
function awis_FORM_Erstelle_ListenFeld($Name, $Wert, $Zeichen, $Breite,$AendernRecht=false,$Hintergrund=0,$Style='',$Link='',$Format='T',$Ausrichtung='L',$ToolTipText='')
{
//	$DIVStyle='';
	$InputStyle='';

	if($Format=='')
	{
		$Format = 'T';
	}
	switch (strtoupper($Format[0]))
	{
		case 'D':
			if($Wert==null)
			{
				$Wert='';
			}
			else
			{
				if(isset($Format[1]) AND $Format[1]=='U')
				{
					$Wert = awis_PruefeDatum($Wert,true);
				}
				else
				{
					$Wert = awis_PruefeDatum($Wert);
				}
				//$Wert = date('d.m.Y',strtotime($Wert));
			}
			break;
		case 'N':
			if(!($Format[0]=='n' AND $Wert == ''))
			{
				$Wert = str_replace(',','.',$Wert);
				if(strlen($Format)>1)
				{
					if(isset($Format[2]) AND $Format[2]=='T')
					{
						$Wert = number_format($Wert,substr($Format,1),',','.');
					}
					else
					{
						$Wert = number_format($Wert,substr($Format,1),',','');
					}
				}
				else
				{
					$Wert = number_format($Wert,2,',','');
				}
			}

			if($Ausrichtung=='')
			{
				if($AendernRecht)		// Inputbox formatieren
				{
					$InputStyle .= ';text-align:right;';
				}
				else
				{
					$Style .= ';text-align:right;';
				}
			}

			break;
		case 'W':
			if($Wert!='')
			{
				$Wert = number_format($Wert,2,',','.');
			}

			if($Ausrichtung=='')
			{
				if($AendernRecht)		// Inputbox formatieren
				{
					$InputStyle .= ';text-align:right;';
				}
				else
				{
					$Style .= ';text-align:right;';
				}
			}
			break;
		case 'T':		// Text
			
			break;
		default:
			break;
	}

	switch ($Ausrichtung)
	{
		case 'R':
			$Style .= ';text-align:right;';
			break;
		case 'L':
			$Style .= ';text-align:left;';
			break;
		case 'Z':
			$Style .= ';text-align:center;';
			break;
	}

	// zeilenweise Farben
	if($Hintergrund==0)
	{
		$Class = 'ListenFeldDunkel';
	}
	else
	{
		$Class = 'ListenFeldHell';
	}

		// Breite des DIV Feldes
	if($Breite==0)		// Variabel
	{
		echo '<div class='.$Class;
		echo ($ToolTipText==''?'':' title="'.$ToolTipText.'"');
		echo ' style="'.$Style.'">';
	}
	elseif($Breite<0)	// Mindestbreite
	{
		echo '<div class='.$Class;
		echo ($ToolTipText==''?'':' title="'.$ToolTipText.'"');
		echo ' style="min-width:'.(-$Breite).';'.$Style.'">';
	}
	else				// Mindest und exakte Breite
	{
		echo '<div class='.$Class;
		echo ($ToolTipText==''?'':' title="'.$ToolTipText.'"');
		echo ' style="min-width:'.$Breite.';width:'.$Breite.';'.$Style.'">';
	}

		// Editierbares Feld?
	if($AendernRecht)
	{
			// TEXT und OLD Wert erstellen
		echo '<input class=InputText name=txt'.$Name.' type=text size='.$Zeichen.' value=\''.$Wert.'\'>';
		if($Name[0]!='#')
		{
			echo '<input name=old'.$Name.' type=hidden value=\''.$Wert.'\'>';
		}
		if($Link!='')		// Link gewünscht?
		{
			echo '<a href='.$Link.'><img border=0 src=/bilder/link.png></a>';
		}
	}
	else
	{
			// Link vorhanden?
		if($Link=='')
		{
			echo '' . ($Wert==''?'&nbsp;':$Wert) . '';
		}
		else
		{
			echo '<a href='.$Link.'>'.($Wert==''?'&nbsp;':$Wert).'</a>';
		}
	}
	echo '</div>';
}

/**
 * Erstellt einen Block mit kleinen Icons und einem Link
 *
 * @param array $Icons (0=Name des Icons, 1=Link, 2=Tastenkuerzel, 3=Tooltipptext)
 * @param int $Breite
 * @param int $Hintergrund (-1=ohne Hintergrund, 0=Dunkel, sonst Hell)
 * @param string $ToolTippText
 * @param string $Style
 */
function awis_FORM_Erstelle_ListeIcons($Icons, $Breite, $Hintergrund=0, $ToolTippText='', $Style='')
{
	if($Hintergrund==0)
	{
		$Class = 'ListenFeldDunkel';
	}
	elseif($Hintergrund==-1)
	{
		$Class = 'ListenFeld';
	}
	else
	{
		$Class = 'ListenFeldHell';
	}

	if($Breite > 0)
	{
		echo '<div class='.$Class.' style="width:'.$Breite.';'.$Style.'">';
	}
	else
	{
		echo '<div class='.$Class.' style="'.$Style.'">';
	}

	if(count($Icons)==0)
	{
		echo '&nbsp;';
	}
	else
	{
		foreach ($Icons AS $Icon)
		{
			$TippText = $ToolTippText;
			echo '<a class=ListenIcon ';
			if(isset($Icon[2]))		// Tastenk�rzel
			{
				echo ' accesskey='.$Icon[2][0].'';
				$TippText .= ' (Alt+'.$Icon[2][0].')';
			}
			echo ' href=' . $Icon[1] . '>';

			// Icon anzeigen
			echo '<img class=ListenIcon width=16 height=16 border=0 ';

			if(isset($Icon[3]))		// eigener Tooltipptext f�r dieses Icon
			{
				$TippText = $Icon[3];
			}

			if($TippText!='')
			{
				echo ' title="'.$TippText.'"';
			}

			echo ' src=/bilder/icon_'.$Icon[0].'.png>';
			echo '</a>';
		}
	}

	echo '</div>';
}


/**
 * Schreibt eine Trennzeile
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20070501
 * @param string $Art (L=Linie, P=gepunktete Linie, S=gestrichelte Linie, D=doppelte Linie, O=Ohne Linie)
 */
function awis_FORM_Trennzeile($Art='L')
{
	switch ($Art)
	{
		case 'P':
			awis_FORM_ZeileStart('border-bottom-style:dotted;padding:0;margin-top:5px;margin-bottom:5px');
			break;
		case 'S':
			awis_FORM_ZeileStart('border-bottom-style:dashed;padding:0;margin-top:5px;margin-bottom:5px');
			break;
		case 'D':
			awis_FORM_ZeileStart('border-bottom-style:double;padding:0;margin-top:5px;margin-bottom:5px');
			break;
		case 'O':
			awis_FORM_ZeileStart('border-style:none;height:10px;padding:0;margin-top:5px;margin-bottom:5px');
			break;
		default:
			awis_FORM_ZeileStart('border-bottom-style:solid;border-bottom-width:thin;padding:0;margin-top:4px;margin-bottom:4px');
			break;
	}
	awis_FORM_ZeileEnde();
}


/**
 * Erstellt eine Infozeile in einem AWIS Formular, das i.d.R. USER und USER_DAT anzeigt
 *
 * @param array $Felder
 * @param string $StyleZeile
 */
function awis_FORM_InfoZeile($Felder,$StyleZeile='')
{
	echo '<div class=InfoZeile';
	if($StyleZeile!='')
	{
		echo ' style="'.$StyleZeile.'"';
	}
	echo '>';

	foreach($Felder as $Feld)
	{

		echo '<div class=InfoZeileFeld ';
		if($Feld['Style']!='')
		{
			echo ' style="'.$Feld['Style'].'"';
		}
		echo '>';

		echo ''.$Feld['Inhalt'];
		echo '</div>';
	}


	echo '</div>';
}


/**
 * Eingabefeld f�r Datei-Uploads
 *
 * @param string $FeldName
 * @param int $Breite
 * @param int $Zeichen
 * @param int $Bytes
 * @param string $Style
 * @author Sacha Kerres
 * @version 20070719
 *
 */
function awis_FORM_Erstelle_DateiUpload($FeldName, $Breite=0, $Zeichen=30, $Bytes=2000000, $Style='')
{

	if($Breite>0)
	{
		echo '<div class=EingabeFeld style="width:'.$Breite.';'.$Style.'">';
	}
	else
	{
		echo '<div class=EingabeFeld style="'.$Style.'">';
	}


	echo '<input type=file class=InputDatei name='.$FeldName.' size='.$Zeichen.' maxlength='.$Bytes.' ';
	echo ' onchange="setzeFarbe(\''.$FeldName.'\');"';
	echo '>';

	echo '</div>';
}


/**
 * Formatiert einen Feldwert f�r die Speicherung (INSERT, UPDATE)
 * �ber das Format wird auch der Inhalt umgewandelt.
 * Formate: 	T=Text
 * 				TZ=Text, der nur aus Zahlen (mit + am Anfang) besteht
 * 				Z=Ganze Zahl
 * 				W=W�hrung
 * 				N=Numerisch (mit Nx f�r x Nackommastellen)
 * 				D=Datum (DU=Datum mit Zeit, DI=ISO Format)
 * 				U=Uhrzeit
 *
 * @param string $Typ(T,TZ,Z,W,N,D,U)
 * @param mixed $Wert
 * @param boolean $NULL_Erlaubt=True
 * @return unknown
 */
function awis_FeldInhaltFormat($Typ='T',$Wert,$NULL_Erlaubt=true)
{
	$Erg = '';

	switch ($Typ[0]) {
		case 'T':
			if($Wert === null AND $NULL_Erlaubt)		// NULL aus der Datenbank
			{
				$Erg = 'null';
			}
			elseif($Wert === '' AND $NULL_Erlaubt)		// Leer und NULL
			{
				$Erg = 'null';
			}
			elseif($Wert === '' AND !$NULL_Erlaubt)		// Leer mit NOT NULL
			{
				$Erg = '\'\'';
			}
			else 										// Ein nomaler Wert
			{
				$Wert = str_replace("'","",($Wert));

				if(isset($Typ[1]) AND $Typ[1]=='Z')		// Nur Zahlen
				{
					for($i=0;$i<strlen($Wert);$i++)
					{
						if(is_numeric($Wert[$i]) or ($i==0 AND $Wert[$i]=='+'))
						{
							$Erg .= $Wert[$i];
						}
					}
					$Erg = "'".$Erg."'";
				}
				else
				{
					//$Erg = '\''.htmlentities($Wert,ENT_QUOTES).'\'';
					$Erg = "'".$Wert."'";
				}
			}
			if(isset($Typ[1]) AND $Typ[1]=='U')
			{
				$Erg = strtoupper($Erg);
			}
			break;
		case 'Z':			// Ganze Zahl
			if($Wert == '' AND $NULL_Erlaubt)
			{
				$Erg = 'null';
			}
			elseif($Wert == '' AND !$NULL_Erlaubt)
			{
				$Erg = '0';
			}
			else
			{
				$Erg = ''.floatval(str_replace(',','.',$Wert)).'';
			}
			break;
		case 'W':			// Währung (2NK)
			if($Wert == '' AND $NULL_Erlaubt)
			{
				$Erg = 'null';
			}
			elseif($Wert == '' AND !$NULL_Erlaubt)
			{
				$Erg = '0.0';
			}
			else
			{
				if(strpos($Wert,','))
				{
					$Wert = str_replace(',','.',str_replace('.','',$Wert));
				}
				$Erg = ''.number_format($Wert,2,'.','').'';
			}
			break;
		case 'N':			// Numerisch (8NK)
			if($Wert == '' AND $NULL_Erlaubt)
			{
				$Erg = 'null';
			}
			elseif($Wert == '' AND !$NULL_Erlaubt)
			{
				$Erg = '0.0';
			}
			else
			{
				$NKStellen = ((isset($Typ[1]) AND is_numeric($Typ[1]))?$Typ[1]:8);
				if(strpos($Wert,",")!==false)
				{
					$Erg = ''.number_format(str_replace(',','.',str_replace('.','',$Wert)),$NKStellen,'.','').'';
				}
				else
				{
					$Erg = ''.number_format($Wert,$NKStellen,'.','').'';
				}
			}			break;
		case 'D':			// Datum
			if($Wert == '' AND $NULL_Erlaubt)
			{
				$Erg = 'null';
			}
			elseif($Wert == '' AND !$NULL_Erlaubt)
			{
				$Erg = '\'1.1.1950\'';
			}
			else
			{
				if(isset($Typ[1])AND ($Typ[1]=='U' OR $Typ[1]=='I'))
				{
					$Datum = awis_PruefeDatum($Wert,true);
				}
				else
				{
					$Datum = awis_PruefeDatum($Wert,false);
				}

				if(strtotime($Datum)!==False)		// Ist es ein konvertierbares Datum?
				{
					if(intval(phpversion())>=5 AND isset($Typ[1])AND ($Typ[1]=='I'))
					{
						$Erg = date('c',strtotime($Datum));
					}
					elseif(isset($Typ[1]) AND $Typ[1]=='O')
					{
						$Erg = 'TO_DATE(\''.$Datum.'\',\'DD.MM.RRRR\')';
					}
					else
					{
						$Erg = '\''.$Datum.'\'';
					}
				}
				else
				{
					$Erg = '\''.($Wert).'\'';		// Keine Formatierung möglich => Läuft in einen Fehler
				}

			}
			break;
		case 'U':			// Datum und Uhrzeit
			if($Wert == '' AND $NULL_Erlaubt)
			{
				$Erg = 'null';
			}
			elseif($Wert == '' AND !$NULL_Erlaubt)
			{
				$Erg = '1.1.1950 0:00';
			}
			else
			{
				if(strtotime($Wert)!==False)		// Ist es ein konvertierbares Datum?
				{
					$Erg = '\''.date('c',strtotime($Wert)).'\'';
				}
				else
				{
					$Erg = '\''.htmlspecialchars($Wert,ENT_QUOTES,'ISO-8859-15').'\'';		// Keine Formatierung möglich => Läuft in einen Fehler
				}
			}
			break;
		default:
			$Erg = '\''.htmlspecialchars($Wert,ENT_QUOTES,'ISO-8859-15').'\'';
			break;
	}

	return $Erg;
}


/**
 * Formatiert ein Datum
 *
 * @param String $DatumsText / Moegliche-Werte: Datum, Datum Zeit, SYSDATE, TRUNC(SYSDATE),  DS=>SYSDATE, DE=>'01.01.2038 00:00:00'
 * @param boolean $MitZeit
 * @param int $Erg (0=String, 1=time)
 * @return string
 */
function awis_PruefeDatum($DatumsText,$MitZeit = 0,$Erg=0)
{
	$DatumsTrennzeichen = '.';
	$DatumsWert=0;
	$Tag = '';
	$Monat = '';
	$Jahr = '';
	$Zeit = '';

	// CA - 23.01.08 Setzen von Sonderwerten, SK Parameter f�r Zeit ber�cksichtigt
	if (strtoupper($DatumsText)=='SYSDATE' AND $MitZeit)
	{
		$DatumsText	= date("d.m.Y H:i:s");
	}
	if (strtoupper($DatumsText)=='SYSDATE' AND !$MitZeit)
	{
		$DatumsText	= date("d.m.Y");
	}
	if (strtoupper($DatumsText)=='DS')
	{
		$DatumsText	= date("d.m.Y H:i:s");
	}
	if (strtoupper($DatumsText)=='DE')
	{
		$DatumsText	= '01.01.2038 00:00:00';
	}

	$DatumsText = str_replace('-',$DatumsTrennzeichen,$DatumsText);
	$DatumsText = str_replace('/',$DatumsTrennzeichen,$DatumsText);

	if(strlen($DatumsText)>10)		// Datum mit Uhrzeit
	{
		$DatumsText = substr($DatumsText,0,19);		// Millisekunden raus
		$DatumsWert = explode(' ',$DatumsText);		// Datum und Zeit teilen
		$DatumsText = $DatumsWert[0];
		$Zeit = $DatumsWert[1];
		$DatumsWert = 0;
	}

	$DatumsWert=false;
	$DatTeile = explode('.',$DatumsText);
	if(count($DatTeile)==3)	// Dreiteiliges Datum
	{
		if(strlen($DatTeile[0])==4)
		{
			$Jahr = $DatTeile[0];
			$Monat = $DatTeile[1];
			$Tag = $DatTeile[2];
			$DatumsWert = mktime(0,0,0,$Monat,$Tag,$Jahr);
		}
		elseif(strlen($DatTeile[2])==4)
		{
			$Jahr = $DatTeile[2];
			$Monat = $DatTeile[1];
			$Tag = $DatTeile[0];
			$DatumsWert = mktime(0,0,0,$Monat,$Tag,$Jahr);
		}
		elseif(strlen($DatTeile[2])==2)
		{
			$Jahr = '20'.$DatTeile[2];
			$Monat = $DatTeile[1];
			$Tag = $DatTeile[0];
			$DatumsWert = mktime(0,0,0,$Monat,$Tag,$Jahr);
		}
	}
	elseif(count($DatTeile)==2)
	{
		$DatumsWert = mktime(0,0,0,$DatTeile[1],$DatTeile[0],date('Y'));
	}
	elseif(count($DatTeile)==1)
	{
		$DatumsWert = mktime(0,0,0,substr($DatTeile[0],4,2),substr($DatTeile[0],6,2),substr($DatTeile[0],0,4));
	}

	if($DatumsWert===false)
	{
		$DatumsWert = mktime(0,0,0,1,1,1950);
	}

	switch ($Erg)
	{
		case 0:
			if($MitZeit)
			{
				if(strlen($Zeit)>0 AND strpos($Zeit,':')===false)
				{
					if(strlen($Zeit)==6)
					{
						$Zeit = substr($Zeit,0,2).':'.substr($Zeit,2,2).':'.substr($Zeit,4,2);
					}
					elseif(strlen($Zeit)==4)
					{
						$Zeit = substr($Zeit,0,2).':'.substr($Zeit,2,2).':00';
					}
				}

				return date('d.m.Y',$DatumsWert).' '.$Zeit;
			}

			return date('d.m.Y',$DatumsWert);

		case 1:		// Zeitstempel
			return $DatumsWert;
	}
}

/**
 * Gibt eine Meldung aus
 *
 * @version 24050942
 * @author Sacha Kerres
 * @package AWIS_FORMS
 * @todo Weitere Ausgabeformen angeben
 * @param int $Typ (1=HinweisText)
 * @param string $Meldung
 */
function awisFORM_Meldung($Typ, $Meldung)
{
	switch ($Typ) {
		case 1:
			echo '<span class=HinweisText>'.$Meldung.'</span>';
			break;
		default:
			echo '<span>'.$Meldung.'</span>';
			break;
	}

}

//*******************************************************************************************
//*******************************************************************************************
// Datenbank Tools
//*******************************************************************************************
//*******************************************************************************************

/**
 * Formatiert einen Wert für die Abfrage in WHERE Bedingungen
 * und bei der Anzeige in der Oberfläche
 *
 * @param string $Format
 * @param mixed $Wert
 * @param string $Typ ('DB','WEB')
 * @param boolean $Null (Anzeigen als NULL)
 * @param string $Umwandlung ('UPPER','LOWER','LIKE')
 * @return string
 */
function awisFeldFormat($Format,$Wert, $Typ='DB',$Null=false,$Umwandlung='')
{
	$Erg = '';
	$Vergleich = '';		// Vergleichszeichen

	$Umwandlungen = explode(',',$Umwandlung);
	foreach($Umwandlungen AS $Umwandlung)
	{
		switch ($Umwandlung)
		{
			case 'UPPER':
				$Wert = mb_strtoupper($Wert);
				break;
			case 'LOWER':
				$Wert = mb_strtolower($Wert);
				break;
			case 'LIKE':
				$ErsatzListe = array('*'=>'%"','?'=>'_');
				$Erg = strtr($Wert,$ErsatzListe);
				if($Wert != $Erg)
				{
					$Vergleich  = ' LIKE ';
					$Wert = strtr($Wert,$ErsatzListe);
				}
				else
				{
					$Vergleich = ' = ';
				}
				break;
		}
	}

	if($Typ=='DB')
	{
		if($Wert == '' AND $Null)
		{
			return $Vergleich . ' null ';
		}

		switch($Format[0])
		{
			case 'T':			// Text mit Hochkommata
				$Erg = $Vergleich . "'".awis_escape_string($Wert)."'";
				break;
			case 'N':
				if(strpos($Wert,".")!==false AND strpos($Wert,",")!==false)
				{
					$Wert = str_replace('.','',$Wert);
					$Wert = str_replace(',','.',$Wert);
				}
				elseif(strpos($Wert,",")!==false)
				{
					$Wert = str_replace(',','.',$Wert);
				}

				$Erg = floatval($Wert);
				if(isset($Format[1]))
				{
					$Erg = number_format($Erg,intval(substr($Format,1)),'.','');
				}
				break;
			case 'Z':
				$Erg = intval($Wert);
				break;
			case 'D':
				if($Format=='DT' OR $Format=='DU')
				{
					$Erg = "TO_DATE('".awis_PruefeDatum($Wert,1)."','DD.MM.RRRR HH24:MI:SS')";
				}
				else
				{
					$Erg = "TO_DATE('".awis_PruefeDatum($Wert,0)."','DD.MM.RRRR')";
				}
				break;
			default:
				$Erg = $Wert;
		}
	}

	return $Erg;
}

/**
 * L�scht nicht speicherbare Zeichen aus einem String
 *
 * @param string $Text
 * @return string
 */
function awis_escape_string($Text)
{
	$ErsetzungsListe= array("\\\\"=>"\\","'"=>"","\""=>"");

	$Erg = strtr($Text,$ErsetzungsListe);

	return $Erg;
}

/**
 * Funktion zerlegt eine Strasse in den Strassennamen und die Hausnummer
 * Das Ergebnis wird als Array zur�ckgeliefert. Sollte keine Hausnummer
 * identifizierbar sein, wird alles in dem Feld 'strasse' geliefert.
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @param string $Strasse
 * @return array
 */
function awis_ZerlegeStrasse($Strasse)
{
	$Erg = Array();
	$Pos = 0;
	$Zeichen = false;

	for($i=strlen($Strasse);$i>=0;$i--)
	{
		if($i<strlen($Strasse) AND !is_numeric($Strasse[$i]))
		{
			$Zeichen = true;
			if($i<strlen($Strasse)-1 AND is_numeric($Strasse[$i+1]))
			{
				$Erg['strasse']=trim(substr($Strasse,0,$i));
				$Erg['hausnummer']=trim(substr($Strasse,$i));
			}
			else
			{
				$Erg['strasse']=trim($Strasse);
				$Erg['hausnummer']='';
			}
			return $Erg;
		}
		elseif($i<strlen($Strasse) AND is_numeric($Strasse[$i]) AND !$Zeichen)
		{
			$Pos = $i;
		}
	}

	$Erg['strasse']=trim($Strasse);
	$Erg['hausnummer']='';
	return $Erg;
}


/**
 * Erstellt ein �berschriftenfeld in einem AWIS Formular
 *
 * @author Sacha Kerres
 * @version 20070501
 * @copyright ATU
 * @param string $Text (anzuzeigender Text)
 */
function awis_FORM_Erstelle_Ueberschrift($Text)
{
	echo '<div class=EingabeUeberschrift>';
	echo $Text;
	echo '</div>';
}



/**
 * Start eines Schaltfl�chenbereichs f�r ein AWIS Formular
 *
 * @param string $Style (CSS Style)
 */
function awis_FORM_SchaltflaechenStart($Style='')
{
	echo '<div class=FORMSchaltflaechenBereich ';

	if($Style!='')
	{
		echo ' style="'.$Style.'"';
	}
	echo '>';
}

/**
 * Ende eines Schaltfl�chenbereichs f�r ein AWIS Formular
 *
 */
function awis_FORM_SchaltflaechenEnde()
{
	echo '</div>';
}

/**
 * Erstellt eine Schaltfl�che
 *
 * @param string $Typ ('image','href','script')
 * @param string $Name
 * @param string $Link/Script
 * @param string $Bilddatei
 * @param string $Titel
 * @param string $TastenKuerzel
 * @param string $Style (CSS Style)
 * @param int $Breite (des Bilds)
 * @param int $Hoehe (des Bilds)
 */
function awis_FORM_Schaltflaeche($Typ, $Name, $Link, $Bilddatei, $Titel='', $TastenKuerzel='', $Style='', $Breite=27, $Hoehe=27)
{
	//echo '<div class=FORMSchaltflaeche '.($Style!=''?'style="'.$Style.';"':'').'>';
	echo '<div class=FORMSchaltflaeche '.(isset($Style)?'style="'.$Style.';"':'').'>';

	switch ($Typ)
	{
		case 'href':
			echo '<a name='.$Name.' href="'.$Link.'"';
			if($TastenKuerzel!='')
			{
				echo ' accesskey='.$TastenKuerzel;
			}

			if($Titel!='')
			{
			 	echo ' title="'.$Titel.'" ';
			}

			echo '><img border=0 height='.$Hoehe.' width='.$Breite.' src="'.$Bilddatei.'">';
			echo '</a>';
			break;
		case 'image':
			echo '<input name='.$Name.' type=image';
			if($TastenKuerzel!='')
			{
				echo ' accesskey='.$TastenKuerzel;
			}

			if($Titel!='')
			{
			 	echo ' title="'.$Titel.'" ';
			}

			echo ' border=0 height='.$Hoehe.' width='.$Breite.' src="'.$Bilddatei.'">';
			break;
		case 'script':
			echo '<a name='.$Name;
			if($TastenKuerzel!='')
			{
				echo ' accesskey='.$TastenKuerzel;
			}

			if($Titel!='')
			{
			 	echo ' title="'.$Titel.'" ';
			 	echo ' alt="'.$Titel.'" ';
			}

			echo $Link;		// Das Skript

			echo '>';
			echo '<img border=0 height='.$Hoehe.' width='.$Breite.' src="'.$Bilddatei.'">';
			echo '</a>';
			break;
	}

	echo '</div>';
}

/**
 * Liefert das Tastenk�rzel aus einem Text (Kennung: <u>)
 *
 * @param string $Text
 * @return string
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200709211114
 */
function awis_FORM_TastenKuerzel($Text)
{
	if(($Pos = strpos(strtolower($Text),'<u>'))===false)
	{
		return '';
	}

	return(substr($Text,$Pos+3,1));
}


/**
 * Liefert einen Wert aus einer Infotabelle
 *
 * @param ressource $con
 * @param string $Tabelle
 * @param int $InfoKey
 * @param int $TabKey
 * @param string $Typ (nach awis_Format())
 * @param int $IMQ_ID
 * @param boolean $MitKey (Tabelle mit Keys liefern)
 * @return mixed
 */
function InfoFeldwert($con, $Tabelle, $InfoKey, $TabKey, $Typ = 'T', $IMQ_ID = '', $MitKey = false)
{
	global $awisRSZeilen;
	global $AWISSprache;

	if($MitKey)
	{
		$Erg = array(array('Key'=>0,'Wert'=>''));	// Defaultarray ist leer
		$Elemente=0;
	}
	else
	{
		$Erg = '';
	}

	switch ($Tabelle)
	{
		case 'AST':
			$SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
			$SQL .= ' WHERE ASI_AST_ATUNR = '.awis_FeldInhaltFormat('T',$TabKey);
			$SQL .= ' AND ASI_AIT_ID = '.awis_FeldInhaltFormat('N0',$InfoKey);
			$SQL .= ($IMQ_ID!=''?' AND BITAND(ASI_IMQ_ID,'.awis_FeldInhaltFormat('N0',$IMQ_ID).') <> 0':'');
			$rs = awisOpenRecordset($con,$SQL);
			$rsZeilen = $awisRSZeilen;
			for($Zeile=0;$Zeile<$rsZeilen;$Zeile++)
			{
				$Wert = awisformat($Typ,(isset($rs['ASI_WERT'][$Zeile])?$rs['ASI_WERT'][$Zeile]:''),$AWISSprache);
				if($MitKey)
				{
					$Erg[$Elemente++]=array('Key'=>(isset($rs['ASI_KEY'][$Zeile])?$rs['ASI_KEY'][$Zeile]:'0'),'Wert'=>$Wert);
				}
				else
				{
					$Erg .= ($Erg==''?'':',').$Wert;
				}
			}

			break;
		default:
			$Erg = '##TAB##';
	}

	return $Erg;
}

/**
 * Formatiert einen Wert f�r den Desktop
 *
 * @param string $Format
 * @param mixed $Wert
 * @param string $Land='DE'
 * @return mixed
 */
function awisFormat($Format, $Wert, $Sprache='DE')
{
	$KommaZeichen=',';
	$TausenderPunkt='.';

	 switch ($Format[0])
	 {
	  case 'D':
	   if($Wert!='')
	   {
	    if(isset($Format[1])AND $Format[1]=='U')
	    {
	     $Wert = date('d.m.Y H:i:s',(is_numeric($Wert)?$Wert:strtotime($Wert)));
	    }
	    else
	    {
	     $Wert = date('d.m.Y',(is_numeric($Wert)?$Wert:strtotime($Wert)));
	    }
	   }
	   break;
	  case 'N':
	   if($Wert!='')
	   {
	    if(isset($Format[1]))
	    {
	     if(($Format[1])=='x')
	     {
	      if($Format[1]=='x')
	      {
	       $Wert = number_format($Wert,8,$KommaZeichen,'');
	      }
	      else
	      {
	       $Wert = number_format($Wert,8,$KommaZeichen,$TausenderPunkt);
	      }
	      for($i=strlen($Wert)-1;$i>0;$i--)
	      {
	       if($Wert[$i]===$KommaZeichen)
	       {
	        $i--;
	        break;
	       }
	       if($Wert[$i]!=='0')
	       {
	        break;
	       }
	      }
	      if($i<strlen($Wert)-1)
	      {
	       $Wert = substr($Wert,0,$i+1);
	      }
	     }
	     elseif($Format[1]=='X')
	     {
	      if($Wert != '')
	      {
	       $Wert = number_format($Wert,8,',','');
	       for($i=strlen($Wert)-1;$i>0;$i--)
	       {
	        if($Wert[$i]===$KommaZeichen)
	        {
	         $i--;
	         break;
	        }
	        if($Wert[$i]!=='0')
	        {
	         break;
	        }
	       }
	       $Wert = substr($Wert,0,$i+1);
	      }
	     }
	     else
	     {
	      $Wert = number_format($Wert,substr($Format,1),$KommaZeichen,'');
	     }
	    }
	    else
	    {
	     $Wert = number_format($Wert,2,$KommaZeichen,'');
	    }
	   }
	   break;
	  case 'W':
	   if($Wert!='')
	   {
	    $Wert = number_format($Wert,2,$KommaZeichen,$TausenderPunkt);
	   }
	   break;
	  case 'T':  // Text
	  	if(isset($Format[1]))
	  	{
	  		switch ($Format[1])
	  		{
				case 'P':		// Aufbereitung f�r PDF Druck
					$Wert = html_entity_decode($Wert);
					// Sonderzeichen umsetzen
					$Wert = str_replace(chr(164),'�',$Wert);
	  				break;
	  		}
	  	}
	  	else
	  	{
	   		$Wert = htmlspecialchars($Wert,ENT_QUOTES,'ISO-8859-15');
	  	}
	   break;
	  case 'P':  // Passwort
	   $Wert = htmlspecialchars($Wert,ENT_QUOTES,'ISO-8859-15');
	   break;
	  default:
	   break;
	 }

	 return $Wert;
}

/**
 * awis_WertListe
 * Liefert Elemente aus einer Werteliste oder ein Array mit allen Eintr�gen
 *
 * @param $Text (Format Wert~Text|Wert~Text)
 * @param string $SuchElement
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 20080506
 * @return mixed
 */
function awis_WertListe($Text, $SuchElement='', $Default='')
{
	$Liste=explode('|',$Text);
	if(is_array($Liste))
	{
		if($SuchElement === '')
		{
			return $Liste;
		}

		for($i=0;$i<count($Liste);$i++)
		{
			$Eintrag = explode('~',$Liste[$i]);
			if($Eintrag[0]==$SuchElement)
			{
				return $Eintrag[1];
			}
		}
		return $Default;
	}
	else
	{
		return $Text;
	}
}
?>