<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $BMV;

try {
   
    if(isset($_POST['cmdLoeschen_x']) or isset($_GET['DEL'])) {
        $BMV->Form->SchreibeHTMLCode('<form name=frmLoeschen action=./sicherheitsanlagen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

        // Fragen ob Vertrag inkl. aller Rechnungen wirklich gel�scht werden soll
        $BMV->Form->Formular_Start();
        $BMV->Form->ZeileStart();
        $BMV->Form->Hinweistext($BMV->AWISSprachKonserven['Wort']['WirklichLoeschen']);
        $BMV->Form->ZeileEnde();

        if(isset($_POST['cmdLoeschen_x'])) {
            $BMV->Form->Erstelle_HiddenFeld('BMV_KEY',$_POST['txtBMV_KEY']);
        }
        else {
            $BMV->Form->Erstelle_HiddenFeld('BMR_KEY',$_GET['BMR_KEY']);
            $BMV->Form->Erstelle_HiddenFeld('BMV_KEY',$_GET['BMV_KEY']);
        }

        $BMV->Form->Trennzeile();

        $BMV->Form->ZeileStart();
        $BMV->Form->Schaltflaeche('submit','cmdLoeschenOK','','',$BMV->AWISSprachKonserven['Wort']['Ja'],'');
        $BMV->Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$BMV->AWISSprachKonserven['Wort']['Nein'],'');
        $BMV->Form->ZeileEnde();

        $BMV->Form->SchreibeHTMLCode('</form>');

        $BMV->Form->Formular_Ende();

        die();

    }

    if(isset($_POST['cmdLoeschenOK'])) {

        // Datensatz soll gel�scht werden

        if(isset($_POST['txtBMR_KEY'])) {

            $SQL = 'delete from BMARECHNUNGEN where BMR_KEY = ' . $BMV->DB->WertSetzen('BMR','N0', $_POST['txtBMR_KEY']);

            $BMV->DB->Ausfuehren($SQL,'','',$BMV->DB->Bindevariablen('BMR'));
            $AWIS_KEY2 = '';
        }
        else {

            // Vertrag und alle dazugeh�rigen Rechnungen sollen gel�scht werden
            try {
                $BMV->DB->TransaktionBegin();

                $SQL = 'delete from BMARECHNUNGEN where BMR_BMV_KEY = ' . $BMV->DB->WertSetzen('BMR','N0',$_POST['txtBMV_KEY']);
                $BMV->DB->Ausfuehren($SQL,'',true,$BMV->DB->Bindevariablen('BMR'));

                $SQL = 'delete from BMAVERTRAEGE where BMV_KEY = ' . $BMV->DB->WertSetzen('BMV','N0',$_POST['txtBMV_KEY']);
                $BMV->DB->Ausfuehren($SQL,'',true,$BMV->DB->Bindevariablen('BMV'));

                $BMV->DB->TransaktionCommit();
            }
            catch (awisException $ex) {
                $BMV->DB->TransaktionRollback();
            }
            $AWIS_KEY1 = '';
        }
    }


} catch (awisException $ex) {
    $BMV->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $BMV->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $BMV->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>