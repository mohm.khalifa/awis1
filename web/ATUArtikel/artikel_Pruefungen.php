<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $AWISBenutzer;
	global $con;

//	echo '<span class=HinweisText>ACHTUNG: Diese Seite ist nur zum TEST, die eingegebenen Daten k�nnen durch neue Importe �berschrieben werden</span>';

	$Recht407 = awisBenutzerRecht($con,407);

		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));


	//awis_Debug(1,$_REQUEST);


/**************************************************************************
* Aktuelle Zeile l�schen
**************************************************************************/

if(awis_NameInArray($_POST,'cmdAPRLoeschen_')!='')
{
	$Key = intval(substr(awis_NameInArray($_POST,'cmdAPRLoeschen_'),15));
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{
		$SQL = "DELETE FROM ArtikelPruefungen WHERE APR_KEY=0" . $_POST["txtKEY"];
		if( awisExecute($con, $SQL )==FALSE)
		{
			awisErrorMailLink("artikel_Main.php", 2, $awisDBFehler['message']);
			awisLogoff($con);
			die();
		}

		die('<br>Die Stellplatzpr�fung wurde erfolgreich gel�scht<br><br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Pruefungen>Weiter</a>');
	}
	elseif(!isset($_POST["cmdLoeschAbbruch"]) or $_POST["cmdLoeschAbbruch"]=='')
	{
		print "<form name=frmAPR method=post>";

		print "<input type=hidden name=cmdAPRLoeschen_x>";
		print "<input type=hidden name=txtKEY value=" . $Key . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Stellplatzpr�fung l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";

		print "</form>";
		awisLogoff($con);
		die();
	}
}

/**************************************************************************
*
* PDF Druck
*
***************************************************************************/
	if(isset($_GET['Druck']))
	{
		include '/daten/web/ATUArtikel/artikel_PruefungenDrucken.php';
		echo '<hr>';
	}


//**************************************************************************
//
// Daten speichern
//
//**************************************************************************

	$DetailAnzeigen = (isset($_GET['APRKey'])OR isset($_POST['cmdAPRNeu_x']));

	if(isset($_POST['cmdSpeichern_x']) AND isset($_POST["txtAPR_KEY_old"]))
	{
		$DetailAnzeigen = False;
		$SQL = '';

		if($_POST["txtAPR_LAR"]=='-1')
		{
			echo '<span class=HinweisText>Sie m�ssen einen Lieferantenartikel w�hlen!</span>';
		}
		else
		{
			if($_POST["txtAPR_KEY_old"]>0)		// Vorhandenen �ndern
			{

				if(isset($_POST["txtAPR_MENGE_N"]) AND $_POST["txtAPR_MENGE_N"]!=$_POST["txtAPR_MENGE_N_old"])
				{
					$SQL .= ', APR_MENGE_N=0' . $_POST["txtAPR_MENGE_N"];
				}
				if(isset($_POST["txtAPR_LAR"]) AND $_POST["txtAPR_LAR_old"]!=$_POST["txtAPR_LAR"])
				{
					$Felder = explode('~',urldecode($_POST["txtAPR_LAR"]));
					$SQL .= ", APR_LAR_KEY=" . $Felder[0] . "";
					$SQL .= ", APR_LAR_LARTNR='" . $Felder[1] . "'";
					$SQL .= ", APR_LIE_NR='" . $Felder[2] . "'";
				}

				if(isset($_POST["txtAPR_PRUEFER_N"]) AND $_POST["txtAPR_PRUEFER_N_old"]!=$_POST["txtAPR_PRUEFER_N"])
				{
					$SQL .= ", APR_PRUEFER_N='" . $_POST["txtAPR_PRUEFER_N"] . "'";
				}
				if(isset($_POST["txtAPR_DATUM_N"]) AND $_POST["txtAPR_DATUM_N_old"]!=$_POST["txtAPR_DATUM_N"])
				{
					$SQL .= ", APR_DATUM_N=TO_DATE('" . awis_format($_POST["txtAPR_DATUM_N"],'Datum') . "','DD.MM.RRRR')";
				}
				if(isset($_POST["txtAPR_ERGEBNIS_N"]) AND $_POST["txtAPR_ERGEBNIS_N_old"]!=$_POST["txtAPR_ERGEBNIS_N"])
				{
					$SQL .= ", APR_ERGEBNIS_N='" . $_POST["txtAPR_ERGEBNIS_N"] . "'";
				}
				if(isset($_POST["txtAPR_MASSNAHME_N"]) AND $_POST["txtAPR_MASSNAHME_N_old"]!=$_POST["txtAPR_MASSNAHME_N"])
				{
					$SQL .= ", APR_MASSNAHME_N='" . $_POST["txtAPR_MASSNAHME_N"] . "'";
				}
				if(isset($_POST["txtAPR_MENGE_L"]) AND $_POST["txtAPR_MENGE_L_old"]!=$_POST["txtAPR_MENGE_L"])
				{
					$SQL = ', APR_MENGE_L=0' . $_POST["txtAPR_MENGE_L"];
				}
				if(isset($_POST["txtAPR_PRUEFER_L"]) AND $_POST["txtAPR_PRUEFER_L_old"]!=$_POST["txtAPR_PRUEFER_L"])
				{
					$SQL .= ", APR_PRUEFER_L='" . $_POST["txtAPR_PRUEFER_L"] . "'";
				}
				if(isset($_POST["txtAPR_DATUM_L"]) AND $_POST["txtAPR_DATUM_L_old"]!=$_POST["txtAPR_DATUM_L"])
				{
					$SQL .= ", APR_DATUM_L=TO_DATE('" . awis_Format($_POST["txtAPR_DATUM_L"],'Datum') . "','DD.MM.RRRR')";
				}
				if(isset($_POST["txtAPR_ERGEBNIS_L"]) AND $_POST["txtAPR_ERGEBNIS_L_old"]!=$_POST["txtAPR_ERGEBNIS_L"])
				{
					$SQL .= ", APR_ERGEBNIS_L='" . $_POST["txtAPR_ERGEBNIS_L"] . "'";
				}
				if(isset($_POST["txtAPR_MASSNAHME_L"]) AND $_POST["txtAPR_MASSNAHME_L_old"]!=$_POST["txtAPR_MASSNAHME_L"])
				{
					$SQL .= ", APR_MASSNAHME_L='" . $_POST["txtAPR_MASSNAHME_L"] . "'";
				}
				if(isset($_POST["txtAPR_FILIALBESTAND"]) AND $_POST["txtAPR_FILIALBESTAND_old"]!=$_POST["txtAPR_FILIALBESTAND"])
				{
					$SQL = ', APR_FILIALBESTAND=0' . $_POST["txtAPR_FILIALBESTAND"];
				}
				if(isset($_POST["txtAPR_MASSNAHME"]) AND $_POST["txtAPR_MASSNAHME_old"]!=$_POST["txtAPR_MASSNAHME"])
				{
					$SQL .= ", APR_MASSNAHME='" . $_POST["txtAPR_MASSNAHME"] . "'";
				}
				if(isset($_POST["txtAPR_MELDUNGDURCH"]) AND $_POST["txtAPR_MELDUNGDURCH_old"]!=$_POST["txtAPR_MELDUNGDURCH"])
				{
					$SQL .= ", APR_MELDUNGDURCH='" . $_POST["txtAPR_MELDUNGDURCH"] . "'";
				}
				if(isset($_POST["txtAPR_LIEFINFORMIERT"]) AND $_POST["txtAPR_LIEFINFORMIERT_old"]!=$_POST["txtAPR_LIEFINFORMIERT"])
				{
					$SQL .= ", APR_LIEFINFORMIERT='" . $_POST["txtAPR_LIEFINFORMIERT"] . "'";
				}
				if(isset($_POST["txtAPR_LIEFINFORMIERTAM"]) AND $_POST["txtAPR_LIEFINFORMIERTAM_old"]!=$_POST["txtAPR_LIEFINFORMIERTAM"])
				{
					$SQL .= ", APR_LIEFINFORMIERTAM=TO_DATE('" . awis_Format($_POST["txtAPR_LIEFINFORMIERTAM"],'Datum') . "','DD.MM.RRRR')";
				}
				if(isset($_POST["txtAPR_FEHLERBESCHREIBUNG"]) AND $_POST["txtAPR_FEHLERBESCHREIBUNG_old"]!=$_POST["txtAPR_FEHLERBESCHREIBUNG"])
				{
					$SQL .= ", APR_FEHLERBESCHREIBUNG='" . $_POST["txtAPR_FEHLERBESCHREIBUNG"] . "'";
				}

				if(isset($_POST["txtAPR_MASSNAHME_FILIALEN"]) AND $_POST["txtAPR_MASSNAHME_FILIALEN_old"]!=$_POST["txtAPR_MASSNAHME_FILIALEN"])
				{
					$SQL .= ", APR_MASSNAHME_FILIALEN='" . htmlspecialchars ($_POST["txtAPR_MASSNAHME_FILIALEN"]) . "'";
				}

				if(isset($_POST["txtAPR_WEITERGELEITETAM"]) AND $_POST["txtAPR_WEITERGELEITETAM_old"]!=$_POST["txtAPR_WEITERGELEITETAM"])
				{
					$SQL .= ", APR_WEITERGELEITETAM='" . awis_format($_POST["txtAPR_WEITERGELEITETAM"],'Datum') . "'";
				}

				if($SQL != '')
				{
					$SQL = 'UPDATE ARTIKELPRUEFUNGEN SET ' . substr($SQL,2);
					$SQL .= ' WHERE APR_KEY=' . $_POST["txtAPR_KEY_old"];
				}
				$KeyAPR = $_POST['txtAPR_KEY_old'];
			}
			else		// Neuen Datensatz hinzuf�gen
			{
				$KeyAPR = 0;	// Neu

				$SQL = 'INSERT INTO AWIS.ARTIKELPRUEFUNGEN (';
				$SQL .= ' APR_AST_ATUNR, APR_LAR_KEY,';
				$SQL .= ' APR_LAR_LARTNR, APR_LIE_NR, APR_FEHLERBESCHREIBUNG, ';
				$SQL .= ' APR_MENGE_N, APR_PRUEFER_N, APR_DATUM_N, ';
				$SQL .= ' APR_ERGEBNIS_N, APR_MASSNAHME_N, APR_MENGE_L, ';
				$SQL .= ' APR_PRUEFER_L, APR_DATUM_L, APR_ERGEBNIS_L, ';
				$SQL .= ' APR_MASSNAHME_L, APR_FILIALBESTAND, APR_MASSNAHME, ';
				$SQL .= ' APR_BEARBEITER, APR_BEARBEITUNGSTAG, APR_MELDUNGDURCH, ';
				$SQL .= ' APR_LIEFINFORMIERT, APR_LIEFINFORMIERTAM,  ';
				$SQL .= ' APR_MASSNAHME_FILIALEN,';
				$SQL .= ' APR_WEITERGELEITETAM, APR_USER,';
				$SQL .= ' APR_USERDAT) ';
				$SQL .= ' VALUES (';

				$SQL .= "'" . $_POST["txtAST_ATUNR"] . "'";
				$Felder = explode('~',urldecode($_POST["txtAPR_LAR"]));
				$SQL .= ", " . $Felder[0] . "";
				$SQL .= ", '" . $Felder[1] . "'";
				$SQL .= ", '" . $Felder[2] . "'";
				$SQL .= ", '" . $_POST["txtAPR_FEHLERBESCHREIBUNG"] . "'";
				$SQL .= ", " . ($_POST["txtAPR_MENGE_N"]==''?0:$_POST["txtAPR_MENGE_N"]) . "";
				$SQL .= ", '" . $_POST["txtAPR_PRUEFER_N"] . "'";
				if($_POST["txtAPR_DATUM_N"]=='')
				{
					$SQL .= ", NULL";
				}
				else
				{
					$SQL .= ", TO_DATE('" . awis_format($_POST["txtAPR_DATUM_N"],'Datum') . "','DD.MM.RRRR')";
				}

				$SQL .= ", '" . $_POST["txtAPR_ERGEBNIS_N"] . "'";
				$SQL .= ", '" . $_POST["txtAPR_MASSNAHME_N"] . "'";

				$SQL .= ", " . ($_POST["txtAPR_MENGE_L"]==''?0:$_POST["txtAPR_MENGE_L"]) . "";
				$SQL .= ", '" . $_POST["txtAPR_PRUEFER_L"] . "'";
				if($_POST["txtAPR_DATUM_L"]=='')
				{
					$SQL .= ", NULL";
				}
				else
				{
					$SQL .= ", TO_DATE('" . awis_format($_POST["txtAPR_DATUM_L"],'Datum') . "','DD.MM.RRRR')";
				}
				$SQL .= ", '" . $_POST["txtAPR_ERGEBNIS_L"] . "'";
				$SQL .= ", '" . $_POST["txtAPR_MASSNAHME_L"] . "'";
				$SQL .= ", " . ($_POST["txtAPR_FILIALBESTAND"]==''?0:$_POST["txtAPR_FILIALBESTAND"]) . "";
				$SQL .= ", '" . $_POST["txtAPR_MASSNAHME"] . "'";
				$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
				$SQL .= ", SYSDATE";
				$SQL .= ", '" . $_POST["txtAPR_MELDUNGDURCH"] . "'";
				$SQL .= ", '" . $_POST["txtAPR_LIEFINFORMIERT"] . "'";

				if($_POST["txtAPR_LIEFINFORMIERTAM"]=='')
				{
					$SQL .= ", NULL";
				}
				else
				{
					$SQL .= ", TO_DATE('" . awis_format($_POST["txtAPR_LIEFINFORMIERTAM"],'Datum') . "','DD.MM.RRRR')";
				}
				$SQL .= ",'" . $_POST['txtAPR_MASSNAHME_FILIALEN'] . "'";
				$SQL .= ", TO_DATE('" . awis_format($_POST['txtAPR_WEITERGELEITETAM'],'Datum') . "','DD.MM.RRRR')";
				$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "'";
				$SQL .= ", SYSDATE)";

			}		// �ndern oder Neu?
		}		// Ausgef�llt?

		if($SQL != '')
		{
			$Erg = awisExecute($con,$SQL );
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_Pruefungen.php", 2, $awisDBFehler['message']);
				die();
			}
			if($KeyAPR==0)
			{
				$rsKey = awisOpenRecordset($con, 'SELECT SEQ_APR_KEY.CURRVAL AS KEY FROM DUAL');
				$KeyAPR = $rsKey['KEY'][0];
			}
		}

		if($KeyAPR>0)		// Vorhandenen �ndern
		{
			$Liste = str_replace('txtAIS_', '', awis_NameInArray($_POST, 'txtAIS_', 1));
			$SQL = 'MERGE INTO ARTIKELPRUEFUNGENMELDUNGEN USING DUAL ON (APM_APR_KEY=0' . $KeyAPR . ')';
			$SQL .= " WHEN MATCHED THEN UPDATE SET APM_BENACHRICHTIGUNGEN='" . $Liste . "', APM_USER='" . $AWISBenutzer->BenutzerName() . "', APM_USERDAT=SYSDATE";
			$SQL .= " WHEN NOT MATCHED THEN INSERT (APM_APR_KEY, APM_BENACHRICHTIGUNGEN, APM_USER, APM_USERDAT)";
			$SQL .= " VALUES( " . $KeyAPR . ", '" . $Liste . "', '" . $AWISBenutzer->BenutzerName() . "', SYSDATE)";

			$Erg = awisExecute($con,$SQL );
			if($Erg==FALSE)
			{
				awisErrorMailLink("artikel_Pruefungen.php", 2, $awisDBFehler['message']);
				die();
			}

		}

	}	// Ende Speichern

//**************************************************************************
//
// Daten anzeigen
//
//**************************************************************************

	if(isset($_POST['cmdAPRNeu_x']))
	{
		$rsAPR = array();
	}
	else
	{
		$SQL = 'SELECT *';
		$SQL .= ' FROM ARTIKELPRUEFUNGEN LEFT OUTER JOIN Lieferanten ON APR_LIE_NR = LIE_NR';
		if($DetailAnzeigen)
		{
			$SQL .= " WHERE APR_KEY=0" . $_GET['APRKey'] . "";
		}
		else
		{
			$SQL .= " WHERE APR_AST_ATUNR='" . $Artikel[1] . "'";
		}

		$SQL .= ' ORDER BY APR_BEARBEITUNGSTAG DESC'	;

		$rsAPR = awisOpenRecordset($con, $SQL);
		$rsAPRZeilen = $awisRSZeilen;
	}


	if($DetailAnzeigen AND (($Recht407&2)==2))				// Einen Datensatz
	{
			// Bearbeitungsfelder

		echo "<input type=hidden name=txtAPR_KEY_old value='" . (isset($rsAPR['APR_KEY'][0])?$rsAPR['APR_KEY'][0]:'') . "'>";

		echo "<input type=hidden name=txtAPR_MENGE_N_old value='" . (isset($rsAPR['APR_MENGE_N'][0])?$rsAPR['APR_MENGE_N'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_PRUEFER_N_old value='" . (isset($rsAPR['APR_PRUEFER_N'][0])?$rsAPR['APR_PRUEFER_N'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_DATUM_N_old value='" . (isset($rsAPR['APR_DATUM_N'][0])?$rsAPR['APR_DATUM_N'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_ERGEBNIS_N_old value='" . (isset($rsAPR['APR_ERGEBNIS_N'][0])?$rsAPR['APR_ERGEBNIS_N'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_MASSNAHME_N_old value='" . (isset($rsAPR['APR_MASSNAHME_N'][0])?$rsAPR['APR_MASSNAHME_N'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_MENGE_L_old value='" . (isset($rsAPR['APR_MENGE_L'][0])?$rsAPR['APR_MENGE_L'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_PRUEFER_L_old value='" . (isset($rsAPR['APR_PRUEFER_L'][0])?$rsAPR['APR_PRUEFER_L'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_DATUM_L_old value='" . (isset($rsAPR['APR_DATUM_L'][0])?$rsAPR['APR_DATUM_L'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_ERGEBNIS_L_old value='" . (isset($rsAPR['APR_ERGEBNIS_L'][0])?$rsAPR['APR_ERGEBNIS_L'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_MASSNAHME_L_old value='" . (isset($rsAPR['APR_MASSNAHME_L'][0])?$rsAPR['APR_MASSNAHME_L'][0]:'') . "'>";

		echo "<input type=hidden name=txtAPR_FILIALBESTAND_old value='" . (isset($rsAPR['APR_FILIALBESTAND'][0])?$rsAPR['APR_FILIALBESTAND'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_MASSNAHME_old value='" . (isset($rsAPR['APR_MASSNAHME'][0])?$rsAPR['APR_MASSNAHME'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_MELDUNGDURCH_old value='" . (isset($rsAPR['APR_MELDUNGDURCH'][0])?$rsAPR['APR_MELDUNGDURCH'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_LIEFINFORMIERT_old value='" . (isset($rsAPR['APR_LIEFINFORMIERT'][0])?$rsAPR['APR_LIEFINFORMIERT'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_LIEFINFORMIERTAM_old value='" . (isset($rsAPR['APR_LIEFINFORMIERTAM'][0])?$rsAPR['APR_LIEFINFORMIERTAM'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_FEHLERBESCHREIBUNG_old value='" . (isset($rsAPR['APR_FEHLERBESCHREIBUNG'][0])?$rsAPR['APR_FEHLERBESCHREIBUNG'][0]:'') . "'>";
		echo "<input type=hidden name=txtAPR_MASSNAHME_FILIALEN_old value='" . (isset($rsAPR['APR_MASSNAHME_L'][0])?$rsAPR['APR_MASSNAHME_L'][0]:'') . "'>";

			// Maske aufbauen

		echo '<table border=1 width=100%>';
		if(($Recht407&64)==64)
		{
			$SQL = "SELECT DISTINCT LAR_KEY, LAR_LARTNR, LIE_NR, LIE_NAME1 ";
			$SQL .= " FROM AWIS.LieferantenArtikel INNER JOIN AWIS.Lieferanten ON LAR_LIE_NR = LIE_NR";
			$SQL .= " INNER JOIN AWIS.TeileInfos ON LAR_KEY = TEI_KEY2 WHERE TEI_WERT1='" . $Artikel[1] . "'";
			$SQL .= ' ORDER BY LAR_LARTNR';
			$rsLAR = awisOpenRecordset($con, $SQL);
			$rsLARZeilen = $awisRSZeilen;

			$KEY2=urlencode((isset($rsAPR['APR_LAR_KEY'][0])?$rsAPR['APR_LAR_KEY'][0]:'').'~'.(isset($rsAPR['APR_LAR_LARTNR'][0])?$rsAPR['APR_LAR_LARTNR'][0]:'').'~'.(isset($rsAPR['APR_LIE_NR'][0])?$rsAPR['APR_LIE_NR'][0]:''));
			//TODO: Aktivieren, wenn der Import umgestellt wurde.
			$AltDatensatz=1;//($rsAPR['APR_LAR_KEY'][0]==''?1:0);
//awis_Debug(1,$SQL);
			echo '<input type=hidden name=txtAPR_LAR_old value=' . $KEY2 . '>';

			echo '<tr><td valign=top width=100 class=FeldBez>Lieferantenartikel:</td><td>';
			echo '<select name=txtAPR_LAR>';

			echo '<option value=-1>::Bitte w�hlen::</option>';

			for($LARNr=0;$LARNr<$rsLARZeilen;$LARNr++)
			{
				$KEY1=urlencode($rsLAR['LAR_KEY'][$LARNr] .'~'.$rsLAR['LAR_LARTNR'][$LARNr].'~'.$rsLAR['LIE_NR'][$LARNr]);

				echo '<option ';
				echo ' value=' . $KEY1;
				if($AltDatensatz==0 AND $KEY1==$KEY2)
				{
					echo ' selected ';
				}
				elseif($AltDatensatz==1)
				{
					$KEYS1=explode('~',urldecode($KEY1));		// Nur die beiden letzten Felder
					$KEYS2=explode('~',urldecode($KEY2));
					if($KEYS1[1]==$KEYS2[1] AND $KEYS1[2]==$KEYS2[2])
					{
						echo ' selected ';
					}
				}
				echo '>';
				echo $rsLAR['LAR_LARTNR'][$LARNr] . ' (' . $rsLAR['LIE_NR'][$LARNr] . ' - ' . $rsLAR['LIE_NAME1'][$LARNr] . ')';
				echo '</option>';
			}

			echo '</select>';

			echo '</td>';
			echo "<td><img name=cmdPDF border=0 src=/bilder/pdf.png title='Pr�fung drucken' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Pruefungen&APRKey=".(isset($rsAPR['APR_KEY'][0])?$rsAPR['APR_KEY'][0]:'')."&Druck=PDF'></td>";
			echo '</tr>';

			echo '<tr><td valign=top width=100 class=FeldBez>Fehlerbeschreibung:</td><td>';
			echo "<textarea cols=120 rows=2 name=txtAPR_FEHLERBESCHREIBUNG>" . (isset($rsAPR['APR_FEHLERBESCHREIBUNG'][0])?$rsAPR['APR_FEHLERBESCHREIBUNG'][0]:'') . "</textarea>";

			echo '</td><td>';
			echo "<img name=cmdAPRTrefferliste border=0 src=/bilder/NeueListe.png title='Trefferliste Pr�fungen ' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Pruefungen'>";
			echo '</td></tr>';
		}
		else
		{
			echo '<tr><td width=100 class=FeldBez>Artikel:</td><td colspan=5>' . (isset($rsAPR['APR_LAR_LARTNR'][0])?$rsAPR['APR_LAR_LARTNR'][0]:'') . '</td>';
			echo "<td><img name=cmdPDF border=0 src=/bilder/pdf.png title='Pr�fung drucken' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Pruefungen&APRKey=".(isset($rsAPR['APR_KEY'][0])?$rsAPR['APR_KEY'][0]:'')."&Druck=PDF'></td></tr>";
			echo '<tr><td width=100 class=FeldBez>Fehlerbeschreibung:</td><td colspan=5>' . (isset($rsAPR['APR_FEHLERBESCHREIBUNG'][0])?$rsAPR['APR_FEHLERBESCHREIBUNG'][0]:'') . '';
			echo '</td><td align=right>';
			echo "<img name=cmdAPRTrefferliste border=0 src=/bilder/NeueListe.png title='Trefferliste Pr�fungen' onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Pruefungen'>";
			echo '</td></tr>';

		}
		echo '</table>';

		echo '<table border=1 width=100%>'	;
		echo '<tr><td valign=top width=40%>';		// Erste Spalte (WEIDEN)

		echo '<table>';
		echo '<tr><td colspan=2 class=FeldBez><b>Logistikzentrum Weiden</b></td></tr>';

		if(($Recht407&4)==4)
		{
			if(isset($_POST['cmdAPRNeu_x']))				// Neuer DS
			{
				$rsASI = awisOpenRecordset($con, "SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AST_ATUNR='" . $Artikel[1] . "' AND ASI_AIT_ID=10");
				$Bestand = $rsASI['ASI_WERT'][0];			// Wert aus dem aktuellen Bestand
			}
			else
			{
				$Bestand = $rsAPR['APR_MENGE_N'][0];	// Gepeicherter Bestand
			}

			echo "<tr><td width=100 class=FeldBez>Menge:</td><td><input type=text size=10 name=txtAPR_MENGE_N value='" . $Bestand . "'></td></tr>";
			echo "<tr><td width=100 class=FeldBez>Pr�fer:</td><td><input type=text size=20 name=txtAPR_PRUEFER_N value='" . (isset($rsAPR['APR_PRUEFER_N'][0])?$rsAPR['APR_PRUEFER_N'][0]:'') . "'></td></tr>";
			echo "<tr><td width=100 class=FeldBez>Datum:</td><td><input type=text size=10 name=txtAPR_DATUM_N value='" . (isset($rsAPR['APR_DATUM_N'][0])?$rsAPR['APR_DATUM_N'][0]:'') . "'></td></tr>";
			echo "<tr><td width=100 class=FeldBez valign=top>Ergebnis:</td><td><textarea cols=45 rows=2 name=txtAPR_ERGEBNIS_N>" . (isset($rsAPR['APR_ERGEBNIS_N'][0])?$rsAPR['APR_ERGEBNIS_N'][0]:'') . "</textarea></td></tr>";
		}
		else		// Anzeigen
		{
			echo '<tr><td width=100 class=FeldBez>Menge:</td><td>' . (isset($rsAPR['APR_MENGE_N'][0])?$rsAPR['APR_MENGE_N'][0]:'') . '</td></tr>';
			echo '<tr><td width=100 class=FeldBez>Pr�fer:</td><td>' . (isset($rsAPR['APR_PRUEFER_N'][0])?$rsAPR['APR_PRUEFER_N'][0]:'') . '</td></tr>';
			echo '<tr><td width=100 class=FeldBez>Datum:</td><td>' . (isset($rsAPR['APR_DATUM_N'][0])?$rsAPR['APR_DATUM_N'][0]:'') . '</td></tr>';
			echo '<tr><td width=100 class=FeldBez valign=top>Ergebnis:</td><td>' . (isset($rsAPR['APR_ERGEBNIS_N'][0])?$rsAPR['APR_ERGEBNIS_N'][0]:'') . '</td></tr>';
		}
		if(($Recht407&128)==128)
		{
			echo "<tr><td width=100 class=FeldBez valign=top>Ma�nahme:</td><td><textarea cols=45 rows=3 name=txtAPR_MASSNAHME_N>" . (isset($rsAPR['APR_MASSNAHME_N'][0])?$rsAPR['APR_MASSNAHME_N'][0]:'') . "</textarea></td></tr>";
		}
		else		// Anzeigen
		{
			echo '<tr><td width=100 class=FeldBez valign=top>Ma�nahme:</td><td>' . (isset($rsAPR['APR_MASSNAHME_N'][0])?$rsAPR['APR_MASSNAHME_N'][0]:'') . '</td></tr>';
		}



		echo '</table>';

		echo '</td><td valign=top width=40%>'; 	// Zweite Spalte (WERL)

		echo '<table>';
		echo '<tr><td colspan=2 class=FeldBez><b>Logistikzentrum Werl</b></td></tr>';
		if(($Recht407&8)==8)
		{
			if(isset($_POST['cmdAPRNeu_x']))				// Neuer DS
			{
				$rsASI = awisOpenRecordset($con, "SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AST_ATUNR='" . $Artikel[1] . "' AND ASI_AIT_ID=11");
				$Bestand = $rsASI['ASI_WERT'][0];			// Wert aus dem aktuellen Bestand
			}
			else
			{
				$Bestand = (isset($rsAPR['APR_MENGE_L'][0])?$rsAPR['APR_MENGE_L'][0]:'');	// Gepeicherter Bestand
			}

			echo "<tr><td width=100 class=FeldBez>Menge:</td><td><input type=text size=10 name=txtAPR_MENGE_L value='" . $Bestand . "'></td></tr>";
			echo "<tr><td width=100 class=FeldBez>Pr�fer:</td><td><input type=text size=20 name=txtAPR_PRUEFER_L value='" . (isset($rsAPR['APR_PRUEFER_L'][0])?$rsAPR['APR_PRUEFER_L'][0]:'') . "'></td></tr>";
			echo "<tr><td width=100 class=FeldBez>Datum:</td><td><input type=text size=10 name=txtAPR_DATUM_L value='" . (isset($rsAPR['APR_DATUM_L'][0])?$rsAPR['APR_DATUM_L'][0]:'') . "'></td></tr>";
			echo "<tr><td width=100 class=FeldBez valign=top>Ergebnis:</td><td><textarea cols=45 rows=2 name=txtAPR_ERGEBNIS_L>" . (isset($rsAPR['APR_ERGEBNIS_L'][0])?$rsAPR['APR_ERGEBNIS_L'][0]:'') . "</textarea></td></tr>";
		}
		else		// Anzeigen
		{
			echo '<tr><td width=100 class=FeldBez>Menge:</td><td>' . (isset($rsAPR['APR_MENGE_L'][0])?$rsAPR['APR_MENGE_L'][0]:'') . '</td></tr>';
			echo '<tr><td width=100 class=FeldBez>Pr�fer:</td><td>' . (isset($rsAPR['APR_PRUEFER_L'][0])?$rsAPR['APR_PRUEFER_L'][0]:'') . '</td></tr>';
			echo '<tr><td width=100 class=FeldBez>Datum:</td><td>' . (isset($rsAPR['APR_DATUM_L'][0])?$rsAPR['APR_DATUM_L'][0]:'') . '</td></tr>';
			echo '<tr><td width=100 class=FeldBez valign=top>Ergebnis:</td><td>' . (isset($rsAPR['APR_ERGEBNIS_L'][0])?$rsAPR['APR_ERGEBNIS_L'][0]:'') . '</td></tr>';
		}

		if(($Recht407&256)==256)
		{
			echo "<tr><td width=100 class=FeldBez valign=top>Ma�nahme:</td><td><textarea cols=45 rows=3 name=txtAPR_MASSNAHME_L>" . (isset($rsAPR['APR_MASSNAHME_L'][0])?$rsAPR['APR_MASSNAHME_L'][0]:'') . "</textarea></td></tr>";
		}
		else		// Anzeigen
		{
			echo '<tr><td width=100 class=FeldBez valign=top>Ma�nahme:</td><td>' . (isset($rsAPR['APR_MASSNAHME_L'][0])?$rsAPR['APR_MASSNAHME_L'][0]:'') . '</td></tr>';
		}

		echo '</table>';

		echo '</td><td valign=top width=20%>'; 	// Dritte Spalte

		echo '<table>';
		echo '<tr><td colspan=2 class=FeldBez><b>Filialbest�nde</b></td></tr>';
		if(($Recht407&16)==16)
		{
			if(isset($_POST['cmdAPRNeu_x']))				// Neuer DS
			{
				$rsASI = awisOpenRecordset($con, "SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AST_ATUNR='" . $Artikel[1] . "' AND ASI_AIT_ID=12");
				$Bestand = $rsASI['ASI_WERT'][0];			// Wert aus dem aktuellen Bestand
			}
			else
			{
				$Bestand = (isset($rsAPR['APR_FILIALBESTAND'][0])?$rsAPR['APR_FILIALBESTAND'][0]:'');	// Gepeicherter Bestand
			}

			echo "<tr><td width=100 class=FeldBez>Menge:</td><td><input type=text size=10 name=txtAPR_FILIALBESTAND value='" . $Bestand . "'></td></tr>";


			echo "<tr><td colspan=2 width=100 class=FeldBez valign=top>Ma�nahme:</td></tr><tr><td colspan=2><textarea type=text cols=20 rows=8 name=txtAPR_MASSNAHME_FILIALEN>" . (isset($rsAPR['APR_MASSNAHME_FILIALEN'][0])?$rsAPR['APR_MASSNAHME_FILIALEN'][0]:'') . "</textarea></td></tr>";

		}
		
		else		// Anzeigen
		{
			echo '<tr><td width=100 class=FeldBez>Menge:</td><td>' . (isset($rsAPR['APR_FILIALBESTAND'][0])?$rsAPR['APR_FILIALBESTAND'][0]:'') . '</td></tr>';
			echo "<tr><td colspan=2 width=100 class=FeldBez valign=top>Ma�nahme:</td></tr><tr><td colspan=2>".(isset($rsAPR['APR_MASSNAHME_FILIALEN'][0])?$rsAPR['APR_MASSNAHME_FILIALEN'][0]:'') . "</td></tr>";			
		}
		echo '</table>';

		echo '</td></tr></table>';

			// Abschlussblock
		echo '<table width=100% border=0>';

		echo '<tr>';
		echo '<td class=FeldBez>Bearbeiter:</td><td>' . (isset($rsAPR['APR_BEARBEITER'][0])?$rsAPR['APR_BEARBEITER'][0]:'') . '</td>';
		echo '<td class=FeldBez>Datum:</td><td>' . (isset($rsAPR['APR_BEARBEITUNGSTAG'][0])?$rsAPR['APR_BEARBEITUNGSTAG'][0]:'') . '</td>';
		if(($Recht407&32)==32)
		{
			echo "<td class=FeldBez>Meldung durch:</td><td><input type=text size=30 name=txtAPR_MELDUNGDURCH value='" . (isset($rsAPR['APR_MELDUNGDURCH'][0])?$rsAPR['APR_MELDUNGDURCH'][0]:'') . "'></td>";
		}
		else
		{
			echo '<td class=FeldBez>Meldung durch:</td><td>' . (isset($rsAPR['APR_MELDUNGDURCH'][0])?$rsAPR['APR_MELDUNGDURCH'][0]:'') . '</td>';
		}
		echo '</tr>';

			// Abschlussblock, zweite Zeile
		if(($Recht407&64)==64)
		{
			echo '<tr>';
			echo "<td class=FeldBez>Lieferanten informiert:</td><td><input type=text size=30 name=txtAPR_LIEFINFORMIERT value='" . (isset($rsAPR['APR_LIEFINFORMIERT'][0])?$rsAPR['APR_LIEFINFORMIERT'][0]:'') . "'></td>";
			echo "<td class=FeldBez>Datum:</td><td><input type=text size=15 name=txtAPR_LIEFINFORMIERTAM value='" . (isset($rsAPR['APR_LIEFINFORMIERTAM'][0])?$rsAPR['APR_LIEFINFORMIERTAM'][0]:'') . "'></td>";
			echo '</tr>';
		}
		else
		{
			echo '<tr>';
			echo '<td class=FeldBez>Lieferanten informiert:</td><td>' . (isset($rsAPR['APR_LIEFINFORMIERT'][0])?$rsAPR['APR_LIEFINFORMIERT'][0]:'') . '</td>';
			echo '<td class=FeldBez>Datum:</td><td>' . (isset($rsAPR['APR_LIEFINFORMIERTAM'][0])?$rsAPR['APR_LIEFINFORMIERTAM'][0]:'') . '</td>';
			echo '</tr>';
		}

			// Abschlussblock, zweite Zeile
		if(($Recht407&64)==64)
		{
			echo '<tr>';
			echo "<td class=FeldBez>Ma�nahme QS:</td><td colspan=5><textarea cols=120 rows=2 name=txtAPR_MASSNAHME>" . (isset($rsAPR['APR_MASSNAHME'][0])?$rsAPR['APR_MASSNAHME'][0]:'') . "</textarea></td>";
			echo '</tr>';
		}
		else
		{
			echo '<tr>';
			echo '<td class=FeldBez>Ma�nahme QS:</td><td colspan=5>' . (isset($rsAPR['APR_MASSNAHME'][0])?$rsAPR['APR_MASSNAHME'][0]:'') . '</td>';
			echo '</tr>';
		}


		echo '</table>';


/**********************************************************************************
* Benachrichtigungen f�r Abteilungen
**********************************************************************************/

		$SQL = 'SELECT * FROM ArtikelPruefungenMeldungen WHERE APM_APR_KEY=0' . (isset($rsAPR['APR_KEY'][0])?$rsAPR['APR_KEY'][0]:'');
		$rsAPM = awisOpenRecordset($con, $SQL);
		$Benachrichtigungen = explode(";",(isset($rsAPM['APM_BENACHRICHTIGUNGEN'][0])?$rsAPM['APM_BENACHRICHTIGUNGEN'][0]:''));

		$SQL = 'SELECT AIS_KEY, AIS_Bezeichnung, AIS_Status FROM ArtikelPruefungenInfoStellen';
		$rsAIS = awisOpenRecordset($con, $SQL);
		$AISZeilen = $awisRSZeilen;

		echo '<table><tr>';
		for($AISZeile=0;$AISZeile<$AISZeilen;$AISZeile++)
		{
			echo '<td>';
			if(($Recht407&32)==32)			// �ndern erlaubt
			{
				echo '<input type=checkbox name=txtAIS_' . $rsAIS['AIS_KEY'][$AISZeile];

				for($a=0;$a<count($Benachrichtigungen);$a++)
				{
					if($Benachrichtigungen[$a]==$rsAIS['AIS_KEY'][$AISZeile])
					{
						echo ' checked';
					}
				}
				echo '>';
				echo $rsAIS['AIS_BEZEICHNUNG'][$AISZeile];
			}
			else
			{
				for($a=0;$a<count($Benachrichtigungen);$a++)
				{
					if($Benachrichtigungen[$a]==$rsAIS['AIS_KEY'][$AISZeile])
					{
						$found=true;
						break;
					}
					else
					{
						$found=false;
					}
				}

				if($found)	// Aktiviert
				{
					echo '<input type=hidden name=txtAIS_' . (isset($rsAIS['AIS_KEY'][$AISZeile])?$rsAIS['AIS_KEY'][$AISZeile]:'').'>';
					echo '<b><font color=#0000FF>&radic;</font></b></td><td>';
					echo (isset($rsAIS['AIS_BEZEICHNUNG'][$AISZeile])?$rsAIS['AIS_BEZEICHNUNG'][$AISZeile]:'');
					echo '</td>';
				}
				else
				{
					echo '&nbsp;</td><td><font color=#A0A0A0>';
					echo (isset($rsAIS['AIS_BEZEICHNUNG'][$AISZeile])?$rsAIS['AIS_BEZEICHNUNG'][$AISZeile]:'');
					echo '</font></td>';
				}
			}

			if((($AISZeile+1)%4)==0 AND $AISZeile>0)
			{
				echo '</tr><tr>';
			}
		}
		echo '</tr></table>';
		if(($Recht407&32)==32)
		{
			echo '<input type=hidden name=txtAPR_WEITERGELEITETAM_old value=\''.(isset($rsAPR['APR_WEITERGELEITETAM'][0])?$rsAPR['APR_WEITERGELEITETAM'][0]:'').'\'>';
			echo '<hr>Weitergeleitet am:<input name=txtAPR_WEITERGELEITETAM value=\''.(isset($rsAPR['APR_WEITERGELEITETAM'][0])?$rsAPR['APR_WEITERGELEITETAM'][0]:'').'\' size=15><hr>';
		}
		else
		{
			echo '<hr>Weitergeleitet am: '.(isset($rsAPR['APR_WEITERGELEITETAM'][0])?$rsAPR['APR_WEITERGELEITETAM'][0]:'').'<hr>';
		}
		if($Recht407>3)
		{
			echo "<input type=image accesskey=S title='Speichern (Alt S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&NeuerDS=True'>";
		}
	}
	else					// Liste anzeigen
	{
		echo '<table width=100% border=1>';

		echo '<tr>'	;
		echo '<td class=FeldBez>Datum</td>';
		echo '<td class=FeldBez>Art-Nr</td>';
		echo '<td class=FeldBez>Lieferant</td>';
		echo '<td class=FeldBez>Lief-Nr</td>';
		echo '<td class=FeldBez>Fehlerbeschreibung</td>';
		if(($Recht407&512)==512)
		{
			echo '<td class=FeldBez>&nbsp;</td>';

		}
		echo '<tr>'	;

		$Recht407 = awisBenutzerRecht($con,407);
		for($APRZeile=0;$APRZeile<$rsAPRZeilen;$APRZeile++)
		{
			echo '<tr>'	;
			echo '<td>';
			if(($Recht407&2)==2)
			{
				echo "<a title='Details anzeigen' href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Pruefungen&APRKey=0". $rsAPR['APR_KEY'][$APRZeile] . '>'. ($rsAPR['APR_BEARBEITUNGSTAG'][$APRZeile]==''?'::ohne Datum::':$rsAPR['APR_BEARBEITUNGSTAG'][$APRZeile]) . '</a></td>';
			}
			else
			{
				echo $rsAPR['APR_BEARBEITUNGSTAG'][$APRZeile] . '</td>';
			}
			echo '<td>' . $rsAPR['APR_LAR_LARTNR'][$APRZeile] . '</td>';
			echo '<td>' . $rsAPR['LIE_NAME1'][$APRZeile] . '</td>';
			echo "<td><a href=/stammdaten/lieferanten.php?LIENR=" . $rsAPR['APR_LIE_NR'][$APRZeile] . "&Zurueck=artikel_APR&ASTNR=" . $rsAPR["APR_AST_ATUNR"][$APRZeile] . ">" . $rsAPR['APR_LIE_NR'][$APRZeile] . "</td>";
			echo '<td>' . $rsAPR['APR_FEHLERBESCHREIBUNG'][$APRZeile] . '</td>';
			if(($Recht407&512)==512)
			{
				echo "<td><input type=image title=\"L�schen (Alt x)\" src=/bilder/muelleimer.png name=cmdAPRLoeschen_" . $rsAPR["APR_KEY"][$APRZeile] . " accesskey=x></td>";
			}
			echo '<tr>'	;
		}
		echo '</table>';
			// Neuen Datensatz hinzuf�gen
		if(($Recht407&1024)==1024)
		{
			echo "<input type=image accesskey=N title='Hinzuf�gen (Alt N)' src=/bilder/plus.png name=cmdAPRNeu>&nbsp;";
		}
	}

?>