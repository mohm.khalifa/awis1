<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try 
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form->DebugAusgabe(1,'l�schen:');
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);

	$Tabelle= '';
	
	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtRRP_KEY']))
		{
			$Tabelle = 'RRP';
			$Key=$_POST['txtRRP_KEY'];
	
			$SQL = 'SELECT RRP_KEY, DECODE(RRP_AST_KEY,NULL,0,1) AS GELISTET, RRP_MEHRFACHKENNUNG, RRP_ATUNR, RRP_BEZEICHNUNGWW';
			$SQL .= ' FROM REIFENPFLEGE ';
			$SQL .= ' WHERE RRP_KEY=0'.$Key;
			
			$rsDaten = $DB->RecordsetOeffnen($SQL);
			$Gelistet = $rsDaten->FeldInhalt('GELISTET');
			$Mehrfachkennung = $rsDaten->FeldInhalt('RRP_MEHRFACHKENNUNG');
			
			$HauptKey = $rsDaten->FeldInhalt('RRP_KEY');
				
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_ATUNR'),$rsDaten->FeldInhalt('RRP_ATUNR'));
			$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_BEZEICHNUNGWW'),$rsDaten->FeldInhalt('RRP_BEZEICHNUNGWW'));
			$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_MEHRFACHKENNUNG'),$rsDaten->FeldInhalt('RRP_MEHRFACHKENNUNG'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		switch($_GET['Seite'])
		{
			case 'Reifenpflege':
				$Tabelle = 'RRP';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT RRP_KEY, DECODE(RRP_AST_KEY,NULL,0,1) AS GELISTET, RRP_MEHRFACHKENNUNG, RRP_ATUNR, RRP_BEZEICHNUNGWW';
				$SQL .= ' FROM REIFENPFLEGE ';
				$SQL .= ' WHERE RRP_KEY=0'.$Key;
				
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$Gelistet = $rsDaten->FeldInhalt('GELISTET');
				$Mehrfachkennung = $rsDaten->FeldInhalt('RRP_MEHRFACHKENNUNG');

				$HauptKey = $rsDaten->FeldInhalt('RRP_KEY');
				
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_ATUNR'),$rsDaten->FeldInhalt('RRP_ATUNR'));
				$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_BEZEICHNUNGWW'),$rsDaten->FeldInhalt('RRP_BEZEICHNUNGWW'));
				$Felder[]=array($Form->LadeTextBaustein('RRP','RRP_MEHRFACHKENNUNG'),$rsDaten->FeldInhalt('RRP_MEHRFACHKENNUNG'));
				break;
			case 'Preise':
				$Tabelle = 'RPP';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT';
				$SQL .= ' RRP_KEY, RPP_KEY, RPP_VK, RPP_GUELTIGAB,';
				$SQL .= ' COALESCE (LAN_LAND, \'unbekannt\') AS LAN_LAND';
				$SQL .= ' FROM REIFENPFLEGEPREISE ';
				$SQL .= ' INNER JOIN REIFENPFLEGE ON RRP_KEY=RPP_RRP_KEY ';
				$SQL .= ' LEFT JOIN LAENDER ON LAN_CODE=RPP_LAN_CODE ';
				$SQL .= ' WHERE RPP_KEY=0'.$Key;
												
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('RRP_KEY');
				$SubKey=$rsDaten->FeldInhalt('RPP_KEY');
				
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RPP','RPP_LAN_CODE'),$rsDaten->FeldInhalt('LAN_LAND'));
				$Felder[]=array($Form->LadeTextBaustein('RPP','RPP_VK'),$Form->Format('N2', $rsDaten->FeldInhalt('RPP_VK')));
				$Felder[]=array($Form->LadeTextBaustein('RPP','RPP_GUELTIGAB'),$Form->Format('D', $rsDaten->FeldInhalt('RPP_GUELTIGAB')));
				break;
			case 'Wiedervorlagen':
				$Tabelle = 'WVL';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT';
				$SQL .= ' RRP_KEY, WVL_KEY, WVL_DATUM, WVL_TEXT, WVL_BEARBEITER';
				$SQL .= ' FROM WIEDERVORLAGEN ';
				$SQL .= ' INNER JOIN REIFENPFLEGE ON RRP_KEY=WVL_XXX_KEY ';
				$SQL .= ' WHERE WVL_KEY=0'.$Key;
												
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('RRP_KEY');
				$SubKey=$rsDaten->FeldInhalt('WVL_KEY');
				
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('WVL','WVL_DATUM'),$Form->Format('D', $rsDaten->FeldInhalt('WVL_DATUM')));
				$Felder[]=array($Form->LadeTextBaustein('WVL','WVL_TEXT'),$Form->Format('T', $rsDaten->FeldInhalt('WVL_TEXT')));
				$Felder[]=array($Form->LadeTextBaustein('WVL','WVL_BEARBEITER'),$Form->Format('T', $rsDaten->FeldInhalt('WVL_BEARBEITER')));
				break;
			default:
				break;
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'RRP':
				if ($_POST['txtGelistet']==1)
				{
					if ($_POST['txtMehrfachkennung']=='')
					{
						$SQL = 'UPDATE REIFENPFLEGE';
						$SQL .= ' SET RRP_GUELTIGBIS=SYSDATE-1';
						$SQL .= ', RRP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', RRP_USERDAT=SYSDATE';
						$SQL .= ' WHERE RRP_KEY=0'.$_POST['txtKey'];
						$DB->Ausfuehren($SQL,'',true);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());
					}
					else 
					{
						$DB->TransaktionBegin();
						
						$SQL = 'DELETE FROM REIFENPFLEGE WHERE RRP_KEY=0'.$_POST['txtKey'];
						$DB->Ausfuehren($SQL,'',true);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());
						
						$SQL = 'DELETE FROM WIEDERVORLAGEN ' ;
						$SQL .= ' WHERE WVL_XTN_KUEREL=\'RRP\'';
						$SQL .= ' AND WVL_XXX_KEY=0'.$_POST['txtKey'];
						$DB->Ausfuehren($SQL,'',true);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());

						$DB->TransaktionCommit();
						$AWIS_KEY1='';
					}
				}
				else
				{
					$SQL = 'DELETE FROM REIFENPFLEGE WHERE RRP_KEY=0'.$_POST['txtKey'];
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
					$AWIS_KEY1='';
				}
				break;		
			case 'RPP':
				$SQL = 'DELETE FROM REIFENPFLEGEPREISE WHERE RPP_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				$AWIS_KEY1=$_POST['txtHauptKey'];
				$AWIS_KEY2='';
				break;		
			case 'WVL':
				$SQL = 'DELETE FROM WIEDERVORLAGEN WHERE WVL_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				$AWIS_KEY1=$_POST['txtHauptKey'];
				$AWIS_KEY2='';
				break;		
			default:
				break;
		}
	}
	
	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./reifenpflege_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
		$Form->Formular_Start();
		
		$Form->ZeileStart();		
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	
		$Form->ZeileEnde();
	
		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',200);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}
	
		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
		if (isset($SubKey))
		{
			$Form->Erstelle_HiddenFeld('SubKey',$SubKey);			// Bei den Unterseiten
		}
		if (isset($Gelistet))
		{
			$Form->Erstelle_HiddenFeld('Gelistet',$Gelistet);
		}
		if (isset($Mehrfachkennung))
		{
			$Form->Erstelle_HiddenFeld('Mehrfachkennung',$Mehrfachkennung);
		}
		
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();
	
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();
	
		$Form->SchreibeHTMLCode('</form>');
	
		$Form->Formular_Ende();
	
		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>