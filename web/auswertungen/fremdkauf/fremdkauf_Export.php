<?php
require_once 'awisFremdkaufauswertung.php';
global $FRK;
try
{
    if(isset($_POST['cmdSpeichern_x'])){
        $SQL = 'Update jobliste set XJO_NAECHSTERSTART = ' . $FRK->DB->WertSetzen('XJO','DU',$_POST['txtXJO_NAECHSTERSTART']);
        $SQL .= ' WHERE XJO_JOBBEZEICHNUNG = ' . $FRK->DB->WertSetzen('XJO','T','Fremdkaufauswertung');
        $FRK->DB->Ausfuehren($SQL,'',false,$FRK->DB->Bindevariablen('XJO'));
    }

    $FRK->RechteMeldung(0);//Anzeigen Recht
	echo "<form name=frmSuche method=post action=fremdkauf_Main.php?cmdAktion=Export>";
	$FRK->Form->Formular_Start();

	$SQL = 'select * from jobliste where XJO_JOBBEZEICHNUNG = ' . $FRK->DB->WertSetzen('XJO','T','Fremdkaufauswertung');
	$rsXJO = $FRK->DB->RecordSetOeffnen($SQL, $FRK->DB->Bindevariablen('XJO'));
	$FRK->Form->ZeileStart();
    $FRK->Form->Erstelle_TextLabel('N�chster Start: ',150);
    $FRK->Form->Erstelle_TextFeld('XJO_NAECHSTERSTART',$rsXJO->FeldInhalt('XJO_NAECHSTERSTART'),25,250,true,'','','','DU');
	$FRK->Form->ZeileEnde();

	$FRK->Form->ZeileStart();
    $Pfad = awisFremdkaufauswertung::PFAD;
	if(is_file($Pfad)){
        $Datum = fileatime($Pfad);
        $Datum = date('d.m.y H:i:s',$Datum);
        $FRK->Form->Erstelle_TextLabel('Download letzte Ergebnisdatei: ' , 220);

        $FRK->Form->Erstelle_TextLabel($Datum,200,'','/dokumentanzeigen.php?erweiterung=csv&dateiname=Fremdkaufauswertung.csv&bereich=fremdkaufauswertung');
    }else{
	    $FRK->Form->Hinweistext('Keine Datei vorhanden.',awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);
    }

    $FRK->Form->ZeileEnde();


	$FRK->Form->Formular_Ende();

	$FRK->Form->SchaltflaechenStart();
	$FRK->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$FRK->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$FRK->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $FRK->AWISSprachKonserven['Wort']['lbl_speichern'], 'W');
	$FRK->Form->SchaltflaechenEnde();

	$FRK->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($FRK->Form instanceof awisFormular)
	{
		$FRK->Form->DebugAusgabe(1, $ex->getSQL());
		$FRK->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201911221053");
	}
	else
	{
		$FRK->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($FRK->Form instanceof awisFormular)
	{
		$FRK->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"202911221054");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>