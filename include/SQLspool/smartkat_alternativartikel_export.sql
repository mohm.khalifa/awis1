set pagesize 0;
set heading off;
set feedback off;
set verify off;
set term off;
set trimspool on;
set linesize 32676;

spool /win/applicationdata2/AWIS/06-Smartkat/awis_alternativartikel_export.csv REPLACE;

select ATUNR
  || ';'
  || REFTYP
  || ';'
  || sort
  || ';'
  || PRIO
  || ';'
  || HERSTELLER
  || ';'
  || ATUNR_REF
from
 V_SMARTKAT_ALTERNATIVARTIKEL;

spool off;
exit;

