<?php
	require_once('/daten/include/db.inc.php');		// DB-Befehle
	
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $con;
	global $AWISBenutzer;

	//$Rechte_OEN = awisBenutzerRecht($con,400);
	$Recht400 = awisBenutzerRecht($con,400);
	$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));

	$SQL = "select DISTINCT Daten.*, OEN_Key FROM (SELECT Fremdkaeufe.* FROM Fremdkaeufe WHERE FRK_AST_ATUNR = '" . $Artikel[1] . "'";
	$SQL .= " UNION SELECT Fremdkaeufe.* FROM Fremdkaeufe WHERE FRK_OEN_SUCHNUMMER IN ";
	$SQL .= " (SELECT TEI_SUCH2 FROM AWIS.TeileInfos WHERE TEI_ITY_ID2='OEN' AND TEI_KEY1=0" . $Artikel[0] . ")) DATEN";
	$SQL .= " 	  	LEFT OUTER JOIN OENUmmern ON OENUMMERN.OEN_NUMMER = Daten.FRK_OEN_Nummer";
	
	
	if(($Recht400&1)==1 && ($UserFilialen!='' && $UserFilialen!='0'))
	{
		$SQL .= " WHERE FRK_QUELLE='F' AND FRK_FIL_ID IN (".$UserFilialen.")";		
	}
	
	if(isset($_GET['Sort']))
	{
		$SQL .= " ORDER BY " . $_GET['Sort'] . ' ' . $_GET['SortTyp'];
	}
	else
	{
		$SQL .= " ORDER BY FRK_DATUM DESC, FRK_LFDNR, FRK_FIL_ID";
	}
	
	awis_Debug(1,$SQL);
	
	$rsOEP = awisOpenRecordset($con, $SQL);
	if($rsOEP==FALSE)
	{
		awisErrorMailLink("artikel_Fremdkauf.php", 2, $awisDBFehler['message']);
	}
	$rsOEPZeilen=$awisRSZeilen;

/*
	$AnzFK=0;
	$Doppelte = array();

	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		$SuchKey = $rsOEP['FRK_FIL_ID'][$OEPZeile] . '-' . $rsOEP['FRK_LFDNR'][$OEPZeile];
		if(array_search($SuchKey, $Doppelte)===FALSE)
		{
			$Dat = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+1);
			if($Dat > time())
			{
				$AnzFK+=$rsOEP['FRK_MENGE'][$OEPZeile];
			}
			$AnzFKGesamt+=$rsOEP['FRK_MENGE'][$OEPZeile];
			array_push($Doppelte, $SuchKey);
		}
	}
//echo '<hr>' . $SQL . '<hr>';
	echo 'Fremdk�ufe gesamt <b>' . $AnzFKGesamt . '</b>. In den letzten 12 Monaten: <b>' . $AnzFK . '</b>';
*/

	$AnzFKGesamt=0;
	$AnzFK=0;
	$Doppelte = array();
	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		$SuchKey = $rsOEP['FRK_FIL_ID'][$OEPZeile] . '-' . $rsOEP['FRK_LFDNR'][$OEPZeile];
		if(array_search($SuchKey, $Doppelte)===FALSE)
		{
				// Gr��er als 1 Jahr
			$Dat1 = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+1);
			$Dat2 = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+2);
			$Dat3 = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+3);
			if($Dat1 >= time())
			{
				$AnzFK+=$rsOEP['FRK_MENGE'][$OEPZeile];
			}
				// Gr��er als 2 Jahre
			if($Dat2 >= time())
			{
//				$AnzFK+=$rsOEP['FRK_MENGE'][$OEPZeile];
				$AnzFKGesamt+=$rsOEP['FRK_MENGE'][$OEPZeile];
			}
//			$AnzFKGesamt+=$rsOEP['FRK_MENGE'][$OEPZeile];
			array_push($Doppelte, $SuchKey);
		}
	}
	echo 'Fremdk�ufe in den letzten 24 Monaten: ' . ($AnzFKGesamt) . '. Davon in den letzten 12 Monaten: ' . $AnzFK;

/**********************/
// ENDE Summe
/**********************/

	echo "<table id=DatenTabelle width=100% border=1>";
	$SortFeld = isset($_GET['Sort'])?$_GET['Sort']:'';
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_FIL_ID&SortTyp=".($SortFeld=='FRK_FIL_ID'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Filiale</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_DATUM&SortTyp=".($SortFeld=='FRK_DATUM'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Datum</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_LFDNR&SortTyp=".($SortFeld=='FRK_LFDNR'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Lfd.Nr</a></td>";
	echo "<td id=FeldBez>WstNr</td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_KBA&SortTyp=".($SortFeld=='FRK_KBA'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">KBA-Nr</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_MENGE&SortTyp=".($SortFeld=='FRK_MENGE'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Menge</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_GRUND&SortTyp=".($SortFeld=='FRK_GRUND'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Grund</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_OEN_NUMMER&SortTyp=".($SortFeld=='FRK_OEN_NUMMER'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">OE-Nr</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_ARTBEZ&SortTyp=".($SortFeld=='FRK_ARTBEZ'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Artikel</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_PREIS&SortTyp=".($SortFeld=='FRK_PREIS'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Preis</a></td>";
	echo "<td id=FeldBez><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Fremdkauf&Sort=FRK_RABATT&SortTyp=".($SortFeld=='FRK_RABATT'?($_GET['SortTyp']=='ASC'?'DESC':'ASC'):'ASC').">Rabatt</a></td>";
	echo "</tr>";

	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		echo '<tr>';
		echo '<td><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&FIL_ID=' .$rsOEP['FRK_FIL_ID'][$OEPZeile].'>'	. $rsOEP['FRK_FIL_ID'][$OEPZeile] . '</a></td>';
		echo '<td>'	. $rsOEP['FRK_DATUM'][$OEPZeile] . '</td>';

		echo '<td><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&LFDNR=' . $rsOEP['FRK_LFDNR'][$OEPZeile] . '&FIL_ID=' . $rsOEP['FRK_FIL_ID'][$OEPZeile] . '>' . $rsOEP['FRK_LFDNR'][$OEPZeile] . '</a>';
		echo " (<span title='Anzahl Datens�tze f�r die Lfrd.Nr und die Filiale'>";
		$rsAnzFK = awisOpenRecordset($con, "SELECT COUNT(*) AS ANZ FROM Fremdkaeufe WHERE FRK_LFDNR='" . $rsOEP['FRK_LFDNR'][$OEPZeile] . "' AND FRK_FIL_ID=" . $rsOEP['FRK_FIL_ID'][$OEPZeile]);
		echo $rsAnzFK['ANZ'][0];
		echo '</span>)';
		echo '</td>';

		echo '<td>'	. $rsOEP['FRK_WSTNR'][$OEPZeile] . '</td>';
		echo '<td><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&KBA_NR=' . $rsOEP['FRK_KBA'][$OEPZeile] . '>' . $rsOEP['FRK_KBA'][$OEPZeile] . '</a></td>';
		echo '<td>'	. $rsOEP['FRK_MENGE'][$OEPZeile] . '</td>';
		echo '<td>'	. $rsOEP['FRK_GRUND'][$OEPZeile] . '</td>';
		echo '<td>'	. ($rsOEP['OEN_KEY'][$OEPZeile]==''?$rsOEP['FRK_OEN_NUMMER'][$OEPZeile]:'<a href=./artikel_Main.php?cmdAktion=OENummern&OENKEY='.$rsOEP['OEN_KEY'][$OEPZeile].'>'.$rsOEP['FRK_OEN_NUMMER'][$OEPZeile].'</a>') . '</td>';
		echo '<td>'	. $rsOEP['FRK_ARTBEZ'][$OEPZeile] . '</td>';
		echo '<td align=right>'	. awis_format($rsOEP['FRK_PREIS'][$OEPZeile],'Currency') . '</td>';
		echo '<td align=right>'	. $rsOEP['FRK_RABATT'][$OEPZeile] . '%</td>';
		echo '</tr>';
	}

	echo "</table>";
	unset($rsOEP);

?>