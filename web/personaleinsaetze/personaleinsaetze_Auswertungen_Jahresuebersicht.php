<?php

/***************************************
* Jahresübersicht
***************************************/
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');


global $AWIS_KEY1;

try 
{
	$AWISBenutzer = awisBenutzer::Init();	
	$Form = new awisFormular();
	
	$TextKonserven = array();				
	$TextKonserven[]=array('Ausdruck','txt_KeineDatenGefunden');	
	$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
	$TextKonserven[]=array('Wort','wrd_Stand');	
	$TextKonserven[]=array('Wort','Seite');
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht4505 = $AWISBenutzer->HatDasRecht(4505);
	if($Recht4505==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI_Auswertung'));					
		
	$SQL = "SELECT xbn_name AS Mitarbeiter, ";
				 //xbn_name || coalesce(\', \'||xbn_vorname,\'\') AS Mitarbeiter, ";	
	$SQL .= " PEA_KEY||DECODE(PEI_PEA_KEY,8,'0'||PEI_FIL_ID,0) AS PEAKEY, PEA_BEZEICHNUNG, ";
	$SQL .= " DECODE(PEI_PEA_KEY,8,PEI_FIL_ID ||  ', ' || FIL_BEZ,NULL) AS Filiale, ";
	$SQL .= " to_char(PEI_DATUM,'RRRR') AS Jahr, to_char(PEI_DATUM,'MM') AS Monat, COUNT(*) AS Anzahl, XBN_KEY ";	

	//$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';		
	
	$SQL .= ' FROM Personaleinsaetze';
	$SQL .= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';	
	$SQL .= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	$SQL .= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';	
	$SQL .= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
	
	$SQL .= ' WHERE PEA_KEY <> 9 AND (PEI_PLANUNGSTYP is null or PEI_PLANUNGSTYP = 2)';
	//$SQL .= ' WHERE PEA_KEY <> 9';
	//$SQL .= " AND MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";
	
	
	if(($AWISBenutzer->HatDasRecht(4500)&16)==0)		// Nur den eigenen Bereich anzeigen
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM PerseinsBereicheMitglieder WHERE ';
		$Bedingung .= ' PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
	}
	
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$SQL .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$SQL .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}
	
	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$SQL .= ' AND PEI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID'],false);
	}
	
	// Einschränken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
		if($Param['PBM_XBN_KEY']==0)
		{
			$SQL .= " AND EXISTS (SELECT *";
			$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$SQL .= "       FROM Perseinsbereichemitglieder";
			$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			$SQL .= " GROUP BY PEB_KEY) Bereiche";
			$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate and CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key "; //TR 07.02.11
			//$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			$SQL .= ' WHERE pbm_xbn_key = PEI_XBN_KEY';
			$SQL .= ' )';
		}
		else
		{
			$SQL .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY'],false);
		}
	}
	
	if(isset($Param['FER_KEY']) AND $Param['FER_KEY']!='0')
	{

		$SQL .= ' AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN';
		$SQL .= ' WHERE xx1_fer_key =0'.$DB->FeldInhaltFormat('N0',$Param['FER_KEY'],false);
		$SQL .= ' AND xx1_kon_key = KON_KEY)';
		/*
		  $Bedingung = " AND EXISTS (SELECT *
		  FROM (
		  SELECT KON_KEY, XX1_FER_KEY, FEB_GUELTIGAB, FRZ_GUELTIGAB, FEZ_GUELTIGAB, FER_GUELTIGAB,
		  FEB_GUELTIGBIS, FRZ_GUELTIGBIS, FEZ_GUELTIGBIS, FER_GUELTIGBIS
		  FROM v_FilialEbenenRollen
		  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY
		  INNER JOIN FilialEbenen ON XX1_FEB_KEY = FEB_KEY
		  INNER JOIN FilialEbenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY
		  INNER JOIN FilialEbenenZuordnungen ON XX1_FEZ_KEY = FEZ_KEY
		  INNER JOIN FilialEbenenRollen ON XX1_FER_KEY = FER_KEY
		  INNER JOIN FilialEbenenRollenBereiche ON FER_FRB_KEY = FRB_KEY
		  WHERE XX1_FER_KEY =".$Param['FER_KEY']."
		  ORDER BY XX1_STUFE, FRZ_GUELTIGBIS, FRZ_USERDAT DESC
		  ) Daten WHERE KON_KEY = XBN_KON_KEY
		  AND FEB_GUELTIGAB <= PEI_DATUM AND FEB_GUELTIGBIS >= PEI_DATUM
		  AND FRZ_GUELTIGAB <= PEI_DATUM AND FRZ_GUELTIGBIS >= PEI_DATUM
		  AND FEZ_GUELTIGAB <= PEI_DATUM AND FEZ_GUELTIGBIS >= PEI_DATUM
		  AND FER_GUELTIGAB <= PEI_DATUM AND FER_GUELTIGBIS >= PEI_DATUM
		  ) ";
		*/
	}
		
	/*	
	if($_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
	{
		$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
	}
	*/		
						
	$SQL .= " GROUP BY to_char(PEI_DATUM,'RRRR'), xbn_name, PEA_KEY||DECODE(PEI_PEA_KEY,8,'0'||PEI_FIL_ID,0), PEA_BEZEICHNUNG, to_char(PEI_DATUM,'MM'), DECODE(PEI_PEA_KEY,8,PEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), XBN_KEY";
	$SQL .= " ORDER BY to_char(PEI_DATUM,'RRRR'), xbn_name, PEA_KEY||DECODE(PEI_PEA_KEY,8,'0'||PEI_FIL_ID,0), PEA_BEZEICHNUNG, to_char(PEI_DATUM,'MM'), DECODE(PEI_PEA_KEY,8,PEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), XBN_KEY";	
	
	//$SQL .= " GROUP BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'MM'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
	//$SQL .= " ORDER BY REZ_ID, to_char(MEI_TAG,'RRRR'), MIT_BEZEICHNUNG, REZ_BEZEICHNUNG, MEA_KEY||DECODE(MEI_MEA_KEY,8,'0'||MEI_FIL_ID,0), MEA_BEZEICHNUNG, to_char(MEI_TAG,'MM'), DECODE(MEI_MEA_KEY,8,MEI_FIL_ID ||  ', ' || FIL_BEZ,NULL), MIT_KEY";
			
		
	$rsPEI = $DB->RecordSetOeffnen($SQL);
	$rsPEIZeilen = $rsPEI->AnzahlDatensaetze();
	
	$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');

	$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Personaleinsatz');
	//$Ausdruck->NeueSeite(0,1);		// Mit Hintergrund

	if($rsPEIZeilen==0)
	{
		$Ausdruck->NeueSeite(0,1);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_KeineDatenGefunden'],0,0,'C',0);		
	}
	
	$RasterZeile=9999;
	$LetzterMitarbeiter = '';
	$LetztesJahr = '';
	$LetztesREZ='';
	$LetztesMEA='';
	$Summen = array();
	
	while(!$rsPEI->EOF())
	{
		// Seitenwechsel?
		//if($LetztesJahr!=$rsPEI['JAHR'][$i] OR $LetztesREZ!=$rsMEI['REZ_BEZEICHNUNG'][$i] OR $LetzterMitarbeiter!=$rsMEI['MIT_KEY'][$i])
		if($LetztesJahr!=$rsPEI->FeldInhalt('JAHR') OR $LetzterMitarbeiter!=$rsPEI->FeldInhalt('XBN_KEY'))
		{
			$LetztesJahr=$rsPEI->FeldInhalt('JAHR');
			$LetzterMitarbeiter=$rsPEI->FeldInhalt('XBN_KEY');
			//$LetztesREZ=$rsMEI['REZ_BEZEICHNUNG'][$i];
			$LetztesPEA='';

			$RasterZeile = 9999;	// Neue Seite erzwingen
		}

		// Neue Seite beginnen
		if($RasterZeile > 190)	
		{
			
			foreach($Summen AS $Pos=>$Wert)
			{
				$Ausdruck->_pdf->setXY((13*12)+100,$Pos-10);
				$Ausdruck->_pdf->cell(20,6,$Wert,1,0,'R',0);
			}
			$Summen = array();		// Neue Summen

			$Ausdruck->NeueSeite(0,1);
			
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

			$Ausdruck->_pdf->setXY(10,12);
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);
			
			$RasterZeile = 10;
			$Ausdruck->_pdf->SetFont('Arial','',20);
			$Ausdruck->_pdf->text(10,$RasterZeile,"Filialbesuche vom " . $Param['PEI_DATUM_VOM'] . ' bis ' . $Param['PEI_DATUM_BIS']);
			$RasterZeile = 40;		

			$Ausdruck->_pdf->SetFillColor(240,240,240);
			$Ausdruck->_pdf->SetDrawColor(0,0,0);
			$Ausdruck->_pdf->setXY(10,$RasterZeile-10);

			$Ausdruck->_pdf->SetFont('Arial','',12);
			//$Ausdruck->_pdf->cell(102,6,$rsPEI->FeldInhalt('Mitarbeiter') . ' (' . $rsMEI['REZ_BEZEICHNUNG'][$i] .')',1,0,'L',1);
			$Ausdruck->_pdf->cell(102,6,$rsPEI->FeldInhalt('MITARBEITER'),1,0,'L',1);

			$Ausdruck->_pdf->SetFont('Arial','',10);
			for($Std=1;$Std<=12;$Std++)
			{
				$Ausdruck->_pdf->setXY(($Std*12)+100,$RasterZeile-10);
				$Ausdruck->_pdf->SetDrawColor(0,0,0);
				$Ausdruck->_pdf->cell(12,6,date('M',mktime(0,0,0,$Std,1,2005)),1,0,'C',1);
			}
			$Ausdruck->_pdf->setXY(($Std*12)+100,$RasterZeile-10);
			$Ausdruck->_pdf->SetDrawColor(0,0,0);
			$Ausdruck->_pdf->cell(20,6,'Gesamt',1,0,'C',1);
			
		}

		// Neue Zeile
		if($LetztesPEA != $rsPEI->FeldInhalt('PEAKEY'))
		{
			if($RasterZeile>180)
			{
				$RasterZeile=9999;		
				//$rsPEI->DSZurueck();
				continue;
			}
			$Ausdruck->_pdf->setXY(10,$RasterZeile-4);
			$Ausdruck->_pdf->cell(102,6,$rsPEI->FeldInhalt('PEA_BEZEICHNUNG') . ' ' . $rsPEI->FeldInhalt('FILIALE'),1,0,'L',1);
			$Ausdruck->_pdf->line(112,$RasterZeile-4,276,$RasterZeile-4);				
			$Ausdruck->_pdf->line(112,$RasterZeile+2,276,$RasterZeile+2);
			
			$RasterZeile+=6;
			$LetztesPEA = $rsPEI->FeldInhalt('PEAKEY');
		}
		
		// Auf den Monat positionieren
		$Ausdruck->_pdf->setXY(($rsPEI->FeldInhalt('MONAT')*12)+100,$RasterZeile-10);
		$Ausdruck->_pdf->cell(12,6,$rsPEI->FeldInhalt('ANZAHL'),1,0,'C',0);

		if (isset($Summen[$RasterZeile]))
		{
			$Summen[$RasterZeile] += $rsPEI->FeldInhalt('ANZAHL');
		}
		else 
		{
			$Summen[$RasterZeile] = $rsPEI->FeldInhalt('ANZAHL');
		}
		$rsPEI->DSWeiter();			
	}// Ende for
	
	foreach($Summen AS $Pos=>$Wert)
	{
		$Ausdruck->_pdf->setXY((13*12)+100,$Pos-10);
		$Ausdruck->_pdf->cell(20,6,$Wert,1,0,'R',0);
	}
	$Summen = array();		// Neue Summen


	$Ausdruck->Anzeigen();	
	
}
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}

?>	