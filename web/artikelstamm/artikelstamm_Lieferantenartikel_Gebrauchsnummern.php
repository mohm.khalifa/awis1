<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('GNR', '%');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht459 = $AWISBenutzer->HatDasRecht(459);            // Recht f�r die Lieferantenartikel
    if ($Recht459 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Bedingung = '';
    $SQL = 'SELECT Gebrauchsnummern.*, TEI_USER, TEI_USERDAT, TEI_KEY';
    $SQL .= ' FROM Gebrauchsnummern';
    $SQL .= ' INNER JOIN Teileinfos ON TEI_KEY2 = GNR_KEY and TEI_KEY1 = 0' . $AWIS_KEY2 . ' ';

    if (isset($_GET['GNR_KEY'])) {
        $Bedingung .= ' AND GNR_KEY = 0' . $DB->FeldInhaltFormat('Z', $_GET['GNR_KEY']);
    }

    $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

    if (isset($_GET['OENSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['OENSort']);
    } else {
        $SQL .= ' ORDER BY GNR_NUMMER';
    }

    $rsGNR = $DB->RecordSetOeffnen($SQL);

    if (!isset($_GET['GNR_KEY']))                    // Liste anzeigen
    {
        $Form->ZeileStart();

        if (($Recht459 & 4)) {
            $Icons = array();
            $Icons[] = array('new', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=Gebrauchsnummern&LAR_KEY=' . $AWIS_KEY2 . '&GNR_KEY=0');
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=Gebrauchsnummern';
        $Link .= '&OENSort=GNR_NUMMER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'GNR_NUMMER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GNR']['GNR_NUMMER'], 250, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=Gebrauchsnummern';
        $Link .= '&OENSort=GNR_BEMERKUNG' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'GNR_BEMERKUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GNR']['GNR_BEMERKUNG'], 500, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsGNR->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            if (($Recht459 & 2))    // �ndernrecht
            {
                $Icons[] = array(
                    'edit',
                    './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=Gebrauchsnummern&GNR_KEY=' . $rsGNR->FeldInhalt('GNR_KEY')
                );
            }
            if (($Recht459 & 4))    // L�schenrecht
            {
                $Icons[] = array(
                    'delete',
                    './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=Gebrauchsnummern&Del=' . $rsGNR->FeldInhalt('TEI_KEY') . '&Key=' . $AWIS_KEY1 . '&LAR_KEY=' . $AWIS_KEY2 . '&GNR_KEY=' . $rsGNR->FeldInhalt('GNR_KEY')
                );
            }
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));

            $Form->Erstelle_ListenFeld('*GNR_NUMMER', $rsGNR->FeldInhalt('GNR_NUMMER'), 10, 250, false, ($DS % 2), '', '', 'T', 'C');
            $Form->Erstelle_ListenFeld('*GNR_BEMERKUNG', $rsGNR->FeldInhalt('GNR_BEMERKUNG'), 10, 500, false, ($DS % 2), '', '', 'T', 'L');

            $Form->ZeileEnde();
            $DS++;

            $rsGNR->DSWeiter();
        }
        $Form->Formular_Ende();
    } else        // Einer oder keiner
    {
        $Form->Formular_Start();

        echo '<input name=txtGNR_KEY type=hidden value=0' . ($rsGNR->FeldInhalt('GNR_KEY')) . '>';

        $AWIS_KEY2 = $rsGNR->FeldInhalt('GNR_KEY');
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=Gebrauchsnummern&OENListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0?'':$rsGNR->FeldInhalt('GNR_USER')));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0?'':$rsGNR->FeldInhalt('GNR_USERDAT')));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();

        $EditModus = ($rsGNR->FeldInhalt('GNR_USER') != 'WWS'?($Recht459 & 6):false);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['GNR']['GNR_NUMMER'] . ':', 150);
        $Form->Erstelle_TextFeld('GNR_NUMMER', $rsGNR->FeldInhalt('GNR_NUMMER'), 20, 200, $EditModus);
        $AWISCursorPosition = ($EditModus?'txtGNR_NUMMER':$AWISCursorPosition);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['GNR']['GNR_BEMERKUNG'] . ':', 150);
        $Form->Erstelle_Textarea('GNR_BEMERKUNG', $rsGNR->FeldInhalt('GNR_BEMERKUNG'), 200, 100, 3, $EditModus);
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901151024");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901151025");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>