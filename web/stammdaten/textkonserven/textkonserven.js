$(document).ready(function(){

    $("#reg_Inhalt").find("input.InputText").attr('readonly','readonly');

    $(".InputText").on('dblclick',function (i) {
        if($('#txtXTX_DBLCLICK').val() == 1){
            $(this).focus();
            $(this).select();
            document.execCommand('copy');
        }else{

            if(!$("[sprache='DE']").length > 0 ){
                alert('Um die Übersetzungsfunktion zu nutzen, muss deutsch eingeblendet werden.');
                return;
            }
            console.log($(this).attr('sprache'));
            var de = $('#txt'+$(this).attr('FeldNameDE')).val();
            var win = window.open('https://www.deepl.com/translator#de/'+$(this).attr('sprache').toLowerCase()+'/'+encodeURI(de), '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Popup wurde blockiert.');
            }
        }

    });

    $('.lock').on('dblclick',function (i) {
           if($(this).attr('status') == 'locked'){
               $(this).attr('src','/bilder/cmd_schloss_offen.png');
               $(this).attr('status','unlocked');
               $("[sprache='"+$(this).attr('sprache')+"']").removeAttr('readonly');

           }else{
               $(this).attr('src','/bilder/cmd_schloss_zu.png');
               $(this).attr('status','locked');
               $("[sprache='"+$(this).attr('sprache')+"']").attr('readonly','readonly');
           }
    });
});
