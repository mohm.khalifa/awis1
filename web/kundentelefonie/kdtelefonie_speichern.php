<?php
require_once('kdtelefonie_funktionen.inc');

global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$Recht26000 = $AWISBenutzer->HatDasRecht(26000);
	//$Form->DebugAusgabe(1,$_POST);
	//$Form->DebugAusgabe(1,$_GET);
	$Frage = 0;
	$NoUpdate = false;
	$Abgeschlossen = true;
	$Sonstiges = false;
	$Kriterium = false;
	$WertNein = false;
	$EmailQM = false;
	$WertLeer = false;
	//***********************************************************************************
	//** Infos speichern ung l�schen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtKTZ_',1,1));
	$FelderKEY = explode(';',$Form->NameInArray($_POST, 'txtKTT_',1,1));
	if(count($FelderKEY)>0 AND $FelderKEY[0]!='')// AND $ATUNR!='')
	{
		//$Form->DebugAusgabe(1,$_POST[$FelderKEY[0]]);
		$SQL = '';
		$SQL .= 'UPDATE KDTELEFONIEAUFTRAGSZUORD';
		$SQL .= ' SET KTZ_KRITERIUM=\'000000\'';
		$SQL .= ', KTZ_USER=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', KTZ_USERDAT=SYSDATE';
		$SQL .= ' WHERE KTZ_KEY='.$_POST[$FelderKEY[0]];
		$DB->Ausfuehren($SQL, '', true);
	}
	if(count($Felder)>0 AND $Felder[0]!='')// AND $ATUNR!='')
	{
		//$Form->DebugAusgabe(1,$Felder[0]);
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			$SQL = '';
			$Wert = $DB->FeldInhaltFormat('T',$_POST[$Feld],true);
			if ($FeldTeile[1]=='WERT')
			{
				//$Form->DebugAusgabe(1,'Wert',$_POST[$Feld]);
				
				if ($_POST[$Feld] == '')
				{
					$WertLeer = true;
					$Abgeschlossen = false;
				}
				else if ($_POST[$Feld] == '0')
				{
					$WertLeer = false;
					$WertNein = true;
					$EmailQM = true;
					$Abgeschlossen = false;
				}
				else
				{
					//$Form->DebugAusgabe(1,'Else');
					$WertLeer = false;
					$WertNein = false;
				}
			}
			else if ($FeldTeile[1]=='KRITERIUM')
			{
				//$Form->DebugAusgabe(1,'Bemerkung',$_POST[$Feld]);
					
				if ($WertNein)
				{
					if($_POST[$Feld] == 1)
					{
						$Kriterium = true;
					}
				}
				else
				{
					$Kriterium = true;
				}
			}
			else if ($FeldTeile[1]=='BEMERKUNG')
			{
				//$Form->DebugAusgabe(1,'Bemerkung',$_POST[$Feld]);

				if ($WertNein)
				{
					if ($_POST[$Feld] <> '')
					{
						$Abgeschlossen = true;
					}
				}
			}			
			//$Form->DebugAusgabe(1,$Wert);	
				if($FeldTeile[3]>0) //Key > 0 --> aktualisieren
				{
					//$Form->DebugAusgabe(1,$_POST[$Feld],'HIER2');
					// Nur schreiben, wenn sich die Felder ge�ndert haben sollten!
					if($DB->FeldInhaltFormat('T',$_POST[$Feld],true)!=$DB->FeldInhaltFormat('T',$_POST['old'.substr($Feld,3)],true))
					{
						$SQL .= 'UPDATE KDTELEFONIEAUFTRAGSZUORD';
						if ($FeldTeile[1]=='BEMERKUNG')
						{
							$SQL .= ' SET KTZ_BEMERKUNG='.$DB->FeldInhaltFormat('T',substr($_POST[$Feld],0,3999),true);
						}
						else if ($FeldTeile[1]=='WERT')
						{
							$SQL .= ' SET KTZ_WERT='.$DB->FeldInhaltFormat('T',$_POST[$Feld],true);
							if($WertNein == false)
							{
								$Wert = '';
								$SQL .= ',KTZ_BEMERKUNG=\''.$Wert.'\'';
								$SQL .= ',KTZ_KRITERIUM=\'000000\'';
							}
						}	
						else 
						{
							//$Form->DebugAusgabe(1,$FeldTeile[3]);
							$SQL2 = 'SELECT *';
							$SQL2 .= 'FROM KDTELEFONIEAUFTRAGSZUORD';
							$SQL2 .= ' WHERE KTZ_KEY='.$FeldTeile[3];
							$rsKrit = $DB->RecordSetOeffnen($SQL2);
							
							//$Form->DebugAusgabe(1,$_POST[$Feld],'HIER2');
							if (!isset($_POST[$Feld]))
							{
								$Wert = 0;
							}
							else
							{
								$Wert = 1;
							}
							
							//$Form->DebugAusgabe(1,$FeldTeile[2],$_POST[$Feld],$rsKrit->FeldInhalt('KTZ_KRITERIUM'));
							//$Form->DebugAusgabe(1,substr_replace($rsKrit->FeldInhalt('KTZ_KRITERIUM'),$_POST[$Feld],$FeldTeile[2],0));
							if($WertNein == false)
							{
								$Wert = '';
								$SQL .= ' SET KTZ_KRITERIUM=\'000000\'';
								$SQL .= ', KTZ_BEMERKUNG=\''.$Wert.'\'';
							}
							else 
							{
								$SQL .= ' SET KTZ_KRITERIUM='.$DB->FeldInhaltFormat('T',substr_replace($rsKrit->FeldInhalt('KTZ_KRITERIUM'),$Wert,$FeldTeile[2],1),true);
							}
						}		
						//$Form->DebugAusgabe(1,$SQL);
						$SQL .= ', KTZ_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', KTZ_USERDAT=SYSDATE';
						$SQL .= ' WHERE KTZ_KEY='.$FeldTeile[3];
					}
					else
					{
						if ($FeldTeile[1]=='KRITERIUM')
						{
							$SQL .= 'UPDATE KDTELEFONIEAUFTRAGSZUORD';
							//$Form->DebugAusgabe(1,$FeldTeile[3]);
							$SQL2 = 'SELECT *';
							$SQL2 .= 'FROM KDTELEFONIEAUFTRAGSZUORD';
							$SQL2 .= ' WHERE KTZ_KEY='.$FeldTeile[3];
							$rsKrit = $DB->RecordSetOeffnen($SQL2);
					
							//$Form->DebugAusgabe(1,$FeldTeile[2],$_POST[$Feld],$rsKrit->FeldInhalt('KTZ_KRITERIUM'));
							//$Form->DebugAusgabe(1,substr_replace($rsKrit->FeldInhalt('KTZ_KRITERIUM'),$_POST[$Feld],$FeldTeile[2],0));
							//$Form->DebugAusgabe(1,$_POST[$Feld],'HIER');
							if (!isset($_POST[$Feld]))
							{
								$Wert = 0;
							}
							else
							{
								$Wert = 1;
							}
							if($WertNein == false)
							{
								$Form->DebugAusgabe(1,'Nullen');
								$Wert = '';
								$SQL .= ' SET KTZ_KRITERIUM=\'000000\'';
								$SQL .= ', KTZ_BEMERKUNG=\''.$Wert.'\'';
							}
							else 
							{
								$SQL .= ' SET KTZ_KRITERIUM='.$DB->FeldInhaltFormat('T',substr_replace($rsKrit->FeldInhalt('KTZ_KRITERIUM'),$Wert,$FeldTeile[2],1),true);
							}
							$SQL .= ', KTZ_USER=\''.$AWISBenutzer->BenutzerName().'\'';
							$SQL .= ', KTZ_USERDAT=SYSDATE';
							$SQL .= ' WHERE KTZ_KEY='.$FeldTeile[3];
						}
					}
				}
				else
				{
					$AWIS_KEY1=$_POST['txtKTA_KEY'];
				
					$SQL = 'INSERT INTO KDTELEFONIEAUFTRAGSZUORD';
					$SQL .= '(KTZ_KTA_KEY,KTZ_KTF_ID,KTZ_WERT,KTZ_USER,KTZ_USERDAT,KTZ_KRITERIUM)';
					$SQL .= ' select '.$AWIS_KEY1. ', KTF_ID , ' . $DB->FeldInhaltFormat('T',$_POST[$Feld],true);
					$SQL .= ' ,\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ' ,SYSDATE,\'000000\'';
					$SQL .= ' FROM KDTELEFONIEFRAGEN';
					$SQL .= ' WHERE KTF_ID = ' . $FeldTeile[2];
					//$Form->DebugAusgabe(1,$SQL);
				}
			$DB->Ausfuehren($SQL, '', true);
		}
	}
	$Key = $_POST['txtKTA_KEY'];
	if(!$WertNein)
	{
		$Kriterium = true;
	}
	if ($Abgeschlossen and $Kriterium)
	{
		$Status = 120;
	}
	else 
	{
		$Status = $_POST['txtKTA_STATUS'];
	}
	//$WV = $_POST['txtKTA_WV'];
	if($NoUpdate == false)
	{
		if ($Status == '')
		{
			$Status = 100;
		}
			
		if (!$Abgeschlossen and !$WertLeer)
		{
			$Status = 110;
		}	
		$SQL = 'SELECT *';
		$SQL .= 'FROM KDTELEFONIEAUFTRAG';
		$SQL .= ' INNER JOIN KDTELEFONIEBEARBEITERINFOS on KTA_KTB_ID = KTI_KTB_ID and KTI_INFORMATION =\'WEITERLEITUNG\'';
		$SQL .= ' AND kta_key ='.$Key;
		$rsQM = $DB->RecordSetOeffnen($SQL);
		if (($Status == 120) and ($rsQM->FeldInhalt('KTI_WERT') == 1) and ($EmailQM))
		{
				$TAG = new kdtelefonie_funktionen();
				$TAG->MailSenden($Key, $AWISBenutzer);
				//$Form->Hinweistext('Email an QM!');
		}
		$SQL = 'UPDATE KDTELEFONIEAUFTRAG';
		$SQL .= ' SET KTA_STATUS='.$Status;
		$SQL .= ', KTA_USER=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', KTA_USERDAT=SYSDATE';
		$SQL .= ' WHERE KTA_KEY='.$Key;
		$DB->Ausfuehren($SQL,'',true);
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>