<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

// Variablen
global $con;
global $_GET;
global $_POST;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;


$Gespeichert=False;		// F�r die Maskenauswahl
$Rechtestufe = awisBenutzerRecht($con, 1104);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		//  8=L�schen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'HilfsUndBetriebsstoffe',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//var_dump($_POST);



/**************************************************************************
* Aktuelle Zeile l�schen
**************************************************************************/

if(isset($_POST['cmdLoeschen_x']))
{
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{
		$SQL = "DELETE FROM HILFSUNDBETRIEBSSTOFFEINSAETZE WHERE HBE_KEY=0" . $_POST["txtHBE_KEY"];
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler['message']);
			awisLogoff($con);
			die();
		}
		
		awisLogoff($con);
		die('<br>Die Zuordnung wurde erfolgreich gel�scht<br><br><a href=./hibe_Main.php?cmdAktion=Einsatz&Liste=True>Weiter</a>');
	}
	elseif(empty($_POST["cmdLoeschAbbruch"]))
	{
		print "<form name=frmHBG method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtHBE_KEY value=" . $_POST["txtHBE_KEY"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie die Hilfs- und Betriebsstoffeinsatz l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}




/*****************************************************
* 
* Daten speichern
* 
*****************************************************/

if(isset($_POST['cmdSpeichern_x']))
{
	$SQL = '';
	if(isset($_POST['txtHBE_KEY']) AND $_POST['txtHBE_KEY']==0)	/****** Neu hinzuf�gen ****/
	{
		$SQL = 'INSERT INTO AWIS.HILFSUNDBETRIEBSSTOFFEINSAETZE (';
		$SQL .= 'HBE_HBS_KEY, HBE_FIL_ID, HBE_DATUMVOM, HBE_DATUMBIS, HBE_BEMERKUNG, HBE_USER, HBE_USERDAT)';
		$SQL .= ' VALUES (';

		$SQL .= "'" . $_POST["txtHBS_KEY"] . "'";
		$SQL .= ", " . $_POST["txtHBE_FIL_ID"];
		//$SQL .= ", TO_DATE(" . ($_POST["txtHBE_DATUMVOM"]==''?'SYSDATE':"'".$_POST["txtHBE_DATUMVOM"]."'") . ",'DD-MM-YYYY')";
		$SQL .= ", ". awis_FeldInhaltFormat('D',(!empty($_POST["txtHBE_DATUMVOM"])?$_POST["txtHBE_DATUMVOM"]:'DS'),false);
		//$SQL .= ", TO_DATE(" . ($_POST["txtHBE_DATUMBIS"]==''?"'31.12.2099'":"'" . $_POST["txtHBE_DATUMBIS"]. "'") . ",'DD-MM-YYYY')";
		$SQL .= ", ". awis_FeldInhaltFormat('D',(!empty($_POST["txtHBE_DATUMBIS"])?$_POST["txtHBE_DATUMBIS"]:'DE'),false);
		$SQL .= ", '" . $_POST["txtHBE_BEMERKUNG"] . "'";

		$SQL .= ", '" . $AWISBenutzer->BenutzerName() . "', SYSDATE)";
	}
	else	/****** �ndern ******/
	{
		$SQL = '';
		if($_POST["txtHBE_DATUMVOM"] != $_POST["txtHBE_DATUMVOM_OLD"])
		{
			$SQL .= ", HBE_DATUMVOM=TO_DATE('" . $_POST["txtHBE_DATUMVOM"] . "','DD-MM-YYYY')";
		}
		if($_POST["txtHBE_DATUMBIS"] != $_POST["txtHBE_DATUMBIS_OLD"])
		{
			$SQL .= ", HBE_DATUMBIS=TO_DATE('" . $_POST["txtHBE_DATUMBIS"] . "','DD-MM-YYYY')";
		}
		if($_POST["txtHBE_BEMERKUNG"] != $_POST["txtHBE_BEMERKUNG_OLD"])
		{
			$SQL .= ", HBE_BEMERKUNG='" . htmlspecialchars(str_replace("'","",stripslashes($_POST["txtHBE_BEMERKUNG"])),ENT_QUOTES) . "'";
		}

		if($SQL != '')
		{
			$SQL .= ", HBE_USER='" . $AWISBenutzer->BenutzerName() . "'";
			$SQL .= ', HBE_USERDAT=SYSDATE';
			
			$SQL = 'UPDATE AWIS.HILFSUNDBETRIEBSSTOFFEINSAETZE SET ' . substr($SQL,2);
			$SQL .= ' WHERE HBE_KEY=0' . $_POST["txtHBE_KEY"];
		}

		$InfoFelder = explode(';',awis_NameInArray($_POST, 'txtInfo_',1));

		foreach($InfoFelder as $InfoFeld=>$InfoWert)
		{
			if(strpos($InfoFelder[$InfoFeld], '_old')==FALSE)
			{
				$pos = strpos($InfoFelder[$InfoFeld],'_')+1;
				$HIT_KEY = substr($InfoFelder[$InfoFeld], $pos);
								
				if($_POST[$InfoFelder[$InfoFeld]] != $_POST[$InfoFelder[$InfoFeld] . '_old'])
				{
					$SQL = "BEGIN AENDERN_HIBE_INFOS(" . $_POST["txtHBE_KEY"] . "," . $HIT_KEY;
					$SQL .= ",'" . $_POST[$InfoFelder[$InfoFeld]];
					$SQL .= "','" . $AWISBenutzer->BenutzerName(). "');";
					$SQL .= " END;";

					$Erg = awisExecute($con, $SQL);
					if($Erg===FALSE)
					{
						awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
						awisLogoff($con);
						die();
					}
				}
			}
		}
	}

	awis_Debug(1,$SQL);
	
	if($SQL != '')
	{
		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
		$Gespeichert=True;
	}
}

/*****************************************************
* 
* Daten anzeigen
* 
*****************************************************/

echo '<form name=frmHBG method=post action=./hibe_Main.php?cmdAktion=Einsatz>';

if(isset($_POST['cmdSuche_x']))
{
	$Param[0] = (isset($_POST['txtAuswahlSpeichern'])?$_POST['txtAuswahlSpeichern']:'');
	$Param[1] = (isset($_POST['txtFIL_ID'])?$_POST['txtFIL_ID']:'');
	$Param[2] = (isset($_POST['txtHBS_SERIENNUMMER'])?$_POST['txtHBS_SERIENNUMMER']:'');
	$Param[3] = (isset($_POST['txtHER_BEZEICHNUNG'])?$_POST['txtHER_BEZEICHNUNG']:'');
	$Param[4] = (isset($_POST['txtHBT_BEZEICHNUNG'])?$_POST['txtHBT_BEZEICHNUNG']:'');
	$Param[5] = (isset($_POST['txtHistory'])?$_POST['txtHistory']:'');
	$Param[6] = (isset($_POST['txtHBS_INVENTARNUMMER'])?$_POST['txtHBS_INVENTARNUMMER']:'');
	
	awis_BenutzerParameterSpeichern($con, 'HBST_Suche', $AWISBenutzer->BenutzerName(),implode(';',$Param));

}
else
{
	$Param = explode(';',awis_BenutzerParameter($con, 'HBST_Suche', $AWISBenutzer->BenutzerName()));
}

/********* SQL TEXT bauen *************/
if(isset($_GET['HBE_KEY']))
{
	$SQL = 'SELECT * FROM HILFSUNDBETRIEBSSTOFFEINSAETZE INNER JOIN HILFSUNDBETRIEBSSTOFFE ON HBE_HBS_KEY = HBS_KEY';
	$SQL .= ' INNER JOIN HILFSUNDBETRIEBSSTOFFTYPEN ON HBS_HBT_KEY = HBT_KEY';
	$SQL .= ' INNER JOIN HERSTELLER ON HBT_HER_ID = HER_ID';
	$SQL .= ' WHERE HBE_KEY=0' . $_GET['HBE_KEY'];
}
else
{
	$SQL = 'SELECT * FROM HILFSUNDBETRIEBSSTOFFEINSAETZE INNER JOIN HILFSUNDBETRIEBSSTOFFE ON HBE_HBS_KEY = HBS_KEY';
	$SQL .= ' INNER JOIN HILFSUNDBETRIEBSSTOFFTYPEN ON HBS_HBT_KEY = HBT_KEY';
	$SQL .= ' INNER JOIN HERSTELLER ON HBT_HER_ID = HER_ID';
	$Bedingung = '';
	$FilterText = '';
	
	if($Param[1]!='')		// Filial-Nummer
	{
		$Bedingung .= " AND HBE_FIL_ID='" . $Param[1] . "'";
		$FilterText .= ', Filiale <b>' . $Param[1] . '</b>';
	}

	if($Param[2]!='')		// Serien-Nummer
	{
		$Bedingung .= " AND UPPER(HBS_SERIENNUMMER) " . awisLIKEoderIST($Param[2],true) . "";
		$FilterText .= ', Seriennummer <b>' . $Param[2] . '</b>';
	}

	if($Param[3]!='')		// Hersteller
	{
		$Bedingung .= " AND UPPER(HER_BEZEICHNUNG) " . awisLIKEoderIST($Param[3],true) . "";
		$FilterText .= ', Hersteller <b>' . $Param[3] . '</b>';
	}

	if($Param[4]!='')		// Typ
	{
		$Bedingung .= " AND UPPER(HBT_BEZEICHNUNG) " . awisLIKEoderIST($Param[4],true) . "";
		$FilterText .= ', Ger�tetyp <b>' . $Param[4] . '</b>';
	}

	if($Param[5]=='')		// Keine Historischen Daten (Default)
	{
		$Bedingung .= " AND HBE_DATUMBIS >= SYSDATE";
		$FilterText .= ', Einsatz ab <b>' . $Param[5] . '</b>';
	}

	if($Param[6]!='')		// Inventarnummer
	{
		$Bedingung .= " AND UPPER(HBS_INVENTARNUMMER) " . awisLIKEoderIST($Param[6],true) . "";
		$FilterText .= ', Inventarnummer <b>' . $Param[6] . '</b>';
	}
	
	if($Bedingung!='')
	{
		$SQL .= " WHERE " . substr($Bedingung,4);
	}
}

if(isset($_GET['Sort']) && $_GET['Sort']!='')
{
	$SQL .= ' ORDER BY ' . $_GET['Sort'];
}

awis_Debug(1,$SQL);
/****** Daten �ffnen *******/
$rsHBE = awisOpenRecordset($con, $SQL);
$rsHBEZeilen = $awisRSZeilen;

/******** Neuen Datensatz hinzuf�gen ***********/
if((isset($_GET['Hinzufuegen']) && $_GET['Hinzufuegen']=='True') OR (isset($_POST['Modus']) && $_POST['Modus']=='Neu' AND !$Gespeichert))
{
	$rsHBSZeilen=0;

			/****** Seriennummer angegeben? ********/
	if(isset($_POST['txtHBS_Seriennummer'])OR isset($_POST['txtHBS_KEY']))		/********* Zun�chst Seriennummer **********/
	{
		if(isset($_POST['txtHBS_KEY']))
		{
			$rsHBS = awisOpenRecordset($con,"SELECT * FROM HilfsUndBetriebsstoffe INNER JOIN  HILFSUNDBETRIEBSSTOFFTYPEN ON HBS_HBT_KEY = HBT_KEY INNER JOIN HERSTELLER ON HBT_HER_ID = HER_ID WHERE HBS_KEY=0" . $_POST['txtHBS_KEY'] . " ORDER BY HBS_Seriennummer");	
		}
		else
		{
			$rsHBS = awisOpenRecordset($con,"SELECT * FROM HilfsUndBetriebsstoffe INNER JOIN  HILFSUNDBETRIEBSSTOFFTYPEN ON HBS_HBT_KEY = HBT_KEY INNER JOIN HERSTELLER ON HBT_HER_ID = HER_ID WHERE HBS_Seriennummer " . awisLIKEoderIST($_POST['txtHBS_Seriennummer']) . " ORDER BY HBS_Seriennummer");
		}
		$rsHBSZeilen = $awisRSZeilen;
	}

	echo '<h3>Neuen Hilfs- und Betriebsstoff zuordnen</h3>';
	echo '<table width=100% border=1>';
	
	if($rsHBSZeilen==0)
	{
		echo '<input type=hidden name=Modus value=Neu>';

			/********** Seriennummer ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Serien-Nummer:</td>';
		echo "<td><input type=text size=30 name=txtHBS_Seriennummer value=''></td>";
		echo '</tr>';	
		if(isset($_POST['txtHBS_Seriennummer']))
		{
			echo '<tr><td colspan=2><span class=HinweisText>Die Serien-Nummer '. $_POST['txtHBS_Seriennummer'] . ' exisitiert nicht.</td></tr>';				
		}
		
		echo '</table>';
	}
	else				/********* Seriennummer eingegeben und passt *************/
	{
		echo '<input type=hidden name=Modus value=Neu>';
		echo "<td><input type=hidden name=txtHBE_KEY value=0>";

			/********** Seriennummer ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Serien-Nummer:</td>';
		if($rsHBSZeilen==1)
		{
			echo "<td><input type=hidden name=txtHBS_KEY value='" . $rsHBS['HBS_KEY'][0] . "'>";
			echo "<input type=hidden name=txtHBS_SERIENNUMMER value='" . $rsHBS['HBS_SERIENNUMMER'][0] . "'>";
			echo "" . $rsHBS['HBS_SERIENNUMMER'][0] . "</td>";
		}
		else
		{
			echo "<td><select name=txtHBS_KEY>";
			for($HBSZeile=0;$HBSZeile<$rsHBSZeilen;$HBSZeile++)
			{
				echo "<option value=" . $rsHBS['HBS_KEY'][$HBSZeile] . ">" . $rsHBS['HBS_SERIENNUMMER'][$HBSZeile] . "/" . $rsHBS['HER_BEZEICHNUNG'][$HBSZeile] . "</option>";
			}
		
			echo "</select>";
		}
		echo '</tr>';	

			/********** Filiale ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Filiale:</td>';
		echo "<td><input type=text size=10 name=txtHBE_FIL_ID value=''></td>";
		echo '</tr>';	

			/********** Einsatz vom ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Einsatz vom:</td>';
		echo "<td><input type=text size=10 name=txtHBE_DATUMVOM value=''></td>";
		echo '</tr>';	

			/********** Einsatz bis ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Einsatz bis:</td>';
		echo "<td><input type=text size=10 name=txtHBE_DATUMBIS value=''></td>";
		echo '</tr>';	
		
			/********** Bemerkung ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Bemerkung:</td>';
		echo "<td><textarea cols=40 rows=10 name=txtHBE_BEMERKUNG></textarea></td>";
		echo '</tr>';	
		
		echo '</table>';
	}

	if(($Rechtestufe&4)==4)
	{
		echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	}

}
elseif(isset($_GET['HBE_KEY']))			/******** Einen Datensatz anzeigen **********/
{

		echo '<table width=100% border=0>';
		echo '<tr><td><h3>Hilfs- und Betriebsstoff</h3></td>';
		print "<td width=30 backcolor=#000000 align=right><img border=0 src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./hibe_Main.php?cmdAktion=Einsatz&Liste=True'></td></tr>";
		echo '<tr><td colspan=2 align=right><font size=1>' . $rsHBE['HBE_USER'][0] . '<br>' . $rsHBE['HBE_USERDAT'][0] . '</font></td>';
		echo '<td width=30>&nbsp;</td></tr>';
		echo '</table>';

		echo '<table width=100% border=1>';

			/********** Seriennummer ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Serien-Nummer:</td>';
		echo "<td><input type=hidden name=txtHBS_KEY value='" . $rsHBE['HBE_HBS_KEY'][0] . "'>";
		echo "<input type=hidden name=txtHBE_KEY value='" . $rsHBE['HBE_KEY'][0] . "'>";
//		echo "" . $rsHBE['HBS_SERIENNUMMER'][0];
		echo "<a href=./hibe_Main.php?cmdAktion=HBST&HBS_KEY=" . $rsHBE['HBS_KEY'][0] . ">" . $rsHBE['HBS_SERIENNUMMER'][0] . "</a></td>";

		echo "</td>";
		echo '</tr>';	

					/********** Inventarnummer ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Inventarnummer:</td>';
		echo "<td>" . $rsHBE['HBS_INVENTARNUMMER'][0] . "</td>";
		echo '</tr>';	
					/********** Ger�t ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Ger�tebezeichnung:</td>';
		echo "<td><a href=./hibe_Main.php?cmdAktion=Typen&HBT_KEY=" . $rsHBE['HBT_KEY'][0] . ">" . $rsHBE['HBT_BEZEICHNUNG'][0] . "</a></td>";
		echo '</tr>';	

			/********** Filiale ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Filiale:</td>';
		echo "<td><a href=/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=" . $rsHBE['HBE_FIL_ID'][0] . ">" . $rsHBE['HBE_FIL_ID'][0] . "</a></td>";
		echo '</tr>';	

			/********** Einsatz vom ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Einsatz vom:</td>';
		if(($Rechtestufe&2)==2)
		{
			setlocale (LC_ALL, 'de_DE@euro', 'de_DE', 'de', 'ge');
			$DatText = $rsHBE['HBE_DATUMVOM'][0];
			echo "<td><input type=text size=10 name=txtHBE_DATUMVOM value=" . $DatText . ">";
			echo "<input type=hidden name=txtHBE_DATUMVOM_OLD value=" . $DatText . ">";
		}
		else
		{
			echo $rsHBE['HBE_DATUMVOM'][0];
		}
		echo '</td></tr>';	
			

			/********** Einsatz bis ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Einsatz bis:</td>';
		if(($Rechtestufe&2)==2)
		{
			echo "<td><input type=text size=10 name=txtHBE_DATUMBIS value=" . $rsHBE['HBE_DATUMBIS'][0] . "></td>";
			echo "<input type=hidden name=txtHBE_DATUMBIS_OLD value=" . $rsHBE['HBE_DATUMBIS'][0] . ">";
		}
		else
		{
			echo $rsHBE['HBE_DATUMBIS'][0];
		}
		echo '</td></tr>';	
		
			/********** Bemerkung ***********/	
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Bemerkung:</td>';
		if(($Rechtestufe&2)==2)
		{
			echo "<td><textarea cols=60 rows=5 name=txtHBE_BEMERKUNG>" . $rsHBE['HBE_BEMERKUNG'][0] . "</textarea></td>";
			echo "<td><input type=hidden name=txtHBE_BEMERKUNG_OLD value='" . $rsHBE['HBE_BEMERKUNG'][0] . "'></td>";
		}
		else
		{
			echo $rsHBE['HBE_BEMERKUNG'][0];
		}
		
		echo '</table>';

			/*************** Zusatzinfos anzeigen *******************/
		echo '<table border=1 width=100%>';
		echo '<tr><td id=FeldBez colspan=2>Zusatzinformationen</td></tr>';
		
		$rsInfo = awisOpenRecordset($con, "SELECT * FROM HILFSUNDBETRIEBSSTOFFINFOTYPEN WHERE HIT_HBT_KEYLISTE='" . $rsHBE['HBT_KEY'][0] . "' OR HIT_HBT_KEYLISTE = '%' ORDER BY HIT_KEY");
		$rsInfoZeilen = $awisRSZeilen;
		for($InfoZeile=0;$InfoZeile<$rsInfoZeilen;$InfoZeile++)		
		{
			echo '<tr>';
			echo '<td id=FeldBez width=300>' . $rsInfo['HIT_INFORMATION'][$InfoZeile] . '</td>';
			echo '<td>';
			$rsHIF = awisOpenRecordset($con, 'SELECT * FROM HILFSUNDBETRIEBSSTOFFINFOS WHERE HIF_HBE_KEY=' . $rsHBE['HBE_KEY'][0] . ' AND HIF_HIT_KEY=' . $rsInfo['HIT_KEY'][$InfoZeile]);
			if($awisRSZeilen>0)
			{
				$Info = $rsHIF['HIF_WERT'][0];
			}
			else
			{
				$Info = '';
			}
			echo "<input type=text name=txtInfo_" . $rsInfo['HIT_KEY'][$InfoZeile] . " size=50 value='" . $Info . "'>";
			echo "<input type=hidden name=txtInfo_" . $rsInfo['HIT_KEY'][$InfoZeile] . "_old size=50 value='" . $Info . "'>";
			echo '</td>';
			echo '</tr>';
		}
		unset($rsInfo);
		
		echo '</table>';
		
		
		if(($Rechtestufe&2)==2)
		{
			echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
		}
		if(($Rechtestufe&8)==8)
		{
			print " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen>";
		}

}
		/********************************************/
else	/******** Alle Datens�tze anzeigen **********/
		/********************************************/
{
	echo '<table width=100% border=1>';

	if($FilterText!='')	
	{
		echo '<tr><td colspan=7>Daten gefiltert nach ' . substr($FilterText,2) . '</td></tr>';
	}

	
	/******** �berschrift ******/
	echo '<tr>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HBS_INVENTARNUMMER>Inventar-Nr</a></td>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HBS_SERIENNUMMER>Serien-Nr</a></td>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HBT_BEZEICHNUNG>Ger�t</a></td>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HER_BEZEICHNUNG>Hersteller</a></td>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HBE_FIL_ID>Filiale</a></td>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HBE_DATUMVOM>Datum vom</a></td>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HBE_USER>Bearbeiter</a></td>';
	echo '<td id=FeldBez ><a href=./hibe_Main.php?cmdAktion=Einsatz&Sort=HBE_USERDAT>Stand</a></td>';
	echo '</tr>';
	
	for($HBEZeilen=0;$HBEZeilen<$rsHBEZeilen;$HBEZeilen++)
	{
		echo '<tr>';

		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsHBE['HBS_INVENTARNUMMER'][$HBEZeilen] . "</td>";

		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";
		
		echo '<a href=./hibe_Main.php?cmdAktion=Einsatz&HBE_KEY=0'. $rsHBE['HBE_KEY'][$HBEZeilen] . '>';
		echo $rsHBE['HBS_SERIENNUMMER'][$HBEZeilen] . "</td>";
		 
		 
		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsHBE['HBT_BEZEICHNUNG'][$HBEZeilen] . "</td>";
		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsHBE['HER_BEZEICHNUNG'][$HBEZeilen] . "</td>";
		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsHBE['HBE_FIL_ID'][$HBEZeilen] . "</td>";
		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsHBE['HBE_DATUMVOM'][$HBEZeilen] . "</td>";

		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><font size=2>" . $rsHBE['HBE_USER'][$HBEZeilen] . "</font></td>";
		echo '<td ' . (($HBEZeilen%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><font size=2>" . $rsHBE['HBE_USERDAT'][$HBEZeilen] . "</font></td>";

		echo '</tr>';
			
	}
	
	echo '</table>';
	echo '<hr>';
	/******** Neuen Datensatz hinzuf�gen *************/
	if(($Rechtestufe&4)==4)
	{
		echo "<a accesskey=n href=./hibe_Main.php?cmdAktion=Einsatz&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}
}		/********* Datensatz hinzuf�gen oder anzeigen **************/

echo '</form>';
?>
