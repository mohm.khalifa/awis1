<?PHP
require_once 'XML/XUL.php';
$doc = &XML_XUL::createDocument();
$win = &$doc->createElement( 'Window', array( 'id' => 'win' ) );
$doc->addRoot( $win );

$box = &$doc->createElement( 'Groupbox', array( 'orient' => 'horizontal' ) );
$win->appendChild( $box );

$bt1 = &$doc->createElement( 'Button', array( 'label' => 'Hallo XUL', 'onclick' => 'alert(\'Hallo Leser!\')' ) );
$box->appendChild( $bt1 );
$bt2 = &$doc->createElement( 'Button', array( 'label' => 'Hallo Leser', 'onclick' => 'alert(\'Hallo XUL!\');' ) );
$box->appendChild( $bt2 );

$box->setCaption( 'Meine zweite XUL-Anwendung' );

$doc->send();
?>