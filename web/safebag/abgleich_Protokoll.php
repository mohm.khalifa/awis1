<?php
require_once('abgleich_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
    
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TAG','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Wort', 'PDFErzeugen');
    $TextKonserven[]=array('Wort', 'KeineDatenVorhanden');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht23050 = $AWISBenutzer->HatDasRecht(23050);
	if($Recht23050 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
    
    $TAG = new awis_abgleich_funktionen();
    
    $AnsichtSuche = true;               // Flag, ob Such-Ansicht angezeigt wird => DEFAULT-Vorbelegung => true
    $AnsichtAnsicht = false;            // Flag, ob Detailansicht angezeigt wird => mit false vorbelegen, wird in den n�chsten Schritten gesetzt
    $ZeigePDFSchaltflaeche = false;     // Flag, ob PDF-Button angezeigt wird => immer mit false vorbelgen, wird weiter unten gesetzt
    
    $BelegNr = '';              // Merker der Belegnummer
    $BelegDatum = '';           // Merker Belegdatum
    $GesamtbetragInkasso = 0;   // Merker f�r �bergabe aus Suche-Maske-Eingabe
    $BelegAbgleich = true;
    
    if ((isset($_POST['txtTAG_BELEGNR']) or isset($_POST['sucTAG_BELEGNRTEXT']))             // wenn Belegnr mitkommt
    and isset($_POST['txtTAG_GESAMTBETRAG']))       // UND Gesamtbetrag (=GesamtbetragInkasso) mitkommt
    {
    	$GesamtbetragInkasso = str_replace(',', '.', $_POST['txtTAG_GESAMTBETRAG']);
    	
    	if (!empty($_POST['sucTAG_BELEGNRTEXT']))
    	{
    		$SuchParam['BelegNrText'] = $_POST['sucTAG_BELEGNRTEXT'];
    		$SuchParam['SPEICHERN'] = true;
    	}
    	
    	if (((($_POST['txtTAG_BELEGNR'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])      // wenn BelegNr NICHT "::Bitte W�hlen::" 
        and ($_POST['txtTAG_BELEGNR'] != 'ID'))                                                  // UND BelegNr NICHT "ID"
        or (!empty($_POST['sucTAG_BELEGNRTEXT'])))
        and (!empty($_POST['txtTAG_GESAMTBETRAG'])))                                            // UND Gesamtbetrag NICHT leer
        {
               // GesamtBetragInkasso merken und Komma in Punkt umwandeln, wgen Double-Datentyp
            
            if (!empty($_POST['sucTAG_BELEGNRTEXT']))
            {
            	$DB->SetzeBindevariable('TAG', 'var_T_BelegnrText', $_POST['sucTAG_BELEGNRTEXT'], awisDatenbank::VAR_TYP_TEXT);
            	
            	$SQL = 'select ' .
            		   'tgd.tgd_belegnr, ' .     // ganze Datenmenge zur Ansicht
            		   'tgd.tgd_belegdatum as order_datum, count(*) as order_anz ' .       // mitgezogen, um nach diesen sortieren zu k�nnen
            		   'from tuevgesellschaftendaten tgd ' .
            		   'where tgd.tgd_belegnr not in (SELECT distinct tgd.tgd_belegnr FROM tuevgesellschaftendaten tgd WHERE tgd.tgd_abgeglichen != 1) ' .
            		   'and tgd.tgd_belegnr in (select distinct tgk.tgk_belegnr from tuevgesellschaftenkopf tgk where tgd.tgd_belegdatum = tgk.tgk_belegdatum and tgd.tgd_belegnr = tgk.tgk_belegnr and tgk_bezahlt = 0) '.
            		   'and tgd.tgd_belegnr = :var_T_BelegnrText'.
            		   ' group by tgd.tgd_belegnr, tgd.tgd_belegdatum ' .
            		   'order by order_datum, order_anz';
           		
           		$rsTAG = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('TAG'));
           		
           		if($rsTAG->AnzahlDatensaetze() == 0)
           		{
           			// Hinweis, dass irgendwelche Eingaben / Auswahlen nicht gepasst haben (vgl. IF-Bedingung weiter oben)
           			$Form->Hinweistext($AWISSprachKonserven['TAG']['KeineBelegNr'].' '.$AWISSprachKonserven['TAG']['oder'].' '.$AWISSprachKonserven['TAG']['BelegNAbgeglichen'], 1);
           			$AnsichtSuche = true;          // Ansicht => nicht Suche
           			$AnsichtAnsicht = false;         // sondern Ansicht => DetailAnsicht
           			$BelegAbgleich = false;
           		}
            
            }	
           		
            if (is_numeric($GesamtbetragInkasso) and $BelegAbgleich)                                          // UND Gesamtbetrag eine Zahl
            {
            	$AnsichtSuche = false;          // Ansicht => nicht Suche
                $AnsichtAnsicht = true;         // sondern Ansicht => DetailAnsicht

                $BelegNrParams = array();       // Zwischenarray f�r POST-Var BelegNr (da hier mehrere Werte (aus Drop-Down-Feld) drinstecken)
                $BelegNrParams = explode(awis_abgleich_funktionen::_UniqueDelimiter, $_POST['txtTAG_BELEGNR']);     // in die enzelnen Werte exploden

                foreach ($BelegNrParams as $key => $value)      // alle einzelwerte durchlaufen
                {
                    switch ($key)       // $key bzw. Array-Index entspricht der Reihenfolge der Werte, siehe weiter unten, wo Drop-Down-Feld erstellt wird << 'tgd.tgd_belegnr || \'' . awis_abgleich_funktionen::_UniqueDelimiter . '\' || tgd.tgd_belegdatum, " . >>
                    {
                        case 0:     // BelegNr
                            $BelegNr = $Form->Format('T', $value);      // BelegNr-Wert merken
                        break;
                        case 1:     // BelegDatum
                            $BelegDatum = $Form->Format('D', $value);   // BelegDatum-Wert merken
                        break;
                    }
                }
                
                if (!empty($_POST['sucTAG_BELEGNRTEXT']))
                {
                    $BelegNr = $_POST['sucTAG_BELEGNRTEXT'];
                    $BelegDatum = '';
                }
                
            }
            else
            {   if(!is_numeric($GesamtbetragInkasso))
            	{
            		// Hinweis, dass irgendwelche Eingaben / Auswahlen nicht gepasst haben (vgl. IF-Bedingung weiter oben)
                	$Form->Hinweistext($AWISSprachKonserven['TAG']['KeinGesamtBetrag'], 1);
            	}
            }
         
        }    
        else
        {   // Hinweis, dass irgendwelche Eingaben / Auswahlen nicht gepasst haben (vgl. IF-Bedingung weiter oben)
            $Form->Hinweistext($AWISSprachKonserven['TAG']['KeineBelegNr'].' '.$AWISSprachKonserven['TAG']['oder'].' '.$AWISSprachKonserven['TAG']['KeinGesamtBetrag'], 1);
        }
    }
    
    if (isset($_POST['cmdDaumenhoch_x']))       // wenn Daten des Pr�fprotokolls als bezahlt markiert werden sollen
    {

            $DB->SetzeBindevariable('TGK', 'var_tgk_belegnr',$BelegNr, awisDatenbank::VAR_TYP_TEXT);
            $DB->SetzeBindevariable('TGK', 'var_tgk_belegdatum',$_POST['txtTAG_BELEGDATUM'], awisDatenbank::VAR_TYP_DATUM);
            $SQL =  'update tuevgesellschaftenkopf set ' .
                    'tgk_bezahlt = 1 ' .
                    'where tgk_belegnr = :var_tgk_belegnr '.
                    'and tgk_belegdatum = :var_tgk_belegdatum';
                    
                    
			$Form->DebugAusgabe(1, $SQL, $BelegNr);
            $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('TGK'));
            
        
        $AnsichtSuche = true;
        $AnsichtAnsicht = false;         // Pr�fprotokoll erneut anzeigen (evtl. will's ja noch jemand drucken)
    	$GesamtbetragInkasso = '';
    	$Form->ZeileStart();
    	$Form->Hinweistext('Belegnr '.$BelegNr. ' wurde als bezahlt markiert.' , 1);
    	$Form->ZeileEnde();
    }  
    
$Form->DebugAusgabe(1, 'AnsichtSuche: ' . $AnsichtSuche, 'AnsichtAnsicht: ' . $AnsichtAnsicht, 'BelegNr: ' . $BelegNr, 'BelegDatum: ' . $BelegDatum, 'Gesamtbetrag: ' . $GesamtbetragInkasso);
$Form->DebugAusgabe(1, $_REQUEST);    
   
    $Form->SchreibeHTMLCode('<form name=frmProtokoll action=./abgleich_Main.php?cmdAktion=Protokoll' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();
    
    /***************************************************************************
     * Suchansicht
     ***************************************************************************/
    if ($AnsichtSuche)
    {
        $Form->ZeileStart();
        // SQL f�r Dropdown mit UNION, 1. SQL => �berschriftenzeile; 2. SQL => Daten
        $SQL =  'select ' .
                    '\'ID\', ' .        // 1. Spalte (siehe Kommentar f�r 1. Spalte nach UNION)
                    '\' Anz.\' || \' | \' || \'Belegdatum\' || \' | \' || rpad(\'Belegnummer\', 15), ' .   // 2. alle �berschriften-Teile verkn�pft
                    'to_date(\'01.01.1970\', \'DD.MM.YYYY\') as order_datum, 0 as order_anz ' .            // Spalte 3 und 4 (hier dummys), damit gleiche Spaltenanzahl der beiden SQLs
                    'from dual ' .
                    'union ' .
                'select ' .
                    'tgd.tgd_belegnr || \'' . awis_abgleich_funktionen::_UniqueDelimiter . '\' || tgd.tgd_belegdatum, ' .            // ganze Datenmenge (weil kein KEY wegen Gruppenfunktion) => 'awis_abgleich_funktionen::_UniqueDelimiter' f�r nachtr�gliches exploden, um zur�ck auf die Daten zu kommen
                    'lpad(count(*), 4, \'0\') || \' | \' || to_char(tgd.tgd_belegdatum, \'DD.MM.YYYY\') || \' | \' || rpad(tgd.tgd_belegnr, 15), ' .     // ganze Datenmenge zur Ansicht
                    'tgd.tgd_belegdatum as order_datum, count(*) as order_anz ' .       // mitgezogen, um nach diesen sortieren zu k�nnen
                    'from tuevgesellschaftendaten tgd ' .
                    'where tgd.tgd_belegnr not in (SELECT distinct tgd.tgd_belegnr FROM tuevgesellschaftendaten tgd WHERE tgd.tgd_abgeglichen != 1) ' .
                    'and tgd.tgd_belegnr in (select distinct tgk.tgk_belegnr from tuevgesellschaftenkopf tgk where tgd.tgd_belegdatum = tgk.tgk_belegdatum and tgd.tgd_belegnr = tgk.tgk_belegnr and tgk_bezahlt = 0) '.
                    'group by tgd.tgd_belegnr, tgd.tgd_belegdatum ' .
                    'order by order_datum, order_anz';
$Form->DebugAusgabe(1, $SQL);

		$Schriftart = "font-family:'Courier New',sans-serif";
		$Form->Erstelle_TextLabel($AWISSprachKonserven['TAG']['BelegNr'] . ':', 200);
        $Form->Erstelle_SelectFeld('TAG_BELEGNR', $BelegNr, 700, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '','','','','',array(),$Schriftart);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('', 200);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['TAG']['oder'], 200, 'font-size:small;font-weight:bolder;');
        $Form->ZeileEnde();
        
        $TAG->Feld_BelegNrText($SuchParam, $AnsichtSuche);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['TAG']['Gesamtbetrag'] . ':', 389);
        $Form->Erstelle_TextFeld('TAG_GESAMTBETRAG', $GesamtbetragInkasso, 10,80, true, '', $Schriftart, '', 'T', 'L');
        $Form->Erstelle_TextLabel('&#8364',1);
        $Form->ZeileEnde();
    }
    /***************************************************************************
     * Detailansicht (=> Pr�fprotokoll zu BelegNr/BelegDatum)
     ***************************************************************************/    
    elseif ($AnsichtAnsicht)
    {
        $Form->Trennzeile('L');
        
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Protokoll accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['TAG']['ZurueckSuche']);
		$Form->InfoZeile($Felder, '');        

        // SQL mit Bedingung zusammenbauen, absetzen und in Recordset schreiben, was zur�ckgegeben wird
        $rsPEI = $TAG->ErstelleProtokollRecordset($BelegNr);
        
        $BereitsBezahlt = 0;        // Merker, ob bereits als bezahlt markiert wurde (vgl. Schaltfl�chen weiter unten)
  
        if (!$rsPEI->EOF())            // wenn �berhaupt Daten da waren 
        {
            $BereitsBezahlt = $rsPEI->FeldInhalt('TAG_BEZAHLT');       // Merker, ob bereits als bezahlt markiert wurde (vgl. Schaltfl�chen weiter unten)
            // Grunddaten werden in Maske erstellt: Belegnummer / Belegdatum / T�V-Gesellschaft
            $TAG->ErstelleProtokollGrunddaten($rsPEI);
            
            $Abgleich = array();
            $Ergebnis = array();
            // die beiden Arrays f�llen:
            // $Abgleich:
            // - 1. Dimension: mit allen TAA_Kennnugen
            // - 2. Dimension: Menge, Betrag, Verrechnung vorinitialisieren (damit assiziotive Array-Indizes verf�gbar sind)
            // $Ergebnis:
            // - Zwischensumme, AnzahlPositionen, SummePositiv, SummeNegativ vorinitialisieren (damit assiziotive Array-Indizes verf�gbar sind)
            // Array $Abgleich mit Weten aus RecordSet f�llen
            $TAG->ErzeugeProtokollAbgleichArrays($Abgleich, $Ergebnis, $rsPEI);
            
            $Breite = array();
            $Breite['Spalte1'] = 100;
            $Breite['Spalte2'] = 500;
            $Breite['Spalte3'] = 200;
            
            // �berschrift wird in Maske erstellt: Automatischer - Abgleich
            $TAG->ErstelleProtokollUeberschrift($AWISSprachKonserven['TAG']['AutoAbgleich'], $Breite);
            $TAG->ResetProtokollZwischenSumme($Ergebnis);                           // Zwischensummen-Variabel f�r diesen Block zur�cksetzen
            // Datensatz wird in Maske erstellt: Positionen durch automatischen Abgleich
            $TAG->ErstelleProtokollDS($Abgleich, '4', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['4'],'4');     // Ergebnis-Variablen aktualisieren
            $TAG->ErstelleProtokollDS($Abgleich, '11', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['11'],'11');     // Ergebnis-Variablen aktualisieren
            // Zwischensumme wird in Maske erstellt: Zwischensumme
            $TAG->ErstelleProtokollZwischenSumme($Breite, $Ergebnis['Zwischensumme']);

            // �berschrift wird in Maske erstellt: Manueller - Abgleich
            $TAG->ErstelleProtokollUeberschrift($AWISSprachKonserven['TAG']['ManuellAbgleich'], $Breite);
            $TAG->ResetProtokollZwischenSumme($Ergebnis);                           // Zwischensummen-Variabel f�r diesen Block zur�cksetzen
            // Datensatz wird in Maske erstellt:  Abgleichart / Kennzeichen falsch
            $TAG->ErstelleProtokollDS($Abgleich, '5', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['5'],'5');     // Ergebnis-Variablen aktualisieren
            // Datensatz wird in Maske erstellt: Abgleichart / Betragsdifferenz
            $TAG->ErstelleProtokollDS($Abgleich, '8', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['8'],'8');     // Ergebnis-Variablen aktualisieren
            // Datensatz wird in Maske erstellt: Abgleichart / Nachverfolgung
            $TAG->ErstelleProtokollDS($Abgleich, '6', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['6'],'6');     // Ergebnis-Variablen aktualisieren
            // Datensatz wird in Maske erstellt: Abgleichart / Sonstiges
            $TAG->ErstelleProtokollDS($Abgleich, '9', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['9'],'9');     // Ergebnis-Variablen aktualisieren
            // Datensatz wird in Maske erstellt: Abgleichart / keine Weiterberechnung
            $TAG->ErstelleProtokollDS($Abgleich, '22', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['22'],'22');     // Ergebnis-Variablen aktualisieren
            // Zwischensumme wird in Maske erstellt: Zwischensumme
            $TAG->ErstelleProtokollZwischenSumme($Breite, $Ergebnis['Zwischensumme']);
            
            // �berschrift wird in Maske erstellt: Rechnungsk�rzung
            $TAG->ErstelleProtokollUeberschrift($AWISSprachKonserven['TAG']['RechnungsKuerz'], $Breite);
            $TAG->ResetProtokollZwischenSumme($Ergebnis);                           // Zwischensummen-Variabel f�r diesen Block zur�cksetzen
            // Datensatz wird in Maske erstellt: Abgleichsart / Rechnungsk�rzung
            $TAG->ErstelleProtokollDS($Abgleich, '7', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['7'],'7');     // Ergebnis-Variablen aktualisieren
            $TAG->ErstelleProtokollDS($Abgleich, '12', $Breite);
            $TAG->AktualisiereProtokollErgebisArray($Ergebnis, $Abgleich['12'],'12');     // Ergebnis-Variablen aktualisieren
            // Zwischensumme wird in Maske erstellt: Zwischensumme
            $TAG->ErstelleProtokollZwischenSumme($Breite, $Ergebnis['Zwischensumme']);
         
            
            if ((($Ergebnis['SummePositiv'] + 1) >= $GesamtbetragInkasso)
            and (($Ergebnis['SummePositiv'] - 1) <= $GesamtbetragInkasso))
            {
            	$ZeigePDFSchaltflaeche = true;
            	$Form->Erstelle_HiddenFeld('TAG_BELEGNR', $BelegNr);        // Werte merken, f�r Schaltfl�che: Bezahlen
            	$Form->Erstelle_HiddenFeld('TAG_GESAMTBETRAG', $GesamtbetragInkasso);       // damit �bersicht erneut angezeigt werden kann
            	$Form->Erstelle_HiddenFeld('TAG_BELEGDATUM', $rsPEI->FeldInhalt('TGD_BELEGDATUM'));
            	$DivColor = '';
            }
            else
            {
            	$DivColor = 'color:red;';
            }
            
            
            // Zusammenfassung wird in der Maske erstellt
            $TAG->ErstelleProtokollZusammenfassung($Ergebnis, $GesamtbetragInkasso, $Breite,$DivColor);
            
            // nur wenn errechneter Betrag und eingegebener Betrag (von Papierinkasso) gleich sind => Flag f�r PDF-Button
        }
        else        // es wurden mit dem gefilterten SQL keine Daten gefunden
        {           // Hinweis ausgeben ...
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['Wort']['KeineDatenVorhanden'], 1);
            $Form->ZeileEnde();
        }
    }
    
	$Form->Formular_Ende();	    
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht23050&1) == 1)
    and ($AnsichtSuche))
	{	
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}
    
	if ((($Recht23050&1) == 1)
    and ($AnsichtAnsicht)
    and ($ZeigePDFSchaltflaeche))
	{
	    $LinkPDF = '/berichte/drucken.php?XRE=33&ID='.base64_encode('tgd_belegnr='.urlencode($BelegNr).'&tgd_belegdatum='.urlencode($BelegDatum).'&GesamtbetragInkasso='.urlencode($GesamtbetragInkasso));
	    $Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
	}

	if ((($Recht23050&1) == 1)
    and ($AnsichtAnsicht)
    and ($ZeigePDFSchaltflaeche)
    and ($BereitsBezahlt == 0))
	{   // Schaltfl�che, um gezeigte Daten als bezahlkt zu markieren
        $Form->Schaltflaeche('image', 'cmdDaumenhoch', '', '/bilder/cmd_daumenhoch.png', $AWISSprachKonserven['TAG']['BezahltMarkierung'], 'B');
    }
    
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>