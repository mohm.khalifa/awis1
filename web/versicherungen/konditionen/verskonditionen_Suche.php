<?php
global $VKD;
try {
    $VKD = new verskonditionen_funktionen();
    $Speichern = isset($VKD->Param['SPEICHERN']) && $VKD->Param['SPEICHERN'] == 'on' ? true : false;

    $VKD->RechteMeldung(0);//Anzeigen Recht
    echo "<form name=frmSuche method=post action=./verskonditionen_Main.php?cmdAktion=Details>";

    $VKD->Form->Formular_Start();

    $SQL = 'SELECT DISTINCT VVE.VVE_KUERZEL, VVE.VVE_KUERZEL';
    $SQL .= ' FROM VERSKONDREF VKR';
    $SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
    $SQL .= ' ON VKR.VKR_VVE_KEY = VVE.VVE_KEY';
    $SQL .= ' INNER JOIN VERSKONDITIONEN VKD';
    $SQL .= ' ON VKD.VKD_VKR_KEY = VKR.VKR_KEY';
    $SQL .= ' ORDER BY VVE.VVE_KUERZEL';

    $Anz = $VKD->DB->ErmittleZeilenAnzahl($SQL);

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VVE']['VVE_VERSICHERUNG'] . ':', 190);
    $VKD->Form->Erstelle_MehrfachSelectFeld('VERS_KONZERN', isset($_POST['txtVERS_KONZERN']) ? $_POST['txtVERS_KONZERN'] : array(), '450:355', true, $SQL, '', '', '', '', '', '', '', [], '', '', '', '', 0);
    $VKD->AWISCursorPosition = 'txtVERS_KONZERN';
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VKD']['VKD_GUELTIG_AB'] . ':', 190);
    $VKD->Form->Erstelle_TextFeld('VKD_GUELTIG_AB', '', 10, 150, true, '', '', '', 'D');
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VKD']['VKD_GUELTIG_BIS'] . ':', 190);
    $VKD->Form->Erstelle_TextFeld('VKD_GUELTIG_BIS', '', 10, 150, true, '', '', '', 'D');
    $VKD->Form->ZeileEnde();

    $VKD->Form->ZeileStart();
    $VKD->Form->Erstelle_TextLabel($VKD->AWISSprachKonserven['VKD']['VKD_SCHADENSTYP'] . ':', 190);
    $Daten = explode('|', $VKD->AWISSprachKonserven['Liste']['lst_SCHADKZ']);
    $VKD->Form->AuswahlSelectFeld('VKR_SKZ', '', '150:150', true, '', '', 'box1', $Daten, '', '', 'T', $Daten);
    $VKD->Form->ZeileEnde();

    $VKD->Form->Formular_Ende();

    $VKD->Form->SchaltflaechenStart();
    $VKD->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $VKD->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $VKD->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $VKD->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    if (($VKD->Recht9400 & 2) == 2) {
        $VKD->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $VKD->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    $VKD->Form->SchaltflaechenEnde();

    $VKD->Form->SchreibeHTMLCode('</form>');

} catch (awisException $ex) {
    if ($VKD->Form instanceof awisFormular) {
        $VKD->Form->DebugAusgabe(1, $ex->getSQL());
        $VKD->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $VKD->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($VKD->Form instanceof awisFormular) {
        $VKD->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>