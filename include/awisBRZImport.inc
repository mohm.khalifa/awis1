<?php
require_once 'awisMailer.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once 'awisFileReader.php';
class awisBRZImport
{

    /**
     * Importpfad
     */
    private $_ImpPfad = '/daten/daten/teams/AWIS-Transfer/11-Personal/04-BRZ/';

    /**
     * Importdateiname
     */
    private $_ImpDatei = 'Personal_Neu.CSV';

    /**
     * @var awisBenutzer
     */
    private $_AWISBenutzer;

    /**
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * @var awisDatenbank
     */
    private $_DBI;

    /**
     * @var awisFormular
     */
    private $_Form;

    /**
     * @var awisWerkzeuge
     */
    public $_Werkzeuge;

    /**
     * Debuglevel 99 = hoch; 1 = niedrig
     * @var int
     */
    private $_DebugLevel = 99;

    /**
     * Emailempfänger
     * @var array
     */
    public $EMAIL_Infos = array('shuttle@de.atu.eu');

    /**
     * Wieviel wurden importiert
     * @var integer
     */
    public $AnzImp;

    function __construct($Benutzer)
    {
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->debugAusgabe('Initialisiere awisBRZImport');
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();

        $this->_DBI = awisDatenbank::NeueVerbindung('AWIS_IMPORT');
        $this->_DBI->Oeffnen();

        $this->_Form = new awisFormular();
        $this->_Werkzeuge = new awisWerkzeuge();
        $this->initTestpfade();
    }

    private function initTestpfade(){
        if($this->_Werkzeuge->awisLevel() != awisWerkzeuge::AWIS_LEVEL_PRODUKTIV
            and $this->_Werkzeuge->awisLevel() != awisWerkzeuge::AWIS_LEVEL_SHUTTLE){
            $this->debugAusgabe('Ich laufe nicht im Produktivsystem');
            $this->_ImpPfad .= 'test/';
        }
    }


    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->debugAusgabe('Setze Debuglevel auf' . $Level);
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            echo date('d.m.Y');
            echo ': ' . $Typ;
            echo ': ' . $Text;
            echo PHP_EOL;
        }
    }

    /**
     * Startet de Import
     */
    public function StarteImport(){

        //Erstmal ins Importschema
        $this->_DateiInImportschema();

        //Tätigkeiten anlegen, die AWIS noch nicht kennt.
        $this->_UnbekannteTaetigkeitenAnlegen();

        //Daten in die AWIS-Personal-Tabelle
        $this->_Importschema2AWIS();

        //Kostenstellen auch noch sichern
        $this->_MergePersonalInfos(330,'PIP_KOSTENSTELLE');

    }

    /**
     * Legt Tätigkeiten an, die AWIS noch nicht kennt.
     */
    private function _UnbekannteTaetigkeitenAnlegen(){
        $SQL  ='insert into PERSONALTAETIGKEITEN (';
        $SQL .='         PET_ID,';
        $SQL .='         PET_TAETIGKEIT,';
        $SQL .='         PET_SORTIERUNG,';
        $SQL .='         PET_LAN_WWS_KENN,';
        $SQL .='         PET_USER,';
        $SQL .='         PET_USERDAT';
        $SQL .='     )';
        $SQL .='         select';
        $SQL .='             SEQ_GLOBAL_KEY.nextval,';
        $SQL .='             DATEN.*';
        $SQL .='         from';
        $SQL .='             (';
        $SQL .='                 select distinct';
        $SQL .='                     PIP_TAETIGKEIT,';
        $SQL .='                     99,';
        $SQL .='                     LAN_WWSKENN,';
        $SQL .='                     \'AWIS-BRZ-Import\',';
        $SQL .='                     SYSDATE';
        $SQL .='                 from';
        $SQL .='                     AWISIMPORT.PERSONALIMPORT';
        $SQL .='                     inner join LAENDER on LAN_AUTOKZ = PIP_LAND_LKZ';
        $SQL .='                     left join PERSONALTAETIGKEITEN on PET_TAETIGKEIT = PIP_TAETIGKEIT';
        $SQL .='                                                       and PET_LAN_WWS_KENN = LAN_WWSKENN';
        $SQL .='                 where';
        $SQL .='                     LAN_WWSKENN is not null';
        $SQL .='                     and PET_ID is null';
        $SQL .='                     and PIP_TAETIGKEIT is not null';
        $SQL .='             ) DATEN';

        $this->_DB->Ausfuehren($SQL);
    }

    /**
     * In die Personal-Tabelle importieren. Aktuell werden nur die Bereiche V und W in die Personaltabelle importiert
     */
    private function _Importschema2AWIS(){

        $this->debugAusgabe('Bringe die Daten vom Importschema ins AWIS');

        $SQL  ='merge into PERSONAL DEST';
        $SQL .=' using (';
        $SQL .='           select';
        $SQL .='               *';
        $SQL .='           from';
        $SQL .='               AWISIMPORT.PERSONALIMPORT left';
        $SQL .='               join LAENDER on LAN_AUTOKZ = PIP_LAND_LKZ';
        $SQL .='               left join PERSONALTAETIGKEITEN on PET_TAETIGKEIT = PIP_TAETIGKEIT';
        $SQL .='                                                 and PET_LAN_WWS_KENN = LAN_WWSKENN';
        $SQL .='           where';
        $SQL .='               PIP_BEREICH in (';
        $SQL .='                   \'V\',';
        $SQL .='                   \'W\'';
        $SQL .='               )';
        $SQL .='       )';
        $SQL .=' SRC on ( SRC.PIP_PERSNR_NEU = DEST.PER_NR )';
        $SQL .=' when matched then update';
        $SQL .=' set ';
        $SQL .='     PER_FIL_ID = NVL(PIP_FILIALE, 0),';
        $SQL .='     PER_ANR_ID =';
        $SQL .='     case lower(PIP_GESCHLECHT)';
        $SQL .='         when \'m\'   then';
        $SQL .='             1';
        $SQL .='         when \'w\'   then';
        $SQL .='             0';
        $SQL .='             else';
        $SQL .='             null';
        $SQL .='     end,';
        $SQL .='     PER_NACHNAME = PIP_NAME,';
        $SQL .='     PER_VORNAME = PIP_VORNAME ,';
        $SQL .='     PER_STRASSE = PIP_STRASSE,';
        $SQL .='     PER_PLZ = PIP_PLZ,';
        $SQL .='     PER_ORT = PIP_ORT,';
        $SQL .='     PER_EINTRITT = PIP_EINTRITT,';
        $SQL .='     PER_AUSTRITTGEPLANT = PIP_AUSTRITT,';
        $SQL .='     PER_AUSTRITT = PIP_AUSTRITT,';
        $SQL .='     PER_GEBURTSDATUM = PIP_GEB_DATUM,';
        $SQL .='     PER_PET_ID = PET_ID,';
        $SQL .='     PER_NRALT = PIP_PERSNR_ALT,';
        $SQL .='     PER_LAN_WWS_KENN = NVL(LAN_WWSKENN, \'?\'),';
        $SQL .='     PER_USERDAT = SYSDATE';
        $SQL .=' when not matched then';
        $SQL .=' insert (';
        $SQL .='     PER_NR,';
        $SQL .='     PER_FIL_ID,';
        $SQL .='     PER_ABRECHNUNGSKREIS,';
        $SQL .='     PER_ANR_ID,';
        $SQL .='     PER_NACHNAME,';
        $SQL .='     PER_VORNAME,';
        $SQL .='     PER_STRASSE,';
        $SQL .='     PER_PLZ,';
        $SQL .='     PER_ORT,';
        $SQL .='     PER_EINTRITT,';
        $SQL .='     PER_AUSTRITTGEPLANT,';
        $SQL .='     PER_AUSTRITT,';
        $SQL .='     PER_GEBURTSDATUM,';
        $SQL .='     PER_TELEFON,';
        $SQL .='     PER_MOBILTELEFON,';
        $SQL .='     PER_EMAIL,';
        $SQL .='     PER_PET_ID,';
        $SQL .='     PER_FIRMENPKW,';
        $SQL .='     PER_FIRMENPKWKENNZEICHEN,';
        $SQL .='     PER_FIRMENPKWTELEFON,';
        $SQL .='     PER_KON_KEY,';
        $SQL .='     PER_NRALT,';
        $SQL .='     PER_LAN_WWS_KENN,';
        $SQL .='     PER_USER,';
        $SQL .='     PER_USERDAT )';
        $SQL .=' values';
        $SQL .='     ( PIP_PERSNR_NEU,';
        $SQL .='     NVL(PIP_FILIALE, 0),';
        $SQL .='     10,';
        $SQL .='         case lower(PIP_GESCHLECHT)';
        $SQL .='             when \'m\'   then';
        $SQL .='                 1';
        $SQL .='             when \'w\'   then';
        $SQL .='                 0';
        $SQL .='             else';
        $SQL .='                 null';
        $SQL .='         end,';
        $SQL .='       PIP_NAME,';
        $SQL .='       PIP_VORNAME,';
        $SQL .='       PIP_STRASSE,';
        $SQL .='       PIP_PLZ,';
        $SQL .='       PIP_ORT,';
        $SQL .='       PIP_EINTRITT,';
        $SQL .='       PIP_AUSTRITT,';
        $SQL .='       PIP_AUSTRITT,';
        $SQL .='       PIP_GEB_DATUM,';
        $SQL .='     null,';
        $SQL .='     null,';
        $SQL .='     null,';
        $SQL .='       PET_ID,';
        $SQL .='     null,';
        $SQL .='     null,';
        $SQL .='     null,';
        $SQL .='     null,';
        $SQL .='       PIP_PERSNR_ALT,';
        $SQL .='     NVL(LAN_WWSKENN, \'?\'),';
        $SQL .='     \'AWIS-BRZ-Import\',';
        $SQL .='       SYSDATE )';

        $this->_DB->Ausfuehren($SQL,'',true);

        $this->debugAusgabe('Habe die Daten vom Importschema ins AWIS gebracht');

    }

    /**
     * Merged Werte in die Personalinfos Tabelle
     * @param $ITY_KEY
     * @param $FeldnameWert aus Importschema
     */
    private function _MergePersonalInfos($ITY_KEY, $FeldnameWert){

        $this->debugAusgabe('Merge den Infotypen ' . $ITY_KEY . ' vom Quellfeld ' . $FeldnameWert . ' in die AWIS Personalinfos');

        $SQL  ='merge into PERSONALINFOS DEST';
        $SQL .=' using (';
        $SQL .='           select';
        $SQL .='               *';
        $SQL .='           from';
        $SQL .='               AWISIMPORT.PERSONALIMPORT left';
        $SQL .='               join LAENDER on LAN_AUTOKZ = PIP_LAND_LKZ';
        $SQL .='               left join PERSONALTAETIGKEITEN on PET_TAETIGKEIT = PIP_TAETIGKEIT';
        $SQL .='                                                 and PET_LAN_WWS_KENN = LAN_WWSKENN';
        $SQL .='       )';
        $SQL .=' SRC on ( SRC.PIP_PERSNR_NEU = DEST.PIF_PER_NR';
        $SQL .='          and dest.PIF_ITY_KEY = '. $this->_DB->WertSetzen('PIF','N0',$ITY_KEY) .' )';
        $SQL .=' when matched then update';
        $SQL .=' set PIF_WERT = '.$FeldnameWert.',';
        $SQL .='     PIF_USERDAT = SYSDATE';
        $SQL .=' when not matched then';
        $SQL .=' insert (';
        $SQL .='     PIF_WERT,';
        $SQL .='     PIF_ITY_KEY,';
        $SQL .='     PIF_PER_NR,';
        $SQL .='     PIF_IMQ_ID,';
        $SQL .='     PIF_USER,';
        $SQL .='     PIF_USERDAT )';
        $SQL .=' values';
        $SQL .='     ( '.$FeldnameWert.',';
        $SQL .=  $this->_DB->WertSetzen('PIF','N0',$ITY_KEY) .',';
        $SQL .='       PIP_PERSNR_NEU,';
        $SQL .='     524288,';
        $SQL .='     \'AWIS-BRZ-Import\',';
        $SQL .='       SYSDATE )';

        $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('PIF'));

        $this->debugAusgabe('Habe den Infotypen ' . $ITY_KEY . ' vom Quellfeld ' . $FeldnameWert . ' ins AWIS gemergt.');
    }

    /**
     * @throws Exception
     */
    private function _DateiInImportschema(){
        $this->debugAusgabe('Importiere die Datei ' . $this->_ImpPfad . $this->_ImpDatei . ' ins Importschema.' );

        $TempDatei = '/tmp/' .  $this->_ImpDatei;
        if(is_file($TempDatei)){
            $this->debugAusgabe('Lösche alte Tempdatei ' . $TempDatei );
            unlink($TempDatei);
        }
        copy($this->_ImpPfad . $this->_ImpDatei, $TempDatei);
        $ImpDatei = new awisFileReader($TempDatei);


        $this->AnzImp = 0;

        $SQL = 'truncate table PERSONALIMPORT';

        $this->_DBI->Ausfuehren($SQL);

        while(!$ImpDatei->EOF()){

            $this->debugAusgabe('Verarbeite folgenden Datensatz: ' . json_encode($ImpDatei->AktuellerDS()) ,999);

            if($ImpDatei->FeldInhalt('Persnr_neu') == ''){
                $this->debugAusgabe('Überspringe Datensatz, weil keine Personalnummer ' ,999);
                $ImpDatei->DSWeiter();
                continue;
            }

            $SQL  ='insert into PERSONALIMPORT (';
            $SQL .='     PIP_ABRECHNUNGSKREIS,';
            $SQL .='     PIP_PERSNR_NEU,';
            $SQL .='     PIP_PERSNR_ALT,';
            $SQL .='     PIP_NAME,';
            $SQL .='     PIP_VORNAME,';
            $SQL .='     PIP_GEB_DATUM,';
            $SQL .='     PIP_STRASSE,';
            $SQL .='     PIP_LAND_LKZ,';
            $SQL .='     PIP_PLZ,';
            $SQL .='     PIP_ORT,';
            $SQL .='     PIP_EINTRITT,';
            $SQL .='     PIP_AUSTRITT,';
            $SQL .='     PIP_TAETIGKEIT,';
            $SQL .='     PIP_BEREICH,';
            $SQL .='     PIP_FILIALE,';
            $SQL .='     PIP_KOSTENSTELLE,';
            $SQL .='     PIP_TGL_ZT,';
            $SQL .='     PIP_WOE_ZT,';
            $SQL .='     PIP_MON_ZT,';
            $SQL .='     PIP_ZEITKONTO,';
            $SQL .='     PIP_GESCHLECHT,';
            $SQL .='     PIP_USER,';
            $SQL .='     PIP_USERDAT';
            $SQL .=' ) values (';
            $SQL .=  $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Abrechnungskreis'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Persnr_neu'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Persnr_alt'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Name'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Vorname'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Geb_Datum'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Strasse'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Land_LKZ'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('PLZ'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Ort'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Eintritt'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Austritt'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Tätigkeit'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Bereich'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Filiale'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Kostenstelle'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Tgl_Zt'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Woe_Zt'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Mon_Zt'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Zeitkonto'));
            $SQL .= ', ' . $this->_DBI->WertSetzen('PIP','T',$ImpDatei->FeldInhalt('Geschlecht'));
            $SQL .=', \'AWIS\', sysdate )';

            $this->_DBI->Ausfuehren($SQL,'',true,$this->_DBI->Bindevariablen('PIP'));

            $ImpDatei->DSWeiter();
            $this->AnzImp++;
        }
        $this->debugAusgabe('Habe folgende Anzahl an Datensätze ins AWIS geschrieben: ' . $this->AnzImp );
    }


}
