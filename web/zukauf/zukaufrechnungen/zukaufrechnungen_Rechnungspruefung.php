<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUR','%');
	$TextKonserven[]=array('ZUB','ZUB_WANRKORR');
	$TextKonserven[]=array('TITEL','tit_ZukaufRechnungen');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10005 = $AWISBenutzer->HatDasRecht(10005);
	if(($Recht10005&16)==0)
	{
	    awisEreignis(3,1000,'Zukaufrechnungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZURPruefung'));

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdWeiter_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['RECHPRUEFUNG'] = $_POST['sucZUR_RECHNUNGSNUMMER'];
		$Param['RECHPRUEFUNG_LIE_NR'] = $_POST['sucZUR_LIE_NR'];
		$Param['RECHPRUEFUNG_ZUR_DATUM_VON'] = $_POST['sucZUR_DATUM_VON'];
		$Param['RECHPRUEFUNG_ZUR_DATUM_BIS'] = $_POST['sucZUR_DATUM_BIS'];
		$Param['RECHPRUEFUNG_ZUR_TYP'] = $_POST['sucZUR_TYP'];
		$Param['SKONTO'] = $_POST['sucSKONTO'];
		$Param['RECHPRUEFUNG_ZUR_ARCHIVID'] = $_POST['sucZUR_ARCHIVID'];
    }
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufrechnungen_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZURPruefung'));
	}
	elseif(isset($_GET['RechNr']))
	{
        $Param['RECHPRUEFUNG']=$_GET['RechNr'];
        $_POST['cmdWeiter_x']=0;
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['RECHPRUEFUNG']))
		{
			$Param['RECHPRUEFUNG']='';
			$Param['RECHPRUEFUNG_LIE_NR']='';
			$Param['RECHPRUEFUNG_ZUR_DATUM_VON']='';
			$Param['RECHPRUEFUNG_ZUR_DATUM_BIS']='';
			$Param['RECHPRUEFUNG_ZUR_TYP']='';
			$Param['RECHPRUEFUNG_ZUR_ARCHIVID']='';
		}
	}

	echo '<form name=frmZukaufRFechnungen action=./zukaufrechnungen_Main.php?cmdAktion=Rechnungspruefung method=POST>';

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_LIE_NR'].':',190);
	$SQL = 'SELECT LIE_NR, LIE_NAME1';
	$SQL .= ' FROM Lieferanten';
	$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
	$SQL .= ' ORDER BY LIE_NAME1';
	$Form->Erstelle_SelectFeld('*ZUR_LIE_NR',$Param['RECHPRUEFUNG_LIE_NR'],200,true,$SQL);
	$AWISCursorPosition='sucZUR_LIE_NR';
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RECHNUNGSNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_RECHNUNGSNUMMER',$Param['RECHPRUEFUNG'],20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_DATUM'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_DATUM_VON',$Param['RECHPRUEFUNG_ZUR_DATUM_VON'],20,200,true);
	$Form->Erstelle_TextFeld('*ZUR_DATUM_BIS',$Param['RECHPRUEFUNG_ZUR_DATUM_BIS'],20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_TYP'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_TYP',$Param['RECHPRUEFUNG_ZUR_TYP'],2,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARCHIVID'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_ARCHIVID',$Param['RECHPRUEFUNG_ZUR_ARCHIVID'],20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Skonto in Prozent:',190);
	$Form->Erstelle_TextFeld('*SKONTO',$Param['SKONTO'],20,200,true);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//********************************************************
	// Daten suchen
	//********************************************************

	if(isset($_POST['cmdWeiter_x']) OR isset($_POST['cmdSpeichern_x']) OR isset($_GET['Sort']))
	{
		$SQL = 'SELECT Zukaufrechnungen.*,MWS_SATZ';
		$SQL .= ' FROM Zukaufrechnungen';
		$SQL .= ' LEFT OUTER JOIN FILIALEN ON ZUR_FIL_ID = FIL_ID';
		$SQL .= ' LEFT OUTER JOIN LIEFERANTENKUNDENNUMMERN ON LKD_FIL_ID = FIL_ID AND LKD_LIE_NR = ZUR_LIE_NR';
		$SQL .= ' LEFT OUTER JOIN MEHRWERTSTEUERSAETZE ON MWS_ID = ZUR_MWS_ID';

        $Bedingung = ' AND ZUR_LIE_NR ='.$DB->FeldInhaltFormat('T',$Param['RECHPRUEFUNG_LIE_NR']);

        if($Param['RECHPRUEFUNG']!='')
        {
            $Bedingung .= ' AND ZUR_RECHNUNGSNUMMER='.$DB->FeldInhaltFormat('T',$Param['RECHPRUEFUNG']);
        }

        if($Param['RECHPRUEFUNG_ZUR_DATUM_VON']!='')
        {
            $Bedingung .= ' AND ZUR_DATUM >='.$DB->FeldInhaltFormat('D',$Param['RECHPRUEFUNG_ZUR_DATUM_VON']);
        }
        if($Param['RECHPRUEFUNG_ZUR_DATUM_BIS']!='')
        {
            $Bedingung .= ' AND ZUR_DATUM <='.$DB->FeldInhaltFormat('D',$Param['RECHPRUEFUNG_ZUR_DATUM_BIS']);
        }
        if($Param['RECHPRUEFUNG_ZUR_TYP']!='')
        {
            $Bedingung .= ' AND ZUR_TYP ='.$DB->FeldInhaltFormat('TU',$Param['RECHPRUEFUNG_ZUR_TYP']);
        }
        if($Param['RECHPRUEFUNG_ZUR_ARCHIVID']!='')
        {
            $Bedingung .= ' AND ZUR_ARCHIVID ='.$DB->FeldInhaltFormat('TU',$Param['RECHPRUEFUNG_ZUR_ARCHIVID']);
        }

		if($Bedingung!='')
		{
			$SQL .= ' WHERE ' . substr($Bedingung,4);
		}


    	if(!isset($_GET['Sort']))
    	{
    		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
    		{
    			$ORDERBY = $Param['ORDER'];
    		}
    		else
    		{
    			$ORDERBY = 'ZUR_DATUM, LKD_KUNDENNR, ZUR_RECHNUNGSNUMMER';
    		}
    	}
    	else
    	{
    		$ORDERBY = str_replace('~',' DESC ',$_GET['Sort']);
    	}

        $Param['ORDER']=$ORDERBY;
        $SQL .= ' ORDER BY '.$ORDERBY;

        //$SQL .= ' ORDER BY ZUR_DATUM, LKD_KUNDENNR, ZUR_RECHNUNGSNUMMER';
$Form->DebugAusgabe(1,$SQL);
		$rsZUR = $DB->RecordsetOeffnen($SQL);
		$AWISBenutzer->ParameterSchreiben('Formular_ZURPruefung',serialize($Param));


		$Steuern = array();		// Alle vorkommenden Steuern
		//********************************************************
		// Daten anzeigen
		//********************************************************
		$Form->Formular_Start();
		
		if($rsZUR->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
		{
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		}
		else
		{
			$AnzFehler = 0;			// Erkannte Fehler
			$DS=0;
			$SummeWareneinsatz = 0;
			$SummeMwSt = 0;
			$SummeAltteil = 0;
			$SummeAltteilMwSt = 0;
			$FormatEndBetraege = 'N2T';			// Format f�r die Endbetraege

			$SummeProSteuer=array();
            $Werte = array();
			// Daten f�r die Rechnung berechnen
			while(!$rsZUR->EOF())
			{
                if(!isset($Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]))
                {
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['RECHNR']=$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER');
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['RECHDATUM']=$rsZUR->FeldInhalt('ZUR_DATUM');
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['TYP']=$rsZUR->FeldInhalt('ZUR_TYP');
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['WARENEINSATZ']=0;
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILWERT']=0;
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ZUSATZBETRAG']=0;

                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['WARENEINSATZSTEUER']=0;
                    
                    // Betrag, von dem die Steuer gerechnet wird + deren Steuer
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILBETRAG']=0;
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILWERTSTEUER']=0;
                    
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ENDBETRAG']=0;
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ARCHIVID']=$rsZUR->FeldInhalt('ZUR_ARCHIVID');
                    
                    $SummeProSteuer[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')] = array();
                }

                // Wenn der Wert manuell angepasst wurde, diesen verwenden
                if($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_WERTANPASSEN'),true)!='null')
                {
                    $PosWert = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_WERTANPASSEN'));
                    $ATWVKPR = ($DB->FeldInhaltFormat('N4',$PosWert)/$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;
                }
                else
                {
                    // EP/Einheiten*Menge

                	// Summe auf die einzelnen Positionen ausrechnen
                	$EinheitenProPreis = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS')==0?1:$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS')));
                    $PosWert = $DB->FeldInhaltFormat('N8',($DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_EINZELPREIS'))))*$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_MENGE'))/$EinheitenProPreis;
                    $PosWert = round($PosWert,2);
					$OrigBetrag = $DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_BETRAG'),false);
					// Nun pr�fen, ob es eine Abweichung zum Rechnungsbetrag gibt
                    $Differenz = abs($OrigBetrag)-abs($PosWert);
                    if(abs($Differenz<0.05))
                    {
                    	// diese Abweichung wird akzeptiert
                    	if($DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_MENGE'))>0)
                    	{
                            $PosWert += $Differenz;
                    	}
                    	else 
                    	{
                    	    $PosWert -= $Differenz;
                    	}
                    }
                    else
                    {
                    	//TODO: Hinweis ausgeben!
                    }

                    // Zus�tze, wie z.B. Wiedereinlagerung, Transport, Beschaffung
                    $ZusatzBetrag = (float)$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_ZUSATZBETRAG'),false);
                    $ZusatzBetrag += (float)$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_UMWELTPAUSCHALE'))*$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_UMWELTPAUSCHALEMENGE'));
                    if($Param['RECHPRUEFUNG_LIE_NR'] == '9173'){ //Bei G�hrum Zahlt man Versandkosten pauschal und nicht Mengenabhngig!
                        $ZusatzBetrag += (float)$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_LIEFERKOSTEN'));
                    }else{
                        $ZusatzBetrag += (float)$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_LIEFERKOSTEN'))*$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_MENGE'));
                    }
                    $ZusatzBetrag += (float)$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_WIEDEREINLAGERUNG'))*$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_WIEDEREINLAGERUNGMENGE'));

                    //$Form->DebugAusgabe(1,$ZusatzBetrag,$rsZUR->FeldInhalt('ZUR_ZUSATZBETRAG'));
                    // Pfad - Betrag
                    $AltteilBetrag = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ALTTEILMENGE'))*$DB->FeldInhaltFormat('N4',($rsZUR->FeldInhalt('ZUR_ATWANPASSEN')!=''?$rsZUR->FeldInhalt('ZUR_ATWANPASSEN'):$rsZUR->FeldInhalt('ZUR_ALTTEILWERT')));
                    $AltteilWert = $AltteilBetrag ;//$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ALTTEILWERT'));
                    $SteuerSatz = ((float)$rsZUR->FeldInhalt('MWS_SATZ')/100.0); 
                    
                    // Problem: Bei gemischten Rechnungen kann dies zu Problen f�hren!
                    // TROST: 0% MwSt auf Dienstleistungen => F�hrt zu Problemen
                    $GesamtSteuerSatz = ((float)$rsZUR->FeldInhalt('MWS_SATZ')/100.0);
                    $RundungStellen = 2;
                    
                    switch($Param['RECHPRUEFUNG_LIE_NR'])
                    {
                    	case '1742':		// HESS
                    						// hier gibt es Zusatzbetr�ge
                    		$ATWVKPR=0;
                    		$SteuerAusGesamtBetrag = true;			// Steuer wird auf die Summe gerechnet, nicht auf einzelne Positionen!
                    		if($rsZUR->FeldInhalt('ZUR_ALTTEILWERT')<>0)		// Wurde ein Pfandwert angegeben?
                    		{                    		
                    			// Basisbetrag f�r die zus�tzliche Altteilsteuer
                    			//$ATWVKPR = round(($PosWert)*0.10,2);
                    		}
                    		
                    		if($rsZUR->FeldInhalt('ZUR_ALTTEILWERT')<>0)		// Wurde ein Pfandwert angegeben?
                    		{                    		
                        		if($Form->PruefeDatum($rsZUR->FeldInhalt('ZUR_DATUM'),false,false,true)<strtotime('01.01.2014'))
                        		{
                        		    $ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_EKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;//*(1.0-($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_RABATT3')/100.0)));
                        		}
                        		else
                        		{
                        		    $ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;
                        		}
                    		}

                            // Zusatzbetr�ge in die Position aufnehmen                    		
                    		break;
                    	case '8252':		// WERTHENBACH
                    		$SteuerAusGesamtBetrag = false;
                    		$ATWVKPR=0;
                    		if($rsZUR->FeldInhalt('ZUR_ALTTEILMENGE')<>0)
                    		{
                    			$ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;
                    			$AltteilBetrag = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ALTTEILWERT'));
                    			$AltteilWert = $AltteilBetrag;
                    		}
                    		break;
                    	case '8164':		// W�tschner
                    		$ATWVKPR=0;
                    		$SteuerAusGesamtBetrag = false;
                    		
                    		if($rsZUR->FeldInhalt('ZUR_ALTTEILMENGE')<>0)
                    		{
	                    		$ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*0.10;
                    		}

                    		// Pfand mit 0 und dennoch ATW-Steuer geliefert
                    		if($rsZUR->FeldInhalt('ZUR_SONSTIGEBEZEICHNUNG')=='ATW ohne Pfand')
                    		{
                    		    $ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*0.10;
                    		}
                    		break;
                    	case '2620':		// Birner (AT)
                    	case '9157':		// KSM AT
                    		$SteuerAusGesamtBetrag = true;
                    		$ATWVKPR=0;
                    		break;
                    	case '9167':		// KSM CZ
                    		$SteuerAusGesamtBetrag = true;
                    		$ATWVKPR=0;
                    		$FormatEndBetraege = 'N2T';
                    		$RundungStellen = 0;
                    		break;
                    	case '0255':		// Trost (SAP)
                    		$SteuerAusGesamtBetrag = true;
                    		$ATWVKPR=0;
                    		$FormatEndBetraege = 'N2T';
                    		$RundungStellen = 2;
                    	    break;
                    	case '8219':		// KSM DE
                    		$SteuerAusGesamtBetrag = true;
                    		$ATWVKPR=0;
                    		
                    		// Als schnelle Methode: Hier 19% fest eintragen!
                    		$GesamtSteuerSatz = 0.19;
                    		
                    		if($rsZUR->FeldInhalt('ZUR_ALTTEILWERT')<>0)
                    		{
                    			//$ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;//*(1.0-($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_RABATT3')/100.0)));
                    			if($Form->PruefeDatum($rsZUR->FeldInhalt('ZUR_DATUM'),false,false,true)>strtotime('01.03.2014'))
                    			{
                    				$ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_EKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;//*(1.0-($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_RABATT3')/100.0)));
                    			}
                    			else
                    			{
                    				$ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;
                    			}

                    			// ATW Berechnung raus, ab dem 1.6.
                    			if($Form->PruefeDatum($rsZUR->FeldInhalt('ZUR_DATUM'),false,false,true)>strtotime('01.06.2015'))
                    			{
                    			    $ATWVKPR = 0;
                    			}
                    			
                    		}
                    		
                    		$AnzFehler += ($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))== $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_EKPR') AND $rsZUR->FeldInhalt('ZUR_RABATT1')==0)?1:0);
                    		break;
                        case '9173': //G�hrum
                            $SteuerAusGesamtBetrag = true;
                            if($rsZUR->FeldInhalt('ZUR_ALTTEILWERT')<>0)
                            {
                                $ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;
                            }
                            else{
                                $ATWVKPR = 0;
                            }
                            break;
                    	default:
                    		$SteuerAusGesamtBetrag = false;
                    		$ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_MENGE'))*0.10;
                    }
                }

                // Jetzt alle Werte in das Array �bertragen
                $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['WARENEINSATZ']+=$PosWert;
                $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILWERT']+=$AltteilWert;
                $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ZUSATZBETRAG']+=$ZusatzBetrag;
                
                if(!$SteuerAusGesamtBetrag)
                {
                	$WarenWertPos = $PosWert+$AltteilWert+$ZusatzBetrag;
                	$Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['WARENEINSATZSTEUER']+=round($WarenWertPos*$SteuerSatz,2);
                }

                // Summen pro MwSt
                if(!isset($SummeProSteuer[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')][$rsZUR->FeldInhalt('MWS_SATZ')]))
                {
                	$SummeProSteuer[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')][$rsZUR->FeldInhalt('MWS_SATZ')]=0;
                }
                $SummeProSteuer[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')][$rsZUR->FeldInhalt('MWS_SATZ')]+=($PosWert+$AltteilWert+$ZusatzBetrag);
//echo $rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER').'='.($PosWert+$AltteilWert+$ZusatzBetrag).'<br>';                
                
                // Altteilbetrag, d.h. Grundlage f�r die Altteilsteuer
                $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILBETRAG']+=$ATWVKPR;
                if(!$SteuerAusGesamtBetrag)
                {
                	$Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILWERTSTEUER'] += round($ATWVKPR*$SteuerSatz,2);
                }
                
				$SummeWareneinsatz += ($PosWert);
				/*
				
                // 19% Steuer auf alle einzelen Betr�ge
				$Steuer = round(($PosWert+$ZusatzBetrag+$AltteilBetrag)*(($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('MWS_SATZ'))/100)),2);

				$SummeMwSt += $Steuer;
//                $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['WARENEINSATZSTEUER']+=$Steuer;


                // In die Steuer pro MwSt als Gesamtbetrag rechnen zu k�nnen
                if(!isset($SummeProSteuer[$rsZUR->FeldInhalt('MWS_SATZ')]))
                {
                	$SummeProSteuer[$rsZUR->FeldInhalt('MWS_SATZ')]=0;
                }
                $SummeProSteuer[$rsZUR->FeldInhalt('MWS_SATZ')]+=$PosWert;

                //if($rsZUR->FeldInhalt('ZUR_ALTTEILMENGE')!=0)
               	//if($ATWVKPR!=0)
               	if($Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILBETRAG']!=0)
                {
                    if($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ATWANPASSEN'),true)!='null')
                    {
                        $ATWBetrag = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ATWANPASSEN'));
                    }
                    else
                    {
                        $ATWBetrag = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ALTTEILWERT'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ALTTEILMENGE'));
                    }

                    $SummeAltteil += $ATWBetrag;
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILWERT'] += $ATWBetrag;

                    $ATWSteuer = $ATWBetrag*(($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('MWS_SATZ'))/100));
                    // TODO: Parameter f�r die 10%

                    $AltteilMwSt = (round(($ATWVKPR*(($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('MWS_SATZ'))/100))),2));
                    $SummeAltteilMwSt += $AltteilMwSt;
echo '<br>'.$ATWVKPR.' = '.$AltteilMwSt.'<br>';      
                    $Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILWERTSTEUER']+=$AltteilMwSt;
                    //$Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILBETRAG']+=$ATWVKPR;
echo '##'.$Werte[$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')]['ALTTEILBETRAG'].'<br>';
                }*/

				$rsZUR->DSWeiter();
				$DS++;
			}

            $Form->Trennzeile();

			$Form->ZeileStart();
			$Link = './zukaufrechnungen_Main.php?cmdAktion=Rechnungspruefung';
			$Link .= '&Sort=ZUR_RECHNUNGSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_RECHNUNGSNUMMER'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift('RechNr', 150,'',$Link);
			$Link = './zukaufrechnungen_Main.php?cmdAktion=Rechnungspruefung';
			$Link .= '&Sort=ZUR_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_DATUM'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift('RechDatum', 114,'',$Link);
			$Form->Erstelle_Liste_Ueberschrift('Warenwert', 110,'text-align:right;');
			$Form->Erstelle_Liste_Ueberschrift('Zusatz', 110,'text-align:right;');
			$Form->Erstelle_Liste_Ueberschrift('Altteilwert', 110,'text-align:right;');
			$Form->Erstelle_Liste_Ueberschrift('Summe Netto', 110,'text-align:right;');
			$Form->Erstelle_Liste_Ueberschrift('Warensteuer', 110,'text-align:right;');
			$Form->Erstelle_Liste_Ueberschrift('Altteilsteuer', 110,'text-align:right;');
			$Form->Erstelle_Liste_Ueberschrift('Summe Brutto', 110,'text-align:right;');
			$Form->Erstelle_Liste_Ueberschrift('Archiv-ID', 160,'text-align:center;');
			$Form->ZeileEnde();

            $DS=0;
            $Summen = array('WARENEINSATZ'=>0, 'ALTTEILWERT'=>0, 'SUMMENETTO'=>0,'ALTTEILBETRAG'=>0,
                        'WARENEINSATZSTEUER'=>0, 'ALTTEILWERTSTEUER'=>0, 'SUMMEBRUTTO'=>0, 'ZUSATZBETRAG'=>0);

            foreach($Werte AS $RechnungsNr=>$Daten)
            {
                $Anzeige='';
                $AnzeigeBrutto='';
                foreach($SummeProSteuer[$RechnungsNr] AS $Steuer=>$Wert)
                {
					$SteuerWert = $Steuer/100.0;
                	$Anzeige .= PHP_EOL.$Form->Format('Nx',$Steuer).'%: '.$Form->Format('N2T',$Wert*(($Steuer/100.0)));
                	$AnzeigeBrutto .= PHP_EOL.$Form->Format('Nx',$Steuer).'%: '.$Form->Format('N2T',($Daten['ALTTEILWERT']+$Daten['ALTTEILWERTSTEUER']+$Wert*(1.0+($Steuer/100.0))));
                }

                $Form->ZeileStart();
                $Link = './zukaufrechnungen_Main.php?cmdAktion=Details&RechNr='.$RechnungsNr;
                $Icons[0] = array('edit',$Link,'','','_RechnungsDetails_');
                $Link = './zukaufrechnungen_Main.php?cmdAktion=Rechnungspruefung&RechNr='.$RechnungsNr;
                $Icons[1] = array('filterein',$Link,'','','_RechnungsPruefung_');
                $Form->Erstelle_ListeIcons($Icons, 40,($DS%2));
                
                $Form->Erstelle_ListenFeld('#RechNr', $RechnungsNr, 0, 130,false,($DS%2),($Daten['TYP']=='R'?'':'color:magenta;'));
                $Form->Erstelle_ListenFeld('#RechDatum', $Daten['RECHDATUM'], 0, 90,false,($DS%2),'','','D');
                $Form->Erstelle_ListenFeld('#Warenwert', $Daten['WARENEINSATZ'], 0, 110,false,($DS%2),'','','N2T','R');
                $Form->Erstelle_ListenFeld('#Zusatzbetrag', $Daten['ZUSATZBETRAG'], 0, 110,false,($DS%2),'','','N2T','R');
                $Form->Erstelle_ListenFeld('#AltteilWert', $Daten['ALTTEILWERT'], 0, 110,false,($DS%2),'','','N2T','R');
                $Form->Erstelle_ListenFeld('#SummeNetto', $Daten['WARENEINSATZ']+$Daten['ZUSATZBETRAG']+$Daten['ALTTEILWERT'], 0, 110,false,($DS%2),'','','N2T','R');
	            
	            if($SteuerAusGesamtBetrag == true)
	            {
//                	$Daten['WARENEINSATZSTEUER'] = round(($Daten['WARENEINSATZ']+$Daten['ZUSATZBETRAG']+$Daten['ALTTEILWERT'])*$GesamtSteuerSatz,2);
                	$Daten['ALTTEILWERTSTEUER'] = round(($Daten['ALTTEILBETRAG'])*($GesamtSteuerSatz),2);
                	$Daten['WARENEINSATZSTEUER'] = 0.00;
                	foreach($SummeProSteuer[$RechnungsNr] AS $Steuer=>$Betrag)
                	{
                		$Daten['WARENEINSATZSTEUER'] += round($Betrag * ($Steuer/100.0),2);
                	}
                	//$SummeProSteuer[$rsZUR->FeldInhalt('MWS_SATZ')]+=$PosWert+$AltteilWert+$ZusatzBetrag;                	
	            }
	            else 
	            {
					$Daten['WARENEINSATZSTEUER']=0.00;
	            	foreach($SummeProSteuer[$RechnungsNr] AS $Steuer=>$Betrag)
                	{
                		$Daten['WARENEINSATZSTEUER'] += round($Betrag * ($Steuer/100.0),2);
                	}
   	            }
   	            
                $Form->Erstelle_ListenFeld('#WarenwertSteuer', $Form->Format('N2T',$Daten['WARENEINSATZSTEUER']).(count($SummeProSteuer[$RechnungsNr])>1?'*':''), 0, 110,false,($DS%2),'','','T','R',$Anzeige);
                $Form->Erstelle_ListenFeld('#AltteilWertSteuer', $Daten['ALTTEILWERTSTEUER'], 0, 110,false,($DS%2),'','','N2T','R');
                $Form->Erstelle_ListenFeld('#SummeBrutto', round($Daten['WARENEINSATZ']+$Daten['ZUSATZBETRAG']+$Daten['ALTTEILWERT']+$Daten['WARENEINSATZSTEUER']+$Daten['ALTTEILWERTSTEUER'],$RundungStellen), 0, 110,false,($DS%2),'','',$FormatEndBetraege,'R',$AnzeigeBrutto);

                $Form->Erstelle_ListenFeld('ARCHIVID_'.$RechnungsNr.'_'.$DB->FeldInhaltFormat('T',$Param['RECHPRUEFUNG_LIE_NR']), $Daten['ARCHIVID'], 0, 160,(($Recht10005&64)==64),($DS%2),'','','T','C');

                $Summen['WARENEINSATZ']+=$Daten['WARENEINSATZ'];
                $Summen['ZUSATZBETRAG']+=$Daten['ZUSATZBETRAG'];
                $Summen['WARENEINSATZSTEUER']+=$Daten['WARENEINSATZSTEUER'];
                $Summen['ALTTEILWERT']+=$Daten['ALTTEILWERT'];
                $Summen['ALTTEILWERTSTEUER']+=$Daten['ALTTEILWERTSTEUER'];

                $Form->ZeileEnde();
                $DS++;
            }
            $Form->Trennzeile();

            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('#Filler', 'Summe:', 0, 268,false,($DS%2),'');
            $Form->Erstelle_ListenFeld('#Warenwert', $Summen['WARENEINSATZ'], 0, 110,false,($DS%2),'','','N2T','R');
            $Form->Erstelle_ListenFeld('#Zusatzbetrag', $Summen['ZUSATZBETRAG'], 0, 110,false,($DS%2),'','','N2T','R');
            $Form->Erstelle_ListenFeld('#AltteilWert', $Summen['ALTTEILWERT'], 0, 110,false,($DS%2),'','','N2T','R');
            $Form->Erstelle_ListenFeld('#SummeNetto', $Summen['WARENEINSATZ']+$Daten['ZUSATZBETRAG']+$Summen['ALTTEILWERT'], 0, 110,false,($DS%2),'','','N2T','R');
           	$Form->Erstelle_ListenFeld('#WarenwertSteuer', $Summen['WARENEINSATZSTEUER'], 0, 110,false,($DS%2),'','','N2T','R');
            $Form->Erstelle_ListenFeld('#AltteilWertSteuer', $Summen['ALTTEILWERTSTEUER'], 0, 110,false,($DS%2),'','','N2T','R');
            $Form->Erstelle_ListenFeld('#SummeBrutto', round($Summen['WARENEINSATZ']+$Daten['ZUSATZBETRAG']+$Summen['ALTTEILWERT']+$Summen['WARENEINSATZSTEUER']+$Summen['ALTTEILWERTSTEUER'],$RundungStellen), 0, 110,false,($DS%2),'','',$FormatEndBetraege,'R');
            $Form->ZeileEnde();
		}

		if($Param['SKONTO']>0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('#Filler', 'Summe bei Skonto:', 0, 932,false,($DS%2),'','','','R');
			$Form->Erstelle_ListenFeld('#SummeSkonto', ($Summen['WARENEINSATZ']+$Daten['ZUSATZBETRAG']+$Summen['ALTTEILWERT']+$Summen['WARENEINSATZSTEUER']+$Summen['ALTTEILWERTSTEUER'])*(1.00-($Param['SKONTO']/100.0)), 0, 110,false,($DS%2),'','','N2T','R');
			$Form->ZeileEnde();
		}
	}
	$Form->Formular_Ende();
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], '');
	$Form->Schaltflaeche('href', 'cmdExport', './zukaufrechnungen_exportrechnung.php', '/bilder/cmd_korb_rauf.png', $AWISSprachKonserven['Wort']['lbl_export'], '');

	if(($Recht10005&64)!=0)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);

	$AWISBenutzer->ParameterSchreiben("Formular_ZURPruefung",serialize($Param));

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ZUR_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['ZUR_ARTIKELNUMMER']) AND $Param['ZUR_ARTIKELNUMMER']!='')
	{
		$Bedingung .= ' AND (ZUR_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUR_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . '';
		$Bedingung .= ' OR ZLA_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUR_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['ZLH_KEY']) AND $Param['ZLH_KEY']>0)
	{
		$Bedingung .= ' AND EXISTS (SELECT * FROM ZUKAUFHERSTELLERCODES';
		$Bedingung .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLH_KEY = ZHK_ZLH_KEY';
		$Bedingung .= ' WHERE ZLH_KEY = ' . $DB->FeldInhaltFormat('N0',$Param['ZLH_KEY']);
		$Bedingung .= ' AND ZHK_CODE = ZUR_HERSTELLER';
		$Bedingung .= ' )';
	}
	if(isset($Param['ZUR_FIL_ID']) AND $Param['ZUR_FIL_ID']!='')
	{
		$Bedingung .= ' AND ZUR_FIL_ID = ' . $Param['ZUR_FIL_ID'] . ' ';
	}

	if(isset($Param['ZUR_RECHNUNGSNUMMER']) AND $Param['ZUR_RECHNUNGSNUMMER']!='')
	{
		$Bedingung .= ' AND ZUR_RECHNUNGSNUMMER = ' . $DB->FeldInhaltFormat('T',$Param['ZUR_RECHNUNGSNUMMER']) . ' ';
	}

	if(isset($Param['ZUR_LIE_NR']) AND $Param['ZUR_LIE_NR']!='')
	{
		$Bedingung .= ' AND ZUR_LIE_NR = ' . $Param['ZUR_LIE_NR'] . ' ';
	}

	if(isset($Param['ZSZ_SORTIMENT']) AND $Param['ZSZ_SORTIMENT']!='')
	{
		$Bedingung .= ' AND ZUR_SORTIMENT = ' . $DB->FeldInhaltFormat('T',$Param['ZSZ_SORTIMENT']) . ' ';
	}

	if(isset($Param['DATUMVOM']) AND $Param['DATUMVOM']!='')
	{
		$Bedingung .= ' AND ZUR_DATUM >= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMVOM']) . ' ';
	}
	if(isset($Param['DATUMBIS']) AND $Param['DATUMBIS']!='')
	{
		$Bedingung .= ' AND ZUR_DATUM <= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMBIS']) . ' ';
	}

	if(isset($Param['ZUR_EXTERNEID']) AND $Param['ZUR_EXTERNEID']!='')
	{
		$Bedingung .= ' AND ZUR_EXTERNEID ' . $DB->LIKEoderIST($Param['ZUR_EXTERNEID'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	if(isset($Param['ZUR_WANR']) AND $Param['ZUR_WANR']!='')
	{
		$Bedingung .= ' AND ZUR_WANR ' . $DB->LIKEoderIST($Param['ZUR_WANR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}

	if(isset($Param['ZUR_ZSZ_KEY']) AND $Param['ZUR_ZSZ_KEY']>0)
	{
		$Bedingung .= ' AND ZUR_ZSZ_KEY= ' . intval($Param['ZUR_ZSZ_KEY']) . ' ';
	}
	if(isset($Param['AST_ATUNR']) AND $Param['AST_ATUNR']!='')
	{
		//$Bedingung .= ' AND (ZAL_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ' AND (ZLA_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['ZUR_TAUSCHTEIL']) AND $Param['ZUR_TAUSCHTEIL']>0)
	{
		$Bedingung .= ' AND ZUR_TAUSCHTEIL = ' . intval($Param['ZUR_TAUSCHTEIL']) . ' ';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>