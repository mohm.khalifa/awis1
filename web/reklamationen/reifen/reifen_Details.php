<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $REX;

$DetailAnsicht = false;

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$AWIS_KEY1 = '';
if (isset($_GET['REX_KEY'])) {
    $AWIS_KEY1 = $REX->DB->FeldInhaltFormat('N0', $_GET['REX_KEY']);
} elseif (isset($_POST['txtREX_KEY'])) {
    $AWIS_KEY1 = $REX->DB->FeldInhaltFormat('N0', $_POST['txtREX_KEY']);
}

$REX->Param['KEY'] = $AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $REX->Param = array();
    $REX->Param['REX_AST_ATUNR'] = $_POST['sucREX_AST_ATUNR'];
    $REX->Param['REX_REKLAMATIONSDATUM_VON'] = $_POST['sucREX_REKLAMATIONSDATUM_VON'];
    $REX->Param['REX_REKLAMATIONSDATUM_BIS'] = $_POST['sucREX_REKLAMATIONSDATUM_BIS'];
    $REX->Param['REX_WERK'] = $_POST['sucREX_WERK'];

    $REX->Param['KEY'] = '';
    $REX->Param['WHERE'] = '';
    $REX->Param['ORDER'] = '';
    $REX->Param['BLOCK'] = 1;
    $REX->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
} elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) { //Irgendwas mit l�schen?
    include('./reifen_loeschen.php');
} elseif (isset($_POST['cmdSpeichern_x']) or isset($_POST['cmdDSNeu_x'])) { //Oder Speichern?
    include('./reifen_speichern.php');
    if(isset($_POST['cmdDSNeu_x'])){ //User machen hier massenanlagen und wollen mit DSNeu auch speichern, danach m�ssen die Posts gelert werden, damit die Maske wieder leer ist.
        $AWIS_KEY1 = -1;
        $_POST = array();
    }
} elseif (isset($_POST['cmdFreigabe_x'])) { //Datensatz als ungedruckt markieren
    include('./reifen_freigabe.php');
}
else { //User hat den Reiter gewechselt.
    $AWIS_KEY1 = $REX->Param['KEY'];
}


//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************
if (!isset($_GET['Sort'])) {
    if (isset($REX->Param['ORDER']) AND $REX->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $REX->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY REX_REKLAMATIONSDATUM DESC';
        $REX->Param['ORDER'] = ' REX_REKLAMATIONSDATUM DESC ';
    }
} else {
    $REX->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $REX->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $REX->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************
$SQL = 'SELECT ';
$SQL .= '   REX_KEY,';
$SQL .= '   REX_REKLAMATIONSDATUM,';
$SQL .= '   REX_WERK,';
$SQL .= '   REX_AST_ATUNR,';
$SQL .= '   REX_HERSTELLER,';
$SQL .= '   REX_P,';
$SQL .= '   REX_EM,';
$SQL .= '   REX_RA,';
$SQL .= '   REX_LAR_NR,';
$SQL .= '   REX_LAN_CODE,';
$SQL .= '   REX_PROFILTIEFE,';
$SQL .= '   REX_STATUS,';
$SQL .= '   REX_USER,';
$SQL .= '   REX_USERDAT';
$SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
$SQL .= ' FROM REIFENREKLAMATIONEN ';

if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($AWIS_KEY1 == '') { //Liste?
    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $REX->Form->Format('N0', $_REQUEST['Block'], false);
        $REX->Param['BLOCK'] = $Block;
    } elseif (isset($REX->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $REX->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $REX->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $REX->DB->ErmittleZeilenAnzahl($SQL, $REX->DB->Bindevariablen('REX', false));
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $REX->DB->WertSetzen('REX', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $REX->DB->WertSetzen('REX', 'N0', ($StartZeile + $ZeilenProSeite));
}

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsREX = $REX->DB->RecordsetOeffnen($SQL, $REX->DB->Bindevariablen('REX', true));
$REX->Form->DebugAusgabe(1, $REX->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$REX->Form->Formular_Start();
echo '<form name=frmreifen action=./reifen_Main.php?cmdAktion=Details method=POST>';

if ($rsREX->EOF() and $AWIS_KEY1 == '') { //Nichts gefunden!
    $REX->Form->Hinweistext($REX->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsREX->AnzahlDatensaetze() > 1) {// Liste

    $FeldBreiten = array();
    $FeldBreiten['REX_REKLAMATIONSDATUM'] = 150;
    $FeldBreiten['REX_AST_ATUNR'] = 150;
    $FeldBreiten['REX_EM'] = 150;
    $FeldBreiten['REX_HERSTELLER'] = 150;
    $FeldBreiten['REX_STATUS'] = 50;

    $REX->Form->ZeileStart();
    $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=REX_REKLAMATIONSDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_REKLAMATIONSDATUM'))?'~':'');
    $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_REKLAMATIONSDATUM'], $FeldBreiten['REX_REKLAMATIONSDATUM'], '', $Link);
    $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=REX_AST_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_AST_ATUNR'))?'~':'');
    $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_AST_ATUNR'], $FeldBreiten['REX_AST_ATUNR'], '', $Link);
    $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=REX_EM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_EM'))?'~':'');
    $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_EM'], $FeldBreiten['REX_EM'], '', $Link);
    $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=REX_HERSTELLER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_HERSTELLER'))?'~':'');
    $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_HERSTELLER'], $FeldBreiten['REX_HERSTELLER'], '', $Link);
    $Link = './reifen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=REX_STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REX_STATUS'))?'~':'');
    $REX->Form->Erstelle_Liste_Ueberschrift($REX->AWISSprachKonserven['REX']['REX_STATUS'], $FeldBreiten['REX_STATUS'], '', $Link);
    $REX->Form->ZeileEnde();

    $DS = 0;
    while (!$rsREX->EOF()) {
        $HG = ($DS % 2);
        $REX->Form->ZeileStart();
        $Link = './reifen_Main.php?cmdAktion=Details&REX_KEY=0' . $rsREX->FeldInhalt('REX_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $REX->Form->Erstelle_ListenFeld('REX_REKLAMATIONSDATUM', $rsREX->FeldInhalt('REX_REKLAMATIONSDATUM'), 0, $FeldBreiten['REX_REKLAMATIONSDATUM'], false, $HG, '', $Link, 'D');
        $REX->Form->Erstelle_ListenFeld('REX_AST_ATUNR', $rsREX->FeldInhalt('REX_AST_ATUNR'), 0, $FeldBreiten['REX_AST_ATUNR'], false, $HG, '', '', 'T', 'L');
        $REX->Form->Erstelle_ListenFeld('REX_EM', $rsREX->FeldInhalt('REX_EM'), 0, $FeldBreiten['REX_EM'], false, $HG, '', '', 'T', 'L');
        $REX->Form->Erstelle_ListenFeld('REX_HERSTELLER', $rsREX->FeldInhalt('REX_HERSTELLER'), 0, $FeldBreiten['REX_HERSTELLER'], false, $HG, '', '', 'T', 'L');
        if ($rsREX->FeldInhalt('REX_STATUS') == 'G') {
            $Icons = array("flagge_gruen", '', "", $REX->AWISSprachKonserven['REX']['REX_STATUS_G']);
        } else {
            $Icons = array("flagge_rot", '', "", $REX->AWISSprachKonserven['REX']['REX_STATUS_ungleich_G']);
        }

        $REX->Form->Erstelle_ListeIcons(array($Icons), $FeldBreiten['REX_STATUS'], $HG, '', 'align: center');
        $REX->Form->ZeileEnde();

        $rsREX->DSWeiter();
        $DS++;
    }

    $Link = './reifen_Main.php?cmdAktion=Details';
    $REX->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
} else { //Ein Datensatz
    $DetailAnsicht = true;

    $REX->Form->Erstelle_HiddenFeld('REX_KEY', $AWIS_KEY1);
    $REX->Param['KEY'] = $AWIS_KEY1;

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./reifen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $REX->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsREX->FeldInhalt('REX_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsREX->FeldInhalt('REX_USERDAT'));
    $REX->Form->InfoZeile($Felder, '');

    $EditRecht = (($REX->Recht52000 & 2) != 0);

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_REKLAMATIONSDATUM'] . ':', 170);
    $REX->Form->Erstelle_TextFeld('!REX_REKLAMATIONSDATUM', $rsREX->FeldInhalt('REX_REKLAMATIONSDATUM'), 12, 150, $EditRecht, '', '', '', 'D', '', '', date('d.m.Y'));
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_WERK'] . ':', 170);
    $REX->Form->Erstelle_TextFeld('!REX_WERK', $rsREX->FeldInhalt('REX_WERK'),12, 150, $EditRecht, '', '', '', 'T');
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_AST_ATUNR'] . ':', 170);
    $REX->Form->AuswahlBox('!REX_AST_ATUNR', 'box1', '', 'REX_AST_ATUNR', '', 150, 12, $rsREX->FeldInhalt('REX_AST_ATUNR'), 'T', $EditRecht, '', '', '', '', '', '', '', 6);
    $REX->Form->ZeileEnde();
    ob_start();
    $REX->erstelleATUNrInfos($rsREX->FeldInhalt('REX_AST_ATUNR'), $rsREX->FeldInhalt('REX_HERSTELLER'),$rsREX->FeldInhalt('REX_LAR_NR'));
    $REX->Form->AuswahlBoxHuelle('box1', '', '', ob_get_clean());

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_P'] . ':', 170);
    $REX->Form->Erstelle_TextFeld('!REX_P', $rsREX->FeldInhalt('REX_P'),12, 150, $EditRecht, '', '', '', 'T', '', '');
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_EM'] . ':', 170);
    $REX->Form->Erstelle_TextFeld('!REX_EM', $rsREX->FeldInhalt('REX_EM'), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_RA'] . ':', 170);
    $REX->Form->Erstelle_TextFeld('!REX_RA', $rsREX->FeldInhalt('REX_RA'), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_LAN_CODE'] . ':', 170);
    $SQL = 'select LAN_CODE,';
    $SQL .= '   LAN_LAND';
    $SQL .= ' from LAENDER';
    $SQL .= ' where LAN_CODE in';
    $SQL .= '   (select LAN_CODE from V_FILIALEN_AKTUELL';
    $SQL .= '   )';
    $SQL .= ' order by LAN_LAND';
    $REX->Form->Erstelle_SelectFeld('!REX_LAN_CODE', $rsREX->FeldInhalt('REX_LAN_CODE'), '120:150', $EditRecht, $SQL, '', 'DE');
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_PROFILTIEFE'] . ':', 170);
    $REX->Form->Erstelle_TextFeld('!REX_PROFILTIEFE', $rsREX->FeldInhalt('REX_PROFILTIEFE'), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $REX->Form->ZeileEnde();
}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$REX->Form->Formular_Ende();
$REX->Form->SchaltflaechenStart();

$REX->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $REX->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

if (($REX->Recht52000 & 6) != 0 AND $DetailAnsicht) {
    $REX->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $REX->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    if($rsREX->FeldInhalt('REX_STATUS') == 'G'){ //Bereits gedruckte, k�nnen erneut in die Warteschlange geschrieben werden
        $REX->Form->Schaltflaeche('image', 'cmdFreigabe', '', '/bilder/cmd_stern.png', $REX->AWISSprachKonserven['REX']['REX_TTT_ZURWARTESCHLANGE'], 'E');
    }
}

if (($REX->Recht52000 & 4) == 4) {       // Hinzuf�gen erlaubt?
    $REX->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $REX->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}
if (($REX->Recht52000 & 8) == 8 AND $AWIS_KEY1 > 0) {
    $REX->Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $REX->AWISSprachKonserven['Wort']['lbl_loeschen'], '','',27,27,'',true);
}

$REX->Form->SchaltflaechenEnde();

$REX->Form->SchreibeHTMLCode('</form>');

?>