#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Zukauf-Rueckgaben
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
1.00.01;Erste Version;10.01.2012;<a href=mailto:Sacha.Kerres@de.atu.eu>Sacha Kerres</a>

