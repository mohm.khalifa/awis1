<?php
/**
 * �bersicht �ber den Import, etc anzeigen
 *
 * @author    Sacha Kerres
 * @copyright ATU
 * @version   20091008
 *
 *
 */
require_once('awisFilialen.inc');

global $AWISCursorPosition;
global $SRFunktionen;

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $AWISBenutzer = awisBenutzer::Init();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('WTE', '%');
    $TextKonserven[] = array('WPO', '%');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'FehlerNichtGefunden');
    $TextKonserven[] = array('Filialinfo', 'fit_lbl_852');
    $TextKonserven[] = array('Filialinfo', 'fit_lbl_853');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht11004 = $AWISBenutzer->HatDasRecht(11004);        // Rechte des Mitarbeiters
    if ($Recht11004 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Recht11000 = $AWISBenutzer->HatDasRecht(11000);
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
    $UserFilialen = $SRFunktionen->FilialenDesUsers();


    if (isset($_POST['cmdSpeichern_x'])) {
        include './srpep_speichern.php';
    }

    //**************************************************************
    // Zuordnung l�schen
    //**************************************************************
    if (isset($_GET['DEL'])) {
        $SQL = 'SELECT WPO_KEY';
        $SQL .= ' FROM WerkstattterminePositionen';
        $SQL .= ' INNER JOIN WERKSTATTTERMINEPLANUNGEN ON WPO_KEY = WTP_WPO_KEY';
        $SQL .= ' WHERE WTP_KEY = ' . $DB->FeldInhaltFormat('N0', $_GET['DEL']);
        $rsWPO = $DB->RecordSetOeffnen($SQL);
        if (!$rsWPO->EOF()) {
            $DB->TransaktionBegin();
            $SQL = 'UPDATE WerkstattterminePositionen';
            $SQL .= ' SET WPO_STATUS = 0';
            $SQL .= ' WHERE WPO_KEY = ' . $rsWPO->FeldInhalt('WPO_KEY');
            $SQL .= ' AND (SELECT COUNT(*) FROM WerkstattTerminePlanungen ';
            $SQL .= ' 		WHERE WTP_STATUS < 8 AND WTP_WPO_KEY = ' . $rsWPO->FeldInhalt('WPO_KEY') . ') <= 1';
            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 1007061849, $SQL, 2);
            }

            $SQL = 'DELETE FROM WERKSTATTTERMINEPLANUNGEN';
            $SQL .= ' WHERE WTP_KEY = ' . $DB->FeldInhaltFormat('N0', $_GET['DEL']);
            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Speichern', 1007061848, $SQL, 2);
            }
            $DB->TransaktionCommit();
        }
    }

    if (isset($_GET['FIL_ID'])) {
        $Param['FIL_ID'] = $_GET['FIL_ID'];
        $Param['TAG'] = $_GET['TAG'];
    } elseif (!isset($_POST['txtPLAN_FIL_ID'])) {
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_WTE'));
    } else {
        $Param['FIL_ID'] = $_POST['txtPLAN_FIL_ID'];
        $Param['TAG'] = $_POST['txtPLAN_TAG'];
    }

    // Ermitteln, welche Gebiete man sehen darf

    $GebieteListe = ',0';
    if (($Recht11000 & 2) != 2) {
        $SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
        $SQL .= ' FROM Filialebenen';
        $SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
        $SQL .= ' AND EXISTS';
        $SQL .= '  (select DISTINCT XX1_FEB_KEY';
        $SQL .= '  FROM V_FILIALEBENENROLLEN';
        $SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
        $SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate)';
        $SQL .= '  WHERE XX1_KON_KEY = ' . $AWISBenutzer->BenutzerKontaktKEY();
        $SQL .= '  AND  XX1_FEB_KEY = FEB_KEY';
        $SQL .= ')';
        $SQL .= ' ORDER BY 2';
        $rsFEB = $DB->RecordSetOeffnen($SQL);
        $GebieteListe = '';
        while (!$rsFEB->EOF()) {
            $GebieteListe .= ',' . $rsFEB->FeldInhalt('FEB_KEY');
            $rsFEB->DSWeiter();
        }
        unset($rsFEB);
        if ($GebieteListe == '') {
            $GebieteListe = ',0';
        }
    }

    // Gebiet f�r die aktuelle Filiale ermitteln (f�r die Ausleihe)
    $SQL = 'SELECT FEB_KEY, FIL_LAN_WWSKENN';
    $SQL .= ' FROM Filialebenen';
    $SQL .= '  INNER JOIN FilialebenenZuordnungen ON FEZ_FEB_KEY = FEB_KEY AND FEZ_FIL_ID = ' . $Param['FIL_ID'] . ' AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
    $SQL .= '  INNER JOIN Filialen ON FEZ_FIL_ID = FIL_ID';
    $Form->DebugAusgabe(1, $SQL);
    $rsFEB = $DB->RecordSetOeffnen($SQL);
    $FEB_KEY = $rsFEB->FeldInhalt('FEB_KEY');
    $FIL_LAN_WWSKENN = $rsFEB->FeldInhalt('FIL_LAN_WWSKENN');

    $AWISBenutzer->ParameterSchreiben('Formular_WTE', serialize($Param));
    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./srpep_Main.php?cmdAktion=Planung>");

    $Form->Formular_Start();

    //* Auswahlparameter
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['WTE']['txtPLAN_TAG'] . ':', 190);
    $Form->Erstelle_TextFeld('PLAN_TAG', (isset($Param['TAG']) ? $Param['TAG'] : date('d.m.Y')), 10, 150, true, '', '', '', 'D');
    $AWISCursorPosition = 'txtPLAN_TAG';
    $Form->Erstelle_TextLabel($AWISSprachKonserven['WTE']['WTE_FIL_ID'] . ':', 190);
    if(count($UserFilialen)>0){
        sort($UserFilialen);
        foreach ($UserFilialen as $Fil){
            $SelectFilialen[] = $Fil .'~' . $Fil;
        }

        $Form->Erstelle_SelectFeld('PLAN_FIL_ID',(isset($Param['FIL_ID']) ? $Param['FIL_ID'] : ''),150,true,'','','','','',$SelectFilialen);
    }else{
        $Form->Erstelle_TextFeld('PLAN_FIL_ID', (isset($Param['FIL_ID']) ? $Param['FIL_ID'] : ''), 5, 150, true, '', '', '', 'N0');
    }

    $Form->ZeileEnde();
    if (isset($Param['FIL_ID'])) {

        $Form->ZeileStart();
        $FILObj = new awisFilialen($Param['FIL_ID']);
        $Tage = $FILObj->NaechsteLacktage($Param['FIL_ID'], 2);
        if (!empty($Tage)) {
            $AnzeigeTage = '';
            foreach ($Tage as $Tag) {
                $AnzeigeTage .= ', ' . date('d.m.Y', $Tag);
            }

            $Form->Erstelle_TextLabel($AWISSprachKonserven['Filialinfo']['fit_lbl_852'] . ': ', 190);
            $Form->Erstelle_TextLabel(substr($AnzeigeTage, 1), 600);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['Filialinfo']['fit_lbl_853'] . ': ', 190);
            $Form->Erstelle_TextLabel($FILObj->FilialInfo('853'), 600);
            $Form->ZeileEnde();
        }
    }
    //*************************************************************************
    // Zeile mit der Zusammenfassung
    //*************************************************************************
    if (isset($Param['FIL_ID']) AND $Param['FIL_ID'] != '') {
        $SQL = 'SELECT WTE_FIL_ID, COUNT(*) AS ANZ, MIN(WTE_KUNDENFZVON) AS WTE_KUNDENFZVON, MAX(WTE_KUNDENFZBIS) AS WTE_KUNDENFZBIS';
        //$SQL .= ', SUM((SELECT SUM(WPO_ZEITMIN) FROM WerkstattTerminePositionen WHERE WPO_WTE_KEY = WTE_KEY)) AS WPO_ZEITMIN';
        $SQL .= ', MIN(WPO_TERMINVON) as WPO_TERMINVON, MAX(WPO_TERMINBIS) AS WPO_TERMINBIS';
        $SQL .= ', SUM(WPO_ZEITMIN) AS WPO_ZEITMIN';        // Zeit in Minuten als Summe
        $SQL .= ' FROM WERKSTATTTERMINE';
        if (($Recht11000 & 2) != 2) {
            $SQL .= ' INNER JOIN FILIALEBENENZUORDNUNGEN ON WTE_FIL_ID = FEZ_FIL_ID AND FEZ_FEB_KEY IN (' . substr($GebieteListe, 1) . ') AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
        }
        $SQL .= ' INNER JOIN WerkstattTerminePositionen ON WPO_WTE_KEY = WTE_KEY';
        $SQL .= ' WHERE WTE_STATUS < 8';      // Neue oder ge�nderte
        $SQL .= ' AND WPO_IMPORTSTATUS <> \'X\'';        // TODO: Nur tempor�r, die X sollen beim Import raus!
        $SQL .= ' AND (TRUNC(WPO_TERMINVON)=' . $DB->FeldInhaltFormat('D', $Param['TAG'], false);
        $SQL .= ' OR TRUNC(WPO_TERMINBIS)=' . $DB->FeldInhaltFormat('D', $Param['TAG'], false) . ')';
        $SQL .= ' AND WTE_FIL_ID =' . $DB->FeldInhaltFormat('N0', $Param['FIL_ID'], false);
        $SQL .= ' GROUP BY WTE_FIL_ID';
        $SQL .= ' ORDER BY WTE_FIL_ID';

        $rsWTE = $DB->RecordSetOeffnen($SQL);
        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['WTE_FIL_ID'], 150);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['txt_AnzahlOffen'], 150);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_TERMINVON'], 200);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_TERMINBIS'], 200);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPOZEITSTD'], 100);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsWTE->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('#FIL', $rsWTE->FeldInhalt('WTE_FIL_ID'), 10, 150, false, ($DS % 2), '', '', 'N0');
            $Form->Erstelle_ListenFeld('#ANZ', $rsWTE->FeldInhalt('ANZ'), 10, 150, false, ($DS % 2), '', '', 'N0');
            $Form->Erstelle_ListenFeld('#WPO_TERMINVON', $rsWTE->FeldInhalt('WPO_TERMINVON'), 10, 200, false, ($DS % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('#WPO_TERMINBIS', $rsWTE->FeldInhalt('WPO_TERMINBIS'), 10, 200, false, ($DS % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('#ZEITGESAMT', $Form->Format('N1', ($rsWTE->FeldInhalt('WPO_ZEITMIN') / 60)), 10, 100, false, ($DS % 2), '', '', 'T', 'Z');
            $Form->ZeileEnde();

            $rsWTE->DSWeiter();
            $DS++;
        }

        //Pr�fen, ob Mitarbeiter zu der ausgew�hlten Filiale GBL ist --> Wenn ja, dann Speichernbutton anzeigen
        $Speichern = false;
        $SQL = 'SELECT FILIALROLLE(' . $Param['FIL_ID'] . ',25,SYSDATE, \'KEY\') as KONKEY FROM DUAL';
        $rsFilialRolle = $DB->RecordSetOeffnen($SQL);

        if ($DB->FeldInhaltFormat('Z', $AWISBenutzer->BenutzerKontaktKEY()) == $DB->FeldInhaltFormat('Z', $rsFilialRolle->FeldInhalt('KONKEY'))) {
            $Speichern = true;
        }

        //**************************************
        // Daten anzeigen
        //**************************************

        $SQL = 'SELECT DISTINCT WTE_KEY, WTE_WANR, WTE_VORGANGSART, WTE_STATUS';
        $SQL .= ', WPO_ZEITMIN, WPO_KEY, WPO_VK, WPO_STATUS, WPO_AST_ATUNR, WPO_BEZEICHNUNG';
        $SQL .= ', WTE_KUNDENFZVON, WTE_KUNDENFZBIS';
        $SQL .= ', WPO_TERMINVON, WPO_TERMINBIS';
        $SQL .= ' FROM WerkstattTermine';
        $SQL .= ' INNER JOIN WerkstattTerminePositionen ON WTE_KEY = WPO_WTE_KEY';
        if (($Recht11000 & 2) != 2) {
            $SQL .= ' INNER JOIN FILIALEBENENZUORDNUNGEN ON WTE_FIL_ID = FEZ_FIL_ID AND FEZ_FEB_KEY IN (' . substr($GebieteListe, 1) . ')';
        }
        $SQL .= ' WHERE WTE_FIL_ID=' . $Param['FIL_ID'];
        $SQL .= ' AND WTE_STATUS <8';
        $SQL .= ' AND WPO_IMPORTSTATUS <> \'X\'';
        $SQL .= ' AND (TRUNC(WPO_TERMINVON)=' . $DB->FeldInhaltFormat('D', $Param['TAG'], false);
        $SQL .= ' OR TRUNC(WPO_TERMINBIS)=' . $DB->FeldInhaltFormat('D', $Param['TAG'], false) . ')';
        $SQL .= ' ORDER BY WTE_WANR, WPO_TERMINVON, WPO_TERMINBIS, WPO_AST_ATUNR';

        $rsWTE = $DB->RecordSetOeffnen($SQL);
        $Form->DebugAusgabe(1, $SQL);
        // Farben f�r den Status der Auftr�ge
        $StatusFarben = array('#FFD0D0', '#FFFDFF');

        echo '<table class="TabelleRund" border=1 witdh=800px>';
        echo '<tr>';
        echo '<td width=450px valign=top>';
        if ($rsWTE->EOF()) {
            echo 'Es wurden keine Termine f&uuml;r diese Filiale gefunden.';
        }

        $LetzterWA = 0;            // F�r die Gruppierung
        while (!$rsWTE->EOF()) {
            $SQL = ' SELECT WTP_KEY, KON_NAME1, KON_NAME2 ';
            $SQL .= ' FROM WERKSTATTTERMINEPLANUNGEN';
            $SQL .= ' INNER JOIN Kontakte ON WTP_KON_KEY = KON_KEY';
            $SQL .= ' WHERE WTP_WPO_KEY = ' . $rsWTE->FeldInhalt('WPO_KEY');
            $SQL .= ' AND WTP_Status < 8';

            $rsWTP = $DB->RecordSetOeffnen($SQL);

            if ($LetzterWA != $rsWTE->FeldInhalt('WTE_KEY')) {
                if ($LetzterWA != 0) {
                    echo '</td></tr></table>';
                }
                echo '<table  class="TabelleRund" border=1 width=100%><tr><td>';
                $LetzterWA = $rsWTE->FeldInhalt('WTE_KEY');

                echo '<div class="ToolTipp">';
                $Java = 'if(document.getElementById(\'ToolTipp_' . $rsWTE->FeldInhalt('WTE_KEY') . '\').style.visibility==\'visible\')';
                $Java .= '{';
                $Java .= 'document.getElementById(\'ToolTipp_' . $rsWTE->FeldInhalt('WTE_KEY') . '\').style.visibility=\'hidden\';';
                $Java .= '}else{';
                $Java .= 'document.getElementById(\'ToolTipp_' . $rsWTE->FeldInhalt('WTE_KEY') . '\').style.visibility=\'visible\';';
                $Java .= '}';

                echo '<img border=0 width=16 height=16 src="/bilder/info.png" onclick="' . $Java . '">';
                // WA
                // ATU Nummer mit Bezeichnung

                $SQL = 'SELECT *';
                $SQL .= ' FROM WerkstatttermineDetails';
                $SQL .= ' WHERE WTD_WTE_KEY = ' . $rsWTE->FeldInhalt('WTE_KEY');
                $SQL .= ' ORDER BY WTD_POSITION';

                $rsWTD = $DB->RecordSetOeffnen($SQL);

                $ToolTipp = '<table  class="TabelleRund" border=0 width=99% style="padding:2px;margin:2px;">';
                $ToolTippZeilen = 1;
                while (!$rsWTD->EOF()) {
                    $ToolTipp .= '<tr><td>' . $rsWTD->FeldInhalt('WTD_AST_ATUNR') . '';
                    $ToolTipp .= '</td><td>' . $rsWTD->FeldInhalt('WTD_BEZEICHNUNG');
                    if ($rsWTD->FeldInhalt('WTD_HINWEIS') != '') {
                        $ToolTipp .= '<br>' . $rsWTD->FeldInhalt('WTD_HINWEIS');
                    }
                    $ToolTipp .= '</td><td align=right>' . $Form->Format('N0T', $DB->FeldInhaltFormat('N2', $rsWTD->FeldInhalt('WTD_VK')) * $rsWTD->FeldInhalt('WTD_MENGE'));
                    $ToolTipp .= '&nbsp;&nbsp;';

                    $ToolTipp .= '</td></tr>';
                    $rsWTD->DSWeiter();
                    if (!$rsWTD->EOF()) {
                        $ToolTippZeilen++;
                    }
                }

                // Kundenfahrzeuganlieferung anzeigen
                $ToolTipp .= '<tr><td colspan="4">' . $AWISSprachKonserven['WTE']['WTE_KUNDENFZVON'] . ': ';
                $ToolTipp .= $Form->Format('DU', $rsWTE->FeldInhalt('WTE_KUNDENFZVON'), false) . ' - ' . $Form->Format('DU', $rsWTE->FeldInhalt('WTE_KUNDENFZBIS'), false) . '</td></tr>';
                $ToolTippZeilen++;

                $ToolTipp .= '</table>';

                // Tooltipp
                echo '<span id="ToolTipp_' . $rsWTE->FeldInhalt('WTE_KEY') . '" style="width:750px;height:' . (21 * ($ToolTippZeilen > 10 ? 10 : $ToolTippZeilen)) . 'px;overflow:' . ($ToolTippZeilen > 10 ? 'auto' : 'none') . '">';
                echo $ToolTipp;
                // Ende Tooltipp
                echo '</span></div><br>';

                echo '<div style="width:350px">' . $rsWTE->FeldInhalt('WTE_WANR') . '/' . $rsWTE->FeldInhalt('WTE_VORGANGSART') . ($rsWTE->FeldInhalt('WTE_STATUS') == 7 ? '*' : '') . '</div><br>';
            }

            echo '<table  class="TabelleRund" border=1 width=100%>';
            echo '<tr><td style="background-color:' . $StatusFarben[$rsWTE->FeldInhalt('WPO_STATUS')] . '">';

            echo $rsWTE->FeldInhalt('WPO_AST_ATUNR') . ' - ' . $rsWTE->FeldInhalt('WPO_BEZEICHNUNG') . '<br>';
            echo date('d.m. H:i', $Form->PruefeDatum($rsWTE->FeldInhalt('WPO_TERMINVON'), true, false, true)) . ' - ' . date('d.m. H:i', $Form->PruefeDatum($rsWTE->FeldInhalt('WPO_TERMINBIS'), true, true, true)) . '<br>';
            echo $Form->Format('N1', ($rsWTE->FeldInhalt('WPO_ZEITMIN')) / 60) . 'h, ' . $Form->Format('N0T', ($rsWTE->FeldInhalt('WPO_VK'))) . ' Eur<br>';

            // Ist der WA zugeordnet?
            $AnzahlZuordnungen = 0;
            while (!$rsWTP->EOF()) {
                if ((($Recht11004 & 2) == 2) or $Speichern == true) {
                    echo '<a class="BilderLink" href="./srpep_Main.php?cmdAktion=Planung&DEL=' . $rsWTP->FeldInhalt('WTP_KEY') . '"><img border=0 src="/bilder/icon_delete.png"></a>';
                }
                echo $rsWTP->FeldInhalt('KON_NAME1') . ', ' . $rsWTP->FeldInhalt('KON_NAME2');
                echo '<br>';

                $AnzahlZuordnungen++;
                $rsWTP->DSWeiter();
            }

            // Es d�rfen maximal 2 Zuordnungen pro Termin gemacht werden
            if ($AnzahlZuordnungen < 2) {
                echo '<input name="txtCHK_WPO_' . $rsWTE->FeldInhalt('WPO_KEY') . '" type="checkbox"><br>';
            }

            echo '</td></tr>';
            echo '</table>';

            $rsWTE->DSWeiter();
        }
        echo '</td></tr></table>';    // Ende des Rahmnes pro WA

        echo '</td>';                // Ende des linken Bereichs
        echo '<td width=350px valign=top >';
        $Height = $rsWTE->AnzahlDatensaetze() * 140; //Deutsche-Industrie-Norm f�r die H�he eines Datensatzes :)
        echo '<div style="height:'.$Height.'px; overflow: scroll" class="Tabellenbegrenzer">';

        //*********************************************************************************************
        //* Mitarbeiter
        //*********************************************************************************************

        if (($Recht11000 & 2) != 2) {
            $SQL  ='select distinct KON_KEY,';
            $SQL .='   KON_NAME1,';
            $SQL .='   KON_NAME2,';
            $SQL .='   KON_PER_NR,';
            $SQL .='   (select KKO_WERT';
            $SQL .='   from KONTAKTEKOMMUNIKATION';
            $SQL .='   where KKO_KOT_KEY = 6';
            $SQL .='   and KKO_KON_KEY   = KON_KEY';
            $SQL .='   ) as HANDYNUMMER';
            $SQL .=' from KONTAKTE';
            $SQL .=' inner join V_FILIALEBENENROLLEN';
            $SQL .=' on XX1_KON_KEY = KON_KEY';
            $SQL .=' inner join FILIALEBENENROLLENZUORDNUNGEN';
            $SQL .=' on FRZ_FER_KEY             = 39';
            $SQL .=' and XX1_FRZ_KEY            = FRZ_KEY';
            $SQL .=' and TRUNC(FRZ_GUELTIGAB)  <= TO_DATE('.$DB->WertSetzen('KON', 'D', $Param['TAG'], false) .',\'DD.MM.RRRR\')';
            $SQL .=' and TRUNC(FRZ_GUELTIGBIS) >= TO_DATE('.$DB->WertSetzen('KON', 'D', $Param['TAG'], false) .', \'DD.MM.RRRR\')';
            $SQL .=' where KON_KEY              = ' . $DB->WertSetzen('KON','N0',$AWISBenutzer->BenutzerKontaktKEY());


        }else{
            $SQL = 'SELECT * FROM ( ';
            $SQL .= 'SELECT DISTINCT TO_NUMBER(PIF_WERT) AS PER_STAMMFILIALE,';
            $SQL .= '   CASE';
            $SQL .= '     WHEN TO_NUMBER(PIF_WERT) = '.  $DB->WertSetzen('KON','N0', $Param['FIL_ID']);
            $SQL .= '     THEN \'0\'';
            $SQL .= '     ELSE TO_CHAR(FEG_ENTFERNUNG)';
            $SQL .= '   END AS FEG_ENTFERNUNG,';
            $SQL .= '   CASE';
            $SQL .= '     WHEN TO_NUMBER(PIF_WERT) = ' .  $DB->WertSetzen('KON','N0', $Param['FIL_ID']);
            $SQL .= '     THEN \'0\'';
            $SQL .= '     ELSE TO_CHAR(FEG_FAHRZEIT)';
            $SQL .= '   END AS FEG_FAHRZEIT,';
            $SQL .= '   CASE';
            $SQL .= '     WHEN TO_NUMBER(PIF_WERT) = ' .  $DB->WertSetzen('KON','N0', $Param['FIL_ID']);
            $SQL .= '     THEN \'0\'';
            $SQL .= '     ELSE TO_CHAR(FEG_KOSTEN)';
            $SQL .= '   END AS FEG_KOSTEN,';
            $SQL .= '   KON_KEY,';
            $SQL .= '   KON_NAME1,';
            $SQL .= '   KON_NAME2,';
            $SQL .= '   KON_PER_NR,';
            $SQL .= '   PEK_TEXT,';
            $SQL .= '   PEK_ID,';
            $SQL .= '   (SELECT KKO_WERT';
            $SQL .= '   FROM KONTAKTEKOMMUNIKATION';
            $SQL .= '   WHERE KKO_KOT_KEY = 6';
            $SQL .= '   AND KKO_KON_KEY   = KON_KEY';
            $SQL .= '   ) AS HANDYNUMMER';
            $SQL .= ' FROM FILIALEBENENROLLENZUORDNUNGEN';
            $SQL .= ' INNER JOIN KONTAKTE';
            $SQL .= ' ON FRZ_KON_KEY = KON_KEY';
            $SQL .= ' LEFT JOIN PERSONALINFOS';
            $SQL .= ' ON KON_PER_NR   = PIF_PER_NR';
            $SQL .= ' AND PIF_ITY_KEY = 11';
            $SQL .= ' LEFT OUTER JOIN KONTAKTEVERFUEGBARKEITEN';
            $SQL .= ' ON KOV_KON_KEY = KON_KEY';
            $SQL .= ' AND KOV_WOCHE  = TO_CHAR(TO_DATE(' . $DB->WertSetzen('KON', 'D', $Param['TAG'], false) . ', \'DD.MM.RRRR\'), \'IW\')';
            $SQL .= ' LEFT JOIN FILIALEN';
            $SQL .= ' ON PIF_WERT = FIL_ID';
            $SQL .= ' LEFT OUTER JOIN PKUERZEL';
            $SQL .= ' ON PEK_KEY =';
            $SQL .= '   CASE TO_CHAR(TO_DATE(' . $DB->WertSetzen('KON', 'D', $Param['TAG'], false) . ', \'DD.MM.RRRR\'), \'D\')';
            $SQL .= '     WHEN \'1\'';
            $SQL .= '     THEN KOV_PEK_KEY_MONTAG';
            $SQL .= '     WHEN \'2\'';
            $SQL .= '     THEN KOV_PEK_KEY_DIENSTAG';
            $SQL .= '     WHEN \'3\'';
            $SQL .= '     THEN KOV_PEK_KEY_MITTWOCH';
            $SQL .= '     WHEN \'4\'';
            $SQL .= '     THEN KOV_PEK_KEY_DONNERSTAG';
            $SQL .= '     WHEN \'5\'';
            $SQL .= '     THEN KOV_PEK_KEY_FREITAG';
            $SQL .= '     WHEN \'6\'';
            $SQL .= '     THEN KOV_PEK_KEY_SAMSTAG';
            $SQL .= '     WHEN \'7\'';
            $SQL .= '     THEN KOV_PEK_KEY_SONNTAG';
            $SQL .= '   END';
            $SQL .= ' AND PEK_LAN_WWS_KENN = FIL_LAN_WWSKENN';
            $SQL .= ' LEFT JOIN FILIALENENTFERNUNGEN';
            $SQL .= ' ON ( ( FEG_FIL_ID_VON      = ' . $DB->WertSetzen('KON','N0',$Param['FIL_ID']);
            $SQL .= ' AND FEG_FIL_ID_NACH        = TO_NUMBER(PIF_WERT) )';
            $SQL .= ' OR ( FEG_FIL_ID_VON        = TO_NUMBER(PIF_WERT)';
            $SQL .= ' AND FEG_FIL_ID_NACH        = '. $DB->WertSetzen('KON','N0', $Param['FIL_ID']) .' ) )';
            $SQL .= ' WHERE FRZ_FER_KEY          = 39';
            $SQL .= ' AND TRUNC(FRZ_GUELTIGAB)  <= ' . $DB->WertSetzen('KON', 'D', $Param['TAG'], false);
            $SQL .= ' AND TRUNC(FRZ_GUELTIGBIS) >= ' . $DB->WertSetzen('KON', 'D', $Param['TAG'], false);
            $SQL .= ' AND PEK_ID IN (' . $AWISBenutzer->ParameterLesen('PKuerzel-Anwesenheit') . ')';
            $SQL .= ' )';
            $SQL .= ' ORDER BY TO_NUMBER(FEG_ENTFERNUNG) ASC';
        }

        $rsKON = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('KON'));
        $Form->DebugAusgabe(1,$DB->LetzterSQL());

        // Hinweis, wenn keine Mitarbeiter verf�gbar sind
        if ($rsKON->EOF()) {
            echo 'Es wurden keine Mitarbeiter gefunden. Pr�fen Sie die <a class=MenueLink href=./srpep_Main.php?cmdAktion=Verfuegbarkeiten>Verf�gbarkeiten.</a>';
            $Form->DebugAusgabe(1, $DB->LetzterSQL());
        }

        while (!$rsKON->EOF()) {

            // SK: Betr�ge aufteilen
            $SQL = 'SELECT SUM(DATEN.VK/COALESCE(';
            $SQL .= ' (SELECT COUNT(*) FROM WerkstattterminePlanungen  W1';
            $SQL .= ' INNER JOIN WERKSTATTTERMINEPOSITIONEN ON WTP_WPO_KEY = WPO_KEY AND WPO_IMPORTSTATUS <> \'X\'';
            $SQL .= ' WHERE W1.WTP_WPO_KEY = DATEN.WTP_WPO_KEY';
            $SQL .= ' AND WTP_STATUS < 8),1)) AS VK';
            $SQL .= ' , MIN(MIN) AS MIN';
            $SQL .= ' FROM(';
            $SQL .= ' SELECT sum(WTP_VK) AS VK, SUM(WTP_ZEITMIN) AS MIN, WTP_WPO_KEY';
            $SQL .= ' FROM WerkstattterminePlanungen W2';
            $SQL .= ' INNER JOIN WERKSTATTTERMINEPOSITIONEN ON WTP_WPO_KEY = WPO_KEY AND WPO_IMPORTSTATUS <> \'X\'';
            $SQL .= ' WHERE WTP_KON_KEY = ' . $rsKON->FeldInhalt('KON_KEY');
            $SQL .= ' AND WTP_TAG = ' . $DB->FeldInhaltFormat('D', $Param['TAG']);
            $SQL .= ' AND WTP_STATUS < 8';
            $SQL .= ' GROUP BY WTP_WPO_KEY';
            $SQL .= ' ) DATEN';
            $rsXXX = $DB->RecordSetOeffnen($SQL);

            echo '<table  class="TabelleRund" width=100%; border=1><tr>';

            echo '<tr><td with="100%" style="background-color:#F0F0F0; ">';
            echo '<div style="width:350px; ">';
            if ($rsKON->FeldInhalt('HANDYNUMMER') != '') {
                $Parameter = base64_encode('KON_KEY=' . $rsKON->FeldInhalt('KON_KEY') . '&HANDY=' . $rsKON->FeldInhalt('HANDYNUMMER') . '&TAG=' . $Form->Format('D', $Param['TAG']));

                if ((($Recht11004 & 2) == 2) or $Speichern == true) {
                    echo '<img class=BilderLink src="/bilder/icon_mail.png"';
                    $Java = " onclick=window.open('./srpep_sms.php?ID=" . $Parameter . "','SMS','width=400,height=300,toolbar=no,menubar=no,dependent=yes,status=no');";
                    echo $Java;
                    echo '>';
                }
            }

            echo $rsKON->FeldInhalt('KON_NAME1') . ', ' . $rsKON->FeldInhalt('KON_NAME2') . '</div>';
            echo '</div><br>';
            echo '' . $rsKON->FeldInhalt('PEK_ID') . '<br>';

            echo 'Umsatz:' . $Form->Format('N0T', $rsXXX->FeldInhalt('VK'), false) . ' Eur<br>';

            $MAFahrzeit = explode(':', ($rsKON->FeldInhalt('FEG_FAHRZEIT')));
            $MAFahrzeit = (@$MAFahrzeit[0] * 60) + (@$MAFahrzeit[1]);
            echo 'Standort:' . $Form->Format('T', $rsKON->FeldInhalt('PER_STAMMFILIALE'), false) . ', ' . $Form->Format('N0', $rsKON->FeldInhalt('FEG_ENTFERNUNG')) . 'km, ' . $Form->Format('N1', ($MAFahrzeit / 60), false) . 'h.<br>';

            echo '<input name="txtCHK_KON_' . $rsKON->FeldInhalt('KON_KEY') . '_' . ($rsKON->FeldInhalt('EIGEN') == 1 ? 0 : 1) . '" type="checkbox"><br>';
            echo '<input type="hidden" name="txtFAHRZEIT_' . $rsKON->FeldInhalt('KON_KEY') . '" value="' . $MAFahrzeit . '">';
            echo '</td>';

            echo '</tr></table>';

            $rsKON->DSWeiter();
        }
        echo '</div>';
        echo '</td>';
        echo '</tr>';
        echo '</table>';
    }

    $Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if ((($Recht11004 & 2) == 2) or $Speichern == true) {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

    $Link = './srpep_bericht_1.php?TAG=' . $Param['TAG'] . '&Typ=M';
    $Form->Schaltflaeche('href', 'cmdDruckenM', $Link, '/bilder/cmd_benutzer.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
    $Link = './srpep_bericht_1.php?TAG=' . $Param['TAG'] . '&Typ=F';
    $Form->Schaltflaeche('href', 'cmdDruckenF', $Link, '/bilder/cmd_antenne.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'P');

    $Form->SchaltflaechenEnde();

    if ($AWISCursorPosition != '') {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();";
        echo '</Script>';
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200912171119");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 3, "200912171120");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}