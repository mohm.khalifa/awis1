<?php
require_once 'awisDatenbank.inc';
require_once 'awisMailer.inc';

class awisAutoglasSteuerUebersicht
{

    const ExpPfad = '/daten/daten/versicherungen/export/';

    /**
     * @var int
     */
    protected $_DebugLevel = 0;

    /**
     * @var String
     */
    protected $_DebugText = "";

    /**
     * @var awisDatenbank
     */
    protected $_DB;

    /**
     * @var awisBenutzer
     */
    protected $_Benutzer;

    /**
     * @var awisWerkzeuge
     */
    protected $_Werkzeuge;

    /**
     * @var awisMailer
     */
    protected $_Mailer;

    /**
     * @var awisRecordset
     */
    protected $_rsVVS;

    protected $_ErrorCounter = 0;

    /**
     * awisAutoglasSteuerUebersicht constructor
     * @param $Benutzer
     */
    function __construct($Benutzer)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Benutzer = awisBenutzer::Init($Benutzer);
        $this->_Werkzeuge = new awisWerkzeuge();
        $this->_Mailer = new awisMailer($this->_DB,$this->_Benutzer);
    }

    /**
     * Normaler Verarbeitungsprozess
     */
    public function verarbeitung() {
        $this->ladeDaten();
        $this->sendeMail();
        if($this->_rsVVS->AnzahlDatensaetze() > 0) {
            $this->speicherNeueDaten();
            if($this->_ErrorCounter) {
                $this->schreibeERRORMail();
            }
        }

    }

    /**
     * Setzt und returnt das Debuglevel
     *
     * @param int $Level
     * @return int
     */
    public function DebugLevel($Level = 0)
    {
        if ($Level != 0) {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Hole neue Datensaetze
     */
    private function ladeDaten() {

        $this->debugAusgabe('Hole Daten', 99);

        $SQL = "select";
        $SQL .= " a.vvs_filid as FILNR,";
        $SQL .= " a.vvs_wanr as WANR,";
        $SQL .= " a.vvs_vorgangnr as AUTOGLASVORGANGNR,";
        $SQL .= " b.umsatz_gesamt as AX_BRUTTOBETRAG,";
        $SQL .= " b.umsatz_gesamt - b.vorst_betrag as AX_NETTOBETRAG,";
        $SQL .= " b.vorst_betrag as AX_MWST,";
        $SQL .= " round(b.vorst_betrag / ((b.umsatz_gesamt - b.vorst_betrag) / 100),0) as AX_MWST_SATZ,";
        $SQL .= " c.fgk_betrag_brutto as AUTOGLAS_BRUTTOBETRAG,";
        $SQL .= " c.fgk_betrag_netto as AUTOGLAS_NETTOBETRAG,";
        $SQL .= " c.fgk_steuersatz as AUTOGLAS_MWST_SATZ";
        $SQL .= " from versvorgangsstatus a left join vers_exp_kopf b";
        $SQL .= " on a.vvs_filid = b.filid";
        $SQL .= " and a.vvs_wanr = b.wanr";
        $SQL .= " left join fgkopfdaten c";
        $SQL .= " on a.vvs_vorgangnr = c.fgk_vorgangnr";
        $SQL .= " left join MWSTUEBERSICHTVERGLEICH d";
        $SQL .= " on a.vvs_filid = d.vvs_filid";
        $SQL .= " and a.vvs_wanr = d.vvs_wanr";
        $SQL .= " where vvs_ven_key = 30";
        $SQL .= " and trunc(vvs_userdat) >= '01.01.2021'";
        $SQL .= " and a.vvs_dubcheck = 0";
        $SQL .= " and d.vvs_filid is null";

        $this->_rsVVS = $this->_DB->RecordSetOeffnen($SQL);
    }

    /**
     * Versende Mail und l�st ggf. Anhangs-Laden-Funktion aus
     */
    private function sendeMail() {
        
        $this->_Mailer->LoescheAdressListe();
        $this->_Mailer->Absender('shuttle@de.atu.eu');

        $this->debugAusgabe('Lade AdressListe', 99);

        foreach(explode(';',$this->_Benutzer->ParameterLesen('MWSTUEBERSICHT_EMPFAENGER')) as $Empfaenger) {
            $this->_Mailer->AdressListe(awisMailer::TYP_TO,$Empfaenger);
        }

        if($this->_rsVVS->AnzahlDatensaetze() > 0) {
            $this->debugAusgabe('Habe Daten. Schreibe Mail mit Anhang', 99);
            $this->_Mailer->Text('Anbei die Differenz-Uebersicht', awisMailer::FORMAT_TEXT);
            $this->_Mailer->Betreff('OK - AUTOGLAS - MWST DIFFERENZ UEBERSICHT - ' . date('dmY'));

            $this->LadeAnhang();
        } else {
            $this->debugAusgabe('Habe keine Daten. Schreibe OK-Mail ohne Anhang', 99);
            $this->_Mailer->Text('Habe keine Daten gefunden; Keine Datei in Anhang', awisMailer::FORMAT_TEXT);
            $this->_Mailer->Betreff('INFO - AUTOGLAS - MWST DIFFERENZ UEBERSICHT - ' . date('dmY'));
        }
        $this->_Mailer->SetzeBezug('VVS', 0);

        $this->_Mailer->MailInWarteschlange();

    }

    /**
     * Nimmt die Daten des Recordsets und setzt sie in eine csv, die dann als Anhang hinzugefuegt wird.
     */
    private function LadeAnhang() {
        $_rsVVS = $this->_rsVVS;

        $this->debugAusgabe('Beginne Export:', 1);
        $ExpDatei = 'atu_mwstUebersicht_' . date('dmY');
        if (file_exists(self::ExpPfad . $ExpDatei)) {
            $this->debugAusgabe('Es liegen Reste vom letzten Export herum. Breche ab. Pfad der Reste: ' . self::ExpPfad, 1);
            die;
        }
        $this->debugAusgabe('Erstelle jetzt Datei', 99);
        $Datei = fopen(self::ExpPfad . $ExpDatei . '.csv', 'w+');

        $Zeile = '';
        $Zeile .= '"FILNR",';
        $Zeile .= '"WANR",';
        $Zeile .= '"AUTOGLASVORGANGNR",';
        $Zeile .= '"AX_BRUTTOBETRAG",';
        $Zeile .= '"AX_NETTOBETRAG",';
        $Zeile .= '"AX_MWST",';
        $Zeile .= '"AX_MWST_SATZ",';
        $Zeile .= '"AUTOGLAS_BRUTTOBETRAG",';
        $Zeile .= '"AUTOGLAS_NETTOBETRAG",';
        $Zeile .= '"AUTOGLAS_MWST_SATZ"';
        $Zeile .= chr(13) . chr(10);
        fwrite($Datei, $Zeile);

        $this->debugAusgabe('Schreibe jetzt Dateizeilen', 99);

        while (!$_rsVVS->EOF()) {
            $Zeile = "";

            $Zeile .= $_rsVVS->FeldInhalt('FILNR') . ',';
            $Zeile .= $_rsVVS->FeldInhalt('WANR') . ',';
            $Zeile .= $_rsVVS->FeldInhalt('AUTOGLASVORGANGNR') . ',';
            $Zeile .= '"' . $_rsVVS->FeldInhalt('AX_BRUTTOBETRAG') . '",';
            $Zeile .= '"' . $_rsVVS->FeldInhalt('AX_NETTOBETRAG') . '",';
            $Zeile .= '"' . $_rsVVS->FeldInhalt('AX_MWST') . '",';
            $Zeile .= $_rsVVS->FeldInhalt('AX_MWST_SATZ') . ',';
            $Zeile .= '"' . $_rsVVS->FeldInhalt('AUTOGLAS_BRUTTOBETRAG') . '",';
            $Zeile .= '"' . $_rsVVS->FeldInhalt('AUTOGLAS_NETTOBETRAG') . '",';
            $Zeile .= $_rsVVS->FeldInhalt('AUTOGLAS_MWST_SATZ') . '';

            //LineFeed
            $Zeile .= chr(13) . chr(10);

            fwrite($Datei, $Zeile);

            $_rsVVS->DSWeiter();
        }
        $this->debugAusgabe('Habe alle Zeilen geschrieben. Haenge als Anhang an', 99);

        fclose($Datei);
        $this->_Mailer->AnhaengeLoeschen();
        $this->_Mailer->Anhaenge(self::ExpPfad . $ExpDatei . '.csv', $ExpDatei . '.csv', true);
    }

    /**
     * Speichert neue Datensaetze ab, damit diese am naechsten Tag nicht nochmal exportiert werden
     */
    private function speicherNeueDaten() {
        $_rsVVS = $this->_rsVVS;

        $this->debugAusgabe('Speicher neue Daten', 99);

        while (!$_rsVVS->EOF()) {
            try {
                $this->_DB->TransaktionBegin();

                $SQL = "";
                $SQL .= "INSERT INTO ";
                $SQL .= " MWSTUEBERSICHTVERGLEICH ";
                $SQL .= " ( ";
                $SQL .= " VVS_FILID, ";
                $SQL .= " VVS_WANR ";
                $SQL .= " ) ";
                $SQL .= " VALUES ";
                $SQL .= " ( ";
                $SQL .= " " . $this->_DB->WertSetzen('VVS', 'Z', $_rsVVS->FeldInhalt('FILNR')) . ', ';
                $SQL .= " " . $this->_DB->WertSetzen('VVS', 'Z', $_rsVVS->FeldInhalt('WANR'));
                $SQL .= " ) ";

                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VVS'));
                $this->_DB->TransaktionCommit();
            } catch (Exception $e) {
                $this->debugAusgabe('Habe Error bei Datensatz: FILNR = '.$_rsVVS->FeldInhalt('FILNR').' WANR = '. $_rsVVS->FeldInhalt('WANR'), 99);
                $this->_ErrorCounter++;
                if($this->_DB->TransaktionAktiv()) {
                    $this->_DB->TransaktionRollback();
                }
            }

            $_rsVVS->DSWeiter();
        }
    }

    /**
     * Schreibt eine ERROR-Mail falls Fehler aufgetreten sind
     */
    private function schreibeERRORMail()
    {
        $this->debugAusgabe("Schreibe ERROR Mail",99);

        $this->_Mailer->LoescheAdressListe();
        $this->_Mailer->AnhaengeLoeschen();
        $this->_Mailer->Absender('shuttle@de.atu.eu');

        foreach(explode(';',$this->_Benutzer->ParameterLesen('MWSTUEBERSICHT_EMPFAENGER')) as $Empfaenger) {
            $this->_Mailer->AdressListe(awisMailer::TYP_TO,$Empfaenger);
        }

        $this->_Mailer->Text($this->_DebugText, awisMailer::FORMAT_TEXT, true);
        $this->_Mailer->Betreff('ERROR - AUTOGLAS - MWST DIFFERENZ UEBERSICHT - ' . date('dmY'));
        $this->_Mailer->SetzeBezug('VVS', 0);

        $this->_Mailer->MailInWarteschlange();
    }


    /**
     * Debugausgabe ausgeben
     *
     * @param string $text
     * @param int $debugLevel
     */
    private function debugAusgabe($text, $debugLevel) {

        $this->_DebugText = $this->_DebugText.$text."\r\n";
        if ($this->_DebugLevel >= $debugLevel) {
            echo $text . ((substr($text, -(strlen(PHP_EOL))) == PHP_EOL)?'':PHP_EOL);
        }
    }
}