<?php
require_once('/daten/include/GooglePlacesExport.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
	//TODO: Protokoll, Debug etc
    $Benutzer = 'ott_h';
	$Datei = '/daten/daten/pccommon/E-Commerce/Google/googleplaces.xls';
	$Protokoll = '/daten/daten/pccommon/E-Commerce/Google/protokoll.txt';
    $DebugLevel = 0;
    $JobId = 0;
    $Testsystem = false;

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--datei':
                            $Datei = $argv[++$i];
                            break;
                    case '--protokoll':
                            $Protokoll = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--testsystem':
                            $Testsystem = true;
                            break;
                    case '--jobid':
                            $JobId = $argv[++$i];
                            break;
                    default:
							echo 'Parameter '.$argv[$i].' ist unbekannt.';
							case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --datei <Name>                     Import-/Exportdatei.'.PHP_EOL;
                            echo '  --protokoll <Name>                 Datei f�r Aenderungsprotokoll.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --testsystem                       Daten aus dem Testsystem abfragen.'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    if($JobId!=0)
    {
        $Job = new awisJobProtokoll($JobId, 'Starte GooglePlaces - Export.');
    }	

    GooglePlacesOeffnungszeitenExport($Benutzer,$Datei,$Protokoll);
	
    if($JobId!=0)
    {
        $Job->SchreibeLog(1,0,'Ende des Jobs.');
    }
}
catch (awisException $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'SQL: '.$ex->getSQL().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausf�hrung:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausf�hrung:'.$ex->getMessage());
    }
}
?>
