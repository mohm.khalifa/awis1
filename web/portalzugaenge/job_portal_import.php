<?php
/**
 * Job_import.php
 * 
 * Mit diesem Job werden die aktuellen St�nde aus dem Portal in AWIS eingelesen
 * 
 * @author Sacha Kerres
 * @copyright ATU Auto-Teile-Unger
 * @version 20091216
 * @package PORTAL
 */
require_once 'awis_portalzugaenge.inc';
require_once 'awisJobVerwaltung.inc';

try
{
	$Job = new awisJobProtokoll(11,'Starte Import.');
	$Portal = new awis_Portalzugaenge('awis_jobs');
	
	$Job->SchreibeLog(awisJobProtokoll::TYP_MELDUNG,0,'Suche Daten');	

	$Portal->DebugLevel(10);

// 	Aktuellen Stand aus dem Portal einlesen
	$Portal->BenutzerDatenEinlesen();
	$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,0,'Import  abgeschlossen.');	
	
}
catch (Exception $ex)
{
	if(isset($Job))
	{
		$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,3,'Fehler:'.$ex->getMessage());
	}
}
?>