<?php
require_once('awisAusdruck.php');

class NeuePersBerichte{

    const LINKERRAND = 15;
    const BREITEFRAGEN = 150;
    const BREITEOK = 10;
    const BREITENOK = 15;
    const ZEILENHOEHESTANDARD = 5;
    const KAESTCHENGROESSE = 3;
    const ABSTANDBLOCKFRAGE = 5;

    /**
     * @var awisAusdruck
     */
    public $Ausdruck;

    /**
     * @var awisBenutzer
     */
    private $_AWISBenutzer;

    /**
     * @var bool
     */
    private $_Seite1Ausgegeben = false;

    /**
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * @var array
     */
    private $_AWISSprachkonserven;

    /**
     * @var awisRecordset
     */
    private $_rsBericht;

    /**
     * @var string
     */
    private $_Schriftart;

    /**
     * @var bool
     */
    private $_LetztesElement = null;

    /**
     * @var string
     */
    private $_HeaderUeberschrift;

    /**
     * @var string
     */
    private $_HeaderUeberschrift2;

    /**
     * @var string
     */
    private $_Herausgabedatum;

    /**
     * @var string
     */
    private $_Version;

    /**
     * @var string
     */
    private $_FooterText;

    private $_GBL_PBF_ID;
    private $_GL_PBF_ID;
    private $_WL_PBF_ID;


    /**
     * NeuePersBerichte constructor.
     *
     * @param awisDatenbank $DB
     * @param awisBenutzer  $AWISBenutzer
     * @param array         $AWISSprachkonserven
     * @param awisRecordset $rsBericht
     *
     * @throws Exception
     */
    function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer, array $AWISSprachkonserven, awisRecordset $rsBericht)
    {
        $this->Ausdruck = new awisAusdruck('P', 'A4', '');;
        $this->_DB = $DB;
        $this->_AWISBenutzer = $AWISBenutzer;
        $this->_AWISSprachkonserven = $AWISSprachkonserven;
        $this->_rsBericht = $rsBericht;

        if ($this->_AWISBenutzer->BenutzerSprache() == 'CZ') {
            $this->Ausdruck->_pdf->ZeichenKodierung('CP1250');
            $this->_Schriftart = 'arialcz';
        } else {
            $this->_Schriftart = 'arial';
        }

    }

    /**
     * Setzt den Footertext
     * @param $Text
     */
    public function FooterDaten($Text){
        $this->_FooterText = $Text;
    }

    /**
     * Erstellt den Footer
     */
    public function ErstelleFooter(){

        $Zeile = 290;
        $Spalte = 15;
        $this->Ausdruck->_pdf->SetFont('Arial', '', 6);
        $this->Ausdruck->_pdf->SetXY($Spalte, $Zeile);
        $this->Ausdruck->_pdf->Cell2(180,self::ZEILENHOEHESTANDARD, $this->_FooterText, 'T', 0, 'C', 0);
        $this->_LetztesElement = 'footer';
    }

    /**
     * Initialisiert die Headerdaten
     *
     * @param $Ueberschrift
     * @param $Ueberschrift2
     * @param $Herausgabedatum
     * @param $Version
     */
    public function HeaderDaten($Ueberschrift, $Ueberschrift2, $Herausgabedatum, $Version, $GBL_PBF_ID, $GL_PBF_ID, $WL_PBF_ID){
        $this->_HeaderUeberschrift = $Ueberschrift;
        $this->_HeaderUeberschrift2 = $Ueberschrift2;
        $this->_Herausgabedatum = $Herausgabedatum;
        $this->_Version = $Version;
        $this->_GBL_PBF_ID = $GBL_PBF_ID;
        $this->_GL_PBF_ID = $GL_PBF_ID;
        $this->_WL_PBF_ID = $WL_PBF_ID;
    }

    /**
     * Erstellt den Header
     */
    public function ErstelleHeader($Kreuzfrage = false){
        $this->Ausdruck->_pdf->AddFont($this->_Schriftart, '', '');

        $Spalte = 15;
        $Zeile = 5;
        $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $this->Ausdruck->_pdf->SetFont($this->_Schriftart, ($this->_AWISBenutzer->BenutzerSprache() != 'CZ' ? 'B' : ''), 16);
        $this->Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5, 12.5, 591 / 22.5, 276 / 22.5);
        $this->Ausdruck->_pdf->cell2(30, 26, '', 'LTRB', 0, 'L', 0);
        $this->Ausdruck->_pdf->cell2(120, 8, $this->_HeaderUeberschrift, 'LTRB', 0, 'C', 0);
        $this->Ausdruck->_pdf->SetFont($this->_Schriftart, '', 8);
        //$this->_Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
        $this->Ausdruck->_pdf->cell2(30, 8, 'g�ltig in ', 'LRT', 0, 'C', 0);

        $Zeile += 8;
        $Spalte += 30;
        $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $this->Ausdruck->_pdf->SetFont($this->_Schriftart, '', 18);
        $this->Ausdruck->_pdf->cell2(120, 8, $this->_HeaderUeberschrift2, 'LTRB', 0, 'C', 0);
        $this->Ausdruck->_pdf->SetFont($this->_Schriftart, '', 8);
        $this->Ausdruck->_pdf->cell2(30, 8, 'DE/AT/CH', 'LRB', 0, 'C', 0);

        $Zeile += 8;
        $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $this->Ausdruck->_pdf->SetFont($this->_Schriftart, '', 8);
        $this->Ausdruck->_pdf->cell2(120,self::ZEILENHOEHESTANDARD, 'COO', 'LTRB', 0, 'L', 0);
        $this->Ausdruck->_pdf->cell2(30,self::ZEILENHOEHESTANDARD, 'Seite '.$this->Ausdruck->_pdf->PageNo().' von {nb}', 'LBTR', 0, 'C', 0);

        $Zeile += 5;
        $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $this->Ausdruck->_pdf->cell2(60,self::ZEILENHOEHESTANDARD, 'Herausgabedatum: ' . $this->_Herausgabedatum, 'LTRB', 0, 'L', 0);
        $this->Ausdruck->_pdf->cell2(60,self::ZEILENHOEHESTANDARD, '', 'LTRB', 0, 'L', 0);
        $this->Ausdruck->_pdf->cell2(30,self::ZEILENHOEHESTANDARD, 'Version ' . $this->_Version, 'LBTR', 0, 'C', 0);


        if(!$this->_Seite1Ausgegeben){
            $Zeile += 10;
            $Spalte -= 30;
            $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);
            $this->Ausdruck->_pdf->SetFont($this->_Schriftart, '', 10);
            $this->Ausdruck->_pdf->cell2(12, 4, $this->_AWISSprachkonserven['IBG']['IBG_Filiale'] . ':', '', 0, 'L', 0);
            $this->Ausdruck->_pdf->cell2(40, 4, $this->_rsBericht->FeldInhalt('PEI_FIL_ID'), 'B', 0, 'L', 0);
            $this->Ausdruck->_pdf->cell2(30, 4, $this->_AWISSprachkonserven['IBG']['IBG_BesuchszeitVon'] . ':', '', 0, 'C', 0);
            $this->Ausdruck->_pdf->cell2(25, 4, $this->_rsBericht->FeldInhalt('ZEITVON'), 'B', 0, 'C', 0);
            $this->Ausdruck->_pdf->cell2(10, 4, $this->_AWISSprachkonserven['IBG']['IBG_BesuchszeitBis'] . ':', '', 0, 'C', 0);
            $this->Ausdruck->_pdf->cell2(25, 4, $this->_rsBericht->FeldInhalt('ZEITBIS'), 'B', 0, 'C', 0);
            $this->Ausdruck->_pdf->cell2(15, 4, $this->_AWISSprachkonserven['IBG']['IBG_Datum'] . ':', '', 0, 'C', 0);
            $this->Ausdruck->_pdf->cell2(22, 4, substr($this->_rsBericht->FeldInhalt('PEI_DATUM'), 0, 10), 'B', 0, 'C', 0);

            $Zeile += 10;
            $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);
            $this->Ausdruck->_pdf->SetFont($this->_Schriftart, '', 10);
            $this->Ausdruck->_pdf->cell2(10, 4, 'GBL:', '', 0, 'L', 0);
            $GBL = $this->rsAuswertung($this->_GBL_PBF_ID)->FeldInhalt('PEZ_WERT');
            $GBL = $GBL!=''?$GBL:str_repeat('_',23);
            $this->Ausdruck->_pdf->cell2(55, 4, $GBL , '', 0, 'L', 0);

            $GL = $this->rsAuswertung($this->_GL_PBF_ID)->FeldInhalt('PEZ_WERT');
            $GL = $GL!=''?$GL:str_repeat('_',23);
            $this->Ausdruck->_pdf->cell2(8, 4, 'FL:', '', 0, 'L', 0);
            $this->Ausdruck->_pdf->cell2(55, 4, $GL , '', 0, 'L', 0);


            $WL = $this->rsAuswertung($this->_WL_PBF_ID)->FeldInhalt('PEZ_WERT');
            $WL = $WL!=''?$WL:str_repeat('_',21);
            $this->Ausdruck->_pdf->cell2(8, 4, 'WL:', '', 0, 'L', 0);
            $this->Ausdruck->_pdf->cell2(55, 4, $WL , '', 0, 'L', 0);

        }

        $Zeile += 15;
        if($Kreuzfrage or !$this->_Seite1Ausgegeben){
            $this->Ausdruck->_pdf->setXY(self::LINKERRAND, $Zeile);
            $this->Ausdruck->_pdf->SetFont($this->_Schriftart, ($this->_AWISBenutzer->BenutzerSprache() != 'CZ' ? 'B' : ''), 10);
            $this->Ausdruck->_pdf->cell2(self::BREITEFRAGEN,self::ZEILENHOEHESTANDARD, '', '', 'L', 0);
            $this->Ausdruck->_pdf->SetFillColor(190, 190, 190);
            $this->Ausdruck->_pdf->cell2(self::BREITEOK,self::ZEILENHOEHESTANDARD,$this->_AWISSprachkonserven['CV']['CV_OK'], 'LTRB', 0, 'C', 1);
            $this->Ausdruck->_pdf->cell2(self::BREITENOK,self::ZEILENHOEHESTANDARD, $this->_AWISSprachkonserven['CV']['CV_NICHTOK'], 'LTRB', 0, 'C', 1);
        }else{
            $this->Ausdruck->_pdf->setY($Zeile);
        }

        $this->_Seite1Ausgegeben = true;
        $Zeile = $this->Ausdruck->_pdf->getY();
        $Zeile += self::ZEILENHOEHESTANDARD;
        $this->Ausdruck->_pdf->setY($Zeile);
        $this->_LetztesElement = 'header';
    }

    /**
     * @param $PBF_ID
     *
     * @return awisRecordset
     */
    public function rsAuswertung($PBF_ID){
        $SQL = 'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY=' . $this->_DB->WertSetzen('PEI','Z',$_GET['PEI_KEY']);
        $SQL .= ' AND PEZ_PBF_ID=' . $this->_DB->WertSetzen('PEI','Z',$PBF_ID);

        $rsAuswertung = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PEI'));

        return $rsAuswertung;
    }

    /**
     * @param $this->_Ausdruck awisAusdruck
     * @param $FragenText
     * @param $PBF_ID
     *
     * @throws awisException
     */
    public function ErstelleFrage($FragenText, $PBF_ID, $Kreuzfrage = true){


        $rsAuswertung = $this->rsAuswertung($PBF_ID);

        //Neue Seite notwendig?
        $ZeichenProZeile = 100;
        $LineFeedsImKommentar = substr_count($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),chr(10));
        $Ben�tigteHoehe = (strlen($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'))/$ZeichenProZeile ) + $LineFeedsImKommentar;
        $Ben�tigteHoehe = $Ben�tigteHoehe * self::ZEILENHOEHESTANDARD ;
        if($this->Ausdruck->_pdf->getY() + $Ben�tigteHoehe >= 280){
            $this->NeueSeite($Kreuzfrage);
        }

        //Blockfrage
        if($PBF_ID == 0){
            $Spalte = self::LINKERRAND;
            $Zeile = $this->Ausdruck->_pdf->getY();

            if($this->_Seite1Ausgegeben and $this->_LetztesElement != 'header'){
                $Zeile += self::ABSTANDBLOCKFRAGE;
            }

            $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);
            $this->Ausdruck->_pdf->SetFont($this->_Schriftart, 'B', 10);
            $this->Ausdruck->_pdf->SetFillColor(190, 190, 190);
            $this->Ausdruck->_pdf->cell2(self::BREITEFRAGEN + self::BREITEOK + self::BREITENOK,self::ZEILENHOEHESTANDARD, $FragenText, 'LTRB', 0, 'L', 1);

            $this->Ausdruck->_pdf->setY($this->Ausdruck->_pdf->getY()+ self::ZEILENHOEHESTANDARD);

            return true;
        }
        $this->Ausdruck->_pdf->SetFont($this->_Schriftart, '', 10);
        //Frage andrucken
        $Zeile = $this->Ausdruck->_pdf->getY();
        $this->Ausdruck->_pdf->setXY(self::LINKERRAND, $Zeile);
        $YVorher = $this->Ausdruck->_pdf->getY();

        if($Kreuzfrage){
            $BreiteFragen = self::BREITEFRAGEN;
        }else{
            $BreiteFragen = self::BREITEFRAGEN + self::BREITEOK + self::BREITENOK;
        }

        $this->Ausdruck->_pdf->MultiCell($BreiteFragen, self::ZEILENHOEHESTANDARD, $FragenText, 'LTRB', 'L', 0);

        $YNacher = $this->Ausdruck->_pdf->getY();
        $ZeilenHoeheFrage = $YNacher - $YVorher;

        $Spalte = self::LINKERRAND + $BreiteFragen;
        $this->Ausdruck->_pdf->setXY($Spalte, $Zeile);

        if($Kreuzfrage){
            //OK
            //Mitte des OK Kasten suchen und die halbe gr��e des Ankreuzk�stchens abziehen = Beginn des Ankreuzkaestchen
            $XKaestechen = $this->Ausdruck->_pdf->getX() + ((self::BREITEOK/2) - (self::KAESTCHENGROESSE/2));
            $YKaestechen = $this->Ausdruck->_pdf->getY() + (($ZeilenHoeheFrage/2) - (self::KAESTCHENGROESSE/2));
            $this->Ausdruck->_pdf->cell2(self::BREITEOK, $ZeilenHoeheFrage, $this->Ausdruck->_pdf->Rect($XKaestechen, $YKaestechen, self::KAESTCHENGROESSE, self::KAESTCHENGROESSE), 'LTRB', 'C', 1);

            //Kreuz machen?
            if ($rsAuswertung->FeldInhalt('PEZ_WERT') == '1') {
                $this->Ausdruck->_pdf->SetLineWidth(1);

                $this->Ausdruck->_pdf->setXY($XKaestechen, $YKaestechen);

                //Kreuzstrich linksoben nach rechts unten
                $x1 = $this->Ausdruck->_pdf->getX() + self::KAESTCHENGROESSE;
                $y1 = $this->Ausdruck->_pdf->getY() + self::KAESTCHENGROESSE;
                $x2 = $this->Ausdruck->_pdf->getX();
                $y2 = $this->Ausdruck->_pdf->getY();
                $this->Ausdruck->_pdf->Line($x1 , $y1, $x2 , $y2);

                //Kreuzstrich linksunten nach rechtsoben
                $y1 = $y1 - self::KAESTCHENGROESSE;
                $y2 = $y2 + self::KAESTCHENGROESSE;
                $this->Ausdruck->_pdf->Line($x1 , $y1, $x2 , $y2);

                $this->Ausdruck->_pdf->SetLineWidth(0);
            }

            //XY zm oberen Rechten Eck des OK-Kastens setzen
            $Y = $YNacher - $ZeilenHoeheFrage;
            $X = self::LINKERRAND + self::BREITEFRAGEN + self::BREITEOK;
            $this->Ausdruck->_pdf->setXY($X, $Y);

            //NOK
            //Mitte des NOK Kasten suchen und die halbe gr��e des Ankreuzk�stchens abziehen = Beginn des Ankreuzkaestchen
            $XKaestechen = $this->Ausdruck->_pdf->getX() + ((self::BREITENOK/2) - (self::KAESTCHENGROESSE/2));
            $YKaestechen = $this->Ausdruck->_pdf->getY() + (($ZeilenHoeheFrage/2) - (self::KAESTCHENGROESSE/2));
            $this->Ausdruck->_pdf->cell2(self::BREITENOK, $ZeilenHoeheFrage, $this->Ausdruck->_pdf->Rect($XKaestechen, $YKaestechen, self::KAESTCHENGROESSE, self::KAESTCHENGROESSE), 'LTRB', 'C', 1);

            //Kreuz machen?
            if ($rsAuswertung->FeldInhalt('PEZ_WERT') == '0') {
                $this->Ausdruck->_pdf->SetLineWidth(1);

                $this->Ausdruck->_pdf->setXY($XKaestechen, $YKaestechen);

                //Kreuzstrich linksoben nach rechts unten
                $x1 = $this->Ausdruck->_pdf->getX() + self::KAESTCHENGROESSE;
                $y1 = $this->Ausdruck->_pdf->getY() + self::KAESTCHENGROESSE;
                $x2 = $this->Ausdruck->_pdf->getX();
                $y2 = $this->Ausdruck->_pdf->getY();
                $this->Ausdruck->_pdf->Line($x1 , $y1, $x2 , $y2);

                //Kreuzstrich linksunten nach rechtsoben
                $y1 = $y1 - self::KAESTCHENGROESSE;
                $y2 = $y2 + self::KAESTCHENGROESSE;
                $this->Ausdruck->_pdf->Line($x1 , $y1, $x2 , $y2);

                $this->Ausdruck->_pdf->SetLineWidth(0);
            }
        }


        $this->Ausdruck->_pdf->setXY(self::LINKERRAND, $YNacher);
        //Blanko oder Bemerkung da?
        if($rsAuswertung->FeldInhalt('PEZ_KEY') == '' or $rsAuswertung->FeldInhalt('PEZ_BEMERKUNG')!='' or ( !$Kreuzfrage and $rsAuswertung->FeldInhalt('PEZ_WERT') != '') ){
            $BreiteBemerkung = self::BREITEFRAGEN + self::BREITEOK + self::BREITENOK;
            $Text = $Kreuzfrage?$rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'):$rsAuswertung->FeldInhalt('PEZ_WERT');
            $this->Ausdruck->_pdf->MultiCell($BreiteBemerkung,self::ZEILENHOEHESTANDARD, $Text, 'LTRB', 'L', 0);

        }
        $this->_LetztesElement = 'frage';
        return true;
    }

    /**
     * Erstellt eine Neue Seite
     */
    public function NeueSeite($Kreuzfrage = false){
        if($this->_Seite1Ausgegeben){
            $this->ErstelleFooter();
        }
        $this->Ausdruck->NeueSeite(0, 1);
        $this->ErstelleHeader($Kreuzfrage);

    }

    /**
     * Gibt die PDF aus
     */
    public function Anzeigen(){
        $this->ErstelleFooter();
        //Platzhalter Seitenzahl f�llen
        $this->Ausdruck->_pdf->AliasNbPages('{nb}');
        $this->Ausdruck->Anzeigen();
    }

}