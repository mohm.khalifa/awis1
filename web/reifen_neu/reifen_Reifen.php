<?php

/**
 * Anzeigeseite der Reifen
 * ************************************************************************
 * Parameter :
 * --- keine ---
 * R�ckgabeparameter:
 * --- keine ---
 * ***********************************************************************
 * Erstellt:
 * 28.06.2011 Stefan Oppl
 * �nderungen:
 * dd.mm.yyyy XX :
 * ***********************************************************************
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('RRP', '%');
    $TextKonserven[] = array('RRA', '%');
    $TextKonserven[] = array('RRT', '%');
    $TextKonserven[] = array('SPI', '%');
    $TextKonserven[] = array('REH', '%');
    $TextKonserven[] = array('RET', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');

    // Ben�tigte Resourcen �ffnen
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    // Berechtigungen Abfragen
    $Rechte400 = $AWISBenutzer->HatDasRecht(400);  // Artikelstamm
    $Recht253 = $AWISBenutzer->HatDasRecht(253);  // Artikelstamm
    $Recht15100 = $AWISBenutzer->HatDasRecht(15100);
    if ($Recht15100 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Form->DebugAusgabe(1, $_POST);

    // Parameter�bergabe aus Suchformular �ber POST
    if (isset($_POST['cmdSuche_x']))
    {
        $Param['BREITE'] = $_POST['sucBREITE'];
        $Param['QUERSCHNITT'] = $_POST['sucQUERSCHNITT'];
        $Param['INNENDURCHMESSER'] = $_POST['sucINNENDURCHMESSER'];
        $Param['LLKW'] = $_POST['sucLLKW'];
        $Param['LOADINDEX'] = $_POST['sucLOADINDEX'];
        $Param['LOADINDEX2'] = $_POST['sucLOADINDEX2'];
        $Param['SPI_ID'] = $_POST['sucSPI_ID'];
        //$Param['MAXVK']=$_POST['sucMaxVK'];
        $Param['ATUNR'] = $_POST['sucATUNR'];
        $Param['REHKEY'] = $_POST['sucREH_KEY'];
        $Param['BEZEICHNUNG'] = $_POST['sucBezeichnung'];
        $Param['RETID'] = $_POST['sucRET_ID'];
        $Param['CCSORT'] = (isset($_POST['txtCCSort']) ? $_POST['txtCCSort'] : '');
        $Param['HSPEED'] = (isset($_POST['txtHSpeed']) ? $_POST['txtHSpeed'] : '');
        $Param['HLOAD'] = (isset($_POST['txtHLoad']) ? $_POST['txtHLoad'] : '');
        $Param['KBPREIS'] = (isset($_POST['txtKBPreise']) ? $_POST['txtKBPreise'] : '');
        $Param['KBLISTE'] = (isset($_POST['txtKBListe']) ? $_POST['txtKBListe'] : '');
        $Param['SORT'] = $_POST['sucSortierung'];
        $Param['LAND'] = $_POST['sucLaender'];
        $Param['FIL_ID'] = $_POST['sucFIL_ID'];

        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern']) ? 'on' : '');

        $Param['ORDERBY'] = '';
        $Param['BLOCK'] = 1;
        $Param['KEY'] = 0;

        // Suchparameter speichern
    }
    else   // Letzten Benutzer suchen
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen("RRESuche"));

        //Pr�fen, ob Filiale angemeldet ist
        $UserFilialen = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
        if ($UserFilialen != '')
        {
            $Param['FIL_ID'] = $UserFilialen;
        }
    }

    $Form->DebugAusgabe(1, $Param);
    
    //**************************************************************************
    // LAND der Filiale ermitteln
    //**************************************************************************
    if ($Param['FIL_ID'] != '')
    {
        //Land zu der eingegebenen Filiale ermitteln
        $SQL = 'SELECT LAN_CODE';
        $SQL.= ' FROM FILIALEN';
        $SQL.= ' INNER JOIN LAENDER ON LAN_WWSKENN = FIL_LAN_WWSKENN';
        $SQL.= ' WHERE FIL_ID = ' . $Param['FIL_ID'];

        $rsLAN = $DB->RecordSetOeffnen($SQL);

        if ($rsLAN->AnzahlDatensaetze() == 0)
        {
            echo 'Ung�ltige Filiale!';
            die();
        }
        else
        {
            $LAND = $rsLAN->FeldInhalt('LAN_CODE');

            if ($Param['LAND'] != $LAND)
            {
                $Param['LAND'] = $LAND;
            }
        }
    }

    //**************************************************************************

    $FarbCodes = $AWISBenutzer->ParameterLesen('ReifenFarbCodes');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen("BildschirmBreite");

    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (isset($Param['SORT']) AND $Param['SORT'] != '')
    {
        $ORDERBY = '';

        $SortFelder = array('RRE_AST_ATUNR', 'DUMMY SPEEDINDEX', 'REH_BEZEICHNUNG', 'VK');
        
        if (isset($Param['CCSORT']) AND $Param['CCSORT'] != '')
        {
            if ($Param['SORT'] != '1') //Alle bis auf SpeedIndex
            {
                $ORDERBY.= " RRE_AusLauf DESC, SPI_ID, ";
                $ORDERBY.= $SortFelder[$Param['SORT']];
            }
            else //Bei SpeedIndex spezielle Sortierung
            {
                $ORDERBY.= " RRE_AusLauf DESC, SPI_ID, RRE_AST_ATUNR";
            }
        }
        else
        {
            if ($Param['SORT'] != '1') //Alle bis auf SpeedIndex
            {
                $ORDERBY.= $SortFelder[$Param['SORT']];
            }
            else
            {
                $ORDERBY.= " SPI_ID, RRE_AST_ATUNR";
            }
        }
    }

    $SQL = 'SELECT a.*';
    if ($AWIS_KEY1 <= 0)
    {
        $SQL .= ', row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr';
    }
    $SQL .= ' FROM ';
    $SQL .='(';
    $SQL .= " Select Rre_RRP_Key, Rre_Ast_Atunr, Rre_Reh_Key, Rre_Breite, Rre_Querschnitt, Rre_Bauart, Rre_Innendurchmesser,";
    $SQL .= " Rre_Gepflegt, Rre_Ungelistet, Rre_Status, Llkw, BEZEICHNUNG, MEHRFACHKENNUNG, ";
    $SQL .= " Rf, Loadindex, Loadindex2, Loadindex3, Speedindex, Speedindex2, Zusatzbemerkung, Zusatzbemerkung2, Zusatzbemerkung3, ";
    $SQL .= " Reifentyp1,Reifentyp2, Reifentyp3,Seal,Rof,Es,Gws,Rx,VKVORBEREITUNG,Ast_Kennung,AST_KENN2 ";
    //$SQL .= " Ast_Vk,Ast_Vkat,Ast_Vkcz,Ast_Vknl,Ast_Vkit, Ast_Vkch ";

    if (isset($Param['FIL_ID']) and $Param['FIL_ID'] != '')
    {
        if ($Param['LAND'] != 'DE')
        {
            if ($Param['LAND'] == 'IT' OR $Param['LAND'] == 'NL' OR $Param['LAND'] == 'AT')
            {
                if (isset($Param['KBPREIS']) and $Param['KBPREIS'] == 'on')
                {
                    $SQL.= ', Rre_Ungelistetkbpreis as VK';
                }
                else
                {
                    $SQL.= ' , case when rre_ungelistet = 1 then rre_ungelistetvk ';
                    $SQL.=' else nvl(fib_vk, AST_VK' . $Param['LAND'] . ') end as VK';
                }
            }
            else //Nicht Euro - L�nder
            {
                if (isset($Param['KBPREIS']) and $Param['KBPREIS'] == 'on')
                {
                    $SQL.= ', null as VK'; //KB-Preis nur in Euro verf�gbar
                }
                else
                {
                    $SQL.= ' , case when rre_ungelistet = 1 then null ';
                    $SQL.=' else nvl(fib_vk, AST_VK' . $Param['LAND'] . ') end as VK';
                }
            }
        }
        else
        {
            if (isset($Param['KBPREIS']) and $Param['KBPREIS'] == 'on')
            {
                $SQL.= ', Rre_Ungelistetkbpreis as VK';
            }
            else
            {
                $SQL.= ' , case when rre_ungelistet = 1 then rre_ungelistetvk ';
                $SQL.=' else nvl(fib_vk, AST_VK) end as VK';
            }
        }
    }
    else
    {
        if ($Param['LAND'] != 'DE')
        {
            if ($Param['LAND'] == 'IT' OR $Param['LAND'] == 'NL' OR $Param['LAND'] == 'AT')
            {
                if (isset($Param['KBPREIS']) and $Param['KBPREIS'] == 'on')
                {
                    $SQL.= ', Rre_Ungelistetkbpreis as VK';
                }
                else
                {
                    $SQL.= ' , case when rre_ungelistet = 1 then rre_ungelistetvk ';
                    $SQL.=' else AST_VK' . $Param['LAND'] . ' end as VK';
                }
            }
            else //Nicht Euro-L�nder
            {
                if (isset($Param['KBPREIS']) and $Param['KBPREIS'] == 'on')
                {
                    $SQL.= ', null as VK'; //KB-Preis nur in Euro verf�gbar
                }
                else
                {
                    $SQL.= ' , case when rre_ungelistet = 1 then null ';
                    $SQL.=' else AST_VK' . $Param['LAND'] . ' end as VK';
                }
            }
        }
        else
        {
            if (isset($Param['KBPREIS']) and $Param['KBPREIS'] == 'on')
            {
                $SQL.= ', Rre_Ungelistetkbpreis as VK';
            }
            else
            {
                $SQL.= ' , case when rre_ungelistet = 1 then rre_ungelistetvk ';
                $SQL.=' else AST_VK end as VK';
            }
        }
    }

    $SQL .= " ,Ast_Brutto, Ast_Aktiv_De, Ast_Aktiv_At, Ast_Aktiv_Cz, Ast_Aktiv_Nl, Ast_Aktiv_It, Ast_Aktiv_Ch, Ast_Light_De,";
    $SQL .= " Ast_Light_At, Ast_Light_Cz, Ast_Light_Nl, Ast_Light_It, Ast_Light_Ch, AST_SUCH1, AST_SUCH2, RRE_AUSLAUF, AST_WGR_ID, AST_WUG_ID,";
    $SQL .= " rra_aktionprozent, rra_aktiontext, spi_id";

    if (isset($_GET['cmdAktion']) AND $_GET['cmdAktion'] == 'ReifenKB')
    {
        $SQL .= ' FROM V_REIFEN_SONDERBESTELLUNG';
    }
    else
    {
        $SQL .= ' FROM V_REIFEN';
    }
    if (isset($Param['FIL_ID']) and $Param['FIL_ID'] != '')
    {
        $SQL .= ' LEFT JOIN FILIALBESTAND ON FIB_AST_ATUNR = RRE_AST_ATUNR AND FIB_FIL_ID = ' . $Param['FIL_ID'];
    }
    $SQL .= ' LEFT JOIN SPEEDINDEX ON SPI_SYMBOL = SPEEDINDEX';
    $SQL .= ' LEFT JOIN V_ARTIKELSTAMMWGR ON AST_ATUNR = RRE_AST_ATUNR';

    //Aktionspreis	(Warenuntergruppe???????)
    $SQL.= ' LEFT JOIN REIREIFENAKTIONEN ';
    $SQL.= ' ON ((RRA_RRE_RRP_KEY = RRE_RRP_KEY)';
    $SQL.= ' OR (RRA_WGR_ID = AST_WGR_ID)'; // AND RRA_WUG_ID = AST_WUG_ID)';
    $SQL.= ' OR (RRA_KENN1 = AST_KENNUNG)';
    $SQL.= ' OR (RRA_KENN2 = AST_KENN2)';
    $SQL.= ' OR (RRA_REH_KEY = RRE_REH_KEY))';
    // G�ltigkeiten pr�fen
    $SQL.= ' AND RRA_GUELTIGAB <= SYSDATE AND RRA_GUELTIGBIS >= SYSDATE';
    $SQL.= ' AND RRA_LAND = \'' . $Param['LAND'] . '\'';

    if ($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $SQL .=') a';

    $Form->DebugAusgabe(1, $SQL);

    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    /*
    $Block = 1;
    if (isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    }
    elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
    {
        $Block = intval($Param['BLOCK']);
    }
    */
    //$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    //$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    //if ($AWIS_KEY1 <= 0)
    //{
        //$SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    //}

    $rsFelder = $DB->RecordSetOeffnen($SQL);

    // Min und Max-Preis ermitteln und hier ausgeben
    // Irgendwie aus RecordSet ermitteln --> erneutes Aufrufen des
    // SQL-Statements d�rfte zu Performance-Problemen f�hren

    //*****************************************************************
    // �berschrift gefundene Hersteller anzeigen
    //*****************************************************************
    // Wird nur angezeigt wenn mehr als 1 DS angezeigt werden soll
    If($AWIS_KEY1 <= 0)
    {
        // Max und Min VK ermitteln
        $arrVK = array();
        $i = 0;
        while (!$rsFelder->EOF())
        {
            $arrVK[$i] = $rsFelder->FeldInhalt('VK');
            $i++;
            $rsFelder->DSWeiter();
        }

        // Sortiert die VK�s aufsteigend der minVK ist Element 0
        sort($arrVK);

        $minVK = $arrVK[0];

        // Sortiert die VK�s absteigend der maxVK ist Element 0
        rsort($arrVK);

        $maxVK = $arrVK[0];

        echo 'Aktions-VK von '.$minVK.' Euro bis '.$maxVK.' Euro';

        $rsFelder->DSErster();


        // alle Reifenhersteller in Array einlesen (Array-Index = REH_KEY)
        $SQL = 'SELECT * FROM reireifenhersteller';
        $rsREH = $DB->RecordSetOeffnen($SQL);

        $arrREH = array();

        while (!$rsREH->EOF())
        {
            $arrREH[$rsREH->FeldInhalt('REH_KEY')] = $rsREH->FeldInhalt('REH_BEZEICHNUNG');
            $rsREH->DSWeiter();
        }
        //var_dump($arrREH);
    }
    // Hersteller ausgeben --> Daten in ein Array
    // einlesen und irgendwie gruppieren
    // anschlie�end hier ausgeben!!!!
    /*
    $arrHersteller = array();

    for($i=0;$i<$rsReifenAnz;$i++)
    {
        if($rsReifen["REI_HERSTELLER"][$i]=="RUNDERNEUERT")
        {
            $rsReifen["REI_HERSTELLER"][$i] = "<i>" . $rsReifen["REI_HERSTELLER"][$i] . "</i>";
        }

        if(in_array($rsReifen["REI_HERSTELLER"][$i],$Hersteller)==FALSE)
        {
            array_push($Hersteller,$rsReifen["REI_HERSTELLER"][$i]);
        }
    }
    array_multisort($Hersteller);
    print "<b>" . implode(", ",$Hersteller) . "</b><br>";
    */
    // *************************************************************************************
    // Ende Herstellerausgabe (Kopf)
    // *************************************************************************************

    $Param['ORDERBY'] = $ORDERBY;
    $Param['KEY'] = $AWIS_KEY1;
    //$Param['BLOCK'] = $Block;
    $AWISBenutzer->ParameterSchreiben("RRPSuche", serialize($Param));


    $Form->SchreibeHTMLCode('<form name=frmReifen action=./reifen_Main.php?cmdAktion=' . (isset($_GET['cmdAktion']) ? $_GET['cmdAktion'] : 'Reifen') . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . '' . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . ' method=post>');


    //********************************************************
    // Daten anzeigen
    //********************************************************
    if ($rsFelder->EOF() AND !isset($_POST['cmdDSNeu_x']))  // Keine Meldung bei neuen Datens�tzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
    }
    elseif (($rsFelder->AnzahlDatensaetze() > 0) or (isset($_GET['Liste'])))      // Liste anzeigen
    {
        $Form->Formular_Start();

        $EditRecht = true;

        $BestandsFIL_ID = '';
        if (isset($_POST['txtFIL_ID']))
        {
            $BestandsFIL_ID = $_POST['txtFIL_ID'];
            $AWISBenutzer->ParameterSchreiben("Reifen-Bestandsfilialen_neu", $_POST['txtFIL_ID']);
            //$AWISBenutzer->ParameterSchreiben("Reifen-Bestandsfilialen_neu",serialize($_POST['txtFIL_ID']));
        }
        else
        {
            $BestandsFIL_ID = $AWISBenutzer->ParameterLesen("Reifen-Bestandsfilialen_neu");
            //$BestandsFIL_ID = unserialize($AWISBenutzer->ParameterLesen("Reifen-Bestandsfilialen_neu"));
        }

        if ($BestandsFIL_ID == '0')
        {
            $BestandsFIL_ID = '';
        }

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel('Vortagesbest�nde anzeigen f�r folgende Filialen:', 400);
        $Form->Erstelle_TextFeld('FIL_ID', $BestandsFIL_ID, 20, 120, $EditRecht);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $SpBreiteFaktor = $BildschirmBreite / 1024;
        $SpBreiteTabelle = (1024 * $SpBreiteFaktor) - 50;
        $SpBreiteArtnr = 80 * $SpBreiteFaktor;
        $SpBreiteHerst = 80 * $SpBreiteFaktor;
        $SpBreiteRabatt = 60;
        $SpBreiteBez = 165;

        $Form->ZeileStart();

        $Form->SchreibeHTMLCode("<table width=" . $SpBreiteTabelle . " cellspacing=0 cellpadding=0 border=1 >");
        $Form->SchreibeHTMLCode("<tr><td id=FeldBez width=" . $SpBreiteArtnr . ">" . $AWISSprachKonserven['RRP']['RRP_AST_ATUNR'] . "</td>");
        $Form->SchreibeHTMLCode("<td id=FeldBez colspan=9 width=360>" . $AWISSprachKonserven['RRP']['RRP_REIFENKENNZEICHEN'] . "</td>");
        $Form->SchreibeHTMLCode("<td id=FeldBez width=" . $SpBreiteBez . ">" . $AWISSprachKonserven['RRP']['RRP_BEZEICHNUNG'] . "</td>");
        $Form->SchreibeHTMLCode("<td id=FeldBez width=20 title='Kennung'>" . $AWISSprachKonserven['RRP']['RRP_KENNL'] . "</td>");
        //if(($ReifenRechte&1024)==1024)
        //{
        $Form->SchreibeHTMLCode("<td id=FeldBez width=20 title='Auslaufartikel-Kennzeichen'>" . $AWISSprachKonserven['RRP']['RRP_KENNL2'] . "</td>");
        //}
        if (isset($Param['KBPREIS']) and $Param['KBPREIS'] == 'on')
        {
            $Form->SchreibeHTMLCode("<td id=FeldBez width=50 align=right>" . $AWISSprachKonserven['RRP']['RRP_KBPREIS']);
        }
        else
        {
            $Form->SchreibeHTMLCode("<td id=FeldBez width=50 align=right>" . $AWISSprachKonserven['RRP']['RRP_VK'] . '_' . $Param['LAND']);
        }

        //echo "<img width=20 height=15 src=/bilder/Flagge_" . $Params[18] . ".gif title='Preise f�r " . $Params[18] . "'></td>";
        $Form->SchreibeHTMLCode("<td id=FeldBezBold align=center width=" . $SpBreiteRabatt . " title='Rabatt in %'>%</td>");
        $Form->SchreibeHTMLCode("<td id=FeldBez width=50>" . $AWISSprachKonserven['RRA']['RRA_AKTION'] . "</td>");
        $Form->SchreibeHTMLCode("<td id=FeldBez width=30></td>");
        $Form->SchreibeHTMLCode("</tr>");


        /*
          $Form->ZeileStart('font-size:10pt');
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_AST_ATUNR'],60,'','');
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_REIFENKENNZEICHEN'],360,'','');
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_BEZEICHNUNG'],140,'','');
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_KENNL'],25,'','');
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_KENNL2'],25,'','');
          if (isset($Param['KBPREIS']) and $Param['KBPREIS']=='on')
          {
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_KBPREIS'],50,'','');
          }
          else
          {
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_VK'].'_'.$Param['LAND'],50,'','');
          }

          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_AKTIONPROZENT'],50,'','');
          $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_AKTION'],50,'','');
         */

        //$Form->ZeileEnde();

        $SEPZeile = 0;
        $AuslaufBlock = false;
        $LetzterSpeedIndex = "";

        while (!$rsFelder->EOF())
        {
            $FilialenListe = array();
            $ToolTipp = '';
            $ToolTipp = 'Best�nde in ';

            $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
            $SQL .= " WHERE ASI_AST_ATUNR = '" . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . "'";
            $SQL .= ' AND ASI_AIT_ID = 10';

            $rsBestande = $DB->RecordSetOeffnen($SQL);
            $Bestand = $rsBestande->FeldInhalt('ASI_WERT');

            $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
            $SQL .= " WHERE ASI_AST_ATUNR = '" . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . "'";
            $SQL .= ' AND ASI_AIT_ID = 13';

            $rsBestande = $DB->RecordSetOeffnen($SQL);

            if ($rsBestande->AnzahlDatensaetze() == 1)
            {
                $Bestand -= $rsBestande->FeldInhalt('ASI_WERT');
            }

            $ToolTipp .= 'Weiden: ' . $Bestand;

            $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
            $SQL .= " WHERE ASI_AST_ATUNR = '" . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . "'";
            $SQL .= ' AND ASI_AIT_ID = 11';

            $rsBestande = $DB->RecordSetOeffnen($SQL);
            $Bestand = $rsBestande->FeldInhalt('ASI_WERT');

            $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
            $SQL .= " WHERE ASI_AST_ATUNR = '" . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . "'";
            $SQL .= ' AND ASI_AIT_ID = 14';

            $rsBestande = $DB->RecordSetOeffnen($SQL);
            if ($rsBestande->AnzahlDatensaetze() == 1)
            {
                $Bestand -= $rsBestande->FeldInhalt('ASI_WERT');
            }

            $ToolTipp .= ' / Werl: ' . $Bestand;

            if ($BestandsFIL_ID != '')
            {
                $BestandsFIL_ID = str_replace(';', ',', $BestandsFIL_ID);
                $BestandsFIL_ID = str_replace('/', ',', $BestandsFIL_ID);
                $BestandsFIL_ID = str_replace('.', ',', $BestandsFIL_ID);
                $FilListe = explode(',', $BestandsFIL_ID);

                foreach ($FilListe AS $FilID)
                {
                    if ($FilID != 0)
                    {
                        $FilialenListe[] = (int) ($FilID);
                    }
                }

                if (!empty($FilialenListe))
                {
                    $SQL = 'SELECT COALESCE(SUM(FIB_BESTAND),0) as Bestand, FIL_ID';
                    $SQL .= " FROM FILIALEN ";
                    $SQL .= " LEFT OUTER JOIN FilialBestand ON FIL_ID = FIB_FIL_ID AND FIB_AST_ATUNR = '" . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . "'";
                    $SQL .= " WHERE ";
                    $SQL .= " FIL_ID IN (" . implode(',', $FilialenListe) . ')';
                    $SQL .= " GROUP BY FIL_ID ";
                    $SQL .= " ORDER BY FIL_ID ";

                    $rsBestande = $DB->RecordSetOeffnen($SQL);

                    if ($rsBestande->AnzahlDatensaetze() > 0)
                    {
                        $ToolTipp .= ', Vortagsbest�nde in ';
                    }

                    $FilialenToolTipp = '';

                    for ($Bestand = 0; $Bestand < $rsBestande->AnzahlDatensaetze(); $Bestand++)
                    {
                        $FilialenToolTipp .= ' / Filiale ' . $rsBestande->FeldInhalt('FIL_ID') . ': ' . $rsBestande->FeldInhalt('BESTAND');
                        $rsBestande->DSWeiter();
                    }
                    $ToolTipp .= substr($FilialenToolTipp, 3);
                }
            }

            //$Form->ZeileStart('font-size:10pt');

            if ($Param['CCSORT'] == 'on')  // Nur f�r CallCenter Blockbildung
            {
                if ($rsFelder->FeldInhalt('RRE_AUSLAUF') == 'A' and !$AuslaufBlock)
                {
                    //$Form->ZeileStart();
                    $Form->SchreibeHTMLCode("<tr><td id=UeberschriftKlein colspan=18 align=center>Auslaufartikel</td></tr>");
                    //$Form->Erstelle_Liste_Ueberschrift('Auslaufartikel',710,'background-color : #808080;');
                    //$Form->ZeileEnde();
                    $AuslaufBlock = True;
                }

                // Anzeige f�r Speedindex
                if ($LetzterSpeedIndex != $rsFelder->FeldInhalt('SPEEDINDEX'))
                {
                    //$Form->ZeileStart();
                    $Form->SchreibeHTMLCode("<tr><td id=UeberschriftKlein colspan=18 align=center>Speedindex: " . $rsFelder->FeldInhalt('SPEEDINDEX') . "</td></tr>");
                    //$Form->Erstelle_Liste_Ueberschrift('Speedindex '.$rsRRP->FeldInhalt('SPEEDINDEX'),710,'background-color : #808080;');
                    //$Form->ZeileEnde();
                    $LetzterSpeedIndex = $rsFelder->FeldInhalt('SPEEDINDEX');
                }
            }


            $Code = '';
            //if(strchr($FarbCodes,"~" . $rsReifen["REI_BESTELLKZ"][$i]))
            if (strchr($FarbCodes, "~N"))
            {
                //$Code = strstr(strstr($FarbCodes,$rsReifen["REI_BESTELLKZ"][$i]),":");
                //$Code = substr($Code,strchr($Code,":")+1,6);


                $Code = strstr(strstr($FarbCodes, 'N'), ":");
                $Code = substr($Code, strchr($Code, ":") + 1, 6);
            }

            $Link = '';
            if (($Rechte400 & 256) != 256 and $rsFelder->FeldInhalt('AST_SUCH2') != '')
            {
                $Link = '';
            }
            else
            {
                if ($Rechte400 >= 0)
                {
                    $Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EKAT&AST_ATUNR=' . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . '';
                }
            }

            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . ">" . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=left width=33>" . $rsFelder->FeldInhalt('RRE_BREITE') . "</td>");

            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=left width=20>&nbsp;" . ($rsFelder->FeldInhalt("RRE_QUERSCHNITT") != "" ? $rsFelder->FeldInhalt("RRE_QUERSCHNITT") : "&nbsp;") . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsFelder->FeldInhalt('RRE_BAUART') . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=20>&nbsp;" . $rsFelder->FeldInhalt('RRE_INNENDURCHMESSER') . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=15>&nbsp;" . $rsFelder->FeldInhalt('LLKW') . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsFelder->FeldInhalt('LOADINDEX') . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsFelder->FeldInhalt('LOADINDEX2') . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right width=25>&nbsp;" . $rsFelder->FeldInhalt('SPEEDINDEX') . "</td>");

            /*
              $Form->Erstelle_ListenFeld('RRP_AST_ATUNR',$rsRRP->FeldInhalt('RRE_AST_ATUNR'),0,60,false,($SEPZeile%2),($Code!=''?'color:'.$Code:''),$Link,'T','',$ToolTipp);
              $Form->Erstelle_ListenFeld('RRP_BREITE',$rsRRP->FeldInhalt('RRE_BREITE'),0,30,false,($SEPZeile%2),'','');
              $Form->Erstelle_ListenFeld('RRP_QUERSCHNITT',$rsRRP->FeldInhalt('RRE_QUERSCHNITT'),0,25,false,($SEPZeile%2),'','','NO','L','');
              $Form->Erstelle_ListenFeld('RRP_BAUART',$rsRRP->FeldInhalt('RRE_BAUART'),0,25,false,($SEPZeile%2),'','','T','L','');
              $Form->Erstelle_ListenFeld('RRP_INNENDURCHMESSER',$rsRRP->FeldInhalt('RRE_INNENDURCHMESSER'),0,25,false,($SEPZeile%2),'','','','','');
              $Form->Erstelle_ListenFeld('RRP_LLKW',$rsRRP->FeldInhalt('LLKW'),0,25,false,($SEPZeile%2),'','','','','');
              $Form->Erstelle_ListenFeld('RRP_LOADINDEX',$rsRRP->FeldInhalt('LOADINDEX'),0,25,false,($SEPZeile%2),'','','','','');
              $Form->Erstelle_ListenFeld('RRP_LOADINDEX2',$rsRRP->FeldInhalt('LOADINDEX2'),0,25,false,($SEPZeile%2),'','','','','');
              $Form->Erstelle_ListenFeld('RRP_SPEEDINDEX',$rsRRP->FeldInhalt('SPEEDINDEX'),0,25,false,($SEPZeile%2),'','','','','');
             */
            $Anzeige = '';

            if ($rsFelder->FeldInhalt("RRE_LOADINDEX3") != '' or $rsFelder->FeldInhalt('SPEEDINDEX2') != '' or $rsFelder->FeldInhalt('RF') != '' or $rsFelder->FeldInhalt('ZUSATZBEMERKUNG2') != '' or $rsFelder->FeldInhalt('ZUSATZBEMERKUNG3') != '' or $rsFelder->FeldInhalt('ZUSATZBEMERKUNG') != '')
            {
                if ($rsFelder->FeldInhalt('LOADINDEX3') != '')
                {
                    $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsFelder->FeldInhalt('LOADINDEX3'));
                }

                if ($rsFelder->FeldInhalt('SPEEDINDEX2') != '')
                {
                    if ($rsFelder->FeldInhalt('LOADINDEX3') != '')
                    {
                        $Form->SchreibeHTMLCode("/" . $rsFelder->FeldInhalt('SPEEDINDEX2'));
                    }
                    else
                    {
                        $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsFelder->FeldInhalt('SPEEDINDEX2'));
                    }
                }

                if ($rsFelder->FeldInhalt('RF') != '')
                {
                    if ($rsFelder->FeldInhalt("RRE_LOADINDEX3") != '' or $rsFelder->FeldInhalt('SPEEDINDEX2') != '')
                    {
                        $Form->SchreibeHTMLCode("/<b>" . $rsFelder->FeldInhalt('RF') . "</b>");
                    }
                    else
                    {
                        $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;<b>" . $rsFelder->FeldInhalt('RF') . "</b>");
                    }
                }

                if ($rsFelder->FeldInhalt('ZUSATZBEMERKUNG2') != '')
                {
                    if ($rsFelder->FeldInhalt("RRE_LOADINDEX3") != '' or $rsFelder->FeldInhalt('SPEEDINDEX2') != '' or $rsFelder->FeldInhalt('RF') != '')
                    {
                        $Form->SchreibeHTMLCode("/<b>" . $rsFelder->FeldInhalt('ZUSATZBEMERKUNG2') . "</b>");
                    }
                    else
                    {
                        $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;<b>" . $rsFelder->FeldInhalt('ZUSATZBEMERKUNG2') . "</b>");
                    }
                }

                if ($rsFelder->FeldInhalt('ZUSATZBEMERKUNG3') != '')
                {
                    if ($rsFelder->FeldInhalt("RRE_LOADINDEX3") != '' or $rsFelder->FeldInhalt('SPEEDINDEX2') != '' or $rsFelder->FeldInhalt('RF') != '' or $rsFelder->FeldInhalt('ZUSATZBEMERKUNG2') != '')
                    {
                        $Form->SchreibeHTMLCode("/" . $rsFelder->FeldInhalt('ZUSATZBEMERKUNG3'));
                    }
                    else
                    {
                        $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsFelder->FeldInhalt('ZUSATZBEMERKUNG3'));
                    }
                }

                if ($rsFelder->FeldInhalt('ZUSATZBEMERKUNG') != '')
                {
                    if ($rsFelder->FeldInhalt("RRE_LOADINDEX3") != '' or $rsFelder->FeldInhalt('SPEEDINDEX2') != '' or $rsFelder->FeldInhalt('RF') != '' or $rsFelder->FeldInhalt('ZUSATZBEMERKUNG2') != '' or $rsFelder->FeldInhalt('ZUSATZBEMERKUNG3') != '')
                    {
                        $Form->SchreibeHTMLCode("/" . $rsFelder->FeldInhalt('ZUSATZBEMERKUNG'));
                    }
                    else
                    {
                        $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsFelder->FeldInhalt('ZUSATZBEMERKUNG'));
                    }
                }

                $Form->SchreibeHTMLCode("</td>");
            }
            else
            {
                $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center width=130>&nbsp;</td>");
            }
            //$Form->Erstelle_ListenFeld('INFO',substr($Anzeige,1),0,160,false,($SEPZeile%2),'','','','','');

            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $rsFelder->FeldInhalt('BEZEICHNUNG') . "</td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " ><b>" . $rsFelder->FeldInhalt('AST_KENNUNG') . "</b></td>");
            $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $rsFelder->FeldInhalt('AST_KENN2') . "</td>");

            //$Form->Erstelle_ListenFeld('RRP_BEZEICHNUNG',substr($rsRRP->FeldInhalt('BEZEICHNUNG'),0,15),0,140,false,($SEPZeile%2),'','','','',$rsRRP->FeldInhalt('BEZEICHNUNG'));
            //$Form->Erstelle_ListenFeld('RRP_KENN',$rsRRP->FeldInhalt('AST_KENNUNG'),0,25,false,($SEPZeile%2),'','','','','');
            //$Form->Erstelle_ListenFeld('RRP_KENN2',$rsRRP->FeldInhalt('AST_KENN2'),0,25,false,($SEPZeile%2),'','','','','');
            /*
              $Feld = 'AST_VK';

              if ($Param['LAND'] != 'DE')
              {
              $Feld = 'AST_VK'.$Param['LAND'];
              }

              $VK = $rsRRP->FeldInhalt($Feld);

              if (isset($Param['FIL_ID']) and $Param['FIL_ID'])
              {
              $VK = $rsRRP->FeldInhalt('VKFIL');
              }


              if ($rsRRP->FeldInhalt('RRE_UNGELISTET') == '1' AND ($Param['LAND']=='DE' OR $Param['LAND']=='IT' OR $Param['LAND']=='NL' OR $Param['LAND']=='AT'))
              {
              $VK = $rsRRP->FeldInhalt('RRE_UNGELISTETVK');
              }
             */
            /*
              $VK = $rsRRP->FeldInhalt($Feld);

              if ($rsRRP->FeldInhalt('RRE_UNGELISTET') == '0' and ($Param['FIL_ID']!=''))
              {
              $SQL = 'SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=\'' . $rsRRP->FeldInhalt('RRP_AST_ATUNR') . '\' AND FIB_FIL_ID='.$Param['FIL_ID'];

              $rsVK = $DB->RecordSetOeffnen($SQL);

              if ($rsVK->AnzahlDatensaetze()>0)
              {
              $VK = $rsVK->FeldInhalt('FIB_VK');
              }
              }
             */

            //Ungelistete Reifen (z.B BS0000)
            //if ($rsRRP->FeldInhalt('RRE_UNGELISTET') == '1')
            //{
            //	if (isset($Param['KBPREIS']) and $Param['KBPREIS']=='on')
            //	{
            //		$Form->Erstelle_ListenFeld('RRP_VK',$rsRRP->FeldInhalt('RRE_UNGELISTETKBPREIS'),0,50,false,($SEPZeile%2),'','','N2','','');
            //	}
            //	else
            //	{
            //		if ($rsRRP->FeldInhalt('RRE_UNGELISTETKBPREIS') != '0' AND $Param['LAND']!='DE' AND $Param['LAND']!='IT' AND $Param['LAND']!='NL' AND $Param['LAND']!='AT')
            //		{
            //			$Form->Erstelle_ListenFeld('RRP_VK','',0,50,false,($SEPZeile%2),'','','N2','','');
            //		}
            //Preise bei manuellen Sonderbestellungen anzeigen
            //		elseif((($Recht253&2)==2)) // or ($rsRRP->FeldInhalt('RRE_UNGELISTET') != '1' AND $rsRRP->FeldInhalt('AST_KENNUNG') == 'S'))
            //		{
            //			$Form->Erstelle_ListenFeld('RRP_VK',$VK,0,50,false,($SEPZeile%2),'','','N2','','');
            //		}
            //		else
            //		{
            //			$Form->Erstelle_ListenFeld('RRP_VK','auf Anfrage',0,100,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','T','','');
            //		}
            //	}
            //}

            if ($rsFelder->FeldInhalt('RRE_UNGELISTET') == '1')
            {
                if (($Recht253 & 2) == 2)
                {
                    $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>" . $rsFelder->FeldInhalt('VK') . "</font></b></td>");
                    //$Form->Erstelle_ListenFeld('RRP_VK',$rsRRP->FeldInhalt('VK'),0,50,false,($SEPZeile%2),'','','N2','','');
                }
                else
                {
                    $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>auf Anfrage</font></b></td>");
                    //$Form->Erstelle_ListenFeld('RRP_VK','auf Anfrage',0,100,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','T','','');
                }
            }
            else
            {
                if ($rsFelder->FeldInhalt('AST_SUCH2') != '')
                {
                    if ($rsFelder->FeldInhalt('VKVORBEREITUNG') != '')
                    {
                        $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>i.V.</font></b></td>");
                        //$Form->Erstelle_ListenFeld('RRP_VK','i.V.',0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','T','','');
                    }
                    else
                    {
                        $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000></font></b></td>");
                        //$Form->Erstelle_ListenFeld('RRP_VK','',0,50,false,($SEPZeile%2),'','','N2','','');
                    }
                }
                else
                {
                    $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>" . $rsFelder->FeldInhalt('VK') . "</font></b></td>");
                    //$Form->Erstelle_ListenFeld('RRP_VK',$rsRRP->FeldInhalt('VK'),0,50,false,($SEPZeile%2),'','','N2','','');
                }
            }

            if ($rsFelder->FeldInhalt('RRA_AKTIONPROZENT') != '' and $rsFelder->FeldInhalt('RRE_UNGELISTET') == '0' and $rsFelder->FeldInhalt('AST_SUCH2') == '')
            {
                $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><center><b><font color=#FF0000>" . ($rsFelder->FeldInhalt('RRA_AKTIONPROZENT') == '0' ? '' : $Form->Format('N1', $rsFelder->FeldInhalt('RRA_AKTIONPROZENT'))) . "</font></b></center></td>");
                //$Form->Erstelle_ListenFeld('RRA_AKTIONPROZENT',$rsRRP->FeldInhalt('RRA_AKTIONPROZENT'),0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','N1','','');
            }
            else
            {
                $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><center><b><font color=#FF0000></font></b></center></td>");
                //$Form->Erstelle_ListenFeld('RRA_AKTIONPROZENT','',0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','N1','','');
            }

            if ($rsFelder->FeldInhalt('RRA_AKTIONTEXT') != '')
            {
                $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . $rsFelder->FeldInhalt('RRA_AKTIONTEXT') . "</font></b></td>");
                //$Form->Erstelle_ListenFeld('RRA_AKTIONTEXT',$rsRRP->FeldInhalt('RRA_AKTIONTEXT'),0,50,false,($SEPZeile%2),'color:#FF0000','','T','','');
            }
            elseif ($rsFelder->FeldInhalt('AST_SUCH2') != '') //Bei Sonderarti
            {
                if ($rsFelder->FeldInhalt('VKVORBEREITUNG') == '')
                {
                    $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>i.V.</font></b></td>");
                    //$Form->Erstelle_ListenFeld('RRA_AKTION','i.V.',0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','T','','');
                }
                elseif ($rsFelder->FeldInhalt('VKVORBEREITUNG') != '')
                {
                    $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . $Form->Format('N2', $rsFelder->FeldInhalt('VKVORBEREITUNG')) . "</font></b></td>");
                    //$Form->Erstelle_ListenFeld('RRA_AKTION',$rsRRP->FeldInhalt('VKVORBEREITUNG'),0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','N2','','');
                }
                else
                {
                    $Form->SchreibeHTMLCode('<td ' . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . '></td>');
                    //$Form->Erstelle_ListenFeld('RRA_AKTION','',0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','T','','');
                }
            }
            else
            {
                $Aktion = 0;

                if ($rsFelder->FeldInhalt('RRA_AKTIONPROZENT') != '' and $rsFelder->FeldInhalt('RRE_UNGELISTET') == '0')
                {
                    $Aktion = ((100 - $rsFelder->FeldInhalt('RRA_AKTIONPROZENT')) / 100);

                    $Aktion = str_replace(',', '.', $Aktion) * str_replace(',', '.', $rsFelder->FeldInhalt('VK'));

                    //$Form->Erstelle_ListenFeld('RRA_AKTION',$Aktion,0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','N2','','');
                    $Form->SchreibeHTMLCode("<td " . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . ($Aktion == 0 ? '' : $Form->Format('N2', $Aktion)) . "</font></b></td>");
                }
                else
                {
                    $Form->SchreibeHTMLCode('<td ' . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . '></td>');
                    //$Form->Erstelle_ListenFeld('RRA_AKTION','',0,50,false,($SEPZeile%2),'color:#FF0000;font-weight:bold','','T','','');
                }
            }

            $SQL = 'SELECT PRI_WERT AS ZERTIFIKAT';
            $SQL.= ' FROM PROBLEMARTIKEL';
            $SQL.= ' INNER JOIN PROBLEMARTIKELINFOS ON PRI_PRA_KEY = PRA_KEY AND PRI_ITY_KEY = 16';
            $SQL.= ' WHERE PRA_AST_ATUNR=\'' . $rsFelder->FeldInhalt('RRE_AST_ATUNR') . '\'';
            $SQL.= ' AND PRA_KATEGORIE = 1';  // NUR REIFEN BIS <= 185

            $ProblemReifenKZ = '';
            $Link = '';
            $rsPAR = $DB->RecordSetOeffnen($SQL);

            if ($rsPAR->AnzahlDatensaetze() > 0 and $rsPAR->FeldInhalt('ZERTIFIKAT') != '')
            {
                $ProblemReifenKZ = 'Z!';
                $Link = '/dokumentanzeigen.php?bereich=reifenzertifikate&dateiname=' . $rsPAR->FeldInhalt('ZERTIFIKAT') . '&erweiterung=pdf';

                $Form->SchreibeHTMLCode('<td ' . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . ' width=30><span title="Reifen ohne S-Kennung">' . ($Link == '' ? '' : '<a href="' . $Link . '">') . $ProblemReifenKZ . ($Link == '' ? '' : '</a>') . '</span></td>');
                //$Form->Erstelle_ListenFeld('ZERTIFIKAT',$ProblemReifenKZ,0,20,false,($SEPZeile%2),'',$Link,'T','','');
            }
            else
            {
                $Form->SchreibeHTMLCode('<td ' . (($SEPZeile % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . ' width=30></td>');
                //$Form->Erstelle_ListenFeld('ZERTIFIKAT','',0,20,false,($SEPZeile%2),'','','T','','');
            }
            $Form->SchreibeHTMLCode("</tr>");

            $rsFelder->DSWeiter();
            $SEPZeile++;
        }

        $Form->SchreibeHTMLCode('</table>');

        $Form->ZeileEnde();

        $Link = './reifen_Main.php?' . (isset($_GET['cmdAktion']) ? 'cmdAktion=' . $_GET['cmdAktion'] : '') . '&Liste=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');
        //$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();

        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        $Form->SchaltflaechenEnde();
        
        
    }

    $AWISBenutzer->ParameterSchreiben("RRESuche", serialize($Param));
	$Form->SetzeCursor($AWISCursorPosition);

}
catch (awisException $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200908031126");
    }
    else
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}
catch (Exception $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200908031127");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}


/**
 * SQL-Bedingung zusammenbauen aufgrund der Paremeter aus dem Suchformular
 * ************************************************************************
 * Parameter :
 * @param string $Param : Parameter aus der Suchmaske
 * R�ckgabeparameter:
 * @return string $Bedingung : Fertige Bedingung f�r den SQL
 * ***********************************************************************
 * Erstellt:
 * 28.06.2011 Stefan Oppl
 * �nderungen:
 * dd.mm.yyyy XX :
 * ***********************************************************************
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;
    
    $Bedingung = '';

    if (floatval($AWIS_KEY1) != 0)
    {
        $Bedingung.= 'AND RRE_KEY = ' . $AWIS_KEY1;
        return $Bedingung;
    }

    if (isset($Param['BREITE']) AND $Param['BREITE'] != '')
    {
        $Bedingung .= 'AND RRE_BREITE =' . $DB->FeldInhaltFormat('N0', $Param['BREITE']) . ' ';
    }

    if (isset($Param['QUERSCHNITT']) AND $Param['QUERSCHNITT'] != '')
    {
        if ($Param['QUERSCHNITT'] == '80') //80 und leer
        {
            $Bedingung .= 'AND (RRE_QUERSCHNITT IS NULL OR RRE_QUERSCHNITT = ' . $DB->FeldInhaltFormat('T', $Param['QUERSCHNITT']) . ') ';
        }
        else
        {
            $Bedingung .= 'AND RRE_QUERSCHNITT ' . $DB->LikeOderIst($Param['QUERSCHNITT']) . ' ';
        }
    }

    if (isset($Param['INNENDURCHMESSER']) AND $Param['INNENDURCHMESSER'] != '')
    {
        $Bedingung .= 'AND RRE_INNENDURCHMESSER =' . $DB->FeldInhaltFormat('N2', $Param['INNENDURCHMESSER']) . ' ';
    }

    if (isset($Param['ATUNR']) AND $Param['ATUNR'] != '')
    {
        $Bedingung .= 'AND RRE_AST_ATUNR ' . $DB->LikeOderIst($Param['ATUNR'], 1) . ' ';
    }

    if (isset($Param['REHKEY']) AND $Param['REHKEY'] != 0)
    {
        $Bedingung .= 'AND RRE_REH_KEY =' . $DB->FeldInhaltFormat('NO', $Param['REHKEY'], false) . ' ';
    }

    if (isset($Param['LLKW']) AND $Param['LLKW'] != 0)
    {
        if (strtoupper($Param['LLKW'] == 'C'))
        {
            $Bedingung .= 'AND (LLKW =' . $DB->FeldInhaltFormat('T', strtoupper($Param['LLKW']), false) . ' OR LLKW = \'CP\')';
        }
        else
        {
            $Bedingung .= 'AND LLKW =' . $DB->FeldInhaltFormat('T', strtoupper($Param['LLKW']), false) . ' ';
        }
    }
    else
    {
        if (isset($Param['CCSORT']) AND $Param['CCSORT'] == 'on')
        {
            $Bedingung .= 'AND LLKW IS NULL';
        }
    }

    if (isset($Param['HLOAD']) && $Param['HLOAD'] == 'on')
    {
    	$SuchOperatorLoadIndex = '>=';
    }
    else
    {
    	$SuchOperatorLoadIndex = '=';
    }
    
    if (isset($Param['LOADINDEX']) AND $Param['LOADINDEX'] != 0 AND isset($Param['LOADINDEX2']) AND $Param['LOADINDEX2'] != 0)
    {
        $Bedingung .= " AND ((to_number(regexp_replace(LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $Param['LOADINDEX'] . " AND LoadIndex2 $SuchOperatorLoadIndex " . $Param['LOADINDEX2'] . ")";
        $Bedingung .= " OR   (to_number(regexp_replace(LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $Param['LOADINDEX2'] . " AND LoadIndex2 $SuchOperatorLoadIndex " . $Param['LOADINDEX'] . "))";
    }
    else
    {
        if (isset($Param['LOADINDEX']) AND $Param['LOADINDEX'] != 0)
        {
            $Bedingung .= " AND to_number(regexp_replace(LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $Param['LOADINDEX'];
        }

        if (isset($Param['LOADINDEX2']) AND $Param['LOADINDEX2'] != 0)
        {
            $Bedingung .= " AND to_number(regexp_replace(LoadIndex2, '\D', '')) $SuchOperatorLoadIndex " . $Param['LOADINDEX2'];
        }
    }

    if (isset($Param['SPI_ID']) AND $Param['SPI_ID'] != 0)
    {
        if (isset($Param['HSPEED']) AND $Param['HSPEED'] == 'on')
        {
            $Bedingung.= ' AND (SPEEDINDEX IS NULL OR SPI_ID >= ' . $Param['SPI_ID'] . ')';
        }
        else
        {
            $Bedingung .= ' AND SPI_ID=' . $Param['SPI_ID'];
        }
    }

    /*
      if(isset($Param['MAXVK']) AND $Param['MAXVK']!=0)
      {
      $Bedingung .= 'AND AST_VK <=' . $DB->FeldInhaltFormat('N2',$Param['MAXVK']) . ' ';
      }
     */

    if (isset($Param['CCSORT']) AND $Param['CCSORT'] == 'on')
    {
        $Bedingung .= ' AND (AST_KENNUNG <> \'N\' OR AST_KENNUNG IS NULL) AND AST_VK > 0';
    }

    if (isset($Param['RETID']) AND $Param['RETID'] > 0)
    {
        $Bedingung .= " AND (EXISTS (SELECT RRI_RRE_RRP_KEY FROM ReiReifenInfos WHERE RRI_RRE_RRP_KEY = RRE_RRP_KEY AND (RRI_RRT_ID = 80 OR RRI_RRT_ID = 81 OR RRI_RRT_ID = 82) AND ";
        $Bedingung .= " RRI_Wert = '" . $Param['RETID'] . "'))";
    }

    if (isset($Param['BEZEICHNUNG']) AND $Param['BEZEICHNUNG'] != '')
    {
        $Bedingung .= " AND UPPER(BEZEICHNUNG) " . $DB->LikeOderIst($Param['BEZEICHNUNG'], 1);
    }

    return $Bedingung;
}
?>