<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;
global $Fehlermeldung;

try
{
    require_once('awisDatenbank.inc');
    require_once('awisFormular.inc');

	$TextKonserven = array();
	$TextKonserven[]=array('GSD','%');
	$TextKonserven[]=array('GKU','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_DS%');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','Kostenstelle');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht28000 = $AWISBenutzer->HatDasRecht(28000);
	
	if($Recht28000==0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}
	
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GSD'));

	// AWIS_KEY1 setzen
	$AWIS_KEY1 = '';
	if (isset($_GET['GSK_AUFTRAGNR']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('T', $_GET['GSK_AUFTRAGNR']);
	}
	elseif (isset($_POST['txtneuAuftragnr']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('T', $_POST['txtneuAuftragnr']);
	}
	elseif (isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = -1;
	}
	$Param['GSD_AUFTRAGNUMMER']= $AWIS_KEY1;//Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..
	$Param['Seite']=isset($_GET['Seite'])?$_GET['Seite']:'';

	// Parameter
	if(isset($_POST['cmdSuche_x']))
	{
		$Param['GSD_GUTSCHEINNUMMER']= $Form->Format('T',$_POST['sucGSD_GUTSCHEINNUMMER'],true);
		$Param['GSD_AUFTRAGNUMMER'] = 	$Form->Format('T',$_POST['sucGSD_AUFTRAGNUMMER'],true);
		$Param['GSD_AUFTRAGSTATUS']= $Form->Format('T',$_POST['sucGSD_AUFTRAGSTATUS'],true);
		$Param['GSD_KOSTENSTELLE']= $Form->Format('T',$_POST['sucGSD_KOSTENSTELLE'],true);
		$Param['GSD_GKU_KEY']= $Form->Format('N0',$_POST['sucGSD_GKU_KEY'],true);
		$Param['GSD_KUNDE_INTERN']=$Form->Format('T',$_POST['sucGSD_KUNDE_INTERN'],true);
		$Param['GSD_PROFIL']= $Form->Format('N0',$_POST['sucGSD_PROFIL'],true);
		$Param['GSD_BUCHUNGSART']= $Form->Format('T',$_POST['sucGSD_BUCHUNGSART'],true);
		$Param['GSD_KUNDE_INTERN']=$Form->Format('T',$_POST['sucGSD_KUNDE_INTERN'],true);
		$Param['GSD_KUNDE']=$Form->Format('T',$_POST['sucGSD_KUNDE']);

		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['Seite']= isset($_GET['Seite'])?$_GET['Seite']:'';
		$Param['Block']='1';
		$Param['ORDER']='';
		$Param['KEY'] = '';
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		//Neuer Gutscheinauftrag wird angelegt
		$AWIS_KEY1=-1;
	}
	elseif((isset($_POST['cmdAktiv_x'])) or (isset($_POST['cmdDeaktiv_x'])))
	{
		include ('./gutschein_speichern.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		//Pr�fen ob alle Felder gepflegt wurden,
		// wenn ja dann weiter zu speichern, ansonsten Meldung ausgeben und zur�ck zur Eingabe
		$Fehlermeldung='';
		$Fehlermeldungpos = '';
		$Fehlermeldungkopf='';
		$Form->Formular_Start();

		//Kopfdaten vorhanden?
		$Kostenstelle=$Form->Format('N0', $_POST['txtGSK_KOSTENSTELLE']);
		$Kunde=$Form->Format('T',$_POST['txtGSK_KUNDE']);
		$KundeExtern=$Form->Format('N0',$_POST['txtGSK_KUNDE_EXTERN']);
		$KundeIntern=$Form->Format('T',$_POST['txtGSK_KUNDE_INTERN']);
		$Gutscheinprofil=$Form->Format('N0',$_POST['txtGSK_PROFIL']);

		if($Kunde=='E')
		{
			if(!isset($KundeExtern) or $KundeExtern == '')
			{
				$Fehlermeldungkopf.='Bitte folgende Felder noch pflegen: Gutscheinkunde Extern <br>';
			}
		}
		elseif($Kunde=='I')
		{
			if(!isset($KundeIntern) or $KundeIntern == '')
			{
				$Fehlermeldungkopf.='Bitte folgende Felder noch pflegen: Gutscheinkunde Intern <br>';
			}

			if($Kostenstelle=='')
			{
				$Fehlermeldungkopf.='Bitte folgende Felder noch pflegen: Kostenstelle <br>';
			}
			elseif ($Kostenstelle == 0)
			{
				$Fehlermeldungkopf.='Kostenstelle muss eine Zahl gr��er 0 sein! <br>';
			}
		}
		else
		{
			$Fehlermeldungkopf.='Bitte folgende Felder noch pflegen: Gutscheinkunde<br>';
		}

		if($Gutscheinprofil=='')
		{
			$Fehlermeldungkopf.='Bitte folgende Felder noch pflegen: Gutscheinprofil <br>';
		}

		//Posdaten vorhanden?
		$AnzahlPos = $Form->Format('N0',$_POST['txtaktCount']);
		$i=1;
		$arrayMenge =  array();
		$arrayBetrag = array();
		$arrayBuchart = array();

		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$arrayMenge[$i]=($Form->Format('N0',($_POST['txtMENGE'.$i])));
		}
		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$arrayBetrag[$i]=($Form->Format('N2',($_POST['txtPREIS'.$i])));
		}
		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$arrayBuchart[$i]=($Form->Format('T',($_POST['txtBUCHART'.$i])));
		}

		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$Betrag=str_replace(',','.',$arrayBetrag[$i]);
			$Betrag2=(float)$Betrag;

			if($arrayBetrag[$i]=='')
			{
				$Fehlermeldungpos.='Bitte folgendes Feld noch pflegen: Betrag <br>';
			}
			elseif ($Betrag2==0)
			{
				$Fehlermeldungpos.='Betrag muss gr��er 0 sein! <br>';
			}
			if ($arrayMenge[$i]=='')
			{
				$Fehlermeldungpos.='Bitte folgendes Feld noch pflegen: Menge <br>';
			}
			elseif ($arrayMenge[$i]==0)
			{
				$Fehlermeldungpos.='Menge muss gr��er 0 sein! <br>';
			}
			if ($arrayBuchart[$i]=='')
			{
				$Fehlermeldungpos.='Bitte folgendes Feld noch pflegen: Buchungsart <br>';
			}
		}
		$Fehlermeldung=($Fehlermeldungkopf!=''?$Fehlermeldungkopf .'<br><br>':''). $Fehlermeldungpos;

		if($Fehlermeldung!='')
		{
			//Fehlermeldung ausgeben und zur�ck zur Eingabe
			$Form->Hinweistext($Fehlermeldung);
		}
		else
		{
			include('./gutschein_speichern.php');
		}
		$Form->Formular_Ende();
		$Form->SchreibeHTMLCode('</form>');
	}
	elseif(isset($_POST['cmdDSZurueck_x']))
	{
		//Komme von Speichern/�ndern/GSNR zur�ck, dann folgendes machen
		$AWIS_KEY1 = $_POST['txtneuAuftragnr'];
		
		$SQL = 'SELECT GSK_AUFTRAGNR';
		$SQL .= ' FROM GUTSCHEINKOPFDATEN';
		$SQL .= ' WHERE GSK_AUFTRAGNR < :var_N0_GSK_AUFTRAGNR';
		$SQL .= ' ORDER BY GSK_AUFTRAGNR DESC';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_GSK_AUFTRAGNR'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsGSK = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
		$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);		
		
		if(!$rsGSK->EOF())
		{
			$AWIS_KEY1=$rsGSK->FeldInhalt('GSK_AUFTRAGNR');
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
		$AWIS_KEY1 = $_POST['txtneuAuftragnr'];
		$SQL = 'SELECT GSK_AUFTRAGNR';
		$SQL .= ' FROM Gutscheinkopfdaten';
		$SQL .= ' WHERE GSK_AUFTRAGNR > :var_N0_GSK_AUFTRAGNR';
		$SQL .= ' ORDER BY GSK_AUFTRAGNR ASC';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_GSK_AUFTRAGNR'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsGSK = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
		
		if(!$rsGSK->EOF())
		{
			$AWIS_KEY1=$rsGSK->FeldInhalt('GSK_AUFTRAGNR');
		}
		$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key bzw. Benutzer abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_GSD',serialize($Param));
		}
		if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
		{
			$Param['BLOCK'] = $_REQUEST['Block'];
		}
		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}
		if(isset($_GET['Seite']))
		{
			$Param['Seite']= isset($_GET['Seite'])?$_GET['Seite']:'';
		}
		$AWIS_KEY1=$Param['KEY'];
	}

	//* Sortierung
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY GSK_AUFTRAGNR DESC';
			$Param['ORDER']='GSK_AUFTRAGNR DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	if($Param['Seite'] == 'Gutscheine')
	{
		include('./gutschein_Details_Positionen.php');
	}
	else
	{
		// Daten suchen
		$Bedingung = _BedingungErstellen($Param);

		$SQL = 'SELECT gskopf.GSK_KEY';
		$SQL .= ', gskopf.GSK_AUFTRAGNR';
		$SQL .= ', gskopf.GSK_KST';
		$SQL .= ', gskopf.GSK_DATEINAME';
		$SQL .= ', gku.GKU_NAME1';
		$SQL .= ', gku.GKU_NAME2';
		$SQL .= ', gku.GKU_KONZERN';
		$SQL .= ', gku.GKU_STRASSE';
		$SQL .= ', gku.GKU_HN';
		$SQL .= ', gku.GKU_PLZ';
		$SQL .= ', gku.GKU_ORT';
		$SQL .= ', gskopf.gsk_gku_key';
		$SQL .= ', gskopf.GSK_STATUS';
		$SQL .= ', gku.GKU_ANREDE';
		$SQL .= ', gku.GKU_TITEL';
		$SQL .= ', gku.GKU_TELEFON';
		$SQL .= ', gku.GKU_EMAIL';
		$SQL .= ', gku.GKU_MOBILTELEFON';
		$SQL .= ', gku.GKU_FAX';
		$SQL .= ', gku.GKU_POSTFACH';
		$SQL .= ', gku.GKU_POSTFACHPLZ';
		$SQL .= ', gku.GKU_VERTRAGNR';
		$SQL .= ', gskopf.GSK_AUFTRAGGEBER';
		$SQL .= ', gskopf.GSK_GSV_KEY';
		$SQL .= ', gsvor.GSV_BEZEICHNUNG';
		$SQL .= ', gskopf.GSK_KUNDE_INTERN';
		$SQL .= ', gskopf.gsk_freitext';
		$SQL .= ', pers.NAME';
		$SQL .= ', pers.VORNAME';
		$SQL .= ', gskopf.GSK_JOBSTATUS';
		$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
		$SQL .= ' FROM GUTSCHEINKOPFDATEN gskopf';
		$SQL .= ' LEFT JOIN GUTSCHEINPOSDATEN gspos on gskopf.GSK_AUFTRAGNR = gspos.GSP_AUFTRAGNR';
		$SQL .= ' LEFT JOIN GUTSCHEINKUNDEEXTERN gku on gskopf.GSK_GKU_KEY = gku.gku_KEY';
		$SQL .= ' LEFT JOIN GUTSCHEINVORLAGE gsvor on gskopf.GSK_GSV_KEY = gsvor.GSV_KEY';
		$SQL .= ' LEFT JOIN PERSONAL_KOMPLETT pers on gskopf.GSK_AUFTRAGGEBER = pers.PERSNR';

		if($Bedingung!='')
		{
			$SQL .= ' WHERE ' . substr($Bedingung,4);
		}

		$SQL .= ' Group by gskopf.GSK_KEY';
		$SQL .= ', gskopf.GSK_AUFTRAGNR';
		$SQL .= ', gskopf.GSK_KST';
		$SQL .= ', gskopf.GSK_DATEINAME';
		$SQL .= ', gku.GKU_NAME1';
		$SQL .= ', gku.GKU_NAME2';
		$SQL .= ', gku.GKU_KONZERN';
		$SQL .= ', gku.GKU_STRASSE';
		$SQL .= ', gku.GKU_HN';
		$SQL .= ', gku.GKU_PLZ';
		$SQL .= ', gku.GKU_ORT';
		$SQL .= ', gskopf.gsk_gku_key';
		$SQL .= ', gskopf.GSK_STATUS';
		$SQL .= ', gku.GKU_ANREDE';
		$SQL .= ', gku.GKU_TITEL';
		$SQL .= ', gku.GKU_TELEFON';
		$SQL .= ', gku.GKU_EMAIL';
		$SQL .= ', gku.GKU_MOBILTELEFON';
		$SQL .= ', gku.GKU_FAX';
		$SQL .= ', gku.GKU_POSTFACH';
		$SQL .= ', gku.GKU_POSTFACHPLZ';
		$SQL .= ', gku.GKU_VERTRAGNR';
		$SQL .= ', gskopf.GSK_AUFTRAGGEBER';
		$SQL .= ', gskopf.GSK_GSV_KEY';
		$SQL .= ', gsvor.GSV_BEZEICHNUNG';
		$SQL .= ', gskopf.GSK_AUFTRAGGEBER';
		$SQL .= ', gskopf.GSK_KUNDE_INTERN';
		$SQL .= ', gskopf.gsk_freitext';
		$SQL .= ', pers.NAME';
		$SQL .= ', pers.VORNAME';
		$SQL .= ', gskopf.GSK_JOBSTATUS';

		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
		// Zum Bl�ttern in den Daten
		if (isset($_REQUEST['Block']))
		{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
		}
		else
		{
			$Block = $Param['Block'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

		if (!isset($AWIS_KEY1) OR $AWIS_KEY1 == '') {
			$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
		}

		$rsGSK = $DB->RecordSetOeffnen($SQL);
		$rsGSKZeilen = $rsGSK->AnzahlDatensaetze();
		$AWISBenutzer->ParameterSchreiben('Formular_GSD',serialize($Param));
		$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1,$_POST);

		// Spaltenbreiten f�r Listenansicht
		$FeldBreiten = array();
		$FeldBreiten['GSK_AUFTRAGNR'] = 150;
		$FeldBreiten['GSV_KEY']=300;
		$FeldBreiten['GSK_KST']=200;
		$FeldBreiten['GSK_AUFTRAGGEBER']=150;
		$FeldBreiten['GSK_STATUS']=100;
		$FeldBreiten['GKU_KONZERN']=250;
		$FeldBreiten['GPD_POSITION']=100;
		$FeldBreiten['GPD_KOMMENTAR']=250;
		$FeldBreiten['GPD_STATUS_MSG']=250;
		$FeldBreiten['GPD_GUTSCHEINNR']=200;
		$FeldBreiten['GPD_BUCHART']=140;
		$FeldBreiten['GPD_STATUS']=130;
		$FeldBreiten['GSP_MENGE']=80;
		$FeldBreiten['GSP_BETRAG']=80;
		$FeldBreiten['GSK_KUNDE_INTERN']=200;
		$FeldBreiten['GSK_PROFIL']=200;

		$script = '	$(document).ready(function(){
						showKunde(-1);});
					function showKunde(intVal){
												
												if (intVal == \'E\'){
													$( "#kundeint" ).hide();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
													$( "#kundeext" ).show();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', true);
													$( "#anzeige" ).hide();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
												} 
												else if (intVal == \'I\'){
													$( "#kundeint" ).show();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', true);
													$( "#kundeext" ).hide();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
													$( "#anzeige" ).hide();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
												}
												else{
													$( "#kundeint" ).hide();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
													$( "#kundeext" ).hide();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', false);
													$( "#anzeige" ).show();
													$( "#suclst_GSD_KUNDEAUSWAHL" ).prop(\'required\', true);
												}
											}';
		$Form->SchreibeHTMLCode('<script>' . $script . '</script>');

		// Daten anzeigen
		if(isset($_POST['cmdDSNeu_x']) or isset($_POST['ico_add__x']) or isset($_POST['ico_delete__x']) or $Fehlermeldung!='')//Neuer Datensatz
		{
			if(($Recht28000&4) == 4)
			{
				$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./gutschein_Main.php?cmdAktion=Details>");
				$Form->Formular_Start();

				//Verstecktes Feld mit neuer Auftragsnummer
				$Form->Erstelle_HiddenFeld('neuAuftragnr','');

				//Gutscheine Kopfdaten eingeben
				//Kostenstelle
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KOSTENSTELLE'],190);
				$Form->Erstelle_TextFeld('GSK_KOSTENSTELLE',(isset($_POST['txtGSK_KOSTENSTELLE'])?$_POST['txtGSK_KOSTENSTELLE']:''),250,250,true,'','','','N0','',$AWISSprachKonserven['GSD']['GSD_KOSTENSTELLE']);
				$Form->ZeileEnde();

				//Gutscheinkunde auswahl
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KUNDE'],190);
				$Daten = explode('|', $AWISSprachKonserven['GSD']['lst_GSD_KUNDEAUSWAHL']);
				$Form->Erstelle_SelectFeld('!GSK_KUNDE', (isset($_POST['txtGSK_KUNDE'])?$_POST['txtGSK_KUNDE']:''), 250, true, '','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten, 'onchange="showKunde(this.value)"',$AWISSprachKonserven['GSD']['GSD_KUNDE']);
				$Form->ZeileEnde();

				//Vorauswahl anzeigen Kunde intern und extern, Steuerung per Script
				if(isset($_POST['txtGSK_KUNDE']) AND isset($_POST['txtGSK_KUNDE_EXTERN']) AND ($_POST['txtGSK_KUNDE']=='E'))
				{
					$script = '	$(document).ready(function(){
						showKunde(\'E\');});';

					$Form->SchreibeHTMLCode('<script>' . $script . '</script>');
				}
				if(isset($_POST['txtGSK_KUNDE']) AND isset($_POST['txtGSK_KUNDE_INTERN']) AND ($_POST['txtGSK_KUNDE']=='I'))
				{
					$script = '	$(document).ready(function(){
						showKunde(\'I\');});';
					$Form->SchreibeHTMLCode('<script>' . $script . '</script>');
				}

				$Form->ZeileStart('', 'kundeint');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KUNDE_INTERN'],190);
				$Form->Erstelle_TextFeld('GSK_KUNDE_INTERN',(isset($_POST['txtGSK_KUNDE_INTERN'])?$_POST['txtGSK_KUNDE_INTERN']:''),250,250,true,'','','','T','',$AWISSprachKonserven['GSD']['GSD_KUNDE_INTERNTTT']);
				$Form->ZeileEnde();

				$Form->ZeileStart('', 'kundeext');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_GKU_KEY'],190);
				$SQL_lst = 'SELECT GKU_KEY, GKU_KONZERN AS Konzern';
				$SQL_lst .= ' FROM GUTSCHEINKUNDEEXTERN';
				$SQL_lst .= ' WHERE GKU_STATUS = \'A\'';
				$Form->Erstelle_SelectFeld('GSK_KUNDE_EXTERN',(isset($_POST['txtGSK_KUNDE_EXTERN'])?$_POST['txtGSK_KUNDE_EXTERN']:''), 200,true,$SQL_lst,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','',$AWISSprachKonserven['GSD']['GSD_GKU_KEY']);
				$Form->ZeileEnde();

				//Gutscheinprofil
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_PROFIL'].':',190);
				$SQL_lst = 'SELECT GSV_KEY, GSV_BEZEICHNUNG AS Profil';
				$SQL_lst .= ' FROM GUTSCHEINVORLAGE';
				$Form->Erstelle_SelectFeld('!GSK_PROFIL',(isset($_POST['txtGSK_PROFIL'])?$_POST['txtGSK_PROFIL']:''), 250,true,$SQL_lst,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','',$AWISSprachKonserven['GSD']['GSD_PROFIL']);
				$Form->ZeileEnde();

				//Freitext
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_FREITEXTFELD'],190);
				$Form->Erstelle_TextFeld('GSK_FREITEXT',(isset($_POST['txtGSK_FREITEXT'])?$_POST['txtGSK_FREITEXT']:''),250,250,true,'','','','T','',$AWISSprachKonserven['GSD']['GSD_FREITEXTFELD']);
				$Form->ZeileEnde();
				$Form->Trennzeile('O');
				$Form->Trennzeile('O');

				//Gutscheine Posdaten eingeben
				$Icons = array();
				$Icons2 = array();
				$Icons[] = array('add','POST~');
				$Icons2[] = array('delete','POST~');

				$Form->ZeileStart();
				$Form->Erstelle_Liste_Ueberschrift('',40);
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_POSITIONEN'],80);
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_MENGE'],100);
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_BETRAG'],100);
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_BUCHUNGSART'],200);
				$Form->ZeileEnde();

				$i=1;

				if(isset($Fehlermeldung))
				{
					$AktuelleAnzahl = isset($_POST['txtaktCount'])?$_POST['txtaktCount']:1;
				}
				elseif(isset($_POST['ico_delete__x']))
				{
					$AktuelleAnzahl =  isset($_POST['txtaktCount'])?$_POST['txtaktCount']-1:1;
				}
				else
				{
					$AktuelleAnzahl =  isset($_POST['txtaktCount'])?$_POST['txtaktCount']+1:1;
				}

				if(isset($_POST['ico_add__x']) or isset($Fehlermeldung) or isset($_POST['ico_delete__x']))
				{
					for($i=1;$i<$AktuelleAnzahl;$i++)
					{
						$Form->ZeileStart();
						$Form->Erstelle_ListenFeld('LEER','',0,40,false);
						$Form->Erstelle_ListenFeld('POS'.$i,$i,10,80,false);
						$Form->Erstelle_ListenFeld('MENGE'.$i,$Form->Format('N0',$_POST['txtMENGE'.$i]),10,100,true);
						$Form->Erstelle_ListenFeld('PREIS'.$i,$Form->Format('N2',$_POST['txtPREIS'.$i]),10,100,true);
						$Daten = explode("|",$AWISSprachKonserven['GSD']['lst_GSD_ANLAGESTATUS']);
						$Form->Erstelle_SelectFeld('BUCHART'.$i,(isset($_POST['txtBUCHART'.$i])?$_POST['txtBUCHART'.$i]:''),200,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'2','','',$Daten);
						$Form->ZeileEnde();
					}
				}

				$Form->ZeileStart();
				$Form->Erstelle_ListeIcons($Icons,25);
				if ($AktuelleAnzahl>=2)
				{
					$Form->Erstelle_ListeIcons($Icons2,13);
				}
				$Form->Erstelle_ListenFeld('LEER','',0,2,false);
				$Form->Erstelle_ListenFeld('POS'.$AktuelleAnzahl,$i,10,80,false);
				$Form->Erstelle_ListenFeld('MENGE'.$AktuelleAnzahl,$Form->Format('N0',(isset($_POST['txtMENGE'.$i])?($_POST['txtMENGE'.$i]):'')),10,100,true,'','','','N0');
				$Form->Erstelle_ListenFeld('PREIS'.$AktuelleAnzahl,$Form->Format('N2',(isset($_POST['txtPREIS'.$i])?($_POST['txtPREIS'.$i]):'')),10,100,true,'','','','N2');
				$Daten = explode("|",$AWISSprachKonserven['GSD']['lst_GSD_ANLAGESTATUS']);
				$Form->Erstelle_SelectFeld('BUCHART'.$AktuelleAnzahl,(isset($_POST['txtBUCHART'.$i])?$_POST['txtBUCHART'.$i]:''),200,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'2','','',$Daten);
				$Form->ZeileEnde();

				$Form->Erstelle_HiddenFeld('aktCount',$AktuelleAnzahl);

				//Schaltfl�chen
				$Form->Formular_Ende();
				$Form->Trennzeile('O');

				// Schaltfl�chen f�r dieses Register
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
				$Form->Schaltflaeche('script','Hinweis',$Form->PopupOeffnen(2),'/bilder/cmd_speichern.png','Hinweis','','',20,20,'',true);

				ob_start();
				$PopUp = 'Es k�nnen keine �nderungen nach dem Speichern vorgenommen werden. Trotzdem Speichern?';
				$Form->PopupDialog('Speichern',$PopUp,array(array('/bilder/cmd_dsloeschen.png','','close',''),array('/bilder/cmd_weiter.png','cmdSpeichern','post','')),$Form::POPUP_INFO,2);
				$Form->SchaltflaechenEnde();

				$Form->SchreibeHTMLCode('</form>');
			}
			else
			{
				$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
				die();
			}
		}
		elseif($rsGSKZeilen==0 AND !isset($_POST['cmdDSNeu_x']))// Keine Datens�tze gefunden!
		{
			echo '<span class=HinweisText>Es wurden keine passenden Daten gefunden.</span>';
		}
		elseif($rsGSKZeilen>1  AND !isset($_GET['Seite'])AND !isset($_GET['GSK_AUFTRAGNR']))// DetailListe anzeigen
		{
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Link = './gutschein_Main.php?cmdAktion=Details&Sort=GSK_AUFTRAGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSK_AUFTRAGNR'))?'~':'').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').'';
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_AUFTRAGNUMMER'],$FeldBreiten['GSK_AUFTRAGNR'],'',$Link);

			$Link = './gutschein_Main.php?cmdAktion=Details&Sort=GSK_GSV_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSK_GSV_KEY'))?'~':'').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').'';
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_PROFIL'],$FeldBreiten['GSV_KEY'],'',$Link);

			$Link = './gutschein_Main.php?cmdAktion=Details&Sort=GSK_KST'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSK_KST'))?'~':'').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').'';
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_KOSTENSTELLE'],$FeldBreiten['GSK_KST'],'',$Link);

			$Link = './gutschein_Main.php?cmdAktion=Details&Sort=GKU_KONZERN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GKU_KONZERN'))?'~':'').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').'';
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_KONZERN'],$FeldBreiten['GKU_KONZERN'],'',$Link);

			$Link = './gutschein_Main.php?cmdAktion=Details&Sort=GSK_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSK_STATUS'))?'~':'').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').'';
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_STATUS'],$FeldBreiten['GSK_STATUS'],'',$Link);
			$Form->ZeileEnde();

			$DS=0;
			while(!$rsGSK->EOF())
			{
				$Form->ZeileStart();
				$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='.$rsGSK->FeldInhalt('GSK_AUFTRAGNR');
				$Form->Erstelle_ListenFeld('GSK_AUFTRAGNR',$rsGSK->FeldInhalt('GSK_AUFTRAGNR'),0,$FeldBreiten['GSK_AUFTRAGNR'],false,($DS%2),'',$Link);
				$Form->Erstelle_ListenFeld('GSV_KEY',$rsGSK->FeldInhalt('GSV_BEZEICHNUNG'),0,$FeldBreiten['GSV_KEY'],false,($DS%2),'',$Link);
				$Form->Erstelle_ListenFeld('GSK_KST',$rsGSK->FeldInhalt('GSK_KST'),0,$FeldBreiten['GSK_KST'],false,($DS%2),'','');
				$Form->Erstelle_ListenFeld('GKU_KONZERN',$rsGSK->FeldInhalt('GKU_KONZERN'),0,$FeldBreiten['GKU_KONZERN'],false,($DS%2),'','');
				$Form->Erstelle_ListenFeld('GSK_STATUS',$rsGSK->FeldInhalt('GSK_STATUS'),0,$FeldBreiten['GSK_STATUS'],false,($DS%2),'','');
				$Form->ZeileEnde();

				$DS++;
				$rsGSK->DSWeiter();
			}
			$Form->Trennzeile('O');
			$Form->Formular_Ende();
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			$Form->SchaltflaechenEnde();

			$Link = './gutschein_Main.php?cmdAktion=Details';
			$Form->SchreibeHTMLCode('<form name="frmDetails" action="' . $Link . '" method=POST  enctype="multipart/form-data">');
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
			$Form->SchreibeHTMLCode('</form>');
		}
		elseif($rsGSKZeilen==1 AND !isset($_GET['Seite']))//Einen einzelnen Datensatz anzeigen
		{
			$AWISBenutzer->ParameterSchreiben('Formular_GSD',serialize($Param));

			$Form->Erstelle_HiddenFeld('GSK_AUFTRAGNR', $AWIS_KEY1);
			$Form->SchreibeHTMLCode("<form name=frmSuche method=post action./gutschein_Main.php?cmdAktion=Details&Seite=Gutscheine>");
			$Form->Formular_Start();
			
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./gutschein_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsGSK->FeldInhalt('GSK_USER')));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsGSK->FeldInhalt('GSK_USERDAT')));
			$Form->InfoZeile($Felder,'');

			$EditRecht = false;

			//Auftragsnummer
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_AUFTRAGNUMMER'].':',190);
			$Form->Erstelle_TextFeld('GSK_AUFTRAGNR',(isset($_POST['txtGSK_AUFTRAGNR'])?$_POST['txtGSK_AUFTRAGNR']:$rsGSK->FeldInhalt('GSK_AUFTRAGNR')),50,400,$EditRecht);
			$AWISCursorPosition='GSK_AUFTRAGNR';
			$Form->ZeileEnde();

			//Auftragsstatus aufgesplittet
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_AUFTRAGSTATUS'].':',190);
			$Daten = explode("|",$AWISSprachKonserven['GSD']['lst_GSD_KOPFSTATUS']);

			for($i=0;$i < count($Daten,0);$i++)
			{
				$DatenDetails = explode("~", $Daten[$i]);

				if($rsGSK->FeldInhalt('GSK_STATUS')==$DatenDetails[0])
				{
					$Form->Erstelle_TextFeld('GSK_STATUS',$rsGSK->FeldInhalt('GSK_STATUS').' => '.$DatenDetails[1],50,150);
				}
			}
			$Form->ZeileEnde();

			//Kostenstelle
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KOSTENSTELLE'].':',190);
			$Form->Erstelle_TextFeld('GSK_KST',isset($_POST['txtGSK_KST'])?$_POST['txtGSK_KST']:$rsGSK->FeldInhalt('GSK_KST'),50,400,$EditRecht);
			$Form->ZeileEnde();

			//Gutscheinprofil
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_VORLAGE'].':',190);
			$Form->Erstelle_TextFeld('GSV_BEZEICHNUNG',isset($_POST['txtGSK_GSV_KEY'])?$_POST['txtGSK_GSV_KEY']:$rsGSK->FeldInhalt('GSV_BEZEICHNUNG'),50,400,$EditRecht);
			$Form->ZeileEnde();

			//Dateiname => Auftraggeber_Kostenstelle_Kunde (INTERN oder EXTERN)
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_DATEINAME'].':',190);
			$Form->Erstelle_TextFeld('GSK_DATEINAME',isset($_POST['txtGSK_DATEINAME'])?$_POST['txtGSK_DATEINAME']:$rsGSK->FeldInhalt('GSK_DATEINAME'),50,400,$EditRecht);
			$Form->ZeileEnde();

			//Freitextfeld
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_FREITEXTFELD'].':',190);
			$Form->Erstelle_TextFeld('GSK_FREITEXT',$rsGSK->FeldInhalt('GSK_FREITEXT'),50,400,$EditRecht);
			$Form->ZeileEnde();

			//Gutscheinkunde extern oder ATU Intern
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KUNDE'].':',190);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Trennzeile();
			$Form->ZeileEnde();

			if(($rsGSK->FeldInhalt('GSK_GKU_KEY')!= null) AND $rsGSK->FeldInhalt('GSK_GKU_KEY')!='')
			{
				//Adresse und Ansprechpartner der externen Firma
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_KUNDENNR'] . ':',130);
				$Form->Erstelle_TextFeld('GKU_KUNDENNR',(isset($_POST['txtGKU_KUNDENNR'])?$_POST['txtGKU_KUNDENNR']:$rsGSK->FeldInhalt('GSK_GKU_KEY')),50,280);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_VERTRAGNR'] . ':',130);
				$Form->Erstelle_TextFeld('GKU_VERTRAGNR',(isset($_POST['txtGKU_VERTRAGNR'])?$_POST['txtGKU_VERTRAGNR']:$rsGSK->FeldInhalt('GKU_VERTRAGNR')),50,280);
				$Form->Trennzeile('O');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_ANREDE'] . ':', 130);
				$Daten = explode('|', $AWISSprachKonserven['GKU']['lst_GKU_ANREDE']);
				$Form->Erstelle_SelectFeld('GKU_ANREDE', isset($_POST['txtGKU_ANREDE'])?$_POST['txtGKU_ANREDE']:$rsGSK->FeldInhalt('GKU_ANREDE'), 280, false, '','', '', '', '', $Daten, '');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_POSTFACH'].' / '.$AWISSprachKonserven['GKU']['GKU_POSTFACHPLZ'].':',180);
				$Form->Erstelle_TextFeld('GKU_POSTFACH',(isset($_POST['txtGKU_POSTFACH'])?$_POST['txtGKU_POSTFACH']:$rsGSK->FeldInhalt('GKU_POSTFACH')).' '.(isset($_POST['txtGKU_POSTFACHPLZ'])?$_POST['txtGKU_POSTFACHPLZ']:$rsGSK->FeldInhalt('GKU_POSTFACHPLZ')),50,200);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_TITEL'].':',130);
				$Form->Erstelle_TextFeld('GKU_TITEL',isset($_POST['txtGKU_TITEL'])?$_POST['txtGKU_TITEL']:$rsGSK->FeldInhalt('GKU_TITEL'),50,280);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_TELEFON'].':',180);
				$Form->Erstelle_TextFeld('GKU_TELEFON',isset($_POST['txtGKU_TELEFON'])?$_POST['txtGKU_TELEFON']:$rsGSK->FeldInhalt('GKU_TELEFON'),50,200);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_NAME1'].':',130);
				$Form->Erstelle_TextFeld('GKU_NAME1',isset($_POST['txtGKU_NAME1'])?$_POST['txtGKU_NAME1']:$rsGSK->FeldInhalt('GKU_NAME1'),50,280);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_MOBILTELEFON'].':',180);
				$Form->Erstelle_TextFeld('GKU_MOBILTELEFON',isset($_POST['txtGKU_MOBILTELEFON'])?$_POST['txtGKU_MOBILTELEFON']:$rsGSK->FeldInhalt('GKU_MOBILTELEFON'),50,200);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_NAME2'].':',130);
				$Form->Erstelle_TextFeld('GKU_NAME2',isset($_POST['txtGKU_NAME2'])?$_POST['txtGKU_NAME2']:$rsGSK->FeldInhalt('GKU_NAME2'),50,280);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_FAX'].':',180);
				$Form->Erstelle_TextFeld('GKU_FAX',isset($_POST['txtGKU_FAX'])?$_POST['txtGKU_FAX']:$rsGSK->FeldInhalt('GKU_FAX'),50,200);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_KONZERN'].':',130);
				$Form->Erstelle_TextFeld('GKU_KONZERN',isset($_POST['txtGKU_KONZERN'])?$_POST['txtGKU_KONZERN']:$rsGSK->FeldInhalt('GKU_KONZERN'),50,280);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_EMAIL'].':',180);
				$Form->Erstelle_TextFeld('GKU_EMAIL',isset($_POST['txtGKU_EMAIL'])?$_POST['txtGKU_EMAIL']:$rsGSK->FeldInhalt('GKU_EMAIL'),50,300);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_STRASSE'].':',130);
				$Form->Erstelle_TextFeld('GKU_STRASSE',(isset($_POST['txtGKU_STRASSE'])?$_POST['txtGKU_STRASSE']:$rsGSK->FeldInhalt('GKU_STRASSE')).' '.(isset($_POST['txtGKU_HN'])?$_POST['txtGKU_HN']:$rsGSK->FeldInhalt('GKU_HN')),50,280);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GKU']['GKU_PLZ'].' / '.$AWISSprachKonserven['GKU']['GKU_ORT'].':',130);
				$Form->Erstelle_TextFeld('GKU_PLZ',(isset($_POST['txtGKU_PLZ'])?$_POST['txtGKU_PLZ']:$rsGSK->FeldInhalt('GKU_PLZ')).' '.(isset($_POST['txtGKU_ORT'])?$_POST['txtGKU_ORT']:$rsGSK->FeldInhalt('GKU_ORT')),50,280);
				$Form->ZeileEnde();
				$Form->Trennzeile('O');
			}
			else
			{
				//Interne Abteilung bzw. Person die Gutscheine erhaelt
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['GSD']['GSD_KUNDE_INTERN'].':',190);
				$Form->Erstelle_TextFeld('GSK_KUNDE_INTERN',(isset($_POST['txtGSK_KUNDE_INTERN'])?$_POST['txtGSK_KUNDE_INTERN']:$rsGSK->FeldInhalt('GSK_KUNDE_INTERN')),50,400,$EditRecht);
				$Form->ZeileEnde();
				$Form->Trennzeile('O');
			}

			//Pruefung ob alle GSNr bereits gezogen wurden => Anzeige PDF-Button
			//Positionsdaten anzeigen
			$SQL_PDF_POS  ='Select sum(pos.GSP_MENGE) as MENGE';
			$SQL_PDF_POS .=' From GUTSCHEINPOSDATEN pos';
			$SQL_PDF_POS .=' Where pos.GSP_AUFTRAGNR = '.($Param['GSD_AUFTRAGNUMMER']===0?'':$rsGSK->FeldInhalt('GSK_AUFTRAGNR'));
			$rsPDF_POS = $DB->RecordSetOeffnen($SQL_PDF_POS);

			$SQL_PDF_GS  ='Select count(gs.GPD_KEY) as MENGE';
			$SQL_PDF_GS .=' From GUTSCHEINE gs';
			$SQL_PDF_GS .=' Where gs.GPD_AUFTRAGNR = '.($Param['GSD_AUFTRAGNUMMER']===0?'':$rsGSK->FeldInhalt('GSK_AUFTRAGNR'));
			$rsPDF_GS = $DB->RecordSetOeffnen($SQL_PDF_GS);



			//Positionsdaten anzeigen
			$SQL_POS = 'SELECT GSP_KEY';
			$SQL_POS .= ', GSP_POSITION';
			$SQL_POS .= ', GSP_AUFTRAGNR';
			$SQL_POS .= ', GSP_MENGE';
			$SQL_POS .= ', GSP_BETRAG*GSP_MENGE as GSP_BETRAGGES';
			$SQL_POS .= ', GSP_BETRAG';
			$SQL_POS .= ', GSK_STATUS';
			$SQL_POS .= ', GSP_STATUS';
			$SQL_POS .= ', GSP_BUCHART';
			$SQL_POS .= ', GSV_WAEHRUNG';
			$SQL_POS .= ' FROM GUTSCHEINPOSDATEN';
			$SQL_POS .= ' INNER JOIN GUTSCHEINKOPFDATEN ON GSP_AUFTRAGNR = GSK_AUFTRAGNR';
			$SQL_POS .= ' LEFT JOIN GUTSCHEINVORLAGE ON GSK_GSV_KEY = GSV_KEY';
			$SQL_POS .= ' WHERE GSP_AUFTRAGNR=' . $DB->WertSetzen('GSP','N0',($Param['GSD_AUFTRAGNUMMER']===0?'':$rsGSK->FeldInhalt('GSK_AUFTRAGNR')),false) . '';
			$SQL_POS .= ' GROUP BY GSP_KEY';
			$SQL_POS .= ', GSP_POSITION';
			$SQL_POS .= ', GSP_Auftragnr';
			$SQL_POS .= ', GSP_BETRAG';
			$SQL_POS .= ', GSP_AUFTRAGNR';
			$SQL_POS .= ', GSK_STATUS';
			$SQL_POS .= ', GSP_MENGE';
			$SQL_POS .= ', GSP_STATUS';
			$SQL_POS .= ', GSP_BUCHART';
			$SQL_POS .= ', GSV_WAEHRUNG';
			$SQL_POS .= ' ORDER BY GSP_POSITION asc';
			$rsGSP = $DB->RecordSetOeffnen($SQL_POS,$DB->Bindevariablen('GSP',true));
			$rsGSPZeilen = $rsGSP->AnzahlDatensaetze();
			$DS=0;

			$Form->Formular_Start();
			$Form->ZeileStart();
			$Link = './gutschein_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_POSITIONEN'], 100, '', $Link);

			$Link = './gutschein_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_AUFTRAGNUMMER'], 160,'',$Link);

			$Link = './gutschein_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_POSANZAHL'], 100, '', $Link);

			$Link = './gutschein_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_BETRAG'], 150, '', $Link);

			$Link = './gutschein_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_BETRAGGES'], 150, '', $Link);

			$Link = './gutschein_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_BUCHUNGSART'], 120, '', $Link);

			$Link = './gutschein_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GSD']['GSD_STATUS'], 100, '', $Link);
			$Form->ZeileEnde();

			for ($GSPZeile = 0; $GSPZeile < $rsGSPZeilen; $GSPZeile++)
			{
				$Form->ZeileStart();
				$Link = './gutschein_Main.php?cmdAktion=Details&GSK_AUFTRAGNR='. ($rsGSP->FeldInhalt('GSP_AUFTRAGNR') === ''?'':$rsGSP->FeldInhalt('GSP_AUFTRAGNR')) .'&Seite=Gutscheine&GSP_KEY=' . ($rsGSP->FeldInhalt('GSP_KEY') === ''?'':$rsGSP->FeldInhalt('GSP_KEY'));
				$Form->Erstelle_ListenFeld('GSP_POSITION', $rsGSP->FeldInhalt('GSP_POSITION'), 0, 100, false, ($DS % 2), '', $Link);
				$Form->Erstelle_ListenFeld('GSP_AUFTRAGNR', $rsGSP->FeldInhalt('GSP_AUFTRAGNR'), 0, 160, false, ($DS % 2), '', '');
				$Form->Erstelle_ListenFeld('GSP_ANZAHL', $rsGSP->FeldInhalt('GSP_MENGE'), 0, 100, false, ($DS % 2), '', '');
				$Form->Erstelle_ListenFeld('GSP_BETRAG', 'a` ' . $rsGSP->FeldInhalt('GSP_BETRAG','N2') . ' ' . $rsGSP->FeldInhalt('GSV_WAEHRUNG','T'), 0, 150, false, ($DS % 2), '', '');
				$Form->Erstelle_ListenFeld('GSP_BETRAGGES', $rsGSP->FeldInhalt('GSP_BETRAGGES','N2') . ' ' . $rsGSP->FeldInhalt('GSV_WAEHRUNG','T'), 0, 150, false, ($DS % 2), '', '');
				$Form->Erstelle_ListenFeld('GSP_BUCHART', $rsGSP->FeldInhalt('GSP_BUCHART'), 0, 120, false, ($DS % 2), '', '');
				$Form->Erstelle_ListenFeld('GSP_STATUS', $rsGSP->FeldInhalt('GSP_STATUS'), 0, 100, false, ($DS % 2), '', '');
				$Form->ZeileEnde();

				$rsGSP->DSWeiter();
				$DS++;
			}
			//Schaltfl�chen
			$Form->Formular_Ende();
			$Form->Trennzeile('O');

			// Schaltfl�chen f�r dieses Register
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			if(($Recht28000&4) == 4)
			{
				$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');

				if(($rsPDF_GS->FeldInhalt('MENGE')==$rsPDF_POS->FeldInhalt('MENGE')))
				{
					//PDF nur anzeigen wenn alle GSNummern gezogen wurden
					$LinkPDF ='/berichte/drucken.php?XRE=47&ID=' . base64_encode('&GSP_AUFTRAGNR=' . ($Param['GSD_AUFTRAGNUMMER']===0?'':$rsGSK->FeldInhalt('GSK_AUFTRAGNR'))) . '&GSK_AUFTRAGNR=' . ($Param['GSD_AUFTRAGNUMMER']===0?'':$rsGSK->FeldInhalt('GSK_AUFTRAGNR'). '&ART=' . 'Gutschein');
					$Form->Schaltflaeche('href','cmdPDF', $LinkPDF,'/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
					$LinkPDF ='/berichte/drucken.php?XRE=47&ID=' . base64_encode('&GSP_AUFTRAGNR=' . ($Param['GSD_AUFTRAGNUMMER']===0?'':$rsGSK->FeldInhalt('GSK_AUFTRAGNR'))) . '&GSK_AUFTRAGNR=' . ($Param['GSD_AUFTRAGNUMMER']===0?'':$rsGSK->FeldInhalt('GSK_AUFTRAGNR'). '&ART=' . 'Lieferschein');
					$Form->Schaltflaeche('href','cmdPDF', $LinkPDF,'/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);

					//Exportbutton Textdatei Semikolon getrennt mit GSNr+PruefZiffer;Betrag
					$Link='./gutschein_csv.php?GSK_AUFTRAGNR='.$rsGSK->FeldInhalt('GSK_AUFTRAGNR');
					$Form->Schaltflaeche('href', 'cmdExportCSV', $Link, '/bilder/cmd_typcsv.png', $AWISSprachKonserven['Wort']['lbl_export'], 'C','',27,27);
				}
			}
			$Form->SchaltflaechenEnde();
			$Form->SetzeCursor($AWISCursorPosition);
			$Form->SchreibeHTMLCode('</form>');
		}
		$Form->Trennzeile('O');
		$Form->SchreibeHTMLCode('</form>');
		$Form->SetzeCursor($AWISCursorPosition);
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWISBenutzer;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $DB;
	global $Form;

	$Textkonserven = array();
	$Textkonserven[]=array('Wort','Auswahl_ALLE');
	$AWISSprachKonserven=$Form->LadeTexte($Textkonserven);

	$Bedingung = '';
	
	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND gskopf.GSK_AUFTRAGNR = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}
	if(isset($Param['GSD_GUTSCHEINNUMMER']) AND $Param['GSD_GUTSCHEINNUMMER'])
	{
		$Bedingung .= ' AND (UPPER(gspos.GSP_GUTSCHEINNR) ' . $DB->LIKEoderIST($Param['GSP_GUTSCHEINNUMMER'].'%',1) . ')';
	}
	if(isset($Param['GSD_AUFTRAGNUMMER']) AND $Param['GSD_AUFTRAGNUMMER']!='')
	{
		$Bedingung .= ' AND (UPPER(gskopf.GSK_AUFTRAGNR) ' . $DB->LIKEoderIST($Param['GSD_AUFTRAGNUMMER']) . ') ';
	}
	if(isset($Param['GSD_AUFTRAGSTATUS']) AND $Param['GSD_AUFTRAGSTATUS']!='0' AND $Param['GSD_AUFTRAGSTATUS'] != $AWISSprachKonserven['Wort']['Auswahl_ALLE'])
	{
		$Bedingung .= ' AND (UPPER(gskopf.GSK_STATUS) ' . $DB->LIKEoderIST($Param['GSD_AUFTRAGSTATUS']) . ') ';
	}
	if(isset($Param['GSD_KOSTENSTELLE']) AND $Param['GSD_KOSTENSTELLE']!='')
	{
		$Bedingung .= ' AND (UPPER(gskopf.GSK_KST) ' . $DB->LIKEoderIST($Param['GSD_KOSTENSTELLE']) . ') ';
	}
	if(isset($Param['GSD_GKU_KEY']) AND $Param['GSD_GKU_KEY']!='0'  AND $Param['GSD_GKU_KEY'] != $AWISSprachKonserven['Wort']['Auswahl_ALLE'])
	{
		$Bedingung .= ' AND (UPPER(gskopf.GSK_GKU_Key) ' . $DB->LIKEoderIST($Param['GSD_GKU_KEY']) . ') ';
	}
	if(isset($Param['GSD_KUNDE_INTERN']) AND $Param['GSD_KUNDE_INTERN']!='')
	{
		$Bedingung .= ' AND (UPPER(gskopf.GSK_KUNDE_INTERN) ' . $DB->LIKEoderIST($Param['GSD_KUNDE_INTERN'].'%',1) . ') ';
	}
	if(isset($Param['GSD_AUFTRAGGEBER']) AND $Param['GSD_AUFTRAGGEBER'])
	{
		$Bedingung .= ' AND (UPPER(pers.PERSNR) ' . $DB->LIKEoderIST($Param['GSD_AUFTRAGGEBER']) . ') ';
	}
	if(isset($Param['GSD_PROFIL']) AND $Param['GSD_PROFIL']!='0')
	{
		$Bedingung .= ' AND (UPPER(gsvor.GSV_KEY) ' . $DB->LIKEoderIST($Param['GSD_PROFIL']) . ') ';
	}
    if(isset($Param['GSD_BUCHUNGSART']) AND $Param['GSD_BUCHUNGSART'])
    {
        $Bedingung .= ' AND (UPPER(gspos.GSP_BUCHART) ' . $DB->LIKEoderIST($Param['GSD_BUCHUNGSART']) . ') ';
    }
	return $Bedingung;
}
?>