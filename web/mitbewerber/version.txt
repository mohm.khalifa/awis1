#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Mitbewerber
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
#Target=_self
ErrorMail=entwick

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.00.01;Erste Testversion;06.03.2007;<a href=mailto:Entwick>Sacha Kerres</a>

