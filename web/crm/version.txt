#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=CRM
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
#Target=_self
ErrorMail=edv-helpdesk@de.atu.eu
Sprachen=DE,FR,CZ,NL,IT

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.14.00;Ausdruck der Konditionsbl�tter nach Rabattgruppen;04.08.2010;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.13.00;Konditionen: Teilehandel hinzugef�gt;21.07.2010;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.12.00;Suche erweitert und Seitenumbruch bei Reifenkonditionen (Konditionsblatt);12.03.2010;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.11.00;Firma 3 aus Neuen Vertr�gen mit speichern bei Daten�bernahme;15.02.2010;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.10.00;Konditionen: Komplettr�der freischalten und Komplett freischalten hinzugef�gt;11.02.2010;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.09.00;Unterst�tzung f�r mehrere E-Mails pro AP in Kontakten;10.11.2009;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.08.00;Nur Adressen mit Status K(=Kunde) bei Massendruck drucken;14.09.2009;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.07.00;Neue Stammdatentexte und Konditionsblatt ge�ndert;13.08.2009;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.06.00;Daten�bernahme von Neuen Vertr�gen ins CRM hinzugef�gt;23.07.2009;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.05.00;Sortierung bei Wiedervorlagen berichtigt;08.07.2009;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
0.04.00;Neue Felder beim Vertragsstatus eingef�hrt;28.09.2008;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.03.00;Berichte freigegeben;03.06.2008;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.02.00;Fehler beim Potential entfernt;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.01.00;Erste Testversion;17.11.2007;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.00.01;Erste Version;30.08.2007;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>

