<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');
    require_once 'mobilitaetsgarantie_funktionen.inc';

    global $AWISCursorPosition;
    global $MOB;

    try {
    $MOB = new mobilitaetsgarantie_funktionen();

    if(isset($_POST['cmdPDF_x'])){
        $_GET['XRE'] = 69;
        $_GET['ID'] = 'bla';

        require_once '/daten/web/berichte/drucken.php';
    }

    echo "<link rel=stylesheet type=text/css href='" . $MOB->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $MOB->AWISSprachKonserven['TITEL']['tit_Mobilitaetsgarantie'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

$MOB->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(56000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$MOB->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($MOB->Form instanceof awisFormular) {
        $MOB->Form->DebugAusgabe(1, $ex->getSQL());
        $MOB->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $MOB->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($MOB->Form instanceof awisFormular) {
        $MOB->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>