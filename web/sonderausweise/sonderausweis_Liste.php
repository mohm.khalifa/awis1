<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;
global $SortAlt;
global $txtAktiv;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

if(substr(strstr($_SERVER['HTTP_REFERER'],'&'),1,7)=='txtSort')
{
	$SortAlt=substr(strstr($_SERVER['HTTP_REFERER'],'&'),9,strlen(strstr($_SERVER['HTTP_REFERER'],'&'))-9);
}

$Rechtestufe = awisBenutzerRecht($con,1700);

if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Sonderausweis',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}


$Gruppenrecht = awisBenutzerRecht($con, 1701);
		//  1=externe Abrechnungen
		//	2=Zeitarbeitsfirmen
		//	4=externe Reinigungsfirmen
		//  8=Sonderausweise
		//  16=Sonstige Filialen
		//  32=Besucher/Lieferanten
		//  64=PDF erstellen
		//  128=Zeitarbeitsfirmen Ausland
		//  256=externe Abrechnungen 2
		
if($Gruppenrecht==0)
{
    awisEreignis(3,1000,'Sonderausweis_Gruppenrecht',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Gruppenrechte!</span>");
}

awis_Debug(1,$_REQUEST);

if(isset($_POST['cmdSuche_x']))			
{
	$Params[0] = $_POST['txtPersonalnummer'];
	$Params[1] = $_POST['txtName'];
	$Params[2] = $_POST['txtEAN'];
	$Params[3] = $_POST['txtFirma'];
	$Params[4] = $_POST['txtFIL_ID'];
	$Params[5] = $_POST['txtStra�e'];
	$Params[6] = $_POST['txtPLZ'];
	$Params[7] = $_POST['txtOrt'];
	$Params[8] = $_POST['txtBereich'];
	$Params[9] = $_POST['txtAWGruppe'];
	$Params[10] = $_POST['txtTaetigkeit'];
	$Params[11] = $_POST['txtEintritt'];
	$Params[12] = $_POST['txtAustritt'];
	$Params[13] = $_POST['txtGueltigVon'];
	$Params[14] = $_POST['txtGueltigBis'];
	$Params[15] = $_POST['txtVorname'];
	
	awis_BenutzerParameterSpeichern($con, "Sonderausweis_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
}
else
{
		$Params=explode(';',awis_BenutzerParameter($con, "Sonderausweis_Suche", $AWISBenutzer->BenutzerName()));
}

awis_Debug(1,$Params);

awis_ZeitMessung(0);

$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

if(!isset($_REQUEST['txtSort']))
{
	//$SQL .= ' ORDER BY SAW_PER_NR ASC';
	$Sort='SAW_PER_NR ASC';
}
else
{
	if($SortAlt!=$_REQUEST['txtSort'])
	{
		//$SQL .= ' ORDER BY '.$_REQUEST['txtSort'].' ASC';
		$Sort=$_REQUEST['txtSort'].' ASC';
	}
	else 
	{
		//$SQL .= ' ORDER BY '.$_REQUEST['txtSort'].' DESC';
		$Sort=$_REQUEST['txtSort'].' DESC';
	}
}


$SQL = "SELECT * FROM (SELECT SAW_KEY, SAW_PER_NR, SAW_EAN, SAW_FIL_ID, SAW_FIRMA, SAW_NAME, SAW_VORNAME, SAW_TAETIGKEIT, ";
$SQL .= "SAW_SAG_KEY, SAG_GRUPPE, SAW_EINTRITT, SAW_AUSTRITT, SAW_GUELTIGAB, SAW_GUELTIGBIS, to_char(SAW_USERDAT,'DD.MM.RR HH24:MI.SS') AS USERDAT, ";
$SQL .= "ROW_NUMBER() OVER (ORDER BY ".$Sort.") AS RNUM ";
$SQL .= "FROM Sonderausweise join Sonderausweisgruppen on(SAW_SAG_KEY=SAG_KEY(+))";

$Bedingung='';
$Zusatz=FALSE;

/*SQL-Zusatz Personalnummer*/
if($Params[0]!='')
{
		$Bedingung='SAW_PER_NR ' . awisLIKEoderIST($Params[0],TRUE,FALSE,FALSE,0);
		$Zusatz=TRUE;
}

/*SQL-Zusatz Name*/
if($Zusatz)
{
		if($Params[1]!='')
		{
				$Bedingung.=' AND upper(SAW_NAME) ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[1]!='')
		{
				$Bedingung.='upper(SAW_NAME) ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz EAN*/
if($Zusatz)
{
		if($Params[2]!='')
		{
				$Bedingung.=' AND SAW_EAN ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[2]!='')
		{
				$Bedingung.='SAW_EAN ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz Firma*/
if($Zusatz)
{
		if($Params[3]!='')
		{
				$Bedingung.=' AND upper(SAW_FIRMA) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[3]!='')
		{
				$Bedingung.='upper(SAW_FIRMA) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz FIL_ID*/
if($Zusatz)
{
		if($Params[4]!='')
		{
				$Bedingung.=' AND SAW_FIL_ID ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[4]!='')
		{
				$Bedingung.='SAW_FIL_ID ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz Stra�e*/
if($Zusatz)
{
		if($Params[5]!='')
		{
				$Bedingung.=' AND upper(SAW_STRASSE) ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[5]!='')
		{
				$Bedingung.='upper(SAW_STRASSE) ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz PLZ*/
if($Zusatz)
{
		if($Params[6]!='')
		{
				$Bedingung.=' AND SAW_PLZ ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[6]!='')
		{
				$Bedingung.='SAW_PLZ ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz Ort*/
if($Zusatz)
{
		if($Params[7]!='')
		{
				$Bedingung.=' AND upper(SAW_ORT) ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[7]!='')
		{
				$Bedingung.='upper(SAW_ORT) ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz Bereich*/
if($Zusatz)
{
		if($Params[8]!='')
		{
				$Bedingung.=' AND upper(SAW_BEREICH) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[8]!='')
		{
				$Bedingung.='upper(SAW_BEREICH) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz AWGruppe*/
if($Zusatz)
{
		if($Params[9]!='')
		{
				$Bedingung.=' AND SAW_SAG_KEY ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[9]!='')
		{
				$Bedingung.='SAW_SAG_KEY ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz Taetigkeit*/
if($Zusatz)
{
		if($Params[10]!='')
		{
				$Bedingung.=' AND SAW_TAETIGKEIT ' . awisLIKEoderIST($Params[10],FALSE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[10]!='')
		{	
				$Bedingung.='SAW_TAETIGKEIT ' . awisLIKEoderIST($Params[10],FALSE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz Eintritt / Austritt */
If($Params[11]!='')
{	
	If($Params[12]=='')
	{
		if($Zusatz)
		{
			$Bedingung.=" AND SAW_EINTRITT=to_date('". $Params[11] ."','DD.MM.RR')";
		}
		else
		{
			$Bedingung.="SAW_EINTRITT=to_date('". $Params[11] ."','DD.MM.RR')";
			$Zusatz=TRUE;
		}
	}
	else
	{
		if($Zusatz)
		{
			$Bedingung.=" AND SAW_EINTRITT>=to_date('". $Params[11] ."','DD.MM.RR') AND SAW_AUSTRITT<=to_date('". $Params[12] ."','DD.MM.RR')";
		}
		else
		{
			$Bedingung.="SAW_EINTRITT>=to_date('". $Params[11] ."','DD.MM.RR') AND SAW_AUSTRITT<=to_date('". $Params[12] ."','DD.MM.RR')";
			$Zusatz=TRUE;
		}
	}
}
elseif($Zusatz)
{
		if($Params[12]!='')
		{
				$Bedingung.=" AND SAW_AUSTRITT=to_date('". $Params[12] ."','DD.MM.RR')";
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[12]!='')
		{	
				$Bedingung.="SAW_AUSTRITT=to_date('". $Params[12] ."','DD.MM.RR')";
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz GueltigVon / GueltigBis */
If($Params[13]!='')
{	
	If($Params[14]=='')
	{
		if($Zusatz)
		{
			$Bedingung.=" AND SAW_GUELTIGAB=to_date('". $Params[13] ."','DD.MM.RR')";
		}
		else
		{
			$Bedingung.="SAW_GUELTIGAB=to_date('". $Params[13] ."','DD.MM.RR')";
			$Zusatz=TRUE;
		}
	}
	else
	{
		if($Zusatz)
		{
			$Bedingung.=" AND SAW_GUELTIGAB>=to_date('". $Params[13] ."','DD.MM.RR') AND SAW_GUELTIGBIS<=to_date('". $Params[14] ."','DD.MM.RR')";
		}
		else
		{
			$Bedingung.="SAW_GUELTIGAB>=to_date('". $Params[13] ."','DD.MM.RR') AND SAW_GUELTIGBIS<=to_date('". $Params[14] ."','DD.MM.RR')";
			$Zusatz=TRUE;
		}
	}
}
elseif($Zusatz)
{
		if($Params[14]!='')
		{
				$Bedingung.=" AND SAW_GUELTIGBIS=to_date('". $Params[14] ."','DD.MM.RR')";
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[14]!='')
		{	
				$Bedingung.="SAW_GUELTIGBIS=to_date('". $Params[14] ."','DD.MM.RR')";
				$Zusatz=TRUE;
		}
}

/*SQL-Zusatz Vorame*/
if($Zusatz)
{
		if($Params[15]!='')
		{
				$Bedingung.=' AND upper(SAW_VORNAME) ' . awisLIKEoderIST($Params[15],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}
else
{
		if($Params[15]!='')
		{
				$Bedingung.='upper(SAW_VORNAME) ' . awisLIKEoderIST($Params[15],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
}

if($Zusatz)
{
	$Bedingung.=' AND SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,'.$Gruppenrecht.')=SAG_RECHTEBIT)';
	$Zusatz=TRUE;
}
else
{
	$Bedingung.='SAW_SAG_KEY in(SELECT SAG_KEY FROM SONDERAUSWEISGRUPPEN WHERE BITAND(SAG_RECHTEBIT,'.$Gruppenrecht.')=SAG_RECHTEBIT)';
	$Zusatz=TRUE;
}

	
if($Bedingung!='')
{
		$SQL .= ' WHERE ' . $Bedingung . ' ) WHERE RNUM<=' . $AwisBenuPara;
}
else
{
		$SQL .= ' )WHERE RNUM<=' . $AwisBenuPara;
}

awis_Debug(1,$SQL);

$rsSonderausweis = awisOpenRecordset($con,$SQL);
$rsSonderausweisZeilen = $awisRSZeilen;

if($rsSonderausweisZeilen==0)		// Keine Daten
{
	echo 'Es wurde kein Eintrag gefunden.';
}
else
{
	echo "<form name=frmSonderausweis method=post action=./sonderausweis_Main.php?cmdAktion=Detail>";
	
	// �berschrift aufbauen
	echo "<table width=100% id=DatenTabelle border=1><tr>";
	if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
	{
		echo "<td id=FeldBez width=30></td>";
	}					
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_PER_NR>Pers.-Nr.</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_EAN>EAN-Nr.</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_FIL_ID>Filial-Nr</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_FIRMA>Firma</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_NAME>Name</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_TAETIGKEIT>T&auml;tigkeit</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAG_GRUPPE>Ausweisgruppe</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_EINTRITT>Eintritt</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_AUSTRITT>Austritt</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_GUELTIGAB>G&uuml;ltig Ab</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=SAW_GUELTIGBIS>G&uuml;ltig Bis</a></td>";
	echo "<td id=FeldBez><a id=Sort href=./sonderausweis_Main.php?cmdAktion=Liste&txtSort=USERDAT>Bearbeitet</a></td>";
	/*if(($Rechtestufe&8)==8){
			echo "<td id=FeldBez width=30></td>";
	}*/
	if(($Gruppenrecht&64)==64){
			echo "<td id=FeldBez width=30></td>";
	}
	echo "</tr>";
	
	for($i=0;$i<$rsSonderausweisZeilen;$i++)
	{
		if (!empty($rsSonderausweis['SAW_AKTIV'][$i]))
		{
			$txtAktiv=$rsSonderausweis['SAW_AKTIV'][$i];
		}
		else{
			$txtAktiv='';
		}
	
		echo '<tr>';
		if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
		{
			echo "<td align=center " . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) . "><input type=image border=0 src=/bilder/aendern.png accesskey=b name=cmdBearbeiten_".$rsSonderausweis['SAW_KEY'][$i]." title='Bearbeiten (Alt+B)'></td>";
		}
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_PER_NR'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_EAN'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'><a href=../filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID=' . $rsSonderausweis['SAW_FIL_ID'][$i] . '>' . $rsSonderausweis['SAW_FIL_ID'][$i] .  '</a></td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_FIRMA'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_NAME'][$i] . ' ' . $rsSonderausweis['SAW_VORNAME'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_TAETIGKEIT'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAG_GRUPPE'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_EINTRITT'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_AUSTRITT'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_GUELTIGAB'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['SAW_GUELTIGBIS'][$i] . '</td>';
		echo '<td ' . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) .'>' . $rsSonderausweis['USERDAT'][$i] . '</td>';
		/*if(($Rechtestufe&8)==8){
			echo "<td align=center " . ($rsSonderausweis['SAW_AKTIV'][$i]==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) ."><a href='./sonderausweis_Main.php?cmdAktion=Detail&txtSAW_KEY_loeschen=". $rsSonderausweis['SAW_KEY'][$i] ."'><img border=0 src=/bilder/muelleimer.png name=cmdLoeschen title='L&ouml;schen'></a></td>";
		}*/
		if(($Gruppenrecht&64)==64){
			if($txtAktiv!=1){
					echo "<td align=center " . ($txtAktiv==1?'id=TabellenZeileRot':(($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss')) ."><a target='_new' href=./sonderausweis_pdf.php?txtSAW_KEY=" . $rsSonderausweis['SAW_KEY'][$i] . "><img border=0 src=/bilder/pdf.png name=cmdErstelleSonderausweis title='Ausweis erstellen'></a></td>";
			}
			else
			{
				echo "<td  id=TabellenZeileRot></td>";
			}
		}
		/*else
		{
			echo "<td  id=TabellenZeileRot></td>";
		}*/
		echo '</tr>';
		
	}
		
	print "</table>";
	
	echo "</form>";
	
	if($i<$AwisBenuPara)
	{
		echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf('%.4f', awis_ZeitMessung(1)) . ' Sekunden gefunden!';
	}
	else
	{
	echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf('%.4f', awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
	}
}

?>
</body>
</html>

