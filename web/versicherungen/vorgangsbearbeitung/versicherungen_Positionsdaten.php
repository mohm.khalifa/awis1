<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('V_VERS_FORM_POSDATEN', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }
    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    $SQL = 'SELECT v.position,v.artnr,v.artkz,v.bez,v.menge,v.vk_preis, ';
    $SQL .= 'v.umsatz, v.rabattkenn ';
    $SQL .= 'FROM v_vers_form_posdaten v, versvorgangsstatus vv ';
    $SQL .= 'WHERE v.filnr = vv.vvs_filid AND ';
    $SQL .= 'v.wanr = vv.vvs_wanr AND ';
    $SQL .= 'vv.vvs_key = ' . $DB->WertSetzen('VVS', 'N0', $Param);

    $rsPosDat = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VVS'));

    $Form->SchreibeHTMLCode('<form name=frmVersicherungenPositionsdaten action=./versicherungen_Details.php?cmdAktion=Details&Seite=Positionsdaten' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

    $EditRecht = $Recht9500 & 2;

    $Form->ZeileStart();

    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['POSITION'], 80, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['ARTNR'], 120, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['ARTKZ'], 80, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['BEZ'], 550, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['MENGE'], 80, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['VK_PREIS'], 80, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['UMSATZ'], 80, '', '');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['V_VERS_FORM_POSDATEN']['RABATTKENN'], 80, '', '');

    $Form->ZeileEnde();

    $PosZeile = 0;

    while(!$rsPosDat->EOF())
    {
        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('POSITION', $rsPosDat->FeldInhalt('POSITION'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('ARTNR', $rsPosDat->FeldInhalt('ARTNR'), 0, 120, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('ARTKZ', $rsPosDat->FeldInhalt('ARTKZ'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('BEZ', $rsPosDat->FeldInhalt('BEZ'), 0, 550, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('MENGE', $rsPosDat->FeldInhalt('MENGE'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('VK_PREIS', $rsPosDat->FeldInhalt('VK_PREIS'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('UMSATZ', $rsPosDat->FeldInhalt('UMSATZ'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('RABATTKENN', $rsPosDat->FeldInhalt('RABATTKENN'), 0, 80, false, ($PosZeile % 2), '', '', 'T');
        $Form->ZeileEnde();

        $rsPosDat->DSWeiter();
        $PosZeile++;
    }
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200905132008");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
