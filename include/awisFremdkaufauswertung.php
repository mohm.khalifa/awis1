<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';

class awisFremdkaufauswertung
{

    const LF = PHP_EOL;
    const TZ = '{'; //War so angefordert
    const PFAD = '/daten/web/dokumente/fremdkaufauswertung/fremdkauf.csv';

    private $DB;

    private $AWISBenutzer;

    private $DebugLevel;

    public function __construct($Benutzer)
    {
        //Viele Fremdk�ufe..
        ini_set('memory_limit','512m');

        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
    }

    public function ErstelleAuswertung()
    {
        $this->Log('Hole Fremdkaufdaten.', 1);

        $SQL ='         select distinct';
        $SQL .='             FRK_OEN_NUMMER,';
        //Subselects weil angefordert war, dass nur die Erstbeste-Bezeichnung und Hersteller exportiert werden soll, auch wenn dann die Gefahr besteht, dass die Daten ungenau werden
        $SQL .='             (';
        $SQL .='                 select';
        $SQL .='                     BEZ.FRK_ARTBEZ';
        $SQL .='                 from';
        $SQL .='                     FREMDKAEUFE BEZ';
        $SQL .='                 where';
        $SQL .='                     BEZ.FRK_OEN_NUMMER = FRK.FRK_OEN_NUMMER and FRK_ARTBEZ is not null';
        $SQL .='                     and rownum = 1) as FRK_ARTBEZ,';
        $SQL .='             (';
        $SQL .='                 select';
        $SQL .='                     HST.FRK_HERSTELLER from FREMDKAEUFE HST where';
        $SQL .='                     HST.FRK_OEN_NUMMER = FRK.FRK_OEN_NUMMER and FRK_HERSTELLER is not null and rownum = 1) as FRK_HERSTELLER ,';
        $SQL .='             (';
        $SQL .='                 select';
        $SQL .='                     HST.FRK_FIRMA from FREMDKAEUFE HST where';
        $SQL .='                     HST.FRK_OEN_NUMMER = FRK.FRK_OEN_NUMMER and FRK_FIRMA is not null and rownum = 1) as FRK_FIRMA ,';
        $SQL .='             (';
        $SQL .='                 select';
        $SQL .='                     HST.FRK_KBA from FREMDKAEUFE HST where';
        $SQL .='                     HST.FRK_OEN_NUMMER = FRK.FRK_OEN_NUMMER and FRK_KBA is not null and rownum = 1) as FRK_KBA ,';
        $SQL .='              AB_J.ASI_WERT    as ABSATZ_JAHR,';
        $SQL .='             AB_VJ.ASI_WERT   as ABSATZ_VORJAHR,';
        $SQL .='             AST_KENNUNG,';
        $SQL .='             AST_BEZEICHNUNG,';
        $SQL .='             FRK_AST_ATUNR,';
        $SQL .='              (';
        $SQL .='                  select';
        $SQL .='                     sum(FRK_MENGE)';
        $SQL .='                  from';
        $SQL .='                     FREMDKAEUFE a';
        $SQL .='                  where';
        $SQL .='                     FRK_DATUM >= SYSDATE - interval \'2\' year';
        $SQL .='                     and a.FRK_OEN_NUMMER = FRK.FRK_OEN_NUMMER';
        $SQL .='                 group by';
        $SQL .='                      FRK_OEN_NUMMER';
        $SQL .='             ) as FK_24,';
        $SQL .='             (';
        $SQL .='                 select';
        $SQL .='                     sum(FRK_MENGE)';
        $SQL .='                 from';
        $SQL .='                     FREMDKAEUFE a';
        $SQL .='                 where';
        $SQL .='                     FRK_DATUM >= SYSDATE - interval \'1\' year';
        $SQL .='                     and a.FRK_OEN_NUMMER = FRK.FRK_OEN_NUMMER';
        $SQL .='                 group by';
        $SQL .='                      FRK_OEN_NUMMER';
        $SQL .='              ) as FK_12';
        $SQL .='          from';
        $SQL .='              FREMDKAEUFE         FRK';
        $SQL .='              left join ARTIKELSTAMM        AST on AST.AST_ATUNR = FRK.FRK_AST_ATUNR';
        $SQL .='             left join ARTIKELSTAMMINFOS   AB_J on AB_J.ASI_AST_ATUNR = AST_ATUNR';
        $SQL .='                                                 and AB_J.ASI_AIT_ID = 38';
        $SQL .='             left join ARTIKELSTAMMINFOS   AB_VJ on AB_VJ.ASI_AST_ATUNR = AST_ATUNR';
        $SQL .='                                                  and AB_VJ.ASI_AIT_ID = 37';
        $SQL .='          where';
        $SQL .='              FRK_DATUM >= SYSDATE - interval \'2\' year';

        $rsFRK = $this->DB->RecordSetOeffnen($SQL);

        $this->Log('Fremdkaufdaten geladen. Beginne Export.', 1);

        $TmpDatei = self::PFAD . '.tmp';
        if (file_exists($TmpDatei)) {
            unlink($TmpDatei);
        }

        $ExpDatei = fopen($TmpDatei, 'w');

        $Ueberschriften = array();
        $Ueberschriften[] = 'FRK_OEN_NUMMER';
        $Ueberschriften[] = 'FRK_ARTBEZ';
        $Ueberschriften[] = 'FRK_HERSTELLER';
        $Ueberschriften[] = 'FRK_FIRMA';
        $Ueberschriften[] = 'FRK_KBA';
        $Ueberschriften[] = 'ABSATZ_JAHR';
        $Ueberschriften[] = 'ABSATZ_VORJAHR';
        $Ueberschriften[] = 'FRK_AST_ATUNR';
        $Ueberschriften[] = 'AST_KENNUNG';
        $Ueberschriften[] = 'AST_BEZEICHNUNG';
        $Ueberschriften[] = 'FK_12';
        $Ueberschriften[] = 'FK_24';
        $Ueberschriften[] = 'AST_ATUNR_SUCHE';

        fwrite($ExpDatei, implode(self::TZ, $Ueberschriften) . self::LF);
        $this->Log('Ueberschriften geschrieben', 999);
        while (!$rsFRK->EOF()) {

            $Zeile = array();

            $Zeile[] = $rsFRK->FeldInhalt('FRK_OEN_NUMMER');
            $Zeile[] = $rsFRK->FeldInhalt('FRK_ARTBEZ');
            $Zeile[] = $rsFRK->FeldInhalt('FRK_HERSTELLER');
            $Zeile[] = $rsFRK->FeldInhalt('FRK_FIRMA');
            $Zeile[] = $rsFRK->FeldInhalt('FRK_KBA');
            $Zeile[] = $rsFRK->FeldInhalt('ABSATZ_JAHR');
            $Zeile[] = $rsFRK->FeldInhalt('ABSATZ_VORJAHR');
            $Zeile[] = $rsFRK->FeldInhalt('FRK_AST_ATUNR');
            $Zeile[] = $rsFRK->FeldInhalt('AST_KENNUNG');
            $Zeile[] = $rsFRK->FeldInhalt('AST_BEZEICHNUNG');
            $Zeile[] = $rsFRK->FeldInhalt('FK_12');
            $Zeile[] = $rsFRK->FeldInhalt('FK_24');

            $SQL = 'SELECT DISTINCT';
            $SQL .= '     tei_wert1';
            $SQL .= ' FROM';
            $SQL .= '     teileinfos';
            $SQL .= ' WHERE';
            $SQL .= '     tei_ity_id1 = \'AST\'';
            $SQL .= '     AND tei_ity_id2 = \'OEN\'';
            $SQL .= '     AND ( tei_such2 ) = asciiwortoe(' . $this->DB->WertSetzen('FRK', 'T', $rsFRK->FeldInhalt('FRK_OEN_NUMMER')) . ')';
            $SQL .= ' UNION';
            $SQL .= ' SELECT';
            $SQL .= '     tei_wert1';
            $SQL .= ' FROM';
            $SQL .= '     teileinfos';
            $SQL .= ' WHERE';
            $SQL .= '     tei_ity_id1 = \'AST\'';
            $SQL .= '     AND tei_ity_id2 = \'LAR\'';
            $SQL .= '     AND tei_key2 IN (';
            $SQL .= '         SELECT';
            $SQL .= '             tei_key1';
            $SQL .= '         FROM    TEILEINFOS where    TEI_ITY_ID2 = \'OEN\'';
            $SQL .= '             AND ( tei_such2 ) = asciiwortoe(' . $this->DB->WertSetzen('FRK', 'T', $rsFRK->FeldInhalt('FRK_OEN_NUMMER')) . ')';
            $SQL .= '             AND tei_ity_id1 = \'LAR\'';
            $SQL .= '     )';
            $SQL .= ' UNION ';
            $SQL .= ' SELECT tei_wert1 ';
            $SQL .= ' FROM ';
            $SQL .= '   teileinfos ';
            $SQL .= ' WHERE ';
            $SQL .= '     tei_ity_id1 = \'AST\'';
            $SQL .= '     AND tei_ity_id2 = \'LAR\'';
            $SQL .= '     AND ( ( tei_such2 ) = asciiwortoe(' . $this->DB->WertSetzen('FRK', 'T', $rsFRK->FeldInhalt('FRK_OEN_NUMMER')) . ')';
            $SQL .= '     or ( tei_wert2 ) = ' . $this->DB->WertSetzen('FRK', 'T', $rsFRK->FeldInhalt('FRK_OEN_NUMMER')) . ' )';

            $this->Log('Suche Daten zu OENummer: ' . $rsFRK->FeldInhalt('FRK_OEN_NUMMER'), 999);
            $rsSuche = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('FRK'));

            $Suchergebnis = '';
            while (!$rsSuche->EOF()) {
                $Suchergebnis .= $rsSuche->FeldInhalt(1) . ',';
                $rsSuche->DSWeiter();
            }
            $this->Log('Folgende Suchergebnisse dazu gefunden: ' . $Suchergebnis, 999);
            $Zeile[] = $Suchergebnis;

            fwrite($ExpDatei, implode(self::TZ, $Zeile) . self::LF);
            $rsFRK->DSWeiter();
        }

        $this->Log('Stelle Datei bereit..',1);
        if(is_file(self::PFAD)){
            $this->Log('Alte Datei vorhanden, l�sche..',1);
            unlink(self::PFAD);
        }

        $this->Log('Bennene Tempdatei um.',1);
        rename($TmpDatei, self::PFAD);

        $this->Log('Habe alles erledigt.', 1);
    }

    public function DebugLevel($Level)
    {
        $this->DebugLevel = $Level;
    }

    public function Log($Text, $Level)
    {
        if ($this->DebugLevel >= $Level) {
            echo date('c') . ' ' . $Text . PHP_EOL;
        }
    }

}