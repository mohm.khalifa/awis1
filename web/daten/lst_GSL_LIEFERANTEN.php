<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['txtRAV_LIEFERANT']))
{
    var_dump($_GET);
    die();
}


try
{
    $DS=0;

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('RAV','%');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Fehler = '';
    if(isset($_GET['txtRAV_LIEFERANT']) and $_GET['txtRAV_LIEFERANT'] != '')
    {
        $SQL  = ' Select * from GSVLIEFERANTEN WHERE GSL_KEY='.$DB->WertSetzen('GSL', 'N0',$_GET['txtRAV_LIEFERANT']).'';
        $rsGSR = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GSL', true));

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FIRMA'] . ':', 180, '', '');
        $Form->Erstelle_TextFeld('!GSL_FIRMA', $rsGSR->FeldInhalt('GSL_FIRMA'), 30, 400, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VORNAME'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_VORNAME', $rsGSR->FeldInhalt('GSL_VORNAME'), 30, 200, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSPRECHPARTNER'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_ANSPRECHPARTNER', $rsGSR->FeldInhalt('GSL_ANSPRECHPARTNER'), 30, 200, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_STRASSE', $rsGSR->FeldInhalt('GSL_STRASSE'), 30, 200, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PLZORT'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_PLZ', $rsGSR->FeldInhalt('GSL_PLZ'), 30, 60, false);
        $Form->Erstelle_TextFeld('!GSL_ORT', $rsGSR->FeldInhalt('GSL_ORT'), 30, 200, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_STRASSE', $rsGSR->FeldInhalt('GSL_STRASSE'), 30, 200, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_FIRMA'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_TELNR_FIRMA', $rsGSR->FeldInhalt('GSL_TELNR_FIRMA'), 30, 250, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FAXNR'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_FAXNR', $rsGSR->FeldInhalt('GSL_FAXNR'), 30, 250, false);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EMAIL'] . ':', 180);
        $Form->Erstelle_TextFeld('!GSL_MAIL', $rsGSR->FeldInhalt('GSL_MAIL'), 30, 300, false);
        $Form->ZeileEnde();

    }
    else
    {
        $Form->Erstelle_TextFeld('Test','', 100,170, false, '', '', '', 'T', 'L','','',50);
        $Form->Hinweistext($AWISSprachKonserven['RAV']['LIEFERANT AUSWAEHLEN']);
    }

}
catch (Exception $ex)
{
    $Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>