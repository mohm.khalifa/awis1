<?php
/**
 * Suchmaske f�r die Auswahl eines Servicedienstleistungen
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 201110
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SVB','%');
	$TextKonserven[]=array('SVT','%');
	$TextKonserven[]=array('CAD','txt_SucheName');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht19000=$AWISBenutzer->HatDasRecht(19000);
	if($Recht19000==0)
	{
	    awisEreignis(3,1000,'MBW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./serviceleistungen_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Serviceleistungen'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();

	$SQL = 'SELECT *';
	$SQL .= ' FROM SVCTeilnehmer';
	$SQL .= ' WHERE SVT_KENNUNG IN (SELECT SVT_KENNUNG';
	$SQL .= '      FROM SVCTeilnehmer';
	$SQL .= '      WHERE SYSDATE >= SVT_DATUMAB AND SYSDATE <= SVT_DATUMBIS';
	$SQL .= '      GROUP BY SVT_KENNUNG';
	$SQL .= '      HAVING COUNT(*) > 1)';
	$SQL .= ' ORDER BY SVT_KENNUNG, SVT_BEZEICHNUNG';
	
	$SQL = 'SELECT DISTINCT SVT_KEY, SVT_SVB_KEY, SVT_BEZEICHNUNG, SVT_KENNUNG, SVT_DATUMAB, SVT_DATUMBIS';
	$SQL .= ' FROM SVCTeilnehmer';
	$SQL .= ' INNER JOIN SVCLeistungen ON SVT_KENNUNG = SVL_KENNUNG AND SVL_STATUS < 9';
	$SQL .= ' WHERE svt_kennung IN (';
	$SQL .= '      SELECT SVT_KENNUNG';
	$SQL .= '      FROM SVCTeilnehmer';
	$SQL .= '      GROUP BY SVT_KENNUNG';
	$SQL .= '      HAVING count(*) > 1)';
	$SQL .= ' ORDER BY SVT_KENNUNG, SVT_DATUMAB, SVT_DATUMBIS';
	
	$rsProbleme = $DB->RecordSetOeffnen($SQL);
	if(!$rsProbleme->EOF())
	{
	    $DS=0;
	    
	    $Datum[]='';
	    $ErsterDS[]='';
	    $Fehler = false;
	    while(!$rsProbleme->EOF())
	    {
	        if(!isset($Datum[$rsProbleme->FeldInhalt('SVT_KENNUNG')]))
	        {
	            unset($Datum);
	            unset($ErsterDS);
	            $Fehler = false;
	            $Datum[$rsProbleme->FeldInhalt('SVT_KENNUNG')]=$Form->PruefeDatum($rsProbleme->FeldInhalt('SVT_DATUMBIS'),false,false,true);
	            $ErsterDS['SVT_BEZEICHNUNG']=$rsProbleme->FeldInhalt('SVT_BEZEICHNUNG');
	            $ErsterDS['SVT_KENNUNG']=$rsProbleme->FeldInhalt('SVT_KENNUNG');
	            $ErsterDS['SVT_DATUMAB']=$rsProbleme->FeldInhalt('SVT_DATUMAB');
	            $ErsterDS['SVT_DATUMBIS']=$rsProbleme->FeldInhalt('SVT_DATUMBIS');
	            $ErsterDS['SVT_SVB_KEY']=$rsProbleme->FeldInhalt('SVT_SVB_KEY');
	            $ErsterDS['SVT_KEY']=$rsProbleme->FeldInhalt('SVT_KEY');
	        }
	        else 
	        {
	            if($Datum[$rsProbleme->FeldInhalt('SVT_KENNUNG')]>=$Form->PruefeDatum($rsProbleme->FeldInhalt('SVT_DATUMAB'),false,false,true))
	            {
	                $Fehler = true;
	            }
	        }
	        if($Fehler)
	        {
	            if($DS==0)
	            {
	                $Form->ZeileStart();
	                $Form->Erstelle_TextLabel('ACHTUNG: Es wurden aktive Scancodes mehrfach gefunden!', 800,'Hinweis');
	                $Form->ZeileEnde();
	                 
	                $Form->ZeileStart();
	                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['SVT_BEZEICHNUNG'], 250);
	                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['SVT_KENNUNG'], 200);
	                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['SVT_DATUMAB'], 100);
	                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVT']['SVT_DATUMBIS'], 100);
	                $Form->ZeileEnde();
	            }
	            
	            
    	        $Form->ZeileStart();
    	        $Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVB_KEY='.$ErsterDS['SVT_SVB_KEY'].'&SVT_KEY='.$ErsterDS['SVT_KEY'];
    	        $Form->Erstelle_ListenFeld('#NAME', $ErsterDS['SVT_BEZEICHNUNG'], 0, 250,false,($DS%2),'',$Link);
    	        $Form->Erstelle_ListenFeld('#KENNUNG', $ErsterDS['SVT_KENNUNG'], 0, 200,false,($DS%2));
    	        $Form->Erstelle_ListenFeld('#VOM', $ErsterDS['SVT_DATUMAB'], 0, 100,false,($DS%2),'','','D');
    	        $Form->Erstelle_ListenFeld('#BIS', $ErsterDS['SVT_DATUMBIS'], 0, 100,false,($DS%2),'','','D');
    	        $Form->ZeileEnde();
    	        
    	        $Form->ZeileStart();
    	        $Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&SVB_KEY='.$rsProbleme->FeldInhalt('SVT_SVB_KEY').'&SVT_KEY='.$rsProbleme->FeldInhalt('SVT_KEY');
    	        $Form->Erstelle_ListenFeld('#NAME', $rsProbleme->FeldInhalt('SVT_BEZEICHNUNG'), 0, 250,false,($DS%2),'',$Link);
    	        $Form->Erstelle_ListenFeld('#KENNUNG', $rsProbleme->FeldInhalt('SVT_KENNUNG'), 0, 200,false,($DS%2));
    	        $Form->Erstelle_ListenFeld('#VOM', $rsProbleme->FeldInhalt('SVT_DATUMAB'), 0, 100,false,($DS%2),'','','D');
    	        $Form->Erstelle_ListenFeld('#BIS', $rsProbleme->FeldInhalt('SVT_DATUMBIS'), 0, 100,false,($DS%2),'','','D');
    	        $Form->ZeileEnde();
    	        
    	        $DS++;
	        }
	        
	        $rsProbleme->DSWeiter();
	    }
	    
	    $Form->Trennzeile();
	}
	
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_VERTRAGSNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*SVB_VERTRAGSNUMMER',($Param['SPEICHERN']=='on'?$Param['SVB_VERTRAGSNUMMER']:''),25,200,true);
	$AWISCursorPosition='sucSVB_VERTRAGSNUMMER';
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_SucheName'].':',190);
	$Form->Erstelle_TextFeld('*SuchName',($Param['SPEICHERN']=='on'?$Param['SuchName']:''),25,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_KUNDENID'].':',190);
	$Form->Erstelle_TextFeld('*SVB_KUNDENID',($Param['SPEICHERN']=='on'?$Param['SVB_KUNDENID']:''),25,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['SVT']['SVT_KENNUNG'].':',190);
	$Form->Erstelle_TextFeld('*SVT_KENNUNG',($Param['SPEICHERN']=='on'?$Param['SVT_KENNUNG']:''),25,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	if(($Recht19000&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221210");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221211");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>