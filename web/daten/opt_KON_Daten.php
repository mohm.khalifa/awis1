<?php
//**************************************************************
// Liefert die Kontakte, die eine Durchwahl haben
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

if(!isset($_GET['KON_KEY']))
{
	die('##PARAM##');
}
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$SQL = 'SELECT KON_KEY, KONNAME FROM (';
$SQL .= "SELECT KON_KEY, KON_NAME1 || ' ' || COALESCE(KON_NAME2,'') || ', ' || KKO_WERT AS KONNAME ";
$SQL .= ', ROW_NUMBER() OVER(Partition BY KON_KEY ORDER BY KKO_WERT) AS zeile';
$SQL .= ' FROM KONTAKTE ';
$SQL .= ' INNER JOIN KONTAKTEKOMMUNIKATION ON KKO_KON_KEY = KON_KEY AND KKO_KOT_KEY = 9';        // Alle mit Telefondurchwahl

$Bedingung = ' AND KON_KKA_KEY = 1';        // Nur ATU Mitarbeiter

if($_GET['FILTER']!='')
{
	$Bedingung .= ' AND ASCIIWORT(UPPER(KON_NAME1)) '.$DB->LikeOderIst(strtoupper($_GET['FILTER']).'*',awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
}
if($_GET['WERT']!='')
{
	$Bedingung .=' OR  KON_KEY = '.$_GET['WERT'];
}

if($Bedingung!='')
{
	$SQL .= ' WHERE '.substr($Bedingung,4);
}
$SQL .= ') DATEN WHERE Zeile = 1';
$SQL .= " ORDER BY 2";

$rsKON=$DB->RecordSetOeffnen($SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo $SQL.'#~#';		// Trennzeichen
$DS=0;
if(isset($_GET['ZUSATZ']) AND $_GET['ZUSATZ']!='')
{
	$Zusatz = explode('~',urldecode($_GET['ZUSATZ']));
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
	}
	else
	{
		echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
	}
}

if($rsKON->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">::--::</option>';
	}
	else
	{
		echo '#+##+#::--::';
	}
}


while(!$rsKON->EOF())
{
	if(++$DS>200 AND $rsKON->FeldInhalt('KON_KEY')==$_GET['WERT'])
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsKON->FeldInhalt('KON_KEY').'">'.$rsKON->FeldInhalt('KONNAME').'</option>';
		}
		else
		{
			echo '#+#'.$rsKON->FeldInhalt('KON_KEY').'#+#'.$rsKON->FeldInhalt('KONNAME').'';
		}
	}
	elseif($DS<200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsKON->FeldInhalt('KON_KEY').'">'.$rsKON->FeldInhalt('KONNAME').'</option>';
		}
		else
		{
			echo '#+#'.$rsKON->FeldInhalt('KON_KEY').'#+#'.$rsKON->FeldInhalt('KONNAME').'';
		}
	}

	$rsKON->DSWeiter();
}
if(++$DS>200)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">...</option>';
	}
	else
	{
		echo '#+##+#...';
	}
}
?>