<?php
require_once('/daten/include/awisZukauf.inc');
require_once('/daten/include/awisJobVerwaltung.inc');


try
{
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $Datum = $Form->PruefeDatum($argv[1]);
    if($Datum != '01.01.1970')
    {
	$SQL = 'SELECT DISTINCT TRUNC(ZUB_BESTELLDATUM) AS ZUL_DATUM';
        $SQL.= ' FROM zukaufbestellungen LEFT OUTER JOIN zukauflieferscheine ON ZUB_KEY = ZUL_ZUB_KEY';
	$SQL.= ' WHERE ZUB_LIE_NR = \'8164\' AND ZUB_BESTELLDATUM >= '.$DB->FeldInhaltFormat('D',$Datum);
	$SQL .= ' AND ZUL_KEY IS NULL';
	$SQL .= ' ORDER BY 1';
	$rsDATUM = $DB->RecordsetOeffnen($SQL);

	while(!$rsDATUM->EOF())
	{

	   $Datum = $Form->Format('D',$rsDATUM->FeldInhalt('ZUL_DATUM'));


        echo 'Verarbeite '.$Datum.PHP_EOL;

    	$ZUK = new awisZukauf_WUETSCHNER(99,false);
    	if($ZUK->LeseLieferscheine($Datum,$Datum,'','','',true)!==false)
	{
	    $ZUK->SpeichereLieferscheine();
	}

	$rsDATUM->DSWeiter();
	}
    }
}
catch (Exception $ex)
{
	echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
	if(trim($ex->getMessage())=='Gateway Error')
	{
	}
	elseif(trim($ex->getMessage())=='Error Fetching http headers')
	{
	}

	echo ' Letztes Datum war: '.$Datum.PHP_EOL;

}
?>
