<?php
global $cmdSuche;
global $AWISBenutzer;

// Suchfelder in der Suchen-Seite

global $con;
// eigene Stufe speichern
$TelefonverzeichnisStufe = awisBenutzerRecht($con, 151);
$Recht150 = awisBenutzerRecht($con, 150);        // Zugriff steuern
if ($Recht150 == 0) {
    die('Keine ausreichenden Rechte.');
}

$MaxDS = awis_BenutzerParameter($con, 'AnzahlDatensaetzeProListe', $AWISBenutzer->BenutzerName());
$KatRechte = "0" . awisBenutzerRecht($con, 151);        // Kategorien f�r Telefon

if (isset($_REQUEST['txtKONKey']) OR isset($_REQUEST['DSNeu']) OR isset($_POST["cmdDSNeu_x"])) {
    include "./telefon_Details.php";
} else {

    // Lokale Variablen
    $Bedingung = ''; // Bedingung f�r SQL
    $SQL = ''; // SQL Abfragestring

    if (isset($_GET['KAB_KEY']) OR isset($_GET['KON_ZUS'])) {
        $Params = "";
        $Params .= ";";
        $Params .= ";";
        $Params .= ";";
        $Params .= ";";
        $Params .= ";";
        $Params .= ";";
        $Params .= ";";
        $Params .= ";";

        awis_BenutzerParameterSpeichern($con, "TelefonSuche", $AWISBenutzer->BenutzerName(), $Params);
        $Params = explode(';', $Params);        // Als Array f�r sp�ter
    } // Aktualisieren wurde gew�hlt
    elseIf (isset($_POST["cmdSuche_x"]) OR (isset($_GET['cmdSuche']) AND $_GET['cmdSuche'] == 'Aktualisieren')) {
        $Params = "";
        $Params .= $_POST['txtName'];
        $Params .= ";" . $_POST['txtKennzeichen'];
        $Params .= ";" . $_POST['txtKategorie'];
        $Params .= ";" . $_POST['txtBereich'];
        $Params .= ";" . $_POST['txtAbteilung'];
        $Params .= ";" . $_POST['txtZustaendigkeit'];
        $Params .= ";" . $_POST['txtGebaude'];
        $Params .= ";" . $_POST['txtSuche'];            // Zentrales Suchfeld
        $Params .= ";" . $_POST['txtBemerkung'];

        awis_BenutzerParameterSpeichern($con, "TelefonSuche", $AWISBenutzer->BenutzerName(), $Params);
        $Params = explode(';', $Params);        // Als Array f�r sp�ter
    } else { // die gespeicherten Werte
        $Params = explode(";", awis_BenutzerParameter($con, "TelefonSuche", $AWISBenutzer->BenutzerName()));
    }

    //PG: Hochkomma sprengt SQL. Ersetzen..
    $ParamAnz = count($Params) - 1;
    for ($i = 0; $i <= $ParamAnz; $i++) {
        $Params[$i] = str_replace("'", '', addslashes($Params[$i]));
    }

    $txtName = $Params[0];
    $txtKennzeichen = $Params[1];
    $txtKategorie = $Params[2];
    $txtBereich = $Params[3];
    $txtAbteilung = $Params[4];
    $txtZustaendigkeit = $Params[5];
    $txtGebaeude = $Params[6];
    $txtSuche = $Params[7];
    $txtBemerkung = $Params[8];

    // Bildschirmbreite f�r die Darstellung
    $BildschirmBreite = awis_BenutzerParameter($con, "BildschirmBreite", $AWISBenutzer->BenutzerName());

    //************************************
    if ($txtSuche != '')                // Suchfeld �ber alles
    {
        if (substr($txtSuche, -1, 1) != '*' AND substr($txtSuche, -1, 1) != '%') {
            $txtSuche .= '%';
        }

        $replace = array("�", "�", "�", "�", "�", "�", "�", "�");
        $search = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", "");
        $txtSucheUmlaut = str_replace($search, $replace, $txtSuche);

        $SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KKA_BEZEICHNUNG, KBE_BEZEICHNUNG, KON_BEMERKUNG,KON_ZUSTAENDIGKEIT, KON_STATUS";
        $SQL .= " FROM Kontakte ";
        $SQL .= " INNER JOIN KontakteBEREICHE ON KON_KBE_KEY = KBE_KEY ";
        $SQL .= " INNER JOIN KontakteKATEGORIEN ON KON_KKA_KEY = KKA_KEY ";
        $SQL .= " LEFT OUTER JOIN KontakteKOMMUNIKATION ON KKO_KON_KEY = KON_KEY ";
        $SQL .= " LEFT OUTER JOIN  KontakteFAHRZEUGE ON KFA_KON_KEY = KON_KEY ";
        $SQL .= " WHERE  ((upper(KON_Name1) " . awisLIKEoderIST($txtSuche, true, false, false, 0);
        $SQL .= " OR  upper(KON_Name2) " . awisLIKEoderIST($txtSuche, true, false, false, 0);
        $SQL .= " or  (upper(KON_Name1) " . awisLIKEoderIST($txtSucheUmlaut, true, false, false, 0);
        $SQL .= " OR  upper(KON_Name2) " . awisLIKEoderIST($txtSucheUmlaut, true, false, false, 0);
        $SQL .= " ) OR (upper(KON_BEMERKUNG) ".awisLIKEoderIST('%'.$txtSuche.'%',true);
        $SQL .= " OR upper(KON_BEMERKUNG) ".awisLIKEoderIST('%'.$txtSucheUmlaut.'%',true);
        $SQL .= " ) OR (upper(KON_ZUSTAENDIGKEIT) ".awisLIKEoderIST('%'.$txtSuche.'%',true);
        $SQL .= " OR upper(KON_ZUSTAENDIGKEIT) ".awisLIKEoderIST('%'.$txtSucheUmlaut.'%',true);
        $SQL .= "))OR  KON_KEY IN (SELECT KKO_KON_KEY FROM KontakteKommunikation INNER JOIN KommunikationsTypen ON KKO_KOT_KEY = KOT_KEY ";
        // Auch nach Durchwahlen suchen ==> 4096 drauf
        $SQL .= " WHERE BITAND(KOT_KZG_ID," . (((int)$KatRechte & 4096) > 0?$KatRechte:(int)$KatRechte + 4096) . ")>0 ";
        $SQL .= " AND (UPPER(KKO_WERT) " . awisLIKEoderIST($txtSuche, true, false, false,
                0) . " OR UPPER(KKO_KURZWAHL) " . awisLIKEoderIST($txtSuche, true, false, false, 0) . "))";
        $SQL .= " OR  KON_KEY IN (SELECT KZA_KON_KEY FROM KontakteAbteilungenZuordnungen ";
        $SQL .= " INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY ";
        $SQL .= " WHERE (BITAND(KAB_KZG_ID," . (($KatRechte & 4096) > 0?$KatRechte:$KatRechte + 4096) . ")>0 AND UPPER(KAB_Abteilung) " . awisLIKEoderIST('%'.$txtSuche.'%',
                true, false, false, 0) . "))) ";
        if (($Recht150 & 64) != 0) {
            $SQL .= ' AND BITAND(KON_LISTEN,2)>0';
        }
        if(!isset($_POST['txtAlleStatus'])){
            $SQL .= " AND KON_STATUS = 'A'";
        }
        $SQL .= ' AND BITAND(KKA_KZG_ID,' . (((int)$KatRechte & 4096) > 0?$KatRechte:(int)$KatRechte + 4096) . ')>0';

        $SQL .= " ORDER BY KON_NAME1, KON_NAME2";
    } // Alle Mitarbeiter einer Abteilung anzeigen
    elseif (isset($_GET['KAB_KEY'])) {
        $SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KKA_BEZEICHNUNG, KBE_BEZEICHNUNG, KON_BEMERKUNG,KON_ZUSTAENDIGKEIT";
        $SQL .= " FROM Kontakte ";
        $SQL .= " INNER JOIN KontakteBEREICHE ON KON_KBE_KEY = KBE_KEY ";
        $SQL .= " INNER JOIN KontakteKATEGORIEN ON KON_KKA_KEY = KKA_KEY ";
        $SQL .= " LEFT OUTER JOIN KontakteKOMMUNIKATION ON KKO_KON_KEY = KON_KEY ";
        $SQL .= " LEFT OUTER JOIN  KontakteFAHRZEUGE ON KFA_KON_KEY = KON_KEY ";

        $SQL .= " WHERE KON_KEY IN (SELECT KZA_KON_KEY FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE BITAND(KAB_KZG_ID,$KatRechte)=KAB_KZG_ID AND KAB_KEY=" . $_GET['KAB_KEY'] . ") ";
        if (($Recht150 & 64) != 0) {
            $SQL .= ' AND BITAND(KON_LISTEN,2)>0';
        }
        if(!isset($_POST['txtAlleStatus'])){
            $SQL .= ' AND KON_STATUS=\'A\'';
        }
        $SQL .= " ORDER BY KON_NAME1, KON_NAME2";
    } elseif (isset($_GET['KON_ZUS'])) {
        $SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KKA_BEZEICHNUNG, KBE_BEZEICHNUNG, KON_BEMERKUNG,KON_ZUSTAENDIGKEIT";
        $SQL .= " FROM Kontakte ";
        $SQL .= " INNER JOIN KontakteBEREICHE ON KON_KBE_KEY = KBE_KEY ";
        $SQL .= " INNER JOIN KontakteKATEGORIEN ON KON_KKA_KEY = KKA_KEY ";
        $SQL .= " LEFT OUTER JOIN KontakteKOMMUNIKATION ON KKO_KON_KEY = KON_KEY ";
        $SQL .= " LEFT OUTER JOIN  KontakteFAHRZEUGE ON KFA_KON_KEY = KON_KEY ";

        $SQL .= " WHERE KON_ZUSTAENDIGKEIT = '" . html_entity_decode($_GET['KON_ZUS']) . "'";
        if (($Recht150 & 64) != 0) {
            $SQL .= ' AND BITAND(KON_LISTEN,2)>0';
        }
        $SQL .= ' AND KON_STATUS=\'A\'';
        $SQL .= " ORDER BY KON_NAME1, KON_NAME2";
    } else            // Einzelne Suchfelder
    {

        // Kontakt-Name
        if ($txtName != '') {
            $Bedingung .= " AND ( SUCHWORT(KON_NAME1) " . awisLIKEoderIST("%" . $txtName . "%", true, false, true,
                    false) . " OR SUCHWORT(KON_NAME2) " . awisLIKEoderIST("%" . $txtName . "%", true, false, true,
                    false) . "";
            if (isset($_POST['txtAehnlicheSuchen']) AND $_POST['txtAehnlicheSuchen'] == 'on') {
                $Bedingung .= " OR SOUNDEXD(KON_NAME1)=SOUNDEXD('" . $txtName . "')";
            }

            $Bedingung .= " OR ( SUCHWORT(KON_NAME2) " . awisLIKEoderIST("%" . $txtName . "%", true, false, true,
                    false) . " OR SUCHWORT(KON_NAME2) " . awisLIKEoderIST("%" . $txtName . "%", true, false, true,
                    false) . "";
            if (isset($_POST['txtAehnlicheSuchen']) AND $_POST['txtAehnlicheSuchen'] == 'on') {
                $Bedingung .= " OR SOUNDEXD(KON_NAME2)=SOUNDEXD('" . $txtName . "')";
            }

            $Bedingung .= "))";
        }

        // Kategorie
        if ($txtKategorie != '' and $txtKategorie != '0') {
            $Bedingung .= " AND (KON_KKA_KEY = " . $txtKategorie . ") ";
        } elseif (isset($_POST['txtKategorie']) AND $_POST['txtKategorie'] == '0') {
            $KatRechte = "0" . awisBenutzerRecht($con, 151);
            $rsKategorien = awisOpenRecordset($con,
                "SELECT KKA_KEY FROM KontakteKategorien WHERE BITAND(KKA_KZG_ID,$KatRechte)=KKA_KZG_ID");
            $Kategorien = '';
            for ($i = 0; $i < $awisRSZeilen; $i++) {
                $Kategorien .= ',' . $rsKategorien["KKA_KEY"][$i];
            }
            unset($rsKategorien);        // Speicher freigeben
            $Bedingung .= ' AND (KON_KKA_KEY IN (0' . $Kategorien . '))';
        }

        // Bereich (T�chter)
        if ($txtBereich != '' and $txtBereich != -1 AND $txtBereich != ';') {
            $Bedingung .= " AND (KON_KBE_KEY = " . $txtBereich . ") ";
        }

        // Abteilungen
        if ($txtAbteilung != '' and $txtAbteilung != -1 and $txtAbteilung != ';') {
            //$Bedingung .= " AND (KON_KAB_KEY = " . $txtAbteilung . ") ";
            $Bedingung .= " AND (KON_KEY IN (SELECT KZA_KON_KEY FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE BITAND(KAB_KZG_ID,$KatRechte)=KAB_KZG_ID AND KZA_KAB_KEY = 0" . $txtAbteilung . ")) ";
        } else {
            $Bedingung .= " AND (KON_KEY IN (SELECT KZA_KON_KEY FROM KontakteAbteilungenZuordnungen INNER JOIN KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE BITAND(KAB_KZG_ID,$KatRechte)=KAB_KZG_ID)) ";
        }

        // Zust�ndigkeit
        if ($txtZustaendigkeit != '') {
            $Bedingung .= " AND (UPPER(KON_ZUSTAENDIGKEIT) " . awisLIKEoderIST("%" . $txtZustaendigkeit . "%",
                    true) . ") ";
        }

        // Geb�ude
        if ($txtGebaeude != '0' AND $txtGebaeude != '') {
            $Bedingung .= " AND (KON_GEB_ID = " . $txtGebaeude . ") ";
        }

        // Kontakt
        if (isset($_POST['txtKontakt']) AND $_POST['txtKontakt'] != '') {
            $Bedingung .= " AND (";

            if ($_POST['txtKontaktTyp'] != '0') {
                $Bedingung .= " KKO_KOT_KEY=" . $_POST['txtKontaktTyp'] . " AND ";
            }

            // Durchwahlen d�rfen immer gesucht werden
            $Bedingung .= " KON_KEY IN (SELECT KKO_KON_KEY FROM KontakteKommunikation
						 INNER JOIN KommunikationsTypen ON KKO_KOT_KEY = KOT_KEY
						 WHERE BITAND(KOT_KZG_ID," . (($KatRechte & 4096) > 0?$KatRechte:$KatRechte + 4096) . ")=KOT_KZG_ID AND
						  UPPER(KKO_WERT) " . awisLIKEoderIST($_POST['txtKontakt'], true, false, false, 0) . "))";
        }

        // KFZ
        if ($txtKennzeichen != '') {
            // SK, 15.3
            $Bedingung .= " AND ASCIIWORT(KFA_KENNZEICHEN) " . awisLIKEoderIST("%" . $txtKennzeichen . "%", true, 0, 0,
                    1) . " ";
        }

        if (isset($_POST['txtFahrzeugTyp']) AND $_POST['txtFahrzeugTyp'] != '') {
            $Bedingung .= " AND UPPER(KFA_FAHRZEUGTYP) " . awisLIKEoderIST("%" . $_POST['txtFahrzeugTyp'] . "%",
                    true) . " ";
        }

        // Bemerkung
        if ($txtBemerkung != '') {
            $Bedingung .= " AND upper(KON_BEMERKUNG) " . awisLIKEoderIST("%".$txtBemerkung."%",true) . " ";
        }

        // ********************************************************************
        // SQL Abfrage zusammenstellen
        // ********************************************************************

        $SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KKA_BEZEICHNUNG, KBE_BEZEICHNUNG, KON_BEMERKUNG,KON_ZUSTAENDIGKEIT, KON_STATUS ";
        $SQL .= " FROM Kontakte, KontakteBEREICHE, KontakteKATEGORIEN, KontakteKOMMUNIKATION, KontakteFAHRZEUGE ";
        $SQL .= " WHERE ((KBE_KEY(+) = KON_KBE_KEY AND KON_KKA_KEY = KKA_KEY(+) AND KKO_KON_KEY(+) = KON_KEY AND KON_KEY = KFA_KON_KEY(+)) ";
        $SQL .= " ";
        $SQL .= " " . $Bedingung . ")";
        if (($Recht150 & 64) != 0) {
            $SQL .= ' AND BITAND(KON_LISTEN,2)>0';
        }
        $SQL .= ' AND KON_STATUS=\'A\'';
        $SQL .= " ORDER BY KON_NAME1, KON_NAME2";
    }    // Ende Suchfeld

    awisExecute($con, "ALTER SESSION SET NLS_SORT=GERMAN_AI");

    // SK, 15.10.2013 -> Suche nach fehlenden Systembenutzern
    if (isset($_POST['txtOhneSystem'])) {
        $SQL = 'SELECT * FROM (' . $SQL . ') DATEN1 WHERE NOT EXISTS(SELECT * FROM BENUTZER WHERE XBN_KON_KEY = KON_KEY) ';
    }

    $rsKontakte = awisOpenRecordset($con, $SQL);
    $rsKontakteAnz = $awisRSZeilen;

    awisExecute($con, "ALTER SESSION SET NLS_COMP=BINARY");
    awisExecute($con, "ALTER SESSION SET NLS_SORT=GERMAN");

    // *****************************************
    // Spaltenbreiten festlegen
    // *****************************************
    $SpBreiteFaktor = $BildschirmBreite / 1024;
    $SpBreiteTabelle = (1024 * $SpBreiteFaktor) - 40;

    If ($rsKontakteAnz == 1)        // Bei einem Datensatz sofort in die Details gehen
    {
        $txtKONKey = $rsKontakte['KON_KEY'][0];
        include "./telefon_Details.php";
    } elseIf ($rsKontakteAnz > 1) { // Liste gefunden
        print "<table width=" . $SpBreiteTabelle . " cellspacing=0 cellpadding=0 border=1 >";
        print "<tr><td id=FeldBez >Name1</td>";
        print "<td id=FeldBez >Name2</td>";
        print "<td id=FeldBez >DW Tel</td>";
        print "<td id=FeldBez >DW Fax</td>";
        print "<td id=FeldBez >Abteilung</td>";
        print "<td id=FeldBez >Zust�ndigkeit</td>";
        print "<td id=FeldBez >Bemerkung</td>";
        print "<td id=FeldBez >Kategorie</td>";
        print "<td id=FeldBez >Bereich</td>";
        print "</tr>";

        for ($i = 0; $i < $rsKontakteAnz; $i++) {

            switch($rsKontakte["KON_STATUS"][$i] ){
                case 'I':
                    $Farbe = "id=TabellenZeileRot";
                    break;
                case 'V':
                    $Farbe = "id=TabellenZeileGelb";
                    break;
                default:
                    $Farbe = (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss");
                    break;
            }

            // Name1
            print "<td " .$Farbe . " ><b><a title='Klick: Details zum Kontakt anzeigen' href=./telefon_Main.php?cmdAktion=Liste&txtKONKey=" . $rsKontakte["KON_KEY"][$i] . ">" . ($rsKontakte["KON_NAME1"][$i] == ''?'?????':$rsKontakte["KON_NAME1"][$i]) . "</a></b></td>";
            // Name2
            print "<td " .$Farbe . " ><b>" . $rsKontakte["KON_NAME2"][$i] . "</b></td>";

            // Fax
            $Telefon = "";
            $Fax = "";
            DurchwahlListe($con, $rsKontakte["KON_KEY"][$i], $Telefon, $Fax);
            print "<td " . $Farbe . " ><b>" . $Telefon . "</b></td>";
            print "<td " . $Farbe . " ><b>" . $Fax . "</b></td>";

            // Abteilung
            $BindeVariablen = array();
            $BindeVariablen['var_N0_kon_key'] = '0' . $rsKontakte["KON_KEY"][$i];

            $rsKZA = awisOpenRecordset($con,
                'SELECT KAB_ABTEILUNG,KAB_KEY FROM KontakteAbteilungenZuordnungen INNER JOIN  KontakteAbteilungen ON KZA_KAB_KEY = KAB_KEY WHERE KZA_KON_KEY=:var_N0_kon_key',
                true, false, $BindeVariablen);
            $rsKZAZeilen = $awisRSZeilen;
            $Abteilungen = '';
            for ($j = 0; $j < $rsKZAZeilen; $j++) {
                $Abteilungen .= ', <a title="Klick: Alle Kontakte in dieser Abteilung anzeigen" href=./telefon_Main.php?cmdAktion=Liste&KAB_KEY=0' . $rsKZA["KAB_KEY"][$j] . '>' . $rsKZA["KAB_ABTEILUNG"][$j] . '</a>';
            }
            unset($rsKZA);
            print "<td xnowrap " .$Farbe . " ><b>" . substr($Abteilungen,
                    1) . "</b></td>";

            // Zust�ndigkeit
            print "<td " .$Farbe. " ><b><a href=./telefon_Main.php?cmdAktion=Liste&KON_ZUS=" . urlencode($rsKontakte["KON_ZUSTAENDIGKEIT"][$i]) . ">" . $rsKontakte["KON_ZUSTAENDIGKEIT"][$i] . "</a></b></td>";

            print "<td " .$Farbe. " ><b>" . $rsKontakte["KON_BEMERKUNG"][$i] . "</b></td>";
            // Kategorie
            print "<td nowrap width=170 " . $Farbe . " ><b>" . $rsKontakte["KKA_BEZEICHNUNG"][$i] . "</b></td>";
            // Bereich
            print "<td " . $Farbe . " ><b>" . $rsKontakte["KBE_BEZEICHNUNG"][$i] . "</b></td>";

            print "</tr>";
        }

        print "</table>";
        print "<br><font size=1>Es wurden $rsKontakteAnz Kontakte in " . awis_ZeitMessung(1) . " Sekunden gefunden. ";
    } else { // Nur eine Filiale gefunden, Daten anzeigen
        print "<span class=HinweisText>Es wurden keine passenden Kontakte f�r die Auswahl gefunden, oder Sie haben nicht die notwendigen Rechte zur Anzeige der Daten.</span>";
        awis_Debug(1, $SQL);
    }
} // Liste oder Details

// ******************************************************************************
// Hilfsfunktionen
// ******************************************************************************

function DurchwahlListe($con, $KonKey, & $Telefon, & $Fax)
{

    // global $Telefon;
    // global $Fax;
    global $awisRSZeilen;

    $BindeVariablen = array();
    $BindeVariablen['var_N0_kon_key'] = "0" . $KonKey;

    $rsDW = awisOpenRecordset($con,
        "SELECT KKO_WERT, KKO_KOT_KEY FROM KONTAKTEKOMMUNIKATION INNER JOIN KOMMUNIKATIONSTYPEN ON KKO_KOT_KEY = KOT_KEY WHERE KKO_KOT_KEY IN (9,10,11) AND KKO_KON_KEY=:var_N0_kon_key ORDER BY KOT_Sortierung,KKO_WERT",
        true, false, $BindeVariablen);

    $rsDWZeilen = $awisRSZeilen;

    for ($i = 0; $i < $rsDWZeilen; $i++) {
        if ($rsDW["KKO_KOT_KEY"][$i] == 9 OR $rsDW["KKO_KOT_KEY"][$i] == 11) {
            if (preg_match('/MOBILE/', strtoupper($_SERVER['HTTP_USER_AGENT']))){
                $Telefon .= ", " . "<a href=\"tel:0961306" . $rsDW["KKO_WERT"][$i] . "\"><img src=\"/bilder/callBtn.png\" alt=\"Call\" style=\"vertical-align: sub\"/>" . $rsDW["KKO_WERT"][$i] . "</a>";
            } else {
                $Telefon .= ", " . $rsDW["KKO_WERT"][$i];
            }
    
        } else {
            $Fax .= ", " . $rsDW["KKO_WERT"][$i];
        }
    }

    $Telefon = substr($Telefon, 2);
    $Fax = substr($Fax, 2);
    unset($rsDW);
}

?>