<?php
/**
 * AWIS Erweiterung für FPDFI
 *
 * Klasse wird erweitert um eine neue Funktion für Zeilenumbrüche in CELL() => CELL2()
 * und Ersetzen von Tschechischen Zeichen (HTML->CP1250)
 */
require_once('fpdi.php');

/**
 * Erweiterung der FPDI Klasse für die Bedürfnisse der ATU
 *
 */
class fpdi_awis extends fpdi
{
	/**
	 * Zeichencodierung
	 *
	 * @var string
	 */
	private $_ZeichenCodierung = 'CP1252';

	/**
	 * Einzelne Zelle mit Zeilenumbrüchen ausgeben
	 *
	 * @param int $w
	 * @param int $h
	 * @param string $txt
	 * @param int $border
	 * @param int $ln
	 * @param string $align
	 * @param boolean $fill
	 * @param string $link
	 */
    function cell2($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
		if($this->_ZeichenCodierung=='CP1250')
		{
			$txt = $this->ErsetzeZeichen_CP1250($txt);
		}

    	//Output a cell
        $k=$this->k;
        if($this->y+$h>$this->PageBreakTrigger && !$this->InHeader && !$this->InFooter && $this->AcceptPageBreak())
        {
            //Automatic page break
            $x=$this->x;
            $ws=$this->ws;
            if($ws>0)
            {
                $this->ws=0;
                $this->_out('0 Tw');
            }
            $this->AddPage($this->CurOrientation,$this->CurPageFormat);
            $this->x=$x;
            if($ws>0)
            {
                $this->ws=$ws;
                $this->_out(sprintf('%.3F Tw',$ws*$k));
            }
        }
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $s='';
    // begin change Cell function
        if($fill || $border>0)
        {
            if($fill)
                $op=($border>0) ? 'B' : 'f';
            else
                $op='S';
            if ($border>1) {
                $s=sprintf('q %.2F w %.2F %.2F %.2F %.2F re %s Q ',$border,
                    $this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
            }
            else
                $s=sprintf('%.2F %.2F %.2F %.2F re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
        }
        if(is_string($border))
        {
            $x=$this->x;
            $y=$this->y;
            if(is_int(strpos($border,'L')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
            else if(is_int(strpos($border,'l')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);

            if(is_int(strpos($border,'T')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
            else if(is_int(strpos($border,'t')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);

            if(is_int(strpos($border,'R')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
            else if(is_int(strpos($border,'r')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);

            if(is_int(strpos($border,'B')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
            else if(is_int(strpos($border,'b')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
        }
        if (trim($txt)!='') {
            $cr=substr_count($txt,"\n");
            if ($cr>0) { // Multi line
                $txts = explode("\n", $txt);
                $lines = count($txts);
                for($l=0;$l<$lines;$l++) {
                    $txt=$txts[$l];
                    $w_txt=$this->GetStringWidth($txt);
                    if($align=='R')
                        $dx=$w-$w_txt-$this->cMargin;
                    elseif($align=='C')
                        $dx=($w-$w_txt)/2;
                    else
                        $dx=$this->cMargin;

                    $txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
                    if($this->ColorFlag)
                        $s.='q '.$this->TextColor.' ';
                    $s.=sprintf('BT %.2F %.2F Td (%s) Tj ET ',
                        ($this->x+$dx)*$k,
                        ($this->h-($this->y+.5*$h+(.7+$l-$lines/2)*$this->FontSize))*$k,
                        $txt);
                    if($this->underline)
                        $s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
                    if($this->ColorFlag)
                        $s.=' Q ';
                    if($link)
                        $this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$w_txt,$this->FontSize,$link);
                }
            }
            else { // Single line
                $w_txt=$this->GetStringWidth($txt);
                $Tz=100;
                if ($w_txt>$w-2*$this->cMargin) { // Need compression
                    $Tz=($w-2*$this->cMargin)/$w_txt*100;
                    $w_txt=$w-2*$this->cMargin;
                }
                if($align=='R')
                    $dx=$w-$w_txt-$this->cMargin;
                elseif($align=='C')
                    $dx=($w-$w_txt)/2;
                else
                    $dx=$this->cMargin;
                $txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
                if($this->ColorFlag)
                    $s.='q '.$this->TextColor.' ';
                $s.=sprintf('q BT %.2F %.2F Td %.2F Tz (%s) Tj ET Q ',
                            ($this->x+$dx)*$k,
                            ($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,
                            $Tz,$txt);
                if($this->underline)
                    $s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
                if($this->ColorFlag)
                    $s.=' Q ';
                if($link)
                    $this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$w_txt,$this->FontSize,$link);
        }
        }

          if($s)
            $this->_out($s);
        $this->lasth=$h;
        if($ln>0)
        {
            //Go to next line
            $this->y+=$h;
            if($ln==1)
                $this->x=$this->lMargin;
        }
        else
            $this->x+=$w;
    }


    /**
     * Mehrzeilige Zelle ausgeben
     *
	 * @param int $w
	 * @param int $h
	 * @param string $txt
	 * @param int $border
	 * @param string $align
	 * @param string $link
     */
	function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false)
	{
		if($this->_ZeichenCodierung=='CP1250')
		{
			$txt = $this->ErsetzeZeichen_CP1250($txt);
		}

		parent::MultiCell($w, $h, $txt, $border, $align, $fill);
	}

	/**
	 * Einzelne Zelle ausgeben
	 *
	 * @param int $w
	 * @param int $h
	 * @param string $txt
	 * @param int $border
	 * @param int $ln
	 * @param string $align
	 * @param boolean $fill
	 * @param string $link
	 */
	function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
	{
		if($this->_ZeichenCodierung=='CP1250')
		{
			$txt = $this->ErsetzeZeichen_CP1250($txt);
		}

		parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
	}

	/**
	 * Setzt die Zeichencodierung für die Ausgabe
	 *
	 * @param string $Codeseite
	 */
	function ZeichenKodierung($Codeseite='CP1252')
	{
		switch($Codeseite)
		{
			case 'CP1252':
			case 'CP1250':
				$this->_ZeichenCodierung = $Codeseite;
				break;
			default:
				throw new Exception('Ungueltige Codeseite',201103291140);
		}

	}


	/**
	 * Ersetzt die Sonderzeichen in AWIS Textkonserven in CP1250 Zeichen
	 *
	 * @param string $Text
	 * @return string
	 */
    function ErsetzeZeichen_CP1250($Text)
    {
        $Text = str_replace("&#225;", iconv("UTF-8","CP1250", "á"),$Text);
        $Text = str_replace("&#233;", iconv("UTF-8","CP1250", "é"),$Text);
        $Text = str_replace("&#237;", iconv("UTF-8","CP1250", "í"),$Text);
        $Text = str_replace("&#243;", iconv("UTF-8","CP1250", "ó"),$Text);
        $Text = str_replace("&#353;", iconv("UTF-8","CP1250", "š"),$Text);
        $Text = str_replace("&#357;", iconv("UTF-8","CP1250", "ť"),$Text);
        $Text = str_replace("&#382;", iconv("UTF-8","CP1250", "ž"),$Text);
        $Text = str_replace("&#283;", iconv("UTF-8","CP1250", "ě"),$Text);
        $Text = str_replace("&#271;", iconv("UTF-8","CP1250", "ď"),$Text);
        $Text = str_replace("&#328;", iconv("UTF-8","CP1250", "ň"),$Text);
        $Text = str_replace("&#345;", iconv("UTF-8","CP1250", "ř"),$Text);
        $Text = str_replace("&#367;", iconv("UTF-8","CP1250", "ů"),$Text);
        $Text = str_replace("&#269;", iconv("UTF-8","CP1250", "č"),$Text);

        $Text = str_replace("&#201;", iconv("UTF-8","CP1250", "É"),$Text);
        $Text = str_replace("&#205;", iconv("UTF-8","CP1250", "Í"),$Text);

        $Text = str_replace("&#211;", iconv("UTF-8","CP1250", "Ó"),$Text);
        $Text = str_replace("&#218;", iconv("UTF-8","CP1250", "Ú"),$Text);
        $Text = str_replace("&#193;", iconv("UTF-8","CP1250", "Á"),$Text);
        $Text = str_replace("&#268;", iconv("UTF-8","CP1250", "Č"),$Text);
        $Text = str_replace("&#352;", iconv("UTF-8","CP1250", "Š"),$Text);
        $Text = str_replace("&#356;", iconv("UTF-8","CP1250", "Ť"),$Text);
        $Text = str_replace("&#381;", iconv("UTF-8","CP1250", "Ž"),$Text);
        $Text = str_replace("&#282;", iconv("UTF-8","CP1250", "Ě"),$Text);
        $Text = str_replace("&#270;", iconv("UTF-8","CP1250", "Ď"),$Text);
        $Text = str_replace("&#327;", iconv("UTF-8","CP1250", "Ň"),$Text);
        $Text = str_replace("&#344;", iconv("UTF-8","CP1250", "Ř"),$Text);
        $Text = str_replace("&#344;", iconv("UTF-8","CP1250", "Ř"),$Text);
        $Text = str_replace("&#366;", iconv("UTF-8","CP1250", "Ů"),$Text);
        /*
		$a["á"] = iconv("UTF-8","CP1250", "á");
		$a["é"] = iconv("UTF-8","CP1250", "é");
		$a["í"] = iconv("UTF-8","CP1250", "í");
		$a["ó"] = iconv("UTF-8","CP1250", "ó");
		$a["ú"] = iconv("UTF-8","CP1250", "ú");
		$a["š"] = iconv("UTF-8","CP1250", "š");
		$a["ť"] = iconv("UTF-8","CP1250", "ť");
		$a["ž"] = iconv("UTF-8","CP1250", "ž");
		$a["ě"] = iconv("UTF-8","CP1250", "ě");
		$a["ď"] = iconv("UTF-8","CP1250", "ď");
		$a["ň"] = iconv("UTF-8","CP1250", "ň");
		$a["ř"] = iconv("UTF-8","CP1250", "ř");
		$a["ů"] = iconv("UTF-8","CP1250", "ů");
		$a["č"] = iconv("UTF-8","CP1250", "č");

		$a["É"] = iconv("UTF-8","CP1250", "É");
		$a["Í"] = iconv("UTF-8","CP1250", "Í");
		$a["Ó"] = iconv("UTF-8","CP1250", "Ó");
		$a["Ú"] = iconv("UTF-8","CP1250", "Ú");
		$a["Á"] = iconv("UTF-8","CP1250", "Á");
		$a["Č"] = iconv("UTF-8","CP1250", "Č");
		$a["Š"] = iconv("UTF-8","CP1250", "Š");
		$a["Ť"] = iconv("UTF-8","CP1250", "Ť");
		$a["Ž"] = iconv("UTF-8","CP1250", "Ž");
		$a["Ě"] = iconv("UTF-8","CP1250", "Ě");
		$a["Ď"] = iconv("UTF-8","CP1250", "Ď");
		$a["Ň"] = iconv("UTF-8","CP1250", "Ň");
		$a["Ř"] = iconv("UTF-8","CP1250", "Ř");
		$a["Ů"] = iconv("UTF-8","CP1250", "Ů");


        foreach ($a as $key=>$value)
        {
            $Text = str_replace($key,$value,$Text);
        }
        */
        return $Text;
    }
	public function SetCharSpacing( $s_w , $s_l = 0)
	{  
        $this->s_w = $s_w; 
        $this->s_l = $s_l; 
        $this->_out( 
            'BT '. 
            sprintf('%.3f Tc ', $this->s_w * $this->k). 
            sprintf('%.3f Tw ', $this->s_l * $this->k). 
            'ET ' 
            );  
	} 
}
?>