<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
try {
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $Recht47000 = $AWISBenutzer->HatDasRecht(47000);
    if ($Recht47000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Form->DebugAusgabe(1, __FILE__ . ': AWIS_KEY1: ' . $AWIS_KEY1);

    //AWIS_KEY1
    if ($AWIS_KEY1 == -1) {//Neuer KopfDS

        $SQL = 'INSERT';
        $SQL .= ' INTO EUKUNDEN';
        $SQL .= '   (';
        $SQL .= '     EKU_USTIDNR,';
        $SQL .= '     EKU_ANREDE,';
        $SQL .= '     EKU_NAME,';
        $SQL .= '     EKU_STRASSE,';
        $SQL .= '     EKU_LAN_NR,';
        $SQL .= '     EKU_PLZ,';
        $SQL .= '     EKU_ORT,';
        $SQL .= '     EKU_GUELTIG,';
        $SQL .= '     EKU_PRUEFDATUM,';
        $SQL .= '     EKU_USER,';
        $SQL .= '     EKU_USERDAT';
        $SQL .= '   )';
        $SQL .= '   VALUES';
        $SQL .= '   (';
        $SQL .= '     ' . $DB->WertSetzen('ERK', 'T', isset($_POST['sucEKU_USTIDNR'])?$_POST['sucEKU_USTIDNR']:$_POST['txtEKU_USTIDNR']);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_ANREDE']);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_NAME']);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_STRASSE']);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_LAN_NR']);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_PLZ']);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_ORT']);
        $Wert = 0;
        if (isset($_POST['txtEKU_GUELTIG'])) {
            $Wert = 1;
        }
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $Wert);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'D', $_POST['txtEKU_PRUEFDATUM']);
        $SQL .= '     ,' . $DB->WertSetzen('ERK', 'T', $AWISBenutzer->BenutzerName());;
        $SQL .= '    , sysdate';
        $SQL .= '   )';

        $DB->Ausfuehren($SQL, '', '', $DB->Bindevariablen('ERK'));
        $Form->DebugAusgabe(1, $DB->LetzterSQL());
        $AWIS_KEY1 = $DB->RecordSetOeffnen('select seq_trg_eku_key.currval from dual')->FeldInhalt(1);
        $_POST['txtEKU_KEY'] = $AWIS_KEY1;
    } elseif ($AWIS_KEY1 > 0 and $AWIS_KEY2 == '') {//Gešnderter KopfDS. KopfDS Kann nur gešndert wreden, wenn man nicht in der Positionsanlage/Bearbeitung war

        $Update = 'UPDATE EUKUNDEN';
        $Update .= ' SET ';
        $Update .= '  EKU_USTIDNR    = ' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_USTIDNR']);
        $Update .= ' , EKU_ANREDE     = ' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_ANREDE']);
        $Update .= ' , EKU_NAME       = ' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_NAME']);
        $Update .= ' , EKU_STRASSE    = ' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_STRASSE']);
        $Update .= ' , EKU_LAN_NR     = ' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_LAN_NR']);
        $Update .= ' , EKU_PLZ        = ' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_PLZ']);
        $Update .= ' , EKU_ORT        = ' . $DB->WertSetzen('ERK', 'T', $_POST['txtEKU_ORT']);
        $Wert = 0;
        if (isset($_POST['txtEKU_GUELTIG'])) {
            $Wert = 1;
        }
        $Update .= ' , EKU_GUELTIG    = ' . $DB->WertSetzen('ERK', 'T', $Wert);
        $Update .= ' , EKU_PRUEFDATUM = ' . $DB->WertSetzen('ERK', 'D', $_POST['txtEKU_PRUEFDATUM']);
        $Update .= ' , EKU_USER       = ' . $DB->WertSetzen('ERK', 'T', $AWISBenutzer->BenutzerName()) . '';
        $Update .= ' , EKU_USERDAT    = sysdate';
        $Update .= ' WHERE EKU_KEY      = ' . $DB->WertSetzen('ERK', 'N0', $AWIS_KEY1);

        $DB->Ausfuehren($Update, '', '', $DB->Bindevariablen('ERK'));
    }

    $Form->DebugAusgabe(1, $DB->LetzterSQL());
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
    $Form->DebugAusgabe(1, $DB->LetzterSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $DB->LetzterSQL());
}

?>