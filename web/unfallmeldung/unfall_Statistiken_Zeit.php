<?php
require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;
global $StatParam;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('UNF','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort', 'AlleAnzeigen');    
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht21035 = $AWISBenutzer->HatDasRecht(21033);
	if($Recht21035 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
    
    $Seite = 'Zeit';                // Subreiter => wird z.B. bei Sortierungen verwendet
	
    $FeldBreiten = array();
    $FeldBreiten['Zeit'] = 380;
    //$FeldBreiten['Info'] = 320;
    $FeldBreiten['Zahl'] = 120;    
	
    //************************************************************
	//* Hier nochmal Parameter anzeigen, die als Filter benutzt werden
	//************************************************************
    $Form->Trennzeile('O');
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_FIL_ID'].':', 200);
    $Form->Erstelle_TextFeld('UNF_FIL_ID', $StatParam['STAT_UNF_FilNr'], 5, 200, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['VON'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DAUERVON', $StatParam['STAT_UNF_DatumVon'], 6, 86, false, '', '', '', 'D');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['BIS'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DAUERBIS', $StatParam['STAT_UNF_DatumBis'], 6, 70, false, '', '', '', 'D');
    $Form->ZeileEnde();        
    
    $StatParam['STAT_UNF_Bereich'] = $AWISSprachKonserven['Wort']['AlleAnzeigen'];      // pauschal mit "Alle Anzeigen" vorbelegen, da hier Auswahl aus �berReiterSeite noch keinen Sinn macht
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEREICH'].':', 200);
    $Form->Erstelle_TextFeld('UNF_BEREICHID', $StatParam['STAT_UNF_Bereich'], 100, 200, false);
    $Form->ZeileEnde();    
    
    $Form->Trennzeile('O');
    
    //************************************************************
	//* �berschriftenzeile der Listenansicht
	//************************************************************
    $Form->ZeileStart();    
    // �berschrift der Listenansicht mit Sortierungslink: Rang
    $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite='.$Seite.'&STATSort=zeit'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='zeit'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ZEIT'], $FeldBreiten['Zeit'], '', $Link);
    // �berschrift der Listenansicht mit Sortierungslink: Info
    //$Link = './unfall_Main.php?cmdAktion=Statistiken&STATSort=bezeichnung'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='bezeichnung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['INFO'], $FeldBreiten['Info'], '', $Link);			
    // �berschrift der Listenansicht mit Sortierungslink: Zahl
    $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite='.$Seite.'&STATSort=anzahl'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='anzahl'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ZAHL'], $FeldBreiten['Zahl'], '', $Link);
    $Form->ZeileEnde();            
    
    //************************************************************
	//* Haupt-SQL-Zusammenbau-Bereich: Start
	//************************************************************
    $Bedingung = '';        // Var zum Zusammenbauen der Filter
    // immer wenn Parameter da, mit and an Bedingungsstring anh�ngen
    if (! empty($StatParam['STAT_UNF_FilNr']))
    {
        $Bedingung .= ' and unf.unf_fil_id = ' . $StatParam['STAT_UNF_FilNr'];
    }
    if (! empty($StatParam['STAT_UNF_DatumVon']))
    {
        $Bedingung .= ' and unf.unf_datum >= ' . $DB->FeldInhaltFormat('D', $StatParam['STAT_UNF_DatumVon']);    
    }
    if (! empty($StatParam['STAT_UNF_DatumBis']))
    {
        $Bedingung .= ' and unf.unf_datum <= ' . $DB->FeldInhaltFormat('D', $StatParam['STAT_UNF_DatumBis']);    
    }
	if ($Bedingung != '')
	{
		$Bedingung = $Bedingung . ' ';
	}    
    
    // Array (Gr��e enspricht auch den folgenden Schleifendurchlauf) mit den abzugragenden Zeitintervallen
    // Dimension 1: von & bis
    // Dimension 2: von-Zeit und Bis-Zeit, je nach 1. Dimension 
    //              ("von" & "bis" m�ssen immer die gleichen Grenzen haben (=> sonst krachts in der Schleife) 
    //              und die Inhalte der Indizes zusammenpassen (=> sonst kann der SQL Schrott bringen))
    $Zeiten = array();
    $Zeiten['von'] = array();
    $Zeiten['bis'] = array();
    $Zeiten['von'][] = '00:00';     $Zeiten['bis'][] = '07:00';
    $Zeiten['von'][] = '07:01';     $Zeiten['bis'][] = '09:15';
    $Zeiten['von'][] = '09:16';     $Zeiten['bis'][] = '11:15';
    $Zeiten['von'][] = '11:16';     $Zeiten['bis'][] = '13:15';
    $Zeiten['von'][] = '13:16';     $Zeiten['bis'][] = '15:15';
    $Zeiten['von'][] = '15:16';     $Zeiten['bis'][] = '17:15';
    $Zeiten['von'][] = '17:16';     $Zeiten['bis'][] = '19:00';
    $Zeiten['von'][] = '19:01';     $Zeiten['bis'][] = '23:59';
    
    $SQL =  '';
    $SQL .= 'select * from (';
    
    // Schleife �ber eines der Beiden Arrays (Grenzen sind von Von-Array und Bis-Array eh gleich)
    for ($Idx = 0; $Idx <= count($Zeiten['von'])-1; $Idx++)
    {
        $SQL .= 'select ' . $DB->FeldInhaltFormat('T', $Zeiten['von'][$Idx] . ' - ' . $Zeiten['bis'][$Idx]) . ' as zeit, ';
        $SQL .= 'count(*) as Anzahl from v_unfallmeldung unf ';
        $SQL .= 'where to_timestamp(to_char(unf_datum, \'hh24:mi\'), \'hh24:mi\') >= to_timestamp(\'' . $Zeiten['von'][$Idx] . '\', \'hh24:mi\') ';
        $SQL .= 'and to_timestamp(to_char(unf_datum, \'hh24:mi\'), \'hh24:mi\') <= to_timestamp(\'' . $Zeiten['bis'][$Idx] . '\', \'hh24:mi\') ';
        $SQL .= $Bedingung;
        
        // nach letzten Element kein 'union' mehr anf�gen
        if ($Idx != (count($Zeiten['von'])-1))
        {
            $SQL .= 'union ';            
        }
    }
    
    $SQL .= ') ';
    
    if (isset($_GET['STATSort']))
    {
        $SQL .= 'order by ' . str_replace('~',' DESC ', $_GET['STATSort']);
    }
    else
    {
        $SQL .= 'order by zeit';
    }        
    
    //************************************************************
	//* Haupt-SQL-Zusammenbau-Bereich: Ende
	//************************************************************    
    $rs = $DB->RecordSetOeffnen($SQL);  
    
    //************************************************************
	//* Listen in Schleife aufbauen
	//************************************************************        
    $DS = 0;	// f�r Hintergrundfarbumschaltung
    $AnzahlGesamt = 0;
    while(! $rs->EOF())
    {
        $Form->ZeileStart();
        $Form->Erstelle_ListenFeld('*ZEIT', $rs->FeldInhalt('ZEIT'), 50, $FeldBreiten['Zeit'], false, ($DS%2), '', '', 'T', 'L');
        $Form->Erstelle_ListenFeld('*ZAHL', $rs->FeldInhalt('ANZAHL'), 10, $FeldBreiten['Zahl'], false, ($DS%2), '', '', 'N0', 'L');		
        $AnzahlGesamt += $rs->FeldInhalt('ANZAHL');     // Gesamtzahl hochaddieren
        $Form->ZeileEnde();
        $DS++;
        $rs->DSWeiter();
    }
    // Gesamtzahl noch hinpinseln
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ANZAHLGESAMT'].': '.$AnzahlGesamt, $FeldBreiten['Zeit']+$FeldBreiten['Zahl']+4, 'font-weight:bolder');        
    
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>