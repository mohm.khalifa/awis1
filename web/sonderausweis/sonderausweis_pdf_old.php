<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>

<body>
<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once 'fpdf.php';
require_once 'fpdi.php';
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;
global $Zeile;
global $LinkerRand;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

clearstatcache();

include ("ATU_Header.php");

awis_Debug(1,$_REQUEST);

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1700);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Sonderausweis-PDF',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

/*$Params=explode(';',awis_BenutzerParameter($con, "Sonderausweis_PDF", $AWISBenutzer->BenutzerName()));

if($Params[0]!='')
{
	$PersNr=$Params[0];
}
else
{
	die("<span class=HinweisText>Keine Personalnummer ausgew&auml;hlt!</span>");
}*/

if(isset($_POST['cmdErstelleLieferanten_x']))
{
	echo "<h1>Ausweis - Erstellung f�r Besucher / Lieferanten</h1>";
	echo "<hr>";

	if($_REQUEST['txtAusweis_Typ']==0)
	{
		die("<span class=HinweisText>Kein g&uuml;ltiger Ausweistyp angegeben!</span>");
	}
	else
	{
		
		if($_REQUEST['txtAusweis_Typ']==1)
		{
			$Bezeichnung='Besucherausweis';
		}
		elseif($_REQUEST['txtAusweis_Typ']==2)
		{
			$Bezeichnung='Lieferantenausweis';
		}
		else
		{
			die("<span class=HinweisText>Kein g&uuml;ltiger Ausweistyp angegeben!</span>");
		}
	}
	
	if((is_numeric($_REQUEST['txtNummerVon']) && strlen($_REQUEST['txtNummerVon'])<4) && (is_numeric($_REQUEST['txtNummerBis']) && strlen($_REQUEST['txtNummerBis'])<4))
	{
		$Start=$_REQUEST['txtNummerVon'];
		$Ende=$_REQUEST['txtNummerBis'];
	}
	else
	{
		die("<span class=HinweisText>Keine g&uuml;ltige Nummerierung angegeben! Von -> ".$_REQUEST['txtNummerVon']." Bis -> ".$_REQUEST['txtNummerBis']."</span>");
	}
	
	
	if($Start>$Ende)
	{
		die("<span class=HinweisText>Nummer Von ist gr&ouml;sser als Nummer Bis</span>");
	}

	$Zeile=0;

	/***************************************
	* Neue PDF Datei erstellen
	***************************************/

        ob_clean();

	define('FPDF_FONTPATH','font/');
	$pdf_format=array('54','86');
	$pdf = new fpdi('l','mm',$pdf_format);
	$pdf->open();
	
	/***************************************
	* Daten laden
	***************************************/
	
	for($i=$Start;$i<=$Ende;$i++)
	{	
		$LinkerRand=3;
	
		$Y_Wert=4;
		
		$Nummer=str_pad($i,3,'0',STR_PAD_LEFT);
		
		//NeueSeite($pdf,$Zeile,$LinkerRand);
		NeueSeite($pdf);
		
		$pdf->SetFont('Arial','b',24);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,$Bezeichnung,0,0,'L',0);
		
		$Y_Wert=$Y_Wert+36;
		
		$pdf->SetFont('Arial','b',48);
		$pdf->setXY($LinkerRand+7,$Y_Wert);
		$pdf->cell(30,6,$Nummer,0,0,'L',0);
		
		//NeueSeite($pdf,$Zeile,$LinkerRand);
		NeueSeite($pdf);

	}
	// Ende Datenaufbereitung
	
	
	/***************************************
	* Abschluss und Datei speichern
	***************************************/
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->output();
		
	echo "<br><a target=_new href=$DateiNameLink>Ausweis anzeigen</a><p>";
	echo "<hr><a href=./sonderausweis_Main.php?cmdAktion=Detail><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
}
elseif(isset($_POST['cmdErstelleRabatt_x']))
{
	echo "<h1>Ausweis - Erstellung f�r Rabatt</h1>";
	echo "<hr>";
	
	if((is_numeric($_REQUEST['txtNummerVon']) && strlen($_REQUEST['txtNummerVon'])<4) && (is_numeric($_REQUEST['txtNummerBis']) && strlen($_REQUEST['txtNummerBis'])<4))
	{
		$Start=$_REQUEST['txtNummerVon'];
		$Ende=$_REQUEST['txtNummerBis'];
	}
	else
	{
		die("<span class=HinweisText>Keine g&uuml;ltige Nummerierung angegeben! Von -> ".$_REQUEST['txtNummerVon']." Bis -> ".$_REQUEST['txtNummerBis']."</span>");
	}
	
	
	if($Start>$Ende)
	{
		die("<span class=HinweisText>Nummer Von ist gr&ouml;sser als Nummer Bis</span>");
	}

	$Zeile=0;

	/***************************************
	* Neue PDF Datei erstellen
	***************************************/
	
	define('FPDF_FONTPATH','font/');
	$pdf_format=array('54','86');
	$pdf = new fpdi('l','mm',$pdf_format);
	$pdf->open();
	$pdf->AddFont('verdana','b','verdanab.php');
	
	/***************************************
	* Daten laden
	***************************************/
	
	if(isset($_REQUEST['txtRabattEAN']) && is_numeric($_REQUEST['txtRabattEAN']) && strlen($_REQUEST['txtRabattEAN'])==13)
	{
		$PDF_EAN=Erzeuge_EAN13(substr($_REQUEST['txtRabattEAN'],0,12));
	}

	var_dump($_REQUEST);
	var_dump($PDF_EAN);
	
	for($i=$Start;$i<=$Ende;$i++)
	{	
		$LinkerRand=3;
		
		$Y_Wert=4;
		
		//NeueSeite($pdf,$Zeile,$LinkerRand);
		NeueSeite($pdf);
		
		$pdf->SetFont('Verdana','B',16);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,'Rabatt-Ausweis',0,0,'L',0);
		
		$Y_Wert=$Y_Wert+6;
		
		$pdf->SetFont('Verdana','B',12);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,$_REQUEST['txtName1'],0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		
		$pdf->SetFont('Verdana','B',12);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,$_REQUEST['txtName2'],0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		
		$pdf->SetFont('Verdana','B',12);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,$_REQUEST['txtName3'],0,0,'L',0);
		
		$Y_Wert=$Y_Wert+5;
		
		$Nummer=str_pad($i,3,'0',STR_PAD_LEFT);
		$pdf->SetFont('Arial','b',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,'Karten-Nr.: '.$Nummer,0,0,'L',0);
		
		if (isset($_REQUEST['txtGueltigBis']) && $_REQUEST['txtGueltigBis']!='')
		{
			$pdf->SetFont('Arial','B',10);
			$pdf->setXY($LinkerRand+46,$Y_Wert);
			$pdf->cell(30,6,'G�ltig bis '.$_REQUEST['txtGueltigBis'],0,0,'L',0);
		}

		if(isset($_REQUEST['txtRabattEAN']) && is_numeric($_REQUEST['txtRabattEAN']) && strlen($_REQUEST['txtRabattEAN'])==13)
		{
			
			$Y_Wert=$Y_Wert+7;
			
			$pdf->Image($PDF_EAN,$LinkerRand,$Y_Wert,'','','png');
				
			$Y_Wert=$Y_Wert+13;
			$pdf->SetFont('Arial','B',8);
			$pdf->setXY($LinkerRand+3,$Y_Wert);
			$pdf->SetCharSpacing(1,0);
			$pdf->cell(30,6,$_REQUEST['txtRabattEAN'],0,0,'L',0);
		}
		else 
		{
			$Y_Wert=$Y_Wert+12;
			$pdf->SetFont('Arial','B',11);
			$pdf->setXY($LinkerRand+3,$Y_Wert);
			$pdf->Cell(30,12,$_REQUEST['txtRabattText'],0,0,'L',0);
		}
		//NeueSeite($pdf,$Zeile,$LinkerRand);
		NeueSeite($pdf);
		
		$LinkerRand=3;
		
		$Y_Wert=40;
		
		$pdf->SetFont('Arial','B',8);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(80,6,'www.atu.de',0,0,'C',0);
	
		$Y_Wert=$Y_Wert+4;
		
		$pdf->SetFont('Arial','B',6);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(80,6,'Achtung: Diese Karte ist nicht �bertragbar.',0,0,'C',0);
		
	}
	// Ende Datenaufbereitung
	
	/***************************************
	* Abschluss und Datei speichern
	***************************************/
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->saveas($DateiName);
		
	echo "<br><a target=_new href=$DateiNameLink>Ausweis anzeigen</a><p>";
	echo "<hr><a href=./sonderausweis_Main.php?cmdAktion=Detail><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
}
elseif($_REQUEST['txtSAW_KEY']!='')
{
	echo "<h1>Ausweis - Erstellung f�r Personalnummer</h1>";
	echo "<hr>";
	
	$Zeile=0;
	ob_clean();
	/*if(isset($_POST['cmdAusweisAnzeigen_x']))
	{*/
		/***************************************
		* Neue PDF Datei erstellen
		***************************************/
		
		define('FPDF_FONTPATH','font/');
		$pdf_format=array('54','86');
		$pdf = new fpdi('l','mm',$pdf_format);
		$pdf->open();
		//$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
		//$ATULogo = $pdf->ImportPage(1);				
		
		/***************************************
		*
		* Daten laden
		*
		***************************************/
		
		$Bemerkung="Dieser Ausweis ist nur in Verbindung mit dem Personalausweis g�ltig, ist nicht �bertragbar und muss ";
		$Bemerkung.="nach dem Ausscheiden aus dem Unternehmen unaufgefordert zur�ckgegeben werden.\n";
		$Bemerkung.="Ein etwaiger Verlust ist unverz�glich zu melden.";
		
		$SQL="SELECT SAW_PER_NR, ANR_ANREDE, SAW_FIRMA, SAW_NAME, ";
		$SQL.="SAW_VORNAME, SAW_EAN, to_char(SAW_GUELTIGBIS,'MM/RR') as GUELTIGBIS, SAW_SAG_KEY ";
		$SQL.="FROM SONDERAUSWEISE, ANREDEN WHERE SAW_ANR_ID=ANR_ID(+) AND SAW_KEY=0".$_REQUEST['txtSAW_KEY'];
		
		$rsAusweisPDF = awisOpenRecordset($con,$SQL);
		$rsAusweisPDFZeilen = $awisRSZeilen;
		
		if($rsAusweisPDFZeilen==0)
		{
			$pdf->addpage();
			$pdf->SetAutoPageBreak(true,0);
			//$pdf->useTemplate($ATULogo,50,4,20);
			$pdf->SetFont('Arial','',10);
			$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
		}
		else 		// Daten gefunden
		{	
			$LinkerRand=3;
			
			$Y_Wert=4;
			
			//Font Verdana-Bold hinzuf�gen
			$pdf->AddFont('verdana','b','verdanab.php');
			
			for($i=0;$i<$rsAusweisPDFZeilen;$i++)
			{	
				//NeueSeite($pdf,$Zeile,$LinkerRand);
				NeueSeite($pdf);
				
				//if(($rsAusweisPDF['SAW_PER_NR'][$i]>=450000 && $rsAusweisPDF['SAW_PER_NR'][$i]<=600000) || ($rsAusweisPDF['SAW_PER_NR'][$i]>=970000 && $rsAusweisPDF['SAW_PER_NR'][$i]<=974999) || ($rsAusweisPDF['SAW_PER_NR'][$i]>=990000 && $rsAusweisPDF['SAW_PER_NR'][$i]<=999999))
				if(($rsAusweisPDF['SAW_SAG_KEY'][$i]==1) || ($rsAusweisPDF['SAW_SAG_KEY'][$i]==2) || ($rsAusweisPDF['SAW_SAG_KEY'][$i]==5) || ($rsAusweisPDF['SAW_SAG_KEY'][$i]==6))
				{
					$pdf->SetFont('Verdana','B',16);
					$pdf->setXY($LinkerRand,$Y_Wert);
					$pdf->cell(30,6,'Mitarbeiterausweis',0,0,'L',0);

                                        $Y_Wert=$Y_Wert+8;

				if(empty($rsAusweisPDF['SAW_FIRMA'][$i]))
				{
					$pdf->SetFont('verdana','b' ,12);
					$pdf->setXY($LinkerRand,$Y_Wert);
					$pdf->cell(30,6,$rsAusweisPDF['SAW_NAME'][$i]." ".$rsAusweisPDF['SAW_VORNAME'][$i] ,0,0,'L',0);

					$Y_Wert=$Y_Wert+20;
				}
				else
				{
					if(!empty($rsAusweisPDF['SAW_NAME'][$i]) || !empty($rsAusweisPDF['SAW_VORNAME'][$i]))
					{
						$pdf->SetFont('Verdana','B',12);
						$pdf->setXY($LinkerRand,$Y_Wert);
						$pdf->cell(30,6,$rsAusweisPDF['SAW_FIRMA'][$i],0,0,'L',0);

						$Y_Wert=$Y_Wert+5;

						$pdf->SetFont('Verdana','b',12);
						$pdf->setXY($LinkerRand,$Y_Wert);
						$pdf->cell(30,6,$rsAusweisPDF['SAW_NAME'][$i]." ".$rsAusweisPDF['SAW_VORNAME'][$i] ,0,0,'L',0);

						$Y_Wert=$Y_Wert+15;
					}
					else
					{
						$pdf->SetFont('Verdana','B',12);
						$pdf->setXY($LinkerRand,$Y_Wert);
						$pdf->cell(30,6,$rsAusweisPDF['SAW_FIRMA'][$i],0,0,'L',0);

						$Y_Wert=$Y_Wert+20;
					}

				}

				if(!empty($rsAusweisPDF['GUELTIGBIS'][$i]))
				{
					$pdf->SetFont('Arial','B',10);
					$pdf->setXY($LinkerRand+48,$Y_Wert);
					$pdf->cell(30,6,'G�ltig bis '.$rsAusweisPDF['GUELTIGBIS'][$i],0,0,'L',0);
				}

				//$Y_Wert=$Y_Wert+4;

				$pdf->Image(Erzeuge_EAN13(substr($rsAusweisPDF['SAW_EAN'][$i],0,12)),$LinkerRand+2,$Y_Wert,'','','png');

				$Y_Wert=$Y_Wert+14;
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+5,$Y_Wert);
				$pdf->SetCharSpacing(1,0);

				$pdf->cell(30,6,$rsAusweisPDF['SAW_EAN'][$i],0,0,'L',0);


				//NeueSeite($pdf,$Zeile,$LinkerRand);
				NeueSeite($pdf);

				$Y_Wert=20;

				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(30,6,'Personal-Nr.:',0,0,'L',0);

				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+25,$Y_Wert);
				$pdf->cell(30,6,$rsAusweisPDF['SAW_PER_NR'][$i],0,0,'L',0);

				$Y_Wert=$Y_Wert+4;

				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(30,6,'Mitarbeitername:',0,0,'L',0);

				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+25,$Y_Wert);
				$pdf->cell(30,6,$rsAusweisPDF['SAW_NAME'][$i]." ".$rsAusweisPDF['SAW_VORNAME'][$i],0,0,'L',0);

				$Y_Wert=$Y_Wert+4;

				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(30,6,'Tel.-Nr. Zentrale:',0,0,'L',0);

				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+25,$Y_Wert);
				$pdf->cell(30,6,'0961 3060',0,0,'L',0);

				$Y_Wert=$Y_Wert+12;

				$pdf->SetFont('Arial','B',6);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->MultiCell(80,2,$Bemerkung,0,'L',0);

                                }
				else
				{
				    $pdf->SetFont('Verdana','B',16);
                                    $pdf->setXY($LinkerRand,$Y_Wert);
                                    $pdf->cell(30,6,'Sonderausweis',0,0,'L',0);

                                    $Bemerkung="- Dieser Ausweis ist nur in Verbindung mit dem Personalausweis,\n  Betriebsausweis bzw. Dienstausweis g�ltig und ist nicht �bertragbar.\n";
                                    $Bemerkung.="- Jeglicher Missbrauch von Personalrabatt - darunter f�llt auch die\n  Gew�hrung f�r";
                                    $Bemerkung.=" Dritte - wird nicht geduldet und kann zum Entzug des\n  Sonderausweises f�hren. \n";
                                    $Bemerkung.="- Ein etwaiger Verlust ist der Personalabteilung zu melden.";

                                    $Y_Wert=$Y_Wert+8;
				
				if(empty($rsAusweisPDF['SAW_FIRMA'][$i]))
				{	
					$pdf->SetFont('verdana','b' ,12);
					$pdf->setXY($LinkerRand,$Y_Wert);
					$pdf->cell(30,6,$rsAusweisPDF['SAW_NAME'][$i]." ".$rsAusweisPDF['SAW_VORNAME'][$i] ,0,0,'L',0);

                                         $Y_Wert=$Y_Wert+8;

                                        $pdf->SetFont('Verdana','b',8);
					$pdf->setXY(6,$Y_Wert);
					$pdf->cell(74,5,"Rabatt nur auf Ware, nicht auf Dienstleistung" ,1,0,'LTRB',0);

                                        $Y_Wert=$Y_Wert+12;

					//$Y_Wert=$Y_Wert+20;
				}
				else
				{
					if(!empty($rsAusweisPDF['SAW_NAME'][$i]) || !empty($rsAusweisPDF['SAW_VORNAME'][$i]))
					{
						$pdf->SetFont('Verdana','B',12);
						$pdf->setXY($LinkerRand,$Y_Wert);
						$pdf->cell(30,6,$rsAusweisPDF['SAW_FIRMA'][$i],0,0,'L',0);
						
						$Y_Wert=$Y_Wert+5;
						
						$pdf->SetFont('Verdana','b',12);
						$pdf->setXY($LinkerRand,$Y_Wert);
						$pdf->cell(30,6,$rsAusweisPDF['SAW_NAME'][$i]." ".$rsAusweisPDF['SAW_VORNAME'][$i] ,0,0,'L',0);

                                                $Y_Wert=$Y_Wert+7;

                                                $pdf->SetFont('Verdana','b',8);
                                                $pdf->setXY(6,$Y_Wert);
                                                $pdf->cell(74,5,"Rabatt nur auf Ware, nicht auf Dienstleistung" ,1,0,'LTRB',0);


						$Y_Wert=$Y_Wert+7;
					}
					else
					{
						$pdf->SetFont('Verdana','B',12);
						$pdf->setXY($LinkerRand,$Y_Wert);
						$pdf->cell(30,6,$rsAusweisPDF['SAW_FIRMA'][$i],0,0,'L',0);
						
						$Y_Wert=$Y_Wert+8;

                                                $pdf->SetFont('Verdana','b',8);
                                                $pdf->setXY(6,$Y_Wert);
                                                $pdf->cell(74,5,"Rabatt nur auf Ware, nicht auf Dienstleistung" ,1,0,'LTRB',0);


						$Y_Wert=$Y_Wert+12;
					}
										
				}
							
				if(!empty($rsAusweisPDF['GUELTIGBIS'][$i]))
				{
					$pdf->SetFont('Arial','B',10);
					$pdf->setXY($LinkerRand+48,$Y_Wert);
					$pdf->cell(30,6,'G�ltig bis '.$rsAusweisPDF['GUELTIGBIS'][$i],0,0,'L',0);
				}
				
				//$Y_Wert=$Y_Wert+4;
				
				$pdf->Image(Erzeuge_EAN13(substr($rsAusweisPDF['SAW_EAN'][$i],0,12)),$LinkerRand+2,$Y_Wert,'','','png');
				$pdf->Image("../bilder/atulogo_2008_Sonderausweis.png",51,$Y_Wert + 7,28,10,'png');



				$Y_Wert=$Y_Wert+14;
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+5,$Y_Wert);
				$pdf->SetCharSpacing(1,0);
                                
				$pdf->cell(30,6,$rsAusweisPDF['SAW_EAN'][$i],0,0,'L',0);
				
				
				//NeueSeite($pdf,$Zeile,$LinkerRand);
				NeueSeite($pdf);
				
				$Y_Wert=14;
				
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(30,6,'Personal-Nr.:',0,0,'L',0);
	
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+33,$Y_Wert);
				$pdf->cell(30,6,$rsAusweisPDF['SAW_PER_NR'][$i],0,0,'L',0);
				
				$Y_Wert=$Y_Wert+4;
				
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(30,6,'Mitarbeitername:',0,0,'L',0);
	
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+33,$Y_Wert);
				$pdf->cell(30,6,$rsAusweisPDF['SAW_NAME'][$i]." ".$rsAusweisPDF['SAW_VORNAME'][$i],0,0,'L',0);
				
				$Y_Wert=$Y_Wert+4;
				
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(35,6,'Personalabteilung:',0,0,'L',0);
	
				$pdf->SetFont('Arial','B',8);
				$pdf->setXY($LinkerRand+33,$Y_Wert);
				$pdf->cell(30,6,'Hr. Schmid (0961/306 -5514)',0,0,'L',0);

                                $Y_Wert=$Y_Wert+16;
				
				$pdf->SetFont('Arial','B',6);
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->MultiCell(80,2,$Bemerkung,0,'L',0);

                           }
				
				
			}
			
		}	// Ende Datenaufbereitung
		
		
		/***************************************
		* Abschluss und Datei speichern
		***************************************/
		
		$DateiName = awis_UserExportDateiName('.pdf');
		$DateiNameLink = pathinfo($DateiName);
		$DateiNameLink = '/export/' . $DateiNameLink['basename'];
		$pdf->output();
		
		if(($RechteStufe&2)==2){
			echo "<br><a href=./sonderausweis_Main.php?cmdAktion=Detail&txtSAW_KEY=".$_REQUEST['txtSAW_KEY']."><img border=0 src=/bilder/aendern.png title='Bearbeiten'></a>&nbsp;<a target=_new href=$DateiNameLink>Ausweis anzeigen</a><p>";
		}
		else{
			echo "<br><a target=_new href=$DateiNameLink>Ausweis anzeigen</a><p>";	
		}
		
		echo "<hr><a href=./sonderausweis_Main.php?cmdAktion=Detail><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
		
	}
//}
/*else
{

echo '<form name=frmPDF method=post action=./sonderausweis_pdf.php>';

echo "<table width=100% id=DatenTabelle>";
echo "<tr><td>&nbsp;</td></tr>";
echo "<tr>";
echo "<td width=50><input type=image border=0 src=/bilder/sc_lupe.png name=cmdAusweisAnzeigen title='Ausweis zeigen'></td>";
echo "<td>Ausweis anzeigen</td>";
echo "</tr>";
echo "<tr><td>&nbsp;</td></tr>";
echo "<tr>";
echo "<td width=50><a href='./sonderausweis_Main.php'><img border=0 title'Zur&uuml;ck' src=/bilder/zurueck.png></a></td>";
echo "</tr>";
echo '</table>';
	
echo '</form>';


}*/


/**
*
* Funktion erzeugt eine neue Seite
*
* @author Christian Argauer
* @param  pointer pdf
* @param  pointer Zeile
* @param  int LinkerRand
*
*/
//function NeueSeite(&$pdf,$Zeile,$LinkerRand)
function NeueSeite(&$pdf)
{
	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	
}

/**
*
* Funktion speichert ein Bild mit EAN-Code
*
* @author Christian Argauer
* @param  char EAN13-Nummer 12-stellig
* @return Datei-Name
*
*/
function Erzeuge_EAN13($EAN_NR)
{
$DateiName = awis_UserExportDateiName('.png');
$symbology = BarcodeFactory::Create (ENCODING_EAN13);
$barcode = BackendFactory ::Create('IMAGE', $symbology);
$barcode ->SetScale(1);
$barcode ->SetMargins(10,0,0,1);
$barcode ->SetColor('black','white');
$barcode ->NoText(true);
$barcode -> SetHeight(42);
//$barcode -> SetHeight(50);
$barcode ->Stroke($EAN_NR,$DateiName);
return $DateiName;
}

?>
</body>
</html>
