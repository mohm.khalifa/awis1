<?php
require_once('awisKontakt.inc');
/**
 * Bereichsverwaltung f�r die Personaleins�tze
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200809161558
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PPR','%');
	$TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
	$TextKonserven[]=array('XBN','XBN_NAME');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AnzahlEintraege');
	$TextKonserven[]=array('Wort','AnzahlFehler');
	$TextKonserven[]=array('Wort','AktuellerStand');
	$TextKonserven[]=array('Wort','FehlerMeldung');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','PPR_STATUS');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Wort','NeuerStatus');
	$TextKonserven[]=array('Wort','AktionFuerAlle');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4504 = $AWISBenutzer->HatDasRecht(4504);
	if($Recht4504==0)
	{
		$Form->Fehler_KeineRechte();
	}


	//********************************************************
	// Parameter verarbeiten
	//********************************************************
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./personaleinsaetze_abschlussspeichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PPR'));
	}
	elseif(isset($_POST['cmdWeiter_x']))
	{
		$AWIS_KEY1 = 0;
	}
	elseif(isset($_GET['PPR_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PPR_KEY']);
	}
	elseif(isset($_POST['txtPPR_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtPPR_KEY']);
	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PPR'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_PPR',serialize($Param));
		}

		if(isset($_GET['PPRListe']))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY PPR_TAG DESC';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	$Form->SchreibeHTMLCode('<form name="frmPersEinsPruefungen" action="./personaleinsaetze_Main.php?cmdAktion=Abschluss" method="POST"  enctype="multipart/form-data">');

	$Form->Formular_Start();

	$EditRecht = $Recht4504;
	//******************************
	// Bearbeitungsbereich
	//******************************

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PPR']['PPR_TAG'].':',190);
	$Form->Erstelle_TextFeld('*PPR_TAG',(isset($_POST['sucPPR_TAG'])?$_POST['sucPPR_TAG']:date('d.m.Y')),10,350,true,'','','','D');
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['NeuerStatus'].':',190);
	$Form->Erstelle_SelectFeld('PPR_STATUS','',250,$EditRecht,'','','','','',explode(';',$AWISSprachKonserven['Liste']['PPR_STATUS']));
	$Form->ZeileEnde();

	$Form->Trennzeile();

	// Aktueller Stand
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['AktuellerStand'],192+254+452);
	$Form->ZeileEnde();


	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEB']['PEB_BEZEICHNUNG'],190);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PPR']['PPR_STATUS'],250);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PPR']['PPR_BEMERKUNG'],450);
	$Form->ZeileEnde();

	//*************************************************************
	//* Daten erzeugen, falls sie noch nicht existieren
	//*************************************************************
	_ErzeugePruefungen($DB->FeldInhaltFormat('D',(isset($_POST['sucPPR_TAG'])?$_POST['sucPPR_TAG']:date('d.m.Y'))));

	$SQL = 'SELECT DISTINCT peb_key, peb_bezeichnung, ppr_status, ppr_bemerkung, PPR_KEY';
	$SQL .= ' FROM Perseinsbereichemitglieder ';
	$SQL .= ' INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY';
	$SQL .= ' INNER JOIN PersEinsBereicheRechteVergabe ON PBM_KEY = PBR_PBM_KEY AND PBR_PBZ_KEY = 9';
	$SQL .= ' LEFT OUTER JOIN PersEinsPruefungen ON PPR_PEB_KEY = PEB_KEY AND PPR_TAG = '.$DB->FeldInhaltFormat('D',(isset($_POST['sucPPR_TAG'])?$_POST['sucPPR_TAG']:date('d.m.Y')),false);
	$SQL .= ' LEFT OUTER JOIN Benutzer ON pbm_xbn_key = xbn_key';
	$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
	$SQL .= ' ORDER BY peb_bezeichnung';
	$rsPEB = $DB->RecordSetOeffnen($SQL);
	$Daten = $Form->WerteListe($AWISSprachKonserven['Liste']['PPR_STATUS'],$rsPEB->FeldInhalt('PPR_STATUS'));
	while(!$rsPEB->EOF())
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($rsPEB->FeldInhalt('PEB_BEZEICHNUNG').':',190);
		$Daten = $Form->WerteListe($AWISSprachKonserven['Liste']['PPR_STATUS'],$rsPEB->FeldInhalt('PPR_STATUS'));
		$Form->Erstelle_TextFeld('*PPR_STATUS',$Daten,10,250,false,'','','','T');
		$Form->Erstelle_TextFeld('PPR_BEMERKUNG_'.$rsPEB->FeldInhalt('PPR_KEY'),$rsPEB->FeldInhalt('PPR_BEMERKUNG'),60,450,$EditRecht,'','','','T');

		$Form->ZeileEnde();

		$rsPEB->DSWeiter();
	}

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=abschluss','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_Hilfe'], 'H');
	$Form->SchaltflaechenEnde();
	//$Form->DebugAusgabe(1, $Param, $Bedingung, $rsPPR, $_POST, $rsPPR, $SQL, $AWISSprache);

	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Erzeugt alle notwendigen Eintragungen f�r die Personaleinsatzpr�fungen
 *
 * @param string $Datum (mit TO_DATE()!!)
 * @return boolean
 */
function _ErzeugePruefungen($Datum)
{
	global $DB;

	$SQL = "insert into
			Perseinspruefungen
			(ppr_tag, ppr_peb_key, ppr_status)
			select xka_tag, peb_key, status from
			(select xka_tag, peb_key, 'O' as status
			from Kalender
			cross join perseinsbereiche
			WHERE xka_tag = ".$Datum.") Daten
			 left outer join perseinspruefungen on ppr_tag = ".$Datum." and peb_key = ppr_peb_key and ppr_tag = xka_tag
			 where ppr_key is null";

	return $DB->Ausfuehren($SQL);
}
?>