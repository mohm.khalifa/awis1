<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $REX;

try {
   
    $Meldung = '';
    if (isset($_POST['txtABL_WARTEND']) and $_POST['txtABL_WARTEND'] == 'on') {
        $SQL = 'INSERT INTO ABELIEFERSCHEINE (ABL_DRUCKDATUM, ABL_WARTEND, ABL_ABF_KEY, ABL_VON, ABL_NACH, ABL_BEMERKUNG, ABL_XBN_KEY ) VALUES (sysdate, 1, ';
        $SQL .= $ABE->DB->WertSetzen('ABL', 'T', $_POST['txtABF_KEY']) . ', ';
        $SQL .= $ABE->DB->WertSetzen('ABL', 'T', $_POST['txtABL_VON']) . ', ';
        $SQL .= $ABE->DB->WertSetzen('ABL', 'T', $_POST['txtABL_NACH']) . ', ';
        $SQL .= $ABE->DB->WertSetzen('ABL', 'T', $_POST['txtABL_BEMERKUNG']) . ', ';
        $SQL .= $ABE->DB->WertSetzen('ABL', 'T', $ABE->AWISBenutzer->BenutzerID()) . ')';

        $ABE->DB->Ausfuehren($SQL, 'ABL', false, $ABE->DB->Bindevariablen('ABL', true));
        $Meldung = $ABE->AWISSprachKonserven['ABE']['ABE_INSERT_OK'];
    } else {
        $SQL = 'DELETE FROM ABELIEFERSCHEINE WHERE ABL_WARTEND = 1 AND ABL_ABF_KEY = ' . $ABE->DB->WertSetzen('ABL', 'T', $_POST['txtABF_KEY']);
        $SQL .= ' AND ABL_XBN_KEY = ' . $ABE->DB->WertSetzen('ABL', 'Z', $ABE->AWISBenutzer->BenutzerID());
        $ABE->DB->Ausfuehren($SQL, 'ABL', true, $ABE->DB->Bindevariablen('ABL', true));
    }

    if($Meldung!=''){
        $ABE->Form->ZeileStart();
        $ABE->Form->Hinweistext($Meldung);
        $ABE->Form->ZeileEnde();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201202081035");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201202081036");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>