<?php
global $con;
global $awisRSZeilen;
global $j;
global $DoppeltHoheUeberschriftZellen;
global $GBL;
global $RGZ;

	/***************************************
	* Gebiets�bersicht
	***************************************/
require_once 'fpdi.php';

ini_set('max_execution_time',60);

// Vertriebsleiter International
$SQL = "select MIT_BEZEICHNUNG";
$SQL .= ' FROM Mitarbeiter ';
$SQL .= ' INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID';
$SQL .= ' WHERE MIT_MTA_ID = 130 AND MIT_Status = \'A\'';
$rsMEI = awisOpenRecordset($con, $SQL );
$IntVertriebsLeiter = '';
for($i=0;$i<$awisRSZeilen;$i++)
{
	$IntVertriebsLeiter .= $rsMEI['MIT_BEZEICHNUNG'][$i];
}


//
$SQL = "select * ";
$SQL .= ' FROM Regionalzentren INNER JOIN Mitarbeiter ON REZ_ID = MIT_REZ_ID ';
$SQL .= ' INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID';
$SQL .= ' WHERE MIT_MTA_ID IN (1,110) AND MIT_Status = \'A\' AND REZ_LAN_CODE<>\'DE\''; // ge�ndert 06.02.07 CA: AND MIT_Status = \'A\'

if(isset($_POST['txtMIT_REZ_ID']) AND $_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
{
	$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
}

// CA 17.09.07 - VKL-International als Zust�ndiger bei Int.-Gebieten ohne Zuordnung von MTA_ID in (1,110)

$SQL .= " union";
$SQL .= " select  REZ_ID,REZ_BEZEICHNUNG,REZ_USER,REZ_USERDAT,REZ_LAN_CODE,MIT_KEY,";
$SQL .= " MIT_BEZEICHNUNG,MIT_PER_NR,MIT_KON_KEY,MIT_STATUS,REZ_ID AS MIT_REZ_ID,MIT_USER,";
$SQL .= " MIT_USERDAT,MIT_MTA_ID,MIT_AKTIVITAET,MIT_AKTIVITAETAB,MIT_BEREICH,MTA_ID,MTA_BEZEICHNUNG from";
$SQL .= " (select * from regionalzentren where rez_lan_code not in"; 
$SQL .= " (select rez_lan_code FROM Regionalzentren INNER JOIN Mitarbeiter ON REZ_ID = MIT_REZ_ID";
$SQL .= " INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID";
$SQL .= " WHERE MIT_MTA_ID IN (1,110) AND MIT_Status in('A') AND REZ_LAN_CODE<>'DE')";
$SQL .= " and REZ_LAN_CODE<>'DE'),";
$SQL .= " (select * from mitarbeiter, mitarbeitertaetigkeiten where mit_mta_id=mta_id and mta_id in (130) and mit_status='A')";

if(isset($_POST['txtMIT_REZ_ID']) AND $_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
{
	$SQL .= " WHERE MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
}

//$SQL .= ' ORDER BY REZ_BEZEICHNUNG';
$SQL .= ' ORDER BY 2'; // CA 17.09.07 - Angepasst wegen VKL-International Feld 2 => REZ_BEZEICHNUNG

awis_debug(1,$SQL);

$rsMEI = awisOpenRecordset($con, $SQL );
$rsMEIZeilen = $awisRSZeilen;
	// Alle kommissarischen TKDLs
$rsFILBem = awisOpenRecordset($con, 'SELECT FIF_WERT, FIF_FIL_ID FROM FilialInfos WHERE FIF_FIT_ID=28');
$rsFILBemZeilen = $awisRSZeilen;

$pdf = new fpdi('l','mm','a4');
$pdf->open();
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);
if($rsMEIZeilen==0)
{
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
}

$RasterZeile=9999;
$LetztesREZ = '';
$LaenderBloecke=0;

for($i=0;$i<$rsMEIZeilen;$i++)			// L�nder mit Regionalzentren
{
	$Seite=1;

	if(($i%2)==0 OR $LaenderBloecke>3)
	{
		$pdf->addpage();
		$pdf->SetAutoPageBreak(true,0);
		$pdf->useTemplate($ATULogo,270,4,20);

		$pdf->setXY(10,5);
		$pdf->SetFont('Arial','',16);
		$pdf->cell(270,6,"Filialzuordnung International",0,0,'C',0);

		$pdf->setXY(10,10);
		$pdf->SetFont('Arial','',5);
		$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

		$pdf->setXY(10,15);
		$pdf->SetFont('Arial','',14);
		$pdf->cell(270,6,"Verkaufsleiter International: " . $IntVertriebsLeiter,0,0,'C',0);


		$pdf->setXY(270,12);
		$pdf->SetFont('Arial','',6);
		$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);

		$pdf->setXY(10,10);
		$pdf->SetFont('Arial','',5);
		$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

		$pdf->setXY(270,12);
		$pdf->SetFont('Arial','',6);
		$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);

		$Spalte=0;
		$LaenderBloecke=0;
	}
	else
	{
		$LaenderBloecke++;
	}

		//******************************************
		// Regionalzentrum
		//******************************************
	$StartZeile = 25;

	$SQL="SELECT MIT_BEZEICHNUNG, count(FIF_FIL_ID) ANZAHL FROM Regionalzentren INNER JOIN Mitarbeiter ON REZ_ID = MIT_REZ_ID INNER JOIN FILIALINFOS ON REZ_ID = FIF_WERT";
	$SQL.=" WHERE MIT_MTA_ID IN (1,110) AND FIF_FIT_ID=26 AND REZ_LAN_CODE<>'DE' AND MIT_BEZEICHNUNG='".$rsMEI['MIT_BEZEICHNUNG'][$i]."' GROUP BY MIT_BEZEICHNUNG";
	
	$rsRZAnzahl = awisOpenRecordset($con, $SQL );
	$rsRZAnzahlZeilen = $awisRSZeilen;

	$pdf->setXY(10+($LaenderBloecke*90)+$LaenderBloecke,$StartZeile);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,$rsMEI['REZ_BEZEICHNUNG'][$i] . ', '. $rsMEI['MTA_BEZEICHNUNG'][$i] . ': ' . $rsMEI['MIT_BEZEICHNUNG'][$i] .' ('.(isset($rsRZAnzahl['ANZAHL'][0])?$rsRZAnzahl['ANZAHL'][0]:'').')',0,0,'L',0);
	$RegZentrumID = $rsMEI['REZ_ID'][$i];

		//******************************************
		// Trainer Technik
		//******************************************
	$rsPers = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_MTA_ID=50 AND MIT_STATUS='A' ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	$pdf->setXY(10+($LaenderBloecke*90)+$LaenderBloecke,$StartZeile+5);
	$pdf->SetFont('Arial','',8);
	$pdf->cell(270,6,"Trainer Technik: " . substr($PersName,2) ,0,0,'L',0);

		//******************************************
		// Wartungstechniker
		//******************************************
	$rsPers = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_MTA_ID=40 AND MIT_STATUS='A' ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	unset($rsPers);
	$pdf->setXY(10+($LaenderBloecke*90)+$LaenderBloecke,$StartZeile+8);
	$pdf->SetFont('Arial','',8);
	$pdf->cell(270,6,"Wartungstechnik: " . substr($PersName,2) ,0,0,'L',0);

		//******************************************
		// Trainer
		//******************************************
	$rsPers = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_MTA_ID=200 AND MIT_STATUS='A' ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	unset($rsPers);
	$pdf->setXY(10+($LaenderBloecke*90)+$LaenderBloecke,$StartZeile+11);
	$pdf->SetFont('Arial','',8);
	$pdf->cell(270,6,"Trainer/PE: " . substr($PersName,2) ,0,0,'L',0);

		//******************************************
		// Administration Manager
		//******************************************
	$rsPers = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_MTA_ID=120 AND MIT_STATUS='A' ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	unset($rsPers);
	$pdf->setXY(10+($LaenderBloecke*90)+$LaenderBloecke,$StartZeile+14);
	$pdf->SetFont('Arial','',8);
	$pdf->cell(270,6,"Administration Manager: " . substr($PersName,2) ,0,0,'L',0);

		//******************************************
		// Projektleiter
		//******************************************
	$rsPers = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_MTA_ID=220 AND MIT_STATUS='A' ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	unset($rsPers);
	$pdf->setXY(10+($LaenderBloecke*90)+$LaenderBloecke,$StartZeile+17);
	$pdf->SetFont('Arial','',8);
	$pdf->cell(270,6,"Projektleiter: " . substr($PersName,2) ,0,0,'L',0);


		//******************************************
		// Alle GBLs suchen
		//******************************************
	$rsGBL = awisOpenRecordset($con, "SELECT * FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY WHERE MIT_REZ_ID='" . $rsMEI['MIT_REZ_ID'][$i] . "' ORDER BY MIT_BEZEICHNUNG");
	$rsGBLZeilen = $awisRSZeilen;

	if($LaenderBloecke==0)
	{
		$Spalte = 0;		// Neue Datenspalte
	}
	else
	{
		$Spalte = $LaenderBloecke*2;
	}

	$RasterZeile = $StartZeile+23;
	$pdf->SetFillColor(240,240,240);
	$MaxFilZeilen=0;
	$BlockZeile = 0;
	$UberschriftenHoehe1 = 0;
	$UberschriftenHoehe2 = 0;

	//*************************************************************
	// Spezialfall: Kein GBL in einem Regionalzentrum
	//*************************************************************
	if($rsGBLZeilen==0)
	{
		$rsFIL = awisOpenRecordset($con, 'SELECT * FROM Filialen INNER JOIN FilialInfos ON FIL_ID = FIF_FIL_ID WHERE FIF_FIT_ID=26 AND FIF_WERT=' . $RegZentrumID . ' ORDER BY FIL_ID');
		$rsFILZeilen = $awisRSZeilen;

		$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile);
		$pdf->SetFont('Arial','',8);
		$pdf->cell(45,5,' (' . $rsFILZeilen . ')',1,0,'C',1);
		$Mitarbeiter = '';

		//***************************
		// TKDL anzeigen
		//***************************
		if(isset($rsGBL['VKG_ID'][$j]))
		{
			$GBL=$rsGBL['VKG_ID'][$j];
		}
		if (isset($rsMEI['MIT_REZ_ID'][$i]))
		{
			$RGZ=$rsMEI['MIT_REZ_ID'][$i];	
		}

		//$SQL = "SELECT * FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_TKDL_MIT_KEY = MIT_KEY WHERE VKG_ID='" . $rsGBL['VKG_ID'][$j] . "' AND MIT_MTA_ID=30 AND MIT_REZ_ID='" . $rsMEI['MIT_REZ_ID'][$i] . "' ORDER BY MIT_BEZEICHNUNG";
		$SQL = "SELECT * FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_TKDL_MIT_KEY = MIT_KEY WHERE VKG_ID='" . $GBL . "' AND MIT_MTA_ID=30 AND MIT_REZ_ID='" . $RGZ . "' ORDER BY MIT_BEZEICHNUNG";
		$rsTKDL = awisOpenRecordset($con, $SQL);

		$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile+5+($DoppeltHoheUeberschriftZellen*3));
		$pdf->SetFont('Arial','',7);
		if(isset($rsTKDL['VKG_TKDLBEMERKUNG'][0]) && $rsTKDL['VKG_TKDLBEMERKUNG'][0]!='')
		{
			$pdf->MultiCell(45,4,'TKDL: '.$rsTKDL['MIT_BEZEICHNUNG'][0] . "\n" . $rsTKDL['VKG_TKDLBEMERKUNG'][0],1,'C',1);
			$DoppeltHoheUeberschriftZellen++;
			$UberschriftenHoehe2=3;
			$MitarbeiterTKDL = 'TKDL: '.$rsTKDL['MIT_BEZEICHNUNG'][0] . "\n" . $rsTKDL['VKG_TKDLBEMERKUNG'][0];
		}
		else
		{
			if(isset($rsTKDL['MIT_BEZEICHNUNG'][0]	) && $rsTKDL['MIT_BEZEICHNUNG'][0]=='')
			{
				$pdf->cell(45,5,'TKDL: unbekannt',1,0,'C',1);
				$MitarbeiterTKDL = 'TKDL: unbekannt';
			}
			else
			{
				$pdf->cell(45,5,'TKDL: '.(isset($rsTKDL['MIT_BEZEICHNUNG'][0])?$rsTKDL['MIT_BEZEICHNUNG'][0]:''),1,0,'C',1);
				$MitarbeiterTKDL = 'TKDL: '.(isset($rsTKDL['MIT_BEZEICHNUNG'][0])?$rsTKDL['MIT_BEZEICHNUNG'][0]:'');
			}
		}

		$DoppeltHoheZellen=0;
		$YZeile = 0;

		//************************************
		// Filialen anzeigen
		//************************************

		for($k=0;$k<$rsFILZeilen;$k++)
		{
			$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,10+$RasterZeile+(($YZeile+$DoppeltHoheZellen)*4)+($DoppeltHoheUeberschriftZellen*3));
			if($rsFIL['FIL_GRUPPE'][$k]=='')
			{
				$pdf->SetFont('Arial','B',8);
			}
			else
			{
				$pdf->SetFont('Arial','',8);
			}
			for($l=0;$l<$rsFILBemZeilen;$l++)		// Sind f�r die Filiale SonderTKDL Infos da?
			{
				if($rsFILBem['FIF_FIL_ID'][$l]==$rsFIL['FIL_ID'][$k] AND $rsFILBem['FIF_WERT'][$l]!='')
				{
					$pdf->MultiCell(45,4,substr('000'.$rsFIL['FIL_ID'][$k],-3) . ' ' . $rsFIL['FIL_BEZ'][$k] . "\nTKDL: " . $rsFILBem['FIF_WERT'][$l] . '',1,'J',0);
					$DoppeltHoheZellen++;
					$rsFILBemZeilen=9999;
					break;
				}
			}
			if($l>=$rsFILBemZeilen)	// Keine TKDL Zusatz Info-> einzeilig
			{
				$pdf->cell(45,4,substr('000'.$rsFIL['FIL_ID'][$k],-3) . ' ' . $rsFIL['FIL_BEZ'][$k],1,0,'L',0);
			}

			if($YZeile>16)		// Spalte zu gross -> umbrechen
			{
				$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile);
				$pdf->SetFont('Arial','',8);
				$pdf->cell(90,5,$Mitarbeiter,1,0,'C',1);
				$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile+5+($DoppeltHoheUeberschriftZellen*3));
				$pdf->SetFont('Arial','',7);
				$pdf->cell(90,5,$MitarbeiterTKDL,1,0,'C',1);

				$YZeile=0;
				$Spalte++;
				$DoppeltHoheZellen=0;
			}
			else
			{
				$YZeile++;
			}

		}

	}

	// Normalfall: GBLs pro Regionalzentrum durchlaufen und Filialen anzeigen
	for($j=0;$j<$rsGBLZeilen;$j++)
	{
		if($Spalte > 5)
		{
			$Spalte = 0;
			$RasterZeile = $RasterZeile+($MaxFilZeilen*4)+13+$UberschriftenHoehe1+$UberschriftenHoehe2;
			$MaxFilZeilen=0;
			if(++$BlockZeile>1)
			{
				$pdf->addpage();
				$pdf->SetAutoPageBreak(true,0);
				$UberschriftenHoehe1=0;
				$UberschriftenHoehe2=0;
				$Spalte = 0;		// Neue Datenspalte
				$RasterZeile = 33;
				$pdf->SetFillColor(240,240,240);
				$MaxFilZeilen=0;
				$BlockZeile = 0;

				$pdf->setXY(10,5);
				$pdf->SetFont('Arial','',16);
				$pdf->cell(270,6,"Filialzuordnung",0,0,'C',0);

				$pdf->setXY(10,10);
				$pdf->SetFont('Arial','',5);
				$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

				$pdf->setXY(270,10);
				$pdf->SetFont('Arial','',6);
				$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'R',0);

				$pdf->setXY(10,15);
				$pdf->SetFont('Arial','',10);
				$pdf->cell(270,6,"Regionalzentrum " . $rsMEI['REZ_BEZEICHNUNG'][$i] . ', VKL: ' . $rsMEI['MIT_BEZEICHNUNG'][$i] ,0,0,'L',0);

				$pdf->setXY(10,20);
				$pdf->SetFont('Arial','',8);
				$pdf->cell(270,6,"Seite " . ++$Seite ,0,0,'L',0);
			}
		}

		//***************************
		// GBL anzeigen
		//***************************

		$rsFIL = awisOpenRecordset($con, 'SELECT * FROM Filialen INNER JOIN FilialInfos ON FIL_ID = FIF_FIL_ID WHERE FIF_FIT_ID=21 AND FIF_WERT=0' . $rsGBL['VKG_ID'][$j] . ' ORDER BY FIL_ID');
		$rsFILZeilen = $awisRSZeilen;

		$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile);
		$pdf->SetFont('Arial','',8);

		$DoppeltHoheUeberschriftZellen=0;
		if($rsGBL['VKG_MITBEMERKUNG'][$j]!='')
		{
			$pdf->MultiCell(45,4,$rsGBL['MIT_BEZEICHNUNG'][$j] . ' (' . $rsFILZeilen . ") \n" . $rsGBL['VKG_MITBEMERKUNG'][$j],1,'C',1);
			$DoppeltHoheUeberschriftZellen++;
			$UberschriftenHoehe1=3;
			$Mitarbeiter =  $rsGBL['MIT_BEZEICHNUNG'][$j] . ' (' . $rsFILZeilen . ") \n" . $rsGBL['VKG_MITBEMERKUNG'][$j];
		}
		else
		{
			$pdf->cell(45,5,$rsGBL['MIT_BEZEICHNUNG'][$j] . ' (' . $rsFILZeilen . ')',1,0,'C',1);
			$Mitarbeiter = $rsGBL['MIT_BEZEICHNUNG'][$j];
		}
		//***************************
		// TKDL anzeigen
		//***************************
		$SQL = "SELECT * FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_TKDL_MIT_KEY = MIT_KEY WHERE VKG_ID='" . $rsGBL['VKG_ID'][$j] . "' AND MIT_MTA_ID=30 AND MIT_REZ_ID='" . $rsMEI['MIT_REZ_ID'][$i] . "' ORDER BY MIT_BEZEICHNUNG";
		$rsTKDL = awisOpenRecordset($con, $SQL);

		$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile+5+($DoppeltHoheUeberschriftZellen*3));
		$pdf->SetFont('Arial','',7);
		if(isset($rsTKDL['VKG_TKDLBEMERKUNG'][0]) && $rsTKDL['VKG_TKDLBEMERKUNG'][0]!='')
		{
			$pdf->MultiCell(45,4,'TKDL: '.$rsTKDL['MIT_BEZEICHNUNG'][0] . "\n" . $rsTKDL['VKG_TKDLBEMERKUNG'][0],1,'C',1);
			$DoppeltHoheUeberschriftZellen++;
			$UberschriftenHoehe2=3;
			$MitarbeiterTKDL = 'TKDL: '.$rsTKDL['MIT_BEZEICHNUNG'][0] . "\n" . $rsTKDL['VKG_TKDLBEMERKUNG'][0];
		}
		else
		{
			$pdf->cell(45,5,'TKDL: '.(isset($rsTKDL['MIT_BEZEICHNUNG'][0])?$rsTKDL['MIT_BEZEICHNUNG'][0]:''),1,0,'C',1);
			$MitarbeiterTKDL = 'TKDL: '.(isset($rsTKDL['MIT_BEZEICHNUNG'][0])?$rsTKDL['MIT_BEZEICHNUNG'][0]:'');
		}

		$DoppeltHoheZellen=0;
		$YZeile = 0;
		for($k=0;$k<$rsFILZeilen;$k++)
		{
			$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,10+$RasterZeile+(($YZeile+$DoppeltHoheZellen)*4)+($DoppeltHoheUeberschriftZellen*3));
			if($rsFIL['FIL_GRUPPE'][$k]=='')
			{
				$pdf->SetFont('Arial','B',8);
			}
			else
			{
				$pdf->SetFont('Arial','',8);
			}
			for($l=0;$l<$rsFILBemZeilen;$l++)		// Sind f�r die Filiale SonderTKDL Infos da?
			{
				if($rsFILBem['FIF_FIL_ID'][$l]==$rsFIL['FIL_ID'][$k] AND $rsFILBem['FIF_WERT'][$l]!='')
				{
					$pdf->MultiCell(45,4,substr('000'.$rsFIL['FIL_ID'][$k],-3) . ' ' . $rsFIL['FIL_BEZ'][$k] . "\nTKDL: " . $rsFILBem['FIF_WERT'][$l] . '',1,'J',0);
					$DoppeltHoheZellen++;
					$rsFILBemZeilen=9999;
					break;
				}
			}
			if($l>=$rsFILBemZeilen)	// Keine TKDL Zusatz Info-> einzeilig
			{
				$pdf->cell(45,4,substr('000'.$rsFIL['FIL_ID'][$k],-3) . ' ' . $rsFIL['FIL_BEZ'][$k],1,0,'L',0);
			}

			if($YZeile>16)		// Spalte zu gross -> umbrechen
			{
				$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile);
				$pdf->SetFont('Arial','',8);
				$pdf->cell(90,5,$Mitarbeiter,1,0,'C',1);
				$pdf->setXY(10+($Spalte*45)+$LaenderBloecke,$RasterZeile+5+($DoppeltHoheUeberschriftZellen*3));
				$pdf->SetFont('Arial','',7);
				$pdf->cell(90,5,$MitarbeiterTKDL,1,0,'C',1);

				$YZeile=0;
				$Spalte++;
				$DoppeltHoheZellen=0;
			}
			else
			{
				$YZeile++;
			}

		}
		$MaxFilZeilen = ($MaxFilZeilen<$rsFILZeilen?$rsFILZeilen:$MaxFilZeilen);
		$Spalte++;
	}
	//Ge�ndert von CA am 22.12.06 => Eine Seite pro Land
	$LaenderBloecke=4;
}		// Ende for

?>