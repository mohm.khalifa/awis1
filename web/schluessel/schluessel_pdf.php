<html>
<head>
	<title>Awis - ATU webbasierendes Informationssystem</title>
</head>

<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once('fpdf.php');
require_once('fpdi.php');
define('FPDF_FONTPATH','/daten/include/font/');
//ob_clean();

global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

clearstatcache();

include ("ATU_Header.php");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1504);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Schluessel-PDF',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$Params=explode(';',awis_BenutzerParameter($con, "Schluessel_PDF", $AWISBenutzer->BenutzerName()));

if($Params[0]!='')
{
	$Filid=$Params[0];
	$Filname=$Params[1];
}
else
{
	die("<span class=HinweisText>Keine Filiale ausgew&auml;hlt!</span>");
}

echo "<h1>PDF - Erstellung f�r Filiale ".$Filid."-".$Filname."</h1>";
echo "<hr>";

$Zeile=0;

if(isset($_POST['cmdProtokollAnzeigen_x']) || isset($_POST['cmdProtokollPDF_x']))
{
	/***************************************
	* Neue PDF Datei erstellen
	***************************************/
	//ob_clean();
	
	//define('FPDF_FONTPATH','font/');
	$pdf = new fpdi('l','mm','a4');
	$pdf->open();
	//$pdf->setSourceFile("../bilder/atulogo_grau.pdf");	
	//$ATULogo = $pdf->ImportPage(1);					
	$pdf->setSourceFile('/daten/web/dokumente/vorlagen/BriefpapierATU_DE_Seite_2_quer_grau.pdf');	
	$ATULogo = $pdf->ImportPage(1);	
	
	
	/***************************************
	*
	* Daten laden
	*
	***************************************/
	
	//$Belehrung="1. Der Schl�ssel des Geldtresors (siehe OA 06/2001 - Rev.) darf nur im Urlaubs-/Krankheitsfalle usw. mit �bergabeprotokoll an einen Dritten weitergegeben werden.\n";
	//$Belehrung.="2. Mit �bernahme der o. a. Schl�ssel sind alle evtl. sich noch in der Filiale befindlichen �berz�hligen Schl�ssel f�r die Tresore (siehe OA 06/2001 - Rev.) nach Weiden zu senden.\n";
	//$Belehrung.="3. Ab sofort ist das Nachmachen von Tresorschl�sseln vor Ort strengstens untersagt. Sollte ein Schl�ssel verloren gehen, ist dies unverz�glich an oben aufgef�hrten Ansprechpartner zu melden.";
	
	$Belehrung="Mit der Unterschrift auf diesem Schl�sselprotokoll best�tige ich die Organisationsanweisung Schl�ssel- und Tresorverwaltung sowie Sicherheitsleitzentrale gelesen zu haben!\n";
	$Belehrung.="Protokoll muss so schnell wie m�glich vollst�ndig unterschrieben zur�ckgefaxt werden!\n";	
	$Belehrung.="Max. Schl�sselanzahl wie in Organisationsanweisung - Restschl�ssel m�ssen per Einschreiben unaufgefordert zur�ck geschickt werden!\n";
	$Belehrung.="Protokoll nur bei dauerhafter �nderung anfordern - nicht bei Urlaub oder Krankheitsfall!\n";
	//$Belehrung.="Das selbstst�ndige Nachmachen aller Schl�ssel ist verboten - bei Bedarf von DV-Organisation anfordern!\n";
	$Belehrung.="Das selbstst�ndige Nachmachen aller Schl�ssel ist verboten � bei Bedarf den Ansprechpartner (siehe Fu�zeile) kontaktieren.\n";
	$Belehrung.="Der Verlust eines Schl�ssels muss sofort an unten aufgef�hrten Ansprechpartner gemeldet werden!";
	
	/*$SQL= "SELECT schluessellieferanten.sll_bezeichnung as SLL_BEZEICHNUNG, ";
	$SQL.= "schluesselgruppen.slg_bezeichnung, schluesselnummern.sln_schluesselnummer, ";
	$SQL.= "schluesselnummern.sln_sicherkartennummer, personal.per_nr, ";
	$SQL.= "personaltaetigkeiten.pet_taetigkeit, personal.per_nachname, personal.per_vorname, ";
	$SQL.= "schluesselvergaben.slv_per_nr_vertreter, p2.per_nachname per_nachname_ver, p2.per_vorname per_vorname_ver, ";
	$SQL.= "schluesselstatus.sls_bezeichnung, schluesseluntergruppen.slu_bezeichnung, pt2.pet_taetigkeit pet_taetigkeit_ver ";
	$SQL.= "FROM schluesselnummern, schluessellieferanten, ";
	$SQL.= "schluesselgruppen, schluesselvergaben, personal, personaltaetigkeiten, personal p2, ";
	$SQL.= "schluesselstatus, schluesseluntergruppen, personaltaetigkeiten pt2 ";
	$SQL.= "WHERE ((schluesselnummern.sln_slg_key = schluesselgruppen.slg_key(+)) ";
	$SQL.= "AND (schluesselnummern.sln_sll_key = schluessellieferanten.sll_key(+)) ";
	$SQL.= "AND (schluesselnummern.sln_slu_key = schluesseluntergruppen.slu_key(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_sln_key = schluesselnummern.sln_key(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_sls_key = schluesselstatus.sls_key(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_per_nr = personal.per_nr(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_per_nr_vertreter = p2.per_nr(+)) ";
	$SQL.= "AND (personal.per_pet_id = personaltaetigkeiten.pet_id(+)) ";
	$SQL.= "AND (p2.per_pet_id = pt2.pet_id(+)) ";
	$SQL.= "AND (schluesselnummern.sln_fil_id=".$Filid.") AND (schluesselstatus.sls_bezeichnung in ('Filiale','Feuerwehr'))) ";
	$SQL.= "ORDER BY schluesselgruppen.slg_bezeichnung, schluesselnummern.sln_schluessellfdnr ";*/
	
	$SQL= "SELECT schluessellieferanten.sll_bezeichnung as SLL_BEZEICHNUNG, schluesselgruppen.slg_bezeichnung, ";
	$SQL.= "schluesselnummern.sln_schluesselnummer, schluesselnummern.sln_sicherkartennummer, v_personal_komplett_map.per_nr, ";
	$SQL.= "v_personal_komplett_map.per_taetigkeit pet_taetigkeit, v_personal_komplett_map.per_nachname, v_personal_komplett_map.per_vorname, ";
	$SQL.= "schluesselvergaben.slv_per_nr_vertreter, p2.per_nachname per_nachname_ver, p2.per_vorname per_vorname_ver, ";
	$SQL.= "schluesselstatus.sls_bezeichnung, schluesseluntergruppen.slu_bezeichnung, v_personal_komplett_map.per_taetigkeit pet_taetigkeit_ver ";
	$SQL.= "FROM schluesselnummern, schluessellieferanten, schluesselgruppen, schluesselvergaben, ";
	$SQL.= "v_personal_komplett_map, v_personal_komplett_map p2, schluesselstatus, schluesseluntergruppen ";
	$SQL.= "WHERE ((schluesselnummern.sln_slg_key = schluesselgruppen.slg_key(+)) ";
	$SQL.= "AND (schluesselnummern.sln_sll_key = schluessellieferanten.sll_key(+)) ";
	$SQL.= "AND (schluesselnummern.sln_slu_key = schluesseluntergruppen.slu_key(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_sln_key = schluesselnummern.sln_key(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_sls_key = schluesselstatus.sls_key(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_per_nr = v_personal_komplett_map.per_nr(+)) ";
	$SQL.= "AND (schluesselvergaben.slv_per_nr_vertreter = p2.per_nr(+)) "; 
	$SQL.= "AND (schluesselnummern.sln_fil_id=".$Filid.") AND (schluesselstatus.sls_bezeichnung in ('Filiale','Feuerwehr','NVS-Dienst','Extra'))) ";
	$SQL.= "ORDER BY schluesselgruppen.slg_bezeichnung, schluesselnummern.sln_schluessellfdnr ";
	
	//awis_Debug(1,$SQL);
	
	$rsSchluesselPDF = awisOpenRecordset($con,$SQL);
	$rsSchluesselPDFZeilen = $awisRSZeilen;
	
	if($rsSchluesselPDFZeilen==0)
	{	
		$pdf->addpage();
		$pdf->SetAutoPageBreak(true,0);
		//->_pdf->useTemplate($VorlagenID,0,0);		
		$pdf->useTemplate($ATULogo,0,0);
		//$pdf->useTemplate($ATULogo,270,4,20);
		$pdf->SetFont('Arial','',10);
		$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
	}
	else 		// Daten gefunden
	{		
		NeueSeite($pdf,$Zeile,$ATULogo,15,$Filid." - ".$Filname,true);
		//NeueSeite($pdf,$Zeile,$ATULogo,15,'',true);
		
		$LinkerRand=15;
		
		$Y_Wert=50;
		
		for($i=0;$i<$rsSchluesselPDFZeilen;$i++)
		{	
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(20,6,$rsSchluesselPDF['SLL_BEZEICHNUNG'][$i],1,0,'L',0);
			//$pdf->cell(20,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+20,$Y_Wert);
			$pdf->cell(35,6,($rsSchluesselPDF['SLU_BEZEICHNUNG'][$i]==''?$rsSchluesselPDF['SLG_BEZEICHNUNG'][$i]:$rsSchluesselPDF['SLG_BEZEICHNUNG'][$i].' => '.$rsSchluesselPDF['SLU_BEZEICHNUNG'][$i]),1,0,'L',0);
			//$pdf->cell(35,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+55,$Y_Wert);
			$pdf->cell(30,6,$rsSchluesselPDF['SLN_SCHLUESSELNUMMER'][$i],1,0,'L',0);
			//$pdf->cell(30,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+85,$Y_Wert);
			$pdf->cell(25,6,$rsSchluesselPDF['SLN_SICHERKARTENNUMMER'][$i],1,0,'L',0);
			//$pdf->cell(25,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+110,$Y_Wert);
			$pdf->cell(20,6,$rsSchluesselPDF['SLS_BEZEICHNUNG'][$i],1,0,'L',0);
			//$pdf->cell(20,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+130,$Y_Wert);
			$pdf->cell(20,6,$rsSchluesselPDF['PER_NR'][$i],1,0,'L',0);
			//$pdf->cell(20,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+150,$Y_Wert);
			$pdf->cell(40,6,$rsSchluesselPDF['PET_TAETIGKEIT'][$i],1,0,'L',0);
			//$pdf->cell(40,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+190,$Y_Wert);
			$pdf->cell(45,6,$rsSchluesselPDF['PER_NACHNAME'][$i] . " " . $rsSchluesselPDF['PER_VORNAME'][$i],1,0,'L',0);
			//$pdf->cell(45,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+235,$Y_Wert);
			$pdf->cell(35,6,"",1,0,'L',0);
			
			$Y_Wert=$Y_Wert+6;
			
			if($rsSchluesselPDF['SLV_PER_NR_VERTRETER'][$i]!='')
			{
				$pdf->setXY($LinkerRand,$Y_Wert);
				$pdf->cell(20,6,'',1,0,'L',0);
				
				$pdf->setXY($LinkerRand+20,$Y_Wert);
				$pdf->cell(35,6,'',1,0,'L',0);
				
				$pdf->setXY($LinkerRand+55,$Y_Wert);
				$pdf->cell(30,6,'',1,0,'L',0);
				
				$pdf->setXY($LinkerRand+85,$Y_Wert);
				$pdf->cell(25,6,'',1,0,'L',0);
				
				$pdf->setXY($LinkerRand+110,$Y_Wert);
				$pdf->cell(20,6,'',1,0,'L',0);
				
				$pdf->setXY($LinkerRand+130,$Y_Wert);
				$pdf->cell(20,6,$rsSchluesselPDF['SLV_PER_NR_VERTRETER'][$i],1,0,'L',0);
				
				$pdf->setXY($LinkerRand+150,$Y_Wert);
				$pdf->cell(40,6,$rsSchluesselPDF['PET_TAETIGKEIT_VER'][$i],1,0,'L',0);
				
				$pdf->setXY($LinkerRand+190,$Y_Wert);
				$pdf->cell(45,6,$rsSchluesselPDF['PER_NACHNAME_VER'][$i] . " " . $rsSchluesselPDF['PER_VORNAME_VER'][$i],1,0,'L',0);
				
				$pdf->setXY($LinkerRand+235,$Y_Wert);
				$pdf->cell(35,6,"",1,0,'L',0);
				
				$Y_Wert=$Y_Wert+6;
			}
			
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(20,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+20,$Y_Wert);
			$pdf->cell(35,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+55,$Y_Wert);
			$pdf->cell(30,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+85,$Y_Wert);
			$pdf->cell(25,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+110,$Y_Wert);
			$pdf->cell(20,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+130,$Y_Wert);
			$pdf->cell(20,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+150,$Y_Wert);
			$pdf->cell(40,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+190,$Y_Wert);
			$pdf->cell(45,6,'',1,0,'L',0);
			
			$pdf->setXY($LinkerRand+235,$Y_Wert);
			$pdf->cell(35,6,"",1,0,'L',0);
			
			
			$Y_Wert=$Y_Wert+6;
		
			if(($Y_Wert+5)>190)
			{
				if(($i+1)<$rsSchluesselPDFZeilen)
				{
					NeueSeite($pdf,$Zeile,$ATULogo,15,$Filid." - ".$Filname,true);
					$Y_Wert=50;		
				}
				else
				{ 
					NeueSeite($pdf,$Zeile,$ATULogo,15,$Filid." - ".$Filname,false);
					$Y_Wert=30;		
				}
			} 	
		}
		
		$pdf->SetFont('Arial','',10);
		
		$Y_Wert=$Y_Wert+6;
	
		$Hoehe=1;
		$bla=$pdf->getStringWidth($Belehrung);
		$HoeheBeleherung=round($bla/58+0.5,0);
		$HoeheBeleherung=($HoeheBeleherung==0?1:$HoeheBeleherung);
		$Hoehe=($Hoehe<$HoeheBeleherung)?$HoeheBeleherung:$Hoehe;
		
		//if(($Y_Wert+$Hoehe+(30))>190)
		if(($Y_Wert+$Hoehe+(35))>190)
		{
			NeueSeite($pdf,$Zeile,$ATULogo,15,$Filid." - ".$Filname,false);
			$Y_Wert=50;		
		}
	
		$pdf->SetFont('Arial','B',10);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,"Hinweis",1,0,'L',0);
		
		$Y_Wert=$Y_Wert+6;
		
		//$pdf->Line($LinkerRand,$Y_Wert,$LinkerRand+270,$Y_Wert);
		//$pdf->setXY($LinkerRand,$Y_Wert);
		//$pdf->SetFont('Arial','',8);
		//$pdf->MultiCell(270,4,$Belehrung1,0,'L',0);
		
		//$Y_Wert=$Y_Wert+4;
		
		$pdf->SetFont('Arial','',8);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->MultiCell(270,4,$Belehrung,1,'L',0);
		
		//$pdf->Line($LinkerRand,$Y_Wert+$Hoehe+6,$LinkerRand+270,$Y_Wert+$Hoehe+6);
		
		//$pdf->Line($LinkerRand,$Y_Wert-4,$LinkerRand,$Y_Wert+$Hoehe+6);
		//$pdf->Line($LinkerRand+270,$Y_Wert-4,$LinkerRand+270,$Y_Wert+$Hoehe+6);
		
		$pdf->SetFont('Arial','',10);
		
		$Y_Wert=$Y_Wert+$Hoehe+12;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,"Unterschrift",1,0,'L',0);
		$pdf->setXY($LinkerRand+50,$Y_Wert);
		$pdf->cell(10,6,"FL",0,0,'L',0);
		
		$pdf->SetFont('Arial','',10);
		
		$Y_Wert=$Y_Wert+6;
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(60,12,"",1,0,'L',0);
		
		$Y_Wert=$Y_Wert+24;
		$pdf->setXY($LinkerRand,$Y_Wert);
		//$pdf->cell(270,6,"Protkollausfertigung an Schlie�anlagen/Schl�sselverwaltung       R�ckfragen - Richard Fritsch - Telefon: 5360 - Fax: #111-934 5360",1,0,'C',0);
		$pdf->cell(270,6,"Protokollausfertigung an Security Management       R�ckfragen - Richard Fritsch - Telefon: 5360 - Fax: #111-934 8120",1,0,'C',0);
		
	}	// Ende Datenaufbereitung
	
	/***************************************
	* Abschluss und Datei speichern
	***************************************/
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	//20110424,ho Umbau auf Standard Funktion aus fpdi.php
	//$pdf->saveas($DateiName);
	$pdf->output($DateiName,'F');
	
	if(isset($_POST['cmdProtokollAnzeigen_x']))
	{
		echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
		echo "<hr><a href=./schluessel_pdf.php><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
	}
	
	if(isset($_POST['cmdProtokollPDF_x']))
	{
		$SQL= "Select to_char(fif_wert) as FaxNr from filialinfos where fif_fit_id=2 and fif_imq_id=2 and fif_fil_id=".$Filid;
	
		$rsFilialinfosFAX = awisOpenRecordset($con,$SQL);
		$rsFilialinfosFAXZeilen = $awisRSZeilen;
		
		if($rsFilialinfosFAXZeilen==0)
		{
			awislogoff($con);
			die("<span class=HinweisText>Keine Faxnummer gefunden!</span>");
		}
		else
		{
			$Sonderzeichen=array('/',' ','-');
			$FaxNr=$rsFilialinfosFAX['FAXNR'][0];
			
			for($i=0;$i<count($Sonderzeichen);$i++)
			{
				$FaxNr=str_replace($Sonderzeichen[$i],'',$FaxNr);		
			}
			
			PDF2FAX("/daten/web".$DateiNameLink,$FaxNr);
			echo "<p><hr><a href=./schluessel_pdf.php><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";
		}
	}
}
else
{

echo '<form name=frmPDF method=post action=./schluessel_pdf.php>';

echo "<table width=100% id=DatenTabelle>";
echo "<tr><td>&nbsp;</td></tr>";
if(($RechteStufe&1)==1){
	echo "<tr>";
	echo "<td width=50><input type=image border=0 src=/bilder/sc_lupe.png name=cmdProtokollAnzeigen title='Protokoll anzeigen'></td>";
	echo "<td>Schl&uuml;sselprotokoll anzeigen</td>";
	echo "</tr>";
	echo "<tr><td>&nbsp;</td></tr>";
}
if(($RechteStufe&2)==2){
echo "<tr>";
echo "<td width=50><input type=image border=0 src=/bilder/sc_fax.png name=cmdProtokollPDF title='Protokoll faxen'></td>";
echo "<td>Schl&uuml;sselprotokoll an Filiale faxen</td>";
echo "</tr>";
echo "<tr><td>&nbsp;</td></tr>";
}
echo "<tr>";
echo "<td width=50><a href='./schluessel_Main.php?cmdAktion=Vergaben&txtFIL_ID=" . $Params[0] . "&Anzeigen=TRUE'><img border=0 title'Zur&uuml;ck' src=/bilder/zurueck.png></a></td>";
echo "</tr>";
echo "<tr><td>&nbsp;</td></tr>";
echo '</table>';
	
echo '</form>';


}


/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Sacha Kerres
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
*
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,$Filiale,$Ueberschrift)
{
	static $Seite;
	
	$pdf->addpage();							// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	//$pdf->useTemplate($ATULogo,$LinkerRand+250,4,20);		// Logo einbauen
	$pdf->useTemplate($ATULogo,0,0);		// Logo einbauen	
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,200);					// Cursor setzen
	$pdf->Cell(270,3,'Diese Liste ist nur f�r interne Zwecke bestimmt, und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);
	
	$pdf->setXY($LinkerRand+260,200);					// Cursor setzen
	$pdf->Cell(20,3,'Seite ' . $Seite,0,0,'L',0);
	
	
	
	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','',18);				// Schrift setzen
	
	// Ueberschrift
	$pdf->SetFillColor(255,255,255);
	$pdf->cell(270,6,"Schl�sselprotokoll",0,0,'C',0);
	
	$pdf->setXY($LinkerRand,10);				// Cursor setzen
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	$pdf->cell(270,6,"Stand: " . date('d.m.Y'),0,0,'C',0);

	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=10;
	
	// Spaltenueberschriften
	
	
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Y_Wert=$Y_Wert+12;
	
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(15,6,"Filiale:",0,0,'L',0);
	
	$pdf->setXY($LinkerRand+15,$Y_Wert);
	$pdf->cell(60,6,$Filiale,1,0,'L',0);
	
	$pdf->SetFont('Arial','',8);				// Schrift setzen
	
	$Y_Wert=$Y_Wert+12;
	
	if($Ueberschrift)
	{
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(20,6,"Hersteller",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+20,$Y_Wert);
		$pdf->cell(35,6,"Schl�ssel-Gruppe",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+55,$Y_Wert);
		$pdf->cell(30,6,"Schl�ssel-Nummer",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+85,$Y_Wert);
		$pdf->cell(25,6,"Sicherungs.-Nr",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+110,$Y_Wert);
		$pdf->cell(20,6,"Status",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(20,6,"Personal-Nr",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(40,6,"T�tigkeit",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+190,$Y_Wert);
		$pdf->cell(45,6,"Mitarbeiter",1,0,'C',0);
		
		$pdf->setXY($LinkerRand+235,$Y_Wert);
		$pdf->cell(35,6,"Unterschrift",1,0,'C',0);
	}
	
	$Zeile = 22;
}

/**
*
* Funktion sendet PDF an FAX-Server
*
* @author Stefan Pongratz
* @param  string file
* @param  string fax_nr
* 
*/


function PDF2FAX($file,$fax_nr)
{
	if($fax_nr!='')
	{
		require_once("activemail/activeMailLib.php");
		$att1="$file";//set a valid file path
		//$mailempf="faxg3/".$fax_nr."@svmsgwi003.ads.atu.de";
		//$mailempf="nvs:faxg3/09613069345742@svmsgwi003.ads.atu.de";
		//$mailempf="faxg3/09613069345742@svmsgwi003.ads.atu.de";
		//$mailempf="1600@svmsgwi003.ads.atu.de";
		$mailempf="christian.argauer@de.atu.eu";
		$email = new activeMailLib();
		
		$email->enableAddressValidation();
		$email->enableServerValidation();
		
		//$email->From("awis@atu.de");//set a valid E-mail
		$email->From("entwick@ads.atu.de");//set a valid E-mail
		$email->To($mailempf);//set a valid E-mail
		
		$email->Subject("activeMailLib Text Mail");
		$email->Attachment($att1);
		
		if($email->checkServer('SVXCHWI003.ads.atu.de'))
		{
			if($email->checkAddress($mailempf)) 
			{
				$email->Send();
			}
		}
		if ($email->isSent($mailempf))
		{
			print "E-Mail versendet: ".$mailempf;
		}
		else
		{
			print "E-Mail nicht versendet";
		}
	}
	else
	{
		die("<span class=HinweisText>Keine Faxnummer angegeben!</span>");
	}
}

?>
</body>
</html>
