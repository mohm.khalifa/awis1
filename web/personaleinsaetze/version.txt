#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung fr ein Modul
#
#	Abschnitt
#	[Header]	Infos fr die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Personaleins&auml;tze
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT,CH

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.00.04;Neue Berichte;04.02.2019;Patrick Gebhardt
0.00.03;Fehler beim Bl�ttern in Bereichen beseitigt.;11.12.2012;Sacha Kerres
0.00.02;Anpassung der Abfragen wg. Abterminierung der Mitarbeiter;04.06.2009;Sacha Kerres
0.00.01;Erste Version;22.07.2008;Sacha Kerres

