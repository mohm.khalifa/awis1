<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RPP','%');
	$TextKonserven[]=array('RRP','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

$Form->DebugAusgabe(1,$_POST);
$Form->DebugAusgabe(1,$_GET);

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15000 = $AWISBenutzer->HatDasRecht(15000);
	if(($Recht15000)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$RPPParam = unserialize($AWISBenutzer->ParameterLesen('Formular_RRP_Preise'));
	
	if(!isset($RPPParam['BLOCK']))
	{
		$RPPParam['BLOCK']=1;
		$RPPParam['KEY']='';
		$RPPParam['RRP_KEY']=$AWIS_KEY1;
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY RPP_LAN_CODE, RPP_GUELTIGAB DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}
	
	$SQL = 'SELECT';
	$SQL .= ' RPP_KEY,';
	$SQL .= ' RPP_RRP_KEY,';
	$SQL .= ' RPP_LAN_CODE,';
	$SQL .= ' COALESCE (LAN_LAND,\'unbekannt\') AS LAN_LAND,';
	$SQL .= ' RPP_VK,';
	$SQL .= ' RPP_GUELTIGAB,';
	$SQL .= ' RPP_USER,';
	$SQL .= ' RPP_USERDAT,';
	$SQL .= ' COALESCE (RRP_AST_KEY,0) AS RRP_AST_KEY,';
	$SQL .= ' row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM REIFENPFLEGEPREISE ';
	$SQL .= ' LEFT JOIN LAENDER ON LAN_CODE = RPP_LAN_CODE';
	$SQL .= ' LEFT JOIN REIFENPFLEGE ON RRP_KEY=RPP_RRP_KEY';
	$SQL .= ' WHERE RPP_RRP_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['RPP_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['RPP_KEY']);
$Form->DebugAusgabe(1,$AWIS_KEY2);
	}
	elseif($AWIS_KEY2=='')
	{
		if($RPPParam['RRP_KEY']==$AWIS_KEY1 and !isset($_GET["RPPListe"]))
		{
			$AWIS_KEY2 = $RPPParam['KEY'];
		}
		else
		{
			$RSTParam['BLOCK']=1;
			$RSTParam['KEY']='';
			$AWIS_KEY2=0;
		}
	}
	
	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND RPP_KEY = '.$AWIS_KEY2;
		$_GET['RPP_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}
	
	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$Form->DebugAusgabe(1,$SQL);
	$rsRPP = $DB->RecordsetOeffnen($SQL);

    $Form->Formular_Start();

	if($rsRPP->AnzahlDatensaetze()>1 OR isset($_GET['RPPListe']) OR !isset($_GET['RPP_KEY']))
	{
		$Form->ZeileStart();

		$gelistet = true;
		if ($rsRPP->FeldInhalt('RRP_AST_KEY')==0)
		{
			$gelistet = false;
			$Icons=array();
			if((intval($Recht15000)&6)!=0)
			{
				$Icons[] = array('new','./reifenpflege_Main.php?cmdAktion=Details&Seite=Preise&RRP_KEY='.$AWIS_KEY1.'&RPP_KEY=-1');
	  		}
	
	  		$Form->Erstelle_ListeIcons($Icons,38,-1);
		}

  		$Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=RPP_LAN_CODE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RPP_LAN_CODE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RPP']['RPP_LAN_CODE'],100,'',$Link);
		
  		$Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=RPP_VK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RPP_VK'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RPP']['RPP_VK'],100,'',$Link,'','R50');
		
		$Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=RPP_GUELTIGAB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RPP_GUELTIGAB'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RPP']['RPP_GUELTIGAB'],100,'',$Link);
		
		$Form->ZeileEnde();

		// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$RPPParam['RRP_KEY']=$AWIS_KEY1;
		$RPPParam['KEY']='';
		
		$DS=0;
		while(!$rsRPP->EOF())
		{
			$Form->ZeileStart();

			if (!$gelistet)
			{
				$Icons = array();
				if(intval($Recht15000&2)>0)	// Ändernrecht
				{
					$Icons[] = array('edit','./reifenpflege_Main.php?cmdAktion=Details&Seite=Preise&RRP_KEY='.$AWIS_KEY1.'&RPP_KEY='.$rsRPP->FeldInhalt('RPP_KEY'));
				}
				if(intval($Recht15000&4)>0)	// Ändernrecht
				{
					$Icons[] = array('delete','./reifenpflege_Main.php?cmdAktion=Details&Seite=Preise&RRP_KEY='.$AWIS_KEY1.'&Del='.$rsRPP->FeldInhalt('RPP_KEY'));
				}
				$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			}
			
			$Form->Erstelle_ListenFeld('*RPP_LAN_CODE',$rsRPP->FeldInhalt('LAN_LAND'),50,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*RPP_VK',$rsRPP->FeldInhalt('RPP_VK'),20,100,false,($DS%2),'','','N2','R50');
			$Form->Erstelle_ListenFeld('*RPP_GUELTIGAB',$rsRPP->FeldInhalt('RPP_GUELTIGAB'),20,100,false,($DS%2),'','','D');
			$Form->ZeileEnde();

			$rsRPP->DSWeiter();
			$DS++;
		}

		$Link = './reifenpflege_Main.php?cmdAktion=Details&Seite=Preise';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsRPP->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht15000&6);
		$AWIS_KEY2 = $rsRPP->FeldInhalt('RPP_KEY');
		$RPPParam['KEY']=$AWIS_KEY2;
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		$Form->Erstelle_HiddenFeld('RPP_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('RPP_RRP_KEY',$AWIS_KEY1);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./reifenpflege_Main.php?cmdAktion=Details&Seite=Preise&RPPListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRPP->FeldInhalt('RPP_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRPP->FeldInhalt('RPP_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RPP']['RPP_LAN_CODE'].':',100);
		$SQL = ' SELECT LAN_CODE, LAN_LAND FROM LAENDER WHERE LAN_WWSKENN IS NOT NULL ORDER BY 1';
		$Form->Erstelle_SelectFeld('!RPP_LAN_CODE',$rsRPP->FeldInhalt('RPP_LAN_CODE'),"100:120",($EditModus and $AWIS_KEY2==''),$SQL,$OptionBitteWaehlen);

		if($EditModus and $AWIS_KEY2=='')
		{
			$AWISCursorPosition='txtRPP_LAN_CODE';
		}
		elseif ($EditModus)
		{
			$AWISCursorPosition='txtRPP_VK';
			$Form->Erstelle_HiddenFeld('RPP_LAN_CODE',$rsRPP->FeldInhalt('RPP_LAN_CODE'));
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RPP']['RPP_VK'].':',100);
		$Form->Erstelle_TextFeld('!RPP_VK',$rsRPP->FeldInhalt('RPP_VK'),14,100,$EditModus,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RPP']['RPP_GUELTIGAB'].':',100);
		$Form->Erstelle_TextFeld('!RPP_GUELTIGAB',$rsRPP->FeldInhalt('RPP_GUELTIGAB'),10,150,$EditModus,'','','','D');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}
	
	$AWISBenutzer->ParameterSchreiben('Formular_RRP_Preise',serialize($RPPParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>