<?php
/**
 * Listen und Detail-Seite des Seminarplans
 * 
 * @author Thomas Riedl
 * @copyright ATU Auto Teile Unger
 * @version 200809291144
 */

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_KEY3;
global $MailOK;
global $EmailMeldung;
try 
{	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SEP','%');
	$TextKonserven[]=array('SET','%');
	$TextKonserven[]=array('SER','%');
	$TextKonserven[]=array('SRG','%');
	$TextKonserven[]=array('SEH','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_termin');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');	
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_Sonstiges');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	$TextKonserven[]=array('Wort','Abschliessen');
	$TextKonserven[]=array('Wort','Abbrechen');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
		
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
	if($BildschirmBreite<1024)
	{
		$FeldBreiten['SEP_BESCHREIBUNG']=230;
		$FeldBreiten['SEP_ESS']=230;
		$FeldBreiten['SER_RAUM']=110;
	}
	elseif($BildschirmBreite<1280)
	{
		$FeldBreiten['SEP_BESCHREIBUNG']=400;
		$FeldBreiten['SEP_ESS']=400;
		$FeldBreiten['SER_RAUM']=200;
	}
	else
	{
		$FeldBreiten['SEP_BESCHREIBUNG']=500;
		$FeldBreiten['SEP_ESS']=500;
		$FeldBreiten['SER_RAUM']=250;
	}
		
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht4200 = $AWISBenutzer->HatDasRecht(4200);
	$Recht4204 = $AWISBenutzer->HatDasRecht(4204);
	if($Recht4200==0)
	{
		$Form->Fehler_KeineRechte();
	}
			
	//********************************************************
	// Parameter ?
	//********************************************************
	$Param = array_fill(0,15,'');	
	$Form->DebugAusgabe(1,$_POST);
	if(isset($_POST['cmdSuche_x']))
	{
		$Param['KEY']=0;			// Key
		
		$Param['SEP_SEMINAR']=$_POST['sucSEP_SEMINAR'];	
		$Param['SEP_BESCHREIBUNG']=$_POST['sucSEP_BESCHREIBUNG'];	
		$Param['SEP_VON']=$_POST['sucSEP_VON'];	
		$Param['SEP_BIS']=$_POST['sucSEP_BIS'];	
		$Param['SEP_SRG_KEY']=$_POST['sucSEP_SRG_KEY'];			
		$Param['SEP_SER_KEY']=$_POST['sucSEP_SER_KEY'];			
		$Param['SEP_ZIELGRUPPE']=$_POST['sucSEP_ZIELGRUPPE'];			
		$Param['SEP_SCHULUNGSART']=$_POST['sucSEP_SCHULUNGSART'];			
		
		if (($Recht4200&32)==32)
		{
			$Param['SET_PER_NR']=$_POST['sucSET_PER_NR'];			
		}
		
		$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
	
		$Param['ORDERBY']='';
		$Param['BLOCK']='';
		$Param['KEY']=0;
	
		$AWISBenutzer->ParameterSchreiben("SEPSuche",serialize($Param));
		$AWISBenutzer->ParameterSchreiben("AktuellerSEP",'');					
	}					
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']) or isset($_GET['DocDel']) or isset($_GET['DelHotel']))
	{
        include('./seminarplan_loeschen.php');
	}
	elseif((isset($_POST['cmdSpeichern_x']) or isset($_GET['ok'])) and (!isset($_GET['SET_MAIL_KEY'])) and (!isset($_POST['txtSET_MAIL_KEY'])) and (!isset($_GET['ANABMELDUNG'])) and !isset($_GET['SET_ABSAGEBEST'])) 
	{
		include('./seminarplan_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['SEP_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['SEP_KEY']);
	}
	elseif(isset($_GET['SET_ANAB_KEY']))
	{
	    $AWIS_KEY1 = 0;
	    $SEPKeySQL = 'Select SET_SEP_KEY from Seminarteilnehmer where SET_KEY = ' . $_GET['SET_ANAB_KEY'];
        $rsSEPKey = $DB->RecordSetOeffnen($SEPKeySQL); 
	    
        $AWIS_KEY1 = $rsSEPKey->FeldInhalt('SET_SEP_KEY');
	}
	elseif(isset($_GET['SET_ABSAGEBEST_KEY']))
	{
	    $AWIS_KEY1 = 0;
	    $SEPKeySQL = 'Select SET_SEP_KEY from Seminarteilnehmer where SET_KEY = ' . $_GET['SET_ABSAGEBEST_KEY'];
	    $rsSEPKey = $DB->RecordSetOeffnen($SEPKeySQL);
	     
	    $AWIS_KEY1 = $rsSEPKey->FeldInhalt('SET_SEP_KEY');
	}elseif(isset($_GET['HOTEL_SET_KEY'])){
        $AWIS_KEY1 = 0;
        $SEPKeySQL = 'Select SET_SEP_KEY from Seminarteilnehmer where SET_KEY = ' . $_GET['HOTEL_SET_KEY'];
        $rsSEPKey = $DB->RecordSetOeffnen($SEPKeySQL);

        $AWIS_KEY1 = $rsSEPKey->FeldInhalt('SET_SEP_KEY');
    }
	else 		// Letzten Benutzer suchen
	{			

		if(!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block']))
		{			
			$AWIS_KEY1 = $AWISBenutzer->ParameterLesen("AktuellerSEP");
		}		
		
		if($Param=='' OR $Param=='0')
		{			
			$AWISBenutzer->ParameterSchreiben("AktuellerSEP",0);			
		}								
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen("SEPSuche"));	
	
	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************
	if(!isset($_GET['Sort']))
	{
		if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
		{
			$ORDERBY = $Param['ORDERBY'];
		}
		else
		{
			$ORDERBY = ' SEP_TRAINING';
		}		
	}
	else
	{
		$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	
	//********************************************************
	// Daten suchen
	//********************************************************
	$SQL = 'SELECT DISTINCT SEP_KEY, SEP_SEMINAR, SEP_BESCHREIBUNG, SEP_TRAINING, SEP_VON, SEP_BIS';
	$SQL.= ', SEP_FREIEPLAETZE, SEP_SER_KEY, SER_RAUM, nvl(SEP_SRG_KEY,0) as SEP_SRG_KEY, SRG_REGION, SEP_ZIELGRUPPE, SEP_TRAINER, SEP_STATUS, SEP_SCHULUNGSART, SEP_PDFNAME, SEP_SEMINARSTATUS';
	$SQL.= ', SEP_BEREICH, SEP_ESS, SEP_USER, SEP_USERDAT';
	if($AWIS_KEY1<=0)
	{
		$SQL .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
	}		
	$SQL .= ' FROM SEMINARPLAN SEP ';
	$SQL .= ' LEFT OUTER JOIN SEMINARRAEUME SER ON SER_KEY = SEP_SER_KEY';
	$SQL .= ' LEFT OUTER JOIN SEMINARREGIONEN SRG ON SRG_KEY = SEP_SRG_KEY';
	$SQL .= ' LEFT OUTER JOIN SEMINARANZEIGE SEA on SEA.SEA_BEREICH = SEP.SEP_BEREICH';
    $SQL .= ' WHERE SEP_STATUS = 1';
    $SQL .= ' and SEA.SEA_BEREICH is null';
	
	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}	
		
	$SQL .= ' ORDER BY '.$ORDERBY;
	$Form->DebugAusgabe(1,$SQL);				
	
	//************************************************
	// Aktuellen Datenblock festlegen
	//************************************************
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
	}
	elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
	{
		$Block = intval($Param['BLOCK']);
	}
	
	//************************************************
	// Zeilen begrenzen
	//************************************************
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		
	//*****************************************************************
	// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
	//*****************************************************************
	if($AWIS_KEY1<=0)
	{
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	
	$rsSEP = $DB->RecordSetOeffnen($SQL);
	
	//*****************************************************************
	// Aktuelle Parameter sichern
	//*****************************************************************	
	$Param['ORDERBY']=$ORDERBY;
	$Param['KEY']=$AWIS_KEY1;
	$Param['BLOCK']=$Block;
	$AWISBenutzer->ParameterSchreiben("SEPSuche",serialize($Param));
	
	
	//Unterscheiden ob normales Formularstart oder mit get als anhang. 
	if(isset($_GET['ANABMELDUNG']))
	{
	    $Form->SchreibeHTMLCode('<form name=frmSeminarplan action=./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_ANAB_KEY='.$_GET['SET_ANAB_KEY'] . '&ANABMELDUNG=1 method=POST  enctype="multipart/form-data">');
	}
	elseif(isset($_GET['SET_ABSAGEBEST']))
	{
	    $Form->SchreibeHTMLCode('<form name=frmSeminarplan action=./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_ABSAGEBEST_KEY='.$_GET['SET_ABSAGEBEST_KEY'] . '&SET_ABSAGEBEST=1 method=POST  enctype="multipart/form-data">');
	}elseif(isset($_GET['HOTEL_SET_KEY'])){
        $Form->SchreibeHTMLCode('<form name="frmSEHDocs" id="frmSEHDocs" action=./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&HOTEL_SET_KEY='.$_GET['HOTEL_SET_KEY'].' method=POST  enctype="multipart/form-data">');
        echo $EmailMeldung;
    }
	else
	{
	    $Form->SchreibeHTMLCode('<form name=frmSeminarPlan action=./seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');   
	}


	//********************************************************
	// Daten anzeigen
	//********************************************************	
	if($rsSEP->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
	}
	elseif(($rsSEP->AnzahlDatensaetze()>1) or (isset($_GET['Liste'])))						// Liste anzeigen
	{	


		$Form->ZeileStart();
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=SEP_SEMINAR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_SEMINAR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_SEMINAR'],100,'',$Link);
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=SEP_TRAINING'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_TRAINING'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_TRAINING'],140,'',$Link);
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=SEP_BESCHREIBUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_BESCHREIBUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_BESCHREIBUNG'],$FeldBreiten['SEP_BESCHREIBUNG'],'',$Link);
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=SEP_VON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_VON'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_VON'],100,'',$Link);				
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=SEP_BIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_BIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_BIS'],100,'',$Link);
        $Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=SEP_ESS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_ESS'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_ESS'],$FeldBreiten['SEP_ESS'],'',$Link);
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=SEP_FREIEPLAETZE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_FREIEPLAETZE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_FREIEPLAETZE'],110,'',$Link);						
		$Link = './seminarplan_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=SER_RAUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SER_RAUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SER']['SER_RAUM'],$FeldBreiten['SER_RAUM'],'',$Link);
        $Form->ZeileEnde();
		
		$SEPZeile=0;
		while(!$rsSEP->EOF())
		{
			$Form->ZeileStart();
			$Link = './seminarplan_Main.php?cmdAktion=Details&SEP_KEY='.$rsSEP->FeldInhalt('SEP_KEY').'';
			$Form->Erstelle_ListenFeld('SEP_SEMINAR',$rsSEP->FeldInhalt('SEP_SEMINAR'),0,100,false,($SEPZeile%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('SEP_TRAINING',$rsSEP->FeldInhalt('SEP_TRAINING'),0,140,false,($SEPZeile%2),'','','T');
			$Form->Erstelle_ListenFeld('SEP_BESCHREIBUNG',$rsSEP->FeldInhalt('SEP_BESCHREIBUNG'),0,$FeldBreiten['SEP_BESCHREIBUNG'],false,($SEPZeile%2),'','');
			$Form->Erstelle_ListenFeld('SEP_VON',$rsSEP->FeldInhalt('SEP_VON'),0,100,false,($SEPZeile%2),'','','D','L','');		
			$Form->Erstelle_ListenFeld('SEP_BIS',$rsSEP->FeldInhalt('SEP_BIS'),0,100,false,($SEPZeile%2),'','','D','L','');

			//Hinweise erhalten wir �ber das ES-Feld beim TC-Import. Stand 3.7.2018: AM
            //0 = buchbar �ber Filiale
            //1 = nicht buchbar �ber Filiale
			$SEP_ESSText="";
            $SEP_ESSDD = explode("|",$AWISSprachKonserven['SEP']['lst_SEP_ESS']);
            foreach ($SEP_ESSDD as $SEP_ESS) {
                $SEP_ESSStatus = explode("~", $SEP_ESS);
                if($SEP_ESSStatus[0] == $rsSEP->FeldInhalt('SEP_ESS')){
                    $SEP_ESSText = $SEP_ESSStatus[1];
                    break;
                }
            }
            $Form->Erstelle_ListenFeld('SEP_ESS', $SEP_ESSText, 0, $FeldBreiten['SEP_ESS'], false, ($SEPZeile % 2), '', '', 'T', 'L', '');
			$Form->Erstelle_ListenFeld('SEP_FREIEPLAETZE',$rsSEP->FeldInhalt('SEP_FREIEPLAETZE'),0,110,false,($SEPZeile%2),'','','','','');											
			$Form->Erstelle_ListenFeld('SEP_SER_KEY',$rsSEP->FeldInhalt('SER_RAUM'),0,$FeldBreiten['SER_RAUM'],false,($SEPZeile%2),'','','','','');
            $Form->ZeileEnde();
	
			$rsSEP->DSWeiter();
			$SEPZeile++;
		}
	
		$Link = './seminarplan_Main.php?cmdAktion=Details&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	
		$Form->Formular_Ende();
	
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		if(($Recht4200&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}			
		$Form->Schaltflaeche('href','cmd_Seminarplan','/dokumentanzeigen.php?dateiname=Seminarprogramm_neu&erweiterung=pdf&bereich=seminarplan','/bilder/cmd_pdf.png');
		$Form->SchaltflaechenEnde();
	
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
	    if(isset($_GET['ANABMELDUNG']))
	    {
	        $Form->SchreibeHTMLCode('<form name=frmSeminarplan action=./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_ANAB_KEY='.$_GET['SET_ANAB_KEY'] . '&ANABMELDUNG=1 method=POST  enctype="multipart/form-data">');
	         
	    }
	    elseif(isset($_GET['SET_MAIL_KEY']))
	    {
	        $Form->SchreibeHTMLCode('<form name=frmSeminarplan action=./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_MAIL_KEY='.$_GET['SET_MAIL_KEY'] . '&STATUSEMAIL='.$_GET['SET_STATUSEMAIL'] .'method=POST  enctype="multipart/form-data">');
	    }
	    elseif (isset($_POST['txtSET_MAIL_KEY']))
	    {
	        $Form->SchreibeHTMLCode('<form name=frmSeminarplan action=./seminarplan_Main.php?cmdAktion=Details&Seite=Teilnehmer&SET_MAIL_KEY='.$_POST['txtSET_MAIL_KEY'] . '&STATUSEMAIL='.$_POST['txtSET_STATUSEMAIL'] .'method=POST  enctype="multipart/form-data">');
	    }
	    else
	    {
	        $Form->SchreibeHTMLCode('<form name=frmSeminarplan action=./seminarplan_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');
	  
	    }
	    
		$AWIS_KEY1 = $rsSEP->FeldInhalt('SEP_KEY');		
		
		$AWISBenutzer->ParameterSchreiben("AktuellerSEP",$AWIS_KEY1);
		$Form->Erstelle_HiddenFeld('SEP_KEY',$AWIS_KEY1);	
	
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./seminarplan_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSEP->FeldInhalt('SEP_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSEP->FeldInhalt('SEP_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$EditRecht=(($Recht4200&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
			$Form->Erstelle_HiddenFeld('SEP_SCHULUNGSART','2');	
		}

		if (($Recht4200&1024)!=0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_SCHULUNGSART'].':',180);
			$KateogrieAlt = explode("|",$AWISSprachKonserven['SEP']['lst_SEP_SCHULUNGSART']);		
			$Form->Erstelle_SelectFeld('SEP_SCHULUNGSART',($AWIS_KEY1==0?'2':($rsSEP->FeldInhalt('SEP_SCHULUNGSART'))),50,$EditRecht,'','','','','',$KateogrieAlt);
			$Form->ZeileEnde();			
		}

		if ($rsSEP->FeldInhalt('SEP_SCHULUNGSART') == 1)
		{
		    $EditRecht = false;
        }
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_SEMINAR'].':',180);
		$Form->Erstelle_TextFeld('SEP_SEMINAR',($rsSEP->FeldInhalt('SEP_SEMINAR')),50,'',$EditRecht);
		$AWISCursorPosition='txtSEP_SEMINAR';
		$Form->Schaltflaeche('href','cmd_Seminarziele','/dokumentanzeigen.php?dateiname=LZB_'.$rsSEP->FeldInhalt('SEP_SEMINAR').'&erweiterung=pdf&bereich=seminarziele','/bilder/info.png','Seminarziele','','',16,16);
		$Form->ZeileEnde();	
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_BESCHREIBUNG'].':',180);
		$Form->Erstelle_TextFeld('SEP_BESCHREIBUNG',($rsSEP->FeldOderPOST('SEP_BESCHREIBUNG')),100,450,$EditRecht);
		$Form->ZeileEnde();	
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SET']['SET_STATUS'].':',180);
		$Form->Erstelle_TextFeld('SEP_SEMINARSTATUS',($rsSEP->FeldOderPOST('SEP_SEMINARSTATUS')),100,450,$EditRecht);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_TRAINING'].':',180);
		$Form->Erstelle_TextFeld('SEP_TRAINING',($rsSEP->FeldOderPOST('SEP_TRAINING')),50,300,$EditRecht);
		$Form->ZeileEnde();	

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_VON'].':',180);
		$Form->Erstelle_TextFeld('SEP_VON',$rsSEP->FeldOderPOST('SEP_VON','DU'),20,200,$EditRecht,'','','','DU','L','',date('d.m.Y') . ' 09:00');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_BIS'].':',180);
		$Form->Erstelle_TextFeld('SEP_BIS',$rsSEP->FeldOderPOST('SEP_BIS','DU'),20,200,$EditRecht,'','','','DU','L','',date('d.m.Y') . ' 16:30');
		$Form->ZeileEnde();	
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_FREIEPLAETZE'].':',180);
		$Form->Erstelle_TextFeld('SEP_FREIEPLAETZE',($rsSEP->FeldOderPOST('SEP_FREIEPLAETZE')),20,200,$EditRecht);
		$Form->ZeileEnde();						
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SRG']['SRG_REGION'].':',180);
		if($AWIS_KEY1==0 or $rsSEP->FeldInhalt('SEP_SCHULUNGSART')=='2')
		{
			$SQL = 'SELECT SRG_KEY, SRG_REGION FROM SEMINARREGIONEN WHERE SRG_STATUS=10 ORDER BY SRG_REGION';
		}
		else 
		{
			$SQL = 'SELECT SRG_KEY, SRG_REGION FROM SEMINARREGIONEN WHERE SRG_STATUS in (1,10) ORDER BY SRG_REGION';
		}
		
		$Form->DebugAusgabe(1, $SQL);
		if  ($rsSEP->FeldInhalt('SEP_SRG_KEY') != '')
		{
		  $SRGKEY = $rsSEP->FeldInhalt('SEP_SRG_KEY');        
		}
		else 
		{
		    $SRGKEY = 0;
		}
        $SQLAnfahrtsbeschreibung = 'Select SRG_REGION from Seminarregionen where SRG_KEY = ' . $SRGKEY;
        $Form->DebugAusgabe(1, $SQLAnfahrtsbeschreibung);
        $rsAnfahrtsbeschreibung = $DB->RecordSetOeffnen($SQLAnfahrtsbeschreibung);
        $Zusatzoption = $AWISSprachKonserven['Liste']['lst_Sonstiges'];
		$Form->Erstelle_SelectFeld('SEP_SRG_KEY',($rsSEP->FeldInhalt('SEP_SRG_KEY')),'',$EditRecht,$SQL,$OptionBitteWaehlen,'','','',[$Zusatzoption]);
        $Form->Schaltflaeche('href','cmd_Anfahrt','/dokumentanzeigen.php?dateiname='. $rsAnfahrtsbeschreibung->FeldInhalt('SRG_REGION').'&erweiterung=pdf&bereich=seminarplananfahrt','/bilder/icon_globus.png','Anfahrtsbeschreibung','','',16,16);

        // SEI_ITY_KEY
        // 600: Lokationsname + Region
        // 601: PLZ
        // 602: Ort
        // 603: Strasse
        // 604: Raum

		$SQL = 'select (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = ' . $DB->WertSetzen('SEI','Z',600);
		$SQL.= ' and SEI_XTN_KUERZEL = \'SEP\' and SEI_XXX_KEY = '.$DB->WertSetzen('SEI','Z',$AWIS_KEY1).') as SONST_SEP_SRG_KEY,';
		$SQL.= ' (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = ' . $DB->WertSetzen('SEI','Z',601);
		$SQL.= ' and SEI_XTN_KUERZEL = \'SEP\' and SEI_XXX_KEY = '.$DB->WertSetzen('SEI','Z',$AWIS_KEY1).') as SONST_SEP_SPA_PLZ,';
		$SQL.= ' (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = ' . $DB->WertSetzen('SEI','Z',602);
		$SQL.= ' and SEI_XTN_KUERZEL = \'SEP\' and SEI_XXX_KEY = '.$DB->WertSetzen('SEI','Z',$AWIS_KEY1).') as SONST_SEP_SPA_ORT,';
		$SQL.= ' (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = ' . $DB->WertSetzen('SEI','Z',603);
		$SQL.= ' and SEI_XTN_KUERZEL = \'SEP\' and SEI_XXX_KEY = '.$DB->WertSetzen('SEI','Z',$AWIS_KEY1).') as SONST_SEP_SPA_STRASSE';
		$SQL.= ' from dual';
		$rsSEI = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SEI'));

		$Style = ($rsSEP->FeldInhalt('SEP_SRG_KEY')==-99?'':'display:none;');

		$Form->Erstelle_TextFeld('SONST_SEP_SRG_KEY',$rsSEI->FeldOderPOST('SONST_SEP_SRG_KEY'),30,250,$EditRecht,'',$Style,'','T','L','Lokationsname','',0,'','','placeholder="Lokationsname"'); //Region + Lokationsname
        $Form->Erstelle_TextFeld('SONST_SEP_SPA_STRASSE',$rsSEI->FeldOderPOST('SONST_SEP_SPA_STRASSE'),30,250,$EditRecht,'',$Style,'','T','L','Strasse + Hausnummer',($rsSEI->FeldInhalt('SONST_SEP_SPA_STRASSE')!='')?$rsSEI->FeldInhalt('SONST_SEP_SPA_STRASSE'):'Dr. Kilian Strasse 11',0,'','','placeholder="Strasse + Hausnr."'); //Strasse
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextFeld('Platzhalter','',30,180,false,'',$Style); //Platzhalter bzw. Spacer
        $Form->Erstelle_TextFeld('SONST_SEP_SPA_PLZ',$rsSEI->FeldOderPOST('SONST_SEP_SPA_PLZ'),10,140,$EditRecht,'',$Style,'','T','L','Postleitzahl',($rsSEI->FeldInhalt('SONST_SEP_SPA_PLZ')!='')?$rsSEI->FeldInhalt('SONST_SEP_SPA_PLZ'):'92637',0,'','','placeholder="PLZ"'); //PLZ
		$Form->Erstelle_TextFeld('SONST_SEP_SPA_ORT',$rsSEI->FeldOderPOST('SONST_SEP_SPA_ORT'),30,250,$EditRecht,'',$Style,'','T','L','Ort',($rsSEI->FeldInhalt('SONST_SEP_SPA_ORT')!='')?$rsSEI->FeldInhalt('SONST_SEP_SPA_ORT'):'Weiden',0,'','','placeholder="Ort"'); //Ort
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SER']['SER_RAUM'].':',180);		
		if ($AWIS_KEY1==0  or $rsSEP->FeldInhalt('SEP_SCHULUNGSART')=='2')
		{
			$SQL = 'SELECT SER_KEY, SER_RAUM FROM SEMINARRAEUME WHERE SER_STATUS=10 ORDER BY SER_RAUM';					
		}
		else 
		{
			$SQL = 'SELECT SER_KEY, SER_RAUM FROM SEMINARRAEUME WHERE SER_STATUS in (1,10) ORDER BY SER_RAUM';					
		}
        $Zusatzoption = $AWISSprachKonserven['Liste']['lst_Sonstiges'];

        $SQL = 'select SEI_WERT as SONST_SEP_SER_KEY from SEMINARPLANINFOS where SEI_ITY_KEY = ' . $DB->WertSetzen('SEI','Z',604);
        $SQL .= ' and SEI_XTN_KUERZEL = \'SEP\' and SEI_XXX_KEY = ' . $DB->WertSetzen('SEI','Z',$AWIS_KEY1);
        $rsSEI = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SEI'));

        $Style = ($rsSEP->FeldInhalt('SEP_SER_KEY')==-99?'':'display:none;');

		$Form->Erstelle_SelectFeld('SEP_SER_KEY',($rsSEP->FeldInhalt('SEP_SER_KEY')),130,$EditRecht,$SQL,$OptionBitteWaehlen,'','','',[$Zusatzoption]);
        $Form->Erstelle_TextFeld('SONST_SEP_SER_KEY',$rsSEI->FeldOderPOST('SONST_SEP_SER_KEY'),15,170,$EditRecht,'',$Style);

        $Form->ZeileEnde();
				
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SEP']['SEP_TRAINER'].':',180);
		$Form->Erstelle_TextFeld('SEP_TRAINER',($rsSEP->FeldInhalt('SEP_TRAINER')),50,300,$EditRecht);		
		$Form->ZeileEnde();	
		
		$Ausblenden = '';
		if(!isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=0)
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Teilnehmer'));
			$RegDet = new awisRegister(4201);
			$RegDet->ZeichneRegister($RegisterSeite);
		}

        //PopUp f�r Terminempf�ngerabfrage
        if(($Recht4200&1048576)==1048576 AND !isset($_POST['cmdDSNeu_x']) AND !isset($_GET['HOTEL_SET_KEY'])) {
            $EmpfohleneEmpfaenger = ladeEmpfaengerListe($AWIS_KEY1);
            ob_start();
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel('An Emailadressen (Semikolon getrennt):', 300);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_Textarea('MailEmpfaenger',$EmpfohleneEmpfaenger,400,50,4,true);
            $Form->ZeileEnde();
            $Inhalt = ob_get_clean();
            $Schaltflaechen = array(array('/bilder/cmd_weiter.png', 'cmdSenden', 'post', 'Senden'), array('/bilder/cmd_ds.png', 'close', 'close', 'Schlie�en'));
            $Form->PopupDialog('Vorgeschlagene Mailempfaenger', $Inhalt, $Schaltflaechen, awisFormular::POPUP_INFO, '1');
        }

		$Form->Formular_Ende();
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();	
		
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	    if(isset($_GET['HOTEL_SET_KEY']) and ($Recht4200&524288)==524288){ //Hotelinformationsseite

            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
            $Form->Erstelle_TextLabel('',23);
            $Form->Erstelle_Checkbox('Emailsenden',(($AWIS_KEY3 AND $EmailMeldung=='')>0?'off':'on'),'20',true,'on','padding-top: 5px;','','','',true);
            $Form->Erstelle_TextLabel('Teilnehmer benachrichtigen',150,'color: white; font-family: Arial, Helvetica, sans-serif;  font-size: 12px; padding-top: 6px;');

        }
        elseif((isset($_GET['ANABMELDUNG'])or isset($_GET['SET_ABSAGEBEST']) ) or (isset($_GET['SET_MAIL_KEY'])) or (isset($_POST['txtSET_MAIL_KEY']))) //Nur speichern anzeigen, wenn ne Mail Manuell ausgel�st wird, bzw. die Absage Best�tigt wird oder ein Teilnehmer Zu oder Absagen will
	    {
           $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	    }
	    elseif((!isset($_POST['txtMailOK']) and !isset($_POST['txtSET_MAIL_KEY']) and !isset($_GET['SET_MAIL_KEY']))) 
	    {
	        if(($Recht4200&6)!=0 or ($Recht4200&128)!=0 or  (isset($_GET['Seite']) and $_GET['Seite'] == 'Feedback' and isset($_GET['SEE_KEY'])) or (isset($_GET['SET_KEY']) and $_GET['SET_KEY'] == 0))
	        {
	            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	        }
	        
	        if(($Recht4200&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	        {
	            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	        }
	        	
	        if(($Recht4200&8)==8 AND !isset($_POST['cmdDSNeu_x']))
	        {
	            $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	        }
	        
	        if(is_file('/daten/web/dokumente/seminarplan/'.($rsSEP->FeldInhalt('SEP_PDFNAME')==''?$rsSEP->FeldInhalt('SEP_SEMINAR'):$rsSEP->FeldInhalt('SEP_PDFNAME')).'.pdf'))
	        {
	            $Form->Schaltflaeche('href','cmd_Seminarplan','/dokumentanzeigen.php?dateiname='.($rsSEP->FeldInhalt('SEP_PDFNAME')==''?$rsSEP->FeldInhalt('SEP_SEMINAR'):$rsSEP->FeldInhalt('SEP_PDFNAME')).'&erweiterung=pdf&bereich=seminarplan','/bilder/cmd_pdf.png');
	        }

            if(($Recht4200&1048576)==1048576 AND !isset($_POST['cmdDSNeu_x']))
            {
                $Form->Schaltflaeche('script', 'cmdTermin', $Form->PopupOeffnen(1), '/bilder/cmd_notizblock.png', $AWISSprachKonserven['Wort']['lbl_termin'], 'T');
            }
	        
	    }
			
		$Form->SchaltflaechenEnde();
        
		if(!isset($_GET['ANABMELDUNG']))
		{
		    $Form->SchreibeHTMLCode('</form>');
		}
	
	}
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}	
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031126");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908031127");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
	
/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';
	
	if(floatval($AWIS_KEY1) != 0)
	{				
		$Bedingung.= 'AND SEP_KEY = '.$AWIS_KEY1;
		return $Bedingung;
	}
				
	if(isset($Param['SEP_SEMINAR']) AND $Param['SEP_SEMINAR']!='')
	{			
		$Bedingung .= 'AND SEP_SEMINAR ' . $DB->LikeOderIst($Param['SEP_SEMINAR']) . ' ';					
	}	
		
	if(isset($Param['SEP_BESCHREIBUNG']) AND $Param['SEP_BESCHREIBUNG']!='')
	{			
		$Bedingung .= 'AND UPPER(SEP_BESCHREIBUNG) ' . $DB->LikeOderIst($Param['SEP_BESCHREIBUNG'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';					
	}	
	
	if(isset($Param['SEP_VON']) AND $Param['SEP_VON']!='')
	{
		$Bedingung .= ' AND SEP_VON >= '.$DB->FeldInhaltFormat('D',$Param['SEP_VON'],false);
	}
	
	if(isset($Param['SEP_BIS']) AND $Param['SEP_BIS']!='')
	{
		$Bedingung .= ' AND SEP_BIS <= '.$DB->FeldInhaltFormat('D',$Param['SEP_BIS'],false);
	}
	
	if(isset($Param['SEP_SER_KEY']) AND $Param['SEP_SER_KEY']!=0)
	{
		$Bedingung .= 'AND SEP_SER_KEY =' . $DB->FeldInhaltFormat('NO',$Param['SEP_SER_KEY']) . ' ';					
	}		
		
	if(isset($Param['SEP_SRG_KEY']) AND $Param['SEP_SRG_KEY']!=0)
	{
		$Bedingung .= 'AND SEP_SRG_KEY =' . $DB->FeldInhaltFormat('NO',$Param['SEP_SRG_KEY']) . ' ';					
	}		
	
	if(isset($Param['SEP_ZIELGRUPPE']) AND $Param['SEP_ZIELGRUPPE']!='')
	{			
		$Bedingung .= 'AND UPPER(SEP_ZIELGRUPPE) ' . $DB->LikeOderIst($Param['SEP_ZIELGRUPPE'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';					
	}		
	
	if(isset($Param['SET_PER_NR']) AND $Param['SET_PER_NR']!='')
	{					
		//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
		$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
				
		// Beschr�nkung auf eine Filiale?
		if ($UserFilialen != '')
		{
			$Bedingung .= 'AND SEP_KEY IN (SELECT SET_SEP_KEY FROM SEMINARTEILNEHMER WHERE SET_STATUSTEILNEHMER <> 3 AND SET_PER_NR = ' . $DB->FeldInhaltFormat('NO',$Param['SET_PER_NR']) . ' AND SET_FIL_NR IN ('.$UserFilialen.')) ';
		}
		else 
		{				
			$Bedingung .= 'AND SEP_KEY IN (SELECT SET_SEP_KEY FROM SEMINARTEILNEHMER WHERE SET_STATUSTEILNEHMER <> 3 AND SET_PER_NR = ' . $DB->FeldInhaltFormat('NO',$Param['SET_PER_NR']) . ') ';
		}
	}		
	
	if(isset($Param['SEP_SCHULUNGSART']) AND $Param['SEP_SCHULUNGSART']!=0)
	{			
		$Bedingung .= 'AND SEP_SCHULUNGSART =' . $DB->FeldInhaltFormat('NO',$Param['SEP_SCHULUNGSART']) . ' ';					
	}		

	return $Bedingung;
}

/**
 * Ladet ein String mit allen vorgeschlagenen Empf�nger-Mail-Adressen
 * Per Semikolon getrennt
 *
 * @param $AWIS_KEY1
 * @return String
 */
function ladeEmpfaengerListe($AWIS_KEY1){

    global $DB;

    $SQL="";
    $SQL.="Select a.*, b.* from ";
    $SQL.=" SEMINARTEILNEHMER a";
    $SQL.=" LEFT JOIN SEMINARPLAN b";
    $SQL.=" ON a.SET_SEP_KEY = b.SEP_KEY";
    $SQL.=" WHERE";
    $SQL.=" a.SET_SEP_KEY = ".$DB->WertSetzen('SEK','Z',$AWIS_KEY1);
    $SQL.=" AND a.SET_STATUSTEILNEHMER != 3";

    $rsSET = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SEK'));

    $Teilnehmerliste = array();

    while (!$rsSET->EOF()) {

        if($rsSET->FeldInhalt('SET_FILIALE')=='Zentrale') { //Zentralmitarbeiter
            $SQL = 'select kko_wert as email from kontakte ';
            $SQL .= ' inner join KONTAKTEKOMMUNIKATION ';
            $SQL .= ' on kon_key = KKO_KON_KEY ';
            $SQL .= ' where kko_kot_key = 7 and rownum = 1 and';
            $SQL .= ' KON_PER_NR = ' . $DB->WertSetzen('SET', 'T', $rsSET->FeldInhalt('SET_PER_NR'));

            $rsKONMail = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SET'));

            if ($rsKONMail->FeldInhalt('EMAIL') != '') {
                $Teilnehmerliste[] = $rsKONMail->FeldInhalt(1);
            }

        } else { //Filialmitarbeiter
            $SQL = 'SELECT LAN_CODE from V_FILIALEN_AKTUELL where FIL_ID = ' . $DB->WertSetzen('FIL', 'Z', $rsSET->FeldInhalt('SET_FIL_NR'));
            $rsLAN = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FIL'));
            $Teilnehmerliste[] = str_pad($rsSET->FeldInhalt('SET_FIL_NR'), 4, '0', STR_PAD_LEFT) . '@' . ($rsLAN->FeldInhalt('LAN_CODE') != '' ? $rsLAN->FeldInhalt('LAN_CODE') : 'de') . '.atu.eu';
        }

        $rsSET->DSWeiter();
    }

    return "ATUAcademy@de.atu.eu;". implode(';',array_unique($Teilnehmerliste));
}

?>