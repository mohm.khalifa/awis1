<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once 'warengruppen_funktionen.inc';
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('WGR', '%');
    $TextKonserven[] = array('WUG', '%');
    $TextKonserven[] = array('WGI', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $WGR = new warengruppen_funktionen();

    $Funktionen = new warengruppen_funktionen();

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_WGI'));

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht621 = $AWISBenutzer->HatDasRecht(621);
    if ($Recht621 == 0) {
        awisEreignis(3, 1000, 'warengruppen', $AWISBenutzer->BenutzerName(), '', '', '');
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $awisWerkzeug = new awisWerkzeuge();

    //********************************************************
    // Parameter ?
    //********************************************************

    if (isset($_GET['WGR_ID'])) {
        $AWIS_KEY1 = $_GET['WGR_ID'];
    } elseif (isset($_POST['txtWGR_ID'])) {
        $AWIS_KEY1 = $_POST['txtWGR_ID'];
    } else {
        $AWIS_KEY1 = '';
    }

    if (isset($_GET['WUG_ID'])) {
        $AWIS_KEY2 = $_GET['WUG_ID'];
    } elseif (isset($_POST['txtWUG_ID'])) {
        $AWIS_KEY2 = $_POST['txtWUG_ID'];
    } else {
        $AWIS_KEY2 = '';
    }

    if (isset($_POST['cmdSuche_x'])) {
        $Param = array();
        $Param['WGR_ID'] = $_POST['txtWGR_ID'];

        $Param['WGR_ID'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    } elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
        include('./warengruppen_loeschen.php');
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include('./warengruppen_speichern.php');
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_WGI'));
    } else        // Nicht �ber die Suche gekommen, letzten Key abfragen
    {
        if (!$AWIS_KEY1 > 0) {
            if (!isset($Param['WGR_ID'])) {
                $Param['WGR_ID'] = '';
                $Param['WHERE'] = '';
                $Param['ORDER'] = '';
            }

            if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
                $Param['WGR_ID'] = 0;
            }
            $AWIS_KEY1 = $Param['WGR_ID'];
        }
    }

    //*********************************************************
    //* Sortierung
    //*********************************************************

    if (!isset($_GET['Sort'])) {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '') {
            $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
        } else {
            $ORDERBY = ' ORDER BY WGR_ID asc';
            $Param['ORDER'] = 'WGR_ID asc';
        }
    } else {
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $BindeVariablen = array();
    $Bedingung = $WGR->erstelleWGRBedingung($Param);

    $SQL = 'SELECT warengruppen.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM warengruppen';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    $Form->DebugAusgabe(1, $SQL);

    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('WGR', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $DB->WertSetzen('WGR', 'N0', $StartZeile) . ' AND  ZeilenNr<' . $DB->WertSetzen('WGR', 'N0',
                ($StartZeile + $ZeilenProSeite));
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $rsWGR = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('WGR', true));

    $Form->DebugAusgabe(1, $DB->LetzterSQL(), $Param, $_GET, $_POST, $AWIS_KEY1);

    $AWISBenutzer->ParameterSchreiben('Formular_WGI', serialize($Param));

    //********************************************************
    // Daten anzeigen
    //********************************************************
    echo '<form name=frmwarengruppen  method=POST>';
    $DetailAnsicht = false;
    $Form->Formular_Start();
    if ($rsWGR->EOF() AND !isset($_POST['cmdDSNeu_x']))        // Keine Meldung bei neuen Datens�tzen!
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    } elseif ($rsWGR->AnzahlDatensaetze() > 1 AND !isset($_GET['WGR_ID']))                        // Liste anzeigen
    {

        $SpaltenBreite = array();
        $SpaltenBreite['WGR_ID'] = 100;
        $SpaltenBreite['WGR_BEZEICHNUNG'] = 400;

        $Form->ZeileStart();
        $Link = './warengruppen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=WGR_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'WGR_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WGR']['WGR_ID'], $SpaltenBreite['WGR_ID'], '', $Link);
        $Link = './warengruppen_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=WGR_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'WGR_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WGR']['WGR_BEZEICHNUNG'], $SpaltenBreite['WGR_BEZEICHNUNG'], '', $Link);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsWGR->EOF()) {
            $Form->ZeileStart();
            $Link = './warengruppen_Main.php?cmdAktion=Details&WGR_ID=' . $rsWGR->FeldInhalt('WGR_ID') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Form->Erstelle_ListenFeld('WGR_ID', $rsWGR->FeldInhalt('WGR_ID'), 0, $SpaltenBreite['WGR_ID'], false, ($DS % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('WGR_BEZEICHNUNG', $rsWGR->FeldInhalt('WGR_BEZEICHNUNG'), 0, $SpaltenBreite['WGR_BEZEICHNUNG'], false, ($DS % 2), '', '', 'T', 'L');
            $Form->ZeileEnde();

            $rsWGR->DSWeiter();
            $DS++;
        }

        $Link = './warengruppen_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else {//Ein DS
        $DetailAnsicht = true;
        $AWIS_KEY1 = $rsWGR->FeldInhalt('WGR_ID');

        $Param['WGR_ID'] = $AWIS_KEY1;

        $Form->Erstelle_HiddenFeld('WGR_ID', $AWIS_KEY1);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./warengruppen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsWGR->FeldInhalt('WGR_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsWGR->FeldInhalt('WGR_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WGR']['WGR_ID'] . ':', 170);
        $Form->Erstelle_TextFeld('!WGR_ID', $rsWGR->FeldInhalt('WGR_ID'), 10, 600, false, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['WGR']['WGR_BEZEICHNUNG'] . ':', 170);
        $Form->Erstelle_TextFeld('!WGR_BEZEICHNUNG', $rsWGR->FeldInhalt('WGR_BEZEICHNUNG'), 10, 600, false, '', '', '', 'T');
        $Form->ZeileEnde();

        $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Warengruppeninfos'));

        $Register = new awisRegister(622);
        $Register->ZeichneRegister($RegisterSeite);
    }

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->Formular_Ende();
    $Form->SchaltflaechenStart();

    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if (($Recht621 & 2) == 2 AND $DetailAnsicht) {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
    $AWISBenutzer->ParameterSchreiben('Formular_WGI', serialize($Param));
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>