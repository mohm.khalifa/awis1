<?php
require_once('/daten/include/awisZukauf_HESS.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobID = 0;
    $StartTag = '';
    $EndTag = '';

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--jobid':
                            $JobID = $argv[++$i];
                            break;
                    case '--starttag':
                            $StartTag = $argv[++$i];
                            break;
                    case '--endtag':
                            $EndTag = $argv[++$i];
                            break;
                    case '--help':
                    default:
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobID fuer die Protokollierung.'.PHP_EOL;
                            echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.'.PHP_EOL;
                            echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    if($JobID > 0)
    {
      //$Job = new awisJobProtokoll($JobID, 'Starte HESS - Rechnungenimport.');
    }


    if($StartTag == '')
    {
      $rsZUR = $DB->RecordSetOeffnen('SELECT MAX(ZUR_DATUM) AS LETZTER FROM Zukaufrechnungen WHERE ZUR_LIE_NR = \'1742\'');
      $Letzter = $Form->PruefeDatum($rsZUR->FeldInhalt('LETZTER'),0,0,1);
      $StartTag = date('d.m.y',$Letzter);
    }
    else
    {
      $Letzter = $Form->PruefeDatum($StartTag,false,false,true);
    }

    // Endedatum festlegen
    if($EndTag == '')
    {
       $EndTag = time();
    }
    else
    {
       $EndTag = $Form->PruefeDatum($EndTag,false,false,true);
    }


    $ZUK = new awisZukauf_HESS($DebugLevel,false);
    if($JobID > 0)
    {
      //$Job->SchreibeLog(3,1,'Suche offene Rechnungen...');
    }

    if($DebugLevel > 0)
    {
      echo 'Suche Daten zwischen dem '.date('d.m.y',$Letzter).' und '.date('d.m.y',$EndTag).'...'.PHP_EOL;
    }


    while($Letzter <= $EndTag)
    {
        if($JobID > 0)
        {
          //$Job->SchreibeLog(3,1,'Hole Daten vom '.date('d.m.Y',$Letzter));
        }

        if($ZUK->LeseRechnungen(date('d.m.y',$Letzter),date('d.m.y',$Letzter),'','','',true)!==false)
        {
           $ZUK->SpeichereRechnungen();
        }
        $Letzter += (60*60*24);             // Einen Tag weiter
    }

    if($JobID > 0)
    {
       //$Job->SchreibeLog(9,1,'Import ohne Fehler abgeschlossen.');
    }
}
catch (Exception $ex)
{
        echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
/*
        if(is_object($Job))
        {
                //$Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
        }
        if(trim($ex->getMessage())=='Gateway Error')
        {
                if(awisWerkzeuge::EMail(array('artikelcontrolling@de.atu.eu','shuttle@de.atu.eu'),'ATU- SOAP Rechnungen','Der Server ist nicht erreichbar. Fehlermeldung war: Gateway Error.',3)===false)
                {
                        echo '  --> Fehler bei EXTERN E-Mail';
                }
        }
        elseif(trim($ex->getMessage())=='Error Fetching http headers')
                if(awisWerkzeuge::EMail(array('artikelcontrolling@de.atu.eu','shuttle@de.atu.eu'),'ATU- SOAP Rechnungen','Der Server ist nicht erreichbar. Fehlermeldung war: Error Fetching http headers.',3)===false)
                {
                        echo '  --> Fehler bei EXTERN E-Mail';
                }
        }
*/
}
?>

