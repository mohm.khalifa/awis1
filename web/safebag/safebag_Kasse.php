<?php
require_once('safebag_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SBI','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_zurueck');    
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Wort','PDFErzeugen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht32000 = $AWISBenutzer->HatDasRecht(32000);
	if($Recht32000 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
    
    $AWIS_KEYs1 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TKD_KEY)
    $AWIS_KEYs2 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TGD_KEY)    
    $AWIS_KEYs1[0] = (isset($_GET['SBK_KEY'])? $_GET['SBK_KEY']: 0);    // wenn �ber GET (direkter Klick vom Benutzer auf href)
    if ($AWIS_KEYs1[0] <= 0)                                            // wenn 0-Index leer,
    {
        foreach ($_POST as $key => $value)                              // dann alle POST-�bergaben durchlaufen
        {
            if (strpos($key, 'txtSBK_CHK_') === 0)                      // wenn an 1. Position (0. Index), 'txtTKD_CHK_' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merken
            {
                if ($AWIS_KEYs1[0] <= 0)                                // wenn 0. Index vom Array noch unbelegt, dann hier hin schreiben
                {
                    $AWIS_KEYs1[0] = $value;
                }
                else                                                    // ansonsten hinzuf�gen
                {
                    $AWIS_KEYs1[] = $value;
                }
            }
        }
    }    
    
    $SBA = new awis_abgleich_funktionen();
    
    $AnsichtSuche = false;
    $AnsichtAuswahl = false;
    $AnsichtZuordnung = false;
    
    $Gespeichert = false;
    
    if (isset($_POST['cmdSuche_x']))
    {
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdDSWeiter_x']) and $AWIS_KEYs1[0] > 0)
    {
        $AnsichtZuordnung = true;
    }
    elseif (isset($_POST['cmdDSZurueck_x']))
    {
    	$AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdMail_x']))
    {
    	$Form->Hinweistext($AWISSprachKonserven['SBI']['MailGesendet'], 1);
    	if(isset($_POST['txtSBK_KEY']))
    	{
    		$SBK_KEY =	$_POST['txtSBK_KEY'];
    		$SBI_KEY = '';
    		$AbgleichArt = 'K'; 
    		$SBA->MailSenden($SBK_KEY,$SBI_KEY,$AbgleichArt,$AWISBenutzer);
    		
    	}
    	$AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdSpeichern_SK']) or isset($_POST['cmdSpeichern_SG']) or isset($_POST['cmdSpeichern_N']) or isset($_POST['cmdSpeichern_SD']))
    {
        if(isset($_POST['cmdSpeichern_SK']))
        {
        	$AbgleichArt = 'S';
        }
        else if (isset($_POST['cmdSpeichern_SG']))
        {
        	$AbgleichArt = 'SG';
        }
        else if (isset($_POST['cmdSpeichern_N']))
        {
        	$AbgleichArt = 'N';
        }
        else if (isset($_POST['cmdSpeichern_SD']))
        {
        	$AbgleichArt = 'SD';
        }
    	foreach ($AWIS_KEYs1 as $key => $SBKvalue)
        {
            //foreach ($AWIS_KEYs2 as $key => $TGDvalue)
            //{
            $SBA->AbgleichSpeichernP($SBKvalue, $_POST['txtSBA_BEMERKUNG_UNTEN'], $AWISBenutzer->BenutzerName(1),$AbgleichArt);
            //}
        }
        $Form->Hinweistext($AWISSprachKonserven['SBI']['DatenGespeichert'], 1);
        //$Form->ZeileStart();
        //$Form->Erstelle_TextLabel($AWISSprachKonserven['TAG']['ZurueckSuche'], 800, '', './abgleich_Main.php?cmdAktion=Kasse'); 
        //$Form->ZeileEnde();
        
        $SQL =  'select * from sbkassendaten ' .
        		//'where tgd_fil_id = :var_N0_fil_id ' .
        'where  sbk_abgeglichen = 0';
        if($_POST['txtFilId'] != '' and $_POST['txtFilId'] != '::bitte w�hlen::')
        {
        	$SQL .=' and sbk_fil_id = :var_N0_fil_id';
        	$DB->SetzeBindevariable('SBK', 'var_N0_fil_id', $_POST['txtFilId'] , awisDatenbank::VAR_TYP_GANZEZAHL);
        }
        if ($_POST['txtSafebagnr'] != '' and $_POST['txtSafebagnr'] != '::bitte w�hlen::')
        {
        	$SQL .=' and sbk_safebagnr =  :var_T_safebagnr';
        	$DB->SetzeBindevariable('SBK', 'var_T_safebagnr', $_POST['txtSafebagnr'] , awisDatenbank::VAR_TYP_TEXT);
        }
        if ($_POST['txtDatumVon'] != '' and $_POST['txtDatumVon'] != '::bitte w�hlen::')
        {
        	$SQL .= ' and (sbk_datum >= :var_D_DatumVon)';
        	$DB->SetzeBindevariable('SBK', 'var_D_DatumVon', $_POST['txtDatumVon'] , awisDatenbank::VAR_TYP_DATUM);
        }
        if ($_POST['txtDatumBis'] != '' and $_POST['txtDatumBis'] != '::bitte w�hlen::')
        {
        	$SQL .= ' and (sbk_datum <= :var_D_DatumBis)';
        	$DB->SetzeBindevariable('SBK', 'var_D_DatumBis', $_POST['txtDatumBis'] , awisDatenbank::VAR_TYP_DATUM);
        }
        
        
        $rsSuche = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SBK'));
        
        if ($rsSuche->AnzahlDatensaetze() > 0)
        {
        	$AnsichtAuswahl = true;
        }
        else
        {
        	$AnsichtSuche = true;
        }
        $Gespeichert = true;
    }    
    else
    {
        if (isset($_REQUEST['txtANSICHT']))        // wegen Block
        {
        	switch ($_REQUEST['txtANSICHT'])
            {
            	case 'Auswahl': $AnsichtAuswahl = true;     break;
                case 'Zuordnung':   $AnsichtZuordnung = true;   break;
                default: break;
            }
        }
        elseif ($AWIS_KEYs1[0] > 0)
        {
        	$AnsichtZuordnung = true;
        }
        else
        {
        	$AnsichtSuche = true;
        }
    }
    
$Form->DebugAusgabe(1, 'AnsichtSuche: ' . $AnsichtSuche, 'AnsichtAuswahl: ' . $AnsichtAuswahl, 'AnsichtZuordnung: ' . $AnsichtZuordnung);        
$Form->DebugAusgabe(1, $AWIS_KEYs1, $AWIS_KEYs2);
$Form->DebugAusgabe(1, $_REQUEST);    

	/**********************************************
	* Benutzerparameter
	***********************************************/
    $SuchParam = array();
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_SBK')) < 8))
	{
		$SuchParam['DatumVon'] = '';
        $SuchParam['DatumBis'] = '';
        $SuchParam['FilId'] = '';
        $SuchParam['Safebag'] = '';
        //$SuchParam['Entsorgt'] = '';
        $SuchParam['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_SBK", serialize($SuchParam));
	}
    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_SBK'));
    
	if (($AnsichtAuswahl and !isset($_GET['SBKSort'])) and !isset($_POST['cmdDSZurueck_x']) and !isset($_POST['cmdMail_x']))
	{
		$SuchParam['DatumVon'] = (isset($_POST['sucSBK_DATUMVON'])? $_POST['sucSBK_DATUMVON']: $SuchParam['DatumVon']);
        $SuchParam['DatumBis'] = (isset($_POST['sucSBK_DATUMBIS'])? $_POST['sucSBK_DATUMBIS']: $SuchParam['DatumBis']);
        $SuchParam['FilId'] = (isset($_POST['sucSBK_FILNR']) ? $_POST['sucSBK_FILNR']: $SuchParam['FilId']);
        $SuchParam['Safebag'] = (isset($_POST['sucSBK_SAFEBAG'])? $_POST['sucSBK_SAFEBAG']: $SuchParam['Safebag']);
        if(($Recht32000&8)==8)
        {
        	$SuchParam['Entsorgt'] = (isset($_POST['sucSBK_ENTSORGT'])? $_POST['sucSBK_ENTSORGT']: $SuchParam['Entsorgt']);
        }
        if(!isset($_POST['cmdSpeichern_SK']) and !isset($_POST['cmdSpeichern_SG']) and !isset($_POST['cmdSpeichern_N']) and !isset($_POST['cmdSpeichern_SD']))
        {
        	$SuchParam['SPEICHERN'] = ((isset($_POST['sucSBA_AUSWAHLSPEICHERN']) or ($Gespeichert == true) or isset($_POST['cmdDSZurueck_x']) or isset($_POST['cmdMail_x']))? 'on':'off');
        }
   
		$AWISBenutzer->ParameterSchreiben("Formular_SBK", serialize($SuchParam));	        
	}    
    else
    {
       	if ($SuchParam['SPEICHERN'] == 'off')
    	{
    		//$SuchParam['DatumVon'] = '';
            //$SuchParam['DatumBis'] = '';
            //$SuchParam['FilId'] = '';
            //$SuchParam['Safebag'] = '';
            $SuchParam['SPEICHERN']= 'off';
    	    $AWISBenutzer->ParameterSchreiben("Formular_SBK", serialize($SuchParam));    		
    	}
	}        

    /**********************************************
	* Formular
	***********************************************/ 
    //$Form->SchreibeHTMLCode('<form name=frmKasse action=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' method=POST enctype="multipart/form-data">');
    $Form->SchreibeHTMLCode('<form name=frmKasse action=./safebag_Main.php?cmdAktion=Kasse method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();
$Form->DebugAusgabe(1, $SuchParam);    

    if ($AnsichtSuche or $AnsichtAuswahl)
    {
        $SBA->Label_Filter($AnsichtSuche);
        $SBA->Feld_DatumVonBis($SuchParam, $AnsichtSuche);
        $SBA->Feld_SelectFilId($SuchParam, $AnsichtSuche, 2);
        
        $SBA->Feld_Safebag($SuchParam, $AnsichtSuche,2);
        if(($Recht32000&8)==8)
        {
        	$SBA->Feld_Entsorgt($SuchParam, $AnsichtSuche);
        }
        $SBA->Feld_AuswahlSpeichern($SuchParam, $AnsichtSuche);
    }
    
    if ($AnsichtAuswahl or $AnsichtZuordnung)
    {
    	$Sort= '';
    	if(isset($_GET['SBKSort']))
    	{
    		$Sort = $_GET['SBKSort'];
    	}
    	elseif(isset($_REQUEST['txtSBKSort']))
    	{
    		$Sort = $_REQUEST['txtSBKSort'];
    	}
    	
    	$Form->Trennzeile('L');
        
		// Infozeile zusammenbauen
		$Felder = array();
		//$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./safebag_Main.php?cmdAktion=Kasse accesskey=S title=' . $AWISSprachKonserven['SBI']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['SBI']['ZurueckSuche']);
		$Form->InfoZeile($Felder, '');
        
        if ($AnsichtAuswahl)
        {
        	$SQL = 'select sbk_key,sbk_fil_id,sbk_datum,sbk_betrag,'.
          		   'sbk_safebagnr,sbk_bgt1,sbk_ausgabeart,sbk_ausgabenr,sbk_user,sbk_userdat,'.
          		   'sbk_abgeglichen,liste,entsorgt,'.
        		   'row_number() OVER (';
        	if ($Sort != '')
        	{		// wenn GET-Sort, dann nach diesen Feld sortieren
        	$SQL .= ' order by ' . str_replace('~',' DESC ', $Sort);
        	}
        	else
        	{		// sonst pauschal nach Datum
        	$SQL .= ' order by sbk_datum asc';
        	}
        	$SQL .= ') AS ZeilenNr from(';
            $SQL .= ' select sbk.*,sbt.*, case when sbk_datum < AbholtagMax then 1 else 0 end as entsorgt from sbkassendaten sbk '.
            		'inner join v_sbt_fil_abholtage sbt on sbk_fil_id = sbt_fil_id) ';
            $Bedingung = ' and sbk_abgeglichen = 0 ';
            $Bedingung .= $SBA->ErstelleSBKBedingung($SuchParam);
            if ($Bedingung != '')
            {
                $SQL .= ' WHERE ' . substr($Bedingung, 4);
            }
            $MaxDS = 1;
            $ZeilenProSeite=1;
            $Block = 1;
            // Zum Bl�ttern in den Daten
            if (isset($_REQUEST['Block']))
            {
                if (! isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x']))
                {
                    $Block = $Form->Format('N0', $_REQUEST['Block'], false);
                }
            }
            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
            $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
            
            $rsSBK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SBT'));
            $Form->DebugAusgabe(1, $SQL);          
            // Spaltenbreiten f�r Listenansicht
            $FeldBreiten = array();
            //$FeldBreiten['ChkBox'] = 20;
            $FeldBreiten['Umsatztag'] = 140;
            $FeldBreiten['Safebag'] = 120;
            $FeldBreiten['FilNr'] =70;
            $FeldBreiten['Betrag'] = 120;
            $FeldBreiten['Abholtag'] = 150;
            if(($Recht32000&8)>0)
            {
            	$FeldBreiten['Entsorgt'] = 50;
            }
            //$FeldBreiten['Kfz'] = 140;
            //$FeldBreiten['Name'] = 250;
            //$FeldBreiten['TuevArt'] = 230;
            //$FeldBreiten['Nachverfolgung']=20;
            $Gesamtbreite = 0;
            foreach ($FeldBreiten as $value)
            {
                $Gesamtbreite += $value + 4.5;
            }
           // $Gesamtbreite -= ;            
            
            $Form->ZeileStart();
            // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
            //$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
            // �berschrift der Listenansicht mit Sortierungslink: Kassendatum
            $Link = './safebag_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&SBKSort=SBK_FIL_ID'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_FIL_ID'], $FeldBreiten['FilNr'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './safebag_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&SBKSort=SBK_DATUM'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_DATUM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_UMSATZTAG'], $FeldBreiten['Umsatztag'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Name
            $Link = './safebag_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&SBKSort=SBK_SAFEBAGNR'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_SAFEBAGNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_SAFEBAGNR'], $FeldBreiten['Safebag'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
            $Link = './safebag_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&SBKSort=SBK_BETRAG'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_BETRAG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_SOLLBETRAG'], $FeldBreiten['Betrag'], '', $Link);            
            $Link = './safebag_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&SBKSort=LISTE'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='LISTE'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');  
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['Abholtag'], $FeldBreiten['Abholtag'], '', $Link,'','C');
            if(($Recht32000&8)>0)
            {
            	$Link = './safebag_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&SBKSort=ENTSORGT'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='ENTSORGT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['ENTSORGT'], $FeldBreiten['Entsorgt'], '', $Link,'','L');
            }
            // �berschrift der Listenansicht mit Sortierungslink: T�V-Art
            //$Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_TUEVART'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_TUEVART'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevArt'], $FeldBreiten['TuevArt'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: FilNr
            //$Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_FIL_ID'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['FilId'], $FeldBreiten['FilNr'], '', $Link);
            //$Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_DUPLIKAT'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_DUPLIKAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            //$Form->Erstelle_Liste_Ueberschrift('D', $FeldBreiten['Nachverfolgung'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: T�V-Nr
            //$Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl&TKDSort=TKD_TUEVNR'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_TUEVNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevNr'], $FeldBreiten['TuevNr'], '', $Link);            
            $Form->ZeileEnde();
            
            if($rsSBK->AnzahlDatensaetze() > 12)
            {
        	    $Form->SchreibeHTMLCode('<div id=scrKasseOffen style="width: 100%; height: 250px;  overflow-y: scroll;">');  
            }
            $DS = 0;	// f�r Hintergrundfarbumschaltung
            while(! $rsSBK->EOF())
            {
            	$Form->ZeileStart();
                
                $Link = ((($Recht32000 & 1) == 1)? './safebag_Main.php?cmdAktion=Kasse&SBK_KEY=0'.$rsSBK->FeldInhalt('SBK_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:''): '');
                //$Form->Erstelle_Checkbox('TKD_CHK_' . $rsTKD->FeldInhalt('TKD_KEY'),'',$FeldBreiten['ChkBox'], true,$rsTKD->FeldInhalt('TKD_KEY'));                
                $TTT = $rsSBK->FeldInhalt('SBK_FIL_ID');
                $Form->Erstelle_ListenFeld('SBK_FIL_ID', $rsSBK->FeldInhalt('SBK_FIL_ID'), 0, $FeldBreiten['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $Form->Format('DU', $rsSBK->FeldInhalt('SBK_DATUM'));
                $Form->Erstelle_ListenFeld('SBK_DATUM', $Form->Format('D', $rsSBK->FeldInhalt('SBK_DATUM')), 0, $FeldBreiten['Umsatztag'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsSBK->FeldInhalt('SBK_SAFEBAGNR');
                $Form->Erstelle_ListenFeld('SBK_SAFEBAGNR', $rsSBK->FeldInhalt('SBK_SAFEBAGNR'), 0, $FeldBreiten['Safebag'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $rsSBK->FeldInhalt('SBK_BETRAG');
                $Form->Erstelle_ListenFeld('SBK_BETRAG', $rsSBK->FeldInhalt('SBK_BETRAG'), 0, $FeldBreiten['Betrag'], false, ($DS%2), '', $Link, 'T', 'R', $TTT);
                $TTT = $rsSBK->FeldInhalt('LISTE');
                $Form->Erstelle_ListenFeld('SBT_TAGE', $rsSBK->FeldInhalt('LISTE'), 0, $FeldBreiten['Abholtag'], false, ($DS%2), '', $Link, 'T', 'C', $TTT);
                if(($Recht32000&8)>0)
                {
	                if ($rsSBK->FeldInhalt('ENTSORGT') == '1')
	                {
	                	$Form->Erstelle_HinweisIcon("icon_geldkasten_gruen", $FeldBreiten['Entsorgt'],($DS%2),'','horizontal-align:center;');
	                }
	                else
	                {
	                	$Form->Erstelle_HinweisIcon("icon_geldkasten_rot", $FeldBreiten['Entsorgt'],($DS%2),'','horizontal-align:center;');
	                } 
                }  
                $Form->ZeileEnde();
                $DS++;
                $rsSBK->DSWeiter();
            }
            if(!($Recht32000&8)>0)
            {
            	$Gesamtbreite += 4;
            }
            $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	    
            if($rsSBK->AnzahlDatensaetze() > 12)
            {
            	$Form->SchreibeHTMLCode('</div>') ;
            }
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['SBI']['SummeGesamtDS'].':', $Gesamtbreite-142, 'font-weight:bolder;');
            $Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsSBK->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0', $DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), 130, 'font-weight:bolder;','','','R');
            $Form->ZeileEnde();            
                      
            $Link = './abgleich_Main.php?cmdAktion=Kasse&txtANSICHT=Auswahl'.'&SBKSort='.$Sort;        // f�r N�chste/Vorherige Seite schalten (href im Framwork) (=> mit txt damit immer die gleiche REQUEST-Var ankommt, wegen HiddenFeld)
            $Form->Erstelle_HiddenFeld('ANSICHT', 'Auswahl');   
            $Form->Erstelle_HiddenFeld('SBKSort', $Sort);                    // f�r direkte Auswahl der Seite
            //$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
        }
        elseif ($AnsichtZuordnung)
        {   
        	$SQL = 'select sbk.*,fil_bez '.
        			'from sbkassendaten sbk '.
        			'inner join filialen on sbk_fil_id = fil_id ';
        	if 	(count($AWIS_KEYs1)==1)
        	{
        		$DB->SetzeBindevariable('SBK', 'var_N0_sbk_key', $AWIS_KEYs1[0], awisDatenbank::VAR_TYP_GANZEZAHL);
        		$SQL.='WHERE sbk.sbk_key = :var_N0_sbk_key';
        	}
        	else
        	{
        		$SQL.='WHERE sbk.sbk_key in (';
        		$i=0;
        		foreach ($AWIS_KEYs1 as $key => $value)
        		{
        			++$i;
        			$DB->SetzeBindevariable('SBK', 'var_N0_sbk_key_'.$i, $value, awisDatenbank::VAR_TYP_GANZEZAHL);
        			$SQL .=':var_N0_sbk_key_'.$i.', ';
        		}
        		$SQL = substr($SQL, 0, -2);
        		$SQL .= ')';
        	}
        	
            $Form->DebugAusgabe(1, $SQL);        
            
            $rsSBK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SBK'));
            
            $SBA->Label_Abgleich($rsSBK->AnzahlDatensaetze());
            
            while(! $rsSBK->EOF())
            {
                $SBA->Felder_AbgleichSBK($rsSBK);
                $rsSBK->DSWeiter();
                
                if (! $rsSBK->EOF())
                {
                    $Form->Trennzeile('O');
                }
            }                
            foreach ($AWIS_KEYs1 as $key => $value)
            {
                $Form->Erstelle_HiddenFeld('SBK_CHK_' . $value, $value);    // AWIS_KEYs1 f�r n�chste Seite merken
            }  

            
            $Form->Erstelle_HiddenFeld('SBK_KEY', $value);
            $Form->Erstelle_HiddenFeld('Safebagnr', $SuchParam['Safebag']);
            $Form->Erstelle_HiddenFeld('FilId', $SuchParam['FilId']);
            $Form->Erstelle_HiddenFeld('DatumVon', $SuchParam['DatumVon']);
            $Form->Erstelle_HiddenFeld('DatumBis', $SuchParam['DatumBis']);
            
            $Form->Trennzeile('L');
            //$TAG->Feld_SelectAbgleicharten();
            $SBA->Feld_Bemerkung();
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel('', awis_abgleich_funktionen::_LabelBreite);
            if(($Recht32000&8)>0)
            {
            	$Form->Schaltflaeche('submit', 'cmdSpeichern_SK', '', '', $AWISSprachKonserven['SBI']['MASonstiges'], '');
            }
            $Form->Schaltflaeche('submit', 'cmdSpeichern_SG', '', '', $AWISSprachKonserven['SBI']['MAGeoeffnet'], '');
            if(($Recht32000&8)>0)
            {
            	$Form->Schaltflaeche('submit', 'cmdSpeichern_N', '', '', $AWISSprachKonserven['SBI']['MANachverfolgung'], '');
            	$Form->Schaltflaeche('submit', 'cmdSpeichern_SD', '', '', $AWISSprachKonserven['SBI']['MADiebstahl'], '');
            }
            $Form->ZeileEnde();
        }
    }
    
	$Form->Formular_Ende();	    
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht32000&1) == 1)
    and ($AnsichtSuche))
	{	
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}    
    if ((($Recht32000&1) == 1) 
    and ($AnsichtZuordnung))
	{	        
        $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'N');
    }
    
    if ((($Recht32000&4) == 4)
    		and ($AnsichtZuordnung))
    {
    $Form->Schaltflaeche('image', 'cmdMail','', '/bilder/cmd_mail.png', $AWISSprachKonserven['SBI']['MailSenden'], 'M', '', 27, 27, '_blank');
    }
    
    if ($AnsichtAuswahl)
    {
    	//var_dump($SuchParam['DatumVon']);
    	//var_dump($SuchParam['DatumBis']);
    	$Param = '&Filiale='.$SuchParam['FilId'];
    	$Param .= '&UmsVon='.$SuchParam['DatumVon'];
    	$Param .= '&UmsBis='.$SuchParam['DatumBis'];
    	$Param .= '&Safebag='.$SuchParam['Safebag'];
    	$Param .= '&Entsorgt='.$SuchParam['Entsorgt'];
    	//$SuchParam['SuchenInKey'] = awis_abgleich_funktionen::_SuchenInTGD;             // Schalter auf TGD-Tabelle setzen
    	$LinkPDF = '/berichte/drucken.php?XRE=49&ID='.base64_encode($Param);
    	$Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
    }
    
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>