SET DEFINE OFF;
INSERT INTO MAILVERSANDTEXTE (MVT_BEREICH, MVT_SPRACHE, MVT_BETREFF,MVT_TEXT,MVT_GUELTIGAB,MVT_USER,MVT_USERDAT) values ('SET_HOTEL_ABBESTELLT','DE',
'[STORNO HOTELRESERVIERUNG] #Vorname Nachname# f�r Seminar "#Seminartitel#" (#Terminnummer#) (KW#KW#)',
'<div style="font-family: Arial; font-size: 10pt">Sehr geehrte Damen und Herren, <br>
<br>
bitte stornieren Sie die Hotelreservierung f�r Herr/Frau #Vorname Nachname# (#Personalnummer#, #Filialnummer#) f�r das Seminar "#Seminartitel#" (#Terminnummer#) in/im <br>
<br>
#Lokationsname#<br>
#Schulungsort#<br>
#Strasse#<br>
#PLZ# #Ort#<br>
Urspr�ngliche Reservierung vom #HOTELDatumvonALT# bis #HOTELDatumbisALT# (KW#KWHotelALT#).<br>
<br>
Vielen Dank.
</div>','01.01.1970','ADMIN',SYSDATE);


SET DEFINE OFF;
INSERT INTO MAILVERSANDTEXTE (MVT_BEREICH, MVT_SPRACHE, MVT_BETREFF,MVT_TEXT,MVT_GUELTIGAB,MVT_USER,MVT_USERDAT) values ('SET_HOTEL_ZEIT_AENDERUNG','DE',
'[�NDERUNG RESERVIERUNG] #Vorname Nachname# f�r Seminar "#Seminartitel#" (#Terminnummer#) (KW#KW#)',
'<div style="font-family: Arial; font-size: 10pt">Sehr geehrte Damen und Herren, <br>
<br>
f�r Herr/Frau #Vorname Nachname# (#Personalnummer#, #Filialnummer#) gibt es eine �nderung des Reservierungszeitraums f�r das Seminar "#Seminartitel#" (#Terminnummer#) in/im<br>
<br>
#Lokationsname#<br>
#Schulungsort#<br>
#Strasse#<br>
#PLZ# #Ort#<br>
<br>
Urspr�ngliche Reservierung vom #HOTELDatumvonALT# bis #HOTELDatumbisALT# (KW#KWHotelALT#).<br>
<b>Neuer Reservierungszeitraum vom #HOTELDatumvon# bis #HOTELDatumbis# (KW#KWHotel#)</b><br>
<br>
Bitte �ndern Sie die Reservierung f�r den entsprechenden �bernachtungszeitraum und leiten die Reservierungsbest�tigung an den Mitarbeiter weiter.<br>
<br>
Vielen Dank.
</div>','01.01.1970','ADMIN',SYSDATE);

SET DEFINE OFF;
UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Folgende Platzhalter werden ersetzt: <br>
<br><br>
<b>#Vorname Nachname#</b> Vorname und Nachname des Teinehmers<br>
<b>#Seminarnummer#</b> Nummer es Seminars (z.B. 2018-01-15)<br>
<b>#Terminnummer#</b> Seminarnummer+Seminarzusatz (z.B. 2018-01-15-0001)<br>
<b>#Seminartitel#</b> Titel des Seminars<br>
<b>#Personalnummer#</b> Personalnummer des Teilnehmers<br>
<b>#Filialnummer#</b> Filialnummer des Teilnehmers<br>
<b>#Datumvon#</b> Beginn des Seminars<br>
<b>#Datumbis#</b> Ende des Seminars<br>
<b>#KW#</b> Kalenderwoche des Seminars<br>
<b>#Schulungsort#</b> Ort (z.B. A.T.U Academy) des Seminars<br>
<b>#Lokationsname#</b> Name des Schulungsortes (z.B. A.T.U Academy)<br>
<b>#Region#</b> Region des Teilnehmers<br>
<b>#SET_KEY#</b> Key zum Datensatz (F�r Links)<br>
<b>#Strasse#</b> Stra�e des Seminars<br>
<b>#Ort#</b> Ort des Seminars<br>
<b>#PLZ#</b> Postleitzahl des Seminars<br>
<b>#TauschText#</b> Eingetragener Freitext des Teilnehmers beim Teilnehmertausch<br>
<b>#HOTELDatumvon#</b> Anreise des Teilnehmers an Hotel<br>
<b>#HOTELDatumbis#</b> Abreise des Teilnehmers von Hotel<br>
<b>#HOTELDatumvonALT#</b> Alte Anreise des Teilnehmers an Hotel (bei �nderungen)<br>
<b>#HOTELDatumbisALT#</b> Alte Abreise des Teilnehmers von Hotel (bei �nderungen)<br>
<b>#KWHotel#</b> Kalenderwoche der Hotelanreise<br>
<b>#KWHotelALT#</b> Alte Kalenderwoche der Hotelanreise (bei �nderungen)<br>
<b>#LINK#</b> Link zur AWIS-Umgebung (Entwick, Staging, Prod) <br>
<b>#LINKAFB#</b> Link zur Anfahrtsbeschreibung <br>
<b>#LINKLZB#</b> Link zum Lernzielblatt <br>
<b>#LINKTH#</b> Link zum Hotel des Teilnehmers<br>
<br>

Leerzeilen m�ssen �ber das HTML-Tag "&lt;br&gt;" eingef�gt werden. ' WHERE XTX_KENNUNG = 'SEP_MAILERSETZUNG' AND XTX_BEREICH = 'SEP';