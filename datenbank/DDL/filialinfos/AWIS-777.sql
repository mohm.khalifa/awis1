INSERT INTO "AWIS"."WEBMASKENREGISTER" (XRG_ID, XRG_XWM_ID, XRG_BEZEICHNUNG, XRG_BESCHRIFTUNG, XRG_AKTION, XRG_SEITE, XRG_BEMERKUNG, XRG_XRC_ID, XRG_RECHTESTUFE, XRG_SORTIERUNG, XRG_NEUEHILFE)
VALUES ('50028', '5001', 'Filialkompetenzen', 'Filialkompetenzen', './filialinfos_Main.php?cmdAktion=Details&Seite=Filialkompetenzen', './filialinfos_Details_Filialkompetenzen.php', 'Kompetenzen der Filiale', '5020', '1', '18', '0');

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_USERDAT)
VALUES ('reg_50028', 'Register', 'Filialkompetenzen', 'Filialkompetenzen', 'Filialkompetenzen', 'Filialkompetenzen', 'Filialkompetenzen', TO_DATE('2018-10-25 14:54:01', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_USERDAT)
VALUES (
'ttt_50028',
'Register',
'Tooltipptext',
'spezielle Kompetenzen der Filiale',
'spezielle Kompetenzen der Filiale',
'spezielle Kompetenzen der Filiale',
'spezielle Kompetenzen der Filiale',
TO_DATE('2018-10-25 15:04:17', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT)
VALUES ('FIK_ALLE_ZUGEORDNET', 'FIK', 'Wenn einer Filiale bereits alle Kompetenzen zugeordnet wurden', 'Dieser Filiale wurden bereits alle m�glichen Kompetenzen zugeordnet.', TO_DATE('2018-10-30 12:46:55', 'YYYY-MM-DD HH24:MI:SS'));

COMMIT;
