<?php

class SuchAnfrageArtikel extends Serialize
{

    /**
     * @var int $EinspeiserNr
     */
    protected $EinspeiserNr = null;

    /**
     * @var string $EinspeiserArtikelNr
     */
    protected $EinspeiserArtikelNr = null;

    /**
     * @var int $AnfrageMenge
     */
    protected $AnfrageMenge = null;

    /**
     * @param int $EinspeiserNr
     * @param int $AnfrageMenge
     */
    public function __construct($EinspeiserNr,$EinspeiserArtikelNr, $AnfrageMenge)
    {
      $this->EinspeiserNr = $EinspeiserNr;
      $this->AnfrageMenge = $AnfrageMenge;
      $this->EinspeiserArtikelNr = $EinspeiserArtikelNr;
    }

    /**
     * @return int
     */
    public function getEinspeiserNr()
    {
      return $this->EinspeiserNr;
    }

    /**
     * @param int $EinspeiserNr
     * @return SuchAnfrageArtikel
     */
    public function setEinspeiserNr($EinspeiserNr)
    {
      $this->EinspeiserNr = $EinspeiserNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getEinspeiserArtikelNr()
    {
      return $this->EinspeiserArtikelNr;
    }

    /**
     * @param string $EinspeiserArtikelNr
     * @return SuchAnfrageArtikel
     */
    public function setEinspeiserArtikelNr($EinspeiserArtikelNr)
    {
      $this->EinspeiserArtikelNr = $EinspeiserArtikelNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getAnfrageMenge()
    {
      return $this->AnfrageMenge;
    }

    /**
     * @param int $AnfrageMenge
     * @return SuchAnfrageArtikel
     */
    public function setAnfrageMenge($AnfrageMenge)
    {
      $this->AnfrageMenge = $AnfrageMenge;
      return $this;
    }

}
