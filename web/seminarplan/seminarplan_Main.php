<?php

if((isset($_POST['cmdSenden_x'])) and (isset($_POST['txtMailEmpfaenger'])) and (!isset($_GET['SET_MAIL_KEY'])) and (!isset($_POST['txtSET_MAIL_KEY'])) and (!isset($_GET['ANABMELDUNG'])) and !isset($_GET['SET_ABSAGEBEST']))
{
    include('./seminarplan_termin.php');
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular


$AWISVersion = '';
if(isset($_GET['cmdAktion']) and $_GET['cmdAktion'] == 'Emails'){
    $AWISVersion = 3;
}

try 
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei($AWISVersion) .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Seminarplan');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new AWISFormular($AWISVersion);
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Seminarplan'].'</title>';
?>
</head>
<body>
<script src="/jquery_ie11.js"></script>
<script src="/jquery.scrollUp.min.js"></script>
<script src="/popup.js"></script>
<script src="/formularBereich.js"></script>
<script src="/jquery.tooltipster.js"></script>
<script src="/jquery-ui.js"></script>
<script src="seminarplan.js"></script>
<link rel=stylesheet type=text/css href="/css/jquery-ui.css">
<link rel=stylesheet type=text/css href="/css/tooltipster.css">
<link rel=stylesheet type=text/css href="/css/tooltipster-punk.css">
<link rel=stylesheet type=text/css href="/css/popup.css">
<script>
    function setzeFarbe() {

    }
</script>

<?php
include ("awisHeader$AWISVersion.inc");	// Kopfzeile

try 
{
	$Form = new awisFormular(3);
	
	if($AWISBenutzer->HatDasRecht(4200)==0)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',1,"200907281613");
		die('Keine Rechte');
	}
	
	$Register = new awisRegister(4200);
	$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));		
	
	if($AWISCursorPosition!=='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';			
	}
}
catch (Exception $ex)
{
	echo  $ex->getMessage();
}
?>
</body>
</html>