<?php
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once('awisDatenbank.inc');

global $AWIS_KEY1;
global $AWIS_KEY2;
global $RUECKFUEHRUNGSTYP;
global $LAND;

try
{


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_keinegueltigeATUNR');
$TextKonserven[]=array('Fehler','err_keinegueltigeFILIALE');
$TextKonserven[]=array('Fehler','err_keingueltigesDatum');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','WirklichAendern');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Wort','FilialenAnlegen');

$Form = new awisFormular();

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

//***********************************************
// R�ckf�hrungsaufforderungen
//***********************************************
$Form->DebugAusgabe(1,$_POST);

if(isset($_POST['txtRFA_KEY']))
{
	$AWIS_KEY1=$_POST['txtRFA_KEY'];
}
if(isset($_POST['txtRFP_KEY']))
{
	$AWIS_KEY2=$_POST['txtRFP_KEY'];
}
if(isset($_POST['txtRFM_KEY']))
{
	$AWIS_KEY3=$_POST['txtRFM_KEY'];
}
if(isset($_POST['txtRFA_TYP']))
{
	$RUECKFUEHRUNGSTYP=$_POST['txtRFA_TYP'];
}

$Form->DebugAusgabe(1,$AWIS_KEY1);
$Form->DebugAusgabe(1,$AWIS_KEY2);

$Felder = $Form->NameInArray($_POST, 'txtRFA_',1,1);

if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('RFA','RFA_%');
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	$FeldListe='';
	$SQL = '';

	if(intval($_POST['txtRFA_KEY'])==0)
	{
		$Speichern = true;

		if($Speichern)
		{
			// Daten auf Vollst�ndigkeit pr�fen
			$Fehler = '';
			$Pflichtfelder = array('RFA_DATUM');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Datum muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFA'][$Pflichtfeld].'<br>';
				}
			}

			$ANFDATUM = $Form->Format('D',$_POST['txtRFA_DATUM'],false);
			$ANFDATUMFELDER = explode('.', $ANFDATUM);

			if (!checkdate($ANFDATUMFELDER[1],$ANFDATUMFELDER[0],$ANFDATUMFELDER[2]))
			{
					$Fehler .= $TXT_Speichern['Fehler']['err_keingueltigesDatum'].'<br>';
			}
			$Form->DebugAusgabe(1,$ANFDATUM, $ANFDATUMFELDER);

			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->SchreibeHTMLCode('<span class=HinweisText>'.$Fehler.'</span>');
			}
			else
			{
				$ANFNR = $ANFDATUMFELDER[2].''.$ANFDATUMFELDER[1].''.$ANFDATUMFELDER[0];

				$rsLFDNR = $DB->RecordSetOeffnen('SELECT NVL(MAX(RFA_LFDNR),0) as MAXLFDNR FROM RUECKFUEHRUNGSANFORDERUNGEN WHERE RFA_NR='. $DB->FeldInhaltFormat('Z',$ANFNR));
				$LFDNR = $DB->FeldInhaltFormat('Z',$rsLFDNR->FeldInhalt('MAXLFDNR'))+1;

				$Fehler = '';
				$SQL = 'INSERT INTO RUECKFUEHRUNGSANFORDERUNGEN';
				$SQL .= '(RFA_NR,RFA_LFDNR,RFA_DATUM,RFA_BEMERKUNG,RFA_TYP,RFA_RFS_KEY';
				$SQL .= ',RFA_USER,RFA_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('Z',$ANFNR,false);
				$SQL .= ',' . $DB->FeldInhaltFormat('Z',$LFDNR,false);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtRFA_DATUM'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtRFA_BEMERKUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N',$_POST['txtRFA_TYP'],true);
				$SQL .= ',0';
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$Form->DebugAusgabe(1,$SQL);

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim L�schen',200809181417,$SQL,2);
				}
				$SQL = 'SELECT seq_RFA_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsRFA = $DB->RecordSetOeffnen('SELECT * FROM rueckfuehrungsanforderungen WHERE RFA_key=' . $_POST['txtRFA_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=$DB->FeldInhaltFormat($rsRFA->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
				$WertAlt=$DB->FeldInhaltFormat($rsRFA->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
				$WertDB=$DB->FeldInhaltFormat($rsRFA->FeldInfo($FeldName,'TypKZ'),$rsRFA->FeldInhalt($FeldName),true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}

						if ($FeldName == 'RFA_DATUM')
						{
							$Fehler = '';
							$Pflichtfelder = array('RFA_DATUM');
							foreach($Pflichtfelder AS $Pflichtfeld)
							{
								if($_POST['txt'.$Pflichtfeld]=='')	// Datum muss angegeben werden
								{
									$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFA'][$Pflichtfeld].'<br>';
								}
							}

							$ANFDATUM = $Form->Format('D',$_POST['txtRFA_DATUM'],false);
							$ANFDATUMFELDER = explode('.', $ANFDATUM);

							if (!checkdate($ANFDATUMFELDER[1],$ANFDATUMFELDER[0],$ANFDATUMFELDER[2]))
							{
									$Fehler .= $TXT_Speichern['Fehler']['err_keingueltigesDatum'].'<br>';
							}
							$Form->DebugAusgabe(1,$ANFDATUM, $ANFDATUMFELDER);

							// Wurden Fehler entdeckt? => Speichern abbrechen
							if($Fehler!='')
							{
								$Form->SchreibeHTMLCode('<span class=HinweisText>'.$Fehler.'</span>');
								die();
							}

							$ANFNR = $ANFDATUMFELDER[2].''.$ANFDATUMFELDER[1].''.$ANFDATUMFELDER[0];

							$rsLFDNR = $DB->RecordSetOeffnen('SELECT NVL(MAX(RFA_LFDNR),0) as MAXLFDNR FROM RUECKFUEHRUNGSANFORDERUNGEN WHERE RFA_NR='. $DB->FeldInhaltFormat('Z',$ANFNR));
							$LFDNR = $DB->FeldInhaltFormat('Z',$rsLFDNR->FeldInhalt('MAXLFDNR'))+1;

							$FeldListe .= ', RFA_NR='.$DB->FeldInhaltFormat('Z',$ANFNR,false);
							$FeldListe .= ', RFA_LFDNR='.$DB->FeldInhaltFormat('Z',$LFDNR,false);
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsRFA->FeldInhalt('RFA_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			$Form->Fehler_Anzeigen(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE rueckfuehrungsanforderungen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1)';
			$SQL .= ', RFA_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', RFA_userdat=sysdate';
			$SQL .= ' WHERE RFA_key=0' . $_POST['txtRFA_KEY'] . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809191133,$SQL,2);
			}
		}
	}
}


//***********************************************
// R�ckfuehrungspositionen
//***********************************************
$Felder = $Form->NameInArray($_POST, 'txtRFP_',1,1);
if($Felder!='')
{
	$Fehler = '';
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('RFP','RFP_%');
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	$FeldListe='';
	$SQL = '';

	if($AWIS_KEY2=='0')
	{
		$Speichern = true;

		if($Speichern)
		{

			// Daten auf Vollst�ndigkeit pr�fen
			$Pflichtfelder = array('RFP_AST_ATUNR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFP'][$Pflichtfeld].'<br>';
				}
			}

			$SQL = 'SELECT *';
			$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR = \''.$_POST['txtRFP_AST_ATUNR'].'\' AND BITAND(AST_IMQ_ID,2)=2';

			$rsAST = $DB->RecordSetOeffnen($SQL);
			if($rsAST->AnzahlDatensaetze()==0)
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_keinegueltigeATUNR'].'<br>';
			}

			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->SchreibeHTMLCode('<span class=HinweisText>'.$Fehler.'</span>');
			}
			else
			{
				$Fehler = '';

				if (intval($RUECKFUEHRUNGSTYP) == 1)
				{
					if(intval($_POST['txtRFP_KEY'])==0)
					{
						$SQL = 'INSERT INTO RUECKFUEHRUNGSANFORDERUNGENPOS';
						$SQL .= '(RFP_AST_ATUNR,RFP_RFA_KEY,RFP_GESAMTMENGE,RFP_MINDESTBESTAND, RFP_BEMERKUNG';
						$SQL .= ',RFP_USER,RFP_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtRFP_AST_ATUNR'],true);
						$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtRFP_RFA_KEY'],true);
						$SQL .= ',' . $DB->FeldInhaltFormat('NO',$_POST['txtRFP_GESAMTMENGE'],true);
						$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtRFP_MINDESTBESTAND'],true);
						$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtRFP_BEMERKUNG'],true);
						$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
						$SQL .= ',SYSDATE';
						$SQL .= ')';

						$Form->DebugAusgabe(1,$SQL);

						if($DB->Ausfuehren($SQL)===false)
						{
							throw new awisException('Fehler beim Speichern',200809191136,$SQL,2);
						}
						$SQL = 'SELECT seq_RFP_KEY.CurrVal AS KEY FROM DUAL';
						$rsKey = $DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

						$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';

						$Form->DebugAusgabe(1,$SQL);

						if($DB->Ausfuehren($SQL)===false)
						{
							throw new awisException('Fehler beim Speichern',200810141429,$SQL,2);
						}
					}
				}
				else
				{
					if(intval($_POST['txtRFP_KEY'])==0)
					{
						$SQL = 'INSERT INTO RUECKFUEHRUNGSANFORDERUNGENPOS';
						$SQL .= '(RFP_AST_ATUNR,RFP_RFA_KEY, RFP_BEMERKUNG';
						$SQL .= ',RFP_USER,RFP_USERDAT';
						$SQL .= ')VALUES (';
						$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtRFP_AST_ATUNR'],true);
						$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtRFP_RFA_KEY'],true);
						$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtRFP_BEMERKUNG'],true);
						$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
						$SQL .= ',SYSDATE';
						$SQL .= ')';

						$Form->DebugAusgabe(1,$SQL);

						if($DB->Ausfuehren($SQL)===false)
						{
							throw new awisException('Fehler beim Speichern',200809191313,$SQL,2);
						}
						$SQL = 'SELECT seq_RFP_KEY.CurrVal AS KEY FROM DUAL';
						$rsKey = $DB->RecordSetOeffnen($SQL);
						$AWIS_KEY2=$rsKey->FeldInhalt('KEY');

						$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';

						$Form->DebugAusgabe(1,$SQL);

						if($DB->Ausfuehren($SQL)===false)
						{
							throw new awisException('Fehler beim Speichern',200810141430,$SQL,2);
						}
					}
					else {
						$AWIS_KEY2=$_POST['txtRFP_KEY'];
					}

				}
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsRFP = $DB->RecordSetOeffnen('SELECT * FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_key=' . $AWIS_KEY2 . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=$DB->FeldInhaltFormat($rsRFP->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
				$WertAlt=$DB->FeldInhaltFormat($rsRFP->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
				$WertDB=$DB->FeldInhaltFormat($rsRFP->FeldInfo($FeldName,'TypKZ'),$rsRFP->FeldInhalt($FeldName),true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsRFP->FeldInhalt('RFP_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			$Form->Fehler_Anzeigen(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE RUECKFUEHRUNGSANFORDERUNGENPOS SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', RFP_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', RFP_userdat=sysdate';
			$SQL .= ' WHERE RFP_key=0' . $_POST['txtRFP_KEY'] . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809191333,$SQL,2);
			}

			$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGENPOS SET RFP_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFP_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFP_USERDAT=SYSDATE WHERE RFP_key=0' . $_POST['txtRFP_KEY'] . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809301013,$SQL,2);
			}

			$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $_POST['txtRFA_KEY'] . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809301013,$SQL,2);
			}
		}

	}
}

//***********************************************
// Rueckfuehrungsanfposfilmengen
//***********************************************

$Felder = $Form->NameInArray($_POST, 'txtRFM_',1,1);
if($Felder!='')
{
	$Fehler = '';
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('RFM','RFM_%');
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	$FeldListe='';
	$SQL = '';

	if($AWIS_KEY3=='0' AND isset($_POST['txtRFM_FIL_ID']))
	{
		$Speichern = true;

		if($Speichern)
		{
			if ($RUECKFUEHRUNGSTYP=='2')
			{
				// Daten auf Vollst�ndigkeit pr�fen
				$Pflichtfelder = array('RFM_FIL_ID','RFM_MENGE');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RFM'][$Pflichtfeld].'<br>';
					}
				}

				$SQL = 'SELECT *';
				$SQL .= ' FROM Filialen WHERE FIL_ID = \''.$_POST['txtRFM_FIL_ID'].'\' AND FIL_LAN_WWSKENN<>\'SUI\'';

				$rsAST = $DB->RecordSetOeffnen($SQL);
				if($rsAST->AnzahlDatensaetze()==0)
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_keinegueltigeFILIALE'].'<br>';
				}
			}

			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->SchreibeHTMLCode('<span class=HinweisText>'.$Fehler.'</span>');
			}
			else
			{

				$Fehler = '';

				if(isset($_POST['txtRFM_KEY']) && intval($_POST['txtRFM_KEY'])==0)
				{
					$SQL = 'INSERT INTO RUECKFUEHRUNGSANFPOSFILMENGEN';
					$SQL .= '(RFM_RFP_KEY,RFM_FIL_ID,RFM_MENGE,RFM_RFS_KEY';
					$SQL .= ',RFM_USER,RFM_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $AWIS_KEY2;
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtRFM_FIL_ID'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtRFM_MENGE'],true);
					$SQL .= ',0';
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$Form->DebugAusgabe(1,$SQL);

					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',200809191314,$SQL,2);
					}

					$Form->Erstelle_HiddenFeld('RFMListe',1);

					$SQL = 'UPDATE RUECKFUEHRUNGSANFORDERUNGENPOS SET RFP_GESAMTMENGE=(SELECT SUM(RFM_MENGE) FROM RUECKFUEHRUNGSANFPOSFILMENGEN WHERE RFM_RFP_KEY='.$AWIS_KEY2.') WHERE RFP_KEY='.$AWIS_KEY2;

					$Form->DebugAusgabe(1,$SQL);

					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',200809191315,$SQL,2);
					}

					$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';

					$Form->DebugAusgabe(1,$SQL);

					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',200810141426,$SQL,2);
					}

				}
			}
		}
	}
	elseif(intval($AWIS_KEY3)!=0) 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		if (intval($RUECKFUEHRUNGSTYP) != 1)
		{
			$Felder = $Form->NameInArray($_POST, 'txtRFM_',1,1);
			$Felder = explode(';',$Felder);

			$rsRFM = $DB->RecordSetOeffnen('SELECT * FROM RUECKFUEHRUNGSANFPOSFILMENGEN WHERE RFM_key=' . $_POST['txtRFM_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsRFM->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsRFM->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsRFM->FeldInfo($FeldName,'TypKZ'),$rsRFM->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsRFM->FeldInhalt('RFM_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE RUECKFUEHRUNGSANFPOSFILMENGEN SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', RFM_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', RFM_userdat=sysdate';
				$SQL .= ' WHERE RFM_key=0' . $_POST['txtRFM_KEY'] . '';

				$Form->DebugAusgabe(1,$SQL);

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200809191335,$SQL,2);
				}

				$Form->Erstelle_HiddenFeld('RFMListe',1);

				$SQL = 'UPDATE RUECKFUEHRUNGSANFORDERUNGENPOS';
				$SQL .=' SET RFP_GESAMTMENGE=(SELECT SUM(RFM_MENGE) FROM RUECKFUEHRUNGSANFPOSFILMENGEN WHERE RFM_RFP_KEY='.$_POST['txtRFP_KEY'].'),';
				$SQL .=' RFP_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1)';
				$SQL .=' WHERE RFP_KEY='.$_POST['txtRFP_KEY'];

				$Form->DebugAusgabe(1,$SQL);

				if($DB->Ausfuehren($SQL))
				{
					throw new awisException('Fehler beim Speichern',200809191336,$SQL,2);
				}

				$SQL='UPDATE RUECKFUEHRUNGSANFPOSFILMENGEN SET RFM_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFM_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFM_USERDAT=SYSDATE WHERE RFM_key=0' . $_POST['txtRFM_KEY'] . '';

				$Form->DebugAusgabe(1,$SQL);

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200809301016,$SQL,2);
				}

				$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';

				$Form->DebugAusgabe(1,$SQL);

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200810141426,$SQL,2);
				}
			}
		}

	}
}


//***********************************************
// R�ckfuehrungsfilialen
//***********************************************
$Felder = $Form->NameInArray($_POST, 'txtRFL_',1,1);
if($Felder!='')
{
	

    
    $Fehler = '';
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('RFL','RFL_%');
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	$FeldListe='';
	$SQL = '';
	$FilialEbenenObj = new awisFilialEbenen();

	if(isset($_POST['txtRFL_KEY']))
	{
		if(intval($_POST['txtRFL_KEY'])==0 && !isset($_POST['txtRFL_FIL_ID']))
		{
			$Fehler = '';

			$arrFilialen='';
			$FilialEbeneFilialen=array();

			if(isset($_POST['txtRFL_FILIALEBENE']) && $_POST['txtRFL_FILIALEBENE']!='')
			{
				$arrFilialen=$FilialEbenenObj->FilialenEinerEbene(intval($_POST['txtRFL_FILIALEBENE']),'',awisFilialEbenen::FILIALEBENELISTE_ERGTYP_ARRAY );
				foreach($arrFilialen['XX1_FIL_ID'] as $key => $value)
				{
					$FilialEbeneFilialen[$key]=$value;
				}
			}

			if(isset($_POST['txtRFL_FILIALE']) && $_POST['txtRFL_FILIALE']!='-1')
			{
				array_push($FilialEbeneFilialen,$_POST['txtRFL_FILIALE']);
			}

			if(isset($_POST['txtRFL_FILIALLISTE']) && $_POST['txtRFL_FILIALLISTE']!='')
			{
				$FilialListeText=$_POST['txtRFL_FILIALLISTE'];
				$FilialListeText=preg_replace('/\D/',',',$FilialListeText);
				$FilialListeText=preg_replace('/[,]+/',',',$FilialListeText);
				$arrFilialen=explode(',',$FilialListeText);
				foreach($arrFilialen as $value)
				{
					array_push($FilialEbeneFilialen,$value);
				}
			}

			if(isset($_POST['txtRFL_LAGER']) && $_POST['txtRFL_LAGER']!='-1')
			{
				$SQL='SELECT DISTINCT FIL_ID FROM FILIALEN WHERE FIL_LAGERKZ=\''.$_POST['txtRFL_LAGER'].'\' AND FIL_GRUPPE IS NOT NULL';
				$rsFIL_LAGER=$DB->RecordSetOeffnen($SQL);

				while(!$rsFIL_LAGER->EOF())
				{
					array_push($FilialEbeneFilialen,$rsFIL_LAGER->FeldInhalt('FIL_ID'));
					$rsFIL_LAGER->DSWeiter();
				}
			}
			
			$Filialliste='';

			for($i=0;$i<10;$i++)
			{
				if(array_key_exists($i,$FilialEbeneFilialen))
				{
					$Filialliste=$Filialliste.$FilialEbeneFilialen[$i].', ';
				}
			}
			//-->PG: 10.02.2015 - Franchiser hinzuf�gen
			if(isset($_POST['txtRFL_FRANCHISE']) && $_POST['txtRFL_FRANCHISE'] = 'on')
			{
			    
			    //Franchiser holen.  
			    $SQLFranchise = 'select fil_id from filialen where Fil_gruppe = \'5\'';    
			    $rsFIL_Franchise=$DB->RecordSetOeffnen($SQLFranchise);
			    
			    while(!$rsFIL_Franchise->EOF())
			    {
			        array_push($FilialEbeneFilialen,$rsFIL_Franchise->FeldInhalt('FIL_ID'));
			        $rsFIL_Franchise->DSWeiter();
			    }
			}
			//-->PG: 10.02.2015 - Franchiser hinzuf�gen
			
			$Filialliste=$Filialliste.'...';
           
			$Form->Hinweistext($TXT_Speichern['Wort']['FilialenAnlegen'].$Filialliste);

			foreach($FilialEbeneFilialen as $value)
			{
				$SQL = 'MERGE INTO RUECKFUEHRUNGSANFFILIALEN RFL ';
				$SQL .= 'USING (SELECT * FROM ';
				$SQL .= '(SELECT '.$DB->FeldInhaltFormat('N0',$value,true).' AS FIL_ID, '.$DB->FeldInhaltFormat('N0',$_POST['txtRFL_RFA_KEY'],true).' AS RFA_KEY FROM DUAL) ';
				$SQL .= 'WHERE FIL_ID IN (SELECT FIL_ID FROM FILIALEN WHERE FIL_LAN_WWSKENN<>\'SUI\')) INS ';
				$SQL .= 'ON (RFL.RFL_FIL_ID=INS.FIL_ID AND RFL.RFL_RFA_KEY=INS.RFA_KEY) ';
				$SQL .= 'WHEN MATCHED THEN UPDATE SET RFL.RFL_USER=\'' . $AWISBenutzer->BenutzerName() . '\', RFL.RFL_USERDAT=SYSDATE ';
				$SQL .= 'WHEN NOT MATCHED THEN INSERT (RFL.RFL_FIL_ID, RFL.RFL_RFA_KEY, RFL.RFL_USER, RFL.RFL_USERDAT) ';
				$SQL .= 'VALUES (INS.FIL_ID, INS.RFA_KEY, \'' . $AWISBenutzer->BenutzerName() . '\', SYSDATE)';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200809191339,$SQL,2);
				}
			}
			$AWIS_KEY2=0; //Liste anzeigen

			$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200810141434,$SQL,2);
			}
		}

		if(intval($_POST['txtRFL_KEY'])==0 and isset($_POST['txtRFL_FIL_ID']) and $_POST['txtRFL_FIL_ID'] !='0')
		{
			$Fehler = '';

			$SQL = 'INSERT INTO RUECKFUEHRUNGSANFFILIALEN';
			$SQL .= ' (RFL_FIL_ID,RFL_RFA_KEY';
			$SQL .= ' ,RFL_USER,RFL_USERDAT) values(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtRFL_FIL_ID'],true);
			$SQL .= ', ' . $DB->FeldInhaltFormat('N0',$_POST['txtRFL_RFA_KEY'],true);
			$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ', SYSDATE)';
			$Form->DebugAusgabe(1,$SQL);


			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809191339,$SQL,2);
			}
			$AWIS_KEY2=0; //Liste anzeigen
			$LAND=$_POST['txtLAND'];
			$Form->DebugAusgabe(1,$_POST);

			$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200810141435,$SQL,2);
			}
		}

	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsRFL = $DB->RecordSetOeffnen('SELECT * FROM RUECKFUEHRUNGSANFFILIALEN WHERE RFL_key=' . $_POST['txtRFL_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=$DB->FeldInhaltFormat($rsRFL->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
				$WertAlt=$DB->FeldInhaltFormat($rsRFL->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
				$WertDB=$DB->FeldInhaltFormat($rsRFL->FeldInfo($FeldName,'TypKZ'),$rsRFL->FeldInhalt($FeldName),true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsRFP->FeldInfo('RFL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			$Form->Fehler_Anzeigen(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE RUECKFUEHRUNGSANFFILIALEN SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', RFL_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', RFL_userdat=sysdate';
			$SQL .= ' WHERE RFL_key=0' . $_POST['txtRFL_KEY'] . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200809191341,$SQL,2);
			}

			$SQL='UPDATE RUECKFUEHRUNGSANFORDERUNGEN SET RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=1), RFA_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFA_USERDAT=SYSDATE WHERE RFA_key=0' . $AWIS_KEY1 . '';

			$Form->DebugAusgabe(1,$SQL);

			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Speichern',200810141436,$SQL,2);
			}
		}
		$AWIS_KEY2 = 0; //Liste anzeigen
	}
}

//*************************************************
// Anforderuung - Filiale bearbeitet "RFC"
//*************************************************

if (isset($_POST['txtRFC_LISTE']))
{
	$Felder = explode(';',$Form->NameInArray($_POST,'txtRFC_Wert',1,1));
	$FelderOld = explode(';',$Form->NameInArray($_POST,'oldRFC_Wert',1,1));

	$SQL = '';

	$Filialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_ARRAY);

	foreach ($FelderOld AS $FeldOld)
	{
		if(!isset($_POST['txt'.substr($FeldOld,3)]))
		{
			$KeyOld=intval(substr($FeldOld,12));
			$WertOld='off';

			foreach ($Filialen as $value)
			{
				$SQL = 'MERGE INTO RUECKFUEHRUNGSCHECKS RFC ';
				$SQL.= 'USING (SELECT '.$KeyOld.' AS  RFC_RFA_KEY, '.$value.' as RFC_FIL_ID FROM DUAL) INS ';
				$SQL.= 'ON (RFC.RFC_RFA_KEY=INS.RFC_RFA_KEY AND RFC.RFC_FIL_ID=INS.RFC_FIL_ID) ';
				$SQL.= 'WHEN MATCHED THEN UPDATE SET RFC.RFC_WERT=\''.$WertOld.'\', RFC.RFC_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFC.RFC_USERDAT=SYSDATE ';
				$SQL.= 'WHEN NOT MATCHED THEN INSERT (RFC.RFC_RFA_KEY, RFC.RFC_FIL_ID, RFC.RFC_WERT, RFC.RFC_USER, RFC.RFC_USERDAT) ';
				$SQL.= 'VALUES (INS.RFC_RFA_KEY, INS.RFC_FIL_ID, \''.$WertOld.'\', \''.$AWISBenutzer->BenutzerName(1).'\', SYSDATE)';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200811050957,$SQL,2);
				}
			}
		}
	}


	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,12));

				foreach ($Filialen as $value)
				{
					$SQL = 'MERGE INTO RUECKFUEHRUNGSCHECKS RFC ';
					$SQL.= 'USING (SELECT '.$Key.' AS  RFC_RFA_KEY, '.$value.' as RFC_FIL_ID FROM DUAL) INS ';
					$SQL.= 'ON (RFC.RFC_RFA_KEY=INS.RFC_RFA_KEY AND RFC.RFC_FIL_ID=INS.RFC_FIL_ID) ';
					$SQL.= 'WHEN MATCHED THEN UPDATE SET RFC.RFC_WERT=\''.$_POST[$Feld].'\', RFC.RFC_USER=\''.$AWISBenutzer->BenutzerName(1).'\', RFC.RFC_USERDAT=SYSDATE ';
					$SQL.= 'WHEN NOT MATCHED THEN INSERT (RFC.RFC_RFA_KEY, RFC.RFC_FIL_ID, RFC.RFC_WERT, RFC.RFC_USER, RFC.RFC_USERDAT) ';
					$SQL.= 'VALUES (INS.RFC_RFA_KEY, INS.RFC_FIL_ID, \''.$_POST[$Feld].'\', \''.$AWISBenutzer->BenutzerName(1).'\', SYSDATE)';

					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',200810201432,$SQL,2);
					}
				}
			}
		}
	}
}


//*************************************************
// R�ckfuehrungstyp in Liste (Artikelstamminfo 122)
//*************************************************

if (isset($_POST['txtRFT_LISTE']))
{
	$Felder = explode(';',$Form->NameInArray($_POST,'txtRUECKLIEFKZ',1,1));
	$SQL = '';

	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,15));

				$SQL = 'MERGE INTO AWIS.ARTIKELSTAMMINFOS ASI';
				$SQL .= ' USING (SELECT RFP_AST_ATUNR FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_KEY=0'.$Key.') RFP';
				$SQL .= ' ON (ASI_AST_ATUNR=RFP_AST_ATUNR AND ASI_AIT_ID=122)';
				$SQL .= ' WHEN MATCHED THEN UPDATE SET ASI.ASI_WERT=\''.$_POST[$Feld].'\', ASI.ASI_USER=\''.$AWISBenutzer->BenutzerName(1).'\', ASI.ASI_USERDAT=SYSDATE';
				$SQL .= ' WHEN NOT MATCHED THEN INSERT (ASI.ASI_AST_ATUNR, ASI.ASI_AIT_ID, ASI.ASI_WERT, ASI.ASI_READONLY, ASI.ASI_IMQ_ID, ASI.ASI_USER, ASI.ASI_USERDAT)';
				$SQL .= ' VALUES (RFP.RFP_AST_ATUNR, 122, \''.$_POST[$Feld].'\', -1 ,4, \''.$AWISBenutzer->BenutzerName(1).'\',SYSDATE)';

				$Form->DebugAusgabe(1,$SQL);

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200810201432,$SQL,2);
				}
			}
		}
	}
}

//*************************************************
// R�ckfuehrungstyp Allgemein (Artikelstamminfo 122)
//*************************************************

if (isset($_POST['txtRUECKLIEFKZ']))
{
	if($_POST['txtRUECKLIEFKZ']!=$_POST['oldRUECKLIEFKZ'])
	{
		if(isset($AWIS_KEY2))
		{
			$SQL = 'SELECT RUECKLIEFKZ FROM V_ARTIKEL_RUECKLIEFKZ';
			$SQL .= ' WHERE AST_ATUNR=(SELECT RFP_AST_ATUNR FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_KEY=0'.$AWIS_KEY2.')';
			$SQL .= ' AND GUELTIG=0';

			$rsRFT = $DB->RecordSetOeffnen($SQL);
			if($rsRFT->AnzahlDatensaetze()>0)
			{
				if($rsRFT->FeldInhalt('RUECKLIEFKZ')!=$_POST['txtRUECKLIEFKZ'])
				{

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('Wort','Rueckfuehrungstyp').' '.$Form->LadeTextBaustein('Wort','geaendert_von'),$rsRFT->FeldInhalt('RUECKLIEFKZ'));
					$Felder[]=array($Form->LadeTextBaustein('Wort','Rueckfuehrungstyp').' '.$Form->LadeTextBaustein('Wort','geaendert_auf'),$_POST['txtRUECKLIEFKZ']);

					$Form->SchreibeHTMLCode('<form name="frmRFTAendern" action="./rueckfuehrung_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite'].'&RFP_KEY='.$AWIS_KEY2:'').'" method="POST">');
					$Form->SchreibeHTMLCode('<span class="HinweisText">'.$Form->LadeTextBaustein('Wort','WirklichAendern').'</span>');

					foreach($Felder AS $Feld)
					{
						echo '<br>'.$Feld[0].': <b>'.$Feld[1].'</b>';
					}

					$Form->Erstelle_HiddenFeld('RFA_KEY',$AWIS_KEY1);
					$Form->Erstelle_HiddenFeld('RFP_KEY',$AWIS_KEY2);
					$Form->Erstelle_HiddenFeld('Rueckfuehrungstyp',$_POST['txtRUECKLIEFKZ']);
					$Form->SchreibeHTMLCode('<br><input type="hidden" name="cmdSpeichern_x" value="">');

					$Form->SchreibeHTMLCode('<br><input type="submit" name="cmdRFTAendernOK" value="'.$Form->LadeTextBaustein('Wort','Ja').'">');
					$Form->SchreibeHTMLCode('&nbsp;<input type="submit" name="cmdRFTAendernAbbrechen" value="'.$Form->LadeTextBaustein('Wort','Nein').'">');

					$Form->SchreibeHTMLCode('</form>');
					die();
				}
			}
			else{

				$SQL = 'MERGE INTO AWIS.ARTIKELSTAMMINFOS ASI';
				$SQL .= ' USING (SELECT RFP_AST_ATUNR FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_KEY=0'.$AWIS_KEY2.') RFP';
				$SQL .= ' ON (ASI_AST_ATUNR=RFP_AST_ATUNR AND ASI_AIT_ID=122)';
				$SQL .= ' WHEN MATCHED THEN UPDATE SET ASI.ASI_WERT=\''.$_POST['txtRUECKLIEFKZ'].'\', ASI.ASI_USER=\''.$AWISBenutzer->BenutzerName(1).'\', ASI.ASI_USERDAT=SYSDATE';
				$SQL .= ' WHEN NOT MATCHED THEN INSERT (ASI.ASI_AST_ATUNR, ASI.ASI_AIT_ID, ASI.ASI_WERT, ASI.ASI_READONLY, ASI.ASI_IMQ_ID, ASI.ASI_USER, ASI.ASI_USERDAT)';
				$SQL .= ' VALUES (RFP.RFP_AST_ATUNR, 122, \''.$_POST['txtRUECKLIEFKZ'].'\', -1 ,4, \''.$AWISBenutzer->BenutzerName(1).'\',SYSDATE)';

				$Form->DebugAusgabe(1,$SQL);

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',200810211539,$SQL,2);
				}
			}
		}
	}
}
elseif (isset($_POST['cmdRFTAendernOK']))
{
	$SQL='UPDATE ARTIKELSTAMMINFOS SET ASI_USER=\''.$AWISBenutzer->BenutzerName(1).'\',';
	$SQL.=' ASI_USERDAT=SYSDATE, ASI_WERT=\''.$_POST['txtRueckfuehrungstyp'].'\'';
	$SQL.=' WHERE ASI_AST_ATUNR=(SELECT RFP_AST_ATUNR FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_KEY=0'.$_POST['txtRFP_KEY'].')';
	$SQL.=' AND ASI_AIT_ID=122';

	$Form->DebugAusgabe(1,$SQL);

	if($DB->Ausfuehren($SQL)===false)
	{
		throw new awisException('Fehler beim Speichern',200810211603,$SQL,2);
	}
}


}
catch (awisException $ex)
{
	$Form->DebugAusgabe(1,$ex->getSQL());
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
catch (Exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getSQL());
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>