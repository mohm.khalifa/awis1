#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung fr ein Modul
#
#	Abschnitt
#	[Header]	Infos fr die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Mitarbeitereins�tze
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
1.00.23;Service Europa - Bereich aufgenommen;29.02.2008;<a href=mailto:thomas.riedl@de.atu.eu>Thomas Riedl</a>
1.00.22;Gebiets�bersicht (Ausland) ge�ndert, div. �nderungen f�r neue PHP-Version;19.09.2007;<a href=mailto:christian.argauer@de.atu.eu>Christian Argauer</a>
1.00.21;Projektleiter f�r Ausland hinzugef�gt;15.03.2007;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.20;Grosskunden-Bereich aufgenommen;24.01.2007;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.19;�nderung der Gebiets�bersicht f�r Task Force;17.01.2007;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.18;Ausdruck f�r die Jahres�bersicht;17.01.2007;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.17;Fehler bei der Anzeige von Mitarbeitern (die den Bereich gewechselt haben) behoben;14.12.2006;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.16;Gebietsbersichten aktualisiert;17.08.2006;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.15;Gebietszuordnung erweitert f�r gr��ere Filialanzahlen;04.08.2006;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.14;Bereich hinzugef�gt (Filial-QS);21.02.2006;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.13;Fehler bei der Speicherungen der 'noch zu kl�en' Seite behoben.;22.12.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.12;Erweiterung f�r mehrere Bereiche (Vertrieb/Personalentwicklung);01.12.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.11;Mitarbeiteraktivit�ten: Startdatum hinzugef�gt;22.03.2004;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.10;Logos in die Listen eingebaut;03.03.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.09;Erweiterte Suche nach T�igkeiten;17.02.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.08;Verkaufsgebiete;16.02.2005;;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.07;Weitere Listen aufgebaut;10.02.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.06;Erste Listen;27.01.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.05;Noch zu kl�ende F�le eingefgt;21.01.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.04;Zweite Produktivversion;20.01.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.03;Erste Produktivversion;13.01.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.02;Zweite Version;11.01.2005;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
1.00.01;Erste Version;22.12.2004;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>

