<?PHP
require_once 'XML/XUL.php';
require_once 'PEAR/Remote.php';

$doc = &XML_XUL::createDocument();

$doc->addStylesheet('chrome://global/skin/');

// Window
$win = &$doc->createElement( 'Window', array(
                                            'title'  => 'PEAR',
                                            'orient' => 'vertical'
                                            )
                           );
$doc->addRoot( $win );

$win->addScript( 'search.js' );

// Box f�r Men�s und Buttons
$gbx = &$doc->createElement( 'Groupbox', array(
                                                'orient' => 'horizontal'
											)
						   );
$win->appendChild( $gbx );
$gbx->setCaption( 'PEAR Browser controls' );

// Suche
$src = &$doc->createElement( 'Groupbox', array(
                                                'orient' => 'horizontal'
											)
						   );
// Eingabefeld
$gbx->appendChild( $src );
$src->setCaption( 'Search' );

$stb = &$doc->createElement( 'Textbox', array(
                                                'id' => 'search',
                                             )
                           );
$src->appendChild( $stb );

$sml = &$doc->createElement( 'Menulist', array(
                                                'id' => 'searchtype',
                                             )
                           );
$src->appendChild( $sml );
$sml->addItem( array( 'label' => 'PEAR packages', 'value' => 'pear', 'id' => 'spear' ) );
$sml->addItem( array( 'label' => 'Google', 'value' => 'google', 'id' => 'sgoogle' ) );

$sbt = &$doc->createElement( 'Button', array(
                                                'label' => 'Start Search',
                                                'onclick' => 'startSearch();'
                                             )
                           );
$src->appendChild( $sbt );

$list = &$doc->createElement( 'Groupbox', array(
                                                'orient' => 'vertical'
											)
						   );
                           
$list->setCaption( 'PEAR Packages' );
$gbx->appendChild( $list );

$pml = &$doc->createElement( 'Menulist', array()
                           );
$list->appendChild( $pml );
$pml->addItem( array( 'label' => "Choose package..." ) );

$config = new PEAR_Config();

$sm = array();
$r = new PEAR_Remote($config);
$packages = $r->call('package.listAll', false);

foreach( $packages as $name => $info )
{
    $cat = $info['categoryid'];
    if( !isset( $sm[$cat] ) )
    {
        $sm[$cat] = &$pml->addSubmenu( array( 'label' => $info['category'] ) );
    }
    $sm[$cat]->addItem( array(
                                'label' => $name,
                                'value' => $info['packageid'],
                                'onclick' => "document.getElementById( 'browser' ).setAttribute( 'src', 'http://pear.php.net/package/$name' );"
                             )
                      );
}

$bwr = &$doc->createElement( 'Browser', array(
                                                'id'     => 'browser',
                                                'flex'   => 1,
                                                'src'    => 'http://pear.php.net'
											)
						   );
$win->appendChild( $bwr );
$doc->send();
?>