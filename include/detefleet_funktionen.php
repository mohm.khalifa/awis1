<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once 'awisMailer.inc';

ini_set('max_execution_time', 0);

date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class detefleet_funktionen
{
    private $_DB;
    protected $_AWISBenutzer;
    private $_Werkzeug;
    private $_Mail;
    private $_DebugLevel;

    /**
     * detefleet_Export constructor.
     * @throws awisException
     */
    public function __construct()
    {
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Werkzeug = new awisWerkzeuge();
        $this->_Mail = new awisMailer($this->_DB, $this->_AWISBenutzer);
    }

    function __destruct()
    {
        echo $this->_DebugText;
    }

    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText .= date('d.m.Y H:i:s');
            $this->_DebugText .=  ': ' . $Typ;
            $this->_DebugText .=  ': ' . $Text;
            $this->_DebugText .=  PHP_EOL;
        }
    }

}