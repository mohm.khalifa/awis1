<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
  


<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular
global $AWISBenutzer;		// Aus AWISFormular

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	if(isset($_POST['cmdDrucken_x'])){
		$_GET['XRE'] = 70;
		$_GET['ID'] = base64_encode('&FIL_ID='.$_POST['sucSBR_FIL_ID'].'&DATUM_VOM='.$_POST['sucDATUM_VOM'].'&DATUM_BIS='.$_POST['sucDATUM_BIS'].'&SPEICHERN='.$_POST['sucAuswahlSpeichern']);
		require_once '/daten/web/berichte/drucken.php';
	}
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei(3) .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Filialrueckmeldung');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Filialrueckmeldung'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader3.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();
	if($AWISBenutzer->HatDasRecht(40015)==0)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"200809161548");
		die();
	}

	$Register = new awisRegister(40015);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</body>
</html>