<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

$TextKonserven = array();
$TextKonserven[] = array('XBT', '%');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineDatenbank');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

try {
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht620 = $AWISBenutzer->HatDasRecht(620);
    if ($Recht620 == 0) {
        awisEreignis(3, 1000, 'LieferantenInfos', $AWISBenutzer->BenutzerName(), '', '', '');
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $Form->Formular_Start();

    $EditModus = (($Recht620 & 6) != 0);
    if ($EditModus) {
        $AWISCursorPosition = '';
    }

    $SQL = 'SELECT *';
    $SQL .= ' FROM Informationstypen';
    $SQL .= ' LEFT OUTER JOIN LieferantenInfos ON ITY_KEY = LIN_ITY_KEY AND LIN_LIE_NR=' . $AWIS_KEY1;
    $SQL .= ' WHERE ITY_STATUS<>\'I\' AND ITY_XTN_KUERZEL = \'LIE\' AND ITY_BEREICH = \'Sonderbestellung\'';
    $SQL .= ' ORDER BY ITY_BEREICH, ITY_SORTIERUNG';
    $rsLIN = $DB->RecordSetOeffnen($SQL);

    $LetzterBereich = '';
    while (!$rsLIN->EOF()) {
        $Form->ZeileStart();
        $Info = $Form->LadeTextBaustein('Infofeld', 'ity_lbl_' . $rsLIN->FeldInhalt('ITY_KEY'));
        $InfoTTT = $Form->LadeTextBaustein('Infofeld', 'ity_ttt_' . $rsLIN->FeldInhalt('ITY_KEY'));
        $Form->Erstelle_TextLabel($Info . ':', 200, '', '', $InfoTTT);
        $EditModus = true;
        if ($rsLIN->FeldInhalt('ITY_DATENQUELLE') != '') {
            switch (substr($rsLIN->FeldInhalt('ITY_DATENQUELLE'), 0, 3)) {
                case 'TXT':
                    $Felder = explode(':', $rsLIN->FeldInhalt('ITY_DATENQUELLE'));
                    $Daten = $Form->LadeTexte(array(array($Felder[1], $Felder[2])));
                    $Daten = explode('|', $Daten[$Felder[1]][$Felder[2]]);
                    $Form->Erstelle_SelectFeld('LIN_WERT_' . $rsLIN->FeldInhalt('ITY_KEY') . '_' . $rsLIN->FeldInhalt('LIN_KEY') . '_' . $rsLIN->FeldInhalt('ITY_FORMAT'),
                        $rsLIN->FeldInhalt('LIN_WERT'), $rsLIN->FeldInhalt('ITY_BREITE'), $EditModus, '', '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '',
                        $Daten);
                    break;
                case 'SQL':
                    $Felder = explode(':', $rsLIN->FeldInhalt('ITY_DATENQUELLE'));
                    $Form->Erstelle_SelectFeld('LIN_WERT_' . $rsLIN->FeldInhalt('ITY_KEY') . '_' . $rsLIN->FeldInhalt('LIN_KEY') . '_' . $rsLIN->FeldInhalt('ITY_FORMAT'),
                        $rsLIN->FeldInhalt('LIN_WERT'), $rsLIN->FeldInhalt('ITY_BREITE'), $EditModus, $Felder[1], '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
                    break;
                default:
                    $Form->Erstelle_TextFeld('LIN_WERT_' . $rsLIN->FeldInhalt('ITY_KEY') . '_' . $rsLIN->FeldInhalt('LIN_KEY') . '_' . $rsLIN->FeldInhalt('ITY_FORMAT'),
                        $rsLIN->FeldInhalt('LIN_WERT'), $rsLIN->FeldInhalt('ITY_ZEICHEN'), $rsLIN->FeldInhalt('ITY_BREITE'), $EditModus, '', '', '',
                        $rsLIN->FeldInhalt('ITY_FORMAT'), '', '', $rsLIN->FeldInhalt('ITY_DATENQUELLE'));
                    break;
            }
        } else {
            $Form->Erstelle_TextFeld('LIN_WERT_' . $rsLIN->FeldInhalt('ITY_KEY') . '_' . $rsLIN->FeldInhalt('LIN_KEY') . '_' . $rsLIN->FeldInhalt('ITY_FORMAT'),
                $rsLIN->FeldInhalt('LIN_WERT'), $rsLIN->FeldInhalt('ITY_ZEICHEN'), $rsLIN->FeldInhalt('ITY_BREITE'), $EditModus, '', '', '', $rsLIN->FeldInhalt('ITY_FORMAT'));
        }
        if ($AWISCursorPosition == '') {
            $AWISCursorPosition = 'txtLIN_WERT_' . $rsLIN->FeldInhalt('ITY_KEY') . '_' . $rsLIN->FeldInhalt('LIN_KEY') . '_' . $rsLIN->FeldInhalt('ITY_FORMAT');
        }

        $Form->ZeileEnde();
        $rsLIN->DSWeiter();
    }

    $Form->Formular_Ende();
} catch (Exception $ex) {
    if (is_object($Form)) {
        $Form->Formular_Start();
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'WIEDERHOLEN', 0);
        $Form->Formular_Ende();
    }
}
?>