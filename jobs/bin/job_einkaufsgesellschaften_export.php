<?php
require_once('awis_einkaufsgesellschaften.inc');
require_once('awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $StartTag = '';
    $EndTag = '';

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    
    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;                       
                   
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }
    }


    $Job = new awisJobProtokoll(58, 'Starte Einkaufsgesellschaften Export.');
    $SonderausweiseExport = new awis_einkaufsgesellschaften($Benutzer);
    $SonderausweiseExport->DebugLevel($DebugLevel);
    $SonderausweiseExport->ExportBewegDat();
    $Job->SchreibeLog(1,0,'Ende des Jobs.');

}
catch (Exception $ex)
{
    echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    echo 'CODE:  '.$ex->getCode().PHP_EOL;
    echo 'Zeile: '.$ex->getLine().PHP_EOL;
    echo 'Datei: '.$ex->getFile().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
    }
}
?>
