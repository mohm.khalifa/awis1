<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUB','ttt_ZUBSTATUS');
	$TextKonserven[]=array('ZUB','ZUB_WANR');
	$TextKonserven[]=array('ZUB','LetzterDatensatz');
	$TextKonserven[]=array('ZIL','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ZIL_TYP');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('XJO','XJO_%');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
 
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10007 = $AWISBenutzer->HatDasRecht(10007);
	if($Recht10007==0)
	{
	    awisEreignis(3,1000,'Zukauf',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$SQL = 'SELECT *';
	$SQL .= ' FROM jobliste';
	$SQL .= ' WHERE xjo_gruppe = \'ZUKAUF\' ';
	$SQL .= ' ORDER BY XJO_JOBBEZEICHNUNG';

	$rsXJO = $DB->RecordSetOeffnen($SQL);



	$Form->Formular_Start();

	$Form->ZeileStart();
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=XJO_JOBBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_JOBBEZEICHNUNG'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_JOBBEZEICHNUNG'],300,'',$Link);

	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=XJO_LETZTERSTART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTERSTART'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTERSTART'],160,'',$Link);

	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=XJO_LETZTESENDE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTESENDE'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTESENDE'],160,'',$Link);

	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=XJO_NAECHSTERSTART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_NAECHSTERSTART'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_NAECHSTERSTART'],160,'',$Link);

	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=XJO_INTERVALL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_INTERVALL'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_INTERVALL'],80,'',$Link);

	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=XJO_LETZTERSTATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTERSTATUS'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTERSTATUS'],140,'',$Link);

	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=XJO_LETZTEMELDUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTEMELDUNG'))?'~':'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTEMELDUNG'],150,'',$Link);

	$Form->ZeileEnde();

	//for($DS=0;$DS<$rsXJOZeilen;$DS++)
	$DS=0;
	while(!$rsXJO->EOF())
	{
		$Status = '';
		$Form->ZeileStart();

		$Link = '';
		$Form->Erstelle_ListenFeld('XJO_JOBBEZEICHNUNG',$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG'),0,300,false,($DS%2),'',$Link,'T');
		$Form->Erstelle_ListenFeld('XJO_LETZTERSTART',$rsXJO->FeldInhalt('XJO_LETZTERSTART'),0,160,false,($DS%2),'',$Link,'DU');
		$Form->Erstelle_ListenFeld('XJO_LETZTESENDE',$rsXJO->FeldInhalt('XJO_LETZTESENDE'),0,160,false,($DS%2),'',$Link,'DU');
		$Form->Erstelle_ListenFeld('XJO_NAECHSTERSTART',$rsXJO->FeldInhalt('XJO_NAECHSTERSTART'),0,160,false,($DS%2),'',$Link,'DU');
		$Form->Erstelle_ListenFeld('XJO_INTERVALL',$rsXJO->FeldInhalt('XJO_INTERVALL'),0,80,false,($DS%2),'',$Link,'Z');
		$Form->Erstelle_ListenFeld('XJO_LETZTERSTATUS',$rsXJO->FeldInhalt('XJO_LETZTERSTATUS'),0,140,false,($DS%2),'',$Link,'Z');
		$Form->Erstelle_ListenFeld('XJO_LETZTEMELDUNG',$rsXJO->FeldInhalt('XJO_LETZTEMELDUNG'),0,150,false,($DS%2),'',$Link,'Z');

		$Form->ZeileEnde();
		$DS++;
		$rsXJO->DSWeiter();
	}
	
	//$Form->PruefeDatum($DatumsText); 

	//***********************************************************************
	//* SOAP Anfragen
	//***********************************************************************
	$Form->Trennzeile();
	
	// Server testen
	$AWISWerkzeug = new awisWerkzeuge();
	$ATU_SOAP_SERVER = $AWISWerkzeug->LeseAWISParameter('zukauf.conf','ATU_SOAP_SERVER');
	$Params = array('trace'=>1
			, 'soap_version'=>SOAP_1_2
			, 'exceptions'=>1		// Keine Exceptions ausl�sen, wenn der Server nicht reagiert
			, 'connection_timeout' => 10		// Kurzes Timeout zum Testen
			, 'location'=>'http://'.$ATU_SOAP_SERVER.'/soap/awisSOAP_Zukauf.php'
	);
	ini_set('soap.wsdl_cache', 0);

	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift('Servertest', 1200);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($ATU_SOAP_SERVER, 250);
	ini_set('display_errors', 'ERROR_NONE');				// Keine Fehler vom PHP anzeigen
	$Texte[1]='Test, ob der AWIS Server verf&uuml;gbar ist.';
	$Texte[2]='Test, ob der AWIS Server auf die Datenbank zugreifen kann.';
	try 
	{
		$Stufen = 2;
		$Stufe = 0;
		$SC = new SoapClient('http://'.$ATU_SOAP_SERVER.'/soap/awisSOAP_Zukauf.php?wsdl',$Params);
		
		for($Stufe=1;$Stufe<=$Stufen;$Stufe++)
		{
			$Erg = $SC->Test($Stufe,'');
			$Form->Erstelle_Bild('bild', 'Pruefung', '', '/bilder/'.($Erg==true?'ampel_gruen.png':'ampel_rot.png'), $Texte[$Stufe], 20);
			if($Erg==false)
			{
				break;
			}
		}
	}
	catch(Exception $ex)
	{
		$Form->Erstelle_Bild('bild', 'Pruefung', '', '/bilder/ampel_rot.png', 20);
		$Form->Erstelle_TextLabel($ex->getMessage(), 600);
	}
	catch(SoapFault $ex)
	{
		$Form->Erstelle_Bild('bild', 'Pruefung', '', '/bilder/ampel_rot.png', 20);
		$Form->Erstelle_TextLabel($ex->getMessage(), 600);
	}
	$Form->ZeileEnde();
	
	//***************************************************************
	//* Hinweistexte bei Fehlern
	//***************************************************************
	if($Stufe<=$Stufen)
	{
		$Aktion = 0;
		switch ($Stufe)
		{
			case 0:				// Server nicht erreichbar
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('&nbsp;', 100);
				$Form->Erstelle_TextLabel('Problem:', 150);
				$Form->Erstelle_TextLabel('Zugriff auf den internen AWIS Server nicht m&ouml;glich', 0,'Hinweis');
				$Form->ZeileEnde();
				$Aktion = 1;
				break;
			case 1:				// TEST-Methode geht nicht
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('&nbsp;', 100);
				$Form->Erstelle_TextLabel('Problem Level ('.$Stufe.'):', 150);
				$Form->Erstelle_TextLabel('Zugriff auf den internen AWIS Server liefert einen Fehler', 0,'Hinweis');
				$Form->ZeileEnde();
				$Aktion = 1;
				break;
			case 2:				// Kein Tabellenzugriff moeglich
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('&nbsp;', 100);
				$Form->Erstelle_TextLabel('Problem Level ('.$Stufe.'):', 150);
				$Form->Erstelle_TextLabel('Kein Zugriff auf die Daten im Oracle Server', 0,'Hinweis');
				$Form->ZeileEnde();
				$Aktion = 1;
				break;
		}
		
		switch($Aktion)
		{
			case 1:
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('&nbsp;', 100);
				$Form->Erstelle_TextLabel('Aktion:', 150);
				$Form->Erstelle_TextLabel('Verst&auml;ndigen Sie: Sacha Kerres (0171 2104846).', 0,'');
				$Form->ZeileEnde();
		}
	}

	// Tests ok
	if($Stufe>$Stufen)
	{
		
		$Form->Trennzeile();
		
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Lieferantentest', 1200);
		$Form->ZeileEnde();
		
		
		$SQL = 'SELECT LIE_NR, LIE_NAME1 || COALESCE(\', \' || LIE_NAME2,\'\') AS LIENAME';
		$SQL .= ', (SELECT LIN_WERT FROM lieferanteninfos WHERE LIE_NR = LIN_LIE_NR AND LIN_ITY_KEY = 7) AS ANFRAGEN';
		$SQL .= ', (SELECT LIN_WERT FROM lieferanteninfos WHERE LIE_NR = LIN_LIE_NR AND LIN_ITY_KEY = 8) AS BESTELLUNG';
		$SQL .= ' FROM lieferanten';
		$SQL .= ' INNER JOIN lieferanteninfos ON LIE_NR = LIN_LIE_NR AND LIN_ITY_KEY = 1 AND LIN_WERT = \'1\'';
		$SQL .= ' ORDER BY LIE_NR';
		$rsLIE = $DB->RecordSetOeffnen($SQL);
		
		while (!$rsLIE->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($rsLIE->FeldInhalt('LIE_NR'), 100);
			$Form->Erstelle_TextLabel($rsLIE->FeldInhalt('LIENAME'), 400);

			$Erg = $SC->Test(100, $rsLIE->FeldInhalt('LIE_NR'));

			if($Erg===1)
			{
				$Form->Erstelle_Bild('bild', 'Pruefung', '', '/bilder/ampel_gruen.png');
				$Form->Erstelle_TextLabel('', 600);
			}
			elseif($Erg===0)
			{
				$Form->Erstelle_Bild('bild', 'Pruefung', '', '/bilder/ampel_gelb.png');
				$Form->Erstelle_TextLabel('', 600);
			}
			else 
			{
				$Erg = simplexml_load_string($Erg);
				$Form->Erstelle_Bild('bild', 'Pruefung', '', '/bilder/ampel_rot.png', $Erg->Fehler->Nummer);
				$Form->Erstelle_TextLabel($Erg->Fehler->Text, 600);
			}
			$Form->ZeileEnde();
			$Form->Trennzeile();
			$rsLIE->DSWeiter();
		}
	}
		
	$Form->Trennzeile();

	//***********************************************************************
	//* Letzte Datenimporte der Lieferanten
	//***********************************************************************
	$SQL = 'SELECT DATEN.*, LIE_NAME1  ';
	$SQL .= ' ,(SELECT LIN_WERT FROM LieferantenInfos WHERE LIN_LIE_NR = ZUB_LIE_NR AND LIN_ITY_KEY=2) AS Hotline';
	$SQL .= ' ,CASE WHEN DATUM<(SYSDATE-8/24) THEN 2 ELSE CASE WHEN DATUM<(SYSDATE-3/24) THEN 1 ELSE 0 END END AS STYLE_STATUS';
	$SQL .= ' FROM (';
	$SQL .= 'SELECT MAX(ZUB_BESTELLDATUM) AS DATUM, ZUB_LIE_NR';
	$SQL .= ' FROM Zukaufbestellungen';
	$SQL .= ' GROUP BY ZUB_LIE_NR) DATEN ';
	$SQL .= ' INNER JOIN Lieferanten ON ZUB_LIE_NR = LIE_NR';
	$SQL .= ' ORDER BY DATUM DESC';

	
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['LetzterDatensatz'],800);
	$Form->ZeileEnde();

	$rsInfo = $DB->RecordsetOeffnen($SQL);
	while(!$rsInfo->EOF())
	{
		$Style = '';
		if($rsInfo->FeldInhalt('STYLE_STATUS')==2)
		{
			$Style='background-color: red; color: white;';
		} 
		elseif ($rsInfo->FeldInhalt('STYLE_STATUS')==1)
		{
			$Style='background-color: yellow;';
		}
		
		$Form->ZeileStart();
		$Link = '/stammdaten/lieferanten/lieferanten_Main.php?cmdAktion=Details&LIE_NR='.$rsInfo->FeldInhalt('ZUB_LIE_NR');
		$Form->Erstelle_TextLabel($rsInfo->FeldInhalt('LIE_NAME1').':',300,'',$Link,$rsInfo->FeldInhalt('ZUB_LIE_NR'));
		$Form->Erstelle_TextFeld('#DATUM',$rsInfo->FeldInhalt('DATUM'),18,180,false,$Style,'','','DU');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('',300);
		$Form->Erstelle_TextLabel($rsInfo->FeldInhalt('HOTLINE'),500);
		$Form->ZeileEnde();
		$Form->Trennzeile();
		$rsInfo->DSWeiter();
	}

	echo '<form name=frmLog action="./zukaufbestellungen_Main.php?cmdAktion=Importinfo" method=post>';


	//*********************************************************************************************
	// Protokoll anzeigen
	//*********************************************************************************************
	if(!isset($_POST['cmdWeiter_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen("Formular_ZIL"));
	}
	else
	{
		$Param['DatumVom']=$_POST['sucDatumVom'];
		$Param['DatumBis']=$_POST['sucDatumBis'];
		$Param['ZIL_TYP']=$_POST['sucZIL_TYP'];
	}

	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZukaufImportLog'],800);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',150);
	$Form->Erstelle_TextFeld('*DatumVom',(isset($Param['DatumVom'])?$Param['DatumVom']:date('d.m.Y')),18,160,true,'','','','DU');
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',150);
	$Form->Erstelle_TextFeld('*DatumBis',(isset($Param['DatumBis'])?$Param['DatumBis']:date('d.m.Y H:i')),18,160,true,'','','','DU');
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZIL']['ZIL_TYP'].':',150);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_ZIL_TYP']);
	$Form->Erstelle_SelectFeld('*ZIL_TYP',(isset($Param['ZIL_TYP'])?$Param['ZIL_TYP']:''),200,true,'','','',1,'',$Daten);

	$Form->ZeileEnde();

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->SchaltflaechenEnde();

	$Form->Trennzeile();

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$ORDERBY = ' ORDER BY ZIL_USERDAT DESC';

	$SQL = 'SELECT ZukaufImportLog.*, ZUB_KEY, ZUB_WANR';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZukaufImportLog';
	$SQL .= ' INNER JOIN Zukaufbestellungen ON ZIL_EXTERNEID = ZUB_EXTERNEID';

	$Bedingung = '';
	$StartZeit = trim($Form->PruefeDatum($Param['DatumVom'],true));
	$Bedingung .= ' AND ZIL_ZEITPUNKT >= '.$DB->FeldInhaltFormat('DU',$StartZeit);

	$EndeZeit = trim($Form->PruefeDatum($Param['DatumBis'],true));
	$Bedingung .= ' AND ZIL_ZEITPUNKT <= '.$DB->FeldInhaltFormat('DU',$EndeZeit);

	if($Param['ZIL_TYP']!='')
	{
		$Bedingung .= ' AND ZIL_TYP = '.$DB->FeldInhaltFormat('T',$Param['ZIL_TYP']);
	}

	if($Bedingung != '')
	{
		$SQL .= ' WHERE '. substr($Bedingung,4);
	}
	$SQL .= $ORDERBY;

//$Form->DebugAusgabe(1,$SQL,$Param,$_POST);
	$AWISBenutzer->ParameterSchreiben("Formular_ZIL",serialize($Param));
	// Zum Bl�ttern in den Daten
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		$Param['BLOCK']=$Block;
		$AWISBenutzer->ParameterSchreiben('Formular_LIE',serialize($Param));
	}
	elseif(isset($Param['BLOCK']))
	{
		$Block=$Param['BLOCK'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	$rsZIL = $DB->RecordsetOeffnen($SQL);

//$Form->DebugAusgabe(1,$SQL,$_POST,$EndeZeit);
	$Form->ZeileStart();

	$Link = '';
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_ZEITPUNKT'],160,'',$Link);
	$Link = '';
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_TYP'],60,'',$Link);
	$Link = '';
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_MELDUNG'],680,'',$Link);
	$Link = '';
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_WANR'],160,'',$Link);
	$Link = '';
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZIL']['ZIL_BEARBEITUNGSSTATUS'],100,'',$Link);

	$Form->ZeileEnde();

	$DS=0;
	while(!$rsZIL->EOF())
	{
		$Form->ZeileStart();

		$Form->Erstelle_ListenFeld('ZIL_ZEITPUNKT',$rsZIL->FeldInhalt('ZIL_ZEITPUNKT'),10,160,false,($DS%2),'','','DU');
		$Form->Erstelle_ListenFeld('ZIL_TYP',$rsZIL->FeldInhalt('ZIL_TYP'),10,60,false,($DS%2),'','','T');
		$Text = (strlen($rsZIL->FeldInhalt('ZIL_MELDUNG'))>85?substr($rsZIL->FeldInhalt('ZIL_MELDUNG'),0,83).'...':$rsZIL->FeldInhalt('ZIL_MELDUNG'));
		$Form->Erstelle_ListenFeld('ZIL_MELDUNG',$Text,10,680,false,($DS%2),'','','T','',$rsZIL->FeldInhalt('ZIL_MELDUNG'));
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=0'.$rsZIL->FeldInhalt('ZUB_KEY');
		$Form->Erstelle_ListenFeld('ZUB_WANR',$rsZIL->FeldInhalt('ZUB_WANR'),10,160,false,($DS%2),'',$Link,'T');
		$Form->Erstelle_ListenFeld('ZIL_BEARBEITUNGSSTATUS',$rsZIL->FeldInhalt('ZIL_BEARBEITUNGSSTATUS'),10,100,false,($DS%2),'','','T');

		$Form->ZeileEnde();
		$rsZIL->DSWeiter();
		$DS++;
	}

	$Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo';
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	$Form->Formular_Ende();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=zukaufbestellungen&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>