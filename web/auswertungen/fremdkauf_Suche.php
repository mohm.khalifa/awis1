<?php
// Variablen
global $awisDBFehler;
global $AWISBenutzer;
global $con;

$RechteStufe = awisBenutzerRecht($con, 501);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Fremdkauf',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

//echo "<form method=post name=frmSuche action='./fremkauf_Main.php?cmdAktion=Liste'>";
echo "<form name=frmSuche method=post action=./fremdkauf_Main.php>";

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='')
{

}
else
{
	echo '<br>';
	echo '<table border=1 width=100%>';

	echo '<tr>';
	echo '<td>OE-Nummer</td>';
	echo '<td><input name=txtOEN_NUMMER size=40 tabindex=10 value=' . (isset($_GET['OEN_NUMMER'])?$_GET['OEN_NUMMER']:'') . '></td>';
	echo '</tr>';
	if(isset($_GET['OEN_NUMMER']) AND $_GET['OEN_NUMMER']!='')
	{
		$EingabeFeld='OEN_NUMMER';
	}

	echo '<tr>';
	echo '<td>KBA-Nummer</td>';
	echo '<td><input name=txtKBA_NR size=20 tabindex=20 value=' . (isset($_GET['KBA_NR'])?$_GET['KBA_NR']:'') . '></td>';
	echo '</tr>';
	if(isset($_GET['KBA_NR']) AND $_GET['KBA_NR']!='')
	{
		$EingabeFeld='KBA_NR';
	}

	echo '<tr>';
	echo '<td>Filiale</td>';
	echo '<td><input name=txtFIL_ID size=5 tabindex=30 value=' . (isset($_GET['FIL_ID'])?$_GET['FIL_ID']:'') . '>';
	echo '&nbsp;bis';
	echo '&nbsp;<input name=txtFIL_ID_Bis size=5 tabindex=30 value=' . (isset($_GET['FIL_ID'])?$_GET['FIL_ID']:'') . '></td>';
	echo '</tr>';
	if(isset($_GET['FIL_ID']) AND $_GET['FIL_ID']!='')
	{
		$EingabeFeld='FIL_ID';
	}


	echo '<tr>';
	echo '<td>Lfd.Nr.</td>';
	echo '<td><input name=txtLFDNR size=8 tabindex=40 value=' . (isset($_GET['LFDNR'])?$_GET['LFDNR']:'') . '></td>';
	echo '</tr>';
	if(isset($_GET['LFDNR']) AND $_GET['LFDNR']!='')
	{
		$EingabeFeld='LFDNR';
	}

	echo '<tr>';
	echo '<td>Datum vom</td>';
	echo '<td><input name=txtDatumVom size=8 tabindex=50 value=' . (isset($_GET['DatumVom'])?$_GET['DatumVom']:'') . '>';
	echo '&nbsp;bis&nbsp;<input name=txtDatumBis size=8 tabindex=60 value=' . (isset($_GET['DatumBis'])?$_GET['DatumBis']:'') . '></td>';
	echo '</tr>';
	if(isset($_GET['DatumVom']) AND $_GET['DatumVom']!='')
	{
		$EingabeFeld='DatumVom';
	}

	echo '<tr><td>Ausgabeformat</td><td><select name=txtFormat tabindex=70>';
	echo '<option value=liste>LISTE</option>';
//	echo '<option value=pdf>PDF</option>';
	echo '<option value=csv>CSV-Datei</option>';
	echo '</select></td></tr>';


	echo '<tr><td>Auswertung</td><td><select name=cmdAktion tabindex=80>';
	echo '<option value=Liste>Fremdkauf</option>';
	echo '<option value=EKAT>EKAT</option>';
	echo '<option value=EKATALLE>EKAT-alle Daten</option>';
	echo '</select></td></tr>';

	echo '<tr><td>Bearbeitungsgruppe (f&uuml;r EKAT Liste)</td><td><select name=txtBearbeitungsgruppe tabindex=80>';
	$SQL = 'SELECT DISTINCT HER_BEARBEITUNGSGRUPPE';
	$SQL .= ' FROM HERSTELLER';
	$SQL .= ' ORDER BY 1';
	echo '<option value=>::alle::</option>';
	$rsHER = awisOpenRecordset($con,$SQL);
	for($i=0;$i<$awisRSZeilen;$i++)
	{
		echo '<option value='.$rsHER['HER_BEARBEITUNGSGRUPPE'][$i].'>'.$rsHER['HER_BEARBEITUNGSGRUPPE'][$i].'</option>';
	}
	echo '</select></td></tr>';

	echo '</table>';
	echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
}
echo '</form>';

if(isset($EingabeFeld) AND $EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('txtOE_NUMMER')[0].focus();";
	echo "\n</Script>";
}

?>

