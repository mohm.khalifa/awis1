<?php
ob_start();  //Einschalten der Pufferung von Ausgaben

require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
require_once("card/card_db.inc.php");			// DB


$strSql="select REC_RNR,REC_KUNDEN_NR,REC_RGDATUM from rechnungen where REC_RGDATUM between '01.09.04' and '30.09.04'";                                                                                                                                                                           ;
$arrRgBuchungen=abfrage("ora",$hdlOraVerbindung,$strSql);

ob_end_clean();	//Verwirft den Inhalt des Ausgabe-Puffers und deaktiviert die Pufferung

require_once("Spreadsheet/Excel/Writer.php");			// PEAR::Spreadsheet_Excel_Writer

$xls=& new Spreadsheet_Excel_Writer();

$xls->send("test.xls");
$sheet=& $xls->addWorksheet('Blatt 1');
$testFormat=& $xls->addFormat();
$testFormat->setNumFormat('#,##0.00 �;[Red]-#,##0.00 �');


for($i=0;$i<$arrRgBuchungen["zeilen"];$i++)
{
	$sheet->write($i,0,$arrRgBuchungen["trefferarray"]["REC_RNR"][$i]);
	$sheet->write($i,1,$arrRgBuchungen["trefferarray"]["REC_KUNDEN_NR"][$i],$testFormat);
	$sheet->write($i,2,$arrRgBuchungen["trefferarray"]["REC_RGDATUM"][$i]);
}

$xls->close();

?>
