<?php
require_once('awisFremdkaufauswertung.php');

try {

    $Benutzer = 'awis_jobs';
    $DebugLevel = 0;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $MOB = new awisFremdkaufauswertung($Benutzer);
    $MOB->DebugLevel($DebugLevel);
    $MOB->ErstelleAuswertung();

} catch (Exception $ex) {
    echo PHP_EOL . 'Fremdkaufauswertung: Fehler: ' . $ex->getMessage() . PHP_EOL;

    $AWISWerkzeug = new awisWerkzeuge();
    $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'), 'Fremdkaufauswertung', 'Fehler :' . $ex->getMessage(), 3, '', 'awis@de.atu.eu');

    echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
    if(is_a($ex,awisException::class)){
        echo 'FEHLER:' . $ex->getSQL() . PHP_EOL;
    }
    echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
    echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
    echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}

?>