<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	if(isset($_POST['txtPPR_STATUS']))
	{
		//***********************************************
		// Abschluss eines Tages
		//***********************************************

		$NeuerStatus = $_POST['txtPPR_STATUS'];
		$Felder = explode(';',$Form->NameInArray($_POST,'txtPPR_BEMERKUNG_',1,1));

		foreach ($Felder as $Feld)
		{
			$Key = substr($Feld,strlen('txtPPR_BEMERKUNG_'));
			if($Key!='')
			{
				$SQL = 'UPDATE perseinspruefungen SET';
				$SQL .= ' PPR_STATUS = '.$DB->FeldInhaltFormat('T',$NeuerStatus);
				$SQL .= ', PPR_BEMERKUNG = '.$DB->FeldInhaltFormat('T',$_POST[$Feld]);
				$SQL .= ', PPR_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PPR_userdat=sysdate';
				$SQL .= ' WHERE PPR_key=0' . $Key . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
				}
			}
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>