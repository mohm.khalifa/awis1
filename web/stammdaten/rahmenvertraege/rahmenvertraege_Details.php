<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RAV','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht630 = $AWISBenutzer->HatDasRecht(630);
	if($Recht630==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GSR'));
	if(empty($Param))
	{
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='GSR_RAHMENVERTR DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['GSV_VERTRAGSART'] = $_POST['sucRAV_VERTRAGSART'];
		$Param['GSV_GASLIEFERANT'] = isset($_POST['sucRAV_GASLIEFERANT'])?$_POST['sucRAV_GASLIEFERANT']:'';
		$Param['GSV_STROMLIEFERANT'] = isset($_POST['sucRAV_STROMLIEFERANT'])?$_POST['sucRAV_STROMLIEFERANT']:'';

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='GSR_RAHMENVERTR DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_GSR",serialize($Param));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./rahmenvertraege_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['GSR_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['GSR_KEY']);
	}
	elseif($AWIS_KEY1==0) 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='GSR_RAHMENVERTR DESC';
			$AWISBenutzer->ParameterSchreiben('Formular_XFT',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	if(isset($_GET['Aktiv']))
	{
		include('./rahmenvertraege_speichern.php');
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY GSR_RAHMENVERTR DESC';
			$Param['ORDER']='GSR_RAHMENVERTR DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT V.*,L.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM GSVRAHMENVERTRAEGE V';
	$SQL .= ' INNER JOIN GSVLIEFERANTEN L ON GSR_LIEFERANT = GSL_KEY';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_GSR',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('GSR', false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsGSR = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('GSR', true));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmRahmenvert action=./rahmenvertraege_Main.php?cmdAktion=Details method=POST enctype="multipart/form-data">');

	if($rsGSR->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Param['BLOCK']=1;		// Zur�ck setzen, falls es einfach den Block nicht mehr gibt.
	}
	elseif($rsGSR->AnzahlDatensaetze()>1 AND !isset($_GET['GSR_KEY']))						// Liste anzeigen
	{
		$FeldBreiten = array();
		$FeldBreiten['Bezeichnung'] = 350;
		$FeldBreiten['Lieferant'] = 300;
		$FeldBreiten['Vertragslaufzeit'] = 150;
		$FeldBreiten['Kuendigung'] = 150;
		$FeldBreiten['Aktiv'] = 20;
		$DetailAnsicht = false;
		$Form->Formular_Start();
		$Param['KEY']='';

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './rahmenvertraege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=GSR_RAHMENVERTR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSR_RAHMENVERTR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_BEZEICHNUNG'],$FeldBreiten['Bezeichnung'],'',$Link);
		$Link = './rahmenvertraege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=GSL_FIRMA'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSL_FIRMA'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_LIEFERANT'],$FeldBreiten['Lieferant'],'',$Link);
		$Link = './rahmenvertraege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=GSR_VERTRAGSLAUFZEIT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSR_VERTRAGSLAUFZEIT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEIT'],$FeldBreiten['Vertragslaufzeit'],'',$Link);
		$Link = './rahmenvertraege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=GSR_KUENDIGUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='GSR_KUENDIGUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGZUM'],$FeldBreiten['Kuendigung'],'',$Link);
		$TTT = $AWISSprachKonserven['RAV']['ttt_UEBAKTIV'];
		$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['Aktiv'], '',$Link,$TTT);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsGSR->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './rahmenvertraege_Main.php?cmdAktion=Details&GSR_KEY='.$rsGSR->FeldInhalt('GSR_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('#GSR_BEZEICHNUNG',$rsGSR->FeldInhalt('GSR_RAHMENVERTR'),0,$FeldBreiten['Bezeichnung'],false,($DS%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('#GSL_FIRMA',$rsGSR->FeldInhalt('GSL_FIRMA'),0,$FeldBreiten['Lieferant'],false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('#GSR_VERTRAGSLAUFZEIT',$Form->Format('D',$rsGSR->FeldInhalt('GSR_VERTRAGSLAUFZEIT')),0,$FeldBreiten['Vertragslaufzeit'],false,($DS%2),'','');
			$Form->Erstelle_ListenFeld('#GSR_KUENDIGUNGZUM',$Form->Format('D',$rsGSR->FeldInhalt('GSR_KUENDIGUNG')),0,$FeldBreiten['Kuendigung'],false,($DS%2),'','');
			$LinkAktiv =  './rahmenvertraege_Main.php?cmdAktion=Details&button='.$rsGSR->FeldInhalt('GSR_KEY').'&Aktiv='.$rsGSR->FeldInhalt('GSR_AKTIV');
			$IconsArray = '';
			if ($rsGSR->FeldInhalt('GSR_AKTIV') == 1)
			{
				$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['RAV']['ttt_Aktiv']);
			}
			else
			{
				$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['RAV']['ttt_Inaktiv']);
			}
			$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['Aktiv'] , ($DS%2));

			$Form->ZeileEnde();
			$rsGSR->DSWeiter();
			$DS++;
		}

		$Link = './rahmenvertraege_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		if(!isset($AWIS_KEY1))
		{
			$AWIS_KEY1 = $rsGSR->FeldInhalt('GSR_KEY');
		}

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_GSR',serialize($Param));

		$Form->Erstelle_HiddenFeld('GSR_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rahmenvertraege_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsGSR->FeldInhalt('GSR_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsGSR->FeldInhalt('GSR_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_RAHMENVERTR'].':',180);
		$Form->Erstelle_TextFeld('!RAV_RAHMENVERTR',$rsGSR->FeldInhalt('GSR_RAHMENVERTR'),40,400,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSART'].':',180);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_VERTRAGSART']);
		$Form->Erstelle_SelectFeld('!RAV_VERTRAGSART',$rsGSR->FeldInhalt('GSR_VERTRAGSART'),150,true,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'onchange="wechsleVertrag(this);"');
		$Form->ZeileEnde();



		if($AWIS_KEY1 <> 0 and $AWIS_KEY1 != '-1')
		{
			$DefaultWert = $rsGSR->FeldInhalt('GSR_LIEFERANT').'~'.$rsGSR->FeldInhalt('GSL_SUCHNAME');
		}
		else {
			$DefaultWert = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		}
		$filter='***RAV_Lieferant;txtRAV_VERTRAGSART;RAV_VERTRAGSART=*txtRAV_VERTRAGSART&ZUSATZ=~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'].';;';

		$Form->ZeileStart();
		$Form->AuswahlBox('ajaxLieferant','box1','','GSL_LIEFERANTEN','txtRAV_LIEFERANT,txtGSR_LIEFERANT',10,10,'','T',true,'','','display:none','','','','',0,'display:none');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LIEFERANT'] . ':',160);
		$Form->Erstelle_SelectFeld('!RAV_LIEFERANT','', 700,true,$filter,$DefaultWert,'', '','','','onChange="key_ajaxLieferant(event);"','',array(),'');
		$Form->ZeileEnde();

		$Form->Erstelle_HiddenFeld('GSR_LIEFERANT',$rsGSR->FeldInhalt('GSR_LIEFERANT'));
		ob_start();
		if($AWIS_KEY1 <> '-1') {
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FIRMA'] . ':', 180, '', '');
			$Form->Erstelle_TextFeld('!GSL_FIRMA', $rsGSR->FeldInhalt('GSL_FIRMA'), 30, 400, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VORNAME'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_VORNAME', $rsGSR->FeldInhalt('GSL_VORNAME'), 30, 200, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSPRECHPARTNER'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_ANSPRECHPARTNER', $rsGSR->FeldInhalt('GSL_ANSPRECHPARTNER'), 30, 200, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_STRASSE', $rsGSR->FeldInhalt('GSL_STRASSE'), 30, 200, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PLZORT'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_PLZ', $rsGSR->FeldInhalt('GSL_PLZ'), 30, 60, false);
			$Form->Erstelle_TextFeld('!GSL_ORT', $rsGSR->FeldInhalt('GSL_ORT'), 30, 200, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_STRASSE', $rsGSR->FeldInhalt('GSL_STRASSE'), 30, 200, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_FIRMA'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_TELNR_FIRMA', $rsGSR->FeldInhalt('GSL_TELNR_FIRMA'), 30, 250, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FAXNR'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_FAXNR', $rsGSR->FeldInhalt('GSL_FAXNR'), 30, 250, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EMAIL'] . ':', 180);
			$Form->Erstelle_TextFeld('!GSL_MAIL', $rsGSR->FeldInhalt('GSL_MAIL'), 30, 300, false);
			$Form->ZeileEnde();
		}
		$Inhalt = ob_get_clean();
		$Form->ZeileStart();
		$Form->AuswahlBoxHuelle('box1','','',$Inhalt);
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEIT'].':',180);
		$Form->Erstelle_TextFeld('!GSL_VERTRAGSLAUFZEIT',$Form->Format('D',$rsGSR->FeldInhalt('GSR_VERTRAGSLAUFZEIT')),7,400,true,'','','','D');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGZUM'].':',180);
		$Form->Erstelle_TextFeld('GSL_KUENDIGUNGZUM',$Form->Format('D',$rsGSR->FeldInhalt('GSR_KUENDIGUNG')),7,400,true,'','','','D');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGSFRIST'].':',180);
		$Form->Erstelle_TextFeld('GSL_KUENDIGUNGSFRIST',$rsGSR->FeldInhalt('GSR_KUENDIGUNGSFRIST'),20,400,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSSTATUS'].':',180);
		$Form->Erstelle_Checkbox('GSL_VETRAGSSTATUS',$rsGSR->FeldInhalt('GSR_VERTRAGSSTATUS'),50,400,true);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_GEKUENDIGT'],180);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PREISANPASSUNG'].':',180);
		$Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_KUNDE',$rsGSR->FeldInhalt('GSR_PREISANPASSUNG_KUNDE'),50,450,true);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EVU'].':',180);
		$Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_EVU',$rsGSR->FeldInhalt('GSR_PREISANPASSUNG_LIEFERANT'),50,450,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_HTPREIS'].':',180);
		$Form->Erstelle_TextFeld('!GSL_HTPREIS',$rsGSR->FeldInhalt('GSR_HTPREIS_A'),20,400,true);
		$Form->ZeileEnde();

		if($AWIS_KEY1 != '-1') {
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_AKTIV'] . ':', 180);
			$Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
			$Form->Erstelle_SelectFeld('GSL_AKTIV', $rsGSR->FeldInhalt('GSR_AKTIV'), 150, true, '', '', '', '', '', $Daten);
			$Form->ZeileEnde();
		}
		$Form->Formular_Ende();

	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if($DetailAnsicht) {
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	} else {
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	$AWISBenutzer->ParameterSchreiben('Formular_GSR',serialize($Param));
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201615");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201614");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND GSR_KEY = '.$DB->WertSetzen('GSR', 'N0',$AWIS_KEY1).' ';
		return $Bedingung;
	}
	if(isset($Param['GSV_VERTRAGSART']) AND $Param['GSV_VERTRAGSART']!='')
	{
		$Bedingung .= ' AND GSR_VERTRAGSART ' . $DB->LIKEoderIST($Param['GSV_VERTRAGSART']) . '';
	}
	if(isset($Param['GSV_GASLIEFERANT']) AND $Param['GSV_GASLIEFERANT']!='')
	{
		$Bedingung .= ' AND GSR_LIEFERANT = ' . $DB->WertSetzen('GSR', 'N0',$Param['GSV_GASLIEFERANT']) . ' ';
	}
	if(isset($Param['GSV_STROMLIEFERANT']) AND $Param['GSV_STROMLIEFERANT']!='')
	{
		$Bedingung .= ' AND GSR_LIEFERANT = ' . $DB->WertSetzen('GSR', 'N0',$Param['GSV_STROMLIEFERANT']) . ' ';
	}
	return $Bedingung;
}
?>