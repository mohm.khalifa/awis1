#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Schadensdatenbank
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
1.01.00;Phase 1.5 abgeschlossen;04.02.2016;<a href=mailto:tobias.schaeffler@de.atu.eu>Tobias Schaeffler</a>
1.00.22;Bugfixes;21.01.2016;<a href=mailto:tobias.schaeffler@de.atu.eu>Tobias Schaeffler</a>
1.00.21;WV-Switch bei �nderung SB;21.01.2016;<a href=mailto:tobias.schaeffler@de.atu.eu>Tobias Schaeffler</a>
1.00.21;Schadens-PDF angepasst;21.01.2016;<a href=mailto:tobias.schaeffler@de.atu.eu>Tobias Schaeffler</a>
1.00.20;Druckoption Aufgaben,OWV,WV;21.01.2016;<a href=mailto:tobias.schaeffler@de.atu.eu>Tobias Schaeffler</a>
1.00.18;Bugfixes;21.12.2015;Patrick Gebhardt
1.00.17;Autocomplete Textfelder deaktiviert;21.12.2015;Patrick Gebhardt
1.00.16;Suche nach Schadensjahr;21.12.2015;Patrick Gebhardt
1.00.15;Antragsarten sortiert;21.12.2015;Patrick Gebhardt
1.00.14;Inforeiter Textareas;21.12.2015;Patrick Gebhardt
1.00.13;Bugfixes;21.12.2015;Patrick Gebhardt
1.00.12;VU-WA aufgebohrt;17.12.2015;Patrick Gebhardt
1.00.11;Geschlossen an doppelklick;17.12.2015;Patrick Gebhardt
1.00.10;Verbesserungen Textareas;17.12.2015;Patrick Gebhardt
1.00.01;Bugfixes;16.12.2015;Patrick Gebhardt
1.00.00;Erste Version;01.12.2014;<a href=mailto:tobias.schaeffler@de.atu.eu>Tobias Schaeffler </a>
