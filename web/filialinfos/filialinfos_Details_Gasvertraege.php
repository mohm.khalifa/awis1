<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RAV','%');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);		// Register in Filialen
	if(($Recht5001&8192)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	if(isset($_GET['SSort']))
	{
		$ORDERBY='ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
	}
	else
	{
		$ORDERBY = ' ORDER BY GSR_RAHMENVERTR DESC';
	}


	$SQL = 'select V.*,L.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM GSVRAHMENVERTRAEGE V';
	$SQL .= ' inner join GSVLIEFERANTEN L ON GSR_LIEFERANT = GSL_KEY';
	$SQL .= ' where gsr_key in (';
	$SQL .= ' select gsi_gsr_key from gsvinfos ';
	$SQL .= ' WHERE gsi_xxx_key = ' . $DB->WertSetzen('SVT', 'N0',$AWIS_KEY1);
	$SQL .= ' group by gsi_gsr_key)';
	$SQL .= ' and gsr_vertragsart = 1';

	$FelderDel = '';
	$FelderEdit = '';
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	$FelderEdit = $Form->NameInArray($_POST, 'ico_edit',1,1);

	if ((isset($_GET['GSI_KEY'])) or (isset($_POST['txtGSI_KEY']) and (isset($_POST['ico_add__x']) or $FelderDel != false  or $FelderEdit != false) ) )
	{

		$SQL .= ' AND GSR_KEY = '. $DB->WertSetzen('SVT', 'N0',(isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY']));
	}

	$rsSVT = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SVT', true));


	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_GASVERTRAEGE'],100, 'font-weight:bolder');
	$Form->Trennzeile('O');
	$Form->ZeileEnde();

	$Form->ZeileStart();

	$Feldbreite['RAV_RAHMENVERTR']  = 250;
	$Feldbreite['RAV_LAUFZEITBIS'] = 200;


	if ((isset($_GET['GSI_KEY'])) or (isset($_POST['txtGSI_KEY'])) and $rsSVT->AnzahlDatensaetze() < 2) //Bestehnden DS editieren.
	{
		$AWIS_KEY2 = isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY'];

		if(($Form->DatumsDifferenz($rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'),date('d.m.Y')))>0) {
			$edit = false;
		}
		else {
			$edit = true;
		}

		$SQL = ' select GST_FELDNAME,GSI_WERT from gsvinfotypen';
		$SQL .= ' inner join gsvinfos on gsi_gst_key = gst_key ';
		$SQL .= ' AND gsi_xxx_key = ' . $DB->WertSetzen('GSI', 'N0',$AWIS_KEY1);
		$SQL .= ' AND gsi_gsr_key = ' . $DB->WertSetzen('GSI', 'N0',(isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY']));
		$SQL .= ' where GST_BEREICH = \'Gas\'';

		$rsInfo = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GSI', true));


		$InfoFelder = array();

		while (!$rsInfo->EOF()) {

			$InfoFelder[$rsInfo->FeldInhalt('GST_FELDNAME')] =
				$rsInfo->FeldInhalt('GSI_WERT');

			$rsInfo->DSWeiter();
		}

		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialinfos_Main.php?cmdAktion=Details&Seite=" . $_GET['Seite'] ." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVT->FeldInhalt('GSR_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVT->FeldInhalt('GSR_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUNDENNUMMER'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_KUNDENNUMMER',isset($InfoFelder['GVT_KUNDENNUMMER'])?$InfoFelder['GVT_KUNDENNUMMER']:'', 10, 280, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LEISTUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_LEISTUNG',isset($InfoFelder['GVT_LEISTUNG'])?$InfoFelder['GVT_LEISTUNG']:'', 10, 100, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KW'], 50);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PREISPROKW'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_PREIS_PRO_KWH',isset($InfoFelder['GVT_PREIS_PRO_KWH'])?$InfoFelder['GVT_PREIS_PRO_KWH']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_WAERMEQUELLE'] . ':', 200);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_WAERMEQUELLEN']);
		$Form->Erstelle_SelectFeld('GVT_WAERMEQUELLE',isset($InfoFelder['GVT_WAERMEQUELLE'])?$InfoFelder['GVT_WAERMEQUELLE']:'',150,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ZAEHLERNUMMER'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_ZAEHLERNUMMER',isset($InfoFelder['GVT_ZAEHLERNUMMER'])?$InfoFelder['GVT_ZAEHLERNUMMER']:'', 10, 280, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERBRAUCH'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_VERBRAUCH',isset($InfoFelder['GVT_VERBRAUCH'])?$InfoFelder['GVT_VERBRAUCH']:'', 10, 100, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'], 50);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSCHLUSSLEISTUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_ANSCHLUSSLEISTUNG',isset($InfoFelder['GVT_ANSCHLUSSLEISTUNG'])?$InfoFelder['GVT_ANSCHLUSSLEISTUNG']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_DECODE'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_DE_CODE',isset($InfoFelder['SVT_DE_CODE'])?$InfoFelder['SVT_DE_CODE']:'', 30, 280, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ZAEHLERSTANDAM'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_ZAEHLERSTAND_A',isset($InfoFelder['GVT_ZAEHLERSTAND_A'])?$InfoFelder['GVT_ZAEHLERSTAND_A']:'', 10, 150, $edit, '', '', '', 'D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VETRAGESTATO'] . ':', 350);
		$Form->Erstelle_Checkbox('GVT_VERTRAGSUMSTELLUNG_ESTATO',isset($InfoFelder['GVT_VERTRAGSUMSTELLUNG_ESTATO'])?$InfoFelder['GVT_VERTRAGSUMSTELLUNG_ESTATO']:'',50,$edit,true);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ABSCHLAG'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_RECHNUNGSLEGUNG_A',isset($InfoFelder['GVT_RECHNUNGSLEGUNG_A'])?$InfoFelder['GVT_RECHNUNGSLEGUNG_A']:'', 3, 55, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURKW'],60);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_ABSCHLAG_2']);
		$Form->Erstelle_SelectFeld('GVT_RECHNUNGSLEGUNG_B',isset($InfoFelder['SVT_LEISTUNGSPREIS_B'])?$InfoFelder['GVT_RECHNUNGSLEGUNG_B']:'',165,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ZAEHLERSTAND'] . ':', 150);
		$Form->Erstelle_TextFeld('GVT_ZAEHLERSTAND_B',isset($InfoFelder['GVT_ZAEHLERSTAND_B'])?$InfoFelder['GVT_ZAEHLERSTAND_B']:'', 10, 150, $edit, '', '', '', 'T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSCHLUSS'] . ':', 350);
		$Form->Erstelle_Checkbox('GVT_VERTRAG_ZUM',isset($InfoFelder['GVT_VERTRAG_ZUM'])?$InfoFelder['GVT_VERTRAG_ZUM']:'',50,$edit,true);
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();


		$SQL = 'select GSR_KEY,GSR_RAHMENVERTR';
		$SQL .= ' FROM GSVRAHMENVERTRAEGE V';
		$SQL .= ' WHERE gsr_vertragsart = 1';
		$SQL .= ' and trunc(gsr_vertragslaufzeit) >= (sysdate)';
		$SQL .= ' and gsr_key not in (select distinct gsi_gsr_key from gsvinfos where gsi_xxx_key='.$DB->WertSetzen('GSR', 'N0',$AWIS_KEY1).')';
		
		
		$Form->ZeileStart();
		$Form->AuswahlBox('ajaxRahmenvertr','box1','','GSR_RAHMENVERTR','txtRAV_RAHMENVERTR,txtGSR_RAHMENVERTR,txtGSR_VERTRAGSART',10,10,'','T',true,'','','display:none','','','','',0,'display:none');
		$Form->AuswahlBox('ajaxHTPREIS','box2','','GSR_HTPREIS','txtRAV_RAHMENVERTR,txtGSR_RAHMENVERTR,txtGSR_VERTRAGSART',10,10,'','T',true,'','','display:none','','','','',0,'display:none');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_GASVERTRAG'] . ':',160, 'font-weight:bolder;color:blue');
		if($AWIS_KEY2 != -1) {
			$Form->Erstelle_TextFeld('!RAV_RAHMENVERTR',$rsSVT->FeldInhalt('GSR_RAHMENVERTR'),7,400,false,'','','');
		} else {
			$Form->Erstelle_SelectFeld('!RAV_RAHMENVERTR',$rsSVT->FeldInhalt('GSR_RAHMENVERTR'), 700, true, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'', '', '', '',
				'onChange="key_ajaxRahmenvertr(event);key_ajaxHTPREIS(event);"', '',$DB->Bindevariablen('GSR', true), '');
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		$Form->Erstelle_HiddenFeld('RAV_KEY',isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY']);
		$Form->Erstelle_HiddenFeld('GSR_RAHMENVERTR',$rsSVT->FeldInhalt('GSR_KEY'));
		$Form->Erstelle_HiddenFeld('GSR_VERTRAGSART',1);

		ob_start();
		if($AWIS_KEY1 <> '-1') {

			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ENERGIELIEFERANT'],560, 'font-weight:bolder;color:blue');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEITEN'],100, 'font-weight:bolder;color:blue');
			$Form->Trennzeile('O');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FIRMA'] . ':', 160, '', '');
			$Form->Erstelle_TextFeld('!GSL_FIRMA', $rsSVT->FeldInhalt('GSL_FIRMA'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSLAUFZEIT'].':',250);
			$Form->Erstelle_TextFeld('!GSL_VERTRAGSLAUFZEIT',$Form->Format('D',$rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT')),7,400,false,'','','','D');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VORNAME'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_VORNAME', $rsSVT->FeldInhalt('GSL_VORNAME'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGZUM'].':',250);
			$Form->Erstelle_TextFeld('!GSL_KUENDIGUNGZUM',$Form->Format('D',$rsSVT->FeldInhalt('GSR_KUENDIGUNG')),7,400,false,'','','','D');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ANSPRECHPARTNER'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_ANSPRECHPARTNER', $rsSVT->FeldInhalt('GSL_ANSPRECHPARTNER'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KUENDIGUNGSFRIST'].':',250);
			$Form->Erstelle_TextFeld('!GSL_KUENDIGUNGSFRIST',$rsSVT->FeldInhalt('GSR_KUENDIGUNGSFRIST'),20,400,false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_STRASSE', $rsSVT->FeldInhalt('GSL_STRASSE'), 30, 400, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_VERTRAGSSTATUS'].':',250);
			$Form->Erstelle_Checkbox('GSL_VETRAGSSTATUS',$rsSVT->FeldInhalt('GSR_VERTRAGSSTATUS'),50,false,true);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_GEKUENDIGT'],180);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PLZORT'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_PLZ', $rsSVT->FeldInhalt('GSL_PLZ'), 30, 60, false);
			$Form->Erstelle_TextFeld('!GSL_ORT', $rsSVT->FeldInhalt('GSL_ORT'), 30, 340, false);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_PREISANPASSUNG'].':',250);
			$Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_KUNDE',$rsSVT->FeldInhalt('GSR_PREISANPASSUNG_KUNDE'),50,false,true);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EVU'].':',180);
			$Form->Erstelle_Checkbox('GSL_PREISANPASSUNG_EVU',$rsSVT->FeldInhalt('GSR_PREISANPASSUNG_LIEFERANT'),50,false,true);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_STRASSE'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_STRASSE', $rsSVT->FeldInhalt('GSL_STRASSE'), 30, 200, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_TELNR_FIRMA'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_TELNR_FIRMA', $rsSVT->FeldInhalt('GSL_TELNR_FIRMA'), 30, 250, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_FAXNR'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_FAXNR', $rsSVT->FeldInhalt('GSL_FAXNR'), 30, 250, false);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EMAIL'] . ':', 160);
			$Form->Erstelle_TextFeld('!GSL_MAIL', $rsSVT->FeldInhalt('GSL_MAIL'), 30, 300, false);
			$Form->ZeileEnde();
		}
		$Inhalt = ob_get_clean();
		$Form->ZeileStart();
		$Form->AuswahlBoxHuelle('box1','','',$Inhalt);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ENERGIEKOSTEN'],560, 'font-weight:bolder;color:blue');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_MESSKOSTEN'],100, 'font-weight:bolder;color:blue');
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_LEISTUNGSPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_LEISTUNGSPREISE_A',isset($InfoFelder['GVT_LEISTUNGSPREISE_A'])?$InfoFelder['GVT_LEISTUNGSPREISE_A']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURKW'].':',120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_LEISTUNGSPREIS']);
		$Form->Erstelle_SelectFeld('GVT_LEISTUNGSPREISE_B',isset($InfoFelder['GVT_LEISTUNGSPREISE_B'])?$InfoFelder['GVT_LEISTUNGSPREISE_B']:'',210,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_MESSPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_MESSPREIS_A', isset($InfoFelder['GVT_MESSPREIS_A'])?$InfoFelder['GVT_MESSPREIS_A']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURPRO'],120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_ZEITRAUM']);
		$Form->Erstelle_SelectFeld('GVT_MESSPREIS_B',isset($InfoFelder['GVT_MESSPREIS_B'])?$InfoFelder['GVT_MESSPREIS_B']:'',150,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_BONUS'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_BONUS',isset($InfoFelder['GVT_BONUS'])?$InfoFelder['GVT_BONUS']:'', 30, 100, $edit);

		$Form->ZeileEnde();

		ob_start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_HTPREIS'] . ':', 120);
		$Form->Erstelle_TextFeld('!RAV_HTPREIS_A', $rsSVT->FeldInhalt('GSR_HTPREIS_A'), 3, 100, false);
		$Form->ZeileEnde();
		$Inhalt = ob_get_clean();
		$Form->ZeileStart();
		$Form->AuswahlBoxHuelle('box2','','',$Inhalt,230,0);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_HT_PREIS_B', isset($InfoFelder['GVT_HT_PREIS_B'])?$InfoFelder['GVT_HT_PREIS_B']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 109);

		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_SONSTKOST'] . ':', 121);
		$Form->Erstelle_TextFeld('GVT_SONSTIGEKOSTEN_A',isset($InfoFelder['GVT_SONSTIGEKOSTEN_A'])?$InfoFelder['GVT_SONSTIGEKOSTEN_A']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_EURPRO'],120);
		$Daten = explode('|',$AWISSprachKonserven['RAV']['lst_ZEITRAUM']);
		$Form->Erstelle_SelectFeld('GVT_SONSTIGEKOSTEN_B',isset($InfoFelder['GVT_SONSTIGEKOSTEN_B'])?$InfoFelder['GVT_SONSTIGEKOSTEN_B']:'',150,$edit,'','~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('GVT_HT_PREIS_C',isset($InfoFelder['GVT_HT_PREIS_C'])?$InfoFelder['GVT_HT_PREIS_C']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_HT_PREIS_D',isset($InfoFelder['GVT_HT_PREIS_D'])?$InfoFelder['GVT_HT_PREIS_D']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 109);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('GVT_HT_PREIS_E',isset($InfoFelder['GVT_HT_PREIS_E'])?$InfoFelder['GVT_HT_PREIS_E']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_HT_PREIS_F',isset($InfoFelder['GVT_HT_PREIS_F'])?$InfoFelder['GVT_HT_PREIS_F']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 120);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('GVT_HT_PREIS_G',isset($InfoFelder['GVT_HT_PREIS_G'])?$InfoFelder['GVT_HT_PREIS_G']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_HT_PREIS_H',isset($InfoFelder['GVT_HT_PREIS_H'])?$InfoFelder['GVT_HT_PREIS_H']:'', 3, 100, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_KWH'] , 108);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_ENERGIESTR'] . ':', 120);
		$Form->Erstelle_TextFeld('GVT_HT_ENERGIE_A', isset($InfoFelder['GVT_HT_ENERGIE_A'])?$InfoFelder['GVT_HT_ENERGIE_A']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 330);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('', 120);
		$Form->Erstelle_TextFeld('GVT_HT_ENERGIE_B', isset($InfoFelder['GVT_HT_ENERGIE_B'])?$InfoFelder['GVT_HT_ENERGIE_B']:'', 3, 108, $edit);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_CTKWH'] . ':', 330);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('L');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RAV']['RAV_INFO'] . ':', 120);
		$Form->Erstelle_Textarea('GVT_INFO',isset($InfoFelder['GVT_INFO'])?$InfoFelder['GVT_INFO']:'',500,80,5,$edit,'font: bold');
		$Form->ZeileEnde();

		if($AWIS_KEY2 != -1) {
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();

			$Feldbreite['RAV_ARZ_VON'] = 150;
			$Feldbreite['RAV_ARZ_BIS'] = 150;
			$Feldbreite['RAV_TAGE'] = 150;
			$Feldbreite['RAV_KWH_M3'] = 150;
			$Feldbreite['RAV_SUM_NETTO'] = 150;
			$Feldbreite['RAV_CT_KWH'] = 150;
			$Feldbreite['RAV_DOCUWARE_NR'] = 150;
			$Feldbreite['RAV_DATUM'] = 150;

			$Icons = array();
			$Icons[] = array('add', 'POST~');

			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('', 40);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_ARZ_VON'], $Feldbreite['RAV_ARZ_VON']);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_ARZ_BIS'], $Feldbreite['RAV_ARZ_BIS']);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_TAGE'], $Feldbreite['RAV_TAGE']);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_KWH_M3'], $Feldbreite['RAV_KWH_M3']);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_SUM_NETTO'], $Feldbreite['RAV_SUM_NETTO']);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_CT_KWH'], $Feldbreite['RAV_CT_KWH']);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_DOCUWARE_NR'], $Feldbreite['RAV_DOCUWARE_NR']);
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_DATUM'], $Feldbreite['RAV_DATUM']);
			$Form->ZeileEnde();

			$i = 1;

			if (isset($_POST['ico_add__x'])) {

				$SQL = 'INSERT INTO GSVGASZUSATZFELDER ';
				$SQL .= '(GSZ_FIL_ID,GSZ_ARZ_VON,GSZ_ARZ_BIS,GSZ_TAGE,GSZ_KW_M3,GSZ_SUM_NETTO,GSZ_CT_KWH,GSZ_DOCUWARENR,GSZ_DATUM,GSZ_USER,GSZ_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->WertSetzen('GSZ','N0',$AWIS_KEY1,true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','D', $_POST['txtRAV_ARZ_VON'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','D', $_POST['txtRAV_ARZ_BIS'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','N', $_POST['txtRAV_TAGE'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','N', $_POST['txtRAV_KWH_M3'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','N', $_POST['txtRAV_SUM_NETTO'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','N3', $_POST['txtRAV_CT_KWH'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','T', $_POST['txtRAV_DOCUWARE_NR'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','D', $_POST['txtRAV_DATUM'], true);
				$SQL .= ',' . $DB->WertSetzen('GSZ','T',$AWISBenutzer->BenutzerName()) . '';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				$DB->Ausfuehren($SQL, '', true,$DB->Bindevariablen('GSZ', true));
			}

			$Felder = '';
			if ($FelderDel != false) {
				$Felder = explode(';', $FelderDel);
				$Feld = explode('_', $Felder[1]);
				$Key = $Feld[2];

				$SQL = 'DELETE FROM GSVGASZUSATZFELDER WHERE gsz_key=' . $DB->WertSetzen('GSZ','N0',$Key);
				$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('GSZ',true));
			}

			if ($FelderEdit != false) {

				$Felder = explode(';', $FelderEdit);
				$Feld = explode('_', $Felder[1]);
				$Key = $Feld[2];

				$SQL = 'UPDATE GSVGASZUSATZFELDER';
				$SQL .= ' SET GSZ_ARZ_VON='.$DB->WertSetzen('GSZ','D',$_POST['txtRAV_ARZ_VON'.$Key]);
				$SQL .= ' ,GSZ_ARZ_BIS='.$DB->WertSetzen('GSZ','D',$_POST['txtRAV_ARZ_BIS'.$Key]);
				$SQL .= ' ,GSZ_TAGE='.$DB->WertSetzen('GSZ','N',$_POST['txtRAV_TAGE'.$Key]);
				$SQL .= ' ,GSZ_KW_M3='.$DB->WertSetzen('GSZ','N',$_POST['txtRAV_KWH_M3'.$Key]);
				$SQL .= ' ,GSZ_SUM_NETTO='.$DB->WertSetzen('GSZ','N2',$_POST['txtRAV_SUM_NETTO'.$Key]);
				$SQL .= ' ,GSZ_CT_KWH='.$DB->WertSetzen('GSZ','N3',$_POST['txtRAV_CT_KWH'.$Key]);
				$SQL .= ' ,GSZ_DOCUWARENR='.$DB->WertSetzen('GSZ','T',$_POST['txtRAV_DOCUWARE_NR'.$Key]);
				$SQL .= ' ,GSZ_DATUM='.$DB->WertSetzen('GSZ','D',$_POST['txtRAV_ARZ_BIS'.$Key]);
				$SQL .= ' ,GSZ_USER='.$DB->WertSetzen('GSZ','T',$AWISBenutzer->BenutzerName()).'';
				$SQL .= ' ,GSZ_USERDAT=SYSDATE';
				$SQL .= ' WHERE GSZ_KEY='.$DB->WertSetzen('GSZ','N0',$Key);
				
				$DB->Ausfuehren($SQL, '', true,$DB->Bindevariablen('GSZ', true));
			}

			if (isset($Fehlermeldung)) {
				$AktuelleAnzahl = isset($_POST['txtaktCount'])?$_POST['txtaktCount']:1;
			} else {
				$AktuelleAnzahl = isset($_POST['txtaktCount'])?$_POST['txtaktCount'] + 1:1;
			}

			$SQL = 'select *';
			$SQL .= ' FROM GSVGASZUSATZFELDER';
			$SQL .= ' WHERE gsz_fil_id = ' . $DB->WertSetzen('GSZ','N0',$AWIS_KEY1);

			$rsZusatz = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GSZ', true));

			$DS = 1;
			while (!$rsZusatz->EOF()) {
				$Icons = array();
				$Icons[] = array('edit', 'POST~' . $rsZusatz->FeldInhalt('GSZ_KEY'));
				$Icons[] = array('delete', 'POST~' . $rsZusatz->FeldInhalt('GSZ_KEY'));
				$Form->ZeileStart();
				$Form->Erstelle_ListeIcons($Icons, 38);
				$Form->Erstelle_ListenFeld('LEER', '', 0, 2, false);
				$Form->Erstelle_ListenFeld('RAV_ARZ_VON' . $rsZusatz->FeldInhalt('GSZ_KEY'), $rsZusatz->FeldInhalt('GSZ_ARZ_VON'), $Feldbreite['RAV_ARZ_VON'] / 7.5, $Feldbreite['RAV_ARZ_VON'], true);
				$Form->Erstelle_ListenFeld('RAV_ARZ_BIS' . $rsZusatz->FeldInhalt('GSZ_KEY'), $rsZusatz->FeldInhalt('GSZ_ARZ_BIS'), $Feldbreite['RAV_ARZ_BIS'] / 7.5, $Feldbreite['RAV_ARZ_BIS'], true);
				$Form->Erstelle_ListenFeld('RAV_TAGE' . $rsZusatz->FeldInhalt('GSZ_KEY'), $rsZusatz->FeldInhalt('GSZ_TAGE'), $Feldbreite['RAV_TAGE'] / 7.5, $Feldbreite['RAV_TAGE'], true);
				$Form->Erstelle_ListenFeld('RAV_KWH_M3' . $rsZusatz->FeldInhalt('GSZ_KEY'), $rsZusatz->FeldInhalt('GSZ_KW_M3'), $Feldbreite['RAV_KWH_M3'] / 7.5, $Feldbreite['RAV_KWH_M3'], true);
				$Form->Erstelle_ListenFeld('RAV_SUM_NETTO' . $rsZusatz->FeldInhalt('GSZ_KEY'), $rsZusatz->FeldInhalt('GSZ_SUM_NETTO'), $Feldbreite['RAV_SUM_NETTO'] / 7.5, $Feldbreite['RAV_SUM_NETTO'], true);
				$Form->Erstelle_ListenFeld('RAV_CT_KWH' . $rsZusatz->FeldInhalt('GSZ_KEY'), $Form->Format('N3', $rsZusatz->FeldInhalt('GSZ_CT_KWH')), $Feldbreite['RAV_CT_KWH'] / 7.5, $Feldbreite['RAV_CT_KWH'],
					true);
				$Form->Erstelle_ListenFeld('RAV_DOCUWARE_NR' . $rsZusatz->FeldInhalt('GSZ_KEY'), $rsZusatz->FeldInhalt('GSZ_DOCUWARENR'), $Feldbreite['RAV_DOCUWARE_NR'] / 7.5, $Feldbreite['RAV_DOCUWARE_NR'],
					true);
				$Form->Erstelle_ListenFeld('RAV_DATUM' . $rsZusatz->FeldInhalt('GSZ_KEY'), $rsZusatz->FeldInhalt('GSZ_DATUM'), $Feldbreite['RAV_DATUM'] / 7.5, $Feldbreite['RAV_DATUM'] - 4, true);
				$Form->ZeileEnde();

				$DS++;
				$rsZusatz->DSWeiter();
			}

			$Form->ZeileStart();
			$Icons = array();
			$Icons[] = array('add', 'POST~');
			$Form->Erstelle_ListeIcons($Icons, 38);
			$Form->Erstelle_ListenFeld('LEER', '', 0, 2, false);
			$Form->Erstelle_ListenFeld('RAV_ARZ_VON', '', $Feldbreite['RAV_ARZ_VON'] / 7.5, $Feldbreite['RAV_ARZ_VON'], true, '', '', '');
			$Form->Erstelle_ListenFeld('RAV_ARZ_BIS', '', $Feldbreite['RAV_ARZ_BIS'] / 7.5, $Feldbreite['RAV_ARZ_BIS'], true, '', '', '');
			$Form->Erstelle_ListenFeld('RAV_TAGE', '', $Feldbreite['RAV_TAGE'] / 7.5, $Feldbreite['RAV_TAGE'], true, '', '', '');
			$Form->Erstelle_ListenFeld('RAV_KWH_M3', '', $Feldbreite['RAV_KWH_M3'] / 7.5, $Feldbreite['RAV_KWH_M3'], true, '', '', '');
			$Form->Erstelle_ListenFeld('RAV_SUM_NETTO', '', $Feldbreite['RAV_SUM_NETTO'] / 7.5, $Feldbreite['RAV_SUM_NETTO'], true, '', '', '');
			$Form->Erstelle_ListenFeld('RAV_CT_KWH', '', $Feldbreite['RAV_CT_KWH'] / 7.5, $Feldbreite['RAV_CT_KWH'], true, '', '', '');
			$Form->Erstelle_ListenFeld('RAV_DOCUWARE_NR', '', $Feldbreite['RAV_DOCUWARE_NR'] / 7.5, $Feldbreite['RAV_DOCUWARE_NR'], true, '', '', '');
			$Form->Erstelle_ListenFeld('RAV_DATUM', '', $Feldbreite['RAV_DATUM'] / 7.5, $Feldbreite['RAV_DATUM'] - 4, true);
			$Form->ZeileEnde();
			$Form->Erstelle_HiddenFeld('aktCount', $AktuelleAnzahl);
		}
			$Form->Erstelle_HiddenFeld('GSI_KEY', isset($_GET['GSI_KEY'])?$_GET['GSI_KEY']:$_POST['txtGSI_KEY']);


	}
	else {
		$Form->ZeileStart();

		$Feldbreite['RAV_RAHMENVERTR'] = 250;
		$Feldbreite['RAV_LAUFZEITBIS'] = 200;

		$Icons = array();
		$Icons[] = array('new', './filialinfos_Main.php?cmdAktion=Details&Seite=' . $_GET['Seite'] . '&GSI_KEY=-1', 'g');
		$Form->Erstelle_ListeIcons($Icons, 38, 0);

		$Link = './filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
		$Link .= '&SSort=GSR_RAHMENVERTR' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'GSR_RAHMENVERTR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_RAHMENVERTR'], $Feldbreite['RAV_RAHMENVERTR'], '', $Link);

		$Link = './filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
		$Link .= '&SSort=GSR_VERTRAGSLAUFZEIT' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'GSR_VERTRAGSLAUFZEIT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RAV']['RAV_LAUFZEITBIS'], $Feldbreite['RAV_LAUFZEITBIS'], '', $Link);

		$Form->ZeileEnde();

		if ($rsSVT->AnzahlDatensaetze() > 0) {
			$DS = 0;
			while (!$rsSVT->EOF()) {
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array(
					'edit',
					'./filialinfos_Main.php?cmdAktion=Details'. (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'').'&GSI_KEY=' . $rsSVT->FeldInhalt('GSR_KEY')
				);
				if(($Form->DatumsDifferenz($rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'),date('d.m.Y')))<0) {
					$Icons[] = array(
						'delete',
						'./filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Del=' . $rsSVT->FeldInhalt('GSR_KEY').'&FIL_ID='.$AWIS_KEY1
					);
				}

				if(($Form->DatumsDifferenz($rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'),date('d.m.Y')))>0){
					$Style = 'background-color: #FF0000;';
				} else {
					$Style = '';
				}

				$Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2), '',$Style);
				$Form->Erstelle_ListenFeld('KZT_TERMINAL_ID', $rsSVT->FeldInhalt('GSR_RAHMENVERTR'), 0, $Feldbreite['RAV_RAHMENVERTR'], false, ($DS % 2),
					$Style, '', 'T', 'L', '');
				$Form->Erstelle_ListenFeld('KZT_GUELTIG_AB', $rsSVT->FeldInhalt('GSR_VERTRAGSLAUFZEIT'), 0, $Feldbreite['RAV_LAUFZEITBIS'], false, ($DS % 2),
					$Style, '', 'T', 'L', '');
				$Form->ZeileEnde();
				$DS++;
				$rsSVT->DSWeiter();
			}
			$Form->Formular_Ende();
		}
	}

	$Form->ZeileStart();
	$Form->Trennzeile('O');
	$Form->ZeileEnde();

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>