<?php
// Variablen
global $awisDBFehler;
global $awisRSZeilen;
global $Params;
global $con;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 1300);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}
$MBEBereiche = awisBenutzerRecht($con, 1306);			// Mitarbeiterbereiche
if($MBEBereiche==0)		// Wg. Umstellung!! -> Rausnehmen, wenn Rechte vergeben wurden
{
	$MBEBereiche=1;
}
$MBEBereicheAnz = substr_count(sprintf('%b',$MBEBereiche),'1');		// Anzahl Bereiche

if(isset($_GET['Reset']) && $_GET['Reset']=='Ja')
{
	$Params = Array();
}
else
{
	$Params = explode(";",awis_BenutzerParameter($con, "Personaleinsatz_Suche", $AWISBenutzer->BenutzerName()));
}

$RZListeStufe = awisBenutzerRecht($con, 1302);		// Einschr�nkungen nach RZ
$RZListe = explode(";",awis_BenutzerParameter($con, "Mitarbeiter_RZListe_Rechte", $AWISBenutzer->BenutzerName()));
$RZUserListe = '';
for($i=0;$i<16;$i++)
{
	if(($RZListeStufe&pow(2,$i))==pow(2,$i))
	{
		$RZUserListe .= ';' . $RZListe[$i+1];
	}
}
$RZUserListe .= ';';


//awis_Debug(1,$MBEBereiche);
//awis_Debug(1,$MBEBereicheAnz);

echo "<form name=frmSuche method=post action=./perseins_Main.php>";

echo '<br>';
echo '<table border=1 width=100%>';

		/*********** Einsatzbereich ************/
if($MBEBereicheAnz>1)
{
	echo '<tr>';
	echo '<td >Einsatzbereich</td>';
	echo '<td>';
	echo "<select name=txtMIT_BEREICH> ";
	
	if(($MBEBereiche & 1)==1)			// Vertrieb
	{
		echo '<option' . ($Params[8]=='Vertrieb'?' selected':'') . '>Vertrieb</option>';
	}
	if(($MBEBereiche & 2)==2)			// ATU Academy
	{
		echo '<option' . ($Params[8]=='ATU Academy'?' selected':'') . '>ATU Academy</option>';
	}
	if(($MBEBereiche & 4)==4)			// Filial-QS - erstellt am 21.02.06 CA
	{
		echo '<option' . ($Params[8]=='Filial-QS'?' selected':'') . '>Filial-QS</option>';
	}
	if(($MBEBereiche & 8)==8)			// Gro�kunden - erstellt am 06.06.06 CA
	{
		echo '<option' . ($Params[8]=='Gro�kunden'?' selected':'') . '>Gro�kunden</option>';
	}
	if(($MBEBereiche & 16)==16)			// Category Management - erstellt am 19.04.07 CA
	{
		echo '<option' . ($Params[8]=='Category Management'?' selected':'') . '>Category Management</option>';
	}
	if(($MBEBereiche & 32)==32)			// Smart-Repair - erstellt am 19.11.07 CA
	{
		echo '<option' . ($Params[8]=='Smart-Repair'?' selected':'') . '>Smart-Repair</option>';
	}
	if(($MBEBereiche & 64)==64)			// Service Europa - erstellt am 06.02.08 TR
	{
		echo '<option' . ($Params[8]=='Service-Europa-L'?' selected':'') . '>Service-Europa-L</option>';
	}
	if(($MBEBereiche & 128)==128)			// Service Europa - erstellt am 06.02.08 TR
	{
		echo '<option' . ($Params[8]=='Service-Europa-TSB'?' selected':'') . '>Service-Europa-TSB</option>';
	}
	if(($MBEBereiche & 256)==256)			// Service Europa - erstellt am 06.02.08 TR
	{
		echo '<option' . ($Params[8]=='Service-Europa-WT'?' selected':'') . '>Service-Europa-WT</option>';
	}
	if(($MBEBereiche & 512)==512)			// Service Europa - erstellt am 06.02.08 TR
	{
		echo '<option' . ($Params[8]=='Service-Europa-HUB'?' selected':'') . '>Service-Europa-HUB</option>';
	}
	if(($MBEBereiche & 1024)==1024)			// Service Europa - erstellt am 06.02.08 TR
	{
		echo '<option' . ($Params[8]=='Service-Europa-TWB'?' selected':'') . '>Service-Europa-TWB</option>';
	}
	if(($MBEBereiche & 2048)==2048)			// Service Europa - erstellt am 06.02.08 TR
	{
		echo '<option' . ($Params[8]=='Service-Europa-DLM'?' selected':'') . '>Service-Europa-DLM</option>';
	}
	if(($MBEBereiche & 4096)==4096)			// Service Europa - erstellt am 06.02.08 TR
	{
		echo '<option' . ($Params[8]=='Service-Europa-PM'?' selected':'') . '>Service-Europa-PM</option>';
	}						
	echo "</select></td></tr>";
}
else 
{
	//echo '<input type=Hidden name=txtMIT_BEREICH value=' . ((($MBEBereiche & 1)==1)?'Vertrieb':'ATU Academy') . '>';
	// Filial-QS - erstellt am 21.02.06 CA
	// Gro�kunden - ge�ndert am 06.06.06 CA
	if(($MBEBereiche & 1)==1)			// Vertrieb
	{
		$Bereich='Vertrieb';
	}
	if(($MBEBereiche & 2)==2)			// ATU Academy
	{
		$Bereich='ATU Academy';
	}
	if(($MBEBereiche & 4)==4)			// Filial-QS - erstellt am 21.02.06 CA
	{
		$Bereich='Filial-QS';
	}
	if(($MBEBereiche & 8)==8)			// Gro�kunden - erstellt am 06.06.06 CA
	{
		$Bereich='Gro�kunden';
	}
	if(($MBEBereiche & 16)==16)			// Category Management - erstellt am 19.04.07 CA
	{
		$Bereich='Category Management';
	}
	if(($MBEBereiche & 32)==32)			// Smart-Repair - erstellt am 19.11.07 CA
	{
		$Bereich='Smart-Repair';
	}
	if(($MBEBereiche & 64)==64)			// Service-Europa-L - erstellt am 06.02.08 TR
	{
		$Bereich='Service-Europa-L';
	}
	if(($MBEBereiche & 128)==128)			// Service-Europa-TSB - erstellt am 06.02.08 TR
	{
		$Bereich='Service-Europa-TSB';
	}
	if(($MBEBereiche & 256)==256)			// Service-Europa-WT - erstellt am 06.02.08 TR
	{
		$Bereich='Service-Europa-WT';
	}
	if(($MBEBereiche & 512)==512)			// Service-Europa-HUB - erstellt am 06.02.08 TR
	{
		$Bereich='Service-Europa-HUB';
	}
	if(($MBEBereiche & 1024)==1024)			// Service-Europa-TWB - erstellt am 06.02.08 TR
	{
		$Bereich='Service-Europa-TWB';
	}
	if(($MBEBereiche & 2048)==2048)			// Service-Europa-DLM - erstellt am 06.02.08 TR
	{
		$Bereich='Service-Europa-DLM';
	}
	if(($MBEBereiche & 4096)==4096)			// Service-Europa-PM - erstellt am 06.02.08 TR
	{
		$Bereich='Service-Europa-PM';
	}
	//awis_Debug(1,$Bereich);
	//echo '<input type=Hidden name=txtMIT_BEREICH value=' . ((($MBEBereiche & 1)==1)?'Vertrieb':(($MBEBereiche & 2)==2)?'ATU Academy':((($MBEBereiche & 4)==4)?'Filial-QS':'Gro�kunden')) . '>';
	echo "<input type=Hidden name=txtMIT_BEREICH value='" . $Bereich . "'>";
}

/******* Personal *******/
$EingabeFeld='txtPER_NR';
echo '<tr><td width=190>Mitar<u>b</u>eiter</td>';
echo '<td><select name=txtMIT_KEY accesskey=b tabindex=10>';

if($MBEBereicheAnz==1)
{
	$SQL = "SELECT MIT_KEY, MIT_BEZEICHNUNG,MIT_REZ_ID, MIT_STATUS ";
}
else 
{
	$SQL = "SELECT MIT_KEY, MIT_BEZEICHNUNG || ' (' || MIT_BEREICH || ')' AS MIT_BEZEICHNUNG,MIT_REZ_ID, MIT_STATUS";
}

$SQL .= " FROM Mitarbeiter";
$Bedingung = "";
if(($MBEBereiche&1)==1)			// Vertrieb
{
	$Bedingung .= " OR MIT_Bereich='Vertrieb'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&2)==2)			// PersEnt
{
	$Bedingung .= " OR MIT_Bereich='ATU Academy'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&4)==4)			// Filial-QS - erstellt am 21.02.06 CA
{
	$Bedingung .= " OR MIT_Bereich='Filial-QS'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&8)==8)			// Gro�kunden - erstellt am 06.06.06 CA
{
	$Bedingung .= " OR MIT_Bereich='Gro�kunden'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&16)==16)			// Category Management - erstellt am 19.04.07 CA
{
	$Bedingung .= " OR MIT_Bereich='Category Management'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&32)==32)			// Smart-Repair - erstellt am 19.11.07 CA
{
	$Bedingung .= " OR MIT_Bereich='Smart-Repair'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&64)==64)			// Service-Europa-L - erstellt am 06.02.08 TR
{
	$Bedingung .= " OR MIT_Bereich='Service-Europa-L'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&128)==128)			// Service-Europa-TSB - erstellt am 06.02.08 TR
{
	$Bedingung .= " OR MIT_Bereich='Service-Europa-TSB'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&256)==256)			// Service-Europa-WT - erstellt am 06.02.08 TR
{
	$Bedingung .= " OR MIT_Bereich='Service-Europa-WT'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&512)==512)			// Service-Europa-HUB - erstellt am 06.02.08 TR
{
	$Bedingung .= " OR MIT_Bereich='Service-Europa-HUB'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&1024)==1024)			// Service-Europa-TWB - erstellt am 06.02.08 TR
{
	$Bedingung .= " OR MIT_Bereich='Service-Europa-TWB'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&2048)==2048)			// Service-Europa-DLM - erstellt am 06.02.08 TR
{
	$Bedingung .= " OR MIT_Bereich='Service-Europa-DLM'";
	$MBEBereicheAnz++;
}
if(($MBEBereiche&4096)==4096)			// Service-Europa-PM - erstellt am 06.02.08 TR
{
	$Bedingung .= " OR MIT_Bereich='Service-Europa-PM'";
	$MBEBereicheAnz++;
}
$SQL .= " WHERE " . substr($Bedingung,3);
$SQL .= " ORDER BY MIT_BEZEICHNUNG";


$rsPER = awisOpenRecordset($con, $SQL);
$rsPERZeilen = $awisRSZeilen;


echo "<option value=0>::alle::</option>";
for($PerZeile=0;$PerZeile<$rsPERZeilen;$PerZeile++)
{
	if($RZUserListe!=';')
	{
		if(strstr($RZUserListe,';'.$rsPER['MIT_REZ_ID'][$PerZeile].';')=='')
		{
			continue;
		}
	}
	echo "<option value='" . $rsPER['MIT_KEY'][$PerZeile] . "'";
	
	if($Params[0]=="on")
	{
		if($Params[1]!='' AND $Params[1]==$rsPER['MIT_KEY'][$PerZeile])		
		{
			echo ' selected ';
		}
	}
	$Status='';
	if ($rsPER['MIT_STATUS'][$PerZeile]=='A')
	{
		$Status='(Aktiv)';
	}
	elseif ($rsPER['MIT_STATUS'][$PerZeile]=='S')
	{
		$Status='(Stillgelegt)';
	}
	echo ">" . $rsPER['MIT_BEZEICHNUNG'][$PerZeile] ." " .$Status . "</option>";
}

echo "</select></td></tr>";

/******* Mitarbeitert�tigkeiten ********/
echo '<tr>';
echo '<td>T�tigkeit</td>';
echo '<td>';
echo '<select name=txtMIT_MTA_ID tabindex=18>';
echo '<option value=-1>::alle::</option>';
$rsPER = awisOpenRecordset($con, "SELECT * FROM MitarbeiterTaetigkeiten ORDER BY MTA_BEZEICHNUNG");
$rsPERZeilen = $awisRSZeilen;

for($PerZeile=0;$PerZeile<$rsPERZeilen;$PerZeile++)
{
	echo "<option value='" . $rsPER['MTA_ID'][$PerZeile] . "'";
	echo ">" . $rsPER['MTA_BEZEICHNUNG'][$PerZeile] . "</option>";
}
echo '</select>';
echo '</td></tr>';	
unset($rsPER);

/******* Datum *******/
echo '<tr>';
echo '<td>Datum vom</td>';
echo '<td><input name=txtDatumVom size=8 tabindex=20 value=' . ($Params[0]=="on"?$Params[2]:date('d.m.Y')) . '>';
echo '&nbsp;bis&nbsp;<input name=txtDatumBis size=8 tabindex=21 value=' . ($Params[0]=="on"?$Params[3]:date('d.m.Y')) . '></td>';
echo '</tr>';

/******* Filiale *******/
echo '<tr>';
echo '<td>Filiale</td>';
echo '<td><input name=txtFIL_ID size=5 tabindex=30 value=' . ($Params[0]=="on"?$Params[4]:'')  . '></td>';
echo '</tr>';

/******* Regionalzentren *******/

echo '<tr>';
echo '<td>Regionalzentrum</td>';
echo '<td>';
$rsREZ = awisOpenRecordset($con,"SELECT * FROM Regionalzentren ORDER BY REZ_BEZEICHNUNG");
$rsREZZeilen = $awisRSZeilen;
echo "<select name=txtMIT_REZ_ID tabindex=40>";

if($RZUserListe==';')
{
	echo '<option value=0>::alle Regionalzentren::</option>';
}

for($i=0; $i<$rsREZZeilen;$i++)
{
	if($RZUserListe!=';')
	{
		if(strstr($RZUserListe,';'.$rsREZ["REZ_ID"][$i].';')=='')
		{
			continue;
		}
	}
	echo "<option ";
	if($rsREZ["REZ_ID"][$i] == ($Params[0]=="on"?$Params[5]:''))
	{
  		echo " selected ";
	}
	echo "value=" . $rsREZ["REZ_ID"][$i] . ">". $rsREZ["REZ_BEZEICHNUNG"][$i] . "</option>";
}
echo "</select></td></tr>";

// Kategorien
echo '<td>Kategorie</td>';
echo '<td>';
echo '<select name=txtMEI_MEA_KEY  tabindex=50>';
echo '<option value=-1>::alle::</option>';
$rsMEA = awisOpenRecordset($con, "SELECT * FROM MITARBEITEREINSAETZEARTEN ORDER BY MEA_BEZEICHNUNG");
$rsMEAZeilen = $awisRSZeilen;

for($MEAZeile=0;$MEAZeile<$rsMEAZeilen;$MEAZeile++)
{
	echo "<option value='" . $rsMEA['MEA_KEY'][$MEAZeile] . "'";

	if($rsMEA["MEA_KEY"][$MEAZeile] ==($Params[0]=="on"?$Params[6]:''))
	{
  		echo " selected ";
	}
	echo ">" . $rsMEA['MEA_BEZEICHNUNG'][$MEAZeile] . "</option>";
}
echo '</select>';
echo '</td>';	
unset($rsMEA);


/******* Auswahl speichern *******/
echo '<tr><td colspan=2>&nbsp;</td></tr>';
print "<tr><td width=190>Auswahl <u>s</u>peichern:</td>";
print "<td><input type=checkbox value=on " . ($Params[0]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=80></td></tr>";
	
echo '</table>';

	// Weiter und Reset - Buttons
echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png alt='Formularinhalt l�schen' name=cmdReset onclick=location.href='./perseins_Main.php?Reset=True';>";
print "<input type=hidden name=cmdAktion value=Einsaetze>";

echo '</form>';

/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('txtMIT_KEY')[0].focus();";
	echo "\n</Script>";
}

?>
