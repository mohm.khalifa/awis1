/***************************************************************************
* Tabellendefinitionen : EXPRabattZuordnungen
* Erstellt am : 14.12.2016 16:08:01
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE TABLE EXPRabattZuordnungen(
ERZ_KEY NUMBER(10,0) CONSTRAINT CNN_ERZ_KEY NOT NULL,
ERZ_KUNDEN_NR NUMBER(10,0) DEFAULT '0',
ERZ_ERM_NR NUMBER(10,0) DEFAULT '0',
CONSTRAINT PK_ERZ_KEY PRIMARY KEY(ERZ_KEY) USING Index TABLESPACE INDX) 
;


/***************************************************************************
* Kommentare : EXPRabattZuordnungen
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

COMMENT ON COLUMN EXPRabattZuordnungen.ERZ_KEY IS 'KEY';
COMMENT ON COLUMN EXPRabattZuordnungen.ERZ_KUNDEN_NR IS 'KUNDEN_NR vom COM-Server';
COMMENT ON COLUMN EXPRabattZuordnungen.ERZ_ERM_NR IS 'Rabattmodel_Nr';


/***************************************************************************
* Indexdefinitionen : EXPRabattZuordnungen
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Sequenz erstellen : EXPRabattZuordnungen
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

CREATE SEQUENCE SEQ_ERZ_KEY INCREMENT BY 1 START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trg_seq_ERZ_KEY
     BEFORE INSERT ON EXPRabattZuordnungen
     REFERENCING NEW AS NEW OLD AS OLD
     FOR EACH ROW
       BEGIN
         :NEW.ERZ_KEY := seq_ERZ_KEY.NEXTVAL ;
     END;
/ 


/***************************************************************************
* FOREIGN KEYS
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Tabelle dokumentieren
* Erstellt am : 14.12.2016 16:08:01
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DELETE FROM TabellenKuerzel WHERE XTN_KUERZEL='ERZ';

INSERT INTO TabellenKuerzel(XTN_KUERZEL, XTN_TABELLENNAME, XTN_DATENQUELLE)
 VALUES('ERZ', 'EXPRabattZuordnungen', 'ORACLE')
;
DELETE FROM Textkonserven WHERE XTX_KENNUNG='ERZ' AND XTX_BEREICH='Tabelle';

INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE)
 VALUES('ERZ','Tabelle', 'EXPRabattZuordnungen', 'EXPRabattZuordnungen')
;


/***************************************************************************
* Ende des Skripts EXPRabattZuordnungen
****************************************************************************
* Fertig am : 14.12.2016 16:08:01
***************************************************************************/

COMMIT;
