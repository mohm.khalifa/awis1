<?php

require_once('/daten/include/awisZukauf.inc');

try {
    $Benutzer = '';
    $DebugLevel = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;
    $WANummer = '';
    $Abfrage = 'Bestellung';
    $Status = '';
    $Bestaetigen = true;
    $DebugModus = false;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--wanummer':
                $WANummer = $argv[++$i];
                break;
            case '--starttag':
                $StartTag = $argv[++$i];
                $Status = 'A';
                break;
            case '--endtag':
                $EndTag = $argv[++$i];
                $Status = 'A';
                break;
            case '--abfrage':
                $Abfrage = $argv[++$i];
                break;
            case '--workaround':
                $StartTag = date('d.m.y');
                $EndTag = date('d.m.y');
                $Status = 'A';
                break;
            case '--nichtbestaetigen':
                $Bestaetigen = false;
                break;
            case '--debugmodus':
                $DebugModus = true;
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo '  --wanummer  <Nr>                   WANummer fuer die Abfrage.' . PHP_EOL;
                echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.' . PHP_EOL;
                echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.' . PHP_EOL;
                echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung.' . PHP_EOL;

                echo PHP_EOL;
                die();
        }
    }

    /**
     * Bei Trost gibt es verschiedene Status.
     * leer/blank = alle nicht angefragten Bestellungen
     * U = Bestellung angefragt, aber nicht best�tigt
     * A = abgeholt und best�tigt
     *
     * Deswegen muss bei Blank auch immer U mit angefragt werden.
     *
     */
    $AnfrageStatus = array($Status);
    if($Status == ''){
        $AnfrageStatus[] = 'U';
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $ZUK = new awisZukauf_TROST($DebugLevel, false);

    if($DebugModus){
        $ZUK->DebugModus();
    }

    if ($StartTag != '') {
        $StartTag = date('d.m.y', $Form->PruefeDatum($StartTag, false, false, true));
        $EndTag = date('d.m.y', $Form->PruefeDatum($EndTag, false, false, true));

        echo 'Start ' . $StartTag . PHP_EOL;
        echo 'Ende ' . $EndTag . PHP_EOL;

    }

    switch ($Abfrage) {
        case 'Bestellung':
            foreach ($AnfrageStatus as $Status){
                if ($ZUK->LeseBestellungen($WANummer, $StartTag, $EndTag, $Status, '') > 0) {
                    $ZUK->SpeichereBestellungen($Bestaetigen);
                }
            }
            break;
    }
} catch (awisException $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'SQL: ' . $ex->getSQL() . PHP_EOL;
} catch (Exception $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
}
?>
