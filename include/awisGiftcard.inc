<?php

require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once('awisBenutzer.inc');

class awisGiftcard
{
	private $_DB = null;
	private $_AWISBenutzer = null;
	private $_DebugLevel = 99;
	private $_SOAPClient = null;
    private $_DebugText='';
	private $Werkzeug;
	private $_EMAIL_Infos = array('shuttle@de.atu.eu', 'tamara.bannert@de.atu.eu');

	public function __construct()
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();
		$this->_AWISBenutzer = awisBenutzer::Init();
		$this->_FORM = new awisFormular();
		$this->Werkzeug = new awisWerkzeuge();
	}

    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Generiert einen DebugEintrag und gibt diesen aus
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText = date('d.m.Y H:i:s');
            $this->_DebugText .=  ': ' . $Typ;
            $this->_DebugText .=  ': ' . $Text;
            $this->_DebugText .=  PHP_EOL;
        }
        echo $this->_DebugText;
    }

	public function _GiftCardVerarbeitung()
    {
        $this->debugAusgabe('oeffne SOAP');
        //URL ob Giftcard Produktiv oder Entwick => Programmparameter suchen
        $WSDL = $this->_AWISBenutzer->ParameterLesen('Giftcard WSDL');
        $WSDL .= '?WSDL';

        $Parameter = array(
            'trace' => 1,
            'exceptions' => 1,
            'timeout' => 25,
            'soap_version' => SOAP_1_2
        );
        $this->_SOAPClient = new SoapClient($WSDL, $Parameter);

        try {
            //Alle Gutscheine holen die noch offen sind bzw. noch bearbeitet werden m�ssen.
            //Wenn Vorgang zum Verarbeiten vorhanden => verarbeiten
            $SQL = 'SELECT kopf.GSK_AUFTRAGNR,';
            $SQL .= ' kopf.GSK_STATUS,';
            $SQL .= ' kopf.GSK_JOBSTATUS,';
            $SQL .= ' kopf.GSK_AUFTRAGGEBER,';
            $SQL .= ' kopf.GSK_KST,';
            $SQL .= ' kopf.GSK_GKU_KEY,';
            $SQL .= ' kopf.GSK_KUNDE_INTERN,';
            $SQL .= ' kopf.GSK_GSV_KEY,';
            $SQL .= ' vor.GSV_PROFILGC,';
            $SQL .= ' vor.GSV_STATUS,';
            $SQL .= ' vor.GSV_WAEHRUNG,';
            $SQL .= ' kopf.GSK_FREITEXT,';
            $SQL .= ' kun.GKU_VERTRAGNR,';
            $SQL .= ' kun.GKU_KONZERN';
            $SQL .= ' FROM GUTSCHEINKOPFDATEN kopf';
            $SQL .= ' INNER JOIN GUTSCHEINVORLAGE vor';
            $SQL .= ' ON kopf.GSK_GSV_KEY=vor.GSV_KEY';
            $SQL .= ' LEFT JOIN GUTSCHEINKUNDEEXTERN kun on kopf.GSK_GKU_KEY = kun.GKU_KEY';
            $SQL .= ' WHERE (kopf.GSK_STATUS = \'O\' and kopf.GSK_JOBSTATUS = \'O\')';
            $SQL .= '  or (kopf.GSK_STATUS = \'O\' and kopf.GSK_JOBSTATUS = \'F\')';
            $SQL .= ' or (kopf.GSK_STATUS = \'B\' and kopf.GSK_JOBSTATUS = \'F\')';
            $SQL .= ' ';
            $rsOffeneKopf = $this->_DB->RecordSetOeffnen($SQL);

            $this->debugAusgabe('Anzahl rsOffenerKopf: ' . $rsOffeneKopf->AnzahlDatensaetze());

            while (!$rsOffeneKopf->EOF()) {
                //1.Gutscheinnummern ziehen wenn Menge ungleich mit Gutschein in Tabelle######################################################################################################################
                $SQLPOS = 'SELECT pos.GSP_POSITION,';
                $SQLPOS .= ' pos.GSP_BETRAG,';
                $SQLPOS .= ' pos.GSP_MENGE,';
                $SQLPOS .= ' pos.GSP_STATUS,';
                $SQLPOS .= ' pos.GSP_BUCHART,';
                $SQLPOS .= ' pos.GSP_USER';
                $SQLPOS .= ' FROM GUTSCHEINPOSDATEN pos';
                $SQLPOS .= ' WHERE pos.GSP_AUFTRAGNR = \'' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '\'';
                $SQLPOS .= ' AND pos.GSP_BUCHART <> GSP_STATUS';
                $SQLPOS .= ' ';
                $rsOffenePos = $this->_DB->RecordSetOeffnen($SQLPOS);

                $this->debugAusgabe('Anzahl rsOffenePos: ' . $rsOffenePos->AnzahlDatensaetze());

                while (!$rsOffenePos->EOF()) {
                    //Pr�fen wieviele Gutscheine bereits gezogen wurden, Restmenge  muss noch verarbeitet werden
                    $SQLPOS_VORHANDEN = 'SELECT count(*) as ANZAHL ';
                    $SQLPOS_VORHANDEN .= 'FROM GUTSCHEINE pos ';
                    $SQLPOS_VORHANDEN .= 'WHERE pos.GPD_AUFTRAGNR = ' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR') . ' ';
                    $SQLPOS_VORHANDEN .= 'AND pos.GPD_POSITION = ' . $rsOffenePos->FeldInhalt('GSP_POSITION');

                    $rsVorhandenePOS = $this->_DB->RecordSetOeffnen($SQLPOS_VORHANDEN);
                    $ISTMenge = $rsVorhandenePOS->FeldInhalt('ANZAHL');
                    $SOLLMenge = $rsOffenePos->FeldInhalt('GSP_MENGE');

                    $this->debugAusgabe('GPD_Auftragsnr: ' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR') . ' / GPD_Pos: ' . $rsOffenePos->FeldInhalt('GSP_POSITION'));
                    $this->debugAusgabe('Anzahl Gutscheine / IstMenge: ' . $ISTMenge . ' // ' . 'SollMenge: ' . $SOLLMenge);

                    if ($ISTMenge <> $SOLLMenge) {
                        //Gutscheinnummern ziehen wenn noch nicht alle vorhanden
                        for ($ISTMenge; $ISTMenge < $SOLLMenge; $ISTMenge++) {
                            //Felder an GiftCard �bermitteln: STORE, OPERATOR, COMMENT, TIMESTAMP
                            $STORE_ID = 'Zentrale';
                            $OPERATOR_NAME = $rsOffenePos->FeldInhalt('GSP_USER');
                            $POS_TIMESTAMP = date("Y-m-d H:i:s");
                            $POS_DATE = $this->_DB->FeldInhaltFormat('DU', $POS_TIMESTAMP);

                            if ($rsOffeneKopf->FeldInhalt('GSK_FREITEXT') != '' and $rsOffeneKopf->FeldInhalt('GKU_VERTRAGNR') != '') {
                                $POS_COMMENT = $rsOffeneKopf->FeldInhalt('GSK_FREITEXT') . ' - ' . $rsOffeneKopf->FeldInhalt('GKU_VERTRAGNR');
                            } else {
                                $POS_COMMENT = $rsOffeneKopf->FeldInhalt('GSK_FREITEXT') . $rsOffeneKopf->FeldInhalt('GKU_VERTRAGNR');
                            }

                            //GETCARDID
                            $Erg = $this->GetCardID($this->_SOAPClient, $rsOffeneKopf->FeldInhalt('GSV_PROFILGC'), $STORE_ID, $OPERATOR_NAME, $POS_COMMENT, $POS_TIMESTAMP);

                            //Ergebnis auf Status Code pr�fen
                            //Wenn Fehler, dann Abbruch und Fehlermail schicken
                            $STATUS_CODE = $Erg['STATUS_CODE'];

                            if ($STATUS_CODE == 0) {
                                //Response von GetCardID
                                $CARD_ID = $Erg['CARD_ID'];
                                $TRANS_ID = $Erg['TRANS_ID'];
                                $STATUS_MSG = $Erg['STATUS_MSG'];
                                $CARD_PIN = $Erg['CARD_PIN'];
                                $PruefZiffer = $this->PruefZiffer($CARD_ID);

                                //Insert in Gutscheine mit Response Ergebnis GetCardID
                                $SQL_INSERT = 'INSERT INTO GUTSCHEINE (GPD_GUTSCHEINNR,';
                                $SQL_INSERT .= ' GPD_PRUEFZ,';
                                $SQL_INSERT .= ' GPD_POSITION,';
                                $SQL_INSERT .= ' GPD_AUFTRAGNR,';
                                $SQL_INSERT .= ' GPD_DATUM,';
                                $SQL_INSERT .= ' GPD_KOMMENTAR,';
                                $SQL_INSERT .= ' GPD_BUCHART,';
                                $SQL_INSERT .= ' GPD_STATUS,';
                                $SQL_INSERT .= ' GPD_STATUS_MSG,';
                                $SQL_INSERT .= ' GPD_USER,';
                                $SQL_INSERT .= ' GPD_USERDAT,';
                                $SQL_INSERT .= ' GPD_TRANSID,';
                                $SQL_INSERT .= ' GPD_STATUS_CODE,';
                                $SQL_INSERT .= ' GPD_UNIT,';
                                $SQL_INSERT .= ' GPD_STORE,';
                                $SQL_INSERT .= ' GPD_OPERATOR,';
                                $SQL_INSERT .= ' GPD_CARD_PIN,';
                                $SQL_INSERT .= ' GPD_AMOUNT) VALUES(';
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $CARD_ID) . ' ,';                                    //Gutscheinnummer
                                $SQL_INSERT .= $PruefZiffer . ' ,';                                    //Pr�fziffer
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $rsOffenePos->FeldInhalt('GSP_POSITION')) . ' ,';    //Postionsnummer
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . ' ,';    //Auftragnr
                                $SQL_INSERT .= '' . $POS_DATE . ' ,';                                        //Anlagedatum
                                $SQL_INSERT .= '' . $this->_DB->FeldInhaltFormat('T', $POS_COMMENT) . ' ,';                                    //Kommentar
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $rsOffenePos->FeldInhalt('GSP_BUCHART')) . ' ,';      //Buchungsart
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', 'P') . ' ,';                                         //Gutscheinstatus
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $STATUS_MSG) . ' ,';                                //Status Message
                                $SQL_INSERT .= '\'' . $this->_AWISBenutzer->BenutzerName() . '\' ,';                                //USER
                                $SQL_INSERT .= ' sysdate,';                                                                            //USERDAT
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $TRANS_ID) . ' ,';                                    //TransID
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $STATUS_CODE) . ' ,';                                //Status Code
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $rsOffeneKopf->FeldInhalt('GSV_WAEHRUNG')) . ' ,';    //UNIT
                                $SQL_INSERT .= ' \'' . $STORE_ID . '\',';                                                                //STORE_ID
                                $SQL_INSERT .= ' \'' . $OPERATOR_NAME . '\',';                                                            //OPERATOR_NAME
                                $SQL_INSERT .= $this->_DB->FeldInhaltFormat('T', $CARD_PIN) . ' ,';                                    //CARDPIN
                                $SQL_INSERT .= '\'0\') ';                                                                            //AMOUNT
                                $this->_DB->Ausfuehren($SQL_INSERT, '', true);
                            } else {
                                //Fehlermail schicken mit STATUS_CODE und STATUS_MSG
                                //Response von GetCardID
                                $TRANS_ID = $Erg['TRANS_ID'];
                                $STATUS_MSG = $Erg['STATUS_MSG'];

                                $Text = '';
                                //Aktivierung konnte nicht durchgef�hrt werden => Fehlermail + die()
                                $Text = 'Gutscheinnummer ziehen vom GiftCard ist fehlgeschlagen' . PHP_EOL;
                                $Text .= 'Auftrag: ' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR') . PHP_EOL;
                                $Text .= 'R�ckmeldung GiftCard' . PHP_EOL;
                                $Text .= 'Status-Code: ' . $STATUS_CODE . PHP_EOL;
                                $Text .= 'Status-MSG: ' . $STATUS_MSG . PHP_EOL;
                                $Text .= 'Status-TRANSID: ' . $TRANS_ID . PHP_EOL;
                                $Text .= 'Gutscheinprofil: ' . $rsOffeneKopf->FeldInhalt('GSV_PROFILGC');

                                $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_AKTIVIERUNG', $Text, 2, '', 'shuttle@de.atu.eu');

                                die();
                            }
                        }
                        //Update alle GSNr gezogen
                        $SQL = 'UPDATE GUTSCHEINPOSDATEN pos SET';
                        $SQL .= ' pos.GSP_STATUS= \'P\'';
                        $SQL .= ' WHERE pos.GSP_AUFTRAGNR=' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '';
                        $SQL .= ' AND pos.GSP_POSITION=' . $this->_FORM->Format('T', $rsOffenePos->FeldInhalt('GSP_POSITION')) . '';
                        $this->_DB->Ausfuehren($SQL, '', true);
                    } else {
                        //Update Status alle GSNr gezogen; aber Status noch ungleich
                        $SQL = 'UPDATE GUTSCHEINPOSDATEN pos SET';
                        $SQL .= ' pos.GSP_STATUS= \'P\'';
                        $SQL .= ' WHERE pos.GSP_AUFTRAGNR=' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '';
                        $SQL .= ' AND pos.GSP_POSITION=' . $this->_FORM->Format('T', $rsOffenePos->FeldInhalt('GSP_POSITION')) . '';
                        $this->_DB->Ausfuehren($SQL, '', true);
                    }
                    $rsOffenePos->DSWeiter();
                }



                //2.Aktualisierung je GS Status ##############################################################################################################################################################
                $SQLGS = 'SELECT gs.GPD_POSITION,';
                $SQLGS .= ' gs.GPD_AUFTRAGNR,';
                $SQLGS .= ' gs.GPD_AMOUNT,';
                $SQLGS .= ' gs.GPD_STATUS,';
                $SQLGS .= ' pos.GSP_BETRAG,';
                $SQLGS .= ' gs.GPD_GUTSCHEINNR,';
                $SQLGS .= ' gs.GPD_BUCHART';
                $SQLGS .= ' FROM GUTSCHEINE gs';
                $SQLGS .= ' LEFT JOIN GUTSCHEINPOSDATEN pos ON gs.GPD_AUFTRAGNR = pos.GSP_AUFTRAGNR';
                $SQLGS .= ' AND gs.GPD_POSITION = pos.GSP_POSITION';
                $SQLGS .= ' WHERE gs.GPD_AUFTRAGNR = \'' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '\'';
                $SQLGS .= ' AND gs.GPD_BUCHART <> gs.GPD_STATUS';
                $SQLGS .= ' ';
                $rsOffeneGS = $this->_DB->RecordSetOeffnen($SQLGS);

                while (!$rsOffeneGS->EOF()) {
                    if ($rsOffeneGS->FeldInhalt('GPD_BUCHART') == 'A') {
                        if ($rsOffeneGS->FeldInhalt('GPD_AMOUNT') == 0) {
                            //Aktivierung
                            //GS aktivieren mit Betrag
                            $TransID = $this->GET_TRANS_ID($this->_SOAPClient, $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR'));
                            $Erg = $this->Activation($this->_SOAPClient, $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR'), $TransID['TRANS_ID'], str_replace(',', '.', ($rsOffeneGS->FeldInhalt('GSP_BETRAG', 'N2'))), $rsOffeneKopf->FeldInhalt('GSV_WAEHRUNG'));

                            $STATUS_CODE = $Erg['STATUS_CODE'];

                            if ($STATUS_CODE == 0) {
                                //Response von GetCardID
                                $TRANS_ID = $Erg['TRANS_ID'];
                                $STATUS_MSG = $Erg['STATUS_MSG'];
                                $AMOUNT = $Erg['AMOUNT'];

                                //Insert in Gutscheine mit Response Ergebnis GetCardID
                                $SQL_UPDATE = 'UPDATE GUTSCHEINE gs SET';
                                $SQL_UPDATE .= ' gs.GPD_STATUS = \'A\',';
                                $SQL_UPDATE .= ' gs.GPD_TRANSID = \'' . $TRANS_ID . '\',';
                                $SQL_UPDATE .= ' gs.GPD_AMOUNT = ' . $AMOUNT . ',';
                                $SQL_UPDATE .= ' gs.GPD_STATUS_CODE = \'' . $STATUS_CODE . '\',';
                                $SQL_UPDATE .= ' gs.GPD_STATUS_MSG = \'' . $STATUS_MSG . '\'';
                                $SQL_UPDATE .= ' WHERE gs.GPD_AUFTRAGNR = \'' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '\'';
                                $SQL_UPDATE .= ' AND gs.GPD_POSITION = \'' . $this->_FORM->Format('T', $rsOffeneGS->FeldInhalt('GPD_POSITION')) . '\'';
                                $SQL_UPDATE .= ' AND gs.GPD_GUTSCHEINNR= \'' . $this->_FORM->Format('T', $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR')) . '\'';
                                $this->_DB->Ausfuehren($SQL_UPDATE, '', true);
                            } else {
                                //Response von GetCardID
                                $TRANS_ID = $Erg['TRANS_ID'];
                                $STATUS_MSG = $Erg['STATUS_MSG'];

                                //Aktivierung konnte nicht durchgef�hrt werden => Fehlermail + die()
                                $Text = 'Aktivierung des Gutscheines: ' . $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR') . ' konnte nicht durchgef�hrt werden.' . PHP_EOL;
                                $Text .= 'Auftrag: ' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR') . PHP_EOL;
                                $Text .= 'R�ckmeldung GiftCard' . PHP_EOL;
                                $Text .= 'Status-Code: ' . $STATUS_CODE . PHP_EOL;
                                $Text .= 'Status-MSG: ' . $STATUS_MSG . PHP_EOL;
                                $Text .= 'Status-TRANSID: ' . $TRANS_ID . PHP_EOL;
                                $Text .= 'CARD_ID: ' . $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR') . PHP_EOL;
                                $Text .= 'TRANS_ID: ' . $TransID . PHP_EOL;
                                $Text .= 'AMOUNT: ' . $rsOffeneGS->FeldInhalt('GSP_BETRAG') . PHP_EOL;
                                $Text .= 'UNIT_TYPE: ' . $rsOffeneKopf->FeldInhalt('GSV_WAEHRUNG') . PHP_EOL;

                                $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_AKTIVIERUNG', $Text, 2, '', 'shuttle@de.atu.eu');

                                die();
                            }
                        } else {
                            //Update GS auf Aktiv
                            $SQL_UPDATE = 'UPDATE GUTSCHEINE gs SET';
                            $SQL_UPDATE .= ' gs.GPD_STATUS = \'A\'';
                            $SQL_UPDATE .= ' WHERE gs.GPD_AUFTRAGNR = \'' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '\'';
                            $SQL_UPDATE .= ' AND gs.GPD_POSITION = \'' . $this->_FORM->Format('T', $rsOffeneGS->FeldInhalt('GPD_POSITION')) . '\'';
                            $SQL_UPDATE .= ' AND gs.GPD_GUTSCHEINNR= \'' . $this->_FORM->Format('T', $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR')) . '\'';
                            $this->_DB->Ausfuehren($SQL_UPDATE, '', true);
                        }
                    } elseif ($rsOffeneGS->FeldInhalt('GPD_BUCHART') == 'S') {
                        //GS Deaktivieren
                        $Erg = $this->Deactivate($this->_SOAPClient, $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR'));

                        $STATUS_CODE = $Erg['STATUS_CODE'];

                        if ($STATUS_CODE == 0) {
                            //Response von GetCardID
                            $TRANS_ID = $Erg['TRANS_ID'];
                            $STATUS_MSG = $Erg['STATUS_MSG'];

                            //Update in Gutscheine mit Response Ergebnis
                            $SQL_UPDATE = 'UPDATE GUTSCHEINE gs SET';
                            $SQL_UPDATE .= ' gs.GPD_STATUS = \'S\',';
                            $SQL_UPDATE .= ' gs.GPD_TRANSID = \'' . $TRANS_ID . '\',';
                            $SQL_UPDATE .= ' gs.GPD_STATUS_CODE = \'' . $STATUS_CODE . '\',';
                            $SQL_UPDATE .= ' gs.GPD_STATUS_MSG = \'' . $STATUS_MSG . '\'';
                            $SQL_UPDATE .= ' WHERE gs.GPD_AUFTRAGNR = \'' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '\'';
                            $SQL_UPDATE .= ' AND gs.GPD_POSITION = \'' . $this->_FORM->Format('T', $rsOffeneGS->FeldInhalt('GPD_POSITION')) . '\'';
                            $SQL_UPDATE .= ' AND gs.GPD_GUTSCHEINNR= \'' . $this->_FORM->Format('T', $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR')) . '\'';
                            $this->_DB->Ausfuehren($SQL_UPDATE, '', true);
                        } else {
                            //Stornierung konnte nicht durchgef�hrt werden => Fehlermail + die()
                            //Response von GetCardID
                            $TRANS_ID = $Erg['TRANS_ID'];
                            $STATUS_MSG = $Erg['STATUS_MSG'];

                            //Stornierung konnte nicht durchgef�hrt werden => Fehlermail + die()
                            $Text = 'Stornierung des Gutscheines: ' . $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR') . ' konnte nicht durchgef�hrt werden.' . PHP_EOL;
                            $Text .= 'Auftrag: ' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR') . PHP_EOL;
                            $Text .= 'R�ckmeldung GiftCard' . PHP_EOL;
                            $Text .= 'Status-Code: ' . $STATUS_CODE . PHP_EOL;
                            $Text .= 'Status-MSG: ' . $STATUS_MSG . PHP_EOL;
                            $Text .= 'Status-TRANSID: ' . $TRANS_ID . PHP_EOL;
                            $Text .= PHP_EOL . 'CARD_ID: ' . $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR') . PHP_EOL;

                            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_AKTIVIERUNG', $Text, 2, '', 'shuttle@de.atu.eu');

                            die();
                        }

                        //Update GS auf Deaktiviert
                        $SQL = 'UPDATE GUTSCHEINE gs SET';
                        $SQL .= ' gs.GPD_STATUS= \'S\'';
                        $SQL .= ' WHERE gs.GPD_AUFTRAGNR = \'' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '\'';
                        $SQL .= ' AND gs.GPD_POSITION = \'' . $this->_FORM->Format('T', $rsOffeneGS->FeldInhalt('GSP_POSITION')) . '\'';
                        $this->_DB->Ausfuehren($SQL, '', true);
                    } else {
                        //Falsche Buchungsart angegeben => Abbruch
                        $Text = 'Falsche Buchungsart wurde angegeben!' . PHP_EOL;
                        $Text .= 'Buchungsart: ' . $rsOffeneGS->FeldInhalt('GPD_BUCHART') . PHP_EOL;
                        $Text .= 'Auftragnr: ' . $rsOffeneGS->FeldInhalt('GPD_AUFTRAGNR') . PHP_EOL;
                        $Text .= 'CARD_ID: ' . $rsOffeneGS->FeldInhalt('GPD_GUTSCHEINNR');

                        $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_AKTIVIERUNG', $Text, 2, '', 'shuttle@de.atu.eu');
                        die();
                    }
                    $rsOffeneGS->DSWeiter();
                }

                //3.Nach GS Aktualisierung muss Posdaten Status noch angepasst werden###########################################################################################################################
                $Statusneu = '';

                $SQL = 'Select pos.GSP_AUFTRAGNR, pos.GSP_POSITION';
                $SQL .= ' From GUTSCHEINPOSDATEN pos ';
                $SQL .= ' WHERE pos.GSP_AUFTRAGNR=' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR');
                $SQL .= ' group by pos.GSP_AUFTRAGNR, pos.GSP_POSITION';
                $rsPOSAKT = $this->_DB->RecordSetOeffnen($SQL);

                while (!$rsPOSAKT->EOF()) {
                    $Statusneu = '';

                    $SQL = 'Select gs.GPD_BUCHART';
                    $SQL .= ' From GUTSCHEINE gs';
                    $SQL .= ' inner join GUTSCHEINPOSDATEN pos on gs.GPD_AUFTRAGNR=pos.GSP_AUFTRAGNR';
                    $SQL .= ' and gs.GPD_POSITION=pos.GSP_POSITION';
                    $SQL .= ' and gs.GPD_AUFTRAGNR=' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR');
                    $SQL .= ' and gs.GPD_POSITION=' . $rsPOSAKT->FeldInhalt('GSP_POSITION');
                    $SQL .= ' group by gs.GPD_BUCHART';
                    $rsBuchStatus = $this->_DB->RecordSetOeffnen($SQL);

                    while (!$rsBuchStatus->EOF()) {
                        $Statusneu = $rsBuchStatus->FeldInhalt('GPD_BUCHART') . ' ' . $Statusneu;
                        $rsBuchStatus->DSWeiter();
                    }

                    //UPDATE der Position
                    $SQL_UPDATEALLE = 'UPDATE GUTSCHEINPOSDATEN pos SET';
                    $SQL_UPDATEALLE .= ' pos.GSP_STATUS = \'' . $Statusneu . '\'';
                    $SQL_UPDATEALLE .= ' WHERE pos.GSP_AUFTRAGNR = ' . $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR') . '';
                    $SQL_UPDATEALLE .= ' AND pos.GSP_POSITION = ' . $rsPOSAKT->FeldInhalt('GSP_POSITION') . '';
                    $this->_DB->Ausfuehren($SQL_UPDATEALLE, '', true);

                    $rsPOSAKT->DSWeiter();
                }

                //4.Pr�fung ob alle GS einen endg�ltigen BuchungsStatus haben => Kopf updaten auf Erledigt ##############################################################################################################################################################
                $SQLKopfUpdate = 'SELECT gs.GPD_AUFTRAGNR,gs.GPD_POSITION,gs.GPD_GUTSCHEINNR, gs.GPD_BUCHART, gs.GPD_STATUS';
                $SQLKopfUpdate .= ' FROM GUTSCHEINE gs';
                $SQLKopfUpdate .= ' WHERE gs.GPD_AUFTRAGNR = ' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '';
                $SQLKopfUpdate .= ' AND gs.GPD_BUCHART not in (\'A\',\'S\')';
                $SQLKopfUpdate .= ' ';
                $rsKopfUpdate = $this->_DB->RecordSetOeffnen($SQLKopfUpdate);

                if ($rsKopfUpdate->AnzahlDatensaetze() != 0) {
                    //Update in Kopfdaten Status auf nicht endg�ltig
                    $SQL = 'UPDATE GUTSCHEINKOPFDATEN kopf SET';
                    $SQL .= ' kopf.GSK_JOBSTATUS= \'F\'';
                    $SQL .= ', kopf.GSK_STATUS= \'B\'';
                    $SQL .= ' WHERE kopf.GSK_AUFTRAGNR=' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '';
                    $this->_DB->Ausfuehren($SQL, '', true);
                } else {
                    //Update in Kopfdaten Status auf endg�ltig
                    $SQL = 'UPDATE GUTSCHEINKOPFDATEN kopf SET';
                    $SQL .= ' kopf.GSK_JOBSTATUS= \'F\'';
                    $SQL .= ', kopf.GSK_STATUS= \'E\'';
                    $SQL .= ' WHERE kopf.GSK_AUFTRAGNR=' . $this->_FORM->Format('T', $rsOffeneKopf->FeldInhalt('GSK_AUFTRAGNR')) . '';
                    $this->_DB->Ausfuehren($SQL, '', true);
                }
                $rsOffeneKopf->DSWeiter();
            }
        } catch (Exception $e) {
            $Text='';
            $Text= 'awisGiftCard: Fehler: ' . $e->getMessage() . PHP_EOL;
            $Text.=PHP_EOL.'FEHLER:' . $e->getMessage() . PHP_EOL;
            $Text.= 'CODE:  ' . $e->getCode() . PHP_EOL;
            $Text.= 'Zeile: ' . $e->getLine() . PHP_EOL;
            $Text.= 'Datei: ' . $e->getFile() . PHP_EOL;
            $Text.= $this->_DB->LetzterSQL().PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_JOBVERARBEITUNG', $Text, 2, '', 'shuttle@de.atu.eu');

            die();
        }
	}

	public function _PruefZifferGenerator($fd)
	{
		try
		{
			$fw=fopen('/daten/web/export/Gutschein-PZGenerator/GSPZ.txt','w');

			//Datei Zeile f�r Zeile durchgehen
			while (!feof($fd))
			{
				$ZeileLesen = fgets($fd);
				$ZeileLesen=trim($ZeileLesen);
				$PruefZ='';

				if($ZeileLesen!='')
				{
					$PruefZ=$this->PruefZiffer($ZeileLesen);
					fwrite($fw,$ZeileLesen.$PruefZ."\r\n");
				}
			}

			fclose($fw);
			fclose($fd);

			return true;
		}
		catch (Exception $e)
		{
			$Text='';
			$Text= 'awisGiftCard: Fehler: ' . $e->getMessage() . PHP_EOL;
			$Text.=PHP_EOL.'FEHLER:' . $e->getMessage() . PHP_EOL;
			$Text.= 'CODE:  ' . $e->getCode() . PHP_EOL;
			$Text.= 'Zeile: ' . $e->getLine() . PHP_EOL;
			$Text.= 'Datei: ' . $e->getFile() . PHP_EOL;
			$Text.= $this->_DB->LetzterSQL().PHP_EOL;

			$this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - PruefZifferGenerator', $Text, 2, '', 'shuttle@de.atu.eu');

			die();
		}
	}


	//Zieht Informationen des Gutscheines vom GiftCardServer
	//REQUEST: CardID
	//RESPONSE: Array mit allen Informationen des Gutscheins
	private function GetInfo($client, $CardID)
	{
		try
		{
			$Aufruf = array();
			$Aufruf = array('CARD_ID' => $CardID, 'TRANS_TYPE' => 20);
			$this->debugAusgabe('API-Call (GetInfo) >>>' . $Aufruf);
			$Erg = $client->doTrans(array('transRequest' => $Aufruf));

			return $Erg->transResponse;
		}
		catch (SoapFault $ex)
		{
			$Text='';
			$Text= PHP_EOL . 'awisGiftCard: Fehler: ' . $ex->getMessage() . PHP_EOL;
			$Text.= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
			$Text.= 'CODE:  ' . $ex->getCode() . PHP_EOL;
			$Text.= 'Zeile: ' . $ex->getLine() . PHP_EOL;
			$Text.= 'Datei: ' . $ex->getFile() . PHP_EOL;
			$Text.= PHP_EOL.'CARD_ID: ' . $CardID. PHP_EOL;
			$Text.= 'TRANS_TYPE: 20' . PHP_EOL;

			$this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_GETINFO', $Text, 2, '', 'shuttle@de.atu.eu');
			die();
		}
	}
	
	//Karten- (Gutschein-)nummer ziehen
	// REQUEST: PROFILE_ID = 2, TRANS_TYPE = 21
	//			STORE_ID = 'Zentrale', OPERATOR_NAME = AWIS-User
	//			POS_COMMENT = Freitextfeld oder Vertragsnummer, POS_TIMESTAMP = Zeitstempel
	// RESPONSE: CARD_ID Gutscheinnummer, STATUS_MSG STatusmeldung vom GiftCardServer
	//				TRANS_ID Transaktionsnummer, CARD_PIN PIN f�r den Gutschein
	//				STATUS_CODE Statusr�ckmeldung vom GiftCardServer
	private function GetCardID($client, $ProfilNr,$STOREID,$OPERATOR,$POS_COMMENT,$POS_TIMESTAMP)
	{
		try
		{
			$Aufruf = array();
			$Aufruf = array('PROFILE_ID' => $ProfilNr, 'TRANS_TYPE' => 21, 'STORE_ID' => $STOREID, 'OPERATOR_NAME' => $OPERATOR, 'CLIENT_COMMENT' => $POS_COMMENT, 'POS_TIMESTAMP' => $POS_TIMESTAMP);
			$this->debugAusgabe('API-Call (GetCardId) >> ' . $Aufruf);
			$Erg = $client->doTrans(array('transRequest' => $Aufruf));

			return $Erg->transResponse;
		}
		catch (SoapFault $ex)
		{
			$Text='';
			$Text.= PHP_EOL . 'awisGiftCard: Fehler: ' . $ex->getMessage() . PHP_EOL;
			$Text.= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
			$Text.= 'CODE:  ' . $ex->getCode() . PHP_EOL;
			$Text.= 'Zeile: ' . $ex->getLine() . PHP_EOL;
			$Text.= 'Datei: ' . $ex->getFile() . PHP_EOL;
			$Text.= PHP_EOL.'PROFILNr: ' . $ProfilNr. PHP_EOL;
			$Text.= 'TRANS_TYPE: 21' . PHP_EOL;

			$this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_GETCARDID', $Text, 2, '', 'shuttle@de.atu.eu');
			die();
		}
	}

	//Liefert eine neue TransID zur�ck
	// REQUEST: TRANS_TYPE = 1, CARD_ID = 12345678
	// RESPONSE: TRANS_ID
	private function GET_TRANS_ID($client, $CardID)
	{
		try
		{
			$Aufruf = array();
			$Aufruf = array('CARD_ID' => $CardID, 'TRANS_TYPE' => 1);
			$this->debugAusgabe('API-Call (GET_TRANS_ID) >>>' . $Aufruf);
			$Erg = $client->doTrans(array('transRequest' => $Aufruf));

			return $Erg->transResponse;
		}
		catch (SoapFault $ex)
		{
			$Text='';
			$Text.= PHP_EOL . 'awisGiftCard: Fehler: ' . $ex->getMessage() . PHP_EOL;
			$Text.= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
			$Text.= 'CODE:  ' . $ex->getCode() . PHP_EOL;
			$Text.= 'Zeile: ' . $ex->getLine() . PHP_EOL;
			$Text.= 'Datei: ' . $ex->getFile() . PHP_EOL;
			$Text.= PHP_EOL.'CARD_ID: ' . $CardID. PHP_EOL;
			$Text.= 'TRANS_TYPE: 1' . PHP_EOL;

			$this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_TRANS_ID', $Text, 2, '', 'shuttle@de.atu.eu');
			die();
		}
	}

	//Activation aktiviert den Gutschein auf dem GiftCard
	// REQUEST: TRANS_TYPE = 4, CARD_ID = 12345678, TRANS_ID = GET_TRANS_ID, AMOUNT = Betrag, UNIT = Waehrung
	// RESPONSE: AMOUNT = 550, STATUS_MSG = Approved, UNIT_TYPE = 2,
	//           TRANS_ID = 223456, STATUS_CODE = 0, UNIT = EUR
	// !!!!!!!!!!!!!!!!BETRAG immer 2 Nachkommastellen angeben, getrennt mit Punkt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	private function Activation($client, $CardID, $TransID, $Betrag, $Waehrung)
	{
		try {
			$AufrufParam = array();
			$AufrufParam = array('CARD_ID' => $CardID, 'TRANS_TYPE' => 4, 'TRANS_ID' => $TransID, 'AMOUNT' => $Betrag, 'UNIT' => $Waehrung, 'UNIT_TYPE' => 1);
			$this->debugAusgabe('API-Call (Activation) >>> ' . $AufrufParam);
			$Erg = $client->doTrans(array('transRequest' => $AufrufParam));

			return $Erg->transResponse;
		}
		catch (SoapFault $ex)
		{
			$Text='';
			$Text.= PHP_EOL . 'awisGiftCard: Fehler: ' . $ex->getMessage() . PHP_EOL;
			$Text.= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
			$Text.= 'CODE:  ' . $ex->getCode() . PHP_EOL;
			$Text.= 'Zeile: ' . $ex->getLine() . PHP_EOL;
			$Text.= 'Datei: ' . $ex->getFile() . PHP_EOL;
			$Text.= PHP_EOL.'CLIENT: ' . $client. PHP_EOL;
			$Text.='CARD_ID' . $CardID . PHP_EOL;
			$Text.='TRANS_ID' . $TransID . PHP_EOL;
			$Text.='AMOUNT' . $Betrag . PHP_EOL;
			$Text.='UNIT_TYPE' . $Waehrung . PHP_EOL;
			$Text.= 'TRANS_TYPE: 4' . PHP_EOL;

			$this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_ACTIVATION', $Text, 2, '', 'shuttle@de.atu.eu');
			die();
		}
	}

	//Karten- (Gutschein-)nummer Deaktivieren
	// REQUEST: CARD_ID = 2, TRANS_TYPE = 18
	// RESPONSE: ??
	private function Deactivate($client, $CardID)
	{
		try
		{
			$Aufruf = array();
			$Aufruf = array('CARD_ID' => $CardID, 'TRANS_TYPE' => 18);
			$this->debugAusgabe('API-Call (Deactivate) >>>' . $Aufruf);
			$Erg = $client->doTrans(array('transRequest' => $Aufruf));

			return $Erg->transResponse;
		}
		catch (SoapFault $ex)
		{
			$Text='';
			$Text.= PHP_EOL . 'awisGiftCard: Fehler: ' . $ex->getMessage() . PHP_EOL;
			$Text.= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
			$Text.= 'CODE:  ' . $ex->getCode() . PHP_EOL;
			$Text.= 'Zeile: ' . $ex->getLine() . PHP_EOL;
			$Text.= 'Datei: ' . $ex->getFile() . PHP_EOL;
			$Text.= PHP_EOL.'CLIENT: ' . $client. PHP_EOL;
			$Text.='CARD_ID' . $CardID . PHP_EOL;

			$this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_DEACTIVATE', $Text, 2, '', 'shuttle@de.atu.eu');
			die();
		}
	}


	private function PruefZiffer($GSNR)
	{
		try
		{
			$i=0;
			$spalte = 0;
			$summe = 0;
			$PruefZiffer = 0;

			$arrayPruefNr = array();

			for($i=1;$i<=30;$i++)
			{
				$spalte=$i;
				$spalte=$spalte-1;
				$arrayPruefNr[$i]=($GSNR==''?'':(substr($GSNR,$spalte,1)==''?0:((substr($GSNR,$spalte,1))*(($i % 2)==0?1:3))));
				$summe=$summe+$arrayPruefNr[$i];
			}

			$PruefZiffer=$GSNR==''?'':(($summe %10)==0?0:(10-($summe % 10)));
			return $PruefZiffer;
		}
		catch (SoapFault $ex)
		{
			$Text='';
			$Text.= PHP_EOL . 'awisGiftCard: Fehler: ' . $ex->getMessage() . PHP_EOL;
			$Text.= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
			$Text.= 'CODE:  ' . $ex->getCode() . PHP_EOL;
			$Text.= 'Zeile: ' . $ex->getLine() . PHP_EOL;
			$Text.= 'Datei: ' . $ex->getFile() . PHP_EOL;
			$Text.= PHP_EOL.'CARD_ID: ' . $GSNR. PHP_EOL;

			$this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - GIFTCARD_PRUEFZIFFER', $Text, 2, '', 'shuttle@de.atu.eu');
			die();
		}
	}
}
?>