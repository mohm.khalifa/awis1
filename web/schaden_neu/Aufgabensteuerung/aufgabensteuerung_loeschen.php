<?php
global $AWIS_KEY1;

$TextKonserven=array();

$TextKonserven[]=array('SBV','%');
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
$DBSCHAD->Oeffnen();

$Tabelle = '';
if(isset($_GET['DelTyp'])and $_GET['DelTyp'] = 'SBV')
{
    $Tabelle = 'SBV';

    $SQL  ='select sbv_key, ver.SBNAME as ver_nachname, zuv.SBNAME as zuv_nachname  from SACHBEARBEITERVERTRETER sach';
    $SQL .=' inner join SACHBEARBEITER ver';
    $SQL .=' on sach.SBV_VERTRETER_SB_ID = ver.ID';
    $SQL .=' inner join SACHBEARBEITER zuv';
    $SQL .=' on sach.SBV_ZUVERTRETEN_SB_ID = zuv.ID';
    $SQL .=' WHERE sach.sbv_key'.$DBSCHAD->LikeOderIst($_GET['Del']);
    $rsDaten = $DBSCHAD->RecordSetOeffnen($SQL);
    	
    /**
     * Werte f�r Frage 'Wirklich l�schen?' holen
    **/
    $Felder=array();
    $Felder[]=array($Form->LadeTextBaustein('SBV','SBV_VERTRETER'),$Form->Format('T',$rsDaten->FeldInhalt('VER_NACHNAME')));
    $Felder[]=array($Form->LadeTextBaustein('SBV','SBV_ZUVERTRETEN'),$Form->Format('T',$rsDaten->FeldInhalt('ZUV_NACHNAME')));
    
    
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren wenn auf Frage 'Wirklich l�schen?' ok
{
	switch ($_POST['txtTabelle'])
	{
		case 'SBV':
    		$SQL ='update SACHBEARBEITERVERTRETER ';
    		$SQL .= ' set SBV_USER=\''.$AWISBenutzer->BenutzerName().'\'';
    		$SQL .= ', sbv_USERDAT=SYSDATE';
    		$SQL .= ', SBV_GUELTIG_BIS=trunc(SYSDATE) -1';
    		
    		$SQL .=' where SBV_KEY='.$_POST['txtKEY'];
    		$Form->DebugAusgabe(1,$SQL);
    		$DBSCHAD->Ausfuehren($SQL,'',true);
            break;
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	
	$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./aufgabensteuerung_Main.php method=post>');
	$Form->Formular_Start();
	$Form->ZeileStart();
	$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
	$Form->ZeileEnde();
		
	$Form->Trennzeile('O');
	
	foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Feld[0].':',200);
		$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		$Form->ZeileEnde();
	}
	
	/**
	 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
	 **/
	$Form->Erstelle_HiddenFeld('KEY',$rsDaten->FeldInhalt('SBV_KEY'));

	$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		
	$Form->Trennzeile();
		
	/**
	 * L�schen ja/nein
	**/
	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
	$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
	$Form->ZeileEnde();
		
	$Form->SchreibeHTMLCode('</form>');
		
	$Form->Formular_Ende();
		
	die();
		
	}


?>