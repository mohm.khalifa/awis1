<?php

global $Funktionen;

if(isset($_POST['cmdLoeschen_x'])) { //L�schung der Kompetenz komplett

    //L�schen der Filialen bei einer Kompetenz

    $SQL = "DELETE FROM FILIALINFOS ";
    $SQL .= " where fif_fit_id = 920";
    $SQL .= " and fif_wert =" . $Funktionen->DB->WertSetzen("FIK", "Z", $_POST["txtAWIS_KEY1"]);
    $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("FIK"));

    //L�schen der Kompetenz selbst

    $SQL = "DELETE FROM filialkompetenzentypen ";
    $SQL .= " where FIK_KEY =" . $Funktionen->DB->WertSetzen("FIK", "Z", $_POST["txtAWIS_KEY1"]);
    $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("FIK"));

} elseif (isset($_GET['Bearb'])){ //Alle Sachen, die per GET kommen

    $SQL = "DELETE FROM ";
    $SQL .= " Filialinfos ";
    $SQL .= " where fif_fit_id = 920";
    $SQL .= " and fif_wert =" . $Funktionen->DB->WertSetzen("FIK", "T", $Funktionen->AWIS_KEY1);
    if($_GET['Bearb'] <> 0){
        $SQL .= " and fif_fil_id =" . $Funktionen->DB->WertSetzen("FIK", "Z", $_GET["Bearb"]);
    }

    $Funktionen->DB->Ausfuehren($SQL,'',false,$Funktionen->DB->Bindevariablen('FIK'));
}