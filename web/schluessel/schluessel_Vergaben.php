<html>
<head>
	<title>Awis - ATU webbasierendes Informationssystem</title>
</head>
<body>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$Rechtestufe = awisBenutzerRecht($con, 1503);
		//  1=Einsehen
		//	2=Hinzuf�gen
		//	4=Bearbeiten
		//  8=L�schen

if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schluessel_Vergaben',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$MinAnzahl=2;

// Start : Funktion f�r Datumspr�fung

function check_date($date)
{
	$matches='';
    $format = '/^(\d{1,2})\.(\d{1,2})\.(\d{4})$/';
	if(preg_match($format, $date, $matches)) {
        return checkdate($matches[2], $matches[1], $matches[3]);
    }
    else return false;
}

// Ende : Funktion f�r Datumspr�fung

$txtSLVKEY=(isset($_REQUEST['txtSLV_KEY'])?$_REQUEST['txtSLV_KEY']:''); //Kein Unterschied ob POST oder GET

if(isset($_POST['cmdTrefferliste_x'])) // Anzeigen der Trefferliste wenn DS hinzugef�gt wurde
{
$txtSLVKEY=0;
}

if(isset($_REQUEST['txtFIL_ID']) && $_REQUEST['txtFIL_ID']!='')			
{
	$Params[0] = $_REQUEST['txtFIL_ID'];
	$Params[1] = (isset($_REQUEST['txtSLV_PER_NR'])?$_REQUEST['txtSLV_PER_NR']:'');	
	$Params[1] = (isset($_REQUEST['txtSLV_PER_NR_VERTRETER'])?$_REQUEST['txtSLV_PER_NR_VERTRETER']:'');	
	awis_BenutzerParameterSpeichern($con, "SchluesselVergaben", $AWISBenutzer->BenutzerName(), implode(';',$Params));
}

$anzeigen=false;

if(isset($_REQUEST['txtSLV_KEY_loeschen']) && $_REQUEST['txtSLV_KEY_loeschen']!='')
{
		$SQL='SELECT * FROM SCHLUESSELVERGABEN WHERE SLV_KEY=0'.$_REQUEST['txtSLV_KEY_loeschen'];
		$rsSLV_KEY_LOESCHEN = awisOpenRecordset($con,$SQL);
		$rsSLV_KEY_LOESCHENZeilen = $awisRSZeilen;
		
		if($rsSLV_KEY_LOESCHENZeilen==1)
		{
				$SQL="UPDATE SCHLUESSELNUMMERN SET SLN_STATUS=0 , SLN_USER='".$AWISBenutzer->BenutzerName()."', SLN_USERDAT=SYSDATE WHERE SLN_KEY=".$rsSLV_KEY_LOESCHEN['SLV_SLN_KEY'][0];
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schluessel_Vergaben.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
						
				$SQL='DELETE FROM SCHLUESSELVERGABEN WHERE SLV_KEY=0'.$_REQUEST['txtSLV_KEY_loeschen'];
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schluessel_Vergaben.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
				
				$anzeigen=true;
		}
		else
		{
				echo "<span class=HinweisText>Datensatz kann nicht gel&ouml;scht werden.</span>";
				$anzeigen=true;
		}
}

// Start : Parameter f�r Mitarbeiter-Suche
if(isset($_POST['cmdSuchen_x']) || isset($_POST['cmdSuchenVertreter_x']))		
{
		if($_POST['txtFIL_ID']!=''){
				$Params[0] = $_POST['txtFIL_ID'];
		}
		else{
				$Params[0] = $Params[0];
		}
		
		if($_POST['txtSLV_PER_NR']!='')
		{
				$Params[1] = $_POST['txtSLV_PER_NR'];	
		}
		else
		{
				$Params[1] = $Params[1];	
		}
		
		if($_POST['txtSLV_PER_NR_VERTRETER']!='')
		{
				$Params[2] = $_POST['txtSLV_PER_NR_VERTRETER'];	
		}
		else
		{
				$Params[2] = $Params[2];	
		}

		awis_BenutzerParameterSpeichern($con, "SchluesselVergaben", $AWISBenutzer->BenutzerName(), implode(';',$Params));
}
else
{
		$Params=explode(';',awis_BenutzerParameter($con, "SchluesselVergaben", $AWISBenutzer->BenutzerName()));
}
// Ende : Parameter f�r Mitarbeiter-Suche

// Start : Eingaben speichern

if(isset($_POST['cmdSpeichern_x'])) //Speichern mit Fallunterschied ob INSERT oder UPDATE
{
		if($txtSLVKEY>0) // UPDATE-Anweisung
		{		
				//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
				//--->
		
				$SQL='SELECT * FROM SCHLUESSELVERGABEN WHERE SLV_KEY='.$txtSLVKEY;
				$rsSchluesselvergaben = awisOpenRecordset($con,$SQL);
				$rsSchluesselvergabenZeilen = $awisRSZeilen;
				
				$SQL='';
				$txtHinweis='';
				
				if($rsSchluesselvergabenZeilen==0)		// Keine Daten
				{
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde gel�scht!</span>");
				}
				
				if(is_numeric(str_replace(" ","",$_POST['txtSLV_PER_NR'])))
				{				
						if($_POST['txtSLV_PER_NR'] != $_POST['txtSLV_PER_NR_old'])
						{
								if($rsSchluesselvergaben['SLV_PER_NR'][0] != $_POST['txtSLV_PER_NR_old'])
								{
										$txtHinweis.=',Mitarbeiter von '.$_POST['txtSLV_PER_NR_old']. ' auf '.$rsSchluesselvergaben['SLV_PER_NR'][0];
								}
								else
								{
										$SQL.=',SLV_PER_NR=\''.$_POST['txtSLV_PER_NR'].'\'';
								}
						}
				}
				else
				{
						$txtHinweis.=',Eingabe SLV_PER_NR : '.$_POST['txtSLV_PER_NR']. ' ist keine Personal-Nummer';
				}
				
				if($_POST['txtSLV_PER_NR_VERTRETER']!='')
				{
						if(is_numeric(str_replace(" ","",$_POST['txtSLV_PER_NR_VERTRETER'])))
						{				
								if($_POST['txtSLV_PER_NR_VERTRETER'] != $_POST['txtSLV_PER_NR_VERTRETER_old'])
								{
										if($rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0] != $_POST['txtSLV_PER_NR_VERTRETER_old'])
										{
												$txtHinweis.=',Mitarbeiter-Vertreter von '.$_POST['txtSLV_PER_NR_VERTRETER_old']. ' auf '.$rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0];
										}
										else
										{
												$SQL.=',SLV_PER_NR_VERTRETER=\''.$_POST['txtSLV_PER_NR_VERTRETER'].'\'';
										}
								}
						}
						else
						{
								$txtHinweis.=',Eingabe SLV_PER_NR_VERTRETER : '.$_POST['txtSLV_PER_NR_VERTRETER']. ' ist keine Personal-Nummer';
						}
				}
				elseif ($_POST['txtSLV_PER_NR_VERTRETER']=='' && $_POST['txtSLV_PER_NR_VERTRETER_old']!='' && $rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0]!='')
				{
					$SQL.=',SLV_PER_NR_VERTRETER=NULL';
				}
				
				if(is_numeric($_POST['txtSLV_SLN_KEY']) && $_POST['txtSLV_SLN_KEY']>0)
				{
						if($_POST['txtSLV_SLN_KEY'] != $_POST['txtSLV_SLN_KEY_old'])
						{
								if($rsSchluesselvergaben['SLV_SLN_KEY'][0] != $_POST['txtSLV_SLN_KEY_old'])
								{
										$txtHinweis.=',Schl&uuml;ssel-Nummer von '.$_POST['txtSLV_SLN_KEY_old']. ' auf '.$rsSchluesselvergaben['SLV_SLN_KEY'][0];
								}
								else
								{
										$SQL.=',SLV_SLN_KEY=\''.$_POST['txtSLV_SLN_KEY'].'\'';
								}
						}
				}
				else
				{
						$txtHinweis.=',Eingabe SLV_SLN_KEY : '.$_POST['txtSLV_PER_NR']. ' ist keine Schl&uuml;ssel-Nummer';
				}
				
				if($_POST['txtSLV_SLS_KEY'] != $_POST['txtSLV_SLS_KEY_old'])
				{
						if($rsSchluesselvergaben['SLV_SLS_KEY'][0] != $_POST['txtSLV_SLS_KEY_old'])
						{
								$txtHinweis.=',Schl&uuml;ssel-Status von '.$_POST['txtSLV_SLS_KEY_old']. ' auf '.$rsSchluesselvergaben['SLV_SLS_KEY'][0];
						}
						else
						{
								$SQL.=',SLV_SLS_KEY=\''.$_POST['txtSLV_SLS_KEY'].'\'';
						}
				}
				
				if(check_date($_POST['txtSLV_UEBERGABE']))
				{
						if($_POST['txtSLV_UEBERGABE'] != $_POST['txtSLV_UEBERGABE_old'])
						{
								if($rsSchluesselvergaben['SLV_UEBERGABE'][0] != $_POST['txtSLV_UEBERGABE_old'])
								{
										$txtHinweis.=',&Uuml;bergabe von '.$_POST['txtSLV_UEBERGABE_old']. ' auf '.$rsSchluesselvergaben['SLV_UEBERGABE'][0];
								}
								else
								{
										$SQL.=',SLV_UEBERGABE=\''.$_POST['txtSLV_UEBERGABE'].'\'';
								}				
						}
				}
				else
				{
						$txtHinweis.=',Eingabe SLV_UEBERGABE : '.$_POST['txtSLV_UEBERGABE']. ' ist keine Datum';
				}
				
				if($_POST['txtSLV_BEMERKUNG'] != urldecode($_POST['txtSLV_BEMERKUNG_old']))
				{
						if($rsSchluesselvergaben['SLV_BEMERKUNG'][0] != urldecode($_POST['txtSLV_BEMERKUNG_old']))
						{
								$txtHinweis.=',BEMERKUNG von '.$_POST['txtSLV_BEMERKUNG_old']. ' auf '.$rsSchluesselvergaben['SLV_BEMERKUNG'][0];
						}
						else
						{
								$SQL.=',SLV_BEMERKUNG=\''.substr($_POST['txtSLV_BEMERKUNG'],0,255).'\'';
						}
				}
				
				//<---
				
				// Update- Befehl
				
				if($txtHinweis=='' && $SQL!='')
				{
						
						$SQL.=',SLV_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						
						$SQL.=',SLV_USERDAT=SYSDATE';
				
						$SQL='UPDATE SCHLUESSELVERGABEN SET ' .substr($SQL,1).' WHERE SLV_KEY=0'.$txtSLVKEY;
						
						if(awisExecute($con, $SQL)===FALSE)
						{
							awisErrorMailLink("schluessel_Vergaben.php", 2, $awisDBFehler);
							awisLogoff($con);
							die();
						}
				}
				elseif($txtHinweis!='')
				{
						echo $txtHinweis;
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsSchluesselvergaben['SLV_USER'][0] ." ge�ndert !</span>");
				}
		}
		else // INSERT-Anweisung
		{
				if(is_numeric(str_replace(" ","",$_POST['txtSLV_PER_NR']))==FALSE)
				{
						awislogoff($con);
						die("<span class=HinweisText>Sie m�ssen einen Mitarbeiter angeben!</span>");
				}
				
				if((is_numeric($_POST['txtSLV_SLN_KEY']) && $_POST['txtSLV_SLN_KEY']>0)==FALSE)
				{
						awislogoff($con);
						die("<span class=HinweisText>Sie m�ssen eine Schl&uuml;ssel-Nummer angeben!</span>");
				}
				
				if(check_date($_POST['txtSLV_UEBERGABE'])==FALSE)
				{
						awislogoff($con);
						die("<span class=HinweisText>Sie m�ssen ein &Uuml;bergabe-Datum angeben!</span>");
				}
				
				$SQL="INSERT INTO SCHLUESSELVERGABEN (SLV_PER_NR,SLV_PER_NR_VERTRETER,SLV_SLN_KEY,SLV_SLS_KEY,SLV_UEBERGABE,SLV_BEMERKUNG,SLV_CREAUSER,SLV_CREAUSERDAT,SLV_USER,SLV_USERDAT) ";
				$SQL.="VALUES('".$_POST['txtSLV_PER_NR']."','".$_POST['txtSLV_PER_NR_VERTRETER']."',".$_POST['txtSLV_SLN_KEY'].",".$_POST['txtSLV_SLS_KEY'].",to_date('".$_POST['txtSLV_UEBERGABE']."','DD.MM.YYYY'),'".substr($_POST['txtSLV_BEMERKUNG'],0,255)."','".$AWISBenutzer->BenutzerName()."',SYSDATE,'".$AWISBenutzer->BenutzerName()."',SYSDATE)";
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schluessel_Vergaben.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
				
				$SQL="UPDATE SCHLUESSELNUMMERN SET SLN_STATUS=1 WHERE SLN_KEY=".$_POST['txtSLV_SLN_KEY'];
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schluessel_Vergaben.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
				
				$rsSLV_KEY = awisOpenRecordset($con,'SELECT SEQ_SLV_KEY.CURRVAL AS KEY FROM DUAL');
				
				$txtSLVKEY=$rsSLV_KEY['KEY'][0];
				$anzeigen=true;
		}
}

// Ende :Eingaben speichern



if((isset($_POST['cmdHinzufuegen_x']) || isset($_POST['cmdSuchen_x']) || isset($_POST['cmdSuchenVertreter_x'])) && ($Rechtestufe&4)==4) // Formular f�r das Hinzuf�gen von Datens�tzen
{
		echo '<form name=frmVergaben method=post action=./schluessel_Main.php?cmdAktion=Vergaben>';
				
		echo "<table  width=100% id=DatenTabelle border=1><tr>";
				echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
		echo "</tr>";
		
		if ((isset($_POST['cmdSuchen_x']) && $_POST['txtSLV_PER_NR']!='') || $Params[1]!='')
		{
				
				$zahl=str_replace(" ","",$_POST['txtSLV_PER_NR']);
				$zahl=str_replace("*","",$zahl);
				
				if(is_numeric($zahl))
				{
						$SQL='SELECT * FROM v_personal_komplett_map WHERE PER_NR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
						$SQL.=' ORDER BY PER_NACHNAME ASC';
						$rsSLV_PER_NR = awisOpenRecordset($con,$SQL);
						$rsSLV_PER_NRZeilen = $awisRSZeilen;
						awis_Debug(1,$SQL);
						
						if($rsSLV_PER_NRZeilen>1)
						{
								echo "<tr>";
										echo "<td id=FeldBez width=150>Mitarbeiter</td>";
										echo "<td><select name=txtSLV_PER_NR>";
										echo '<option value=-1>Bitte w�hlen...</option>';
										for($SLV_PER_NRZeile=0;$SLV_PER_NRZeile<$rsSLV_PER_NRZeilen;$SLV_PER_NRZeile++)
										{
												echo "<option value='" . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "'>" . $rsSLV_PER_NR['PER_NACHNAME'][$SLV_PER_NRZeile] . " " . $rsSLV_PER_NR['PER_VORNAME'][$SLV_PER_NRZeile] . " - "  . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "</option>";
										}
										echo '</select>';
										unset($rsSLV_PER_NR);
								echo "</tr>";
						}
						else
						{
								echo "<tr>";
										echo "<td id=FeldBez width=150>Mitarbeiter</td>";
										echo "<td id=TabellenZeileGrau><input name=txtSLV_PER_NR size=68 value='" . $rsSLV_PER_NR['PER_NACHNAME'][0] . " " . $rsSLV_PER_NR['PER_VORNAME'][0] . " - " . $rsSLV_PER_NR['PER_NR'][0] . "'></td><input type=hidden name=txtSLV_PER_NR value='" . $rsSLV_PER_NR['PER_NR'][0] . "'>";
								echo "</tr>";
						}
				}
				else
				{
						$SQL='SELECT * FROM v_personal_komplett_map WHERE UPPER(PER_NACHNAME) '. awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0) . ' OR UPPER(PER_VORNAME) ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
						$SQL.=' ORDER BY PER_NACHNAME ASC';
						$rsSLV_PER_NR = awisOpenRecordset($con,$SQL);
						$rsSLV_PER_NRZeilen = $awisRSZeilen;
						
						echo "<tr>";
								echo "<td id=FeldBez width=150>Mitarbeiter</td>";
								echo "<td><select name=txtSLV_PER_NR>";
								echo '<option value=-1>Bitte w�hlen...</option>';
								for($SLV_PER_NRZeile=0;$SLV_PER_NRZeile<$rsSLV_PER_NRZeilen;$SLV_PER_NRZeile++)
								{
										echo "<option value='" . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "'>" . $rsSLV_PER_NR['PER_NACHNAME'][$SLV_PER_NRZeile] . " " . $rsSLV_PER_NR['PER_VORNAME'][$SLV_PER_NRZeile] . " - "  . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "</option>";
								}
								echo '</select>';
								unset($rsSLV_PER_NR);
						echo "</tr>";
				}
		}
		else
		{	
				echo "<tr>";
						echo "<td id=FeldBez width=150>Mitarbeiter</td>";
						echo "<td id=TabellenZeileGrau><table><tr><td><input name=txtSLV_PER_NR size=68></td><td align=left><input type=image title='Suchen' src=/bilder/filter.png name=cmdSuchen></td></tr></table></td>";
				echo "</tr>";
		}
		
		// START Personal - Vertreter
		if (isset($_POST['cmdSuchenVertreter_x']) && $_POST['txtSLV_PER_NR_VERTRETER']!='')
		{
				
				$zahl=str_replace(" ","",$_POST['txtSLV_PER_NR_VERTRETER']);
				$zahl=str_replace("*","",$zahl);
				
				if(is_numeric($zahl))
				{
						$SQL='SELECT * FROM v_personal_komplett_map WHERE PER_NR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
						$SQL.=' ORDER BY PER_NACHNAME ASC';
						$rsSLV_PER_NR_VERTRETER = awisOpenRecordset($con,$SQL);
						$rsSLV_PER_NR_VERTRETERZeilen = $awisRSZeilen;
						
						if($rsSLV_PER_NR_VERTRETERZeilen>1)
						{
								echo "<tr>";
										echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
										echo "<td><select name=txtSLV_PER_NR_VERTRETER>";
										echo '<option value=-1>Bitte w�hlen...</option>';
										for($SLV_PER_NR_VERTRETERZeile=0;$SLV_PER_NR_VERTRETERZeile<$rsSLV_PER_NR_VERTRETERZeilen;$SLV_PER_NR_VERTRETERZeile++)
										{
												echo "<option value='" . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "'>" . $rsSLV_PER_NR_VERTRETER['PER_NACHNAME'][$SLV_PER_NR_VERTRETERZeile] . " " . $rsSLV_PER_NR_VERTRETER['PER_VORNAME'][$SLV_PER_NR_VERTRETERZeile] . " - "  . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "</option>";
										}
										echo '</select>';
										unset($rsSLV_PER_NR_VERTRETER);
								echo "</tr>";
						}
						else
						{
								echo "<tr>";
										echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
										echo "<td id=TabellenZeileGrau><input name=txtSLV_PER_NR_VERTRETER size=68 value='" . $rsSLV_PER_NR_VERTRETER['PER_NACHNAME'][0] . " " . $rsSLV_PER_NR_VERTRETER['PER_VORNAME'][0] . " - " . $rsSLV_PER_NR_VERTRETER['PER_NR'][0] . "'></td><input type=hidden name=txtSLV_PER_NR_VERTRETER value='" . $rsSLV_PER_NR_VERTRETER['PER_NR'][0] . "'>";
								echo "</tr>";
						}
				}
				else
				{
						$SQL='SELECT * FROM v_personal_komplett_map WHERE UPPER(PER_NACHNAME) '. awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0) . ' OR UPPER(PER_VORNAME) ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
						$SQL.=' ORDER BY PER_NACHNAME';
						$rsSLV_PER_NR_VERTRETER = awisOpenRecordset($con,$SQL);
						$rsSLV_PER_NR_VERTRETERZeilen = $awisRSZeilen;
						
						echo "<tr>";
								echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
								echo "<td><select name=txtSLV_PER_NR_VERTRETER>";
								echo '<option value=-1>Bitte w�hlen...</option>';
								for($SLV_PER_NR_VERTRETERZeile=0;$SLV_PER_NR_VERTRETERZeile<$rsSLV_PER_NR_VERTRETERZeilen;$SLV_PER_NR_VERTRETERZeile++)
								{
										echo "<option value='" . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "'>" . $rsSLV_PER_NR_VERTRETER['PER_NACHNAME'][$SLV_PER_NR_VERTRETERZeile] . " " . $rsSLV_PER_NR_VERTRETER['PER_VORNAME'][$SLV_PER_NR_VERTRETERZeile] . " - "  . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "</option>";
								}
								echo '</select>';
								unset($rsSLV_PER_NR_VERTRETER);
						echo "</tr>";
				}
		}
		else
		{	
				echo "<tr>";
						echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
						echo "<td id=TabellenZeileGrau><table><tr><td><input name=txtSLV_PER_NR_VERTRETER size=68></td><td align=left><input type=image title='Suchen Vertreter' src=/bilder/filter.png name=cmdSuchenVertreter></td></tr></table></td>";
				echo "</tr>";
		}
		// ENDE Personal - Vertreter
		
		
		$SQL='';
		$SQL='SELECT SLN_KEY,SLN_SCHLUESSELNUMMER, SLN_SCHLUESSELLFDNR, SLG_BEZEICHNUNG, SLU_BEZEICHNUNG FROM SCHLUESSELNUMMERN, ';
		$SQL.='SCHLUESSELGRUPPEN, SCHLUESSELUNTERGRUPPEN WHERE SLN_FIL_ID='.$Params[0].' AND ';
		$SQL.='SLN_SLG_KEY=SLG_KEY(+) AND SLN_SLU_KEY=SLU_KEY(+) AND SLN_STATUS=0 ';
		$SQL.='ORDER BY SLG_BEZEICHNUNG, SLN_SCHLUESSELLFDNR';
		$rsSLV_SLN_KEY = awisOpenRecordset($con,$SQL);
		$rsSLV_SLN_KEYZeilen = $awisRSZeilen;
		
		echo "<tr>";
				echo "<td id=FeldBez width=150>Schl&uuml;ssel-Nummer</td>";
				echo "<td><select name=txtSLV_SLN_KEY>";
				echo '<option value=-1>Bitte w�hlen...</option>';
				for($SLV_SLN_KEYZeile=0;$SLV_SLN_KEYZeile<$rsSLV_SLN_KEYZeilen;$SLV_SLN_KEYZeile++)
				{
						echo '<option value=' . $rsSLV_SLN_KEY['SLN_KEY'][$SLV_SLN_KEYZeile] . '>' . $rsSLV_SLN_KEY['SLN_SCHLUESSELNUMMER'][$SLV_SLN_KEYZeile] . '-' . $rsSLV_SLN_KEY['SLN_SCHLUESSELLFDNR'][$SLV_SLN_KEYZeile] . ' - '  . $rsSLV_SLN_KEY['SLG_BEZEICHNUNG'][$SLV_SLN_KEYZeile] . ' - ' . $rsSLV_SLN_KEY['SLU_BEZEICHNUNG'][$SLV_SLN_KEYZeile] .  '</option>';
				}
				echo '</select>';
				unset($rsSLV_SLN_KEY);
		echo "</tr>";
		
		$SQL='';
		$SQL='SELECT SLS_KEY, SLS_BEZEICHNUNG ';
		$SQL.='FROM SCHLUESSELSTATUS';
		$rsSLV_SLS_KEY = awisOpenRecordset($con,$SQL);
		$rsSLV_SLS_KEYZeilen = $awisRSZeilen;
		
		echo "<tr>";
				echo "<td id=FeldBez width=150>Schl&uuml;ssel-Status</td>";
				echo "<td><select name=txtSLV_SLS_KEY>";
				for($SLV_SLS_KEYZeile=0;$SLV_SLS_KEYZeile<$rsSLV_SLS_KEYZeilen;$SLV_SLS_KEYZeile++)
				{
						if($rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile]==1){
								echo '<option value=' . $rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile] . ' standard="standard">' . $rsSLV_SLS_KEY['SLS_BEZEICHNUNG'][$SLV_SLS_KEYZeile] . '</option>';
						}
						else{
								if($rsSLV_SLS_KEY['SLS_BEZEICHNUNG'][$SLV_SLS_KEYZeile]=='Filiale')
								{
									echo '<option value=' . $rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile] . ' selected="selected">' . $rsSLV_SLS_KEY['SLS_BEZEICHNUNG'][$SLV_SLS_KEYZeile] . '</option>';
								}
								else 
								{
									echo '<option value=' . $rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile] . '>' . $rsSLV_SLS_KEY['SLS_BEZEICHNUNG'][$SLV_SLS_KEYZeile] . '</option>';
								}
						}
				}
				echo '</select>';
				unset($rsSLV_SLS_KEY);
		echo "</tr>";
		
		echo "<tr>";
				echo "<td id=FeldBez width=150>&Uuml;bergabe-Datum TT.MM.JJJJ</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLV_UEBERGABE size=30></td>";
		echo "</tr>";
		
		echo "<tr>";
				echo "<td id=FeldBez valign=top>Bemerkung</td>";
				echo "<td id=TabellenZeileGrau><textarea name=txtSLV_BEMERKUNG cols=50 rows=5></textarea></td>";
		echo "</tr>";
		
		echo "</table>";
					
		echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
		echo "<input type=hidden name=txtSLV_KEY value=0>";
		if($Params[0]!=''){
				echo "<input type=hidden name=txtFIL_ID value=".$Params[0].">";
		}
		echo "</form>";
}
elseif((isset($_POST['cmdAnzeigen_x']) && $_POST['cmdAnzeigen_x']==TRUE) || (isset($_GET['cmdAnzeigen_x']) && $_GET['cmdAnzeigen_x']==TRUE) || $anzeigen || (isset($_REQUEST['Anzeigen']) && $_REQUEST['Anzeigen']==TRUE)) // Formular f�r das Hinzuf�gen von Datens�tzen
{
		$Params=explode(';',awis_BenutzerParameter($con, "SchluesselVergaben", $AWISBenutzer->BenutzerName()));
		
		echo '<form name=frmVergaben method=post action=./schluessel_Main.php?cmdAktion=Vergaben>';

				echo '<table width=100%>';
				echo '<tr><td width=50><a title=\'Filial-Details anzeigen\' href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$Params[0].'&Zurueck=../schluessel/schluessel_Main.php?cmdAktion=Vergaben~~1~~Anzeigen=TRUE>Filiale</a></td>';
				echo '<td width=250><select name=txtFIL_ID>';
				
				//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ORDER BY FIL_ID");
				//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8 ORDER BY FIL_ID");				
				//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
				//union
				//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32 ORDER BY FIL_ID");
				
				//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
				//union
				//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				//union
				//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('A','B','C') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				//union
				//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('D','E','F') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				//ORDER BY FIL_ID");
				
				$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='BRD' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				union
				SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='OES' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				union
				SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='CZE' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				union
				SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='NED' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				union
				SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='SUI' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				union
				SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='ITA' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
				union
				SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_LAN_WWSKENN='BRD' AND FIL_ID=597
				ORDER BY FIL_ID");
				
				$rsFilialenZeilen = $awisRSZeilen;
				
				echo "<option value=0>::Auswahl::</option>";
				for($FilialenZeile=0;$FilialenZeile<$rsFilialenZeilen;$FilialenZeile++)
				{
						if($Params[0]==$rsFilialen['FIL_ID'][$FilialenZeile])
						{
								$txtFiliale=$rsFilialen['FIL_BEZ'][$FilialenZeile];
								echo "<option value=" . $rsFilialen['FIL_ID'][$FilialenZeile] . " selected='selected'>" . $rsFilialen['FIL_ID'][$FilialenZeile] . " - " . $rsFilialen['FIL_BEZ'][$FilialenZeile] . "</option>";
						}
						else
						{
								echo "<option value=" . $rsFilialen['FIL_ID'][$FilialenZeile] . ">" . $rsFilialen['FIL_ID'][$FilialenZeile] . " - " . $rsFilialen['FIL_BEZ'][$FilialenZeile] . "</option>";
						}
				}
				echo "</select></td>";
				echo "<td width=50>&nbsp;<input type=image border=0 src=/bilder/eingabe_ok.png accesskey=a name=cmdAnzeigen title='Anzeigen (Alt+A)'></td>";
				$RechtPDF=awisBenutzerRecht($con, 1504);
				
				$Params_PDF[0]=$Params[0];
				$Params_PDF[1]=$txtFiliale;
				awis_BenutzerParameterSpeichern($con, "Schluessel_PDF", $AWISBenutzer->BenutzerName(), implode(';',$Params_PDF));
				
				echo "<td align=right>";

				if(($RechtPDF&8)==8)
				{
					$DateiName = realpath('../dokumente/schluessel/schliessplan') . '/' . $Params_PDF[0] . '.pdf';
					
					if(file_exists($DateiName))
					{
						echo "<a href=../dokumente/schluessel/schliessplan/" . $Params_PDF[0] . ".pdf target='_blank'><img border=0 src=/bilder/sc_splan.png title='Schlie�plan' onmouseover=\"window.status='Schlie�plan';return true;\" onmouseout=\"window.status='';return true;\" onFocus=\"window.status='Schlie�plan';return true;\"></a>&nbsp;";
					}
				}
					
				if(($RechtPDF&4)==4)
				{
					$DateiName = realpath('../dokumente/schluessel/protokoll') . '/' . $Params_PDF[0] . '.pdf';
					
					if(file_exists($DateiName))
					{
						echo "<a href=../dokumente/schluessel/protokoll/" . $Params_PDF[0] . ".pdf target='_blank'><img border=0 src=/bilder/sc_sprotokoll.png title='Schl&uuml;sselprotokoll anzeigen' onmouseover=\"window.status='Schl&uuml;sselprotokoll';return true;\" onmouseout=\"window.status='';return true;\" onFocus=\"window.status='Schl&uuml;sselprotokoll';return true;\"></a>&nbsp;";
					}
				}
				
				if(($RechtPDF&1)==1)
				{
					echo "<a href='./schluessel_pdf.php'><img border=0 src=/bilder/pdf_gross.png name=cmdPDF title='Schl&uuml;sselprotokol erstellen'></a>";
				}
				
				echo "</td>";
				
				echo "</tr>";
				echo '</table>';
				unset($rsFilialen);
				
				$rsFilialInfos = awisOpenRecordset($con, "SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIL_ID=".$Params[0]." AND FIF_FIT_ID=20 AND FIF_IMQ_ID=8");
				$rsFilialinfosZeilen = $awisRSZeilen;
				
				If($rsFilialinfosZeilen!=0)
				{
					echo '<table width=100%>';
					echo '<tr>';
					echo '<td>'.$rsFilialInfos['FIF_WERT'][0].' - Schicht Betrieb</td>';
					echo '</tr>';
					echo '</table>';
				}
				
				unset($rsFilialInfos);
				
				/*
				$SQL="SELECT SLG_BEZEICHNUNG, count(SLN_SLG_KEY) AS ANZ_GES, sum(SLN_STATUS) AS ANZ_AUS  FROM SCHLUESSELNUMMERN, SCHLUESSELGRUPPEN ";
				$SQL.="WHERE SLN_SLG_KEY=SLG_KEY(+) AND SLN_FIL_ID=".$Params[0]." GROUP BY SLG_BEZEICHNUNG ORDER BY SLG_BEZEICHNUNG ASC";
				*/
				
				$SQL="SELECT nvl2(schluesseluntergruppen.slu_bezeichnung,schluesselgruppen.slg_bezeichnung || ' => ' || schluesseluntergruppen.slu_bezeichnung, schluesselgruppen.slg_bezeichnung) AS Gruppe, ";
				$SQL.="count(nvl2(schluesseluntergruppen.slu_bezeichnung,schluesselgruppen.slg_bezeichnung || '=>' || schluesseluntergruppen.slu_bezeichnung, schluesselgruppen.slg_bezeichnung)) AS ANZ_GES, ";
				$SQL.="sum(schluesselnummern.sln_status) As ANZ_AUS ";
				$SQL.="FROM schluesseluntergruppen, schluesselnummern, schluesselgruppen ";
				$SQL.="WHERE ((schluesselgruppen.slg_key = schluesselnummern.sln_slg_key(+)) ";
				$SQL.="AND (schluesseluntergruppen.slu_key(+) = schluesselnummern.sln_slu_key) AND (schluesselnummern.sln_fil_id =0".$Params[0].")) ";
				$SQL.="GROUP BY nvl2(schluesseluntergruppen.slu_bezeichnung,schluesselgruppen.slg_bezeichnung || ' => ' || schluesseluntergruppen.slu_bezeichnung, schluesselgruppen.slg_bezeichnung)";
				
				$rsSLN_FIL = awisOpenRecordset($con, $SQL);
				$rsSLN_FILZeilen = $awisRSZeilen;
				
				if($rsSLN_FILZeilen>0)
				{
						echo "<br>";
						
						echo "<table  id=DatenTabelle border=1><tr>";
								echo "<td id=FeldBez>Schl&uuml;ssel-Gruppe</td>";						
								echo "<td id=FeldBez>Anzahl-Gesamt</td>";
								echo "<td id=FeldBez>Ausgegeben</td>";
								echo "<td id=FeldBez>Reserve</td>";
						echo "</tr>";
						
						for($i=0;$i<$rsSLN_FILZeilen;$i++)
						{
								echo "<tr>";
										echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSLN_FIL['GRUPPE'][$i] . '</td>';
										echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSLN_FIL['ANZ_GES'][$i] . '</td>';
										echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSLN_FIL['ANZ_AUS'][$i] . '</td>';
										echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . (($rsSLN_FIL['ANZ_GES'][$i]-$rsSLN_FIL['ANZ_AUS'][$i])<$MinAnzahl?'<b><span class=HinweisText>'.($rsSLN_FIL['ANZ_GES'][$i]-$rsSLN_FIL['ANZ_AUS'][$i]).'</span></b>':($rsSLN_FIL['ANZ_GES'][$i]-$rsSLN_FIL['ANZ_AUS'][$i])) . '</td>';
								echo "</tr>";
						}
						echo '</table>';		
				}
				
				
				echo '<hr>';
				
		echo '</form>';
		
		echo '<form name=frmVergaben method=post action=./schluessel_Main.php?cmdAktion=Vergaben>';
		
				if(($Rechtestufe&4)==4) // Falls Recht ADD-Button hinzuf�gen
				{
						echo "<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegen title='Hinzuf�gen (Alt+N)' onclick=location.href='./schluessel_Main.php?cmdAktion=Vergbaben'>";
						echo '<hr>';
				}
				
				$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

				$SQL='SELECT schluesselvergaben.slv_key, v_personal_komplett_map.per_fil_id, v_personal_komplett_map.per_nachname,v_personal_komplett_map.per_vorname,v_personal_komplett_map.per_taetigkeit, schluesselvergaben.slv_per_nr,schluesselstatus.sls_bezeichnung, schluesselvergaben.slv_uebergabe,  ';
				$SQL.='schluesselnummern.sln_schluesselnummer,schluesselnummern.sln_schluessellfdnr, schluesselnummern.sln_sicherkartennummer, schluesselgruppen.slg_bezeichnung, schluesseluntergruppen.slu_bezeichnung  ';
				$SQL.='FROM schluesselvergaben, v_personal_komplett_map, schluesselstatus, schluesselnummern, schluesselgruppen, schluesseluntergruppen  ';
				$SQL.='WHERE ( (schluesselvergaben.slv_per_nr = v_personal_komplett_map.per_nr(+)) AND (schluesselvergaben.slv_sls_key = schluesselstatus.sls_key(+)) AND (schluesselvergaben.slv_sln_key = schluesselnummern.sln_key(+))  ';
				$SQL.='AND (schluesselnummern.sln_slg_key=schluesselgruppen.slg_key(+)) ';
				$SQL.='AND (schluesselnummern.sln_slu_key=schluesseluntergruppen.slu_key(+)) ';
				$SQL.='AND (schluesselnummern.sln_fil_id='.$Params[0].')) ';
				$SQL.='ORDER BY SLG_BEZEICHNUNG, SLN_SCHLUESSELLFDNR';
				
				awis_Debug(1,$SQL);
				
				$rsSchluesselvergaben = awisOpenRecordset($con,$SQL);
				$rsSchluesselvergabenZeilen = $awisRSZeilen;
				
				if($rsSchluesselvergabenZeilen==0)		// Keine Daten
				{
						echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
						awisLogoff($con);
						die();
				}
				else
				{
						// �berschrift aufbauen
						echo "<table width=100% id=DatenTabelle border=1><tr>";
							if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
							{
								echo "<td id=FeldBez></td>";
							}
							echo "<td id=FeldBez>Schl&uuml;ssel-Gruppe</td>";						
							echo "<td id=FeldBez>Schl&uuml;ssel-Nummer</td>";
							echo "<td id=FeldBez>LfdNr</td>";
							echo "<td id=FeldBez>Sicherungskarten-Nr</td>";
							echo "<td id=FeldBez>Fil-Nr</td>";
							echo "<td id=FeldBez>Personal-Nr</td>";
							echo "<td id=FeldBez>Personal-T&auml;tigkeit</td>";
							echo "<td id=FeldBez>Mitarbeiter</td>";
							echo "<td id=FeldBez>&Uuml;bergabe</td>";
							echo "<td id=FeldBez>Status</td>";
							if(($Rechtestufe&8)==8){
									echo "<td id=FeldBez></td>";
							}
						echo "</tr>";
						
						for($i=0;$i<$rsSchluesselvergabenZeilen;$i++)
						{
							if($i<$AwisBenuPara)
							{
								echo '<tr>';
									if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
									{
											echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . "><a href=./schluessel_Main.php?cmdAktion=Vergaben&txtSLV_KEY=" . $rsSchluesselvergaben['SLV_KEY'][$i] . "><img border=0 src=/bilder/aendern.png name=cmdBearbeiten title='Bearbeiten'></a></td>";
									}
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['SLG_BEZEICHNUNG'][$i] .'' . (empty($rsSchluesselvergaben['SLU_BEZEICHNUNG'][$i])?'':' => '.$rsSchluesselvergaben['SLU_BEZEICHNUNG'][$i].'') .' </td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['SLN_SCHLUESSELNUMMER'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['SLN_SCHLUESSELLFDNR'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['SLN_SICHERKARTENNUMMER'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['PER_FIL_ID'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['SLV_PER_NR'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['PER_TAETIGKEIT'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['PER_NACHNAME'][$i] . ' ' . $rsSchluesselvergaben['PER_VORNAME'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['SLV_UEBERGABE'][$i] . '</td>';
									echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluesselvergaben['SLS_BEZEICHNUNG'][$i] . '</td>';
									if(($Rechtestufe&8)==8){
											echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href='./schluessel_Main.php?cmdAktion=Vergaben&txtSLV_KEY_loeschen=". $rsSchluesselvergaben['SLV_KEY'][$i] ."'><img border=0 src=/bilder/muelleimer.png name=cmdLoeschen title='L&ouml;schen'></a></td>";
									}
								echo '</tr>';
							}
						}
						print "</table>";
						
						if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
						{
								echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
						}
						else
						{
								echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
						}
						
				}		
		echo '</form>';
}
elseif(isset($_POST['cmdBearbeiten_x']) || ($txtSLVKEY!=0 && ($Rechtestufe&2)==2)) // Formular f�r die Bearbeitung der Datens�tze
{
		$SQL='SELECT * FROM SCHLUESSELVERGABEN WHERE SLV_KEY='.$txtSLVKEY;
		
		$rsSchluesselvergaben = awisOpenRecordset($con,$SQL);
		$rsSchluesselvergabenZeilen = $awisRSZeilen;
		
		if($rsSchluesselvergabenZeilen==0)		// Keine Daten
		{
			echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
		}
		else
		{
				echo '<form name=frmVergaben method=post action=./schluessel_Main.php?cmdAktion=Vergaben>';
				
				echo "<table  width=100% id=DatenTabelle border=1><tr>";
				echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
				echo "</tr>";
				
				if (isset($_POST['cmdBearbeiten_x']) && $_POST['txtSLV_PER_NR']!='')
				{
						$zahl=str_replace(" ","",$_POST['txtSLV_PER_NR']);
						$zahl=str_replace("*","",$zahl);
						
						if(is_numeric($zahl))
						{
								$SQL='SELECT * FROM v_personal_komplett_map WHERE PER_NR ' . awisLIKEoderIST($_POST['txtSLV_PER_NR'],TRUE,FALSE,FALSE,0);
								$SQL.=' ORDER BY PER_NACHNAME ASC';
								$rsSLV_PER_NR = awisOpenRecordset($con,$SQL);
								$rsSLV_PER_NRZeilen = $awisRSZeilen;
								
								if($rsSLV_PER_NRZeilen>1)
								{
										echo "<tr>";
												echo "<td id=FeldBez width=150>Mitarbeiter</td>";
												echo "<td><input type=hidden name=txtSLV_PER_NR_old value='". $rsSchluesselvergaben['SLV_PER_NR'][0] ."'><select name=txtSLV_PER_NR>";
												echo '<option value=-1>Bitte w�hlen...</option>';
												for($SLV_PER_NRZeile=0;$SLV_PER_NRZeile<$rsSLV_PER_NRZeilen;$SLV_PER_NRZeile++)
												{
														echo "<option value='" . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "'>" . $rsSLV_PER_NR['PER_NACHNAME'][$SLV_PER_NRZeile] . " " . $rsSLV_PER_NR['PER_VORNAME'][$SLV_PER_NRZeile] . " - "  . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "</option>";
												}
												echo '</select>';
												unset($rsSLV_PER_NR);
										echo "</tr>";
								}
								else
								{
										echo "<tr>";
												echo "<td id=FeldBez width=150>Mitarbeiter</td>";
												echo "<td id=TabellenZeileGrau><input name=txtSLV_PER_NR size=68 value='" . $rsSLV_PER_NR['PER_NACHNAME'][0] . " " . $rsSLV_PER_NR['PER_VORNAME'][0] . " - " . $rsSLV_PER_NR['PER_NR'][0] . "'></td><input type=hidden name=txtSLV_PER_NR value='" . $rsSLV_PER_NR['PER_NR'][0] . "'><input type=hidden name=txtSLV_PER_NR_old value='". $rsSchluesselvergaben['SLV_PER_NR'][0] ."'>";
										echo "</tr>";
								}
						}
						else
						{
								$SQL='SELECT * FROM v_personal_komplett_map WHERE UPPER(PER_NACHNAME) '. awisLIKEoderIST($_POST['txtSLV_PER_NR'],TRUE,FALSE,FALSE,0) . ' OR UPPER(PER_VORNAME) ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
								$SQL.=' ORDER BY PER_NACHNAME ASC';
								$rsSLV_PER_NR = awisOpenRecordset($con,$SQL);
								$rsSLV_PER_NRZeilen = $awisRSZeilen;
								
								echo "<tr>";
										echo "<td id=FeldBez width=150>Mitarbeiter</td>";
										echo "<td><input type=hidden name=txtSLV_PER_NR_old value='". $rsSchluesselvergaben['SLV_PER_NR'][0] ."'><select name=txtSLV_PER_NR>";
										echo '<option value=-1>Bitte w�hlen...</option>';
										for($SLV_PER_NRZeile=0;$SLV_PER_NRZeile<$rsSLV_PER_NRZeilen;$SLV_PER_NRZeile++)
										{
												echo "<option value='" . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "'>" . $rsSLV_PER_NR['PER_NACHNAME'][$SLV_PER_NRZeile] . " " . $rsSLV_PER_NR['PER_VORNAME'][$SLV_PER_NRZeile] . " - "  . $rsSLV_PER_NR['PER_NR'][$SLV_PER_NRZeile] . "</option>";
										}
										echo '</select>';
										unset($rsSLV_PER_NR);
								echo "</tr>";
						}
				}
				else
				{	
						echo "<tr>";
								echo "<td id=FeldBez width=150>Mitarbeiter</td>";
								echo "<td id=TabellenZeileGrau><table><tr><td><input name=txtSLV_PER_NR size=68 value='". $rsSchluesselvergaben['SLV_PER_NR'][0] ."'><input type=hidden name=txtSLV_PER_NR_old value='". $rsSchluesselvergaben['SLV_PER_NR'][0] ."'></td><td align=left><input type=image title='Suchen' src=/bilder/filter.png name=cmdBearbeiten></td></tr></table></td>";
						echo "</tr>";
				}
				
				// START Mitarbeiter-Vertreter
				
				if (isset($_POST['cmdBearbeitenVertreter_x']) && $_POST['txtSLV_PER_NR_VERTRETER']!='')
				{
						$zahl=str_replace(" ","",$_POST['txtSLV_PER_NR_VERTRETER']);
						$zahl=str_replace("*","",$zahl);
						
						if(is_numeric($zahl))
						{
								$SQL='SELECT * FROM v_personal_komplett_map WHERE PER_NR ' . awisLIKEoderIST($_POST['txtSLV_PER_NR_VERTRETER'],TRUE,FALSE,FALSE,0);
								$SQL.=' ORDER BY PER_NACHNAME ASC';
								$rsSLV_PER_NR_VERTRETER = awisOpenRecordset($con,$SQL);
								$rsSLV_PER_NR_VERTRETERZeilen = $awisRSZeilen;
								
								if($rsSLV_PER_NR_VERTRETERZeilen>1)
								{
										echo "<tr>";
												echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
												echo "<td><input type=hidden name=txtSLV_PER_NR_VERTRETER_old value='". $rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0] ."'><select name=txtSLV_PER_NR_VERTRETER>";
												echo '<option value=-1>Bitte w�hlen...</option>';
												for($SLV_PER_NR_VERTRETERZeile=0;$SLV_PER_NR_VERTRETERZeile<$rsSLV_PER_NR_VERTRETERZeilen;$SLV_PER_NR_VERTRETERZeile++)
												{
														echo "<option value='" . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "'>" . $rsSLV_PER_NR_VERTRETER['PER_NACHNAME'][$SLV_PER_NR_VERTRETERZeile] . " " . $rsSLV_PER_NR_VERTRETER['PER_VORNAME'][$SLV_PER_NR_VERTRETERZeile] . " - "  . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "</option>";
												}
												echo '</select>';
												unset($rsSLV_PER_NR_VERTRETER);
										echo "</tr>";
								}
								else
								{
										echo "<tr>";
												echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
												echo "<td id=TabellenZeileGrau><input name=txtSLV_PER_NR_VERTRETER size=68 value='" . $rsSLV_PER_NR_VERTRETER['PER_NACHNAME'][0] . " " . $rsSLV_PER_NR_VERTRETER['PER_VORNAME'][0] . " - " . $rsSLV_PER_NR_VERTRETER['PER_NR'][0] . "'></td><input type=hidden name=txtSLV_PER_NR_VERTRETER value='" . $rsSLV_PER_NR_VERTRETER['PER_NR'][0] . "'><input type=hidden name=txtSLV_PER_NR_VERTRETER_old value='". $rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0] ."'>";
										echo "</tr>";
								}
						}
						else
						{
								$SQL='SELECT * FROM v_personal_komplett_map WHERE UPPER(PER_NACHNAME) '. awisLIKEoderIST($_POST['txtSLV_PER_NR'],TRUE,FALSE,FALSE,0) . ' OR UPPER(PER_VORNAME) ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
								$SQL.=' ORDER BY PER_NACHNAME ASC';
								$rsSLV_PER_NR_VERTRETER = awisOpenRecordset($con,$SQL);
								$rsSLV_PER_NR_VERTRETERZeilen = $awisRSZeilen;
								
								echo "<tr>";
										echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
										echo "<td><input type=hidden name=txtSLV_PER_NR_VERTRETER_old value='". $rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0] ."'><select name=txtSLV_PER_NR_VERTRETER>";
										echo '<option value=-1>Bitte w�hlen...</option>';
										for($SLV_PER_NR_VERTRETERZeile=0;$SLV_PER_NR_VERTRETERZeile<$rsSLV_PER_NR_VERTRETERZeilen;$SLV_PER_NR_VERTRETERZeile++)
										{
												echo "<option value='" . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "'>" . $rsSLV_PER_NR_VERTRETER['PER_NACHNAME'][$SLV_PER_NR_VERTRETERZeile] . " " . $rsSLV_PER_NR_VERTRETER['PER_VORNAME'][$SLV_PER_NR_VERTRETERZeile] . " - "  . $rsSLV_PER_NR_VERTRETER['PER_NR'][$SLV_PER_NR_VERTRETERZeile] . "</option>";
										}
										echo '</select>';
										unset($rsSLV_PER_NR_VERTRETER);
								echo "</tr>";
						}
				}
				else
				{	
						echo "<tr>";
								echo "<td id=FeldBez width=150>Mitarbeiter-Vertreter</td>";
								echo "<td id=TabellenZeileGrau><table><tr><td><input name=txtSLV_PER_NR_VERTRETER size=68 value='". $rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0] ."'><input type=hidden name=txtSLV_PER_NR_VERTRETER_old value='". $rsSchluesselvergaben['SLV_PER_NR_VERTRETER'][0] ."'></td><td align=left><input type=image title='Suchen' src=/bilder/filter.png name=cmdBearbeitenVertreter></td></tr></table></td>";
						echo "</tr>";
				}
				
				// ENDE Mitarbeiter-Vertreter
				
				$SQL='';
				$SQL='SELECT SLN_KEY,SLN_SCHLUESSELNUMMER, SLN_SCHLUESSELLFDNR, SLG_BEZEICHNUNG, SLU_BEZEICHNUNG FROM SCHLUESSELNUMMERN, ';
				$SQL.='SCHLUESSELGRUPPEN, SCHLUESSELUNTERGRUPPEN WHERE SLN_FIL_ID='.$Params[0].' AND ';
				$SQL.='SLN_SLG_KEY=SLG_KEY(+) AND SLN_SLU_KEY=SLU_KEY(+) AND SLN_STATUS=1 ';
				$SQL.='ORDER BY SLG_BEZEICHNUNG, SLN_SCHLUESSELLFDNR';
				$rsSLV_SLN_KEY = awisOpenRecordset($con,$SQL);
				$rsSLV_SLN_KEYZeilen = $awisRSZeilen;
				
				echo "<tr>";
						echo "<td id=FeldBez width=150>Schl&uuml;ssel-Nummer</td>";
						echo "<td><input type=hidden name=txtSLV_SLN_KEY_old value=". $rsSchluesselvergaben['SLV_SLN_KEY'][0] ."><select name=txtSLV_SLN_KEY>";
						echo '<option value=-1>Bitte w�hlen...</option>';
						for($SLV_SLN_KEYZeile=0;$SLV_SLN_KEYZeile<$rsSLV_SLN_KEYZeilen;$SLV_SLN_KEYZeile++)
						{		
								if($rsSLV_SLN_KEY['SLN_KEY'][$SLV_SLN_KEYZeile]==$rsSchluesselvergaben['SLV_SLN_KEY'][0])
								{
										echo '<option value=' . $rsSLV_SLN_KEY['SLN_KEY'][$SLV_SLN_KEYZeile] . ' selected="selected">' . $rsSLV_SLN_KEY['SLN_SCHLUESSELNUMMER'][$SLV_SLN_KEYZeile] . '-' . $rsSLV_SLN_KEY['SLN_SCHLUESSELLFDNR'][$SLV_SLN_KEYZeile] . ' - '  . $rsSLV_SLN_KEY['SLG_BEZEICHNUNG'][$SLV_SLN_KEYZeile] . ' - ' . $rsSLV_SLN_KEY['SLU_BEZEICHNUNG'][$SLV_SLN_KEYZeile] .  '</option>';
								}
								else
								{
										echo '<option value=' . $rsSLV_SLN_KEY['SLN_KEY'][$SLV_SLN_KEYZeile] . '>' . $rsSLV_SLN_KEY['SLN_SCHLUESSELNUMMER'][$SLV_SLN_KEYZeile] . '-' . $rsSLV_SLN_KEY['SLN_SCHLUESSELLFDNR'][$SLV_SLN_KEYZeile] . ' - '  . $rsSLV_SLN_KEY['SLG_BEZEICHNUNG'][$SLV_SLN_KEYZeile] . ' - ' . $rsSLV_SLN_KEY['SLU_BEZEICHNUNG'][$SLV_SLN_KEYZeile] .  '</option>';
								}
								
						}
						echo '</select>';
						unset($rsSLV_SLN_KEY);
				echo "</tr>";
				
				$SQL='';
				$SQL='SELECT SLS_KEY, SLS_BEZEICHNUNG ';
				$SQL.='FROM SCHLUESSELSTATUS';
				$rsSLV_SLS_KEY = awisOpenRecordset($con,$SQL);
				$rsSLV_SLS_KEYZeilen = $awisRSZeilen;
				
				echo "<tr>";
						echo "<td id=FeldBez width=150>Schl&uuml;ssel-Status</td>";
						echo "<td><input type=hidden name=txtSLV_SLS_KEY_old value=". $rsSchluesselvergaben['SLV_SLS_KEY'][0] ."><select name=txtSLV_SLS_KEY>";
						for($SLV_SLS_KEYZeile=0;$SLV_SLS_KEYZeile<$rsSLV_SLS_KEYZeilen;$SLV_SLS_KEYZeile++)
						{
								if($rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile]==$rsSchluesselvergaben['SLV_SLS_KEY'][0]){
										echo '<option value=' . $rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile] . ' selected="selected">' . $rsSLV_SLS_KEY['SLS_BEZEICHNUNG'][$SLV_SLS_KEYZeile] . '</option>';
								}
								else
								{
										echo '<option value=' . $rsSLV_SLS_KEY['SLS_KEY'][$SLV_SLS_KEYZeile] . '>' . $rsSLV_SLS_KEY['SLS_BEZEICHNUNG'][$SLV_SLS_KEYZeile] . '</option>';
								}
						}
						echo '</select>';
						unset($rsSLV_SLS_KEY);
				echo "</tr>";
				
				echo "<tr>";
						echo "<td id=FeldBez width=150>&Uuml;bergabe-Datum TT.MM.JJJJ</td>";
						echo "<td id=TabellenZeileGrau><input name=txtSLV_UEBERGABE size=30 value=".substr($rsSchluesselvergaben['SLV_UEBERGABE'][0],0,10)."><input type=hidden name=txtSLV_UEBERGABE_old value=". $rsSchluesselvergaben['SLV_UEBERGABE'][0] ."></td>";
				echo "</tr>";
				
				echo "<tr>";
						echo "<td id=FeldBez valign=top>Bemerkung</td>";
						echo "<td id=TabellenZeileGrau><textarea name=txtSLV_BEMERKUNG cols=50 rows=5>".$rsSchluesselvergaben['SLV_BEMERKUNG'][0]."</textarea><input type=hidden name=txtSLV_BEMERKUNG_old value=". urlencode($rsSchluesselvergaben['SLV_BEMERKUNG'][0]) ."></td>";
				echo "</tr>";
				echo "<tr>";
						echo "<td colspan=2 align=left>Erstellt von User " . $rsSchluesselvergaben['SLV_CREAUSER'][0] . " am " . $rsSchluesselvergaben['SLV_CREAUSERDAT'][0] . "</td>";
				echo "</tr>";
				echo "<tr>";
						echo "<td colspan=2 align=left>Letze &Auml;nderung durch User " . $rsSchluesselvergaben['SLV_USER'][0] . " am " . $rsSchluesselvergaben['SLV_USERDAT'][0] . "</td>";
				echo "</tr>";
				echo "</table>";
				
				echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
				echo "<input type=hidden name=txtSLV_KEY value=".$rsSchluesselvergaben['SLV_KEY'][0].">"; // Aufruf der gleichen Seite nach dem speichern
				echo "</form>";
		
		}
}
else
{
echo '<form name=frmVergaben method=post action=./schluessel_Main.php?cmdAktion=Vergaben>';

echo '<table>';
echo '<tr><td width=50>Filiale</td>';
echo '<td><select name=txtFIL_ID>';

//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ORDER BY FIL_ID");
//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8 ORDER BY FIL_ID");
//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
//union
//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32 ORDER BY FIL_ID");

//$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=8
//union
//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('6','7','8') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
//union
//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('A','B','C') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
//union
//SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_GRUPPE IN('D','E','F') AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
//ORDER BY FIL_ID");

$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='BRD' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
union
SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='OES' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
union
SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='CZE' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
union
SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='NED' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
union
SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='SUI' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
union
SELECT FIL_ID, FIL_BEZ FROM FILIALEN JOIN FILIALINFOS ON (FIF_FIL_ID=FIL_ID(+)) WHERE FIL_LAN_WWSKENN='ITA' AND FILIALINFOS.FIF_FIT_ID=34 AND FILIALINFOS.FIF_WERT IS NOT NULL AND FIF_IMQ_ID=32
union
SELECT FIL_ID, FIL_BEZ FROM FILIALEN WHERE FIL_LAN_WWSKENN='BRD' AND FIL_ID in (597,683)
ORDER BY FIL_ID");

$rsFilialenZeilen = $awisRSZeilen;

echo "<option value=0>::Auswahl::</option>";
for($FilialenZeile=0;$FilialenZeile<$rsFilialenZeilen;$FilialenZeile++)
{
	echo "<option value=" . $rsFilialen['FIL_ID'][$FilialenZeile] . ">" . $rsFilialen['FIL_ID'][$FilialenZeile] . " - " . $rsFilialen['FIL_BEZ'][$FilialenZeile] . "</option>";
}
echo "</select></td>";
echo "<td>&nbsp;<input type=image border=0 src=/bilder/eingabe_ok.png accesskey=a name=cmdAnzeigen title='Anzeigen (Alt+A)'></td>";
echo "</tr>";
echo '</table>';

echo '<hr>';

unset($rsFilialen);
		
echo '</form>';
}

?>
</body>
</html>
