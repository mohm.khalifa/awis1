<?php
/**
 * Created by PhpStorm.
 * User: roesch_n
 * Date: 04.11.2016
 * Time: 10:40
 */

require_once 'versicherungen_Funktionen.php';

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_HISTKEY;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FGP', 'FGP*');
    $TextKonserven[] = array('FPH', 'FPH_*');
    $TextKonserven[] = array('FGK', '*');
    $TextKonserven[] = array('FKH', '*');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Zugriffsrechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'txt_FGBruttopreis');

    $Form = new AWISFormular();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);
    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Form->Formular_Start();

    $Form->SchreibeHTMLCode('<form name=frmVersicherungenAutoglasPositionen action=./versicherungen_Details.php?cmdAktion=Details&Seite=AutoglasPositionen' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

    $SQL = 'SELECT kopf.AUTOGLAS_KEY, kopf.DS, kopf.ZEITSTEMPEL';
    $SQL .= '      FROM';
    $SQL .= '        ( SELECT DISTINCT \'Neuester Vorgang\' as DS, fgk.FGK_KEY AS AUTOGLAS_KEY,';
    $SQL .= '          fgk.FGK_VORGANGNR                   AS VORGANGNR,';
    $SQL .= '          fgk.FGK_ZEITSTEMPEL                 AS ZEITSTEMPEL';
    $SQL .= '        FROM FGKOPFDATEN fgk';
    $SQL .= '        INNER JOIN FGPOSITIONSDATEN fgp';
    $SQL .= '        ON fgk.FGK_KEY = fgp.FGP_FGK_KEY';
    $SQL .= '        UNION';
    $SQL .= '        SELECT DISTINCT \'Historisierter Vorgang\' as DS, fkh.FKH_KEY AS AUTOGLAS_KEY,';
    $SQL .= '          fph.FPH_VORGANGNR             AS VORGANGNR,';
    $SQL .= '          fph.FPH_ZEITSTEMPEL           AS ZEITSTEMPEL';
    $SQL .= '        FROM FGKOPFDATEN_HIST fkh ';
    $SQL .= '        INNER JOIN FGPOSITIONSDATEN_HIST fph';
    $SQL .= '        ON fkh.FKH_KEY = fph.FPH_FKH_KEY';
    $SQL .= '        ) kopf';
    $SQL .= '      INNER JOIN VERSVORGANGSSTATUS vvs';
    $SQL .= '      ON vvs.VVS_VORGANGNR = kopf.VORGANGNR';
    $SQL .= '      WHERE vvs.VVS_KEY    = ' . $DB->WertSetzen('VVS', 'N0', $AWIS_KEY1);
    $SQL .= '      ORDER BY kopf.ZEITSTEMPEL DESC';

    $rsaktVorgang = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VVS', false));

    if($rsaktVorgang->AnzahlDatensaetze() == 0)
    {
        echo '<span class=HinweisText>Es wurden keine Datensätze gefunden.</span>';
    }
    else
    {
        $Form->ZeileStart('display:block', 'AutoglasVorgang');
        $Form->Erstelle_TextLabel('Autoglas Vorgang:', 150);
        
        $Form->Erstelle_SelectFeld('*AUTOGLAS_KEY', $rsaktVorgang->FeldInhalt('AUTOGLAS_KEY'), '300:290', true, $SQL, '', '', '', '', '', 'onchange = "key_AJAX_AUTOGLAS_KEY(event);"', '', $DB->Bindevariablen('VVS'));
        $Form->AuswahlBox('AJAX_AUTOGLAS_KEY', 'Box1', '', 'VVS_Autoglasvorgang', 'sucAUTOGLAS_KEY', 10, 1, '', 'T', true, '', '', 'display:none', 10, '', '', '', 0, 'display:none');
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        ob_start();

        $ajax = new versicherungen_Funktionen();
        $ajax->ErstelleAutoglasUebersicht($rsaktVorgang->FeldInhalt('AUTOGLAS_KEY'));

        $Form->AuswahlBoxHuelle('Box1', '', '', ob_get_clean());
    }
    $Form->Formular_Ende();
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    }
    else
    {
        echo 'allg . Fehler:' . $ex->getMessage();
    }
}
?>

