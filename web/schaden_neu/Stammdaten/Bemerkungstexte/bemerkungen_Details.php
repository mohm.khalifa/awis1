<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht40009 = $AWISBenutzer->HatDasRecht(40009);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SCHAD_SBE'));
	
	$Fehler = 0;
	$Save = 0;
	$FehlerFest = false;
	$NeuAnlage = 0;
	
	if (isset($_GET['button']))
	{
		$Aktiv = '';
		if($_GET['aktiv'] == '1')
		{
			$Aktiv = 0;
		}
		else 
		{
			$Aktiv = 1;
		}
		$SQL = "UPDATE VORSCHLAG_BEM ";
		$SQL .="SET AKTIV =".$Aktiv;
		$SQL .= ',  BEMUSER=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ',  BEMUSERDAT=SYSDATE';
		$SQL .=" WHERE ID = " .$_GET['button'];
		$DBSCHAD->Ausfuehren($SQL);
	}
	
    if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
    {
		$Param['ORDER'] = 'BEM';
 	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht40009==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_GET['ID']))
	{
		$AWIS_KEY1 = $_GET['ID'];
	}
	if(isset($_POST['txtID']))
	{
		$AWIS_KEY1 = $_POST['txtID'];
	}
	
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{	
		$Param['BES_BEMKERUNG'] = 	$Form->Format('T',$_POST['sucBES_BEMERKUNG'],true);
		$Param['BES_AKTIV']= $Form->Format('N0',$_POST['sucBES_AKTIV'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./bemerkungen_speichern.php');
		if($Fehler == 0)
		{
			$AWIS_KEY1 = '';
		}
	}

	/** Test-SQL Eskalationen **/
	
	$SQL = ' select ID,BEM,AKTIV,BEMUSER,BEMUSERDAT,row_number() OVER ( ';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	$SQL .= 'select * ';
	$SQL .=	' from vorschlag_bem) ';

	
	if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '')
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		//if ($Bedingung != '' and (!isset($_POST['cmdHinzufuegen_x'])) and (!isset($_POST['cmdDSNeu_x'])) and !isset($_GET['HRT_KEY']) and $NeuAnlage != 1 and !isset($_POST['txtHRT_KEY']))
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}
	if($AWIS_KEY1 != '')
	{
		if($NeuAnlage == 1 or isset($_GET['HRT_KEY']) or $AWIS_KEY1 <> '')
		{
			$SQL .= ' WHERE ID='.$AWIS_KEY1;
		}
		else 
		{
			$SQL .= ' AND ID='.$AWIS_KEY1;
		}
		
	
	}
	


	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);

	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	
	if(!isset($_GET['ID']) and $AWIS_KEY1 == '')
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsSBE = $DBSCHAD->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['BEMTEXT'] = 900;
	$FeldBreiten['AKTIV'] = 20;
	
	
	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 3.5);
	}
	
	$Form->SchreibeHTMLCode('<form name="frmSBEDetails" action="./bemerkungen_Main.php?cmdAktion=Details" method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	if (($rsSBE->AnzahlDatensaetze() > 1))
	{
		$Form->ZeileStart();
		// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
		//$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
		// �berschrift der Listenansicht mit Sortierungslink: Filiale
		$Link = './bemerkungen_Main.php?cmdAktion=Details&Sort=BEM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEMTEXT'], $FeldBreiten['BEMTEXT'], '', $Link);
		if (($Recht40009&2) == 2)
		{
			$Link = './bemerkungen_Main.php?cmdAktion=Details&Sort=AKTIV'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AKTIV'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBAKTIV'];
			$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['AKTIV'], '',$Link,$TTT);
		}
		/*
		if(($Recht34000&8)==8)
		{
			// �berschrift der Listenansicht: History
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_HIST'], $FeldBreiten['Hist'], '', '');
		}*/
		$Form->ZeileEnde();
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsSBE->EOF())
		{
			$Form->ZeileStart();
			if (($Recht40009&2) == 2)
			{
				$Link = './bemerkungen_Main.php?cmdAktion=Details&ID='.$rsSBE->FeldInhalt('ID') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			}
			else 
			{
				$Link = '';
			}
			$TTT = $rsSBE->FeldInhalt('BEM');
			$Form->Erstelle_ListenFeld('BES_BEMTEXT', $rsSBE->FeldInhalt('BEM'), 0, $FeldBreiten['BEMTEXT'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
			$LinkAktiv = '';
			if (($Recht40009&2) == 2)
			{
				$LinkAktiv .=  './bemerkungen_Main.php?cmdAktion=Details&button='.$rsSBE->FeldInhalt('ID').'&aktiv='.$rsSBE->FeldInhalt('AKTIV');
				if ($rsSBE->FeldInhalt('AKTIV') == '1')
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Aktiv']);
				}
				else
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Inaktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['AKTIV'] , ($DS%2));
				$IconsArray = '';
			}
			
			/** Neue History erst mit VERSION 2
			 * 
			 */
			
			/*
			if(($Recht34000&8)==8)
			{
				$SQL  = ' Select * from QMHISTORY '; 
				$SQL .= ' where QMH_QMP_KEY='.$rsKDI->FeldInhalt('QMP_KEY');
				$SQL .= ' order by QMH_USERDAT';
				
				$rsQMH = $DB->RecordSetOeffnen($SQL);
				$Bild = '';
				
				if($rsQMH->AnzahlDatensaetze() > 0)
				{
					$Bild = '/bilder/attach.png';
					$Form->Erstelle_ListenBild('script','QMZ_HIST','onclick="ladeDiv('.$rsKDI->FeldInhalt('QMP_KEY').');"',$Bild,'',($DS%2),'',16,18,$FeldBreiten['Hist'],'C');
				}
				else 
				{
					$Form->Erstelle_ListenFeld('QMZ_HIST','', 0, $FeldBreiten['Hist'], false, ($DS%2), '','', 'T', 'C','');
				}				
			}
			*/
			$Form->ZeileEnde();
			$DS++;
			$rsSBE->DSWeiter();
		}
		
		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsSBE->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './bemerkungen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
		
		//$Link = './bemerkungen_Main.php?cmdAktion=Details';
		//$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	elseif(($rsSBE->AnzahlDatensaetze() == 1) or $AWIS_KEY1 == '-1')
	{
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./bemerkungen_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBE->FeldInhalt('BEMUSER'));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBE->FeldInhalt('BEMUSERDAT'));
			$Form->InfoZeile($Felder,'');

			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEMTEXT'] . ':',140);
		$AWISCursorPosition = 'txtBES_BEMERKUNGEN';
		$Form->Erstelle_Textarea('!BES_BEMERKUNGEN',isset($_POST['txtBES_BEMERKUNGEN'])?$_POST['txtBES_BEMERKUNGEN']:$rsSBE->FeldInhalt('BEM'),750,90,3,true);
		$Form->ZeileEnde();
	
		if (isset($_POST['txtBES_BEMERKUNGEN']))
		{
			if (isset($_POST['txtCHK_AKTIV']))
			{
				$CHK_AKT = 1;
			}
			else
			{
				$CHK_AKT = 0;
			}
		
		}
		else if($AWIS_KEY1 == '-1')
		{
			$CHK_AKT = 1;
		}
		else
		{
			$CHK_AKT =$rsSBE->FeldInhalt('AKTIV')==1?1:0;
		}
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVSUCH'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIV',$CHK_AKT,40,true,1);
		$Form->ZeileEnde();	
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
	if($AWIS_KEY1 == '')
	{
		$AWIS_KEY1 = $rsSBE->FeldInhalt('ID');
	}
	$Form->Erstelle_HiddenFeld('ID',$AWIS_KEY1);
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht40009&4)==4 and ($AWIS_KEY1 != '-1' and $rsSBE->AnzahlDatensaetze() != 1))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht40009&2)==2 and ($AWIS_KEY1 == '-1' or $rsSBE->AnzahlDatensaetze() == 1))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SCHAD_SBE',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	if(isset($Param['BES_BEMKERUNG']) AND $Param['BES_BEMKERUNG']!='')
	{
		$Bedingung .= 'AND UPPER(BEM) like' . '(upper(\''.$Param['BES_BEMKERUNG'] . '\')) ';
	}
	if(isset($Param['BES_AKTIV']) AND $Param['BES_AKTIV']!='')
	{
		$Bedingung .= 'AND AKTIV ' . $DB->LikeOderIst($Param['BES_AKTIV']) . ' ';
	}
	
	return $Bedingung;
}

?>