<?php
require_once 'bewerberverwaltung_Details_Bewerbungen_Funktionen.php';
/**
 * Details zu den Bewerbern
 *
 * @author    Patrick Gebhardt
 * @copyright ATU Auto Teile Unger
 * @version   201606205211
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SEQBWH;
global $SEQ;
global $Duplikat;
global $EingabeOK;
global $Folgestatus;
global $ScrollPosition;
global $rsHRK;
global $Speichernausblenden;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('HRK', '%');
    $TextKonserven[] = array('BEW', 'ZurueckSuche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Liste', 'lst_OffenMass');
    $TextKonserven[] = array('Liste', 'lst_MassErfuellt');
    $TextKonserven[] = array('Wort', 'PDFErzeugen');

    $AWISBenutzer = awisBenutzer::Init();

    $script = "<script type='text/javascript'>
		
	       
	    function setzePunkte(Feld) {
			 v=Feld.value;
          if(v.match(/^\d{2}$/))
            {
              Feld.value+='.';return;
            }
          if(v.match(/^\d{2}\.\d{2}$/))
            {
              Feld.value+='.';return;
            }
	    
		 }
	       
		  function toggleDiv(KEY,Tabelle) {
		
			var position = document.getElementById('Zeile'+Tabelle+'_'+KEY).offsetTop;
			if(window.ActiveXObject){
				if(Tabelle == 'HRK')
				{
					position = position + 123; //F�r IE 110 mehr!?
				}
			    else
				{
					position = position + 408; //F�r IE 110 mehr!?
				}
			}
			document.getElementById('Hist_'+Tabelle).style.top = position ;	// Position Div an Datenzeile festmachen.
			if(Tabelle == 'HRK')
			{
				document.getElementById('Hist_'+Tabelle).style.left =  20;
			}
			else
			{
				document.getElementById('Hist_'+Tabelle).style.left =  70;
			}
		
			if(document.getElementById('Hist_'+Tabelle).style.display == 'none')
			{
				document.getElementById('Hist_'+Tabelle).style.display = 'block'; // History einblenden
			}
		}
		
		function closeHist(Tabelle) {
			if(document.getElementById('Hist_'+Tabelle).style.display = 'block')
			{
				document.getElementById('Hist_'+Tabelle).style.display = 'none'; // History schlie�en
			}
		 }
		
		    function ladeDiv(KEY,Tabelle) {
				var test = null;
				if(window.XMLHttpRequest){
					test = new XMLHttpRequest();
				}
				else if(window.ActiveXObject){
					try{
						test = new ActiveXObject('Msxml2.XMLHTTP.6.0');
					} catch(e){
						try{
							test = new ActiveXObject('Msxml2.XMLHTTP.3.0');
						}
						catch(e){}
					}
				}
				if(test==null)
				{
					alert('Ihr Browser unterst�tzt kein Ajax!');
				}
				
				test.open('GET','/daten/bewerberverwaltung'+Tabelle+'_Hist.php?KEY='+KEY+'&Tabelle='+Tabelle);
				test.send(null);
	
				test.onreadystatechange = function() {
            	if(test.readyState == 4 && test.status == 200) {
				document.getElementById('Hist_'+Tabelle).innerHTML = test.responseText; // Wert von Seite per Ajax ins Div laden
            	}
	
		
				toggleDiv(KEY,Tabelle);
        }
		
	
		  }
	</script>";

    if (isset($_POST['cmdDSNeu_y'])) {
        $AWIS_KEY1 = '-1';
    } elseif (isset($_GET['HRK_KEY'])) {
        $AWIS_KEY1 = intval($_GET['HRK_KEY']);
    } elseif (isset($_POST['txtHRK_KEY'])) {
        $AWIS_KEY1 = intval($_POST['txtHRK_KEY']);
    } else {
        $AWIS_KEY1 = '';
    }

    if (isset($SEQBWH) and $SEQBWH > 0) {
        $AWIS_KEY2 = $SEQBWH;
    } elseif (isset($_GET['BWH_KEY']) and !isset($_POST['cmdDSNeu_y'])) {
        $AWIS_KEY2 = intval($_GET['BWH_KEY']);
    } else {
        $AWIS_KEY2 = '';
    }

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht39001 = $AWISBenutzer->HatDasRecht(39001);

    $Aendern = ($Recht39001 & 2) == 2;

    $Speichernausblenden = false;
    $Param = array();
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BEW'));

    if (!isset($Param['ORDER']) or $Param['ORDER'] == '') {
        $Param['ORDER'] = 'HRK_NACHNAME';
    }

    if (isset($_GET['Sort']) and $_GET['Sort'] != '') {        // wenn GET-Sort, dann nach diesen Feld sortieren
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    }

    if (isset($_REQUEST['Block']) and $_REQUEST['Block'] != '') {
        $Param['BLOCK'] = $_REQUEST['Block'];
    } else {
        $Param['BLOCK'] = 1;
    }

    if ($Recht39001 == 0) {
        $Form->Fehler_KeineRechte();
    } elseif (isset($_POST['cmdSuche_x'])) {

        $Param['HRK_NACHNAME'] = $Form->Format('T', $_POST['sucHRK_NACHNAME'], true);
        $Param['HRK_VORNAME'] = $Form->Format('T', $_POST['sucHRK_VORNAME'], true);
        $Param['HRK_ORT'] = $Form->Format('T', $_POST['sucHRK_ORT'], true);
        $Param['HRK_EMAIL'] = $Form->Format('T', $_POST['sucHRK_EMAIL'], true);

        $Param['BEW_FIRMIERUNG'] = $Form->Format('T', $_POST['sucBEW_FIRMIERUNG'], true);

        //Abteilung als Filiale behandeln, um SQLs zu vereinfachen
        if(isset($_POST['sucBEW_FILIALE']) and $_POST['sucBEW_FILIALE'] != '' ){
            $Param['BEW_FILIALE'] = $Form->Format('T', $_POST['sucBEW_FILIALE'], true);
        }
        elseif(isset($_POST['sucBEW_ABTEILUNG']) and $_POST['sucBEW_ABTEILUNG']!= $AWISSprachKonserven['Wort']['txt_BitteWaehlen']){
            $Param['BEW_FILIALE'] = $Form->Format('T', $_POST['sucBEW_ABTEILUNG'], true);

        }
        else{
            $Param['BEW_FILIALE'] = '';
        }

        if(isset($_POST['sucBEW_TAETIGKEITEN'])){
            $Param['BEW_TAETIGKEITEN'] = $Form->Format('T', $_POST['sucBEW_TAETIGKEITEN'], true);
        }

        $Param['HRK_GEBURTSDATUM'] = $Form->Format('D', $_POST['sucHRK_GEBURTSDATUM'], true);
        $Param['BEW_GEBIET'] = $Form->Format('T', $_POST['sucBEW_GEBIET'], true);
        $Param['BEW_REGION'] = $Form->Format('T', $_POST['sucBEW_REGION'], true);
        $Param['BEW_STATUS'] = $Form->Format('T', $_POST['sucBEW_STATUS'], true);

        $Param['BEW_VORSTELLUNGSGESPRAECH'] = $Form->Format('T', $_POST['sucBEW_VORSTELLUNGSGESPRAECH'], true);
        $Param['BEW_VORSTELLUNGSDATUM'] = $Form->Format('T', $_POST['sucBEW_VORSTELLUNGSDATUM'], true);
        $Param['BEW_INTERNER_BEWERBER'] = $Form->Format('T', $_POST['sucBEW_INTERNER_BEWERBER'], true);

        $Param['ALLE'] = isset($_POST['sucAlleAnzeigen'])?'on':'';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
        $Param['BLOCK'] = 1;
    } elseif (isset($_POST['cmdSpeichern_x']) or isset($_POST['cmdDuplikat']) or issetCMDStatusWeiter()) {
      
        include('./bewerberverwaltung_speichern.php');

        if (isset($SEQBWH) and $SEQBWH > 0) {
            $AWIS_KEY2 = $SEQBWH;
        }
        if (isset($SEQ) and $SEQ > 0) {
            $AWIS_KEY1 = $SEQ;
        }
    } elseif (isset($_POST['cmdLoeschen_x']) or isset($_POST['cmdLoeschenOK']) or (isset($_GET['Del'])) and !isset($_POST['cmdLoeschenAbbrechen'])) {
        include './bewerberverwaltung_Loeschen.php';
    }
    echo $script;

    $Bedingung = '';
    $SQL = '';
    $SQL .= 'Select ';
    $SQL .= '   HRK_KEY ,';
    $SQL .= '   HRK_ANR_ID ,';
    $SQL .= '   HRK_TITEL ,';
    $SQL .= '   HRK_VORNAME ,';
    $SQL .= '   HRK_NACHNAME ,';
    $SQL .= '   HRK_GEBURTSDATUM ,';
    $SQL .= '   HRK_USER ,';
    $SQL .= '   HRK_USERDAT ,';
    $SQL .= '   HRK_STRASSEHNR ,';
    $SQL .= '   HRK_PLZ ,';
    $SQL .= '   HRK_ORT ,';
    $SQL .= '   HRK_LAN_NR ,';
    $SQL .= '   HRK_EMAIL ,';
    $SQL .= '   HRK_TELEFON ,';
    $SQL .= '   HRK_MOBIL,';
    
    $SQL .= '   row_number() OVER ( ';
    $SQL .= '   order by ' . ($Param['ORDER'] != ''?$Param['ORDER']:1) . ') as ZEILENNR';
    $SQL .= ' from (';

    $SQL .= 'Select * from (';

    $SQL .= 'SELECT';
    $SQL .= '   a.HRK_KEY ,';
    $SQL .= '   HRK_ANR_ID ,';
    $SQL .= '   HRK_TITEL ,';
    $SQL .= '   HRK_VORNAME, (select listagg(BWS_DATENSATZAKTIV,\';\') within group(order by 1) from V_BEWERBERVERWALTUNG b where b.hrk_key = a.hrk_key) as Aktiv ,';
    $SQL .= '   HRK_NACHNAME , (select listagg(BEW_GEBIET,\';\') within group(order by 1) from V_BEWERBERVERWALTUNG c where c.hrk_key = a.hrk_key) as BEW_GEBIET ,';
    $SQL .= '   HRK_GEBURTSDATUM , (select listagg(BEW_REGION,\';\') within group(order by 1) from V_BEWERBERVERWALTUNG d where d.hrk_key = a.hrk_key) as BEW_REGION ,';
    $SQL .= ' nvl((select listagg(BEW_INTERNER_BEWERBER,\';\') within group(order by 1) from V_BEWERBERVERWALTUNG e where e.hrk_key = a.hrk_key),0) as BEW_INTERNER_BEWERBER ,';
    $SQL .= ' (select listagg(BEW_VORSTELLUNGSDATUM,\';\') within group(order by 1) from V_BEWERBERVERWALTUNG f where f.hrk_key = a.hrk_key) as BEW_VORSTELLUNGSDATUM ,';
    $SQL .= ' (select listagg(BEW_VORSTELLUNGSGESPRAECH,\';\') within group(order by 1) from V_BEWERBERVERWALTUNG g where g.hrk_key = a.hrk_key) as BEW_VORSTELLUNGSGESPRAECH ,';
    $SQL .= '   HRK_USER ,';
    $SQL .= '   HRK_USERDAT ,';
    $SQL .= '   HRK_STRASSEHNR ,';
    $SQL .= '   HRK_PLZ ,';
    $SQL .= '   HRK_ORT ,';
    $SQL .= '   HRK_LAN_NR ,';
    $SQL .= '   HRK_EMAIL ,';
    $SQL .= '   HRK_TELEFON ,';
    $SQL .= '   HRK_MOBIL , ';
    $SQL .= ' b.HRF_KEY ';
    if ((isset($Param['BEW_TAETIGKEITEN']) and $Param['BEW_TAETIGKEITEN'] != '')) {
        $SQL .= ' ,taet.bif_wert as taetigkeit ';
    }

    if(isset($Param['BEW_FILIALE']) and $Param['BEW_FILIALE']!=''){
        $SQL .= ' ,(select bif_wert as bew_filiale from bewerberinfos ';
        $SQL .= ' where bif_bwh_key = b.bwh_key  ';
        $SQL .= ' and';
        //Firmierung einen Infotypen zuordnen (Abteilung o. Filiale)
        $SQL .= ' bif_bit_id    = case when b.hrf_key = 1 then 1';
        $SQL .= '					   when b.hrf_key = 2 then 6';
        $SQL .= '					   when b.hrf_key = 3 then 7';
        $SQL .= '					   when b.hrf_key = 4 then 38';
        $SQL .= '					   when b.hrf_key = 5 then 39';
        $SQL .= '					   when b.hrf_key = 6 then 40';
        $SQL .= '                 else 0 end ) as BEW_FILIALE';
    }

    $SQL .= ' FROM';
    $SQL .= '   HRKOPF a';
    $SQL .= ' left JOIN V_BEWERBERVERWALTUNG b';
    $SQL .= ' ON';
    $SQL .= '   a.HRK_KEY = B.HRK_KEY';
    if ((isset($Param['BEW_TAETIGKEITEN']) and $Param['BEW_TAETIGKEITEN'] != '')) {
        $SQL .= ' left join bewerberinfos taet ';
        $SQL .= ' on taet.bif_bit_id = 3 ';
        $SQL .= ' and taet.bif_bwh_key =  b.bwh_key ' ;
    }


    $SQL .= ' GROUP BY';
    $SQL .= '   a.HRK_KEY,';
    $SQL .= '   HRK_ANR_ID,  ';
    $SQL .= '   HRK_TITEL,';
    $SQL .= '   HRK_VORNAME,';
    $SQL .= '   HRK_NACHNAME,';
    $SQL .= '   HRK_GEBURTSDATUM,';
    $SQL .= '   HRK_USER,';
    $SQL .= '   HRK_USERDAT,';
    $SQL .= '   HRK_STRASSEHNR,';
    $SQL .= '   HRK_PLZ,';
    $SQL .= '   HRK_ORT,';
    $SQL .= '   HRK_LAN_NR,';
    $SQL .= '   HRK_EMAIL,';
    $SQL .= '   HRK_TELEFON,';
    $SQL .= '   HRF_KEY, ';
    $SQL .= '   HRK_MOBIL';
    if ((isset($Param['BEW_TAETIGKEITEN']) and $Param['BEW_TAETIGKEITEN'] != '')) {
        $SQL .= '  ,taet.bif_wert';
    }
    $SQL .= ' ))';

    $Bedingung = '';
    if ((isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '') and !isset($_POST['cmdDSNeu_y'])) {

        $Bedingung .= _BedingungErstellen($Param);       // mit dem Rest

        if ($Bedingung != '') {
            $SQL .= ' WHERE ' . substr($Bedingung, 4);
        }
    }
    $MaxDS = 1;
    $ZeilenProSeite = 1;
    $Block = 1;
    // Zum Bl�ttern in den Daten
    if (isset($_REQUEST['Block'])) {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
    } else {
        $Block = $Param['BLOCK'];
    }

    if (!isset($_POST['cmdSpeichern_x']) and !isset($_POST['cmdLoeschenOK']) and !isset($_POST['cmdLoeschenAbbrechen']) and !isset($_POST['cmdDuplikat']) and (!isset($_GET['BWH_KEY']) and !isset($_GET['HRK_KEY']) or isset($_POST['cmdLoeschenOK'])) and !isset($_POST['cmdDSNeu_y'])) {
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
    }

    if ($AWIS_KEY1 != '') {

        if ($Bedingung != '') {
            $SQL .= ' AND HRK_KEY=' . $AWIS_KEY1;
        } else {
            $SQL .= ' WHERE HRK_KEY=' . $AWIS_KEY1;
        }
    } elseif ($AWIS_KEY1 === 0) {
        if ($Bedingung != '') {
            $SQL .= ' AND HRK_KEY=0';
        } else {
            $SQL .= ' WHERE HRK_KEY=0';
        }
    }
    $Form->DebugAusgabe(1, $SQL);
    $rsHRK = $DB->RecordSetOeffnen($SQL);
    // Spaltenbreiten f�r Listenansicht
    $FeldBreiten = array();
    $FeldBreiten['HRK_VORNAME'] = 120;
    $FeldBreiten['HRK_NACHNAME'] = 120;
    $FeldBreiten['HRK_GEBURTSDATUM'] = 120;
    $FeldBreiten['HRK_PLZ'] = 100;
    $FeldBreiten['HRK_ORT'] = 110;
    $FeldBreiten['Hist'] = 40;

    //GETs zusammenschneiden
    $GETs = '';

    if (isset($AWIS_KEY2) and $AWIS_KEY2 != '' and !isset($_POST['cmdDSNeu_y']))//Gerade angelegter Detaildatensatz
    {
        $GETs .= '&BWH_KEY=' . $AWIS_KEY2;
    }

    if (isset($AWIS_KEY1)) //Kopf Datensatz
    {
        $GETs .= '&HRK_KEY=0' . $AWIS_KEY1;
    }

    $Form->SchreibeHTMLCode('<form name="frmBEWDetails" action="./bewerberverwaltung_Main.php?cmdAktion=Details' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . $GETs . '#' . $ScrollPosition . ' "method=POST  enctype="multipart/form-data">');

    $Form->Formular_Start();

    //Falls eine Duplette beim Speichern gefunden wurde, Diese in einer Liste anzeigen
    if ($AWIS_KEY1 == '-1' and $Duplikat == true) {
        $SQLDuplikat = 'select HRK_KEY ,';
        $SQLDuplikat .= ' HRK_ANR_ID ,';
        $SQLDuplikat .= ' HRK_TITEL ,';
        $SQLDuplikat .= ' HRK_VORNAME ,';
        $SQLDuplikat .= ' HRK_NACHNAME ,';
        $SQLDuplikat .= ' HRK_GEBURTSDATUM ,';
        $SQLDuplikat .= ' HRK_USER ,';
        $SQLDuplikat .= ' HRK_USERDAT ,';
        $SQLDuplikat .= ' HRK_STRASSEHNR ,';
        $SQLDuplikat .= ' HRK_PLZ ,';
        $SQLDuplikat .= ' HRK_ORT ,';
        $SQLDuplikat .= ' HRK_LAN_NR ,';
        $SQLDuplikat .= ' HRK_EMAIL ,';
        $SQLDuplikat .= ' HRK_TELEFON ,';
        $SQLDuplikat .= ' HRK_MOBIL  from HRKOPF ';
        $SQLDuplikat .= ' where SOUNDEXD(HRK_VORNAME) = SOUNDEXD(\'' . $_POST['txtHRK_VORNAME'] . '\')';
        $SQLDuplikat .= ' and SOUNDEXD(HRK_NACHNAME) = SOUNDEXD(\'' . $_POST['txtHRK_NACHNAME'] . '\')';
        $SQLDuplikat .= ' and HRK_GEBURTSDATUM = \'' . $_POST['txtHRK_GEBURTSDATUM'] . '\'';

        $rsDuplikat = $DB->RecordSetOeffnen($SQLDuplikat);

        $Form->Trennzeile('L');
        $Form->Trennzeile('O');

        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_UEBERSCHRIFT_DUPLIKAT'] . ':', 800,
            'font-weight:bolder');

        $Form->ZeileStart();
        // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
        //$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
        // �berschrift der Listenansicht mit Sortierungslink: Filiale
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_NACHNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_NACHNAME'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_NACHNAME'], $FeldBreiten['HRK_NACHNAME'],
            '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_VORNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_VORNAME'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_VORNAME'], $FeldBreiten['HRK_VORNAME'], '',
            $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_GEBURTSDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_GEBURTSDATUM'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_GEBURTSDATUM'],
            $FeldBreiten['HRK_GEBURTSDATUM'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Nachname
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_PLZ'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_PLZ'], $FeldBreiten['HRK_PLZ'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Nachname
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_ORT' . ((isset($_GET['HRK_ORT']) AND ($_GET['Sort'] == 'HRK_ORT'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_ORT'], $FeldBreiten['HRK_ORT'], '', $Link);

        $Form->ZeileEnde();
        $DS = 0;    // f�r Hintergrundfarbumschaltung
        while (!$rsDuplikat->EOF()) {
            $Form->ZeileStart('', 'Zeile"ID="Zeile_' . $rsDuplikat->FeldInhalt('HRK_KEY'));
            $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&HRK_KEY=0' . $rsDuplikat->FeldInhalt('HRK_KEY') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $TTT = $rsDuplikat->FeldInhalt('HRK_NACHNAME');
            $Form->Erstelle_ListenFeld('HRK_NACHNAME', $rsDuplikat->FeldInhalt('HRK_NACHNAME'), 0,
                $FeldBreiten['HRK_NACHNAME'], false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_VORNAME');
            $Form->Erstelle_ListenFeld('HRK_VORNAME', $rsDuplikat->FeldInhalt('HRK_VORNAME'), 0,
                $FeldBreiten['HRK_VORNAME'], false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_GEBURTSDATUM');
            $Form->Erstelle_ListenFeld('HRK_GEBURTSDATUM',
                $Form->Format('D', $rsDuplikat->FeldInhalt('HRK_GEBURTSDATUM')), 0, $FeldBreiten['HRK_GEBURTSDATUM'],
                false, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_PLZ');
            $Form->Erstelle_ListenFeld('HRK_PLZ', $Form->Format('T', $rsDuplikat->FeldInhalt('HRK_PLZ')), 0,
                $FeldBreiten['HRK_PLZ'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_ORT');
            $Form->Erstelle_ListenFeld('HRK_ORT', $rsDuplikat->FeldInhalt('HRK_ORT'), 0, $FeldBreiten['HRK_ORT'], false,
                ($DS % 2), '', '', 'T', 'L', $TTT);

            $Form->ZeileEnde();
            $DS++;
            $rsDuplikat->DSWeiter();
        }

        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
        $Form->Trennzeile('O');
        $Form->Trennzeile('L');
    }

    if (($rsHRK->AnzahlDatensaetze() == 1) or $AWIS_KEY1 != '' ) //Detailansicht des Kopfdatensatzes
    {
        if ($Duplikat) {
            $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_UEBERSCHRIFT_EINGEGEBENEDATEN'] . ':', 800,
                'font-weight:bolder');
        }

        if ($AWIS_KEY1 == '-1') {
            // Infozeile zusammenbauen
            $Felder = array();
            //$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
            $Felder[] = array(
                'Style' => 'font-size:smaller;',
                'Inhalt' => '<a href="./bewerberverwaltung_Main.php?cmdAktion=Suche" accesskey=S title=' . $AWISSprachKonserven['BEW']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>'
            );
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['BEW']['ZurueckSuche']);
            $Form->InfoZeile($Felder, '');
        } else {
            // Infozeile zusammenbauen
            $Felder = array();
            $Felder[] = array(
                'Style' => 'font-size:smaller;',
                'Inhalt' => "<a href=./bewerberverwaltung_Main.php?cmdAktion=Details accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
            );
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsHRK->FeldInhalt('HRK_USER'));
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsHRK->FeldInhalt('HRK_USERDAT'));
            $Form->InfoZeile($Felder, '');
        }

        //Awis_KEY1 = Kopfdatensatz
        if ($AWIS_KEY1 == '') {
            $AWIS_KEY1 = $rsHRK->FeldInhalt('HRK_KEY');
        }
        $Form->Erstelle_HiddenFeld('HRK_KEY', $AWIS_KEY1);
        $AWISCursorPosition = 'txtHRK_ANR_ID';
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_ANR_ID'] . ':', 140);
        $SQLANR = 'Select ANR_ID, ANR_ANREDE from ANREDEN where anr_id in (1,2) ';
        $Form->Erstelle_SelectFeld('HRK_ANR_ID',
            (isset($_POST['txtHRK_ANR_ID']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_ANR_ID']:$rsHRK->FeldInhalt('HRK_ANR_ID'),
            150, $Aendern, $SQLANR, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '1');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_TITEL'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_TITEL', $Form->Format('T',
            (isset($_POST['txtHRK_TITEL']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_TITEL']:$rsHRK->FeldInhalt('HRK_TITEL')),
            10, 250, $Aendern, '', '', '', '', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_VORNAME'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_VORNAME', $Form->Format('T',
            (isset($_POST['txtHRK_VORNAME']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_VORNAME']:$rsHRK->FeldInhalt('HRK_VORNAME')),
            20, 250, $Aendern, '', '', '', '', 'L');

        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_NACHNAME'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_NACHNAME', $Form->Format('T',
            (isset($_POST['txtHRK_NACHNAME']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_NACHNAME']:$rsHRK->FeldInhalt('HRK_NACHNAME')),
            20, 250, $Aendern, '', '', '', '', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_GEBURTSDATUM'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_GEBURTSDATUM', $Form->Format('D',
            (isset($_POST['txtHRK_GEBURTSDATUM']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_GEBURTSDATUM']:$rsHRK->FeldInhalt('HRK_GEBURTSDATUM')),
            10, 250, $Aendern, '', '', '', 'D', 'L', '', '', '', 'onkeyup="setzePunkte(this)";');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_STRASSEHNR'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_STRASSEHNR', $Form->Format('T',
            (isset($_POST['txtHRK_STRASSEHNR']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_STRASSEHNR']:$rsHRK->FeldInhalt('HRK_STRASSEHNR')),
            30, 250, $Aendern, '', '', '', '', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_PLZ'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_PLZ', $Form->Format('T',
            (isset($_POST['txtHRK_PLZ']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_PLZ']:$rsHRK->FeldInhalt('HRK_PLZ')),
            10, 250, $Aendern, '', '', '', '', 'L');

        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_ORT'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_ORT', $Form->Format('T',
            (isset($_POST['txtHRK_ORT']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_ORT']:$rsHRK->FeldInhalt('HRK_ORT')),
            20, 250, $Aendern, '', '', '', '', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_LAN_NR'] . ':', 140);
        $SQLLaender = 'Select lan_nr, lan_land from LAENDER order by LAN_LAND';
        $Form->Erstelle_SelectFeld('HRK_LAN_NR', $Form->Format('T',
            (isset($_POST['txtHRK_LAN_NR']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_LAN_NR']:$rsHRK->FeldInhalt('HRK_LAN_NR')),
            150, $Aendern, $SQLLaender, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '276');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_EMAIL'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_EMAIL', $Form->Format('T',
            (isset($_POST['txtHRK_EMAIL']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_EMAIL']:$rsHRK->FeldInhalt('HRK_EMAIL')),
            30, 250, $Aendern, '', '', '', '', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_TELEFON'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_TELEFON', $Form->Format('T',
            (isset($_POST['txtHRK_TELEFON']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_TELEFON']:$rsHRK->FeldInhalt('HRK_TELEFON')),
            30, 250, $Aendern, '', '', '', '', 'L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_MOBIL'] . ':', 140);
        $Form->Erstelle_TextFeld('HRK_MOBIL', $Form->Format('T',
            (isset($_POST['txtHRK_MOBIL']) and !isset($_POST['cmdDSNeu_y']))?$_POST['txtHRK_MOBIL']:$rsHRK->FeldInhalt('HRK_MOBIL')),
            30, 250, $Aendern, '', '', '', '', 'L');
        $Form->ZeileEnde();

        if ($AWIS_KEY1 != '-1' and !isset($_POST['cmdDSNeu_y'])) {
            $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
            $Register = new awisRegister(39001);
            $AWISCursorPosition = '';
            $Register->ZeichneRegister($RegisterSeite);
        }

        if ($Duplikat) {
            $Form->Trennzeile('O');
            $Form->Schaltflaeche('submit', 'cmdDuplikat', '', '', 'Eingegebene Daten �bernehmen');
            $Form->Trennzeile('O');
        }
    } elseif ($rsHRK->AnzahlDatensaetze() == 0) {
        $Form->ZeileStart();
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
        $Form->ZeileEnde();
    } else {
        $Form->ZeileStart();
        // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
        //$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
        // �berschrift der Listenansicht mit Sortierungslink: Filiale
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_NACHNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_NACHNAME'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_NACHNAME'], $FeldBreiten['HRK_NACHNAME'],
            '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_VORNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_VORNAME'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_VORNAME'], $FeldBreiten['HRK_VORNAME'], '',
            $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_GEBURTSDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_GEBURTSDATUM'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_GEBURTSDATUM'],
            $FeldBreiten['HRK_GEBURTSDATUM'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Nachname
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'HRK_PLZ'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_PLZ'], $FeldBreiten['HRK_PLZ'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Nachname
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Sort=HRK_ORT' . ((isset($_GET['HRK_ORT']) AND ($_GET['Sort'] == 'HRK_ORT'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_ORT'], $FeldBreiten['HRK_ORT'], '', $Link);

        if (($Recht39001 & 8) == 8) {
            // �berschrift der Listenansicht: History
            //$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HRK']['HRK_HIST'], $FeldBreiten['Hist'], '', '');
        }

        $Form->ZeileEnde();
        $DS = 0;    // f�r Hintergrundfarbumschaltung
        while (!$rsHRK->EOF()) {
            $Form->ZeileStart('', 'ZeileHRK"ID="ZeileHRK_' . $rsHRK->FeldInhalt('HRK_KEY'));
            $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&HRK_KEY=0' . $rsHRK->FeldInhalt('HRK_KEY') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $TTT = $rsHRK->FeldInhalt('HRK_NACHNAME');
            $Form->Erstelle_ListenFeld('HRK_NACHNAME', $rsHRK->FeldInhalt('HRK_NACHNAME'), 0,
                $FeldBreiten['HRK_NACHNAME'], false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_VORNAME');
            $Form->Erstelle_ListenFeld('HRK_VORNAME', $rsHRK->FeldInhalt('HRK_VORNAME'), 0, $FeldBreiten['HRK_VORNAME'],
                false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_GEBURTSDATUM');
            $Form->Erstelle_ListenFeld('HRK_GEBURTSDATUM', $Form->Format('D', $rsHRK->FeldInhalt('HRK_GEBURTSDATUM')),
                0, $FeldBreiten['HRK_GEBURTSDATUM'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_PLZ');
            $Form->Erstelle_ListenFeld('HRK_PLZ', $Form->Format('T', $rsHRK->FeldInhalt('HRK_PLZ')), 0,
                $FeldBreiten['HRK_PLZ'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsHRK->FeldInhalt('HRK_ORT');
            $Form->Erstelle_ListenFeld('HRK_ORT', $rsHRK->FeldInhalt('HRK_ORT'), 0, $FeldBreiten['HRK_ORT'], false,
                ($DS % 2), '', '', 'T', 'L', $TTT);

            if (($Recht39001 & 8) == 8) {
                $SQL = ' Select * from BEWERBERHISTORY ';
                $SQL .= ' where BEH_HRK_KEY=' . $rsHRK->FeldInhalt('HRK_KEY');
                $SQL .= ' and BEH_AKTION = \'Aktivit�t +\'';

                $rsHist = $DB->RecordSetOeffnen($SQL);
                $Bild = '';

                if ($rsHist->AnzahlDatensaetze() > 0) {
                    $Bild = '/bilder/attach.png';
                    $Form->Erstelle_ListenBild('script', 'HRK_HIST',
                        'onclick="ladeDiv(' . $rsHRK->FeldInhalt('HRK_KEY') . ',\'HRK\');"', $Bild, '', ($DS % 2), '',
                        16, 18, $FeldBreiten['Hist'], 'C');
                } else {
                    $Form->Erstelle_ListenFeld('HRK_HIST', '', 0, $FeldBreiten['Hist'], false, ($DS % 2), '', '', 'T',
                        'C', '');
                }
            }

            $Form->ZeileEnde();
            $DS++;
            $rsHRK->DSWeiter();
        }

        $FeldBreiten['Aktion'] = 100;
        $FeldBreiten['Alt'] = 170;
        $FeldBreiten['Neu'] = 170;
        $FeldBreiten['User'] = 150;
        $FeldBreiten['Userdat'] = 150;
        if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false) {
            $Rand = 20;
        } else {
            $Rand = 10;
        }
        $Breite = $FeldBreiten['Aktion'] + $FeldBreiten['Alt'] + $FeldBreiten['Neu'] + $FeldBreiten['User'] + $FeldBreiten['Userdat'] + $Rand + 20;

        //DIV NOCH INS FRAMEWORK PACKEN!!!
        echo '<div id="Hist_HRK" style="display:none; position:absolute; border:5px outset #81bef7;border-radius:5px; z-index:1; background-color:#81bef7; top:100px; left:100px; width:' . $Breite . 'px;">';
        echo '</div>';
        $Link = './bewerberverwaltung_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    }

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/hrtools/index.php', '/bilder/cmd_zurueck.png',
        $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if ((($Recht39001 & 2) == 2) and $AWIS_KEY1 != '' and !$Duplikat and !$Speichernausblenden)// and (!$EingabeOK or $Gespeichert) and !$Speichernausblenden)//($rsKDI->AnzahlDatensaetze() == 1 or $AWIS_KEY1 == 0) )
    {

        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png',
            $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    if (($Recht39001 & 2) == 2 and $AWIS_KEY1 != '-1') //Ist genauso angefordert... Neubutton muss in jeder lage der Software m�glich sein.
    {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png',
            $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    if ((($Recht39001 & 16) == 16) and $AWIS_KEY1 != '' and $AWIS_KEY1 > 0) {
        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png',
            $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X', '', 27, 27, '', true);
    }

    $Form->ZeileStart('display:none');
    $Form->Schaltflaeche('submit', 'cmdFirmierungsChange', '', '/bilder/cmd_speichern.png',
        $AWISSprachKonserven['Wort']['lbl_speichern'], 'S', '', '', '', '', true);
    $Form->ZeileEnde();
    $Form->ZeileStart('display:none');
    $Form->Schaltflaeche('submit', 'cmdStatusChange', '', '/bilder/cmd_speichern.png',
        $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    $Form->ZeileEnde();

    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
    $AWISBenutzer->ParameterSchreiben('Formular_BEW', serialize($Param));
} catch (awisException $ex) {

    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {

        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{

    $Bedingung = '';

    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $AWISBenutzer;
    global $DB;

    $Form = new awisFormular();
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    if (isset($Param['HRK_NACHNAME']) AND $Param['HRK_NACHNAME'] != '') {
        $Bedingung .= 'AND upper(HRK_NACHNAME) ' . $DB->LikeOderIst($Param['HRK_NACHNAME'] . '%',
                awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['HRK_VORNAME']) AND $Param['HRK_VORNAME'] != '') {
        $Bedingung .= 'AND upper(HRK_VORNAME) ' . $DB->LikeOderIst($Param['HRK_VORNAME'] . '%',
                awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['HRK_ORT']) AND $Param['HRK_ORT'] != '') {
        $Bedingung .= 'AND upper(HRK_ORT) ' . $DB->LikeOderIst($Param['HRK_ORT'] . '%', awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['HRK_GEBURTSDATUM']) AND $Param['HRK_GEBURTSDATUM'] != '') {
        $Bedingung .= 'AND HRK_GEBURTSDATUM ' . $DB->LikeOderIst($Param['HRK_GEBURTSDATUM'] . '%');
    }

    if (isset($Param['BEW_STATUS']) AND $Param['BEW_STATUS'] != '' and $Param['BEW_STATUS'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND Aktiv ' . $DB->LikeOderIst('%' . $Param['BEW_STATUS'] . '%');
    }

    if (isset($Param['BEW_GEBIET']) AND $Param['BEW_GEBIET'] != '' and $Param['BEW_GEBIET'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND upper(BEW_GEBIET) ' . $DB->LikeOderIst('%' . $Param['BEW_GEBIET'] . '%',
                awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['BEW_REGION']) AND $Param['BEW_REGION'] != '' and $Param['BEW_REGION'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND upper(BEW_REGION) ' . $DB->LikeOderIst('%' . $Param['BEW_REGION'] . '%',
                awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['HRK_EMAIL']) AND $Param['HRK_EMAIL'] != '' and $Param['HRK_EMAIL'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND upper(HRK_EMAIL) ' . $DB->LikeOderIst('%' . $Param['HRK_EMAIL'] . '%',
                awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['BEW_INTERNER_BEWERBER']) AND $Param['BEW_INTERNER_BEWERBER'] != '' and $Param['BEW_INTERNER_BEWERBER'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND BEW_INTERNER_BEWERBER ' . $DB->LikeOderIst('%' .$Param['BEW_INTERNER_BEWERBER']. '%');
    }

    if (isset($Param['BEW_VORSTELLUNGSDATUM']) AND $Param['BEW_VORSTELLUNGSDATUM'] != '' and $Param['BEW_VORSTELLUNGSDATUM'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND BEW_VORSTELLUNGSDATUM ' . $DB->LikeOderIst('%' . $Param['BEW_VORSTELLUNGSDATUM' ]. '%');
    }

    if (isset($Param['BEW_VORSTELLUNGSGESPRAECH']) AND $Param['BEW_VORSTELLUNGSGESPRAECH'] != '' and $Param['BEW_VORSTELLUNGSGESPRAECH'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND BEW_VORSTELLUNGSGESPRAECH ' . $DB->LikeOderIst($Param['BEW_VORSTELLUNGSGESPRAECH']);
    }

    //Deckt Filiale und Abteilung ab
    if (isset($Param['BEW_FILIALE']) AND $Param['BEW_FILIALE'] !== '' and $Param['BEW_FILIALE'] !== $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] ) {
        $Bedingung .= 'AND BEW_FILIALE ' . $DB->LikeOderIst($Param['BEW_FILIALE']);
    }


    if (isset($Param['BEW_FIRMIERUNG']) AND $Param['BEW_FIRMIERUNG'] != '' AND $Param['BEW_FIRMIERUNG'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND HRF_KEY ' . $DB->LikeOderIst( $Param['BEW_FIRMIERUNG'] , awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['BEW_TAETIGKEITEN']) AND $Param['BEW_TAETIGKEITEN'] != '' AND $Param['BEW_TAETIGKEITEN'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) {
        $Bedingung .= 'AND TAETIGKEIT ' . $DB->LikeOderIst( $Param['BEW_TAETIGKEITEN'] , awisDatenbank::AWIS_LIKE_UPPER);

    }
    return $Bedingung;
}

?>