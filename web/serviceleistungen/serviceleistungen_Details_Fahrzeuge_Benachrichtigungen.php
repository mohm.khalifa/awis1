<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('STB','%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Liste','SAB_EMPFAENGER');
	$TextKonserven[]=array('SAB','SAB_BEZEICHNUNG');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');



	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$Form = new awisFormular();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


	$Recht19000 = $AWISBenutzer->HatDasRecht(19000);			// Recht f�r die Teilnehmer
	if($Recht19000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$Bedingung = ' AND STB_SVT_KEY = '.$AWIS_KEY2;
	
	$SQL = 'SELECT SVCTEILNBENACHRICHTUNGEN.*, SAB_BEZEICHNUNG ';
	$SQL .= ' FROM SVCTEILNBENACHRICHTUNGEN';
	$SQL .= ' INNER JOIN SVCARTIKELBENACHRICHTIGUNGEN ON STB_SAB_KEY = SAB_KEY';
	
		
	if(isset($_GET['STB_KEY']))
	{
		$Bedingung .= ' AND STB_KEY = 0'.$DB->FeldInhaltFormat('Z',$_GET['STB_KEY']);
	}
	if($AWIS_KEY2!=0)
	{
	//	$Bedingung .= ' AND STB_KEY = 0'.$AWIS_KEY2;
	}

	$SQL .= ($Bedingung!=''?' WHERE '.substr($Bedingung,4):'');

	if(isset($_GET['STBSort']))
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['STBSort']);
	}
	else
	{
		$SQL .= ' ORDER BY STB_DATUM';
	}

	//awis_Debug(1,$SQL,$_GET);
	$rsSTB = $DB->RecordSetOeffnen($SQL);

	if(!isset($_GET['STB_KEY']))					// Liste anzeigen
	{
		$Form->ZeileStart();

		if(($Recht19000&4))
		{
			$Icons = array();
			$Icons[] = array('new','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Benachrichtigungen&SVT_KEY='.$AWIS_KEY2.'&STB_KEY=0');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Benachrichtigungen';
		$Link .= '&SVT_KEY='.$AWIS_KEY2.'&STBSort=STB_DATUM'.((isset($_GET['STBSort']) AND ($_GET['STBSort']=='STB_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['STB']['STB_DATUM'],100,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Benachrichtigungen';
		$Link .= '&SVT_KEY='.$AWIS_KEY2.'&STBSort=STB_EMPFAENGER'.((isset($_GET['STBSort']) AND ($_GET['STBSort']=='STB_EMPFAENGER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['STB']['STB_EMPFAENGER'],250,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Teilnehmer&Unterseite=Benachrichtigungen';
		$Link .= '&SVT_KEY='.$AWIS_KEY2.'&STBSort=SAB_BEZEICHNUNG'.((isset($_GET['STBSort']) AND ($_GET['STBSort']=='SAB_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAB']['SAB_BEZEICHNUNG'],350,'',$Link);
		
		
		$Form->ZeileEnde();

		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SAB_EMPFAENGER']);
		foreach($ListenDaten AS $Eintrag)
		{
			$Daten = explode('~',$Eintrag);
			$Empfaenger[$Daten[0]]=$Daten[1];
		}
		
		$DS=0;
		while(!$rsSTB->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(($Recht19000&2))	// �ndernrecht
			{
				$Icons[] = array('edit','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Benachrichtigungen&SVT_KEY='.$AWIS_KEY2.'&STB_KEY='.$rsSTB->FeldInhalt('STB_KEY'));
			}
			if(($Recht19000&4))	// L�schenrecht
			{
				$Icons[] = array('delete','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Benachrichtigungen&Del='.$rsSTB->FeldInhalt('STB_KEY').'&Key='.$AWIS_KEY1.'&SVT_KEY='.$AWIS_KEY2.'&STB_KEY='.$rsSTB->FeldInhalt('STB_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));


			$Form->Erstelle_ListenFeld('*STB_DATUM',$rsSTB->FeldInhalt('STB_DATUM'),10,100,false,($DS%2),'','','T','C');
			$Form->Erstelle_ListenFeld('*STB_EMPFAENGER',$Empfaenger[$rsSTB->FeldInhalt('STB_EMPFAENGER')],10,250,false,($DS%2),'','','T','L',$rsSTB->FeldInhalt('FIL_BEZ'));
			$Form->Erstelle_ListenFeld('*SAB_BEZEICHNUNG',$rsSTB->FeldInhalt('SAB_BEZEICHNUNG'),10,350,false,($DS%2),'','','T','L');
				
			$Form->ZeileEnde();
			$DS++;

			$rsSTB->DSWeiter();
		}
		$Form->Formular_Ende();
	}
	else 		// Einer oder keiner
	{
		$Form->Formular_Start();

		echo '<input name=txtSTB_KEY type=hidden value=0'.($rsSTB->FeldInhalt('STB_KEY')).'>';
		echo '<input name=txtSTB_SVT_KEY type=hidden value=0'.$AWIS_KEY2.'>';
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Benachrichtigungen&SVT_KEY=".$AWIS_KEY2."&STBListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsSTB->FeldInhalt('STB_USER')));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsSTB->FeldInhalt('STB_USERDAT')));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();

		$EditModus = ($rsSTB->FeldInhalt('STB_USER')!='WWS'?($Recht19000&6):false);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['STB']['STB_DATUM'].':',200);
		$Form->Erstelle_TextFeld('STB_DATUM',$rsSTB->FeldInhalt('STB_DATUM'),10,200,$EditModus,'','','','D');
		$AWISCursorPosition=($EditModus?'txtSTB_DATUM':$AWISCursorPosition);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['STB']['STB_SAB_KEY'].':',200,'');
		$SQL = 'select SAB_KEY, SAB_BEZEICHNUNG';
		$SQL .= ' FROM SVCARTIKELBENACHRICHTIGUNGEN';
		$SQL .= ' WHERE SAB_SVA_KEY = (SELECT SVB_SVA_KEY FROM SVCVereinbarungen INNER JOIN SVCTeilnehmer ON SVB_KEY = SVT_SVB_KEY AND SVT_KEY = 0'.$AWIS_KEY2.')'; 
		$SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('STB_SAB_KEY',$rsSTB->FeldInhalt('STB_SAB_KEY'),'200:190',$EditModus,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['STB']['STB_EMPFAENGER'].':',200);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SAB_EMPFAENGER']);
		$Form->Erstelle_SelectFeld('STB_EMPFAENGER',$rsSTB->FeldInhalt('STB_EMPFAENGER'),"200:190",$EditModus,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['STB']['STB_SMSTEXT'].':',200);
		$Form->Erstelle_TextFeld('STB_SMSTEXT',$rsSTB->FeldInhalt('STB_SMSTEXT'),80,100,$EditModus,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['STB']['STB_MAILBETREFF'].':',200);
		$Form->Erstelle_TextFeld('STB_MAILBETREFF',$rsSTB->FeldInhalt('STB_MAILBETREFF'),80,100,$EditModus,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['STB']['STB_MAILTEXT'].':',200);
		$Form->Erstelle_TextArea('STB_MAILTEXT',$rsSTB->FeldInhalt('STB_MAILTEXT'),800,99,10,$EditModus);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['STB']['STB_BRIEFTEXT'].':',200);
		$Form->Erstelle_TextArea('STB_BRIEFTEXT',$rsSTB->FeldInhalt('STB_BRIEFTEXT'),800,99,10,$EditModus);
		$Form->ZeileEnde();

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202190721");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202190720");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>