<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once 'awisMailer.inc';

/**
 * Class awisArtikelSplit
 */
class awisArtikelSplit
{

    /**
     * DebugLevel
     *
     * @var int
     */
    private $_DebugLevel=0;

    /**
     * @var String
     */
    protected $_DebugText = "";

    /**
     * Aktueller DebugText
     *
     * @var String
     */
    protected $_AktDebugText = "";

    /**
     * Datenbankverbindung
     *
     * @var awisDatenbank
     */
    private $_DB = '';

    /**
     * Aktueller Benutzer
     *
     * @var awisBenutzer
     */
    private $_Benutzer;

    /**
     * @var awisWerkzeuge
     */
    protected $_Werkzeuge;

    /**
     * @var awisMailer
     */
    protected $_Mailer;

    /**
     * Empf�nger f�r Mails
     *
     * @var array
     */
    private $_MailEmpfaenger = '';

    /**
     * RecordSet der Quelle
     *
     * @var awisRecordset
     */
    private $_rsARTQuelle = '';

    /**
     * RecordSet des Originalen Artikels
     *
     * @var awisRecordset
     */
    private $_rsARTOriginal = '';

    /**
     * Flag ob nur OE-Nummern gemacht werden duerfen (0=nein/1=ja)
     *
     * @var integer
     */
    private $_onlyOENummer = 0;

    /**
     * True falls bei Datensatz Error aufgetreten ist
     *
     * @var int
     */
    private $_DSError = false;

    public function __construct($Benutzer, $DebugLevel, $OnlyOENummer)
    {
        $this->_DebugLevel = $DebugLevel;

        //Datenbank �ffnen
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Werkzeuge = new awisWerkzeuge();
        $this->_Benutzer = awisBenutzer::Init($Benutzer);
        $this->_MailEmpfaenger = explode(';',$this->_Benutzer->ParameterLesen('ARTIKELSPLIT_MAIL_EMPFAENGER'));
        $this->_Mailer = new awisMailer($this->_DB,$this->_Benutzer);
        $this->_onlyOENummer = $OnlyOENummer;
    }

    /**
     * Normaler Job-Durchlauf
     */
    public function Start() {
        $this->holeImportDaten();
        $this->verarbeiteArtikel();
    }

    /**
     * Holt sich alle Daten aus der Artikelsplit aus dem AWISIMPORT-Schema
     * und speichert das RecordSet ab
     */
    private function holeImportDaten() {
        $SQL = " SELECT *";
        $SQL.= " FROM awisimport.artikelsplit";
        $SQL.= " WHERE verarbeitet = " . $this->_DB->WertSetzen('ARS', 'Z', 0);
        $SQL.= " ORDER BY LIEFART_NR ASC";

        $this->_rsARTQuelle = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ARS'));
    }

    /**
     * KEY
     * ATU_NR
     * SAP_ARTIKELBEZEICHNUNG
     * LIEFART_NR
     * ATU_NR_NEU
     * VERARBEITET (0 = offen / 1 = verarbeitet / -1 = ERROR)
     */
    private function verarbeiteArtikel() { // $this->aktDebugAusgabe('');       leere DebugAusgabe zum Kopieren
        while(!$this->_rsARTQuelle->EOF()) {
            $this->_DSError = false; //Zuruecksetzen vom Error-Flag
            $this->leereAktDebugAusgabe(); //Leere DebugAusgabe

            $this->aktDebugAusgabe('Verarbeite jetzt ATU_NR (alt) '.$this->_rsARTQuelle->FeldInhalt('ATU_NR').' bzw. (neu) '.$this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));
            $this->aktDebugAusgabe('Datensatz hat LiefArtNr '.$this->_rsARTQuelle->FeldInhalt('LIEFART_NR'));

            if($this->_rsARTQuelle->FeldInhalt('ATU_NR') != $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU')) {
                // die bisherige ATU-Nr. entspricht nicht der neuen -> d.h. alle Punkte durchlaufen

                //***********************************************************
                //   1. Originalartikel holen zum darauf referenzieren
                //***********************************************************
                $this->ladeOriginalArtikel($this->_rsARTQuelle->FeldInhalt('ATU_NR'));

                //***********************************************************
                //   2. SK Haken loeschen -> Teileinfosinfos-DS loeschen
                //***********************************************************
                if($this->_onlyOENummer == 0) {
                    if (!$this->_DSError) {
                        $TEIKEYS = $this->holeTeiKey($this->_rsARTQuelle->FeldInhalt('ATU_NR'), $this->_rsARTQuelle->FeldInhalt('LIEFART_NR'));
                    }

                    if (!$this->_DSError) {
                        foreach ($TEIKEYS as $TEI_KEY) {
                            $this->loescheSKHaken($TEI_KEY);
                        }
                    }

                    //***********************************************************
                    //   3. Artikel im Artikelstamm anlegen
                    //***********************************************************
                    if (!$this->_DSError) {
                        $SQL = "SELECT ast_atunr FROM artikelstamm";
                        $SQL .= " WHERE ast_atunr = " . $this->_DB->WertSetzen('ART', 'T', $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));

                        $rsAST = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ART'));

                        $this->aktDebugAusgabe('Suche neue ATU_NR im Artikelstamm ' . $this->_DB->LetzterSQL());
                        if ($rsAST->AnzahlDatensaetze() == 0) {
                            $this->erstelleATUNr(
                                $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'),
                                $this->_rsARTQuelle->FeldInhalt('SAP_ARTIKELBEZEICHNUNG')
                            );
                        } else {
                            $this->aktDebugAusgabe('Artikel bereits vorhanden -> Ueberspringe Anlage');
                        }
                    }

                    //***********************************************************
                    //   4. Lieferantenartikel erstellen ATUNR + LiefNr 7777
                    //***********************************************************
                    if (!$this->_DSError) {
                        $SQL = "SELECT lar_lartnr FROM lieferantenartikel";
                        $SQL .= " WHERE lar_lartnr = " . $this->_DB->WertSetzen('LAR', 'T', $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));

                        $rsLAR = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('LAR'));

                        $this->aktDebugAusgabe('Suche neue ATU_NR bei Lieferantenartikel ' . $this->_DB->LetzterSQL());
                        if ($rsLAR->AnzahlDatensaetze() == 0) {
                            $this->erstelleLiefArtikel($this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));
                        } else {
                            $this->aktDebugAusgabe('Lieferantenartikel bereits vorhanden -> Ueberspringe Anlage');
                        }
                    }

                    //4.2 Teileinfo zu Lieferantenartikel erstellen
                    if (!$this->_DSError) {
                        $SQL = "SELECT tei_key FROM teileinfos";
                        $SQL .= " WHERE tei_such1 = " . $this->_DB->WertSetzen('TEI', 'T', $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));
                        $SQL .= " AND tei_such2 = " . $this->_DB->WertSetzen('TEI', 'T', $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));
                        $SQL .= " AND tei_ity_id1 = " . $this->_DB->WertSetzen('TEI', 'T', 'AST');
                        $SQL .= " AND tei_ity_id2 = " . $this->_DB->WertSetzen('TEI', 'T', 'LAR');

                        $rsTEI = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TEI'));

                        $this->aktDebugAusgabe('Suche neue ATU_NR bei Teileinfos ' . $this->_DB->LetzterSQL());
                        if ($rsTEI->AnzahlDatensaetze() == 0) {
                            $this->erstelleTeileInfosSatz($this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'), $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));
                        } else {
                            $this->aktDebugAusgabe('Teileinfo bereits vorhanden -> Ueberspringe Anlage');
                        }
                    }

                    //4.3 Teileinfoinfo dazu erstellen (SK-Haken an)
                    if (!$this->_DSError) {
                        $TEIKEYS = $this->holeTeiKey($this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'), $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));

                        foreach ($TEIKEYS as $TEI_KEY) {
                            $SQL = "SELECT tii_key FROM teileinfosinfos";
                            $SQL .= " WHERE TII_TEI_KEY = " . $this->_DB->WertSetzen('TII', 'Z', $TEI_KEY);
                            $SQL .= " AND TII_ITY_KEY = " . $this->_DB->WertSetzen('TII', 'T', 318);

                            $rsTII = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TII'));

                            $this->aktDebugAusgabe('Suche SK-Haken bei Teileinfosinfos ' . $this->_DB->LetzterSQL());
                            if ($rsTII->AnzahlDatensaetze() == 0) {
                                $this->setzeSKHaken($TEI_KEY);
                            } else {
                                $this->aktDebugAusgabe('SK-Haken fuer TEI_KEY ' . $TEI_KEY . ' bereits vorhanden -> Ueberspringe Anlage');
                            }
                        }
                    }

                    //***********************************************************
                    //   5. Lieferantenartikel mit neuen ATU-Nr. verknuepfen -> Teileinfos
                    //***********************************************************
                    // Verknuepfen hei�t neuen TeileInfos-Satz erstellen
                    if (!$this->_DSError) {
                        $SQL = "SELECT tei_key FROM teileinfos";
                        $SQL .= " WHERE tei_such1 = " . $this->_DB->WertSetzen('TEI', 'T', $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));
                        $SQL .= " AND tei_such2 = " . $this->_DB->WertSetzen('TEI', 'T', $this->generiereSuchBegriff($this->_rsARTQuelle->FeldInhalt('LIEFART_NR')));
                        $SQL .= " AND tei_ity_id1 = " . $this->_DB->WertSetzen('TEI', 'T', 'AST');
                        $SQL .= " AND tei_ity_id2 = " . $this->_DB->WertSetzen('TEI', 'T', 'LAR');

                        $rsTEI = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TEI'));

                        $this->aktDebugAusgabe('Suche neue ATU_NR bei Teileinfos ' . $this->_DB->LetzterSQL());
                        if ($rsTEI->AnzahlDatensaetze() == 0) {
                            $this->erstelleTeileInfosSatz($this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'), $this->_rsARTQuelle->FeldInhalt('LIEFART_NR'));
                        } else {
                            $this->aktDebugAusgabe('Teileinfo bereits vorhanden -> Ueberspringe Anlage');
                        }
                    }

                    //5.2 Teileinfoinfo dazu erstellen
                    if (!$this->_DSError) {
                        $TEIKEYS = $this->holeTeiKey($this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'), $this->_rsARTQuelle->FeldInhalt('LIEFART_NR'));

                        foreach ($TEIKEYS as $TEI_KEY) {
                            $SQL = "SELECT tii_key FROM teileinfosinfos";
                            $SQL .= " WHERE TII_TEI_KEY = " . $this->_DB->WertSetzen('TII', 'Z', $TEI_KEY);
                            $SQL .= " AND TII_ITY_KEY = " . $this->_DB->WertSetzen('TII', 'T', 318);

                            $rsTII = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TII'));

                            $this->aktDebugAusgabe('Suche SK-Haken bei Teileinfosinfos ' . $this->_DB->LetzterSQL());
                            if ($rsTII->AnzahlDatensaetze() == 0) {
                                $this->setzeSKHaken($TEI_KEY);
                            } else {
                                $this->aktDebugAusgabe('SK-Haken fuer TEI_KEY ' . $TEI_KEY . ' bereits vorhanden -> Ueberspringe Anlage');
                            }
                        }
                    }
                }
                //***********************************************************
                //6 alle OE-Nummern mit dem neuen Artikel verknuepfen
                //***********************************************************
                if(!$this->_DSError) {
                    $TEIKEYS = $this->holeTeiKeyOENummer($this->_rsARTQuelle->FeldInhalt('ATU_NR'));

                    foreach ($TEIKEYS as $TEI_KEY) {
                        $this->erstelleTeileInfosSatzOE($this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'), $TEI_KEY);
                    }

                }

                //RecordSet fuer Original Artikel leeren
                $this->_rsARTOriginal = '';
            } else {
                // nichts machen da LiefArt auf der ATU-Nummer bleibt
                $this->aktDebugAusgabe('Liefartenartikelnummer ' . $this->_rsARTQuelle->FeldInhalt('LIEFART_NR') . ' bleibt auf ATUNR ' . $this->_rsARTQuelle->FeldInhalt('ATU_NR_NEU'));
                // @TODO: hier m�ssten wir Artikelbezeichnung evtl. noch im Artikelstamm Updaten -> Laut letzter Mail vom Schmidberger nicht, oder?
            }

            $this->setVerarbeitetFlag($this->_rsARTQuelle->FeldInhalt('KEY'));

            $this->debugAusgabe($this->_AktDebugText,$this->_DebugLevel);

            //Bei Error -> Mail schreiben
            if($this->_DSError) {
                $this->schreibeERRORMail();
            }

            $this->_rsARTQuelle->DSWeiter();
        }
    }

    /**
     * Suche den Original-Artikel
     *
     * @param $ATU_NR
     */
    private function ladeOriginalArtikel($ATU_NR) {
        try {
            $this->aktDebugAusgabe('Hole mir originalen Artikel');
            $SQL = "SELECT AST_WUG_KEY FROM ";
            $SQL .= " ARTIKELSTAMM ";
            $SQL .= " WHERE ";
            $SQL .= " AST_ATUNR = ".$this->_DB->WertSetzen('AST', 'T', $ATU_NR);

            $this->_rsARTOriginal = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('AST'));
        } catch (Exception $e) {
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler beim Holen des originalen Artikels');
            $this->aktDebugAusgabe($e->getMessage());
        }
    }

    /**
     * Erstellt einen Eintrag in der TeileInfos-Tabelle und stellt damit eine
     * Verknuepfung von der Artikelnummer zur der Lieferantenartikelnummer her
     *
     * @param string $atunr_neu Die neue ATU-Nr.
     * @param string $liefArtNr Die Lieferantenartikelnummer
     */
    private function erstelleTeileInfosSatz($atu_nr, $liefArtNr) {
        try{
            $this->aktDebugAusgabe('Erstelle jetzt TeileInfos-Datensatz mit ATU_NR ( '.$atu_nr.' ) und LiefArtNr ( '.$liefArtNr.' )');

            $suchLiefArtNr = $this->generiereSuchBegriff($liefArtNr);

            // hole mir AST_KEY
            $SQL = " SELECT ast_key";
            $SQL.= " FROM artikelstamm";
            $SQL.= " WHERE ast_atunr = " . $this->_DB->WertSetzen('AST', 'T', $atu_nr);

            $rsAST = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('AST'));

            $this->aktDebugAusgabe('erstellteTeileInfoSatz -> select AST_KEY: ' . $this->_DB->LetzterSQL());

            if($rsAST->AnzahlDatensaetze() == 0) {
                throw new Exception('erstelleTeileInfosSatz - kein ast_key gefunden', '2104191452');
            }

            // hole mir LAR_KEY
            $SQL = " SELECT lar_key";
            $SQL.= " FROM lieferantenartikel";
            $SQL.= " WHERE lar_such_lartnr = " . $this->_DB->WertSetzen('LAR', 'T', $suchLiefArtNr);
            $SQL.= " OR lar_lartnr = " . $this->_DB->WertSetzen('LAR', 'T', $liefArtNr);

            $rsLAR = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('LAR'));

            $this->aktDebugAusgabe('erstellteTeileInfoSatz -> select LAR_KEY: ' . $this->_DB->LetzterSQL());

            if($rsLAR->AnzahlDatensaetze() == 0) {
                throw new Exception('erstelleTeileInfosSatz - kein lar_key gefunden', '2104191457');
            }

            $SQL = " INSERT INTO teileinfos";
            $SQL.= " (";
            $SQL.= "  TEI_ITY_ID1";
            $SQL.= " ,TEI_KEY1";
            $SQL.= " ,TEI_SUCH1";
            $SQL.= " ,TEI_WERT1";
            $SQL.= " ,TEI_ITY_ID2";
            $SQL.= " ,TEI_KEY2";
            $SQL.= " ,TEI_SUCH2";
            $SQL.= " ,TEI_WERT2";
            $SQL.= " ,TEI_USER";
            $SQL.= " ,TEI_USERDAT";
            $SQL.= " )";
            $SQL.= " VALUES";
            $SQL.= " (";
            $SQL.= "  " . $this->_DB->WertSetzen('TEI', 'T', 'AST');
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'Z', $rsAST->FeldInhalt('AST_KEY'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $atu_nr);
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $atu_nr);
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', 'LAR');
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $rsLAR->FeldInhalt('LAR_KEY'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $suchLiefArtNr);
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $liefArtNr);
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', 'AWIS');
            $SQL.= " ,SYSDATE";
            $SQL.= " )";

            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('TEI'));

            $this->aktDebugAusgabe('erstellteTeileInfoSatz -> INSERT teileInfo: ' . $this->_DB->LetzterSQL());

        } catch (Exception $e) {
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler bei Erstellung von Teileinfos-Datensatz');
            $this->aktDebugAusgabe($e->getMessage());
        }
    }

    /**
     * Hole alle TEIKEYS fuer eine ATU-Nr/eine LiefArtNr/eine Kombi daraus
     *
     * @param string $ATU_NR
     * @param string $LiefArtNr
     * @return array
     */
    private function holeTeiKey($ATU_NR = '', $LiefArtNr = '') {
        try {
            $this->aktDebugAusgabe('Hole mir Tei-Keys mit ATU_NR ( '.$ATU_NR.' ) und/oder LiefArtNr ( '.$LiefArtNr.' )');
            $TEIKEYS = array();
            if($ATU_NR != '' or $LiefArtNr != '') {

                $SQL = "SELECT TEI_KEY FROM TEILEINFOS ";
                $SQL .= " WHERE ";
                $SQL .= " TEI_ITY_ID1 = ".$this->_DB->WertSetzen('TEI', 'T', "AST");
                $SQL .= " AND TEI_ITY_ID2 = ".$this->_DB->WertSetzen('TEI', 'T', "LAR");
                if($ATU_NR != '') {
                    $SQL .= " AND TEI_SUCH1 = ".$this->_DB->WertSetzen('TEI', 'T', $ATU_NR);
                }
                if($LiefArtNr != '') {
                    $SQL .= " AND TEI_SUCH2 = ".$this->_DB->WertSetzen('TEI', 'T', $this->generiereSuchBegriff($LiefArtNr));
                }
                $rsTEI = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TEI'));

                while(!$rsTEI->EOF()){
                    $TEIKEYS[] = $rsTEI->FeldInhalt('TEI_KEY');
                    $rsTEI->DSWeiter();
                }
            }
            return $TEIKEYS;
        } catch (Exception $e) {
            $this->_DSError = true;
            $this->aktDebugAusgabe('Hatte Fehler bei Tei-Key holen');
            $this->aktDebugAusgabe($e->getMessage());
        }
    }

    /**
     * Hole alle TEIKEYS fuer eine ATU-Nr und deren OE-Nummern
     *
     * @param string $ATU_NR
     * @return array
     */
    private function holeTeiKeyOENummer($ATU_NR = '') {
        try {
            $this->aktDebugAusgabe('Hole mir Tei-Keys mit ATU_NR ( '.$ATU_NR.' ) und deren OE-Nummern )');
            $TEIKEYS = array();
            if($ATU_NR != '') {

                $SQL = "SELECT TEI_KEY FROM TEILEINFOS ";
                $SQL .= " WHERE ";
                $SQL .= " TEI_ITY_ID1 = ".$this->_DB->WertSetzen('TEI', 'T', "AST");
                $SQL .= " AND TEI_ITY_ID2 = ".$this->_DB->WertSetzen('TEI', 'T', "OEN");
                if($ATU_NR != '') {
                    $SQL .= " AND TEI_SUCH1 = ".$this->_DB->WertSetzen('TEI', 'T', $ATU_NR);
                }
                $rsTEI = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TEI'));

                while(!$rsTEI->EOF()){
                    $TEIKEYS[] = $rsTEI->FeldInhalt('TEI_KEY');
                    $rsTEI->DSWeiter();
                }
            }
            return $TEIKEYS;
        } catch (Exception $e) {
            $this->_DSError = true;
            $this->aktDebugAusgabe('Hatte Fehler bei Tei-Key (OE) holen');
            $this->aktDebugAusgabe($e->getMessage());
        }
    }

    /**
     * Erstellt eine neue ATU-Nummer im Artikelstamm inkl. Eintraege in den ArtikelstammInfos
     *
     * @param string $ATU_NR ATU-Artikelnummer
     * @param string $ArtBez    Artikelbeschreibung
     */
    private function erstelleATUNr($ATU_NR,$ArtBez) {
        try {
            $this->aktDebugAusgabe('Erstelle jetzt ATUNR ( '.$ATU_NR.' ) - '.$ArtBez);
            $this->_DB->TransaktionBegin();

            $SQL = " INSERT INTO artikelstamm";
            $SQL.= " (";
            $SQL.= "   AST_ATUNR";
            $SQL.= "  ,AST_BEZEICHNUNG";
            $SQL.= "  ,AST_WUG_KEY";
            $SQL.= "  ,AST_IMQ_ID";
            $SQL.= "  ,AST_USER";
            $SQL.= "  ,AST_USERDAT";
            $SQL.= " )";
            $SQL.= " VALUES";
            $SQL.= " (";
            $SQL.= "   ".$this->_DB->WertSetzen('AST', 'T', $ATU_NR);
            $SQL.= "  ,".$this->_DB->WertSetzen('AST', 'T', $ArtBez);
            $SQL.= "  ,".$this->_DB->WertSetzen('AST', 'Z', $this->_rsARTOriginal->FeldInhalt('AST_WUG_KEY'));
            $SQL.= "  ,".$this->_DB->WertSetzen('AST', 'Z', 4);
            $SQL.= "  ,".$this->_DB->WertSetzen('AST', 'T', 'AWIS');
            $SQL.= "  ,SYSDATE";
            $SQL.= " )";

            $this->_DB->Ausfuehren($SQL,'', true, $this->_DB->Bindevariablen('AST'));

            $this->aktDebugAusgabe('erstelleATUNr -> INSERT artikelstamm: ' . $this->_DB->LetzterSQL());

            $asiArr = array(
                400 => '03',
                100 => strftime('%d.%m.%Y'),
                61 => '0',
                101 => '1'
            );

            foreach ($asiArr as $asi_aid_id => $asi_wert) {
                $SQL = " INSERT INTO artikelstamminfos";
                $SQL.= " (";
                $SQL.= "   ASI_AST_ATUNR";
                $SQL.= "  ,ASI_AIT_ID";
                $SQL.= "  ,ASI_WERT";
                $SQL.= "  ,ASI_IMQ_ID";
                $SQL.= "  ,ASI_USER";
                $SQL.= "  ,ASI_USERDAT";
                $SQL.= " )";
                $SQL.= " VALUES";
                $SQL.= " (";
                $SQL.= "  " . $this->_DB->WertSetzen('ASI', 'T', $ATU_NR);
                $SQL.= " ," . $this->_DB->WertSetzen('ASI', 'Z', $asi_aid_id);
                $SQL.= " ," . $this->_DB->WertSetzen('ASI', 'T', $asi_wert);
                $SQL.= " ," . $this->_DB->WertSetzen('ASI', 'Z', 4);
                $SQL.= " ," . $this->_DB->WertSetzen('ASI', 'T', 'AWIS');
                $SQL.= " ,SYSDATE";
                $SQL.= " )";

                $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('ASI'));

                $this->aktDebugAusgabe('erstelleATUNr -> INSERT artikelInfo ('.$asi_aid_id.'): ' . $this->_DB->LetzterSQL());
            }

            $this->_DB->TransaktionCommit();
        } catch (Exception $e) {
            $this->_DB->TransaktionRollback();
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler bei ATUNr-Anlage in Artikelstamm oder Artikelstamminfos');
            $this->aktDebugAusgabe($e->getMessage());
        }


    }

    /**
     * Erstellt den Lieferantenartikel mithilfe der Nr aus der Importtabelle
     * LiefArtNr ist nur die neue ATU-NR
     *
     * @param $liefArtNr
     */
    private function erstelleLiefArtikel($liefArtNr) {

        try {
            $this->aktDebugAusgabe('Erstelle jetzt den Lieferantenartikel-Datensatz');
            $this->_DB->TransaktionBegin();

            $SQL = "INSERT INTO LIEFERANTENARTIKEL ";
            $SQL .= " (";
            $SQL .= "  LAR_LARTNR";
            $SQL .= " ,LAR_LIE_NR";
            $SQL .= " ,LAR_IMQ_ID";
            $SQL .= " ,LAR_SUCH_LARTNR";
            $SQL .= " ,LAR_BEKANNTWW";
            $SQL .= " ,LAR_USER";
            $SQL .= " ,LAR_USERDAT";
            $SQL .= " )";
            $SQL .= " VALUES";
            $SQL .= " (";
            $SQL .= "  " . $this->_DB->WertSetzen('LAR', 'T', $liefArtNr);
            $SQL .= " ," . $this->_DB->WertSetzen('LAR', 'T', 7777);
            $SQL .= " ," . $this->_DB->WertSetzen('LAR', 'T', 4);
            $SQL .= " ," . $this->_DB->WertSetzen('LAR', 'T', $liefArtNr);
            $SQL .= " ," . $this->_DB->WertSetzen('LAR', 'Z', 0);
            $SQL .= " ," . $this->_DB->WertSetzen('LAR', 'T', 'AWIS');
            $SQL .= " ,SYSDATE";
            $SQL .= " )";

            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('LAR'));

            $this->aktDebugAusgabe('erstelleLiefArtikel -> INSERT liefArtNr: ' . $this->_DB->LetzterSQL());

            $this->_DB->TransaktionCommit();
        } catch (Exception $e) {
            $this->_DB->TransaktionRollback();
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler bei Erstellung von Lieferatenartikel-Datensatz');
            $this->aktDebugAusgabe($e->getMessage());
        }

    }


    /**
     * Loescht den SK Haken fuer den TEI KEY
     *
     * @param $TEI_KEY
     */
    private function loescheSKHaken($TEI_KEY) {

        try {
            $this->aktDebugAusgabe('Loesche jetzt SK-Haken fuer TEI_KEY - '.$TEI_KEY);
            $this->_DB->TransaktionBegin();

            $SQL = " DELETE";
            $SQL .= " FROM TEILEINFOSINFOS";
            $SQL .= " WHERE";
            $SQL .= " TII_TEI_KEY = ".$this->_DB->WertSetzen('TII', 'T', $TEI_KEY);
            $SQL .= " AND TII_ITY_KEY = ".$this->_DB->WertSetzen('TII', 'Z', 318);

            $this->_DB->Ausfuehren($SQL,'', true, $this->_DB->Bindevariablen('TII'));

            $this->aktDebugAusgabe('loescheSKHaken -> DELETE TEI_KEY-Haken: ' . $this->_DB->LetzterSQL());

            $this->_DB->TransaktionCommit();
        } catch (Exception $e) {
            $this->_DB->TransaktionRollback();
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler bei Loeschung von SK-Haken');
            $this->aktDebugAusgabe($e->getMessage());
        }

    }

    /**
     * Setzt den SK Haken fuer den TEI KEY
     *
     * @param $TEI_KEY
     */
    private function setzeSKHaken($TEI_KEY) {

        try {
            $this->aktDebugAusgabe('Erstelle/Setzte jetzt SK-Haken fuer TEI_KEY - '.$TEI_KEY);
            $this->_DB->TransaktionBegin();

            $SQL = " INSERT INTO TEILEINFOSINFOS";
            $SQL .= " (";
            $SQL .= " TII_TEI_KEY";
            $SQL .= ", TII_ITY_KEY";
            $SQL .= ", TII_WERT";
            $SQL .= ", TII_BEMERKUNG";
            $SQL .= ", TII_USER";
            $SQL .= ", TII_USERDAT";
            $SQL .= " )";
            $SQL .= " VALUES";
            $SQL .= " (";
            $SQL .= " ".$this->_DB->WertSetzen('TII', 'Z', $TEI_KEY);
            $SQL .= ", ".$this->_DB->WertSetzen('TII', 'Z', 318);
            $SQL .= ", ".$this->_DB->WertSetzen('TII', 'T', 1);
            $SQL .= ", ".$this->_DB->WertSetzen('TII', 'T', 'Artikel Splitting Import');
            $SQL .= ", ".$this->_DB->WertSetzen('TII', 'T', 'AWIS');
            $SQL .= ", SYSDATE";
            $SQL .= " )";

            $this->_DB->Ausfuehren($SQL,'', true, $this->_DB->Bindevariablen('TII'));

            $this->aktDebugAusgabe('setzeSKHaken -> INSERT TEI_KEY-Haken: ' . $this->_DB->LetzterSQL());

            $this->_DB->TransaktionCommit();
        } catch (Exception $e) {
            $this->_DB->TransaktionRollback();
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler bei Erstellung von SK-Haken');
            $this->aktDebugAusgabe($e->getMessage());
        }

    }

    /**
     * Erstellt einen Eintrag in der TeileInfos-Tabelle und stellt damit eine
     * Verknuepfung von der Artikelnummer zur der Lieferantenartikelnummer her
     *
     * @param string $atunr_neu Die neue ATU-Nr.
     * @param string $tei_key Die Lieferantenartikelnummer
     */
    private function erstelleTeileInfosSatzOE($atunr_neu, $tei_key) {
        try{
            $this->aktDebugAusgabe('Erstelle jetzt TeileInfos-Datensatz mit TEI_KEY ( '.$tei_key.' )');

            // hole mir den AST_KEY fuer die $atunr_neu
            $SQL = " SELECT *";
            $SQL.= " FROM artikelstamm";
            $SQL.= " WHERE ast_atunr = " . $this->_DB->WertSetzen('AST', 'T', $atunr_neu);

            $rsAST = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('AST'));

            if($rsAST->AnzahlDatensaetze() == 0) {
                throw new Exception('erstelleTeileInfosSatz OE - kein ast_key gefunden', '2106210837');
            }

            // hole mir TeileInfos-Daten
            $SQL = " SELECT *";
            $SQL.= " FROM teileinfos";
            $SQL.= " WHERE tei_key = " . $this->_DB->WertSetzen('TEI', 'N0', $tei_key);

            $rsTEI = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TEI'));

            $this->aktDebugAusgabe('erstellteTeileInfoSatz OE -> select TEI_KEY: ' . $this->_DB->LetzterSQL());

            if($rsTEI->AnzahlDatensaetze() == 0) {
                throw new Exception('erstelleTeileInfosSatz OE - kein tei_key gefunden', '2106181452');
            }

            $SQL = " INSERT INTO teileinfos";
            $SQL.= " (";
            $SQL.= "  TEI_ITY_ID1";
            $SQL.= " ,TEI_KEY1";
            $SQL.= " ,TEI_SUCH1";
            $SQL.= " ,TEI_WERT1";
            $SQL.= " ,TEI_ITY_ID2";
            $SQL.= " ,TEI_KEY2";
            $SQL.= " ,TEI_SUCH2";
            $SQL.= " ,TEI_WERT2";
            $SQL.= " ,TEI_USER";
            $SQL.= " ,TEI_USERDAT";
            $SQL.= " )";
            $SQL.= " VALUES";
            $SQL.= " (";
            $SQL.= "  " . $this->_DB->WertSetzen('TEI', 'T', $rsTEI->FeldInhalt('TEI_ITY_ID1'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'Z', $rsAST->FeldInhalt('AST_KEY'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $atunr_neu);
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $atunr_neu);
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $rsTEI->FeldInhalt('TEI_ITY_ID2'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $rsTEI->FeldInhalt('TEI_KEY2'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $rsTEI->FeldInhalt('TEI_SUCH2'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', $rsTEI->FeldInhalt('TEI_WERT2'));
            $SQL.= " ," . $this->_DB->WertSetzen('TEI', 'T', 'AWIS');
            $SQL.= " ,SYSDATE";
            $SQL.= " )";

            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('TEI'));

            $this->aktDebugAusgabe('erstellteTeileInfoSatz OE -> INSERT teileInfo: ' . $this->_DB->LetzterSQL());

        } catch (Exception $e) {
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler bei Erstellung von Teileinfos-Datensatz OE');
            $this->aktDebugAusgabe($e->getMessage());
        }
    }

    /**
     * Setzt den Datensatz im AWISIMPORT auf Verarbeitet
     *
     * @param $KEY
     */
    private function setVerarbeitetFlag($KEY) {
        try {
            $this->aktDebugAusgabe('Setzte jetzt Verarbeitungsstatus');
            $SQL = "UPDATE awisimport.artikelsplit ";
            $SQL .= " SET ";
            $SQL .= " verarbeitet = ".$this->_DB->WertSetzen('ARS', 'Z', ($this->_DSError)?-1:1);
            $SQL .= " WHERE ";
            $SQL .= " KEY = ".$this->_DB->WertSetzen('ARS', 'Z', $this->_rsARTQuelle->FeldInhalt('KEY'));

            $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('ARS'));

            $this->aktDebugAusgabe('setVerarbeitetFlag -> UPDATE AWISIMPORT-KEY: ' . $this->_DB->LetzterSQL());
        } catch (Exception $e) {
            $this->_DSError = true;
            $this->aktDebugAusgabe('Fehler bei Setzung von Verarbeitungsstatus');
            $this->aktDebugAusgabe($e->getMessage());
        }

    }

    /**
     * Entfernt Sonderzeichen und Leertasten fuer Suchfelder
     *
     * @param $text string
     * @return string|string[]|null
     */
    private function generiereSuchBegriff($text = ''){
        $SQL = "SELECT suchwort('$text') as suchBegriff FROM DUAL";

        $rsSuchBegriff = $this->_DB->RecordSetOeffnen($SQL);
        return $rsSuchBegriff->FeldInhalt('suchBetriff');
    }

    /**
     * Schreibt eine ERROR-Mail falls Fehler aufgetreten sind
     */
    private function schreibeERRORMail()
    {
        $this->debugAusgabe("Schreibe ERROR Mail",99);

        $this->_Mailer->LoescheAdressListe();
        $this->_Mailer->Absender('awis' . strval(($this->_Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_SHUTTLE) ? (awisWerkzeuge::AWIS_LEVEL_SHUTTLE) : (awisWerkzeuge::AWIS_LEVEL_ENTWICK)) . '@de.atu.eu');

        foreach($this->_MailEmpfaenger as $Empfaenger) {
            $this->_Mailer->AdressListe(awisMailer::TYP_TO,$Empfaenger);
        }

        $this->_Mailer->Text($this->_AktDebugText, awisMailer::FORMAT_TEXT);
        $this->_Mailer->Betreff("ERROR ArtikelSplitJOB - DATUM " . date('Ymd'));

        $this->_Mailer->SetzeBezug('ARS', 0);

        $this->_Mailer->MailInWarteschlange();
    }


    /**
     * Aktuelle Debugausgabe speichern
     *
     * @param string $text
     */
    private function aktDebugAusgabe($text)
    {
        $this->_AktDebugText = $this->_AktDebugText.$text."\r\n";
        echo"<br>";
    }
    /**
     * Leert aktuelle/letzte Debugausgabe
     */
    private function leereAktDebugAusgabe()
    {
        $this->debugAusgabe('-------Naechster DS-------',$this->_DebugLevel);
        $this->_AktDebugText = "";
    }

    /**
     * Debugausgabe ausgeben
     *
     * @param string $text
     * @param int $debugLevel
     */
    private function debugAusgabe($text, $debugLevel)
    {
        $this->_DebugText = $this->_DebugText.$text."\r\n";
        if ($this->_DebugLevel >= $debugLevel) {
            echo $text . ((substr($text, -(strlen(PHP_EOL))) == PHP_EOL)?'':PHP_EOL);
        }
    }
}