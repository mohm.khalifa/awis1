<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('praktikum_funktionen.inc');

try {
    $ZeigeSchaltflaecheWeiter = false;
    $ZeigeSchaltflaecheSpeichern = false;
    $ZeigeSchaltflaecheNeu = false;
    $ZeigeSchaltflaecheEditieren = false;

    $Editiermodus = false;

    // Textkonserven laden
    $TextKonserven[] = array('PVS', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_liste');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    //$TextKonserven[]=array('Wort','lbl_hilfe');
    //$TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    //$TextKonserven[]=array('Wort','Seite');
    //$TextKonserven[]=array('Wort','Status');
    //$TextKonserven[]=array('Wort','AktuellesSortiment');
    //$TextKonserven[]=array('Wort','txt_BitteWaehlen');
    //$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    //$TextKonserven[]=array('Fehler','err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht18000 = $AWISBenutzer->HatDasRecht(18000);
    if ($Recht18000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $PV = new awis_praktikum_funktionen($DB, $AWISBenutzer, $Form, $AWISCursorPosition);
    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PVS'));
    if ((strlen($AWISBenutzer->ParameterLesen('Formular_PVS')) < 8)) {
        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = 'PVS_KEY DESC';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    if (isset($_POST['cmdSuche_x'])) {
        $Param = array();
        $Param['SUCH_PVS_FilNr'] = isset($_POST['sucFilNr'])?$_POST['sucFilNr']:$_POST['txtFilNr'];
        $Param['SUCH_PVS_Filiale'] = isset($_POST['sucFiliale'])?$_POST['sucFiliale']:'';
        $Param['SUCH_PVS_Betreuer'] = isset($_POST['sucBetreuer'])?$_POST['sucBetreuer']:'';
        $Param['SUCH_PVS_Praktikant'] = isset($_POST['sucPraktikant'])?$_POST['sucPraktikant']:'';
        $Param['SUCH_PVS_Praktikumart'] = isset($_POST['sucPraktikumart'])?$_POST['sucPraktikumart']:'';
        $Param['SUCH_PVS_DatumVon'] = isset($_POST['sucDatumVon'])?$_POST['sucDatumVon']:'';
        $Param['SUCH_PVS_DatumBis'] = isset($_POST['sucDatumBis'])?$_POST['sucDatumBis']:'';

        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = 'PVS_KEY DESC';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';

        $AWISBenutzer->ParameterSchreiben("Formular_PVS", serialize($Param));
    } elseif (isset($_POST['cmdSpeichern_x'])) {

        if (isset($_POST['txtLISTE'])) {
            $PV->Liste_Speichern($_POST);
        } elseif (isset($_POST['txtMAIL_EIGNUNG'])){
            $PV->Speicher_Schritt3($_POST['txtMAIL_PVS_KEY'],$_POST['txtMAIL_EIGNUNG']);
        }
        else {
            $PV->HoleWerteFuerParamSpeichern($Param);

            if ($PV->Kontrolliere_NeuePvSchritt1($Param, false) >= 0) {
                if ($PV->Kontrolliere_NeuePvSchritt2($Param, false) >= 0) {
                    if (isset($_POST['txtUpdate'])) {
                        $PV->EditPv_Speichern($Param, $_POST['txtPVS_KEY']);
                    } else {
                        $PV->NeuePv_Speichern($Param);
                    }
                } else {
                    if (isset($_POST['txtUpdate'])) {
                        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_POST['txtPVS_KEY']);
                        $HiddenFeldUpdate = true;
                    }
                    $_POST['cmdWeiter_x'] = "X";        // Einfach setzen, dass if weiter unten anschlägt, weil Daten korrigieren
                }
            } else {
                $ZeigeSchaltflaecheWeiter = true;
                if (isset($_POST['txtUpdate'])) {
                    $Form->Erstelle_HiddenFeld('PVS_KEY', $_POST['txtPVS_KEY']);
                    $PV->Erstelle_NeuePvSchritt1($Param, true);
                } else {
                    $_POST['cmdDSNeu_x'] = "X";            // Einfach setzen, dass if weiter unten anschlägt, weil Daten korrigieren
                }
            }
        }
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $PV->HoleWerteFuerParamNeu($Param);
        $AWIS_KEY1 = -1;
    } elseif (isset($_POST['cmdWeiter_x'])) {
        if (isset($_POST['txtUpdate'])) {
            $HiddenFeldUpdate = true;
            $PV->HoleWerteFuerParamAusDB($_POST['txtPVS_KEY'], $Param, false);
        } else {
            $PV->HoleWerteFuerParamWeiter($Param);
            $HiddenFeldUpdate = false;
        }

        $AWIS_KEY1 = -1;
    } elseif (isset($_GET['PVS_KEY'])) {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_GET['PVS_KEY']);

        if (isset($_GET['aktiv'])) {
            $AktivDaten = explode(awis_praktikum_funktionen::_DecodeEncodeSperatorString, base64_decode($_GET['aktiv']));
            if (($DB->FeldInhaltFormat('N0', $AktivDaten[0]) === $AWIS_KEY1) AND ($AktivDaten[1] == awis_praktikum_funktionen::_AktivString)) {
                $PV->NegiereAktivInaktiv($_GET['PVS_KEY'], $_GET['button']);
                unset($_GET['PVS_KEY']);
                unset($_GET['button']);
                $AWIS_KEY1 = 0;
            }
        }
    }
    elseif(isset($_GET['MAIL_PVS_KEY'])){
        $AWIS_KEY1 = $_GET['MAIL_PVS_KEY'];
    }
    elseif (isset($_POST['cmdEdit_x'])) {
        if (isset($_POST['txtPVS_KEY'])) {
            $AWIS_KEY1 = $_POST['txtPVS_KEY'];
        }
    } elseif ($AWIS_KEY1 == 0) {
        if (!isset($Param['KEY'])) {
            $Param['KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = 'PVS_KEY DESC';
            $AWISBenutzer->ParameterSchreiben('Formular_PVS', serialize($Param));
        }

        if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
            $Param['KEY'] = 0;
        }

        $AWIS_KEY1 = $Param['KEY'];
    }

    //*********************************************************
    //* Sortierung
    //*********************************************************
    if (!isset($_GET['Sort'])) {
        if ($Param['ORDER'] != '') {
            $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
        } else {
            $ORDERBY = ' ORDER BY PVS_KEY DESC';
            $Param['ORDER'] = 'PVS_KEY DESC';
        }
    } else {
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $SQL = "";
    $SQL .= "SELECT pvs.*, ";
    $SQL .= "row_number() OVER (" . $ORDERBY . ") AS ZeilenNr ";
    $SQL .= "FROM v_praktikumsvereinbarung pvs ";

    $Bedingung = $PV->Erstelle_Filterbedingung($Param, $Recht18000);

    if ($Bedingung != '') {
        $SQL .= " WHERE " . substr($Bedingung, 4);
    }

    // Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
    if ($AWIS_KEY1 <= 0) {
        // Zum Blättern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
            $AWISBenutzer->ParameterSchreiben('Formular_PVS', serialize($Param));
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = 'SELECT * FROM (' . $SQL . ') DATEN WHERE ZeilenNr >= ' . $StartZeile . ' AND ZeilenNr < ' . ($StartZeile + $ZeilenProSeite) . ' ';
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }
    $SQL .= $ORDERBY;

    // Zeilen begrenzen
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $rsPVS = $DB->RecordsetOeffnen($SQL);
    $AWISBenutzer->ParameterSchreiben('Formular_PVS', serialize($Param));

    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->SchreibeHTMLCode('<form name=frmPTSDetails action=./praktikum_Main.php?cmdAktion=Details method=POST enctype="multipart/form-data">');

    if (isset($_POST['cmdDSNeu_x']))    // Neue PV hinzufügen, Schritt 1 - Auswahl an Formularen
    {
        $ZeigeSchaltflaecheWeiter = true;
        $PV->Erstelle_NeuePvSchritt1($Param);
    } elseif (isset($_POST['cmdWeiter_x']))    // Neue PV anlegen, Schritt 2
    {
        if ($PV->Kontrolliere_NeuePvSchritt1($Param) >= 0) {
            $ZeigeSchaltflaecheSpeichern = true;
            if (isset($_POST['txtPVS_KEY'])) {
                $PV->Erstelle_NeuePvSchritt2($Param, $HiddenFeldUpdate, $_POST['txtPVS_KEY']);
            } else {
                $PV->Erstelle_NeuePvSchritt2($Param);
            }
        } else {
            $ZeigeSchaltflaecheWeiter = true;
            if (isset($_POST['txtUpdate'])) {
                $Form->Erstelle_HiddenFeld('PVS_KEY', $_POST['txtPVS_KEY']);
                $PV->Erstelle_NeuePvSchritt1($Param, true);
            } else {
                $PV->Erstelle_NeuePvSchritt1($Param);
            }
        }
    } elseif (isset($_POST['cmdEdit_x'])) {
        $Form->Erstelle_HiddenFeld('PVS_KEY', $AWIS_KEY1);
        $PV->HoleWerteFuerParamAusDB($AWIS_KEY1, $Param);
        $PV->Erstelle_NeuePvSchritt1($Param, true);
        $ZeigeSchaltflaecheWeiter = true;
    } elseif ($rsPVS->EOF() AND !isset($_POST['cmdDSNeu_x']))    // Keine Meldung bei neuen Datensätzen!
    {
        $ZeigeSchaltflaecheNeu = true;
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    }elseif(isset($_GET['MAIL_PVS_KEY'])){
        $PV->Erstelle_Schritt3($rsPVS);
        $ZeigeSchaltflaecheSpeichern=true;
    }
    elseif ($rsPVS->AnzahlDatensaetze() >= 1 AND !isset($_GET['PVS_KEY']))    // Liste anzeigen
    {
        if (($Recht18000 & 32) == 32) {
            $ZeigeSchaltflaecheSpeichern = true;
            $Form->Erstelle_HiddenFeld('LISTE', true);
        }
        $Feldcount = 1;
        $ZeigeSchaltflaecheNeu = true;
        $Form->Formular_Start();

        $Form->ZeileStart($ListenSchriftGroesse == 0?'':'font-size:' . intval($ListenSchriftGroesse) . 'pt');

        $SpaltenArray = array();
        /*
        $SpaltenArray[0] = array('Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Aktiv'],
                                 'Wert' => '',
                                 'WertToolTip' => '',
                                 'Breite' => 32,
                                 'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])? '&Block=' . intval($_GET['Block']): '') . (isset($_GET['Seite'])? '&Seite=' . ($_GET['Seite']): '')
                                             . '&Sort=p_aktiv' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'p_aktiv'))? '~': ''),
                                 'ToolTip' => $AWISSprachKonserven['PVS']['ttt_SortierreihenfolgeAktiv'],
                                 'Style' => 'font-weight:bolder'); */
        // Spalte => Praktikant
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Praktikant'],
            'Feldname' => '#PVS_PRAKTIKANTNAME',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 150,
            'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=p_name' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'p_name'))?'~':''),
            'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
            'Style' => 'font-weight:bolder'
        );
        // Spalte => Von	                                    
        $Feldcount++;
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Von'],
            'Feldname' => '#PVS_DATUMVON',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 84,
            'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=p_datumvon' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'p_datumvon'))?'~':''),
            'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
            'Style' => 'font-weight:bolder'
        );
        // Spalte => Bis	                                    
        $Feldcount++;
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Bis'],
            'Feldname' => '#PVS_DATUMBIS',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 84,
            'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=p_datumbis' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'p_datumbis'))?'~':''),
            'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
            'Style' => 'font-weight:bolder'
        );
        // Spalte => Tätigkeit	                         			
        $Feldcount++;
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Taetigkeit'],
            'Feldname' => '#PVS_EINSATZGEBIET',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 68,
            'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=pvs_taetigkeit' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'pvs_taetigkeit'))?'~':''),
            'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
            'Style' => 'font-weight:bolder'
        );
        // Spalte => Art des Praktikums	                         			
        $Feldcount++;
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Praktikumart'],
            'Feldname' => '#PVS_PRAKTIKUMART',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 146,
            'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=pvs_part' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'pvs_part'))?'~':''),
            'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
            'Style' => 'font-weight:bolder'
        );
        // Spalte => Betreuer
        $Feldcount++;
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Betreuer'],
            'Feldname' => '#PVS_BETRPERSNR',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 68,
            'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=b_persnr' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'b_persnr'))?'~':''),
            'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
            'Style' => 'font-weight:bolder'
        );
        // Spalte => Filiale
        $Feldcount++;
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Filiale'],
            'Feldname' => '#PVS_FIL_ID',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 46,
            'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=f_nr' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'f_nr'))?'~':''),
            'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
            'Style' => 'font-weight:bolder'
        );
        $Feldcount++;
        if (($Recht18000 & 32) == 32) {
            // Spalte => Status/Warengutschein
            $SpaltenArray[$Feldcount] = array(
                'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Warengutschein'],
                'Feldname' => '',
                'Wert' => '',
                'WertToolTip' => '',
                'Breite' => 30,
                'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=p_gutschein' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'p_gutschein'))?'~':''),
                'ToolTip' => $AWISSprachKonserven['PVS']['ttt_StatusGutschein'],
                'Style' => 'font-weight:bolder'
            );
            $Feldcount++;
        }

        // Spalte => Status/PDF
        $SpaltenArray[$Feldcount] = array(
            'Bezeichnung' => ((($Recht18000 & 16) == 16)?$AWISSprachKonserven['PVS']['PVS_StatusPdfDiplomAlle']:$AWISSprachKonserven['PVS']['PVS_StatusPdfDiplom']),
            'Feldname' => '',
            'Wert' => '',
            'WertToolTip' => '',
            'Breite' => 55,
            'Link' => ((($Recht18000 & 16) == 16)?'./praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=p_aktiv' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'p_aktiv'))?'~':''):''),
            'ToolTip' => ((($Recht18000 & 16) == 16)?$AWISSprachKonserven['PVS']['ttt_StatusPdfDiplomAlle']:$AWISSprachKonserven['PVS']['ttt_StatusPdfDiplom']),
            'Style' => 'font-weight:bolder'
        );

        $Feldcount++;
        if(($Recht18000 & 64) == 64) {
            //Spalte => Eignung
            $SpaltenArray[$Feldcount] = array(
                'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Eignung_Short'],
                'Feldname' => '',
                'Wert' => '',
                'WertToolTip' => '',
                'Breite' => 25,
                'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=pvs_eignung' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'pvs_eignung'))?'~':''),
                'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Eignung'],
                'Style' => 'font-weight:bolder'
            );
        }

        $Feldcount++;
        if (($Recht18000 & 32) == 32) {
            // Spalte => Kommentar/Fachabteilung
            $SpaltenArray[$Feldcount] = array(
                'Bezeichnung' => $AWISSprachKonserven['PVS']['PVS_Kommentar'],
                'Feldname' => 'kommentar',
                'Wert' => '',
                'WertToolTip' => '',
                'Breite' => 230,
                'Link' => './praktikum_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'') . '&Sort=p_kommentar' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'p_kommentar'))?'~':''),
                'ToolTip' => $AWISSprachKonserven['PVS']['ttt_Sortierreihenfolge'],
                'Style' => 'font-weight:bolder'
            );
        }
        $Form->Formular_Start();
        $Form->ZeileStart($ListenSchriftGroesse == 0?'':'font-size:' . intval($ListenSchriftGroesse) . 'pt');
        // Schleife für Überschriftzeile (7 Felder, siehe Array-Def. drüber)
        for ($i = 1; $i <= $Feldcount; $i++) {
            if(isset($SpaltenArray[$i])) {
                $Form->Erstelle_Liste_Ueberschrift($SpaltenArray[$i]['Bezeichnung'], $SpaltenArray[$i]['Breite'], $SpaltenArray[$i]['Style'], $SpaltenArray[$i]['Link'],
                    $SpaltenArray[$i]['ToolTip']);
            }
        }
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsPVS->EOF()) {
            $Form->ZeileStart($ListenSchriftGroesse == 0?'':'font-size:' . intval($ListenSchriftGroesse) . 'pt');

            $LinkAktiv = './praktikum_Main.php?cmdAktion=Details&PVS_KEY=0' . $rsPVS->FeldInhalt('PVS_KEY') . '&aktiv=' . base64_encode($rsPVS->FeldInhalt('PVS_KEY') . awis_praktikum_funktionen::_DecodeEncodeSperatorString . awis_praktikum_funktionen::_AktivString);
            //$LinkEdit = './praktikum_Main.php?cmdAktion=Details&PVS_KEY=0' . $rsPVS->FeldInhalt('PVS_KEY') . '&edit=' . base64_encode($rsPVS->FeldInhalt('PVS_KEY') . awis_praktikum_funktionen::_DecodeEncodeSperatorString . awis_praktikum_funktionen::_EditString);
            $LinkDetail = './praktikum_Main.php?cmdAktion=Details&PVS_KEY=0' . $rsPVS->FeldInhalt('PVS_KEY'); // . (isset($_GET['Block'])? '&Block=' . intval($_GET['Block']): '') . (isset($_GET['Seite'])? '&Seite=' . ($_GET['Seite']): '');
            $Param = 'PVS_KEY=' . urlencode('=~') . $rsPVS->FeldInhalt('PVS_KEY');
            $LinkDiplom = '';
            $LinkPDFs = '';
            switch ($rsPVS->FeldInhalt('PVS_PART')) {
                case 1:            // OiB
                    if ($rsPVS->FeldInhalt('PVS_PGRUND') != 2) {
                        $LinkPDFs = '/berichte/drucken.php?XRE=6&ID=' . base64_encode($Param);
                        $LinkDiplom = '/berichte/drucken.php?XRE=14&ID=' . base64_encode($Param);
                    } else {
                        $LinkPDFs = '/berichte/drucken.php?XRE=72&ID=' . base64_encode($Param);
                        $LinkDiplom = '/berichte/drucken.php?XRE=73&ID=' . base64_encode($Param);
                    }
                    break;
                case 2:            // Berufsvorbereitend
                    if (($Recht18000 & 16) == 16 or ($Recht18000 & 32) == 32) {
                        $LinkPDFs = '/berichte/drucken.php?XRE=11&ID=' . base64_encode($Param);
                        //$LinkDiplom = '/berichte/drucken.php?XRE=14&ID=' . base64_encode($Param);
                    }
                    break;
                case 3:            // freiwillig
                    //if (($Recht18000&16) == 16)
                    //{
                    $LinkPDFs = '/berichte/drucken.php?XRE=13&ID=' . base64_encode($Param);
                    $LinkDiplom = '/berichte/drucken.php?XRE=38&ID=' . base64_encode($Param);
                    //}
                    break;
                case 4:            // Lehrstellenbewerber
                    $LinkPDFs = '/berichte/drucken.php?XRE=8&ID=' . base64_encode($Param);
                    break;
                default:
                    break;
            }

            /*
            if (($Recht18000&2) == 2)
            {
                $IconsArray[] = array("edit", $LinkEdit, "", $AWISSprachKonserven['PVS']['ttt_Bearbeiten']);
            }
            */

            $Daten = explode('|', $AWISSprachKonserven['PVS']['lst_Taet']);

            $MaxStringLength = 12;
            $SpaltenArray[1]['Wert'] = ((strlen($rsPVS->FeldInhalt('P_NAME')) > ($MaxStringLength + 1))?substr($rsPVS->FeldInhalt('P_NAME'), 0,
                    $MaxStringLength) . ' ...':$rsPVS->FeldInhalt('P_NAME'));

            //$SpaltenArray[1]['Wert'] = $rsPVS->FeldInhalt('P_NAME');

            $SpaltenArray[1]['WertTooltip'] = $rsPVS->FeldInhalt('P_ANREDE') . ' ' . $rsPVS->FeldInhalt('P_NAME') . ' ' . $rsPVS->FeldInhalt('P_VORNAME') . ' - ' . $rsPVS->FeldInhalt('P_STRASSE') . ' ' . $rsPVS->FeldInhalt('P_HAUSNR') . ' - ' . $rsPVS->FeldInhalt('P_PLZ') . ' ' . $rsPVS->FeldInhalt('P_ORT');
            $SpaltenArray[2]['Wert'] = $Form->Format('D', $rsPVS->FeldInhalt('P_DATUMVON'));
            $SpaltenArray[2]['WertTooltip'] = $Form->Format('D', $rsPVS->FeldInhalt('P_DATUMVON'));
            $SpaltenArray[3]['Wert'] = $Form->Format('D', $rsPVS->FeldInhalt('P_DATUMBIS'));
            $SpaltenArray[3]['WertTooltip'] = $Form->Format('D', $rsPVS->FeldInhalt('P_DATUMBIS'));
            $SpaltenArray[4]['Wert'] = $PV->LiefereWertZuInfoKey($rsPVS->FeldInhalt('PVS_TAETIGKEIT'), $Daten);
            $SpaltenArray[4]['WertTooltip'] = $rsPVS->FeldInhalt('PVS_TAETIGKEIT');
            $SpaltenArray[5]['Wert'] = $PV->HolePraktikumartBezeichnung($rsPVS->FeldInhalt('PVS_PART'));
            $SpaltenArray[5]['WertTooltip'] = $PV->HolePraktikumartBezeichnung($rsPVS->FeldInhalt('PVS_PART'), $rsPVS->FeldInhalt('PVS_PGRUND'));
            $SpaltenArray[6]['Wert'] = $rsPVS->FeldInhalt('B_PERSNR');
            $SpaltenArray[6]['WertTooltip'] = $rsPVS->FeldInhalt('B_VORNAME') . $rsPVS->FeldInhalt('B_NAME');
            $SpaltenArray[7]['Wert'] = $rsPVS->FeldInhalt('F_NR');
            $SpaltenArray[7]['WertTooltip'] = $rsPVS->FeldInhalt('F_BEZ') . ' - ' . $rsPVS->FeldInhalt('F_STRASSE') . ' - ' . $rsPVS->FeldInhalt('F_LAND') . '_' . $rsPVS->FeldInhalt('F_PLZ') . ' ' . $rsPVS->FeldInhalt('F_ORT');
            $SpaltenArray[11]['Wert'] = $rsPVS->FeldInhalt('P_KOMMENTAR');
            $SpaltenArray[11]['WertTooltip'] = '';

            //$Form->Erstelle_ListeIcons($IconsArray, $SpaltenArray[0]['Breite'], ($DS%2));

            $Form->Erstelle_ListenFeld($SpaltenArray[1]['Feldname'], $SpaltenArray[1]['Wert'], 0, $SpaltenArray[1]['Breite'], false, ($DS % 2), '', $LinkDetail, '', 'L',
                $SpaltenArray[1]['WertTooltip']);
            $Form->Erstelle_ListenFeld($SpaltenArray[2]['Feldname'], $SpaltenArray[2]['Wert'], 0, $SpaltenArray[2]['Breite'], false, ($DS % 2), '', '', '', 'L',
                $SpaltenArray[2]['WertTooltip']);
            $Form->Erstelle_ListenFeld($SpaltenArray[3]['Feldname'], $SpaltenArray[3]['Wert'], 0, $SpaltenArray[3]['Breite'], false, ($DS % 2), '', '', '', 'L',
                $SpaltenArray[3]['WertTooltip']);
            $Form->Erstelle_ListenFeld($SpaltenArray[4]['Feldname'], $SpaltenArray[4]['Wert'], 0, $SpaltenArray[4]['Breite'], false, ($DS % 2), '', '', '', 'L',
                $SpaltenArray[4]['WertTooltip']);
            $Form->Erstelle_ListenFeld($SpaltenArray[5]['Feldname'], $SpaltenArray[5]['Wert'], 0, $SpaltenArray[5]['Breite'], false, ($DS % 2), '', '', '', 'L',
                $SpaltenArray[5]['WertTooltip']);
            $Form->Erstelle_ListenFeld($SpaltenArray[6]['Feldname'], $SpaltenArray[6]['Wert'], 0, $SpaltenArray[6]['Breite'], false, ($DS % 2), '', '', '', 'L',
                $SpaltenArray[6]['WertTooltip']);
            $Form->Erstelle_ListenFeld($SpaltenArray[7]['Feldname'], $SpaltenArray[7]['Wert'], 0, $SpaltenArray[7]['Breite'], false, ($DS % 2), '', '', '', 'L',
                $SpaltenArray[7]['WertTooltip']);

            $IconsArray = array();

            if (($Recht18000 & 32) == 32) {
                $LinkAktiv .= '&button=Gutschein';
                if ($rsPVS->FeldInhalt('P_GUTSCHEIN') == 'A') {
                    $IconsArray[] = array("geldkasten_gruen", $LinkAktiv, "", $AWISSprachKonserven['PVS']['ttt_PvAktiv']);
                } else {
                    $IconsArray[] = array("geldkasten_rot", $LinkAktiv, "", $AWISSprachKonserven['PVS']['ttt_PvInaktiv']);
                }
                $Form->Erstelle_ListeIcons($IconsArray, $SpaltenArray[8]['Breite'], ($DS % 2));
                $IconsArray = '';
                $Ware = true;
            }

            if (($Recht18000 & 16) == 16) {
                $LinkAktiv .= '&button=Vorgang';
                if ($rsPVS->FeldInhalt('P_AKTIV') == 'A') {
                    $IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['PVS']['ttt_PvAktiv']);
                } else {
                    $IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['PVS']['ttt_PvInaktiv']);
                }
            }

            if (!empty($LinkPDFs)) {
                $IconsArray[] = array("pdf", $LinkPDFs, "", $AWISSprachKonserven['PVS']['ttt_PdfDrucken']);
            }

            if (!empty($LinkDiplom)) {
                if ($rsPVS->FeldInhalt('PVS_PART') == 1 and $rsPVS->FeldInhalt('PVS_PGRUND') == 2) {
                    $IconsArray[] = array("mw", $LinkDiplom, "", $AWISSprachKonserven['PVS']['ttt_PdfDiplomDrucken']);
                } else {
                    $IconsArray[] = array("rosette", $LinkDiplom, "", $AWISSprachKonserven['PVS']['ttt_PdfDiplomDrucken']);
                }
            }

            $Form->Erstelle_ListeIcons($IconsArray, (isset($Ware))?$SpaltenArray[9]['Breite']:$SpaltenArray[8]['Breite'], ($DS % 2));
            if (($Recht18000 & 64) == 64) {
                $Feldname =$SpaltenArray[9]['Feldname'] . '_' . $rsPVS->FeldInhalt('PVS_KEY');
                if($rsPVS->FeldInhalt('PVS_EIGNUNG') == '2') {
                    $Farbe = 'gruen';
                } elseif($rsPVS->FeldInhalt('PVS_EIGNUNG') == '0') {
                    $Farbe = 'rot';
                } else {
                    $Farbe = 'gelb';
                }
                if(strtotime(date('d.m.Y'))>=strtotime($rsPVS->FeldInhalt('P_DATUMBIS', 'D'))){
                    $BildTyp = awisFormular::BILD_TYP_HREF;
                } else {
                    $BildTyp = awisFormular::BILD_TYP_OHNEAKTION;
                }
                $Form->Erstelle_Bild($BildTyp,'Eignung','praktikum_Main.php?cmdAktion=Details&MAIL_PVS_KEY='.$rsPVS->FeldInhalt('PVS_KEY'),'/bilder/ampel_' . $Farbe.'_neu_hoch.jpg',$AWISSprachKonserven['PVS']['ttt_EignungAendern'], '',16,32, 29);
            }

            if (($Recht18000 & 32) == 32) {
                $Feldname = $SpaltenArray[11]['Feldname'] . '_' . $rsPVS->FeldInhalt('PVS_KEY');
                $Form->Erstelle_ListenFeld($Feldname, $SpaltenArray[11]['Wert'], 32, $SpaltenArray[11]['Breite'], true, ($DS % 2), '', '', '', 'L',
                    $SpaltenArray[11]['WertTooltip']);
            }

            $Form->ZeileEnde();
            $rsPVS->DSWeiter();
            $DS++;
        }
        unset($SpaltenArray);    // temporäres Array wieder zerstören

        $Link = './praktikum_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    } else {
        $PV->HoleWerteFuerParamAusDB($AWIS_KEY1, $Param);
        $ZeigeSchaltflaecheEditieren = true;
        $Form->Formular_Start();
        $PV->Kontrolliere_NeuePvSchritt1($Param);
        $PV->Kontrolliere_NeuePvSchritt2($Param);
        $Form->Erstelle_HiddenFeld('PVS_KEY', $AWIS_KEY1);
        $Form->Formular_Ende();
    }

    //awis_Debug(1, $Param, $Bedingung, $rsOTT, $_POST, $rsAZG, $SQL, $AWISSprache);

    //***************************************
    // Schaltflächen für dieses Register
    //***************************************
    $Form->SchaltflaechenStart();

    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    // Weiter
    if ($ZeigeSchaltflaecheWeiter) {
        $Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    }

    // Editieren
    if ($ZeigeSchaltflaecheEditieren AND (($Recht18000 & 2) == 2)) {
        $Form->Schaltflaeche('image', 'cmdEdit', '', '/bilder/cmd_notizblock.png', $AWISSprachKonserven['Wort']['lbl_aendern'], 'E');
    }

    // Speichern
    if ($ZeigeSchaltflaecheSpeichern AND ((($Recht18000 & 32) == 32) OR ($Recht18000 & 2) == 2)) {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    // Hinzufügen, wenn erlaubt	UND Detailübersicht
    if ($ZeigeSchaltflaecheNeu AND (($Recht18000 & 4) == 4)) {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201110231158");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201110231157");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>