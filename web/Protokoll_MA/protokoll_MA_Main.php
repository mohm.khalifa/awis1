<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

if(isset($_POST['cmdDrucken_x']))
{
	require_once '/daten/web/berichte/ber_Mitarbeitergespraech.inc';

	$BerObj = new ber_Mitarbeitergespraech($AWISBenutzer, $DB, 40);
	//var_dump($_POST);
	$Parameter['PERSNR']=$_POST['txtPMA_PERSNR'];
	$Parameter['NAMEVOLL']=$_POST['txtPMA_NAMEMA'];
	if($_POST['txtPMA_VORNAMEMA'] != '')
	{
		$Parameter['NAMEVOLL'].= ','.$_POST['txtPMA_VORNAMEMA'];
		if($_POST['txtPMA_NAMEMA'] == '')
		{
			$Parameter['NAMEVOLL'] = substr(trim($Parameter['NAMEVOLL']), 1);
		}
	}
	$Parameter['FUNKTION']=$_POST['txtPMA_FUNCMA'];
	$Parameter['ABTEILUNG']=$_POST['txtPMA_ABTEILUNG'];
	$Parameter['NAMEVOLLV']=$_POST['txtPMA_NAMEVO'];
	if($_POST['txtPMA_VORNAMEVO'] != '')
	{
		$Parameter['NAMEVOLLV'].= ','.$_POST['txtPMA_VORNAMEVO'];
		if($_POST['txtPMA_NAMEVO'] == '')
		{
			$Parameter['NAMEVOLLV'] = substr(trim($Parameter['NAMEVOLL']), 1);
		}
	}
	$Parameter['FUNKTIONV']=$_POST['txtPMA_FUNCVO'];
	$Parameter['ANTWORT1']=$_POST['txtPMA_ANTWORT1'];
	$Parameter['ANTWORT2']=$_POST['txtPMA_ANTWORT2'];
	$Parameter['ANTWORT3']=$_POST['txtPMA_ANTWORT3'];
	$Parameter['ANTWORT4']=$_POST['txtPMA_ANTWORT4'];
	$Parameter['ANTWORT5']=$_POST['txtPMA_ANTWORT5'];
	$Parameter['ANTWORT6']=$_POST['txtPMA_ANTWORT6'];
	$Parameter['ANTWORT7']=$_POST['txtPMA_ANTWORT7'];
	$Parameter['ANTWORT8']=$_POST['txtPMA_ANTWORT8'];
	$Parameter['ENTWICKL']=$_POST['txtENTWICK'];
	
	$BerObj->init($Parameter);
	$BerObj->ErzeugePDF(1);
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_Protokoll_MA');


$AWISSprachKonserven = awisFormular::LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Protokoll_MA'].'</title>';
?>
</head>
<body>
<?php
include ("awisHeader.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	if($AWISBenutzer->HatDasRecht(30000)==0)
	{
		$Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"200809161548");
		die();
	}

	$Register = new awisRegister(30000);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

	if($AWISCursorPosition!=='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</body>
</html>