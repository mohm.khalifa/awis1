<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<link rel=stylesheet type=text/css href=ATU.css>
</head>

<body>
<?php
require_once("config.inc.php");

global $AWISBenutzer;
global $awis_web_osuser;
global $awis_web_ospwd;
global $awis_webtest_user;
global $awis_webtest_pwd;
global $_GET;
include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 10);

if(($RechteStufe&1)!=1)
{
    awisEreignis(3,1000,'LoadBalancer',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}
awislogoff($con);

$lblog = '/daten/web/admin/loadbalancer/loadbalancer';
$fehl_nfs = 1;						// Per NFS "geholte" Zeit h�ngt hinterher
$fehl_lokal = 2;					// Lokale Zeit h�ngt hinterher
$fehl_erreichbar = 4;				// Port 61001 ist nicht erreichbar
$fehl_loadbalancer = 8;				// Server ist �ber den Loadbalancer nicht erreichbar
$fehl_db_nicht_erreichbar = 16;		// Anmeldung an die Datenbank hat nicht geklappt

echo 'Anhand dieser Daten entscheidet der Loadbalancer, ob die Webserver (und ihre NFS-Einbindungen!) OK sind <br>(Info: Port 61001)<br><br>';
//var_dump($_GET);
echo '<table border=1>';
echo '<th id=FeldBez>Rechner</th><th id=FeldBez>LB1 =&gt; NFS (61001)</th><th id=FeldBez>LB2 =&gt; Lokal (61001)</th><th id=FeldBez>Loadbalancer (80)</th><th id=FeldBez>Status</th>';
foreach($awis_webserver as $key => $rechner)
{
	$fehler = 0;
	$Log1 = @fopen("http://".$awis_web_osuser.":".$awis_web_ospwd."@" . $rechner . "/dokumente/vorlagen/BriefpapierATU_DE_Seite_1.pdf",'r');
	$Log2 = @file("http://".$awis_webtest_user.":".$awis_webtest_pwd."@" . $rechner . ":61001/LB2/");
	
	/* HOST-Test START */
	//$Log3 = @file("http://loadbalancer:atu@" . $rechner . "m");
	$errno='';
	$errstr='';
	$Test_Port=80;
	
	$fp = @fsockopen($rechner, $Test_Port, $errno, $errstr, 10);
	if (!$fp) {
	    $Log3=FALSE;
	} else {
		$Log3=TRUE;
	    fclose($fp);
	}
	/* HOST-Test ENDE */

	
	echo '<tr>';
	echo '<td width=100 align=center>' . $rechner . '</td>';
	if ($Log1 !== FALSE) {
			echo '<td width=300 align=center>NFS erreichbar</td>';
	}
	else
	{
		echo '<td width=300 align=center><font color=#FF0000>nicht erreichbar</font></td>';
		$fehler = $fehler | $fehl_erreichbar;
	}
	if ($Log2 != FALSE) {
		if (strcmp(substr($Log2[0],0,39),"LoadBalancer<br>Alle Tests erfolgreich.") != 0) {
			echo '<td width=300 align=center><font color=#FF0000>' . $Log2[0] . '</font>';
			$fehler = $fehler | $fehl_lokal;
		}
		else
		{
			echo '<td width=300 align=center>' . $Log2[0];
// 			echo '<td width=300 align=center>' . var_dump($Log2);
		}
		if ($Log2[0] == "200101010101 LoadBalancer\n") {
			$fehler = $fehler | $fehl_db_nicht_erreichbar;
			echo '<sup>*</sup></td>';
		}
	}
	else
	{
		echo '<td width=300 align=center><font color=#FF0000>nicht erreichbar</font></td>';
		$fehler = $fehler | $fehl_erreichbar;
	}

	if ($Log3 != FALSE) {
 		echo '<td width=150 align=center>'.$rechner.':'.$Test_Port.' OK</td>';
		//echo '<td width=150 align=center>' . substr($Log3[10], strpos($Log3[10], 'Server: ') + 8, 8) . '</td>';
// 		echo '<td width=150 align=center>' . var_dump($Log3) . '</td>';
		
		if ($fehler == 0) {
			echo '<td width=50 align=center><img src=/bilder/button_ok.png title=OK></td>';
		}
		else
		{
			echo '<td width=50 align=center><img src=/bilder/warnung.gif title="Kontrolle notwendig"></td>';
		}
	}
	else
	{
		echo '<td width=150 align=center><font size=+1 color=#FF0000>'.$errstr.' ('.$errno.')</font></td>';
		$fehler = $fehler | $fehl_loadbalancer;
		echo '<td width=50 align=center><img src=/bilder/button_cancel.png title=Fehler></td>';
//		echo '<td width=70 align=center><b><font color=#FF0000>Fehler</font></b></td>';
	}
	echo '</tr>';
	//awis_Debug(1,$fehler);
	flush();	
}
	echo '</table></form>';
	
	echo "<p>";
	
echo '<table border=1>';
echo '<th id=FeldBez>Rechner</th><th id=FeldBez>LB1 =&gt; NFS (61001)</th><th id=FeldBez>LB2 =&gt; Lokal (61001)</th><th id=FeldBez>Loadbalancer (80)</th><th id=FeldBez>Status</th>';
foreach($awis_testwebserver as $key => $rechner)
{
	$fehler = 0;
	$Log1 = @fopen("http://".$awis_web_osuser.":".$awis_web_ospwd."@" . $rechner . "/dokumente/vorlagen/BriefpapierATU_DE_Seite_1.pdf",'r');
	$Log2 = @file("http://".$awis_webtest_user.":".$awis_webtest_pwd."@" . $rechner . ":61001/LB2/");
	
	/* HOST-Test START */
	//$Log3 = @file("http://loadbalancer:atu@" . $rechner . "m");
	$errno='';
	$errstr='';
	$Test_Port=80;
	
	$fp = @fsockopen($rechner, $Test_Port, $errno, $errstr, 10);
	if (!$fp) {
	    $Log3=FALSE;
	} else {
		$Log3=TRUE;
	    fclose($fp);
	}
	/* HOST-Test ENDE */

	
	echo '<tr>';
	echo '<td width=100 align=center>' . $rechner . '</td>';
	if ($Log1 !== FALSE) {
			echo '<td width=300 align=center>NFS erreichbar</td>';
	}
	else
	{
		echo '<td width=300 align=center><font color=#FF0000>nicht erreichbar</font></td>';
		$fehler = $fehler | $fehl_erreichbar;
	}
	if ($Log2 != FALSE) {
		if (strcmp(substr($Log2[0],0,39),"LoadBalancer<br>Alle Tests erfolgreich.") != 0) {
			echo '<td width=300 align=center><font color=#FF0000>' . $Log2[0] . '</font>';
			$fehler = $fehler | $fehl_lokal;
		}
		else
		{
			echo '<td width=300 align=center>' . $Log2[0];
// 			echo '<td width=300 align=center>' . var_dump($Log2);
		}
		if ($Log2[0] == "200101010101 LoadBalancer\n") {
			$fehler = $fehler | $fehl_db_nicht_erreichbar;
			echo '<sup>*</sup></td>';
		}
	}
	else
	{
		echo '<td width=300 align=center><font color=#FF0000>nicht erreichbar</font></td>';
		$fehler = $fehler | $fehl_erreichbar;
	}

	if ($Log3 != FALSE) {
 		echo '<td width=150 align=center>'.$rechner.':'.$Test_Port.' OK</td>';
		//echo '<td width=150 align=center>' . substr($Log3[10], strpos($Log3[10], 'Server: ') + 8, 8) . '</td>';
// 		echo '<td width=150 align=center>' . var_dump($Log3) . '</td>';
		
		if ($fehler == 0) {
			echo '<td width=50 align=center><img src=/bilder/button_ok.png title=OK></td>';
		}
		else
		{
			echo '<td width=50 align=center><img src=/bilder/warnung.gif title="Kontrolle notwendig"></td>';
		}
	}
	else
	{
		echo '<td width=150 align=center><font size=+1 color=#FF0000>'.$errstr.' ('.$errno.')</font></td>';
		$fehler = $fehler | $fehl_loadbalancer;
		echo '<td width=50 align=center><img src=/bilder/button_cancel.png title=Fehler></td>';
//		echo '<td width=70 align=center><b><font color=#FF0000>Fehler</font></b></td>';
	}
	echo '</tr>';
	//awis_Debug(1,$fehler);
	flush();	
}
	echo '</table></form>';	
	
	echo "<p>";

echo '<table border=1>';
echo '<th id=FeldBez>Rechner</th><th id=FeldBez>LB1 =&gt; NFS (61001)</th><th id=FeldBez>LB2 =&gt; Lokal (61001)</th><th id=FeldBez>Loadbalancer (80)</th><th id=FeldBez>Status</th>';
foreach($awis_entwickwebserver as $key => $rechner)
{
	$fehler = 0;
	$Log1 = @fopen("http://".$awis_web_osuser.":".$awis_web_ospwd."@" . $rechner . "/dokumente/vorlagen/BriefpapierATU_DE_Seite_1.pdf",'r');
	$Log2 = @file("http://".$awis_webtest_user.":".$awis_webtest_pwd."@" . $rechner . ":61001/LB2/");
	
	/* HOST-Test START */
	//$Log3 = @file("http://loadbalancer:atu@" . $rechner . "m");
	$errno='';
	$errstr='';
	$Test_Port=80;
	
	$fp = @fsockopen($rechner, $Test_Port, $errno, $errstr, 10);
	if (!$fp) {
	    $Log3=FALSE;
	} else {
		$Log3=TRUE;
	    fclose($fp);
	}
	/* HOST-Test ENDE */

	
	echo '<tr>';
	echo '<td width=100 align=center>' . $rechner . '</td>';
	if ($Log1 !== FALSE) {
			echo '<td width=300 align=center>NFS erreichbar</td>';
	}
	else
	{
		echo '<td width=300 align=center><font color=#FF0000>nicht erreichbar</font></td>';
		$fehler = $fehler | $fehl_erreichbar;
	}
	if ($Log2 != FALSE) {
		if (strcmp(substr($Log2[0],0,39),"LoadBalancer<br>Alle Tests erfolgreich.") != 0) {
			echo '<td width=300 align=center><font color=#FF0000>' . $Log2[0] . '</font>';
			$fehler = $fehler | $fehl_lokal;
		}
		else
		{
			echo '<td width=300 align=center>' . $Log2[0];
// 			echo '<td width=300 align=center>' . var_dump($Log2);
		}
		if ($Log2[0] == "200101010101 LoadBalancer\n") {
			$fehler = $fehler | $fehl_db_nicht_erreichbar;
			echo '<sup>*</sup></td>';
		}
	}
	else
	{
		echo '<td width=300 align=center><font color=#FF0000>nicht erreichbar</font></td>';
		$fehler = $fehler | $fehl_erreichbar;
	}

	if ($Log3 != FALSE) {
 		echo '<td width=150 align=center>'.$rechner.':'.$Test_Port.' OK</td>';
		//echo '<td width=150 align=center>' . substr($Log3[10], strpos($Log3[10], 'Server: ') + 8, 8) . '</td>';
// 		echo '<td width=150 align=center>' . var_dump($Log3) . '</td>';
		
		if ($fehler == 0) {
			echo '<td width=50 align=center><img src=/bilder/button_ok.png title=OK></td>';
		}
		else
		{
			echo '<td width=50 align=center><img src=/bilder/warnung.gif title="Kontrolle notwendig"></td>';
		}
	}
	else
	{
		echo '<td width=150 align=center><font size=+1 color=#FF0000>'.$errstr.' ('.$errno.')</font></td>';
		$fehler = $fehler | $fehl_loadbalancer;
		echo '<td width=50 align=center><img src=/bilder/button_cancel.png title=Fehler></td>';
//		echo '<td width=70 align=center><b><font color=#FF0000>Fehler</font></b></td>';
	}
	echo '</tr>';
	//awis_Debug(1,$fehler);
	flush();	
}
	echo '</table></form>';	
	
	echo "<p>";	
	
	echo "Abfrage von " . date("H:i:s") . " Uhr<br>";
	//echo "Fehler: " . $fehler . "<br>";
	

    echo "<br><table border=0><tr><td width=70>Info:</td><td colspan=3>Die Webserver k�nnen vom Hausnetz aus einzeln �ber folgende Wege erreicht werden:</td></tr>";
    echo "<tr valign=top><td>&nbsp;</td><td>&uuml;ber Loadbalancer:</td><td>�ber die Adressen 10.97.181.x</td><td>z.B. 10.97.181.21 f�r svweblx003.server.atu.de</td></tr>";
//    echo "<tr valign=top><td>&nbsp;</td><td>&nbsp;</td><td>�ber die Adressen awisx.awis.atu.de</td><td>z.B. awis1.awis.atu.de</td></tr>";
    echo "<tr valign=top><td>&nbsp;</td><td>&uuml;ber Firewall:</td><td>�ber die Adressen 10.97.133.x</td><td>z.B. 10.97.133.111 f�r svweblx003-lb.server.atu.de</td></tr>";
	echo "</table>";


if ($fehler > 0) {
    echo "<br><table><tr><td width=70>Hinweis!</td><td>Wenn sich die Zeiten nur um 1 Minute unterscheiden, bitte die Anzeige nochmals aktualisieren!</td></tr>";
    echo "<tr><td>&nbsp;</td><td>Es kann bis zu 2 Minuten dauern, bis der Loadbalancer auf �nderungen reagiert!</td></tr>";
	echo "</table>";
	
/*
	if (($fehler & $fehl_db_nicht_erreichbar) == $fehl_db_nicht_erreichbar) {
    	echo "<br><table border=1 width=100%><tr><td colspan=3>Fehler: <sup>*</sup>'200101010101 LoadBalancer' =&gt; Datenbank-Connect hat nicht geklappt!</td></tr>";
		echo "<tr><td colspan=3>Vorgehen: Ist die Datenbank (per Netzwerk) erreichbar?</td></tr>";
    	echo "<tr><td colspan=3>Apache neu starten?</td></tr>";
		echo "</table><br>";
	}
	if (($fehler & $fehl_erreichbar) == $fehl_erreichbar) {
    	echo "<br>Details: Der Webserver ist momentan nicht erreichbar -> bitte den Apache (oder den ganzen Webserver) neu starten!<br>";
	}
	if (($fehler & $fehl_nfs) == $fehl_nfs) {
    	echo "<br><table border=1 width=100%><tr><td colspan=3>Fehler: Die NFS-Zeit h�ngt hinterher </td></tr>";
		echo "<tr><td colspan=3>Vorgehen: Ist die Datei /daten/zugang/awis_time.php auf dem Oracle-Server aktuell?</td></tr>";
    	echo "<tr><td width=50>Nein</td><td colspan=2>Bitte kontrollieren ob das Script /daten/zugang/awis_time_akt.sh auf dem Oracle-Server in Ordnung ist und auch regelm��ig aufgerufen wird (crontab von User root)!</td></tr>";
    	echo "<tr><td>Ja</td><td colspan=2>Betrifft es alle Webserver oder nur einzelne?</td></tr>";
    	echo "<tr><td>&nbsp;</td><td width=50>Alle</td><td>NFS-Server auf dem Oracle-Server kontrollieren und ggf. neu starten</td></tr>";
    	echo "<tr><td>&nbsp;</td><td>Einzelne</td><td>NFS-Client auf dem Webserver kontrollieren und ggf. neu starten</td></tr>";
		echo "</table><br>";
	}
	if (($fehler & $fehl_lokal) == $fehl_lokal) {
    	echo "<br><table border=1 width=100%><tr><td colspan=3>Fehler: Die Lokale Zeit h�ngt hinterher </td></tr>";
		echo "<tr><td colspan=3>Vorgehen: Ist die Datei /daten/zugang/awis_time.php auf dem Webserver aktuell?</td></tr>";
    	echo "<tr><td colspan=3>Bitte kontrollieren ob das Script /daten/zugang/awis_time_akt.sh auf dem Oracle-Server in Ordnung ist und auch regelm��ig aufgerufen wird (crontab von User root)!</td></tr>";
		echo "</table><br>";
	}
	if ((($fehler & $fehl_erreichbar) != $fehl_erreichbar) && (($fehler & $fehl_loadbalancer) == $fehl_loadbalancer)) {
    	echo "<br><table border=1 width=100%><tr><td colspan=3>Fehler: Die LB-Zeiten werden angezeigt, aber der Webserver wird";
		echo " trotzdem nicht vom Loadbalancer angeboten</td></tr>";
		echo "<tr><td colspan=3>Vorgehen: Sind die LB-Zeiten unterschiedlich?";
    	echo "<tr><td width=50>Ja</td><td colspan=2>Bitte zuerst dieses Problem abstellen. Der Loadbalancer nimmt den Server automatisch vom Netz!</td></tr>";
		echo "<tr><td width=50>Nein</td><td colspan=2>Ist der Webserver ohne Loadbalancer erreichbar und funktioniert hier normal? (Port 80 und 61001) <br>";
    	echo "<tr><td>&nbsp;</td><td width=50>Ja</td><td>Netzwerk-Abteilung informieren!</td></tr>";
    	echo "<tr><td>&nbsp;</td><td width=50>Nein</td><td>Apache (bzw. den Webserver) neu starten</td></tr>";
		echo "</table><br>";
	}
*/
}

print "<hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./index.php'>";

flush();	
clearstatcache();
//$bla = mktime(09,34,05,'JUL',15,2004);
//echo "<br>" . $bla;
//echo "<br>" . date("M-d-Y", $bla);
echo "<p>Auszug aus " . $lblog . " (" . filesize($lblog) . " Byte): &nbsp;";

echo '<form name=frmAnzeige method=get action=' . $_SERVER["PHP_SELF"] . '>';

if (!isset($_GET["alles"]) || $_GET["alles"] != "ja") {
	echo '<input type=submit value="Alles anzeigen">';
	echo '<input type="hidden" name="alles" value="ja">';
}else{
	echo '<input type=submit value="Kurzversion anzeigen">';
	echo '<input type="hidden" name="alles" value="nein">';
}
echo "</form>";

echo "<p>";

setlocale(LC_TIME, "en_US");
$aktMonat = strtoupper(strftime("%b", time()));
$vorMonat = strtoupper(strftime("%b", mktime(0, 0, 0, date("m") - 1, 1, date("Y"))));
echo "<br>F�r die Monate " . $vorMonat . " und " . $aktMonat . "<br><br>";

$log_array = file($lblog);
for($i=count($log_array)-1;$i>-1;$i--)
{
	$arr1 = explode(": ", $log_array[$i]);
	
	if(!isset($_GET["alles"]) || $_GET["alles"] != "ja") {
		if ((isset($arr1[2]) && $arr1[2] == "bad ARP packet received \n") || (isset($arr1[2]) && $arr1[2] == "Internal Messages Dropped.  \n")) {
			continue;
		}
		if (strcmp($aktMonat, strtoupper(substr($arr1[0], 0, 3))) != 0 && strcmp($vorMonat, strtoupper(substr($arr1[0], 0, 3))) != 0 ) {
		    break;
		}
	}
	echo $log_array[$i];
	echo "<br>";
}
?>
</body>
</html>
