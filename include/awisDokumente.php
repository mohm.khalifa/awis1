<?php
/**
 * Diese Klasse ist f�r die Verwaltung der im AWIS gespeicherten Dokumente
 *
 * @author Sacha Kerres
 * @version 20071030
 * @copyright Sacha Kerres, ATU
 * @package AWIS
 *
 */
require_once('awisDatenbank.inc');
require_once 'awisBenutzer.inc';

/**
 * Class awisDokumente
 * @deprecated awisDokument benutzen!
 */
class awisDokumente
{
	var $_DB;
	var $_AWISBenutzer;


	function awisDokumente()
	{
		global $AWISBenutzer;

        if($AWISBenutzer instanceof awisBenutzer === false){
            $AWISBenutzer = awisBenutzer::Init();
        }

		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();

		$this->_AWISBenutzer = $AWISBenutzer;
	}

	/**
	 * Speichert ein Dokument
	 * Die Datei wird �ber $_FILES[] ausgelesen
	 *
	 * @param int $DOCKEY (0 fuer neu)
	 * @param string $FELDNAME (enth�lt den Namen des Inputfelds)
	 * @param string $Bezeichnung
	 * @param string $Datum
	 * @param string $Bemerkung
	 * @param int $Eigentuemer
	 * @param array $Zuordnungen (XXX_KUERZEL=>ID)
	 */
	function DokumentSpeichern($DOCKEY, $FELDNAME, $Bezeichnung, $Datum, $Bemerkung, $Eigentuemer, $Zuordnungen)
	{
		global $AWISBenutzer;

		$DateiInfo = pathinfo($_FILES['DOC']['name']);
		$Erweiterung = strtolower($DateiInfo['extension']);

		if(isset($_FILES['DOC']['error']) AND $_FILES['DOC']['error']==1)
		{
			awisErrorMailLink('awisDokumente.php',1,'Dokument kann nicht erkannt werden','200812120733');
			die();
		}


		if($DOCKEY == 0)		// Neues Dokument
		{
			$SQL = 'INSERT INTO Dokumente';
			$SQL .= '(DOC_DATUM,DOC_BEZEICHNUNG,DOC_BEMERKUNG,DOC_XBN_KEY,DOC_ERWEITERUNG';
			$SQL .= ',DOC_USER,DOC_USERDAT)VALUES (';
			$SQL .= ' ' . $this->_DB->WertSetzen('DOC','D',$Datum,true);
			$SQL .= ',' . $this->_DB->WertSetzen('DOC','T',$Bezeichnung,true);
			$SQL .= ',' . $this->_DB->WertSetzen('DOC','T',$Bemerkung,true);
			$SQL .= ',' . $this->_DB->WertSetzen('DOC','Z',$Eigentuemer,true);
			$SQL .= ',' . $this->_DB->WertSetzen('DOC','T',$Erweiterung,true);
			$SQL .= ',' . $this->_DB->WertSetzen('DOC','T',$this->_AWISBenutzer->BenutzerName(),true) ;
			$SQL .= ',SYSDATE';
			$SQL .= ')';

            $this->_DB->Ausfuehren($SQL,'',false,$this->_DB->Bindevariablen('DOC'));

			$SQL = 'SELECT seq_DOC_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $this->_DB->RecordSetOeffnen($SQL);
			$DOCKEY = $rsKey->FeldInhalt('KEY');

			// Zuordungen speichern
			foreach ($Zuordnungen as $Zuordnung=>$Wert)
			{
				$SQL = 'INSERT INTO DOKUMENTZUORDNUNGEN';
				$SQL .= '(DOZ_DOC_KEY, DOZ_XXX_KEY, DOZ_XXX_KUERZEL, DOZ_USER,DOZ_USERDAT)VALUES (';
				$SQL .= ' ' . $this->_DB->WertSetzen('DOC','Z',$DOCKEY,false);
				$SQL .= ',' .  $this->_DB->WertSetzen('DOC','Z',$Wert,true);
				$SQL .= ',' .  $this->_DB->WertSetzen('DOC','T',$Zuordnung,true);
				$SQL .= ',' . $this->_DB->WertSetzen('DOC','T',$this->_AWISBenutzer->BenutzerName()) ;
				$SQL .= ',SYSDATE';
				$SQL .= ')';

                $this->_DB->Ausfuehren($SQL,'',false,$this->_DB->Bindevariablen('DOC'));

            }

			if(is_uploaded_file($_FILES['DOC']['tmp_name']))
			{
				$Pfad = $AWISBenutzer->ParameterLesen('Dokumenten-Stammverzeichnis',false);
				$Pfad .= '/'.intval($DOCKEY/500);
				if(!is_dir($Pfad))
				{
					mkdir($Pfad);
				}

				if(is_dir($Pfad))
				{
					$DateiInfo = pathinfo($_FILES['DOC']['name']);

					$Pfad .= '/'.$DOCKEY.'.'.strtolower($DateiInfo['extension']);

			   		if(!move_uploaded_file($_FILES['DOC']["tmp_name"], $Pfad))
			   		{
			   			echo '###FEHLER###';
			   		}
			   		else 
			   		{
			   		    file_put_contents('/var/log/awis/awisDokumente.log', date('c').';'.$this->_AWISBenutzer->BenutzerName(1).';UPLOAD;'.$Pfad);
			   		}
				}
			}
		}
		else 	// Aktualisieren
		{
			$SQL = 'UPDATE Dokumente ';
			$SQL .= ' SET DOC_DATUM='.$this->_DB->WertSetzen('DOC','D',$Datum,true);
			$SQL .= ',DOC_BEZEICHNUNG=' . $this->_DB->WertSetzen('DOC','T',$Bezeichnung,true);
			$SQL .= ',DOC_BEMERKUNG=' . $this->_DB->WertSetzen('DOC','T',$Bemerkung,true);
			$SQL .= ',DOC_USER=' . $this->_DB->WertSetzen('DOC','T',$this->_AWISBenutzer->BenutzerName());
			$SQL .= ',DOC_USERDAT=SYSDATE';
			$SQL .= ' WHERE DOC_KEY = '. $this->_DB->WertSetzen('DOC','N0',$DOCKEY);

            $this->_DB->Ausfuehren($SQL,'',false,$this->_DB->Bindevariablen('DOC'));
        }

		return $DOCKEY;
	}





	/**
	 * Liefert den ARRAY zu einem Dokument oder FALSE, wenn Sie nicht existiert
	 *
	 * @param int $DOCKEY
	 * @param string $DOCTYP
	 * @return array
     *
     * @deprecated
	 */
	function DokumentPfad($DOCKEY, $DOCTYP='')
	{
		global $AWISBenutzer;
		$Erg = Array();

		$SQL = 'SELECT *';
		$SQL .= ' FROM Dokumente';
		$SQL .= ' INNER JOIN Dokumentzuordnungen ON DOC_KEY = DOZ_DOC_KEY';
		$SQL .= ' WHERE DOC_KEY = '.$this->_DB->WertSetzen('DOC','N0',intval($DOCKEY));
		$SQL .= ' AND DOZ_XXX_KUERZEL='.$this->_DB->WertSetzen('DOC','T',$DOCTYP);

		$rsDOC =  $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('DOC'));
		if($rsDOC->FeldInhalt('DOC_KEY') != ''){
			$Pfad = $AWISBenutzer->ParameterLesen('Dokumenten-Stammverzeichnis',false);
			$Pfad .= '/'.(intval($rsDOC->FeldInhalt('DOC_KEY')/500));
			if(!is_dir($Pfad))
			{
				echo '###PFAD###';
				return false;
			}
			else
			{
				$Pfad .= '/'.$DOCKEY.'.'.strtolower($rsDOC->FeldInhalt('DOC_ERWEITERUNG'));
		   		if(!is_file($Pfad))
		   		{
		   			echo '###DATEI###'.$Pfad.'###';
		   			return false;
		   		}

		   		$Erg['Pfad']=$Pfad;
		   		$Erg['Erweiterung']=$rsDOC->FeldInhalt('DOC_ERWEITERUNG');
		   		$Erg['Eigentuemer']=$rsDOC->FeldInhalt('DOC_XBN_KEY');
		   		$Erg['Bezeichnung']=$rsDOC->FeldInhalt('DOC_BEZEICHNUNG');
			}
			return $Erg;
		}
		else
		{
			return false;
		}
	}

	/**
	 * L�scht ein Dokument aund alle Bez�ge
	 *
	 * @param int $ID
	 */
	function DokumentLoeschen($ID)
	{
	    global $AWISBenutzer;
		$SQL = 'SELECT *';
		$SQL .= ' FROM Dokumente ';
		$SQL .= ' WHERE DOC_KEY = '.$this->_DB->WertSetzen('DOC','N0',$ID);
        $rsDOC =  $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('DOC'));

		if($rsDOC->FeldInhalt('DOC_KEY')!='')
		{
			$SQL = 'DELETE ';
			$SQL .= ' FROM Dokumente';
			$SQL .= ' WHERE DOC_KEY = '.$this->_DB->WertSetzen('DOC','N0',$ID);
            $this->_DB->Ausfuehren($SQL,'',false,$this->_DB->Bindevariablen('DOC'));

            $Pfad = $AWISBenutzer->ParameterLesen('Dokumenten-Stammverzeichnis',false);
            $Pfad .= '/'.(intval($rsDOC->FeldInhalt('DOC_KEY')/500));
            $Pfad .= '/'.$rsDOC->FeldInhalt('DOC_KEY').'.'.strtolower($rsDOC->FeldInhalt('DOC_ERWEITERUNG'));
            if(is_file($Pfad))
            {
                unlink($Pfad);
            }

		}
	}
}
?>