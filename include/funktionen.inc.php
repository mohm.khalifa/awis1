<?php

/* Funktionensammlung */

function maxSpaltenbreite($pdfp, $arr)
{

	for ($i=0; $i<count($arr); $i++)
	{
		$spb = 0;
		for ($j=0; $j<count($arr[key($arr)]); $j++)
		{
//			$spb = max($spb, pdf_stringwidth($pdfp, $arr[key($arr)][$j]));
			$spb = max($spb, strlen($arr[key($arr)][$j]));
		}
		$erg[key($arr)] = $spb;
//		print key($arr).": ".$spb."<br>";
		next($arr);
	}
	print_r($erg);
	print "<br>";
	return $erg;

}

function calcColsize($arr, $fontgroesse = 7)
{

	for ($i=0; $i<count($arr); $i++)
	{
		$erg[] = current($arr) * $fontgroesse + 3;
print key($arr). " - ".current($arr)."<br>";
		next($arr);
	}
	print_r($erg);
	print "<br>";
	return $erg;

}

function creaLatex($rsDaten, $strDateiname)
{
	global $awisRSZeilen;
	
/*	Um nicht immer die alten PDF-Dokumente zu sehen, sollte im IE 
	unter Internetoptionen -> Temp. Internetdateien -> Neuere Versionen suchen => Bei jedem Zugriff auf Seite
	eingestellt werden
*/

	
	# Dieses Array dient zum Maskieren der Sonderzeichen, falls welche in Textfeldern vorhanden sind.
	$wechsel = array ("_" => "\_", "&" => "\&");

	print "Erstelle Listen-Datei:<br>";
	flush();
	$filename = $strDateiname . ".tex";
	$auxname = "/daten/web/test/" . $strDateiname . ".aux";

	# Cache leeren, damit file_exists richtig arbeitet!
	clearstatcache();
	if (file_exists($auxname))
	{
		if (unlink($auxname))
		{
			echo "Aux-Datei gel�scht!<br>";
		}
		else
		{
			echo "Aux-Datei nicht gel�scht!<br>";
		}
	
	}

	$fp = fopen($filename, "w");

	// Dokumenten-Vorspamm
	fwrite ($fp, sprintf("%s \n","\documentclass[ngerman]{article}"));
	fwrite ($fp, sprintf("%s \n","\usepackage[T1]{fontenc}"));
	fwrite ($fp, sprintf("%s \n","\usepackage[latin1]{inputenc}"));
	fwrite ($fp, sprintf("%s \n","\usepackage{times}"));
	fwrite ($fp, sprintf("%s \n","\usepackage{geometry}"));
	fwrite ($fp, sprintf("%s \n","\geometry{verbose,a4paper,tmargin=0cm,bmargin=0cm,lmargin=0cm,rmargin=0cm}"));
	fwrite ($fp, sprintf("%s \n","\usepackage{longtable}"));
	fwrite ($fp, sprintf("%s \n","\makeatletter"));
	fwrite ($fp, sprintf("%s \n","\usepackage{babel}"));
	fwrite ($fp, sprintf("%s \n","\makeatother"));
	fwrite ($fp, sprintf("%s \n","\usepackage{graphicx}"));

	fwrite ($fp, sprintf("%s \n","\begin{document}"));

	// Tabellenanfang
	fwrite ($fp, sprintf("%s \n","\begin{longtable}{|r|l|l|l|l|l|}"));
	
	fwrite ($fp, sprintf("%s \n","\hline"));
	fwrite ($fp, sprintf("%s \n","\multicolumn{6}{|c|}{\\textbf{Dies ist eine Filialliste}}") . "\\\\");
	#fwrite ($fp, sprintf("%s \n","\includegraphics{/daten/web/bilder/warnung}"));


	fwrite ($fp, sprintf("%s \n","\hline"));
	fwrite ($fp, sprintf("%s \n","\hline"));
	fwrite ($fp, sprintf("%s \n","Filnr&"));
	fwrite ($fp, sprintf("%s \n","Name&"));
	fwrite ($fp, sprintf("%s \n","Strasse&"));
	fwrite ($fp, sprintf("%s \n","Plz&"));
	fwrite ($fp, sprintf("%s \n","Ort&"));
	fwrite ($fp, sprintf("%s \n","Ortsteil\\\\"));
	fwrite ($fp, sprintf("%s \n","\hline"));
	fwrite ($fp, sprintf("%s \n","\endfirsthead"));

	// Tabellenkopf
	fwrite ($fp, sprintf("%s \n","\hline "));
	fwrite ($fp, sprintf("%s \n","Filnr&"));
	fwrite ($fp, sprintf("%s \n","Name&"));
	fwrite ($fp, sprintf("%s \n","Strasse&"));
	fwrite ($fp, sprintf("%s \n","Plz&"));
	fwrite ($fp, sprintf("%s \n","Ort&"));
	fwrite ($fp, sprintf("%s \n","Ortsteil\\\\"));
	fwrite ($fp, sprintf("%s \n","\hline"));
	fwrite ($fp, sprintf("%s \n","\endhead"));

	for($i=0;$i<$awisRSZeilen;$i++)
	{
		$bez = strtr($rsDaten["FIL_BEZ"][$i],$wechsel) ;
		fwrite ($fp,sprintf("%s ",$rsDaten["FIL_ID"][$i] . " & "));
		fwrite ($fp,sprintf("%s ",$bez . " & "));
//		fwrite ($fp,sprintf("%s ",$rsDaten["FIL_BEZ"][$i] . " & "));
		fwrite ($fp,sprintf("%s ",$rsDaten["FIL_STRASSE"][$i] . "&"));
		fwrite ($fp,sprintf("%s ",$rsDaten["FIL_PLZ"][$i] . "&"));
		fwrite ($fp,sprintf("%s ",$rsDaten["FIL_ORT"][$i] . "&"));
		fwrite ($fp,sprintf("%s ",$rsDaten["FIL_ORTSTEIL"][$i] . "\\\\"));
		fwrite ($fp,sprintf("%s \n","\hline"));
	}

	fwrite ($fp,sprintf("%s \n","\end{longtable}"));
	fwrite ($fp,sprintf("%s \n","\end{document}"));
	
	//fwrite($fp, "tescht");
	fclose($fp);
	
	print "Latexe Datei:<br>";
	flush();
	$i = 0;
	do
	{
		$i += 1;
		unset ($output);
		exec ("pdflatex tabellentest.tex | grep 'Rerun LaTeX'", $output);
	}
	while (!empty($output) and $i < 5);
	print $i . " Durchl�ufe ben�tigt!<br>";
	print count($output) . " Count!<br>";
#	print_r($output);

}
?>
