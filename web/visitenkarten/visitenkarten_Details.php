<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('VSK','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht35000 = $AWISBenutzer->HatDasRecht(35000);
	
	//var_dump($_POST);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Visitenkarten'));
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		//$Param['ORDER'] = 'vsk_filid';
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
	 $Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}
	
	
	if($Recht35000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
	       //var_dump($_POST['sucVSK_FILID']);
	    if (isset($_POST['txtVSK_FILID']) && $_POST['txtVSK_FILID'] != "") //bei Filialzugriff da Hiddentextfield.
	    {
	        $Param['VSK_FILID'] = $_POST['txtVSK_FILID'];
	    }
	    elseif(isset($_POST['sucVSK_FILID']) && $_POST['sucVSK_FILID'] != "") //bei regul�ren Zugriff das Textfeld benutzen. 
	    {
	       $Param['VSK_FILID'] = $_POST['sucVSK_FILID'];
	    }
	 
	       
	}
	elseif(isset($_GET['VSK_KEY'])) //Von Detail zu Unterdetail
	{
		$AWIS_KEY1 = $_GET['VSK_KEY'];
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
	    include('./visitenkarten_speichern.php');
	}
	
	$SQL ='SELECT *';
    $SQL .=' FROM';
    $SQL .='   (SELECT *';
    $SQL .='   FROM visitenkarten vsk';
    $SQL .='   inner JOIN visitenkartenabfragen vsa';
    $SQL .='   ON vsk.VSK_VSA_ID                        = vsa.vsa_key';
    $SQL .='   AND TO_CHAR(vsa.vsa_datumvon,\'DD.MM.YYYY\') <= TO_CHAR(sysdate,\'DD.MM.YYYY\')';
    $SQL .='   AND TO_CHAR(vsa.vsa_datumbis,\'DD.MM.YYYY\') >= TO_CHAR(sysdate,\'DD.MM.YYYY\')';
    $SQL .='   )';
  
    
	$Bedingung = '';
	
	//Bedinnung erstellen VSK_FILID
	
	if (isset($Param['VSK_FILID']) && $Param['VSK_FILID'] != "" ) 
	{
	    $Bedingung .= 'AND VSK_FILID = ' . $Param['VSK_FILID'];
	}
	
	
	
	if ($Bedingung != '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;

	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
		if (! isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x']))
		{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
		}
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
//	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	$Form->DebugAusgabe(1, $SQL);
	//echo $SQL;
	$rsQMP = $DB->RecordSetOeffnen($SQL);
	
	
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['FilID'] = 50;
	$FeldBreiten['VORNAME'] = 150;
	$FeldBreiten['NACHNAME'] = 200;
	$FeldBreiten['VSK_PER_NR'] = 80;
	
	$Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./visitenkarten_Main.php?cmdAktion=Details>");
	$Form->Formular_Start();
	
	
	if($AWIS_KEY1 == '')
	{
		if (($rsQMP->AnzahlDatensaetze() > 0))
		{
			$Form->ZeileStart();
			$Link = './visitenkarten_Main.php?cmdAktion=Details&Sort=VSK_PERSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VSK_PERSNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VSK']['VSK_PERSNR'], $FeldBreiten['VSK_PER_NR'], '', $Link);
			
			$Link = './visitenkarten_Main.php?cmdAktion=Details&Sort=VSK_VORNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VSK_VORNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VSK']['VSK_VORNAME'], $FeldBreiten['VORNAME'], '', $Link);
			
			$Link = './visitenkarten_Main.php?cmdAktion=Details&Sort=VSK_NACHNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VSK_NACHNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VSK']['VSK_NACHNAME'], $FeldBreiten['NACHNAME'], '', $Link);
				
				
			$Form->ZeileEnde();
			$DS = 0;	// f�r Hintergrundfarbumschaltung
			while(! $rsQMP->EOF())
			{
				$Form->ZeileStart();

				$Link = './visitenkarten_Main.php?cmdAktion=Details&VSK_KEY=0'.$rsQMP->FeldInhalt('VSK_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$TTT = $rsQMP->FeldInhalt('VSK_PER_NR');
				$Form->Erstelle_ListenFeld('VSK_PER_NR', $rsQMP->FeldInhalt('VSK_PER_NR'), 0, $FeldBreiten['VSK_PER_NR'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
				
				$TTT = $rsQMP->FeldInhalt('VSK_VORNAME');
				$Form->Erstelle_ListenFeld('VSK_VORNAME', $rsQMP->FeldInhalt('VSK_VORNAME'), 0, $FeldBreiten['VORNAME'], false, ($DS%2), '', '', 'T', 'L', $TTT);
				
				$TTT = $rsQMP->FeldInhalt('VSK_NACHNAME');
				$Form->Erstelle_ListenFeld('VSK_NACHNAME', $rsQMP->FeldInhalt('VSK_NACHNAME'), 0, $FeldBreiten['NACHNAME'], false, ($DS%2), '', '', 'T', 'L', $TTT);
				
				
				$Form->ZeileEnde();
				$DS++;
				$rsQMP->DSWeiter();
			}
			$Link = './visitenkarten_Main.php?cmdAktion=Details';
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		}
		else 
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
			$Form->ZeileEnde();
		}
	}
	else 
	{
	    //�ndernrecht
	    if(($Recht35000&2)==2)	
	    {
	        $Aendernrecht = true; 
	    }
	    else 
	    {
	        $Aendernrecht = false; 
	    }
	    
	    //Feldbreiten 
	    $FeldBreiten = array();
	    $FeldBreiten['VORNAME'] = 40;
	    $FeldBreiten['NACHNAME'] = 40;
	    $FeldBreiten['TAETIGKEIT'] = 40;
	    $FeldBreiten['TELEFONNUMMER'] = 40;
	    $FeldBreiten['FAXNUMMER'] = 40;
	    $FeldBreiten['FILIALBEZEICHNUNG'] = 60;
	    $FeldBreiten['FILIALSTRASSE'] = 60;
	    $FeldBreiten['FILIALORT'] = 60;
	    $FeldBreiten['FILIALPLZ'] = 60;
	    $FeldBreiten['EMAIL'] = 40;
	     
		$SQL = 'SELECT *';
		$SQL .= 'FROM visitenkarten';
		$SQL .= ' WHERE VSK_KEY='.$AWIS_KEY1;
		
		$rsDetails = $DB->RecordSetOeffnen($SQL);
		
		$Felder[] = array(
		    'Style' => 'font-size:smaller;',
		    'Inhalt' => "<a href=./visitenkarten_Main.php?cmdAktion=Details"   .  "><img border=0 src=/bilder/cmd_trefferliste.png></a>"
		);
		$Form->InfoZeile($Felder, '');
		
		$Form->Erstelle_HiddenFeld('VSK_KEY', $AWIS_KEY1);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('',140);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_ORIGINAL'] . ':',250,'font-weight:bolder');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_AENDERN'] . ':',250,'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_VORNAME'] . ':',140);
		//$Form->Erstelle_TextFeld('VSK_VORNAME',$Form->Format('T',$rsDetails->FeldInhalt('VSK_VORNAME')), $FeldBreiten['VORNAME'], $FeldBreiten['VORNAME'],FALSE, '','', '','', 'L');
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_VORNAME'),250);
		$Form->Erstelle_TextFeld('VSK_VORNAME',$Form->Format('T',$rsDetails->FeldInhalt('VSK_VORNAME')), $FeldBreiten['VORNAME'], $FeldBreiten['VORNAME'],$Aendernrecht, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_NACHNAME'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_NACHNAME'),250);
		$Form->Erstelle_TextFeld('VSK_NACHNAME',$Form->Format('T',$rsDetails->FeldInhalt('VSK_NACHNAME')), $FeldBreiten['NACHNAME'], $FeldBreiten['NACHNAME'],$Aendernrecht, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_TAETIGKEIT'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_TAETIGKEIT'),250);
		$Daten = explode('|',$AWISSprachKonserven['VSK']['VSK_SELECTTAETIGKEIT']);
		$Form->Erstelle_SelectFeld('VSK_TAETIGKEIT','',150,true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		//$Form->Erstelle_TextFeld('VSK_TAETIGKEIT',$Form->Format('T',$rsDetails->FeldInhalt('VSK_TAETIGKEIT')), $FeldBreiten['TAETIGKEIT'], $FeldBreiten['TAETIGKEIT'],$Aendernrecht, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_TELEFON'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_TELEFON'),250);
		$Form->Erstelle_TextFeld('VSK_TELEFON',$Form->Format('T',$rsDetails->FeldInhalt('VSK_TELEFON')), $FeldBreiten['TELEFONNUMMER'], $FeldBreiten['TELEFONNUMMER'],$Aendernrecht, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FAX'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_FAX'),250);
		$Form->Erstelle_TextFeld('VSK_FAX',$Form->Format('T',$rsDetails->FeldInhalt('VSK_FAX')), $FeldBreiten['FAXNUMMER'], $FeldBreiten['FAXNUMMER'],$Aendernrecht, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FILBEZEICHNUNG'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_FILBEZEICHNUNG'),250);	
		$Form->Erstelle_TextFeld('VSK_FILBEZEICHNUNG',$Form->Format('T',$rsDetails->FeldInhalt('VSK_FILBEZEICHNUNG')), $FeldBreiten['FILIALBEZEICHNUNG']+300, $FeldBreiten['FILIALBEZEICHNUNG']+300,false, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FILSTRASSE'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_FILSTRASSE'),250);
		$Form->Erstelle_TextFeld('VSK_FILSTRASSE',$Form->Format('T',$rsDetails->FeldInhalt('VSK_FILSTRASSE')), $FeldBreiten['FILIALSTRASSE']+300, $FeldBreiten['FILIALSTRASSE']+300,false, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FILORT'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_FILORT'),250);	
		$Form->Erstelle_TextFeld('VSK_FILORT',$Form->Format('T',$rsDetails->FeldInhalt('VSK_FILORT')), $FeldBreiten['FILIALORT']+300, $FeldBreiten['FILIALORT']+300,false, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_FILPLZ'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_FILPLZ'),250);
		$Form->Erstelle_TextFeld('VSK_FILPLZ',$Form->Format('T',$rsDetails->FeldInhalt('VSK_FILPLZ')), $FeldBreiten['FILIALPLZ'], $FeldBreiten['FILIALPLZ'],false, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['VSK']['VSK_EMAIL'] . ':',140);
		$Form->Erstelle_TextLabel($rsDetails->FeldInhalt('VSK_EMAIL'),250);
		$Form->Erstelle_TextFeld('VSK_EMAIL',$Form->Format('T',$rsDetails->FeldInhalt('VSK_EMAIL')), $FeldBreiten['EMAIL'], $FeldBreiten['EMAIL'],$Aendernrecht, '','', '','', 'L');
		$Form->ZeileEnde();
		
	}
	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if (isset($_GET['VSK_KEY'])) //Nur in der Unterdetailmaske speichern
	{
	    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_Visitenkarten',serialize($Param));
	
	
	
	
	

	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}



?>