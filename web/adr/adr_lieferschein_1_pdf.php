<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

//Werden ben�tigt f�r EAN
require_once('register.inc.php');
require_once('db.inc.php');
require_once('sicherheit.inc.php');

require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('ADK','%');		
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Wort','Absender');
	$TextKonserven[]=array('Wort','Empfaenger');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht4300 = $AWISBenutzer->HatDasRecht(4300);
	if($Recht4300==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');

	if(isset($_GET['ADK_KEY']))
	{
		$AWIS_KEY1 = floatval($_GET['ADK_KEY']);
	}	
	
	$Spalte = 20;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	
	global $PunkteGesamt;
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Lieferschein');
	
	//Pr�fen, ob Daten f�r Block 1 zu Drucken sind
	//ho, 20120802	bei "Nichtchlorierte Maschinen-, Getriebe- und Schmier�le auf Mineral�lbasis"
	//				keine Ausdrucke -> ADS_LGACODE<>'130205' AND ADS_EWCCODE<>'130205*'
	
	$SQL = 'SELECT * FROM ADRSTAMM';
	$SQL.= ' INNER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY';
	$SQL.= ' WHERE ADP_ADK_KEY=0'.$AWIS_KEY1;
	$SQL.= ' AND ADS_LAGACODE<>\'130205\' AND ADS_EWCCODE<>\'130205*\'';
	$SQL.= ' AND ((ADS_ADRNR >= 11 AND ADS_ADRNR <= 15) OR (ADS_ADRNR >= 21 AND ADS_ADRNR <= 25) OR (ADS_ADRNR >= 41 AND ADS_ADRNR <= 45))';
	$rsADK=$DB->RecordSetOeffnen($SQL);	
	
	if ($rsADK->AnzahlDatensaetze()>0)
	{
		$PunkteGesamt=0;
		$Zeile = _Erzeuge_Seite_Lieferschein(1);
		$Zeile = _Erzeuge_Inhalt_Lieferschein($Zeile);
	
		/*
		$PunkteGesamt=0;
		$PunkteGesamt=0;
		$Zeile = _Erzeuge_Seite_Lieferschein(1);
		$Zeile = _Erzeuge_Inhalt_Lieferschein($Zeile);
		*/
	}

	$Zeile = _Erzeuge_Seite_Abfallschein(1);
	$Zeile = _Erzeuge_Inhalt_Abfallschein($Zeile);	
	
	/*
	$Zeile = _Erzeuge_Seite_Abfallschein(1);
	$Zeile = _Erzeuge_Inhalt_Abfallschein($Zeile);	
	*/
		
	$Ausdruck->Anzeigen();
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}

function _Erzeuge_Seite_Lieferschein($Lieferscheinseite)
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;

	$SQL = 'SELECT ';
	$SQL .= ' ADK_KEY';
	$SQL .= ',ADK_FIL_ID';
	$SQL .= ',ADK_ART';
	$SQL .= ',ADK_VORGANGNR';
	$SQL .= ',ADK_STATUS';
	$SQL .= ',ADK_DATUMERSTELLUNG';
	$SQL .= ',TO_CHAR(ADK_DATUMABSCHLUSS,\'DD.MM.YYYY\') as ADK_DATUMABSCHLUSS';
	$SQL .= ',TO_CHAR(ADK_DATUMABSCHLUSS, \'MM\') as ADK_DATUMABSCHLUSS_MONAT';
	$SQL .= ',TO_CHAR(ADK_DATUMABSCHLUSS, \'YY\') as ADK_DATUMABSCHLUSS_JAHR';
	$SQL .= ',ADK_ERZEUGERNR';
	$SQL .= ',ADK_BEFOERDERERNR';
	$SQL .= ',ADK_ENTSORGERNR';
	$SQL .= ',ADK_LAGERKZ';
	$SQL .= ',ADK_RFK_KEY';
	$SQL .= ',ADK_USER';
	$SQL .= ',ADK_USERDAT';
	$SQL .=' FROM ADRKOPF ';
	$SQL .=' WHERE ADK_KEY=0'.$AWIS_KEY1;

	$rsADKKopf = $DB->RecordSetOeffnen($SQL);

	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	//$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	//$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,$rsADKKopf->FeldInhalt('ADK_DATUMABSCHLUSS', 'D'),0,0,'L',0);

	$Ausdruck->_pdf->SetFont('Arial','B',18);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['ADK']['txt_Lieferschein'],0,0,'L',0);				
	$Zeile+=20;		
	
	$Ausdruck->_pdf->SetFont('Arial','B',12);
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
	$Ausdruck->_pdf->cell(60,5,$AWISSprachKonserven['ADK']['txt_Uebernahmescheinnummer'].':',0,0,'L',0);

	$Monat = $rsADKKopf->FeldInhalt('ADK_DATUMABSCHLUSS_MONAT');
	$Jahr = $rsADKKopf->FeldInhalt('ADK_DATUMABSCHLUSS_JAHR');
	 
	$EAN='1'.str_pad($rsADKKopf->FeldInhalt('ADK_FIL_ID'),4,'0',STR_PAD_LEFT).$Jahr.$Monat.str_pad($rsADKKopf->FeldInhalt('ADK_VORGANGNR'),3,'0',STR_PAD_LEFT);
	echo $EAN;
	$EAN_CLASS = new BarcodeEncode_EAN13();
	$EAN_SAVE = '';
	if ($EAN_CLASS->Validate($EAN))
	{
		$EAN_CODE = $EAN_CLASS->Enc($EAN);
		$EAN_SAVE = $EAN_CODE->iData;
	}
			
	$Ausdruck->_pdf->cell(30,5,$EAN_SAVE,0,0,'L',0);		
	
	$EAN2='2'.str_pad($rsADKKopf->FeldInhalt('ADK_FIL_ID'),4,'0',STR_PAD_LEFT).$Jahr.$Monat.str_pad($rsADKKopf->FeldInhalt('ADK_VORGANGNR'),3,'0',STR_PAD_LEFT);
	if ($EAN_CLASS->Validate($EAN2))
	{
		$EAN_CODE = $EAN_CLASS->Enc($EAN2);
		$EAN_SAVE = $EAN_CODE->iData;
	}
				
	$Ausdruck->_pdf->Image(Erzeuge_EAN13($EAN2,2),$Spalte+125,$Zeile-8,50,10,'png');	
	$Ausdruck->_pdf->SetXY($Spalte+150,$Zeile+2);
	$Ausdruck->_pdf->SetFont('Courier','B',9);
	//$Ausdruck->_pdf->SetCharSpacing(1,0);
	$Ausdruck->_pdf->Cell(30,6,$EAN_SAVE,0,0,'L',0);	
	$Ausdruck->_pdf->SetFont('Arial','',10);
	//$Ausdruck->_pdf->SetCharSpacing(0,0);
	
	$Zeile+=10;	
	
	if ($Lieferscheinseite == 1)
	{
		$SQL = 'SELECT FIL_ID, FIL_STRASSE, FIL_PLZ, FIL_ORT FROM FILIALEN WHERE FIL_ID = '.$DB->FeldInhaltFormat('Z',$rsADKKopf->FeldInhalt('ADK_FIL_ID'));
		$rsFil = $DB->RecordSetOeffnen($SQL);				
		
		$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=912 AND FIF_FIL_ID ='.$DB->FeldInhaltFormat('Z',$rsADKKopf->FeldInhalt('ADK_FIL_ID'));
		$rsFilinfoName1 = $DB->RecordSetOeffnen($SQL);
		
		$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=913 AND FIF_FIL_ID ='.$DB->FeldInhaltFormat('Z',$rsADKKopf->FeldInhalt('ADK_FIL_ID'));
		$rsFilinfoName2 = $DB->RecordSetOeffnen($SQL);
		
		
		//Weiden
		if ($rsADKKopf->FeldInhalt('ADK_LAGERKZ')=='N')
		{
			//$Empf_Name = 'ATU - Auto-Teile-Unger';
			$Empf_Name = 'ESTATO';
			$Empf_Form = 'Umweltservice GmbH';
			$Empf_Strasse = 'Dr.-Kilian-Str. 11';
			$Empf_Ort = '92637 Weiden';
			$Empf_Telefon = 'Telefon (0961) 306-0';
		}
		//Werl
		elseif (($rsADKKopf->FeldInhalt('ADK_LAGERKZ')=='L'))
		{
			//$Empf_Name = 'ATU - Auto-Teile-Unger';
			$Empf_Name = 'ESTATO';
			$Empf_Form = 'Umweltservice GmbH';
			$Empf_Strasse = 'Hansering 2';
			$Empf_Ort = '59457 Werl';
			$Empf_Telefon = 'Telefon (02922) 807-4400';			
		}
		
		//Absender
		$Ausdruck->_pdf->SetFont('Arial','',12);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
		$Ausdruck->_pdf->cell(22,6,$AWISSprachKonserven['Wort']['Absender'].':',0,0,'L',0);						
		$Ausdruck->_pdf->cell(50,6,$rsFilinfoName1->FeldInhalt('FIF_WERT'),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$rsFilinfoName2->FeldInhalt('FIF_WERT'),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$rsFil->FeldInhalt('FIL_STRASSE'),0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$rsFil->FeldInhalt('FIL_PLZ').' '.$rsFil->FeldInhalt('FIL_ORT'),0,0,'L',0);						
		$Zeile+=5;		
		
		//Empf�nger
		$Zeile-=20;
		$Ausdruck->_pdf->SetXY($Spalte+90,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$AWISSprachKonserven['Wort']['Empfaenger'].':',0,0,'L',0);						
		$Ausdruck->_pdf->cell(50,6,$Empf_Name,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(60,6,$Empf_Form,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$Empf_Strasse,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(50,6,$Empf_Ort,0,0,'L',0);						
		$Zeile+=5;		
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);				
		$Ausdruck->_pdf->cell(30,6,$Empf_Telefon,0,0,'L',0);						
		$Zeile+=5;			
	}

	return $Zeile;	
}

function _Erzeuge_Inhalt_Lieferschein($Zeile)
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	global $PunkteGesamt;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	/***************************************
	* Hole Daten per SQL
	***************************************/		
	$Ausdruck->_pdf->SetFont('Arial','',12);	
	
	$Zeile+=5;
	
	//Pr�fen, ob Daten f�r Block 1 zu Drucken sind
	$SQL = 'SELECT * FROM ADRSTAMM';
	$SQL.= ' INNER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY';
	$SQL.= ' WHERE ADP_ADK_KEY=0'.$AWIS_KEY1.' AND ADS_ADRNR >= 11 AND ADS_ADRNR <= 15';
	$rsADP = $DB->RecordSetOeffnen($SQL);	
	if ($rsADP->AnzahlDatensaetze()>0)
	{
		$Zeile=_Erzeuge_Block_Lieferschein($Zeile+3, 1);		
	}
	
	$SQL = 'SELECT * FROM ADRSTAMM';
	$SQL.= ' INNER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY';
	$SQL.= ' WHERE ADP_ADK_KEY=0'.$AWIS_KEY1.' AND ADS_ADRNR >= 21 AND ADS_ADRNR <= 25';		
	$rsADP = $DB->RecordSetOeffnen($SQL);
	if ($rsADP->AnzahlDatensaetze()>0)
	{
		$Zeile=_Erzeuge_Block_Lieferschein($Zeile+3, 2);		
	}
	
	$SQL = 'SELECT * FROM ADRSTAMM';
	$SQL.= ' INNER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY';
	$SQL.= ' WHERE ADP_ADK_KEY=0'.$AWIS_KEY1.' AND ADS_ADRNR >= 41 AND ADS_ADRNR <= 45';	
	$rsADP = $DB->RecordSetOeffnen($SQL);
	if ($rsADP->AnzahlDatensaetze()>0)
	{
		$Zeile=_Erzeuge_Block_Lieferschein($Zeile+3, 3);		
	}
	
	if ($Zeile > 200)
	{
		$Zeile=_Erzeuge_Seite_Lieferschein(2);
	}
	
	$Ausdruck->_pdf->SetFont('Arial','',12);
	
	//Block 4
	$SQL = 'SELECT * FROM ADRTEXTE WHERE ADX_ADRART = 1 AND ADX_ADRBLOCK = 4 ORDER BY ADX_ADRSORT';
	$rsADR = $DB->RecordSetOeffnen($SQL);
	
	if ($rsADR->AnzahlDatensaetze()>0)
	{
		$Zeile+=5;
		
		while (!$rsADR->EOF())
		{
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			$Ausdruck->_pdf->cell(180,6,$rsADR->Feldinhalt('ADX_ADRBEZ'),0,0,'L',0);
			$Zeile+=5;
			$rsADR->DSWeiter();
		}
	}
	
	$Zeile+=6;
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	
	$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->cell(150,6,'Anzahl der Gesamtpunkte',0,0,'R',0);
	$Ausdruck->_pdf->cell(30,6,$PunkteGesamt,0,0,'R',0);
	$Ausdruck->_pdf->Line($Spalte,$Zeile+6,$Spalte+180,$Zeile+6);
	$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+6);
	$Ausdruck->_pdf->Line($Spalte+180,$Zeile,$Spalte+180,$Zeile+6);
	
	//Block 5
	$SQL = ' SELECT * FROM ADRTEXTE';
	$SQL.= ' WHERE ADX_ADRART = 1';
	$SQL.= ' AND (ADX_ADRBLOCK >= 5 AND ADX_ADRBLOCK <= 15)';
	$SQL.= ' ORDER BY ADX_ADRBLOCK, ADX_ADRSORT';
	$rsADR = $DB->RecordSetOeffnen($SQL);
	
	if ($rsADR->AnzahlDatensaetze()>0)
	{
		$Zeile+=6;
		
		$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);
		$Zeilebeginn=$Zeile;
		
		while (!$rsADR->EOF())
		{
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			$Ausdruck->_pdf->cell(180,6,$rsADR->Feldinhalt('ADX_ADRBEZ'),0,0,'L',0);
			$Zeile+=5;
			if($Zeile >= 270) {
				$Ausdruck->_pdf->Line($Spalte,$Zeilebeginn,$Spalte,$Zeile+6);
				$Ausdruck->_pdf->Line($Spalte+180,$Zeilebeginn,$Spalte+180,$Zeile+6);
				$Ausdruck->_pdf->Line($Spalte,$Zeile+6,$Spalte+180,$Zeile+6);
				$Ausdruck->NeueSeite(0,1);
				$Seite++;
				$Zeile = 25;
				$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);
				$Zeilebeginn=$Zeile;
			}
			$rsADR->DSWeiter();
		}

		$Zeile=$Zeile+3;
		$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);

		//Fu�zeile
		$Zeile=$Zeile+15;		
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+55,$Zeile);
		$Ausdruck->_pdf->cell(55,6,'Pol.Kennzeichen des LKW',0,0,'L',0);
		
		$Ausdruck->_pdf->SetXY($Spalte+60,$Zeile);
		$Ausdruck->_pdf->Line($Spalte+60,$Zeile,$Spalte+115,$Zeile);
		$Ausdruck->_pdf->cell(55,6,'Unterschrift des Fahrers',0,0,'L',0);
		
		$Ausdruck->_pdf->SetXY($Spalte+120,$Zeile);
		$Ausdruck->_pdf->Line($Spalte+120,$Zeile,$Spalte+175,$Zeile);
		$Ausdruck->_pdf->cell(55,6,'Unterschrift Filialleiter',0,0,'L',0);		
		
		$Ausdruck->_pdf->Line($Spalte,$Zeilebeginn,$Spalte,$Zeile+6);
		$Ausdruck->_pdf->Line($Spalte+180,$Zeilebeginn,$Spalte+180,$Zeile+6);
		$Ausdruck->_pdf->Line($Spalte,$Zeile+6,$Spalte+180,$Zeile+6);
	}	
	return $Zeile;
}

function _Erzeuge_Block_Lieferschein($Zeile, $Block)
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	global $PunkteGesamt;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$GesamtPunkte = 0;
	$GesamtGewicht = 0;
	
	//Block 1
	$SQL = 'SELECT * FROM ADRTEXTE WHERE ADX_ADRART = 1 AND ADX_ADRBLOCK = '.$Block.' ORDER BY ADX_ADRSORT';
	$rsADR = $DB->RecordSetOeffnen($SQL);
	
	if ($rsADR->AnzahlDatensaetze()>0)
	{
		$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);
		$Zeilebeginn=$Zeile;
					
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(50,6,'Abfall enth�lt:',0,0,'L',0);
		$Zeile+=5;
		
		while (!$rsADR->EOF())
		{
			$Ausdruck->_pdf->SetXY($Spalte+5,$Zeile);
			$Ausdruck->_pdf->cell(180,6,$rsADR->Feldinhalt('ADX_ADRBEZ'),0,0,'L',0);
			$Zeile+=5;
			$rsADR->DSWeiter();
		}
		
		$Zeile=$Zeile+3;		
		$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+180,$Zeile);
		$Ausdruck->_pdf->Line($Spalte,$Zeilebeginn,$Spalte,$Zeile);
		$Ausdruck->_pdf->Line($Spalte+180,$Zeilebeginn,$Spalte+180,$Zeile);
										
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(30,12,'',1,0,'C',0);				
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(30,6,'EAK',0,0,'L',0);							
		
		$Ausdruck->_pdf->setXY($Spalte+30,$Zeile);
		$Ausdruck->_pdf->cell(70,12,'',1,0,'C',0);				
		$Ausdruck->_pdf->setXY($Spalte+30,$Zeile);
		$Ausdruck->_pdf->cell(70,6,'Interne Bezeichnung',0,0,'C',0);		
		
		$Zeile+=6;		
		$Ausdruck->_pdf->setXY($Spalte+30,$Zeile);
		$Ausdruck->_pdf->cell(70,6,'Nettogewicht je Fass',0,0,'C',0);		
		
		$Zeile-=6;		
		$Ausdruck->_pdf->setXY($Spalte+100,$Zeile);
		if ($Block==3)
		{
			$Ausdruck->_pdf->cell(40,12,'Anzahl Batterien',1,0,'C',0);		
		}
		else 
		{
			$Ausdruck->_pdf->cell(40,12,'Anzahl F�sser',1,0,'C',0);		
		}
		
		$Ausdruck->_pdf->setXY($Spalte+140,$Zeile);
		$Ausdruck->_pdf->cell(40,12,'Gesamtgewicht',1,0,'C',0);																	
								
		if ($Block == 1)
		{			
			//Positionen zu Block 1		
			$SQL = 'SELECT ADRST.*, ADRPOS.*';		
			$SQL .= ' FROM ADRSTAMM ADRST ';	
			$SQL .= ' INNER JOIN ADRPOS ON ADS_KEY = ADP_ADS_KEY AND ADP_ADK_KEY ='.$AWIS_KEY1;	
			$SQL .= ' WHERE ADS_ADRART=1 AND ADS_ADRNR >= 11 AND ADS_ADRNR <= 15';
			$Faktor=3;
		}
		elseif ($Block == 2)
		{
			//Positionen zu Block 2		
			$SQL = 'SELECT ADRST.*, ADRPOS.*';		
			$SQL .= ' FROM ADRSTAMM ADRST ';	
			$SQL .= ' INNER JOIN ADRPOS ON ADS_KEY = ADP_ADS_KEY AND ADP_ADK_KEY ='.$AWIS_KEY1;	
			$SQL .= ' WHERE ADS_ADRART=1 AND ADS_ADRNR >= 21 AND ADS_ADRNR <= 25';			
			$Faktor=3;
		}
		elseif ($Block == 3)
		{
			//Positionen zu Block 3		
			$SQL = 'SELECT ADRST.*, ADRPOS.*';		
			$SQL .= ' FROM ADRSTAMM ADRST ';	
			$SQL .= ' INNER JOIN ADRPOS ON ADS_KEY = ADP_ADS_KEY AND ADP_ADK_KEY ='.$AWIS_KEY1;	
			$SQL .= ' WHERE ADS_ADRART=1 AND ADS_ADRNR >= 41 AND ADS_ADRNR <= 45';						
			$Faktor=1;
		}
		
		$rsADP = $DB->RecordSetOeffnen($SQL);
		
		while (!$rsADP->EOF())
		{
			$Zeile+=12;
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			$Ausdruck->_pdf->cell(30,6,$rsADP->FeldInhalt('ADS_EWCCODE'),0,0,'L',0);			
			
			$Ausdruck->_pdf->SetXY($Spalte+30,$Zeile);
			$Ausdruck->_pdf->cell(70,6,$rsADP->FeldInhalt('ADS_LAGABEZ'),0,0,'L',0);			
			
			$Ausdruck->_pdf->SetXY($Spalte+50,$Zeile+6);
			$Ausdruck->_pdf->cell(50,6,$rsADP->FeldInhalt('ADS_ZULADGEW'). ' '.$rsADP->FeldInhalt('ADS_EINHEIT'),0,0,'L',0);						
			
			$Ausdruck->_pdf->SetXY($Spalte+100,$Zeile+6);
			$Ausdruck->_pdf->cell(20,6,($rsADP->FeldInhalt('ADP_MENGE')!=''?$rsADP->FeldInhalt('ADP_MENGE'):'0'),0,0,'R',0);			
			$Ausdruck->_pdf->cell(20,6,'St�ck',0,0,'L',0);			
			
			$Punkte = $DB->FeldInhaltFormat('N',$rsADP->FeldInhalt('ADP_GEWICHT')) * $Faktor;
			$Gewicht = $DB->FeldInhaltFormat('N',$rsADP->FeldInhalt('ADP_MENGE'))*$DB->FeldInhaltFormat('N',$rsADP->FeldInhalt('ADS_ZULADGEW'));
			
			$Ausdruck->_pdf->SetXY($Spalte+140,$Zeile+6);
			$Ausdruck->_pdf->cell(40,6,$Gewicht.' '.$rsADP->FeldInhalt('ADS_EINHEIT'),0,0,'R',0);			

			$Ausdruck->_pdf->Line($Spalte,$Zeile+12,$Spalte+180,$Zeile+12);		
			$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+12);		
			$Ausdruck->_pdf->Line($Spalte+30,$Zeile,$Spalte+30,$Zeile+12);		
			$Ausdruck->_pdf->Line($Spalte+100,$Zeile,$Spalte+100,$Zeile+12);		
			$Ausdruck->_pdf->Line($Spalte+140,$Zeile,$Spalte+140,$Zeile+12);		
			$Ausdruck->_pdf->Line($Spalte+180,$Zeile,$Spalte+180,$Zeile+12);		
			
			$rsADP->DSWeiter();		
			$GesamtPunkte+=$Punkte;
			$GesamtGewicht+=$Gewicht;
		}
		
		$Zeile+=12;
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(140,6,'Gesamtgewicht der oben aufgef�hrten Abf�lle',1,0,'R',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+140,$Zeile);
		$Ausdruck->_pdf->cell(40,6,$GesamtGewicht. ' '.$rsADP->FeldInhalt('ADS_EINHEIT'),1,0,'R',0);					
		
		$Zeile+=6;
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(140,6,'',1,0,'R',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+140,$Zeile);
		$Ausdruck->_pdf->cell(40,6,$GesamtPunkte,1,0,'R',0);		

		$Zeile+=6;	
		
		$PunkteGesamt+=$GesamtPunkte;
	}
	return $Zeile;
}

function _Erzeuge_Seite_Abfallschein($Abfallscheinseite)
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;
	
	//if ($Abfallscheinseite == 1)
	//{			
		$SQL = 'SELECT ';
		$SQL .= ' ADK_KEY';
		$SQL .= ',ADK_FIL_ID';
		$SQL .= ',ADK_ART';
		$SQL .= ',ADK_VORGANGNR';
		$SQL .= ',ADK_STATUS';
		$SQL .= ',ADK_DATUMERSTELLUNG';
		$SQL .= ',TO_CHAR(ADK_DATUMABSCHLUSS,\'DD.MM.YYYY\') as ADK_DATUMABSCHLUSS';
		$SQL .= ',TO_CHAR(ADK_DATUMABSCHLUSS, \'MM\') as ADK_DATUMABSCHLUSS_MONAT';
		$SQL .= ',TO_CHAR(ADK_DATUMABSCHLUSS, \'YY\') as ADK_DATUMABSCHLUSS_JAHR';
		$SQL .= ',ADK_ERZEUGERNR';
		$SQL .= ',ADK_BEFOERDERERNR';
		$SQL .= ',ADK_ENTSORGERNR';
		$SQL .= ',ADK_LAGERKZ';
		$SQL .= ',ADK_RFK_KEY';
		$SQL .= ',ADK_USER';
		$SQL .= ',ADK_USERDAT';
		$SQL .=' FROM ADRKOPF ';		
		$SQL .=' WHERE ADK_KEY=0'.$AWIS_KEY1;		
		
		$rsADKKopf = $DB->RecordSetOeffnen($SQL);

		// Fu�zeile
		$Ausdruck->_pdf->SetFont('Arial','',8);
		$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
		//$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
		$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
		//$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
		$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
		$Ausdruck->_pdf->Cell(20,8,$rsADKKopf->FeldInhalt('ADK_DATUMABSCHLUSS'),0,0,'L',0);
									
		$Ausdruck->_pdf->SetFont('Arial','B',14);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		//$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['ADK']['txt_Lieferschein'],0,0,'L',0);				
		$Ausdruck->_pdf->Cell(40,5,'�bernahmeschein zum Nachweis der freiwilligen',0,0,'L',0);				
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile+5);
		$Ausdruck->_pdf->Cell(40,5,'R�cknahme von Abf�llen',0,0,'L',0);				
		$Zeile+=20;					
		
		$Ausdruck->_pdf->SetFont('Arial','B',12);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(60,5,$AWISSprachKonserven['ADK']['txt_Uebernahmescheinnummer'].':',0,0,'L',0);

		$Monat = $rsADKKopf->FeldInhalt('ADK_DATUMABSCHLUSS_MONAT');
		$Jahr = $rsADKKopf->FeldInhalt('ADK_DATUMABSCHLUSS_JAHR');

		$EAN='1'.str_pad($rsADKKopf->FeldInhalt('ADK_FIL_ID'),4,'0',STR_PAD_LEFT).$Jahr.$Monat.str_pad($rsADKKopf->FeldInhalt('ADK_VORGANGNR'),3,'0',STR_PAD_LEFT);
	
		$EAN_CLASS = new BarcodeEncode_EAN13();

		
		if ($EAN_CLASS->Validate($EAN))
		{
			$EAN_CODE = $EAN_CLASS->Enc($EAN);
			$EAN_SAVE = $EAN_CODE->iData;
		}
				
		$Ausdruck->_pdf->cell(30,5,$EAN_SAVE,0,0,'L',0);		
						
		$EAN3='3'.str_pad($rsADKKopf->FeldInhalt('ADK_FIL_ID'),4,'0',STR_PAD_LEFT).$Jahr.$Monat.str_pad($rsADKKopf->FeldInhalt('ADK_VORGANGNR'),3,'0',STR_PAD_LEFT);
		if ($EAN_CLASS->Validate($EAN3))
		{
			$EAN_CODE = $EAN_CLASS->Enc($EAN3);
			$EAN_SAVE = $EAN_CODE->iData;
		}
					
		$Ausdruck->_pdf->Image(Erzeuge_EAN13($EAN3,3),$Spalte+125,$Zeile-8,50,10,'png');	
		$Ausdruck->_pdf->SetXY($Spalte+150,$Zeile+2);
		$Ausdruck->_pdf->SetFont('Courier','B',9);
		//$Ausdruck->_pdf->SetCharSpacing(1,0);
		$Ausdruck->_pdf->Cell(30,6,$EAN_SAVE,0,0,'L',0);	
		$Ausdruck->_pdf->SetFont('Arial','',10);
		//$Ausdruck->_pdf->SetCharSpacing(0,0);
		
		$Zeile+=10;	
		
	//}
	//else 
	//{
	//	$Zeile+=15;
	//}

	return $Zeile;
}

function _Erzeuge_Inhalt_Abfallschein($Zeile)
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	

	/***************************************
	* Hole Daten per SQL
	***************************************/	
	
	$Ausdruck->_pdf->SetFont('Arial','',12);		
		
	// Listen�berschrift f� Abfallschein
	$Ausdruck->_pdf->SetFont('Arial','B',8);			
	$Ausdruck->_pdf->line($Spalte,$Zeile,$Spalte+175,$Zeile);	
	$Ausdruck->_pdf->line($Spalte,$Zeile,$Spalte,$Zeile+12);	
	$Ausdruck->_pdf->setXY($Spalte,$Zeile+6);
	$Ausdruck->_pdf->cell(20,6,'EAK',0,0,'C',0);				
	
	$Ausdruck->_pdf->setXY($Spalte+20,$Zeile);
	$Ausdruck->_pdf->line($Spalte+20,$Zeile,$Spalte+20,$Zeile+12);	
	$Ausdruck->_pdf->cell(80,6,'Interne Bezeichnung',0,0,'C',0);				
	$Ausdruck->_pdf->setXY($Spalte+20,$Zeile+6);
	$Ausdruck->_pdf->cell(80,6,'AVV Bezeichnung',0,0,'C',0);				
		
	$Ausdruck->_pdf->setXY($Spalte+100,$Zeile);
	$Ausdruck->_pdf->line($Spalte+100,$Zeile,$Spalte+100,$Zeile+12);	
	$Ausdruck->_pdf->cell(25,6,'Menge',0,0,'C',0);		
		
	$Ausdruck->_pdf->setXY($Spalte+125,$Zeile);
	$Ausdruck->_pdf->line($Spalte+125,$Zeile,$Spalte+125,$Zeile+12);	
	$Ausdruck->_pdf->cell(25,6,'Gebinde',0,0,'C',0);		
		
	$Ausdruck->_pdf->setXY($Spalte+150,$Zeile);
	$Ausdruck->_pdf->line($Spalte+150,$Zeile,$Spalte+150,$Zeile+12);	
	$Ausdruck->_pdf->cell(25,6,'Gewicht',0,0,'C',0);	
	$Ausdruck->_pdf->line($Spalte+175,$Zeile,$Spalte+175,$Zeile+12);		
				
	$Zeile+=12;	
	
	$SQL = 'SELECT ADRST.*, ADRPOS.*';		
	$SQL .= ' FROM ADRSTAMM ADRST ';		
	$SQL .= ' INNER JOIN ADRPOS ON ADS_KEY = ADP_ADS_KEY AND ADP_ADK_KEY ='.$AWIS_KEY1;		
	$SQL .= ' WHERE ADS_ADRART=1 AND (ADS_ADRNR <= 20 OR ADS_ADRNR >= 26)';	
	$SQL .= ' ORDER BY ADS_ADRNR';			

	$rsADP = $DB->RecordSetOeffnen($SQL);
	
	$Ausdruck->_pdf->SetFont('Arial','',8);		
		
	while (!$rsADP->EOF())
	{			
		if ($Zeile > 260)
		{
			$Zeile=_Erzeuge_Seite_Abfallschein();
		}	
	
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile+4);
		$Ausdruck->_pdf->line($Spalte,$Zeile,$Spalte+175,$Zeile);	
		$Ausdruck->_pdf->line($Spalte,$Zeile,$Spalte,$Zeile+8);	
		$Ausdruck->_pdf->cell(20,4,$rsADP->FeldInhalt('ADS_EWCCODE'),0,0,'L',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+20,$Zeile);
		$Ausdruck->_pdf->line($Spalte+20,$Zeile,$Spalte+20,$Zeile+8);	
		$Ausdruck->_pdf->cell(80,4,$rsADP->FeldInhalt('ADS_LAGABEZ'),0,0,'L',0);			
		$Ausdruck->_pdf->SetXY($Spalte+20,$Zeile+4);
		$Ausdruck->_pdf->cell(80,4,$rsADP->FeldInhalt('ADS_EWCBEZ'),0,0,'L',0);			
					
		$Ausdruck->_pdf->SetXY($Spalte+100,$Zeile);
		$Ausdruck->_pdf->line($Spalte+100,$Zeile,$Spalte+100,$Zeile+8);	
		$Ausdruck->_pdf->cell(25,4,$rsADP->FeldInhalt('ADP_MENGE'),0,0,'R',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
		$Ausdruck->_pdf->line($Spalte+125,$Zeile,$Spalte+125,$Zeile+8);	
		$Ausdruck->_pdf->cell(25,4,$rsADP->FeldInhalt('ADS_BEHTYP'),0,0,'L',0);			
		
		$Ausdruck->_pdf->SetXY($Spalte+150,$Zeile);
		$Ausdruck->_pdf->line($Spalte+150,$Zeile,$Spalte+150,$Zeile+8);	
		$Ausdruck->_pdf->cell(25,4,$Form->Format('N2',$rsADP->FeldInhalt('ADP_GEWICHT')).' '.$rsADP->FeldInhalt('ADS_EINHEIT'),0,0,'R',0);						
		//$Ausdruck->_pdf->cell(25,4,$rsADP->FeldInhalt('ADP_GEWICHT').' '.$rsADP->FeldInhalt('ADS_EINHEIT'),0,0,'R',0);						
		$Ausdruck->_pdf->line($Spalte+175,$Zeile,$Spalte+175,$Zeile+8);				
		
		$Zeile+=8;
		$rsADP->DSWeiter();
	}
	
	$Ausdruck->_pdf->line($Spalte,$Zeile,$Spalte+175,$Zeile);					

	$Zeile+=6;
	
	$Zeile=_Erzeuge_Ende_Abfallschein($Zeile, 1);
	$Zeile=_Erzeuge_Ende_Abfallschein($Zeile, 2);
	$Zeile=_Erzeuge_Ende_Abfallschein($Zeile, 3);

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile+6);
	$Ausdruck->_pdf->SetFont('Arial','B',8);;	
	$Ausdruck->_pdf->cell(180,6,'Vermerke: Weiterf�hrendes Begleitpapier f�r die Zuf�hrung der �bernommenen Abf�lle zu einer Verwertungs- oder Beseitigungsanlage',0,0,'L',0);							
	$Ausdruck->_pdf->SetFont('Arial','B','');;	
	$Zeile+=18;
	
	$Zeile=_Erzeuge_Ende_Abfallschein($Zeile, 4);
	$Zeile=_Erzeuge_Ende_Abfallschein($Zeile, 5);
	$Zeile=_Erzeuge_Ende_Abfallschein($Zeile, 6);
	
	return $Zeile;
}

function _Erzeuge_Ende_Abfallschein($Zeile, $Art)
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$SQL = 'SELECT *';
	$SQL .=' FROM ADRKOPF ';		
	$SQL .=' WHERE ADK_KEY=0'.$AWIS_KEY1;		
		
	$rsADKKopf = $DB->RecordSetOeffnen($SQL);	
	
	$SQL = 'SELECT FIL_ID, FIL_STRASSE, FIL_PLZ, FIL_ORT FROM FILIALEN WHERE FIL_ID = '.$DB->FeldInhaltFormat('Z',$rsADKKopf->FeldInhalt('ADK_FIL_ID'));
	$rsFil = $DB->RecordSetOeffnen($SQL);
	
	$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=912 AND FIF_FIL_ID ='.$DB->FeldInhaltFormat('Z',$rsADKKopf->FeldInhalt('ADK_FIL_ID'));
	$rsFilinfoName1 = $DB->RecordSetOeffnen($SQL);
		
	$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=913 AND FIF_FIL_ID ='.$DB->FeldInhaltFormat('Z',$rsADKKopf->FeldInhalt('ADK_FIL_ID'));
	$rsFilinfoName2 = $DB->RecordSetOeffnen($SQL);
	
	$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=911 AND FIF_FIL_ID ='.$DB->FeldInhaltFormat('Z',$rsADKKopf->FeldInhalt('ADK_FIL_ID'));
	$rsFilinfoErzeugerNr = $DB->RecordSetOeffnen($SQL);
	
	if ($Art == 1)
	{
		$Ausgabe = 'Erzeuger';
		//$AusgabeName = 'ATU Auto-Teile-Unger';
		//$AusgabeFirma = 'GmbH & Co. KG';
		$AusgabeName = $rsFilinfoName1->FeldInhalt('FIF_WERT');
		$AusgabeFirma = $rsFilinfoName2->FeldInhalt('FIF_WERT');
		$AusgabeStrasse = $rsFil->FeldInhalt('FIL_STRASSE');
		$AusgabeOrt = $rsFil->FeldInhalt('FIL_PLZ'). ' '.$rsFil->FeldInhalt('FIL_ORT');
		$AusgabeNummer = 'Erzeugernummer';
		$AusgabeUnterschrift = 'Deklaration';
		$Nummer = $rsFilinfoErzeugerNr->FeldInhalt('FIF_WERT');
	}
	elseif ($Art == 2)
	{
		$Ausgabe = 'Bef�rderer';
		$AusgabeName = 'ATU Auto-Teile-Unger';
		$AusgabeFirma = 'Handels GmbH & Co. KG';
		$AusgabeStrasse = 'Dr.-Kilian-Str. 11';		
		$AusgabeOrt = '92637 Weiden';
		$AusgabeNummer = 'Bef�rderernummer';
		$AusgabeUnterschrift = 'Bef�rderung';
		$Nummer='I363T0250';
	}	
	elseif ($Art == 3)
	{
		$Ausgabe = 'Entsorger';
		$AusgabeName = 'ATU Auto-Teile-Unger';
		$AusgabeFirma = 'Handels GmbH & Co. KG';
		$AusgabeStrasse = 'Dr.-Kilian-Str. 11';		
		$AusgabeOrt = '92637 Weiden';
		$AusgabeNummer = 'Entsorgernummer';
		$AusgabeUnterschrift = 'Entsorgung';
		$Nummer='I363W1002';
	}
	elseif ($Art == 4)
	{
		$Ausgabe = 'Erzeuger';
		//$AusgabeName = 'ATU - Auto-Teile-Unger';
		$AusgabeName = 'ESTATO';
		$AusgabeFirma = 'Umweltservice GmbH';
		$AusgabeStrasse = 'Dr.-Kilian-Str. 11';		
		$AusgabeOrt = '92637 Weiden';
		$AusgabeNummer = 'Erzeugernummer';
		$AusgabeUnterschrift = 'Deklaration';
		$Nummer='IF363E100';
	}
	elseif ($Art == 5)
	{
		$Ausgabe = 'Bef�rderer';
		$AusgabeName = 'ATU Auto-Teile-Unger';
		$AusgabeFirma = 'Handels GmbH & Co. KG';
		$AusgabeStrasse = 'Dr.-Kilian-Str. 11';		
		$AusgabeOrt = '92637 Weiden';
		$AusgabeNummer = 'Bef�rderernummer';
		$AusgabeUnterschrift = 'Entsorgung';
		$Nummer='I363T0250';
	}
	elseif ($Art == 6)
	{
		$Ausgabe = 'Entsorger';
		//$AusgabeName = 'ATU - Auto-Teile-Unger';
		$AusgabeName = 'ESTATO';
		$AusgabeFirma = 'Umweltservice GmbH';		
		$AusgabeStrasse='';
		$AusgabeOrt='';
		$Nummer='';
		if ($rsADKKopf->FeldInhalt('ADK_LAGERKZ')=='N')
		{
			$AusgabeStrasse = 'Dr.-Kilian-Str. 11';		
			$AusgabeOrt = '92637 Weiden';
			$Nummer='I363W1003';
		}
		else 
		{
			$AusgabeStrasse = 'Hansering 2';		
			$AusgabeOrt = '59457 Werl';
			$Nummer='E97497404';
		}
		$AusgabeNummer = 'Verwerternummer';
		$AusgabeUnterschrift = 'Entsorgung';
		
	}
	
	if ($Zeile > 250)
	{
		$Zeile=_Erzeuge_Seite_Abfallschein();
	}
			
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
	//$Ausdruck->_pdf->cell(22,6,$AWISSprachKonserven['Wort']['Absender'].':',0,0,'L',0);						
	$Ausdruck->_pdf->cell(22,6,$Ausgabe.':',0,0,'L',0);						
	$Ausdruck->_pdf->cell(50,6,$AusgabeName,0,0,'L',0);							
	$Ausdruck->_pdf->setXY($Spalte+100,$Zeile);					
	$Ausdruck->_pdf->cell(30,6,$AusgabeNummer.':',0,0,'L',0);							
	$Ausdruck->_pdf->SetFont('Arial','B',8);
	$Ausdruck->_pdf->cell(30,6,$Nummer,0,0,'L',0);							
	$Ausdruck->_pdf->SetFont('Arial','',8);	
	$Zeile+=5;		
		
	$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
	$Ausdruck->_pdf->cell(50,6,$AusgabeFirma,0,0,'L',0);						
	$Zeile+=5;		
		
	$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
	$Ausdruck->_pdf->cell(50,6,$AusgabeStrasse,0,0,'L',0);						
	$Ausdruck->_pdf->setXY($Spalte+100,$Zeile);				
	$Ausdruck->_pdf->cell(30,6,'Unterschrift:',0,0,'L',0);						
	$Ausdruck->_pdf->line($Spalte+130,$Zeile+5,$Spalte+170,$Zeile+5);						
	$Zeile+=5;		
		
	$Ausdruck->_pdf->setXY($Spalte+22,$Zeile);				
	$Ausdruck->_pdf->cell(50,6,$AusgabeOrt,0,0,'L',0);						
	$Ausdruck->_pdf->setXY($Spalte+100,$Zeile);				
	$Ausdruck->_pdf->cell(50,6,'(zur ordnungsgem��en '.$AusgabeUnterschrift.')',0,0,'L',0);						
	$Zeile+=5;	
	
	return $Zeile;
}


/**
*
* Funktion speichert ein Bild mit EAN-Code
*
* @author Christian Argauer
* @param  char EAN13-Nummer 12-stellig
* @return Datei-Name
*
*/
function Erzeuge_EAN13($EAN_NR,$Art)
{
	$DateiName = awis_UserExportDateiName('.png');
	if ($Art==3)
	{
		$DateiName = awis_UserExportDateiName('3.png');
	}
	$symbology = BarcodeFactory::Create (ENCODING_EAN13);
	$barcode = BackendFactory ::Create('IMAGE', $symbology);
	$barcode->SetScale(1);
	$barcode->SetMargins(10,0,0,1);
	$barcode->SetColor('black','white');
	$barcode->NoText(true);
	$barcode->SetHeight(30);
	//$barcode -> SetHeight(50);
	$barcode->Stroke($EAN_NR,$DateiName);
	return $DateiName;
}

?>

