<?php
global $con;
global $Recht3000;
global $AWISSprache;
global $awisRSZeilen;

$EingabeFeld='';		// Zum Cursor setzen

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Adressen','ADR_%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Adressen','txtAdresstypen');
$TextKonserven[]=array('Adressen','txtAZG_OEFFENTLICH');
$TextKonserven[]=array('Adressen','txtNeuerAdressenTyp');
$TextKonserven[]=array('Adressen','txtAnredenListe');

$TXT_AdrListe = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);


//********************************************************
// Parameter ?
//********************************************************

if(isset($_POST['cmdSuche_x']))
{
	$Param = $_POST['txtSuche'];				// Namen
	$Param .= ';' . $_POST['txtKontakt'];		// Kontaktinfo
	$Param .= ';0';								// ADRKEY

	awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $_SERVER['PHP_AUTH_USER'] , $Param );
}
			// Neuen Datensatz anlegen
elseif(($Recht3000&2) == 2 AND isset($_GET['DSNeu']))
{
	$Param = ';;-1';
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	$Param = ';;0';
	include('./adressen_speichern.php');
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	$Param = awis_BenutzerParameter($con, 'ArtikelSuche', $_SERVER['PHP_AUTH_USER']);
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = '';
$Param = explode(';',$Param);

// Name angegeben?
if($Param[0]!='')
{
	$Bedingung .= 'OR  UPPER(ADR_Name1) ' . awisLIKEoderIST($Param[0].'%',1) . ' ';
	$Bedingung .= 'OR  UPPER(ADR_Name2) ' . awisLIKEoderIST($Param[0].'%',1) . ' ';

	$Bedingung .= 'OR  UPPER(AAP_Nachname) ' . awisLIKEoderIST($Param[0].'%') . ' ';
	$Bedingung .= 'OR  UPPER(AAP_Vorname) ' . awisLIKEoderIST($Param[0],'%') . ' ';
}

if($Param[1]!='')
{
	$Bedingung .= 'OR  UPPER(AKO_Wert) ' . awisLIKEoderIST($Param[1].'%') . ' ';
}

if($Param[2]!='')			// Eine bestimmte Adresse
{
	$Bedingung .= 'AND ADR_KEY = ' . $Param[2] . '';
}

$SQL = 'SELECT Adressen.*';
$SQL .= ' FROM Adressen';
$SQL .= ' LEFT OUTER JOIN ADRESSENANSPRECHPARTNER ON ADR_Key = AAP_ADR_Key';
$SQL .= ' LEFT OUTER JOIN ADRESSENKONTAKTE K1 ON ADR_Key = AKO_XXX_Key';
$SQL .= ' LEFT OUTER JOIN ADRESSENKONTAKTE K2 ON AAP_Key = AKO_XXX_Key';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}
$SQL .= ' ORDER BY ADR_Name1, ADR_Name2';


$rsFelder = awisOpenRecordset($con, $SQL);
$rsADRZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
if($rsADRZeilen==0 AND $Param[2]!=-1)		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden Adressen gefunden.</span>';
}
elseif($rsADRZeilen>1)						// Liste anzeigen
{

}
elseif($Param[2]==-1 AND !isset($_POST['txtAdressTyp']))
{
	echo '<form name=frmAdressen action=./adressen_Main.php?cmdAktion=Details&DSNeu=True method=POST>';
	echo '<table>';

	echo '<tr>';
	echo '<td>';
	$EingabeFeld='txtAdressTyp';
	echo $TXT_AdrListe['Adressen']['txtNeuerAdressenTyp'].'&nbsp;';
	echo '</td>';
	echo '<td>';
	echo '<select name=txtAdressTyp>';
	$AdressTypen =  explode(';',$TXT_AdrListe['Adressen']['txtAdresstypen']);
	echo '<option value=1>' . $AdressTypen[0] . '</option>';
	echo '<option value=1>' . $AdressTypen[1] . '</option>';
	echo '</select>';
	echo '</td>';
	echo '</tr>';

	echo '</table>';

	echo "&nbsp;<input accesskey=w type=image src=/bilder/eingabe_ok.png title='".$TXT_AdrListe['Wort']['lbl_weiter']." (Alt+W)' name=cmdWeiter value=\"Export\">";


	echo '</form>';
}
else										// Eine einzelne oder neue Adresse
{
	if(isset($_POST['txtAdressTyp']))			// Firmen(1) oder Privatadresse(2)
	{
		$AdressTyp = $_POST['txtAdressTyp'];
	}
	else
	{
		$AdressTyp = ($rsFelder['ADR_ANREDE'][0]==1?1:2);
	}
	echo '<form name=frmAdressen action=./adressen_Main.php?cmdAktion=Details method=POST>';
	echo '<table>';


	echo '<input type=hidden name=txtADR_KEY value='.$rsFelder['ADR_KEY'][0] . '>';
	echo '<input type=hidden name=txtAdressTyp value=1>';	// Firma oder Privat

	if($AdressTyp == 1)						// Firmenadresse
	{
		echo '<tr>';
		echo '<td>';
		echo $TXT_AdrListe['Adressen']['ADR_ANREDE'].':';
		echo '</td>';
		echo '<td>';
		echo '<input type=hidden name=txtADR_ANREDE value=1>';
		$Anreden = explode(";",$TXT_AdrListe['Adressen']['txtAnredenListe']);
		foreach($Anreden AS $Anrede)
		{
			$Option = explode('-',$Anrede);
			if($Option[0]==1)
			{
				echo $Option[1];
				break;
			}
		}
		echo '</td>';
		echo '</tr>';

		// Konzern
		$EingabeFeld='txtADR_KONZERN';
		echo '<tr>';
		awis_ErstelleTextFeld('ADR_KONZERN',$TXT_AdrListe['Adressen']['ADR_KONZERN'].':',25,$rsFelder['ADR_KONZERN'][0],(($Recht3000&6)!=0),1);
		echo '</tr>';


		// Name1
		echo '<tr>';
		awis_ErstelleTextFeld('ADR_NAME1',$TXT_AdrListe['Adressen']['ADR_NAME1'].':',50,$rsFelder['ADR_NAME1'][0],(($Recht3000&6)!=0),1);
		echo '</tr>';

		// Name2
		echo '<tr>';
		awis_ErstelleTextFeld('ADR_NAME2',$TXT_AdrListe['Adressen']['ADR_NAME2'].':',50,$rsFelder['ADR_NAME2'][0],(($Recht3000&6)!=0),1);
		echo '</tr>';

		// Postfach + PLZ
		echo '<tr>';
		echo '<td>';
		echo $TXT_AdrListe['Adressen']['ADR_POSTFACH'].':';
		echo '</td>';
		echo '<td>';
		if(($Recht3000&6)!=0)
		{
			echo '<input name=txtADR_POSTFACH type=text size=20 value=\''.$rsFelder['ADR_POSTFACH'][0].'\'>';
			echo '<input name=oldADR_POSTFACH type=hidden value=\''.$rsFelder['ADR_POSTFACH'][0].'\'>';
			echo '&nbsp;'.$TXT_AdrListe['Adressen']['ADR_POSTFACHPLZ'].':';
			echo '<input name=txtADR_POSTFACHPLZ type=text size=6 value=\''.$rsFelder['ADR_POSTFACHPLZ'][0].'\'>';
			echo '<input name=oldADR_POSTFACHPLZ type=hidden value=\''.$rsFelder['ADR_POSTFACHPLZ'][0].'\'>';
		}
		else
		{
			echo '' . $rsFelder['ADR_POSTFACH'][0];
			echo '&nbsp;'.$TXT_AdrListe['Adressen']['ADR_POSTFACHPLZ'].':';
			echo '&nbsp;' . $rsFelder['ADR_POSTFACHPLZ'][0];
		}
		echo '</td>';
		echo '</tr>';

		// Strasse und Hausnummer
		echo '<tr>';
		awis_ErstelleTextFeld('ADR_STRASSE',$TXT_AdrListe['Adressen']['ADR_STRASSE'].':',50,$rsFelder['ADR_STRASSE'][0],(($Recht3000&6)!=0),1);
		awis_ErstelleTextFeld('ADR_HAUSNUMMER','',10,$rsFelder['ADR_HAUSNUMMER'][0],(($Recht3000&6)!=0),1);
		echo '</tr>';

		echo '<tr>';
		echo '<td>';
		echo $TXT_AdrListe['Adressen']['ADR_STRASSE'].':';
		echo '</td>';
		echo '<td>';
		if(($Recht3000&6)!=0)
		{
			echo '<input name=txtADR_STRASSE type=text size=20 value=\''.$rsFelder['ADR_STRASSE'][0].'\'>';
			echo '<input name=oldADR_STRASSE type=hidden value=\''.$rsFelder['ADR_STRASSE'][0].'\'>';
			echo '&nbsp;'.$TXT_AdrListe['Adressen']['ADR_HAUSNUMMER'].':';
			echo '<input name=txtADR_HAUSNUMMER type=text size=6 value=\''.$rsFelder['ADR_HAUSNUMMER'][0].'\'>';
			echo '<input name=oldADR_HAUSNUMMER type=hidden value=\''.$rsFelder['ADR_HAUSNUMMER'][0].'\'>';
		}
		else
		{
			echo '' . $rsFelder['ADR_STRASSE'][0];
			echo '&nbsp;'.$TXT_AdrListe['Adressen']['ADR_HAUSNUMMER'].':';
			echo '&nbsp;' . $rsFelder['ADR_HAUSNUMMER'][0];
		}
		echo '</td>';
		echo '</tr>';

		// LAND, PLZ und Ort
		echo '<tr>';
		echo '<td>';
		echo $TXT_AdrListe['Adressen']['ADR_LAN_CODE'].'/'.$TXT_AdrListe['Adressen']['ADR_PLZ'].'/'.$TXT_AdrListe['Adressen']['ADR_ORT'].':';
		echo '</td>';
		echo '<td>';
		if(($Recht3000&6)!=0)
		{
			echo '<select name=txtADR_LAN_CODE>';
			$rsLAN = awisOpenRecordset($con,'SELECT LAN_CODE, LAN_LAND FROM Laender ORDER BY LAN_LAND');
			$rsLANZeilen = $awisRSZeilen;
			for($LANZeile=0;$LANZeile<$rsLANZeilen;$LANZeile++)
			{
				echo '<option value=' . $rsLAN['LAN_CODE'][$LANZeile];
				if($rsFelder['ADR_LAN_CODE'][0]==$rsLAN['LAN_CODE'][$LANZeile] OR ($rsFelder['ADR_LAN_CODE'][0]=='' AND $rsLAN['LAN_CODE'][$LANZeile]==$AWISSprache))
				{
					echo ' selected ';
				}
				echo '>'.$rsLAN['LAN_LAND'][$LANZeile].'</option>';
			}

			echo '</select>';
			echo '<input name=oldADR_LAN_CODE type=hidden value=\''.$rsFelder['ADR_ORT'][0].'\'>';
			echo '&nbsp;';
			echo '<input name=txtADR_PLZ type=text size=5 value=\''.$rsFelder['ADR_PLZ'][0].'\'>';
			echo '<input name=oldADR_PLZ type=hidden value=\''.$rsFelder['ADR_PLZ'][0].'\'>';
			echo '&nbsp;';
			echo '<input name=txtADR_ORT type=text size=50 value=\''.$rsFelder['ADR_ORT'][0].'\'>';
			echo '<input name=oldADR_ORT type=hidden value=\''.$rsFelder['ADR_ORT'][0].'\'>';
			echo '&nbsp;';
		}
		else
		{
			echo '' . $rsFelder['ADR_PLZ'][0];
			echo '&nbsp;' . $rsFelder['ADR_ORT'][0];
		}
		echo '</td>';
		echo '</tr>';

		// Bemerkung
		echo '<tr>';
		echo '<td valign=top>';
		echo $TXT_AdrListe['Adressen']['ADR_BEMERKUNG'].':';
		echo '</td>';
		echo '<td>';
		if(($Recht3000&6)!=0)
		{
			echo '<textarea cols=100 rows=3 name=txtADR_BEMERKUNG>'.$rsFelder['ADR_BEMERKUNG'][0].'</textarea>';
			echo '<input name=oldADR_BEMERKUNG type=hidden value=\''.stripslashes($rsFelder['ADR_BEMERKUNG'][0]).'\'>';
		}
		else
		{
			echo '' . $rsFelder['ADR_BEMERKUNG'][0];
		}
		echo '</td>';
		echo '</tr>';


			// Zugriff
		echo '<tr>';
		echo '<td>';
		echo $TXT_AdrListe['Adressen']['ADR_AZG_KEY'].':';
		echo '</td>';
		echo '<td>';
		echo '<select name=txtADR_AZG_KEY>';
		$SQL = 'SELECT AZG_KEY, AZG_BEZEICHNUNG ';
		$SQL .= ' FROM ADRESSENZUGRIFFSGRUPPEN';
		$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
		$SQL .= ' WHERE AZM_XBN_Key=0' . awisBenutzerID();
		$SQL .= ' OR AZG_KEY = 1';		// oder �ffentlich!
		$SQL .= ' ORDER BY AZG_BEZEICHNUNG';


		$rsAZG = awisOpenRecordset($con,$SQL);
		$rsAZGZeilen = $awisRSZeilen;
		for($AZGZeile=0;$AZGZeile<$rsAZGZeilen;$AZGZeile++)
		{
			echo '<option value='.$rsAZG['AZG_KEY'][$AZGZeile];
			if($rsFelder['ADR_AZG_KEY'][0]==$rsAZG['AZG_KEY'][$AZGZeile])
			{
				echo ' selected ';
			}
			echo '>'.$rsAZG['AZG_BEZEICHNUNG'][$AZGZeile].'</option>';
		}
		echo '</select>';
		echo '</td>';
		echo '</tr>';
		echo '</table>';

		awis_RegisterErstellen(3001, $con, (isset($_GET['Seite'])?$_GET['Seite']:'Infos'));


	}
											//********************************
	else 									// Privatadresse
											//********************************
	{

	}

	echo '<hr>';

	if(($Recht3000&6)!==0)
	{
		echo "&nbsp;<input accesskey=s type=image src=/bilder/diskette.png title='".$TXT_AdrListe['Wort']['lbl_speichern']." (ALT+S)' name=cmdSpeichern>";
	}
	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsADR, $_POST, $rsAZG, $SQL, $AWISSprache);

if($EingabeFeld!=='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$EingabeFeld."\")[0].focus();";
	echo '</Script>';
}
?>