<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $con;
global $comcon;
global $AWISBenutzer;
global $awisDBFehler;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Allgemein','OCA_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Allgemein');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Allgemein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Allgemein'].'</title>';
?>
</head>
<body>
<?php

$Recht3114 = awisBenutzerRecht($con,3114);
if($Recht3114==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$ILO_REF_ID='';

if (isset($_REQUEST['ILO_REF_ID']))
{
	$ILO_REF_ID=$_REQUEST['ILO_REF_ID'];
}

$ILO_LOG_KEY='';

if (isset($_REQUEST['ILO_LOG_KEY']))
{
	$ILO_LOG_KEY=$_REQUEST['ILO_LOG_KEY'];
}

$comcon = awisLogonComCluster('DE',false);

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

echo '<form name=frmOperating_Errlog_ILO action=./operating_COM_Details_Errlog_ILO.php method=POST>';

echo "<p><center><h2><b>COM - ERROR-Log - ILO</b></h2></center><p>";

If(($Recht3114&1 )==1 ){

	$BindeVariablen=array();
	$BindeVariablen['var_ilo_ref_id']=$ILO_REF_ID;

	$SQL="SELECT LEVEL, ";
	$SQL.="LPAD(ILO_MODULE||DECODE(ILO_ACTION,NULL,'','.'||ILO_ACTION),LENGTH(ILO_MODULE||DECODE(ILO_ACTION,NULL,'','.'||ILO_ACTION))+LEVEL-1,'_') AS LEVEL_INFO, ";
	$SQL.="XX.* FROM "; 
	$SQL.="(SELECT AA.* FROM ILO_LOG_TABLE AA ";
	$SQL.="START WITH ILO_WORKER_ID = :var_ilo_ref_id ";
	$SQL.="CONNECT BY PRIOR ILO_WORKER_ID = ILO_CALLER_ID ";
	$SQL.="UNION ";
	$SQL.="SELECT AA.* FROM ILO_LOG_TABLE AA ";
	$SQL.="START WITH ILO_CALLER_ID = :var_ilo_ref_id ";
	$SQL.="CONNECT BY PRIOR ILO_CALLER_ID = ILO_WORKER_ID ";
	$SQL.=")XX ";
	$SQL.="START WITH ILO_CALLER_ID IS NULL ";
	$SQL.="CONNECT BY PRIOR ILO_WORKER_ID = ILO_CALLER_ID ";
	
	$rsILO=awisOpenRecordset($comcon,$SQL, true, false, $BindeVariablen);
	$rsILOZeilen = $awisRSZeilen;
	
	if ($rsILOZeilen===0)
	{
		/*
		$SQL="SELECT LEVEL, ";
		$SQL.="LPAD(ILO_MODULE||DECODE(ILO_ACTION,NULL,'','.'||ILO_ACTION),LENGTH(ILO_MODULE||DECODE(ILO_ACTION,NULL,'','.'||ILO_ACTION))+LEVEL-1,'_') AS LEVEL_INFO, ";
		$SQL.="XX.* FROM "; 
		$SQL.="(SELECT AA.* FROM ILO_LOG_TABLE AA ";
		$SQL.="START WITH ILO_WORKER_ID = :var_ilo_ref_id ";
		$SQL.="CONNECT BY PRIOR ILO_WORKER_ID = ILO_CALLER_ID ";
		$SQL.="UNION ";
		$SQL.="SELECT AA.* FROM ILO_LOG_TABLE AA ";
		$SQL.="START WITH ILO_CALLER_ID = :var_ilo_ref_id ";
		$SQL.="CONNECT BY PRIOR ILO_CALLER_ID = ILO_WORKER_ID ";
		$SQL.=")XX ";
		$SQL.="START WITH ILO_CALLER_ID = :var_ilo_ref_id ";
		$SQL.="CONNECT BY PRIOR ILO_WORKER_ID = ILO_CALLER_ID ";
		*/
		$SQL="SELECT LEVEL, ";
		$SQL.="LPAD(ILO_MODULE||DECODE(ILO_ACTION,NULL,'','.'||ILO_ACTION),LENGTH(ILO_MODULE||DECODE(ILO_ACTION,NULL,'','.'||ILO_ACTION))+LEVEL-1,'_') AS LEVEL_INFO, ";
		$SQL.="XX.* FROM ( ";
		$SQL.="SELECT DISTINCT YY.* FROM ";
		$SQL.="(SELECT AA.* FROM ILO_LOG_TABLE AA ";
		$SQL.="START WITH ILO_WORKER_ID = :VAR_ILO_REF_ID ";
		$SQL.="CONNECT BY PRIOR ILO_WORKER_ID = ILO_CALLER_ID ";
		$SQL.="UNION ";
		$SQL.="SELECT AA.* FROM ILO_LOG_TABLE AA ";
		$SQL.="START WITH ILO_CALLER_ID = :VAR_ILO_REF_ID ";
		$SQL.="CONNECT BY PRIOR ILO_CALLER_ID = ILO_WORKER_ID ";
		$SQL.="UNION ";
		$SQL.="SELECT AA.* FROM (SELECT * FROM ILO_LOG_TABLE WHERE ILO_WORKER_ID=:VAR_ILO_REF_ID) AA ";
		$SQL.="UNION ";
		$SQL.="SELECT AA.* FROM ILO_LOG_TABLE AA WHERE ";
		$SQL.="ILO_CALLER_ID IN (SELECT ILO_CALLER_ID FROM ILO_LOG_TABLE WHERE ILO_WORKER_ID=:VAR_ILO_REF_ID) ";
		$SQL.="UNION ";
		$SQL.="SELECT AA.* FROM ILO_LOG_TABLE AA WHERE ";
		$SQL.="ILO_WORKER_ID IN (SELECT ILO_CALLER_ID FROM ILO_LOG_TABLE WHERE ILO_WORKER_ID=:VAR_ILO_REF_ID) ";
		$SQL.=")YY ";
		$SQL.="CONNECT BY PRIOR ILO_WORKER_ID = ILO_CALLER_ID ";
		$SQL.=")XX ";
		$SQL.="START WITH ILO_CALLER_ID IS NULL ";
		$SQL.="CONNECT BY PRIOR ILO_WORKER_ID = ILO_CALLER_ID";
		
		$rsILO=awisOpenRecordset($comcon,$SQL, true, false, $BindeVariablen);
		$rsILOZeilen = $awisRSZeilen;
	}
	
	if ($rsILOZeilen>0)
	{
		awis_FORM_FormularStart();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift('LEVEL',50);
		awis_FORM_Erstelle_Liste_Ueberschrift('LEVEL_INFO',710);
		awis_FORM_Erstelle_Liste_Ueberschrift('WORKER_ID',180);
		awis_FORM_Erstelle_Liste_Ueberschrift('CALLER_ID',180);
		awis_FORM_ZeileEnde();
		
		for($ILOZeile=0;$ILOZeile<$rsILOZeilen;$ILOZeile++)
		{
			if(($ILO_LOG_KEY=='' && $rsILO['ILO_WORKER_ID'][$ILOZeile]==$ILO_REF_ID) || ($ILO_LOG_KEY==$rsILO['ILO_LOG_KEY'][$ILOZeile])){
				$style='font-weight: bold; font-size: 75%;';
				$LinkURL=0;
			} 
			else{
//				if ($rsILO['ILO_WORKER_ID'][$ILOZeile]==$ILO_REF_ID || $rsILO['ILO_CALLER_ID'][$ILOZeile]==$ILO_REF_ID){
//					$style='font-style: italic;';
//				}
//				else{			
					$style='font-size: 75%;';
//				}
				$LinkURL=1;
			}
			
			
			if ($LinkURL==1) {
				$Link_ILO='./operating_COM_Details_Errlog_ILO.php?ILO_REF_ID='.$ILO_REF_ID.'&ILO_LOG_KEY='.$rsILO['ILO_LOG_KEY'][$ILOZeile];
			}else {
				$Link_ILO='';
			}
			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_ListenFeld('LEVEL',$rsILO['LEVEL'][$ILOZeile],0,50,false,($ILOZeile%2),$style,'','Z');
			awis_FORM_Erstelle_ListenFeld('LEVEL_INFO',$rsILO['LEVEL_INFO'][$ILOZeile],0,710,false,($ILOZeile%2),$style,$Link_ILO,'T');	
			awis_FORM_Erstelle_ListenFeld('ILO_WORKER_ID',$rsILO['ILO_WORKER_ID'][$ILOZeile],0,180,false,($ILOZeile%2),$style,'','Z');	
			awis_FORM_Erstelle_ListenFeld('ILO_CALLER_ID',$rsILO['ILO_CALLER_ID'][$ILOZeile],0,180,false,($ILOZeile%2),$style,'','Z');	
			awis_FORM_ZeileEnde();
		}
		
		awis_FORM_Trennzeile();
		
		awis_FORM_FormularEnde();
		
		
		for($ILOZeile=0;$ILOZeile<$rsILOZeilen;$ILOZeile++)
		{
			if(($ILO_LOG_KEY==''?$rsILO['ILO_WORKER_ID'][$ILOZeile]==$ILO_REF_ID:$rsILO['ILO_LOG_KEY'][$ILOZeile]==$ILO_LOG_KEY))
			{
				awis_FORM_FormularStart();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('INST_ID',100);
				awis_FORM_Erstelle_Liste_Ueberschrift('SID',100);
				awis_FORM_Erstelle_Liste_Ueberschrift('SERIAL',100);
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('INST_ID',$rsILO['ILO_INST_ID'][$ILOZeile],0,100,false,($ILOZeile%2),'','','Z');	
				awis_FORM_Erstelle_ListenFeld('SID',$rsILO['ILO_SID'][$ILOZeile],0,100,false,($ILOZeile%2),'','','Z');	
				awis_FORM_Erstelle_ListenFeld('SERIAL',$rsILO['ILO_SERIAL'][$ILOZeile],0,100,false,($ILOZeile%2),'','','Z');	
				awis_FORM_ZeileEnde();
				
				awis_FORM_Trennzeile('O');
								
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('MODULE',200);
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('MODULE',$rsILO['ILO_MODULE'][$ILOZeile],0,800,false,($ILOZeile%2),'','','T');	
				awis_FORM_ZeileEnde();
				
				awis_FORM_Trennzeile('O');
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('ACTION',200);
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('ACTION',$rsILO['ILO_ACTION'][$ILOZeile],0,800,false,($ILOZeile%2),'','','T');	
				awis_FORM_ZeileEnde();

				awis_FORM_Trennzeile('O');
							
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('CLIENT_INFO',200);
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();				
				awis_FORM_Erstelle_ListenFeld('CLIENT_INFO',$rsILO['ILO_CLIENT_INFO'][$ILOZeile],0,800,false,($ILOZeile%2),'','','T');	
				awis_FORM_ZeileEnde();
								
				awis_FORM_Trennzeile('O');
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('START_TIME',250);
				awis_FORM_Erstelle_Liste_Ueberschrift('END_TIME',250);
				awis_FORM_Erstelle_Liste_Ueberschrift('ELAPSED_TIME_MS',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('WIDGET_COUNT',150);
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('START_TIME',$rsILO['ILO_STARTTIME'][$ILOZeile],0,250,false,($ILOZeile%2),'','','DU');	
				awis_FORM_Erstelle_ListenFeld('END_TIME',$rsILO['ILO_ENDTIME'][$ILOZeile],0,250,false,($ILOZeile%2),'','','DU');	
				awis_FORM_Erstelle_ListenFeld('ELAPSED_TIME_MS',$rsILO['ILO_ELAPSED_TIME_MS'][$ILOZeile],0,150,false,($ILOZeile%2),'','','Z');	
				awis_FORM_Erstelle_ListenFeld('WIDGET_COUNT',$rsILO['ILO_WIDGET_COUNT'][$ILOZeile],0,150,false,($ILOZeile%2),'','','Z');	
				awis_FORM_ZeileEnde();
				
				awis_FORM_Trennzeile('O');
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('LINE_NUMBER',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('CALLER_TYPE',200);
				awis_FORM_Erstelle_Liste_Ueberschrift('CALL_STACK',600);
				awis_FORM_ZeileEnde();
								
				awis_FORM_ZeileStart();
								awis_FORM_Erstelle_ListenFeld('LINE_NUMBER',$rsILO['ILO_LINE_NUMBER'][$ILOZeile],0,150,false,($ILOZeile%2),'','','T');	
				awis_FORM_Erstelle_ListenFeld('CALLER_TYPE',$rsILO['ILO_CALLER_TYPE'][$ILOZeile],0,200,false,($ILOZeile%2),'','','T');	

				//awis_FORM_Erstelle_ListenFeld('CALL_STACK',$rsILO['ILO_CALL_STACK'][$ILOZeile],0,750,false,($ILOZeile%2),'','','T');
				echo '<div class='.($ILOZeile%2?"ListenFeldHell":"ListenFeldDunkel").' style="min-width:600;width:600;;text-align:left;"><pre>'.$rsILO['ILO_CALL_STACK'][$ILOZeile].'</pre></div>';
				
				awis_FORM_ZeileEnde();
				
				
				
				awis_FORM_Trennzeile('O');
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('OERR_NUM',100);
				awis_FORM_Erstelle_Liste_Ueberschrift('ERROR_BACKTRACE',300);
				awis_FORM_Erstelle_Liste_Ueberschrift('ERROR_STACK',700);
				
				awis_FORM_ZeileEnde();
								
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('ILO_ERR_NUM',$rsILO['ILO_ERR_NUM'][$ILOZeile],0,100,false,($ILOZeile%2),'','','T');	
				awis_FORM_Erstelle_ListenFeld('ILO_ERR_BACKTRACE',$rsILO['ILO_ERR_BACKTRACE'][$ILOZeile],0,300,false,($ILOZeile%2),'','','T');	
				awis_FORM_Erstelle_ListenFeld('ILO_ERR_STACK',$rsILO['ILO_ERR_STACK'][$ILOZeile],0,700,false,($ILOZeile%2),'','','T');	
				//echo '<div class=ListenFeldDunkel style="min-width:700;width:700;;text-align:left;"><pre>'.$rsILO['ILO_ERR_STACK'][$ILOZeile].'</pre></div>';
				awis_FORM_ZeileEnde();
				
				awis_FORM_Trennzeile('O');
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('COMMENTS',800);
				awis_FORM_ZeileEnde();
				
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('COMMENTS',$rsILO['ILO_COMMENTS'][$ILOZeile],0,800,false,($ILOZeile%2),'','','T');	
				awis_FORM_ZeileEnde();
				
				awis_FORM_FormularEnde();
			}
		}
		
	}
	else
	{
		awis_FORM_FormularStart();
		awisFORM_Meldung(1,'Keine Informationen zur Anzeige vorhanden.');
		awis_FORM_FormularEnde();	
	}
	
	echo '<p><input type=button value="Schlie�en" onclick="javascript:self.close();">';
	
}
else {
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
}


echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

