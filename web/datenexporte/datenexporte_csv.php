<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('datenexporte_erstelleabfrage.php');

global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();

	ini_set('max_execution_time',600);

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XDE','%');
	$TextKonserven[]=array('XAB','XAB_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Feldname');
	$TextKonserven[]=array('Wort','Bedingung');
	$TextKonserven[]=array('Wort','EinmalAbfrage');
	$TextKonserven[]=array('Wort','GespeicherteAbfrage');
	$TextKonserven[]=array('Wort','BedingungHinzufuegen');
	$TextKonserven[]=array('Wort','NeueAbfrage');
	$TextKonserven[]=array('Wort','Ausgabe');
	$TextKonserven[]=array('Wort','DateiOeffnenLink');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','txtVerknuepfung');
	$TextKonserven[]=array('Liste','Bedingungen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	// Parameter
	$Trenner="\t";
	$AWIS_KEY1=$_GET['XDE_KEY'];
	$AWIS_KEY2=$_GET['XAB_KEY'];

	@ob_clean();

	//header("Cache-Control: no-cache, must-revalidate");
	//header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="datenexport.csv"');

	//***********************************************************************
	// Liste mit den Feldnamen aufbauen
	//***********************************************************************
	$SQL = 'SELECT XDF_TEXTKONSERVE,XDF_FELDNAME';
	$SQL .= ' FROM ExportDatenFelder';
	$SQL .= ' WHERE XDF_XDE_KEY = 0'.$_GET['XDE_KEY'];
	$SQL .= ' ORDER BY XDF_SORTIERUNG';
	$rsXDF = $DB->RecordSetOeffnen($SQL);

	$FeldLabels = array();
	
	//Zus�tzliche Einstellungen holen
	$XABSQL = 'SELECT XAB_UEBERSCHRIFT, XAB_TRENNZEICHEN_CSV FROM EXPORTABFRAGEN WHERE XAB_KEY='.$DB->WertSetzen('XAB', 'N0', $_GET['XAB_KEY']);
	$rsXAB = $DB->RecordSetOeffnen($XABSQL,$DB->Bindevariablen('XAB',true));
	
	$UeberschriftenTyp = $rsXAB->FeldInhalt('XAB_UEBERSCHRIFT');
	$Trenner = $rsXAB->FeldInhalt('XAB_TRENNZEICHEN_CSV');

	//Nachfolgende Zeilen sehen komisch aus, doch leider gibt es laut Internet keine bessere L�sung. 
	//--> Trennzeichen brauchen doppelte Hochkommas. Wenn man \t direkt von der Datenbank nehmen w�rde, w�rde auch \t exportiert werden und kein Tab. 
	switch($Trenner)
	{
		case '\n':
			$Trenner = "\n";
			break;
		case '\t':
			$Trenner = "\t";
			break;
		case 'Standard':
			$Trenner = "\t";
			break;
		default:
			$Trenner = $Trenner;
	}
	

	while(!$rsXDF->EOF())
	{
		$FeldName=$rsXDF->FeldInhalt('XDF_FELDNAME');

		if($UeberschriftenTyp == 0 or $UeberschriftenTyp == 3) //0 = Standard, 3 = Textkonserven
		{
			if($rsXDF->FeldInhalt('XDF_TEXTKONSERVE')!='')
			{
				$FeldKonserve = explode(':',$rsXDF->FeldInhalt('XDF_TEXTKONSERVE'));
				$Label = $Form->LadeTexte(array(array($FeldKonserve[0],$FeldKonserve[1])),$AWISBenutzer->BenutzerSprache());
				$FeldLabels[$FeldName]= $Label[$FeldKonserve[0]][$FeldKonserve[1]];
			}
			else
			{
				$Label = $Form->LadeTexte(array(array(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3),$rsXDF->FeldInhalt('XDF_FELDNAME'))),$AWISBenutzer->BenutzerSprache());
				$FeldLabels[$rsXDF->FeldInhalt('XDF_FELDNAME')]=$Label[substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3)][$rsXDF->FeldInhalt('XDF_FELDNAME')];
			}
		}
		elseif($UeberschriftenTyp==2) //Feldbezeichnungen aus Datenbank als �berschrift
		{
			$FeldLabels[$rsXDF->FeldInhalt('XDF_FELDNAME')]=$rsXDF->FeldInhalt('XDF_FELDNAME');
		}		

		$rsXDF->DSWeiter();
	}

	// Blockgr��e festlegen
	$Blockgroesse = 5000;

	for($i=1;$i<999;$i++)
	{
		$Abfrage = ''.ErstelleAbfrage($DB, $_GET['XDE_KEY'], $_GET['XAB_KEY']).'';

		$SQL = 'SELECT ABFRAGE2.* FROM (';
		$SQL .= 'SELECT ABFRAGE1.*, row_number() over(order by 1) AS zeile';
		$SQL .= ' FROM ('.$Abfrage.') ABFRAGE1';
		$SQL .= ') ABFRAGE2 WHERE zeile >= '.(($i-1)*$Blockgroesse).' AND zeile < '.(($i)*$Blockgroesse);
       
		$rsDaten = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1, $SQL);
		if($rsDaten->AnzahlDatensaetze()==0)
		{
			break;
		}
		if($i==1)		// Beim ersten Mal zus�tzliche Dinge tun..
		{
			$Felder = $rsDaten->SpaltenNamen();
			$Zeile = '';
			if($UeberschriftenTyp!=1)//nicht wenn "keine" �berschrift geschrieben werden soll
			{
				foreach($Felder AS $FeldName)
				{
					if($FeldName!='ZEILE')
					{
						$Zeile .= $Trenner.$FeldLabels[$FeldName].'';
					}
				}
				echo substr($Zeile,1)."\n";
			}		
		}
		while(!$rsDaten->EOF())
		{
			$Zeile = '';
			foreach($Felder AS $FeldName)
			{
				if($FeldName!='ZEILE')
				{
					$Text = str_replace("\n",'',$rsDaten->FeldInhalt($FeldName));
					$Text = str_replace("\r",'',$Text);
					$Text = str_replace("\n",'',$Text);
					$Text = str_replace("\t",'',$Text);
					$Zeile .= $Trenner.$Text;
				}
			}
			echo substr($Zeile,1)."\n";

			$rsDaten->DSWeiter();
		}
	}
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
?>