<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>

<?php
/****************************************************************************************************
*
* 	Lieferanten
*
* 	Pflege der Lieferanten
*
* 	Autor: 	Sacha Kerres
* 	Datum:	Dez. 2003
*
****************************************************************************************************/
// Variablen
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();
$Rechtestufe = awisBenutzerRecht($con, 605) ;
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		
$RechtFilialen = awisBenutzerRecht($con, 409);		

if($Rechtestufe==0 and $RechtFilialen==0)
{
    awisEreignis(3,1000,'Lieferanten',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

	// Kein spezieller angegeben -> suchen
if(!isset($_GET['LIENR']) or isset($_GET['Liste']))
{
	$SQL = "SELECT * FROM AWIS.LIEFERANTEN";
}
else
{
	$SQL = "SELECT * FROM AWIS.LIEFERANTEN WHERE LIE_NR='". $_GET['LIENR'] ."'";
}

if(isset($_GET['Sort']))
{
	$SQL .= " ORDER BY " . $_GET['Sort'];
}
else
{
	$SQL .= " ORDER BY LIE_NR";
}


$rsLIE = awisOpenRecordset($con, $SQL);
$rsLIEZeilen = $awisRSZeilen;
if($rsLIEZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<tr><td id=FeldBez><a href=./lieferanten.php?Sort=LIE_NR>Nummer</a></td><td id=FeldBez><a href=./lieferanten.php?Sort=LIE_NAME1>Bezeichnung</a></td></tr>';

	for($Zeile=0;$Zeile<$rsLIEZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./lieferanten.php?LIENR=" . $rsLIE['LIE_NR'][$Zeile] . ">" . $rsLIE['LIE_NR'][$Zeile] . "</a></td><td id=DatenFeld>" . $rsLIE['LIE_NAME1'][$Zeile] . "</td></tr>";
	}

	echo '</table>';
}
elseif($rsLIEZeilen == 0)	// Liste anzeigen
{
	echo '<span class=HinweisText>Mit den angegeben Parametern kann kein Lieferant gefunden werden</span>';
}
else					// einzelnen Lieferanten anzeigen
{
	echo '<form action=./lieferanten.php method=post name=frmLieferanten>';

	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Lieferantenstamm</h3></td>';
	echo '<td><font size=1>' . $rsLIE['LIE_USER'][0] . '<br>' . $rsLIE['LIE_USERDAT'][0] . '</font></td>';
    
	if ($Rechtestufe <> 0)
	{
		echo "<td width=40 backcolor=#000000 align=right><input type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./lieferanten.php?Liste=True';></td>";
	}
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=400><col width=100><col width=*></colgroup>';
	echo '<tr>';
	echo '<td id=FeldBez>Lieferantennummer</td>';
	echo '<td>' . $rsLIE['LIE_NR'][0] . '</td>';
	$MitReklInfo = awis_BenutzerParameter($con,"ReklamationsInfoLieferanten" , $_SERVER['PHP_AUTH_USER']);
	if($MitReklInfo and $Rechtestufe <> 0)
	{
		$DateiName = realpath('../dokumente/lieferanten/reklamationen') . '/lief_rekl_' . $rsLIE['LIE_NR'][0] . '.pdf';
		if(file_exists($DateiName))
		{
			print "<td id=FeldBez>Rekl.-Info</td><td><a href=../dokumente/lieferanten/reklamationen/lief_rekl_" . $rsLIE['LIE_NR'][0] . ".pdf><img border=0 src=/bilder/pdf.png alt=Anleitung onmouseover=\"window.status='Reklamation';return true;\" onmouseout=\"window.status='';return true;\" onFocus=\"window.status='Reklamation';return true;\"></a></td>";
		}
	}
	echo '</tr>';

	echo '<tr>';
	echo '<td id=FeldBez>Name1</td>';
	echo '<td>' . $rsLIE['LIE_NAME1'][0] . '</td>';
	echo '<td id=FeldBez>Name2</td>';
	echo '<td>' . $rsLIE['LIE_NAME2'][0] . '</td>';
	echo '</tr>';

	if ($Rechtestufe <> 0)
	{
		echo '<tr>';
		echo '<td id=FeldBez>Ansprechpartner</td>';
		echo '<td>' . $rsLIE['LIE_ANSPRECHP1'][0] . '</td>';
		echo '<td id=FeldBez>Im WWS</td>';
		echo '<td>' . ($rsLIE['LIE_BEKANNTWW'][0]==0?'Nein':'Ja') . '</td>';
		echo '</tr>';
	}

	echo '<tr>';
	echo '<td id=FeldBez>Strasse</td>';
	echo '<td>' . $rsLIE['LIE_STRASSE'][0] . '</td>';
	echo '<td id=FeldBez>Land/Plz/Ort</td>';
	echo '<td>' . ($rsLIE['LIE_LAND'][0]!=''?$rsLIE['LIE_LAND'][0].' - ':'') . '' . $rsLIE['LIE_PLZ'][0] . ' ' . $rsLIE['LIE_ORT'][0] . '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td id=FeldBez>Telefon</td>';
	echo '<td>' . $rsLIE['LIE_TELEFON'][0] . '</td>';
	echo '<td id=FeldBez>Faxnummer</td>';
	echo '<td>' . $rsLIE['LIE_TELEFAX'][0] . '</td>';
	echo '</tr>';

	if ($Rechtestufe <> 0)
	{
		echo '<tr><td colspan=4 id=FeldBez align=center><b>Reklamationen an</b></td></tr>';
	
		echo '<tr>';
		echo '<td id=FeldBez>Firma</td>';
		echo '<td colspan=3>' . $rsLIE['LIE_REKLNAME'][0] . '</td>';
		echo '</tr>';
	
		echo '<tr>';
		echo '<td id=FeldBez>Strasse</td>';
		echo '<td>' . $rsLIE['LIE_REKLSTRASSE'][0] . '</td>';
		echo '<td id=FeldBez>Plz/Ort</td>';
		echo '<td>' . $rsLIE['LIE_REKLPLZ'][0] . ' ' . $rsLIE['LIE_REKLORT'][0] . '</td>';
		echo '</tr>';
	}

	echo '</table>';

	if ($Rechtestufe <> 0)
	{
		$SQL = 'SELECT DISTINCT APR_AST_ATUNR, APR_LAR_LARTNR, APR_BEARBEITER, APR_BEARBEITUNGSTAG, LAR_LARTNR';
		$SQL .= ' FROM ArtikelPruefungen LEFT OUTER JOIN LieferantenArtikel ON APR_LIE_NR = LAR_LIE_NR AND APR_LAR_LARTNR = LAR_LARTNR';
		$SQL .= ' WHERE APR_LIE_NR=\'' . $rsLIE['LIE_NR'][0] . '\'';
		$SQL .= ' ORDER BY APR_AST_ATUNR';
	
		$rsAPR = awisOpenRecordset($con, $SQL);
		$rsAPRZeilen = $awisRSZeilen;
	
		echo '<table border=1>';
		echo '<tr><td colspan=4 class=FeldBez>Artikelpr�fungen</td></tr>';
		echo '<tr class=FeldBez><td>ATU Nummer</td>';
		echo '<td class=FeldBez>Lieferantenartikel</td>';
		echo '<td class=FeldBez>Erfasst am</td>';
		echo '<td class=FeldBez>Erfasst von</td>';
		echo '</tr>';
	
		for($i=0;$i<$rsAPRZeilen;$i++)
		{
			echo '<tr>';
			echo '<td><a href=/ATUArtikel/artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Pruefungen&ATUNR='.$rsAPR['APR_AST_ATUNR'][$i].'>'.$rsAPR['APR_AST_ATUNR'][$i].'</td>';
			echo '<td>' . $rsAPR['APR_LAR_LARTNR'][$i] . '</td>';
			echo '<td>' . $rsAPR['APR_BEARBEITUNGSTAG'][$i] . '</td>';
			echo '<td>' . $rsAPR['APR_BEARBEITER'][$i] . '</td>';
			echo '</tr>';
		}
		echo '</table>';
	}

	echo '</form>';
}

if(isset($_GET['Zurueck']) && $_GET['Zurueck']=='artikel_LIEF')
{
	$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $_GET['ASTKEY'] . '&cmdAktion=ArtikelInfos';
}
elseif(isset($_GET['Zurueck']) && $_GET['Zurueck']=='artikel_APR')
{
	$ZurueckLink = '/ATUArtikel/artikel_Main.php?ATUNR=' . $_GET['ASTNR'] . '&cmdAktion=ArtikelInfos&Seite=Pruefungen';
}
elseif(isset($_GET['Zurueck']))
{
	$ZurueckLink = str_replace('**','#',str_replace('~~', '&', $_GET['Zurueck']));
}
else
{
	$ZurueckLink = './stammdaten_Main.php';
}

print "<br><hr><input type=image alt='Zur�ck (Alt+z)' src=/bilder/zurueck.png name=cmdZurueck accesskey=z onclick=location.href='$ZurueckLink';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=lieferanten','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awisLogoff($con);

?>
