<?php

class AuftraegeProTagResponse
{

    /**
     * @var AuftragsListe $AuftraegeProTagResult
     */
    protected $AuftraegeProTagResult = null;

    /**
     * @param AuftragsListe $AuftraegeProTagResult
     */
    public function __construct($AuftraegeProTagResult)
    {
      $this->AuftraegeProTagResult = $AuftraegeProTagResult;
    }

    /**
     * @return AuftragsListe
     */
    public function getAuftraegeProTagResult()
    {
      return $this->AuftraegeProTagResult;
    }

    /**
     * @param AuftragsListe $AuftraegeProTagResult
     * @return AuftraegeProTagResponse
     */
    public function setAuftraegeProTagResult($AuftraegeProTagResult)
    {
      $this->AuftraegeProTagResult = $AuftraegeProTagResult;
      return $this;
    }

}
