<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	if(isset($_POST['txtXBV_KEY']))
	{
		$AWIS_KEY1=$_POST['txtXBV_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtXBV_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('XBV','XBV_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('XBV_DATUMVOM','XBV_DATUMBIS','XBV_VERTRETER_XBN_KEY');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['XBV'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtXBV_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO benutzervertreter';
				$SQL .= '(XBV_XBN_KEY,XBV_VERTRETER_XBN_KEY,XBV_DATUMVOM,XBV_DATUMBIS,XBV_GRUND';
				$SQL .= ',XBV_USER,XBV_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtXBV_XBN_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtXBV_VERTRETER_XBN_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtXBV_DATUMVOM'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtXBV_DATUMBIS'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtXBV_GRUND'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				$DB->Ausfuehren($SQL);

				$SQL = 'SELECT seq_XBV_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');

				// Benachrichtigen, wenn ein fremder die Vertretung eintr�gt
				if($AWISBenutzer->BenutzerKontaktKEY()!=$_POST['txtXBV_XBN_KEY'])
				{
				    require_once 'awisMailer.inc';
    				
				    $ZielMail = $AWISBenutzer->EMailAdresse($_POST['txtXBV_XBN_KEY']);
				    if($ZielMail!='')
				    {
    				    $Mail = new awisMailer($DB, $AWISBenutzer);
        				$Mail->Absender('awis@de.atu.eu');
        				$Mail->Betreff('Vertretung eingetragen');
        				$Mail->AdressListe(awisMailer::TYP_TO,$ZielMail);
        				$Text = 'Der Anwender '.$AWISBenutzer->BenutzerName();
        				$Text .= ' hat f&uuml; Sie eine Vertretung '.$AWISBenutzer->EMailAdresse($_POST['txtXBV_VERTRETER_XBN_KEY']);
        				$Text .= ' f&uuml;r den Zeitraum '.$Form->Format('D',$_POST['txtXBV_DATUMVOM']);
        				$Text .= ' - '.$Form->Format('D',$_POST['txtXBV_DATUMBIS']);
        				$Text .= ' eingetragen.<br><br>Ihr AWIS Team';
        				$Mail->Text($Text);
        				$Mail->SetzeBezug('XBV', $AWIS_KEY1);
        				
        				$Mail->MailInWarteschlange();
				    }
				}				
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsSVT = $DB->RecordSetOeffnen('SELECT * FROM benutzervertreter WHERE XBV_Key=' . $_POST['txtXBV_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$rsSVT->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVT->FeldInhalt('XBV_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE benutzervertreter SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', XBV_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', XBV_userdat=sysdate';
					$SQL .= ' WHERE XBV_KEY=0' . $_POST['txtXBV_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}
}
catch (awisException $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
    $Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>