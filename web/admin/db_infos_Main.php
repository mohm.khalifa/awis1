<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>

</head>

<body>
<?php
global $awisDBFehler;

include ("ATU_Header.php");	// Kopfzeile

clearstatcache();

awis_ZeitMessung(0);		// Zeit setzen

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m&ouml;glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,10);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'DB-Status',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$cmdAktion='';

if (isset($_GET['cmdAktion']))
{
	$cmdAktion=$_GET['cmdAktion'];
}

awis_RegisterErstellen(70, $con, $cmdAktion);

print "<br><input type=image title=Zur&uuml;ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . (isset($_GET['ZurueckLink'])?$_GET['ZurueckLink']:'./index.php') . "';>";

//echo "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=dbstatus&HilfeBereich=','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awislogoff($con);

echo '<p><font size=1>Diese Seite wurde in ' . sprintf('%.4f', awis_ZeitMessung(1)) . ' Sekunden erstellt.';

?>
</body>
</html>

