<?php

require_once("db.inc.php");

global $COMWebSrv;
global $COMAppSrv;

$Level = awisLeseAWISParameter('awis.conf','AWIS_LEVEL');
	
switch($Level)
{
	case 'PROD':
		$COMWebSrv = "http://svapplx001.server.atu.de";
		$COMAppSrv = "svapplx001.server.atu.de";
		break;
	default:
		$COMWebSrv = "http://svapplx012.server.atu.de";
		$COMAppSrv = "svapplx012.server.atu.de";
		break;
}

?>
