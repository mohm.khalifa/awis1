<?php
global $AWISCursorPosition;
global $AWISBenutzer;


$Form = new awisFormular();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('SCH','%');
$TextKonserven[]=array('PVS','lst_Waehlen');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Liste','lst_ALLE_0');


$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht25040=$AWISBenutzer->HatDasRecht(25040);

echo "<form name=frmSuche method=post action=./schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht>";

/**********************************************
* * Eingabemaske
***********************************************/
//$Param = unserialize($AWISBenutzer->ParameterLesen('SCHSuche'));

$Form->Formular_Start();

if($Recht25040 == 0)
{
    $Form->Fehler_KeineRechte();
}
	
$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_SchluesselNr'].':',180);
$SQL1 = 'SELECT sch_key, sch_Bezeichnung from SchlSchluessel';
$Form->Erstelle_SelectFeld('*SCH_Key',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SCH_Key']:''),50,true,$SQL1,$AWISSprachKonserven['PVS']['lst_Waehlen']);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Tuer'].':',180);
$SQL2 = 'SELECT tur_key, tur_Bezeichnung from SchlTueren';
$Form->Erstelle_SelectFeld('*TUR_Key',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['TUR_Key']:''),50,true,$SQL2,$AWISSprachKonserven['PVS']['lst_Waehlen']);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Mitarbeiter'].':',180);
$SQL3 = 'SELECT KON_Key, KON_Name1|| \', \' || KON_Name2 FROM Kontakte WHERE KON_Key IN (SELECT MAS_KON_Key FROM SchlMitarbeiterSchluessel)';
$Form->Erstelle_SelectFeld('*KON_Key',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['KON_Key']:''),50,true,$SQL3,$AWISSprachKonserven['PVS']['lst_Waehlen']);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Historie'].':',180);
$Form->Erstelle_Checkbox('*SCH_Historie',(isset($Param['SCH_Historie']) && $Param['SCH_Historie']=='on'?'on':''),50,true,'on');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
$Form->Erstelle_Checkbox('*AuswahlSpeichern',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?'on':''),50,true,'on');
$Form->ZeileEnde();

$Form->Formular_Ende();

$Form->SchaltflaechenStart();

$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

$Form->SchaltflaechenEnde();

?>
