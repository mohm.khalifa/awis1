<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?php
    require_once 'asesplandaten_funktionen.php';

    global $AWISCursorPosition;
    global $APD;

    try {
    $APD = new asesplandaten_funktionen();


    echo "<link rel=stylesheet type=text/css href='" . $APD->AWISBenutzer->CSSDatei(3) . "'>";

    echo '<title>' . $APD->AWISSprachKonserven['TITEL']['tit_APD'] . '</title>';
    ?>
</head>
<body>
<?php
include("awisHeader3.inc");    // Kopfzeile

echo "<script type=\"application/javascript\" src=\"asesplandaten.js\"></script>";
$APD->RechteMeldung(0); //Anzeigenrecht?

$Register = new awisRegister(74000);
$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

$APD->Form->SetzeCursor($AWISCursorPosition);

} catch (awisException $ex) {
    if ($APD->Form instanceof awisFormular) {
        $APD->Form->DebugAusgabe(1, $ex->getSQL());
        $APD->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "202009161520");
    } else {
        $APD->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($APD->Form instanceof awisFormular) {
        $APD->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "202009161521");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
</body>
</html>