<?php
/**
 * awisFormulare.inc
 * 
 * Klasse zur Darstellung von AWIS-Formularen
 * 
 * @author Sacha Kerres
 * @copyright ATU - Auto Teile Unger
 * @version 200807
 * @package AwisFramework
 */

require_once('awisDatenbank.inc');

class awisFormulare
{
	
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;
	
	/**
	 * Initialisierung
	 *
	 */
	public function __construct()
	{
		// Datenbankverbindung �ffnen
		$this->_DB = new awisDatenbank('AWIS');
		
	}
	
	
	
}



?>