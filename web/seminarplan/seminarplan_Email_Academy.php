<?php
global $AWIS_KEY2;				// UnfallMeldungenInfoTypen: UFT_KEY
global $FehlerAugetreten;		// Flag: Beim Speichern ist ein Fehler aufgetreten (z.B. Pflichtfeld nicht gef�llt)
global $ListeAnzeigen;			// Flag: Liste anzeigen (nicht Detailansicht) 
require_once 'awisMailer.inc';
try
{
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    // $Debug = 'SET_KEY = ' . $_GET['SET_ANAB_KEY'] . '<br> SET_STATUSEMAIL = ' . $_GET['SET_STATUSEMAIL'];
    
    //$Form->DebugAusgabe(2,$Debug);
   

    if (isset($_GET['ANABMELDUNG']))
    {
        //Email an Academy bei Hotelbuchung.

        $SQL ='SELECT';
        $SQL .='   lpad(SET_FIL_NR,4,\'0\') AS FILIALE,';
        $SQL .='   SEP_KEY, SEMINARRAUMADRESSEN.*,';
        $SQL .='   SEP_SEMINAR,';
        $SQL .='   SEP_BESCHREIBUNG,';
        $SQL .='   SEP_TRAINING,';
        $SQL .='   SET_KEY,';
        $SQL .='   SET_NACHNAME,SET_HOTEL_VON, SET_HOTEL_BIS,';
        $SQL .='   SET_VORNAME,';
        $SQL .='   SET_ABSAGEGRUND, SEP_SEMINARSTATUS,';
        $SQL .='   SET_STATUSEMAIL,';
        $SQL .='   SET_TEILNAHME,';
        $SQL .='   SER_RAUM,';
        $SQL .='   to_char(SEP_VON, \'dd.mm.yyyy\') as SEP_VON,';
        $SQL .='   to_char(SEP_BIS, \'dd.mm.yyyy\') as SEP_BIS,';
        $SQL .='   SET_PER_NR,to_char(MVT_BETREFF) as BETREFF, MVT_TEXT as MAILTEXT,';
        $SQL .='   (SELECT lower(laender.lan_code) as lan_code from AWIS.FILIALEN';
        $SQL .='       INNER JOIN awis.laender on fil_lan_wwskenn = lan_wwskenn';
        $SQL .='       WHERE FIL_ID = SET_FIL_NR) as Mailland,';
        $SQL .='         (select Name from V_FILIALE_GBL where fil_id = SET_FIL_NR) as GBL_NACHNAME,';
        $SQL .='         (select vorname from V_FILIALE_GBL where fil_id = SET_FIL_NR) as GBL_VORNAME,';
        $SQL .='         (select EMAIL_ADRESSE from V_FILIALE_GBL where fil_id = SET_FIL_NR) as GBL_EMAIL, ';
        $SQL .='         (select EMAIL_ADRESSE from V_FILIALE_RL where fil_id = SET_FIL_NR and rownum = 1) as RL_EMAIL ';
        $SQL .=' FROM';
        $SQL .='   AWIS.SEMINARPLAN';
        $SQL .=' INNER JOIN seminarteilnehmer';
        $SQL .=' ON';
        $SQL .='   sep_key = set_sep_key';
        $SQL .=' INNER JOIN seminarraeume ';
        $SQL .=' ON';
        $SQL .='  SEP_SER_KEY = SER_KEY
            INNER JOIN MAILVERSANDTEXTE MVT
            on MVT.MVT_BEREICH = \'SET_HOTEL_ACADEMY\'
            inner join seminarraumadressen
            on SER_RAUMNR = SPA_RAUMID ';
        $SQL .=' WHERE';
        $SQL .='   SEP_SCHULUNGSART           = 1 and set_key ='  . $_GET['SET_ANAB_KEY'];
         
        
        $rsMail = $DB->RecordSetOeffnen($SQL);
        
        
        $Mail = new awisMailer($DB, $AWISBenutzer);          // Mailer-Klasse instanzieren
        $Werkzeuge = new awisWerkzeuge();
        $Absender = 'noreply@de.atu.eu';                     // Absender der Mails
        
         
         
        
        if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
        {
            $Empfaenger = 'academy.hotel@de.atu.eu';
            $EmpfaengerCC = '';
        }
        else
        {
            $Empfaenger = 'shuttle@de.atu.eu';
            $EmpfaengerCC = '';
        }
       
        
        $EmpfaengerBCC = '';
        
		$Betreff = $rsMail->FeldInhalt('BETREFF'); 
		$Betreff = str_replace('#Personalnummer#', $rsMail->FeldInhalt('SET_PER_NR'), $Betreff) ;
		$Betreff = str_replace('#Terminnummer#', $rsMail->FeldInhalt('SEP_TRAINING'), $Betreff) ;
        $Betreff = str_replace('#Vorname#', $rsMail->FeldInhalt('SET_VORNAME'), $Betreff) ;
		$Betreff = str_replace('#Nachname#', $rsMail->FeldInhalt('SET_NACHNAME'), $Betreff) ;
		$Betreff = str_replace('#Region#', $rsMail->FeldInhalt('SET_REGION'), $Betreff) ;
        $Text = $rsMail->FeldInhalt('MAILTEXT');
		$Text = str_replace('#Filialnummer#', $rsMail->FeldInhalt('FILIALE'), $Text) ;
		$Text = str_replace('#Personalnummer#', $rsMail->FeldInhalt('SET_PER_NR'), $Text) ;
		$Text = str_replace('#Seminartitel#', $rsMail->FeldInhalt('SEP_BESCHREIBUNG'), $Text) ;
		$Text = str_replace('#Terminnummer#', $rsMail->FeldInhalt('SEP_TRAINING'), $Text) ;
		$Text = str_replace('#Region#', $rsMail->FeldInhalt('SER_RAUM'), $Text) ;
        $Text = str_replace('#Vorname#', $rsMail->FeldInhalt('SET_VORNAME'), $Text) ;
		$Text = str_replace('#Nachname#', $rsMail->FeldInhalt('SET_NACHNAME'), $Text) ;
		$Text = str_replace('#Datumvon#', $Form->Format('D0',$rsMail->FeldInhalt('SET_HOTEL_VON')), $Text) ;
        $Text = str_replace('#Datumbis#', $Form->Format('D0',$rsMail->FeldInhalt('SET_HOTEL_BIS')), $Text) ;
        $KW = date('W',strtotime($rsMail->FeldInhalt('SET_HOTEL_BIS')));
        $Text = str_replace('#KW#',$KW,$Text);
        $Text = str_replace('#Strasse#', $rsMail->FeldInhalt('SPA_STRASSE'), $Text) ;
        $Text = str_replace('#Ort#', $rsMail->FeldInhalt('SPA_ORT'), $Text) ;
        $Text = str_replace('#PLZ#', $rsMail->FeldInhalt('SPA_POSTLEITZAHL'), $Text) ;
        $Text = str_replace('#Lokationsname#', $rsMail->FeldInhalt('SPA_LOKATIONSNAME'), $Text) ;
        
        $Form->DebugAusgabe(1, "Betreff: " . $Betreff . "<br>");
        $Form->DebugAusgabe(1, "Text: " . $Text . "<br>");
        $Form->DebugAusgabe(1,  "Empf�nger: " . $Empfaenger);
        
        $Mail->AnhaengeLoeschen();
        $Mail->LoescheAdressListe();
        $Mail->DebugLevel(0);
        $Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
        $Mail->AdressListe(awisMailer::TYP_CC, $EmpfaengerCC, false, false);
        $Mail->AdressListe(awisMailer::TYP_BCC, $EmpfaengerBCC, false, false);
        $Mail->Absender($Absender);
        $Mail->Betreff($Betreff);
        $Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
        $Mail->SetzeBezug('SET',$_GET['SET_ANAB_KEY']); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
        $Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
        //$Mail->MailSenden();
		$Mail->MailInWarteschlange();
        
        
        //Status des Datensatzes auf Versendet manuell setzen
        $SQL ='UPDATE ';
        $SQL .='seminarteilnehmer ';
        $SQL .='SET ';
        $SQL .='SET_STATUSEMAIL_HOTEL           = 1,';
        $SQL .=' SET_EMAILVERSANDZEITPUNKT_HOTE = sysdate';
        $SQL .=' WHERE';
        $SQL .=' set_key = ' . $_GET['SET_ANAB_KEY'];
        
        if($DB->Ausfuehren($SQL)===false)
        {
            awisErrorMailLink('seminarplan_emailHOTEL_1',1,$awisDBError['messages'],'');
        }
    }
    elseif (isset($_GET['SET_ABSAGEBEST']))
    {
        //Email an Academy wenn der GBL die Absage best�tigt hat.
        $SQL ='SELECT';
        $SQL .='   lpad(SET_FIL_NR,4,\'0\') AS FILIALE,';
        $SQL .='   SEP_KEY,';
        $SQL .='   SEP_SEMINAR,';
        $SQL .='   SEP_BESCHREIBUNG,';
        $SQL .='   SEP_TRAINING,';
        $SQL .='   SET_KEY,';
        $SQL .='   SET_NACHNAME,';
        $SQL .='   SET_VORNAME,';
        $SQL .='   SET_ABSAGEGRUND, SEP_SEMINARSTATUS,';
        $SQL .='   SET_STATUSEMAIL,';
        $SQL .='   SET_TEILNAHME,';
        $SQL .='   SER_RAUM,';
        $SQL .='   to_char(SEP_VON, \'dd.mm.yyyy\') as SEP_VON,';
        $SQL .='   to_char(SEP_BIS, \'dd.mm.yyyy\') as SEP_BIS,';
        $SQL .='   SET_PER_NR,to_char(MVT_BETREFF) as BETREFF, MVT_TEXT as MAILTEXT,';
        $SQL .='   (SELECT lower(laender.lan_code) as lan_code from AWIS.FILIALEN';
        $SQL .='       INNER JOIN awis.laender on fil_lan_wwskenn = lan_wwskenn';
        $SQL .='       WHERE FIL_ID = SET_FIL_NR) as Mailland,';
        $SQL .='         (select Name from V_FILIALE_GBL where fil_id = SET_FIL_NR and rownum = 1) as GBL_NACHNAME,';
        $SQL .='         (select vorname from V_FILIALE_GBL where fil_id = SET_FIL_NR and rownum = 1) as GBL_VORNAME,';        
        $SQL .='         (select EMAIL_ADRESSE from V_FILIALE_GBL where fil_id = SET_FIL_NR and rownum = 1) as GBL_EMAIL, ';     
        $SQL .='         (select EMAIL_ADRESSE from V_FILIALE_RL where fil_id = SET_FIL_NR and rownum = 1) as RL_EMAIL ';
        $SQL .=' FROM';
        $SQL .='   AWIS.SEMINARPLAN';
        $SQL .=' INNER JOIN seminarteilnehmer';
        $SQL .=' ON';
        $SQL .='   sep_key = set_sep_key';
        $SQL .=' INNER JOIN seminarraeume ';
        $SQL .=' ON';
        $SQL .='  SEP_SER_KEY = SER_KEY
            INNER JOIN MAILVERSANDTEXTE MVT
            on MVT.MVT_BEREICH = \'SET_ABSAGEBEST_ACADEMY\'';
        $SQL .=' WHERE';
        $SQL .='   SEP_SCHULUNGSART           = 1 and set_key ='  . $_GET['SET_ABSAGEBEST_KEY'];
     
        
        
        
        
        $rsMail = $DB->RecordSetOeffnen($SQL);
        
        
        $Mail = new awisMailer($DB, $AWISBenutzer);          // Mailer-Klasse instanzieren
        $Werkzeuge = new awisWerkzeuge();
        $Absender = 'noreply@de.atu.eu';                     // Absender der Mails
        
         
         
        
        if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
        {
            $Empfaenger = 'academy.storno@de.atu.eu';
            $EmpfaengerCC = $rsMail->FeldInhalt('RL_EMAIL')==''?'shuttle@de.atu.eu':$rsMail->FeldInhalt('GBL_EMAIL');
        }
        else
        {
            $Empfaenger = 'shuttle@de.atu.eu';
            $EmpfaengerCC = '';
        }
        
        
        $EmpfaengerBCC = '';
       
        $Betreff = $rsMail->FeldInhalt('BETREFF'); //Betreff Massnahme versandt
        $Betreff = str_replace('#Filialnummer#', $rsMail->FeldInhalt('filiale'), $Betreff) ;
        $Betreff = str_replace('#Personalnummer#', $rsMail->FeldInhalt('set_per_nr'), $Betreff) ;
        $Betreff = str_replace('#Seminartitel#', $rsMail->FeldInhalt('SEP_BESCHREIBUNG'), $Betreff) ;
        $Betreff = str_replace('#Terminnummer#', $rsMail->FeldInhalt('SEP_TRAINING'), $Betreff) ;
        $Text = $rsMail->FeldInhalt('MAILTEXT');
        $Text = str_replace('#Filialnummer#', $rsMail->FeldInhalt('filiale'), $Text) ;
        $Text = str_replace('#Personalnummer#', $rsMail->FeldInhalt('set_per_nr'), $Text) ;
        $Text = str_replace('#Seminartitel#', $rsMail->FeldInhalt('SEP_BESCHREIBUNG'), $Text) ;
        $Text = str_replace('#Terminnummer#', $rsMail->FeldInhalt('SEP_TRAINING'), $Text) ;
        $Text = str_replace('#Vorname#', $rsMail->FeldInhalt('SET_VORNAME'), $Text) ;
        $Text = str_replace('#Nachname#', $rsMail->FeldInhalt('SET_NACHNAME'), $Text) ;
        $Text = str_replace('#Datumvon#', $rsMail->FeldInhalt('SEP_VON'), $Text) ;
        $Text = str_replace('#Datumbis#', $rsMail->FeldInhalt('SEP_BIS'), $Text) ;
        $Text = str_replace('#Vorname GBL#', $rsMail->FeldInhalt('GBL_VORNAME'), $Text) ;
        $Text = str_replace('#Nachname GBL#', $rsMail->FeldInhalt('GBL_NACHNAME'), $Text) ;
        $Text = str_replace('#Stornogrund#', $rsMail->FeldInhalt('SET_ABSAGEGRUND'), $Text) ;
        
        
        $Form->DebugAusgabe(1, "Betreff: " . $Betreff . "<br>");
        $Form->DebugAusgabe(1, "Text: " . $Text . "<br>");
        $Form->DebugAusgabe(1, "Empf�nger: " . $Empfaenger);
        
        $Mail->AnhaengeLoeschen();
        $Mail->LoescheAdressListe();
        $Mail->DebugLevel(0);
        $Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
        $Mail->AdressListe(awisMailer::TYP_CC, $EmpfaengerCC, false, false);
        $Mail->AdressListe(awisMailer::TYP_BCC, $EmpfaengerBCC, false, false);
        $Mail->Absender($Absender);
        $Mail->Betreff($Betreff);
        $Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
        $Mail->SetzeBezug('SET',$_GET['SET_ABSAGEBEST_KEY']); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
        $Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
        $Mail->MailInWarteschlange();
        //$Mail->MailSenden();
        
        
        //Status des Datensatzes auf Versendet manuell setzen
        $SQL ='UPDATE ';
        $SQL .='seminarteilnehmer ';
        $SQL .='SET ';
        $SQL .='SET_STATUSEMAIL_ACAD           = 1,';
        $SQL .=' SET_EMAILVERSANDZEITPUNKT_ACAD = sysdate';
        $SQL .=' WHERE';
        $SQL .=' set_key = ' . $_GET['SET_ABSAGEBEST_KEY'];
        
        if($DB->Ausfuehren($SQL)===false)
        {
            awisErrorMailLink('seminarplan_emailACAD_1',1,$awisDBError['messages'],'');
        }
    }
    
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}

?>