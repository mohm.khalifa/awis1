<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Preislistenpr�fungen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $con;
global $comcon;
global $AWISBenutzer;
global $awisRSZeilen;

print "<link rel=stylesheet media=screen type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

?>
</head>

<body bgcolor=#3399ff >
<?php
$con = awislogon();
$comcon = awisLogonComCluster('DE',false);
awisExecute($comcon, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'");

$BindeVariablen=array();
$BindeVariablen['var_N0_pph_key']=$_GET['PPHKEY'];

$SQL = 'SELECT DISTINCT BEZEICHNUNG, PPH_TRANSAKTIONSID, PPH_BEGINN, PPH_ENDE, PPH_STATUS';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN PLP';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTEN PTA ON PLP_PTA_ID=PTA_ID  ';
$SQL .= ' INNER JOIN EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB PLI ON ID_FPLB=PLP_ID_FPLB';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST PPH ON PPH_PLP_KEY =  PLP_Key';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT PAU ON PAU_PPH_KEY = PPH_KEY';
$SQL .= ' WHERE PPH_Key= :var_N0_pph_key';

$rsPTA = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
$rsPTAZeilen = $awisRSZeilen;
//awis_Debug(1, $SQL, $rsPTA);
echo 'Preisliste: <b>' . $rsPTA['BEZEICHNUNG'][0] . '</b><br>';
echo 'Transaktions-ID: <b>' . $rsPTA['PPH_TRANSAKTIONSID'][0] . '</b><br>';
echo 'Beginn: <b>' . $rsPTA['PPH_BEGINN'][0] . '</b><br>';
echo 'Ende: <b>' . $rsPTA['PPH_ENDE'][0] . '</b><br>';
echo 'Ergebnis: <b>' . PPHStatusText($rsPTA['PPH_STATUS'][0]) . '</b><br>';

unset($rsPTA);

echo '<hr>';
echo "<input type=image title=Zur�ck alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=window.close();>";
echo '&nbsp;&nbsp;<img src=/bilder/drucker.png onclick=window.print(); style="@media:print,display:none">';
echo '<br>';

$SQL = 'SELECT PAU_SCHRITT, PAU_AUFFAELLIGKEIT ';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN PLP';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTEN PTA ON PLP_PTA_ID=PTA_ID  ';
$SQL .= ' INNER JOIN EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB PLI ON ID_FPLB=PLP_ID_FPLB';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST PPH ON PPH_PLP_KEY =  PLP_Key';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT PAU ON PAU_PPH_KEY = PPH_KEY';
$SQL .= ' WHERE PPH_Key= :var_N0_pph_key';
$SQL .= ' ORDER BY PAU_SCHRITT, PAU_AUFFAELLIGKEIT';

$rsPTA = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
$rsPTAZeilen = $awisRSZeilen;

echo '<table border=1 width=100%>';
echo '<tr><td class=FeldBez width=80>Schritt</td><td class=FeldBez>Auff�lligkeit</td></tr>';
for($PTAZeile=0;$PTAZeile<$rsPTAZeilen;$PTAZeile++)
{
	echo '<tr>';
	echo '<td>' . $rsPTA['PAU_SCHRITT'][$PTAZeile] . '</td>';

	echo '<td>' . $rsPTA['PAU_AUFFAELLIGKEIT'][$PTAZeile] . '</td>';

	echo '</tr>';
	
	if(($PTAZeile%1000)==0)
	{
		flush();
	}
}

echo '</table>';


awisLogoff($comcon);
awisLogoff($con);



function PPHStatusText($StatusID)
{
	switch ($StatusID) {
		case 0:
			return '<font color=red>Nicht gepr�ft.</font>';
			break;
		case 1:
			return '<font color=green>Erfolgreich.</font>';
			break;
		case 2:
			return '<font color=red>Fehler.</font>';
			break;
		default:
			return '::unbekannt::';
			break;
	}
}

?>
