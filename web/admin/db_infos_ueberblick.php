<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

global $con;
global $awisDBFehler;
global $awisRSZeilen;

$awis_link = '';
$db = awisDBServer();
/*
$awis_link='';
if (!empty($_GET["txtDB"]))
{
	$awis_link = "@" . strtolower($_GET["txtDB"]) . ".atu.de";
	$db = strtolower($_GET["txtDB"]);
}
else
{
	$db = strtolower(awisDBServer());
	$awis_link= "@" . $db . ".atu.de";
}
*/

echo '<form name=frmSuche method=get action=' . $_SERVER["PHP_SELF"] . '>';
echo '<input type="hidden" name="cmdAktion" value="Ueberblick">';

echo '<br>';
echo '<table border=1>';
echo '<tr><td id=FeldBez colspan=4>';
echo '<h3>Datenbanken</h3></td></tr>';
echo '<tr>';
echo '<td id=FeldBez><u>D</u>atenbank</td>';

echo '<td colspan=3><input name=txtDB readonly accesskey=d value="'.awisDBServer().'"></td>';
// Keine Auswahl mehr, SK 14.02.2008
/*

echo '<td colspan=3><select accesskey=d name="txtDB">';

$rsResult = awisOpenRecordset($con,'SELECT * from mcp.datenbanken');
$rsResultZeilen = $awisRSZeilen;

for($DBNr=0;$DBNr<$rsResultZeilen;$DBNr++)
{
	echo '<option value="' . $rsResult['DBK_NAME'][$DBNr] . '"';
	if ($db == strtolower($rsResult['DBK_NAME'][$DBNr]))
	{
		echo ' selected="selected"';
	}
	echo '>' . $rsResult['DBK_NAME'][$DBNr] . '</option>';
}

echo '</select></td>';
*/
echo '</tr>';
echo '</table>';
print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo '</form>';

if(isset($_REQUEST['cmdSuche_x'])||$awis_link!='')
{

$SQL = "Select * ";
$SQL .= "From gv\$instance".$awis_link;
awis_Debug(1,$SQL);
$rsResult = awisOpenRecordset($con, $SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_status_ueberblick.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;

print "<h1 id=SeitenTitel>Instance " . $rsResult['INSTANCE_NAME'][0] . "</h1>";

$tab_breite = 650;
$tab_spalte1 = 350;

print "<table id=DatenTabelle width=$tab_breite border=1>";
// print "<table id=DatenTabelle border=1>";
print "<tr>";
print "<td id=FeldBez>Parameter</td>";
print "<td id=FeldBez>Wert</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss width=$tab_spalte1>STATUS</td><td id=TabellenZeileWeiss>" . $rsResult['STATUS'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>DATABASE_STATUS</td><td id=TabellenZeileGrau>" . $rsResult['DATABASE_STATUS'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>ACTIVE_STATE</td><td id=TabellenZeileWeiss>" . $rsResult['ACTIVE_STATE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>STARTUP_TIME</td><td id=TabellenZeileGrau>" . $rsResult['STARTUP_TIME'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>SHUTDOWN_PENDING</td><td id=TabellenZeileWeiss>" . $rsResult['SHUTDOWN_PENDING'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>LOGINS</td><td id=TabellenZeileGrau>" . $rsResult['LOGINS'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>VERSION</td><td id=TabellenZeileWeiss>" . $rsResult['VERSION'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>INSTANCE_ROLE</td><td id=TabellenZeileGrau>" . $rsResult['INSTANCE_ROLE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>ARCHIVER</td><td id=TabellenZeileWeiss>" . $rsResult['ARCHIVER'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>LOG_SWITCH_WAIT</td><td id=TabellenZeileGrau>" . $rsResult['LOG_SWITCH_WAIT'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>PARALLEL</td><td id=TabellenZeileWeiss>" . $rsResult['PARALLEL'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>THREAD#</td><td id=TabellenZeileGrau>" . $rsResult['THREAD#'][0] . "</td>";
print "</tr>";	
print "</table>";
print "<hr>";

$SQL = "Select * ";
$SQL .= "From v\$database ".$awis_link;
$rsResult = awisOpenRecordset($con, $SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_status_ueberblick.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;
print "<h1 id=SeitenTitel>Database</h1>";

print "<table id=DatenTabelle width=$tab_breite border=1>";
print "<tr>";
print "<td id=FeldBez>Parameter</td>";
print "<td id=FeldBez>Wert</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss width=$tab_spalte1>DBID</td><td id=TabellenZeileWeiss>" . $rsResult['DBID'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>NAME</td><td id=TabellenZeileGrau>" . $rsResult['NAME'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>CREATED</td><td id=TabellenZeileWeiss>" . $rsResult['CREATED'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>RESETLOGS_CHANGE#</td><td id=TabellenZeileGrau>" . $rsResult['RESETLOGS_CHANGE#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>RESETLOGS_TIME</td><td id=TabellenZeileWeiss>" . $rsResult['RESETLOGS_TIME'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>PRIOR_RESETLOGS_CHANGE#</td><td id=TabellenZeileGrau>" . $rsResult['PRIOR_RESETLOGS_CHANGE#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>PRIOR_RESETLOGS_TIME</td><td id=TabellenZeileWeiss>" . $rsResult['PRIOR_RESETLOGS_TIME'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>LOG_MODE</td><td id=TabellenZeileGrau>" . $rsResult['LOG_MODE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>CHECKPOINT_CHANGE#</td><td id=TabellenZeileWeiss>" . $rsResult['CHECKPOINT_CHANGE#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>ARCHIVE_CHANGE#</td><td id=TabellenZeileGrau>" . $rsResult['ARCHIVE_CHANGE#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>CONTROLFILE_TYPE</td><td id=TabellenZeileWeiss>" . $rsResult['CONTROLFILE_TYPE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>CONTROLFILE_CREATED</td><td id=TabellenZeileGrau>" . $rsResult['CONTROLFILE_CREATED'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>CONTROLFILE_SEQUENCE#</td><td id=TabellenZeileWeiss>" . $rsResult['CONTROLFILE_SEQUENCE#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>CONTROLFILE_CHANGE#</td><td id=TabellenZeileGrau>" . $rsResult['CONTROLFILE_CHANGE#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>CONTROLFILE_TIME</td><td id=TabellenZeileWeiss>" . $rsResult['CONTROLFILE_TIME'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>OPEN_RESETLOGS</td><td id=TabellenZeileGrau>" . $rsResult['OPEN_RESETLOGS'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>VERSION_TIME</td><td id=TabellenZeileWeiss>" . $rsResult['VERSION_TIME'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>OPEN_MODE</td><td id=TabellenZeileGrau>" . $rsResult['OPEN_MODE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>PROTECTION_MODE</td><td id=TabellenZeileWeiss>" . $rsResult['PROTECTION_MODE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>PROTECTION_LEVEL</td><td id=TabellenZeileGrau>" . $rsResult['PROTECTION_LEVEL'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>REMOTE_ARCHIVE</td><td id=TabellenZeileWeiss>" . $rsResult['REMOTE_ARCHIVE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>ACTIVATION#</td><td id=TabellenZeileGrau>" . $rsResult['ACTIVATION#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>DATABASE_ROLE</td><td id=TabellenZeileWeiss>" . $rsResult['DATABASE_ROLE'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>ARCHIVELOG_CHANGE#</td><td id=TabellenZeileGrau>" . $rsResult['ARCHIVELOG_CHANGE#'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>SWITCHOVER_STATUS</td><td id=TabellenZeileWeiss>" . $rsResult['SWITCHOVER_STATUS'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>DATAGUARD_BROKER</td><td id=TabellenZeileGrau>" . $rsResult['DATAGUARD_BROKER'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>GUARD_STATUS</td><td id=TabellenZeileWeiss>" . $rsResult['GUARD_STATUS'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>SUPPLEMENTAL_LOG_DATA_MIN</td><td id=TabellenZeileGrau>" . $rsResult['SUPPLEMENTAL_LOG_DATA_MIN'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>SUPPLEMENTAL_LOG_DATA_PK</td><td id=TabellenZeileWeiss>" . $rsResult['SUPPLEMENTAL_LOG_DATA_PK'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileGrau>SUPPLEMENTAL_LOG_DATA_UI</td><td id=TabellenZeileGrau>" . $rsResult['SUPPLEMENTAL_LOG_DATA_UI'][0] . "</td>";
print "</tr>";
print "<tr>";
print "<td id=TabellenZeileWeiss>FORCE_LOGGING</td><td id=TabellenZeileWeiss>" . $rsResult['FORCE_LOGGING'][0] . "</td>";
print "</tr>";
print "</table>";
}
?>

