<?php

	global $FILID;
	global $cmdSuche;		
	global $txtFIL_PLZ;
	global $txtFIL_BEZ;
	global $txtFIL_ORT;
	global $txtFIL_ID;
					
	// Lokale Variablen
	$Bedingung = '';		// Bedingung f�r SQL
	$SQL = '';				// SQL Abfragestring

		// Aktualisieren wurde gew�hlt
	If(!isset($FILID) and isset($cmdSuche))
	{
		awis_BenutzerParameterSpeichern($con, "FilialSuche", $_SERVER["PHP_AUTH_USER"], implode(";",array($txtFIL_ID,$txtFIL_BEZ,$txtFIL_ORT,$txtFIL_PLZ)));
		// Bedingungen zusammenbauen
		if($txtFIL_ID!='')
		{
			$Bedingung .= "AND FIL_ID " . awisLIKEoderIST($txtFIL_ID, false);
		}
		if($txtFIL_BEZ!='')
		{	
			$Bedingung .= "AND UPPER(FIL_BEZ) " . awisLIKEoderIST("*" . $txtFIL_BEZ. "*", true);
		}
		if($txtFIL_ORT!='')
		{
			$Bedingung .= "AND UPPER(FIL_ORT) " . awisLIKEoderIST("*" . $txtFIL_ORT . "*", true);
		}
		if($txtFIL_PLZ!='')
		{
			$Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($txtFIL_PLZ, false);
		}
	}
	elseif(isset($FILID))		// Aufruf von dieser Seite -> eine Filiale zeigen
	{
		$Bedingung = "AND FIL_ID = " . $FILID;
	}
	else						// Direkter Einsprung in die Seite
	{
		$Params = explode(";",awis_BenutzerParameter($con, "FilialSuche", $_SERVER["PHP_AUTH_USER"]));
		if($Params[0]!='')
		{
			$Bedingung .= "AND FIL_ID " . awisLIKEoderIST($Params[0],0);
		}
		if($Params[1]!='')
		{	
			$Bedingung .= "AND UPPER(FIL_BEZ) " . awisLIKEoderIST("*" . $Params[1],1);
		}
		if($Params[2]!='')
		{
			$Bedingung .= "AND UPPER(FIL_ORT) " . awisLIKEoderIST($Params[2],1);
		}
		if($Params[3]!='')
		{
			$Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($Params[3],0);
		}
	}

	$SQL = "SELECT * FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ";	
	if($Bedingung!='')
	{
		$SQL .= " AND ( " . substr($Bedingung,4) . ")";
	}

	$rsFiliale = awisOpenRecordset($con, $SQL);
	$rsFilialeAnz = $awisRSZeilen;

	If($rsFilialeAnz>1)		// Liste gefunden
	{
		print "<table border=0 width=100%>";
		for($i=0;$i<$rsFilialeAnz;$i++)
		{
			print "<tr><td><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Filialinfos&FILID=" . $rsFiliale["FIL_ID"][$i] . ">";
			print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
			print "<td>" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td>". $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td><tr>";
		}
		print "</table>";
	}
	else					// Nur eine Filiale gefunden, Daten anzeigen
	{
		$SeitenZusatz = awis_BenutzerParameter($con, "FilialInfo_Version", $PHP_AUTH_USER);
		include("./filialinfo_Filialinfos_" .$SeitenZusatz . ".php");
		//rsFiliale wird als Datenquelle "�bergeben"
	}


?>