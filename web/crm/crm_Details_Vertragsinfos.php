<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CGK','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('CGK','CGK_%');
$TextKonserven[]=array('CAP','CAP_NACHNAME');
$TextKonserven[]=array('CAP','CAP_VORNAME');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3710 = awisBenutzerRecht($con,3710,$AWISBenutzer->BenutzerName());
if($Recht3710==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


awis_FORM_FormularStart();

$EditRecht=(($Recht3710&6)!=0);

//***********************************
//Kontakte anzeigen
//***********************************

/* 
	24.05.2013 OP :Es sollen nur gueltige Karten bzw. Karten die nur mit einer tempraeren Sperre versehen sind angezeigt
   	werden. Die Sperrgruende die temporaer sind wurden von Hr. Lehner Mario festgelegt.
*/
$SQL = 'SELECT CRMGewerbekunden.*';
$SQL .= ' FROM CRMGewerbekunden ';
$SQL .= ' INNER JOIN CRMAkquiseStand ON CAQ_VERTRAGSNUMMER = CGK_ACCOUNT_ID';
$SQL .= ' WHERE CAQ_CAD_KEY=0'.$AWIS_KEY1;
$SQL .= " AND CGK_OPAL_KARTENART IN ('HK','ZK')";
$SQL .= " AND (CGK_LOCK_REASON IN (9,14,17,29,30,75,76,77,78,80,87,90,96,98,99,100,102,111) OR CGK_LOCK_REASON IS NULL)";


if(isset($_GET['CGKKEY']))
{
	$SQL .= ' AND CGK_KEY = 0'.intval($_GET['CGKKEY']);
}

if(!isset($_GET['CGKSort']))
{
	$SQL .= ' ORDER BY CGK_CUST_ID DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CGKSort']);
}
$rsCGK = awisOpenRecordset($con,$SQL);
$rsCGKZeilen = $awisRSZeilen;

if(!isset($_GET['CGKKEY']) OR isset($_GET['CGKListe']))					// Liste anzeigen
{
	awis_FORM_ZeileStart();

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Vertragsinfos';
	$Link .= '&CGKSort=CGK_CARD_ID'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_CARD_ID'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_CARD_ID'],240,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Vertragsinfos';
	$Link .= '&CGKSort=CGK_DBM_KARTENART'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_DBM_KARTENART'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_DBM_KARTENART'],30,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Vertragsinfos';
	$Link .= '&CGKSort=CGK_NACHNAME'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_NACHNAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_NACHNAME'],220,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Vertragsinfos';
	$Link .= '&CGKSort=CGK_FIRMA1'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_FIRMA1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_FIRMA1'],300,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Vertragsinfos';
	$Link .= '&CGKSort=CGK_EMBOSSINGNAME'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_EMBOSSINGNAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_EMBOSSINGNAME'],300,'',$Link);

	awis_FORM_ZeileEnde();

	for($CGKZeile=0;$CGKZeile<$rsCGKZeilen;$CGKZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		$Icons[] = array('edit','./crm_Main.php?cmdAktion=Details&Seite=Vertragsinfos&CGKKEY='.$rsCGK['CGK_KEY'][$CGKZeile]);
		awis_FORM_Erstelle_ListeIcons($Icons,20,($CGKZeile%2));

		$ToolTipp='';
		$ToolTipp=(isset($rsCGK['CGK_LOCK_REASON'][0])?$AWISSprachKonserven['CGK']['CGK_CARDLOCKREASON_TEXT'].': '.$rsCGK['CGK_CARDLOCKREASON_TEXT'][0]:'');
		$ToolTipp.=($rsCGK['CGK_CARDBLOCKSTATUS'][$CGKZeile]>1?($ToolTipp==''?'':'/').$AWISSprachKonserven['CGK']['CGK_CARDBLOCKSTATUS_TEXT'].': '.$rsCGK['CGK_CARDBLOCKSTATUS_TEXT'][$CGKZeile]:'');
		awis_FORM_Erstelle_ListenFeld('#CGK_CARD_ID',$rsCGK['CGK_CARD_ID'][$CGKZeile],20,220,false,($CGKZeile%2),($ToolTipp==''?'':'color:red;'),'','T','',$ToolTipp);
		awis_FORM_Erstelle_ListenFeld('#CGK_OPAL_KARTENART',$rsCGK['CGK_OPAL_KARTENART'][$CGKZeile],20,30,false,($CGKZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CGK_NACHNAME',$rsCGK['CGK_NACHNAME'][$CGKZeile],20,220,false,($CGKZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CGK_FIRMA1',$rsCGK['CGK_FIRMA1'][$CGKZeile],20,300,false,($CGKZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CGK_EMBOSSINGNAME',$rsCGK['CGK_EMBOSSINGNAME'][$CGKZeile],20,300,false,($CGKZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}
	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtCGK_KEY type=hidden value=0'.(isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_KEY'][0]:'').'>';
	echo '<input name=txtCGK_CGK_KEY type=hidden value=0'.$AWIS_KEY1.'>';

	$AWIS_KEY2 = (isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_KEY'][0]:'0');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Details&Seite=Vertragsinfos&CGKListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_AC_DBM_UPDATE_DT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_BETREUER'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_BETREUER',(isset($rsCGK['CGK_BETREUER'][0])?$rsCGK['CGK_BETREUER'][0]:''),10,500,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CUST_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CUST_ID',(isset($rsCGK['CGK_CUST_ID'][0])?$rsCGK['CGK_CUST_ID'][0]:''),10,200,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARD_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CARD_ID',(isset($rsCGK['CGK_CARD_ID'][0])?$rsCGK['CGK_CARD_ID'][0]:''),10,200,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_DBM_KARTENART'].':',100);
	awis_FORM_Erstelle_TextFeld('*CGK_DBM_KARTENART',(isset($rsCGK['CGK_DBM_KARTENART'][0])?$rsCGK['CGK_DBM_KARTENART'][0]:''),10,200,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_NACHNAME'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_NACHNAME',(isset($rsCGK['CGK_NACHNAME'][0])?$rsCGK['CGK_NACHNAME'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PLATE_NUMBER'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PLATE_NUMBER',(isset($rsCGK['CGK_PLATE_NUMBER'][0])?$rsCGK['CGK_PLATE_NUMBER'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_EMBOSSINGNAME'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_EMBOSSINGNAME',(isset($rsCGK['CGK_EMBOSSINGNAME'][0])?$rsCGK['CGK_EMBOSSINGNAME'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CATEGORY'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CATEGORY',(isset($rsCGK['CGK_CATEGORY'][0])?$rsCGK['CGK_CATEGORY'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRSTTRANSACTIONDATE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_FIRSTTRANSACTIONDATE',(isset($rsCGK['CGK_FIRSTTRANSACTIONDATE'][0])?$rsCGK['CGK_FIRSTTRANSACTIONDATE'][0]:''),10,120,false,'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LASTTRANSACTIONDATE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_LASTTRANSACTIONDATE',(isset($rsCGK['CGK_LASTTRANSACTIONDATE'][0])?$rsCGK['CGK_LASTTRANSACTIONDATE'][0]:''),10,120,false,'','','','D');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARDLIMIT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CARDLIMIT',(isset($rsCGK['CGK_CARDLIMIT'][0])?$rsCGK['CGK_CARDLIMIT'][0]:''),10,120,false,'','','','N0');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_SALES_STORE_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_SALES_STORE_ID',(isset($rsCGK['CGK_SALES_STORE_ID'][0])?$rsCGK['CGK_SALES_STORE_ID'][0]:''),10,120,false,'','','','T');
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id']=intval('0'.(isset($rsCGK['CGK_SALES_STORE_ID'][0])?$rsCGK['CGK_SALES_STORE_ID'][0]:''));
	$SQL = 'SELECT FIL_BEZ FROM FILIALEN WHERE FIL_ID=:var_N0_fil_id';
	$rsFIL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
	awis_FORM_Erstelle_TextFeld('*FIL_BEZ',(isset($rsFIL['FIL_BEZ'][0])?$rsFIL['FIL_BEZ'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LOCK_DT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_LOCK_DT',(isset($rsCGK['CGK_LOCK_DT'][0])?$rsCGK['CGK_LOCK_DT'][0]:''),10,120,false,'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LOCK_REASON'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_LOCK_REASON',(isset($rsCGK['CGK_LOCK_REASON'][0])?$rsCGK['CGK_LOCK_REASON'][0]:''),10,120,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARDLOCKREASON_TEXT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CARDLOCKREASON_TEXT',(isset($rsCGK['CGK_CARDLOCKREASON_TEXT'][0])?$rsCGK['CGK_CARDLOCKREASON_TEXT'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
}
?>