<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');


try
{
	$script = "<script type='text/javascript'>
	function blendeEinBemerkung(obj) {
    	       var checkWert = obj.options[obj.selectedIndex].value;
			   var checkText = obj.options[obj.selectedIndex].text;
    	       if(checkText == '::bitte w�hlen::') // bitte w�hlen
    	       {
    	           document.getElementById('DB').style.display = 'none';
				   document.getElementsByName('cmdExportXLSB')[0].style.display = 'none';
	           }
    	       else
    	        {
					document.getElementById('DB').style.display = 'block';
					document.getElementsByName('cmdExportXLSB')[0].style.display = 'block';
				}
    	      
	
	}
	</script>";
	
	echo $script;
	
	
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	
	
	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Seite = 1;
	$Fehler = 0;
	$Neuanlage = 0;
	$Edit = 0;


	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	//var_dump ($_POST);
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	if($Recht34000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BES_Export'));
		//var_dump(date('d.m.Y G:i:s ',time()));

		//$Form->SchreibeHTMLCode('<form name=frmExport action=./qualitaetsmaengel_Main.php?cmdAktion=Auswertungen method=POST enctype="multipart/form-data">');
		$Form->SchreibeHTMLCode('<form name=frmExport action=./betrag_export.php method=POST enctype="multipart/form-data">');
		$Form->Formular_Start();
		
	
		$Daten = explode('|',$AWISSprachKonserven['BES']['lst_EXPORT']);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EXPORT'].':',200);
		$Form->Erstelle_SelectFeld('*Export',($Param['SPEICHERN']=='on'?$Param['BES_EXPORT']:''),400,true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'onchange="blendeEinBemerkung(this);"');
		$Form->ZeileEnde();
		
		if(($Param['SPEICHERN'] != '' and $Param['BES_EXPORT'] == 1))
		{
			$Form->ZeileStart('display:block','DB"ID="DB');
		}
		else
		{
			$Form->ZeileStart('display:none','DB"ID="DB');
		}
		
		$Form->ZeileStart();
		$Form->SchreibeHTMLCode('<hr noshade width="600" size="1" align="left" color="black">');
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARBEITUNG'].':',200);
		$Form->Erstelle_TextFeld('*BES_BEARBEITUNG',($Param['SPEICHERN']=='on'?$Param['BES_BEARBEITUNG']:''),10,180,true,'','','','T','L','','',10,'','','autocomplete=off');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KENNUNG'] . ':',200);
		$SQL6 ='select KENNUNG,KENNUNG,null,KENNUNG_BEZEICHNUNG from KENNUNGEN where VERALTET = 0 order by KENNUNG_BEZEICHNUNG ';
		$Form->Erstelle_SelectFeld('*BES_KENNUNG',($Param['SPEICHERN']=='on'?$Param['BES_KENNUNG']:''),348,true,$SQL6,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUATUNR'] . ':',200);
		$Form->Erstelle_TextFeld('*BES_ATUNR',($Param['SPEICHERN']=='on'?$Param['BES_ATUNR']:''), 10, 135,true, '','', '','', 'L','','',6,'','','autocomplete=off');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_LIEFERANT'] . ':',200);
		$Form->AuswahlBox('BES_LIEFERANT', 'box','', 'BES_LIEF_AUS', '',100,10,($Param['SPEICHERN']=='on'?$Param['BES_LIEFERANT']:''),'T',true,'','','',4,'','','',0,'','autocomplete=off');
		
		$Inhalt = '';
		$Fehler = '';
		$LIE_NR = '';
		
	
		ob_start();
		
		
		if((!isset($Param['BES_LIEFERANT']) or $Param['BES_LIEFERANT'] == ''))
		{
			$LIE_NR = '0';
			$Fehler = '';
		}
		else
		{
			if(isset($Param['BES_LIEFERANT']) and $Param['BES_LIEFERANT']!= '')
			{
				$LIE_NR= intval($Param['BES_LIEFERANT']);
				if($Param['BES_LIEFERANT'] == '')
				{
					$Fehler = '';
				}
			}
			else
			{
				if(isset($Param['BES_LIEFERANT']) and $Param['BES_LIEFERANT'] == '')
				{
					$LIE_NR = '0';
					$Fehler = '';
				}
				else
				{
					$LIE_NR = $Param['BES_LIEFERANT'];				}
			}
		
			if($LIE_NR == '0' and $Fehler == '')
			{
				$Fehler == '';
			}
		}
		
		$SQL  = ' Select * from LIEFERANTEN WHERE LIE_NR=\''.$Param['BES_LIEFERANT'].'\'';
		
		$rsLIE = $DB->RecordSetOeffnen($SQL);
		
		if($rsLIE->AnzahlDatensaetze() == 0 and $Param['BES_LIEFERANT'] != '')
		{
			$Fehler = 'kein g�ltiger Lieferant';
		}
		$Form->Erstelle_TextFeld('BES_LIENAME',$Fehler == '' ? $rsLIE->FeldInhalt('LIE_NAME1'):$Fehler, 200,423, false, '', '', '', 'T', 'L','','',50,'','','autocomplete=off');
		
		$Inhalt = ob_get_contents();
		ob_end_clean();
		

		$Form->AuswahlBoxHuelle('box','AuswahlListe','',$Inhalt);
		
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STAND'].':', 200, '');
		$SQL2 ='select STANDID,STANDBEM from STAND where ORT=\'ATU\' and veraltet = 0 order by standbem ';
		$Form->Erstelle_SelectFeld('*BES_STAND',($Param['SPEICHERN']=='on'?$Param['BES_STAND']:''),110,true,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();
		
		$Form->ZeileEnde();
		
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
	
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		
		
		if($Param['SPEICHERN']!='on' or $Param['BES_EXPORT']==0)
		{
			$Form->Schaltflaeche('image', 'cmdExportXLSB"style="display:none"', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		}
		else
		{
			$Form->Schaltflaeche('image', 'cmdExportXLSB', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		}
		$Form->SchaltflaechenEnde();

		$Form->SetzeCursor($AWISCursorPosition);
			
		$Form->SchreibeHTMLCode('</form>');
		
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>