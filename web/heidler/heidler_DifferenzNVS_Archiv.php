<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

$POSTbenutzen = true;

try {
    require_once 'heidler_funktionen.php';
    $Functions = new heidler_funktionen();
    $UserFilialen = $Functions->_AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
    $FilialNummer = is_numeric($UserFilialen) ? $UserFilialen : 0;
    $OptionBitteWaehlen = '~'.$Functions->AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
    $Recht45000 = $Functions->_AWISBenutzer->HatDasRecht(45000);

    //********************************************************
    // Recht pr�fen
    //********************************************************
    if ($Recht45000 == 0) {
        $Functions->_Form->Formular_Start();
        $Functions->_Form->Fehler_KeineRechte();
        $Functions->_Form->Formular_Ende();
        die();
    }

    //********************************************************
    // AWIS_KEY1 Param setzen
    //********************************************************
    $POSTbenutzen = false;
    $Functions->_Form->DebugAusgabe(1, $_POST);
    $Functions->_Form->DebugAusgabe(1, $_GET);
    $Param = array();
    $Param = unserialize($Functions->_AWISBenutzer->ParameterLesen('Formular_HeidlerDiffArchiv'));

    if (isset($_GET['ID'])){
        $AWIS_KEY1= $_GET['ID'];
    }
    if (isset($_POST['cmdNeu_x']))
    {
        $AWIS_KEY1 = -1;
    }
    
    if (isset($_POST['cmdSpeichern_x'])) { //Speichern?

        $AWIS_KEY1 = $_POST['txtNDK_KEY'];

        include('./heidler_speichern.php');

        echo '<form name=frmHeidlerMail action=./heidler_Main.php?cmdAktion=DifferenzNVS_Archiv method=POST>';

        // Abfragen ob Mail an Warenbestands-Abteilung gesendet werden soll
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Hinweistext('Datensatz gespeichert. Mail an Warenbest�nde senden?',awisFormular::HINWEISTEXT_OK);
        $Functions->_Form->ZeileEnde();
        $Functions->_Form->Erstelle_HiddenFeld('NDK_KEY', $AWIS_KEY1);

        $Functions->_Form->ZeileStart();
        $Functions->_Form->Schaltflaeche('submit','cmdMailSenden','','',$Functions->AWISSprachKonserven['Wort']['Ja'],'');
        $Functions->_Form->Schaltflaeche('submit','cmdKeineMailSenden','','',$Functions->AWISSprachKonserven['Wort']['Nein'],'');
        $Functions->_Form->ZeileEnde();

        $Functions->_Form->SchreibeHTMLCode('</form>');

        $Functions->_Form->Trennzeile('L');

    }

    if(isset($_POST['cmdMailSenden'])) { // Mail senden?
        $Functions->versendeMail($_POST['txtNDK_KEY']);
    }

    if (!isset($_POST['cmdNeu_x']))
    {
        //********************************************************
        // Suchfelder
        //********************************************************
        $Param['ID'] = (isset($_GET['ID']) ? $Functions->_Form->Format('N0', $_GET['ID'], true) : '');
        $Param['FIL_ID'] = (isset($_POST['sucNDA_FIL_ID']) ? $Functions->_Form->Format('N0', $_POST['sucNDA_FIL_ID'], true) : '');
        $Param['NDK_DIENSTLEISTER'] = (isset($_POST['txtNDK_DIENSTLEISTER']) ? $Functions->_Form->Format('T', $_POST['txtNDK_DIENSTLEISTER'], true) : '');
        $Param['LIEFERSCHEINDATUM'] = (isset($_POST['txtLIEFERSCHEINDATUM']) ? $Functions->_Form->Format('D', $_POST['txtLIEFERSCHEINDATUM'], true) : '');
        $Param['ORDER'] = ' ORDER BY NDK_LIEFERSCHEINDATUM DESC';
    }

    $Param['ID']=$AWIS_KEY1;

    //********************************************************
    // Daten ziehen
    //********************************************************
    $SQL = 'SELECT *';
    $SQL.= ' FROM';
    $SQL.= ' (';
    $SQL.= '  SELECT NDK_KEY,';
    $SQL.= '  NDK_FIL_ID,';
    $SQL.= '  NDK_DIENSTLEISTER,';
    $SQL.= '  NDK_GRUND,';
    $SQL.= '  NDK_ZUGESTELLT,';
    $SQL.= '  NDK_ANSPRECHPARTNER,';
    $SQL.= '  func_textkonserve(\'lst_NDK_DIENSTLEISTER\',\'NDK\',\'DE\',NDK_DIENSTLEISTER) as NDK_DIENSTLEISTER_WORT,';
    $SQL.= '  NDK_LIEFERSCHEINDATUM';
    $SQL.= ' FROM nvsdifferenzkopf';
    $SQL.= ' WHERE NDK_ANZMAILS > 0';
    // Zentrale duerfen alles sehen, die Filiale nur ihre eigenen
    if($FilialNummer != 0) {
        $SQL .= ' AND NDK_FIL_ID = ' . $FilialNummer;
    }
    $Bedingung = '';
    $Bedingung .= _BedingungErstellen($Param);
    if ($Bedingung != '') {
        $SQL .= ' AND ' . substr($Bedingung, 4);
    }
    $SQL .= $Param['ORDER'];
    $SQL.= ' )';
    $SQL.= ' WHERE rownum <= 30';

    $Functions->_Form->DebugAusgabe(1, $SQL);
    $rsNDA = $Functions->_DB->RecordSetOeffnen($SQL, $Functions->_DB->Bindevariablen('NDK', true));

    if($rsNDA->AnzahlDatensaetze() > 0) {
        $AWIS_KEY1 = $rsNDA->FeldInhalt('NDK_KEY');
    }
    $Functions->_Form->DebugAusgabe(1, $AWIS_KEY1);

    if (isset($_GET['NDP_KEY'])) {
        $Functions->loeschePosition($_GET['NDP_KEY']);
    }

    if(isset($_POST['ico_add__x'])) {
        $POSTbenutzen = true;
        $anzahlPos = $Functions->getMaximalePosNummer() + 1;
        $anzahlPos++;
    } else {
        $anzahlPos = $Functions->getMaximalePosNummer();
        $anzahlPos = $anzahlPos<1?1:$anzahlPos;
    }

    //********************************************************
    // Suchfelder
    //********************************************************
    $Functions->_Form->Formular_Start();
    $Functions->_Form->SchreibeHTMLCode('<form name="frmHeidlerDiffArchiv" action="./heidler_Main.php?cmdAktion=DifferenzNVS_Archiv" method="POST"  >');

    $Functions->_Form->FormularBereichStart('Suche');
    $Functions->_Form->FormularBereichInhaltStart('Suche',true);
    //********************************************************
    // Filiale Suche
    //********************************************************
    if ($FilialNummer != 0) {

        $Functions->_Form->ZeileStart();
        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILNR'] . ':', 200);
        $Functions->_Form->Erstelle_TextFeld('NDA_FIL_ID', $FilialNummer, 10, 120, false);

        $FilBez = '';
        $SQL = 'SELECT * FROM FILIALEN WHERE FIL_ID = ' . $Functions->_DB->WertSetzen('FIL', 'N0', $FilialNummer);
        $rsFil = $Functions->_DB->RecordSetOeffnen($SQL, $Functions->_DB->Bindevariablen('FIL', true));
        $Functions->_Form->DebugAusgabe(1, $Functions->_DB->LetzterSQL());
        if ($rsFil->AnzahlDatensaetze() > 0) {
            $FilBez = $rsFil->FeldInhalt('FIL_BEZ');
        }

        $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILBEZ'] . ':', 120);
        $Functions->_Form->Erstelle_TextFeld('FILBEZ', $FilBez, 80, 170, false);
        $Functions->_Form->ZeileEnde();
    }
    else
        {
            //********************************************************
            // Zentrale Suche
            //********************************************************
            $Block2 = 'NDA_FIL_ID';
            $Functions->_Form->ZeileStart('', '', 'Block' . $Block2 . '');
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILNR'] . ':', 200);
            $Functions->_Form->AuswahlBox('' . $Block2, 'box2', '', 'NDA_Daten', '', 65, 170, '', 'T', true, '', '', '', 0, '', '', '', 0, '', 'autocomplete=off');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart('visibility:hidden;', '', 'Block' . $Block2 . '');
            $Functions->_Form->AuswahlBoxHuelle('box2');
            $Functions->_Form->ZeileEnde();

            echo '<script type="text/javascript">' . PHP_EOL;

            echo '$( document ).ready(function() {' . PHP_EOL;
            echo '  blenden();' . PHP_EOL;
            echo ' toggleRequire(); ' . PHP_EOL;
            echo '});' . PHP_EOL;

            echo '$("#txtFIL_ID").change(function(){' . PHP_EOL;
            echo '  blenden();' . PHP_EOL;
            echo ' toggleRequire(); ' . PHP_EOL;
            echo '})' . PHP_EOL;

            echo 'function blenden() {' . PHP_EOL;
            echo '  $(".BlockNDK_FIL_ID").css({"visibility": "visible"});' . PHP_EOL;
            echo '  $(".BlockNDK_FIL_ID").show();' . PHP_EOL;
            echo '};' . PHP_EOL;

            echo 'function toggleRequire(){ ' . PHP_EOL;
            echo '      $("#txtBMV_ZENTRALBEREICH").prop("required",false);' . PHP_EOL;
            echo '      $("#sucBMV_FIL_ID").prop("required",true);' . PHP_EOL;
            echo ' } ';
            echo '</script>' . PHP_EOL;

    }

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['Dienstleister'],200);
    $Daten = explode('|',$Functions->AWISSprachKonserven['NDK']['lst_NDK_DIENSTLEISTER']);
    $Functions->_Form->Erstelle_SelectFeld('NDK_DIENSTLEISTER','', 220, true, '',$OptionBitteWaehlen,'','','',$Daten);
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['LIEFERSCHEINDATUM'] . ':', 200);
    $Functions->_Form->Erstelle_TextFeld('LIEFERSCHEINDATUM', '', 15, 200, true, '', '', '', 'D');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->ZeileStart();
    $Functions->_Form->Trennzeile('O');
    $Functions->_Form->ZeileEnde();

    $Functions->_Form->FormularBereichEnde();
    $Functions->_Form->FormularBereichInhaltEnde();

    //********************************************************
    // Listenansicht Standard oder nach Suche
    //********************************************************
        if ($rsNDA->AnzahlDatensaetze() > 1) {

            $Functions->_Form->FormularBereichStart('Liste');
            $Functions->_Form->FormularBereichInhaltStart('Liste',true);
            $Functions->_Form->ZeileStart('display: inline-block');

            $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&Sort=FIL_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FIL_ID')) ? '~' : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['FILNR'], 100, '', $Link);

            $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&Sort=NDK_DIENSTLEISTER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'NDK_DIENSTLEISTER')) ? '~' : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['Wort']['Dienstleister'], 110, '', $Link,'','');

            $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&Sort=GRUND' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'GRUND')) ? '~' : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['Wort']['Meldegrund'], 180, '', $Link);

            $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&Sort=ZUGESTELLT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ZUGESTELLT')) ? '~' : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['Wort']['Zugestellt'], 110, '', $Link);

            $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&Sort=ANSPRECHPARTNER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'ANSPRECHPARTNER')) ? '~' : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['Wort']['Ansprechpartner'], 200, '', $Link);

            $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&Sort=LIEFERSCHEINDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'LIEFERSCHEINDATUM')) ? '~' : '');
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['HEIDLER']['LIEFERSCHEINDATUM'], 160, '', $Link);

            $Functions->_Form->ZeileEnde();
            $DS = 0;    // f�r Hintergrundfarbumschaltung

            while (!$rsNDA->EOF()) {
                $Functions->_Form->ZeileStart('display: inline-block', 'Zeile"ID="Zeile_' . $rsNDA->FeldInhalt('NDK_KEY'));

                $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&ID=' . $rsNDA->FeldInhalt('NDK_KEY');
                $TTT = $rsNDA->FeldInhalt('NDK_FIL_ID');
                $Functions->_Form->Erstelle_ListenFeld('FIL_ID', $rsNDA->FeldInhalt('NDK_FIL_ID'), 0, 100, false, ($DS % 2), '', $Link, 'N0', 'L', $TTT);

                $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&ID=' . $rsNDA->FeldInhalt('NDK_KEY');
                $TTT = $rsNDA->FeldInhalt('NDK_DIENSTLEISTER');
                $Functions->_Form->Erstelle_ListenFeld('NDK_DIENSTLEISTER', $rsNDA->FeldInhalt('NDK_DIENSTLEISTER_WORT'), 0, 110, false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

                $TTT = $rsNDA->FeldInhalt('NDK_GRUND');

                $Daten = explode('|', $Functions->AWISSprachKonserven['NDK']['lst_NDK_GRUND']);
                foreach($Daten as $key => $value) {
                    $dummy = explode('~', $value);
                    $DatenGrund[$dummy[0]] = $dummy[1];
                }
                $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&ID=' . $rsNDA->FeldInhalt('NDK_KEY');
                $Functions->_Form->Erstelle_ListenFeld('GRUND', $DatenGrund[$rsNDA->FeldInhalt('NDK_GRUND')], 0, 180, false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

                $TTT = $rsNDA->FeldInhalt('NDK_ZUGESTELLT');
                $DatenZugestellt = array(0 => 'nein', 1 => 'ja');
                $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&ID=' . $rsNDA->FeldInhalt('NDK_KEY');
                $Functions->_Form->Erstelle_ListenFeld('ZUGESTELLT', $DatenZugestellt[$rsNDA->FeldInhalt('NDK_ZUGESTELLT')], 0, 110, false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

                $TTT = $rsNDA->FeldInhalt('NDK_ANSPRECHPARTNER');
                $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&ID=' . $rsNDA->FeldInhalt('NDK_KEY');
                $Functions->_Form->Erstelle_ListenFeld('ANSPRECHPARTNER', $rsNDA->FeldInhalt('NDK_ANSPRECHPARTNER'), 0, 200, false, ($DS % 2), '', $Link, 'T', 'L', $TTT);

                $TTT = $rsNDA->FeldInhalt('NDK_LIEFERSCHEINDATUM');
                $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&ID=' . $rsNDA->FeldInhalt('NDK_KEY');
                $Functions->_Form->Erstelle_ListenFeld('LIEFERSCHEINDATUM', $rsNDA->FeldInhalt('NDK_LIEFERSCHEINDATUM'), 0, 160, false, ($DS % 2), '', $Link, 'D', 'L', $TTT);

                $Functions->_Form->ZeileEnde();
                $DS++;
                $rsNDA->DSWeiter();
            }
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Link = './heidler_Main.php?cmdAktion=DifferenzNVS_Archiv';
            $Functions->_Form->ZeileStart();
            $Functions->_Form->SchreibeHTMLCode('<form name="frmHeidlerDiffArchiv" action="' . $Link . '" method=POST  enctype="multipart/form-data">');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->FormularBereichEnde();
            $Functions->_Form->FormularBereichInhaltEnde();
        }
        //********************************************************
        // Detail Ansicht oder Neuanlage
        //********************************************************
        elseif(($AWIS_KEY1==-1) or ($AWIS_KEY1!=''))
        {
            $Functions->_Form->FormularBereichStart('Detail');
            $Functions->_Form->FormularBereichInhaltStart('Detail',true);
            $Functions->_Form->Erstelle_HiddenFeld('NDK_KEY', $AWIS_KEY1);
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->Formular_Start();
            echo '<form name=frmHeidlerDiffArchiv action=./heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&ID=' . $rsNDA->FeldInhalt('NDK_KEY') . ' method=POST>';

            if($FilialNummer != 0) {
                // Filiale
                $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILNR'] . ':',100);
                $Functions->_Form->Erstelle_TextFeld('FIL_ID',$FilialNummer,10,120,false);
                $Functions->_Form->Erstelle_HiddenFeld('NDK_FIL_ID', $FilialNummer);

                $FilBez = '';
                $SQL = 'SELECT * FROM FILIALEN WHERE FIL_ID = ' . $Functions->_DB->WertSetzen('FIL', 'N0', $FilialNummer);
                $rsFil = $Functions->_DB->RecordSetOeffnen($SQL,$Functions->_DB->Bindevariablen('FIL',true));
                $Functions->_Form->DebugAusgabe(1, $Functions->_DB->LetzterSQL());
                if($rsFil->AnzahlDatensaetze() > 0) {
                    $FilBez = $rsFil->FeldInhalt('FIL_BEZ');
                }

                $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILBEZ'] . ':',120);
                $Functions->_Form->Erstelle_TextFeld('FILIALE',$FilBez,80,170,false);
                $Functions->_Form->ZeileEnde();
            } else
                {
                // Zentrale
                    $Block = 'NDK_FIL_ID';
                    $Functions->_Form->ZeileStart('','','Block'.$Block.'');
                    $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['FILNR'] . ':',100);
                    $Functions->_Form->AuswahlBox(''.$Block,'box1','','NDK_Daten','', 65, 170, $rsNDA->FeldInhalt('NDK_FIL_ID'),'T',true,'','','',0,'','','',0,'','autocomplete=off');
                    $Functions->_Form->ZeileEnde();


                    ob_start();
                    $Functions->erstelleFilBezAjax($rsNDA->FeldInhalt('NDK_FIL_ID'));
                    $Inhalt=ob_get_clean();

                    $Functions->_Form->ZeileStart('','','Block'.$Block.'');
                    $Functions->_Form->AuswahlBoxHuelle('box1','','',$Inhalt);
                    $Functions->_Form->ZeileEnde();

                    echo '<script type="text/javascript">'.PHP_EOL;

                    echo '$( document ).ready(function() {'.PHP_EOL;
                    echo '  blenden();'.PHP_EOL;
                    echo ' toggleRequire(); '.PHP_EOL;
                    echo '});'.PHP_EOL;

                    echo '$("#txtFIL_ID").change(function(){'.PHP_EOL;
                    echo '  blenden();'.PHP_EOL;
                    echo ' toggleRequire(); '.PHP_EOL;
                    echo '})'.PHP_EOL;

                    echo 'function blenden() {'.PHP_EOL;
                    echo '  $(".BlockNDK_FIL_ID").css({"visibility": "visible"});'.PHP_EOL;
                    echo '  $(".BlockNDK_FIL_ID").show();'.PHP_EOL;
                    echo '};'.PHP_EOL;

                    echo 'function toggleRequire(){ ' . PHP_EOL;
                    echo '      $("#txtBMV_ZENTRALBEREICH").prop("required",false);' . PHP_EOL;
                    echo '      $("#sucBMV_FIL_ID").prop("required",true);' . PHP_EOL;
                    echo ' } ';
                    echo '</script>'.PHP_EOL;



                }

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['Dienstleister'],120);
            $Daten = explode('|',$Functions->AWISSprachKonserven['NDK']['lst_NDK_DIENSTLEISTER']);
            $Functions->_Form->Erstelle_SelectFeld('NDK_DIENSTLEISTER',$rsNDA->FeldInhalt('NDK_DIENSTLEISTER'), 120, true, '','','','','',$Daten);
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            if($rsNDA->FeldInhalt('NDK_ANSPRECHPARTNER') != '') {
                $Functions->_Form->ZeileStart();
                $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['Name'] . ':', 180);
                $Functions->_Form->Erstelle_TextFeld('NDK_ANSPRECHPARTNER', $rsNDA->FeldOderPOST('NDK_ANSPRECHPARTNER', 'T', $POSTbenutzen),180, 200, true);
                $Functions->_Form->ZeileEnde();
            } else {
                $Functions->_Form->ZeileStart('width:1024px;');
                $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['PER']['PER_NR'].':',180);
                $Block = 'NDK_ANSPRECHPARTNER';
                $Functions->_Form->AuswahlBox(''.$Block,'box_ansprechpartner','','NDK_Ansprechpartner','', 80, 8, '','T',true,'','','',0,'','','',0,'','autocomplete=off');

                $Functions->_Form->AuswahlBoxHuelle('box_ansprechpartner','','');
                $Functions->_Form->ZeileEnde();
            }

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['Meldegrund'],220);
            $Daten = explode('|',$Functions->AWISSprachKonserven['NDK']['lst_NDK_GRUND']);
            $Functions->_Form->Erstelle_SelectFeld('NDK_MELDEGRUND',$rsNDA->FeldInhalt('NDK_GRUND'), 180, true, '','','','','',$Daten);
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['Wort']['NachtraeglichZugestellt'] . ':',220);
            $Functions->_Form->Erstelle_Checkbox('NDK_ZUGESTELLT', $rsNDA->FeldInhalt('NDK_ZUGESTELLT')==1?'on':'', 80, true);
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Erstelle_TextLabel($Functions->AWISSprachKonserven['HEIDLER']['LIEFERSCHEINDATUM'] . ':',180);
            $Functions->_Form->Erstelle_TextFeld('NDK_LIEFERSCHEINDATUM',$rsNDA->FeldInhalt('NDK_LIEFERSCHEINDATUM'),15,200,true,'','','','D');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->Erstelle_HiddenFeld('ARCHIV', 'ARCHIV');

            $FeldBreiten['NDP_AST_ATUNR'] = 80;
            $FeldBreiten['NDP_AST_BEZEICHNUNGWW'] = 320;
            $FeldBreiten['NDP_MENGE'] = 100;
            $FeldBreiten['NDP_BEMERKUNG'] = 300;

            //$Functions->_Form->Erstelle_Liste_Ueberschrift('', 25);
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_AST_ATUNR'], $FeldBreiten['NDP_AST_ATUNR']);
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_AST_BEZEICHNUNGWW'], $FeldBreiten['NDP_AST_BEZEICHNUNGWW']);
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_MENGE'], $FeldBreiten['NDP_MENGE']);
            $Functions->_Form->Erstelle_Liste_Ueberschrift($Functions->AWISSprachKonserven['NDP']['NDP_BEMERKUNG'], $FeldBreiten['NDP_BEMERKUNG']);

            $SQL = '  SELECT *';
            $SQL .= ' FROM nvsdifferenzpos LEFT JOIN ';
            $SQL .= ' (';
            $SQL .= '   SELECT ast_atunr, ast_bezeichnungww FROM artikelstamm ';
            $SQL .= '   UNION ALL ';
            $SQL .= '   SELECT hba_artnr as ast_atunr, hba_artbezl as ast_bezeichnungww FROM hubstamm ';
            $SQL .= ' )';
            $SQL .= ' ON ndp_ast_atunr = ast_atunr';
            $SQL .= ' WHERE ndp_ndk_key = ' . $AWIS_KEY1;
            $SQL .= ' ORDER BY ndp_key';

            $rsNDP = $Functions->_DB->RecordSetOeffnen($SQL, $Functions->_DB->Bindevariablen('NDP', true));
            $Functions->_Form->DebugAusgabe($Functions->_DB->LetzterSQL());

            //********************************************************
            // Positonsdaten
            //********************************************************
            $i = 0;
            //$IconsAdd[] = array('add','POST~');

            while(!$rsNDP->EOF())
            {
                $Functions->_Form->ZeileStart();
                $Functions->_Form->Erstelle_HiddenFeld('NDP_KEY_'.$i, $rsNDP->FeldInhalt('NDP_KEY'));
                $IconsDel[] = array('delete','/heidler/heidler_Main.php?cmdAktion=DifferenzNVS_Archiv&NDP_KEY='.$rsNDP->FeldInhalt('NDP_KEY').'&ID='.$AWIS_KEY1);
                //$Functions->_Form->Erstelle_ListeIcons($IconsDel, 25);
                //unset($IconsDel);
                $Functions->_Form->Erstelle_ListenFeld('NDP_AST_ATUNR_'.$i,$rsNDP->FeldInhalt('NDP_AST_ATUNR'),8,80,false);
                $Functions->_Form->Erstelle_ListenFeld('NDP_AST_BEZEICHNUNGWW_'.$i,$rsNDP->FeldInhalt('AST_BEZEICHNUNGWW'),50,320,false);
                $Functions->_Form->Erstelle_ListenFeld('NDP_MENGE_'.$i,$rsNDP->FeldInhalt('NDP_MENGE'),10,100,false);
                $Functions->_Form->Erstelle_ListenFeld('NDP_BEMERKUNG_'.$i,$rsNDP->FeldInhalt('NDP_BEMERKUNG'),80,300,true);
                $Functions->_Form->ZeileEnde();

                $i++;
                $rsNDP->DSWeiter();
            }

            while($i < $anzahlPos)
            {
                $Functions->_Form->ZeileStart('width:1024px;');
                $Functions->_Form->Erstelle_ListeIcons('', 25);
                $Block = 'NDP_AST_ATUNR';
                $Functions->_Form->AuswahlBox($Block.'_'.$i,'box_'.$i,'','NDP_Artbez','', 80, 8, $rsNDP->FeldInhalt('NDP_AST_ATUNR_'.$i,'T'),'T',true,'','','height:19px;font-size:12px;',0,'','','',0,'','autocomplete=off');

                ob_start();
                $rsArtbez = $Functions->getArtikelbezeichnung($rsNDP->FeldInhalt('NDP_AST_ATUNR_'.$i));
                $Functions->_Form->Erstelle_ListenFeld('NDP_AST_BEZEICHNUNGWW_'.$i,$rsArtbez->FeldInhalt('AST_BEZEICHNUNGWW'),50,320, true);
                $Listenfeld = ob_get_clean();

                $Functions->_Form->AuswahlBoxHuelle('box_'.$i,'','',$Listenfeld);
                $Functions->_Form->Erstelle_ListenFeld('NDP_MENGE_'.$i,$rsNDP->FeldInhalt('NDP_MENGE'.$i, 'N0'),10,100,true);
                $Functions->_Form->Erstelle_ListenFeld('NDP_BEMERKUNG_'.$i,$rsNDP->FeldInhalt('NDP_BEMERKUNG_'.$i, 'T'),80,300,true);
                $Functions->_Form->ZeileEnde();

                $i++;
            }

            //$Functions->_Form->ZeileStart();
            //$Functions->_Form->Erstelle_ListeIcons($IconsAdd, 25);
            //$Functions->_Form->ZeileEnde();

            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();

            $Functions->_Form->FormularBereichEnde();
            $Functions->_Form->FormularBereichInhaltEnde();
        }
        //********************************************************
        // Keine Datens�tze
        //********************************************************
        else {

            echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
            $Functions->_Form->ZeileStart();
            $Functions->_Form->Trennzeile('O');
            $Functions->_Form->ZeileEnde();
        }

    //********************************************************
    // Schaltfl�chen
    //********************************************************
        $Functions->_Form->ZeileStart();
        $Functions->_Form->Trennzeile('O');
        $Functions->_Form->ZeileEnde();

        $Functions->_Form->SchaltflaechenStart();
        $Functions->_Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Functions->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        $Functions->_Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_lupe.png', $Functions->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    /*
    if (!isset($_POST['cmdNeu_x']))
    {
        $Functions->_Form->Schaltflaeche('image', 'cmdNeu', '', '/bilder/cmd_neu.png', $Functions->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    */

    if (isset($_POST['cmdNeu_x']) OR isset($_GET['ID']) OR $rsNDA->AnzahlDatensaetze() == 1)
    {
        $Functions->_Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Functions->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }
        $Functions->_Form->SchaltflaechenEnde();
        $Functions->_Form->SchreibeHTMLCode('</form>');
        $Functions->_AWISBenutzer->ParameterSchreiben('Formular_HeidlerDiffArchiv',serialize($Param));

}
catch (awisException $ex)
{
    if($Functions->_Form instanceof awisFormular)
    {
        $Functions->_Form->DebugAusgabe(1, $ex->getSQL());
        $Functions->_Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else
    {
        echo 'AWIS-Fehler:'.$ex->getMessage();
    }
}
catch (Exception $ex)
{
    if($Functions->_Form instanceof awisFormular)
    {

        $Functions->_Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}


/**
 * Bedingung zusammenbauen
 *
 * @param array $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
    $Functions = new heidler_funktionen();
    $Bedingung='';

    if (isset($Param['ID']) and $Param['ID']!=0)
    {
        $Bedingung .= 'AND NDK_KEY = ' . $Param['ID'] . ' ';
    }
    else
    {
        if (isset($Param['FIL_ID']) AND $Param['FIL_ID'] != 0) {
            $Bedingung .= 'AND LTRIM(NDK_FIL_ID, \'0\') = LTRIM(' . $Functions->_DB->FeldInhaltFormat('T',$Param['FIL_ID']) . ', \'0\') ';
        }
        if(isset($Param['NDK_DIENSTLEISTER']) AND $Param['NDK_DIENSTLEISTER']!='' AND $Param['NDK_DIENSTLEISTER']!='%')
        {
            $Bedingung .= 'AND NDK_DIENSTLEISTER = ' . $Functions->_DB->FeldInhaltFormat('Z',$Param['NDK_DIENSTLEISTER']);
        }
        if(isset($Param['LIEFERSCHEINDATUM']) AND $Param['LIEFERSCHEINDATUM']!='')
        {
            $Bedingung .= 'AND TRUNC(NDK_LIEFERSCHEINDATUM) = ' . $Functions->_DB->FeldInhaltFormat('D', $Param['LIEFERSCHEINDATUM']);
        }
    }
    return $Bedingung;
}