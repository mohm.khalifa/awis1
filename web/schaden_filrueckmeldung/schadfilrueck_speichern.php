<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $meldung;

$TextKonserven=array();
$TextKonserven[]=array('HRK','%');
$TextKonserven[]=array('BEW','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_DatumEndeVorAnfang');
$TextKonserven[]=array('Fehler','err_DatumAbstandTage');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');


$TXT_Speichern = $Form->LadeTexte($TextKonserven);
$Werkzeuge = new awisWerkzeuge();

$Recht40004 = $AWISBenutzer->HatDasRecht(40004);


try{
	if(isset($_POST['cmdHinzufuegen_x'])) {
		$SQL = 'insert into BEARBEITUNGSSTAND_NEW ';
		$SQL .= ' (BEARBNRNEU,DATUM,BEMERKUNGEN,ZAHLUNGSART,BNUSER,EINTRAG,EINGABEDURCH_FILPERSNR,BETRAG,BNUSERDAT,ID)';
		$SQL .= ' values';
		$SQL .= '(' . $DB->FeldInhaltFormat('T', $_POST['txtBEARBNRNEU']);
		$SQL .= ',sysdate' ;
		$SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtFESTSTELLUNG']);
		$SQL .= ',' . $DB->FeldInhaltFormat('T', $_POST['txtBES_ZAHLUNGSART']);
		$SQL .= ',\'\'';
		$SQL .= ',\'' . $rsPep->FeldInhalt('NAME').','.$rsPep->FeldInhalt('VORNAME') . '\'';
		$SQL .= ',' . $DB->FeldInhaltFormat('N0', $rsPep->FeldInhalt('PERSNR'));
		$SQL .= ',' . $DB->FeldInhaltFormat('N2', $_POST['txtBES_BETRAG']);
		$SQL .= ',sysdate,SEQ_BEARBST_NEW_ID.nextval)';
		$DB->Ausfuehren($SQL, '', true);

		$meldung = "Rückmeldung hinterlegt";

		$SQL3='INSERT INTO WIEDERVORLAGEN_NEW (WV_DATUM, BEARBEITUNGSNR, WV_AN,WV_GRUND,WV_USER,WV_USERDAT)';
		$SQL3.=' VALUES(SYSDATE,\''.$DB->FeldInhaltFormat('N0', $_POST['txtBEARBEITUNGSNR']).'\',\''.$_POST['txtSBNAME'].'\',\''."Filialrückmeldung".'\',\''.$AWISBenutzer->BenutzerName().'\',sysdate)';

		$DB->Ausfuehren($SQL3, '', true);

	}
	elseif(isset($_POST['cmdSpeichern_x'])){
		$SQL3 = 'UPDATE SCHAEDEN_NEW SET ';
		$SQL3.= ' AUFTRAGSART_ATU_NEU='.$DB->FeldInhaltFormat('N0',$_POST['txtBES_AUFTRAGSART_NEU']);
		$SQL3.= ',ATUNR='.$DB->FeldInhaltFormat('T',$_POST['txtBES_ATUNR']);
		$SQL3.= ',SCHADENSGRUND='.$DB->FeldInhaltFormat('N0',$_POST['txtBES_WORANARB']);
		$SQL3.= ',BID='.$DB->FeldInhaltFormat('N0',$_POST['txtBES_WASBESCH']);
		$SQL3.= ',AUSFALLURSACHE='.$DB->FeldInhaltFormat('T',$_POST['txtBES_AUSFALLURSACHE']);
		$SQL3.= ',STAND='.$DB->FeldInhaltFormat('N0',$_POST['txtBES_STAND']);
		$SQL3.= ' where bearbeitungsnr =\''. $DB->FeldInhaltFormat('N0', $_POST['txtBEARBEITUNGSNR']).'\'';

		$meldung = "Änderungen übernommen";

		$DB->Ausfuehren($SQL3, '', true);

		$SQL5 = 'insert into HISTORIE (HISTORIE_BEARBNR,HISTORIE_KENNUNG,HISTORIE_DATUM,HISTORIE_AART)';
		$SQL5 .= 'values(';
		$SQL5 .= $DB->FeldInhaltFormat('N0', $_POST['txtBEARBEITUNGSNR']).',';
		$SQL5 .='\''.$AWISBenutzer->BenutzerName().'\',';
		$SQL5 .='sysdate,';
		$SQL5 .='0)';

		$DB->Ausfuehren($SQL5,'',true);

	}
}
catch (awisException $ex)
{
	
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_QMZ_ZUORD) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Form->Hinweistext('Der einzufügende Datensatz befindet sich schon in der Datenbank');
	}
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.PK_QMK_KEY) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 4;
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
