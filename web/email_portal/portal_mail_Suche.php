<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $Recht26000;

require_once('awisFilialen.inc');
require_once('register.inc.php');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$Email = false;
	$TextKonserven = array();
	$TextKonserven[]=array('EPO','*');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','Personalnummer');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','FalschePerNr');
	$TextKonserven[]=array('Wort','KeineMail');
	$TextKonserven[]=array('Wort','IdentPruefung');
	

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht29000 = $AWISBenutzer->HatDasRecht(26000);

	if($Recht29000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$Form->DebugAusgabe(1,$_POST);

		$Form->SchreibeHTMLCode('<form name="frmEmailPortalSuche" action="./portal_mail_Main.php?cmdAktion=Suche" method="POST"  enctype="multipart/form-data">');

		if(isset($_POST['cmdSuche_x']))
		{
			if(isset($_POST['sucAuswahlSpeichern']))
			{
				$Param['PERSNR'] = $_POST['txtEPO_PERSNR'];
				$Param['SPEICHERN'] = 'on';
				$AWISBenutzer->ParameterSchreiben('Formular_EPO',serialize($Param));
			}
			else 
			{
				$Param['PERSNR'] = '';
				$Param['SPEICHERN'] = 'off';
				$AWISBenutzer->ParameterSchreiben('Formular_EPO',serialize($Param));
			}
		}
		
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_EPO',true));
		$Form->Formular_Start();

		if(isset($_POST['txtEPO_PERSNR']))
		{
			if($_POST['txtEPO_PERSNR'] <> '')
			{
				$SQL=  'select * from personal_komplett ';
				$SQL.= 'where persnr = '.$_POST['txtEPO_PERSNR'];
				$rsEPO = $DB->RecordSetOeffnen($SQL);
		
				if($rsEPO->AnzahlDatensaetze() <> 0)
				{
					$Email = true;
				}
				else
				{
					$Form->Hinweistext($AWISSprachKonserven['Wort']['FalschePerNr'], 1);
				}
			}
			else
			{
				$Form->Hinweistext($AWISSprachKonserven['Wort']['FalschePerNr'], 1);
			}
		}
		
		if($Email)
		{
			$SQL=  'select * from personalinfos ';
			$SQL.= 'where pif_per_nr = '.$_POST['txtEPO_PERSNR'];
			$SQL.= ' and pif_ity_key = 312';
			$rsEmail = $DB->RecordSetOeffnen($SQL);
			if($rsEmail->AnzahlDatensaetze() == 0)
			{
				$Form->Hinweistext($AWISSprachKonserven['Wort']['KeineMail'], 1);
			}
		}
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Personalnummer'].':',200);
		$Form->Erstelle_TextFeld('!EPO_PERSNR',($Param['SPEICHERN']=='on'?$Param['PERSNR']:''),6,150,true,'','','','T','L','','',8);	
		
		if($Email)
		{
			if($rsEmail->AnzahlDatensaetze() <> 0)
			{
				$Form->Erstelle_TextLabel($AWISSprachKonserven['EPO']['EPO_NAME'].':',80);
				$Form->Erstelle_TextLabel($rsEPO->FeldInhalt('NAME').','.$rsEPO->FeldInhalt('VORNAME'),250);
				//$Form->Erstelle_TextLabel('Bitte pr�fen!',1,'font-size:0.9em;BACKGROUND-COLOR: #ffff00;COLOR: #ff0000;border-width:1px;border-style:outset;width:82px;height:18px');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['EPO']['EPO_GEBDAT'].':',80);
				$Form->Erstelle_TextLabel($Form->Format('D',$rsEPO->FeldInhalt('GEB_DATUM')),100);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['IdentPruefung'],1,'font-size:0.9em;BACKGROUND-COLOR: #ffff00;COLOR: #ff0000;border-width:1px;border-style:outset;width:228px;height:18px');
			}
		}
		
		$Form->ZeileEnde();
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),150,true,'on');
		
		
		if($Email)
		{
			if($rsEmail->AnzahlDatensaetze() <> 0)
			{
				$Form->Erstelle_TextLabel($AWISSprachKonserven['EPO']['EPO_EMAIL'].':',80);
				$Form->Erstelle_TextLabel($rsEmail->FeldInhalt('PIF_WERT'),200);
			}
		}
		
		$Form->ZeileEnde();
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
		//$Form->Schaltflaeche('image', 'cmdMail', '', '/bilder/cmd_statistik.png', $AWISSprachKonserven['Wort']['Auswertung'], 'W');
		$Form->SchaltflaechenEnde();

		$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
		
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>