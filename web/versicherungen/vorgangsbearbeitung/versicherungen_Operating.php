<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('VJS', '*');
    $TextKonserven[] = array('VJI', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    //$Form->DebugAusgabe(1,$_POST);
    //$Form->DebugAusgabe(1,$_GET);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $SQL = 'SELECT * ';
    $SQL .= 'FROM versjobid ';

    if($DB->ErmittleZeilenAnzahl($SQL) >= 1)
    {
        $rsVJI = $DB->RecordSetOeffnen($SQL);

        $Form->Formular_Start();
        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJI']['VJI_JOBID'], 110, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJI']['VJI_DATUMJOBID'], 180, '', false);

        $Form->ZeileEnde();
        $VJIZeile = 0;

        while(!$rsVJI->EOF())
        {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('VJI_JOBID', $rsVJI->FeldInhalt('VJI_JOBID'), 0, 110, false, ($VJIZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VJI_DATUMJOBID', $rsVJI->FeldInhalt('VJI_DATUMJOBID'), 0, 180, false, ($VJIZeile % 2), '', '', 'D');
            $Form->ZeileEnde();

            $rsVJI->DSWeiter();
            $VJIZeile++;
        }
    }

    $SQL = 'SELECT * ';
    $SQL .= 'FROM versjobsteuerung ';
    //$SQL .= 'WHERE VJS_SYSTEM = \'Versicherung\' ';
    $SQL .= 'ORDER BY VJS_REIHENFOLGE';

    if($DB->ErmittleZeilenAnzahl($SQL) >= 1)
    {
        $rsVJS = $DB->RecordSetOeffnen($SQL);

        $Form->Formular_Start();
        $Form->ZeileStart();

        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJS']['VJS_SYSTEM'], 110, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJS']['VJS_MODUL'], 180, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJS']['VJS_JOBID'], 50, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJS']['VJS_STARTZEIT'], 170, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJS']['VJS_ENDEZEIT'], 170, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJS']['VJS_LAUFZEIT'], 80, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VJS']['VJS_MODULSTATUS'], 400, '', false);

        $Form->ZeileEnde();

        $VJSZeile = 0;

        while(!$rsVJS->EOF())
        {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('VJS_SYSTEM', $rsVJS->FeldInhalt('VJS_SYSTEM'), 0, 110, false, ($VJSZeile % 2), '', '', 'T');
            //$Form->Erstelle_ListenFeld('VJS_REIHENFOLGE',$rsVJS->FeldInhalt('VJS_REIHENFOLGE'),0,150,false,($VJSZeile%2),'','','T');
            $Form->Erstelle_ListenFeld('VJS_MODUL', $rsVJS->FeldInhalt('VJS_MODUL'), 0, 180, false, ($VJSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VJS_JOBID', $rsVJS->FeldInhalt('VJS_JOBID'), 0, 50, false, ($VJSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VJS_STARTZEIT', $rsVJS->FeldInhalt('VJS_STARTZEIT'), 0, 170, false, ($VJSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VJS_ENDEZEIT', $rsVJS->FeldInhalt('VJS_ENDEZEIT'), 0, 170, false, ($VJSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VJS_LAUFZEIT', $rsVJS->FeldInhalt('VJS_LAUFZEIT') . 's', 0, 80, false, ($VJSZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VJS_MODULSTATUS', $rsVJS->FeldInhalt('VJS_MODULSTATUS'), 0, 400, false, ($VJSZeile % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsVJS->DSWeiter();
            $VJSZeile++;
        }
    }

    //Erstelle Unterregister

    $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:'');
    $SubReg = new awisRegister(9506);
    $SubReg->ZeichneRegister($RegisterSeite);

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->SchaltflaechenEnde();
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906231637");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
