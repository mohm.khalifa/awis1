<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
require_once 'aufgabensteuerung_funktionen.php';
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;
global $SpeichernFrame1;
global $SpeichernFrame2;
global $SpeichernFrame3;

try
{
    //var_dump($_REQUEST);
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SBV','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	
	$Recht40016 = $AWISBenutzer->HatDasRecht(40016);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SBV'));
	
	$Fehler = 0;

	
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1); 
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'zuv_sbname';
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
		
	if(isset($_POST['cmdSpeichern_x']))
	{
	    $SpeichernOK[1]=true;
	    $SpeichernOK[2]=true;
		include('./aufgabensteuerung_speichern.php');	
	}
	elseif(isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./aufgabensteuerung_loeschen.php');
	}
	
	$Form->SchreibeHTMLCode('<form name="frmSBVWiedervorlagenVertretung" action="./aufgabensteuerung_Main.php?cmdAktion=WiedervorlagenVertretung" method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	$Script = "
            <script type='text/javascript'>
   
            function EinAusklappen(frame,Seite) {
	
    	 	if(document.getElementById('FRAME'+frame).style.display == 'block')
        	{
        	     document.getElementById('FRAME'+frame).style.display = 'none';
    			 if(frame != 5)
    			 {
    				document.getElementById('UEB'+frame).style.borderBottom = '';
    			 }
    			 document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_plus.png';
    	    }
    		else
    		{
    			  document.getElementById('FRAME'+frame).style.display = 'block';
    			  if(frame != 5)
    			  {
    			  	document.getElementById('UEB'+frame).style.borderBottom = '1px solid black';
    			  }
    			  document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_minus.png';
    		}
	   
    	}
	    
	    function WVInhaberUebernehmen()
            {
                if (document.getElementsByName('txtSBV_WV_ZUVERTRETEN')[0].value != '::bitte w�hlen::')
                {
                    document.getElementsByName('sucSBV_WV_ID')[0].value = document.getElementsByName('txtSBV_WV_ZUVERTRETEN')[0].value;
                }
                else
                {
                     document.getElementsByName('sucSBV_WV_ID')[0].value = '';
                }
        
				document.getElementById('Hinweis').style.display = 'none';
                //alert(document.getElementsByName('txtBEW_BPK')[0].value);       
            }
	    
	       function VorgangUebernehmen()
            {
	            if (document.getElementsByName('txtSBV_oWV_ZUVERTRETEN')[0].value != '::bitte w�hlen::')
                {
	               document.getElementsByName('sucSBV_oWV_ID')[0].value = document.getElementsByName('txtSBV_oWV_ZUVERTRETEN')[0].value;
                
                }
                else
                {
                     document.getElementsByName('sucSBV_oWV_ID')[0].value = '';
                }
        
                //alert(document.getElementsByName('txtBEW_BPK')[0].value);       
            }
            
	       
	         function CheckAlle(Name)
	         {
	           var string = Name, arr=string.split('|'), i;
	           var box;
	           for (i in arr)
	           {
	               box=arr[i];
	               if (box!='')
	               {
                          //document.getElementsByName('txt'+box)[0].checked = true;
	                     document.getElementsByName('txt'+box)[0].checked=document.getElementsByName('txtcheckAlle')[0].checked;	                     
	               }
	           }
	           
	         }
	
             </script>
            ";
	
	echo $Script;
	
	$FeldBreiten = array();
	$FeldBreiten['SBV_ZUVERTRETEN'] = 250;
	$FeldBreiten['SBV_VERTRETER'] = 250;
	$FeldBreiten['ICON'] = 30;

	$SQLZuVertreten  ='SELECT';
	$SQLZuVertreten .='   a.id,';
	$SQLZuVertreten .='   a.sbname';
	$SQLZuVertreten .='   ||\' \'';
	$SQLZuVertreten .='   ||A.SBVORNAME';
	$SQLZuVertreten .='   ||\'(\'';
	$SQLZuVertreten .='   ||A.SBKUERZEL';
	$SQLZuVertreten .='   ||\')\' AS Name';
	$SQLZuVertreten .=' FROM';
	$SQLZuVertreten .='   sachbearbeiter a';
	$SQLZuVertreten .=' WHERE';
	$SQLZuVertreten .='   A.AUSGESCHIEDEN = 0';
	if (($Recht40016&64) != 64)
	{
	    $SQLZuVertreten .=' and AWIS_USER = ' .$DBSCHAD->WertSetzen('SBV', 'T', $AWISBenutzer->BenutzerID());
	}
	 
	$SQLZuVertreten .=' ORDER BY';
	$SQLZuVertreten .='   name';
	
	//echo $SQLZuVertreten;
    //Start Frame 2
	$Frame2 = isset($_GET['Frame2'])?false:true;
	$Aendern =  true;
	
    if($Frame2)
    {
        $Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border: 1px solid black;','UEB2"ID="UEB2');
        $Form->Schaltflaeche('script', 'cmdKlappen','onclick="EinAusklappen(2)"', '/bilder/icon_minus.png"ID="IMG_LOAD2','Ein-/Ausklappen', 'W','width:30px',17,17);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBV']['SBV_WIEDERVORLAGEN_STEUERUNG'], 300, 'color:#FFFFFF;font-weight:bolder');
        $Form->ZeileEnde();
        $Form->ZeileStart('display:block','FRM2"ID="FRAME2');
    }
    else
    {
        $Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;','UEB2"ID="UEB2');
        $Form->Schaltflaeche('script', 'cmdKlappen','onclick="EinAusklappen(2)"', '/bilder/icon_plus.png"ID="IMG_LOAD2','Ein-/Ausklappen', 'W','width:30px',17,17);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBV']['SBV_WIEDERVORLAGEN_STEUERUNG'], 300, 'color:#FFFFFF;font-weight:bolder');
        $Form->ZeileEnde();
        $Form->ZeileStart('display:none','FRM2"ID="FRAME2');
    }
    
    if($SpeichernFrame2!='')
    {
    	$Form->ZeileStart('display:block','HNW"ID="Hinweis');
        $Form->Hinweistext($SpeichernFrame2);
        $Form->ZeileEnde();
    }
    else 
    {
    	$Form->ZeileStart('display:none','HNW"ID="Hinweis');
    	$Form->Hinweistext($SpeichernFrame2);
    	$Form->ZeileEnde();
    }
    
    
    $SQL = ' Select * from Sachbearbeiter where AWIS_USER='.$AWISBenutzer ->BenutzerID();
    $SQL .= ' and ausgeschieden = 0';
    $rsSB = $DBSCHAD->RecordSetOeffnen($SQL);
    $SB = '-1';
    while (!$rsSB->EOF())
    {
    	$SB .= ', '.$rsSB->FeldInhalt('ID');
    	$DatenSB[] = $rsSB->FeldInhalt('ID').'~'.$rsSB->FeldInhalt('SBNAME');
    	$rsSB->DSWeiter();
    }
     
    
    //Eigene Postf�cher
    $SQL  ='select SP_KEY,SBNAME from SACHBEARBEITERZUORD sbz ';
    $SQL .=' inner join Sachbearbeiter sb on sbz.sp_key = sb.id ';
    $SQL .=' where SB_KEY in('.$SB .')' ;
    
    $rsSBZ = $DBSCHAD->RecordSetOeffnen($SQL);
    
    while(! $rsSBZ->EOF())
    {
    	$DatenSB[] = $rsSBZ->FeldInhalt('SP_KEY').'~'.$rsSBZ->FeldInhalt('SBNAME');
    	$rsSBZ->DSWeiter();
    }
    
    //Schauen ob ich jemanden Vertreten muss
    $VerSQL  =' Select * from Sachbearbeitervertreter ';
    $VerSQL .=' where SBV_GUELTIG_AB <= trunc(sysdate) ';
    $VerSQL .=' and (SBV_GUELTIG_BIS >= trunc(sysdate) or SBV_GUELTIG_BIS is null) ';
    $VerSQL .=' and SBV_VERTRETER_SB_ID in(' . $SB .')';
    $rsVer = $DBSCHAD->RecordSetOeffnen($VerSQL);
    
    if($rsVer->AnzahlDatensaetze()>0) //Wenn Ja
    {
    	//Dann hol die die Postf�cher von dem zu Vertretenen
    	$SQL  ='select SP_KEY,SBNAME from SACHBEARBEITERZUORD sbz ';
    	$SQL .=' inner join Sachbearbeiter sb on sbz.sp_key = sb.id ';
    	$SQL .=' where SB_KEY=0';
    	while(!$rsVer->EOF())
    	{
    		$SQL .=' or SB_KEY '. $DBSCHAD->LikeOderIst('0'.$rsVer->FeldInhalt('SBV_ZUVERTRETEN_SB_ID')   );
    		 
    		$rsVer->DSWeiter();
    	}
    	$rsSBZ = $DBSCHAD->RecordSetOeffnen($SQL);
    
    	//Alle durchgehen f�r das ComboboxenArray
    	while(! $rsSBZ->EOF())
    	{
    		if (!in_array($rsSBZ->FeldInhalt('SP_KEY').'~'.$rsSBZ->FeldInhalt('SBNAME'), $DatenSB))
    		{
    			$DatenSB[] = $rsSBZ->FeldInhalt('SP_KEY').'~'.$rsSBZ->FeldInhalt('SBNAME') . '(Vertr.)';
    		}
    
    		$rsSBZ->DSWeiter();
    	}
    
    	$rsVer->DSErster();
    
    	while(!$rsVer->EOF())
    	{
    		//Zu vertretene Sachbearbeiter auch holen und in das Comboboxenarray hinzuf�gen
    		$SQLSB = ' Select * from Sachbearbeiter where ID '. $DBSCHAD->LikeOderIst('0'.$rsVer->FeldInhalt('SBV_ZUVERTRETEN_SB_ID'));
    		$rsSBVER = $DBSCHAD->RecordSetOeffnen($SQLSB);
    
    		$DatenSB[]= $rsVer->FeldInhalt('SBV_ZUVERTRETEN_SB_ID'). '~'.$rsSBVER->FeldInhalt('SBNAME') . '(Vertr.)';
    
    		$rsVer->DSWeiter();
    	}
    }
    
    //Doppelte Werte rausschmei�en,
    $DatenSB = array_unique($DatenSB);
    
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SBV']['SBV_WV_INHABER'].':', 280);
    
    $AWISCursorPosition = 'txtSBV_WV_ZUVERTRETEN';
    if (($Recht40016&64) == 64)
    {
    	$Form->Erstelle_SelectFeld('SBV_WV_ZUVERTRETEN', ((!$SpeichernOK[2] and isset($_POST['txtSBV_WV_ZUVERTRETEN']))?$_POST['txtSBV_WV_ZUVERTRETEN']:(isset($_GET['Inhaber'])?$_GET['Inhaber']:'')), $FeldBreiten['SBV_ZUVERTRETEN'],true,$SQLZuVertreten,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'',null,'onchange="WVInhaberUebernehmen();key_SBV_WV_ID(event)"','',$DBSCHAD->Bindevariablen('SBV'),'','SCHAD');
    	
    }
    else 
    {
    	$Form->Erstelle_SelectFeld('SBV_WV_ZUVERTRETEN', ((!$SpeichernOK[2] and isset($_POST['txtSBV_WV_ZUVERTRETEN']))?$_POST['txtSBV_WV_ZUVERTRETEN']:(isset($_GET['Inhaber'])?$_GET['Inhaber']:'')), $FeldBreiten['SBV_ZUVERTRETEN'],true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'',$DatenSB,'onchange="WVInhaberUebernehmen();key_SBV_WV_ID(event)"','',$DBSCHAD->Bindevariablen('SBV'),'','SCHAD');
    }
   
    echo "<div style='display:none'";
    $Form->AuswahlBox('SBV_WV_ID', 'box1','', 'SBV_WV', '', 25,10,isset($_POST['sucSBV_WV_ID'])?$_POST['sucSBV_WV_ID']:(isset($_GET['Inhaber'])?$_GET['Inhaber']:''),'T',$Aendern,'','','',50,'','','',0,'');
    echo "</div>";
    
    ob_start();
   
    if(((isset($_POST['sucSBV_WV_ID']) and $_POST['sucSBV_WV_ID']!='') or isset($_GET['Inhaber'])) and !$SpeichernOK[2])
    {
        erstelleAjaxMitWV(isset($_GET['Inhaber'])?$_GET['Inhaber']:$_POST['sucSBV_WV_ID']);
    }
   
    $Inhalt = ob_get_contents();
    ob_end_clean();
    $Form->ZeileStart();
    $Form->AuswahlBoxHuelle('box1','AuswahlListe','',$Inhalt);
    
    $Form->ZeileEnde();
    
       
    $Form->ZeileEnde();//Ende Frame2
    
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if(($Recht40016&2)==2)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	$Form->Schaltflaeche('image', 'cmdExportXLSX', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export']);
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SBV',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

?>