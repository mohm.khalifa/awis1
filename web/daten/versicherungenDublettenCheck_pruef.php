<?php
require_once ('awisDatenbank.inc');
require_once ('awisFormular.inc');
require_once ('awisBenutzer.inc');

$AWISBenutzer = awisBenutzer::Init ();

// Textkonserven laden

$TextKonserven = array ();
$TextKonserven [] = array ('Wort','lbl_speichern');
$TextKonserven [] = array ('Wort','lbl_abbrechen');

$Form = new awisFormular ();

$Recht9500 = $AWISBenutzer->HatDasRecht ( 9500 );

// echo $Recht9500;

if ($Recht9500 == 0) 

{
	
	$Form->Formular_Start ();
	
	$Form->Fehler_KeineRechte ();
	
	$Form->Formular_Ende ();
	
	die ();
}

$DB = awisDatenbank::NeueVerbindung ( 'AWIS' );

$DB->Oeffnen ();

$AWISSprachKonserven = $Form->LadeTexte ( $TextKonserven );

$FeldBreiten = array ();
$FeldBreiten ['FILID'] = 40;
$FeldBreiten ['KFZ_KZ'] = 100;
$FeldBreiten ['WANR'] = 70;
$FeldBreiten ['VORGANGNR'] = 130;
$FeldBreiten ['VERSNR'] = 60;
$FeldBreiten ['DATUMUHRZEIT'] = 150;
$FeldBreiten ['UMSATZ'] = 100;
$FeldBreiten ['SCHADKZ'] = 40;

// Infozeile zusammenbauen



// $Form->SchreibeHTMLCode('<form name="frmDublettenCheck" action="./versicherungenDublettenCheck.php?cmdAktion=Details" method=POST enctype="multipart/form-data">');

$Form->ZeileStart ();

$Freigabe_KEYS = substr ( $_GET ['VDB_KEYS'], 1 );


if ($Freigabe_KEYS != '') 
{	
	
	$SQL  = ' SELECT * ';
	$SQL .= ' FROM VERSICHERUNGEN_DUBLETTEN';
	$SQL .= ' where vdb_vvs_key in (' . $Freigabe_KEYS . ')';
	$SQL .= ' and VDB_status != 0';
	
	$rsCheck = $DB->RecordSetOeffnen ( $SQL );
	
	if($rsCheck->AnzahlDatensaetze() > 0)
	{
		$Form->Hinweistext('Datensatz wird bereits bearbeitet. Nach Schließen erneut ausführen. System schleust den bereits bearbeiteten Datensatz automatisch aus.');
		$Form->SchaltflaechenStart ();
		$Form->Schaltflaeche ( 'image', 'cmd_abbrechen_oSp', '', '/bilder/cmd_ds.png', $AWISSprachKonserven ['Wort'] ['lbl_abbrechen'], 'A' );
		$Form->SchaltflaechenEnde ();
	}
	else 
	{
		$SQL = 'update versicherungen_dubletten';	
		$SQL .= ' set vdb_status = 1,';	
		$SQL .= ' vdb_userdat = sysdate, ';
		$SQL .= ' vdb_user = \''.$AWISBenutzer->BenutzerName().'\'';;
		$SQL .= ' where vdb_vvs_key in (' . $Freigabe_KEYS . ')';
		
		$DB->Ausfuehren ( $SQL );
		
		$SQL = 'update versicherungen_dubletten';	
		$SQL .= ' set vdb_status = 2';
		$SQL .= ' ,vdb_userdat = sysdate, ';
		$SQL .= ' vdb_user = \''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ' where vdb_vvs_key in (';	
		$SQL .= ' select vdb.vdb_vvs_key';	
		$SQL .= ' from ';	
		$SQL .= ' (select vdb_vvs_key, vdb_filid, vdb_versnr, vdb_kassedatum, vdb_kfzkennz_uf';	
		$SQL .= ' from versicherungen_dubletten';	
		$SQL .= ' where vdb_status = 1';	
		$SQL .= ' ) vv';	
		$SQL .= ' inner join versicherungen_dubletten vdb';	
		$SQL .= ' on vv.vdb_filid = vdb.vdb_filid';	
		$SQL .= ' and vv.vdb_versnr = vdb.vdb_versnr';	
		$SQL .= ' and vv.vdb_kassedatum = vdb.vdb_kassedatum';	
		$SQL .= ' and vv.vdb_kfzkennz_uf = vdb.vdb_kfzkennz_uf';	
		$SQL .= ' where vdb_status = 0)';
		
		$DB->Ausfuehren ( $SQL );
	
	
		$SQL = ' Select * from versicherungen_dubletten ';
		$SQL .= 'where vdb_vvs_key in (' . $Freigabe_KEYS . ')';
		
		$rsVDB = $DB->RecordSetOeffnen ( $SQL );
		
		if ($rsVDB->AnzahlDatensaetze () > 0) 
		{
			
			$Form->ZeileStart ();
			$Form->Erstelle_TextFeld ( 'Freigabe', 'Folgende Datensätze wurden zur Freigabe ausgewählt:', 10, 400, false, 'font-weight:bolder;', '', '', '', 'L' );	
			$Form->ZeileEnde ();
			
			$Form->ZeileStart ();	
			$Form->Erstelle_Liste_Ueberschrift ( 'FILNR', $FeldBreiten ['FILID'], 'font-weight:bolder;', '' );	
			$Form->Erstelle_Liste_Ueberschrift ( 'WANR', $FeldBreiten ['WANR'], 'font-weight:bolder;', '' );	
			$Form->Erstelle_Liste_Ueberschrift ( 'Vorgangnr.', $FeldBreiten ['VORGANGNR'], 'font-weight:bolder;', '' );	
			$Form->Erstelle_Liste_Ueberschrift ( 'KFZ_Kennz.', $FeldBreiten ['KFZ_KZ'], 'font-weight:bolder;', '' );	
			$Form->Erstelle_Liste_Ueberschrift ( 'VersNr', $FeldBreiten ['VERSNR'], 'font-weight:bolder;', '' );	
			$Form->Erstelle_Liste_Ueberschrift ( 'Datum/Uhrzeit Kasse', $FeldBreiten ['DATUMUHRZEIT'], 'font-weight:bolder;', '' );	
			$Form->Erstelle_Liste_Ueberschrift ( 'Ges_Umsatz', $FeldBreiten ['UMSATZ'], 'font-weight:bolder;', '' );	
			$Form->Erstelle_Liste_Ueberschrift ( 'SKZ', $FeldBreiten ['SCHADKZ'], 'font-weight:bolder;', '' );
			
			$DS2 = 0;
			
			while ( ! $rsVDB->EOF () ) 
			{
				$Form->ZeileStart ( 'width:100%' );
				$Form->Erstelle_ListenFeld ( 'VDB_FILID', $rsVDB->FeldInhalt ( 'VDB_FILID' ), 0, $FeldBreiten ['FILID'], false, ($DS2 % 2), 'color:green', '', 'T' );
				$Form->Erstelle_ListenFeld ( 'VDB_WANR', $rsVDB->FeldInhalt ( 'VDB_WANR' ), 0, $FeldBreiten ['WANR'], false, ($DS2 % 2), 'color:green', '', 'T' );		
				$Form->Erstelle_ListenFeld ( 'VDB_VORGANGNR', $rsVDB->FeldInhalt ( 'VDB_VORGANGNR' ), 0, $FeldBreiten ['VORGANGNR'], false, ($DS2 % 2), 'color:green', '', 'T' );		
				$Form->Erstelle_ListenFeld ( 'VDB_KFZKENNZ', $rsVDB->FeldInhalt ( 'VDB_KFZKENNZ' ), 0, $FeldBreiten ['KFZ_KZ'], false, ($DS2 % 2), 'color:green', '', 'T' );		
				$Form->Erstelle_ListenFeld ( 'VDB_VERSNR', $rsVDB->FeldInhalt ( 'VDB_VERSNR' ), 0, $FeldBreiten ['VERSNR'], false, ($DS2 % 2), 'color:green', '', 'T' );		
				$Form->Erstelle_ListenFeld ( 'VDB_KASSEDATUM_UHRZEIT', $rsVDB->FeldInhalt ( 'VDB_KASSEDATUM_UHRZEIT' ), 0, $FeldBreiten ['DATUMUHRZEIT'], false, ($DS2 % 2), 'color:green', '', 'DU' );		
				$Form->Erstelle_ListenFeld ( 'VDB_UMSATZGES', $rsVDB->FeldInhalt ( 'VDB_UMSATZGES' ), 0, $FeldBreiten ['UMSATZ'], false, ($DS2 % 2), 'color:green', '', 'N2' );		
				$Form->Erstelle_ListenFeld ( 'VDB_SCHADENKZ', $rsVDB->FeldInhalt ( 'VDB_SCHADENKZ' ), 0, $FeldBreiten ['SCHADKZ'], false, ($DS2 % 2), 'color:green', '', 'T' );		
				$Form->ZeileEnde ();
				
				$DS2 ++;
				
				$rsVDB->DSWeiter ();
			}
			
			$SQL = 'select *';	
			$SQL .= ' from ';	
			$SQL .= ' (select vdb_vvs_key, vdb_filid, vdb_versnr, vdb_kassedatum, vdb_kfzkennz_uf';	
			$SQL .= ' from versicherungen_dubletten';	
			$SQL .= ' where vdb_status = 1';
			$SQL .= ' ) vv';	
			$SQL .= ' inner join versicherungen_dubletten vdb';	
			$SQL .= ' on vv.vdb_filid = vdb.vdb_filid';	
			$SQL .= ' and vv.vdb_versnr = vdb.vdb_versnr';	
			$SQL .= ' and vv.vdb_kassedatum = vdb.vdb_kassedatum';	
			$SQL .= ' and vv.vdb_kfzkennz_uf = vdb.vdb_kfzkennz_uf';	
			$SQL .= ' where vdb_status = 2';
			
			$rsAufzuloesendeVorgaenge = $DB->RecordSetOeffnen ( $SQL );
			
			$sperrKeys = '';
			
			if ($rsAufzuloesendeVorgaenge->AnzahlDatensaetze () > 0) 
			{
				$Form->ZeileStart ();
				
				$Form->Erstelle_TextFeld ( 'Filler', '', 10, 400, false, '', '', '', '', 'L' );
				$Form->ZeileEnde ();
				
				$Form->ZeileStart ();
				$Form->Erstelle_TextFeld ( 'Aufloesen', 'Folgende Datensätze werden hiermit aufgelöst und gesperrt:', 10, 400, false, 'font-weight:bolder;', '', '', '', 'L' );
				$Form->ZeileEnde ();
				
				$Form->ZeileStart ();
				
				$Form->Erstelle_Liste_Ueberschrift ( 'FILNR', $FeldBreiten ['FILID'], 'font-weight:bolder;', '' );
				$Form->Erstelle_Liste_Ueberschrift ( 'WANR', $FeldBreiten ['WANR'], 'font-weight:bolder;', '' );
				$Form->Erstelle_Liste_Ueberschrift ( 'Vorgangnr.', $FeldBreiten ['VORGANGNR'], 'font-weight:bolder;', '' );
				$Form->Erstelle_Liste_Ueberschrift ( 'KFZ_Kennz.', $FeldBreiten ['KFZ_KZ'], 'font-weight:bolder;', '' );
				$Form->Erstelle_Liste_Ueberschrift ( 'VersNr', $FeldBreiten ['VERSNR'], 'font-weight:bolder;', '' );
				$Form->Erstelle_Liste_Ueberschrift ( 'Datum/Uhrzeit Kasse', $FeldBreiten ['DATUMUHRZEIT'], 'font-weight:bolder;', '' );
				$Form->Erstelle_Liste_Ueberschrift ( 'Ges_Umsatz', $FeldBreiten ['UMSATZ'], 'font-weight:bolder;', '' );
				$Form->Erstelle_Liste_Ueberschrift ( 'SKZ', $FeldBreiten ['SCHADKZ'], 'font-weight:bolder;', '' );
				
				$DS2 = 0;
				
				while ( ! $rsAufzuloesendeVorgaenge->EOF () ) 
				{
					
					$sperrKeys = $sperrKeys . ',' . $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_VVS_KEY' );
					
					$Form->ZeileStart ( 'width:100%' );
					
					$Form->Erstelle_ListenFeld ( 'VDB_FILID', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_FILID' ), 0, $FeldBreiten ['FILID'], false, ($DS2 % 2), 'color:red', '', 'T' );
					$Form->Erstelle_ListenFeld ( 'VDB_WANR', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_WANR' ), 0, $FeldBreiten ['WANR'], false, ($DS2 % 2), 'color:red', '', 'T' );			
					$Form->Erstelle_ListenFeld ( 'VDB_VORGANGNR', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_VORGANGNR' ), 0, $FeldBreiten ['VORGANGNR'], false, ($DS2 % 2), 'color:red', '', 'T' );			
					$Form->Erstelle_ListenFeld ( 'VDB_KFZKENNZ', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_KFZKENNZ' ), 0, $FeldBreiten ['KFZ_KZ'], false, ($DS2 % 2), 'color:red', '', 'T' );			
					$Form->Erstelle_ListenFeld ( 'VDB_VERSNR', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_VERSNR' ), 0, $FeldBreiten ['VERSNR'], false, ($DS2 % 2), 'color:red', '', 'T' );			
					$Form->Erstelle_ListenFeld ( 'VDB_KASSEDATUM_UHRZEIT', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_KASSEDATUM_UHRZEIT' ), 0, $FeldBreiten ['DATUMUHRZEIT'], false, ($DS2 % 2), 'color:red', '', 'DU' );			
					$Form->Erstelle_ListenFeld ( 'VDB_UMSATZGES', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_UMSATZGES' ), 0, $FeldBreiten ['UMSATZ'], false, ($DS2 % 2), 'color:red', '', 'N2' );			
					$Form->Erstelle_ListenFeld ( 'VDB_SCHADENKZ', $rsAufzuloesendeVorgaenge->FeldInhalt ( 'VDB_SCHADENKZ' ), 0, $FeldBreiten ['SCHADKZ'], false, ($DS2 % 2), 'color:red', '', 'T' );			
					$Form->ZeileEnde ();
					
					$DS2 ++;
					
					$rsAufzuloesendeVorgaenge->DSWeiter ();
				}
				
				$sperrKeys = substr ( $sperrKeys, 1 );
			}
			
			$Form->Erstelle_HiddenFeld ( 'SperrKeys', $sperrKeys );	
			$Form->Erstelle_HiddenFeld ( 'FreigabeKeys', $Freigabe_KEYS );
			
			$SQL = 'update versicherungen_dubletten';
			$SQL .= ' set vdb_status = 3';
			$SQL .= ' where vdb_vvs_key in (' . $Freigabe_KEYS . ')';
			
			$DB->Ausfuehren ( $SQL );
			
			if($sperrKeys!='')
			{
				$SQL = 'update versicherungen_dubletten';
				$SQL .= ' set vdb_status = 4';
				$SQL .= ' where vdb_vvs_key in (' . $sperrKeys . ')';
				
				$DB->Ausfuehren ($SQL);
			}
		
			
			$Form->SchaltflaechenStart ();
			$Form->Schaltflaeche ( 'image', 'cmd_dubclear', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven ['Wort'] ['lbl_speichern'], 'S' );	
			$Form->Schaltflaeche ( 'image', 'cmd_abbrechen', '', '/bilder/cmd_ds.png', $AWISSprachKonserven ['Wort'] ['lbl_abbrechen'], 'A' );	
			$Form->SchaltflaechenEnde ();
		} 
		else 
		{
			$Form->Hinweistext('Datensatz wurde bereits bearbeitet');
			$Form->SchaltflaechenStart ();
			$Form->Schaltflaeche ( 'image', 'cmd_abbrechen_oSp', '', '/bilder/cmd_ds.png', $AWISSprachKonserven ['Wort'] ['lbl_abbrechen'], 'A' );
			$Form->SchaltflaechenEnde ();
		}
	}
}
else 
{
	echo '<span class=HinweisText>Es wurden keine Datensätze zur Freigabe ausgewählt.</span>';
	$Form->SchaltflaechenStart ();
	$Form->Schaltflaeche ( 'image', 'cmd_abbrechen_oSp', '', '/bilder/cmd_ds.png', $AWISSprachKonserven ['Wort'] ['lbl_abbrechen'], 'A' );
	$Form->SchaltflaechenEnde ();
}

?>

