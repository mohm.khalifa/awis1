<?php
require_once ('awisDatenbank.inc');
require_once ('awisFormular.inc');
require_once ('awisBenutzer.inc');
require_once ('awisGiftcard.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $Testsystem = false;


    for($i=1;$i<$argc;$i++)
    {
        switch (strtolower($argv[$i]))
        {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--testsystem':
                $Testsystem = true;
                break;
            default:
                echo 'Parameter '.$argv[$i].' ist unbekannt.';
            case '--help':
                echo $argv[0].PHP_EOL;
                echo 'Optionen:'.PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                echo '  --testsystem                       Daten aus dem Testsystem abfragen.'.PHP_EOL;

                echo PHP_EOL;
                die();
        }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $giftcard = new awisGiftcard();
    $giftcard->DebugLevel($DebugLevel);
    $giftcard->_GiftCardVerarbeitung();

}
catch (Exception $ex)
{

}

?>