<?php
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;

try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('SCH', '%');
    $TextKonserven[]=array('PVS','lst_Waehlen');
    $TextKonserven[]=array('Wort', 'lbl_zurueck');
    $TextKonserven[]=array('Wort', 'PDFErzeugen');
    $TextKonserven[]=array('Wort', 'lbl_suche');
    $TextKonserven[]=array('Wort', 'lbl_trefferliste');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht25030 = $AWISBenutzer->HatDasRecht(25010);
    if($Recht25030 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    
    $Form->SchreibeHTMLCode('<form name=frmTODO action=/berichte/drucken.php?XRE=36&ID=' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' method=POST >');

    $EditRecht = true;
    
    $Form->Formular_Start();
    //Infozeile mit Daten zur letzten �nderung durch einen Mitarbeiter und Button zur�ck zur Suchseite
    $Felder = array();
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./schluesselzentrale_Main.php?cmdAktion=Suche' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=T title=' . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0? $rsUNF->FeldInhalt('UNF_USER'): $AWISBenutzer->BenutzerName()));	// bei Edit => User aus DB; bei New => aktUser 
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0? $rsUNF->FeldInhalt('UNF_USERDAT'): date('d.m.Y H:i:s')));			// bei Edit => Dat aus DB; bei New => aktDat
    $Form->InfoZeile($Felder, '');
    //Checkbox zum ausw�hlen, ob nur die DS angezeigt werden sollen, wo die Schluessel aktuell ausgegeben sind
    //oder ob alle DS angezeigt werden sollen (auch die wo Schluessel schon zur�ckgegeben oder als verloren gemeldet
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Historie'].':',200);
    $Form->Erstelle_Checkbox('*SCH_Historie',(isset($Param['SCH_Historie']) && $Param['SCH_Historie']=='on'?'on':''),50,true,'on');
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_ErsteEbene'].':',200);
    $Liste = explode("|",$AWISSprachKonserven['SCH']['lst_SCH_AUSWAHL']);
    $Form->Erstelle_SelectFeld('*SCH_ErsteEbene','',200,$EditRecht,'','','','','',$Liste,'');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_ZweiteEbene'].':',200);
    $AktuelleDaten = '::alle::';
    //$Form->Erstelle_SelectFeld('UNF_URSACHEKEY', $this->_Param['UrsacheKey'], 0, true, '***UFT_Daten;txtUNF_URSACHEKEY;UFT_GEHOERTZUUFTKEY=*txtUNF_KATEGORIEKEY&ZUSATZ=~'.$this->_SprachKonserven['Wort']['txt_BitteWaehlen'].'&BEREICHID=8', (empty($this->_Param['UrsacheKey'])? '~'.$this->_SprachKonserven['Wort']['txt_BitteWaehlen']: $this->_Param['UrsacheKey'].'~'.$this->_Param['UrsacheID'].'  '.$this->_Param['Ursache']), '', '', '');
    $Form->Erstelle_SelectFeld('*SCH_ZweiteEbene','',"220:180",$EditRecht,'***SCH_Daten;sucSCH_ZweiteEbene;AUSWAHLTYP=*SCH_ErsteEbene&HISTORIE=*SCH_Historie&WERT=','','','','',$AktuelleDaten,'','','','');
    $SQL1 = 'SELECT sch_key, sch_Bezeichnung from SchlSchluessel';
    $Form->Erstelle_SelectFeld('*SCH_Key',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['SCH_Key']:''),50,true,$SQL1,$AktuelleDaten);
    $Form->ZeileEnde();

    $Form->Formular_Ende();
    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�  
    $LinkPDF = '/berichte/drucken.php?XRE=36&ID=';
    //$LinkPDF = '/schluesselzentrale/schluesselzentrale_Main.php?cmdAktion=Bericht';
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdPDF', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen'], 'P');	
    if (($Recht25030&1) == 1)
    {	
        
    }

    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>