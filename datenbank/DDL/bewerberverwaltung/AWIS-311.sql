insert into berichte (xre_key, xre_bezeichnung, xre_klassenname,xre_format,xre_ausrichtung, XRE_vorlage,xre_xrc_id,xre_stufe,xre_protokolleintrag,xre_status,xre_bemerkung,xre_user,xre_userdat)
values (77,'Absage nach langer Bearbeitungszeit', 'ber_Bewerberverwaltung_Absage_NachBearbeitungszeit','A4','P','Vorlage_BewerberverwaltungATU.pdf',39001,1,1,'A','Bewerberverwaltung Absage lange Bearbeitungszeit','Andre Meyer','23.10.2017');

update bewerberstatus set BWS_BEZEICHNUNG = 'Absage Bewerber � Unterlagen behalten', bws_textkonserve = 'BEW:BEW_STATUS_ABSUNTERBEH' where bws_key = 6;

INSERT INTO textkonserven (
  xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de
) VALUES (
  'BEW_STATUS_ABSUNTERBEH','BEW','Absage Bewerber Unterlagen behalten','Absage Bewerber Unterlagen behalten'
);

insert into bewerberstatus
(bws_bezeichnung,bws_textkonserve,bws_user,bws_userdat,bws_aktiv,bws_datensatzaktiv,bws_steuer_id,bws_zwischenseitentyp,bws_aktionsscript)
values
('Absage nach langer Bearbeitungszeit', 'BEW:BEW_STATUS_ABSLANGEZEIT','meyer_a',SYSDATE,'A',0,1007,1,'bewerberverwaltung_Details_Aktion_Absage_Nach_Bearbeitungszeit.php');

insert into TEXTKONSERVEN (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) values ( 'BEW_STATUS_ABSLANGEZEIT', 'BEW','Absage nach langer Bearbeitungszeit','Absage nach langer Bearbeitungszeit');

Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Adresse-Groesse','10','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','LinkerRand','20','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','RechterRand','20','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','RechterRand','30','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Schriftart','Arial','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Seite1-Oben','20','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Text-Groesse','10','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Text-Groesse-Klein','8','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Ueberschrift-Groesse','14','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Zeilenhoehe','5','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','Zeilenhoehe-Gross','13','Andre Meyer','23.10.2017');
Insert into BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('77','ZwUeberschrift-Groesse','12','Andre Meyer','23.10.2017');

set define off;
Update textkonserven set xtx_text_de = 'A.T.U Auto-Teile-Unger GmbH & Co. KG
Dr.-Kilian-Str. 11  92637 Weiden i.d. OPf.' where xtx_kennung = 'BEW_PDF_ANSCHRIFT_ATU' and xtx_bereich = 'BEW';
set define off;
Update textkonserven set xtx_text_de = 'ESTATO Umweltservice GmbH
Dr.-Kilian-Stra�e 11, 92637 Weiden i.d.OPf.' where xtx_kennung = 'BEW_PDF_ANSCHRIFT_ESTATO' and xtx_bereich = 'BEW';
set define off;
Update textkonserven set xtx_text_de = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U. Auto Teile Unger Handels GmbH & Co.KG' where xtx_kennung = 'BEW_PDF_SIGNATUR_1' and xtx_bereich = 'BEW';
set define off;
Update textkonserven set xtx_text_de = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U. Auto Teile Unger Handels GmbH & Co.KG' where xtx_kennung = 'BEW_PDF_SIGNATUR_2' and xtx_bereich = 'BEW';
set define off;
Update textkonserven set xtx_text_de = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U. Auto Teile Unger Handels GmbH & Co.KG' where xtx_kennung = 'BEW_PDF_SIGNATUR_3' and xtx_bereich = 'BEW';
set define off;
Update textkonserven set xtx_text_de = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U. Auto Teile Unger Handels GmbH & Co.KG' where xtx_kennung = 'BEW_PDF_SIGNATUR_4' and xtx_bereich = 'BEW';
set define off;
Update textkonserven set xtx_text_de = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U. Auto Teile Unger Handels GmbH & Co.KG' where xtx_kennung = 'BEW_PDF_SIGNATUR_5' and xtx_bereich = 'BEW';
set define off;
Update textkonserven set xtx_text_de = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U. Auto Teile Unger Handels GmbH & Co.KG' where xtx_kennung = 'BEW_PDF_SIGNATUR_6' and xtx_bereich = 'BEW';



------ Problem SQLs

set define off;
UPDATE textkonserven SET xtx_TEXT_DE = 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

wir beziehen uns auf Ihre Bewerbung und bedanken uns nochmals f�r Ihr Interesse an einer Mitarbeit in unserem Hause.

Nach sorgf�ltiger Pr�fung Ihrer Bewerbungsunterlagen m�ssen wir Ihnen heute leider mitteilen, dass es dieses Mal nicht geklappt hat. Es gab in diesem Fall andere Bewerber, deren berufliches Profil noch besser zu den Anforderungen der Stelle gepasst hat. Oftmals sind es nur Nuancen, die den Ausschlag f�r die Entscheidung geben.

Nach wie vor finden wir Ihre Unterlagen jedoch sehr Interessant und w�rden gerne mit Ihnen im Kontakt bleiben. Ihr Einverst�ndnis vorausgesetzt, w�rden wir gerne Ihre Daten in unserem Bewerbertool speichern, um wieder aus Sie zukommen zu k�nnen, wenn wir eine Einstiegsm�glichkeit sehen.

Alle unsere freien Stellen finden Sie daneben auch auf unserer Homepage www.atu.de. Schauen Sie doch hin und wieder einmal hinein. Wenn Sie Ihrerseits eine interessante Stelle sehen, freuen wir aus auf ihre Bewerbung.

In der Zwischenzeit danken wir Ihnen f�r Ihr Vertrauen und w�nschen Ihnen f�r Ihre berufliche und pers�nliche Zukunft alles Gute und viel Erfolg.
', xtx_kennung = 'BEW_PDF_ABSAGE_UNTERLAGEN_BEHALTEN_TEXT'
WHERE
  xtx_kennung = 'BEW_PDF_BEWERBERPOOL_TEXT'
  AND
  xtx_bereich = 'BEW';

set define off;
UPDATE textkonserven SET xtx_TEXT_DE = 'Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

vielen Dank f�r Ihre Bewerbung und das damit zum Ausdruck gebrachte Interesse an einer Mitarbeit in unserem Unternehmen.

Nach eingehender Pr�fung Ihrer Bewerbungsunterlagen m�ssen wir Ihnen heute leider mitteilen, dass es dieses Mal nicht geklappt hat. Wir bedauern, Ihnen keine anderslautende R�ckmeldung geben zu k�nnen. Sicherlich haben Sie mit Ihrer Bewerbung Erwartungen und Hoffnungen verbunden und es f�llt uns schwer, diese nun zu entt�uschen.

Wir schreiben jedoch immer wieder Stellen aus und m�chten Sie ermutigen, uns auf unserer Homepage www.atu.de zu besuchen. �ber Ihre erneute Bewerbung zu einem sp�teren Zeitpunkt  w�rden wir uns freuen.

Wir danken Ihnen f�r Ihr Vertrauen und w�nschen Ihnen f�r Ihre berufliche Neuorientierung alles Gute und viel Erfolg.
'
WHERE
  xtx_kennung = 'BEW_PDF_ABSAGE_TEXT'
  AND
  xtx_bereich = 'BEW';


set define off;
insert into textkonserven (xtx_kennung,xtx_bereich,xtx_bemerkung,xtx_text_de) values ('BEW_PDF_ABSAGE_NACH_BEARBEITUNGSZEIT_TEXT', 'BEW', 'PDF-Text einer Absage bei der Bewerberverwaltung','Sehr #GEEHRT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,

vielen Dank f�r Ihre Bewerbung und das damit zum Ausdruck gebrachte Interesse an einer Mitarbeit in unserem Unternehmen. Gleichzeitig entschuldigen wir uns vielmals f�r die lange Bearbeitungszeit.

Nach eingehender Pr�fung Ihrer Bewerbungsunterlagen m�ssen wir Ihnen heute leider mitteilen, dass es dieses Mal nicht geklappt hat. Wir bedauern, Ihnen keine anderslautende R�ckmeldung geben zu k�nnen. Sicherlich haben Sie mit Ihrer Bewerbung Erwartungen und Hoffnungen verbunden und es f�llt uns schwer, diese nun zu entt�uschen.

Wir schreiben jedoch immer wieder Stellen aus und m�chten Sie ermutigen, uns auf unserer Homepage www.atu.de zu besuchen. �ber Ihre erneute Bewerbung zu einem sp�teren Zeitpunkt  w�rden wir uns freuen.

Wir danken Ihnen f�r Ihr Vertrauen und w�nschen Ihnen f�r Ihre berufliche Neuorientierung alles Gute und viel Erfolg.
');

set define off;
insert into Mailversandtexte (MVT_BEREICH, MVT_SPRACHE, MVT_BETREFF, MVT_TEXT,MVT_GUELTIGAB, MVT_USER, MVT_USERDAT)
values
('BEW_ABSAGE_BEARBEITUNGSZEIT','DE','Ihre Bewerbung bei #FIRMIERUNG#','<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta name=Generator content="Microsoft Word 11 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
{font-family:"MS Mincho";
panose-1:2 2 6 9 4 2 5 8 3 4;}
@font-face
{font-family:"\@MS Mincho";
panose-1:0 0 0 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
{margin:0cm;
margin-bottom:.0001pt;
font-size:12.0pt;
font-family:"Times New Roman";}
a:link, span.MsoHyperlink
{color:blue;
text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
{color:navy;
text-decoration:underline;}
p.txt, li.txt, div.txt
{margin-top:0cm;
margin-right:0cm;
margin-bottom:3.0pt;
margin-left:0cm;
line-height:13.0pt;
text-autospace:none;
font-size:10.0pt;
font-family:Arial;}
span.E-MailFormatvorlage18
{mso-style-type:personal;
font-family:Arial;
color:windowtext;}
span.E-MailFormatvorlage19
{mso-style-type:personal-reply;
font-family:Arial;
color:navy;}
@page Section1
{size:595.3pt 841.9pt;
margin:72.0pt 90.0pt 72.0pt 90.0pt;}
div.Section1
{page:Section1;}
-->
</style>

</head>

<body lang=DE link=blue vlink=navy>

<div class=Section1>

<p class=txt style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><font
size=2 color=black face=Arial><span style='font-size:10.0pt;color:black'>Sehr
#GEEHERT# #ANREDE# #HRK_TITEL# #HRK_VORNAME# #HRK_NACHNAME#,<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>
vielen Dank f�r Ihre Bewerbung und das damit zum Ausdruck gebrachte Interesse an einer Mitarbeit in unserem Unternehmen. Gleichzeitig entschuldigen wir uns vielmals f�r die lange Bearbeitungszeit.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Nach eingehender Pr�fung Ihrer Bewerbungsunterlagen m�ssen wir Ihnen heute leider mitteilen, dass es dieses Mal nicht geklappt hat. Wir bedauern, Ihnen keine anderslautende R�ckmeldung geben zu k�nnen. Sicherlich haben Sie mit Ihrer Bewerbung Erwartungen und Hoffnungen verbunden und es f�llt uns schwer, diese nun zu entt�uschen.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Wir schreiben jedoch immer wieder Stellen aus und m�chten Sie ermutigen, uns auf unserer Homepage www.atu.de zu besuchen. �ber Ihre erneute Bewerbung zu einem sp�teren Zeitpunkt w�rden wir uns freuen.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Wir danken Ihnen f�r Ihr Vertrauen und w�nschen Ihnen f�r Ihre berufliche Neuorientierung alles Gute und viel Erfolg.<o:p></o:p></span></font></p>

</div>

</body>

</html>','01.01.1970','meyer_a',SYSDATE);

set define off;
UPDATE MAILVERSANDTEXTE set mvt_TExt = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta name=Generator content="Microsoft Word 11 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
{font-family:"MS Mincho";
panose-1:2 2 6 9 4 2 5 8 3 4;}
@font-face
{font-family:"\@MS Mincho";
panose-1:0 0 0 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
{margin:0cm;
margin-bottom:.0001pt;
font-size:12.0pt;
font-family:"Times New Roman";}
a:link, span.MsoHyperlink
{color:blue;
text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
{color:navy;
text-decoration:underline;}
p.txt, li.txt, div.txt
{margin-top:0cm;
margin-right:0cm;
margin-bottom:3.0pt;
margin-left:0cm;
line-height:13.0pt;
text-autospace:none;
font-size:10.0pt;
font-family:Arial;}
span.E-MailFormatvorlage18
{mso-style-type:personal;
font-family:Arial;
color:windowtext;}
span.E-MailFormatvorlage19
{mso-style-type:personal-reply;
font-family:Arial;
color:navy;}
@page Section1
{size:595.3pt 841.9pt;
margin:72.0pt 90.0pt 72.0pt 90.0pt;}
div.Section1
{page:Section1;}
-->
</style>

</head>

<body lang=DE link=blue vlink=navy>

<div class=Section1>

<p class=txt style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><font
size=2 color=black face=Arial><span style='font-size:10.0pt;color:black'>Sehr
#GEEHERT# #ANREDE# #HRK_TITEL# #HRK_VORNAME# #HRK_NACHNAME#,<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>
vielen Dank f�r Ihre Bewerbung und das damit zum Ausdruck gebrachte Interesse an einer Mitarbeit in unserem Unternehmen.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Nach eingehender Pr�fung Ihrer Bewerbungsunterlagen m�ssen wir Ihnen heute leider mitteilen, dass es dieses Mal nicht geklappt hat. Wir bedauern, Ihnen keine anderslautende R�ckmeldung geben zu k�nnen. Sicherlich haben Sie mit Ihrer Bewerbung Erwartungen und Hoffnungen verbunden und es f�llt uns schwer, diese nun zu entt�uschen.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Wir schreiben jedoch immer wieder Stellen aus und m�chten Sie ermutigen, uns auf unserer Homepage www.atu.de zu besuchen. �ber Ihre erneute Bewerbung zu einem sp�teren Zeitpunkt w�rden wir uns freuen.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Wir danken Ihnen f�r Ihr Vertrauen und w�nschen Ihnen f�r Ihre berufliche Neuorientierung alles Gute und viel Erfolg.<o:p></o:p></span></font></p>

</div>

</body>

</html>' where mvt_bereich = 'BEW_ABSAGE';


set define off;
UPDATE MAILVERSANDTEXTE set mvt_TExt = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta name=Generator content="Microsoft Word 11 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
{font-family:"MS Mincho";
panose-1:2 2 6 9 4 2 5 8 3 4;}
@font-face
{font-family:"\@MS Mincho";
panose-1:0 0 0 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
{margin:0cm;
margin-bottom:.0001pt;
font-size:12.0pt;
font-family:"Times New Roman";}
a:link, span.MsoHyperlink
{color:blue;
text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
{color:navy;
text-decoration:underline;}
p.txt, li.txt, div.txt
{margin-top:0cm;
margin-right:0cm;
margin-bottom:3.0pt;
margin-left:0cm;
line-height:13.0pt;
text-autospace:none;
font-size:10.0pt;
font-family:Arial;}
span.E-MailFormatvorlage18
{mso-style-type:personal;
font-family:Arial;
color:windowtext;}
span.E-MailFormatvorlage19
{mso-style-type:personal-reply;
font-family:Arial;
color:navy;}
@page Section1
{size:595.3pt 841.9pt;
margin:72.0pt 90.0pt 72.0pt 90.0pt;}
div.Section1
{page:Section1;}
-->
</style>

</head>

<body lang=DE link=blue vlink=navy>

<div class=Section1>

<p class=txt style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><font
size=2 color=black face=Arial><span style='font-size:10.0pt;color:black'>Sehr
#GEEHERT# #ANREDE# #HRK_TITEL# #HRK_VORNAME# #HRK_NACHNAME#,<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>
wir beziehen uns auf Ihre Bewerbung und bedanken uns nochmals f�r Ihr Interesse an einer Mitarbeit in unserem Hause.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Nach sorgf�ltiger Pr�fung Ihrer Bewerbungsunterlagen m�ssen wir Ihnen heute leider mitteilen, dass es dieses Mal nicht geklappt hat. Es gab in diesem Fall andere Bewerber, deren berufliches Profil noch besser zu den Anforderungen der Stelle gepasst hat. Oftmals sind es nur Nuancen, die den Ausschlag f�r die Entscheidung geben.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Nach wie vor finden wir Ihre Unterlagen jedoch sehr Interessant und w�rden gerne mit Ihnen im Kontakt bleiben. Ihr Einverst�ndnis vorausgesetzt, w�rden wir gerne Ihre Daten in unserem Bewerbertool speichern, um wieder aus Sie zukommen zu k�nnen, wenn wir eine Einstiegsm�glichkeit sehen. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>Alle unsere freien Stellen finden Sie daneben auch auf unserer Homepage www.atu.de. Schauen Sie doch hin und wieder einmal hinein. Wenn Sie Ihrerseits eine interessante Stelle sehen, freuen wir aus auf ihre Bewerbung.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:
10.0pt;font-family:Arial;color:black'><o:p>&nbsp;</o:p></span></font></p>
<p class=MsoNormal><font size=2 color=black face=Arial><span style='font-size:10.0pt;font-family:Arial;color:black'>In der Zwischenzeit danken wir Ihnen f�r Ihr Vertrauen und w�nschen Ihnen f�r Ihre berufliche und pers�nliche Zukunft alles Gute und viel Erfolg.<o:p></o:p></span></font></p>

</div>

</body>

</html>' where mvt_bereich = 'BEW_BEWERBERPOOL';



