<?php
require_once 'awisDatenbank.inc';
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();

if(isset($_GET['ExportAdressenWEB_KEY']))
{
	$SQL = " Select WBE_FILIALEN FROM WERBEMITTELZUORDNUNG WHERE WBE_WEB_KEY=".$DB->FeldInhaltFormat('NO',$_GET['ExportAdressenWEB_KEY']);
	$SQL .= " ORDER BY WBE_FILIALEN ASC";
	
	$rsFilialen = $DB->RecordSetOeffnen($SQL);
	
	$SQL = "Select distinct Per_Fil_Id,Per_Nachname,Per_Vorname, Per_Strasse,Per_Plz,Per_Ort, Bul_Bundesland,";
	$SQL .= " verkaufsleiter, gebietsleiter,Xx1_Ebene As Region";
	$SQL .= " FROM werbemedium";
	$SQL .= " inner join werbemittelzuordnung on wbe_web_key = web_key";
        $SQL .= " inner join v_fil_vkl_gbl on fil_id = wbe_filialen";
	$SQL .= " inner join personal on per_fil_id = fil_id";
	$SQL .= " Left Join Filialinfos On Per_Fil_Id = Fif_Fil_Id And Fif_Fit_Id = 65";
        $SQL .= " Left Join Bundeslaender On Bul_Id = Fif_Wert";
	$SQL .= " Inner Join V_Filialpfad On Per_Fil_Id = Xx1_Fil_Id";
	$SQL .= " WHERE PER_FIL_ID=".$DB->FeldInhaltFormat('NO',$rsFilialen->FeldInhalt('WBE_FILIALEN'));
	$SQL .= " AND WEB_KEY=".$DB->FeldInhaltFormat('NO',$_GET['ExportAdressenWEB_KEY']);
	$SQL .= " AND PER_AUSTRITT IS NULL";
	$SQL .= " ORDER BY PER_FIL_ID";

	$rsExport = $DB->RecordSetOeffnen($SQL);

	@ob_clean();
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="'.'VerteilqualitaetExportKomplett'.'.csv"');

	$Zeile='';
	$Spalten = $rsExport->FeldListe();
	
	$Zeile = '';
	
	for($Spalte=0;$Spalte<=$rsExport->AnzahlSpalten() - 1;$Spalte++)
	{
		
		$Zeile .= ";".$Spalten[$Spalte];
		
	}
	$Zeile .= "\n";

	echo substr($Zeile,1);
	
	$Zeile='';
	
	while(!$rsFilialen->EOF())
	{
		$SQL = "Select distinct Per_Fil_Id,Per_Nachname,Per_Vorname, Per_Strasse,Per_Plz,Per_Ort, Bul_Bundesland,";
		$SQL .= " verkaufsleiter, gebietsleiter,Xx1_Ebene As Region";
		$SQL .= " FROM werbemedium";
		$SQL .= " inner join werbemittelzuordnung on wbe_web_key = web_key";
		$SQL .= " inner join v_fil_vkl_gbl on fil_id = wbe_filialen";
		$SQL .= " inner join personal on per_fil_id = fil_id";
		$SQL .= " Left Join Filialinfos On Per_Fil_Id = Fif_Fil_Id And Fif_Fit_Id = 65";
		$SQL .= " Left Join Bundeslaender On Bul_Id = Fif_Wert";
		$SQL .= " Inner Join V_Filialpfad On Per_Fil_Id = Xx1_Fil_Id";
		$SQL .= " WHERE web_key =".$DB->FeldInhaltFormat('NO',$_GET['ExportAdressenWEB_KEY']); 
		$SQL .= " AND PER_FIL_ID=".$DB->FeldInhaltFormat('NO',$rsFilialen->FeldInhalt('WBE_FILIALEN'));
		$SQL .= " AND PER_AUSTRITT IS NULL";
		$SQL .= " ORDER BY PER_FIL_ID";
		
		$rsExportKomplett = $DB->RecordSetOeffnen($SQL);
	
			for($CSVZeile=0;$CSVZeile<$rsExportKomplett->AnzahlDatensaetze()- 1;$CSVZeile++)
			{
			   $Zeile='';
				for($Spalte=0;$Spalte<=$rsExportKomplett->AnzahlSpalten()-1;$Spalte++)
				{
					$Zeile .= ";".$rsExportKomplett->FeldInhalt($Spalten[$Spalte]);
				
				}
				$rsExportKomplett->DSWeiter();
			   $Zeile .= "\n";
	
			echo substr($Zeile,1);
			}
		$rsFilialen->DSWeiter();
	}
    }
    die();
	
}
catch (Exception $ex)
{
}
?>