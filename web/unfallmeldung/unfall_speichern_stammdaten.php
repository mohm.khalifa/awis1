<?php
global $AWIS_KEY2;				// UnfallMeldungenInfoTypen: UFT_KEY
global $FehlerAugetreten;		// Flag: Beim Speichern ist ein Fehler aufgetreten (z.B. Pflichtfeld nicht gef�llt)
global $ListeAnzeigen;			// Flag: Liste anzeigen (nicht Detailansicht) 

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	$TextKonserven[]=array('Fehler', 'err_KeinWert');
	$TextKonserven[]=array('Fehler', 'err_WertBereitsVorhanden');
	$TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);	
	
	$FehlerText = '';				// r�cksetzen / initialisieren
	$FehlerAugetreten = false;		// r�cksetzen / initialisieren
	
	if (empty($_POST['txtUFT_KEY']) or ($_POST['txtUFT_KEY'] == 0))	
	{	// wenn kein Key mitkommt oder 0 ist => Neuanlage
		$Neuanlage = true;
	}
	else
	{	// ansonsten => Bearbeiten
		$Neuanlage = false;
	}

	// Pflichtfeld �berpr�fen: Bezeichnung nicht eingegeben
	if (empty($_POST['txtUFT_BEZEICHNUNG']))
	{
		$FehlerText .= $AWISSprachKonserven['Fehler']['err_KeinWert'] . '<br>';
		$FehlerText	.= $AWISSprachKonserven['UNF']['BEZEICHNUNG'] . '<br>';
		$FehlerAugetreten = true;		// Flag, dass Fehler
	}
	
	$FehlerText .= '<br>';
	
	// ID bereits vorhanden
	$SQL =	'select uft_kennung ' .							// Hole UFT_Kennung
			'from unfallmeldunginfotypen ' .
			'where uft_bereich = :var_UftBereich ' .		// wenn gleicher Bereich
			'and uft_bereichid = :var_UftBereichId ' . 		// wenn gleiche BereichID
			'and uft_status = 1 ' . 						// wenn Status = AKTIV
			'and uft_kennung = :var_UftId';					// wenn gleiche Kennung
	$BindeVariablen = array();
	$BindeVariablen['var_UftBereich'] = $_GET['Seite'];
	$BindeVariablen['var_UftBereichId'] = $_POST['txtUFT_BEREICHID'];
	$BindeVariablen['var_UftId'] = $_POST['txtUFT_KENNUNG'];		
	$AnzahlKennung = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);		// Anzahl der gefundenen Datens�tze
	$rsKennung = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
	
	if ( ( ($Neuanlage and ($AnzahlKennung != 0))	// Neuanlage => Kennung darf noch nicht vorhanden sein
	or	   (!$Neuanlage and (($_POST['oldUFT_KENNUNG']) != $rsKennung->FeldInhalt('UFT_KENNUNG')) and ($AnzahlKennung != 0)) )	// Bearbeiten => wenn alte und neu Kennung untzerschiedlich => Kennung darf noch nicht vorhanden sein
	and	 (! empty($_POST['txtUFT_KENNUNG'])) )		// und Kennung nicht leer => ganz leer w�r auch OK
	{
		$FehlerText .= $AWISSprachKonserven['Fehler']['err_WertBereitsVorhanden'] . '<br>';
		$FehlerText	.= $AWISSprachKonserven['UNF']['KENNUNG'] . '<br>';
		$FehlerAugetreten = true;		// Flag, dass Fehler
	}

	if ($FehlerAugetreten)	// wenn Fehler
	{
		$Form->Hinweistext($FehlerText, 1);
		$ListeAnzeigen = false;		// Flag, f�r Nicht Liste anzeigen, sondern nachbearbeiten (unfall_Stammdaten_Seite.php)
	}
	else					// wenn kein Fehler
	{
		if ($Neuanlage)
		{
			$BindeVariablen = array();	
			$SQL =	'insert into unfallmeldunginfotypen (' .
					'uft_information, uft_bereich, uft_bereichid, uft_status, uft_ueberschrift, ' .
					'uft_bemerkung, uft_user, uft_userdat, uft_kennung';
			if (isset($_POST['txtUFT_GEHOERTZU']))
			{
				if ($_POST['txtUFT_GEHOERTZU'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
				{
					$SQL .= ', uft_gehoertzuuftkey';
				}
			}
			$SQL .=	') values (' .
					':var_UftInformation, :var_UftBereich, :var_UftBereichId, 1, 0, ' .
					':var_UftBemerkung, :var_UftUser, SYSDATE, :var_UftId';
			if (isset($_POST['txtUFT_GEHOERTZU']))
			{ 
				if ($_POST['txtUFT_GEHOERTZU'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
				{
					$SQL .= ', :var_UftGehoertZuUftKey';
					$BindeVariablen['var_UftGehoertZuUftKey'] = $_POST['txtUFT_GEHOERTZU'];
				}
			}
			$SQL .=  ')';			
			$BindeVariablen['var_UftInformation'] = $_POST['txtUFT_BEZEICHNUNG'];
			$BindeVariablen['var_UftBereich'] = $_GET['Seite'];
			$BindeVariablen['var_UftBereichId'] = $_POST['txtUFT_BEREICHID'];
			$BindeVariablen['var_UftBemerkung'] = $_POST['txtUFT_BEMERKUNG'];
			$BindeVariablen['var_UftUser'] = $AWISBenutzer->BenutzerName();
			$BindeVariablen['var_UftId'] = $_POST['txtUFT_KENNUNG'];					
			$DB->Ausfuehren($SQL, '', true, $BindeVariablen);
		}
		else	// Bearbeiten 
		{
			$BindeVariablen = array();
			$SQL =	'update unfallmeldunginfotypen set ' .
					'uft_information = :var_UftInformation, ' .
					'uft_bemerkung = :var_UftBemerkung, ' .
					'uft_kennung = :var_UftId, ' .
					'uft_user = :var_UftUser, ' . 
					'uft_userdat = SYSDATE';
			if (isset($_POST['txtUFT_GEHOERTZU']))
			{
				if ($_POST['txtUFT_GEHOERTZU'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
				{
					$SQL .= ', uft_gehoertzuuftkey = :var_UftGehoertZuUftKey';
					$BindeVariablen['var_UftGehoertZuUftKey'] = $_POST['txtUFT_GEHOERTZU'];
				}
				else
				{
					$SQL .= ', uft_gehoertzuuftkey = null';
				}				
			}			
			$SQL .= ' where uft_key = :var_UftKey';
			$BindeVariablen['var_UftInformation'] = $_POST['txtUFT_BEZEICHNUNG'];
			$BindeVariablen['var_UftBemerkung'] = $_POST['txtUFT_BEMERKUNG'];
			$BindeVariablen['var_UftId'] = $_POST['txtUFT_KENNUNG'];
			$BindeVariablen['var_UftUser'] = $AWISBenutzer->BenutzerName();
			$BindeVariablen['var_UftKey'] = $_POST['txtUFT_KEY'];
			$DB->Ausfuehren($SQL, '', true, $BindeVariablen);								
		}
		$AWIS_KEY2 = 0;				// zur�cksetzen
		$ListeAnzeigen = true;		// Flag, damit Liste angezeigt wird (unfall_Stammdaten_Seite.php)
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>