<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $BearbeitungsStufe;
	global $AWISBenutzer;

	if(awisBenutzerRecht($con, 405))
	{
		
	}
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));
	
	$SQL = "select * from artikelstammVKAenderungen, Importquellen ";
	$SQL .= " WHERE AVK_AST_ATUNR='" . $Artikel[1] . "'";
	$SQL .= " AND AVK_LAN_CODE='DE' AND AVK_IMQ_ID = IMQ_ID(+) ";
	$SQL .= " ORDER BY AVK_DATUM DESC";

	$rsVK = awisOpenRecordset($con, $SQL);
	if($rsVK==FALSE)
	{
		awisErrorMailLink("artikel_VK.php", 2, $awisDBFehler['message']);
	}
	$rsVKZeilen=$awisRSZeilen;
	
	print "<table id=DatenTabelle width=800 border=1>";
	print "<cols><col width=150></col><col width=*></col><col width=150></col><col width=200><col width=100></col></col></cols>";
	print "<tr>";
	print "<td id=FeldBez>Preisänderung</td>";
	print "<td id=FeldBez>Verantwortlicher</td>";	
	print "<td id=FeldBez>Alter Preis</td>";	
	print "<td id=FeldBez>Neuer Preis</td>";	
	print "<td id=FeldBez>Quelle</td>";	
	print "</tr>";

	//**************************************
	// EKAT Infos
	//**************************************

	$AnzInfos=0;	
	for($Zeile=0;$Zeile<$rsVKZeilen;$Zeile++)
	{
		echo '<tr>';
	
		echo '<td>'	. $rsVK['AVK_DATUM'][$Zeile] . '</td>';
		echo '<td>'	. $rsVK['AVK_USER'][$Zeile] . '</td>';
		echo '<td align=right>'	. awis_format($rsVK['AVK_PREISALT'][$Zeile],'Currency') . '</td>';
		echo '<td align=right>'	. awis_format($rsVK['AVK_PREISNEU'][$Zeile],'Currency') . '</td>';
		if(isset($rsVK['IMQ_BEZEICHNUNG'][$Zeile]))
		{
			echo '<td >'.$rsVK['IMQ_BEZEICHNUNG'][$Zeile] . '</td>';
		}
		else
		{
			echo '<td >unbek.</td>';
		}
		echo '</tr>';
		$AnzInfos++;
	}
	if($AnzInfos==0)
	{
		echo '<tr><td colspan=5 align=center><font size=2>Keine Preisänderungen vorhanden.</font></td></tr>';
	}
	
	print "</table>";

	unset($rsVK);
?>