#!/bin/bash

cd /daten/jobs/fleet

for VERTRAG in 419525 419524 419522 419523 419520 419521 419526 419526 419518 419517 419519 419527 423193 423557 999999
do
  echo Exportiere OpalVertragNr $VERTRAG;
  #sqlplus -S FLADM/FLADM@FLEET @fleet_experian.sql $VERTRAG;
  sqlplus -S COMMFLEET_DATA/COMMFLEET_DATA@FLEET @fleet_experian.sql $VERTRAG;
done;

# Alle Dateien umbenennen
mv experian_kundenkfz_999999.csv sonstige_kundenkfz_999999_$(date +%Y%m%d).csv
mmv -m "experian_kundenkfz_*.*" "experian_kundenkfz_#1_$(date +%Y%m%d).#2"



echo Exportiere Fleet-Text Datei...
#sqlplus -S FLADM/FLADM@FLEET @fleet_text.sql;
sqlplus -S COMMFLEET_DATA/COMMFLEET_DATA@FLEET @fleet_text.sql;
echo Konvertiere Fleet-Text Datei fuer die MS Welt...
unix2dos -n fleet.txt fleet_MS.txt
chown awis fleet_MS.txt

echo Exportiere Fleet-Fehler-Text Datei...
#sqlplus -S FLADM/FLADM@FLEET @fleet_text_fehler.sql;
sqlplus -S COMMFLEET_DATA/COMMFLEET_DATA@FLEET @fleet_text_fehler.sql;

cp ./experian_*.csv /daten/daten/pcedv11/Daten/experian/kunden_kfz
cp ./sonstige_*.csv /daten/daten/pcedv11/Daten/experian/kunden_kfz
cp ./fleet*.txt /daten/daten/pcedv11/FleetDaten

rm ./experian_*.csv
rm ./sonstige_*.csv
rm ./fleet*.txt
