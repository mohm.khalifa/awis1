<?php
global $AWIS_KEY1;
global $ZPL;
require_once 'awisDiagramm.inc';
require_once 'awis_preislisten.inc';

try {

    if (isset($_FILES['Importdatei'])) {

        $Import = new awis_preislisten(true);
        $Import->MD5Check = false;
        $Erg = $Import->ImportAusMaske($_FILES['Importdatei']['tmp_name'],$_FILES['Importdatei']['name']);

        //Fehlerausgabe
        if ($Erg ===true) {
            $ZPL->Form->ZeileStart();
            $ZPL->Form->Hinweistext($ZPL->AWISSprachKonserven['ZPL']['ZPL_IMPORT_OK'],awisFormular::HINWEISTEXT_OK);
            $ZPL->Form->ZeileEnde();
        } else{
            $ZPL->Form->ZeileStart();
            $ZPL->Form->Hinweistext($ZPL->AWISSprachKonserven['ZPL'][$Erg],awisFormular::HINWEISTEXT_FEHLER);
            $ZPL->Form->ZeileEnde();
        }
    }


    $ZPL->Form->Formular_Start();
    echo '<form name=frmpreislisten action=./preislisten_Main.php?cmdAktion=Import method=POST  enctype="multipart/form-data">';


    $ZPL->Form->ZeileStart();
    $ZPL->Form->Erstelle_TextLabel('Import:',170,'font-weight: bold; font-size:15px');
    $ZPL->Form->ZeileEnde();

    $ZPL->Form->ZeileStart();
    $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['Wort']['Dateiname'] . ':', 150);
    $ZPL->Form->Erstelle_DateiUpload('Importdatei', 600, 30,2000000,'','.csv,.xls,.xlsx');
    $ZPL->Form->ZeileEnde();


    $ZPL->Form->Formular_Ende();
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $ZPL->Form->SchaltflaechenStart();
    $ZPL->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $ZPL->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        $ZPL->Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $ZPL->AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    $ZPL->Form->SchaltflaechenEnde();

    $ZPL->Form->SchreibeHTMLCode('</form>');

} catch (awisException $ex) {
    if ($ZPL->Form instanceof awisFormular) {
        $ZPL->Form->DebugAusgabe(1, $ex->getSQL());
        $ZPL->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $ZPL->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($ZPL->Form instanceof awisFormular) {
        $ZPL->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>