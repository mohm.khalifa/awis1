<?php
require_once 'awisSeminarplanEmail.php';

class awisSeminarplanEmailHotelAbbestellt extends awisSeminarplanEmail
{
    /**
     * awisSeminarplanEmailHotelAbbestellt constructor.
     * @param $SET_KEY
     */
    function __construct($SET_KEY)
    {
        parent::__construct($SET_KEY);
    }


    /**
     * Vearbeitet die Daten
     */
    function Verarbeitung($Empfaenger = '')
    {
        try {
            $SQL = "SELECT MVT_TEXT, MVT_BETREFF from MAILVERSANDTEXTE WHERE MVT_BEREICH = 'SET_HOTEL_ABBESTELLT'";
            $rsMailText = $this->_DB->RecordSetOeffnen($SQL);

            $Text = $rsMailText->FeldInhalt('MVT_TEXT');
            $this->Text($Text);

            $this->Betreff($rsMailText->FeldInhalt('MVT_BETREFF'));

            $this->Absender($this->_Benutzer->ParameterLesen('SEHAbsender'));

            if($Empfaenger==''){
                $Empfaenger = serialize(array('ACADEMY_HOTEL'));
                foreach ($this->EmpfaengerAufloesen($Empfaenger) as $Emp) {
                    $this->AdressListe($Emp, awisMailer::TYP_TO);
                }
            }else{
                $this->Adressliste($Empfaenger,awisMailer::TYP_TO);
            }

            if(count($this->getAdressliste('TO')) > 0){
                $this->Senden();
                return true;
            }else{
                return false;
            }


        } catch (awisException $e){
            return false;
        }
    }

    /**
     * Versendet die Email
     *
     * @return bool
     */
    public function Senden()
    {
        $this->_Mailer->Text($this->_Text, awisMailer::FORMAT_HTML);


        if(count($this->getAdressliste('TO'))>0){
            $this->_Mailer->Betreff($this->_Betreff);
        }else{
            $this->_Mailer->Betreff('ERROR: Hotel-Abbestellung bei SET_KEY ' . $this->_SET_KEY);
            $this->Adressliste('shuttle@de.atu.eu','TO');
        }

        return $this->_Mailer->MailInWarteschlange();
    }

}
