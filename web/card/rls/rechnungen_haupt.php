<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
?>
<html>
<head>
<title>Rechnungen</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets 



//Firma ermitteln

$strSql='select 
		case when "Firma" is null then
			case when "Vorname" is null then
				"Name"
			else
				"Name" || \' \' || "Vorname"
			end
		else
			case when "Abteilung" is null then
				"Firma"
			else
				"Firma" || \' \' || "Abteilung"
			end
		end || \', \' || "Ort" as "Firma"
		from '.$tabAdressen.' where "Kunden_Nr"=\''.$strKundennr.'\'
		and "Adress_Art"=\'01\'';

$arrAdressen=abfrage("db2",$strSql);

if($arrAdressen["zeilen"])
{
	?>
	
	<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
	
	<?php 
	
	$strFilter='';
	$strSqlFilter='';
	
	if(isset($strAuswahl)&&$strAuswahl!='alle')
	{
		$strFilter="Filter: ".$strAuswahl;
		
		switch($strAuswahl)
		{
			case "offen": $strSqlFilter=' where stand_rechnungen!=0';
			break;
			case "ausgeglichen": $strSqlFilter=' where stand_rechnungen=0';
			break;
			default: $strSqlFilter='';
		}
	}
	
	if(isset($strDatum))
	{
		if($strFilter=='')
		{
			$strFilter="Filter: ".$strDatum;
			$strSqlFilter=" where rek_datum='".$strDatum."'";
		}
		else
		{
			$strFilter.="/".$strDatum;
			$strSqlFilter.=" and rek_datum='".$strDatum."'";
		}	
	}
	
	if($strFilter!='')
		$strFilter=' ('.$strFilter.')';
	
	//Rechnungen ermitteln
	
	$strDb = "ora";
	
	$strSql="
	select * from
	(select a.rec_rnr,rek_datum,rek_betrag,
	(select nvl(sum(rek_betrag),0) from rechnungen_konto where rek_rnr=a.rec_rnr and rek_loesch=0) stand_rechnungen,
	(select nvl(sum(mag_betrag),0) from mahngebuehren where mag_rnr=a.rec_rnr and mag_loesch=0) stand_mahnungen
	from
	(select rec_rnr, rek_datum, rek_betrag from rechnungen,
	(select rek_rnr,rek_datum,rek_betrag from rechnungen_konto where rek_betrag<0 and rek_loesch=0 and rek_betragart=19)
	where
	rec_kunden_nr='".$strKundennr."'
	and rec_loesch=0
	and rec_rnr=rek_rnr 
	) a)".$strSqlFilter." order by rek_datum desc,rec_rnr";
	
	$strQueryString = "strKundennr=".$strKundennr;
	
	include("card/card_blaettern_haupt.inc.php");	//Blättern 
	
	
	
	?>
	<script language="JavaScript">
	
	<?php
	if($intZeitmessung)
	{
		echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.startzeit();	//Zeitmessung
	<?php } ?>
	
	function init()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
				
		document.ondblclick = zoom;		//Zoomfenster
	}
	
	//Wird bei Klick auf das Bild eingabe_ok (Parameter Rechnungsnr) ausgeführt, d.h. übermittelt die Rechnungsnr nach card_haupt.php
	
	function auswahlrg(rgnr)
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.sendezeit("Suche Rechnungsliste pro Kunde");	//Zeitmessung
		<?php } ?>
		parent.window.returnValue = rgnr;
		parent.self.close();
	}
	
	function filtern()
	{
		var parameter = "intDialog=1";
		
		<?php echo dialog("rechnungen_filter.php"); ?>
	
		if(rueckgabe!='undefined')
		{
			var arrRueck = rueckgabe.split(";");
						
			strFilter='&strAuswahl='+arrRueck[1];
			
			if(arrRueck[0]!='')
			{
				strFilter+='&strDatum='+arrRueck[0];
			}
			
			strQuery = querystringlesen();
					
			intPos = strQuery.indexOf("&strAuswahl");
			
			if(intPos!=-1)
			{
				strLoesch = strQuery.slice(intPos);
				querystringverkuerzen(strLoesch);
			}
					
			querystringerweitern(strFilter);
			
			location.href='<?php echo $PHP_SELF; ?>?'+querystringlesen();
		}
	}
	</script>
	
	</head>
	<body onload="init();" style="overflow:hidden;">
	<table cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr><td align="center" valign="top">
	<br>
	Name: <span class="titel"><?php echo $arrAdressen["trefferarray"]["Firma"][0]; ?></span><br>
	Kundennr: <span class="titel"><?php echo $strKundennr;?></span><br><br>
	Datens&auml;tze: <span class="titel"><?php echo $strPosition.$strFilter; ?></span><br><br>
		<table cellspacing="0" cellpadding="0">
		<tr><td>&nbsp;</td><td class="titel">Rechnungs-Nr:</td><td class="titel">Datum:</td><td class="titel">RG-Betrag:</td><td class="titel">RG offen:</td><td class="titel">MG offen:</td></tr>
		<?php
			$arrRechnungen = abfrage("ora",$strSqlScroll);
			
			$arrHilf = $arrRechnungen["trefferarray"];
			
			for($i=0;$i<$arrRechnungen["zeilen"];$i++)
			{
				echo "<tr>";
				echo "<td><img src=\"/bilder/eingabe_ok.png\" class=\"quadrat\" alt=\"Datensatz ausw&auml;hlen\" onclick=\"auswahlrg('".$arrHilf["REC_RNR"][$i]."');\"></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"rgnr\" value=\"".
				$arrHilf["REC_RNR"][$i]."\" readonly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"dat\" value=\"".
				$arrHilf["REK_DATUM"][$i]."\" readonly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"betr\" value=\"".
				eurobetrag($arrHilf["REK_BETRAG"][$i])."\" readonly></td>";
				if($arrHilf["STAND_RECHNUNGEN"][$i]!=0)
					$strFarbe=' style="color:red;"';
				else
					$strFarbe='';
				echo "<td class=\"text\"><input type=\"text\" class=\"betr\" value=\"".
				eurobetrag($arrHilf["STAND_RECHNUNGEN"][$i])."\" readonly".$strFarbe."></td>";
				if($arrHilf["STAND_MAHNUNGEN"][$i]!=0)
					$strFarbe=' style="color:red;"';
				else
					$strFarbe='';
				echo "<td class=\"text\"><input type=\"text\" class=\"betr\" value=\"".
				eurobetrag($arrHilf["STAND_MAHNUNGEN"][$i])."\" readonly".$strFarbe."></td>";
				echo "</tr>";
			}
			
		?>
		</table>
	</td></tr></table>
	<?php 
}
else		//Wenn keine Datensätze vorhanden sind
{
	echo "</head><body>";
	echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"top\">";
	echo "<br><span class=\"titel\">Die gesuchte Kundennummer<br>".$strKundennr."<br>existiert nicht!</span>";
	echo "</td></tr></table>";
}

$intDialog = 1;
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>
</body>
</html>