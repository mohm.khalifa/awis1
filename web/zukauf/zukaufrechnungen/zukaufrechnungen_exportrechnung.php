<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUR','%');
	$TextKonserven[]=array('ZUB','%');
	$TextKonserven[]=array('ZUL','%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10005 = $AWISBenutzer->HatDasRecht(10005);
	if(($Recht10005&16)==0)
	{
	    awisEreignis(3,1000,'Zukaufrechnungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZURPruefung'));

    $SQL = 'SELECT Zukaufrechnungen.*, MWS_SATZ, ZUL_DATUM, ZUL_LIEFERSCHEINNR, ZUB_BESTELLDATUM';
    $SQL .= ' FROM Zukaufrechnungen';
    $SQL .= ' LEFT OUTER JOIN MEHRWERTSTEUERSAETZE ON MWS_ID = ZUR_MWS_ID';
    $SQL .= ' LEFT OUTER JOIN ZukaufLieferscheine ON ZUL_KEY= ZUR_ZUL_KEY';
    $SQL .= ' LEFT OUTER JOIN ZukaufBestellungen ON ZUB_KEY= ZUR_ZUB_KEY';

    $Bedingung = ' AND ZUR_LIE_NR ='.$DB->FeldInhaltFormat('T',$Param['RECHPRUEFUNG_LIE_NR']);
    if($Param['RECHPRUEFUNG']!='')
    {
        $Bedingung .= ' AND ZUR_RECHNUNGSNUMMER='.$DB->FeldInhaltFormat('T',$Param['RECHPRUEFUNG']);
    }

    if($Param['RECHPRUEFUNG_ZUR_DATUM_VON']!='')
    {
        $Bedingung .= ' AND ZUR_DATUM >='.$DB->FeldInhaltFormat('D',$Param['RECHPRUEFUNG_ZUR_DATUM_VON']);
    }
    if($Param['RECHPRUEFUNG_ZUR_DATUM_BIS']!='')
    {
        $Bedingung .= ' AND ZUR_DATUM <='.$DB->FeldInhaltFormat('D',$Param['RECHPRUEFUNG_ZUR_DATUM_BIS']);
    }
    if($Param['RECHPRUEFUNG_ZUR_TYP']!='')
    {
        $Bedingung .= ' AND ZUR_TYP ='.$DB->FeldInhaltFormat('TU',$Param['RECHPRUEFUNG_ZUR_TYP']);
    }

    if($Bedingung!='')
    {
        $SQL .= ' WHERE ' . substr($Bedingung,4);
    }

    $rsZUR = $DB->RecordsetOeffnen($SQL);
    
    header('Pragma: public');
    header('Cache-Control: max-age=0');
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename="rechnungskontrolle_'.$Param['RECHPRUEFUNG'].'.csv"');

    $Felder = 'ZUR_KEY,ZUR_LIE_NR,ZUR_WANR,ZUR_TYP,ZUR_EXTERNEID,ZUR_DATUM';
    $Felder .= ',ZUR_LIEFERSCHEINNUMMER,ZUL_DATUM,ZUR_ZUL_KEY';
    $Felder .= ',ZUR_ZUB_KEY,ZUB_BESTELLDATUM,ZUR_WAEHRUNG,ZUR_ARTIKELNUMMER,ZUR_ARTIKELBEZEICHNUNG';
    $Felder .= ',ZUR_HERSTELLER,ZUR_MENGE,ZUR_MENGENEINHEIT,ZUR_EINZELPREIS,ZUR_EINHEITENPROPREIS';
    $Felder .= ',ZUR_RABATT1,ZUR_RABATT2,ZUR_RABATT3,ZUR_BETRAG,ZUR_FIL_ID,ZUR_BEMERKUNG,ZUR_MWST,ZUR_ABSCHLUSSART';
    $Felder .= ',ZUR_ABSCHLUSSGRUND,ZUR_RECHNUNGSNUMMER,ZUR_VKPR';
    $Felder .= ',ZUR_EKPR,ZUR_BESTELLID,ZUR_EKPRGERECHNET,ZUR_ALTTEILMENGE,ZUR_ALTTEILWERT';
    $Felder .= ',ZUR_ARCHIVID';

    $FeldListe = explode(',',$Felder);

    foreach($FeldListe AS $Feld)
    {
        echo $AWISSprachKonserven[substr($Feld,0,3)][$Feld]."\t";
    }
    echo PHP_EOL;

    while(!$rsZUR->EOF())
    {
        foreach($FeldListe AS $Feld)
        {
            echo $rsZUR->FeldInhalt($Feld)."\t";

        }
        echo PHP_EOL;
        $rsZUR->DSWeiter();
    }
}
catch(Exception $ex)
{
    echo $ex->getMessage();
}
?>
