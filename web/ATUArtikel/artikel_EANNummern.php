<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $BearbeitungsStufe;
	global $AWISBenutzer;

		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));

	$SQL = "SELECT * FROM EANNummern, TeileInfos ";
	$SQL .= " WHERE TEI_KEY2=EAN_KEY AND TEI_ITY_ID2 = 'EAN' AND TEI_KEY1=0" . $Artikel[0];
//echo "<hr>$SQL<hr>";

	$rsEANNummer = awisOpenRecordset($con, $SQL);
	if($rsEANNummer==FALSE)
	{
		awisErrorMailLink("artikel_EANNummern.php", 2, $awisDBFehler['message']);
	}
	$rsEANNummerZeilen=$awisRSZeilen;

	echo "<table id=DatenTabelle width=500 border=1>";
	echo "<cols><col width=20></col><col width=200></col><col width=220></col></cols>";
	echo "<tr>";
	echo "<tr><td id=FeldBez></td>";
	echo "<td id=FeldBez>EAN-Nummer</td>";
	echo "<td id=FeldBez>Typ</td>";
	echo "</tr>";

	for($Zeile=0;$Zeile<$rsEANNummerZeilen;$Zeile++)
	{
		echo "<tr>";

		if(($BearbeitungsStufe & 4)==4)			// Recht 401
		{
			echo "<td><input name=cmdEANLoeschen_" . $rsEANNummer["EAN_KEY"][$Zeile] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\"></td>";
		}
		else
		{
			echo "<td>&nbsp;</td>";
		}
		echo "<td><a href=./artikel_Main.php?cmdAktion=EANArtikel&EANKEY=" . $rsEANNummer["EAN_KEY"][$Zeile];
		echo ">" . $rsEANNummer["EAN_NUMMER"][$Zeile] . "</a></td>";
		echo "<td>" . EANTypText($rsEANNummer["EAN_TYP"][$Zeile]) . "</td>";

		echo "</tr>";
	}

		// Neue Zuordnung erlaubt?
	if(($BearbeitungsStufe & 4)==4)			// Recht 401
	{
		echo "<tr>";
		echo "<td><input name=txtEAN_KEY_0 type=hidden></td>";
		echo "<td><input name=txtEAN_Nummer size=30></td>";
		echo "<td>&nbsp;</td>";
		echo "</tr>";
	}

	echo "</table>";

	unset($rsEANNummer);

/**
 * L�st das Kennzeichen in einen Text auf
 *
 * @param string $Kennzeichen
 * @return string
 * @author Sacha Kerres
 * @since 04.05.2006
 */
function EANTypText($Kennzeichen)
{
	switch ($Kennzeichen) {
		case '1':
			return 'Artikel';
		case '2':
			return 'Dienstleistung';
		case 'S':
			return 'Sonderartikel';
		case 'H':
			return 'Hilfs- und Betriebsstoff';
		case 'A':
			return 'Altteil';
		case 'U':
			return 'Umweltservice';
		default:
			return $Kennzeichen;
	}
}

	?>

