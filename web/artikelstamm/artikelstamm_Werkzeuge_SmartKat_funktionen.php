<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

class artikelstamm_Werkzeuge_SmartKat_funktionen
{

    public $AWISSprachKonserven;
    public $AWISBenutzer;
    public $Form;
    public $DB;
    public $MaxDSAnzahl;
    public $Param;
    public $Recht463;
    public $FilterFelder = array();
    public $BitteWaehlen;

    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->Form = new awisFormular();

        $this->MaxDSAnzahl = $this->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $Recht458 = $this->AWISBenutzer->HatDasRecht(458);// Recht f�r Werkzeuge
        $this->Recht463 = $this->AWISBenutzer->HatDasRecht(463);// Recht f�r das Werkzeug "SmartKat"

        if (($Recht458 & 8) == 0) {
            $this->Form->Formular_Start();
            $this->Form->Fehler_KeineRechte();
            $this->Form->Formular_Ende();
            die();
        }

        $TextKonserven = array();
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'BedingungHinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'wrd_Filiale');
        $TextKonserven[] = array('Liste', 'lst_JaNein');
        $TextKonserven[] = array('Fehler', 'err_keineRechte');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Wort', 'txt_NaechsteFreieNummer');
        $TextKonserven[] = array('AST', 'AST_ATUNR');
        $TextKonserven[] = array('AST', 'AST_BEZEICHNUNG');
        $TextKonserven[] = array('WGR', 'WGR_ID');
        $TextKonserven[] = array('WGR', 'WGR_BEZEICHNUNG');
        $TextKonserven[] = array('WUG', 'WUG_ID');
        $TextKonserven[] = array('WUG', 'WUG_BEZEICHNUNG');
        $TextKonserven[] = array('WUG', 'WUG_KEY');
        $TextKonserven[] = array('LIE', 'LIE_NR');
        $TextKonserven[] = array('LAR', 'LAR_LARTNR');
        $TextKonserven[] = array('Liste', 'txtVerknuepfung');
        $TextKonserven[] = array('AST', 'AST_SK_SPEICHERN_OK');
        $TextKonserven[] = array('AST', 'AST_SK_SPEICHERN_NOK');
        $TextKonserven[] = array('AST', 'AST_HILFE_SK_IST_ZUSTAND');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven);
        $this->BitteWaehlen = $this->AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
        $this->Param = unserialize($this->AWISBenutzer->ParameterLesen('Formular_SmartKat'));

        $this->_FilterFelderInit();
    }

    private function _FilterFelderInit()
    {
        $this->FilterFelder[] = 'txtAST_ATUNR';
        $this->FilterFelder[] = 'txtAST_BEZEICHNUNG';
        $this->FilterFelder[] = 'cmbWGR_ID';
        $this->FilterFelder[] = 'cmbWUG_KEY';
        $this->FilterFelder[] = 'cmbLIE_NR';
    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_SmartKat', serialize($this->Param));
    }

    public function MergeAlle($Wert, $ErrCount = 0)
    {
        try {
            $SQL = $this->Param['SQL'];

            $Bindevariablen = $this->Param['Bindevariablen'];

            $this->DB->BindevariablensatzArray2BindevariablenSatz($Bindevariablen, 'SMK');
            $Merge = 'merge INTO teileinfosinfos a USING';
            $Merge .= ' (';
            $Merge .= 'Select distinct TEI_KEY from (' . $SQL . ')';
            $Merge .= ' ) src ON (src.TEI_KEY = a.TII_TEI_KEY and a.TII_ITY_KEY = 318)';
            $Merge .= ' WHEN matched THEN';
            $Merge .= '   UPDATE SET TII_WERT = ' . $this->DB->WertSetzen('SMK', 'Z', $Wert);
            $Merge .= ' WHEN NOT matched THEN';
            $Merge .= '   INSERT';
            $Merge .= '     (';
            $Merge .= '       TII_TEI_KEY,';
            $Merge .= '       TII_WERT,';
            $Merge .= '       tii_ity_key,';
            $Merge .= '       tii_user,';
            $Merge .= '       tii_userdat';
            $Merge .= '     )';
            $Merge .= '     VALUES';
            $Merge .= '     (';
            $Merge .= '       src.TEI_KEY,';
            $Merge .= $this->DB->WertSetzen('SMK', 'Z', $Wert);
            $Merge .= ',       318,';
            $Merge .= $this->DB->WertSetzen('SMK', 'T', $this->AWISBenutzer->BenutzerName());
            $Merge .= ',       sysdate';
            $Merge .= '     )';

            $this->DB->Ausfuehren($Merge, '', true, $this->DB->Bindevariablen('SMK'));
            return true;
        } catch (awisException $ex) {
            if (strpos($ex->getMessage(), 'kjbrchkpkeywait:timeout') and $ErrCount <= 20) {
                echo 'Toller ORA 0600';
                ob_flush();
                sleep(2);
                $this->MergeAlle($Wert, $ErrCount++);
            } elseif ($ErrCount >= 20) {
                echo 'Nicht l�sbarer Datenbankfehler (ORA-00600)';
            }else{
                $this->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'WIEDERHOLEN', 5, '201714021459');
            }

            return false;
        }
    }

    public function MergeAuserwaehlte(array $LarKeys, $Wert, $ErrCount = 0)
    {
        try {
            $SQL = '';
            foreach ($LarKeys as $KEY) {
                $SQL .= " UNION SELECT $KEY as TEI_KEY, $Wert as TII_WERT from dual";
            }
            if ($SQL === '') {
                return true;
            }

            $SQL = substr($SQL, 6);

            $Merge = 'merge INTO TEILEINFOSINFOS a USING';
            $Merge .= ' (';
            $Merge .= 'Select distinct TEI_KEY, TII_WERT  from (' . $SQL . ')';
            $Merge .= ' ) src ON (src.TEI_KEY = a.TII_TEI_KEY and TII_ITY_KEY = 318)';
            $Merge .= ' WHEN matched THEN';
            $Merge .= '   UPDATE SET a.TII_WERT = src.TII_WERT ';
            $Merge .= ' WHEN NOT matched THEN';
            $Merge .= '   INSERT';
            $Merge .= '     (';
            $Merge .= '       TII_TEI_KEY,';
            $Merge .= '       TII_WERT,';
            $Merge .= '       tii_ity_key,';
            $Merge .= '       Tii_user,';
            $Merge .= '       Tii_userdat';
            $Merge .= '     )';
            $Merge .= '     VALUES';
            $Merge .= '     (';
            $Merge .= '       src.TEI_KEY,';
            $Merge .= ' src.TII_WERT ';
            $Merge .= ',       318,';
            $Merge .= $this->DB->WertSetzen('SMK', 'T', $this->AWISBenutzer->BenutzerName());
            $Merge .= ',       sysdate';
            $Merge .= '     )';

            $this->DB->Ausfuehren($Merge, '', true, $this->DB->Bindevariablen('SMK'));

            return true;
        } catch (awisException $ex) {
            if (strpos($ex->getMessage(), 'kjbrchkpkeywait:timeout') and $ErrCount <= 20) {
                ob_flush();
                sleep(2);
                $this->MergeAuserwaehlte($LarKeys, $Wert, $ErrCount++);
            } elseif ($ErrCount >= 20) {
                $this->Form->Fehler_Anzeigen('SpeicherFehler', 'Nicht l�sbarer Datenbankfehler (ORA-0600). Breche nach 20 Versuchen ab.', 'WIEDERHOLEN', 5, '20170120171735');
            } else {
                $this->Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'WIEDERHOLEN', 5, '20170120171735');
            }

            return false;
        }
    }

    public function ladebalkenStart()
    {
        echo '
            <script>
            var WertNeu;
            var faktor = 1000; 
            
                
            function animiere() {
                        $("#ladebalken").width($("#txtProzent").val() * 10 + "px");
             
                        if ($("#txtProzent").val() < 100) {
                            WertNeu = parseInt($("#txtProzent").val())+1;
                            if(WertNeu > 100){
                                WertNeu = 100;
                            }
                            $("#txtProzent").val(WertNeu);              
                            window.setTimeout(animiere, (3*faktor));
                        }
                         
                        if(WertNeu == 100){
                            $("#Ladebalken").hide();
                            $("#Maske").show();
                        }
                        
                    }
            </script>
            
            ';

        echo '<div id="Ladebalken">';
        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel('Fortschritt: ', 100);
        $this->Form->Erstelle_TextFeld('Prozent', '0', 3, 50, true, '', '', '', '', '', '', '', '', '', '', 'disabled');
        $this->Form->Erstelle_TextLabel('% ', 100);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->SchreibeHTMLCode('<div id="ladebalken" style="display: block;width: 0px; background: blue">&nbsp;</div>');
        $this->Form->ZeileEnde();
        echo '</div>';
        echo '<div id="Maske" style="display: none;" >';
        echo '  <script type="text/javascript">animiere();</script>';

        ob_implicit_flush(true);
        ob_end_flush();
    }

    public function ladebalkenEnde()
    {

        echo '</div>';
        echo '<script>faktor = 1;</script>';
    }
}


?>