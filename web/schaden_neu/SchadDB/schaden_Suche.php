<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try
{
	$TextKonserven = array();
	$TextKonserven[]=array('BES','*');
	//$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	//$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	//$TextKonserven[]=array('Wort','AlleAnzeigen');
	
	$script = "<script type='text/javascript'>
	function blendeEinBemerkung(obj) {
    	       var checkWert = obj.options[obj.selectedIndex].value;
			   var checkText = obj.options[obj.selectedIndex].text;
    	       if(checkWert == '0' && checkText == '::bitte w�hlen::') // bitte w�hlen
    	       {
    	           document.getElementById('DB').style.display = 'none';
				   document.getElementById('AUF').style.display = 'none';
				   document.getElementsByName('cmdSuche')[0].style.display = 'none'; 
				   document.getElementsByName('cmdExportXLSX')[0].style.display = 'none'; 
	           }
    	       else if(checkWert == '1') // Datenbank
    	        {
					document.getElementById('DB').style.display = 'block';
					document.getElementsByName('cmdSuche')[0].style.display = 'block'; 
					document.getElementsByName('cmdExportXLSX')[0].style.display = 'none'; 
					document.getElementById('AUF').style.display = 'none';
				}
    	       else // Aufgaben
    	        {
    	            document.getElementById('DB').style.display = 'none';
					document.getElementById('AUF').style.display = 'block';
					document.getElementsByName('cmdSuche')[0].style.display = 'block';
					document.getElementsByName('cmdExportXLSX')[0].style.display = 'block'; 
    	        }
	
	}
	</script>";
	
	echo $script;
	

	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	
	$Sachbearbeiter = false;
	$SQL = ' Select * from Sachbearbeiter where AWIS_USER='.$AWISBenutzer ->BenutzerID();
	$SQL .= ' and ausgeschieden = 0';
	$rsSB = $DBSCHAD->RecordSetOeffnen($SQL);
	
	if($rsSB->AnzahlDatensaetze() > 0)
	{
		$Sachbearbeiter = true;
	}
	else
	{
		$Sachbearbeiter = false;
	}
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht40000 = $AWISBenutzer->HatDasRecht(40000);

	if($Recht40000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

		$Form->SchreibeHTMLCode('<form name=frmSchaden method=post action=./schaden_Main.php?cmdAktion=Details>');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BES'));

		$Form->Formular_Start();

		if($Sachbearbeiter)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SUCHE'].':',200);
			$Daten = explode('|',$AWISSprachKonserven['BES']['lst_SUCHE']);
			$Form->Erstelle_SelectFeld('*BES_SUCHE',($Param['SPEICHERN']=='on'?$Param['BES_SUCHE']:''),150,true,'','0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'onchange="blendeEinBemerkung(this);"');
			$Form->ZeileEnde();
			$AWISCursorPosition = 'sucBES_SUCHE';
		}
		
		if(($Param['SPEICHERN'] != '' and $Param['BES_SUCHE'] == 1) or $Sachbearbeiter == false)
		{
			$Form->ZeileStart('display:block','DB"ID="DB');
		}
		else
		{
			$Form->ZeileStart('display:none','DB"ID="DB');
		}	
			$Form->ZeileStart();
			$Form->SchreibeHTMLCode('<hr noshade width="600" size="1" align="left" color="black">');
			$Form->ZeileEnde();
		
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='') // Wenn keine Filiale
			{
				$Form->ZeileStart();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SCHADENJAHR'].':',200);
				$Form->Erstelle_TextFeld('*BES_SCHADENJAHR',($Param['SPEICHERN']=='on'?$Param['BES_SCHADENJAHR']:''), 4,150, true, '', '', '', 'T', 'L','',date('Y'),'','','','autocomplete=off');
				$Form->ZeileEnde();
				
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VON'] . ':', 200);
				$Form->Erstelle_TextFeld('*BES_DATUMVON',$Param['SPEICHERN'] == 'on' ? $Param['BES_DATUMVON']: '', 10, 120, true, '', '', '', 'D', 'L','',''.'','','','','autocomplete=off');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BIS'], 30);
				$Form->Erstelle_TextFeld('*BES_DATUMBIS', $Param['SPEICHERN'] == 'on' ? $Param['BES_DATUMBIS']: '', 10, 120, true, '', '', '', 'D', 'L','',''.'','','','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
				$Form->Erstelle_TextFeld('*BES_FIL_ID',($Param['SPEICHERN']=='on'?$Param['BES_FIL_ID']:''), 4,150, true, '', '', '', 'T', 'L','','',''.'','','','autocomplete=off');
				$Form->ZeileEnde();
			}
			else 
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
				$Form->Erstelle_TextFeld('BES_FIL_ID', $FilZugriff, 4,150, false, '', '', '', 'T', 'L','');
				$Form->Erstelle_HiddenFeld('BES_FIL_ID',$FilZugriff);
				$Form->ZeileEnde();
			}
				
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARBEITUNG'].':',200);
			$Form->Erstelle_TextFeld('*BES_BEARBEITUNG',($Param['SPEICHERN']=='on'?$Param['BES_BEARBEITUNG']:''),10,180,true,'','','','T','L','','',10,'','','autocomplete=off');
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORGANG'].':',200);
			$Daten = explode('|',$AWISSprachKonserven['BES']['lst_VORGANG']);
			$Form->Erstelle_SelectFeld('*BES_VORGANG',($Param['SPEICHERN']=='on'?$Param['BES_VORGANG']:''),150,true,'','','','','',$Daten);
			$Form->ZeileEnde();
			
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='') // Wenn GL
			{
				$SQL ='select ID,WERT from Antragart where id in (10,3,18,17) order by WERT';
			}
			else 
			{
				$SQL ='select ID,WERT from Antragart order by WERT';
			}
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AART'] . ':',200);
			$Form->Erstelle_SelectFeld('*BES_AART',$Param['SPEICHERN']=='on'?$Param['BES_AART']:'',120,true,$SQL,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
			$Form->ZeileEnde();
			
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='') // Wenn GL
			{
				$Form->ZeileStart();
				$Form->SchreibeHTMLCode('<hr noshade width="600" size="1" align="left" color="black">');
				$Form->ZeileEnde();		
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KUNDE'].':',200);
				$Form->Erstelle_TextFeld('*BES_KUNDE',($Param['SPEICHERN']=='on'?$Param['BES_KUNDE']:''),30,180,true,'','','','T','L','','',30,'','','autocomplete=off');
				$Form->ZeileEnde();	
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STRASSE'].':',200);
				$Form->Erstelle_TextFeld('*BES_STRASSE',($Param['SPEICHERN']=='on'?$Param['BES_STRASSE']:''),30,180,true,'','','','T','L','','',30,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_PLZ'].':',200);
				$Form->Erstelle_TextFeld('*BES_PLZ',($Param['SPEICHERN']=='on'?$Param['BES_PLZ']:''),6,180,true,'','','','T','L','','',6,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ORT'].':',200);
				$Form->Erstelle_TextFeld('*BES_ORT',($Param['SPEICHERN']=='on'?$Param['BES_ORT']:''),10,180,true,'','','','T','L','','',10,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON'].':',200);
				$Form->Erstelle_TextFeld('*BES_TELEFON',($Param['SPEICHERN']=='on'?$Param['BES_TELEFON']:''),10,180,true,'','','','T','L','','',10,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EMAIL'].':',200);
				$Form->Erstelle_TextFeld('*BES_EMAIL',($Param['SPEICHERN']=='on'?$Param['BES_EMAIL']:''),30,180,true,'','','','T','L','','',30,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KFZKZ'].':',200);
				$Form->Erstelle_TextFeld('*BES_KFZ','',15,0,true,'','','','T','L','','',15,'','f','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ATUCARD'].':',200);
				$Form->Erstelle_TextFeld('*BES_ATUCARD',($Param['SPEICHERN']=='on'?$Param['BES_ATUCARD']:''),30,180,true,'','','','T','L','','',30,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SACHBEARBEITER'].':',200);
				$Form->Erstelle_TextFeld('*BES_SACHBEARBEITER',($Param['SPEICHERN']=='on'?$Param['BES_SACHBEARBEITER']:''),30,180,true,'','','','T','L','','',30,'','','autocomplete=off');
				$Form->ZeileEnde();	
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VERSNR'].':',200);
				$Form->Erstelle_TextFeld('*BES_VERSNR',($Param['SPEICHERN']=='on'?$Param['BES_VERSNR']:''),30,180,true,'','','','T','L','','',25,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AZ'].':',200);
				$Form->Erstelle_TextFeld('*BES_AZ',($Param['SPEICHERN']=='on'?$Param['BES_AZ']:''),30,180,true,'','','','T','L','','',25,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_RSCHEINNR'].':',200);
				$Form->Erstelle_TextFeld('*BES_REKLANR',($Param['SPEICHERN']=='on'?$Param['BES_REKLANR']:''),30,180,true,'','','','T','L','','',25,'','','autocomplete=off');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GROSSKDNR'].':',200);
				$Form->Erstelle_TextFeld('*BES_GROSSKDNR',($Param['SPEICHERN']=='on'?$Param['BES_GROSSKDNR']:''),30,180,true,'','','','T','L','','',25,'','','autocomplete=off');
				$Form->ZeileEnde();
			}
		$Form->ZeileEnde();
		
		if(($Param['SPEICHERN'] != '' and $Param['BES_SUCHE'] == 2) and $Sachbearbeiter == true)
		{
			$Form->ZeileStart('display:block','AUF"ID="AUF');
		}
		else
		{
			$Form->ZeileStart('display:none','AUF"ID="AUF');
		}
		
	
	    $Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VERTEILER'].':',200);
		
		//Eigene Postf�cher
		$SQL  ='select SP_KEY,SBNAME from SACHBEARBEITERZUORD sbz ';
		$SQL .=' inner join Sachbearbeiter sb on sbz.sp_key = sb.id ';
		$SQL .=' where SB_KEY '.$DBSCHAD->LikeOderIst('0'.$rsSB->FeldInhalt('ID')) ;
		
		$rsSBZ = $DBSCHAD->RecordSetOeffnen($SQL);
		
		$DatenSB[] = '0~eigene';
		
		while(! $rsSBZ->EOF())
		{
			$DatenSB[] = $rsSBZ->FeldInhalt('SP_KEY').'~'.$rsSBZ->FeldInhalt('SBNAME');
			$rsSBZ->DSWeiter();
		}
		
		//Schauen ob ich jemanden Vertreten muss
		$VerSQL  =' Select * from Sachbearbeitervertreter ';
		$VerSQL .=' where SBV_GUELTIG_AB <= trunc(sysdate) ';
		$VerSQL .=' and (SBV_GUELTIG_BIS >= trunc(sysdate) or SBV_GUELTIG_BIS is null) ';
		$VerSQL .=' and SBV_VERTRETER_SB_ID ' . $DBSCHAD->LikeOderIst('0'.$rsSB->FeldInhalt('ID'));
		$rsVer = $DBSCHAD->RecordSetOeffnen($VerSQL);
		
		if($rsVer->AnzahlDatensaetze()>0) //Wenn Ja
		{    
		    //Dann hol die die Postf�cher von dem zu Vertretenen 
		    $SQL  ='select SP_KEY,SBNAME from SACHBEARBEITERZUORD sbz ';
		    $SQL .=' inner join Sachbearbeiter sb on sbz.sp_key = sb.id ';
		    $SQL .=' where SB_KEY=0'; 
	        while(!$rsVer->EOF())
	        {
	            $SQL .=' or SB_KEY '. $DBSCHAD->LikeOderIst('0'.$rsVer->FeldInhalt('SBV_ZUVERTRETEN_SB_ID')   );
	            
	            $rsVer->DSWeiter();
	        }
		    $rsSBZ = $DBSCHAD->RecordSetOeffnen($SQL);
		    
		    //Alle durchgehen f�r das ComboboxenArray
		    while(! $rsSBZ->EOF())
		    {
		        if (!in_array($rsSBZ->FeldInhalt('SP_KEY').'~'.$rsSBZ->FeldInhalt('SBNAME'), $DatenSB))
		        {
		            $DatenSB[] = $rsSBZ->FeldInhalt('SP_KEY').'~'.$rsSBZ->FeldInhalt('SBNAME') . '(Vertr.)';
		        }
		        
		        $rsSBZ->DSWeiter();
		    }
		    
		    $rsVer->DSErster();
		    
		    while(!$rsVer->EOF())
		    {
		        //Zu vertretene Sachbearbeiter auch holen und in das Comboboxenarray hinzuf�gen
		        $SQLSB = ' Select * from Sachbearbeiter where ID '. $DBSCHAD->LikeOderIst('0'.$rsVer->FeldInhalt('SBV_ZUVERTRETEN_SB_ID'));
		        $rsSBVER = $DBSCHAD->RecordSetOeffnen($SQLSB);
		        
		        $DatenSB[]= $rsVer->FeldInhalt('SBV_ZUVERTRETEN_SB_ID'). '~'.$rsSBVER->FeldInhalt('SBNAME') . '(Vertr.)';
		        
		        $rsVer->DSWeiter();
		    }		    
		}
		
		//Doppelte Werte rausschmei�en, 
		$DatenSB = array_unique($DatenSB);
		
		$Form->Erstelle_SelectFeld('*BES_AUFGABE',($Param['SPEICHERN']=='on'?$Param['BES_AUFGABE']:''),150,true,'','','','','',$DatenSB);
		$Form->ZeileEnde();
		
		$Form->ZeileEnde();
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/schaden_neu/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		if((!isset($Param['SPEICHERN']) or $Param['SPEICHERN'] == '') and $Sachbearbeiter)
		{
			$Form->Schaltflaeche('image', 'cmdSuche"style="display:none"', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		}
		else 
		{
			$Form->Schaltflaeche('image', 'cmdSuche', 'SUC', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		}
		if(($Recht40000&4)==4)
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}
		
		
		if((!isset($Param['SPEICHERN']) or $Param['SPEICHERN'] == '') and $Sachbearbeiter)
		{
			$Form->Schaltflaeche('image', 'cmdExportXLSX"style="display:none"', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
		}
		else 
		{
			if(($Param['BES_SUCHE'] == 0 or $Param['BES_SUCHE'] == 1))
			{
				$Form->Schaltflaeche('image', 'cmdExportXLSX"style="display:none"', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
			}
			else 
			{
				$Form->Schaltflaeche('image', 'cmdExportXLSX', '', '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
			}
		}
		
		
		$Form->SchaltflaechenEnde();

		$Form->SetzeCursor($AWISCursorPosition);
		$Form->SchreibeHTMLCode('</form>');
		
}
catch (Exception $ex)
{
    //echo $DBSCHAD->LetzterSQL();
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>