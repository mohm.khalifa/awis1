<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <?php
    $Version = 3;
    require_once 'airbag_funktionen.inc';
    $ABG = new airbag_funktionen();
    echo "<link rel=stylesheet type=text/css href=" . $ABG->AWISBenutzer->CSSDatei($Version) . ">";
    echo '<title>' . $ABG->AWISSprachKonserven['TITEL']['tit_Airbag'] . '</title>';
    ?>
</head>
<body>
<?php
if (isset($_POST['cmdPDF_x'])) {
    $_GET['XRE'] = 68;
    $_GET['ID'] = 'bla';
    require_once 'airbag_speichern.php';
    require_once '/daten/web/berichte/drucken.php';
}
include("awisHeader$Version.inc");    // Kopfzeile
try {
    $Register = new awisRegister(57000);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    $ABG->Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($ABG->Form instanceof awisFormular) {
        $ABG->Form->DebugAusgabe(1, $ex->getSQL());
        $ABG->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($ABG->Form instanceof awisFormular) {

        $ABG->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
</body>
</html>