<?php

require_once 'awisBerichte.inc';
require_once 'awisBarcode.inc';
require_once 'awisDatenbank.inc';

require_once("register.inc.php");
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

class ber_GrosskundenkartenRoh
    extends awisBerichte
{

    var $angle = 0;

    private $_TBL_GROSSKUNDENKARTEN = "";
    private $_TBL_GROSSKUNDEST = "";

    private $_AusgabeArt;

    private $_AWISWerkzeuge;

    public $Speicherort = '';

    public function init(array $Parameter)
    {
        $this->_AWISWerkzeuge = new awisWerkzeuge();

        if($this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK or $this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING){
            $this->_TBL_GROSSKUNDENKARTEN = " TESTGROSSKUNDENKARTEN ";
            $this->_TBL_GROSSKUNDEST = " TESTGROSSKUNDEST ";

            $this->_AusgabeArt = 1; //Anzeige im Browser -> nur zum Testen
        } else {
            $this->_TBL_GROSSKUNDENKARTEN = " EXPERIAN_KOM.EXPERIAN_GROSSKUNDENKARTEN@COM_DE.ATU.DE ";
            $this->_TBL_GROSSKUNDEST = " EXPGW.ATU_GROSSKUNDEST@COM_DE.ATU.DE ";

            $this->_AusgabeArt = 1; //Download der Datei
        }

        $EAN = '';
        if(isset($_REQUEST["txtKUNDEN"])){ //Dirket mit nem Request gekommen?
            if (is_array($_REQUEST["txtKUNDEN"])) {
                $KdNr = implode(',', $_REQUEST["txtKUNDEN"]);
            } else {
                $KdNr = $_REQUEST["txtKUNDEN"];
            }
        }elseif(isset($Parameter['KDNR'])){ //Oder Klasse aufgerufen
            $KdNr = $Parameter['KDNR'];
            $EAN = $Parameter['EAN'];
        }else{
            throw new Exception('Kann keine KDNR ermitteln', '201908261429');
        }

        //Daten laden
        $SQL = 'SELECT DISTINCT KND.EAN_NR, KND.KUNDEN_NR, KND.KARTEN_NR, KND.NAME1, KND.NAME2, KND.NAME3, KND.GUELTIGKEIT, ERZ_KEY, KND.EAN_NR, LAND.GKB_STAAT, LAND.ZAHL_ART FROM '.$this->_TBL_GROSSKUNDENKARTEN.' KND
                LEFT JOIN EXPRABATTZUORDNUNGEN ON ERZ_KUNDEN_NR = KND.KUNDEN_NR 
                LEFT JOIN '.$this->_TBL_GROSSKUNDEST.' LAND ON KND.KUNDEN_NR = LAND.KUNDEN_NR 
                WHERE KND.KUNDEN_NR IN (' . $KdNr . ') AND KND.GEDRUCKT = 1';
        if($EAN){
            $SQL .= ' AND EAN_NR = ' . $this->_DB->WertSetzen('KND','T',$EAN);
        }
        $SQL.= ' ORDER BY KND.EAN_NR ASC';


        $this->_rec = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('KND'));

        $TextKonserven[] = array('SYSTEM', 'PROGNAME');
        $TextKonserven[] = array('ERM', '*');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
        //$this->_Vorlage1 = '/daten/web/dokumente/vorlagen/' .'GrosskundenRoh_1.png'; //Nur bei Tests laden; Sonst: Wei�e Karte
        //$this->_Vorlage2 = '/daten/web/dokumente/vorlagen/' .'GrosskundenRoh_2.png'; //Nur bei Tests laden; Sonst: Wei�e Karte
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        $this->SetTitle('Grosskundenkarten');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);
        $Barcode = new awisBarcode($this);

        $this->AddFont('Arial', 'B', 'ariblk.php'); //Arial (Bold) mit Arial Black ersetzen (aufpassen: Zeichenabstand breiter als bei Arial)
        $this->AddFont('Arial Bold', '', 'arialb.php'); //Arial Bold neu erstellen, weil es bei manchen Texten besser aussieht

        while (!$this->_rec->EOF()) {
            $Typ = '';
            if($this->_rec->FeldInhalt('NAME3') == 'Mana Ara'){ // Mana Ara Kunde
                $Typ = 'MA';
            } else if($this->_rec->FeldInhalt('ZAHL_ART') == 'G'){ // Zahlart G = Barzahler
                $Typ = 'B';
            } else { // Zahlart K = Unbar
                $Typ = 'U';
            }
            $SIXT = false;
            if($this->_rec->FeldInhalt('KUNDEN_NR') == 450627){
                $SIXT = true;
            }

            $this->NeueSeite();
            $this->SetCompression(false);
            $this->_Zeile = $this->_Parameter['SeiteOben'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetTextColor(0, 0, 0);

            //$this->Image($this->_Vorlage1,0,0,-300,-300); //Nur bei Tests laden; Sonst: Wei�e Karte

            //�berschrift
            $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde']-1, $this->_Zeile+1);
            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseUeberschrift']);
            $this->Cell($this->_Parameter['FeldBreiteUeberschrift'], 0, $Typ == "MA"?"Mana-Ara":'Gro�kunden-Ausweis', 0, 0, 'L');

            //Daten
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseDaten']);
            if($Typ == "MA" and strlen(trim($this->_rec->FeldInhalt('NAME1') . " " . $this->_rec->FeldInhalt('NAME2'))) <= 40) { //Falls MA und Name < 40 Zeichen dann in eine Zeile
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde']-1, $this->y + $this->_Parameter['ZeilenAbstandUeberschrift']+1);
                $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME1') . " " . $this->_rec->FeldInhalt('NAME2'), 0, 0, 'L');
            } else { //Sonst immer 2/3 Zeilen
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde']-1, $this->y + $this->_Parameter['ZeilenAbstandUeberschrift']+1);
                $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME1'), 0, 0, 'L');
                if($this->_rec->FeldInhalt('NAME2') != "") {
                    $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'] - 1, $this->y + $this->_Parameter['ZeilenAbstandDaten']);
                    $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME2'), 0, 0, 'L');
                }
                if ($this->_rec->FeldInhalt('NAME3') != "" and $Typ != "MA") {
                    $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'] - 1, $this->y + $this->_Parameter['ZeilenAbstandDaten']);
                    $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME3'), 0, 0, 'L');
                }
            }

            //Kundennummer + G�ltigkeit
            //Bei Mana-Ara soll keine G�ltigkeit angzeigt werden
            if($Typ!="MA") {
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde']-1, $this->_Parameter['ZeilenAbstandKundennummer']+2);
            } else {
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde']-1, $this->_Parameter['ZeilenAbstandKundennummer']+5);
            }
            $this->SetFont($this->_Parameter['Schriftart'], '', 7.5);
            $this->Cell($this->_Parameter['FeldBreiteKdNr'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_KD_NR'] . " ". $this->_rec->FeldInhalt('KUNDEN_NR'), 0, 0, 'L');

            if($Typ!="MA") {
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'] - 1, $this->_Parameter['ZeilenAbstandKundennummer'] + 5);
                $gueltigkeit = substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 0, 2) . '/' . substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 2, 2);
                $this->Cell($this->_Parameter['FeldBreiteGueltigBis'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_GUELTIG_BIS'] . " " . $gueltigkeit, 0, 0, 'L');
            }

            //Ecke unten rechts: Barcode
            $this->SetXY(48, 36.5);
            $this->SetFont($this->_Parameter['Schriftart'], '', 7.5);
            $this->Cell($this->_Parameter['FeldBreiteKdNr'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_KARTENNUMMER'], 0, 0, 'L');

            $Barcode->EAN13(49, 38, substr($this->_rec->FeldInhalt('EAN_NR'),0,12), $this->_Parameter['BarcodeKarteH'], $this->_Parameter['BarcodeKarteW'],false);
            $this->SetXY(53.25,50.25);
            $this->SetFont($this->_Parameter['Schriftart'], '', 7.5); //TextGroesseDaten
            $this->Cell($this->_Parameter['EAN_NrW'],$this->_Parameter['EAN_NrH'],$this->_rec->FeldInhalt('EAN_NR'),0,0,'C');

            //-------------------------------------------------
            //R�ckseite
            $this->NeueSeite();
            $this->SetCompression(false);
            $this->_Zeile = $this->_Parameter['SeiteObenRueckseite'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            //$this->Image($this->_Vorlage2,0,0,-300,-300); //Nur bei Tests laden; Sonst: Wei�e Karte

            $this->SetFillColor(0,0,0);

            $this->SetFont($this->_Parameter['Schriftart'], '', 5.5); //TextGroesseDaten
            $this->SetXY($this->_Parameter['LinkerRand']-1,$this->_Parameter['DisclaimerY']+3);
            $this->MultiCell($this->_Parameter['DisclaimerW']-30,$this->_Parameter['DisclaimerH']+0.5,$this->_AWISSprachkonserven['ERM']['ERM_PDF_DISCLAIMER_KURZ'],0,'J');


            //Barcode EAN herausfinden
            if($Typ == "MA") {
                //30% : 999 999 000 8107
                $ProzentEAN = "9999990008107";
            } elseif($Typ == "B") {
                if($SIXT) { //Bei SIXT anderer EAN: 999 999 003 5738
                    $ProzentEAN = "9999990035738";
                } else { //10% : 999 999 000 5755
                    $ProzentEAN = "9999990005755";
                }
            }
            //Barcode in der Mitte (Nur Mana-Ara und Bar)
            if(isset($ProzentEAN) and $ProzentEAN != "") {
                $Barcode->EAN13(26.6, 18, substr($ProzentEAN,0,12), $this->_Parameter['BarcodeKarteH'], $this->_Parameter['BarcodeKarteW'],false);
                $this->SetFont($this->_Parameter['Schriftart'], '', 7.5); //TextGroesseDaten
                $this->SetXY(30.5,30.25);
                $this->Cell($this->_Parameter['EAN_NrW'],$this->_Parameter['EAN_NrH'],$ProzentEAN,0,0,'C');

                //Rabattcode als Wort dahinter
                $this->SetXY(53.6,22.95);
                $this->SetFont($this->_Parameter['Schriftart'], '', 6.5);
                $this->Cell($this->_Parameter['EAN_NrW'],$this->_Parameter['EAN_NrH'],$this->_AWISSprachkonserven['ERM']['ERM_PDF_RABATTCODE'],0,0,'C');
            }

            //Anschrift Unten Links
            $this->SetFont('Arial Bold', '', 6.5); //TextGroesseDaten
            $this->SetXY($this->_Parameter['LinkerRand']-1,34);
            $this->Cell($this->_Parameter['AnschriftW'], $this->_Parameter['AnschriftH']+0.5, $this->_AWISSprachkonserven['ERM']['ERM_PDF_ANSCHRIFT_1'], 0, 0, 'L');
            $this->SetFont($this->_Parameter['Schriftart'], '', 5.5); //TextGroesseDaten
            $this->SetXY($this->_Parameter['LinkerRand']-1,37);

            if($Typ == "MA"){
                $Text = $this->_AWISSprachkonserven['ERM']['ERM_PDF_ANSCHRIFT_2_MA'];
            } elseif($this->_rec->FeldInhalt('GKB_STAAT') == 4 ) { //Deutschland -> GKB_STAAT = 4
                $Text = $this->_AWISSprachkonserven['ERM']['ERM_PDF_ANSCHRIFT_2'];
            } else { //�sterreich -> GKB_STAAT = 38 + Alles andere, da bei fehlendem Wert wir auf Nummer sicher gehen
                $Text = $this->_AWISSprachkonserven['ERM']['ERM_PDF_ANSCHRIFT_2_AT'];
            }

            $this->MultiCell($this->_Parameter['AnschriftW'],$this->_Parameter['AnschriftH']+0.5,$Text,0,'L');
            $this->SetXY($this->_Parameter['LinkerRand']-1,42.35);
            if($Typ == "MA") {
                $this->Cell($this->_Parameter['AnschriftW'], $this->_Parameter['AnschriftH']+0.5, $this->_AWISSprachkonserven['ERM']['ERM_PDF_WEB_MANAARA'], 0, 0, 'L');
            } else {
                $this->Cell($this->_Parameter['AnschriftW'], $this->_Parameter['AnschriftH']+0.5, $this->_AWISSprachkonserven['ERM']['ERM_PDF_WEB_FLOTTE'], 0, 0, 'L');
            }

            //Warnung Unten Rechts
            $this->SetFont($this->_Parameter['Schriftart'],'' , 5.5); //TextGroesseDaten
            $this->SetXY($this->_Parameter['LinkerRand']+56.5,47);
            $this->Cell($this->_Parameter['AnschriftW'], $this->_Parameter['AnschriftH'], $this->_AWISSprachkonserven['ERM']['ERM_PDF_WARNUNG_1'], 0, 0, 'R');
            $this->SetFont($this->_Parameter['Schriftart'], '', 5.5); //TextGroesseDaten
            $this->SetXY($this->_Parameter['LinkerRand']+56.5,49);
            $this->Cell($this->_Parameter['AnschriftW'], $this->_Parameter['AnschriftH'], $this->_AWISSprachkonserven['ERM']['ERM_PDF_WARNUNG_2'], 0, 0, 'R');



            $this->_rec->DSWeiter();
        }

        //*********************************************************************
        // Berichtsende -> Zur�ckliefern
        //*********************************************************************

        if($this->Speicherort != '') {
            $this->Output($this->Speicherort, 'F');
        } else {

            switch ($this->_AusgabeArt) {
                case 1:            // PDF im extra Fenster -> Download
                    $this->Output($this->_BerichtsName . '.pdf', 'D');
                    break;
                case 2:            // Standard-Stream
                    $this->Output($this->_BerichtsName . '.pdf', 'I');
                    break;
                case 3:            // Als String zur�ckliefern
                    return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                    break;
                case 4:                // Als Datei speichern
                    $this->Output($this->_Dateiname, 'F');
                    break;
            }
        }
    }

    public function setSpeicherort($Speicherort = ''){
        if($Speicherort !=''){
            $this->Speicherort = $Speicherort;
        }
    }

    private function NeueSeite()
    {
        $this->AddPage('L', [85.60, 53.98]);
    }

    function _endpage()
    {
        if ($this->angle != 0) {
            $this->angle = 0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

}