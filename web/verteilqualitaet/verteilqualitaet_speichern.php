<?php

global $AWIS_KEY1;
global $AWIS_KEY2;
global $Key;


$TextKonserven=array();
$TextKonserven[]=array('WEB','*');
$TextKonserven[]=array('WBE','*');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','lbl_drucken');
$TextKonserven[]=array('Wort','lbl_Hilfe');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNein');

try
{
   $Form = new awisFormular();
   $DB = awisDatenbank::NeueVerbindung('AWIS');
   $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
   $AWISBenutzer = awisBenutzer::Init();

   $flagSpeichereBetragsKuerzung = false;
   $flag=false;
   $abgleichsGrund = "";

   $Form->DebugAusgabe(1,$_POST);
	

   if(!isset($_POST['txtEdit']))
   {
	if(isset($_POST['txtWEB_KEY']) && $_POST['txtWEB_KEY'] == 0)
	{
	
	  $Felder = $Form->NameInArray($_POST, 'txtWEB_',1,1);
	  
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('WEB','WEB_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('WEB_WERBEMEDIUM');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['WEB'][$Pflichtfeld].'<br>';
				}
			}

			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
		}
		
			
			$SQL  = "Select WEB_KEY,WEB_WERBEMEDIUM FROM WERBEMEDIUM WHERE WEB_WERBEMEDIUM=".$DB->FeldInhaltFormat('T',$_POST['txtWEB_WERBEMEDIUM']);
			
			$rsDuplikat = $DB->RecordSetOeffnen($SQL);
 
			if($rsDuplikat->AnzahlDatensaetze()==0)
			{
			
			$SQL = "INSERT INTO WERBEMEDIUM (WEB_WERBEMEDIUM,WEB_USER,WEB_USERDAT) VALUES(";
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtWEB_WERBEMEDIUM'],false);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			
			
			if($DB->Ausfuehren($SQL)===false)
			{
				die();
			}
			
			
			$SQL = "Select max(WEB_KEY) AS WEB_KEY FROM WERBEMEDIUM";
			
			$rsWEBKEY = $DB->RecordSetOeffnen($SQL);
			
			if(isset($_POST['sucFilialenDeutschAlle']) == 'on')
			{
				$SQL = " Select DISTINCT FIL_ID from filialen";
				$SQL .= " inner join v_filialpfad on xx1_fil_id = fil_id";
				$SQL .= " where fil_lan_wwskenn = 'BRD'";
				$SQL .= " order by fil_id";
				
				$rsFilialen = $DB->RecordSetOeffnen($SQL);
				
				while(!$rsFilialen->EOF())
				{
				  $SQL = "INSERT INTO WERBEMITTELZUORDNUNG (WBE_FILIALEN, WBE_WEB_KEY, WBE_DATVON, WBE_DATBIS, WBE_USER, WBE_USERDAT) VALUES (";
				  $SQL .= ' ' . $DB->FeldInhaltFormat('T',$rsFilialen->FeldInhalt('FIL_ID'),false);
			       	  $SQL .= ', ' . $DB->FeldInhaltFormat('NO',$rsWEBKEY->FeldInhalt('WEB_KEY'),false);
				  $SQL .= ', ' . $DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATVON'],false);
				  $SQL .= ', ' . $DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATBIS'],false);
				  $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				  $SQL .= ',SYSDATE';
				  $SQL .= ')';
				
				  if($DB->Ausfuehren($SQL)===false)
				  {
				    die();
				  }
				  
				  $rsFilialen->DSWeiter();
				}
			
			}
			else
			{
			   $WBEFilialen = explode(",",$_POST['txtWBEFILIALEN']);
			
				for($i=0;$i<count($WBEFilialen);$i++)
				{
					$SQL = "INSERT INTO WERBEMITTELZUORDNUNG (WBE_FILIALEN, WBE_WEB_KEY, WBE_DATVON, WBE_DATBIS, WBE_USER, WBE_USERDAT) VALUES (";
					$SQL .= ' ' . $DB->FeldInhaltFormat('T',$WBEFilialen[$i],false);
					$SQL .= ', ' . $DB->FeldInhaltFormat('NO',$rsWEBKEY->FeldInhalt('WEB_KEY'),false);
					$SQL .= ', ' . $DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATVON'],false);
					$SQL .= ', ' . $DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATBIS'],false);
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';
					
					if($DB->Ausfuehren($SQL)===false)
					{
						die();
					}
				}
			}
			
			$AWIS_KEY1 = $rsWEBKEY->FeldInhalt('WEB_KEY');
			
			}
			else
			{
			   $Fehler = ' Werbemedium: ' .$_POST['txtWEB_WERBEMEDIUM'] .' wurde bereits anlegt';
			   
			   if($Fehler!='')
			   {
				echo('<span class=HinweisText>'.$Fehler.'</span>');
			   }

			   $AWIS_KEY1 = $rsDuplikat->FeldInhalt('WEB_KEY'); 	
			}
			
			
	}
	elseif(isset($_POST['txtWEB_KEY']) && isset($_POST['txtWEB_KEY']) != '') 
	{
	
	
	   $Felder = $Form->NameInArray($_POST, 'txtWEB_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('WEB','WEB_%');
			$TextKonserven[]=array('WBE','WBE_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('WEB_KEY','WBEFILIALEN','WBE_DATVON','WBE_DATBIS');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['WEB'][$Pflichtfeld].'<br>';
				}
			}

			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
		}
		
			$WBEFilialen = explode(",",$_POST['txtWBEFILIALEN']);
			
			for($i=0;$i<count($WBEFilialen);$i++)
			{
			   $SQL = "Select WBE_FILIALEN FROM WERBEMITTELZUORDNUNG WHERE WBE_FILIALEN=".$DB->FeldInhaltFormat('T',$WBEFilialen[$i]); 
			   $SQL .= " AND WBE_WEB_KEY=".$DB->FeldInhaltFormat('NO',$_POST['txtWEB_KEY']);

			   $rsDuplikat = $DB->RecordSetOeffnen($SQL);	
			   
			   if($rsDuplikat->AnzahlDatensaetze() == 0)
			   {
				   $SQL = "INSERT INTO WERBEMITTELZUORDNUNG (WBE_FILIALEN, WBE_WEB_KEY, WBE_DATVON, WBE_DATBIS, WBE_USER, WBE_USERDAT) VALUES (";
				   $SQL .= ' ' . $DB->FeldInhaltFormat('T',$WBEFilialen[$i],false);
				   $SQL .= ', ' . $DB->FeldInhaltFormat('NO',$_POST['txtWEB_KEY'],false);
				   $SQL .= ', ' . $DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATVON'],false);
				   $SQL .= ', ' . $DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATBIS'],false);
				   $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				   $SQL .= ',SYSDATE';
				   $SQL .= ')';
					
				   if($DB->Ausfuehren($SQL)===false)
				   {
					die();
				   }
			   }
			   else
			   {
				$Fehler = 'Filiale :'.$rsDuplikat->FeldInhalt('WBE_FILIALEN').' wird bereits in diesen Werbemedium verwendet';
			   
				if($Fehler!='')
				{
					echo('<span class=HinweisText>'.$Fehler.'</span>');
				}
			    }
			
			}
			
			$AWIS_KEY1 = $_POST['txtWEB_KEY'];
	}
     }
     else
     {
	$SQL = "UPDATE WERBEMITTELZUORDNUNG SET WBE_DATVON=".$DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATVON']); 
	$SQL .= " ,WBE_DATBIS=".$DB->FeldInhaltFormat('DU',$_POST['txtWBE_DATBIS']);
	$SQL .= " WHERE WBE_KEY=".$DB->FeldInhaltFormat('NO',$_POST['txtWBE_KEY']);
	
	if($DB->Ausfuehren($SQL)===false)
	{
		die();
	}
	
	$AWIS_KEY1 = $_POST['txtWEB_KEY'];
	
	
	
     }
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SYSTcherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

?>