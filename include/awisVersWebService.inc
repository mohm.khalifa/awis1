<?php

class awisVersWebService
{

    private $_DebugLevel;
    private $_curl = null;
    private $Werkzeug;
    private $_AWISBenutzer;
    private $_EMAIL_Infos = array('shuttle@de.atu.eu');


    public function __construct($DebugLevel = 0, $Benutzer)
    {
        // DB, etc. initialisieren
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_DebugLevel = $DebugLevel;
        $this->Werkzeug = new awisWerkzeuge();
    }

    public function __destruct()
    {
        if($this->_curl)
        {
            curl_close($this->_curl);
        }
    }

    public function WebserviceHandling($SQL, $updateCallback, $WebserviceURL)
    {


        $RecordSet = $this->_DB->RecordSetOeffnen($SQL);

        while(!$RecordSet->EOF())
        {

            try
            {
                $url = rtrim($WebserviceURL, '/') . '/' . $RecordSet->FeldInhalt('VKE_KEY');
                //setzt die Webservice-URL
                curl_setopt($this->_curl, CURLOPT_URL, $url);
                curl_setopt($this->_curl, CURLOPT_TIMEOUT, 120);

                $response = curl_exec($this->_curl);

                //curl_errno = Abfrage ob ein Verbindungsfehler aufgetreten ist
                if(curl_errno($this->_curl) !== CURLE_OK)
                {
                    //wartet x-Sekunden und versucht dann noch einmal eine Verbindung herzustellen
                    sleep(5);

                    $response = curl_exec($this->_curl);

                    if(curl_errno($this->_curl) !== CURLE_OK)
                    {
                        //sollte nach dem 2. Versuch noch immer keine Verbindung m�glich sein, dann breche ab.
                        echo curl_error($this->_curl);
                        echo $url;
                        throw new Exception(curl_error($this->_curl));
                    }
                }
                $info = curl_getinfo($this->_curl, CURLINFO_HTTP_CODE);

                if($info == 200)
                {
                    call_user_func_array($updateCallback, array($RecordSet->FeldInhalt('VKE_KEY'), $info));
                }
                else
                {
                    $decoded = json_decode($response);

                    if($decoded)
                    {
                        $error_message = "Bei dem Datensatz, mit dem KEY: " . $RecordSet->FeldInhalt('VKE_KEY');
                        $error_message .= " ist ein Fehler aufgetreten: " . PHP_EOL;
                        $error_message .= " HTTP_CODE: " . $info . PHP_EOL;
                        $error_message .= " ERROR-DETAIL: " . $decoded->errors->detail . PHP_EOL;
                        $error_message .= " URL: " . $url .PHP_EOL;
                        $error_message .= "\n";
                        $error_message .= " Dieser wurde in der AWIS-Tabelle VERSKOPFDATENEXPORT, ";

                        if($WebserviceURL == $this->_AWISBenutzer->ParameterLesen('URL_WEBSERVICE_DAMAGEREDUCTION_SMARTREPAIR')) {
                            $error_message .= " mit dem Status 11 gekennzeichnet." . PHP_EOL;
                            $error_message .= " Nach der Korrektur den VKE_VWS_KEY bitte wieder auf 0 setzen." . PHP_EOL;
                        }
                        elseif($WebserviceURL == $this->_AWISBenutzer->ParameterLesen('URL_WEBSERVICE_PROCESSINSURANCECASE')) {
                            $error_message .= " mit dem VKE_VWS_KEY 12 gekennzeichnet." . PHP_EOL;
                            $error_message .= " Nach der Korrektur den VKE_VWS_KEY bitte wieder auf 1 setzen." . PHP_EOL;
                        }
                        call_user_func_array($updateCallback, array($RecordSet->FeldInhalt('VKE_KEY'), $info));
                        throw new ErrorException($error_message);
                    }
                    else
                    {
                        // Fehlerbehandlung bei Katastrophe...
                        throw new ErrorException('Unerwartete R�ckgabe, HTTP Status:' . $info);
                    }
                }
            }
            catch(ErrorException $ex)
            {
                echo $ex->getMessage();
                $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AWISVERSWEBSERVICE', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');
            }
            $RecordSet->DSWeiter();
        }
    }

    public function Webservice_damagereduction()
    {
        try
        {
            $URL = '';
            $Methode = '';
            $AufrufWS = false;
            $SQLWS = '';

            $SQL = ' SELECT DISTINCT VKE_SCHADEN_KZ ';
            $SQL .= ' FROM VERSKOPFDATENEXPORT';

            $rsSchadkz = $this->_DB->RecordSetOeffnen($SQL);

            while(!$rsSchadkz->EOF())
            {
                $schadkz = $rsSchadkz->FeldInhalt('VKE_SCHADEN_KZ');

                if($schadkz == 'P')
                {
                    $AufrufWS = true;
                    //$URL = $this->_URL_WEBSERVICE_DAMAGEREDUCTION_SMARTREPAIR;
                    $URL = $this->_AWISBenutzer->ParameterLesen('URL_WEBSERVICE_DAMAGEREDUCTION_SMARTREPAIR');
                    $Methode = 'GET';
                }
                else
                {
                    $SQL = 'UPDATE VERSKOPFDATENEXPORT ';
                    $SQL .= ' SET VKE_VWS_KEY = 1, ';
                    $SQL .= ' VKE_ZEITSTEMPEL_WEBSERVICE = sysdate ';
                    $SQL .= ' WHERE VKE_SCHADEN_KZ = \'' . $schadkz . '\'';
                    $SQL .= ' AND VKE_VWS_KEY = 0';

                    $this->_DB->Ausfuehren($SQL, '', true);
                }

                if($AufrufWS)
                {
                    $SQLWS = 'SELECT *';
                    $SQLWS .= ' FROM VERSKOPFDATENEXPORT';
                    $SQLWS .= ' WHERE VKE_SCHADEN_KZ = \'' . $schadkz . '\'';
                    $SQLWS .= ' AND VKE_VWS_KEY = 0';

                    //ruft die Methode auf, die die Curl-Instanz erzeugt
                    //�bergabe-Parameter ist die verwendete Methode
                    $this->ErzeugeCurlInstanz($Methode);
                    //ruft die Methode auf die auf die Response abfraegt
                    //und entscheidet, was im Gutfall bzw Fehlerfall gemacht werden muss
                    $this->WebserviceHandling($SQLWS, array($this, 'damageReductionUpdater'), $URL);
                }

                $rsSchadkz->DSWeiter();
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisVersWebService: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AWISVERSWEBSERVICE', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            throw $ex;
        }
    }

    protected function damageReductionUpdater($key, $code)
    {
        // Update sql erstellen und ausf�hren
        $SQLupdate = 'UPDATE VERSKOPFDATENEXPORT ';
        $SQLupdate .= ' SET VKE_VWS_KEY = ' . ($code == 200?1:11) . ', VKE_ZEITSTEMPEL_WEBSERVICE = sysdate ';
        $SQLupdate .= ' WHERE VKE_KEY = ' . $key;

        $this->_DB->Ausfuehren($SQLupdate, '', true);
    }


    public function Webservice_processInsuranceCase()
    {
        try
        {
            $SQL = 'SELECT VKE_KEY ';
            $SQL .= ' FROM V_VERSKOPFDATENEXPORT';

            $this->_URL = $this->_AWISBenutzer->ParameterLesen('URL_WEBSERVICE_PROCESSINSURANCECASE');
            //ruft die Methode auf, die die Curl-Instanz erzeugt
            //�bergabe-Parameter ist die verwendete Methode
            //in dem Fall "PATCH"
            $this->ErzeugeCurlInstanz('PATCH');

            //ruft die Methode auf die auf die Response abfraegt
            //und entscheidet, was im Gutfall bzw Fehlerfall gemacht werden muss
            $this->WebserviceHandling($SQL, array($this, 'processInsuranceCaseUpdater'), $this->_URL);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisVersWebService: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AWISVERSWEBSERVICE', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            throw $ex;
        }
    }

    protected function processInsuranceCaseUpdater($key, $code)
    {
        // Update sql erstellen und ausf�hren
        $SQLupdate = 'UPDATE VERSKOPFDATENEXPORT ';
        $SQLupdate .= ' SET VKE_VWS_KEY = ' . ($code == 200?2:12) . ', VKE_ZEITSTEMPEL_WEBSERVICE = sysdate ';
        $SQLupdate .= ' WHERE VKE_KEY = ' . $key;

        $this->_DB->Ausfuehren($SQLupdate, '', true);

        $SQL = 'UPDATE VERSVORGANGSSTATUS';
        $SQL .= ' SET vvs_vcn_key        = 129,';
        $SQL .= '   VVS_DATUMGDVABGELEGT = sysdate';
        $SQL .= ' WHERE VVS_KEY         IN';
        $SQL .= '   (SELECT VVS_KEY';
        $SQL .= '   FROM VERSVORGANGSSTATUS VVS';
        $SQL .= '   INNER JOIN VERSKOPFDATENEXPORT VKE';
        $SQL .= '   ON VKE.VKE_FILID      = VVS.VVS_FILID';
        $SQL .= '   AND VKE.VKE_WANR      = VVS.VVS_WANR';
        $SQL .= '   WHERE VKE.VKE_KEY = ' . $key;
        $SQL .= '   )';

        $this->_DB->Ausfuehren($SQL, '', true);
    }


    public function ErzeugeCurlInstanz($Methode)
    {
        if(!$this->_curl)
        {
            $this->_curl = curl_init();
        }

        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->_curl, CURLOPT_CUSTOMREQUEST, $Methode);
        $headers = array('Content-Type: application/json');
        curl_setopt($this->_curl, CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * Echo't einen Text wenn das Level passt
     *
     * @param string $Text  der Ausgegeben werden soll
     * @param number $Level Level welches Ben�tigt wird
     */
    public function debugAusgabe($Text = '', $Level = 500)
    {
        if($this->_DebugLevel >= $Level)
        {
            echo date('d.m.y H:i:s ') . $Text . PHP_EOL;
        }
    }
}

?>
