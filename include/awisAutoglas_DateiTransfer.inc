<?php

class awisAutoglas_DateiTransfer
{

    private $_DebugLevel;
    private $_AWISBenutzer;
    private $_FTP_USER = "FirstGlassFTP";
    private $_FTP_SERVER = "svftpwi001.ads.atu.de";
    private $_FTP_PWD = "dpU9!DXN";
    private $_FTP_PORT = 21;

    private $_PFAD_FTP_AUFTRAGSDAT = "/k/AppData/FirstGlass-FTP/auftragsdateien/";
    private $_PFAD_FTP_AUFTRAGSDAT_TEST = "/k/AppData/FirstGlass-FTP/auftragsdateien_test/";
    private $_PFAD_FTP_GLASSTAMM = "/k/AppData/FirstGlass-FTP/artikelabgleich/";
    private $_PFAD_FTP_GLASSTAMM_TEST = "/k/AppData/FirstGlass-FTP/artikelabgleich_test/";
    private $_PFAD_FTP_KASSENDAT = "/k/AppData/FirstGlass-FTP/kassendaten/";
    private $_PFAD_FTP_KASSENDAT_TEST = "/k/AppData/FirstGlass-FTP/kassendaten_test/";
    private $_PFAD_FTP_KASSENDATSTORNO = "/k/AppData/FirstGlass-FTP/kassendaten_stornierungen/";
    private $_PFAD_FTP_KASSENDATSTORNO_TEST = "/k/AppData/FirstGlass-FTP/kassendaten_stornierungen_test/";
    private $_PFAD_FTP_VERSEXPORT_ABGLAGE = "/k/AppData/FirstGlass-FTP/versicherungen/";
    private $_PFAD_FTP_VERSEXPORT_ABGLAGE_TEST = "/k/AppData/FirstGlass-FTP/versicherungen_test/";
    private $_PFAD_FTP_VERSEXPORT_ABHOLUNG = "/k/AppData/FirstGlass-FTP/versicherungen/gdv/";
    private $_PFAD_FTP_VERSEXPORT_ABHOLUNG_TEST = "/k/AppData/FirstGlass-FTP/versicherungen_test/gdv/";

    //bestehende Pfade
    private $_PFAD_INT_AUFTRAGSDAT = "/daten/daten/versicherungen/firstglas/Auftrag/";
    private $_PFAD_INT_GLASSTAMM = "/daten/daten/teams/AWIS-Transfer/01-Autoglas/Glasstamm/";
    private $_PFAD_INT_GLASSTAMM_TEST = "/daten/daten/versicherungen/firstglas/glasstamm/";
    private $_PFAD_INT_KASSENDAT = "/daten/daten/versicherungen/firstglas/FirstglasExport/";
    private $_PFAD_INT_KASSENDAT_BACKUP = "/daten/daten/versicherungen/firstglas/FirstglasExport/bereits_verschoben/";
    private $_PFAD_INT_KASSENDATSTORNO = "/daten/daten/versicherungen/firstglas/Export/Storno/";
    private $_PFAD_INT_KASSENDATSTORNO_BACKUP = "/daten/daten/versicherungen/firstglas/Export/Storno/bereits_verschoben/";
    private $_PFAD_INT_VERSEXPORT = "/daten/daten/versicherungen/export/";
    private $_PFAD_INT_VERSEXPORT_BACKUP = "/daten/daten/versicherungen/archiv/export_dateien/";
    private $_PFAD_INT_VERSRUECKMELDEDAT = "/daten/daten/versicherungen/rueckmeldedateien/";
    private $_PFAD_INT_VERSRUECKMELDEDAT_MAN_ZUPRUEFEN = "/daten/daten/versicherungen/rueckmeldedateien/manuell_pruefen/zu_pruefen/";
    private $_PFAD_INT_VERSRUECKMELDEDAT_MAN_GEPRUEFT = "/daten/daten/versicherungen/rueckmeldedateien/manuell_pruefen/geprueft/";
    private $_PFAD_INT_ZIPARCHIV = "/daten/daten/FileAblage/PCDatenAblage/01-Versicherungsdaten/Onlineversicherungen/Archiv/ZIP_NEU/";
    private $_PFAD_INT_ZIPARCHIV_TEST = "/daten/daten/FileAblage/PCDatenAblage/01-Versicherungsdaten/Onlineversicherungen/Archiv/test/ZIP_NEU/";
    private $_PFAD_INT_HUKCLEARING = "/daten/daten/teams/AWIS-Transfer/01-Autoglas/HUK-Clearing/zu_bearbeiten/";
    private $_PFAD_INT_HUKCLEARING_TEST = "/daten/daten/teams/AWIS-Transfer/01-Autoglas/test/HUK-Clearing/zu_bearbeiten/";
    private $_PFAD_INT_HUKCLEARING_GEPRUEFT = "/daten/daten/teams/AWIS-Transfer/01-Autoglas/HUK-Clearing/geprueft/";
    private $_PFAD_INT_HUKCLEARING_GEPRUEFT_TEST = "/daten/daten/teams/AWIS-Transfer/01-Autoglas/test/HUK-Clearing/geprueft/";
    private $_PFAD_INT_HUKCLEARING_GEPRUEFT_BACKUP = "/daten/daten/teams/AWIS-Transfer/01-Autoglas/HUK-Clearing/Backup/";
    private $_PFAD_INT_HUKCLEARING_GEPRUEFT_BACKUP_TEST = "/daten/daten/teams/AWIS-Transfer/01-Autoglas/test/HUK-Clearing/Backup/";

    private $_conn_id = '';
    private $_EMAIL_Infos = array('shuttle@de.atu.eu');
    private $Werkzeug;

    public function __construct($DebugLevel = 0, $Benutzer)
    {
        // DB, etc. initialisieren
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_DebugLevel = $DebugLevel;
        $this->Werkzeug = new awisWerkzeuge();
        $this->_setzePfade();
    }

    public function __destruct()
    {
        if($this->_conn_id != '')
        {
            ftp_close($this->AutoglasFTPConnect());
            $this->_conn_id = '';
        }
    }

    private function _setzePfade()
    {
        try
        {

            if($this->Werkzeug->awisLevel() != awisWerkzeuge::AWIS_LEVEL_PRODUKTIV and $this->Werkzeug->awisLevel() != awisWerkzeuge::AWIS_LEVEL_SHUTTLE)
            {
                $Klassenvariablen = array_keys(get_class_vars("awisAutoglas_DateiTransfer"));

                foreach($Klassenvariablen as $Klassenvariable)
                {
                    if((strpos($Klassenvariable, "PFAD_") !== false) and (strpos($Klassenvariable, "_TEST") === false))
                    {
                        if(in_array($Klassenvariable . '_TEST', $Klassenvariablen))
                        {
                            $Testvar = '$this->' . $Klassenvariable . '_TEST';
                            $Evalstring = '$this->' . $Klassenvariable . ' = ' . $Testvar . ";";

                            echo "Evalstring: $Evalstring" . PHP_EOL;

                            if(eval($Evalstring) === false)
                            {
                                $err = "Evalstring: $Evalstring ist tot" . PHP_EOL;
                                throw new Exception($err);
                            }
                        }
                        else
                        {
                            $this->debugAusgabe($Testvar . " ist nicht gesetzt", 500);
                        }
                    }
                    else
                    {
                        $this->debugAusgabe("Andere Variable: " . $Klassenvariable, 500);
                    }
                }
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }

    public function Auftragsdaten_abholen()
    {
        try
        {
            if(!ftp_chdir($this->AutoglasFTPConnect(), $this->_PFAD_FTP_AUFTRAGSDAT))
            {
                $Err = date('d.m.Y H:i:s') . " Verzeichniswechsel ist fehlgeschlagen";
                throw new Exception($Err);
            }
            else
            {
                $this->debugAusgabe("Verzeichniswechsel war erfolgreich", 500);
            }

            //Hier wird die Funktion "HoleDateienvonServer" nicht verwendet,
            //da bevor die Dateien abgeholt werden, noch gepr�ft werden muss, ob diese bereits vorhanden bzw importiert wurden
            $ftp_nlist_root = ftp_nlist($this->AutoglasFTPConnect(), '*.aem');
            foreach($ftp_nlist_root as $file)
            {

                $mod = ftp_mdtm($this->AutoglasFTPConnect(), $file);

                //schneidet vom Dateinamen die Dateiendung ab
                $cut_file = str_replace('.aem', '', $file);

                $pruef = array();
                $pruef[] = $this->_PFAD_INT_AUFTRAGSDAT . $file;
                $pruef[] = $this->_PFAD_INT_AUFTRAGSDAT . "/Backup/" . "imp_$cut_file.txt";
                $pruef[] = $this->_PFAD_INT_AUFTRAGSDAT . $cut_file . ".txt";
                $pruef[] = $this->_PFAD_INT_AUFTRAGSDAT . "/Fehler/" . "err_$cut_file.txt";

                $exist = false;

                //durchl�uft das obere Array, um zu pr�fen, ob die Auftragsdatei bereits existiert,
                //Hierbei kann es verschiedene Konstellationen geben:
                //da die Datei nach z. B. beim Erhalt in ".txt"  und nach erfolgreichen Import in "imp_<dateiname>.txt" umbenannt wird
                foreach($pruef as $check_file)
                {
                    if(file_exists($check_file))
                    {
                        $exist = true;
                    }
                }
                if(!$exist)
                {
                    //wenn die Datei noch nicht existiert, dann hole ab

                    $result = ftp_get($this->AutoglasFTPConnect(), $this->_PFAD_INT_AUFTRAGSDAT . $file, $this->_PFAD_FTP_AUFTRAGSDAT . $file, FTP_BINARY);

                    if($result == false)
                    {
                        $Err = date('d.m.Y H:i:s') . " Das Abholen der Datei $file vom FTP-Server ist fehlgeschlagen!";
                        throw new Exception($Err);
                    }
                    else
                    {
                        ftp_delete($this->AutoglasFTPConnect(), $file);
                    }
                }
            }

            //benennt die Datei vom Autoglas-FTP, in ".txt" um
            $Datei = '';
            $directory = opendir($this->_PFAD_INT_AUFTRAGSDAT);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_AUFTRAGSDAT . $Datei))
                {
                    $Dateiendung = substr($Datei, -4);

                    if($Dateiendung == '.aem')
                    {
                        rename($this->_PFAD_INT_AUFTRAGSDAT . $Datei, $this->_PFAD_INT_AUFTRAGSDAT . str_replace('.aem', '', $Datei) . '.txt');
                    }
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisAutoglas_DateiTransfer: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AUTOGLAS_DATEITRANSFER', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function Glasstamm_abholen()
    {
        try
        {
            $this->HoleDateienvonServer($this->_PFAD_FTP_GLASSTAMM, $this->_PFAD_INT_GLASSTAMM, '*.txt', true, FTP_BINARY);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisAutoglas_DateiTransfer: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AUTOGLAS_DATEITRANSFER', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function Versicherungsdateien_abholen()
    {
        try
        {
            $this->HoleDateienvonServer($this->_PFAD_FTP_VERSEXPORT_ABHOLUNG, $this->_PFAD_INT_VERSRUECKMELDEDAT, '*.zip', true, FTP_BINARY);

            $Datei = '';
            $zip = new ZipArchive;
            $directory = opendir($this->_PFAD_INT_VERSRUECKMELDEDAT);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_VERSRUECKMELDEDAT . $Datei))
                {
                    if(substr($Datei, -4) == '.zip')
                    {
                        $ErgDatei = $Datei;

                        $ersetzen = array("zip_", ".zip", "_geprueft", "-Dpl-OK", "_Dpl-OK", "DEBEKA_");

                        $ErgDatei = str_replace($ersetzen, "", $ErgDatei);

                        //mit select pruefen ob der Dateiname in der Exporttabelle vorhanden ist
                        $SQL = "SELECT count(*) as anz ";
                        $SQL .= " FROM VERSEXPORTPROTOKOLL";
                        $SQL .= " WHERE UPPER(VEP_DATEINAME) " . $this->_DB->LikeOderIst("%$ErgDatei%", awisDatenbank::AWIS_LIKE_UPPER, "vers");

                        $rsPruefeZip = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen("vers", false));

                        //wenn Dateiname nicht in Exporttabelle vorhanden ist, dann in den Ordner manuell_pruefen verschieben,
                        //da dies eventuell neu verarbeitete Vorg�nge sind, die vor Versandt an die Versicherung
                        //noch gepr�ft werden m�ssen. Nachdem die Datei/en gepr�ft wurden, m�ssen sie manuell in den Ordner "geprueft"
                        //verschoben werden.
                        if($rsPruefeZip->FeldInhalt("ANZ") == 0)
                        {
                            $warn = "Bei der Abholung der Ergebnis-Zip-Dateien vom Autoglas-FTP-Server, " . PHP_EOL;
                            $warn .= "wurde folgende Datei gefunden: $Datei" . PHP_EOL;
                            $warn .= "diese nicht den regul�ren Dateien entspricht (siehe Tabelle 'VERSEXPORTPROTOKOLL'): " . PHP_EOL;
                            $warn .= "Die Datei wurde unter folgendem Pfad: " . $this->_PFAD_INT_VERSRUECKMELDEDAT_MAN_ZUPRUEFEN . $Datei . "abgelegt." . PHP_EOL;
                            $warn .= "Bitte manuell pr�fen, und nach Pr�fung in den Ordner: /manuell_pruefen/geprueft/ verschieben! ";

                            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'WARNING - AUTOGLAS_DATEITRANSFER', $warn, 2, '', 'shuttle@de.atu.eu');
                            rename($this->_PFAD_INT_VERSRUECKMELDEDAT . $Datei, $this->_PFAD_INT_VERSRUECKMELDEDAT_MAN_ZUPRUEFEN . $Datei);
                        }
                        else
                        {
                            //Nachdem die Ergebnisdateien erfolgreich vom FTP abgeholt wurden:
                            //Feld "VEP_RUECK_DATUM" mit Sysdate updaten
                            //Feld "VEP_VES_ID" mit 2 updaten = Ergebnisdateien zur�ck erhalten
                            $SQL = "UPDATE VERSEXPORTPROTOKOLL ";
                            $SQL .= " SET VEP_RUECK_DATUM = sysdate, ";
                            $SQL .= " VEP_VES_ID = 2 ";
                            $SQL .= " WHERE UPPER(VEP_DATEINAME) " . $this->_DB->LikeOderIst("%$ErgDatei%", awisDatenbank::AWIS_LIKE_UPPER, "vers");

                            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen("vers", true));

                            if($zip->open($this->_PFAD_INT_VERSRUECKMELDEDAT . $Datei) === true)
                            {
                                $zip->extractTo($this->_PFAD_INT_VERSRUECKMELDEDAT);
                                $zip->close();
                            }
                            else
                            {
                                $Err = date('d.m.Y H:i:s') . " Fehler beim Entpacken der ZIP-Datei unter $this->_PFAD_INT_VERSRUECKMELDEDAT.$Datei ";
                                throw new Exception($Err);
                            }

                            //Schiebe die Dateien ins ZIP-Archiv unter Fileablage
                            rename($this->_PFAD_INT_VERSRUECKMELDEDAT . $Datei, $this->_PFAD_INT_ZIPARCHIV . $Datei);
                        }
                    }
                }
            }

            //Entzippe die Dateien die im Ordner rueckmeldedateien/manuell_pruefen/geprueft liegen:
            $Datei = '';
            $zip = new ZipArchive;
            $directory = opendir($this->_PFAD_INT_VERSRUECKMELDEDAT_MAN_GEPRUEFT);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_VERSRUECKMELDEDAT_MAN_GEPRUEFT . $Datei))
                {
                    if($zip->open($this->_PFAD_INT_VERSRUECKMELDEDAT_MAN_GEPRUEFT . $Datei) === true)
                    {
                        //extrahiere die Dateien in den Rueckmeldeordner, damit von Dort die Dateien sofort verschickt werden k�nnen
                        $zip->extractTo($this->_PFAD_INT_VERSRUECKMELDEDAT);
                        $zip->close();
                    }
                    else
                    {
                        $Err = date('d.m.Y H:i:s') . " Fehler beim Entpacken der ZIP-Datei unter $this->_PFAD_INT_VERSRUECKMELDEDAT.$Datei ";
                        throw new Exception($Err);
                    }

                    if(substr($Datei, -4) == '.zip')
                    {
                        //Schiebe die Dateien ins ZIP-Archiv unter Fileablage
                        rename($this->_PFAD_INT_VERSRUECKMELDEDAT_MAN_GEPRUEFT . $Datei, $this->_PFAD_INT_ZIPARCHIV . $Datei);
                    }
                }
            }

            $Datei = '';
            $directory = opendir($this->_PFAD_INT_VERSRUECKMELDEDAT);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_VERSRUECKMELDEDAT . $Datei))
                {

                    if(strpos($Datei, ".pdf"))
                    {
                        //Setzt den Status 126 = Datei zurueck erhalten
                        //zur Pruefung werden die PDF-Dateien heran gezogen.
                        //Der BHDAT-Job uebernimmt dann die weiteren Pruefungen
                        $filid = substr($Datei, 0, strpos($Datei, "_"));
                        $wanr = substr($Datei, strpos($Datei, "_") + 1, -4);

                        $SQL = 'update versvorgangsstatus';
                        $SQL .= '   set vvs_vcn_key = 126, vvs_datumerhalten = sysdate';
                        $SQL .= '   where vvs_filid = ' . $filid;
                        $SQL .= '   and vvs_wanr = ' . $wanr;
                        $SQL .= '   and vvs_datumerhalten is null';

                        $this->_DB->Ausfuehren($SQL, '', true);
                    }
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisAutoglas_DateiTransfer: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AUTOGLAS_DATEITRANSFER', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function Kassendaten_exportieren()
    {
        try
        {
            //Kopiert die Kassendaten auf den FTP-Server
            $Datei = '';
            $directory = opendir($this->_PFAD_INT_KASSENDAT);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_KASSENDAT . $Datei))
                {
                    $this->debugAusgabe("verarbeite Datei $Datei", 100);

                    $this->KopiereDateienaufServer($this->_PFAD_FTP_KASSENDAT . $Datei, $this->_PFAD_INT_KASSENDAT . $Datei, FTP_BINARY);
                    //Verschiebt die Dateien in das Backup-Verzeichnis
                    if(file_exists($this->_PFAD_INT_KASSENDAT . $Datei))
                    {
                        rename($this->_PFAD_INT_KASSENDAT . $Datei, $this->_PFAD_INT_KASSENDAT_BACKUP . $Datei);
                    }
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisAutoglas_DateiTransfer: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . date('d.m.Y H:i:s') . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AUTOGLAS_DATEITRANSFER', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function KassendatenStorno_exportieren()
    {
        try
        {
            //Kopiert die Kassendaten auf den FTP-Server
            $Datei = '';
            $directory = opendir($this->_PFAD_INT_KASSENDATSTORNO);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_KASSENDATSTORNO . $Datei))
                {
                    //Kopiert die Kassendaten-Dateien auf den FTP-Server
                    $this->KopiereDateienaufServer($this->_PFAD_FTP_KASSENDATSTORNO . $Datei, $this->_PFAD_INT_KASSENDATSTORNO . $Datei, FTP_BINARY);

                    //Verschiebt die Dateien in das Backup-Verzeichnis
                    if(file_exists($this->_PFAD_INT_KASSENDATSTORNO . $Datei))
                    {
                        rename($this->_PFAD_INT_KASSENDATSTORNO . $Datei, $this->_PFAD_INT_KASSENDATSTORNO_BACKUP . $Datei);
                    }
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisAutoglas_DateiTransfer: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AUTOGLAS_DATEITRANSFER', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function Versicherungsdateien_exportieren()
    {
        try
        {
            //Verschiebe zuerst die HUK-Dateien in die Clearing-Stelle und verschiebe alle leeren Dateien ins Backup
            $Datei = '';
            $directory = opendir($this->_PFAD_INT_VERSEXPORT);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_VERSEXPORT . $Datei))
                {
                    $tmp_Datei = '.tmp';

                    //prueft ob die Datei eine Temp-Datei ist
                    //Wenn ja dann darf die Datei nicht angefasst werden
                    if(!strpos($Datei, $tmp_Datei))
                    {
                        $ClearingDatei = 'huk';

                        //prueft ob die Datei einen Inhalt hat,
                        //wenn nicht, kann diese sofort in das Archiv verschoben werden
                        if(filesize($this->_PFAD_INT_VERSEXPORT . $Datei) > 0)
                        {
                            //Pruefen ob die Export-Datei eine HUK-Datei ist, da diese vor Versand zu PD
                            //erst noch durch die Clearing-Stelle bearbeitet werden m�ssen (Fachabteilung)
                            if(strpos($Datei, $ClearingDatei) !== false)
                            {
                                if(file_exists($this->_PFAD_INT_VERSEXPORT . $Datei))
                                {
                                    if(copy($this->_PFAD_INT_VERSEXPORT . $Datei, $this->_PFAD_INT_HUKCLEARING . $Datei))
                                    {
                                        rename($this->_PFAD_INT_VERSEXPORT . $Datei, $this->_PFAD_INT_VERSEXPORT_BACKUP . $Datei);
                                    }
                                }
                            }
                        }
                        else
                        {
                            $SQL = "UPDATE VERSEXPORTPROTOKOLL ";
                            $SQL .= " SET VEP_VES_ID = 3 ";
                            $SQL .= " WHERE VEP_DATEINAME = " . $this->_DB->WertSetzen('VEP', 'T', $Datei);

                            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VEP'));

                            //Feld "VEP_VES_ID" mit 3 updaten = Datei leer, keine weiteren Pr�fungen notwendig
                            $SQL = "UPDATE VERSEXPORTPROTOKOLL ";
                            $SQL .= " SET VEP_VES_ID = 3 ";
                            $SQL .= " WHERE VEP_DATEINAME = " . $this->_DB->WertSetzen('VEP', 'T', $Datei);

                            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VEP'));

                            //leere Dateien direkt ins Backup (Archiv) verschieben.
                            if(file_exists($this->_PFAD_INT_VERSEXPORT . $Datei))
                            {
                                rename($this->_PFAD_INT_VERSEXPORT . $Datei, $this->_PFAD_INT_VERSEXPORT_BACKUP . $Datei);
                            }
                        }
                    }
                }
            }

            $SQL = 'SELECT count(*) as ANZ';
            $SQL .= ' FROM VERSEXPORTPROTOKOLL';
            $SQL .= ' WHERE (VEP_VES_ID = 1 OR VEP_VES_ID = 9)';
            $SQL .= ' AND VEP_DATEINAME NOT LIKE \'%huk%\'';

            $rsExportierteVorgaenge = $this->_DB->RecordSetOeffnen($SQL);

            //pruefen ob, noch Vorgaenge vorhanden sind, bei denen wir noch keine Ergebnisdatei erhalten haben
            //Sollten wir noch keine erhalten haben, sollen noch keine Dateien exportiert werden.
            //Huk-Dateien werden hierbei ausgeschlossen, da diese immer nur einzeln verschickt werden
            if($rsExportierteVorgaenge->FeldInhalt("ANZ") == 0)
            {
                $Datei = '';
                $directory = opendir($this->_PFAD_INT_VERSEXPORT);

                while($Datei = readdir($directory))
                {
                    if(is_file($this->_PFAD_INT_VERSEXPORT . $Datei))
                    {
                        $tmp_Datei = '.tmp';

                        //prueft ob die Datei eine Temp-Datei ist
                        //Wenn ja dann darf die Datei nicht angefasst werden
                        if(!strpos($Datei, $tmp_Datei))
                        {
                            //Alle anderen Dateien die keine "HUK"-Dateien sind k�nnen direkt auf den FTP-Server kopiert werden.

                            $this->KopiereDateienaufServer($this->_PFAD_FTP_VERSEXPORT_ABGLAGE . $Datei, $this->_PFAD_INT_VERSEXPORT . $Datei, FTP_BINARY);

                            $SQL = 'update versvorgangsstatus';
                            $SQL .= '   set vvs_vcn_key = 125, vvs_datumexport = sysdate';
                            $SQL .= '   where vvs_key in(';
                            $SQL .= '   select vvs_key';
                            $SQL .= '   from versvorgangsstatus';
                            $SQL .= '   where vvs_exportdatei = REPLACE(\'' . $Datei . '\', \'.txt\', \'\'))';

                            $this->_DB->Ausfuehren($SQL, '', true);

                            //Nachdem die Exportdatei erfolgreich an FTP �bermittelt wurde:
                            //Feld "VEP_EXPORT_DATUM" mit Sysdate updaten
                            //Feld "VEP_VES_ID" mit 1 updaten = Exportiert zu PD
                            $SQL = "UPDATE VERSEXPORTPROTOKOLL ";
                            $SQL .= " SET VEP_EXPORT_DATUM = sysdate, ";
                            $SQL .= " VEP_VES_ID = 1 ";
                            $SQL .= " WHERE VEP_DATEINAME = " . $this->_DB->WertSetzen('VEP', 'T', $Datei);

                            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VEP'));

                            if(file_exists($this->_PFAD_INT_VERSEXPORT . $Datei))
                            {
                                rename($this->_PFAD_INT_VERSEXPORT . $Datei, $this->_PFAD_INT_VERSEXPORT_BACKUP . $Datei);
                            }
                        }
                    }
                }
            }
            else
            {
                $err = "Kann nicht exportieren, Ergebnisdateien noch nicht erhalten! Bitte pruefen!!!!";
                $this->debugAusgabe($err, 500);
                throw new Exception($err);
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisAutoglas_DateiTransfer: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AUTOGLAS_DATEITRANSFER', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function GepruefteHUKDateien_exportieren()
    {
        try
        {
            $Datei = '';
            $directory = opendir($this->_PFAD_INT_HUKCLEARING_GEPRUEFT);

            while($Datei = readdir($directory))
            {
                if(is_file($this->_PFAD_INT_HUKCLEARING_GEPRUEFT . $Datei))
                {
                    $filesize = filesize($this->_PFAD_INT_HUKCLEARING_GEPRUEFT . $Datei);

                    $this->KopiereDateienaufServer($this->_PFAD_FTP_VERSEXPORT_ABGLAGE . $Datei, $this->_PFAD_INT_HUKCLEARING_GEPRUEFT . $Datei, FTP_BINARY);

                    $SQL = 'update versvorgangsstatus';
                    $SQL .= '   set vvs_vcn_key = 125';
                    $SQL .= '   where vvs_key in(';
                    $SQL .= '   select vvs_key';
                    $SQL .= '   from versvorgangsstatus';
                    $SQL .= '   where vvs_exportdatei = REPLACE(\'' . $Datei . '\', \'.txt\', \'\'))';

                    $this->_DB->Ausfuehren($SQL, '', true);

                    //Nachdem Datei erfolgreich an FTP �bermittelt wurde:
                    //Feld "VEP_EXPORT_DATUM" mit Sysdate updaten = wann die Datei auf den FTP �bermittelt wurde
                    //Feld "VEP_VES_ID" mit 1 updaten = Exportiert zu PD
                    $SQL = "UPDATE VERSEXPORTPROTOKOLL ";
                    $SQL .= " SET VEP_EXPORT_DATUM = sysdate, ";
                    $SQL .= " VEP_VES_ID = 1 ";
                    $SQL .= " WHERE VEP_DATEINAME = " . $this->_DB->WertSetzen('VEP', 'T', $Datei);

                    $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VEP'));

                    if(file_exists($this->_PFAD_INT_HUKCLEARING_GEPRUEFT . $Datei))
                    {
                        rename($this->_PFAD_INT_HUKCLEARING_GEPRUEFT . $Datei, $this->_PFAD_INT_HUKCLEARING_GEPRUEFT_BACKUP . $Datei);
                    }
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'awisAutoglas_DateiTransfer: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - AUTOGLAS_DATEITRANSFER', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function PruefeaufErgebnisDateien()
    {

        //selektiert alle Dateien, bei denen nach 18 Stunden noch immer keine Ergebnisdatei zur�ck gekommen ist
        //und verschickt eine Warning-EMail an die betroffenen Personen
        $SQL = 'SELECT ((sysdate-vep.VEP_EXPORT_DATUM)*24), vep.*';
        $SQL .= ' FROM VERSEXPORTPROTOKOLL vep';
        $SQL .= ' WHERE vep.VEP_VES_ID = 1 AND vep.VEP_VES_ID <> 9';
        $SQL .= ' AND ((sysdate-vep.VEP_EXPORT_DATUM)*24) > ' . $this->_AWISBenutzer->ParameterLesen('Versicherung_PruefeaufErgebnisDateien');

        $rsPruefeErg = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsPruefeErg->EOF())
        {
            $warn = "Sehr geehrte Damen und Herren," . PHP_EOL;
            $warn .= " WARNUNG: Zu folgender Datei" . $rsPruefeErg->FeldInhalt("VEP_DATEINAME") . PHP_EOL;
            $warn .= " die um " . $rsPruefeErg->FeldInhalt("VEP_EXPORT_DATUM") . " auf den Autoglas-FTP-Server exportiert wurde, " . PHP_EOL;
            $warn .= " haben wir noch keine Ergebnisdatei (ZIP-Datei) von PD erhalten. Bitte pruefen!" . PHP_EOL;

            //E-mail an ProDEVELOP?????????
            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'WARNING - AUTOGLAS_DATEITRANSFER', $warn, 2, '', 'shuttle@de.atu.eu');

            $SQL = "UPDATE VERSEXPORTPROTOKOLL ";
            $SQL .= " SET VEP_VES_ID = 9";
            $SQL .= " WHERE VEP_KEY =" . $rsPruefeErg->FeldInhalt("VEP_KEY");

            $updateVorgaenge = $this->_DB->Ausfuehren($SQL);

            $rsPruefeErg->DSWeiter();
        }
    }

    public function KopiereDateienaufServer($zielpfad, $quelldateien, $mode)
    {
        $upload = ftp_put($this->AutoglasFTPConnect(), $zielpfad, $quelldateien, $mode);

        if($this->_DebugLevel >= 500)
        {
            echo "zielpfad: $zielpfad" . PHP_EOL;
            echo "quellpfad: $quelldateien" . PHP_EOL;
            echo $mode . PHP_EOL;
        }

        if(!$upload)
        {
            $Err = " Das Kopieren der Datei unter '$quelldateien', in das Verzeichnis '$zielpfad' auf dem FTP-Server ist fehlgeschlagen" . PHP_EOL;
            throw new Exception($Err);
        }
        else
        {
            $this->debugAusgabe("$quelldateien erfolgreich hochgeladen\n", 500);
        }
    }

    public function HoleDateienvonServer($Pfad_Server, $Pfad_Lokal, $dateiendung, $loeschen = true, $mode)
    {

        if(!ftp_chdir($this->AutoglasFTPConnect(), $Pfad_Server))
        {
            $Err = date('d.m.Y H:i:s') . " Verzeichniswechsel ist fehlgeschlagen! Pfad auf Server: $Pfad_Server" . PHP_EOL;
            throw new Exception($Err);
        }
        else
        {
            $this->debugAusgabe("Verzeichniswechsel war erfolgreich", 500);
        }

        $ftp_nlist_root = ftp_nlist($this->AutoglasFTPConnect(), $dateiendung);

        if(count($ftp_nlist_root) > 0 and $ftp_nlist_root != false)
        {
            foreach($ftp_nlist_root as $file)
            {
                $mod = ftp_mdtm($this->AutoglasFTPConnect(), $file);

                $result = ftp_get($this->AutoglasFTPConnect(), $Pfad_Lokal . $file, $Pfad_Server . $file, $mode);

                if($result == false)
                {
                    $Err = date('d.m.Y H:i:s') . " Fehler beim Abholen der Datei/en $file vom FTP! Pfad auf FTP: $Pfad_Server" . PHP_EOL;
                    throw new Exception($Err);
                }

                if($loeschen == true)
                {
                    if(ftp_delete($this->AutoglasFTPConnect(), $file))
                    {
                        $this->debugAusgabe("Datei/en erfolgreich gel�scht.", 500);
                    }
                    else
                    {
                        $Err = "Fehler beim L�schen der Datei/en $file vom FTP! Pfad auf FTP: $Pfad_Server" . PHP_EOL;
                        throw new Exception($Err);
                    }
                }
            }
        }
    }

    public function AutoglasFTPConnect()
    {

        $this->debugAusgabe("Ich bin die connid: " . $this->_conn_id, 500);

        if($this->_conn_id == '')
        {
            $this->_conn_id = ftp_connect($this->_FTP_SERVER, $this->_FTP_PORT, 60);

            $this->debugAusgabe($this->_FTP_SERVER . PHP_EOL . ' ' . $this->_FTP_USER . PHP_EOL . ' ' . $this->_FTP_PWD . PHP_EOL, 500);

            $login_result = ftp_login($this->_conn_id, $this->_FTP_USER, $this->_FTP_PWD);
            ftp_pasv($this->_conn_id, true);
            // Login mit Benutzername und Passwort
            if((!$this->_conn_id) || (!$login_result))
            {
                ftp_close($this->_conn_id);
                $Err = "Die Verbindung mit dem Autoglas-FTP-Server ist fehlgeschlagen! " . PHP_EOL;
                throw new Exception($Err);
            }
            else
            {
                $this->debugAusgabe("FTP Verbindung war erfolgreich.", 500);
            }
        }
        else
        {

            if(!ftp_nlist($this->_conn_id, "/k/AppData/FirstGlass-FTP/"))
            {
                $this->debugAusgabe("AutoglasFTPConnect konnte das Verzeichnis nicht wechseln. Rufe mich selbst nochmal auf. ", 500);
                $this->_conn_id = '';
                $this->AutoglasFTPConnect();
            }
            else
            {
                $this->debugAusgabe("Ich hatte bereits eine Conn_ID. Conn_ID: $this->_conn_id ", 500);
            }
        }

        $this->debugAusgabe("Returne Conn_ID: $this->_conn_id ", 500);

        return $this->_conn_id;
    }

    /**
     * Echo't einen Text wenn das Level passt
     *
     * @param string $Text  der Ausgegeben werden soll
     * @param number $Level Level welches Ben�tigt wird
     */
    public function debugAusgabe($Text = '', $Level = 500)
    {
        if($this->_DebugLevel >= $Level)
        {
            echo date('d.m.y H:i:s ') . $Text . PHP_EOL;
        }
    }

}

?>