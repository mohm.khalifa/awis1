<?php

/**
 * 
 * @author gebhardt_p
 * @version 0.1
 */

class awisXML
{
	private $_XMLRoot = 'root';
	private $_XMLVersion = '1.0';
	
	/**
	 * Wandelt eine XML in ein Array
	 * @param unknown $xml
	 * @return array
	 */
	public function XMLzuArray($xml) {
		$Werte = '';
		$parser = xml_parser_create('ISO-8859-1');
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, $xml, $Werte);
		xml_parser_free($parser);
	
		$return = array();
		$temp = array();
		//$Werte kommt von xml_parse_into_struct
		//Alle Werte durchgehen und schauen, "was" das Element ist.
		//$Wert ist ein array mit [TYPE] [TAG] = Array Key und [VALUE] = ArrayWert
		foreach($Werte as $Wert)
		{
			if($Wert['type'] == "open")
			{
				array_push($temp, $Wert['tag']);
			}
			elseif($Wert['type'] == "close")
			{
				array_pop($temp);
			}
			elseif($Wert['type'] == "complete")
			{
				array_push($temp, $Wert['tag']); //Key ins Temparray
				$this->_setArrayValue($return, $temp, @$Wert['value']); //Den Wert des XML Elements des Keys $temp in das Array $Element schreiben
				array_pop($temp);
			}
		}
		return $return;
	}
	
	/**
	 * @param unknown $array ist eine Referenz auf das �bergebene Element
	 * @param unknown $Schluessel = array();
	 * @param unknown $Wert
	 */
	private function _setArrayValue(&$array, $Schluessel=array(), $Wert)
	{
		if ($Schluessel)
		{
			$key = array_shift($Schluessel); //Erstes Element des Arrays holen und entfernen,
			$this->_setArrayValue($array[$key], $Schluessel, $Wert); //Mich selbst nochmal aufrufen. Dabei ins zweite if gehen und somit den wert ins array an Stelle $key setzen
		}
		else
		{
			$array = $Wert;
		}
	
	}
	
	
	/**
	 * Wandelt ein Array in eine XML Struktur um
	 * @param unknown $Array
	 */
	public function ArrayZuXML($Array)
	{
		$XmlObj = new SimpleXMLElement("<?xml version=\"$this->_XMLVersion\"?><$this->_XMLRoot></$this->_XMLRoot>");
	
		return $this->_arrayZuXML($Array, $XmlObj);
	}
	
	private function _arrayZuXML($Array, $XmlObj)
	{
		foreach($Array as $key => $value)
		{
			if(is_array($value))
			{
				if(!is_numeric($key))
				{
					$subnode = $XmlObj->addChild("$key");
					$this->_arrayZuXML($value, $subnode);
				}
				else
				{
					$subnode = $XmlObj->addChild("item$key");
					$this->_arrayZuXML($value, $subnode);
				}
			}
			else
			{
				$XmlObj->addChild("$key",htmlspecialchars("$value"));
			}
		}
			
		return $XmlObj->asXML();
	}
	
	/**
	 * Setzt das �u�erste XML Tag
	 * @param unknown $Name
	 */
	public function setzeXMLRoot($Name)
	{
		$this->_XMLRoot = $Name;
	}
	
	/**
	 * Setzt die XML Verion
	 * @param unknown $Version
	 */
	public function setzeXMLVersion($Version)
	{
		$this->_XMLVersion = $Version;
	}
	

}