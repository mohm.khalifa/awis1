<?php

class pdf 
{
	function pt2cm($px)
	{
		return round($px / 72 * 2.54, 2);		//px in cm umrechnen
	}
	
	function cm2pt($cm)
	{
		return round($cm * 72 / 2.54, 0);		//cm in px umrechnen
	}
	
	function rgb($farbwert)					//Umwandlung rgb-Wert von 0 - 255 in Wert von 0 - 1
	{
		return $farbwert/255;
	}
	
	//Erh�lt als Parameter die Tabellenbreite in px, sowie eine beliebige Anzahl Verh�ltniszahlen von Spaltenbreiten
	//rechnet die Zahlen in px um und gibt ein Array mit diesen Zahlen zur�ck
	
	function spaltenbreitepx()
	{
		$arr=func_get_args();
		
		$tabellenbreite=array_shift($arr);	//Array $arr wird um erstes Element, welches Tabellenbreite enth�lt, verk�rzt
	
		$summe=array_sum($arr);

		$zwischensumme=0;

		for($i=0;$i<count($arr)-1;$i++)
		{
			$br=round(($arr[$i]/$summe*$tabellenbreite),0);
			$zwischensumme+=$br;
			$arr[$i]=$br;
		}
		
		$arr[count($arr)-1]=$tabellenbreite-$zwischensumme;
		
		return $arr;
	}
}


//Daten aus Datenbank

$verbindung=ocilogon("schad","schad", "schad9.atu.de");

$anweisung=ociparse($verbindung,"select * from sachbearbeiter");
ociexecute($anweisung);
$anz=ocifetchstatement($anweisung,$arr);
ocifreestatement($anweisung);
ocilogoff($verbindung);


$schriftgroesse=10;
$ausgleichsfaktor_j=0.05;

$grafikx=0;
$grafiky=0;
$textx=0;
$texty=0;


//Seitenma�e in cm

$seitenrand=2.0;
$seitenbreite=21.0;
$seitenhoehe=29.4;




//Umrechnung in px

$seitenrand = pdf::cm2pt($seitenrand);	//Seitenrand

$seitenbreite = pdf::cm2pt($seitenbreite);		//Breite Din-A4
$seitenhoehe = pdf::cm2pt($seitenhoehe);		//H�he Din-A4


$spaltenueberschriften=array("Ngb","SBKUERZEL","jSBNAME","SBVORNAME");

$spaltenbreiten = pdf::spaltenbreitepx(($seitenbreite-2*$seitenrand),10,10,50,50);	//liefert die Spaltenbreiten in px aufgrund der Verh�ltniszahlen

//print_r($spaltenbreiten);



$pdf=pdf_new();				//neues pdf-Objekt erzeugen
pdf_open_file($pdf);		//pdf-Datei in den Arbeitsspeicher laden

pdf_begin_page($pdf, $seitenbreite, $seitenhoehe);

$schrift=pdf_findfont($pdf,"Helvetica-Bold","host",0);
pdf_setfont($pdf, $schrift, $schriftgroesse);

$oberlaenge=round((pdf_get_value($pdf,"ascender",$schrift)*$schriftgroesse),0);
$unterlaenge=round((pdf_get_value($pdf,"descender",$schrift)*$schriftgroesse),0);
$rest2=round((($schriftgroesse-$oberlaenge+$unterlaenge)/2),2);		// 1/2 Rest nachdem Ober- und Unterl�nge von Schriftgr��e abgezogen worden sind


$textx=$seitenrand+$rest2;
$texty=$seitenhoehe-$seitenrand-2*$rest2-$oberlaenge;

$grau="pdf_setcolor(\$pdf,'fill','rgb',pdf::rgb(200),pdf::rgb(200),pdf::rgb(200));";

$blau="pdf_setcolor(\$pdf,'fill','rgb',pdf::rgb(0),pdf::rgb(0),pdf::rgb(255));";

$gelb="pdf_setcolor(\$pdf,'both','rgb',pdf::rgb(255),pdf::rgb(255),pdf::rgb(0));";



eval($grau);

$grafikx=$seitenrand;
$grafiky=$seitenhoehe-$seitenrand-$schriftgroesse;

pdf_rect($pdf,$grafikx,$grafiky,($seitenbreite-2*$seitenrand),$schriftgroesse);
pdf_fill($pdf);

eval($blau);

for($i=0;$i<count($spaltenbreiten);$i++)
{
	$text=$spaltenueberschriften[$i];
	
	while((pdf_stringwidth($pdf,$text)+2*$rest2)>$spaltenbreiten[$i])	//k�rzt den Text solange, bis er in die Spaltenbreite passt
	{
		$text=substr($text,0,strlen($text)-1);
	}
	
	pdf_show_xy($pdf,$text,$textx,$texty);
	
	$textx+=$spaltenbreiten[$i];
}


for($k=0;$k<$anz;$k++)
{
	
	$arrdaten=array();
	$arrdaten[0]=$arr["ID"][$k];
	$arrdaten[1]=$arr["SBKUERZEL"][$k];
	$arrdaten[2]=$arr["SBNAME"][$k];
	$arrdaten[3]=$arr["SBVORNAME"][$k];
	
	if($k % 2)
	{
		eval($grau);
	}
	else
	{
		eval($gelb);
	}
	
	$grafiky-=$schriftgroesse;
	$texty-=$schriftgroesse;
	$textx=$seitenrand;
	
	pdf_rect($pdf,$grafikx,$grafiky,($seitenbreite-2*$seitenrand),$schriftgroesse);
	pdf_fill($pdf);
	
	eval($blau);
	
	for($i=0;$i<count($spaltenbreiten);$i++)
	{
		$text=$arrdaten[$i];
		
		while((pdf_stringwidth($pdf,$text)+2*$rest2)>$spaltenbreiten[$i])	//k�rzt den Text solange, bis er in die Spaltenbreite passt
		{
			$text=substr($text,0,strlen($text)-1);
		}
		
		pdf_show_xy($pdf,$text,$textx,$texty);
		
		$textx+=$spaltenbreiten[$i];
	}
	
	
	
}


pdf_end_page($pdf);
pdf_close($pdf);

$speicherinhalt=pdf_get_buffer($pdf);
$laenge=strlen($speicherinhalt);


header("Content-type: application/pdf");
header("Content-Length: $laenge");
header("Content-Disposition: inline; filename=pdftest.pdf");

echo $speicherinhalt;

pdf_delete($pdf);

?>
