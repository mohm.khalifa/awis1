<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

class VersicherungenFunktionen
{
    private $_DB;
    private $_AWISBenutzer;
    private $_SQL;
    private $_VersImport;
    private $_VersVerarbeitung;
    private $_VersImportAX;
    private $_JobID;
    private $_DWHGeladen = true;
    private $_FGJob = false;
    private $_awisJobs;
    private $_startJob = false;

    // Steuervariablen
    private $_checkDWH = true;

    private $_checkFG = true;

    private $_loadDWHViews = true;
    private $_awisLevel = '';
    private $_Werkzeug = '';

    function __construct()
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Werkzeug = new awisWerkzeuge();
    }

    public function PruefeAnstehend($Modulname, $System)
    {
        //Prueft ob vor diesem Modul, noch ein Modul offen ist,
        //bei dem noch keine Endezeit gesetzt wurde
        //Wenn ja, darf diese Funktion noch nicht ausgef�hrt werden
        $this->_SQL = 'SELECT *';
        $this->_SQL .= ' FROM versjobsteuerung';
        $this->_SQL .= ' WHERE vjs_endezeit IS NULL';
        $this->_SQL .= ' AND vjs_reihenfolge <';
        $this->_SQL .= '   (SELECT vjs_reihenfolge';
        $this->_SQL .= '   FROM versjobsteuerung';
        $this->_SQL .= '   WHERE vjs_system = \'' . $System . '\'';
        $this->_SQL .= '   AND  vjs_modul = \'' . $Modulname . '\'';
        $this->_SQL .= '   )';

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
        {
            $Rueckgabe = false;
        }
        else
        {
            $this->_SQL = 'SELECT * ';
            $this->_SQL .= ' FROM VersJobSteuerung LEFT JOIN VersJobID ';
            $this->_SQL .= ' ON VJS_JOBID = VJI_JOBID ';
            $this->_SQL .= ' WHERE VJS_SYSTEM = \'' . $System . '\'';
            $this->_SQL .= ' AND VJS_MODUL = \'' . $Modulname . '\'';

            $Rueckgabe = false;

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) == 0)
            {
                $Rueckgabe = false;
            }
            else
            {
                $rsJobModul = $this->_DB->RecordSetOeffnen($this->_SQL);

                if($rsJobModul->FeldInhalt('VJI_KEY') == null)
                {
                    $Rueckgabe = true;
                }
            }
        }

        return $Rueckgabe;
    }

    public function PruefeFGJob()
    {
        $this->_SQL = 'SELECT * ';
        $this->_SQL .= ' FROM VersJobSteuerung INNER JOIN VersJobID ';
        $this->_SQL .= ' ON VJS_JOBID = VJI_JOBID ';
        $this->_SQL .= ' WHERE VJS_SYSTEM = \'Firstglas\' ';
        $this->_SQL .= ' AND vjs_modul = \'runImport\'';

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) == 1)
        {
            $this->_FGJob = true;
        }
    }

    public function SchreibeStartZeit($Modulname)
    {
        $this->_SQL = 'UPDATE versjobsteuerung ';
        $this->_SQL .= ' SET vjs_startzeit = sysdate, ';
        $this->_SQL .= ' vjs_endezeit = null, ';
        $this->_SQL .= ' vjs_modulstatus = null ';
        $this->_SQL .= ' WHERE vjs_modul = \'' . $Modulname . '\'';

        $this->_DB->Ausfuehren($this->_SQL, '', true);
    }

    public function SchreibeEndeZeit($Modulname, $JobID)
    {
        $this->_SQL = 'UPDATE versjobsteuerung ';
        $this->_SQL .= ' SET vjs_endezeit = sysdate, ';
        $this->_SQL .= ' vjs_jobid = ' . $JobID;
        $this->_SQL .= ' WHERE vjs_modul = \'' . $Modulname . '\'';

        $this->_DB->Ausfuehren($this->_SQL, '', true);

        $this->ErmittleScriptLaufzeit($Modulname);
    }

    public function ErmittleScriptLaufzeit($Modulname)
    {
        $this->_SQL = 'SELECT vjs_startzeit, vjs_endezeit ';
        $this->_SQL .= ' FROM versjobsteuerung ';
        $this->_SQL .= ' WHERE vjs_modul = \'' . $Modulname . '\'';

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
        {
            $rsZeit = $this->_DB->RecordSetOeffnen($this->_SQL);
            $timeStartzeit = strtotime($rsZeit->FeldInhalt('VJS_STARTZEIT'));
            $timeEndezeit = strtotime($rsZeit->FeldInhalt('VJS_ENDEZEIT'));
            $timeDifferenz = $timeEndezeit - $timeStartzeit;

            $this->_SQL = 'UPDATE versjobsteuerung ';
            $this->_SQL .= ' SET vjs_laufzeit = \'' . $timeDifferenz . '\' ';
            $this->_SQL .= ' WHERE vjs_modul = \'' . $Modulname . '\'';

            $this->_DB->Ausfuehren($this->_SQL, '', true);
        }
    }

    public function SchreibeModulStatus($Modulname, $Statustext)
    {
        try
        {
            $this->_SQL = 'UPDATE versjobsteuerung ';
            $this->_SQL .= ' SET vjs_modulstatus = ' . $this->_DB->WertSetzen('VJS', 'T', $Statustext);
            $this->_SQL .= ' WHERE vjs_modul = ' . $this->_DB->WertSetzen('VJS', 'T', $Modulname);

            $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->Bindevariablen('VJS', true));
        }
        catch(Exception $e)
        {
            echo('Exception:' . $e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo('Exception:' . $awis->getMessage() . $awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }


    /**
     * Methode zum kopieren von Dateien.
     * Bei Problemen wird ingesamt 3mal Versucht die Datei zu kopieren,
     * danach wird eine Fehlermeldung ausgegeben.
     *
     * @param string $QuellPfad
     * @param string $ZielPfad
     *
     * �nderungen:
     * 29.09.2011 OP : Funktion soll zuk�nftig abpr�fen ob �bergebene Quelldatei
     *                 ein Directory ist (copy schmei�t bei Directory ein Warning).
     */
    public function KopiereDatei($QuellPfad, $ZielPfad)
    {
        $copyStatus = false;
        $isDir = false;
        $i = 0;

        if(is_dir($QuellPfad))
        {
            $isDir = true;
        }

        while($copyStatus === false && $isDir === false && $i < 3)
        {
            if(copy($QuellPfad, $ZielPfad))
            {
                if(is_file($ZielPfad))
                {
                    $copyStatus = true;
                }
            }

            if($copyStatus === false)
            {
                sleep(20);
            }

            $i++;
        }

        if($copyStatus === false && $isDir === false)
        {
            echo "Kopieren der Datei: " . $QuellPfad . " fehlgeschlagen<br>\n";

            return false;
        }

        return true;
    }

    /**
     * Methode zum umbenennen von Dateien.
     * Bei Problemen wird 3mal probiert die Datei umzubenennen,
     * danach kommt es zum Scriptabruch.
     *
     * @param string $Quellname
     * @param string $Zielname
     */
    public function DateiUmbenennen($Quellname, $Zielname)
    {
        $renameStatus = false;
        $i = 0;

        while($renameStatus === false && $i < 3)
        {
            if(rename($Quellname, $Zielname))
            {
                if(is_file($Zielname))
                {
                    $renameStatus = true;
                }
            }

            if($renameStatus === false)
            {
                sleep(20);
            }

            $i++;
        }

        if($renameStatus === false)
        {
            echo "Fehler beim Umbenennen der Datei" . $Quellname;
            die();
        }
    }

    /**
     * Methode zum verschieben einer Datei. Bei Problemen wird 3mal versucht die
     * Datei zu verschieben, gelingt dies nicht wird eine Fehlermeldung ausgegeben.
     *
     * @param string $Quellpfad
     * @param string $Zielpfad
     *
     * �nderungen:
     * 29.09.2011 OP : Funktion soll zuk�nftig abpr�fen ob �bergebene Quelldatei
     *                 ein Directory ist (copy schmei�t bei Directory ein Warning).
     */
    public function DateiVerschieben($QuellPfad, $ZielPfad)
    {
        $copyStatus = false;
        $unlinkStatus = false;
        $isDir = false;
        $i = 0;

        if(is_dir($QuellPfad))
        {
            $isDir = true;
        }

        while($copyStatus === false && $isDir === false && $i < 3)
        {
            if(copy($QuellPfad, $ZielPfad))
            {
                if(is_file($ZielPfad))
                {
                    $copyStatus = true;
                }
            }

            if($copyStatus === false)
            {
                sleep(20);
            }

            $i++;
        }

        if($copyStatus === false && $isDir === false)
        {
            echo "Datei Verschieben fehlgeschlagen: " . $QuellPfad . " konnte nicht kopiert werden<br>\n";
        }

        $i = 0;

        while($copyStatus === true && $unlinkStatus === false && $isDir === false && $i < 3)
        {
            if(unlink($QuellPfad))
            {
                if(!is_file($QuellPfad))
                {
                    $unlinkStatus = true;
                }
            }

            if($unlinkStatus === false)
            {
                sleep(20);
            }

            $i++;
        }

        if($unlinkStatus === false && $isDir === false)
        {
            echo "Datei Verschieben fehlgeschlagen: " . $QuellPfad . " konnte nicht gel�scht werden<br>\n";
        }
    }
}

?>
