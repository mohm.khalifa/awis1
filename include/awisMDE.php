<?php
/**
 * Klasse fuer die Verwaltung der MDE Daten
 *
 * Mit Hilfe dieser Klasse werden die Basisfunktionalitaeten fuer die
 * Verwaltung der MDE Daten zur Verfuegung gestellt.
 *
 * @author Sacha Kerres
 * @version 20070802
 * @copyright ATU Auto Teile Unger
 *
 * �nderungen:
 * 15.04.2016 PG @version 1.1 : Scanndatum muss nun >= Kommisionierdatum sein.
 * 29.08.2017 PG @version 1.2 : A-027052, Auskommentierten/Unn�tigen Code entfernt
 *
 */
require_once('awisINFO.php');
require_once('db.inc.php');
require_once('awis_forms.inc.php');
require_once('awisBenutzer.inc');

class awisMDE
{
    /**
     * Debugmodus aktiv/inaktiv
     * Hiermit werden Ausgaben fuer die Kommandozeile erzeugt.
     *
     * @var int
     */
    private $_Debug = 0;            // Debug Infos ausgeben

    /**
     * DebugLevel
     * Grad der ausgabe
     *
     * @var int
     */
    private $_DebugLevel = 0;            // Debug Infos ausgeben


    /**
     * Pfad fuer die Lieferkontroll-Dateien
     *
     * @var string
     */
    private $_LKPfad = '';

    /**
     * Pfad fuer die Bestandsabfrage-Dateien
     *
     * @var string
     */
    private $_BAPfad = '';

    /**
     * Pfad fuer die Raedereinlagerung-Dateien
     *
     * @var string
     */
    private $_REPfad = '';

    /**
     * Pfad fuer die Raederauslagerung-Dateien
     *
     * @var string
     */
    private $_RAPfad = '';

    /**
     * Pfad fuer die R�ckf�hrungs-Dateien (Filiale)
     *
     * @var string
     */
    private $_RFPfad = '';

    /**
     * Pfad fuer die R�ckf�rhungs-Dateien (Zentrale)
     *
     * @var string
     */
    private $_RZPfad = '';

    /**
     * Objekt f�r die AWISInfo - Klasse
     *
     * @var AWISInfo
     */
    private $_AWISParams;

    /**
     * Mailserver fuer Benachrichtigungem
     *
     * @var string
     */
    private $_MailServer = '';

    /**
     * AWIS Benutzer
     *
     * @var awisUser
     */
    private $_AWISBenutzer;

    /**
     * Erzeugung des Objekts
     *
     * @param int $Debug (Debugmodus fuer Ausgaben)
     * @author Sacha Kerres
     * @version 20070802
     */
    public function __construct($Debug = 0, $LoginName = '')
    {

        if ($Debug > 0) {
            $this->_Debug = 1; //Debugs anmachen
            $this->_DebugLevel = $Debug;
        } else {
            $this->_Debug = 0;

        }

        $this->_AWISBenutzer = new awisUser($LoginName);
        $this->_AWISParams = new awisINFO();

        // Mailserver fuer Benachrichtigungen
        $this->_MailServer = $this->_AWISParams->LeseAWISParameter('awis.conf', 'MAIL_SERVER');
        // Pfad fuer die Lieferkonztrolle
        $this->_LKPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'PFAD_LIEFERKONTROLLE');
        //Pfad fuer die Bestandsabfrage
        $this->_BAPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'PFAD_BESTANDSABFRAGE');
        //Pfad fuer die R�dereinlagerungen
        $this->_REPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'PFAD_RAEDEREINLAGERUNG');
        //Pfad fuer die R�derauslagerungen
        $this->_RAPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'PFAD_RAEDERAUSLAGERUNG');
        //Pfad fuer die R�ckf�hrungen (Filiale)
        $this->_RFPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'PFAD_RUECKFUEHRUNGFILIALE');
        //Pfad fuer die R�ckf�hrungen (Zentrale)
        $this->_RZPfad = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'PFAD_RUECKFUEHRUNGZENTRALE');
        if ($this->_Debug) {
            echo 'Pfad LK:' . $this->_LKPfad . PHP_EOL;
        }


        if ($this->_Debug) {
            echo 'Init: OK' . PHP_EOL;
        }
    }

    /**
     * Funktion sucht die Lieferabgleichsdateien und traegt sie in die Datenbank ein
     *
     */
    public function LeseLieferabgleich()
    {
        global $awisRSZeilen;
        try {
            $LieferscheinKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'ERWEITERUNG_LIEFERSCHEIN');
            if ($LieferscheinKZ == '') {
                $LieferscheinKZ = 'LI';
            }

            if ($this->_Debug) {
                $DebugPrefix = date("d.m.Y H:i:s");
            }

            // Alle Dateien vom Verzeichnis lesen
            $LIDateien = new DirectoryIterator($this->_LKPfad);
            foreach ($LIDateien AS $Datei) {

                if (substr($Datei, 0, strlen($LieferscheinKZ)) == $LieferscheinKZ) {
                    if ($this->_Debug) {
                        echo $DebugPrefix . ' Datei: ' . $Datei . ' wird eingelesen' . PHP_EOL;
                    }

                    $Filiale = substr($Datei, strlen($LieferscheinKZ), 4);

                    $con = awisLogon();

                    // Pruefsumme der Datei pr�fen
                    $Pruefsumme = md5_file($this->_LKPfad . '/' . $Datei);

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = '0' . $Filiale;
                    $BindeVariablen['var_T_lkp_pruefsumme'] = $Pruefsumme;

                    $SQL = 'SELECT * FROM LieferkontrolleProtokoll';
                    $SQL .= ' WHERE LKP_FIL_ID = :var_N0_fil_id';
                    $SQL .= ' AND LKP_Pruefsumme = :var_T_lkp_pruefsumme';

                    $rsLKP = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    if ($awisRSZeilen != 0) {
                        // Protokoll updaten
                        $Key = $rsLKP['LKP_KEY'][0];
                        $Zaehler = $rsLKP['LKP_ERGEBNIS'][0];
                        $Zaehler++;

                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_lkp_ergebnis'] = $Zaehler;
                        $BindeVariablen['var_N0_lkp_key'] = $Key;

                        $SQL = 'Update LieferkontrolleProtokoll';
                        $SQL .= ' SET LKP_Zeit=SYSDATE';
                        $SQL .= ' ,LKP_Ergebnis=:var_N0_lkp_ergebnis';
                        $SQL .= " ,LKP_Meldung='Wiederholung'";
                        $SQL .= " ,LKP_USER='" . getenv('HOST') . "'";
                        $SQL .= ' ,LKP_USERDAT=SYSDATE';
                        $SQL .= ' WHERE LKP_KEY=:var_N0_lkp_key';

                        awisExecute($con, $SQL, true, false, 0, $BindeVariablen);

                        $rmreturn = 0;

                        if (is_file($this->_LKPfad . '/~' . $Datei)) {
                            unlink($this->_LKPfad . '/~' . $Datei);
                        }
                        if (rename($this->_LKPfad . '/' . $Datei, $this->_LKPfad . '/~' . $Datei)) {
                            if ($this->_Debug) {
                                echo $DebugPrefix . ' Datei wurde auf ~' . $Datei . ' umbenannt' . PHP_EOL;
                            }
                        } else {
                            if (filesize($this->_LKPfad . '/' . $Datei) == 0) {
                                system("rm -f " . $this->_LKPfad . "/" . $Datei, $rmreturn);
                            }

                            if ($this->_Debug) {
                                echo $DebugPrefix . ' Error: Datei wurde NICHT auf ~' . $Datei . ' umbenannt' . PHP_EOL;
                            }
                        }

                        if ($rmreturn === 0) {
                            $Mailtext = 'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...';
                        } else {
                            $Mailtext = 'Leere Datei ' . $Datei . ' kann nicht gel�scht werden. Bitte in MultiLink-Software �berpr�fen und Verbindung abbrechen...';
                        }

                        syslog(LOG_WARNING, 'MDE - Datei ' . $Mailtext);
                        if ($this->_Debug) {
                            echo $DebugPrefix . $Mailtext . PHP_EOL;
                        }
                        $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_FEHLER');
                        if ($EMail != '') {
                            $EMail = explode(';', $EMail);
                            if ($Zaehler <= 50) //einfache "Loop"erkennung
                            {
                                if ($Zaehler > 10) {
                                    $this->_AWISParams->EMail($EMail,
                                        'MDE FEHLER - LIEFERKONTROLLE - ' . $Datei . ' BEREITS EINGELESEN (LOOP!)',
                                        $Mailtext, 3);
                                } else {
                                    $this->_AWISParams->EMail($EMail,
                                        'MDE FEHLER - LIEFERKONTROLLE - ' . $Datei . ' BEREITS EINGELESEN', $Mailtext,
                                        3);
                                }
                            }
                        }

                        continue;
                    }

                    // Aktuellen Lieferschein ermitteln
                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = '0' . $Filiale;

                    $SQL = 'SELECT LKK_KEY, LKK_LIEFERSCHEINNR, LKK_DATUMKOMM';
                    $SQL .= ' FROM Lieferkontrollenkopf';
                    $SQL .= ' WHERE LKK_STATUS=\'O\'';
                    $SQL .= ' AND LKK_FIL_ID=:var_N0_fil_id';
                    $SQL .= ' AND ROWNUM=1';

                    $rsLKK = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    if ($awisRSZeilen == 0)//Keine Daten
                    {
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' Datei ' . $Datei . ' wird umbenannt, kein offener Lieferschein.' . PHP_EOL;
                        }
                        $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_FEHLER');
                        if ($EMail != '') {
                            $EMail = explode(';', $EMail);
                            $this->_AWISParams->EMail($EMail,
                                'MDE FEHLER - LIEFERKONTROLLE - ' . $Filiale . ' KEIN OFFENER LIEFERSCHEIN',
                                'F�r die Filiale ' . $Filiale . ' liegt kein offener Lieferschein vor.', 3);
                        }
                        syslog(LOG_WARNING,
                            'MDE - F�r die Filiale ' . $Filiale . ' liegt kein offener Lieferschein vor.');
                        $DATUM = date("YmdHis");
                        rename($this->_LKPfad . '/' . $Datei, $this->_LKPfad . '/~' . $DATUM . $Datei);
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                        }
                        continue;
                    } else {
                        $Lieferschein = $rsLKK['LKK_LIEFERSCHEINNR'][0];
                        $Datumkomm = $rsLKK['LKK_DATUMKOMM'][0];
                        $Key = $rsLKK['LKK_KEY'][0];
                    }

                    // Datei komplett lesen
                    $Daten = file($this->_LKPfad . '/' . $Datei, FILE_IGNORE_NEW_LINES);

                    $KommScanMail = false; //Flag ob f�r diese Email bereits eine Email mit Warnung "Scandatum �lter als Kommisionierdatum rausgegangen ist
                    foreach ($Daten AS $Zeile) {
                        $Felder = explode(';', $Zeile);

                        if ($Felder[0] == '') {
                            continue;            // Leerzeilen ueberspringen
                        }

                        if ($Felder[3] == '')        // Keine ATU Nummer -> ignorieren
                        {
                            syslog(LOG_WARNING, 'MDE Warnung: Keine A.T.U Nummer im Datensatz');
                            continue;
                        } else //Pr�fen, ob g�ltige ATUNR
                        {
                            $BindeVariablen = array();
                            $BindeVariablen['var_T_ast_atunr'] = $Felder[3];

                            $SQL = ' SELECT AST_ATUNR ';
                            $SQL .= ' FROM Artikelstamm ';
                            $SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
                            $SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';

                            $rsAST = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                            $rsASTZeilen = $awisRSZeilen;
                            if ($rsASTZeilen == 0) {
                                syslog(LOG_WARNING, 'MDE Warnung: Keine g�ltige A.T.U Nummer');
                                continue;
                            }
                        }

                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_lik_lkk_key'] = $Key;
                        $BindeVariablen['var_T_ast_atunr'] = $Felder[3] . '';

                        $SQL = 'SELECT LIK_KEY';
                        $SQL .= ' FROM Lieferkontrollen';
                        $SQL .= ' WHERE LIK_LKK_KEY=:var_N0_lik_lkk_key';
                        $SQL .= ' AND LIK_AST_ATUNR=:var_T_ast_atunr';

                        $rsLIK = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                        //Das Kommisionierdatum muss kleiner oder zumindest gleich dem Kommisionierdatum sein,
                        //da ansonsten versehentliche Altdaten mit �bertragen werden. Durch einen Batteriewechsel kann
                        //Es vorkommen, dass Datens�tze um 2000 herum entstehen. Diese k�nnen Plausibel sein und m�ssen
                        //Verarbeitet werden.

                        if ((strtotime($Datumkomm) <= strtotime($Felder[2])) or (strtotime($Felder[2]) <= strtotime('31.12.2000'))) {
                            if ($awisRSZeilen == 0)        // Keine Daten
                            {
                                $BindeVariablen = array();
                                $BindeVariablen['var_N0_lik_fil_id'] = awis_FeldInhaltFormat('Z', $Felder[0], false);
                                $BindeVariablen['var_T_lik_ast_atunr'] = str_replace("'", "",
                                    awis_FeldInhaltFormat('T', $Felder[3], false));
                                $BindeVariablen['var_T_lik_lieferscheinnr'] = $Lieferschein;
                                $BindeVariablen['var_D_lik_datumkomm'] = str_replace("'", "",
                                    awis_FeldInhaltFormat('D', $Datumkomm));
                                $BindeVariablen['var_D_lik_datumscan'] = str_replace("'", "",
                                    awis_FeldInhaltFormat('D', $Felder[2]));
                                $BindeVariablen['var_N0_lik_mengeist'] = awis_FeldInhaltFormat('N', $Felder[4]);
                                $BindeVariablen['var_N0_lik_lkk_key'] = $Key;
                                $BindeVariablen['var_N0_lik_mengescan'] = awis_FeldInhaltFormat('N', $Felder[4]);

                                $SQL = 'INSERT INTO Lieferkontrollen';
                                $SQL .= '(LIK_FIL_ID,LIK_QUELLE,LIK_AST_ATUNR,LIK_LIEFERSCHEINNR,LIK_DATUMKOMM,LIK_DATUMSCAN';
                                $SQL .= ',LIK_MENGESOLL,LIK_MENGEIST,LIK_MENGEKORREKTUR,LIK_KORREKTURGRUND,LIK_STATUS';
                                $SQL .= ',LIK_USER,LIK_USERDAT,LIK_LKK_KEY,LIK_MENGESCAN)';
                                $SQL .= 'VALUES(:var_N0_lik_fil_id,\'X\',:var_T_lik_ast_atunr,:var_T_lik_lieferscheinnr,:var_D_lik_datumkomm';
                                $SQL .= ',:var_D_lik_datumscan,0,:var_N0_lik_mengeist,0,null,1';
                                $SQL .= ',\'' . getenv('HOST') . '\',SYSDATE';
                                $SQL .= ',:var_N0_lik_lkk_key,:var_N0_lik_mengescan)';

                                if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                                    $DATUM = date("YmdHis");
                                    rename($this->_LKPfad . '/' . $Datei, $this->_LKPfad . '/~' . $DATUM . $Datei);
                                    syslog(LOG_WARNING,
                                        'MDE - Fehler beim Speichern der Lieferkontrolldaten:' . $SQL . '-' . $Felder[0] . '-');
                                    if ($this->_Debug) {
                                        echo $DebugPrefix . ' MDE - Fehler beim Insert der Lieferkontrolldaten' . PHP_EOL;
                                        echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                                    }
                                    throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:' . $SQL . '-' . $Felder[0] . '-',
                                        17);
                                }
                            } else // Aktualisieren
                            {
                                $BindeVariablen = array();
                                $BindeVariablen['var_D_lik_datumscan'] = str_replace("'", "",
                                    awis_FeldInhaltFormat('D', $Felder[2]));
                                $BindeVariablen['var_N0_lik_mengeist'] = '0' . awis_FeldInhaltFormat('N', $Felder[4]);
                                $BindeVariablen['var_N0_lik_key'] = '0' . $rsLIK['LIK_KEY'][0];
                                $BindeVariablen['var_N0_lik_mengescan'] = '0' . awis_FeldInhaltFormat('N', $Felder[4]);
                                $BindeVariablen['var_N0_lik_mengesollist'] = '0' . awis_FeldInhaltFormat('N',
                                        $Felder[4]);

                                $SQL = 'UPDATE Lieferkontrollen';
                                $SQL .= ' SET LIK_MengeIst=LIK_MengeIst+:var_N0_lik_mengeist';
                                $SQL .= ' ,LIK_MengeScan=LIK_MengeScan+:var_N0_lik_mengescan';
                                $SQL .= ' ,LIK_DatumScan=:var_D_lik_datumscan';
                                $SQL .= ',LIK_STATUS= CASE WHEN LIK_MENGESOLL=(LIK_MengeIst+:var_N0_lik_mengesollist) THEN 10 ELSE 1 END';
                                $SQL .= ',LIK_USER=\'' . getenv('HOST') . '\'';
                                $SQL .= ',LIK_USERDAT=SYSDATE';
                                $SQL .= ' WHERE LIK_KEY=:var_N0_lik_key';

                                if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                                    $DATUM = date("YmdHis");
                                    rename($this->_LKPfad . '/' . $Datei, $this->_LKPfad . '/~' . $DATUM . $Datei);
                                    syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Lieferkontrolldaten:' . $SQL);
                                    if ($this->_Debug) {
                                        echo $DebugPrefix . ' MDE - Fehler beim Update der Lieferkontrolldaten' . PHP_EOL;
                                        echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                                    }
                                    throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:' . $SQL, 16);
                                }
                            }
                        } else //Daten ignorieren, da das Scandatum �lter als das Kommisionierdatum ist
                        {
                            //Nur eine Email pro Datei verschicken
                            if (!$KommScanMail) {
                                echo $DebugPrefix . "Mindestens ein Datensatz der Datei wird �bersprungen: Scandatensatz ist �lter als Kommisionierdatum" . PHP_EOL;
                                syslog(LOG_WARNING, 'MDE Warnung: Scandatensatz ist �lter als Kommisionierdatum');

                                $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_FEHLER');
                                if ($EMail != '') {
                                    $EMail = explode(';', $EMail);
                                    $this->_AWISParams->EMail($EMail,
                                        'MDE FEHLER - LIEFERKONTROLLE - ' . $Filiale . ' Scandatum �lter als Kommisionierdatum',
                                        'F�r die Filiale ' . $Filiale . ' im Datensatz ' . $Felder[3] . PHP_EOL . "Kommisionierung: $Datumkomm " . PHP_EOL . "Scandatum: $Felder[2]" . PHP_EOL . "Dieser und alle weiteren vermeintlich falschen Datens�tze in der Datei wurden �bersprungen",
                                        3);
                                }
                                $KommScanMail = true;
                            }
                        }
                    }

                    //Wenn Feedbacks gesammelt werden dann nur Abchliessen wenn auch Feedback eingegeben
                    $FeedbackOK = true;

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_lkf_lkk_key'] = intval($Key);
                    $Benutzer = awisBenutzer::Init('awis_jobs');

                    if ($Benutzer->ParameterLesen('LieferkontrollFeedback') == "1") {
                        $FeedbackOK = false;
                        $SQL = "SELECT LKF_KEY FROM LIEFERKONTROLLENFEEDBACK";
                        $SQL .= " WHERE LKF_LKK_KEY = :var_N0_lkf_lkk_key";
                        $rsLKF = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        $rsLKFZeilen = $awisRSZeilen;
                        if($rsLKFZeilen>0){
                            $FeedbackOK = true;
                        }
                    }

                    //Wenn keine Differenzen mehr bestehen, LSNR automatisch abschliessen
                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_lik_lkk_key'] = intval($Key);

                    $SQL = 'SELECT COUNT(*) AS ANZAHLDIFF FROM Lieferkontrollen WHERE LIK_MENGEIST<>LIK_MENGESOLL AND LIK_LKK_KEY = :var_N0_lik_lkk_key';
                    $rsLIK = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                    $rsLIKZeilen = $awisRSZeilen;
                    //awis_debug(1,$rsLIK['ANZAHLDIFF'][0]);
                    if ($rsLIK['ANZAHLDIFF'][0] == 0 and $FeedbackOK) {
                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_lkk_key'] = intval($Key);

                        $SQL = 'UPDATE LIEFERKONTROLLENKOPF SET LKK_STATUS = \'A\' WHERE LKK_KEY = :var_N0_lkk_key';
                        if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                            $DATUM = date("YmdHis");
                            rename($this->_LKPfad . '/' . $Datei, $this->_LKPfad . '/~' . $DATUM . $Datei);
                            syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Lieferkontrolldaten:' . $SQL);
                            if ($this->_Debug) {
                                echo $DebugPrefix . ' MDE - Fehler beim Update Status der Lieferkontrolldaten' . PHP_EOL;
                                echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                            }
                            throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:' . $SQL, 16);
                        }

                        //PG: 13.04.2016
                        //Man sollte nicht nur den Kopf auf abgeschlossen setzen,
                        //Sondern auch die Datens�tze, wenn man schon so ein Tabellenkonstrukt baut..


                        $SQL = 'UPDATE Lieferkontrollen';
                        $SQL .= ' SET LIK_Status=12';
                        $SQL .= ' , LIK_UserDat=SYSDATE';
                        $SQL .= ' , LIK_DATUMABSCHLUSS=SYSDATE';
                        $SQL .= ' WHERE LIK_Status < 10 AND LIK_LKK_KEY= :var_N0_lkk_key ';

                        if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                            $DATUM = date("YmdHis");
                            syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Lieferkontrolldaten:' . $SQL);
                            if ($this->_Debug) {
                                echo $DebugPrefix . ' MDE - Fehler beim Update Status der LieferkontrollPOSITIONSDATEN Update 1' . PHP_EOL;
                                echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                            }
                            throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:' . $SQL, 16);
                        }


                        $SQL = 'UPDATE Lieferkontrollen';
                        $SQL .= ' SET LIK_StatusLieferung=1';
                        $SQL .= ' , LIK_DatumAbschluss=SYSDATE';
                        $SQL .= ' , LIK_UserDat=SYSDATE';
                        $SQL .= ' WHERE LIK_LKK_KEY= :var_N0_lkk_key';

                        if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                            $DATUM = date("YmdHis");
                            syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Lieferkontrolldaten:' . $SQL);
                            if ($this->_Debug) {
                                echo $DebugPrefix . ' MDE - Fehler beim Update Status der LieferkontrollPOSITIONSDATEN Update 2' . PHP_EOL;
                                echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                            }
                            throw new Exception('Fehler beim Speichern der Lieferkontrolldaten:' . $SQL, 16);
                        }

                        //Ende PG
                    }

                    // Erfolg schreiben
                    $BindeVariablen = array();
                    $BindeVariablen['var_T_lkp_datei'] = $Datei->getFilename();
                    $BindeVariablen['var_T_lkp_pruefsumme'] = $Pruefsumme;
                    $BindeVariablen['var_N0_lkp_fil_id'] = $Filiale;
                    $BindeVariablen['var_N0_lkp_lieferscheinnr'] = $Lieferschein;

                    $SQL = 'INSERT INTO LieferkontrolleProtokoll';
                    $SQL .= '(LKP_Datei,LKP_Zeit,LKP_Pruefsumme,LKP_FIL_ID,LKP_Ergebnis,LKP_Meldung,LKP_LIEFERSCHEINNR,LKP_User,LKP_UserDat)';
                    $SQL .= 'VALUES( :var_T_lkp_datei,SYSDATE , :var_T_lkp_pruefsumme , :var_N0_lkp_fil_id';
                    $SQL .= ',1,null, :var_N0_lkp_lieferscheinnr';
                    $SQL .= ',\'' . getenv('HOST') . '\'';
                    $SQL .= ',SYSDATE)';

                    if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                        $DATUM = date("YmdHis");
                        rename($this->_LKPfad . '/' . $Datei, $this->_LKPfad . '/~' . $DATUM . $Datei);
                        syslog(LOG_WARNING, 'MDE - Fehler beim Speichern des Protokolls:' . $SQL);
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' MDE - Fehler beim Speichern des Protokolls' . PHP_EOL;
                            echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                        }
                        throw new Exception('Fehler beim Speichern des Protokolls:' . $SQL, 15);
                    }

                    // Datei umbenennen
                    if (is_file($this->_LKPfad . '/_' . $Datei)) {
                        unlink($this->_LKPfad . '/_' . $Datei);
                    }
                    if (rename($this->_LKPfad . '/' . $Datei, $this->_LKPfad . '/_' . $Datei)) {
                        syslog(LOG_INFO, 'MDE - Datei ' . $Datei . ' erfolgreich eingelesen.');
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' ' . $Datei . ' wurde auf _' . $Datei . ' umbenannt' . PHP_EOL;
                        }
                    } else {
                        syslog(LOG_WARNING, 'MDE - Datei ' . $Datei . ' konnte nicht umbenannt werden.');
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' Error: ' . $Datei . ' wurde NICHT auf _' . $Datei . ' umbenannt' . PHP_EOL;
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            if ($ex->getCode() == 1) {

            }
            throw new Exception('Fehler beim Lesen der Lieferscheinabgleich-Daten: ' . $ex->getMessage() . ' - ' . $ex->getCode(),
                12);
        }

    }

    /**
     * Funktion traegt die Bestandsabfrage - Daten in die Datenbank ein
     *
     */
    public function ImportBestandsabfrage()
    {
        global $awisRSZeilen;
        try {
            $BestandsabfrageKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'ERWEITERUNG_BESTANDSABFRAGE');
            if ($BestandsabfrageKZ == '') {
                $BestandsabfrageKZ = 'BA';
            }

            // Alle Dateien vom Verzeichnis lesen
            $BADateien = new DirectoryIterator($this->_BAPfad);
            foreach ($BADateien AS $Datei) {
                if (substr($Datei, 0, strlen($BestandsabfrageKZ)) == $BestandsabfrageKZ) {
                    $Filiale = substr($Datei, strlen($BestandsabfrageKZ), 4);
                    $Scannerid = substr($Datei, strlen($BestandsabfrageKZ) + 4, 1);

                    if ($this->_Debug) {
                        echo 'BESTANDSABFRAGE' . PHP_EOL;
                        echo 'Datei: ' . $Datei . PHP_EOL;
                        echo 'Filiale: ' . $Filiale . PHP_EOL;
                        echo 'Scannerid: ' . $Scannerid . PHP_EOL;
                    }

                    $con = awisLogon();

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = $Filiale;
                    $BindeVariablen['var_N0_baf_scannerid'] = $Scannerid;

                    $SQL = 'DELETE FROM Bestandsabfragen where BAF_FIL_ID = :var_N0_fil_id AND BAF_SCANNERID = :var_N0_baf_scannerid';

                    if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                        syslog(LOG_WARNING,
                            'MDE - Fehler beim L�schen der alten Bestandsabfragedaten:' . $SQL . '-' . $Felder[0] . '-');
                        throw new Exception('Fehler beim L�chen der alten Bestandsabfragedaten:' . $SQL . '-' . $Felder[0] . '-',
                            17);
                    }

                    // Datei komplett lesen
                    $Daten = file($this->_BAPfad . '/' . $Datei, FILE_IGNORE_NEW_LINES);
                    foreach ($Daten AS $Zeile) {
                        $Felder = explode(';', $Zeile);

                        $Felderanzahl = 0;
                        $Felderanzahl = count($Felder);

                        if ($Felder[0] == '') {
                            continue;            // Leerzeilen ueberspringen
                        }

                        $ATUNR = substr($Felder[2], 0, 6);

                        if ($Felder[2] == '')        // Keine A.T.U Nummer -> ignorieren
                        {
                            syslog(LOG_WARNING, 'MDE Warnung: Keine A.T.U Nummer im Datensatz');
                            continue;
                        } else //Pr�fen, ob g�ltige ATUNR
                        {
                            $BindeVariablen = array();
                            $BindeVariablen['var_T_ast_atunr'] = $ATUNR;

                            $SQL = ' SELECT AST_ATUNR ';
                            $SQL .= ' FROM Artikelstamm ';
                            $SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
                            $SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';

                            $rsAST = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                            $rsASTZeilen = $awisRSZeilen;
                            if ($rsASTZeilen == 0) {
                                syslog(LOG_WARNING, 'MDE Warnung: Keine g�ltige A.T.U Nummer');
                                continue;
                            }
                        }

                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_fil_id'] = $Filiale;
                        $BindeVariablen['var_T_ast_atunr'] = $ATUNR;

                        $SQL = 'SELECT BAF_KEY';
                        $SQL .= ' FROM Bestandsabfragen';
                        $SQL .= ' WHERE BAF_FIL_ID=:var_N0_fil_id';
                        $SQL .= ' AND BAF_AST_ATUNR=:var_T_ast_atunr';
                        //$SQL .= ' AND LIK_LIEFERSCHEINNR=0'.awis_FeldInhaltFormat('Z',$Lieferschein.'',false);

                        $rsLIK = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        if ($awisRSZeilen == 0)        // Keine Daten
                        {

                            //Neue Datei Speichern
                            $SQL = 'INSERT INTO Bestandsabfragen';
                            $SQL .= '(BAF_FIL_ID,BAF_SCANNERID,BAF_AST_ATUNR,BAF_DATUMSCAN,BAF_USER,BAF_USERDAT,BAF_MENGESCAN)';
                            $SQL .= 'VALUES(';
                            $SQL .= '' . awis_FeldInhaltFormat('Z', $Felder[0], false);
                            $SQL .= ',' . $Scannerid;
                            $SQL .= ',' . awis_FeldInhaltFormat('T', $ATUNR, false);
                            $SQL .= ',' . awis_FeldInhaltFormat('D', $Felder[1], false);
                            $SQL .= ',\'' . getenv('HOST') . '\'';
                            $SQL .= ',SYSDATE';

                            if (intval($Felderanzahl) >= 4) {
                                $SQL .= ',' . awis_FeldInhaltFormat('Z', $Felder[3], false);
                            } else {
                                $SQL .= ',' . awis_FeldInhaltFormat('Z', 0, false);
                            }

                            $SQL .= ')';

                            if (awisExecute($con, $SQL) === false) {
                                syslog(LOG_WARNING,
                                    'MDE - Fehler beim Speichern der Bestandsabfragedaten:' . $SQL . '-' . $Felder[0] . '-');
                                throw new Exception('Fehler beim Speichern der Bestandsabfragedaten:' . $SQL . '-' . $Felder[0] . '-',
                                    17);
                            }
                        } else    // Aktualisieren
                        {
                            $SQL = 'UPDATE Bestandsabfragen';
                            $SQL .= ' SET BAF_DatumScan=' . awis_FeldInhaltFormat('D', $Felder[1]);

                            if (intval($Felderanzahl) >= 4) {
                                $SQL .= ' ,BAF_MengeScan=BAF_MengeScan+' . awis_FeldInhaltFormat('N', $Felder[3]);
                            }

                            $SQL .= ',BAF_USER=\'' . getenv('HOST') . '\'';
                            $SQL .= ',BAF_USERDAT=SYSDATE';
                            $SQL .= ' WHERE BAF_KEY=0' . $rsLIK['BAF_KEY'][0];

                            if (awisExecute($con, $SQL) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_BAPfad . '/' . $Datei, $this->_BAPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der BAdaten:' . $SQL);
                                throw new Exception('Fehler beim Speichern der BAdaten:' . $SQL, 16);
                            }
                        }
                    }

                    // Datei umbenennen
                    if (is_file($this->_BAPfad . '/_' . $Datei)) {
                        unlink($this->_BAPfad . '/_' . $Datei);
                    }
                    rename($this->_BAPfad . '/' . $Datei, $this->_BAPfad . '/_' . $Datei);

                    syslog(LOG_INFO, 'MDE - Datei ' . $Datei . ' erfolgreich eingelesen.');
                }
            }
        } catch (Exception $ex) {
            if ($ex->getCode() == 1) {

            }
            throw new Exception('Fehler beim Lesen der Bestandsabfrage-Daten: ' . $ex->getMessage() . ' - ' . $ex->getCode(),
                12);
        }

    }

    /**
     * Funktion sucht die Raedereinlagerungsdateien und traegt sie in die Datenbank ein
     *
     */
    public function LeseRaedereinlagerung()
    {
        global $awisRSZeilen;
        try {
            $RaedereinlagerungKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf',
                'ERWEITERUNG_RAEDEREINLAGERUNG');
            if ($RaedereinlagerungKZ == '') {
                $RaedereinlagerungKZ = 'RE';
            }

            // Alle Dateien vom Verzeichnis lesen
            $REDateien = new DirectoryIterator($this->_REPfad);
            foreach ($REDateien AS $Datei) {
                if (substr($Datei, 0, strlen($RaedereinlagerungKZ)) == $RaedereinlagerungKZ) {
                    if ($this->_Debug) {
                        echo 'Datei: ' . $Datei . PHP_EOL;
                    }

                    $Filiale = substr($Datei, strlen($RaedereinlagerungKZ), 4);

                    $con = awisLogon();

                    // Pruefsumme der Datei pr�fen
                    $Pruefsumme = md5_file($this->_REPfad . '/' . $Datei);

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = $Filiale;
                    $BindeVariablen['var_T_rep_pruefsumme'] = $Pruefsumme;

                    $SQL = 'SELECT * FROM RaedereinlagerungProtokoll';
                    $SQL .= ' WHERE REP_FIL_ID = :var_N0_fil_id';
                    $SQL .= ' AND REP_Pruefsumme= :var_T_rep_pruefsumme';

                    $rsREP = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    if ($awisRSZeilen != 0) {
                        if ($this->_Debug) {
                            echo 'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...' . PHP_EOL;
                        }
                        $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_FEHLER');
                        if ($EMail != '') {
                            $EMail = explode(';', $EMail);
                            $this->_AWISParams->EMail($EMail,
                                'MDE FEHLER - RAEDEREINLAGERUNG - ' . $Datei . ' BEREITS EINGELESEN',
                                'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...', 3);
                        }
                        syslog(LOG_WARNING,
                            'MDE - Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...');
                        if (is_file($this->_REPfad . '/~' . $Datei)) {
                            unlink($this->_REPfad . '/~' . $Datei);
                        }
                        rename($this->_REPfad . '/' . $Datei, $this->_REPfad . '/~' . $Datei);
                        continue;
                    }

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = $Filiale;

                    $SQL = 'SELECT NVL(MAX(REL_LFDNR),0) as MAXLFDNR';
                    $SQL .= ' FROM Raedereinlagerungen';
                    $SQL .= ' WHERE REL_STATUS<10';
                    $SQL .= ' AND REL_FIL_ID=:var_N0_fil_id';
                    //$SQL.=' AND ROWNUM=1';
                    $rsREL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    //Keine Aktueller Lieferschein vorhanden Daten
                    if ($rsREL['MAXLFDNR'][0] == '0' or $awisRSZeilen == 0) {
                        //N�chste LFDNR von der Filiale ermitteln

                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_fil_id'] = $Filiale;

                        $SQL = 'SELECT NVL(MAX(REL_LFDNR),0) as MAXLFDNR';
                        $SQL .= ' FROM Raedereinlagerungen';
                        $SQL .= ' WHERE REL_FIL_ID=:var_N0_fil_id';
                        $rsREL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                        //Noch keine Daten f�r die Filiale vorhanden
                        if ($rsREL['MAXLFDNR'][0] == '0' or $awisRSZeilen == 0) {
                            $Laufendenr = 1;
                        } else {
                            $Laufendenr = $rsREL['MAXLFDNR'][0] + 1;
                        }
                    } else {
                        $Laufendenr = $rsREL['MAXLFDNR'][0];
                    }

                    // Datei komplett lesen
                    $Daten = file($this->_REPfad . '/' . $Datei, FILE_IGNORE_NEW_LINES);
                    foreach ($Daten AS $Zeile) {
                        $Felder = explode(';', $Zeile);

                        if ($Felder[0] == '') {
                            continue;            // Leerzeilen ueberspringen
                        }

                        if ($Felder[2] == '')        // Keine Nummer -> ignorieren
                        {
                            syslog(LOG_WARNING, 'MDE Warnung: Keine Einlagerungsnummer im Datensatz');
                            continue;
                        }

                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_fil_id'] = $Filiale;
                        $BindeVariablen['var_N0_rel_lfdnr'] = $Laufendenr;
                        $BindeVariablen['var_T_rel_nr'] = $Felder[2] . '';

                        $SQL = 'SELECT REL_KEY';
                        $SQL .= ' FROM Raedereinlagerungen';
                        $SQL .= ' WHERE REL_FIL_ID=:var_N0_fil_id';
                        $SQL .= ' AND REL_LFDNR=:var_N0_rel_lfdnr';
                        $SQL .= ' AND REL_NR=:var_T_rel_nr';

                        $rsREL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        if ($awisRSZeilen == 0)        // Keine Daten
                        {
                            $SQL = 'INSERT INTO Raedereinlagerungen';
                            $SQL .= '(REL_FIL_ID,REL_NR,REL_MENGE,REL_LFDNR,REL_DATUMSCAN,REL_STATUS';
                            $SQL .= ',REL_MENGESCAN,REL_USER,REL_USERDAT,REL_DATUMUEBERTRAGUNG)';
                            $SQL .= 'VALUES(';
                            $SQL .= '' . awis_FeldInhaltFormat('Z', $Felder[0], false);
                            $SQL .= ',' . awis_FeldInhaltFormat('T', $Felder[2], false);
                            $SQL .= ',' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ',' . awis_FeldInhaltFormat('Z', $Laufendenr . '', false);
                            $SQL .= ',' . awis_FeldInhaltFormat('D', $Felder[1]);
                            $SQL .= ',1';
                            $SQL .= ',' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ',\'' . getenv('HOST') . '\'';
                            $SQL .= ',SYSDATE';
                            $SQL .= ',SYSDATE';
                            $SQL .= ')';

                            if (awisExecute($con, $SQL) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_REPfad . '/' . $Datei, $this->_REPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING,
                                    'MDE - Fehler beim Speichern der Raedereinlagerungdaten:' . $SQL . '-' . $Felder[0] . '-');
                                throw new Exception('Fehler beim Speichern der Raedereinlagerungdaten:' . $SQL . '-' . $Felder[0] . '-',
                                    17);
                            }
                        } else                        // Aktualisieren
                        {
                            $SQL = 'UPDATE Raedereinlagerungen';
                            $SQL .= ' SET REL_MENGE=REL_MENGE+' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ' ,REL_MENGESCAN=REL_MENGESCAN+' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ' ,REL_DATUMSCAN=' . awis_FeldInhaltFormat('D', $Felder[1]);
                            $SQL .= ' ,REL_STATUS= 1';
                            $SQL .= ' ,REL_USER=\'' . getenv('HOST') . '\'';
                            $SQL .= ', REL_USERDAT=SYSDATE';
                            $SQL .= ', REL_DATUMUEBERTRAGUNG=SYSDATE';
                            $SQL .= ' WHERE REL_KEY=0' . $rsREL['REL_KEY'][0];
                            if (awisExecute($con, $SQL) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_REPfad . '/' . $Datei, $this->_REPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Raedereinlagerungdaten:' . $SQL);
                                throw new Exception('Fehler beim Speichern der Raedereinlagerungdaten:' . $SQL, 16);
                            }
                        }
                    }

                    // Erfolg schreiben
                    $SQL = 'INSERT INTO RaedereinlagerungProtokoll';
                    $SQL .= '(REP_Datei,REP_Zeit,REP_Pruefsumme,REP_FIL_ID,REP_Ergebnis,REP_Meldung,REP_LFDNR,REP_USER,REP_USERDAT)';
                    $SQL .= 'VALUES(';
                    $SQL .= awis_FeldInhaltFormat('T', $Datei, false);
                    $SQL .= ',SYSDATE';
                    $SQL .= ',' . awis_FeldInhaltFormat('T', $Pruefsumme, false);
                    $SQL .= ',' . awis_FeldInhaltFormat('Z', $Filiale, false);
                    $SQL .= ',0';
                    $SQL .= ',null';
                    $SQL .= ',' . awis_FeldInhaltFormat('Z', $Laufendenr, false);
                    $SQL .= ',\'' . getenv('HOST') . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';

                    if (awisExecute($con, $SQL) === false) {
                        $DATUM = date("YmdHis");
                        rename($this->_REPfad . '/' . $Datei, $this->_REPfad . '/~' . $DATUM . $Datei);
                        syslog(LOG_WARNING, 'MDE - Fehler beim Speichern des Protokolls:' . $SQL);
                        throw new Exception('Fehler beim Speichern des Protokolls:' . $SQL, 15);
                    }

                    // Datei umbenennen
                    if (is_file($this->_REPfad . '/_' . $Datei)) {
                        unlink($this->_REPfad . '/_' . $Datei);
                    }
                    rename($this->_REPfad . '/' . $Datei, $this->_REPfad . '/_' . $Datei);
                    syslog(LOG_INFO, 'MDE - Datei ' . $Datei . ' erfolgreich eingelesen.');
                }
            }
        } catch (Exception $ex) {
            if ($ex->getCode() == 1) {

            }
            throw new Exception('Fehler beim Lesen der Raedereinlagerungs-Daten: ' . $ex->getMessage() . ' - ' . $ex->getCode(),
                12);
        }

    }

    /* Funktion sucht die Raedereinlagerungsdateien und traegt sie in die Datenbank ein
	 *
	 */
    public function LeseRaederauslagerung()
    {
        global $awisRSZeilen;
        try {
            $RaederauslagerungKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf',
                'ERWEITERUNG_RAEDERAUSLAGERUNG');
            if ($RaederauslagerungKZ == '') {
                $RaederauslagerungKZ = 'RA';
            }

            // Alle Dateien vom Verzeichnis lesen
            $RADateien = new DirectoryIterator($this->_RAPfad);
            foreach ($RADateien AS $Datei) {
                if (substr($Datei, 0, strlen($RaederauslagerungKZ)) == $RaederauslagerungKZ AND substr($Datei, 0,
                        3) != 'RAN'
                ) {
                    if ($this->_Debug) {
                        echo 'Datei: ' . $Datei . PHP_EOL;
                    }

                    $Filiale = substr($Datei, strlen($RaederauslagerungKZ), 4);

                    $con = awisLogon();

                    // Pruefsumme der Datei pr�fen
                    $Pruefsumme = md5_file($this->_RAPfad . '/' . $Datei);

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = $Filiale;
                    $BindeVariablen['var_T_rap_pruefsumme'] = $Pruefsumme;

                    $SQL = 'SELECT * FROM RaederauslagerungProtokoll';
                    $SQL .= ' WHERE RAP_FIL_ID = :var_N0_fil_id';
                    $SQL .= ' AND RAP_Pruefsumme= :var_T_rap_pruefsumme';

                    $rsRAP = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    if ($awisRSZeilen != 0) {
                        if ($this->_Debug) {
                            echo 'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...' . PHP_EOL;
                        }
                        $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_FEHLER');
                        if ($EMail != '') {
                            $EMail = explode(';', $EMail);
                            $this->_AWISParams->EMail($EMail,
                                'MDE FEHLER - RAEDERAUSLAGERUNG - ' . $Datei . ' BEREITS EINGELESEN',
                                'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...', 3);
                        }
                        syslog(LOG_WARNING,
                            'MDE - Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...');
                        if (is_file($this->_RAPfad . '/~' . $Datei)) {
                            unlink($this->_RAPfad . '/~' . $Datei);
                        }
                        rename($this->_RAPfad . '/' . $Datei, $this->_RAPfad . '/~' . $Datei);
                        continue;
                    }

                    // Aktuellen Lieferschein ermitteln

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = $Filiale;

                    $SQL = 'SELECT NVL(MAX(RAL_LFDNR),0) as MAXLFDNR';
                    $SQL .= ' FROM Raederauslagerungen';
                    $SQL .= ' WHERE RAL_STATUS<10';
                    $SQL .= ' AND RAL_FIL_ID=:var_N0_fil_id';
                    //$SQL.=' AND ROWNUM=1';
                    $rsRAL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    //Keine Aktueller Lieferschein vorhanden Daten
                    if ($rsRAL['MAXLFDNR'][0] == '0' or $awisRSZeilen == 0) {
                        //N�chste LFDNR von der Filiale ermitteln

                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_fil_id'] = $Filiale;

                        $SQL = 'SELECT NVL(MAX(RAL_LFDNR),0) as MAXLFDNR';
                        $SQL .= ' FROM Raederauslagerungen';
                        $SQL .= ' WHERE RAL_FIL_ID=:var_N0_fil_id';
                        //$SQL.=' AND ROWNUM=1';
                        $rsRAL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                        //Noch keine Daten f�r die Filiale vorhanden
                        if ($rsRAL['MAXLFDNR'][0] == '0' or $awisRSZeilen == 0) {
                            $Laufendenr = 1;
                        } else {
                            $Laufendenr = $rsRAL['MAXLFDNR'][0] + 1;
                        }
                    } else {
                        $Laufendenr = $rsRAL['MAXLFDNR'][0];
                    }

                    // Datei komplett lesen
                    $Daten = file($this->_RAPfad . '/' . $Datei, FILE_IGNORE_NEW_LINES);
                    foreach ($Daten AS $Zeile) {
                        $Felder = explode(';', $Zeile);

                        if ($Felder[0] == '') {
                            continue;            // Leerzeilen ueberspringen
                        }

                        if ($Felder[2] == '')        // Keine Nummer -> ignorieren
                        {
                            syslog(LOG_WARNING, 'MDE Warnung: Keine Einlagerungsnummer im Datensatz');
                            continue;
                        }


                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_fil_id'] = $Filiale;
                        $BindeVariablen['var_N0_ral_lfdnr'] = $Laufendenr;
                        $BindeVariablen['var_T_ral_nr'] = $Felder[2] . '';

                        $SQL = 'SELECT RAL_KEY';
                        $SQL .= ' FROM Raederauslagerungen';
                        $SQL .= ' WHERE RAL_FIL_ID=:var_N0_fil_id';
                        $SQL .= ' AND RAL_LFDNR=:var_N0_ral_lfdnr';
                        $SQL .= ' AND RAL_NR=:var_T_ral_nr';

                        $rsRAL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        if ($awisRSZeilen == 0)        // Keine Daten
                        {
                            $SQL = 'INSERT INTO Raederauslagerungen';
                            $SQL .= '(RAL_FIL_ID,RAL_NR,RAL_MENGE,RAL_LFDNR,RAL_DATUMSCAN,RAL_STATUS';
                            $SQL .= ',RAL_MENGESCAN,RAL_USER,RAL_USERDAT,RAL_DATUMUEBERTRAGUNG)';
                            $SQL .= 'VALUES(';
                            $SQL .= '' . awis_FeldInhaltFormat('Z', $Felder[0], false);
                            $SQL .= ',' . awis_FeldInhaltFormat('T', $Felder[2], false);
                            $SQL .= ',' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ',' . awis_FeldInhaltFormat('Z', $Laufendenr . '', false);
                            $SQL .= ',' . awis_FeldInhaltFormat('D', $Felder[1]);
                            $SQL .= ',1';
                            $SQL .= ',' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ',\'' . getenv('HOST') . '\'';
                            $SQL .= ',SYSDATE';
                            $SQL .= ',SYSDATE';
                            $SQL .= ')';

                            if (awisExecute($con, $SQL) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_RAPfad . '/' . $Datei, $this->_RAPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING,
                                    'MDE - Fehler beim Speichern der Raederauslagerungdaten:' . $SQL . '-' . $Felder[0] . '-');
                                throw new Exception('Fehler beim Speichern der Raederauslagerungdaten:' . $SQL . '-' . $Felder[0] . '-',
                                    17);
                            }
                        } else                        // Aktualisieren
                        {
                            $SQL = 'UPDATE Raederauslagerungen';
                            $SQL .= ' SET RAL_MENGE=RAL_MENGE+' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ' ,RAL_MENGESCAN=RAL_MENGESCAN+' . awis_FeldInhaltFormat('N', $Felder[3]);
                            $SQL .= ' ,RAL_DATUMSCAN=' . awis_FeldInhaltFormat('D', $Felder[1]);
                            $SQL .= ' ,RAL_STATUS= 1';
                            $SQL .= ' ,RAL_USER=\'' . getenv('HOST') . '\'';
                            $SQL .= ', RAL_USERDAT=SYSDATE';
                            $SQL .= ', RAL_DATUMUEBERTRAGUNG=SYSDATE';
                            $SQL .= ' WHERE RAL_KEY=0' . $rsRAL['RAL_KEY'][0];
                            if (awisExecute($con, $SQL) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_RAPfad . '/' . $Datei, $this->_RAPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Raederauslagerungdaten:' . $SQL);
                                throw new Exception('Fehler beim Speichern der Raederauslagerungdaten:' . $SQL, 16);
                            }
                        }
                    }

                    // Erfolg schreiben
                    $SQL = 'INSERT INTO RaederauslagerungProtokoll';
                    $SQL .= '(RAP_Datei,RAP_Zeit,RAP_Pruefsumme,RAP_FIL_ID,RAP_Ergebnis,RAP_Meldung,RAP_LFDNR,RAP_USER,RAP_USERDAT)';
                    $SQL .= 'VALUES(';
                    $SQL .= awis_FeldInhaltFormat('T', $Datei, false);
                    $SQL .= ',SYSDATE';
                    $SQL .= ',' . awis_FeldInhaltFormat('T', $Pruefsumme, false);
                    $SQL .= ',' . awis_FeldInhaltFormat('Z', $Filiale, false);
                    $SQL .= ',0';
                    $SQL .= ',null';
                    $SQL .= ',' . awis_FeldInhaltFormat('Z', $Laufendenr, false);
                    $SQL .= ',\'' . getenv('HOST') . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';

                    if (awisExecute($con, $SQL) === false) {
                        $DATUM = date("YmdHis");
                        rename($this->_RAPfad . '/' . $Datei, $this->_RAPfad . '/~' . $DATUM . $Datei);
                        syslog(LOG_WARNING, 'MDE - Fehler beim Speichern des Protokolls:' . $SQL);
                        throw new Exception('Fehler beim Speichern des Protokolls:' . $SQL, 15);
                    }

                    // Datei umbenennen
                    if (is_file($this->_RAPfad . '/_' . $Datei)) {
                        unlink($this->_RAPfad . '/_' . $Datei);
                    }
                    rename($this->_RAPfad . '/' . $Datei, $this->_RAPfad . '/_' . $Datei);

                    syslog(LOG_INFO, 'MDE - Datei ' . $Datei . ' erfolgreich eingelesen.');
                }
            }
        } catch (Exception $ex) {
            if ($ex->getCode() == 1) {

            }
            throw new Exception('Fehler beim Lesen der Raederauslagerungs-Daten: ' . $ex->getMessage() . ' - ' . $ex->getCode(),
                12);
        }

    }

    /**
     * Funktion sucht die Rueckfuehrungsdaten von den Filialen und traegt sie in die Datenbank ein
     *
     */
    public function LeseRueckfuehrung()
    {
        global $awisRSZeilen;
        try {
            $RueckfuehrungsKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'ERWEITERUNG_RUECKFUEHRUNGFIL');
            if ($RueckfuehrungsKZ == '') {
                $RueckfuehrungsKZ = 'RU';
            }
            if ($this->_Debug) {
                $DebugPrefix = $RueckfuehrungsKZ . 'F' . date("YmdHis");
                echo $DebugPrefix . ' Pr�fung auf R�ckf�hrung FILIALE beginnt' . PHP_EOL;
            }
            // Alle Dateien vom Verzeichnis lesen
            $RFDateien = new DirectoryIterator($this->_RFPfad);
            foreach ($RFDateien AS $Datei) {
                if (substr($Datei, 0, strlen($RueckfuehrungsKZ)) == $RueckfuehrungsKZ) {
                    if ($this->_Debug) {
                        echo $DebugPrefix . ' Datei: ' . $Datei . PHP_EOL;
                    }

                    $Filiale = substr($Datei, strlen($RueckfuehrungsKZ), 4);

                    $con = awisLogon();

                    // Pruefsumme der Datei pr�fen
                    $Pruefsumme = md5_file($this->_RFPfad . '/' . $Datei);

                    $BindeVariablen = array();
                    $BindeVariablen['var_N0_fil_id'] = $Filiale;
                    $BindeVariablen['var_T_rpf_pruefsumme'] = $Pruefsumme;

                    $SQL = 'SELECT * FROM RueckfuehrungsProtokollFiliale';
                    $SQL .= ' WHERE RPF_FIL_ID = :var_N0_fil_id';
                    $SQL .= ' AND RPF_Pruefsumme= :var_T_rpf_pruefsumme';

                    $rsRPF = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    if ($awisRSZeilen != 0) {
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...' . PHP_EOL;
                        }
                        $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_FEHLER');
                        if ($EMail != '') {
                            $EMail = explode(';', $EMail);
                            $this->_AWISParams->EMail($EMail,
                                'MDE FEHLER - RUECKFUEHRUNG (FILIALE) - ' . $Datei . ' BEREITS EINGELESEN',
                                'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...', 3);
                        }
                        syslog(LOG_WARNING,
                            'MDE - Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...');
                        if (is_file($this->_RFPfad . '/~' . $Datei)) {
                            unlink($this->_RFPfad . '/~' . $Datei);
                        }
                        rename($this->_RFPfad . '/' . $Datei, $this->_RFPfad . '/~' . $Datei);
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' Datei wurde auf ~' . $Datei . ' umbenannt' . PHP_EOL;
                        }

                        continue;
                    }

                    //Datei komplett lesen
                    $Daten = file($this->_RFPfad . '/' . $Datei, FILE_IGNORE_NEW_LINES);
                    foreach ($Daten AS $Zeile) {
                        $Felder = explode(';', $Zeile);

                        if ($Felder[0] == '') {
                            continue;            // Leerzeilen ueberspringen
                        }

                        if ($Felder[2] == '')        // Keine A.T.U Nummer -> ignorieren
                        {
                            syslog(LOG_WARNING, 'MDE Warnung: Keine A.T.U Nummer im Datensatz');
                            continue;
                        } else //Pr�fen, ob g�ltige ATUNR
                        {
                            $BindeVariablen = array();
                            $BindeVariablen['var_T_ast_atunr'] = $Felder[2];

                            $SQL = ' SELECT AST_ATUNR ';
                            $SQL .= ' FROM Artikelstamm ';
                            $SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
                            $SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';

                            $rsAST = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                            if ($awisRSZeilen == 0) {
                                syslog(LOG_WARNING, 'MDE Warnung: Keine g�ltige A.T.U Nummer');
                                continue;
                            }
                        }

                        if ($Felder[4] == '') {
                            syslog(LOG_WARNING, 'MDE Warnung: Keine Boxart im Datensatz');
                            continue;
                        } else {
                            if ($Felder[4] == 'GB') {
                                $BoxArt = 1;
                            } elseif ($Felder[4] == 'RP') {
                                $BoxArt = 2;
                            } elseif ($Felder[4] == 'ST') {
                                $BoxArt = 3;
                            }
                        }


                        //Aktuelle R�ckf�hrung ermitteln
                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_fil_id'] = $Filiale;
                        $BindeVariablen['var_N0_rfk_boxart'] = $BoxArt;

                        $SQL = 'SELECT RFK_KEY';
                        $SQL .= ' FROM Rueckfuehrungsfilialmengenkopf';
                        $SQL .= ' LEFT JOIN Rueckfuehrungsstatus on RFS_KEY = RFK_RFS_KEY';
                        $SQL .= ' WHERE RFK_FIL_ID=:var_N0_fil_id';
                        $SQL .= ' AND RFS_STATUS = 30';
                        $SQL .= ' AND RFK_BOXART = :var_N0_rfk_boxart';

                        if (trim($Felder[5]) != '') {
                            $BindeVariablen['var_T_rfk_boxean'] = trim($Felder[5]);

                            $SQL .= ' AND RFK_BOXEAN = :var_T_rfk_boxean';
                        } else {
                            $SQL .= ' AND RFK_BOXEAN is null';
                        }

                        //echo $SQL;

                        $rsRFK = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        if ($awisRSZeilen == 0)//Keine Daten
                        {
                            //Neuen R�ckf�hrungskopf anlegen
                            $rsRFS = awisOpenRecordset($con,
                                'SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=30');
                            $Status = $rsRFS['RFS_KEY'][0];

                            $BindeVariablen = array();
                            $BindeVariablen['var_N0_fil_id'] = $Felder[0];

                            $rsLFDNR = awisOpenRecordset($con,
                                'SELECT NVL(MAX(RFK_LFDNR),0) as MAXLFDNR FROM RUECKFUEHRUNGSFILIALMENGENKOPF WHERE RFK_FIL_ID=:var_N0_fil_id',
                                true, false, $BindeVariablen);
                            $LFDNR = $rsLFDNR['MAXLFDNR'][0] + 1;

                            $BindeVariablen = array();
                            $BindeVariablen['var_N0_rfk_fil_id'] = awis_FeldInhaltFormat('Z', $Felder[0], false);
                            $BindeVariablen['var_N0_rfk_lfdnr'] = awis_FeldInhaltFormat('Z', $LFDNR, true);
                            $BindeVariablen['var_N0_rfk_rfs_key'] = awis_FeldInhaltFormat('Z', $Status, true);
                            $BindeVariablen['var_N0_rfk_boxart'] = awis_FeldInhaltFormat('Z', $BoxArt, false);
                            $BindeVariablen['var_T_rfk_boxean'] = trim($Felder[5]);

                            $SQL = 'INSERT INTO RueckFuehrungsFilialMengenKopf';
                            $SQL .= '(RFK_FIL_ID, RFK_LFDNR, RFK_RFS_KEY, RFK_BOXART, RFK_BOXEAN';
                            $SQL .= ',RFK_USER, RFK_USERDAT';
                            $SQL .= ')VALUES (:var_N0_rfk_fil_id,:var_N0_rfk_lfdnr,:var_N0_rfk_rfs_key,:var_N0_rfk_boxart,:var_T_rfk_boxean';
                            $SQL .= ',\'' . getenv('HOST') . '\'';
                            $SQL .= ',SYSDATE)';

                            if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_RFPfad . '/' . $Datei, $this->_RFPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING,
                                    'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL . '-' . $Felder[0] . '-');
                                if ($this->_Debug) {
                                    echo $DebugPrefix . ' MDE - Fehler beim Insert der Rueckfuehrungsdaten' . PHP_EOL;
                                    echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                                }
                                throw new Exception('Fehler beim Speichern Kopf der Rueckfuehrungsdaten:' . $SQL . '-' . $Felder[0] . '-',
                                    17);
                            }

                            $SQL = 'SELECT seq_RFK_KEY.CurrVal AS KEY FROM DUAL';
                            $rsKey = awisOpenRecordset($con, $SQL);
                            $Rueckfuehrungkey = $rsKey['KEY'][0];

                        } else {
                            $Rueckfuehrungkey = $rsRFK['RFK_KEY'][0];
                        }

                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_fil_id'] = $Filiale;
                        $BindeVariablen['var_N0_rff_rfk_key'] = $Rueckfuehrungkey;
                        $BindeVariablen['var_T_ast_atunr'] = $Felder[2];

                        $SQL = 'SELECT RFK_KEY, RFF_KEY, RFF_AST_ATUNR';
                        $SQL .= ' FROM Rueckfuehrungsfilialmengenkopf';
                        $SQL .= ' LEFT JOIN Rueckfuehrungsfilialmengenpos on RFF_RFK_KEY = RFK_KEY';
                        $SQL .= ' LEFT JOIN Rueckfuehrungsstatus on RFS_KEY = RFK_RFS_KEY';
                        $SQL .= ' WHERE RFK_FIL_ID=:var_N0_fil_id';
                        $SQL .= ' AND RFF_RFK_KEY=:var_N0_rff_rfk_key';
                        $SQL .= ' AND RFF_AST_ATUNR=:var_T_ast_atunr';
                        $SQL .= ' AND RFS_STATUS = 30';

                        $rsRFF = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        if ($awisRSZeilen == 0)        // Keine Daten
                        {
                            $BindeVariablen = array();
                            $BindeVariablen['var_T_rff_ast_atunr'] = str_replace("'", "",
                                awis_FeldInhaltFormat('T', $Felder[2], false));
                            $BindeVariablen['var_N0_rff_mengefiliale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_N0_rff_mengescanfiliale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_D_rff_datumscanfiliale'] = str_replace("'", "",
                                awis_FeldInhaltFormat('D', $Felder[1]));
                            $BindeVariablen['var_N0_rff_rfk_key'] = $Rueckfuehrungkey;

                            $SQL = 'INSERT INTO RueckfuehrungsfilialmengenPos';
                            $SQL .= '(RFF_AST_ATUNR,RFF_MENGEFILIALE,RFF_MENGESCANFILIALE,RFF_DATUMSCANFILIALE';
                            $SQL .= ',RFF_RFK_KEY,RFF_USER,RFF_USERDAT)';
                            $SQL .= 'VALUES(:var_T_rff_ast_atunr, :var_N0_rff_mengefiliale, :var_N0_rff_mengescanfiliale, :var_D_rff_datumscanfiliale, :var_N0_rff_rfk_key';
                            $SQL .= ',\'' . getenv('HOST') . '\'';
                            $SQL .= ',SYSDATE';
                            $SQL .= ')';

                            if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_RFPfad . '/' . $Datei, $this->_RFPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING,
                                    'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL . '-' . $Felder[0] . '-');
                                if ($this->_Debug) {
                                    echo $DebugPrefix . ' MDE - Fehler beim Insert POS der Rueckfuehrungsdaten' . PHP_EOL;
                                    echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                                }
                                throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL . '-' . $Felder[0] . '-',
                                    17);
                            }
                        } else                        // Aktualisieren
                        {
                            $BindeVariablen = array();
                            $BindeVariablen['var_N0_rff_mengefiliale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_N0_rff_mengescanfiliale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_D_rff_datumscanfiliale'] = str_replace("'", "",
                                awis_FeldInhaltFormat('D', $Felder[1]));
                            $BindeVariablen['var_N0_rff_key'] = '0' . $rsRFF['RFF_KEY'][0];

                            $SQL = 'UPDATE RueckfuehrungsfilialmengenPos';
                            $SQL .= ' SET RFF_MengeFiliale=RFF_MengeFiliale+:var_N0_rff_mengefiliale';
                            $SQL .= ' ,RFF_MengeScanFiliale=RFF_MengeScanFiliale+:var_N0_rff_mengescanfiliale';
                            $SQL .= ' ,RFF_DatumScanFiliale=:var_D_rff_datumscanfiliale';
                            $SQL .= ',RFF_USER=\'' . getenv('HOST') . '\'';
                            $SQL .= ',RFF_USERDAT=SYSDATE';
                            $SQL .= ' WHERE RFF_KEY=:var_N0_rff_key';
                            if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_RFPfad . '/' . $Datei, $this->_RFPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL);
                                if ($this->_Debug) {
                                    echo $DebugPrefix . ' MDE - Fehler beim Update POS der Rueckfuehrungsdaten' . PHP_EOL;
                                    echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                                }
                                throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL, 16);
                            }
                        }
                    }

                    // Erfolg schreiben
                    $BindeVariablen = array();
                    $BindeVariablen['var_T_rpf_datei'] = str_replace("'", "",
                        awis_FeldInhaltFormat('T', $Datei, false));
                    $BindeVariablen['var_N0_rpf_rfk_key'] = $Rueckfuehrungkey;
                    $BindeVariablen['var_T_rpf_pruefsumme'] = str_replace("'", "",
                        awis_FeldInhaltFormat('T', $Pruefsumme, false));
                    $BindeVariablen['var_N0_rpf_fil_id'] = awis_FeldInhaltFormat('Z', $Filiale, false);

                    $SQL = 'INSERT INTO RueckfuehrungsProtokollFiliale';
                    $SQL .= '(RPF_Datei,RPF_Zeit,RPF_RFK_KEY,RPF_Pruefsumme,RPF_FIL_ID,RPF_Ergebnis,RPF_Meldung,RPF_User,RPF_UserDat)';
                    $SQL .= 'VALUES(:var_T_rpf_datei,SYSDATE,:var_N0_rpf_rfk_key,:var_T_rpf_pruefsumme,:var_N0_rpf_fil_id';
                    $SQL .= ',0,null';
                    $SQL .= ',\'' . getenv('HOST') . '\'';
                    $SQL .= ',SYSDATE)';

                    if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                        $DATUM = date("YmdHis");
                        rename($this->_RFPfad . '/' . $Datei, $this->_RFPfad . '/~' . $DATUM . $Datei);
                        syslog(LOG_WARNING, 'MDE - Fehler beim Speichern des Protokolls:' . $SQL);
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' MDE - Fehler beim Speichern des Protokolls' . PHP_EOL;
                            echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                        }
                        throw new Exception('Fehler beim Speichern des Protokolls:' . $SQL, 15);
                    }

                    // Datei umbenennen
                    if (is_file($this->_RFPfad . '/_' . $Datei)) {
                        unlink($this->_RFPfad . '/_' . $Datei);
                    }
                    rename($this->_RFPfad . '/' . $Datei, $this->_RFPfad . '/_' . $Datei);
                    if ($this->_Debug) {
                        echo $DebugPrefix . ' Datei wurde auf _' . $Datei . ' umbenannt' . PHP_EOL;
                    }

                    syslog(LOG_INFO, 'MDE - Datei ' . $Datei . ' erfolgreich eingelesen.');
                }
            }

        } catch (Exception $ex) {
            if ($ex->getCode() == 1) {

            }
            throw new Exception('Fehler beim Lesen der Rueckfuehrungs-Daten: ' . $ex->getMessage() . ' - ' . $ex->getCode(),
                12);
        }

    }

    /**
     * Funktion sucht die Rueckfuehrungsdaten von der Zentrale und traegt sie in die Datenbank ein
     *
     */
    public function LeseRueckfuehrungZentrale()
    {
        global $awisRSZeilen;
        try {
            $RueckfuehrungZentraleKZ = $this->_AWISParams->LeseAWISParameter('awismde.conf',
                'ERWEITERUNG_RUECKFUEHRUNGZEN');
            if ($RueckfuehrungZentraleKZ == '') {
                $RueckfuehrungZentraleKZ = 'RU';
            }
            if ($this->_Debug) {
                $DebugPrefix = $RueckfuehrungZentraleKZ . 'Z' . date("YmdHis");
                echo $DebugPrefix . ' Pr�fung auf R�ckf�hrung ZENTRALE beginnt' . PHP_EOL;
            }

            if(!$this->PruefePID('RFFZ')){
                if ($this->_Debug) {
                    echo $DebugPrefix . ' Ich beende mich, da ich bereits laufe. ' . PHP_EOL;
                }
                return false;
            }

            // Alle Dateien vom Verzeichnis lesen
            $RZDateien = new DirectoryIterator($this->_RZPfad);
            foreach ($RZDateien AS $Datei) {
                if (substr($Datei, 0, strlen($RueckfuehrungZentraleKZ)) == $RueckfuehrungZentraleKZ) {
                    //PG 04.06.2019: Datei sofort umbennenen, da sie zwischenzeitlich evtl. �berschrieben wird.
                    $DateinameNeu = microtime() . '_' . $Datei;
                    rename($this->_RZPfad . '/' . $Datei, $this->_RZPfad . '/' . $DateinameNeu);
                    $Datei = $DateinameNeu;
                    if ($this->_Debug) {
                        echo $DebugPrefix . ' Datei: ' . $Datei . PHP_EOL;
                    }

                    $con = awisLogon();

                    // Pruefsumme der Datei pr�fen
                    $Pruefsumme = md5_file($this->_RZPfad . '/' . $Datei);

                    $BindeVariablen = array();
                    $BindeVariablen['var_T_rpz_pruefsumme'] = $Pruefsumme;

                    $SQL = 'SELECT * FROM RueckfuehrungsProtokollZen';
                    $SQL .= ' WHERE RPZ_Pruefsumme=:var_T_rpz_pruefsumme';

                    $rsRPZ = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                    if ($awisRSZeilen != 0) {
                        if ($this->_Debug) {
                            echo $DebugPrefix . 'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...' . PHP_EOL;
                        }
                        $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_FEHLER');
                        if ($EMail != '') {
                            $EMail = explode(';', $EMail);
                            $this->_AWISParams->EMail($EMail,
                                'MDE FEHLER - RUECKFUEHRUNG (ZENTRALE) - ' . $Datei . ' BEREITS EINGELESEN',
                                'Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...', 3);

                        }
                        syslog(LOG_WARNING,
                            'MDE - Datei ' . $Datei . ' wurde bereits eingelesen. Wird uebersprungen...');
                        if (is_file($this->_RZPfad . '/~' . $Datei)) {
                            unlink($this->_RZPfad . '/~' . $Datei);
                        }
                        rename($this->_RZPfad . '/' . $Datei, $this->_RZPfad . '/~' . $Datei);
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' Datei wurde auf ~' . $Datei . ' umbenannt' . PHP_EOL;
                        }

                        continue;
                    }


                    //Datei komplett lesen
                    $Daten = file($this->_RZPfad . '/' . $Datei, FILE_IGNORE_NEW_LINES);
                    $Fehler = 0; //Fehlerz�hler bei Dateiverarbeitung
                    foreach ($Daten AS $Zeile) {
                        $Zeile = trim($Zeile);
                        if ($Zeile == '') {
                            continue;
                        }

                        $Felder = explode(';', $Zeile);

                        if ($Felder[0] == '') {
                            continue;            // Leerzeilen ueberspringen
                        }

                        if ($Felder[2] == '')        // Keine A.T.U Nummer -> ignorieren
                        {
                            syslog(LOG_WARNING, 'MDE Warnung: Keine A.T.U Nummer im Datensatz');
                            continue;
                        } else //Pr�fen, ob g�ltige ATUNR
                        {
                            $BindeVariablen = array();
                            $BindeVariablen['var_T_ast_atunr'] = $Felder[2];

                            $SQL = ' SELECT AST_ATUNR ';
                            $SQL .= ' FROM Artikelstamm ';
                            $SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
                            $SQL .= ' AND BITAND(AST_IMQ_ID,2)=2';

                            $rsAST = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                            if ($awisRSZeilen == 0) {
                                syslog(LOG_WARNING, 'MDE Warnung: Keine g�ltige A.T.U Nummer');
                                continue;
                            }
                        }

                        $Rueckfuehrungskey = '';
                        $Filiale = '';

                        //Aktuelle R�ckf�hrung ermitteln
                        $BindeVariablen = array();
                        $BindeVariablen['var_rfk_rueckfuehrungsnr'] = $Felder[0];

                        $SQL = 'SELECT RFK_KEY, RFK_FIL_ID';
                        $SQL .= ' FROM Rueckfuehrungsfilialmengenkopf';
                        $SQL .= ' LEFT JOIN Rueckfuehrungsstatus on RFS_KEY = RFK_RFS_KEY';
                        $SQL .= ' WHERE RFK_RUECKFUEHRUNGSNR=:var_rfk_rueckfuehrungsnr';
                        $SQL .= ' AND RFS_STATUS = 31';

                        $rsRFK = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        if ($awisRSZeilen == 0)//Keine Daten
                        {
                            $Fehler++;
                            syslog(LOG_WARNING,
                                'MDE - Zu der R�ckf�hrungsnummer ' . $Felder[0] . ' liegt keine R�ckf�hrung mit dem Status (abgeschlossen Filiale) vor.');
                            $EMail = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'MAIL_ADRESSEN_INFO_RF');
                            if ($EMail != '') {
                                $EMail = explode(';', $EMail);
                                $this->_AWISParams->EMail($EMail,
                                    'MDE FEHLER - RUECKFUEHRUNG (ZENTRALE) - ' . $Felder[0],
                                    'Zu der R�ckf�hrungsnummer ' . $Felder[0] . ', ATUNR:' . $Felder[2] . ', Menge: ' . awis_FeldInhaltFormat('N',
                                        $Felder[3]) . ' liegt keine R�ckf�hrung mit dem Status (abgeschlossen Filiale) vor',
                                    2);
                            }
                            if ($this->_Debug) {
                                echo $DebugPrefix . 'MDE - Zu der R�ckf�hrungsnummer ' . $Felder[0] . ' liegt keine R�ckf�hrung mit dem Status (abgeschlossen Filiale) vor.' . PHP_EOL;
                            }
                            continue;
                        } else {
                            $Rueckfuehrungskey = $rsRFK['RFK_KEY'][0];
                            $Filiale = $rsRFK['RFK_FIL_ID'][0];
                        }

                        //Pr�fen, ob die ATUNR in der R�ckf�rhug vorhanden ist
                        $BindeVariablen = array();
                        $BindeVariablen['var_N0_rff_rfk_key'] = $Rueckfuehrungskey;
                        $BindeVariablen['var_T_ast_atunr'] = $Felder[2] . '';

                        $SQL = 'SELECT RFF_KEY';
                        $SQL .= ' FROM Rueckfuehrungsfilialmengenpos';
                        $SQL .= ' WHERE RFF_RFK_KEY=:var_N0_rff_rfk_key';
                        $SQL .= ' AND RFF_AST_ATUNR=:var_T_ast_atunr';

                        $rsRFF = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        if ($awisRSZeilen == 0)        // Keine Daten
                        {

                            $BindeVariablen = array();
                            $BindeVariablen['var_T_rff_ast_atunr'] = str_replace("'", "",
                                awis_FeldInhaltFormat('T', $Felder[2], false));


                            $SQL = 'SELECT NVL ((SELECT RUECKLIEFKZ FROM AWIS.V_ARTIKEL_RUECKLIEFKZ WHERE GUELTIG = 0 AND AST_ATUNR = :VAR_T_RFF_AST_ATUNR),\' \') AS RUECKLIEFKZ FROM DUAL';

                            $rsRFT = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                            $BindeVariablen = array();
                            $BindeVariablen['var_T_rff_ast_atunr'] = str_replace("'", "",
                                awis_FeldInhaltFormat('T', $Felder[2], false));
                            $BindeVariablen['var_N0_rff_mengezentrale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_N0_rff_mengescanzentrale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_D_rff_datumscanzentrale'] = str_replace("'", "",
                                awis_FeldInhaltFormat('D', $Felder[1]));
                            $BindeVariablen['var_T_rff_rueckliefkz'] = $rsRFT['RUECKLIEFKZ'][0];
                            $BindeVariablen['var_N0_rff_rfk_key'] = $Rueckfuehrungskey;

                            $SQL = 'INSERT INTO Rueckfuehrungsfilialmengenpos';
                            $SQL .= '(RFF_AST_ATUNR,RFF_MENGEZENTRALE,RFF_MENGESCANZENTRALE,RFF_DATUMSCANZENTRALE,RFF_RFK_KEY,RFF_RUECKLIEFKZ';
                            $SQL .= ',RFF_USER,RFF_USERDAT)';
                            $SQL .= 'VALUES(:var_T_rff_ast_atunr,:var_N0_rff_mengezentrale,:var_N0_rff_mengescanzentrale,:var_D_rff_datumscanzentrale,:var_N0_rff_rfk_key,:var_T_rff_rueckliefkz';
                            $SQL .= ',\'' . getenv('HOST') . '\'';
                            $SQL .= ',SYSDATE)';

                            if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_RZfad . '/' . $Datei, $this->_RZPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING,
                                    'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL . '-' . $Felder[0] . '-');
                                if ($this->_Debug) {
                                    echo $DebugPrefix . ' MDE - Fehler beim Insert POS der Rueckfuehrungsdaten' . PHP_EOL;
                                    echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                                }
                                throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL . '-' . $Felder[0] . '-',
                                    17);
                            }
                        } else                        // Aktualisieren
                        {

                            $BindeVariablen = array();
                            $BindeVariablen['var_T_rff_ast_atunr'] = str_replace("'", "",
                                awis_FeldInhaltFormat('T', $Felder[2], false));

                            //A-027052: R�ckf�hrungskennzeichen soll nun beim zentralen aktualisiert werden.
                            $SQL = 'SELECT NVL ((SELECT RUECKLIEFKZ FROM AWIS.V_ARTIKEL_RUECKLIEFKZ WHERE GUELTIG = 0 AND AST_ATUNR = :VAR_T_RFF_AST_ATUNR),\' \') AS RUECKLIEFKZ FROM DUAL';

                            $rsRFT = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

                            $BindeVariablen = array();
                            $BindeVariablen['var_N0_rff_mengezentrale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_N0_rff_mengescanzentrale'] = awis_FeldInhaltFormat('N', $Felder[3]);
                            $BindeVariablen['var_D_rff_datumscanzentrale'] = str_replace("'", "",
                                awis_FeldInhaltFormat('D', $Felder[1]));
                            $BindeVariablen['var_N0_rff_key'] = '0' . $rsRFF['RFF_KEY'][0];
                            $BindeVariablen['var_T_rff_rueckliefkz'] = $rsRFT['RUECKLIEFKZ'][0];


                            $SQL = 'UPDATE Rueckfuehrungsfilialmengenpos';
                            $SQL .= ' SET RFF_MengeZentrale=RFF_MengeZentrale+:var_N0_rff_mengezentrale';
                            $SQL .= ' ,RFF_MengeScanZentrale=RFF_MengeScanZentrale+:var_N0_rff_mengescanzentrale';
                            $SQL .= ' ,RFF_DatumScanZentrale=:var_D_rff_datumscanzentrale';
                            $SQL .= ' ,RFF_RUECKLIEFKZ=:var_T_rff_rueckliefkz';
                            $SQL .= ',RFF_USER=\'' . getenv('HOST') . '\'';
                            $SQL .= ',RFF_USERDAT=SYSDATE';
                            $SQL .= ' WHERE RFF_KEY=:var_N0_rff_key';

                            if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                                $DATUM = date("YmdHis");
                                rename($this->_RZPfad . '/' . $Datei, $this->_RZPfad . '/~' . $DATUM . $Datei);
                                syslog(LOG_WARNING, 'MDE - Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL);
                                if ($this->_Debug) {
                                    echo $DebugPrefix . ' MDE - Fehler beim Update POS der Rueckfuehrungsdaten' . PHP_EOL;
                                    echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                                }
                                throw new Exception('Fehler beim Speichern der Rueckfuehrungsdaten:' . $SQL, 16);
                            }
                        }
                    }

                    if ($Fehler <> 0) {
                        $DATUM = date("YmdHis");
                        copy($this->_RZPfad . '/' . $Datei, $this->_RZPfad . '/Fehler/~' . $DATUM . $Datei);
                        echo $DebugPrefix . ' Datei wurde auf /Fehler/~' . $DATUM . $Datei . ' kopiert' . PHP_EOL;
                    }

                    // Erfolg schreiben
                    $BindeVariablen = array();
                    $BindeVariablen['var_T_rpz_datei'] = str_replace("'", "",
                        awis_FeldInhaltFormat('T', $Datei, false));
                    $BindeVariablen['var_N0_rpz_rfk_key'] = awis_FeldInhaltFormat('Z', $Rueckfuehrungskey, true);
                    $BindeVariablen['var_T_rpz_pruefsumme'] = str_replace("'", "",
                        awis_FeldInhaltFormat('T', $Pruefsumme, false));
                    $BindeVariablen['var_N0_rpz_fil_id'] = awis_FeldInhaltFormat('Z', $Filiale, true);

                    $SQL = 'INSERT INTO RueckfuehrungsProtokollZen';
                    $SQL .= '(RPZ_Datei,RPZ_Zeit,RPZ_RFK_KEY,RPZ_Pruefsumme,RPZ_FIL_ID,RPZ_Ergebnis,RPZ_Meldung,RPZ_User,RPZ_UserDat)';
                    $SQL .= 'VALUES(:var_T_rpz_datei, SYSDATE, :var_N0_rpz_rfk_key, :var_T_rpz_pruefsumme, :var_N0_rpz_fil_id';
                    $SQL .= ',0,null';
                    $SQL .= ',\'' . getenv('HOST') . '\'';
                    $SQL .= ',SYSDATE)';

                    if (awisExecute($con, $SQL, true, false, 0, $BindeVariablen) === false) {
                        $DATUM = date("YmdHis");
                        rename($this->_RZPfad . '/' . $Datei, $this->_RZPfad . '/~' . $DATUM . $Datei);
                        syslog(LOG_WARNING, 'MDE - Fehler beim Speichern des Protokolls:' . $SQL);
                        if ($this->_Debug) {
                            echo $DebugPrefix . ' MDE - Fehler beim Speichern des Protokolls' . PHP_EOL;
                            echo $DebugPrefix . ' Datei wurde auf ~' . $DATUM . $Datei . ' umbenannt' . PHP_EOL;
                        }
                        throw new Exception('Fehler beim Speichern des Protokolls:' . $SQL, 15);
                    }

                    // Datei umbenennen
                    if (is_file($this->_RZPfad . '/_' . $Datei)) {
                        unlink($this->_RZPfad . '/_' . $Datei);
                    }
                    rename($this->_RZPfad . '/' . $Datei, $this->_RZPfad . '/_' . $Datei);
                    if ($this->_Debug) {
                        echo $DebugPrefix . ' Datei wurde auf _' . $Datei . ' umbenannt' . PHP_EOL;
                    }

                    syslog(LOG_INFO, 'MDE - Datei ' . $Datei . ' erfolgreich eingelesen.');
                }
            }
            if ($this->_Debug) {
                //echo  $DebugPrefix.' Pr�fung auf R�ckf�hrung ZENTRALE beendet'.PHP_EOL;
            }
        } catch (Exception $ex) {
            if ($ex->getCode() == 1) {

            }
            throw new Exception('Fehler beim Lesen der Rueckfuehrungs-Daten: ' . $ex->getMessage() . ' - ' . $ex->getCode(),
                12);
        }

    }


    public function HoleZetesDaten()
    {
        $CopyKuerzel = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'ZETES_COPY_KUERZEL');
        $CopyKuerzel = explode(',', $CopyKuerzel);

        $Quelle = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'PFAD_ZETES');
        $Ziel = $this->_AWISParams->LeseAWISParameter('awismde.conf', 'IDENTASS_Receive');

        foreach ($CopyKuerzel as $Kuerzel) {
            $KZQuelle = str_replace('#KUERZEL#', $Kuerzel, $Quelle);
            $KZZiel = str_replace('#KUERZEL#', $Kuerzel, $Ziel);

            foreach (scandir($KZQuelle) as $Datei) {

                if(substr(strtoupper($Datei),-3) == 'TXT'){
                    //Backuppen
                    copy($KZQuelle . "/" . $Datei, $KZQuelle.'/99-Archiv/'.$Datei );

                    $ZielDatei = $Datei;
                    if(substr($Datei,0,3)=='RAN'){
                        $ZielDatei = substr($Datei,0,8) . '.TXT';
                    }

                    if(!file_exists( $KZZiel . "/" . strtoupper($ZielDatei))){ //Nur wenn die alte Datei noch nicht verarbeitet ist.

                        $SHA1Quelle = sha1_file($KZQuelle . "/" . $Datei);

                        if(copy($KZQuelle . "/" . $Datei, $KZZiel . "/" . strtoupper($ZielDatei))){ //Kopieren Erfolgreich?
                            $SHA1Ziel = sha1_file( $KZZiel . "/" . strtoupper($ZielDatei));
                            if($SHA1Quelle==$SHA1Ziel){ //Pr�fsumme OK?
                                unlink($KZQuelle . "/" . $Datei);
                            }else{
                                $this->_AWISParams->EMail(array('shuttle@de.atu.eu'), 'MDE FEHLER - Zetes Abholung - ' . $Datei . ' Pr�fsumme nach kopieren unterschiedlich!', '', 3);
                                unlink($KZZiel . "/" . strtoupper($ZielDatei));
                            }

                        }else{
                            $this->_AWISParams->EMail(array('shuttle@de.atu.eu'), 'MDE FEHLER - Zetes Abholung - ' . $Datei . ' Konnte nicht kopieren!', '', 3);
                        }
                    }
                }
            }
        }
    }

    public function PruefePID($Typ){
        $AktuellePid = getmypid();

        $PidFile = '/daten/jobs/pid/mde_'. $Typ . '.pid';

        if(file_exists($PidFile)){
            $AltePid = file_get_contents($PidFile);
            //Wenn es eine andere Pid ist und der Prozess noch existiert
            if($AltePid != $AktuellePid and posix_kill($AltePid,0)){
                return false;
            }
        }

        file_put_contents($PidFile,$AktuellePid);

        return true;
    }

}

//************ ENDE KLASSE awisMDE **********************//

// Testroutine

if (isset($argv[2]) and $argv[2] == 'TEST') {
    echo PHP_EOL . '=======================================' . PHP_EOL;
    echo 'TEST Routine fuer awisMDE Klasse.' . PHP_EOL;
    echo '=======================================' . PHP_EOL;

    try {
        $MDE = new awisMDE(1, $argv[1]);

        $MDE->LeseLieferabgleich();
    } catch (Exception $ex) {

        echo PHP_EOL . 'awisMDE: Fehler: ' . $ex->getMessage() . PHP_EOL;

    }


    echo '=======================================' . PHP_EOL;
    echo 'TEST Routine abgeschlossen.' . PHP_EOL;
    echo '=======================================' . PHP_EOL;
}

?>
