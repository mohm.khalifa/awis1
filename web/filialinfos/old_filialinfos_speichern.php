<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$Form->DebugAusgabe(1,$_POST);

	$AWIS_KEY1 = $_POST['txtFIL_ID'];

	$Felder = $Form->NameInArray($_POST,'txtFIT_WERT_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);

	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);

			$FIT_ID = $FeldTeile[2];
			$FIF_KEY = $FeldTeile[3];
			$DatenTyp = $FeldTeile[4];
			$WertAlt = $DB->FeldInhaltFormat($DatenTyp,$_POST['old'.substr($Feld,3)]);
			$WertNeu = $DB->FeldInhaltFormat($DatenTyp,$_POST[$Feld]);
			$Speichern = true;

			if($FIF_KEY == '')
			{
				$SQL = 'SELECT * FROM Filialinfos WHERE FIF_FIT_ID = '.$DB->FeldInhaltFormat('N0',$FIT_ID,true).' AND FIF_FIL_ID = '.$DB->FeldInhaltFormat('N0',$_POST['txtFIL_ID'],true);
				$rsFIF = $DB->RecordSetOeffnen($SQL);
				if(!$rsFIF->EOF())
				{
					$Speichern = false;
				}
			}

			$DB->TransaktionBegin();
			if($Speichern AND $WertAlt!==$WertNeu)
			{
				if($FIF_KEY == '')			// Neuer Eintrag
				{
					$SQL = 'INSERT INTO Filialinfos';
					$SQL .= ' (FIF_FIL_ID, FIF_FIT_ID, FIF_WERT, FIF_IMQ_ID, FIF_USER, FIF_USERDAT)';
					$SQL .= ' VALUES(';
					$SQL .= ' '.$DB->FeldInhaltFormat('N0',$_POST['txtFIL_ID'],true);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$FIT_ID,true);
					$SQL .= ','.$WertNeu;
					$SQL .= ',4';
					$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
					$SQL .= ', sysdate)';
				}
				else
				{
					$SQL = 'SELECT FIT_PROTOKOLL FROM FilialInfostypen2 WHERE FIT_ID = '.$FIT_ID;
					$rsFIT = $DB->RecordSetOeffnen($SQL);
					if($rsFIT->FeldInhalt('FIT_PROTOKOLL')==1)
					{
						$SQL = 'INSERT INTO FilialInfos_H';
						$SQL .= '(H00_FIF_KEY, H00_WERT, H00_FIT_ID, H00_FIL_ID, H00_READONLY, H00_IMQ_ID';
						$SQL .= ', H00_USER, H00_USERDAT, H00_CMDTYPE)';
						$SQL .= ' SELECT FIF_KEY, FIF_WERT, FIF_FIT_ID, FIF_FIL_ID, FIF_READONLY, FIF_IMQ_ID';
						$SQL .= ', FIF_USER, FIF_USERDAT, \'U\'';
						$SQL .= ' FROM FilialInfos';
						$SQL .= ' WHERE FIF_KEY = '.$FIF_KEY;

						$DB->Ausfuehren($SQL,'',true);
					}

					$SQL = ' UPDATE Filialinfos';
					$SQL .= ' SET FIF_WERT = '.$WertNeu;
					$SQL .= ' , FIF_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
					$SQL .= ' , FIF_USERDAT = SYSDATE';
					$SQL .= ' WHERE FIF_KEY = '.$FIF_KEY;
					$SQL .= ' AND FIF_FIT_ID = '.$FIT_ID;
				}

				$DB->Ausfuehren($SQL,'',true);

			}
			$DB->TransaktionCommit();
		}
	}


	//*****************************************************************************
	//*
	//* Zukauflieferanten
	//*
	//*****************************************************************************
	$Felder = $Form->NameInArray($_POST, 'txtFLI_',1,1);

	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('FLI','FLI_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		$Fehler = '';
		$Pflichtfelder = array('FLI_LIE_NR','FLI_SORTIERUNG','FLI_GUELTIGAB','FLI_GUELTIGBIS');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]==='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FLI'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtFLI_KEY'])==0)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO FilialenLieferanten';
			$SQL .= '(FLI_LIE_NR,FLI_FIL_ID,FLI_GUELTIGAB,FLI_GUELTIGBIS,FLI_SORTIERUNG,FLI_LVK_KEY,FLI_LIEFERART';
			$SQL .= ',FLI_USER, FLI_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtFLI_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFLI_GUELTIGAB'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFLI_GUELTIGBIS'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_SORTIERUNG'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_LVK_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFLI_LIEFERART'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			$DB->Ausfuehren($SQL);

			$SQL = 'SELECT seq_FLI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsFLI = $DB->RecordSetOeffnen('SELECT * FROM FilialenLieferanten WHERE FLI_key=' . $_POST['txtFLI_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsFLI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsFLI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsFLI->FeldInfo($FeldName,'TypKZ'),$rsFLI->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsFLI->FeldInhalt('FLI_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE FilialenLieferanten SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', FLI_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', FLI_userdat=sysdate';
				$SQL .= ' WHERE FLI_key=0' . $_POST['txtFLI_KEY'] . '';
				$DB->Ausfuehren($SQL,'',true);

				$AWIS_KEY2 = $_POST['txtFLI_KEY'];
			}
		}
	}

	// Kundennummern in der Filiale f�r den Lieferanten
	
	$Felder = $Form->NameInArray($_POST, 'txtLKD_',1,1);

	if($Felder!='' AND $_POST['txtLKD_KUNDENNR']!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('LKD','LKD_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		$Fehler = '';
		$Pflichtfelder = array('LKD_LIE_NR','LKD_FIL_ID','LKD_KUNDENNR');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]==='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['LKD'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtLKD_KEY'])==0)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO LieferantenKundennummern';
			$SQL .= '(LKD_LIE_NR,LKD_FIL_ID,LKD_KUNDENNR,LKD_KENNWORT,LKD_ZUSATZ1,LKD_VERWENDUNG';
			$SQL .= ',LKD_USER, LKD_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtLKD_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_KUNDENNR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_KENNWORT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtLKD_ZUSATZ1'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtLKD_VERWENDUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			$DB->Ausfuehren($SQL,'',true);
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsLKD = $DB->RecordSetOeffnen('SELECT * FROM LieferantenKundennummern WHERE LKD_key=' . $_POST['txtLKD_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsLKD->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsLKD->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsLKD->FeldInfo($FeldName,'TypKZ'),$rsLKD->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsLKD->FeldInhalt('LKD_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE LieferantenKundennummern SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', LKD_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', LKD_userdat=sysdate';
				$SQL .= ' WHERE LKD_key=0' . $_POST['txtLKD_KEY'] . '';
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}

	//*****************************************************************************
	//*
	//* ATU Karten
	//*
	//*****************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST,'txtCGK_KONTAKT',1,1));
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,15));
				$SQL = 'UPDATE CRMGEWERBEKUNDEN';
				$SQL .= ' SET CGK_KONTAKT='.($_POST[$Feld]=='::bitte w�hlen::'?'null':intval($_POST[$Feld]));
				$SQL .= ' ,CGK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,CGK_UserDat=SYSDATE';
				$SQL .= ' ,CGK_UserDatFiliale=SYSDATE';
				$SQL .= ' WHERE CGK_KEY=0'.$Key.'';
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}

	$Felder = explode(';',$Form->NameInArray($_POST,'txtCGK_BEMERKUNG',1,1));
	$SQL = '';
	foreach($Felder AS $Feld)
	{
		if(isset($_POST[$Feld]))
		{
			if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
			{
				$Key = intval(substr($Feld,17));
				$SQL = 'UPDATE CRMGEWERBEKUNDEN';
				$SQL .= ' SET CGK_BEMERKUNG=\''.$_POST[$Feld].'\'';
				$SQL .= ' ,CGK_User=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ' ,CGK_UserDat=SYSDATE';
				$SQL .= ' ,CGK_UserDatFiliale=SYSDATE';
				$SQL .= ' WHERE CGK_KEY=0'.$Key.'';
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}

	//*****************************************************************************
	//* 20120430,ho
	//* �nderung Telefon/Telefax Fachbetriebe in Reiter BetreuungExtern
	//*
	//*****************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST,'txtFAB_TELEFON',1,1));
	$SQL = '';
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			if(isset($_POST[$Feld]))
			{
				if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
				{
					$Key = intval(substr($Feld,14));
					$SQL = 'UPDATE FACHBETRIEBE';
					$SQL .= ' SET FAB_TELEFON=' .$DB->FeldInhaltFormat('T',$_POST[$Feld]) .',';
					$SQL .= ' FAB_USER=\''.$AWISBenutzer->BenutzerName().'\',';
					$SQL .= ' FAB_USERDAT=SYSDATE';
					$SQL .= ' WHERE FAB_KEY=0'.$Key.'';
					$Form->DebugAusgabe(1,$SQL);
					$DB->Ausfuehren($SQL,'',true);
				}
			}
		}
	}
	
	$Felder = explode(';',$Form->NameInArray($_POST,'txtFAB_TELEFAX',1,1));
	$SQL = '';
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			if(isset($_POST[$Feld]))
			{
				if($_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
				{
					$Key = intval(substr($Feld,14));
					$SQL = 'UPDATE FACHBETRIEBE';
					$SQL .= ' SET FAB_TELEFAX=' .$DB->FeldInhaltFormat('T',$_POST[$Feld]) .',';
					$SQL .= ' FAB_USER=\''.$AWISBenutzer->BenutzerName().'\',';
					$SQL .= ' FAB_USERDAT=SYSDATE';
					$SQL .= ' WHERE FAB_KEY=0'.$Key.'';
					$Form->DebugAusgabe(1,$SQL);
					$DB->Ausfuehren($SQL,'',true);
				}
			}
		}
	}
	
	//*****************************************************************************
	//* 20120510,ho
	//* �nderung Zuordnung der Fachbetriebe im Reiter BetreuungExtern
	//*
	//*****************************************************************************
	$Felder = $Form->NameInArray($_POST, 'txtFFA_',1,1);

	if($Felder!='')
	{
		$Felder = explode(';',$Felder);
		$TextKonserven[]=array('FFA','FFA_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		$Fehler = '';
		$Pflichtfelder = array('FFA_GUELTIGAB','FFA_GUELTIGBIS');
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if($_POST['txt'.$Pflichtfeld]==='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FFA'][$Pflichtfeld].'<br>';
			}
		}
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtFFA_KEY'])==0)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO Fachbetriebefilialen ';
			$SQL .= '(FFA_FAB_KEY,FFA_FIL_ID,FFA_GUELTIGAB,FFA_GUELTIGBIS,';
			$SQL .= ' FFA_USER, FFA_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtFFA_FAB_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtFFA_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFFA_GUELTIGAB'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtFFA_GUELTIGBIS'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			$Form->DebugAusgabe(1,$SQL);
			$DB->Ausfuehren($SQL,'',true);
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsFFA = $DB->RecordSetOeffnen('SELECT * FROM Fachbetriebefilialen WHERE FFA_key=' . $_POST['txtFFA_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsFFA->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsFFA->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsFFA->FeldInfo($FeldName,'TypKZ'),$rsFFA->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsFFA->FeldInhalt('FFA_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Fachbetriebefilialen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', FFA_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', FFA_userdat=sysdate';
				$SQL .= ' WHERE FFA_key=0' . $_POST['txtFFA_KEY'] . '';
				$Form->DebugAusgabe(1,$SQL);
				$DB->Ausfuehren($SQL,'',true);
			}
		}
	}
	
}

catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>