<?php

global $Funktionen;

$Funktionen->RechteMeldung(0);
$Ansicht = 0;

$Funktionen->AWIS_KEY1 = 0;

$Funktionen->Form->SchreibeHTMLCode("<form method=post action=./filialkompetenzen_Main.php?cmdAktion=Details>");
$Funktionen->Form->Formular_Start();

if(isset($_POST['txtAWIS_KEY1'])){
    $Funktionen->AWIS_KEY1 = $_POST['txtAWIS_KEY1'];
} elseif(isset($_GET['FIK_KEY'])){
    $Funktionen->AWIS_KEY1 = $_GET['FIK_KEY'];
} else {
    $Funktionen->AWIS_KEY1 = '';
}

if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Bearb'])){ //L�schen der Kompetenz / Filial-DS
    include('./filialkompetenzen_loeschen.php');
} elseif(isset($_POST['cmdSpeichern_x'])) { //Beim �ndern von Daten
    include('./filialkompetenzen_speichern.php');
}

$SelectFelder = "FIK_BEZEICHNUNG, FIK_KEY";
$Group = " group by ".$SelectFelder;
if (isset($_POST['cmdDSNeu_x'])) { //Erstellung neues DS -> kein RS ben�tigt
    $Funktionen->AWIS_KEY1 = "-1";
} elseif (isset($_GET['FIK_KEY']) or isset($_POST['txtAWIS_KEY1']) or $Funktionen->AWIS_KEY1 <> 0) { //Wenn bereits in Einzel-DS-Ansicht/Nach Speichern

            $SQL = "Select * from filialkompetenzentypen where FIK_KEY = ". $Funktionen->DB->WertSetzen("FIK","Z",$_POST['txtAWIS_KEY1']);
            $rsCheck = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen("FIK"));
            if($rsCheck->AnzahlDatensaetze()){
                $Funktionen->AWIS_KEY1 = $_POST['txtAWIS_KEY1'];
    }

    $JOIN = " left join ( select * from filialinfos where fif_fit_id = 920 ) fi on fi.fif_wert = fkt.fik_key ";
    $Order = ' ORDER BY FIK_BEZEICHNUNG ASC';
    $Bedingung = " where FIK_KEY =" . $Funktionen->DB->WertSetzen('FIK', 'Z', $Funktionen->AWIS_KEY1);
    $SQL = "Select " . $SelectFelder . " from filialkompetenzentypen fkt " . $JOIN ." ". $Bedingung ." ". $Group. " ". $Order;
    $rsFIK = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('FIK'));
} else { //aus der Suche/direkt auf Detail-Seite

    if (isset($_POST['cmdSuche_x'])) {
        $Funktionen->Param['FIK_BEZEICHNUNG']=$_POST['sucFIK_BEZEICHNUNG'];
        $Funktionen->Param['FIL_ID']=$_POST['sucFIL_ID'];
        $Funktionen->Param['FIL_BEZ']=$_POST['sucFIL_BEZ'];
        $Funktionen->Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
    }
    if (!isset($_GET['SortList'])) {
        $Order = ' ORDER BY FIK_BEZEICHNUNG ASC';
    } else {
        $Order = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SortList']);
    }

    $JOIN = " left join ( select * from filialinfos where fif_fit_id = 920 ) fi on fi.fif_wert = fkt.fik_key ";
    $Bedingung = '';
    if($Funktionen->Param['FIK_BEZEICHNUNG']<>''){
        $Bedingung .= ' and upper(FIK_BEZEICHNUNG) ' .$Funktionen->DB->LikeOderIst('%'.$Funktionen->Param['FIK_BEZEICHNUNG'].'%',awisDatenbank::AWIS_LIKE_UPPER,'FIK').' ';
    }
    if($Funktionen->Param['FIL_ID']<>''){
        $Bedingung .= ' and FIF_FIL_ID = '. $Funktionen->DB->WertSetzen("FIK","Z",$Funktionen->Param['FIL_ID']).' ';
    }
    if($Funktionen->Param['FIL_BEZ']<>''){
        $Bedingung .= " and FIF_FIL_ID in ( ";
        $Bedingung .= " select fil_id from v_filialen where ";
        $Bedingung .= ' upper(FIL_BEZ) '. $Funktionen->DB->LikeOderIst($Funktionen->Param['FIL_BEZ'],awisDatenbank::AWIS_LIKE_UPPER,'FIK') ;
        $Bedingung .= ' or upper(FIL_ORT) '. $Funktionen->DB->LikeOderIst($Funktionen->Param['FIL_BEZ'],awisDatenbank::AWIS_LIKE_UPPER,'FIK') ;
        $Bedingung .= ' or upper(FIL_PLZ) '. $Funktionen->DB->LikeOderIst($Funktionen->Param['FIL_BEZ'],awisDatenbank::AWIS_LIKE_UPPER,'FIK');
        $Bedingung .= ' or upper(FIL_ORTSTEIL) '. $Funktionen->DB->LikeOderIst($Funktionen->Param['FIL_BEZ'],awisDatenbank::AWIS_LIKE_UPPER,'FIK');
        $Bedingung .= ' )';
    }

    if($Bedingung!= ''){
        $Bedingung = ' where '. substr($Bedingung, 4);
    }

    $SQL = "Select " . $SelectFelder . " from filialkompetenzentypen fkt " . $JOIN ." ". $Bedingung ." ". $Group ." ". $Order;
    $rsFIK = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('FIK'));
}

if($Funktionen->AWIS_KEY1 == "-1" or $rsFIK->AnzahlDatensaetze() == 1) { //Einzel-DS-Ansicht/Neuer DS/Sortierung der Liste bei Einzel-DS-Ansicht
    $NeuerDatensatz = true;
    if((isset($rsFIK) and $rsFIK->AnzahlDatensaetze() == 1)){
        if($Funktionen->AWIS_KEY1 == 0) {
            $rsFIK->DSErster();
            $Funktionen->AWIS_KEY1 = $rsFIK->FeldInhalt('FIK_KEY');
        }

        $SQL = "select * from filialkompetenzentypen where fik_key = ".$Funktionen->DB->WertSetzen('FIK','Z',$Funktionen->AWIS_KEY1);
        $rsFIK = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('FIK'));
        $NeuerDatensatz = false;
        $Funktionen->Form->Erstelle_HiddenFeld('AWIS_KEY1',$Funktionen->AWIS_KEY1);
    } else {
        $Funktionen->Form->Erstelle_HiddenFeld('AWIS_KEY1',"-1");
    }

    $Felder = array();
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./filialkompetenzen_Main.php?cmdAktion=Details accesskey=T title='" . $Funktionen->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    if(isset($rsFIK)){
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFIK->FeldInhalt('FIK_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFIK->FeldInhalt('FIK_USERDAT'));
    }
    $Funktionen->Form->InfoZeile($Felder);

    if($NeuerDatensatz and ($Funktionen->AWISBenutzer->HatDasRecht(660)& 4) == 4) { //Neuer Datensatz
        $Ansicht = -1;

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIK']['FIK_NAME'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("!FIK_BEZEICHNUNG", (isset($rsFIK) and $rsFIK->FeldInhalt('FIK_BEZEICHNUNG') <> "")?($rsFIK->FeldInhalt('FIK_BEZEICHNUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(660)& 4) == 4)?true:false);
        $Funktionen->Form->ZeileEnde();
        $Funktionen->AWISCursorPosition = "txtFIK_BEZEICHNUNG";

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIK']['FIK_BESCHREIBUNG'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("FIK_BESCHREIBUNG", (isset($rsFIK) and $rsFIK->FeldInhalt('FIK_BESCHREIBUNG') <> "")?($rsFIK->FeldInhalt('FIK_BESCHREIBUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(660)& 4) == 4)?true:false);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Trennzeile('O');
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $SQL = "SELECT FIL_ID, ( FIL_ID ||  ' | ' ||  FIL_BEZ ) AS FIL_BEZ FROM V_FILIALEN_AKTUELL order by FIL_ID asc";
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIK']['FIK_KOMPETENZFILIALEN'].":",200);
        $Funktionen->Form->Erstelle_MehrfachSelectFeld("FIK_KOMPETENZFILIALEN", array(), 500,true, $SQL,'','','','','','','',array(),'','AWIS','','',0);
        $Funktionen->Form->ZeileEnde();
    } else { //Alter Datensatz -> Ansehen/�ndern
        $Ansicht = 1;

        //Erstelle die PopUps zum L�schen von Filial-DS

        if(($Funktionen->AWISBenutzer->HatDasRecht(660)& 2) == 2) {
            $Schaltflaechen = array(array('/bilder/cmd_weiter.png', '', 'href', './filialkompetenzen_Main.php?cmdAktion=Details&FIK_KEY=' . $Funktionen->AWIS_KEY1 . '&Bearb=0'), array('/bilder/cmd_ds.png', 'close', 'close', 'Schlie�en'));
            $Funktionen->Form->PopupDialog($Funktionen->AWISSprachKonserven['Wort']['Achtung'] . "!", $Funktionen->AWISSprachKonserven['FIK']['FIK_POPUP_TEXT'] , $Schaltflaechen, awisFormular::POPUP_WARNUNG, '0');
        }

        //Erstelle das PopUp zum L�schen der Kompetenz

        if(($Funktionen->AWISBenutzer->HatDasRecht(660)& 8) == 8) {

            $SQL = "Select count(*) as ANZAHL from filialinfos where fif_fit_id = 920 and fif_wert = ".$Funktionen->DB->WertSetzen('FIK',"Z",$Funktionen->AWIS_KEY1);
            $rsANZFIL = $Funktionen->DB->RecordSetOeffnen($SQL,$Funktionen->DB->Bindevariablen("FIK"));

            $Inhalt = str_replace("#ANZ_FIL#", $rsANZFIL->FeldInhalt("ANZAHL"), $Funktionen->AWISSprachKonserven['FIK']['FIK_POPUP_LOESCHEN']);
            $Schaltflaechen = array(array('/bilder/cmd_weiter.png', 'cmdLoeschen', 'post', 'post'),array('/bilder/cmd_ds.png', 'close', 'close', 'Schlie�en'));
            $Funktionen->Form->PopupDialog($Funktionen->AWISSprachKonserven['Wort']['Achtung'] . "!", $Inhalt, $Schaltflaechen, awisFormular::POPUP_WARNUNG, 'LOESCHEN');

        }

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIK']['FIK_BEZEICHNUNG'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("!FIK_BEZEICHNUNG", (isset($rsFIK) and $rsFIK->FeldInhalt('FIK_BEZEICHNUNG') <> "")?($rsFIK->FeldInhalt('FIK_BEZEICHNUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(660)& 2) == 2)?true:false);
        $Funktionen->Form->ZeileEnde();
        $Funktionen->AWISCursorPosition = "txtFIK_BEZEICHNUNG";

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIK']['FIK_BESCHREIBUNG'].":",200);
        $Funktionen->Form->Erstelle_TextFeld("FIK_BESCHREIBUNG", (isset($rsFIK) and $rsFIK->FeldInhalt('FIK_BESCHREIBUNG') <> "")?($rsFIK->FeldInhalt('FIK_BESCHREIBUNG')): "",200, 500,(($Funktionen->AWISBenutzer->HatDasRecht(660)& 2) == 2)?true:false);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Trennzeile('O');
        $Funktionen->Form->ZeileEnde();

        //Filialen mit der Kompetenz

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['FIK']['FIK_KOMPETENZFILIALEN'].":",400);
        $Funktionen->Form->ZeileEnde();

        if(($Funktionen->AWISBenutzer->HatDasRecht(660)& 2) == 2) {
            $Funktionen->Form->ZeileStart('', 'FIK_KOMPETENZFILIALEN_DIV'); //Filialen hinzuf�gen -> automatisch ausgeblendet -> Einblendung durch hinzuf�gen-Button
            $SQL = "SELECT FIL_ID, ( FIL_ID ||  ' | ' ||  FIL_BEZ ) AS FIL_BEZ FROM V_FILIALEN_AKTUELL WHERE FIL_ID NOT IN (SELECT FIF_FIL_ID FROM filialinfos WHERE fif_fit_id = 920 and fif_wert = " . $Funktionen->DB->WertSetzen('FIK',"Z",$Funktionen->AWIS_KEY1) . ") ORDER BY FIL_ID ASC";
            $Funktionen->Form->Erstelle_MehrfachSelectFeld("FIK_KOMPETENZFILIALEN", array(), 1000, true, $SQL, '', '', '', '', '', '', '', $Funktionen->DB->Bindevariablen('FIK'), '', 'AWIS', '', '', 0);
            $Funktionen->Form->ZeileEnde();
        }

        $Funktionen->Form->ZeileStart(); //Kopfzeile der Tabelle
        if(($Funktionen->AWISBenutzer->HatDasRecht(660)& 2) == 2) {
            $Funktionen->Form->Erstelle_ListenBild('ohne', 'FIK_KOMPETENZFILIALEN_ADD', '', '/bilder/icon_new.png', $Funktionen->AWISSprachKonserven['FIK']['FIK_KOMPETENZFILIALEN_ADD'], -2, '', 16, 16, 19);
            $Funktionen->Form->Erstelle_ListenBild('script', 'Del_PopUp', $Funktionen->Form->PopupOeffnen("0"), '/bilder/icon_delete.png', '', -2, '', 16, 16, 19);
        }
        $Link = './filialkompetenzen_Main.php?cmdAktion=Details&FIK_KEY='.$Funktionen->AWIS_KEY1.'&Sort2=FIL_ID'.((isset($_GET['Sort2']) AND ($_GET['Sort2']=='FIL_ID'))?'~':'');
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['FIL']['FIL_ID'],250,'',$Link);
        $Link = './filialkompetenzen_Main.php?cmdAktion=Details&FIK_KEY='.$Funktionen->AWIS_KEY1.'&Sort2=FIL_BEZ'.((isset($_GET['Sort2']) AND ($_GET['Sort2']=='FIL_BEZ'))?'~':'');
        $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['FIL']['FIL_BEZ'],250,'',$Link);
        $Funktionen->Form->ZeileEnde();

        $SQL = "Select FIL_ID, FIL_BEZ from V_FILIALEN_AKTUELL where FIL_ID in ( select fif_fil_id from filialinfos where fif_fit_id = 920 and fif_wert = ".$Funktionen->DB->WertSetzen('FIL',"Z",$Funktionen->AWIS_KEY1). ") ";

        if (!isset($_GET['Sort2'])) {
            $Order = ' ORDER BY FIL_ID, FIL_BEZ ASC';
        } else {
            $Order = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort2']);
        }
        $SQL.= $Order;

        $rsFIL = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('FIL'));

        $DS = 0;    // f�r Hintergrundfarbumschaltung

        while(!$rsFIL->EOF()) //Alle Datens�tze einzeln ausgeben
        {
            $Funktionen->Form->ZeileStart();
            if((($Funktionen->AWISBenutzer->HatDasRecht(660)& 2) == 2)) {
                $Funktionen->Form->Erstelle_ListenFeld('Platzhalter', '', 0, 19, false, ($DS % 2));
                $Funktionen->Form->Erstelle_ListenLoeschPopUp('./filialkompetenzen_Main.php?cmdAktion=Details&FIK_KEY=' . $Funktionen->AWIS_KEY1 . '&Bearb=' . $rsFIL->FeldInhalt('FIL_ID'),$rsFIL->FeldInhalt('FIL_ID'), 19, '', '', ($DS % 2));
            }
            $Funktionen->Form->Erstelle_ListenFeld('FIL_ID',$rsFIL->FeldInhalt('FIL_ID'),200,250,false,($DS % 2));
            $Funktionen->Form->Erstelle_ListenFeld('FIL_BEZ',$rsFIL->FeldInhalt('FIL_BEZ'),200,250,false,($DS % 2));
            $Funktionen->Form->ZeileEnde();

            $DS++;
            $rsFIL->DSWeiter();
        }
        $Funktionen->LadeFilialenHinzufuegen();
    }

} elseif ($rsFIK->AnzahlDatensaetze() == 0) { //Ausgabe wenn kein Datensatz zur Suche gefunden wurde
    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['FIK']['FIK_KEINDS'],awisFormular::HINWEISTEXT_HINWEIS);
    $Funktionen->Form->ZeileEnde();
} else { //Listenansicht
    $Feldbreiten = array();
    $Feldbreiten['FIK_BEZEICHNUNG']=500;

    $Funktionen->Form->ZeileStart();
    $Link = './filialkompetenzen_Main.php?cmdAktion=Details&SortList=FIK_BEZEICHNUNG' . ((isset($_GET['SortList']) AND ($_GET['SortList'] == 'FIK_BEZEICHNUNG')) ? '~' : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['FIK']['FIK_BEZEICHNUNG'], $Feldbreiten['FIK_BEZEICHNUNG'], '', $Link);
    $Funktionen->Form->ZeileEnde();

    $DS = 0;    // f�r Hintergrundfarbumschaltung

    while (!$rsFIK->EOF()) {
        if($rsFIK->FeldInhalt('FIK_KEY') <> "") {
            $Link = "./filialkompetenzen_Main.php?cmdAktion=Details&FIK_KEY=" . $rsFIK->FeldInhalt('FIK_KEY');
            $Funktionen->Form->ZeileStart();
            $Funktionen->Form->Erstelle_ListenFeld('FIK_BEZEICHNUNG', $rsFIK->FeldInhalt('FIK_BEZEICHNUNG'), 0,  $Feldbreiten['FIK_BEZEICHNUNG'], false, ($DS % 2), '', $Link);
            $Funktionen->Form->ZeileEnde();
            $DS++;
        }
        $rsFIK->DSWeiter();
    }
}

$Funktionen->Form->Formular_Ende();

$Funktionen->Form->SchaltflaechenStart();
$Funktionen->Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
if((($Funktionen->AWISBenutzer->HatDasRecht(660)& 2) == 2 and $Ansicht == 1) or (($Funktionen->AWISBenutzer->HatDasRecht(660)& 4) == 4 and $Ansicht == -1)) {
    $Funktionen->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}
if(($Funktionen->AWISBenutzer->HatDasRecht(660)& 4) == 4 and $Ansicht <> "-1") {
    $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}
if(($Funktionen->AWISBenutzer->HatDasRecht(660)& 8) == 8 and $Ansicht == 1) {
    $Funktionen->Form->Schaltflaeche('script', 'cmdLoeschen', $Funktionen->Form->PopupOeffnen("LOESCHEN"), '/bilder/cmd_loeschen.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
}
$Funktionen->Form->SchaltflaechenEnde();
$Funktionen->Form->SchreibeHTMLCode("</form>");