<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisCSVSpooler.inc';
require_once 'awisWerkzeuge.inc';
require_once 'awisMailer.inc';

class awisCarglass
{
    private $DB;

    private $AWISBenutzer;

    private $DebugLevel;

    private $Exportpfad = '';

    private $Werkzeuge;

    const PFAD = '/win/svdbswi005/99_FTP/';
    //const PFAD = '/tmp/';

    const TZ = ';';

    public function __construct($Benutzer)
    {
        $this->AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();

        $this->Werkzeuge = new awisWerkzeuge();

        $Dateiname = 'Carglass_outbound_' . date('YmdHis') . '.csv';

        $this->Exportpfad .= self::PFAD . $Dateiname;
    }


    public function ExportCarglass()
    {
        $this->Log('Hole alle aktuellen Uebermittlungen aus Datenbank.', 1);
        $Ueberschrift = 'FILNR'. self::TZ . 'DATUM' . self::TZ . 'ZEITSTEMPEL';
        $Ueberschrift .= "\r\n";

        //Filiale, Datum, Anzahl Aufrufe
        $SQL = 'SELECT FILNR, DATUM, ZEITSTEMPEL';
        $SQL .= ' FROM(';
        $SQL .= ' SELECT cag_filnr as FILNR, ';
        $SQL .= ' TO_CHAR(cag_klick, \'dd.mm.yyyy\') as DATUM, ';
        $SQL .= ' cag_klick as ZEITSTEMPEL';
        $SQL .= ' FROM carglasskooperation';
        $SQL .= ' WHERE cag_klick >= TO_CHAR(sysdate, \'dd.mm.yyyy\'))';
        $SQL .= ' GROUP BY FILNR, DATUM, ZEITSTEMPEL';

        $rsCAG = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('CAG'));

        $this->Log($SQL, 1);

        if($rsCAG->AnzahlDatensaetze() > 0){
            $Datei = fopen($this->Exportpfad . '.tmp', 'w');
            $this->Log('Schreibe in CSV.', 1);
            fwrite($Datei, $Ueberschrift);
        }else{
            return true;
        }

        while (!$rsCAG->EOF()){
            $Zeile = $rsCAG->FeldInhalt('FILNR','T') . self::TZ;
            $Zeile .= $rsCAG->FeldInhalt('DATUM','T') . self::TZ;
            $Zeile .= $rsCAG->FeldInhalt('ZEITSTEMPEL', 'T');
            $Zeile .= "\r\n";

            fwrite($Datei,$Zeile);

            $rsCAG->DSWeiter();
        }

        fclose($Datei);

        $this->Log('Datei fertig.', 1);

        $this->Log('Stelle Datei bereit..', 1);
        if (is_file($this->Exportpfad)) {
            $this->Log('Alte Datei vorhanden, loesche..', 1);
            unlink($this->Exportpfad);
        }

        $this->Log('Bennene Tempdatei um.', 1);
        rename($this->Exportpfad . '.tmp', $this->Exportpfad);


        $this->Log('Habe alles erledigt.', 1);
    }

    public function DebugLevel($Level)
    {
        $this->DebugLevel = $Level;
    }

    public function Log($Text, $Level)
    {
        if ($this->DebugLevel >= $Level) {
            echo date('c') . ' ' . $Text . PHP_EOL;
        }
    }

}