<?php

global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven = array();
$TextKonserven[] = array('FGK', 'FGK_*');
$TextKonserven[] = array('Fehler', 'err_KeinWert');
$TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
$TextKonserven[] = array('Wort', 'geaendert_von');
$TextKonserven[] = array('Wort', 'geaendert_auf');
$TextKonserven[] = array('Meldung', 'DSVeraendert');
$TextKonserven[] = array('Meldung', 'EingabeWiederholen');
$TextKonserven[] = array('FKH', '*');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_Hilfe');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Liste', 'lst_JaNein');

try
{
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    //$Form->DebugAusgabe(1,$_REQUEST);

    if (isset($_POST['txtFKA_KEY']) != '')
    {

        $Fehler = '';
        $TXT_Speichern = array();

        $AWIS_KEY1 = $_POST['txtFKA_KEY'];
        $Felder = $Form->NameInArray($_POST, 'txtFKA_', 1, 1);

        $Pflichtfelder = array('FKA_BEMERKUNG');

        /* foreach($Pflichtfelder AS $Pflichtfeld)
          {
          if($_POST['txt'.$Pflichtfeld]=='')	// Freigabegrund muss bei manueller Freischaltung eingegeben werden!!!
          {
          //$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['FKA'][$Pflichtfeld].'<br>';
          $Fehler .= 'Bitte eine Begründung eingeben'.'<br>';
          }
          }

          if($Fehler!='')
          {
          die('<span class=HinweisText>'.$Fehler.'</span>');

          //-------------Ja - Nein Abfrage------------------

          }
          $FeldListe='';
          $SQL = '';
         */
        if ($_POST['oldFKA_AEMNR'] !== $_POST['txtFKA_AEMNR'])
        {
            if (floatval($_POST['txtFKA_KEY']) == 0)
            {
                
            }
            else
            {
                $SQL = 'Select * from FKASSENDATEN WHERE FKA_KEY=' . $_POST['txtFKA_KEY'];

                $rsUpdateKassendaten = $DB->RecordSetOeffnen($SQL);

                $aktDatum = date('d.m.Y H:i:s');

                $SQL = 'UPDATE FKASSENDATEN SET ';
                $SQL .= ' FKA_KENNUNG=' . $DB->FeldInhaltFormat('T', $rsUpdateKassendaten->FeldInhalt("FKA_KENNUNG"), false);
                $SQL .= ',FKA_FILID=' . $DB->FeldInhaltFormat('NO', $rsUpdateKassendaten->FeldInhalt("FKA_FILID"), false);
                $SQL .= ',FKA_DATUM=' . $DB->FeldInhaltFormat('D', $rsUpdateKassendaten->FeldInhalt("FKA_DATUM"), false);
                $SQL .= ',FKA_UHRZEIT=' . $DB->FeldInhaltFormat('T', $rsUpdateKassendaten->FeldInhalt("FKA_UHRZEIT"), false);
                $SQL .= ',FKA_BSA=' . $DB->FeldInhaltFormat('T', $rsUpdateKassendaten->FeldInhalt("FKA_BSA"), false);
                $SQL .= ',FKA_WANR=' . $DB->FeldInhaltFormat('T', $rsUpdateKassendaten->FeldInhalt("FKA_WANR"), false);
                $SQL .= ',FKA_AEMNR=' . $DB->FeldInhaltFormat('T', $_POST['txtFKA_AEMNR'], false);
                $SQL .= ',FKA_ATUNR=' . $DB->FeldInhaltFormat('T', $rsUpdateKassendaten->FeldInhalt("FKA_ATUNR"), false);
                $SQL .= ',FKA_MENGE=' . $DB->FeldInhaltFormat('N2', $rsUpdateKassendaten->FeldInhalt("FKA_MENGE"), false);
                $SQL .= ',FKA_BETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdateKassendaten->FeldInhalt("FKA_BETRAG"), false);
                $SQL .= ',FKA_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsUpdateKassendaten->FeldInhalt("FKA_KFZKENNZ"), false);
                $SQL .= ',FKA_BEMERKUNG=' . $DB->FeldInhaltFormat('T', "Vorherige Vorgangsnr:    " . $_POST['oldFKA_AEMNR'] . " " . $aktDatum, false);
                $SQL .= ',FKA_IMP_KEY=' . $DB->FeldInhaltFormat('NO', $rsUpdateKassendaten->FeldInhalt('FKA_IMP_KEY'), true);
                $SQL .= ',FKA_STATUS=' . $DB->FeldInhaltFormat('NO', $rsUpdateKassendaten->FeldInhalt('FKA_STATUS'), true);
                $SQL .= ',FKA_USER=' . $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName(), false);
                $SQL .= ',FKA_USERDAT=SYSDATE';
                $SQL .= ' WHERE FKA_KEY=' . $DB->FeldInhaltFormat('NO', $_POST["txtFKA_KEY"], false);

                $DB->Ausfuehren($SQL);
          
                //Firstglas - Nummer in der AEM - Datenbank drehen

                $SQL = "UPDATE versvorgangsstatus";
                $SQL .= " SET vvs_vorgangnr =" . $DB->FeldInhaltFormat('T', $_POST['txtFKA_AEMNR'], false);
                $SQL .= " WHERE vvs_filid =" . $DB->FeldInhaltFormat('NO', $rsUpdateKassendaten->FeldInhalt("FKA_FILID"), false);
                $SQL .= " AND vvs_wanr =" . $DB->FeldInhaltFormat('NO', $rsUpdateKassendaten->FeldInhalt("FKA_WANR"), false);
                $SQL .= " AND vvs_vcn_key = 100";
                $SQL .= " AND vvs_ven_key in (5,6,7,8,10,11,12,13)";
                $SQL .= " AND vvs_freigabe = 0";
                $SQL .= " AND vvs_schadenkz = 'E'";

                $DB->Ausfuehren($SQL);                 
                
            }
        }
    }//Ende FKA - KEY
}
catch (Exception $ex)
{
    //$Form->Fehler_Anzeigen('SYSTcherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>
