<?php
$Zeilenabschluss = chr(13).chr(10);

require_once 'awisMailer.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

$AWISBenutzer = awisBenutzer::Init('gebhardt_p');
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();


$SQL  ='SELECT ';
$SQL .='   distinct';
$SQL .='   KON_KEY, KON_NAME1 as Nachname,';
$SQL .='   KON_NAME2 as vorname,';
$SQL .='    (select listagg(KAB_ABTEILUNG,\';\') within group (order by KAB_ABTEILUNG) "list" ';
$SQL .='    from KONTAKTEABTEILUNGENZUORDNUNGEN';
$SQL .='    INNER JOIN kontakteabteilungen';
$SQL .='    ON kontakteabteilungenzuordnungen.kza_kab_key = kontakteabteilungen.kab_key';
$SQL .='    where kontakte.kon_key = kontakteabteilungenzuordnungen.kza_kon_key';
$SQL .='   ) as KAB_ABTEILUNG,';
$SQL .='   KON_ZUSTAENDIGKEIT as Zustaendigkeit, ';
$SQL .='   KON_BEMERKUNG as Bemerkung,';
$SQL .='   GEB_BEZEICHNUNG as Gebaeude_bezeichnung,';
$SQL .='   GEB_STRASSE as Gebaeude_Strasse, ';
$SQL .='   GEB_HAUSNUMMER as Gebaeude_Hausnummer, ';
$SQL .='   GEB_ORT as Gebaeude_ORT,';
$SQL .='   GEB_ORTSTEIL as Gebaeude_Ortsteil,';
$SQL .='   GEB_PLZ as Gebaeude_PLZ,';
$SQL .='   GEB_LAN_CODE as Gebaeude_Land,';
$SQL .='   GEB_BEMERKUNG as gebaeude_Bemerkung, ';
$SQL .='   (select listagg(\'0961/306-\'||kko_wert,\',\') within group (order by kko_kon_key) "list" ';
$SQL .='   from kontaktekommunikation where kko_kot_key = 9 and kko_kon_key = kon_key';
$SQL .='   ) as DURCHWAHL_TEL,';
$SQL .='   (select listagg(kko_wert,\',\') within group (order by kko_kon_key) "list" ';
$SQL .='   from kontaktekommunikation where kko_kot_key = 6 and kko_kon_key = kon_key';
$SQL .='   ) as HANDY_GESCHAEFT,';
$SQL .='   (select listagg(\'0961/306-\'||kko_wert,\', \') within group (order by kko_kon_key) "list" ';
$SQL .='   from kontaktekommunikation where kko_kot_key = 10 and kko_kon_key = kon_key';
$SQL .='   ) as DURCHWAHL_FAX,';
$SQL .='  ';
$SQL .='   (select listagg(kko_wert,\',\') within group (order by kko_kon_key) "list" ';
$SQL .='   from kontaktekommunikation where kko_kot_key = 7 and kko_kon_key = kon_key';
$SQL .='   ) as EMAIL,';
$SQL .='   (select listagg(KFA_KENNZEICHEN,\', \') within group (order by KFA_KON_KEY) "list" ';
$SQL .='   from kontaktefahrzeuge where KFA_KON_KEY = kon_key) as Kennzeichen';
$SQL .=' FROM kontakte';
$SQL .=' inner join AWIS.kontaktekategorien';
$SQL .=' on kontakte.kon_kka_key = kontaktekategorien.kka_key';
$SQL .=' left join Gebaeude';
$SQL .=' on GEB_ID = kon_geb_id';
$SQL .=' WHERE kon_status        = \'A\'';
$SQL .=' and kon_listen <> 0';
$SQL .=' and kka_key in (1,2,3,37,41)';
$SQL .=' and kon_name2 is not null';
$SQL .=' order by kon_name1,kon_name2';

$rsKontakte = $DB->RecordSetOeffnen($SQL);

$MaxMail = 1;
$MaxTel = 1;
$MaxHandy = 1;
$MaxFax = 1;
while(!$rsKontakte->EOF())
{	
	if ($MaxMail < count(explode(",",$rsKontakte->FeldInhalt('EMAIL'))))
	{
		$MaxMail = count(explode(",",$rsKontakte->FeldInhalt('EMAIL')));
	}
	
	if ($MaxTel < count(explode(",",$rsKontakte->FeldInhalt('DURCHWAHL_TEL'))))
	{
		$MaxTel = count(explode(",",$rsKontakte->FeldInhalt('DURCHWAHL_TEL')));
	}
	
	if ($MaxHandy < count(explode(",",$rsKontakte->FeldInhalt('HANDY_GESCHAEFT'))))
	{
		$MaxHandy = count(explode(",",$rsKontakte->FeldInhalt('HANDY_GESCHAEFT')));
	}
	
	if ($MaxFax < count(explode(",",$rsKontakte->FeldInhalt('DURCHWAHL_FAX'))))
	{
		$MaxFax = count(explode(",",$rsKontakte->FeldInhalt('DURCHWAHL_FAX')));
	}
	
	$rsKontakte->DSWeiter();
}
$rsKontakte->DSErster();

$ExportDatei = fopen($_SERVER['DOCUMENT_ROOT'] . '/export/Kontakte.csv',"w");

$Inhalt = '"VORNAME",';
$Inhalt .= '"NACHNAME",';

//Alle Emailspalten hinmalen...

for ($i=1;$i<=$MaxMail+1;$i++)
{
	$Inhalt.='"EMAIL'.$i .'",';
}

$Inhalt .= '"KAB_ABTEILUNG",';
$Inhalt .= '"ZUSTAENDIGKEIT",';

for ($i=1;$i<=$MaxTel+1;$i++)
{
	$Inhalt.='"DURCHWAHL_TEL'.$i .'",';
}



for ($i=1;$i<=$MaxFax+1;$i++)
{
	$Inhalt .= '"DURCHWAHL_FAX'.$i .'",';
}

for ($i=1;$i<=$MaxHandy+1;$i++)
{
	$Inhalt.='"HANDY_GESCHAEFT'.$i .'",';
}

//$Inhalt .= '"NOTIZ"';

$Inhalt .= $Zeilenabschluss;

fwrite($ExportDatei, $Inhalt);

while(!$rsKontakte->EOF())
{
	
	$Inhalt = '"'.$rsKontakte->FeldInhalt('VORNAME').'",';
	$Inhalt .= '"'.$rsKontakte->FeldInhalt('NACHNAME').'",';
	$Emails = Array();
	$EmailArray = Array();
	$Emails = explode (',',$rsKontakte->FeldInhalt('EMAIL'));
	$ZusatzInfos = '';
	
	$i=0;
	//Eindimensionales Emailarray, Mehrdimensional aufbauen und die Wertigkeit berechnen..
	foreach($Emails as $Email)
	{
		$EmailArray[$i]['Adresse'] = $Email;
		$EmailArray[$i]['Wertigkeit'] = 0;
		$tempMail = explode('@',$EmailArray[$i]['Adresse']);
		
		
		//1. Schauen wieviel Vor und Nachname in der Adresse steckt... 
		$Uebereinstimmung = similar_text(mb_strtolower($rsKontakte->FeldInhalt('VORNAME').'.'.$rsKontakte->FeldInhalt('NACHNAME')),mb_strtolower($tempMail[0]));
		
	    $Uebereinstimmung = $Uebereinstimmung!=0?($Uebereinstimmung / strlen($rsKontakte->FeldInhalt('VORNAME').'.'.$rsKontakte->FeldInhalt('NACHNAME')) * 100):0;
		$EmailArray[$i]['Wertigkeit'] += $Uebereinstimmung;
		
		
		//2. RGZ, RPL, GL, WL Adressen bekommen 20 Wertigkeitspunkte
		$Rollen = array('RGZ','RPL','GL-','WL-');
		foreach ($Rollen as $Rolle)
		{
			if (mb_strpos(mb_strtoupper($tempMail[0]), $Rolle)!==false)
			{
				$EmailArray[$i]['Wertigkeit'] += 20;
			}
		}
		
		//3. Schauen wieviel Abteilung in der Adresse steckt und bepunkten
		$Uebereinstimmung = similar_text(mb_strtolower($rsKontakte->FeldInhalt('KAB_ABTEILUNG')),mb_strtolower($tempMail[0]));
	
		$Uebereinstimmung = $Uebereinstimmung!=0?($Uebereinstimmung / strlen($rsKontakte->FeldInhalt('KAB_ABTEILUNG')) * 100):0;
		$EmailArray[$i]['Wertigkeit'] += $Uebereinstimmung;
		
		$i++;		
	}
	
	//zwei eindimensionale Arrays bauen um sp�ter $EmailArray zu sortieren
	$Wertigkeit = array();
	$Adresse = array();
	foreach ($EmailArray as $key => $Wert) {
		$Adresse[$key]    = $Wert['Adresse'];
		$Wertigkeit[$key] = $Wert['Wertigkeit'];
	}
	
	// Die Adressnm nach der Wertigkeit absteigend sortieren
	array_multisort($Wertigkeit, SORT_DESC, $Adresse, SORT_ASC, $EmailArray);
	
	
	$i=1;
	foreach($Adresse as $Email)
	{
		$Inhalt .= '"'.$Email."\",";
		
		//Outlook kann pro Kontakt "nur" 3 Emailadressen, deswegen alle weiteren zus�tzlich als Zusatzinfo merken
		if($i == 4) //Als Zusatzinfo merken..
		{
			$ZusatzInfos .= 'Zus�tzliche Emailadressen ' . chr(10) .chr(13); // �berschrift im Zusatzinfo block
			$ZusatzInfos .= $Email . chr(10).chr(13);
		}
		elseif($i > 4) //Restliche Mails ohne �berschrift
		{			
			$ZusatzInfos .= $Email . chr(10).chr(13);
		}
		
		$i++;
		
	}


	//Restliche Emailspalten hinmalen f�r den Fall, dass Outlook irgendwann mehr als 3 Emails kann.
	for ($i = count($Emails);$i<=$MaxMail;$i++)
	{
		$Inhalt.=',';
		if($i != $MaxMail)
		{
			$Inhalt .= '""';
		}
	}
	
	
	$Inhalt .= '"'.str_replace('"', '\'', $rsKontakte->FeldInhalt('KAB_ABTEILUNG')).'",';
	$Inhalt .= '"'.str_replace('"', '\'',$rsKontakte->FeldInhalt('ZUSTAENDIGKEIT')).'",';
	
	$Tels = explode (',',$rsKontakte->FeldInhalt('DURCHWAHL_TEL'));
	$i = 1;
	foreach($Tels as $Tel)
	{
		$Inhalt .= '"'.$Tel.'",';
		
		//Outlook kann pro Kontakt "nur" 4 Telefonnummern, deswegen alle weiteren zus�tzlich als Zusatzinfo merken
		if($i == 5) //Als Zusatzinfo merken..
		{
			$ZusatzInfos .= 'Zus�tzliche Telefonnummern ' . chr(10) .chr(13); // �berschrift im Zusatzinfo block
			$ZusatzInfos .= $Tel . chr(10).chr(13);
		}
		elseif($i > 5) //Restliche Mails ohne �berschrift
		{
			$ZusatzInfos .= $Tel . chr(10).chr(13);
		}
		
		$i++;
		
	}
	
	//Restliche Telefonspalten hinmalen..
	for ($i = count($Tels);$i<=$MaxTel;$i++)
	{
		$Inhalt.=',';
		if($i != $MaxTel)
		{
			$Inhalt .= '""';
		}
	}
	
	$Faxe = explode (',',$rsKontakte->FeldInhalt('DURCHWAHL_FAX'));
	
	foreach($Faxe as $Fax)
	{
		$Inhalt .= '"'.$Fax.'",';
		

		//Outlook kann pro Kontakt "nur" 2 Faxnummern, deswegen alle weiteren zus�tzlich als Zusatzinfo merken
		if($i == 3) //Als Zusatzinfo merken..
		{
			$ZusatzInfos .= 'Zus�tzliche Faxnummern ' . chr(10) .chr(13); // �berschrift im Zusatzinfo block
			$ZusatzInfos .= $Fax . chr(10).chr(13);
		}
		elseif($i > 3) //Restliche Mails ohne �berschrift
		{
			$ZusatzInfos .= $Fax . chr(10).chr(13);
		}
		
		$i++;
		
	}
	
	//Restliche Faxspalten hinmalen..
	for ($i = count($Faxe);$i<=$MaxFax;$i++)
	{
		$Inhalt.=',';
		if($i != $MaxFax)
		{
			$Inhalt .= '""';
		}
	}
	
	
	$Handys = explode (',',$rsKontakte->FeldInhalt('HANDY_GESCHAEFT'));
	
	foreach($Handys as $Handy)
	{
		$Inhalt .= '"'.$Handy.'",';		
	}
	//Restliche Handyspalten hinmalen..
	for ($i = count($Handys);$i<=$MaxHandy;$i++)
	{
		$Inhalt.=',';
		if($i != $MaxHandy)
		{
			$Inhalt .= '""';
		}
	}
	
	//L�ppt nicht. Linefeeds zerhauen die ganze Datei, trotz Textbegrenzungszeichen...
	//$Inhalt .= ',"' . $ZusatzInfos . '"'; 

	
	$Inhalt .=  $Zeilenabschluss;
	
	echo $Inhalt;
	fwrite($ExportDatei, $Inhalt);
	
	
	$rsKontakte->DSWeiter();
}
fclose($ExportDatei);