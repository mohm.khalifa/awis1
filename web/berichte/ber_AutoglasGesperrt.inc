<?php

require_once 'awisBerichte.inc';

class ber_AutoglasGesperrt extends awisBerichte
{

    /**
     * Prozedur zum Initialisieren des Berichts
     * @see awisBerichte::init()
     */
    public function init(array $Parameter)
    {
        //var_dump($Parameter);

        $Param = unserialize($this->_AWISBenutzer->ParameterLesen('Formular_FirstGlassGesperrt'));
        
        
        $TextKonserven[] = array('Fehler', 'err_KeineDaten');
        $TextKonserven[] = array('Wort', 'DatumVom');
        $TextKonserven[] = array('Wort', 'DatumBis');
        $TextKonserven[] = array('Liste', 'lst_ALLE_0');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Soll');
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');
        $TextKonserven[] = array('Ausdruck', 'txt_HinweisAusdruckIntern');


        $TextKonserven[] = array('FGK', '%');
        $TextKonserven[] = array('FKH', '%');
        $TextKonserven[] = array('FKA', '%');
        $TextKonserven[] = array('FGP', '%');
        $TextKonserven[] = array('FPH', '%');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    /**
     * Eigentliches PDF erzeugen
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {
        // PDF Attribute setzen
        $this->SetTitle($this->_BerichtsName);
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        //$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->AliasNbPages();


        // Seite erstellen
        $this->NeueSeite();

        $SQL  = 'SELECT pos.fgp_vorgangnr, ';
        $SQL .= '       kasse.sumkasse, ';
        $SQL .= '       kasse.fka_wanr,';
        $SQL .= '       pos.sumpos, ';
        $SQL .= '       kasse.kassedatum,';
        $SQL .= '      (kasse.sumkasse - pos.sumpos) AS differenz,';
        $SQL .= '       pos.fgk_versicherung, ';
        $SQL .= '       pos.fgk_notiz';
        $SQL .= ' FROM ';
        $SQL .= '	(SELECT kopf.fgk_versicherung, ';
        $SQL .= '           kopf.fgk_notiz, ';
        $SQL .= '           p.fgp_vorgangnr,';
        $SQL .= '	        SUM(ABS(p.fgp_anzahl) * p.fgp_oempreis * (1 + kopf.fgk_steuersatz /100)) AS sumpos';
        $SQL .= '	 FROM fgpositionsdaten p';
        $SQL .= '	 INNER JOIN fgkopfdaten kopf';
        $SQL .= '	 ON kopf.fgk_vorgangnr  = p.fgp_vorgangnr';
        $SQL .= '	 WHERE kopf.fgk_fgn_key in (6,18)';
        $SQL .= '	 GROUP BY kopf.fgk_versicherung,kopf.fgk_notiz, p.fgp_vorgangnr';
        $SQL .= '	) pos ';
        $SQL .= ' INNER JOIN ';
        $SQL .= '	(SELECT fka_aemnr, ';
        $SQL .= '			MIN(fka_wanr) AS fka_wanr, ';
        $SQL .= '			SUM(ABS(fka_menge) * fka_betrag) AS sumkasse, ';
        $SQL .= '			MIN(fka_datum) AS kassedatum ';
        $SQL .= '	 FROM fkassendaten ';
        $SQL .= ' 	 GROUP BY fka_aemnr ';
        $SQL .= '	 ORDER BY kassedatum';
        $SQL .= '	 ) kasse';
        $SQL .= ' ON kasse.fka_aemnr = pos.fgp_vorgangnr';
        $SQL .= ' ORDER BY kasse.kassedatum ASC';
        $recPDF = $this->_DB->RecordSetOeffnen($SQL);
        
        while(!$recPDF->EOF())
        {
        	// Start des ersten Kastens
        	$StartY = $this->GetY();	
        	
        	$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        	
        	$this->Cell(35, $this->_Parameter['Zeilenhoehe'], $recPDF->FeldInhalt('FGP_VORGANGNR'), 0, 0, 'C', 0);
        	$this->Cell(18, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N2T',$recPDF->FeldInhalt('SUMKASSE')), 0, 0, 'R', 0);
        	$this->Cell(15, $this->_Parameter['Zeilenhoehe'], $recPDF->FeldInhalt('FKA_WANR'), 0, 0, 'C', 0);
        	$this->Cell(18, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N2T',$recPDF->FeldInhalt('SUMPOS')), 0, 0, 'R', 0);
        	$this->Cell(18, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N2T',$recPDF->FeldInhalt('DIFFERENZ')), 0, 0, 'R', 0);
        	$this->Cell(45, $this->_Parameter['Zeilenhoehe'], substr($recPDF->FeldInhalt('FGK_VERSICHERUNG'),0,22), 0, 0, 'L', 0);
        	$this->Cell(27, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('D',$recPDF->FeldInhalt('KASSEDATUM')), 0, 'C', 0);
        	
        	$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseMulticell']);
        	$this->Multicell(95, $this->_Parameter['Zeilenhoehe'], $recPDF->FeldInhalt('FGK_NOTIZ'), 'LTRB', 'L', 0);
        	
        	$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        	// Untere Querlinie
        	$this->Line($this->_Spalte,$this->getY(),self::SeitenBreite()-self::RECHTERRAND,$this->getY());
        	$EndeY = $this->GetY();
        	
        	$this->SetXY($this->_Spalte, $EndeY);
        	
        	$recPDF->DSWeiter();
        }
        //*********************************************************************
        // Berichtsende -> Zur�ckliefern
        //*********************************************************************
        switch ($Ausgabeart) {
            case 1:   // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:   // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:   // Als String zur�ckliefern
                return($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:    // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage();
    }

    /**
     * Automatische Fu�zeile
     * @see TCPDF::Footer()
     */
	public function Footer()
	{
		$BlockYStart = 30;
		$BlockYEnde = $this->GetY();
		$this->SetXY(15, $BlockYStart);
		$this->Cell(35, ($BlockYEnde-$BlockYStart), '', 'LTRB', 'L', 0);
		$this->Cell(18, ($BlockYEnde-$BlockYStart), '', 'LTRB', 'R', 0);
		$this->Cell(15, ($BlockYEnde-$BlockYStart), '', 'LTRB', 'L', 0);
		$this->Cell(18, ($BlockYEnde-$BlockYStart), '', 'LTRB', 'R', 0);
		$this->Cell(18, ($BlockYEnde-$BlockYStart), '', 'LTRB', 'R', 0);
		$this->Cell(45, ($BlockYEnde-$BlockYStart), '', 'LTRB', 'L', 0);
		$this->Cell(27, ($BlockYEnde-$BlockYStart), '', 'LTRB', 'C', 0);
		
	      // Seitennummer schreiben
	      $this->SetXY(15,self::SeitenHoehe()-5);
	      $this->SetFont('Arial','',7);
	      $this->Cell(10,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo(),0,0,'L','');

	      $this->SetXY(0,self::SeitenHoehe()-5);
	      $this->Cell(self::Seitenbreite(),$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Ausdruck']['txt_HinweisAusdruckIntern']),0,0,'C','');

	      $this->SetXY(self::SeitenBreite()-40,self::SeitenHoehe()-5);
	      $this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('DU',date('d.m.Y H:i:s')),0,0,'R','');
	      //$this->SetXY($this->_Spalte,$this->_Zeile);
	      
	      	
	      	// => Kasten zeichnen
	      $this->SetXY(15,30);
      }


      /**
     * Automatische Kopfzeile
     * @see TCPDF::Header()
     */
    public function Header()
    {
        // Startpositionen setzen
        $this->_Zeile = 30;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        
        if($this->_BerichtsVorlage!='')
        {
        	$pdf_vorlage = $this->ImportPage(1);		// Erste Seite aus der Vorlage importieren
        	$this->useTemplate($pdf_vorlage);
        }
        
        //Tabellenueberschrift ausgeben
    	$this->SetFont($this->_Parameter['Schriftart'], 'b', $this->_Parameter['Schriftgroesse']);
    	$this->SetFillColor(209,209,209);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'] * 1.4, $this->_AWISSprachkonserven['FGK']['FGK_VORGANGNR'], 'LTRB', 0, 'C', 1);
        $this->Cell(18, $this->_Parameter['Zeilenhoehe'] * 1.4, 'Kasse', 'LTRB', 0, 'C', 1);
        $this->Cell(15, $this->_Parameter['Zeilenhoehe'] * 1.4, 'WANR', 'LTRB', 0, 'C', 1);
        $this->Cell(18, $this->_Parameter['Zeilenhoehe'] * 1.4, 'FG', 'LTRB', 0, 'C', 1);
        $this->Cell(18, $this->_Parameter['Zeilenhoehe'] * 1.4, 'Diff.', 'LTRB', 0, 'C', 1);
        $this->Cell(45, $this->_Parameter['Zeilenhoehe'] * 1.4, 'Versicherung', 'LTRB', 0, 'C', 1);
        $this->Cell(27, $this->_Parameter['Zeilenhoehe'] * 1.4, 'Datum(Kasse)', 'LTRB', 0, 'C', 1);
        $this->Cell(95, $this->_Parameter['Zeilenhoehe'] * 1.4, 'Bemerkung', 'LTRB', 0, 'C', 1);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.4;
        $this->SetXY($this->_Spalte, $this->_Zeile);
	}
}
?>