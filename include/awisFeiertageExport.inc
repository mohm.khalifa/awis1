<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';


class awisFeiertageExport
{

    private $_AWISBenutzer;

    private $_DB;

    private $_Trenner = ';';

    private $_LineFeed = PHP_EOL;

    private $_FP = null;

    private $_Pfad = '';

    private $_DebugLevel = 0;

    function __construct($Benutzer,$DebugLevel=0)
    {
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_DebugLevel = $DebugLevel;
    }


    public function ExportFilialfinder(){

        $this->LogAusgabe('Beginne mit den Export f�r den Filialfinder. ', 0);

        $this->_Pfad = $this->_AWISBenutzer->ParameterLesen('FilialfinderFeiertagePfad',true);
        $this->_OeffneDatei();

        $rsFil = $this->_rsFil();

        $this->LogAusgabe('Schreibe Spalten�berschriften', 999);
        $this->_ZeileSchreiben(['FIL_ID','DATUM','BEZEICHNUNG_KURZ','BEZEICHNUNG_LANG']);

        $DatumAb = '01.01.' . date('Y');

        while(!$rsFil->EOF()){

            $rsFeiertag = $this->_rsFeiertage($rsFil->FeldInhalt('FIL_ID'), $rsFil->FeldInhalt('LAN_CODE'), $rsFil->FeldInhalt('FIF_BUL_ID'), $DatumAb);

            while(!$rsFeiertag->EOF()){
                $Zeile = array();
                $Zeile[] = $rsFil->FeldInhalt('FIL_ID');
                $Zeile[] = $rsFeiertag->FeldInhalt('XFT_TAG');

                if(strpos($rsFeiertag->FeldInhalt('XFT_BEZEICHNUNG'),'-') !== false){
                    $Zeile[] = trim(explode('-',$rsFeiertag->FeldInhalt('XFT_BEZEICHNUNG'))[0]);
                }else{
                    $Zeile[] = $rsFeiertag->FeldInhalt('XFT_BEZEICHNUNG');
                }

                $Zeile[] = $rsFeiertag->FeldInhalt('XFT_BEZEICHNUNG');

                $this->_ZeileSchreiben($Zeile);

                $rsFeiertag->DSWeiter();
            }

            $rsFil->DSWeiter();
        }

        $this->_DateiBereitstellen();
    }

    private function _OeffneDatei(){

        if(is_file($this->_Pfad. '.tmp')){
            $this->LogAusgabe('Alte .tmp Datei war noch da. L�sche..');
            unlink($this->_Pfad . '.tmp');
        }

        $this->LogAusgabe('�ffne Datei ' . $this->_Pfad,0);

        $this->_FP = fopen($this->_Pfad . '.tmp', "w");

        if(!$this->_FP){
            throw new Exception('Konnte Datei ' . $this->_Pfad . ' nicht �ffnen.',201903011347);
        }
    }

    private function _ZeileSchreiben(array $Zeile){
        if(count($Zeile)){
            $Zeile = implode($this->_Trenner,$Zeile) . $this->_LineFeed;
            $this->LogAusgabe('Schreibe Zeile: ' .$Zeile,999);
            fwrite($this->_FP, $Zeile);
        }
    }

    private function _DateiBereitstellen(){
        fclose($this->_FP);
        $this->LogAusgabe('Stelle Datei bereit indem ich das .tmp entferne.');
        if(!rename($this->_Pfad.'.tmp',$this->_Pfad)){
            $this->LogAusgabe('Konnte datei nicht bereitstellen, da ich nicht umbenennen konnte ');
            throw new Exception('Konnte Datei nicht umbennenen. ' . $this->_Pfad . '.tmp',201903011356);
        }
    }

    private function _rsFil(){
        $SQL = 'SELECT FIL_ID, FIF_BUL_ID, LAN_CODE from V_FILIALEN_AKTUELL';
        $SQL .= ' order by 1 asc';
        $rsFil = $this->_DB->RecordSetOeffnen($SQL);

        $this->LogAusgabe('Suche Filialen mit: ' . $this->_DB->LetzterSQL(),999);

        return $rsFil;
    }

    private function _rsFeiertage($FIL_ID, $LAN_CODE, $BUL_ID, $DatumAb='01.01.1970'){
        $SQL = ' SELECT * FROM FEIERTAGE where ';
        $SQL .= 'XFT_TAG >= ' .$this->_DB->WertSetzen('XXX','D', $DatumAb);
        $SQL .= ' AND (';
        $SQL .= ' XFT_LAN_CODE = ' . $this->_DB->WertSetzen('XXX','T', $LAN_CODE);
        $SQL .= ' or XFT_BUL_ID = ' . $this->_DB->WertSetzen('XXX','Z', $BUL_ID);
        $SQL .= ' or XFT_FIL_ID = ' . $this->_DB->WertSetzen('XXX','Z', $FIL_ID);
        $SQL .= ')';

        $SQL .= ' ORDER BY XFT_TAG asc';

        $rsFeiertage = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('XXX'));

        $this->LogAusgabe('Suche Feiertage mit: ' . $this->_DB->LetzterSQL(),999);

        return $rsFeiertage;
    }


    public function LogAusgabe($Text, $Level = 0){
        if($this->_DebugLevel >= $Level){
            echo date('d.m.y H:i:s') . ' - ' . $Text . PHP_EOL;
        }
    }
}