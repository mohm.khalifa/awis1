<?php

/**
 * 
 * @author Henry Ott
 * @copyright ATU Auto Teile Unger
 * @version 20130502
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('RRP', '%');
    $TextKonserven[] = array('RST', '%');
    $TextKonserven[] = array('REH', '%');
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'gelistet');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'InVorbereitung');
    $TextKonserven[] = array('Wort', 'Reifensuche');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Liste', 'lst_KEINE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Liste','lst_KENN1');
    

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht15000 = $AWISBenutzer->HatDasRecht(15000);
    if ($Recht15000 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Form->DebugAusgabe(1, $_POST);
    $Form->DebugAusgabe(1, $_GET);
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_RRP"));
	
    $Form->DebugAusgabe(1,$Param);
    
    if (isset($_POST['cmdSuche_x']))
    {
        $Param['KEY'] = 0;
        $Param['ORDER'] = '';
        $Param['BLOCK'] = '';
        
        $Param['GELISTET'] = $_POST['sucGELISTET'];
        $Param['RRP_ATUNR'] = $_POST['sucRRP_ATUNR'];
        $Param['RRP_GUELTIGAB'] = $_POST['sucRRP_GUELTIGAB'];
        $Param['RRP_GUELTIGBIS'] = $_POST['sucRRP_GUELTIGBIS'];
        $Param['RRP_BEZEICHNUNGWW'] = $_POST['sucRRP_BEZEICHNUNGWW'];
        $Param['RRP_KENN1'] = $_POST['sucRRP_KENN1'];
        $Param['RRP_KENN2'] = $_POST['sucRRP_KENN2'];
        $Param['RRP_KENN_L2'] = $_POST['sucRRP_KENN_L2'];
        $Param['RRP_SUCH1'] = $_POST['sucRRP_SUCH1'];
        $Param['RRP_MEHRFACHKENNUNG'] = $_POST['sucRRP_MEHRFACHKENNUNG'];
        $Param['RST_REH_KEY'] = $_POST['sucRST_REH_KEY'];
        
        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');

        $AWISBenutzer->ParameterSchreiben("Formular_RRP", serialize($Param));
    }
    elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
    	$Form->DebugAusgabe(1,'geh ins l�schen');
        include('./reifenpflege_loeschen.php');
    }
    elseif (isset($_POST['cmdSpeichern_x']) or isset($_GET['ok']))
    {
        include('./reifenpflege_speichern.php');
    }
    elseif (isset($_POST['cmdDSNeu_x']))
    {
        $AWIS_KEY1 = -1;
    }
    elseif (isset($_GET['RRP_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['RRP_KEY']);
        if (isset($_GET['Wiedervorlagen']))
        {
        	$Param = array();
        	$Param['KEY']=0;
        	$Param['WHERE']='';
        	$Param['ORDER']='';
        }
        
    }
    else   // Letzten Benutzer suchen
    {
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_RRP',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
    }

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (!isset($_GET['Sort']))
    {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '')
        {
            $ORDERBY = $Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' RRP_ATUNR,RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        	$Param['ORDER'] = $ORDERBY;
        }
    }
    else
    {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        $Param['ORDER'] = $ORDERBY;
    }

    if (isset($_GET['SortExt']))
    {
    	$ORDERBY .= ','.$_GET['SortExt'];
    }
    
    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $BindeVariablen = array();
    $Bedingung = _BedingungErstellen($Param, $BindeVariablen);

	//********************************************************
	// Daten suchen
	//********************************************************
	
    $SQL  = ' SELECT rrp.*, rst.*';
    $SQL .= ' ,CASE';
	$SQL .= '  WHEN RRP_GUELTIGAB <= SYSDATE AND RRP_GUELTIGBIS < SYSDATE THEN \'font-style:italic;color:black;\'';
	$SQL .= '  WHEN RRP_GUELTIGAB <= SYSDATE AND RRP_GUELTIGBIS >= SYSDATE THEN \'\'';
	$SQL .= '  WHEN RRP_GUELTIGAB > SYSDATE THEN \'font-style:italic;color:green;\'';
	$SQL .= '  ELSE \'font-style:italic;color:red;\'';
	$SQL .= ' END as STYLE';
    
    if ($AWIS_KEY1 <= 0)
    {
        $SQL .= ' ,row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr';
    }

    $SQL .= ' FROM V_REIFENPFLEGE rrp';    
    $SQL .= ' LEFT JOIN REIFENSTAMM rst ON rst.rst_key = rrp.rrp_rst_key';
    
    if ($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $SQL .= ' ORDER BY ' . $ORDERBY;
    
    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if (isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        $Param['BLOCK']=$Block;
    }
    elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
    {
        $Block = intval($Param['BLOCK']);
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $Form->DebugAusgabe(1,$SQL);
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if ($AWIS_KEY1 <= 0)
    {
        $SQL = 'SELECT * FROM (' . $SQL . ') DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    }
    
    $rsRRP = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
    $Form->DebugAusgabe(1,$DB->LetzterSQL());

    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->SchreibeHTMLCode('<form name=frmReifenpflege action=./reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . '' . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . ' method=post>');
    
	if ($rsRRP->EOF() AND !isset($_POST['cmdDSNeu_x']) AND !isset($_GET['RRP_KEY']))  // Keine Meldung bei neuen Datens�tzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';

		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht15000 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	    $Form->SchaltflaechenEnde();
    }
    elseif (($rsRRP->AnzahlDatensaetze() > 1) or (isset($_GET['Liste'])))      // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart('font-size:10pt');
    	
        $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_ATUNR')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_ATUNR'], 60, '', $Link);

        $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_BEZEICHNUNGWW' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_BEZEICHNUNGWW')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_BEZEICHNUNGWW'], 270, '', $Link);

        $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RST_BREITE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RST_BREITE')) ? '~' : '');
        $Link .= '&SortExt=RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX,RRP_ATUNR';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['txt_Reifenkennzeichen'],300, '', $Link);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_KENN1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_KENN1')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX,RRP_ATUNR';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_KENN1_superkurz'], 30, '', $Link);
        
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_KENN2_superkurz'], 30);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_SUCH1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_SUCH1')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX,RRP_ATUNR';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_SUCH1_kurz'], 90, '', $Link);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_SUCH2' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_SUCH2')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX,RRP_ATUNR';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_SUCH2'], 90, '', $Link);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_MEHRFACHKENNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_MEHRFACHKENNUNG')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX,RRP_ATUNR';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_MEHRFACHKENNUNG_kurz'], 60, '', $Link);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
        $Link .= '&SSort=RRP_GUELTIGAB'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_GUELTIGAB'))?'~':'');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX,RRP_ATUNR';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_GUELTIGAB'],80,'',$Link);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
        $Link .= '&SSort=RRP_GUELTIGBIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='RRP_GUELTIGBIS'))?'~':'');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX,RRP_ATUNR';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_GUELTIGBIS'],80,'',$Link);
        
        $Form->ZeileEnde();

        $DS = 0;

        while (!$rsRRP->EOF())
        {
            $Form->ZeileStart('font-size:10pt');
            $Link = './reifenpflege_Main.php?cmdAktion=Details&RRP_KEY=' . $rsRRP->FeldInhalt('RRP_KEY') . '';

            $Style = $rsRRP->FeldInhalt('STYLE');
            $Form->Erstelle_ListenFeld('RRP_ATUNR', $rsRRP->FeldInhalt('RRP_ATUNR'), 6, 60, false, ($DS % 2), $Style, $Link, 'T');
            $Form->Erstelle_ListenFeld('RRP_BEZEICHNUNGWW', $rsRRP->FeldInhalt('RRP_BEZEICHNUNGWW'), 40, 270, false, ($DS % 2), $Style, '', '', '', '');

            $Form->Erstelle_ListenFeld('RST_BREITE', $rsRRP->FeldInhalt('RST_BREITE'), 3, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
            $Form->Erstelle_ListenFeld('RST_QUERSCHNITT', $rsRRP->FeldInhalt('RST_QUERSCHNITT'), 2, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
            $Form->Erstelle_ListenFeld('RST_BAUART', $rsRRP->FeldInhalt('RST_BAUART'), 10, 30, false, ($DS % 2), '', '', 'T', 'L', '');
            $Form->Erstelle_ListenFeld('RST_INNENDURCHMESSER', $rsRRP->FeldInhalt('RST_INNENDURCHMESSER'), 3, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
            $Form->Erstelle_ListenFeld('RST_LOADINDEX', $rsRRP->FeldInhalt('RST_LOADINDEX'), 10, 30, false, ($DS % 2), '', '', 'T', '', '');
            $Form->Erstelle_ListenFeld('RST_SPEEDINDEX', $rsRRP->FeldInhalt('RST_SPEEDINDEX'), 3, 30, false, ($DS % 2), '', '', 'T', '', '');
            	
            $Zusatz = '';
            if ($rsRRP->FeldInhalt('RST_RF')!='')
            {
            	$Zusatz  = $rsRRP->FeldInhalt('RST_RF');
            	if ($rsRRP->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
            	{
            		$Zusatz .= '/'.$rsRRP->FeldInhalt('RST_ZUSATZBEMERKUNG');
            	}
            }
            elseif ($rsRRP->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
            {
            	$Zusatz = $rsRRP->FeldInhalt('RST_ZUSATZBEMERKUNG');
            }
            
            $Form->Erstelle_ListenFeld('ZUSATZ',$Zusatz, 0, 100, false, ($DS % 2), '', '', 'T', '', '');
            
            
            
            
            $Form->Erstelle_ListenFeld('RRP_KENN1', $rsRRP->FeldInhalt('RRP_KENN1'), 1, 25, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('RRP_KENN2', $rsRRP->FeldInhalt('RRP_KENN2').'/'.$rsRRP->FeldInhalt('RRP_KENN_L2'), 1, 30, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('RRP_SUCH1', $rsRRP->FeldInhalt('RRP_SUCH1'), 10, 90, false, ($DS % 2),$Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('RRP_SUCH2', $rsRRP->FeldInhalt('RRP_SUCH2'), 10, 90, false, ($DS % 2),$Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('RRP_MEHRFACHKENNUNG', $rsRRP->FeldInhalt('RRP_MEHRFACHKENNUNG'), 1, 60, false, ($DS % 2),$Style, '', '', '', '');
			$Form->Erstelle_ListenFeld('RRP_GUELTIGAB',$rsRRP->FeldInhalt('RRP_GUELTIGAB'),10,80,false,($DS%2),$Style,'','D');
			$Form->Erstelle_ListenFeld('RRP_GUELTIGBIS',$rsRRP->FeldInhalt('RRP_GUELTIGBIS'),10,80,false,($DS%2),$Style,'','D');
			
			if($rsRRP->FeldInhalt('RRP_BEMERKUNG')!='')
			{
				$Form->Erstelle_HinweisIcon('info', 20, '',$rsRRP->FeldInhalt('RRP_BEMERKUNG'));
			}
				
			$Form->ZeileEnde();

            $rsRRP->DSWeiter();
            $DS++;
        }

        $Link = './reifenpflege_Main.php?cmdAktion=Details&Liste=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        if (($Recht15000 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
        {
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }
        $Form->SchaltflaechenEnde();
    }
    else          // Eine einzelner Datensatz
    {
        $Form->SchreibeHTMLCode('<form name=frmReifenpflege action=./reifenpflege_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

        $AWIS_KEY1 = $rsRRP->FeldInhalt('RRP_KEY');
        $Param['KEY']=$AWIS_KEY1;
        $AWISBenutzer->ParameterSchreiben("Formular_RRP", serialize($Param));
        $EditRecht = ($Recht15000&2!=0);
        $EditErlaubt = 
        (
        		($EditRecht AND $rsRRP->FeldInhalt('GELISTET')=='0') or
        		($EditRecht AND $AWIS_KEY1=='')
        		
        );
        $Form->Erstelle_HiddenFeld('RRP_KEY', $AWIS_KEY1);

        $Form->Formular_Start();
        $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

    	$Form->DebugAusgabe(1,$AWIS_KEY1);

        // Infozeile
        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./reifenpflege_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsRRP->FeldInhalt('RRP_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsRRP->FeldInhalt('RRP_USERDAT'));
        $Form->InfoZeile($Felder, '');


        if ($AWIS_KEY1=='')
        {
        	if(isset($_GET['RST_KEY']) AND $_GET['RST_KEY']>0)
        	{
        		$Form->Erstelle_HiddenFeld('RRP_RST_KEY', $_GET['RST_KEY']);
        		
        		// Reifen laden und Bezeichung, etc anzeigen
        		$SQL  = 'SELECT RST_KEY,';
        		$SQL .= ' RST_BEZEICHNUNG,';
        		$SQL .= ' RST_REH_KEY,';
        		$SQL .= ' REH_BEZEICHNUNG,';
        		$SQL .= ' REH_KURZBEZEICHNUNG,';
        		$SQL .= ' RST_LIE_NR,';
        		$SQL .= ' RST_LARTNR,';
        		$SQL .= ' RST_BREITE,';
        		$SQL .= ' RST_QUERSCHNITT,';
        		$SQL .= ' RST_BAUART,';
        		$SQL .= ' RST_INNENDURCHMESSER,';
        		$SQL .= ' RST_LLKW,';
        		$SQL .= ' RST_LOADINDEX,';
        		$SQL .= ' RST_LOADINDEX2,';
        		$SQL .= ' RST_LOADINDEX3,';
        		$SQL .= ' RST_RF,';
        		$SQL .= ' RST_SPEEDINDEX,';
        		$SQL .= ' RST_SPEEDINDEX2,';
        		$SQL .= ' RST_ZUSATZBEMERKUNG,';
        		$SQL .= ' RST_ZUSATZBEMERKUNG2,';
        		$SQL .= ' RST_ZUSATZBEMERKUNG3,';
        		$SQL .= ' RST_TYP,';
        		$SQL .= ' RST_TYP2,';
        		$SQL .= ' RST_TYP3,';
        		$SQL .= ' RST_ROLLRES,';
        		$SQL .= ' RST_WETGRIP,';
        		$SQL .= ' RST_NOISEPE,';
        		$SQL .= ' RST_NOISECL,';
        		$SQL .= ' RST_KBPREIS,';
        		$SQL .= ' RST_XDI_KEY,';
        		$SQL .= ' RST_IMPORTSTATUS,';
        		$SQL .= ' RST_STATUS,';
        		$SQL .= ' RST_BEMERKUNG,';
        		$SQL .= ' RST_USER,';
        		$SQL .= ' RST_USERDAT';
        		$SQL .= ' FROM REIFENSTAMM';
        		$SQL .= ' INNER JOIN REIFENHERSTELLER ON REH_KEY = RST_REH_KEY';
       			$SQL .= ' WHERE RST_KEY =0'.$_GET['RST_KEY'];
       			$rsRST = $DB->RecordSetOeffnen($SQL);

       			$Form->ZeileStart();
       			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'], 120, '', '');
       			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_LIE_NR'], 70, '', '');
       			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_LARTNR'], 140, '', '');
       			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['txt_Reifenkennzeichen'],283, '', '');
       			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_BEZEICHNUNG'],280, '', '');
       			$Form->ZeileEnde();
       			
       			if (!$rsRST->EOF())
       			{
       				$Form->ZeileStart();
       				$Link = '../reifenstamm/reifenstamm_Main.php?cmdAktion=Details&RST_KEY=' . $rsRST->FeldInhalt('RST_KEY') . '';
       				$Form->Erstelle_ListenFeld('REH_BEZEICHNUNG', $rsRST->FeldInhalt('REH_BEZEICHNUNG'), 0, 120, false, '', '', '', 'T');
       				$Form->Erstelle_ListenFeld('RST_LIE_NR', $rsRST->FeldInhalt('RST_LIE_NR'), 0, 70, false, '', '', '', 'T');
       				$Form->Erstelle_ListenFeld('RST_LARTNR', $rsRST->FeldInhalt('RST_LARTNR'), 0, 140, false, '', '', $Link, 'T');
       				$Form->Erstelle_ListenFeld('RST_BREITE', $rsRST->FeldInhalt('RST_BREITE'), 0, 30, false, '', '', '', 'N0', 'L', '');
       				$Form->Erstelle_ListenFeld('RST_QUERSCHNITT', $rsRST->FeldInhalt('RST_QUERSCHNITT'), 0, 30, false, '', '', '', 'N0', 'L', '');
       				$Form->Erstelle_ListenFeld('RST_BAUART', $rsRST->FeldInhalt('RST_BAUART'), 0, 30, false, '', '', '', 'T', 'L', '');
       				$Form->Erstelle_ListenFeld('RST_INNENDURCHMESSER', $rsRST->FeldInhalt('RST_INNENDURCHMESSER'), 0, 30, false, '', '', '', 'N0', 'L', '');
       				$Form->Erstelle_ListenFeld('RST_LOADINDEX', $rsRST->FeldInhalt('RST_LOADINDEX'), 0, 30, false, '', '', '', 'T', '', '');
       				$Form->Erstelle_ListenFeld('RST_SPEEDINDEX', $rsRST->FeldInhalt('RST_SPEEDINDEX'), 0, 30, false, '', '', '', 'T', '', '');
       					
       				$Zusatz = '';
       				if ($rsRST->FeldInhalt('RST_RF')!='')
       				{
       					$Zusatz  = $rsRST->FeldInhalt('RST_RF');
       					if ($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
       					{
       						$Zusatz .= '/'.$rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG');
       					}
       				}
       				elseif ($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
       				{
       					$Zusatz = $rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG');
       				}
       			
       				$Form->Erstelle_ListenFeld('ZUSATZ',$Zusatz, 0, 80, false, '', '', '', 'T', '', '');
       			
       				$Form->Erstelle_ListenFeld('RST_BEZEICHNUNG', $rsRST->FeldInhalt('RST_BEZEICHNUNG'), 0, 280, false, '', '', '', 'T', '', '');
       				if($rsRST->FeldInhalt('RST_BEMERKUNG')!='')
       				{
       					$Form->Erstelle_HinweisIcon('info', 20, '',$rsRST->FeldInhalt('RST_BEMERKUNG'));
       				}
       			
       				$Form->ZeileEnde();

       			}
       			$Form->Trennzeile('O');
       			
        	}
        	else
        	{
	        	$Form->ZeileStart();
        		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Reifensuche'] . ':', 150);
	        	$Form->AuswahlBox('RST_SUCHE', 'box1', 'txtRRP_RST_KEY', 'RST_Liste', '', 300, 100, '','T',true,'','','',50,'',$AWISSprachKonserven['RST']['ttt_Reifensuche']);
	        	$Form->ZeileEnde();
	        	
	        	$Form->ZeileStart('font-size:8pt');
	        	$Form->Erstelle_TextLabel('&nbsp',150);
	        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['txt_Reifensuche'],600);
	        	$Form->ZeileEnde();
	        	
	        	$Form->ZeileStart();
	        	$Form->AuswahlBoxHuelle('box1','AuswahlListe','');
	        	$Form->ZeileEnde();
        		$AWISCursorPosition = 'sucRST_SUCHE';
        		
        		$Form->Trennzeile('O');
        	}
        }
		else
		{
			$AWISCursorPosition = 'txtRRP_ATUNR';
		}			        
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_ATUNR'] . ':', 150);
        $Form->Erstelle_TextFeld('!RRP_ATUNR', ($rsRRP->FeldInhalt('RRP_ATUNR')), 6, 100, $EditErlaubt,'','','','','','','',6);
        $Form->ZeileEnde();
        
        $Form->Trennzeile('O');

        if ($AWIS_KEY1!='')
        {
			//Wenn noch keine Artikelbezeichnung vorhanden -> Vorschl�ge generieren
        	if ($rsRRP->FeldInhalt('RRP_BEZEICHNUNGWW')=='')
        	{
        		// Reifen laden und Bezeichung, etc anzeigen
        		$SQL  = 'SELECT *';
        		$SQL .= ' FROM REIFENSTAMM';
        		$SQL .= ' INNER JOIN REIFENHERSTELLER ON REH_KEY = RST_REH_KEY';
        		$SQL .= ' WHERE RST_KEY =0'.$rsRRP->FeldInhalt('RRP_RST_KEY');
        		$rsRST = $DB->RecordSetOeffnen($SQL);
				
        		if (!$rsRST->EOF())
        		{
        			//Vorschlagsliste f�r WWS Artikelnummer generieren
        			$BezeichnungStamm  = str_pad(trim($rsRST->FeldInhalt('REH_KURZBEZEICHNUNG')),9," ",STR_PAD_RIGHT);
        			$BezeichnungStamm .= str_pad($rsRST->Feldinhalt('RST_BREITE'), 3, " ", STR_PAD_LEFT);
        			$BezeichnungStamm .= '/';
        			$BezeichnungStamm .= str_pad($rsRST->Feldinhalt('RST_QUERSCHNITT'), 3, " ",STR_PAD_RIGHT);
        			
        			$OptionArray = array();
        			$DS = 0;
        			
        			$Bezeichnung  = $BezeichnungStamm;
        			$Bezeichnung .= str_pad('R', 2, " ",STR_PAD_LEFT);
        			$Bezeichnung .= ' ';
        			$Bezeichnung .= str_pad($rsRST->Feldinhalt('RST_INNENDURCHMESSER'), 3, " ",STR_PAD_RIGHT);
        			$Bezeichnung .= str_pad($rsRST->Feldinhalt('RST_LOADINDEX'), 3, " ",STR_PAD_LEFT);
        			$Bezeichnung .= str_pad($rsRST->Feldinhalt('RST_SPEEDINDEX'), 2, " ",STR_PAD_RIGHT);
        			$Bezeichnung .= substr($rsRST->Feldinhalt('RST_BEZEICHNUNG'),0,11);
        			$OptionArray[$DS] = $Bezeichnung .'~'.$Bezeichnung;
        			 
        			$DS++;
        			$Bezeichnung  = $BezeichnungStamm;
        			$Bezeichnung .= str_pad('ZR', 2, " ",STR_PAD_LEFT);
        			$Bezeichnung .= ' ';
        			$Bezeichnung .= str_pad($rsRST->Feldinhalt('RST_INNENDURCHMESSER'), 3, " ",STR_PAD_RIGHT);
        			$Bezeichnung .= str_pad($rsRST->Feldinhalt('RST_LOADINDEX'), 3, " ",STR_PAD_LEFT);
        			$Bezeichnung .= str_pad($rsRST->Feldinhalt('RST_SPEEDINDEX'), 2, " ",STR_PAD_RIGHT);
        			$Bezeichnung .= substr($rsRST->Feldinhalt('RST_BEZEICHNUNG'),0,11);
        			$OptionArray[$DS] = $Bezeichnung .'~'.$Bezeichnung;
        		}
        		else
        		{
        			$OptionArray = array();
        			$Bezeichnung = 'Keine Daten gefunden';
        			$OptionArray[0] = ''.'~'.$Bezeichnung;
        		}
        		
        		$Form->ZeileStart();
        		$Java = 'onclick="document.getElementsByName(\'txtRRP_BEZEICHNUNGWW\')[0].value=document.getElementsByName(\'txtBEZEICHNUNGSVORSCHLAG\')[0].value;"';
        		$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['txt_Bezeichnungsvorschlag'] . ':', 150);
        		$Form->Erstelle_SelectFeld('BEZEICHNUNGSVORSCHLAG','', '500:315', $EditErlaubt, '',' ','','','',$OptionArray,$Java);
        		$Form->ZeileEnde();
        		
        		
        	}
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_BEZEICHNUNGWW'] . ':', 150);
        	$Form->Erstelle_TextFeld('RRP_BEZEICHNUNGWW',($rsRRP->FeldInhalt('RRP_BEZEICHNUNGWW')),45,'500:450',$EditErlaubt,'','','','','','','',50);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_ARTBEZK'] . ':', 150);
        	$Form->Erstelle_TextFeld('RRP_ARTBEZK',($rsRRP->FeldInhalt('RRP_ARTBEZK')),20,'200:180',$EditErlaubt,'','','','','','','',12);
        	$Form->ZeileEnde();
        	
        	$Form->Trennzeile('O');

        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_WGR_ID'] . ':', 150, '', '', '');
        	$SQL = "SELECT WGR_ID, WGR_ID || ' - ' || WGR_BEZEICHNUNG FROM WARENGRUPPEN WHERE WGR_ID IN ('01','02','23','29') ORDER BY WGR_ID";
        	$Form->Erstelle_SelectFeld('RRP_WGR_ID', $rsRRP->FeldInhalt('RRP_WGR_ID'),'240:230', $EditErlaubt, $SQL, $OptionBitteWaehlen);
        	 
//TODO code mit Sacha anpassen
       		$SQL = "SELECT DISTINCT WUG_ID AS KEY , WUG_ID || ' - ' || WUG_BEZEICHNUNG AS ANZEIGE ";
       		$SQL .= ' FROM WARENUNTERGRUPPEN';
       		$SQL .= ' WHERE WUG_WGR_ID='.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_WGR_ID'),true);
       		$SQL .= ' AND WUG_ID='.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_WUG_ID'),true);
       		$SQL .= ' ORDER BY WUG_ID';
        	if ($EditErlaubt)
        	{
        		$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_WUG_ID'] . ':', 140, '', '', '');
        		$rsWUG=$DB->RecordSetOeffnen($SQL);
        		$Form->Erstelle_SelectFeld('RRP_WUG_ID',$rsRRP->FeldInhalt('RRP_WUG_ID'),"240:220",$EditErlaubt,'***WUG_Daten;txtRRP_WUG_ID;WGR=*txtRRP_WGR_ID&Zusatz='.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],($rsWUG->EOF())?$OptionBitteWaehlen:$rsWUG->FeldInhalt('KEY').'~'.$rsWUG->FeldInhalt('ANZEIGE'));
        	}
        	else
        	{
        		$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_WUG_ID'] . ':', 160, '', '', '');
        		$Form->Erstelle_SelectFeld('RRP_WUG_ID',$rsRRP->FeldInhalt('RRP_WUG_ID'),'250:240',$EditErlaubt,$SQL, $OptionBitteWaehlen);
        	}
        	$Form->ZeileEnde();
        	 
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_KENN1'] . ':', 150);
    		$Daten = explode("|", $AWISSprachKonserven['Liste']['lst_KENN1']);
    		$Form->Erstelle_SelectFeld('RRP_KENN1',($rsRRP->FeldInhalt('RRP_KENN1')), '240:230', $EditErlaubt, '', $OptionBitteWaehlen, '', '', '', $Daten);
        	$Form->ZeileEnde();

        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_KENN2'] . ':', 150);
        	$Form->Erstelle_TextFeld('RRP_KENN2', ($rsRRP->FeldInhalt('RRP_KENN2')), 1, 240, $EditErlaubt,'','','','','','','',1);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_KENN_L2'] . ':', 160);
        	$Form->Erstelle_TextFeld('RRP_KENN_L2', ($rsRRP->FeldInhalt('RRP_KENN_L2')), 1, 200, $EditErlaubt,'','','','','','','',1);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_SUCH1'] . ':', 150);
        	$Form->Erstelle_TextFeld('RRP_SUCH1',($rsRRP->FeldInhalt('RRP_SUCH1')),10,240,$EditErlaubt,'','','','','','','',10);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_SUCH2'] . ':', 160);
        	$Daten = explode("|",$AWISSprachKonserven['RRP']['lst_RRP_SUCH2']);
        	$Form->Erstelle_SelectFeld('RRP_SUCH2',($rsRRP->FeldInhalt('RRP_SUCH2')), 150, $EditErlaubt, '',$OptionBitteWaehlen,'','','',$Daten);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_BESTELLKZ'] . ':', 150);
        	$Form->Erstelle_TextFeld('RRP_BESTELLKZ', ($rsRRP->FeldInhalt('RRP_BESTELLKZ')), 1, 200, $EditErlaubt,'','','','','','','',1);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_MEHRFACHKENNUNG'] . ':', 150);
        	$Daten = explode("|",$AWISSprachKonserven['RRP']['lst_RRP_MEHRFACHKENNUNG']);
        	$Form->Erstelle_SelectFeld('RRP_MEHRFACHKENNUNG',($rsRRP->FeldInhalt('RRP_MEHRFACHKENNUNG')), '200:180', false, '',$AWISSprachKonserven['Liste']['lst_KEINE_0'],'','','',$Daten);
        	$Form->ZeileEnde();

			$Form->Trennzeile('O');
        	
        	$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['gelistet'].':',150);
			$Daten = explode("|",$AWISSprachKonserven['RRP']['lst_gelistet']);
			$Form->Erstelle_SelectFeld('GELISTET',($rsRRP->FeldInhalt('GELISTET')), 250, false, '','','','','',$Daten);
			$Form->ZeileEnde();
				
			$Form->Trennzeile('O');
        }
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_GUELTIGAB'].':',150);
        $Form->Erstelle_TextFeld('RRP_GUELTIGAB',($rsRRP->FeldInhalt('RRP_GUELTIGAB')),20,200,$EditRecht,'','','','D','','','',10);
        $Form->ZeileEnde();
        
        if ($AWIS_KEY1!='')
        {
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_GUELTIGBIS'].':',150);
        	$Form->Erstelle_TextFeld('RRP_GUELTIGBIS',($rsRRP->FeldInhalt('RRP_GUELTIGBIS')),20,200,$EditRecht,'','','','D','','','',10);
        	$Form->ZeileEnde();
        	
        	$Form->Trennzeile('O');
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_BEMERKUNG'].':',150);
        	$Form->Erstelle_TextArea('RRP_BEMERKUNG',$rsRRP->FeldInhalt('RRP_BEMERKUNG'),800,103,5,$EditRecht);
        	$Form->ZeileEnde();
        }
        
        $Form->Trennzeile('O');

        $Form->ZeileStart();

        //Anzeige weiterer Infos aus Artikelstamm --> Nur bei gelisteten Artikeln
        if ($rsRRP->FeldInhalt('GELISTET') == '1')
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVDE'] . ':', 150);
            $Form->Erstelle_TextFeld('RRP_AKTIVDE', ($rsRRP->FeldInhalt('AST_AKTIV_DE')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVLIGHTDE'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVLIGHTDE', ($rsRRP->FeldInhalt('AST_LIGHT_DE')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVOES'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVOES', ($rsRRP->FeldInhalt('AST_AKTIV_AT')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVLIGHTOES'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVLIGHTOES', ($rsRRP->FeldInhalt('AST_LIGHT_AT')), 1, 80, '');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVCZ'] . ':', 150);
            $Form->Erstelle_TextFeld('RRP_AKTIVCZ', ($rsRRP->FeldInhalt('AST_AKTIV_CZ')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVLIGHTCZ'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVLIGHTCZ', ($rsRRP->FeldInhalt('AST_LIGHT_CZ')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVNL'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVNL', ($rsRRP->FeldInhalt('AST_AKTIV_NL')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVLIGHTNL'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVLIGHTNL', ($rsRRP->FeldInhalt('AST_LIGHT_NL')), 1, 80, '');
            $Form->ZeileEnde();
            
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVI'] . ':', 150);
            $Form->Erstelle_TextFeld('RRP_AKTIVI', ($rsRRP->FeldInhalt('AST_AKTIV_IT')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVLIGHTI'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVLIGHTI', ($rsRRP->FeldInhalt('AST_LIGHT_IT')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVCH'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVCH', ($rsRRP->FeldInhalt('AST_AKTIV_CH')), 1, 80, '');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RRP']['RRP_AKTIVLIGHTCH'] . ':', 120);
            $Form->Erstelle_TextFeld('RRP_AKTIVLIGHTCH', ($rsRRP->FeldInhalt('AST_LIGHT_CH')), 1, 80, '');
            $Form->ZeileEnde();
            
            $Form->Trennzeile('O');
            
        }

        if ($AWIS_KEY1 != 0)
        {
            $RegisterSeite = (isset($_GET['Seite']) ? $_GET['Seite'] : (isset($_POST['Seite']) ? $_POST['Seite'] : ''));
            $SubReg = new awisRegister(15001);
            $SubReg->ZeichneRegister($RegisterSeite);
        }

		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht15000 & 6) != 0) // Bearbeiten/Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	    }
	
	    if (($Recht15000 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	
	    if (($Recht15000 & 8) == 8 AND !isset($_POST['cmdDSNeu_x']))
	    {
	        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	    }
	
	    $Form->SchaltflaechenEnde();
    }

    $Form->SchreibeHTMLCode('</form>');
    
    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $AWISBenutzer->ParameterSchreiben("Formular_RRP", serialize($Param));

    $Form->SetzeCursor($AWISCursorPosition);
    
}
catch (awisException $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201305021656");
    }
    else
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}
catch (Exception $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201305021657");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param, &$BindeVariablen)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if (floatval($AWIS_KEY1) != 0)
    {
        $Bedingung.= 'AND RRP_KEY = :var_N0_RRP_KEY';
        $BindeVariablen['var_N0_RRP_KEY'] = $AWIS_KEY1;
        return $Bedingung;
    }

    if (isset($Param['GELISTET']) AND $Param['GELISTET'] != '-1')
    {
    	if ($Param['GELISTET']<2)
    	{
    		$Bedingung .= 'AND RRP_AST_KEY IS '.($Param['GELISTET']==0?'':'NOT').' NULL ';
    	}
    	elseif ($Param['GELISTET']==2)
    	{
    		$Bedingung .= 'AND RRP_AST_KEY IS NOT NULL ';
    		$Bedingung .= 'AND RRP_RST_KEY IS NULL ';
    	}
       	elseif ($Param['GELISTET']==3)
    	{
    		$Bedingung .= 'AND RRP_AST_KEY IS NULL ';
    		$Bedingung .= 'AND RRP_RST_KEY IS NULL ';
    	}
    }
    
    if (isset($Param['RRP_ATUNR']) AND $Param['RRP_ATUNR'] != '')
    {
        $Bedingung .= 'AND RRP_ATUNR ' . $DB->LIKEoderIST($Param['RRP_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER);
    }

    if (isset($Param['RRP_BEZEICHNUNGWW']) AND $Param['RRP_BEZEICHNUNGWW'] != '')
    {
        $Bedingung .= 'AND TRIM(UPPER(RRP_BEZEICHNUNGWW)) '. $DB->LIKEoderIST($Param['RRP_BEZEICHNUNGWW'],awisDatenbank::AWIS_LIKE_UPPER);
    }
    
    if (isset($Param['RRP_KENN1']) AND $Param['RRP_KENN1'] != '0')
    {
        $Bedingung .= 'AND RRP_KENN1 = :var_T_RRP_KENN1 ';
        $BindeVariablen['var_T_RRP_KENN1'] = $Param['RRP_KENN1'];
    }

    if (isset($Param['RRP_KENN2']) AND $Param['RRP_KENN2'] != '')
    {
    	$Bedingung .= 'AND RRP_KENN2 = :var_T_RRP_KENN2 ';
    	$BindeVariablen['var_T_RRP_KENN2'] = $Param['RRP_KENN2'];
    }
    
    if (isset($Param['RRP_KENN_L2']) AND $Param['RRP_KENN_L2'] != '')
    {
    	$Bedingung .= 'AND RRP_KENN_L2 = :var_T_RRP_KENN_L2 ';
    	$BindeVariablen['var_T_RRP_KENN_L2'] = $Param['RRP_KENN_L2'];
    }
    
    if (isset($Param['RRP_MEHRFACHKENNUNG']) AND $Param['RRP_MEHRFACHKENNUNG'] != '0')
    {
    	$Bedingung .= 'AND RRP_MEHRFACHKENNUNG = :var_T_RRP_MEHRFACHKENNUNG ';
    	$BindeVariablen['var_T_RRP_MEHRFACHKENNUNG'] = $Param['RRP_MEHRFACHKENNUNG'];
    }
    
    if (isset($Param['RST_REH_KEY']) AND $Param['RST_REH_KEY'] != '-1')
    {
    	$Bedingung .= 'AND RST_REH_KEY = :var_N0_RST_REH_KEY ';
    	$BindeVariablen['var_N0_RST_REH_KEY'] = $Param['RST_REH_KEY'];
    }
    
    return $Bedingung;
}

?>