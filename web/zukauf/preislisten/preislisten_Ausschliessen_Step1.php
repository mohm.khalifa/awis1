<?php
global $ZPL;
global $Aus;
global $AWIS_KEY1;

$GespeicherteFelder = array();
if (isset($ZPL->Param['GespeicherteFelder'])) {
    $GespeicherteFelder = $ZPL->Param['GespeicherteFelder'];
}


foreach ($Aus->FilterFelder as $Feld) {
    $Feldtyp = substr($Feld, 0, 3);
    $Feld = substr($Feld, 3);
    $ZPL->Form->ZeileStart();
    $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['ZPL'][$Feld] . ': ', 190);

    $Operatoren = '=~=|like~like|<>~<>|not like~not like|is not null~nicht leer|is null~ist leer';
    $Operatoren = explode('|', $Operatoren);

    switch ($Feldtyp) {
        case 'txt':
            $ZPL->Form->Erstelle_SelectFeld($Feld . '_operator', '', '250:120', true, '', $ZPL->OptionBitteWaehlen, '', '', '', $Operatoren);

            $ZPL->Form->Erstelle_TextFeld($Feld, '', 20, 200, true);
            break;

        case 'cmb':
            $ZPL->Form->Erstelle_SelectFeld($Feld . '_operator', '', '250:120', true, '', $ZPL->OptionBitteWaehlen, '', '', '', $Operatoren);

            $SQL = 'select distinct ' . $Feld .', '. $Feld . ' from ZUKAUFIMPORT order by 1 asc';
            $ZPL->Form->Erstelle_SelectFeld('~' . $Feld, '', '500:200', true, $SQL, $ZPL->OptionBitteWaehlen, '', '', '', '','','',array(),'','AWIS_IMPORT');


            break;
    }

    $ZPL->Form->ZeileEnde();
}

$ZPL->Form->ZeileStart();
$ZPL->Form->Trennzeile('L');
$ZPL->Form->ZeileEnde();

$ZPL->Form->ZeileStart();
$ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['Wort']['BedingungHinzufuegen'], 0);
$ZPL->Form->ZeileEnde();

$ZPL->Form->ZeileStart();
$ZPL->Form->Erstelle_TextLabel('AUTOMATISCH', 110, 'font-weight: bold;');
$ZPL->Form->Schaltflaeche('image', 'BedinungAdd', '', '/bilder/cmd_neu.png', 'hinzuf�gen', '', '', 20, 20, '', true);

$ZPL->Form->ZeileEnde();
if (isset($_POST['BedinungAdd_x'])) {
    foreach ($Aus->FilterFelder as $Feld) {
        $Feld = str_replace('cmb', 'txt', $Feld);
        //Feld da und Wert enthalten?
        if (isset($_POST[$Feld]) and $_POST[$Feld] != '' and $_POST[$Feld] != $ZPL->OptionBitteWaehlen) {
            if ($_POST[$Feld . '_operator'] != '' and $_POST[$Feld . '_operator'] != $ZPL->OptionBitteWaehlen) {
                $GespeicherteFelder[] = array('Feld' => $Feld, 'Wert' => $_POST[$Feld], 'Operator' => $_POST[$Feld . '_operator']);
            }
        }
    }
}
$Del = $ZPL->Form->NameInArray($_POST, 'BedinungEntf_', 0, 1);
//Wurde gel�scht?

if ($Del !== false) {
    $IDX = explode('_', $Del)[1];
    unset($GespeicherteFelder[$IDX]);
}

foreach ($GespeicherteFelder as $Key => $Feld) {
    $ZPL->Form->ZeileStart();
    $ZPL->Form->Schaltflaeche('image', 'BedinungEntf_' . $Key, '', '/bilder/icon_delete.png', 'entfernen', '', '', 20, 20, '', true);
    $Bereich = substr(explode('_', $Feld['Feld'])[0], 3);
    $ZPL->Form->Erstelle_TextLabel($ZPL->AWISSprachKonserven['ZPL'][substr($Feld['Feld'], 3)], 250);
    $ZPL->Form->Erstelle_TextLabel($Feld['Operator'], 190);
    $ZPL->Form->Erstelle_TextLabel($Feld['Wert'], 100);
    $ZPL->Form->ZeileEnde();
}

$ZPL->Form->Formular_Ende();

$ZPL->Param['GespeicherteFelder'] = $GespeicherteFelder;

echo '</div>';
$ZPL->Form->SchaltflaechenStart();
$ZPL->Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $ZPL->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
if (count($GespeicherteFelder) > 0) {
    $ZPL->Form->Schaltflaeche('image', 'cmdStep_2', '', '/bilder/cmd_dsweiter.png', $ZPL->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
}
$ZPL->Form->SchaltflaechenEnde();

$ZPL->Form->SchreibeHTMLCode('</form>');

?>