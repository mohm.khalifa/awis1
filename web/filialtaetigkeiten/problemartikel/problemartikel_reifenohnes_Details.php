<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FIL_ID;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PRA','%');
	$TextKonserven[]=array('PAB','%');
	$TextKonserven[]=array('PRI','%');
	$TextKonserven[]=array('FIB','FIB_BESTAND');
	$TextKonserven[]=array('FIL','FIL_ID');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','lbl_drucken');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$ArtikelGruppe=1;
	if(isset($_GET['Gruppe']))
	{
		$ArtikelGruppe = (int)$_GET['Gruppe'];
	}

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht6000 = $AWISBenutzer->HatDasRecht(6000);
	if($Recht6000==0)
	{
	    awisEreignis(3,1000,'PRA',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$DetailAnsicht=false;
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./problemartikel_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./problemartikel_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}

	$EditModus = ($Recht6000&6);

	//********************************************************
	// Daten suchen
	//********************************************************

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ProblemArtikel_1'));
	$FIL_ID=$Param['FIL_ID'];
	$AWISCursorPosition='';
//var_dump($Param);
	if($ArtikelGruppe==1)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PRA']['NeueATUNR'].':',350);
		$Form->Erstelle_TextFeld('PRA_AST_ATUNR','',8,100,$EditModus,'','','','T');
		//$AWISCursorPosition = ($AWISCursorPosition==''?'txtPRA_AST_ATUNR':$AWISCursorPosition);
		$Form->ZeileEnde();

		$Form->Trennzeile();
	}

	$Form->Erstelle_HiddenFeld('PRA_KATEGORIE',1);

	$ORDERBY = ' ORDER BY AST_ATUNR';

	$SQL = ' SELECT DATEN.* ';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM (';
	$SQL .= ' SELECT problemartikel.*, PAB_ISTBESTAND, PAB_BEMERKUNG, PAB_DATUM, FIB_BESTAND, AST_ATUNR, AST_BEZEICHNUNGWW';
	$SQL .= ' , CASE WHEN PAB_ISTBESTAND IS NULL THEN 1 WHEN PAB_ISTBESTAND > 0 THEN 2 ELSE 3 END AS SORT';
	$SQL .= ', row_number() over(partition by PRA_KEY ORDER BY PAB_DATUM DESC) as Zeile';
	$SQL .= ', CASE WHEN PAB_DATUM = TRUNC(SYSDATE) THEN PAB_KEY ELSE 0 END AS PAB_KEY';
	$SQL .= ', (SELECT PRI_WERT FROM ProblemartikelInfos WHERE PRI_PRA_KEY = PRA_KEY AND PRI_ITY_KEY=16) AS Zertifikat';
	$SQL .= ' FROM problemartikel';
	$SQL .= ' INNER JOIN artikelstamm ON PRA_AST_ATUNR = AST_ATUNR';
	$SQL .= ' LEFT OUTER JOIN FilialBestand ON FIB_FIL_ID = '.$DB->FeldInhaltFormat('N0',$FIL_ID,false).' AND FIB_AST_ATUNR = AST_ATUNR';
	$SQL .= ' LEFT OUTER JOIN ProblemArtikelBestand ON PAB_PRA_KEY = PRA_KEY AND PAB_FIL_ID = '.$DB->FeldInhaltFormat('N0',$FIL_ID,false).'';
	//$SQL .= ' WHERE FIB_FIL_ID = '.$DB->FeldInhaltFormat('N0',$FIL_ID,false);
	$SQL .= ' WHERE PRA_KATEGORIE = 1';
	$SQL .= ' ) DATEN ';
	$SQL .= ' WHERE Zeile=1';
	if($ArtikelGruppe==1)
	{
		//$SQL .= ' AND ((FIB_BESTAND > 0 AND COALESCE(PAB_ISTBESTAND,1) > 0))';
		$SQL .= ' AND (COALESCE(PAB_ISTBESTAND,1) > 0)';
	}
	else
	{
		$SQL .= ' AND (COALESCE(PAB_ISTBESTAND,0) = 0)';
	}
	$SQL .= $ORDERBY;


$Form->DebugAusgabe(1,$SQL);
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		$Param['BLOCK']=$Block;
		$AWISBenutzer->ParameterSchreiben('Formular_MBW',serialize($Param));
	}
	elseif(isset($Param['BLOCK']))
	{
		$Block=$Param['BLOCK'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

//var_dump($SQL);
//$Form->DebugAusgabe(1,$SQL);
	$rsPRA = $DB->RecordsetOeffnen($SQL);

	$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PRA']['PRA_AST_ATUNR'],110);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIB']['FIB_BESTAND'],110);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAB']['PAB_ISTBESTAND'],120);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAB']['PAB_BEMERKUNG'],230);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAB']['PAB_DATUM'],100);
	$Form->ZeileEnde();

	$DS=0;
	$Link= '';
	$LetzteGruppe=0;

	while(!$rsPRA->EOF())
	{
		if($LetzteGruppe!=$rsPRA->FeldInhalt('SORT'))
		{
//			$Form->ZeileStart();
//			$Form->Hinweistext($AWISSprachKonserven['PAB']['txt_PAB_HINWEIS_'.$rsPRA->FeldInhalt('SORT')],2);
//			$Form->ZeileEnde();
			//$Form->Trennzeile();

			$LetzteGruppe=$rsPRA->FeldInhalt('SORT');
		}

		$Form->ZeileStart();
		$Link='';
		$Icons=array();
		if($rsPRA->FeldInhalt('ZERTIFIKAT')!='')
		{
			$Icons[0] = array('pdf','/dokumentanzeigen.php?bereich=reifenzertifikate&dateiname='.urlencode($rsPRA->FeldInhalt('ZERTIFIKAT')).'&erweiterung=pdf');
			//$Link = '/dokumentanzeigen.php?bereich=reifenzertifikate&dateiname='.$rsPRA->FeldInhalt('ZERTIFIKAT').'&erweiterung=pdf';
		}
		$Form->Erstelle_ListeIcons($Icons,20,($DS%2));
		$Form->Erstelle_ListenFeld('#PRA_AST_ATUNR',$rsPRA->FeldInhalt('PRA_AST_ATUNR'),0,90,false,($DS%2),'',$Link,'T','L',$rsPRA->FeldInhalt('AST_BEZEICHNUNGWW'));

		$AltBestand=$rsPRA->FeldInhalt('PAB_ISTBESTAND');
		$AltBemerkung=$rsPRA->FeldInhalt('PAB_BEMERKUNG');
		$AltStand = $rsPRA->FeldInhalt('PAB_DATUM');
		if($rsPRA->FeldInhalt('PAB_ISTBESTAND')=='')
		{
			$SQL = 'SELECT PAB_ISTBESTAND,PAB_BEMERKUNG, PAB_DATUM';
			$SQL .= ' FROM ProblemartikelBestand';
			$SQL .= ' WHERE PAB_PRA_KEY = '.$rsPRA->FeldInhalt('PRA_KEY');
			$SQL .= ' AND PAB_FIL_ID = '.$FIL_ID;
			$SQL .= ' ORDER BY PAB_DATUM DESC';
			$rsPAB = $DB->RecordSetOeffnen($SQL);
			$AltBestand = $rsPAB->FeldInhalt('PAB_ISTBESTAND');
			$AltBemerkung = $rsPAB->FeldInhalt('PAB_BEMERKUNG');
			$AltStand = $rsPAB->FeldInhalt('PAB_DATUM');
		}
		$Form->Erstelle_ListenFeld('#FIBBESTAND_'.$rsPRA->FeldInhalt('PRA_KEY'),$rsPRA->FeldInhalt('FIB_BESTAND'),5,110,false,($DS%2),'','','N0','L');
		$Form->Erstelle_HiddenFeld('PAB_FIBBESTAND_'.$rsPRA->FeldInhalt('PRA_KEY'),$rsPRA->FeldInhalt('FIB_BESTAND'));
		$Form->Erstelle_ListenFeld('PAB_ISTBESTAND_'.$rsPRA->FeldInhalt('PRA_KEY').'_'.$rsPRA->FeldInhalt('PAB_KEY'),$AltBestand,5,120,$EditModus,($DS%2),'','','T','L');
		if($AWISCursorPosition=='')
		{
			$AWISCursorPosition='txtPAB_ISTBESTAND_'.$rsPRA->FeldInhalt('PRA_KEY').'_'.$rsPRA->FeldInhalt('PAB_KEY');
		}
		$Form->Erstelle_ListenFeld('PAB_BEMERKUNG_'.$rsPRA->FeldInhalt('PRA_KEY').'_'.$rsPRA->FeldInhalt('PAB_KEY'),$AltBemerkung,30,230,$EditModus,($DS%2),'','','T','L');
		$Form->Erstelle_ListenFeld('#STAND',$AltStand,5,100,false,($DS%2),($rsPRA->FeldInhalt('SORT')==1?'background:#FFFF00':''),'','D','L');

		$Form->ZeileEnde();

		$rsPRA->DSWeiter();
		$DS++;
	}

	// Bl�ttern
	$Link = './problemartikel_Main.php?cmdAktion=ReifenOhneS'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'&Gruppe='.$ArtikelGruppe;
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	$Form->Formular_Ende();

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht6000&6)!=0))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	if($FilZugriff==='')
	{
		$Form->Schaltflaeche('href', 'cmdDrucken', './problemartikel_Drucken_Liste.php?KAT=1', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');
	}
	$Form->Schaltflaeche('href', 'cmdDrucken', './problemartikel_Drucken_Liste.php?KAT=1'.(($FilZugriff=='' AND $FIL_ID!='')?'&FIL_ID='.$FIL_ID:''), '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>