<html>
<head>
<meta http-equiv="content-type" content="text/html"; charset="windows-1252">
</head>
<?php

global $AWISWebSrv;

require_once("operating_AWIS_config.php");

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
include ("awisHeader.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	if ($AWISBenutzer->HatDasRecht(3124) == 0 || $AWISBenutzer->HatDasRecht(3124)&32 !== 32 )
	{
	    $Form->Fehler_KeineRechte();
	}

if (!empty($_GET["dir"])) {
  	$dir=$_GET["dir"];
  
	if ($dir=="/" || substr($dir,0,1)=="/") 
	{
		print "<h1>Kein Zugriff auf ROOT-Verzeichnis erlaubt</h1>";
	  	exit (0);	
	}
	elseif (substr($dir,0,3)=="../" || substr($dir,0,2)=="..")	
	{
		print "<h1>Pfadwechsel nicht erlaubt.</h1>";
	  	exit (0);	
	}
	else {
  		print "<h1>". $AWISAppSrv . ' - ' . $dir ."</h1>";
	}  
  
	$mydir = array_diff(scandir($dir,0),array_merge(array(".","..")));
	if ($mydir == FALSE) {
	print "<h2>Kann Verzeichnis nicht &ouml;ffnen.</h2>";
	exit (0);
  	};
} 
else {
	print "<h1>Kein Verzeichnis angegeben</h1>";
	exit (0);
}

if ($dir=="/") {$dir="";}	

$dir_back=substr($dir,0,strripos($dir,"/"));
if ($dir_back=="") {$dir_back="/";}	
//if($dir_back!="/")
//{
	echo "<h2><a href=./showdir_log.php?dir=$dir_back>Verzeichnis nach oben</a></h2>";
//}

foreach ($mydir as $d)
{
	if (is_file($dir.'/'.$d)&&is_readable($dir.'/'.$d))
	{
		echo "<br><a href=./showfile_log.php?file=$dir/$d target='_blank'><img src='../images/page_white_text.png' border='0' alt='Datei &ouml;ffnen'> $d</a>";
	}
	elseif (is_dir($dir.'/'.$d)&&is_readable($dir.'/'.$d))
	{
		echo "<br><a href=./showdir_log.php?dir=$dir/$d><img src='../images/folder.png' border='0' alt='Ordner &ouml;ffnen'> $d</a>";
	}
	else
	{
		echo "<br><img src='../images/exclamation.png' border='0' alt='Keine Berechtigung'> $d</a>";		
	}
}

}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</html>
