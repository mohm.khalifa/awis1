<?php
/**
 * Formular: 01 - Hardware
 * @author Sacha Kerres
 *
 */
$FormularKennung = '001';

$TextKonserven = array();
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'TTT_*');
$ToolTipps = $Form->LadeTexte($TextKonserven, $AWISBenutzer->BenutzerSprache());


//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

$SQL = ' select ADE_MAIL, ADE_SURNAME || COALESCE(\', \'||ADE_GIVENNAME,\'\') AS KON_NAME from ACTIVEDIRECTORYEXPORT ';


$Form->ZeileStart();
$Form->Erstelle_TextLabel('Benachrichtigung an:', 299,'','','');
$Form->Erstelle_MehrfachSelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn',isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn']:array(),'450:450',true,$SQL,'','','','','');
$Form->ZeileEnde();


//**************************************************
// Neueintritt / Nachbesetzung
//**************************************************
$Block='190';

$Form->CheckboxGruppeStart('Mitarbeiter');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('!DETAILS_'.$FormularKennung.'_'.$Block.'-Neueintritt',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Neueintritt'])?'on':'off'), 30, true, 'on','','','','Mitarbeiter', '', '', 'Mitarbeiter');
$Form->Erstelle_TextLabel('Neueintritt', 300,'','','');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('!DETAILS_'.$FormularKennung.'_'.$Block.'-Nachbesetzung',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nachbesetzung'])?'on':'off'), 30, true, 'on','','','','Mitarbeiter', '', '', 'Mitarbeiter');
$Form->Erstelle_TextLabel('Nachbesetzung', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_NOTEBOOK']);
$Form->ZeileEnde();
$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 75,'','','');
$Form->Erstelle_TextLabel('Name des vorherigen Mitarbeiters:', 225,'','','');
$Form->Erstelle_TextFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-VorherigerMA', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-VorherigerMA'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Mitarbeiter']:''), 50, 550, true, '', '', '', 'T');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('!DETAILS_'.$FormularKennung.'_'.$Block.'-BestehenderMA',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BestehenderMA'])?'on':'off'), 30, true, 'on','','','','Mitarbeiter', '', '', 'Mitarbeiter');
$Form->Erstelle_TextLabel('bestehender Mitarbeiter', 300,'','','');
$Form->ZeileEnde();
$Form->CheckboxGruppeEnde();
 
echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$(".Mitarbeiter").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nachbesetzung").prop("checked")==true)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-VorherigerMA").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-VorherigerMA").addClass("InputTextPflicht")'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-VorherigerMA").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-VorherigerMA").addClass("InputText")'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document).ready(function() {$(".Mitarbeiter").change()});';

echo '</script>'.PHP_EOL;


//**************************************************
// Hardware
//**************************************************
$Block='120';

$Form->ZeileStart();
$Form->Erstelle_TextLabel('Hardware:', 300,'','','');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-ThinClient',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-ThinClient'])?'on':'off'), 30, true, 'on','','','','Hardware');
$Form->Erstelle_TextLabel('Thin Client (Igel)', 300,'','','');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Notebook',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Notebook'])?'on':'off'), 30, true, 'on','','','','Hardware');
$Form->Erstelle_TextLabel('Notebook', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_NOTEBOOK']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Handy',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Handy'])?'on':'off'), 30, true, 'on','','','','Hardware');
$Form->Erstelle_TextLabel('Handy', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_NOTEBOOK']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Smartphone',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Smartphone'])?'on':'off'), 30, true, 'on','','','','Hardware');
$Form->Erstelle_TextLabel('Smartphone', 300,'','','');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Sonstiges:', 250,'','','');
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges']:''), 600, 60, 3, true, '','','','','Hardware');
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 75,'','','');
$Form->Erstelle_TextLabel('Verwendungszweck:', 225,'','','');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Zweck', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck']:''), 50, 550, true, '', '', '', 'T');
$Form->ZeileEnde();

echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$(".Hardware").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-ThinClient").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Notebook").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Handy").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Smartphone").prop("checked")==true ||  $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()!="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputTextPflicht")'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document).ready(function() {$(".Hardware").change()});';

echo '</script>'.PHP_EOL;


//**************************************************
// Drucker
//**************************************************
$Block='140';

$Form->ZeileStart();
$Form->Erstelle_TextLabel('Drucker:', 300,'','','');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-SW',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-SW'])?'on':'off'), 30, true, 'on', '','', '', 'Drucker');
$Form->Erstelle_TextLabel('Schwarz-wei&szlig;-Drucker', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_SWDRUCKER']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-FARBE',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-FARBE'])?'on':'off'), 30, true, 'on', '','', '', 'Drucker');
$Form->Erstelle_TextLabel('Farbdrucker', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_FARBDRUCKER']);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Sonstiges:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_DRUCKER_SONSTIGES']);
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges']:''), 600, 60, 3, true, '','','','','Drucker');
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 75,'','','');
$Form->Erstelle_TextLabel('Verwendungszweck:', 225,'','','');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Zweck', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck']:''), 50, 550, true, '', '', '', 'T');
$Form->ZeileEnde();

echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$(".Drucker").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-SW").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-FARBE").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()!="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputTextPflicht")'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document).ready(function() {$(".Drucker").change()});';

echo '</script>'.PHP_EOL;

//**************************************************
// Peripherie Ger�te
//**************************************************

$Block='150';

$Form->ZeileStart();
$Form->Erstelle_TextLabel('Zusatzausstattung:', 300,'','','');
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Maus',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus'])?'on':'off'), 30, true, 'on', '','', '', 'PG');
$Form->Erstelle_TextLabel('Maus', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_SCANNER']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur'])?'on':'off'), 30, true, 'on', '','', '', 'PG');
$Form->Erstelle_TextLabel('Tastatur', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_FAX']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Monitor',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor'])?'on':'off'), 30, true, 'on', '','', '', 'PG');
$Form->Erstelle_TextLabel('Monitor', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_BEAMER']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Telefon',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon'])?'on':'off'), 30, true, 'on', '','', '', 'PG');
$Form->Erstelle_TextLabel('Telefon', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_USB']);
$Form->ZeileEnde();

$Block = '180';
$Form->ZeileStart('', '', 'HSZ');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale'])?'on':'off'), 30, true, 'on', '','', '', 'HSZ');
$Form->Erstelle_TextLabel('Headset Zentrale', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MONITOR']);
$Form->ZeileEnde();

$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel('Typ des Headsets: ', 225, '', '', '');
$hstyp = array("Mono Headset~Mono Headset (1-Ohr)", "Duo Headset~Duo Headset (2-Ohr)");
$Form->Erstelle_SelectFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetTyp', ($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']), '', true, '', ($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']), '', '', '', $hstyp, '', '' , array() , '' , 'AWIS', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');

echo '<script type="text/javascript">'.PHP_EOL;
echo '$(".AngefordertFuer").change(function(){'.PHP_EOL;
echo 'var angefordert = $(this).val();'.PHP_EOL;
echo 'if(angefordert == "")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$(".HS").hide();';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").attr("checked", false);'.PHP_EOL;
echo '$( document).ready(function() {$(".HSZ").change()});';
echo '$(".HSZ").hide();';
$Block = '160';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").attr("checked", false);'.PHP_EOL;
echo '$( document).ready(function() {$(".HS").change()});';
echo '} else if(angefordert == 1 || angefordert == 3)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '$(".HSZ").show();';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").attr("checked", false);'.PHP_EOL;
echo '$( document).ready(function() {$(".HSZ").change()});';
echo '$(".HS").hide();';
$Block = '180';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").attr("checked", false);'.PHP_EOL;
echo '$( document).ready(function() {$(".HS").change()});';
echo '} else {'.PHP_EOL;
echo '$(".HS").show();';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").attr("checked", false);'.PHP_EOL;
echo '$( document).ready(function() {$(".HS").change()});';
echo '$(".HSZ").hide();';
$Block = '160';
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").attr("checked", false);'.PHP_EOL;
echo '$( document).ready(function() {$(".HSZ").change()});';
echo '}'.PHP_EOL;
echo '})';
echo '</script>'.PHP_EOL;

$Block = '180';
echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() { ';
echo '$(".Block'.$Block.'").hide();';
echo '});' . PHP_EOL;

echo '$(".HSZ").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==true)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetTyp").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetTyp").addClass("InputTextPflicht")'.PHP_EOL;
$Block = '150';
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputTextPflicht")'.PHP_EOL;
$Block = '180';
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetTyp").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetTyp").addClass("InputText")'.PHP_EOL;
$Block = '170';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '160';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '180';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '150';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()=="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}';
echo '}';
echo '}';
echo '}';
echo '}';
echo '});'.PHP_EOL;
echo '$(".PG").change(function(){'.PHP_EOL;
$Block = '170';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '160';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '180';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '150';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()=="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;
echo '$( document).ready(function() {$(".HSZ").change()});';
echo '</script>'.PHP_EOL;


$Block = '160';
$Form->ZeileStart('', '', 'HS');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale'])?'on':'off'), 30, true, 'on', '','', '', 'HS');
$Form->Erstelle_TextLabel('Headset Filiale', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MONITOR']);
$Form->ZeileEnde();

$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel('Nebenstelle: ', 225, '', '', '');
$SQL_NUM = 'select key, value from (Select rownum key, rownum value from all_objects) where key >= 5 and key <= 20';
$Form->Erstelle_SelectFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle', ($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']), '', true, $SQL_NUM, $rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], $rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel('Telefontyp: ', 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp']:''), 50, 550, true, '', '', '', 'T');
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');

echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() { ';
echo '$(".Block'.$Block.'").hide();';
echo '});' . PHP_EOL;

echo '$(".HS").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==true)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").addClass("InputTextPflicht")'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").addClass("InputTextPflicht")'.PHP_EOL;
$Block = '150';
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputTextPflicht")'.PHP_EOL;
$Block = '160';
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").addClass("InputText")'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").addClass("InputText")'.PHP_EOL;
$Block = '170';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '160';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '180';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '150';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()=="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}';
echo '}';
echo '}';
echo '}';
echo '}';
echo '});'.PHP_EOL;
echo '$(".PG").change(function(){'.PHP_EOL;
$Block = '170';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '160';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '180';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '150';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()=="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;
echo '$( document).ready(function() {$(".HS").change()});';
echo '</script>'.PHP_EOL;

$Block = '170';
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-Repeater',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater'])?'on':'off'), 30, true, 'on', '','', '', 'R');
$Form->Erstelle_TextLabel('Repeater', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MONITOR']);
$Form->ZeileEnde();

$Form->SchreibeHTMLCode('<div style="visibility: hidden" class="Block' . $Block. ' ">');
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel('Nebenstelle: ', 225, '', '', '');
$SQL_NUM = 'select key, value from (Select rownum key, rownum value from all_objects) where key >= 5 and key <= 20';
$Form->Erstelle_SelectFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle', ($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']), '', true, $SQL_NUM, $rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], $rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel('Telefontyp: ', 225, '', '', '');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp']:''), 50, 550, true, '', '', '', 'T');
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 75, '', '', '');
$Form->Erstelle_TextLabel('Ist mit dem Telefon ein Headset verbunden? ', 225, '', '', '');
$Daten = explode('|', $AWISSprachKonserven['OTT']['Ja_Nein']);
$Form->Erstelle_SelectFeld('!DETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetVerbunden', ($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']), '', true, '', $rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], $rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', $Daten);
$Form->ZeileEnde();
$Form->SchreibeHTMLCode('</div>');

echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() { ';
echo '$(".Block'.$Block.'").hide();';
echo '});' . PHP_EOL;

echo '$(".R").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==true)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").addClass("InputTextPflicht")'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").addClass("InputTextPflicht")'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetVerbunden").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetVerbunden").addClass("InputTextPflicht")'.PHP_EOL;
$Block = '150';
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputTextPflicht")'.PHP_EOL;
$Block = '170';
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Nebenstelle").addClass("InputText")'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefontyp").addClass("InputText")'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetVerbunden").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetVerbunden").addClass("InputText")'.PHP_EOL;
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '160';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '180';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '150';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()=="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}';
echo '}';
echo '}';
echo '}';
echo '}';
echo '});'.PHP_EOL;
echo '$(".PG").change(function(){'.PHP_EOL;
$Block = '170';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '160';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '180';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==false) '.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '150';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon").prop("checked")==false && $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()=="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;
echo '$( document).ready(function() {$(".R").change()});';
echo '</script>'.PHP_EOL;

$Form->ZeileStart();
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Sonstiges:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_GERAETE_SONSTIGES']);
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges']:''), 600, 60, 3, true, '','','','','PG');
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 75,'','','');
$Form->Erstelle_TextLabel('Verwendungszweck:', 225,'','','');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Zweck', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck']:''), 50, 550, true, '', '', '', 'T');
$Form->ZeileEnde();

echo '<script type="text/javascript">'.PHP_EOL;
echo '$( document ).ready(function() {' . PHP_EOL ;
echo '$(".Block'.$Block.'").hide();' . PHP_EOL;
echo '});';
echo '$(".PG").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Maus").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Tastatur").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Monitor").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Telefon").prop("checked")==true || $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges").val()!="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputTextPflicht")'.PHP_EOL;
echo '}else';
echo '{'.PHP_EOL;
$Block = '170';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Repeater").prop("checked")==false)'.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '160';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetFiliale").prop("checked")==false)'.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '180';
echo 'if ($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-HeadsetZentrale").prop("checked")==false)'.PHP_EOL;
echo '{'.PHP_EOL;
$Block = '150';
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Zweck").addClass("InputText")'.PHP_EOL;
echo '}';
echo '}';
echo '}';
echo '}';
echo '});'.PHP_EOL;
echo '$( document).ready(function() {$(".PG").change()});';
echo '</script>'.PHP_EOL;

//**************************************************
// Bemerkungen
//**************************************************
$Block='900';

$Form->ZeileStart();
$Form->Erstelle_TextLabel('Allgemeine Bemerkungen:', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_BEMERKUNGEN_ALLG']);
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']:''), 600, 60, 3, true);
$Form->ZeileEnde();
?>