<?php

require_once('awisFilialen.inc');

global $AWISCursorPosition;
global $SRFunktionen;

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();

    $RechtPlanung =$AWISBenutzer->HatDasRecht(11004);

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('WTE', '%');
    $TextKonserven[] = array('WPO', 'WPO_TERMINVON');
    $TextKonserven[] = array('WPO', 'WPO_TERMINBIS');
    $TextKonserven[] = array('WPO', 'WPOZEITSTD');
    $TextKonserven[] = array('KOV', 'KOV_KON_KEY');
    $TextKonserven[] = array('KOV', 'KOV_WOCHE');
    $TextKonserven[] = array('KOV', 'KOV_JAHR');
    $TextKonserven[] = array('FEB', 'FEB_BEZEICHNUNG');
    $TextKonserven[] = array('Wort', 'Wochentag_%');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'VERFUEGBARKEIT');
    $TextKonserven[] = array('Wort', 'ATUNR');
    $TextKonserven[] = array('Wort', 'Bezeichnung');
    $TextKonserven[] = array('Wort', 'Umsatz');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht11008 = $AWISBenutzer->HatDasRecht(11008);
    if ($Recht11008 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SRTermine'));
    if (!isset($Param['FEB_KEY'])) {
        $Param['FEB_KEY'] = '';
        $Param['KOV_WOCHE'] = date('W');
        $Param['KOV_JAHR'] = date('Y');
    }
    if (isset($_POST['sucFEB_KEY'])) {
        $Param['FEB_KEY'] = $_POST['sucFEB_KEY'];
        $Param['KOV_WOCHE'] = $_POST['sucKOV_WOCHE'];
        $Param['KOV_JAHR'] = $_POST['sucKOV_JAHR'];
    }

    $Recht11000 = $AWISBenutzer->HatDasRecht(11000);
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./srpep_Main.php?cmdAktion=Termine>");

    $Form->Formular_Start();
    $Form->FormularBereichStart();
    $Form->FormularBereichInhaltStart('Mir zugeordnete Termine', true, 10);

    //*************************************************************************
    // Listenansicht der anstehenden SR-Termine f�r den User
    //*************************************************************************
    $SQL = 'select wte_key, WTE_KUNDENFZVON, WTE_KUNDENFZBIS, kon_name1, kon_name2, wpo_ast_atunr, wpo_bezeichnung, wpo_terminvon, wpo_terminbis, wpo_zeitmin, wte_fil_id, wpo_vk';
    $SQL .= ' from kontakte ';
    $SQL .= ' inner join werkstatttermineplanungen';
    $SQL .= ' on werkstatttermineplanungen.wtp_kon_key = kontakte.kon_key';
    $SQL .= ' inner join werkstattterminepositionen';
    $SQL .= ' on werkstattterminepositionen.wpo_key = werkstatttermineplanungen.wtp_wpo_key';
    $SQL .= ' inner join werkstatttermine ';
    $SQL .= ' on werkstatttermine.wte_key = werkstattterminepositionen.wpo_wte_key';
    $SQL .= ' where wpo_terminvon >= trunc(SYSDATE) ';
    $SQL .= ' and kon_key = ' . $DB->WertSetzen('WTE', 'N0', $AWISBenutzer->BenutzerKontaktKEY());
    $SQL .= ' order by wpo_terminvon asc, wte_fil_id asc, wpo_ast_atunr asc ';

    $rsWTE = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('WTE'));

    if ($rsWTE->AnzahlDatensaetze() > 0) {

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['WTE_FIL_ID'], 50);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['ATUNR'], 120);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Bezeichnung'], 450);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_TERMINVON'], 180);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_TERMINBIS'], 180);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPOZEITSTD'], 100);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Umsatz'], 100);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsWTE->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('#WTE_FIL_ID', $rsWTE->FeldInhalt('WTE_FIL_ID'), 10, 50, false, ($DS % 2), '', '', 'N0');
            $Form->Erstelle_ListenFeld('#WPO_AST_ATUNR', $rsWTE->FeldInhalt('WPO_AST_ATUNR'), 10, 120, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#WPO_BEZEICHNUNG', $rsWTE->FeldInhalt('WPO_BEZEICHNUNG'), 10, 450, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#WPO_TERMINVON', $rsWTE->FeldInhalt('WPO_TERMINVON'), 10, 180, false, ($DS % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('#WPO_TERMINBIS', $rsWTE->FeldInhalt('WPO_TERMINBIS'), 10, 180, false, ($DS % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('#ZEITGESAMT', $Form->Format('N1', ($rsWTE->FeldInhalt('WPO_ZEITMIN') / 60)), 10, 100, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#UMSATZ', $rsWTE->FeldInhalt('WPO_VK'), 10, 100, false, ($DS % 2), '', '', 'T');
            $SRFunktionen->ErstelleAuftragdetailInfofeld($rsWTE->FeldInhalt('WTE_KEY'),$rsWTE->FeldInhalt('WTE_KUNDENFZVON'),$rsWTE->FeldInhalt('WTE_KUNDENFZBIS'));
            $Form->ZeileEnde();

            $rsWTE->DSWeiter();
            $DS++;
        }
    } else {
        $Form->Hinweistext('Keine Datens�tze gefunden', awisFormular::HINWEISTEXT_HINWEIS);
    }

    $Form->FormularBereichInhaltEnde();
    $Form->FormularBereichEnde();

    $Form->FormularBereichStart();
    $Form->FormularBereichInhaltStart('Termine meiner Filialen', true);
    //*************************************************************************
    // Listenansicht der anstehenden SR-Termine f�r den User
    //*************************************************************************
    $SQL = 'select  wpo_ast_atunr, wpo_bezeichnung, wpo_terminvon, wpo_terminbis, wpo_zeitmin, wte_key, WTE_KUNDENFZVON, wte_fil_id, wpo_vk ';
    $SQL .= ' from werkstattterminepositionen ';
    $SQL .= ' left join werkstatttermineplanungen';
    $SQL .= ' on werkstattterminepositionen.wpo_key = werkstatttermineplanungen.wtp_wpo_key';
    $SQL .= ' inner join werkstatttermine ';
    $SQL .= ' on werkstatttermine.wte_key = werkstattterminepositionen.wpo_wte_key';
    $SQL .= ' where wpo_terminvon >= trunc(SYSDATE) ';
    $SQL .= ' AND WTP_KEY is null ';
    if(count($SRFunktionen->FilialenDesUsers()) > 0){
        $SQL .= ' and WTE_FIL_ID  in (' . implode(',',$SRFunktionen->FilialenDesUsers()) .' ) ';
    }

    $SQL .= ' order by wpo_terminvon asc, wte_fil_id asc, wpo_ast_atunr asc ';

    $rsWTE = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('WTE'));

    if ($rsWTE->AnzahlDatensaetze() > 0) {

        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTE']['WTE_FIL_ID'], 50);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['ATUNR'], 120);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Bezeichnung'], 450);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_TERMINVON'], 180);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPO_TERMINBIS'], 180);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WPO']['WPOZEITSTD'], 100);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Umsatz'], 100);
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsWTE->EOF()) {
            $Form->ZeileStart();
            $Link='';
            if(($RechtPlanung&2)==2){
                $Link = './srpep_Main.php?cmdAktion=Planung&FIL_ID='.$rsWTE->FeldInhalt('WTE_FIL_ID').'&TAG='.$Form->Format('D',$rsWTE->FeldInhalt('WPO_TERMINVON'));
            }

            $Form->Erstelle_ListenFeld('#WTE_FIL_ID', $rsWTE->FeldInhalt('WTE_FIL_ID'), 10, 50, false, ($DS % 2), '', $Link, 'N0');
            $Form->Erstelle_ListenFeld('#WPO_AST_ATUNR', $rsWTE->FeldInhalt('WPO_AST_ATUNR'), 10, 120, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#WPO_BEZEICHNUNG', $rsWTE->FeldInhalt('WPO_BEZEICHNUNG'), 10, 450, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#WPO_TERMINVON', $rsWTE->FeldInhalt('WPO_TERMINVON'), 10, 180, false, ($DS % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('#WPO_TERMINBIS', $rsWTE->FeldInhalt('WPO_TERMINBIS'), 10, 180, false, ($DS % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('#ZEITGESAMT', $Form->Format('N1', ($rsWTE->FeldInhalt('WPO_ZEITMIN') / 60)), 10, 100, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#UMSATZ', $rsWTE->FeldInhalt('WPO_VK'), 10, 100, false, ($DS % 2), '', '', 'T');

            $SRFunktionen->ErstelleAuftragdetailInfofeld($rsWTE->FeldInhalt('WTE_KEY'),$rsWTE->FeldInhalt('WTE_KUNDENFZVON'),$rsWTE->FeldInhalt('WTE_KUNDENFZBIS'));
            $Form->ZeileEnde();


            $rsWTE->DSWeiter();
            $DS++;
        }
    } else {
        $Form->Hinweistext('Keine Datens�tze gefunden', awisFormular::HINWEISTEXT_HINWEIS);
    }
    $Form->FormularBereichInhaltEnde();
    $Form->FormularBereichEnde();

    $Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Link = './srpep_bericht_3.php';
    $Form->Schaltflaeche('href', 'cmdDruckenM', $Link, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
    $Form->SchaltflaechenEnde();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200912171119");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 3, "200912171120");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
