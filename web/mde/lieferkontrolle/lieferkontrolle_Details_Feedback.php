<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('LKK','%');
    $TextKonserven[]=array('LKF','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','lbl_Hilfe');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Liste','lst_Zeitangabe');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Wort','Status');
    $TextKonserven[]=array('Wort','AlleAnzeigen');
    $TextKonserven[]=array('Wort','Abschliessen');
    $TextKonserven[]=array('Wort','Abbrechen');
    $TextKonserven[]=array('Wort','Abgeschlossen');
    $TextKonserven[]=array('Wort','NichtAbgeschlossen');
    $TextKonserven[]=array('Wort','PDFErzeugen');
    $TextKonserven[]=array('Wort','Zwangsabschluss');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $AWISSprache = $AWISBenutzer->BenutzerSprache();

    if($AWISSprache=='')
    {
        $AWISSprache='DE';
    }

    $Recht3623 = $AWISBenutzer->HatDasRecht(3623);
    if($Recht3623==0)
    {
        $Form->Fehler_KeineRechte();
    }

    $Form->Formular_Start();
    $OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']; //Value-Feld muss leer sein bei Pflicht-Selects

    $SQL = '';
    $SQL .= 'SELECT lkf.*, lkk.LKK_STATUS FROM ';
    $SQL .= ' LIEFERKONTROLLENFEEDBACK lkf';
    $SQL .= ' RIGHT JOIN LIEFERKONTROLLENKOPF lkk';
    $SQL .= ' ON lkf.LKF_LKK_KEY = lkk.LKK_KEY';
    $SQL .= ' WHERE LKK_KEY = '.$DB->WertSetzen('LKF','Z',$AWIS_KEY1);

    $rsLKF = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('LKF'));

    $FeedbackBearbeitbar = true;

    if($rsLKF->FeldInhalt('LKF_STATUS')==1 or $rsLKF->FeldInhalt('LKK_STATUS')=='Z' or $rsLKF->FeldInhalt('LKK_STATUS')=='A'){
        $FeedbackBearbeitbar = false;
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_FEEDBACK_ANLIEFERUNG'], 400, 'font-weight: bold');
    $Form->ZeileEnde();

    if($rsLKF->AnzahlDatensaetze()>0) {
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKF->FeldInhalt('LKF_USER'));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsLKF->FeldInhalt('LKF_USERDAT'));
        $Form->InfoZeile($Felder,'');
    } else {
        $Form->Trennzeile('O');
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_ZEIT'] . ':<font color=#FF0000>*</font>', 300);
    $Form->Erstelle_SelectFeld('!LKF_ZEIT', $rsLKF->FeldInhalt('LKF_ZEIT'), '', $FeedbackBearbeitbar, '', $OptionBitteWaehlen, '', '', '', explode('|', $AWISSprachKonserven['Liste']['lst_Zeitangabe']));
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_DATUM'] . ':<font color=#FF0000>*</font>', 300);
    $Form->Erstelle_TextFeld('!LKF_DATUM', $rsLKF->FeldInhalt('LKF_DATUM'), 13, '', $FeedbackBearbeitbar, '', '', '', 'D','L','','',0,'','','','','',true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_ZUFRIEDEN'] . '<font color=#FF0000>*</font>', 300);
    $Form->Erstelle_SelectFeld('!LKF_ZUFRIEDEN', $rsLKF->FeldInhalt('LKF_ZUFRIEDEN'), '', $FeedbackBearbeitbar, '', $OptionBitteWaehlen, '', '', '', explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']));
    $Form->ZeileEnde();

    $Form->Trennzeile('O');
    //**************************************************
    // Optionales
    //**************************************************

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_OPTIONAL'], 400, 'font-weight: bold');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_SCHADEN'], 300);
    $Form->Erstelle_SelectFeld('LKF_SCHADEN', $rsLKF->FeldInhalt('LKF_SCHADEN'), '', $FeedbackBearbeitbar, '', '-1'.$OptionBitteWaehlen, '', '', '', explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']));
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_TEXT'] . ':', 300);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_Textarea('LKF_TEXT', $rsLKF->FeldInhalt('LKF_TEXT'), 400, 50, 5, $FeedbackBearbeitbar, '', '', '', '', '', 500);
    $Form->ZeileEnde();

    $Form->Trennzeile('L');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LKF']['LKF_DISCLAIMER'], 600,'font-size:12px; font-weight: bold');
    $Form->ZeileEnde();

    $Form->Erstelle_HiddenFeld('LKF_FEEDBACK','1');

    $Form->Formular_Ende();
}

catch (awisException $ex)
{
    echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
    echo 'allg. Fehler:'.$ex->getMessage();
}
?>