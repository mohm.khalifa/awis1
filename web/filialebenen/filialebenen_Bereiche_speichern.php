<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());


//***********************************************
// Filialebenenrollenbereiche
//***********************************************
awis_Debug(1,$_POST);

$AWIS_KEY1=$_POST['txtFRB_KEY'];

$Felder = awis_NameInArray($_POST, 'txtFRB_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('FRB','FRB_%');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';
	
	if($_POST['txtFRB_KEY']=='0')
	{
		$Speichern = true;
		
		if($Speichern)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO filialebenenrollenbereiche';
			$SQL .= '(FRB_BEZEICHNUNG,FRB_BEMERKUNG';
			$SQL .= ',FRB_USER, FRB_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtFRB_BEZEICHNUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtFRB_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('adressen_stammsaten_speichern.php',1,$awisDBError['message'],'200804011001');
				die();
			}
			$SQL = 'SELECT seq_FRB_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = awisOpenRecordset($con,$SQL);
			$AWIS_KEY1=$rsKey['KEY'][0];
		}
	}
	else 					// gešnderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsFRB = awisOpenRecordset($con,'SELECT * FROM filialebenenrollenbereiche WHERE FRB_key=' . $_POST['txtFRB_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsFRB[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsFRB['FRB_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE filialebenenrollenbereiche SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', FRB_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', FRB_userdat=sysdate';
			$SQL .= ' WHERE FRB_key=0' . $_POST['txtFRB_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('FilialebenenBereiche',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}//***********************************************
// Filialebenenrollenbereiche
//***********************************************
awis_Debug(1,$_POST);
$Felder = awis_NameInArray($_POST, 'txtFBR_',1,1);
if($Felder!='')
{
	$AWIS_KEY2=$_POST['txtFRB_KEY'];
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('FRB','FBR_%');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';
	
	if($_POST['txtFBR_KEY']=='0')
	{
		$Speichern = true;
		
		if($Speichern)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO filialebenenrollenberzugriffe';
			$SQL .= '(FBR_FRB_KEY,FBR_XBN_KEY,FBR_RECHT';
			$SQL .= ',FBR_USER, FBR_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $AWIS_KEY1;
			$SQL .= ', ' . awis_FeldInhaltFormat('N0',$_POST['txtFBR_XBN_KEY'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFBR_RECHT'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('FilialebenenRollenBereiche',1,$awisDBError['message'],'200806131400');
				awis_Debug(1,$SQL);
				die();
			}
			$SQL = 'SELECT seq_FBR_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = awisOpenRecordset($con,$SQL);
			$AWIS_KEY2=$rsKey['KEY'][0];
		}
	}
	else 					// gešnderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsFRB = awisOpenRecordset($con,'SELECT * FROM filialebenenrollenberzugriffe WHERE FBR_key=' . $_POST['txtFBR_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsFRB[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsFRB['FBR_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE filialebenenrollenberzugriffe SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', FBR_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', FBR_userdat=sysdate';
			$SQL .= ' WHERE FBR_key=0' . $_POST['txtFBR_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('FilialebenenBereiche',1,'Fehler beim Speichern','200816131401');
				awis_Debug(1,$SQL);
			}
		}

	}
}
?>