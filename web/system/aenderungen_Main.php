<html>
<head>

<title>Awis - ATU Datenänderungsauftrag</title>

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>
</head>

<body>
<?
global $PHP_AUTH_USER;			// USER Kennung
global $HTTP_GET_VARS;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung möglich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}


$RechteStufe = awisBenutzerRecht($con,8);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Systemänderungen',"$PHP_AUTH_USER",'','','');
    die("Keine ausreichenden Rechte!");
}

awis_RegisterErstellen(9999, $con);

print "<br><hr><input type=image alt=Zurück src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
if($RechteStufe & 4)
{
	print "&nbsp;<input type=image accesskey=n alt='Hinzufügen (Alt+N)' src=/bilder/plus.png name=cmdDSNeu onclick=location.href='./aenderungen_Main.php?cmdAktion=" . $HTTP_GET_VARS["cmdAktion"] . "&txtXSA_KEY=-1&txtXSD_KEY=-1'>";
}

print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

?>
</body>
</html>

