<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once('php_sql_wandler_functions.php');

global $AWISCursorPosition;		// Aus AWISFormular


?>
</head>
<body> 
<?php 
include ("awisHeader3.inc");	// Kopfzeile
try
{
    $TextKonserven = array();
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('ZZZ','%');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei(3) .">";

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
}
catch (Exception $ex)
{
    die($ex->getMessage());
}

try
{
    if($AWISBenutzer->HatDasRecht(37000)==0)
    {
        $Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"201409101521");
        die();
    }
    
    if($AWISCursorPosition!=='')
    {
        $Form->SchreibeHTMLCode('<Script Language=JavaScript>');
        $Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
        $Form->SchreibeHTMLCode('</Script>');
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201409101522");
    }
    else
    {
        echo 'AWIS: '.$ex->getMessage();
    }
}

//Jquery f�r Zeile 2

echo "<script>";

echo "$(document).ready(function(){".PHP_EOL;
echo "  toggleDivs();".PHP_EOL;
echo "  $('#txtBindevariablebenutzen').change(function(){".PHP_EOL;
echo "      toggleDivs();".PHP_EOL;
echo "  });".PHP_EOL;
echo "  $('#txtAusfuehrenRSerzeugen').change(function(){".PHP_EOL;
echo "      toggleDivs();".PHP_EOL;
echo "  });".PHP_EOL;
echo "  function toggleDivs(){".PHP_EOL;
echo "      if( $('#txtBindevariablebenutzen').prop('checked') == true){".PHP_EOL;
echo "          $('#Bindevariablensatzdiv').css('visibility', 'visible');".PHP_EOL;
echo "      }else{".PHP_EOL;
echo "          $('#Bindevariablensatzdiv').css('visibility', 'hidden');".PHP_EOL;
echo "      }".PHP_EOL;
echo "      if( $('#txtAusfuehrenRSerzeugen').prop('checked') == true){".PHP_EOL;
echo "          $('#Oeffnungsartdiv').css('visibility', 'visible');".PHP_EOL;
echo "      }else{".PHP_EOL;
echo "          $('#Oeffnungsartdiv').css('visibility', 'hidden');".PHP_EOL;
echo "      }".PHP_EOL;
echo "      if(($('#txtBindevariablebenutzen').prop('checked') == true) || ($('#txtAusfuehrenRSerzeugen').prop('checked') ==true)){".PHP_EOL;
echo "          $('#zeile2').show();".PHP_EOL;
echo "      }else{".PHP_EOL;
echo "          $('#zeile2').hide();".PHP_EOL;
echo "      }".PHP_EOL;
echo "  }".PHP_EOL;
echo "})".PHP_EOL;

echo "</script>";

//Ende JQuery

$Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./php_sql_wandler_Main.php>");

$Form->Formular_Start();
$Form->ZeileStart();
$Form->SchreibeHTMLCode("<br>");
$Form->ZeileEnde();
$Form->ZeileStart();

$Sprachen = explode('|',$AWISSprachKonserven['ZZZ']['ZZZ_PHPSQL']);

If (isset($_POST['txtSprachwahl']) && $_POST['txtSprachwahl'] == "2")
{
    $Sprachen = array_reverse($Sprachen);
}

//M�gliche Felder beim Selectfeld

$Daten = explode('|',$AWISSprachKonserven['ZZZ']['ZZZ_RS_AUSF']);

//Alle Textfelder, Textlabels, etc. �ber der Textarea

$Form->Erstelle_TextLabel("Variable:", 70);
$Form->Erstelle_TextFeld("Variable", (isset($_POST['txtVariable'])?trim($_POST['txtVariable']):"SQL"), 20, 160, true);

$Form->Erstelle_TextLabel("Bindevariablen benutzen", 170, 'margin-left: 10px');
$Form->Erstelle_Checkbox("Bindevariablebenutzen", (isset($_POST['txtBindevariablebenutzen'])?($_POST['txtBindevariablebenutzen']):('')),25, true);

$Form->Erstelle_TextLabel("Dom�nenbenutzer ersetzen", 200,null,null,'Ersetzt den Dom�nenbenutzernamen durch \'$AWISBenutzer->BenutzerName()\'');
$Form->Erstelle_Checkbox("Nutzername", isset($_POST["txtNutzername"])?$_POST["txtNutzername"]:false, 20, true);

$Form->Erstelle_TextLabel("Ausf�hren/RecordSet erzeugen", 215);
$Form->Erstelle_Checkbox("AusfuehrenRSerzeugen", (isset($_POST['txtAusfuehrenRSerzeugen'])?($_POST['txtAusfuehrenRSerzeugen']):('')) ,20, true);

//Zeile 2 (�ffnet sich, wenn "Bindevariablen benutzen" oder/und "Ausf�hren/RecordSet erzeugen" an ist)
$Form->ZeileStart('width= 100%','zeile2');

$Form->SchreibeHTMLCode("<span id='Bindevariablensatzdiv'>");
$Form->Erstelle_TextLabel("Bindevariablensatz:", 140,'margin-left: 239px');
$Form->Erstelle_TextFeld("Bindevariablensatz",'',15,175, true, 'margin-right: 101px','','','T','','', (isset($_POST["txtBindevariablensatz"])?($_POST["txtBindevariablensatz"]):('')));
$Form->SchreibeHTMLCode("</span>");

$Form->SchreibeHTMLCode("<span id='Oeffnungsartdiv'>");
$Form->Erstelle_TextLabel("�ffnungsart:", 94);
$Form->Erstelle_SelectFeld("Oeffnungsart",(isset($_POST['txtOeffnungsart'])?($_POST['txtOeffnungsart']):('')),150,true,'','::Bitte w�hlen::','','','',$Daten);
$Form->SchreibeHTMLCode("</span>");

$Form->ZeileEnde();


If (isset($_POST['txtSprachwahl']) && $_POST['txtSprachwahl'] == 1){
    If (isset($_POST['txtVariable']) && trim($_POST['txtVariable']) <> "") 
    {
        $Variable = str_replace("$", "", trim($_POST['txtVariable']));
    }
    elseif (isset($_POST['txtVariable']) && trim($_POST['txtVariable']) == "" )
    {
        $Variable = "SQL";
    }
}
else
{
    $Variable = '';
}
    
If (isset($_POST["txtNutzername"]))
{
    $NutzerErsetzen = true;
}
else
{
    $NutzerErsetzen = false;
}


$Form->ZeileEnde();
$Form->ZeileStart();
$Form->ZeileEnde();
$Form->ZeileStart();
$Form->Erstelle_SelectFeld("Sprachwahl", 0, 70,true,"","","","","",$Sprachen);
$Form->Erstelle_Textarea("PHPSQL", (isset($_POST['txtPHPSQL'])?$_POST['txtPHPSQL']:""), 150, 150, 16,true);
$Form->ZeileEnde();

If (isset($_POST["txtSprachwahl"]) && isset($_POST["txtPHPSQL"]) && trim($_POST['txtPHPSQL' ]) <> '')
{
    $Form->ZeileStart();
    $Form->SchreibeHTMLCode("<br> ");
    $Form->ZeileEnde();
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel("&nbsp;Ausgabe:", 70);
    If (isset($_POST["txtSprachwahl"]) && $_POST["txtSprachwahl"] == "1" )
    {
        $Form->Erstelle_Textarea("Umgewandelt", SQLzuPHP($_POST["txtPHPSQL"], $Variable, $NutzerErsetzen), 150, 150, 16,true);
    }
    elseif (isset($_POST["txtSprachwahl"]) && $_POST["txtSprachwahl"] == "2")
    {
        $Form->Erstelle_Textarea("Umgewandelt", PHPzuSQL($_POST["txtPHPSQL"], ''), 150, 150, 16,true);
    }
    $Form->ZeileEnde();
}

$Form->Formular_Ende();

$Form->SchaltflaechenStart();
$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
$Form->SchaltflaechenEnde();

$Form->SchreibeHTMLCode('</form>');
?>
</body>
</html>