<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $con;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_AWIS_Allgemein','OAA_%');
$TextKonserven[]=array('TITEL','tit_Operating_AWIS_Allgemein');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Mail_SB');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_AWIS_Allgemein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_AWIS_Allgemein'].'</title>';
?>
</head>
<body>
<?php

$Recht3121 = awisBenutzerRecht($con,3121);
if($Recht3121==0)
{
	awisEreignis(3,1000,'Operating-AWIS',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$MNU_ID='';

if (isset($_REQUEST['MNU_ID']))
{
	$MNU_ID=$_REQUEST['MNU_ID'];
}

$MNU_ITEM_ID='';

if (isset($_REQUEST['MNU_ITEM_ID']))
{
	$MNU_ITEM_ID=$_REQUEST['MNU_ITEM_ID'];
}

$JOBID='';

if (isset($_REQUEST['JOBID']))
{
	$JOBID=$_REQUEST['JOBID'];
}

$JOBNAME='';

if (isset($_REQUEST['JOBNAME']))
{
	$JOBID=$_REQUEST['JOBNAME'];
}

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($con,$SQL);

echo '<form name=frmOperating_Allgemein action=./operating_AWIS_Main.php?cmdAktion=Allgemein method=POST>';

echo "<p><center><h2><b>Allgemeine-&Uuml;bersicht</b></h2></center><p>";

If(($Recht3121&1)==1){
	$MNU_LINK="<li><a href=./operating_AWIS_Main.php?cmdAktion=Allgemein&MNU_ID=1>AWIS</a></li><p>";
	echo $MNU_LINK;
}


if ($MNU_ID=='1')
{
	If(($Recht3121&4)==4){
		$MNU_ITEM_LINK="./operating_AWIS_Main.php?cmdAktion=Allgemein&MNU_ID=".$MNU_ID."&MNU_ITEM_ID=2";
		echo "<p><ul type='square'><li><a href=".$MNU_ITEM_LINK.">Scheduler-&Uuml;bersicht</a></li></ul>";
	}
	
	if ($MNU_ITEM_ID=='2')
	{
		//$SQL="SELECT * FROM ATU2EXP_SCHEDULER WHERE ENABLED='TRUE'";
		
		$Recht3127=awisBenutzerRecht($con,3127);
		
		if(($Recht3127&8)==8)
		{
			$SQL="SELECT * FROM AWIS_SCHEDULER ";			
		}
		else {
			$SQL="SELECT * FROM AWIS_SCHEDULER ";
			$SQL .="WHERE (OWNER, JOB_NAME) IN (SELECT JOB_OWNER, JOB_NAME FROM AWIS_SCHEDULER_OPERATING WHERE BITAND(AWIS_RECHT_3127_BIT,".$Recht3127.")>0) ";
		}
		
		if(!isset($_GET['Sort']))
		{
			$SORT_LINK='';
			$SQL .= ' ORDER BY NEXT_START_UTS, OWNER, JOB_NAME';
		}
		else
		{
			$SORT_LINK='&Sort='.$_GET['Sort'];
			$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
		}
		
		$rsAES=awisOpenRecordset($con,$SQL);
		$rsAESZeilen=$awisRSZeilen;

//		awis_Debug(1,$SQL);
//		awis_Debug(1,$rsAES);
		
		if ($rsAESZeilen>0)
		{
			if ($JOBNAME==''){
				$JOB_LINK='';
			}
			else {
				$JOB_LINK="&JOBNAME=".$JOBNAME;
			}
				
			
			awis_FORM_FormularStart();
		
			awis_FORM_ZeileStart();
			If(($Recht3121&8)==8){
				awis_FORM_Erstelle_Liste_Ueberschrift('EXEC',40,'font-size: 85%; font_weight:bold;');
			}
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=OWNER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='OWNER'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_OWNER'],110,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=JOB_NAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='JOB_NAME'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_JOBNAME'],280,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=STATE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='STATE'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_STATE'],90,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=NEXT_START_UTS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='NEXT_START_UTS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_NEXT_START'],150,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=LAST_START_UTS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_START_UTS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_LAST_START'],150,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=LAST_OPERATION'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_OPERATION'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_LAST_OPERATION'],110,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=LAST_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_STATUS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_LAST_STATUS'],110,'font-size: 85%;',$Link);
				$Link = $MNU_ITEM_LINK.$JOB_LINK.'&Sort=COMMENTS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='COMMENTS'))?'~':'');
			awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Operating_AWIS_Allgemein']['OAA_COMMENT'],150,'font-size: 85%;',$Link);
			awis_FORM_ZeileEnde();
			
			for($AESZeile=0;$AESZeile<$rsAESZeilen;$AESZeile++)
			{
				$JOBNAME=urlencode($rsAES['OWNER'][$AESZeile]).'.'.urlencode($rsAES['JOB_NAME'][$AESZeile]);
				$Link=$MNU_ITEM_LINK."&JOBNAME=".$JOBNAME.$SORT_LINK.'#EXEC_'.$JOBNAME;
				
				$Hintergrund='';
				
				if ($rsAES['STATE'][$AESZeile]=='RUNNING')
				{
					$Hintergrund='background-color: FFFF00';
				}elseif ($rsAES['LAST_STATUS'][$AESZeile]=='FAILED')
				{						
					$Hintergrund='background-color: FF0000';
				}elseif ($rsAES['LAST_STATUS'][$AESZeile]=='SUCCEEDED' AND $rsAES['STATE'][$AESZeile]!='DISABLED')
				{
					$Hintergrund='background-color: 00FF00';
				}else{
					if($rsAES['LAST_STATUS'][$AESZeile]=='SUCCEEDED' AND $rsAES['RUN_TODAY'][$AESZeile]==1)
					{
						$Hintergrund='background-color: 00FF00';
					}else{
						$Hintergrund='';
					}
				}
				
				awis_FORM_ZeileStart('margin: 0px 0px 5px 0px;');
				If(($Recht3121&8)==8){
					$LinkRS="onclick=\"window.open('./operating_AWIS_Details_Allgemein_RunScheduler.php?JOBNAME=".$JOBNAME."','','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');"; 					
					awis_FORM_Erstelle_ListenBild('script','EXEC_'.$JOBNAME,'','/bilder/icon_go.png" '.$LinkRS,'AWIS - Scheduler-Job',($AESZeile%2),'','16','16',40,'C'); 					
				}					
				awis_FORM_Erstelle_ListenFeld('OWNER',$rsAES['OWNER'][$AESZeile],0,110,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('JOB_NAME',$rsAES['JOB_NAME'][$AESZeile],0,280,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('STATE',$rsAES['STATE'][$AESZeile],0,90,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('NEXT_START',$rsAES['NEXT_START'][$AESZeile],0,150,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'DU');
				awis_FORM_Erstelle_ListenFeld('LAST_START',$rsAES['LAST_START'][$AESZeile],0,150,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'DU');
				awis_FORM_Erstelle_ListenFeld('LAST_OPERATION',$rsAES['LAST_OPERATION'][$AESZeile],0,110,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('LAST_STATUS',$rsAES['LAST_STATUS'][$AESZeile],0,110,false,($AESZeile%2),'font-size: 85%;'.$Hintergrund,$Link,'T');
				awis_FORM_Erstelle_ListenFeld('COMMENTS',$rsAES['COMMENTS'][$AESZeile],0,150,false,($AESZeile%2),'font-size: 70%;'.$Hintergrund,$Link,'T');
				awis_FORM_ZeileEnde();
				
				if (isset($_GET['JOBNAME']) && $_GET['JOBNAME']===$JOBNAME)
				{
					$SCHEDJOB=explode('.',$JOBNAME);
					
					$BindeVariablen=array();
					$BindeVariablen['var_T_owner']=$SCHEDJOB[0];
					$BindeVariablen['var_T_job_name']=$SCHEDJOB[1];
					
					$SQL = 'SELECT * FROM ( ';
					$SQL .='SELECT AA.LOG_ID, AA.LOG_DATE, AA.JOB_CLASS, AA.OPERATION, AA.STATUS, ';
					$SQL .='AA.USER_NAME, AA.ADDITIONAL_INFO AS LOG_ADDITIONAL_INFO, BB.REQ_START_DATE, BB.ACTUAL_START_DATE, BB.RUN_DURATION, ';
					$SQL .='BB.INSTANCE_ID AS INST_ID, BB.SESSION_ID, BB.SLAVE_PID, BB.CPU_USED, BB.ERROR#, BB.ADDITIONAL_INFO AS RUN_ADDITIONAL_INFO, ';
					$SQL .='ROW_NUMBER() OVER (ORDER BY AA.LOG_ID DESC) AS RNUM ';
					$SQL .='FROM SYS.ALL_SCHEDULER_JOB_LOG AA, SYS.ALL_SCHEDULER_JOB_RUN_DETAILS BB ';
					$SQL .='WHERE AA.OWNER=:var_T_owner AND AA.JOB_NAME=:var_T_job_name ';
					$SQL .='AND AA.LOG_ID=BB.LOG_ID(+)) ';
					$SQL .='WHERE RNUM<11';
					
					$rsSHL=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
					$rsSHLZeilen=$awisRSZeilen;
					
					if ($rsSHLZeilen>0)
					{
						
						$style='font-size: 70%';
						
						for($SHLZeile=0;$SHLZeile<$rsSHLZeilen;$SHLZeile++)
						{
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_Liste_Ueberschrift('LOG_ID',80,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('LOG_DATE',200,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('USER_NAME',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('INST_ID',50,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('SID,SERIAL#',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('SLAVE_PID',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('JOB_CLASS',200,$style);							 	
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['LOG_ID'][$SHLZeile],'',80,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['LOG_DATE'][$SHLZeile],'',200,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['USER_NAME'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['INST_ID'][$SHLZeile],'',50,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['SESSION_ID'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['SLAVE_PID'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['JOB_CLASS'][$SHLZeile],'',200,'',($SHLZeile%2),$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_Liste_Ueberschrift('OPERATION',80,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('STATUS',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('REQ_START_DATE',300,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('ACTUAL_START_DATE',300,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('RUN_DURATION',100,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('CPU_USED',120,$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['OPERATION'][$SHLZeile],'',80,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['STATUS'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['REQ_START_DATE'][$SHLZeile],'',300,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['ACTUAL_START_DATE'][$SHLZeile],'',300,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['RUN_DURATION'][$SHLZeile],'',100,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['CPU_USED'][$SHLZeile],'',120,'',($SHLZeile%2),$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_Liste_Ueberschrift('ERROR#',80,$style);
								awis_FORM_Erstelle_Liste_Ueberschrift('LOG_ADDITIONAL_INFO',400,$style);
							 	awis_FORM_Erstelle_Liste_Ueberschrift('RUN_ADDITIONAL_INFO',400,$style);							 	
							awis_FORM_ZeileEnde();
							
							awis_FORM_ZeileStart();
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['ERROR#'][$SHLZeile],'',80,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['LOG_ADDITIONAL_INFO'][$SHLZeile],'',400,'',($SHLZeile%2),$style);
								awis_FORM_Erstelle_ListenFeld('',$rsSHL['RUN_ADDITIONAL_INFO'][$SHLZeile],'',400,'',($SHLZeile%2),$style);
							awis_FORM_ZeileEnde();
							
							awis_FORM_Trennzeile('D');
							
						}
						
					}
				
				}
							
			}
			
			awis_FORM_FormularEnde();
		}
		else {
			awis_FORM_Hinweistext('Sieh haben keine Rechte um diese Aktion auszuführen');
		}
	}
}

echo "</form>";

awislogoff($con);
?>
</body>
</html>

