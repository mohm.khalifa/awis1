<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Wort', 'txt_NaechsteFreieNummer');
$TextKonserven[] = array('AST', 'AST_ATUNR');

try {
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_453_2'));

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht458 = $AWISBenutzer->HatDasRecht(458);        // Recht f�r Werkzeuge
    if (($Recht458 & 2) == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if (isset($_POST['txtAST_ATUNR'])) {
        $Param['AST_ATUNR'] = strtoupper($_POST['txtAST_ATUNR']);
        $AWISBenutzer->ParameterSchreiben('Formular_453_2', serialize($Param));
    }

    $Form->SchreibeHTMLCode('<form name=frmFreieNummern action=./artikelstamm_Main.php?cmdAktion=Werkzeuge&Seite=FreieNummern method=POST  enctype="multipart/form-data">');

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ATUNR'] . ':', 200);
    $Form->Erstelle_TextFeld('AST_ATUNR', $Param['AST_ATUNR'], 10, 100, true, '', '', '', 'T', '', '', '', 5);
    $AWISCursorPosition = 'txtAST_ATUNR';
    $Form->ZeileEnde();

    if ($Param['AST_ATUNR'] != '') {
        $SQL = 'SELECT ATUNR AS AST_ATUNR, AST_BEZEICHNUNGWW, AST_KEY, WUG.WUG_WGR_ID, WUG.WUG_ID, WUG.WUG_BEZEICHNUNG';
        $SQL .= ',(select  listagg(HERSTELLER,\',\')  within group (order by HERSTELLER)  from V_ARTIKEL_FAHRZEUGE fzg WHERE fzg.ATU_NR = AST_ATUNR group by ATU_NR) as HERSTELLER';
        $SQL .= ' FROM (SELECT AST_ATUNR AS ATUNR';
        $SQL .= ' FROM Artikelstamm';
        $SQL .= ' WHERE AST_ATUNR LIKE ' . $DB->WertSetzen('AST', 'T', $Param['AST_ATUNR'] . '%');
        $SQL .= ' UNION SELECT LOA_AST_ATUNR';
        $SQL .= ' FROM Loeschartikel';
        $SQL .= ' WHERE LOA_AST_ATUNR LIKE ' . $DB->WertSetzen('AST', 'T', $Param['AST_ATUNR'] . '%') . ') DATEN';
        $SQL .= ' LEFT OUTER JOIN ARTIKELSTAMM ON ATUNR = AST_ATUNR';
        $SQL .= ' LEFT JOIN WARENUNTERGRUPPEN WUG on WUG.WUG_KEY = AST_WUG_KEY';
        $SQL .= ' WHERE LENGTH(DATEN.ATUNR)=6';
        $SQL .= ' ORDER BY nlssort(ast_atunr,\'NLS_SORT=BINARY\')';

        $rsAST = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('AST'));

        $Muster = str_pad($Param['AST_ATUNR'], 6, '0', STR_PAD_RIGHT);
        $SuchZeichen = (int)('1' . str_pad('', (6 - strlen($Param['AST_ATUNR'])), '0'));
        $Form->DebugAusgabe(1, $Muster, $SuchZeichen, $rsAST->FeldInhalt('AST_ATUNR'));
        $Treffer = 0;
        for ($i = 0; $i < $SuchZeichen; $i++) {

            while (!is_numeric(substr($rsAST->FeldInhalt('AST_ATUNR'), -1, 1))) {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($rsAST->FeldInhalt('AST_ATUNR'), 150, 'font-style:italic;',
                    '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=' . $rsAST->FeldInhalt('AST_KEY'));
                $Form->Erstelle_TextLabel(($rsAST->FeldInhalt('AST_BEZEICHNUNGWW') == ''?'--nicht in WaWi--':$rsAST->FeldInhalt('AST_BEZEICHNUNGWW')), 400, 'font-style:italic;');
                $Form->ZeileEnde();
                $rsAST->DSWeiter();

                if ($rsAST->EOF()) {
                    break;
                }
            }
            if ($Muster != $rsAST->FeldInhalt('AST_ATUNR'))// AND $i>0)
            {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($Muster, 150, '', '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR=' . $Muster);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['txt_NaechsteFreieNummer'] . '', 400, 'Hinweis');
                $Form->ZeileEnde();
                if ((++$Treffer) > 1000)        // Damit irgendwann ein Ende erreicht ist
                {
                    break;
                }
            } else {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($rsAST->FeldInhalt('AST_ATUNR'), 150, '',
                    '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_KEY=' . $rsAST->FeldInhalt('AST_KEY'));
                $Form->Erstelle_TextLabel(($rsAST->FeldInhalt('AST_BEZEICHNUNGWW') == ''?'--nicht in WaWi--':$rsAST->FeldInhalt('AST_BEZEICHNUNGWW')), 400,
                    ($rsAST->FeldInhalt('AST_BEZEICHNUNGWW') == ''?'Ueberschrift':''));
                $Form->Erstelle_TextLabel($rsAST->FeldInhalt('HERSTELLER')  ,200,'','',$rsAST->FeldInhalt('HERSTELLER'));
                $Form->Erstelle_TextLabel($rsAST->FeldInhalt('WUG_WGR_ID') . ' ' . $rsAST->FeldInhalt('WUG_ID')  ,80,'','',$rsAST->FeldInhalt('WUG_BEZEICHNUNG'));

                $Form->ZeileEnde();
                $rsAST->DSWeiter();
            }

            $Muster++;
        }
    }

    $Form->Formular_Ende();
    echo '</div>'; //PG: Verzweiflungs-DIV, da ich den Fehler nicht finde.
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Form->Schaltflaeche('script', 'cmdHilfe',
        "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=werkzeuge&Seite=" . (isset($_GET['Seite'])?$_GET['Seite']:'Geloeschte') . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');",
        '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060006");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "20081��61223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>