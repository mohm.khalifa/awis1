<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FIL_ID;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BAF','%');	
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','vom');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht3601 = $AWISBenutzer->HatDasRecht(3601);
	if($Recht3601==0)
	{
	    awisEreignis(3,1000,'PRA',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Bestandsabfrage'));
    $FIL_ID=$Param['FIL_ID'];

	if(isset($_POST['cmdLoeschen_x'])){
	    $SQL = 'delete from Bestandsabfragen where BAF_FIL_ID = ' . $DB->WertSetzen('BAF','Z',$FIL_ID);
	    $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('BAF'));
    }

	$DetailAnsicht=false;
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	//********************************************************
	// Daten suchen
	//********************************************************
	$AWISCursorPosition='';
	
	$DB->SetzeBindevariable('BAF', 'var_N0_baf_fil_id', '0'.$FIL_ID);

	$SQL = 'SELECT DISTINCT BAF_FIL_ID, MAX(BAF_DATUMSCAN) as DATUMSCAN';
	$SQL .=' FROM Bestandsabfragen BAF';		
	$SQL .=' WHERE BAF_FIL_ID=:var_N0_baf_fil_id';
	$SQL .=' GROUP BY BAF_FIL_ID';					
	
	$rsBAFKopf = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('BAF',false));
	
	$Form->Trennzeile();

	$ORDERBY = ' ORDER BY BAF_DATUMSCAN DESC, BAF_SCANNERID ASC, BAF_AST_ATUNR ASC';
	
	$SQL = ' SELECT DATEN.* ';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM (';
	$SQL .= 'SELECT BAF_AST_ATUNR, AST_BEZEICHNUNGWW, BAF_DATUMSCAN, BAF_SCANNERID, BAF_MENGESCAN, FIB_BESTAND, RUF_REGUNDFACHNR, AST_VK' ; 
	$SQL .= ' FROM BESTANDSABFRAGEN';
	$SQL .= ' LEFT JOIN ARTIKELSTAMM ON AST_ATUNR = BAF_AST_ATUNR';
	$SQL .= ' LEFT JOIN FILIALBESTAND ON FIB_AST_ATUNR = BAF_AST_ATUNR AND FIB_FIL_ID = BAF_FIL_ID';
	$SQL .= ' LEFT JOIN REGALUNDFACHNUMMER ON RUF_AST_ATUNR = BAF_AST_ATUNR AND RUF_FIL_ID = BAF_FIL_ID AND RUF_AKTIV = 1';
	$SQL .= ' WHERE BAF_FIL_ID=:var_N0_baf_fil_id';
	$SQL .= ' ) DATEN ';	
	$SQL .= $ORDERBY;
	
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		$Param['BLOCK']=$Block;
		$AWISBenutzer->ParameterSchreiben('Formular_BAF',serialize($Param));
	}
	elseif(isset($Param['BLOCK']))
	{
		$Block=$Param['BLOCK'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('BAF',false));
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	
	$rsBAF = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('BAF',false));

	$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_AST_ATUNR'],70);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_AST_BEZEICHNUNG'],180);	
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_DATUMSCAN'],90);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_SCANNERID'],80);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_FILIALBESTAND'],90);
	
	if (($Recht3601 & 2) == 2)
	{
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_MENGESCAN'],90);
	}
	
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_VK'],60);	
	if(($Recht3601 & 4) == 4)
	{
	   $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BAF']['BAF_REGALUNDFACHNUMMER'],270);	
	}
	$Form->ZeileEnde();
		
	$BAFZeile=0;
	$Link= '';
	while(!$rsBAF->EOF())
	{
		$Form->ZeileStart();
		$Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR='.$rsBAF->Feldinhalt('BAF_AST_ATUNR');		

		$Form->Erstelle_ListenFeld('BAF_AST_ATUNR',$rsBAF->Feldinhalt('BAF_AST_ATUNR'),10,70,false,($BAFZeile%2),'',$Link,'T','L');
		$Form->Erstelle_ListenFeld('BAF_AST_BEZEICHNUNG',substr($rsBAF->Feldinhalt('AST_BEZEICHNUNGWW'),0,15),10,180,false,($BAFZeile%2),'','','T','L');
		$Form->Erstelle_ListenFeld('BAF_AST_ATUNR',$rsBAF->Feldinhalt('BAF_DATUMSCAN'),10,90,false,($BAFZeile%2),'','','D','L');
		$Form->Erstelle_ListenFeld('BAF_AST_ATUNR',$rsBAF->Feldinhalt('BAF_SCANNERID'),10,80,false,($BAFZeile%2),'','','N0	','L');
		$Form->Erstelle_ListenFeld('BAF_FILIALBESTAND',$rsBAF->Feldinhalt('FIB_BESTAND'),10,90,false,($BAFZeile%2),'','','N0','L');
		
		if (($Recht3601 & 2) == 2)
		{
			$Form->Erstelle_ListenFeld('BAF_MENGESCAN',$rsBAF->Feldinhalt('BAF_MENGESCAN'),10,90,false,($BAFZeile%2),'','','N0','L');
		}
		
		$Form->Erstelle_ListenFeld('BAF_VK',$rsBAF->Feldinhalt('AST_VK'),10,60,false,($BAFZeile%2),'','','N2','L');
		
		//PG: 20150427 Aufgrund der neuen Inventursoftware bekommen wir diese Daten nicht mehr. 
		//PG: Somit m�ssen diese auch nicht mehr angezeigt werden. (Laut Hr. Krumpholz)
		//PG: Vorerst per Recht weggeschaltet, f�r denn Fall, dass wir in Zukunft wieder Daten bekommen. 
		
		if(($Recht3601 & 4) == 4)
		{
		  $Form->Erstelle_ListenFeld('BAF_REGALUNDFACHNUMMER',$rsBAF->Feldinhalt('RUF_REGUNDFACHNR'),10,270,false,($BAFZeile%2),'','','T','L');		
		}
		
		$Form->ZeileEnde();

		$rsBAF->DSWeiter();
		$BAFZeile++;
	}

	// Bl�ttern
	$Link = './bestandsabfrage_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
	$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	
	$Form->Formular_Ende();			

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../../filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');	
	$Form->Schaltflaeche('href', 'cmdDrucken', './bestandsabfrage_pdf.php?BAF_FIL_ID='.$FIL_ID, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');
	$Form->Schaltflaeche('image', 'cmdOK', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'S');
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>