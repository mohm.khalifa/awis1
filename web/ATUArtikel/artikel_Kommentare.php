<?php
	global $awisDBFehler;		// Fehlerarray
	global $awisRSZeilen;
	global $_POST;
	global $AWISBenutzer;


$BildSchirmBreite = awis_BenutzerParameter($con, 'BildschirmBreite', $AWISBenutzer->BenutzerName());

/********************************************************************************
* Daten Speichern, Hinzuf�gen und L�schen
********************************************************************************/


if(awis_NameInArray($_POST,"cmdKOMMLoeschen_"))
{
	//$ID= intval(substr(awis_NameInArray($_POST,"cmdKOMMLoeschen_"),16));
	$ID= floatval(substr(awis_NameInArray($_POST,"cmdKOMMLoeschen_"),16));
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{
		$SQL = "DELETE FROM ArtikelstammInfos WHERE ASI_KEY=0" . $_POST["txtKEY_old"] . "";
		awis_Debug(1,$SQL);
		$Erg = awisExecute($con, $SQL);
		awisLogoff($con);
		if($Erg==FALSE)
		{
			
			awisErrorMailLink("ArtikelKommentare.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Der Kommentar wurde erfolgreich gel�scht<br><br><a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=Kommentare>Weiter</a>');
	}
	elseif(!isset($_POST["cmdLoeschAbbruch"]) or $_POST["cmdLoeschAbbruch"]=='')
	{
		echo "<form name=frmAIS method=post>";

		echo "<input type=hidden name=cmdKOMMLoeschen_x>";
		echo "<input type=hidden name=txtKEY_old value=" . $ID . ">";

		echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Kommentar l�schen m�chten?</span><br><br>";
		echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		echo "</form>";
		awisLogoff($con);
		die();			
	}	
}


	if(isset($_POST['cmdSpeichern_x']) OR awis_NameInArray($_POST,"cmdKOMMLoeschen_X")!='')
	{
		foreach($_POST as $Eintrag => $EintragsWert)
		{
			if(substr($Eintrag,0,13) == "txtKOMM_WERT_")
			{
				if($EintragsWert . '' != $_POST['txtKOMM_ALT_WERT_' . substr($Eintrag,13)] . '')
				{
					$SQL = "UPDATE AWIS.ArtikelstammInfos ";
					$SQL .= " SET ASI_WERT='" . $EintragsWert . "'";
					$SQL .= " , ASI_USER='" . $AWISBenutzer->BenutzerName() . "', ASI_USERDAT=SYSDATE";
					$SQL .= " WHERE ASI_KEY=0" . substr($Eintrag,13);
					
					$Erg = awisExecute($con, $SQL);
					if($Erg===FALSE)
					{
						awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
					}
				}
			}
		    if(substr($Eintrag,0,16) == "cmdKOMMLoeschen_" AND substr($Eintrag,strlen($Eintrag)-1,1)=='x')
			{
				awis_Debug(1,$SQL);
				$SQL = "DELETE FROM AWIS.ArtikelstammInfos WHERE ASI_KEY=0" . intval(substr($Eintrag,16));
				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}

			if($Eintrag == "txtKOMM_NEUWERT_110" AND $EintragsWert != '')
			{
				$SQL = "INSERT INTO AWIS.ArtikelstammInfos(ASI_AST_ATUNR,ASI_WERT, ASI_AIT_ID, ASI_USER, ASI_USERDAT, ASI_IMQ_ID)";
				$SQL .= " VALUES('" . $_POST["txtAST_ATUNR"] . "',";
				$SQL .= " '" . $EintragsWert . "',";							
				$SQL .= " 110,";							
				$SQL .= " '" . $AWISBenutzer->BenutzerName() . "',";
				$SQL .= " SYSDATE, 4)";				

				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}
			if($Eintrag == "txtKOMM_NEUWERT_111" AND $EintragsWert != '')
			{
				$SQL = "INSERT INTO AWIS.ArtikelstammInfos(ASI_AST_ATUNR,ASI_WERT, ASI_AIT_ID, ASI_USER, ASI_USERDAT, ASI_IMQ_ID)";
				$SQL .= " VALUES('" . $_POST["txtAST_ATUNR"] . "',";
				$SQL .= " '" . $EintragsWert . "',";							
				$SQL .= " 111,";							
				$SQL .= " '" . $AWISBenutzer->BenutzerName() . "',";
				$SQL .= " SYSDATE, 4)";				

				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}
			if($Eintrag == "txtKOMM_NEUWERT_112" AND $EintragsWert != '')
			{
				$SQL = "INSERT INTO AWIS.ArtikelstammInfos(ASI_AST_ATUNR,ASI_WERT, ASI_AIT_ID, ASI_USER, ASI_USERDAT, ASI_IMQ_ID)";
				$SQL .= " VALUES('" . $_POST["txtAST_ATUNR"] . "',";
				$SQL .= " '" . $EintragsWert . "',";							
				$SQL .= " 112,";							
				$SQL .= " '" . $AWISBenutzer->BenutzerName() . "',";
				$SQL .= " SYSDATE, 4)";				

				$Erg = awisExecute($con, $SQL);
				if($Erg===FALSE)
				{
					awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
				}
			}
		}
	}	


/*****************************************************************************************************
* Daten anzeigen
*****************************************************************************************************/
	
	$BearbeitungsStufe = awisBenutzerRecht($con, 401) ;
	$Recht400 = awisBenutzerRecht($con, 400);
	
		// KEY=0, ATUNR=1
	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerArtikel", $AWISBenutzer->BenutzerName()));
	
	$SQL = "SELECT * FROM AWIS.ArtikelstammInfos, AWIS.ArtikelstammInfosTypen ";
	$SQL .= " WHERE ASI_AIT_ID = AIT_ID AND ASI_AST_ATUNR='" . $Artikel[1] . "'";
	
//echo "<hr>$SQL<hr>";

	if(!($Recht400&128)==128)
	{
		if(($Recht400&32)==32)
			{$SQL .= " AND ASI_AIT_ID IN(111,112)";} // Kommmentare au�er Crossing - Kommentare (110) anzeigen
		else 
			{$SQL .= " AND 1=2";} // keine Kommmentare anzeigen
	}
	else 
	{
		$SQL .= " AND ASI_AIT_ID IN(110,111,112)"; // Alle Kommentare anzeigen
	}

	$SQL .= " ORDER BY ASI_AIT_ID, ASI_USERDAT DESC";	
	
	$rsKommentare = awisOpenRecordset($con, $SQL);
	if($rsKommentare==FALSE)
	{
		awisErrorMailLink("artikel_Kommentare.php", 2, $awisDBFehler['message']);
	}
	$rsKommentareZeilen=$awisRSZeilen;
	
	echo "<table id=DatenTabelle width=100% border=1>";
	//echo "<cols><col width=20></col><col width=*></col><col width=200></col><col width=100></col></cols>";

	$NeuerKommentar = 0;
	$AktuelleID = 0;		// F�r den Blocktrenner	
	for($Zeile=0;$Zeile<$rsKommentareZeilen;$Zeile++)
	{
		if($AktuelleID!=$rsKommentare["AIT_ID"][$Zeile])
		{
			$AktuelleID=$rsKommentare["AIT_ID"][$Zeile];
			echo "<tr><td id=FeldBez></td>";
			echo "<td id=FeldBez>" . $rsKommentare['AIT_INFORMATION'][$Zeile] . "</td>";
			echo "<td id=FeldBez>Ersteller</td>";	
			echo "<td id=FeldBez>Datum</td>";	

				//**********************************************************************
				// Neue Datens�tze hinzuf�gen (als erste Zeile)
				//**********************************************************************
			
			if($AktuelleID<>0)
			{
				if(($BearbeitungsStufe & 32)==32 AND $AktuelleID==110)			// Recht 401
				{
					$NeuerKommentar |= 32;	// Am Ende nicht nocheinmal
					echo "<tr>";
					echo "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
					echo "<td><input name=txtKOMM_NEUWERT_110 size=" . (($BildSchirmBreite/10)-20) . ">";
					echo "<input type=hidden name=txtKOMM_TYP value=110></td>";
					echo "<td>" . $AWISBenutzer->BenutzerName() . "</td>";
					echo "<td>" . date('d.m.Y') . "</td>";
					echo "<td></td>";
					echo "<td></td>";
					echo "</tr>";
				}
				if(($BearbeitungsStufe & 64)==64 AND $AktuelleID == 111)			// Recht 401
				{
					$NeuerKommentar |= 64;	// Am Ende nicht nocheinmal
					echo "<tr>";
					echo "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
					echo "<td><input name=txtKOMM_NEUWERT_111 size=" . (($BildSchirmBreite/10)-20) . ">";
					echo "<input type=hidden name=txtKOMM_TYP value=111></td>";
					echo "<td>" . $AWISBenutzer->BenutzerName() . "</td>";
					echo "<td>" . date('d.m.Y') . "</td>";
					echo "<td></td>";
					echo "<td></td>";
					echo "</tr>";
				}
				if(($BearbeitungsStufe & 128)==128 AND $AktuelleID ==112)			// Recht 401
				{
					$NeuerKommentar |= 128;	// Am Ende nicht nocheinmal
					echo "<tr>";
					echo "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
					echo "<td><input name=txtKOMM_NEUWERT_112 size=" . (($BildSchirmBreite/10)-20) . ">";
					echo "<input type=hidden name=txtKOMM_TYP value=112></td>";
					echo "<td>" . $AWISBenutzer->BenutzerName() . "</td>";
					echo "<td>" . date('d.m.Y') . "</td>";
					echo "<td></td>";
					echo "<td></td>";
					echo "</tr>";
				}
			}
		}

		echo "<tr>";

			//**********************************
			// Daten anzeigen
			//**********************************		
		if((($BearbeitungsStufe & 32)==32 AND $rsKommentare["ASI_AIT_ID"][$Zeile]==110)
			OR  (($BearbeitungsStufe & 64)==64 AND $rsKommentare["ASI_AIT_ID"][$Zeile]==111)
			OR  (($BearbeitungsStufe & 128)==128 AND $rsKommentare["ASI_AIT_ID"][$Zeile]==112)
		  )			// Recht 401
		{
			echo "<td><input name=cmdKOMMLoeschen_" . $rsKommentare["ASI_KEY"][$Zeile] . " type=image src=/bilder/muelleimer.png alt=\"Zuordnung l�schen\"></td>";
			$AnzZeilen = intval(strlen($rsKommentare["ASI_WERT"][$Zeile])/(($BildSchirmBreite/10)-20));
			$AnzZeilen = ($AnzZeilen==0?1:$AnzZeilen);
			
			echo "<td><textarea rows=" . $AnzZeilen . " cols=" . (($BildSchirmBreite/10)-20) . " name=txtKOMM_WERT_" . $rsKommentare["ASI_KEY"][$Zeile] . " >" . $rsKommentare["ASI_WERT"][$Zeile] . "</textarea>";
			echo "<input name=txtKOMM_ALT_WERT_" . $rsKommentare["ASI_KEY"][$Zeile] . " type=hidden value='" . $rsKommentare["ASI_WERT"][$Zeile] . "'></td>";
		}
		else
		{
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") . '>&nbsp;</td>';
			echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") . '>' . $rsKommentare["ASI_WERT"][$Zeile] . "</a></td>";
		}
		echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") . '>' . $rsKommentare["ASI_USER"][$Zeile] . "</a></td>";
		echo '<td ' . (($Zeile%2)==0?"id=TabellenZeileGrau ":"id=TabellenZeileWeiss ") . '>' . $rsKommentare["ASI_USERDAT"][$Zeile] . "</a></td>";

		echo "</tr>";

		// Neue Zuordnung erlaubt?
	}

		//**********************************************************************
		//* Wenn noch keine existieren -> Neuen Block anzeigen
		//**********************************************************************
		
	if(($BearbeitungsStufe & 32)==32 AND (($NeuerKommentar&32)!=32))			// Recht 401
	{
		echo "<tr><td id=FeldBez></td>";
		echo "<td id=FeldBez>Neuer Crossing Kommentar</td>";
		echo "<td id=FeldBez>Ersteller</td>";	
		echo "<td id=FeldBez>Datum</td></tr>";	
	
		echo "<tr>";
		echo "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
		echo "<td><input name=txtKOMM_NEUWERT_110 size=" . (($BildSchirmBreite/10)-20) . ">";
		echo "<input type=hidden name=txtKOMM_TYP value=110></td>";
		echo "<td>" . $AWISBenutzer->BenutzerName() . "</td>";
		echo "<td>" . date('d.m.Y') . "</td>";
		echo "<td></td>";
		echo "<td></td>";
		echo "</tr>";
	}

	if(($BearbeitungsStufe & 64)==64 AND (($NeuerKommentar&64)!=64))			// Recht 401
	{
		echo "<tr><td id=FeldBez></td>";
		echo "<td id=FeldBez>Neuer Einkauf Kommentar</td>";
		echo "<td id=FeldBez>Ersteller</td>";	
		echo "<td id=FeldBez>Datum</td></tr>";	
	
		echo "<tr>";
		echo "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
		echo "<td><input name=txtKOMM_NEUWERT_111 size=" . (($BildSchirmBreite/10)-20) . ">";
		echo "<input type=hidden name=txtKOMM_TYP value=111></td>";
		echo "<td>" . $AWISBenutzer->BenutzerName() . "</td>";
		echo "<td>" . date('d.m.Y') . "</td>";
		echo "<td></td>";
		echo "<td></td>";
		echo "</tr>";
	}

	if(($BearbeitungsStufe & 128)==128 AND (($NeuerKommentar&128)!=128))			// Recht 401
	{
		echo "<tr><td id=FeldBez></td>";
		echo "<td id=FeldBez>Neuer QS Kommentar</td>";
		echo "<td id=FeldBez>Ersteller</td>";	
		echo "<td id=FeldBez>Datum</td></tr>";	
	
		echo "<tr>";
		echo "<td><input name=txtKOMM_KEY_0 type=hidden></td>";
		echo "<td><input name=txtKOMM_NEUWERT_112 size=" . (($BildSchirmBreite/10)-20) . ">";
		echo "<input type=hidden name=txtKOMM_TYP value=112></td>";
		echo "<td>" . $AWISBenutzer->BenutzerName() . "</td>";
		echo "<td>" . date('d.m.Y') . "</td>";
		echo "<td></td>";
		echo "<td></td>";
		echo "</tr>";
	}
	
	echo "</table>";

	unset($rsKommentare);
?>