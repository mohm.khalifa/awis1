<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','wrd_Filiale');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Wort','txt_NaechsteFreieNummer');
$TextKonserven[]=array('ADX','%');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	//$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_453_2'));

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4301 = $AWISBenutzer->HatDasRecht(4301);		// Recht f�r Stammdaten
	if($Recht4301==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$Bedingung='';
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./adr_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./adr_speichern.php');
	}
	elseif(isset($_GET['ADX_KEY']))
	{
		$Bedingung .= ' AND ADX_KEY='.$DB->FeldInhaltFormat('Z',$_GET['ADX_KEY']).'';
	}
	elseif(isset($_POST['txtADX_KEY']))
	{
		$Bedingung .= ' AND ADX_KEY='.$DB->FeldInhaltFormat('Z',$_POST['ADX_KEY']).'';
	}
	
	if($AWIS_KEY2>0)
	{
		$Bedingung .= ' AND ADX_KEY='.$DB->FeldInhaltFormat('Z',$AWIS_KEY2).'';
	}
	
	//********************************************************
	// Daten suchen
	//********************************************************
	
	$SQL = 'SELECT *';		
	$SQL .= ' FROM ADRTEXTE ';			
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
	if(!isset($_GET['Sort']))
	{
		$SQL .= ' ORDER BY ADX_ADRART, ADX_ADRBLOCK, ADX_ADRSORT';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	
	$rsADX = $DB->RecordsetOeffnen($SQL);
	
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmadrstamm action=./adr_Main.php?cmdAktion=Stammdaten&Seite=Texte method=POST>';

	$Form->Formular_Start();
	
	$EditRecht=false;
	
	if($rsADX->EOF() AND  !isset($_GET['ADX_KEY']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsADX->AnzahlDatensaetze()>1)						// Liste anzeigen
	{		
		$Form->ZeileStart();
		if(($Recht4301&4))
		{
			$Icons[] = array('new','./adr_Main.php?cmdAktion=Stammdaten&Seite=Texte&ADX_KEY=0','g',$AWISSprachKonserven['Wort']['lbl_hinzufuegen']);
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}
		
		$Link = './adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADX_ADRART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADX_ADRART'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADX']['ADX_ADRART'],150,'',$Link);	
		$Link = './adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADX_ADRBLOCK'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADX_ADRBLOCK'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADX']['ADX_ADRBLOCK'],150,'',$Link);	
		$Link = './adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADX_ADRSORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADX_ADRSORT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADX']['ADX_ADRSORT'],150,'',$Link);	
		$Link = './adr_Main.php?cmdAktion=Stammdaten&Seite=Abfallstamm'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADX_ADRBEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADX_ADRBEZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADX']['ADX_ADRBEZ'],500,'',$Link);	
		
		$Form->ZeileEnde();
		
		$DS=0;
		while(!$rsADX->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(($Recht4301&6)>0)	// �ndernrecht
			{
				$Icons[] = array('edit','./adr_Main.php?cmdAktion=Stammdaten&Seite=Texte&ADX_KEY='.$rsADX->FeldInhalt('ADX_KEY'));
				$Icons[] = array('delete','./adr_Main.php?cmdAktion=Stammdaten&Seite=Texte&Del='.$rsADX->FeldInhalt('ADX_KEY'));
			}			
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			
			$ADXArt='';
			$ADRText = explode("|",$AWISSprachKonserven['ADX']['lst_ADX_ADRART']);			
			foreach ($ADRText as $Art)
			{
				$AXArt = explode("~",$Art);								
				if ($AXArt[0]==$rsADX->FeldInhalt('ADX_ADRART'))
				{
					$ADXArt = $AXArt[1];					
				}				
			}			
			$Form->Erstelle_ListenFeld('ADX_ADRART',$ADXArt,0,150,false,($DS%2),'','','T','L','');	
			
			//$Form->Erstelle_ListenFeld('ADX_ADRART',$rsADX->Feldinhalt('ADX_ADRART'),0,150,false,($DS%2),'','','','','');											
			$Form->Erstelle_ListenFeld('ADX_ADRBLOCK',$rsADX->Feldinhalt('ADX_ADRBLOCK'),0,150,false,($DS%2),'','','','','');											
			$Form->Erstelle_ListenFeld('ADX_ADRSORT',$rsADX->Feldinhalt('ADX_ADRSORT'),0,150,false,($DS%2),'','','','','');											
			$Form->Erstelle_ListenFeld('ADX_ADRBEZ',$rsADX->Feldinhalt('ADX_ADRBEZ'),0,500,false,($DS%2),'','','','','');											
			$Form->ZeileEnde();
			
			$DS++;
			$rsADX->DSWeiter();					
		}
	}
	else 
	{
		echo '<form name=frmADRAbfallstamm action=./adr_Main.php?cmdAktion=Stammdaten&Seite=Texte method=POST  enctype="multipart/form-data">';
		
		$AWIS_KEY1 = $rsADX->FeldInhalt('ADX_KEY');		
		echo '<input type=hidden name=txtADX_KEY value='.$AWIS_KEY1. '>';
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adr_Main.php?cmdAktion=Stammdaten&Seite=Texte&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsADX->FeldInhalt('ADX_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsADX->FeldInhalt('ADX_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$EditRecht=(($Recht4301&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRART'].':',200);
		$ADRText = explode("|",$AWISSprachKonserven['ADX']['lst_ADX_ADRART']);
		$Form->Erstelle_SelectFeld('ADX_ADRART',$rsADX->FeldInhalt('ADX_ADRART'),200,$EditRecht,'','','','','',$ADRText,'');						
		$Form->ZeileEnde();	
		/*		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRART'].':',200);
		$Form->Erstelle_TextFeld('ADX_ADRART',$rsADX->FeldInhalt('ADX_ADRART'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		*/
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRBLOCK'].':',200);
		$Form->Erstelle_TextFeld('ADX_ADRBLOCK',$rsADX->FeldInhalt('ADX_ADRBLOCK'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRSORT'].':',200);
		$Form->Erstelle_TextFeld('ADX_ADRSORT',$rsADX->FeldInhalt('ADX_ADRSORT'),20,50,$EditRecht,'','','','T');		
		$Form->ZeileEnde();
		
		$Form->Trennzeile(0);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRBEZ'].':',200);
		$Form->Erstelle_TextFeld('ADX_ADRBEZ',$rsADX->FeldInhalt('ADX_ADRBEZ'),100,400,$EditRecht,'','','','T');		
		$Form->ZeileEnde();

		if ($rsADX->FeldInhalt('ADX_ADRART')=='3')
		{		
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRBEZ'].' - CZ: ',200);
			$Form->Erstelle_TextFeld('ADX_ADRBEZCZ',$rsADX->FeldInhalt('ADX_ADRBEZCZ'),100,400,$EditRecht,'','','','T');		
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRBEZ'].' - NL:',200);
			$Form->Erstelle_TextFeld('ADX_ADRBEZNL',$rsADX->FeldInhalt('ADX_ADRBEZNL'),100,400,$EditRecht,'','','','T');		
			$Form->ZeileEnde();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ADX']['ADX_ADRBEZ'].' - IT:',200);
			$Form->Erstelle_TextFeld('ADX_ADRBEZIT',$rsADX->FeldInhalt('ADX_ADRBEZIT'),100,400,$EditRecht,'','','','T');		
			$Form->ZeileEnde();
		}
		
		$Form->Trennzeile(0);	
		
		//$Form->ZeileStart();
		//$Form->Erstelle_TextLabel('Land :',200);
		//$Form->Erstelle_TextFeld('ADS_ZULADGEW',$rsADS->FeldInhalt('ADS_ZULADGEW'),20,50,$EditRecht,'','','','T');		
		//$Form->ZeileEnde();

	}

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if ($EditRecht==true)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=werkzeuge&Seite=".(isset($_GET['Seite'])?$_GET['Seite']:'Geloeschte')."','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200810060006");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"20081��61223");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>