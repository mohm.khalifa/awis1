<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	if(isset($_POST['txtZSZ_KEY']))
	{
		//***********************************************
		// warengruppen
		//***********************************************
		$AWIS_KEY1=$_POST['txtZSZ_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtZSZ_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ZSZ','ZSZ_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('ZSZ_SORTIMENT','ZSZ_LIE_NR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]==='')
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZSZ'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtZSZ_KEY'])==0)
			{

				$Fehler = '';
				$SQL = 'INSERT INTO zukaufsortimentzuordnungen';
				$SQL .= '(ZSZ_ZLI_KEY,ZSZ_SORTIMENT,ZSZ_LIE_NR,ZSZ_WG1,ZSZ_WG2,ZSZ_WG3,ZSZ_BEZEICHNUNG';
				$SQL .= ',ZSZ_USER, ZSZ_USERDAT';
				$SQL .= ')VALUES (1,';
				$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtZSZ_SORTIMENT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZSZ_LIE_NR'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZSZ_WG1'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZSZ_WG2'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZSZ_WG3'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZSZ_BEZEICHNUNG'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Anlegen eines Datensatzes',903242032,$SQL,2);
				}
				$SQL = 'SELECT seq_ZSZ_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
			}
			else 					// gešnderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM zukaufsortimentzuordnungen WHERE ZSZ_key=' . $_POST['txtZSZ_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('ZSZ_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE zukaufsortimentzuordnungen SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ZSZ_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ZSZ_userdat=sysdate';
					$SQL .= ' WHERE ZSZ_key=0' . $_POST['txtZSZ_KEY'] . '';
					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',903242032,$SQL,2);
					}
				}
			}
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>