<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

class asesplandaten_funktionen
{
    public $Form;
    public $DB;
    public $AWISBenutzer;
    public $OptionBitteWaehlen;
    public $Recht74000;
    private $_EditRecht;
    public $AWISSprachKonserven;
    public $Param;
    public $AWISCursorPosition;
    public $AWISWerkzeuge;


    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular(3);
        $this->OptionBitteWaehlen = '~' . $this->Form->LadeTextBaustein('Wort', 'txt_BitteWaehlen', $this->AWISBenutzer->BenutzerSprache());
        $this->Recht74000 = $this->AWISBenutzer->HatDasRecht(74000);
        $this->_EditRecht = (($this->Recht74000 & 2) != 0);
        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_APD'));
        $this->AWISWerkzeuge = new awisWerkzeuge();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('APD', '%');
        $TextKonserven[] = array('FIL', 'FIL_ID');
        $TextKonserven[] = array('PER', 'PER_NR');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'lbl_drucken');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'Land');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Datum%');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('TITEL', 'tit_APD');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven, $this->AWISBenutzer->BenutzerSprache());
    }

    public function erstelleFilialinfos($FilID){
        $SQL = 'select * from filialen';
        $SQL .= ' WHERE FIL_ID = '.$this->DB->WertSetzen('FIL','T',$FilID);
        $rsFil = $this->DB->RecordSetOeffnen($SQL,$this->DB->Bindevariablen('FIL'));

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['APD']['APD_FIL_BEZ'],170);
        $this->Form->Erstelle_TextFeld('APD_FIL_BEZ',$rsFil->FeldInhalt('FIL_BEZ'),10,110,false);
        $this->Form->ZeileEnde();
    }

    private function _POSTString2Array($POST){
        if($POST!=''){
            $POST =  explode('|~|',$POST);
            $return = array();
            foreach($POST as $p){
                if(strpos($p,'=')!==false){
                    $p = explode('=',$p);
                    $key = $p[0];
                    $value = $p[1];
                    $return[$key]  = $value;
                }
            }
            return $return;
        }
        return array();
    }

    public function POST2String(){
        $POST = '';
        foreach($_POST as $key => $value){
            if($key!='txtPOST'){
                $POST .= $key.'='.$value . '|~|';
            }
        }
        return $POST;
    }

    public function erstelleKundenBlock($Kartennummer,$POST){

        $SQL  ='SELECT BSK_KEY,';
        $SQL .='   BSK_TITEL,';
        $SQL .='   BSK_NACHNAME,';
        $SQL .='   BSK_VORNAME,';
        $SQL .='   BSK_STRASSE,';
        $SQL .='   BSK_POSTLEITZAHL,';
        $SQL .='   BSK_ORT,';
        $SQL .='   BSK_TELEFONNUMMER,';
        $SQL .='   BSK_FAXNUMMER,';
        $SQL .='   BSK_EMAIL,';
        $SQL .='   BSK_GEBURTSDATUM,';
        $SQL .='   BSK_KARTENNUMMER,';
        $SQL .='   BSK_USER,';
        $SQL .='   BSK_USERDAT';
        $SQL .=' FROM APDKUNDEN ';
        $SQL .= ' WHERE BSK_KARTENNUMMER = ' . $this->DB->WertSetzen('BSK','T',$Kartennummer);

        $rsBSK = $this->DB->RecordSetOeffnen($SQL,$this->DB->Bindevariablen('BSK'));
        $Disabled ='';

        $POST = $this->_POSTString2Array($POST);

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_TITEL'],170);
        $this->Form->Erstelle_TextFeld('BSK_TITEL',isset($POST['txtBSK_TITEL'])?$POST['txtBSK_TITEL']:$rsBSK->FeldInhalt('BSK_TITEL'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_NACHNAME'],170);
        $this->Form->Erstelle_TextFeld('!BSK_NACHNAME',isset($POST['txtBSK_NACHNAME'])?$POST['txtBSK_NACHNAME']:$rsBSK->FeldInhalt('BSK_NACHNAME'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_VORNAME'],170);
        $this->Form->Erstelle_TextFeld('!BSK_VORNAME',isset($POST['txtBSK_VORNAME'])?$POST['txtBSK_VORNAME']:$rsBSK->FeldInhalt('BSK_VORNAME'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_STRASSE'],170);
        $this->Form->Erstelle_TextFeld('BSK_STRASSE',isset($POST['txtBSK_STRASSE'])?$POST['txtBSK_STRASSE']:$rsBSK->FeldInhalt('BSK_STRASSE'),50,540,true,'','','','','','','','','','',$Disabled);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_POSTLEITZAHL'],170);
        $this->Form->Erstelle_TextFeld('BSK_POSTLEITZAHL',isset($POST['txtBSK_POSTLEITZAHL'])?$POST['txtBSK_POSTLEITZAHL']:$rsBSK->FeldInhalt('BSK_POSTLEITZAHL'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_ORT'],170);
        $this->Form->Erstelle_TextFeld('BSK_ORT',isset($POST['txtBSK_ORT'])?$POST['txtBSK_ORT']:$rsBSK->FeldInhalt('BSK_ORT'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_TELEFONNUMMER'],170);
        $this->Form->Erstelle_TextFeld('BSK_TELEFONNUMMER',isset($POST['txtBSK_TELEFONNUMMER'])?$POST['txtBSK_TELEFONNUMMER']:$rsBSK->FeldInhalt('BSK_TELEFONNUMMER'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_FAXNUMMER'],170);
        $this->Form->Erstelle_TextFeld('BSK_FAXNUMMER',isset($POST['txtBSK_FAXNUMMER'])?$POST['txtBSK_FAXNUMMER']:$rsBSK->FeldInhalt('BSK_FAXNUMMER'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->ZeileEnde();

        $this->Form->ZeileStart();
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_EMAIL'],170);
        $this->Form->Erstelle_TextFeld('BSK_EMAIL',isset($POST['txtBSK_EMAIL'])?$POST['txtBSK_EMAIL']:$rsBSK->FeldInhalt('BSK_EMAIL'),10,110,true,'','','','','','','','','','',$Disabled);
        $this->Form->Erstelle_TextLabel($this->AWISSprachKonserven['BSK']['BSK_GEBURTSDATUM'],170);
        $this->Form->Erstelle_TextFeld('BSK_GEBURTSDATUM',isset($POST['txtBSK_GEBURTSDATUM'])?$POST['txtBSK_GEBURTSDATUM']:$rsBSK->FeldInhalt('BSK_GEBURTSDATUM'),10,150,true,'','','','D','','','','','','',$Disabled);
        $this->Form->ZeileEnde();


    }

    function __destruct()
    {
        $this->AWISBenutzer->ParameterSchreiben('Formular_APD', serialize($this->Param));
        $this->Form->SetzeCursor($this->AWISCursorPosition);
    }

    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht74000 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'APD'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function BonDetails($BonDatum, $BonFiliale){
        $Bedingung = '';
        $SQL= 'SELECT * ';
        $SQL .= ' FROM EXPERIAN_ATU.POS_KOPF@COM_DE.ATU.DE k';
        $SQL .= ' WHERE FILIALE=' .$this->DB->WertSetzen('POS','Z',$BonFiliale);
        $SQL .= ' and BONDATUM=' .$this->DB->WertSetzen('POS','T',date('Ymd', strtotime($BonDatum)));

        $rsPOKSuche = $this->DB->RecordSetOeffnen($SQL,$this->DB->Bindevariablen('POS'));

        $FeldBreiten = array();
        $FeldBreiten['FILIALE'] = 60;
        $FeldBreiten['BONDATUM'] = 100;
        $FeldBreiten['BONZEIT'] = 100;
        $FeldBreiten['BSA'] = 100;
        $FeldBreiten['KARTENNR'] = 130;
        $FeldBreiten['VERTRAGSNR'] = 100;
        $FeldBreiten['BONWERT'] = 100;
        $FeldBreiten['LIEFERRG'] = 100;

        $this->Form->ZeileStart();
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['FILIALE'], $FeldBreiten['FILIALE']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['BONDATUM'], $FeldBreiten['BONDATUM']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['BONZEIT'], $FeldBreiten['BONZEIT']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['BSA'], $FeldBreiten['BSA']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['KARTENNR'], $FeldBreiten['KARTENNR']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['VERTRAGSNR'], $FeldBreiten['VERTRAGSNR']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['BONWERT'], $FeldBreiten['BONWERT']);
        $this->Form->Erstelle_Liste_Ueberschrift($this->AWISSprachKonserven['PosComSuche']['LIEFERRG'], $FeldBreiten['LIEFERRG']);

        $this->Form->ZeileEnde();

        $DS=0;
        while(!$rsPOKSuche->EOF())
        {
            $this->Form->ZeileStart();

            $this->Form->Erstelle_ListenFeld('FILIALE',$rsPOKSuche->FeldInhalt('FILIALE'),12,$FeldBreiten['FILIALE'],false,($DS%2),'','','T');
            $this->Form->Erstelle_ListenFeld('BONDATUM',$rsPOKSuche->FeldInhalt('BONDATUM'),12,$FeldBreiten['BONDATUM'],false,($DS%2),'','','T');
            $this->Form->Erstelle_ListenFeld('BONZEIT',$rsPOKSuche->FeldInhalt('BONZEIT'),12,$FeldBreiten['BONZEIT'],false,($DS%2),'','','T');
            $this->Form->Erstelle_ListenFeld('BSA',$rsPOKSuche->FeldInhalt('BSA'),12,$FeldBreiten['BSA'],false,($DS%2),'','','T');
            $this->Form->Erstelle_ListenFeld('KARTENNR',$rsPOKSuche->FeldInhalt('KARTENNR'),12,$FeldBreiten['KARTENNR'],false,($DS%2),'','','T');
            $this->Form->Erstelle_ListenFeld('VERTRAGSNR',$rsPOKSuche->FeldInhalt('VERTRAGSNR'),12,$FeldBreiten['VERTRAGSNR'],false,($DS%2),'','','T');
            $this->Form->Erstelle_ListenFeld('BONWERT',$rsPOKSuche->FeldInhalt('BONWERT'),12,$FeldBreiten['BONWERT'],false,($DS%2),'','','T');
            $this->Form->Erstelle_ListenFeld('LIEFERRG',$rsPOKSuche->FeldInhalt('LIEFERRG'),12,$FeldBreiten['LIEFERRG'],false,($DS%2),'','','T');
            $this->Form->ZeileEnde();
            $rsPOKSuche->DSWeiter();
            $DS++;
        }


    }

    public function BedingungErstellen()
    {
        global $AWIS_KEY1;

        $Bedingung = '';
        $IMQ_ID = '';
        //Recht f�r manuelle Buchungen
        if(($this->Recht74000&4)==4){
            $IMQ_ID .= ',4';
        }
        //Recht f�r Kassen/Online Buchungen
        if(($this->Recht74000&8)==8){
            $IMQ_ID .= ',131072, 262144 ';
        }
        $Bedingung .= ' AND APD_IMQ_ID in (0'.$IMQ_ID.')';

        if ($AWIS_KEY1 != 0) {
            $Bedingung .= ' AND APD_KEY = ' . $this->DB->WertSetzen('APD', 'N0', $AWIS_KEY1);

            return $Bedingung;
        }

        if (isset($this->Param['APD_KARTENNUMMER']) and $this->Param['APD_KARTENNUMMER'] != '') {
            $Bedingung .= ' AND APD_KARTENNUMMER ' . $this->DB->LikeOderIst($this->Param['APD_KARTENNUMMER'], awisDatenbank::AWIS_LIKE_UPPER, 'APD');
        }

        if (isset($this->Param['APD_BONNR']) and $this->Param['APD_BONNR'] != '') {
            $Bedingung .= ' AND APD_BONNR ' . $this->DB->LikeOderIst($this->Param['APD_BONNR'], awisDatenbank::AWIS_LIKE_UPPER, 'APD');
        }

        if (isset($this->Param['APD_DATUM_VON']) and $this->Param['APD_DATUM_VON'] != '') {
            $Bedingung .= ' AND APD_DATUM <= ' . $this->DB->WertSetzen('APD', 'D', $this->Param['APD_DATUM_VON']);
        }

        if (isset($this->Param['APD_DATUM_BIS']) and $this->Param['APD_DATUM_BIS'] != '') {
            $Bedingung .= ' AND APD_DATUM >= ' . $this->DB->WertSetzen('APD', 'D', $this->Param['APD_DATUM_BIS']);
        }

        return $Bedingung;
    }

    /**
     * Updatet APD Kunden, eigene Funktion, da sowohl bei Neuanlage APD DS, als auch bei ver�nderung ein Kundenupdate durchgef�hrt werden k�nnte
     */
    public function UpdateAPDKunden($BSK_KARTENNUMMER, $BSK_TITEL,$BSK_NACHNAME, $BSK_VORNAME, $BSK_STRASSE, $BSK_POSTLEITZAHL, $BSK_ORT, $BSK_TELEFONNUMMER, $BSK_FAXNUMMER, $BSK_EMAIL, $BSK_GEBURTSDATUM){
        $SQL  ='UPDATE APDKUNDEN';
        $SQL .=' SET ';
        $SQL .='  BSK_TITEL         = ' . $this->DB->WertSetzen('BSK','T',$BSK_TITEL);
        $SQL .=' , BSK_NACHNAME      = '. $this->DB->WertSetzen('BSK','T',$BSK_NACHNAME);
        $SQL .=' , BSK_VORNAME       = '. $this->DB->WertSetzen('BSK','T',$BSK_VORNAME);
        $SQL .=' , BSK_STRASSE       = '. $this->DB->WertSetzen('BSK','T',$BSK_STRASSE);
        $SQL .=' , BSK_POSTLEITZAHL  = '. $this->DB->WertSetzen('BSK','T',$BSK_POSTLEITZAHL);
        $SQL .=' , BSK_ORT           = '. $this->DB->WertSetzen('BSK','T',$BSK_ORT);
        $SQL .=' , BSK_TELEFONNUMMER = '. $this->DB->WertSetzen('BSK','T',$BSK_TELEFONNUMMER);
        $SQL .=' , BSK_FAXNUMMER     = '. $this->DB->WertSetzen('BSK','T',$BSK_FAXNUMMER);
        $SQL .=' , BSK_EMAIL         = '. $this->DB->WertSetzen('BSK','T',$BSK_EMAIL);
        $SQL .=' , BSK_GEBURTSDATUM  = '. $this->DB->WertSetzen('BSK','D',$BSK_GEBURTSDATUM);
        $SQL .=' , BSK_USER          = '. $this->DB->WertSetzen('BSK','T',$this->AWISBenutzer->BenutzerName());
        $SQL .=' , BSK_USERDAT       = sysdate';
        $SQL .=' WHERE BSK_KARTENNUMMER  = '.$this->DB->WertSetzen('BSK','T',$BSK_KARTENNUMMER);

        $this->DB->Ausfuehren($SQL,'',true,$this->DB->Bindevariablen('BSK'));
    }


}