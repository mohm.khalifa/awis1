<?php

/***************************************
* Jahres�bersicht
***************************************/
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');


global $AWIS_KEY1;

try 
{
	$AWISBenutzer = awisBenutzer::Init();	
	$Form = new awisFormular();
	
	$TextKonserven = array();				
	$TextKonserven[]=array('Ausdruck','txt_KeineDatenGefunden');	
	$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
	$TextKonserven[]=array('Wort','wrd_Stand');	
	$TextKonserven[]=array('Wort','Seite');
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht4505 = $AWISBenutzer->HatDasRecht(4505);
	if($Recht4505==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI_Auswertung'));					
		
	$SQL = "SELECT xbn_name AS Mitarbeiter, PEA_KEY, PEA_BEZEICHNUNG, ";
	$SQL .= " DECODE(PEI_PEA_KEY,8,PEI_FIL_ID ||  ', ' || FIL_BEZ,NULL) AS Filiale, ";
	$SQL .= " to_char(PEI_DATUM,'RRRR') AS Jahr, to_char(PEI_DATUM,'MM') AS Monat, XBN_KEY, PEI_DATUM ";				 	

	//$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';		
	
	$SQL .= ' FROM Personaleinsaetze';
	$SQL .= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';	
	$SQL .= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	$SQL .= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';	
	$SQL .= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
	
	$SQL .= " WHERE PEA_KEY <> 9 AND to_char(PEI_DATUM,'D')=6 AND (PEI_PLANUNGSTYP is null or PEI_PLANUNGSTYP = 2)";
	//$SQL .= " WHERE PEA_KEY <> 9 AND to_char(PEI_DATUM,'D')=6";
	//$SQL .= " AND MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";;
		
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$SQL .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$SQL .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}
	
	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$SQL .= ' AND PEI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID'],false);
	}

	// Einschr�nken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
		if($Param['PBM_XBN_KEY']==0)
		{
			$SQL .= " AND EXISTS (SELECT *";
			$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$SQL .= "       FROM Perseinsbereichemitglieder";
			$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			$SQL .= " GROUP BY PEB_KEY) Bereiche";
			$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			$SQL .= ' WHERE pbm_xbn_key = PEI_XBN_KEY';
			$SQL .= ' )';
		}
		else
		{
			$SQL .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY'],false);
		}
	}
	
	if(isset($Param['FER_KEY']) AND $Param['FER_KEY']!='0')
	{

		$SQL .= ' AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN';
		$SQL .= ' WHERE xx1_fer_key =0'.$DB->FeldInhaltFormat('N0',$Param['FER_KEY'],false);
		$SQL .= ' AND xx1_kon_key = KON_KEY)';
		/*
		  $Bedingung = " AND EXISTS (SELECT *
		  FROM (
		  SELECT KON_KEY, XX1_FER_KEY, FEB_GUELTIGAB, FRZ_GUELTIGAB, FEZ_GUELTIGAB, FER_GUELTIGAB,
		  FEB_GUELTIGBIS, FRZ_GUELTIGBIS, FEZ_GUELTIGBIS, FER_GUELTIGBIS
		  FROM v_FilialEbenenRollen
		  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY
		  INNER JOIN FilialEbenen ON XX1_FEB_KEY = FEB_KEY
		  INNER JOIN FilialEbenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY
		  INNER JOIN FilialEbenenZuordnungen ON XX1_FEZ_KEY = FEZ_KEY
		  INNER JOIN FilialEbenenRollen ON XX1_FER_KEY = FER_KEY
		  INNER JOIN FilialEbenenRollenBereiche ON FER_FRB_KEY = FRB_KEY
		  WHERE XX1_FER_KEY =".$Param['FER_KEY']."
		  ORDER BY XX1_STUFE, FRZ_GUELTIGBIS, FRZ_USERDAT DESC
		  ) Daten WHERE KON_KEY = XBN_KON_KEY
		  AND FEB_GUELTIGAB <= PEI_DATUM AND FEB_GUELTIGBIS >= PEI_DATUM
		  AND FRZ_GUELTIGAB <= PEI_DATUM AND FRZ_GUELTIGBIS >= PEI_DATUM
		  AND FEZ_GUELTIGAB <= PEI_DATUM AND FEZ_GUELTIGBIS >= PEI_DATUM
		  AND FER_GUELTIGAB <= PEI_DATUM AND FER_GUELTIGBIS >= PEI_DATUM
		  ) ";
		*/
	}

//	if($_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
//	{
//		$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
//	}	
	
		//$SQL .= ' ORDER BY MIT_REZ_ID, MIT_KEY, MEI_TAG, MEI_VONXTZ_ID';
	$SQL .= ' ORDER BY XBN_KEY, PEI_DATUM, PEI_VONXTZ_ID';
	
		
	$rsPEI = $DB->RecordSetOeffnen($SQL);
	$rsPEIZeilen = $rsPEI->AnzahlDatensaetze();
	
	$Vorlagen = array('BriefpapierATU_DE_Seite_2.pdf');

	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Personaleinsatz');			

	if($rsPEIZeilen==0)
	{
		$Ausdruck->NeueSeite(0,1);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_KeineDatenGefunden'],0,0,'C',0);			
	}

	$RasterZeile=9999;
	$LetzterMitarbeiter = '';
	$LetztesJahr = '';
	

	while(!$rsPEI->EOF())		
	{
			// Seitenwechsel?
		if($LetztesJahr!=$rsPEI->FeldInhalt('JAHR') OR $LetzterMitarbeiter!=$rsPEI->FeldInhalt('XBN_KEY'))
		{
			if($LetztesJahr!='')
			{
				$Ausdruck->_pdf->setXY(10,$RasterZeile+2);
				$Ausdruck->_pdf->cell(110,6,'Gesamt',1,0,'R',0);
				for($SumNr=0;$SumNr<4;$SumNr++)
				{
					$Ausdruck->_pdf->setXY(($SumNr*10)+120,$RasterZeile+2);
					$Ausdruck->_pdf->cell(10,6,$Summen[$SumNr],1,0,'C',0);
				}
			}
			$Summen = array();		// Neue Summen
			$Summen[0]=0;
			$Summen[1]=0;
			$Summen[2]=0;
			$Summen[3]=0;

			$LetztesJahr=$rsPEI->FeldInhalt('JAHR');
			$LetzterMitarbeiter=$rsPEI->FeldInhalt('XBN_KEY');

			$RasterZeile = 9999;	// Neue Seite erzwingen
		}

			// Neue Seite beginnen
		if($RasterZeile > 270)	
		{
			$Ausdruck->NeueSeite(0,1);
			
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(140,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

			$Ausdruck->_pdf->setXY(10,12);
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);
				
			$Ausdruck->_pdf->setXY(10,5);
			$Ausdruck->_pdf->SetFont('Arial','',16);
			$Ausdruck->_pdf->cell(190,6,"Einsatz Samstage vom " . $Param['PEI_DATUM_VOM'] . ' bis ' . $Param['PEI_DATUM_BIS'],0,0,'L',0);			

			$RasterZeile = 40;

			$Ausdruck->_pdf->SetFillColor(240,240,240);
			$Ausdruck->_pdf->SetDrawColor(0,0,0);
			$Ausdruck->_pdf->setXY(10,$RasterZeile-10);

			$Ausdruck->_pdf->SetFont('Arial','',12);
			$Ausdruck->_pdf->cell(150,5,$rsPEI->FeldInhalt('MITARBEITER') . ' (RZZZZZ)',1,0,'L',1);

			$Ausdruck->_pdf->SetFont('Arial','',9);
			$Ausdruck->_pdf->setXY(10,$RasterZeile-4);
			$Ausdruck->_pdf->cell(20,5,'Datum',1,0,'C',1);
			$Ausdruck->_pdf->setXY(30,$RasterZeile-4);
			$Ausdruck->_pdf->cell(100,5,'T�tigkeit',1,0,'C',1);
			$Ausdruck->_pdf->setXY(130,$RasterZeile-4);
			$Ausdruck->_pdf->cell(10,5,'Frei',1,0,'C',1);
			$Ausdruck->_pdf->setXY(140,$RasterZeile-4);
			$Ausdruck->_pdf->cell(10,5,'Urlaub',1,0,'C',1);
			$Ausdruck->_pdf->setXY(150,$RasterZeile-4);
			$Ausdruck->_pdf->cell(10,5,'Krank',1,0,'C',1);
		}
				// Auf den Monat positionieren
		$RasterZeile+=5;
		$Ausdruck->_pdf->setXY(10,$RasterZeile-4);
		$LetzterTag = $rsPEI->FeldInhalt('PEI_DATUM');
		$Ausdruck->_pdf->cell(20,5,str_replace('-','.',$Form->Format('D',$rsPEI->FeldInhalt('PEI_DATUM'))),1,0,'R',0);
		$Ausdruck->_pdf->setXY(30,$RasterZeile-4);

		$Text='';
		
		$j = $rsPEI->DSNummer();
		while(!$rsPEI->EOF())
		{
			if($LetzterTag != $rsPEI->FeldInhalt('PEI_DATUM'))
			{
				break;
			}
			$Text .= ' / ' . $rsPEI->FeldInhalt('PEA_BEZEICHNUNG');
			if($rsPEI->FeldInhalt('MEA_KEY')==8)		// Filiale
			{
				$Text .= ' ' . $rsPEI->FeldInhalt('FILIALE');
			}
			if($rsPEI->FeldInhalt('PEI_BEMERKUNG')!='')
			{
				$Text .= ' (' . $rsPEI->FeldInhalt('PEI_BEMERKUNG') . ')';
			}
		
			$rsPEI->DSWeiter();	
		}
		
		$rsPEI->GeheZu($j);
		/*
		for($j=$i;$j<$rsMEIZeilen;$j++)
		{
			if($LetzterTag != $rsMEI['MEI_TAG'][$j])
			{
				break;
			}
			$Text .= ' / ' . $rsMEI['MEA_BEZEICHNUNG'][$j];
			if($rsMEI['MEA_KEY'][$j]==8)		// Filiale
			{
				$Text .= ' ' . $rsMEI['FILIALE'][$j];
			}
			if(isset($rsMEI['MEI_BEMERKUNG'][$j]) AND $rsMEI['MEI_BEMERKUNG'][$j]!='')
			{
				$Text .= ' (' . $rsMEI['MEI_BEMERKUNG'][$j] . ')';
			}
		}
		*/
		$Text = substr($Text, 3);
		$MehrEintraege = strpos($Text, '/');		// Sind mehr Eintr�ge vorhanden? (-> FREI nur dann werten, wenn es aleine steht)
		//$i=$j-1;
		
		$Ausdruck->_pdf->cell(100,5,$Text,1,0,'L',0);
		$Text = '';
						
		(!isset($Summen[0])?$Summen[0]=1:$Summen[0] ++);		
		
		$Ausdruck->_pdf->setXY(130,$RasterZeile-4);
		if($rsPEI->FeldInhalt('PEA_KEY')==4 and $MehrEintraege==0)			// Frei
		{
			(!isset($Summen[1])?$Summen[1]=1:$Summen[1] ++);					
			//$Summen[1] ++;
			$Text = 'X';
		}
		$Ausdruck->_pdf->cell(10,5,$Text,1,0,'C',0);
		$Text = '';

		$Ausdruck->_pdf->setXY(140,$RasterZeile-4);
		if($rsPEI->FeldInhalt('PEA_KEY')==3)			// Urlaub
		{
			(!isset($Summen[2])?$Summen[2]=1:$Summen[2] ++);		
			//$Summen[2] ++;
			$Text = 'X';
		}
		$Ausdruck->_pdf->cell(10,5,$Text,1,0,'C',0);
		$Text = '';

		$Ausdruck->_pdf->setXY(150,$RasterZeile-4);
		if($rsPEI->FeldInhalt('PEA_KEY')==5)			// Krank
		{
			(!isset($Summen[3])?$Summen[3]=1:$Summen[3] ++);					
			//$Summen[3] ++;
			$Text = 'X';
		}
		$Ausdruck->_pdf->cell(10,5,$Text,1,0,'C',0);
		$Text = '';
		
		//$Ausdruck->_pdf->cell(10,5,$Summen[0].' '.$Summen[1].' '.$Summen[2].' '.$Summen[3],1,0,'C',0);
		$rsPEI->DSWeiter();
	}// Ende for

		// Summe f�r den Letzten Mitarbeiter
	$Ausdruck->_pdf->setXY(10,$RasterZeile+2);
	$Ausdruck->_pdf->cell(110,5,'Gesamt',1,0,'R',0);
	for($SumNr=0;$SumNr<4;$SumNr++)
	{
		$Ausdruck->_pdf->setXY(($SumNr*10)+120,$RasterZeile+2);
		$Ausdruck->_pdf->cell(10,5,$Summen[$SumNr],1,0,'C',0);
	}
	
	$Ausdruck->Anzeigen();
}	
	
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}

?>	