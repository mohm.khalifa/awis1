<?php
// Variablen
global $_POST;
//global $HTTP_GET_VARS;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 500);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

// lokale Variablen
// Ende Variablen

//var_dump($_POST);

print "<form name=frmSuche method=post action=./filialen_auswertung_Main.php?cmdAktion=StundenSatz>";

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='')
{
	if(isset($_POST['txtVKL_GBL']) AND $_POST['txtVKL_GBL']!='')
	{	
		$SQL = 'SELECT Filialen.FIL_ID, Filialen.FIL_BEZ, FIF_WERT, V_FIL_VKL_GBL.VERKAUFSLEITER, V_FIL_VKL_GBL.GEBIETSLEITER ';
		$SQL .= 'FROM Filialen, FilialInfos, V_FIL_VKL_GBL ';
		$SQL .= 'WHERE Filialen.FIL_ID = FIF_FIL_ID AND V_FIL_VKL_GBL.FIL_ID = Filialen.FIL_ID AND FIF_FIT_ID IN (200) ';
		$SQL .= 'ORDER BY Filialen.FIL_ID';
	
		$rsAusstattung = awisOpenRecordset($con, $SQL);
		$rsAusstattungZeilen = $awisRSZeilen;
	
		$DateiName = awis_UserExportDateiName('.csv');
		$fd = fopen($DateiName,'w' );
		
		fputs($fd, "Filiale;Bezeichnung;Stundensatz;Verkaufsleiter;Gebietsleiter\n");
		for($DSNr=0;$DSNr<$rsAusstattungZeilen;$DSNr++)
		{
			fputs($fd, $rsAusstattung['FIL_ID'][$DSNr] . ';' . $rsAusstattung['FIL_BEZ'][$DSNr] . ';' . awis_format($rsAusstattung['FIF_WERT'][$DSNr],'Standardzahl') . ';' . $rsAusstattung['VERKAUFSLEITER'][$DSNr] . ';' . $rsAusstattung['GEBIETSLEITER'][$DSNr] ."\n");
		}
		fclose($fd);
		
		$DateiName = pathinfo($DateiName);
		echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
	}
	else
	{	
		$SQL = 'SELECT FIL_ID, FIL_BEZ, FIF_WERT FROM Filialen, FilialInfos ';
		$SQL .= ' WHERE FIL_ID = FIF_FIL_ID AND FIF_FIT_ID IN (200)';
		$SQL .= ' ORDER BY FIL_ID';
	
		$rsAusstattung = awisOpenRecordset($con, $SQL);
		$rsAusstattungZeilen = $awisRSZeilen;
	
		$DateiName = awis_UserExportDateiName('.csv');
		$fd = fopen($DateiName,'w' );
		
		fputs($fd, "Filiale;Bezeichnung;Stundensatz\n");
		for($DSNr=0;$DSNr<$rsAusstattungZeilen;$DSNr++)
		{
			fputs($fd, $rsAusstattung['FIL_ID'][$DSNr] . ';' . $rsAusstattung['FIL_BEZ'][$DSNr] . ';' . awis_format($rsAusstattung['FIF_WERT'][$DSNr],'Standardzahl') . "\n");
		}
		fclose($fd);
		
		$DateiName = pathinfo($DateiName);
		echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
	}

}
else
{
	echo '<br>';
	echo '<b>Folgende Daten werden generiert:</b>';
	echo '<ul><li>Aktuelle Stundens�tze pro Filiale.</li>';
	echo "<br><li><input type=checkbox value=on name=txtVKL_GBL>&nbsp;Verkaufsleiter / Gebietsleiter anzeigen</li></ul>";
	print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
}

//print "<input type=hidden name=cmdAktion value=Austattung>";
print "</form>";

?>

