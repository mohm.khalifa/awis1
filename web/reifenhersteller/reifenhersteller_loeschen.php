<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try 
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form->DebugAusgabe(1,'l�schen:');
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);

	$Tabelle= '';
	
	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtREH_KEY']))
		{
			$Tabelle = 'REH';
			$Key=$_POST['txtREH_KEY'];
	
			$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG, REH_KURZBEZEICHNUNG';
			$SQL .= ' FROM REIFENHERSTELLER ';
			$SQL .= ' WHERE REH_KEY=0'.$Key;
			
			$rsDaten = $DB->RecordsetOeffnen($SQL);
			$HauptKey = $rsDaten->FeldInhalt('REH_KEY');
				
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('REH','REH_KEY'),$rsDaten->FeldInhalt('REH_KEY'));
			$Felder[]=array($Form->LadeTextBaustein('REH','REH_BEZEICHNUNG'),$rsDaten->FeldInhalt('REH_BEZEICHNUNG'));
			$Felder[]=array($Form->LadeTextBaustein('REH','REH_KURZBEZEICHNUNG'),$rsDaten->FeldInhalt('REH_KURZBEZEICHNUNG'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		switch($_GET['Seite'])
		{
			case 'Reifenstamm':
				$Tabelle = 'REH';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT REH_KEY, REH_BEZEICHNUNG, REH_KURZBEZEICHNUNG';
				$SQL .= ' FROM REIFENHERSTELLER ';
				$SQL .= ' WHERE REH_KEY=0'.$Key;
								
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('REH_KEY');
					
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('REH','REH_KEY'),$rsDaten->FeldInhalt('REH_KEY'));
				$Felder[]=array($Form->LadeTextBaustein('REH','REH_BEZEICHNUNG'),$rsDaten->FeldInhalt('REH_BEZEICHNUNG'));
				$Felder[]=array($Form->LadeTextBaustein('REH','REH_KURZBEZEICHNUNG'),$rsDaten->FeldInhalt('REH_KURZBEZEICHNUNG'));
				break;
			default:
				break;
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'REH':
				$SQL = 'DELETE FROM REIFENHERSTELLER WHERE REH_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				$AWIS_KEY1='';
				break;		
			default:
				break;
		}
	}
	
	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./reifenhersteller_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
		$Form->Formular_Start();
		
		$Form->ZeileStart();		
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	
		$Form->ZeileEnde();
	
		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',200);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}
	
		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
/*		
		if (isset($SubKey))
		{
			$Form->Erstelle_HiddenFeld('SubKey',$SubKey);			// Bei den Unterseiten
		}
*/		
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();
	
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();
	
		$Form->SchreibeHTMLCode('</form>');
	
		$Form->Formular_Ende();
	
		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>