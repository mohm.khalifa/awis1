<?php


 function autoload_5d3f79afb287d4d703cae402ee2b9cc1($class)
{
    if(!defined('__DIR__')){
        define('__DIR__',dirname(__FILE__));
    }

    $classes = array(
        'PartnerserviceV3' => __DIR__ . '/PartnerserviceV3.php',
        'Bestandsauskunft' => __DIR__ . '/Bestandsauskunft.php',
        'SuchAnfrage' => __DIR__ . '/SuchAnfrage.php',
        'Serialize' => __DIR__ . '/Serialize.php',
        'BestandsabfrageV3' => __DIR__ . '/BestandsabfrageV3.php',
        'BestandV3' => __DIR__ . '/BestandV3.php',
        'ArrayOfBestandsabfrageV3' => __DIR__ . '/ArrayOfBestandsabfrageV3.php',
        'SuchAnfrageArtikel' => __DIR__ . '/SuchAnfrageArtikel.php',
        'ArrayOfSuchAnfrageArtikel' => __DIR__ . '/ArrayOfSuchAnfrageArtikel.php',
        'BestandsauskunftResponse' => __DIR__ . '/BestandsauskunftResponse.php',
        'ArrayOfBestandV3' => __DIR__ . '/ArrayOfBestandV3.php',
        'Bestellung' => __DIR__ . '/Bestellung.php',
        'PartnerBestellungV3' => __DIR__ . '/PartnerBestellungV3.php',
        'Versandart' => __DIR__ . '/Versandart.php',
        'ArrayOfBestellPosition' => __DIR__ . '/ArrayOfBestellPosition.php',
        'BestellPosition' => __DIR__ . '/BestellPosition.php',
        'OrderLieferadresse' => __DIR__ . '/OrderLieferadresse.php',
        'BestellungResponse' => __DIR__ . '/BestellungResponse.php',
        'PartnerBestellBestaetigungV3' => __DIR__ . '/PartnerBestellBestaetigungV3.php',
        'ArrayOfBestaetigungPosition' => __DIR__ . '/ArrayOfBestaetigungPosition.php',
        'BestaetigungPosition' => __DIR__ . '/BestaetigungPosition.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };

}

spl_autoload_register('autoload_5d3f79afb287d4d703cae402ee2b9cc1');

// Do nothing. The rest is just leftovers from the code generation.
{
}
