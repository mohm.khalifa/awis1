<?php
/**
 * Details Stammdatenpflege GDV-Satzarten
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607131457
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Fehler;

try
{

	$script = "<script type='text/javascript'>
	function pflichtAus() {
		document.getElementsByName('txtGDN_SATZARTEN')[0].required = false;  // required aus 	
	}
	</script>";

	echo $script;

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('GDV','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht44003 = $AWISBenutzer->HatDasRecht(44004);
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GDV_SA'));
	$Fehler = false;

	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1);
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);


	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'VVE_BEZEICHNUNG';
	}
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}
	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	if($Recht44003==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(isset($_GET['VVW_VERSNR']))
	{
		$AWIS_KEY1 = $_GET['VVW_VERSNR'];
	}
	else
	{
		$AWIS_KEY1='';
	}

	if(isset($_POST['txtAWIS_KEY1'])) {
		$AWIS_KEY1 = $_POST['txtAWIS_KEY1'];
	}

	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	if(isset($_POST['cmdSuche_x']))
	{
		$Param['VMV_VERSICHERUNG'] = 	$Form->Format('N0',$_POST['sucVMV_VERSICHERUNG'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	if($FelderNew != '')
	{
		include('./satzarten_speichern.php');
	}
	if($FelderDel != '' or isset($_POST['cmdLoeschenOK']) or isset($_POST['cmdLoeschen_x']))
	{
		include('./satzarten_loeschen.php');
	}

	/* Daten beschaffen */
	$SQL  ='SELECT';
	$SQL .='   VVE_VERSNR,VVE_BEZEICHNUNG,VVW_ZUSATZWEG,VVW_RELEASE, ';
	$SQL .='   row_number() OVER (';
	$SQL .= ' order by ' . $Param['ORDER'] .' )  AS ZeilenNr';
	$SQL .=' FROM';
	$SQL .='   (';
	$SQL .='SELECT ';
	$SQL .='VVE_VERSNR,VVE_BEZEICHNUNG,VVW_ZUSATZWEG,VVW_RELEASE ';
	$SQL .=' FROM ';
	$SQL .='versversandweg ';
	$SQL .=' inner join versversicherungen on VVW_VERSNR = VVE_VERSNR';
	$SQL .=' where VVW_RELEASE is not null';
	$SQL .='   )daten';

	if ((isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '') and !isset($_POST['cmdSpeichern_x']) and !isset($_POST['cmdLoeschenOK']) and !isset($_POST['cmdLoeschen_x']))
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}

	if($AWIS_KEY1 != '' and !isset($_POST['cmdSpeichern_x']))
	{
		if(!isset($_POST['cmdSuche_x'])){
			$SQL .= ' WHERE VVE_VERSNR='.$AWIS_KEY1;
		}
		else{
			$SQL .= ' AND VVE_VERSNR='.$AWIS_KEY1;
		}
	}

	if(isset($_POST['sucVNT_VERSICHERUNG']) and isset($_POST['cmdSuche_x']))
	{
		$AWIS_KEY1 = $_POST['sucVNT_VERSICHERUNG'];
	}
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Blättern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

	//AWIS_KEY1 ist leer, wenn man über die Suche kommt. Wenn gespeichert wird(AWIS_KEY1 ist dann nicht leer), kommt man wieder in die Liste, deswegen auch die Blockparameter hernehmen.
	if($AWIS_KEY1 == '' or (isset($_POST['cmdSpeichern_x'])))
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsVVW = $DB->RecordSetOeffnen($SQL);
	// Spaltenbreiten für Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['BES_AUFTRAGSART'] = 500;
	$FeldBreiten['BES_VERALTET'] = 20;

	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 4);
	}
	$Gesamtbreite += 20;

	$Form->SchreibeHTMLCode('<form name="frmVVWDetails" action="./satzarten_Main.php?cmdAktion=Details'.' " method=POST  enctype="multipart/form-data">');
	$Form->Formular_Start();

	if (($rsVVW->AnzahlDatensaetze() > 1))
	{
		$FeldBreiten = array();
		$FeldBreiten['Blank'] = 38;
		$FeldBreiten['VERSICHERUNG'] = 350;
		$Param['BLOCK']=1;

		$count = 0;
		$Gesamtbreite = '';
		foreach ($FeldBreiten as $value)
		{
			$Gesamtbreite += $value;
			$count++;
		}

		

		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');


		$Link= '';

		$Link = './satzarten_Main.php?cmdAktion=Details&Sort=VVE_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VVE_BEZEICHNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'], $FeldBreiten['VERSICHERUNG'], '', $Link);
		$Form->ZeileEnde();
		$DS = 0;
		while(! $rsVVW->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			$Icons[] = array('edit','./satzarten_Main.php?cmdAktion=Details&VVW_VERSNR='.$rsVVW->FeldInhalt('VVE_VERSNR'));
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$TTT = $rsVVW->FeldInhalt('VVE_BEZEICHNUNG');
			$Form->Erstelle_ListenFeld('VVE_BEZEICHNUNG', $rsVVW->FeldInhalt('VVE_BEZEICHNUNG'), 0, $FeldBreiten['VERSICHERUNG'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsVVW->DSWeiter();
		}

		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['GDV']['SummeGesamtDS'].': '.$Form->Format('N0',$rsVVW->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './satzarten_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
	}
	elseif($rsVVW->AnzahlDatensaetze() == 1 or $AWIS_KEY1 == '-1')
	{
		/* Daten beschaffen */
		$SQL ='     SELECT';

		if($rsVVW->FeldInhalt('VVE_VERSNR') == '')
		{
			$VVE_VERSNR ='-1';
			$SQL .='   distinct VVW_VERSNR,VVE_BEZEICHNUNG';
		}
		else{
			$VVE_VERSNR=$rsVVW->FeldInhalt('VVE_VERSNR');
			$SQL .='   VVW_VERSNR,VVE_BEZEICHNUNG,VVW_USER,VVW_USERDAT,VVW_ZUSATZWEG';
		}

		$SQL .='     FROM';
		$SQL .='       versversandweg';
		$SQL .='    inner join versversicherungen on VVW_VERSNR = VVE_VERSNR';

		$SQL2 =' where VVE_VERSNR ='.$VVE_VERSNR;

		if($rsVVW->FeldInhalt('VVE_VERSNR') == '')
		{
			$SQL3 = ' left join versnachrichtentypen on VVW_VERSNR = VNT_VERSNR ';
			$SQL3 .= ' WHERE VNT_VERSNR is not null ';
		}
		else{
			$SQL3 = '';
		}

		$rsVVE = $DB->RecordSetOeffnen($SQL.$SQL2);

		$edit = true;
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./satzarten_Main.php?cmdAktion=Details  accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVVE->FeldInhalt('VVW_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVVE->FeldInhalt('VVW_USERDAT'));
		$Form->InfoZeile($Felder,'');


		if($rsVVW->AnzahlDatensaetze() == 1 and $AWIS_KEY1 != '-1') {
			$edit = false;
		}
		else
		{
			$edit = true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'].':',200);
		$Form->Erstelle_SelectFeld('!VVW_VERSICHERUNG',$rsVVW->FeldInhalt('VVE_VERSNR'),400,$edit,$SQL.$SQL3,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'','','','',array(),'');
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

		if($AWIS_KEY1 != '-1') {
			$FeldBreiten = array();
			$FeldBreiten['Blank'] = 20;
			$FeldBreiten['Nummer'] = 250;
			$FeldBreiten['Nachrichtentyp'] = 250;
			
			$SQL = 'select VSA_KEY,VSA_VERSNR,VSA_GDN_NUMMER,GDN_BEZEICHNUNG,VSA_GDS_NUMMER,GDS_BEZEICHNUNG from verssatzarten ';
			$SQL .= ' inner join versversandweg on VVW_VERSNR =  VSA_VERSNR';
			$SQL .= ' inner join gdvrelease on GDR_RELEASE =  VVW_RELEASE';
			$SQL .= ' left join gdvnachrichtentypen on gdn_gdr_key = gdr_key and gdn_nummer = VSA_GDN_NUMMER';
			$SQL .= ' left join gdvsatzarten on gds_gdr_key = gdr_key and GDS_NUMMER = VSA_GDS_NUMMER';
			$SQL .= ' where vsa_versnr = '.$VVE_VERSNR;
			$SQL .= ' order by VSA_GDN_NUMMER NULLS FIRST,VSA_GDS_NUMMER';

			$Form->Erstelle_HiddenFeld('AWIS_KEY1', $VVE_VERSNR);


			$rsZuord = $DB->RecordSetOeffnen($SQL);
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['GDN_BEZEICHNUNG'], $FeldBreiten['Nummer'], '',
				'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['GDS_SATZART'],
				$FeldBreiten['Nachrichtentyp'], '', '');
			$Form->ZeileEnde();
			$DS = 0;    // für Hintergrundfarbumschaltung

			while (!$rsZuord->EOF()) {
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array('delete', 'POST~' . $rsZuord->FeldInhalt('VSA_KEY'));
				$Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2),'','',' onclick="pflichtAus();"');
				$TTT = $rsZuord->FeldInhalt('VNT_GDN_NUMMER');
				$Form->Erstelle_ListenFeld('GDN_BEZEICHNUNG', $rsZuord->FeldInhalt('VSA_GDN_NUMMER') != ''?$rsZuord->FeldInhalt('VSA_GDN_NUMMER').'~'.$rsZuord->FeldInhalt('GDN_BEZEICHNUNG'):'', 0,
					$FeldBreiten['Nummer'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
				$TTT = $rsZuord->FeldInhalt('GDN_BEZEICHNUNG');
				$Form->Erstelle_ListenFeld('GDN_BEZEICHNUNG', $rsZuord->FeldInhalt('VSA_GDS_NUMMER').'~'.$rsZuord->FeldInhalt('GDS_BEZEICHNUNG'), 0,
					$FeldBreiten['Nachrichtentyp'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
				$Form->ZeileEnde();
				$DS++;
				$rsZuord->DSWeiter();
			}
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();



			$SQL2 = ' select GDN_KEY,GDN_NUMMER||\'~\'||GDN_BEZEICHNUNG from gdvnachrichtentypen gnt  ';
			$SQL2 .= ' inner join gdvrelease r on gnt.GDN_GDR_KEY=r.GDR_KEY';
			$SQL2 .= ' inner join VERSVERSANDWEG vve on r.GDR_RELEASE=vve.VVW_RELEASE';
			$SQL2 .= ' inner join VERSNACHRICHTENTYPEN vnt on vnt.VNT_VERSNR = vve.VVW_VERSNR and gnt.GDN_NUMMER=vnt.VNT_GDN_NUMMER';
			$SQL2 .= ' inner join (select count(*) as cnt, gns_gdn_key from GDVNACHRICHTENTYPENSATZARTEN  group by gns_gdn_key) cnt_nts on  cnt_nts.gns_gdn_key = gnt.GDN_KEY';
			$SQL2 .= ' left join (select count(*) as cnt, vsa_gdn_nummer,vsa_versnr from VERSSATZARTEN group by vsa_gdn_nummer,vsa_versnr) cnt_vnts on cnt_vnts.vsa_gdn_nummer = gnt.GDN_NUMMER and cnt_vnts.vsa_versnr = vvw_VERSNR';
			$SQL2 .= ' where vve.VVW_VERSNR='.$VVE_VERSNR;
			$SQL2 .= ' and (';
			$SQL2 .= 'cnt_vnts.cnt is null';
			$SQL2 .= ' or cnt_vnts.cnt<cnt_nts.cnt';
			$SQL2 .= ')';
			$SQL2 .= ' order by gnt.GDN_NUMMER';


			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDN_BEZEICHNUNG'].':',180);
			$Form->Erstelle_SelectFeld('GDN_NACHRICHTENTYPEN','', '320:320', true, $SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '',
				'', array(), 'font-size:14px;height:22px');
			$Icons = array();
			$Icons[] = array('add', 'POST~' . $AWIS_KEY1);
			$Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2));

			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDS_SATZART'].':',160);
			$Form->Erstelle_SelectFeld('!GDN_SATZARTEN','','350:320', true,
				'***GDS_Daten;txtGDN_NACHRICHTENTYPEN;VVE_NTYP=*txtGDN_NACHRICHTENTYPEN&ZUSATZ=~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'].'&VVE_VERSNR='.$VVE_VERSNR,
				'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'', '');
			$Form->ZeileEnde();


			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
		else{
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();

			$Form->Erstelle_HiddenFeld('Key',$AWIS_KEY1);
		}

	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	// Zurück zum Menü
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/gdvService/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if($AWIS_KEY1 == '-1')//Speichern wenn und 1(Detailansicht) oder kein (Neuanlage) Datensatz gefunden wurden
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	$AWISBenutzer->ParameterSchreiben('Formular_GDV_SA',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{

	if($Form instanceof awisFormular)
	{

		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $DB;
	global $Form;

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';


	if(isset($Param['VMV_VERSICHERUNG']) AND ($Param['VMV_VERSICHERUNG']!='0' and $Param['VMV_VERSICHERUNG']!=''))
	{
		$Bedingung .= ' AND VVE_VERSNR ' . $DB->LikeOderIst($Param['VMV_VERSICHERUNG']);
	}



	return $Bedingung;
}

?>