<?php

require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');


$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[] = array('FGK', '%');
$TextKonserven[] = array('FKA', '%');
$TextKonserven[] = array('FKH', '%');
$TextKonserven[] = array('FGP', '%');
$TextKonserven[] = array('FPH', '%');

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Spalte = 15;
$Zeile = 20;

$SQL = "SELECT * FROM ( ";
$SQL .= " SELECT FKA_AEMNR, FKA_FILID, FKA_BETRAG, FGK_KFZKENNZ, FKA_WANR, sum(FGP_ANZAHL*FGP_OEMPREIS)*1.19 as PosBetrag, FGP_VORGANGNR, FGK_FGN_KEY, FGK_ZEITSTEMPEL";
$SQL .= " FROM FGKOPFDATEN INNER JOIN FGPOSITIONSDATEN ON FGK_KEY = FGP_FGK_KEY";
$SQL .= " LEFT JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR";
$SQL .= " GROUP BY FKA_AEMNR, FKA_FILID, FKA_BETRAG, FGK_KFZKENNZ, FKA_WANR,FGP_VORGANGNR, FGK_FGN_KEY, FGK_ZEITSTEMPEL)";
$SQL .= " WHERE FGK_FGN_KEY = 2 AND FGK_KFZKENNZ IN";
$SQL .= " ( SELECT FGK_KFZKENNZ FROM FGKOPFDATEN INNER JOIN FGPOSITIONSDATEN ON FGK_KEY = FGP_FGK_KEY";
$SQL .= " LEFT JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR";
$SQL .= " WHERE FGK_FGN_KEY = 2 )";
$SQL .= " ORDER BY FGP_VORGANGNR";

$rsStornoliste = $DB->RecordSetOeffnen($SQL);

$Ausdruck = new awisAusdruck('L', 'A4', array('BriefpapierATU_DE_Seite_2_quer.pdf'));
$Ausdruck->NeueSeite(0, 1);

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial', 'B', 10);

$Zeile = 30;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_AEMNR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(25, 6, $AWISSprachKonserven['FKA']['FKA_FILID'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_BETRAG'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_KFZKENNZ'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_WANR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(30, 6, 'PosBetrag', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(35, 6, $AWISSprachKonserven['FGP']['FGP_VORGANGNR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(20, 6, 'Status', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(40, 6, 'Zeitstempel', 'LTRB', 0, 'L', 0);


$count = 0;

while (!$rsStornoliste->EOF())
{
    $Zeile += 5;

    if ($count < 30)
    {
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_AEMNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 6, $rsStornoliste->FeldInhalt('FKA_FILID'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_BETRAG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_KFZKENNZ'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_WANR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('POSBETRAG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(35, 6, $rsStornoliste->FeldInhalt('FGP_VORGANGNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $rsStornoliste->FeldInhalt('FGK_FGN_KEY'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(40, 6, $rsStornoliste->FeldInhalt('FGK_ZEITSTEMPEL'), 'LTRB', 0, 'L', 0);
    }
    else
    {
        $Zeile = 5;
        $count = 0;
        $Ausdruck->_pdf->AddPage();

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_AEMNR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 6, $AWISSprachKonserven['FKA']['FKA_FILID'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_BETRAG'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_KFZKENNZ'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $AWISSprachKonserven['FKA']['FKA_WANR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, 'PosBetrag', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(35, 6, $AWISSprachKonserven['FGP']['FGP_VORGANGNR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, 'Status', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(40, 6, 'Zeitstempel', 'LTRB', 0, 'L', 0);

        $Zeile += 5;

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_AEMNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 6, $rsStornoliste->FeldInhalt('FKA_FILID'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_BETRAG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_KFZKENNZ'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('FKA_WANR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(30, 6, $rsStornoliste->FeldInhalt('POSBETRAG'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(35, 6, $rsStornoliste->FeldInhalt('FGP_VORGANGNR'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(20, 6, $rsStornoliste->FeldInhalt('FGK_FGN_KEY'), 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(40, 6, $rsStornoliste->FeldInhalt('FGK_ZEITSTEMPEL'), 'LTRB', 0, 'L', 0);
    }

    $count++;
    $rsStornoliste->DSWeiter();
}


$Ausdruck->Anzeigen();
?>