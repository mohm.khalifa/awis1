<?php
global $AWIS_KEY1;
global $ABG;
try {

    $ABG->Form->Formular_Start();
    echo '<form name=frmabe action=./airbag_Main.php?cmdAktion=Drucken method=POST>';

    $Script = '
        <script>
        //Alle anchecken..
        $( document ).ready(function() {
            $("#txtcheckAlle").change(function() {
                $(".anzeigen_chk").prop(\'checked\',  $("#txtcheckAlle").prop(\'checked\'));
            });
        });
        </script>
    ';

    echo $Script;

    if (($ABG->Recht57000 & 4) == 4) { //Auftr�ge anderer User mit drucken?
        $ABG->Form->ZeileStart();
        $ABG->Form->Erstelle_TextLabel($ABG->AWISSprachKonserven['ABG']['ABG_AUFTRAEGE_ANDERER'] . ':', 170);
        $ABG->Form->Erstelle_Checkbox('AndereUser', isset($_POST['txtAndereUser'])?'1':0, 20, true, 1, '', '', 'onChange="this.form.submit()"');
        $ABG->Form->ZeileEnde();
    }

    $Drucken = false;
    $ABG->Form->ZeileStart();
    $ABG->Form->Trennzeile('L');
    $ABG->Form->ZeileEnde();

    $ABG->Form->ZeileStart();
    $ABG->Form->Erstelle_TextLabel($ABG->AWISSprachKonserven['ABG']['ABG_ZU_DRUCKEN'], 200, 'font-weight: bold; font-size: 15px');
    $ABG->Form->ZeileEnde();

    $SQL = 'SELECT ABG_KEY, ABG_BESTELLNR, ABG_FILIALE, ABG_DATUM, ABG_MODELLNR, ABG_MENGE, ABG_BEZEICHNUNG, ABG_AUTO, ABG_XBN_KEY FROM AIRBAGS';

    if (!isset($_POST['txtAndereUser'])) {
        $SQL .= ' WHERE ABG_XBN_KEY = ' . $ABG->DB->WertSetzen('ABG', 'N0', $ABG->AWISBenutzer->BenutzerID());
    }

    $rsABE = $ABG->DB->RecordSetOeffnen($SQL, $ABG->DB->Bindevariablen('ABG'));

    if ($rsABE->EOF()) {
        $ABG->Form->ZeileStart();
        $ABG->Form->Hinweistext($ABG->AWISSprachKonserven['Fehler']['err_keineDaten']);
        $ABG->Form->ZeileEnde();
    } else {
        $FeldBreiten = array();
        $FeldBreiten['ABG_KEY'] = 90;
        $FeldBreiten['ABG_MENGE'] = 50;
        $FeldBreiten['ABG_MODELLNR'] = 150;
        $FeldBreiten['ABG_FILIALE'] = 90;
        $FeldBreiten['ABG_BESTELLNR'] = 90;
        $FeldBreiten['ABG_DATUM'] = 90;
        $FeldBreiten['ABG_BEZEICHNUNG'] = 220;
        $FeldBreiten['ABG_AUTO'] = 220;
        $Link = '';
        $ABG->Form->ZeileStart();
        ob_start();
        $ABG->Form->Erstelle_Checkbox('checkAlle', 1, 20, true, 1, '', '', '');
        $Inhalt = ob_get_clean();
        $ABG->Form->Erstelle_Liste_Ueberschrift($Inhalt, 20, 'padding-left: 5px;padding-top: 2px;');
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_KEY'], $FeldBreiten['ABG_KEY'], '', $Link);
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_MENGE'], $FeldBreiten['ABG_MENGE'], '', $Link);
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_MODELLNR'], $FeldBreiten['ABG_MODELLNR'], '', $Link);
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_FILIALE'], $FeldBreiten['ABG_FILIALE'], '', $Link);
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_BESTELLNR'], $FeldBreiten['ABG_BESTELLNR'], '', $Link);
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_DATUM'], $FeldBreiten['ABG_DATUM'], '', $Link);
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_BEZEICHNUNG'], $FeldBreiten['ABG_BEZEICHNUNG'], '', $Link);
        $ABG->Form->Erstelle_Liste_Ueberschrift($ABG->AWISSprachKonserven['ABG']['ABG_AUTO'], $FeldBreiten['ABG_AUTO'], '', $Link);
        $ABG->Form->ZeileEnde();

        $DS = 0;    // f�r Hintergrundfarbumschaltung
        while (!$rsABE->EOF()) {
            $ABG->Form->ZeileStart('', 'Zeile"ID="Zeile_' . $rsABE->FeldInhalt('ABF_KEY'));
            $ABG->Form->Erstelle_ListenCheckbox('DruckKey[]', ((isset($_POST['txtDruckKey']) and !in_array($rsABE->FeldInhalt('ABG_KEY'), $_POST['txtDruckKey']))?'':$rsABE->FeldInhalt('ABG_KEY')), 20, true, $rsABE->FeldInhalt('ABG_KEY'), '', '', '', '', ($DS % 2), false, '', 'anzeigen_chk');

            $TTT = $rsABE->FeldInhalt('ABG_KEY');
            $ABG->Form->Erstelle_ListenFeld('ABG_KEY', $rsABE->FeldInhalt('ABG_KEY'), $FeldBreiten['ABG_KEY'], $FeldBreiten['ABG_KEY'], false, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsABE->FeldInhalt('ABG_MENGE');
            $ABG->Form->Erstelle_ListenFeld('ABG_MENGE_' . $rsABE->FeldInhalt('ABG_KEY'), $rsABE->FeldInhalt('ABG_MENGE'), $FeldBreiten['ABG_MENGE'], $FeldBreiten['ABG_MENGE'], true, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsABE->FeldInhalt('ABG_MODELLNR');
            $ABG->Form->Erstelle_ListenFeld('ABG_MODELLNR_' . $rsABE->FeldInhalt('ABG_KEY'), $rsABE->FeldInhalt('ABG_MODELLNR'), $FeldBreiten['ABG_MODELLNR'], $FeldBreiten['ABG_MODELLNR'], true, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsABE->FeldInhalt('ABG_FILIALE');
            $ABG->Form->Erstelle_ListenFeld('ABG_FILIALE_' . $rsABE->FeldInhalt('ABG_KEY'), $rsABE->FeldInhalt('ABG_FILIALE'), $FeldBreiten['ABG_FILIALE'], $FeldBreiten['ABG_FILIALE'], true, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsABE->FeldInhalt('ABG_BESTELLNR');
            $ABG->Form->Erstelle_ListenFeld('ABG_BESTELLNR_' . $rsABE->FeldInhalt('ABG_KEY'), $rsABE->FeldInhalt('ABG_BESTELLNR'), $FeldBreiten['ABG_BESTELLNR'], $FeldBreiten['ABG_BESTELLNR'], true, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsABE->FeldInhalt('ABG_DATUM');
            $ABG->Form->Erstelle_ListenFeld('ABG_DATUM_' . $rsABE->FeldInhalt('ABG_KEY'), $rsABE->FeldInhalt('ABG_DATUM', 'D'), $FeldBreiten['ABG_DATUM'], $FeldBreiten['ABG_DATUM'], true, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsABE->FeldInhalt('ABG_BEZEICHNUNG');
            $ABG->Form->Erstelle_ListenFeld('ABG_BEZEICHNUNG_' . $rsABE->FeldInhalt('ABG_KEY'), $rsABE->FeldInhalt('ABG_BEZEICHNUNG'), $FeldBreiten['ABG_BEZEICHNUNG'], $FeldBreiten['ABG_BEZEICHNUNG'], true, ($DS % 2), '', '', 'T', 'L', $TTT);

            $TTT = $rsABE->FeldInhalt('ABG_AUTO');
            $ABG->Form->Erstelle_ListenFeld('ABG_AUTO_' . $rsABE->FeldInhalt('ABG_KEY'), $rsABE->FeldInhalt('ABG_AUTO'), $FeldBreiten['ABG_AUTO'], $FeldBreiten['ABG_AUTO'], true, ($DS % 2), '', '', 'T', 'L', $TTT);

            $ABG->Form->ZeileEnde();
            $DS++;
            $rsABE->DSWeiter();
        }
        $Drucken = true;
    }

    $ABG->Form->Formular_Ende();
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $ABG->Form->SchaltflaechenStart();
    $ABG->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $ABG->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    if ($Drucken) {
        $ABG->Form->Schaltflaeche('image', 'cmdPDF', '', '/bilder/cmd_PDF.png', $ABG->AWISSprachKonserven['Wort']['lbl_drucken'], 'P');
    }
    $ABG->Form->SchaltflaechenEnde();

    $ABG->Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($ABG->Form instanceof awisFormular) {
        $ABG->Form->DebugAusgabe(1, $ex->getSQL());
        $ABG->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $ABG->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($ABG->Form instanceof awisFormular) {
        $ABG->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>