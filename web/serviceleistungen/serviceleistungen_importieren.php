<?php
try
{
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');


	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISWerkzeuge = new awisWerkzeuge();
	
	// Importmodus setzen
	$ImportModus = (int)$_POST['txtIMPORTMODUS'];
	if($ImportModus<0 OR $ImportModus>2)
	{
	    $ImportModus=1;
	}
	
	$Statistik['neu']=0;
	$Statistik['geaendert']=0;
	// Pr�fsumme ermitten, um zu pr�fen, ob die Datei schon mal importiert wurde
	$MD5 = md5_file($_FILES['IMPORT']['tmp_name']);
	$SQL = 'SELECT *';
	$SQL .= ' FROM IMPORTPROTOKOLL';
	$SQL .= ' WHERE XDI_BEREICH = \'SVT\'';
	$SQL .= ' AND XDI_MD5 = '.$DB->FeldInhaltFormat('T',$MD5);
	$rsXDI = $DB->RecordSetOeffnen($SQL);
	if(!$rsXDI->EOF())
	{
		// Dateien k�nnen immer nur einmal eingelesen werden
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datei wurde bereits importiert.', 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Benutzer:'.$rsXDI->FeldInhalt('XDI_USER'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datum:'.$rsXDI->FeldInhalt('XDI_DATUM'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Dateiname:'.$rsXDI->FeldInhalt('XDI_DATEINAME'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->Trennzeile();
		$Form->Formular_Ende();
	}
	else
	{
		$Form->Formular_Start();
		
		$DB->TransaktionBegin();
		
		// Zun�chst mal Daten zum Vertrag laden
		$SQL = 'SELECT SVB_KUNDENID';
		$SQL .= ' FROM SVCVereinbarungen';
		$SQL .= ' WHERE SVB_KEY = '.$DB->WertSetzen('SVB', 'N0', $_POST['txtSVB_KEY']);
		$rsSVB = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SVB'));
		
		$Datei = $_FILES['IMPORT']['tmp_name'];
		//var_dump($Datei);
		// Datei �ffnen
		$XLSXObj = PHPExcel_IOFactory::load($Datei);

        $GueltigeLaender = array('DE','AT');

		// Blatt untersuchen
		$BlattObj = $XLSXObj->getActiveSheet();
		// Kenner lesen
		$Kenner = trim($BlattObj->getCell('A1')->getValue());
		//var_dump($Kenner);
		if($Kenner != 'AWIS')
		{
			$Form->Erstelle_TextLabel('Falsche Importdatei. Kenner <b>AWIS</b> erwartet, aber <b>'.$Kenner.'</b> gefunden.', 800);
		}
		else
		{
			$Form->Erstelle_TextLabel('Daten werden importiert...', 800);

			$SQL = 'INSERT INTO IMPORTPROTOKOLL(';
			$SQL .= 'XDI_BEREICH, XDI_DATEINAME, XDI_DATUM, XDI_MD5';
			$SQL .= ', XDI_USER,XDI_USERDAT)VALUES (';
			$SQL .= ' \'SVT\'';
			$SQL .= ', '.$DB->WertSetzen('XDI','T',$_FILES['IMPORT']['name']);
			$SQL .= ', SYSDATE';
			$SQL .= ', '.$DB->WertSetzen('XDI','T',$MD5);
			$SQL .= ', '.$DB->WertSetzen('XDI','T',$AWISBenutzer->BenutzerName());
			$SQL .= ', SYSDATE)';
			$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('XDI'));
			$SQL = 'SELECT seq_XDI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$XDI_KEY=$rsKey->FeldInhalt('KEY');

			// �berschriften lesen
			$Ueberschrift=array();
			$Zeile = 2;
			$Spalte = 65;

			for($Spalte=65;$BlattObj->cellExists(chr($Spalte).$Zeile);$Spalte++)
			{
				$Ueberschrift[$BlattObj->getCell(chr($Spalte).$Zeile)->getValue()]=chr($Spalte);
			}

			// Beim Modus 2 werden alle deaktiviert, die nicht in der Datei stehen
			if($ImportModus==2)
			{
			    $SQL = 'UPDATE SVCTeilnehmer';
			    $SQL .= ' SET SVT_DATUMBIS = TRUNC(SYSDATE)-1';
			    $SQL .= ' WHERE SVT_SVB_KEY = '.$DB->WertSetzen('SVT', 'N0', $_POST['txtSVB_KEY']);
			    $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('SVT'));
			}
				
			
			$FEHLER = false;

			// Jetzt die Daten
			for($Zeile=3;$BlattObj->cellExists($Ueberschrift['EAN'].$Zeile);$Zeile++)
			{
			    // Beim Excel k�nnen leere Zeilen existieren
			    $EAN = $BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue();
			    if($EAN=='')
			    {
			        continue;
			    }

			    if(isset($Ueberschrift['KUNDENNR']) AND $BlattObj->cellExists($Ueberschrift['KUNDENNR'].$Zeile))
			    {
			    	$KundenNr = $BlattObj->getCell($Ueberschrift['KUNDENNR'].$Zeile)->getValue();
			    	if($KundenNr != $rsSVB->FeldInhalt('SVB_KUNDENID'))
			    	{
			    		$Form->ZeileStart();
			    		$Form->Erstelle_TextLabel('(F) Kundennummer '.$KundenNr.' im Import passt nicht zum aktuellen Vertrag ('.$rsSVB->FeldInhalt('SVB_KUNDENID').')!',800);
			    		$Form->ZeileEnde();
			    		$Form->Formular_Ende();
			    		
			    		$DB->TransaktionRollback();
			    		
			    		die();
			    	}
			    }
			    // Pr�froutinen f�r die Daten
                $Land = $BlattObj->getCell($Ueberschrift['LAND'].$Zeile)->getValue();
                if(!in_array($Land, $GueltigeLaender))
                {
                	$Form->ZeileStart();
                    $Form->Erstelle_TextLabel('(W) Zeile '.$Zeile.': falsches Land: '.$Land.'. Verwende DE',800);
                    $Form->ZeileEnde();
                    $Land = 'DE';
                }


				$DatumAb = $BlattObj->getCell($Ueberschrift['DATUMAB'].$Zeile)->getValue();
				if($DatumAb!='')
				{
					if(is_numeric($DatumAb) AND $DatumAb >30000 AND $DatumAb <99999)
					{
						$DatumAb = date('d.m.Y',strtotime('01-01-1900 + '.$DatumAb .' days - 2 days'));
					}
					else
					{
						$DatumAb = $Form->PruefeDatum($DatumAb,false,false,false);
					}
				}
				else
				{
					$DatumAb = date('d.m.Y');
				}

				$DatumBis = $BlattObj->getCell($Ueberschrift['DATUMBIS'].$Zeile)->getValue();
				if($DatumBis!='')
				{
					if(is_numeric($DatumBis) AND $DatumBis >30000 AND $DatumBis <99999)
					{
						$DatumBis = date('d.m.Y',strtotime('01-01-1900 + '.$DatumBis .' days - 2 days'));
					}
					else
					{
						$DatumBis = $Form->PruefeDatum($DatumBis,false,false,false);
					}
				}
				else
				{
					$DatumBis = '31.12.2030';
				}
				
				$Benachrichtigung = 0;
				switch(strtolower($BlattObj->getCell($Ueberschrift['INFOMAIL'].$Zeile)->getValue()))
				{
					case '1':
					case 'y':
					case 'j':
						$Benachrichtigung  |= 1;
						break;
				}
				switch(strtolower($BlattObj->getCell($Ueberschrift['INFOSMS'].$Zeile)->getValue()))
				{
					case '1':
					case 'j':
					case 'y':
						$Benachrichtigung  |= 2;
						break;
				}
				switch(strtolower($BlattObj->getCell($Ueberschrift['INFOBRIEF'].$Zeile)->getValue()))
				{
					case '1':
					case 'j':
					case 'y':
						$Benachrichtigung  |= 4;
						break;
				}

                if($Benachrichtigung==0)
				{
					$Form->ZeileStart();
                    $Form->Erstelle_TextLabel('(W) Zeile '.$Zeile.': keine Benachrichtigung definiert.',800);
                    $Form->ZeileEnde();
                }

				if(isset($Ueberschrift['KENNZEICHEN']) AND $BlattObj->cellExists($Ueberschrift['KENNZEICHEN'].$Zeile))
				{
					$Bezeichnung = $BlattObj->getCell($Ueberschrift['KENNZEICHEN'].$Zeile)->getValue() . ' - '. $BlattObj->getCell($Ueberschrift['NACHNAME'].$Zeile)->getValue();
				}
				else
				{
					$Bezeichnung = $BlattObj->getCell($Ueberschrift['NACHNAME'].$Zeile)->getValue().', '.$BlattObj->getCell($Ueberschrift['VORNAME'].$Zeile)->getValue();
				}
				
				$HandyNummer = $Form->Format('T0',$BlattObj->getCell($Ueberschrift['HANDY'].$Zeile)->getValue());

				//*********************************************************************************
				// Pr�fziffer berechnen und Warnung ausgeben, wenn keine Nummer passt
				//*********************************************************************************
				$Kennung = $BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue();
				if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_EAN,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
				{
					if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_EUROSA,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
					{
						if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_EUKARTE,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
						{
							if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_GRAU,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
							{
								if(!$AWISWerkzeuge->BerechnePruefziffer($Kennung,awisWerkzeuge::PRUEFZIIFER_TYP_FS_DDR,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
								{
                					$Form->ZeileStart();
                                    $Form->Erstelle_TextLabel('(W) Zeile '.$Zeile.': Kennung '.$Kennung.' kann nicht als EAN oder F&uuml;hrerschein-Nr identifiziert werden.',800);
                                    $Form->ZeileEnde();
								}
							}
						}
					}
				}
				
				//*********************************************************************************
				// Daten importieren
				//*********************************************************************************
				
				$SQL = 'SELECT *';
				$SQL .= ' FROM SVCTeilnehmer';
				$SQL .= ' WHERE SVT_SVB_KEY = '.$DB->WertSetzen('SVT', 'N0', $_POST['txtSVB_KEY']);
				$SQL .= ' AND SVT_KENNUNG = '.$DB->WertSetzen('SVT', 'T', $BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue());

				$rsSVT = $DB->RecordSetOeffnen($SQL, $DB->BindeVariablen('SVT'));

				if($rsSVT->EOF())
				{
					// Gibt es die Kennung schon einmal?
					$SQL = 'SELECT svt_svb_key, svt_bezeichnung, svb_vertragsnummer';
					$SQL .= ' FROM SVCTeilnehmer';
					$SQL .= ' INNER JOIN SVCVEREINBARUNGEN ON svt_svb_key = svb_key';
					$SQL .= ' WHERE SVT_KENNUNG = '.$DB->WertSetzen('SVT', 'T', $BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue());
					$SQL .= ' AND SVT_DATUMAB <= SYSDATE ';
					$SQL .= ' AND SVT_DATUMBIS >= SYSDATE ';
					
					$rsPruefung = $DB->RecordSetOeffnen($SQL, $DB->BindeVariablen('SVT'));
					if(!$rsPruefung->EOF())
					{
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel('Die Kennung '.$BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue(). ' exisitiert schon im Vertrag : '.$rsPruefung->FeldInhalt('SVB_VERTRAGSNUMMER').' fuer '.$rsPruefung->FeldInhalt('SVT_BEZEICHNUNG'),800);
						$Form->ZeileEnde();
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel('-- Import wird abgebrochen...',800);
						$Form->ZeileEnde();
						$Form->Formular_Ende();
						
						$DB->TransaktionRollback();
						die();
					}

					// Jetzt den neuen anlegen
					$SQL = 'INSERT INTO SVCTEILNEHMER(SVT_SVB_KEY, SVT_BEZEICHNUNG,SVT_KENNUNG,  SVT_DATUMAB,  SVT_DATUMBIS';
					$SQL .= ',SVT_BEMERKUNG,SVT_BENACHRICHTIGUNG,SVT_MAILADRESSE,SVT_SMSNUMMER,SVT_ANREDE,SVT_NAME,SVT_VORNAME,SVT_ADRESSZUSATZ';
					$SQL .= ',SVT_STRASSE,SVT_HAUSNUMMER,SVT_LAN_CODE,SVT_PLZ,SVT_ORT,SVT_KFZKENNZEICHEN,SVT_FIN';
					$SQL .= ',SVT_XDI_KEY,SVT_USER,SVT_USERDAT) VALUES (';
					$SQL .= ' '.$DB->WertSetzen('SVT','N0',$_POST['txtSVB_KEY']);
					$SQL .= ','.$DB->WertSetzen('SVT','T',KodiereText($Bezeichnung));
					$SQL .= ','.$DB->WertSetzen('SVT','T',$BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue());
					$SQL .= ','.$DB->WertSetzen('SVT','D',$DatumAb);
					$SQL .= ','.$DB->WertSetzen('SVT','D',$DatumBis);
					$SQL .= ', \'Import '.date('d.m.Y')."'";
					$SQL .= ','.$DB->WertSetzen('SVT','N0',$Benachrichtigung,false);
					$SQL .= ','.$DB->WertSetzen('SVT','T',$BlattObj->getCell($Ueberschrift['EMAIL'].$Zeile)->getValue());
					$SQL .= ','.$DB->WertSetzen('SVT',awisDatenbank::FORMAT_TEXT_TELEFON,$HandyNummer);
					$SQL .= ','.$DB->WertSetzen('SVT','T',$BlattObj->getCell($Ueberschrift['ANREDE'].$Zeile)->getValue());
					$SQL .= ','.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['NACHNAME'].$Zeile)->getValue()));
					$SQL .= ','.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['VORNAME'].$Zeile)->getValue()));
					$SQL .= ','.$DB->WertSetzen('SVT','T',substr(KodiereText($BlattObj->getCell($Ueberschrift['ZUSATZ'].$Zeile)->getValue()),0,50));
					$SQL .= ','.$DB->WertSetzen('SVT','T',substr(KodiereText($BlattObj->getCell($Ueberschrift['STRASSE'].$Zeile)->getValue()),0,100));
					$SQL .= ','.$DB->WertSetzen('SVT','T',substr(KodiereText($BlattObj->getCell($Ueberschrift['HAUSNUMMER'].$Zeile)->getValue()),0,10));
					$SQL .= ','.$DB->WertSetzen('SVT','T',$Land);
					$SQL .= ','.$DB->WertSetzen('SVT','T',$BlattObj->getCell($Ueberschrift['PLZ'].$Zeile)->getValue());
					$SQL .= ','.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['ORT'].$Zeile)->getValue()));
					$SQL .= ','.$DB->WertSetzen('SVT','T',KodiereText(isset($Ueberschrift['KENNZEICHEN'])?$BlattObj->getCell($Ueberschrift['KENNZEICHEN'].$Zeile)->getValue():''),true);
					$SQL .= ','.$DB->WertSetzen('SVT','T',(isset($Ueberschrift['FIN'])?$BlattObj->getCell($Ueberschrift['FIN'].$Zeile)->getValue():''),true);
					$SQL .= ','.$DB->WertSetzen('SVT','N0',$XDI_KEY);
					$SQL .= ','.$DB->WertSetzen('SVT','T',$AWISBenutzer->BenutzerName());
					$SQL .= ', SYSDATE)';
					$Statistik['neu']++;
				}
				else
				{
					$SQL = 'UPDATE svcteilnehmer';
					$SQL .= ' SET SVT_BEZEICHNUNG = '.$DB->FeldInhaltFormat('T',KodiereText($Bezeichnung));
					$SQL .= ', SVT_KENNUNG = '.$DB->WertSetzen('SVT','T',$BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue());
					$SQL .= ', SVT_DATUMAB = '.$DB->WertSetzen('SVT','D',$DatumAb);
					$SQL .= ', SVT_DATUMBIS = '.$DB->WertSetzen('SVT','D',$DatumBis);
					$SQL .= ', SVT_BEMERKUNG = '.$DB->WertSetzen('SVT','T','Update vom '.date('d.m.Y'));
					$SQL .= ', SVT_BENACHRICHTIGUNG = '.$DB->WertSetzen('SVT','N0',$Benachrichtigung,false);
					$SQL .= ', SVT_MAILADRESSE = '.$DB->WertSetzen('SVT','T',$BlattObj->getCell($Ueberschrift['EMAIL'].$Zeile)->getValue());
					$SQL .= ', SVT_SMSNUMMER= '.$DB->WertSetzen('SVT','T',$HandyNummer);
					$SQL .= ', SVT_ANREDE = '.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['ANREDE'].$Zeile)->getValue()));
					$SQL .= ', SVT_NAME = '.trim($DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['NACHNAME'].$Zeile)->getValue())));
					$SQL .= ', SVT_VORNAME = '.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['VORNAME'].$Zeile)->getValue()));
					$SQL .= ', SVT_ADRESSZUSATZ = '.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['ZUSATZ'].$Zeile)->getValue()));
					$SQL .= ', SVT_STRASSE = '.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['STRASSE'].$Zeile)->getValue()));
					$SQL .= ', SVT_HAUSNUMMER = '.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['HAUSNUMMER'].$Zeile)->getValue()));
					$SQL .= ', SVT_LAN_CODE = '.$DB->WertSetzen('SVT','T',$Land);
					$SQL .= ', SVT_PLZ = '.$DB->WertSetzen('SVT','T',$BlattObj->getCell($Ueberschrift['PLZ'].$Zeile)->getValue());
					$SQL .= ', SVT_ORT = '.$DB->WertSetzen('SVT','T',KodiereText($BlattObj->getCell($Ueberschrift['ORT'].$Zeile)->getValue()));
					$SQL .= ', SVT_KFZKENNZEICHEN = '.$DB->WertSetzen('SVT','T',(isset($Ueberschrift['KENNZEICHEN'])?$BlattObj->getCell($Ueberschrift['KENNZEICHEN'].$Zeile)->getValue():''),true);
					$SQL .= ', SVT_FIN = '.$DB->WertSetzen('SVT','T',(isset($Ueberschrift['FIN'])?$BlattObj->getCell($Ueberschrift['FIN'].$Zeile)->getValue():''),true);
					$SQL .= ', SVT_XDI_KEY='.$DB->WertSetzen('SVT','N0',$XDI_KEY);
					$SQL .= ', SVT_user='.$DB->WertSetzen('SVT','T',$AWISBenutzer->BenutzerName());
					$SQL .= ', SVT_userdat=sysdate';
					$SQL .= ' WHERE SVT_key=0' . $rsSVT->FeldInhalt('SVT_KEY') . '';
					$Statistik['geaendert']++;
				}

				$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('SVT'));
			}
		}
		$DB->TransaktionCommit();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('-- Neue Daten:'.$Statistik['neu'], 800);
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('-- Ge&auml;nderte Daten:'.$Statistik['geaendert'], 800);
		$Form->ZeileEnde();
		if($ImportModus==2)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel('-- Nicht enthaltene Teilnehmer wurden daektiviert!', 800);
		    $Form->ZeileEnde();
		}
		$Form->Trennzeile();
		
		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL(),$ex->getLine());
}
catch (Exception $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

/**
 * Kodierung des Textes, damit Umlaute richtig gespeichert werden
 * @param string $Text
 */
function KodiereText($Text)
{
    $Erg = iconv('UTF-8','WINDOWS-1252//TRANSLIT',$Text);
    return ($Erg);
}
?>