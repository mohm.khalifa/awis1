<?php

require_once("awisMailer.inc");
global $DB;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $BIT_KEY;
global $ScrollPosition;
global $AWISCursorPosition;

function ladeBewerbungenDetail($HRF_KEY, $AWIS_KEY1, $AWIS_KEY2, $NeuerDS = false)
{
    try {
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
        $AWISBenutzer = awisBenutzer::Init();
        $Form = new awisFormular();
        $Recht39001 = $AWISBenutzer->HatDasRecht(39001);

        $Script = "<script type=\"text/javascript\">
        function AzubFeldUebertrag()
        {
            document.getElementsByName('sucBEW_TAETIGKEIT')[0].value = document.getElementsByName('txtTaetigkeitsart')[0].value;   
        }
        </script>";

        $EditRecht = ($Recht39001 & 2) == 2;

        $TextKonserven = array();
        $TextKonserven[] = array('BEW', '%');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

        $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
        echo $Script;

        //Allgemeiner SQL für Liste + Detaildatensatz.
        $SQL = 'SELECT';
        $SQL .= '   HRT_KEY ,';
        $SQL .= '   BWH_KEY , BWH_USER, BWH_USERDAT, BWS_STEUER_ID, BWS_KEY, ';
        $SQL .= '   HRT_SUCH ,';
        $SQL .= '  HRF_KEY, HRF_FIRMIERUNG , HRK_KEY, BWS_KEY, FUNC_TEXTKONSERVE(XTX_KENNUNG, XTX_BEREICH, \'' . $AWISBenutzer->BenutzerSprache() . '\') as BWS_TEXT, ';
        $SQL .= '   EINGANGSDATUM';
        $SQL .= ' FROM';
        $SQL .= '   V_BEWERBERVERWALTUNG';
        $SQL .= ' WHERE HRK_KEY ' . $DB->LikeOderIst($AWIS_KEY1);
        $SQL .= ' and bwh_key ' . $DB->LikeOderIst($AWIS_KEY2);
        $rsBewerbungen = $DB->RecordSetOeffnen($SQL);

        $SQLDetail = 'select';
        $SQLDetail .= ' a.BIT_KEY ,';
        $SQLDetail .= ' a.BIT_INFORMATION ,';
        $SQLDetail .= ' a.BIT_BEMERKUNG ,';
        $SQLDetail .= ' a.BIT_HRF_KEY ,';
        $SQLDetail .= ' a.BIT_XRC_ID_ANZEIGE ,';
        $SQLDetail .= ' a.BIT_RECHTEBIT_ANZEIGE ,';
        $SQLDetail .= ' a.BIT_STATUS ,';
        $SQLDetail .= ' SUBSTR(bit_textkonserve,0,instr(bit_textkonserve,\':\')-1) AS  TXTBEREICH,
                       SUBSTR(bit_textkonserve,instr(bit_textkonserve,\':\')+1) AS TXTKENNUNG ,';
        $SQLDetail .= ' a.BIT_DATENQUELLE ,';
        $SQLDetail .= ' a.BIT_DEFAULT ,';
        $SQLDetail .= ' a.BIT_FORMAT ,';
        $SQLDetail .= ' a.BIT_FELDTYP ,';
        $SQLDetail .= ' a.BIT_LABELBREITE ,';
        $SQLDetail .= ' a.BIT_FELDBREITE ,';
        $SQLDetail .= ' a.BIT_FELDLAENGE ,';
        $SQLDetail .= ' a.BIT_PFLICHT ,';
        $SQLDetail .= ' a.BIT_USER ,';
        $SQLDetail .= ' a.BIT_USERDAT ,';
        $SQLDetail .= ' a.BIT_SICHTBAR ,';
        $SQLDetail .= ' a.BIT_RECHTEBIT_BEARBEITEN ,';
        $SQLDetail .= ' a.BIT_ZUORDKUERZEL ,';
        $SQLDetail .= ' a.BIT_ZUORDNUNG,';
        $SQLDetail .= ' a.BIT_SORTIERUNG, a.BIT_LETZTEZEILE';
        $SQLDetail .= ' from';
        $SQLDetail .= '   BEWERBERINFOSTYPEN a';
        $SQLDetail .= ' where';
        $SQLDetail .= '   a.BIT_STATUS     = \'A\'';
        $SQLDetail .= ' and a.BIT_SICHTBAR = 1';
        $SQLDetail .= ' and ( a.BIT_HRF_KEY = 0 or
            a.BIT_HRF_KEY ' . $DB->LikeOderIst($HRF_KEY);

        $SQLDetail .= ') order by a.BIT_SORTIERUNG asc';
        $Form->DebugAusgabe(1, 'SQL DETAIL: ' . $SQLDetail);
        $rsDetail = $DB->RecordSetOeffnen($SQLDetail);

        //Alle verwendeten Textkonserven laden.
        $SQLKonserven = 'select distinct';
        $SQLKonserven .= ' SUBSTR(bit_textkonserve,0,instr(bit_textkonserve,\':\')-1) AS  TEXTKONSERVENBEREICHE';
        $SQLKonserven .= ' from';
        $SQLKonserven .= '   BEWERBERINFOSTYPEN a';
        $SQLKonserven .= ' where';
        $SQLKonserven .= '   a.BIT_STATUS     = \'A\'';
        $SQLKonserven .= ' and a.BIT_SICHTBAR = 1';
        $SQLKonserven .= ' and ( a.BIT_HRF_KEY ' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('HRF_KEY') > 0?$rsBewerbungen->FeldInhalt('HRF_KEY'):0);
        $SQLKonserven .= ' or a.BIT_HRF_KEY = 0 )';
        $Form->DebugAusgabe(1, 'SQL Textkonserven: ' . $SQLKonserven);
        $rsKonserven = $DB->RecordSetOeffnen($SQLKonserven);
        $KonservenBIT = array();
        while (!$rsKonserven->EOF()) {
            $KonservenBIT[] = array($rsKonserven->FeldInhalt('TEXTKONSERVENBEREICHE'), '%');
            //var_dump($rsKonserven->FeldInhalt('TEXTKONSERVENBEREICHE'));
            $rsKonserven->DSWeiter();
        }

        $Konserven = $Form->LadeTexte($KonservenBIT);

        $Form->ZeileStart();
        $Form->Trennzeile('L');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Trennzeile('O');
        $Form->ZeileEnde();

        $Form->Erstelle_HiddenFeld('Step1', true);
        $ErstesFeld = true; //Für die Steuerung, ob ein ZeileEnde benötigt wird. Bei der ErstenZeile wird keins benötigt.

        if ($NeuerDS) {
            $Status = 8;
        } else {
            $Status = isset($_POST['txtBWS_KEY']) && $_POST['txtBWS_KEY'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']?$_POST['txtBWS_KEY']:$rsBewerbungen->FeldInhalt('BWS_KEY');
        }

        $AWISCursorPosition = 'txtBearbeitungsdatum';
        while (!$rsDetail->EOF()) {
            //Grundwerte holen.
            $TKB = $rsDetail->FeldInhalt('TXTBEREICH');//Textkonservenbereich
            $TKK = $rsDetail->FeldInhalt('TXTKENNUNG');//Textkonservenkennung
            $FeldAendern = true; //TODO: Ändernrecht abfragen des Feldes. Wenn null dann das allgemeine nehmen..
            $Script = '';

            //Allgemein schauen ob ich ändern darf oder nicht. Felder die Hinten eine Zuordnung auf den Status (BWS) haben dürfen auch nur in diesem geändert werden.
            if (($rsDetail->FeldInhalt('BIT_ZUORDKUERZEL') == 'BWS' and $rsDetail->FeldInhalt('BIT_ZUORDNUNG') != $Status)) {
                $FeldAendern = false;
            }

            //Schauen ob was per POST kommt, wenn nicht dann nehm der Wert aus der Datenbank

            $Feld = $rsDetail->FeldInhalt('BIT_INFORMATION');
            $Praefix = (isset($_POST['txt' . $Feld])?'txt':'suc');

            $Wert = '';

            $SQLBIF = baueBIFWertSQL($rsDetail->FeldInhalt('BIT_KEY'), $rsBewerbungen->FeldInhalt('BWH_KEY'));
            $rsBIF = $DB->RecordSetOeffnen($SQLBIF);

            if (isset($_POST[$Praefix . $Feld])) {
                $Wert = $_POST[$Praefix . $Feld];
            } else {
                $Wert = $rsBIF->FeldInhalt('BIF_WERT');

                if ($Wert == '') {
                    $Wert = $rsDetail->FeldInhalt('BIT_DEFAULT');
                }
            }

            $Ausgabe = "Feld: " . $rsDetail->FeldInhalt('BIT_INFORMATION') . "<br>" . "Wert: " . (is_array($Wert)?implode(' + ',
                    $Wert):$Wert) . "<br>" . "KZ: " . $rsDetail->FeldInhalt('BIT_ZUORDKUERZEL') . "<br>" . "Zuord: " . $rsDetail->FeldInhalt('BIT_ZUORDNUNG') . "<br>" . "BWS_KEY: " . $rsBewerbungen->FeldInhalt('BWS_KEY') . "<br>_____ <br>";

            //$Form->DebugAusgabe(2, $Ausgabe);

            //Wenn der Status schonmal da war(erkennbar daran, dass ein Wert da ist, und ich gerade auf diesen Status bin, dann darf ich das Feld nicht ändern.
            if ($Wert != '' and $rsDetail->FeldInhalt('BIT_ZUORDKUERZEL') == 'BWS' and $rsDetail->FeldInhalt('BIT_ZUORDNUNG') == $rsBewerbungen->FeldInhalt('BWS_KEY')) {
                $FeldAendern = false;
            }

            //Das Feld nur anzeigen, wenn in der Datenbank ein Wert steht und der Status zum Feld passt. Somit werden neue Infotypen nicht angezeigt, bei Datensätzen wo es diesen Infotypen noch nicht gab. Außerdem funktioniert so auch das Selectfeld, welches beim wechseln Postet und entsprechend Felder einblenden soll die am Status hängen.

            if ($rsBIF->FeldInhalt('BIF_WERT') != '' or ($rsDetail->FeldInhalt('BIT_ZUORDKUERZEL') == 'BWS' and $rsDetail->FeldInhalt('BIT_ZUORDNUNG') == $Status)) {
                //Letzte Zeile abschließen wenn erforderlich.
                if (!$ErstesFeld and $rsDetail->FeldInhalt('BIT_LETZTEZEILE') == 1) {
                    $Form->ZeileEnde();
                    $Form->ZeileStart();
                } elseif ($ErstesFeld) {
                    $Form->ZeileStart();
                    $ErstesFeld = false;
                }

                switch (substr($rsDetail->FeldInhalt('BIT_FELDTYP'), 0, 3)) {
                    case 'TXT':

                        if ($rsDetail->FeldInhalt('BIT_DATENQUELLE') == '') {
                            $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK] . ': ', $rsDetail->FeldInhalt('BIT_LABELBREITE'));
                            $Form->Erstelle_Textfeld(($rsDetail->FeldInhalt('BIT_PFLICHT') == 1?'!':'') . $rsDetail->FeldInhalt('BIT_INFORMATION'), $Wert,
                                $rsDetail->FeldInhalt('BIT_FELDLAENGE'), $rsDetail->FeldInhalt('BIT_FELDBREITE'), $FeldAendern, '', '', '', $rsDetail->FeldInhalt('BIT_FORMAT'),
                                'L', '', $rsDetail->FeldInhalt('BIT_FORMAT') == 'D'?date("d.m.Y", time()):'');
                        }

                        break;
                    case 'GSA':
                        $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK] . ': ', $rsDetail->FeldInhalt('BIT_LABELBREITE'));

                        $SQL = 'select email, KAB.KAB_ABTEILUNG   || \' &rarr; \'|| email|| \'\' from V_BEWERBERVERTEILER_ABTL a ';
                        $SQL .= ' inner join kontakteabteilungen kab on a.KAB_KEY = KAB.KAB_KEY';
                        $SQL .= ' WHERE email is not null ';

                        if ($Wert == '') {
                            $SQLTTT = $SQL . ' AND email = ' . $DB->WertSetzen('TTT', 'T', $Wert);
                            $rsTTT = $DB->RecordSetOeffnen($SQLTTT, $DB->Bindevariablen('TTT'));
                        }
                        $SQL .= ' ORDER BY KAB_ABTEILUNG ASC ';

                        if ($Wert == '') {

                            $Form->Erstelle_SelectFeld('~' . $rsDetail->FeldInhalt('BIT_INFORMATION'). 'CMB', $Wert, 230, $FeldAendern, $SQL,
                                $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '', $rsTTT->FeldInhalt(2), array(), 'max-width: 200px;', 'AWIS', 'onChange="gesendetAnUebernehmenZentrale();"');
                        }
                        if (is_array($Wert)) {
                            $array = $Wert;
                            $Wert = '';
                            $count = count($array) - 1;
                            for ($i = 0; $i <= $count; $i++) {
                                //foreach ($array as $w) {
                                $Wert .= $array[$i];
                                if (!$count == $i) {
                                    $Wert .= ';';
                                }
                            }
                        }
                        $Form->Erstelle_TextFeld($rsDetail->FeldInhalt('BIT_INFORMATION') , $Wert, 500, 500, $FeldAendern,'','','','','','','','','');
                        break;
                    case 'CMB':
                        //Label

                        $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK] . ': ', $rsDetail->FeldInhalt('BIT_LABELBREITE'));

                        //Schauen ob ich Klappfelder für diese CMB habe.
                        $SQLKLP = "Select Bewerberinfostypen.*, SUBSTR(bit_textkonserve,0,instr(bit_textkonserve,':')-1) AS  TXTBEREICH,  SUBSTR(bit_textkonserve,instr(bit_textkonserve,':')+1) AS TXTKENNUNG from Bewerberinfostypen where BIT_STATUS = 'A' 
                                    and BIT_SICHTBAR = 1 and BIT_ZUORDKUERZEL = 'BIT'
                                    and BIT_ZUORDNUNG" . $DB->LikeOderIst($rsDetail->FeldInhalt('BIT_KEY'));

                        $rsKLP = $DB->RecordSetOeffnen($SQLKLP);

                        if ($rsKLP->AnzahlDatensaetze() > 0) {
                            $Script .= ' onChange="divsklappen(\'div' . $rsDetail->FeldInhalt('BIT_INFORMATION') . '\', \'' . $rsKLP->FeldInhalt('BIT_DATENQUELLE') . '\', \'txt' . $rsDetail->FeldInhalt('BIT_INFORMATION') . '\', \'txt' . $rsKLP->FeldInhalt('BIT_INFORMATION') . '\')";';
                        }

                        if (substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 0, 3) == 'SQL') {
                            $SQLCMB = substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 4);
                            $SQLCMB = str_replace('#HRF_KEY#', $HRF_KEY, $SQLCMB);
                            $Form->DebugAusgabe(1, "SQL" . $rsDetail->FeldInhalt('BIT_INFORMATION') . ": " . $SQLCMB);
                            $Form->Erstelle_SelectFeld(($rsDetail->FeldInhalt('BIT_PFLICHT') == 1?'!':'') . $rsDetail->FeldInhalt('BIT_INFORMATION'), $Wert,
                                $rsDetail->FeldInhalt('BIT_FELDBREITE'), $FeldAendern, $SQLCMB, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Script);
                            $SQLCMB = '';
                        } elseif (substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 0, 3) == 'TXT') {
                            $Liste = substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 4);
                            $Bereich = substr($Liste, 0, strpos($Liste, ':'));
                            $Kennung = substr($Liste, strpos($Liste, ':') + 1);
                            $Liste = explode("|", $Konserven[$Bereich][$Kennung]);
                            $Form->Erstelle_SelectFeld(($rsDetail->FeldInhalt('BIT_PFLICHT') == 1?'!':'') . $rsDetail->FeldInhalt('BIT_INFORMATION'), $Wert,
                                $rsDetail->FeldInhalt('BIT_FELDBREITE'), $FeldAendern, '', $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Liste, $Script);
                        }

                        //Klappfeld für die Combobox zeichnen, falls nötig.
                        if ($rsKLP->AnzahlDatensaetze() > 0) {

                            $SQLBIF = baueBIFWertSQL($rsKLP->FeldInhalt('BIT_KEY'), $rsBewerbungen->FeldInhalt('BWH_KEY'));
                            $rsBIF = $DB->RecordSetOeffnen($SQLBIF);

                            //Werte für das Klappfeld holen
                            if (isset($_POST[$Praefix . $rsKLP->FeldInhalt('BIT_INFORMATION')])) {
                                $WertKLP = $_POST[$Praefix . $rsKLP->FeldInhalt('BIT_INFORMATION')];
                            } else {
                                $WertKLP = $rsBIF->FeldInhalt('BIF_WERT');
                                if ($WertKLP == '') {
                                    $WertKLP = $rsKLP->FeldInhalt('BIT_DEFAULT');
                                }
                            }

                            $TKB = $rsKLP->FeldInhalt('TXTBEREICH');//Textkonservenbereich
                            $TKK = $rsKLP->FeldInhalt('TXTKENNUNG');//Textkonservenkennung

                            $Klappkriterien = explode($rsKLP->FeldInhalt('BIT_DATENQUELLE'), ',');
                            //Wenn ein Wert da ist, und der ausgewählte Wert in der CMB den der Kriterien entspricht, blende das Feld ein.
                            if ($WertKLP != '' or in_array($Wert, $Klappkriterien)) {
                                $Display = 'block';
                            } else {
                                $Display = 'none';
                            }

                            $Form->SchreibeHTMLCode('<div id="div' . $rsDetail->FeldInhalt('BIT_INFORMATION') . '" style="display:' . $Display . '">');
                            $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK] . ': ', $rsKLP->FeldInhalt('BIT_LABELBREITE'));
                            $Form->Erstelle_Textfeld(($rsKLP->FeldInhalt('BIT_PFLICHT') == 1?'!':'') . $rsKLP->FeldInhalt('BIT_INFORMATION'), $WertKLP,
                                $rsKLP->FeldInhalt('BIT_FELDLAENGE'), $rsKLP->FeldInhalt('BIT_FELDBREITE'), $FeldAendern, '', '', '', $rsKLP->FeldInhalt('BIT_FORMAT'), 'L', '',
                                $rsKLP->FeldInhalt('BIT_FORMAT') == 'D'?date("d.m.Y", time()):'');
                            $Form->SchreibeHTMLCode('</div>');
                        }

                        break;
                    case 'TXA':
                        $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK] . ': ', $rsDetail->FeldInhalt('BIT_LABELBREITE'));
                        $Form->Erstelle_Textarea(($rsDetail->FeldInhalt('BIT_PFLICHT') == 1?'!':'') . $rsDetail->FeldInhalt('BIT_INFORMATION'), $Wert, 250, 56, 2, true);
                        break;
                    case 'AJX':

                        //Wenn in der Datenquelle "Auswahlbox" steht, ist es ein Aufwahlboxfeld, worauf ein späteres Feld reagiert
                        if (substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 0, 10) == 'Auswahlbox') {
                            $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK] . ': ', $rsDetail->FeldInhalt('BIT_LABELBREITE'));
                            $BIT_KEY = $rsDetail->FeldInhalt('BIT_KEY');

                            $Box = substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 11, 4);
                            $Form->AuswahlBox('!' . $rsDetail->FeldInhalt('BIT_INFORMATION'), $Box, $BIT_KEY, substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 16), '', 50, 10,
                                isset($_POST['suc' . $rsDetail->FeldInhalt('BIT_INFORMATION')])?$_POST['suc' . $rsDetail->FeldInhalt('BIT_INFORMATION')]:$Wert, 'T', $FeldAendern,
                                '', '', '', 50, '', '', '', 0, '');

                            if (isset($_REQUEST['suc' . $rsDetail->FeldInhalt('BIT_INFORMATION')])) {
                                $AJXWert = $_REQUEST['suc' . $rsDetail->FeldInhalt('BIT_INFORMATION')];
                            } elseif (isset($_REQUEST['txt' . $rsDetail->FeldInhalt('BIT_INFORMATION')])) {
                                $AJXWert = $_REQUEST['txt' . $rsDetail->FeldInhalt('BIT_INFORMATION')];
                            } else {
                                $AJXWert = 0;
                            }
                            $Form->ZeileEnde();
                            erstelleAjax($rsDetail->FeldInhalt('BIT_KEY'), $AJXWert, $Box);
                            $Form->ZeileStart();
                        }
                        break;
                    case 'LBL':
                        $Form->Trennzeile('O');
                        $Form->ZeileEnde();
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK], $rsDetail->FeldInhalt('BIT_LABELBREITE'), 'font-weight:bolder');
                        $Form->ZeileEnde();
                        $Form->ZeileStart();
                        $Form->Trennzeile('O');
                        break;
                    case 'BPK':

                        $SQLPruefBIF = BaueEmailPruefBIFSQL($rsBewerbungen->FeldInhalt('BWH_KEY'));

                        $Replace = '';

                        //Schauen welchen BIT_KEY ich zum ersetzen Brauche.
                        $SQLBIT_KEY = 'SELECT';
                        $SQLBIT_KEY .= '   *';
                        $SQLBIT_KEY .= ' FROM';
                        $SQLBIT_KEY .= '   bewerberinfostypen';
                        $SQLBIT_KEY .= ' WHERE';
                        $SQLBIT_KEY .= '   BIT_STATUS        = \'A\'';
                        $SQLBIT_KEY .= ' and BIT_HRF_KEY' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('HRF_KEY'));
                        $SQLBIT_KEY .= ' AND BIT_INFORMATION =';
                        $SQLBIT_KEY .= '   CASE';
                        $SQLBIT_KEY .= '     WHEN bit_hrf_key = 1';
                        $SQLBIT_KEY .= '     THEN \'Filialnummer\'';
                        $SQLBIT_KEY .= '     ELSE \'Abteilung\'';
                        $SQLBIT_KEY .= '   END';

                        $rsBITKey = $DB->RecordSetOeffnen($SQLBIT_KEY);

                        $SQLReplace = baueBIFWertSQL($rsBITKey->FeldInhalt('BIT_KEY'), $rsBewerbungen->FeldInhalt('BWH_KEY'));
                        $rsReplace = $DB->RecordSetOeffnen($SQLReplace);

                        $Replace = $rsReplace->FeldInhalt('BIF_WERT');
                        //var_dump ($DB->LetzterSQL());
                        $rsPruefBif = $DB->RecordSetOeffnen($SQLPruefBIF);
                        $Insert = false;

                        //Neuer Verteilerlistendatensatz
                        if (isset($_POST['ico_add_BEW_BPK_SONST_x']) or isset($_POST['cmdSpeichern_x'])) {
                            if (isset($_POST['txtBEW_BPK_SONST']) and zerlegeUndCheckEmails($_POST['txtBEW_BPK_SONST'])) {

                                $SQLPersonensuche = BauePersonenkreisSQL('', $Replace);

                                $rsPersonensuche = $DB->RecordSetOeffnen($SQLPersonensuche);

                                $DS = $rsPruefBif->FeldInhalt('DS') + 1;
                                $Unbekannt = true;
                                //Schauen ob man die Email einer Person aus der Personenkreistabelle zuordnen kann.
                                while (!$rsPersonensuche->EOF()) {
                                    if (strpos($rsPersonensuche->FeldInhalt('EMAIL'), $_POST['txtBEW_BPK_SONST']) !== false) {

                                        insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 28, $DS . '~' . $rsPersonensuche->FeldInhalt('TYP'));
                                        insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 26, $DS . '~' . $rsPersonensuche->FeldInhalt('VORNAME'));
                                        insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 27, $DS . '~' . $rsPersonensuche->FeldInhalt('NACHNAME'));
                                        insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 29, $DS . '~' . $_POST['txtBEW_BPK_SONST']);
                                        $Unbekannt = false;
                                        $rsPersonensuche->DSLetzter(); //Weitersuchen macht kein sinn, weil schon gefunden..
                                    }
                                    $rsPersonensuche->DSWeiter();
                                }
                                if ($Unbekannt) {
                                    insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 28, $DS . '~MANUELL' . $rsPersonensuche->FeldInhalt('TYP'));
                                    insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 26, $DS . '~ ');
                                    insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 27, $DS . '~' . $DS);
                                    insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 29, $DS . '~' . $_POST['txtBEW_BPK_SONST']);
                                }
                            } else {
                                if ($_POST['txtBEW_BPK_SONST'] != '') {
                                    $Form->ZeileStart();
                                    $Form->Hinweistext("Fehler in der Emailadresse.");
                                    $Form->ZeileEnde();
                                    $Fehleremail = true;
                                }
                            }
                        }

                        if ($Form->NameInArray($_POST, 'ico_delete', 0, 1) != '') {
                            $ScrollPosition = 'Verteiler';
                            $Name = $Form->NameInArray($_POST, 'ico_delete', 0, 1);
                            $Name = str_replace('ico_delete_', '', $Name);
                            $Name = str_replace('_x', '', $Name);
                            $Name = str_replace('_y', '', $Name);

                            deleteEmailBIFs($Name, $rsBewerbungen->FeldInhalt('BWH_KEY'));
                        }

                        if ($rsPruefBif->AnzahlDatensaetze() == 0 and !isset($_POST['ico_add_BEW_BPK_SONST_x'])) //Status vorstellungsgespräch gab es schonmal
                        {

                            $SQL = BauePersonenkreisSQL('BPK_AKT_VERTEILER', $Replace, false, $rsDetail->FeldInhalt('BIT_KEY'));

                            $rsbla = $DB->RecordSetOeffnen($SQL);

                            $DS = 0;
                            while (!$rsbla->EOF()) {
                                insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 28, $DS . '~' . $rsbla->FeldInhalt('TYP'));
                                insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 26, $DS . '~' . $rsbla->FeldInhalt('VORNAME'));
                                insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 27, $DS . '~' . $rsbla->FeldInhalt('NACHNAME'));
                                insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 29, $DS . '~' . $rsbla->FeldInhalt('EMAIL'));
                                $rsbla->DSWeiter();
                                $DS++;
                            }
                            $DS++;
                            $SQLKopf = 'Select * from HRKOPF where HRK_KEY' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('HRK_KEY'));
                            $rsKopf = $DB->RecordSetOeffnen($SQLKopf);
                            insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 28, $DS . '~Bewerber');
                            insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 26, $DS . '~' . $rsKopf->FeldInhalt('HRK_VORNAME'));
                            insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 27, $DS . '~' . $rsKopf->FeldInhalt('HRK_NACHNAME'));
                            insertBIF($rsBewerbungen->FeldInhalt('BWH_KEY'), 29, $DS . '~' . $rsKopf->FeldInhalt('HRK_EMAIL'));
                        }

                        $SQL = 'Select ';
                        $SQLUnion = '';
                        $rsPruefBif = $DB->RecordSetOeffnen($SQLPruefBIF);

                        if ($rsPruefBif->AnzahlDatensaetze() > 0) {
                            while (!$rsPruefBif->EOF()) {
                                if ($SQLUnion != '') {
                                    $SQLUnion .= ' from dual UNION ALL Select ';
                                }
                                $SQLUnion .= '(select bif_wert from V_BEWERBERVERTEILER v
                                where  V.BIF_BIT_ID = 28 and V.DS' . $DB->LikeOderIst($rsPruefBif->FeldInhalt('DS')) . ' and v.BIF_BWH_KEY' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('BWH_KEY')) . ') as typ, ';

                                $SQLUnion .= '(select bif_wert from V_BEWERBERVERTEILER v
                                where  V.BIF_BIT_ID = 26 and V.DS' . $DB->LikeOderIst($rsPruefBif->FeldInhalt('DS')) . ' and v.BIF_BWH_KEY' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('BWH_KEY')) . ') as VORNAME, ';

                                $SQLUnion .= '(select bif_wert from V_BEWERBERVERTEILER v
                                where  V.BIF_BIT_ID = 27 and V.DS' . $DB->LikeOderIst($rsPruefBif->FeldInhalt('DS')) . ' and v.BIF_BWH_KEY' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('BWH_KEY')) . ') as NACHNAME, ';

                                $SQLUnion .= '(select bif_wert from V_BEWERBERVERTEILER v
                                where  V.BIF_BIT_ID = 29 and V.DS' . $DB->LikeOderIst($rsPruefBif->FeldInhalt('DS')) . ' and v.BIF_BWH_KEY' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('BWH_KEY')) . ') as EMAIL ';

                                $rsPruefBif->DSWeiter();
                            }
                        } else {
                            $SQLUnion = '*';
                        }

                        $SQL .= $SQLUnion . ' from dual';

                        $rsbla = $DB->RecordSetOeffnen($SQL);

                        $FuerLeftJoin = $SQL;

                        $Form->Erstelle_Liste_Ueberschrift('', 20, '', '');
                        //$Link = './taetigkeiten_Main.php?cmdAktion=Stammdaten&HRT_KEY='.$AWIS_KEY1.'&Sort2=FIRM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FIRM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
                        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_TEILNEHMERKREIS'], 570, '', '');
                        $Form->ZeileEnde();

                        $DS = 0;

                        if ($rsPruefBif->AnzahlDatensaetze() > 0) //Nur wenn Bifs da sind..
                        {
                            while (!$rsbla->EOF()) {
                                $Form->ZeileStart();
                                $Icons = array();
                                //$Icons[] = array('delete','"./taetigkeiten_Main.php?cmdAktion=Stammdaten&Del='.$rsZuord->FeldInhalt('HTZ_KEY').'&HRT_KEY='.$AWIS_KEY1.'" onmouseover="urlanpassen(this);"');
                                if ($rsbla->FeldInhalt('TYP') != 'Bewerber' and $rsbla->AnzahlDatensaetze() > 2 and (($Recht39001 & 2) == 2)) {
                                    $Icons[] = array('delete', 'POST~' . $rsbla->FeldInhalt('NACHNAME'));
                                    $Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2));
                                } else {
                                    $Form->Erstelle_ListenFeld('leer', '', 20, 20);
                                    //$Form->Erstelle_ListeIcons('',20,($DS%2));
                                }
                                $TTT = '';
                                // var_dump($rsbla);
                                $Form->Erstelle_ListenFeld('BEW_EMAILS', $rsbla->FeldInhalt('EMAIL') . ' (' . $rsbla->FeldInhalt('TYP') . ') ', 0, 570, false, ($DS % 2), '', '',
                                    'T', 'L', $TTT);
                                $Form->ZeileEnde();
                                $DS++;
                                $rsbla->DSWeiter();
                            }
                        }

                        $SQLCMB = BauePersonenkreisSQL('BPK_AKT_VERTEILERCMB', $Replace, true, $rsDetail->FeldInhalt('BIT_KEY'));

                        $SQLCMB .= 'a LEFT JOIN (';
                        $SQLCMB .= $FuerLeftJoin;
                        $SQLCMB .= ')b  on a.VORNAME = B.VORNAME and a.NACHNAME = B.NACHNAME
                                         where b.nachname is null';

                        $SQL = 'Select Email, Email, Typ from (' . $SQLCMB . ' )';

                        if ($DB->ErmittleZeilenAnzahl($SQL) > 0 and (($Recht39001 & 2) == 2)) {
                            $Form->ZeileStart();
                            $Form->Erstelle_TextLabel('', 24);
                            $Form->Erstelle_SelectFeld('BEW_BPK', '', '570:575', true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '',
                                'onchange="emailUebernehmen()"', '', array(), 'font-size:14px;height:20px');
                            $Form->ZeileEnde();
                        }

                        if ((($Recht39001 & 2) == 2)) {
                            $Form->ZeileStart();
                            $Icons = array();
                            $Icons[] = array('add', 'POST~' . 'BEW_BPK_SONST');
                            $Form->Erstelle_ListeIcons($Icons, 20, ($DS));

                            $Form->Erstelle_TextFeld('BEW_BPK_SONST', isset($Fehleremail)?$_POST['txtBEW_BPK_SONST']:'', 91, 575, true);
                            $Form->ZeileEnde();
                        }
                        $Form->ZeileStart();

                        break;
                    case 'TAT':
                        $Liste = substr($rsDetail->FeldInhalt('BIT_DATENQUELLE'), 4);
                        $Bereich = substr($Liste, 0, strpos($Liste, ':'));
                        $Kennung = substr($Liste, strpos($Liste, ':') + 1);
                        $Liste = explode("|", $Konserven[$Bereich][$Kennung]);
                        //Label
                        $Form->Erstelle_TextLabel($Konserven[$TKB][$TKK] . ': ', $rsDetail->FeldInhalt('BIT_LABELBREITE'));

                        $ArtFest = true;
                        if ($rsBewerbungen->FeldInhalt('BWH_KEY') != null) {
                            $SQL = "SELECT BIT_KEY FROM BEWERBERINFOSTYPEN WHERE BIT_INFORMATION = 'Taetigkeitsart'";
                            $rsTemp = $DB->RecordSetOeffnen($SQL);
                            $SQL = baueBIFWertSQL($rsTemp->FeldInhalt('BIT_KEY'), $rsBewerbungen->FeldInhalt('BWH_KEY'));
                            $rsTemp = $DB->RecordSetOeffnen($SQL);
                            $Wert = ($rsTemp->FeldInhalt('BIF_WERT') == "")?0:$rsTemp->FeldInhalt('BIF_WERT');
                            $ArtFest = false;
                        } else {
                            $Wert = "";
                        }
                        $Form->Erstelle_SelectFeld('Taetigkeitsart', $Wert, $rsDetail->FeldInhalt('BIT_FELDBREITE'), $ArtFest, '', '', '', '', '', $Liste,
                            'onchange="AzubFeldUebertrag();key_BEW_TAETIGKEIT(event)"');
                        $Form->ZeileEnde();
                        $Form->ZeileStart();
                        if ($FeldAendern) {
                            echo "<div style='display:none'>";
                            $Form->Erstelle_HiddenFeld('aendernrecht', $FeldAendern);
                            $Form->Erstelle_HiddenFeld('labelbreite', $rsDetail->FeldInhalt('BIT_LABELBREITE'));
                            $Form->AuswahlBox('BEW_TAETIGKEIT', 'BOX_Taetigkeiten', '', 'BEW_TAETIGKEITEN', 'txtBEW_FIRM,txtaendernrecht,txtlabelbreite', 25, 10, '', 'T', true, '',
                                '', '', 50, '', '', '', 0, '');
                            echo "</div>";

                            ob_start();
                            erstelleAjaxTaetigkeiten(0, $HRF_KEY, $FeldAendern, $rsDetail->FeldInhalt('BIT_LABELBREITE'));
                            $Inhalt = ob_get_contents();
                            ob_end_clean();

                            $Form->ZeileEnde();
                            $Form->ZeileStart();
                            $Form->AuswahlBoxHuelle('BOX_Taetigkeiten', 'AuswahlListe', '', $Inhalt);
                        } else {
                            $Form->ZeileEnde();
                            $SQL = baueBIFWertSQL($rsDetail->FeldInhalt('BIT_KEY'), $rsBewerbungen->FeldInhalt('BWH_KEY'));
                            $rsTaet = $DB->RecordSetOeffnen($SQL);
                            while (!$rsTaet->EOF()) {
                                $SQL = "SELECT HRT_SUCH FROM HRTAETIGKEITEN WHERE HRT_KEY = " . $DB->WertSetzen('TAT', 'T', $rsTaet->FeldInhalt('BIF_WERT'));
                                $rsTemp = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('TAT', true));
                                $Form->ZeileStart();
                                $Form->Erstelle_TextLabel('', $rsDetail->FeldInhalt('BIT_LABELBREITE'));
                                $Form->Erstelle_TextFeld('name', $rsTemp->FeldInhalt('HRT_SUCH'), 100, $rsDetail->FeldInhalt('BIT_FELDBREITE'), false);
                                $Form->ZeileEnde();
                                $rsTaet->DSWeiter();
                            }
                            $Form->ZeileStart();
                        }
                        break;
                    default:
                        break;
                }
            }
            $rsDetail->DSWeiter();
            if ($rsDetail->EOF()) {
                $Form->ZeileEnde();
            }
        }

        if ($rsBewerbungen->FeldInhalt('BWS_STEUER_ID') >= 20) //Wenn der Datensatz auf Offen steht, der Bewerberprozess aber schon gestartet wurde, also alle Daten eingegeben, Eingangsbestätigung verschickt.
        {

            $Form->ZeileStart();
            $Form->Trennzeile('L');
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Trennzeile('O');
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($Konserven['BEW']['BEW_STATUS'], 250);
            $SQLStat = 'Select BWS_KEY,  FUNC_TEXTKONSERVE(
                        SUBSTR(BWS_TEXTKONSERVE,instr(BWS_TEXTKONSERVE,\':\')+1),
                        SUBSTR(BWS_TEXTKONSERVE,0,instr(BWS_TEXTKONSERVE,\':\')-1)
                        , \'' . $AWISBenutzer->BenutzerSprache() . '\') as BWS_TEXT from bewerberstatus
                        where BWS_AKTIV = \'A\' and BWS_STEUER_ID >= 21 order by 2';
            $Form->Erstelle_SelectFeld('BWS_KEY',
                isset($_POST['txtBWS_KEY']) && $_POST['txtBWS_KEY'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']?$_POST['txtBWS_KEY']:$rsBewerbungen->FeldInhalt('BWS_KEY'),
                250, $EditRecht, $SQLStat, $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', 'onchange="postFirm();"');
            echo '<a name="Status" />';
            $Form->ZeileEnde();
            $Form->SetzeCursor($AWISCursorPosition);
            if (($Recht39001 & 32) > 0) {
                $Frame1 = isset($_GET['Dokumente'])?true:false;
                $Form->FormularBereichStart();
                $Form->FormularBereichInhaltStart($AWISSprachKonserven['BEW']['BEW_DOKUMENTE'], $Frame1);
                include("bewerberverwaltung_Details_Bewerbungen_Dokumente.php");
                $Form->FormularBereichInhaltEnde();
                $Form->FormularBereichEnde();
            }
        } else {
            $Form->Erstelle_HiddenFeld('BWS_KEY', 8);
        }
    } catch (awisException $ex) {
        if ($Form instanceof awisFormular) {
            $Form->DebugAusgabe(1, $ex->getSQL());
            $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
        } else {
            echo 'AWIS-Fehler:' . $ex->getMessage();
        }
    } catch (Exception $ex) {
        if ($Form instanceof awisFormular) {

            $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
        } else {
            echo 'allg. Fehler:' . $ex->getMessage();
        }
    }
}

function erstelleAjax($BIT_KEY = 0, $Param = '', $Box = '')
{
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    try {

        $DS = 0;
        ob_start();
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();

        $AWISBenutzer = awisBenutzer::Init();
        $Form = new awisFormular();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('BEW', '%');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_suche');
        $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'wrd_Filiale');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Liste', 'lst_OffenMass');
        $TextKonserven[] = array('Wort', 'PDFErzeugen');
        $TextKonserven[] = array('Wort', 'DatumVom');
        $TextKonserven[] = array('Wort', 'Datum');
        $TextKonserven[] = array('FIL', 'FIL_GEBIET');
        $TextKonserven[] = array('Liste', 'lst_ALLE_0');
        $TextKonserven[] = array('Liste', 'lst_JaNein');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');

        $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

        $SQLAJAX = 'select';
        $SQLAJAX .= ' a.BIT_KEY ,';
        $SQLAJAX .= ' a.BIT_INFORMATION ,';
        $SQLAJAX .= ' a.BIT_BEMERKUNG ,';
        $SQLAJAX .= ' a.BIT_HRF_KEY ,';
        $SQLAJAX .= ' a.BIT_XRC_ID_ANZEIGE ,';
        $SQLAJAX .= ' a.BIT_RECHTEBIT_ANZEIGE ,';
        $SQLAJAX .= ' a.BIT_STATUS ,';
        $SQLAJAX .= ' SUBSTR(bit_textkonserve,0,instr(bit_textkonserve,\':\')-1) AS  TXTBEREICH,
                   SUBSTR(bit_textkonserve,instr(bit_textkonserve,\':\')+1) AS TXTKENNUNG ,';
        $SQLAJAX .= ' a.BIT_DATENQUELLE ,';
        $SQLAJAX .= ' a.BIT_FORMAT ,';
        $SQLAJAX .= ' a.BIT_FELDTYP ,';
        $SQLAJAX .= ' a.BIT_LABELBREITE ,';
        $SQLAJAX .= ' a.BIT_FELDBREITE ,';
        $SQLAJAX .= ' a.BIT_FELDLAENGE ,';
        $SQLAJAX .= ' a.BIT_PFLICHT ,';
        $SQLAJAX .= ' a.BIT_USER ,';
        $SQLAJAX .= ' a.BIT_USERDAT ,';
        $SQLAJAX .= ' a.BIT_SICHTBAR ,';
        $SQLAJAX .= ' a.BIT_RECHTEBIT_BEARBEITEN ,';
        $SQLAJAX .= ' a.BIT_ZUORDKUERZEL ,';
        $SQLAJAX .= ' a.BIT_ZUORDNUNG,';
        $SQLAJAX .= ' a.BIT_SORTIERUNG, a.BIT_LETZTEZEILE';
        $SQLAJAX .= ' from';
        $SQLAJAX .= '   BEWERBERINFOSTYPEN a';
        $SQLAJAX .= ' where';
        $SQLAJAX .= '   a.BIT_STATUS     = \'A\'';
        $SQLAJAX .= ' and a.BIT_SICHTBAR = 1';
        $SQLAJAX .= ' and ( a.BIT_ZUORDKUERZEL = \'BIT\' and
        a.BIT_ZUORDNUNG ' . $DB->LikeOderIst($BIT_KEY);

        $SQLAJAX .= ') order by a.BIT_SORTIERUNG asc';
        $Form->DebugAusgabe(1, 'SQL $SQLAJAX: ' . $SQLAJAX);
        $rsAJAX = $DB->RecordSetOeffnen($SQLAJAX);

        //Sonderbehandlung für den Infotypen 1(Filiale)
        if ($BIT_KEY == 1 and !ctype_digit($Param) and $Param != '') {

            $SQLFil = ' select distinct FIL_ID, FILIALEN.FIL_BEZ from V_FILIALEBENENROLLEN_AKTUELL
                    inner join filialen
                    on FILIALEN.FIL_ID = V_FILIALEBENENROLLEN_AKTUELL.XX1_FIL_ID where upper(FIL_BEZ) ' . $DB->LikeOderIst('%' . $Param . '%', awisDatenbank::AWIS_LIKE_UPPER);
            $SQLFil .= ' order by FIL_BEZ asc';
            $rsFil = $DB->RecordSetOeffnen($SQLFil);
            $Param = $rsFil->FeldInhalt('FIL_ID') > 0?$rsFil->FeldInhalt('FIL_ID'):0;
            $Form->Erstelle_HiddenFeld('Filialnummer', str_pad($Param, 4, '0', STR_PAD_LEFT));
        } elseif ($BIT_KEY == 1 and ctype_digit($Param) and $Param != '') //Filialnummer muss 4 Stellig sein
        {
            $Form->Erstelle_HiddenFeld('Filialnummer', str_pad($Param, 4, '0', STR_PAD_LEFT));
        }

        $ErstesFeld = true; //Für die Steuerung, ob ein ZeileEnde benötigt wird. Bei der ErstenZeile wird keins benötigt.
        while (!$rsAJAX->EOF()) {
            $Fehler = '';
            //Letzte Zeile abschließen wenn erforderlich.
            if (!$ErstesFeld and $rsAJAX->FeldInhalt('BIT_LETZTEZEILE') == 1) {
                $Form->ZeileEnde();
                $Form->ZeileStart();
            } elseif ($ErstesFeld) {
                $Form->ZeileStart();
                $ErstesFeld = false;
            }

            //Grundwerte holen.
            $TKB = $rsAJAX->FeldInhalt('TXTBEREICH');//Textkonservenbereich
            $TKK = $rsAJAX->FeldInhalt('TXTKENNUNG');//Textkonservenkennung
            $FeldAendern = false;

            //schau ob es schon einen Wert gibt.
            $SQLBIF = 'Select BIF_WERT from BEWERBERINFOS ';
            $SQLBIF .= ' where BIF_BIT_ID ' . $DB->LikeOderIst($rsAJAX->FeldInhalt('BIT_KEY'));
            $SQLBIF .= ' and BIF_BWH_KEY ' . $DB->LikeOderIst($AWIS_KEY2);

            $rsBIF = $DB->RecordSetOeffnen($SQLBIF);

            $Wert = $rsBIF->FeldInhalt('BIF_WERT');

            if ($Wert == '')//Wenn noch kein Wert in der DB steht, dann laden
            {
                if (substr($rsAJAX->FeldInhalt('BIT_DATENQUELLE'), 0, 3) == 'SQL') {
                    $DatenquelleSQL = substr($rsAJAX->FeldInhalt('BIT_DATENQUELLE'), 4);

                    if ($Param == '') {
                        $Fehler = 'Es konnten keine Daten gefunden werden!';
                    } else {
                        $DatenquelleSQL = str_replace('#FIL_ID#', $Param, $DatenquelleSQL);

                        $rsDatenquelle = $DB->RecordSetOeffnen($DatenquelleSQL);

                        $Wert = $rsDatenquelle->FeldInhalt('EINS');
                        if ($Wert == '') {
                            $Fehler = 'Es konnten keine Daten gefunden werden!';
                        }
                    }
                } elseif (substr($rsAJAX->FeldInhalt('BIT_DATENQUELLE'), 0, 3) == 'BPK') {
                    $Form->Erstelle_TextLabel($AWISSprachKonserven[$TKB][$TKK] . ':', $rsAJAX->FeldInhalt('BIT_LABELBREITE'));

                    $SQLCMB = BauePersonenkreisSQL('BPK_AKT_ESKGESENDETAN', ($Param == ''?0:$Param), false, 25);

                    $SQLCMB = 'Select Email, Email || \' &rarr; \'||Typ from (' . $SQLCMB . ' )';

                    $rsTTT = $DB->RecordSetOeffnen($SQLCMB);

                    if ($DB->ErmittleZeilenAnzahl($SQLCMB) > 0) {
                        $Form->Erstelle_SelectFeld('~' . (($rsAJAX->FeldInhalt('BIT_PFLICHT') == 1?'!':'') . $rsAJAX->FeldInhalt('BIT_INFORMATION')),
                            isset($Fehleremail)?$_POST['txt' . $rsAJAX->FeldInhalt('BIT_INFORMATION')]:$rsTTT->FeldInhalt('EMAIL'), 300, true, $SQLCMB, '', '', '', '', '', '', $rsTTT->FeldInhalt(2), array(), 'font-size:14px;height:20px');
                    }
                }
            }
            if ($Wert != '' or ($Fehler and $Param != 0)) {
                $Form->Erstelle_TextLabel($AWISSprachKonserven[$TKB][$TKK] . ':', $rsAJAX->FeldInhalt('BIT_LABELBREITE'));
                $Form->Erstelle_TextFeld($rsAJAX->FeldInhalt('BIT_INFORMATION'), $Fehler == ''?$Wert:$Fehler, $rsAJAX->FeldInhalt('BIT_FELDLAENGE'),
                    $rsAJAX->FeldInhalt('BIT_FELDBREITE'), false, '', '', '', 'T', 'L', '', '', 50);
                $Form->Erstelle_HiddenFeld($rsAJAX->FeldInhalt('BIT_INFORMATION'), $Wert);
            }
            $rsAJAX->DSWeiter();
        }
        $Form->ZeileEnde();
        $Inhalt = ob_get_contents();
        ob_end_clean();
        $Form->AuswahlBoxHuelle($Box, 'AuswahlListe', '', $Inhalt);
    } catch (awisException $ex) {
        if ($Form instanceof awisFormular) {
            $Form->DebugAusgabe(1, $ex->getSQL());
            $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";

            echo $DB->LetzterSQL();
        } else {
            echo 'AWIS-Fehler:' . $ex->getMessage();
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";

            echo $DB->LetzterSQL();
        }
    } catch (Exception $ex) {
        if ($Form instanceof awisFormular) {

            $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");

            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";

            echo $DB->LetzterSQL();
        } else {
            echo 'allg. Fehler:' . $ex->getMessage();
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";
            echo "<br>";

            echo $DB->LetzterSQL();
        }
    }
}

function post2hidden()
{
    //Ballert alle POSTs (außer old) in Hiddenfelder.
    $Form = new awisFormular();
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) != 'old') {
            if (is_array($value)) {
                foreach ($value as $item) {
                    $Form->Erstelle_HiddenFeld(substr($key, 3) . '[]', $item);
                }
            } else {
                $Form->Erstelle_HiddenFeld(substr($key, 3), $value);
            }
        }
    }
}

function insertBIF($BWH_KEY, $BIT_ID, $BIF_WERT)
{
    global $AWISBenutzer;
    global $DB;
    $InsertBIF = 'INSERT';
    $InsertBIF .= ' INTO';
    $InsertBIF .= '   bewerberinfos';
    $InsertBIF .= '   (';
    $InsertBIF .= '     BIF_BIT_ID,';
    $InsertBIF .= '     BIF_BWH_KEY,';
    $InsertBIF .= '     BIF_IMQ_ID,';
    $InsertBIF .= '     BIF_USER,';
    $InsertBIF .= '     BIF_USERDAT,';
    $InsertBIF .= '     BIF_WERT';
    $InsertBIF .= '   )';
    $InsertBIF .= '   VALUES';
    $InsertBIF .= '   (';
    $InsertBIF .= '    ' . $BIT_ID . ',';
    $InsertBIF .= $BWH_KEY . ', ';
    $InsertBIF .= '     4,';
    $InsertBIF .= '\'' . $AWISBenutzer->BenutzerName() . '\', ';
    $InsertBIF .= '     sysdate,';
    $InsertBIF .= '     \'' . $BIF_WERT . '\'';
    $InsertBIF .= '   )';

    $DB->Ausfuehren($InsertBIF);
}

function deleteEmailBIFs($Nachname, $BWH_KEY)
{
    global $DB;
    //Leider aufgrund der Freitexteingabe + Infotypenkonstrukt nur über diesen krassen Umweg machbar
    $SQLPruefBIF = 'Select DS from V_BEWERBERVERTEILER
                                        where bif_bwh_key' . $DB->LikeOderIst($BWH_KEY);
    $SQLPruefBIF .= ' and BIF_BIT_ID = 27 and BIF_WERT ' . $DB->LikeOderIst($Nachname);;

    $rsDelDS = $DB->RecordSetOeffnen($SQLPruefBIF);

    $Delete = 'Delete from bewerberinfos where bif_bwh_key' . $DB->LikeOderIst($BWH_KEY);
    $Delete .= ' and (substr(BIF_WERT,0,instr(BIF_WERT,\'~\')-1))' . $DB->LikeOderIst($rsDelDS->FeldInhalt('DS'));
    $Delete .= ' ANd bif_bit_id IN (28,26,27,29) ';

    $DB->Ausfuehren($Delete);
}

function BaueEmailPruefBIFSQL($BWH_KEY)
{
    global $DB;
    $SQLPruefBIF = 'Select distinct DS from V_BEWERBERVERTEILER
                                        where bif_bwh_key' . $DB->LikeOderIst($BWH_KEY);
    $SQLPruefBIF .= ' order by 1 desc';

    //echo $SQLPruefBIF;
    return $SQLPruefBIF;
}

function baueBIFWertSQL($BIT_KEY, $BWH_KEY)
{
    global $DB;
    $SQLBIF = 'Select BIF_WERT from BEWERBERINFOS ';
    $SQLBIF .= ' where BIF_BIT_ID ' . $DB->LikeOderIst($BIT_KEY);
    $SQLBIF .= ' and BIF_BWH_KEY ' . $DB->LikeOderIst($BWH_KEY);

    return $SQLBIF;
}

/**
 * Holt den aktuellen Status des Datensatzes
 */
function holeAktuellenStatus()
{
    global $DB;
    global $AWIS_KEY2;

    $SQLStatus = 'Select distinct BWS_KEY from V_BEWERBERVERWALTUNG
                                        where bwh_key' . $DB->LikeOderIst($AWIS_KEY2);

    $rsStatus = $DB->RecordSetOeffnen($SQLStatus);

    return $rsStatus->FeldInhalt('BWS_KEY');
}

function BauePersonenkreisSQL($Akt = '', $Replace = '', $LeftJoin = false, $BIT_KEY = '')
{
    global $DB;

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $SQLPersonenkreis = 'SELECT';
    $SQLPersonenkreis .= '   BPK_KEY ,';
    $SQLPersonenkreis .= '   BPK_BEZEICHNUNG ,';
    $SQLPersonenkreis .= '   BPK_ZUORD ,';
    $SQLPersonenkreis .= '   BPK_ZUORD_KEY ,';
    $SQLPersonenkreis .= '   BPK_BWS_KEY ,';
    $SQLPersonenkreis .= '   BPK_DATENQUELLE ,';
    $SQLPersonenkreis .= '   BPK_AKTIV ,';
    $SQLPersonenkreis .= '   BPK_AKT_VERTEILER ,';
    $SQLPersonenkreis .= '   BPK_AKT_VERTEILERCMB';
    $SQLPersonenkreis .= ' FROM';
    $SQLPersonenkreis .= '   BEWERBERPERSONENKREIS';
    $SQLPersonenkreis .= ' WHERE';
    $SQLPersonenkreis .= '   BPK_AKTIV   = 1';

    if ($Akt != '') {
        $SQLPersonenkreis .= ' and ' . $Akt . $DB->LikeOderIst('1');
    }

    $SQLPersonenkreis .= ' AND BPK_ZUORD = \'BIT\'';

    if ($BIT_KEY != '') {
        $SQLPersonenkreis .= ' and BPK_ZUORD_KEY' . $DB->LikeOderIst($BIT_KEY);
    }

    $rsPersonenkreis = $DB->RecordSetOeffnen($SQLPersonenkreis);

    if ($LeftJoin) {
        $SQL = 'Select a.typ, a.vorname, a.nachname, a.email from (';
    } else {
        $SQL = 'Select * from (';
    }

    $SQLUnion = '';
    //Kompletten SQL zusammenbauen
    while (!$rsPersonenkreis->EOF()) {
        if ($rsPersonenkreis->FeldInhalt('BPK_DATENQUELLE') != '') {
            $DQ = $rsPersonenkreis->FeldInhalt('BPK_DATENQUELLE');
            $DQ = str_replace('#FIL_ID#', $Replace, $DQ);
            $DQ = str_replace('#KAB_KEY#', $Replace, $DQ);

            if ($SQLUnion != '') {
                $SQLUnion .= ' UNION ALL ';
            }
            $SQLUnion .= $DQ;
        }
        $rsPersonenkreis->DSWeiter();
    }

    if ($SQLUnion != '') {
        $SQL .= $SQLUnion . ')';

        return $SQL;
    }
}

function zerlegeUndCheckEmails($Email)
{
    global $DB;
    global $AWISBenutzer;

    $Return = true;

    $Mail = new awisMailer($DB, $AWISBenutzer);

    if (strpos($Email, ';') !== false) //Mehrere Emails
    {
        $Emails = explode(';', $Email);
        foreach ($Emails as $Email) {
            $Email = trim($Email);
            if ($Email != '' and !$Mail->checkEmail($Email, $Mail::PRUEFE_LOGIK)) {
                $Return = false;
            }
        }
    } else //Nur eine Email
    {
        $Email = trim($Email);
        if (!$Mail->checkEmail($Email, $Mail::PRUEFE_LOGIK)) {
            $Return = false;
        }
    }

    return $Return;
}

/**
 * Schaut ob einer der verschiedenen StatusWeiter gesetzt ist
 *
 * @return boolean
 */
function issetCMDStatusWeiter()
{
    if (isset($_POST['cmdStatusWeiterM']) or isset($_POST['cmdStatusWeiterO']) or isset($_POST['cmdStatusWeiterS'])) {
        return true;
    } else {
        return false;
    }
}

/**
 *  Erstellt das Ergebnis des Ajax für die Tätigkeiten
 *
 * @param $Ausbild Boolean, ob Ausbildungsberufe gewollt
 */
function erstelleAjaxTaetigkeiten($Ausbild, $HRF_KEY, $FeldAendern, $Labelbreite)
{
    global $AWISCursorPosition;
    global $AWISBenutzer;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    try {
        $DS = 0;

        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();

        $AWISBenutzer = awisBenutzer::Init();
        $Form = new awisFormular();

        // Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('BEW', '%');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

        $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

        $SQL = "select hrt_key, a.HRT_SUCH from HRTAETIGKEITEN a inner join HRTAETZUORD b on B.HTZ_HRT_KEY = a.HRT_KEY  where a.HRT_AKTIV = 1 and B.HTZ_FIRMIERUNG in(#HRF_KEY#)";
        $SQL = str_replace('#HRF_KEY#', $HRF_KEY, $SQL);

        if (isset($Ausbild) and $Ausbild <> '') {
            $Ausbild = ($Ausbild == 0?0:1);
            $SQL .= " AND HRT_AUSBILD in (1, $Ausbild)";
        }

        $SQL .= " order by a.HRT_SUCH asc";

        //$Form->Erstelle_SelectFeld('Taetigkeit[]', '0', 250, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);

        $Form->Erstelle_MultiSelectFeld('Taetigkeiten', array(), 250, $SQL, $Labelbreite, $FeldAendern, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
    } catch (awisException $ex) {
        if ($Form instanceof awisFormular) {
            $Form->DebugAusgabe(1, $ex->getSQL());
            $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
            echo $DB->LetzterSQL();
        } else {
            echo 'AWIS-Fehler:' . $ex->getMessage();
            echo $DB->LetzterSQL();
        }
    } catch (Exception $ex) {
        if ($Form instanceof awisFormular) {

            $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
            echo $DB->LetzterSQL();
        } else {
            echo 'allg. Fehler:' . $ex->getMessage();
            echo $DB->LetzterSQL();
        }
    }
}
