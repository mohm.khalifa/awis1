<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['sucBES_GROSSKDNR_D']))
{
	var_dump($_GET);
	die();
}


try
{
	$DS=0;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','Datum');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$SQL = ' SELECT replace(CIN_WERT,\'.\',\'\') as CIN_WERT FROM CRMAKQUISESTAND INNER JOIN CRMADRESSEN ON CAD_KEY = CAQ_CAD_KEY';
	$SQL .= ' left join crminfos on cad_key = CIN_CAD_KEY and CIN_CIT_ID = 7101';
	$SQL .= ' WHERE CAQ_VERTRAGSNUMMER ' . $DB->LikeOderIst(($_GET['sucBES_GROSSKDNR_D']));

	$rsCRM = $DB->RecordSetOeffnen($SQL);

	if($rsCRM->AnzahlDatensaetze() == 0 and $_GET['sucBES_GROSSKDNR_D'] != '')
	{
		$Form->Erstelle_TextFeld('BES_AART','keine g�ltige Gro�-Kd-Nr', 40,250, false,'', '', '', 'T', 'L','','',50);
		$Form->Erstelle_HiddenFeld('BES_AART2','');
	}
	else if($_GET['sucBES_GROSSKDNR_D'] == '')
	{
		$SQL2 ='select ID,WERT from ANTRAGART where id not in (18,20) order by wert ';
		$Form->Erstelle_SelectFeld('!BES_AART','',250,true,$SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	}
	else
	{
		$SQL2 ='select ID,WERT from ANTRAGART where id in (18,20) order by wert ';
		$Form->Erstelle_SelectFeld('BES_AART2',$rsCRM->Feldinhalt('CIN_WERT')< 730 ? '20':'18',250,false,$SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_HiddenFeld('BES_AART',$rsCRM->Feldinhalt('CIN_WERT')< 730 ? '20':'18');
	}

}
catch (Exception $ex)
{
	$Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>