<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

global $AWIS_KEY1;
global $AWISCursorPosition;

try
{
$AWISBenutzer = awisBenutzer::Init();
$Form = new awisFormular();

// Textkonserven laden
$TextKonservenPDF = array();
$TextKonservenPDF[]=array('Ausdruck','txtUeberschriftRückführungsanforderung');
$TextKonservenPDF[]=array('Ausdruck','txtHinweisVertraulich');
$TextKonservenPDF[]=array('Wort','Artikelnummer');
$TextKonservenPDF[]=array('Wort','ARTIKELBEZEICHNUNG');
$TextKonservenPDF[]=array('Wort','Filialbestand');
$TextKonservenPDF[]=array('Wort','KeineDatenVorhanden');
$TextKonservenPDF[]=array('RFA','RFA_%');
$TextKonservenPDF[]=array('RFM','RFM_%');
$TextKonservenPDF[]=array('RFP','RFP_BEMERKUNG');

$AWISSprachKonservenPDF = $Form->LadeTexte($TextKonservenPDF,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Recht3910= $AWISBenutzer->HatDasRecht(3910);
if($Recht3910==0)
{
	$Form->Fehler_KeineRechte();
}

$Filiale=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
$Vorlagen = array('A4mitATULogo.pdf');

if(isset($_GET['RFA_KEY']))
{
	$AWIS_KEY1=$_GET['RFA_KEY'];
}

if(isset($_GET['PDF_LISTE']))
{
	$PFD_LISTE=$_GET['PDF_LISTE'];
}
else
{
	$PFD_LISTE=0;
}

if($PFD_LISTE==0)
{
	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfa_key']=intval('0'.$AWIS_KEY1);
	
	$SQL = 'SELECT RFS_STATUS FROM RUECKFUEHRUNGSANFORDERUNGEN JOIN RUECKFUEHRUNGSSTATUS ON (RFA_RFS_KEY=RFS_KEY)';
	$SQL.= ' WHERE RFA_KEY=:var_N0_rfa_key';
	
	//$Form->DebugAusgabe(1,$SQL);
	
	$rsRFAStatus = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
	$rsRFAStatusZeilen = $rsRFAStatus->AnzahlDatensaetze();
	
	//$Form->DebugAusgabe(1,$Filiale);
	
	if($rsRFAStatus->FeldInhalt('RFS_STATUS')>='19')
	{
		$Spalte = 10;
		$Zeile = 20;
		$Ausdruck = null;
		$Seite='';
		$GesMenge=0;
		$GesBestand=0;
	
		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,$Form->Format('TP',$AWISSprachKonservenPDF['Ausdruck']['txtUeberschriftRückführungsanforderung']));
	
		$Zeile=_Erzeuge_Seite_Rueckfuehrungsanforderung();
		
		$BindeVariablen=array();
		$BindeVariablen['var_N0_rfa_key']=intval($AWIS_KEY1);
		$BindeVariablen['var_T_land']=$AWISBenutzer->BenutzerSprache();
		
		$SQL='SELECT ANF.*, NVL(BST.BESTAND,0) AS BESTAND FROM ';
		//$SQL.='(SELECT RFM_FIL_ID, RFP_AST_ATUNR, RFP_BEMERKUNG, ASP_BEZEICHNUNG, RFM_MENGE ';
		$SQL.='(SELECT RFM_FIL_ID, RFP_AST_ATUNR, RFP_BEMERKUNG, FUNC_ASP_BEZEICHNUNG(RFP_AST_ATUNR,:var_T_land) AS ASP_BEZEICHNUNG, RFM_MENGE ';
		$SQL.='FROM RUECKFUEHRUNGSANFORDERUNGENPOS JOIN RUECKFUEHRUNGSANFPOSFILMENGEN ON (RFP_KEY = RFM_RFP_KEY) ';
//		$SQL.='LEFT JOIN (SELECT ASP_AST_ATUNR, ASP_BEZEICHNUNG FROM ARTIKELSPRACHEN WHERE ASP_LAN_CODE = \''.$AWISBenutzer->BenutzerSprache().'\' ';
//		$SQL.='AND ASP_AST_ATUNR IN (SELECT RFP_AST_ATUNR FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_RFA_KEY = '.$AWIS_KEY1.')) ';
//		$SQL.='ON (RFP_AST_ATUNR = ASP_AST_ATUNR) ';
		$SQL.='WHERE RFP_RFA_KEY=:var_N0_rfa_key) ANF, ';
		$SQL.='(SELECT * FROM V_FILIALBESTAND_AKTUELL) BST ';
		$SQL.='WHERE ANF.RFP_AST_ATUNR=BST.AST_ATUNR(+) ';
		$SQL.='AND ANF.RFM_FIL_ID=BST.FIL_ID(+)';
	
		if($Filiale!='')
		{
			$SQL.=' AND RFM_FIL_ID IN ('.$Filiale.')';
		}
		//$SQL.=' ORDER BY RFP_AST_ATUNR, RFM_FIL_ID';
		$SQL.=' ORDER BY RFM_FIL_ID, RFP_AST_ATUNR';
	
		$rsPDFDetail = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
		$rsPDFDetailZeilen = $rsPDFDetail->AnzahlDatensaetze();
	
		if($rsPDFDetailZeilen>0)
		{
			for($PDFDetailZeile=0;$PDFDetailZeile<$rsPDFDetailZeilen;$PDFDetailZeile++)
			{
				$Ausdruck->_pdf->SetFont('Arial','',8);
				if(strlen($rsPDFDetail->FeldInhalt('RFP_BEMERKUNG'))>30)
				{
					$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
					$Ausdruck->_pdf->Cell(10,4*2,$rsPDFDetail->FeldInhalt('RFM_FIL_ID'),1,0,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);
					$Ausdruck->_pdf->Cell(25,4*2,$rsPDFDetail->FeldInhalt('RFP_AST_ATUNR'),1,0,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+35,$Zeile);
					$Ausdruck->_pdf->Cell(70,4*2,$rsPDFDetail->FeldInhalt('ASP_BEZEICHNUNG'),1,0,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
					$Ausdruck->_pdf->MultiCell(50,4,$rsPDFDetail->FeldInhalt('RFP_BEMERKUNG'),1,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile);
					$Ausdruck->_pdf->Cell(15,4*2,$rsPDFDetail->FeldInhalt('RFM_MENGE'),1,0,'R',0);
					$Ausdruck->_pdf->SetXY($Spalte+170,$Zeile);
					$Ausdruck->_pdf->Cell(25,4*2,$rsPDFDetail->FeldInhalt('BESTAND'),1,0,'R',0);
					$Zeile+=4*2;
				}
				else
				{				
					$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
					$Ausdruck->_pdf->Cell(10,4,$rsPDFDetail->FeldInhalt('RFM_FIL_ID'),1,0,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);
					$Ausdruck->_pdf->Cell(25,4,$rsPDFDetail->FeldInhalt('RFP_AST_ATUNR'),1,0,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+35,$Zeile);
					$Ausdruck->_pdf->Cell(70,4,$rsPDFDetail->FeldInhalt('ASP_BEZEICHNUNG'),1,0,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
					$Ausdruck->_pdf->Cell(50,4,$rsPDFDetail->FeldInhalt('RFP_BEMERKUNG'),1,0,'L',0);
					$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile);
					$Ausdruck->_pdf->Cell(15,4,$rsPDFDetail->FeldInhalt('RFM_MENGE'),1,0,'R',0);
					$Ausdruck->_pdf->SetXY($Spalte+170,$Zeile);
					$Ausdruck->_pdf->Cell(25,4,$rsPDFDetail->FeldInhalt('BESTAND'),1,0,'R',0);	
					$Zeile+=4;
				}
				
				$GesMenge=$GesMenge+$rsPDFDetail->FeldInhalt('RFM_MENGE');
				$GesBestand=$GesBestand+$rsPDFDetail->FeldInhalt('BESTAND');
				$rsPDFDetail->DSWeiter();
	
					if(($Ausdruck->SeitenHoehe()-30)<$Zeile)
					{
						$Zeile=_Erzeuge_Seite_Rueckfuehrungsanforderung();
					}
			}
	
				$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile);
				$Ausdruck->_pdf->Cell(15,4,$GesMenge,1,0,'R',0);
				$Ausdruck->_pdf->SetXY($Spalte+170,$Zeile);
				$Ausdruck->_pdf->Cell(25,4,$GesBestand,1,0,'R',0);
				$Zeile+=4;
		}
		else{
			$Ausdruck->_pdf->SetFont('Arial','BU',14);
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile+10);
			$Ausdruck->_pdf->Cell(50,6,$Form->Format('TP',$AWISSprachKonservenPDF['Wort']['KeineDatenVorhanden']),0,0,'L',0);
		}
	
		$Ausdruck->Anzeigen();
	}
}
elseif($PFD_LISTE==1)
{
	$Spalte = 10;
	$Zeile = 20;
	$Ausdruck = null;
	$Seite='';
	$GesMenge=0;
	$GesBestand=0;
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,$Form->Format('TP',$AWISSprachKonservenPDF['Ausdruck']['txtUeberschriftRückführungsanforderung']));
	
	$Zeile=_Erzeuge_Seite_Rueckfuehrungsanforderung_Liste();
	
//	$SQL ='SELECT RFP_AST_ATUNR, ASP_BEZEICHNUNG,SUM (RFM_MENGE) AS RFM_RUECK_GES_MENGE ';
//	$SQL.='FROM RUECKFUEHRUNGSANFORDERUNGENPOS JOIN RUECKFUEHRUNGSANFPOSFILMENGEN ON (RFP_KEY = RFM_RFP_KEY) ';
//	$SQL.='LEFT JOIN (SELECT ASP_AST_ATUNR, ASP_BEZEICHNUNG ';
//	$SQL.='FROM ARTIKELSPRACHEN WHERE ASP_LAN_CODE = \''.$AWISBenutzer->BenutzerSprache().'\' ';
//	$SQL.='AND ASP_AST_ATUNR IN (SELECT RFP_AST_ATUNR ';
//	$SQL.='FROM RUECKFUEHRUNGSANFORDERUNGENPOS WHERE RFP_RFA_KEY = '.$AWIS_KEY1.')) ';
//	$SQL.='ON (RFP_AST_ATUNR = ASP_AST_ATUNR) ';
//	$SQL.='WHERE RFP_RFA_KEY = '.$AWIS_KEY1.' ';
//	$SQL.='GROUP BY RFP_AST_ATUNR, ASP_BEZEICHNUNG ';
//	$SQL.='ORDER BY RFP_AST_ATUNR';
	
	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfa_key']=intval($AWIS_KEY1);
	$BindeVariablen['var_T_land']=$AWISBenutzer->BenutzerSprache();
	
	$SQL ='SELECT RFP_AST_ATUNR, ASP_BEZEICHNUNG,SUM (RFM_MENGE) AS RFM_RUECK_GES_MENGE FROM( ';
	$SQL.='SELECT RFP_AST_ATUNR, FUNC_ASP_BEZEICHNUNG(RFP_AST_ATUNR,:var_T_land) AS ASP_BEZEICHNUNG,RFM_MENGE ';
	$SQL.='FROM RUECKFUEHRUNGSANFORDERUNGENPOS JOIN RUECKFUEHRUNGSANFPOSFILMENGEN ON (RFP_KEY = RFM_RFP_KEY) ';
	$SQL.='WHERE RFP_RFA_KEY = :var_N0_rfa_key) ';
	$SQL.='GROUP BY RFP_AST_ATUNR, ASP_BEZEICHNUNG '; 
	$SQL.='ORDER BY RFP_AST_ATUNR ';
	
	$rsPDFDetail = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
	$rsPDFDetailZeilen = $rsPDFDetail->AnzahlDatensaetze();

	if($rsPDFDetailZeilen>0)
	{
		for($PDFDetailZeile=0;$PDFDetailZeile<$rsPDFDetailZeilen;$PDFDetailZeile++)
		{
			$Ausdruck->_pdf->SetFont('Arial','',8);
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			$Ausdruck->_pdf->Cell(35,4,$rsPDFDetail->FeldInhalt('RFP_AST_ATUNR'),1,0,'L',0);
			$Ausdruck->_pdf->SetXY($Spalte+35,$Zeile);
			$Ausdruck->_pdf->Cell(90,4,$rsPDFDetail->FeldInhalt('ASP_BEZEICHNUNG'),1,0,'L',0);
			$Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
			$Ausdruck->_pdf->Cell(35,4,$rsPDFDetail->FeldInhalt('RFM_RUECK_GES_MENGE'),1,0,'R',0);
			$Zeile+=4;
			$GesMenge=$GesMenge+$rsPDFDetail->FeldInhalt('RFM_RUECK_GES_MENGE');
			$rsPDFDetail->DSWeiter();

			if(($Ausdruck->SeitenHoehe()-30)<$Zeile)
			{
				$Zeile=_Erzeuge_Seite_Rueckfuehrungsanforderung_Liste();
			}
		}

		$Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
		$Ausdruck->_pdf->Cell(35,4,$GesMenge,1,0,'R',0);
		$Zeile+=4;
	}
	else{
		$Ausdruck->_pdf->SetFont('Arial','BU',14);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile+10);
		$Ausdruck->_pdf->Cell(50,6,$Form->Format('TP',$AWISSprachKonservenPDF['Wort']['KeineDatenVorhanden']),0,0,'L',0);
	}
	
	$Ausdruck->Anzeigen();
}

}
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}

/**
*
* Funktion erzeugt die Standard-Ansicht der PDF (Kopf, Fuss, Seite, usw.)
*
* @name _Erzeuge_Seite_Rueckfuehrungsanforderung
* @version 0.1
* @author Christian Argauer
* @return int (Zeile)
*
*/
function _Erzeuge_Seite_Rueckfuehrungsanforderung()
{
	global $AWIS_KEY1;
	global $AWISSprachKonservenPDF;
	global $Spalte;
	global $Seite;
	global $Form;
	global $Ausdruck;

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;

	// Fußzeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,($Form->Format('TP',$AWISSprachKonservenPDF['Ausdruck']['txtHinweisVertraulich'])),0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);

	// Überschrift
	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfa_key']=intval('0'.$AWIS_KEY1);
	
	$SQL = 'SELECT RFA_NR, RFA_LFDNR, RFA_DATUM, DECODE(RFA_TYP,1,4090,2,4096,NULL) AS TYP, RFA_BEMERKUNG FROM RUECKFUEHRUNGSANFORDERUNGEN';
	$SQL.= ' WHERE RFA_KEY=:var_N0_rfa_key';
	$rsPDFKopf = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
	$rsPDFKopfZeilen = $rsPDFKopf->AnzahlDatensaetze();
	//$Ausdruck->_pdf->AddFont('luxxx','','luxxx.php');
	if($rsPDFKopfZeilen>0)
	{
		$Ausdruck->_pdf->SetFont('Arial','B',14);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['Ausdruck']['txtUeberschriftRückführungsanforderung']),0,0,'L',0);
		$Zeile+=10;
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_NR']).': '.$rsPDFKopf->FeldInhalt('RFA_NR').'-'.str_pad($rsPDFKopf->FeldInhalt('RFA_LFDNR'), 3, "0", STR_PAD_LEFT),0,0,'L',0);
		$Zeile+=6;
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_DATUM']).': '.$Form->Format('D',$rsPDFKopf->FeldInhalt('RFA_DATUM')),0,0,'L',0);
		$Zeile+=6;
		//$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		//$Ausdruck->_pdf->Cell(180,6,$AWISSprachKonservenPDF['RFA']['RFA_TYP'].': '.$Form->Format('T',$rsPDFKopf->FeldInhalt('TYP')),0,0,'L',0);
		//$Zeile+=6;
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->SetFont('Arial','BU',14);

        if (strlen($rsPDFKopf->FeldInhalt('RFA_BEMERKUNG')) <= 59) {
            $Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_BEMERKUNG']).': '.$rsPDFKopf->FeldInhalt('RFA_BEMERKUNG'),0,0,'L',0);
        }
        else if (strlen($rsPDFKopf->FeldInhalt('RFA_BEMERKUNG')) > 59) {
            $Ausdruck->_pdf->MultiCell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_BEMERKUNG']).': '.$rsPDFKopf->FeldInhalt('RFA_BEMERKUNG'),0,'L',0);
            $Zeile+=6;
        }

		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Zeile+=15;
	}
	else{
		$Zeile+=42;
	}

	// Listenüberschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(10,4,$Form->Format('TP',$AWISSprachKonservenPDF['RFM']['RFM_FIL_ID']),1,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);
	$Ausdruck->_pdf->Cell(25,4,$Form->Format('TP',$AWISSprachKonservenPDF['Wort']['Artikelnummer']),1,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+35,$Zeile);
	$Ausdruck->_pdf->Cell(70,4,$Form->Format('TP',$AWISSprachKonservenPDF['Wort']['ARTIKELBEZEICHNUNG']),1,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+105,$Zeile);
	$Ausdruck->_pdf->Cell(50,4,$Form->Format('TP',$AWISSprachKonservenPDF['RFP']['RFP_BEMERKUNG']),1,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile);
	$Ausdruck->_pdf->Cell(15,4,$Form->Format('TP',$AWISSprachKonservenPDF['RFM']['RFM_MENGE']),1,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+170,$Zeile);
	$Ausdruck->_pdf->Cell(25,4,$Form->Format('TP',$AWISSprachKonservenPDF['Wort']['Filialbestand']),1,0,'L',0);
	$Zeile+=4;

	return $Zeile;
}

/**
*
* Funktion erzeugt die Standard-Ansicht der PDF (Kopf, Fuss, Seite, usw.)
*
* @name _Erzeuge_Seite_Rueckfuehrungsanforderung_Liste
* @version 0.1
* @author Christian Argauer
* @return int (Zeile)
*
*/
function _Erzeuge_Seite_Rueckfuehrungsanforderung_Liste()
{
	global $AWIS_KEY1;
	global $AWISSprachKonservenPDF;
	global $Spalte;
	global $Seite;
	global $Form;
	global $Ausdruck;

	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;

	// Fußzeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,($Form->Format('TP',$AWISSprachKonservenPDF['Ausdruck']['txtHinweisVertraulich'])),0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);

	// Überschrift
	$BindeVariablen=array();
	$BindeVariablen['var_N0_rfa_key']=intval('0'.$AWIS_KEY1);
	
	$SQL = 'SELECT RFA_NR, RFA_LFDNR, RFA_DATUM, DECODE(RFA_TYP,1,4090,2,4096,NULL) AS TYP, RFA_BEMERKUNG FROM RUECKFUEHRUNGSANFORDERUNGEN';
	$SQL.= ' WHERE RFA_KEY=:var_N0_rfa_key';
	$rsPDFKopf = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
	$rsPDFKopfZeilen = $rsPDFKopf->AnzahlDatensaetze();
	
	if($rsPDFKopfZeilen>0)
	{
		$Ausdruck->_pdf->SetFont('Arial','B',14);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['Ausdruck']['txtUeberschriftRückführungsanforderung']),0,0,'L',0);
		$Zeile+=10;
		$Ausdruck->_pdf->SetFont('Arial','B',10);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_NR']).': '.$rsPDFKopf->FeldInhalt('RFA_NR').'-'.str_pad($rsPDFKopf->FeldInhalt('RFA_LFDNR'), 3, "0", STR_PAD_LEFT),0,0,'L',0);
		$Zeile+=6;
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_DATUM']).': '.$Form->Format('D',$rsPDFKopf->FeldInhalt('RFA_DATUM')),0,0,'L',0);
		$Zeile+=6;

		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);

		if (strlen($rsPDFKopf->FeldInhalt('RFA_BEMERKUNG')) <= 55) {
            $Ausdruck->_pdf->Cell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_BEMERKUNG']).': '.$rsPDFKopf->FeldInhalt('RFA_BEMERKUNG'),0,0,'L',0);
        }
        else if (strlen($rsPDFKopf->FeldInhalt('RFA_BEMERKUNG')) > 55) {
		    $Bemerkung = $rsPDFKopf->FeldInhalt('RFA_BEMERKUNG');
            $Ausdruck->_pdf->MultiCell(180,6,$Form->Format('TP',$AWISSprachKonservenPDF['RFA']['RFA_BEMERKUNG']).': '.$Bemerkung,0,'L');
        }

		$Zeile+=15;
	}
	else{
		$Zeile+=42;
	}

	// Listenüberschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(35,4,$Form->Format('TP',$AWISSprachKonservenPDF['Wort']['Artikelnummer']),1,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+35,$Zeile);
	$Ausdruck->_pdf->Cell(90,4,$Form->Format('TP',$AWISSprachKonservenPDF['Wort']['ARTIKELBEZEICHNUNG']),1,0,'L',0);
	$Ausdruck->_pdf->SetXY($Spalte+125,$Zeile);
	$Ausdruck->_pdf->Cell(35,4,$Form->Format('TP',$AWISSprachKonservenPDF['RFM']['RFM_RUECK_GES_MENGE']),1,0,'L',0);
	$Zeile+=4;

	return $Zeile;
}


/**
*
* Funktion speichert ein Bild mit EAN-Code
*
* @author Christian Argauer
* @param  char EAN13-Nummer 12-stellig
* @return Datei-Name
*
*/
function _Erzeuge_EAN13($EAN_NR)
{
	$DateiName = awis_UserExportDateiName('.png');
	$symbology = BarcodeFactory::Create (ENCODING_EAN13);
	$barcode = BackendFactory ::Create('IMAGE', $symbology);
	$barcode->SetScale(1);
	$barcode->SetMargins(10,0,0,1);
	$barcode->SetColor('black','white');
	$barcode->NoText(true);
	$barcode->SetHeight(30);
	//$barcode -> SetHeight(50);
	$barcode->Stroke($EAN_NR,$DateiName);
	return $DateiName;
}
?>