<?php
global $AWIS_KEY1;
global $FelderDel;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Tabelle= '';


if($FelderDel != '') // Wenn rotes X bei einer Massnahme gedr�ckt
{
		switch($_POST['txtSeite'])
		{
			case 'Taetigkeiten':
				
				$Tabelle = 'HTZ';
				$Felder = explode(';',$FelderDel);
				$Feld = explode('_',$Felder[1]);
				$Key=$Feld[2];	
				$SQL  = ' Select * from hrtaetzuord ';
				$SQL .= ' inner join hrfirmierung on htz_firmierung = hrf_key ';
				$SQL .= ' where htz_key ='.$Key;
				$rsDaten = $DB->RecordSetOeffnen($SQL);
					
				/**
				 * Werte f�r Frage 'Wirklich l�schen?' holen
				**/
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('BEW','HRT_FIRM'),$rsDaten->FeldInhalt('HRF_FIRMIERUNG'));
			break;
				
		}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren wenn auf Frage 'Wirklich l�schen?' ok
{
	switch ($_POST['txtTabelle'])
	{
		case 'HTZ':
			$SQL = 'DELETE FROM HRTAETZUORD WHERE htz_key='.$_POST['txtKEY']; // Massnahme aus Zuordnungen l�schen
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
				$SQL ='update hrtaetigkeiten ';
				$SQL .= 'set hrt_USER=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', hrt_USERDAT=SYSDATE';
				$SQL .=' where hrt_key='.$_POST['txtHRT'];
				$DB->Ausfuehren($SQL,'',true);
		break;	
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['cmdAktion'])
	{
		case 'Stammdaten':
			$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./taetigkeiten_Main.php?cmdAktion=Stammdaten&HRT_KEY='.$_POST['txtHRT_KEY'].' method=post>');
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
				
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
			 **/
			$Form->Erstelle_HiddenFeld('KEY',$Key);
			$Form->Erstelle_HiddenFeld('HRT',$_POST['txtHRT_KEY']);
			$Form->Erstelle_HiddenFeld('HRT_BEZEICHNUNG',$_POST['txtHRT_BEZEICHNUNG']);
			if(isset($_POST['txtCHK_AKTIV']))
			{
				$Check = 1;
			}
			else 
			{
				$Check = 0;
			}
			$Form->Erstelle_HiddenFeld('CHK_AKTIV',$Check);
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
				
			$Form->Trennzeile();
				
			/**
			 * L�schen ja/nein
			**/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();
				
			$Form->SchreibeHTMLCode('</form>');
				
			$Form->Formular_Ende();
				
			die();
		break;
	}
}

?>