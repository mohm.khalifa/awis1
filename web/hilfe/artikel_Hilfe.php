<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";

$con = awisLogon();
$Recht = awisBenutzerRecht($con, 400);
$RechtBearbeitung = awisBenutzerRecht($con, 401);
$LARRechtestufe = awisBenutzerRecht($con, 402) ;
$OENRechtestufe = awisBenutzerRecht($con, 403);

?>

<title>AWIS Hilfe</title>
</head>

<body>

<h2>AWIS - Artikelinformationen</h2>
<br>
In dieser Maske werden Informationen �ber den ATU Artikelstamm angezeigt.<br>
<hr>
<h3><a name=Suche>Suche</a></h3>
<br>
Im Register <i>Suche</i> k�nnen die Parameter eingegeben werden, nach denen gesucht werden kann. Mit dieser Maske k�nnen
unterschiedliche Ergebnisse angezeigt werden. Diese sind auf unterschiedlichen Registern enthalten. Diese k�nnen auch direkt
angew�hlt werden. Hierbei wird die zuletzt angezeigte Information erneut angezeigt.
<br>Folgende Felder k�nnen f�r die Suche verwendet werden.<br>
<table id=DatenTabelle border=1 width=95% >
<tr><td id=FeldBez>Suchfeld</td><td id=FeldBez>Hinweis</td></tr>

<tr>
	<td>ATU Artikelnummer</td>
	<td>Hier kann die (bis zu 8 stellige) ATU Artikelnummer eingegeben werden. Soll nach einem Teil
		der Nummer gesucht werden, muss ein Jokerzeichen (? oder *) angegeben werden, z.B. ein <i>CA0*</i> (f�r alle
		Artikel, deren Nummer mit 'CA0' beginnt), oder <i>C?0*</i> (f�r alle Artikel, die mit C beginnen, einen weiteren
		beliebigen Buchstaben haben, dann eine 0 folgt und weitere beliebige Zeichen folgen, CA0001 und CB0001). Dieser Parameter wird
		f�r alle Ergebnisseiten unterst�tzt.
	</td>
</tr>
<tr>
	<td>Artikel-Bezeichnung</td>
	<td>Wird hier ein Begriff eingegeben, so wird in der Artikelbezeichnung und der alternativen Artikelbezeichnung
		danach gesucht. Es m�ssen keine Jokerzeichen angegeben werden, d.h. es wird immer nach Bezeichungen gesucht,
		in denen die Bezeichnung enthalten ist. Dieser Parameter wird nur in der Ergebnisseite Artikelinfo unterst�tzt.
	</td>
</tr>
<tr>
	<td>Kommentar</td>
	<td>Hier kann in den Kommentaren (Crossing und Einkauf) gesucht werden. Soll nach einem Teil 
		der Nummer gesucht werden, muss ein Jokerzeichen (? oder *) angegeben werden. Dieser Parameter wird
		nur in der Ergebnisseite Artikelinfo unterst�tzt.
	</td>
</tr>
<tr>
	<td>Warengruppe</td>
	<td>�ber dieses Feld kann die Anzeige f�r eine Warengruppe einschr�nkt werden.
	</td>
</tr>
<tr>
	<td>Warenuntergruppe</td>
	<td>�ber dieses Feld kann die Anzeige f�r eine Warenuntergruppe einschr�nkt werden.
	</td>
</tr>
</table>

<br>Im zweiten Block k�nnen weitere Nummern f�r die Suche angegeben werden. In den weiteren Optionsfeldern
wird angegeben, nach welcher Nummer gesucht werden soll.<br>

<table id=DatenTabelle border=1 width=95% >
<tr><td id=FeldBez>Suchfeld</td><td id=FeldBez>Hinweis</td></tr>
<tr>
	<td>Zu suchende Nummer</td>
	<td>Hier wird die zu suchende Nummer angegeben. Standardm��ig wird diese Nummer vor dem Suchen umgewandelt, 
		d.h. f�hrende Nullen entfernt, etc. Soll die Nummer exakt so gesucht werden, wie sie eingegeben wurde,
		so kann die Option <k>exakte �bereinstimmung</k> aktiviert werden.
	</td>
</tr>
<tr>
	<td>EAN Nummer</td>
	<td>Die zu suchende Nummer ist eine EAN Nummer. Dieser Parameter wird
		nur in der Ergebnisseite <k>Artikelinfo</k> und <k>EAN Nummer</k> unterst�tzt.</td>
</tr>
<tr>
	<td>Gebrauchsnummer</td>
	<td>Die zu suchende Nummer ist eine Gebrauchsnummer. Dieser Parameter wird
		nur in der Ergebnisseite <k>Artikelinfo</k> unterst�tzt.</td>
</tr>
<tr>
	<td>Lieferanten-Artikel-Nummer</td>
	<td>Die zu suchende Nummer ist eine Lieferanten Artikelnummer. Dieser Parameter wird
		nur in der Ergebnisseite <k>Artikelinfo</k> und <k>Lieferantenartikel</k> unterst�tzt.</td>
</tr>
<tr>
	<td>Lieferantenset</td>
	<td>Die zu suchende Nummer ist eine Lieferanten Setnummer. Dieser Parameter wird
		nur in der Ergebnisseite <k>Artikelinfo</k> und <k>Lieferantenartikel</k> unterst�tzt.</td>
</tr>
<tr>
	<td>OE Nummer</td>
	<td>Die zu suchende Nummer ist eine OE Nummer. Dieser Parameter wird
		nur in der Ergebnisseite <k>Artikelinfo</k>, <k>OE Nummern</k> und <k>Lieferantenartikel</k> unterst�tzt.</td>
</tr>
<tr>
	<td>Beliebige Nummer</td>
	<td>Wenn diese Schaltfl�che aktiv ist, wird ein anderes Verfahren zur Suche angewandt. Dabei werden auch
	Artikel angezeigt, zu denen keine ATU Nummer existiert. Diese Option kann gerade f�r die Suche nach
	OE-Nummern sinnvoll sein.
</tr>
</table>


<br>Im zweiten Block k�nnen weitere Nummern f�r die Suche angegeben werden. In den weiteren Optionsfeldern
wird angegeben, nach welcher Nummer gesucht werden soll.<br>

<table id=DatenTabelle border=1 width=95% >
<tr><td id=FeldBez>Suchfeld</td><td id=FeldBez>Hinweis</td></tr>
<tr>
	<td>OE-Preise immer zeigen</td>
	<td>�ber diese Option wird eingestellt, ob immer eine Suche f�r OE-Preise stattfinden soll. Diese Option hat
	jedoch nur dann eine Auswirkung, wenn nach einer 'Zu suchenden Nummer' gesucht wird. Bei der direkten Suche
	nach einer ATU Nummer werden in keinem Fall OE-Preise angezeigt.
	</td>
</tr>
</table>



<br>Im zweiten Block k�nnen weitere Nummern f�r die Suche angegeben werden. In den weiteren Optionsfeldern
wird angegeben, nach welcher Nummer gesucht werden soll.<br>

<table id=DatenTabelle border=1 width=95% >
<tr><td id=FeldBez>Suchfeld</td><td id=FeldBez>Hinweis</td></tr>
<tr>
	<td>Auswahl speichern</td>
	<td>Hier k�nnen alle Parameter zwischengespeichert werden, um sie 
		bei einer erneuten Suche als Vorgabewerte zu setzen.
	</td>
</tr>
<tr>
	<td>Anzuzeigendes Register</td>
	<td>Hier kann ausgew�hlt werden, welches Register f�r die Anzeige der gefundenen Daten verwendet werden soll. Eine
		�bersicht �ber alle m�glichen Seiten finden Sie weiter unten.
	</td>
</tr>
<tr>
	<td>Maximale Datensatzanzahl</td>
	<td>Hier wird angezeigt, viele Datens�tze pro Anzeigebereich ausgegeben werden. Ein Bereich ist dabei z.B. bei einer
		Suche nach OE-Nummern mit Preise, zum einen die Liste der OE-Nummern, zum anderen die OE-Preise. Bei einer Suche nach
		beliebiger Nummer wird immer eine maximale Obergrenze von 100 Datens�tzen pro Suchebreich (OE, Liefart, ...) 
		verwendet. F�r die anderen Bereiche ist ein Programmparameter vorhanden, der auf 500 Datens�tze voreingestellt
		ist.
	</td>
</tr>
</table>


<h5>Ergebnisseiten</h5>
Folgende Ergebnisseiten stehen zur Verf�gung:<br>
<ul>
<li>Artikelstamminfos<br>
	ATU Artikel mit allen Informationen. Es werden Unterregister f�r Lieferantenartikel,
	Kommentaren, EAN Nummern und OE Nummern angezeigt. Weitere links f�hren z.B. zum Lieferantensatmmsatz.
</li>
<li>OE Nummern<br>
	OE Nummern �bersicht. Es wird eine Liste mit OE-Nummern angezeigt. Durch einen Klick auf die OE Nummer werden
	die Details angezeigt. Hierzu geh�ren der Hersteller und Bemerkungen. In weiteren Unterregistern werden die 
	ATU Artikel und die Lieferantenartikel zu der OE Nummer angezeigt.
</li>
<li>Lieferantenartikel<br>
	Hier werden die Lieferantenartikel angezeigt. Durch einen Klick auf den Link werden die Details zu den Lieferantenartikel
	angezeigt. Dazu z�hlen die ATU Artikel und die OE Nummern zu einem Lieferantenartikel.
</li>
<li>EAN-Nummern<br>
	Hier werden EAN Nummern angezeigt.
</li>
</ul>
Mit einem Klick auf die Schaltfl�che mit dem blauen Haken <img src=/bilder/eingabe_ok.png> oder durch Dr�cken
der Eingabetaste kann die Suche gestartet werden. Mit der Reset-Schaltfl�che <img src=/bilder/radierer.png> kann
das Formular gel�scht werden.


<br><br>Im folgenden werden die einzelnen Register ausf�hrlich erl�utert.<br>
<hr>
<h3><a name=ArtikelInfos>Artikelinformationen</a></h3>
<br>
In diesem Register werden die ATU Artikel angezeigt. Werden mehr Artikel zu dem in der Suchmaske ausgew�hlten
Kriterien gefunden, so wird eine Auswahlliste (<i>Trefferliste</i>) angezeigt. In dieser Trefferliste werden
wichtige Informationen �ber den Artikel angezeigt.
<?php
if (($Recht&256)==256) 	// Exportrecht?
{
  echo '<br>�ber die Schaltfl�che <i>Export</i> (<img src=/bilder/tabelle.png>) am oberen Bildschirmrand k�nnen die gefundenen Artikel als .CSV Datei
        exportiert und anschlie�end mit einem beliebigen Programm ge�ffnet werden.'   ;
}
?>
<br>Mit einem Klick auf den Link bei der ATU Nummer kann die Detailseite zu einem Artikel ge�ffnet werden.<br><br>
Diese Detailansicht ist in zwei Bereiche aufgeteilt. Im oberen Bereich werden die Informationen zum Artikel angezeigt.
<?php
if($RechtBearbeitung>0)
{
	echo 'In den Eigabefeldern k�nnen �nderungen an den Daten vorgenommen werden. Die �nderungen werden mit der 
		  Speichern-Schaltfl�che oder der Tastenkombination ALT+S gespeichert. Nach erfolgreicher Speicherung
		  wird der Bildschirm neu aufgebaut. Wird die Seite mit �nderungen verlassen oder der Browser geschlossen,
		  ohne zu Speichern, werden die Informationen ohne Warnung verworfen.';
}
?>
<br>Im unteren Bereich des Bildschirms werden mit diesem Artikel zusammenh�ngende Informationen in Form von 
Registerbl�tter dargestellt. In diesen Registerbl�ttern k�nnen die dargestellten Daten u.U. umsortiert werden.
Wenn nach einem Feld sortiert werden kann, so wird die �berschrift �ber eine Spalte als Link dargestellt.
<?php
if($RechtBearbeitung>0)
{
	echo 'In den Registern k�nnen auch z.T. Daten hinzugef�gt, ge�ndert oder gel�scht werden. Wenn ein L�schen
		  m�glich ist, so wird vor der entsprechenden Zeile ein M�lleimer dargestellt. Bei �nderungsm�glichkeit
		  werden entsprechende Eingabefelder zur Verf�gung gestellt. Die �nderungen werden mit der 
		  Speichern-Schaltfl�che oder der Tastenkombination ALT+S gespeichert. Nach erfolgreicher Speicherung
		  wird der Bildschirm neu aufgebaut. Dies gilt ebenso f�r das Hinzuf�gen von Daten.';
}
?>
<br><br>Durch einen Klick auf die Links in den Listen kann auf andere Register oder Seiten gewechselt 
und die Details f�r diese Datens�tze angezeigt werden.
<hr>
<h3><a name=OENummern>OE Nummern</a></h3>
<br>
In diesem Register werden Informationen zu den OE-Nummern angezeigt. Werden mehr als eine OE Nummer gefunden,
so wird eine Liste angezeigt. In dieser Liste kann durch einen Klick auf einen Link auf die Detailseite zu
den OE Nummern gewechselt werden.<br><br>
Die Darstellung der OE Nummern ist zweiteilig. Im oberen Teil werden die Details zu OE Nummer angezeigt.
<?php
if($OENRechtestufe>1)
{
	echo 'In den Eingabefeldern k�nnen Daten bearbeitet werden. Die �nderungen werden mit der 
		  Speichern-Schaltfl�che oder der Tastenkombination ALT+S gespeichert. Nach erfolgreicher Speicherung
		  wird der Bildschirm neu aufgebaut. Wird die Seite mit �nderungen verlassen oder der Browser geschlossen,
		  ohne zu Speichern, werden die Informationen ohne Warnung verworfen.';
}
?>
<br>Im unteren Teil des Bildschirms werden in zus�tzlichen Registern Daten angezeigt, die mit den OE Nummern
verkn�pft sind. In diesen Registerbl�ttern k�nnen die dargestellten Daten u.U. umsortiert werden.
Wenn nach einem Feld sortiert werden kann, so wird die �berschrift �ber eine Spalte als Link dargestellt.
<br>Durch einen Klick auf die Links in den Listen kann auf andere Register oder Seiten gewechselt 
und die Details f�r diese Datens�tze angezeigt werden.
<hr>
<h3><a name=Lieferantenartikel>Lieferantenartikel</a></h3>
<br>
In diesem Register werden die Lieferantenartikel dargestellt. Wird mehr als eine Lieferantennummer gefunden,
so wird eine Liste angezeigt. In dieser Liste kann durch einen Klick auf einen Link auf die Detailseite zu
den Lieferantennummern gewechselt werden.<br><br>
Die Darstellung der Informationen ist zweiteilig. Im oberen Teil werden die Details zum Lieferantenartikel angezeigt.
<?php
if($LARRechtestufe>1)
{
	echo 'In den Eingabefeldern k�nnen Daten bearbeitet werden. Die �nderungen werden mit der 
		  Speichern-Schaltfl�che oder der Tastenkombination ALT+S gespeichert. Nach erfolgreicher Speicherung
		  wird der Bildschirm neu aufgebaut. Wird die Seite mit �nderungen verlassen oder der Browser geschlossen,
		  ohne zu Speichern, werden die Informationen ohne Warnung verworfen.';
}
?>
<br>Im unteren Teil des Bildschirms werden in zus�tzlichen Registern Daten angezeigt, die mit den Lieferantenartikel
verkn�pft sind. In diesen Registerbl�ttern k�nnen die dargestellten Daten u.U. umsortiert werden.
Wenn nach einem Feld sortiert werden kann, so wird die �berschrift �ber eine Spalte als Link dargestellt.
<br>Durch einen Klick auf die Links in den Listen kann auf andere Register oder Seiten gewechselt 
und die Details f�r diese Datens�tze angezeigt werden.
<hr>
<h3><a name=EANArtikel>EAN Artikel</a></h3>
<br>
In diesem Register werden die EAN-Artikel dargestellt. Wird mehr als ein EAN-Artikel gefunden,
so wird eine Liste angezeigt. In dieser Liste kann durch einen Klick auf einen Link auf die Detailseite zu
den EAN-Artikeln gewechselt werden.<br><br>
Die Darstellung der Informationen ist zweiteilig. Im oberen Teil werden die Details zum Lieferantenartikel angezeigt.
<br>Im unteren Teil des Bildschirms werden ATU Artikel angezeigt, die mit den EAN-Artikel
verkn�pft sind. Durch einen Klick auf die Links in der Artikelliste kann auf das Register mit den ATU Artikeln
gewechselt werden.
<hr>

<br>Weitergehende Hilfe:<br><br>

<a target=_top href=/hilfe/hilfe_Main.php?HilfeThema=schaltflaechen>Schaltfl�chen</a><br>
<a target=_top href=/hilfe/hilfe_Main.php?HilfeThema=tastatur>Tastenbelegung</a><br>

<?php

awisLogoff($con);

?>
</body>

</html>
