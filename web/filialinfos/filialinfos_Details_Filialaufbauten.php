<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FilialeGeschlossen;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisFilialen.inc');
require_once('filialinfos_funktionen.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('HBT','%');
	$TextKonserven[]=array('Filialinfo','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);
	if(($Recht5001&256)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FILMBW'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$INET_LINK_Telefon =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Telefon');
	$INET_LINK_Karte =  $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	$Form->Formular_Start();

	$PEIKey = new awisFilialen($AWIS_KEY1);
	FilialInfos('FILIALAUFBAU', $PEIKey, $FilialeGeschlossen);

	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908041634");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200908041633");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>