<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RRP','*');
	$TextKonserven[]=array('RRA','*');
	$TextKonserven[]=array('REH','*');
	$TextKonserven[]=array('RST','*');
	$TextKonserven[]=array('RPP','*');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht15200 = $AWISBenutzer->HatDasRecht(15200);
	if($Recht15200==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	if (isset($_POST['sucEXPORTART']))
	{
		$Param['EXPORTART'] = $_POST['sucEXPORTART'];
		$Param['RRA_AKTIONSNAME'] = $_POST['sucRRA_AKTIONSNAME'];
		$Param['RRA_GUELTIGAB'] = $_POST['sucRRA_GUELTIGAB'];
		$Param['RRA_GUELTIGBIS'] = $_POST['sucRRA_GUELTIGBIS'];
		$Param['RRA_LAN_CODE'] = $_POST['sucRRA_LAN_CODE'];
		$Param['RST_REH_KEY'] = $_POST['sucRST_REH_KEY'];
		$Param['RRA_WGR_ID'] = $_POST['txtRRA_WGR_ID'];
		$Param['RRA_WUG_ID'] = $_POST['txtRRA_WUG_ID'];
		$Param['RRA_KENN1'] = $_POST['sucRRA_KENN1'];
		$Param['RRA_ATUNR'] = $_POST['sucRRA_ATUNR'];
		$Param['RRA_AKTIONSTEXT'] = $_POST['sucRRA_AKTIONSTEXT'];
		$Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');
		$AWISBenutzer->ParameterSchreiben("Formular_RRA_Export", serialize($Param));
	}
	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	//$_POST['sucEXPORTART']='Preisexport';
	
	$DateiName = $_POST['sucEXPORTART'];
	
	$ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);
	
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');

	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
			break;
		case 2:                 // Excel 2007
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
			break;
	}
	$XLSXObj = new PHPExcel();
	$XLSXObj->getProperties()->setCreator(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setTitle(utf8_encode($DateiName));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode($_POST['sucEXPORTART']));
	
	$XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');

	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($_POST);
	
	switch($_POST['sucEXPORTART'])
	{
		case 'Aktionen':
			// Alle Informationen sammeln
			$SQL  = 'SELECT SYSDATE AS EXPORTDATE,';
			$SQL .= ' RRA_AKTIONSNAME,';
			$SQL .= ' RRA_GUELTIGAB,';
			$SQL .= ' RRA_GUELTIGBIS,';
			$SQL .= ' RRA_LAN_CODE,';
			$SQL .= ' RRA_AKTIONSPROZENT,';
			$SQL .= ' REH_BEZEICHNUNG,';
			$SQL .= ' RRA_WGR_ID,';
			$SQL .= ' RRA_WUG_ID,';
			$SQL .= ' RRA_KENN1,';
			$SQL .= ' RRA_ATUNRLISTE,';
			$SQL .= ' RRA_AKTIONSTEXT';
			$SQL .= ' FROM reifenaktionen';
			$SQL .= ' LEFT JOIN reifenhersteller ON rra_reh_key=reh_key ';
			if ($Bedingung != '')
			{
				$SQL .= ' WHERE ' . substr($Bedingung, 4);
			}
			$SQL .= ' ORDER BY rra_gueltigab, rra_gueltigbis, rra_aktionsname';
		
			$rsRRA = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('RRA'));		
			break;
		default:
			break;
	}
	
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode($DateiName));
	$SpaltenNr = 0;
	$ZeilenNr = 1;		

	if($_POST['sucEXPORTART']=='MusterAktionen' or $_POST['sucEXPORTART']=='Aktionen')
	{
		//�berschrift
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		
		$SpaltenNr++;
	
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($_POST['sucEXPORTART']));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$SpaltenNr++;
	
		if($_POST['sucEXPORTART']=='Aktionen')
		{
			//bin zu doof, da� Datum anders auszugeben :-(
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->Format('DU',$rsRRA->FeldInhalt('EXPORTDATE')));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
			$SpaltenNr++;
		}
	
		$ZeilenNr++;
	
		// Daten zeilenweise exportieren
		$SpaltenNr = 0;
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AKTIONSNAME');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'GUELTIGAB');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'GUELTIGBIS');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'LAND');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$XLSXObj->getActiveSheet()->getCommentByColumnAndRow($SpaltenNr,$ZeilenNr)->setAuthor('AWIS');
		$objCommentRichText = $XLSXObj->getActiveSheet()->getCommentByColumnAndRow($SpaltenNr,$ZeilenNr)->getText()->createTextRun('Land:');
		$objCommentRichText->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getCommentByColumnAndRow($SpaltenNr,$ZeilenNr)->getText()->createTextRun("\r\n");
		$XLSXObj->getActiveSheet()->getCommentByColumnAndRow($SpaltenNr,$ZeilenNr)->getText()->createTextRun(utf8_encode('Moegliche Eingaben: AT, CH, CZ, DE, IT, NL'));
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AKTIONSPROZENT');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'HERSTELLER');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'WG');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'SORT');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'KENN1');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'ATUNRLISTE');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AKTIONSTEXT');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		if ($_POST['sucEXPORTART']=='Aktionen')
		{
			$ZeilenNr++;
			$SpaltenNr=0;
			while(!$rsRRA->EOF())
			{
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('RRA_AKTIONSNAME'))),PHPExcel_Cell_DataType::TYPE_STRING);
				
				$Datum=$Form->PruefeDatum($rsRRA->FeldInhalt('RRA_GUELTIGAB'),false,false,true);
				$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->ZeitZuExcel($Datum));
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
				
				$Datum=$Form->PruefeDatum($rsRRA->FeldInhalt('RRA_GUELTIGBIS'),false,false,true);
				$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->ZeitZuExcel($Datum));
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
				
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('RRA_LAN_CODE'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, $Form->Format('Z',$rsRRA->FeldInhalt('RRA_AKTIONSPROZENT')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('REH_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('RRA_WGR_ID'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('RRA_WUG_ID'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('RRA_KENN1'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('RRA_ATUNRLISTE'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, utf8_encode($Form->Format('T',$rsRRA->FeldInhalt('RRA_AKTIONSTEXT'))),PHPExcel_Cell_DataType::TYPE_STRING);
				
				$ZeilenNr++;
				$SpaltenNr=0;
				
				$rsRRA->DSWeiter();
			}
		}
		elseif ($_POST['sucEXPORTART']=='MusterAktionen')
		{
			$ZeilenNr++;
			$SpaltenNr=0;
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'Aktionsname zur eindeutigen Erkennung',PHPExcel_Cell_DataType::TYPE_STRING);
	
			$Datum=$Form->PruefeDatum('01.01.2013',false,false,true);
			$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->ZeitZuExcel($Datum));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
	
			$Datum=$Form->PruefeDatum('31.12.2013',false,false,true);
			
			$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr, $Form->ZeitZuExcel($Datum));
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
			
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'DE/CH/CZ/AT/IT/NL',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 0,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'CONTINENTAL',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '01',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, '100',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'A',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'CN0001,CN0002',PHPExcel_Cell_DataType::TYPE_STRING);
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr++,$ZeilenNr, 'WERBUNG',PHPExcel_Cell_DataType::TYPE_STRING);
	
			$ZeilenNr++;
			$SpaltenNr=0;
		}
		
	}
	
	for($S='A';$S<='F';$S++)
	{
		$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	
	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			$objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
			break;
		case 2:                 // Excel 2007
			$objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
			break;
	}
	$objWriter->save('php://output');
	$XLSXObj->disconnectWorksheets();
}
catch(exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getMessage());
}



/**
 * Bedingung zusammenbauen
 *
 * @param string $_POST
 * @return string
 */
function _BedingungErstellen()
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if (floatval($AWIS_KEY1) != 0)
	{
		$Bedingung.= 'AND RRA_KEY = :var_N0_RRA_KEY';
		$DB->SetzeBindevariable('RRA', 'var_N0_RRA_KEY', $AWIS_KEY1, awisDatenbank::VAR_TYP_GANZEZAHL);
		return $Bedingung;
	}
	
	if (isset($_POST['sucRRA_AKTIONSNAME']) AND $_POST['sucRRA_AKTIONSNAME'] != '')
	{
		$Bedingung .= 'AND UPPER(RRA_AKTIONSNAME) ' . $DB->LIKEoderIST($_POST['sucRRA_AKTIONSNAME'],awisDatenbank::AWIS_LIKE_UPPER);
	}
	
	if (isset($_POST['sucRRA_LAN_CODE']) AND $_POST['sucRRA_LAN_CODE'] != '-1')
	{
		$Bedingung .= 'AND RRA_LAN_CODE = :var_T_RRA_LAN_CODE ';
		$DB->SetzeBindevariable('RRA', 'var_T_RRA_LAN_CODE', $_POST['sucRRA_LAN_CODE'], awisDatenbank::VAR_TYP_TEXT);
	}
	
	if (isset($_POST['sucRRA_GUELTIGAB']) AND $_POST['sucRRA_GUELTIGAB'] != '')
	{
		$Bedingung .= " AND RRA_GUELTIGAB >= " . $DB->FeldInhaltFormat('DU', $_POST['sucRRA_GUELTIGAB'], true);
	}
	
	if (isset($_POST['sucRRA_GUELTIGBIS']) AND $_POST['sucRRA_GUELTIGBIS'] != '')
	{
		$Bedingung .= " AND RRA_GUELTIGBIS <= " . $DB->FeldInhaltFormat('DU', $_POST['sucRRA_GUELTIGBIS'], true);
	}
	
	if (isset($_POST['sucRST_REH_KEY']) AND $_POST['sucRST_REH_KEY'] != '-1')
	{
		$Bedingung .= 'AND RST_REH_KEY = :var_N0_RST_REH_KEY ';
		$DB->SetzeBindevariable('RRA', 'var_N0_RST_REH_KEY', $_POST['sucRST_REH_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
	}
	
	if (isset($_POST['txtRRA_WGR_ID']) AND $_POST['txtRRA_WGR_ID'] != '')
	{
		$Bedingung .= 'AND RRA_WGR_ID = :var_T_RRA_WGR_ID ';
		$DB->SetzeBindevariable('RRA', 'var_T_RRA_WGR_ID', $_POST['txtRRA_WGR_ID'], awisDatenbank::VAR_TYP_TEXT);
	}
	
	if (isset($_POST['txtRRA_WUG_ID']) AND $_POST['txtRRA_WUG_ID'] != '')
	{
		$Bedingung .= 'AND RRA_WUG_ID = :var_T_RRA_WUG_ID ';
		$DB->SetzeBindevariable('RRA', 'var_T_RRA_WUG_ID', $_POST['txtRRA_WUG_ID'], awisDatenbank::VAR_TYP_TEXT);
	}

	if (isset($_POST['sucRRA_KENN1']) AND $_POST['sucRRA_KENN1'] != '')
	{
		$Bedingung .= 'AND RRA_KENN1 = :var_T_RRA_KENN1 ';
		$DB->SetzeBindevariable('RRA', 'var_T_RRA_KENN1', $_POST['sucRRA_KENN1'], awisDatenbank::VAR_TYP_TEXT);
	}
	
	if (isset($_POST['sucRRA_ATUNR']) AND $_POST['sucRRA_ATUNR'] != '')
	{
		$Bedingung .= 'AND RRA_ATUNRLISTE ' . $DB->LIKEoderIST($_POST['sucRRA_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER);
	}

	if (isset($_POST['sucRRA_AKTIONSTEXT']) AND $_POST['sucRRA_AKTIONSTEXT'] != '')
	{
		$Bedingung .= 'AND UPPER(RRA_AKTIONSTEXT) ' . $DB->LIKEoderIST($_POST['sucRRA_AKTIONSTEXT'],awisDatenbank::AWIS_LIKE_UPPER);
	}
	
	return $Bedingung;
}

?>