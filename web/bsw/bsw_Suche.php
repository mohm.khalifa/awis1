<?php
global $BSW;
try
{
    $Speichern = isset($BSW->Param['SPEICHERN']) && $BSW->Param['SPEICHERN'] == 'on'?true:false;

    $BSW->RechteMeldung(0);//Anzeigen Recht
	echo "<form name=frmSuche method=post action=./bsw_Main.php?cmdAktion=Details>";

	$BSW->Form->Formular_Start();
    
	$BSW->Form->ZeileStart();
	$BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_KARTENNUMMER'].':',190);
	$BSW->Form->Erstelle_TextFeld('*BSW_KARTENNUMMER',($Speichern?$BSW->Param['BSW_KARTENNUMMER']:''),10,110,true);
	$BSW->AWISCursorPosition='sucBSW_KARTENNUMMER';
	$BSW->Form->ZeileEnde();

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_BONNR'].':',190);
    $BSW->Form->Erstelle_TextFeld('*BSW_BONNR',($Speichern?$BSW->Param['BSW_BONNR']:''),10,110,true);
    $BSW->Form->ZeileEnde();


    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['Wort']['DatumVom'].':',190);
    $BSW->Form->Erstelle_TextFeld('*BSW_DATUM_VON',($Speichern?$BSW->Param['BSW_REKLAMATIONSDATUM_VON']:''),10,150,true,'','','','D');
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['Wort']['DatumBis'].':',100);
    $BSW->Form->Erstelle_TextFeld('*BSW_DATUM_BIS',($Speichern?$BSW->Param['BSW_REKLAMATIONSDATUM_BIS']:''),10,150,true,'','','','D');
    $BSW->Form->ZeileEnde();


    // Auswahl kann gespeichert werden
    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $BSW->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $BSW->Form->ZeileEnde();

	$BSW->Form->Formular_Ende();

	$BSW->Form->SchaltflaechenStart();
	$BSW->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$BSW->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$BSW->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $BSW->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($BSW->Recht54000&32) == 32){
		$BSW->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $BSW->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$BSW->Form->SchaltflaechenEnde();

	$BSW->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($BSW->Form instanceof awisFormular)
	{
		$BSW->Form->DebugAusgabe(1, $ex->getSQL());
		$BSW->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$BSW->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($BSW->Form instanceof awisFormular)
	{
		$BSW->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>