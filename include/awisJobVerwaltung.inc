<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
//require_once('awisINFO.php');

class awisJobScheduler
{
	/**
	 * Keine g�ltige Verbindung zur Datenquelle
	 *
	 */
	const ERR_KEINREC = 1;
	/**
	 * Falsches Zeitformat
	 *
	 */
	const ERR_ZEIT = 2;
	/**
	 * Unbekannter Job Typ
	 *
	 */
	const ERR_FALSCHERTYP = 3;
	/**
	 * Fehler im Zeitformat
	 *
	 */
	const ERR_ZEITFORMAT = 4;
	/**
	 * DB Fehler
	 *
	 */
	const ERR_DBFEHLER = 5;

	/**
	 * Recordset
	 *
	 * @var array
	 */
	private $_rsXJO;
	/**
	 * OCI-Connection
	 *
	 * @var awisDBConnection
	 */
	private $_DB;
	/**
	 * Letzter Starttermin
	 *
	 * @var time
	 */
	private $_Starttermin = '';

	/**
	 * Anzahl aller aktiven Jobs
	 *
	 * @var int
	 */
	private $_AnzahlAktiverJobs=0;
	/**
	 * Aktueller Benutzer
	 * @var awisBenutzer
	 */
	private $_AWISBentzer = null;

	function __construct()
	{
		$SQL = 'SELECT * ';
		$SQL .= ' FROM jobliste ';
		$SQL .= ' WHERE (xjo_status=1';
		$SQL .= ' OR (xjo_status=2 AND xjo_userdat<(sysdate-(xjo_intervall/1440)))';		// Erneuter Start, falls aufgerufenes Programm sich nicht gemeldet hat
		$SQL .= ' OR (xjo_status=3 AND xjo_userdat<(sysdate-((xjo_intervall*2)/1440))))';	// Erneuter Start, falls mehr als 2 Intervalle keinen Start -> Absturz?
		$SQL .= ' AND xjo_naechsterstart < sysdate';
		$SQL .= ' ORDER BY XJO_JobBezeichnung';

		$this->_AWISBentzer = awisBenutzer::Init('awis_jobs');
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();

		$this->_rsXJO = $this->_DB->RecordSetOeffnen($SQL);

		$this->_AnzahlAktiverJobs = $this->_rsXJO->AnzahlDatensaetze();

		if($this->_AnzahlAktiverJobs>0)
		{
			$this->_Starttermin = strtotime($this->_rsXJO->FeldInhalt('XJO_NAECHSTERSTART'));
			if($this->_Starttermin==-1)
			{
				throw new Exception('Fehler im Zeitformat',self::ERR_ZEITFORMAT);
			}
			echo 'Habe '.$this->_AnzahlAktiverJobs.' aktive Jobs'.PHP_EOL;
		}
		else
		{
			echo date('c').': Nichts zu tun...'.PHP_EOL;
		}
	}

	/**
	 * Starten der einzelnen Job
	 *
	 * @return unknown
	 */
	function StarteJobs()
	{

		if($this->_rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG')=='')
		{
			return 0;
		}

		echo 'Starte Jobs...'.PHP_EOL;
		//for($i=0;$i<$this->_AnzahlAktiverJobs;$i++)
		while(!$this->_rsXJO->EOF())
		{
			$Typ = $this->_rsXJO->FeldInhalt('XJO_TYP');
			$Befehl = $this->_rsXJO->FeldInhalt('XJO_BEFEHLSZEILE');
			$StartZeit = $this->_rsXJO->FeldInhalt('XJO_NAECHSTERSTART');
			$ErlaubteKnoten = $this->_rsXJO->FeldInhalt('XJO_ERLAUBTEKNOTEN');

			// Darf der Job auf diesem Rechner laufen?
			if(array_search(getenv('HOST'),explode(';',$ErlaubteKnoten))===false)
			{
                echo '  -> '.$this->_rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').' darf nicht auf diesem Knoten laufen...'.PHP_EOL;
                $this->_rsXJO->DSWeiter();
				continue;
			}
			echo '  -> Knoten '.getenv('HOST').', Job  '.$this->_rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG').' wird gestartet...'.PHP_EOL;

			if(strtotime($StartZeit)==-1)
			{
				throw new Exception('Falsche Zeit',self::ERR_ZEIT);
			}

			$StartZeit = strtotime($StartZeit);
			if($StartZeit <= time())
			{
				echo 'Job '.$Befehl.' ausfuehren ('.date('c') .')'.PHP_EOL;

				//***************************************************
				//* Status auf 'L�uft' setzen
				//***************************************************
				$SQL = 'UPDATE jobliste ';
				$SQL .= ' SET xjo_status=2';		// Job wurde gestartet
				$SQL .= ' ,XJO_AKTUELLERKNOTEN=\''.getenv('HOST').'\'';
				$SQL .= ' ,xjo_user=\'jobscheduler\'';
				$SQL .= ' ,xjo_userdat=SYSDATE';
				$SQL .= ' WHERE xjo_key=0'.$this->_rsXJO->FeldInhalt('XJO_KEY');
				if($this->_DB->Ausfuehren($SQL)===false)
				{
					throw new Exception('Fehler bei der Datenbank',self::ERR_DBFEHLER);
				}

				//***************************************************
				//* Befehl ausf�hren
				//***************************************************
				$Erg = 0;
				switch ($Typ)
				{
					case 0:		// Deaktiviert
						continue;
					case 1:		// System
						// Befehl aufrufen
						system('/bin/bash '.$Befehl,$Erg);
						break;
					case 2:			// PHP Skript
						// Befehl aufrufen
						system('/usr/local/php/bin/php -c /etc/php.ini -f '.$Befehl,$Erg);
						break;
					case 3:		// SQL Skript
						// Befehl aufrufen
						system('/usr/bin/sqlplus awis/iloulua@awisrac @'.$Befehl,$Erg);
						break;
					default:
						throw new Exception('Falscher Typ:'.$Typ, self::ERR_FALSCHERTYP);
						break;
				}

				//***********************************************************************
				//* Jobliste wieder aktualisieren und n�chsten Termin berechnen
				//***********************************************************************
				echo ' Job beendet (oder im Hintergrund). Berechne neuen Start...'.PHP_EOL;
				$StartNeu = strtotime($this->_rsXJO->FeldInhalt('XJO_NAECHSTERSTART'));
                
                
				if($StartNeu<0)
				{
					$StartNeu = time();
				}
				while($StartNeu < time())
				{
					$StartNeu += (60*$this->_rsXJO->FeldInhalt('XJO_INTERVALL'));
				}
                
				$StartZeit = date('H:i:s',$StartNeu);

				if($StartZeit=='00:00:00')
				{
					$StartZeit='00:00:01';
				}
				$Tag=0;
				if(strtotime($StartZeit) < strtotime($this->_rsXJO->FeldInhalt('XJO_SERVICESTART')))
				{
					$StartZeit = ($this->_rsXJO->FeldInhalt('XJO_SERVICESTART'));
                    echo '     -> StartZeit kleiner ServiceStart : '.$StartZeit.PHP_EOL;
				}
				elseif(strtotime($StartZeit) > strtotime($this->_rsXJO->FeldInhalt('XJO_SERVICEENDE')))
				{
					$Tag += 1;
					$StartZeit = ($this->_rsXJO->FeldInhalt('XJO_SERVICESTART'));
                    echo '     -> StartZeit am naechsten Tag: '.$StartZeit.PHP_EOL;
				}

                // Zusatztag ber�cksichtigen
                $StartNeu += ($Tag * (60*60*24));
                // Und die ermittelte Startzeit dazu
                $StartNeu = date('d.m.Y',$StartNeu).' '.$StartZeit;

					// Wochentage ber�cksichtigen
				$TageArray = explode(';',$this->_rsXJO->FeldInhalt('XJO_SERVICETAGE'));
				$j=0;
                
				$StartNeu = strtotime($StartNeu);
				while(array_search(date('w',$StartNeu),$TageArray)===false)
				{
						// Einen Tag weiter, fr�hester Termin!
					$StartNeu = strtotime(date('d.m.Y',($StartNeu+(60*60*24)).' '.$StartZeit));
					if($j++>10)
					{
						break;
					}
				}
                
				$SQL = 'UPDATE jobliste';
				$SQL .= ' SET xjo_naechsterstart = '.$this->_DB->FeldInhaltFormat('DU',date('d.m.Y H:i:s',$StartNeu));
				$SQL .= ', xjo_user=\'jobscheduler\', xjo_userdat=sysdate';
                $SQL .= ', xjo_status = 1';             // Job wieder aktivieren
				$SQL .= ' WHERE xjo_key = '.$this->_rsXJO->FeldInhalt('XJO_KEY');
				if($this->_DB->Ausfuehren($SQL)===false)
				{
					throw new Exception('Fehler beim Speichern:',200709101050);
				}

				echo '   -> Neuer Starttermin:'.date('c',$StartNeu).PHP_EOL;
			}

			$this->_rsXJO->DSWeiter();
		}
		return true;
	}
}
/**
 * Klasse zur Verwaltung von Jobmeldungen
 *
 */
class awisJobProtokoll
{
	/**
	 * Startmeldung
	 *
	 */
	const TYP_START = 1;
	/**
	 * Standardmeldung
	 *
	 */
	const TYP_MELDUNG = 5;
	/**
	 * Endemeldung
	 *
	 */
	const TYP_ENDE = 9;
	// Fehlercodes
	/**
	 * Job konnte nicht gefunden werden
	 *
	 */
	const ERR_KEINJOB = 1;
	/**
	 * Job ist nicht aktiv
	 *
	 */
	const ERR_KEINAKTIVERJOB = 2;
	/**
	 * Fehler beim Schreiben eines Log Eintrags
	 *
	 */
	const ERR_LOGSCHREIBEN = 3;

	/**
	 * Keine aktive Datenbankverbindung
	 *
	 */
	const ERR_KEINEDBVERBINDUNG = 4;
	/**
	 * Job l�uft bereits
	 *
	 */
	const ERR_JOBLAEUFT = 5;
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDBConnection
	 */
	private $_DB = null;

	/**
	 * Job-ID
	 *
	 * @var int
	 */
	private $_JobID = 0;

	/**
	 * Mailadresse f�r Start und Ende-Mails
	 *
	 * @var string
	 */
	private $_MailAdresse = '';

	/**
	 * Meldung f�r die Start und Ende Eintragungen.
	 *
	 * @var string
	 */
	private $_Meldung = '';

	/**
	 * Erstellen des skJobProtokolls
	 *
	 * @param int $JobID
	 * @param resource $this->_DB
	 * @param string $Meldung
	 */
	public function __construct($JobID, $Meldung='')
	{
		$this->_DB= awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();

		$this->_JobID=intval($JobID);

		$SQL = 'SELECT * FROM jobliste WHERE xjo_key=0'.intval($JobID);
		$rsXJL = $this->_DB->RecordSetOeffnen($SQL);

		if($rsXJL->EOF())
		{
			throw new Exception('Kein Job ('.$JobID.').',self::ERR_KEINJOB);
		}
		elseif($rsXJL->FeldInhalt('XJO_STATUS')=='I')
		{
			throw new Exception('Kein aktiver Job.',self::ERR_KEINAKTIVERJOB);
		}

			// Start protokollieren
		$SQL = 'UPDATE jobliste ';
		$SQL .= ' SET xjo_status=3 ';
		$SQL .= ' ,XJO_AKTUELLERKNOTEN=\''.getenv('HOST').'\'';
		$SQL .= ' ,xjo_letzterStart=sysdate';
		$SQL .= ' ,XJO_LetzterStatus=0';
		$SQL .= ' ,XJO_LetzteMeldung=\''.$Meldung.'\'';
		$SQL .= ' WHERE xjo_key=0'.$JobID;

		if($this->_DB->Ausfuehren($SQL)===false)
		{
			throw new Exception('Fehler beim Job-Log schreiben:'.$SQL,self::ERR_LOGSCHREIBEN);
		}
	}

	/**
	 * Beenden des Joblogs
	 *
	 */
	public function __destruct()
	{
		$this->SchreibeLog(9, 'Ende '.$this->_Meldung,0,$this->_MailAdresse);

			// Ende: Status wieder zuruecksetzen
		$SQL = 'UPDATE jobliste SET xjo_status=1 ';
		$SQL .= ' ,xjo_letztesende=sysdate ';
		$SQL .= ' ,xjo_userdat=sysdate ';
		$SQL .= ' ,xjo_user=\''.getenv('HOST').'\'';
		$SQL .= ' WHERE xjo_key=0'.$this->_JobID;
		if($this->_DB->Ausfuehren($SQL)===false)
		{
			throw new Exception('Fehler beim Job-Log schreiben:',self::ERR_LOGSCHREIBEN);
		}
	}

	/**
	 * Schreibt einen Log-Eintrag
	 *
	 * @param int $Typ (1=Start, 5=Debug, 9=Ende)
	 * @param $Status  (0=Ende, 1=Info, 2=Warnung, 3=Fehler, 4=Fataler Fehler)
	 * @param string $Meldung
	 */
	public function SchreibeLog($Typ, $Status = 0, $Meldung = '')
	{
		$this->_JobID=intval($this->_JobID);
		$SQL = 'SELECT * FROM jobliste WHERE xjo_key=0'.intval($this->_JobID);
		$rsXJL = $this->_DB->RecordsetOeffnen($SQL);
		if($rsXJL->EOF())
		{
			throw new Exception('Kein Job.',self::ERR_KEINJOB);
		}
		elseif($rsXJL->FeldInhalt('XJO_STATUS')=='I')
		{
			throw new Exception('Kein aktiver Job.',self::ERR_KEINAKTIVERJOB);
		}

		$SQL = 'UPDATE jobliste ';
		$SQL .= ' SET XJO_LetzterStatus=0'.intval($Status);
		$SQL .= ' ,XJO_LetzteMeldung=\''.str_replace("'","~",$Meldung).'\'';
		$SQL .= ' WHERE xjo_key=0'.$this->_JobID;

		if($this->_DB->Ausfuehren($SQL)===false)
		{
			throw new Exception('Fehler beim Job-Log schreiben:',self::ERR_LOGSCHREIBEN);
		}

		//$Inf = new awisINFO();
		$Inf = new awisWerkzeuge();

		switch (intval($Status))
		{
			case 1:	// Info
				if($rsXJL->FeldInhalt('XJO_INFOMAIL')!='')
				{
					$Inf->EMail($rsXJL->FeldInhalt('XJO_INFOMAIL'),'JobScheduler: INFO - '.$rsXJL->FeldInhalt('XJO_JOBBEZEICHNUNG'),$Meldung,1);
				}
				break;
			case 2: // Warnung
				if($rsXJL->FeldInhalt('XJO_WARNMAIL'))
				{
					$Inf->EMail($rsXJL->FeldInhalt('XJO_WARNMAIL'),'JobScheduler: WARNUNG - '.$rsXJL->FeldInhalt('XJO_JOBBEZEICHNUNG'),$Meldung,2);
				}
				break;
			case 3:	// Fehler
			case 4: // Fataler Fehler
				if($rsXJL->FeldInhalt('XJO_FEHLMAIL'))
				{
					$Inf->EMail($rsXJL->FeldInhalt('XJO_FEHLMAIL'),'JobScheduler: FEHLER - '.$rsXJL->FeldInhalt('XJO_JOBBEZEICHNUNG'),$Meldung,3);
				}
				$Inf->SyslogEintrag(3,$Meldung);
				break;
		}
	}
}
?>
