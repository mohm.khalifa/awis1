<?php

class awisAenderungshistorie
{

    /**
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * @var awisBenutzer
     */
    private $_AWISBenutzer;

    function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
    {
        $this->_DB = $DB;
        $this->_AWISBenutzer = $AWISBenutzer;
    }

    /**
     * @param string    $XTN_Kuerzel
     * @param string    $XXX_KEY
     * @param string    $Feld
     * @param string    $WertAlt
     * @param string    $WertNeu
     * @param string    $Bemerkung
     * @param string    $User
     * @param string    $UserDat
     *
     * @return bool
     */
    public function SchreibeAenderung($XTN_Kuerzel, $XXX_KEY, $Feld, $WertAlt, $WertNeu, $Bemerkung = '', $User = '', $UserDat = '')
    {
        if ($User == '') {
            $User = $this->_AWISBenutzer->BenutzerName();
        }

        if($WertAlt != $WertNeu) {

            $SQL = 'insert into AENDERUNGSHISTORIE (';
            $SQL .= '     XAH_XTN_KUERZEL,';
            $SQL .= '     XAH_XXX_KEY,';
            $SQL .= '     XAH_WERTALT,';
            $SQL .= '     XAH_WERTNEU,';
            $SQL .= '     XAH_FELD,';
            $SQL .= '     XAH_BEMERKUNG,';
            $SQL .= '     XAH_USER,';
            $SQL .= '     XAH_USERDAT';
            $SQL .= ' ) values (';
            $SQL .= ' ' . $this->_DB->WertSetzen('XAH', 'T', $XTN_Kuerzel);
            $SQL .= ', ' . $this->_DB->WertSetzen('XAH', 'N0', $XXX_KEY);
            $SQL .= ', ' . $this->_DB->WertSetzen('XAH', 'T', $WertAlt);
            $SQL .= ', ' . $this->_DB->WertSetzen('XAH', 'T', $WertNeu);
            $SQL .= ', ' . $this->_DB->WertSetzen('XAH', 'T', $Feld);
            $SQL .= ', ' . $this->_DB->WertSetzen('XAH', 'T', $Bemerkung);
            $SQL .= ', ' . $this->_DB->WertSetzen('XAH', 'T', $User);
            if ($UserDat == '') {
                $SQL .= ', sysdate';
            } else {
                $SQL .= ', ' . $this->_DB->WertSetzen('XAH', 'DU', $UserDat);
            }

            $SQL .= ' )';

            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('XAH'));
        }
        return true;
    }

    public function SchreibeAenderungenAusPOST($XXX_KEY, array $POST, array $RelevanteFelder = array()){
        $RelevanteFelder = array_flip($RelevanteFelder);
        foreach ($POST as $Feld => $Wert){
            if(substr($Feld,0,3) == 'txt'){
                $Feldname = substr($Feld,3);
                //Nur wenn es ein oldFeld gibt und das Feld relevant ist
                if(isset($POST['old'.$Feldname]) and (isset($RelevanteFelder[$Feldname]) or count($RelevanteFelder) == 0 )){
                    //Und nur wenn sich auch wirklich was ge�ndert hat
                    if($POST['old'.$Feldname] != $POST['txt'.$Feldname]){
                        $Kuerzel = @explode('_',$Feldname)[0];
                        $this->SchreibeAenderung($Kuerzel,$XXX_KEY,$Feldname, $POST['old'.$Feldname],  $POST['txt'.$Feldname]);
                    }
                }
            }
        }
    }

    /**
     * @param string  $XTN_Kuerzel
     * @param string  $XXX_KEY
     * @param string  $Feld
     * @param string  $User
     * @param string  $DatumVon
     * @param string  $DatumBis
     * @param boolean $OrderByDESC
     *
     * @return awisRecordset
     */
    public function LeseAenderungen($XTN_Kuerzel, $XXX_KEY = '', $Feld = '', $User = '', $DatumVon = '', $DatumBis = '', $OrderByDESC = true)
    {
        $SQL = 'select';
        $SQL .= '     XAH_KEY,';
        $SQL .= '     XAH_XTN_KUERZEL,';
        $SQL .= '     XAH_XXX_KEY,';
        $SQL .= '     XAH_WERTALT,';
        $SQL .= '     XAH_WERTNEU,';
        $SQL .= '     XAH_FELD,';
        $SQL .= '     XAH_BEMERKUNG,';
        $SQL .= '     XAH_USER,';
        $SQL .= '     XAH_USERDAT';
        $SQL .= ' from';
        $SQL .= '     AENDERUNGSHISTORIE';
        $SQL .= ' WHERE ';
        $SQL .= ' XAH_XTN_KUERZEL = ' . $this->_DB->WertSetzen('XAH', 'T', $XTN_Kuerzel);

        if ($XXX_KEY != '') {
            $SQL .= ' AND XAH_XXX_KEY = ' . $this->_DB->WertSetzen('XAH', 'T', $XXX_KEY);
        }

        if ($Feld != '') {
            $SQL .= ' AND XAH_FELD ' . $this->_DB->LikeOderIst($Feld, awisDatenbank::AWIS_LIKE_UPPER, 'XAH');
        }

        if ($User != '') {
            $SQL .= ' AND upper(XAH_USER) = ' . $this->_DB->WertSetzen('XAH', 'T', $User);
        }

        if ($DatumVon != '') {
            $SQL .= ' AND XAH_USERDAT >= ' . $this->_DB->WertSetzen('XAH', 'D', $DatumVon);
        }

        if ($DatumBis != '') {
            $SQL .= ' AND XAH_USERDAT <= ' . $this->_DB->WertSetzen('XAH', 'D', $DatumBis);
        }

        $SQL .= ' ORDER BY XAH_USERDAT ' . ($OrderByDESC?'DESC':'ASC');

        $rsXAH = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('XAH'));

        return $rsXAH;
    }

}