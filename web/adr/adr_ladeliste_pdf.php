<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

//Werden ben�tigt f�r EAN
require_once('register.inc.php');
require_once('db.inc.php');
require_once('sicherheit.inc.php');

require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('ADK','%');		
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht4300 = $AWISBenutzer->HatDasRecht(4300);
	if($Recht4300==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');

	if(isset($_GET['ADK_KEY']))
	{
		$AWIS_KEY1 = floatval($_GET['ADK_KEY']);
	}	
	
	$Spalte = 20;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	
	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Ladeliste');
	
	$Zeile=_Erzeuge_Seite_Ladeliste();
	
	$SQL = 'SELECT ADK_STATUS FROM ADRKOPF WHERE ADK_KEY=0'.$AWIS_KEY1;
	$rsADK=$DB->RecordSetOeffnen($SQL);
	$Status = $rsADK->FeldInhalt('ADK_STATUS');
	
	/***************************************
	* Hole Daten per SQL
	***************************************/	
	$SQL = 'SELECT ADRST.*, ADRPOS.*';		
	$SQL .= ' FROM ADRSTAMM ADRST';	
	$SQL .= ' LEFT OUTER JOIN ADRKOPF ON ADK_KEY = 0'.$AWIS_KEY1;	
	if ($Status == 'A')
	{
		$SQL .= ' INNER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY AND ADP_ADK_KEY = ADK_KEY';
	}
	else 
	{
		$SQL .= ' LEFT OUTER JOIN ADRPOS ON ADP_ADS_KEY = ADS_KEY AND ADP_ADK_KEY = ADK_KEY';
	}	
	$SQL .= ' WHERE ADS_ADRART = ADK_ART';	
	$SQL .= ' ORDER BY ADS_ADRNR ASC';

	$rsADP = $DB->RecordsetOeffnen($SQL);	
		
	$Form->DebugAusgabe(1,$SQL);
	
	$rsADP = $DB->RecordSetOeffnen($SQL);
	$rsADPZeilen = $rsADP->AnzahlDatensaetze();
	
	if($rsADPZeilen==0)		// Keine Daten
	{
		$Form->Hinweistext('Keine Daten gefunden!');
		die();
	}		
		
	for($ADPZeile=0;$ADPZeile<$rsADPZeilen;$ADPZeile++)
	{	
		if(($Ausdruck->SeitenHoehe()-20)<$Zeile)
		{
			$Zeile=_Erzeuge_Seite_Ladeliste();
		}			
		
		$Ausdruck->_pdf->SetFont('Arial','',8);
				
		$Ausdruck->_pdf->SetFont('Arial','',9);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(80,6,$rsADP->FeldInhalt('ADS_LAGABEZ'),1,0,'L',0);	
			
		$Ausdruck->_pdf->setXY($Spalte+80,$Zeile);
		$Ausdruck->_pdf->cell(40,6,$rsADP->FeldInhalt('ADS_BEHTYP'),1,0,'L',0);
			
		$Ausdruck->_pdf->setXY($Spalte+120,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$Form->Format('N0', ($rsADP->FeldInhalt('ADP_MENGE')!=''?$rsADP->FeldInhalt('ADP_MENGE'):'0')),1,0,'R',0);		
						
		$Ausdruck->_pdf->setXY($Spalte+145,$Zeile);
		$Ausdruck->_pdf->cell(25,6,$Form->Format('NO', ($rsADP->FeldInhalt('ADP_GEWICHT')!=''?$rsADP->FeldInhalt('ADP_GEWICHT'):'0')). ' kg',1,0,'R',0);			
		
		$Zeile+=6;
	
		$rsADP->DSWeiter();
	}	
	
	$Ausdruck->Anzeigen();	
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


function _Erzeuge_Seite_Ladeliste()
{
	global $AWIS_KEY1;
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
	
	if ($Seite == 1)
	{			
		$SQL = 'SELECT *';
		$SQL .=' FROM ADRKOPF ';		
		$SQL .=' WHERE ADK_KEY=0'.$AWIS_KEY1;		
		
		$rsADKKopf = $DB->RecordSetOeffnen($SQL);							
									
		$Ausdruck->_pdf->SetFont('Arial','B',20);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->Cell(40,6,$AWISSprachKonserven['ADK']['txt_Ladeliste'],0,0,'L',0);				
		$Zeile+=8;		
		
		$Ausdruck->_pdf->SetFont('Arial','B',12);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
		$Ausdruck->_pdf->cell(60,6,$AWISSprachKonserven['ADK']['txt_Uebernahmescheinnummer'].':',0,0,'L',0);
		
		$EAN='1'.str_pad($rsADKKopf->FeldInhalt('ADK_FIL_ID'),4,'0','L').date('y').date('m').str_pad($rsADKKopf->FeldInhalt('ADK_VORGANGNR'),3,'0','L');	
	
		$EAN_CLASS = new BarcodeEncode_EAN13();
				
		if ($EAN_CLASS->Validate($EAN))
		{
			$EAN_CODE = $EAN_CLASS->Enc($EAN);
			$EAN_SAVE = $EAN_CODE->iData;
		}
				
		$Ausdruck->_pdf->cell(30,6,$EAN_SAVE,0,0,'L',0);				
		$Zeile+=10;		
		
		$Ausdruck->_pdf->SetFont('Arial','',12);
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);				
		$Ausdruck->_pdf->cell(40,6,$AWISSprachKonserven['ADK']['txt_Hinweisladeliste'],0,0,'L',0);						
	}
	else 
	{
		$Zeile+=5;
	}
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);		
	$Zeile=$Zeile+10;		
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	$Ausdruck->_pdf->cell(80,6,'Interne Bezeichnung',1,0,'C',1);				
		
	$Ausdruck->_pdf->setXY($Spalte+80,$Zeile);
	$Ausdruck->_pdf->cell(40,6,'Gebinde',1,0,'C',1);		
		
	$Ausdruck->_pdf->setXY($Spalte+120,$Zeile);
	$Ausdruck->_pdf->cell(25,6,'Menge',1,0,'C',1);		
		
	$Ausdruck->_pdf->setXY($Spalte+145,$Zeile);
	$Ausdruck->_pdf->cell(25,6,'Gewicht',1,0,'C',1);															
			
	$Zeile+=6;	
	
	return $Zeile;
}
?>

