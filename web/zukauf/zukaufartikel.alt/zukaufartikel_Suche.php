<?
global $CursorFeld;
global $AWISBenutzer;
global $con;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$CursorFeld='';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZLA','%');
$TextKonserven[]=array('ZSZ','ZSZ_SORTIMENT');
$TextKonserven[]=array('ZSZ','Nummer');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Liste','lst_JaNein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$Recht10001=awisBenutzerRecht($con,10001);

echo "<br>";
echo "<form name=frmSuche method=post action=./zukaufartikel_Main.php?cmdAktion=Details>";

/**********************************************
* * Eingabemaske
***********************************************/

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ARTIKELNUMMER'].':',190);
awis_FORM_Erstelle_TextFeld('*ZLA_ARTIKELNUMMER','',20,200,true);
$CursorFeld='sucZLA_ARTIKELNUMMER';
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_BEZEICHNUNG'].':',190);
awis_FORM_Erstelle_TextFeld('*ZLA_BEZEICHNUNG','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLI_KEY'].':',190);
$SQL = 'SELECT ZLI_KEY, ZLI_BEZEICHNUNG FROM ZUKAUFLIEFERANTEN';
$SQL .= ' ORDER BY ZLI_BEZEICHNUNG';
awis_FORM_Erstelle_SelectFeld('*ZLA_ZLI_KEY','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_ZLH_KEY'].':',190);
$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
awis_FORM_Erstelle_SelectFeld('*ZLA_ZLH_KEY','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZSZ']['ZSZ_SORTIMENT'].':',190);
$SQL = 'SELECT DISTINCT ZSZ_SORTIMENT FROM ZUKAUFSORTIMENTZUORDNUNGEN';
$SQL .= ' ORDER BY ZSZ_SORTIMENT';
awis_FORM_Erstelle_SelectFeld('*ZSZ_SORTIMENT','',200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',190);
awis_FORM_Erstelle_TextFeld('*ZLA_AST_ATUNR','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['Nummer'].':',190);
awis_FORM_Erstelle_TextFeld('*Nummer','',20,200,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_TAUSCHTEIL'].':',190);
$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
awis_FORM_Erstelle_SelectFeld('*ZLA_TAUSCHTEIL','',200,true,$con,'','~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Daten);
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();


awis_FORM_SchaltflaechenStart();
	// Zur�ck zum Men�
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Recht10001&4) == 4)		// Hinzuf�gen erlaubt?
{
	awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

awis_FORM_SchaltflaechenEnde();


if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>