<?php
// Gantt hour example
require_once("jpgraph/jpgraph.php");
require_once("jpgraph/jpgraph_gantt.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $awisRSZeilen;

// $con = awisLogon();  <- Geht nicht wegen der Kommentare in der Funktion!
$db = "";
$awis_link = "";
$endzeit = date("Y-m-d") . " 12:00";
$startzeit = date("Y-m-d", time() - 86400) . " 12:00";
$laufzeit = 1;

$con = @OCILogon("awis","IlOuLua",awisDBServer());
//$con = awisLogon();

if (!empty($_GET["txtDB"]))
{
	$awis_link = "@" . strtolower($_GET["txtDB"]) . ".atu.de";
	$db = $_GET["txtDB"];
}

if (!empty($_GET["startzeit"]))
{
	$startzeit = $_GET["startzeit"];
}
if (!empty($_GET["endzeit"]))
{
	$endzeit = $_GET["endzeit"];
}
if (!empty($_GET["laufzeit"]))
{
	$laufzeit = $_GET["laufzeit"];
}

/* Post-Variablen funzen nicht!!!!!!!!!!!!!!!!!!!!!!!!!
if (!empty($HTTP_POST_VARS[txtDB]))
{
	$awis_link = "@" . strtolower($HTTP_POST_VARS[txtDB]) . ".atu.de";
	$db = $HTTP_POST_VARS["txtDB"];
}
*/

if($con)
{
//	$sql = "select jou_argument, to_char(jou_start, 'RRMMDD HH24:MI:SS') Anfang, to_char(jou_ende, 'RRRR-MM-DD HH24:MI:SS') Ende, round((jou_ende - jou_start) * 3600 * 24, 0) Dauer from journal ";
	$sql = "select decode(jou_argument, null, jou_funktion, jou_argument) BEZ, to_char(jou_start, 'RRRR-MM-DD HH24:MI:SS') Anfang, to_char(jou_ende, 'RRRR-MM-DD HH24:MI:SS') Ende, round((jou_ende - jou_start) * 3600 * 24, 0) Dauer, JOU_FEHLERCODE, JOU_ANZSTATEMENT from journal" . $awis_link . " ";
	$sql .= "where jou_start > to_date('" . $startzeit . "', 'RRRR-MM-DD HH24:MI') ";
	$sql .= "and jou_start < to_date('" . $endzeit . "', 'RRRR-MM-DD HH24:MI') ";
//  	$sql .= "and jou_start < sysdate - 2 ";
	$sql .= "and not (jou_argument like 'k%' and jou_funktion = 'Datei-Import') ";
	$sql .= "and round((jou_ende - jou_start) * 3600 * 24, 0) >= " . $laufzeit . " ";
	$sql .= "order by jou_start";

	$rsDaten = awisOpenRecordset($con, $sql);
	$rsDatenZeilen = $awisRSZeilen;
	
	awisLogoff($con);			// Wieder abmelden

}

$gJpgBrandTiming=true;			// Laufzeit mit ausgeben
$graph = new GanttGraph();
$graph->SetMarginColor('blue:1.7');
$graph->SetColor('white');
// $endzeit = date("Y-m-d", time() - 186400) . " 12:00";
// $startzeit = date("Y-m-d", time() -286400) . " 12:00";
$graph->SetDateRange($startzeit,$endzeit);
// $graph->SetDateRange('2005-03-08 12:00','2005-03-09 12:00');

$graph->SetBackgroundGradient('navy','white',GRAD_HOR,BGRAD_MARGIN);
$graph->scale->hour->SetBackgroundColor('lightyellow:1.5');
$graph->scale->hour->SetFont(FF_FONT1);
$graph->scale->day->SetBackgroundColor('lightyellow:1.5');
$graph->scale->day->SetFont(FF_FONT1,FS_BOLD);

$graph->title->Set("Zeitlicher Ablauf der Importe (" . $db . ") als Image-Map");
$graph->title->SetColor('white');
$graph->subtitle->Set("Alle Jobs mit Laufzeit >= " . $laufzeit . " Sekunden von " . $startzeit . " bis " . $endzeit);
$graph->subtitle->SetColor('white');
//$graph->title->SetFont(FF_VERDANA,FS_BOLD,14);

$graph->ShowHeaders(GANTT_HDAY | GANTT_HHOUR);

// $graph->scale->week->SetStyle(WEEKSTYLE_FIRSTDAY);
// $graph->scale->week->SetFont(FF_FONT1);
$graph->scale->hour->SetIntervall(1);
$graph->scale->SetDateLocale('de_DE');

$graph->scale->hour->SetStyle(HOURSTYLE_HM24);
$graph->scale->day->SetStyle(DAYSTYLE_SHORTDAYDATE3);
$graph->scale->UseWeekendBackground(false);

for($DS=0;$DS<$rsDatenZeilen;$DS++)
//for($DS=0;$DS<35;$DS++)
{
	$bar = new GanttBar($DS,$rsDaten["BEZ"][$DS],$rsDaten["ANFANG"][$DS],$rsDaten["ENDE"][$DS],$rsDaten["DAUER"][$DS] . " (" . $rsDaten["JOU_ANZSTATEMENT"][$DS] . ")",10);
	$bar->SetPattern(BAND_RDIAG,"yellow");
	$bar->SetFillColor("red");
//	$bar->progress->Set(0.5);
	if ($rsDaten["JOU_FEHLERCODE"][$DS] == "0")
	{
		$bar->SetCSIMTarget('/admin/journal.php','OK ' . substr($rsDaten["ANFANG"][$DS], 7) . '-' . substr($rsDaten["ENDE"][$DS], 7));
		$bar->title->SetCSIMTarget('/admin/journal.php','OK ' . substr($rsDaten["ANFANG"][$DS], 7) . '-' . substr($rsDaten["ENDE"][$DS], 7));
//		$bar->caption->SetColor('darkgreen');
		
	}
	else
	{
		$bar->SetCSIMTarget('/admin/journal.php','Fehler ' . substr($rsDaten["ANFANG"][$DS], 7) . '-' . substr($rsDaten["ENDE"][$DS], 7));
		$bar->title->SetCSIMTarget('/admin/journal.php','Fehler ' . substr($rsDaten["ANFANG"][$DS], 7) . '-' . substr($rsDaten["ENDE"][$DS], 7));
		$bar->caption->SetColor('red');
/*		$bar->rightMark->Show();
		$bar->rightMark->title->Set("?");
		$bar->rightMark->SetType(MARK_FILLEDCIRCLE);
		$bar->rightMark->SetWidth(10);
		$bar->rightMark->SetColor("red");
		$bar->rightMark->SetFillColor("red");
//		$bar->rightMark->title->SetFont(FF_ARIAL,FS_BOLD,12);
		$bar->rightMark->title->SetColor("white");
*/
	}
//	$bar->SetShadow(true,"gray");
	$graph->Add($bar);
}

   // Setup a horizontal grid
$graph->hgrid->Show();
//$graph->hgrid-> line->SetColor('lightblue' );
//$graph->hgrid->line->Show(false);
$graph->hgrid->SetRowFillColor( 'darkblue@0.95');


$graph->StrokeCSIM(basename(__FILE__));
?>