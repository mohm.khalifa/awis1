<?php
global $AWISBenutzer;
global $CursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDokumente.php');

try 
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('DOC','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','Uploaddatei');
    $TextKonserven[]=array('Wort','DateiOeffnen');
    $TextKonserven[]=array('Wort','KeineDatenVorhanden');
    $TextKonserven[]=array('Wort','Typ');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Wort','DateiLoeschen');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht3705 = $AWISBenutzer->HatDasRecht(3705);
    if(($Recht3705&1)==0)
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
    }
    
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $BindeVariablen=array();
    $BindeVariablen['var_N0_doz_xxx_key']=intval('0' . $AWIS_KEY1);
    
    $SQL = 'SELECT Dokumente.*, xbn_name FROM Dokumente';
    $SQL .= ' INNER JOIN dokumentZuordnungen ON doc_key = doz_doc_key';
    $SQL .= ' INNER JOIN Benutzer ON doc_xbn_key = xbn_key';
    $SQL .= ' WHERE doz_xxx_key=:var_N0_doz_xxx_key';
    $SQL .= ' AND doz_xxx_kuerzel=\'ADR\'';
    
    if(isset($_GET['DOC_KEY']))
    {
        $BindeVariablen['var_N0_doc_key']=intval('0' . $_GET['DOC_KEY']);
        $SQL .= ' AND doc_key=:var_N0_doc_key';
    }
    
    $SQL .= ' ORDER BY doc_datum desc';
    $rsDOC = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
    $rsDOCZeilen = $rsDOC->AnzahlDatensaetze();
    
    
    
    if((($rsDOCZeilen >= 1 or isset($_GET['DOCListe'])) and !isset($_GET['DOC_KEY'])))
    {      
        $Form->Formular_Start();
        $Form->ZeileStart();
    
        if(($Recht3705&6)>0)
        {
            $Icons[] = array('new','./adressen_Main.php?cmdAktion=Details&Seite=Dokumente&DOC_KEY=0');
            $Form->Erstelle_ListeIcons($Icons,38,-1);
        }
    
        $Form->Erstelle_HiddenFeld('DOC_KEY',$rsDOC->FeldInhalt('DOC_KEY'));
        
        $Link = './adressen_Main.php?cmdAktion=Details&Seite=Dokumente';
        $Link .= '&DOCSort=DOC_DATUM'.((isset($_GET['DOCSort']) AND ($_GET['DOCSort']=='DOC_DATUM'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_DATUM'],150,'',$Link);
    
        $Link = './adressen_Main.php?cmdAktion=Details&Seite=Dokumente';
        $Link .= '&DOCSort=DOC_BEZEICHNUNG'.((isset($_GET['DOCSort']) AND ($_GET['DOCSort']=='DOC_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'],550,'',$Link);
    
        $Link = './adressen_Main.php?cmdAktion=Details&Seite=Dokumente';
        $Link .= '&DOCSort=XBN_NAME'.((isset($_GET['DOCSort']) AND ($_GET['DOCSort']=='XBN_NAME'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_XBN_KEY'],180,'',$Link);
    
        $Form->Erstelle_Liste_Ueberschrift('',30,'',$Link);
    
        $Form->ZeileEnde();
    
        for($DOCZeile=0;$DOCZeile<$rsDOCZeilen;$DOCZeile++)
        {
            $Link='';
            $Form->ZeileStart();
            $Icons = array();
            if(($Recht3705&6)>0)	// Ändernrecht
            {
                $Icons[] = array('edit','./adressen_Main.php?cmdAktion=Details&Seite=Dokumente&DOC_KEY='.$rsDOC->FeldInhalt('DOC_KEY'));
    			$Icons[] = array('delete','./adressen_Main.php?cmdAktion=Details&Seite=Dokumente&Del='.$rsDOC->FeldInhalt('DOC_KEY'));
            }
            $Form->Erstelle_ListeIcons($Icons,38,($DOCZeile%2));
            $Form->Erstelle_ListenFeld('#DOC_DATUM',$rsDOC->FeldInhalt('DOC_DATUM'),20,150,false,($DOCZeile%2),'','','D');
            $Form->Erstelle_ListenFeld('#DOC_BEZEICHNUNG',$rsDOC->FeldInhalt('DOC_BEZEICHNUNG'),20,550,false,($DOCZeile%2),'','','T');
            $Form->Erstelle_ListenFeld('#XBN_NAME',$rsDOC->FeldInhalt('XBN_NAME'),20,180,false,($DOCZeile%2),'','','T');
            $Link = '/dokumentanzeigen.php?doctab=ADR&dockey='.$rsDOC->FeldInhalt('DOC_KEY').'';
    		$Form->Erstelle_ListenBild('href','DOC',$Link,'/bilder/dateioeffnen_klein.png',$AWISSprachKonserven['Wort']['DateiOeffnen'],($DOCZeile%2),'',17,17,30);
            $Form->ZeileEnde();
            
            $rsDOC->DSWeiter();
        }
        $Form->Formular_Ende();
    }
    else 		// Einer oder keiner
    {
        $Form->Formular_Start();
        $AWIS_KEY2 = $rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_KEY'):0;
        
        echo"<br><br>";
        var_dump($_POST);
        echo"<br><br>";
        var_dump($SQL);
        echo"<br><br>";
        var_dump($AWIS_KEY2);
        
        // Infozeile zusammenbauen
        $Felder = array();
	    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Details&Seite=Dokumente&DOCListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_USER'):''));
	    $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_USERDAT'):''));
	    $Form->InfoZeile($Felder,'');

	    if($AWIS_KEY2==0)
	    {
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_DATUM'].':',150);
	        $Form->Erstelle_TextFeld('DOC_DATUM',$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_DATUM'):'',10,200,true,'','','','D','','',$Form->PruefeDatum(date('d.m.Y')));
	        $Form->ZeileEnde();
	        
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'].':',150);
	        $Form->Erstelle_TextFeld('DOC_BEZEICHNUNG',$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_BEZEICHNUNG'):'',40,400,true);
	        $Form->ZeileEnde();

            $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'].':',150);
	        $Form->Erstelle_DateiUpload('DOC',200,50);
	        $Form->ZeileEnde();

	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEMERKUNG'].':',150);
	        $Form->Erstelle_Textarea('DOC_BEMERKUNG',$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_BEMERKUNG'):'',400,80,3,true);
	        $Form->ZeileEnde();
	    }
	    elseif(isset($AWIS_KEY2))
	    {
	        $Link='';
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_DATUM'].':',150);
	        $Form->Erstelle_TextFeld('DOC_DATUM',$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_DATUM'):'',10,200,false,'','','','D','','',$Form->PruefeDatum(date('d.m.Y')));
	        $Form->ZeileEnde();
	         
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'].':',150);
	        $Form->Erstelle_TextFeld('DOC_BEZEICHNUNG',$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_BEZEICHNUNG'):'',40,400,false);
	        $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'].':',150);
            $Link = '/dokumentanzeigen.php?doctab=ADR&dockey='.$rsDOC->FeldInhalt('DOC_KEY').'';
            $Form->Erstelle_ListenBild('href','DOC',$Link,'/bilder/dateioeffnen_klein.png',$AWISSprachKonserven['Wort']['DateiOeffnen'],0,'',17,17,50);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Typ'].':',100);
            $Form->Erstelle_TextLabel($rsDOC->FeldInhalt('DOC_ERWEITERUNG'),100);
            $Form->ZeileEnde();

	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEMERKUNG'].':',150);
	        $Form->Erstelle_Textarea('DOC_BEMERKUNG',$rsDOC->FeldInhalt('DOC_KEY')!=''?$rsDOC->FeldInhalt('DOC_BEMERKUNG'):'',400,80,3,false);
	        $Form->ZeileEnde();
	    }
	    $Form->Formular_Ende();
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>