<?php
require_once('abgleich_funktionen.inc');
require_once('awisMailer.inc');
require_once('awisBerichte.inc');

try
{
    
    $AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();    
    $TAG = new awis_abgleich_funktionen();
    
	$Recht23010 = $AWISBenutzer->HatDasRecht(23010);
    if (($Recht23010&2) < 2)
	{
	    $Form->Fehler_KeineRechte();
	}
    
    $Param = array();
	if(isset($_GET['ID']))                  // Parameter kommen base64 kodiert
	{
		// Parameter werden durch & getrennt
		$ParameterListe = explode('&',base64_decode($_GET['ID']));
        foreach($ParameterListe AS $Parameter)
		{
			// Jeder Parameter hat das Format <Param>=<Operator>~<Wert>
			$Parameter = explode('=',$Parameter);
			$Param[$Parameter[0]]=(isset($Parameter[1])?urldecode($Parameter[1]):'');
		}
		//var_dump($Param);
		
		apache_setenv('no-gzip', '1');
		echo str_repeat(" ",4096)."<pre></pre>";
		echo "<br><center><input type=image title='Scheduler-Job' src=/bilder/atulogo_neu.png name=cmdLogo></center>";
		echo '<br><center><h2>E-Mails werden versendet ...</h2></center>';
		echo "<br><center><input type=image title='Scheduler-Job' src=/bilder/loading_bar.gif name=cmdLoading></center>";
		echo "<br><center>Working...</center><p>";
		ob_flush();
		flush();
		usleep(50000);
		
		$TAG->MailSenden($Param, $AWISBenutzer);
	}
    else
    {
        throw new Exception('Fehler bei Parameterübergabe (Mail mit PDF-Anhang).');
    }
}
catch(Exception $ex)
{
    $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(),'MELDEN',6,'201210160001');
}        
?>