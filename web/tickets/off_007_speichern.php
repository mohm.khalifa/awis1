<?php
/**
 * Formular: 07 - Softwareanforderung
 * @author Sacha Kerres
 *
 */

$FormularKennung = '007';
$DetailBlock = '';
$DetailBlock110 = '';

$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'txt_*');
$TextFormular = $Form->LadeTexte($TextKonserven);
$ToolTipps = $Form->LadeTexte($TextKonserven, $AWISBenutzer->BenutzerSprache());

$DetailFelder = $Form->NameInArray($_POST, 'txtDETAILS', 2, 0);
$Form->DebugAusgabe(3,$DetailFelder);
$OT_EOL = "\r\n";

$Form->DebugAusgabe(3,$_POST);

// Ist-Zustand
if($_POST['txtIstZustand']!='')
{
    $DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_istzustand'].$OT_EOL;
    $DetailBlock .= $OT_EOL.$_POST['txtIstZustand'].$OT_EOL;
}

// Soll-Zustand
$DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_sollzustand'].$OT_EOL;
$DetailBlock .= $OT_EOL.$_POST['txtSollZustand'].$OT_EOL;

// Lösungsvoschlag
$DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_loesungsvorschlag'].$OT_EOL;
$DetailBlock .= $OT_EOL.$_POST['txtLoesungsvorschlag'].$OT_EOL;

// Nutzen
if($_POST['txtIstZustand']!='')
{
    $DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_nutzen_vorteil'].$OT_EOL;
    $DetailBlock .= $OT_EOL.$_POST['txtNutzenVorteil'].$OT_EOL;
}


//*********************************************************
//* Bemerkung
//*********************************************************
$Block='900';
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']!='')
{
    $DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_allgbemerkungen'].$OT_EOL;
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'].$OT_EOL;
}


//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']!='') {

    $Namelist = '';
    foreach($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn'] as $KEY)
    {
        $Namelist .= ';'.$KEY;
    }
    $Namelist = substr($Namelist,1);
    $DetailBlock110 .= $Namelist;

    $DetailBlock .= $OT_EOL.$TextFormular['OTF_FRM_'.$FormularKennung]['txt_benachrichtigungan'].$OT_EOL;
    $DetailBlock .= $Namelist. $OT_EOL;
}


?>