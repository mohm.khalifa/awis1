<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('AVK', '%');
$TextKonserven[] = array('IMQ', 'IMQ_IMPORTQUELLE');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'wrd_Filiale');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

try {

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MitReklInfo = $AWISBenutzer->ParameterLesen('ReklamationsInfoLieferanten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht405 = $AWISBenutzer->HatDasRecht(405);            // Recht f�r die Lieferantenartikel
    if ($Recht405 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $SQL = 'SELECT *';
    $SQL .= ' FROM artikelstammVKAenderungen';
    $SQL .= ' INNER JOIN artikelstamm ON AST_ATUNR = AVK_AST_ATUNR';
    $SQL .= ' LEFT OUTER JOIN Importquellen ON AVK_IMQ_ID = IMQ_ID';
    $SQL .= ' WHERE AST_KEY = 0' . $AWIS_KEY1;
    $SQL .= ' AND AVK_LAN_CODE = \'DE\'';

    if (isset($_GET['AVKSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['AVKSort']);
    } else {
        $SQL .= ' ORDER BY AVK_DATUM DESC';
    }

    $Form->DebugAusgabe(1, $SQL);
    $rsAVK = $DB->RecordSetOeffnen($SQL);

    if (!isset($_GET['AVK_KEY']))                    // Liste anzeigen
    {
        $Form->Formular_Start();
        $Form->ZeileStart();

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=AVK_DATUM';
        $Link .= '&AVKSort=AVK_DATUM' . ((isset($_GET['AVKSort']) AND ($_GET['AVKSort'] == 'AVK_DATUM'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AVK']['AVK_DATUM'], 120, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=AVK_USER';
        $Link .= '&AVKSort=AVK_USER' . ((isset($_GET['AVKSort']) AND ($_GET['AVKSort'] == 'AVK_USER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AVK']['AVK_USER'], 250, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=VK';
        $Link .= '&AVKSort=AVK_PREISALT' . ((isset($_GET['AVKSort']) AND ($_GET['AVKSort'] == 'AVK_PREISALT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AVK']['AVK_PREISALT'], 150, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=AVK_PREISNEU';
        $Link .= '&AVKSort=AVK_PREISNEU' . ((isset($_GET['AVKSort']) AND ($_GET['AVKSort'] == 'AVK_PREISNEU'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AVK']['AVK_PREISNEU'], 150, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=IMQ_IMPORTQUELLE';
        $Link .= '&AVKSort=IMQ_IMPORTQUELLE' . ((isset($_GET['AVKSort']) AND ($_GET['AVKSort'] == 'IMQ_IMPORTQUELLE'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['IMQ']['IMQ_IMPORTQUELLE'], 150, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsAVK->EOF()) {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('*AVK_DATUM', $rsAVK->FeldInhalt('AVK_DATUM'), 10, 120, false, ($DS % 2), '', '', 'D', 'C');
            $Form->Erstelle_ListenFeld('*AVK_USER', $rsAVK->FeldInhalt('AVK_USER'), 10, 250, false, ($DS % 2), '', '', 'T', 'C');
            $Form->Erstelle_ListenFeld('*AVK_PREISALT', $rsAVK->FeldInhalt('AVK_PREISALT'), 10, 150, false, ($DS % 2), '', '', 'N2', 'C');
            $Form->Erstelle_ListenFeld('*AVK_PREISNEU', $rsAVK->FeldInhalt('AVK_PREISNEU'), 10, 150, false, ($DS % 2), '', '', 'N2', 'C');
            $Form->Erstelle_ListenFeld('*IMQ_IMPORTQUELLE', $rsAVK->FeldInhalt('IMQ_IMPORTQUELLE'), 10, 150, false, ($DS % 2), '', '', 'T', 'L');

            $Form->ZeileEnde();
            $DS++;

            $rsAVK->DSWeiter();
        }
        $Form->Formular_Ende();
    } else        // Einer oder keiner
    {
        $Form->Formular_Start();

        echo '<input name=txtAVK_KEY type=hidden value=0' . ($rsAVK->FeldInhalt('AVK_KEY') == ''?$rsAVK->FeldInhalt('AVK_KEY'):'') . '>';
        echo '<input name=txtAVK_IMQ_ID type=hidden value=' . ($rsAVK->FeldInhalt('AVK_IMQ_ID') == ''?$rsAVK->FeldInhalt('AVK_IMQ_ID'):'4') . '>';

        $AWIS_KEY2 = $rsAVK->FeldInhalt('AVK_KEY');
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=VK&AVKListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAVK->FeldInhalt('AVK_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAVK->FeldInhalt('AVK_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();

        $EditModus = ($rsAVK->FeldInhalt('AVK_USER') != ''?($rsAVK->FeldInhalt('AVK_USER') != 'WWS'?($Recht405 & 6):false):($Recht405 & 6));

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AVK']['AVK_NUMMER'] . ':', 150);
        $Form->Erstelle_TextFeld('AVK_NUMMER', $rsAVK->FeldInhalt('AVK_NUMMER'), 20, 200, $EditModus);
        $AWISCursorPosition = ($EditModus?'txtAVK_NUMMER':$AWISCursorPosition);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AVK']['AVK_HER_ID'] . ':', 150);
        $SQL = 'SELECT HER_ID, HER_BEZEICHNUNG FROM Hersteller WHERE HER_TYP = 1 ORDER BY HER_BEZEICHNUNG';
        $Form->Erstelle_SelectFeld('AVK_HER_ID', $rsAVK->FeldInhalt('AVK_HER_ID'), 200, $EditModus, $SQL, '0~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AVK']['AVK_MUSTER'] . ':', 150);
        $Form->Erstelle_SelectFeld('AVK_MUSTER', $rsAVK->FeldInhalt('AVK_MUSTER'), 70, $EditModus, '', '', '0', '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AVK']['AVK_BEMERKUNG'] . ':', 150);
        $Form->Erstelle_Textarea('AVK_BEMERKUNG', $rsAVK->FeldInhalt('AVK_BEMERKUNG'), 200, 100, 3, $EditModus);
        $Form->ZeileEnde();

        $SQL = '';

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201320");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901201321");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>