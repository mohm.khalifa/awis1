<?php
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con,153);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Listen Telefonverzeichnis',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}
$KatRechte = "0" . awisBenutzerRecht($con,151);		// Kategorien f�r Telefon

/**********************************
*
* Eingabeformular f�r die Parameter
*
**********************************/
if(!isset($_POST['txtKategorie']))
{
	echo '<Form name=frmTelefon method=post action=./telefon_Main.php?cmdAktion=Ausdrucke>';
	echo '<table border=1 id=SuchMaske>';

	echo '<tr><td colspan=2 id=FeldBez>Listenkriterien</td></tr>';
	echo "<tr><td  width=180>K<u>a</u>tegorie:</td>";
	echo "<td ><select name=txtKategorie accesskey='a' size=1 tabindex=10>";
	if($KatRechte & 8192)
	{
		echo "<option value=-1>Alle</option>";
	}
	
	$KatRechte = "0" . awisBenutzerRecht($con,151);
	$rsKategorien=awisOpenRecordset($con,"SELECT * FROM KontakteKategorien WHERE BITAND(KKA_KZG_ID,$KatRechte)=KKA_KZG_ID ORDER BY KKA_BEZEICHNUNG");
	$rsKategorienZeilen = $awisRSZeilen;
	
	for($i=0;$i<$rsKategorienZeilen;$i++)
	{
		echo "<option " . ($i==0?"selected":"") . " value=" . $rsKategorien["KKA_KEY"][$i] . ">" . $rsKategorien["KKA_BEZEICHNUNG"][$i] . "</option>";
	}
	
	echo "</select></td></tr>";
	unset($rsKategorien);		// Speicher freigeben
	
		// Bereich 
		
	$rsBereiche=awisOpenRecordset($con,"SELECT * FROM KontakteBereiche WHERE BITAND(KBE_KZG_ID,$KatRechte)=KBE_KZG_ID ORDER BY KBE_BEZEICHNUNG");
	$rsBereicheZeilen = $awisRSZeilen;
	
	echo "<tr><td width=180>B<u>e</u>reich:</td>";
	echo "<td ><select name=txtBereich accesskey='e' size=1 tabindex=10>";
	echo "<option value=-1 selected>Alle</option>";
	
	for($i=0;$i<$rsBereicheZeilen;$i++)
	{
		echo "<option value=" . $rsBereiche["KBE_KEY"][$i] . ">" . $rsBereiche["KBE_BEZEICHNUNG"][$i] . "</option>";
	}
	echo "</select></td></tr>";
	unset($rsBereiche);		// Speicher freigeben
	
	// Abteilungen
	
	echo "<tr><td width=180>ATU A<u>b</u>teilung:</td>";
	echo "<td ><select name=txtAbteilung accesskey='b' size=1 tabindex=10>";
	echo "<option value=-1>Alle</option>";
	
	$rsAbteilungen=awisOpenRecordset($con,"SELECT * FROM KontakteAbteilungen WHERE BITAND(KAB_KZG_ID,$KatRechte)=KAB_KZG_ID ORDER BY KAB_ABTEILUNG");
	$rsAbteilungenZeilen = $awisRSZeilen;
	for($i=0;$i<$rsAbteilungenZeilen;$i++)
	{
		echo "<option value=" . $rsAbteilungen["KAB_KEY"][$i] . ">" . $rsAbteilungen["KAB_ABTEILUNG"][$i] . "</option>";
	}
	echo "</select></td></tr>";
	unset($rsAbteilungen);

		// Geb�ude
	echo "<tr><td width=180>ATU <u>G</u>eb�ude:</td>";
	echo "<td ><select name=txtGebaeude accesskey='g' size=1 tabindex=50>";
	echo "<option value=-1 checked>Alle</option>";
	
	$rsGebaeude=awisOpenRecordset($con,"SELECT * FROM Gebaeude ORDER BY GEB_BEZEICHNUNG");
	$rsGebaeudeZeilen = $awisRSZeilen;
	for($i=0;$i<$rsGebaeudeZeilen;$i++)
	{
		echo "<option value=" . $rsGebaeude["GEB_ID"][$i] . ">" . $rsGebaeude["GEB_BEZEICHNUNG"][$i] . "</option>";
	}
	echo "</select></tr>";

			// Zusatzkriterien
	echo '<tr><td colspan=2 id=FeldBez>Listenart</td></tr>';
	echo '<tr><td >Nur �ffentliche:</td><td ><input type=checkbox name=chkOeffentliche value=1 checked></td></tr>';
	echo '<tr><td >Filialliste:</td><td ><input type=checkbox name=chkFilialen value=2></td></tr>';

		// Listen
		//	1: Kurzwahlliste
	echo "<tr><td width=180><u>L</u>iste:</td>";
	echo "<td ><select name=txtListe accesskey='l' size=1 tabindex=99>";

	if(($RechteStufe&1)==1)
	{
		echo "<option value=1 checked>Mitarbeiterliste</option>";
	}
	if(($RechteStufe&2)==2)
	{
		echo "<option value=2 >Abteilungsliste</option>";
	}
	if(($RechteStufe&4)==4)
	{
		echo "<option value=3 >Branchenliste</option>";
	}
	if(($RechteStufe&1)==1)
	{
		echo "<option value=4 >E-Mail Liste Mitarbeiter</option>";
	}
	if(($RechteStufe&1)==1)
	{
		echo "<option value=5 >Handy Liste Mitarbeiter</option>";
	}
	
	echo "</select></td></tr>";
	
	echo '</table>';
	
	echo "<br>&nbsp;<input accesskey=w tabindex=95 type=image src=/bilder/eingabe_ok.png title='Suche starten (Alt+w)' name=cmdAktion value=Ausdrucke>";

	echo '</Form>';

}	// Ende Eingabeformular
else 
{
	include './telefon_PDF_' . $_POST['txtListe'] . '.php';
}



?>
	
