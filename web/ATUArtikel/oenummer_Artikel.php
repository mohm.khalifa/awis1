<?
/****************************************************************************************************
*
* 	Artikel-Liste f�r OENummern
*
* 	Die Daten werden in einer Liste dargestellt und der Bearbeitungsmodus aktiviert
*
* 	Autor: 	Sacha Kerres
* 	Datum:	Dez. 2003
*
****************************************************************************************************/
// Variablen
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $con;
global $LARNr;
global $AWISBenutzer;

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$OENRechtestufe = awisBenutzerRecht($con, 403) ;

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

		//********************************************************************
		// Aktualisieren gew�hlt?
		// Block muss identisch sein zu artikel_Artikel.php !!!!
		//********************************************************************
if(isset($_POST['cmdSuche_x']))
{
	$Param = $_POST['txtAuswahlSpeichern'];
	$Param .= ";" . $_POST['txtAST_ATUNR'];
	$Param .= ";" . $_POST['txtAST_BEZEICHNUNG'];
	$Param .= ";" . $_POST['txtTEI_SUCH'];
	$Param .= ";" . $_POST['txtTEI_SUCH_EAN'];			// Suche nach EAN-Nummer
	$Param .= ";" . $_POST['txtTEI_SUCH_GNR'];			// Suche nach Gebrauchsmuster
	$Param .= ";" . $_POST['txtTEI_SUCH_LAR'];			// Suche nach Lieferantenartikel
	$Param .= ";" . $_POST['txtTEI_SUCH_Exakt'];		// Exakte Suche
	$Param .= ";" . $_POST['txtTEI_SUCH_LAS'];			// Suche nach Lieferantenartikel-Sets
	$Param .= ";" . $_POST['txtTEI_SUCH_OEN'];			// Suche nach OE-Nummer
	$Param .= ";" . $_POST['txtKommentar'];				// Kommentar im Artikel
	$Param .= ";" . $_POST['txtTEI_SUCH_XXX'];			// Suche nach beliebiger Nummer
	$Param .= ";" . $_POST['txtWUG_KEY'];				// Suche nach Warenuntergruppe
	$Param .= ";" . $_POST['txtWGR_KEY'];				// Suche nach Warengruppe
	$Param .= ";" . $_POST['txtOEPreise'];				// Immer OE-Preise zeigen

	awis_BenutzerParameterSpeichern($con, "ArtikelSuche" , $AWISBenutzer->BenutzerName() , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerOEN", $AWISBenutzer->BenutzerName(), "");

	$Param = explode(";", $Param);
	$Artikel = array('','');
}
else		// Gespeicherte Parameter verwenden
{
	$Param = explode(";", awis_BenutzerParameter($con, "ArtikelSuche", $AWISBenutzer->BenutzerName()));
	if(isset($_GET['Liste']) && $_GET['Liste']=='True')
	{
		$Artikel=array('','');
		$Param[9]='on';
	}
	else
	{
    	$Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $AWISBenutzer->BenutzerName()));
	}
}
	// SQL Anweisung erstellen
$SQL = "SELECT * FROM OENummern, Hersteller ";

$DSAnz=-1;		// Anzahl DS in der Liste -> wird verwendet, ob Liste oder Einzeldarstellung zu entscheiden

//***************************************************
// Ein Artikel ausgew�hlt
//***************************************************
if(isset($_GET["OENKEY"]) OR $Artikel[0]!='' OR isset($_GET["OEN_NUMMER"]))
{

	if(isset($_GET["OENKEY"]))
	{
		$OENKEY = $_GET["OENKEY"];
//		awis_BenutzerParameterSpeichern($con, "AktuellerOEN", $AWISBenutzer->BenutzerName(), $OENKEY);
		$Bedingung = " AND OEN_KEY = " . $OENKEY;
	}
	elseif(isset($_GET["OEN_NUMMER"]))
	{
		$Bedingung = " AND OEN_NUMMER = '" . $_GET["OEN_NUMMER"] . "'";
	}
	else
	{
		$OENKEY=$Artikel[0];
		$Bedingung = " AND OEN_KEY = " . $OENKEY;
	}

//**********************************************************
// Daten speichern
//**********************************************************

	if(awis_NameInArray($_POST, "cmdSpeichern")!='')
	{

		$SQL = "UPDATE OENummern SET ";
		$SQL .= "OEN_HER_ID = '" . $_POST['txtOEN_HER_ID'] . "'";
		$SQL .= ",OEN_BEMERKUNG= '" . $_POST['txtOEN_BEMERKUNG'] . "'";
		$SQL .= ",OEN_MUSTER= '" . $_POST['txtOEN_MUSTER'] . "'";
		$SQL .= ",OEN_IMQ_ID=4";
		$SQL .= ",OEN_USER='" . $AWISBenutzer->BenutzerName() . "'";
		$SQL .= ",OEN_USERDAT=SYSDATE";
		$SQL .= " WHERE OEN_KEY=0" . $_POST['txtOEN_KEY'];

		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("oenummer_Artikel.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}  //


}
													//***********************************************************
elseif(!isset($_GET["OENKEY"]) OR $_GET['Liste']=='True')						// Artikel suchen (Liste anzeigen, oder einen)
													//***********************************************************
{
	$DSAnz=0;		// Insgesamt gelesene Zeilen

	$Bedingung = "";
	if(($Param[11]=='on' OR $Param[9]=='on') AND $Param[3] != '')	// Beliebige oder OEN Suche
	{
		if($Param[7]=='on')			// NICHT Exakte suche
		{
			$Bedingung .= " AND OEN_SUCHNUMMER " . awisLIKEoderIST($Param[3],TRUE,FALSE,false,2) . "";
		}
		else
		{
			$Bedingung .= " AND UPPER(OEN_NUMMER) " . awisLIKEoderIST($Param[3],TRUE) . "";
		}
		//$SQL .= ', TeileInfos';
		//$Bedingung .= ' AND OEN_Key = TEI_Key2(+)';
	}

	if($Param[1]!='')		// ATU-Nummer
	{
		$SQL .= ', TeileInfos';
		$Bedingung .= 'AND TEI_Wert1 '. awisLIKEoderIST($Param[1],1,1,0,1) . ' AND TEI_Key2 = OEN_KEY ';
	}

	if($Bedingung == '')
	{
		awisLogoff($con);
		die('<span class=HinweisText>Mit diesen Kriterien ist keine Suche in OE Nummern m�glich</span>');
	}

	$SQL .= " WHERE OEN_HER_ID = HER_ID(+) " . $Bedingung;
	$SQL .= " ORDER BY OEN_NUMMER";

	// Max. Datenanzahl einschr�nken
	$SQL = 'SELECT * FROM (' . $SQL . ') WHERE ROWNUM <= 0' . ($MaxDSAnzahl+1);

	awis_Debug(1,$SQL);
	$rsArtikel = awisOpenRecordset($con, $SQL);
	$rsArtikelZeilen = $awisRSZeilen;
	if($rsArtikelZeilen == 1)
	{
		$DSAnz=-1;			// Einzelanzeige
		$OENKEY = $rsArtikel["OEN_KEY"][0];
	}
	else
	{
		echo "<table width=100% border=1>";

		echo "\n<tr>";	// �berschrift
		echo "\n<td id=FeldBez>OE-Nummer</td>";
		echo "\n<td id=FeldBez>Hersteller</td>";
		echo "\n<td id=FeldBez>Bemerkung</td>";
		echo "\n<td id=FeldBez>ATU-Nr</td>";
		echo "</tr>\n";

 		for($OENNr=0;$OENNr<$rsArtikelZeilen;$OENNr++)
		{
			echo "<tr>";

			echo "<td" . (($OENNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") ."><a href=./artikel_Main.php?cmdAktion=OENummern&OENKEY=" . $rsArtikel["OEN_KEY"][$OENNr] . ">";
			echo "" . $rsArtikel["OEN_NUMMER"][$OENNr] . "</a></td>";
			echo "<td" . (($OENNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["HER_BEZEICHNUNG"][$OENNr] . "</td>";
			echo "<td" . (($OENNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">" . $rsArtikel["OEN_BEMERKUNG"][$OENNr] . "</td>";

			echo "<td" . (($LARNr%2)==0?" id=TabellenZeileGrau":" id=TabellenZeileWeiss") .">";
			if(isset($rsArtikel['TEI_KEY1'][$OENNr]))
			{
				echo '<a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key='.$rsArtikel['TEI_KEY1'][$OENNr].'>';
				echo $rsArtikel["TEI_WERT1"][$OENNr];
				echo "</a>";
			}
			else
			{
				$SQL = 'SELECT DISTINCT TEI_Wert1, TEI_Key1 FROM TeileInfos WHERE TEI_Key2=' . $rsArtikel['OEN_KEY'][$OENNr] . ' AND TEI_ITY_ID2=\'OEN\' AND TEI_ITY_ID1=\'AST\'';
				$rsART = awisOpenRecordset($con,$SQL);
				for($rsArtZeile=0;$rsArtZeile<$awisRSZeilen;$rsArtZeile++)
				{
					echo ($rsArtZeile>0?', ':'') . '<a href=./artikel_Main.php?cmdAktion=ArtikelInfos&Key=' . $rsART['TEI_KEY1'][$rsArtZeile] . '>' . $rsART['TEI_WERT1'][$rsArtZeile] . '</a>';
				}
			}
			echo '</td>';
			echo "</tr>";

			if($OENNr==$MaxDSAnzahl)
			{
				echo '<tr><td colspan=99><span Class=HinweisText>Das Maximum von ' . $OENNr . ' Zeilen ist erreicht</span></td></tr>';
				break;
			}

		}

		echo "</table>";
		echo "<br><font size=1>Ben�tigte Zeit: " . date("i:s", time()-$ZeitVerbrauch) . ". Es wurden $OENNr Artikel gefunden.</font>";
	}
}

/*********************************************************************
*
* Datensatz l�schen oder zur�cksetzen
*
**********************************************************************/
//echo "<hr>$SQL<hr>";

if(awis_NameInArray($_POST, "cmdLARLoeschen")!='' AND !isset($_POST["cmdSpeichernAbbruch"]))
{

	if(isset($_POST["cmdSpeichernBestaetigung"]))
	{
		if($_POST['txtLAR_BEKANNTWW']=='0')
		{
			$SQL = "DELETE FROM LieferantenArtikel WHERE LAR_KEY=" . $_POST['txtOEN_KEY'];
		}
		else
		{
			$SQL = "UPDATE LieferantenArtikel SET LAR_USER='LOESCHEN' WHERE LAR_KEY=" . $_POST['txtOEN_KEY'];
		}
		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}
	else
	{
		echo "<form name=frmArtikel method=post>";

		echo "<input type=hidden name=txtOEN_KEY value='" . $_POST['txtOEN_KEY'] . "'>";
		echo "<input type=hidden name=txtLAR_BEKANNTWW value='" . $_POST['txtLAR_BEKANNTWW'] . "'>";
		echo "<input type=hidden name=cmdLARLoeschen>";

		echo "<br><span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Lieferantenartikel " . ($_POST['txtLAR_BEKANNTWW']==0?'l�schen':'zur�cksetzen') . " wollen?</span><br><br>";
		echo "<input accesskey=j type=submit value=\"Ja, Aktion ausf�hren\" name=cmdSpeichernBestaetigung>";
		echo "<input accesskey=n type=submit value=\"Nein, Aktion nicht ausf�hren\" name=cmdSpeichernAbbruch>";

		echo "</form>";
		awisLogoff($con);
		die();
	}
}

//********************************************************************
// Einzelner Datensatz -> Formular anzeigen
//********************************************************************
//die("1");

if($DSAnz==-1)
{
	echo "<form name=frmLieferantenArtikel method=post>";

    $Artikel = explode(";",awis_BenutzerParameter($con, "AktuellerOEN", $AWISBenutzer->BenutzerName()));

	$SQL = "SELECT * FROM OENummern LEFT OUTER JOIN Hersteller ON OEN_HER_ID = HER_ID ";

		// Beim Aufruf von der OEPreise Seite aus
	if(strstr($Bedingung,'OEN_NUMMER'))
	{
		if(isset($_GET['HER_ID']))
		{
			$Bedingung .= ' AND OEN_HER_ID = ' . $_GET['HER_ID'];
		}
		else
		{
			$SQL .= '';
		}

		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	else
	{
		$SQL .= " WHERE OEN_KEY=0" . $OENKEY;
		$SQL .= " ";
	}

	$rsArtikel = awisOpenRecordset($con, $SQL);
	if($rsArtikel==FALSE)
	{
		awisErrorMailLink("liefart_Artikel.php", 2, $awisDBFehler);
		awisLogoff($con);
		die();
	}

	if($awisRSZeilen==1)
	{
			// Aktuelle Daten speichern
		awis_BenutzerParameterSpeichern($con, "AktuellerOEN", $AWISBenutzer->BenutzerName(), $rsArtikel["OEN_KEY"][0] . ";" . $rsArtikel["OEN_NUMMER"][0] . ";" . $rsArtikel["OEN_HER_ID"][0]);

		echo "<table border=0 width=100%>";
		echo "<tr><td><font size=5><b>OE-Nummer " . $rsArtikel['OEN_NUMMER'][0] . "</b></font></td>";
		echo "<td width=100><font size=1>" . $rsArtikel["OEN_USER"][0] ."<br>"  . $rsArtikel["OEN_USERDAT"][0] . "</font></td>";
		echo "<td width=40 backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./artikel_Main.php?cmdAktion=OENummern&Liste=True';></td>";
		echo "</tr></table>";

			// Zeile 1
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=120><col width=*></colgroup>";
		echo "<tr>";
		echo "<td id=FeldBez>Nummer</td><td><input type=hidden name=txtOEN_KEY value=". $rsArtikel['OEN_KEY'][0] . ">" . $rsArtikel['OEN_NUMMER'][0] . "</td>";
		echo "</tr>";
		echo "</table>";


			// Zeile 2
		echo "<table border=1 width=100% >";
		echo "<colgroup><col width=120><col width=500><col width=120><col width=*></colgroup>";

		If(($OENRechtestufe & 2)==2)		// Bearbeiten
		{
			echo "<tr><td id=FeldBez><table border=0 width=100%><tr><td id=FeldBez>Hersteller</td><td align=right>";
			echo "<a href=/stammdaten/hersteller.php?HERID=" . $rsArtikel['OEN_HER_ID'][0] . "><img src=/bilder/suche.png border=0></a>";
			echo "</td></tr></table></td><td>";
			echo "<select name=txtOEN_HER_ID>";
			$rsHER = awisOpenRecordset($con, "SELECT HER_ID, HER_BEZEICHNUNG FROM HERSTELLER ORDER BY HER_BEZEICHNUNG");
			for($HERZeile=0;$HERZeile<$awisRSZeilen;$HERZeile++)
			{
				echo "<option " . ($rsArtikel['OEN_HER_ID'][0]==$rsHER['HER_ID'][$HERZeile]?'selected':'') . " value=" . $rsHER['HER_ID'][$HERZeile] . ">";
				echo $rsHER['HER_BEZEICHNUNG'][$HERZeile];
				echo "</option>";
			}
			echo '</select>';
			echo '</td>';
			echo "<td id=FeldBez>Muster</td><td><select name=txtOEN_MUSTER>";
			echo "<option value=0 " . ($rsArtikel['OEN_MUSTER'][0]==0?'selected':'') . ">Nein</option>";
			echo "<option value=1 " . ($rsArtikel['OEN_MUSTER'][0]==1?'selected':'') . ">Ja</option>";
			echo "</select></td>";
		}
		else
		{
			echo "<tr><td id=FeldBez>Hersteller</td><td>" . $rsArtikel["HER_BEZEICHNUNG"][0] . "</td>";
			echo "<td id=FeldBez>Muster</td><td>" . ($rsArtikel['OEN_MUSTER'][0]==0?'Nein':'Ja') . "</td>";
		}

		echo "</tr>";
		echo "</table>";

				// Zeile 3
		echo "<table border=1 width=100%>";
		echo "<colgroup><col width=120></col><col width=*></col></colgroup>";
		echo "<tr>";
		If(($OENRechtestufe & 2)==2)
		{
			echo "<td id=FeldBez>Bemerkung</td><td><textarea cols=90 rows=3 type=text name=txtOEN_BEMERKUNG>" . $rsArtikel['OEN_BEMERKUNG'][0] . "</textarea></td>";
		}
		else
		{
			echo "<td id=FeldBez>Bemerkung</td><td>" . $rsArtikel['OEN_BEMERKUNG'][0] . "</td>";
		}
		echo "</tr>";
		echo "</table>";

		echo "<hr>";


		// Unterregister mit abh�ngigen Daten
		$cmdAktion = (isset($_GET["Seite"])?$_GET["Seite"]:"ATUArtikel");
		awis_RegisterErstellen(43, $con);


		If(($OENRechtestufe & 2)==2)
		{
			echo "<input type=hidden name=Speichern Value=True>";
			echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./artikel_Main.php?cmdAktion=ArtikelInfos&Seite=$cmdAktion&Key=" . (isset($rsArtikel["AST_KEY"][0])?$rsArtikel["AST_KEY"][0]:'') . "&Speichern=True'>";

						// Cursor in das erste Feld setzen
			echo "<Script Language=JavaScript>";
			echo "document.getElementsByName(\"txtOEN_HER_ID\")[0].focus();";
			echo "</Script>";

		}
	}
	else
	{
		echo "<span class=HinweisText>Keine OE-Nummern</span>";
	}

	unset($rsGNr);

}

echo "</form>";

