<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>

</head>

<body>
<?php
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $tab_breite;

$con = awislogon();
$RechteStufe = awisBenutzerRecht($con, 10);

if(($RechteStufe&4)!=4)
{
	awisEreignis(3,1000,'Journal',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
	die("Keine ausreichenden Rechte!");
}

$awis_link = "";
$db = awisDBServer();
/*
if (!empty($_GET["txtDB"]))
{
	$awis_link = "@" . strtolower($_GET["txtDB"]) . ".atu.de";
	$db = strtolower($_GET["txtDB"]);
}
else
{
	$db = strtolower(awisDBServer());
}
*/
$rsResult = awisOpenRecordset($con,'SELECT * from mcp.datenbanken');
$rsResultZeilen = $awisRSZeilen;

print "<h1 id=SeitenTitel>Speicherverbrauch";
if (!empty($_GET["txtTablespaceName"]))
{
	print " von Tablespace: " . $_GET["txtTablespaceName"];
}
print "</h1>";

echo '<form name=frmSuche method=get action=' . $_SERVER["PHP_SELF"] . '>';
echo '<input type="hidden" name="cmdAktion" value="Tablespace">';

echo '<table border=1>';
echo '<tr><td id=FeldBez colspan=4>';
echo '<h3>Datenbanken</h3></td></tr>';
echo '<tr>';
echo '<td id=FeldBez><u>D</u>atenbank</td>';
echo '<td colspan=3><input name=txtDB readonly accesskey=d value="'.awisDBServer().'"></td>';
// Keine Auswahl mehr, SK 14.02.2008
/*

echo '<td colspan=3><select accesskey=d name="txtDB">';
for($DBNr=0;$DBNr<$rsResultZeilen;$DBNr++)
{
	echo '<option value="' . $rsResult['DBK_NAME'][$DBNr] . '"';
	if ($db == strtolower($rsResult['DBK_NAME'][$DBNr]))
	{
		echo ' selected="selected"';
	}
	echo '>' . $rsResult['DBK_NAME'][$DBNr] . '</option>';
}

echo '</select></td>';
*/
echo '</tr>';
echo '</table>';
print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";

if (empty($_GET["txtTablespaceName"]))
{
	$tab_breite = 750;
	$tab_spalte1 = 350;
	
	
	$SQL = 'SELECT * from ';
	$SQL .= '(SELECT d.status "Status", d.tablespace_name "TBSName", d.contents "Type", d.extent_management "Extent Management", ';
	$SQL .= 'TO_CHAR(NVL(a.bytes / 1024 / 1024, 0),\'99,999,990.900\') "Size (M)", TO_CHAR(NVL(a.bytes - NVL(f.bytes, 0), 0)/1024/1024,\'99,999,999.999\') '; 
	$SQL .= '"Used (M)", TO_CHAR(NVL((a.bytes - NVL(f.bytes, 0)) / a.bytes * 100, 0), \'990.00\') "Used %" ';
	$SQL .= 'FROM sys.dba_tablespaces' . $awis_link . ' d, (select tablespace_name, sum(bytes) bytes from dba_data_files' . $awis_link . ' group by tablespace_name) a, ';
	$SQL .= '(select tablespace_name, sum(bytes) bytes from dba_free_space' . $awis_link . ' group by tablespace_name) f ';
	$SQL .= 'WHERE d.tablespace_name = a.tablespace_name(+) AND d.tablespace_name = f.tablespace_name(+) AND NOT (d.extent_management like \'LOCAL\' ';
	$SQL .= 'AND d.contents like \'TEMPORARY\') ';
	$SQL .= 'UNION ALL ';
	$SQL .= 'SELECT d.status "Status", d.tablespace_name "TBSName", d.contents "Type", d.extent_management "Extent Management", ';
	$SQL .= 'TO_CHAR(NVL(a.bytes / 1024 / 1024, 0),\'99,999,990.900\') "Size (M)", TO_CHAR(NVL(t.bytes, 0)/1024/1024,\'99,999,999.999\') '; 
	$SQL .= '"Used (M)", TO_CHAR(NVL(t.bytes / a.bytes * 100, 0), \'990.00\') "Used %" ';
	$SQL .= 'FROM sys.dba_tablespaces' . $awis_link . ' d, (select tablespace_name, sum(bytes) bytes from dba_temp_files' . $awis_link . ' group by tablespace_name) a, ';
	$SQL .= '(select tablespace_name, sum(bytes_cached) bytes from v$temp_extent_pool' . $awis_link . ' group by tablespace_name) t ';
	$SQL .= 'WHERE d.tablespace_name = a.tablespace_name(+) AND d.tablespace_name = t.tablespace_name(+) AND d.extent_management like \'LOCAL\' ';
	$SQL .= 'AND d.contents like \'TEMPORARY\') ';
	$SQL .= 'order by 2';

	
	$rsResult = awisOpenRecordset($con, $SQL);
	// var_dump($rsResult);
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_infos_tbs.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	$rsResultZeilen=$awisRSZeilen;
	
	print "<table id=DatenTabelle width=$tab_breite border=1>";
	// print "<table id=DatenTabelle border=1>";
	print "<tr>";
	print "<td id=FeldBez>Status</td>";
	print "<td id=FeldBez>Name</td>";
	print "<td id=FeldBez>Type</td>";
	print "<td id=FeldBez>Extent Management</td>";
	print "<td id=FeldBez>Size (M)</td>";
	print "<td id=FeldBez>Used (M)</td>";
	print "<td id=FeldBez>Used %</td>";
	print "</tr>";
	for($i=0;$i<$rsResultZeilen;$i++)
	{
		print "<tr>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsResult['Status'][$i] . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " ><a href=" . $_SERVER["PHP_SELF"] . "?cmdAktion=Tablespace";
		print "&txtTablespaceName=" . $rsResult['TBSName'][$i] . "&txtDB=" . (isset($_GET["txtDB"])?$_GET["txtDB"]:'') . ">" . $rsResult['TBSName'][$i] . "</a></td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsResult['Type'][$i] . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsResult['Extent Management'][$i] / 1024 . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right >" . $rsResult['Size (M)'][$i] . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right >" . $rsResult['Used (M)'][$i] . "</td>";
		if ($rsResult['Used %'][$i] > 90 && $rsResult['Type'][$i] == "PERMANENT")
		{
			print "<td class=HinweisText align=right >";
		}
		else
		{
			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right >";
		}
		print $rsResult['Used %'][$i];
		print "</td>";
		print "</tr>";
	}
	print "</table>";
	
}
else
{
	
	$SQL = "SELECT OWNER, SEGMENT_NAME, SEGMENT_TYPE, TABLESPACE_NAME, BYTES, EXTENTS FROM DBA_SEGMENTS" . $awis_link . " ";
	$SQL .= "WHERE TABLESPACE_NAME = '" . $_GET["txtTablespaceName"] . "' ";
	$SQL .= "ORDER BY BYTES DESC, SEGMENT_NAME ";

	$rsResult = awisOpenRecordset($con, $SQL);
	
	if($rsResult==FALSE)
	{
		awisErrorMailLink("db_status_tbs.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
	}
	$rsResultZeilen=$awisRSZeilen;
	
	print "<table id=DatenTabelle width=$tab_breite border=1>";
	// print "<table id=DatenTabelle border=1>";
	print "<tr>";
	print "<td id=FeldBez>Besitzer</td>";
	print "<td id=FeldBez>Name</td>";
	print "<td id=FeldBez width=90>Typ</td>";
// 	print "<td id=FeldBez>Tablespace</td>";
	print "<td id=FeldBez>K-Bytes</td>";
	print "<td id=FeldBez>Extents</td>";
	print "</tr>";
	for($i=0;$i<$rsResultZeilen;$i++)
	{
		print "<tr>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsResult['OWNER'][$i] . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsResult['SEGMENT_NAME'][$i] . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsResult['SEGMENT_TYPE'][$i] . "</td>";
// 		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsResult['TABLESPACE_NAME'][$i] . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right>" . $rsResult['BYTES'][$i] / 1024 . "</td>";
		print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right>" . $rsResult['EXTENTS'][$i] . "</td>";
		print "</tr>";
	}
	print "</table>";
}

print "</form>";
awislogoff($con);

?>
</body>
</html>

