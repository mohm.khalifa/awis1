<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
echo '<title>SMS Versand</title>';
echo "<link rel=stylesheet type=text/css href=/css/awis.css>";
?>
</head>
<body>
<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisMailer.inc';
/*
- Filialnummer
- Filial-Name
- Anzahl zu bearbeitender Werkstattaufträge
- Umsatz vor Rabatt für jeden Auftrag
- Start-Zeit (entspricht dem Fahrzeuganlieferung)
- End-Zeit (entspricht dem Fahrzeugabholung)
 */

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();
$Werkzeug = new awisWerkzeuge();
$MailObj = new awisMailer($DB, $AWISBenutzer);

$Parameter = explode('&',base64_decode($_GET['ID']));
$Param = array();
foreach($Parameter AS $Eintrag)
{
	$Element = explode("=",$Eintrag);

	$Param[$Element[0]]=$Element[1];
}

$Handy= '';
for($i=0;$i<strlen($Param['HANDY']);$i++)
{
    if(is_numeric($Param['HANDY'][$i]))
    {
        $Handy .= $Param['HANDY'][$i];
    }
}

if($Handy=='')     // Ist ein Fehler !!
{
    $Handy = '01712104846';
}

$Absender = $AWISBenutzer->EMailAdresse();
if($Absender == '')
{
	$Absender = 'shuttle@de.atu.eu';
}



$Form = new awisFormular();

$Form->Formular_Start();

try
{

	//***************************************************************************
	//* In Entwicklung und Test immer an den tester (falls Handy hinterlegt)
	//***************************************************************************
	if($Werkzeug->awisLevel()!=awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
	{
	    $Handy = '01712104846';
	    $Absender = 'shuttle@de.atu.eu';
	
	    $HandyNummer = $AWISBenutzer->KontaktInfo(6);
	    if($HandyNummer !== false AND $HandyNummer!='')
	    {
	        $Handy = $HandyNummer;
	        $Absender = $AWISBenutzer->KontaktInfo(7);
	    }
	    else
	    {
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel('HINWEIS: Keine Handynummer fuer '.$AWISBenutzer->BenutzerName().' gefunden', 300, 'Hinweis');
	        $Form->ZeileEnde();
	    }
	
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel('TESTMODUS: '.$AWISBenutzer->BenutzerName().' an '.$Handy, 300, 'Hinweis');
	    $Form->ZeileEnde();
	}
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('SMS an '.$Handy, 300);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Absender '.$Absender, 300);
	$Form->ZeileEnde();
	
	$Form->Trennzeile();
	
	$SQL ='SELECT DISTINCT SUM(WTP_VK) AS UMSATZ, MIN(WPO_TERMINVON) AS VON, MAX(WPO_TERMINBIS) AS BIS';
	$SQL .= ', COUNT(DISTINCT WTE_KEY) AS ANZ, WTE_FIL_ID, FIL_BEZ';
	$SQL .= ' FROM WerkstattterminePlanungen';
	$SQL .= ' INNER JOIN WerkstattterminePositionen ON WTP_WPO_KEY = WPO_KEY AND WPO_IMPORTSTATUS <> \'X\'';
	$SQL .= ' INNER JOIN Werkstatttermine ON WPO_WTE_KEY = WTE_KEY';
	$SQL .= ' INNER JOIN Filialen ON WTE_FIL_ID = FIL_ID';
	$SQL .= ' WHERE WTP_KON_KEY = '.$DB->FeldInhaltFormat('N0', $Param['KON_KEY'], false);
	$SQL .= ' AND WTP_TAG = '.$DB->FeldInhaltFormat('D',$Param['TAG']);
	$SQL .= ' AND WTP_STATUS < 8';
	$SQL .= ' GROUP BY WTE_FIL_ID, FIL_BEZ';
	$SQL .= ' ORDER BY WTE_FIL_ID';
	$rsWTP = $DB->RecordSetOeffnen($SQL);
	
	while(!$rsWTP->EOF())
	{
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel('Filiale :',120);
	    $Form->Erstelle_TextFeld('A', $rsWTP->FeldInhalt('WTE_FIL_ID').'-'.$rsWTP->FeldInhalt('FIL_BEZ'), 0, 250, false, '','','','T');
	    $Form->ZeileEnde();
	
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel('Umsatz :',120);
	    $Form->Erstelle_TextFeld('A', $Form->Format('N0',$rsWTP->FeldInhalt('UMSATZ')).' Eur', 0, 150, false,'','','','T');
	    $Form->ZeileEnde();
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel('Termine ab :',120);
	    $Form->Erstelle_TextFeld('A', $rsWTP->FeldInhalt('VON'), 0, 150, false,'','','','DU');
	    $Form->ZeileEnde();
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel('Termine bis :',120);
	    $Form->Erstelle_TextFeld('A', $rsWTP->FeldInhalt('BIS'), 0, 150, false,'','','','DU');
	    $Form->ZeileEnde();
	    $Form->ZeileStart();
	    $Form->Erstelle_TextLabel('Auftr&auml;ge :',120);
	    $Form->Erstelle_TextFeld('A', $rsWTP->FeldInhalt('ANZ'), 0, 150, false,'','','','N0');
	    $Form->ZeileEnde();
	
	    $Form->TrennZeile();
	
	    $Text = 'Filiale '.$rsWTP->FeldInhalt('WTE_FIL_ID').'-'.str_replace('ATU ','',$rsWTP->FeldInhalt('FIL_BEZ'))." ";
	    $Text .= ' Umsatz '.$Form->Format('N0',$rsWTP->FeldInhalt('UMSATZ'))." Eur ";
	    $Zeit = $Form->PruefeDatum($rsWTP->FeldInhalt('VON'),false,false,true);
	    $Text .= ' ab:'.date('d.m. H:i',$Zeit)." ";
	    $Zeit = $Form->PruefeDatum($rsWTP->FeldInhalt('BIS'),false,false,true);
	    $Text .= ' bis '.date('d.m. H:i',$Zeit)." ";
	    $Text .= ' Anz '.$Form->Format('N0',$rsWTP->FeldInhalt('ANZ'));

	    // TODO: SMS direkt senden, oder Warteschlange?
	    if(false)
	    {
			// SMS direkt senden
		    $Erg = $MailObj->SMSSenden($Handy, substr($Text,0,160));
		    
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($Erg?'SMS wurde an den Provider weitergeleitet':'SMS konnte nicht gesendet werden.',300);
		    $Form->ZeileEnde();
	    }
	    else
	    {
	    	$MailObj->LoescheAdressListe();
	    	$MailObj->Text(substr($Text,0,160),awisMailer::FORMAT_TEXT,true);
	    	$MailObj->AdressListe(awisMailer::TYP_TO,$Handy,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
	    	$MailObj->SetzeBezug('KON', $DB->FeldInhaltFormat('N0', $Param['KON_KEY'], false));
	    	$MailObj->Absender($Absender);
	    	$MailObj->Betreff('SmartRepair');
	    	$MailObj->SMSInWarteschlange();
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel('SMS wurde in die Warteschlange aufgenommen',300);
		    $Form->ZeileEnde();
	    }
	    
	    $rsWTP->DSWeiter();
	
	}
}
catch (Exception $ex)
{
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('FEHLER beim Senden einer SMS:'.$ex->getMessage(),300);
	$Form->Erstelle_TextLabel('SQL:'.$ex->getSQL(),300);
	$Form->ZeileEnde();
	
}
$Form->Formular_Ende();

$Form->SchaltflaechenStart();
$Form->Schaltflaeche('script', 'cmdSchliessen', "onclick=window.close();", '/bilder/cmd_zurueck.png', 'Fertig','');
$Form->SchaltflaechenEnde();
?>