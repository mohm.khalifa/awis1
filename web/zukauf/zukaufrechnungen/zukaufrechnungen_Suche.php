<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUR','%');
	$TextKonserven[]=array('ZUB','ZUB_ZLH_KEY');
	$TextKonserven[]=array('AST','AST_ATUNR');
	$TextKonserven[]=array('ZSZ','ZSZ_SORTIMENT');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Datum%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10002 = $AWISBenutzer->HatDasRecht(10002);
	if($Recht10002==0)
	{
	    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	echo "<br>";
	echo "<form name=frmSuche method=post action=./zukaufrechnungen_Main.php?cmdAktion=Details>";

	/**********************************************
	* Eingabemaske
	***********************************************/

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RECHNUNGSNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_RECHNUNGSNUMMER','',20,200,true);
	$AWISCursorPosition='sucZUR_RECHNUNGSNUMMER';
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARTIKELNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_ARTIKELNUMMER','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARTIKELBEZEICHNUNG'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_ARTIKELBEZEICHNUNG','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_LIE_NR'].':',190);
	$SQL = 'SELECT LIE_NR, LIE_NAME1';
	$SQL .= ' FROM Lieferanten';
	$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
	$SQL .= ' ORDER BY LIE_NAME1';
	$Form->Erstelle_SelectFeld('*ZUR_LIE_NR','',200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLH_KEY'].':',190);
	$SQL = 'SELECT ZLH_KEY, ZLH_KUERZEL || \' - \' || ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*ZLH_KEY','',200,true,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ATUNR'].':',190);
	$Form->Erstelle_TextFeld('*AST_ATUNR','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_FIL_ID'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_FIL_ID','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WANR'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_WANR','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EXTERNEID'].':',190);
	$Form->Erstelle_TextFeld('*ZUR_EXTERNEID','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
	$Form->Erstelle_TextFeld('*DATUMVOM','',20,200,true);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',190);
	$Form->Erstelle_TextFeld('*DATUMBIS','',20,200,true);
	$Form->ZeileEnde();

	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht10002&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();



	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>