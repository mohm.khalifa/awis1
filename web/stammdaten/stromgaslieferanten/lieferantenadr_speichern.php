<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
	
	if(isset($_GET['button']))
	{
		$SQL = 'UPDATE GSVLIEFERANTEN SET';
		$SQL .= '  GSL_AKTIV='.$DB->WertSetzen('GSL', 'N0',$_GET['Aktiv']==1?0:1,false);
		$SQL .= ', GSL_user='.$DB->WertSetzen('GSL', 'T',$AWISBenutzer->BenutzerName());
		$SQL .= ', GSL_userdat=sysdate';
		$SQL .= ' WHERE GSL_KEY='.$DB->WertSetzen('GSL', 'N0',$_GET['button']);
		$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('GSL', true));
	}
	
	if(isset($_POST['txtGSL_KEY']))
	{
		if(floatval($_POST['txtGSL_KEY'])=='-1')
			{
				$SQL = 'INSERT INTO GSVLIEFERANTEN(';
				$SQL .= 'GSL_SUCHNAME,GSL_FIRMA,GSL_ANREDE,GSL_TITEL,GSL_VORNAME,GSL_ANSPRECHPARTNER,GSL_STRASSE,GSL_PLZ,GSL_ORT,GSL_BRANCHENSCHL,GSL_TELNR_FIRMA,GSL_TELNR_PRIVAT,GSL_MOBIL,GSL_FAXNR,GSL_MAIL,GSL_LIEFERANTENART';
				$SQL .= ',GSL_USER,GSL_USERDAT)';
  				$SQL .= 'VALUES (';
				$SQL .= ' '.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_SUCHNAME'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_FIRMA'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_ANREDE'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TITEL'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_VORNAME'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_ANSPRECHPARTNER'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_STRASSE'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_PLZ'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_ORT'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_BRANCHEN'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TELNR_FIRMA'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TELNR_PRIVAT'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TELNR_MOBIL'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_FAXNR'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_EMAIL'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_LIEFERANTENART'],false);
				$SQL .= ','.$DB->WertSetzen('GSL', 'T',$AWISBenutzer->BenutzerName(),true);
				$SQL .= ', SYSDATE)';
				
				$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('GSL', true));
			}
			else 					// gešnderter Lieferschein
			{
					$SQL = 'UPDATE GSVLIEFERANTEN SET';
					$SQL .= ' GSL_SUCHNAME ='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_SUCHNAME'],false);
					$SQL .= ', GSL_FIRMA='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_FIRMA'],false);
					$SQL .= ', GSL_ANREDE='.$DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_ANREDE'],false);
					$SQL .= ', GSL_TITEL='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TITEL'],false);
					$SQL .= ', GSL_VORNAME='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_VORNAME'],false);
					$SQL .= ', GSL_ANSPRECHPARTNER='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_ANSPRECHPARTNER'],false);
					$SQL .= ', GSL_STRASSE='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_STRASSE'],false);
					$SQL .= ', GSL_PLZ='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_PLZ'],false);
					$SQL .= ', GSL_ORT='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_ORT'],false);
					$SQL .= ', GSL_BRANCHENSCHL='.$DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_BRANCHEN'],false);
					$SQL .= ', GSL_TELNR_FIRMA='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TELNR_FIRMA'],false);
					$SQL .= ', GSL_TELNR_PRIVAT='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TELNR_PRIVAT'],false);
					$SQL .= ', GSL_MOBIL='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_TELNR_MOBIL'],false);
					$SQL .= ', GSL_FAXNR='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_FAXNR'],false);
					$SQL .= ', GSL_MAIL='.$DB->WertSetzen('GSL', 'T',$_POST['txtGSL_EMAIL'],false);
					$SQL .= ', GSL_LIEFERANTENART='.$DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_LIEFERANTENART'],false);
					if(isset($_POST['txtGSL_AKTIV'])) {
						$SQL .= ', GSL_AKTIV='.$DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_AKTIV'],false);
					}
					$SQL .= ', GSL_user='.$DB->WertSetzen('GSL', 'T',$AWISBenutzer->BenutzerName(),true);
					$SQL .= ', GSL_userdat=sysdate';
					$SQL .= ' WHERE GSL_KEY=' . $DB->WertSetzen('GSL', 'N0',$_POST['txtGSL_KEY']) . '';
					$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('GSL', true));
			}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>