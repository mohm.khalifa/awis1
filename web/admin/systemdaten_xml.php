<?php

echo '<?xml version="1.0"?>' . "\n";
//echo '<!DOCTYPE SYSTEMDATEN [' . "\n";
//echo '<!ELEMENT DATEN (ZEIT)>' . "\n";
//echo '<!ELEMENT DATEN (WEBSERVERNAME)>' . "\n";
//echo '<!ELEMENT ZEIT (#PCDATA)>' . "\n";
//echo '<!ELEMENT WEBSERVERNAME (#PCDATA)>' . "\n";
//echo ']>' . "\n";
echo "<daten>\n";
echo "<startzeit>" . strftime("%Y.%m.%d %X") . "</startzeit>\n";
echo "<startzeit_ts>" . time() . "</startzeit_ts>\n";
echo "<webservername>". $_ENV['HOSTNAME'] . "</webservername>\n";

require_once("db.inc.php");		// DB-Befehle
require_once("config.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
//include ("ATU_Header.php");	// Kopfzeile

global $awis_webserver;
global $awis_testwebserver;
global $awis_db_user;
global $awis_db_pwd;

$con = awislogon();
//$RechteStufe = awisBenutzerRecht($con, 1);
If(isset($con))
{
	$rsInfo = awisOpenRecordset($con,'SELECT HOST_NAME FROM SYS.V_$INSTANCE');
	print "<oracleservername>". $rsInfo["HOST_NAME"][0] . "</oracleservername>\n";
}
awislogoff($con);

$cmd = "mount|grep :/daten/"; 
exec($cmd, $output, $fehler);
if ($fehler != 0) {
	echo "<fehler>";
	print_r($fehler);
	echo "</fehler>\n";
}

$i = 0;
echo "<mountpunkt>";
foreach ($output as $mp) {
	echo "<mountpunkt" . ++$i . ">";
	echo $mp;
	echo "</mountpunkt" . $i . ">\n";
}
echo "</mountpunkt>\n";

echo "<endzeit>" . strftime("%Y.%m.%d %X") . "</endzeit>\n";
echo "<endzeit_ts>" . time() . "</endzeit_ts>\n";
echo "</daten>\n";
die();

?>
