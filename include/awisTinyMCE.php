<?php

/**
 * Klasse f�r das Erstllen des WYSIWYG-Editors "TinyMCE"
 *
 * Class awisTinyMCE
 */
class awisTinyMCE
{

    /**
     * Beinhaltet das Script f�r den WYSIWYG-Editor
     *
     * @var string
     */
    private $_Script;

    /**
     * Beinhaltet pro Toolbar  Zeile ein Toolbarobjekt
     *
     * @var array
     */
    private $_Toolbars = array();

    /**
     * Beinhaltet alle Contextmenu-Inhalte (rechtsklick)
     *
     * @var array
     */
    private $_Contextmenu = array();

    /**
     * Menubar des Editors
     *
     * @var awisTinyMCEMenubar
     */
    private $_Menu;

    /**
     * Name des Textareafelds
     *
     * @var string
     */
    private $_Name = '';

    /**
     * awisBenutzer
     *
     * @var awisBenutzer
     */
    private $_AWISBenutzer;

    /**
     * awisDatenbank
     *
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * @var awisFormular
     */
    private $_Form;

    /**
     * Array mit allen Plugins die ben�tigt werden
     *
     * @var array
     */
    private $_Plugins = array();

    /**
     * awisTinyMCE constructor.
     * @param awisFormular $Form
     * @param awisBenutzer $AWISBenutzer
     * @param $Name
     */
    public function __construct(awisFormular $Form, awisBenutzer $AWISBenutzer, awisDatenbank $DB, $Name, $mitTextFarbe = false,$MitRechtsklickMenu = false, $MitCodeEditor = false)
    {
        require_once 'awisTinyMCEToolbar.php';
        $this->_Toolbars[1] = new awisTinyMCEToolbar();
        if ($mitTextFarbe) { //Mit Textfarbauswahl
            $this->_Toolbars[1]->MitStandardFunktionen(); //Sonst wird wirklich nur die Textfarbauswahl angezeigt
            $this->_Toolbars[1]->MitTextfarbAuswahl();
        }
        require_once 'awisTinyMCEContextmenu.php';
        $this->_Contextmenu = new awisTinyMCEContextmenu();

        if ($MitRechtsklickMenu) { //Mit Context/Rechtsklickmenu
            $this->_Contextmenu->MitRechtsklickMenu();
            $this->_Toolbars[1]->MitLinkPlugin(); //Lade Link Plugin, da sonst nicht angezeigt
            $this->_Toolbars[1]->MitQuickbarsPlugin(); //Lade Quickbar (Schnellauswahl direkt �ber Text, jedoch im Textfeld)
            //Ggf. Quickbar als extra Funktion... Fand ich pers�nlich hier passend - AM
        }
        if ($MitCodeEditor) { //Es muss trotzdem extra der Tools-Reiter bei der Menubar geladen werden
            $this->_Toolbars[1]->MitCodePlugin();
        }
        $this->_AWISBenutzer = $AWISBenutzer;
        $this->_Form = $Form;
        $this->_DB = $DB;
        $this->_Name = $Name;

        require_once 'awisTinyMCEMenubar.php';
        $this->_Menu = new awisTinyMCEMenubar($this->_DB);
    }

    /**
     * Erzeugt eine Toolbarzeile, welche angepasst werden kann.
     *
     * @param int $Zeile
     * @return awisTinyMCEToolbar
     */
    public function Toolbar($Zeile = 1)
    {

        if (!isset($this->_Toolbars[$Zeile])) {
            $this->_Toolbars[$Zeile] = new awisTinyMCEToolbar();
        }

        return $this->_Toolbars[$Zeile];
    }

    /**
     * Erzeugt die Menubar (Dropdowns)
     * @return awisTinyMCEMenubar
     */
    public function Menubar()
    {
        return $this->_Menu;
    }

    /**
     * Liefert alle ben�tigten Plugins f�r den Editor
     *
     * @return array
     */
    private function _getPlugins()
    {

        $return = array();
        foreach ($this->_Toolbars as $Toolbar) {
            $TB = $Toolbar->getToolbar();
            $return = array_merge($this->_Plugins, $TB['plugins'], $return);
        }

        $return = array_unique($return);
        return $return;
    }

    /**
     * Echot den Code
     */
    function __destruct()
    {
        echo '<script src="/js/tinymce/tinymce.min.js"></script>' . PHP_EOL;

        echo " <script>tinymce.init({ selector:'textarea',  " . PHP_EOL;
        echo " language: '" . strtolower($this->_AWISBenutzer->BenutzerSprache()) . "' " . PHP_EOL;


        //Plugins ausgeben
        echo ', plugins: "';
        foreach ($this->_getPlugins() as $Plugin) {
            echo ' ' . $Plugin . ' ';
        }
        echo '"';

        echo ', contextmenu: "';
        echo ' ' . $this->_Contextmenu->getContextmenu() . ' ';
        echo '"';

        echo ' , toolbar: [ ';
        foreach ($this->_Toolbars as $Toolbar) {
            echo '"';
            foreach ($Toolbar->getToolbar()['buttons'] as $Button) {
                echo ' ' . $Button . ' ';
            }
            echo '", ';
        }

        echo '  ]';

        // Ggf. die "quickbars_selection_toolbar" einbauen um automatische Felder anzupassen (Kleines Auswahlfeld innerhalb des Textfelds f�r Schnell�nderung)

        foreach ($this->_Toolbars as $Toolbar) {
            foreach ($Toolbar->getToolbar()['additionals'] as $additional) {
                echo ',' . $additional;
            }

        }
        echo ", quickbars_insert_toolbar: false " . PHP_EOL;
        echo ", link_title: false " . PHP_EOL;
        echo ", link_assume_external_targets: 'https' " . PHP_EOL;
        echo ", link_list: [" . PHP_EOL;
        echo " {title: 'atu.de', value: 'https://www.atu.de'}, " . PHP_EOL;
        echo " ] " . PHP_EOL;


        echo $this->Menubar()->getMenubar();

        echo "});</script>";

        echo $this->_Script;
    }

}