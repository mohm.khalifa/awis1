<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");


global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "\n<style type=text/css> @page { size:landscape; } </style>\n";
print "<link rel=stylesheet type=text/css href=/css/reifen.css>";

?>
</head>

<body>
<?php

include ("ATU_Header.php");	// Kopfzeile

awis_ZeitMessung(0);			// Zeitmessung aktivieren

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$ReifenRechte = awisBenutzerRecht($con,250);

if($ReifenRechte==0)
{
    awisEreignis(3,1000,'Reifen',$AWISBenutzer->BenutzerName(),'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}


if(strpos($_SERVER['QUERY_STRING'],"cmdReset"))
{
	awis_BenutzerParameterSpeichern($con, "ReifenSuche", $AWISBenutzer->BenutzerName(),'');
	$cmdAktion = '';
}


/************************************************
* Daten anzeigen
************************************************/
$SpeichernButton = False;


	// Bei KB Liste und Suche -> Umschalten auf die Seite ReifenKB
if((isset($_REQUEST['cmdAktion']) AND $_REQUEST['cmdAktion']=='Reifen') AND 
	(isset($_POST['txtKBListe']) AND $_POST['txtKBListe']=='on'))
{
	$cmdAktion = 'ReifenKB';
	awis_RegisterErstellen(2, $con, $cmdAktion);
}
elseif(isset($_POST['txtFahrzeuge']) AND $_POST['txtFahrzeuge']=='on')
{

	$cmdAktion = 'Fahrzeuge';
	awis_RegisterErstellen(2, $con, $cmdAktion);
}
else
{
	$cmdAktion = isset($_REQUEST['cmdAktion'])?$_REQUEST['cmdAktion']:'';
	awis_RegisterErstellen(2, $con, $cmdAktion);
}

print "<input type=hidden name=cmdAktion value=Reifen>";
print "</form>";

print "<br><hr><input type=image title=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
if($SpeichernButton)
{
	print "&nbsp;<input type=image alt=Speichern src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./reifen_Main.php?" . $_SERVER['QUERY_STRING'] . "&Speichern=True'>";
}
print "&nbsp;<input type=image title='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=reifen','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awislogoff($con);

echo '<br><font size=2>Ben�tigte Zeit:' . sprintf('%0.6f',awis_ZeitMessung(1)) . ' Sekunden</font>';

?>
</body>
</html>