<?
//*********************************
// Auswertung Gesamtliste
//*********************************
// Variablen
global $awisRSZeilen;
global $AWISBenutzer;
//$RechteStufe = awisBenutzerRecht($con, 500);
//if($RechteStufe==0)
//{
  // awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
  // die("Keine ausreichenden Rechte!");
//}

// lokale Variablen
// Ende Variablen

//var_dump($_POST);

$FeldListe = array('FIL_ID','EROEFFNUNG','FIL_BEZ','BUNDESLAND', 'FIL_STRASSE', 'LAND', 'FIL_PLZ', 'FIL_ORT', 'TELEFON',
	'FAXNUMMER', 'GESCHÄFTSLEITER1', 'GESCHÄFTSLEITER2', 'GEBIETSLEITER', 'VERKAUFSLEITER',  'MO_FR_VON', 'MO_FR_BIS',
	'SA_VON' , 'SA_BIS', 'FIL_GRUPPE', 'FIL_LAGERKZ');

print "<form name=frmSuche method=post action=./filialinfo_Main.php?cmdAktion=Liste>";

//if($_POST['cmdSuche_x']!='')
//{
	$SQL = 'Select FIL_ID';
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=34 AND FIF_IMQ_ID=32) AS EROEFFNUNG";
	$SQL .= ",FIL_BEZ";
	$SQL .= ",(SELECT BUL_Bundesland FROM FilialInfos INNER JOIN BundesLaender ON FIF_WERT = BUL_ID WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=65 AND FIF_IMQ_ID=32) AS BUNDESLAND";
	$SQL .= ",FIL_STRASSE"; 
	$SQL .= ",(SELECT LAN_Code FROM Laender WHERE lan_wwskenn = fil_lan_wwskenn) AS LAND";
	$SQL .= ",FIL_PLZ";
	$SQL .= ",FIL_ORT";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=1 AND FIF_IMQ_ID=8) AS TELEFON";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=2 AND FIF_IMQ_ID=8) AS FAXNUMMER";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=23 AND FIF_IMQ_ID=2) AS GESCHÄFTSLEITER1";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=24 AND FIF_IMQ_ID=2) AS GESCHÄFTSLEITER2";
	$SQL .= ", GBLName AS GEBIETSLEITER";
	$SQL .= ", VKLName AS VERKAUFSLEITER";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=121 AND FIF_IMQ_ID=32) AS MO_FR_VON";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=122 AND FIF_IMQ_ID=32) AS MO_FR_BIS";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=123 AND FIF_IMQ_ID=32) AS SA_VON";
	$SQL .= ",(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIL_ID=Filialen.FIL_ID AND FIF_FIT_ID=124 AND FIF_IMQ_ID=32) AS SA_BIS";
	$SQL .= ", FIL_GRUPPE";
	$SQL .= ", FIL_LAGERKZ";
	$SQL .= " from filialen";
	$SQL .= " INNER JOIN 
				(
				select FIL_ID AS VKL_FIL_ID,MIT_BEZEICHNUNG AS VKLName, MIT_PER_NR AS VKLPersNr, fif_wert AS RZID, PER_ANR_ID AS VKL_Anrede
				from filialen
				inner join filialinfos on fil_id = fif_fil_id AND fif_fit_id = 26
				inner join mitarbeiter on MIT_REZ_ID = fif_wert AND MIT_STATUS='A'
				INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID AND MIT_MTA_ID IN (1,110)
				LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR
				) VKL ON VKL_FIL_ID = FIL_ID
				INNER JOIN 
				(
				SELECT VKG_ID AS GEBIET, MIT_Bezeichnung AS GBLName,PER_NR AS GBLPersNr, fif_fil_id AS GBL_FIL_ID, PER_ANR_ID AS GBL_Anrede
				FROM Verkaufsgebiete
				INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY
				LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR
				inner join filialinfos on vkg_id = fif_wert and fif_fit_id=21
				) GBL ON GBL_FIL_ID = FIL_ID";

 				

	//$SQL .= ' INNER JOIN  V_FIL_VKL_GBL ON V_FIL_VKL_GBL.FIL_ID = Filialen.FIL_ID';
	//$SQL .= ' LEFT OUTER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
	$SQL .= ' WHERE FIL_LAN_WWSKENN IS NOT NULL';
	$SQL .= ' ORDER BY Filialen.FIL_ID';
	//awis_Debug(1,$SQL);
	$rsFIL= awisOpenRecordset($con, $SQL);
	$rsFILZeilen= $awisRSZeilen;

	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );

	foreach($FeldListe AS $Feld)
	{
		fputs($fd,$Feld.";");
	}
	fputs($fd,"\n");

	for($DSNr=0;$DSNr<$rsFILZeilen;$DSNr++)
	{
		foreach($FeldListe AS $Feld)
		{
			fputs($fd, $rsFIL[$Feld][$DSNr] . ";");
		}

		fputs($fd,"\n");
	}
	fclose($fd);

	$DateiName = pathinfo($DateiName);
	
	echo '<br>';
	echo '<b>Folgende Daten werden generiert:</b>';
	echo '<ul><li>Liste mit wichtigen Daten zur Filiale.</li>';
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei öffnen</a>';

//print "<input type=hidden name=cmdAktion value=Austattung>";
print "</form>";

?>

