<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Meldung;
try {
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $Recht47000 = $AWISBenutzer->HatDasRecht(47000);
    if ($Recht47000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Form->DebugAusgabe(1, __FILE__ . ': AWIS_KEY1: ' . $AWIS_KEY1);
    $Form->DebugAusgabe(1, __FILE__ . ': AWIS_KEY2: ' . $AWIS_KEY2);

    $TextKonserven = array();
    $TextKonserven[] = array('ERE', '%');
    $TextKonserven[] = array('Fehler', 'err_KeinWert');
    $TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
    $TextKonserven[] = array('Fehler', 'err_DatumEndeVorAnfang');
    $TextKonserven[] = array('Fehler', 'err_DatumAbstandTage');
    $TextKonserven[] = array('Wort', 'geaendert_von');
    $TextKonserven[] = array('Wort', 'geaenderuf');
    $TextKonserven[] = array('Meldung', 'DSVeraendert');
    $TextKonserven[] = array('Meldung', 'Eit_angabeWiederholen');

    $TXT_Speichern = $Form->LadeTexte($TextKonserven);
    //AWIS_KEY1
    if ($AWIS_KEY1 == -1) {//Neuer KopfDS

        //Pr�fungen:
        if ((!isset($_POST['sucERK_ID'])) or (isset($_POST['sucERK_ID']) and $_POST['sucERK_ID'] == '')) {
            $Meldung = $TXT_Speichern['ERE']['ERE_FEHLER_ERK_ID'];
        }
        if ((!isset($_POST['txtERK_DATUM']) or (isset($_POST['txtERK_DATUM']) and $_POST['txtERK_DATUM'] == ''))) {
            $Meldung = $TXT_Speichern['ERE']['ERE_FEHLER_ERK_DATUM'];
        }
        if ((!isset($_POST['txtERK_FIL_ID']) or (isset($_POST['txtERK_FIL_ID']) and $_POST['txtERK_FIL_ID'] == ''))) {
            $Meldung = $TXT_Speichern['ERE']['ERE_FEHLER_FIL_ID'];
        }
        if ((!isset($_POST['txtEKU_KEY']) or (isset($_POST['txtEKU_KEY']) and $_POST['txtEKU_KEY'] == ''))) {
            $Meldung = $TXT_Speichern['ERE']['ERE_EKU_KEY'];
        }

        if ($Meldung == '') {
            $Insert = 'INSERT';
            $Insert .= ' INTO EURECHNUNGSKOPF';
            $Insert .= '   (';
            $Insert .= '     ERK_ID,';
            $Insert .= '     ERK_DATUM,';
            $Insert .= '     ERK_FIL_ID,';
            $Insert .= '     ERK_USER,';
            $Insert .= '     ERK_USERDAT';
            $Insert .= '   )';
            $Insert .= '   VALUES';
            $Insert .= '   (';
            $Insert .= $DB->WertSetzen('ERK', 'T', $_POST['sucERK_ID']);
            $Insert .= ' ,' . $DB->WertSetzen('ERK', 'D', $_POST['txtERK_DATUM']);
            $Insert .= ' ,' . $DB->WertSetzen('ERK', 'N0', $_POST['txtERK_FIL_ID']);
            $Insert .= ' ,' . $DB->WertSetzen('ERK', 'T', $AWISBenutzer->BenutzerName());
            $Insert .= ' ,sysdate';
            $Insert .= '   )';

            $DB->Ausfuehren($Insert, '', '', $DB->Bindevariablen('ERK'));
            $Form->DebugAusgabe(1,$DB->LetzterSQL());
            $Insert = 'INSERT';
            $Insert .= ' INTO EURECHNUNG';
            $Insert .= '   (';
            $Insert .= '     ERE_EKU_KEY,';
            $Insert .= '     ERE_ERK_ID,';
            $Insert .= '     ERE_STORNO,';
            $Insert .= '     ERE_USER,';
            $Insert .= '     ERE_USERDAT';
            $Insert .= '   )';
            $Insert .= '   VALUES';
            $Insert .= '   (';
            $Insert .=    $DB->WertSetzen('ERE','N0', $_POST['txtEKU_KEY']);
            $Insert .= '     ,' . $DB->WertSetzen('ERE', 'T', $_POST['sucERK_ID']);
            $Insert .= ',1';
            $Insert .= ' ,' . $DB->WertSetzen('ERE', 'T', $AWISBenutzer->BenutzerName());
            $Insert .= ' ,sysdate';
            $Insert .= '   )';

            $DB->Ausfuehren($Insert, '', '', $DB->Bindevariablen('ERE'));
            $Form->DebugAusgabe(1,$DB->LetzterSQL());
            $Meldung = $TXT_Speichern['ERE']['ERE_SPEICHERN_OK'];
        }
    } elseif ($AWIS_KEY1 > 0 and $AWIS_KEY2 == '') {//Ge�nderter KopfDS. KopfDS Kann nur ge�ndert wreden, wenn man nicht in der Positionsanlage/Bearbeitung war

        $Update = 'UPDATE EURECHNUNGSKOPF';
        $Update .= ' SET ';
        $Update .= '  ERK_DATUM   = ' . $DB->WertSetzen('ERK', 'D', $_POST['txtERK_DATUM']);;
        $Update .= ' , ERK_FIL_ID  = ' . $DB->WertSetzen('ERK', 'N0', $_POST['txtERK_FIL_ID']);;
        $Update .= ' , ERK_USER    = ' . $DB->WertSetzen('ERK', 'T', $AWISBenutzer->BenutzerName());;
        $Update .= ' , ERK_USERDAT = sysdate';
        $Update .= ' WHERE ERK_ID = ' . $DB->WertSetzen('ERK', 'T', $AWIS_KEY1);

        $DB->Ausfuehren($Update, '', '', $DB->Bindevariablen('ERK'));

        $Update = 'UPDATE EURECHNUNG';
        $Update .= ' SET ';
        $Update .= ' ERE_EKU_KEY = ' . $DB->WertSetzen('ERE', 'T', $_POST['txtEKU_KEY']);;
        $Update .= ' , ERE_STORNO  = 0';
        $Update .= ' , ERE_USER    = ' . $DB->WertSetzen('ERE', 'T', $AWISBenutzer->BenutzerName());;
        $Update .= ' , ERE_USERDAT = sysdate';
        $Update .= ' where  ERE_ERK_ID  = ' . $DB->WertSetzen('ERE', 'T', $AWIS_KEY1);

        $DB->Ausfuehren($Update, '', '', $DB->Bindevariablen('ERE'));
        $Meldung = $TXT_Speichern['ERE']['ERE_SPEICHERN_OK'];
    }

    $NeueReihenfolge = $Form->NameInArray($_POST, 'newERP', 2, 1);

    if (count($NeueReihenfolge) > 0) {
        foreach ($NeueReihenfolge as $Feld) {
            $alt = $_POST['old' . substr($Feld, 3)];
            $neu = $_POST['new' . substr($Feld, 3)];
            $ERP_KEY = substr($Feld,6);

            if ($alt != $neu) {
                $SQL = 'Update EURECHNUNGSPOSITIONEN set ERP_POSITION = ' . $DB->WertSetzen('ERP','Z',$neu);
                $SQL .= ' WHERE ERP_KEY = ' . $DB->WertSetzen('ERP','Z',$ERP_KEY);
                $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ERP'));
            }
        }
    }


    if ($AWIS_KEY2 != '') { //Positionsdaten
        if (mb_strtoupper($_POST['sucERP_AST_ATUNR']) == 'PREISSENKUNG') {
            $Menge = 1;

            $txtERP_BETRAG = str_replace(',','.',$_POST['txtERP_BETRAG_NETTO'])<=0?$_POST['txtERP_BETRAG_NETTO']:'-'.$_POST['txtERP_BETRAG_NETTO'];
            $txtERP_BETRAG_NETTO = str_replace(',','.',$_POST['txtERP_BETRAG_NETTO'])<=0?$_POST['txtERP_BETRAG_NETTO']:'-'.$_POST['txtERP_BETRAG_NETTO'];
            $txtERP_ZUSATZTEXT = '';
            $txtERP_VK_ORG = 0;
        } else {
            $Menge = $_POST['txtERP_MENGE'];
            $txtERP_BETRAG =  str_replace(',','.',$_POST['txtERP_BETRAG']);
            $txtERP_BETRAG_NETTO =  str_replace(',','.',$_POST['txtERP_BETRAG_NETTO']);
            $txtERP_ZUSATZTEXT = $_POST['txtERP_ZUSATZTEXT'];
            if (isset($_POST['txtERP_VK_ORG'])) {
                $txtERP_VK_ORG = $_POST['txtERP_VK_ORG'];
            } else {
                $txtERP_VK_ORG = 0;
            }
        }

        //AWIS_KEY2
        if ($AWIS_KEY2 == -1) {//Neue Position
            $PositionsNrSQL = 'select count(*) as anz from EURECHNUNGSPOSITIONEN where ERP_ERK_ID = ' . $DB->WertSetzen('ANZ', 'T', $AWIS_KEY1);

            $PosNr = $DB->RecordSetOeffnen($PositionsNrSQL, $DB->Bindevariablen('ANZ'))->FeldInhalt(1) + 1;
            $Insert = 'INSERT';
            $Insert .= ' INTO EURECHNUNGSPOSITIONEN';
            $Insert .= '   (';
            $Insert .= '     ERP_ERK_ID,';
            $Insert .= '     ERP_POSITION,';
            $Insert .= '     ERP_AST_ATUNR,';
            $Insert .= '     ERP_MENGE,';
            $Insert .= '     ERP_VK_ORG,';
            $Insert .= '     ERP_VK_NEU,';
            $Insert .= '     ERP_VK_NETTO,';
            $Insert .= '     ERP_BETRAG_NETTO,';
            $Insert .= '     ERP_ZUSATZTEXT,';
            $Insert .= '     ERP_EST_KEY,';
            $Insert .= '     ERP_USER,';
            $Insert .= '     ERP_USERDAT';

            $Insert .= '   )';
            $Insert .= '   VALUES';
            $Insert .= '   (';
            $Insert .= $DB->WertSetzen('ERP', 'T', $AWIS_KEY1);
            $Insert .= '     ,' . $DB->WertSetzen('ERP', 'N0', $PosNr);
            $Insert .= ' ,  ' . $DB->WertSetzen('ERP', 'TU', $_POST['sucERP_AST_ATUNR']);
            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'N1', $Menge);
            //Auf N2 umstellen, wenn Bug behoben
            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'T', str_replace('.', ',', $txtERP_VK_ORG));
            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'T', str_replace('.', ',', $txtERP_BETRAG));
            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'T', str_replace('.', ',', $txtERP_BETRAG_NETTO));


            $BetragNetto = str_replace(',','.',$txtERP_BETRAG_NETTO);
            $Menge = str_replace(',','.',$Menge);
            $BetragNetto = $BetragNetto * $Menge;
            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'T', str_replace('.', ',',  $BetragNetto));

            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'T', $txtERP_ZUSATZTEXT);
            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'N0', $_POST['txtERP_EST_KEY']);
            $Insert .= '   ,' . $DB->WertSetzen('ERP', 'T', $AWISBenutzer->BenutzerName());;
            $Insert .= '   ,sysdate';
            $Insert .= '   ';
            $Insert .= '   )';

            $DB->Ausfuehren($Insert, '', true, $DB->Bindevariablen('ERP'));
            $AWIS_KEY2 = '';
        } elseif ($AWIS_KEY2 > 0) {//Ge�nderte Position

            $Update = 'UPDATE EURECHNUNGSPOSITIONEN';
            $Update .= ' SET ';
            $Update .= '    ERP_AST_ATUNR    = ' . $DB->WertSetzen('ERP', 'TU', $_POST['sucERP_AST_ATUNR']);
            $Menge = str_replace(',','.',$Menge);
            $Update .= '   , ERP_MENGE        = ' . $DB->WertSetzen('ERP', 'N1', $Menge);
            if (isset($_POST['txtERP_VK_ORG'])) {
                $Update .= '   ,ERP_VK_ORG=' . $DB->WertSetzen('ERP', 'T', str_replace('.', ',', $_POST['txtERP_VK_ORG']));
            }
            $Update .= '   ,ERP_VK_NEU=' . $DB->WertSetzen('ERP', 'N2',  $txtERP_BETRAG);
            $Update .= '   ,ERP_VK_NETTO=' . $DB->WertSetzen('ERP', 'N2', $txtERP_BETRAG_NETTO);
            $BetragNetto = str_replace(',','.',$txtERP_BETRAG_NETTO);
            $BetragNetto = $BetragNetto * $Menge;
            $Update .= '   ,ERP_BETRAG_NETTO=' . $DB->WertSetzen('ERP', 'N2', $BetragNetto);
            $Update .= '   , ERP_ZUSATZTEXT   = ' . $DB->WertSetzen('ERP', 'T', $txtERP_ZUSATZTEXT);
            $Update .= '   , ERP_USER         = ' . $DB->WertSetzen('ERP', 'T', $AWISBenutzer->BenutzerName());;
            $Update .= '   , ERP_USERDAT      = sysdate';
            $Update .= '   , ERP_EST_KEY      = ' . $DB->WertSetzen('ERP', 'N0', $_POST['txtERP_EST_KEY']);
            $Update .= ' WHERE ERP_KEY      = ' . $DB->WertSetzen('ERP', 'N0', $AWIS_KEY2);

            $DB->Ausfuehren($Update, '', '', $DB->Bindevariablen('ERP'));
        }
        $Meldung = $TXT_Speichern['ERE']['ERE_SPEICHERN_OK'];
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}

?>