<?php

global $Funktionen;

if(isset($_POST['cmdLoeschen_x'])) { //L�schung der Abteilung komplett

    //L�schen der Personen in Abteilung

    $SQL = "DELETE FROM Kontakteabteilungenzuordnungen ";
    $SQL .= " where kza_kab_key =" . $Funktionen->DB->WertSetzen("KAB", "Z", $_POST["txtAWIS_KEY1"]);
    $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("KAB"));

    //L�schen der Betreuer

    $SQL = "DELETE FROM Kontakteinfos ";
    $SQL .= " where KIN_WERT =" . $Funktionen->DB->WertSetzen("KAB", "Z", $_POST["txtAWIS_KEY1"]);
    $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("KAB"));

    //L�schen der Abteilung selbst

    $SQL = "DELETE FROM KONTAKTEABTEILUNGEN ";
    $SQL .= " where KAB_KEY =" . $Funktionen->DB->WertSetzen("KAB", "Z", $_POST["txtAWIS_KEY1"]);
    $Funktionen->DB->Ausfuehren($SQL, '', false, $Funktionen->DB->Bindevariablen("KAB"));

} elseif (isset($_GET['Bearb']) or isset($_GET['Bearb2'])){ //Alle Sachen, die per GET kommen

    $Ursprung = 0;

    if(isset($_GET['Bearb'])) { //Betreuung
        $Wert = $_GET['Bearb'];
        $Ursprung = 1;
    } else {
        $Wert = $_GET['Bearb2']; //Abteilungsmitarbeiter
        $Ursprung = 2;
    }

    $SQL = "Delete from ";
    if($Ursprung == 1) {
        $SQL.= " Kontakteinfos ";
    }elseif($Ursprung == 2){
        $SQL.= " Kontakteabteilungenzuordnungen ";
    }

    if($Ursprung == 1) {
        $SQL .= " WHERE KIN_WERT = ".$Funktionen->DB->WertSetzen("KAB","Z",$_GET['KAB_KEY']);
        if($Wert <> 0) {
            $SQL .= " AND KIN_KON_KEY = ".$Funktionen->DB->WertSetzen("KAB","Z",$Wert);
        }
    }elseif($Ursprung == 2){

        $SQL .= " WHERE KZA_KAB_KEY = ".$Funktionen->DB->WertSetzen("KAB","Z",$_GET['KAB_KEY']);
        if($Wert <> 0) {
            $SQL .= " AND KZA_KON_KEY = ".$Funktionen->DB->WertSetzen("KAB","Z",$Wert);
        }
    }

    $Funktionen->DB->Ausfuehren($SQL,'',false,$Funktionen->DB->Bindevariablen('KAB'));
}