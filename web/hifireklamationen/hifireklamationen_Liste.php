<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awisDokumente.php");
require_once("awis_forms.inc.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body>
<?php

global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWIS_KEY1;
global $AWIS_KEY2;

awis_Debug(1,$_REQUEST);

$Rechtestufe = awisBenutzerRecht($con, 1400);
		//  1=Einsehen
		//	2=Bearbeiten
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Reklamtionen_Hifi',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

if (isset($_REQUEST['txt_Bearb_ID_Save']))
{
	$txt_Bearb_ID=$_REQUEST['txt_Bearb_ID_Save'];
}

if(isset($_POST['cmdSpeichern_x'])) 
{
	//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
	//--->

	$SQL='';
	
	$SQL="SELECT * FROM REKLAMATIONENHIFI WHERE RKH_BEARB_ID='".$txt_Bearb_ID."'";
	$rsReklamationenhifi = awisOpenRecordset($con,$SQL);
	$rsReklamationenhifiZeilen = $awisRSZeilen;
	
	$SQL='';
	$txtHinweis='';
	
	if($rsReklamationenhifiZeilen==0)		// Keine Daten
	{
			awislogoff($con);
			die("<span class=HinweisText>Datensatz wurde nicht gefunden!</span>");
	}
	
	if($_POST['txtRMA_Nummer'] != $_POST['txtRMA_Nummer_old'])
	{
			if($rsReklamationenhifi['RKH_RMA_NUMMER'][0] != $_POST['txtRMA_Nummer_old'])
			{
					$txtHinweis.=',RMA-Nummer von '.$_POST['txtRMA_Nummer_old']. ' auf '.$rsReklamationenhifi['RKH_RMA_NUMMER'][0];
			}
			else
			{
					$SQL.=',RKH_RMA_NUMMER=\''.$_POST['txtRMA_Nummer'].'\'';
			}
	}
	
	if($_POST['txtBemerkung'] != $_POST['txtBemerkung_old'])
	{
			if($rsReklamationenhifi['RKH_BEMERKUNG'][0] != $_POST['txtBemerkung_old'])
			{
					$txtHinweis.=',Bermerkung von '.$_POST['txtBemerkung_old']. ' auf '.$rsReklamationenhifi['RKH_BEMERKUNG'][0];
			}
			else
			{
					$SQL.=',RKH_BEMERKUNG=\''.$_POST['txtBemerkung'].'\'';
			}
	}
		
	//<---
	
	// Update- Befehl
	
	if($txtHinweis=='' && $SQL!='')
	{
			
			$SQL.=',RKH_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			
			$SQL.=',RKH_USERDAT=SYSDATE';
	
			$SQL='UPDATE REKLAMATIONENHIFI SET ' .substr($SQL,1).' WHERE RKH_BEARB_ID='.$txt_Bearb_ID;
			
			if(awisExecute($con, $SQL)===FALSE)
			{
				awisErrorMailLink("hifireklamationen_Liste.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}
	}
	elseif($txtHinweis!='')
	{
			echo $txtHinweis;
			awislogoff($con);
			
			die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsReklamationenhifi['RKH_USER'][0] ." ge�ndert !</span>");
	}
	
	//*****************************************************
	// Dokumente speichern
	//*****************************************************
	if(isset($_POST['txtDOC_BEZEICHNUNG']) && $_POST['txtDOC_BEZEICHNUNG']!='')
	{
		$Felder = awis_NameInArray($_POST, 'txtDOC_',1,1);
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$DOC = new awisDokumente();
			$Zuordnungen = array('RKH'=>$_POST['txtRKH_KEY']);
			$AWIS_KEY2 = $DOC->DokumentSpeichern($_POST['txtDOC_KEY'],'DOC',$_POST['txtDOC_BEZEICHNUNG'],$_POST['txtDOC_DATUM'],$_POST['txtDOC_BEMERKUNG'],$AWISBenutzer->BenutzerID(),$Zuordnungen);
		}
	}
}

// Dokumente l�schen
if(isset($_GET['Del']) && (($Rechtestufe&32)==32))
{
	$DOC = new awisDokumente();
	$DOC->DokumentLoeschen($_GET['Del']);
	//$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerCAD',$AWISBenutzer->BenutzerName());
}

if ((isset($_REQUEST['BEARB_ID']) && $_REQUEST['BEARB_ID']!='') && ! isset($_POST['cmdAnzeigen_x'])) //Anzeigen der Details f�r Bearbeitungsnummer 
{
	if (isset($_POST['BEARB_ID']) && $_POST['BEARB_ID']!='')
	{
		$BEARB_ID=$_POST['BEARB_ID'];
	}
	else 
	{
		$BEARB_ID=$_REQUEST['BEARB_ID'];
	}
	
	echo '<form name=frmHifireklamationenListe method=post action=./hifireklamationen_Main.php?cmdAktion=Liste enctype="multipart/form-data">';
	echo "<table width=100% border=0><tr>";
	if (($Rechtestufe&2)==2)
	{
		echo "<td colspan=2 align=right><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>&nbsp;<input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	}else
	{
		echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	}
	echo "</tr>";
	echo "</table>";
	echo "<input type=hidden name=txt_Bearb_ID_Save  value='" . $BEARB_ID . "'>";
	echo "<input type=hidden name=BEARB_ID value='" . $BEARB_ID . "'>";
	
	//echo "<center><h3>HiFi-Reklamationsauftrag</h3></center>";
	
	$SQL="SELECT RKH_KEY, RKH_BEARB_ID, RKH_STATUS, RKH_DATUM, RKH_FIL_ID, FILIALEN.FIL_BEZ, RKH_ANTRAGART, RKH_VERKAEUFER, RKH_EINREICHER, RKH_REPARATURDIENST, RKH_REPARATURANSPRECHPARTNER, "; 
	$SQL.="RKH_REPARATURSTRASSE, RKH_REPARATURPLZ, RKH_REPARATURORT,RKH_REPARATURTELEFON, RKH_REPARATURFAX, RKH_REPARATUREMAIL, ";
	$SQL.="RKH_AST_ATUNR, AST_BEZEICHNUNGWW, RKH_GERAETETYP, RKH_GERAETENR,RKH_SERIALNR, RKH_SECURITYCODE, RKH_VERKAUFSDATUM, ";
	$SQL.="RKH_BEDIENTEIL, RKH_KABELSATZ, RKH_MATERIALZUBEHOER, RKH_VORFUEHRGERAET, RKH_GARANTIEREPARATUR, RKH_FABRIKNEU, ";
	$SQL.="RKH_REPARATURBERECHNUNG, RKH_REPARATURKOSTENOGRENZE, RKH_REPARATURKOSTENOBETRAG, RKH_KOSTENVORANSCHLAG, RKH_SONSTIGES, RKH_FEHLEREMPFANGSTEIL_LMK, "; 
	$SQL.="RKH_FEHLEREMPFANGSTEIL_FM, RKH_FEHLERLAUFWERK_LI, RKH_FEHLERLAUFWERK_RE, RKH_FEHLERLAUTSPRECHER_LI, RKH_FEHLERLAUTSPRECHER_RE, RKH_FEHLERZEITWEISE, ";
	$SQL.="RKH_FEHLERDISPLAY, RKH_FEHLEREMPFINDLICHKEIT, RKH_FEHLERSPEICHERUNG, RKH_FEHLERSUCHLAUF, RKH_FEHLERSD_DK, RKH_FEHLERHOEHEN_TIEFEN, ";
	$SQL.="RKH_FEHLERSTOERUNGEN, RKH_FEHLERCD_TUNER, RKH_FEHLERGLEICHLAUF, RKH_FEHLERGESCHWINDIGKEIT, RKH_FEHLERVOR_RUECKLAUF, RKH_FEHLERDOLBY, "; 
	$SQL.="RKH_FEHLERLAUTSTAERKE, RKH_FEHLERVERZERRUNGEN, RKH_FEHLERSONSTIGES, RKH_SONSTIGESDECODE, RKH_SONSTIGESEIGENTUM, RKH_SONSTIGESVORFUEHRGERAET, "; 
	$SQL.="RKH_SONSTIGESVERKAUFT, RKH_REPARATURKOSTEN, RKH_REPARATURBON_NR, RKH_REPARATURDATUM, RKH_REPARATURBETRAG, RKH_REPARATURBEGRUENDUNG, ";
	$SQL.="RKH_EIGENTUEMERNACHNAME, RKH_EIGENTUEMERVORNAME, RKH_EIGENTUEMERSTRASSE, RKH_EIGENTUEMERPLZ, RKH_EIGENTUEMERORT, RKH_EIGENTUEMERTELEFON, RKH_ABHOLDATUM, RKH_RMA_NUMMER, RKH_BEMERKUNG ";
	$SQL.="FROM REKLAMATIONENHIFI LEFT JOIN ARTIKELSTAMM ON (ARTIKELSTAMM.AST_ATUNR=REKLAMATIONENHIFI.RKH_AST_ATUNR) ";
	$SQL.="LEFT JOIN FILIALEN ON (FILIALEN.FIL_ID=REKLAMATIONENHIFI.RKH_FIL_ID) ";
	$SQL.="WHERE RKH_BEARB_ID='". $BEARB_ID ."'";
	
	$rsRekHifi = awisOpenRecordset($con,$SQL,true,true);
	$rsRekHifiZeilen = $awisRSZeilen;	
	
	for($i=0;$i<$rsRekHifiZeilen;$i++)
	{
		echo "<table width=100% rules=groups border=1>";
		echo '<tbody>';
     	echo '<tr>';
		echo '<td width=150 id=FeldBez>Filiale</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><a title='Filial-Details anzeigen' href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=" . $rsRekHifi['RKH_FIL_ID'][$i] . "&Zurueck=../hifireklamationen/hifireklamationen_Main.php?cmdAktion=Liste~~1~~BEARB_ID=".$_REQUEST['BEARB_ID'].">" . $rsRekHifi['RKH_FIL_ID'][$i] . " - " . $rsRekHifi['FIL_BEZ'][$i] . "</a></td>";
		echo '<td width=150 id=FeldBez>Status</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . ($rsRekHifi['RKH_STATUS'][$i]=='O'?'Offen':'Erledigt') . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=150 id=FeldBez>Bearbeitungs-Nr.</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_BEARB_ID'][$i] . '</td>';
		echo '<td width=150 id=FeldBez>Datum</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_DATUM'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=150 id=FeldBez>RMA-Nummer</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtRMA_Nummer size=30 value='" . $rsRekHifi['RKH_RMA_NUMMER'][$i] . "'><input type=hidden name=txtRMA_Nummer_old  value='" . $rsRekHifi['RKH_RMA_NUMMER'][$i] . "'></td>";
		echo '<td width=150 id=FeldBez>Kaufdatum</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_VERKAUFSDATUM'][$i] . '</td>';		
		echo '</tr>';
		echo '<tr>';
		echo '<td width=150 id=FeldBez>Artikel-Nr. / -Bez.</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><a title='Artikel-Details anzeigen' href=../ATUArtikel/artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=" . $rsRekHifi['RKH_AST_ATUNR'][$i] . "&Zurueck=../hifireklamationen/hifireklamationen_Main.php?cmdAktion=Liste~~1~~BEARB_ID=".$_REQUEST['BEARB_ID'].">" . $rsRekHifi['RKH_AST_ATUNR'][$i] . "</a>&nbsp; " . $rsRekHifi['AST_BEZEICHNUNGWW'][$i] . "</td>";
		echo '<td width=150 id=FeldBez><u>Versandanschrift</u></td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_REPARATURDIENST'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=150 id=FeldBez>Ger&auml;tebez. / -typ</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_GERAETETYP'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=150 id=FeldBez>Ger&auml;te-Nr</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_GERAETENR'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=150 id=FeldBez>Serien-Nr</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_SERIALNR'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=150 id=FeldBez>Security-Code</td>';
		echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_SECURITYCODE'][$i] . '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
		
		echo "<h4><u>Fehlerbeschreibung</u></h4>";
		
		echo "<table width=100% rules=all border=1>";
		echo '<tbody>';
     	echo '<tr>';
		echo '<td width=250 id=FeldBez>Empfangsteil (LMK/FM)</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLEREMPFANGSTEIL_LMK'][$i] . '</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLEREMPFANGSTEIL_FM'][$i] . '</td>';
		echo '<td width=250 id=FeldBez>Laufwerk (LI/RE)</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERLAUFWERK_LI'][$i] . '</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERLAUFWERK_RE'][$i] . '</td>';
		echo '<td width=250 id=FeldBez>Lautsprecher (LI/RE)</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERLAUTSPRECHER_LI'][$i] . '</td>';
		echo '<td width=' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERLAUTSPRECHER_RE'][$i] . '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
		
		echo "<p>";
			
		echo "<table width=100% rules=rows border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Display</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERDISPLAY'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>Suchlauf</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERSUCHLAUF'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>St&ouml;rungen</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERSTOERUNGEN'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>Geschwindigkeit</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERGESCHWINDIGKEIT'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>Lautst&auml;ke</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERLAUTSTAERKE'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Empfindlichkeit</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLEREMPFINDLICHKEIT'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>SD / DK</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERSD_DK'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>CD-Tuner</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERCD_TUNER'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>Vor-/ R&uuml;cklauf</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERVOR_RUECKLAUF'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>Verzerrungen</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERVERZERRUNGEN'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=200 id=FeldBez>Speicherung</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERSPEICHERUNG'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>H&ouml;hen / Tiefen</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERHOEHEN_TIEFEN'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>Gleichlauf</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERGLEICHLAUF'][$i] . '</td>';
		echo '<td width=200 id=FeldBez>Loudness / Dolby</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERDOLBY'][$i] . '</td>';
		echo '<td width=200></td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'></td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";			
		
		echo '<br>';
		
		echo "<table width=100% rules=all border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td width=50% id=FeldBez>ausf&uuml;hrliche Fehlerbeschreibung</td>';
		echo '<td width=50% id=FeldBez>Sonstiges</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=50%' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERZEITWEISE'][$i] . '</td>';
		echo '<td width=50%' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FEHLERSONSTIGES'][$i] . '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
		
		echo '<br>';
		
		echo "<table width=100% rules=all border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td width=250 id=FeldBez>Vorf�hrger&auml;t</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_SONSTIGESVORFUEHRGERAET'][$i] . '</td>';
		echo '<td width=250 id=FeldBez>Fabrikneu mit Zubeh&ouml;r</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_FABRIKNEU'][$i] . '</td>';
		echo '<td width=250 id=FeldBez>Rep.-Kosten Obergrenze</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_REPARATURKOSTENOGRENZE'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=250 id=FeldBez>Garantiereparatur</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_GARANTIEREPARATUR'][$i] . '</td>';
		echo '<td width=250 id=FeldBez>Rep. gegen Berechnung</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_REPARATURBERECHNUNG'][$i] . '</td>';
		echo '<td width=250 id=FeldBez>Kostenvoranschlag</td>';
		echo '<td width=*' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_KOSTENVORANSCHLAG'][$i] . '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
		
		echo '<br>';

		echo "<table width=100% rules=all border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td width=50% id=FeldBez>Sonstiges</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=50%' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_SONSTIGES'][$i] . '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
		
		echo '<br>';
		
		echo "<table width=100% rules=all border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td width=50% id=FeldBez>Materialzubeh�r</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=50%' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_MATERIALZUBEHOER'][$i] . '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
		
		echo '<br>';
		
		echo "<table width=100% rules=groups border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td width=25% id=FeldBez><u>Ger&auml;teeigent&uuml;mer</u></td>';
		echo '<td width=75% ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_EIGENTUEMERNACHNAME'][$i]  . ' ' . $rsRekHifi['RKH_EIGENTUEMERVORNAME'][$i]  . '&nbsp;| ' . $rsRekHifi['RKH_EIGENTUEMERSTRASSE'][$i]  . '&nbsp;| ' . $rsRekHifi['RKH_EIGENTUEMERPLZ'][$i]  . '&nbsp;' . $rsRekHifi['RKH_EIGENTUEMERORT'][$i]  . '&nbsp;| ' . $rsRekHifi['RKH_EIGENTUEMERTELEFON'][$i]  . '</td>';
		echo '</tr>';
     	echo '<tr>';
		echo '<td width=25% id=FeldBez><u>Reparatur-Kosten kassiert</u></td>';
		echo '<td width=75% ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_REPARATURKOSTEN'][$i] . '</td>';
		//echo '<td width=250 id=FeldBez><u>Ger&auml;teeigent&uuml;mer</u></td>';
		//echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_EIGENTUEMERNACHNAME'][$i]  . ' ' . $rsRekHifi['RKH_EIGENTUEMERVORNAME'][$i]  . '<br>' . $rsRekHifi['RKH_EIGENTUEMERSTRASSE'][$i]  . '<br>' . $rsRekHifi['RKH_EIGENTUEMERPLZ'][$i]  . ' ' . $rsRekHifi['RKH_EIGENTUEMERORT'][$i]  . '<br>' . $rsRekHifi['RKH_EIGENTUEMERTELEFON'][$i]  . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=25% id=FeldBez>Bonnummer</td>';
		echo '<td width=75% ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_REPARATURBON_NR'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=25% id=FeldBez>Rechnungsdatum</td>';
		echo '<td width=75% ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_REPARATURDATUM'][$i] . '</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td width=25% id=FeldBez>Rechnungsbetrag</td>';
		echo '<td width=75%	 ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsRekHifi['RKH_REPARATURBETRAG'][$i] . '</td>';
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";
		
		echo '<br>';
		
		echo "<table width=100% rules=groups border=1>";
		echo '<tbody>';
		echo '<tr>';
		echo '<td width=25% id=FeldBez>Bemerkung</td>';
		echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><textarea name=txtBemerkung rows=5 cols=100>" . $rsRekHifi['RKH_BEMERKUNG'][$i] . "</textarea><input type=hidden name=txtBemerkung_old  value='" . $rsRekHifi['RKH_BEMERKUNG'][$i] . "'></td>";
		echo '</tr>';
		echo '</tbody>';
		echo "</table>";

		echo '<input type="hidden" name="txtRKH_KEY" value="'.$rsRekHifi['RKH_KEY'][$i].'">';
		
		global $AWIS_KEY1;
		$AWIS_KEY1=$rsRekHifi['RKH_KEY'][$i];
		
		awis_Debug(1,$AWIS_KEY1);
		if (($Rechtestufe&4)>0)
		{	
			awis_FORM_Trennzeile('L');
			include('hifireklamationen_Liste_Dokumente.php');
		}
		echo "</form>";
		
	}	
	
}
else //Zweig f�r Listen-Aufbau nach Suche
{
	//awis_Debug(1,$_POST);
	
		if(isset($_POST['cmdSuche_x']))			
		{
			$Params[0] = $_POST['txtBearbeitungsnummer'];
			$Params[1] = $_POST['txtFilialNr'];	
			$Params[2] = $_POST['txtArtikelNr'];
			$Params[3] = $_POST['txtEigentuemer'];	
			$Params[4] = $_POST['txtRMA'];	
			$Params[5] = $_POST['txtGeraetenummer'];	
			$Params[6] = $_POST['txtGeraetetyp'];	
			
			
			awis_BenutzerParameterSpeichern($con, "Rekl_HiFi_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
		}
		else
		{
				$Params=explode(';',awis_BenutzerParameter($con, "Rekl_HiFi_Suche", $AWISBenutzer->BenutzerName()));
		}
		
		awis_ZeitMessung(0);
		
		$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

		$SQL = 'SELECT * FROM Reklamationenhifi';
		$Bedingung='';
		$Zusatz=FALSE;
		
		/*SQL-Zusatz Bearbeitungsnummer*/
		if($Params[0]!='')
		{
				$Bedingung='RKH_BEARB_ID ' . awisLIKEoderIST($Params[0],TRUE,FALSE,FALSE,0);
				$Zusatz=TRUE;
		}
		
		/*SQL-Zusatz Filial-Nummer*/
		if($Zusatz)
		{
				if($Params[1]!='')
				{
						$Bedingung.=' AND RKH_FIL_ID ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		else
		{
				if($Params[1]!='')
				{
						$Bedingung='RKH_FIL_ID ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		
		/*SQL-Zusatz Artikel-Nummer*/
		if($Zusatz)
		{
				if($Params[2]!='')
				{
						$Bedingung.=' AND RKH_AST_ATUNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		else
		{
				if($Params[2]!='')
				{
						$Bedingung='RKH_AST_ATUNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		
		/*SQL-Zusatz Ger�teeigent�mer*/
		if($Zusatz)
		{
				if($Params[3]!='')
				{
						$Bedingung.=' AND (RKH_EIGENTUEMERNACHNAME ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0). ' OR RKH_EIGENTUEMERVORNAME ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) .')';
						$Zusatz=TRUE;
				}
		}
		else
		{
				if($Params[3]!='')
				{
						$Bedingung='(RKH_EIGENTUEMERNACHNAME ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0). ' OR RKH_EIGENTUEMERVORNAME ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) .')';
						$Zusatz=TRUE;
				}
		}
		
		/*SQL-Zusatz RMA-Nummer*/
		if($Zusatz)
		{
				if($Params[4]!='')
				{
						$Bedingung.=' AND RKH_RMA_NUMMER ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		else
		{
				if($Params[4]!='')
				{
						$Bedingung='RKH_RMA_NUMMER ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		
		/*SQL-Zusatz Ger�tenummer*/
		if($Zusatz)
		{
				if($Params[5]!='')
				{
						$Bedingung.=' AND (RKH_GERAETENR ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0) .' OR RKH_SERIALNR ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0).')';
						$Zusatz=TRUE;
				}
		}
		else
		{
				if($Params[5]!='')
				{
						$Bedingung='(RKH_GERAETENR ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0).' OR RKH_SERIALNR ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0).')';
						$Zusatz=TRUE;
				}
		}
		
		/*SQL-Zusatz Ger�tetyp*/
		if($Zusatz)
		{
				if($Params[6]!='')
				{
						$Bedingung.=' AND RKH_GERAETETYP ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		else
		{
				if($Params[6]!='')
				{
						$Bedingung='RKH_GERAETETYP ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
						$Zusatz=TRUE;
				}
		}
		
		if($Bedingung!='')
		{
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
		}
		else
		{
				//$SQL .= ' WHERE ROWNUM<=' . $AwisBenuPara;
				echo "<br>";
				die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		
		$SQL .= ' ORDER BY RKH_STATUS DESC, RKH_FIL_ID, RKH_BEARB_ID';
		
		//var_dump($SQL);
		
		$rsHifiReklamationen = awisOpenRecordset($con,$SQL);
		$rsHifiReklamationenZeilen = $awisRSZeilen;
		
		if($rsHifiReklamationenZeilen==0)		// Keine Daten
		{
			//echo 'Es wurde kein Eintrag gefunden.';
			echo "<br>";
			die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		else
		{
			// �berschrift aufbauen
			echo "<table  width=100% id=DatenTabelle border=1><tr>";
		
			echo "<td id=FeldBez>Bearbeitungsnummer</td>";
			echo "<td id=FeldBez>Filial-Nummer</td>";
			echo "<td id=FeldBez>Artikel-Nummer</td>";
			echo "<td id=FeldBez>Eigent�mer</td>";
			echo "<td id=FeldBez>Status</td>";
			
			echo "</tr>";
			
			for($i=0;$i<$rsHifiReklamationenZeilen;$i++)
			{
				echo '<tr>';
				echo '<td id=TabellenZeileGrau><a href=./hifireklamationen_Main.php?cmdAktion=Liste&BEARB_ID=' . $rsHifiReklamationen['RKH_BEARB_ID'][$i] . '>' . $rsHifiReklamationen['RKH_BEARB_ID'][$i] . '</a></td>';
				echo '<td id=TabellenZeileGrau><a href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsHifiReklamationen['RKH_FIL_ID'][$i] . '>' . $rsHifiReklamationen['RKH_FIL_ID'][$i] .  '</a></td>';
				echo '<td id=TabellenZeileGrau><a href=../ATUArtikel/artikel_Main.php?cmdAktion=ArtikelInfos&ATUNR=' . $rsHifiReklamationen['RKH_AST_ATUNR'][$i] . '>' . $rsHifiReklamationen['RKH_AST_ATUNR'][$i] .  '</a></td>';
				echo '<td id=TabellenZeileGrau>' . $rsHifiReklamationen['RKH_EIGENTUEMERNACHNAME'][$i] . ' ' . $rsHifiReklamationen['RKH_EIGENTUEMERVORNAME'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsHifiReklamationen['RKH_STATUS'][$i] . '</td>';
				echo '</tr>';
				
			}
				
			print "</table>";
			
			if($i<$AwisBenuPara)
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden!';
			}
			else
			{
			echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
			}
		}
}

?>
</body>
</html>

