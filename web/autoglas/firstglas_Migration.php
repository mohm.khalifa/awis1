<?php

require_once('awisDatenbank.inc');
require_once('awisFirstglasFunktionen.php');
require_once('awisBenutzer.inc');

class FirstGlasMigration
{
    private static $instance = NULL; //OK
    private $_Funktionen;            //OK
    private $_DB;                    //OK
    protected $_AWISBenutzer;        //OK
    protected $pfadMigration = '/daten/daten/versicherungen/firstglas/Migration/';
    protected $aktMigrationsdateien = array();  //OK
    private $ber_Kopfsatz = array();            //OK
    protected $ber_Positionssatz = array();     //OK
    protected $ber_Stornosatz = array();        //OK
    private $unb_Kopfsatz = array();            //OK
    private $unb_Positionssatz = array();       //OK
    private $unb_Stornosatz = array();          //OK
    private $ber_temp_Positionssatz = array();  //OK
    private $temp_fehlerPositionssatz = array(); //OK
    protected $fehlerPositionssatz = array();    //OK
    protected $pruefeDuplikate = false;
    //Kassendaten werden aus den DWH - IMPORTIERT
    private $flagDatenDWH = True;
    protected $KassendatenFile = '';
    protected $lastKassendatenFile = '';
    protected $ber_Kassendaten = array();
    protected $unb_Kassendaten = array();
    protected $aktDateiKassendaten = array();
    protected $aktDateiBereitImport;
    protected $processingFlag = 0;
    protected $error_msg;
    protected $pruefen;
    private $Anzahl_DS_Positionssatz;
    protected $bol_checkPossatz;
    //protected $ber_Positionssatz = array();
    //protected $ber_Stornosatz = array();
    protected $aktZeile;
    protected $aktZeilennummer;
    protected $aktKopfnummer;
    protected $aktPosnummer;
    protected $aktStornonummer;
    protected $aktKassennummer;
    protected $posZeilen;
    protected $stornoZeilen;
    private $_VorgangNr;
    protected $checkPossatz;
    protected $XDI_KEY = '';
    protected $KassendatenVerarbeitung = 0;
    protected $countDuplikate = 0;
    protected $countDuplikatsEintrag = 0;

    private function __construct()
    { //OK
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Funktionen = new FirstglasFunktionen();
        $this->pruefen = new FirstglasFunktionen();
    }

    public function getInstance()
    {
        if (self::$instance === NULL)
        {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function __clone()
    {
        
    }

    public function setpfadMigration($pfadMigration)
    { //OK
        $this->pfadMigration = '';
        $this->pfadMigration = $pfadMigration;
    }

    //Funktion zum Setzen des ImportKeys

    public function setXDIKEY($XDIKEY)
    { //OK
        $this->XDI_KEY = '';
        $this->XDI_KEY = $XDIKEY;
    }

    public function getXDIKEY()
    {      //OK
        return $this->XDI_KEY;
    }

    public function getpfadMigration()
    { //OK
        return $this->pfadMigration;
    }

    public function starteImport()
    {
        //$this->_Funktionen->SchreibeStartZeit("runImport"); //OK

        $this->aktuelleMigrationsDateien(); //OK
        $this->readInArray();
        //$this->letzteImportierteMigrationsdatei(); //OK
        //READINARRAY
        //$this->_Funktionen->SchreibeEndeZeit("runImport"); //OK
    }

    public function aktuelleMigrationsDateien()
    {
        $checkFile = '';
        $element = 0;

        if ($handle = opendir($this->pfadMigration))
        {
            while (false !== ($file = readdir($handle)))
            {
                $checkFile = '';
                $checkFile = substr($file, 0, 3);

                if ($file == '.' || $file == '..')
                {
                    
                }
                else
                {
                    if ($checkFile == 'imp')
                    {
                        
                    }
                    else
                    {
                        $this->aktMigrationsdateien[$element] = $file;
                        $element++;
                    }
                }
            }
            var_dump($this->aktMigrationsdateien);
        }
    }

    private function readInArray()
    {
        $this->aktKopfnummer = 0;
        $this->aktPosnummer = 0;
        $this->aktStornonummer = 0;

        for ($i = 0; $i < count($this->aktMigrationsdateien); $i++)
        {
            $this->aktDateiBereitImport = $this->aktMigrationsdateien[$i];

            if (($fd = fopen($this->pfadMigration . $this->aktDateiBereitImport, 'r')) === false)
            {
                echo $this->error_msg = "Fehler beim �ffnen der Datei";
            }
            else
            {
                $this->aktZeilennummer = 1;

                while (!feof($fd))
                {
                    $this->aktZeile = fgets($fd);

                    if (substr($this->aktZeile, 0, 1) == 'K')
                    {
                        $this->unb_Kopfsatz = explode(';', $this->aktZeile);

                        $this->aktKopfnummer++;
                        $this->pruefeKopfArray();
                        //var_dump($this->unb_Kopfsatz);
                    }
                }
                fclose($fd);

                $fd = null;
                $this->posZeilen = 0;

                //$this->_Funktionen->SchreibeModulStatus("runImport","Start Import - Positionsdaten");

                if (($fd = fopen($this->pfadMigration . $this->aktDateiBereitImport, 'r')) === false)
                {
                    echo $this->error_msg = "Fehler beim �ffnen der Datei";
                }
                else
                {
                    $this->aktZeilennummer = 1;

                    while (!feof($fd))
                    {
                        $this->aktZeile = fgets($fd);

                        if (substr($this->aktZeile, 0, 1) == 'P')
                        {
                            $this->unb_Positionssatz[$this->posZeilen] = explode(';', $this->aktZeile);
                            $this->posZeilen++;
                            $this->aktPosnummer++;
                        }
                    }
                    $this->pruefePosArray();
                }
                fclose($fd);

                $fd = null;

                if (($fd = fopen($this->pfadMigration . $this->aktDateiBereitImport, 'r')) === false)
                {
                    echo $this->error_msg = "Fehler beim �ffnen der Datei";
                }
                else
                {
                    $this->_Funktionen->SchreibeModulStatus("runImport", "Start Import - Stornodaten");

                    while (!feof($fd))
                    {
                        $this->aktZeile = fgets($fd);

                        if (substr($this->aktZeile, 0, 1) == 'S')
                        {
                            $this->unb_Stornosatz = explode(';', $this->aktZeile);

                            $this->pruefeStornoArray();

                            $this->aktStornonummer++;
                        }
                    }
                }
                fclose($fd);

                echo "Anzahl Kopfdatens�tze" . $this->aktKopfnummer;
                echo "Anzahl Positionsdatens�tze" . $this->aktPosnummer;
                echo "Anzahl Stornodatens�tze" . $this->aktStornonummer;
            }
        }
    }

    private function pruefeKopfArray()
    {
        $this->bereinigtesKopfdatenArray();
        $this->speichereKopfsatz();
    }

    private function bereinigtesKopfdatenArray()
    {
        for ($element = 0; $element < count($this->unb_Kopfsatz); $element++)
        {
            $this->ber_Kopfsatz[$element] = $this->unb_Kopfsatz[$element];
        }

        //var_dump($this->ber_Kopfsatz);
    }

    private function pruefePosArray()
    {
        //echo "CHECKPOSARRAY";
        //echo "<br>";
        $STARTZEIT = time();
        //echo "START GRUPPENFUNKTION:".$STARTZEIT;

        $Pruefe_Anzahl = false;
        $Pruef_Array_Anzahl = Array();
        $Pruefe_Alle_Anzahl = false;
        $Pruef_Array_Oempreis = Array();
        $Pruefe_Alle_Oempreis = false;
        $Pruef_Zaehler = 0;

        $pruef_Anzahl = false;
        $pruef_Oempreis = false;

        $Pruefe_Oempreis = false;

        echo "<br>";
        echo "Anzahl unb_Datens�tze:" . count($this->unb_Positionssatz);
        echo "<br>";

        $count = 0;
        $count_True = 0;
        $count_Fehler = 0;

        for ($i = 0; $i < count($this->unb_Positionssatz); $i++)
        {
            array_splice($this->temp_fehlerPositionssatz, 0);

            //$Pruefe_Anzahl = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][4]);
            //$Pruefe_Oempreis = $this->pruefen->checkNumber($this->unb_Positionssatz[$i][6]);
            //Pruefungen f�r MIGRATION deaktiviert

            $Pruefe_Anzahl = true;
            $Pruefe_Oempreis = true;

            if ($Pruefe_Anzahl == true && $Pruefe_Oempreis == true)
            {
                $count_True++;
            }
            else
            {
                //Schreibe VorgangsNr in Fehlerarray mit Zeitstempel
                $count_Fehler++;

                //Kopiere Fehlerhaften DS
                for ($y = 0, $z = 0; $y < count($this->unb_Positionssatz[$i]); $y++, $z++)
                {
                    $this->temp_fehlerPositionssatz[$z] = $this->unb_Positionssatz[$i][$y];
                }

                $this->fehlerPositionsdatensatz();
            }
            $count++;
        }
        $anz = 0;
        $anz = $this->getAnzahlFehlerhaftePositionsdaten();

        if ($anz == 0)
        {
            $this->kopiereUNBZUBEREINIGT();
            $this->anzahlPositionsdaten();
        }
        else
        {
            $this->bereinigePositionsdaten();
            $this->anzahlPositionsdaten();
        }

        $LAUFZEIT = time() - $STARTZEIT;
        echo "TOTAL DURATION OF THIS FUNCTION:" . $LAUFZEIT . "Seconds";

        //ANPASSEN
        //---------------------------------------------------------------------------

        $this->speicherePossatz();
    }

    public function fehlerPositionsdatensatz()
    { //OK
        $r = count($this->fehlerPositionssatz);

        for ($y = 0; $y < count($this->temp_fehlerPositionssatz); $y++)
        {
            $this->fehlerPositionssatz[$r][$y] = $this->temp_fehlerPositionssatz[$y];
        }
    }

    public function getAnzahlFehlerhaftePositionsdaten()
    {
        $anz = count($this->fehlerPositionssatz);

        return $anz;
    }

    public function bereinigePositionsdaten()
    {
        array_splice($this->temp_fehlerPositionssatz, 0);

        for ($i = 0; $i < count($this->unb_Positionssatz); $i++)
        {
            $flag_Fehler = false;

            for ($z = 0; $z < count($this->fehlerPositionssatz); $z++)
            {
                if ($this->unb_Positionssatz[$i][1] == $this->fehlerPositionssatz[$z][1] && $this->unb_Positionssatz[$i][8] == $this->fehlerPositionssatz[$z][8])
                {
                    $flag_Fehler = true;

                    $anzTempPositionssatz = count($this->temp_fehlerPositionssatz);

                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][0] = $this->unb_Positionssatz[$i][0];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][1] = $this->unb_Positionssatz[$i][1];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][2] = $this->unb_Positionssatz[$i][2];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][3] = $this->unb_Positionssatz[$i][3];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][4] = $this->unb_Positionssatz[$i][4];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][5] = $this->unb_Positionssatz[$i][5];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][6] = $this->unb_Positionssatz[$i][6];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][7] = $this->unb_Positionssatz[$i][7];
                    $this->temp_fehlerPositionssatz[$anzTempPositionssatz][8] = $this->unb_Positionssatz[$i][8];

                    $i++;
                    $z--;
                }
                else
                {
                    if ($flag_Fehler != true)
                    {
                        $element = count($this->ber_Positionssatz);

                        for ($x = 0; $x < count($this->unb_Positionssatz[$i]); $x++)
                        {
                            $this->ber_Positionssatz[$element][$x] = $this->unb_Positionssatz[$i][$x];
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
        }
        $this->loescheFehlerPositionssatz();
        $this->fehlerBerPositionsdatensatz();
        $this->zeigePositionsdatenFehler();
    }

    public function loescheFehlerPositionssatz()
    {
        array_splice($this->fehlerPositionssatz, 0);
    }

    public function fehlerBerPositionsdatensatz()
    {
        for ($i = 0; $i < count($this->temp_fehlerPositionssatz); $i++)
        {
            for ($y = 0; $y < count($this->temp_fehlerPositionssatz[$i]); $y++)
            {
                $this->fehlerPositionssatz[$i][$y] = $this->temp_fehlerPositionssatz[$i][$y];
            }
        }
    }

    public function zeigePositionsdatenFehler()
    { //OK
        var_dump($this->fehlerPositionssatz);
    }

    public function anzahlPositionsdaten()
    {
        echo "Anzahl Positionsdaten:" . count($this->ber_Positionssatz);
    }

    public function kopiereUNBZUBEREINIGT()
    {
        for ($i = 0; $i < count($this->unb_Positionssatz); $i++)
        {
            for ($y = 0; $y < count($this->unb_Positionssatz[$i]); $y++)
            {
                $this->ber_Positionssatz[$i][$y] = $this->unb_Positionssatz[$i][$y];
            }
        }
    }

    private function pruefeStornoArray()
    {

        //Pruefungen deaktiviert
        //---------------------------------------------------------
        //$SB = ctype_digit($this->unb_Stornosatz[9]);
        //$SB = $this->pruefen->pruefeSB($this->unb_Stornosatz[9]);
        //$this->unb_Stornosatz[9] = $SB;
        //---------------------------------------------------------

        $this->copyToCleanStornoArray();
        array_splice($this->unb_Stornosatz, 0);
        $this->speichereStornosatz();
    }

    private function copyToCleanStornoArray()
    {
        for ($element = 0; $element < count($this->unb_Stornosatz); $element++)
        {
            $this->ber_Stornosatz[$element] = $this->unb_Stornosatz[$element];
        }
        //var_dump($this->unb_Stornsatz);
    }

    private function speichereKopfsatz()
    {
        //var_dump($this->ber_Kopfsatz);

        $ImportFile = $this->getXDIKEY();

        $SQL = "Select * from FGKOPFDATEN";
        $SQL .= " WHERE FGK_VORGANGNR=" . $this->ber_Kopfsatz[2];

        $rsKopfDaten = $this->_DB->RecordSetOeffnen($SQL);

        if ($rsKopfDaten->FeldInhalt('FGK_VORGANGNR') == '')
        {
            if ($this->ber_Kopfsatz[19] != '')
            {
                $SQL = "INSERT INTO FGKOPFDATEN (FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,";
                $SQL .= "FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,";
                $SQL .= "FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,";
                $SQL .= "FGK_KUNDENNAME,FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_DATUMZUGANG,FGK_USER,FGK_USERDAT) VALUES (";
                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[0], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Kopfsatz[1], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[3], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[4], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[5], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[6], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[7], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[8], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Kopfsatz[9], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[10], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[11], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Kopfsatz[12], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[13], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $ImportFile, false);
                $SQL .= ',null';
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N', $this->ber_Kopfsatz[16], false);

                if ($this->ber_Kopfsatz[17] != 0)
                {
                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N', $this->ber_Kopfsatz[17], false);
                }
                else
                {
                    $SQL .= ',null';
                }
                if ($this->ber_Kopfsatz[19] != '')
                {
                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[19], false);
                }
                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                array_splice($this->ber_Kopfsatz, 0);
            }
            else
            {
                $SQL = "INSERT INTO FGKOPFDATEN (FGK_KENNUNG,FGK_FILID,FGK_VORGANGNR,";
                $SQL .= "FGK_KFZBEZ,FGK_KFZKENNZ,FGK_KBANR,FGK_FAHRGESTELLNR,FGK_VERSICHERUNG,";
                $SQL .= "FGK_VERSSCHEINNR,FGK_SB,FGK_MONTAGEDATUM,FGK_ZEITSTEMPEL,FGK_STEUERSATZ,";
                $SQL .= "FGK_KUNDENNAME,FGK_IMP_KEY,FGK_ZEITSTEMPEL_KASSIERT,FGK_FGN_KEY,FGK_FCN_KEY,FGK_USER,FGK_USERDAT) VALUES (";
                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[0], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Kopfsatz[1], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[3], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[4], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[5], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[6], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[7], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[8], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Kopfsatz[9], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[10], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[11], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Kopfsatz[12], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[13], false);
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $ImportFile, false);
                $SQL .= ',null';
                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N', $this->ber_Kopfsatz[16], false);

                if ($this->ber_Kopfsatz[17] != 0)
                {
                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N', $this->ber_Kopfsatz[17], false);
                }
                else
                {
                    $SQL .= ',null';
                }
                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                array_splice($this->ber_Kopfsatz, 0);
            }
        }
        else
        {
            if ($rsKopfDaten->FeldInhalt('FGK_VORGANGNR') == $this->ber_Kopfsatz[2])
            {
                $wand_Datum = strtotime($this->ber_Kopfsatz[11]);
                //$wand_Datum = date('d.m.Y H:i:s',$wand_Datum);

                /*
                  echo "<br>";
                  echo $rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL');
                  echo "<br>";
                  echo $wand_Datum;
                  echo "<br>";
                 */

                if ($rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL') == $wand_Datum)
                {
                    
                }
                else
                {
                    if (strtotime($rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL')) < $wand_Datum)
                    {
                        $this->ber_Kopfsatz[2];

                        $SQLCHECKHISTORY = 'Select * from FGKOPFDATEN_HIST WHERE FKH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[11], false);

                        $rsCheck_History = $this->_DB->RecordSetOeffnen($SQLCHECKHISTORY);

                        if ($rsCheck_History->AnzahlDatensaetze() == 0)
                        {
                            $SQL = 'INSERT INTO FGKOPFDATEN_HIST SELECT * FROM FGKOPFDATEN WHERE ';
                            $SQL .= 'FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);

                            $SQL = 'Select * from FGKOPFDATEN_HIST WHERE ';
                            $SQL .= ' FKH_VORGANGNR= ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);
                            $SQL .= ' AND FKH_ZEITSTEMPEL= ' . $this->_DB->FeldInhaltFormat('DU', $rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL'));

                            $rsKey = $this->_DB->RecordSetOeffnen($SQL);

                            $SQL = 'Select * from FGPOSITIONSDATEN WHERE';
                            $SQL .= ' FGP_VORGANGNR= ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);
                            $SQL .= ' AND FGP_ZEITSTEMPEL= ' . $this->_DB->FeldInhaltFormat('DU', $rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL'));

                            $rsPosition = $this->_DB->RecordSetOeffnen($SQL);

                            while (!$rsPosition->EOF())
                            {
                                $SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST (FPH_KENNUNG,';
                                $SQL .= 'FPH_VORGANGNR,FPH_ARTNR,FPH_ARTBEZ,FPH_ANZAHL,FPH_EINHEIT,FPH_OEMPREIS,FPH_EKNETTO,';
                                $SQL .= 'FPH_ZEITSTEMPEL,FPH_FREMDWAEHRUNG,FPH_FKH_KEY,FPH_USER,FPH_USERDAT) VALUES(';
                                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $rsPosition->FeldInhalt('FGP_KENNUNG'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $rsPosition->FeldInhalt('FGP_VORGANGNR'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $rsPosition->FeldInhalt('FGP_ARTNR'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $rsPosition->FeldInhalt('FGP_ARTBEZ'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $rsPosition->FeldInhalt('FGP_ANZAHL'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $rsPosition->FeldInhalt('FGP_EINHEIT'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $rsPosition->FeldInhalt('FGP_OEMPREIS'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $rsPosition->FeldInhalt('FGP_EKNETTO'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $rsPosition->FeldInhalt('FGP_ZEITSTEMPEL'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', 0, false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $rsKey->FeldInhalt('FKH_KEY'), false);
                                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                $SQL .= ',SYSDATE';
                                $SQL .= ')';

                                $SQL = 'Delete from FGPOSITIONSDATEN WHERE';
                                $SQL .= ' FGP_VORGANGNR= ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);
                                $SQL .= '  AND FGP_ZEITSTEMPEL= ' . $this->_DB->FeldInhaltFormat('DU', $rsKopfDaten->FeldInhalt('FGK_ZEITSTEMPEL'));

                                $rsPosition->DSWeiter();
                            }
                        }
                        $ImportFile = $this->getXDIKEY();

                        $SQL = "Select * from FGKOPFDATEN";
                        $SQL .= " WHERE FGK_VORGANGNR=" . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);

                        $rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen($SQL);

                        $SQL = 'UPDATE FGKOPFDATEN SET ';
                        $SQL .= ' FGK_KENNUNG=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[0], false);
                        $SQL .= ',FGK_FILID=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Kopfsatz[1], false);
                        $SQL .= ',FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);
                        $SQL .= ',FGK_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[3], false);
                        $SQL .= ',FGK_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[4], false);
                        $SQL .= ',FGK_KBANR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[5], false);
                        $SQL .= ',FGK_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[6], false);
                        $SQL .= ',FGK_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[7], false);
                        $SQL .= ',FGK_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[8], false);
                        $SQL .= ',FGK_SB=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Kopfsatz[9], false);
                        $SQL .= ',FGK_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[10], false);
                        $SQL .= ',FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Kopfsatz[11], false);
                        $SQL .= ',FGK_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Kopfsatz[12], false);
                        $SQL .= ',FGK_KUNDENNAME=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[13], false);
                        $SQL .= ',FGK_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO', $ImportFile, true);
                        $SQL .= ',FGK_ZEITSTEMPEL_KASSIERT=' . $this->_DB->FeldInhaltFormat('DU', $rsKopfDatenUpdate->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'), true);
                        $SQL .= ',FGK_FGN_KEY=' . $this->_DB->FeldInhaltFormat('NO', $rsKopfDatenUpdate->FeldInhalt('FGK_FGN_KEY'), true);
                        $SQL .= ',FGK_FCN_KEY=' . $this->_DB->FeldInhaltFormat('NO', $rsKopfDatenUpdate->FeldInhalt('FGK_FCN_KEY'), true);
                        $SQL .= ',FGK_FREIGABEGRUND=' . $this->_DB->FeldInhaltFormat('T', $rsKopfDatenUpdate->FeldInhalt('FGK_FREIGABEGRUND'), true);
                        $SQL .= ',FGK_DATUMZUGANG=' . $this->_DB->FeldInhaltFormat('DU', $rsKopfDatenUpdate->FeldInhalt('FGK_DATUMZUGANG'), true);
                        $SQL .= ',FGK_USER=\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',FGK_USERDAT=sysdate';
                        $SQL .= ' WHERE FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Kopfsatz[2], false);
                    }
                }
            }
        }
    }

    public function speicherePossatz()
    {
        $this->countDuplikate = 0;
        $this->countDuplikatsEintrag = 0;

        $wandDatumPosition = '';
        $wandDatumPositionHist = '';

        $count_Pos = 0;
        $count_Hist = 0;

        $ImportFile = $this->getXDIKEY();

        echo "Anzahl DS unbereinigtes ARRAY:<br>";
        echo count($this->unb_Positionssatz);
        echo "------------------------------<br>";
        echo "Anzahl DS bereinigtes ARRAY:<br>";
        echo count($this->ber_Positionssatz);
        echo "--------------------------------<br>";
        echo "Anzahl DS Fehler Positionsdaten:<br>";
        echo count($this->fehlerPositionssatz);
        echo "--------------------------------<br>";

        for ($y = 0; $y < count($this->ber_Positionssatz); $y++)
        {
            $SQL = 'Select * from FGKOPFDATEN WHERE FGK_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1]);
            $SQL .= ' AND FGK_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);

            $wandDatumPosition = $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);
            $wandDatumPosition = substr($wandDatumPosition, 9, 19);

            $rsKopf = $this->_DB->RecordSetOeffnen($SQL);

            if ($rsKopf->AnzahlDatensaetze() == 1)
            {
                if ($rsKopf->FeldInhalt('FGK_VORGANGNR') == $this->ber_Positionssatz[$y][1] && $rsKopf->FeldInhalt('FGK_ZEITSTEMPEL') == $wandDatumPosition)
                {
                    $SQL = 'Select * from FGPOSITIONSDATEN WHERE ';
                    $SQL .= ' FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1]);
                    $SQL .= ' AND FGP_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2]);
                    $SQL .= ' AND FGP_OEMPREIS=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6]);
                    $SQL .= ' AND FGP_EKNETTO=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7]);
                    $SQL .= ' AND FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);

                    $checkVorhandenePositionsdaten = $this->_DB->RecordSetOeffnen($SQL);

                    //Wenn bereits die gleichen Positionsdaten vorhanden sind f�hre kein INSERT durch

                    if ($checkVorhandenePositionsdaten->AnzahlDatensaetze() == 0)
                    {
                        $SQL = "INSERT INTO FGPOSITIONSDATEN (FGP_KENNUNG,FGP_VORGANGNR,";
                        $SQL .= "FGP_ARTNR,FGP_ARTBEZ,FGP_ANZAHL,FGP_EINHEIT,FGP_OEMPREIS,";
                        $SQL .= "FGP_EKNETTO,FGP_ZEITSTEMPEL,FGP_FREMDWAEHRUNG,FGP_FGK_KEY,";
                        $SQL .= "FGP_USER,FGP_USERDAT) VALUES (";
                        $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][0], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][3], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][4], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][5], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', 0, false);
                        $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $rsKopf->FeldInhalt('FGK_KEY'), false);
                        $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',SYSDATE';
                        $SQL .= ')';

                        $count_Pos++;
                    }
                    else
                    {
                        //Erster Durchlauf
                        echo $this->pruefeDuplikate;

                        if ($this->pruefeDuplikate == false)
                        {
                            $SQL = 'Select * from FGPOSITIONSDATEN WHERE';
                            $SQL .= ' FGP_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2]);
                            $SQL .= ' AND FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);
                            $SQL .= ' AND FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1]);

                            $rsSummeDuplikate = $this->_DB->RecordSetOeffnen($SQL);

                            if ($rsSummeDuplikate->AnzahlDatensaetze() > 0)
                            {
                                $Anzahl = 0;
                                $anzFGPOS = 0;
                                $anzDuplikat = 0;

                                $anzFGPOS = $rsSummeDuplikate->FeldInhalt('FGP_ANZAHL');
                                $anzDuplikat = $this->ber_Positionssatz[$y][4];
                                $Anzahl = $anzFGPOS + $anzDuplikat;

                                $SQL = 'UPDATE FGPOSITIONSDATEN SET ';
                                $SQL .= ' FGP_KENNUNG=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][0], false);
                                $SQL .= ',FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                                $SQL .= ',FGP_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                                $SQL .= ',FGP_ARTBEZ=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][3], false);
                                $SQL .= ',FGP_ANZAHL=' . $this->_DB->FeldInhaltFormat('N2', $Anzahl, false);
                                $SQL .= ',FGP_EINHEIT=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][5], false);
                                $SQL .= ',FGP_OEMPREIS=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6], false);
                                $SQL .= ',FGP_EKNETTO=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7], false);
                                $SQL .= ',FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);
                                $SQL .= ',FGP_FREMDWAEHRUNG=' . $this->_DB->FeldInhaltFormat('N2', '0', false);
                                $SQL .= ',FGP_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO', $rsKopf->FeldInhalt('FGK_KEY'), false);
                                $SQL .= ',FGP_USER=\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                $SQL .= ',FGP_USERDAT=sysdate';
                                $SQL .= ' WHERE FGP_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                                $SQL .= ' AND FGP_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                                $SQL .= ' AND FGP_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);

                                //Speichere Duplikat in der Tabelle FGDUPLIKATE
                                //---------------------------------------------

                                $SQL = "INSERT INTO FGDUPLIKATE (FDP_KENNUNG,FDP_VORGANGNR,";
                                $SQL .= "FDP_ARTNR,FDP_ARTBEZ,FDP_ANZAHL,FDP_EINHEIT,FDP_OEMPREIS,";
                                $SQL .= "FDP_EKNETTO,FDP_ZEITSTEMPEL,FDP_FREMDWAEHRUNG,FDP_FGK_KEY,";
                                $SQL .= "FDP_DATEINAME,FDP_USER,FDP_USERDAT) VALUES (";
                                $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][0], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][3], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][4], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][5], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', 0, false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $rsKopf->FeldInhalt('FGK_KEY'), false);
                                $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->aktDateiBereitImport, false);
                                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                $SQL .= ',SYSDATE';
                                $SQL .= ')';

                                $this->countDuplikate++;

                                echo "<br>";
                                echo "DUPLIKATE-------------------";
                                echo "<br>";
                                echo $this->ber_Positionssatz[$y][0];
                                echo $this->ber_Positionssatz[$y][1];
                                echo $this->ber_Positionssatz[$y][2];
                                echo $this->ber_Positionssatz[$y][3];
                                echo $this->ber_Positionssatz[$y][4];
                                echo $this->ber_Positionssatz[$y][5];
                                echo $this->ber_Positionssatz[$y][6];
                                echo $this->ber_Positionssatz[$y][7];
                                echo $this->ber_Positionssatz[$y][8];
                                echo "<br>";
                                echo "-----------------------------";
                                echo "<br>";
                            }
                        }
                    }
                }
            }
            else
            {
                $wandDatumPositionHist = $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);
                $wandDatumPositionHist = substr($wandDatumPositionHist, 9, 19);

                $SQL = 'Select * from FGKOPFDATEN_HIST WHERE FKH_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1]);
                $SQL .= ' AND FKH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);

                $rsHistory = $this->_DB->RecordSetOeffnen($SQL);

                if ($rsHistory->AnzahlDatensaetze() == 1)
                {
                    if ($rsHistory->FeldInhalt('FKH_VORGANGNR') == $this->ber_Positionssatz[$y][1] && $rsHistory->FeldInhalt('FKH_ZEITSTEMPEL') == $wandDatumPositionHist)
                    {
                        $SQL = 'Select * from FGPOSITIONSDATEN_HIST WHERE ';
                        $SQL .= ' FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1]);
                        $SQL .= ' AND FPH_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2]);
                        $SQL .= ' AND FPH_OEMPREIS=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6]);
                        $SQL .= ' AND FPH_EKNETTO=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7]);
                        $SQL .= ' AND FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);

                        $checkVorhandenePositionsdatenHist = $this->_DB->RecordSetOeffnen($SQL);

                        if ($checkVorhandenePositionsdatenHist->AnzahlDatensaetze() == 0)
                        {
                            $SQL = 'INSERT INTO FGPOSITIONSDATEN_HIST (FPH_KENNUNG,';
                            $SQL .= 'FPH_VORGANGNR,FPH_ARTNR,FPH_ARTBEZ,FPH_ANZAHL,FPH_EINHEIT,FPH_OEMPREIS,FPH_EKNETTO,';
                            $SQL .= 'FPH_ZEITSTEMPEL,FPH_FREMDWAEHRUNG,FPH_FKH_KEY,FPH_USER,FPH_USERDAT) VALUES(';
                            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][0], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][3], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][4], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][5], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', 0, false);
                            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $rsHistory->FeldInhalt('FKH_KEY'), false);
                            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                            $SQL .= ',SYSDATE';
                            $SQL .= ')';

                            $count_Hist++;
                        }
                        else
                        {
                            //Berechnung DUPLIKATE f�r die History - Tabellen
                            //Erster Durchlauf
                            echo $this->pruefeDuplikate;

                            if ($this->pruefeDuplikate == false)
                            {
                                $SQL = 'Select * from FGPOSITIONSDATEN_HIST WHERE';
                                $SQL .= ' FPH_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2]);
                                $SQL .= ' AND FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8]);
                                $SQL .= ' AND FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1]);

                                $rsSummeDuplikate = $this->_DB->RecordSetOeffnen($SQL);

                                if ($rsSummeDuplikate->AnzahlDatensaetze() > 0)
                                {
                                    $Anzahl = 0;
                                    $anzFGPOS = 0;
                                    $anzDuplikat = 0;

                                    $anzFGPOS = $rsSummeDuplikate->FeldInhalt('FPH_ANZAHL');
                                    $anzDuplikat = $this->ber_Positionssatz[$y][4];
                                    $Anzahl = $anzFGPOS + $anzDuplikat;

                                    $SQL = 'UPDATE FGPOSITIONSDATEN_HIST SET ';
                                    $SQL .= ' FPH_KENNUNG=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][0], false);
                                    $SQL .= ',FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                                    $SQL .= ',FPH_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                                    $SQL .= ',FPH_ARTBEZ=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][3], false);
                                    $SQL .= ',FPH_ANZAHL=' . $this->_DB->FeldInhaltFormat('N2', $Anzahl, false);
                                    $SQL .= ',FPH_EINHEIT=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][5], false);
                                    $SQL .= ',FPH_OEMPREIS=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6], false);
                                    $SQL .= ',FPH_EKNETTO=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7], false);
                                    $SQL .= ',FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);
                                    $SQL .= ',FPH_FREMDWAEHRUNG=' . $this->_DB->FeldInhaltFormat('N2', '0', false);
                                    $SQL .= ',FPH_FKH_KEY=' . $this->_DB->FeldInhaltFormat('NO', $rsHistory->FeldInhalt('FKH_KEY'), false);
                                    $SQL .= ',FPH_USER=\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                    $SQL .= ',FPH_USERDAT=sysdate';
                                    $SQL .= ' WHERE FPH_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                                    $SQL .= ' AND FPH_ARTNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                                    $SQL .= ' AND FPH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);

                                    //Speichere Duplikat in der Tabelle FGDUPLIKATE
                                    //---------------------------------------------

                                    $SQL = "INSERT INTO FGDUPLIKATE (FDP_KENNUNG,FDP_VORGANGNR,";
                                    $SQL .= "FDP_ARTNR,FDP_ARTBEZ,FDP_ANZAHL,FDP_EINHEIT,FDP_OEMPREIS,";
                                    $SQL .= "FDP_EKNETTO,FDP_ZEITSTEMPEL,FDP_FREMDWAEHRUNG,FDP_FGK_KEY,";
                                    $SQL .= "FDP_DATEINAME,FDP_USER,FDP_USERDAT) VALUES (";
                                    $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][0], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][1], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Positionssatz[$y][2], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][3], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][4], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Positionssatz[$y][5], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][6], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Positionssatz[$y][7], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Positionssatz[$y][8], false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', 0, false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $rsKopf->FeldInhalt('FGK_KEY'), false);
                                    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->aktDateiBereitImport, false);
                                    $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                                    $SQL .= ',SYSDATE';
                                    $SQL .= ')';

                                    $this->countDuplikate++;

                                    echo "<br>";
                                    echo "DUPLIKATE-------------------";
                                    echo "<br>";
                                    echo $this->ber_Positionssatz[$y][0];
                                    echo $this->ber_Positionssatz[$y][1];
                                    echo $this->ber_Positionssatz[$y][2];
                                    echo $this->ber_Positionssatz[$y][3];
                                    echo $this->ber_Positionssatz[$y][4];
                                    echo $this->ber_Positionssatz[$y][5];
                                    echo $this->ber_Positionssatz[$y][6];
                                    echo $this->ber_Positionssatz[$y][7];
                                    echo $this->ber_Positionssatz[$y][8];
                                    echo "<br>";
                                    echo "-----------------------------";
                                    echo "<br>";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function speichereStornosatz()
    {
        $ImportFile = $this->getXDIKEY();

        $SQL = "Select * from FGSTORNODATEN";
        $SQL .= " WHERE FGS_VORGANGNR=" . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[2], false);

        $rsStornoDaten = $this->_DB->RecordSetOeffnen($SQL);
        if ($rsStornoDaten->FeldInhalt('FGS_VORGANGNR') == '')
        {
            $SQL = "INSERT INTO FGSTORNODATEN (FGS_KENNUNG,FGS_FILIALNR,FGS_VORGANGNR,";
            $SQL .= "FGS_KFZBEZ,FGS_KFZKENNZ,FGS_KBANR,FGS_FAHRGESTELLNR,FGS_VERSICHERUNG,";
            $SQL .= "FGS_VERSSCHEINNR,FGS_SB,FGS_MONTAGEDATUM,FGS_ZEITSTEMPEL,FGS_STEUERSATZ,";
            $SQL .= "FGS_AKTIV,FGS_FGK_KEY,FGS_IMP_KEY,FGS_USER,";
            $SQL .= "FGS_USERDAT) VALUES (";
            $SQL .= ' ' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[0], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Stornosatz[1], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[2], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[3], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[4], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[5], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[6], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[7], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[8], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Stornosatz[9], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Stornosatz[10], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Stornosatz[11], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Stornosatz[12], false);
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', 1, false);
            $SQL .= ',null';
            $SQL .= ',' . $this->_DB->FeldInhaltFormat('NO', $ImportFile, false);
            $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';

            array_splice($this->ber_Stornosatz, 0);
        }
        else
        {
            $ImportFile = $this->getXDIKEY();

            if ($rsStornoDaten->FeldInhalt('FGS_VORGANGNR') == $this->ber_Stornosatz[2])
            {
                $wand_Datum = strtotime($this->ber_Stornosatz[11]);
                //$wand_Datum = date('d.m.Y H:i:s',$wand_Datum);

                if (strtotime($rsStornoDaten->FeldInhalt('FGS_ZEITSTEMPEL')) == $wand_Datum)
                {
                    
                }
                else
                {
                    if (strtotime($rsStornoDaten->FeldInhalt('FGS_ZEITSTEMPEL')) < $wand_Datum)
                    {
                        $SQLCHECKHISTORY = 'Select * from FGSTORNODATEN_HIST WHERE FSH_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Stornosatz[11], false);

                        $rsCheck_History = $this->_DB->RecordSetOeffnen($SQLCHECKHISTORY);

                        if ($rsCheck_History->AnzahlDatensaetze() == 0)
                        {
                            $SQL = 'INSERT INTO FGSTORNODATEN_HIST SELECT * FROM FGSTORNODATEN WHERE';
                            $SQL .= ' FGS_VORGANGNR=' . $this->ber_Stornosatz[2];
                            //$SQL .= 'AND FGS_KEY='.$rsStornoDaten->FeldInhalt('FGS_KEY');
                        }

                        $SQL = "Select * from FGSTORNODATEN";
                        $SQL .= " WHERE FGS_VORGANGNR=" . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[2], false);

                        $rsKopfDatenUpdate = $this->_DB->RecordSetOeffnen($SQL);

                        $SQL = 'UPDATE FGSTORNODATEN SET ';
                        $SQL .= ' FGS_KENNUNG=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[0], false);
                        $SQL .= ',FGS_FILIALNR=' . $this->_DB->FeldInhaltFormat('NO', $this->ber_Stornosatz[1], false);
                        $SQL .= ',FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[2], false);
                        $SQL .= ',FGS_KFZBEZ=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[3], false);
                        $SQL .= ',FGS_KFZKENNZ=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[4], false);
                        $SQL .= ',FGS_KBANR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[5], false);
                        $SQL .= ',FGS_FAHRGESTELLNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[6], false);
                        $SQL .= ',FGS_VERSICHERUNG=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[7], false);
                        $SQL .= ',FGS_VERSSCHEINNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[8], false);
                        $SQL .= ',FGS_SB=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Stornosatz[9], false);
                        $SQL .= ',FGS_MONTAGEDATUM=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Stornosatz[10], false);
                        $SQL .= ',FGS_ZEITSTEMPEL=' . $this->_DB->FeldInhaltFormat('DU', $this->ber_Stornosatz[11], false);
                        $SQL .= ',FGS_STEUERSATZ=' . $this->_DB->FeldInhaltFormat('N2', $this->ber_Stornosatz[12], false);
                        $SQL .= ',FGS_AKTIV=' . $this->_DB->FeldInhaltFormat('NO', 1, false);
                        $SQL .= ',FGS_FGK_KEY=' . $this->_DB->FeldInhaltFormat('NO', $rsKopfDatenUpdate->FeldInhalt('FGS_FGK_KEY'), true);
                        $SQL .= ',FGS_IMP_KEY=' . $this->_DB->FeldInhaltFormat('NO', $ImportFile, true);
                        $SQL .= ',FGS_USER=\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ',FGS_USERDAT=sysdate';
                        $SQL .= ' WHERE FGS_VORGANGNR=' . $this->_DB->FeldInhaltFormat('T', $this->ber_Stornosatz[2], false);
                    }
                }
            }
        }
    }

}

//Ende Klasse Migration
?>