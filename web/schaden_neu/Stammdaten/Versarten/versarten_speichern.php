<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
	
	$AWISBenutzer = awisBenutzer::Init();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	
	if(isset($_POST['txtID']))
	{
			if($_POST['txtID'] == '-1')
			{	
					$SQL ='insert into ART ';
					$SQL .=' (ART,ARTBEZEICHNUNG,BEMERKUNGEN,AKTIV,ARTUSER,ARTUSERDAT)';
					$SQL .=' values';
					$SQL .='('.$DB->FeldInhaltFormat('T',$_POST['txtBES_ART']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEZEICHNUNG']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEMERKUNGEN']);
					if(isset($_POST['txtCHK_AKTIV']))
					{
						$SQL .= ',1';
					}
					else
					{
						$SQL .= ',0';
					}
					$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .=',sysdate)';
					
					$DBSCHAD->Ausfuehren($SQL,'',true);
					$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_NEUANLAGE']);
			}
			else 
			{
				$SQL ='update ART ';
				$SQL .= 'set ARTBEZEICHNUNG='.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEZEICHNUNG']);
				$SQL .= ', BEMERKUNGEN='.$DB->FeldInhaltFormat('T',$_POST['txtBES_BEMERKUNGEN']);
				if(isset($_POST['txtCHK_AKTIV']))
				{
					$SQL .= ', AKTIV=1';
				}
				else
				{
					$SQL .= ', AKTIV=0';
				}
				$SQL .= ', ARTUSER=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', ARTUSERDAT=SYSDATE';
				$SQL .=' where ART=\''.$_POST['txtID'].'\'';
				$DBSCHAD->Ausfuehren($SQL,'',true);		
				$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_SPEICHERN']);
			}
	}
}
catch (awisException $ex)
{
	
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (SCHADDEV09.PK_ART) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Hinweis = 'Art "'.$_POST['txtBES_ART'].'" befindet sich schon in der Datenbank.';
		if(isset($_POST['txtID']))
		{
			$AWIS_KEY1 = $_POST['txtID'];
		}
		$Form->Hinweistext($Hinweis);
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
