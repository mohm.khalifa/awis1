<?php

global $AWISCursorPosition;

try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FGK', '*');
    $TextKonserven[] = array('FKA', '*');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'ttt_AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'KassendatenSuchen');
    $TextKonserven[] = array('Wort', 'ttt_KassendatenSuchen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9100 = $AWISBenutzer->HatDasRecht(9100);  // Rechte des Mitarbeiters

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./autoglas_Main.php?cmdAktion=Details>");

    $Param = explode(";", ($AWISBenutzer->ParameterLesen('FirstglasSuche')));

    if (!is_array($Param))
    {
        $Param = array_fill(0, 20, '');
    }


    /*     * ********************************************
     * * Eingabemaske
     * ********************************************* */

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_FILID'] . ':', 240);
    $Form->Erstelle_TextFeld('*FGK_FILID', ($Param[0] == 'on' ? $Param[1] : ''), 4, 200, true, '', '', '', 'T', '', $AWISSprachKonserven['FGK']['FGK_FILID'], '', 50);
    $AWISCursorPosition = 'sucFGK_FILID';
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VORGANGNR'] . ':', 240);
    $Form->Erstelle_TextFeld('*FGK_VORGANGNR', ($Param[0] == 'on' ? $Param[2] : ''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['FGK']['FGK_VORGANGNR'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_WANR'] . ':', 240);
    $Form->Erstelle_TextFeld('*FKA_WANR', ($Param[0] == 'on' ? $Param[2] : ''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['FKA']['FKA_WANR'], '', 50);
    $Form->ZeileEnde();

    //Datum von --> bis

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_DATUMVON'] . ':', 240);
    $Form->Erstelle_TextFeld('*FKA_DATUMVON', ($Param[0] == 'on' ? $Param[2] : ''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['FKA']['FKA_DATUMVON'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_DATUMBIS'] . ':', 240);
    $Form->Erstelle_TextFeld('*FKA_DATUMBIS', ($Param[0] == 'on' ? $Param[2] : ''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['FKA']['FKA_DATUMBIS'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KFZKENNZ'] . ':', 240);
    $Form->Erstelle_TextFeld('*FGK_KFZKENNZ', ($Param[0] == 'on' ? $Param[3] : ''), 13, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['FGK']['FGK_KFZKENNZ'], '', 50);
    $Form->ZeileEnde();

    $Form->Trennzeile('L');

    //Kassendaten die nicht gemachtet werden anzeigen
    //--------------------------------------------------------------------------

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['KassendatenSuchen'] . ':', 240);
    $Form->Erstelle_Checkbox('*KassendatenSuchen', ($Param[0] == 'on' ? 'on' : ''), 30, true, 'on', '', $AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
    $Form->ZeileEnde();

    /*
      $Form->ZeileStart();
      $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',240);
      $Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param[0]=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
      $Form->ZeileEnde();
     */

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    /*
      if(($Recht9100&4) == 4)		// Hinzuf�gen erlaubt?
      {
      $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
      }
     */
    $Form->Schaltflaeche('script', 'cmdHilfe', "", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

    $Form->SchaltflaechenEnde();

    if ($AWISCursorPosition != '')
    {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();";

        // Javafunktion zum L�schen des Suchefeldes
        echo 'function ClearFeld(Feld)';
        echo '{';
        echo 'if (Feld == 2) {';
        echo "    document.getElementsByName(\"sucFGK_FILID\")[0].value='';";
        echo '} else {';
        echo "    document.getElementsByName(\"sucFGK_FILID\")[0].value='';";
        echo '}';
        echo '  return(0)';
        echo '}';
        echo '</Script>';
    }
}
catch (Exception $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200904291847");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>