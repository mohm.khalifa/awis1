<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);

date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class detefleet_Matching
{

    private $_Funktionen;
    private $_DB;
    protected $_AWISBenutzer;
    protected $preisListe = '';

    /**
     * detefleet_Matching constructor.
     * @throws awisException
     */
    public function __construct() {
      $this->_AWISBenutzer = awisBenutzer::Init();
      $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
      $this->_DB->Oeffnen();
    }

    /**
     * Setter fuer die Preisliste
     * @param $PreisListe
     */
    public function setPreisListe($PreisListe) {
        $this->preisListe = $PreisListe;
    }

    /**
     * Startet das Matching der Grunddaten zu den Artikeldaten
     */
    public function starteMatching() {
        //Matching anhand Warengruppen / Sortiment (Felgen...)
        $BindeVariablen=array();
        $BindeVariablen['var_N0_gkz_gkp_key']=$this->preisListe;

        $SQL = "Select * from GKPREISLISTEZUORD WHERE GKZ_WG IS NOT NULL AND GKZ_SG IS NOT NULL AND GKZ_AUTO='j'";
        $SQL .= " AND GKZ_GKP_KEY=:var_N0_gkz_gkp_key";

        $rsRSL = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        while(!$rsRSL->EOF()) {
            $BindeVariablen=array();
            $BindeVariablen['var_T_ast_wgr_id']=$rsRSL->FeldInhalt('GKZ_WG');
            $BindeVariablen['var_T_ast_wug_id']=$rsRSL->FeldInhalt('GKZ_SG');

            $SQL =  'SELECT AST_ATUNR, AST_BEZEICHNUNGWW, AST_WGR_ID, AST_WUG_ID FROM V_ARTIKELSTAMMWGR WHERE AST_WGR_ID=:var_T_ast_wgr_id';
            $SQL .= ' AND AST_WUG_ID=:var_T_ast_wug_id';

            $rsInsert = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

            while(!$rsInsert->EOF()) {
                //Bei Radkappen die VPE beruecksichtigen
                if ($rsInsert->FeldInhalt('AST_WGR_ID') == '07' and
                    $this->_DB->FeldInhaltFormat('Z', $rsInsert->FeldInhalt('AST_WUG_ID')) >= 500 and
                    $this->_DB->FeldInhaltFormat('Z', $rsInsert->FeldInhalt('AST_WUG_ID')) <= 599 and
                    strpos($rsInsert->FeldInhalt('AST_BEZEICHNUNGWW'),'VP1')!==false)
                {
                    //Wenn VPE = 1 --> Preis durch 4 dividieren
                    $Preis = $this->_DB->FeldInhaltFormat('N2',$rsRSL->FeldInhalt('GKZ_PREIS')) / 4;

                    $SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_PREIS,GKA_USER,GKA_USERDAT) VALUES (';
                    $SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$rsRSL->FeldInhalt('GKZ_KEY'));
                    $SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsInsert->FeldInhalt('AST_ATUNR'));
                    $SQL .= ','.$this->_DB->FeldInhaltFormat('N2',$Preis);
                    $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';
                }
                else
                {
                    $SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_USER,GKA_USERDAT) VALUES (';
                    $SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$rsRSL->FeldInhalt('GKZ_KEY'));
                    $SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsInsert->FeldInhalt('AST_ATUNR'));
                    $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';
                }

                $this->_DB->Ausfuehren($SQL);

                $rsInsert->DSWeiter();
            }

            $rsRSL->DSWeiter();
        }

        //Matching der Reifen
        $BindeVariablen=array();
        $BindeVariablen['var_N0_gkz_gkp_key']=$this->preisListe;

        $SQL = "Select * from GKPREISLISTEZUORD WHERE GKZ_AUTO='j' AND GKZ_WG IS NULL AND GKZ_SG IS NULL";
        $SQL .= " AND GKZ_GKP_KEY=:var_N0_gkz_gkp_key";
        $SQL .= " order by gkz_rx";

        $rsReifen = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        while(!$rsReifen->EOF())
        {
            $SQL  = "Select REI_AST_ATUNR FROM V_REIFENZUSATZINFOS WHERE";
            $SQL .= " REI_BREITE=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_BREITE'));
            $SQL .= " AND REI_QUERSCHNITT=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_QUERSCHNITT'));
            $SQL .= " AND (REI_BAUART=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_BAUART'));
            $SQL .= " OR REI_BAUART='ZR')";
            $SQL .= " AND REI_INNENDURCHMESSER=".$this->_DB->FeldInhaltFormat('NO',$rsReifen->FeldInhalt('GKZ_INNENDURCHMESSER'));
            $SQL .= " AND REI_LOADINDEX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_LOADINDEX1'));
            $SQL .= " AND REI_SPI_SYMBOL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SPEEDINDEX'));
            if($rsReifen->FeldInhalt('GKZ_GWS') != '')
            {
                $SQL .= " AND GWS=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_GWS'));
            }
            else
            {
                $SQL .= " AND GWS is null";
            }
            $SQL .= " AND RX=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_RX'));
            if($rsReifen->FeldInhalt('GKZ_ROF') != '')
            {
                $SQL .= " AND ROF=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_ROF'));
            }
            else
            {
                $SQL .= " AND ROF is null";
            }
            if($rsReifen->FeldInhalt('GKZ_SEAL') != '')
            {
                $SQL .= " AND SEAL=".$this->_DB->FeldInhaltFormat('T',$rsReifen->FeldInhalt('GKZ_SEAL'));
            }
            else
            {
                $SQL .= " AND SEAL is null";
            }

            $rsZuordnung = $this->_DB->RecordSetOeffnen($SQL);

            while(!$rsZuordnung->EOF())
            {
                $BindeVariablen=array();
                $BindeVariablen['var_N0_gka_gkz_key']=$rsReifen->FeldInhalt('GKZ_KEY');
                $BindeVariablen['var_T_gka_ast_atunr']=$rsZuordnung->FeldInhalt('REI_AST_ATUNR');
                $BindeVariablen['var_T_gka_user']=$this->_AWISBenutzer->BenutzerName();

                $SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_USER,GKA_USERDAT) VALUES';
                $SQL .= ' (:var_N0_gka_gkz_key,:var_T_gka_ast_atunr,:var_T_gka_user,SYSDATE)';

                $this->_DB->Ausfuehren($SQL,'',false,$BindeVariablen);

                $rsZuordnung->DSWeiter();
            }

            $rsReifen->DSWeiter();
        }
    }

    /**
     * Starte Matching fuer die verschiedenen R-Kennungen
     * @param $RX
     */
    public function starteMatchingRX($RX)
    {
        //Noch nicht zugeordnete R3 - Reifen nochmals ueberpruefen ob Zuordnung zu R1 oder R2 moeglich ist
        $BindeVariablen=array();
        $BindeVariablen['var_N0_gkz_gkp_key']=$this->preisListe;

        $SQL = "SELECT * ";
        $SQL.= " FROM V_REIFENZUSATZINFOS";
        $SQL.= " LEFT JOIN (SELECT * FROM GKPREISLISTEZUORD";
        $SQL.= " INNER JOIN GKATUNRZUORD ON GKA_GKZ_KEY = GKZ_KEY";
        $SQL.= " WHERE GKZ_GKP_KEY = :var_N0_gkz_gkp_key) ON REI_AST_ATUNR = GKA_AST_ATUNR";
        $SQL.= " WHERE RX= 'R3' AND GKA_KEY IS NULL";
        $rsReifenR3 = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        while (!$rsReifenR3->EOF())
        {
            $SQL  = "SELECT GKZ_KEY FROM GKPREISLISTEZUORD WHERE GKZ_AUTO='j' AND GKZ_WG IS NULL AND GKZ_SG IS NULL";
            $SQL .= " AND GKZ_GKP_KEY=".$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
            $SQL .= " AND GKZ_BREITE=".$this->_DB->FeldInhaltFormat('NO',$rsReifenR3->FeldInhalt('REI_BREITE'));
            $SQL .= " AND GKZ_QUERSCHNITT=".$this->_DB->FeldInhaltFormat('T',$rsReifenR3->FeldInhalt('REI_QUERSCHNITT'));
            $SQL .= " AND GKZ_BAUART=".$this->_DB->FeldInhaltFormat('T',$rsReifenR3->FeldInhalt('REI_BAUART'));
            $SQL .= " AND GKZ_INNENDURCHMESSER=".$this->_DB->FeldInhaltFormat('NO',$rsReifenR3->FeldInhalt('REI_INNENDURCHMESSER'));
            $SQL .= " AND GKZ_LOADINDEX1=".$this->_DB->FeldInhaltFormat('T',$rsReifenR3->FeldInhalt('REI_LOADINDEX'));
            if($rsReifenR3->FeldInhalt('REI_LOADINDEX2') != '')
            {
                $SQL .= " AND GKZ_LOADINDEX2=".$this->_DB->FeldInhaltFormat('NO',$rsReifenR3->FeldInhalt('REI_LOADINDEX2'));
            }
            else
            {
                $SQL .= " AND GKZ_LOADINDEX2 IS NULL";
            }
            $SQL .= " AND GKZ_SPEEDINDEX=".$this->_DB->FeldInhaltFormat('T',$rsReifenR3->FeldInhalt('REI_SPI_SYMBOL'));
            if($rsReifenR3->FeldInhalt('GWS') != '')
            {
                $SQL .= " AND GKZ_GWS=".$this->_DB->FeldInhaltFormat('T',$rsReifenR3->FeldInhalt('GWS'));
            }
            else
            {
                $SQL .= " AND GKZ_GWS IS NULL";
            }
            $SQL .= " AND GKZ_RX=".$this->_DB->FeldInhaltFormat('T',$RX);
            if($rsReifenR3->FeldInhalt('ROF') != '')
            {
                $SQL .= " AND GKZ_ROF=".$this->_DB->FeldInhaltFormat('T',$rsReifenR3->FeldInhalt('ROF'));
            }
            else
            {
                $SQL .= " AND GKZ_ROF IS NULL";
            }
            if($rsReifenR3->FeldInhalt('SEAL') != '')
            {
                $SQL .= " AND GKZ_SEAL=".$this->_DB->FeldInhaltFormat('T',$rsReifenR3->FeldInhalt('SEAL'));
            }
            else
            {
                $SQL .= " AND GKZ_SEAL IS NULL";
            }

            $rsZuordnungR3 = $this->_DB->RecordSetOeffnen($SQL);

            if ($rsZuordnungR3->AnzahlDatensaetze()==1)
            {
                $BindeVariablen=array();
                $BindeVariablen['var_N0_gka_gkz_key']=$rsZuordnungR3->FeldInhalt('GKZ_KEY');
                $BindeVariablen['var_T_gka_ast_atunr']=$rsReifenR3->FeldInhalt('REI_AST_ATUNR');
                $BindeVariablen['var_T_gka_user']=$this->_AWISBenutzer->BenutzerName();

                $SQL =  'INSERT INTO GKATUNRZUORD (GKA_GKZ_KEY,GKA_AST_ATUNR,GKA_USER,GKA_USERDAT) VALUES';
                $SQL .= ' (:var_N0_gka_gkz_key,:var_T_gka_ast_atunr,:var_T_gka_user,SYSDATE)';

                $this->_DB->Ausfuehren($SQL,'',false,$BindeVariablen);
            }

            $rsReifenR3->DSWeiter();
        }
    }

    /**
     * Startet die Anreicherung der Daten
     */
    public function starteAnreicherung()
    {
        $BindeVariablen=array();
        $BindeVariablen['var_N0_gkp_key']=$this->preisListe;

        $SQL = "Select GKP_GUELTIGAB, GKP_GUELTIGBIS";
        $SQL.= " From Gkpreisliste Aa";
        $SQL.= " WHERE aa.GKP_KEY = :var_N0_gkp_key";

        $rsGKP = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        //Reifen mit Kenn 1 = E, die nicht ermittelt werden konnten

        $BindeVariablen=array();
        $BindeVariablen['var_N0_gkz_gkp_key']=$this->preisListe;

        $SQL = 'select * ';
        $SQL.= ' from artikelstamm aa';
        $SQL.= ' inner join v_artikelstammwgr a on a.ast_atunr = aa.ast_atunr and (a.ast_wgr_id = \'01\' or a.ast_wgr_id = \'02\' or (a.ast_wgr_id = \'23\' and (a.ast_wug_id = 200 or a.ast_wug_id = 201)))';
        $SQL.= ' left join gkatunrzuord g on G.GKA_AST_ATUNR = AA.AST_ATUNR';
        $SQL.= ' and G.GKA_GKZ_KEY in';
        $SQL.= ' (select p.gkz_key';
        $SQL.= ' from gkpreislistezuord p';
        $SQL.= ' inner join gkpreisliste l on L.GKP_KEY = P.GKZ_GKP_KEY';
        $SQL.= ' inner join gkkunde k on K.GKD_KEY = 1';
        $SQL.= ' where GKZ_WG IS NOT NULL AND GKZ_SG IS NOT NULL AND GKZ_AUTO=\'j\' and GKZ_GKP_KEY=:var_N0_gkz_gkp_key)';
        $SQL.= ' where aa.ast_kennung = \'E\' and G.GKA_KEY is null';

        $rsAST = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        while (!$rsAST->EOF())
        {
            //Letzten gueltigen Preis von dem Artikel vom COM-Server holen
            $BindeVariablen=array();
            $BindeVariablen['var_T_ast_atunr']=$rsAST->FeldInhalt('AST_ATUNR');

            $SQL = 'select * ';
            $SQL.= ' from experian_atu.EXP_FESTPREISLISTEN_TAB@com_de.atu.de';
            $SQL.= ' where id_fplb = 23 and artnr=:var_T_ast_atunr';
            $SQL.= ' order by transaktionsid desc';

            $rsEXP = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

            if (!$rsEXP->EOF())
            {
                $SQL = 'INSERT INTO GKATUNRANREICHERUNG (GKR_GKP_KEY,GKR_AST_ATUNR,GKR_PREIS,GKR_GUELTIGAB,GKR_GULTIGBIS,GKR_TRANSAKTIONSID,GKR_USER,GKR_USERDAT)';
                $SQL .= ' VALUES(';
                $SQL .= ' '.$this->_DB->FeldInhaltFormat('NO',$this->preisListe);
                $SQL .= ','.$this->_DB->FeldInhaltFormat('T',$rsEXP->FeldInhalt('ARTNR'));
                $SQL .= ','.$this->_DB->FeldInhaltFormat('N2',$rsEXP->FeldInhalt('PREIS'));
                $SQL .= ','.$this->_DB->FeldInhaltFormat('D',$rsGKP->FeldInhalt('GKP_GUELTIGAB'));
                $SQL .= ','.$this->_DB->FeldInhaltFormat('D',$rsGKP->FeldInhalt('GKP_GUELTIGBIS'));
                $SQL .= ','.$this->_DB->FeldInhaltFormat('NO',$rsEXP->FeldInhalt('TRANSAKTIONSID'));
                $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                $this->_DB->Ausfuehren($SQL);
            }

            $rsAST->DSWeiter();

        }
    }

}


?>