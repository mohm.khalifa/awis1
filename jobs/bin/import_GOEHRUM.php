<?php
require_once('/daten/include/awisZukauf_GOEHRUM.inc');

try {
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;
    $WANummer = '';
    $Abfrage = 'Bestellung';
    $AlleAbfragen = false;
    $Ignore = array();

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            case '--starttag':
                $StartTag = $argv[++$i];
                break;
            case '--endtag':
                $EndTag = $argv[++$i];
                break;
            case '--abfrage':
                $Abfrage = $argv[++$i];
                break;
            case '--ignore':
                $Ignore[$argv[++$i]] = true;
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.' . PHP_EOL;
                echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.' . PHP_EOL;
                echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung, Lieferschein oder Rechnung.' . PHP_EOL;
                echo '  --ignore  [Ignoretyp]              Was soll ignoriert werden. Werte. FTP' . PHP_EOL;

                echo PHP_EOL;
                die();
        }
    }


    $AWISUser = awisBenutzer::init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $ZUK = new awisZukauf_GOEHRUM($DebugLevel, $Testsystem);

    if(!isset($Ignore['FTP'])){
        $ZUK->HoleFTPDaten();
    }
    if($Abfrage == 'komplett'){
        $Abfrage = 'Bestellung';
        $AlleAbfragen = true;
    }
    switch ($Abfrage) {
        case 'Bestellung':
            if ($ZUK->LeseBestellungen('', $StartTag, $EndTag, '') > 0) {
                $ZUK->SpeichereBestellungen();
            }
            if(!$AlleAbfragen){
                break;
            }
        case 'Lieferschein':
            if ($ZUK->LeseLieferscheine('', $StartTag, $EndTag, '', '', '', false) > 0) {
                $ZUK->SpeichereLieferscheine();
            }
            if(!$AlleAbfragen){
                break;
            }
        case 'Rechnung':
            if ($ZUK->LeseRechnungen('', $StartTag, $EndTag, '', '', '', true, '') > 0) {
                $ZUK->SpeichereRechnungen();
            }
            $ZUK->PDFRechnungenVersenden();
            if(!$AlleAbfragen){
                break;
            }
    }

} catch (awisException $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'SQL: ' . $ex->getSQL() . PHP_EOL;
} catch (Exception $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
}
?>

