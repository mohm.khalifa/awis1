
        <?php
        require_once('awisDatenbank.inc');
        require_once('awisBenutzer.inc');
        require_once('awisFormular.inc');
        include_once('awisFirstglasImport.inc.php');

        global $AWISCursorPosition;
        global $AWIS_KEY1;
        global $AWIS_KEY2;


        try {
	
	    $TextKonserven = array();
            $TextKonserven[]=array('FIL','%');
	    $TextKonserven[]=array('WEB','%');
	    $TextKonserven[]=array('WBE','%');
            $TextKonserven[]=array('PER','%');
            $TextKonserven[]=array('Wort','lbl_weiter');
            $TextKonserven[]=array('Wort','lbl_speichern');
            $TextKonserven[]=array('Wort','lbl_trefferliste');
            $TextKonserven[]=array('Wort','lbl_aendern');
            $TextKonserven[]=array('Wort','lbl_hinzufuegen');
            $TextKonserven[]=array('Wort','lbl_loeschen');
            $TextKonserven[]=array('Wort','lbl_zurueck');
            $TextKonserven[]=array('Wort','lbl_DSZurueck');
            $TextKonserven[]=array('Wort','lbl_DSWeiter');
            $TextKonserven[]=array('Wort','lbl_Hilfe');
            $TextKonserven[]=array('Wort','lbl_hilfe');
            $TextKonserven[]=array('Wort','Seite');
            $TextKonserven[]=array('Wort','txt_BitteWaehlen');
            $TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
            $TextKonserven[]=array('Fehler','err_keineRechte');
            $TextKonserven[]=array('Fehler','err_keineDaten');
            $TextKonserven[]=array('Wort','Status');
            $TextKonserven[]=array('Wort','AlleAnzeigen');
            $TextKonserven[]=array('Wort','lbl_RueckfuehrungsscheinDrucken');
            $TextKonserven[]=array('Wort','lbl_RueckfuehrungsdifferenzenDrucken');
            $TextKonserven[]=array('Wort','lbl_RueckfuehrungslisteDrucken');
            $TextKonserven[]=array('Wort','KeineBerechtigungRueckfuehrung');
            $TextKonserven[]=array('Wort','AlleAnzeigen');
            $TextKonserven[]=array('Wort','Abschliessen');
            $TextKonserven[]=array('Wort','Abbrechen');
            $TextKonserven[]=array('Wort','Abschlussdatum');
	    $TextKonserven[]=array('Liste','lst_WEBERHALTEN');
	    $TextKonserven[]=array('Liste','lst_WEBVERWEIGERER');
	    $TextKonserven[]=array('Liste','lst_JaNein');
	    $TextKonserven[]=array('RFS','lst_RFS_STATUS');
            $TextKonserven[]=array('Wort','Text');

            $Form = new awisFormular();
	    
	    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	    $AWISBenutzer = awisBenutzer::Init();
            $DB = awisDatenbank::NeueVerbindung('AWIS');
            $DB->Oeffnen();

            $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

            $Recht14001 = $AWISBenutzer->HatDasRecht(14001
	    );
            if (($Recht14001)==0) {
                $Form->Fehler_KeineRechte();
            }
	    
            $speichern = false;

            $AWIS_KEY1 = -1;

	    $flagDatensaetzeVorhanden = false;	

            $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./verteilqualitaetFilialen_Main.php>");

            $Form->Formular_Start();

            $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasRueckfuehrung'));

            if(isset($_POST['cmdSpeichern_x']))
	    {
		include('./verteilqualitaetFilialen_speichern.php');
	    }
	    
	    if($speichern == false) {



                if(!isset($_GET['Sort'])) {
                    $ORDERBY = ' ORDER BY FFR_FILID DESC';
                }
                else {
                    $ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
                }

		$Param['FIL_ID']=(isset($_POST['sucFIL_ID'])?$_POST['sucFIL_ID']:$Param['FIL_ID']);
                $Param['ORDERBY']=$ORDERBY;
                $Param['BLOCK']=(isset($_REQUEST['Block'])?$_REQUEST['Block']:1);

                $AWISBenutzer->ParameterSchreiben('FirstglasDetails',implode(';',$Param));
                
		$FilZugriff=$AWISBenutzer->FilialZugriff(null,awisBenutzer::FILIALZUGRIFF_STRING);

		if($FilZugriff!='')
                {
                    if(strpos($FilZugriff,',')!==false)
                    {
                        $Form->ZeileStart();
                        $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',140);
                        $SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
                        $SQL .= ' FROM Filialen ';
                        $SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
                        $SQL .= ' ORDER BY FIL_BEZ';
                        $Form->Erstelle_SelectFeld('*FIL_ID',$Param['FIL_ID'],150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
                        $Form->ZeileEnde();
                        $AWISCursorPosition='sucFIL_ID';
                    }
                    elseif($Param['FIL_ID']=='') {

                        $Param['FIL_ID']=$FilZugriff;
                    }
                }
                else {

                    if($AWIS_KEY1 == -1)
                    {
                      $Form->ZeileStart();
                      $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',140);
                      $Form->Erstelle_TextFeld('*FIL_ID',$Param['FIL_ID'],10,180,true,'','','','T','L','','',10);
                      $Form->ZeileEnde();
                      $AWISCursorPosition='sucFIL_ID';
                    }
                }
                //echo $AWIS_KEY1;
                if($Param['FIL_ID']!='' && $AWIS_KEY1 == -1) {
                    $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));

                    $Form->Trennzeile();
		     
		    $SQL = 'Select WBE_FILIALEN, WEB_WERBEMEDIUM, WEB_KEY from WERBEMITTELZUORDNUNG inner join WERBEMEDIUM ON WBE_WEB_KEY = WEB_KEY WHERE WBE_DATVON <='.$DB->FeldInhaltFormat('DU',date('d.m.Y'));				
		    $SQL .= ' AND WBE_DATBIS >='.$DB->FeldInhaltFormat('DU',date('d.m.Y')).'AND WBE_FILIALEN='.$DB->FeldInhaltFormat('NO',$Param['FIL_ID']);

		    $rsFiliale = $DB->RecordSetOeffnen($SQL);	
		
		    if($rsFiliale->AnzahlDatensaetze() > 0)	
		    {
		    
			$flagDatensaetzeVorhanden = true;
		
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PER']['PER_NACHNAME'],110,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PER']['PER_VORNAME'],110,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WEB']['WEB_WERBEMEDIUM'],180,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WEB']['WEB_STATUS'],150,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WEB']['WEB_WERBEVERWEIGERER'],150,'');
			$Form->ZeileEnde();
		
		while(!$rsFiliale->EOF())
		{

			//Selektiere Filialmitarbeiter aus 
			$SQL = "Select PER_NR,PER_NACHNAME, PER_VORNAME, PER_FIL_ID FROM PERSONAL WHERE PER_AUSTRITT IS NULL AND PER_FIL_ID=".$DB->FeldInhaltFormat('NO',$rsFiliale->FeldInhalt('WBE_FILIALEN'));
			$SQL .= " ORDER BY PER_NACHNAME";
			//inner join wenn DS abgeglichen ist nicht mehr anzeigen
			
			$rsPers = $DB->RecordSetOeffnen($SQL);
			
			$DS = 0;
		
			while(!$rsPers->EOF())
			{
			   $SQL = "Select WER_PERSNR,WER_SELWERT1,WER_SELWERT2,Wer_Web_Key FROM WERBEEINTRAEGE ";
			   $SQL .= " WHERE WER_PERSNR=".$DB->FeldInhaltFormat('T',$rsPers->FeldInhalt('PER_NR'));
			   $SQL .= " AND WER_WEB_KEY=".$DB->FeldInhaltFormat('NO',$rsFiliale->FeldInhalt('WEB_KEY'));
			
                           $rsEintraege = $DB->RecordSetOeffnen($SQL);
			
			   $Form->ZeileStart();
                           $Form->Erstelle_HiddenFeld('PER_PER_NR',$rsPers->FeldInhalt('PER_NR'));
			   $Form->Erstelle_HiddenFeld('WEB_KEY',$rsFiliale->FeldInhalt('WEB_KEY'));
                           $Form->Erstelle_ListenFeld('PER_NACHNAME',$rsPers->FeldInhalt('PER_NACHNAME'),0,110,false,($DS%2),'','','T');
                           $Form->Erstelle_ListenFeld('PER_VORNAME',$rsPers->FeldInhalt('PER_VORNAME'),0,110,false,($DS%2),'','','T');
                           $Form->Erstelle_ListenFeld('WEB_WERBEMEDIUM',$rsFiliale->FeldInhalt('WEB_WERBEMEDIUM'),0,180,false,($DS%2),'','','T');
                           $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_WEBERHALTEN']);
                           if($rsEintraege->AnzahlDatensaetze() == 0)		
			   {
				$Form->Erstelle_SelectFeld('Web_Erhalten'.$rsPers->FeldInhalt('PER_NR').$rsFiliale->FeldInhalt('WEB_KEY'),'',150,true,'','','0','','',$Daten);
			   }
			   else
			   {
			        $Form->Erstelle_SelectFeld('Web_Erhalten'.$rsPers->FeldInhalt('PER_NR').$rsFiliale->FeldInhalt('WEB_KEY'),$rsEintraege->FeldInhalt('WER_SELWERT1'),150,true,'','','0','','',$Daten);
			   }
			  $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_WEBVERWEIGERER']);
                          if($rsEintraege->AnzahlDatensaetze() == 0)		
			   {
			   $Form->Erstelle_SelectFeld('Web_Verweigert'.$rsPers->FeldInhalt('PER_NR').$rsFiliale->FeldInhalt('WEB_KEY'),'',150,true,'','','0','','',$Daten);
			   }
			   else
			   {
			     $Form->Erstelle_SelectFeld('Web_Verweigert'.$rsPers->FeldInhalt('PER_NR').$rsFiliale->FeldInhalt('WEB_KEY'),$rsEintraege->FeldInhalt('WER_SELWERT2'),150,true,'','','0','','',$Daten);
			   }
			   $rsPers->DSWeiter();
			   $Form->ZeileEnde();	
			}
			
		  $rsFiliale->DSWeiter();	   
		}	   
			
			   
			
			
		
			if(($Recht14001&4)==4) {
                        //$Form->Erstelle_HiddenFeld('FFR_KEY', $rsFFR->FeldInhalt('FFR_KEY'));
                     }
                    
		
		}
		else
		{
			 
			 
			 
                }

		    	
                $Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		if($flagDatensaetzeVorhanden == true)
		{
			//Speicherbutton
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
			
		}
		$Form->SchaltflaechenEnde();

                $Form->SchreibeHTMLCode('</form>');


                }

            }
        }
        catch (Exception $ex)
        {
            die($ex->getMessage());
        }

        ?>
