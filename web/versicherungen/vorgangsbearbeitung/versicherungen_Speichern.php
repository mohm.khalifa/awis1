<?php

global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven = array();
$TextKonserven[] = array('VVS', '*');
$TextKonserven[] = array('VVE', '*');
$TextKonserven[] = array('FIL', '*');
$TextKonserven[] = array('VEB', '*');
$TextKonserven[] = array('VCB', '*');
$TextKonserven[] = array('V_VERS_FORM_DETAIL', '*');
$TextKonserven[] = array('SONDERAKTION', '*');
$TextKonserven[] = array('Fehler', 'err_KeinWert');
$TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
$TextKonserven[] = array('Wort', 'geaendert_von');
$TextKonserven[] = array('Wort', 'geaendert_auf');
$TextKonserven[] = array('Meldung', 'DSVeraendert');
$TextKonserven[] = array('Meldung', 'EingabeWiederholen');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_Hilfe');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Liste', 'lst_JaNein');

try
{
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    //$Form->DebugAusgabe(1,$_POST);

    if(isset($_POST['txtVVS_KEY']))
    {
        $AWIS_KEY1 = $_POST['txtVVS_KEY'];

        if(intval($_POST['txtVVS_KEY']) != 0)
        {

            if(isset($_POST['txtSON_LST_ZURUECKSETZEN']) && isset($_POST['txtSON_LST_NACHGEDRUCKT']) && isset($_POST['txtSON_LST_UMGEBUCHT']))
            {
                if($_POST['txtSON_LST_ZURUECKSETZEN'] == 0 && $_POST['txtSON_LST_NACHGEDRUCKT'] == 0 && $_POST['txtSON_LST_UMGEBUCHT'] == 0)
                {
                    if(isset($_POST['txtVVS_FREIGABE']) && $_POST['txtVVS_FREIGABE'] == 1)
                    {
                        $Fehler = '';
                        $Pflichtfelder = array('VVS_LAENDERSCHL', 'VVS_FREIGABEGRUND');
                        foreach($Pflichtfelder AS $Pflichtfeld)
                        {
                            if($_POST['txt' . $Pflichtfeld] == '')
                            {
                                $Fehler .= $AWISSprachKonserven['Fehler']['err_KeinWert'] . ' ' . $AWISSprachKonserven['VVS'][$Pflichtfeld] . '<br>';
                            }
                        }
                        if($Fehler != '')
                        {
                            die('<span class=HinweisText>' . $Fehler . '</span>');
                        }
                    }
                    // bei �nderung der Felder (KFZ_KZ, SchadenKZ) in der Tabelle versvorgangsstatus
                    // ein �nderungsflag setzten f�r die R�ckspielung ins DWH

                    $SQL = 'SELECT vvs_export_dwh ';
                    $SQL .= 'FROM versvorgangsstatus ';
                    $SQL .= 'WHERE vvs_key = ' . $AWIS_KEY1;

                    $rsExportFlag = $DB->RecordSetOeffnen($SQL);

                    if(isset($_POST['oldVVS_KFZKENNZ']) && isset($_POST['txtVVS_KFZKENNZ']))
                    {

                        if($_POST['oldVVS_KFZKENNZ'] <> $_POST['txtVVS_KFZKENNZ'])
                        {
                            if(($rsExportFlag->FeldInhalt('VVS_EXPORT_DWH') & 4) == 0)
                            {
                                $SQL = 'UPDATE versvorgangsstatus ';
                                $SQL .= 'SET vvs_export_dwh = 4 + vvs_export_dwh ';
                                $SQL .= 'WHERE vvs_key = ' . $AWIS_KEY1;

                                $DB->Ausfuehren($SQL, '', true);
                            }
                        }
                    }

                    if(isset($_POST['oldVVS_SCHADENKZ']) && isset($_POST['txtVVS_SCHADENKZ']))
                    {
                        if($_POST['oldVVS_SCHADENKZ'] <> $_POST['txtVVS_SCHADENKZ'])
                        {
                            if(($rsExportFlag->FeldInhalt('VVS_EXPORT_DWH') & 1) == 0)
                            {
                                $SQL = 'UPDATE versvorgangsstatus ';
                                $SQL .= 'SET vvs_export_dwh = 1 + vvs_export_dwh ';
                                $SQL .= 'WHERE vvs_key = ' . $AWIS_KEY1;

                                $DB->Ausfuehren($SQL, '', true);
                            }
                        }
                    }

                    $Felder = explode(';', $Form->NameInArray($_POST, 'txtVVS_', 1, 1));
                    $FehlerListe = array();
                    $UpdateFelder = '';

                    $AWISBenutzer->ParameterSchreiben("AktuellerVVS", $_POST['txtVVS_KEY']);

                    $SQL = 'SELECT *';
                    $SQL .= ' FROM VERSVORGANGSSTATUS VVS';
                    $SQL .= ' INNER JOIN VERSCHECKSTATUSNUMMERN VCN ';
                    $SQL .= ' ON VVS.VVS_VCN_KEY = VCN.VCN_KEY';
                    $SQL .= ' WHERE VVS_KEY = ' . $DB->WertSetzen('VVS', 'N0', $_POST['txtVVS_KEY']);

                    $rsVVS = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VVS', true));

                    $FeldListe = '';
                    foreach($Felder AS $Feld)
                    {

                        $FeldName = substr($Feld, 3);
                        if(isset($_POST['old' . $FeldName]))
                        {
                            switch($FeldName)
                            {
                                default:
                                    $WertNeu = $DB->FeldInhaltFormat($rsVVS->FeldInfo($FeldName, awisRecordset::FELDINFO_FORMAT), $_POST[$Feld], true);
                            }
                            $WertAlt = $DB->FeldInhaltFormat($rsVVS->FeldInfo($FeldName, awisRecordset::FELDINFO_FORMAT), $_POST['old' . $FeldName], true);
                            $WertDB = $DB->FeldInhaltFormat($rsVVS->FeldInfo($FeldName, awisRecordset::FELDINFO_FORMAT), $rsVVS->FeldInhalt($FeldName), true);
                            if(isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY'))
                            {
                                if($WertAlt != $WertDB AND $WertDB != 'null')
                                {
                                    $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
                                }
                                else
                                {
                                    $FeldListe .= ', ' . $FeldName . '=';
                                    if($_POST[$Feld] == '')
                                    {
                                        $FeldListe .= ' null';
                                    }
                                    else
                                    {
                                        $FeldListe .= $WertNeu;
                                    }
                                }
                            }
                        }
                    }
                    if(count($FehlerListe) > 0)
                    {
                        $Meldung = str_replace('%1', $rsVVS->FeldInhalt('VVS_USER'), $AWISSprachKonserven['Meldung']['DSVeraendert']);
                        foreach($FehlerListe AS $Fehler)
                        {
                            $FeldName = $Form->LadeTexte(array(array(substr($Fehler[0], 0, 3), $Fehler[0])));
                            $Meldung .= '<br>&nbsp;' . $FeldName[substr($Fehler[0], 0, 3)][$Fehler[0]] . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
                        }
                        $Form->Fehler_Anzeigen('GleichzeitigsSpeichern', $Meldung, 'WIEDERHOLEN', 0, '200907151411');

                    }
                    elseif($FeldListe != '')
                    {
                        $SQL = 'UPDATE versvorgangsstatus SET';
                        $SQL .= substr($FeldListe, 1);
                        $SQL .= ', VVS_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
                        $SQL .= ', VVS_userdat=sysdate';
                        $SQL .= ' WHERE VVS_key=0' . $_POST['txtVVS_KEY'] . '';

                        $DB->Ausfuehren($SQL, '', true);
                    }
                }
                elseif($_POST['txtSON_LST_ZURUECKSETZEN'] == 1 && $_POST['txtSON_LST_NACHGEDRUCKT'] == 0 && $_POST['txtSON_LST_UMGEBUCHT'] == 0)
                {
                    $SQL = 'SELECT * FROM versvorgangsstatus WHERE vvs_key = ' . $_POST['txtVVS_KEY'];
                    $rsVVS = $DB->RecordSetOeffnen($SQL);
                    if($rsVVS->FeldInhalt('VVS_VCN_KEY') == 112)
                    {
                        die('<span class=HinweisText>Vorgang wurde bereits Exportiert und kann nicht mehr zur�ckgesetzt werden</span>');
                    }
                    else
                    {
                        $SQL = 'UPDATE versvorgangsstatus SET ';
                        $SQL .= 'VVS_VEN_KEY = 0, VVS_VCN_KEY = 0, VVS_FREIGABE = 0, VVS_FREIGABEGRUND = \'\' ';
                        $SQL .= 'WHERE VVS_KEY = ' . $_POST['txtVVS_KEY'];

                        $DB->Ausfuehren($SQL, '', true);
                    }
                }
                elseif($_POST['txtSON_LST_NACHGEDRUCKT'] == 1 && $_POST['txtSON_LST_ZURUECKSETZEN'] == 0 && $_POST['txtSON_LST_UMGEBUCHT'] == 0)
                {
                    $SQL = 'SELECT * FROM versvorgangsstatus WHERE vvs_key = ' . $_POST['txtVVS_KEY'];
                    $rsVVS = $DB->RecordSetOeffnen($SQL);
                    if($rsVVS->FeldInhalt('VVS_VCN_KEY') == 113)
                    {
                        die('<span class=HinweisText>Vorgang wurde bereits als Nachgedruckt markiert</span>');
                    }
                    elseif($rsVVS->FeldInhalt('VVS_SCHADENKZ') != 'T')
                    {
                        die('<span class=HinweisText>Nur T-F�lle k�nnen als Nachgedruckt markiert werden</span>');
                    }

                    $SQL = 'UPDATE versvorgangsstatus ';
                    $SQL .= 'SET vvs_datumexport = sysdate, ';
                    $SQL .= 'vvs_ven_key = 0, ';
                    $SQL .= 'vvs_vcn_key = 113 ';
                    $SQL .= 'WHERE VVS_KEY = ' . $_POST['txtVVS_KEY'];

                    $DB->Ausfuehren($SQL, '', true);
                }
                elseif($_POST['txtSON_LST_NACHGEDRUCKT'] == 0 && $_POST['txtSON_LST_ZURUECKSETZEN'] == 0 && $_POST['txtSON_LST_UMGEBUCHT'] == 1)
                {
                    $SQL = 'SELECT * FROM versvorgangsstatus WHERE vvs_key = ' . $_POST['txtVVS_KEY'];
                    $rsVVS = $DB->RecordSetOeffnen($SQL);

                    if($rsVVS->FeldInhalt('VVS_VCN_KEY') == 120)
                    {
                        die('<span class=HinweisText>Vorgang wurde bereits als Umgebucht markiert</span>');
                    }
                    elseif($rsVVS->FeldInhalt('VCN_KATEGORIE') == 'VERARBEITET')
                    {
                        die('<span class=HinweisText>Nur Vorg�nge die noch nicht verarbeitet wurden k�nnen als Umgebucht markiert werden</span>');
                    }

                    //Wenn VEN_KEY 29 Doppelter Rechnungsversand => Vorgang blockieren, kein Versand 124
                    if($rsVVS->FeldInhalt('VVS_VEN_KEY') == 29)
                    {
                        $SQL = 'UPDATE versvorgangsstatus ';
                        $SQL .= 'SET vvs_datumexport = sysdate, ';
                        $SQL .= 'vvs_vcn_key = 124 ';
                        $SQL .= 'WHERE VVS_KEY = ' . $_POST['txtVVS_KEY'];
                    }
                    else
                    {
                        $SQL = 'UPDATE versvorgangsstatus ';
                        $SQL .= 'SET vvs_datumexport = sysdate, ';
                        $SQL .= 'vvs_vcn_key = 120, ';
                        $SQL .= 'vvs_freigabegrund = \'' . $_POST['txtVVS_FREIGABEGRUND'] . '\'';
                        $SQL .= ' WHERE VVS_KEY = ' . $_POST['txtVVS_KEY'];
                    }
                    $DB->Ausfuehren($SQL, '', true);
                }
            }
        }
        $AWIS_KEY1 = $_POST['txtVVS_KEY'];
    }

    $AWIS_KEY1 = $_POST['txtVVS_KEY'];
}
catch(Exception $ex)
{
    $Form->Fehler_Anzeigen('SYSTcherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>
