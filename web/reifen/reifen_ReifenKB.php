<?php
$txtKBListe = 'on';

global $cmdSuche;
global $AWISBenutzer;
global $con;
global $awisRSZeilen;
global $ReifenRechte;

if ($ReifenRechte == 0) {
    awisEreignis(3, 1000, 'Reifen', $AWISBenutzer->BenutzerName(), '', '', '');
    die("Keine ausreichenden Rechte!");
}

$RechteAST = awisBenutzerRecht($con, 400); // Artikelstamminfos
$Recht253 = awisBenutzerRecht($con, 253);

// Suchfelder in der Suchen-Seite

// Lokale Variablen
$Bedingung = ''; // Bedingung f�r SQL
$SQL = ''; // SQL Abfragestring
$ZeitVerbrauch = time();

// Aktualisieren wurde gew�hlt
If (isset($_POST['cmdSuche_x']) and $_POST['cmdSuche_x'] != '') {
    $Params = "";
    $Params .= $_POST["txtBreite"];
    $Params .= ";" . $_POST["txtQSchnitt"];
    $Params .= ";" . $_POST["txtDMesser"];
    $Params .= ";" . $_POST["txtLLKW"];
    $Params .= ";" . $_POST["txtLIndex"];
    $Params .= ";" . $_POST["txtLIndex2"];
    $Params .= ";" . $_POST["txtSpeedIndex"];
    $Params .= ";" . $_POST["txtMaxVK"];
    $Params .= ";" . $_POST["txtATUNr"];
    $Params .= ";" . $_POST["txtBez"];
    $Params .= ";" . $_POST["txtSort"];
    $Params .= ";" . $_POST["txtTyp"];
    $Params .= ";" . (isset($_POST["txtCCSort"])?$_POST["txtCCSort"]:'');
    $Params .= ";" . (isset($_POST["txtReifSpd"])?$_POST["txtReifSpd"]:'');
    $Params .= ";" . (isset($_POST["txtAuswahlSpeichern"])?$_POST["txtAuswahlSpeichern"]:''); // Alle gespeicherten Parameter wieder anzeigen
    $Params .= ";" . (isset($_POST["txtHersteller"])?$_POST["txtHersteller"]:'');
    $Params .= ";" . (isset($_POST["txtKBListe"])?$_POST["txtKBListe"]:'');
    $Params .= ";" . (isset($_POST["txtHerstellerZuerst"])?$_POST["txtHerstellerZuerst"]:'');
    $Params .= ";" . (isset($_POST['txtLAN_CODE'])?$_POST['txtLAN_CODE']:''); // 18
    $Params .= ";" . (isset($_POST['txtFahrzeuge'])?$_POST['txtFahrzeuge']:''); // 19
    $Params .= ";" . (isset($_POST['txtReifLoad'])?$_POST['txtReifLoad']:''); // 20
    $Params .= ";" . (isset($_POST['txtKBPreise'])?$_POST['txtKBPreise']:''); // 21
    $Params .= ";" . $_POST['txtFiliale']; // 22
    $Params .= ";" . $_POST['txtROLLRES']; // 23
    $Params .= ";" . $_POST['txtWETGRIP']; // 24
    $Params .= ";" . $_POST['txtNOISECL']; // 25
    $Params .= ";" . $_POST['txtNOISEPE']; // 26

    awis_BenutzerParameterSpeichern($con, "ReifenSuche", $AWISBenutzer->BenutzerName(), $Params);

    awis_BenutzerParameterSpeichern($con, "ArtikelStammSucheFiliale", $AWISBenutzer->BenutzerName(),
        $_POST['txtFiliale']);
}

$Params = explode(";", awis_BenutzerParameter($con, "ReifenSuche", $AWISBenutzer->BenutzerName()));

$txtBreite = $Params[0];
$txtQSchnitt = $Params[1];
$txtDMesser = $Params[2];
$txtLLKW = $Params[3];
$txtLIndex = $Params[4];
$txtLIndex2 = $Params[5];
$txtSpeedIndex = $Params[6];
$txtMaxVK = $Params[7];
$txtATUNr = $Params[8];
$txtBez = $Params[9];
$txtSort = $Params[10];
$txtTyp = $Params[11];
$txtCCSort = $Params[12];
$txtReifSpd = $Params[13];
$txtAuswahlSpeichern = $Params[14]; // Alle gespeicherten Parameter wieder anzeigen
$txtHersteller = $Params[15];
$txtLAN_CODE = $Params[18];

if (!isset($Params[22])) {
    $UserFilialen = awisBenutzerFilialen($con, $AWISBenutzer->BenutzerName(), 2);
    if ($UserFilialen != '') {
        $Params[22] = $UserFilialen;
    }
}

if ($Params[22] != '') {
    // Land zu der eingegebenen Filiale ermitteln

    $BindeVariablen = array();
    $BindeVariablen['var_N0_fil_id'] = $Params[22];

    $SQL = 'SELECT LAN_CODE, FIL_GRUPPE';
    $SQL .= ' FROM FILIALEN';
    $SQL .= ' INNER JOIN LAENDER ON LAN_WWSKENN = FIL_LAN_WWSKENN';
    $SQL .= ' WHERE FIL_ID = :var_N0_fil_id';

    $rsLAN = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
    $rsLANAnz = $awisRSZeilen;

    if ($rsLANAnz == 0) {
        echo 'Ung�ltige Filiale!';
        die();
    } else {
        $LAN_CODE = $rsLAN['LAN_CODE'][0];
        $FIL_GRUPPE = $rsLAN['FIL_GRUPPE'][0];

        if ($Params[18] != $LAN_CODE) {
            $Params[18] = $LAN_CODE;
            $txtLAN_CODE = $LAN_CODE;
        }
    }
} else {
    $FIL_GRUPPE = -1;
}
$txtROLLRES = $Params[23];
$txtWETGRIP = $Params[24];
$txtNOISECL = $Params[25];
$txtNOISEPE = $Params[26];

$SuchOperatorLoadIndex = (isset($Params[20]) && $Params[20] == 'on'?'>=':'=');

// Bildschirmbreite f�r die Darstellung
$BildschirmBreite = awis_BenutzerParameter($con, "BildschirmBreite", $AWISBenutzer->BenutzerName());
// Farbcodes f�r KBListe
$FarbCodes = awis_BenutzerParameter($con, "ReifenFarbCodes", $AWISBenutzer->BenutzerName());

// Landesauswahl f�r die Preise
$Bedingung = " AND ((REI_KBPREIS = -1 AND RPR_LAN_CODE='" . $txtLAN_CODE . "') OR REI_KBPREIS <> -1)";

// Parameter speichern
if ($txtTyp == "1" or $txtTyp == "2") {
    awis_BenutzerParameterSpeichern($con, "ReifenTyp", $AWISBenutzer->BenutzerName(), $txtTyp);
}

if ($txtBreite != '') {
    $Bedingung .= " AND REI_BREITE = " . awis_format($txtBreite, "US-Zahl", ",");
}

// *********************************************************
// Querschnitt
// *********************************************************
If ($txtQSchnitt != '') {
    if ($txtQSchnitt == '80') // 80 und leer
    {
        $Bedingung .= " AND (REI_Querschnitt IS NULL OR REI_Querschnitt = '80')";
    } else {
        $Bedingung .= " AND REI_Querschnitt LIKE '%" . $txtQSchnitt . "%'";
    }
}

// *********************************************************
// InnenDurchmesser in Zoll
// *********************************************************
If ($txtDMesser != "") {
    $Bedingung .= " AND REI_InnenDurchmesser = " . awis_format($txtDMesser, "US-Zahl", ",");
}

// *********************************************************
// LLKW
// *********************************************************
If ($txtLLKW != '') {
    if (strtoupper($txtLLKW) == 'C') {
        $Bedingung .= " AND (REI_LLKW = '" . strtoupper($txtLLKW) . "' OR REI_LLKW = 'CP')";
    } else {
        $Bedingung .= " AND REI_LLKW = '" . strtoupper($txtLLKW) . "'";
    }
} else {
    If ($txtCCSort == "on") {
        $Bedingung .= " AND REI_LLKW IS NULL";
    }
}

// *********************************************************
// LoadIndex
// *********************************************************

If ($txtLIndex != '' and $txtLIndex2 != '') {
    $Bedingung .= " AND ((to_number(regexp_replace(REI_LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $txtLIndex . " AND REI_LoadIndex2 $SuchOperatorLoadIndex " . $txtLIndex2 . ")";
    $Bedingung .= " OR (to_number(regexp_replace(REI_LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $txtLIndex2 . " AND REI_LoadIndex2 $SuchOperatorLoadIndex " . $txtLIndex . "))";
} else {
    If ($txtLIndex != '') {
        $Bedingung .= " AND to_number(regexp_replace(REI_LoadIndex, '\D', '')) $SuchOperatorLoadIndex " . $txtLIndex;
    }

    If ($txtLIndex2 != '') {
        $Bedingung .= " AND REI_LoadIndex2 $SuchOperatorLoadIndex " . $txtLIndex2;
    }
}

// *********************************************************
// SpeedIndex
// *********************************************************

If ($txtSpeedIndex != '' and $txtSpeedIndex != '#-#') {
    If ($txtReifSpd != '') {
        $Bedingung .= " AND (REI_SPI_SYMBOL IS NULL OR SPI_ID >= (SELECT SpeedIndex.SPI_ID FROM SpeedIndex WHERE SpeedIndex.SPI_Symbol = '" . strtoupper($txtSpeedIndex) . "'))";
    } else {
        $Bedingung .= " AND (REI_SPI_SYMBOL IS NULL OR SPI_ID = (SELECT SpeedIndex.SPI_ID FROM SpeedIndex WHERE SpeedIndex.SPI_Symbol = '" . strtoupper($txtSpeedIndex) . "'))";
    }
}

// *********************************************************
// MaxVK
// *********************************************************

If ($txtMaxVK != '') {
    $Bedingung .= " AND REI_AKTSORT <= " . str_replace(',', '.', $txtMaxVK);
}

// *********************************************************
// ATU-Nummer
// *********************************************************
If ($txtATUNr != "") {
    $Bedingung .= " AND REI_AST_ATUNr " . awisLIKEoderIST($txtATUNr, 1);
}

// *********************************************************
// Rollwiderstand
// *********************************************************
If ($Params[23] != '' and $Params[23] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID, RIN_WERT as ROLLRES FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=10 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[23] . "'))";
}

// *********************************************************
// Nasshaftungsklasse
// *********************************************************
If ($Params[24] != '' and $Params[24] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=11 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[24] . "'))";
}

// *********************************************************
// Rollger�uschklasse
// *********************************************************
If ($Params[25] != '' and $Params[25] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=13 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[25] . "'))";
}

// *********************************************************
// Rollger�usch(dB)
// *********************************************************
If ($Params[26] != '' and $Params[26] != '#-#') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=12 AND ";
    $Bedingung .= " RIN_Wert = '" . $Params[26] . "'))";
}

// *********************************************************
// Reifentyp
// *********************************************************
If ($txtTyp != "" and $txtTyp != '0') {
    $Bedingung .= " AND (EXISTS (SELECT RIN_REI_ID FROM ReifenInfos WHERE REI_ID=RIN_REI_ID AND RIN_RNT_ID=1 AND ";
    $Bedingung .= " RIN_Wert = '" . $txtTyp . "'))";
}

// *********************************************************
// Bezeichnung
// *********************************************************
If ($txtBez != '') {
    $Bedingung .= " AND UPPER(REI_Bezeichnung) " . strtoupper(awisLIKEoderIST("%" . $txtBez . "%", 1)) . "";
}

// *********************************************************
// Hersteller
// *********************************************************
If ($txtHersteller != '') {
    $Bedingung .= " AND UPPER(REI_HERSTELLER) " . strtoupper(awisLIKEoderIST($txtHersteller . "%", true)) . "";
}

// *********************************************************
// sonstige Bedingungen
// *********************************************************
If ($txtCCSort == 'on') {
    $Bedingung .= " AND (RPR_K <> 'N' OR RPR_K IS NULL) AND RPR_VK > 0 ";
}

// CA 18.09.07 - Globale BEdingung. Ausblenden von Sonderartikel AST_SUCH2.

if (!($ReifenRechte & 2) == 2) {
    $Bedingung .= " AND EXISTS (SELECT AST_ATUNR FROM ARTIKELSTAMM WHERE REI_AST_ATUNR=AST_ATUNR AND AST_SUCH2 IS NULL)";
}

// *********************************************************
// Sortierung
// *********************************************************

If ($txtSort !== '') {
    $SortFelder = array(
        'AST_ATUNR',
        'VK',
        'AKTSORT',
        'SPI_SYMBOL',
        'HERSTELLER'
    );
    If ($txtCCSort == 'on') {
        If ($txtSort != 3 and $SortFelder[$txtSort] != '') // Alle bis auf Speed-Index
        {
            $Bedingung .= " ORDER BY REI_AusLauf DESC, SPI_ID, REI_";
            if ($Params[21] == 'on' and $txtSort == 1) {
                $Bedingung .= 'KBPREIS';
            } else {
                $Bedingung .= $SortFelder[$txtSort];
            }
        } else // Bei Speed-Index spezielle Sortierung
        {
            $Bedingung .= " ORDER BY REI_AusLauf DESC, SPI_ID, REI_AST_ATUNR";
        }
    } else {
        If ($txtSort != 3 and $txtCCSort != 'on' and $SortFelder[$txtSort] != '') {
            $Bedingung .= " ORDER BY REI_";
            if (isset($Params[21]) and $Params[21] == 'on' and $txtSort == 1) {
                $Bedingung .= 'KBPREIS';
            } else {
                $Bedingung .= strtoupper($SortFelder[$txtSort]);
            }
        } else {
            $Bedingung .= " ORDER BY SPI_ID, REI_AST_ATUNR";
        }
    }
}

// *************************************************************
// * Bestandsinfos fuer mehrere Filialen anzeigen
// *************************************************************
if ($ReifenRechte & 4 || $FIL_GRUPPE == 5 || $FIL_GRUPPE == -1) {

    echo '<form name="frmReifenKB" method="post" action="./reifen_Main.php?cmdAktion=ReifenKB">';
    echo 'Vortagesbest&auml;nde anzeigen f&uuml;r folgende Filialen:';

    $BestandsFIL_ID = isset($_POST['txtFIL_ID'])?$_POST['txtFIL_ID']:awis_BenutzerParameter($con,
        'Reifen-Bestandsfilialen', $AWISBenutzer->BenutzerName());

    echo '<input title="Tragen Sie hier eine oder mehrere Filialnummern durch Komma getrennt ein." type=input name=txtFIL_ID value="' . $BestandsFIL_ID . '">';
    echo '<Script Language=JavaScript>';
    echo 'document.getElementsByName("txtFIL_ID")[0].focus();';
    echo '</Script>';

    echo '</form>';
    echo '<hr>';
} else {
    $BestandsFIL_ID = '';
}

// ********************************************************************
// SQL Abfrage zusammenstellen
// ********************************************************************

print "<b>Angezeigte Preise gelten f�r " . DLookup('LAN_LAND', 'LAENDER', "LAN_CODE='" . $txtLAN_CODE . "'",
        $con) . "</b><br>";

if ($txtKBListe != 'on') {
    // Filialindividuellen Preis ber�cksichtigen
    if ($Params[22] != '') {
        $SQL = ' select min(aktion) as MinAktion, max(aktion) as MaxAktion, RST_FELGENRIPPE from (';
        $SQL .= ' select rei_ast_atunr, RST_FELGENRIPPE, rpr_aktion, fib_vk, ((100 - nvl(rpr_rabatt,0)) / 100) * nvl(fib_vk,rpr_aktion) as aktion';
        $SQL .= ' FROM v_ReifenListe';
        $SQL .= ' left join filialbestand on fib_ast_atunr = rei_ast_atunr and fib_fil_id = ' . $Params[22];
        $SQL .= " " . (substr($Bedingung, 0, 4) == " AND"?"WHERE " . substr($Bedingung, 5):$Bedingung);
        $SQL .= ")";
    } else {
        $SQL = "SELECT DISTINCT MIN(RPR_AKTION) AS MinAktion, MAX(RPR_AKTION) AS MaxAktion FROM v_ReifenListe";
        $SQL .= " " . (substr($Bedingung, 0, 4) == " AND"?"WHERE " . substr($Bedingung, 5):$Bedingung);
    }

    $rsReifen = awisOpenRecordset($con, $SQL);
    print "<b>Aktions-VK von " . awis_format($rsReifen["MINAKTION"][0],
            "Currency") . " bis " . awis_format($rsReifen["MAXAKTION"][0], "Currency") . "</b><br>";

    print "Folgende Lieferanten f�hren die gesuchte Gr��e:<br>";

    $SQL = "SELECT DISTINCT REI_AST_ATUNR, REI_HERSTELLER, REI_BREITE, REI_QUERSCHNITT, REI_BAUART, REI_INNENDURCHMESSER, REI_LLKW, REI_LOADINDEX, ";
    $SQL .= " RST_FELGENRIPPE, REI_LOADINDEX2, REI_SPI_SYMBOL, REI_RF, REI_BEZEICHNUNG, RPR_K, RPR_VK, RPR_AKTION, RPR_AKTIONSKENN, RPR_AKTIONTEXT, ";
    $SQL .= " RPR_AKTSORT, RPR_RABATT, RPR_KBPREIS, REI_BESTELLKZ, SPDIDX, REI_AUSLAUF, SPI_SYMBOL, SPI_ID, REI_ZUSATZBEMERKUNG, REI_VK ";
    $SQL .= " , REI_AKTSORT, REI_K, REI_LOADINDEX3, REI_SPI_SYMBOL2, REI_ZUSBEM2, REI_ZUSBEM3, ";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin10 WHERE rin10.rin_rei_id = aa.rei_id AND rin10.rin_rnt_id=10) AS ROLLRES,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin11 WHERE rin11.rin_rei_id = aa.rei_id AND rin11.rin_rnt_id=11) AS WETGRIP,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin12 WHERE rin12.rin_rei_id = aa.rei_id AND rin12.rin_rnt_id=12) AS NOISEPE,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin13 WHERE rin13.rin_rei_id = aa.rei_id AND rin13.rin_rnt_id=13) AS NOISECL,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin14 WHERE rin14.rin_rei_id = aa.rei_id AND rin14.rin_rnt_id=14) AS WINTERTAUGLICHKEIT,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin30 WHERE rin30.rin_rei_id = aa.rei_id AND rin30.rin_rnt_id=30) AS RIN_INFO";
    $SQL .= " FROM v_ReifenListe aa";
    $SQL .= " " . (substr($Bedingung, 0, 4) == " AND"?"WHERE " . substr($Bedingung, 5):$Bedingung);
    $rsReifen = awisOpenRecordset($con, $SQL);
    $rsReifenAnz = $awisRSZeilen;

    $Hersteller = array();
    for ($i = 0; $i < $rsReifenAnz; $i++) {
        if ($rsReifen["REI_HERSTELLER"][$i] == "RUNDERNEUERT") {
            $rsReifen["REI_HERSTELLER"][$i] = "<i>" . $rsReifen["REI_HERSTELLER"][$i] . "</i>";
        }

        if (in_array($rsReifen["REI_HERSTELLER"][$i], $Hersteller) == false) {
            array_push($Hersteller, $rsReifen["REI_HERSTELLER"][$i]);
        }
    }
    array_multisort($Hersteller);
    print "<b>" . implode(", ", $Hersteller) . "</b><br>";
} else { // Daten f�r die KB Liste
    $SQL = "SELECT DISTINCT DECODE(SUBSTR(REI_AST_ATUNR,3),'0000',TO_CHAR(REI_ID),REI_AST_ATUNR) AS REISONDERID, REI_AST_ATUNR, REI_HERSTELLER, REI_BREITE, REI_QUERSCHNITT, REI_BAUART, REI_INNENDURCHMESSER, REI_LLKW, REI_LOADINDEX, ";
    $SQL .= "REI_LOADINDEX2, REI_SPI_SYMBOL, REI_RF, REI_BEZEICHNUNG, RPR_K, RPR_VK, RPR_AKTION, RPR_AKTIONSKENN, RPR_AKTIONTEXT, ";
    $SQL .= "RPR_RABATT, RST_FELGENRIPPE, RPR_KBPREIS, REI_BESTELLKZ, SPDIDX, REI_AUSLAUF, SPI_SYMBOL, SPI_ID, REI_ZUSATZBEMERKUNG, RPR_AKTSORT, REI_VK, ";
    $SQL .= "REI_LOADINDEX3, REI_SPI_SYMBOL2, REI_ZUSBEM2, REI_ZUSBEM3, REI_K, REI_AKTSORT, ";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin10 WHERE rin10.rin_rei_id = aa.rei_id AND rin10.rin_rnt_id=10) AS ROLLRES,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin11 WHERE rin11.rin_rei_id = aa.rei_id AND rin11.rin_rnt_id=11) AS WETGRIP,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin12 WHERE rin12.rin_rei_id = aa.rei_id AND rin12.rin_rnt_id=12) AS NOISEPE,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin13 WHERE rin13.rin_rei_id = aa.rei_id AND rin13.rin_rnt_id=13) AS NOISECL,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin14 WHERE rin14.rin_rei_id = aa.rei_id AND rin14.rin_rnt_id=14) AS WINTERTAUGLICHKEIT,";
    $SQL .= " (SELECT RIN_WERT FROM REIFENINFOS rin30 WHERE rin30.rin_rei_id = aa.rei_id AND rin30.rin_rnt_id=30) AS RIN_INFO";
    $SQL .= " FROM v_ReifenListeKB aa";
    $SQL .= " " . (substr($Bedingung, 0, 4) == " AND"?"WHERE " . substr($Bedingung, 5):$Bedingung);
    awis_Debug(1, $SQL);
    $rsReifen = awisOpenRecordset($con, $SQL);
    $rsReifenAnz = $awisRSZeilen;
}

// *****************************************
// Spaltenbreiten festlegen
// *****************************************

$SpBreiteFaktor = $BildschirmBreite / 1024;
$SpBreiteTabelle = (1024 * $SpBreiteFaktor) - 40;
$SpBreiteArtnr = 80 * $SpBreiteFaktor;
$SpBreiteHerst = 80 * $SpBreiteFaktor;
$SpBreiteRabatt = 60;
$SpBreiteBez = 165;

// ho
$SpBreiteRabatt = 60 * $SpBreiteFaktor;
$SpBreiteBez = 165 * $SpBreiteFaktor;
//

If ($rsReifenAnz > 0) // Liste gefunden
{
    print "<table width=" . $SpBreiteTabelle . " cellspacing=0 cellpadding=0 border=1 >";
    print "<tr><td id=FeldBez width=" . $SpBreiteArtnr . ">ATUNr</td>";
    print "<td id=FeldBez width=" . $SpBreiteHerst . ">Herst.</td>";
    print "<td id=FeldBez colspan=9 width=360>Reifenkennzeichen</td>";

    print "<td id=FeldBez width=30 align=center>";
    echo "<img border=0 align=center width=30 height=30 src=/bilder/Kraftstoff_32px.png title='Kraftstoffeffizienz'></td>";
    print "<td id=FeldBez width=30 align=center>";
    echo "<img border=0 align=center width=30 height=30 src=/bilder/Nass_32px.png title='Nasshaftungsklasse'></td>";
    print "<td id=FeldBez width=30 align=center>";
    echo "<img border=0 align=center width=30 height=30 src=/bilder/Roll_3_32px.png title='Rollger�uschklasse'></td>";
    print "<td id=FeldBez width=20 align=center title='Rollger�usch in dB'>dB</td>";

    print "<td id=FeldBez width=" . $SpBreiteBez . ">Bezeichnung</td>";
    print "<td id=FeldBez width=20 title='Kennung'>K</td>";
    if (($ReifenRechte & 1024) == 1024) {
        print "<td id=FeldBez width=20 title='Auslaufartikel-Kennzeichen'>K2</td>";
    }
    print "<td id=FeldBez align=center width=50>" . (isset($Params[21]) && $Params[21] == 'on'?'<b>KB-Preis</b>':'VK') . "&nbsp;";
    echo "<img width=20 height=15 src=/bilder/Flagge_" . $txtLAN_CODE . ".gif title='Preise f�r " . $txtLAN_CODE . "'></td>";
    print "<td id=FeldBezBold align=center width=" . $SpBreiteRabatt . " title='Rabatt in %'>%</td>";
    print "<td id=FeldBez width=50>Aktion</td>";
    print "<td id=FeldBez width=50>FR</td>";
    print "<td id=FeldBez width=32 align=center>";
    echo "<img width=32 height=32 src=/bilder/DreizackigerBerg.png title='Schneeflockensymbol f�r garantierte Wintertauglichkeit'></td>";
    print "<td id=FeldBez width=200 align=center>INFO</td>";
    print "</tr>";

    $LetzterSpeedIndex = "";
    $AuslaufBlock = false;

    for ($i = 0; $i < $rsReifenAnz; $i++) {
        if ($txtCCSort == 'on') // Nur f�r CallCenter Blockbildung
        {
            if ($rsReifen["REI_AUSLAUF"][$i] == 'A' and !$AuslaufBlock) {
                print "<tr><td id=UeberschriftKlein colspan=18 align=center>Auslaufartikel</td></tr>";
                $AuslaufBlock = true;
            }

            // Anzeige f�r Speedindex
            if ($LetzterSpeedIndex != $rsReifen["SPI_ID"][$i]) {
                print "<tr><td id=UeberschriftKlein colspan=18 align=center>Speedindex: " . $rsReifen["SPDIDX"][$i] . "</td></tr>";
                $LetzterSpeedIndex = $rsReifen["SPI_ID"][$i];
            }
        }

        print "<tr>";

        $BindeVariablen = array();
        $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];

        $rsSuch2 = awisOpenRecordset($con, "SELECT AST_SUCH2 FROM ArtikelStamm WHERE AST_ATUNR=:var_T_ast_atunr", true,
            false, $BindeVariablen);

        if ($rsReifen["REI_HERSTELLER"][$i] == "<i>RUNDERNEUERT</i>") {
            print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " ><b>";

            // Wenn der Benutzer das Recht nicht hat, Sonderartikel zu sehen, kein Link auf Artikelseite, da
            // diese Artikel f�r diese Benutzer nicht sichtbar sind (TR 22.01.08)
            if (($RechteAST & 256) != 256 and $rsSuch2['AST_SUCH2'][0] != '') {
                echo $rsReifen["REI_AST_ATUNR"][$i];
            } else {
                if ($RechteAST > 0) {
                    if ($rsReifen["RPR_KBPREIS"][$i] == '-1' and strtoupper($rsReifen["RPR_K"][$i]) == 'S') {
                        echo '<a href=/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EKAT&AST_ATUNR=' . $rsReifen["REI_AST_ATUNR"][$i] . '>' . $rsReifen["REI_AST_ATUNR"][$i] . '</a>';
                    } else {
                        echo $rsReifen["REI_AST_ATUNR"][$i];
                    }
                } else {
                    echo $rsReifen["REI_AST_ATUNR"][$i];
                }
            }
            echo "</b></td>";

            print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " ><i>RUND.</i></td>";
        } else {
            $Code = ""; // Wird auch beim Preis abgefragt

            if (strchr($FarbCodes, "~" . $rsReifen["REI_BESTELLKZ"][$i])) {
                $Code = strstr(strstr($FarbCodes, $rsReifen["REI_BESTELLKZ"][$i]), ":");
                $Code = substr($Code, strchr($Code, ":") + 1, 6);
                echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss");

                // ******************
                // Bestaende zeigen
                // ******************

                $FilialenListe = array();
                $ToolTipp = '';
                $ToolTipp = 'Best&auml;nde in ';

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 10;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                $rsBestaendeAnz = $awisRSZeilen;
                $Bestand = $rsBestaende['ASI_WERT'][0];

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 13;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                if ($awisRSZeilen == 1) {
                    $Bestand -= $rsBestaende['ASI_WERT'][0];
                }

                $ToolTipp .= 'Weiden: ' . $Bestand;

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 11;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                $rsBestaendeAnz = $awisRSZeilen;
                $Bestand = $rsBestaende['ASI_WERT'][0];

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 14;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                if ($awisRSZeilen == 1) {
                    $Bestand -= $rsBestaende['ASI_WERT'][0];
                }
                $ToolTipp .= ' / Werl: ' . $Bestand;

                if ($BestandsFIL_ID != '') {
                    $BestandsFIL_ID = str_replace(';', ',', $BestandsFIL_ID);
                    $BestandsFIL_ID = str_replace('/', ',', $BestandsFIL_ID);
                    $BestandsFIL_ID = str_replace('.', ',', $BestandsFIL_ID);
                    $FilListe = explode(',', $BestandsFIL_ID);
                    foreach ($FilListe as $FilID) {
                        if ($FilID != 0) {
                            $FilialenListe[] = (int)($FilID);
                        }
                    }
                    if (!empty($FilialenListe)) {
                        $AnzFilListe = count($FilialenListe);

                        $BindeVariablen = array();
                        $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];

                        $SQL = 'SELECT COALESCE(SUM(FIB_BESTAND),0) as Bestand, FIL_ID';
                        $SQL .= " FROM FILIALEN ";
                        $SQL .= " LEFT OUTER JOIN FilialBestand ON FIL_ID = FIB_FIL_ID AND FIB_AST_ATUNR = :var_T_ast_atunr";
                        $SQL .= " WHERE ";
                        if ($AnzFilListe === 1) {
                            $BindeVariablen['var_N0_fil_id'] = implode(',', $FilialenListe);
                            $SQL .= " FIL_ID = :var_N0_fil_id";
                        } else {
                            $SQL .= " FIL_ID IN (" . implode(',', $FilialenListe) . ')';
                        }
                        $SQL .= " GROUP BY FIL_ID ";
                        $SQL .= " ORDER BY FIL_ID ";
                        $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                        $rsBestaendeAnz = $awisRSZeilen;
                        if ($rsBestaendeAnz > 0) {
                            $ToolTipp .= ', Vortagsbest&auml;nde in ';
                        }
                        $FilialenToolTipp = '';
                        for ($Bestand = 0; $Bestand < $rsBestaendeAnz; $Bestand++) {

                            $FilialenToolTipp .= ' / Filiale ' . $rsBestaende['FIL_ID'][$Bestand] . ': ' . $rsBestaende['BESTAND'][$Bestand];
                        }
                        $ToolTipp .= substr($FilialenToolTipp, 3);
                    }
                }

                echo ' title="' . $ToolTipp . '"';
                echo " ><font color=#$Code>";
                awis_BenutzerParameterSpeichern($con, 'Reifen-Bestandsfilialen', $AWISBenutzer->BenutzerName(),
                    implode(',', $FilialenListe));

                If (($RechteAST & 256) != 256 and $rsSuch2['AST_SUCH2'][0] != '') {
                    echo $rsReifen["REI_AST_ATUNR"][$i];
                } else {
                    if ($RechteAST >= 0) {
                        if ($rsReifen["RPR_KBPREIS"][$i] == '-1' and strtoupper($rsReifen["RPR_K"][$i]) == 'S') {
                            echo '<a href=/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EKAT&AST_ATUNR=' . $rsReifen["REI_AST_ATUNR"][$i] . '&Zurueck=/reifen/reifen_Main.php?cmdAktion=ReifenKB>' . $rsReifen["REI_AST_ATUNR"][$i] . '</a>';
                        } else {
                            echo $rsReifen["REI_AST_ATUNR"][$i];
                        }
                    } else {
                        echo $rsReifen["REI_AST_ATUNR"][$i];
                    }
                }
                echo '</font></td>';

                print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " ><font color=#$Code>" . substr($rsReifen["REI_HERSTELLER"][$i],
                        0, 5) . "</font></td>";
            } else {
                echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss");

                // ******************
                // Bestaende zeigen
                // ******************

                $FilialenListe = array();
                $ToolTipp = '';
                $ToolTipp = 'Best&auml;nde in ';

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 10;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                $rsBestaendeAnz = $awisRSZeilen;
                $Bestand = $rsBestaende['ASI_WERT'][0];

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 13;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                if ($awisRSZeilen == 1) {
                    $Bestand -= $rsBestaende['ASI_WERT'][0];
                }

                $ToolTipp .= 'Weiden: ' . $Bestand;

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 11;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                $rsBestaendeAnz = $awisRSZeilen;
                $Bestand = $rsBestaende['ASI_WERT'][0];

                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_asi_ait_id'] = 14;

                $SQL = 'SELECT ASI_WERT, ASI_KEY FROM ArtikelstammInfos ';
                $SQL .= " WHERE ASI_AST_ATUNR = :var_T_ast_atunr";
                $SQL .= ' AND ASI_AIT_ID = :var_N0_asi_ait_id';
                $rsBestaende = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
                if ($awisRSZeilen == 1) {
                    $Bestand -= $rsBestaende['ASI_WERT'][0];
                }
                $ToolTipp .= ' / Werl: ' . $Bestand;

                if ($BestandsFIL_ID != '') {
                    $BestandsFIL_ID = str_replace(';', ',', $BestandsFIL_ID);
                    $BestandsFIL_ID = str_replace('/', ',', $BestandsFIL_ID);
                    $BestandsFIL_ID = str_replace('.', ',', $BestandsFIL_ID);
                    $FilListe = explode(',', $BestandsFIL_ID);
                    foreach ($FilListe as $FilID) {
                        if ($FilID != 0) {
                            $FilialenListe[] = (int)($FilID);
                        }
                    }
                    if (!empty($FilialenListe)) {
                        $SQL = 'SELECT COALESCE(SUM(FIB_BESTAND),0) as Bestand, FIL_ID';
                        $SQL .= " FROM FILIALEN ";
                        $SQL .= " LEFT OUTER JOIN FilialBestand ON FIL_ID = FIB_FIL_ID AND FIB_AST_ATUNR = '" . $rsReifen["REI_AST_ATUNR"][$i] . "'";
                        $SQL .= " WHERE ";
                        $SQL .= " FIL_ID IN (" . implode(',', $FilialenListe) . ')';
                        $SQL .= " GROUP BY FIL_ID ";
                        $SQL .= " ORDER BY FIL_ID ";
                        $rsBestaende = awisOpenRecordset($con, $SQL);
                        $rsBestaendeAnz = $awisRSZeilen;
                        if ($rsBestaendeAnz > 0) {
                            $ToolTipp .= ', Vortagsbest&auml;nde in ';
                        }
                        $FilialenToolTipp = '';
                        for ($Bestand = 0; $Bestand < $rsBestaendeAnz; $Bestand++) {

                            $FilialenToolTipp .= ' / Filiale ' . $rsBestaende['FIL_ID'][$Bestand] . ': ' . $rsBestaende['BESTAND'][$Bestand];
                        }
                        $ToolTipp .= substr($FilialenToolTipp, 3);
                    }
                }

                echo ' title="' . $ToolTipp . '"';
                echo " ><b>";
                awis_BenutzerParameterSpeichern($con, 'Reifen-Bestandsfilialen', $AWISBenutzer->BenutzerName(),
                    implode(',', $FilialenListe));

                If (($RechteAST & 256) != 256 and $rsSuch2['AST_SUCH2'][0] != '') {
                    echo $rsReifen["REI_AST_ATUNR"][$i];
                } else {
                    if ($RechteAST > 0) {
                        if ($rsReifen["RPR_KBPREIS"][$i] == '-1' and strtoupper($rsReifen["RPR_K"][$i]) == 'S') {
                            echo '<a href=/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=EKAT&AST_ATUNR=' . $rsReifen["REI_AST_ATUNR"][$i] . '&Zurueck=/reifen/reifen_Main.php?cmdAktion=ReifenKB>' . $rsReifen["REI_AST_ATUNR"][$i] . '</a>';
                        } else {
                            echo $rsReifen["REI_AST_ATUNR"][$i];
                        }
                    } else {
                        echo $rsReifen["REI_AST_ATUNR"][$i];
                    }
                }
                echo "</b></td>";

                print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . substr($rsReifen["REI_HERSTELLER"][$i],
                        0, 5) . "</td>";
            }
        }

        // Info-Block

        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=left width=33>" . $rsReifen["REI_BREITE"][$i] . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=left width=20>&nbsp;" . ($rsReifen["REI_QUERSCHNITT"][$i] != ""?$rsReifen["REI_QUERSCHNITT"][$i]:"&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsReifen["REI_BAUART"][$i] . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=20>&nbsp;" . $rsReifen["REI_INNENDURCHMESSER"][$i] . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=15>&nbsp;" . $rsReifen["REI_LLKW"][$i] . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsReifen["REI_LOADINDEX"][$i] . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right width=35>&nbsp;" . $rsReifen["REI_LOADINDEX2"][$i] . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right width=25>&nbsp;" . $rsReifen["REI_SPI_SYMBOL"][$i] . "</td>";

        // Eingef�gt TR 30.04.2007
        if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '' or $rsReifen["REI_ZUSBEM2"][$i] != '' or $rsReifen["REI_ZUSBEM3"][$i] != '' or $rsReifen["REI_ZUSATZBEMERKUNG"][$i] != '') {
            if ($rsReifen["REI_LOADINDEX3"][$i] != '') {
                print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_LOADINDEX3"][$i];
            }

            if ($rsReifen["REI_SPI_SYMBOL2"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '') {
                    print "/" . $rsReifen["REI_SPI_SYMBOL2"][$i];
                } else {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_SPI_SYMBOL2"][$i];
                }
            }

            if ($rsReifen["REI_RF"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '') {
                    print "/<b>" . $rsReifen["REI_RF"][$i] . "</b>";
                } else {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=130>&nbsp;<b>" . $rsReifen["REI_RF"][$i] . "</b>";
                }
            }

            if ($rsReifen["REI_ZUSBEM2"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '') {
                    print "/<b>" . $rsReifen["REI_ZUSBEM2"][$i] . "</b>";
                } else {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=130>&nbsp;<b>" . $rsReifen["REI_ZUSBEM2"][$i] . "</b>";
                }
            }

            if ($rsReifen["REI_ZUSBEM3"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '' or $rsReifen["REI_ZUSBEM2"][$i] != '') {
                    print "/" . $rsReifen["REI_ZUSBEM3"][$i];
                } else {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_ZUSBEM3"][$i];
                }
            }

            if ($rsReifen["REI_ZUSATZBEMERKUNG"][$i] != '') {
                if ($rsReifen["REI_LOADINDEX3"][$i] != '' or $rsReifen["REI_SPI_SYMBOL2"][$i] != '' or $rsReifen["REI_RF"][$i] != '' or $rsReifen["REI_ZUSBEM2"][$i] != '' or $rsReifen["REI_ZUSBEM3"][$i] != '') {
                    print "/" . $rsReifen["REI_ZUSATZBEMERKUNG"][$i];
                } else {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=130>&nbsp;" . $rsReifen["REI_ZUSATZBEMERKUNG"][$i];
                }
            }
            print "</td>";
        } else {
            print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=130>&nbsp;</td>";
        }

        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["ROLLRES"][$i] != ""?$rsReifen["ROLLRES"][$i]:"&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["WETGRIP"][$i] != ""?$rsReifen["WETGRIP"][$i]:"&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["NOISECL"][$i] != ""?$rsReifen["NOISECL"][$i]:"&nbsp;") . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center width=30>" . ($rsReifen["NOISEPE"][$i] != ""?$rsReifen["NOISEPE"][$i]:"&nbsp;") . "</td>";

        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsReifen["REI_BEZEICHNUNG"][$i] . "</td>";
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " ><b>" . $rsReifen["REI_K"][$i] . "</b></td>";

        // Zus�tzliches Kennzeichen aus dem Artikelstamm
        // Artikelstamminfos, Wert 70
        if (($ReifenRechte & 1024) == 1024) {
            $BindeVariablen = array();
            $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
            $BindeVariablen['var_N0_asi_ait_id'] = 70;

            $rsKenn2 = awisOpenRecordset($con,
                "SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=:var_N0_asi_ait_id AND ASI_AST_ATUNR=:var_T_ast_atunr",
                true, false, $BindeVariablen);
            if ($rsKenn2['ASI_WERT'][0] == 'A') {
                echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsKenn2['ASI_WERT'][0] . "</td>";
            } else {
                echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >&nbsp;</td>";
            }
        }

        $VK = $rsReifen["RPR_VK"][$i];

        if ($rsReifen["RPR_KBPREIS"][$i] == '-1') {
            if ($Params[22] != "") {
                // Filialindividuellen Preis hernehmen??
                $BindeVariablen = array();
                $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
                $BindeVariablen['var_N0_fil_id'] = $Params[22];

                $rsVKFil = awisOpenRecordset($con,
                    "SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=:var_T_ast_atunr AND FIB_FIL_ID=:var_N0_fil_id",
                    true, false, $BindeVariablen);
                $rsVKAnz = $awisRSZeilen;

                if ($rsVKAnz > 0) {
                    $VK = $rsVKFil["FIB_VK"][0];
                }
            }
        }

        if ($Code != "") {
            if (isset($Params[21]) and $Params[21] == 'on') // KB-Preis zeigen
            {
                print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><font color=#$Code>" . ($rsReifen["RPR_KBPREIS"][$i] != '-1'?awis_format($rsReifen["RPR_KBPREIS"][$i],
                        'Standardzahl,2'):'') . "</font></td>";
            } else {
                if ($rsReifen["RPR_KBPREIS"][$i] != '-1' and $txtLAN_CODE != 'DE' and $txtLAN_CODE != 'IT' and $txtLAN_CODE != 'NL' and $txtLAN_CODE != 'AT') {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><font color=#$Code></font></td>";
                } elseif ((($Recht253 & 2) == 2) or ($rsReifen["RPR_KBPREIS"][$i] == '-1' and strtoupper($rsReifen["RPR_K"][$i]) == 'S')) {
                    if ($rsSuch2['AST_SUCH2'][0] != '') {
                        if ($rsReifen["RPR_AKTION"][$i] != '0') {
                            print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>i.V.</font></b></td>";
                        } else {
                            print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><font color=#$Code></font></td>";
                        }
                    } else {
                        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><font color=#$Code>" . awis_format($VK,
                                'Standardzahl,2') . "</font></td>";
                    }
                } else {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center><font color=#$Code>auf Anfrage</font></td>";
                }
            }
        } else {
            if (isset($Params[21]) and $Params[21] == 'on') // KB-Preis zeigen
            {
                print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right>" . ($rsReifen["RPR_KBPREIS"][$i] != '-1'?awis_format($rsReifen["RPR_KBPREIS"][$i],
                        'Standardzahl,2'):'') . "</td>";
            } else {
                if ($rsReifen["RPR_KBPREIS"][$i] != '-1' and $txtLAN_CODE != 'DE' and $txtLAN_CODE != 'IT' and $txtLAN_CODE != 'NL' and $txtLAN_CODE != 'AT') {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right></td>";
                } elseif ((($Recht253 & 2) == 2) or ($rsReifen["RPR_KBPREIS"][$i] == '-1' and strtoupper($rsReifen["RPR_K"][$i]) == 'S')) {
                    if ($rsSuch2['AST_SUCH2'][0] != '') {
                        if ($rsReifen["RPR_AKTION"][$i] != '0') {
                            print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>i.V.</font></b></td>";
                        } else {
                            print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right></td>";
                        }
                    } else {
                        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right>" . awis_format($VK,
                                'Standardzahl,2') . "</td>";
                    }
                } else {
                    print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center>auf Anfrage</td>";
                }
            }
        }

        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><center><b><font color=#FF0000>" . ($rsReifen["RPR_RABATT"][$i] == 0?'':awis_format($rsReifen["RPR_RABATT"][$i],
                'Standardzahl,1')) . "</font></b></center></td>";

        $BindeVariablen = array();
        $BindeVariablen['var_T_ast_atunr'] = $rsReifen["REI_AST_ATUNR"][$i];
        $BindeVariablen['var_N0_pri_ity_key'] = 16;
        $BindeVariablen['var_N0_pra_kategorie'] = 1;

        $SQL = 'SELECT PRI_WERT AS ZERTIFIKAT';
        $SQL .= ' FROM PROBLEMARTIKEL';
        $SQL .= ' INNER JOIN PROBLEMARTIKELINFOS ON PRI_PRA_KEY = PRA_KEY AND PRI_ITY_KEY =:var_N0_pri_ity_key';
        $SQL .= ' WHERE PRA_AST_ATUNR=:var_T_ast_atunr';
        $SQL .= ' AND PRA_KATEGORIE =:var_N0_pra_kategorie'; // NUR REIFEN BIS <= 185

        $ProblemReifenKZ = '';
        $Link = '';
        $rsPAR = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);

        if ($awisRSZeilen > 0) {
            $ProblemReifenKZ = '!!!';
            $Link = '';
            if ($rsPAR["ZERTIFIKAT"][0] != '') {
                $ProblemReifenKZ = 'Z!';
                $Link = '/dokumentanzeigen.php?bereich=reifenzertifikate&dateiname=' . $rsPAR["ZERTIFIKAT"][0] . '&erweiterung=pdf';
            }
        }

        if (isset($rsReifen["RPR_AKTIONTEXT"][$i]) and $rsReifen["RPR_AKTIONTEXT"][$i] != '' and is_numeric(str_replace(",",
                ".", $rsReifen["RPR_AKTIONTEXT"][$i])) == false
        ) {
            echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . $rsReifen["RPR_AKTIONTEXT"][$i] . "</font></b></td>";
        } elseif ($rsSuch2['AST_SUCH2'][0] != '') {
            if ($rsReifen["RPR_VK"][$i] == '0') {
                echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=center><b><font color=#FF0000>i.V.</font></b></td>";
            } elseif ($rsReifen["RPR_AKTION"][$i] != '0') {
                echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . ($rsReifen["RPR_AKTION"][$i] == 0?'':awis_format($rsReifen["RPR_AKTION"][$i],
                        'Standardzahl,2')) . "</font></b></td>";
            } else {
                echo '<td ' . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '></td>';
            }
        } else {

            $Aktion = 0;
            // Spalte Aktion berechnen
            if ($rsReifen["RPR_RABATT"][$i] != '0') {
                $Aktion = ((100 - awis_format($rsReifen["RPR_RABATT"][$i], 'Standardzahl,2')) / 100);
                $Aktion = str_replace(',', '.', $Aktion) * str_replace(',', '.', $VK);

                echo "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " align=right><b><font color=#FF0000>" . ($Aktion == 0?'':awis_format($Aktion,
                        'Standardzahl,2')) . "</font></b></td>";
            } else {
                echo '<td ' . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '></td>';
            }
        }
        
        print "<td " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . " >" . $rsReifen["RST_FELGENRIPPE"][$i] . "</td>";

        if($rsReifen['WINTERTAUGLICHKEIT'][$i] !==  NULL and $rsReifen['WINTERTAUGLICHKEIT'][$i] == 'X'){
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . 'X' . "</td>";
        } else {
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . '' . "</td>";
        }

        if($rsReifen['RIN_INFO'][$i] !==  NULL and $rsReifen['RIN_INFO'][$i] != ""){
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . $rsReifen['RIN_INFO'][$i] . "</td>";
        } else {
            print "<td " . (($i % 2) == 0 ? "id=TabellenZeileGrau" : "id=TabellenZeileWeiss") . " >" . '' . "</td>";
        }
        echo "</tr>";
    }

    print "</table>";
    $ZeitVerbrauch = time() - $ZeitVerbrauch;
    print "<br><font size=1>Es wurden $rsReifenAnz Reifen in $ZeitVerbrauch Sekunden gefunden. </font>";

    // TEST auf korrekte Reifenmehrfachkennung
    $SQL = "select rei_ast_atunr, count(rei_ast_atunr) AS ANZ from reifen ";
    $SQL .= "where substr(rei_ast_atunr,-4,4)<>'0000' and SUBSTR(rei_ast_atunr,-3,3)<>'000' ";
    $SQL .= "group by rei_ast_atunr ";
    $SQL .= "minus ";
    $SQL .= "select rei_ast_atunr, sum(mehrfachkennung)+1 as anz ";
    $SQL .= "from(select rei_ast_atunr, decode(rei_mehrfachkennung, null,0,1) as mehrfachkennung from reifen ";
    $SQL .= "where substr(rei_ast_atunr,-4,4)<>'0000' and SUBSTR(rei_ast_atunr,-3,3)<>'000') ";
    $SQL .= "group by rei_ast_atunr";
    $rsReifen = awisOpenRecordset($con, $SQL);
    if ($awisRSZeilen > 0) {
        echo '<hr><span class=Hinweistext>Reifen mit falscher Mehrfachkennung gefunden</span>';
        echo '<table border=1 class=DatenTabelle>';
        echo '<tr><td class=FeldBez>ATU-Nr</td><td class=FeldBez>Anzahl</td></tr>';

        for ($i = 0; $i < $awisRSZeilen; $i++) {
            echo '<tr>';
            echo '<td>' . $rsReifen['REI_AST_ATUNR'][$i] . '</td>';
            echo '<td>' . $rsReifen['ANZ'][$i] . '</td>';
            echo '</tr>';
        }

        echo '</table>';
    }
} else // Nur eine Filiale gefunden, Daten anzeigen
{
    print "<span class=HinweisText>Es wurden keine passenden Reifen f�r die Auswahl gefunden.</span>";
}

// ******************************************************************************
function ReifenTypen($REIID, $con, $SonderID)
{
    global $awisRSZeilen;
    $erg = '';

    if ($SonderID != $REIID and $SonderID != '') // Sondernummer XX0000
    {
        $rsReifenTypen = awisOpenRecordset($con,
            "SELECT DISTINCT RIN_WERT FROM ReifenInfos, Reifen WHERE REI_ID = RIN_REI_ID AND REI_ID='" . $SonderID . "' AND RIN_RNT_ID=1 ORDER BY RIN_WERT");
    } else {
        $rsReifenTypen = awisOpenRecordset($con,
            "SELECT DISTINCT RIN_WERT FROM ReifenInfos, Reifen WHERE REI_ID = RIN_REI_ID AND REI_AST_ATUNR='" . $REIID . "' AND RIN_RNT_ID=1 ORDER BY RIN_WERT");
    }

    for ($rt = 0; $rt < $awisRSZeilen; $rt++) {
        if ($rsReifenTypen["RIN_WERT"][$rt] != '') {
            $erg .= $rsReifenTypen["RIN_WERT"][$rt];
        }
    }

    return $erg;
}

?>