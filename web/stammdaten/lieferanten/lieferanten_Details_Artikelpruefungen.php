<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('APR', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Altteilwert');
    $TextKonserven[] = array('Wort', 'AktuellesSortiment');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'KeineDatenVorhanden');
    $TextKonserven[] = array('Wort', 'ZusammenfassungBestellungen');
    $TextKonserven[] = array('Liste', 'lst_AktivInaktiv');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht607 = $AWISBenutzer->HatDasRecht(607);        // Artikelstamm
    if (($Recht607) == 0) {
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        die();
    }

    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    if (!isset($_GET['SSort'])) {
        $ORDERBY = ' ORDER BY APR_BEARBEITUNGSTAG DESC';
    } else {
        $SortFelder = explode(';', $_GET['SSort']);
        $OrderBy = '';
        foreach ($SortFelder AS $SortFeld) {
            $OrderBy .= ' ' . str_replace('~', ' DESC ', $_GET['SSort']);
        }
        $ORDERBY = ($OrderBy == ''?'':' ORDER BY ' . $OrderBy);
    }

    $SQL = 'SELECT DISTINCT APR_AST_ATUNR, APR_LAR_LARTNR, APR_BEARBEITER, APR_BEARBEITUNGSTAG, LAR_LARTNR';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM ArtikelPruefungen LEFT OUTER JOIN LieferantenArtikel ON APR_LIE_NR = LAR_LIE_NR AND APR_LAR_LARTNR = LAR_LARTNR';
    $SQL .= ' WHERE APR_LIE_NR=\'' . $AWIS_KEY1 . '\'';
    $SQL .= ' ORDER BY APR_AST_ATUNR';

    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($AWIS_KEY2 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $SQL .= $ORDERBY;

    $rsAPR = $DB->RecordsetOeffnen($SQL);
    $Form->Formular_Start();

    if ($rsAPR->AnzahlDatensaetze() > 0) {
        $Form->ZeileStart();
        $Link = './lieferanten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Link .= '&SSort=APR_AST_ATUNR' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'APR_AST_ATUNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_AST_ATUNR'], 150, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Link .= '&SSort=APR_LAR_LARTNR' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'APR_LAR_LARTNR'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_LAR_LARTNR'], 250, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Link .= '&SSort=APR_BEARBEITUNGSTAG' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'APR_BEARBEITUNGSTAG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_BEARBEITUNGSTAG'], 150, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Link .= '&SSort=APR_BEARBEITER' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'APR_BEARBEITER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['APR']['APR_BEARBEITER'], 250, '', $Link);

        $Form->ZeileEnde();

        // Blockweise
        $StartZeile = 0;
        if (isset($_GET['Block'])) {
            $StartZeile = intval($_GET['Block']) * $MaxDSAnzahl;
        }

        $Menge = 0;
        $Vorgaenge = 0;

        $DS = 0;
        while (!$rsAPR->EOF()) {
            $Form->ZeileStart();

            $Form->Erstelle_ListenFeld('*APR_AST_ATUNR', $rsAPR->FeldInhalt('APR_AST_ATUNR'), 20, 150, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('*APR_LAR_LARTNR', $rsAPR->FeldInhalt('APR_LAR_LARTNR'), 40, 250, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('*APR_BEARBEITUNGSTAG', $rsAPR->FeldInhalt('APR_BEARBEITUNGSTAG'), 40, 150, false, ($DS % 2), '', '', 'D');
            $Form->Erstelle_ListenFeld('*APR_BEARBEITER', $rsAPR->FeldInhalt('APR_BEARBEITER'), 40, 250, false, ($DS % 2), '', '', 'T');

            $Form->ZeileEnde();

            $rsAPR->DSWeiter();
            $DS++;
        }

        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Historie';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else {
        $Form->Erstelle_TextFeld('#Hinweis', $AWISSprachKonserven['Wort']['KeineDatenVorhanden'], 20, 500);
    }

    $Form->Formular_Ende();
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812181640");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812181642");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>