<?php
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;        // Zum Cursor setzen
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('ABG', '%');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'Dateiname');
$TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Fehler', 'err_keineRechte');
$TextKonserven[] = array('Fehler', 'err_keingueltigesDatum');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht57000 = $AWISBenutzer->HatDasRecht(57000);
if ($Recht57000 == 0) {
    $Form->Fehler_KeineRechte();
}

$Form->SchreibeHTMLCode('<form name=frmImport method=post action=./airbag_Main.php?cmdAktion=Import enctype="multipart/form-data">');

$Form->Formular_Start();

if (isset($_POST['cmd_Import_x']) and isset($_FILES['Importdatei']) and $_FILES['Importdatei']["name"] != '') {
    if (isset($_POST['txtAltdaten'])) {
        $SQL = 'DELETE FROM AIRBAGS WHERE ABG_XBN_KEY = ' . $AWISBenutzer->BenutzerID();
        $DB->Ausfuehren($SQL);
    }

    if (($fd = fopen($_FILES['Importdatei']['tmp_name'], 'r')) !== false) {
        //Importieren
        $Erg = importKassette($fd);

        //Fehlerausgabe
        if (count($Erg['Fehler']) > 0) {
            $Form->ZeileStart();
            $Form->Hinweistext(implode('<br />', $Erg["Fehler"]));
            $Form->ZeileEnde();
        } else {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['ABG']['ABG_IMPORT_ERFOLGREICH']);
            $Form->ZeileEnde();
        }
    }
}

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'] . ':', 150);
$Form->Erstelle_DateiUpload('Importdatei', 500, 30, 20000000);
$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['ABG']['ABG_ALTDATEN'] . ':', 150);
$Form->Erstelle_Checkbox('Altdaten', '1', 20, true, 1);
$Form->ZeileEnde();

$Form->Formular_Ende();

$Form->SchaltflaechenStart();
$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
$Form->Schaltflaeche('image', 'cmd_Import', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
$Form->SchaltflaechenEnde();

$Form->SchreibeHTMLCode('</form');

/**
 * @param $fd resource          Die zu Importierende Datei
 * @return array
 */
function importKassette($fd)
{
    global $AWISBenutzer;
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $TextKonserven = array();
    $TextKonserven[] = array('ABG', '%');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Erg = [];
    $Fehler = [];
    $Commit = true;

    $DB->TransaktionBegin();

    try {
        while (!feof($fd)) {
            $Zeile = fgets($fd);
            if (strlen($Zeile) == 107) {
                $SQL = 'INSERT INTO AIRBAGS (ABG_BESTELLNR, ABG_FILIALE, ABG_DATUM, ABG_MODELLNR, ABG_MENGE, ABG_BEZEICHNUNG, ABG_AUTO, ABG_XBN_KEY) VALUES (';
                $SQL .= $DB->WertSetzen('ABG', 'T', substr($Zeile, 0, 9)) . ', ';
                $SQL .= $DB->WertSetzen('ABG', 'T', substr($Zeile, 9, 5)) . ', ';
                $SQL .= $DB->WertSetzen('ABG', 'D', substr($Zeile, 14, 8)) . ', ';
                $SQL .= $DB->WertSetzen('ABG', 'T', substr($Zeile, 22, 14)) . ', ';
                $SQL .= $DB->WertSetzen('ABG', 'T', substr($Zeile, 36, 1)) . ', ';
                $SQL .= $DB->WertSetzen('ABG', 'T', substr($Zeile, 37, 39)) . ', ';
                $SQL .= $DB->WertSetzen('ABG', 'T', substr($Zeile, 76, 30)) . ', ';
                $SQL .= $DB->WertSetzen('ABG', 'T', $AWISBenutzer->BenutzerID()) . ')';

                $DB->Ausfuehren($SQL, 'ABG', true, $DB->Bindevariablen('ABG', true));
            } elseif (strlen($Zeile) > 0) {
                $Commit = false;
                $Fehler[] = $AWISSprachKonserven['ABG']['ABG_IMPORT_ERR_FORMAT'];
            }
        }
    } catch (awisException $e) {
        $Commit = false;
        $Fehler[] = $AWISSprachKonserven['ABG']['ABG_IMPORT_ERR_SQL'];
        $Fehler[] = $DB->LetzterSQL();
    }

    //Soll commitet werden
    if ($Commit) {
        $DB->TransaktionCommit();
    } else {
        $DB->TransaktionRollback();
    }

    $Erg["Fehler"] = $Fehler;

    return $Erg;
}
