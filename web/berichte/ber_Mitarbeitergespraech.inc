<?php
require_once 'awisBerichte.inc';

class ber_Mitarbeitergespraech
	extends awisBerichte
{
	private $pdf_vorlage = '';
	private $Zeilenmerker = '';
	private $ZeilenmerkerE = '';
	private $Groesse = '';
	private $Spaltenmerker = '';
	private $letzte_positon = '';
	private $Persnr = '';
	private $Name = '';
	private $Funktion = '';
	private $Abteilung = '';
	private $NameV = '';
	private $FunktionV = '';
	private $Antwort1 = '';
	private $Antwort2 = '';
	private $Antwort3 = '';
	private $Antwort4 = '';
	private $Antwort5 = '';
	private $Antwort6 = '';
	private $Antwort7 = '';
	private $Antwort8 = '';
	private $Entwickl = '';
	private $LetzteZeile = '';
	private $HauptrahmenAnfangY = '';
	private $HauptrahmenLinksX = '';
	private $HauptrahmenEndeY = '';
	
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
		// Alle Textbausteine laden
		$TextKonserven[]=array('PMA','*');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('Wort','Personalnummer');
		$TextKonserven[]=array('Wort','NameVoll');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
		$this->Persnr = $Parameter['PERSNR'];
		$this->Name = $Parameter['NAMEVOLL'];
		$this->Funktion = $Parameter['FUNKTION'];
		$this->Abteilung = $Parameter['ABTEILUNG'];
		$this->NameV = $Parameter['NAMEVOLLV'];
		$this->FunktionV = $Parameter['FUNKTIONV'];
		$this->Antwort1 = $Parameter['ANTWORT1'];
		$this->Antwort2 = $Parameter['ANTWORT2'];
		$this->Antwort3 = $Parameter['ANTWORT3'];
		$this->Antwort4 = $Parameter['ANTWORT4'];
		$this->Antwort5 = $Parameter['ANTWORT5'];
		$this->Antwort6 = $Parameter['ANTWORT6'];
		$this->Antwort7 = $Parameter['ANTWORT7'];
		$this->Antwort8 = $Parameter['ANTWORT8'];
		$this->Entwickl = $Parameter['ENTWICKL'];

	}
	
	/**
	 * Eigentliches PDF erzeugen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins(20,30, 20);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->AliasNbPages();
		$this->SetAutoPageBreak(true,20);		// SK
		
		
		$this->ErstellePMAStammBlatt01();
		
		
		$this->FertigStellen($Ausgabeart);        
	}

	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
	    $this->AddPage();
	}
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		// hier RAHMEN fertig malen
		$this->HauptrahmenEndeY = $this->GetY();
		$this->Line(self::LINKERRAND,$this->PageNo() == 1 ?$this->HauptrahmenAnfangY:20,self::LINKERRAND,$this->HauptrahmenEndeY);
		$this->Line(self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->PageNo() == 1 ?$this->HauptrahmenAnfangY:20,self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->HauptrahmenEndeY);
		$this->Line(self::LINKERRAND,$this->HauptrahmenEndeY,self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->HauptrahmenEndeY);
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		if($this->PageNo() <> 1)
		{
			$this->Line(self::LINKERRAND,20,self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,20);
		}
		$this->Cell(170,10,'Seite '.$this->PageNo().'/{nb}',0,0,'C');
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		
		
	}
	
	public function FertigStellen($Ausgabeart)
	{
	    //*********************************************************************
	    // Berichtsende -> Zur�ckliefern
	    //*********************************************************************
	    switch($Ausgabeart)
	    {
	        case 1:			// PDF im extra Fenster -> Download
	            $this->Output($this->_BerichtsName.'.pdf','D');
	            break;
	        case 2:			// Standard-Stream
	            $this->Output($this->_BerichtsName.'.pdf','I');
	            break;
	        case 3:			// Als String zur�ckliefern
	            return($this->Output($this->_BerichtsName.'.pdf','S'));
	            break;
	        case 4:         // Als Datei speichern
	            $this->Output($this->_Dateiname,'F');
	            break;
	    }
	}
	
	protected function EditierBreite()
	{
		return self::SeitenBreite() - (self::RECHTERRAND + self::LINKERRAND);
	}	
	
	
	
	private function ErstellePMAStammBlatt01()
	{		
		//Startpositionen setzen
		//$this->_Zeile = $this->_Parameter['Seite1-Oben'];
		//$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		//$this->_Zeile += 50;
		$this->NeueSeite();
        $this->Hauptpunkte();
        //$this->Unternehmen();
        //$this->UnternehmenNummer();
		//$this->Empfaenger();
		//$this->Rahmen();
		//$this->Werte();
	}

	private function Hauptpunkte()
	{		
		$KompletteBreite = self::SeitenBreite() - (self::RECHTERRAND + self::LINKERRAND);
		$Abstand = ($KompletteBreite - $this->_Parameter['KopfzeileMittlereBreite']) / 2;
		$KopfzeileHoehe = ($this->_Parameter['Zeilenhoehe'] * 6) + 5;
		
		$ZeileMerker = $this->_Zeile ;
		$this->_Zeile = $this->_Zeile +3;
		
		//$this->_Zeile = 30;
		// komplettes Kopfrechgteck zeichnen
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($KompletteBreite, $KopfzeileHoehe, '', 1, 0, 'C', false);
		
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
		$this->_Spalte = self::LINKERRAND + $Abstand;
		
		// 1. Zeile mit Text in Kopfrechteck
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['KopfzeileMittlereBreite'], $this->_Parameter['Zeilenhoehe'] + 3,$this->_AWISSprachkonserven['PMA']['PDF_FORMULAR'] , 1, 0, 'C', false);
		
		$this->_Spalte = self::LINKERRAND + $Abstand + $this->_Parameter['KopfzeileMittlereBreite'];
		
		// 1. / 2. Zeile rechts (g�ltig in DE)
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($Abstand, $this->_Parameter['Zeilenhoehe'] + 3, $this->_AWISSprachkonserven['PMA']['PDF_GUELTIG'] , 0, 0, 'C', false);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($Abstand, $this->_Parameter['Zeilenhoehe'] + 3, $this->_AWISSprachkonserven['PMA']['PDF_DE'], 0, 0, 'C', false);
		
		$this->_Spalte = self::LINKERRAND + $Abstand;
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['KopfzeileSchriftgroesse']-5);
		$this->_Zeile = $this->_Parameter['Seite1-Oben'] + $this->_Parameter['Zeilenhoehe'] - 4;
			
		// 2. Zeile (�berschrift) mit Text in Kopfrechteck
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['KopfzeileMittlereBreite'], $this->_Parameter['Zeilenhoehe'] * 3,$this->_AWISSprachkonserven['PMA']['PDF_BEZEICHNUNG'], 1, 0, 'C', false);
		
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;
		
		// 3. Zeile (Zust�ndiger) mit Text in Kopfrechteck
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['KopfzeileMittlereBreite'], $this->_Parameter['Zeilenhoehe'] + 1, $this->_AWISSprachkonserven['PMA']['PDF_ACADEMY'], 1, 0, 'L', false);
		
		$this->_Spalte = self::LINKERRAND + $Abstand + $this->_Parameter['KopfzeileMittlereBreite'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($Abstand, $this->_Parameter['Zeilenhoehe'] + 1,'Seite 4 von 4', 1, 0, 'C', false);
		
		$this->_Spalte = self::LINKERRAND + $Abstand;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] + 1;
		
		// 4.1 Zeile (Datum) mit Text in Kopfrechteck
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['KopfzeileMittlereBreite'] / 2, $this->_Parameter['Zeilenhoehe'] + 1, $this->_AWISSprachkonserven['PMA']['PDF_HERAUSGABEDATUM'] . ": " . date('d.m.Y') , 1, 0, 'L', false);
		
		$this->_Spalte += $this->_Parameter['KopfzeileMittlereBreite'] / 2;
		
		// 4.2 Zeile (Version) mit Text in Kopfrechteck
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['KopfzeileMittlereBreite'] / 2, $this->_Parameter['Zeilenhoehe'] + 1, 'Ersetzt Version', 1, 0, 'L', false);
		
		$this->_Spalte = self::LINKERRAND + $Abstand + $this->_Parameter['KopfzeileMittlereBreite'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($Abstand, $this->_Parameter['Zeilenhoehe'] + 1, $this->_AWISSprachkonserven['PMA']['PDF_VERSION'], 1, 0, 'C', false);
		
		// ATU Logo
		$this->_Zeile = $this->_Parameter['Seite1-Oben'] + $this->_Parameter['Zeilenhoehe'] - 3;
		$this->_Spalte = $this->_Parameter['LinkerRand'] - 5;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Image('/daten/web/dokumente/vorlagen/ATU_Deutschland_grau.png', $this->GetX() + 1, $this->GetY(), $Abstand - 2, ($this->_Parameter['Zeilenhoehe'] * 4) - 2);
		
		$this->_Zeile = $this->_Parameter['Seite1-Oben'] + $KopfzeileHoehe;
		
		$KompletteBreite = self::SeitenBreite() - (self::RECHTERRAND + self::LINKERRAND);
		$Abstand = ($KompletteBreite - $this->_Parameter['KopfzeileMittlereBreite']) / 2;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']-6;
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']+5);
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(3, $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PDF_PROTOKOLL']);
		// Hauptrahmen zeichnen
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'] * 5, ($this->_Parameter['Zeilenhoehe'] * 6.5), '',1 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']+1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(3, $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_MITARBEITER']);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-13,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Wort']['Personalnummer'].':',0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->Persnr,0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-13,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Wort']['NameVoll'].':',0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->Name,0 ,0 ,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FUNKTION'].':',0 ,0 ,'R');
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Spalte += $this->_Spalte + 15.5;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->Funktion,0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-15,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_ABTEILUNG'].':',0 ,0 ,'R');
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Spalte += $this->_Spalte + 27;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->Abteilung,0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']+1);
		$this->_Spalte = self::LINKERRAND  ;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell(3, $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_VORGESETZTER']);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Wort']['NameVoll'].':',0 ,0 ,'R');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->NameV,0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-15.5,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FUNKTION'].':',0 ,0 ,'R');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Spalte += $this->_Spalte * 8.8  ;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH']-10, $this->_Parameter['Zeilenhoehe'],$this->FunktionV,0 ,0 ,'L');
		$this->HauptrahmenAnfangY = $this->GetY();
		$this->HauptrahmenLinksX = $this->GetX();
		$this->_Spalte = self::LINKERRAND  ;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		// Erste Frage
		$this->Zeilenmerker = $this->_Zeile;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FRAGE1'],0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_UEBERSCHRIFT1'],0 ,0 ,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort1, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_UEBERSCHRIFT2'],0 ,0 ,'L');
		$this->_Zeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort2, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->LetzteZeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->ZeilenmerkerE = $this->GetY();
		$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		
		
		// Zweite Frage
		$this->_Zeile = $this->LetzteZeile;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FRAGE2'],0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort3, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->LetzteZeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->ZeilenmerkerE = $this->GetY();
		$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		
		// Dritte Frage
		$this->_Zeile = $this->LetzteZeile;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FRAGE3'],0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort4, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->LetzteZeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->ZeilenmerkerE = $this->GetY();
		$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		
		// Vierte Frage
		$this->_Zeile = $this->LetzteZeile;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FRAGE4'],0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort5, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_UEBERSCHRIFT3'],0 ,0 ,'L');
		$this->_Zeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort6, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->LetzteZeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->ZeilenmerkerE = $this->GetY();
		$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		
	
		// F�nfte Frage
		$this->_Zeile = $this->LetzteZeile;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FRAGE5'],0 ,0 ,'L');
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_UEBERSCHRIFT4'].':',0 ,0 ,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort7, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->LetzteZeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->ZeilenmerkerE = $this->GetY();
		$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		$this->_Zeile = $this->Zeilenmerker;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		//$this->Cell($this->_Parameter['AbstandH'] * 5,$this->Groesse, '',1 ,0 ,'L');
	
		// Sechste Frage
		$this->_Zeile = $this->LetzteZeile;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_UEBERSCHRIFT5'].':',0 ,0 ,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Rect($this->_Spalte+2, $this->_Zeile+0.75,2,2);
		if($this->Entwickl == '1')
		{
			$this->Line($this->_Spalte+2,$this->_Zeile+0.75, $this->_Spalte + 4,$this->_Zeile+2.75);
			$this->Line($this->_Spalte + 4, $this->_Zeile+0.75, $this->_Spalte+2, $this->_Zeile+2.75);
		}
		$this->_Spalte = $this->_Spalte + 4;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_ANSTEIGEND'],0 ,0 ,'L');
		$this->Rect($this->_Spalte+20, $this->_Zeile+0.75,2,2);
		if($this->Entwickl == '2')
		{
			$this->Line($this->_Spalte+20,$this->_Zeile+0.75, $this->_Spalte + 22,$this->_Zeile+2.75);
			$this->Line($this->_Spalte + 22, $this->_Zeile+0.75, $this->_Spalte+ 20, $this->_Zeile+2.75);
		}
		$this->_Spalte = $this->_Spalte + 22;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_GLEICHBLEIBEND'],0 ,0 ,'L');
		$this->Rect($this->_Spalte+25, $this->_Zeile+0.75,2,2);
		if($this->Entwickl == '3')
		{
			$this->Line($this->_Spalte+25,$this->_Zeile+0.75, $this->_Spalte+27,$this->_Zeile+2.75);
			$this->Line($this->_Spalte+27, $this->_Zeile+0.75, $this->_Spalte+25, $this->_Zeile+2.75);
		}
		$this->_Spalte = $this->_Spalte + 27;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_NACHLASSEND'],0 ,0 ,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']+2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->LetzteZeile = $this->GetY();
		$this->ZeilenmerkerE = $this->GetY();
		$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		//$this->ZeilenmerkerE = $this->GetY();
		//$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		//$this->_Zeile = $this->Zeilenmerker;
		//$this->SetXY($this->_Spalte, $this->_Zeile);
		//$this->Cell($this->_Parameter['AbstandH'] * 5,$this->Groesse, '',1 ,0 ,'L');
	
		// Siebte Frage
		$this->_Spalte = self::LINKERRAND;
		$this->_Zeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 0.5;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKlein']);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_FRAGE6'],0 ,0 ,'L');
		$this->_Zeile = $this->GetY();
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] + 2;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->MultiCell($this->_Parameter['AbstandH'] * 4.9, $this->_Parameter['Zeilenhoehe']*0.5,$this->Antwort8, 0,'L',0,1);
		$this->_Zeile = $this->GetY();
		$this->LetzteZeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Groesse = $this->ZeilenmerkerE - $this->Zeilenmerker;
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		
		//Unterschrift
		
		$this->_Spalte = self::LINKERRAND;
		$this->_Zeile = $this->LetzteZeile + 2;
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 0.5;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein'] - 2);
		$this->Cell($this->_Parameter['AbstandH']-12,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PDF_UNTERSCHRIFT'],0 ,0 ,'L');
		$this->_Zeile = $this->GetY();
		$this->SetFont($this->_Parameter['Schriftart'],'', $this->_Parameter['SchriftgroesseKlein']-1);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		$this->Zeilenmerker = $this->GetY();
		$this->_Spalte = $this->_Spalte;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_DATUM'],0 ,0 ,'L');
		$this->_Zeile = $this->GetY();
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		$this->_Spalte = $this->_Spalte + 50;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_UNTERSCHRIFTV'],0 ,0 ,'L');
		$this->_Zeile = $this->GetY();
		$this->_Spalte = $this->_Spalte + 80;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($this->_Parameter['AbstandH'], $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['PMA']['PMA_UNTERSCHRIFTM'],0 ,0 ,'L');
		$this->_Zeile = $this->GetY();
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'] + 4;
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->ZeilenmerkerE = $this->GetY();
		if ($this->Zeilenmerker > $this->ZeilenmerkerE)
		{
			$this->Zeilenmerker = 30;
		}
		$this->Line(self::LINKERRAND,$this->GetY(),self::LINKERRAND+$this->_Parameter['AbstandH'] * 5,$this->GetY());
		$this->Line(self::LINKERRAND + 50,$this->Zeilenmerker,self::LINKERRAND + 50,$this->ZeilenmerkerE);
		$this->Line(self::LINKERRAND + 130,$this->Zeilenmerker,self::LINKERRAND + 130,$this->ZeilenmerkerE);
		$this->_Spalte = self::LINKERRAND;
		$this->SetXY($this->_Spalte, $this->_Zeile);
	}
}

?>