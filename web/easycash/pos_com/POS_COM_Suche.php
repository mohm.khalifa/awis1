<?php
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PosComSuche','%');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISWerkzeuge = new awisWerkzeuge();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht13020 = $AWISBenutzer->HatDasRecht(13020);
	if($Recht13020==0)
	{
	    awisEreignis(3,1000,'KundenKfzSuche',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	echo '<form name=frmSuche method=post action=./POS_COM_Main.php?cmdAktion=Details>';

	/**********************************************
	* Eingabemaske
	***********************************************/
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PosComSuche'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}
	
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PosComSuche']['FILIALE'].':',120,'text-decoration:underline; font-weight:bold;');
	$Form->Erstelle_TextFeld('!*FILIALE',($Param['SPEICHERN']=='on'?$Param['FILIALE']:''),20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PosComSuche']['BONDATUM'].':',120,'text-decoration:underline; font-weight:bold;','','Format: YYYYMMDD');
	$Form->Erstelle_TextFeld('!*BONDATUM',($Param['SPEICHERN']=='on'?$Param['BONDATUM']:''),20,200,true,'','','','T','L','Format: YYYYMMDD');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PosComSuche']['BONZEIT'].':',120,'','','Format: HHMMSS');
	$Form->Erstelle_TextFeld('*BONZEIT',($Param['SPEICHERN']=='on'?$Param['BONZEIT']:''),20,200,true,'','','','T','L','Format: HHMMSS');
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	
	$AWISCursorPosition='sucFILIALE';
	
	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071619");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071620");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>