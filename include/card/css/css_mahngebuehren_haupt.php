/* css_mahngebuehren_haupt.php */


<?php

switch($intSchriftgroesse)
{
	case 12:	//Werte f�r die Breite der Spalten / Felder in mahngebuehrenhaupt.php (werden �ber css zugewiesen)
				
				$intHpt_bet=65;			//Breite der Spalte Betrag
				$intHpt_dat=65;		//Breite der Spalte Datum
				$intHpt_mat=195;		//Breite der Spalte Mahngebuehrentext
				$intHpt_rnr=150;		//Breite der Spalte Rechnungs-Nr
				$intHpt_kaz=90;		//Breite der Spalte Kontoauszug
				
				break;
				
	case 16:	//Werte f�r die Breite der Spalten / Felder in mahngebuehrenhaupt.php (werden �ber css zugewiesen)

				$intHpt_bet=80;		//Breite der Spalte Betrag
				$intHpt_dat=80;			//Breite der Spalte Datum
				$intHpt_mat=250;		//Breite der Spalte Mahngebuehrentext
				$intHpt_rnr=180;		//Breite der Spalte Rechnungs-Nr
				$intHpt_kaz=125;		//Breite der Spalte Kontoauszug
				
				break;
			
	default:	//Werte f�r die Breite der Spalten / Felder in mahngebuehrenhaupt.php (werden �ber css zugewiesen)

				$intHpt_bet=75;		//Breite der Spalte Betrag
				$intHpt_dat=75;		//Breite der Spalte Datum
				$intHpt_mat=210;		//Breite der Spalte Mahngebuehrentext
				$intHpt_rnr=200;		//Breite der Spalte Rechnungs-Nr
				$intHpt_kaz=80;		//Breite der Spalte Kontoauszug
}

?>
	.speibut								{	height:<?php echo $intElementhoehe; ?>;
												width:<?php echo $intElementhoehe; ?>;
												display:none; }
												
	.hpt_titel								{	font-weight:bold;
												text-align:center; }
	
	.hpt_bet								{	width:<?php echo $intHpt_bet; ?>px;
												text-align:right; }										
	.hpt_dat,#td_dat						{	width:<?php echo $intHpt_dat; ?>px; }
	
	.hpt_mat								{	width:<?php echo $intHpt_mat; ?>px; }
	
	.hpt_rnr								{	width:<?php echo $intHpt_rnr; ?>px; }
	
	.hpt_kaz								{	width:<?php echo $intHpt_kaz; ?>px; }
	
	#dat									{	display:none; }			/*Felder neue Zeile sind versteckt*/
	#bet,#mat,#but,#rnr,#kaz				{	visibility:hidden; }
	
	

