<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SRFunktionen;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('WTG','%');
	$TextKonserven[]=array('KON','KON_NAME1');
	$TextKonserven[]=array('FEB','FEB_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht11002 = $AWISBenutzer->HatDasRecht(11002);
	if(($Recht11002)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$SABParam = unserialize($AWISBenutzer->ParameterLesen('Formular_110101'));
	$KOVParam = unserialize($AWISBenutzer->ParameterLesen('Formular_SRVerfuegbarkeiten'));
	if(!isset($SABParam['BLOCK']))
	{
		$SABParam['BLOCK']=1;
		$SABParam['ORDERBY']='WTG_DATUMVOM DESC';
		$SABParam['KEY']='';
		$SABParam['SAAKEY']=$AWIS_KEY1;
	}

	// Löschen einer Langzeitausleihe
	if(isset($_GET['Del']))
	{
		$SQL = 'DELETE FROM WERKSTATTTERMINEAUSLEIHE';
		$SQL .= ' WHERE WTG_KEY = '.$DB->WertSetzen('WTG', 'N0', $_GET['Del']);
		$DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('WTG'));
	}
	
	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY WTG_DATUMVOM DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}
	
	$SQL = 'SELECT WERKSTATTTERMINEAUSLEIHE.*, KON_NAME1, KON_NAME2, FEB_BEZEICHNUNG';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM WERKSTATTTERMINEAUSLEIHE ';
	$SQL .= ' INNER JOIN KONTAKTE ON WTG_KON_KEY = KON_KEY';
	$SQL .= ' INNER JOIN FILIALEBENEN ON WTG_FEB_KEY = FEB_KEY';
	$SQL .= ' WHERE WTG_KON_KEY IN(';
	$SQL .= '     SELECT DISTINCT KON_KEY';
	$SQL .= '     FROM Kontakte';
	$SQL .= '     INNER JOIN V_FILIALEBENENROLLEN ON XX1_KON_KEY = KON_KEY';
	$SQL .= '     INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate)'; //Nur die aktuell Zugeordneten anzeigen (TR 16.04.2010)
	$SQL .= '     LEFT OUTER JOIN PERSONALINFOS ON KON_PER_NR = PIF_PER_NR AND PIF_ITY_KEY = 11';	// Stammfiliale
	$SQL .= '     WHERE XX1_FER_KEY = 39';
	$SQL .= '     AND XX1_FEB_KEY = '.$KOVParam['FEB_KEY'];
	$SQL .= '     )';
	
	
	if(isset($_GET['WTG_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['WTG_KEY']);
	}
	elseif($SABParam['KEY']!='' AND !isset($_GET['SListe']))
	{
		$AWIS_KEY2 = ($SABParam['SAAKEY']==$AWIS_KEY1?$SABParam['KEY']:0);
	}
	
	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND WTG_KEY = 0'.$AWIS_KEY2;
		$_GET['WTG_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}


	$SABParam['WTGKEY']=$AWIS_KEY1;
	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$rsSAB = $DB->RecordsetOeffnen($SQL);
$Form->DebugAusgabe(1,$AWIS_KEY2);
    $Form->Formular_Start();

	if($rsSAB->AnzahlDatensaetze()>1 OR isset($_GET['SListe']) OR isset($_GET['Del']) OR $AWIS_KEY2==0)
	{
		$Form->ZeileStart();
		$Icons=array();
		if((intval($Recht11002)&6)!=0)
		{
			$Icons[] = array('new','./srpep_Main.php?cmdAktion=Verfuegbarkeiten&Seite=Ausleihe&WTG_KEY=-1&');
  		}

  		$Form->Erstelle_ListeIcons($Icons,38,-1);

  		$Link = './srpep_Main.php?cmdAktion=Verfuegbarkeiten'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=KON_NAME1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='KON_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME1'],350,'',$Link);
		
  		$Link = './srpep_Main.php?cmdAktion=Verfuegbarkeiten'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=WTG_DATUMVOM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='WTG_DATUMVOM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTG']['WTG_DATUMVOM'],110,'',$Link);

		$Link = './srpep_Main.php?cmdAktion=Verfuegbarkeiten'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=WTG_DATUMBIS'.((isset($_GET['SSort']) AND ($_GET['SSort']=='WTG_DATUMBIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WTG']['WTG_DATUMBIS'],110,'',$Link);
		
  		$Link = './srpep_Main.php?cmdAktion=Verfuegbarkeiten'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=FEB_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FEB_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEB']['FEB_BEZEICHNUNG'],150,'',$Link);
		
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$SABParam['KEY']='';
		
		$DS=0;
		while(!$rsSAB->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht11002&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./srpep_Main.php?cmdAktion=Verfuegbarkeiten&Seite=Ausleihe&KOV_KEY='.$AWIS_KEY1.'&WTG_KEY='.$rsSAB->FeldInhalt('WTG_KEY'));
			}
			if(intval($Recht11002&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./srpep_Main.php?cmdAktion=Verfuegbarkeiten&Seite=Ausleihe&KOV_KEY='.$AWIS_KEY1.'&Del='.$rsSAB->FeldInhalt('WTG_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('*KON_NAME1',$rsSAB->FeldInhalt('KON_NAME1'),20,350,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*WTG_DATUMVOM',$rsSAB->FeldInhalt('WTG_DATUMVOM'),20,110,false,($DS%2),'','','D');
			$Form->Erstelle_ListenFeld('*WTG_DATUMBIS',$rsSAB->FeldInhalt('WTG_DATUMBIS'),20,110,false,($DS%2),'','','D');
			$Form->Erstelle_ListenFeld('*FEB_BEZEICHNUNG',$rsSAB->FeldInhalt('FEB_BEZEICHNUNG'),20,150,false,($DS%2),'','','T');
			$Form->ZeileEnde();

			$rsSAB->DSWeiter();
			$DS++;
		}

		$Link = './srpep_Main.php?cmdAktion=Verfuegbarkeiten&Seite=Ausleihe';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsSAB->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht11002&6);
		$AWIS_KEY2 = $rsSAB->FeldInhalt('WTG_KEY');
		$SABParam['KEY']=$AWIS_KEY2;
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		$Form->Erstelle_HiddenFeld('WTG_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('WTG_KOV_KEY',$AWIS_KEY1);
		//$Form->Erstelle_HiddenFeld('WTG_FEB_KEY',$KOVParam['FEB_KEY']);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./srpep_Main.php?cmdAktion=Verfuegbarkeiten&Seite=Ausleihe&SListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSAB->FeldInhalt('WTG_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSAB->FeldInhalt('WTG_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['WTG']['WTG_KON_KEY'].':',200);
		$SQL = 'SELECT DISTINCT KON_KEY, KON_NAME1 || \', \' || KON_NAME2 AS ANZEIGE';
//		$SQL .= ', COALESCE(PIF_WERT,\'90\') AS STAMMFILIALE';
		$SQL .= ' FROM Kontakte';
		$SQL .= ' INNER JOIN V_FILIALEBENENROLLEN ON XX1_KON_KEY = KON_KEY';
		$SQL .= ' INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate)'; //Nur die aktuell Zugeordneten anzeigen (TR 16.04.2010)
		$SQL .= ' LEFT OUTER JOIN PERSONALINFOS ON KON_PER_NR = PIF_PER_NR AND PIF_ITY_KEY = 11';	// Stammfiliale
		$SQL .= ' WHERE XX1_FER_KEY = 39';
		$SQL .= ' AND XX1_FEB_KEY = '.$KOVParam['FEB_KEY'];
		$SQL .= ' ORDER BY 2';

		$Form->Erstelle_SelectFeld('!WTG_KON_KEY',$rsSAB->FeldInhalt('WTG_KON_KEY'),"200:190",$EditModus,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		if($EditModus)
		{
			$AWISCursorPosition='txtWTG_KON_KEY';
		}
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['WTG']['WTG_FEB_KEY'].':',200);
		$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
		$SQL .= ' FROM Filialebenen';
		$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
		$SQL .= ' AND EXISTS';
		$SQL .= '  (select DISTINCT XX1_FEB_KEY';
		$SQL .= '  FROM V_FILIALEBENENROLLEN';
		$SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate) ';
		$SQL .= '  WHERE XX1_FER_KEY = 39';         // 39=SMART Repair Mitarbeiter
		$SQL .= '  AND  XX1_FEB_KEY = FEB_KEY)';
		$SQL .= ' ORDER BY 2';
		$Form->Erstelle_SelectFeld('!WTG_FEB_KEY',$rsSAB->FeldInhalt('WTG_FEB_KEY'),"200:190",$EditModus,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['WTG']['WTG_DATUMVOM'].':',200);
		$DatDefault = date('d.m.Y');
		$Form->Erstelle_TextFeld('!WTG_DATUMVOM',$rsSAB->FeldInhalt('WTG_DATUMVOM'),10,200,$EditModus,'','','','D','','',$DatDefault);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['WTG']['WTG_DATUMBIS'].':',200);
		$DatDefault = date('d.m.Y',strtotime('today + 4 weeks'));
		$Form->Erstelle_TextFeld('!WTG_DATUMBIS',$rsSAB->FeldInhalt('WTG_DATUMBIS'),10,200,$EditModus,'','','','D','','',$DatDefault);
		$Form->ZeileEnde();
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}
	
	$Form->Formular_Ende();
	
	$AWISBenutzer->ParameterSchreiben('Formular_110101',serialize($SABParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>