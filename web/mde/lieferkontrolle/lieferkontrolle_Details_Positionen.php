<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('LIK','%');	
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','ttt_PositionHinzufuegen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Wort','Rueckfuehrungstyp');
	$TextKonserven[]=array('Wort','Gesamtbestand');
	$TextKonserven[]=array('Wort','Urspruengliche');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$AWISSprache = $AWISBenutzer->BenutzerSprache();

	if($AWISSprache=='')
	{
		$AWISSprache='DE';
	}

	$Recht3600 = $AWISBenutzer->HatDasRecht(3600);
	if($Recht3600==0)
	{
		$Form->Fehler_KeineRechte();
	}
			
	//$Param = unserialize($AWISBenutzer->ParameterLesen('LKKSuche'));
	$Params = unserialize($AWISBenutzer->ParameterLesen('LKKPos'));
		
	//********************************************************
	// Daten suchen
	//********************************************************	
	$BindeVariablen=array();
	$BindeVariablen['var_N0_lkk_key']=$AWIS_KEY1;
	
	$SQL = 'SELECT distinct LKK.*, LIK.*';
	$SQL .= ', row_number() over (order by ' . (isset($_GET['SubSort'])?str_replace('~',' DESC ',$_GET['SubSort']):'LIK_AST_ATUNR'). ') AS ZeilenNr';
	$SQL .=' FROM LieferkontrollenKopf LKK';
	$SQL .=' INNER JOIN Lieferkontrollen LIK on lik_lkk_key = lkk_key';
	$SQL .=' WHERE LKK_KEY=:var_N0_lkk_key';			
	//$SQL .= ' ORDER BY LIK_AST_ATUNR';

	$Form->DebugAusgabe(1,$SQL);
	
	$Block = 1;
	
	if(isset($_REQUEST['Block']) AND !isset($_POST['cmdSpeichern_x']) AND !isset($_POST['cmdAbschliessen_x']))
	{				
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);		
		$Params['BLOCKPOS']=$Block;	
		$AWISBenutzer->ParameterSchreiben('LKKPos',serialize($Params));
	}
	elseif(isset($Param['BLOCKPOS']) AND $Param['BLOCKPOS']!='')
	{
		$Block = intval($Params['BLOCKPOS']);
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//var_dump($SQL,$Param['BLOCKPOS'], $_POST);
	$Form->DebugAusgabe(1,$SQL);
	$rsLIK = $DB->RecordSetOeffnen($SQL,$BindeVariablen);

	if($rsLIK->AnzahlDatensaetze()>=1)						// Liste anzeigen
	{	
		$Form->Formular_Start();
		$Form->ZeileStart();

		$Link = "/mde/lieferkontrolle/lieferkontrolle_Main.php?cmdAktion=Details".(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'')."&LKK_KEY=".$AWIS_KEY1.'&SubSort=';

		$Form->Erstelle_Liste_Ueberschrift('',100);
		$Form->Erstelle_Liste_Ueberschrift('',120);
		$Form->Erstelle_Liste_Ueberschrift('',100);
		$Form->Erstelle_Liste_Ueberschrift('',100);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Gesamtbestand'],150);
		$Form->Erstelle_Liste_Ueberschrift('',50);
		if (($Recht3600 & 8 ) == 8)
		{
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Urspruengliche'],130,'',$Link.''.'LIK_MENGEIST'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_MENGEIST'))?'~':''));
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIK']['LIK_AST_ATUNR'],100,'',$Link.''.'LIK_AST_ATUNR'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_AST_ATUNR'))?'~':''));
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIK']['LIK_DATUMSCAN'],120,'',$Link.''.'LIK_DATUMSCAN'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_DATUMSCAN'))?'~':''));
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIK']['LIK_MENGESOLL'],100,'',$Link.''.'LIK_MENGESOLL'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_MENGESOLL'))?'~':''));
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIK']['LIK_MENGEIST'],100,'',$Link.''.'LIK_MENGEIST'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_MENGEIST'))?'~':''));
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIK']['LIK_SOLLBESTAND'],100,'',$Link.''.'LIK_SOLLBESTAND'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_SOLLBESTAND'))?'~':''));
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIK']['LIK_ISTBESTAND'],100,'',$Link.''.'LIK_ISTBESTAND'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_ISTBESTAND'))?'~':''));
		if(($Recht3600 & 8) == 8)
		{
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIK']['LIK_MENGEIST'],130,'',$Link.''.'LIK_MENGEIST'.((isset($_GET['SubSort']) AND ($_GET['SubSort']=='LIK_MENGEIST'))?'~':''));
		}
		$Form->ZeileEnde();										

		$LIKZeile=0;
		while(!$rsLIK->EOF())
		{
			//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
			if(((($Recht3600&2)==2) AND $DB->FeldInhaltFormat('NO',$rsLIK->FeldInhalt('LIK_STATUS'))<10) OR (($Recht3600&4)==4))
			{
				$EditModus=true;					
			}
			else
			{
				$EditModus=false;				
			}
						
			$Form->ZeileStart();			
			$Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&AST_ATUNR='.$rsLIK->FeldInhalt('LIK_AST_ATUNR');
			$Form->Erstelle_ListenFeld('LIK_AST_ATUNR',$rsLIK->FeldInhalt('LIK_AST_ATUNR'),5,100,false,($LIKZeile%2),'',$Link,'T','L');
			$Form->Erstelle_ListenFeld('LIK_DATUMSCAN',$rsLIK->FeldInhalt('LIK_DATUMSCAN'),5,120,false,($LIKZeile%2),'','','D','L');
			$Form->Erstelle_ListenFeld('LIK_MENGESOLL',$rsLIK->FeldInhalt('LIK_MENGESOLL'),5,100,false,($LIKZeile%2),'','','Z','L');
			$Form->Erstelle_ListenFeld('LIK_MENGEIST_'.$rsLIK->FeldInhalt('LIK_KEY'),$rsLIK->FeldInhalt('LIK_MENGEIST'),5,100,$EditModus,($LIKZeile%2),'','','Z','L');
			$Form->Erstelle_ListenFeld('LIK_SOLLBESTAND_'.$rsLIK->FeldInhalt('LIK_KEY'),$rsLIK->FeldInhalt('LIK_SOLLBESTAND'),5,100,$EditModus,($LIKZeile%2),'','','Z','L');
			$Form->Erstelle_ListenFeld('LIK_ISTBESTAND_'.$rsLIK->FeldInhalt('LIK_KEY'),$rsLIK->FeldInhalt('LIK_ISTBESTAND'),5,100,$EditModus,($LIKZeile%2),'','','Z','L');		
			if(($Recht3600 & 8) == 8)
			{
				$Form->Erstelle_ListenFeld('LIK_MENGESCAN',$rsLIK->FeldInhalt('LIK_MENGESCAN'),5,130,false,($LIKZeile%2),'','','T');				
			}
			$Form->ZeileEnde();
	
			$rsLIK->DSWeiter();
			$LIKZeile++;
		}		

		$Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'&LKK_KEY='.$AWIS_KEY1;
		//$Link = './lieferkontrolle_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'&LKK_KEY='.$AWIS_KEY1.(isset($_POST['txtAlleAnzeigen'])||isset($_GET['ALLE'])?'&ALLE=1':'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
			
		$Form->Formular_Ende();

	}			// Eine einzelne Adresse	
}

catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>