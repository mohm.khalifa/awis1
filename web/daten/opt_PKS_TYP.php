<?php
//**************************************************************
// Liefert die Daten f�r das Verbundfeld in CRM Details
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');


if(!isset($_GET['FILTER']))
{
	die('##PARAM##');
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();


$SQL = 'SELECT a.KTYPNR,BEZ,KBA_NR,PS from kfz_typ a  ';
$SQL .= ' inner join KFZ_KBANR b on a.KTYPNR = b.KTYPNR ';
//$Bedingung = '';

$Bedingung = '';
if($_GET['FILTER'] != '')
{
	$Bedingung .= 'AND (UPPER(BEZ) '.$DB->LikeOderIst($_GET['FILTER'].'*',awisDatenbank::AWIS_LIKE_UPPER);
	$Bedingung .= ' OR UPPER(KBA_NR) '.$DB->LikeOderIst($_GET['FILTER'].'*',awisDatenbank::AWIS_LIKE_UPPER).')';
}
if($_GET['PKS_KHERNR'] and $_GET['PKS_KHERNR'] != '::bitte w�hlen::' and $_GET['PKS_KHERNR'] != 'B')
{
	$Bedingung .= ' AND a.KMODNR in(select kmodnr from kfz_modell where khernr ='.$_GET['PKS_KHERNR'].')';
}
if(isset($_GET['PKS_KMODNR']) and $_GET['PKS_KMODNR'] != '::bitte w�hlen::' and $_GET['PKS_KMODNR'] != 'B')
{
	$Bedingung .= ' AND KMODNR= '.$_GET['PKS_KMODNR'];
}

if($Bedingung != '')
{
	$SQL .= ' WHERE '.substr($Bedingung, 4);
}

$SQL .= ' order by bez,PS';


$rsPKSDaten=$DB->RecordSetOeffnen($SQL);




echo '#~#';		// Trennzeichen
$DS=0;
if(isset($_GET['ZUSATZ'])!='')
{
	$Zusatz = explode('~',urldecode($_GET['ZUSATZ']));
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
	}
	else 
	{
		echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
	}
}
while(!$rsPKSDaten->EOF())
{
	if(++$DS>200 AND $rsPKSDaten->FeldInhalt('BEZ')==$_GET['FILTER'])
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsPKSDaten->FeldInhalt('KTYPNR').'">'.$rsPKSDaten->FeldInhalt('BEZ').', '.$rsPKSDaten->FeldInhalt('PS').'PS, ('.$rsPKSDaten->FeldInhalt('KBA_NR').')</option>';
		}
		else 
		{
			echo '#+#'.$rsPKSDaten->FeldInhalt('KTYPNR').'#+#'.$rsPKSDaten->FeldInhalt('BEZ').', '.$rsPKSDaten->FeldInhalt('PS').'PS, ('.$rsPKSDaten->FeldInhalt('KBA_NR').')';
		}
	}
	elseif($DS<200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsPKSDaten->FeldInhalt('KTYPNR').'">'.$rsPKSDaten->FeldInhalt('BEZ').', '.$rsPKSDaten->FeldInhalt('PS').'PS, ('.$rsPKSDaten->FeldInhalt('KBA_NR').')</option>';
		}
		else 
		{
			echo '#+#'.$rsPKSDaten->FeldInhalt('KTYPNR').'#+#'.$rsPKSDaten->FeldInhalt('BEZ').', '.$rsPKSDaten->FeldInhalt('PS').'PS, ('.$rsPKSDaten->FeldInhalt('KBA_NR').')';
		}
	}
	
	$rsPKSDaten->DSWeiter();
}
if(++$DS>200)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">...</option>';
	}
	else 
	{
		echo '#+##+#...';
	}
	//break;
}
?>