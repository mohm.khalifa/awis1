<?php
/**
 * benutzerparameter_speichern.php
 *
 * Speichert die Daten im Modul benutzerparameter
 *
 * @author  Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 20090504
 * @todo -Keine-
 *
 * Änderungen
 * Wer		Wann		Was
 * ---------------------------------------------------------------
 */

global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('XPP','%');
$TextKonserven[]=array('XBP','%');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');

	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	if(isset($_POST['txtXPP_ID']))
	{
		$AWIS_KEY1 = $_POST['txtXPP_ID'];
		$rsXPP =$DB->RecordSetOeffnen('SELECT * FROM Benutzerparameter WHERE XBP_XPP_ID=' . $_POST['txtXPP_ID'] . ' AND XBP_XBN_KEY = 0'.$AWISBenutzer->BenutzerID());

		if($rsXPP->EOF())		// Neuer Parameter
		{
			$SQL = 'INSERT INTO ';
			$SQL .= ' BENUTZERPARAMETER(XBP_XPP_ID, XBP_XBN_KEY, XBP_WERT) VALUES(';
			$SQL .= ' '. $DB->FeldInhaltFormat('N0',$_POST['txtXPP_ID'],false);
			$SQL .= ', '.$AWISBenutzer->BenutzerID();
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$_POST['txtXBP_WERT'],true);
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200809041224');
			}
		}
		else 					// geänderter Artikel
		{
			$Felder = explode(';',$Form->NameInArray($_POST, 'txtXBP_',1,1));
			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					switch ($FeldName)
					{
						default:
							$WertNeu=$DB->FeldInhaltFormat($rsXPP->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST[$Feld],true);
					}
					$WertAlt=$DB->FeldInhaltFormat($rsXPP->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsXPP->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsXPP->FeldInhalt($FeldName),true);
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}
			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsXPP->FeldInhalt('XPP_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
					$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('GleichzeitigsSpeichern',$Meldung,'WIEDERHOLEN',0,'200809021400');
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Benutzerparameter SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ' WHERE XBP_XPP_ID=0' . $_POST['txtXPP_ID'] . '';
				$SQL .= ' AND XBP_XBN_KEY=0' . $AWISBenutzer->BenutzerID() . '';
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'200809041224');
				}
			}

			$AWIS_KEY1=$_POST['txtXPP_ID'];
		//awis_Debug(1,$FeldListe,$SQL,$Felder);
		}
		$AWISBenutzer->ParameterLesen($_POST['txtXPP_BEZEICHNUNG'],true);
	}

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>