<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUL','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht10003 = $AWISBenutzer->HatDasRecht(10003);
	if($Recht10003==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZUL_KEY';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
	}


	// Daten ermitteln
	$SQL = 'SELECT Zukauflieferscheine.*';
	$SQL .= ' , ZAL_BEZEICHNUNG';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM zukauflieferscheine ';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAL_KEY = ZUL_ZAL_KEY';
	$SQL .= ' WHERE ZUL_ZUB_KEY = '.$DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);

	$AWIS_KEY2=0;
	if(isset($_GET['ZUL_KEY']))
	{
		$AWIS_KEY2=$DB->FeldInhaltFormat('N0',$_GET['ZUL_KEY']);
		$SQL .= ' AND ZUL_KEY = '.$AWIS_KEY2;
	}

	$SQL .= $ORDERBY;


	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUL',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$rsZUL = $DB->RecordSetOeffnen($SQL);

	if($rsZUL->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0)						// Liste anzeigen
	{
		$Form->Formular_Start();

		$Form->ZeileStart();

		if((intval($Recht10003)&4)!=0)
		{
			$Icons[] = array('new','./zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$AWIS_KEY1.'&Seite=Lieferscheine&ZUL_KEY=-1');
			$Form->Erstelle_ListeIcons($Icons,58,-1);
  		}

		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUL_DATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUL_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_DATUM'],120,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUL_EXTERNEID'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUL_EXTERNEID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_EXTERNEID'],250,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUL_LIEFERSCHEINNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUL_LIEFERSCHEINNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_LIEFERSCHEINNR'],150,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUL_LIEFERMENGE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUL_LIEFERMENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_LIEFERMENGE'],150,'',$Link);
		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUL_VKPR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUL_VKPR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_VKPR'],150,'',$Link);
		if(($Recht10003&32)==32)
		{
			$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&ZUB_KEY='.$AWIS_KEY1.'&SSort=ZUL_EK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZUL_EK'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_EK'],150,'',$Link);
		}

		$Form->ZeileEnde();

		$PEBZeile=0;
		$DS=0;
		while(!$rsZUL->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(intval($Recht10003&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine&ZUL_KEY='.$rsZUL->FeldInhalt('ZUL_KEY'));
			}
			if(intval($Recht10003&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./zukaufbestellungen_Main.php?cmdAktion=Details&Seite=Lieferscheine&Del='.$rsZUL->FeldInhalt('ZUL_KEY'));
			}
			if($rsZUL->FeldInhalt('ZUL_EMAIL')!='')	// EMail vorhanden?
			{
				$Icons[] = array('mail','mailto:'.$rsZUL->FeldInhalt('ZUL_EMAIL'));
			}
			$Form->Erstelle_ListeIcons($Icons,58,($DS%2));

			$Form->Erstelle_ListenFeld('#ZUL_DATUM',$rsZUL->FeldInhalt('ZUL_DATUM'),0,120,false,($PEBZeile%2),'','','D');
			$Link = '../zukauflieferscheine/zukauflieferscheine_Main.php?cmdAktion=Details&ZUL_KEY='.$rsZUL->FeldInhalt('ZUL_KEY');
			$Form->Erstelle_ListenFeld('#ZUL_EXTERNEID',$rsZUL->FeldInhalt('ZUL_EXTERNEID'),0,250,false,($PEBZeile%2),'','','T','','');
			$Form->Erstelle_ListenFeld('#ZUL_LIEFERSCHEINNR',$rsZUL->FeldInhalt('ZUL_LIEFERSCHEINNR'),0,150,false,($PEBZeile%2),'',$Link,'T','','');
			$Form->Erstelle_ListenFeld('#ZUL_LIEFERMENGE',$rsZUL->FeldInhalt('ZUL_LIEFERMENGE'),0,150,false,($PEBZeile%2),'','','Nx','','');
			$Form->Erstelle_ListenFeld('#ZUL_VKPR',$rsZUL->FeldInhalt('ZUL_VKPR'),0,150,false,($PEBZeile%2),'','','N2');
			if(($Recht10003&32)==32)
			{
				$Form->Erstelle_ListenFeld('#ZUL_EK',$rsZUL->FeldInhalt('ZUL_EK'),0,150,false,($PEBZeile%2),'','','N2');
			}
			$Form->ZeileEnde();

			$rsZUL->DSWeiter();
			$PEBZeile++;
		}

		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$AWIS_KEY1.''.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->Formular_Start();

		$AWIS_KEY2 = $rsZUL->FeldInhalt('ZUL_KEY');

		$Form->Erstelle_HiddenFeld('ZUL_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('ZUL_ZUB_KEY',$AWIS_KEY1);

					// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./zukaufbestellungen_Main.php?cmdAktion=Details&ZULListe=1&Seite=Lieferscheine accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUL->FeldInhalt('ZUL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUL->FeldInhalt('ZUL_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10003&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_DATUM'].':',150);
		$Form->Erstelle_TextFeld('ZUL_DATUM',$rsZUL->FeldInhalt('ZUL_DATUM'),10,300,$EditRecht,'','','','D');
		$AWISCursorPosition='txtZUL_DATUM';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EXTERNEID'].':',150);
		$Form->Erstelle_TextFeld('ZUL_EXTERNEID',$rsZUL->FeldInhalt('ZUL_EXTERNEID'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_BESTELLID'].':',150);
		$Form->Erstelle_TextFeld('ZUL_BESTELLID',$rsZUL->FeldInhalt('ZUL_BESTELLID'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_VKPR'].':',150);
		$Form->Erstelle_TextFeld('ZUL_VKPR',$rsZUL->FeldInhalt('ZUL_VKPR'),10,300,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WANR'].':',150);
		$Form->Erstelle_TextFeld('ZUL_WANR',$rsZUL->FeldInhalt('ZUL_WANR'),12,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELNUMMER'].':',150);
		$Form->Erstelle_TextFeld('ZUL_ARTIKELNUMMER',$rsZUL->FeldInhalt('ZUL_ARTIKELNUMMER'),30,300,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELBEZEICHNUNG'].':',150);
		$Form->Erstelle_TextFeld('ZUL_ARTIKELBEZEICHNUNG',$rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG'),50,900,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_HERSTELLER'].':',150);
		$Form->Erstelle_TextFeld('ZUL_HERSTELLER',$rsZUL->FeldInhalt('ZUL_HERSTELLER'),10,150,$EditRecht,'','','','T');
		$Form->ZeileEnde();




		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIEFERMENGE'].':',150);
		$Form->Erstelle_TextFeld('ZUL_LIEFERMENGE',$rsZUL->FeldInhalt('ZUL_LIEFERMENGE'),10,150,$EditRecht,'','','','N3');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EINHEITENPROPREIS'].':',150);
		$Form->Erstelle_TextFeld('ZUL_EINHEITENPROPREIS',$rsZUL->FeldInhalt('ZUL_EINHEITENPROPREIS'),10,150,$EditRecht,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WAEHRUNG'].':',150);
		$Form->Erstelle_TextFeld('ZUL_WAEHRUNG',$rsZUL->FeldInhalt('ZUL_WAEHRUNG'),3,150,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_VKPR'].':',150);
		$Form->Erstelle_TextFeld('ZUL_VKPR',$rsZUL->FeldInhalt('ZUL_VKPR'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EKPR'].':',150);
		$Form->Erstelle_TextFeld('ZUL_EKPR',$rsZUL->FeldInhalt('ZUL_EKPR'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_RABATT1'].':',150);
		$Form->Erstelle_TextFeld('ZUL_RABATT1',$rsZUL->FeldInhalt('ZUL_RABATT1'),10,150,$EditRecht,'','','','N4');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_RABATT2'].':',150);
		$Form->Erstelle_TextFeld('ZUL_RABATT2',$rsZUL->FeldInhalt('ZUL_RABATT2'),10,150,$EditRecht,'','','','N4');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_VK'].':',150);
		$Form->Erstelle_TextFeld('ZUL_VK',$rsZUL->FeldInhalt('ZUL_VK'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EK'].':',150);
		$Form->Erstelle_TextFeld('ZUL_EK',$rsZUL->FeldInhalt('ZUL_EK'),10,150,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

				// Abschluss
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ABSCHLUSSART'].':',150);
		$Liste = explode("|",$AWISSprachKonserven['ZUL']['lst_ZUL_ABSCHLUSSART']);
		$Form->Erstelle_SelectFeld('ZUL_ABSCHLUSSART',$rsZUL->FeldInhalt('ZUL_ABSCHLUSSART'),200,$EditRecht,'','','1','','',$Liste,'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ABSCHLUSSGRUND'].':',150);
		$Form->Erstelle_TextFeld('ZUL_ABSCHLUSSGRUND',$rsZUL->FeldInhalt('ZUL_ABSCHLUSSGRUND'),60,900,$EditRecht,'','','','N2');
		$Form->ZeileEnde();


//ZUL_ZAL_KEY
/*
ZUL_MWS_ID
ZUL_ABSCHLUSSART

ZUL_FIL_ID
ZUL_ZUSATZINFOS
*/

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,200908131432);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>