<?php
require_once 'grosskundenkarten_funktionen.php';
require_once('/daten/web/berichte/ber_GrosskundenkartenRoh.inc');

try{

    $GK = new grosskundenkarten_funktionen();

    //Daten laden
    $SQL = 'SELECT DISTINCT KND.KUNDEN_NR, KND.EAN_NR FROM '.$GK->holeKartenTabelle().' KND
                LEFT JOIN EXPRABATTZUORDNUNGEN ON ERZ_KUNDEN_NR = KND.KUNDEN_NR 
                LEFT JOIN '.$GK->holeDestTabelle().' LAND ON KND.KUNDEN_NR = LAND.KUNDEN_NR 
                WHERE KND.KUNDEN_NR IN (' . $_REQUEST['KDNR'] . ') AND KND.GEDRUCKT = 1';

    $rs = $GK->DB->RecordSetOeffnen($SQL);

    while (!$rs->EOF()){


        $Parameter = array();
        $Parameter['KDNR'] = $rs->FeldInhalt('KUNDEN_NR');
        $Parameter['EAN'] = $rs->FeldInhalt('EAN_NR');


        $Bericht = new ber_GrosskundenkartenRoh($GK->AWISBenutzer, $GK->DB, 79);

        $Bericht->init($Parameter);

        $Bericht->setSpeicherort($GK->DruckSpeicherort($rs->FeldInhalt('EAN_NR')));

        $Bericht->ErzeugePDF();

        $rs->DSWeiter();
    }
    echo 'queued';
}catch (Exception $exception){
    echo $GK->getQueueStatus();
}