<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PBM','%');
	$TextKonserven[]=array('PEI','lst_PEI_PEZ_STATUS');
	$TextKonserven[]=array('PEI','PEI_PEZ_STATUS');
	$TextKonserven[]=array('PEI','PEI_BEMERKUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('CV','%');
	$TextKonserven[]=array('CA','%');
	$TextKonserven[]=array('MQC','%');
	$TextKonserven[]=array('LF','%');
	$TextKonserven[]=array('CBT','%');
	$TextKonserven[]=array('CBG','%');
	$TextKonserven[]=array('IBT','%');
	$TextKonserven[]=array('IBG','%');
    $TextKonserven[]=array('FM','%');
    $TextKonserven[]=array('FQ','%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4500 = $AWISBenutzer->HatDasRecht(4500);
	if($Recht4500==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$SQL = 'SELECT PEI_PEZ_STATUS FROM PERSONALEINSAETZE WHERE PEI_KEY = 0'.$AWIS_KEY1;
	$rsPEI = $DB->RecordSetOeffnen($SQL);
	
	$EditRecht=(($Recht4500&32)!=0);
	
	if ($EditRecht==true)
	{
		if ($rsPEI->FeldInhalt('PEI_PEZ_STATUS')=='1')
		{
			$EditRecht = false;
		}
	}	
	
	$script = "<script type='text/javascript'>
	function blendeEinBemerkung(obj,KEY,ID) {	
    	       var checkWert = obj.options[obj.selectedIndex].value;
			   var checkText = obj.options[obj.selectedIndex].text;
    	       if(checkWert == '0' && checkText != '::bitte w�hlen::')
    	       { 
    	           document.getElementById('Bemerkung_'+KEY).style.display = 'block';	    
	           }
    	       else if(checkWert == '1') //nein
    	        {
					document.getElementById('Bemerkung_'+KEY).style.display = 'none';
    	        	document.getElementById('txtPEZ_BEMERKUNG_'+KEY+'_'+ID).value = '';	
				}
    	       else //bitte w�hlen
    	        {
    	            document.getElementById('Bemerkung_'+KEY).style.display = 'none';
					document.getElementById('txtPEZ_BEMERKUNG_'+KEY+'_'+ID).value = '';
    	        }
		
	}
	</script>";
	
	echo $script;
	
	$SQL = 'SELECT *';
	$SQL .= ' FROM Personaleinsaetze';
	$SQL .= ' INNER JOIN Personaleinsberichte on PBE_KEY = PEI_PBE_KEY';
	$SQL .= ' INNER JOIN Personaleinsberichtsfelder ON PBF_PBE_KEY = PBE_KEY';
	//$SQL .= ' LEFT OUTER JOIN Personaleinsberichtszuord ON PEZ_PBF_ID = PBF_ID AND PEZ_PEI_KEY='.$AWIS_KEY1;
	$SQL .= ' WHERE PEI_KEY = 0'.$AWIS_KEY1 .' AND PBF_STATUS=\'A\' AND PBF_PBF_ID=0';
	$SQL .= ' ORDER BY PBF_SORTIERUNG';
	
	$Form->DebugAusgabe(1,$SQL);

	$rsPBF = $DB->RecordSetOeffnen($SQL);

	$i = 1;
	while (!$rsPBF->EOF())
	{		
		$SQL = 'SELECT *';
		$SQL .= ' FROM Personaleinsaetze';
		$SQL .= ' INNER JOIN Personaleinsberichte on PBE_KEY = PEI_PBE_KEY';
		$SQL .= ' INNER JOIN Personaleinsberichtsfelder ON PBF_PBE_KEY = PBE_KEY AND PBF_ID <> 46 AND PBF_ID <> 54';		
		$SQL .= ' LEFT OUTER JOIN Personaleinsberichtszuord ON PEZ_PBF_ID = PBF_ID AND PEZ_PEI_KEY='.$AWIS_KEY1;
		$SQL .= ' WHERE PEI_KEY = 0'.$AWIS_KEY1 .' AND PBF_STATUS=\'A\' AND (PBF_ID = '.$rsPBF->FeldInhalt('PBF_ID').' OR PBF_PBF_ID = '.$rsPBF->FeldInhalt('PBF_ID').')';
		$SQL .= ' ORDER BY PBF_SORTIERUNG';
			
		$rsPEZ = $DB->RecordSetOeffnen($SQL);
        //$Form->DebugAusgabe(1,$SQL);
		while (!$rsPEZ->EOF())
		{								
			$Farbe = '';
			if ($rsPEZ->FeldInhalt('PEZ_WERT')=='0' and $rsPEZ->FeldInhalt('PEZ_BEMERKUNG') == '' and ($rsPEZ->FeldInhalt('PBF_SPALTEN')&2)==2)
			{
				$Farbe = 'color:#FF0000';
			}
			
			
			if($rsPEZ->FeldInhalt('PBF_PBF_ID') == '0' AND $i <> 1)
			{
				$Form->Trennzeile();
			}
					
			//$Form->ZeileStart();
			$Form->ZeileStart('font-size:10pt');
			//echo '<input type=hidden name=txtPEZ_KEY value=0'.$rsPEZ->FeldInhalt('PEZ_KEY').'>';			
	
			//�berschrift
			if($rsPEZ->FeldInhalt('PBF_SPALTEN')=='0')
			{
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven[$rsPEZ->FeldInhalt('PBE_TEXTKONSERVE')][$rsPEZ->FeldInhalt('PBF_BEZEICHNUNG')].':',727);
			}
			else 
			{
				$Form->Erstelle_TextLabel($AWISSprachKonserven[$rsPEZ->FeldInhalt('PBE_TEXTKONSERVE')][$rsPEZ->FeldInhalt('PBF_BEZEICHNUNG')].':',400,$Farbe);
			}
						
			//Feld Wert anzeigen
			if(($rsPEZ->FeldInhalt('PBF_SPALTEN')&1)==1)
			{
				if($rsPEZ->FeldInhalt('PBF_DATENQUELLE')!='')
				{		
					switch(substr($rsPEZ->FeldInhalt('PBF_DATENQUELLE'),0,3))
					{
						case 'TXT':
							$Felder = explode(':',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])), $AWISBenutzer->BenutzerSprache());
							$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
							if ($rsPEZ->FeldInhalt('PBF_BEMERKUNG') != '' or $rsPEZ->FeldInhalt('PBF_SPALTEN')==1)
							{
								$Form->Erstelle_SelectFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
							}
							else 
							{
								$Form->Erstelle_SelectFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'onchange="blendeEinBemerkung(this,'.$rsPEZ->FeldInhalt('PBF_ID').','.$rsPEZ->FeldInhalt('PEZ_KEY').')";');
							}
							break;
						case 'SQL':
							$Felder = explode(':',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							if ($rsPEZ->FeldInhalt('PBF_BEMERKUNG') != '' or $rsPEZ->FeldInhalt('PBF_SPALTEN')==1)
							{
								$Form->Erstelle_SelectFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','');
							}
							else
							{
								$Form->Erstelle_SelectFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','onchange="blendeEinBemerkung(this,'.$rsPEZ->FeldInhalt('PBF_ID').','.$rsPEZ->FeldInhalt('PEZ_KEY').')";');
							}
							break;	
						case 'FIL':
							$Felder = explode(':',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							$SQL2 = $Felder[1];
							$SQL2 .= 'and FIL_ID = '.$rsPEZ->FeldInhalt('PEI_FIL_ID');
							$rsFIL = $DB->RecordSetOeffnen($SQL2);
							$Form->Erstelle_TextFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_ZEICHEN'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','','',$rsPEZ->FeldInhalt('PBF_FORMAT'),'','','');
							break;
						case 'GBL':
							$Felder = explode(':',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							$SQL2 = $Felder[1];
							$SQL2 = str_replace('#FIL_ID#',$rsPEZ->FeldInhalt('PEI_FIL_ID'),$SQL2);
							$rsFIL = $DB->RecordSetOeffnen($SQL2);
							$Form->Erstelle_TextFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_ZEICHEN'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','','',$rsPEZ->FeldInhalt('PBF_FORMAT'),'','','');
							break;
						default:
							$Form->Erstelle_TextFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_ZEICHEN'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','','',$rsPEZ->FeldInhalt('PBF_FORMAT'),'','',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							break;			
					}		
			
				}
				elseif(strpos($rsPEZ->FeldInhalt('PBF_ZEICHEN'),',')===false)
				{
					$Form->Erstelle_TextFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_ZEICHEN'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','','',$rsPEZ->FeldInhalt('PBF_FORMAT'),'','',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'),254);
				}
				else
				{
				    $Form->ZeileEnde();
				    $Form->ZeileStart();
					$Groesse = explode(',',$rsPEZ->FeldInhalt('PBF_ZEICHEN'));
					$Form->Erstelle_Textarea('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),800,$Groesse[0],$Groesse[1],$EditRecht);
				}				
			}
			
			//Feld Bemerkung anzeigen			
			if(($rsPEZ->FeldInhalt('PBF_SPALTEN')&2)==2)
			{
				if ($rsPEZ->FeldInhalt('PBF_BEMERKUNG') != '')
				{				
					$Form->Erstelle_TextLabel($rsPEZ->FeldInhalt('PBF_BEMERKUNG'),80,''); //$AWISSprachKonserven[$rsPEZ->FeldInhalt('PBE_TEXTKONSERVE')][$rsPEZ->FeldInhalt('PBF_BEZEICHNUNG')].':',400);
					$Form->Erstelle_TextFeld('PEZ_BEMERKUNG_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_BEMERKUNG'),5,20,$EditRecht,'','','','N2','','','',5);							
				}
		
				else
				{
					if($rsPEZ->FeldInhalt('PEZ_WERT') == '0')
					{
						$type = 'block';
					}
					else
					{
						$type = 'none';
					}
					$Form->ZeileEnde();
					$Form->ZeileStart();
					$Form->SchreibeHTMLCode('<div id="Bemerkung_'.$rsPEZ->FeldInhalt('PBF_ID').'" style="display:'.$type.'">');
					$Color = $EditRecht?'red':'black';

                    $Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_BEMERKUNG'].':',90,"color: $Color; font-weight: bold;");
					if($EditRecht ==false)
					{
					    //$Form->Erstelle_TextFeld('PEZ_BEMERKUNG_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),substr($rsPEZ->FeldInhalt('PEZ_BEMERKUNG'),0,25),26,180,$EditRecht,'','','','T','',$rsPEZ->FeldInhalt('PEZ_BEMERKUNG'),'',4000);
					    $Form->Erstelle_TextLabel($rsPEZ->FeldInhalt('PEZ_BEMERKUNG'), '','','',$rsPEZ->FeldInhalt('PEZ_BEMERKUNG'));
					}
					else
					{
					    $Form->Erstelle_Textarea('PEZ_BEMERKUNG_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_BEMERKUNG'),20,100,1,$EditRecht,'','','','','',4000);
					}
					    
					$Form->SchreibeHTMLCode('</div>');
				}
					
			}					
			$Form->ZeileEnde();
			
			if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')!==false)
			{
				$Form->ZeileStart('line-height:5px;');
			}
			else
			{
				$Form->ZeileStart('height:2mm;font-size:10px');
			}
			$Form->ZeileEnde();
			
			$i++;
			$rsPEZ->DSWeiter();
		}
	$rsPBF->DSWeiter();	
	}		
	
	$Form->Trennzeile();
	//$Form->ZeileStart();
	$Form->ZeileStart('font-size:10pt');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_PEZ_STATUS'].':',200);	
	$KateogrieAlt = explode("|",$AWISSprachKonserven['PEI']['lst_PEI_PEZ_STATUS']);
	$Form->Erstelle_SelectFeld('PEI_PEZ_STATUS',$rsPEI->FeldInhalt('PEI_PEZ_STATUS'),200,$EditRecht,'','','','','',$KateogrieAlt,'');						
	$Form->ZeileEnde();		
	
	$Form->Formular_Ende();	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,1102031407);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>