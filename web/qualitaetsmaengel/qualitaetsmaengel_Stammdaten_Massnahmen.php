<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('QMM','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_QMP'));
	
	
	if($Recht34000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Form->Formular_Start();
	
	$FeldBreiten = array();
	$FeldBreiten['Lang'] = 400;
	$FeldBreiten['Aktiv'] = 50;
	$Param['BLOCK']=1;
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'qmm_key';
	}
	if ((isset($_GET['Sort']) and $_GET['Sort'] != '') and !isset($_GET['QMK_KEY']))
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
	$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}
	
	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	

	
	$SQL  = ' Select qmm_key,qmm_bezeichnung,qmm_user,qmm_userdat,qmm_aktiv, ';
	$SQL .='row_number() OVER (';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ' ;
	$SQL .= ' Select * from QMMASSNAHMEN ';
	
	$Bedingung = '';
	if(isset($_GET['QMM_KEY']))
	{
		$Bedingung .= 'and qmm_key=\''.$_GET['QMM_KEY'].'\'';
	}
	if($Bedingung <> '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	$SQL .= ')';
	
	
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Blättern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	
	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	$rsQMK = $DB->RecordSetOeffnen($SQL);
	
	
	if(!isset($_GET['QMM_KEY']))
	{
		$Form->ZeileStart('width:100%;');
		$Icons = array();
		$Icons[] = array('new','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&QMM_KEY=-1','g');
		$Form->Erstelle_ListeIcons($Icons,38,0);
		
		$Link= '';
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMM_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMM_BEZEICHNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_LANG'], $FeldBreiten['Lang'], '', $Link);
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMM_AKTIV,QMM_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMM_AKTIV,QMM_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_AKTIV'], $FeldBreiten['Aktiv'], '', '');
		$Form->ZeileEnde();
		$DS = 0;
		while(! $rsQMK->EOF())
		{
		
			$Form->ZeileStart('width:100%');
			$Icons = array();
			//$Icons[] = array('edit','./filialinfos_Main.php?cmdAktion=Details&FFA_KEY='.$rsFFA->FeldInhalt('FFA_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			$Icons[] = array('edit','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&QMM_KEY='.$rsQMK->FeldInhalt('QMM_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			$Icons[] = array('delete','./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Del='.$rsQMK->FeldInhalt('QMM_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):''));
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$TTT =  $rsQMK->FeldInhalt('QMM_BEZEICHNUNG');
			$Form->Erstelle_ListenFeld('QMK_LANG',$rsQMK->FeldInhalt('QMM_BEZEICHNUNG'), 0, $FeldBreiten['Lang'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsQMK->FeldInhalt('QMM_AKTIV');
			$Form->Erstelle_ListenFeld('QMK_AKTIV',$rsQMK->FeldInhalt('QMM_AKTIV') == 1 ? 'Ja' : 'Nein', 0, $FeldBreiten['Aktiv'], false, ($DS%2), '','', 'T', 'C', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsQMK->DSWeiter();
		}
		$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'];
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	else
	{
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite=Massnahmen accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsQMK->FeldInhalt('QMM_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsQMK->FeldInhalt('QMM_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMM_LANG'] . ':',140);
		$Form->Erstelle_TextFeld('QMM_BESCHREIBUNG',!isset($_POST['cmdSpeichern_x'])?$rsQMK->FeldInhalt('QMM_BEZEICHNUNG'): $_POST['txtQMM_BESCHREIBUNG'], 40,170,true, '','', '','', 'L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['QMM']['QMK_AKTIV'] . ':',140);
		if (($rsQMK->AnzahlDatensaetze() == 0))
		{
			$Form->Erstelle_Checkbox('CHK_QMM_AKTIV',!isset($_POST['cmdSpeichern_x']) or isset($_POST['txtCHK_QMM_AKTIV']) ? 1 : 0,40,true,1);
		}
		else
		{
			$Form->Erstelle_Checkbox('CHK_QMM_AKTIV',!isset($_POST['cmdSpeichern_x']) ? $rsQMK->FeldInhalt('QMM_AKTIV') : isset($_POST['txtCHK_QMM_AKTIV']) ? 1 : 0,40,true,1);
		}
		$Form->ZeileEnde();
		
		if(isset($_GET['QMM_KEY']))
		{
			$Form->Erstelle_HiddenFeld('QMM_KEY',$_GET['QMM_KEY']);
		}
		
	}
	
	$Form->Formular_Ende();
	$AWISBenutzer->ParameterSchreiben('Formular_QMP',serialize($Param));
	
	
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}



?>