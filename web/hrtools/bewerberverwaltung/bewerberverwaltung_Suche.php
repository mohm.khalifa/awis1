<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try {
    $TextKonserven = array();
    //$TextKonserven[]=array('QMM','*');
    $TextKonserven[] = array('BEW', '*');
    $TextKonserven[] = array('HRK', '*');

    $TextKonserven[] = array('FIL', 'FIL_GEBIET');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $script = "<script type='text/javascript'>
	
	    
	function setzePunkte(Feld) {
			 v=Feld.value;
          if(v.match(/^\d{2}$/))
            {
              Feld.value+='.';return;
            }
          if(v.match(/^\d{2}\.\d{2}$/))
            {
              Feld.value+='.';return;
            }
	    
		 }    
	    
	function blendeEinFirmierung(obj) {
		var checkWert = obj.options[obj.selectedIndex].value;
		
		if(checkWert == 1)
		{
			document.getElementById('ABTEILUNG').style.display = 'none'; 
			document.getElementById('FILIALE').style.display = 'block';
			document.getElementsByName('sucBEW_ABTEILUNG')[0].value = '::bitte w�hlen::'  // Ablehnungsgrund leeren                                                                         0
		}	
		else if(checkWert == 2) // wenn Storno Stornogrund mit �berschrift einbleden
		{
			document.getElementById('ABTEILUNG').style.display = 'block';
			document.getElementById('FILIALE').style.display = 'none';
			document.getElementsByName('sucBEW_ABTEILUNG')[0].value = '::bitte w�hlen::'  // Ablehnungsgrund leeren
			document.getElementsByName('sucBEW_FILIALE')[0].value = ''
		}
		else if(checkWert == 3) // wenn Storno Stornogrund mit �berschrift einbleden
		{
			document.getElementById('ABTEILUNG').style.display = 'block';
			document.getElementById('FILIALE').style.display = 'none';
			document.getElementsByName('sucBEW_ABTEILUNG')[0].value = '::bitte w�hlen::'  // Ablehnungsgrund leeren
			document.getElementsByName('sucBEW_FILIALE')[0].value = ''
		}
		else // wenn Storno Stornogrund mit �berschrift einbleden
		{
			document.getElementById('ABTEILUNG').style.display = 'none';
			document.getElementById('FILIALE').style.display = 'none';
			document.getElementsByName('sucBEW_ABTEILUNG')[0].value = '::bitte w�hlen::'  // Ablehnungsgrund leeren
			document.getElementsByName('sucBEW_FILIALE')[0].value = ''
		}
	}
	</script>";

    echo $script;

    $Recht39001 = $AWISBenutzer->HatDasRecht(39001);

    if ($Recht39001 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Form->SchreibeHTMLCode('<form name="frmBEWsuche" action="./bewerberverwaltung_Main.php?cmdAktion=Details" method="POST"  >');

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BEW'));
    //var_dump($Param);
    $Form->Formular_Start();
    $Speichern = isset($Param['SPEICHERN']) && $Param['SPEICHERN'] == 'on'?true:false;

    /**
     * Bewerbernachname
     */
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_NACHNAME'] . ':', 200);
    $Form->Erstelle_TextFeld('*HRK_NACHNAME', ($Speichern?$Param['HRK_NACHNAME']:''), 25, 250, true, '', '', '', 'T',
        'L', '', '', 50);
    $Form->ZeileEnde();
    $AWISCursorPosition = 'sucHRK_NACHNAME';

    /**
     * Bewerbervorname
     */
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_VORNAME'] . ':', 200);
    $Form->Erstelle_TextFeld('*HRK_VORNAME', ($Speichern?$Param['HRK_VORNAME']:''), 25, 250, true, '', '', '', 'T', 'L',
        '', '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_EMAIL'] . ':', 200);
    $Form->Erstelle_TextFeld('*HRK_EMAIL', ($Speichern?$Param['HRK_EMAIL']:''), 25, 250, true, '', '', '', 'T', 'L', '',
        '', 50);
    $Form->ZeileEnde();

    /**
     * Geburtsdatum
     */
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_GEBURTSDATUM'] . ':', 200);
    $Form->Erstelle_TextFeld('*HRK_GEBURTSDATUM', ($Speichern?$Param['HRK_GEBURTSDATUM']:''), 22, 250, true, '', '', '',
        'D', 'L', '', '', 50, 'onkeyup="setzePunkte(this)"');
    $Form->ZeileEnde();

    /**
     * Ort
     */
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HRK']['HRK_ORT'] . ':', 200);
    $Form->Erstelle_TextFeld('*HRK_ORT', ($Speichern?$Param['HRK_ORT']:''), 25, 250, true, '', '', '', 'T', 'L', '', '',
        50);
    $Form->ZeileEnde();

    /**
     *  Firmierung
     */
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_FIRMIERUNG'] . ':', 200);
    //$Daten = explode('|',$AWISSprachKonserven['BEW']['BEW_LST_FIRMIERUNG']);
    $SQLFirm = 'SELECT HRF_KEY, HRF_FIRMIERUNG from HRFIRMIERUNG where HRF_AKTIV = 1';

    $Form->Erstelle_SelectFeld('*BEW_FIRMIERUNG', ($Speichern?$Param['BEW_FIRMIERUNG']:''), 150, true, $SQLFirm,
        $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', 'onchange="blendeEinFirmierung(this);"');
    $Form->ZeileEnde();

    /**
     * Abteilungen
     */
    if ($Speichern and ($Param['BEW_FIRMIERUNG'] == 2 or $Param['BEW_FIRMIERUNG'] == 3)) {
        $Form->ZeileStart('display:block', 'GS"ID="ABTEILUNG');
    } else {
        $Form->ZeileStart('display:none', 'GS"ID="ABTEILUNG');
    }
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_ABTEILUNG'] . ':', 200);

    $SQL = ' SELECT KAB_KEY,KAB_ABTEILUNG FROM KONTAKTEABTEILUNGEN '; // Alle Aktiven Versicherungen f�r Parksch�denerfassung holen
    $SQL .= ' ORDER BY KAB_ABTEILUNG';

    $Form->Erstelle_SelectFeld('*BEW_ABTEILUNG', ($Speichern?$Param['BEW_ABTEILUNG']:''), 400, true, $SQL,
        $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
    $Form->ZeileEnde();

    /**
     * Filiale
     */
    if ($Speichern and $Param['BEW_FIRMIERUNG'] == 1) {
        $Form->ZeileStart('display:block', 'GS"ID="FILIALE');
    } else {
        $Form->ZeileStart('display:none', 'GS"ID="FILIALE');
    }
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_FILIALE'] . ':', 200);
    $Form->Erstelle_TextFeld('*BEW_FILIALE', ($Speichern?$Param['BEW_FILIALE']:''), 4, 250, true, '', '', '', 'T', 'L',
        '', '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_GEBIET'] . ':', 200);
    $Form->Erstelle_TextFeld('*BEW_GEBIET', ($Speichern?$Param['BEW_GEBIET']:''), 25, 250, true, '', '', '', 'T', 'L',
        '', '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_REGION'] . ':', 200);
    $Form->Erstelle_TextFeld('*BEW_REGION', ($Speichern?$Param['BEW_REGION']:''), 25, 250, true, '', '', '', 'T', 'L',
        '', '', 50);
    $Form->ZeileEnde();

    $Form->ZeileEnde();

    /**
     * T�tigkeiten
     */

    $SpKey = '';
    $SpSuch = $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
    if ($Speichern and $Param['BEW_TAETIGKEITEN'] != '') {
        $SQL2 = ' select * from hrtaetigkeiten';
        $SQL2 .= ' where hrt_key=' . $Param['BEW_TAETIGKEITEN'];
        $SQL2 .= ' and hrt_aktiv=1';
        $rsHRTSp = $DB->RecordSetOeffnen($SQL2);
        if ($rsHRTSp->AnzahlDatensaetze() != 0) {
            $SpKey = $rsHRTSp->FeldInhalt('HRT_KEY');
            $SpSuch = $rsHRTSp->FeldInhalt('HRT_SUCH');
        }
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_TAETIGKEITEN'] . ':', 180);
    $Form->Erstelle_SelectFeld('*BEW_TAETIGKEITEN', $Speichern?$Param['BEW_TAETIGKEITEN']:'', 600, true,
        '***BEW_Daten;sucBEW_TAETIGKEITEN;BEW_FIRMIERUNG=*sucBEW_FIRMIERUNG&ZUSATZ=~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'],
        ((isset($Param['BEW_TAETIGKEITEN']) && $Param['BEW_TAETIGKEITEN'] == '0' or !$Speichern)?'~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']:$SpKey . '~' . $SpSuch),
        '', '', '');
    $Form->ZeileEnde();

    $Daten = explode('|', $AWISSprachKonserven['BEW']['HRT_LST_AKTIV']);

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_STATUS'] . ':', 200);
    $Form->Erstelle_SelectFeld('*BEW_STATUS', $Speichern?$Param['BEW_STATUS']:'', 500, true, '',
        $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_VORSTELLUNGSGESPRAECH'] . ':', 200);
    $Daten = explode('|', $AWISSprachKonserven['BEW']['BEW_lst_JaNein']);

    $Form->Erstelle_SelectFeld('*BEW_VORSTELLUNGSGESPRAECH', $Speichern?$Param['BEW_VORSTELLUNGSGESPRAECH']:'', 500,
        true, '', $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_PDF_DATUMVG'] . ':', 200);
    $Form->Erstelle_TextFeld('*BEW_VORSTELLUNGSDATUM', ($Speichern?$Param['BEW_VORSTELLUNGSDATUM']:''), 22, 250, true,
        '', '', '', 'D', 'L', '', '', 50, 'onkeyup="setzePunkte(this)"');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['BEW']['BEW_INTERNER_BEWERBER'] . ':', 200);
    $Daten = explode('|', $AWISSprachKonserven['BEW']['BEW_lst_JaNein']);

    $Form->Erstelle_SelectFeld('*BEW_INTERNER_BEWERBER', $Speichern?$Param['BEW_INTERNER_BEWERBER']:'', 500, true, '',
        $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten);
    //$Form->Erstelle_TextFeld('*BEW_VORSTELLUNGSGESPRAECH',($Speichern?$Param['BEW_VORSTELLUNGSGESPRAECH']:''),25,50, true, '', '', '', 'T', 'L','','',50);
    $Form->ZeileEnde();

    // Auswahl kann gespeichert werden
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/hrtools/index.php', '/bilder/cmd_zurueck.png',
        $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'],
        'W');

    if (($Recht39001 & 4) == 4) {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png',
            $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
    $Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241613");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>