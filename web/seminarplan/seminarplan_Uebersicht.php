<?php

global $AWISCursorPosition;
global $AWIS_KEY1;

try
{
	$AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
    $TextKonserven = array();
    $TextKonserven[]=array('SEP','*');    
    $TextKonserven[]=array('SET','*');    
    $TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Filiale');

    $Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4202 = $AWISBenutzer->HatDasRecht(4202);

	if($Recht4202==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$Form->DebugAusgabe(1,$_POST);
	
	//Pr�fen, ob Filiale angemeldet ist
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);
	
	if ($FilZugriff != '' and count($FilZugriffListe)==1
		and !isset($_GET['Liste']) and !isset($_POST['Block']) 
		and !isset($_GET['Sort']) and !isset($_GET['Block']))// and !isset($FIL_ID))
	{	
		$FIL_ID=$FilZugriff;
	}

	if(!isset($_POST['cmdSuche_x']) and !isset($_POST['sucSET_FIL_NR'])
		and !isset($_GET['Liste']) and !isset($_POST['Block']) 
		and !isset($_GET['Sort']) and !isset($_GET['Block']) and !isset($FIL_ID))
	{	
		$Form->SchreibeHTMLCode('<form name="frmSeminarplanUebersichtSuche" action="./seminarplan_Main.php?cmdAktion=Uebersicht" method="POST"  enctype="multipart/form-data">');
				
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Filiale'].':',200);
		$Form->Erstelle_TextFeld('*SET_FIL_NR','',20,200,true);
		$AWISCursorPosition='sucSET_FIL_NR';
		$Form->ZeileEnde();		
		$Form->Formular_Ende();
	
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');		
		$Form->SchaltflaechenEnde();
		
		$Form->SchreibeHTMLCode('</form>');
	}
	else if(isset($_POST['cmdSuche_x']) and isset($_POST['sucSET_FIL_NR']) and $DB->FeldInhaltFormat('Z',$_POST['sucSET_FIL_NR']) <= 0)
	{		
		$Form->ZeileStart();
		$Form->Hinweistext('Sie m�ssen eine Filiale eingeben!');
		$Form->ZeileEnde();		
		
		$Link='./seminarplan_Main.php?cmdAktion=Uebersicht';
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmdZurueck',$Link,'/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
		$Form->SchaltflaechenEnde();
	}		
	else 
	{							
		if(isset($_POST['cmdSuche_x']) or isset($_POST['sucSET_FIL_NR']) or isset($FIL_ID)) 
		{
			$Param['KEY']=0;			// Key
						
			if (isset($FIL_ID))
			{
				$Param['SET_FIL_NR']=$FIL_ID;				
			}
			else 
			{
				$Param['SET_FIL_NR']=$_POST['sucSET_FIL_NR'];
			}
			
			$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
		
			$Param['ORDERBY']='';
			$Param['BLOCK']='';
			$Param['KEY']=0;
		
			$AWISBenutzer->ParameterSchreiben("SETUebersichtSuche",serialize($Param));			
		}
					
		$Param = unserialize($AWISBenutzer->ParameterLesen("SETUebersichtSuche"));
		
		//********************************************************
		// Bedingung erstellen
		//********************************************************	
		$Bedingung = _BedingungErstellen($Param);
		
		//*****************************************************************
		// Sortierung aufbauen
		//*****************************************************************
		if(!isset($_GET['Sort']))
		{
			if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
			{
				$ORDERBY = $Param['ORDERBY'];
			}
			else
			{
				$ORDERBY = ' SET_NACHNAME, SET_VORNAME';
			}		
		}
		else
		{
			$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
		}
		
		
		//********************************************************
		// Daten suchen
		//********************************************************
		$SQL = 'SELECT DISTINCT SEP_KEY, SEP_SEMINAR, SEP_BESCHREIBUNG, SEP_TRAINING, SEP_VON, SEP_BIS';
		$SQL.= ', SEP_FREIEPLAETZE, SEP_SER_KEY, SER_RAUM, SEP_SRG_KEY, SRG_REGION, SEP_ZIELGRUPPE, SEP_TRAINER';
		$SQL.= ', SET_NACHNAME, SET_VORNAME, SET_FIL_NR';	
		$SQL.= ', SEP_USER, SEP_USERDAT';	
		if($AWIS_KEY1<=0)
		{
			$SQL .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
		}		
		$SQL .= ' FROM SEMINARPLAN SEP ';
		$SQL .= ' LEFT OUTER JOIN SEMINARRAEUME SER ON SER_KEY = SEP_SER_KEY';
		$SQL .= ' LEFT OUTER JOIN SEMINARREGIONEN SRG ON SRG_KEY = SEP_SRG_KEY';	
		$SQL .= ' LEFT OUTER JOIN SEMINARTEILNEHMER SETT ON SEP_KEY = SET_SEP_KEY';	
		$SQL .= ' WHERE SEP_STATUS <> 3 AND SET_STATUSTEILNEHMER <> 3 AND SEP_SCHULUNGSART = 1';
				
		if($Bedingung!='')
		{			
			$SQL .= ' AND ' . substr($Bedingung,4);
		}	
		$SQL .= ' AND UPPER(SET_STATUS) != UPPER(\'Abgelehnt\')';
		
		$SQL .= ' ORDER BY '.$ORDERBY;
		$Form->DebugAusgabe(1,$SQL);				
			
	
		//************************************************
		// Aktuellen Datenblock festlegen
		//************************************************
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		}
		elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
		{
			$Block = intval($Param['BLOCK']);
		}
		
		//************************************************
		// Zeilen begrenzen
		//************************************************
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
			
		//*****************************************************************
		// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
		//*****************************************************************
		if($AWIS_KEY1<=0)
		{
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
		}
		
		$rsSET = $DB->RecordSetOeffnen($SQL);
		
		//*****************************************************************
		// Aktuelle Parameter sichern
		//*****************************************************************	
		$Param['ORDERBY']=$ORDERBY;
		$Param['KEY']=$AWIS_KEY1;
		$Param['BLOCK']=$Block;
		$AWISBenutzer->ParameterSchreiben("SETUebersichtSuche",serialize($Param));
				
		$Form->SchreibeHTMLCode('<form name=frmSeminarplanUebersicht action=./seminarplan_Main.php?cmdAktion=Uebersicht'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
		
		//********************************************************
		// Daten anzeigen
		//********************************************************	
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Filiale'].':',200);
		$Form->Erstelle_TextFeld('*SET_FIL_NR',(isset($Param['SET_FIL_NR'])?$Param['SET_FIL_NR']:''),20,200,false);		
		$Form->ZeileEnde();		
		$Form->Trennzeile();
		$Form->Formular_Ende();		
		
		if($rsSET->EOF())
		{
			echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
		}		
		elseif(($rsSET->AnzahlDatensaetze()>0) or (isset($_GET['Liste'])))						// Liste anzeigen
		{
			$Form->Formular_Start();
		
			$Form->ZeileStart();			
			
			$Link = './seminarplan_Main.php?cmdAktion=Uebersicht'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=SET_NACHNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SET_NACHNAME'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_NACHNAME'],150,'',$Link);
			$Link = './seminarplan_Main.php?cmdAktion=Uebersicht'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=SET_VORNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SET_VORNAME'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SET']['SET_VORNAME'],150,'',$Link);
			$Link = './seminarplan_Main.php?cmdAktion=Uebersicht'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=SEP_VON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_VON'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_VON'],150,'',$Link);
			$Link = './seminarplan_Main.php?cmdAktion=Uebersicht'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=SEP_BIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_BIS'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_BIS'],150,'',$Link);
			$Link = './seminarplan_Main.php?cmdAktion=Uebersicht'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');			
			$Link .= '&Sort=SEP_SEMINAR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SEP_SEMINAR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SEP']['SEP_SEMINAR'],150,'',$Link);
			
			$Form->ZeileEnde();
			
			$SETZeile=0;
			
			while(!$rsSET->EOF())
			{
				$Form->ZeileStart();
				$Form->Erstelle_ListenFeld('SET_NACHNAME',$rsSET->FeldInhalt('SET_NACHNAME'),0,150,false,($SETZeile%2),'','','T');
				$Form->Erstelle_ListenFeld('SET_VORNAME',$rsSET->FeldInhalt('SET_VORNAME'),0,150,false,($SETZeile%2),'','','T');								
				$Form->Erstelle_ListenFeld('SEP_VON',$rsSET->FeldInhalt('SEP_VON'),0,150,false,($SETZeile%2),'','','D');
				$Form->Erstelle_ListenFeld('SEP_BIS',$rsSET->FeldInhalt('SEP_BIS'),0,150,false,($SETZeile%2),'','','D');
				$Link = './seminarplan_Main.php?cmdAktion=Details&SEP_KEY='.$rsSET->FeldInhalt('SEP_KEY').'';				
				$Form->Erstelle_ListenFeld('SEP_SEMINAR',$rsSET->FeldInhalt('SEP_SEMINAR'),0,150,false,($SETZeile%2),'',$Link);
				$Form->ZeileEnde();
				
				$rsSET->DSWeiter();
				$SETZeile++;
			}
		
			$Link = './seminarplan_Main.php?cmdAktion=Uebersicht&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
			$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		
			$Form->Formular_Ende();									
		}
		
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Link='./seminarplan_Main.php?cmdAktion=Uebersicht';
		$Form->SchaltflaechenStart();

		if (!isset($FIL_ID))
		{
			$Form->Schaltflaeche('href','cmdZurueck',$Link,'/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');			
		}
		else 
		{
			$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		}
		
		$Form->SchaltflaechenEnde();		
		
		$Form->SchreibeHTMLCode('</form>');
		
	}
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
			
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;	
	global $DB;

	$Bedingung = '';
	
	if(isset($Param['SET_FIL_NR']) AND $Param['SET_FIL_NR']!='')
	{				
		$Bedingung .= 'AND SET_FIL_NR = ' . $DB->FeldInhaltFormat('T', $Param['SET_FIL_NR']) . ' ';			
	}		
	
	return $Bedingung;
}

?>
