<?php
global $SEP;
require_once 'seminarplan_Emails_funktionen.inc';
try {

    $EditierbareBereiche = array ('SET_EINLADUNG','SET_HOTEL','SET_ABSAGE','SET_TEILNEHMER','SEK_KALENDER');

    $SEP = new seminarplan_Emails_funktionen();

    $SQL = 'SELECT * FROM MAILVERSANDTEXTE ';
    $SQL .= " WHERE MVT_GUELTIGAB < sysdate";
    $SQL .= " AND (";
    $Or = false;
    foreach ($EditierbareBereiche as $Bereich){
        if(!$Or){
            $Or = true;
        }else{
            $SQL .= ' OR ';
        }
        $SQL .= ' MVT_BEREICH LIKE ' .$SEP->DB->WertSetzen('MVT','T',$Bereich.'%');

    }
    $SQL .= " )";


    if (isset($_GET['MVT_KEY'])) {
        $SQL .= ' AND MVT_KEY = ' . $SEP->DB->WertSetzen('MVT', 'T', $_GET['MVT_KEY']);
        $SEP->AWIS_KEY1 = $_GET['MVT_KEY'];
    }

    $SQL .= " ORDER BY MVT_BEREICH";

    $rsMVT = $SEP->DB->RecordSetOeffnen($SQL, $SEP->DB->Bindevariablen('MVT'));

    $SEP->Form->DebugAusgabe(1, $SEP->DB->LetzterSQL());

    //Muss eine Testemail versendet werden?
    if (isset($_POST['cmdSenden_x'])) {
        require_once 'awisSeminarplanEinladungsemail.php';
        $Test = new awisSeminarplanEinladungsemail(221898);
        $Test->LoescheAdressListe();
        $Test->Adressliste($_POST['txtEmpfaenger'], 'TO');
        $Test->Text($_POST['txtMVT_TEXT']);
        $Test->Betreff($_POST['txtMVT_BETREFF']);
        $Test->Senden();
    }

    if ($rsMVT->AnzahlDatensaetze() > 0 and $SEP->AWIS_KEY1 == '') {//Liste

        $Feldbreiten = array();
        $Feldbreiten['Icons'] = 20;
        $Feldbreiten['MVT_BEREICH'] = 200;
        $Feldbreiten['MVT_BETREFF'] = 800;

        $SEP->Form->ZeileStart();

        if (($SEP->Recht4207 & 4) == 4) {
            array();
            $Icons[] = array('new', './seminarplan_Main.php?cmdAktion=Emails&Seite=Texte&MVT_KEY=-1');
            $SEP->Form->Erstelle_ListeIcons($Icons, $Feldbreiten['Icons'], -2, $SEP->AWISSprachKonserven['Wort']['ttt_PositionHinzufuegen']);
        } else {
            $SEP->Form->Erstelle_Liste_Ueberschrift('', $Feldbreiten['Icons']);
        }

        $SEP->Form->Erstelle_Liste_Ueberschrift($SEP->AWISSprachKonserven['MVT']['MVT_BEREICH'], $Feldbreiten['MVT_BEREICH']);
        $SEP->Form->Erstelle_Liste_Ueberschrift($SEP->AWISSprachKonserven['MVT']['MVT_BETREFF'], $Feldbreiten['MVT_BETREFF']);
        $SEP->Form->ZeileEnde();

        $HG = 0;
        while (!$rsMVT->EOF()) {
            $HG = ($rsMVT->DSNummer() % 2);
            $SEP->Form->ZeileStart();

            if ($SEP->EditRecht) {
                $Icons = array();
                $Icons[] = array('edit', './seminarplan_Main.php?cmdAktion=Emails&Seite=Texte&MVT_KEY=' . $rsMVT->FeldInhalt('MVT_KEY'));
                $SEP->Form->Erstelle_ListeIcons($Icons, $Feldbreiten['Icons'], $HG, $SEP->AWISSprachKonserven['Wort']['ttt_PositionHinzufuegen']);
            } else {
                $SEP->Form->Erstelle_ListenFeld('', '', $Feldbreiten['Icons'] / 10, $Feldbreiten['Icons'], false, $HG);
            }

            $SEP->Form->Erstelle_ListenFeld('', $rsMVT->FeldInhalt('MVT_BEREICH'), $Feldbreiten['MVT_BEREICH'] / 10, $Feldbreiten['MVT_BEREICH'], false, $HG);
            $SEP->Form->Erstelle_ListenFeld('', $rsMVT->FeldInhalt('MVT_BETREFF'), $Feldbreiten['MVT_BETREFF'] / 10, $Feldbreiten['MVT_BETREFF'], false, $HG);

            $SEP->Form->ZeileEnde();
            $rsMVT->DSWeiter();
        }
    } else {

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./seminarplan_Main.php?cmdAktion=Emails&Seite=Texte&Liste=True accesskey=T title='" . $SEP->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMVT->FeldInhalt('MVT_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMVT->FeldInhalt('MVT_USERDAT'));
        $SEP->Form->InfoZeile($Felder, '');

        $SEP->Form->ZeileStart();
        $SEP->Form->Hinweistext($SEP->AWISSprachKonserven['SEP']['SEP_MAILERSETZUNG'], awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);
        $SEP->Form->ZeileEnde();

        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['MVT']['MVT_BEREICH'], 200);

        if ($rsMVT->FeldInhalt('MVT_KEY') > 0) { //Bestehender DS
            $SEP->Form->Erstelle_HiddenFeld('MVT_BEREICH', $rsMVT->FeldOderPOST('MVT_BEREICH'));
            $SEP->Form->Erstelle_HiddenFeld('MVT_BEREICH_PRAEFIX', '');
            $SEP->Form->Erstelle_TextLabel($rsMVT->FeldOderPOST('MVT_BEREICH'), 500);
        } else {//Neuer DS
            $Zusatzoptionen = array();
            foreach($EditierbareBereiche as $Bereich){
                $Zusatzoptionen[] = $Bereich.'_~'.$Bereich.'_';
            }
            $SEP->Form->Erstelle_SelectFeld('!MVT_BEREICH_PRAEFIX','','200:200',true,'','','','','',$Zusatzoptionen);
            $SEP->Form->Erstelle_TextFeld('!MVT_BEREICH', str_replace('SET_EINLADUNG_', '', $rsMVT->FeldOderPOST('MVT_BEREICH')), 100, 560, ($rsMVT->FeldOderPOST('MVT_KEY') != '' ? false : $SEP->EditRecht));
            $SEP->Form->Erstelle_Bild('default', 'HilfeISTZustand', '', '/bilder/info.png', 'Keine Leerzeichen, keine Sonderzeichen au�er Unterstriche. <br>Beispiel: IT_EINLADUNGEN', 'padding-top: 5px;', 20, 20);
        }
        $SEP->Form->ZeileEnde();

        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['MVT']['MVT_BETREFF'], 200);
        $SEP->Form->Erstelle_TextFeld('!MVT_BETREFF', $rsMVT->FeldOderPOST('MVT_BETREFF'), 100, 800, $SEP->EditRecht);
        $SEP->Form->ZeileEnde();


        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['MVT']['MVT_TEXT'], 200);
        $SEP->Form->Erstelle_Textarea('!MVT_TEXT', $rsMVT->FeldOderPOST('MVT_TEXT'), 800, 90, 10, $SEP->EditRecht);
        $SEP->Form->ZeileEnde();

    }

    ob_start();
    $SEP->Form->ZeileStart();
    $SEP->Form->Erstelle_TextLabel('An (Emailadresse): ', 150);
    $SEP->Form->Erstelle_TextFeld('Empfaenger', $SEP->AWISBenutzer->EMailAdresse(), 20, 200, true);
    $SEP->Form->ZeileEnde();
    $Inhalt = ob_get_clean();
    $Schaltflaechen = array(array('/bilder/cmd_weiter.png', 'cmdSenden', 'post', 'Senden'), array('/bilder/cmd_ds.png', 'close', 'close', 'Schlie�en'));
    $SEP->Form->PopupDialog('Testemail versenden', $Inhalt, $Schaltflaechen, awisFormular::POPUP_INFO, '1');

    $SEP->Form->Formular_Ende();
    $SEP->Form->SchaltflaechenStart();
    $SEP->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $SEP->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    if ($SEP->AWIS_KEY1 != '') {
        $SEP->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $SEP->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');

        $SEP->Form->Schaltflaeche('script', 'cmdTestversand', $SEP->Form->PopupOeffnen(1), '/bilder/cmd_mail.png', '', 'S');
    }
    $SEP->Form->SchaltflaechenEnde();


} catch (awisException $ex) {
    if ($SEP->Form instanceof awisFormular) {
        $SEP->Form->DebugAusgabe(1, $ex->getSQL());
        $SEP->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}

?>