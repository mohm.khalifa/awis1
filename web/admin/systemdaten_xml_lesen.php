<?php
require_once("config.inc.php");		// DB-Befehle
include ("ATU_Header.php");	// Kopfzeile
require_once("xml_funktionen.inc.php");		// DB-Befehle

global $awis_webserver;
global $awis_testwebserver;
global $awis_webtest_user;
global $awis_webtest_pwd;
global $min_time_diff;

//foreach ($awis_webserver as $webserver) {
//	$testurl = "http://" . $awis_webtest_user . ":" . $awis_webtest_pwd . "@" . $webserver . "/admin/systemdaten_xml.php";
	$testurl = "http://" . $awis_webtest_user . ":" . $awis_webtest_pwd . "@" . $awis_testwebserver[0] . "/admin/systemdaten_xml.php";
	$fp = @file($testurl);
	$meineZeit = strftime("%Y.%m.%d %X");
	$meineZeit_ts = time();

	$bla = xml_to_array($fp);
	$daten = $bla["DATEN"];

	foreach ($daten["MOUNTPUNKT"] as $mp){
		$stelle=strpos($mp, ":");
		$RemoteRechner = substr($mp, 0, $stelle);
		if ($RemoteRechner != $daten["ORACLESERVERNAME"]) {
		    echo "<font size=+1 color=#FF0000>" . $RemoteRechner . "</font>" . substr($mp, $stelle);
			echo "<br>\n";
		}
		else
		{
		    echo "<font size=+1 color=#00AA00>" . $RemoteRechner . "</font>" . substr($mp, $stelle);
			echo "<br>\n";
		}
	}
	
	if ($daten["ENDZEIT_TS"] == $meineZeit_ts) {
	    echo "Die Zeit auf $webserver stimmt<br>";
	}
	else
	{
		if (abs($daten["ENDZEIT_TS"] - $meineZeit_ts) <= $min_time_diff) {
		    echo "Die Zeit auf $webserver stimmt fast<br>Seine Zeit: ";
			echo $daten["ENDZEIT"];
			echo "<br>Meine Zeit: $meineZeit<br>";
		}
		else
		{
		    echo "Die Zeit auf $webserver stimmt nicht<br>Seine Zeit: ";
			echo $daten["ENDZEIT"];
			echo "<br>Meine Zeit: $meineZeit<br>";
		}
	}
	echo "<hr>";
	flush();
//}
?>
