<?php
global $MVT;
require_once 'mailtexte_funktionen.inc';

try {

    //********************************************************
    // Parameter ?
     //********************************************************
    if (isset($_GET['MVT_KEY'])) {
        $MVT->AWIS_KEY1 = $_GET['MVT_KEY'];
    } elseif (isset($_POST['txtMVT_KEY'])) {
        $MVT->AWIS_KEY1 = $_POST['txtMVT_KEY'];
    } else {
        $MVT->AWIS_KEY1 = '';
    }

    if (isset($_POST['cmdSuche_x'])) {
        $Param = array();
        $Param['MVT_BEREICH'] = $_POST['txtMVT_BEREICH'];

        $Param['MVT_BEREICH'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern']) ? 'on' : '';
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include('./mailtexte_speichern.php');
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $MVT->AWIS_KEY1 = -1;
    } else {        // Nicht �ber die Suche gekommen, letzten Key abfragen
        if (!$MVT->AWIS_KEY1 > 0) {
            if (!isset($Param['MVT_BEREICH'])) {
                $Param['MVT_BEREICH'] = '';
                $Param['WHERE'] = '';
                $Param['ORDER'] = '';
            }

            if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
                $Param['MVT_BEREICH'] = 0;
            }
            $MVT->AWIS_KEY1 = $Param['MVT_BEREICH'];
        }
    }

    //*********************************************************
    //* Sortierung
    //*********************************************************

    if (!isset($_GET['Sort'])) {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '') {
            $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
        } else {
            $ORDERBY = ' ORDER BY MVT_BEREICH asc';
            $Param['ORDER'] = 'MVT_BEREICH asc';
        }
    } else {
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
        $ORDERBY = ' ORDER BY ' . $Param['ORDER'];
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $BindeVariablen = array();
    $Bedingung = $MVT->BedingungErstellen();

    $SQL = 'SELECT mailversandtexte.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM mailversandtexte';
    $SQL .= ' where nvl( ';
    $SQL .= '    ( select count(*) from v_AccountRechte where MVT_XRC_ID = XRC_ID AND BITAND(XBA_STUFE,MVT_RECHTESTUFE_ANZEIGEN) = MVT_RECHTESTUFE_ANZEIGEN AND XBN_KEY = ' . $MVT->DB->WertSetzen('MVT', 'N0', $MVT->AWISBenutzer->BenutzerID());
    $SQL .= ' ),0) >0';

    $SQL .= $Bedingung;

    $MVT->Form->DebugAusgabe(1, $SQL);

    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($MVT->AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $MVT->Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $MVT->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $MVT->DB->ErmittleZeilenAnzahl($SQL, $MVT->DB->Bindevariablen('MVT', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $MVT->DB->WertSetzen('MVT', 'N0', $StartZeile) . ' AND  ZeilenNr<' . $MVT->DB->WertSetzen('MVT', 'N0',
                ($StartZeile + $ZeilenProSeite));
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $rsMVT = $MVT->DB->RecordsetOeffnen($SQL, $MVT->DB->Bindevariablen('MVT', true));

    $MVT->Form->DebugAusgabe(1, $MVT->DB->LetzterSQL(), $Param, $_GET, $_POST, $MVT->AWIS_KEY1);

    //********************************************************
    // Daten anzeigen
    //********************************************************
    $MVT->Form->SchreibeHTMLCode('<form name=frmMailtexte  method=POST>');

    $DetailAnsicht = false;
    $MVT->Form->Formular_Start();
    if ($rsMVT->EOF() AND !isset($_POST['cmdDSNeu_x'])) {       // Keine Meldung bei neuen Datens�tzen!
        $MVT->Form->Hinweistext($MVT->AWISSprachKonserven['Fehler']['err_keineDaten'], awisFormular::HINWEISTEXT_FEHLER);
    } elseif ($rsMVT->AnzahlDatensaetze() > 1 AND !isset($_GET['MVT_BEREICH'])) {                // Liste anzeigen
        $SpaltenBreite = array();
        $SpaltenBreite['MVT_BEREICH'] = 500;

        $MVT->Form->ZeileStart();
        $Link = './mailtexte_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=MVT_BEREICH' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MVT_BEREICH')) ? '~' : '');
        $MVT->Form->Erstelle_Liste_Ueberschrift($MVT->AWISSprachKonserven['MVT']['MVT_BEREICH'], $SpaltenBreite['MVT_BEREICH'], '', $Link);
        $MVT->Form->ZeileEnde();

        $DS = 0;
        while (!$rsMVT->EOF()) {
            $MVT->Form->ZeileStart();
            $Link = './mailtexte_Main.php?cmdAktion=Details&MVT_KEY=' . $rsMVT->FeldInhalt('MVT_KEY') . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
            $MVT->Form->Erstelle_ListenFeld('MVT_BEREICH', $rsMVT->FeldInhalt('MVT_BEREICH'), 0, $SpaltenBreite['MVT_BEREICH'], false, ($DS % 2), '', $Link, 'T');
            $MVT->Form->ZeileEnde();

            $rsMVT->DSWeiter();
            $DS++;
        }

        $Link = './mailtexte_Main.php?cmdAktion=Details';
        $MVT->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    } else {//Ein DS
        $DetailAnsicht = true;
        $MVT->AWIS_KEY1 = $rsMVT->FeldInhalt('MVT_KEY');
        $Param['MVT_KEY'] = $MVT->AWIS_KEY1;

        $MVT->Form->Erstelle_HiddenFeld('MVT_KEY', $MVT->AWIS_KEY1);

        //Jeder Mailtext bringt sene eigene Rechtestruktur mit
        $RechtText = $MVT->AWISBenutzer->HatDasRecht($rsMVT->FeldInhalt('MVT_XRC_ID'));
        $EditRecht = (($RechtText & $rsMVT->FeldInhalt('MVT_RECHTESTUFE_BEARBEITEN')) == $rsMVT->FeldInhalt('MVT_RECHTESTUFE_BEARBEITEN'));

        //Beim Hinzuf�gen eines Mailtexts darf man alle Felder bearbeiten
        $HinzfuegenRecht = (($MVT->Recht640&4)==4);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./mailtexte_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $MVT->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMVT->FeldInhalt('MVT_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMVT->FeldInhalt('MVT_USERDAT'));
        $MVT->Form->InfoZeile($Felder, '');

        $MVT->Form->ZeileStart();
        $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_BEREICH'] . ':', 180);
        $MVT->Form->Erstelle_TextFeld('!MVT_BEREICH', $rsMVT->FeldInhalt('MVT_BEREICH'), 40, 290, $HinzfuegenRecht, '', '', '', 'T');
        $MVT->Form->Erstelle_Bild('default', 'HilfeISTZustand', '', '/bilder/info.png', 'Keine Leerzeichen, keine Sonderzeichen au�er Unterstriche. <br>Beispiel: IT_EINLADUNGEN', 'padding-top: 2px;', 20, 20);
        $MVT->Form->ZeileEnde();

        $MVT->Form->ZeileStart();
        $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_GRUPPE'] . ':', 180);
        $MVT->Form->Erstelle_TextFeld('!MVT_GRUPPE', $rsMVT->FeldInhalt('MVT_GRUPPE'), 40, 290, $HinzfuegenRecht, '', '', '', 'T');
        $MVT->Form->ZeileEnde();

        $MVT->Form->ZeileStart();
        $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_SPRACHE'] . ':', 180);
        $SQL = 'select LAN_CODE, LAN_CODE from LAENDER where LAN_ATU_STAAT_NR is not null order by LAN_LAND asc ';
        $MVT->Form->Erstelle_SelectFeld('MVT_SPRACHE', $rsMVT->FeldOderPOST('MVT_SPRACHE'), '290:290', $HinzfuegenRecht, $SQL, '', 4);
        $MVT->Form->ZeileEnde();

        $MVT->Form->ZeileStart();
        $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_GUELTIGAB'] . ':', 180);
        $MVT->Form->Erstelle_TextFeld('!MVT_GUELTIGAB', $rsMVT->FeldInhalt('MVT_GUELTIGAB'), 40, 320, $HinzfuegenRecht, '', '', '', 'D');
        $MVT->Form->ZeileEnde();


        if($MVT->AWIS_KEY1=='-1'){
            $MVT->Form->ZeileStart();
            $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_XRC_ID'] . ':',180);
            $SQLRechte = 'Select XRC_ID, XRC_ID || \' - \' || XRC_RECHT from Rechte order by XRC_RECHT asc';
            $MVT->Form->Erstelle_SelectFeld('~!*XRC_ID',$rsMVT->FeldInhalt('MVT_XRC_ID'), 700, $HinzfuegenRecht,$SQLRechte, '~' . $MVT->AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '','','','','',array());
            $MVT->Form->ZeileEnde();

            $filter='***XRS_Daten;sucXRC_ID';

            $AnzBit = log($rsMVT->FeldInhalt('MVT_RECHTESTUFE_ANZEIGEN'),2);
            $MVT->Form->ZeileStart();
            $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_RECHTESTUFE_ANZEIGEN'] . ':', 160);
            $MVT->Form->Erstelle_SelectFeld('!*XRS_BIT_ANZ',$AnzBit, 400, $HinzfuegenRecht,$filter,'~' . $MVT->AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array());
            $MVT->Form->ZeileEnde();

            $BearbBit = log($rsMVT->FeldInhalt('MVT_RECHTESTUFE_BEARBEITEN'),2);
            $MVT->Form->ZeileStart();
            $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_RECHTESTUFE_BEARBEITEN'] . ':', 160);
            $MVT->Form->Erstelle_SelectFeld('!*XRS_BIT_BEARB',$BearbBit, 400, $HinzfuegenRecht,$filter,'~'.$MVT->AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array());
            $MVT->Form->ZeileEnde();
        }


        $MVT->Form->Trennzeile('O');
        $MVT->Form->Trennzeile('T');
        $MVT->Form->Trennzeile('O');

        $MVT->Form->ZeileStart();
        $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_BETREFF'] . ':', 180);
        $MVT->Form->Erstelle_TextFeld('!MVT_BETREFF', $rsMVT->FeldInhalt('MVT_BETREFF'), 87, 900, $EditRecht, '', '', '', 'T');
        $MVT->Form->ZeileEnde();


        $MVT->Form->ZeileStart();
        $TinyMCE = $MVT->Form->TinyMCE('MVT_TEXT', $rsMVT->FeldInhalt('MVT_TEXT'), 800, 10, $EditRecht);
        $TinyMCE->Menubar()->MitStandardfunktionen();
        $TinyMCE->Menubar()->MitMailPlatzhalter($rsMVT->FeldInhalt('MVT_GRUPPE'));
        $MVT->Form->ZeileEnde();

    }

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $MVT->Form->Formular_Ende();
    $MVT->Form->SchaltflaechenStart();

    $MVT->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $MVT->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if (($MVT->Recht640 & 2) == 2 AND $DetailAnsicht) {
        $MVT->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $MVT->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    if (($MVT->Recht640 & 4) == 4 AND !$DetailAnsicht) {
        $MVT->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $MVT->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $MVT->Form->SchaltflaechenEnde();

    $MVT->Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($MVT->Form instanceof awisFormular) {
        $MVT->Form->DebugAusgabe(1, $ex->getSQL());
        $MVT->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $MVT->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($MVT->Form instanceof awisFormular) {
        $MVT->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>