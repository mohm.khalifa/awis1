<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>
</head>

<body>
<?
global $HTTP_POST_VARS;
global $HTTP_GET_VARS;
global $awisDBFehler;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,504);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Nachgefragte Artikel',$_SERVER['PHP_AUTH_USER'],'','','');
    die("Keine ausreichenden Rechte!");
}

if(!isset($HTTP_POST_VARS['txtDatum']))
{
	echo '<Form name=frmNART method=post action=./NachgefragteArtikel.php>';
	echo '<table border=0 width=100%>';

	echo '<tr><td id=FeldBez colspan=2>Parameter</td></tr>'	;

	echo "<tr><td id=RegisterInhalt width=180>D<u>a</u>tum:</td>";
	echo "<td id=RegisterInhalt><input type=text name=txtDatum accesskey='a' size=10 tabindex=10>";
	echo '</td></tr>';

	echo "<tr><td id=RegisterInhalt width=180>A<u>T</u>U-Nr:</td>";
	echo "<td id=RegisterInhalt><input type=text name=txtATUNR accesskey='T' size=10 tabindex=10>";
	echo '</td></tr>';

	if($_SERVER['PHP_AUTH_USER']!='entwick1')
	{
		echo "<tr><td id=RegisterInhalt width=180>Warengruppe:</td>";
		echo "<td id=RegisterInhalt><input type=text name=txtWGR size=3 tabindex=10>";
		echo '</td></tr>';
	}
	else
	{
		echo "<tr><td id=RegisterInhalt width=180>Warengruppe:</td>";
		echo "<td id=RegisterInhalt><select name=txtWGR size=1 tabindex=10>";
	
		$rsWGR=awisOpenRecordset($con,"SELECT * FROM WARENGRUPPEN ORDER BY WGR_BEZEICHNUNG");
		$rsWGRZeilen = $awisRSZeilen;
		
		for($i=0;$i<$rsWGRZeilen;$i++)
		{
			print "<option " . ($i==0?"selected":"") . " value=" . $rsWGR["WGR_ID"][$i] . ">" . $rsWGR["WGR_BEZEICHNUNG"][$i] . "</option>";
		}
		
		print "</select></td></tr>";
		unset($rsWGR);		// Speicher freigeben
	}
		
	echo "<tr><td id=RegisterInhalt width=180>Unterwarengruppe:</td>";
	echo "<td id=RegisterInhalt><input type=text name=txtWUG size=10 tabindex=10>";
	echo '</td></tr>';

	echo "<tr><td id=RegisterInhalt width=180>Filiale:</td>";
	echo "<td id=RegisterInhalt><input type=text name=txtFIL_ID size=10 tabindex=10>";
	echo '</td></tr>';
	
	echo "<tr><td id=RegisterInhalt width=180>Begr�ndung:</td>";
	echo "<td id=RegisterInhalt><input type=text name=txtGrund size=30 tabindex=10>";
	echo '</td></tr>';

	echo '</table>';
	
	echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
	echo '</Form>';
}
else
{
	$Bedingung='';
	
    $SQL = "SELECT *";
	$SQL .= " from ARTIKELNACHFRAGEN, ARTIKELNACHFRAGENGRUND, ARTIKELSTAMM, WARENUNTERGRUPPEN";
	$SQL .= " WHERE ANA_ANG_ID = ANG_ID AND ANA_AST_ATUNR = AST_ATUNR(+) AND AST_WUG_KEY = WUG_KEY";	
	$SQL .= " " . $Bedingung . "";
	$SQL .= " ORDER BY ANA_FIL_ID, ANA_DATUM DESC";
    
	$rsNART = awisOpenRecordset($con, $SQL);
    $rsNARTZeilen = $awisRSZeilen;

	echo '<table border=1 width=100%>';
	echo '<tr>';
	echo '<td id=FeldBez>Filiale</td>';
	echo '<td id=FeldBez>ATU-NR</td>';
	echo '<td id=FeldBez>Artikel</td>';
	echo '<td id=FeldBez>Anzahl</td>';
	echo '<td id=FeldBez>WGr</td>';
	echo '<td id=FeldBez>WUGr</td>';
	echo '<td id=FeldBez>KBA-Nr</td>';
	echo '<td id=FeldBez>Bauj.</td>';
	echo '<td id=FeldBez>Menge</td>';
	echo '<td id=FeldBez>K</td>';
	echo '<td id=FeldBez>Datum</td>';
	
	
	echo '</tr>';

	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );
	fputs($fd, "Filiale;ATU-NR;Artikel;Anzahl;WGr;WUGr;KBA-Nr;Baujahr;Menge;K;Datum\n");

	for($i=0;$i<$rsNARTZeilen;$i++)
	{
		$Zeile='';
		echo '<tr>';
		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['ANA_FIL_ID'][$i] . '</td>';
		$Zeile .= $rsNART['ANA_FIL_ID'][$i];

		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['ANA_AST_ATUNR'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['ANA_AST_ATUNR'][$i];

		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['AST_BEZEICHNUNGWW'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['AST_BEZEICHNUNGWW'][$i];

				// Gesamtmenge
		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . 0 . '</td>';
		$Zeile .= ';' . 0;

		
		
		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['ANA_WGR_ID'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['ANA_WGR_ID'][$i];

		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['WUG_ID'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['WUG_ID'][$i];
		
		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['ANA_KBANR'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['ANA_KBANR'][$i];

		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . substr($rsNART['ANA_BAUJAHR'][$i],0,2).'/'. substr($rsNART['ANA_BAUJAHR'][$i],2,2) . '</td>';
		$Zeile .= ';' . substr($rsNART['ANA_BAUJAHR'][$i],0,2).'/'. substr($rsNART['ANA_BAUJAHR'][$i],2,2);

		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['ANA_MENGE'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['ANA_MENGE'][$i];

		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['ANA_ANG_ID'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['ANA_ANG_ID'][$i];
		
		echo '<td ' . (($i % 2) == 0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') . ' >' . $rsNART['ANA_DATUM'][$i] . '</td>';
		$Zeile .= ';' . $rsNART['ANA_DATUM'][$i];
		
		
		$Zeile .= "\n";
		fputs($fd, $Zeile);

	}
	echo '</table>';

	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
}
	
	
print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . $HTTP_GET_VARS['ZurueckLink'] . "';>";

print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

// JAVA Skript zum Cursor positionieren

	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('txtDatum')[0].focus();";
	echo "\n</Script>";

?>
</body>
</html>

