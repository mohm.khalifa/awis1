<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
	require_once('awisDatenbank.inc');
	require_once('awisBenutzer.inc');
	require_once('awisFormular.inc');

	global $AWISBenutzer;
	global $AWISCursorPosition;
	global $AWIS_KEY1;
	global $AWIS_KEY2;		// Aus AWISFormular

	try
	{
		$Version = 3;
		$DB = awisDatenbank::NeueVerbindung('AWIS');
		$DB->Oeffnen();
		$AWISBenutzer = awisBenutzer::Init();
		$Form = new awisFormular();
		echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei($Version) .">";
	}
	catch (Exception $ex)
	{
		die($ex->getMessage());
	}

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('TITEL','tit_Gutschein');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	echo '<title>'.$AWISSprachKonserven['TITEL']['tit_Gutschein'].'</title>';
?>
</head>
<body>
<?php
	include ("awisHeader$Version.inc");	// Kopfzeile

	try
	{
		$Form = new awisFormular();

		if($AWISBenutzer->HatDasRecht(28000)==0)
		{
			$Form->Fehler_Anzeigen('Rechte','','MELDEN',-9,"201611150931");
			die();
		}

		$Register = new awisRegister(28000);
		$Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));
		$Form->SetzeCursor($AWISCursorPosition);
	}
	catch (Exception $ex)
	{
		if($Form instanceof awisFormular)
		{
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"1308141349");
		}
		else
		{
			$Form->SchreibeHTMLCode('AWIS: '.$ex->getMessage());
		}
	}
?>
</body>
</html>