<?php
global $AWIS_KEY1;
global $AWISCursorPosition;

ini_set('error_reporting', 'E_ALL');
ini_set('display_errors', 'on');

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LIN', '%');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();

    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht615 = $AWISBenutzer->HatDasRecht(615);            // Recht f�r die Notizen
    if ($Recht615 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
    if ($BildschirmBreite < 1024) {
        $FeldBreiten['NOTIZ'] = 500;
        $FeldBreiten['UEBERSCHRIFT'] = 686;
        $FeldBreiten['USER'] = 90;
    } elseif ($BildschirmBreite < 1280) {
        $FeldBreiten['NOTIZ'] = 700;
        $FeldBreiten['USER'] = 100;
        $FeldBreiten['UEBERSCHRIFT'] = 896;
    } else {
        $FeldBreiten['NOTIZ'] = 920;
        $FeldBreiten['UEBERSCHRIFT'] = 1160;
        $FeldBreiten['USER'] = 150;
    }

    $SQL = "SELECT *";
    $SQL .= ' FROM Informationstypen';
    $SQL .= ' WHERE ITY_KEY = 303';
    $SQL .= ' AND ITY_Status = \'A\'';
    $SQL .= ' ORDER BY ITY_Sortierung';

    $rsITY = $DB->RecordSetOeffnen($SQL);

    $SQL = 'SELECT *';
    $SQL .= ' FROM LieferantenInfos ';
    $SQL .= ' WHERE LIN_ITY_KEY = 303';
    $SQL .= ' AND LIN_LIE_NR = ' . $AWIS_KEY1;

    if (isset($_GET['LINKEY'])) {
        $SQL .= ' AND LIN_KEY = ' . $DB->FeldInhaltFormat('N0', $_GET['LINKEY'], false);
    }

    if (isset($_GET['SortLNo'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SortLNo']);
    } else {
        $SQL .= ' ORDER BY LIN_USERDAT DESC';
    }

    $Form->DebugAusgabe(1, $SQL);

    $rsLIN = $DB->RecordSetOeffnen($SQL);

    // Liste anzeigen
    if ($rsLIN->AnzahlDatensaetze() > 1 or isset($_GET['Liste']) or !isset($_GET['LINKEY']))                    // Liste anzeigen	
    {
        $Form->Formular_Start();
        $Form->ZeileStart();

        $Icons = array();
        if (($Recht615 & 4) != 0) {
            $Icons[] = array('new', './lieferanten_Main.php?cmdAktion=Details&Seite=Notizen&LINKEY=0');
        }
        $Form->Erstelle_ListeIcons($Icons, 18);
        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Notizen';
        $Sort = '&SortLNo=LIN_WERT' . ((isset($_GET['SortLNo']) AND ($_GET['SortLNo'] == 'LIN_WERT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIN']['wrd_Notiz'], $FeldBreiten['NOTIZ'], '', $Link . $Sort);
        $Sort = '&SortLNo=LIN_USER' . ((isset($_GET['SortLNo']) AND ($_GET['SortLNo'] == 'LIN_USER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIN']['LIN_USER'], $FeldBreiten['USER'], '', $Link . $Sort);
        $Sort = '&SortLNo=LIN_USERDAT' . ((isset($_GET['SortLNo']) AND ($_GET['SortLNo'] == 'LIN_USERDAT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIN']['LIN_USERDAT'], 100, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;

        while (!$rsLIN->EOF()) {
            $Form->ZeileStart();
            $Icons = array();

            // Daten �ndern
            if (($Recht615 & 2) != 0) {
                $Icons[] = array('edit', './lieferanten_Main.php?cmdAktion=Details&Seite=Notizen&LINKEY=' . $rsLIN->FeldInhalt('LIN_KEY'));
            }
            // Daten l�schen
            if ((($Recht615 & 8) != 0 AND $rsITY->FeldInhalt('ITY_KEY') == 303)) {
                $Icons[] = array('delete', './lieferanten_Main.php?cmdAktion=Details&Seite=Notizen&Del=' . $rsLIN->FeldInhalt('LIN_KEY'));
            }
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));

            $Form->Erstelle_ListenFeld('*LIN_WERT', $rsLIN->FeldInhalt('LIN_WERT'), 10, $FeldBreiten['NOTIZ'] - 42, false, ($DS % 2), '', '', 'T', 'L',
                $rsLIN->FeldInhalt('LIN_WERT'));
            $Form->Erstelle_ListenFeld('*LIN_USER', $rsLIN->FeldInhalt('LIN_USER'), 10, $FeldBreiten['USER'], false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('*LIN_USERDAT', $rsLIN->FeldInhalt('LIN_USERDAT'), 10, 100, false, ($DS % 2), '', '', 'D');

            $Form->ZeileEnde();
            $DS++;

            $rsLIN->DSWeiter();
        }
    } else        // Einer oder keiner (= Neu)
    {
        $Form->Formular_Start();
        $AWIS_KEY2 = ($rsLIN->FeldInhalt('LIN_KEY') != ''?$rsLIN->FeldInhalt('LIN_KEY'):'0');

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./lieferanten_Main.php?cmdAktion=Details&Seite=Notizen&Liste=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLIN->FeldInhalt('LIN_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLIN->FeldInhalt('LIN_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();

        $Form->DebugAusgabe(1, $AWIS_KEY2);

        $EditModus = false;
        if (($AWIS_KEY2 == 0 and ($Recht615 & 4) != 0) or (($AWIS_KEY2 != 0 and ($Recht615 & 2) != 0))) {
            $EditModus = true;
        }

        $ItyName = 'LIN_WERT' . '_' . $rsITY->FeldInhalt('ITY_KEY') . '_' . $rsLIN->FeldInhalt('LIN_KEY') . '_' . $rsITY->FeldInhalt('ITY_FORMAT');
        $Zeichen = explode(':', $rsITY->FeldInhalt(ITY_ZEICHEN));
        $Spalten = $Zeichen[0];
        $Zeilen = $Zeichen[1];

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LIN']['wrd_Notiz'] . ':', 120);
        $Form->Erstelle_Textarea($ItyName, $rsLIN->FeldInhalt('LIN_WERT'), $rsITY->FeldInhalt('ITY_BREITE'), $Spalten, $Zeilen, $EditModus);
        $AWISCursorPosition = ($EditModus?'txt' . $ItyName:$AWISCursorPosition);
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060008");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812061223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>