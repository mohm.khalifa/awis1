<?php
global $AWIS_KEY1;

$TextKonserven = array();
$TextKonserven[] = array('Wort', 'WirklichLoeschen');
$TextKonserven[] = array('Wort', 'Ja');
$TextKonserven[] = array('Wort', 'Nein');

try {
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Tabelle = '';

    if (!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x'])) {
        if (isset($_POST['txtLIE_NR'])) {
            $Tabelle = 'LIE';
            $Key = $_POST['txtLIE_NR'];

            $SQL = 'SELECT LIE_NR, LIE_BEZEICHNUNG, LIE_KUERZEL';
            $SQL .= ' FROM LIEFERANTEN ';
            $SQL .= ' WHERE LIE_NR=0' . $Key;

            $rsDaten = $DB->RecordsetOeffnen($SQL);

            $LIEKey = $rsDaten->FeldInhalt('LIE_NR');

            $Felder = array();
            $Felder[] = array($Form->LadeTextBaustein('LIE', 'LIE_BEZEICHNUNG'), $rsDaten->FeldInhalt('LIE_BEZEICHNUNG'));
            $Felder[] = array($Form->LadeTextBaustein('LIE', 'LIE_KUERZEL'), $rsDaten->FeldInhalt('LIE_KUERZEL'));
        } elseif (isset($_POST['txtLZG_KEY']))        // Zugriffsgruupe AP
        {
            $Tabelle = 'LZG';
            $Key = $_POST['txtLZG_KEY'];

            $SQL = 'SELECT *';
            $SQL .= ' FROM LIEFERANTENAPZUGRIFFSGRP ';
            $SQL .= ' WHERE LZG_KEY=0' . $Key;

            $rsDaten = $DB->RecordsetOeffnen($SQL);

            $LIEKey = $rsDaten->FeldInhalt('LIE_NR');

            $Felder = array();
            $Felder[] = array($Form->LadeTextBaustein('LZG', 'LZG_BEZEICHNUNG'), $rsDaten->FeldInhalt('LZG_BEZEICHNUNG'));
        }
    } elseif (isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x'])) {
        if (isset($_GET['Unterseite'])) {
            switch ($_GET['Unterseite']) {
                default:
                    break;
            }
        } else {
            switch ($_GET['Seite']) {
                case 'Notizen':
                    $Tabelle = 'LIN';
                    $Key = $_GET['Del'];

                    $SQL = 'SELECT *';
                    $SQL .= ' FROM LieferantenInfos';
                    $SQL .= ' WHERE LIN_KEY=0' . $Key . '';
                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $LIEKey = $rsDaten->FeldInhalt('LIN_LIE_NR');
                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('LIN', 'LIN_USER'), $rsDaten->FeldInhalt('LIN_USER'));
                    $Felder[] = array($Form->LadeTextBaustein('LIN', 'LIN_USERDAT'), $rsDaten->FeldInhalt('LIN_USERDAT'));
                    $Felder[] = array($Form->LadeTextBaustein('LIN', 'wrd_Notiz'), substr($rsDaten->FeldInhalt('LIN_WERT'), 0, 30) . ' ...');
                    break;
                case 'TecDoc':
                    $Tabelle = 'LIN';
                    $Key = $_GET['Del'];

                    $SQL = 'SELECT *';
                    $SQL .= ' FROM LieferantenInfos';
                    $SQL .= ' WHERE LIN_KEY=0' . $Key . '';
                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $LIEKey = $rsDaten->FeldInhalt('LIN_LIE_NR');
                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('LIN', 'LIN_USER'), $rsDaten->FeldInhalt('LIN_USER'));
                    $Felder[] = array($Form->LadeTextBaustein('LIN', 'LIN_USERDAT'), $rsDaten->FeldInhalt('LIN_USERDAT'));
                    $Felder[] = array($Form->LadeTextBaustein('LIN', 'LIN_TecDocNr'), $rsDaten->FeldInhalt('LIN_WERT'));
                    break;
                case 'Ansprechpartner':
                    $Tabelle = 'LAP';
                    $Key = $_GET['Del'];

                    $SQL = 'SELECT LAP_KEY, LAP_NACHNAME, LAP_VORNAME, LAP_LIE_NR';
                    $SQL .= ' FROM LieferantenAP ';
                    $SQL .= ' WHERE LAP_KEY=0' . $Key . '';
                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $LIEKey = $rsDaten->FeldInhalt('LAP_LIE_NR');
                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('LAP', 'LAP_NACHNAME'), $rsDaten->FeldInhalt('LAP_NACHNAME'));
                    $Felder[] = array($Form->LadeTextBaustein('LAP', 'LAP_VORNAME'), $rsDaten->FeldInhalt('LAP_VORNAME'));
                    break;
                case 'Mitarbeiter':
                    $Tabelle = 'LGM';
                    $Key = $_GET['Del'];

                    $SQL = 'SELECT LGM_KEY, XBN_NAME';
                    $SQL .= ' FROM LIEFERANTENAPZUGRIFFSGRPBEN ';
                    $SQL .= ' LEFT OUTER JOIN Benutzer ON lgm_xbn_key = xbn_key';
                    $SQL .= ' WHERE LGM_KEY=0' . $Key . '';
                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $LIEKey = $rsDaten->FeldInhalt('LGM_LZG_KEY');
                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('XBN', 'XBN_NAME'), $rsDaten->FeldInhalt('XBN_NAME'));
                    break;
                case 'Verkaufshaeuser':
                    $Tabelle = 'LVK';
                    $Key = $_GET['Del'];

                    $SQL = 'SELECT LVK_KEY, LVK_LIE_NR, LVK_NAME1, LVK_STRASSE, LVK_ORT';
                    $SQL .= ' FROM LIEFERANTENVERKAUFSHAEUSER ';
                    $SQL .= ' WHERE LVK_KEY=0' . $Key . '';
                    $rsDaten = $DB->RecordsetOeffnen($SQL);

                    $LIEKey = $rsDaten->FeldInhalt('LVK_LIE_NR');
                    $Felder = array();

                    $Felder[] = array($Form->LadeTextBaustein('LVK', 'LVK_NAME1'), $rsDaten->FeldInhalt('LVK_NAME1'));
                    $Felder[] = array($Form->LadeTextBaustein('LVK', 'LVK_STRASSE'), $rsDaten->FeldInhalt('LVK_STRASSE'));
                    $Felder[] = array($Form->LadeTextBaustein('LVK', 'LVK_ORT'), $rsDaten->FeldInhalt('LVK_ORT'));
                    break;
                default:
                    break;
            }
        }
    } elseif (isset($_POST['cmdLoeschenOK']))    // Loeschen durchführen
    {
        $SQL = '';
        switch ($_POST['txtTabelle']) {
            case 'LIN':
                $SQL = 'DELETE FROM LIEFERANTENINFOS WHERE LIN_KEY = 0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtLIEKey'];
                break;
            case 'LIE':
                $SQL = 'DELETE FROM LIEFERANTEN WHERE LIE_NR=0' . $_POST['txtKey'];
                $AWIS_KEY1 = -1;
                break;
            case 'LAP':
                $SQL = 'DELETE FROM LieferantenAP WHERE LAP_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtLIEKey'];
                break;
            case 'LGM':
                $SQL = 'DELETE FROM LIEFERANTENAPZUGRIFFSGRPBEN WHERE LGM_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtLIEKey'];
                break;
            case 'LVK':
                $SQL = 'DELETE FROM LIEFERANTENVERKAUFSHAEUSER WHERE LVK_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = $_POST['txtLIEKey'];
                break;
            case 'LZG':
                $SQL = 'DELETE FROM LIEFERANTENAPZUGRIFFSGRP WHERE LZG_key=0' . $_POST['txtKey'];
                $AWIS_KEY1 = 0;
                break;
            default:
                break;
        }

        if ($SQL != '') {
            if ($DB->Ausfuehren($SQL) === false) {
                throw new awisException('Fehler beim Loeschen', 200910201246, $SQL, awisException::AWIS_ERR_SYSTEM);
            }
        }
    }

    if ($Tabelle != '') {
        $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

        $Form->SchreibeHTMLCode('<form name=frmLoeschen action=./lieferanten_Main.php?cmdAktion=' . $_GET['cmdAktion'] . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

        $Form->Formular_Start();
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
        $Form->ZeileEnde();

        foreach ($Felder AS $Feld) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($Feld[0] . ':', 150);
            $Form->Erstelle_TextFeld('Feld', $Feld[1], 100, 500, false);
            $Form->ZeileEnde();
        }

        $Form->Erstelle_HiddenFeld('LIEKey', $LIEKey);
        $Form->Erstelle_HiddenFeld('Tabelle', $Tabelle);
        $Form->Erstelle_HiddenFeld('Key', $Key);

        $Form->Trennzeile();

        $Form->ZeileStart();
        $Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '');
        $Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '');
        $Form->ZeileEnde();

        $Form->SchreibeHTMLCode('</form>');

        $Form->Formular_Ende();

        die();
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>