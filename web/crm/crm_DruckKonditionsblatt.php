<?php
require_once('db.inc.php');
require_once('register.inc.php');
require_once('awisAusdruck.php');
require_once('awis_forms.inc.php');

global $awisRSZeilen;

ini_set('max_execution_time','90000');

$AWISBenutzer = new awisUser();

$Vorlagen = array('BriefpapierATU_DE_CRM.pdf','BriefpapierATU_Ausland_CRM.pdf');

//$Vorlagen = array();


$DruckArt = 1;		// Standard: 1 Datei pro Kunde
$CADKEY = 0;		// Alle offenen drucken
$Param = array();

if(isset($_GET['Druckart']) AND $_GET['Druckart']==2)
{
	$DruckArt=2;								// Alles in eine Datei drucken
}
elseif(isset($_GET['Druckart']) AND $_GET['Druckart']==3)
{
	$DruckArt=3;								// Alles in eine Datei drucken
}
if(isset($_GET['CADKEY']))
{
	$CADKEY=floatval($_GET['CADKEY']);			// nur f�r einen bestimmten Kunden drucken
}

$con = awisLogon();

// Parameter beim Massendruck lesen
if($DruckArt>=2)			// F�r PDF und CSV!
{
	$Param = awis_BenutzerParameter($con, "Formular_CRM_Massendruck",$AWISBenutzer->BenutzerName());
	$Param['MAIL'] = false;
    
    // Sobald E-Mail-Adresse angegeben wird soll bei PDF-Druck diese als E-Mail versendet werden
    if(isset($Param['EMAIL']) && $Param['EMAIL'] != '' && strpos($Param['EMAIL'], '@') !== false && $DruckArt == 2)
    {
        // bei diesem Key wird die Ausgabe in Bloecke unterteilt und die PDF's als
        // E-Mail versendet. Dieser Key wird auch gesetzt wenn das Konditionsmodell
        // "Sonstige" ausgewaehlt wird.
        $Param['MAIL'] = true;
    }
}

$AWISSprache = awis_BenutzerParameter($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName());

if($DruckArt==3)			// CSV Export
{
	$ORDERBY = " ORDER BY CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_ORT";

	$SQL = "SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_LAN_CODE, CAD_ORT, CAD_KEY, CAD_STATUS
		, CKO_LETZTEAKTION, cap_key, cap_anrede, cap_nachname, cap_vorname, cap_funktion
		, KEYWG, KEYDL, KEYRH, KEYTH
		, REIFENBEZ
		, KON_NAME1, KON_NAME2
		, cap_key1, cap_anrede1, cap_nachname1, cap_vorname1, cap_funktion1
		, cap_key2, cap_anrede2, cap_nachname2, cap_vorname2, cap_funktion2
		, cap_key3, cap_anrede3, cap_nachname3, cap_vorname3, cap_funktion3
		, cap_key4, cap_anrede4, cap_nachname4, cap_vorname4, cap_funktion4
		, cap_key5, cap_anrede5, cap_nachname5, cap_vorname5, cap_funktion5
		, LAND
		, row_number() over(".$ORDERBY.") as zeile
		FROM (
	SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_LAN_CODE, CAD_ORT, CAD_KEY, CAD_STATUS
		, CKO_LETZTEAKTION, cap_key, cap_anrede, cap_nachname, cap_vorname, cap_funktion
		, NVL(KEYWG,0) as KEYWG, NVL(KEYDL,0) as KEYDL, NVL(KEYRH,0) as KEYRH, NVL(KEYTH,0) as KEYTH
		, REIFENBEZ
		, KON_NAME1, KON_NAME2
		, cap_key1, cap_anrede1, cap_nachname1, cap_vorname1, cap_funktion1
		, cap_key2, cap_anrede2, cap_nachname2, cap_vorname2, cap_funktion2
		, cap_key3, cap_anrede3, cap_nachname3, cap_vorname3, cap_funktion3
		, cap_key4, cap_anrede4, cap_nachname4, cap_vorname4, cap_funktion4
		, cap_key5, cap_anrede5, cap_nachname5, cap_vorname5, cap_funktion5
        , POST_STATUS
        , VERTRAGSSTATUS
		, LAND
		FROM (
	SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_LAN_CODE, CAD_ORT, CAD_KEY, CAD_STATUS
		, CKO_LETZTEAKTION, cap_key, cap_anrede, cap_nachname, cap_vorname, cap_funktion
		, (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1) AS KEYWG
		, (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1110 AND ROWNUM=1) AS KEYDL
		, (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1) AS KEYRH
		, (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1121 AND ROWNUM=1) AS KEYTH
		, (select CKM_BEZEICHNUNG from crmkonditionsmodelle where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 2220 AND ROWNUM=2)) AS REIFENBEZ
		, KON_NAME1, KON_NAME2
		, cap_key1, cap_anrede1, cap_nachname1, cap_vorname1, cap_funktion1
		, cap_key2, cap_anrede2, cap_nachname2, cap_vorname2, cap_funktion2
		, cap_key3, cap_anrede3, cap_nachname3, cap_vorname3, cap_funktion3
		, cap_key4, cap_anrede4, cap_nachname4, cap_vorname4, cap_funktion4
		, cap_key5, cap_anrede5, cap_nachname5, cap_vorname5, cap_funktion5
        , (SELECT CIN_WERT  FROM CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 4007 AND ROWNUM = 1) AS POST_STATUS
        , (SELECT CIN_WERT  FROM CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 4005 AND ROWNUM = 1) AS VERTRAGSSTATUS
		, (SELECT CIN_WERT 	FROM CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1013 AND ROWNUM = 1) AS LAND
		 FROM crmAdressen
		INNER JOIN crmAktionen ON cad_key = cko_xxx_key AND cko_cat_key IN (6,37)
		 LEFT OUTER JOIN crmAkquisestand ON cad_key = caq_cad_key
		 LEFT OUTER JOIN Kontakte ON KON_KEY = CAD_KON_KEY
		 LEFT OUTER JOIN crmAnsprechpartner ON CAP_KEY = CAQ_CAP_KEY
		 LEFT OUTER JOIN
		 (
		 SELECT cap_cad_key1, cap_key1, cap_anrede1, cap_nachname1, cap_vorname1, cap_funktion1
		 FROM (
		 SELECT cap_cad_key AS cap_cad_key1, cap_anrede as cap_anrede1, cap_key AS cap_key1, cap_nachname AS cap_nachname1, cap_vorname as cap_vorname1, cap_funktion as cap_funktion1
		 , row_number() OVER (partition by cap_cad_key order by cap_key) AS ZEILE
		 FROM crmAnsprechpartner
		 ) CAP1
		 WHERE Zeile = 1
		 ) AP1 ON cap_cad_key1=cad_key
		 LEFT OUTER JOIN
		 (
		 SELECT cap_cad_key2, cap_key2, cap_anrede2, cap_nachname2, cap_vorname2, cap_funktion2
		 FROM (
		 SELECT cap_cad_key AS cap_cad_key2, cap_anrede as cap_anrede2, cap_key AS cap_key2, cap_nachname AS cap_nachname2, cap_vorname as cap_vorname2, cap_funktion as cap_funktion2
		 , row_number() OVER (partition by cap_cad_key order by cap_key) AS ZEILE
		 FROM crmAnsprechpartner
		 ) CAP2
		 WHERE Zeile = 2
		 ) AP2 ON cap_cad_key2=cad_key
		 LEFT OUTER JOIN
		 (
		 SELECT cap_cad_key3, cap_key3, cap_anrede3, cap_nachname3, cap_vorname3, cap_funktion3
		 FROM (
		 SELECT cap_cad_key AS cap_cad_key3, cap_anrede as cap_anrede3, cap_key AS cap_key3, cap_nachname AS cap_nachname3, cap_vorname as cap_vorname3, cap_funktion as cap_funktion3
		 , row_number() OVER (partition by cap_cad_key order by cap_key) AS ZEILE
		 FROM crmAnsprechpartner
		 ) CAP3
		 WHERE Zeile = 3
		 ) AP3 ON cap_cad_key3=cad_key
		 LEFT OUTER JOIN
		 (
		 SELECT cap_cad_key4, cap_key4, cap_anrede4, cap_nachname4, cap_vorname4, cap_funktion4
		 FROM (
		 SELECT cap_cad_key AS cap_cad_key4, cap_anrede as cap_anrede4, cap_key AS cap_key4, cap_nachname AS cap_nachname4, cap_vorname as cap_vorname4, cap_funktion as cap_funktion4
		 , row_number() OVER (partition by cap_cad_key order by cap_key) AS ZEILE
		 FROM crmAnsprechpartner
		 ) CAP4
		 WHERE Zeile = 4
		 ) AP4 ON cap_cad_key4=cad_key
		 LEFT OUTER JOIN
		 (
		 SELECT cap_cad_key5, cap_key5, cap_anrede5, cap_nachname5, cap_vorname5, cap_funktion5
		 FROM (
		 SELECT cap_cad_key AS cap_cad_key5, cap_anrede as cap_anrede5, cap_key AS cap_key5, cap_nachname AS cap_nachname5, cap_vorname as cap_vorname5, cap_funktion as cap_funktion5
		 , row_number() OVER (partition by cap_cad_key order by cap_key) AS ZEILE
		 FROM crmAnsprechpartner
		 ) CAP5
		 WHERE Zeile = 5
		 ) AP5 ON cap_cad_key5=cad_key
		 ))
		 LEFT OUTER JOIN V_CRM_KONDITIONEN ON CIN_WG = KEYWG AND CIN_DL = KEYDL AND CIN_RH = KEYRH
		 LEFT OUTER JOIN mailversand
		 ON cad_key = mvs_xxx_key
		 AND mvs_xtn_kuerzel = 'CAD'
		 AND mvs_eingestellt > sysdate - 150
		 WHERE  (CKO_LETZTEAKTION IS NULL OR CKO_LETZTEAKTION < SYSDATE)";

	//Wegen Druck f�r sonstige Konditionsbl�tter
	//if (isset($Param['Sonstiges']) AND $Param['Sonstiges']<>'on')
	//{
	//	 $SQL.= ' LEFT OUTER JOIN V_CRM_KONDITIONEN ON CIN_WG = KEYWG AND CIN_DL = KEYDL AND CIN_RH = KEYRH';
	//}
	//$SQL.=" WHERE  (CKO_LETZTEAKTION IS NULL OR CKO_LETZTEAKTION < SYSDATE)";

	//if (isset($Param['Sonstiges']) AND $Param['Sonstiges']=='on')
	//{
	//	$SQL .= ' AND CAD_KEY IN (SELECT DISTINCT CAD_KEY FROM CRM_DRUCK_KONDITIONEN)';
	//}

	//Nur Status Kunde drucken   TR 14.09.09
	$SQL .= ' AND CAD_STATUS = \'K\'';
    //nur aktive Vertrage + keinen Poststatus drucken OP 11.03.2013
    $SQL .= ' AND POST_STATUS = 0';
    $SQL .= ' AND VERTRAGSSTATUS = \'aktiv\'';
    $SQL .= ' AND mvs_key IS NULL';

	if(isset($Param['LAN_CODE']) AND $Param['LAN_CODE']!='')		// Parameter f�r Massendruck
	{
		$SQL .= ' AND CAD_LAN_CODE = '.awis_FeldInhaltFormat('T',$Param['LAN_CODE']);
	}
	if(isset($Param['KON_KEY']) AND $Param['KON_KEY']>0)		// Parameter f�r Massendruck
	{
		$SQL .= ' AND KON_KEY = 0'.$Param['KON_KEY'];
	}
	if (isset($Param['CKM_KEY']) AND $Param['CKM_KEY']!='') // Konditionsmodell ausgewaehlt
	{
		if ($Param['CKM_KEY'] != '999999999') // Konditionsmodell "Sonstige" ausgewaehlt
		{
			$SQL .= ' AND KEY2 = \''.$Param['CKM_KEY'].'\'';
			$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
		}
		else
		{
			$SQL .= ' AND ANZAHL <= 50';
	        $SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))='.awis_FeldInhaltFormat('T',$Param['CKD_KONDITIONSBLATT_REIFENGRUPPE']);
			$SQL .= ' AND ((select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
            //Wegen Druck f�r sonstige Konditionsbl�tter
			//if (isset($Param['Sonstiges']) AND $Param['Sonstiges']<>'on')
			//{
			//	$SQL .= ' AND ANZAHL <= 50';
			//}
		}
	}
    elseif($Param['CKM_KEY'] == '') // kein Konditionsmodell ausgewaehlt
    {
		if($Param['CKD_KONDITIONSBLATT_REIFENGRUPPE'] == '1')
		{	
			$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))=1';
			$SQL .= ' AND ((select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
			$SQL .= ' OR (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1)) is null)';
		}
		elseif($Param['CKD_KONDITIONSBLATT_REIFENGRUPPE'] == '2')
		{
			$SQL .= ' AND ((select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))=2';
			$SQL .= ' OR (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1)) is null)';
			$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
		}
		
        //$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))='.awis_FeldInhaltFormat('T',$Param['CKD_KONDITIONSBLATT_REIFENGRUPPE']);
    }

    //$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
	//	$SQL .= " ORDER BY " . $ORDERBY;
	$SQL .= ' '.$ORDERBY;
	awis_Debug(1,$SQL);
//var_dump($SQL);
//die();

    @ob_clean();
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename="'.'Konditionsblatt'.'.csv"');

	for($Block=0;;$Block++)
	{
		$SQLCSV = 'SELECT * FROM ('.$SQL.') CSVDATEN';
		$SQLCSV .= ' WHERE zeile >= '.($Block*1000).' AND zeile<'.(($Block+1)*1000);

		$rsCSV = awisOpenRecordset($con,$SQLCSV);
		$rsCSVZeilen = $awisRSZeilen;
		if($rsCSVZeilen==0)
		{
			break;
		}

		if($Block==0)
		{
			$Zeile='';
			for($Spalte=1;$Spalte<=$awisRSSpalten;$Spalte++)
			{
				$Zeile .= "\t".$awisRSInfo[$Spalte]["Name"];
			}
			$Zeile .= "\n";

			echo substr($Zeile,1);
		}

		for($CSVZeile=0;$CSVZeile<$rsCSVZeilen;$CSVZeile++)
		{
			$Zeile='';
			for($Spalte=1;$Spalte<=$awisRSSpalten;$Spalte++)
			{
				$Zeile .= "\t".$rsCSV[$awisRSInfo[$Spalte]["Name"]][$CSVZeile];
			}
			$Zeile .= "\n";

			echo substr($Zeile,1);
		}
	}
	die();
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Ausdruck','txtUeberschriftFlottenkundenKonditionen');
$TextKonserven[]=array('Ausdruck','txtFirmaVertraulich');
$TextKonserven[]=array('Ausdruck','txtKonditionen');
$TextKonserven[]=array('Ausdruck','txtAlleWertezzglMwSt');
$TextKonserven[]=array('Ausdruck','txtKonditionenHinweisReifen');
$TextKonserven[]=array('Ausdruck','txtRabattbasisArtikel');
$TextKonserven[]=array('Ausdruck','txtRabattbasisReifen1');
$TextKonserven[]=array('Ausdruck','txtRabattbasisReifen2');
$TextKonserven[]=array('Ausdruck','txtKonditionenSchluss');
$TextKonserven[]=array('Ausdruck','txtHinweisPortal');
$TextKonserven[]=array('Wort','Sommer');
$TextKonserven[]=array('Wort','Winter');
$TextKonserven[]=array('Wort','Benutzername');
$TextKonserven[]=array('Wort','Kennwort');
$TextKonserven[]=array('CKD','CRM_ZusatzText_%');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

$ORDERBY = " ORDER BY CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_ORT";

// Abfrage mit den Kunden, f�r die etwas gedruckt werden soll
$SQL = 'SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_LAN_CODE, CAD_ORT, CAD_KEY, CAD_STATUS';
$SQL .= ', CKO_LETZTEAKTION, CAQ_DATUM, CAQ_VERTRAGSNUMMER';
$SQL .= ', KEYWG, KEYDL, KEYRH, KEYTH';
$SQL .= ', TEXT_SOMMER, TEXT_WINTER, REIFENBEZ, PORTAL_USER, PORTAL_USERPWD, KON_NAME1, KON_NAME2, LAND';
$SQL .= ', ROW_NUMBER() OVER('.$ORDERBY.') AS ZEILE';
$SQL .= ' FROM (';
$SQL .= 'SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_LAN_CODE, CAD_ORT, CAD_KEY, CAD_STATUS';
$SQL .= ', CKO_LETZTEAKTION, CAQ_DATUM, CAQ_VERTRAGSNUMMER';
$SQL .= ', NVL(KEYWG,0) as KEYWG, NVL(KEYDL,0) as KEYDL, NVL(KEYRH,0) as KEYRH, NVL(KEYTH,0) as KEYTH';
$SQL .= ', TEXT_SOMMER, TEXT_WINTER, REIFENBEZ, PORTAL_USER, PORTAL_USERPWD, KON_NAME1, KON_NAME2, KON_KEY,POST_STATUS,VERTRAGSSTATUS,LAND';
$SQL .= ' FROM (';
$SQL .= 'SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_LAN_CODE, CAD_ORT, CAD_KEY, CAD_STATUS';
$SQL .= ', CKO_LETZTEAKTION, CAQ_DATUM, CAQ_VERTRAGSNUMMER';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1) AS KEYWG';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1110 AND ROWNUM=1) AS KEYDL';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1) AS KEYRH';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1121 AND ROWNUM=1) AS KEYTH';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1018 AND ROWNUM=1) AS TEXT_SOMMER';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1019 AND ROWNUM=1) AS TEXT_WINTER';
$SQL .= ', (select CKM_BEZEICHNUNG from crmkonditionsmodelle where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1)) AS REIFENBEZ';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1002 AND ROWNUM=1) AS PORTAL_USER';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1003 AND ROWNUM=1) AS PORTAL_USERPWD';
$SQL .= ', KON_NAME1, KON_NAME2,KON_KEY';
$SQL .= ', (SELECT CIN_WERT FROM CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 4007 AND ROWNUM = 1) AS POST_STATUS';
$SQL .= ', (SELECT CIN_WERT FROM CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 4005 AND ROWNUM = 1) AS VERTRAGSSTATUS';
$SQL .= ', (SELECT CIN_WERT FROM CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1013 AND ROWNUM = 1) AS LAND';
$SQL .= ' FROM crmAdressen';
if($DruckArt==1)
{
	$SQL .= ' LEFT OUTER JOIN crmAktionen ON cad_key = cko_xxx_key AND cko_cat_key IN (6,37)';		// Aktion: Konditionsbl�tter (Mail & Post)
}
else
{
	$SQL .= ' INNER JOIN crmAktionen ON cad_key = cko_xxx_key AND cko_cat_key IN (6,37)';		// Aktion: Konditionsbl�tter (Mail & Post)
}
$SQL .= ' LEFT OUTER JOIN crmAkquisestand ON cad_key = caq_cad_key';
$SQL .= ' LEFT OUTER JOIN Kontakte ON KON_KEY = CAD_KON_KEY';
$SQL .= ')';
$SQL .= ')';
$SQL .= ' LEFT OUTER JOIN V_CRM_KONDITIONEN ON CIN_WG = KEYWG AND CIN_DL = KEYDL AND CIN_RH = KEYRH';
$SQL .= ' LEFT OUTER JOIN mailversand ON cad_key = mvs_xxx_key AND mvs_xtn_kuerzel =  \'CAD\' AND mvs_eingestellt > sysdate - 150';

//Wegen Druck f�r sonstige Konditionsbl�tter
//if (isset($Param['Sonstiges']) AND $Param['Sonstiges']<>'on')
//{
	//$SQL .= ' LEFT OUTER JOIN V_CRM_KONDITIONEN ON CIN_WG = KEYWG AND CIN_DL = KEYDL AND CIN_RH = KEYRH';
//}


if($CADKEY==0)		// Alle abh�ngig vom letzten Termin
{
	//$SQL .= ' 		WHERE  (CKO_LETZTEAKTION IS NULL OR CKO_LETZTEAKTION < SYSDATE)';
	$SQL .= ' 		WHERE (CKO_LETZTEAKTION IS NULL OR CKO_LETZTEAKTION < TRUNC(SYSDATE))';

	//Wegen Druck f�r sonstige Konditionsbl�tter
	//if (isset($Param['Sonstiges']) AND $Param['Sonstiges']=='on')
	//{
	//	$SQL .= ' AND CAD_KEY IN (SELECT DISTINCT CAD_KEY FROM CRM_DRUCK_KONDITIONEN)';
	//}


	//Nur Status Kunde drucken   TR 14.09.09
	// TODO: �ndern auf aktive Vertr�ge!!, SK 15.09.2009
	$SQL .= ' AND CAD_STATUS = \'K\'';
    //nur aktive Vertrage die keinen Poststatus haben drucken OP 07.03.2013
    $SQL .= ' AND POST_STATUS = 0';
    $SQL .= ' AND VERTRAGSSTATUS = \'aktiv\'';
    $SQL .= ' AND mvs_key IS NULL';

	if(isset($Param['KON_KEY']) AND $Param['KON_KEY']>0)		// Parameter f�r Massendruck
	{
		$SQL .= ' AND KON_KEY = 0'.$Param['KON_KEY'];
	}

	if(isset($Param['LAN_CODE']) AND $Param['LAN_CODE']!='')		// Parameter f�r Massendruck
	{
		$SQL .= ' AND CAD_LAN_CODE = '.awis_FeldInhaltFormat('T',$Param['LAN_CODE']);
	}


	if (isset($Param['CKM_KEY']) AND $Param['CKM_KEY']!='') // Konditionsmodell ausgewaehlt
	{
		if ($Param['CKM_KEY'] != '999999999') // Konditionsmodell "Sonstige" ausgewaehlt
		{
			$SQL .= ' AND KEY2 = \''.$Param['CKM_KEY'].'\'';
		}
		else
		{
			$SQL .= ' AND ANZAHL <= 50';
	        $SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))='.awis_FeldInhaltFormat('T',$Param['CKD_KONDITIONSBLATT_REIFENGRUPPE']);

			//Wegen Druck f�r sonstige Konditionsbl�tter
			//if (isset($Param['Sonstiges']) AND $Param['Sonstiges']<>'on')
			//{
			//	$SQL .= ' AND ANZAHL <= 50';
			//}
		}
	}
    elseif($Param['CKM_KEY'] == '') // kein Konditionsmodell ausgewaehlt
    {
		if($Param['CKD_KONDITIONSBLATT_REIFENGRUPPE'] == '1')
		{	
			$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))=1';
			$SQL .= ' AND ((select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
			$SQL .= ' OR (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1)) is null)';
		}
		elseif($Param['CKD_KONDITIONSBLATT_REIFENGRUPPE'] == '2')
		{
			$SQL .= ' AND ((select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))=2';
			$SQL .= ' OR (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1)) is null)';
			$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
		}
		
        //$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1))='.awis_FeldInhaltFormat('T',$Param['CKD_KONDITIONSBLATT_REIFENGRUPPE']);
    }

	// Nur wenn f�r dieses Modell das Konditionsblatt erlaubt/notwendig ist
	//$SQL .= ' AND (select CKM_KONDBLATT from CRMKONDITIONSMODELLE where ckm_key = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1))=1';
}
else 				// Nur einen einzelnen, ohne eine Aktion zu setzen
{
	$SQL .= ' WHERE CAD_KEY = 0'.$CADKEY;
}
//$SQL .= ' ORDER BY CAD_NAME1, CAD_NAME2';
$SQL .= ' '. $ORDERBY;
//awis_Debug(1,$SQL);
//var_dump($SQL);
//die();
$SQLBasis = $SQL;
for($Block=0;;$Block++)
{
	$SQLCSV = 'SELECT * FROM ('.$SQLBasis.') CSVDATEN';
	if ($Param['CKM_KEY'] == '999999999' || $Param['MAIL'] === true)
	{
	    $SQLCSV .= ' WHERE zeile >= '.($Block*1000).' AND zeile<'.(($Block+1)*1000);
	}

    $rsCAD = awisOpenRecordset($con, $SQLCSV);
    $rsCADZeilen = $awisRSZeilen;
    
    if($rsCADZeilen==0)
    {
        break;
    }
    
    /*
    if($rsCADZeilen==0)
    {
    	$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Konditionsblatt f�r '.$rsCAD['CAD_NAME1'][$CADZeile]);
    	$Ausdruck->NeueSeite(0,1);		// Mit Hintergrund
    	$Ausdruck->_pdf->SetFont('Arial','',20);
    	$Ausdruck->_pdf->SetXY(50,50);
    	$Ausdruck->_pdf->Cell(100,5,'Keine Daten zum Drucken!',0,0,'L',0);

    	$Ausdruck->Anzeigen();
    	die();
    }

    */

    if(isset($_GET['Text_1']))
    {
    	$ZusatzSommer = urldecode($_GET['Text_1']);
    }
    elseif(!isset($rsCAD['TEXT_SOMMER'][0]) OR $rsCAD['TEXT_SOMMER'][0]=='')
    {
    	//$ZusatzSommer = awis_BenutzerParameter($con,'CRM_ZusatzText_Sommer',$AWISBenutzer->BenutzerName());
    	$ZusatzSommer = $AWISSprachKonserven['CKD']['CRM_ZusatzText_Sommer'];

    	$SQL = 'SELECT XHW_KEY, XHW_WERTTXT FROM Hilfswerte WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID=\'1\'';
    	$rsText = awisOpenRecordset($con,$SQL);
    	$ZusatzSommer = $rsText['XHW_WERTTXT'][0];
    }
    else
    {
    	$ZusatzSommer = $rsCAD['TEXT_SOMMER'][0];
    }

    if(isset($_GET['Text_2']))
    {
    	$ZusatzWinter = urldecode($_GET['Text_2']);
    }
    elseif(!isset($rsCAD['TEXT_WINTER'][0]) OR $rsCAD['TEXT_WINTER'][0]=='')
    {
    	//$ZusatzWinter = awis_BenutzerParameter($con,'CRM_ZusatzText_Winter',$AWISBenutzer->BenutzerName());
    	$ZusatzWinter = $AWISSprachKonserven['CKD']['CRM_ZusatzText_Winter'];

    	$SQL = 'SELECT XHW_KEY, XHW_WERTTXT FROM Hilfswerte WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID=\'2\'';
    	$rsText = awisOpenRecordset($con,$SQL);
    	$ZusatzWinter = $rsText['XHW_WERTTXT'][0];
    }
    else
    {
    	$ZusatzWinter = $rsCAD['TEXT_WINTER'][0];
    }


    $SQL = 'SELECT XHW_KEY, XHW_WERTDAT FROM Hilfswerte WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID=\'3\'';
    $rsText = awisOpenRecordset($con,$SQL);
    $GuetligVom = $rsText['XHW_WERTDAT'][0];

    $SQL = 'SELECT XHW_KEY, XHW_WERTDAT FROM Hilfswerte WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID=\'4\'';
    $rsText = awisOpenRecordset($con,$SQL);
    $GuetligBis = $rsText['XHW_WERTDAT'][0];


    $Spalte = 20;
    $Zeile = 20;
    $Ausdruck = null;


    //awis_Debug(1, $rsCADZeilen, $DruckArt, $SQL);
    //die();

    for($CADZeile=0;$CADZeile<$rsCADZeilen;$CADZeile++)
    //for($CADZeile=0;$CADZeile<4;$CADZeile++)
    {
    	//echo $rsCAD['CAD_KEY'][$CADZeile].' - '.$rsCAD['CAD_NAME1'][$CADZeile].'<br>';
    	
    	// fuer Ausland soll eine andere Vorlage verwendet werden als fuer Deutschland
    	if($rsCAD['LAND'][$CADZeile] == 'DE')
    	{
    		$VorlagenNr = 0;
    	}
    	else
    	{
    		$VorlagenNr = 1;
    	}    	

    	if($DruckArt==1)
    	{
    		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Konditionsblatt f�r '.$rsCAD['CAD_NAME1'][$CADZeile]);
    		$Ausdruck->NeueSeite($VorlagenNr,1);		// Mit Hintergrund
    	}
    	elseif($Ausdruck==null AND $DruckArt==2)	// Eine Seite pro Kunde oder alles in eine)
    	{
    		//$Ausdruck = new awisAusdruck('P','A4',array(),'Konditionsblaetter');
    		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Konditionsblaetter');
    		//$Ausdruck->NeueSeite();			// Ohne Hintergrund
    		$Ausdruck->NeueSeite($VorlagenNr,1);			// Mit Hintergrund
    	}
    	elseif($Ausdruck!=null AND $DruckArt==2)	// Eine Seite pro Kunde oder alles in eine)
    	{
    		//$Ausdruck->NeueSeite();			// Ohne Hintergrund
    		$Ausdruck->NeueSeite($VorlagenNr,1);			// Mit Hintergrund
    	}

    	$Zeile = 20;

    	if(isset($Param['AktionSchreiben']) AND $Param['AktionSchreiben']=='on')
    	{
    		$SQL = 'UPDATE CRMAKTIONEN';
    		$SQL .= ' SET CKO_LETZTEAKTION = SYSDATE';
    		$SQL .= ' WHERE CKO_CAT_KEY = 6';
    		$SQL .= ' AND CKO_XXX_KEY = 0'.$rsCAD['CAD_KEY'][$CADZeile];

    		if(awisExecute($con, $SQL)===false)
    		{
    			awisErrorMailLink('CRM-Massendruck',1,'Fehler beim Speichern der letzten Aktion',$SQL);
    		}
    	}
    	$Ausdruck->_pdf->SetFont('Arial','',8);
    	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
    	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,$AWISSprachKonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L',0);
    	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,$AWISSprachKonserven['Ausdruck']['txtKonditionen'],0,0,'C',0);
    	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,date('d.m.Y'),0,0,'R',0);

    	// Firmenname
    	$Ausdruck->_pdf->SetFont('Arial','',10);
    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_NAME1'][$CADZeile]),0,0,'L',0);
    	//$Ausdruck->_pdf->Cell(40,5,'��������',0,0,'L',0);
    	$Zeile+=5;
    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_NAME2'][$CADZeile]),0,0,'L',0);
    	$Zeile+=5;
    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_STRASSE'][$CADZeile].' '.$rsCAD['CAD_HAUSNUMMER'][$CADZeile]),0,0,'L',0);
    	$Zeile+=5;
    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_PLZ'][$CADZeile].' '.$rsCAD['CAD_ORT'][$CADZeile]),0,0,'L',0);
    	$Zeile+=10;


    	// �berschrift
    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->SetFont('Arial','B',14);
    	//TODO: Text ersetzen!!


    	if($DruckArt==1)	// Einzelausdruck
    	{
    		$GueltigDatum = awis_PruefeDatum($rsCAD['CAQ_DATUM'][$CADZeile]);
    	}
    	else
    	{
    		$AktDatum = getdate();
    		if(isset($_GET['GueltigAb']))
    		{
    			$GueltigDatum = awis_PruefeDatum($_GET['GueltigAb'],0);
    		}
    		else
    		{
    			$GueltigDatum = date('d.m.Y',mktime(0,0,0,$AktDatum['mon']+1,1,$AktDatum['year']));
    		}
    	}
    	$Ueberschrift=$AWISSprachKonserven['Ausdruck']['txtUeberschriftFlottenkundenKonditionen'];
    	//$Ueberschrift=str_replace('#DATUM#',$GueltigDatum,$Ueberschrift);
    	$DatText = awisFormat('D',$GuetligVom).' - '.awisFormat('D',$GuetligBis);
    	$Ueberschrift=str_replace('#DATUM#',$DatText,$Ueberschrift);
    	$Ausdruck->_pdf->Cell(200,10,$Ueberschrift,0,0,'L',0);
    	$Zeile+=5;

    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->SetFont('Arial','B',10);
    	$Ausdruck->_pdf->Cell(200,10,'Vertragsnummer: '.$rsCAD['CAQ_VERTRAGSNUMMER'][$CADZeile],0,0,'L',0);
    	$Zeile+=8;


    /*
    	$ReifenBasis = 1;			// Hinweistext f�r die Reifen
    	//if($rsCAD['KEYRH'][$CADZeile]==20)
    	if(substr($rsCAD['REIFENBEZ'][$CADZeile],0,3)=='R 0')
    	{
    		$ReifenBasis=2;
    	}
    */
    	$Bereiche = array('KEYWG','KEYDL','KEYTH','KEYRH');
    	$flagReifen = false;

    	foreach ($Bereiche as $Bereich)
    	{
    		$SQL = "SELECT CKD_BEREICH, CKD_BEZEICHNUNG, CKM_BASIS, CKD_TEXT, CKD_TEXT2, CKM_KEY, CKD_BEMERKUNG, CKM_BEZEICHNUNG";
    		$SQL .= " FROM CRMKONDITIONSMODELLE";
    		$SQL .= " LEFT OUTER JOIN CRMKONDITIONSMODELLDETAILS ON CKM_KEY = CKD_CKM_KEY";
    		$SQL .= " WHERE CKM_KEY = 0".$rsCAD[$Bereich][$CADZeile];
    		$SQL .= " AND CKD_STATUS = 'A'";
    		$SQL .= " ORDER BY CKD_SORTIERUNG";

    		$rsDaten = awisOpenRecordset($con, $SQL);
    		$rsDatenZeilen = $awisRSZeilen;
    		$LetzterBereich = '';
    		for($DatenZeile=0;$DatenZeile<$rsDatenZeilen;$DatenZeile++)
    		{
    			if($LetzterBereich!=$rsDaten['CKD_BEREICH'][$DatenZeile])
    			{
    				$Ausdruck->_pdf->SetFillColor(200,200,200);

    				if($Bereich=='KEYRH' and $rsDaten['CKM_BEZEICHNUNG'][$DatenZeile]!='RBest')
    				{
    					// wenn Konditionen Reifen R1,R2 und groesser hinterlegt ist dann
    					// muss weiterer Zusatztext angezeigt werden (daher Flag auf true)
    					$flagReifen = true;
    					
    					$Zeile+=10;
    					$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    					$Ausdruck->_pdf->SetFont('Arial','',10);
    					$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-30,5,'Reifen: siehe Seite 2',0,0,'L',0);

    					$Ausdruck->NeueSeite($VorlagenNr,1);			// Mit Hintergrund
    					$Zeile = 35;

    					$Ausdruck->_pdf->SetFont('Arial','',8);
    					$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
    					$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,$AWISSprachKonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L',0);
    					$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,$AWISSprachKonserven['Ausdruck']['txtKonditionen'],0,0,'C',0);
    					$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,date('d.m.Y'),0,0,'R',0);
    				}
    				else
    				{
    					$Zeile+=5;
    				}

    				$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    				$Ausdruck->_pdf->SetFont('Arial','B',12);
    				$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-30,5,$rsDaten['CKD_BEREICH'][$DatenZeile].''.($rsDaten['CKM_BASIS'][$DatenZeile]!=''?': '.$rsDaten['CKM_BASIS'][$DatenZeile]:''),0,0,'L',1);
    				if($Bereich=='KEYRH')
    				{
    					$Zeile+=5;
    					$Ausdruck->_pdf->SetXY($Spalte+$Ausdruck->SeitenBreite()-100,$Zeile);
    					$Ausdruck->_pdf->SetFont('Arial','B',10);
    					$Ausdruck->_pdf->Cell(35,5,$AWISSprachKonserven['Wort']['Sommer'].' '.$ZusatzSommer,0,0,'R',0);
    					$Ausdruck->_pdf->Cell(35,5,$AWISSprachKonserven['Wort']['Winter'].' '.$ZusatzWinter,0,0,'R',0);
    				}
    				$Zeile+=5;

    				$LetzterBereich=$rsDaten['CKD_BEREICH'][$DatenZeile];
    			}

    			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    			$Ausdruck->_pdf->SetFont('Arial','',10);
//    			if($rsDaten['CKD_TEXT2'][$DatenZeile]=='')
				if(is_null($rsDaten['CKM_BASIS'][$DatenZeile]))
    			{
    				if($Bereich=='KEYWG')
    				{
    					$Text = $rsDaten['CKD_BEZEICHNUNG'][$DatenZeile].' '.$rsDaten['CKD_BEMERKUNG'][$DatenZeile];
    				}
    				else
    				{
    					$Text = $rsDaten['CKD_BEZEICHNUNG'][$DatenZeile];
    				}
    				$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-60,5,awisFormat('TP',$Text),0,0,'L',0);

    				$Ausdruck->_pdf->Cell(30,5,awisFormat('TP',$rsDaten['CKD_TEXT'][$DatenZeile]),0,0,'R',0);
    			}
    			else		// bei Reifen gibt es zwei Bereiche!
    			{
    				if($Bereich=='KEYRH')
    				{
    					$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-180,5,$rsDaten['CKD_BEZEICHNUNG'][$DatenZeile],0,0,'L',0);
    					$Ausdruck->_pdf->SetFont('Arial','',8);
    					$Ausdruck->_pdf->Cell(80,5,$rsDaten['CKD_BEMERKUNG'][$DatenZeile],0,0,'L',0);
    					$Ausdruck->_pdf->SetFont('Arial','',10);
    				}
    				else
    				{
    					$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-100,5,$rsDaten['CKD_BEZEICHNUNG'][$DatenZeile],0,0,'L',0);
    				}
    					// Sommer = Text2
    				$Ausdruck->_pdf->Cell(35,5,awisFormat('TP',$rsDaten['CKD_TEXT2'][$DatenZeile]),0,0,'R',0);
    					// Winter = Text
    				$Ausdruck->_pdf->Cell(35,5,awisFormat('TP',$rsDaten['CKD_TEXT'][$DatenZeile]),0,0,'R',0);
    			}

    			$Zeile+=5;
    		}
    	}

    	$Zeile+=10;

    	// Zugang zum Portal
    	if($rsCAD['PORTAL_USER'][$CADZeile]!='')
    	{
    		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    		$Ausdruck->_pdf->SetFont('Arial','',10);
    		$Ausdruck->_pdf->Cell(100,5,$AWISSprachKonserven['Ausdruck']['txtHinweisPortal'],0,0,'L',0);
    		$Zeile+=5;
    		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    		$Ausdruck->_pdf->Cell(40,5,$AWISSprachKonserven['Wort']['Benutzername'].':',0,0,'L',0);
    		$Ausdruck->_pdf->Cell(100,5,$rsCAD['PORTAL_USER'][$CADZeile],0,0,'L',0);
    		$Zeile+=5;
    		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    		$Ausdruck->_pdf->Cell(40,5,$AWISSprachKonserven['Wort']['Kennwort'].':',0,0,'L',0);
    		$Ausdruck->_pdf->Cell(100,5,$rsCAD['PORTAL_USERPWD'][$CADZeile],0,0,'L',0);
    	}

    	$Zeile+=10;

    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->SetFont('Arial','',10);
    	$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-30,5,$AWISSprachKonserven['Ausdruck']['txtAlleWertezzglMwSt'],0,0,'L',0);
    	$Zeile+=5;
    	/*$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-30,5,$AWISSprachKonserven['Ausdruck']['txtRabattbasisArtikel'],0,0,'L',0);
    	$Zeile+=5;
    	*/
    	/*
    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	if($ReifenBasis==2)
    	{
    		$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-30,5,$AWISSprachKonserven['Ausdruck']['txtRabattbasisReifen2'],0,0,'L',0);
    	}
    	else
    	{
    		$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-30,5,$AWISSprachKonserven['Ausdruck']['txtRabattbasisReifen1'],0,0,'L',0);
    	}
    	$Zeile+=5;
    */
    	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    	//$Ausdruck->_pdf->Cell($Ausdruck->SeitenBreite()-30,5,$AWISSprachKonserven['Ausdruck']['txtKonditionenSchluss'],0,0,'L',0);
    	$Ausdruck->_pdf->MultiCell($Ausdruck->SeitenBreite()-30,5,$AWISSprachKonserven['Ausdruck']['txtKonditionenSchluss'],0,'L',0);
    	
    	if($flagReifen)
    	{
    		$Ausdruck->_pdf->SetXY($Spalte,$Ausdruck->_pdf->GetY());
    		$Ausdruck->_pdf->MultiCell($Ausdruck->SeitenBreite()-30,5,$AWISSprachKonserven['Ausdruck']['txtKonditionenHinweisReifen'],0,'L',0);
    		//$Ausdruck->_pdf->MultiCell($Ausdruck->SeitenBreite()-30,5,'bla',0,'L',0);
    	}
    	
    	if($DruckArt==1)
    	{
    		$Ausdruck->Anzeigen();
    	}
    }	// Jeden Kunden

    // Ende des Massendrucks?
    if($DruckArt==2 AND $Ausdruck!=null)
    {
    	
    	if ($Param['CKM_KEY'] == '999999999' || $Param['MAIL'] === true)
    	{
    	    @ob_clean();
    	    $Ausdruck->_pdf->Output('/tmp/crm.pdf');

            require_once 'awisMailer.inc';
            require_once 'awisDatenbank.inc';
            require_once 'awisFormular.inc';
            $DB = awisDatenbank::NeueVerbindung('AWIS');
            $AktBenutzer = awisBenutzer::Init();
            $Mailer = new awisMailer($DB, $AktBenutzer);
            $Mailer->Absender('awis@de.atu.eu');
            $Mailer->Betreff('CRM');
            $Mailer->AdressListe(awisMailer::TYP_TO,$Param['EMAIL'],false,false);
            //$Mailer->AdressListe(awisMailer::TYP_CC,'shuttle@de.atu.eu',false,false);
            $Mailer->Anhaenge('/tmp/crm.pdf', 'Konditionsblatt_Gruppe_'.$Param['CKD_KONDITIONSBLATT_REIFENGRUPPE'].'_Block_'.$Block.'.pdf');
            $Mailer->MailSenden();
    	}
    	else
    	{
        	@ob_clean();
    	    $Ausdruck->Anzeigen();
    	}
    }
}    // Schleife mit den (Teil-) Bl�cken
awisLogoff($con);
if ($Param['CKM_KEY'] == '999999999' || $Param['MAIL'] === true)
{

    echo 'Daten wurden per E-Mail versendet.<br>';
}

?>