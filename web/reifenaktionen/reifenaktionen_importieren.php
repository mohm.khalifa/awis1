<?php
try
{
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');


	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);
	$Form->DebugAusgabe(1,$_FILES);
	
	$AWISWerkzeuge = new awisWerkzeuge();
	
	$Statistik['neu']=0;
	$Statistik['geaendert']=0;
	// Pr�fsumme ermitten, um zu pr�fen, ob die Datei schon mal importiert wurde
	$MD5 = md5_file($_FILES['IMPORT']['tmp_name']);
	$SQL = 'SELECT *';
	$SQL .= ' FROM IMPORTPROTOKOLL';
	$SQL .= ' WHERE XDI_BEREICH = \'RRA\'';
	$SQL .= ' AND XDI_MD5 = '.$DB->FeldInhaltFormat('T',$MD5);
	$rsXDI = $DB->RecordSetOeffnen($SQL);
	$Form->DebugAusgabe(1,$DB->LetzterSQL());
//	die();
	
	if(!$rsXDI->EOF())
	{
		// Dateien k�nnen immer nur einmal eingelesen werden
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datei wurde bereits importiert.', 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Benutzer:'.$rsXDI->FeldInhalt('XDI_USER'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datum:'.$rsXDI->FeldInhalt('XDI_DATUM'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Dateiname:'.$rsXDI->FeldInhalt('XDI_DATEINAME'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->Trennzeile();
		$Form->Formular_Ende();
	}
	else
	{
		$DB->TransaktionBegin();
		
		$Datei = $_FILES['IMPORT']['tmp_name'];

		// Datei �ffnen
		$XLSXObj = PHPExcel_IOFactory::load($Datei);

				// Blatt untersuchen
		$BlattObj = $XLSXObj->getActiveSheet();
		// Kenner lesen
		$Kenner = $BlattObj->getCell('A1')->getValue();
		
		if($Kenner != 'AWIS')
		{
			$Form->Erstelle_TextLabel('Falsche Importdatei.'.$Kenner, 800);
		}
		else
		{
			$Form->Erstelle_TextLabel('Daten werden importiert...', 800);

			$SQL = 'INSERT INTO IMPORTPROTOKOLL(';
			$SQL .= 'XDI_BEREICH, XDI_DATEINAME, XDI_DATUM, XDI_MD5';
			$SQL .= ', XDI_USER,XDI_USERDAT)VALUES (';
			$SQL .= ' \'RRA\'';
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$_FILES['IMPORT']['name']);
			$SQL .= ', SYSDATE';
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$MD5);
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
			$SQL .= ', SYSDATE)';
			$DB->Ausfuehren($SQL);
			$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
			$SQL = 'SELECT seq_XDI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$XDI_KEY=$rsKey->FeldInhalt('KEY');
			
			// �berschriften lesen
			$Ueberschrift=array();
			$Zeile = 2;
			$Spalte = 'A';
			$Zelle = $Spalte.$Zeile;
			while($BlattObj->getCell($Zelle)->getValue()!='')
			{
				$Ueberschrift[$BlattObj->getCell($Zelle)->getValue()]=$Spalte++;
				$Zelle = $Spalte.$Zeile;
			}
						
			$Form->DebugAusgabe(1,$Ueberschrift);
			
			$FEHLER = false;
			
			$SQL = 'SELECT LAN_CODE FROM LAENDER WHERE LAN_WWSKENN IS NOT NULL';
			$rsLAN=$DB->RecordSetOeffnen($SQL);
			$GueltigeLaender=$rsLAN->Tabelle();
			
			// Jetzt die Daten
			$Zeile=3;
			while ($BlattObj->getCell($Ueberschrift['AKTIONSNAME'].$Zeile)->getValue()!='')
			{
				// Pr�froutinen f�r die Daten
                $Land = $BlattObj->getCell($Ueberschrift['LAND'].$Zeile)->getValue();
                if(!in_array($Land, $GueltigeLaender['LAN_CODE']))
                {
                     echo '<br>(W) Zeile '.$Zeile.': falsches Land: '.$Land.'. Verwende DE';
                     $Land = 'DE';
                }
				
				$GueltigAb = $BlattObj->getCell($Ueberschrift['GUELTIGAB'].$Zeile)->getValue();
				if($GueltigAb!='')
				{
					if(is_numeric($GueltigAb) AND $GueltigAb >30000 AND $GueltigAb <99999)
					{
						$GueltigAb = date('d.m.Y',strtotime('01-01-1900 + '.$GueltigAb .' days - 2 days'));
					}
					else
					{
						$GueltigAb = $Form->PruefeDatum($GueltigAb,false,false,false);
					}
				}
				else
				{
					$GueltigAb = date('d.m.Y');
				}
                
				$GueltigBis = $BlattObj->getCell($Ueberschrift['GUELTIGBIS'].$Zeile)->getValue();
				if($GueltigAb!='')
				{
					if(is_numeric($GueltigBis) AND $GueltigBis >30000 AND $GueltigBis <99999)
					{
						$GueltigBis = date('d.m.Y',strtotime('01-01-1900 + '.$GueltigBis .' days - 2 days'));
					}
					else
					{
						$GueltigBis = $Form->PruefeDatum($GueltigBis,false,false,false);
					}
				}
				else
				{
					$GueltigBis = date('d.m.Y');
				}
/*				
				$Benachrichtigung = 0;
				switch(strtolower($BlattObj->getCell($Ueberschrift['INFOMAIL'].$Zeile)->getValue()))
				{
					case '1':
					case 'y':
					case 'j':
						$Benachrichtigung  |= 1;
						break;
				}
				switch(strtolower($BlattObj->getCell($Ueberschrift['INFOSMS'].$Zeile)->getValue()))
				{
					case '1':
					case 'j':
					case 'y':
						$Benachrichtigung  |= 2;
						break;
				}
				switch(strtolower($BlattObj->getCell($Ueberschrift['INFOBRIEF'].$Zeile)->getValue()))
				{
					case '1':
					case 'j':
					case 'y':
						$Benachrichtigung  |= 4;
						break;
				}
                if($Benachrichtigung==0)
				{
                     echo '<br>(W) Zeile '.$Zeile.': keine Benachrichtigung definiert.';
                }
*/
				$ATUNRLISTE = KodiereText($BlattObj->getCell($Ueberschrift['ATUNRLISTE'].$Zeile)->getValue());
				$ATUNRLISTE=str_replace("\r\n",',',$ATUNRLISTE);
				$ATUNRLISTE=str_replace("\t",',',$ATUNRLISTE);
				$ATUNRLISTE=str_replace(' ','',$ATUNRLISTE);
				$ATUNRLISTE=str_replace(';',',',$ATUNRLISTE);
				$ATUNRLISTE=str_replace('/',',',$ATUNRLISTE);
				$ATUNRLISTE=strtoupper($ATUNRLISTE);
				
				$i = 1;
				while ($i>0)
				{
					$ATUNRLISTE=str_replace(',,',',',$ATUNRLISTE,$i);
				}
				
				$SortArr=array();
				$SortArr=explode(',',$ATUNRLISTE);
				asort($SortArr);
				$ATUNRLISTE=implode(',', $SortArr);
				
				//*********************************************************************************
				// Daten importieren
				//*********************************************************************************
				$SQL = 'SELECT REH_KEY';
				$SQL .= ' FROM REIFENHERSTELLER';
				$SQL .= ' WHERE TRIM(UPPER(REH_BEZEICHNUNG)) = TRIM(UPPER('.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['HERSTELLER'].$Zeile)->getValue())) .')) ';
				$rsREH = $DB->RecordSetOeffnen($SQL);
				
				$SQL = 'SELECT *';
				$SQL .= ' FROM REIFENAKTIONEN';
				$SQL .= ' WHERE RRA_AKTIONSNAME ='.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['AKTIONSNAME'].$Zeile)->getValue()));
				$SQL .= ' AND TRUNC(RRA_GUELTIGAB) = TRUNC('.$DB->FeldInhaltFormat('D',$GueltigAb).')';
				$SQL .= ' AND TRUNC(RRA_GUELTIGBIS) = TRUNC('.$DB->FeldInhaltFormat('D',$GueltigBis).')';
				$SQL .= ' AND (RRA_LAN_CODE ='.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['LAND'].$Zeile)->getValue()));
				$SQL .= ' OR RRA_LAN_CODE IS NULL)';
				$SQL .= ' AND RRA_AKTIONSPROZENT = 0'.$DB->FeldInhaltFormat('N0',$BlattObj->getCell($Ueberschrift['AKTIONSPROZENT'].$Zeile)->getValue(),false);
				$SQL .= ' AND (RRA_REH_KEY ='.$DB->FeldInhaltFormat('N0',$rsREH->FeldInhalt('REH_KEY'));
				$SQL .= ' OR RRA_REH_KEY IS NULL)';
				$SQL .= ' AND (RRA_WGR_ID ='.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['WG'].$Zeile)->getValue()));
				$SQL .= ' OR RRA_WGR_ID IS NULL)';
				$SQL .= ' AND (RRA_WUG_ID ='.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['SORT'].$Zeile)->getValue()));
				$SQL .= ' OR RRA_WUG_ID IS NULL)';
				$SQL .= ' AND (RRA_KENN1 ='.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['KENN1'].$Zeile)->getValue()));
				$SQL .= ' OR RRA_KENN1 IS NULL)';
				$SQL .= ' AND (RRA_AKTIONSTEXT='.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['AKTIONSTEXT'].$Zeile)->getValue()));
				$SQL .= ' OR RRA_AKTIONSTEXT IS NULL)';
				$rsRRA = $DB->RecordSetOeffnen($SQL);
				
				$Form->DebugAusgabe(1,$DB->LetzterSQL());

				if($rsRRA->EOF())
				{
					// Jetzt den neuen anlegen
					$SQL  = 'INSERT';
					$SQL .= ' INTO REIFENAKTIONEN (';
					$SQL .= ' RRA_ATUNRLISTE,';
					$SQL .= ' RRA_WGR_ID,';
					$SQL .= ' RRA_WUG_ID,';
					$SQL .= ' RRA_KENN1,';
					$SQL .= ' RRA_REH_KEY,';
					$SQL .= ' RRA_AKTIONSNAME,';
					$SQL .= ' RRA_AKTIONSPROZENT,';
					$SQL .= ' RRA_AKTIONSTEXT,';
					$SQL .= ' RRA_GUELTIGAB,';
					$SQL .= ' RRA_GUELTIGBIS,';
					$SQL .= ' RRA_LAN_CODE,';
					$SQL .= ' RRA_XDI_KEY,';
					$SQL .= ' RRA_IMPORTSTATUS,';
					$SQL .= ' RRA_CREADAT,';
					$SQL .= ' RRA_USER,';
					$SQL .= ' RRA_USERDAT';
					$SQL .= ' ) VALUES (';
					$SQL .= ' :var_CL_rra_atunrliste';
					$SQL .= ',LPAD('.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['WG'].$Zeile)->getValue())).',2,\'0\')';
					$SQL .= ',LPAD('.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['SORT'].$Zeile)->getValue())).',3,\'0\')';
					$SQL .= ','.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['KENN1'].$Zeile)->getValue()));
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$rsREH->FeldInhalt('REH_KEY'));
					$SQL .= ','.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['AKTIONSNAME'].$Zeile)->getValue()));
					$SQL .= ',COALESCE('.$DB->FeldInhaltFormat('N0',KodiereText($BlattObj->getCell($Ueberschrift['AKTIONSPROZENT'].$Zeile)->getValue())).',0)';
					$SQL .= ','.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['AKTIONSTEXT'].$Zeile)->getValue()));
					$SQL .= ', TRUNC('.$DB->FeldInhaltFormat('D',$GueltigAb).')';
					$SQL .= ', TRUNC('.$DB->FeldInhaltFormat('D',$GueltigBis).')';
					$SQL .= ','.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['LAND'].$Zeile)->getValue()));
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$XDI_KEY);
					$SQL .= ',20';
					$SQL .= ',SYSDATE';
					$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', SYSDATE)';
					$Statistik['neu']++;
				}
				else
				{
					$SQL = 'UPDATE REIFENAKTIONEN';
					$SQL .= ' SET RRA_ATUNRLISTE = :var_CL_rra_atunrliste';
					$SQL .= ', RRA_WGR_ID = LPAD('.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['WG'].$Zeile)->getValue())).',2,\'0\')';
					$SQL .= ', RRA_WUG_ID = LPAD('.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['SORT'].$Zeile)->getValue())).',3,\'0\')';
					$SQL .= ', RRA_KENN1 = '.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['KENN1'].$Zeile)->getValue()));
					$SQL .= ', RRA_REH_KEY = '.$DB->FeldInhaltFormat('N0',$rsREH->FeldInhalt('REH_KEY'));
					$SQL .= ', RRA_AKTIONSPROZENT = COALESCE('.$DB->FeldInhaltFormat('N0',KodiereText($BlattObj->getCell($Ueberschrift['AKTIONSPROZENT'].$Zeile)->getValue())).',0)';
					$SQL .= ', RRA_AKTIONSTEXT = '.$DB->FeldInhaltFormat('T',KodiereText($BlattObj->getCell($Ueberschrift['AKTIONSTEXT'].$Zeile)->getValue()));
					$SQL .= ', RRA_XDI_KEY='.$DB->FeldInhaltFormat('N0',$XDI_KEY);
					$SQL .= ', RRA_IMPORTSTATUS = 30';
					$SQL .= ', RRA_USER=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', RRA_USERDAT=SYSDATE';
					$SQL .= ' WHERE RRA_key=0' . $rsRRA->FeldInhalt('RRA_KEY');
					$Statistik['geaendert']++;
				}
				
				$DB->SetzeBindevariable('RRA', 'var_CL_rra_atunrliste', $ATUNRLISTE);
				$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('RRA'));
				
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
				$Zeile++;
			}
		}
		$DB->TransaktionCommit();
		//$DB->TransaktionRollback();
		
		$Form->Erstelle_TextLabel('-- Neue Daten:'.$Statistik['neu'], 800);
		$Form->Erstelle_TextLabel('-- Ge&auml;nderte Daten:'.$Statistik['geaendert'], 800);
	}
}
catch (awisException $ex)
{
	var_dump($SQL);
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL(),$ex->getLine());
}
catch (Exception $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	var_dump($SQL);
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

/**
 * Kodierung des Textes, damit Umlaute richtig gespeichert werden
 * @param string $Text
 */
function KodiereText($Text)
{
    $Erg = iconv('UTF-8','WINDOWS-1252',$Text);
    return ($Erg);
}
?>