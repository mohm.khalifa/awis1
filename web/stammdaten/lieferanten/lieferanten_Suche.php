<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author    Sacha Kerres
 * @copyright ATU
 * @version   20090220
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LIE', '%');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'DatumVom');
    $TextKonserven[] = array('Wort', 'DatumBis');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'ttt_AuswahlSpeichern');
    $TextKonserven[] = array('Liste', 'lst_JaNein');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht605 = $AWISBenutzer->HatDasRecht(605);        // Rechte des Mitarbeiters

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./lieferanten_Main.php?cmdAktion=Details>");

    /**********************************************
     * * Eingabemaske
     ***********************************************/
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_LIE'));

    if (!isset($Param['SPEICHERN'])) {
        $Param['SPEICHERN'] = 'off';
        $Param['PBM_XBN_KEY'] = $AWISBenutzer->BenutzerID();
    }

    $Form->Formular_Start();

    // Lieferantennummer
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_NR'] . ':', 200);
    $Form->Erstelle_TextFeld('*LIE_NR', ($Param['SPEICHERN'] == 'on'?$Param['LIE_NR']:''), 5, 200, true);
    $AWISCursorPosition = 'sucLIE_NR';
    $Form->ZeileEnde();

    // Name des Lieferanten
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['LIE_NAME1'] . ':', 200);
    $Form->Erstelle_TextFeld('*LIE_NAME1', ($Param['SPEICHERN'] == 'on'?$Param['LIE_NAME1']:''), 30, 200, true);
    $Form->ZeileEnde();

    $Form->Trennzeile();

    $SQL = 'SELECT ITY_KEY,ITY_DATENQUELLE';
    $SQL .= ', func_textkonserve(\'ity_lbl_\'||ITY_KEY,\'Infofeld\',' . $DB->WertSetzen('ITY', 'T', $AWISBenutzer->BenutzerSprache()) . ') AS ITY_INFORMATION';
    $SQL .= ' FROM INFORMATIONSTYPEN';
    $SQL .= ' WHERE ITY_XTN_KUERZEL =' . $DB->WertSetzen('ITY', 'T', 'LIE');
    $SQL .= ' AND ITY_SUCHFELD=1';
    $SQL .= ' AND EXISTS (SELECT * FROM v_AccountRechte WHERE ITY_XRC_ID = XRC_ID ';
    $SQL .= '             AND XBN_KEY = ' . $DB->WertSetzen('ITY', 'N0', $AWISBenutzer->BenutzerID());
    $SQL .= '             AND BITAND(XBA_STUFE,ITY_RECHTESTUFE)= ITY_RECHTESTUFE';
    $SQL .= '             )';
    $SQL .= ' ORDER BY ITY_BEREICH, ITY_SORTIERUNG';

    $rsITY = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ITY'));
    while (!$rsITY->EOF()) {
        // Standardwert f�r das Feld aus der gespeicherten Suche
        $AktWert = (($Param['SPEICHERN'] == 'on' AND isset($Param['Suchfelder'][$rsITY->FeldInhalt('ITY_KEY')]) AND $Param['Suchfelder'][$rsITY->FeldInhalt('ITY_KEY')] != '')?$Param['Suchfelder'][$rsITY->FeldInhalt('ITY_KEY')]:'');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($rsITY->FeldInhalt('ITY_INFORMATION') . ':' . $AktWert . ':', 200);

        if ($rsITY->FeldInhalt('ITY_DATENQUELLE') != '') {
            switch (substr($rsITY->FeldInhalt('ITY_DATENQUELLE'), 0, 3)) {
                case 'TXT':
                    $Felder = explode(':', $rsITY->FeldInhalt('ITY_DATENQUELLE'));
                    $Daten = $Form->LadeTexte(array(array($Felder[1], $Felder[2])));
                    $Daten = explode('|', $Daten[$Felder[1]][$Felder[2]]);
                    $Form->Erstelle_SelectFeld('*ITY_KEY_' . $rsITY->FeldInhalt('ITY_KEY'), $AktWert, $rsITY->FeldInhalt('ITY_BREITE'), true, '',
                        '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten);
                    break;
                case 'SQL':
                    $Felder = explode(':', $rsITY->FeldInhalt('ITY_DATENQUELLE'));
                    $Form->Erstelle_SelectFeld('*ITY_KEY_' . $rsITY->FeldInhalt('ITY_KEY'), $AktWert, $rsITY->FeldInhalt('ITY_BREITE'), true, '', $Felder[1],
                        '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
                    break;
                default:
                    $Form->Erstelle_TextFeld('*ITY_KEY_' . $rsITY->FeldInhalt('ITY_KEY'), $AktWert, $rsITY->FeldInhalt('ITY_ZEICHEN'), $rsITY->FeldInhalt('ITY_BREITE'), true, '',
                        '', '', $rsITY->FeldInhalt('ITY_FORMAT'), '', '', $rsITY->FeldInhalt('ITY_DATENQUELLE'));
                    break;
            }
        } else {
            $Form->Erstelle_TextFeld('*ITY_KEY_' . $rsITY->FeldInhalt('ITY_KEY'), $AktWert, $rsITY->FeldInhalt('ITY_ZEICHEN'), $rsITY->FeldInhalt('ITY_BREITE'), true, '', '', '',
                $rsITY->FeldInhalt('ITY_FORMAT'));
        }

        $Form->ZeileEnde();
        $rsITY->DSWeiter();
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param['SPEICHERN'] == 'on'?'on':''), 30, true, 'on', '', $AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '../stammdaten_Main.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    if (($Recht605 & 4) == 4)        // Hinzuf�gen erlaubt?
    {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }

    $Form->SchaltflaechenEnde();
    $Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180836");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180825");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>