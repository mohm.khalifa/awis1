<?php
require_once 'awisMailer.inc';
require_once 'awisSlack.php';
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 04.10.2016
 * Time: 10:09
 *
 * Klasse f�r die Benachrichtigungen vom Scheduler.
 * Theoretisch auch f�r Benachrichtungen aus Jobs nutzbar
 *
 */
class awisJobsNotifications
{
    /**
     * AwisMailer
     * @var awisMailer
     */
    private $awisMail;

    /**
     * awisSlack
     * @var awisSlack
     */
    private $awisSlack;

    /**
     * Betreff der Nachricht
     * @var
     */
    private $_Betreff;

    /**
     * Text der Nachricht
     * @var
     */
    private $_Text;

    /**
     * Empf�nger f�r Email
     * @var
     */
    private $_MailEmpfaenger;

    /**
     * Empfenger der Slacknachricht
     * @var
     */
    private $_SlackEmpfaenger;
    /**
     * Ansender der Nachricht
     * @var
     */
    private $_Absender;

    /**
     * Prio der Nachricht
     * @var
     */
    private $_Prio;

    /**
     * Slack Nachricht enthalten?
     * @var
     */
    private $_Slack;

    /**
     * Mail Nachricht enthalten?
     * @var
     */
    private $_Mail;


    /**
     * setzt alles wieder zur�ck, f�r die n�chste Nachricht.
     */
    function LoescheAdressListe()
    {
        $this->_Slack = false;
        $this->_Mail = false;
        unset ($this->_Absender);
        unset ($this->_Betreff);
        unset ($this->_Text);
        unset ($this->_MailEmpfaenger);
        unset ($this->_SlackEmpfaenger);
        unset($this->_Prio);
        $this->awisMail->LoescheAdressListe();
    }

    function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
    {
        $this->awisMail = new awisMailer($DB, $AWISBenutzer);
        $this->awisSlack = new awisSlack($AWISBenutzer);
    }


    /**
     * Setzt den Betreff
     * @param string $NeuerBetreff
     */
    public function Betreff($NeuerBetreff = '')
    {
        $this->_Betreff = $NeuerBetreff;
    }

    /**
     * Setzt den Text
     * @param $NeuerText
     */
    public function Text($NeuerText)
    {
        $this->_Text = $NeuerText;
    }

    /**
     * Setzt den oder die Empf�nger.
     * Empf�nger kann sowohl eine Mailadresse, als auch ein Slack Channel/User sein. + Mischungen
     * @param $Empfaenger
     */
    public function Empaenger($Empfaenger)
    {
        $Empfaenger = explode(';', $Empfaenger);
        foreach ($Empfaenger as $Empf) {
            $Empf = trim($Empf);
            //Beginnt mit @ oder Hashtag deutet alles auf einen Slackemp�nger hin. Kein @ enthalten? deutet auf eine SlackChannelID hin.
            if ($Empf[0] == '@' or $Empf[0] == '#' or strpos($Empf, '@') === false) {
                $this->_SlackEmpfaenger = $Empf . ';';
                $this->_Slack = true;
            } else {
                $this->_MailEmpfaenger = $Empf . ';';
                $this->awisMail->AdressListe(awisMailer::TYP_TO, $Empf); //Mailempf�nger der Performance zur liebe schon hier setzen
                $this->_Mail = true;
             }
        }
    }

    /**
     * Setzt den Absender
     * @param $Absender
     */
    public function Absender($Absender)
    {
        $this->_Absender = $Absender;
    }

    const Benachrichtigung_Fehler = 3;
    const Benachrichtigung_Warnung = 2;
    const Benachrichtigung_Ok = 1;

    /**
     * Setzt die Prio der Nachricht. (awisMailer->Prioritaet() oder awisSlack->Benachrichtigungstyp())
     * @param $Prioritaet
     */
    public function Prioritaet($Prioritaet)
    {
        $this->_Prio = $Prioritaet;
    }

    /**
     * Versendet die Nachricht
     */
    public function Senden()
    {
        //War ein Slackempf�nger enthalten?
        if ($this->_Slack) {
            $this->awisSlack->Absender($this->_Absender);
            $this->awisSlack->Text($this->_Text);
            $this->awisSlack->Betreff($this->_Betreff);
            $this->awisSlack->Empfaenger($this->_SlackEmpfaenger);
            $this->awisSlack->BenachrichtigungsTyp($this->_Prio);
            $this->awisSlack->Senden();
        }

        //War ein Emailempf�nger enhalten?
        if ($this->_Mail) {
            $this->awisMail->Absender($this->_Absender);
            $this->awisMail->Text($this->_Text);
            $this->awisMail->Betreff($this->_Betreff);
            $this->awisMail->Prioritaet($this->_Prio);
            if ($this->awisMail->MailSenden() == false) {
                $this->awisMail->MailInWarteschlange();
            }
        }
    }
}