<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');


try
{
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


	if(($AWISBenutzer->HatDasRecht(10003)&16)==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$Form->SchreibeHTMLCode('<form name=frmStatistik method=post action=./zukaufbestellungen_Main.php?cmdAktion=Statistik>');

	$Form->Formular_Start();

	$Jahr = (isset($_GET['Jahr'])?$DB->FeldInhaltFormat('N0',$_GET['Jahr']):date('Y'));
	
	$Form->ZeileStart();
	for($i=2009;$i<=date('Y');$i++)
	{
		if($Jahr!=$i)
		{
			$Link = './zukaufbestellungen_Main.php?cmdAktion=Statistik&Jahr='.$i;
			$Form->Erstelle_TextLabel($i,50,'',$Link);
		}
	}	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->SchreibeHTMLCode('<img src=./zukaufbestellungen_Statistik_Zukauf.php?Jahr='.$Jahr.'>');
	$Form->ZeileEnde();

	$Form->Trennzeile('O');

	$Form->ZeileStart();
	$Form->SchreibeHTMLCode('<img src=./zukaufbestellungen_Statistik_OnlineBestellungen.php?Jahr='.$Jahr.'>');
	$Form->ZeileEnde();

	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=zukaufbestellungen&Aktion=statistik','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
}
catch (Exception $ex)
{
	awisFormular::DebugAusgabe(1,'Fehler '.$ex->getMessage());
}
?>