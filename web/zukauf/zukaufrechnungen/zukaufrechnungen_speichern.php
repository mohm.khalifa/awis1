<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	//***********************************************************************************
	//** Lieferschein �ndern / anlegen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZUR_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('ZUR','ZUR_%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtZUR_KEY'];
        $ArchivID = '~~';
        if($_POST['oldZUR_ARCHIVID'] != $_POST['txtZUR_ARCHIVID'])
        {
            $ArchivID = $_POST['txtZUR_ARCHIVID'];
        }


		$rsZUR = $DB->RecordSetOeffnen('SELECT * FROM Zukaufrechnungen WHERE ZUR_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsZUR->EOF())
		{
die('Keine NEUEN');
			// Artikel suchen
			$SQL = 'SELECT ZLA_KEY, ZAL_KEY, ZLA_BEZEICHNUNG';
			$SQL .= ' FROM Zukaufartikel';
			$SQL .= ' INNER JOIN Zukaufartikellieferanten ON ZLA_KEY = ZAL_ZLA_KEY AND ZAL_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUR_LIE_NR'],true);
			$SQL .= ' INNER JOIN ZUKAUFHERSTELLERCODES ON ZLA_ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUR_LIE_NR'],false);
			$SQL .= ' WHERE ZLA_ARTIKELNUMMER = '.$DB->FeldInhaltFormat('TU',$_POST['txtZUR_ARTIKELNUMMER'],false);
			$SQL .= ' AND ZHK_CODE = '.$DB->FeldInhaltFormat('TU',$_POST['txtZUR_HERSTELLER'],false);
			$rsZLA = $DB->RecordSetOeffnen($SQL);

			// Wenn nicht, rausl�schen, dann l�uft er in die Fehler
			if($rsZLA->EOF())
			{
				$_POST['txtZUR_ARTIKELNUMMER'] = '';
			}

			// Bezeichnung aus dem Artikelstamm, wenn sie angegeben wurde
			$_POST['txtZUR_ARTIKELBEZEICHNUNG'] = ($_POST['txtZUR_ARTIKELBEZEICHNUNG']==''?$rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'):$_POST['txtZUR_ARTIKELBEZEICHNUNG']);


			$Fehler = '';
			$Pflichtfelder = array('ZUR_FIL_ID','ZUR_LIE_NR','ZUR_HERSTELLER','ZUR_BESTELLDATUM','ZUR_ARTIKELNUMMER','ZUR_ARTIKELBEZEICHNUNG','ZUR_WANRKORR','ZUR_BESTELLMENGE','ZUR_PREISBRUTTO');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZUR'][$Pflichtfeld].'<br>';
				}
			}
			if($_POST['txtZUR_WANRKORR']!='')
			{
				if(!awisWerkzeuge::BerechnePruefziffer($_POST['txtZUR_WANRKORR'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_PruefzifferFalsch'].' '.$TXT_Speichern['ZUR']['ZUR_WANRKORR'].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}


			$SQL = 'INSERT INTO Zukaufrechnungen';
			$SQL .= '(ZUR_BESTELLDATUM,ZUR_FIL_ID,ZUR_LIE_NR,ZUR_WANR,ZUR_ZAL_KEY,ZUR_ZLA_KEY,';
			$SQL .= 'ZUR_ARTIKELNUMMER,ZUR_ARTIKELBEZEICHNUNG,ZUR_HERSTELLER,ZUR_VERSANDARTBEZ,';
			$SQL .= 'ZUR_SORTIMENT,ZUR_BESTELLMENGE,ZUR_MENGENEINHEITEN,ZUR_TAUSCHTEIL,ZUR_PREISBRUTTO,';
			$SQL .= 'ZUR_PREISANGELIEFERT,ZUR_ALTTEILWERT,ZUR_EINHEITENPROPREIS,ZUR_WAEHRUNG,';
			$SQL .= 'ZUR_ABSCHLUSSART,ZUR_ABSCHLUSSGRUND,';
			$SQL .= 'ZUR_USER,ZUR_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('DU',$_POST['txtZUR_BESTELLDATUM'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUR_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_WANRKORR'],true);		// in das richtige Feld speichern!!
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsZLA->FeldInhalt('ZAL_KEY'),true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsZLA->FeldInhalt('ZLA_KEY'),true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_ARTIKELNUMMER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_ARTIKELBEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_HERSTELLER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_VERSANDARTBEZ'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_SORTIMENT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUR_BESTELLMENGE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_MENGENEINHEITEN'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUR_TAUSCHTEIL'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUR_PREISBRUTTO'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUR_PREISANGELIEFERT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUR_ALTTEILWERT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUR_EINHEITENPROPREIS'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_WAEHRUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUR_ABSCHLUSSART'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUR_ABSCHLUSSGRUND'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_ZUR_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$ZUR_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$ZUR_KEY;

			if(isset($ZURZURKEY) AND $ZURZURKEY!=='')
			{
				$SQL = 'UPDATE Zukaufrechnungen SET ZUR_ZUR_KEY = '.$ZUR_KEY;
				$SQL .= ' WHERE ZUR_KEY = '.$ZURZURKEY;

				if($DB->Ausfuehren($SQL,'',true)===false)
				{
					self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Aktualisieren einer verkn�ften Bestellung von '.$this->_LIE_NR.'. ZURKEY:'.$ZURZURKEY.' sollte auf '.$ZUR_KEY.' verknpuepft werden.',$_POST['EXTERNEID']);
				}
			}
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';
$Form->DebugAusgabe(1,$_POST);
			$rsZUR = $DB->RecordSetOeffnen('SELECT * FROM Zukaufrechnungen WHERE ZUR_key=' . $_POST['txtZUR_KEY'] . '');
			$FeldListe = '';
			
			$_POST['oldZUR_EINZELPREIS']=$rsZUR->FeldInhalt('ZUR_EINZELPREIS');
			$_POST['txtZUR_EINZELPREIS']=$DB->FeldInhaltFormat('N2',$_POST['txtZUR_EKPR'],false)*(1-($DB->FeldInhaltFormat('N2',$_POST['txtZUR_RABATT1'],false)/100.0));
			$_POST['oldZUR_BETRAG']=$rsZUR->FeldInhalt('ZUR_BETRAG');
			$_POST['txtZUR_BETRAG']=$DB->FeldInhaltFormat('N2',$_POST['txtZUR_MENGE'],false)*$DB->FeldInhaltFormat('N2',$_POST['txtZUR_EINZELPREIS'],false);
			$Felder[]='txtZUR_EINZELPREIS';
			$Felder[]='txtZUR_BETRAG';
			
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$NullErlaubt = true;
					switch($FeldName)
					{
					    case 'ZUR_EINZELPREIS':
					    case 'ZUR_BETRAG':
					    case 'ZUR_EKPR':
					    case 'ZUR_MENGE':
					        $NullErlaubt=false;
					        break;
					}
					$WertNeu=$DB->FeldInhaltFormat($rsZUR->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],$NullErlaubt);
					$WertAlt=$DB->FeldInhaltFormat($rsZUR->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],$NullErlaubt);
					$WertDB=$DB->FeldInhaltFormat($rsZUR->FeldInfo($FeldName,'TypKZ'),$rsZUR->FeldInhalt($FeldName),$NullErlaubt);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='' AND $NullErlaubt)	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsZUR->FeldInhalt('ZUR_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Zukaufrechnungen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', ZUR_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', ZUR_userdat=sysdate';
				$SQL .= ' WHERE ZUR_key=0' . $_POST['txtZUR_KEY'] . '';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
				}
			}
		}

        // Archiv-ID ist immer f�r alle Positionen mit der gleichen Rechnungsnummer
        if($ArchivID!='~~')
        {
            $SQL = 'UPDATE Zukaufrechnungen SET';
            $SQL .= ' ZUR_ARCHIVID = '.$DB->FeldInhaltFormat('T',$ArchivID,true);
            $SQL .= ' WHERE ZUR_RECHNUNGSNUMMER = '.$DB->FeldInhaltFormat('T',$_POST['txtZUR_RECHNUNGSNUMMER'],true);
            $SQL .= ' AND ZUR_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUR_LIE_NR']);

            if($DB->Ausfuehren($SQL)===false)
            {
                throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
            }
        }
	}// Zukaufrechnungen


	$Felder = explode(';',$Form->NameInArray($_POST, 'txtARCHIVID_',1,1));
	if(!empty($Felder))
	{
        foreach($Felder as $Feld)
        {
            if($Feld!='' AND $_POST[$Feld]!=$_POST['old'.substr($Feld,3)])
            {
                $FeldTeile = explode('_',$Feld);
                $SQL = 'UPDATE zukaufrechnungen';
                $SQL .= ' SET ZUR_ARCHIVID = '.$DB->FeldInhaltFormat('T',$_POST[$Feld]);
                $SQL .= ' WHERE ZUR_RECHNUNGSNUMMER = '.$DB->FeldInhaltFormat('T',$FeldTeile[1]);
                $SQL .= ' AND ZUR_LIE_NR = '.$DB->FeldInhaltFormat('T',$FeldTeile[2]);
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
                }
            }
        }
    }


}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>