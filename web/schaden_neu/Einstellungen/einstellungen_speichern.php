<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('BES','BES_SPEICHERNOK');



try
{
	
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	
	if($_GET['cmdAktion'] == 'Einstellungen')
	{
		if(isset($_POST['txtCHK_SCHNELLSUCHE']))
		{
			$SchnellSuche = '1';
		}
		else 
		{
			$SchnellSuche = '0';
		}
		$AWISBenutzer->ParameterSchreiben('Schad_Schnellsuche',$SchnellSuche);
		
		for($i = 1; $i < 6;$i++)
		{
			
			if(isset($_POST['txtCHK_FRAME'.$i]))
			{
				$Frames[]='1';
			}
			else 
			{
				$Frames[]='0';
			}
		}
		$AWISBenutzer->ParameterSchreiben('Schad_Frame',serialize($Frames));
		
		$Form->Hinweistext($TXT_Speichern['BES']['BES_SPEICHERNOK']);
		
	}
	
}
catch (awisException $ex)
{
	
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_QMZ_ZUORD) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Form->Hinweistext('Der einzufügende Datensatz befindet sich schon in der Datenbank');
	}
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.PK_QMK_KEY) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 4;
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
