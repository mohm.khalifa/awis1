<?php
global $AWIS_KEY1;

require_once('awisFilialen.inc');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	
    $TextKonserven = array();
    $TextKonserven[]=array('RFM','RFM_AST_ATUNR');
    $TextKonserven[]=array('RFA','RFA_DATUM');
    $TextKonserven[]=array('RFA','RFA_LFDNR');
    $TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
    $TextKonserven[]=array('RFP','RFP_MINDESTBESTAND');
    $TextKonserven[]=array('FIB','FIB_BESTAND');
    
    $TextKonserven[]=array('FIL','FIL_ID');
    $TextKonserven[]=array('FIL','FIL_GEBIET');
    $TextKonserven[]=array('FIL','FIL_BEZ');
    $TextKonserven[]=array('FIB','FIB_BESTAND');
    
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','Soll');
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_RueckfuehrungOffen'));
	
	if (isset($_GET['cmdAktion']) AND ($_GET['cmdAktion']=='CSV'))
	{
		$Trenner=";";
	
		@ob_clean();
	
		//header("Cache-Control: no-cache, must-revalidate");
		//header("Expires: 01 Jan 2000");
		header('Pragma: public');
		header('Cache-Control: max-age=0');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="datenexport.csv"');
	
		//********************************************************
		// Bedingung erstellen
		//********************************************************
		$BindeVariablen = array();
		$Bedingung = _ExportBedingungErstellen($Param, $BindeVariablen);
			
		//*****************************************************************
		// Sortierung aufbauen
		//*****************************************************************
		if (isset($Param['ORDER']) and $Param['ORDER'] != '')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' rfm_fil_id, rfp_ast_atunr, rfa_nr DESC, rfa_lfdnr DESC';
		}
	
		$SQL  = 'SELECT';
		$SQL .= ' ANFDAT.*,';
		$SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILE';
		$SQL .= ' FROM ';
		$SQL .= ' (';
		$SQL .= ' SELECT ';
		$SQL .= ' RFM_FIL_ID, ';
		$SQL .= ' (SELECT FIL_BEZ';
		$SQL .= '  FROM FILIALEN';
		$SQL .= '  WHERE FIL_ID = RFM_FIL_ID) AS FIL_BEZ,';
		$SQL .= ' (SELECT XX1_EBENE';
		$SQL .= '  FROM V_FILIALPFAD';
		$SQL .= '  WHERE XX1_FIL_ID = RFM_FIL_ID) AS FEB_BEZEICHNUNG,';
		$SQL .= ' RFP_AST_ATUNR,';
		$SQL .= ' (SELECT AST_BEZEICHNUNGWW';
		$SQL .= '  FROM ARTIKELSTAMM';
		$SQL .= '  WHERE AST_ATUNR = RFP.RFP_AST_ATUNR) AS AST_BEZEICHNUNGWW,';
		//$SQL .= ' RFA_KEY,';
		$SQL .= ' RFA_NR,';
		$SQL .= ' RFA_LFDNR,';
		//$SQL .= ' RFA.RFA_DATUM,';
		//$SQL .= ' NVL (RFM_MENGE, 0) AS RFM_MENGE,';
		$SQL .= ' NVL (RFP_MINDESTBESTAND, 0) AS RFP_MINDESTBESTAND,';
		$SQL .= ' (SELECT NVL (BESTAND, 0)';
		$SQL .= '  FROM V_FILIALBESTAND_AKTUELL';
		$SQL .= '  WHERE AST_ATUNR = RFP.RFP_AST_ATUNR';
		$SQL .= '  AND FIL_ID = RFM.RFM_FIL_ID) AS FIL_BEST';
		$SQL .= ' FROM RUECKFUEHRUNGSANFPOSFILMENGEN RFM ';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSANFORDERUNGENPOS RFP';
		$SQL .= ' ON RFM.RFM_RFP_KEY = RFP.RFP_KEY';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSANFORDERUNGEN RFA';
		$SQL .= ' ON RFA.RFA_KEY = RFP.RFP_RFA_KEY';
		$SQL .= ' WHERE RFA.RFA_RFS_KEY					= 10';
		$SQL .= ' AND RFM.RFM_MENGE						> 0';
		$SQL .= ' AND NVL (RFP.RFP_MINDESTBESTAND, 0)	= 0';
		if($Bedingung!='')
		{
			$SQL .= ' AND ' . substr($Bedingung,4);
		}		
		$SQL .= ' ) ANFDAT';
		$SQL .= ' WHERE FIL_BEST > 0';
		$SQL .= ' ORDER BY ' .$ORDERBY;
			
		// Blockgr��e festlegen
		$Blockgroesse = 5000; 
	
		for($i=1;$i<999;$i++)
		{
			$BindeVariablen['var_N0_ZEILE_VON'] = $Form->Format('Z',($i-1)*$Blockgroesse,false);
			$BindeVariablen['var_N0_ZEILE_BIS'] = $Form->Format('Z',($i)*$Blockgroesse,false) ;
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZEILE >= :var_N0_ZEILE_VON AND ZEILE < :var_N0_ZEILE_BIS';
			
			$rsDaten = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	
			if($i==1)		// Beim ersten Mal den Kopf schreiben
			{
				$Felder = $rsDaten->SpaltenNamen();
				
				$Zeile  = $AWISSprachKonserven['FIL']['FIL_ID'];
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['FIL']['FIL_BEZ'];
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['FIL']['FIL_GEBIET'];
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['RFM']['RFM_AST_ATUNR'];
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'];			
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['RFA']['RFA_DATUM'];			
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['RFA']['RFA_LFDNR'];			
				$Zeile .= $Trenner;
				//$Zeile .= $AWISSprachKonserven['RFP']['RFP_MINDESTBESTAND'];			
				$Zeile .= $AWISSprachKonserven['Wort']['Soll'];			
				$Zeile .= $Trenner;
				$Zeile .= $AWISSprachKonserven['FIB']['FIB_BESTAND'];			
				$Zeile .= "\n";
				
				echo $Zeile;
			}
	
			while(!$rsDaten->EOF())
			{
				$Zeile = '';
				foreach($Felder AS $FeldName)
				{
					if($FeldName!='ZEILE')
					{
						$Text = str_replace("\n",'',$rsDaten->FeldInhalt($FeldName));
						$Text = str_replace("\r",'',$Text);
						$Text = str_replace("\t",'',$Text);
						$Zeile .= $Trenner.$Text;
					}
				}
				echo substr($Zeile,1)."\n";
				
				$rsDaten->DSWeiter();
			}
	
			if($rsDaten->AnzahlDatensaetze()==0)
			{
				break;
			}
		}
	}
}

catch (Exception $ex)
{
	die($ex->getMessage());
}

function _ExportBedingungErstellen($Param, &$BindeVariablen)
{
	global $Form;
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Recht3950;
	
	$Bedingung = '';
	if(isset($Param['DATUM_VOM']) AND $Param['DATUM_VOM']!='')
	{
		$Bedingung .= ' AND TRUNC(RFA.RFA_DATUM) >= :var_D_DATUM_VOM';
		$BindeVariablen['var_D_DATUM_VOM'] = $Form->Format('D',$Param['DATUM_VOM'],false);
	}
	
	if(isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS']!='')
	{
		$Bedingung .= ' AND TRUNC(RFA.RFA_DATUM) <= :var_D_DATUM_BIS';
		$BindeVariablen['var_D_DATUM_BIS'] = $Form->Format('D',$Param['DATUM_BIS'],false);
	}

	if(isset($Param['RFP_AST_ATUNR']) AND $Param['RFP_AST_ATUNR']!='')
	{
		$Bedingung .= ' AND RFP.RFP_AST_ATUNR = :var_T_RFP_AST_ATUNR';
		$BindeVariablen['var_T_RFP_AST_ATUNR'] = $Form->Format('T',$Param['RFP_AST_ATUNR'],true);
	}
	
	if(isset($Param['RFM_FIL_ID']) AND $Param['RFM_FIL_ID']!='')
	{
		$Bedingung .= ' AND rfm.rfm_fil_id = :var_N_RFM_FIL_ID';
		$BindeVariablen['var_N_RFM_FIL_ID'] = $Form->Format('N0',$Param['RFM_FIL_ID'],false);
	}

	if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET']!=0)
	{
		$Bedingung .= ' AND EXISTS (';
		$Bedingung .= '  SELECT *';
		$Bedingung .= '  FROM V_FILIALPFAD';
		$Bedingung .= '  WHERE REGEXP_LIKE (XX1_PFADKEYS, :var_N_FIL_GEBIET)';
		$Bedingung .= '  AND XX1_FIL_ID = RFM.RFM_FIL_ID)';
		$BindeVariablen['var_N_FIL_GEBIET'] = $Form->Format('N0',$Param['FIL_GEBIET'],false);
	}

	return $Bedingung;
}

?>

