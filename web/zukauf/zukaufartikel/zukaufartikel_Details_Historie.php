<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZAH','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','ZusammenfassungBestellungen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);		// Artikelstamm
	$Recht10002 = $AWISBenutzer->HatDasRecht(10002);		// Bestellungen
	if(($Recht10001&32)==0)
	{
	    $DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER,array(__FILE__,$AWISBenutzer->BenutzerName()),$AWISBenutzer->BenutzerName());
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZAH_ARCHIVDATUM DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = "SELECT ZAH_ZLA_KEY, TO_CHAR(ZAH_ARCHIVDATUM,'DD.MM.YYYY HH24:MI') AS ZAH_ARCHIVDATUMTXT, ZAH_ARCHIVDATUM";
	$SQL .= ', ZAH_WG1, ZAH_WG2, ZAH_WG3, ZAH_EK, ZAH_VK, ZAH_RABATT1, ZAH_RABATT2, ZAH_AST_ATUNR, ZAH_USER';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukauflieferantenartikelarchiv ';
	$SQL .= ' WHERE ZAH_ZLA_KEY=0'.$AWIS_KEY1;

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$Form->DebugAusgabe(1,$SQL);
	$rsZAH = $DB->RecordsetOeffnen($SQL);
	$Form->Formular_Start();

	if($rsZAH->AnzahlDatensaetze()>0)
	{
		$Form->ZeileStart();
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_ARCHIVDATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_ARCHIVDATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_ARCHIVDATUM'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_USER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_USER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_USER'],150,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_WG1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_WG1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_WG1'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_WG2'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_WG2'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_WG2'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_WG3'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_WG3'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_WG3'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_VK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_VK'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_VK'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_EK'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_EK'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_EK'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_RABATT1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_RABATT1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_RABATT1'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_RABATT2'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_RABATT2'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_RABATT2'],100,'',$Link);
		$Link = './zukaufartikel_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZAH_AST_ATUNR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZAH_AST_ATUNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZAH']['ZAH_AST_ATUNR'],150,'',$Link);

		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZAH->EOF())
		{
			$Form->ZeileStart();

			$Form->Erstelle_ListenFeld('*ZAH_ARCHIVDATUM',$rsZAH->FeldInhalt('ZAH_ARCHIVDATUMTXT'),20,150,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZAH_USER',$rsZAH->FeldInhalt('ZAH_USER'),20,150,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZAH_WG1',$rsZAH->FeldInhalt('ZAH_WG1'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZAH_WG2',$rsZAH->FeldInhalt('ZAH_WG2'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZAH_WG3',$rsZAH->FeldInhalt('ZAH_WG3'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZAH_VK',$rsZAH->FeldInhalt('ZAH_VK'),20,100,false,($DS%2),'','','N2');
			$Form->Erstelle_ListenFeld('*ZAH_EK',$rsZAH->FeldInhalt('ZAH_EK'),20,100,false,($DS%2),'','','N2');
			$Form->Erstelle_ListenFeld('*ZAH_RABATT1',$rsZAH->FeldInhalt('ZAH_RABATT1'),20,100,false,($DS%2),'','','N2');
			$Form->Erstelle_ListenFeld('*ZAH_RABATT2',$rsZAH->FeldInhalt('ZAH_RABATT2'),20,100,false,($DS%2),'','','N2');
			$Form->Erstelle_ListenFeld('*ZAH_AST_ATUNR',$rsZAH->FeldInhalt('ZAH_AST_ATUNR'),20,150,false,($DS%2),'','','T');

			$Form->ZeileEnde();

			$rsZAH->DSWeiter();
			$DS++;
		}


		$Link = './zukaufartikel_Main.php?cmdAktion=Details&Seite=Historie';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}


	$Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>