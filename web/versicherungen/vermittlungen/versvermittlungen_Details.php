<?php
global $VBA;
$DetailAnsicht = false;
$POSTbenutzen = true;

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$VBA->AWIS_KEY1 = '';


if (isset($_GET['VBA_KEY'])) {
    $VBA->AWIS_KEY1 = $_GET['VBA_KEY'];
} elseif (isset($_POST['txtVBA_KEY'])) {
    $VBA->AWIS_KEY1 = $_POST['txtVBA_KEY'];
}

if (isset($_POST['cmdDSNeu_x'])) { //Neuanlage?
    $POSTbenutzen = false;
    $VBA->AWIS_KEY1 = -1;
}
$VBA->Param['KEY'] = $VBA->AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $POSTbenutzen = false;
    $VBA->Param = array();
    $VBA->Param['VBA_KFZKZ'] = $_POST['sucVBA_KFZKZ'];
    $VBA->Param['VBA_VSNR'] = $_POST['sucVBA_VSNR'];
    $VBA->Param['VBA_SNR'] = $_POST['sucVBA_SNR'];
    $VBA->Param['VVE_KUERZEL'] = $_POST['sucVVE_KUERZEL'];
    $VBA->Param['VBA_AXZ'] = $_POST['sucVBA_AXZ'];
    $VBA->Param['VBA_ORDNUNGSNR'] = $_POST['sucVBA_ORDNUNGSNR'];

    $VBA->Param['KEY'] = '';
    $VBA->Param['WHERE'] = '';
    $VBA->Param['ORDER'] = '';
    $VBA->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern']) ? 'on' : '';
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include('./versvermittlungen_speichern.php');
} else { //User hat den Reiter gewechselt.
    $VBA->AWIS_KEY1 = $VBA->Param['KEY'];
}

//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************
if (!isset($_GET['Sort'])) {
    if (isset($VBA->Param['ORDER']) AND $VBA->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $VBA->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY VBA_ERSTELLDATUM DESC,VBA_KEY DESC';
        $VBA->Param['ORDER'] = ' VBA_ERSTELLDATUM DESC,VBA_KEY DESC ';
    }
} else {
    $VBA->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $VBA->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $VBA->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************

$SQL = ' SELECT';
$SQL .= ' VBA.VBA_KEY,';
$SQL .= ' VBA.VBA_DATEINAME,';
$SQL .= ' VBA.VBA_NACHRICHTENTYP,';
$SQL .= ' VBA.VBA_ORDNUNGSNR,';
$SQL .= ' VBA.VBA_ERSTELLDATUM,';
$SQL .= ' VBA.VBA_VUNUMMER,';
$SQL .= ' VBA.VBA_STATUS,';
$SQL .= ' VBA.VBA_AX_VORGANGNR,';
$SQL .= ' VBA.VBA_VSNR,';
$SQL .= ' VBA.VBA_SNR,';
$SQL .= ' VBA.VBA_AUFTRAGSBESCHREIBUNG,';
$SQL .= ' VBA.VBA_AKTENZEICHEN,';
$SQL .= ' VBA.VBA_SCHADENDATUM,';
$SQL .= ' VBA.VBA_SCHADENTYP,';
$SQL .= ' VBA.VBA_VORSTABZUGKZ,';
$SQL .= ' VBA.VBA_VNANREDE,';
$SQL .= ' VBA.VBA_VNNAME1,';
$SQL .= ' VBA.VBA_VNNAME2,';
$SQL .= ' VBA.VBA_VNNAME3,';
$SQL .= ' VBA.VBA_VNTITEL,';
$SQL .= ' VBA.VBA_VNLAENDERKZ,';
$SQL .= ' VBA.VBA_VNPLZ,';
$SQL .= ' VBA.VBA_VNORT,';
$SQL .= ' VBA.VBA_VNSTRASSE,';
$SQL .= ' VBA.VBA_VNKOMM1,';
$SQL .= ' VBA.VBA_VNKOMM2,';
$SQL .= ' VBA.VBA_KFZKZ,';
$SQL .= ' VBA.VBA_KFZHERSTELLER,';
$SQL .= ' VBA.VBA_KFZMODELL,';
$SQL .= ' VBA.VBA_KFZERSTZULASSUNG,';
$SQL .= ' VBA.VBA_KFZHSN,';
$SQL .= ' VBA.VBA_KFZTSN,';
$SQL .= ' VBA.VBA_KFZVIN,';
$SQL .= ' VBA.VBA_USER,';
$SQL .= ' VBA.VBA_USERDAT,';
$SQL .= ' VBA.VBA_KEEPALIVE,';
$SQL .= ' VBA.VBA_KEEPALIVE_USER,';
$SQL .= ' VBA.VBA_BEMERKUNG,';
$SQL .= ' VBA.VBA_BEMERKUNG_USER,';
$SQL .= ' VBA.VBA_BEMERKUNG_USERDAT,';
$SQL .= ' VBA.VBA_WUNSCHFILIALE,';
$SQL .= ' CASE WHEN (VBA_KEEPALIVE is null or (VBA_KEEPALIVE <= sysdate - 19*(1/24/60/60))) ';
$SQL .= ' then';
$SQL .= ' 0 else 1 end as Sperre,';
$SQL .= ' VVE.VVE_VERSNR,';
$SQL .= ' VVE.VVE_BEZEICHNUNG,';
$SQL .= ' VDV.VDV_ERFASS_MANUELL,';
$SQL .= ' row_number() over (' . $ORDERBY . ') AS ZeilenNr';
$SQL .= ' FROM VERSBEAUFTRAGUNGEN VBA';
$SQL .= ' INNER JOIN VERSDIREKTVERMITTLUNGREF VDV';
$SQL .= ' ON VDV.VDV_VU_NUMMER = VBA.VBA_VUNUMMER';
$SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
$SQL .= ' ON VVE.VVE_VERSNR = VDV.VDV_VERS_NR';

if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($VBA->AWIS_KEY1 == '') { //Liste?


    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $VBA->Form->Format('N0', $_REQUEST['Block'], false);
        $VBA->Param['BLOCK'] = $Block;
    } elseif (isset($VBA->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $VBA->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $VBA->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $VBA->DB->ErmittleZeilenAnzahl($SQL, $VBA->DB->Bindevariablen('VBA', false));

    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $VBA->DB->WertSetzen('VBA', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $VBA->DB->WertSetzen('VBA', 'N0', ($StartZeile + $ZeilenProSeite));
}


//********************************************************
// Fertigen SQL ausf�hren
//********************************************************

$rsVBA = $VBA->DB->RecordsetOeffnen($SQL, $VBA->DB->Bindevariablen('VBA', true));
$VBA->Form->DebugAusgabe(1, $VBA->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$VBA->Form->Formular_Start();
echo '<form name=frmVBA action=./versvermittlungen_Main.php?cmdAktion=Details method=POST>';

if ($rsVBA->EOF() and $VBA->AWIS_KEY1 == '') { //Nichts gefunden!
    $VBA->Form->Hinweistext($VBA->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsVBA->AnzahlDatensaetze() > 1) {// Liste

    $FeldBreiten = array();
    $FeldBreiten['VBA_VU'] = 210;
    $FeldBreiten['VBA_KFZKZ'] = 150;
    $FeldBreiten['VBA_VSNR'] = 200;
    $FeldBreiten['VBA_SNR'] = 150;
    $FeldBreiten['VBA_ERSTELLDATUM'] = 150;
    $FeldBreiten['VBA_AX_VORGANGNR'] = 200;
    $FeldBreiten['VBA_USER'] = 100;
    $FeldBreiten['VBA_BEMERKUNG'] = 200;

    $VBA->Form->ZeileStart();

    $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VVE_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVE_BEZEICHNUNG')) ? '~' : '');
    $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_VU'], $FeldBreiten['VBA_VU'], '', $Link);

    $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VBA_KFZKZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VBA_KFZKZ')) ? '~' : '');
    $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_KFZKZ'], $FeldBreiten['VBA_KFZKZ'], '', $Link);

    $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VBA_VSNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VBA_VSNR')) ? '~' : '');
    $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_VSNR'], $FeldBreiten['VBA_VSNR'], '', $Link);

    $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VBA_SNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VBA_SNR')) ? '~' : '');
    $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_SNR'], $FeldBreiten['VBA_SNR'], '', $Link);

    $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VBA_ERSTELLDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VBA_ERSTELLDATUM')) ? '~' : '');
    $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_ERSTELLDATUM'], $FeldBreiten['VBA_ERSTELLDATUM'], '', $Link);

    $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VBA_AX_VORGANGNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VBA_AX_VORGANGNR')) ? '~' : '');
    $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_AX_VORGANGNR'], $FeldBreiten['VBA_AX_VORGANGNR'], '', $Link);

    if (($VBA->Recht9800 & 8) == 8) {
        $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $Link .= '&Sort=VBA_USER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VBA_USER')) ? '~' : '');
        $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_USER'], $FeldBreiten['VBA_USER'], '', $Link);
    }

    $Link = './versvermittlungen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VBA_BEMERKUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VBA_BEMERKUNG')) ? '~' : '');
    $VBA->Form->Erstelle_Liste_Ueberschrift($VBA->AWISSprachKonserven['VBA']['VBA_BEMERKUNG'], $FeldBreiten['VBA_BEMERKUNG'], '', $Link);

    $VBA->Form->ZeileEnde();

    $DS = 0;
    while (!$rsVBA->EOF()) {
        $HG = ($DS % 2);
        $VBA->Form->ZeileStart();
        $Link = './versvermittlungen_Main.php?cmdAktion=Details&VBA_KEY=0' . $rsVBA->FeldInhalt('VBA_KEY') . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $VBA->Form->Erstelle_ListenFeld('VBA_VU', $rsVBA->FeldInhalt('VVE_BEZEICHNUNG'), 0, $FeldBreiten['VBA_VU'], false, $HG, '', $Link, 'T');
        $VBA->Form->Erstelle_ListenFeld('VBA_KFZKZ', $rsVBA->FeldInhalt('VBA_KFZKZ'), 0, $FeldBreiten['VBA_KFZKZ'], false, $HG, '', '', 'T', 'L');
        $VBA->Form->Erstelle_ListenFeld('VBA_VSNR', $rsVBA->FeldInhalt('VBA_VSNR'), 0, $FeldBreiten['VBA_VSNR'], false, $HG, '', '', 'T', 'L');
        $VBA->Form->Erstelle_ListenFeld('VBA_SNR', $rsVBA->FeldInhalt('VBA_SNR'), 0, $FeldBreiten['VBA_SNR'], false, $HG, '', '', 'T', 'L');
        $VBA->Form->Erstelle_ListenFeld('VBA_ERSTELLDATUM', $rsVBA->FeldInhalt('VBA_ERSTELLDATUM'), 0, $FeldBreiten['VBA_ERSTELLDATUM'], false, $HG, '', '', 'D', 'L');
        $VBA->Form->Erstelle_ListenFeld('VBA_AX_VORGANGNR', $rsVBA->FeldInhalt('VBA_AX_VORGANGNR'), 0, $FeldBreiten['VBA_AX_VORGANGNR'], false, $HG, '', '', 'T', 'L');
        if (($VBA->Recht9800 & 8) == 8) {
            $VBA->Form->Erstelle_ListenFeld('VBA_USER', $rsVBA->FeldInhalt('VBA_USER'), 0, $FeldBreiten['VBA_USER'], false, $HG, '', '', 'T', 'L');
        }
        $VBA->Form->Erstelle_ListenFeld('VBA_BEMERKUNG', $rsVBA->FeldInhalt('VBA_BEMERKUNG'), 0, $FeldBreiten['VBA_BEMERKUNG'], false, $HG, '', '', 'T', 'L',$rsVBA->FeldInhalt('VBA_BEMERKUNG'));
        $VBA->Form->ZeileEnde();

        $rsVBA->DSWeiter();
        $DS++;
    }

    $Link = './versvermittlungen_Main.php?cmdAktion=Details';
    $VBA->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
} else { //Ein Datensatz

    if($rsVBA->AnzahlDatensaetze()==1){
        $VBA->AWIS_KEY1 = $rsVBA->FeldInhalt('VBA_KEY');
    }
    $edit = false;
    $lock = false;
    if ($rsVBA->FeldInhalt('VDV_ERFASS_MANUELL') == 1 or $VBA->AWIS_KEY1 == -1) {
        $edit = true;
    }

    if(strtolower($rsVBA->FeldInhalt('VBA_KEEPALIVE_USER')) != strtolower($VBA->AWISBenutzer->BenutzerName()) and $rsVBA->FeldInhalt('SPERRE') == '1') {
        $Hinweistext = str_replace('#USER#',$rsVBA->FeldInhalt('VBA_KEEPALIVE_USER'),$VBA->AWISSprachKonserven['VBA']['VBA_LOCKMESSAGE']);
        $VBA->Form->Hinweistext($Hinweistext,5);
        $edit = false;
        $lock = true;
    }

    if ($VBA->AWIS_KEY1 != -1 and $lock == false) {
        $SQL = 'UPDATE VERSBEAUFTRAGUNGEN';
        $SQL .= ' SET VBA_KEEPALIVE = sysdate ,';
        $SQL .= ' VBA_KEEPALIVE_USER = ' . $VBA->DB->WertSetzen('VBA', 'T', $VBA->AWISBenutzer->BenutzerName());
        $SQL .= ' WHERE VBA_KEY = ' . $VBA->DB->WertSetzen('VBA', 'Z', $VBA->AWIS_KEY1);

        $VBA->DB->Ausfuehren($SQL, '', true, $VBA->DB->Bindevariablen('VBA'));

        echo '<script src="keepAlive.js"></script>';
    }


    $DetailAnsicht = true;
    $VBA->Form->Erstelle_HiddenFeld('VBA_KEY', $VBA->AWIS_KEY1);
    $VBA->Form->Erstelle_HiddenFeld('VDV_ERFASS_MANUELL',$rsVBA->FeldInhalt('VDV_ERFASS_MANUELL'));


    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./versvermittlungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $VBA->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVBA->FeldInhalt('VBA_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsVBA->FeldInhalt('VBA_USERDAT'));
    $VBA->Form->InfoZeile($Felder, '');

    //Versicherungsnehmer
    $VBA->Form->FormularBereichStart();
    $VBA->Form->FormularBereichInhaltStart('Allgemein', true, 10);

    if ($VBA->AWIS_KEY1 != -1) {
        $VBA->Form->ZeileStart();
        $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_ERFASSUNG'] . ':', 200);
        $VBA->Form->Erstelle_TextFeld('VBA_ERFASSUNG', $rsVBA->FeldOderPOST('VDV_ERFASS_MANUELL', 'T') == '0' ? 'elektronisch' : 'manuell', 30, 250, false, '', '', '', 'T');
        $VBA->Form->ZeileEnde();

        if ($rsVBA->FeldInhalt('VDV_ERFASS_MANUELL') == 0) {
            $VBA->Form->ZeileStart();
            $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_DATEINAME'] . ':', 200);
            $VBA->Form->Erstelle_TextFeld('VBA_DATEINAME', $rsVBA->FeldOderPOST('VBA_DATEINAME', 'T'), 30, 250, false, '', '', '', 'T');
            $VBA->Form->ZeileEnde();
        }

        $VBA->Form->ZeileStart();
        $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_STATUS'] . ':', 200);
        $VBA->Form->Erstelle_TextFeld('VBA_STATUS', $rsVBA->FeldOderPOST('VBA_STATUS', 'T') == 'CLOSED' ? 'Geschlossen' : 'Offen', 20, 200, false, '', '', '', 'T');
        $VBA->Form->ZeileEnde();

        $VBA->Form->ZeileStart();
        $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_AX_VORGANGNR'] . ':', 200);
        $VBA->Form->Erstelle_TextFeld('VBA_AX_VORGANGNR', $rsVBA->FeldOderPOST('VBA_AX_VORGANGNR', 'T') == '' ? 'Noch nicht zugeordnet' : $rsVBA->FeldOderPOST('VBA_AX_VORGANGNR', 'T'), 20, 200, false, '', '', '', 'T');
        $VBA->Form->ZeileEnde();

        $VBA->Form->ZeileStart();
        $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_WUNSCHFILIALE'] . ':', 200);
        $VBA->Form->Erstelle_TextFeld('VBA_WUNSCHFILIALE', $rsVBA->FeldOderPOST('VBA_WUNSCHFILIALE', 'T'), 20, 200, false, '', '', '', 'T');
        $VBA->Form->ZeileEnde();
    }

    $VBA->Form->FormularBereichInhaltEnde();
    $VBA->Form->FormularBereichEnde();

    $VBA->Form->FormularBereichStart();
    $VBA->Form->FormularBereichInhaltStart('Versicherungsinfos', true, 10);

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VERSBEZ'] . ':', 200);

    if ($VBA->AWIS_KEY1 == -1) {
        $SQL = 'SELECT';
        $SQL .= ' VDV.VDV_VU_NUMMER,';
        $SQL .= ' VVE.VVE_VERSNR || \' - \' || VVE.VVE_BEZEICHNUNG FROM VERSDIREKTVERMITTLUNGREF VDV';
        $SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
        $SQL .= ' ON VVE.VVE_VERSNR = VDV.VDV_VERS_NR';
        $SQL .= ' WHERE VDV_ERFASS_MANUELL = 1';

        $VBA->Form->Erstelle_SelectFeld('VBA_VU', '', 400, true, $SQL, '', '');
    }
    $VBA->Form->Erstelle_TextFeld('VBA_VERSBEZ', $rsVBA->FeldOderPOST('VVE_BEZEICHNUNG', 'T', $POSTbenutzen), 25, 250, false, 'font-weight: bold');
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_SB'] . ':', 200);
    
    $Daten = explode("|", $VBA->AWISSprachKonserven['VBA']['VBA_LST_SB']);

    if(isset($_POST['txtVBA_AUFTRAGSBESCHREIBUNG'])) {
        $VBA->Form->Erstelle_SelectFeld('VBA_AUFTRAGSBESCHREIBUNG', $_POST['txtVBA_AUFTRAGSBESCHREIBUNG'], '100', $edit, '', '', '', '', '', $Daten, '', '', [], 'height:22px');
    } else {
        $VBA->Form->Erstelle_SelectFeld('VBA_AUFTRAGSBESCHREIBUNG', intval(substr($rsVBA->FeldInhalt('VBA_AUFTRAGSBESCHREIBUNG'), 70, 5)), '100', $edit, '', '', '', '', '', $Daten, '', '', [], 'height:22px');
    }
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VSNR'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VSNR', $rsVBA->FeldOderPOST('VBA_VSNR', 'T', $POSTbenutzen), 25, 200, $edit, '');
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_SNR'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_SNR', $rsVBA->FeldOderPOST('VBA_SNR', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_ORDNUNGSNR']. ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_ORDNUNGSNR', $rsVBA->FeldOderPOST('VBA_ORDNUNGSNR', 'T', $POSTbenutzen), 25, 300, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->FormularBereichInhaltEnde();
    $VBA->Form->FormularBereichEnde();


    //Versicherungsnehmer
    $VBA->Form->FormularBereichStart();
    $VBA->Form->FormularBereichInhaltStart('Versicherungsnehmer', true, 10);

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VNNAME3'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VNNAME3', $rsVBA->FeldOderPOST('VBA_VNNAME3', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VNNAME1'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VNNAME1', $rsVBA->FeldOderPOST('VBA_VNNAME1', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VNSTRASSE'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VNSTRASSE', $rsVBA->FeldOderPOST('VBA_VNSTRASSE', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VNPLZ'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VNPLZ', $rsVBA->FeldOderPOST('VBA_VNPLZ', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VNORT'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VNORT', $rsVBA->FeldOderPOST('VBA_VNORT', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VNKOMM1'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VNKOMM1', $rsVBA->FeldOderPOST('VBA_VNKOMM1', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VNKOMM2'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_VNKOMM2', $rsVBA->FeldOderPOST('VBA_VNKOMM2', 'T', $POSTbenutzen), 25, 200, $edit);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VORSTABZUG'] . ':', 200);

    $Daten = explode("|", '0~nein|1~ja');
    $VBA->Form->Erstelle_SelectFeld('VBA_VORSTABZUGKZ', $rsVBA->FeldOderPOST('VBA_VORSTABZUGKZ', 'T', $POSTbenutzen), '100', $edit, '', '', '', '', '', $Daten, '', '', [], 'height:22px');
    $VBA->Form->ZeileEnde();

    $VBA->Form->FormularBereichInhaltEnde();
    $VBA->Form->FormularBereichEnde();


    //Versicherungsnehmer
    $VBA->Form->FormularBereichStart();
    $VBA->Form->FormularBereichInhaltStart('KFZ-Daten', true, 10);

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_KFZKZ'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_KFZKZ', $rsVBA->FeldOderPOST('VBA_KFZKZ', 'T', $POSTbenutzen), 10, 110, $edit, 'font-weight: bold;', 'height:24px', '', 'T', 'L', 'KFZ-Kennzeichen im Format XX-X 1234', '', 0, '', '', '');
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VIN'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_KFZVIN', $rsVBA->FeldOderPOST('VBA_KFZVIN', 'T',$POSTbenutzen), 25, 200, $edit, '', '', '', 'T', 'L', 'VIN Max 17 Stellen', '', 17);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_KFZHSN'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_KFZHSN', $rsVBA->FeldOderPOST('VBA_KFZHSN', 'T',$POSTbenutzen), 25, 200, $edit, '', '', '', 'T', 'L', '', '', 17);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_KFZTSN'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_KFZTSN', $rsVBA->FeldOderPOST('VBA_KFZTSN', 'T',$POSTbenutzen), 25, 200, $edit, '', '', '', 'T', 'L', '', '', 17);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_SCHADENDATUM'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_SCHADENDATUM', $rsVBA->FeldOderPOST('VBA_SCHADENDATUM', 'D', $POSTbenutzen), 10, 110, $edit, '', '', '', 'D');
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_ERSTZULASSUNG'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('VBA_KFZERSTZULASSUNG', $rsVBA->FeldOderPOST('VBA_KFZERSTZULASSUNG', 'D', $POSTbenutzen), 10, 110, $edit, '', '', '', 'D');
    $VBA->Form->ZeileEnde();

    $VBA->Form->FormularBereichInhaltEnde();
    $VBA->Form->FormularBereichEnde();

    if ($VBA->AWIS_KEY1 != -1) {
        $VBA->Form->FormularBereichStart();
        $VBA->Form->FormularBereichInhaltStart('Bemerkung', true, 10);

        $Felder = [];
        $Felder[] = [
            'Style'  => 'font-size:smaller;',
            'Inhalt' => ""
        ];
        $Felder[] =
            [
                'Style'  => 'font-size:smaller;',
                'Inhalt' => $rsVBA->FeldInhalt('VBA_BEMERKUNG_USER')
            ];
        $Felder[] =
            [
                'Style'  => 'font-size:smaller;',
                'Inhalt' => $rsVBA->FeldInhalt('VBA_BEMERKUNG_USERDAT')
            ];
        $VBA->Form->InfoZeile($Felder, '');

        $VBA->Form->ZeileStart();
        $VBA->Form->Erstelle_Textarea('VBA_BEMERKUNG', $rsVBA->FeldOderPOST('VBA_BEMERKUNG', 'T', $POSTbenutzen), 750, 90, 3, true);
        $VBA->Form->ZeileEnde();

        $VBA->Form->FormularBereichInhaltEnde();
        $VBA->Form->FormularBereichEnde();
    }
}

//***************************************
// Schaltflaechen fuer dieses Register
//***************************************
$VBA->Form->Formular_Ende();

$VBA->Form->SchaltflaechenStart();

$VBA->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $VBA->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

if (($VBA->Recht9800 & 2) == 2 AND $VBA->AWIS_KEY1 != '') { //Speichern erlaubt + in einen Datensatz?
    $VBA->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $VBA->AWISSprachKonserven['Wort']['lbl_speichern'], 'S', '', 27, 27, '', true);
}

if (($VBA->Recht9800 & 2) == 2 AND $VBA->AWIS_KEY1 != -1) {// Hinzuf�gen erlaubt + nicht gerade in einer Neuanlage?
    $VBA->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $VBA->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}
if (($VBA->Recht9800 & 2) == 2 AND $VBA->AWIS_KEY1 != -1 AND $VBA->AWIS_KEY1 == '') {
    $VBA->Form->Schaltflaeche('image', 'cmdExportXLSX', '', '/bilder/cmd_export_xls.png', $VBA->AWISSprachKonserven['Wort']['lbl_export']);
}

$VBA->Form->SchaltflaechenEnde();

$VBA->Form->SchreibeHTMLCode('</form>');

?>