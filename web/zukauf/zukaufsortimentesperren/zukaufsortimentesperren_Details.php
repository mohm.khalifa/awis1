<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once 'zukaufsortimentesperren_funktionen.inc';
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZSS','%');
	$TextKonserven[]=array('ZLH','%');
    $TextKonserven[]=array('ZAL','ZAL_BESTELLNUMMER');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

    $Funktionen = new zukaufsortimentesperren_funktionen();

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZSS'));

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10065 = $AWISBenutzer->HatDasRecht(10065);
	if($Recht10065==0)
	{
	    awisEreignis(3,1000,'Zukaufsortimentesperren',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$awisWerkzeug = new awisWerkzeuge();
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['ZSS_ZLH_KEY'] = $_POST['sucZSS_ZLH_KEY'];
		$Param['ZSS_SORTIMENT'] = $_POST['sucZSS_SORTIMENT'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./zukaufsortimentesperren_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufsortimentesperren_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZSS'));
	}
	elseif(isset($_GET['ZSS_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZSS_KEY']);
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	
	if(!isset($_GET['Sort']))
	{
		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZSS_SORTIMENT DESC';
			$Param['ORDER']='ZSS_SORTIMENT DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$BindeVariablen = array();
	$Bedingung = _BedingungErstellen($Param, $BindeVariablen);

	$SQL = 'SELECT Zukaufsortimentesperren.*, ZLH_KUERZEL, ZLH_BEZEICHNUNG';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukaufsortimentesperren';
	$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZSS_ZLH_KEY = ZLH_KEY';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	$Form->DebugAusgabe(1,$SQL);

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $BindeVariablen);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsZSS = $DB->RecordsetOeffnen($SQL, $BindeVariablen);


	$AWISBenutzer->ParameterSchreiben('Formular_ZSS',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmZukaufSortimenteSperren action=./zukaufsortimentesperren_Main.php?cmdAktion=Details method=POST>';

	if($rsZSS->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZSS->AnzahlDatensaetze()>1 AND !isset($_GET['ZSS_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './zukaufsortimentesperren_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZSS_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSS_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSS']['ZSS_BEZEICHNUNG'],200,'',$Link);
		$Link = './zukaufsortimentesperren_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZSS_SORTIMENT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSS_SORTIMENT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSS']['ZSS_SORTIMENT'],150,'',$Link);
		$Link = './zukaufsortimentesperren_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZSS_DATUMVOM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSS_DATUMVOM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSS']['ZSS_DATUMVOM'],130,'',$Link);
		$Link = './zukaufsortimentesperren_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZSS_DATUMBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZSS_DATUMBIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSS']['ZSS_DATUMBIS'],130,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZSS->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './zukaufsortimentesperren_Main.php?cmdAktion=Details&ZSS_KEY=0'.$rsZSS->FeldInhalt('ZSS_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZSS_BEZEICHNUNG',$rsZSS->FeldInhalt('ZSS_BEZEICHNUNG'),0,200,false,($DS%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('ZSS_SORTIMENT',$rsZSS->FeldInhalt('ZSS_SORTIMENT'),0,150,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('ZSS_DATUMVOM',$rsZSS->FeldInhalt('ZSS_DATUMVOM'),0,130,false,($DS%2),'','','D','L');
			$Form->Erstelle_ListenFeld('ZSS_DATUMBIS',$rsZSS->FeldInhalt('ZSS_DATUMBIS'),0,130,false,($DS%2),'','','D','L');

			$Form->ZeileEnde();

			$rsZSS->DSWeiter();
			$DS++;
		}

		$Link = './zukaufsortimentesperren_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	else //Ein DS
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZSS->FeldInhalt('ZSS_KEY');

		$Param['KEY']=$AWIS_KEY1;

		$Form->Erstelle_HiddenFeld('ZSS_KEY', $AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

        // Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufsortimentesperren_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZSS->FeldInhalt('ZSS_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZSS->FeldInhalt('ZSS_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10065&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10065&6);
		}

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_BEZEICHNUNG'].':',170);
        $Form->Erstelle_TextFeld('!ZSS_BEZEICHNUNG',$rsZSS->FeldInhalt('ZSS_BEZEICHNUNG'),10,600,$EditRecht,'','','','T');
        $Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_SORTIMENT'].':',170);
        $SQL = ' select ast_atunr, ast_atunr || \' - \' || AST_BEZEICHNUNGww as anzeige from artikelstamm where ast_atunr like \'ZUK___\'';
        $Form->Erstelle_SelectFeld('~ZSS_SORTIMENT',$rsZSS->FeldInhalt('ZSS_SORTIMENT'),220,$EditRecht,$SQL,$OptionBitteWaehlen,'','','',array());
		$Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_ZLH_KEY'].':',170);
        $SQL = "SELECT ZLH_KEY, ZLH_kuerzel || ' - ' || ZLH_BEZEICHNUNG as anzeige ";
        $SQL .= " FROM zukauflieferantenhersteller where ZLH_KEY is not null ORDER BY 2";
        $Form->Erstelle_SelectFeld('~ZSS_ZLH_KEY',$rsZSS->FeldInhalt('ZSS_ZLH_KEY'),220,$EditRecht,$SQL,$OptionBitteWaehlen,'','','');
        $Form->ZeileEnde();



        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_LIE_NR'].':',170);
        $SQL = "select LIE_NR, LIE_NR||' - ' || LIE_NAME1  ";
        $SQL .= " from lieferanten ORDER BY LIE_NAME1";
        $Form->Erstelle_SelectFeld('~ZSS_LIE_NR',$rsZSS->FeldInhalt('ZSS_LIE_NR'),220,$EditRecht,$SQL,$OptionBitteWaehlen,'','','',array());
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZAL']['ZAL_BESTELLNUMMER'].':',170);
        $Form->Erstelle_TextFeld('ZSS_ZAL_BESTELLNUMMER',$rsZSS->FeldInhalt('ZSS_ZAL_BESTELLNUMMER'),15,150,$EditRecht,'','','','T');

        $Form->ZeileEnde();


        $Form->ZeileStart();
        $Form->AuswahlBox('sperrAjax','box1','','ZSS_Sperrzuordnung','txtZSS_XTN_KUERZEL,txtZSS_XXX_KEY_Ursprung',10,10,'','T',true,'','','display:none','','','','',0,'display:none');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_XTN_KUERZEL'].':',170);
        $Daten = explode('|',$AWISSprachKonserven['ZSS']['ZSS_LST_XTN_KUERZEL']);
        $Form->Erstelle_SelectFeld('ZSS_XTN_KUERZEL',$rsZSS->FeldInhalt('ZSS_XTN_KUERZEL'),200,$EditRecht,'',$OptionBitteWaehlen,'','','',$Daten,'onchange="key_sperrAjax(event);"');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_HiddenFeld('ZSS_XXX_KEY_Ursprung',$rsZSS->FeldInhalt('ZSS_XXX_KEY'));
        ob_start();
        $Funktionen->erstelleSperrzuordnungsAjax($rsZSS->FeldInhalt('ZSS_XTN_KUERZEL'),$rsZSS->FeldInhalt('ZSS_XXX_KEY'));
        $Inhalt = ob_get_clean();
        
        $Form->AuswahlBoxHuelle('box1','','',$Inhalt);

       $Form->ZeileEnde();



		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_DATUMVOM'].':',170);
		$Form->Erstelle_TextFeld('ZSS_DATUMVOM',$rsZSS->FeldInhalt('ZSS_DATUMVOM'),10,600,$EditRecht,'','','','D','','',date('d.m.Y'));
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSS']['ZSS_DATUMBIS'].':',170);
		$Form->Erstelle_TextFeld('ZSS_DATUMBIS',$rsZSS->FeldInhalt('ZSS_DATUMBIS'),10,600,$EditRecht,'','','','D','','','31.12.2033');
		$Form->ZeileEnde();


		$Form->Formular_Ende();
	}

	//awis_Debug(1, $Param, $Bedingung, $rsZSS, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht10065&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht10065&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht10065&8) == 8 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
    $AWISBenutzer->ParameterSchreiben('Formular_ZSS',serialize($Param));
    
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param, &$Bindevariablen)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Form = new awisFormular();
	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		//$Bedingung.= ' AND ZSS_KEY = '.floatval($AWIS_KEY1);
		$Bedingung .= ' AND ZSS_KEY = :var_N0_zub_key ';
		$Bindevariablen['var_N0_zub_key'] = $AWIS_KEY1;
		return $Bedingung;
	}

	if(isset($Param['ZSS_ZLH_KEY']) AND $Param['ZSS_ZLH_KEY']>0)
	{
		$Bedingung .= ' AND EXISTS (SELECT * FROM ZUKAUFHERSTELLERCODES';
		$Bedingung .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLH_KEY = ZHK_ZLH_KEY';
		$Bedingung .= ' WHERE ZLH_KEY = ' . $DB->FeldInhaltFormat('N0',$Param['ZSS_ZLH_KEY']);
		$Bedingung .= ' AND ZHK_CODE = ZSS_HERSTELLER';
		$Bedingung .= ' )';
	}

	if(isset($Param['ZSS_SORTIMENT']) AND $Param['ZSS_SORTIMENT']!='')
	{
		//$Bedingung .= ' AND ZSS_SORTIMENT = ' . $DB->FeldInhaltFormat('T',$Param['ZSS_SORTIMENT']) . ' ';
		$Bindevariablen['var_T_ZSS_SORTIMENT']=$Param['ZSS_SORTIMENT'];
		$Bedingung .= ' AND ZSS_SORTIMENT = :var_T_ZSS_SORTIMENT ';
	}

	$Param['WHERE']=$Bedingung;
	return $Bedingung;
}
?>