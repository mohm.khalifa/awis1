<?php
require_once 'awisBerichte.inc';

class ber_PraktikantenMappe
    extends awisBerichte
{
    private $pdf_vorlage = '';

    /**
     * Prozedur zum Initialisieren des Berichts
     *
     * @see awisBerichte::init()
     */
    public function init(array $Parameter)
    {
        // Daten für den Bericht laden
        $SQL = "";
        $SQL .= "SELECT * FROM v_praktikumsvereinbarung";

        // Alle Parameter auswerten
        // Format $Parameter[<Name>] = "<Vergleichsoperator>~<Wert>"
        //			Beispiel: $Parameter['KEY_ZRU_KEY']="=~27"
        $Bedingung = "";
        if (isset($Parameter['PVS_KEY'])) {
            $Param = explode('~', $Parameter['PVS_KEY']);
            $Bedingung .= " AND pVs_key " . self::_VergleichsOperatorPruefen($Param[0]) . " " . $this->_DB->WertSetzen('PVS', 'N0', $Param[1]);
        }

        if ($Bedingung != "") {
            $SQL .= " WHERE " . substr($Bedingung, 4);
        }

        // Daten laden

        $this->_rec = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PVS'));

        // Alle Textbausteine laden
        $TextKonserven[] = array('PVS', '*');
        $TextKonserven[] = array('Fehler', 'err_KeineDaten');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    /**
     * Eiegntliches PDF erzegen
     *
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {
        // PDF Attribute setzen
        $this->SetTitle($this->_BerichtsName);
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->AliasNbPages();
        $this->SetFillColor(192, 192, 192);

        $this->ErstelleDoc01();
        $this->ErstelleDoc02();
        $this->ErstelleDoc03();
        $this->ErstelleDoc04();
        $this->ErstelleDoc05();

        $this->FertigStellen($Ausgabeart);
    }


    private function ErstelleDoc01()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(1);
        $this->useTemplate($pdf_vorlage);
        $this->_Spalte = $this->_Parameter['SpalteSeite1'];
        $this->_Zeile = $this->_Parameter['StartSeite1'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_NAME'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_VORNAME'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.1;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleZeitraum(), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_ZEITVON') . 'Uhr - ' . $this->_rec->FeldInhalt('P_ZEITBIS') . 'Uhr', 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(1), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(2), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(3), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.4;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleTaetigkeitbezeichnung(), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . ',' . $this->_rec->FeldInhalt('B_VORNAME'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 1.1;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(4), 0, 0, 'L');
    }

    private function ErstelleDoc02()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(2);
        $this->useTemplate($pdf_vorlage);
        $this->_Spalte = $this->_Parameter['SpalteSeite2'];
        $this->_Zeile = $this->_Parameter['StartSeite2'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftklein']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(4), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] - 3.8;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(6), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] - 3.9;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(5), 0, 0, 'L');

        $this->_Spalte = $this->_Parameter['BetreuerSeite2'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftklein'] + 2);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . ',' . $this->_rec->FeldInhalt('B_VORNAME'), 0, 0, 'L');
    }

    private function ErstelleDoc03()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(3);
        $this->useTemplate($pdf_vorlage);
    }

    private function ErstelleDoc04()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(4);
        $this->useTemplate($pdf_vorlage);

        $this->_Spalte = $this->_Parameter['SpalteSeite4'];
        $this->_Zeile = $this->_Parameter['StartSeite4'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftklein']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_NAME') . ',' . $this->_rec->FeldInhalt('P_VORNAME'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] - 3.4;
        $this->SetXY($this->_Spalte - 13, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'],
        $this->HoleZeitraum() . ' ' . $this->_rec->FeldInhalt('P_ZEITVON') . ' - ' . $this->_rec->FeldInhalt('P_ZEITBIS') . 'Uhr', 0, 0, 'L');

        $this->_Spalte = $this->_Parameter['Spalte2Seite4'];
        $this->_Zeile = $this->_Parameter['StartSeite4'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftklein']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('F_NR'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] - 3.4;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleTaetigkeitbezeichnung(), 0, 0, 'L');
    }

    private function ErstelleDoc05()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(5);
        $this->useTemplate($pdf_vorlage);
    }


    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage();
    }

    public function FertigStellen($Ausgabeart)
    {
        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    private function HoleFiliale($Line = 1)
    {
        $Filiale = '';
        switch ($Line) {
            case 1:
                $Filiale = $this->_rec->Feldinhalt('F_BEZ');
                break;
            case 2:
                $Filiale = $this->_rec->Feldinhalt('F_STRASSE');
                break;
            case 3:
                $Filiale = $this->_rec->Feldinhalt('F_PLZ') . " " . $this->_rec->Feldinhalt('F_ORT');
                break;
            case 4:
                $Filiale = $this->_rec->Feldinhalt('F_TEL');
                break;
            case 5:
                $Filiale = $this->_rec->Feldinhalt('F_MAIL');
                break;
            case 6:
                $Filiale = $this->_rec->Feldinhalt('F_FAX');
                break;
            default:
                $Filiale = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt'];
                break;
        }

        return $Filiale;
    }

    private function HoleZeitraum()
    {
        if ($this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) == $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS'))) {
            $Zeitraum = $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . " ";
        } else {
            $Zeitraum = $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . " - " . $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS'));
        }

        return $Zeitraum;
    }

    public function HoleTaetigkeitbezeichnung()
    {
        $Taetigkeit = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt'];
        $Daten = explode('|', $this->_AWISSprachkonserven['PVS']['lst_Taet']);
        foreach ($Daten AS $Felder) {
            $Feld = explode('~', $Felder);
            if ($this->_rec->FeldInhalt('PVS_TAETIGKEIT') == $Feld[0]) {
                $Taetigkeit = $Feld[1];
                break;
            }
        }

        return $Taetigkeit;
    }
}

?>