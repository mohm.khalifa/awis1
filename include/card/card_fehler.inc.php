<?php

require_once("card/card_funktionen.inc.php");

//Wird bei einem Fehler aufgerufen (anstatt ... or die(...);)

function fehler($strFehlerart,$strFehlertext,$strSql='',$intZeile=null)
{
	global $PHP_SELF;
	global $PHP_AUTH_USER;
	
	$strFehlerausgabe="<script language=\"JavaScript\">var parameter = \"intDialog=1&strFehlerart=".rawurlencode($strFehlerart).
						"&strFehlertext=".rawurlencode($strFehlertext)."&strDatei=".rawurlencode($PHP_SELF)."&intZeile=".rawurlencode($intZeile).
						"&strBenutzer=".rawurlencode($PHP_AUTH_USER)."&strSql=".rawurlencode($strSql)."\";window.setTimeout('".dialog("fehler_frameset.php")."',50);</script>";
	
	die($strFehlerausgabe);
}

//Ermittelt den String sqlfehler, �bergeben wird: sql-statement, bindevariablenname1, bindevariablenwert1 ...

function sqlfehlertext()
{
	$arr=func_get_args();
	
	$rueckgabe="SQL-Statement: ".$arr[0];
	
	if(count($arr)>1)
	{
		$rueckgabe.="  /  Werte Bindevariablen:";
	
		for($i=1;$i<count($arr);$i+=2)
		{
			$rueckgabe.=" ".$arr[$i]."=".$arr[$i+1].",";
		}
		
		$rueckgabe=substr($rueckgabe,0,strlen($rueckgabe)-1);
	}
		
	return str_replace("'","''",$rueckgabe);
}


/*
Gibt einen Fehlertext aus:
Parameter:
$anw: connection- bzw. statement-handle
boolean $param: 1: statement-handle, 0: connection-handle
 */
function fehlermeldung($anw,$param,$seitenadresse='unbekannt',$abbruch="ja")
{
	if(!$param&&@ociserverversion($anw)=='')
	{
		echo "<script type=\"text/javascript\">";
		echo "datenbankfehlermeldung('Keine Verbindung zur Datenbank!');";
		echo "</script>";

		ocirollback($anw);
		ocilogoff($anw);
		sqlfehlereintrag($code,$meldung,$seitenadresse);
		if($abbruch=="ja")
			exit;
	}
	
	if($arr=ocierror($anw))
	{
		$code=$arr["code"];
		$meldung=$arr["message"];
		echo "<script type=\"text/javascript\">";
		echo "datenbankfehlermeldung('".$code."','".trim($meldung)."');";
		echo "</script>";
		
		if($param)
		ocifreestatement($anw);
		else
		ocirollback($anw);
		ocilogoff($anw);
		sqlfehlereintrag($code,$meldung,$seitenadresse);
		if($abbruch=="ja")
			exit;
	}
}

function phpfehlereintrag($fehlernummer,$fehlertext,$dateiname,$zeilennr)	//Schreibt php-Fehler in die Datenbank Fehler
{
	$fehlertype=array(
				1		=>	"Schwerer Laufzeitfehler",
				2		=>	"Laufzeit-Warnung",
				4		=>	"Parse-Fehler beim Kompilieren",
				8		=>	"Mitteilung zur Laufzeit",
				16		=>	"Schwerer Fehler beim Start von PHP",
				32		=>	"Warnung beim Start von PHP",
				64		=>	"Schwerer Fehler beim Kompilieren",
				128		=>	"Warnung beim Kompilieren",
				256 	=>	"Vom Benutzer erzeugte Fehlermeldung",
				512		=>	"Vom Benutzer erzeugte Warnung",
				1024	=>	"Vom Benutzer erzeugte Mitteilung"
				);

	if($fehlernummer!=2&&$fehlernummer!=8)		//Laufzeit-Warnungen sowie Mitteilungen zur Laufzeit sollen nicht in Tabelle eingetragen werden
	{
		//Fehlereintrag in Log-Datei
	
		global $pfadfehlerlogdatei;
		global $sqlfehler;
		global $PHP_AUTH_USER;
		$aktdatum=date("d.m.Y H:i:s");
				
		$dateihandler=fopen($pfadfehlerlogdatei,'a');
		
		$fehlereintrag=$aktdatum." @@@ ".$PHP_AUTH_USER." @@@ PHP-Fehler: ".$fehlertype[$fehlernummer].
		" @@@ ".$fehlertext." @@@ ".$dateiname." @@@ ".$zeilennr." @@@ \r\n";
		
		fputs($dateihandler,$fehlereintrag);
		
		fclose($dateihandler);
	
		//Fehlereintrag in Datenbank
	
		global $verbindung;
		
		$sql=	"insert into fehler
				select
				seq_fehlerid.nextval,
				to_date('".$aktdatum."','dd.mm.yy hh24:mi:ss'),
				'".str_replace("'","''",$PHP_AUTH_USER)."',
				'PHP-Fehler: ".$fehlertype[$fehlernummer]."',
				'".str_replace("'","''",$fehlertext)."',
				'".str_replace("'","''",$dateiname)."',
				".$zeilennr.",
				''
				from dual";
				
		$anweisung=@ociparse($verbindung,$sql);
		@ociexecute($anweisung);
		@ocifreestatement($anweisung);
		
		$text= "Datum: ".$aktdatum."\nBenutzer: ".$PHP_AUTH_USER."\nMeldung: ".$fehlertext."\nDatei: ".$dateiname.
		"\nZeile: ".$zeilennr;

		mail("blattner_m","PHP-Fehler: ".$fehlertype[$fehlernummer],$text);
		
	}
}

set_error_handler('phpfehlereintrag');

function sqlfehlereintrag($fehlercode,$fehlermeldung,$datei)	//Schreibt sql-Fehler in Datenbank Fehler
{
	//Fehlereintrag in Log-Datei
	
	global $pfadfehlerlogdatei;
	global $sqlfehler;
	global $PHP_AUTH_USER;
	$aktdatum=date("d.m.Y H:i:s");
			
	$dateihandler=fopen($pfadfehlerlogdatei,'a');
	
	$fehlereintrag=$aktdatum." @@@ ".$PHP_AUTH_USER." @@@ ";
	
	if($fehlercode==0)
		$fehlereintrag.="Import-Fehler";
	else
		$fehlereintrag.="Datenbank-Fehler Nr.: ".$fehlercode;
		
	$fehlereintrag.=" @@@ ".$fehlermeldung." @@@ ".$datei." @@@   @@@ ".$sqlfehler;
	
	$fehlereintrag=preg_replace('/\t/','',$fehlereintrag);		//Tabulatoren l�schen
	$fehlereintrag=preg_replace('/\r\n|\n/',' ',$fehlereintrag);	//Zeilenschaltung durch leerzeichen ersetzen
	
	$fehlereintrag.="\r\n";
	
	fputs($dateihandler,$fehlereintrag);
	
	fclose($dateihandler);

	//Fehlereintrag in Datenbank

	global $verbindung;
		
	$sql=	"insert into fehler
			select
			seq_fehlerid.nextval,
			to_date('".$aktdatum."','dd.mm.yy hh24:mi:ss'),
			'".str_replace("'","''",$PHP_AUTH_USER)."',";
	if($fehlercode==0)
	{
		$sql.="'Import-Fehler',";
		$sub="Import-Fehler";
	}
	else
	{
		$sql.="'Datenbank-Fehler Nr. ".$fehlercode."',";
		$sub="Datenbank-Fehler Nr. ".$fehlercode;
	}
		
	$sql.=	"'".str_replace("'","''",$fehlermeldung)."',
			'".str_replace("'","''",$datei)."',
			'',
			'$sqlfehler'
			from dual";
	
	$sql=preg_replace('/\t/','',$sql);		//Tabulatoren l�schen
	$sql=preg_replace('/\r\n|\n/',' ',$sql);	//Zeilenschaltung durch leerzeichen ersetzen
	$anweisung=@ociparse($verbindung,$sql);
	@ociexecute($anweisung);
	@ocifreestatement($anweisung);	
	
	$text= "Datum: ".$aktdatum."\nBenutzer: ".$PHP_AUTH_USER."\nMeldung: ".$fehlermeldung."\nDatei: ".$datei.
		"\n".$sqlfehler;
		
	mail("blattner_m",$sub,$text);
}









?>
