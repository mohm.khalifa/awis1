<?php
global $con;
global $Recht3708;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CKA','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3708 = awisBenutzerRecht($con,3708,$AWISBenutzer->BenutzerName());
if($Recht3708==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
$Bedingung = '';

//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdDSZurueck_x']))
{
	$SQL = 'SELECT CKA_KEY FROM (SELECT CKA_KEY FROM CRMKontaktarten';
	$SQL .= ' WHERE  UPPER(CKA_BEZEICHNUNG) < '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtCKA_BEZEICHNUNG']));
	$SQL .= ' ORDER BY CKA_BEZEICHNUNG DESC';	
	$SQL .= ') WHERE ROWNUM = 1';
	
	$rsCKA = awisOpenRecordset($con, $SQL);
	if(isset($rsCKA['CKA_KEY'][0]))
	{
		$AWIS_KEY2=$rsCKA['CKA_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCKA_KEY'];
	}
}
elseif(isset($_POST['cmdDSWeiter_x']))
{
	$SQL = 'SELECT CKA_KEY FROM (SELECT CKA_KEY FROM CRMKontaktarten';
	$SQL .= ' WHERE  UPPER(CKA_BEZEICHNUNG) > '.strtoupper(awis_FeldInhaltFormat('T',$_POST['txtCKA_BEZEICHNUNG']));
	$SQL .= ' ORDER BY CKA_BEZEICHNUNG ASC';	
	$SQL .= ') WHERE ROWNUM = 1';
	$rsCKA = awisOpenRecordset($con, $SQL);

	if(isset($rsCKA['CKA_KEY'][0]))
	{
		$AWIS_KEY2=$rsCKA['CKA_KEY'][0];
	}
	else 
	{
		$AWIS_KEY2 = $_POST['txtCKA_KEY'];
	}
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./crm_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./crm_speichern_stammdaten.php');
}
elseif(isset($_GET['CKA_KEY']))
{
	$Bedingung .= ' AND CKA_KEY='.awis_FeldInhaltFormat('Z',$_GET['CKA_KEY']).'';
}
elseif(isset($_POST['txtCKA_KEY']))
{
	$Bedingung .= ' AND CKA_KEY='.awis_FeldInhaltFormat('Z',$_POST['CKA_KEY']).'';
}

if($AWIS_KEY2>0)
{
	$Bedingung .= ' AND CKA_KEY='.awis_FeldInhaltFormat('Z',$AWIS_KEY2).'';
}

//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM CRMKontaktarten';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY CKA_BEZEICHNUNG';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

$rsCKA = awisOpenRecordset($con, $SQL);
$rsCKAZeilen = $awisRSZeilen;
awis_Debug(1,$_POST,$_GET, $SQL);
//********************************************************
// Daten anzeigen
//********************************************************
if($rsCKAZeilen==0 AND !isset($_GET['CKA_KEY']))		// Keine Meldung bei neuen Datensätzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden CRMKontaktarten gefunden.</span>';
}
elseif($rsCKAZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	if(($Recht3708&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktarten&CKA_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktarten'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CKA_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CKA_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKA']['CKA_BEZEICHNUNG'],450,'',$Link);
	awis_FORM_ZeileEnde();

	for($CKAZeile=0;$CKAZeile<$rsCKAZeilen;$CKAZeile++)
	{
		awis_FORM_ZeileStart();
		
		$Icons = array();
		if(($Recht3708&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktarten&CKA_KEY='.$rsCKA['CKA_KEY'][$CKAZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktarten&Del='.$rsCKA['CKA_KEY'][$CKAZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CKAZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('CKA_BEZEICHNUNG',$rsCKA['CKA_BEZEICHNUNG'][$CKAZeile],0,450,false,($CKAZeile%2));
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmCRMKontaktarten action=./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktarten method=POST  enctype="multipart/form-data">';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsCKA['CKA_KEY'][0])?$rsCKA['CKA_KEY'][0]:0);

	echo '<input type=hidden name=txtCKA_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Stammdaten&Seite=Kontaktarten&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCKA['CKA_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCKA['CKA_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$TabellenKuerzel='CKA';
	$EditRecht=(($Recht3708&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKA']['CKA_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('CKA_BEZEICHNUNG',($AWIS_KEY1===0?'':$rsCKA['CKA_BEZEICHNUNG'][0]),50,350,$EditRecht);
	$CursorFeld='txtCKA_BEZEICHNUNG';
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();


	if(!isset($_POST['cmdDSNeu_x']))
	{
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Infos'));
		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';

		awis_RegisterErstellen(3701, $con, $RegisterSeite);
	}

	
	//***************************************
	// Schaltflächen für dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3708&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
	awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

	awis_FORM_SchaltflaechenEnde();

	echo '</form>';
}

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>