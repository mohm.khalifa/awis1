<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once 'awisFormular.inc';
require_once 'awisDatenbank.inc';

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	//***********************************************************************************
	//** Preise speichern
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPFA_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('PFA','%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtPAF_KEY'];

		// Alle Felder zu einem Array zusammenbauen
		$Daten = array();
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);

			$Daten[$FeldTeile[2]][$FeldTeile[1]]=$_POST[$Feld];

		}

		$Form->DebugAusgabe(1,$Daten);

		foreach($Daten AS $PFA_KEY=>$Felder)
		{
			$SQL = 'UPDATE PreisabfragenPreise';
			$SQL .= ' SET PFA_PREIS = '.$DB->FeldInhaltFormat('N2',$Felder['PREIS']);
			$SQL .= ' , PFA_ERMITTLUNGSSTATUS = '.$DB->FeldInhaltFormat('N0',$Felder['ERMITTLUNGSSTATUS']);
			$SQL .= ' , PFA_BEMERKUNG = '.$DB->FeldInhaltFormat('T',$Felder['BEMERKUNG']);
			$SQL .= ' WHERE PFA_KEY='.$DB->FeldInhaltFormat('N0',$PFA_KEY);

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
		}
	}

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>