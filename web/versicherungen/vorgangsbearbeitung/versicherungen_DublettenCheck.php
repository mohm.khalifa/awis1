<?php

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{

    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    if($BildschirmBreite < 1024)
    {
        //$FeldBreiten['AST_BEZEICHNUNGWW']=100;
        //$FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN']=10;
    }
    elseif($BildschirmBreite < 1280)
    {
        //$FeldBreiten['AST_BEZEICHNUNGWW']=200;
        //$FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN']=20;
    }
    else
    {
        //$FeldBreiten['AST_BEZEICHNUNGWW']=400;
        //$FeldBreiten['AST_BEZEICHNUNGWW_ZEICHEN']=40;
    }

    $TextKonserven = array();
    $TextKonserven[] = array('VVS', '*');
    $TextKonserven[] = array('VVE', '*');
    $TextKonserven[] = array('FIL', '*');
    $TextKonserven[] = array('SONDERAKTION', '*');
    $TextKonserven[] = array('V_VERS_FORM_DETAIL', '*');
    $TextKonserven[] = array('V_VERS_FORM_RECHNUNGSDATEN', '*');
    $TextKonserven[] = array('VCB', '*');
    $TextKonserven[] = array('VEB', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    echo '<script src="versicherungenDublettenCheck.js"></script>';

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    //echo $Recht9500;

    if(isset($_POST['cmd_dubclear_x']))
    {
        include('versicherungen_dubclear.php');
    }
    elseif(isset($_POST['cmd_abbrechen_x']))
    {
        include('versicherungen_dubclear.php');
    }
    else
    {
        $SQL = 'update versicherungen_dubletten ';
        $SQL .= ' set vdb_status = 0, vdb_userdat = null, vdb_user = null';
        $SQL .= ' WHERE vdb_key in (';
        $SQL .= ' SELECT vdb_key ';
        $SQL .= ' FROM VERSICHERUNGEN_DUBLETTEN';
        $SQL .= ' WHERE VDB_status in (3,4)';
        $SQL .= ' AND VDB_USER = \'' . $AWISBenutzer->BenutzerName() . '\')';

        $DB->Ausfuehren($SQL);
    }

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    

    $SQL = ' SELECT * ';
    $SQL .= ' FROM VERSICHERUNGEN_DUBLETTEN';
    $SQL .= ' WHERE VDB_status = 0';
    $SQL .= ' ORDER BY VDB_FILID, VDB_KFZKENNZ_UF, VDB_VERSNR, VDB_KASSEDATUM_UHRZEIT';

    $rsDubletten = $DB->RecordSetOeffnen($SQL);

    //************************************************
    // Zeilen begrenzen
    //************************************************
    /*
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
	*/
    $FeldBreiten = array();

    $FeldBreiten['FILID'] = 100;
    $FeldBreiten['KFZ_KZ'] = 210;
    $FeldBreiten['KASSEDAT'] = 714;

    $FeldBreiten['AKTION'] = 40;
    $FeldBreiten['WANR'] = 70;
    $FeldBreiten['VORGANGNR'] = 130;
    $FeldBreiten['VERSNR'] = 60;
    $FeldBreiten['DATUMUHRZEIT'] = 150;
    $FeldBreiten['SCHADENDAT'] = 110;
    $FeldBreiten['KFZ_HALTER'] = 170;
    $FeldBreiten['FIN'] = 170;
    $FeldBreiten['UMSATZ'] = 100;
    $FeldBreiten['SCHADKZ'] = 40;

    $Form->SchreibeHTMLCode('<form name="frmDublettenCheck" action="versicherungen_Main.phpmdAktion=DublettenCheck" method=POST  enctype="multipart/form-data">');

    $Form->Formular_Start();

    //********************************************************
    // Daten anzeigen
    //********************************************************
    if($rsDubletten->EOF())        // Keine Meldung bei neuen Datens�tzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
    }
    elseif(($rsDubletten->AnzahlDatensaetze() > 1))    // Liste anzeigen
    {

        $Form->ZeileStart();

        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_WA'], $FeldBreiten['WANR'], '', '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VVS']['VVS_VORGANGNR'], $FeldBreiten['VORGANGNR'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('VersNr', $FeldBreiten['VERSNR'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('Datum/Uhrzeit Kasse', $FeldBreiten['DATUMUHRZEIT'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('Schaden_Dat', $FeldBreiten['SCHADENDAT'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('KFZ_Halter', $FeldBreiten['KFZ_HALTER'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('Fahrgestellnr', $FeldBreiten['FIN'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('GesUmsatz', $FeldBreiten['UMSATZ'], '', '');
        $Form->Erstelle_Liste_Ueberschrift('SKZ', $FeldBreiten['SCHADKZ'], '', '');
        $Form->Schaltflaeche('script', 'cmd_speichern', 'onclick="LadeDiv()";', '/bilder/icon_save.png', 'Gew�hlte Vorg�nge freigeben', '', '', 18, 18);

        echo '<div id="Dubletten" style="display:none; position:absolute; border:5px outset #81bef7;border-radius:5px; z-index:1; background-color:#81bef7; top:100px; left:100px; width:850px;">';
        echo '</div>';

        $Form->ZeileEnde();

        $VDBZeile = 1;

        $VVS_KEY = 0;
        $VVS_KEY2 = array();
        $FILID = 0;
        $KFZKZ = '';
        $VERSNR = 0;
        $DATUM = '';

        while(!$rsDubletten->EOF())
        {
            if($FILID == 0 or ($FILID != $rsDubletten->FeldInhalt('VDB_FILID') or $KFZKZ != $rsDubletten->FeldInhalt('VDB_KFZKENNZ_UF') or $VERSNR != $rsDubletten->FeldInhalt('VDB_VERSNR') or $DATUM != $rsDubletten->FeldInhalt('VDB_KASSEDATUM')))
            {
                $VDBZeile++;

                $Form->ZeileStart('', 'Zeile"ID="Zeile_' . $rsDubletten->FeldInhalt('VDB_VVS_KEY'));
                $Form->Erstelle_ListenFeld('FIL_ID', 'Filnr: ' . $rsDubletten->FeldInhalt('VDB_FILID'), 0, $FeldBreiten['FILID'], false, ($VDBZeile % 2), 'color:red;', '', 'T', 'L');
                $Form->Erstelle_ListenFeld('KFZ_KENNZ', 'Kennzeichen: ' . $rsDubletten->FeldInhalt('VDB_KFZKENNZ'), 0, $FeldBreiten['KFZ_KZ'], false, ($VDBZeile % 2), 'color:red;', '', 'T', 'L');
                $Form->Erstelle_ListenFeld('DAT', 'Kassierdatum: ' . $Form->Format('D', $rsDubletten->FeldInhalt('VDB_KASSEDATUM')), 0, $FeldBreiten['KASSEDAT'], false, ($VDBZeile % 2), 'color:red;', '', 'T', 'L');
                $Form->ZeileEnde();

                $FILID = $rsDubletten->FeldInhalt('VDB_FILID');
                $KFZKZ = $rsDubletten->FeldInhalt('VDB_KFZKENNZ_UF');
                $VERSNR = $rsDubletten->FeldInhalt('VDB_VERSNR');
                $DATUM = $rsDubletten->FeldInhalt('VDB_KASSEDATUM');
            }

            //$Link = './versicherungen_Main.php?cmdAktion=Details&DublettenCheck='.$rsDubletten->FeldInhalt('VVS_KEY').'';
            //$Form->Erstelle_ListenFeld('VVS_KEY',$rsVVS->FeldInhalt('VVS_KEY'),0,80,false,($VDBZeile%2),'',$Link,'T');

            $Form->ZeileStart('', 'Zeile"ID="Zeile_' . $rsDubletten->FeldInhalt('VDB_VVS_KEY'));
            //$Form->Erstelle_ListenFeld('VVS_FILID',$rsDubletten->FeldInhalt('VVS_FILID'),0,$FeldBreiten['FILID'],false,($VDBZeile%2),'','','T');

            $Link = './versicherungen_Main.php?cmdAktion=Details&VVS_KEY=' . $rsDubletten->FeldInhalt('VDB_VVS_KEY') . '&Hauptseite=Dubletten';

            $Form->Erstelle_ListenFeld('VDB_WANR', $rsDubletten->FeldInhalt('VDB_WANR'), 0, $FeldBreiten['WANR'], false, ($VDBZeile % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('VDB_VORGANGNR', $rsDubletten->FeldInhalt('VDB_VORGANGNR'), 0, $FeldBreiten['VORGANGNR'], false, ($VDBZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VDB_VERSNR', $rsDubletten->FeldInhalt('VDB_VERSNR'), 0, $FeldBreiten['VERSNR'], false, ($VDBZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VDB_KASSEDATUM_UHRZEIT', $rsDubletten->FeldInhalt('VDB_KASSEDATUM_UHRZEIT'), 0, $FeldBreiten['DATUMUHRZEIT'], false, ($VDBZeile % 2), '', '', 'DU');
            $Form->Erstelle_ListenFeld('VDB_SCHADENDAT', $rsDubletten->FeldInhalt('VDB_SCHADENDAT'), 0, $FeldBreiten['SCHADENDAT'], false, ($VDBZeile % 2), '', '', 'D');
            $Form->Erstelle_ListenFeld('VDB_KFZHALTER', $rsDubletten->FeldInhalt('VDB_KFZHALTER'), 0, $FeldBreiten['KFZ_HALTER'], false, ($VDBZeile % 2), 'color:red;', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('VDB_FIN', $rsDubletten->FeldInhalt('VDB_FIN'), 0, $FeldBreiten['FIN'], false, ($VDBZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VDB_UMSATZGES', $rsDubletten->FeldInhalt('VDB_UMSATZGES'), 0, $FeldBreiten['UMSATZ'], false, ($VDBZeile % 2), '', '', 'N2');
            $Form->Erstelle_ListenFeld('VDB_SCHADENKZ', $rsDubletten->FeldInhalt('VDB_SCHADENKZ'), 0, $FeldBreiten['SCHADKZ'], false, ($VDBZeile % 2), '', '', 'T');

            //$Link = './versicherungen_Main.php?cmdAktion=DublettenCheck&Sperren='.$rsDubletten->FeldInhalt('VVS_KEY').'#DS2'.$VVS_KEY.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            // $Form->Schaltflaeche('href', 'cmd_freigabe','./versicherungen_Main.php?cmdAktion=DublettenCheck&Freigabe='.$rsDubletten->FeldInhalt('VDB_VVS_KEY'),'/bilder/button_ok.png','Vorgang freigeben','','',16,16);
            $Form->Erstelle_Checkbox('VDB_CHK_' . $rsDubletten->FeldInhalt('VDB_VVS_KEY'), isset($_POST['txtVDB_CHK_' . $rsDubletten->FeldInhalt('VDB_VVS_KEY')]), 40, true);
            $Form->ZeileEnde();

            $rsDubletten->DSWeiter();
        }

        //$Link = './versicherungen_Main.php?cmdAktion=Details&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
        //$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

        $Form->Formular_Ende();
    }

    $Form->SchreibeHTMLCode('</form>');
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        //$Form->Fehler _Anzeigen('INTERN',$ex->getMessage('MELDEN'),6,"200905131735");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>