<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $Recht16001;

require_once('awisFilialen.inc');

try
{
	$AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
    $TextKonserven = array();
    $TextKonserven[]=array('PEI','*');        
    $TextKonserven[]=array('PBM','PBM_XBN_KEY');
    $TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
    $TextKonserven[]=array('FIL','FIL_GEBIET');
    $TextKonserven[]=array('Wort','Abschlussdatum');
    $TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Auswertung');
	$TextKonserven[]=array('Liste','lst_PEI_AUSWERTUNGEN');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');

    $Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht16001 = $AWISBenutzer->HatDasRecht(16001);

	if($Recht16001==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	//$Form->DebugAusgabe(1,$_POST);
	
	if(!isset($_POST['cmdSuche_x']))
	{	
		
		$Form->SchreibeHTMLCode('<form name="frmFilialberichtAuswertungSuche" action="./filialberichte_Main.php?cmdAktion=Berichtsauswertungen" method="POST"  enctype="multipart/form-data">');
		
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FLB_Berichtsauswertung'));
				
		$Form->Formular_Start();	
		
		//Filiale
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_FIL_ID'].':',200);
		$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
		if($FilZugriff!='')
		{	
			$Form->Erstelle_TextFeld('FLB_FIL_ID',$FilZugriff,20,200,false,'','','','T');
			echo '<input type=hidden name="sucFLB_FIL_ID" value="'.$FilZugriff.'">';
		}		
		else
		{
			$Form->Erstelle_TextFeld('*FLB_FIL_ID',($Param['SPEICHERN']=='on'?$Param['FLB_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
		}
		$Form->ZeileEnde();				
		
		// Datumsbereich festlegen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',200);
		$Form->Erstelle_TextFeld('*FLB_DATUM_VOM',($Param['SPEICHERN']=='on'?$Param['FLB_DATUM_VOM']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('01.01.Y'))),10);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',200);
		$Form->Erstelle_TextFeld('*FLB_DATUM_BIS',($Param['SPEICHERN']=='on'?$Param['FLB_DATUM_BIS']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('31.12.Y'))),10);		
		$Form->ZeileEnde();						
		
		//Wenn keine Filiale, dann auch Auswahl nach Gebiet 
		if ($FilZugriff == '')
		{
			// Gebiet
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);
	
			//Alle Gebiete anzeigen	
			if (($Recht16001&2)==2)
			{
				$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
				$SQL .= ' FROM Filialebenen';
				$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
				$SQL .= ' ORDER BY 2';		
				
				$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
			}
			else 
			{
				$SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
				$SQL .= ' FROM Filialebenen';
				$SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
				$SQL .= ' AND EXISTS';
				$SQL .= '  (select DISTINCT XX1_FEB_KEY';
				$SQL .= '  FROM V_FILIALEBENENROLLEN';
				$SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
				$SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE)';
				$SQL .= '  WHERE  XX1_FEB_KEY = FEB_KEY';		
				$SQL .= '  AND XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();			
				$SQL .= ')';
				$SQL .= ' ORDER BY 2';		
				
				$Form->Erstelle_SelectFeld('*FIL_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:'0'),400,true,$SQL);
			}
			$Form->ZeileEnde();							
		}		
								
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');		
		$Form->SchaltflaechenEnde();
		
		$Form->SchreibeHTMLCode('</form>');		
	}	
	else 
	{	
		$Param = array();
	
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';

		$Param['FLB_DATUM_VOM']=$Form->Format('D',$_POST['sucFLB_DATUM_VOM'],true);
		$Param['FLB_DATUM_BIS']=$Form->Format('D',$_POST['sucFLB_DATUM_BIS'],true);
		$Param['FLB_FIL_ID']=$Form->Format('N0',$_POST['sucFLB_FIL_ID'],true);
		$Param['FIL_GEBIET']=$Form->Format('N0',(isset($_POST['sucFIL_GEBIET'])?$_POST['sucFIL_GEBIET']:''),true);

		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$AWISBenutzer->ParameterSchreiben('Formular_FLB_Berichtsauswertung',serialize($Param));
		
		$Bedingung = _BedingungErstellen($Param);
		
		$SQL = 'select ergebnis, count(*) as Anzahl ';
		$SQL.= ' from(';
		$SQL.= ' select flb_key, flb_datum, flb_fil_id, flb_pbe_key, flb_pez_status, anzfelder, anzfelderok, anzfeldernichtok, quote,';//xx1_ebene, Region,';
		$SQL.= ' case when quote >= 90 then \'01 - GRUEN\' when quote < 90 and quote >= 80 then \'02 - GELB\' else \'03 - ROT\' end as Ergebnis';
		$SQL.= ' from (';
		$SQL.= ' select flb_key, flb_datum, flb_fil_id, flb_pbe_key, flb_pez_status, anzfelder, anzfelderok, anzfeldernichtok,';
		$SQL.= ' round(100 * anzfelderok / decode(anzfelder,0,1,anzfelder),0) as quote';
		$SQL.= ' from (';
		$SQL.= ' select distinct flb_key, flb_datum, flb_fil_id, flb_pbe_key, flb_pez_status';
		$SQL.= ', (select count(*) from personaleinsberichtszuord where (pez_wert = \'1\' or pez_wert = \'0\') and pez_xxx_key = flb_key and pez_xtn_kuerzel = \'FLB\') as anzfelder';
		$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'1\' and pez_xxx_key = flb_key and pez_xtn_kuerzel = \'FLB\') as anzfelderok';
		$SQL.= ', (select count(*) from personaleinsberichtszuord where pez_wert = \'0\' and pez_xxx_key = flb_key and pez_xtn_kuerzel = \'FLB\') as anzfeldernichtok';
		$SQL.= ' from filialberichte p';
		$SQL.= ' inner join personaleinsberichte on pbe_key = flb_pbe_key';
		$SQL.= ' inner join filialen on flb_fil_id = fil_id ';
		$SQL.= ' where flb_pez_status = 1';						
		
		if($Bedingung!='')
		{
			$SQL .= ' AND ' . substr($Bedingung,4);
		}		
		
		$SQL.=')))';				
		$SQL.= ' group by ergebnis';
		$SQL.= ' order by ergebnis';
		$Form->DebugAusgabe(1,$SQL);
		
		$rsPEI = $DB->RecordSetOeffnen($SQL);
		
		$SummeBerichte=0;
		
		//Überschrift
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Status',200);
		$Form->Erstelle_Liste_Ueberschrift('Anzahl',200);
		$Form->ZeileEnde();
		
		while (!$rsPEI->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_ListenFeld('Erg', substr($rsPEI->FeldInhalt('ERGEBNIS'),5),10,200);
			$Form->Erstelle_ListenFeld('Anz', $rsPEI->FeldInhalt('ANZAHL'),10,200);
			$Form->ZeileEnde();
			
			$SummeBerichte+=$rsPEI->FeldInhalt('ANZAHL');
			
			$rsPEI->DSWeiter();			
		}
				
		$Form->ZeileStart();
		$Form->Erstelle_ListenFeld('Sum', 'Summe',10,200,false,0,'font-weight:bold');
		$Form->Erstelle_ListenFeld('Anz', $Form->Format('N0',$SummeBerichte),10,200,false,0,'font-weight:bold');
		$Form->ZeileEnde();				
		
		$Form->Trennzeile('O');
		$Form->Trennzeile('O');		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Fehlerquote Einzelkategorien',500,'font-weight:bold');
		$Form->ZeileEnde();
		
		//Überschrift
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
		$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
		$Form->Erstelle_Liste_Ueberschrift('OK',100);
		$Form->Erstelle_Liste_Ueberschrift('Gesamt',100);
		$Form->ZeileEnde();
		
		//Bericht QS
		$SummeNichtOk=0;
		
		//Berichtsauswertung je nach Kategorie
		$Summe['A'] = Auswertung('A', 'Allgemein', '21,22,23,24', $Bedingung);
		$SummeNichtOk += $Summe['A'];
		$Summe['B'] = Auswertung('B', 'Inspektionen', '26,27', $Bedingung);
		$SummeNichtOk += $Summe['B'];
		$Summe['C'] = Auswertung('C', 'Positonen', '29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,59', $Bedingung);
		$SummeNichtOk += $Summe['C'];
		
		$Form->Trennzeile('O');
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Fehlerquote Checks - Gesamt',500,'font-weight:bold');
		$Form->ZeileEnde();
		
		//Überschrift
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Kategorie',350);
		$Form->Erstelle_Liste_Ueberschrift('Nicht OK',184);
		$Form->ZeileEnde();

		Auswertung_2('A', 'Allgemein',$Summe['A'],$SummeNichtOk);
		Auswertung_2('B', 'Inspektionen',$Summe['B'],$SummeNichtOk);
		Auswertung_2('C', 'Positionen',$Summe['C'],$SummeNichtOk);
				
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/filialberichte/filialberichte_Main.php?cmdAktion=Berichtsauswertungen','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->SchaltflaechenEnde();		
	}
				
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
			
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

function Auswertung($Kategorie, $Bezeichnung, $IDs, $Bedingung)
{
	global $Form;
	global $DB;		
	
	//nicht o.K. Kategorie A
	$SQL= 'SELECT SUM(anzfelder) as gesamt, sum(anzfelderok) as ok, sum(anzfeldernichtok) as nichtok';
	$SQL.= ' from(';
	$SQL.= ' select distinct flb_key, flb_datum, flb_fil_id, flb_pbe_key, flb_pez_status';//, xx1_ebene, f2.feb_bezeichnung as Region';
	$SQL.= ' , (select count(*) from personaleinsberichtszuord where pez_pbf_id in ('.$IDs.') and (pez_wert = \'1\' or pez_wert = \'0\') and pez_xxx_key = flb_key and pez_xtn_kuerzel = \'FLB\') as anzfelder';
	$SQL.= ' , (select count(*) from personaleinsberichtszuord where pez_pbf_id in ('.$IDs.') and pez_wert = \'1\' and pez_xxx_key = flb_key and pez_xtn_kuerzel = \'FLB\') as anzfelderok';
	$SQL.= ' , (select count(*) from personaleinsberichtszuord where pez_pbf_id in ('.$IDs.') and pez_wert = \'0\' and pez_xxx_key = flb_key and pez_xtn_kuerzel = \'FLB\') as anzfeldernichtok';
	$SQL.= ' from filialberichte p';
	$SQL.= ' inner join personaleinsberichte on pbe_key = flb_pbe_key';
	$SQL.= ' inner join filialen on fil_id = flb_fil_id';
	$SQL.= ' where flb_pez_status = 1';
	
	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}			
	
	$SQL.= ' )';			
	//$Form->DebugAusgabe(1,$SQL);
	$rsPEI = $DB->RecordSetOeffnen($SQL);
		
	$Form->ZeileStart();
	$Form->Erstelle_ListenFeld($Kategorie,$Kategorie.' - '.$Bezeichnung,10,350);
	
	
	if ($rsPEI->FeldInhalt('GESAMT') == '0' or $rsPEI->FeldInhalt('GESAMT') == '')
	{
		$Gesamt = 1;
	}
	else 
	{
		$Gesamt = $rsPEI->FeldInhalt('GESAMT');
	}
	
	$Prozent = $Form->Format('N2', $rsPEI->FeldInhalt('NICHTOK') * 100 / $Gesamt);		
	$Form->Erstelle_ListenFeld('nichtok',$rsPEI->FeldInhalt('NICHTOK'),10,80);
	$Form->Erstelle_ListenFeld('nichtokprozent',$Prozent.' %',10,100);
	
	$Form->Erstelle_ListenFeld('ok',$rsPEI->FeldInhalt('OK'),10,100);
	$Form->Erstelle_ListenFeld('gesamt',$rsPEI->FeldInhalt('GESAMT'),10,100);		
	$Form->ZeileEnde();
	
	return $rsPEI->FeldInhalt('NICHTOK');		
}


function Auswertung_2($Kategorie, $Bezeichnung, $Summe, $SummeNichtOk)
{
	global $Form;
	global $DB;
	
	if ($SummeNichtOk==0)
	{
		$SummeNichtOk=1;
	}
	
	$Form->ZeileStart();
	$Prozent = $Form->Format('N2', $Summe * 100 / $SummeNichtOk);					
	$Form->Erstelle_ListenFeld($Kategorie,$Kategorie.' - '.$Bezeichnung,10,350);
	$Form->Erstelle_ListenFeld('nichtok',$Summe,10,80);			
	$Form->Erstelle_ListenFeld('nichtokprozent',$Prozent.' %',10,100);
	$Form->ZeileEnde();
}

function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Recht16001;

	$Bedingung = '';
	/*
	if (isset($Param['PBE_KEY']) and $Param['PBE_KEY']!='')
	{
		$Bedingung .= ' AND PEI_PBE_KEY = '.$Param['PBE_KEY'];
	}*/
	
	$Bedingung .= ' AND FLB_PBE_KEY = 2';
	
	if(isset($Param['FLB_DATUM_VOM']) AND $Param['FLB_DATUM_VOM']!='')
	{
		$Bedingung .= ' AND FLB_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['FLB_DATUM_VOM'],false);
	}
	
	if(isset($Param['FLB_DATUM_BIS']) AND $Param['FLB_DATUM_BIS']!='')
	{
		$Bedingung .= ' AND FLB_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['FLB_DATUM_BIS'],false);
	}

	if(isset($Param['FLB_FIL_ID']) AND $Param['FLB_FIL_ID']!='')
	{
		$Bedingung .= ' AND FIL_ID =' .$DB->FeldInhaltFormat('N0',$Param['FLB_FIL_ID']);

		if (($Recht16001&2)!=2 and ($Recht16001&4)!=4)
		{		
			$Bedingung .= ' AND FIL_ID IN (SELECT FEZ_FIL_ID FROM FILIALEBENENZUORDNUNGEN WHERE FEZ_FEB_KEY IN (';			
			$Bedingung .= 'SELECT FEB_KEY';//, FEB_BEZEICHNUNG';
			$Bedingung .= ' FROM Filialebenen';
			$Bedingung .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
			$Bedingung .= ' AND EXISTS';
			$Bedingung .= '  (select DISTINCT XX1_FEB_KEY';
			$Bedingung .= '  FROM V_FILIALEBENENROLLEN';
			$Bedingung .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
			$Bedingung .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(SYSDATE)';
			$Bedingung .= '  WHERE  XX1_FEB_KEY = FEB_KEY';		
			$Bedingung .= '  AND XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();			
			$Bedingung .= '))';
			$Bedingung .= ' AND trunc(FEZ_GUELTIGAB) <= trunc(sysdate) and trunc(FEZ_GUELTIGBIS) >= trunc(sysdate))';
		}
	}

	if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET']!='0' AND $Param['FIL_GEBIET']!='')
	{
		$Bedingung .= " AND FIL_ID IN (0";
		$FilObj = new awisFilialen();
		$FilListe = $FilObj->FilialenEinerEbene($DB->FeldInhaltFormat('N0',$Param['FIL_GEBIET'],false));
		foreach($FilListe AS $FilialZeile)
		{
			$Bedingung .= ','.$FilialZeile['FEZ_FIL_ID'];
		}
		$Bedingung .= ')';
	}

	$Param['WHERE']=$Bedingung;
	return $Bedingung;
}

?>

