<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('CKO','CKO_%');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3702 = awisBenutzerRecht($con,3702,$AWISBenutzer->BenutzerName());
if($Recht3702==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$SQL = 'SELECT CAD_AKTIONEN, CAD_KEY';
$SQL .= ' FROM CRMAdressen';
$SQL .= ' WHERE CAD_KEY=0'.$AWIS_KEY1;
$rsCAD = awisOpenRecordset($con,$SQL);

if(isset($rsCAD['CAD_KEY'][0]))
{
	awis_FORM_FormularStart();

	$EditRecht=(($Recht3702&2)!=0);

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_AKTIONEN'].':',150);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	awis_FORM_Erstelle_SelectFeld('CAD_AKTIONEN',$rsCAD['CAD_AKTIONEN'][0],200,$EditRecht,$con,'','','','','',$Daten);
	awis_FORM_ZeileEnde();

	awis_FORM_Trennzeile('L');


	
// Aktionen anzeigen
//***********************************
	
	$SQL = 'SELECT * ';
	$SQL .= ' FROM CRMAktionen ';
	$SQL .= ' INNER JOIN CRMAktionenTypen ON CAT_KEY = CKO_CAT_KEY';
	$SQL .= ' WHERE CKO_XXX_KEY=0'.$rsCAD['CAD_KEY'][0];
	
	if(isset($_GET['CKOKEY']))
	{
		$SQL .= ' AND CKO_KEY=0'.intval($_GET['CKOKEY']);
	}
	if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
	{
		$SQL .= ' AND CKO_KEY=0'.intval($AWIS_KEY2);
	}
	if(!isset($_GET['CKOSort']))
	{
		$SQL .= ' ORDER BY CAT_BEZEICHNUNG';
	}
	else
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CKOSort']);
	}

	$rsCKO = awisOpenRecordset($con,$SQL);
	$rsCKOZeilen = $awisRSZeilen;
	

	if($rsCKOZeilen > 1 or isset($_GET['CKOListe']))					// Liste anzeigen
	{

		awis_FORM_ZeileStart();
	
		if(($Recht3702&6)>0)
		{
			$Icons[] = array('new','./crm_Main.php?cmdAktion=Details&Seite=Aktionen&CKOKEY=0');
			awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
		}
	
		$Link = './crm_Main.php?cmdAktion=Details&Seite=Aktionen';
		$Link .= '&CKOSort=CAT_BEZEICHNUNG'.((isset($_GET['CKOSort']) AND ($_GET['CKOSort']=='CAT_BEZEICHNUNG'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKO']['CKO_CAT_KEY'],250,'',$Link);
	
		$Link = './crm_Main.php?cmdAktion=Details&Seite=Aktionen';
		$Link .= '&CKOSort=CKO_BEMERKUNG'.((isset($_GET['CKOSort']) AND ($_GET['CKOSort']=='CKO_BEMERKUNG'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKO']['CKO_BEMERKUNG'],250,'',$Link);
	
		$Link = './crm_Main.php?cmdAktion=Details&Seite=Aktionen';
		$Link .= '&CKOSort=CKO_LETZTEAKTION'.((isset($_GET['CKOSort']) AND ($_GET['CKOSort']=='CKO_LETZTEAKTION'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CKO']['CKO_LETZTEAKTION'],150,'',$Link);
	
		awis_FORM_ZeileEnde();
	
		for($CKOZeile=0;$CKOZeile<$rsCKOZeilen;$CKOZeile++)
		{
			awis_FORM_ZeileStart();
			$Icons = array();
			if(($Recht3702&6)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./crm_Main.php?cmdAktion=Details&Seite=Aktionen&CKOKEY='.$rsCKO['CKO_KEY'][$CKOZeile]);
				$Icons[] = array('delete','./crm_Main.php?cmdAktion=Details&Seite=Aktionen&Del='.$rsCKO['CKO_KEY'][$CKOZeile]);
			}
			awis_FORM_Erstelle_ListeIcons($Icons,38,($CKOZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('CAT_BEZEICHNUNG',$rsCKO['CAT_BEZEICHNUNG'][$CKOZeile],20,250,false,($CKOZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('CKO_BEMERKUNG',$rsCKO['CKO_BEMERKUNG'][$CKOZeile],20,250,false,($CKOZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('CKO_LETZTEAKTION',$rsCKO['CKO_LETZTEAKTION'][$CKOZeile],20,150,false,($CKOZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
	}
	else 		// Einer oder keiner
	{
		awis_FORM_FormularStart();
		
		echo '<input name=txtCKO_KEY type=hidden value=0'.(isset($rsCKO['CKO_KEY'][0])?$rsCKO['CKO_KEY'][0]:'').'>';
		$AWIS_KEY2 = (isset($rsCKO['CKO_KEY'][0])?$rsCKO['CKO_KEY'][0]:'');
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Details&Seite=Aktionen&CKOListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKO['CKO_KEY'][0])?$rsCKO['CKO_USER'][0]:'')));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCKO['CKO_KEY'][0])?$rsCKO['CKO_USERDAT'][0]:'')));
		awis_FORM_InfoZeile($Felder,'');
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKO']['CKO_CAT_KEY'].'',150);
		$SQL = 'SELECT CAT_KEY, CAT_BEZEICHNUNG FROM CRMAKTIONENTYPEN';
		$SQL .= ' WHERE CAT_KEY NOT IN (SELECT CKO_CAT_KEY FROM CRMAKTIONEN WHERE CKO_XXX_KEY=0'.$AWIS_KEY1.')';
		$SQL .= ' OR CAT_KEY =0'.(isset($rsCKO['CKO_CAT_KEY'][0])?$rsCKO['CKO_CAT_KEY'][0]:'');
		$SQL .=  ' ORDER BY CAT_BEZEICHNUNG';

		$Daten = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		awis_FORM_Erstelle_SelectFeld('CKO_CAT_KEY',(isset($rsCKO['CKO_CAT_KEY'][0])?$rsCKO['CKO_CAT_KEY'][0]:''),200,true,$con,$SQL,$Daten,'','');
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKO']['CKO_BEMERKUNG'].'',150);
		awis_FORM_Erstelle_TextFeld('CKO_BEMERKUNG',(isset($rsCKO['CKO_BEMERKUNG'][0])?$rsCKO['CKO_BEMERKUNG'][0]:''),40,300,($Recht3702&6));
		awis_FORM_ZeileEnde();
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CKO']['CKO_LETZTEAKTION'].'',150);
		awis_FORM_Erstelle_TextFeld('CKO_LETZTEAKTION',(isset($rsCKO['CKO_LETZTEAKTION'][0])?$rsCKO['CKO_LETZTEAKTION'][0]:''),10,120,($Recht3702&6),'','','','D');
		awis_FORM_ZeileEnde();
	
		awis_FORM_FormularEnde();
	}
}
?>