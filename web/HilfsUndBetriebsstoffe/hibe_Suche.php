<?php
// Variablen
global $con;
global $awisDBFehler;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 1100);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Hilfs- und Betriebsstoffe',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

if(isset($_REQUEST['Reset']))
{
	awis_BenutzerParameterSpeichern($con,"HBST_Suche",$AWISBenutzer->BenutzerName(),'');	
}

$Params = explode(";",awis_BenutzerParameter($con, "HBST_Suche", $AWISBenutzer->BenutzerName()));

echo "<form name=frmSuche method=post action=./hibe_Main.php>";

echo '<br>';
echo '<table border=1 width=100%>';

/******* Filiale *******/
$EingabeFeld='txtFIL_ID';
print "<tr><td width=190>Filial-<u>N</u>ummer</td><td><input name=txtFIL_ID accesskey=n size=5 value=\"" . ($Params[0]=='on'?$Params[1]:'') . "\"></td></tr>";

/******* Seriennummer *******/
print "<tr><td width=190>Seriennummer</td><td><input name=txtHBS_SERIENNUMMER size=20 value=\"" . ($Params[0]=='on'?$Params[2]:'') . "\"></td></tr>";

/******* Inventarnummer *******/
print "<tr><td width=190>Inventarnummer</td><td><input name=txtHBS_INVENTARNUMMER size=20 value=\"" . ($Params[0]=='on'?$Params[6]:'') . "\"></td></tr>";

/******* Hersteller *******/
print "<tr><td width=190>Hersteller</td><td><input name=txtHER_BEZEICHNUNG size=20 value=\"" . ($Params[0]=='on'?$Params[3]:'') . "\"></td></tr>";

/******* Typ *******/
print "<tr><td width=190>Typ</td><td><input name=txtHBT_BEZEICHNUNG size=20 value=\"" . ($Params[0]=='on'?$Params[4]:'') . "\"></td></tr>";


/******* Alle Eins�tze *******/
print "<tr><td width=190>Auch historische Daten:</td>";
print "<td><input type=checkbox value=on " . (isset($Params[5]) && $Params[5]=="on"?"checked":"") . " name=txtHistory tabindex=80></td></tr>";


/******* Auswahl speichern *******/
echo '<tr><td colspan=2>&nbsp;</td></tr>';
print "<tr><td width=190>Auswahl <u>s</u>peichern:</td>";
print "<td><input type=checkbox value=on " . ($Params[0]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=80></td></tr>";
	
echo '</table>';
echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png alt='Formularinhalt l�schen' name=cmdReset onclick=location.href='./hibe_Main.php?cmdAktion=Suche&Reset=True';>";
print "<input type=hidden name=cmdAktion value=Einsatz>";

echo '</form>';


/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('" . $EingabeFeld . "')[0].focus();";
	echo "\n</Script>";
}

?>
