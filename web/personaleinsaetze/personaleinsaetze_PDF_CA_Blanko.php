<?php

//Blanko -> Formular VisitCheck
require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('IBG','%');
$TextKonserven[]=array('CV','%');
$TextKonserven[]=array('LF','%');
$TextKonserven[]=array('CA','%');

$AWISBenutzer = awisBenutzer::Init();

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Ausdruck = new awisAusdruck('P','A4','');
$Ausdruck->NeueSeite(0,1);

if ($AWISBenutzer->BenutzerSprache()=='CZ')
{
	$Ausdruck->_pdf->ZeichenKodierung('CP1250');
	$Schriftart = 'arialcz';
}
else 
{
	$Schriftart = 'arial';
}

$Ausdruck->_pdf->AddFont($Schriftart,'','');

function Auswertung_Felder($Ausdruck,$ID,$x1,$y1,$x1_2,$y1_2,$FeldBreite = 5)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	global $Schriftart;
	
	$Schritt = 3;
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;
	
	$rsAuswertung = $DB->RecordSetOeffnen($SQL);
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1,$y1,$x2 = ($x1  + $Schritt),$y2 = ($y1 + $Schritt));
	  $Ausdruck->_pdf->Line($x2, $y1, $x1, $y2);
          $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	}
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1_2,$y1_2,$x2 = ($x1_2  + $Schritt),$y2 = ($y1_2 + $Schritt));
      $Ausdruck->_pdf->Line($x2, $y1_2, $x1_2, $y2);
      $Ausdruck->_pdf->SetLineWidth(0); 	
	}
	else
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	}
	$SchriftartTemp = $Schriftart;
	if($rsAuswertung->FeldInhalt('PBF_BEMERKUNG') == '')
	{
	  $Schriftart = 'courier';
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6);
	  //$Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,35)."\n".substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),35,33).($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG') != '' ?'...':'');
	  $Text = '';
	  $DeadEnd = '';
	  if($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG') == '')
	  {
	  	$Ausdruck->_pdf->SetFillColor(255,255,255);
	  	$Spalte = $Ausdruck->_pdf->getX();
	  	$Zeile = $Ausdruck->_pdf->getY();
	  	$Ausdruck->_pdf->cell2(40,$FeldBreite,'','LTRB','L',1,1);
	  	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	  	$Ausdruck->_pdf->cell2(40,$FeldBreite,$Text,'LTRB',0,'L',0);
	  }
	  else 
	  {
	  	$Spalte = $Ausdruck->_pdf->getX();
	  	$Ausdruck->_pdf->SetFillColor(255,255,255);
	  	$Zeile = $Ausdruck->_pdf->getY();
	  	$Ausdruck->_pdf->cell2(40,$FeldBreite,'','LTRB','L',1,1);
	  	switch($FeldBreite)
	  	{
	  		case '5':			// PDF im extra Fenster -> Download
	  			$Hoehe = 2.5;
	  			$Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,60);
	  			break;
	  		case '8':			// Standard-Stream
	  			$Hoehe = 1.9;
	  			$Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,110);
	  			break;
	  		
	  	}
	  		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	  		$Spalte = $Ausdruck->_pdf->getX();
	  		$Zeile = $Ausdruck->_pdf->getY();
	  		$Ausdruck->_pdf->MultiCell(40,$Hoehe, $Text,0,'N');	
	  		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	  		$Ausdruck->_pdf->cell2(40,$FeldBreite,'','LTRB','L',1);
	  }
	  
	}
	else 
	{
	  
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6); 	
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$rsAuswertung->FeldInhalt('PBF_BEMERKUNG').':'.$rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),'LTRB','L',1);  	
	}
	
	$Schriftart = $SchriftartTemp;
	
}

function FuelleFelder($Ausdruck,$ID)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	global $Schriftart;

	$Schritt = 3;

	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;

	$rsAuswertung = $DB->RecordSetOeffnen($SQL);
	
	$Ausdruck->_pdf->cell2(40,4,$rsAuswertung->FeldInhalt('PEZ_WERT'),'B',0,'L',0);
}


$Spalte = 15;
$Zeile = 5;

if($_GET['PEI_KEY'] == 0)
{
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Checkliste Autoglas','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 25.02.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);
	
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['IBG']['IBG_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['IBG']['IBG_Datum'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,'','B',0,'C',0);
$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['LF']['LF_GBL'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_GL'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_WL'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);


$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 72, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 72, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 77, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 77, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 82, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 82, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);




$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 92, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 92, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 97, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 97, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 102, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 102, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 107, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 107, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 112, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 112, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 117, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 117, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B7'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 122, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 122, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_B8'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 127, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 127, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_C1'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 140, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 140, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_C2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 148, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 148, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 156, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 156, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;


$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 161, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 161, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_C5'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 166, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 166, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 174, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 174, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 184, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 184, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 189, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 189, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 194, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 194, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 199, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 199, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 204, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 204, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 209, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 209, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);



$Zeile+=78;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Checkliste Autoglas '.date('m-Y'),'T',0,'L',0);


$Ausdruck->NeueSeite(0,1);

$Spalte = 15;
$Zeile = 5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Checkliste Autoglas','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 2 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 25.02.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);

$Zeile+=15;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 52, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 52, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_E2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 57, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 57, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 65, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 65, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 70, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 70, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 75, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 75, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 80, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 80, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_E7'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 85, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 85, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E8'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 93, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 93, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E9'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 98, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 98, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=25;
$Spalte+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Unterschrift GL/WL','T',0,'C',0);

$Spalte+=120;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL','T',0,'C',0);

$Zeile+=164;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Liste Filialbesuch GBL '.date('m-Y'),'T',0,'L',0);





}
elseif($_GET['PEI_KEY'] != 0)
{

$SQL = 	'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS '; 
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= 'WHERE PEZ_PEI_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEI_KEY']);
	
$rsBericht = $DB->RecordSetOeffnen($SQL);	

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Checkliste Autoglas','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 25.02.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['IBG']['IBG_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,$rsBericht->FeldInhalt('PEI_FIL_ID'),'B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITVON'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITBIS'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['IBG']['IBG_Datum'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,substr($rsBericht->FeldInhalt('PEI_DATUM'),0,10),'B',0,'C',0);
$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['LF']['LF_GBL'].':','',0,'L',0);
FuelleFelder($Ausdruck,474);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_GL'].':','',0,'L',0);
FuelleFelder($Ausdruck,475);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_WL'].':','',0,'L',0);
FuelleFelder($Ausdruck,476);

$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,478, 128.5, 72,143,72);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,479, 128.5, 77,143,77);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_A3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,480, 128.5, 82,143,82);

$Zeile+=5;
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,482, 128.5, 92,143,92);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,483, 128.5, 97,143,97);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,484, 128.5, 102,143,102);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,485, 128.5, 107,143,107);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,486, 128.5, 112,143,112);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,489, 128.5, 117,143,117);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_B7'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,490, 128.5, 122,143,122);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_B8'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,491, 128.5, 127,143,127,8);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',1,1);
$Spalte-=140;

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_C1'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,493, 128.5, 140,143,140,8);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_C2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,494, 128.5, 148,143,148,8);
$Spalte-=110;

$Zeile+=8;


$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,495, 128.5, 156,143,156);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,495, 128.5, 161,143,161);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_C5'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,494, 128.5, 166,143,166,8);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_C6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,495, 128.5, 174,143,174);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,500, 128.5, 184,143,184);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,501, 128.5, 189,143,189);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,502, 128.5, 194,143,194);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,503, 128.5, 199,143,199);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,504, 128.5, 204,143,204);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_D6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,505, 128.5, 209,143,209);

$Zeile+=5;
$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','T','L',1,1);
$Spalte-=140;
$Ausdruck->_pdf->SetFillColor(190,190,190);


$Zeile+=73;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Checkliste Autoglas '.date('m-Y'),'T',0,'L',0);


$Ausdruck->NeueSeite(0,1);

$Spalte = 15;
$Zeile = 5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Checkliste Autoglas','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 2 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 25.02.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);

$Zeile+=15;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,507, 128.5, 52,143,52);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_E2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,508, 128.5, 57,143,57,8);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,509, 128.5, 65,143,65);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,510, 128.5, 70,143,70);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,511, 128.5, 75,143,75);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,512, 128.5, 80,143,80);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['CA']['CA_E7'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,513, 128.5, 85,143,85,8);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E8'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,514, 128.5, 93,143,93);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['CA']['CA_E9'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,515, 128.5, 98,143,98);


$Zeile+=5;
$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','T','L',1,1);
$Spalte-=140;
$Ausdruck->_pdf->SetFillColor(190,190,190);


$Zeile+=20;
$Spalte+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Unterschrift GL/WL','T',0,'C',0);

$Spalte+=120;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL','T',0,'C',0);

//$Zeile+=110;
//$Spalte=15;


//$Ausdruck->_pdf->SetFont('Arial','',6);

//$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
//$Ausdruck->_pdf->Cell2(180,5,'Liste Filialbesuch GBL '.date('m-Y'),'T',0,'L',0);


$Zeile+=5;
$Spalte-=136;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";

$rsGesamt = $DB->RecordSetOeffnen($SQL);

$Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');


$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= "AND PEZ_WERT = '0'";

$GesamtFalsch = $DB->RecordSetOeffnen($SQL);



$Falsch = $GesamtFalsch->FeldInhalt('ANZ');



$Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) *100); 


//echo 'Gesamt:'.$Gesamtanzahl;
//echo 'Falsch:'.$Falsch;
//echo 'Prozent:'.$Prozentsatz;
//die;

if($Prozentsatz >= 89 && $Prozentsatz <= 100)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',87.5),'',0,'C',0);
	$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_hoch.jpg',$Spalte+85,$Zeile-20,'','');
	$Zeile+=6;
	$Spalte=$Spalte+10.5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(160,5,'(GR�N)','',0,'C',0);
	$Spalte=$Spalte-10.5;
	
}
elseif($Prozentsatz >= 79 && $Prozentsatz <= 89)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',87.5),'',0,'C',0);
	$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_hoch.jpg',$Spalte+85,$Zeile-20,'','');
	$Zeile+=6;
	$Spalte=$Spalte+10.5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(160,5,'(GELB)','',0,'C',0);
	$Spalte=$Spalte-10.5;
}
elseif($Prozentsatz < 79)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	//$Ausdruck->_pdf->cell2(180,4,'Hallo','LTRB',0,'L',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_hoch.jpg',$Spalte+85,$Zeile-20,'','');
	
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=6;
	$Spalte=$Spalte+10.5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(159.75,5,'(ROT)','',0,'C',0);
	$Spalte=$Spalte-10.5;
}
$Zeile+=4;
//$Ausdruck->_pdf->setXY($Spalte, $Zeile);
//$Ausdruck->_pdf->cell2(190,5,'Erf�llungsgrad: min. 75 % der Felder, Erf�llungsrating: 100% - 90% = Gr�n, 89% - 80% = Gelb, < 80% = Rot   ','',0,'C',0);
	


}

$Zeile+=149;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Checkliste Autoglas '.date('m-Y'),'T',0,'L',0);

$Ausdruck->Anzeigen();


?>