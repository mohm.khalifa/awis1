<?php

try{
    require_once 'awisDatenbank.inc';
    require_once 'awisBenutzer.inc';
    require_once 'awisRest.inc';

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Benutzer = awisBenutzer::Init('awis_jobs');

    $awisREST = new awisRest('RoleAPI');
    $awisREST->verarbeiteRequest();

    $Request = $awisREST->getRequest();


    //Standardmaessig wird nur der Bereich Vertrieb ausgeliefert. Ueber den Parameter "ALLSECTIONS" koennen alle Bereiche angefragt werden
    $AlleBereiche = (isset($Request['ALLSECTIONS']['WERT']) and $Request['ALLSECTIONS']['WERT'] = 1);

    $SQL  ='SELECT FER_KEY    AS ROLE_KEY,';
    $SQL .='   FER_BEZEICHNUNG AS role,';
    $SQL .='   FER_BEMERKUNG   AS DESCRIPTION,';
    $SQL .='   FER_GUELTIGAB   AS VALIDFROM,';
    $SQL .='   FER_GUELTIGBIS  AS VALIDTO';
    if($AlleBereiche){
        $SQL .=' ,  FILIALEBENENROLLENBEREICHE.FRB_BEZEICHNUNG as SECTION,';
        $SQL .='   FILIALEBENENROLLENBEREICHE.FRB_KEY as SECTION_KEY';
    }
    $SQL .=' FROM FILIALEBENENROLLEN';

    if($AlleBereiche){
        $SQL .=' INNER JOIN FILIALEBENENROLLENBEREICHE';
        $SQL .= ' on FILIALEBENENROLLEN.FER_FRB_KEY  = FILIALEBENENROLLENBEREICHE.FRB_KEY ';
    }

    //Gueltig von:
    if(isset($Request['DATE'])){
        $SQL .=' where FER_GUELTIGAB <= '.$DB->WertSetzen('FER','D',$Request['DATE']['WERT']);
        $SQL .=' and FER_GUELTIGBIS >= '.$DB->WertSetzen('FER','D',$Request['DATE']['WERT']);
    }else{
        $SQL .=' where FER_GUELTIGAB <= sysdate';
        $SQL .=' and FER_GUELTIGBIS >= sysdate';
    }

    //Nur Vertrieb, ausser man will alles
    if(!$AlleBereiche ){
        $SQL .= ' and FER_FRB_KEY = ' . $DB->WertSetzen('FER','Z',8);;
        $SQL .= ' AND FER_KEY in (1,2,3,4,23,25) ';
    }elseif(isset($Request['SECTION_KEY'])){
        $SQL .= ' AND FER_FRB_KEY = ' . $DB->WertSetzen('FER','Z',$Request['SECTION_KEY']['WERT']);
    }

    $rsFilialebenenrollen = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FER'));

    $awisREST->sendeResponse($rsFilialebenenrollen,awisRest::AntwortTyp_JSON);

}

catch(awisException $e){
    var_dump($e);
}catch (Exception $e){
    var_dump($e);
}