<?php
require_once '/daten/web/zukauf/preislisten/preislisten_funktionen.inc';
require_once 'awisMailer.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 06.12.2016
 * Time: 09:20
 */
class awisZukaufpreislisten
{
    private $_ZPL;

    public $MD5Check = true;

    protected $_DebugText = '';

    protected $_DebugLevel = 0;

    private $_ProtokollDB;

    private $_Maske;

    const ImpPfad = '/win/applicationdata2/AWIS/05-ZPLilitaetsgarantien/';

    const ZPLTemp = '/tmp/ZPL/';

    private $_ZPL_KEY = 0;

    private $_ZPL_Pfad = '';


    function __construct($Maske = false, $Benutzer = '')
    {
        $this->_Maske = $Maske;
        $this->_ProtokollDB = awisDatenbank::NeueVerbindung('AWIS');

        $this->_ProtokollDB->Oeffnen();
        $this->_ZPL = new preislisten_funktionen($Benutzer);
        //Debugs nur, wenn der User das Recht hat

        if (!$this->_ZPL->AWISBenutzer->BenutzerOption(1)) {
            $this->DebugLevel(0);
        } else {
            $this->DebugLevel(999);
        }

    }

    function __destruct()
    {
        if ($this->_Maske) {
            $this->_ZPL->Form->DebugAusgabe(1, $this->_DebugText);
        } else {
            echo $this->_DebugText;
        }
    }


    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }


    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText .= date('d.m.Y H:i:s');
            $this->_DebugText .= ': ' . $Typ;
            $this->_DebugText .= ': ' . $Text;
            $this->_DebugText .= PHP_EOL;
        }
    }


    public function ImportiereDatei()
    {
        //Schauen, ob �berhaupt Auftr�ge ausstehen
        $SQL = 'SELECT ZPL_KEY, ZPL_PFAD, ZPL_LIE_NR from ZUKAUFPREISLISTEN where ZPL_STATUS = 1 order by 1 asc';
        $rsZPL = $this->_ZPL->DBi->RecordSetOeffnen($SQL);

        if ($rsZPL->AnzahlDatensaetze() >= 1) {

            if (method_exists($this, '_Importiere' . $rsZPL->FeldInhalt('ZPL_LIE_NR'))) {
                $this->_ZPL_KEY = $rsZPL->FeldInhalt('ZPL_KEY');
                $this->_ZPL_Pfad = $rsZPL->FeldInhalt('ZPL_PFAD');

                eval('$this->_Importiere' . $rsZPL->FeldInhalt('ZPL_LIE_NR') . '();');

            } else {
                $this->_ZPL->addZukaufpreislistenProtokoll($rsZPL->FeldInhalt('ZPL_KEY'), -1);
            }

            $rsZPL->DSWeiter();

            //Sollten noch mehr Preislisten offen sein, Protokolleintrag schreiben.
            while (!$rsZPL->EOF()) {
                $this->_ZPL->addZukaufpreislistenProtokoll($rsZPL->FeldInhalt('ZPL_KEY'), -2);
                $rsZPL->DSWeiter();
            }
        }else{
            $this->debugAusgabe('Keine relevanten Dateien gefunden. :(',1,'WARNING');
        }
    }

    private function _InsertZukaufImportTabelle($LIEFERANT,$LAND, $HSK, $TECDOCID, $HERSTELLER, $ARTIKELNUMMER, $TECDOCARTNR, $BEZEICHNUNG, $GHBRUTTOPREISOMWST,
        $MWST, $VKLISTE, $EKBASIS, $RABATT1, $RABATT2, $RABATT3, $EK, $WAEHRUNG, $MENGENEINHEIT, $EINHEITPROVK, $DATUMAB, $PREISBINDUNGBIS, $BESTELLNUMMER,
        $WARENGRUPPE1, $WARENGRUPPE2, $WARENGRUPPE3, $ALTTEILWERT, $ATUSORTIMENT, $ATUNR, $ATUNROHNEPREIS, $EAN, $GEWICHT, $SPERRE, $SPERRGRUND, $PREISEINHEIT){

        $SQL  ='INSERT INTO ZUKAUFIMPORT';
        $SQL .=' (';
        $SQL .=' ZPL_KEY,';
        $SQL .=' LIEFERANT,';
        $SQL .=' LAND,';
        $SQL .=' HSK,';
        $SQL .=' TECDOCID,';
        $SQL .=' HERSTELLER,';
        $SQL .=' ARTIKELNUMMER,';
        $SQL .=' TECDOCARTNR,';
        $SQL .=' BEZEICHNUNG,';
        $SQL .=' GHBRUTTOPREISOMWST,';
        $SQL .=' MWST,';
        $SQL .=' VKLISTE,';
        $SQL .=' EKBASIS,';
        $SQL .=' RABATT1,';
        $SQL .=' RABATT2,';
        $SQL .=' RABATT3,';
        $SQL .=' EK,';
        $SQL .=' WAEHRUNG,';
        $SQL .=' MENGENEINHEIT,';
        $SQL .=' EINHEITPROVK,';
        $SQL .=' DATUMAB,';
        $SQL .=' PREISBINDUNGBIS,';
        $SQL .=' BESTELLNUMMER,';
        $SQL .=' WARENGRUPPE1,';
        $SQL .=' WARENGRUPPE2,';
        $SQL .=' WARENGRUPPE3,';
        $SQL .=' ALTTEILWERT,';
        $SQL .=' ATUSORTIMENT,';
        $SQL .=' ATUNR,';
        $SQL .=' ATUNROHNEPREIS,';
        $SQL .=' EAN,';
        $SQL .=' GEWICHT,';
        $SQL .=' SPERRE,';
        $SQL .=' SPERRGRUND,';
        $SQL .=' PREISEINHEIT';
        $SQL .=' )';
        $SQL .=' VALUES';
        $SQL .=' (';
        $SQL .= $this->_ZPL->DBi->WertSetzen('ZUK','T',$this->_ZPL_KEY);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$LIEFERANT);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$LAND);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$HSK);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$TECDOCID);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$HERSTELLER);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$ARTIKELNUMMER);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$TECDOCARTNR);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$BEZEICHNUNG);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$GHBRUTTOPREISOMWST);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$MWST);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$VKLISTE);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$EKBASIS);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$RABATT1);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$RABATT2);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$RABATT3);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$EK);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$WAEHRUNG);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$MENGENEINHEIT);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$EINHEITPROVK);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$DATUMAB);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$PREISBINDUNGBIS);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$BESTELLNUMMER);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$WARENGRUPPE1);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$WARENGRUPPE2);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$WARENGRUPPE3);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$ALTTEILWERT);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$ATUSORTIMENT);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$ATUNR);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$ATUNROHNEPREIS);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$EAN);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$GEWICHT);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$SPERRE);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$SPERRGRUND);
        $SQL .= ', ' .  $this->_ZPL->DBi->WertSetzen('ZUK','T',$PREISEINHEIT);
        $SQL .=' )';

        $this->_ZPL->DBi->Ausfuehren($SQL,'',true,$this->_ZPL->DBi->Bindevariablen('ZUK',true));

    }

    private function _Importiere9173(){
        $Commit = true;
        $LIEFERANT = '9173';
        $this->_ZPL->DBi->TransaktionBegin();
        try{
            $this->_ZPL->addZukaufpreislistenProtokoll($this->_ZPL_KEY,2,true);

            $Felder = array();
            $Felder['LIEFERANT'] = 1;
            $Felder['LAND'] = 0;
            $Felder['HSK'] = 29;
            $Felder['TECDOCID'] = 2;
            $Felder['HERSTELLER'] = 3;
            $Felder['ARTIKELNUMMER'] = 4;
            $Felder['TECDOCARTNR'] = 5;
            $Felder['BEZEICHNUNG'] = 6;
            $Felder['GHBRUTTOPREISOMWST'] = 7;
            $Felder['MWST'] = 8;
            $Felder['VKLISTE'] = 9;
            $Felder['EKBASIS'] = 14;
            $Felder['RABATT1'] = 11;
            $Felder['RABATT2'] = 12;
            $Felder['RABATT3'] = 13;
            $Felder['EK'] = 14;
            $Felder['WAEHRUNG'] = 15;
            $Felder['MENGENEINHEIT'] = 16;
            $Felder['EINHEITPROVK'] = 17;
            $Felder['DATUMAB'] = 18;
            $Felder['PREISBINDUNGBIS'] = 19;
            $Felder['BESTELLNUMMER'] = 4;
            $Felder['WARENGRUPPE1'] = 21;
            $Felder['WARENGRUPPE2'] = 22;
            $Felder['WARENGRUPPE3'] = 23;
            $Felder['ALTTEILWERT'] = 25;
            $Felder['EAN'] = 26;
            $Felder['PREISEINHEIT'] = 28;

            $Datei = glob($this->_ZPL_Pfad . '/*.csv')[0];

            $fp = fopen($Datei,'r');
            if($fp===false){
            	throw new Exception('Datei konnte nicht ge�ffnet werden! Breche ab!!!', -1);
            }
            fgets($fp);
            $i = 0;
            while(!feof($fp)){
                echo ++$i . PHP_EOL;
            	$Zeile = explode(';',fgets($fp));

                $LAND= $Zeile[$Felder['LAND']];
                $HSK= $Zeile[$Felder['HSK']];
                $TECDOCID= $Zeile[$Felder['TECDOCID']];
                $HERSTELLER= $Zeile[$Felder['HERSTELLER']];
                $ARTIKELNUMMER= $Zeile[$Felder['ARTIKELNUMMER']];
                $TECDOCARTNR= $Zeile[$Felder['TECDOCARTNR']];
                $BEZEICHNUNG= $Zeile[$Felder['BEZEICHNUNG']];
                $GHBRUTTOPREISOMWST= $Zeile[$Felder['GHBRUTTOPREISOMWST']];
                $MWST= $Zeile[$Felder['MWST']];
                $VKLISTE= $Zeile[$Felder['VKLISTE']];
                $EKBASIS= $Zeile[$Felder['EKBASIS']];
                $RABATT1= $Zeile[$Felder['RABATT1']];
                $RABATT2= $Zeile[$Felder['RABATT2']];
                $RABATT3= $Zeile[$Felder['RABATT3']];
                $EK= $Zeile[$Felder['EK']];
                $WAEHRUNG= $Zeile[$Felder['WAEHRUNG']];
                $MENGENEINHEIT= $Zeile[$Felder['MENGENEINHEIT']];
                $EINHEITPROVK= $Zeile[$Felder['EINHEITPROVK']];
                $DATUMAB= $Zeile[$Felder['DATUMAB']];
                $PREISBINDUNGBIS= $Zeile[$Felder['PREISBINDUNGBIS']];
                $BESTELLNUMMER= $Zeile[$Felder['BESTELLNUMMER']];
                $WARENGRUPPE1= $Zeile[$Felder['WARENGRUPPE1']];
                $WARENGRUPPE2= $Zeile[$Felder['WARENGRUPPE2']];
                $WARENGRUPPE3= $Zeile[$Felder['WARENGRUPPE3']];
                $ALTTEILWERT= $Zeile[$Felder['ALTTEILWERT']];
                $ATUSORTIMENT= '';
                $ATUNR= '';
                $ATUNROHNEPREIS= '';
                $EAN= $Zeile[$Felder['EAN']];
                $GEWICHT= '';
                $SPERRE= '';
                $SPERRGRUND= '';
                $PREISEINHEIT= $Zeile[$Felder['PREISEINHEIT']];

                $this->_InsertZukaufImportTabelle($LIEFERANT,$LAND, $HSK, $TECDOCID, $HERSTELLER, $ARTIKELNUMMER, $TECDOCARTNR, $BEZEICHNUNG, $GHBRUTTOPREISOMWST,
                    $MWST, $VKLISTE, $EKBASIS, $RABATT1, $RABATT2, $RABATT3, $EK, $WAEHRUNG, $MENGENEINHEIT, $EINHEITPROVK, $DATUMAB, $PREISBINDUNGBIS, $BESTELLNUMMER,
                    $WARENGRUPPE1, $WARENGRUPPE2, $WARENGRUPPE3, $ALTTEILWERT, $ATUSORTIMENT, $ATUNR, $ATUNROHNEPREIS, $EAN, $GEWICHT, $SPERRE, $SPERRGRUND, $PREISEINHEIT);
            }


            $this->_ZPL->addZukaufpreislistenProtokoll($this->_ZPL_KEY, 3);
        }catch (awisException $ex){
            $this->_handleErrors($ex,'9173');
        }catch (Exception $ex){
            $this->_handleErrors($ex,'9173');
        }

        if($Commit) {
            $this->_ZPL->DB->TransaktionCommit();
            $this->debugAusgabe('Fertig und commitet!' , 1, 'Info');
            $this->_ZPL->AWISWerkzeug->EMail('shuttle@de.atu.eu', 'OK Zukaufpreislisten-ImportschemaImport', $this->_DebugText, 1, '', 'awis@de.atu.eu');
        }else{
            $this->_ZPL->DB->TransaktionRollback();
            $this->debugAusgabe('Fehler aufgetreten und rollbackt! Logdatei komplett durchlesen! Anzahl Datens�tze ' . $Protokoll['DSANZ'], 1, 'FEHLER');
            $this->_ZPL->AWISWerkzeug->EMail('shuttle@de.atu.eu', 'ERROR ZPLI-Import', 'RUNTER SCROLLEN!' . PHP_EOL .  $this->_DebugText, 1, '', 'awis@de.atu.eu');
        }
    }

    /**
     * @param $e awisException
     */
    private function _handleErrors($e, $Zusatzinfo = '')
    {
        $this->debugAusgabe('Nicht behandelter Fehler aufgetreten!' , 1, 'Fehler');
        $this->_ZPL->DBi->TransaktionRollback();
        if($Zusatzinfo!=''){
            $this->debugAusgabe($Zusatzinfo , 1, 'Fehler');
        }

    }

    protected function _PruefeImportProtokoll($Dateiname)
    {

        if ($this->MD5Check == false) {
            return true;
        }

        $SQL = 'SELECT XDI_KEY,';
        $SQL .= '   XDI_BEREICH,';
        $SQL .= '   XDI_DATEINAME,';
        $SQL .= '   XDI_DATUM,';
        $SQL .= '   XDI_MD5,';
        $SQL .= '   XDI_BEMERKUNG,';
        $SQL .= '   XDI_USER,';
        $SQL .= '   XDI_USERDAT';
        $SQL .= ' FROM IMPORTPROTOKOLL ';
        $SQL .= ' WHERE XDI_BEREICH = \'ZPL\' ';
        $SQL .= ' AND XDI_MD5 = ' . $this->_ProtokollDB->WertSetzen('XDI', 'T', sha1_file($Dateiname));

        $rsXDI = $this->_ProtokollDB->RecordSetOeffnen($SQL, $this->_ProtokollDB->Bindevariablen('XDI'));

        if ($rsXDI->AnzahlDatensaetze() > 0) {
            $this->debugAusgabe('Laut Pr�fsumme, wurde die Datei ' . $Dateiname . ' bereits importiert. 
            XDI_KEY: ' . $rsXDI->FeldInhalt('XDI_KEY') . ' 
            XDI_DATEINAME: ' . $rsXDI->FeldInhalt('XDI_DATEINAME') . ' 
            XDI_DATUM: ' . $rsXDI->FeldInhalt('XDI_DATUM') . ' 
            Sollte es sich hierbei um einen Fehler handeln, Job erneut mit dem Parameter --ignore md5 aufrufen', 1, 'Fehler');

            return false;
        } else {
            $this->debugAusgabe('Pr�fsumme der Datei unbekannt. ', 999, 'Debug');

            return true;
        }
    }

    /**
     * Schreibt die MD5 ins ImportProtokoll
     *
     * @param $Dateiname
     */
    protected function _SchreibeImportProtokoll($Dateiname)
    {
        
        $SQL = 'INSERT';
        $SQL .= ' INTO IMPORTPROTOKOLL';
        $SQL .= '   (';
        $SQL .= '     XDI_BEREICH,';
        $SQL .= '     XDI_DATEINAME,';
        $SQL .= '     XDI_DATUM,';
        $SQL .= '     XDI_MD5,';
        $SQL .= '     XDI_USER,';
        $SQL .= '     XDI_USERDAT';
        $SQL .= '   )';
        $SQL .= '   VALUES';
        $SQL .= '   (';
        $SQL .= '     \'ZPL\'';
        $SQL .= ',' . $this->_ProtokollDB->WertSetzen('XDI', 'T', $Dateiname);
        $SQL .= ', sysdate ';
        $SQL .= ',' . $this->_ProtokollDB->WertSetzen('XDI', 'T', sha1_file($Dateiname));
        $SQL .= ', \'AWIS\'';
        $SQL .= ', sysdate ';
        $SQL .= '   )';

        $this->_ProtokollDB->Ausfuehren($SQL, '', true, $this->_ProtokollDB->Bindevariablen('XDI'));
        $this->debugAusgabe('Importiere Datei:' . $Dateiname, 1, 'Info');
        $SQL = ' select seq_xdi_key.currval from dual';

        return $this->_ProtokollDB->RecordSetOeffnen($SQL)->FeldInhalt(1);
    }






}