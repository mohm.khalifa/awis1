<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $_POST;
global $_GET;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,150);
if(($RechteStufe & 16)==0)
{
    awisEreignis(3,1000,'Listen Telefonverzeichnis',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

if(!isset($_POST['txtKategorie']))
{
	echo '<Form name=frmTelefon method=post action=./telefon_Main.php>';
	echo '<table border=0 width=100%>';

	echo '<tr><td id=RegisterInhalt colspan=2>Parameter</td></tr>'	;
	echo "<tr><td  id=RegisterInhalt width=180>K<u>a</u>tegorie:</td>";
	echo "<td id=RegisterInhalt><select name=txtKategorie accesskey='a' size=1 tabindex=10>";
	echo "<option value=-1 selected>Alle</option>";
	
	$KatRechte = "0" . awisBenutzerRecht($con,151);
	$rsKategorien=awisOpenRecordset($con,"SELECT * FROM KontakteKategorien WHERE BITAND(KKA_KZG_ID,$KatRechte)=KKA_KZG_ID ORDER BY KKA_BEZEICHNUNG");
	$rsKategorienZeilen = $awisRSZeilen;
	
	for($i=0;$i<$rsKategorienZeilen;$i++)
	{
		echo "<option " . ($i==0?"selected":"") . " value=" . $rsKategorien["KKA_KEY"][$i] . ">" . $rsKategorien["KKA_BEZEICHNUNG"][$i] . "</option>";
	}
	if($KatRechte & 8192)
	{
		echo "<option value=0>Alle</option>";
	}
	
	echo "</select></td></tr>";
	unset($rsKategorien);		// Speicher freigeben
	
		// Bereich 
		
	$rsBereiche=awisOpenRecordset($con,"SELECT * FROM KontakteBereiche WHERE BITAND(KBE_KZG_ID,$KatRechte)=KBE_KZG_ID ORDER BY KBE_BEZEICHNUNG");
	$rsBereicheZeilen = $awisRSZeilen;
	
	echo "<tr><td id=RegisterInhalt width=180>B<u>e</u>reich:</td>";
	echo "<td id=RegisterInhalt ><select name=txtBereich accesskey='e' size=1 tabindex=10>";
	echo "<option value=-1 selected>Alle</option>";
	
	for($i=0;$i<$rsBereicheZeilen;$i++)
	{
		echo "<option value=" . $rsBereiche["KBE_KEY"][$i] . ">" . $rsBereiche["KBE_BEZEICHNUNG"][$i] . "</option>";
	}
	echo "</select></td></tr>";
	unset($rsBereiche);		// Speicher freigeben
	
	// Abteilungen
	
	echo "<tr><td id=RegisterInhalt width=180>ATU A<u>b</u>teilung:</td>";
	echo "<td id=RegisterInhalt><select name=txtAbteilung accesskey='b' size=1 tabindex=10>";
	echo "<option value=-1>Alle</option>";
	
	$rsAbteilungen=awisOpenRecordset($con,"SELECT * FROM KontakteAbteilungen WHERE BITAND(KAB_KZG_ID,$KatRechte)=KAB_KZG_ID ORDER BY KAB_ABTEILUNG");
	$rsAbteilungenZeilen = $awisRSZeilen;
	for($i=0;$i<$rsAbteilungenZeilen;$i++)
	{
		echo "<option value=" . $rsAbteilungen["KAB_KEY"][$i] . ">" . $rsAbteilungen["KAB_ABTEILUNG"][$i] . "</option>";
	}
	echo "</select></td></tr>";
	unset($rsAbteilungen);

		// Geb�ude
	echo "<tr><td id=RegisterInhalt width=180>ATU <u>G</u>eb�ude:</td>";
	echo "<td id=RegisterInhalt><select name=txtGebaeude accesskey='g' size=1 tabindex=50>";
	echo "<option value=-1 checked>Alle</option>";
	
	$rsGebaeude=awisOpenRecordset($con,"SELECT * FROM Gebaeude ORDER BY GEB_BEZEICHNUNG");
	$rsGebaeudeZeilen = $awisRSZeilen;
	for($i=0;$i<$rsGebaeudeZeilen;$i++)
	{
		echo "<option value=" . $rsGebaeude["GEB_ID"][$i] . ">" . $rsGebaeude["GEB_BEZEICHNUNG"][$i] . "</option>";
	}
	echo "</select></tr>";

		// Listen
		//	1: Kurzwahlliste
	echo "<tr><td id=RegisterInhalt width=180><u>L</u>iste:</td>";
	echo "<td id=RegisterInhalt><select name=txtListe accesskey='l' size=1 tabindex=99>";

	echo "<option value=1 checked>Kurzwahlliste</option>";
	echo "<option value=2 >Mitarbeiterliste</option>";

	echo "</select></td></tr>";
	
	
	echo '</table>';
	
	
	echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
	echo '</Form>';
}
else
{
	$Bedingung='';
	
	if($_POST['txtKategorie']!=-1)
	{
		$Bedingung = ' AND KON_KKA_KEY=0' . $_POST['txtKategorie'];
	}
	
	if($_POST['txtBereich']!=-1)
	{
		$Bedingung .= ' AND KON_KBE_KEY=0' . $_POST['txtBereich'];
	}

	if($_POST['txtAbteilung']!=-1)
	{
		$Bedingung .= ' AND KON_KAB_KEY=0' . $_POST['txtAbteilung'];
	}

	if($_POST['txtGebaeude']!=-1)
	{
		$Bedingung .= ' AND KON_GEB_ID=0' . $_POST['txtGebaeude'];
	}


			// Datei �ffnen
	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );
	
			/*******************************
			* Listen
			*******************************/

	if($_POST['txtListe']==1)		// Kurzwahlliste
	{
		/*
	    $SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KKA_BEZEICHNUNG, KBE_BEZEICHNUNG, KAB_ABTEILUNG, KKO_KURZWAHL, KKO_KOT_KEY, KKO_WERT, KON_BEMERKUNG ";
		$SQL .= " FROM Kontakte, KontakteABTEILUNGEN, KontakteBEREICHE, KontakteKATEGORIEN, KontakteKOMMUNIKATION, KontakteFAHRZEUGE ";
		$SQL .= " WHERE (((KON_KAB_KEY = KAB_KEY(+) AND KBE_KEY(+) = KON_KBE_KEY AND KON_KKA_KEY = KKA_KEY(+) AND KKO_KON_KEY(+) = KON_KEY AND KON_KEY = KFA_KON_KEY(+))) ";
		$SQL .= " AND (KKO_KURZWAHL IS NOT NULL AND KKO_KURZWAHL <> 0)";
		$SQL .= " " . $Bedingung . ")";
		$SQL .= " ORDER BY KON_NAME1, KON_NAME2";
		*/

		$SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KKA_BEZEICHNUNG, KBE_BEZEICHNUNG, KKO_KURZWAHL, KKO_KOT_KEY, KKO_WERT, KON_BEMERKUNG ";
		$SQL .= " FROM Kontakte, KontakteABTEILUNGEN, KontakteAbteilungenZuordnungen, KontakteBEREICHE, KontakteKATEGORIEN, KontakteKOMMUNIKATION, KontakteFAHRZEUGE ";
		$SQL .= " WHERE (((KON_KEY = KZA_KON_KEY AND KZA_KAB_KEY = KAB_KEY AND KBE_KEY(+) = KON_KBE_KEY AND KON_KKA_KEY = KKA_KEY(+) AND KKO_KON_KEY(+) = KON_KEY AND KON_KEY = KFA_KON_KEY(+))) ";
		$SQL .= " AND (KKO_KURZWAHL IS NOT NULL AND KKO_KURZWAHL <> 0)";
		$SQL .= " " . $Bedingung . ")";
		$SQL .= " ORDER BY KON_NAME1, KON_NAME2";
		
		$rsKontakte = awisOpenRecordset($con, $SQL);
	    $rsKontakteAnz = $awisRSZeilen;
	
		echo '<table border=1 width=100%>';
		echo '<tr>';
		echo '<td id=FeldBez>Kurzwahl</td>';
		echo '<td id=FeldBez>Ruf</td>';
		echo '<td id=FeldBez>Namen</td>';
		echo '<td id=FeldBez>Kategorie</td>';
		echo '<td id=FeldBez>Bemerkung</td>';
	
		echo '</tr>';
	
		fputs($fd, "Kurzwahl;Ruf;Name;Kategorie;Bemerkung\n");
	
		for($i=0;$i<$rsKontakteAnz;$i++)
		{
			$Zeile='';
			echo '<tr>';
			echo '<td>' . $rsKontakte['KKO_KURZWAHL'][$i] . '</td>';
			$Zeile .= $rsKontakte['KKO_KURZWAHL'][$i];
	
			echo '<td>' . $rsKontakte['KKO_WERT'][$i] . '</td>';
			$Zeile .= ';' . $rsKontakte['KKO_WERT'][$i];
			
			echo '<td>' . $rsKontakte['KON_NAME1'][$i];
			$Zeile .= ';' . $rsKontakte['KON_NAME1'][$i];
	
			if($rsKontakte['KON_NAME2'][$i]!='')
			{
				echo ', ' . $rsKontakte['KON_NAME2'][$i];
				$Zeile .= ', ' . $rsKontakte['KON_NAME2'][$i];
			}
			echo '</td>';
			
			echo '<td>' . $rsKontakte['KKA_BEZEICHNUNG'][$i] . '</td>';
			$Zeile .= '; ' . $rsKontakte['KKA_BEZEICHNUNG'][$i];
	
			echo '<td>' . $rsKontakte['KON_BEMERKUNG'][$i] . '</td>';
			$Zeile .= '; ' . $rsKontakte['KON_BEMERKUNG'][$i];
			
			$Zeile .= "\n";
			fputs($fd, $Zeile);
	
		}
		echo '</table>';
	}	// Ende Kurzwahlliste
	
	elseif($_POST['txtListe']==2)		// Mitarbeiterliste
	{
		/*
	    $SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KON_ZUSTAENDIGKEIT, GEB_ID, GEB_BEZEICHNUNG, KAB_ABTEILUNG ";
		$SQL .= " FROM Kontakte, KontakteABTEILUNGEN, GEBAEUDE ";
		$SQL .= " WHERE ((KON_KAB_KEY = KAB_KEY(+) AND KON_GEB_ID = GEB_ID) ";
		$SQL .= " " . $Bedingung . ")";
		$SQL .= " ORDER BY KAB_ABTEILUNG, KON_NAME1, KON_NAME2";
		*/
		
		$SQL = "SELECT DISTINCT KON_KEY, KON_NAME1, KON_NAME2, KON_ZUSTAENDIGKEIT, GEB_ID, GEB_BEZEICHNUNG, KAB_ABTEILUNG ";
		$SQL .= " FROM KONTAKTE, KONTAKTEABTEILUNGEN, KONTAKTEABTEILUNGENZUORDNUNGEN, GEBAEUDE ";
		$SQL .= " WHERE ((KON_KEY = KZA_KON_KEY AND KZA_KAB_KEY = KAB_KEY AND KON_GEB_ID = GEB_ID) AND BITAND(KON_LISTEN,1)=1 ";
		$SQL .= " " . $Bedingung . ")"; 
		$SQL .= " ORDER BY KAB_ABTEILUNG, KON_NAME1, KON_NAME2";
		
		awis_Debug(1,$SQL);
		
		$rsKontakte = awisOpenRecordset($con, $SQL);
	    $rsKontakteAnz = $awisRSZeilen;
	
		echo '<table border=1 width=100%>';
		echo '<tr>';
		echo '<td id=FeldBez>Mitarbeiter</td>';
		echo '<td id=FeldBez>Zust�ndigkeit</td>';
		echo '<td id=FeldBez>Geb�ude</td>';
		echo '<td id=FeldBez>Telefon</td>';
		echo '<td id=FeldBez>Fax</td>';
		echo '<td id=FeldBez>E-Mail</td>';
	
		echo '</tr>';
	
		fputs($fd, "Abteilung;Mitarbeiter;Zust�ndigkeit;Geb�ude;Telefon;Fax;E-Mail\n");
	
		$LetzteAbteilung = '';
		
		for($i=0;$i<$rsKontakteAnz;$i++)
		{
			$Zeile='';
			echo '<tr>';
			
			$Zeile .= $rsKontakte['KAB_ABTEILUNG'][$i];		// Immer beim Export
			
			if($LetzteAbteilung!=$rsKontakte['KAB_ABTEILUNG'][$i])
			{
			
				echo '<tr><td colspan=6 id=FeldBez>' . $rsKontakte['KAB_ABTEILUNG'][$i] . '</td></tr>';
				$LetzteAbteilung=$rsKontakte['KAB_ABTEILUNG'][$i];
			}

			echo '<td>' . $rsKontakte['KON_NAME1'][$i];
			$Zeile .= '; ' . $rsKontakte['KON_NAME1'][$i];
			if($rsKontakte['KON_NAME2'][$i]!='')
			{
				echo ', '. $rsKontakte['KON_NAME2'][$i];
				$Zeile .= ', ' . $rsKontakte['KON_NAME2'][$i];
			}
			echo '</td>';


			echo '<td>' . $rsKontakte['KON_ZUSTAENDIGKEIT'][$i] . '</td>';
			$Zeile .= '; ' . $rsKontakte['KON_ZUSTAENDIGKEIT'][$i];

			echo '<td>' . $rsKontakte['GEB_ID'][$i] . '</td>';
			$Zeile .= '; ' . $rsKontakte['GEB_BEZEICHNUNG'][$i];

				// Durchwahl Telefon
			echo '<td>';
			$rsKonTel = awisOpenRecordset($con, "SELECT KKO_KON_KEY, KKO_WERT FROM KONTAKTEKOMMUNIKATION WHERE KKO_KOT_KEY=9 AND KKO_KON_KEY=" . $rsKontakte['KON_KEY'][$i]);
			echo $rsKonTel['KKO_WERT'][0];
			$Zeile .= '; ' . $rsKonTel['KKO_WERT'][0];
			echo '</td>';

				// Durchwahl Fax
			echo '<td>';
			$rsKonTel = awisOpenRecordset($con, "SELECT KKO_KON_KEY, KKO_WERT FROM KONTAKTEKOMMUNIKATION WHERE KKO_KOT_KEY=10 AND KKO_KON_KEY=" . $rsKontakte['KON_KEY'][$i]);
			echo $rsKonTel['KKO_WERT'][0];
			$Zeile .= '; ' . $rsKonTel['KKO_WERT'][0];
			echo '</td>';

				// Durchwahl E-Mail
			echo '<td>';
			$rsKonTel = awisOpenRecordset($con, "SELECT KKO_KON_KEY, KKO_WERT FROM KONTAKTEKOMMUNIKATION WHERE KKO_KOT_KEY=7 AND KKO_KON_KEY=" . $rsKontakte['KON_KEY'][$i]);
			echo $rsKonTel['KKO_WERT'][0];
			$Zeile .= '; ' . $rsKonTel['KKO_WERT'][0];
			echo '</td>';
			
			$Zeile .= "\n";
			fputs($fd, $Zeile);

		}	
		echo '</table>';

			// Schl�ssel 
		$rsGebaeude=awisOpenRecordset($con,"SELECT * FROM Gebaeude ORDER BY GEB_BEZEICHNUNG");
		$rsGebaeudeZeilen = $awisRSZeilen;
		echo '<br><font size=2>Orte: ';
		for($i=0;$i<$rsGebaeudeZeilen;$i++)
		{
			echo $rsGebaeude["GEB_ID"][$i] . "=" . $rsGebaeude["GEB_BEZEICHNUNG"][$i] . " // ";
		}
		echo "</font>";


	
	}	// Ende Mitarbeiterliste
	
	// Datei ausgeben
	
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';

}
	
if(isset($_GET['ZurueckLink']))
{
	$ZurueckLink = $_GET['ZurueckLink'];
}
else
{
	$ZurueckLink = '/auswertungen/auswertungen_Main.php';
}
echo "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . $ZurueckLink . "';>";

echo "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

// JAVA Skript zum Cursor positionieren

if(!isset($_POST['txtListe']))
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('txtKategorie')[0].focus();";
	echo "\n</Script>";
}
?>
</body>
</html>

