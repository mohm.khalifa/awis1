<html>
<head>
<link rel="SHORTCUT ICON" href="http://atlx22su81/favicon.ico">
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>

<body>
<?php
require_once("db.inc.php");		// DB-Befehle
require_once("register.inc.php");
require_once("sicherheit.inc.php");
global $AWISBenutzer;
								// Benutzerdefinierte CSS Datei
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();

        // Beginn Hauptmen�
print "<table border=0 width=100%><tr><td><h1 id=SeitenTitel>Stammdaten</h1></td><td align=right></td></tr></table>";

        // �berschrift der Tabelle
print "<table border=0 width=100%><tr><th align=left id=FeldBez>Aktion</th><th align=left id=FeldBez>Beschreibung</th></tr>";

	// Lieferantenstammdaten
if(awisBenutzerRecht($con, 605)>0)
{
	//echo "<tr><td><a href=./lieferanten.php>Lieferanten</a></td><td>Liste aller Lieferanten.</td></tr>";
	echo "<tr><td><a href=./lieferanten/lieferanten_Main.php>Lieferanten</a></td><td>Liste aller Lieferanten.</td></tr>";
}

	// Hersteller
if(awisBenutzerRecht($con, 602)>0)
{
	echo "<tr><td><a href=./hersteller.php>Hersteller</a></td><td>Liste aller Hersteller.</td></tr>";
}

	// Hersteller
if(awisBenutzerRecht($con, 604)>0)
{
	echo "<tr><td><a href=./textkonserven.php>Textkonserven</a></td><td>Anpassen der Textkonserven f�r AWIS.</td></tr>";
}
	// Filialebenen
if(awisBenutzerRecht($con, 3800)>0)
{
	echo "<tr><td><a href=/filialebenen/filialebenen_Main.php>Filialebenen</a></td><td>Verwalten der Filialebenen und der Rollen.</td></tr>";
}
	// Feitertage
if(awisBenutzerRecht($con, 22000)>0)
{
	echo "<tr><td><a href=/stammdaten/feiertage/feiertage_Main.php>Feiertage</a></td><td>Feiertage f&uuml;r die Filialen pflegen und exportieren.</td></tr>";
}

print "</table>";	//Ende Hauptmen�

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";

awisLogoff($con);

?>

</body>
</html>

