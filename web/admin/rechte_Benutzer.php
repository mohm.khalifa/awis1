<?php
global $PHP_AUTH_USER;
global $con;
global $_POST;
global $_GET;

	$RechteStufe = awisBenutzerRecht($con, 20);
	if($RechteStufe==0)
	{
		awisEreignis(3,1000,'Rechte�bersicht: Benutzer',$_SERVER['PHP_AUTH_USER'],'','','');
	    die("Keine ausreichenden Rechte!");
	}

	print "<table width=100%";

	print "<tr><td width=100>Benutzer</td><td>";
	if($RechteStufe & 2) 
	{
		$SQL = "SELECT XBN_KEY, XBN_VollerName FROM Benutzer ORDER BY XBN_VollerName";
		
		$rsBenutzer = awisOpenRecordset($con, $SQL);
		$rsBenutzerAnz = $awisRSZeilen;
		
		print "<select name=txtBenutzerID onchange='document.frmRechte.submit();'>";
		if(!isset($_POST["txtBenutzerID"]))
		{
			print "<option selected value=0>Bitte w�hlen...</option>";
		}
		for($i=0;$i<$rsBenutzerAnz;$i++)
		{
			print "<option value=" . $rsBenutzer["XBN_KEY"][$i];
			if(isset($_POST["txtBenutzerID"]) AND $rsBenutzer["XBN_KEY"][$i] == $_POST["txtBenutzerID"])
			{
				print " selected ";
			}
			print ">" . $rsBenutzer["XBN_VOLLERNAME"][$i] . "</option>";
		}

		print "</select>";
		// Benutzer
		$BNKEY = '' . (isset($_REQUEST["txtBenutzerID"])?$_REQUEST["txtBenutzerID"]:'');
	}	
	else
	{
		print "<input type=hidden name=txtBenutzerID value=" . awisBenutzerID() . ">" . $_SERVER['PHP_AUTH_USER'];
		$BNKEY = awisBenutzerID();
	}
	print "</td></tr>";
	print "</table>";
	
	if($BNKEY != '')
	{
		$RechteStufeBenutzerVerwaltung = awisBenutzerRecht($con, 1);

		$SQL = "SELECT * FROM V_ACCOUNTRECHTE WHERE XBN_KEY=" . $BNKEY . " ORDER BY XRC_RECHT ";
		$rsBenutzerRechte = awisOpenRecordset($con, $SQL);

		print "<h3>Vergebene Rechte f�r Benutzer '" . $rsBenutzerRechte["XBL_LOGIN"][0] . "'<h3>";
		
		print "<table border=1 width=510>";
		print "<tr><td id=FeldBez>Recht</td><td id=FeldBez>Rechte-ID</td><td id=FeldBez>Rechte-Stufe</td></tr>";
		for($i=0;$i<$awisRSZeilen;$i++)
		{
			print "<tr>";
			print "<td width=300>" . $rsBenutzerRechte["XRC_RECHT"][$i] . "</td>";
			if($RechteStufeBenutzerVerwaltung>0)
			{
				print "<td width=100><a href=/hilfe/hilfe_Main.php?HilfeThema=rechte&HilfeBereich=" . $rsBenutzerRechte["XRC_ID"][$i] . ">" . $rsBenutzerRechte["XRC_ID"][$i] . "</a></td>";
			}
			else
			{
				print "<td width=100>" . $rsBenutzerRechte["XRC_ID"][$i] . "</td>";
			}
			print "<td width=100>" . $rsBenutzerRechte["XBA_STUFE"][$i] . "</td>";
			print "</tr>";
		
		}
		print "</table>";
	}

?>