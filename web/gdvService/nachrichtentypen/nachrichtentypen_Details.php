<?php
/**
 * Details Stammdatenpflege GDV-Nachrichtentypen
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607061358
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('GDV','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht44002 = $AWISBenutzer->HatDasRecht(44002);
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GDV_NT'));

	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1);

	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'VVE_BEZEICHNUNG';
	}
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}
	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	if($Recht44002==0)
	{
		$Form->Fehler_KeineRechte();
	}

	if(isset($_GET['VVW_VERSNR']))
	{
		$AWIS_KEY1 = $_GET['VVW_VERSNR'];
	}
	else
	{
		$AWIS_KEY1='';
	}

	if(isset($_POST['txtAWIS_KEY1'])) {
		$AWIS_KEY1 = $_POST['txtAWIS_KEY1'];
	}

	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	if(isset($_POST['cmdSuche_x']))
	{
		$Param['VNT_VERSICHERUNG'] = 	$Form->Format('N0',$_POST['sucVNT_VERSICHERUNG'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
	if($FelderDel != '' or isset($_POST['cmdLoeschenOK']) or isset($_POST['cmdLoeschen_x']))
	{
		include('./nachrichtentypen_loeschen.php');
	}
	//Speichernbutton
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./nachrichtentypen_speichern.php');
	}
	if($FelderNew != '')
	{
		include('./nachrichtentypen_speichern.php');
	}
	/* Daten beschaffen */
	$SQL  ='SELECT';
	$SQL .='   VVE_VERSNR,VVE_BEZEICHNUNG, ';
	$SQL .='   row_number() OVER (';
	$SQL .= ' order by ' . $Param['ORDER'] .' )  AS ZeilenNr';
	$SQL .=' FROM';
	$SQL .='   (';
	$SQL .='     SELECT';
	$SQL .='      distinct VVE_VERSNR,VVE_BEZEICHNUNG';
	$SQL .='     FROM';
	$SQL .='       versnachrichtentypen';
	$SQL .='    inner join versversicherungen on VNT_VERSNR = VVE_VERSNR';
	$SQL .='   )daten';

	if ((isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '') and !isset($_POST['cmdSpeichern_x']) and !isset($_POST['cmdLoeschenOK']) and !isset($_POST['cmdLoeschen_x']))
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}

	if($AWIS_KEY1 != '' and !isset($_POST['cmdSpeichern_x']))
	{
		if(!isset($_POST['cmdSuche_x'])){
			$SQL .= ' WHERE VVE_VERSNR='.$AWIS_KEY1;
		}
		else{
			$SQL .= ' AND VVE_VERSNR='.$AWIS_KEY1;
		}
	}

	if(isset($_POST['sucVNT_VERSICHERUNG']) and isset($_POST['cmdSuche_x']))
	{
		$AWIS_KEY1 = $_POST['sucVNT_VERSICHERUNG'];
	}
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Blättern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

	//AWIS_KEY1 ist leer, wenn man über die Suche kommt. Wenn gespeichert wird(AWIS_KEY1 ist dann nicht leer), kommt man wieder in die Liste, deswegen auch die Blockparameter hernehmen.
	if($AWIS_KEY1 == '' or (isset($_POST['cmdSpeichern_x'])))
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsVVW = $DB->RecordSetOeffnen($SQL);
	// Spaltenbreiten für Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['BES_AUFTRAGSART'] = 500;
	$FeldBreiten['BES_VERALTET'] = 20;

	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 4);
	}
	$Gesamtbreite += 20;

	$Form->SchreibeHTMLCode('<form name="frmVVWDetails" action="./nachrichtentypen_Main.php?cmdAktion=Details'.' " method=POST  enctype="multipart/form-data">');
	$Form->Formular_Start();

	if (($rsVVW->AnzahlDatensaetze() > 1))
	{
		$FeldBreiten = array();
		$FeldBreiten['VERSICHERUNG'] = 350;
		$Param['BLOCK']=1;

		$count = 0;
		$Gesamtbreite = '';
		foreach ($FeldBreiten as $value)
		{
			$Gesamtbreite += $value;
			$count++;
		}

		$Gesamtbreite += 41;

		$Form->ZeileStart();
		$Icons = array();
		$Icons[] = array('new','./nachrichtentypen_Main.php?cmdAktion=Details&VVW_VERSNR=-1','g');
		$Form->Erstelle_ListeIcons($Icons,38,0);

		$Link= '';

		$Link = './nachrichtentypen_Main.php?cmdAktion=Details&Sort=VVE_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VVE_BEZEICHNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'], $FeldBreiten['VERSICHERUNG'], '', $Link);
		$Form->ZeileEnde();
		$DS = 0;
		while(! $rsVVW->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			$Icons[] = array('edit','./nachrichtentypen_Main.php?cmdAktion=Details&VVW_VERSNR='.$rsVVW->FeldInhalt('VVE_VERSNR'));
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			$TTT = $rsVVW->FeldInhalt('VVE_BEZEICHNUNG');
			$Form->Erstelle_ListenFeld('VVE_BEZEICHNUNG', $rsVVW->FeldInhalt('VVE_BEZEICHNUNG'), 0, $FeldBreiten['VERSICHERUNG'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$Form->ZeileEnde();
			$DS++;
			$rsVVW->DSWeiter();
		}

		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['GDV']['SummeGesamtDS'].': '.$Form->Format('N0',$rsVVW->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './nachrichtentypen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
	}
	elseif($rsVVW->AnzahlDatensaetze() == 1 or $AWIS_KEY1 == '-1')
	{
		/* Daten beschaffen */
		$SQL ='     SELECT';

		if($rsVVW->FeldInhalt('VVE_VERSNR') == '')
		{
			$VVE_VERSNR ='-1';
			$SQL .='   distinct VVW_VERSNR,VVE_BEZEICHNUNG';
		}
		else{
			$VVE_VERSNR=$rsVVW->FeldInhalt('VVE_VERSNR');
			$SQL .='   VVW_VERSNR,VVE_BEZEICHNUNG,VVW_USER,VVW_USERDAT';
		}

		$SQL .='     FROM';
		$SQL .='       versversandweg';
		$SQL .='    inner join versversicherungen on VVW_VERSNR = VVE_VERSNR';

		$SQL2 =' where VVE_VERSNR ='.$VVE_VERSNR;

		if($rsVVW->FeldInhalt('VVE_VERSNR') == '')
		{
			$SQL3 = ' left join versnachrichtentypen on VVW_VERSNR = VNT_VERSNR ';
			$SQL3 .= ' WHERE VNT_VERSNR is null ';
			$SQL3 .= ' AND VVW_RELEASE is not null ';
		}
		else{
			$SQL3 = '';
		}

		$rsVVE = $DB->RecordSetOeffnen($SQL.$SQL2);

		$edit = true;
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./nachrichtentypen_Main.php?cmdAktion=Details  accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVVE->FeldInhalt('VVW_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsVVE->FeldInhalt('VVW_USERDAT'));
		$Form->InfoZeile($Felder,'');


		if($rsVVW->AnzahlDatensaetze() == 1 and $AWIS_KEY1 != '-1') {
			$edit = false;
		}
		else
		{
			$edit = true;
		}



		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'].':',200);
		$Form->Erstelle_SelectFeld('!VVW_VERSICHERUNG',$rsVVW->FeldInhalt('VVE_VERSNR'),150,$edit,$SQL.$SQL3,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'','','','',array(),'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();

	    if($AWIS_KEY1 != '-1') {
			$FeldBreiten = array();
			$FeldBreiten['Blank'] = 20;
			$FeldBreiten['Nummer'] = 130;
			$FeldBreiten['Nachrichtentyp'] = 250;


			$SQL = ' select GDN_GDR_KEY,VNT_KEY,GDN_KEY,VNT_GDN_NUMMER,GDN_BEZEICHNUNG from versnachrichtentypen ';
			$SQL .= ' inner join VERSVERSANDWEG on vnt_versnr = vvw_versnr';
			$SQL .= ' inner join gdvrelease on vvw_release = gdr_release';
			$SQL .= ' inner join gdvnachrichtentypen on VNT_GDN_NUMMER = GDN_NUMMER and GDR_KEY = GDN_GDR_KEY';
			$SQL .= ' where VNT_VERSNR =' . $VVE_VERSNR;
			$SQL .= ' order by VNT_GDN_NUMMER';
			$Form->Erstelle_HiddenFeld('AWIS_KEY1', $VVE_VERSNR);

			$rsZuord = $DB->RecordSetOeffnen($SQL);

			$SQL2 = ' select GDN_NUMMER,GDN_NUMMER || \'~\' || GDN_BEZEICHNUNG from gdvnachrichtentypen where gdn_key not in  (select GDN_KEY from (' . $SQL . ')) and GDN_GDR_KEY =' . $rsZuord->FeldInhalt('GDN_GDR_KEY');
			$SQL2 .= ' order by GDN_NUMMER';

			$rsList = $DB->RecordSetOeffnen($SQL2);
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['GDN_NNUMMER'], $FeldBreiten['Nummer'], '',
				'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['GDV']['GDN_BEZEICHNUNG'],
				$FeldBreiten['Nachrichtentyp'], '', '');
			$Form->ZeileEnde();
			$DS = 0;    // für Hintergrundfarbumschaltung

			while (!$rsZuord->EOF()) {
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array('delete', 'POST~' . $rsZuord->FeldInhalt('VNT_KEY'));
				$Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2));
				$TTT = $rsZuord->FeldInhalt('VNT_GDN_NUMMER');
				$Form->Erstelle_ListenFeld('GDN_NNUMMER', $rsZuord->FeldInhalt('VNT_GDN_NUMMER'), 0,
					$FeldBreiten['Nummer'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
				$TTT = $rsZuord->FeldInhalt('GDN_BEZEICHNUNG');
				$Form->Erstelle_ListenFeld('GDN_BEZEICHNUNG', $rsZuord->FeldInhalt('GDN_BEZEICHNUNG'), 0,
					$FeldBreiten['Nachrichtentyp'], false, ($DS % 2), '', '', 'T', 'L', $TTT);
				$Form->ZeileEnde();
				$DS++;
				$rsZuord->DSWeiter();
			}

			if ($rsList->AnzahlDatensaetze() > 0) {
				$Form->ZeileStart();
				$Icons = array();
				$Icons[] = array('add', 'POST~' . $AWIS_KEY1);
				$Form->Erstelle_ListeIcons($Icons, 20, ($DS % 2));
				$Form->Erstelle_SelectFeld('GDN_NACHRICHTENTYPEN', '', '500:380', true, $SQL2, '', '', '', '', '', '',
					'', array(), 'font-size:14px;height:22px');
				$Form->ZeileEnde();
			}

			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
		else{

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['GDN_BEZEICHNUNG'].':',180);
			$Form->Erstelle_SelectFeld('!GDN_NACHRICHTENTYPEN',1, 500, true,
				'***GDV_Daten;txtVVW_VERSICHERUNG;VVE_VERSNR=*txtVVW_VERSICHERUNG&ZUSATZ=~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],
				'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'', '');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();

			$Form->Erstelle_HiddenFeld('Key',$AWIS_KEY1);
		}

	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	// Zurück zum Menü
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/gdvService/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if($AWIS_KEY1 == '-1')//Speichern wenn und 1(Detailansicht) oder kein (Neuanlage) Datensatz gefunden wurden
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if ($rsVVW->AnzahlDatensaetze() == 1 and $AWIS_KEY1 != '-1')
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	$AWISBenutzer->ParameterSchreiben('Formular_GDV_NT',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{

	if($Form instanceof awisFormular)
	{

		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $DB;
	global $Form;

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';


	if(isset($Param['VNT_VERSICHERUNG']) AND ($Param['VNT_VERSICHERUNG']!='0' and $Param['VNT_VERSICHERUNG']!=''))
	{
		$Bedingung .= ' AND VVE_VERSNR ' . $DB->LikeOderIst($Param['VNT_VERSICHERUNG']);
	}



	return $Bedingung;
}

?>