<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $rsErgebnis;
global $AWISSprachKonserven;
global $meldung;
global $Fehler;

try
{
	$AWISBenutzer = awisBenutzer::Init();
	
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Seite = 1;
	
	
	$script = "<script type='text/javascript'>
	    	      	
	function Wechsel() {
			
			if(document.getElementById('SELECT').style.display == 'none')
			{
				document.getElementById('AREA').style.display = 'none';
				document.getElementById('SELECT').style.display = 'block';
			}
			else
			{
				document.getElementById('AREA').style.display = 'block';
				document.getElementById('SELECT').style.display = 'none';
			}	
		}
	function Uebergabe(obj){
		var checkText = obj.options[obj.selectedIndex].text;
		if(checkText == '::bitte wählen::')
		{
			checkText = '';
		}
		document.getElementsByName('txtBES_BEMERKUNGEN')[0].value = checkText;
		document.getElementById('AREA').style.display = 'block';
		document.getElementById('SELECT').style.display = 'none';
	}
	
	</script>";
	
	echo $script;
	
	
	$FeldBreiten = array();
	$FeldBreiten['DATUM'] = 90;
	$FeldBreiten['BEMERKUNGEN'] = 650;
	$FeldBreiten['BETRAG'] = 120;
	$FeldBreiten['ZAHLUNGSART'] = 120;
	$FeldBreiten['EINTRAG'] = 120;

	
	$SQL=	'select ID,DATUM,BEMERKUNGEN,BETRAG,ZAHLUNGSART,EINTRAG,BNUSERDAT, ';	
	$SQL .='row_number() OVER (';
	$SQL .= ' order by ID asc';
	$SQL .= ') AS ZeilenNr from( ' ;
	$SQL .= 'select * from BEARBEITUNGSSTAND_NEW ';	
	$SQL .= ' where BEARBNRNEU='.$rsErgebnis->FeldInhalt('BEARBNRNEU');
	
	if(isset($_GET['ID']) or $AWIS_KEY2 != '')
	{
		if(isset($_GET['ID']))
		{
			$SQL .= ' and ID ='.$_GET['ID'];
		}
		else 
		{
			$SQL .= ' and ID ='.$AWIS_KEY2;
		}
	}
	//$SQL .= ' order by DATUM';
	$SQL .= ')';
		
		
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	
	$Recht40005 = $AWISBenutzer->HatDasRecht(40005);
	
	
	if($Recht40005==0)
	{
		$Form->Fehler_KeineRechte();
		die;
	}
	
	
	// Zum Blättern in den Daten
	if (isset($_REQUEST['Block']))
	{
		$Block = $Form->Format('N0', $_REQUEST['Block'], false);
			
	}
		
		
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	$rsINFO = $DBSCHAD->RecordSetOeffnen($SQL);
		
	$Form->DebugAusgabe(1, $DBSCHAD->LetzterSQL());

	

	if(!isset($_GET['ID']) and $AWIS_KEY2 == '')
	{
		if($meldung != '')
		{
			$Form->Hinweistext($meldung);
		}
		else
		{
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
		$Form->ZeileStart('width:100%;');
		if(($Recht40005&8)==8)
		{
			$Icons = array();
			$Icons[] = array('new','./schaden_Main.php?cmdAktion=Details&Seite=Info&ID=-1&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'],'g');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}
		elseif((($Recht40005&2)==2 or ($Recht40005&4)==4))
		{
			$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['Blank'], '', '');
		}	
			
			
		$Link= '';
		// Überschrift der Listenansicht mit Sortierungslink: Filiale
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_DATUM'], $FeldBreiten['DATUM'], '', $Link);
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_LANG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_LANG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEMERKUNG'], $FeldBreiten['BEMERKUNGEN'], '', $Link);
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMK_AKTIV,QMK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMK_AKTIV,QMK_KEY'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BETRAG'].' EUR', $FeldBreiten['BETRAG'], '', '');
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGAB'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGAB'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_ZAHLUNGSART'], $FeldBreiten['ZAHLUNGSART'], '', $Link);
		// Überschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
		//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Sort=QMG_GUELTIGBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMG_GUELTIGBIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_EINTRAG'], $FeldBreiten['EINTRAG'], '', $Link);
		
		$Form->ZeileEnde();
			
		$DS = 0;
		while(! $rsINFO->EOF())
		{
				
			$Form->ZeileStart('width:100%');
			if((($Recht40005&2)==2 or ($Recht40005&4)==4))
			{
				$Icons = array();
				if(($Recht40005&2)==2)
				{
					$Icons[] = array('edit','./schaden_Main.php?cmdAktion=Details&Seite=Info&ID='.$rsINFO->FeldInhalt('ID').'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR']);
				}
				if(($Recht40005&4)==4)
				{
					$Icons[] = array('delete','./schaden_Main.php?cmdAktion=Details&Seite=Info&BEARBNEU='.$rsErgebnis->FeldInhalt('BEARBNRNEU').'&Del='.$rsINFO->FeldInhalt('ID').'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR']);
				}
				$Form->Erstelle_ListeIcons($Icons,38,($DS%2));
			}
			else 
			{
				if(($Recht40005&8)==8)
				{
					$Form->Erstelle_ListenFeld('BES_BLANK','', 0, $FeldBreiten['Blank'], false, ($DS%2), '','', 'T', 'L','');
				}
			}
			$TTT = $rsINFO->FeldInhalt('DATUM');
			$Form->Erstelle_ListenFeld('BES_DATUM',$Form->Format('D',$rsINFO->FeldInhalt('DATUM')), 0, $FeldBreiten['DATUM'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsINFO->FeldInhalt('BEMERKUNGEN');
			$Form->Erstelle_Textarea('BES_BEMERKUNGEN', $rsINFO->FeldInhalt('BEMERKUNGEN'), $FeldBreiten['BEMERKUNGEN'], 80, 1,true,'','','onFocus="textareaAuf(this)"onFocusout="textareaZu(this)";','readonly');
			
			//$Form->Erstelle_ListenFeld('BES_BEMERKUNGEN',strlen($rsINFO->FeldInhalt('BEMERKUNGEN')) > 100 ? substr($rsINFO->FeldInhalt('BEMERKUNGEN'),0,99).'...':$rsINFO->FeldInhalt('BEMERKUNGEN'), 0, $FeldBreiten['BEMERKUNGEN'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsINFO->FeldInhalt('BETRAG').($rsINFO->FeldInhalt('BETRAG') == ''?'':' Eur');
			$Form->Erstelle_ListenFeld('BES_BETRAG',$Form->Format('N2',$rsINFO->FeldInhalt('BETRAG')).($rsINFO->FeldInhalt('BETRAG') == ''?'':'&euro;'), 0, $FeldBreiten['BETRAG'], false, ($DS%2), '','', 'T', 'R', $TTT);
			$TTT =  $rsINFO->FeldInhalt('ZAHLUNGSART');
			$Form->Erstelle_ListenFeld('BES_ZAHLUNGSART',$rsINFO->FeldInhalt('ZAHLUNGSART'), 0, $FeldBreiten['ZAHLUNGSART'], false, ($DS%2), '','', 'T', 'L', $TTT);
			$TTT =  $rsINFO->FeldInhalt('EINTRAG');
			$Form->Erstelle_ListenFeld('BES_ZAHLUNGSART',$rsINFO->FeldInhalt('EINTRAG'), 0, $FeldBreiten['EINTRAG'], false, ($DS%2), '','', 'T', 'L', $TTT);
				
			$Form->ZeileEnde();
			$DS++;
			$rsINFO->DSWeiter();
		}
			
			
		$Link = './schaden_Main.php?cmdAktion=Details&Seite='.$_GET['Seite'].'&BEARBEITUNGSNR='.$_GET['BEARBEITUNGSNR'];
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	else 
	{
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./schaden_Main.php?cmdAktion=Details&Seite=Info&BEARBEITUNGSNR=".$_GET['BEARBEITUNGSNR']." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsINFO->FeldInhalt('EINTRAG'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsINFO->FeldInhalt('BNUSERDAT'));
		$Form->ZeileStart();
		$Form->InfoZeile($Felder,'');
		$Form->ZeileEnde();
		
		$FehlerText = '';
		if(($Fehler&1) == 1)
		{
			$FehlerText .= $AWISSprachKonserven['BES']['FEHLER_BETRAG'];
		}
		if(($Fehler&2) == 2)
		{
			if($FehlerText != '')
			{
				$FehlerText.= '<br>';
			}
			$FehlerText .= $AWISSprachKonserven['BES']['FEHLER_DATUM'];
		}
		if($FehlerText != '')
		{
			$Form->Hinweistext($FehlerText);
		}
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_DATUM'] . ':',160);
		$Form->Erstelle_TextFeld('BES_DATUM',(isset($_POST['txtBES_DATUM'])?$_POST['txtBES_DATUM']:($rsINFO->FeldInhalt('DATUM') == ''?date('d.m.Y',time()):$rsINFO->FeldInhalt('DATUM'))), 10, 223,true, '','', '','D', 'L','','',10);
		$Form->ZeileEnde();
		
		$AWISCursorPosition = 'txtBES_DATUM';
		
		$Form->ZeileStart('display:block','AREA"ID="AREA');
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEMERKUNG'] . ':',160);
		$Form->Erstelle_Textarea('BES_BEMERKUNGEN',isset($_POST['txtBES_BEMERKUNGEN'])?$_POST['txtBES_BEMERKUNGEN']:$rsINFO->FeldInhalt('BEMERKUNGEN'),750,90,3,true);
		$Form->Schaltflaeche('script', 'cmdHinzufuegen','onclick="Wechsel();"', '/bilder/icon_plus.png"ID="IMG_LOAD', $AWISSprachKonserven['BES']['lbl_Wechsel'], 'W','',18,18);
		$Form->ZeileEnde();
		$Form->ZeileEnde();
		
		
		$Form->ZeileStart('display:none','SELECT"ID="SELECT');
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEMERKUNG'] . ':',160);
		$SQL ='select BEM,BEM as BEMTEXT from VORSCHLAG_BEM order by BEM';
		$Form->Erstelle_SelectFeld('BES_BEMERKUNGENS','',630,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','onchange=Uebergabe(this);','',array(),'','SCHAD');
		$Form->Schaltflaeche('script', 'cmdHinzufuegen','onclick="Wechsel();"', '/bilder/icon_minus.png"ID="IMG_LOAD', $AWISSprachKonserven['BES']['lbl_Wechsel'], 'W','position:relative;left:2px;',18,18);
		$Form->ZeileEnde();
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRAG'] . ':',160);
		$Form->Erstelle_TextFeld('BES_BETRAG',isset($_POST['txtBES_BETRAG'])?$_POST['txtBES_BETRAG']:$Form->Format('N2',$rsINFO->FeldInhalt('BETRAG')), 10, 95,true, '','text-align:right', '','T', 'L');
		$Form->Erstelle_TextLabel('&euro;',160,'font-weight:bolder');
		$Form->ZeileEnde();
	

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ZAHLUNGSART'] . ':',160);
		$SQL ='select WERT,WERT as TextWert from ZAHLUNG_DURCH order by WERT';
		$Form->Erstelle_SelectFeld('BES_ZAHLUNGSART',isset($_POST['txtBES_ZAHLUNGSART'])?$_POST['txtBES_ZAHLUNGSART']:$rsINFO->FeldInhalt('ZAHLUNGSART'),630,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->ZeileEnde();

		$Form->Erstelle_HiddenFeld('BEARBNRNEU',$rsErgebnis->FeldInhalt('BEARBNRNEU'));
		if(isset($_GET['ID']))
		{
			$KEY =	$_GET['ID']; 
		}
		else 
		{
			$KEY = $AWIS_KEY2;
		}
		$Form->Erstelle_HiddenFeld('ID',$KEY);
		
		$Form->ZeileStart();
		$Form->Trennzeile('O');
		$Form->ZeileEnde();
		
		/*
		$TTT =  $rsINFO->FeldInhalt('ART');
		$Form->Erstelle_ListenFeld('BES_ART',$rsINFO->FeldInhalt('ART'), 0, $FeldBreiten['ART'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$TTT =  $rsINFO->FeldInhalt('STATUS');
		$Form->Erstelle_ListenFeld('BES_STATUS',$rsINFO->FeldInhalt('STATUS'), 0, $FeldBreiten['STATUS'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$TTT =  $rsINFO->FeldInhalt('VERSNR');
		$Form->Erstelle_ListenFeld('BES_VERSNR',$rsINFO->FeldInhalt('VERSNR'), 0, $FeldBreiten['VERSNR'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$TTT =  $rsINFO->FeldInhalt('ZUSATZINFO');
		$Form->Erstelle_ListenFeld('BES_ZUSATZINFO',$rsINFO->FeldInhalt('ZUSATZINFO'), 0, $FeldBreiten['ZUSATZINFO'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$Form->ZeileEnde();
		*/
	}
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>