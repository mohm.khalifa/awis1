--------------------------------------------------------
--  Datei erstellt -Dienstag-Februar-07-2017   
--------------------------------------------------------
SET DEFINE OFF;
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = 'Bei R�ckfragen wenden Sie sich bitte telefonisch an Frau Bettina Gottschalk (Durchwahl -5805)<br>
oder Herrn Siegfried Sch�nberger (Durchwahl -5456)<br><br>'
WHERE XTX_KENNUNG = 'mail_TextBlock03'
AND XTX_BEREICH   = 'TAG';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = 'f�r nachfolgende Abnahmen konnte kein Kassiervorgang festgestellt werden,
<br>
bzw. wurde eine Betragsdifferenz festgestellt.
<br>
Bitte pr�fen Sie alle Positionen, auch anhand des Leistungsnachweises und senden das Dokument <b>umgehend</b> vollst�ndig<br>
ausgef�llt an die Fax-Nr. #111 934 1585 zur�ck.<br><br>'
WHERE XTX_KENNUNG = 'mail_TextBlock01'
AND XTX_BEREICH   = 'TAG';
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE   = 'Fehlerliste Abnahmegesellschaften',
XTX_BEMERKUNG = 'Fehlerliste Abnahmegesellschaften'
WHERE XTX_KENNUNG = 'PDF_OffeneInkasso'
AND XTX_BEREICH   = 'TAG';
update berichteparameter set xrp_wert = 50
where xrp_xre_key = 34 and xrp_parameter = 'Spalte01'; 
commit;

