<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Awis - Kontakte</title>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Cache-Control" content="no-cache" />
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
include ("ATU_Header.php");	// Kopfzeile


awis_ZeitMessung(0);

if(!awisHatDasRecht(150))
{
    awisEreignis(3,1000,'Telefonbuch',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$cmdAktion=(isset($_GET['cmdAktion'])?$_GET['cmdAktion']:'');
//print "<form name=frmTelefon method=POST action=./telefon_Main.php?cmdAktion=Liste>";
if(isset($_REQUEST['cmdSuche_x']))
{
	$cmdAktion='Liste';
}

if(isset($_POST['cmdXReset_x']))
{
	awis_BenutzerParameterSpeichern($con, "TelefonSuche", $AWISBenutzer->BenutzerName(),'');
	$cmdAktion = 'Suche';
}

/************************************************
* Daten anzeigen
************************************************/

//$SpeichernButton = False;

awis_RegisterErstellen(3, $con, $cmdAktion);

print "<input type=hidden name=cmdAktion value=Liste><br>";

//print "</form>";		// Form schlie�en wegen Schaltfl�chen

print "<input type=image title='Zur�ck (Alt+z)' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
if((awisBenutzerRecht($con,150)&2) == 2)		// Hinzuf�gen erlaubt?
{
	print "&nbsp;<input type=image accesskey=n title='Hinzuf�gen (Alt+N)' src=/bilder/plus.png name=cmdDSNeu onclick=location.href='./telefon_Main.php?cmdAktion=Liste&txtKONKey=-1&DSNeu=True'>";
}

		// Formulardaten l�schen
if($cmdAktion=="Suche" OR $cmdAktion=="")
{
	print "&nbsp;<input title='Parameter l�schen (Alt+r)' accesskey=r name=cmdXReset type=image src=/bilder/radierer.png onclick=location.href='./telefon_Main.php?cmdAktion=Reset'>";
}


print "&nbsp;&nbsp;<input type=image title='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=telefon','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";



//include "debug_info.php";
awislogoff($con);

?>
</body>
</html>

