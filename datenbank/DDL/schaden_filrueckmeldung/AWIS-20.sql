--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into WEBMASKENREGISTER
SET DEFINE OFF;
Insert into WEBMASKENREGISTER (XRG_ID,XRG_XWM_ID,XRG_BEZEICHNUNG,XRG_BESCHRIFTUNG,XRG_AKTION,XRG_SEITE,XRG_BEMERKUNG,XRG_XRC_ID,XRG_RECHTESTUFE,XRG_SORTIERUNG,XRG_NEUEHILFE,XRG_BILD) values ('400152','40015','Auswertungen','Auswertungen','./schadfilrueck_Main.php?cmdAktion=Auswertungen','./schadfilrueck_Auswertungen.php','Auswertung für die Filialrückmeldungen der Schadensdatenbank','40015','32','20','0',null);
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into TEXTKONSERVEN
SET DEFINE OFF;
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('reg_400152','Register','Registerbeschriftung','Auswertungen','Auswertungen','Auswertungen','Auswertungen','Auswertungen','Auswertungen','Auswertungen',to_date('25.01.2017 12:23:13','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('ttt_400152','Register','Auswertung Filialrückmeldung','Auswertung Filialrückmeldung','Auswertung Filialrückmeldung','Auswertung Filialrückmeldung','Auswertung Filialrückmeldung','Auswertung Filialrückmeldung','Auswertung Filialrückmeldung','Auswertung Filialrückmeldung',to_date('25.01.2017 12:22:00','DD.MM.RRRR HH24:MI:SS'));
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into RECHTESTUFEN
SET DEFINE OFF;
Insert into RECHTESTUFEN (XRS_XRC_ID,XRS_BIT,XRS_BESCHREIBUNG) values ('40015','5','Auswertungsreiter Anzeigen');
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into PROGRAMMPARAMETER
SET DEFINE OFF;
Insert into PROGRAMMPARAMETER (XPP_ID,XPP_BEZEICHNUNG,XPP_DEFAULT,XPP_UEBERSCHREIBBAR,XPP_BEMERKUNG,XPP_DATENQUELLE,XPP_KODIERT,XPP_XRC_ID,XPP_STUFE,XPP_DATENTYP) values ('200128','Formular_BES_Auswertung','a:0:{}','255','Parameter für die SchadDB Filrürckmeldung Asuwertung',null,'1','0',null,'T');
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into BERICHTE
SET DEFINE OFF;
Insert into BERICHTE (XRE_KEY,XRE_BEZEICHNUNG,XRE_KLASSENNAME,XRE_FORMAT,XRE_AUSRICHTUNG,XRE_VORLAGE,XRE_XRC_ID,XRE_STUFE,XRE_PROTOKOLLEINTRAG,XRE_STATUS,XRE_BEMERKUNG,XRE_USER,XRE_USERDAT) values ('70','SchadFilialrueckmeldungen','ber_SchadFilRueck','A4','P',null,'40015','5','1','A','Filialrückmeldung Auswertung','Tobias Schäffler',to_date('26.01.2017 11:38:06','DD.MM.RRRR HH24:MI:SS'));
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into BERICHTEPARAMETER
SET DEFINE OFF;
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('953','70','Tabellenfeldhoehe','4','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('954','70','BreiteZwischenPos1','10','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('955','70','BreiteZwischenPos2','50','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('956','70','BreiteZwischenPos3','50','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('957','70','BreiteZwischenPos4','132','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('928','70','LinkerRand','10','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('929','70','Schriftart','Arial','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('930','70','Schriftgroesse','8','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('931','70','Seite1-Oben','10','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('932','70','Zeilenhoehe','5','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('933','70','PosLogo','250','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('934','70','FooterStart','190','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('935','70','FooterEigentum','120','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('936','70','FooterErstellt','250','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('937','70','Schriftgroesse2','7','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('938','70','Schriftgroesse3','6','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('939','70','Seite1-Ueberschrift','20','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('940','70','BreiteVUFIL','10','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('941','70','BreiteKennung','50','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('942','70','BreiteBEARBNR','15','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('943','70','Tabellenhoehe','8','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('944','70','BreiteAntragsart','35','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('945','70','BreiteBearbFil','15','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('946','70','BreiteFallkosten','20','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('947','70','BreiteUrsprAuftrag','23','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('948','70','BreiteEingabedatum','20','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('949','70','BreiteWorangearb','23','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('950','70','BreiteWasBesch','23','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('951','70','BreiteAusfallUrsach','25','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
Insert into BERICHTEPARAMETER (XRP_KEY,XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER,XRP_USERDAT) values ('952','70','BreiteKFZ','18','Tobias Schäffler',to_date('26.01.2017 14:30:58','DD.MM.RRRR HH24:MI:SS'));
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
REM INSERTING into TEXTKONSERVEN
SET DEFINE OFF;
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_ATUNR','BES','ATUNR','ATUNR','ATUNR','ATUNR','ATUNR','ATUNR','ATUNR','ATUNR',to_date('17.09.2015 17:15:21','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_AUSFALLUR','BES','Ausfallursache','Ausfallursache','Ausfallursache','Ausfallursache','Ausfallursache','Ausfallursache','Ausfallursache','Ausfallursache',to_date('21.09.2015 09:33:59','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_BEARBFIL','BES','Bearb-Fil','Bearb-Fil','Bearb-Fil','Bearb-Fil','Bearb-Fil','Bearb-Fil','Bearb-Fil','Bearb-Fil',to_date('24.11.2014 00:00:00','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_BESCHAEDIGT','BES','Beschädigt','Beschädigt','Beschädigt','Beschädigt','Beschädigt','Beschädigt','Beschädigt','Beschädigt',to_date('24.11.2014 00:00:00','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_EINGABEDATUM','BES','Eingabedatum','Eingabedatum','Eingabedatum','Eingabedatum','Eingabedatum','Eingabedatum','Eingabedatum','Eingabedatum',to_date('31.01.2017 09:26:57','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_FALLKOSTEN','BES','Fallkosten','Fallkosten','Fallkosten','Fallkosten','Fallkosten','Fallkosten','Fallkosten','Fallkosten',to_date('17.09.2015 17:15:21','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_GEARBEITET','BES','gearbeitet','gearbeitet','gearbeitet','gearbeitet','gearbeitet','gearbeitet','gearbeitet','gearbeitet',to_date('21.09.2015 08:32:10','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_URSPR','BES','Urspr.','Urspr.','Urspr.','Urspr.','Urspr.','Urspr.','Urspr.','Urspr.',to_date('17.09.2015 17:15:21','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_URSPRAUFTRAG','BES','Auftrag','Auftrag','Auftrag','Auftrag','Auftrag','Auftrag','Auftrag','Auftrag',to_date('17.09.2015 17:15:21','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_WAS','BES','Was','Was','Was','Was','Was','Was','Was','Was',to_date('21.09.2015 11:42:40','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('BES_WORAN','BES','Woran','Woran','Woran','Woran','Woran','Woran','Woran','Woran',to_date('21.09.2015 08:32:10','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('DATUM_BIS','BES','Datum bis','Datum bis','Datum bis','Datum bis','Datum bis','Datum bis','Datum bis','Datum bis',to_date('26.01.2017 11:01:03','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('DATUM_VON','BES','Datum von','Datum von','Datum von','Datum von','Datum von','Datum von','Datum von','Datum von',to_date('26.01.2017 11:00:22','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('PDF_FOOTER_EIGENTUM','BES','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.','Dieser Audruck ist Eigentum der A.T.U Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.',to_date('21.09.2015 00:00:00','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('PDF_FOOTER_HINWEIS','BES','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.','Hinweis: Sie können zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.',to_date('21.09.2015 00:00:00','DD.MM.RRRR HH24:MI:SS'));
Insert into TEXTKONSERVEN (XTX_KENNUNG,XTX_BEREICH,XTX_BEMERKUNG,XTX_TEXT_DE,XTX_TEXT_CZ,XTX_TEXT_NL,XTX_TEXT_IT,XTX_TEXT_FR,XTX_TEXT_CH,XTX_TEXT_AT,XTX_USERDAT) values ('PDF_FOOTER_LEGENDE','BES','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale','Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale',to_date('21.09.2015 00:00:00','DD.MM.RRRR HH24:MI:SS'));
commit;
--------------------------------------------------------
--  Datei erstellt -Mittwoch-Februar-01-2017   
--------------------------------------------------------
SET DEFINE OFF;
UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Bearb.-Nr',XTX_TEXT_CZ = 'Bearb.-Nr',XTX_TEXT_NL = 'Bearb.-Nr',XTX_TEXT_IT = 'Bearb.-Nr',XTX_TEXT_FR = 'Bearb.-Nr',XTX_TEXT_CH = 'Bearb.-Nr',XTX_TEXT_AT = 'Bearb.-Nr' WHERE XTX_KENNUNG = 'BES_BEARBNR' AND XTX_BEREICH   = 'BES';
commit;