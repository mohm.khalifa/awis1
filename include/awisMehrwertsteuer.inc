<?php
require_once 'awisFormular.inc';
/**
 *
 * Class awisMehrwertsteuer
 */
class awisMehrwertsteuer
{

    const RUECKGABETYP_PROZENT_ARRAY = 1;
    const RUECKGABETYP_BEZEICHNUNGS_ARRAY = 2;
    const RUECKGABETYP_OBJEKT_ARRAY = 3;

    const TYP_ALLE = '';
    const TYP_KEINE = 'k';
    const TYP_VOLL = 'v';
    const TYP_REDUZIERT = 'r';
    const TYP_UNBEKANNT = 'u';
    const TYP_ALTTEILSTEUER_VOLL = 'a';

    /**
     * @var awisDatenbank
     */
    private $DB;

    /**
     * @var awisFormular
     */
    private $Form;

    /**
     * @var array mit den Steuersaetze-Objekten
     */
    private $Steuersaetze;

    /**
     * @var array mit den Ids der einzelnen Laender
     */
    private $LANSteuersaetze;

    /**
     * @var array mit den Ids der einzelnen Typen
     */
    private $TypSteuersatze;

    /**
     * @var array mit allen Ids
     */
    private $SteuerIds;

    /**
     * @var string mit Datum
     */
    private $GueltigAm;

    /**
     * awisMehrwertsteuer constructor.
     *
     * @param awisDatenbank $DB
     * @param string        $GueltigAm Datum, zu welchem die S�tze g�ltig sein sollen. Wenn leer, dann heute
     */
    function __construct(awisDatenbank $DB, $GueltigAm = '')
    {
        $this->DB = $DB;
        $this->Form = new awisFormular();

        if ($GueltigAm == '') {
            $GueltigAm = date('d.m.Y');
        }

        $this->init($GueltigAm);
    }

    /**
     * Initialisiert die Daten
     *
     * @param string $GueltigAm Datum, zu welchem die S�tze g�ltig sein sollen.
     * @return boolean
     */
    public function init($GueltigAm)
    {
        if($GueltigAm == $this->GueltigAm){
            return true;
        }

        if(!$this->Form->PruefeDatum($GueltigAm,false,false,false, '0')){
            return false;
        }


        $this->GueltigAm = $GueltigAm;
        $this->Steuersaetze = null;
        $this->LANSteuersaetze = null;
        $this->TypSteuersatze = null;
        $this->SteuerIds = null;

        $SQL = 'select * from MEHRWERTSTEUERSAETZE ';
        $SQL .= ' where trunc(MWS_GUELTIGAB) <= ' . $this->DB->WertSetzen('MWS', 'D', $GueltigAm);
        $SQL .= ' and trunc(MWS_GUELTIGBIS) >= ' . $this->DB->WertSetzen('MWS', 'D', $GueltigAm);

        $rsMWS = $this->DB->RecordSetOeffnen($SQL, $this->DB->Bindevariablen('MWS'));

        while (!$rsMWS->EOF()) {

            $Steuersatz = new Steuersatz();
            $Steuersatz->setId($rsMWS->FeldInhalt('MWS_ID'));
            $Steuersatz->setBezeichnung($rsMWS->FeldInhalt('MWS_BEZEICHNUNG'));
            $Steuersatz->setSatz($rsMWS->FeldInhalt('MWS_SATZ'));
            $Steuersatz->setGueltigAb($rsMWS->FeldInhalt('MWS_GUELTIGAB'));
            $Steuersatz->setGueltigBis($rsMWS->FeldInhalt('MWS_GUELTIGBIS'));
            $Steuersatz->setLanCode($rsMWS->FeldInhalt('MWS_LAN_CODE'));
            $Steuersatz->setTyp($rsMWS->FeldInhalt('MWS_TYP'));

            //Steuersatz ins gro�e Haupt-Array
            $this->Steuersaetze[$rsMWS->FeldInhalt('MWS_ID')] = $Steuersatz;

            //ID ins L�nder, Steuer und "alle Ids" array
            $this->LANSteuersaetze[$rsMWS->FeldInhalt('MWS_LAN_CODE')][$rsMWS->FeldInhalt('MWS_ID')] = $rsMWS->FeldInhalt('MWS_ID');
            $this->TypSteuersatze[$rsMWS->FeldInhalt('MWS_TYP')][$rsMWS->FeldInhalt('MWS_ID')] = $rsMWS->FeldInhalt('MWS_ID');
            $this->SteuerIds[$rsMWS->FeldInhalt('MWS_ID')] = $rsMWS->FeldInhalt('MWS_ID');

            $rsMWS->DSWeiter();
        }

        return true;
    }

    /**
     * @param string $LAN_CODE
     * @param string $Typ
     * @param        $Rueckgabetyp
     *
     * @return array
     *
     * Gibt ein Array zur�ck mit folgenden aufbau $Return[MWS_ID] = awisMehrwertsteuer::RUECKGABETYP_XXXX
     */
    public function Steuersatz($LAN_CODE = '', $Typ = '', $Rueckgabetyp = self::RUECKGABETYP_PROZENT_ARRAY)
    {

        //Warum auch immer keine Daten? => Schlecht!
        if (is_null($this->SteuerIds)) {
            return false;
        }

        if ($LAN_CODE != '' and isset($this->LANSteuersaetze[$LAN_CODE])) {
            //Wenn Land �bergeben und das K�rzel in der DB Stand, dann alle zutreffenden Ids merken
            $LAN_CODE_Ids = $this->LANSteuersaetze[$LAN_CODE];
        } elseif ($LAN_CODE != '' and !isset($this->LANSteuersaetze[$LAN_CODE])) {
            //Es wurde ein Land �bergeben, was wir gar nicht kennen => Schlecht!
            return false;
        } else {
            //Kein Land �bergeben => Somit Alle Ids
            $LAN_CODE_Ids = $this->SteuerIds;
        }

        if ($Typ != '' and isset($this->TypSteuersatze[$Typ])) {
            //Wenn Typ �bergeben und das K�rzel in der DB Stand, dann alle zutreffenden Ids merken
            $Typ_Ids = $this->TypSteuersatze[$Typ];
        } elseif ($Typ != '' and !isset($this->TypSteuersatze[$Typ])) {
            //Es wurde ein Typ �bergeben, den wir gar nicht kennen => Schlecht!
            return false;
        } else {
            //Kein Typ �bergeben => Somit Alle Ids
            $Typ_Ids = $this->SteuerIds;
        }

        //Die Schnittmenge der beiden Arrays beinhaltet die richtigen S�tze
        $TrefferIds = array_intersect($Typ_Ids, $LAN_CODE_Ids);

        $Return = array();
        foreach ($TrefferIds as $Id) {
            $Satz = array();
            $Satz['id'] = $Id;
            switch ($Rueckgabetyp) {
                case self::RUECKGABETYP_BEZEICHNUNGS_ARRAY:
                    $Satz['wert'] = $this->Steuersaetze[$Id]->getBezeichnung();
                    break;
                case self::RUECKGABETYP_OBJEKT_ARRAY:
                    $Satz['wert'] = $this->Steuersaetze[$Id];
                    break;
                case self::RUECKGABETYP_PROZENT_ARRAY;
                    $Satz['wert'] = $this->Steuersaetze[$Id]->getSatz();
                    break;
            }

            $Return[] = $Satz;
        }

        return $Return;
    }
}

class Steuersatz
{

    /**
     * @var int
     */
    private $Id;

    /**
     * @var string
     */
    private $Lan_code;

    /**
     * @var string
     */
    private $Bezeichnung;

    /**
     * @var float
     */
    private $Satz;

    /**
     * @var string
     */
    private $GueltigAb;

    /**
     * @var string
     */
    private $GueltigBis;

    /**
     * @var string
     */
    private $Typ;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string
     */
    public function getLanCode()
    {
        return $this->Lan_code;
    }

    /**
     * @param string $Lan_code
     */
    public function setLanCode($Lan_code)
    {
        $this->Lan_code = $Lan_code;
    }

    /**
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->Bezeichnung;
    }

    /**
     * @param string $Bezeichnung
     */
    public function setBezeichnung($Bezeichnung)
    {
        $this->Bezeichnung = $Bezeichnung;
    }

    /**
     * @return float
     */
    public function getSatz()
    {
        return $this->Satz;
    }

    /**
     * @param float $Satz
     */
    public function setSatz($Satz)
    {
        $this->Satz = $Satz;
    }

    /**
     * @return string
     */
    public function getGueltigAb()
    {
        return $this->GueltigAb;
    }

    /**
     * @param string $GueltigAb
     */
    public function setGueltigAb($GueltigAb)
    {
        $this->GueltigAb = $GueltigAb;
    }

    /**
     * @return string
     */
    public function getGueltigBis()
    {
        return $this->GueltigBis;
    }

    /**
     * @param string $GueltigBis
     */
    public function setGueltigBis($GueltigBis)
    {
        $this->GueltigBis = $GueltigBis;
    }

    /**
     * @return string
     */
    public function getTyp()
    {
        return $this->Typ;
    }

    /**
     * @param string $Typ
     */
    public function setTyp($Typ)
    {
        $this->Typ = $Typ;
    }

}