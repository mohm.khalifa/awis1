<?php

// Variablen
global $_POST;
global $_GET;
global $awisDBFehler;

$RechteFilialInfo = awisBenutzerRecht($con, 100);
$RechteStufeMitarbeiter = awisBenutzerRecht($con, 1301);
$RechteStufe = awisBenutzerRecht($con, 1300);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleins�tze',$_SERVER['PHP_AUTH_USER'],'','','');
   die("Keine ausreichenden Rechte!");
}


$RZListeStufe = awisBenutzerRecht($con, 1302);		// Einschr�nkungen nach RZ
$RZListe = explode(";",awis_BenutzerParameter($con, "Mitarbeiter_RZListe_Rechte", $_SERVER['PHP_AUTH_USER']));
$RZUserListe = '';
for($i=0;$i<16;$i++)
{
	if(($RZListeStufe&pow(2,$i))==pow(2,$i))
	{
		$RZUserListe .= ';' . $RZListe[$i+1];
	}
}
$RZUserListe .= ';';


$EingabeFeld='';		// F�r Cursor Positionierung
$Params = explode(";",awis_BenutzerParameter($con, "Personaleinsatz_Suche", $_SERVER['PHP_AUTH_USER']));

/********************************
* Parameter speichern
********************************/
if(isset($_POST['cmdSuche_x']))		
{
	$Params[0] = $_POST['txtAuswahlSpeichern'];
	$Params[1] = $_POST['txtMIT_KEY'];	
	$Params[2] = $_POST['txtDatumVom'];	
	$Params[3] = $_POST['txtDatumBis'];	
	$Params[4] = $_POST['txtFIL_ID'];	
	$Params[5] = $_POST['txtMIT_REZ_ID'];	

	if($Params[0]=='on')
	{
		awis_BenutzerParameterSpeichern($con, "Personaleinsatz_Suche", $_SERVER['PHP_AUTH_USER'], implode(';',$Params));
	}
}

/********************************
* Formulardaten speichern
********************************/

if(isset($_POST['cmdSpeichern_x']))
{
	include './perseins_Speichern.php';
}


if(awis_NameInArray($_POST, 'cmdLoeschen_')!='')
{
	$DELKey = intval(substr(awis_NameInArray($_POST,'cmdLoeschen_'),12));
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{
		$SQL = "DELETE FROM MitarbeiterEinsaetze WHERE MEI_KEY=0" . $DELKey;
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			awisErrorMailLink("perseins_Main.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Der Eintrag wurde erfolgreich gel�scht<br><br><a href=./perseins_Main.php?cmdAktion=Einsaetze>Weiter</a>');
	}
	elseif($_POST["cmdLoeschAbbruch"]=='')
	{
		echo "<form name=frmHBG method=post>";

		echo "<input type=hidden name=cmdLoeschen_" . $DELKey . "_x>";
		echo "<input type=hidden name=txtMEI_KEY value=" . $DELKey . ">";

		echo "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Eintrag ";
		echo " vom " . $_POST['txtMEI_TAG'];
		echo " l�schen m�chten?</span><br><br>";
		echo "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		echo "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		echo "</form>";
		awisLogoff($con);
		die();			
	}	
}

/********************************
* Formular aufbauen
********************************/

echo "<form name=frmMitarbeiter method=post action=./perseins_Main.php?cmdAktion=Einsaetze>";
echo '<table class=DatenTabelle border=1 width=100%>';

/********************************************
* Daten hinzuf�gen
********************************************/

$Key = 0;		// Gibt an, welcher KEY ge�ndert werden soll (0=Keiner)
if(($RechteStufe&4)==4 OR ($RechteStufe&2)==2 AND awis_NameInArray($_POST,'cmdAendern_'))		// Neue hinzuf�gen?
{
	if(awis_NameInArray($_POST,'cmdAendern_'))
	{
		$Key = intval(substr(awis_NameInArray($_POST,'cmdAendern_'),11));
		$rsMEI = awisOpenRecordset($con, 'SELECT * FROM MitarbeiterEinsaetze WHERE MEI_KEY=' . $Key);
		if($awisRSZeilen<=0)
		{
			$Key=0;
		}
	}

	echo '<tr>';
	echo '<td class=FeldBez>&nbsp;</td>';
	echo '<td class=FeldBez>Mitarbeiter</td>';
	echo '<td class=FeldBez>Datum</td>';
	echo '<td class=FeldBez>Zeit von</td>';
	echo '<td class=FeldBez>Zeit bis</td>';
	echo '<td class=FeldBez>Kategorie</td>';
	echo '<td class=FeldBez>Filiale</td>';
	echo '<td class=FeldBez>Bemerkung</td>';
	echo '</tr>';

	echo '<tr>';

	echo '<td>';
	echo " <input type=image accesskey=S title='Speichern (Alt+S)' alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo '</td>';	
		// Mitarbeiter
	echo '<td>';

	If($Key != 0)		// Datensatz �ndern
	{
		echo '<input type=Hidden name=txtMEI_KEY value=' . $Key . '>';
		
		echo '<input type=Hidden name=txtMEI_MIT_KEY_old value=' . $rsMEI['MEI_MIT_KEY'][0] . '>';
		echo '<input type=Hidden name=txtMEI_TAG_old value=' . str_replace('-','.',$rsMEI['MEI_TAG'][0]) . '>';
		echo '<input type=Hidden name=txtMEI_VONXTZ_ID_old value=' . $rsMEI['MEI_VONXTZ_ID'][0] . '>';
		echo '<input type=Hidden name=txtMEI_BISXTZ_ID_old value=' . $rsMEI['MEI_BISXTZ_ID'][0] . '>';
		echo '<input type=Hidden name=txtMEI_MEA_KEY_old value=' . $rsMEI['MEI_MEA_KEY'][0] . '>';
		echo '<input type=Hidden name=txtMEI_FIL_ID_old value=' . $rsMEI['MEI_FIL_ID'][0] . '>';
		echo '<input type=Hidden name=txtMEI_BEMERKUNG_old value=' . $rsMEI['MEI_BEMERKUNG'][0] . '>';
	}
	$EingabeFeld='txtMEI_MIT_KEY';
	echo '<select name=txtMEI_MIT_KEY tabindex=10>';
	$rsPER = awisOpenRecordset($con, "SELECT * FROM Mitarbeiter ORDER BY MIT_BEZEICHNUNG");
	$rsPERZeilen = $awisRSZeilen;

	for($PerZeile=0;$PerZeile<$rsPERZeilen;$PerZeile++)
	{
		if($RZUserListe!=';')
		{
			if(strstr($RZUserListe,';'.$rsPER['MIT_REZ_ID'][$PerZeile].';')=='')
			{
				continue;
			}
		}
		echo "<option value='" . $rsPER['MIT_KEY'][$PerZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsPER['MIT_KEY'][$PerZeile] == $rsMEI['MEI_MIT_KEY'][0])
			{
				echo ' selected ';
			}
		}
		echo ">" . $rsPER['MIT_BEZEICHNUNG'][$PerZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';	
	unset($rsPER);

	/******* Datum *******/
	if($Key != 0)
	{
		echo '<td><input name=txtMEI_TAG size=8 tabindex=20 value=' . str_replace('-','.',$rsMEI['MEI_TAG'][0]) . '></td>';
	}
	else
	{
		echo '<td><input name=txtMEI_TAG size=8 tabindex=20 value=' . date('d.m.Y') . '></td>';
	}
	
	/******* Zeitvon *******/
	echo '<td>';
	echo '<select name=txtMEI_VONXTZ_ID tabindex=30>';
	$rsXTZ = awisOpenRecordset($con, "SELECT * FROM TagesZeiten ORDER BY XTZ_ID");
	$rsXTZZeilen = $awisRSZeilen;

	for($XTZZeile=0;$XTZZeile<$rsXTZZeilen;$XTZZeile++)
	{
		echo "<option value='" . $rsXTZ['XTZ_ID'][$XTZZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsXTZ['XTZ_ID'][$XTZZeile] == $rsMEI['MEI_VONXTZ_ID'][0])
			{
				echo ' selected ';
			}
		}
		echo ">" . $rsXTZ['XTZ_TAGESZEIT'][$XTZZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';	

	/******* ZeitBis *******/
	echo '<td>';
	echo '<select name=txtMEI_BISXTZ_ID  tabindex=40>';

	for($XTZZeile=0;$XTZZeile<$rsXTZZeilen;$XTZZeile++)
	{
		echo "<option value='" . $rsXTZ['XTZ_ID'][$XTZZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsXTZ['XTZ_ID'][$XTZZeile] == $rsMEI['MEI_BISXTZ_ID'][0])
			{
				echo ' selected ';
			}
		}
		echo ">" . $rsXTZ['XTZ_TAGESZEIT'][$XTZZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';	
	unset($rsXTZ);
	
	/******* Einsatzart *******/
	echo '<td>';
	echo '<select name=txtMEI_MEA_KEY  tabindex=50>';
	$rsMEA = awisOpenRecordset($con, "SELECT * FROM MITARBEITEREINSAETZEARTEN ORDER BY MEA_BEZEICHNUNG");
	$rsMEAZeilen = $awisRSZeilen;

	for($MEAZeile=0;$MEAZeile<$rsMEAZeilen;$MEAZeile++)
	{
		echo "<option value='" . $rsMEA['MEA_KEY'][$MEAZeile] . "'";
		if($Key != 0)	// �ndern?
		{
			if($rsMEA['MEA_KEY'][$MEAZeile] == $rsMEI['MEI_MEA_KEY'][0])
			{
				echo ' selected ';
			}
		}
		echo ">" . $rsMEA['MEA_BEZEICHNUNG'][$MEAZeile] . "</option>";
	}
	echo '</select>';
	echo '</td>';	
	unset($rsMEA);

	/******* Filiale *******/
	if($Key != 0)
	{
		echo '<td><input name=txtMEI_FIL_ID size=5 tabindex=55 value=' . $rsMEI['MEI_FIL_ID'][0] . '></td>';
	}
	else
	{
		echo '<td><input name=txtMEI_FIL_ID size=5 tabindex=55></td>';
	}

	/******* Bemerkung *******/
	if($Key != 0)
	{
		echo '<td><input name=txtMEI_BEMERKUNG size=50 tabindex=60 value=' . $rsMEI['MEI_BEMERKUNG'][0] . '></td>';
	}
	else
	{
		echo '<td><input name=txtMEI_BEMERKUNG size=50 tabindex=60></td>';
	}
	
	echo '</tr>';
	if($Key != 0)
	{
		echo '<tr><td colspan=99><font size=2>Zuletzt ge�ndert am ' . str_replace('-', '.', $rsMEI['MEI_USERDAT'][0]) . ' durch ' . $rsMEI['MEI_USER'][0] . '.';
		echo '</font></td></tr>';
	}	
}
echo '</table>';
/**********************************
* Ende neuer Eintrag
**********************************/

/*********************************************************************
* Daten anzeigen
*********************************************************************/
if($Key == 0)		// Bei Datensatz �ndern ==> keine weiteren Daten anzeigen
{
	echo '<table class=DatenTabelle border=1 width=100%>';
	echo '<tr><td Class=FeldBez colspan=9>Gefundene Eintr�ge</td></tr>';
	
	echo '<tr>';
	echo '<td Class=FeldBez>&nbsp;</td>';
	echo '<td Class=FeldBez>Kurzwahl</td>';
	echo '<td Class=FeldBez>Mitarbeiter</td>';
	echo '<td Class=FeldBez>Tag</td>';
	echo '<td Class=FeldBez>von</td>';
	echo '<td Class=FeldBez>bis</td>';
	echo '<td Class=FeldBez>Ort</td>';
	echo '<td Class=FeldBez>Bemerkung</td>';
	echo '</tr>';
	
	$SQL = 'SELECT MitarbeiterEinsaetze.*, Filialen.FIL_BEZ, Mitarbeiter.*, Personal.*, ';
	$SQL .= ' MitarbeiterEinsaetzeArten.*, TZVon.XTZ_TAGESZEIT AS ZeitVon, ';
	$SQL .= ' TZBis.XTZ_TAGESZEIT AS ZeitBis';
	$SQL .= ' FROM MitarbeiterEinsaetze INNER JOIN Mitarbeiter ON MEI_MIT_KEY = MIT_KEY';
	$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY';
	$SQL .= ' LEFT OUTER JOIN Filialen ON MEI_FIL_ID = FIL_ID';
	$SQL .= ' INNER JOIN TagesZeiten TZVon ON MEI_VonXTZ_ID = TZVon.XTZ_ID';
	$SQL .= ' INNER JOIN TagesZeiten TZBis ON MEI_BisXTZ_ID = TZBis.XTZ_ID';
	$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';
	
	$Bedingung = '';
	if($Params[1]!='0')		//MIT_KEY
	{
		$Bedingung .= ' AND MEI_MIT_KEY=0' . $Params[1];
	}	
	if($Params[2]!='')		//txtDatumVom
	{
		$Bedingung .= " AND MEI_TAG>=TO_DATE('" . $Params[2] . "','DD.MM.RRRR')";
	}	
	if($Params[3]!='')		//txtDatumBis
	{
		$Bedingung .= " AND MEI_TAG<=TO_DATE('" . $Params[3] . "','DD.MM.RRRR')";
	}	
	if($Params[4]!='')		//txtFIL_ID
	{
		$Bedingung .= " AND MEI_FIL_ID=" . $Params[4] . "";
	}	
	if($Params[5]!='0' AND $Params[5]!='')		//Regionalzentren
	{
		$Bedingung .= " AND MIT_REZ_ID=" . $Params[5] . "";
	}	
	
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
	$rsMEI = awisOpenRecordset($con, $SQL);
	$rsMEIZeilen = $awisRSZeilen;
	
	//var_dump($rsMEI);
	
	for($MeiZeile=0;$MeiZeile<$rsMEIZeilen;$MeiZeile++)
	{
		echo '<tr>';
	
		echo '<td width=50 style=padding:0;margin:0;white-space:nowrap;>';
		if(($RechteStufe&4)==4)
		{
			echo "<input type=image accesskey=A alt='�ndern (Alt+A)' src=/bilder/aendern.png name=cmdAendern_" . $rsMEI['MEI_KEY'][$MeiZeile] . ">";
		}
		if(($RechteStufe&8)==8)
		{
			print "<input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/muelleimer.png name=cmdLoeschen_" . $rsMEI['MEI_KEY'][$MeiZeile] . ">";
		}
		echo '</td>';
		
		
		$rsKurzwahl = awisOpenRecordset($con, 'SELECT * FROM KontakteKommunikation WHERE KKO_KOT_Key IN (6,2) AND KKO_KON_Key=0' . $rsMEI['MIT_KON_KEY'][$MeiZeile] . ' ORDER BY KKO_KOT_KEY DESC');
	
		echo '<td>';
		if($awisRSZeilen==0)
		{
			echo '::unbekannt::';
		}
		else
		{
			for($Kurzwahl=0;$Kurzwahl<$awisRSZeilen;$Kurzwahl++)
			{
				if($Kurzwahl>0)
				{
					echo '<br>';
				}
				if($rsKurzwahl['KKO_KOT_KEY'][$Kurzwahl]==6)	// Handy
				{
					echo '<img src=/bilder/handy.png ' . " onMouseover=\"window.status='Tel. " . $rsKurzwahl['KKO_WERT'][$Kurzwahl] . ".';return true;\" onMouseOut=\"window.status='AWIS';return true;\">";
				}
				elseif($rsKurzwahl['KKO_KOT_KEY'][$Kurzwahl]==2)	// Telefon
				{
					echo '<img src=/bilder/telefon.png ' . " onMouseover=\"window.status='Tel. " . $rsKurzwahl['KKO_WERT'][$Kurzwahl] . ".';return true;\" onMouseOut=\"window.status='AWIS';return true;\">";
				}
				echo $rsKurzwahl['KKO_KURZWAHL'][$Kurzwahl];
			}
		}
		echo '</td>';
		
		echo '<td>';
		if($RechteStufeMitarbeiter>0)
		{
			echo '<a href=./perseins_Main.php?cmdAktion=Mitarbeiter&MIT_KEY=' . $rsMEI['MEI_MIT_KEY'][$MeiZeile] . '>';
			echo $rsMEI['MIT_BEZEICHNUNG'][$MeiZeile] . '</a>';
		}
		else
		{
			echo $rsMEI['MIT_BEZEICHNUNG'][$MeiZeile];
		}
		echo '</td>';
		
		
		echo '<td>' . str_replace('-','.',$rsMEI['MEI_TAG'][$MeiZeile]) . '</td>';
		echo '<td align=right>' . $rsMEI['ZEITVON'][$MeiZeile] . '</td>';	
		echo '<td align=right>' . $rsMEI['ZEITBIS'][$MeiZeile] . '</td>';	
	
		if($rsMEI['MEI_MEA_KEY'][$MeiZeile]==8)		// Filale oder
		{
			echo '<td>' . $rsMEI['MEA_BEZEICHNUNG'][$MeiZeile] . '&nbsp;';
			
			if($RechteFilialInfo>0)
			{
				echo '<a href=/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsMEI['MEI_FIL_ID'][$MeiZeile] . '>' . $rsMEI['MEI_FIL_ID'][$MeiZeile] . '</a> (' . $rsMEI['FIL_BEZ'][$MeiZeile] .  ')';	
			}
			else
			{
				echo $rsMEI['MEI_FIL_ID'][$MeiZeile] . ' (' . $rsMEI['FIL_BEZ'][$MeiZeile] .  ')';	
			}
			echo '</td>';
		}
		else
		{
			echo '<td>' . $rsMEI['MEA_BEZEICHNUNG'][$MeiZeile] . '</td>';	
		}
		
	
		echo '<td>' . $rsMEI['MEI_BEMERKUNG'][$MeiZeile] . '</td>';	
		
		echo '</tr>';
	}
	
	
	echo '</table>';
}
else		// Daten werden ge�ndert
{
	echo "<input accesskey=x title='Abbrechen (Alt+X)' type=image src=/bilder/abbrechen.png alt='Abbrechen (Alt+X)' name=cmdAbbrechen>";
}
/*********************************************************************
* Ende Daten anzeigen
*********************************************************************/

echo '</form>';

/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('" . $EingabeFeld . "')[0].focus();";
	echo "\n</Script>";
}

?>
