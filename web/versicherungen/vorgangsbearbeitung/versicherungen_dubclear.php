<?php
//require_once('kdtelefonie_funktionen.inc');

global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	if(isset($_POST['cmd_dubclear_x']))
	{
		$SperrKeys = $_POST['txtSperrKeys'];
		$FreigabeKeys = $_POST['txtFreigabeKeys'];
		
		$KEYS = $_POST['txtFreigabeKeys'];
		
		if($_POST['txtSperrKeys'] != '')
		{
			$KEYS .= ','.$_POST['txtSperrKeys'];
		}
		
		
		$SQL = 'select * from versicherungen_dubletten ';
		$SQL .= ' where vdb_vvs_key in ('.$KEYS.')';
		
		$rsDSvorhanden = $DB->RecordSetOeffnen($SQL);
		
		if($rsDSvorhanden->AnzahlDatensaetze()>0)
		{
			if($rsDSvorhanden->FeldInhalt('VDB_STATUS')==0)
			{
				echo '<span class=HinweisText>Die Bearbeitungszeit f�r diesen Datensatz ist abgelaufen. Bitte erneut �ffnen.</span>';
			}
			else 
			{
				$SQL  ='update versvorgangsstatus';
				$SQL .=' set vvs_ven_key = 0,';
				$SQL .=' vvs_vcn_key = 0';
				$SQL .=' where vvs_key in ';
				$SQL .=' ('.$FreigabeKeys.')';
				
				$DB->Ausfuehren($SQL);
				
				if($SperrKeys!='')
				{
					$SQL  ='update versvorgangsstatus';
					$SQL .=' set vvs_ven_key = 29,';
					$SQL .=' vvs_vcn_key = 124';
					$SQL .=' where vvs_key in ';
					$SQL .=' ('.$SperrKeys.')';
					
					$DB->Ausfuehren($SQL);
				}

				$SQL = 'delete from versicherungen_dubletten ';
				$SQL.= ' where vdb_vvs_key in ('.$KEYS.')';
				
				$DB->Ausfuehren($SQL);
				
			}
		}
	}
	elseif(isset($_POST['cmd_abbrechen_x']))
	{
		
		$rollback = $_POST['txtFreigabeKeys'];
		
		if($_POST['txtSperrKeys'] != '')
		{
			$rollback .= ','.$_POST['txtSperrKeys'];
		}
		$SQL  ='update versicherungen_dubletten';
		$SQL .=' set vdb_status = 0, vdb_userdat = null, vdb_user = null';
		$SQL .=' where vdb_vvs_key in ('.$rollback.')';
				
		$DB->Ausfuehren($SQL);
	}
	//var_dump($_POST);
	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>