<?php
global $SEP;
try 
{
    $SQL='DELETE FROM SEMINAREMAILS WHERE SEM_KEY = ' . $SEP->DB->WertSetzen('SEM','Z',$_GET['DEL']);

    $SEP->DB->Ausfuehren($SQL,'',true,$SEP->DB->Bindevariablen('SEM'));

}catch(awisException $ex){
    if ($SEP->Form instanceof awisFormular) {
        $SEP->Form->DebugAusgabe(1, $ex->getSQL());
        $SEP->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}

?>