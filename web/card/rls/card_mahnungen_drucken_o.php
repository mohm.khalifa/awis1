<?php
ob_start();  //Einschalten der Pufferung von Ausgaben
require_once("card/card_pdf.inc.php");		// PDF
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen
require_once("card/card_db.inc.php");			// DB




//$pdf=new PDF_MDR('','','',0,0);

require_once("card/card_pdf_mahnungen_drucken.inc.php");	// PDF
	
ob_end_clean();	//Verwirft den Inhalt des Ausgabe-Puffers und deaktiviert die Pufferung

//Korrektur allg.

$dblKorrekturX=0;
$dblKorrekturY=0;

//Seitenr�nder

$dblSeitenrandLinks=19+$dblKorrekturX;
$dblSeitenrandOben=19;
$dblSeitenrandRechts=210-192+$dblKorrekturX;
$dblSeitenrandUnten=19;

//Spaltenbreiten Tabelle

$dblSpaltenBrText=40;
$dblSpaltenBrKartennr=42;
$dblSpaltenBrDatum=35;
$dblSpaltenBrBetrag=(210-$dblSeitenrandLinks-$dblSeitenrandRechts-$dblSpaltenBrDatum-$dblSpaltenBrText-$dblSpaltenBrKartennr)/2;

$pdf=new PDF_MDR('P','mm','A4',$dblKorrekturX,$dblKorrekturY,$dblSeitenrandLinks,$dblSeitenrandOben,$dblSeitenrandRechts,$dblSeitenrandUnten,$strAbrDat,$strRechnungsSumme);	//A4-Seite im Hochformat mit Seitenrand 10 mm

//maa_mahnstufe<3: Datens�tze mit aktueller Mahnstufe 3 werden an Inkassounternehmen abgegeben und m�ssen nicht gedruckt werden
//MAA_BETRAG_RECHNUNGEN+MAA_SKONTOABZUG ergibt MAA_BETRAG_RECHNUNGEN (Skontoabzug bzw Rabatt muss von Rechnungsforderung abgezogen werden)

$strSql="select MAA_ID, MAA_RNR, MAA_RG_DATUM, MAA_KUNDEN_NR, (MAA_BETRAG_RECHNUNGEN+MAA_SKONTOABZUG)*-1 MAA_BETRAG_RECHNUNGEN, MAA_BETRAG_RECHNUNGEN_OFF*-1 MAA_BETRAG_RECHNUNGEN_OFF,
MAA_BETRAG_MAHNUNGEN*-1 MAA_BETRAG_MAHNUNGEN, MAA_BETRAG_MAHNUNGEN_OFF*-1 MAA_BETRAG_MAHNUNGEN_OFF,MAA_BETRAG_GESAMT, MAA_SKONTOABZUG,
MAA_ZAHLUNGSZIEL, MAA_MAHNUNG1_DATUM, MAA_MAHNUNG2_DATUM, MAA_MAHNUNG3_DATUM,
MAA_INKASSO_DATUM, MAA_MAHNSTUFE, MAA_GEKAUFTAUFPAN, MAA_EINKAEUFER, MAA_KUNDENART, MAA_FIRMA, MAA_ABTEILUNG, MAA_TITEL, MAA_NAME,
MAA_VORNAME, MAA_STRASSE, MAA_PLZ, MAA_ORT, MAA_LAND, MAA_GESCHLECHT, MAA_AEND_BENUTZER, MAA_AEND_DATUM, MAA_LOESCH, MAA_VERARBEITET,nvl(rec_zahlungsziel,'') MAA_ZAHLUNGSZIEL_NEU
from mahnungen_aufbereitet,rechnungen where maa_rnr=rec_rnr(+) and maa_verarbeitet=1 and maa_mahnstufe<3
order by maa_mahnstufe,maa_zahlungsziel,maa_kunden_nr";

$arrRueckgabe = abfrage("ora",$strSql);
$intZeilen = $arrRueckgabe["zeilen"];
$arrMahnungen = $arrRueckgabe["trefferarray"];

$dblSumBetragRechnungen=0;
$dblSumBetragMahnungen=0;
$dblSumBetragRechnungenOffen=0;
$dblSumBetragMahnungenOffen=0;

$intMehrzahl=0;

for($i=0,$intSchalterEnde=0;$i<$intZeilen||$intSchalterEnde;$i++)		//$intSchalterEnde wird beim letzten Schleifendurchlauf auf 1 gesetzt, Summenzeilen wird noch einmal durchlaufen
{
	if((!($arrMahnungen["MAA_MAHNSTUFE"][$i]==$arrMahnungen["MAA_MAHNSTUFE"][$i-1]&&$arrMahnungen["MAA_ZAHLUNGSZIEL"][$i]==$arrMahnungen["MAA_ZAHLUNGSZIEL"][$i-1]&&
	$arrMahnungen["MAA_KUNDEN_NR"][$i]==$arrMahnungen["MAA_KUNDEN_NR"][$i-1]))||$i==0||$intSchalterEnde)
	{
		if($i!=0)	//Summenzeilen
		{
			$pdf->Ln($dblZeilenhoehe/2);
			
			$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
			$pdf->Cell($dblSpaltenBrText+$dblSpaltenBrDatum+$dblSpaltenBrKartennr,$dblZeilenhoehe,'Summe',1,0,'L',0);
			$pdf->Cell($dblSpaltenBrBetrag,$dblZeilenhoehe,eurobetrag($dblSumBetragRechnungen),1,0,'R',0);
			$pdf->Cell($dblSpaltenBrBetrag,$dblZeilenhoehe,eurobetrag($dblSumBetragRechnungenOffen),1,1,'R',0);
			
			if($dblSumBetragMahnungen!=0)
			{
				$pdf->Cell($dblSpaltenBrText+$dblSpaltenBrDatum+$dblSpaltenBrKartennr,$dblZeilenhoehe,'Mahngeb�hren',1,0,'L',0);
				$pdf->Cell($dblSpaltenBrBetrag,$dblZeilenhoehe,eurobetrag($dblSumBetragMahnungen),1,0,'R',0);
				$pdf->Cell($dblSpaltenBrBetrag,$dblZeilenhoehe,eurobetrag($dblSumBetragMahnungenOffen),1,1,'R',0);
			}
			
			$pdf->Ln($dblZeilenhoehe/2);
			
			$pdf->Cell($dblSpaltenBrText+$dblSpaltenBrDatum+$dblSpaltenBrKartennr+$dblSpaltenBrBetrag,$dblZeilenhoehe,'Zu zahlender Betrag',1,0,'L',1);
			$pdf->Cell($dblSpaltenBrBetrag,$dblZeilenhoehe,eurobetrag($dblSumBetragMahnungenOffen+$dblSumBetragRechnungenOffen),1,1,'R',1);
			
			$dblSumBetragRechnungen=0;
			$dblSumBetragMahnungen=0;
			$dblSumBetragRechnungenOffen=0;
			$dblSumBetragMahnungenOffen=0;
			
			$pdf->Ln($dblZeilenhoehe);
			
			$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
						
			$intMahnstufe=$arrMahnungen["MAA_MAHNSTUFE"][$i-1]+1;
			
			if($intMahnstufe==1)	//Text Zahlungserinnerung
			{
				$text="Sicherlich ";
				
				if($intMehrzahl)
				{
					$text.="sind ";
				}
				else
				{
					$text.="ist ";
				}
				
				$text.="diese lediglich Ihrer Aufmerksamkeit entgangen. Bitte �berweisen Sie den Saldo";
							
				
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'L',0);
							
				$pdf->Ln($dblZeilenhoehe/2);
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
							
				$text="bis zum ".$arrMahnungen["MAA_ZAHLUNGSZIEL_NEU"][$i-1];
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'C',0);
				
				$pdf->Ln($dblZeilenhoehe/2);
				$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
				
				$text="auf unser Konto 348 651 072 (BLZ 753 200 75) HYPOVEREINSBANK WEIDEN OBERPF unter Angabe der Karten-Nr., Rechnungs-Nr. und des Rechnungsdatums.";
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'L',0);
				
				$pdf->Ln($dblZeilenhoehe/2);
				$text="Leider sehen wir uns gezwungen, bei fehlendem Geldeingang Mahngeb�hren zu erheben.";
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'L',0);
				
				$pdf->Ln($dblZeilenhoehe);
				$pdf->MultiCell(0,$dblZeilenhoehe,"Mit freundlichen Gr��en",0,'L',0);
			}
			elseif($intMahnstufe==2)	//Text 2. Mahnung
			{
				$text="Wir hatten Sie hierzu bereits am ".$arrMahnungen["MAA_MAHNUNG1_DATUM"][$i-1]." angeschrieben und um �berweisung ";
				
				if($intMehrzahl)
				{
					$text.="der offen stehenden Betr�ge";
				}
				else
				{
					$text.="des offen stehenden Betrages";
				}
				
				$text.=" gebeten. Dies ist jedoch bisher nicht geschehen. Bitte �berweisen Sie den Saldo";
				
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'L',0);
				
				$pdf->Ln($dblZeilenhoehe/2);
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
							
				$text="bis zum ".$arrMahnungen["MAA_ZAHLUNGSZIEL_NEU"][$i-1];
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'C',0);
				
				$pdf->Ln($dblZeilenhoehe/2);
				$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
				
				$text="auf unser Konto 348 651 072 (BLZ 753 200 75) HYPOVEREINSBANK WEIDEN OBERPF unter Angabe der Karten-Nr., Rechnungs-Nr. und des Rechnungsdatums.";
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'L',0);
				
				$pdf->Ln($dblZeilenhoehe/2);
				
				$text="Bitte haben Sie Verst�ndnis daf�r, dass wir bis zum Ausgleich unserer ";
				
				if($intMehrzahl)
				{
					$text.="Rechnungen";
				}
				else
				{
					$text.="Rechnung";
				}
				
				$text.=" keine weiteren Eink�ufe �ber Ihre ATU-Karten genehmigen k�nnen.";
				
				$pdf->MultiCell(0,$dblZeilenhoehe,$text,0,'L',0);
				
				$pdf->Ln($dblZeilenhoehe);
			}
			elseif($intMahnstufe==3)	//Text 3. Mahnung
			{
				$text="Nachdem unsere Mahnungen vom ";
				
				$pdf->Write($dblZeilenhoehe,$text);
				
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$pdf->Write($dblZeilenhoehe,$arrMahnungen["MAA_MAHNUNG1_DATUM"][$i-1]);
				$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
				$pdf->Write($dblZeilenhoehe," und ");
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$pdf->Write($dblZeilenhoehe,$arrMahnungen["MAA_MAHNUNG2_DATUM"][$i-1]);
				
				$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
				
				$text=" ohne vollst�ndigen Forderungsausgleich verstrichen sind, werden wir in den n�chsten Tagen die Angelegenheit einem Inkassounternehmen �bergeben.";
				
				$pdf->Write($dblZeilenhoehe,$text);
				
				$pdf->Ln($dblZeilenhoehe/2*3);
				
				$text="Bitte beachten Sie, dass Ihre Kundenkarten, wie bereits erw�hnt, bis auf weiteres nicht f�r den Zahlungsverkehr zugelassen sind.";
				$pdf->Write($dblZeilenhoehe,$text);
				
				$pdf->Ln($dblZeilenhoehe*2);
				
			
			}
			
			$pdf->MultiCell(0,$dblZeilenhoehe,"ATU Auto-Teile Unger GmbH",0,'L',0);
		}
		
		$intMehrzahl=0;
			
		if($arrMahnungen["MAA_MAHNSTUFE"][$i]==$arrMahnungen["MAA_MAHNSTUFE"][$i+1]&&$arrMahnungen["MAA_ZAHLUNGSZIEL"][$i]==$arrMahnungen["MAA_ZAHLUNGSZIEL"][$i+1]&&
		$arrMahnungen["MAA_KUNDEN_NR"][$i]==$arrMahnungen["MAA_KUNDEN_NR"][$i+1])
			$intMehrzahl=1;
	
		if(!$intSchalterEnde)
		{
			$pdf->AddPage();
			//Briefkopf
			$pdf->briefkopf_atu();
			
			
			//Adressfeld
			
			$strAdressfeld = '';
			$strAnredeEnde='';
			
			$strBriefAnrede='';
			
			if(trim($arrMahnungen["MAA_KUNDENART"][$i])=='PR')
			{
				$strAnredeEnde=' ';
			
				if(trim($arrMahnungen["MAA_FIRMA"][$i])!='')
					$strAdressfeld.=trim($arrMahnungen["MAA_FIRMA"][$i])."\n";
					
				if(trim($arrMahnungen["MAA_ABTEILUNG"][$i])!='')
					$strAdressfeld.=trim($arrMahnungen["MAA_ABTEILUNG"][$i])."\n";
			}
			else
			{
				$strAnredeEnde="\n";
			}
			
			$strGeschlecht=trim($arrMahnungen["MAA_GESCHLECHT"][$i]);
			
			if(trim($arrMahnungen["MAA_VORNAME"][$i])!=''||trim($arrMahnungen["MAA_NAME"][$i])!='')
			{
				if($strGeschlecht=='1')
				{
					$strAdressfeld.="Herrn".$strAnredeEnde;
					$strBriefAnrede='Sehr geehrter Herr '.trim($arrMahnungen["MAA_NAME"][$i]).',';
				}
				elseif($strGeschlecht=='2')
				{
					$strAdressfeld.="Frau".$strAnredeEnde;
					$strBriefAnrede='Sehr geehrte Frau '.trim($arrMahnungen["MAA_NAME"][$i]).',';
				}
				elseif($strGeschlecht=='3')
				{
					$strBriefAnrede='Sehr geehrte Damen und Herren,';
				}
					
				if(trim($arrMahnungen["MAA_TITEL"][$i])!='')
					$strAdressfeld.=trim($arrMahnungen["MAA_TITEL"][$i])." ";
					
				if(trim($arrMahnungen["MAA_VORNAME"][$i])!='')
					$strAdressfeld.=trim($arrMahnungen["MAA_VORNAME"][$i])." ";
				
				if(trim($arrMahnungen["MAA_NAME"][$i])!='')
					$strAdressfeld.=trim($arrMahnungen["MAA_NAME"][$i]);
					
				$strAdressfeld.="\n";
			}
			else
			{
				$strBriefAnrede='Sehr geehrte Damen und Herren,';
			}
		
			if(trim($arrMahnungen["MAA_STRASSE"][$i])!='')
					$strAdressfeld.=trim($arrMahnungen["MAA_STRASSE"][$i])."\n";
					
			$strAdressfeld.="\n";
			
			if(trim($arrMahnungen["MAA_LAND"][$i])!='DE'&&trim($arrMahnungen["MAA_LAND"][$i])!='')
				$strAdressfeld.=trim($arrMahnungen["MAA_LAND"][$i])."-";
				
			if(trim($arrMahnungen["MAA_PLZ"][$i])!='')
				$strAdressfeld.=trim($arrMahnungen["MAA_PLZ"][$i])." ";
		
			if(trim($arrMahnungen["MAA_ORT"][$i])!='')
				$strAdressfeld.=trim($arrMahnungen["MAA_ORT"][$i]);
					
			$pdf->SetFontSize(10);
			$pdf->adressfeld($strAdressfeld,$dblSeitenrandLinks,48,$pdf->ptinmm(11));
			
			
			//�berschrift
			
			$pdf->SetXY($dblSeitenrandLinks,95);
			
			$pdf->SetFillColor(192,192,192);
			$dblSchriftgroesse=10.5;
			$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
			$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
			
			
			$intMahnstufe=$arrMahnungen["MAA_MAHNSTUFE"][$i]+1;
			$strUeberschrift='';
			
			switch($intMahnstufe)
			{
				case 1: $strUeberschrift='Zahlungserinnerung Kunden-Nr.: '.$arrMahnungen["MAA_KUNDEN_NR"][$i];
				break;
				case 2: $strUeberschrift='2. Mahnung Kunden-Nr.: '.$arrMahnungen["MAA_KUNDEN_NR"][$i];
				break;
				case 3: $strUeberschrift='3. Mahnung - Ank�ndigung Inkassoverfahren Kunden-Nr.: '.$arrMahnungen["MAA_KUNDEN_NR"][$i];
				break;
			}
			
			
			$pdf->Cell(130,$dblZeilenhoehe,$strUeberschrift,'L,T,B',0,'L',1);
			$pdf->Cell(0,$dblZeilenhoehe,strftime("%d.%m.%Y"),'R,T,B',1,'R',1);
			
			$pdf->Ln(8);
			
			//Anrede
			
			$dblSchriftgroesse=10.5;
			$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
			$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse);
			
			$pdf->Cell(0,$dblZeilenhoehe,$strBriefAnrede,0,1,'L',0);
			
			$pdf->Ln();
			
			//Mahntext 1
			
			
			
			$strMahntext='Sie haben in unseren ATU-Filialen Eink�ufe mit Ihrer ATU-Card get�tigt. Leider konnten wir zu ';
					
			if($intMehrzahl)
			{
				$strMahntext.='den angegebenen Rechnungs-Nummern';
			}
			else
			{
				$strMahntext.='der angegebenen Rechnungs-Nummer';
			}
				
			$strMahntext.=' keinen vollst�ndigen Zahlungseingang verbuchen.';
			
			$pdf->MultiCell(0,$dblZeilenhoehe+1,$strMahntext,0,'L');
			
			$pdf->Ln();
			
			$dblSchriftgroesse=10.5;
			$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
			$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
			
			$pdf->Cell($dblSpaltenBrText,$dblZeilenhoehe,'Rechnungs-Nr.',1,0,'L',1);
			$pdf->Cell($dblSpaltenBrKartennr,$dblZeilenhoehe,'Karten-Nr.',1,0,'L',1);
			$pdf->Cell($dblSpaltenBrDatum,$dblZeilenhoehe,'Rechnungsdatum',1,0,'C',1);
			$pdf->Cell($dblSpaltenBrBetrag,$dblZeilenhoehe,'Forderung',1,0,'C',1);
			$pdf->Cell(0,$dblZeilenhoehe,'noch offen',1,1,'C',1);
		}
	}
	
	
	if(!$intSchalterEnde)
	{
		//Datenzeilen bei mehreren Rechnungen
		$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
		$pdf->Cell($dblSpaltenBrText,$dblZeilenhoehe,$arrMahnungen["MAA_RNR"][$i],1,0,'L',0);
		$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
		$pdf->Cell($dblSpaltenBrKartennr,$dblZeilenhoehe,$arrMahnungen["MAA_GEKAUFTAUFPAN"][$i],1,0,'L',0);
		$pdf->Cell($dblSpaltenBrDatum,$dblZeilenhoehe,$arrMahnungen["MAA_RG_DATUM"][$i],1,0,'C',0);
		$pdf->Cell($dblSpaltenBrBetrag,$dblZeilenhoehe,eurobetrag($arrMahnungen["MAA_BETRAG_RECHNUNGEN"][$i]),1,0,'R',0);
		$pdf->Cell(0,$dblZeilenhoehe,eurobetrag($arrMahnungen["MAA_BETRAG_RECHNUNGEN_OFF"][$i]),1,1,'R',0);
		
		$dblSumBetragRechnungen+=(double)str_replace(",",".",$arrMahnungen["MAA_BETRAG_RECHNUNGEN"][$i]);
		$dblSumBetragMahnungen+=(double)str_replace(",",".",$arrMahnungen["MAA_BETRAG_MAHNUNGEN"][$i]);
		$dblSumBetragRechnungenOffen+=(double)str_replace(",",".",$arrMahnungen["MAA_BETRAG_RECHNUNGEN_OFF"][$i]);
		$dblSumBetragMahnungenOffen+=(double)str_replace(",",".",$arrMahnungen["MAA_BETRAG_MAHNUNGEN_OFF"][$i]);
		
		if($i==$intZeilen-1)
			$intSchalterEnde=1;
	}
	else
	{
		$intSchalterEnde=0;
	}
	
	
	
	
	
}	//Ende for-Schleife

//summenzeilen();	//f�r letzten Datensatz

$pdf->SetDisplayMode('fullwidth','continuous');

$pdf->Output();





?>