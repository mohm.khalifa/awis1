<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
global $AWISBenutzer;

try 
{
    print "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
    print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort', 'AlterWert');
    $TextKonserven[]=array('Wort', 'NeuerWert');
    $TextKonserven[]=array('Wort', 'FeldName');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Adressen','PKT_Adressen');
    $TextKonserven[]=array('AAE','AAE_%');
    $TextKonserven[]=array('AAP','%');
    $TextKonserven[]=array('ADR','%');
    $TextKonserven[]=array('ADI','ADI_%');
    $TextKonserven[]=array('ADT','ADT_%');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISSprache = $AWISBenutzer->ParameterLesen('AnzeigeSprache');
    
    $Recht3000 = $AWISBenutzer->HatDasRecht(3000);
    
    echo '<title>Awis - '.$AWISSprachKonserven['Textkonserven']['PKT_Adressen'].'</title>';
    ?>
    </head>
    <body>
    <?
    include ("ATU_Header.php");	// Kopfzeile

    
    if($Recht3000==0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}  
    
    $SQL = 'SELECT DISTINCT aae_adr_key';
    $SQL .= ' FROM adressenaenderungen';
    $SQL .= ' WHERE aae_aktion is null';			// Keine Aktion ausgeführt
    
    $rsAAE = $DB->RecordSetOeffnen($SQL);
    $rsAAEZeilen = $rsAAE->AnzahlDatensaetze();
    if($rsAAEZeilen==0)
    {
    	echo 'Keine Änderungen...';
    	
    }
    else 
    {
        $Form->Formular_Start();
    	
    	$SQL = 'SELECT * FROM adressenaenderungen WHERE aae_adr_key = 0'.$rsAAE->FeldInhalt('AAE_ADR_KEY');
    	
    	$rsDaten = $DB->RecordSetOeffnen($SQL);
    	$Aenderungen = array();
    	for($i=0;$i<$rsAAEZeilen;$i++)
    	{
    		$Aenderungen[$rsDaten->FeldInhalt('AAE_FELD')]=$rsDaten->FeldInhalt('AAE_NEUERWERT');
    	}
    	$SQL = 'SELECT * FROM adressen WHERE adr_key = 0'.$rsAAE->FeldInhalt('AAE_ADR_KEY');
    	$rsMBW = $DB->RecordSetOeffnen($SQL);
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['AAE']['AAE_MELDER'].':',150,'');
        $Form->Erstelle_TextFeld('AAE_MELDER',$rsDaten['AAE_MELDER'][0],50,400,false,'');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AAE']['AAE_USER'].':',150,'');
        $Form->Erstelle_TextFeld('AAE_MELDER',$rsDaten['AAE_USER'][0],50,400,false,'');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['AAE']['AAE_USERDAT'].':',150,'');
        $Form->Erstelle_TextFeld('AAE_MELDER',$rsDaten['AAE_USERDAT'][0],50,400,false,'');
        $Form->ZeileEnde();
    	
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($TextKonserven['Wort']['FeldName'],150,'background-color:gray;');
        
    	if($_GET['Edit']!='0')
    	{
    	    $Form->Erstelle_TextLabel($TextKonserven['Wort']['AlterWert'],300,'background-color:gray;');
    	}
    	$Form->Erstelle_TextLabel($TextKonserven['Wort']['NeuerWert'],400,'background-color:gray;');
    	$Form->ZeileEnde();
    	
    	$TXT_Felder = array();
    	$TXT_Felder[] = array('ADR','ADR_%');
    	$AWISSprachKonserven = $Form->LadeTexte($TXT_Felder);
    	
    	$Felder = array();
    	//$Felder[] = 'ADR_ANREDE';
    	$Felder[] = 'ADR_NAME1';
    	$Felder[] = 'ADR_NAME2';
    	$Felder[] = 'ADR_KONZERN';
    	$Felder[] = 'ADR_STRASSE';
    	$Felder[] = 'ADR_HAUSNUMMER';
    	$Felder[] = 'ADR_POSTFACH';
    	$Felder[] = 'ADR_POSTFACHPLZ';
    	$Felder[] = 'ADR_LAN_CODE';
    	$Felder[] = 'ADR_PLZ';
    	$Felder[] = 'ADR_ORT';
    	$Felder[] = 'ADR_BEMERKUNG';
    	
    	echo '<input type=hidden name=txtAAE_ADR_KEY value='.$rsMBW->FeldInhalt('ADR_KEY').'>';
    	foreach($Felder AS $Feld)
    	{
    	    $Form->ZeileStart();
    	    $Form->Erstelle_TextLabel($TXT_Felder['ADR'][$Feld].':',150,'');
    	    $Form->Erstelle_TextLabel($rsMBW[$Feld][0],300,'');
    	    $Form->Erstelle_TextFeld($Feld,$Aenderungen[$Feld],50,400,true,'');
    		$Form->ZeileEnde();
    	}
    	$Form->Formular_Ende();
    }
    
    //*******************************************************
    // Schaltflächen
    //*******************************************************
    
    print "<input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
    
    if($rsAAEZeilen!=0)
    {
    	print "&nbsp;<input type=image accesskey=s title='".$AWISSprachKonserven['Wort']['lbl_speichern']."' src=/bilder/diskette.png name=cmdDSSpeichern onclick=location.href='./adressen_Aenderungen.php?Save=".$rsMBW->FeldInhalt('ADR_KEY')."'>";
    	print "&nbsp;<input title='".$AWISSprachKonserven['Wort']['lbl_loeschen']."' accesskey=l name=cmdDelete type=image src=/bilder/Muelleimer_gross.png onclick=location.href='./adressen_Aenderungen.php?Del=".$rsMBW->FeldInhalt('ADR_KEY')."'>";
    }
    
    print "&nbsp;&nbsp;<input type=image title='".$AWISSprachKonserven['Wort']['lbl_hilfe']."' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=adressen','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";
    $DB->Schliessen();
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}


?>
</body>
</html>

