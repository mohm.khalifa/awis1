<?php
global $AWISCursorPosition;
global $AWIS_KEY1;

try 
{
	$AWISBenutzer = awisBenutzer::Init ();
	
	$TextKonserven = array ();
	$TextKonserven [] = array ('RFF', '*' );
	$TextKonserven [] = array ('RFK', '*' );
	$TextKonserven [] = array ('RFA', '*' );
	$TextKonserven [] = array ('AST', 'AST_ATUNR');
	$TextKonserven [] = array ('Wort', 'Abschlussdatum' );
	$TextKonserven [] = array ('Wort', 'Rueckfuehrungstyp' );
	$TextKonserven [] = array ('Wort', 'Status' );
	$TextKonserven [] = array ('Wort', 'Seite' );
	$TextKonserven [] = array ('Wort', 'lbl_suche' );
	$TextKonserven [] = array ('Wort', 'lbl_weiter' );
	$TextKonserven [] = array ('Wort', 'lbl_speichern' );
	$TextKonserven [] = array ('Wort', 'lbl_trefferliste' );
	$TextKonserven [] = array ('Wort', 'lbl_aendern' );
	$TextKonserven [] = array ('Wort', 'lbl_hilfe' );
	$TextKonserven [] = array ('Wort', 'lbl_hinzufuegen' );
	$TextKonserven [] = array ('Wort', 'lbl_loeschen' );
	$TextKonserven [] = array ('Wort', 'lbl_zurueck' );
	$TextKonserven [] = array ('Wort', 'lbl_DSZurueck' );
	$TextKonserven [] = array ('Wort', 'lbl_DSWeiter' );
	$TextKonserven [] = array ('Wort', 'lbl_drucken' );
	$TextKonserven [] = array ('Wort', 'lbl_Hilfe' );
	$TextKonserven [] = array ('Wort', 'txt_BitteWaehlen' );
	$TextKonserven [] = array ('Liste','lst_Rueckfuehrungstyp');
	
	$Form = new awisFormular ();
	$DB = awisDatenbank::NeueVerbindung ( 'AWIS' );
	$AWISSprachKonserven = $Form->LadeTexte ( $TextKonserven );
	
	$Recht3910 = $AWISBenutzer->HatDasRecht(3910);
	$Recht3940 = $AWISBenutzer->HatDasRecht(3940);
	
	if (($Recht3940 & 2) == 0) 
	{
		$Form->Formular_Start ();
		$Form->Fehler_KeineRechte ();
		$Form->Formular_Ende ();
		die ();
	}
	$Param = unserialize ( $AWISBenutzer->ParameterLesen ( "RFKRueckfuehrungstyp" ) );
	$ParamSuche = unserialize($AWISBenutzer->ParameterLesen("RFKSuche"));
	
	//$Form->DebugAusgabe(1,$_POST);
	

	if (! isset ( $_POST ['cmdSuche_x'] ) and ! isset ( $_POST ['sucAST_ATUNR'] ) and ! isset ( $_GET ['Liste'] ) and ! isset ( $_POST ['Block'] ) and ! isset ( $_GET ['Sort'] ) and ! isset ( $_GET ['Block'] )) 
	{
		$Form->SchreibeHTMLCode ( '<form name="frmRueckfuehrungstyp" action="./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp" method="POST"  enctype="multipart/form-data">' );
		
		$Form->Formular_Start ();
		$Form->ZeileStart ();
		$Form->Erstelle_TextLabel ( $AWISSprachKonserven ['AST'] ['AST_ATUNR'] . ':', 200 );
		$Form->Erstelle_TextFeld ( '*AST_ATUNR',($ParamSuche['SPEICHERN']=='on'?$ParamSuche['RFF_AST_ATUNR']:''), 20, 200, true );
		$AWISCursorPosition = 'sucAST_ATUNR';
		$Form->ZeileEnde ();
		$Form->Formular_Ende ();
		
		$Form->SchaltflaechenStart ();
		$Form->Schaltflaeche ( 'href', 'cmd_zurueck', '/filialtaetigkeiten/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven ['Wort'] ['lbl_zurueck'], 'Z' );
		$Form->Schaltflaeche ( 'image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven ['Wort'] ['lbl_suche'], 'W' );
		$Form->SchaltflaechenEnde ();
		
		$Form->SchreibeHTMLCode ( '</form>' );
	} 
	else
	{
		if (isset ( $_POST ['cmdSuche_x'] ) or isset ( $_POST ['sucAST_ATUNR'] ))
		{
			$Param ['KEY'] = 0; // Key

			$Param ['AST_ATUNR'] = $_POST ['sucAST_ATUNR'];
			
			$Param ['SPEICHERN'] = (isset ( $_POST ['sucAuswahlSpeichern'] ) ? 'on' : '');
			
			$Param ['ORDERBY'] = '';
			$Param ['BLOCK'] = '';
			$Param ['KEY'] = 0;
		}
		
		//********************************************************
		// Bedingung erstellen
		//********************************************************	
		$Bedingung = _BedingungErstellen ( $Param );
		
		//*****************************************************************
		// Sortierung aufbauen
		//*****************************************************************
		if (! isset ( $_GET ['Sort'] ))
		{
			if (isset ( $Param ['ORDERBY'] ) and $Param ['ORDERBY'] != '')
			{
				$ORDERBY = $Param ['ORDERBY'];
			}
			else
			{
				$ORDERBY = ' RFA_DATUM DESC, RFA_LFDNR DESC nulls last';
			}
		}
		else
		{
			$ORDERBY = ' ' . str_replace ( '~', ' DESC ', $_GET ['Sort'] );
		}
		
		//********************************************************
		// Daten suchen
		//********************************************************
		
		$SQL  = 'SELECT RFA_KEY,';
		$SQL .= ' RFA_DATUM,';
		$SQL .= ' RFA_LFDNR,';
		$SQL .= ' RFA_BEMERKUNG,';
		$SQL .= ' RFP.RFP_AST_ATUNR,';
		$SQL .= ' RFP_RUECKLIEFKZ,';
		$SQL .= ' COUNT(*) AS Anzahl,';
		$SQL .= ' row_number() over (order by ' .$ORDERBY .') AS ZeilenNr';
		$SQL .= ' FROM RUECKFUEHRUNGSANFORDERUNGEN RFA';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSANFORDERUNGENPOS RFP';
		$SQL .= ' ON (RFA.RFA_KEY         =RFP.RFP_RFA_KEY)';
		if ($Bedingung != '')
		{
			$SQL .= $Bedingung;
		}
		$SQL .= ' GROUP BY RFA_KEY,';
		$SQL .= ' RFA_DATUM,';
		$SQL .= ' RFA_LFDNR,';
		$SQL .= ' RFA_BEMERKUNG,';
		$SQL .= ' RFP.RFP_AST_ATUNR,';
		$SQL .= ' RFP_RUECKLIEFKZ';
		$SQL .= ' ORDER BY ' .$ORDERBY;
		
		//************************************************
		// Aktuellen Datenblock festlegen
		//************************************************
		$Block = 1;
		//*****************************************************************
		// Nicht einschränken, wenn nur 1 DS angezeigt werden soll
		//*****************************************************************
		if ($AWIS_KEY1 <= 0)
		{
			if (isset ( $_REQUEST ['Block'] ))
			{
				$Block = $Form->Format ( 'N0', $_REQUEST ['Block'], false );
			} 
			elseif (isset ( $Param ['BLOCK'] ) and $Param ['BLOCK'] != '')
			{
				$Block = intval ( $Param ['BLOCK'] );
			}
	
			//************************************************
			// Zeilen begrenzen
			//************************************************
			$ZeilenProSeite = $AWISBenutzer->ParameterLesen ( 'AnzahlDatensaetzeProListe' );
			$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
			$MaxDS = $DB->ErmittleZeilenAnzahl ($SQL);
			
			$SQL = 'SELECT * FROM (' .$SQL .') DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
		}
		
		$Form->DebugAusgabe ( 1, $SQL );
		$rsRFK = $DB->RecordSetOeffnen ( $SQL );
		
		//*****************************************************************
		// Aktuelle Parameter sichern
		//*****************************************************************	
		$Param ['ORDERBY'] = $ORDERBY;
		$Param ['KEY'] = $AWIS_KEY1;
		$Param ['BLOCK'] = $Block;
		
		$Form->SchreibeHTMLCode ( '<form name=frmRueckfuehrungstyp action=./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp' . (isset ( $_GET ['Seite'] ) ? '&Seite=' . $_GET ['Seite'] : '') . '' . (isset ( $_GET ['Unterseite'] ) ? '&Unterseite=' . $_GET ['Unterseite'] : '') . ' method=post>' );
		
		//********************************************************
		// Daten anzeigen
		//********************************************************	
		$Form->Formular_Start();


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel ( $AWISSprachKonserven ['AST'] ['AST_ATUNR'] . ':', 200 );
		$Form->Erstelle_TextFeld ( '*AST_ATUNR', (isset ( $Param ['AST_ATUNR'] ) ? $Param ['AST_ATUNR'] : ''), 20, 200, false );
		$Form->ZeileEnde();
		
		$Form->Trennzeile();
		
		if ($rsRFK->EOF ())
		{
			echo '<span class=HinweisText>Es wurden keine Datensätze gefunden.</span>';
		} 
		elseif (($rsRFK->AnzahlDatensaetze () > 0) or (isset ( $_GET ['Liste'] ))) // Liste anzeigen
		{
			$Form->ZeileStart ();

			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFA_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFA_DATUM'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_DATUM'],130,'',$Link);

			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFA_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFA_BEMERKUNG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFA']['RFA_BEMERKUNG'],250,'',$Link);
			
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFP_AST_ATUNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFP_AST_ATUNR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['RFF'] ['RFF_AST_ATUNR'], 100, '', $Link );

			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFP_RUECKLIEFKZ,RFA_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFP_RUECKLIEFKZ,RFA_DATUM'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Rueckfuehrungstyp'],200,'',$Link);			
			$Form->ZeileEnde ();
			
			$RFKZeile = 0;
			
			while ( ! $rsRFK->EOF () )
			{
				$Form->ZeileStart ();
				
				if(($Recht3910&16)==16)
					{	
					$Link = '/rueckfuehrung/anforderung/rueckfuehrung_Main.php?cmdAktion=Details&RFA_KEY='.$rsRFK->FeldInhalt('RFA_KEY').' target="_blank"';
				}
				else{
					$Link ='';
				}
			
				$ANFNR = $Form->Format('D',$rsRFK->FeldInhalt('RFA_DATUM')).' - '.str_pad($Form->Format('T',$rsRFK->FeldInhalt('RFA_LFDNR')), 3, "0", STR_PAD_LEFT);			
				$Form->Erstelle_ListenFeld('RFA_DATUM',$ANFNR,0,130,false,($RFKZeile%2),'',$Link,'T','L');
				$Form->Erstelle_ListenFeld('RFA_BEMERKUNG',$rsRFK->FeldInhalt('RFA_BEMERKUNG'),0,250,false,($RFKZeile%2),'','','T','L');
				$Form->Erstelle_ListenFeld('RFP_AST_ATUNR',$rsRFK->FeldInhalt('RFP_AST_ATUNR'),0,100,false,($RFKZeile%2),'','','T','L');

				$RueckLiefKZ='';
				if ($rsRFK->FeldInhalt('RFP_RUECKLIEFKZ') != '')
				{
					$RueckLiefText = explode("|",$AWISSprachKonserven['Liste']['lst_Rueckfuehrungstyp']);			
					foreach ($RueckLiefText as $Text)
					{
						$RFKZ = explode("~",$Text);				
						if (($RFKZ[0])==$rsRFK->FeldInhalt('RFP_RUECKLIEFKZ'))
						{
							$RueckLiefKZ = $RFKZ[1];					
						}				
					}
				}		
				$Form->Erstelle_ListenFeld('RFP_RUECKLIEFKZ',$RueckLiefKZ,0,200,false,($RFKZeile%2),'','','T','L');
				
				$Form->ZeileEnde ();
				
				$rsRFK->DSWeiter ();
				$RFKZeile ++;
			}
				
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
			$Form->BlaetternZeile ( $MaxDS, $ZeilenProSeite, $Link, $Block, '' );
			$Form->Formular_Ende ();
		}
		
		//***************************************
		// Schaltflächen für dieses Register
		//***************************************
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche( 'href', 'cmd_zurueck', '/rueckfuehrung/bearbeitung/rueckfuehrungsbearbeitung_Main.php?cmdAktion=Rueckfuehrungstyp', '/bilder/cmd_zurueck.png', $AWISSprachKonserven ['Wort'] ['lbl_zurueck'], 'Z' );
		$Form->SchaltflaechenEnde();
	}
	
	$AWISBenutzer->ParameterSchreiben ( "RFKRueckfuehrungstyp", serialize ( $Param ) );
	$Form->SetzeCursor ( $AWISCursorPosition );
	$Form->SchreibeHTMLCode ('</form>');
} 
catch ( Exception $ex )
{
	if ($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen ( 'INTERN', $ex->getMessage (), 'MELDEN', 6, "200906241613" );
	} 
	else 
	{
		echo 'allg. Fehler:' . $ex->getMessage ();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $DB;
	
	$Bedingung = ' WHERE RFA.RFA_RFS_KEY=(SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS=22) ';
	
	if (isset ( $Param ['AST_ATUNR'] ) and $Param ['AST_ATUNR'] != '') {
		$Bedingung .= ' AND RFP.RFP_AST_ATUNR ' . $DB->LikeOderIst ( $Param ['AST_ATUNR'], awisDatenbank::AWIS_LIKE_UPPER ) . ' ';
	}
	
	return $Bedingung;
}
?>