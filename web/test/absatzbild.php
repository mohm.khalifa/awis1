<?php
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
include ("jpgraph/jpgraph.php");
include ("jpgraph/jpgraph_line.php");

$con = @OCILogon("awis","IlOuLua","AWIS3");

$ATUNR = "";

if (empty($_GET["atunr"]))
{
	$ATUNR = 'FU6071';
}
else
{
	$ATUNR = strtoupper($_GET["atunr"]);
}

if($con)
{
	$sql = "select ABS_FIL_MONABS1, ABS_FIL_MONABS2, ABS_FIL_MONABS3, ABS_FIL_MONABS4, ABS_FIL_MONABS5, ABS_FIL_MONABS6, ABS_FIL_MONABS7, ABS_FIL_MONABS8, ";
   	$sql .= "ABS_FIL_MONABS9, ABS_FIL_MONABS10, ABS_FIL_MONABS11, ABS_FIL_MONABS12 from absatz ";
	$sql .= "where abs_atunr = '" . $ATUNR . "' ";
	$sql .= "and abs_jahr = 2003";

	$rsDaten = awisOpenRecordset($con, $sql);
	$rsDatenZeilen = $awisRSZeilen;

}
if($con)
{
	$sql = "select ABS_FIL_MONABS1, ABS_FIL_MONABS2, ABS_FIL_MONABS3, ABS_FIL_MONABS4, ABS_FIL_MONABS5, ABS_FIL_MONABS6, ABS_FIL_MONABS7, ABS_FIL_MONABS8, ";
   	$sql .= "ABS_FIL_MONABS9, ABS_FIL_MONABS10, ABS_FIL_MONABS11, ABS_FIL_MONABS12 from absatz ";
	$sql .= "where abs_atunr = '" . $ATUNR . "' ";
	$sql .= "and abs_jahr = 2004";

	$rsDaten2 = awisOpenRecordset($con, $sql);
	$rsDatenZeilen2 = $awisRSZeilen;

}
if($con)
{
	$sql = "select ABS_FIL_MONABS1, ABS_FIL_MONABS2, ABS_FIL_MONABS3, ABS_FIL_MONABS4, ABS_FIL_MONABS5, ABS_FIL_MONABS6, ABS_FIL_MONABS7, ABS_FIL_MONABS8, ";
   	$sql .= "ABS_FIL_MONABS9, ABS_FIL_MONABS10, ABS_FIL_MONABS11, ABS_FIL_MONABS12 from absatz ";
	$sql .= "where abs_atunr = '" . $ATUNR . "' ";
	$sql .= "and abs_jahr = 2005";

	$rsDaten3 = awisOpenRecordset($con, $sql);
	$rsDatenZeilen3 = $awisRSZeilen;

	awisLogoff($con);			// Wieder abmelden

}

$ydata = array($rsDaten["ABS_FIL_MONABS1"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS2"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS3"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS4"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS5"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS6"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS7"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS8"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS9"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS10"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS11"][0]);
array_push($ydata,$rsDaten["ABS_FIL_MONABS12"][0]);

$ydata2 = array($rsDaten2["ABS_FIL_MONABS1"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS2"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS3"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS4"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS5"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS6"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS7"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS8"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS9"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS10"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS11"][0]);
array_push($ydata2,$rsDaten2["ABS_FIL_MONABS12"][0]);

$ydata3 = array($rsDaten3["ABS_FIL_MONABS1"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS2"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS3"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS4"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS5"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS6"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS7"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS8"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS9"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS10"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS11"][0]);
array_push($ydata3,$rsDaten3["ABS_FIL_MONABS12"][0]);

// $ydata = array(11,3,8,12,5,1,9,13,5,7);
// $ydata2 = array(1,19,15,7,22,14,5,9,21,13);

// Create the graph. These two calls are always required
$graph = new Graph(400,200,"auto");
// $graph = new Graph();
$graph->SetScale("textlin");
$graph->SetBackgroundGradient('navy','white',GRAD_HOR,BGRAD_MARGIN);

// Create the linear plot
$lineplot=new LinePlot($ydata);
$lineplot2=new LinePlot($ydata2);
$lineplot3=new LinePlot($ydata3);

// Add the plot to the graph
$graph->Add($lineplot);
$graph->Add($lineplot2);
$graph->Add($lineplot3);

$graph->img->SetMargin(40,100,20,40);
$graph->title->Set($ATUNR);
$graph->title->SetColor("white");
//$graph->xaxis->title->Set("Monate");
//$graph->yaxis->title->Set("Menge");

$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);

// Use months as X-labels
$gDateLocale = new DateLocale();
$gDateLocale->Set('de_DE');
$graph->xaxis->SetTickLabels($gDateLocale->GetShortMonth());

$lineplot->SetColor("orange");
$lineplot->SetWeight(2);

$lineplot2->SetColor("red");
$lineplot2->SetWeight(2);

$lineplot3->SetColor("blue");
$lineplot3->SetWeight(2);

// Set the legends for the plots
$lineplot->SetLegend("2003");
$lineplot2->SetLegend("2004");
$lineplot3->SetLegend("2005");

// Adjust the legend position
$graph->legend->Pos(0.05,0.5,"right","center");
$graph->legend->SetShadow(0);

//$graph->yaxis->SetColor("red");
$graph->yaxis->SetWeight(2);
$graph->SetShadow();

// Display the graph
$graph->Stroke();
?>


