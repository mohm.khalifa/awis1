<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;


try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KTA','%');
	$TextKonserven[]=array('KTK','%');
	$TextKonserven[]=array('KTF','%');
	$TextKonserven[]=array('KT1','%');
	$TextKonserven[]=array('FLB','HinweisQSCheck');
	$TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Offen');
	$TextKonserven[]=array('Wort','ZeitVon');
	$TextKonserven[]=array('Wort','ZeitBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','lst_Auftrag_Status');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$speichern = '';
	$BmOffen = false;
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$EditRecht = true;
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht26000 = $AWISBenutzer->HatDasRecht(26000);
	if($Recht26000==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$Form->DebugAusgabe(1,$_POST);
	$MAXEditTage=2;
	$MAXEditTageErweitert=7;		// Mit dem Recht 10
	$TestRecht2 = true;	// todo
	//********************************************************
	// Parameter verarbeiten
	//********************************************************
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./kdtelefonie_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KTA'));
		$AWIS_KEY1=$_POST['txtKTA_KEY'];
		$speichern = true;
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_GET['PEI_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PEI_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_POST['txtPEZ']) and $AWIS_KEY1 != 0)
	{
		//$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PEI_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_GET['PEI_XBN_KEY']) AND isset($_GET['PEI_DATUM']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
		$Param['PBM_XBN_KEY']=$DB->FeldInhaltFormat('N0',$_GET['PEI_XBN_KEY']);
		$Param['PEI_DATUM_VOM']=$Form->Format('D',$_GET['PEI_DATUM']);
		$Param['PEI_DATUM_BIS']=$Form->Format('D',$_GET['PEI_DATUM']);
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
		$Param = array();

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';

		//$Param['PEB_KEY']=$Form->Format('N0',$_POST['txtPEB_KEY'],true);
		//$Param['KTA_DATUM_VOM']=$Form->Format('D',$_POST['sucKTA_DATUM_VOM'],true);
		//$Param['KTA_DATUM_BIS']=$Form->Format('D',$_POST['sucKTA_DATUM_BIS'],true);
		$Param['KTA_FIL_ID']=$Form->Format('T',$_POST['sucKTA_FIL_ID'],true);
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
		{
			$Param['FIL_GEBIET']=$Form->Format('T',$_POST['sucFIL_GEBIET'],true);
		}
		$Param['KTA_STATUS']=$Form->Format('NO',$_POST['sucKTA_STATUS'],true);
		if ($Param['KTA_STATUS']=='')
		{
			$Param['KTA_STATUS'] = '100';
		}
		$Param['GRUPPE'] = '';
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$SQL = 'SELECT * from KDTELEFONIEBEARBEITERINFOS';
			$SQL .= ' WHERE KTI_INFORMATION = \'FILIALZUGRIFF\'';
			$rsGR = $DB->RecordSetOeffnen($SQL);
			while(!$rsGR->EOF())
			{
				$Param['GRUPPE'] .=  $rsGR->FeldInhalt('KTI_KTB_ID').',';
				$Form->DebugAusgabe(1,$Param['GRUPPE']);
				$rsGR->DSWeiter();
			}
			$Param['GRUPPE'] = substr($Param['GRUPPE'],0,strlen($Param['GRUPPE'])-1);
			$Bearbeiter = 'Filiale';
		}
		else
		{
			$SQL = 'SELECT * from KDTELEFONIEBEARBEITERINFOS';
			$SQL .= ' WHERE KTI_INFORMATION = \'GRUPPE\'';
			$rsGR = $DB->RecordSetOeffnen($SQL);
			while(!$rsGR->EOF())
			{
				//$Form->DebugAusgabe(1,$rsGR->FeldInhalt('KTI_WERT'));
				$SQL = 'SELECT * from BENUTZERGRUPPENBEZ';
				$SQL .= ' INNER JOIN Benutzergruppen ON XBG_XBB_KEY = XBB_KEY';
				$SQL .= ' INNER JOIN Benutzer on xbg_xbn_key = xbn_key';
				$SQL .= ' WHERE XBB_BEZ = \''.$rsGR->FeldInhalt('KTI_WERT').'\'';
				$rsGRBEZ = $DB->RecordSetOeffnen($SQL);
				echo '<br>';
				//$Form->DebugAusgabe(1,$rsGRBEZ->FeldInhalt('XBG_XBN_KEY'));
				while(!$rsGRBEZ->EOF())
				{
					//$Form->DebugAusgabe(1,$rsGRBEZ->FeldInhalt('XBG_XBN_KEY'));
					if($rsGRBEZ->FeldInhalt('XBG_XBN_KEY') == $AWISBenutzer->BenutzerID())
					{
						$Param['GRUPPE'] = $Param['GRUPPE'] .','.$rsGR->FeldInhalt('KTI_KTB_ID');
					}
					$rsGRBEZ->DSWeiter();
				}
				$rsGR->DSWeiter();
			}
			$Param['GRUPPE'] = substr($Param['GRUPPE'],1);
			$Bearbeiter = 'Gruppe';
		}
		if ($Param['GRUPPE'] == '')
		{
			$Bearbeiter = 'Person';
		}
		$Param['ALLE']=isset($_POST['sucAlleAnzeigen'])?'on':'';
		
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$AWISBenutzer->ParameterSchreiben('Formular_KTA',serialize($Param));
		
		//$Form->DebugAusgabe(1,$Param['KTA_FIL_ID'],$Param['FIL_GEBIET'],$Param['KTA_STATUS'],$Param['ALLE']);

	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KTA'));
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_KTA',serialize($Param));
		}

		if(isset($_GET['KTAListe']))
		{
			//$Form->DebugAusgabe(1,'Test');
			$Param['KEY']=0;
		}
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KTA'));
		$AWIS_KEY1=$Param['KEY'];
	}

	//var_dump($Param['GRUPPE']);
	
	//********************************************************
	// Daten suchen
	//********************************************************
    if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY KTA_FIL_ID ASC';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	
	$Bedingung = '';
	if($Param['GRUPPE'] <> '' and $Param['ALLE'] <> 'on')
	{
		$Bedingung .= ' AND kta_ktb_id in ('.$Param['GRUPPE'].')';
	}
	if($Param['KTA_FIL_ID'] <> '')
	{
		if($Param['KTA_FIL_ID'] <> 0)
		{
			$Bedingung .= ' AND kta_fil_id = '.$Param['KTA_FIL_ID'];
		}
		else 
		{
			$Bedingung .= ' AND kta_fil_id in ('.$FilZugriff.')';
		}
	}

	if($Param['KTA_STATUS'] <> '0')
	{
			$Bedingung .= ' AND kta_status = '.$Param['KTA_STATUS'];
	}
	
	$Form->DebugAusgabe(1,$Param);
	$Form->DebugAusgabe(1,$AWISBenutzer->BenutzerKontaktKEY());
	
	if(($Recht26000&16)!=16)
	{
		if(isset($Param['FIL_GEBIET']))
		{
			if($Param['ALLE'] <> 'on')
			{
					if($Param['FIL_GEBIET'] == 0)
					{
						if($Param['GRUPPE'] == '')
						{
							$Bedingung .= ' and kta_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
						}
					}
					else
					{
						if($Param['GRUPPE'] == '')
						{
							$Bedingung .= ' and kta_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
						}
							$Bedingung .= " and kta_fil_id in (select distinct xx1_fil_id from v_filialebenenrollen_aktuell where xx1_pfadkey || '/' like '%/".$Param['FIL_GEBIET']."/%')";
					}
			}
		    else
		    {
		    	if($Param['GRUPPE'] == '')
		    	{
    		        if($Param['FIL_GEBIET'] == 0)
    		    	{
    		    		$SQL2 = 'SELECT FEB_KEY';
    		    		$SQL2 .= ' FROM Filialebenen';
    		    		$SQL2 .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';
    		    		$SQL2 .= ' AND EXISTS';
    		    		$SQL2 .= '  (select DISTINCT XX1_FEB_KEY';
    		    		$SQL2 .= '  FROM V_FILIALEBENENROLLEN_AKTUELL';
    		    		$SQL2 .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
    		    		$SQL2 .= '  WHERE  XX1_FEB_KEY = FEB_KEY';
    		    		$SQL2 .= '  AND XX1_KON_KEY ='.$AWISBenutzer->BenutzerKontaktKEY();
    		    		$SQL2 .= ')';
    		    			
    		    		$Bedingung .= ' and kta_fil_id in (select distinct xx1_fil_id from v_filialebenenrollen_aktuell where xx1_feb_key in ('.$SQL2.'))';
    		    	}
    		    	else
    		    	{
    		    		$Bedingung .= ' and kta_fil_id in (select distinct xx1_fil_id from v_filialebenenrollen_aktuell where xx1_feb_key ='.$Param['FIL_GEBIET'].')';
    		    	}
		    	}
		    	else
		    	{
		    	    if ($Param['FIL_GEBIET'] == 0)
		    	    {
		    	        $SQL2 = 'select distinct xx1_fil_id';
                        $SQL2 .= ' FROM v_filialpfad';
                        $SQL2 .= ' INNER JOIN kdtelefoniebearbeiterinfos';
                        $SQL2 .= ' ON kti_ktb_id='.$Param['GRUPPE'];
                        $SQL2 .= ' inner join kdtelefoniebearbeiter';
                        $SQL2 .= ' on ktb_id=kti_ktb_id';
                        $SQL2 .= " where xx1_pfadkeys || '/' like '%/' || ktb_feb_key || '/%'";    
		    	    }
		    	    else
		    	    {
		    	        $SQL2 = 'select distinct xx1_fil_id';
                        $SQL2 .= ' FROM v_filialpfad';
                        $SQL2 .= " where xx1_pfadkeys || '/' like '%/' ||" .$Param['FIL_GEBIET'] ." || '/%'";    
		    	    }
                   $Bedingung .= ' and kta_fil_id in ('.$SQL2.')';
		    	}	
		    }
		}
	}
	else
	{
		if($Param['ALLE'] <> 'on')
		{
			$Bedingung .= ' and kta_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
			if($Param['FIL_GEBIET'] <> 0)
			{
				$Bedingung .= " and kta_fil_id in (select distinct xx1_fil_id from v_filialebenenrollen_aktuell where xx1_pfadkey || '/' like '%/".$Param['FIL_GEBIET']."/%')";
			}
		}
		else 
		{
			if(isset($Param['FIL_GEBIET']))
			{
				if($Param['FIL_GEBIET'] <> 0)
				{
					$Bedingung .= " and kta_fil_id in (select distinct xx1_fil_id from v_filialebenenrollen_aktuell where xx1_pfadkey || '/' like '%/".$Param['FIL_GEBIET']."/%')";
				}
			}
		}
	}
	
	$SQL = 'SELECT kta_key,kta_vorgangsnr,kta_email,kta_fil_id as FILIALE,kta_nachname as NACHNAME,kta_vorname as VORNAME,kta_plz as PLZ,kta_wohnort as WOHNORT,kta_telefon as TELEFON,kta_ktk_id,nvl(kti_wert,\' \') as EMPF';
	$SQL .= ' FROM KDTELEFONIEAUFTRAG';
	$SQL .= ' LEFT JOIN KDTELEFONIEBEARBEITERINFOS';
	$SQL .= ' ON KTA_KTB_ID=KTI_KTB_ID AND KTI_INFORMATION=\'FILIALZUGRIFF\'';
		//$SQL .= ' WHERE kta_datum <= sysdate - 3';
	if ($Bedingung <> '')
	{
	    $SQL .= ' WHERE ' .substr($Bedingung, 4);
	}
	
	$SQL .= $ORDERBY;

//var_dump($SQL);
$Form->DebugAusgabe(1,$SQL);
	
	$rsPEI = $DB->RecordSetOeffnen($SQL);

	//**************************************
	// Rechte des Benutzers ermitteln
	//**************************************
	$SQL = 'SELECT DISTINCT PBR_PBZ_KEY, PBM_PEB_KEY ';
	$SQL .= ' FROM PerseinsbereicheRechteVergabe';
	$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBR_PBM_KEY = PBM_KEY';
	$SQL .= ' INNER JOIN benutzer ON PBM_XBN_KEY = XBN_KEY AND XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
	$SQL .= ' WHERE PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	//$SQL .= ' AND PBM_PEB_KEY = 0'.$Param['PEB_KEY'];
	if(true==false)
	{
		//$SQL .= ' AND PBM_PEB_KEY IN (SELECT PBM_PEB_KEY FROM PersEinsBereicheMitglieder ';
		//$SQL .= ' WHERE PBM_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY']).')';
	}
	else
	{
		$SQL .= " AND EXISTS (SELECT *";
		$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
		$SQL .= "       FROM Perseinsbereichemitglieder";
		$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
		$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
		$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
		$SQL .= " GROUP BY PEB_KEY) Bereiche";
		//$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key ";
		$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate"; //TR 04.02.11
		$SQL .= " AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
		$SQL .= ' WHERE pbm_xbn_key = XBN_KEY';
		$SQL .= ' )';
	}

	$SQL .= ' AND (XBN_STATUS = \'A\' OR XBN_KEY = 0'.$AWISBenutzer->BenutzerID().')';
	$SQL .= ' ORDER BY PBR_PBZ_KEY ';
	$rsPBR = $DB->RecordSetOeffnen($SQL);
	$RechtPEI = 0;
	while(!$rsPBR->EOF())
	{
		$RechtPEI = $RechtPEI  | pow(2,($rsPBR->FeldInhalt('PBR_PBZ_KEY')-1));
		$rsPBR->DSWeiter();
	}
	
//$Form->DebugAusgabe(1, $SQL, $RechtPEI);	

	if ($TestRecht2)	// todo
	{
		//$FeldBreiten['Name'] -= 20;
		$FeldBreiten['PdfBGHW'] = 20;
	}
	$Form->Formular_Start();
	$Form->SchreibeHTMLCode('<form name=frmKundentelefonie action=./kdtelefonie_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');
	if(true===true)
	{
		if (($rsPEI->AnzahlDatensaetze() > 1 AND ! isset($_GET['KTA_KEY']) AND $speichern != true) OR $AWIS_KEY1 = 0)
		{
				//$Form->DebugAusgabe(1,'Test2');
				//**********************************************
				// Vorhandene Daten anzeigen
				//**********************************************
				$Form->ZeileStart();
				$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=KTA_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_FIL_ID'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['FILIALE'],60,'',$Link);
				$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=KTA_VORGANGSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_VORGANGSNR'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['WANUMMER'],80,'',$Link);
				$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=KTA_NACHNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_NACHNAME'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['KTA_NACHNAME'],150,'',$Link);
				$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=KTA_VORNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_VORNAME'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['KTA_VORNAME'],120,'',$Link);
				$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=KTA_WOHNORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_WOHNORT'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['KTA_WOHNORT'],130,'',$Link);
				$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=KTA_PLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_PLZ'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['KTA_PLZ'],100,'',$Link);
				$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				$Link .= '&Sort=KTA_TELEFON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_TELEFON'))?'~':'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['TELEFON'],130,'',$Link);
				if(($Recht26000&16)==16)
				{
				    $Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				    $Link .= '&Sort=KTA_EMAIL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTA_EMAIL'))?'~':'');
				    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['KTA_EMAIL'],180,'',$Link);
				}
				else if ($Bearbeiter == 'Filiale')
				{
				    $Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				    $Link .= '&Sort=KTI_WERT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KTI_WERT'))?'~':'');
				    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTA']['txt_BEARBEITER'],180,'',$Link);   
				}
				//$Link = './kdtelefonie_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				//$Link .= '&Sort=PEI_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEI_BEMERKUNG'))?'~':'');
				//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_BEMERKUNG'],120,'',$Link);
				//$Link = './personaleinsaetze_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
				//$Link .= '&Sort=PEI_BERICHTTYP'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEI_BERICHTTYP'))?'~':'');
				//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_BERICHTTYP'],45,'',$Link);
				if ($TestRecht2)	// todo
				{
					// �berschrift der Listenansicht: PDF Berufsgenossenschaft
					$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['PdfBGHW'], '', '', $AWISSprachKonserven['KTA']['TTT_PdfAuftrag'], 'C');
				}
				$Form->ZeileEnde();
	
				$PEIZeile=0;
	
				$Pruefung = array();	// Pr�fung der eizelnen Werte
	
				while(!$rsPEI->EOF())
				{
					$EditRechtErweitert=false;
					
					$Icons = array();
					if(($RechtPEI&4)!=0)	// �ndernrecht
					{
						//TODO: �ndern auf PEI-Recht!!
						if(($Recht26000&16)!=0 OR $rsPEI->FeldInhalt('PEI_XBN_KEY')==$AWISBenutzer->BenutzerID())
						{
							if((($RechtPEI&64)!=0 OR $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTage,date('Y')))
							OR (($RechtPEI&512)!=0 AND $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTageErweitert,date('Y'))))
							{
								$EditRechtErweitert=true;
								
								$Icons[] = array('edit','./personaleinsaetze_Main.php?cmdAktion=Details&PEI_KEY='.$rsPEI->FeldInhalt('PEI_KEY'));
							}
						}
					}
					if(($RechtPEI&8)!=0)	// L�schberechtigung
					{
						if(($Recht26000&16)!=0 OR $rsPEI->FeldInhalt('PEI_XBN_KEY')==$AWISBenutzer->BenutzerID())
						{
							if((($RechtPEI&64)!=0 OR $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTage,date('Y')))
							OR (($RechtPEI&512)!=0 AND $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTageErweitert,date('Y'))))
							{
								$Icons[] = array('delete','./personaleinsaetze_Main.php?cmdAktion=Details&Del='.$rsPEI->FeldInhalt('PEI_KEY'));
							}
						}
					}
	
					if ($rsPEI->FeldInhalt('PEI_PLANUNGSTYP')=='4')
					{
						$Pruefung['Test']=2;
					}
					else 
					{
						$Pruefung['Test']=0;
						if(isset($Pruefung[0]['USER']) AND $rsPEI->FeldInhalt('PEI_XBN_KEY')==$Pruefung[0]['USER'] AND $Pruefung[0]['TAG']==$rsPEI->FeldInhalt('PEI_DATUM'))
						{
							if($ORDERBY == ' ORDER BY PEI_DATUM ASC, PEI_XBN_KEY, PEI_VONXTZ_ID ASC')
							{
								if($Pruefung[0]['ZEITBIS']==$rsPEI->FeldInhalt('PEI_VONXTZ_ID'))
								{
									$Pruefung['Test']=1;
								}
							}
							elseif($ORDERBY == ' ORDER BY PEI_DATUM DESC, PEI_XBN_KEY, PEI_VONXTZ_ID DESC')
							{
								if($Pruefung[0]['ZEITVON']==$rsPEI->FeldInhalt('PEI_BISXTZ_ID'))
								{
									$Pruefung['Test']=1;
								}
							}
							else
							{
								$Pruefung['Test']=1;
							}
						}
						else
						{
							$Pruefung['Test']=1;
						}
					}
					$Pruefung[0]['ZEITVON']=$rsPEI->FeldInhalt('PEI_VONXTZ_ID');
					$Pruefung[0]['ZEITBIS']=$rsPEI->FeldInhalt('PEI_BISXTZ_ID');
					$Pruefung[0]['TAG']=$rsPEI->FeldInhalt('PEI_DATUM');
					$Pruefung[0]['USER']=$rsPEI->FeldInhalt('PEI_XBN_KEY');
	
					$FilBez = ($rsPEI->FeldInhalt('PEI_FIL_ID')!=''?' ('.$rsPEI->FeldInhalt('FIL_BEZ').')':'');
					$PdfDatei = '/daten/web/dokumente/kdtelefonie/kt_'.str_pad($rsPEI->FeldInhalt('FILIALE'),4,'000',STR_PAD_LEFT).'_'.$rsPEI->FeldInhalt('KTA_VORGANGSNR').'.pdf';
					
		
						$Form->ZeileStart();
						//$Form->Erstelle_ListeIcons($Icons,36,($PEIZeile%2));
						//$Link = ($rsPEI->FeldInhalt('XBN_KON_KEY')==''?'':'/telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey='.$rsPEI->FeldInhalt('XBN_KON_KEY'));
						$Form->Erstelle_ListenFeld('FILIALE',$rsPEI->FeldInhalt('FILIALE'),0,60,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T');
						$Link = '';
						$Link='./kdtelefonie_Main.php?cmdAktion=Details&KTA_KEY='.$rsPEI->FeldInhalt('KTA_KEY').'&PBE=1';
						$Form->Erstelle_ListenFeld('VORGANGSNR',$rsPEI->FeldInhalt('KTA_VORGANGSNR'),0,80,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'',$Link,'T');
						//$Form->Erstelle_ListenFeld('',$rsPEI->FeldInhalt('PEI_DATUM'),0,90,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','D');
						//$Link='./kdtelefonie_Main.php?cmdAktion=Details&KTA_KEY='.$rsPEI->FeldInhalt('KTA_KEY').'&PBE=1';
						$Form->Erstelle_ListenFeld('NACHNAME',$rsPEI->FeldInhalt('NACHNAME'),0,150,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T');
						$Form->Erstelle_ListenFeld('VORNAME',$rsPEI->FeldInhalt('VORNAME'),0,120,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T');
						$Form->Erstelle_ListenFeld('WOHNORT',$rsPEI->FeldInhalt('WOHNORT'),0,130,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T','L',$rsPEI->FeldInhalt('PEA_BEZEICHNUNG'));
						$Form->Erstelle_ListenFeld('PLZ',$rsPEI->FeldInhalt('PLZ'),0,100,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T');
						$Form->Erstelle_ListenFeld('TELEFON',$rsPEI->FeldInhalt('TELEFON'),0,130,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T');
				        if(($Recht26000&16)==16)
				        {
						    $Form->Erstelle_ListenFeld('EMAIL',$rsPEI->FeldInhalt('KTA_EMAIL'),0,180,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T');
				        }
				        else if ($Bearbeiter == 'Filiale')
				        {
				            $Form->Erstelle_ListenFeld('EMAIL',$rsPEI->FeldInhalt('EMPF'),0,180,false,(($Pruefung['Test'])==0?2:(($Pruefung['Test'])==2?3:($PEIZeile%2))),'','','T');
				        }
						//$Form->DebugAusgabe(1, $PdfDatei);
						
						
						if ($TestRecht2)	// todo
						{
							$Icons = array();
							//$LinkPDF = '/berichte/drucken.php?XRE=36&ID='.base64_encode('kta_fil='.urlencode(str_pad($rsPEI->FeldInhalt('FILIALE'),4,'000',STR_PAD_LEFT)).'&kta_vgnr='.urlencode($rsPEI->FeldInhalt('KTA_VORGANGSNR')));
							$LinkPDF = '/dokumentanzeigen.php?bereich=kdtelefonie&dateiname=kt_'.str_pad($rsPEI->FeldInhalt('FILIALE'),4,'000',STR_PAD_LEFT).'_'.$rsPEI->FeldInhalt('KTA_VORGANGSNR').'&erweiterung=pdf';
							//$Icons[] = array('pdf', '/berichte/drucken.php?XRE=36&ID='.base64_encode('KTA_FILIALE=' . urlencode('=~') . str_pad($rsPEI->FeldInhalt('FILIALE'),4,'000',STR_PAD_LEFT)));
							$Icons[] = array('pdf', $LinkPDF);
							$Form->Erstelle_ListeIcons($Icons, $FeldBreiten['PdfBGHW'], ($PEIZeile%2));
						}
						$Form->ZeileEnde();
					$rsPEI->DSWeiter();
					$PEIZeile++;
				}
				//$Link = './personaleinsaetze_Main.php?cmdAktion=Details';
				//$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
				//***************************************
				// Schaltfl�chen f�r dieses Register
				//***************************************
				$Form->SchaltflaechenStart();
				
				$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
				
				
				//$Form->Schaltflaeche('href', 'cmdDrucken', './personaleinsaetze_Drucken_Liste.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');
				
				//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?ID=80052','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
				
				$Form->SchaltflaechenEnde();
				
				$Form->SchreibeHTMLCode('</form>');
		}
		else
		{
			if ($rsPEI->AnzahlDatensaetze() < 1)
			{
				$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
				die();
			}
			else
			{
				if (isset($_GET['KTA_KEY']))
				{
					$Key = $_GET['KTA_KEY'];
				}
				elseif (isset($_POST['txtKTA_KEY']))
				{
					//$Form->DebugAusgabe(1,"AwisKey",$_POST['txtKTA_KEY'] );
					$Key = $_POST['txtKTA_KEY'];
				}
				else
				{
					$Key = $rsPEI->FeldInhalt('KTA_KEY');
				}
				//$Form->DebugAusgabe(1,$Key );
				$SQL = 'SELECT kta_ktb_id,kta_fil_id as FILIALE,kta_vorgangsnr as VORGANG,kta_umsatz,kta_datum,kta_anrede as ANREDE,kta_titel as TITEL,kta_nachname as NACHNAME,kta_vorname as VORNAME,kta_plz as PLZ,kta_wohnort as WOHNORT,kta_strasse as STRASSE,kta_telefon as TELEFON,kta_mobil as MOBIL,ktk_Bezeichnung as KATALOG,ktk_textkonserve,lan_land as LAND,kta_user,kta_userdat,ktk_id,kta_land,fil_ort,kta_datum';
				$SQL .= ' FROM KDTELEFONIEAUFTRAG';
				$SQL .= ' INNER JOIN KDTELEFONIEKATALOG';
				$SQL .= ' ON kta_ktk_id = ktk_id';
				$SQL .= ' INNER JOIN LAENDER';
				$SQL .= ' ON lan_code = kta_land';
				$SQL .= ' INNER JOIN Filialen';
				$SQL .= ' ON kta_fil_id = fil_id';
				$SQL .= ' WHERE kta_key ='.$Key;
				$rsKDT = $DB->RecordSetOeffnen($SQL);
				
				//$Form->DebugAusgabe(1, $SQL);
				//$Form->DebugAusgabe(1, $Key);
				$Felder = array();
				//./kdtelefonie_Main.php?cmdAktion=Details&KTA_KEY='.$rsPEI->FeldInhalt('KTA_KEY').'&PBE=1'
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./kdtelefonie_Main.php?cmdAktion=Details&KTAListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKDT->FeldInhalt('KTA_USER'));
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsKDT->FeldInhalt('KTA_USERDAT')));
				$Form->InfoZeile($Felder,'');
				
				$SQL='SELECT FUNC_TEXTKONSERVE (\'KLKOPF\', \''.$rsKDT->FeldInhalt('KTK_TEXTKONSERVE').'\',\''.$rsKDT->FeldInhalt('KTA_LAND'). '\') as TK from dual';
				$rsTK = $DB->RecordSetOeffnen($SQL);	
				
				$vKopf = $rsTK->FeldInhalt('TK');
				$vKopf = str_replace('$KTA_DATUM$',substr($rsKDT->FeldInhalt('KTA_DATUM'),0,10),$vKopf);
				$vKopf = str_replace('$KTA_ANREDE$',$AWISSprachKonserven['KTA']['txt_ANREDE_'.$rsKDT->FeldInhalt('ANREDE')],$vKopf);
				$vKopf = str_replace('$KTA_TITEL$',$rsKDT->FeldInhalt('TITEL'),$vKopf);
				$vKopf = str_replace('$KTA_NACHNAME$',$rsKDT->FeldInhalt('NACHNAME').',',$vKopf);
				$vKopf = str_replace('$FIL_ORT$',$rsKDT->FeldInhalt('FIL_ORT'),$vKopf);
				//var_dump($rsTK->FeldInhalt('TK'));
				
				$Form->ZeileStart();
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTF']['UEBERSCHRIFT1'],800, 'font-weight:bolder', '', '');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['FILIALE'].':',200);
				$Form->Erstelle_TextLabel($rsKDT->FeldInhalt('FILIALE'),20);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['VORGANG'].':',200);
				$Form->Erstelle_TextLabel($rsKDT->FeldInhalt('VORGANG'),500);
				$Form->ZeileEnde();
				$Form->Trennzeile('O');
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KNAME'].':',200);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['txt_ANREDE_'.$rsKDT->FeldInhalt('ANREDE')].' '.$rsKDT->FeldInhalt('TITEL').' '.$rsKDT->FeldInhalt('NACHNAME').' '.$rsKDT->FeldInhalt('VORNAME'),500);
				$Form->ZeileEnde();	
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KTA_WOHNORT'].':',200);
				$Form->Erstelle_TextLabel($rsKDT->FeldInhalt('PLZ').' '.$rsKDT->FeldInhalt('WOHNORT'),500);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KTA_LAND'].':',200);
				$Form->Erstelle_TextLabel($rsKDT->FeldInhalt('LAND'),500);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KTA_TELEFON'].':',200);
				$Form->Erstelle_TextLabel($rsKDT->FeldInhalt('TELEFON'),500);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['MOBIL'].':',200);
				$Form->Erstelle_TextLabel($rsKDT->FeldInhalt('MOBIL'),500);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTF']['UEBERSCHRIFT2'],800, 'font-weight:bolder', '', '');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KATALOG'].':',200);
				$Form->Erstelle_TextLabel($rsKDT->FeldInhalt('KATALOG'),500);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KTF']['UEBERSCHRIFT3'],800, 'font-weight:bolder', '', '');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_Textarea('TK',$vKopf,600,1,4);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Trennzeile('L');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->ZeileEnde();
				
				$SQL = 'SELECT *';
				$SQL .= 'FROM KDTELEFONIEAUFTRAG';
				$SQL .= ' INNER JOIN KDTELEFONIEKATALOG on KTK_ID = KTA_KTK_ID';
				$SQL .= ' INNER JOIN KDTELEFONIEFRAGEN on KTK_ID = KTF_KTK_KEY';
				$SQL .= ' WHERE KTF_STATUS=\'A\'';
				$SQL .= ' AND kta_key ='.$Key;
				$SQL .= ' ORDER BY KTF_SORTIERUNG';
				
				//$Form->DebugAusgabe(1,$SQL);
				
				$rsKTF = $DB->RecordSetOeffnen($SQL);
				$Farbe = 'color:#FF0000';
			    $EditRecht = (($rsKTF->FeldInhalt('KTA_STATUS') >= 100 and $rsKTF->FeldInhalt('KTA_STATUS') <= 110) and 
							             (($Param['GRUPPE'] <> '' and strpos($Param['GRUPPE'], $rsKTF->FeldInhalt('KTA_KTB_ID'))) > 0 or 
							             ($rsKTF->FeldInhalt('KTA_KON_KEY') == $AWISBenutzer->BenutzerKontaktKEY())));
               
                $EditRecht = ($rsKTF->FeldInhalt('KTA_STATUS') >= 100 and $rsKTF->FeldInhalt('KTA_STATUS') < 120); //Debug         
				//var_dump($EditRecht);
				$i = 1;
				while (!$rsKTF->EOF())
				{
					if($rsKTF->FeldInhalt('KTF_KTF_ID') == '0' AND $i <> 1)
					{
						$Form->ZeileStart();
						$Form->Trennzeile('O');
						$Form->ZeileEnde();
					}
					
					$Form->ZeileStart();
					
					$SQL = 'SELECT *';
					$SQL .= 'FROM KDTELEFONIEAUFTRAG';
					$SQL .= ' INNER JOIN KDTELEFONIEKATALOG on KTK_id = KTA_KTK_ID';
					$SQL .= ' INNER JOIN KDTELEFONIEFRAGEN on KTK_id = KTF_KTK_KEY';
					$SQL .= ' INNER JOIN KDTELEFONIEAUFTRAGSZUORD on KTZ_KTF_ID = KTF_ID AND KTZ_KTA_KEY='.$Key;
					$SQL .= ' WHERE KTF_STATUS=\'A\'';
					$SQL .= ' AND kta_key ='.$Key;
					$SQL .= ' AND ktz_ktf_id ='.$rsKTF->FeldInhalt('KTF_ID');
					$SQL .= ' ORDER BY KTF_SORTIERUNG';
					
					$rsKTZ = $DB->RecordSetOeffnen($SQL);
					
					//�berschrift
					if($rsKTF->FeldInhalt('KTF_KTF_ID')=='0')
					{
						$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven[$rsKTF->FeldInhalt('KTK_TEXTKONSERVE')][$rsKTF->FeldInhalt('KTF_BEZEICHNUNG')].':',800);
					}
					else
					{
						if ($rsKTZ->FeldInhalt('KTZ_WERT')=='0' AND $rsKTZ->FeldInhalt('KTZ_BEMERKUNG') == '')
						{
							$Farbe = 'color:#FF0000';
							$BmOffen = true;
						}
						else
						{
							$Farbe = 'color:#000000';
						}
						$Frage = $AWISSprachKonserven[$rsKTF->FeldInhalt('KTK_TEXTKONSERVE')][$rsKTF->FeldInhalt('KTF_BEZEICHNUNG')];
						$Frage = str_replace('$FIL_ORT$',$rsKDT->FeldInhalt('FIL_ORT'),$Frage);
						$Form->Erstelle_TextLabel($Frage.':',420,$Farbe.';margin-top:5px');
					}
								
					$SQL = 'SELECT *';
					$SQL .= 'FROM KDTELEFONIEAUFTRAG';
					$SQL .= ' WHERE KTA_KEY='.$Key;
					$rsStat = $DB->RecordSetOeffnen($SQL);
					switch(substr($rsKTF->FeldInhalt('KTF_DATENQUELLE'),0,3))
					{
						case 'TXT':
							$Felder = explode(':',$rsKTF->FeldInhalt('KTF_DATENQUELLE'));
							//$Form->DebugAusgabe(1,$Felder[1],$Felder[2]);
							$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])), $AWISBenutzer->BenutzerSprache());
							$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
							//$Form->Erstelle_SelectFeld('KTF_WERT_'.$rsKTF->FeldInhalt('PBF_ID').'_'.'1','Test','150',$EditRecht,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
							if (!$EditRecht)
							{
								if ($rsKTZ->FeldInhalt('KTZ_WERT') == '0')
								{
									$Daten = explode('~',$Daten[1]);
									$Form->Erstelle_TextLabel($Daten[1],50,'font: bold;margin-left:75px;margin-top:5px');
								}
								else if ($rsKTZ->FeldInhalt('KTZ_WERT') == '')
								{
									$Form->Erstelle_TextLabel('',50,'font: bold;margin-left:75px;margin-top:5px');
								}
								else
								{
									$Daten = explode('~',$Daten[0]);
									$Form->Erstelle_TextLabel($Daten[1],50,'font: bold;margin-left:75px;margin-top:5px');
								}
							}
							else 
							{
								$Form->Erstelle_SelectFeld('KTZ_WERT_'.$rsKTF->FeldInhalt('KTF_ID').'_'.$rsKTZ->FeldInhalt('KTZ_KEY'),$rsKTZ->FeldInhalt('KTZ_WERT'),150,$EditRecht,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','margin-left:75px;margin-top:5px',$Daten);
							}
							//var_dump($rsPEZ->FeldInhalt('PEZ_WERT'));
							break;
						case 'SQL':
							//$Felder = explode(':',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							//$Form->Erstelle_SelectFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','');
							break;
						default:
							//$Form->Erstelle_TextFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_ZEICHEN'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','','',$rsPEZ->FeldInhalt('PBF_FORMAT'),'','',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							break;
					}
					//$Form->Erstelle_TextLabel($rsKTF->FeldInhalt('KTZ_BEMERKUNG'),80,''); //$AWISSprachKonserven[$rsPEZ->FeldInhalt('PBE_TEXTKONSERVE')][$rsPEZ->FeldInhalt('PBF_BEZEICHNUNG')].':',400);
					if($rsKTF->FeldInhalt('KTF_KTF_ID') <> '0' AND $rsKTZ->FeldInhalt('KTZ_WERT') <> 1 AND $rsKTZ->FeldInhalt('KTZ_WERT') != null)
					{
						//$Form->Erstelle_TextFeld('KTZ_BEMERKUNG_'.$rsKTF->FeldInhalt('KTF_ID').'_'.$rsKTZ->FeldInhalt('KTZ_KEY'),$rsKTZ->FeldInhalt('KTZ_BEMERKUNG'),32,340,$EditRecht,'','','','T','','','',255);	
						//$Form->Erstelle_Textarea('KTZ_BEMERKUNG_'.$rsKTF->FeldInhalt('KTF_ID').'_'.$rsKTZ->FeldInhalt('KTZ_KEY'),$rsKTZ->FeldInhalt('KTZ_BEMERKUNG'),5,25,5,$EditRecht,'font: bold');
						
						$SQL = 'SELECT *';
						$SQL .= 'FROM QMPLAENE';
						$SQL .= ' WHERE QMP_KEY='.$AWIS_KEY1;

						$rsKTT = $DB->RecordSetOeffnen($SQL);
						
						$count = 0;
						
						//$Form->Trennzeile('O');
						
						if($BmOffen == true)
						{
							$Form->ZeileStart();
							$Form->Hinweistext('Bitte Kriterien w�hlen.Bemerkung muss gef�llt sein!',1,'margin-left:0;margin-top:10px');
							$Form->ZeileEnde();
						}
						$Form->ZeileStart();
						// TODO : Textkonserve fehlt
						$Form->Erstelle_TextLabel('Bemerkung:',110,'font: bold;margin-top:10px');
						$Form->ZeileEnde();
						$Form->ZeileStart();
						$Form->Erstelle_Textarea('KTZ_BEMERKUNG_'.$rsKTF->FeldInhalt('KTF_ID').'_'.$rsKTZ->FeldInhalt('KTZ_KEY'), $rsKTZ->FeldInhalt('KTZ_BEMERKUNG'), 700, 80, 3,$EditRecht);
						//$Form->Erstelle_TextFeld('KTZ_BEMERKUNG_'.$rsKTF->FeldInhalt('KTF_ID').'_'.$rsKTZ->FeldInhalt('KTZ_KEY'),$rsKTZ->FeldInhalt('KTZ_BEMERKUNG'),50,340,$EditRecht,'','','','T','','','',255);
						$Form->ZeileEnde();
						
						
						// Textkonserve TODO:  fehlt
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel('Kriterien:',110,'font: bold;margin-top:10px');
						$Form->ZeileEnde();
						$KritCount = 0;
						while(!$rsKTT->EOF())
						{
							if($count == 0)
							{
								$Form->ZeileStart();
							}
							//$Form->DebugAusgabe(1,$rsKTT->FeldInhalt('KTT_KRITERIUM'));
							$Form->Erstelle_Checkbox('KTZ_KRITERIUM_'.$KritCount.'_'.$rsKTZ->FeldInhalt('KTZ_KEY'),substr($rsKTZ->FeldInhalt('KTZ_KRITERIUM'),$KritCount,1),30,$EditRecht, '1');
							echo '<input type=hidden name=txtKTT_KEY value='.$rsKTZ->FeldInhalt('KTZ_KEY').'>';
							$Form->Erstelle_TextLabel($rsKTT->FeldInhalt('KTT_KRITERIUM'),110);
							$rsKTT->DSWeiter();
							$count++;
							if($count == 2)
							{
								$Form->ZeileEnde();
								$count = 0;
							}
							$KritCount++;
						}
					}
					
					$i++;
					$rsKTF->DSWeiter();
					$Form->ZeileEnde();
				}
				$Form->ZeileStart();
				$Form->Trennzeile('L');
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->ZeileEnde();
				$Form->ZeileStart();
				//$Form->DebugAusgabe(1,$rsStat->FeldInhalt('KTZ_WERT'));
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KTA_STATUS'].':',75);
				$Daten = $AWISSprachKonserven['Liste']['lst_Auftrag_Status'];
				$Daten = explode('|',$Daten);
				if ($rsStat->FeldInhalt('KTA_STATUS') <> 120)
				{
					unset($Daten[1]);
				}
				//$Form->DebugAusgabe(1,$rsStat->FeldInhalt('KTA_STATUS'));
				//$Form->DebugAusgabe(1,$rsStat->FeldInhalt('KTA_WEITERVERARBEITUNG'));
				$Form->Erstelle_SelectFeld('KTA_STATUS',$rsStat->FeldInhalt('KTA_STATUS'),300,$EditRecht,'','~'.$AWISSprachKonserven['Wort']['Offen'],'','','',$Daten);
				$Form->ZeileEnde();
				/*
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['KTA']['KTA_WEITERVERABEITUNG'].':',200);
				$Daten = $AWISSprachKonserven['Liste']['lst_JaNein'];
				$Daten = explode('|',$Daten);
				if (!$EditRecht)
				{
					if ($rsStat->FeldInhalt('KTA_WEITERVERARBEITUNG') == 0)
					{
						$Feld = 1;
					}
					else
					{
						$Feld = 0;
					}
					$Daten = explode('~',$Daten[$Feld]);
					$Form->Erstelle_TextLabel($Daten[1],200);
					echo '<input type=hidden name=txtKTA_WV value='.$rsStat->FeldInhalt('KTA_WEITERVERARBEITUNG').'>';
					echo '<input type=hidden name=oldKTA_WV value='.$rsStat->FeldInhalt('KTA_WEITERVERARBEITUNG').'>';
				}
				else
				{
					$Form->Erstelle_SelectFeld('KTA_WV',$rsStat->FeldInhalt('KTA_WEITERVERARBEITUNG'),300,$EditRecht,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				}
				
				$Form->ZeileEnde();
				*/
				$Form->ZeileStart();
				$Form->ZeileEnde();
				echo '<input type=hidden name=txtKTA_KEY value='.$Key.'>';
			}
			//***************************************
			// Schaltfl�chen f�r dieses Register
			//***************************************
			$Form->SchaltflaechenStart();
			
			$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			
			if($EditRecht)
			{
			    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
			}
			$LinkPDF = '/dokumentanzeigen.php?bereich=kdtelefonie&dateiname=kt_'.str_pad($rsKDT->FeldInhalt('FILIALE'),4,'000',STR_PAD_LEFT).'_'.$rsKDT->FeldInhalt('VORGANG').'&erweiterung=pdf';
			$Form->Schaltflaeche('href', 'cmdDrucken',$LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');
			//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?ID=80052','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
			
			$Form->SchaltflaechenEnde();
			
			$Form->SchreibeHTMLCode('</form>');
		}
		$Form->Formular_Ende();
	}

	//$Form->DebugAusgabe(1, $Param, $Bedingung, $rsPEI, $_POST, $rsPEI, $SQL, $AWISSprache);
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PEI_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(($AWISBenutzer->HatDasRecht(4500)&16)==0)		// Nur den eigenen Bereich anzeigen
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM PerseinsBereicheMitglieder WHERE ';
		$Bedingung .= ' PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
	}

	// Einschr�nken nach Bereich
	if(isset($Param['PEB_KEY']) AND $Param['PEB_KEY']!='')
	{
		$Bedingung .= ' AND PEI_PEB_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PEB_KEY'],false);
	}
	// Einschr�nken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
		if($Param['PBM_XBN_KEY']==0)
		{
			$Bedingung .= " AND EXISTS (SELECT *";
			$Bedingung .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$Bedingung .= "       FROM Perseinsbereichemitglieder";
			$Bedingung .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$Bedingung .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$Bedingung .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			$Bedingung .= " GROUP BY PEB_KEY) Bereiche";
			$Bedingung .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key"; //TR 04.02.11
			//$Bedingung .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			$Bedingung .= ' WHERE pbm_xbn_key = PEI_XBN_KEY';
			$Bedingung .= ' )';
		}
		else
		{
			$Bedingung .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY'],false);
		}
	}
	// Einschr�nken nach Datum
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$Bedingung .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$Bedingung .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}

	if(isset($Param['PEI_PEA_KEY']) AND $Param['PEI_PEA_KEY']!='0')
	{
		$Bedingung .= ' AND PEI_PEA_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PEI_PEA_KEY'],false);
	}

	//var_dump($Param['PEI_FIL_ID']);
	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$Bedingung .= ' AND PEI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID'],false);
	}

	if(isset($Param['FER_KEY']) AND $Param['FER_KEY']!='0')
	{

		$Bedingung .= ' AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN';
		$Bedingung .= ' WHERE xx1_fer_key =0'.$DB->FeldInhaltFormat('N0',$Param['FER_KEY'],false);
		$Bedingung .= ' AND xx1_kon_key = KON_KEY)';
/*
  $Bedingung = " AND EXISTS (SELECT *
  FROM (
  SELECT KON_KEY, XX1_FER_KEY, FEB_GUELTIGAB, FRZ_GUELTIGAB, FEZ_GUELTIGAB, FER_GUELTIGAB,
  FEB_GUELTIGBIS, FRZ_GUELTIGBIS, FEZ_GUELTIGBIS, FER_GUELTIGBIS
  FROM v_FilialEbenenRollen
  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY
  INNER JOIN FilialEbenen ON XX1_FEB_KEY = FEB_KEY
  INNER JOIN FilialEbenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY
  INNER JOIN FilialEbenenZuordnungen ON XX1_FEZ_KEY = FEZ_KEY
  INNER JOIN FilialEbenenRollen ON XX1_FER_KEY = FER_KEY
  INNER JOIN FilialEbenenRollenBereiche ON FER_FRB_KEY = FRB_KEY
  WHERE XX1_FER_KEY =".$Param['FER_KEY']."
  ORDER BY XX1_STUFE, FRZ_GUELTIGBIS, FRZ_USERDAT DESC
  ) Daten WHERE KON_KEY = XBN_KON_KEY
  AND FEB_GUELTIGAB <= PEI_DATUM AND FEB_GUELTIGBIS >= PEI_DATUM
  AND FRZ_GUELTIGAB <= PEI_DATUM AND FRZ_GUELTIGBIS >= PEI_DATUM
  AND FEZ_GUELTIGAB <= PEI_DATUM AND FEZ_GUELTIGBIS >= PEI_DATUM
  AND FER_GUELTIGAB <= PEI_DATUM AND FER_GUELTIGBIS >= PEI_DATUM
  ) ";
*/
	}


	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>