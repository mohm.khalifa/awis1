<?php

class awisATUArtikel
{
	var $_AIT_VK = array();	
	
	var $_rsAST = null;
	
	var $_con = null;
	
	//public function __construct($con, $ATUNR)
	function awisATUArtikel($con, $ATUNR, $Sprache='DE')
	{
		$this->_AIT_VK['IT']=53;
		$this->_AIT_VK['NL']=52;
		$this->_AIT_VK['CH']=54;
		$this->_AIT_VK['CZ']=51;
		$this->_AIT_VK['AT']=50;

		$this->_con = $con;
/*		if($con == false)
		{
			throw new Exception('Keine Datenbankverbindung',200803241303);
		}
*/		

		$SQL = 'SELECT Artikelstamm.*, WGR_ID, WUG_ID, COALESCE(ASP_BEZEICHNUNG,AST_BEZEICHNUNGWW) AS ASP_BEZEICHNUNGWW';
		if($Sprache=='DE')
		{
			$SQL .= ', AST_VK';
		}
		else 
		{
			$SQL .= ',(SELECT ASI_WERT FROM Artikelstamminfos WHERE asi_ast_atunr = artikelstamm.ast_atunr AND asi_ait_id='.$this->_AIT_VK[$Sprache].') AS AST_VK';
		}
		$SQL .= ' FROM Artikelstamm';
		$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
		$SQL .= ' INNER JOIN Warengruppen ON WUG_WGR_ID = WGR_ID';
		$SQL .= ' LEFT OUTER JOIN ARTIKELSPRACHEN ON AST_ATUNR = ASP_AST_ATUNR AND ASP_LAN_CODE = \''.$Sprache.'\'';

		$rsAST = awisOpenRecordset($this->_con, $SQL);
	}
}


?>