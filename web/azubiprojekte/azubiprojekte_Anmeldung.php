<?php
global $Funktionen;
try {


    $Funktionen->RechteMeldung(1);

    if (isset($_POST['cmdSpeichern_x'])) {
        include_once './azubiprojekte_speichern.php';
    }

    $Funktionen->Form->SchreibeHTMLCode('<form name="frmAzubiprojekte" action="" method="POST" >');

    $LabelBreite = 300;
    $FeldBreite = 200;
    $Funktionen->Form->Formular_Start();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_INFOTEXT'].$Funktionen->AWISSprachKonserven['AZT']['AZT_BILDER'], 6, 'width: auto');
    $Funktionen->Form->ZeileEnde();

    $SQL = 'SELECT *';
    $SQL .= ' FROM AZUBIPROJEKTEBEREICHE';
    $SQL .= ' WHERE AZB_STATUS = \'A\'';
    $SQL .= ' ORDER BY AZB_STUFE';
    $rsAZB = $Funktionen->DB->RecordSetOeffnen($SQL);

    if ($rsAZB->FeldInhalt('AZB_KEY')!=''){
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_BEREICHSFRAGE'], $LabelBreite);

        $Selectfeldinhalt = '';
        for($i = $rsAZB->AnzahlDatensaetze(); $i > 0; $i--){
            if($i != $rsAZB->AnzahlDatensaetze()) {
                $Selectfeldinhalt .= "|";
            }
            $Selectfeldinhalt .= $rsAZB->FeldInhalt('AZB_STUFE').'~'.$rsAZB->FeldInhalt('AZB_BEREICHNAME');
            $rsAZB->DSWeiter();
        }

        $Funktionen->Form->Erstelle_SelectFeld('!AZB_STUFE', (isset($_POST['txtAZB_STUFE'])?$_POST['txtAZB_STUFE']:''), $FeldBreite, true, '', '~'.$Funktionen->AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','width: auto',explode('|',$Selectfeldinhalt));
        $Funktionen->Form->ZeileEnde();
        $Funktionen->AWISCursorPosition = "txtAZB_STUFE";
        
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($rsAZB->FeldInhalt('AZT_INFOTEXT'), 1000);
        $Funktionen->Form->ZeileEnde();

        //Hidden-Feld, damit Abteilung/FilNr nicht verworfen wenn Fehler fliegt
        $Funktionen->Form->Erstelle_HiddenFeld('AZT_FILNR_HIDDEN', isset($_POST['txtAZT_FILNR'])?$_POST['txtAZT_FILNR']:'');

        $Funktionen->LadeBereichInfoTexte();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_PERSNR'], $LabelBreite);
        $Funktionen->Form->Erstelle_TextFeld('!AZT_PERSNR', (isset($_POST['sucAZT_PERSNR'])?$_POST['sucAZT_PERSNR']:''),200,$FeldBreite,true,'','','','T','L','',(isset($_POST['txtAZT_PERSNR'])?$_POST['txtAZT_PERSNR']:''),6,'','','pattern = "[0-9]{6}"');
        $Funktionen->Form->ZeileEnde();
        $FilNr = "";
        if(isset($_POST['txtAZT_FILNR'])){
            $FilNr = $_POST['txtAZT_FILNR'];
        } else{
            $FilNr = $Funktionen->AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
        }
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_FILNR'], $LabelBreite);
        $Funktionen->Form->Erstelle_TextFeld('!AZT_FILNR', $FilNr, 200, $FeldBreite, true,'','','','T','L','','',4,"","",'pattern = "[0-9]{1,4}"');
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_VORNAME'], $LabelBreite);
        $Funktionen->Form->Erstelle_TextFeld('!AZT_VORNAME', (isset($_POST['txtAZT_VORNAME'])?$_POST['txtAZT_VORNAME']:''), 200, $FeldBreite, true);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_NAME'], $LabelBreite);
        $Funktionen->Form->Erstelle_TextFeld('!AZT_NAME', (isset($_POST['txtAZT_NAME'])?$_POST['txtAZT_NAME']:''), 200, $FeldBreite, true);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_TSHIRT_GROESSE'], $LabelBreite);
        $Funktionen->Form->Erstelle_SelectFeld('!AZT_TSHIRT_GROESSE', (isset($_POST['txtAZT_TSHIRT_GROESSE'])?$_POST['txtAZT_TSHIRT_GROESSE']:''), $FeldBreite, true, '', '~'.$Funktionen->AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', explode('|', $Funktionen->AWISSprachKonserven['AZT']['AZT_LST_TSHIRT_GROESSE']));
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_TEL_NR'], $LabelBreite);
        $Funktionen->Form->Erstelle_TextFeld('!AZT_TEL_NR', (isset($_POST['txtAZT_TEL_NR'])?$_POST['txtAZT_TEL_NR']:''), 200, $FeldBreite, true);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_EMAIL'], $LabelBreite);
        $Funktionen->Form->Erstelle_TextFeld('AZT_EMAIL', (isset($_POST['txtAZT_EMAIL'])?$_POST['txtAZT_EMAIL']:''), 200, $FeldBreite, true);
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_BESTAETIGUNG'], 4,'width: auto');
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->Formular_Ende();

        $Funktionen->Form->SchaltflaechenStart();
        $Funktionen->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        $Funktionen->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }else{
        $Funktionen->Form->ZeileStart();
        $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_ZEITRAUMVORBEI'], 4, 'width: auto');
        $Funktionen->Form->ZeileEnde();

        $Funktionen->Form->Formular_Ende();

        $Funktionen->Form->SchaltflaechenStart();
        $Funktionen->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    }
    $Funktionen->Form->SchaltflaechenEnde();


    $Funktionen->Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($Funktionen->Form instanceof awisFormular) {
        $Funktionen->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241613");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}