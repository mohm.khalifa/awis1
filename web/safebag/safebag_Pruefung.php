<?php
require_once('safebag_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SBI','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Wort', 'PDFErzeugen');
    $TextKonserven[]=array('Liste', 'FreigabeBemerkungen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht32000 = $AWISBenutzer->HatDasRecht(32000);
	if($Recht32000 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
     
    $SBA = new awis_abgleich_funktionen();
    
    $AnsichtSuche = false;
    $AnsichtAnsicht = false;
    $AnsichtSachb = false;
    $ErzeugePDF = false;
    $Freigabe = '';
    
    if (isset($_POST['cmdSuche_x']) or isset($_POST['cmdPdf_x']))
    {
        $AnsichtAnsicht = true;
        if (isset($_POST['cmdPdf_x'])) //and (count($AWIS_KEYs1) > 0))
        {
            $ErzeugePDF = true;
        }
    }
    else
    {
        if (isset($_REQUEST['txtANSICHT']))        // wegen Block
        {
			if ($_REQUEST['txtANSICHT'] == 'Ansicht')
			{
				$AnsichtAnsicht = true;
			}
			else 
			{
				$AnsichtSachb = true;
			}
        }        
        else
        {
            $AnsichtSuche = true;
        }
    }
    
    if (isset($_GET['Storno']))
    {
    	$SQL =  'select * from sbabgleich where sba_key = :var_N0_sba_key';
    	$DB->SetzeBindevariable('SBA', 'var_N0_sba_key', $_GET['Storno'], awisDatenbank::VAR_TYP_GANZEZAHL);
    	$rsStorno = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SBA'));
    
    	if (! $rsStorno->EOF())
    	{
    		$DB->SetzeBindevariable('SBI', 'var_N0_SBI_key', $rsStorno->FeldInhalt('SBA_SBI_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
    		$SQL =  'update sbgeldentsorger set ' .
    				'sbi_abgeglichen = 0 ' .
    				'where sbi_key = :var_N0_sbi_key';
    		$DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SBI'));
    
    		$DB->SetzeBindevariable('SBK', 'var_N0_sbk_key', $rsStorno->FeldInhalt('SBA_SBK_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
    		$SQL =  'update sbkassendaten set ' .
    				'sbk_abgeglichen = 0 ' .
    				'where sbk_key = :var_N0_sbk_key';
    		$DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SBK'));
    
    		$DB->SetzeBindevariable('SBA', 'var_N0_sba_key', $rsStorno->FeldInhalt('SBA_KEY'), awisDatenbank::VAR_TYP_GANZEZAHL);
    		$SQL =  'delete from sbabgleich where sba_key = :var_N0_sba_key';
    		$DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SBA'));
    	}
    }
    if (isset($_GET['Freigabe']))
    {
    	$Freigabe = $_GET['Freigabe'];
    }
    if (isset($_GET['FreigabeSave']))
    {
    	$Save = '';
    	if($_POST['txtABGLEICHKENN'.$_GET['FreigabeSave']] == 'B' and $_POST['txtFreigabeBem'] == '::bitte w�hlen::')
    	{
    		$Form->Hinweistext($AWISSprachKonserven['SBI']['KeineFreigabeBem'], 1);
    		$Save = false;
    		$Freigabe = $_GET['FreigabeSave'];
    		$AnsichtSachb = true;
    		$Anc = 'anc'.$_GET['FreigabeSave'];
    	}
    	else 
    	{
    		if(isset($_POST['txtFreigabeBem']) and $_POST['txtFreigabeBem'] != '::bitte w�hlen::')
    		{
    			$FreigabeBem =	$_POST['txtFreigabeBem'];
    		}
    		else
    		{
    			$FreigabeBem =	0;
    		}
    		$Save = true;
    	}
   			
    	if($Save)
    	{
	    	$DB->SetzeBindevariable('SBA', 'var_N0_sba_key',$_GET['FreigabeSave'], awisDatenbank::VAR_TYP_GANZEZAHL);
	    	$SQL =  'update sbabgleich set ' .
	    			'sba_freigabe = 1, ' .
	    			'sba_freigabebem ='.$FreigabeBem.
	    			',sba_user = \'' .$AWISBenutzer->BenutzerName(1).'\','.
	    			'sba_userdat = sysdate '.
	    			'where sba_key = :var_N0_sba_key';
	    	$DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SBA'));
	    	$Form->Hinweistext($AWISSprachKonserven['SBI']['DatensatzFrei'], 1);
    	}
    }
    
$Form->DebugAusgabe(1, 'AnsichtSuche: ' . $AnsichtSuche, 'AnsichtAnsicht: ' . $AnsichtAnsicht, 'ErzeugePDF: ' . $ErzeugePDF);
$Form->DebugAusgabe(1, $_REQUEST);

	/**********************************************
	* Benutzerparameter
	***********************************************/
    $SuchParam = array();
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_SBA')) < 8))
	{
        $SuchParam['SuchenInKey'] = 1;
        $SuchParam['Safebagnr'] = '';
        $SuchParam['AbgleichDatumVon'] = '';
        $SuchParam['AbgleichDatumBis'] = '';
        $SuchParam['AbgleichArtKey'] = 0;
        $SuchParam['FilId'] = '';
        $SuchParam['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_SBA", serialize($SuchParam));
	}    
    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_SBA'));
	if ($AnsichtAnsicht and !$ErzeugePDF and isset($_POST['cmdSuche_x']) and !isset($_GET['Storno']) and !isset($_GET['SBASort']) and !isset($_GET['FreigabeSave']))
	{
        $SuchParam['SuchenInKey'] = (isset($_POST['sucSBI_SUCHENINKEY'])? $_POST['sucSBI_SUCHENINKEY']: $SuchParam['SuchenInKey']);
        $SuchParam['Safebagnr'] = (isset($_POST['sucSBA_SAFEBAG'])? $_POST['sucSBA_SAFEBAG']: $SuchParam['Safebagnr']);
        $SuchParam['AbgleichDatumVon'] = (isset($_POST['sucSBA_ABGLEICHDATUMVON'])? $_POST['sucSBA_ABGLEICHDATUMVON']: $SuchParam['AbgleichDatumVon']);
        $SuchParam['AbgleichDatumBis'] = (isset($_POST['sucSBA_ABGLEICHDATUMBIS'])? $_POST['sucSBA_ABGLEICHDATUMBIS']: $SuchParam['AbgleichDatumBis']);
        $SuchParam['AbgleichArtKey'] = (isset($_POST['sucSBA_ABGLEICHARTKEY'])? $_POST['sucSBA_ABGLEICHARTKEY']: $SuchParam['AbgleichArtKey']);
        $SuchParam['FilId'] = (isset($_POST['sucSBA_FILNR'])? $_POST['sucSBA_FILNR']: $SuchParam['FilId']);
        $SuchParam['SPEICHERN'] = isset($_POST['sucSBA_AUSWAHLSPEICHERN']) ? 'on': 'off';
		$AWISBenutzer->ParameterSchreiben("Formular_SBA", serialize($SuchParam));
	}    

    if ($ErzeugePDF)
    {
        $_GET['XRE'] = '32';        // Berichtskey
        $_GET['ID'] = base64_encode($SBA->ErstellePDFBedingung($SuchParam));
        include('../berichte/drucken.php');
    }    
    if (isset($_GET['Freigabe']) or $Freigabe != '')
    {
    	if(!isset($_GET['Freigabe']))
    	{
    		$FreigabeLink = '&FreigabeSave='.$Freigabe;
    	}
    	else 
    	{
    		$FreigabeLink = '&FreigabeSave='.$_GET['Freigabe'];
    	}
    	
    	$FreigabeLink .= '#anc'.$Freigabe;
    }
    else
    {
    	$FreigabeLink = '';
    }
    
    
    $Form->SchreibeHTMLCode('<form name=frmAbgeglichen action=./safebag_Main.php?cmdAktion=Pruefung'.$FreigabeLink. (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();
$Form->DebugAusgabe(1, $SuchParam);

	    $SBA->Feld_SelectSBIoderSBK($SuchParam, $AnsichtSuche);
	    $Form->Trennzeile('O');
	    $SBA->Feld_Safebag($SuchParam, $AnsichtSuche, 3);
	    $SBA->Feld_SelectFilId($SuchParam, $AnsichtSuche, 3);
	    if(($Recht32000&8)>0)
	    {
	    	$SBA->Feld_AbgleichVonBis($SuchParam, $AnsichtSuche);
	    }
	    if(!($Recht32000&8)>0)
	    {
	    	$Sachbearbeiter = true;
	    }
	    else 
	    {
	    	$Sachbearbeiter = false;
	    }
	    $SBA->Feld_SelectAbgleichArt($SuchParam, $AnsichtSuche,$Sachbearbeiter);
	    $SBA->Feld_AuswahlSpeichern($SuchParam, $AnsichtSuche);
    if ($AnsichtAnsicht or $AnsichtSachb)
    {
        $Form->Trennzeile('L');
        
        $Sort= '';
        if(isset($_GET['SBASort']))
        {
        	$Sort = $_GET['SBASort'];
        }
        elseif(isset($_REQUEST['txtSBASort']))
        {
        	$Sort = $_REQUEST['txtSBASort'];
        }		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./safebag_Main.php?cmdAktion=Pruefung' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['SBI']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['SBI']['ZurueckSuche']);
		$Form->InfoZeile($Felder, '');
        
        $SQL =  'SELECT sba.*,sbi.*,sbk.*,sat.*, ' . 
                        'fil_sbi.fil_bez AS fil_sbi_bez, ' . 
                        'fil_sbk.fil_bez AS fil_sbk_bez, ' .
                        'row_number() over (';
        if ($Sort != '')
        {		// wenn GET-Sort, dann nach diesen Feld sortieren
            $SQL .= ' order by ' . str_replace('~',' DESC ',$Sort);
        }
        else if ($SuchParam['SuchenInKey'] == awis_abgleich_funktionen::_SuchenInSBI)							
        {		// sonst pauschal nach Abgleichdatum
            $SQL .= ' order by sbi_umsatztag asc';
        } 
        else 
        {
        	$SQL .= ' order by sbk_datum asc';
        }               
        $SQL .= ') as zeilennr ' .
                'from sbabgleich sba ' . 
                'left join sbgeldentsorger sbi on sbi.sbi_key = sba.sba_sbi_key ' . 
                'left join sbkassendaten sbk on sbk.sbk_key = sba.sba_sbk_key ' .
                'left join sbabgleichart sat on sat.sat_key = sba.sba_sat_key ' .
                'left join filialen fil_sbi on fil_sbi.fil_id = sbi.sbi_fil_id ' . 
                'left join filialen fil_sbk on fil_sbk.fil_id = sbk.sbk_fil_id ' . 
            $Bedingung = ' and sba_userdat >= (sysdate - 366)';         // nur Datens�tze der letzten 366 Tage anzeigen
            $Bedingung .= ' and sba_freigabe = 0';        // die als bezahlt markierten nicht mehr anzeigen
            $Bedingung .= $SBA->ErstelleTAGBedingung($SuchParam);
            if ($Bedingung != '')
            {
                $SQL .= ' WHERE ' . substr($Bedingung, 4);
            }
$Form->DebugAusgabe(1, $SQL);                    
        $MaxDS = 1;
        $ZeilenProSeite=1;
        $Block = 1;
        // Zum Bl�ttern in den Daten
        if (isset($_REQUEST['Block']))
        {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        }
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
$Form->DebugAusgabe(1, $SQL);
        $rsSBA = $DB->RecordSetOeffnen($SQL);
     
        // Spaltenbreiten f�r Listenansicht
        $FeldBreiten = array();
        $FeldBreitenO['ChkBox'] = 20;
        $FeldBreitenU['FilNr'] = 70;
        $FeldBreitenU['Datum'] = 180;
        $FeldBreitenU['Betrag'] = 150;
        $FeldBreitenO['AbgleichArt'] = 350;
        $FeldBreitenO['Bemerkung'] = 270;
        $FeldBreitenO['User'] = 100;
        $FeldBreitenO['UserDat'] = 177;
        $FeldBreitenO['Storno'] = 37;
        $FeldBreitenU['Dummy'] = 364;
        $FeldBreitenU['Safebagnr'] = 150;
        $Gesamtbreite = 0;
        foreach ($FeldBreitenO as $value)
        {
            $Gesamtbreite += $value + 4;
        }
        foreach ($FeldBreitenU as $value)
        {
        	$Gesamtbreite += $value + 4;
        }
   

        $Form->ZeileStart();
        // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
        $Form->Erstelle_Liste_Ueberschrift('', $FeldBreitenO['ChkBox']-4);
        // �berschrift der Listenansicht mit Sortierungslink: FilNr
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort='.($SuchParam['SuchenInKey'] == '1' ? 'SBI_FIL_ID' : 'SBK_FIL_ID').((isset($_GET['SBASort']) AND (($_GET['SBASort']=='SBI_FIL_ID') OR ($_GET['SBASort']=='SBK_FIL_ID')))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_FIL_ID'], $FeldBreitenU['FilNr'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Pr�fdatum
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort='.($SuchParam['SuchenInKey'] == '1' ? 'SBI_UMSATZTAG' : 'SBK_DATUM').((isset($_GET['SBASort']) AND (($_GET['SBASort']=='SBI_UMSATZTAG') OR ($_GET['SBASort']=='SBK_DATUM')))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_DATUM'], $FeldBreitenU['Datum'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
        // �berschrift der Listenansicht mit Sortierungslink: Name
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort='.($SuchParam['SuchenInKey'] == '1' ? 'SBI_SAFEBAGNR' : 'SBK_SAFEBAGNR').((isset($_GET['SBASort']) AND (($_GET['SBASort']=='SBI_SAFEBAGNR') OR ($_GET['SBASort']=='SBK_SAFEBAGNR')))?'~':'')  . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_SAFEBAGNR'], $FeldBreitenU['Safebagnr'], '', $Link);
        // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort='.($SuchParam['SuchenInKey'] == '1' ? 'SBI_BETRAG' : 'SBK_BETRAG').((isset($_GET['SBASort']) AND (($_GET['SBASort']=='SBI_BETRAG') OR ($_GET['SBASort']=='SBK_BETRAG')))?'~':'')  . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_BETRAG'], $FeldBreitenU['Betrag'], '', $Link);      
        //$Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&TAGSort=TGD_PREIS'.((isset($_GET['TAGSort']) AND ($_GET['TAGSort']=='TGD_PREIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift('', $FeldBreitenU['Dummy'], '');
        $Form->ZeileEnde();
        $Form->ZeileStart();
        // �berschrift der Listenansicht mit Sortierungslink: Abgleichart
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort=SAT_KENNUNG'.((isset($_GET['SBASort']) AND ($_GET['SBASort']=='SAT_KENNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['AbgleichArt'], $FeldBreitenO['AbgleichArt'], '', $Link);
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort=SBA_BEMERKUNG'.((isset($_GET['SBASort']) AND ($_GET['SBASort']=='SBA_BEMERKUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['Bemerkung'], $FeldBreitenO['Bemerkung'], '', $Link);                
        // �berschrift der Listenansicht mit Sortierungslink: User
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort=SBA_USER'.((isset($_GET['SBASort']) AND ($_GET['SBASort']=='SBA_USER'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['User'], $FeldBreitenO['User'], '', $Link);                
        // �berschrift der Listenansicht mit Sortierungslink: Userdat
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&SBASort=SBA_USERDAT'.((isset($_GET['SBASort']) AND ($_GET['SBASort']=='SBA_USERDAT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['UserDat'], $FeldBreitenO['UserDat'], '', $Link);                        
        // �berschrift der Listenansicht mit Sortierungslink: Storno
        $Link = '';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['Aktionen'], $FeldBreitenO['Storno'], 'font-weight:bolder;', $Link, $AWISSprachKonserven['SBI']['TTTAktionen'], 'C');
        $Form->ZeileEnde();
        
        $DS = 0;	// f�r Hintergrundfarbumschaltung
        if($rsSBA->AnzahlDatensaetze() > 4)
        {
        	$Form->SchreibeHTMLCode('<div id=scrKasse style="width: 100%; height: 250px;  overflow-y: scroll;">');
        }
        while(! $rsSBA->EOF())
        {        
            $Form->ZeileStart();
            $TTT = $AWISSprachKonserven['SBI']['Geldentsorger'];
            $Form->SchreibeHTMLCode('<a name="anc'.$rsSBA->FeldInhalt('SBA_KEY').'"></a>');
            $Form->Erstelle_ListenFeld('dummy', 'S:', 0, $FeldBreitenO['ChkBox']-4, false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Geldentsorger'] . ': ' . $rsSBA->FeldInhalt('SBI_FIL_ID') . ' - ' . $rsSBA->FeldInhalt('FIL_SBI_BEZ');
            $Form->Erstelle_ListenFeld('SBI_FIL_ID', $rsSBA->FeldInhalt('SBI_FIL_ID'), 0, $FeldBreitenU['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Geldentsorger'] . ': ' . $Form->Format('D', $rsSBA->FeldInhalt('SBI_UMSATZTAG'));
            $Form->Erstelle_ListenFeld('SBI_UMSATZTAG', $Form->Format('D', $rsSBA->FeldInhalt('SBI_UMSATZTAG')), 0, $FeldBreitenU['Datum'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Geldentsorger'] . ': ' . $rsSBA->FeldInhalt('SBI_SAFEBAGNR');
            $Form->Erstelle_ListenFeld('SBI_SAFEBAGNR', $rsSBA->FeldInhalt('SBI_SAFEBAGNR'), 0, $FeldBreitenU['Safebagnr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Geldentsorger'] . ': ' . $rsSBA->FeldInhalt('SBI_BETRAG');
            $Form->Erstelle_ListenFeld('SBI_BETRAG', $rsSBA->FeldInhalt('SBI_BETRAG'), 0, $FeldBreitenU['Betrag'], false, ($DS%2), '', $Link, 'T', 'R', $TTT);
            //$TTT = $AWISSprachKonserven['SBI']['Geldentsorger'] . ': ' . $rsSBA->FeldInhalt('SBI_BETRAG');
            $Form->Erstelle_ListenFeld('SBI_DUMMY','', 0, $FeldBreitenU['Dummy'], false, ($DS%2), '','', 'T', 'R', '');
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $TTT = $AWISSprachKonserven['SBI']['Kasse'];
            $Form->Erstelle_ListenFeld('TAG_DUMMY', 'K:', 0, $FeldBreitenO['ChkBox']-4, false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Kasse'] . ': ' . $rsSBA->FeldInhalt('SBK_FIL_ID') . ' - ' . $rsSBA->FeldInhalt('FIL_SBK_BEZ');
            $Form->Erstelle_ListenFeld('SBK_FIL_ID', $rsSBA->FeldInhalt('SBK_FIL_ID'), 0, $FeldBreitenU['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Kasse'] . ': ' . $Form->Format('DU', $rsSBA->FeldInhalt('SBK_DATUM'));
            $Form->Erstelle_ListenFeld('SBK_DATUM', $Form->Format('D', $rsSBA->FeldInhalt('SBK_DATUM')), 0, $FeldBreitenU['Datum'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Kasse'] . ': ' . $rsSBA->FeldInhalt('SBK_SAFEBAGNR');
            $Form->Erstelle_ListenFeld('SBK_SAFEBAGNR', $rsSBA->FeldInhalt('SBK_SAFEBAGNR'), 0, $FeldBreitenU['Safebagnr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Kasse'] . ': ' . $rsSBA->FeldInhalt('SBK_BETRAG');
            $Form->Erstelle_ListenFeld('SBK_BETRAG', $rsSBA->FeldInhalt('SBK_BETRAG'), 0, $FeldBreitenU['Betrag'], false, ($DS%2), '', $Link, 'T', 'R', $TTT);
            //$Form->Erstelle_ListenFeld('SBK_DUMMY','', 0, $FeldBreitenU['Dummy'], false, ($DS%2), '','', 'T', 'R', '');
            $Form->Erstelle_ListenFeld('SBI_DUMMY','', 0, $FeldBreitenU['Dummy']-39, false, ($DS%2), '','', 'T', 'R', '');
            $TTT = $AWISSprachKonserven['SBI']['TTTFreigabeTTT'];
            $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Sachb&Freigabe='.$rsSBA->FeldInhalt('SBA_KEY').'#anc'.$rsSBA->FeldInhalt('SBA_KEY').(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_ListenFeld('SBA_FREIGABE', $AWISSprachKonserven['SBI']['Freigabe'], 0, $FeldBreitenO['Storno']-2, false, ($DS%2), '', $Link, 'T', 'C', $TTT);
            $Form->ZeileEnde();
            
            $LInk = '';
            $Form->ZeileStart();
            //$Form->Erstelle_Checkbox('TAG_CHK_' . $rsTAG->FeldInhalt('TAG_KEY'), '', $FeldBreitenO['ChkBox'], true, $rsTAG->FeldInhalt('TAG_KEY'));
            //$TTT = $AWISSprachKonserven['TAG']['Abgleich'];
            //$Form->Erstelle_ListenFeld('TAG_DUMMY', 'A:', 0, $FeldBreitenO['ChkBox']-4, false, ($DS%2), '', $Link, 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['AbgleichArt'] . ': ' . $rsSBA->FeldInhalt('SAT_KENNUNG') . ' - ' . $rsSBA->FeldInhalt('SAT_BESCHREIBUNG');
            $Form->Erstelle_ListenFeld('SBA_ABGLEICHART', $rsSBA->FeldInhalt('SAT_BESCHREIBUNG'), 0, $FeldBreitenO['AbgleichArt'], false, ($DS%2), '','', 'T', 'L', $TTT);
            $Form->Erstelle_HiddenFeld('ABGLEICHKENN'.$rsSBA->FeldInhalt('SBA_KEY'), $rsSBA->FeldInhalt('SAT_KENNUNG'));
            $TTT = $AWISSprachKonserven['SBI']['Abgleich'] . ': ' . $rsSBA->FeldInhalt('SAT_BESCHREIBUNG');
            $Form->Erstelle_ListenFeld('SBA_BEMERKUNG', $rsSBA->FeldInhalt('SBA_BEMERKUNG'), 0, $FeldBreitenO['Bemerkung'], false, ($DS%2), '','', 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Abgleich'] . ': ' . $rsSBA->FeldInhalt('SBA_USER') . ' (' . $Form->Format('DU', $rsSBA->FeldInhalt('SBA_USERDAT')) . ')';
            $Form->Erstelle_ListenFeld('SBA_USER', $rsSBA->FeldInhalt('SBA_USER'), 0, $FeldBreitenO['User'], false, ($DS%2), '','', 'T', 'L', $TTT);
            $TTT = $AWISSprachKonserven['SBI']['Abgleich'] . ': ' . $Form->Format('DU', $rsSBA->FeldInhalt('SBA_USERDAT')) . ' (' . $rsSBA->FeldInhalt('SBA_USER') . ')';
            $Form->Erstelle_ListenFeld('SBA_USERDAT', $rsSBA->FeldInhalt('SBA_USERDAT'), 0, $FeldBreitenO['UserDat'], false, ($DS%2), '','', 'T', 'L', $TTT);
            if(($Recht32000&8)>0)
            {
            	$TTT = $AWISSprachKonserven['SBI']['TTTStornoTTT'];
            	$Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht&Storno='.$rsSBA->FeldInhalt('SBA_KEY').(isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            	$Form->Erstelle_ListenFeld('SBA_FREIGABE', $AWISSprachKonserven['SBI']['Storno'], 0, $FeldBreitenO['Storno'], false, ($DS%2), '', $Link, 'T', 'C', $TTT);
            }
            else 
            {
            	$Form->Erstelle_ListenFeld('SBI_DUMMY','', 0, $FeldBreitenU['Dummy']-331, false, ($DS%2), '','', 'T', 'R', '');
            }
            $Link = '';
            $Form->ZeileEnde();
            if($AnsichtSachb AND $Freigabe == $rsSBA->FeldInhalt('SBA_KEY'))
            {
            	if($DS%2 == 0)
            	{
            		$Background = 'background-color:#D0D0D0;';
            	}
            	else
            	{
            		$Background = 'background-color:#F0F0F0;';
            	}
   
   
            
            	$Form->ZeileStart('border-top-width:thin;border-top-style:solid;width:930px;'.$Background);
            	$Form->Erstelle_ListenFeld('Filler','', 10,878,'',($DS%2));
            	$Form->Schaltflaeche('image', 'cmd_speichern', '', '/bilder/icon_save.png',$AWISSprachKonserven['SBI']['FreigabeAbschluss'],'','',18,18);
            	$Form->Schaltflaeche('href', 'cmd_back','./safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Ansicht#anc'.$rsSBA->FeldInhalt('SBA_KEY'),'/bilder/icon_cancel.png',$AWISSprachKonserven['SBI']['FreigabeAbbrechen'],'','',18,18);
            	$Form->ZeileEnde();
            	       	
            	
            	$Form->ZeileStart();
            	$Form->Erstelle_ListenFeld('Freigabe',$AWISSprachKonserven['SBI']['FreigabeBemerkung'].':', 10,180,'',($DS%2),'height:23px');
            	//$Form->Erstelle_SelectFeld('Select','1',10,true,'','Testdatensatz f�r Selectfeld', '', '',$Background,'','','',array(),"font-family:'Courier New',sans-serif");
            	//$Form->Erstelle_SelectFeld('Select','1',10,true,'',$AWISSprachKonserven['Liste']['FreigabeBemerkungen'],'','',$Background);
            	$Daten = explode('|',$AWISSprachKonserven['Liste']['FreigabeBemerkungen']);
            	$Form->Erstelle_SelectFeld('FreigabeBem','0','366',true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','',$Background,$Daten);
           
            	$Form->Erstelle_ListenFeld('Filler','', 10,384,'',($DS%2),'height:23px');
            	$Form->ZeileEnde();
            }
            
            
            
           
            
            
            
            $DS++;
            $rsSBA->DSWeiter();                
        }
        if($rsSBA->AnzahlDatensaetze() > 4)
        {
        	$Form->SchreibeHTMLCode('</div>') ;
        }
        $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $Form->ZeileStart();
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['SBI']['SummeGesamtDS'].':', $Gesamtbreite-1215, 'font-weight:bolder;');
        $Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsSBA->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt+ $DS).' / '.$Form->Format('N0',$MaxDS), 250, 'font-weight:bolder;','','','R');
      
        $Form->ZeileEnde();   
           
        $Link = './safebag_Main.php?cmdAktion=Pruefung&txtANSICHT=Auswahl'.'&TAGSort='.$Sort;;        // f�r N�chste/Vorherige Seite schalten (href im Framwork) (=> mit txt damit immer die gleiche REQUEST-Var ankommt, wegen HiddenFeld)
        $Form->Erstelle_HiddenFeld('ANSICHT', 'Ansicht');                       // f�r direkte Auswahl der Seite
        $Form->Erstelle_HiddenFeld('TAGSort', $Sort);    
    }
    
	$Form->Formular_Ende();
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht32000&1) == 1)
    and ($AnsichtSuche))
	{	
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}
    
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>