<?php

class AuftraegeProTag
{

    /**
     * @var string $KundenNr
     */
    protected $KundenNr = null;

    /**
     * @var string $Kennwort
     */
    protected $Kennwort = null;

    /**
     * @var \DateTime $Datum
     */
    protected $Datum = null;

    /**
     * @param string $KundenNr
     * @param string $Kennwort
     * @param \DateTime $Datum
     */
    public function __construct($KundenNr, $Kennwort, \DateTime $Datum)
    {
      $this->KundenNr = $KundenNr;
      $this->Kennwort = $Kennwort;
      $this->Datum = $Datum->format(\DateTime::ATOM);
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
      return $this->KundenNr;
    }

    /**
     * @param string $KundenNr
     * @return AuftraegeProTag
     */
    public function setKundenNr($KundenNr)
    {
      $this->KundenNr = $KundenNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getKennwort()
    {
      return $this->Kennwort;
    }

    /**
     * @param string $Kennwort
     * @return AuftraegeProTag
     */
    public function setKennwort($Kennwort)
    {
      $this->Kennwort = $Kennwort;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatum()
    {
      if ($this->Datum == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Datum);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Datum
     * @return AuftraegeProTag
     */
    public function setDatum(\DateTime $Datum)
    {
      $this->Datum = $Datum->format(\DateTime::ATOM);
      return $this;
    }

}
