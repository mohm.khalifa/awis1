<?php
require_once('db.inc.php');
require_once('register.inc.php');
require_once('awisAusdruck.php');
require_once('awis_forms.inc.php');
require_once('awisFormular.inc');

global $awisRSZeilen;

//ini_set('max_execution_time','9000');

//$AWISBenutzer = new awisUser();
//print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

//$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');
$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');
//$Vorlagen = array();

$AWISSprache = awis_BenutzerParameter($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName());

$SQL= "SELECT SCHAEDEN_NEW.BEARBEITUNGSNR AS BearbNr, SCHAEDEN_NEW.BEARBEITUNGSNRFIL AS BearbNrFiliale, ";
$SQL.= " SCHAEDEN_NEW.EINGABEAM AS Eingabe_am, SCHAEDEN_NEW.KENNUNG AS Kennung, KENNUNGEN.KENNUNG_BEZEICHNUNG AS Kennung_Bezeichnung, ";
$SQL.= " SCHAEDEN_NEW.FEHLERBESCHREIBUNG AS Fehlerbeschreibung, SCHAEDEN_NEW.DIAGNOSE_WERKSTATTLEITER AS DiagnoseWL, ";
$SQL.= " SCHAEDEN_NEW.DIAGNOSE_TKDL AS BesprMassnahme, ";
//--Anwaltsache.Anwaltsache, ";
$SQL.= " SCHAEDEN_NEW.FILNR AS BearbFil, ";
//$SQL.= "--[Filialliste_GBL+VKL_20080208_1].FIL_BEZ AS [Bearb-Fil_Name], ";
$SQL.= " SCHAEDEN_NEW.VERURS_FILIALE AS VUFil, VFIL.FILIALNAME AS FILIALBEZ, ";
//--[Filialliste_GBL+VKL_20080208].FIL_BEZ AS [VU-Fil_Name], 
$SQL.= " Sum(ZAHLUNGEN.BETRAG) AS Fallkosten, STAND.STANDBEM AS BearbStatus, AUFTRAGSARTEN.AUFTRAGSART_ATU AS urspr_Auftrag, ";
$SQL.= " SCHADENSGRUND.GRUND AS woran_gearbeitet, BESCHAEDIGT.SCHLAGWORT AS was_beschaedigt, AUSFALLURSACHE.WERT AS Ausfallursache, ";
$SQL.= " SCHAEDEN_NEW.KFZ_KENNZ AS Kfz_Kennz, SCHAEDEN_NEW.KUNDENNAME AS KDName, ANTRAGART.WERT as ANTRAGART, ";
$SQL.= " SCHAEDEN_NEW.ATUNR AS ATUNr";
$SQL.= " FROM SCHAEDEN_NEW ";
$SQL.= " LEFT JOIN AUSFALLURSACHE ON SCHAEDEN_NEW.AUSFALLURSACHE = AUSFALLURSACHE.ID ";
//--LEFT JOIN Anwaltsache ON SCHAEDEN_NEW.ANWALTSACHE = Anwaltsache.Rechtstreit_ID 
//--LEFT JOIN [Filialliste_GBL+VKL_20080208] AS [Filialliste_GBL+VKL_20080208_1] ON SCHAEDEN_NEW.FILNR = [Filialliste_GBL+VKL_20080208_1].FIL_ID) 
//--LEFT JOIN [Filialliste_GBL+VKL_20080208] ON SCHAEDEN_NEW.VERURS_FILIALE = [Filialliste_GBL+VKL_20080208].FIL_ID) 
$SQL.= " LEFT JOIN ZAHLUNGEN ON SCHAEDEN_NEW.BEARBEITUNGSNR = ZAHLUNGEN.BEARBEITUNGSNR ";
$SQL.= " LEFT JOIN STAND ON SCHAEDEN_NEW.STAND = STAND.STANDID";
$SQL.= " LEFT JOIN AUFTRAGSARTEN ON SCHAEDEN_NEW.AUFTRAGSART_ATU_NEU = AUFTRAGSARTEN.ART_ID ";
$SQL.= " LEFT JOIN SCHADENSGRUND ON SCHAEDEN_NEW.SCHADENSGRUND = SCHADENSGRUND.ID ";
$SQL.= " LEFT JOIN BESCHAEDIGT ON SCHAEDEN_NEW.BID = BESCHAEDIGT.BID ";
$SQL.= " LEFT JOIN KENNUNGEN ON SCHAEDEN_NEW.KENNUNG = KENNUNGEN.KENNUNG ";
$SQL.= " LEFT JOIN ANTRAGART ON SCHAEDEN_NEW.ANTRAGART = ANTRAGART.ID";
$SQL.= " LEFT JOIN VIEW_FILIALEN VFIL ON VFIL.FILIALNR = SCHAEDEN_NEW.VERURS_FILIALE";

$SQL.= " WHERE SCHAEDEN_NEW.KENNUNG Not Like 'V%' ";
$SQL.= " AND SCHAEDEN_NEW.KENNUNG <> 'Z-STORNO' AND SCHAEDEN_NEW.KENNUNG <> 'Q-W-H-LUK' AND SCHAEDEN_NEW.KENNUNG <> 'FEHLT' ";
	      	
if($_POST['txtDatumVom']=='') //txtDatumVom
{
	$Datum = '01.01.2008';
}
else if (awis_PruefeDatum($_POST['txtDatumVom'],0,1) < awis_PruefeDatum('01.01.2008',0,1))
{
	$Datum = '01.01.2008';
}
else 
{
	$Datum = $_POST['txtDatumVom'];
}
	
$SQL .= " AND SCHAEDEN_NEW.EINGABEAM >= TO_DATE('" . $Datum . "','DD.MM.RRRR')";

if($_POST['txtDatumBis']=='') //txtDatumVom
{
	$Datumbis = date('d.m.Y');//'01.01.2008';
}

else//if($_POST['txtDatumBis']!='')		//txtDatumBis
{
	$Datumbis = $_POST['txtDatumBis'];	
}	
$SQL .= " AND SCHAEDEN_NEW.EINGABEAM <= TO_DATE('" . $Datumbis . "','DD.MM.RRRR')";

if($_POST['txtFIL_ID']!='')		//txtFIL_ID
{
	$SQL .= " AND SCHAEDEN_NEW.VERURS_FILIALE = ". $_POST['txtFIL_ID'] . "";		
}					
	
$SQL.= " GROUP BY SCHAEDEN_NEW.BEARBEITUNGSNR, SCHAEDEN_NEW.BEARBEITUNGSNRFIL, SCHAEDEN_NEW.EINGABEAM, SCHAEDEN_NEW.KENNUNG, ";
$SQL.= " KENNUNGEN.KENNUNG_BEZEICHNUNG, SCHAEDEN_NEW.FEHLERBESCHREIBUNG, SCHAEDEN_NEW.DIAGNOSE_WERKSTATTLEITER, SCHAEDEN_NEW.DIAGNOSE_TKDL, ";
//--Anwaltsache.Anwaltsache, 
$SQL.= " SCHAEDEN_NEW.FILNR, ";
//--[Filialliste_GBL+VKL_20080208_1].FIL_BEZ, 
$SQL.= " SCHAEDEN_NEW.VERURS_FILIALE, VFIL.FILIALNAME, ";
//--[Filialliste_GBL+VKL_20080208].FIL_BEZ, 
$SQL.= " STAND.STANDBEM, AUFTRAGSARTEN.AUFTRAGSART_ATU, SCHADENSGRUND.GRUND, BESCHAEDIGT.SCHLAGWORT, ";
$SQL.= " AUSFALLURSACHE.WERT, SCHAEDEN_NEW.KFZ_KENNZ, SCHAEDEN_NEW.KUNDENNAME, ANTRAGART.WERT, ";
$SQL.= " SCHAEDEN_NEW.ATUNR";
//$SQL.= "HAVING SCHAEDEN_NEW.EINGABEAM>='01.01.2008' AND SCHAEDEN_NEW.KENNUNG Not Like 'V%' And SCHAEDEN_NEW.KENNUNG <> 'Z-STORNO' And ";
//$SQL.= "SCHAEDEN_NEW.KENNUNG <> 'Q-W-H-LUK'";
$SQL.= " ORDER BY VUFIL, SCHAEDEN_NEW.KENNUNG";

awis_Debug(1,$SQL);
$Form = new awisFormular();
$Form->DebugAusgabe(1, $SQL);

$rsSchad = awisOpenRecordset($conSchad, $SQL );
$rsSchadZeilen = $awisRSZeilen;

if ($rsSchadZeilen == 0)
{
	die("<center><span class=HinweisText>Es wurden keine Daten gefunden!</span></center>");		
}
		
$Spalte = 10;
$Zeile = 20;
$Ausdruck = null;

$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Schaden-Liste-Detail');

	$Ausdruck->NeueSeite(0,1);	
	$Ausdruck->_pdf->SetFillColor(210,210,210);

	$Zeile = 20;

	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);

	// �berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',14);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(180,6,'Beschwerdereport Filiale '.$rsSchad['FILIALBEZ']['0'],0,0,'L',0);
	
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile+6);
	$Ausdruck->_pdf->Cell(50,6,'Zeitraum: '. $Datum .' bis '. $Datumbis .'. Erstellt am: '. date('d.m.Y') ,0,0,'L',0);
	$Zeile+=15;
	
	$Ausdruck->_pdf->SetFont('Arial','B',7);
	
	//$Zeile+=10;
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(10,8,'VU-FIL',1,0,'C',1);	

	$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);	
	$Ausdruck->_pdf->Cell(60,8,'KENNUNG',1,0,'C',1);
	
	$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile);
	$Ausdruck->_pdf->Cell(15,4,'BEARB-NR',0,0,'C',1);
	$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile+4);	
	$Ausdruck->_pdf->Cell(15,4,'AWIS',0,0,'C',1);	
	$Ausdruck->_pdf->Line($Spalte+85,$Zeile,$Spalte+85,$Zeile+4);
	$Ausdruck->_pdf->Line($Spalte+70,$Zeile,$Spalte+85,$Zeile);
	$Ausdruck->_pdf->Line($Spalte+70,$Zeile,$Spalte+70,$Zeile+8);
	
	$Ausdruck->_pdf->SetXY($Spalte+85,$Zeile);
	$Ausdruck->_pdf->Cell(35,8,'ANTRAGART',1,0,'C',1);
	
	$Ausdruck->_pdf->SetXY($Spalte+120,$Zeile);
	$Ausdruck->_pdf->Cell(15,8,'BEARB-FIL',1,0,'C',1);
	
	$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
	$Ausdruck->_pdf->Cell(20,8,'FALLKOSTEN',1,0,'C',1);	
	
	$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile);
	$Ausdruck->_pdf->Cell(23,4,'URSPR.',0,0,'C',1);
	$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile+4);	
	$Ausdruck->_pdf->Cell(23,4,'AUFTRAG',0,0,'C',1);	
	$Ausdruck->_pdf->Line($Spalte+178,$Zeile,$Spalte+178,$Zeile+4);
	$Ausdruck->_pdf->Line($Spalte+155,$Zeile,$Spalte+178,$Zeile);
	$Ausdruck->_pdf->Line($Spalte+155,$Zeile,$Spalte+155,$Zeile+8);
	
	$Ausdruck->_pdf->SetXY($Spalte+178,$Zeile);
	$Ausdruck->_pdf->Cell(15,8,'ATUNR',1,0,'C',1);
	
	$Ausdruck->_pdf->SetXY($Spalte+193,$Zeile);
	$Ausdruck->_pdf->Cell(23,4,'WORAN',0,0,'C',1);
	$Ausdruck->_pdf->SetXY($Spalte+193,$Zeile+4);	
	$Ausdruck->_pdf->Cell(23,4,'GEARBEITET',0,0,'C',1);	
	$Ausdruck->_pdf->Line($Spalte+216,$Zeile,$Spalte+216,$Zeile+4);
	$Ausdruck->_pdf->Line($Spalte+193,$Zeile,$Spalte+216,$Zeile);
	$Ausdruck->_pdf->Line($Spalte+193,$Zeile,$Spalte+193,$Zeile+8);
	
	$Ausdruck->_pdf->SetXY($Spalte+216,$Zeile);
	$Ausdruck->_pdf->Cell(23,4,'WAS',0,0,'C',1);
	$Ausdruck->_pdf->SetXY($Spalte+216,$Zeile+4);	
	$Ausdruck->_pdf->Cell(23,4,'BESCH�DIGT',0,0,'C',1);	
	$Ausdruck->_pdf->Line($Spalte+239,$Zeile,$Spalte+239,$Zeile+4);
	$Ausdruck->_pdf->Line($Spalte+216,$Zeile,$Spalte+239,$Zeile);
	$Ausdruck->_pdf->Line($Spalte+216,$Zeile,$Spalte+216,$Zeile+8);
	
	$Ausdruck->_pdf->SetXY($Spalte+239,$Zeile);
	$Ausdruck->_pdf->Cell(25,8,'AUSFALLURSACHE',1,0,'C',1);
	
	$Ausdruck->_pdf->SetXY($Spalte+264,$Zeile);
	$Ausdruck->_pdf->Cell(18,8,'KFZ-KENNZ',1,0,'C',1);

	$Summe_Anzahl = 0;
	$Summe_Gesamt = 0;
	
	$Summe_Gesamt_Fil = 0;
	$Summe_Anzahl_Fil = 0;
	
	$Seite = 1;
	
	$Zeile=$Zeile+4;
	
	for($SchadZeile=0;$SchadZeile<$rsSchadZeilen;$SchadZeile++)
	{		
		$Kennung = true; 
		$VUFIL = true; 
		
		if ($SchadZeile > 0)
		{
			if ($rsSchad['KENNUNG_BEZEICHNUNG'][$SchadZeile]!=$rsSchad['KENNUNG_BEZEICHNUNG'][$SchadZeile-1])
			{
				$Zeile = $Zeile+4;				
				
				$Ausdruck->_pdf->SetFont('Arial','B',7);
				
				$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile);
				$Ausdruck->_pdf->Cell(15,4,$Summe_Anzahl,1,0,'R',0);						
				$Summe_Anzahl = 0;
				
				$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
				$Ausdruck->_pdf->Cell(20,4,number_format($Summe_Gesamt,2,',','.').' �',1,0,'R',0);	
				$Summe_Gesamt = 0;
				
				$Ausdruck->_pdf->Line($Spalte+10,$Zeile+4,$Spalte+282,$Zeile+4);											
				$Ausdruck->_pdf->Line($Spalte+282,$Zeile,$Spalte+282,$Zeile+4);											
				$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+4);											
				$Ausdruck->_pdf->Line($Spalte+10,$Zeile,$Spalte+10,$Zeile+4);											
			}
			else 
			{
				$Kennung = false;
			}
			
			if ($rsSchad['VUFIL'][$SchadZeile]!=$rsSchad['VUFIL'][$SchadZeile-1])				
			{
				$Zeile = $Zeile+4;				
				
				$Ausdruck->_pdf->SetFont('Arial','B',7);
				
				$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
				$Ausdruck->_pdf->Cell(70,4,'Gesamt',1,0,'L',0);
				
				$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile);
				$Ausdruck->_pdf->Cell(15,4,$Summe_Anzahl_Fil,1,0,'R',0);		
				$Summe_Anzahl_Fil = 0;
				
				$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
				$Ausdruck->_pdf->Cell(20,4,number_format($Summe_Gesamt_Fil,2,',','.').' �',1,0,'R',0);	
				$Summe_Gesamt_Fil = 0;
				
				$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+282,$Zeile);											
				$Ausdruck->_pdf->Line($Spalte+282,$Zeile,$Spalte+282,$Zeile+4);											
			}
			else 
			{
				$VUFIL = false;
			}			
		}
		
		
		if ($Zeile > 180)
		{		
			// Fu�zeile
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
			$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
			$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
			$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,'Dieser Audruck ist Eigentum der ATU Auto-Teile-Unger. Sie darf nicht an Dritte weiter gegeben werden.',0,0,'C',0);
			$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-35),$Ausdruck->SeitenHoehe()-10);
			$Ausdruck->_pdf->Cell(30,8,'Erstellt: '.$AWISBenutzer->BenutzerName(). '  '.date('d.m.Y'),0,0,'R',0);					
			
			
			$Zeile=16;
			$Zeile=$Zeile+15;
			$Seite = $Seite + 1;						
			
			$Ausdruck->NeueSeite(0,1);
			
			$Ausdruck->_pdf->SetFont('Arial','B',7);
					
			$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
			$Ausdruck->_pdf->Cell(10,8,'VU-FIL',1,0,'C',1);
		
			$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);	
			$Ausdruck->_pdf->Cell(60,8,'KENNUNG',1,0,'C',1);
						
			$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile);
			$Ausdruck->_pdf->Cell(15,4,'BEARB-NR',0,0,'C',1);
			$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile+4);	
			$Ausdruck->_pdf->Cell(15,4,'AWIS',0,0,'C',1);	
			$Ausdruck->_pdf->Line($Spalte+85,$Zeile,$Spalte+85,$Zeile+4);
			$Ausdruck->_pdf->Line($Spalte+70,$Zeile,$Spalte+85,$Zeile);
			$Ausdruck->_pdf->Line($Spalte+70,$Zeile,$Spalte+70,$Zeile+8);
			
			$Ausdruck->_pdf->SetXY($Spalte+85,$Zeile);
			$Ausdruck->_pdf->Cell(35,8,'ANTRAGART',1,0,'C',1);
			
			$Ausdruck->_pdf->SetXY($Spalte+120,$Zeile);
			$Ausdruck->_pdf->Cell(15,8,'BEARB-FIL',1,0,'C',1);
			
			$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
			$Ausdruck->_pdf->Cell(20,8,'FALLKOSTEN',1,0,'C',1);			
			
			$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile);
			$Ausdruck->_pdf->Cell(23,4,'URSPR.',0,0,'C',1);
			$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile+4);	
			$Ausdruck->_pdf->Cell(23,4,'AUFTRAG',0,0,'C',1);	
			$Ausdruck->_pdf->Line($Spalte+178,$Zeile,$Spalte+178,$Zeile+4);
			$Ausdruck->_pdf->Line($Spalte+155,$Zeile,$Spalte+178,$Zeile);
			$Ausdruck->_pdf->Line($Spalte+155,$Zeile,$Spalte+155,$Zeile+8);
			
			$Ausdruck->_pdf->SetXY($Spalte+178,$Zeile);
			$Ausdruck->_pdf->Cell(15,8,'ATUNR',1,0,'C',1);
			
			$Ausdruck->_pdf->SetXY($Spalte+193,$Zeile);
			$Ausdruck->_pdf->Cell(23,4,'WORAN',0,0,'C',1);
			$Ausdruck->_pdf->SetXY($Spalte+193,$Zeile+4);	
			$Ausdruck->_pdf->Cell(23,4,'GEARBEITET',0,0,'C',1);	
			$Ausdruck->_pdf->Line($Spalte+216,$Zeile,$Spalte+216,$Zeile+4);
			$Ausdruck->_pdf->Line($Spalte+193,$Zeile,$Spalte+216,$Zeile);
			$Ausdruck->_pdf->Line($Spalte+193,$Zeile,$Spalte+193,$Zeile+8);
			
			$Ausdruck->_pdf->SetXY($Spalte+216,$Zeile);
			$Ausdruck->_pdf->Cell(23,4,'WAS',0,0,'C',1);
			$Ausdruck->_pdf->SetXY($Spalte+216,$Zeile+4);	
			$Ausdruck->_pdf->Cell(23,4,'BESCH�DIGT',0,0,'C',1);	
			$Ausdruck->_pdf->Line($Spalte+239,$Zeile,$Spalte+239,$Zeile+4);
			$Ausdruck->_pdf->Line($Spalte+216,$Zeile,$Spalte+239,$Zeile);
			$Ausdruck->_pdf->Line($Spalte+216,$Zeile,$Spalte+216,$Zeile+8);
			
			$Ausdruck->_pdf->SetXY($Spalte+239,$Zeile);
			$Ausdruck->_pdf->Cell(25,8,'AUSFALLURSACHE',1,0,'C',1);
			
			$Ausdruck->_pdf->SetXY($Spalte+264,$Zeile);
			$Ausdruck->_pdf->Cell(18,8,'KFZ-KENNZ',1,0,'C',1);
						
			$Zeile=$Zeile+4;
		}
		
		$Zeile = $Zeile+4;
		$Ausdruck->_pdf->SetFont('Arial','',7);
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		
		if ($VUFIL == true)
		{
			$Ausdruck->_pdf->Cell(10,4,$rsSchad['VUFIL'][$SchadZeile],0,0,'L',0);
			$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+4);											
		}
		else 
		{
			$Ausdruck->_pdf->Cell(10,4,'',0,0,'L',0);
			$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+4);											
		}
		
		$Ausdruck->_pdf->SetXY($Spalte+10,$Zeile);
		
		if ($Kennung == true)
		{
			$Ausdruck->_pdf->Cell(60,4,$rsSchad['KENNUNG_BEZEICHNUNG'][$SchadZeile],0,0,'L',0);			
			$Ausdruck->_pdf->Line($Spalte+10,$Zeile,$Spalte+10,$Zeile+4);											
		}
		else 
		{
			$Ausdruck->_pdf->Cell(60,4,'',0,0,'L',0);
			$Ausdruck->_pdf->Line($Spalte+10,$Zeile,$Spalte+10,$Zeile+4);											
		}		
		
		$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile);
		$Ausdruck->_pdf->Cell(15,4,$rsSchad['BEARBNR'][$SchadZeile],1,0,'R',0);
				
		$Ausdruck->_pdf->SetXY($Spalte+85,$Zeile);
		$Ausdruck->_pdf->Cell(35,4,$rsSchad['ANTRAGART'][$SchadZeile],1,0,'R',0);							
		
		$Ausdruck->_pdf->SetXY($Spalte+120,$Zeile);
		$Ausdruck->_pdf->Cell(15,4,$rsSchad['BEARBFIL'][$SchadZeile],1,0,'R',0);											
		
		$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
		$Ausdruck->_pdf->Cell(20,4,($rsSchad['FALLKOSTEN'][$SchadZeile]!=''?$rsSchad['FALLKOSTEN'][$SchadZeile].' �':''),1,0,'R',0);							
		
		$Ausdruck->_pdf->SetXY($Spalte+155,$Zeile);
		$Ausdruck->_pdf->Cell(23,4,substr($rsSchad['URSPR_AUFTRAG'][$SchadZeile],0,15),1,0,'R',0);							
		
		$Ausdruck->_pdf->SetXY($Spalte+178,$Zeile);
		$Ausdruck->_pdf->Cell(15,4,$rsSchad['ATUNR'][$SchadZeile],1,0,'R',0);							
		
		$Ausdruck->_pdf->SetXY($Spalte+193,$Zeile);
		$Ausdruck->_pdf->Cell(23,4,substr($rsSchad['WORAN_GEARBEITET'][$SchadZeile],0,15),1,0,'R',0);							
		
		$Ausdruck->_pdf->SetXY($Spalte+216,$Zeile);
		$Ausdruck->_pdf->Cell(23,4,substr($rsSchad['WAS_BESCHAEDIGT'][$SchadZeile],0,15),1,0,'R',0);							
		
		$Ausdruck->_pdf->SetXY($Spalte+239,$Zeile);
		$Ausdruck->_pdf->Cell(25,4,substr($rsSchad['AUSFALLURSACHE'][$SchadZeile],0,16),1,0,'R',0);							
		
		$Ausdruck->_pdf->SetXY($Spalte+264,$Zeile);
		$Ausdruck->_pdf->Cell(18,4,$rsSchad['KFZ_KENNZ'][$SchadZeile],1,0,'R',0);							
				
		$Summe = floatval(str_replace(",",".",$rsSchad['FALLKOSTEN'][$SchadZeile]));
		$Summe_Anzahl = $Summe_Anzahl + 1;
		$Summe_Gesamt = $Summe_Gesamt + $Summe;											
		
		$Summe_Anzahl_Fil = $Summe_Anzahl_Fil + 1;				
		$Summe_Gesamt_Fil = $Summe_Gesamt_Fil + $Summe;	
	}
	
	$Zeile = $Zeile+4;				
			
	$Ausdruck->_pdf->SetFont('Arial','B',6);
	
	$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile);
	$Ausdruck->_pdf->Cell(15,4,$Summe_Anzahl,1,0,'R',0);		
	$Summe_Anzahl = 0;
	
	$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
	$Ausdruck->_pdf->Cell(20,4,number_format($Summe_Gesamt,2,',','.').' �',1,0,'R',0);	
	$Summe_Gesamt = 0;
	
	$Ausdruck->_pdf->Line($Spalte+10,$Zeile+4,$Spalte+282,$Zeile+4);											
	$Ausdruck->_pdf->Line($Spalte+282,$Zeile,$Spalte+282,$Zeile+4);											
	$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte,$Zeile+4);											
	$Ausdruck->_pdf->Line($Spalte+10,$Zeile,$Spalte+10,$Zeile+4);											
		
	$Zeile = $Zeile+4;					
	
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(70,4,'Gesamt',1,0,'L',0);
	
	$Ausdruck->_pdf->SetXY($Spalte+70,$Zeile);
	$Ausdruck->_pdf->Cell(15,4,$Summe_Anzahl_Fil,1,0,'R',0);		
	$Summe_Anzahl_Fil = 0;
	
	$Ausdruck->_pdf->SetXY($Spalte+135,$Zeile);
	$Ausdruck->_pdf->Cell(20,4,number_format($Summe_Gesamt_Fil,2,',','.').' �',1,0,'R',0);	
	$Summe_Gesamt_Fil = 0;
	
	$Ausdruck->_pdf->Line($Spalte,$Zeile,$Spalte+282,$Zeile);											
	$Ausdruck->_pdf->Line($Spalte+282,$Zeile,$Spalte+282,$Zeile+4);																			
	$Ausdruck->_pdf->Line($Spalte,$Zeile+4,$Spalte+282,$Zeile+4);																			
	
	//Legende
	$Ausdruck->_pdf->SetFont('Arial','',7);
	$Ausdruck->_pdf->SetXY($Spalte,$Ausdruck->SeitenHoehe()-15);
	$Ausdruck->_pdf->Cell(120,4,'Hinweis: Sie k�nnen zu jedem in Ihrer Filiale regulierten Vorgang via AWIS Beschwerdeverwaltung die Details abrufen.',0,0,'L',0);	
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-12);
	$Ausdruck->_pdf->SetFont('Arial','B',7);
	$Ausdruck->_pdf->Cell(120,4,'Legende: VU-FIL = Verursachende Filiale; BEARB-FIL = bearbeitende / regulierende Filiale',0,0,'L',0);
		
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',6);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,'Dieser Audruck ist Eigentum der ATU Auto-Teile-Unger. Dieser darf nicht an Dritte weiter gegeben werden.',0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-35),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(30,8,'Erstellt: '.$AWISBenutzer->BenutzerName(). '  '.date('d.m.Y'),0,0,'R',0);
	//$Ausdruck->Anzeigen();

?>