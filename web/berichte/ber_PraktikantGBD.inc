<?php
require_once 'awisBerichte.inc';

class ber_PraktikantGBD
    extends awisBerichte
{
    private $pdf_vorlage = '';

    /**
     * Prozedur zum Initialisieren des Berichts
     *
     * @see awisBerichte::init()
     */
    public function init(array $Parameter)
    {
        // Daten f�r den Bericht laden
        $SQL = "";
        $SQL .= "SELECT * FROM v_praktikumsvereinbarung";

        $Bedingung = "";
        if (isset($Parameter['PVS_KEY'])) {
            $Param = explode('~', $Parameter['PVS_KEY']);
            $Bedingung .= " AND pVs_key " . self::_VergleichsOperatorPruefen($Param[0]) . " " . $this->_DB->WertSetzen('PVS', 'N0', $Param[1]);
        }

        if ($Bedingung != "") {
            $SQL .= " WHERE " . substr($Bedingung, 4);
        }

        // Daten laden
        $this->_rec = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('PVS'));

        // Alle Textbausteine laden
        $TextKonserven[] = array('PVS', '*');
        $TextKonserven[] = array('Fehler', 'err_KeineDaten');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    /**
     * Eiegntliches PDF erzegen
     *
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {
        // PDF Attribute setzen
        $this->SetTitle($this->_BerichtsName);
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->AliasNbPages();
        $this->SetFillColor(192, 192, 192);

        $this->ErstelleDoc01();
        $this->ErstelleDoc02();
        $this->ErstelleDoc03();
        $this->ErstelleDoc04();
        $this->ErstelleDoc05();
        $this->ErstelleDoc06();

        $this->FertigStellen($Ausgabeart);
    }


    private function ErstelleDoc01()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(1);
        $this->useTemplate($pdf_vorlage);

        $this->_Spalte = $this->_Parameter['SpalteFiliale'];
        $this->_Zeile = $this->_Parameter['StartSeite1'];

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], " " . $this->_rec->FeldInhalt('F_NR') . " // " . $this->_rec->FeldInhalt('F_BEZ'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['BreiteBetreuer'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('GL1_ANREDE'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['BreiteBetreuer'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Parameter['BreiteBetreuer'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . " " . $this->_rec->FeldInhalt('B_VORNAME'), 0, 0,
            'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.1;
        $this->_Spalte = $this->_Parameter['BreiteBisRechts'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], date("d.m.Y"), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HolePraktikantDatum(true, false, true), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] + 12.3;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 35;
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_NAME') . ',' . $this->_rec->FeldInhalt('P_VORNAME'), 0, 0, 'L');
    }

    private function ErstelleDoc02()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(2);
        $this->useTemplate($pdf_vorlage);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] + 110;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetXY($this->_Spalte,$this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_GBDFuerFragen']);
    }

    private function ErstelleDoc03()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(3);
        $this->useTemplate($pdf_vorlage);

        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->_Zeile = $this->_Parameter['StartSeite3'];

        //*****************************************************************************************************************************************************************************
        // Empf�nger-Daten
        //*****************************************************************************************************************************************************************************
        $MitAdrZusatz = false;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('BT_NAME'), 0, 0, 'L', false);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_AllgZuHd'] . ' ' . $this->_rec->FeldInhalt('BT_KONPERSON'), 0, 0, 'L',
            false);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('BT_STRASSE') . ' ' . $this->_rec->FeldInhalt('BT_HAUSNR'), 0, 0, 'L', false);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $P_AdrZusatz = $this->_rec->FeldInhalt('BT_ADRZUSATZ');
        if (!empty($P_AdrZusatz)) {
            $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('BT_ADRZUSATZ'), 0, 0, 'L', false);
            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $MitAdrZusatz = true;
        }
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('BT_PLZ') . ' ' . $this->_rec->FeldInhalt('BT_ORT'), 0, 0, 'L', false);

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 8;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L');
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L');
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('F_NR'), 0, 0, 'L');
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], date("d.m.Y"), 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3;
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_VORNAME') . ' ' . $this->_rec->FeldInhalt('P_NAME'));
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3.2;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 53;
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->HolePraktikantDatum(false, false) . $this->HolePraktikantDatum(true, true) . '.');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 6.1;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 12;
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $Text = $this->_rec->FeldInhalt('F_BEZ') . ', ' . $this->_rec->FeldInhalt('F_STRASSE') . ', ' . $this->_rec->FeldInhalt('F_PLZ') . ' ' . $this->_rec->FeldInhalt('F_ORT');
        $this->Cell($this->_Spalte + 100, $this->_Parameter['Zeilenhoehe'], $Text, 0, 0, 'L', false);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 12;
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte + 100, $this->_Parameter['Zeilenhoehe'],
            $this->_rec->FeldInhalt('P_NAME') . ',' . $this->_rec->FeldInhalt('P_VORNAME') . ' wird sich in der Filiale bei', 0, 0, 'L', false);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 12;
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte + 100, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . ',' . $this->_rec->FeldInhalt('B_VORNAME') . ' melden.', 0, 0, 'L',
            false);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 5.2;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 12;

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftklein']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(4), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(6), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(5), 0, 0, 'L');

        //$this->_Spalte = $this->_Parameter['BetreuerSeite2'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftklein'] + 2);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . ', ' . $this->_rec->FeldInhalt('B_VORNAME'), 0, 0, 'L');
    }

    private function ErstelleDoc04()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(4);
        $this->useTemplate($pdf_vorlage);

        $this->_Spalte = $this->_Parameter['SpalteKopf'];
        $this->_Zeile = $this->_Parameter['StartSeite4'];

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_NAME') . ", " . $this->_rec->FeldInhalt('P_VORNAME'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['SpalteKopf'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('F_NR') . " // " . $this->_rec->FeldInhalt('F_BEZ'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['SpalteKopf'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HolePraktikantDatum(true, true), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['SpalteKopf'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleTaetigkeitbezeichnung(), 0, 0, 'L');

        $this->_Zeile = $this->_Parameter['Seite4Unterschrift'];
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell(24, $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L');
        $this->Cell(37, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('GL1'), 0, 0, 'L');
        $this->Cell(37, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('WL1'), 0, 0, 'L');
        $this->Cell(37, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . ' ' . $this->_rec->FeldInhalt('B_VORNAME'), 0, 0, 'L');
        $this->Cell(44, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_NAME') . ' ' . $this->_rec->FeldInhalt('P_VORNAME'), 0, 0, 'L');
    }

    private function ErstelleDoc05()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(5);
        $this->useTemplate($pdf_vorlage);

        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 41;
        $this->_Zeile = $this->_Parameter['StartSeite5'];

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseGross']);

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'],
            $this->_rec->FeldInhalt('P_ANREDE') . ' ' . $this->_rec->FeldInhalt('P_VORNAME') . " " . $this->_rec->FeldInhalt('P_NAME'), 0, 0, 'C');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 5;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 41;
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseGross'] - 4);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HolePraktikantDatum(true, true, false), 0, 0, 'C');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2.6;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseGross'] - 4);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HolePraktikumsArt(1), 0, 0, 'L');

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseGross']);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 41;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(1), 0, 0, 'C');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 41;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(2), 0, 0, 'C');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 41;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(3), 0, 0, 'C');

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseGross'] - 4);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 6.2;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'],
            $this->_rec->FeldInhalt('P_ANREDE') . ' ' . $this->_rec->FeldInhalt('P_NAME') . ' wurde im Bereich ' . $this->HoleTaetigkeitbezeichnung(), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 5.8;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'],
            'Wir w�nschen ' . $this->_rec->FeldInhalt('P_ANREDE') . ' ' . $this->_rec->FeldInhalt('P_NAME') . ' f�r den weiteren Lebensweg',
            0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 12.8;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(44, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_VORNAME') . ' ' . $this->_rec->FeldInhalt('B_NAME'), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] + 1.1;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], 'Praktikumsbetreuer ' . $this->HoleFiliale(1), 0, 0, 'L');
    }

    private function ErstelleDoc06()
    {
        $this->NeueSeite();
        $pdf_vorlage = $this->ImportPage(6);
        $this->useTemplate($pdf_vorlage);

        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->_Zeile = $this->_Parameter['StartSeite6'];

        //*****************************************************************************************************************************************************************************
        // Empf�nger-Daten
        //*****************************************************************************************************************************************************************************
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_Sorgeberechtigte'], 0, 0, 'L', false);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_ANREDE') . ' ' . $this->_rec->FeldInhalt('P_VORNAME') . ' ' . $this->_rec->FeldInhalt('P_NAME'), 0, 0, 'L',
            false);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_STRASSE') . ' ' . $this->_rec->FeldInhalt('P_HAUSNR'), 0, 0, 'L', false);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $P_AdrZusatz = $this->_rec->FeldInhalt('BT_ADRZUSATZ');
        if (!empty($P_AdrZusatz)) {
            $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_ADRZUSATZ'), 0, 0, 'L', false);
            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
        }
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_PLZ') . ' ' . $this->_rec->FeldInhalt('P_ORT'), 0, 0, 'L', false);

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * (!empty($P_AdrZusatz)?7:8);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L');
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], '', 0, 0, 'L');
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('F_NR'), 0, 0, 'L');
        $this->Cell($this->_Parameter ['Zellengroesse'], $this->_Parameter['Zeilenhoehe'], date("d.m.Y"), 0, 0, 'L');
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2 + 0.75;
        $this->_Spalte = $this->_Parameter ['BreiteAussen'] + 21;
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['PVS']['pdf_GBDUeberschrift'] );
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);

        $this->_Spalte = $this->_Parameter ['BreiteAussen'];
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('P_VORNAME') . ' ' . $this->_rec->FeldInhalt('P_NAME'));
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 3.1 - 0.12;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 76.25;
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'], $this->HolePraktikantDatum(false, false));

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Write($this->_Parameter['Zeilenhoehe'],$this->HolePraktikantDatum(true, true) . '.');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 5.6 - 0.4;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 12;
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $Text = $this->_rec->FeldInhalt('F_BEZ') . ', ' . $this->_rec->FeldInhalt('F_STRASSE') . ', ' . $this->_rec->FeldInhalt('F_PLZ') . ' ' . $this->_rec->FeldInhalt('F_ORT');
        $this->Cell($this->_Spalte + 100, $this->_Parameter['Zeilenhoehe'], $Text, 0, 0, 'L', false);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte + 100, $this->_Parameter['Zeilenhoehe'],
            $this->_rec->FeldInhalt('P_NAME') . ',' . $this->_rec->FeldInhalt('P_VORNAME') ." ". $this->_AWISSprachkonserven['PVS']['pdf_InDerFiliale'], 0, 0, 'L', false);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Spalte + 100, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . ',' . $this->_rec->FeldInhalt('B_VORNAME') ." ". $this->_AWISSprachkonserven['PVS']['pdf_Melden'], 0, 0, 'L',
            false);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4.6;
        $this->_Spalte = $this->_Parameter['BreiteAussen'] + 12;

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftklein']);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(4), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(6), 0, 0, 'L');

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->HoleFiliale(5), 0, 0, 'L');

        //$this->_Spalte = $this->_Parameter['BetreuerSeite2'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 4.25;
        $this->_Spalte = $this->_Parameter['BreiteAussen'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftklein'] + 2);
        $this->Cell($this->_Spalte, $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('B_NAME') . ', ' . $this->_rec->FeldInhalt('B_VORNAME'), 0, 0, 'L');
    }
    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage();
    }


    public function FertigStellen($Ausgabeart)
    {
        //*********************************************************************
        // Berichtsende -> Zur�ckliefern
        //*********************************************************************
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zur�ckliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }


    private function HolePraktikumsArt($PraktikumartKey)
    {
        $RetVal = '';

        switch ($PraktikumartKey) {
            case 1:        // Sch�lerp.
                $RetVal = $this->_AWISSprachkonserven['PVS']['pdf_DiplomOibText02'];
                break;
            case 2:        // berufsvorb. P.
                $RetVal = $this->_AWISSprachkonserven['PVS']['pdf_DiplomBvbText02'];
                break;
            case 3:        // freiwilliges P.
                break;
            case 4:        // Lehrstellenbewerber
                break;
        }

        return $RetVal;
    }

    private function HoleFiliale($Line = 1)
    {
        $Filiale = '';
        switch ($Line) {
            case 1:
                $Filiale = $this->_rec->Feldinhalt('F_BEZ');
                break;
            case 2:
                $Filiale = $this->_rec->Feldinhalt('F_STRASSE');
                break;
            case 3:
                $Filiale = $this->_rec->Feldinhalt('F_PLZ') . " " . $this->_rec->Feldinhalt('F_ORT');
                break;
            case 4:
                $Filiale = $this->_rec->Feldinhalt('F_TEL');
                break;
            case 5:
                $Filiale = $this->_rec->Feldinhalt('F_MAIL');
                break;
            case 6:
                $Filiale = $this->_rec->Feldinhalt('F_FAX');
                break;
            default:
                $Filiale = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt'];
                break;
        }

        return $Filiale;
    }

    private function HolePraktikantDatum($MitDatum = true, $NurDatum = false, $HerrMitN = false)
    {
        $PraktikantDatum = '';
        if (!$NurDatum) {
            if ($HerrMitN) {
                if ($this->_rec->FeldInhalt('PVS_ANR_ID') == 2) {
                    $PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_AllgFrau'] . " ";
                } else {
                    $PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_AllgHerr'] . " ";
                }
            } else {
                $PraktikantDatum .= $this->_rec->FeldInhalt('P_ANREDE') . " ";
            }
            $PraktikantDatum .= $this->_rec->FeldInhalt('P_VORNAME') . " ";
            $PraktikantDatum .= $this->_rec->FeldInhalt('P_NAME') . " ";
        }

        if ($MitDatum) {
            if ($this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) == $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS'))) {
                $PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_Am'] . " ";
                $PraktikantDatum .= $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . "";
            } else {
                $PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_Von'] . " ";
                $PraktikantDatum .= $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMVON')) . " ";
                $PraktikantDatum .= $this->_AWISSprachkonserven['PVS']['pdf_Bis'] . " ";
                $PraktikantDatum .= $this->_Form->Format('D', $this->_rec->FeldInhalt('P_DATUMBIS'));
            }
        }

        return $PraktikantDatum;
    }

    public function HoleTaetigkeitbezeichnung()
    {
        $Taetigkeit = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt'];
        $Daten = explode('|', $this->_AWISSprachkonserven['PVS']['lst_Taet']);
        foreach ($Daten AS $Felder) {
            $Feld = explode('~', $Felder);
            if ($this->_rec->FeldInhalt('PVS_TAETIGKEIT') == $Feld[0]) {
                $Taetigkeit = $Feld[1];
                break;
            }
        }

        return $Taetigkeit;
    }

    public function HoleTaetigkeitbezeichnungBewertung($Geschlecht = '')
    {
        if ($Geschlecht == '') {
            if ($this->_rec->FeldInhalt('PVS_ANR_ID') == 2) {
                $Geschlecht = 'FRAU';
            } else {
                $Geschlecht = 'MANN';
            }
        }

        $Taetigkeit = $this->_AWISSprachkonserven['PVS']['PVS_Unbekannt'];

        switch ($this->_rec->FeldInhalt('PVS_TAETIGKEIT')) {
            case 1:
                if ($Geschlecht == 'MANN') {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungVerkaufMann'];
                } elseif ($Geschlecht == 'FRAU') {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungVerkaufFrau'];
                }
                break;
            case 2:
                if ($Geschlecht == 'MANN') {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungWerkstattMann'];
                } elseif ($Geschlecht == 'FRAU') {
                    $Taetigkeit = $this->_AWISSprachkonserven['PVS']['pdf_BewertungWerkstattFrau'];
                }
                break;
        }

        return $Taetigkeit;
    }
}

?>