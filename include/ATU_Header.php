<?php
//*****************************************************************************************************
//*
//* Datei:   ATU_Header.php
//* Autor:   Sacha Kerres
//* Datum:   1.1.2000
//*
//* Beschreibung:      Diese Datei muss f�r jede Seite (.._main.php) eingebunden werden
//*
//* Voraussetzungen:   Es sollte eine Datei mit dem Namen version.txt existieren, in der der
//*                    Inhalt des Headers beschrieben wird. Fehlt die Datei, wird ein allgemeiner
//*                    Text verwendet.
//*
//* �nderungen:
//*
//*****************************************************************************************************
	require_once("register.inc.php");
	require_once("db.inc.php");		// DB-Befehle
	require_once("sicherheit.inc.php");
	require_once("awisUser.php");			// BenutzerObjekt

	global $AWISBenutzer;

	if(session_id()=='')
	{
		session_start();
	}

	echo '<table border="0" width="100%" cellspacing=0 cellpadding=0 style="border-spacing:0; margin: 0 0 0 0; background-image:url(/bilder/headerbild.png);">';

	$con = awisLogon();
	$MinAnzeige = awis_BenutzerParameter($con, 'MinimaleKopfzeile', $AWISBenutzer->BenutzerName());
	$ShortCutLeiste = awis_BenutzerParameter($con, 'ShortCutLeiste', $AWISBenutzer->BenutzerName());
	$pfad = dirname($_SERVER['SCRIPT_FILENAME'])."/version.txt";

	if(file_exists($pfad))
	{
		$fd = fopen($pfad, "r");
		$Zeile = trim(fgets($fd));
		while((substr($Zeile,0,8)!="[Header]")AND !feof($fd))		// Bis zum Header lesen
		{
			$Zeile = trim(fgets($fd));
		}

		$Zeile = trim(fgets($fd));
		$i=0;

		while((substr($Zeile,0,1)!="[")AND !feof($fd))		// Bis zum n�chsten Header lesen
		{													// Alle Parameter auslesen
			if(substr($Zeile,0,1)!="#" AND substr($Zeile,0,1)!="")
			{
				$Werte=explode("=",$Zeile);
				$VInfo[strtoupper($Werte[0])] = $Werte[1];
			}

			$Zeile = trim(fgets($fd));
		}

		// Header schreiben

		echo "<tr><td width=20%><p align=left style='margin: 0 0 0 0'>";
		if(strstr(',AP-SRV03,AP-SRV04,ATLX22SU91,ATUTEST.AWIS.ATU.DE,',','.strtoupper($_SERVER['SERVER_NAME']).',')!=FALSE||strtoupper($_SERVER['HTTP_HOST'])=="ATLX22SU91")
		{
			echo '<span class=HinweisText>T E S T S Y S T E M</span><br>';
		}

		if(!$MinAnzeige)
		{
			echo "<a href=/index.php target=_self><img src='";
			echo $VInfo["LOGO"];
			echo "' ></a></p>";
		}
		else
		{
			print "<font size=1pt ><b>" . $AWISBenutzer->BenutzerName() . "</b></font>" ;
		}
		echo "</td>";

			//**********************************************
			// Shortcut - Leiste
			//**********************************************
		$ShortCutLeiste = explode(';', $ShortCutLeiste);

		echo '<td width=40 valign=center>';

		echo '<table border=0>';
		echo '<tr>';
		if(isset($ShortCutLeiste[1]) AND $ShortCutLeiste[1]!='')
		{
			echo '<td><a target=' . $ShortCutLeiste[0] . ' href=' . $ShortCutLeiste[1] . " onMouseover=\"window.status='" . $ShortCutLeiste[3] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[2] . ' title=\'' . $ShortCutLeiste[3] . '\'></td>';
		}
		if(isset($ShortCutLeiste[5]) AND $ShortCutLeiste[5]!='')
		{
			echo '<td><a target=' . $ShortCutLeiste[4] . ' href=' . $ShortCutLeiste[5] . " onMouseover=\"window.status='" . $ShortCutLeiste[7] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[6] . ' title=\'' . $ShortCutLeiste[7] . '\'></td>';
		}
		if(isset($ShortCutLeiste[9]) AND $ShortCutLeiste[9]!='')
		{
			echo '<td><a target=' . $ShortCutLeiste[8] . ' href=' . $ShortCutLeiste[9] . " onMouseover=\"window.status='" . $ShortCutLeiste[11] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[10]. ' title=\'' . $ShortCutLeiste[11] . '\'></td>';
		}

		if(!$MinAnzeige)
		{
			echo '</tr>';
			echo '<tr>';
		}

		if(isset($ShortCutLeiste[13]) AND $ShortCutLeiste[13]!='')
		{
			echo '<td><a target=' . $ShortCutLeiste[12] . ' href=' . $ShortCutLeiste[13]. " onMouseover=\"window.status='" . $ShortCutLeiste[15] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[14]. ' title=\'' . $ShortCutLeiste[15] . '\'></td>';
		}
		if(isset($ShortCutLeiste[17]) AND $ShortCutLeiste[17]!='')
		{
			echo '<td><a target=' . $ShortCutLeiste[16] . ' href=' . $ShortCutLeiste[17]. " onMouseover=\"window.status='" . $ShortCutLeiste[19] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[18]. ' title=\'' . $ShortCutLeiste[19] . '\'></td>';
		}
		if(isset($ShortCutLeiste[21]) AND $ShortCutLeiste[21]!='')
		{
			echo '<td><a target=' . $ShortCutLeiste[20] . ' href=' . $ShortCutLeiste[21]. " onMouseover=\"window.status='" . $ShortCutLeiste[23] . "';return true;\" onMouseOut=\"window.status='AWIS';return true;\"><img border=0 width=22 height=22 src=/bilder/" . $ShortCutLeiste[22]. ' title=\'' . $ShortCutLeiste[23] . '\'></td>';
		}
		echo '</tr>';
		echo '</table>';

		echo '</td>';


			//***********************************
			// Mittelblock mit dem Namen
			//***********************************
		echo "<td width=35%><p align=center style='margin:0 0 0 0'><b><font color=#3399FF>";

				// Alle Versionsnummern auslesen
		$Zeile = trim(fgets($fd));
		while((substr($Zeile,0,1)=="#" OR substr($Zeile,0,1)=="") AND !feof($fd))
		{
			$Zeile = trim(fgets($fd));
		}
		$VersionsInfos=explode(";", $Zeile);
		if(!$MinAnzeige)
		{
			echo $VInfo["PRODUKTNAME"] . "<br>";
		}
		echo "<font size=2>" . $VInfo["MODULNAME"] .", Version " . $VersionsInfos[0] . "</font>";

		print "<img src=/bilder/info_klein.png alt=VersionsInfo onclick=window.open('/hilfe/versionsinfo.php?Verzeichnis=$pfad','Versionsinformation','toolbar=no,menubar=no,dependent=yes,status=no'); onMouseover=\"window.status='Versionshistorie anzeigen';return true;\" onMouseOut=\"window.status='AWIS';return true;\">";
		fclose($fd);

		print "</font>";
		if(!$MinAnzeige)
		{
			if(strstr(',AWIS.SERVER.ATU.DE,',','.strtoupper($_SERVER['SERVER_NAME']).',')!=FALSE)
			{
				echo "<br><font size=1 color=#3399FF>" . $_SERVER['SERVER_NAME'] . "</font></b>";
				//echo "<br><span class=Hinweistext>AWIS BETA TEST</span>";
				echo "</td>";
			}
			else
			{
				echo "<br><font size=1 color=#3399FF>" . $_SERVER['SERVER_NAME'] . "</font></b></td>";
			}

			//*********************************************************
			// Sprachauswahl
			//*********************************************************
			echo "<td width=5%>";
			if(isset($VInfo["SPRACHEN"]))
			{
				$AltLink = $_SERVER['REQUEST_URI'];
				if(strpos($AltLink,'AWISSprache')!==false)
				{
					global $AWISSprache;
					$AWISSprache = substr($AltLink,strpos($AltLink,'AWISSprache')+12,2);
					awis_BenutzerParameterSpeichern($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName() ,$AWISSprache);
					$AltLink=substr($AltLink,0,strpos($AltLink,'AWISSprache')-1);
				}
				echo '<table border=0 width=60px><tr><td width=55px>';
				$Sprachen = explode(',',$VInfo["SPRACHEN"]);
				foreach($Sprachen AS $Sprache)
				{
					echo '<a id=BilderLink href="'.$AltLink.(strpos($AltLink,'?')===false?'?':'&').'AWISSprache='.$Sprache.'"><img border=0 style="background-color:#D0D0D0;padding:2px;margin:1px;width:20px;height:12px" width="20" height="12" src="/bilder/Flagge_'.strtoupper($Sprache).'.gif"></a>';
				}
				echo "</td></tr></table>";

			}
			echo "</td>";
		}


    	print "<td width=30% align=right style='margin: 0 0 0 0'>";
		if($VInfo["STARTSEITE"]!='')
		{
			print "<a href=" . $VInfo["STARTSEITE"] . " target=" . (isset($VInfo["TARGET"])?($VInfo["TARGET"]==''?"_self":$VInfo["TARGET"]):'') . "><img border=0 src=/bilder/StartSeite.gif alt=Startseite></a>";
		}
		elseif($VInfo["MARQUEE"]!='')
		{
			print "<marquee scrollamount=1 scrolldelay=50><font size=1>" . $VInfo["MARQUEE"] . "</font></marquee>";
		}
	}
	else		// Falls keine Versionsdatei gefunden wurde -> Allg. Infos anzeigen
	{
		echo "<td width=33% align=left style=margin: 0 0 0 0;>";
		echo "<img src=/bilder/atulogo_neu.png>";
		echo '</td>';

		echo "<td width=33% align=center style=margin: 0 0 0 0;><b><font color=#3399FF>";
		echo "Awis, Version 2.00.00";
		echo "</font><br><font size=1 color=#3399FF>" . $_SERVER['SERVER_NAME'] . "</font></b></td>";


		echo "<td width=34% align=right style=margin: 0 0 0 0;>";
     	echo '<a href=/index.php target=_self><img border=0 src=/bilder/StartSeite.gif alt=Startseite></a>';
	}

	if(!$MinAnzeige)
	{

			print "<br><font size=1pt >Angemeldet als <b>" . $AWISBenutzer->BenutzerName() . "</b></font>" ;

		//if($AWISBenutzer->BenutzerName()=='entwick')
		{
			//print "<br><font size=1pt>Server: " . $_SERVER['HTTP_HOST'] . "</font>";
			$hostname=gethostbyaddr($_SERVER['SERVER_ADDR']);
			print "<br><font size=1pt>Server: " . $hostname . "</font>";
		}
	}
	setlocale (LC_TIME, "de_DE");
	setlocale (LC_NUMERIC, ",");


	echo '</td></tr></table><hr>';

	if(isset($VInfo["WARNUNG"]) AND $VInfo["WARNUNG"]!='')
	{
		echo '<span class=Hinweistext>';
		echo $VInfo['WARNUNG'];
		echo '</span>';
	}

awisLogoff($con);		// Verbindung beenden, um Eigenstaendigkeit zu erhalten
?>