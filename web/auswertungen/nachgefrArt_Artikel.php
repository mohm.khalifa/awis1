<?php
// Variablen
global $awisDBFehler;
global $awisRSZeilen;
global $_POST;
global $_GET;
global $con;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 540);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Nachgefragte Artikel',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

// Rechte f�r die Links
$RechteFil = awisBenutzerRecht($con, 100);

if(isset($_GET['Reset']) AND $_GET['Reset']=='True')
{
	$Params=array();
}
else
{
	if(isset($_POST['cmdSuche_x']))
	{
		$Params=array();
		
		$Params[0] = (isset($_POST['txtAuswahlSpeichern'])?$_POST['txtAuswahlSpeichern']:'');
		$Params[1] = ($_POST['txtDatum']==''?'':awis_format($_POST['txtDatum'],'Datum'));
		$Params[2] = $_POST['txtAST_ATUNR'];
		$Params[3] = $_POST['txtWGR_KEY'];		
		$Params[4] = $_POST['txtUnterwarengruppe'];
		$Params[5] = $_POST['txtANG_ID'];
		$Params[6] = $_POST['txtFIL_ID'];
		
		awis_BenutzerParameterSpeichern($con, 'NachgefragteArt:SuchMaske', $AWISBenutzer->BenutzerName(),implode(';',$Params));
	}
	else
	{
		$Params = explode(';',awis_BenutzerParameter($con, 'NachgefragteArt:SuchMaske', $AWISBenutzer->BenutzerName()));
	}
}

echo '<table border=1 >';
echo '<form name=frmSuchmaske method=post action=./nachgefrArt_Main.php?cmdAktion=NachgefrArt>';
echo '<tr>';
echo '<td width=200 class=FeldBez>Datum</td><td width=100><input name=txtDatum type=text size=10 value=' . $Params[1] . '></td>';
echo '<td width=200 class=FeldBez>ATU Nummer</td><td width=100><input name=txtAST_ATUNR type=text size=10 value=' . $Params[2] . '></td>';
echo '</tr>';

echo '<tr>';
echo '<td class=FeldBez>Warengruppe</td>';
echo '<td><select name=txtWGR_KEY>';
echo '<option value=-1>::alle::</option>';
$rsWGR = awisOpenRecordset($con, 'SELECT * FROM WARENGRUPPEN ORDER BY WGR_Bezeichnung');
$rsWGRZeilen = $awisRSZeilen;
for($WGRZeile=0;$WGRZeile<$rsWGRZeilen;$WGRZeile++)
{
	echo '<option ';
	if($rsWGR['WGR_ID'][$WGRZeile]==$Params[3])
	{
		echo ' selected ';
	}
	echo ' value=' . $rsWGR['WGR_ID'][$WGRZeile] . '>' . $rsWGR['WGR_BEZEICHNUNG'][$WGRZeile] . ' (' . $rsWGR['WGR_ID'][$WGRZeile] . ')</option>';
}
echo '</select></td>';
unset($rsWGR);

echo '<td class=FeldBez>Unterwarengruppe</td><td ><input name=txtUnterwarengruppe type=text size=10 value=' . $Params[4] . '></td>';
echo '</tr>';

echo '<tr>';
echo '<td class=FeldBez>Grund</td><td>';
echo '<select name=txtANG_ID>';
echo '<option value=-1>::alle::</option>';
$rsANG = awisOpenRecordset($con, 'SELECT * FROM ARTIKELNACHFRAGENGRUND ORDER BY ANG_BEZEICHNUNG');
for($i=0;$i<$awisRSZeilen;$i++)
{
	echo '<option value=' . $rsANG['ANG_ID'][$i];
	if($rsANG['ANG_ID'][$i] == $Params[5])
	{
		echo ' selected ';
	}
	echo '>' . $rsANG['ANG_BEZEICHNUNG'][$i] . '</option>';	
}


echo '</select>';
echo '</td>';
echo '<td class=FeldBez>Filiale</td><td ><input name=txtFIL_ID type=text size=10 value=' . $Params[6] . '></td>';
echo '</tr>';
echo '</table>';		// Ende Suchmaske
echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png alt='Formularinhalt l�schen' name=cmdReset onclick=location.href='./nachgefrArt_Main.php?Reset=True';>";
echo '</form>';
echo '<hr>';

/*****************************************************************
* 
* Daten anzeigen
* 
*****************************************************************/

$Bedingung = '';
if($Params[1]!='')	// Datum
{
	$Bedingung .= " AND ANA.ANA_DATUM=TO_DATE('" . $Params[1] . "','DD.MM.RRRR')";
}
if($Params[2]!='')	// ATU-Nr
{
	$Bedingung .= " AND ANA.ANA_AST_ATUNR " . awisLIKEoderIST($Params[2]) . "";
}
if($Params[3]!=-1 AND $Params[3]!='')	// Warengruppe
{
	$Bedingung .= " AND ANA.ANA_WGR_ID='" . $Params[3] . "'";
}
if($Params[4]!='')	// UnterWarengruppe
{
	$Bedingung .= " AND AST_WUG_KEY='" . $Params[4] . "'";
}
if($Params[5]!=-1 AND $Params[5]!='')		// Grund
{
	$Bedingung .= " AND ANA.ANA_ANG_ID=" . $Params[5] . "";
}
if($Params[6]!='')
{
	$Bedingung .= " AND ANA.ANA_FIL_ID=" . $Params[6] . "";
}


if($Bedingung!='')
{
	
	/**********************************************************
	* Liste mit allen Artikeln anzeigen
	**********************************************************/
	
	$SQL = 'SELECT *';
	$SQL .= ' FROM ARTIKELNACHFRAGEN ANA INNER JOIN ARTIKELNACHFRAGENGRUND ON ANA.ANA_ANG_ID=ANG_ID';
	$SQL .= ' LEFT OUTER JOIN ARTIKELSTAMM ON ANA.ANA_AST_ATUNR = AST_ATUNR';
	$SQL .= ' INNER JOIN (SELECT COUNT(*)AS Anzahl, ANA_AST_ATUNR FROM ARTIKELNACHFRAGEN GROUP BY ANA_AST_ATUNR) A ON A.ANA_AST_ATUNR = ANA.ANA_AST_ATUNR';
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	else
	{
		$SQL .= ' WHERE ROWNUM < 500';
	}
	
	if(isset($_GET['Sort']))
	{
		$SQL .= ' ORDER BY ' . $_GET['Sort'];
	}
	else
	{
		$SQL .= ' ORDER BY ANA.ANA_AST_ATUNR';
	}
	
	
	
	$rsANA = awisOpenRecordset($con, $SQL);
	$rsANAZeilen = $awisRSZeilen;
	
	echo '<table border=1 width=100%>';
	echo '<tr>';
	echo '<td class=FeldBez><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Sort=' . (isset($_GET['Sort']) && $_GET['Sort']=='ANA_FIL_ID'?"ANA_FIL_ID%20DESC title='Absteigend Sortieren'":"ANA_FIL_ID title='Sortieren'")  . '>Filiale</a></td>';
	echo '<td class=FeldBez><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Sort=' . (isset($_GET['Sort']) && $_GET['Sort']=='ANA_AST_ATUNR'?"ANA_AST_ATUNR%20DESC title='Absteigend Sortieren'":"ANA_AST_ATUNR title='Sortieren'")  . '>ATU-Nr</a></td>';
	echo '<td class=FeldBez><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Sort=' . (isset($_GET['Sort']) && $_GET['Sort']=='AST_BEZEICHNUNGWW'?"AST_BEZEICHNUNGWW%20DESC title='Absteigend Sortieren'":"AST_BEZEICHNUNGWW title='Sortieren'")  . '>Bezeichnung</a></td>';
	echo '<td class=FeldBez><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Sort=' . (isset($_GET['Sort']) && $_GET['Sort']=='ANZAHL'?"ANZAHL%20DESC title='Absteigend Sortieren'":"ANZAHL title='Sortieren'")  . '>Anzahl </a></td>';
	echo '<td class=FeldBez>WGr</td><td class=FeldBez>UWGr</td>';
	echo '<td class=FeldBez>KBA</td><td class=FeldBez>Baujahr</td>';
	echo '<td class=FeldBez><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Sort=' . (isset($_GET['Sort']) && $_GET['Sort']=='ANA_MENGE'?"ANA_MENGE%20DESC title='Absteigend Sortieren'":"ANA_MENGE title='Sortieren'")  . '>Menge</a></td>';
	echo '<td class=FeldBez><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Sort=' . (isset($_GET['Sort']) && $_GET['Sort']=='ANA_DATUM'?"ANA_DATUM%20DESC title='Absteigend Sortieren'":"ANA_DATUM title='Sortieren'")  . '>Datum</a></td>';
	echo '<td class=FeldBez>Grund</td>';
	echo '</tr>';
	
	$DetailsOffen = False;
	for($ANAZeile=0;$ANAZeile<$rsANAZeilen;$ANAZeile++)
	{
		echo '<tr>';
		if($RechteFil>0)
		{
			echo '<td><a href=/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsANA['ANA_FIL_ID'][$ANAZeile] . '>'.$rsANA['ANA_FIL_ID'][$ANAZeile] . '</a></td>';
		}
		else
		{
			echo '<td>'.$rsANA['ANA_FIL_ID'][$ANAZeile] . '</td>';
		}
	
		if(isset($_GET['Details']) AND $_GET['Details'] == $rsANA['ANA_AST_ATUNR'][$ANAZeile])		// Details anzeigen?
		{
			echo '<td><a name=X></a><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Details=' . $rsANA['ANA_AST_ATUNR'][$ANAZeile] . '#X>'.$rsANA['ANA_AST_ATUNR'][$ANAZeile] . '</a></td>';
		}
		else
		{
			echo '<td><a href=./nachgefrArt_Main.php?cmdAktion=NachgefrArt&Details=' . $rsANA['ANA_AST_ATUNR'][$ANAZeile] . '#X>'.$rsANA['ANA_AST_ATUNR'][$ANAZeile] . '</a></td>';
		}
	
		echo '<td>'.$rsANA['AST_BEZEICHNUNGWW'][$ANAZeile] . '</td>';
		echo '<td align=right>'.$rsANA['ANZAHL'][$ANAZeile] . '</td>';
		echo '<td>'.$rsANA['ANA_WGR_ID'][$ANAZeile] . '</td>';
		echo '<td>'.$rsANA['AST_WUG_KEY'][$ANAZeile] . '</td>';
		echo '<td>'.$rsANA['ANA_KBANR'][$ANAZeile] . '</td>';
		echo '<td>'.$rsANA['ANA_BAUJAHR'][$ANAZeile] . '</td>';
		echo '<td align=right>'.$rsANA['ANA_MENGE'][$ANAZeile] . '</td>';
		echo '<td>'. awis_format($rsANA['ANA_DATUM'][$ANAZeile],'Datum') . '</td>';
		echo '<td>'.$rsANA['ANG_BEZEICHNUNG'][$ANAZeile] . '</td>';
		echo '</tr>';
		
		if(isset($_GET['Details']) AND $_GET['Details'] == $rsANA['ANA_AST_ATUNR'][$ANAZeile] AND $DetailsOffen==FALSE)		// Details anzeigen?
		{
			$DetailsOffen = True;
			echo '<tr><td colspan=2>&nbsp</td>';
		
			echo '<td colspan=8>';
			echo '<table border=1 width=600>';
			echo '<tr><td colspan=6 class=FeldBez>Details</td></tr>';
			
			$rsASI = awisOpenRecordset($con, "SELECT * FROM ArtikelStammInfos WHERE ASI_AIT_ID IN (10,11,12,40,41,33,34,30,31,35,36,72,73,74,75,76,77) AND ASI_AST_ATUNR='" . $rsANA['ANA_AST_ATUNR'][$ANAZeile] . "'");
			$rsABS = awisOpenRecordset($con,"SELECT * FROM Absatz WHERE ABS_ATUNR='" . $rsANA['ANA_AST_ATUNR'][$ANAZeile] . "' AND ABS_JAHR=" . date('Y'));
			
			echo '<tr>';		// Zeile1
			echo '<td class=FeldBez witdh=190>Bestand Weiden</td><td>' . awis_ArtikelInfo(10, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') . '</td>';
			echo '<td class=FeldBez>Letzte Menge</td><td>' . awis_ArtikelInfo(74, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') . '</td>';
			echo '<td class=FeldBez>Datum</td><td>' . awis_ArtikelInfo(72, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') . '</td>';
			echo '</tr>';
			
	
			echo '<tr>';		// Zeile2
			echo '<td class=FeldBez>Bestand Werl</td><td>' . awis_ArtikelInfo(11, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') . '</td>';
			echo '<td class=FeldBez>Letzte Menge</td><td>' . awis_ArtikelInfo(75, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') . '</td>';
			echo '<td class=FeldBez>Datum</td><td>' . awis_ArtikelInfo(73, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') . '</td>';
			echo '</tr>';
	
			echo '<tr>';		// Zeile3
			echo '<td class=FeldBez>Kennung</td><td>' . $rsANA['AST_KENNUNG'][$ANAZeile] . '</td>';
	
			$VerkVorjahr=awis_ArtikelInfo(33, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			$VerkVorjahr+=awis_ArtikelInfo(34, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			echo '<td class=FeldBez>Verk. Vorjahr</td><td>' . $VerkVorjahr . '</td>';
			$Woche=awis_ArtikelInfo(35, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			$Woche+=awis_ArtikelInfo(36, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			echo '<td class=FeldBez>Woche</td><td>' . $Woche . '</td>';
			echo '</tr>';
	
			echo '<tr>';		// Zeile4
			$Rueckstand=awis_ArtikelInfo(76, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			$Rueckstand+=awis_ArtikelInfo(77, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			echo '<td class=FeldBez>R�ckstand</td><td>' . $Rueckstand . '</td>';
			$VerkAktjahr=awis_ArtikelInfo(30, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			$VerkAktjahr+=awis_ArtikelInfo(31, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT');
			echo '<td class=FeldBez>Jahr</td><td>' . $VerkAktjahr . '</td>';
			echo '<td class=FeldBez>VK</td><td>' . $rsANA['AST_VK'][$ANAZeile] .'</td>';
			echo '</tr>';
	
			echo '<tr>';		// Zeile5
	
			if(awisBenutzerRecht($con, 605)>0)
			{
				echo '<td class=FeldBez>Lieferant</td><td><a href=/stammdaten/lieferanten.php?LIENR=' . awis_ArtikelInfo(40, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') .  '&Zurueck=/auswertungen/nachgefrArt_Main.php?cmdAktion=NachgefrArt~~Details='.$_GET['Details'].'**X>' . awis_ArtikelInfo(40, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') .  '</a></td>';
			}
			else
			{
				echo '<td class=FeldBez>Lieferant</td><td>' . awis_ArtikelInfo(40, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') .  '</td>';
			}
			echo '<td class=FeldBez>LArt-Nr</td><td colspan=3>' . awis_ArtikelInfo(41, $rsASI, $rsANA['ANA_AST_ATUNR'][$ANAZeile], 'ASI_WERT') . '</td>';
			echo '</tr>';
	
			echo '</table>';		// Ende Detailtabelle
			
			echo '</td></tr>';
		}
	}
	echo '</table>';
	echo '<br><font size=2>Es wurden ' . $rsANAZeilen . ' Datens�tze gefunden</font>';
}


echo "\n<Script Language=JavaScript>";
echo "document.getElementsByName('txtDatum')[0].focus();";
echo "\n</Script>";
?>

