<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
include_once('awisFirstglasImport_neu.php');
include_once('awisFirstglasStatus.php');

ini_set('max_execution_time', 0);

class firstglasShuttle
{

    private $_DB;
    private $_AWISBenutzer;
    private $_FORM;
    private $_FirstglasImport;
    private $_FirstglasStatus;
    private $_DWHGeladen = true;
    private $_DatumLetzteJobAusfuehrung = '';
    private $_AktuellesDatum = '';
    private $_JobID;
    private $_pruefeMappings = array();
    private $_AktDatum = '';

    function __construct()
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_FORM = new awisFormular();
        $this->_FirstglasImport = FirstGlasImport_neu::getInstance();
        $this->_FirstglasStatus = new awisFirstglasStatus();
    }

    public function pruefeDWHGeladen()
    {
        // festlegen um welche Abwicklung es sich handelt
        $System = 'Firstglass';

        // Status fuer jede Tabelle zuruecksetzen
        $SQL = 'UPDATE versmappingdwh SET vdm_geladen = 0, vdm_ladenjanein = 0 ';
        $SQL .= "WHERE vdm_system = '" . $System . "'";

        $this->_DB->Ausfuehren($SQL);

        // Selektieren welche Tabellen geprueft werden muessen
        $SQL = "SELECT vdm_mappingname ";
        $SQL .= "FROM versmappingdwh ";
        $SQL .= "WHERE vdm_system = '" . $System . "' ";
        $SQL .= "AND vdm_aktiv = 1";

		$rsTabellen = $this->_DB->RecordSetOeffnen($SQL);
		
        while (!$rsTabellen->EOF())
        {
            $SQL = 'SELECT phase_2, errors_2, laden_ja_nein, phase_1 ';
            $SQL .= 'FROM int.V$_EXPGW_EXPORT_LAST_FREE@DWH a INNER JOIN ods.DWH$EXPGW_EXPORT_JOURNAL@DWH b ';
            $SQL .= "ON a.jobtype_id = b.jobtype_id AND a.transaktionsid = b.transaktionsid ";
            $SQL .= "WHERE b.source_tab = '" . $rsTabellen->FeldInhalt('VDM_MAPPINGNAME') . "' ";

            if ($this->_DB->ErmittleZeilenAnzahl($SQL) > 0)
            {
                // Tabelle hat aktuelleste TransID
                $rsErgebnis = $this->_DB->RecordSetOeffnen($SQL);

                if ($rsErgebnis->FeldInhalt('PHASE_2') == 1 && $rsErgebnis->FeldInhalt('ERRORS_2') == 0)
                {
                    // Tabelle wurde geladen und kann freigegeben werden
                    $Geladen = 1;
                    $LadenJaNein = 1;
                }
                elseif ($rsErgebnis->FeldInhalt('PHASE_2') == 1 && $rsErgebnis->FeldInhalt('ERRORS_2') == 1)
                {
                    // Tabelle wurde mit Fehler geladen, nicht freigeben
                    $Geladen = 2;
                    $LadenJaNein = 1;

                    $this->_DWHGeladen = false;
                }
                elseif ($rsErgebnis->FeldInhalt('LADEN_JA_NEIN') !== 1)
                {
                    // Tabelle wurde fuer den Load ausgehaengt, kann freigegeben werden
                    $Geladen = 3;
                    $LadenJaNein = -1;
                }
                else
                {
                    // alle sonstigen Faelle (kann normal nicht sein), nicht freigeben
                    $Geladen = 4;
                    $LadenJaNein = 0;

                    $this->_DWHGeladen = false;
                }
            }
            else
            {
                // Tabelle ist vom Vorsystem freigegeben aber noch nicht ins DWH geladen.
                // Nicht freigeben
                $Geladen = 0;
                $LadenJaNein = 1;

                $this->_DWHGeladen = false;
            }

            // Status fuer gepruefte Tabelle schreiben
            $SQL = "UPDATE versmappingdwh SET vdm_geladen = " . $Geladen . ", vdm_ladenjanein = " . $LadenJaNein . " ";
            $SQL .= "WHERE vdm_mappingname = '" . $rsTabellen->FeldInhalt('VDM_MAPPINGNAME') . "' ";
            $SQL .= "AND vdm_system = '" . $System . "'";

            $this->_DB->Ausfuehren($SQL);

            $rsTabellen->DSWeiter();
        }
    }

    public function Shuttle()
    {
        if ($this->_DWHGeladen == true)
        {
            $SQL = 'Select VJI_KEY,VJI_JOBID,VJI_DATUMJOBID from VERSJOBID ORDER BY VJI_KEY';

            $rsJOB = $this->_DB->RecordSetOeffnen($SQL);

            $this->_DatumLetzteJobAusfuehrung = $rsJOB->FeldInhalt('VJI_DATUMJOBID');

            $this->_DatumLetzteJobAusfuehrung = substr($this->_DatumLetzteJobAusfuehrung, 0, 10);

            $this->_AktuellesDatum = date('d.m.Y');

            $this->_JobID = $rsJOB->FeldInhalt('VJI_JOBID');

            // wenn Verarbeitungsdatum in der Vergangenheit liegt dann JobID erhoehen und
            // und Verarbeitungsdatum auf aktuelles Datum setzten
            if (strtotime($this->_DatumLetzteJobAusfuehrung) < strtotime($this->_AktuellesDatum))
            {
                $this->_JobID++;

                $SQL = 'UPDATE VERSJOBID SET ';
                $SQL .= ' VJI_JOBID=' . $this->_DB->FeldInhaltFormat('NO', $this->_JobID, false) . ', ';
                $SQL .= ' VJI_DATUMJOBID=' . $this->_DB->FeldInhaltFormat('D', $this->_AktuellesDatum, false);

                $this->_DB->Ausfuehren($SQL);
            }

            $this->SichereDWHTabelle();

            // pruefen ob Modul im aktuellen Verarbeitungsturnus schon lief und
            // wenn nicht dann Modul aufrufen
            if ($this->PruefeAnstehend('runImport') === true)
            {
                $this->SchreibeStartZeit('runImport');
                $this->_FirstglasImport->starteImport();
                $this->SchreibeEndeZeit('runImport');
            }

            // pruefen ob Modul im aktuellen Verarbeitungsturnus schon lief und
            // wenn nicht dann Modul aufrufen
            if ($this->PruefeAnstehend('setStatus') === true)
            {
                $this->SchreibeStartZeit('setStatus');
                $this->_FirstglasStatus->setStatus();
                $this->SchreibeEndeZeit('setStatus');
            }

            $this->SichereDWHTabelle();
        }
    }

    public function PruefeAnstehend($Modulname)
    {
        $SQL = ' SELECT * ';
        $SQL .= ' FROM VersJobSteuerung INNER JOIN VersJobID ';
        $SQL .= ' ON VJS_JOBID = VJI_JOBID ';
        $SQL .= ' WHERE VJS_SYSTEM = \'Firstglas\' ';
        $SQL .= ' AND VJS_MODUL = \'' . $Modulname . '\'';

        $Rueckgabe = false;

        if ($this->_DB->ErmittleZeilenAnzahl($SQL) == 0)
        {
            $Rueckgabe = true;
        }
        return $Rueckgabe;
    }

    public function SchreibeStartZeit($Modulname)
    {
        $SQL = 'UPDATE versjobsteuerung ';
        $SQL .= ' SET vjs_startzeit = sysdate, ';
        $SQL .= ' vjs_endezeit = null, ';
        $SQL .= ' vjs_modulstatus = null ';
        $SQL .= ' WHERE vjs_modul = \'' . $Modulname . '\'';

        $this->_DB->Ausfuehren($SQL);
    }

    public function SchreibeEndeZeit($Modulname)
    {
        $SQL = 'UPDATE versjobsteuerung ';
        $SQL .= 'SET vjs_endezeit = sysdate, ';
        $SQL .= 'vjs_jobid = ' . $this->_JobID . ', ';
        $SQL .= 'vjs_modulstatus = \'Ende Modul ' . $Modulname . '\' ';
        $SQL .= 'WHERE vjs_modul = \'' . $Modulname . '\'';

        $this->_DB->Ausfuehren($SQL);

        $this->ErmittleScriptLaufzeit($Modulname);
    }

    public function ErmittleScriptLaufzeit($Modulname)
    {
        $SQL = 'SELECT vjs_startzeit, vjs_endezeit ';
        $SQL .= 'FROM versjobsteuerung ';
        $SQL .= 'WHERE vjs_modul = \'' . $Modulname . '\'';

        if ($this->_DB->ErmittleZeilenAnzahl($SQL) > 0)
        {
            $rsZeit = $this->_DB->RecordSetOeffnen($SQL);
            $timeStartzeit = strtotime($rsZeit->FeldInhalt('VJS_STARTZEIT'));
            $timeEndezeit = strtotime($rsZeit->FeldInhalt('VJS_ENDEZEIT'));
            $timeDifferenz = $timeEndezeit - $timeStartzeit;

            $SQL = 'UPDATE versjobsteuerung ';
            $SQL .= 'SET vjs_laufzeit = \'' . $timeDifferenz . '\' ';
            $SQL .= 'WHERE vjs_modul = \'' . $Modulname . '\'';

            $this->_DB->Ausfuehren($SQL);
        }
    }

    private function SichereDWHTabelle()
    {
        try
        {

            $SQL = 'ALTER TABLE SICH_WA_GLAS RENAME TO SICH_WA_GLAS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'ALTER TABLE SICH_GLASPOS RENAME TO SICH_GLASPOS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'ALTER TABLE SICH_GLASREP RENAME TO SICH_GLASREP_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'CREATE TABLE SICH_WA_GLAS AS (SELECT * FROM DWH.WA_GLAS@DWH)';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'CREATE TABLE SICH_GLASPOS AS (SELECT * FROM DWH.GLASPOS@DWH)';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'CREATE TABLE SICH_GLASREP AS (SELECT * FROM DWH.GLASREP@DWH)';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'DROP TABLE SICH_WA_GLAS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'DROP TABLE SICH_GLASPOS_TMP';
            $this->_DB->Ausfuehren($SQL);

            $SQL = 'DROP TABLE SICH_GLASREP_TMP';
            $this->_DB->Ausfuehren($SQL);
 
        }
        catch (Exception $e)
        {
            echo($e->getMessage());
            die();
        }
        catch (awisException $awis)
        {
            echo($awis->getMessage() . $awis->SQL());
            die();
        }
    }
}

//Ende Klasse
?>
