<?php

global $con;
global $Recht3700;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;  // Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $updOPAL; // Array mit allen crminfos mit CIN_CIT_ID 4004 -> fuer UpdateKennung

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('CAD', '%');
$TextKonserven[] = array('CAQ', 'CAQ_VERTRAGSNUMMER');
$TextKonserven[] = array('MBW', 'txtAnredenListe');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Wort', 'txt_KeinVerbund');
$TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
$TextKonserven[] = array('Wort', 'lbl_AaenderungInfoLoeschen');



$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3700 = awisBenutzerRecht($con, 3700, $AWISBenutzer->BenutzerName());
if ($Recht3700 == 0)
{
    awisEreignis(3, 1000, 'CRM', $AWISBenutzer->BenutzerName(), '', '', '');
    echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
    echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
    die();
}

// Fuer die Suchfunktionen
$INET_LINK_Telefon = awis_BenutzerParameter($con, 'INET_SUCHE_Telefon', $AWISBenutzer->BenutzerName());
$INET_LINK_Karte = awis_BenutzerParameter($con, 'INET_SUCHE_Karte', $AWISBenutzer->BenutzerName());

//awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
if (isset($_POST['cmdSuche_x']))
{
//awis_Debug(1,$_POST);
    $Param = '';
    $Param .= ';' . $_POST['sucSuchName'];
    $Param .= ';' . $_POST['sucSuchPLZ'];
    $Param .= ';' . $_POST['sucSuchOrt'];
    $Param .= ';' . $_POST['sucSucheBetreuer'];
    $Param .= ';' . $_POST['sucSucheStrasse'];
    $Param .= ';' . $_POST['sucSucheGebiet'];
    $Param .= ';' . $_POST['sucSucheVertrag'];
    $Param .= ';' . $_POST['sucCAK_WERT'];
    $Param .= ';' . $_POST['sucCAD_LAN_CODE'];
    $Param .= ';' . $_POST['sucCAD_STATUS'];
    $Param .= ';' . $_POST['sucCAD_IMQ_ID'];
    $Param .= ';' . $_POST['sucCAD_RANKING'];
    $Param .= ';' . $_POST['sucCIT_3006'];

    awis_BenutzerParameterSpeichern($con, "CRMSuche", $AWISBenutzer->BenutzerName(), $Param);
    awis_BenutzerParameterSpeichern($con, "AktuellerCAD", $AWISBenutzer->BenutzerName(), '');
    awis_BenutzerParameterSpeichern($con, "CRMDetailSeite", $AWISBenutzer->BenutzerName(), '');
}
elseif (isset($_POST['cmdDSZurueck_x']))
{
    $Param = awis_BenutzerParameter($con, 'CRMSuche', $AWISBenutzer->BenutzerName());
    $Bedingung = _BedingungErstellen($Param);
    $Bedingung .= ' AND CAD_NAME1 < ' . awis_FeldInhaltFormat('T', $_POST['txtCAD_NAME1']);

    $SQL = 'SELECT CAD_KEY FROM (SELECT CAD_KEY FROM CRMAdressen';
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
    $SQL .= ' ORDER BY CAD_NAME1 DESC, CAD_NAME2 DESC';
    $SQL .= ') WHERE ROWNUM = 1';

    $rsCAD = awisOpenRecordset($con, $SQL);
    if (isset($rsCAD['CAD_KEY'][0]))
    {
        $AWIS_KEY1 = $rsCAD['CAD_KEY'][0];
    }
    else
    {
        $AWIS_KEY1 = $_POST['txtCAD_KEY'];
    }
}
elseif (isset($_POST['cmdDSWeiter_x']))
{
    $Param = awis_BenutzerParameter($con, 'CRMSuche', $AWISBenutzer->BenutzerName());
    $Bedingung = _BedingungErstellen($Param);
    $Bedingung .= ' AND CAD_NAME1 > ' . awis_FeldInhaltFormat('T', $_POST['txtCAD_NAME1']);

    $SQL = 'SELECT CAD_KEY FROM (SELECT CAD_KEY FROM CRMAdressen';
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
    $SQL .= ' ORDER BY CAD_NAME1 ASC, CAD_NAME2 ASC';
    $SQL .= ') WHERE ROWNUM = 1';
    $rsCAD = awisOpenRecordset($con, $SQL);
    if (isset($rsCAD['CAD_KEY'][0]))
    {
        $AWIS_KEY1 = $rsCAD['CAD_KEY'][0];
    }
    else
    {
        $AWIS_KEY1 = $_POST['txtCAD_KEY'];
    }
}
elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
    $Param = ';;;';
    include('./crm_loeschen.php');
    if ($AWIS_KEY1 == 0)
    {
        $Param = awis_BenutzerParameter($con, "CRMSuche", $AWISBenutzer->BenutzerName());
    }
    else
    {
        $Param = $AWIS_KEY1;
    }
}
elseif (isset($_POST['cmdSpeichern_x']))
{
    $Param = ';;;';
    include('./crm_speichern.php');
    $Param = awis_BenutzerParameter($con, "AktuellerCAD", $AWISBenutzer->BenutzerName());
}
elseif (isset($_POST['cmdSpeichernV_x']))
{
    $Param = ';;;';
    include('./crm_speichern.php');
    $Param = awis_BenutzerParameter($con, "AktuellerCAD", $AWISBenutzer->BenutzerName());
}
elseif (isset($_POST['cmdUpdBest_x']))
{
    include('./crm_speichern.php');
    $Param = awis_BenutzerParameter($con, 'AktuellerCAD', $AWISBenutzer->BenutzerName());
}
elseif (isset($_POST['cmdDSNeu_x']))
{
    $Param = '-1;;;';
}
elseif (isset($_GET['CAQ_VERTRAGSNUMMER']))
{
    $Param = ';;;;;;;' . $_GET['CAQ_VERTRAGSNUMMER'] . ';;';
}
elseif (isset($_GET['CAD_KEY']))
{
    $Param = '' . $_GET['CAD_KEY'] . ';;;';  // Nur den Key speiechern
}
elseif (isset($_POST['txtCAD_KEY']))
{
    $Param = '' . $_POST['txtCAD_KEY'] . ';;;';  // Nur den Key speiechern
}
else   // Nicht ueber die Suche gekommen, letzte Adresse abfragen
{
    $Param = '';
    if (!isset($_GET['Liste']))
    {
        $Param = awis_BenutzerParameter($con, "AktuellerCAD", $AWISBenutzer->BenutzerName());
    }
    else
    {
        $Param = awis_BenutzerParameterSpeichern($con, "AktuellerCAD", $AWISBenutzer->BenutzerName(), '');
    }
    if ($Param == '' OR $Param == '0')
    {
        $Param = awis_BenutzerParameter($con, 'CRMSuche', $AWISBenutzer->BenutzerName());
    }
}

if(isset($_GET['VonSeite']) and isset($_GET['Block']))
{
	$ParamSeite = array();
	$ParamSeite[0] = $_GET['VonSeite'];
	$ParamSeite[1] = $_GET['Block']; // Block von der Seite Aenderungen
	awis_BenutzerParameterSpeichern($con, "CRMDetailSeite", $AWISBenutzer->BenutzerName(), implode(';',$ParamSeite));
}
else
{
	$ParamSeite = explode(';',awis_BenutzerParameter($con, 'CRMDetailSeite', $AWISBenutzer->BenutzerName()));
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = _BedingungErstellen($Param);

$SQL = 'SELECT DISTINCT CRMAdressen.*,CGB_GEBIET, KON_NAME1';
$SQL .= ', (SELECT CAQ_VERTRAGSNUMMER FROM CRMAKQUISESTAND WHERE CAQ_CAD_KEY = CAD_KEY AND ROWNUM = 1) AS CAQ_VERTRAGSNUMMER';
$SQL .= ', (SELECT CIN_WERT FROM CRMINFOS WHERE CIN_CIT_ID = 1013 AND CIN_CAD_KEY = CAD_KEY AND ROWNUM = 1) AS CIN_VERTRAGSLAND';
$SQL .= ', (SELECT CAD_NAME1 || COALESCE(\', \' || CAD_NAME2,\'\') FROM CRMAdressen C1 WHERE C1.CAD_KEY = CRMAdressen.CAD_CAD_KEY) AS VERBUND_FIRMA';
$SQL .= ' FROM CRMAdressen';
$SQL .= ' LEFT OUTER JOIN CRMGEBIETE ON CAD_CGB_KEY = CGB_KEY';
$SQL .= ' LEFT OUTER JOIN KONTAKTE ON CAD_KON_KEY = KON_KEY';


if ($Bedingung != '')
{
    $SQL .= ' WHERE ' . substr($Bedingung, 3);
}

if (!isset($_GET['Sort']))
{
    $SQL .= ' ORDER BY CAD_Name1, CAD_Name2, CAQ_VERTRAGSNUMMER';
}
else
{
    $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
}
// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con, 'AnzahlDatensaetzeProListe', $AWISBenutzer->BenutzerName());
$rsCAD = awisOpenRecordset($con, $SQL);
$rsCADZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
if ($rsCADZeilen == 0 AND !isset($_POST['cmdDSNeu_x']))  // Keine Meldung bei neuen Datens???tzen!
{
    echo '<span class=HinweisText>Es wurden keine passenden CRMAdressen gefunden.</span>';
}
elseif ($rsCADZeilen > 1)      // Liste anzeigen
{
    awis_FORM_FormularStart();

    awis_FORM_ZeileStart();
    $Link = './crm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    $Link .= '&Sort=CAD_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAD_NAME1')) ? '~' : '');
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_NAME1'] . '/' . $AWISSprachKonserven['CAD']['CAD_NAME2'], 400, '', $Link);
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_STRASSE'], 220);
    $Link = './crm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    $Link .= '&Sort=CAD_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAD_PLZ')) ? '~' : '');
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_PLZ'], 100, '', $Link);
    $Link = './crm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    $Link .= '&Sort=CAD_ORT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAD_ORT')) ? '~' : '');
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_ORT'], 200, '', $Link);
    //awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_CGB_KEY'],200,'',$Link);
    $Link = './crm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    $Link .= '&Sort=KON_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'KON_NAME1')) ? '~' : '');
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_KON_KEY'], 120, '', $Link);
    $Link = './crm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    $Link .= '&Sort=CAQ_VERTRAGSNUMMER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'CAQ_VERTRAGSNUMMER')) ? '~' : '');
    awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAQ']['CAQ_VERTRAGSNUMMER'], 140, '', $Link);
    awis_FORM_ZeileEnde();

    if ($rsCADZeilen > $MaxDSAnzahl)
    {
//		awis_FORM_Hinweistext('Ausgabe begrenzt auf '.$MaxDSAnzahl.'. Insgesamt '.$rsCADZeilen.' Zeilen gefunden.');
//		$rsCADZeilen=$MaxDSAnzahl;
    }

    // Blockweise
    $StartZeile = 0;
    if (isset($_GET['Block']))
    {
        $StartZeile = intval($_GET['Block']) * $MaxDSAnzahl;
    }

    // Seitenweises blaettern
    if ($rsCADZeilen > $MaxDSAnzahl)
    {
        awis_FORM_ZeileStart();
        awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'], 50, '');

        for ($i = 0; $i < ($rsCADZeilen / $MaxDSAnzahl); $i++)
        {
            if ($i != ($StartZeile / $MaxDSAnzahl))
            {
                $Text = '&nbsp;<a href=./crm_Main.php?cmdAktion=Details&Block=' . $i . (isset($_GET['Sort']) ? '&Sort=' . $_GET['Sort'] : '') . '>' . ($i + 1) . '</a>';
                awis_FORM_Erstelle_TextLabel($Text, 30, '');
            }
            else
            {
                $Text = '&nbsp;' . ($i + 1) . '';
                awis_FORM_Erstelle_TextLabel($Text, 30, '');
            }
        }
        awis_FORM_ZeileEnde();
    }

    // Tooltipptext vorbereiten
    $StatusAuswahl = explode("|", $AWISSprachKonserven['CAD']['lst_CAD_STATUS']);
    $StatusListe = array();
    foreach ($StatusAuswahl AS $Eintrag)
    {
        $Wert = explode('~', $Eintrag);
        $StatusListe[$Wert[0]] = $Wert[1];
    }

    for ($ADRZeile = $StartZeile; $ADRZeile < $rsCADZeilen and $ADRZeile < $StartZeile + $MaxDSAnzahl; $ADRZeile++)
    {    	
        awis_FORM_ZeileStart();
        $Link = './crm_Main.php?cmdAktion=Details&&CAD_KEY=' . $rsCAD['CAD_KEY'][$ADRZeile] . '';
        $Farbe = ($rsCAD['CAD_STATUS'][$ADRZeile] == 'S' ? 'color:#FF0000' : ($rsCAD['CAD_STATUS'][$ADRZeile] == 'I' ? 'color:#0000FF' : ($rsCAD['CAD_STATUS'][$ADRZeile] == 'O' ? 'color:#BB44FF' : ($rsCAD['CAD_STATUS'][$ADRZeile] == 'R' ? 'color:#FF7700' : ($rsCAD['CAD_STATUS'][$ADRZeile] == 'L' ? 'color:#008000' : ($rsCAD['CAD_STATUS'][$ADRZeile] == 'P' ? 'color:#2AB0E9' : ''))))));
        
        // Stillgelegt = Rot
        // Interesent = Blau
        // Inkasso = Violett
        // Rechnungslegung = Orange
        // Loeschen = Gr�n
        // Passiv = Hellblau
        
        $Status = isset($StatusListe[$rsCAD['CAD_STATUS'][$ADRZeile]]) ? $StatusListe[$rsCAD['CAD_STATUS'][$ADRZeile]] : '';
        awis_FORM_Erstelle_ListenFeld('CAD_NAME1', $rsCAD['CAD_NAME1'][$ADRZeile] . ($rsCAD['CAD_NAME2'][$ADRZeile] == '' ? '' : ' ' . $rsCAD['CAD_NAME2'][$ADRZeile]), 0, 400, false, ($ADRZeile % 2), '', $Link);
        awis_FORM_Erstelle_ListenFeld('CAD_STRASSE', $rsCAD['CAD_STRASSE'][$ADRZeile] . ' ' . $rsCAD['CAD_HAUSNUMMER'][$ADRZeile], 0, 220, false, ($ADRZeile % 2), $Farbe, '', 'T', '', $Status);
        awis_FORM_Erstelle_ListenFeld('CAD_PLZ', $rsCAD['CAD_LAN_CODE'][$ADRZeile] . '-' . $rsCAD['CAD_PLZ'][$ADRZeile], 0, 100, false, ($ADRZeile % 2), $Farbe, '', 'T', '', $Status);
        awis_FORM_Erstelle_ListenFeld('CAD_ORT', $rsCAD['CAD_ORT'][$ADRZeile], 0, 200, false, ($ADRZeile % 2), $Farbe, '', 'T', '', $Status);
        //awis_FORM_Erstelle_ListenFeld('CGB_GEBIET',$rsCAD['CGB_GEBIET'][$ADRZeile],0,200,false,($ADRZeile%2),'','');
        awis_FORM_Erstelle_ListenFeld('KON_NAME1', $rsCAD['KON_NAME1'][$ADRZeile], 0, 120, false, ($ADRZeile % 2), '', '');
        $Link = './crm_Main.php?cmdAktion=Details&&CAD_KEY=0' . $rsCAD['CAD_KEY'][$ADRZeile] . '&Seite=Vertragsstatus';
        awis_FORM_Erstelle_ListenFeld('CAQ_VERTRAGSNUMMER', (($rsCAD['CIN_VERTRAGSLAND'][$ADRZeile] != 'DE' AND $rsCAD['CIN_VERTRAGSLAND'][$ADRZeile] != '') ? $rsCAD['CIN_VERTRAGSLAND'][$ADRZeile] . '-' : '') . $rsCAD['CAQ_VERTRAGSNUMMER'][$ADRZeile], 0, 140, false, ($ADRZeile % 2), '', $Link);
        awis_FORM_ZeileEnde();
    }

    awis_FORM_FormularEnde();
}   // Eine einzelne Adresse
else          // Eine einzelne oder neue Adresse
{
    echo '<form name=frmCRMAdressen action=./crm_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">';
    //echo '<table>';
    $AWIS_KEY1 = (isset($rsCAD['CAD_KEY'][0]) ? $rsCAD['CAD_KEY'][0] : 0);

    awis_BenutzerParameterSpeichern($con, "AktuellerCAD", $AWISBenutzer->BenutzerName(), $AWIS_KEY1);
    echo '<input type=hidden name=txtCAD_KEY value=' . $AWIS_KEY1 . '>';

    awis_FORM_FormularStart();
    $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

    // Infozeile zusammenbauen
    $Felder = array();
    if($ParamSeite[0] == 'Aenderung')
    {
    	$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./crm_Main.php?cmdAktion=Aenderungen&Block=". $ParamSeite[1] ." accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/NeueListe.png></a>");
    }
    else
    {
    	$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./crm_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/NeueListe.png></a>");
    }
    //$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./crm_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/NeueListe.png></a>");
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_USER'][0]));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_USERDAT'][0]));
    awis_FORM_InfoZeile($Felder, '');

    $TabellenKuerzel = 'CAD';
    $EditRecht = (($Recht3700 & 2) != 0);
    $EditRechtOPALFelder = false;

    if ($AWIS_KEY1 == 0)
    {
        $EditRecht = true;
        $EditRechtOPALFelder = true;
    }

    /*
     * Nur Eintraege mit Status 'K' werden mit OPAL-Daten geupdatet. Bei allen
     * anderen duerfen alle Felder daher editierbar bleiben.
     */
    if($rsCAD['CAD_STATUS'][0] != 'K')
    {
        $EditRechtOPALFelder = true;
    }
    
    // Fuer ausgewaehlten Datensatz alle Update-InfoDS in ein Array laden
    HoleOPALUpdateDaten($AWIS_KEY1,$con);

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_ANREDE'] . ':', 150);
    $AnredenText = explode("|", $AWISSprachKonserven['MBW']['txtAnredenListe']);
    awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_ANREDE', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_ANREDE'][0]), 100, $EditRecht, $con, '', '', '3', '', '', $AnredenText, '');
    if ($EditRecht)
    {
        $CursorFeld = 'txtCAD_ANREDE';
    }

    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_STATUS'] . ':', 70);
    $KateogrieAlt = explode("|", $AWISSprachKonserven['CAD']['lst_CAD_STATUS']);
    awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_STATUS', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_STATUS'][0]), 145, $EditRecht, $con, '', '', 'A', '', '', $KateogrieAlt, '');

    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_RANKING'] . ':', 120);
    $KateogrieAlt = explode("|", $AWISSprachKonserven['CAD']['lst_CAD_RANKING']);
    awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_RANKING', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_RANKING'][0]), 200, $EditRecht, $con, '', $OptionBitteWaehlen, '', '', '', $KateogrieAlt, '');
    
    // Info-Feld "Provistion"
    $BindeVariablen=array();
    $SQL = InfoFeldSQLErstellen(4010,$AWIS_KEY1,'Detail',$BindeVariablen );
    $rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen );
    
    $cinWert = (is_null($rsCIN['CIN_WERT'][0]) || $rsCIN['CIN_WERT'][0] == '') ? 'off' : 'on';
    
    if (($Recht3700 & 32) == 32)
    {
    	awis_FORM_Erstelle_TextLabel('Provision AD:',120);
    	awis_FORM_Erstelle_Checkbox('INFO_Provision', ($AWIS_KEY1 === 0 ? 'off' : $cinWert), 20, true, 'on');
    }    
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_CGB_KEY'] . ':', 150);
    $SQL = 'SELECT CGB_KEY, CGB_Gebiet FROM CRMGebiete ORDER BY CGB_Gebiet';
    awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_CGB_KEY', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_CGB_KEY'][0]), 315, $EditRecht, $con, $SQL, $OptionBitteWaehlen, '', '', '', '', '');

    $Link = '';
    if (isset($rsCAD['CAD_KON_KEY'][0]))
    {
        $SQL = 'SELECT KKO_WERT';
        $SQL .= ' FROM KONTAKTEKOMMUNIKATION ';
        $SQL .= ' WHERE KKO_KON_KEY = 0' . $rsCAD['CAD_KON_KEY'][0] . ' AND KKO_KOT_KEY=7';
        $rsKKO = awisOpenRecordset($con, $SQL);
        $Link = (isset($rsKKO['KKO_WERT'][0]) ? 'mailto:' . $rsKKO['KKO_WERT'][0] : '');
    }
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_KON_KEY'] . ':', 120, '', $Link);

    if (($Recht3700 & 16) == 16)
    {
        /* $SQL = "select DISTINCT * FROM (SELECT  kon_key, kon_name1 || ', ' || COALESCE(kon_name2,'') AS kontaktName";
          $SQL .= ' FROM kontakte';
          $SQL .= ' INNER JOIN benutzer B on kon_key = B.xbn_kon_key';// and xbn_status=\'A\'';
          $SQL .= ' LEFT OUTER JOIN V_ACCOUNTRECHTE R on B.xbn_key = R.xbn_key';
          $SQL .= ' WHERE ((R.XRC_ID = 3704 AND BITAND(XBA_STUFE,16)=16 and kon_status=\'A\') ';
          $SQL .= ' OR XBN_KEY = 0'.$rsCAD['CAD_KON_KEY'][0].')';		// wenn er schon mal vergeben wurde, lassen
          $SQL .= ' )';
          $SQL .= ' ORDER BY kontaktName'; */

        $SQL = 'SELECT DISTINCT *';
        $SQL .= " FROM (SELECT kon.kon_key, kon.kon_name1||', '|| COALESCE(kon.kon_name2,'') AS kontaktname";
        $SQL .= ' FROM kontakte kon INNER JOIN v_accountrechte b';
        $SQL .= ' ON kon_key = b.xbn_kon_key';
        $SQL .= " WHERE ((b.xrc_id = 3704 AND BITAND (b.xba_stufe, 16) = 16 AND kon.kon_status='A')";
        $SQL .= ' OR b.xbn_key = 0' . $rsCAD['CAD_KON_KEY'][0] . '))';
        $SQL .= ' ORDER BY kontaktName';

        awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_KON_KEY', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_KON_KEY'][0]), 300, $EditRecht, $con, $SQL, $OptionBitteWaehlen, '', '', '', '', '');
    }
    elseif ((isset($rsCAD['CAD_KON_KEY'][0]) AND $rsCAD['CAD_KON_KEY'][0] == '') OR !isset($rsCAD['CAD_KON_KEY'][0]))
    {
        $SQL = "select DISTINCT * FROM (SELECT  kon_key, kon_name1 || ', ' || COALESCE(kon_name2,'') AS kontaktName";
        $SQL .= ' FROM kontakte';
        $SQL .= ' INNER JOIN benutzer B on kon_key = B.xbn_kon_key and xbn_status=\'A\'';
        $SQL .= ' WHERE (kon_status=\'A\'';
        $SQL .= ' AND KON_KEY = 0' . $AWISBenutzer->BenutzerKontaktKEY();
        $SQL .= ' OR XBN_KEY = 0' . $rsCAD['CAD_KON_KEY'][0] . ')';  // wenn er schon mal vergeben wurde, lassen
        $SQL .= ' ORDER BY KON_NAME1 , KON_NAME2)';

        awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_KON_KEY', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_KON_KEY'][0]), 300, $EditRecht, $con, $SQL, $OptionBitteWaehlen, '', '', '', '', '');
    }
    else
    {
        awis_FORM_Erstelle_TextFeld('KON_NAME1', $rsCAD['KON_NAME1'][0], 10, 350, false);
    }
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('CAD_NAME1')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_NAME1'] . ':', 150,$style);    
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_NAME1', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_NAME1'][0]), 50, 350,$EditRechtOPALFelder,$style);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('CAD_NAME2')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_NAME2'] . ':', 150,$style);
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_NAME2', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_NAME2'][0]), 50, 350,$EditRechtOPALFelder,$style);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('CAD_NAME3')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_NAME3'] . ':', 150,$style);
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_NAME3', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_NAME3'][0]), 50, 350,$EditRechtOPALFelder,$style);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    $styleStrasse = (PruefeObOpalUpdate('CAD_STRASSE')?'color:#FF0000':'');
    $styleHNR = (PruefeObOpalUpdate('CAD_HAUSNUMMER')?'color:#FF0000':'');
    $styleLabel = ($styleStrasse == 'color:#FF0000'?$styleStrasse:$styleHNR);
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_STRASSE'] . '/' . $AWISSprachKonserven['CAD']['CAD_HAUSNUMMER'] . ':', 150,$styleLabel);
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_STRASSE', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_STRASSE'][0]), 50, 330,$EditRechtOPALFelder,$styleStrasse);
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_HAUSNUMMER', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_HAUSNUMMER'][0]), 10, 90,$EditRechtOPALFelder,$styleHNR);
    awis_FORM_ZeileEnde();

    // InfoFeld Hausnummerzusatz
    $BindeVariablen = array();
    $SQL = InfoFeldSQLErstellen(4000,$AWIS_KEY1,'Detail',$BindeVariablen);
    $rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('INFO_HAUSNRZUSATZ')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
    awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$rsCIN['CIN_WERT'][0],$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],$EditRechtOPALFelder,$style,'');
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    if ($AWIS_KEY1 === 0)
    {
        $Parameter = array();
    }
    else
    {
        $Parameter = array('$NAME1' => urlencode($rsCAD['CAD_NAME1'][0]),
            '$NAME2' => urlencode($rsCAD['CAD_NAME2'][0]),
            '$STRASSE' => urlencode($rsCAD['CAD_STRASSE'][0]),
            '$HAUSNUMMER' => urlencode($rsCAD['CAD_HAUSNUMMER'][0]),
            '$PLZ' => urlencode($rsCAD['CAD_PLZ'][0]),
            '$ORT' => urlencode($rsCAD['CAD_ORT'][0]),
            '$LAN_CODE' => urlencode($rsCAD['CAD_LAN_CODE'][0])
        );
    }

    $stylePLZ = (PruefeObOpalUpdate('CAD_PLZ')?'color:#FF0000':'');
    $styleOrt = (PruefeObOpalUpdate('CAD_ORT')?'color:#FF0000':'');
    $styleLabel = ($stylePLZ == 'color:#FF0000'?$stylePLZ:$styleOrt);
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_LAN_CODE'] . '/' . $AWISSprachKonserven['CAD']['CAD_PLZ'] . '/' . $AWISSprachKonserven['CAD']['CAD_ORT'] . ':', 150,$styleLabel);
    $SQL = 'SELECT LAN_CODE, LAN_LAND FROM Laender ORDER BY LAN_LAND';
    awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_LAN_CODE', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_LAN_CODE'][0]), -40, $EditRecht, $con, $SQL, false, (($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_LAN_CODE'][0]) == '' ? 'DE' : ''), 'LAN_CODE', '', '', '');
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_PLZ', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_PLZ'][0]), 8, 80,$EditRechtOPALFelder,$stylePLZ);
    
    $Link = ($AWIS_KEY1 === 0 ? '' : strtr($INET_LINK_Karte, $Parameter));
    
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_ORT', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_ORT'][0]), 50, 400, $EditRechtOPALFelder, $styleOrt, '', $Link);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_ORTSTEIL'] . ':', 150);
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_ORTSTEIL', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_ORTSTEIL'][0]), 50, 400, $EditRecht);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('CAD_TELEFON')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_TELEFON'] . ':', 150,$style);
    $Link = strtr($INET_LINK_Telefon, $Parameter);
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_TELEFON', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_TELEFON'][0]), 25, 220, $EditRechtOPALFelder , $style, '', $Link);
    
    $style = (PruefeObOpalUpdate('CAD_TELEFAX')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_TELEFAX'] . ':', 120,$style);
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_TELEFAX', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_TELEFAX'][0]), 25, 300, $EditRechtOPALFelder, $style, '', '');
    awis_FORM_ZeileEnde();

    // InfoFeld Mobiltelefon
    $BindeVariablen = array();
    $SQL = InfoFeldSQLErstellen(4001,$AWIS_KEY1,'Detail',$BindeVariablen );
    $rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen );
    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('INFO_MOBILTEL')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
    awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$rsCIN['CIN_WERT'][0],$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],$EditRechtOPALFelder,$style,'');
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_WEBSEITE'] . ':', 150);
    $Link = '';
    if (($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_WEBSEITE'][0]) != '')
    {
        $Link = ($rsCAD['CAD_WEBSEITE'][0]);
    }
    awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_WEBSEITE', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_WEBSEITE'][0]), 100, 850, $EditRecht, '', '', $Link);
    awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_EMAIL'] . ':', 150);
    $Link = '';
    if (($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_EMAIL'][0]) != '')
    {
        $Link = ('mailto:' . $rsCAD['CAD_EMAIL'][0]);
    }
    awis_FORM_Erstelle_TextFeld('CAD_EMAIL', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_EMAIL'][0]), 60, 500, $EditRecht, '', '', $Link);
    awis_FORM_ZeileEnde();

    // InfoFeld Mail1 OPAL
    $BindeVariablen=array();
    $SQL = InfoFeldSQLErstellen(4009,$AWIS_KEY1,'Detail',$BindeVariablen );
    $rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen );
    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('INFO_EMAIL1')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
    awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$rsCIN['CIN_WERT'][0],$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],$EditRechtOPALFelder,$style);
    awis_FORM_ZeileEnde();

    // InfoFeld Mail2 OPAL
    $BindeVariablen=array();
    $SQL = InfoFeldSQLErstellen(4002,$AWIS_KEY1,'Detail',$BindeVariablen );
    $rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('INFO_EMAIL2')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
    awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$rsCIN['CIN_WERT'][0],$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],$EditRechtOPALFelder,$style);
    awis_FORM_ZeileEnde();

    // InfoFeld Mail3 OPAL
    $BindeVariablen=array();
    $SQL = InfoFeldSQLErstellen(4003,$AWIS_KEY1,'Detail',$BindeVariablen );
    $rsCIN = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
    awis_FORM_ZeileStart();
    $style = (PruefeObOpalUpdate('INFO_EMAIL3')?'color:#FF0000':'');
    awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][0] . ':',150,$style);
    awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][0].'_'.$rsCIN['CIN_KEY'][0],$rsCIN['CIN_WERT'][0],$rsCIN['CIT_ZEICHEN'][0],$rsCIN['CIT_BREITE'][0],$EditRechtOPALFelder,$style);
    awis_FORM_ZeileEnde();
      
    // Verbundenes Unternehmen
    if ($AWIS_KEY1 !== 0)
    {
        awis_FORM_ZeileStart();
        $Link = ($rsCAD['CAD_CAD_KEY'][0] == '' ? '' : './crm_Main.php?cmdAktion=Details&CAD_KEY=' . $rsCAD['CAD_CAD_KEY'][0] . '');
        awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_CAD_KEY'] . ':', 150, '', $Link, $AWISSprachKonserven['CAD']['ttt_CAD_VERBUND']);
        /*
          //$SQL = "SELECT CAD_KEY, CAD_NAME1 || ' ' || COALESCE(CAD_NAME2,'') AS CADNAME FROM CRMADRESSEN WHERE CAD_KON_KEY = 0".$rsCAD['CAD_KON_KEY'][0]." ORDER BY CAD_NAME1, CAD_NAME2";
          //$SQL = "SELECT CAD_KEY, CAD_NAME1 || ' ' || COALESCE(CAD_NAME2,'') AS CADNAME FROM CRMADRESSEN WHERE CAD_KON_KEY = 0".$rsCAD['CAD_KON_KEY'][0]." OR CAD_KEY = ".$rsCAD['CAD_CAD_KEY'][0]." ORDER BY CAD_NAME1, CAD_NAME2";
          $SQL = "SELECT DISTINCT CAD_KEY, CAD_NAME1 || ' ' || COALESCE(CAD_NAME2,'') AS CADNAME ";
          $SQL .= " FROM CRMADRESSEN ";
          $SQL .= " WHERE CAD_KON_KEY = 0".$rsCAD['CAD_KON_KEY'][0]. ($rsCAD['CAD_CAD_KEY'][0]==''?'':' OR CAD_KEY = '.$rsCAD['CAD_CAD_KEY'][0]);
          $SQL .= " ORDER BY CAD_NAME1, CAD_NAME2";
         */
        //awis_FORM_Erstelle_SelectFeld($TabellenKuerzel.'_CAD_KEY',($AWIS_KEY1===0?'':$rsCAD['CAD_CAD_KEY'][0]),-40,$EditRecht,$con,$SQL,false,'','CADNAME','',array('~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']),'');

        awis_FORM_Erstelle_TextFeld('*CAD_CAD_KEY', '', 10, 100, $EditRecht, '', 'background-color:#22FF22');
        $AktuelleDaten = ($AWIS_KEY1 === 0 ? '' : ($rsCAD['CAD_CAD_KEY'][0] == '' ? '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] : array($rsCAD['CAD_CAD_KEY'][0] . '~' . $rsCAD['VERBUND_FIRMA'][0])));
        if($EditRecht)
        {
        	awis_FORM_Erstelle_SelectFeld('CAD_CAD_KEY', (isset($rsCAD['CAD_CAD_KEY'][0]) ? $rsCAD['CAD_CAD_KEY'][0] : ''), 100, $EditRecht, $con, '*F*CAD_Daten;txtCAD_CAD_KEY;KON_KEY=' . $rsCAD['CAD_KON_KEY'][0] . '&ZUSATZ=~' . $AWISSprachKonserven['Wort']['txt_KeinVerbund'], (!isset($rsCAD['CAD_CAD_KEY'][0]) ? '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'] : ''), '', '', '', $AktuelleDaten, '', '', '', '');
        }
        else
        {
        	
	        //$SQL = "SELECT CAD_KEY, CAD_NAME1 || ' ' || COALESCE(CAD_NAME2,'') AS CADNAME FROM CRMADRESSEN WHERE CAD_KON_KEY = 0".$rsCAD['CAD_KON_KEY'][0]." ORDER BY CAD_NAME1, CAD_NAME2";
	        //$SQL = "SELECT CAD_KEY, CAD_NAME1 || ' ' || COALESCE(CAD_NAME2,'') AS CADNAME FROM CRMADRESSEN WHERE CAD_KON_KEY = 0".$rsCAD['CAD_KON_KEY'][0]." OR CAD_KEY = ".$rsCAD['CAD_CAD_KEY'][0]." ORDER BY CAD_NAME1, CAD_NAME2";
	        $SQL = "SELECT DISTINCT CAD_KEY, CAD_NAME1 || ' ' || COALESCE(CAD_NAME2,'') AS CADNAME ";
	        $SQL .= " FROM CRMADRESSEN ";
	        $SQL .= " WHERE CAD_KON_KEY = 0".$rsCAD['CAD_KON_KEY'][0]. ($rsCAD['CAD_CAD_KEY'][0]==''?'':' OR CAD_KEY = '.$rsCAD['CAD_CAD_KEY'][0]);
	        $SQL .= " ORDER BY CAD_NAME1, CAD_NAME2";
	        
	        awis_FORM_Erstelle_SelectFeld($TabellenKuerzel.'_CAD_KEY',($AWIS_KEY1===0?'':$rsCAD['CAD_CAD_KEY'][0]),-40,$EditRecht,$con,$SQL,false,'','CADNAME','',array('~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']),'');
        }
        awis_FORM_ZeileEnde();
        
        awis_FORM_ZeileStart();
        awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_VERBUNDHINWEIS'] . ':', 150);
        awis_FORM_Erstelle_TextFeld($TabellenKuerzel . '_VERBUNDHINWEIS', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_VERBUNDHINWEIS'][0]), 50, 400, $EditRecht);
        awis_FORM_ZeileEnde();
        
    }

    awis_FORM_ZeileStart();
    awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_BEMERKUNG'] . ':', 150);
    awis_FORM_Erstelle_Textarea('CAD_BEMERKUNG', ($AWIS_KEY1 === 0 ? '' : $rsCAD['CAD_BEMERKUNG'][0]), 300, 80, 4, (($Recht3700 & 16384) != 0), 'border-style=solid;');
    awis_FORM_ZeileEnde();

    awis_FORM_FormularEnde();


    if (!isset($_POST['cmdDSNeu_x']))
    {
        $RegisterSeite = (isset($_GET['Seite']) ? $_GET['Seite'] : (isset($_POST['Seite']) ? $_POST['Seite'] : ''));
        if ($RegisterSeite == '')
        {
            $RegisterSeite = awis_BenutzerParameter($con, 'CRM: Standardregister', $AWISBenutzer->BenutzerName());
        }

        echo '<input type=hidden name=Seite value=' . awisFeldFormat('T', $RegisterSeite, 'DB', false) . '>';

        awis_RegisterErstellen(3701, $con, $RegisterSeite);
    }


    //***************************************
    // Schaltfl???chen f???r dieses Register
    //***************************************
    awis_FORM_SchaltflaechenStart();
    if (($Recht3700 & (2 + 4 + 256)) !== 0)  //
    {
        awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }
    if (($Recht3700 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf???gen erlaubt?
    {
        awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    if (($Recht3700 & 8) !== 0 AND !isset($_POST['cmdDSNeu_x']))
    {
        awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
    }

    awis_FORM_Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/pfeil_links.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
    awis_FORM_Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/pfeil_rechts.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');

    awis_FORM_Schaltflaeche('image', 'cmdUpdBest', '', '/bilder/sc_daumen.png', $AWISSprachKonserven['Wort']['lbl_AaenderungInfoLoeschen'], '.');

    awis_FORM_SchaltflaechenEnde();

    echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsCAD, $_POST, $rsAZG, $SQL, $AWISSprache);

if ($CursorFeld != '')
{
    echo '<Script Language=JavaScript>';
    echo "document.getElementsByName(\"" . $CursorFeld . "\")[0].focus();";
    echo '</Script>';
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;

    $Bedingung = '';
    $Param = explode(';', $Param);

    if ($AWIS_KEY1 > 0)
    {
        $Bedingung = 'AND CAD_KEY = ' . $AWIS_KEY1;
        return $Bedingung;
    }
    // Name angegeben?
    if ($Param[0] != '')  // Key
    {
        $Bedingung .= 'AND CAD_Key = ' . intval($Param[0]) . ' ';
    }
    if (isset($Param[1]) AND $Param[1] != '')
    {
        $Bedingung .= 'AND (UPPER(CAD_Name1) ' . awisLIKEoderIST($Param[1] . '%', 1) . ' ';
        $Bedingung .= 'OR  UPPER(CAD_Name2) ' . awisLIKEoderIST($Param[1] . '%', 1) . ' ';
        $Bedingung .= ')';
    }
    if (isset($Param[2]) AND $Param[2] != '')  // PLZ
    {
        $Bedingung .= 'AND CAD_PLZ ' . awisLikeOderIst($Param[2], 1) . ' ';
    }
    if (isset($Param[3]) AND $Param[3] != '')  // Ort
    {
        $Bedingung .= 'AND UPPER(CAD_ORT) ' . awisLIKEoderIST($Param[3], 1) . ' ';
    }
    if (isset($Param[4]) AND $Param[4] != '')  // Betreuer
    {
        if ($Param[4] != '99999')
        {
            $Bedingung .= 'AND CAD_KON_KEY=' . awisFeldFormat('Z', $Param[4], 'DB', true) . '';
        }
        else
        {
            $Bedingung .= 'AND CAD_KON_KEY IS NULL ';
        }
    }
    if (isset($Param[5]) AND $Param[5] != '')  // Strasse
    {
        $Bedingung .= 'AND UPPER(CAD_STRASSE) ' . awisLikeoderIst($Param[5], 1) . '';
    }
    if (isset($Param[6]) AND $Param[6] != '')  // Strasse
    {
        $Bedingung .= 'AND CAD_CGB_KEY = ' . intval($Param[6]) . '';
    }
    if (isset($Param[7]) AND $Param[7] != '')  // Vertragsnummer
    {
        $Bedingung .= 'AND EXISTS ( ';
        $Bedingung .= ' SELECT * FROM CRMAKQUISESTAND ';
        $Bedingung .= ' WHERE (CAQ_VERTRAGSNUMMER ' . awisLIKEoderIST($Param[7]);
        $Bedingung .= ' OR CAQ_VERBANDNR ' . awisLIKEoderIST($Param[7]) . ')';
        $Bedingung .= ' AND CAQ_CAD_KEY = CRMAdressen.CAD_KEY';
        $Bedingung .= ')';
    }

    if (isset($Param[8]) AND $Param[8] != '')  // Kontaktdaten
    {
        $Bedingung .= 'AND (EXISTS ( ';
        $Bedingung .= ' SELECT CAK_KEY FROM CRMANSPRECHPARTNERKONTAKTE ';
        $Bedingung .= ' INNER JOIN CRMAnsprechpartner ON CAK_CAP_KEY = CAP_KEY';
        $Bedingung .= ' WHERE CAK_WERT ' . awisLIKEoderIST($Param[8]);
        $Bedingung .= ' AND CAP_CAD_KEY = CRMAdressen.CAD_KEY';
        $Bedingung .= ') ';
        $Bedingung .= ' OR CAD_TELEFON ' . awisLIKEoderIST($Param[8]);
        $Bedingung .= ' OR CAD_TELEFAX ' . awisLIKEoderIST($Param[8]);
        $Bedingung .= ' OR CAD_EMAIL ' . awisLIKEoderIST($Param[8]);
        $Bedingung .= ')';
    }

    if (isset($Param[9]) AND $Param[9] != '')  // Land
    {
        $Bedingung .= 'AND CAD_LAN_CODE = ' . awis_FeldInhaltFormat('T', $Param[9]);
    }

    if (isset($Param[10]) AND $Param[10] != '')  // Status
    {
        $Bedingung .= 'AND CAD_STATUS = ' . awis_FeldInhaltFormat('T', $Param[10]);
    }

    if (isset($Param[11]) AND $Param[11] != '')  // Importquelle
    {
        $Bedingung .= 'AND CAD_IMQ_ID = ' . awis_FeldInhaltFormat('N0', $Param[11]);
    }

    if (isset($Param[12]) AND $Param[12] != '')  // Ranking
    {
        $Bedingung .= 'AND CAD_RANKING = ' . awis_FeldInhaltFormat('N0', $Param[12]);
    }

    if (isset($Param[13]) AND $Param[13] != '')  // Importkennung
    {
        $Bedingung .= 'AND EXISTS(SELECT * ';
        $Bedingung .= ' FROM CRMINFOS WHERE CIN_WERT = ' . awis_FeldInhaltFormat('T', $Param[13]);
        $Bedingung .= ' AND CIN_CIT_ID = 3006 AND CIN_CAD_KEY = CAD_KEY )';
    }

    return $Bedingung;
}

/**
 * Baut den SQL zusammen der den Wert des Infofeldes ermittelt und gibt
 * den SQL zurueck.
 * @param int $InfoFeldID
 * @param int $AWIS_KEY1
 * @param string $infoTypSeite
 * @return string
 */
function InfoFeldSQLErstellen($InfoFeldID,$AWIS_KEY1,$infoTypSeite,&$BindeVariablen)
{
    // Infofelder Laden
    $SQL = 'SELECT * ';
    $SQL .= ' FROM CRMInfoTypen ';
    $SQL .= ' LEFT OUTER JOIN CRMInfos ON CIT_ID = CIN_CIT_ID AND CIN_CAD_KEY=:var_N0_CIN_CAD_KEY';
    $SQL .= ' WHERE CIT_SEITE = :var_T_CIT_SEITE';
    $SQL .= ' AND CIT_STATUS=\'A\'';
    $SQL .= ' AND CIT_ID = :var_N0_CIT_ID';

    $BindeVariablen['var_N0_CIN_CAD_KEY']=$AWIS_KEY1;
    $BindeVariablen['var_T_CIT_SEITE']=$infoTypSeite;
    $BindeVariablen['var_N0_CIT_ID']=$InfoFeldID;
    return $SQL;
}

/**
 * Ermittelt die Daten anhand derer geprueft werden kann
 * ob das Feld durch Updateprozess geaendert wurde
 * @param int    $AWIS_KEY1
 * @param DB-Con $con
 * @param string $FeldName
 * @param string $Vertragsnr
*/
function HoleOPALUpdateDaten($AWIS_KEY1,$con,$Vertragsnr = '')
{
	global $awisRSZeilen;
	global $updOPAL;
	
	if($Vertragsnr != '' || $AWIS_KEY1 != '')
	{
		$SQL = 'SELECT * ';
		$SQL .= ' FROM crminfos ';
		$BindeVariablen = array();
		
		if($Vertragsnr != '')
		{
			$SQL .= ' INNER JOIN crmadressen ON cin_cad_key = cad_key ';
			$SQL .= ' INNER JOIN crmakquisestand ON cad_key = caq_cad_key ';
			$SQL .= ' WHERE caq_vertragsnummer = :var_T_caq_vertragsnummer';
			$BindeVariablen['var_T_caq_vertragsnummer']=$Vertragsnr;
		}
		if($AWIS_KEY1 != '')
		{
			$SQL .= ' WHERE cin_cad_key = :var_N0_cin_cad_key';
			$BindeVariablen['var_N0_cin_cad_key']=$AWIS_KEY1;
		}
		$SQL .= ' AND cin_cit_id = :var_N0_cin_cit_id';
		$BindeVariablen['var_N0_cin_cit_id']=4004;
		
		$updOPAL = awisOpenRecordset($con, $SQL,'','', $BindeVariablen);
	}
}



/**
 * Gibt zurueck ob fuer uebergebenes Feld ein Update-Eintrag vorhanden ist
 * @param string $feldname DEFAULT 4004 wenn nicht nach speziellen Feld geprueft werden soll
 * @return boolean
 */
function PruefeObOpalUpdate($feldname='4004')
{
	global $updOPAL;
	
	if(is_array($updOPAL))
	{
		if($feldname != '4004')
		{
			return in_array($feldname, $updOPAL['CIN_WERT']);
		}
		else
		{
			return in_array($feldname, $updOPAL['CIN_CIT_ID']);
		}	
	}
}