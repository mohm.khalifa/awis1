<?php
global $con;
global $Recht3708;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CGB','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3708 = awisBenutzerRecht($con,3708,$AWISBenutzer->BenutzerName());
if($Recht3708==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$Bedingung = '';


if(isset($_POST['cmdSpeichern_x']))
{
	include('./crm_speichern_stammdaten.php');
}


$EditRecht = ($Recht3708&6);

awis_FORM_FormularStart();


awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('Text Preisliste Sommer:',250);
$SQL = 'SELECT XHW_WERTTXT ';
$SQL .= ' FROM Hilfswerte';
$SQL .= ' WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID = 1';
$rsXHW = awisOpenRecordset($con,$SQL);
awis_FORM_Erstelle_TextFeld('XHW_1',$rsXHW['XHW_WERTTXT'][0],20,350,$EditRecht);
if($CursorFeld=='')
{
	$CursorFeld='txtXHW_1';
}
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('Text Preisliste Winter:',250);
$SQL = 'SELECT XHW_WERTTXT ';
$SQL .= ' FROM Hilfswerte';
$SQL .= ' WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID = 2';
$rsXHW = awisOpenRecordset($con,$SQL);
awis_FORM_Erstelle_TextFeld('XHW_2',$rsXHW['XHW_WERTTXT'][0],20,350,$EditRecht);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('Preise g�ltig ab:',250);
$SQL = 'SELECT XHW_WERTDAT ';
$SQL .= ' FROM Hilfswerte';
$SQL .= ' WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID = 3';
$rsXHW = awisOpenRecordset($con,$SQL);
awis_FORM_Erstelle_TextFeld('XHW_3',$rsXHW['XHW_WERTDAT'][0],10,350,$EditRecht);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel('Preise g�ltig bis:',250);
$SQL = 'SELECT XHW_WERTDAT ';
$SQL .= ' FROM Hilfswerte';
$SQL .= ' WHERE XHW_BEREICH = \'CRM_TEXT\' AND XHW_ID = 4';
$rsXHW = awisOpenRecordset($con,$SQL);
awis_FORM_Erstelle_TextFeld('XHW_4',$rsXHW['XHW_WERTDAT'][0],10,350,$EditRecht);
awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();
/*

//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM Textkonserven';
$SQL .= ' WHERE (XTX_KENNUNG = \'CRM_ZusatzText_Sommer\' OR XTX_KENNUNG = \'CRM_ZusatzText_Winter\') AND XTX_Bereich =\'CKD\'';

$rsCGB = awisOpenRecordset($con, $SQL);
$rsCGBZeilen = $awisRSZeilen;
awis_Debug(1,$_POST,$_GET, $SQL, $rsCGBZeilen);
//********************************************************
// Daten anzeigen
//********************************************************
echo '<form name=frmCRMGebiete action=./crm_Main.php?cmdAktion=Stammdaten&Seite=Gebiete method=POST  enctype="multipart/form-data">';
$AWIS_KEY1 = (isset($rsCGB['CGB_KEY'][0])?$rsCGB['CGB_KEY'][0]:0);

//echo '<input type=hidden name=txtCGB_KEY value='.$AWIS_KEY1. '>';

awis_FORM_FormularStart();
$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

$EditRecht=(($Recht3708&2)!=0);

if($AWIS_KEY1==0)
{
	$EditRecht=true;
}

for($CGBZeile=0;$CGBZeile<$rsCGBZeilen;$CGBZeile++)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($rsCGB['XTX_BEMERKUNG'][$CGBZeile].':',250);
	awis_FORM_Erstelle_TextFeld('XTX_KENNUNG_'.$rsCGB['XTX_KENNUNG'][$CGBZeile],$rsCGB['XTX_TEXT_DE'][$CGBZeile],50,350,$EditRecht);
	if($CursorFeld=='')
	{
		$CursorFeld='XTX_KENNUNG_'.$rsCGB['XTX_KENNUNG'][$CGBZeile];
	}


	awis_FORM_ZeileEnde();
}

awis_FORM_FormularEnde();
*/

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
awis_FORM_SchaltflaechenStart();
if(($Recht3708&(2+4+256))!==0)		//
{
	awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}

awis_FORM_SchaltflaechenEnde();

echo '</form>';

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}
?>