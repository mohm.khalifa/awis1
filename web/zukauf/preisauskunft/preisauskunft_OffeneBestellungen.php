<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUB','%');
	$TextKonserven[]=array('ZLH','%');
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('ZUL','%');
	$TextKonserven[]=array('AST','%');
	$TextKonserven[]=array('FIL','FIL_ID');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_200038'));

	// TODO: Zum testen wg. Gro�-/Klein
	//$DB->Ausfuehren("ALTER SESSION SET NLS_COMP=LINGUISTIC");
	//$DB->Ausfuehren("ALTER SESSION SET NLS_SORT=BINARY_CI");
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10015 = $AWISBenutzer->HatDasRecht(10015);
	if($Recht10015==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Recht10001 = $AWISBenutzer->HatDasRecht(10001);           // Zukaufartikelstamm
	$Recht5000 = $AWISBenutzer->HatDasRecht(5000);             // Filialinfo
	
	$AWISWerkzeug = new awisWerkzeuge();
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSpeichern_x']))
	{
		include('./preisauskunft_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_200038'));
	}
	elseif(isset($_GET['ZUB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZUB_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_200038'));
	}
	
    echo '<form name="frmZukaufBestellungen" action="./preisauskunft_Main.php?cmdAktion=Offene" method="POST">';

	$Param['FIL_ID']=(isset($_POST['sucFIL_ID'])?$_POST['sucFIL_ID']:$Param['FIL_ID']);
	if($Param['FIL_ID']=='')
	{
		$Param['FIL_ID']=90;
	}
	$Param['ORDERBY']='zub_key DESC';
	$Param['BLOCK']=(isset($_REQUEST['Block'])?$_REQUEST['Block']:1);

	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

	if(((int)$AWIS_KEY1)<=0)
	{
    	if($FilZugriff!='')
    	{
    		if(strpos($FilZugriff,',')!==false)
    		{
    			$Form->ZeileStart();
    			$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',240);
    			$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
    			$SQL .= ' FROM Filialen ';
    			$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
    			$SQL .= ' ORDER BY FIL_BEZ';
    			$Form->Erstelle_SelectFeld('*FIL_ID',$Param['FIL_ID'],150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
    			$Form->ZeileEnde();
    			$AWISCursorPosition='sucFIL_ID';
    		}
    		else
    		{
    			$Param['FIL_ID']=$FilZugriff;
    		}
    	}
    	else
    	{
    		$Form->ZeileStart();
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',240);
    		$Form->Erstelle_TextFeld('*FIL_ID',$Param['FIL_ID'],10,180,true,'','','','T','L','','',10);
    		$Form->ZeileEnde();
    		$AWISCursorPosition='sucFIL_ID';
    	}
	}
	
	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDERBY']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDERBY'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZUB_BESTELLDATUM DESC';
			$Param['ORDERBY']='ZUB_BESTELLDATUM DESC';
		}
	}
	else
	{
		$Param['ORDERBY']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDERBY'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	
	if($AWIS_KEY1>0)
	{
	    $Bedingung = ' AND ZUB_KEY = '.$DB->WertSetzen('ZUB', 'N0', $AWIS_KEY1);
	}
	else
	{
    	$Bedingung = ' AND ZUB_FIL_ID = '.$DB->WertSetzen('ZUB', 'N0', $Param['FIL_ID']);
    	$Bedingung .= ' AND NOT EXISTS(SELECT * FROM ZUKAUFWERKSTATTAUFTRAEGE WHERE ZWA_ZUB_KEY = ZUB_KEY)';
    	$Bedingung .= ' AND ZUB_BESTELLMENGE > 0';
    	$Bedingung .= ' AND ZUB_ABSCHLUSSART >= 1';
	}
	$SQL = 'SELECT Zukaufbestellungen.*, ZLH_KUERZEL, ZLH_BEZEICHNUNG, ZLA_AST_ATUNR, FIL_BEZ, LIE_NAME1, ZHK_CODE';
	$SQL .= ', LIE_NAME2';
	$SQL .= ', (SELECT COUNT(*) FROM Zukauflieferscheine WHERE ZUL_ZUB_KEY = ZUB_KEY) AS AnzLieferscheine';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukaufbestellungen';
	$SQL .= ' LEFT OUTER JOIN ZukaufArtikel ON ZUB_ZLA_KEY = ZLA_KEY';
	$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ' LEFT OUTER JOIN FILIALEN ON FIL_ID = ZUB_FIL_ID';
	$SQL .= ' LEFT OUTER JOIN LIEFERANTEN ON ZUB_LIE_NR = LIE_NR';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFHERSTELLERCODES ON ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = ZUB_LIE_NR AND ZUB_HERSTELLER = ZHK_CODE';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	//$Form->DebugAusgabe(1,$SQL);

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_200038',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('ZUB',false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= ' '.$ORDERBY;

    //$Form->DebugAusgabe(3,$Param,$_GET,$_POST,$AWIS_KEY1,$FilZugriff);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsZUB = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('ZUB'));

	
	$AWISBenutzer->ParameterSchreiben('Formular_200038',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************

	if($rsZUB->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif(($rsZUB->AnzahlDatensaetze()>1 AND !isset($_GET['ZUB_KEY']) OR isset($_REQUEST['Block'])))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './preisauskunft_Main.php?cmdAktion=Offene'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_BESTELLDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_BESTELLDATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'],165,'',$Link);
		$Link = './preisauskunft_Main.php?cmdAktion=Offene'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_WANR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_WANR'],100,'',$Link);
		$Link = './preisauskunft_Main.php?cmdAktion=Offene'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_BESTELLMENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_BESTELLMENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'],60,'',$Link);
		$Link = './preisauskunft_Main.php?cmdAktion=Offene'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_ARTIKELNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'],170,'',$Link);
		$Link = './preisauskunft_Main.php?cmdAktion=Offene'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],60,'',$Link);
		$Link = './preisauskunft_Main.php?cmdAktion=Offene'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUB_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_ARTIKELBEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_ARTIKELBEZEICHNUNG'],350,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_LIE_NR'],120);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZUB->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './preisauskunft_Main.php?cmdAktion=Offene&ZUB_KEY=0'.$rsZUB->FeldInhalt('ZUB_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZUB_BESTELLDATUM',$rsZUB->FeldInhalt('ZUB_BESTELLDATUM'),0,165,false,($DS%2),'',$Link,'DU');

			if($rsZUB->FeldInhalt('ZUB_WANRKORR')=='')
			{
				$WAKorrekt =  $AWISWerkzeug->BerechnePruefziffer($rsZUB->FeldInhalt('ZUB_WANR'),awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN);
				$Form->Erstelle_ListenFeld('ZUB_WANR',$rsZUB->FeldInhalt('ZUB_WANR'),0,100,false,($WAKorrekt==false?2:($DS%2)),'','','T');
			}
			else
			{
				$WAKorrekt =  $AWISWerkzeug->BerechnePruefziffer($rsZUB->FeldInhalt('ZUB_WANRKORR'),awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN);
				$Form->Erstelle_ListenFeld('ZUB_WANR',$rsZUB->FeldInhalt('ZUB_WANRKORR'),0,100,false,($WAKorrekt==false?2:($DS%2)),($WAKorrekt==false?'color:white;':'color:red;'),'','T','',$rsZUB->FeldInhalt('ZUB_WANR'));
			}
			$Form->Erstelle_ListenFeld('ZUB_BESTELLMENGE',$rsZUB->FeldInhalt('ZUB_BESTELLMENGE'),0,60,false,($DS%2),'','','T','L');
			$Link='';
			if(($Recht10001&3)!=0)
			{
                $Link = '../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUB->FeldInhalt('ZUB_ZLA_KEY');
			}
			$Form->Erstelle_ListenFeld('ZUB_ARTIKELNUMMER',$rsZUB->FeldInhalt('ZUB_ARTIKELNUMMER'),0,170,false,($DS%2),'',$Link);
			$Link='';	// TODO: Sp�ter auf die Hersteller
			$Form->Erstelle_ListenFeld('ZLH_KUERZEL',$rsZUB->FeldInhalt('ZLH_KUERZEL'),0,60,false,($DS%2),'',$Link,'T','L',$rsZUB->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Form->Erstelle_ListenFeld('ZUB_ARTIKELBEZEICHNUNG',$rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG'),0,350,false,($DS%2),'',$Link,'T','L');

			$Form->Erstelle_ListenFeld('LIE_NAME1',$rsZUB->FeldInhalt('LIE_NAME1'),0,120,false,($DS%2),'',$Link,'T','R',$rsZUB->FeldInhalt('LIE_NAME1'));

			$Form->ZeileEnde();

			$rsZUB->DSWeiter();
			$DS++;
		}

		$Link = './preisauskunft_Main.php?cmdAktion=Offene';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZUB->FeldInhalt('ZUB_KEY');
		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_200038',serialize($Param));

		echo '<input type=hidden name=txtZUB_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./preisauskunft_Main.php?cmdAktion=Offene&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUB->FeldInhalt('ZUB_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUB->FeldInhalt('ZUB_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10015&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10015&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_LIE_NR'].':',200);
		if($EditRecht)
		{
			$AWISCursorPosition='txtZUB_LIE_NR';
		}
		$SQL = 'SELECT LIE_NR, LIE_NAME1 ';
		$SQL .= ' FROM LIEFERANTEN';
		$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('ZUB_LIE_NR',$rsZUB->FeldInhalt('ZUB_LIE_NR'),500,false,$SQL);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_EXTERNEID'].':',200);
		$Form->Erstelle_TextFeld('ZUB_EXTERNEID',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_EXTERNEID')),20,200,false,'','','','T');
		$Form->ZeileEnde();

			// Bestellinformationen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'].':',200);
		$Form->Erstelle_TextFeld('ZUB_BESTELLDATUM',$rsZUB->FeldInhalt('ZUB_BESTELLDATUM'),20,200,false,'','','','DU');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_FIL_ID'].':',190);
		$Form->Erstelle_TextFeld('ZUB_FIL_ID',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_FIL_ID')),20,200,false,'','','','T');
		$Link='';
		if(($Recht5000&3)!=0)
		{
            $Link = ($rsZUB->FeldInhalt('ZUB_FIL_ID')==''?'':'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsZUB->FeldInhalt('ZUB_FIL_ID'));
		}
		$Form->Erstelle_TextFeld('#FIL_BEZ',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('FIL_BEZ')),20,200,false,'','',$Link,'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANR'].':',200);
	 	$Form->Erstelle_TextFeld('ZUB_WANR',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_WANR')),20,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANRKORR'].':',190);
	 	$Form->Erstelle_TextFeld('ZUB_WANRKORR',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_WANRKORR')),20,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_VERSANDARTBEZ'].':',200);
		$Form->Erstelle_TextFeld('ZUB_VERSANDARTBEZ',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_VERSANDARTBEZ')),20,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'].':',200);
		$Form->Erstelle_TextFeld('ZUB_BESTELLMENGE',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_BESTELLMENGE')),10,200,false,'','','','N3');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_MENGENEINHEITEN'].':',190);
		$Form->Erstelle_TextFeld('ZUB_MENGENEINHEITEN',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_MENGENEINHEITEN')),10,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_EINHEITENPROPREIS'].':',190);
		$Form->Erstelle_TextFeld('ZUB_EINHEITENPROPREIS',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_EINHEITENPROPREIS')),10,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'].':',200);
		$Form->Erstelle_TextFeld('ZUB_PREISBRUTTO',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_PREISBRUTTO')),10,200,false,'','','','N2');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ALTTEILWERT'].':',190);
		$Form->Erstelle_TextFeld('ZUB_ALTTEILWERT',$rsZUB->FeldInhalt('ZUB_ALTTEILWERT'),10,200,false,($rsZUB->FeldInhalt('ZUB_ALTTEILWERT')>0?'color:red;':''),'','','N2');
		$Form->ZeileEnde();

			// Nummer und Hersteller
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'].':',200,'');
		$Form->Erstelle_TextFeld('ZUB_ARTIKELNUMMER',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_ARTIKELNUMMER')),20,200,false);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLH_KEY'].':',190);
		$Form->Erstelle_TextFeld('HERSTELLER',$rsZUB->FeldInhalt('ZHK_CODE').' - '.$rsZUB->FeldInhalt('ZLH_BEZEICHNUNG'),20,300,false);
		$Form->ZeileEnde();

			// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELBEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('ZUB_ARTIKELBEZEICHNUNG',($AWIS_KEY1===0?'':$rsZUB->FeldInhalt('ZUB_ARTIKELBEZEICHNUNG')),50,400,false);
		$Form->ZeileEnde();

		if($rsZUB->FeldInhalt('ZLA_AST_ATUNR')!='')
		{
			$Form->Trennzeile();

			$SQL = 'SELECT Artikelstamm.* ';
			$SQL .= ',(SELECT ATW_BETRAG FROM Altteilwerte WHERE ATW_KENNUNG = (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND asi_ast_atunr=\''.$rsZUB->FeldInhalt('ZLA_AST_ATUNR').'\' AND asi_ait_id=190) AND ATW_LAN_CODE=\'DE\') AS AST_ALTTEILWERT';
			$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND  asi_ast_atunr=\''.$rsZUB->FeldInhalt('ZLA_AST_ATUNR').'\' AND asi_ait_id=191) AS AST_VPE';
			$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR='.$DB->FeldInhaltFormat('T',$rsZUB->FeldInhalt('ZLA_AST_ATUNR'));
			$rsAST = $DB->RecordSetOeffnen($SQL);
			$Recht450=$AWISBenutzer->HatDasRecht(450);
			if(!$rsAST->EOF())
			{
				$Form->ZeileStart();
				// RECHTE
				$Link='';
				if(($Recht450&3)!==0)
				{
    				$Link = '/artikelstamm/artikelstamm_Main.php?cmdAktion=Artikelinfo&ATUNR='.$rsAST->FeldInhalt('AST_ATUNR');
				}
				$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'');
				$Form->Erstelle_TextFeld('AST_ATUNR',$rsAST->FeldInhalt('AST_ATUNR'),20,200,false,'','',$Link);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':',200);
				$Form->Erstelle_TextFeld('AST_BEZEICHNUNGWW',$rsAST->FeldInhalt('AST_BEZEICHNUNGWW'),20,500,false);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VPE'].':',200);
				$Form->Erstelle_TextFeld('AST_VPE',$rsAST->FeldInhalt('AST_VPE'),20,200,false,'','','','N0');
				$Form->ZeileEnde();
			}
		}

		$Form->Trennzeile();
		
		// Lieferscheine suchen
		$SQL = 'SELECT Zukauflieferscheine.*';
		$SQL .= ' , ZAL_BEZEICHNUNG';
		$SQL .= ' FROM zukauflieferscheine ';
		$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAL_KEY = ZUL_ZAL_KEY';
		$SQL .= ' WHERE ZUL_ZUB_KEY = '.$DB->WertSetzen('ZUL','N0',$AWIS_KEY1,false);
		$rsZUL = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZUL'));
		
		$Link='';
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_DATUM'],120,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_EXTERNEID'],250,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_LIEFERSCHEINNR'],150,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_LIEFERMENGE'],150,'',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_VKPR'],150,'',$Link);
		
		$Form->ZeileEnde();

		$Recht10002 = $AWISBenutzer->HatDasRecht(10002);
		$PEBZeile=0;
		$DS=0;
		while(!$rsZUL->EOF())
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_ListenFeld('#ZUL_DATUM',$rsZUL->FeldInhalt('ZUL_DATUM'),0,120,false,($PEBZeile%2),'','','D');
		    $Link='';
		    if(($Recht10002&3)!==0)
		    {
		      $Link = '../zukauflieferscheine/zukauflieferscheine_Main.php?cmdAktion=Details&ZUL_KEY='.$rsZUL->FeldInhalt('ZUL_KEY');
		    }
		    $Form->Erstelle_ListenFeld('#ZUL_EXTERNEID',$rsZUL->FeldInhalt('ZUL_EXTERNEID'),0,250,false,($PEBZeile%2),'','','T','','');
		    $Form->Erstelle_ListenFeld('#ZUL_LIEFERSCHEINNR',$rsZUL->FeldInhalt('ZUL_LIEFERSCHEINNR'),0,150,false,($PEBZeile%2),'',$Link,'T','','');
		    $Form->Erstelle_ListenFeld('#ZUL_LIEFERMENGE',$rsZUL->FeldInhalt('ZUL_LIEFERMENGE'),0,150,false,($PEBZeile%2),'','','Nx','','');
		    $Form->Erstelle_ListenFeld('#ZUL_VKPR',$rsZUL->FeldInhalt('ZUL_VKPR'),0,150,false,($PEBZeile%2),'','','N2');
		    $Form->ZeileEnde();
		
		    $rsZUL->DSWeiter();
		    $PEBZeile++;
		}

		$Form->Formular_Ende();
	}

	//awis_Debug(1, $Param, $Bedingung, $rsZUB, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	
	if(($Recht10015&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	else
	{
	    $Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		//$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"180112180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"180112180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>