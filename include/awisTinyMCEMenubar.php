<?php

/**
 * Klasse zum erzeugen der TinyMCE Men�bar (Dropdowns)
 *
 * Class awisTinyMCEMenubar
 */
class awisTinyMCEMenubar
{

    /**
     * @var string
     */
    private $_Menubar = '';

    /**
     * Array mit den Einzelnen Kontextmenus die geladen werden sollen
     *
     * @var array
     */
    private $_MenubarItems = array();

    /**
     * awisDatenbank
     *
     * @var awisDatenbank
     */
    private $_DB;

    /**
     * awisTinyMCEMenubar constructor.
     */
    function __construct($DB)
    {
        $this->_DB = $DB;
    }

    public function MitMailPlatzhalter($MVT_GRUPPE = '')
    {
        $SQL = ' SELECT MVP_LABEL, MVP_BESCHREIBUNG, MVP_PLATZHALTER ';
        $SQL .= ' FROM MAILPLATZHALTER ';
        $SQL .= ' WHERE MVP_MVT_GRUPPE = ' . $this->_DB->WertSetzen('MVT', 'T', $MVT_GRUPPE);

        $rsPlatzhalter = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('MVT'));

        $Platzhalter = array();
        while (!$rsPlatzhalter->EOF()) {
            $Platzhalter[] = array('Label' => $rsPlatzhalter->FeldInhalt('MVP_LABEL'), 'Text' => $rsPlatzhalter->FeldInhalt('MVP_PLATZHALTER'));
            $rsPlatzhalter->DSWeiter();
        }

        if (count($Platzhalter) > 0) {
            $this->MitTextbausteinen($Platzhalter);
        }

    }

    /**
     * Erzeugt ein Men�, mitwelchem der User Textbausteine ausw�hlen kann.
     *
     * @param array $Textbausteine Multidimesionales Array nach folgenden Schema: array(array('Label' => 'Beschriftung des Men�eintrags','Text' => 'Baustein welcher eingef�gt wird'))
     */
    public function MitTextbausteinen($Textbausteine = array(array('Label' => 'Beschriftung des Men�eintrags', 'Text' => 'Baustein welcher eingef�gt wird')))
    {
        $i = 0;
        $this->_MenubarItems[] = 'tools';
        $this->_Menubar .= ',setup: function(editor) { ';
        foreach ($Textbausteine as $Textbaustein) {
            $this->_Menubar .= 'editor.addMenuItem(\'myitem' . $i++ . '\', {';
            $this->_Menubar .= 'text: \'' . $Textbaustein['Label'] . '\',';
            $this->_Menubar .= 'context: \'tools\', ';
            $this->_Menubar .= 'onclick: function() {';
            $this->_Menubar .= ' editor.insertContent("' . $Textbaustein['Text'] . '");';
            $this->_Menubar .= ' }';
            $this->_Menubar .= '  });';
        }

        $this->_Menubar .= '  },';
    }

    /**
     * Liefert den Code f�r die Menubar. (Framework intern)
     *
     * @return string
     */
    public function getMenubar()
    {
        $return = ',menubar:"';

        $this->_MenubarItems = array_unique($this->_MenubarItems);

        foreach ($this->_MenubarItems as $item) {
            $return .= ' ' . $item . ' ';
        }

        $return .= '"';
        $return .= $this->_Menubar;
        return $return;
    }

    /**
     * F�gt der Menubar die AWISStandardelemente hinzu
     * Bearbeiten, Ansicht, Format, Einf�gen
     */
    public function MitStandardfunktionen()
    {
        $this->MitBearbeiten();
        $this->MitAnsicht();
        $this->MitFormat();
        $this->MitInsert();
    }


    /**
     * F�gt der Menubar das "Bearbeiten" Kontextmen� hinzu
     */
    public function MitBearbeiten()
    {
        $this->_MenubarItems[] = 'edit';
    }

    /**
     * F�gt der Menubar das "Einf�gen" Kontextmen� hinzu
     */
    public function MitInsert()
    {
        $this->_MenubarItems[] = 'insert';
    }

    /**
     * F�gt der Menubar das "Ansicht" Kontextmen� hinzu
     */
    public function MitAnsicht()
    {
        $this->_MenubarItems[] = 'view';
    }

    /**
     * F�gt der Menubar das "Format" Kontextmen� hinzu
     */
    public function MitFormat()
    {
        $this->_MenubarItems[] = 'format';
    }

    /**
     * F�gt der Menubar das "Tools" Kontextmen� hinzu
     */
    public function MitTools()
    {
        $this->_MenubarItems[] = 'tools';
    }
}