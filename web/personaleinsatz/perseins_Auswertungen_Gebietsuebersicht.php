<?php
global $con;
global $awisRSZeilen;

	/***************************************
	* Gebiets�bersicht
	***************************************/
require_once 'fpdi.php';

$SpaltenHoehen = array();

ini_set('max_execution_time',60);

$SQL = "select * ";
$SQL .= ' FROM Regionalzentren INNER JOIN Mitarbeiter ON REZ_ID = MIT_REZ_ID ';
$SQL .= ' WHERE MIT_MTA_ID=1 AND MIT_STATUS=\'A\' AND REZ_LAN_CODE=\'DE\'';

if(isset($_POST['txtMIT_REZ_ID']) AND $_POST['txtMIT_REZ_ID']!='0')		//Regionalzentren
{
	$SQL .= " AND MIT_REZ_ID=" . $_POST['txtMIT_REZ_ID'] . "";
}

$SQL .= ' ORDER BY REZ_BEZEICHNUNG';

//awis_Debug(1,$SQL);

$rsMEI = awisOpenRecordset($con, $SQL );
$rsMEIZeilen = $awisRSZeilen;
	// Alle kommissarischen TKDLs
$rsFILBem = awisOpenRecordset($con, 'SELECT FIF_WERT, FIF_FIL_ID FROM FilialInfos WHERE FIF_FIT_ID=28');
$rsFILBemZeilen = $awisRSZeilen;

$pdf = new fpdi('l','mm','a4');
$pdf->open();
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$ATULogo = $pdf->ImportPage(1);
if($rsMEIZeilen==0)
{
	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
}

$RasterZeile=9999;
$LetztesREZ = '';

for($i=0;$i<$rsMEIZeilen;$i++)
{
	$Seite=1;

	$pdf->addpage();
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,270,4,20);

	$pdf->setXY(10,5);
	$pdf->SetFont('Arial','',16);
	$pdf->cell(270,6,"Filialzuordnung",0,0,'C',0);

	$pdf->setXY(10,10);
	$pdf->SetFont('Arial','',5);
	$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

	$pdf->setXY(270,12);
	$pdf->SetFont('Arial','',6);
	$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'L',0);


	$SQL="SELECT MIT_BEZEICHNUNG, count(FIF_FIL_ID) ANZAHL FROM Regionalzentren INNER JOIN Mitarbeiter ON REZ_ID = MIT_REZ_ID INNER JOIN FILIALINFOS ON REZ_ID = FIF_WERT";
	$SQL.=" WHERE MIT_MTA_ID=1 AND FIF_FIT_ID=26 AND REZ_LAN_CODE='DE' AND MIT_BEZEICHNUNG='".$rsMEI['MIT_BEZEICHNUNG'][$i]."' GROUP BY MIT_BEZEICHNUNG";

	$rsVKLAnzahl = awisOpenRecordset($con, $SQL );
	$rsVKLAnzahlZeilen = $awisRSZeilen;

	$pdf->setXY(10,15);
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,"Regionalzentrum " . $rsMEI['REZ_BEZEICHNUNG'][$i] . ', VKL: ' . $rsMEI['MIT_BEZEICHNUNG'][$i] .' ('.$rsVKLAnzahl['ANZAHL'][0].')',0,0,'L',0);

		//******************************************
		// Trainer Technik
		//******************************************
	$rsPers = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_STATUS='A' AND MIT_MTA_ID=50 ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	$pdf->setXY(10,20);
	$pdf->SetFont('Arial','',8);
	$pdf->cell(270,6,"Trainer Technik: " . substr($PersName,2) ,0,0,'L',0);

		//******************************************
		// Wartungstechniker
		//******************************************
	$rsPers = awisOpenRecordset($con,"SELECT * FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_STATUS='A' AND MIT_MTA_ID=40 ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	unset($rsPers);
	$pdf->setXY(10,23);
	$pdf->SetFont('Arial','',8);
	$pdf->cell(270,6,"Wartungstechnik: " . substr($PersName,2) ,0,0,'L',0);

		//******************************************
		// Taskforce (oder anderes)
		//******************************************
	$ZusatzInfo = 210;
	$rsPers = awisOpenRecordset($con,"SELECT DISTINCT MIT_BEZEICHNUNG FROM Mitarbeiter WHERE MIT_REZ_ID='" . $rsMEI['REZ_ID'][$i] . "' AND MIT_STATUS='A' AND MIT_MTA_ID=".$ZusatzInfo." ORDER BY MIT_BEZEICHNUNG");
	$PersName = '';
	for($PersZeile=0;$PersZeile<$awisRSZeilen;$PersZeile++)
	{
		$PersName .= ', ' . $rsPers['MIT_BEZEICHNUNG'][$PersZeile];
	}
	unset($rsPers);
	$pdf->setXY(10,26);
	$pdf->SetFont('Arial','',8);
	$rsPers = awisOpenRecordset($con,"SELECT MTA_BEZEICHNUNG FROM MITARBEITERTAETIGKEITEN WHERE MTA_ID=0".$ZusatzInfo);
	$pdf->cell(270,6,$rsPers['MTA_BEZEICHNUNG'][0] . ": " . substr($PersName,2) ,0,0,'L',0);


		//******************************************
		// Alle GBLs suchen
		//******************************************
	$rsGBL = awisOpenRecordset($con, "SELECT * FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY WHERE MIT_REZ_ID='" . $rsMEI['MIT_REZ_ID'][$i] . "' ORDER BY MIT_BEZEICHNUNG");
	$rsGBLZeilen = $awisRSZeilen;

	$Spalte = 0;		// Neue Datenspalte
	$RasterZeile = 33;
	$pdf->SetFillColor(240,240,240);
	$MaxFilZeilen=0;
	$BlockZeile = 0;
	$UberschriftenHoehe1 = 0;
	$UberschriftenHoehe2 = 0;
	$LetzteFilZeile = 0;

			// Jeder Gebietsleiter wird ein einzelner Block
	for($j=0;$j<$rsGBLZeilen;$j++)
	{
		// Neue Zeile notwendig?
		if($Spalte > 5)
		{
			$Spalte = 0;
			$RasterZeile = $RasterZeile+($MaxFilZeilen*4)+13+$UberschriftenHoehe1+$UberschriftenHoehe2;
			$MaxFilZeilen=0;
			if(++$BlockZeile>1)
			{
				$pdf->addpage();
				$pdf->SetAutoPageBreak(true,0);
				$UberschriftenHoehe1=0;
				$UberschriftenHoehe2=0;
				$Spalte = 0;		// Neue Datenspalte
				$RasterZeile = 33;
				$pdf->SetFillColor(240,240,240);
				$MaxFilZeilen=0;
				$BlockZeile = 0;

				$pdf->setXY(10,5);
				$pdf->SetFont('Arial','',16);
				$pdf->cell(270,6,"Filialzuordnung",0,0,'C',0);

				$pdf->setXY(10,10);
				$pdf->SetFont('Arial','',5);
				$pdf->cell(270,6,'Diese Liste ist nur f�r interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

				$pdf->setXY(270,10);
				$pdf->SetFont('Arial','',6);
				$pdf->cell(10,6,'Stand ' . date('d.m.Y'),0,0,'R',0);

				$pdf->setXY(10,15);
				$pdf->SetFont('Arial','',10);
				$pdf->cell(270,6,"Regionalzentrum " . $rsMEI['REZ_BEZEICHNUNG'][$i] . ', VKL: ' . $rsMEI['MIT_BEZEICHNUNG'][$i] ,0,0,'L',0);

				$pdf->setXY(10,20);
				$pdf->SetFont('Arial','',8);
				$pdf->cell(270,6,"Seite " . ++$Seite ,0,0,'L',0);
			}
		}

		$rsFIL = awisOpenRecordset($con, 'SELECT * FROM Filialen INNER JOIN FilialInfos ON FIL_ID = FIF_FIL_ID WHERE FIF_FIT_ID=21 AND FIF_WERT=0' . $rsGBL['VKG_ID'][$j] . ' ORDER BY FIL_ID');
		$rsFILZeilen = $awisRSZeilen;

		$pdf->setXY(10+($Spalte*45),$RasterZeile);
		$pdf->SetFont('Arial','',8);

		$DoppeltHoheUeberschriftZellen=0;
		if($rsGBL['VKG_MITBEMERKUNG'][$j]!='')
		{
			$pdf->MultiCell(45,4,$rsGBL['MIT_BEZEICHNUNG'][$j] . ' (' . $rsFILZeilen . ") \n" . $rsGBL['VKG_MITBEMERKUNG'][$j],1,'C',1);
			$DoppeltHoheUeberschriftZellen++;
			$UberschriftenHoehe1=3;
			$Mitarbeiter =  $rsGBL['MIT_BEZEICHNUNG'][$j] . ' (' . $rsFILZeilen . ") \n" . $rsGBL['VKG_MITBEMERKUNG'][$j];
		}
		else
		{
			$pdf->cell(45,5,$rsGBL['MIT_BEZEICHNUNG'][$j] . ' (' . $rsFILZeilen . ')',1,0,'C',1);
			$Mitarbeiter = $rsGBL['MIT_BEZEICHNUNG'][$j]. ' (' . $rsFILZeilen . ")";
		}

		$SQL = "SELECT * FROM Verkaufsgebiete INNER JOIN Mitarbeiter ON VKG_TKDL_MIT_KEY = MIT_KEY WHERE VKG_ID='" . $rsGBL['VKG_ID'][$j] . "' AND MIT_MTA_ID=30 AND MIT_REZ_ID='" . $rsMEI['MIT_REZ_ID'][$i] . "' ORDER BY MIT_BEZEICHNUNG";

		$rsTKDL = awisOpenRecordset($con, $SQL);
		$pdf->setXY(10+($Spalte*45),$RasterZeile+5+($DoppeltHoheUeberschriftZellen*3));
		$pdf->SetFont('Arial','',7);
		if(isset($rsTKDL['VKG_TKDLBEMERKUNG'][0]) && $rsTKDL['VKG_TKDLBEMERKUNG'][0]!='')
		{
			$pdf->MultiCell(45,4,'TKDL: '.$rsTKDL['MIT_BEZEICHNUNG'][0] . "\n" . $rsTKDL['VKG_TKDLBEMERKUNG'][0],1,'C',1);
			$DoppeltHoheUeberschriftZellen++;
			$UberschriftenHoehe2=3;
			$MitarbeiterTKDL = 'TKDL: '.$rsTKDL['MIT_BEZEICHNUNG'][0] . "\n" . $rsTKDL['VKG_TKDLBEMERKUNG'][0];
		}
		else
		{
			$pdf->cell(45,5,'TKDL: '.(isset($rsTKDL['MIT_BEZEICHNUNG'][0])?$rsTKDL['MIT_BEZEICHNUNG'][0]:''),1,0,'C',1);
			$MitarbeiterTKDL = 'TKDL: '.(isset($rsTKDL['MIT_BEZEICHNUNG'][0])?$rsTKDL['MIT_BEZEICHNUNG'][0]:'');
		}

		$DoppeltHoheZellen=0;
		$YZeile = 0;
			// Alle zugeordneten Filialen
		for($k=$LetzteFilZeile;$k<$rsFILZeilen;$k++)
		{

			$pdf->setXY(10+($Spalte*45),10+$RasterZeile+(($YZeile+$DoppeltHoheZellen)*4)+($DoppeltHoheUeberschriftZellen*3));
			if($rsFIL['FIL_GRUPPE'][$k]=='')
			{
				$pdf->SetFont('Arial','B',8);
			}
			else
			{
				$pdf->SetFont('Arial','',8);
			}
			$IstDoppelteZeile = 0;
			for($l=0;$l<$rsFILBemZeilen;$l++)		// Sind f�r die Filiale SonderTKDL Infos da?
			{
				if($rsFILBem['FIF_FIL_ID'][$l]==$rsFIL['FIL_ID'][$k] AND $rsFILBem['FIF_WERT'][$l]!='')
				{
					$pdf->MultiCell(45,4,substr('000'.$rsFIL['FIL_ID'][$k],-3) . ' ' . $rsFIL['FIL_BEZ'][$k] . "\nTKDL: " . $rsFILBem['FIF_WERT'][$l] . '',1,'J',0);
					$DoppeltHoheZellen++;
					$rsFILBemZeilen=9999;
					$IstDoppelteZeile = 1;
					break;
				}
			}
			if($l>=$rsFILBemZeilen)	// Keine TKDL Zusatz Info-> einzeilig
			{
				$pdf->cell(45,4,substr('000'.$rsFIL['FIL_ID'][$k],-3) . ' ' . $rsFIL['FIL_BEZ'][$k],1,0,'L',0);
			}

			//echo substr('000'.$rsFIL['FIL_ID'][$k],-3) . ' ' . $rsFIL['FIL_BEZ'][$k];
			//echo $pdf->GetX().' - '.$pdf->GetY();
			//echo "\n";

			// Zu viele Zeilen pro MA -> neue Spalte f�r einen GBL
			if($k+1 < $rsFILZeilen AND (($YZeile+$DoppeltHoheZellen+$DoppeltHoheUeberschriftZellen)>=(14-$IstDoppelteZeile) OR $pdf->GetY()>195))		// Spalte zu gross -> umbrechen
			{
				if($Spalte>=5)			// Rest des Filialblocks passt nicht mehr auf die Seite -> Neue Zeile
				{
					$Spalte++;
					$LetzteFilZeile = ++$k;			// Weiter machen mit dem Rest
					$j--;							// Wieder den gleichen GBL
					break;
				}

				$pdf->setXY(10+($Spalte*45),$RasterZeile);
				$pdf->SetFont('Arial','',8);
				$pdf->setXY(10+($Spalte*45),$RasterZeile+5+($DoppeltHoheUeberschriftZellen*3));
				$pdf->SetFont('Arial','',7);
				$pdf->cell(90,5,$MitarbeiterTKDL,1,0,'C',1);
				$pdf->setXY(10+($Spalte*45),$RasterZeile);
				$pdf->SetFont('Arial','',8);
				$pdf->cell(90,5,$Mitarbeiter,1,0,'C',1);

				$MaxFilZeilen = ($MaxFilZeilen<$YZeile?$YZeile:$MaxFilZeilen);

				$YZeile=0;
				$Spalte++;
				$DoppeltHoheZellen=0;
			}
			else
			{
				$YZeile++;
			}

		}
		//$MaxFilZeilen = ($MaxFilZeilen<$rsFILZeilen?$rsFILZeilen:$MaxFilZeilen);
		$MaxFilZeilen = ($MaxFilZeilen<$YZeile?$YZeile:$MaxFilZeilen);
		$Spalte++;
		if($k>=$rsFILZeilen)
		{
			$LetzteFilZeile=0;
		}
	}
}		// Ende for

?>