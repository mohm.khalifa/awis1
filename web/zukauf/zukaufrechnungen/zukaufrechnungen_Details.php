<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUR','%');
	$TextKonserven[]=array('ZUB','ZUB_WANRKORR');
	$TextKonserven[]=array('TITEL','tit_ZukaufRechnungen');
	$TextKonserven[]=array('TITEL','tit_ZukaufLieferscheine');
	$TextKonserven[]=array('ZLH','%');
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('AST','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10005 = $AWISBenutzer->HatDasRecht(10005);
	if($Recht10005==0)
	{
	    awisEreignis(3,1000,'Zukaufrechnungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUR'));

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['ZUR_ARTIKELNUMMER'] = $_POST['sucZUR_ARTIKELNUMMER'];
		$Param['ZUR_RECHNUNGSNUMMER'] = $_POST['sucZUR_RECHNUNGSNUMMER'];
		$Param['ZUR_ARTIKELBEZEICHNUNG'] = $_POST['sucZUR_ARTIKELBEZEICHNUNG'];
		$Param['ZUR_FIL_ID'] = $_POST['sucZUR_FIL_ID'];
		$Param['ZUR_LIE_NR'] = $_POST['sucZUR_LIE_NR'];
		$Param['ZLH_KEY'] = $_POST['sucZLH_KEY'];
		$Param['DATUMBIS'] = $_POST['sucDATUMBIS'];
		$Param['DATUMVOM'] = $_POST['sucDATUMVOM'];
		$Param['AST_ATUNR'] = $_POST['sucAST_ATUNR'];
		$Param['ZUR_EXTERNEID'] = $_POST['sucZUR_EXTERNEID'];
		$Param['ZUR_WANR'] = $_POST['sucZUR_WANR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ZUR",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./zukaufrechnungen_loeschen.php');
		if($AWIS_KEY1==0)
		{
			$Param = $AWISBenutzer->ParameterLesen("Formular_ZUR");
		}
		else
		{
			$Param = $AWIS_KEY1;
		}
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufrechnungen_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUR'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUR'));
	}
	elseif(isset($_GET['ZUR_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZUR_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUR'));
	}
	elseif(isset($_GET['RechNr']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUR'));
		$Param['ZUR_RECHNUNGSNUMMER']= $_GET['RechNr'];
		$Param['ZUR_ARTIKELNUMMER'] = '';
		$Param['ZUR_FIL_ID'] = '';
		$Param['ZUR_LIE_NR'] = '';
		$Param['ZLH_KEY'] = '';
		$Param['DATUMBIS'] = '';
		$Param['DATUMVOM'] = '';
		$Param['AST_ATUNR'] = '';
		$Param['ZUR_EXTERNEID'] = '';
		$Param['ZUR_WANR'] = '';
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		
	}
	elseif(isset($_GET['ZUR_WANR']))
	{
		$Param = array();
		$Param['ZUR_ARTIKELNUMMER'] = '';
		$Param['ZUR_FIL_ID'] = '';
		$Param['ZUR_LIE_NR'] = '';
		$Param['ZLH_KEY'] = '';
		$Param['DATUMBIS'] = '';
		$Param['DATUMVOM'] = '';
		$Param['AST_ATUNR'] = '';
		$Param['ZUR_EXTERNEID'] = '';
		$Param['ZUR_WANR'] = $_GET['ZUR_WANR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ZUR",serialize($Param));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_ZUR',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZUR_DATUM DESC';
			$Param['ORDER']='ZUR_DATUM DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT Zukaufrechnungen.*, ZAL_ZLA_KEY, MWS_SATZ, ZLA_ARTIKELNUMMER, ZLH_KEY, ZLH_KUERZEL, ZLH_BEZEICHNUNG';
	$SQL .= ', ZUB_KEY, ZUB_BESTELLMENGE, ZUB_EINHEITENPROPREIS, ZUB_WANR, ZUB_EK, ZUB_PREISANGELIEFERT, ZUB_WANRKORR, ZAP_VKPR, ZAP_EKPR';
	$SQL .= ', ZUL_KEY, ZUL_LIEFERMENGE, ZUL_EINHEITENPROPREIS, ZUL_EKPR, ZUL_VKPR';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukaufrechnungen';
	$SQL .= ' LEFT OUTER JOIN Zukaufbestellungen ON ZUR_ZUB_KEY = ZUB_KEY';
	$SQL .= ' LEFT OUTER JOIN Zukauflieferscheine ON ZUR_ZUL_KEY = ZUL_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAL_KEY = ZUR_ZAL_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKEL ON ZLA_KEY = ZAL_ZLA_KEY';
	$SQL .= ' LEFT OUTER JOIN MEHRWERTSTEUERSAETZE ON MWS_ID = ZUR_MWS_ID';
	$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZLH_KEY = ZUR_ZLH_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTENPREISE ON ZUB_ZAP_KEY = ZAP_KEY';
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUR',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1,$SQL);
		
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	
	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsZUR = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_ZUR',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmZukaufLieferscheine action=./zukaufrechnungen_Main.php?cmdAktion=Details method=POST>';

	if($rsZUR->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZUR->AnzahlDatensaetze()>1 AND !isset($_GET['ZUR_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUR_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_DATUM_kurz'],95,'',$Link);
		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUR_RECHNUNGSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_RECHNUNGSNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_RECHNUNGSNUMMER'],140,'',$Link);
		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUR_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_WANR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_WANR'],110,'',$Link);
		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUR_MENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_MENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_MENGE'],60,'',$Link);
		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUR_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_ARTIKELNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_ARTIKELNUMMER'],180,'',$Link);
		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],50,'',$Link);
		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUR_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_ARTIKELBEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_ARTIKELBEZEICHNUNG'],320,'',$Link);
		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUR_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUR_BETRAG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_BETRAG'],80,'text-align:right;',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_ALTTEILWERT'],80,'text-align:right;',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],55);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUR']['ZUR_LIE_NR'],60);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZUR->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './zukaufrechnungen_Main.php?cmdAktion=Details&ZUR_KEY=0'.$rsZUR->FeldInhalt('ZUR_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZUR_DATUM',$rsZUR->FeldInhalt('ZUR_DATUM'),0,95,false,($DS%2),'',$Link,'D');
			$Form->Erstelle_ListenFeld('ZUR_RECHNUNGSNUMMER',$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER'),0,140,false,($DS%2),'','','T');
			$Link = '';
			if($rsZUR->FeldInhalt('ZUB_KEY')!='')
			{
				$Link = '../zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$rsZUR->FeldInhalt('ZUB_KEY');
			}

			//
            $Werkzeug = new awisWerkzeuge();
			
			if(($rsZUR->FeldInhalt('ZUR_WANR')!=''
					AND $Werkzeug->BerechnePruefziffer($rsZUR->FeldInhalt('ZUR_WANR'),awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN)===false)
				OR $rsZUR->FeldInhalt('ZUR_WANR')=='')
			{
				$WANr = ($rsZUR->FeldInhalt('ZUB_WANRKORR')!=''?$rsZUR->FeldInhalt('ZUB_WANRKORR'):($rsZUR->FeldInhalt('ZUB_WANR')==''?$rsZUR->FeldInhalt('ZUR_WANR'):$rsZUR->FeldInhalt('ZUB_WANR')));
			}
			else
			{
				$WANr = ($rsZUR->FeldInhalt('ZUR_WANR')==''?'???':$rsZUR->FeldInhalt('ZUR_WANR'));
			}


			$Form->Erstelle_ListenFeld('ZUR_WANR',$WANr.($rsZUR->FeldInhalt('ZUB_WANRKORR')==''?'':'*'),0,110,false,($DS%2),'',$Link,'T','');
			$Form->Erstelle_ListenFeld('ZUR_MENGE',$rsZUR->FeldInhalt('ZUR_MENGE'),0,60,false,($DS%2),'','','N2','L');
			$Link = '';
			if($rsZUR->FeldInhalt('ZAL_ZLA_KEY')!='')
			{
				$Link = '../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUR->FeldInhalt('ZAL_ZLA_KEY');
			}
			$Form->Erstelle_ListenFeld('ZUR_ARTIKELNUMMER',$rsZUR->FeldInhalt('ZUR_ARTIKELNUMMER'),0,180,false,($DS%2),'',$Link);
			$Link='../zukaufhersteller/zukaufhersteller_Main.php?cmdAktion=Details&ZLH_KEY='.$rsZUR->FeldInhalt('ZLH_KEY');
			$Form->Erstelle_ListenFeld('ZLH_KUERZEL',$rsZUR->FeldInhalt('ZLH_KUERZEL'),0,50,false,($DS%2),'',$Link,'T','L',$rsZUR->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Link ='';
			$Form->Erstelle_ListenFeld('ZUR_ARTIKELBEZEICHNUNG',$rsZUR->FeldInhalt('ZUR_ARTIKELBEZEICHNUNG'),0,320,false,($DS%2),'',$Link,'T','L');
			
			$Betrag = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_BETRAG')) + $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ZUSATZBETRAG'));
			$Form->Erstelle_ListenFeld('ZUR_BETRAG',$Betrag,0,80,false,($DS%2),'',$Link,'N2','R');
			$Form->Erstelle_ListenFeld('ZUR_ALTTEILWERT',($rsZUR->FeldInhalt('ZUR_ALTTEILWERT')*$rsZUR->FeldInhalt('ZUR_ALTTEILMENGE')),0,80,false,($DS%2),'',$Link,'N2','R');

			//****************************************
			// Status
			//****************************************
			// Abweichungen berechnen
			$Status='';
			$TippText = '';

			if($rsZUR->FeldInhalt('ZUB_KEY')=='')
			{
				$Status .= 'B';
				$TippText .= '/(B)Bestellung nicht gefunden.'."\n";
			}
			if($rsZUR->FeldInhalt('ZUB_KEY')!='' AND ($rsZUR->FeldInhalt('ZUB_EK')!=$rsZUR->FeldInhalt('ZUR_EINZELPREIS')))
			{
				$Status .= 'P';
				$TippText .= '/(P)Preis weicht ab('.$Form->Format('N2',$rsZUR->FeldInhalt('ZUR_EINZELPREIS')).' / '.$Form->Format('N2',$rsZUR->FeldInhalt('ZUB_EK')).')';
			}
			$Form->Erstelle_ListenFeld('STATUS',$Status,0,55,false,($DS%2),'','','T','',substr($TippText,1));

			$Form->Erstelle_ListenFeld('ZUR_LIE_NR',$rsZUR->FeldInhalt('ZUR_LIE_NR'),0,60,false,($DS%2),'',$Link,'T','R',$rsZUR->FeldInhalt('LIE_NAME1'));

			$Form->ZeileEnde();

			$rsZUR->DSWeiter();
			$DS++;
		}

		$Link = './zukaufrechnungen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZUR->FeldInhalt('ZUR_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_ZUR',serialize($Param));

		echo '<input type=hidden name=txtZUR_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufrechnungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUR->FeldInhalt('ZUR_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUR->FeldInhalt('ZUR_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10005&2)!=0);
		$EditRecht2=(($Recht10005&32)!=0);      // Importfelder bearbeiten

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10005&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_LIE_NR'].':',200);
		if($EditRecht)
		{
			$AWISCursorPosition='txtZUR_LIE_NR';
		}
		$SQL = 'SELECT LIE_NR, LIE_NAME1 ';
		$SQL .= ' FROM LIEFERANTEN';
		$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('ZUR_LIE_NR',$rsZUR->FeldInhalt('ZUR_LIE_NR'),500,$EditRecht,$SQL);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EXTERNEID'].':',200);
		$Form->Erstelle_TextFeld('ZUR_EXTERNEID',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_EXTERNEID')),20,200,false,'','','','T');
		$Form->ZeileEnde();

			// Bestellinformationen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_DATUM'].':',200);
		$Form->Erstelle_TextFeld('ZUR_DATUM',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_DATUM')),20,200,$EditRecht2,'','','','D');
		$Link = ($rsZUR->FeldInhalt('ZUR_FIL_ID')==''?'':'/filialinfos/filialinfos_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsZUR->FeldInhalt('ZUR_FIL_ID'));
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_FIL_ID'].':',200,'',$Link);
		$Form->Erstelle_TextFeld('ZUR_FIL_ID',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_FIL_ID')),4,60,$EditRecht,'','','','T');
		$Form->Erstelle_TextFeld('#FIL_BEZ',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('FIL_BEZ')),20,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WANR'].':',200);
	 	$Form->Erstelle_TextFeld('ZUR_WANR',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_WANR')),20,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANRKORR'].':',200);
	 	$Form->Erstelle_TextFeld('ZUB_WANRKORR',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUB_WANRKORR')),20,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RECHNUNGSNUMMER'].':',200);
	 	$Form->Erstelle_TextFeld('ZUR_RECHNUNGSNUMMER',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_RECHNUNGSNUMMER')),20,200,$EditRecht2,'','','','T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARCHIVID'].':',200);
	 	$Form->Erstelle_TextFeld('ZUR_ARCHIVID',$rsZUR->FeldInhalt('ZUR_ARCHIVID'),20,200,$EditRecht2,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WAEHRUNG'].':',200);
		$Form->Erstelle_TextFeld('ZUR_WAEHRUNG',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_WAEHRUNG')),10,200,$EditRecht2,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_MWS_ID'].':',200);
		$SQL = 'SELECT MWS_ID, MWS_BEZEICHNUNG || \' - \' || MWS_LAN_CODE FROM Mehrwertsteuersaetze ORDER BY 2';
		$Form->Erstelle_SelectFeld('ZUR_MWS_ID',$rsZUR->FeldInhalt('ZUR_MWS_ID'),200,$EditRecht,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_VERSANDART'].':',200);
		$Form->Erstelle_TextFeld('ZUR_VERSANDART',$rsZUR->FeldInhalt('ZUR_VERSANDART'),10,200,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_LIEFERKOSTEN'].':',200);
		$Form->Erstelle_TextFeld('ZUR_LIEFERKOSTEN',$rsZUR->FeldInhalt('ZUR_LIEFERKOSTEN'),10,200,$EditRecht,'','','','N2');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('',400);
		//$Form->Erstelle_TextLabel('|',5);
		$Link = '../zukauflieferscheine/zukauflieferscheine_Main.php?cmdAktion=Details&ZUL_KEY=0'.$rsZUR->FeldInhalt('ZUR_ZUL_KEY');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['TITEL']['tit_ZukaufLieferscheine'],200,'',$Link);
		//$Form->Erstelle_TextLabel('|',5);
		$Link = '../zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=0'.$rsZUR->FeldInhalt('ZUR_ZUB_KEY');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ZUB_KEY'],200,'',$Link);
		$Form->ZeileEnde();
		
		// TODO: R�cksprache JS
/*		
		if($rsZUR->FeldInhalt('ZUR_ZUL_KEY')=='' OR $rsZUR->FeldInhalt('ZUR_ZUB_KEY')=='')
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel('',400);
		    
		    // Lieferschein manuell zuordnen
		    if($rsZUR->FeldInhalt('ZUR_ZUL_KEY')=='')
		    {
		      $SQL = 'SELECT ZUL_KEY, ZUL_LIEFERSCHEINNR || \' - \' || TO_CHAR(ZUL_DATUM,\'DD.MM.RRRR\') AS ZUL ';
		      $SQL .= ' FROM ZUKAUFLIEFERSCHEINE ';
		      $SQL .= ' WHERE ZUL_WANR = '.$DB->FeldInhaltFormat('T',$rsZUR->FeldInhalt('ZUR_WANR')).' ORDER BY 2';
		      $Form->Erstelle_SelectFeld('ZUR_ZUL_KEY',$rsZUR->FeldInhalt('ZUR_ZUL_KEY'),'200:190',$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','T');
		    }
		    else 
		    {
		        $Form->Erstelle_TextLabel('',200);
		    }
		    
		    // Bestellung manuell zuordnen
		    if($rsZUR->FeldInhalt('ZUR_ZUB_KEY')=='')
		    {
		      $SQL = 'SELECT ZUB_KEY, TO_CHAR(ZUB_BESTELLDATUM,\'DD.MM.RRRR\') || \' - \' || ZUB_ARTIKELNUMMER || \', \' || ZUB_BESTELLMENGE AS ZUB ';
		      $SQL .= ' FROM ZUKAUFBESTELLUNGEN ';
		      $SQL .= ' WHERE ZUB_WANR = '.$DB->FeldInhaltFormat('T',$rsZUR->FeldInhalt('ZUR_WANR')).' ORDER BY 2';
		      $Form->Erstelle_SelectFeld('ZUR_ZUB_KEY',$rsZUR->FeldInhalt('ZUR_ZUB_KEY'),'200:190',$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','T');
		    }
		    $Form->ZeileEnde();
		}
*/
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_MENGE'].':',200);
		$Form->Erstelle_TextFeld('ZUR_MENGE',$rsZUR->FeldInhalt('ZUR_MENGE'),10,200,$EditRecht2,'','text-align:right;','','N2T','L');
		//$Form->Erstelle_TextLabel('|',5);
        if($rsZUR->FeldInhalt('ZUL_LIEFERMENGE')=='')
        {
            $Form->Erstelle_TextLabel('%',200);
        }
        else
        {
            $Form->Erstelle_TextFeld('ZUL_LIEFERMENGE',$rsZUR->FeldInhalt('ZUL_LIEFERMENGE'),10,200,false,'','text-align:right;','','NxT');
        }
		//$Form->Erstelle_TextLabel('|',5);
        if($rsZUR->FeldInhalt('ZUB_BESTELLMENGE')=='')
        {
            $Form->Erstelle_TextLabel('%',200);
        }
        else
        {
    		$Form->Erstelle_TextFeld('ZUB_BESTELLMENGE',$rsZUR->FeldInhalt('ZUB_BESTELLMENGE'),10,200,false,'','text-align:right;','','NxT');
        }
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EINHEITENPROPREIS'].':',200);
		$Form->Erstelle_TextFeld('ZUR_EINHEITENPROPREIS',$rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS'),10,200,$EditRecht2,'','text-align:right;','','T');
		//$Form->Erstelle_TextLabel('|',5);
		$Form->Erstelle_TextFeld('ZUL_EINHEITENPROPREIS',$rsZUR->FeldInhalt('ZUL_EINHEITENPROPREIS'),10,200,false,'','text-align:right;','','T');
		//$Form->Erstelle_TextLabel('|',5);
		$Form->Erstelle_TextFeld('ZUB_EINHEITENPROPREIS',$rsZUR->FeldInhalt('ZUB_EINHEITENPROPREIS'),10,200,false,'','text-align:right;','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_VKPR'].':',200);
		$Form->Erstelle_TextFeld('ZUR_VKPR',$rsZUR->FeldInhalt('ZUR_VKPR'),10,200,$EditRecht2,'','text-align:right;','','N2');
		//$Form->Erstelle_TextLabel('|',5);
		if($rsZUR->FeldInhalt('ZUL_VKPR')=='')
		{
		    $Form->Erstelle_TextLabel('--',200);
		}
		else
		{
		    $Form->Erstelle_TextFeld('ZUL_VKPR',$rsZUR->FeldInhalt('ZUL_VKPR'),10,200,false,'','text-align:right;','','N2');
		}
		//$Form->Erstelle_TextLabel('|',5);
		if($rsZUR->FeldInhalt('ZAP_VKPR')=='')
		{
		    $Form->Erstelle_TextLabel('--',200);
		}
		else
		{
		    $GHB = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUB_PREISANGELIEFERT'))/(1+(($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('MWS_SATZ'))/100.0)));
		    $Form->Erstelle_TextFeld('ZUB_PREISANGELIEFERT',$GHB,10,200,false,'','text-align:right;','','N2');
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EKPR'].':',200);
		$Form->Erstelle_TextFeld('ZUR_EKPR',$rsZUR->FeldInhalt('ZUR_EKPR'),10,200,$EditRecht2,'','text-align:right;','','N2');
		//$Form->Erstelle_TextLabel('|',5);
		if($rsZUR->FeldInhalt('ZUL_EKPR')=='')
		{
		    $Form->Erstelle_TextLabel('--',200);
		}
		else
		{
		    $Form->Erstelle_TextFeld('ZUL_EKPR',$rsZUR->FeldInhalt('ZUL_EKPR'),10,200,false,'','text-align:right;','','N2');
		}
		//$Form->Erstelle_TextLabel('|',5);
		if($rsZUR->FeldInhalt('ZAP_EKPR')=='')
		{
		    $Form->Erstelle_TextLabel('--',200);
		}
		else
		{
		    $Form->Erstelle_TextFeld('ZUB_EK',$rsZUR->FeldInhalt('ZUB_EK'),10,200,false,'','text-align:right;','','N2');
		}
		$Form->ZeileEnde();


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RABATT1'].':',200);
		$Form->Erstelle_TextFeld('ZUR_RABATT1',$rsZUR->FeldInhalt('ZUR_RABATT1'),10,200,$EditRecht2,'','text-align:right;','','N2');
		$Form->ZeileEnde();

		if($rsZUR->FeldInhalt('ZUR_RABATT2')!=0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RABATT2'].':',200);
			$Form->Erstelle_TextFeld('ZUR_RABATT2',$rsZUR->FeldInhalt('ZUR_RABATT2'),10,200,$EditRecht2,'','text-align:right;','','N2','L');
			$Form->ZeileEnde();
		}

		if($rsZUR->FeldInhalt('ZUR_RABATT3')!=0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_RABATT3'].':',200);
			$Form->Erstelle_TextFeld('ZUR_RABATT3',$rsZUR->FeldInhalt('ZUR_RABATT3'),10,200,$EditRecht2,'','text-align:right;','','N2','L');
			$Form->ZeileEnde();
		}
		
		$Form->ZeileStart();
		$EK = (float)$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_EINZELPREIS'))/($rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS')=='0'?1:(float)$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS')));
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_EINZELPREIS'].':',200);
		$Form->Erstelle_TextFeld('ZUR_EINZELPREIS',$EK,10,80,false,'','text-align:right;','','N2','R');
		$Form->Erstelle_TextFeld('#FILLER','',10,120,false,'','text-align:right;','','T');
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ANPASSUNG'].':',200);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ANPASSUNG_GRUND'].':',200);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WARENWERT'].':',200);
        $Warenwert = $DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_EINZELPREIS'))*$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_MENGE'));
        $Warenwert = ($Warenwert/($rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS')=='0'?1:$rsZUR->FeldInhalt('ZUR_EINHEITENPROPREIS')));
		$Form->Erstelle_TextFeld('#WARENWERT',$Warenwert,10,80,false,'','','','N2','R');
		$Form->Erstelle_TextFeld('#FILLER','',10,120,false,'','text-align:right;','','T');
		$Form->Erstelle_TextFeld('ZUR_WERTANPASSEN',$rsZUR->FeldInhalt('ZUR_WERTANPASSEN'),10,200,$EditRecht,'','text-align:right;','','N2');
		$Form->Erstelle_TextFeld('ZUR_WERTANPASSENGRUND',$rsZUR->FeldInhalt('ZUR_WERTANPASSENGRUND'),70,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ALTTEILMENGE'].':',200);
		$Form->Erstelle_TextFeld('ZUR_ALTTEILMENGE',$rsZUR->FeldInhalt('ZUR_ALTTEILMENGE'),10,200,$EditRecht2,'','text-align:right;','','Nx');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ALTTEILWERT'].':',200);
		$Form->Erstelle_TextFeld('ZUR_ALTTEILWERT',$rsZUR->FeldInhalt('ZUR_ALTTEILWERT'),10,200,$EditRecht2,'','text-align:right;','','N2');
		$Form->Erstelle_TextFeld('ZUR_ATWANPASSEN',$rsZUR->FeldInhalt('ZUR_ATWANPASSEN'),10,200,$EditRecht,'','','','N2');
		$Form->Erstelle_TextFeld('ZUR_ATWANPASSENGRUND',$rsZUR->FeldInhalt('ZUR_ATWANPASSENGRUND'),70,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		// Zusatzbetrag (v.a. HESS)
		// Aber keine Altteile und kein Rabatt
		// => daher vom Warenwert abziehen!
		// => pr�fen, ob das f�r alle Positionen so gilt!		
		if($rsZUR->FeldInhalt('ZUR_ZUSATZBETRAG')<>0)
		{
    		$Form->ZeileStart();
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ZUSATZBETRAG'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_ZUSATZBETRAG',$rsZUR->FeldInhalt('ZUR_ZUSATZBETRAG'),10,200,$EditRecht,'','text-align:right;','','N2');
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ZUSATZBETRAGTYP'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_ZUSATZBETRAGTYP',$rsZUR->FeldInhalt('ZUR_ZUSATZBETRAGTYP'),20,200,$EditRecht,'','','','T');
    		$Form->ZeileEnde();
		}

		if($rsZUR->FeldInhalt('ZUR_UMWELTPAUSCHALEMENGE')>0)
		{
    		$Form->ZeileStart();
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_UMWELTPAUSCHALE'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_UMWELTPAUSCHALE',$rsZUR->FeldInhalt('ZUR_UMWELTPAUSCHALE'),10,200,$EditRecht,'','text-align:right;','','N2');
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_UMWELTPAUSCHALEMENGE'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_UMWELTPAUSCHALEMENGE',$rsZUR->FeldInhalt('ZUR_UMWELTPAUSCHALEMENGE'),5,100,$EditRecht,'','','','Nx');
    		$Form->ZeileEnde();
		}
		
		if($rsZUR->FeldInhalt('ZUR_WIEDEREINLAGERUNGMENGE')>0)
		{
    		$Form->ZeileStart();
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WIEDEREINLAGERUNG'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_WIEDEREINLAGERUNG',$rsZUR->FeldInhalt('ZUR_WIEDEREINLAGERUNG'),10,200,$EditRecht,'','text-align:right;','','N2');
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_WIEDEREINLAGERUNGMENGE'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_WIEDEREINLAGERUNGMENGE',$rsZUR->FeldInhalt('ZUR_WIEDEREINLAGERUNGMENGE'),5,100,$EditRecht,'','','','Nx');
    		$Form->ZeileEnde();
		}
				
		if($rsZUR->FeldInhalt('ZUR_SONSTIGEMENGE')>0)
		{
    		$Form->ZeileStart();
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_SONSTIGEKOSTEN'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_SONSTIGEKOSTEN',$rsZUR->FeldInhalt('ZUR_SONSTIGEKOSTEN'),10,200,$EditRecht,'','text-align:right;','','N2');
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_SONSTIGEMENGE'].':',200);
    		$Form->Erstelle_TextFeld('ZUR_SONSTIGEMENGE',$rsZUR->FeldInhalt('ZUR_SONSTIGEMENGE'),5,100,$EditRecht,'','','','Nx');
    		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['       '].':',100);
    		$Form->Erstelle_TextFeld('ZUR_SONSTIGEBEZEICHNUNG',$rsZUR->FeldInhalt('ZUR_SONSTIGEBEZEICHNUNG'),36,200,$EditRecht,'','','','T');
    		$Form->ZeileEnde();
		}		
		
		$Form->Trennzeile();

		$Form->ZeileStart();
		$EK = $DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_EINZELPREIS'))*(1-$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_RABATT1'))/100)*(1-$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_RABATT2'))/100)*(1-$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_RABATT3'))/100);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_BETRAG'].':',200);
		$Betrag = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_BETRAG')) + $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ZUSATZBETRAG'));
		// TODO: Was ist den anderen Betr�gen? Auch dazuz�hlen?
		$Betrag += ($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_WIEDEREINLAGERUNG'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_WIEDEREINLAGERUNGMENGE')));

		$Form->Erstelle_TextFeld('ZUR_BETRAG',$Betrag,10,80,false,'','','','N2','R');
		$Form->Erstelle_TextFeld('#MWSTSATZ',$Form->Format('Nx',$rsZUR->FeldInhalt('MWS_SATZ')).'%',10,70,false,'','','','T','R');
		$Form->Erstelle_TextFeld('#MWSTBETRAG',$DB->FeldInhaltFormat('N4',$Betrag)*(1+($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('MWS_SATZ'))/100)),10,100,false,'','','','N2','R');
		$Form->ZeileEnde();

		if($rsZUR->FeldInhalt('ZUR_ALTTEILMENGE')!=0)
		{
			$Form->ZeileStart();
			$EK = $DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_EINZELPREIS'))*(1-$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_RABATT1'))/100)*(1-$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_RABATT2'))/100)*(1-$DB->FeldInhaltFormat('N2',$rsZUR->FeldInhalt('ZUR_RABATT3'))/100);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ALTTEILBETRAG'].':',200);
			$ATWBetrag = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ALTTEILWERT'))*$DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_ALTTEILMENGE'));
			$Form->Erstelle_TextFeld('#ALTTEILBETRAG',$ATWBetrag,10,80,false,'','','','N2','R');
			$Form->Erstelle_TextFeld('#FILLER1','',10,70,false,'','','','N2','R');
			$ATWSteuer = $ATWBetrag*(1+($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('MWS_SATZ'))/100));
			// TODO: Parameter f�r die 10%
			$ATWVKPR = $DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('ZUR_VKPR'))*0.10;
			$ATWSteuer += ($ATWVKPR*(($DB->FeldInhaltFormat('N4',$rsZUR->FeldInhalt('MWS_SATZ'))/100)));
			$Form->Erstelle_TextFeld('#ATWMWSTBETRAG',$ATWSteuer,10,100,false,'','','','N2','R');
			$Form->ZeileEnde();
		}


		$Form->Trennzeile('O');
			// Nummer und Hersteller
		$Form->ZeileStart();
		$Link = '../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUR->FeldInhalt('ZAL_ZLA_KEY');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARTIKELNUMMER'].':',200,'',$Link);
		$Form->Erstelle_TextFeld('ZUR_ARTIKELNUMMER',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_ARTIKELNUMMER')),20,200,$EditRecht);
		$Link = '../zukaufhersteller/zukaufhersteller_Main.php?cmdAktion=Details&ZLH_KEY='.$rsZUR->FeldInhalt('ZUR_ZLH_KEY');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_HERSTELLER'].':',190,'',$Link);
		$Form->Erstelle_TextFeld('ZUR_HERSTELLER',$rsZUR->FeldInhalt('ZUR_HERSTELLER'),20,200,$EditRecht);
		$Form->ZeileEnde();

			// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ARTIKELBEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('ZUR_ARTIKELBEZEICHNUNG',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_ARTIKELBEZEICHNUNG')),50,200,$EditRecht);
		$Form->ZeileEnde();

				// Abschluss
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ABSCHLUSSART'].':',200);
		$Liste = explode("|",$AWISSprachKonserven['ZUR']['lst_ZUR_ABSCHLUSSART']);
		$Form->Erstelle_SelectFeld('ZUR_ABSCHLUSSART',$rsZUR->FeldInhalt('ZUR_ABSCHLUSSART'),200,$EditRecht,'','','1','','',$Liste,'');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUR']['ZUR_ABSCHLUSSGRUND'].':',200);
		$Form->Erstelle_TextFeld('ZUR_ABSCHLUSSGRUND',($AWIS_KEY1===0?'':$rsZUR->FeldInhalt('ZUR_ABSCHLUSSGRUND')),50,200,$EditRecht);
		$Form->ZeileEnde();

		if($rsZUR->FeldInhalt('ZLA_AST_ATUNR')!='')
		{
			$Form->Trennzeile();

			$SQL = 'SELECT Artikelstamm.* ';
			$SQL .= ',(SELECT ATW_BETRAG FROM Altteilwerte WHERE ATW_KENNUNG = (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND asi_ast_atunr=\''.$rsZUR->FeldInhalt('ZLA_AST_ATUNR').'\' AND asi_ait_id=190) AND ATW_LAN_CODE=\'DE\') AS AST_ALTTEILWERT';
			$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND  asi_ast_atunr=\''.$rsZUR->FeldInhalt('ZLA_AST_ATUNR').'\' AND asi_ait_id=191) AS AST_VPE';
			$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR='.$DB->FeldInhaltFormat('T',$rsZUR->FeldInhalt('ZLA_AST_ATUNR'));
			$rsAST = $DB->RecordSetOeffnen($SQL);
			if(!$rsAST->EOF())
			{
				$Form->ZeileStart();
				$Link = '/artikelstamm/artikelstamm_Main.php?ATUNR='.$rsAST->FeldInhalt('AST_ATUNR').'&cmdAktion=ArtikelInfos';
				$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'');
				$Form->Erstelle_TextFeld('AST_ATUNR',$rsAST->FeldInhalt('AST_ATUNR'),20,200,false,'','',$Link);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':',200);
				$Form->Erstelle_TextFeld('AST_BEZEICHNUNGWW',$rsAST->FeldInhalt('AST_BEZEICHNUNGWW'),20,500,false);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'].':',200);
				$Form->Erstelle_TextFeld('AST_VK',$rsAST->FeldInhalt('AST_VK'),20,200,false,'','','','N2');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ALTTEILWERT'].':',200);
				$Form->Erstelle_TextFeld('AST_ALTTEILWERT',$rsAST->FeldInhalt('AST_ALTTEILWERT'),20,200,false,'','','','T');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VPE'].':',200);
				$Form->Erstelle_TextFeld('AST_VPE',$rsAST->FeldInhalt('AST_VPE'),20,200,false,'','','','N0');
				$Form->ZeileEnde();
			}
		}

		$Form->Formular_Ende();
	}

	//awis_Debug(1, $Param, $Bedingung, $rsZUR, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht10005&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht10005&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ZUR_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['ZUR_ARTIKELNUMMER']) AND $Param['ZUR_ARTIKELNUMMER']!='')
	{
		$Bedingung .= ' AND (ZUR_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUR_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . '';
		$Bedingung .= ' OR ZLA_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUR_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['ZLH_KEY']) AND $Param['ZLH_KEY']>0)
	{
		$Bedingung .= ' AND EXISTS (SELECT * FROM ZUKAUFHERSTELLERCODES';
		$Bedingung .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLH_KEY = ZHK_ZLH_KEY';
		$Bedingung .= ' WHERE ZLH_KEY = ' . $DB->FeldInhaltFormat('N0',$Param['ZLH_KEY']);
		$Bedingung .= ' AND ZHK_CODE = ZUR_HERSTELLER';
		$Bedingung .= ' )';
	}
	if(isset($Param['ZUR_FIL_ID']) AND $Param['ZUR_FIL_ID']!='')
	{
		$Bedingung .= ' AND ZUR_FIL_ID = ' . $Param['ZUR_FIL_ID'] . ' ';
	}

	if(isset($Param['ZUR_RECHNUNGSNUMMER']) AND $Param['ZUR_RECHNUNGSNUMMER']!='')
	{
		$Bedingung .= ' AND ZUR_RECHNUNGSNUMMER = ' . $DB->FeldInhaltFormat('T',$Param['ZUR_RECHNUNGSNUMMER']) . ' ';
	}

	if(isset($Param['ZUR_LIE_NR']) AND $Param['ZUR_LIE_NR']!='')
	{
		$Bedingung .= ' AND ZUR_LIE_NR = ' . $Param['ZUR_LIE_NR'] . ' ';
	}

	if(isset($Param['ZSZ_SORTIMENT']) AND $Param['ZSZ_SORTIMENT']!='')
	{
		$Bedingung .= ' AND ZUR_SORTIMENT = ' . $DB->FeldInhaltFormat('T',$Param['ZSZ_SORTIMENT']) . ' ';
	}

	if(isset($Param['DATUMVOM']) AND $Param['DATUMVOM']!='')
	{
		$Bedingung .= ' AND ZUR_DATUM >= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMVOM']) . ' ';
	}
	if(isset($Param['DATUMBIS']) AND $Param['DATUMBIS']!='')
	{
		$Bedingung .= ' AND ZUR_DATUM <= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMBIS']) . ' ';
	}

	if(isset($Param['ZUR_EXTERNEID']) AND $Param['ZUR_EXTERNEID']!='')
	{
		$Bedingung .= ' AND ZUR_EXTERNEID ' . $DB->LIKEoderIST($Param['ZUR_EXTERNEID'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	if(isset($Param['ZUR_WANR']) AND $Param['ZUR_WANR']!='')
	{
		$Bedingung .= ' AND ZUR_WANR ' . $DB->LIKEoderIST($Param['ZUR_WANR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}

	if(isset($Param['ZUR_ZSZ_KEY']) AND $Param['ZUR_ZSZ_KEY']>0)
	{
		$Bedingung .= ' AND ZUR_ZSZ_KEY= ' . intval($Param['ZUR_ZSZ_KEY']) . ' ';
	}
	if(isset($Param['AST_ATUNR']) AND $Param['AST_ATUNR']!='')
	{
		//$Bedingung .= ' AND (ZAL_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ' AND (ZLA_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['ZUR_TAUSCHTEIL']) AND $Param['ZUR_TAUSCHTEIL']>0)
	{
		$Bedingung .= ' AND ZUR_TAUSCHTEIL = ' . intval($Param['ZUR_TAUSCHTEIL']) . ' ';
	}
	if(isset($Param['ZUR_ARTIKELBEZEICHNUNG']) AND $Param['ZUR_ARTIKELBEZEICHNUNG']!='')
	{
		$Bedingung .= ' AND UPPER(ZUR_ARTIKELBEZEICHNUNG)' . $DB->LIKEoderIst($Param['ZUR_ARTIKELBEZEICHNUNG'],  awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>