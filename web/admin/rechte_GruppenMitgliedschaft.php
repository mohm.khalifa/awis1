<?php
global $PHP_AUTH_USER;
global $con;
global $_POST;

$RechteStufe = awisBenutzerRecht($con, 20);
if($RechteStufe==0)
{
	awisEreignis(3,1000,'Rechte�bersicht: Benutzer',$_SERVER['PHP_AUTH_USER'],'','','');
    die("Keine ausreichenden Rechte!");
}

print "<table width=100%";

print "<tr><td width=100>Benutzer</td><td>";
if($RechteStufe & 2) 
{
	$SQL = "SELECT XBN_KEY, XBN_VollerName FROM Benutzer ORDER BY XBN_VollerName";
	
	$rsBenutzer = awisOpenRecordset($con, $SQL);
	$rsBenutzerAnz = $awisRSZeilen;
		
	print "<select name=txtBenutzerID onchange='document.frmRechte.submit();'>";
	if(!isset($_POST["txtBenutzerID"]))
	{
		print "<option selected value=0>Bitte w�hlen...</option>";
	}
	for($i=0;$i<$rsBenutzerAnz;$i++)
	{
		print "<option value=" . $rsBenutzer["XBN_KEY"][$i];
		if(isset($_POST["txtBenutzerID"]) AND $rsBenutzer["XBN_KEY"][$i] == $_POST["txtBenutzerID"])
		{
			print " selected ";
		}
		print ">" . $rsBenutzer["XBN_VOLLERNAME"][$i] . "</option>";
	}

	print "</select>";
	$BNKEY = isset($_POST["txtBenutzerID"])?$_POST["txtBenutzerID"]:'';
}	
else
{
	print "<input type=hidden name=txtBenutzerID value=" . awisBenutzerID($_SERVER['PHP_AUTH_USER']) . ">" . $_SERVER['PHP_AUTH_USER'];
	$BNKEY = awisBenutzerID();
}
print "</td></tr>";
print "</table>";

if($BNKEY!='')
{
	$RechteStufeBenutzerVerwaltung = awisBenutzerRecht($con, 1);

	print "<h3>Gruppenmitgliedschaft<h3>";
	$SQL = "SELECT * FROM BENUTZERGRUPPEN, BENUTZERGRUPPENBEZ WHERE XBG_XBB_KEY=XBB_KEY AND XBG_XBN_KEY=" . $BNKEY . " ORDER BY XBB_BEZ ";
	$rsBenutzerRechte = awisOpenRecordset($con, $SQL);
		
	print "<table border=1 width=510>";
	print "<tr><td id=FeldBez>Gruppe</td></tr>";
	for($i=0;$i<$awisRSZeilen;$i++)
	{
		print "<tr>";
		if($RechteStufeBenutzerVerwaltung>0)
		{
			print "<td width=300><a target='_blank' href=/admin/benutzerundgruppen/gruppenverwaltung_Main.php?cmdAktion=Details&XBB_KEY=" . $rsBenutzerRechte["XBB_KEY"][$i] . ">" . $rsBenutzerRechte["XBB_BEZ"][$i] . "</a></td>";
		}
		else
		{
			print "<td width=300>" . $rsBenutzerRechte["XBB_BEZ"][$i] . "</td>";
		}
		print "</tr>";
	}
	print "</table>";
}

?>