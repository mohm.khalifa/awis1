<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

ob_clean();

//print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;
global $Y_Wert;

require_once('fpdf.php');
require_once 'fpdi.php';

clearstatcache();

include ("ATU_Header.php");

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1600);

if($RechteStufe==0)
{
    awisEreignis(3,1000,'Peronalverkauf-PDF',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//awis_Debug(1,$_REQUEST);

if ($_REQUEST['Export']=='Alle')
{		
	$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
	
	$SQL_EXPORT=str_replace("''","'",$Params[8]);
	
	//awis_Debug(1,$SQL_EXPORT);
	
	$rsPersvkExport = awisOpenRecordset($con,$SQL_EXPORT,true,true);
	$rsPersvkExportZeilen = $awisRSZeilen;
	
	$SQL_EXPORT_UEBERSCHRIFT=str_replace("''","'",$Params[12]);
	
	//awis_Debug(1,$SQL_EXPORT_UEBERSCHRIFT);
	
	$rsPersvkExportUeberschrift = awisOpenRecordset($con,$SQL_EXPORT_UEBERSCHRIFT,true,true);
	$rsPersvkExportUeberschriftZeilen = $awisRSZeilen;
	
	
	/***************************************
	* Neue PDF Datei erstellen
	***************************************/
	ob_clean();
	
	define('FPDF_FONTPATH','font/');
	$pdf = new fpdi('l','mm','a4');
	$pdf->open();
	$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
	$ATULogo = $pdf->ImportPage(1);	
	
	$Zeile=0;
	$Seitenrand=15;
	$Seite=1;
	
	NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,$Seite);
	
	$LinkerRand=$Seitenrand;
		
	$Y_Wert=16;
	
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(50,6,"Personalverkauf",0,0,'L',0);
	
	$Y_Wert=$Y_Wert+5;
	
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(50,6,"Zeitraum: " . $rsPersvkExport['PEV_KAUFDATUM'][0] . " bis " . $rsPersvkExport['PEV_KAUFDATUM'][$rsPersvkExportZeilen-1],0,0,'L',0);
		
	$Y_Wert=$Y_Wert+13;
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(30,6,"PER-NR",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+30,$Y_Wert);
	$pdf->cell(30,6,"Name",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+60,$Y_Wert);
	$pdf->cell(30,6,"Menge-Gesamt",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+90,$Y_Wert);
	$pdf->cell(30,6,"VK-Gesamt",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+120,$Y_Wert);
	$pdf->cell(30,6,"Rabatt/Ware",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+150,$Y_Wert);
	$pdf->cell(30,6,"Nachlass/Dienstl.",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+180,$Y_Wert);
	$pdf->cell(50,6,"Nachlass/Dienstl.I-Gesamt",1,0,'C',1);
	
	
	for($DSNr=0;$DSNr<$rsPersvkExportUeberschriftZeilen;$DSNr++)
	{
		if ($Y_Wert > 185)
		{
			$Y_Wert=16;
			$Y_Wert=$Y_Wert+10;
			$Seite = $Seite + 1;
			
			NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,$Seite);
			
			$pdf->SetFont('Arial','B',10);
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(30,6,"PER-NR",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+30,$Y_Wert);
			$pdf->cell(30,6,"Name",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+60,$Y_Wert);
			$pdf->cell(30,6,"Menge-Gesamt",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+90,$Y_Wert);
			$pdf->cell(30,6,"VK-Gesamt",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+120,$Y_Wert);
			$pdf->cell(30,6,"Rabatt/Ware",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+150,$Y_Wert);
			$pdf->cell(30,6,"Nachlass/Dienstl.",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+180,$Y_Wert);
			$pdf->cell(50,6,"Nachlass/Dienstl.I-Gesamt",1,0,'C',1);
		}
		
		$Y_Wert = $Y_Wert + 6;
		$pdf->setFont('Arial','',9);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,$rsPersvkExportUeberschrift['PEV_PERSONALNUMMER'][$DSNr],1,0,'C',0);
	
		$pdf->setXY($LinkerRand+30,$Y_Wert);
		$pdf->cell(30,6,$rsPersvkExportUeberschrift['PEV_KAEUFERNAME'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+60,$Y_Wert);
		$pdf->cell(30,6,$rsPersvkExportUeberschrift['MENGE'][$DSNr],1,0,'R',0);
		
		$pdf->setXY($LinkerRand+90,$Y_Wert);
		$pdf->cell(30,6,awis_formatZahl($rsPersvkExportUeberschrift['VK'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+120,$Y_Wert);
		$pdf->cell(30,6,awis_formatZahl($rsPersvkExportUeberschrift['RABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(30,6,awis_formatZahl($rsPersvkExportUeberschrift['MANRABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+180,$Y_Wert);
		$pdf->cell(50,6,awis_formatZahl($rsPersvkExportUeberschrift['RABATTGESAMT'][$DSNr],'Standardzahl'),1,0,'R',0);		
	}
	
	$Y_Wert=$Y_Wert+13;
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(20,6,"Filiale",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+20,$Y_Wert);
	$pdf->cell(20,6,"Datum",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+40,$Y_Wert);
	$pdf->cell(20,6,"Zeit",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+60,$Y_Wert);
	$pdf->cell(10,6,"BSA",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+70,$Y_Wert);
	$pdf->cell(20,6,"Artikel-Nr",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+90,$Y_Wert);
	$pdf->cell(20,6,"Menge",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+110,$Y_Wert);
	$pdf->cell(20,6,"VK",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+130,$Y_Wert);
	$pdf->cell(20,6,"VK-Gesamt",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+150,$Y_Wert);
	$pdf->cell(20,6,"Rabatt",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+170,$Y_Wert);
	$pdf->cell(20,6,"Nachlass",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+190,$Y_Wert);
	$pdf->cell(20,6,"Umsatz",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+210,$Y_Wert);
	$pdf->cell(25,6,"WA-NR",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+235,$Y_Wert);
	$pdf->cell(25,6,"BON-NR",1,0,'C',1);
		
	for($DSNr=0;$DSNr<$rsPersvkExportZeilen;$DSNr++)
	{		
		if ($Y_Wert > 185)
		{
			$Y_Wert=16;
			$Y_Wert=$Y_Wert+10;
			$Seite = $Seite + 1;
			
			NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,$Seite);
			
			$pdf->SetFont('Arial','B',10);
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(20,6,"Filiale",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+20,$Y_Wert);
			$pdf->cell(20,6,"Datum",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+40,$Y_Wert);
			$pdf->cell(20,6,"Zeit",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+60,$Y_Wert);
			$pdf->cell(10,6,"BSA",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+70,$Y_Wert);
			$pdf->cell(20,6,"Artikel-Nr",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+90,$Y_Wert);
			$pdf->cell(20,6,"Menge",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+110,$Y_Wert);
			$pdf->cell(20,6,"VK",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+130,$Y_Wert);
			$pdf->cell(20,6,"VK-Ges",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+150,$Y_Wert);
			$pdf->cell(20,6,"Rabatt",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+170,$Y_Wert);
			$pdf->cell(20,6,"Nachlass",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+190,$Y_Wert);
			$pdf->cell(20,6,"Umsatz",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+210,$Y_Wert);
			$pdf->cell(25,6,"WA-NR",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+235,$Y_Wert);
			$pdf->cell(25,6,"BON-NR",1,0,'C',1);
		}
		
		$Y_Wert = $Y_Wert + 6;
		$pdf->setFont('Arial','',9);		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['PEV_FIL_ID'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+20,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['PEV_KAUFDATUM'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+40,$Y_Wert);
		$pdf->cell(20,6,substr($rsPersvkExport['PEV_KAUFZEIT'][$DSNr],0,2) . ':' . substr($rsPersvkExport['PEV_KAUFZEIT'][$DSNr],2,2) . ':' . substr($rsPersvkExport['PEV_KAUFZEIT'][$DSNr],4,2),1,0,'C',0);
		
		$pdf->setXY($LinkerRand+60,$Y_Wert);
		$pdf->cell(10,6,$rsPersvkExport['PEV_BSA'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['PEV_AST_ATUNR'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+90,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['PEV_MENGE'][$DSNr],1,0,'R',0);
		
		$pdf->setXY($LinkerRand+110,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_VK'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['VK_GES'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_PERSRABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+170,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_MANRABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+190,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_UMSATZ'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+210,$Y_Wert);
		$pdf->cell(25,6,$rsPersvkExport['PEV_WANR'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+235,$Y_Wert);
		$pdf->cell(25,6,$rsPersvkExport['PEV_BONNR'][$DSNr],1,0,'C',0);		
	}
						
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	//$pdf->saveas($DateiName);
	$pdf->output();
	
	//echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
	//echo "<hr><a href=./persvk_Main.php><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";

}

if ($_REQUEST['Export']=='Filiale')
{		
	$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
	
	$SQL_EXPORT=str_replace("''","'",$Params[8]);
	
	//awis_Debug(1,$SQL_EXPORT);
	
	$rsPersvkExport = awisOpenRecordset($con,$SQL_EXPORT,true,true);
	$rsPersvkExportZeilen = $awisRSZeilen;
	
	$SQL_EXPORT_UEBERSCHRIFT="SELECT personalverkaeufe.pev_personalnummer, personalverkaeufe.pev_kaeufername, ";		  
	$SQL_EXPORT_UEBERSCHRIFT.="sum(personalverkaeufe.pev_menge) AS menge, sum((personalverkaeufe.pev_vk*personalverkaeufe.pev_menge)) as vk, sum(personalverkaeufe.pev_persrabatt) as rabatt, ";
	$SQL_EXPORT_UEBERSCHRIFT.= "sum(nvl(personalverkaeufe.pev_manrabatt,0)) as manrabatt, ";
	$SQL_EXPORT_UEBERSCHRIFT.= "sum(nvl(personalverkaeufe.pev_rabatt,0)) as maschinellerrabatt, ";
	$SQL_EXPORT_UEBERSCHRIFT.= "sum(nvl(personalverkaeufe.pev_persrabatt,0)+nvl(personalverkaeufe.pev_manrabatt,0)) as rabattgesamt ";
	$SQL_EXPORT_UEBERSCHRIFT.="FROM personalverkaeufe ";
	$SQL_EXPORT_UEBERSCHRIFT.="WHERE (personalverkaeufe.pev_personalnummer=".$Params[6].") ";
	$SQL_EXPORT_UEBERSCHRIFT.="AND (personalverkaeufe.pev_fil_id=".$Params[7].") ";
	
	If($Params[0]==1)
	{	
		if($Params[5]=='')
		{
			$SQL_EXPORT_UEBERSCHRIFT.="AND personalverkaeufe.pev_kaufdatum=to_date('".$Params[4]."','DD.MM.RR') ";
		}
		else
		{
			$SQL_EXPORT_UEBERSCHRIFT.="AND (personalverkaeufe.pev_kaufdatum>=to_date('".$Params[4]."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('".$Params[5]."','DD.MM.RR')) ";
		}
	}
	
	If($Params[9]!='')
	{
		$SucheArtbez = $Params[9];
		
	   	if(substr($Params[9],-1,1)!='*' AND substr($Params[9],-1,1)!='%')
    	{
    		//$Params[9] .='%';
    		$SucheArtbez .='%';
    	}
    	
    	if(substr($Params[9],1,1)!='*' AND substr($Params[9],1,1)!='%')
    	{
    		$SucheArtbez = '%'.$SucheArtbez;
    	}
    	
		$SQL_EXPORT_UEBERSCHRIFT.="AND ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($SucheArtbez,TRUE,FALSE,FALSE,0).")"; 		
	}
	
	If($Params[11]!='')
	{
		$SQL_EXPORT_UEBERSCHRIFT.="AND ( upper(personalverkaeufe.pev_ast_atunr) ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0).")"; 		
	}
	
	$SQL_EXPORT_UEBERSCHRIFT.="GROUP BY personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername";			

	//awis_Debug(1,$DateiName);
	//awis_Debug(1,$SQL_EXPORT_UEBERSCHRIFT);
	
	$rsPersvkExportUeberschrift = awisOpenRecordset($con,$SQL_EXPORT_UEBERSCHRIFT,true,true);
	$rsPersvkExportUeberschriftZeilen = $awisRSZeilen;
	
	
	/***************************************
	* Neue PDF Datei erstellen
	***************************************/
	ob_clean();
	
	define('FPDF_FONTPATH','font/');
	$pdf = new fpdi('l','mm','a4');
	$pdf->open();
	$pdf->setSourceFile("../bilder/atulogo_grau.pdf");
	$ATULogo = $pdf->ImportPage(1);	
	
	$Zeile=0;
	$Seitenrand=15;
	$Seite=1;
	
	NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,$Seite);
	
	$LinkerRand=$Seitenrand;
		
	$Y_Wert=16;
	
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(50,6,"Personalverkauf",0,0,'L',0);
	
	$Y_Wert=$Y_Wert+5;
	
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(50,6,"Zeitraum: " . $rsPersvkExport['PEV_KAUFDATUM'][0] . " bis " . $rsPersvkExport['PEV_KAUFDATUM'][$rsPersvkExportZeilen-1],0,0,'L',0);
		
	$Y_Wert=$Y_Wert+13;
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(30,6,"PER-NR",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+30,$Y_Wert);
	$pdf->cell(30,6,"Name",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+60,$Y_Wert);
	$pdf->cell(30,6,"Menge-Gesamt",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+90,$Y_Wert);
	$pdf->cell(30,6,"VK-Gesamt",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+120,$Y_Wert);
	$pdf->cell(30,6,"Rabatt/Ware",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+150,$Y_Wert);
	$pdf->cell(30,6,"Nachlass/Dienstl.",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+180,$Y_Wert);
	$pdf->cell(50,6,"Nachlass/Dienstl.I-Gesamt",1,0,'C',1);
	
	
	for($DSNr=0;$DSNr<$rsPersvkExportUeberschriftZeilen;$DSNr++)
	{
		if ($Y_Wert > 185)
		{
			$Y_Wert=16;
			$Y_Wert=$Y_Wert+10;
			$Seite = $Seite + 1;
			
			NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,$Seite);
			
			$pdf->SetFont('Arial','B',10);
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(30,6,"PER-NR",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+30,$Y_Wert);
			$pdf->cell(30,6,"Name",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+60,$Y_Wert);
			$pdf->cell(30,6,"Menge-Gesamt",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+90,$Y_Wert);
			$pdf->cell(30,6,"VK-Gesamt",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+120,$Y_Wert);
			$pdf->cell(30,6,"Rabatt/Ware",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+150,$Y_Wert);
			$pdf->cell(30,6,"Nachlass/Dienstl.",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+180,$Y_Wert);
			$pdf->cell(50,6,"Nachlass/Dienstl.I-Gesamt",1,0,'C',1);
		}
		
		$Y_Wert = $Y_Wert + 6;
		$pdf->setFont('Arial','',9);
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(30,6,$rsPersvkExportUeberschrift['PEV_PERSONALNUMMER'][$DSNr],1,0,'C',0);
	
		$pdf->setXY($LinkerRand+30,$Y_Wert);
		$pdf->cell(30,6,$rsPersvkExportUeberschrift['PEV_KAEUFERNAME'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+60,$Y_Wert);
		$pdf->cell(30,6,$rsPersvkExportUeberschrift['MENGE'][$DSNr],1,0,'R',0);
		
		$pdf->setXY($LinkerRand+90,$Y_Wert);
		$pdf->cell(30,6,awis_formatZahl($rsPersvkExportUeberschrift['VK'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+120,$Y_Wert);
		$pdf->cell(30,6,awis_formatZahl($rsPersvkExportUeberschrift['RABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(30,6,awis_formatZahl($rsPersvkExportUeberschrift['MANRABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+180,$Y_Wert);
		$pdf->cell(50,6,awis_formatZahl($rsPersvkExportUeberschrift['RABATTGESAMT'][$DSNr],'Standardzahl'),1,0,'R',0);		
	}
	
	$Y_Wert=$Y_Wert+13;
	$pdf->SetFont('Arial','B',10);
	$pdf->setXY($LinkerRand,$Y_Wert);
	$pdf->cell(20,6,"Filiale",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+20,$Y_Wert);
	$pdf->cell(20,6,"Datum",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+40,$Y_Wert);
	$pdf->cell(20,6,"Zeit",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+60,$Y_Wert);
	$pdf->cell(10,6,"BSA",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+70,$Y_Wert);
	$pdf->cell(20,6,"Artikel-Nr",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+90,$Y_Wert);
	$pdf->cell(20,6,"Menge",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+110,$Y_Wert);
	$pdf->cell(20,6,"VK",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+130,$Y_Wert);
	$pdf->cell(20,6,"VK-Ges",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+150,$Y_Wert);
	$pdf->cell(20,6,"Rabatt",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+170,$Y_Wert);
	$pdf->cell(20,6,"Nachlass",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+190,$Y_Wert);
	$pdf->cell(20,6,"Umsatz",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+210,$Y_Wert);
	$pdf->cell(25,6,"WA-NR",1,0,'C',1);
	
	$pdf->setXY($LinkerRand+235,$Y_Wert);
	$pdf->cell(25,6,"BON-NR",1,0,'C',1);
		
	for($DSNr=0;$DSNr<$rsPersvkExportZeilen;$DSNr++)
	{		
		if ($Y_Wert > 185)
		{
			$Y_Wert=16;
			$Y_Wert=$Y_Wert+10;
			$Seite = $Seite + 1;
			
			NeueSeite($pdf,$Zeile,$ATULogo,$Seitenrand,$Seite);
			
			$pdf->SetFont('Arial','B',10);
			$pdf->setXY($LinkerRand,$Y_Wert);
			$pdf->cell(20,6,"Filiale",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+20,$Y_Wert);
			$pdf->cell(20,6,"Datum",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+40,$Y_Wert);
			$pdf->cell(20,6,"Zeit",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+60,$Y_Wert);
			$pdf->cell(10,6,"BSA",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+70,$Y_Wert);
			$pdf->cell(20,6,"Artikel-Nr",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+90,$Y_Wert);
			$pdf->cell(20,6,"Menge",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+110,$Y_Wert);
			$pdf->cell(20,6,"VK",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+130,$Y_Wert);
			$pdf->cell(20,6,"VK-Gesamt",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+150,$Y_Wert);
			$pdf->cell(20,6,"Rabatt",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+170,$Y_Wert);
			$pdf->cell(20,6,"Nachlass",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+190,$Y_Wert);
			$pdf->cell(20,6,"Umsatz",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+210,$Y_Wert);
			$pdf->cell(25,6,"WA-NR",1,0,'C',1);
			
			$pdf->setXY($LinkerRand+235,$Y_Wert);
			$pdf->cell(25,6,"BON-NR",1,0,'C',1);
		}
		
		$Y_Wert = $Y_Wert + 6;
		$pdf->setFont('Arial','',9);		
		$pdf->setXY($LinkerRand,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['FIL_ID'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+20,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['PEV_KAUFDATUM'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+40,$Y_Wert);
		$pdf->cell(20,6,substr($rsPersvkExport['PEV_KAUFZEIT'][$DSNr],0,2) . ':' . substr($rsPersvkExport['PEV_KAUFZEIT'][$DSNr],2,2) . ':' . substr($rsPersvkExport['PEV_KAUFZEIT'][$DSNr],4,2),1,0,'C',0);
		
		$pdf->setXY($LinkerRand+60,$Y_Wert);
		$pdf->cell(10,6,$rsPersvkExport['PEV_BSA'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+70,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['PEV_AST_ATUNR'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+90,$Y_Wert);
		$pdf->cell(20,6,$rsPersvkExport['PEV_MENGE'][$DSNr],1,0,'R',0);
		
		$pdf->setXY($LinkerRand+110,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_VK'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+130,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['VK_GES'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+150,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_PERSRABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+170,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_MANRABATT'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+190,$Y_Wert);
		$pdf->cell(20,6,awis_formatZahl($rsPersvkExport['PEV_UMSATZ'][$DSNr],'Standardzahl'),1,0,'R',0);
		
		$pdf->setXY($LinkerRand+210,$Y_Wert);
		$pdf->cell(25,6,$rsPersvkExport['PEV_WANR'][$DSNr],1,0,'C',0);
		
		$pdf->setXY($LinkerRand+235,$Y_Wert);
		$pdf->cell(25,6,$rsPersvkExport['PEV_BONNR'][$DSNr],1,0,'C',0);		
	}
						
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	//$pdf->saveas($DateiName);
	$pdf->output();
	
	//echo "<br><a target=_new href=$DateiNameLink>PDF Datei �ffnen</a><p>";
	//echo "<hr><a href=./persvk_Main.php><img border=0 src=/bilder/zurueck.png title='Zur&uuml;ck'></a>";

}	
	
/**
*
* Funktion erzeugt eine neue Seite mit Ueberschriften
*
* @author Thomas Riedl
* @param  pointer pdf
* @param  pointer Zeile
* @param  resource ATULogo
* @param  int LinkerRand
* @param int Seite
*/
function NeueSeite(&$pdf,&$Zeile,$ATULogo,$LinkerRand,&$Seite)
{
	static $Seite;
	
	$pdf->addpage();									// Neue Seite hinzuf�gen
	$pdf->SetAutoPageBreak(true,0);
	$pdf->useTemplate($ATULogo,$LinkerRand+250,4,20);		// Logo einbauen
	
	$Seite++;
	$pdf->SetFont('Arial','',6);					// Schrift setzen
	
	$pdf->setXY($LinkerRand,200);					// Cursor setzen
	$pdf->Cell(270,3,'Seite '. $Seite,0,0,'C',0);
	//$pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
		
	$pdf->setXY($LinkerRand,5);					// Cursor setzen
	$pdf->SetFont('Arial','B',14);				// Schrift setzen
	
	// Ueberschrift
	//$pdf->SetFillColor(255,255,255);
	//$pdf->cell(180,6,"Differenzliste von Lieferschein-Nr: ".$LSNR,0,0,'C',0);
	
	// Ueberschrift setzen
	$pdf->SetFillColor(210,210,210);
	$pdf->SetFont('Arial','',10);				// Schrift setzen
	
	$Zeile = 22;
}

/*
*	@author Thomas Riedl
*	@param mixed $Ausdruck
*	@param string $Format
*	@param string $KommaZeichen
*	@return string
*	@version 1.0
*
*****************************************************************************************************/
function awis_formatZahl($Ausdruck, $Format, $KommaZeichen = ',')
{
	$Erg = '';
	$NK = 0;

	if(substr($Format,0,12)=='Standardzahl')
	{
		$NK = substr($Format,13,9);
		if($NK=='')
		{
			$NK=2;
		}
		$Erg = str_replace($KommaZeichen,'.',$Ausdruck);
		$Erg = number_format((float)$Erg,$NK,',','.') . '';
		//$Erg = str_replace("-","&minus;" , $Erg );
	}
	else
	{
		$Erg = $Ausdruck;
	}

	return $Erg;

}
	
?>
</body>
</html>
