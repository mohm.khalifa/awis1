<?php
require_once('awisDokumente.php');

global $Param;
global $awisRSInfo;
global $awisDBError;
global $awisDBFehler;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWISBenutzer;
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');
$TextKonserven[]=array('Fehler','err_UntergeordneterDatensatz');

$TXT_AdrLoeschen = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

awis_Debug(1,$_POST,$_GET);
$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
{
	$Tabelle = 'CAD';
	$Key=$_POST['txtCAD_KEY'];

	$Felder=array();
	$Felder[]=array(awis_TextKonserve($con,'CAD_NAME1','CAD',$AWISSprache),$_POST['txtCAD_NAME1']);
	$Felder[]=array(awis_TextKonserve($con,'CAD_NAME2','CAD',$AWISSprache),$_POST['txtCAD_NAME2']);
	$Felder[]=array(awis_TextKonserve($con,'CAD_ORT','CAD',$AWISSprache),$_POST['txtCAD_ORT']);

}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	if(isset($_GET['Unterseite']))
	{
		switch($_GET['Unterseite'])
		{
			case 'Kontaktdaten':
				$Tabelle = 'CAK';
				$Key=$_GET['Del'];

				$SQL = 'SELECT CAK_WERT, KOT_TYP ';
				$SQL .= ' FROM CRMAnsprechpartnerKontakte INNER JOIN KommunikationsTypen ON CAK_KOT_KEY = KOT_KEY';
				$SQL .= ' WHERE CAK_KEY=0'.$Key;
				$rsDaten = awisOpenRecordset($con, $SQL);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CAK_WERT','CAK',$AWISSprache),$rsDaten['CAK_WERT'][0]);
				$Felder[]=array(awis_TextKonserve($con,'KOT_TYP','KOT',$AWISSprache),$rsDaten['KOT_TYP'][0]);
				break;
		}
	}
	else
	{
		switch($_GET['Seite'])
		{
			case 'Ansprechpartner':
				$Tabelle = 'CAP';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT COUNT(*) AS ANZ FROM CRMKontakte WHERE CKT_CAP_KEY = 0'.$Key);
				if($rsDaten['ANZ'][0]>0)
				{
					echo '<form name=frmLoeschen action=./crm_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
					echo '<span class=HinweisText>Der Ansprechpartner kann nicht gel&ouml;scht werden, da er in Kontakten hinterlegt ist.</span>';
					echo '<br>';
					echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value="OK">';
				
					echo '</form>';
					awisLogoff($con);
					die();
				}
				
				$rsDaten = awisOpenRecordset($con, 'SELECT CAP_CAD_KEY,CAP_NACHNAME, CAP_VORNAME FROM CRMAnsprechpartner WHERE CAP_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CAP_NACHNAME','CAP',$AWISSprache),$rsDaten['CAP_NACHNAME'][0]);
				$Felder[]=array(awis_TextKonserve($con,'CAP_VORNAME','CAP',$AWISSprache),$rsDaten['CAP_VORNAME'][0]);
				break;
			case 'Aktionen':
				$Tabelle = 'CKO';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT CKO_BEMERKUNG, CAT_BEZEICHNUNG FROM CRMAktionen INNER JOIN CRMAktionenTypen ON CKO_CAT_KEY = CAT_KEY WHERE CKO_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CKO_BEMERKUNG','CKO',$AWISSprache),$rsDaten['CKO_BEMERKUNG'][0]);
				$Felder[]=array(awis_TextKonserve($con,'CAT_BEZEICHNUNG','CAT',$AWISSprache),$rsDaten['CAT_BEZEICHNUNG'][0]);
				break;
			case 'Wiedervorlagen':
				$Tabelle = 'CWV';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT CWV_BEMERKUNG, CWV_DATUM FROM CRMWiedervorlagen WHERE CWV_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CWV_BEMERKUNG','CWV',$AWISSprache),$rsDaten['CWV_BEMERKUNG'][0]);
				$Felder[]=array(awis_TextKonserve($con,'CWV_DATUM','CWV',$AWISSprache),$rsDaten['CWV_DATUM'][0]);
				break;
			case 'Kontakte':
				$Tabelle = 'CKT';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT CKT_DATUM FROM CRMKontakte WHERE CKT_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CKT_DATUM','CKT',$AWISSprache),$rsDaten['CKT_DATUM'][0]);
				break;
			case 'Kontaktarten':
				$Tabelle = 'CKA';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT CKA_BEZEICHNUNG FROM CRMKontaktarten WHERE CKA_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CKA_BEZEICHNUNG','CKA',$AWISSprache),$rsDaten['CKA_BEZEICHNUNG'][0]);
				break;
			case 'Kontaktergebnisse':
				$Tabelle = 'CKE';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT CKE_BEZEICHNUNG FROM CRMKontaktErgebnisse WHERE CKE_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CKE_BEZEICHNUNG','CKE',$AWISSprache),$rsDaten['CKE_BEZEICHNUNG'][0]);
				break;
			case 'Gebiete':
				$Tabelle = 'CGB';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT CGB_GEBIET FROM CRMGebiete WHERE CGB_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CGB_GEBIET','CGB',$AWISSprache),$rsDaten['CGB_GEBIET'][0]);
				break;
			case 'Aktionentypen':
				$Tabelle = 'CAT';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT CAT_BEZEICHNUNG FROM CRMAktionentypen WHERE CAT_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'CAT_BEZEICHNUNG','CAT',$AWISSprache),$rsDaten['CAT_BEZEICHNUNG'][0]);
				break;
			case 'Teilehandel':
			case 'Reifen':
			case 'Dienstleistungen':
			case 'Warengruppen':
				if(isset($_GET['CKMKEY']))	// in den Details des Modells
				{
					$Tabelle = 'CKD';
					$Key=$_GET['Del'];

					$rsDaten = awisOpenRecordset($con, 'SELECT CKD_BEZEICHNUNG FROM CRMKONDITIONSMODELLDETAILS WHERE CKD_KEY=0'.$Key);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'CKD_BEZEICHNUNG','CKD',$AWISSprache),$rsDaten['CKD_BEZEICHNUNG'][0]);
				}
				else
				{
					$Tabelle = 'CKM';
					$Key=$_GET['Del'];

					$rsDaten = awisOpenRecordset($con, 'SELECT CKM_BEZEICHNUNG FROM CRMKONDITIONSMODELLE WHERE CKM_KEY=0'.$Key);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'CKM_BEZEICHNUNG','CKM',$AWISSprache),$rsDaten['CKM_BEZEICHNUNG'][0]);
				}
				break;
			case 'Dokumente':
				$Tabelle = 'DOC';
				$Key=$_GET['Del'];

				$rsDaten = awisOpenRecordset($con, 'SELECT DOC_DATUM, DOC_BEZEICHNUNG FROM Dokumente WHERE DOC_KEY=0'.$Key);
				$Felder=array();

				$Felder[]=array(awis_TextKonserve($con,'DOC_DATUM','DOC',$AWISSprache),$rsDaten['DOC_DATUM'][0]);
				$Felder[]=array(awis_TextKonserve($con,'DOC_BEZEICHNUNG','DOC',$AWISSprache),$rsDaten['DOC_BEZEICHNUNG'][0]);
				break;
			case 'Pflichtfelder':
				$Tabelle = 'CPF';
				$Key=$_GET['Del'];
				
				//$rsDaten = awisOpenRecordset($con, 'SELECT CPF_PFLICHTFELD FROM crmpflichtfelder WHERE CPF_KEY=0'.$Key);
				$Felder=array();
				
				//$Felder[]=array(awis_TextKonserve($con,'CPF_PFLICHTFELD','CPF',$AWISSprache),$rsDaten['CPF_PFLICHTFELD'][0]);
				break;
            case 'Kondblattversand':
                $Tabelle = 'CKV';
                $Key = $_GET['Del'];

                $rsDaten = awisOpenRecordset($con, 'SELECT CKV_DATUM, CKV_NAME FROM CRMKONDBLATTVERSAND WHERE CKV_KEY =0'.$Key);
                $Felder=array();

                $Felder[]=array(awis_TextKonserve($con,'CKV_DATUM','CKV',$AWISSprache),$rsDaten['CKV_DATUM'][0]);
                $Felder[]=array(awis_TextKonserve($con, 'CKV_NAME','CKV',$AWISSprache),$rsDaten['CKV_NAME'][0]);

                break;
		}
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{

//awis_Debug(1,$_POST);
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'CAD':
			$SQL = 'DELETE FROM CRMAdressen WHERE CAD_key=0'.$_POST['txtKey'];
			awis_BenutzerParameterSpeichern($con, "AktuellerCAD" , $AWISBenutzer->BenutzerName(),'');
			$AWIS_KEY1=0;
			break;
		case 'CAP':
			$SQL = 'DELETE FROM CRMAnsprechpartnerKontakte WHERE CAK_CAP_key=0'.$_POST['txtKey'];
			if(awisExecute($con,$SQL)===false)
			{
				awisErrorMailLink('crm_loeschen_1',1,$awisDBError['messages'],'');
			}
			$SQL = 'DELETE FROM CRMAnsprechpartner WHERE CAP_key=0'.$_POST['txtKey'];
			$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerCAD',$AWISBenutzer->BenutzerName());
			break;
		case 'CAK':
			$SQL = 'DELETE FROM CRMAnsprechpartnerKontakte WHERE CAK_key=0'.$_POST['txtKey'];
			$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerCAD',$AWISBenutzer->BenutzerName());
			$AWIS_KEY2 = awis_BenutzerParameter($con,'AktuellerCAP',$AWISBenutzer->BenutzerName());
			break;
		case 'CKO':
			$SQL = 'DELETE FROM CRMAktionen WHERE CKO_key=0'.$_POST['txtKey'];
			$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerCAD',$AWISBenutzer->BenutzerName());
			break;
		case 'CKM':
			$SQL = 'DELETE FROM CRMKONDITIONSMODELLE WHERE CKM_key=0'.$_POST['txtKey'];
			break;
		case 'CKD':
			$SQL = 'DELETE FROM CRMKONDITIONSMODELLDETAILS WHERE CKD_key=0'.$_POST['txtKey'];
			break;
		case 'CWV':
			$SQL = 'DELETE FROM CRMWiedervorlagen WHERE CWV_key=0'.$_POST['txtKey'];
			$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerCAD',$AWISBenutzer->BenutzerName());
			break;
		case 'CKT':
			$SQL = 'DELETE FROM CRMKontakte WHERE CKT_key=0'.$_POST['txtKey'];
			$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerCAD',$AWISBenutzer->BenutzerName());
			break;
		case 'CKA':
			$SQL = 'DELETE FROM CRMKontaktarten WHERE CKA_key=0'.$_POST['txtKey'];
			break;
		case 'CKE':
			$SQL = 'DELETE FROM CRMKontaktergebnisse WHERE CKE_key=0'.$_POST['txtKey'];
			break;
		case 'CGB':
			$SQL = 'DELETE FROM CRMGebiete WHERE CGB_key=0'.$_POST['txtKey'];
			break;
		case 'CAT':
			$SQL = 'DELETE FROM CRMAktionenTypen WHERE CAT_key=0'.$_POST['txtKey'];
			break;
		case 'DOC':
			$SQL = '';
			$DOC = new awisDokumente();
			$DOC->DokumentLoeschen($_POST['txtKey']);
			$AWIS_KEY1 = awis_BenutzerParameter($con,'AktuellerCAD',$AWISBenutzer->BenutzerName());
		case 'CPF':
			$SQL = 'DELETE FROM CRMPflichtfelder WHERE cpf_key=0'.$_POST['txtKey'];
			break;
        case 'CKV':
            $SQL = 'DELETE FROM CRMKONDBLATTVERSAND WHERE ckv_key=0'.$_POST['txtKey'];
            break;
		default:
			break;
	}

	if($SQL !='')
	{
		if(awisExecute($con,$SQL)===false)
		{
			if(strpos($awisDBFehler['message'],'ORA-02292')!==false)
			{
				awisFORM_Meldung(1, $TXT_AdrLoeschen['Fehler']['err_UntergeordneterDatensatz']);
			}
			else
			{
				awisErrorMailLink('crm_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}
}

if($Tabelle!='')
{
	echo '<form name=frmLoeschen action=./crm_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
	echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': '.$Feld[1];
	}

	echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
	echo '<input type=hidden name=txtKey value="'.$Key.'">';

	echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
	echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';

	echo '</form>';
	awisLogoff($con);
	die();
}


?>