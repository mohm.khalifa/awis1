<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once '../grosskundenkarten/grosskundenkarten_funktionen.php';

$GK = new grosskundenkarten_funktionen();
if (isset($_GET['sucKUNDEN_NR'])) {
    $GK->KundenDatenAJAX($_GET['sucKUNDEN_NR']);
} elseif (isset($_GET['sucERM_NR'])) {
    $GK->ModellDatenAJAX($_GET['sucERM_NR']);
}
