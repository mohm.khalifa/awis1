<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamte Bereich
{
	$Tabelle = 'FER';
	$Key=$_POST['txtFER_KEY'];
	$FER_KEY=$_POST['txtFER_KEY'];

	$Felder=array();

	$Felder[]=array(awis_TextKonserve($con,'FER_BEZEICHNUNG','FER',$AWISSprache),$_POST['txtFER_BEZEICHNUNG']);
}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	switch($_GET['Seite'])
	{
		case 'xxx':
			$Tabelle = 'xxx';
			$Key=$_GET['Del'];
	
			$rsDaten = awisOpenRecordset($con, 'xxx WHERE FBR_KEY=0'.$Key);
			$Felder=array();
			$Felder[]=array(awis_TextKonserve($con,'xxx','XXX',$AWISSprache),$rsDaten['xxx'][0]);
			$FER_KEY = $rsDaten['xxx'][0];
			break;
	}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'FER':
			$SQL = 'DELETE FROM filialebenenrollen WHERE FER_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
			break;
		default:
			break;
	}

	if($SQL !='')
	{
		if(awisExecute($con,$SQL)==false)
		{
			awisErrorMailLink('filialebenen_stammdaten_loeschen',1,$awisDBError['messages'],'');
		}
		awis_BenutzerParameterSpeichern($con, "AktuellerFilialBereich" , $AWISBenutzer->BenutzerName(), $AWIS_KEY1);
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

	echo '<form name=frmLoeschen action=./filialebenen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
	echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': '.$Feld[1];
	}

	echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
	echo '<input type=hidden name=txtKey value="'.$Key.'">';
	echo '<input type=hidden name=txtFER_KEY value="'.$FER_KEY.'">';

	echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
	echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';

	echo '</form>';
	awisLogoff($con);
	die();
}
?>