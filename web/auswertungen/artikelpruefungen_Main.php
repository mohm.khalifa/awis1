<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;
global $awisRSZeilen;

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body>
<?php
include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

echo '<h2>Export Artikelpr�fungen</h2>';

echo '<form name=frmPruefungen method=POST>';
echo '<table border=0>';

echo '<tr>';
echo '<td width=150 class=FeldBez>ATU-Nr</td><td><input name=txtAST_ATUNR size=10 value=\''.(isset($_POST['txtAST_ATUNR'])?$_POST['txtAST_ATUNR']:'').'\'></td>';
echo '</tr>';

echo '<tr>';
echo '<td width=150 class=FeldBez>Lieferant</td><td><input name=txtLIE_Nr size=10 value=\''.(isset($_POST['txtLIE_Nr'])?$_POST['txtLIE_Nr']:'').'\'></td>';
echo '</tr>';

echo '<tr>';
echo '<td width=150 class=FeldBez>Datum vom</td><td><input name=txtDatumVom size=10 value=\''.(isset($_POST['txtDatumVom'])?$_POST['txtDatumVom']:'').'\'></td>';
echo '</tr>';


echo '<tr>';
echo '<td width=150 class=FeldBez>Datum bis</td><td><input name=txtDatumBis size=10 value=\''.(isset($_POST['txtDatumBis'])?$_POST['txtDatumBis']:'').'\'></td>';
echo '</tr>';

echo '</table>';


if(awis_NameInArray($_POST,'cmdSuche',0)!='')
{
	echo '<hr>';
	$SQL = 'SELECT *';
	$SQL .= ' FROM ARTIKELPRUEFUNGEN ';
	$SQL .= ' LEFT OUTER JOIN Lieferanten ON APR_LIE_NR = LIE_NR';
	$SQL .= ' INNER JOIN ARTIKELSTAMM ON APR_AST_ATUNR = AST_ATUNR';
	$SQL .= ' INNER JOIN WARENUNTERGRUPPEN ON AST_WUG_KEY = WUG_KEY';

	$Bedingung = '';
	if($_POST['txtAST_ATUNR']!=='')
	{
		$Bedingung = ' AND APR_AST_ATUNR ' . awisLIKEoderIST($_POST['txtAST_ATUNR'],true);
	}
	if($_POST['txtLIE_Nr']!=='')
	{
		$Bedingung .= ' AND APR_LIE_NR = \'' . $_POST['txtLIE_Nr'] . '\'';
	}

	if($_POST['txtDatumVom']!=='')
	{
		$Bedingung .= ' AND APR_BEARBEITUNGSTAG >=TO_DATE(\'' . $_POST['txtDatumVom'] . '\',\'DD.MM.RRRR\')';
		if($_POST['txtDatumBis']=='')
		{
			$Bedingung .= ' AND APR_BEARBEITUNGSTAG <=TO_DATE(\'' . $_POST['txtDatumVom'] . '\',\'DD.MM.RRRR\')';
		}
	}

	if($_POST['txtDatumBis']!=='')
	{
		$Bedingung .= ' AND APR_BEARBEITUNGSTAG <=TO_DATE(\'' . $_POST['txtDatumBis'] . '\',\'DD.MM.RRRR\')';
	}

	if($Bedingung=='')
	{
		$Bedingung .= ' AND ROWNUM<60000';
	}

	$SQL .= ' WHERE ' . substr($Bedingung,4);


	$rsAPR = awisOpenRecordset($con, $SQL);
	$rsAPRZeilen = $awisRSZeilen;

	if($rsAPRZeilen==0)
	{
		echo '<span class=HinweisText>Es wurden keine Daten mit diesen Parametern gefunden</span>';
	}
	else
	{
		$DateiName = awis_UserExportDateiName('.csv');
		$fd = fopen($DateiName,'w' );

		fputs($fd, "Erstellungsdatum;ATU-Nr.;Lieferant;Lieferantenartikelnummer;Bez-WWS;Fehlerbeschreibung;Warengruppe;Bearbeiter\n");

		for($APRZeile=0;$APRZeile<$rsAPRZeilen;$APRZeile++)
		{
			fputs($fd, $rsAPR['APR_BEARBEITUNGSTAG'][$APRZeile]);
			fputs($fd, ';' . $rsAPR['APR_AST_ATUNR'][$APRZeile]);
			fputs($fd, ';' . $rsAPR['APR_LIE_NR'][$APRZeile]);
			fputs($fd, ';' . $rsAPR['APR_LAR_LARTNR'][$APRZeile]);
			fputs($fd, ';' . $rsAPR['AST_BEZEICHNUNGWW'][$APRZeile]);
			fputs($fd, ';' . str_replace("\r","",str_replace("\n"," \ ",str_replace(';','~',$rsAPR['APR_FEHLERBESCHREIBUNG'][$APRZeile]))));
			fputs($fd, ';' . $rsAPR['WUG_WGR_ID'][$APRZeile]);
			fputs($fd, ';' . $rsAPR['APR_BEARBEITER'][$APRZeile]);
			fputs($fd, "\n");
		}
		fclose($fd);

		$DateiName = pathinfo($DateiName);
		echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';

	}
}

echo '<hr>';

echo "<a accesskey=z href=./auswertungen_Main.php><img border=0 title=Zur�ck src=/bilder/zurueck.png name=cmdZurueck;></a>";
echo "&nbsp;<input accesskey=x type=image src=/bilder/eingabe_ok.png title='Export starten (Alt+x)' name=cmdSuche value=\"Export\">";


echo '</form>';


awisLogoff($con);

?>
<script type="text/javascript">
document.getElementsByName('txtAST_ATUNR')[0].focus();
</script>
</body>
</html>
