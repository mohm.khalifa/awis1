<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=WIN1252">
        <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
        <meta http-equiv="author" content="ATU">

        <?php
        require_once('awisDatenbank.inc');
        require_once('awisBenutzer.inc');
        require_once('awisFormular.inc');
        include_once('awisFirstglasImport.inc.php');
        include_once('awisFirstglasStatus.php');
        include_once('awisFirstglasExport.php');


        global $AWISCursorPosition;  // Aus AWISFormular

        ini_set('max_execution_time', 0);

        try
        {

            $DB = awisDatenbank::NeueVerbindung('AWIS');
            $DB->Oeffnen();
            $AWISBenutzer = awisBenutzer::Init();
            echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() . ">";
        }
        catch (Exception $ex)
        {
        	die($ex->getMessage());
        }

// Textkonserven laden
        $TextKonserven = array();
        $TextKonserven[] = array('TITEL', 'tit_FirstGlas');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_reset');
        $TextKonserven[] = array('Wort', 'lbl_hilfe');
        $TextKonserven[] = array('Wort', 'lbl_korbrauf');
        $TextKonserven[] = array('Wort', 'lbl_korbrunter');
        $TextKonserven[] = array('Wort', 'lbl_Koffer');
        $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
        $TextKonserven[] = array('Fehler', 'err_keineRechte');
        $TextKonserven[] = array('FGK', 'txt_Export_Autoglas');

        $Form = new awisFormular();
        $Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
        echo '<title>' . $AWISSprachKonserven['TITEL']['tit_FirstGlas'] . '</title>';
        ?>
    </head>
    <body>
<?php
try
{

    $ExportStornoDaten = new FirstglasExport();

    $Recht9100 = $AWISBenutzer->HatDasRecht(9100);

    if ($AWISBenutzer->HatDasRecht(9100) == 0)
    {
        $Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', -9, "200904291315");
        die();
    }

    //$Form->DebugAusgabe(1, $_REQUEST);
    //$Form->DebugAusgabe(1,$_POST);
    /*
      if (isset($_POST['cmdKorbrauf_x']))
      {
      $ExportStornoDaten->ExportStornoKasse();
      }
      elseif(isset($_POST['cmdKorbrunter_x']))
      {
      $ExportStornoDaten->ExportKassendatenohneAuftraege();
      }
      elseif(isset($_POST['cmdKoffer_x']))
      {
      $ExportStornoDaten->ExportFreigabeRechnungen();
      }
     */
    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['txt_Export_Autoglas'], 1195);
    $Form->SchreibeHTMLCode('<form name=frmFirstGlasDetails action=./autoglas_Main.php?cmdAktion=Export method=POST  enctype="multipart/form-data">');
    $Form->ZeileEnde();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('href', 'cmdKorbrauf', 'firstglas_Drucken_Stornoliste.php', '/bilder/cmd_korb_rauf.png', 'Alle Auftr�ge mit Storno und Kassendaten', 'S');
    $Form->Schaltflaeche('href', 'cmdKorbrunter', 'firstglas_Drucken_KassendatenAuftrag.php', '/bilder/cmd_korb_runter.png', 'Alle Kassendaten ohne Auftrag', 'Y');
    //$Icons[] = array('pdf', '/berichte/drucken.php?XRE=17&ID='.base64_encode('UNF_KEY=' . urlencode('=~') . $rsUNF->FeldInhalt('UNF_KEY')));
    $Form->Schaltflaeche('href', 'cmdschlosszu','/berichte/drucken.php?XRE=39&ID='.base64_encode(0), '/bilder/cmd_schloss_zu.png', 'Gesperrte Vorgaenge', 'Z');

    //$Form->Schaltflaeche('image', 'cmdKoffer', '', '/bilder/cmd_koffer.png', 'Export Freigaberechnungen', 'X');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

    $Form->Formular_Ende();
    //$Form->Formular_Ende();
}
catch (Exception $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200904291312");
    }
    else
    {
        echo 'AWIS: ' . $ex->getMessage();
    }
}
?>
    </body>
</html>