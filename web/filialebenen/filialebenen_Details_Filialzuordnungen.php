<?php
require_once('awisFilialEbenen.inc');

global $con;
global $AWISSprache;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('FEZ','%');
$TextKonserven[]=array('FIL','%');
$TextKonserven[]=array('Wort','Filialebene');
$TextKonserven[]=array('KON','KON_NAME');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3803 = awisBenutzerRecht($con,3803,$AWISBenutzer->BenutzerName());
$Recht3805 = awisBenutzerRecht($con,3805,$AWISBenutzer->BenutzerName());
if($Recht3803==0)
{
    awisEreignis(3,1000,'Filialebenenrollen',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


awis_FORM_FormularStart();

$EditRecht=(($Recht3803&6)!=0);

//***********************************	
//Zuordnungen anzeigen
//***********************************

$SQL = 'SELECT FILIALEBENENZUORDNUNGEN.*, FIL_BEZ, FIL_ID';
$SQL .= ' FROM FILIALEBENENZUORDNUNGEN';
$SQL .= ' INNER JOIN Filialen ON FEZ_FIL_ID = FIL_ID';

$Bedingung = '';

if(isset($_GET['FEZ_KEY']))
{
	$Bedingung .= ' AND FEZ_KEY = '.floatval($_GET['FEZ_KEY']);
}

$Bedingung .= ' AND FEZ_FEB_KEY = 0'.$AWIS_KEY1;

if(isset($_POST['txtFRZ_XXX_KEY']))
{	
	$Bedingung .= ' AND FEZ_FIL_ID = '.$_POST['txtFRZ_XXX_KEY'];
}
if($Bedingung!='')
{
	$SQL .= ' WHERE '.substr($Bedingung,4);	
}

if(!isset($_GET['FEZSort']))
{
	$SQL .= ' ORDER BY FIL_BEZ DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['FEZSort']);
}
$rsFEZ = awisOpenRecordset($con,$SQL);
$rsFEZZeilen = $awisRSZeilen;
awis_Debug(1,$SQL,$AWIS_KEY1,$AWIS_KEY2);

//if($rsFEZZeilen > 0 and (!isset($_GET['FEZ_KEY']) OR isset($_GET['FEZListe'])))					// Liste anzeigen
if($rsFEZZeilen > 1 OR isset($_GET['FEZListe']))					// Liste anzeigen
{
	awis_FORM_ZeileStart();

	if(($Recht3803&6)>0)
	{
		$Icons[] = array('new','./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&FEZ_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen';
	$Link .= '&FEZSort=FIL_BEZ'.((isset($_GET['FEZSort']) AND ($_GET['FEZSort']=='FIL_BEZ'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_BEZ'],400,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen';
	$Link .= '&FEZSort=FEZ_GUELTIGAB'.((isset($_GET['FEZSort']) AND ($_GET['FEZSort']=='FEZ_GUELTIGAB'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEZ']['FEZ_GUELTIGAB'],100,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen';
	$Link .= '&FEZSort=FEZ_GUELTIGBIS'.((isset($_GET['FEZSort']) AND ($_GET['FEZSort']=='FEZ_GUELTIGBIS'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FEZ']['FEZ_GUELTIGBIS'],100,'',$Link);

	awis_FORM_ZeileEnde();

	for($FEZZeile=0;$FEZZeile<$rsFEZZeilen;$FEZZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3803&2)>0 or ($Recht3805&2)>0)	// �ndernrecht
		{
			$Icons[] = array('edit','./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&FEZ_KEY='.$rsFEZ['FEZ_KEY'][$FEZZeile]);
		}
		if(($Recht3803&4)>0)	// L�schen
		{
			$Icons[] = array('delete','./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Del='.$rsFEZ['FEZ_KEY'][$FEZZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($FEZZeile%2));
	
		$Style='';

		if(awis_PruefeDatum($rsFEZ['FEZ_GUELTIGAB'][$FEZZeile],1,1) > strtotime(date('d.m.Y'),time()))
		{
			$Style='font-style:italic;color:#FF2020';
		}
		if(awis_PruefeDatum($rsFEZ['FEZ_GUELTIGBIS'][$FEZZeile],1,1) < strtotime(date('d.m.Y'),time()))
		{
			$Style='font-style:italic;color:#FF2020';
		}
		
		awis_FORM_Erstelle_ListenFeld('#FIL_BEZ',$rsFEZ['FIL_ID'][$FEZZeile].' - '.$rsFEZ['FIL_BEZ'][$FEZZeile],20,400,false,($FEZZeile%2),$Style,'','T');
		awis_FORM_Erstelle_ListenFeld('#FEZ_GUELTIGAB',$rsFEZ['FEZ_GUELTIGAB'][$FEZZeile],20,100,false,($FEZZeile%2),$Style,'','D');
		awis_FORM_Erstelle_ListenFeld('#FEZ_GUELTIGBIS',$rsFEZ['FEZ_GUELTIGBIS'][$FEZZeile],20,100,false,($FEZZeile%2),$Style,'','D');
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();
	
	echo '<input name=txtFEZ_KEY type=hidden value=0'.(isset($rsFEZ['FEZ_KEY'][0])?$rsFEZ['FEZ_KEY'][0]:'').'>';
	echo '<input name=txtFEZ_FEB_KEY type=hidden value=0'.$AWIS_KEY1.'>';	
	
	$AWIS_KEY2 = (isset($rsFEZ['FEZ_KEY'][0])?$rsFEZ['FEZ_KEY'][0]:'0');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&FEZListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsFEZ['FEZ_KEY'][0])?$rsFEZ['FEZ_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsFEZ['FEZ_KEY'][0])?$rsFEZ['FEZ_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEZ']['FEZ_FIL_ID'].':',150);
	$SQL = "select FIL_ID, FIL_ID || ' - ' || FIL_BEZ ";
	$SQL .= ' FROM Filialen';
	$SQL .= ' LEFT OUTER JOIN Filialebenenzuordnungen ON FEZ_FIL_ID = FIL_ID AND trunc(FEZ_GUELTIGAB)>=trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS)<=trunc(SYSDATE)';
	$SQL .= ' WHERE FEZ_KEY IS NULL';
	$SQL .= ' ORDER BY FIL_ID';

	awis_FORM_Erstelle_SelectFeld('FEZ_FIL_ID',($AWIS_KEY2==0?'':$rsFEZ['FEZ_FIL_ID'][0]),300,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEZ']['FEZ_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_Textarea('FEZ_BEMERKUNG',(isset($rsFEZ['FEZ_BEMERKUNG'][0])?$rsFEZ['FEZ_BEMERKUNG'][0]:''),600,120,5,($Recht3803&6),'');
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEZ']['FEZ_GUELTIGAB'].':',150);
	awis_FORM_Erstelle_TextFeld('FEZ_GUELTIGAB',(isset($rsFEZ['FEZ_GUELTIGAB'][0])?$rsFEZ['FEZ_GUELTIGAB'][0]:''),10,200,($Recht3803&6),'','','','D','','',awis_PruefeDatum(date('d.m.Y')));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FEZ']['FEZ_GUELTIGBIS'].':',150);
	awis_FORM_Erstelle_TextFeld('FEZ_GUELTIGBIS',(isset($rsFEZ['FEZ_GUELTIGBIS'][0])?$rsFEZ['FEZ_GUELTIGBIS'][0]:''),10,200,($Recht3803&6),'','','','D','','',awis_PruefeDatum('31.12.2030'));
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();

	if(($Recht3805!=0) AND (isset($rsFEZ['FEZ_KEY'][0]) and $rsFEZ['FEZ_KEY'][0]!=''))
	{
		$RegisterSeite = (isset($_GET['Unterseite'])?$_GET['Unterseite']:(isset($_POST['Unterseite'])?$_POST['Unterseite']:'FilialzuordnungenRollen'));
		echo '<input type=hidden name=Unterseite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';

		awis_RegisterErstellen(3803, $con, $RegisterSeite);
	}

}
?>