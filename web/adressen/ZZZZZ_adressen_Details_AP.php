<?php
global $con;
global $Recht3000;
global $AWISSprache;

if($Recht3000==0)
{
	die('<span class=HinweisText>Sie haben keine ausreichenden Rechte!</span>');
}



// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('AAP','AAP_%');
$TextKonserven[]=array('ADT','ADT_%');
$TextKonserven[]=array('ADI','ADI_%');
$TextKonserven[]=array('Adressen','txtAnredenListe');
$TextKonserven[]=array('Wort','Loeschabfrage');

$TXT_AAPListe = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);


echo '<input type=hidden name=RegisterSeite value=AP>';
//************************************************************************************************
// Adresse l�schen
//************************************************************************************************
if(($Recht3000&8)==8 AND isset($_GET['Del']))	// L�schrecht?
{
	if(isset($_POST['cmdBestaetigung_x']))
	{
		if(isset($_POST['txt_APP_Key']))
		{
			$SQL = 'DELETE FROM AdressenAnsprechpartner';
			$SQL .= ' WHERE AAP_Key=0'.$_POST['txt_APP_Key'];

			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('adressen_Liste_AP.php',1,'Feherl im SQL: '.$SQL);
			}
		}
	}
	elseif(!isset($_POST['cmdAbbruch_x']))
	{
		$SQL = 'SELECT ADRESSENANSPRECHPARTNER.*';
		$SQL .= ' FROM ADRESSENANSPRECHPARTNER';
		$SQL .= ' WHERE AAP_KEY=0'.$_GET['Del'];

		$rsAAP = awisOpenRecordset($con, $SQL);

		echo '</form>';
		echo '<form name=frmDel method=post action=./adressen_Main.php?cmdAktion=Details&Seite=AP&Del='.$_GET['Del'].'>';
		echo '<input type=hidden name=txt_APP_Key value='.$_GET['Del'].'>';

		awis_FORM_FormularStart('background-color:#FAF0A0;');

		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($TXT_AAPListe['Wort']['Loeschabfrage'],400,'background-color:yellow;color=#FF0000;');
		awis_FORM_ZeileEnde();

		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_NACHNAME'].':',100);
		awis_FORM_Erstelle_TextLabel($rsAAP['AAP_NACHNAME'][0],150,'font-weight:bold;');
		awis_FORM_ZeileEnde();
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_VORNAME'].':',100);
		awis_FORM_Erstelle_TextLabel($rsAAP['AAP_VORNAME'][0],150,'font-weight:bold;');
		awis_FORM_ZeileEnde();

		awis_FORM_ZeileStart();
		echo '&nbsp;&nbsp;<input name=cmdBestaetigung type=image src=/bilder/button_ok.png>';
		echo '&nbsp;&nbsp;<input name=cmdAbbruch type=image src=/bilder/button_cancel.png>';
		awis_FORM_ZeileEnde();

		awis_FORM_FormularEnde();

		echo '</form>';
		die();
	}
}		// Loeschen


//************************************************************************************************
// Aktuelle Adresse laden
//************************************************************************************************
$Param = explode(';',awis_BenutzerParameter($con, 'AdressenSuche', $_SERVER['PHP_AUTH_USER']));

if($Param[2]!='')		// ADR_KEY
{
	awis_FORM_FormularStart();

	// Details zu einem Ansprechpartner
	if(isset($_GET['Edit']) OR isset($_GET['Neu']))
	{
		$SQL = 'SELECT ADRESSENANSPRECHPARTNER.*';
		$SQL .= ' FROM ADRESSENANSPRECHPARTNER';
		$SQL .= ' WHERE (AAP_ADR_Key='.$Param[2].') AND AAP_KEY=0'.$_GET['Edit'];
		$SQL .= ' ORDER BY AAP_NACHNAME, AAP_VORNAME';

		$rsAAP = awisOpenRecordset($con, $SQL);
		$rsAAPZeilen = $awisRSZeilen;
		if($rsAAPZeilen==1 or $rsAAPZeilen==0)
		{
			echo '<input type=hidden name=txtAAP_KEY value=0'.$rsAAP['AAP_KEY'][0].'>';

			awis_FORM_ZeileStart();

			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_ANREDE'],150);
			awis_FORM_Erstelle_TextFeld('AAP_ANREDE',$rsAAP['AAP_ANREDE'][0],20,200,(($Recht3000&2)==2),'');
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_TITEL'],100);
			awis_FORM_Erstelle_TextFeld('AAP_TITEL',$rsAAP['AAP_TITEL'][0],20,200,(($Recht3000&2)==2),'');

			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_NACHNAME'],150);
			awis_FORM_Erstelle_TextFeld('AAP_NACHNAME',$rsAAP['AAP_NACHNAME'][0],20,200,(($Recht3000&2)==2),'');
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_VORNAME'],100);
			awis_FORM_Erstelle_TextFeld('AAP_VORNAME',$rsAAP['AAP_VORNAME'][0],20,200,(($Recht3000&2)==2),'');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_ABTEILUNG'],150);
			awis_FORM_Erstelle_TextFeld('AAP_ABTEILUNG',$rsAAP['AAP_ABTEILUNG'][0],20,200,(($Recht3000&2)==2),'');
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_POSITION'],100);
			awis_FORM_Erstelle_TextFeld('AAP_POSITION',$rsAAP['AAP_POSITION'][0],20,200,(($Recht3000&2)==2),'');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_TELEFON'],150);
			awis_FORM_Erstelle_TextFeld('AAP_TELEFON',$rsAAP['AAP_TELEFON'][0],20,200,(($Recht3000&2)==2),'');
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['AAP']['AAP_EMAIL'],100);
			awis_FORM_Erstelle_TextFeld('AAP_EMAIL',$rsAAP['AAP_EMAIL'][0],30,300,(($Recht3000&2)==2),'');
			awis_FORM_ZeileEnde();

			awis_FORM_Trennzeile();
		}

		//*****************************************************************************
		// Bearbeiten der AP Infos
		//*****************************************************************************
		if($rsAAPZeilen==1 AND isset($_GET['EditADI']))
		{
			$SQL = 'SELECT ADRESSENINFOTYPEN.*,ADI_WERT,ADI_KEY,ADI_BEMERKUNG,ADI_ADT_KEY';
			$SQL .= ' FROM ADRESSENINFOTYPEN';
			$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_KAB_KEY = AZG_KAB_KEY';
			$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
			$SQL .= ' LEFT OUTER JOIN ADRESSENINFORMATIONEN ON ADI_ADT_KEY=ADT_KEY';
			$SQL .= ' WHERE AZM_XBN_Key=0' . awisBenutzerID();
			$SQL .= ' AND (ADI_XXX_Key='.$rsAAP['AAP_KEY'][0].')';
			$SQL .= ' AND ADI_KEY ='.$_GET['EditADI'];
			$SQL .= ' ORDER BY ADT_INFORMATION';
		//awis_Debug(1,$SQL);
			$rsADI = awisOpenRecordset($con, $SQL);
			$rsADIZeilen = $awisRSZeilen;



			$SQL = 'SELECT ADT_KEY, ADT_INFORMATION';
			$SQL .= ' FROM ADRESSENINFOTYPEN';
			$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_KAB_KEY = AZG_KAB_KEY';
			$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
			$SQL .= ' WHERE ((AZM_XBN_Key=0' . awisBenutzerID();
			$SQL .= ' AND (ADT_ANZAHL > (SELECT COUNT(*) FROM ADRESSENINFORMATIONEN WHERE ADI_ADT_KEY=ADRESSENINFOTYPEN.ADT_KEY AND ADI_XXX_KEY=0'.$rsAAP['AAP_KEY'][0].'))';
			$SQL .= ' ) OR ADT_KEY=0'.$rsADI['ADI_ADT_KEY'][0].')';
			$SQL .= ' AND BITAND(ADT_VERWENDUNG,2)=2';
			$SQL .= ' GROUP BY ADT_KEY, ADT_INFORMATION';

	//awis_Debug(1,$SQL);
			echo '<input type=hidden name=txtADI_KEY value='.$_GET['EditADI'].'>';
			echo '<input type=hidden name=txtADI_XXX_KEY value='.$rsAAP['AAP_KEY'][0].'>';

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['ADT']['ADT_INFORMATION'],150,'');
			awis_FORM_Erstelle_SelectFeld('ADI_ADT_KEY',$rsADI['ADI_ADT_KEY'][0],200,true,$con,$SQL,true,'','ADT_INFORMATION','','','');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['ADI']['ADI_WERT'],150,'');
			awis_FORM_Erstelle_TextFeld('ADI_WERT',$rsADI['ADI_WERT'][0],50,380,(($Recht3000&6)!=0),true,'');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($TXT_AAPListe['ADI']['ADI_BEMERKUNG'],150,'');
			awis_FORM_Erstelle_TextFeld('ADI_BEMERKUNG',$rsADI['ADI_BEMERKUNG'][0],50,380,(($Recht3000&6)!=0),true,'');
			awis_FORM_ZeileEnde();

			awis_FORM_Trennzeile();
		}
		elseif($rsAAPZeilen==1)			// Anzeige aller AP Infos
		{
			awis_FORM_ZeileStart();
			if(($Recht3000&4)==4)
			{
				$Link = '<a href=./adressen_Main.php?cmdAktion=Details&Seite=AP&Edit='.$rsAAP['AAP_KEY'][0].'&EditADI=0><img border=0 src=/bilder/icon_new.png></a>';
				awis_FORM_Erstelle_Liste_Ueberschrift($Link,34,'background-color:transparent;');
			}
			else
			{
				awis_FORM_Erstelle_Liste_Ueberschrift('',34);
			}
			awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['ADT']['ADT_INFORMATION'],250);
			awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['ADI']['ADI_WERT'],380);
			awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['ADI']['ADI_BEMERKUNG'],380);
			awis_FORM_ZeileEnde();

			$SQL = 'SELECT ADRESSENINFOTYPEN.*,ADI_WERT,ADI_KEY,ADI_BEMERKUNG';
			$SQL .= ' FROM ADRESSENINFOTYPEN';
			$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_KAB_KEY = AZG_KAB_KEY';
			$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
			$SQL .= ' LEFT OUTER JOIN ADRESSENINFORMATIONEN ON ADI_ADT_KEY=ADT_KEY';
			$SQL .= ' WHERE AZM_XBN_Key=0' . awisBenutzerID();
			$SQL .= ' AND (ADI_XXX_Key='.$rsAAP['AAP_KEY'][0].')';
			if(isset($_GET['Edit']))
			{
				$SQL .= ' AND ADI_KEY <> 0'.$_GET['EditADI'];
			}
			$SQL .= ' ORDER BY ADT_INFORMATION';
		//awis_Debug(1,$SQL);
			$rsADI = awisOpenRecordset($con, $SQL);
			$rsADIZeilen = $awisRSZeilen;
			for($ADIZeilen=0;$ADIZeilen<$rsADIZeilen;$ADIZeilen++)
			{
				awis_FORM_ZeileStart();

				$Icons = array();
				$Icons[0] = array('edit','./adressen_Main.php?cmdAktion=Details&Seite=AP&EditADI='.$rsADI['ADI_KEY'][$ADIZeilen].'&Edit='.$rsAAP['AAP_KEY'][0]);
				if(($Recht3000&8)==8)	// L�schrecht?
				{
					$Icons[] = array('delete','./adressen_Main.php?cmdAktion=Details&Seite=AP&DelADI='.$rsADI['ADI_KEY'][$ADIZeilen]);
				}

				awis_FORM_Erstelle_ListeIcons($Icons,34);


				awis_FORM_Erstelle_TextLabel($rsADI['ADT_INFORMATION'][$ADIZeilen],250);
				awis_FORM_Erstelle_TextFeld('*ADI_WERT',$rsADI['ADI_WERT'][$ADIZeilen],50,380,false);
				awis_FORM_Erstelle_TextFeld('*ADI_BEMERKUNG',$rsADI['ADI_BEMERKUNG'][$ADIZeilen],50,380,false);
				awis_FORM_ZeileEnde();
			}


		}
	}

	if(!isset($_GET['Edit']))
	{
	// Liste aller APs
	awis_FORM_ZeileStart();
	if(($Recht3000&4)==4)
	{
		$Link = '<a href=./adressen_Main.php?cmdAktion=Details&Seite=AP&Neu=1><img border=0 src=/bilder/icon_new.png></a>';
		awis_FORM_Erstelle_Liste_Ueberschrift($Link,34,'background-color:transparent;');
	}
	else
	{
		awis_FORM_Erstelle_Liste_Ueberschrift('',34);
	}
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['AAP']['AAP_NACHNAME'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['AAP']['AAP_VORNAME'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['AAP']['AAP_ABTEILUNG'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['AAP']['AAP_POSITION'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['AAP']['AAP_TELEFON'],150);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_AAPListe['AAP']['AAP_EMAIL'],250);
	awis_FORM_ZeileEnde();

	$SQL = 'SELECT ADRESSENANSPRECHPARTNER.*';
	$SQL .= ' FROM ADRESSENANSPRECHPARTNER';
	$SQL .= ' WHERE (AAP_ADR_Key='.$Param[2].')';
	if(isset($_GET['Edit']))
	{
		$SQL .= ' AND AAP_KEY<>0'.$_GET['Edit'];
	}

	$SQL .= ' ORDER BY AAP_NACHNAME, AAP_VORNAME';
	$rsAAP = awisOpenRecordset($con, $SQL);
	$rsAAPZeilen = $awisRSZeilen;
	for($AAPZeile=0;$AAPZeile<$rsAAPZeilen;$AAPZeile++)
	{
		awis_FORM_ZeileStart();

		$Icons = array();
		$Icons[0] = array('edit','./adressen_Main.php?cmdAktion=Details&Seite=AP&Edit='.$rsAAP['AAP_KEY'][$AAPZeile]);
		if(($Recht3000&8)==8)	// Löschrecht?
		{
			$Icons[] = array('delete','./adressen_Main.php?cmdAktion=Details&Seite=AP&Del='.$rsAAP['AAP_KEY'][$AAPZeile]);
		}

		awis_FORM_Erstelle_ListeIcons($Icons,34);

		awis_FORM_Erstelle_ListenFeld('AAP_NACHNAME',$rsAAP['AAP_NACHNAME'][$AAPZeile],50,150,false,($AAPZeile%2),'');
		awis_FORM_Erstelle_ListenFeld('AAP_VORNAME',$rsAAP['AAP_VORNAME'][$AAPZeile],50,150,false,($AAPZeile%2),'');
		awis_FORM_Erstelle_ListenFeld('AAP_ABTEILUNG',$rsAAP['AAP_ABTEILUNG'][$AAPZeile],50,150,false,($AAPZeile%2),'');
		awis_FORM_Erstelle_ListenFeld('AAP_POSITION',$rsAAP['AAP_POSITION'][$AAPZeile],50,150,false,($AAPZeile%2),'');
		awis_FORM_Erstelle_ListenFeld('AAP_TELEFON',$rsAAP['AAP_TELEFON'][$AAPZeile],50,150,false,($AAPZeile%2),'');
		$Link = '';
		if($rsAAP['AAP_EMAIL'][$AAPZeile]!='')
		{
			$Link = 'mailto:'.$rsAAP['AAP_EMAIL'][$AAPZeile];
		}
		awis_FORM_Erstelle_ListenFeld('AAP_EMAIL',$rsAAP['AAP_EMAIL'][$AAPZeile],50,250,false,($AAPZeile%2),'',$Link);

		awis_FORM_ZeileEnde();
	}
	}
	awis_FORM_FormularEnde();
}



?>