<?php
global $VKD;
try {

    //$VKD->Form->DebugAusgabe(1, $_GET);
    //$VKD->Form->DebugAusgabe(1, $_POST);

    $DatumHeute = date('d.m.Y');
    $DatumGueltigAb = '';
    $DatumGueltigBis = '';

    if (isset($_POST['txtVKR_GUELTIG_AB'])) {
        $DatumGueltigAb = $_POST['txtVKR_GUELTIG_AB'];
    }
    if (isset($_POST['txtVKR_GUELTIG_BIS'])) {
        $DatumGueltigBis = $_POST['txtVKR_GUELTIG_BIS'];
    }
    $Fehler = $VKD->CheckDatum($DatumGueltigAb, $DatumGueltigBis, $VKD->AWIS_KEY1, $_POST['txtVKR_SKZ'], $_POST['txtVVE_VERSNR']);
    $Fehler = $VKD->CheckRequestDaten($_POST, $_POST['txtVKR_SKZ']);

    $SQL = 'SELECT VVE_KEY FROM VERSVERSICHERUNGEN WHERE VVE_VERSNR = ' . $VKD->DB->WertSetzen('VVE', 'Z', $_POST['txtVVE_VERSNR']);
    $rsVVE = $VKD->DB->RecordSetOeffnen($SQL, $VKD->DB->Bindevariablen('VVE'));

    if ($Fehler != '') {
        $VKD->Form->ZeileStart();
        $VKD->Form->Hinweistext('Datensatz konnte nicht gespeichert werden! <br>', awisFormular::HINWEISTEXT_FEHLER);
        $VKD->Form->ZeileEnde();

        $VKD->Form->ZeileStart();
        $VKD->Form->Hinweistext($Fehler, awisFormular::HINWEISTEXT_FEHLER);
        $VKD->Form->ZeileEnde();
    } else {
        if ($VKD->AWIS_KEY1 == -1) {//Neuer DS

            $VKD->Form->DebugAusgabe(1, 'Insert');

            $SQL = 'INSERT';
            $SQL .= ' INTO VERSKONDREF';
            $SQL .= '   (';
            $SQL .= '     VKR_VVE_KEY,';
            $SQL .= '     VKR_GUELTIG_AB,';
            $SQL .= '     VKR_GUELTIG_BIS,';
            $SQL .= '     VKR_SKZ';
            $SQL .= '   )';
            $SQL .= ' VALUES ';
            $SQL .= '   (';
            $SQL .= $VKD->DB->WertSetzen('VKD', 'Z', $rsVVE->FeldInhalt('VVE_KEY')) . ',';
            $SQL .= $VKD->DB->WertSetzen('VKD', 'DU', $DatumGueltigAb) . ',';
            $SQL .= $VKD->DB->WertSetzen('VKD', 'DU', $DatumGueltigBis) . ',';
            $SQL .= $VKD->DB->WertSetzen('VKD', 'T', $_POST['txtVKR_SKZ']);
            $SQL .= '   )';

            $VKD->DB->Ausfuehren($SQL, '', true, $VKD->DB->Bindevariablen('VKD'));

            $SQL = 'SELECT seq_VKR_KEY.CurrVal AS KEY FROM DUAL';
            $rsKey = $VKD->DB->RecordSetOeffnen($SQL);
            $VKD->AWIS_KEY1 = $rsKey->FeldInhalt('KEY');

            $POS = 1;
            $Felder = $VKD->Form->NameInArray($_POST, 'txtVKD_POSITION_', 2, 1);

            foreach ($Felder as $Feld) {
                $SQL = 'INSERT INTO VERSKONDITIONEN';
                $SQL .= ' (';
                $SQL .= ' VKD_VVE_VERSNR,';
                $SQL .= ' VKD_ARTNR,';
                $SQL .= ' VKD_PREIS_NETTO,';
                $SQL .= ' VKD_PREIS_BRUTTO,';
                $SQL .= ' VKD_STEUERSATZ,';
                $SQL .= ' VKD_GUELTIG_AB,';
                $SQL .= ' VKD_GUELTIG_BIS,';
                $SQL .= ' VKD_SKZ,';
                $SQL .= ' VKD_POSITION,';
                $SQL .= ' VKD_VKR_KEY,';
                if($_POST['txtVKR_SKZ'] == 'S') {
                    $SQL .= ' VKD_VSTABZUG,';
                } else {
                    $SQL .= 'VKD_MAX_AUSBUCHUNG,';
                    $SQL .= 'VKD_MIN_AUSBUCHUNG,';
                    $SQL .= 'VKD_SB,';
                    $SQL .= 'VKD_HOECHSTGRENZE,';
                }
                $SQL .= ' VKD_GESTEUERT';
                $SQL .= ' )';
                $SQL .= ' VALUES';
                $SQL .= ' (';
                $SQL .= $VKD->DB->WertSetzen('VKD', 'Z', $_POST['txtVVE_VERSNR']);

                if($_POST['txtVKR_SKZ'] == 'S') {
                    $Artnr = $_POST['txtVKD_ARTNR_' . $POS] == '' ? '1EIN69' : $_POST['txtVKD_ARTNR_' . $POS];
                    $PreisNetto = $_POST['txtVKD_PREIS_NETTO_' . $POS] == '' ? '0' : $_POST['txtVKD_PREIS_NETTO_' . $POS];
                    $PreisBrutto = $_POST['txtVKD_PREIS_BRUTTO_' . $POS] == '' ? '0' : $_POST['txtVKD_PREIS_BRUTTO_' . $POS];
                } else {
                    $Artnr = $_POST['txtVKD_ARTNR_' . $POS];
                    $PreisNetto = $_POST['txtVKD_PREIS_NETTO_' . $POS];
                    $PreisBrutto = $_POST['txtVKD_PREIS_BRUTTO_' . $POS];
                }

                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'T', $Artnr);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'N2',$PreisNetto);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'N2',$PreisBrutto);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'N', $_POST['txtVKD_STEUERSATZ_' . $POS]);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'D', $DatumGueltigAb);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'D', $DatumGueltigBis);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'T', $_POST['txtVKR_SKZ']);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'Z', $_POST[$Feld]);
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'Z', $VKD->AWIS_KEY1);
                if($_POST['txtVKR_SKZ'] == 'S') {
                    $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'Z', $_POST['txtVKD_VSTABZUG_' . $POS]);
                } else {
                    $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_MAX_AUSBUCHUNG_' . $POS]);
                    $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_MIN_AUSBUCHUNG_' . $POS]);
                    $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_SB_' . $POS] );
                    $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_HOECHSTGRENZE_' . $POS]);
                }
                $SQL .= ', ' . $VKD->DB->WertSetzen('VKD', 'Z', 0);
                $SQL .= ' )';

                $VKD->DB->Ausfuehren($SQL, '', true, $VKD->DB->Bindevariablen('VKD'));
                $VKD->Form->DebugAusgabe(1,$VKD->DB->LetzterSQL());

                $POS++;
            }

            $RelevanteFelder[] = 'VKD_GUELTIG_AB';
            $RelevanteFelder[] = 'VKD_GUELTIG_BIS';
            $RelevanteFelder[] = 'VKD_ARTNR_1';
            $RelevanteFelder[] = 'VKD_PREIS_NETTO_1';
            $RelevanteFelder[] = 'VKD_PREIS_BRUTTO_1';
            $RelevanteFelder[] = 'VKD_ARTNR_2';
            $RelevanteFelder[] = 'VKD_PREIS_NETTO_2';
            $RelevanteFelder[] = 'VKD_PREIS_BRUTTO_2';
            $RelevanteFelder[] = 'VKD_ARTNR_3';
            $RelevanteFelder[] = 'VKD_PREIS_NETTO_3';
            $RelevanteFelder[] = 'VKD_PREIS_BRUTTO_3';

            $VKD->AenderungsHistorie->SchreibeAenderungenAusPOST($VKD->AWIS_KEY1, $_POST, $RelevanteFelder);

        } else { // Datensatz ge�ndert

            $VKD->Form->DebugAusgabe(1, 'Update');

            $SQL = 'UPDATE VERSKONDREF';
            $SQL .= ' SET VKR_GUELTIG_BIS = ' . $VKD->DB->WertSetzen('VKD', 'D', $DatumGueltigBis);
            $SQL .= ' , VKR_GUELTIG_AB = ' . $VKD->DB->WertSetzen('VKD', 'D', $DatumGueltigAb);
            $SQL .= ' WHERE VKR_KEY = ' . $VKD->DB->WertSetzen('VKD', 'Z', $VKD->AWIS_KEY1);

            $VKD->DB->Ausfuehren($SQL, '', true, $VKD->DB->Bindevariablen('VKD'));

            $Pos = 1;
            $maxPos = $_POST['txtVKR_SKZ'] == 'S'? 3: 1;
            while ($Pos <= $maxPos) {

                if($_POST['txtVKR_SKZ'] == 'S') {
                    $Artnr = $_POST['txtVKD_ARTNR_' . $Pos] == '' ? '1EIN69' : $_POST['txtVKD_ARTNR_' . $Pos];
                    $PreisNetto = $_POST['txtVKD_PREIS_NETTO_' . $Pos] == '' ? '0' : $_POST['txtVKD_PREIS_NETTO_' . $Pos];
                    $PreisBrutto = $_POST['txtVKD_PREIS_BRUTTO_' . $Pos] == '' ? '0' : $_POST['txtVKD_PREIS_BRUTTO_' . $Pos];
                } else {
                    $Artnr = $_POST['txtVKD_ARTNR_' . $Pos];
                    $PreisNetto = $_POST['txtVKD_PREIS_NETTO_' . $Pos];
                    $PreisBrutto = $_POST['txtVKD_PREIS_BRUTTO_' . $Pos];
                }

                $SQL = 'UPDATE VERSKONDITIONEN';
                $SQL .= ' SET VKD_GUELTIG_BIS = ' . $VKD->DB->WertSetzen('VKD', 'D', $DatumGueltigBis);
                $SQL .= ' , VKD_GUELTIG_AB = ' . $VKD->DB->WertSetzen('VKD', 'D', $DatumGueltigAb);
                $SQL .= ' , VKD_ARTNR = ' . $VKD->DB->WertSetzen('VKD', 'T', $Artnr);
                $SQL .= ' , VKD_STEUERSATZ = ' . $VKD->DB->WertSetzen('VKD', 'T', $_POST['txtVKD_STEUERSATZ_'.$Pos]);
                $SQL .= ' , VKD_PREIS_NETTO = ' . $VKD->DB->WertSetzen('VKD', 'T', $PreisNetto);
                $SQL .= ' , VKD_PREIS_BRUTTO = ' . $VKD->DB->WertSetzen('VKD', 'T', $PreisBrutto);
                if($_POST['txtVKR_SKZ'] == 'S') {
                    $SQL .= ' , VKD_VSTABZUG = ' . $VKD->DB->WertSetzen('VKD', 'T', $_POST['txtVKD_VSTABZUG_'.$Pos]);
                } else {
                    $SQL .= ' , VKD_MAX_AUSBUCHUNG = ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_MAX_AUSBUCHUNG_'.$Pos]);
                    $SQL .= ' , VKD_MIN_AUSBUCHUNG = ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_MIN_AUSBUCHUNG_'.$Pos]);
                    $SQL .= ' , VKD_SB = ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_SB_'.$Pos]);
                    $SQL .= ' , VKD_HOECHSTGRENZE = ' . $VKD->DB->WertSetzen('VKD', 'N2', $_POST['txtVKD_HOECHSTGRENZE_'.$Pos]);
                }
                $SQL .= ' WHERE VKD_VKR_KEY = ' . $VKD->DB->WertSetzen('VKD', 'Z', $VKD->AWIS_KEY1);
                $SQL .= ' AND VKD_POSITION = ' . $VKD->DB->WertSetzen('VKD', 'Z', $_POST['txtVKD_POSITION_'.$Pos]);

                $VKD->DB->Ausfuehren($SQL, '', true, $VKD->DB->Bindevariablen('VKD'));

                $Pos++;
            }
        }
        $VKD->AenderungsHistorie->SchreibeAenderungenAusPOST($VKD->AWIS_KEY1, $_POST);
        $VKD->Form->Hinweistext('Datensatz gespeichert',awisFormular::HINWEISTEXT_OK);
    }

} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $VKD->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $VKD->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $VKD->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>