<?php
global $VBA;
try {
        $Auftragsbeschreibung = '000000000000000000000000000000000000000000000          0000000000     00';
        $FehlerDatum = false;
        if ($VBA->AWIS_KEY1 == -1) {//Neuer DS
            $VBA->RechteMeldung(4);
            if($VBA->DatumKorrekt($_POST['txtVBA_SCHADENDATUM']) AND $VBA->DatumKorrekt($_POST['txtVBA_KFZERSTZULASSUNG'])) {
                $SQL = 'INSERT INTO VERSBEAUFTRAGUNGEN (';
                $SQL .= ' VBA_DATEINAME,';
                $SQL .= ' VBA_ERSTELLDATUM,';
                $SQL .= ' VBA_VUNUMMER,';
                $SQL .= ' VBA_VSNR,';
                $SQL .= ' VBA_SNR,';
                $SQL .= ' VBA_AUFTRAGSBESCHREIBUNG,';
                $SQL .= ' VBA_SCHADENDATUM,';
                $SQL .= ' VBA_VORSTABZUGKZ,';
                $SQL .= ' VBA_VNNAME1,';
                $SQL .= ' VBA_VNNAME3,';
                $SQL .= ' VBA_VNPLZ,';
                $SQL .= ' VBA_VNORT,';
                $SQL .= ' VBA_VNSTRASSE,';
                $SQL .= ' VBA_VNKOMM1,';
                $SQL .= ' VBA_VNKOMM2,';
                $SQL .= ' VBA_KFZKZ,';
                $SQL .= ' VBA_KFZERSTZULASSUNG,';
                $SQL .= ' VBA_KFZHSN,';
                $SQL .= ' VBA_KFZTSN,';
                $SQL .= ' VBA_KFZVIN,';
                $SQL .= ' VBA_ORDNUNGSNR,';
                $SQL .= ' VBA_USER,';
                $SQL .= ' VBA_USERDAT ) VALUES (';
                $SQL .= ' \'manuell erfasst\', ';
                $SQL .= 'sysdate,';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VU']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VSNR']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_SNR']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $Auftragsbeschreibung . str_pad($_POST['txtVBA_AUFTRAGSBESCHREIBUNG'], 3, '0', STR_PAD_LEFT)) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'DU', $_POST['txtVBA_SCHADENDATUM']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'N', $_POST['txtVBA_VORSTABZUGKZ']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNNAME1']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNNAME3']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNPLZ']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNORT']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNSTRASSE']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNKOMM1']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNKOMM2']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZKZ']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'D', $_POST['txtVBA_KFZERSTZULASSUNG']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZHSN']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZTSN']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZVIN']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_ORDNUNGSNR']) . ',';
                $SQL .= $VBA->DB->WertSetzen('VBA', 'T', $VBA->AWISBenutzer->BenutzerName()) . ',';
                $SQL .= 'sysdate';
                $SQL .= ' )';

                $VBA->DB->Ausfuehren($SQL, '', true, $VBA->DB->Bindevariablen('VBA'));

                $SQL = 'SELECT SEQ_VBA_KEY.CurrVal AS KEY FROM DUAL';
                $rsKey = $VBA->DB->RecordSetOeffnen($SQL);
                $VBA->AWIS_KEY1 = $rsKey->FeldInhalt('KEY');
            } else {
                $FehlerDatum = true;
            }
        } else {

            if($_POST['txtVDV_ERFASS_MANUELL'] == 1 ){
                if($VBA->DatumKorrekt($_POST['txtVBA_SCHADENDATUM']) AND $VBA->DatumKorrekt($_POST['txtVBA_KFZERSTZULASSUNG'])) {
                    $SQL = 'UPDATE VERSBEAUFTRAGUNGEN';
                    $SQL .= ' SET VBA_VSNR = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VSNR']) . ',';
                    $SQL .= ' VBA_SNR = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_SNR']) . ',';
                    $SQL .= ' VBA_AUFTRAGSBESCHREIBUNG = ' . $VBA->DB->WertSetzen('VBA', 'T', $Auftragsbeschreibung . str_pad($_POST['txtVBA_AUFTRAGSBESCHREIBUNG'], 3, '0', STR_PAD_LEFT)) . ',';
                    $SQL .= ' VBA_SCHADENDATUM = ' . $VBA->DB->WertSetzen('VBA', 'D', $_POST['txtVBA_SCHADENDATUM']) . ',';
                    $SQL .= ' VBA_VORSTABZUGKZ = ' . $VBA->DB->WertSetzen('VBA', 'Z', $_POST['txtVBA_VORSTABZUGKZ']) . ',';
                    $SQL .= ' VBA_VNNAME1 = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNNAME1']) . ',';
                    $SQL .= ' VBA_VNNAME3 = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNNAME3']) . ',';
                    $SQL .= ' VBA_VNPLZ = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNPLZ']) . ',';
                    $SQL .= ' VBA_VNORT = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNORT']) . ',';
                    $SQL .= ' VBA_VNSTRASSE = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNSTRASSE']) . ',';
                    $SQL .= ' VBA_VNKOMM1 = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNKOMM1']) . ',';
                    $SQL .= ' VBA_VNKOMM2 = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_VNKOMM2']) . ',';
                    $SQL .= ' VBA_KFZKZ = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZKZ']) . ',';
                    $SQL .= ' VBA_KFZERSTZULASSUNG = ' . $VBA->DB->WertSetzen('VBA', 'D', $_POST['txtVBA_KFZERSTZULASSUNG']) . ',';
                    $SQL .= ' VBA_KFZHSN = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZHSN']) . ',';
                    $SQL .= ' VBA_KFZTSN = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZTSN']) . ',';
                    $SQL .= ' VBA_KFZVIN = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_KFZVIN']) . ',';
                    $SQL .= ' VBA_ORDNUNGSNR = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_ORDNUNGSNR']);
                    $SQL .= ' WHERE VBA_KEY = ' . $VBA->DB->WertSetzen('VBA', 'Z', $VBA->AWIS_KEY1);

                    $VBA->DB->Ausfuehren($SQL, '', true, $VBA->DB->Bindevariablen('VBA'));
                } else {
                    $FehlerDatum = true;
                }
            }

            if($_POST['oldVBA_BEMERKUNG'] != $_POST['txtVBA_BEMERKUNG']) {
                $SQL = 'UPDATE VERSBEAUFTRAGUNGEN';
                $SQL .= ' SET VBA_BEMERKUNG = ' . $VBA->DB->WertSetzen('VBA', 'T', $_POST['txtVBA_BEMERKUNG']) . ',';
                $SQL .= ' VBA_BEMERKUNG_USER = ' .$VBA->DB->WertSetzen('VBA', 'T', $VBA->AWISBenutzer->BenutzerName()) . ',';
                $SQL .= ' VBA_BEMERKUNG_USERDAT = sysdate';
                $SQL .= ' WHERE VBA_KEY = ' . $VBA->DB->WertSetzen('VBA', 'Z', $VBA->AWIS_KEY1);

                $VBA->DB->Ausfuehren($SQL, '', true, $VBA->DB->Bindevariablen('VBA'));
            }


        }
        if($FehlerDatum) {
            $VBA->Form->Hinweistext('Datensatz nicht gespeichert - Datum ist ung�ltig (liegt in der Zukunft)',awisFormular::HINWEISTEXT_FEHLER);
        } else {
            $VBA->Form->Hinweistext('Datensatz gespeichert',awisFormular::HINWEISTEXT_OK);
        }

} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $VBA->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $VBA->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $VBA->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>