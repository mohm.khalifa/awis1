<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php

global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

require_once('awisDatenbank.inc');

$TextKonserven=array();
$TextKonserven[]=array('GSD','%');
$TextKonserven[]=array('GKU','%');
$TextKonserven[]=array('AAP','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_zurueck');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht28000 = $AWISBenutzer->HatDasRecht(28000);

	if(($Recht28000&4) !== 4)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}

	if (isset($_POST['txtneuAuftragnr']) and $_POST['txtneuAuftragnr']=='')
	{
		$Form->Formular_Start();

		//Neue Auftragsnummer generieren
		$SQL_neu = 'SELECT max(kopf.GSK_AUFTRAGNR) as GSK_AUFTRAGNR';
		$SQL_neu .=' FROM GUTSCHEINKOPFDATEN kopf';
		$rsNeu = $DB->RecordSetOeffnen($SQL_neu);

		$AWIS_KEY1=$Form->Format('N0',($rsNeu->FeldInhalt('GSK_AUFTRAGNR'))+1);
		
		//Gutschein Dateiname: Auftraggeber_Kostenstelle_Kunde (INTERN oder EXTERN)
		if($_POST['txtGSK_KUNDE']=='I')
		{
			$KUNDE= $_POST['txtGSK_KUNDE_INTERN'];
		}
		else
		{
			$KUNDE = $_POST['txtGSK_KUNDE_EXTERN'];
		}
		$Dateiname = $KUNDE . '_' . $_POST['txtGSK_PROFIL'];


		//Kopfdaten speichern
		//Auftraggeber entfernt wird nicht ben�tigt
		$SQL_KOPF  ='INSERT INTO GUTSCHEINKOPFDATEN';
		$SQL_KOPF .=' (GSK_AUFTRAGNR,GSK_GSV_KEY,GSK_KST,GSK_GKU_KEY,GSK_STATUS,GSK_JOBSTATUS,GSK_KUNDE_INTERN,GSK_FREITEXT,GSK_DATEINAME';
		$SQL_KOPF .=' ,GSK_USER,GSK_USERDAT';
		$SQL_KOPF .=' )VALUES (';
		$SQL_KOPF .=' ' . $DB->WertSetzen('GSKO','T',(($rsNeu->FeldInhalt('GSK_AUFTRAGNR'))+1),false).'';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','N0',($_POST['txtGSK_PROFIL']),false) .'';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','N0', ($_POST['txtGSK_KOSTENSTELLE']),true) .'';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','TN', ((isset($_POST['txtGSK_KUNDE_EXTERN']) AND is_numeric($_POST['txtGSK_KUNDE_EXTERN']))?$_POST['txtGSK_KUNDE_EXTERN']:''),true,null) .'';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','T','O',false) . '';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','T','O',false) . '';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','T-', ($_POST['txtGSK_KUNDE_INTERN']),true) .'';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','T-', ($_POST['txtGSK_FREITEXT']),true) .'';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','T-', $Dateiname,true) .'';
		$SQL_KOPF .=' ,' . $DB->WertSetzen('GSKO','T',($AWISBenutzer->BenutzerName()),false) . '';
		$SQL_KOPF .=' ,SYSDATE';
		$SQL_KOPF .=' )';
		$DB->Ausfuehren($SQL_KOPF,'',false,$DB->Bindevariablen('GSKO',true));

		//Posdaten speichern
		$AnzahlPos = $Form->Format('N0',$_POST['txtaktCount']);
		$arrayMenge = array();
		$arrayBetrag = array();
		$arrayBuchart = array();

		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$arrayMenge[$i]=($Form->Format('N0',($_POST['txtMENGE'.$i])));
		}
		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$arrayBetrag[$i]=str_replace(',','.',$_POST['txtPREIS'.$i]);
		}
		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$arrayBuchart[$i]=($Form->Format('T',($_POST['txtBUCHART'.$i])));
		}

		for($i=1;$i<=$AnzahlPos;$i++)
		{
			$SQL_POS  ='INSERT INTO GUTSCHEINPOSDATEN';
			$SQL_POS .=' (GSP_POSITION,GSP_AUFTRAGNR,GSP_BETRAG,GSP_MENGE,GSP_STATUS,GSP_BUCHART';
			$SQL_POS .=' ,GSP_USER,GSP_USERDAT';
			$SQL_POS .=' )VALUES (';
			$SQL_POS .=' ' . $DB->WertSetzen('GSPO','T',$i,false) . '';
			$SQL_POS .=' ,' . $DB->WertSetzen('GSPO','T',(($rsNeu->FeldInhalt('GSK_AUFTRAGNR'))+1),false).'';
			$SQL_POS .=' ,' . $DB->WertSetzen('GSPO','N2',$arrayBetrag[$i],false) .'';
			$SQL_POS .=' ,' . $DB->WertSetzen('GSPO','N',$arrayMenge[$i],false) .'';
			$SQL_POS .=' ,' . $DB->WertSetzen('GSPO','T','O',false) .'';
			$SQL_POS .=' ,' . $DB->WertSetzen('GSPO','T',$arrayBuchart[$i]) .'';
			$SQL_POS .=' ,' . $DB->WertSetzen('GSPO', 'T',($AWISBenutzer->BenutzerName()),false) . '';
			$SQL_POS .=' ,SYSDATE';
			$SQL_POS .=' )';
			$DB->Ausfuehren($SQL_POS,'',false,$DB->Bindevariablen('GSPO',true));
		}
		$Form->Formular_Ende();
		$Form->SchreibeHTMLCode('</form>');
	}
	elseif((isset($_POST['cmdDeaktiv_x'])) or (isset($_POST['cmdAktiv_x'])))
	{
		//Unterscheidung ob alle GS ausgew�hlt wurden oder nur einzelne
		$Status=$Form->NameInArray($_POST,'cmd');
		if($Status == 'cmdDeaktiv_x')
		{
			$Status='S';
		}
		elseif ($Status == 'cmdAktiv_x')
		{
			$Status='A';
		}
		else
		{
			//Falscher Status beim Speichern
			die();
		}

		if(isset($_POST['txtAUSWAHL_ALLE']))
		{

			//Selektion f�r Update Gutscheine
			$SQL = 'Select gs.GPD_KEY';
			$SQL .=' FROM GUTSCHEINE gs';
			$SQL .=' INNER JOIN GUTSCHEINPOSDATEN pos on gs.GPD_AUFTRAGNR=pos.GSP_AUFTRAGNR';
			$SQL .=' AND gs.GPD_POSITION=pos.GSP_POSITION';
			$SQL .=' AND pos.GSP_KEY= ' . $DB->WertSetzen('GS','N0',($_GET['GSP_KEY']),false) . '';
			$SQL .=' AND pos.GSP_AUFTRAGNR = ' . $DB->WertSetzen('GS', 'N0',($_GET['GSK_AUFTRAGNR']),false) . '';
			$rsGPDKey = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GS',true));

			while(!$rsGPDKey->EOF())
			{
				//UPDATE der GS
				$SQL_UPDATEALLE ='UPDATE GUTSCHEINE gs SET';
				$SQL_UPDATEALLE .=' gs.GPD_BUCHART = \'' . $Status . '\'';
				$SQL_UPDATEALLE .=' WHERE gs.GPD_AUFTRAGNR = ' . $DB->WertSetzen('GS','N0',($_GET['GSK_AUFTRAGNR']),false) . '';
				$SQL_UPDATEALLE .=' AND gs.GPD_KEY = ' . $DB->WertSetzen('GS', 'N0', ($rsGPDKey->FeldInhalt('GPD_KEY')),false) . '';
				$SQL_UPDATEALLE .=' AND gs.GPD_BUCHART <> \'S\'';
				$DB->Ausfuehren($SQL_UPDATEALLE,'',true,$DB->Bindevariablen('GS',true));

				$rsGPDKey->DSWeiter();
			}

			//UPDATE der Position
			$SQL_UPDATEALLE ='UPDATE GUTSCHEINPOSDATEN pos SET';
			$SQL_UPDATEALLE .=' pos.GSP_BUCHART = \'' . $Status . '\'';
			$SQL_UPDATEALLE .=' WHERE pos.GSP_AUFTRAGNR = ' . $DB->WertSetzen('GSP','N0', ($_GET['GSK_AUFTRAGNR']),false) . '';
			$SQL_UPDATEALLE .=' AND pos.GSP_KEY = ' . $DB->WertSetzen('GSP','N0', ($_GET['GSP_KEY']),false) . '';
			$DB->Ausfuehren($SQL_UPDATEALLE,'',true,$DB->Bindevariablen('GSP', true));

			$SQL_UPDATEKOPF ='UPDATE GUTSCHEINKOPFDATEN kopf SET';
			$SQL_UPDATEKOPF .=' kopf.GSK_STATUS = \'B\'';
			$SQL_UPDATEKOPF .=' WHERE kopf.GSK_AUFTRAGNR = ' . $DB->WertSetzen('GSK','N0', $_GET['GSK_AUFTRAGNR'],false) . '';
			$DB->Ausfuehren($SQL_UPDATEKOPF,'',true,$DB->Bindevariablen('GSK',true));
		}
		else
		{
			//Einzelne GS �ndern bzw. updaten
			$Felder = $Form->NameInArray($_POST, 'txtGPD_KEY_', awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
			$Statusneu='';

			if(!empty($Felder))
			{
				foreach ($Felder as $Feld)
				{
					$FeldTeile = explode('_', $Feld);

					$SQL_UPDATEGS ='UPDATE GUTSCHEINE gs SET';
					$SQL_UPDATEGS .=' gs.GPD_BUCHART = \'' . $Status . '\'';
					$SQL_UPDATEGS .=' WHERE gs.GPD_KEY = ' . $DB->WertSetzen('GS','N0',$FeldTeile[2],false) . '';
					$SQL_UPDATEGS .=' AND gs.GPD_BUCHART <> \'S\'';
					$DB->Ausfuehren($SQL_UPDATEGS,'',true,$DB->Bindevariablen('GS',true));
				}
			}

            //Buchungsstatus raussuchen
            $SQL  ='Select gs.GPD_BUCHART, gs.GPD_AUFTRAGNR, gs.GPD_POSITION';
            $SQL .=' From GUTSCHEINE gs';
            $SQL .=' INNER JOIN GUTSCHEINPOSDATEN pos on gs.GPD_AUFTRAGNR=pos.GSP_AUFTRAGNR';
            $SQL .=' and gs.GPD_POSITION=pos.GSP_POSITION';
            $SQL .=' and gs.GPD_POSITION=(SELECT gu.GPD_POSITION FROM GUTSCHEINE gu WHERE gu.GPD_KEY = ' . $DB->WertSetzen('GS','N0',$FeldTeile[2],false) . ')';
            $SQL .=' and gs.GPD_AUFTRAGNR=(SELECT gu.GPD_AUFTRAGNR FROM GUTSCHEINE gu WHERE gu.GPD_KEY = ' . $DB->WertSetzen('GS','N0',$FeldTeile[2],false) . ')';
            $SQL .= ' group by gs.GPD_BUCHART,gs.GPD_AUFTRAGNR, gs.GPD_POSITION';
            $rsBuchStatus= $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('GS',true));

            while(!$rsBuchStatus->EOF())
            {
                $Statusneu = $rsBuchStatus->FeldInhalt('GPD_BUCHART') . ' ' .$Statusneu  ;
                $rsBuchStatus->DSWeiter();
            }
            $rsBuchStatus->DSErster();

            //UPDATE der Position
            $SQL_UPDATEALLE ='UPDATE GUTSCHEINPOSDATEN pos SET';
            $SQL_UPDATEALLE .=' pos.GSP_BUCHART = \'' . $Statusneu . '\'';
            $SQL_UPDATEALLE .=' WHERE pos.GSP_AUFTRAGNR = ' . $DB->WertSetzen('GSP','N0',($rsBuchStatus->FeldInhalt('GPD_AUFTRAGNR')),false) . '';
            $SQL_UPDATEALLE .=' AND pos.GSP_POSITION = ' . $DB->WertSetzen('GSP', 'N0',($rsBuchStatus->FeldInhalt('GPD_POSITION')),false) . '';
            $DB->Ausfuehren($SQL_UPDATEALLE,'',true,$DB->Bindevariablen('GSP',true));

            $SQL_UPDATEKOPF ='UPDATE GUTSCHEINKOPFDATEN kopf SET';
            $SQL_UPDATEKOPF .=' kopf.GSK_STATUS = \'B\'';
            $SQL_UPDATEKOPF .=' WHERE kopf.GSK_AUFTRAGNR = (SELECT gs.GPD_AUFTRAGNR';
            $SQL_UPDATEKOPF .=' FROM GUTSCHEINE gs';
            $SQL_UPDATEKOPF .=' WHERE gs.GPD_KEY = ' . $DB->WertSetzen('GSK', 'N0', $FeldTeile[2], false) . '';
            $SQL_UPDATEKOPF .= ' AND gs.GPD_STATUS <> \'S\')';
            $DB->Ausfuehren($SQL_UPDATEKOPF,'',true,$DB->Bindevariablen('GSK',true));
		}
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>