<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awisATUArtikel
{

	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	private $_DB = null;

	/**
	 * AWIS Benutzer
	 *
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;

	/**
	 * Aktueller Artikel
	 *
	 * @var awisRecordset
	 */
	private $_rsAST = null;

	/**
	 * Initialisiert das Objekt
	 *
	 */
	public function __construct()
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init();
	}


	/**
	 * L�dt die Informationen zu einem Artikel
	 *
	 * @param unknown_type $SuchNr (Suchnummer)
	 * @param boolean [optional] $IstKey (Gibt an, ob die Suchnummer eine ATUNR oder ein ASTKEY ist)
	 */
	public function LadeArtikel($SuchNr, $IstKey=false)
	{
		$SQL = 'SELECT *';
		$SQL .= ' FROM Artikelstamm';
		if($IstKey)
		{
			$this->_DB->SetzeBindevariable('AST','var_NO_ast_key',$SuchNr,awisDatenbank::VAR_TYP_GANZEZAHL);
			$SQL .= ' WHERE AST_KEY = :var_NO_ast_key';
		}
		else
		{
			$this->_DB->SetzeBindevariable('AST','var_T_ast_atunr',$SuchNr,awisDatenbank::VAR_TYP_TEXT);
			$SQL .= ' WHERE AST_ATUNR = :var_T_ast_atunr';
		}

		$this->_rsAST = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('AST', false));
	}

	/**
	 * Zeigt die Fremdk�ufe f�r einen Artikel in 12 und 24 Monaten
	 *
	 * @return array
	 */
	public function Fremdkaeufe()
	{
		$this->_DB->SetzeBindevariable('FRK','var_NO_ast_key','0'.$this->_rsAST->FeldInhalt('AST_KEY'),awisDatenbank::VAR_TYP_GANZEZAHL);
		
		$SQL = "select DISTINCT Daten.*, OEN_Key ";
		$SQL .= " FROM (SELECT Fremdkaeufe.* FROM Fremdkaeufe WHERE FRK_AST_ATUNR = (SELECT AST_ATUNR FROM ARTIKELSTAMM WHERE ast_key = :var_NO_ast_key)";
		$SQL .= " UNION SELECT Fremdkaeufe.* FROM Fremdkaeufe WHERE FRK_OEN_SUCHNUMMER IN ";
		$SQL .= " (SELECT TEI_SUCH2 FROM AWIS.TeileInfos WHERE TEI_ITY_ID2='OEN' AND TEI_KEY1 = :var_NO_ast_key)) DATEN";
		$SQL .= " 	  	LEFT OUTER JOIN OENUmmern ON OENUMMERN.OEN_NUMMER = Daten.FRK_OEN_Nummer";

		$UserFilialen=$this->_AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
		if($UserFilialen!='')
		{
			if(count(explode(',',$UserFilialen))==1)
			{
				$this->_DB->SetzeBindevariable('FRK','var_NO_fil_id',$UserFilialen,awisDatenbank::VAR_TYP_GANZEZAHL);
				$SQL .= " WHERE FRK_QUELLE='F' AND FRK_FIL_ID = :var_NO_fil_id";
			} else {
				$SQL .= " WHERE FRK_QUELLE='F' AND FRK_FIL_ID IN (".$UserFilialen.")";
			}
		}

		//*******************************************************
		//* Berechnung der Summen
		//*******************************************************
		$rsFRK = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('FRK', false));
		$AnzFKGesamt=0;
		$AnzFK=0;
		$Doppelte = array();
		while(!$rsFRK->EOF())
		{
			$SuchKey = $rsFRK->FeldInhalt('FRK_FIL_ID') . '-' . $rsFRK->FeldInhalt('FRK_LFDNR');
			if(array_search($SuchKey, $Doppelte)===FALSE)
			{
					// Gr��er als 1 Jahr
				$Dat1 = mktime(0,0,0,substr($rsFRK->FeldInhalt('FRK_DATUM'),3,2),substr($rsFRK->FeldInhalt('FRK_DATUM'),0,2),substr($rsFRK->FeldInhalt('FRK_DATUM'),6)+1);
				$Dat2 = mktime(0,0,0,substr($rsFRK->FeldInhalt('FRK_DATUM'),3,2),substr($rsFRK->FeldInhalt('FRK_DATUM'),0,2),substr($rsFRK->FeldInhalt('FRK_DATUM'),6)+2);
				$Dat3 = mktime(0,0,0,substr($rsFRK->FeldInhalt('FRK_DATUM'),3,2),substr($rsFRK->FeldInhalt('FRK_DATUM'),0,2),substr($rsFRK->FeldInhalt('FRK_DATUM'),6)+3);
				if($Dat1 >= time())
				{
					$AnzFK+=$rsFRK->FeldInhalt('FRK_MENGE');
				}
					// Gr��er als 2 Jahre
				if($Dat2 >= time())
				{
	//				$AnzFK+=$rsFRK->FeldInhalt('FRK_MENGE');
					$AnzFKGesamt+=$rsFRK->FeldInhalt('FRK_MENGE');
				}
	//			$AnzFKGesamt+=$rsFRK->FeldInhalt('FRK_MENGE');
				array_push($Doppelte, $SuchKey);
			}

			$rsFRK->DSWeiter();
		}
		$Erg = array();
		$Erg[12]=$AnzFK;
		$Erg[24]=$AnzFKGesamt;

		return $Erg;
	}

}