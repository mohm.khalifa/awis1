<?php
global $AWIS_KEY1;
global $BSW;
try {

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_LETZTER_KASSENDS'].': ',300,'font-weight:bold; font-size:15px');

    $SQL = 'select nvl(max(bsw_datum),\'01.01.1970\') from bsw where BSW_IMQ_ID = 262144';
    $rsKasse = $BSW->DB->RecordSetOeffnen($SQL);
    $BSW->Form->Erstelle_TextLabel( $rsKasse->FeldInhalt(1,'D'),200,'font-weight:bold; font-size:15px');
    $BSW->Form->ZeileEnde();

    $BSW->Form->ZeileStart();
    $BSW->Form->Trennzeile('L');
    $BSW->Form->ZeileEnde();


    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_ONLINESHOP'],100,'font-weight:bold; font-size:15px');
    $BSW->Form->ZeileEnde();

    $SQL = 'Select * from (';
    $SQL .= 'SELECT XDI_KEY ,';
    $SQL .= '   XDI_DATEINAME ,';
    $SQL .= '   XDI_DATUM ,';
    $SQL .= '   XDI_MD5 ,';
    $SQL .= '   XDI_BEMERKUNG ,';
    $SQL .= '   XDI_USER ,';
    $SQL .= '   XDI_USERDAT';
    $SQL .= ' FROM IMPORTPROTOKOLL';
    $SQL .= ' WHERE xdi_bereich = \'BSE\'';
    $SQL .= ' ORDER BY XDI_DATUM DESC';
    $SQL .= ') where rownum <= 30';

    $rsBSW = $BSW->DB->RecordSetOeffnen($SQL, $BSW->DB->Bindevariablen('BSW'));

    $FeldBreiten = array();
    $FeldBreiten['XDI_KEY'] = 100;
    $FeldBreiten['XDI_DATEINAME'] = 500;
    $FeldBreiten['XDI_DATUM'] = 150;
    $Link = '';

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_Liste_Ueberschrift('XDI_KEY', $FeldBreiten['XDI_KEY'], '', $Link);
    $BSW->Form->Erstelle_Liste_Ueberschrift('XDI_DATEINAME', $FeldBreiten['XDI_DATEINAME'], '', $Link);
    $BSW->Form->Erstelle_Liste_Ueberschrift('XDI_DATUM', $FeldBreiten['XDI_DATUM'], '', $Link);

    $BSW->Form->ZeileEnde();

    $DS = 0;
    while (!$rsBSW->EOF()) {
        $HG = ($DS % 2);
        $BSW->Form->ZeileStart();
        $BSW->Form->Erstelle_ListenFeld('XDI_KEY', $rsBSW->FeldInhalt('XDI_KEY'), 0, $FeldBreiten['XDI_KEY'], false, $HG, '', '', 'T', 'L');
        $BSW->Form->Erstelle_ListenFeld('XDI_DATEINAME', $rsBSW->FeldInhalt('XDI_DATEINAME'), 0, $FeldBreiten['XDI_DATEINAME'], false, $HG, '', '', 'T', 'L');
        $BSW->Form->Erstelle_ListenFeld('XDI_DATUM', $rsBSW->FeldInhalt('XDI_DATUM'), 0, $FeldBreiten['XDI_DATUM'], false, $HG, '', '', 'T', 'L');
        $BSW->Form->ZeileEnde();

        $rsBSW->DSWeiter();
        $DS++;
    }



    $BSW->Form->Formular_Ende();
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $BSW->Form->SchaltflaechenStart();
    $BSW->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $BSW->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    $BSW->Form->SchaltflaechenEnde();

    $BSW->Form->SchreibeHTMLCode('</form>');
} catch (awisException $ex) {
    if ($BSW->Form instanceof awisFormular) {
        $BSW->Form->DebugAusgabe(1, $ex->getSQL());
        $BSW->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $BSW->Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($BSW->Form instanceof awisFormular) {
        $BSW->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>