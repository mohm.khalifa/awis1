<?php
global $HER;

try {

    $Meldung = '';

    /***************************************
     * TecoDoc Hersteller Speichern        *
     ***************************************/
    if(isset($_POST['txtHER_TECDOCNR'])){

        $SQL  ='merge into HERSTELLERINFOS DEST using';
        $SQL .=' (select ';
        $SQL .= $HER->DB->WertSetzen('HEI','Z',$HER->AWIS_KEY1) . ' as HER_ID,';
        $SQL .= $HER->DB->WertSetzen('HEI','T',$_POST['txtHER_TECDOCNR']). '   as TECDOCNR,';
        $SQL .= $HER->DB->WertSetzen('HEI','T',$_POST['txtHER_TECDOCBEZ']) . '   as TECDOCBEZ, ';
        $SQL .= $HER->DB->WertSetzen('HEI','Z',$_GET['HEI_KEY']) . '   as HEI_KEY ';
        $SQL .=' from DUAL';
        $SQL .=' ) SRC on (SRC.HER_ID = DEST.HEI_HER_ID ';
        $SQL .= ' and src.HEI_KEY = dest.HEI_KEY ';
        $SQL .= ' and DEST.HEI_ITY_KEY1 = 473)';
        $SQL .=' when matched then';
        $SQL .='   update';
        $SQL .='   set HEI_WERT1 = SRC.TECDOCNR ,';
        $SQL .='     HEI_WERT2   = SRC.TECDOCBEZ ,';
        $SQL .='     HEI_USER    = '.$HER->DB->WertSetzen('HEI','T',$HER->AWISBenutzer->BenutzerName()).' ,';
        $SQL .='     HEI_USERDAT = sysdate when not matched then';
        $SQL .='   insert';
        $SQL .='     (';
        $SQL .='       HEI_HER_ID,';
        $SQL .='       HEI_WERT1,';
        $SQL .='       HEI_WERT2,';
        $SQL .='       HEI_USER,';
        $SQL .='       HEI_USERDAT,';
        $SQL .='       HEI_ITY_KEY1,';
        $SQL .='       HEI_ITY_KEY2';
        $SQL .='     )';
        $SQL .='     values';
        $SQL .='     (';
        $SQL .='       SRC.HER_ID,';
        $SQL .='       SRC.TECDOCNR,';
        $SQL .='       SRC.TECDOCBEZ,';
        $SQL .=$HER->DB->WertSetzen('HEI','T',$HER->AWISBenutzer->BenutzerName()).',';
        $SQL .='       sysdate,';
        $SQL .='       473,';
        $SQL .='       473';
        $SQL .='     ) ';

        $HER->DB->Ausfuehren($SQL,'',true,$HER->DB->Bindevariablen('HEI'));
        $Meldung = $HER->AWISSprachKonserven['HER']['HER_SPEICHERN_OK'];
    }
    if ($Meldung != '') {
        $HER->Form->Hinweistext($Meldung,awisFormular::HINWEISTEXT_OK);
    }
} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $HER->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $HER->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $HER->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>