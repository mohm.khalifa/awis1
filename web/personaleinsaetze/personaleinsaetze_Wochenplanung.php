<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PEI','%');
	$TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','ZeitVon');
	$TextKonserven[]=array('Wort','ZeitBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4500 = $AWISBenutzer->HatDasRecht(4500);
	$Recht4506 = $AWISBenutzer->HatDasRecht(4506);
	if($Recht4506==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$MAXEditTage=2;
	$MAXEditTageErweitert=7;		// Mit dem Recht 10

	//********************************************************
	// Parameter verarbeiten
	//********************************************************
	if(isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./personaleinsaetze_loeschen.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./personaleinsaetze_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_GET['PEI_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PEI_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_POST['txtPEZ']) and $AWIS_KEY1 != 0)
	{
		//$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PEI_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
	}
	elseif(isset($_GET['PEI_XBN_KEY']) AND isset($_GET['PEI_DATUM']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));
		$Param['PBM_XBN_KEY']=$DB->FeldInhaltFormat('N0',$_GET['PEI_XBN_KEY']);
		$Param['PEI_DATUM_VOM']=$Form->Format('D',$_GET['PEI_DATUM']);
		$Param['PEI_DATUM_BIS']=$Form->Format('D',$_GET['PEI_DATUM']);
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
		$Param = array();

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';

		//$Param['PEB_KEY']=$Form->Format('N0',$_POST['txtPEB_KEY'],true);
		$Param['PBM_XBN_KEY']=$Form->Format('N0',$_POST['txtPBM_XBN_KEY'],true);
		$Param['PEI_DATUM_VOM']=$Form->Format('D',$_POST['sucPEI_DATUM_VOM'],true);
		$Param['PEI_DATUM_BIS']=$Form->Format('D',$_POST['sucPEI_DATUM_BIS'],true);
		$Param['PEI_FIL_ID']=$Form->Format('N0',$_POST['sucPEI_FIL_ID'],true);
		$Param['PEI_PEA_KEY']=$Form->Format('N0',$_POST['sucPEI_PEA_KEY'],true);
		$Param['FER_KEY']=$Form->Format('N0',$_POST['sucFER_KEY'],true);

		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$AWISBenutzer->ParameterSchreiben('Formular_PEI',serialize($Param));

	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_PEI',serialize($Param));
		}

		if(isset($_GET['PEIListe']))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY PEI_DATUM ASC, PEI_XBN_KEY, PEI_VONXTZ_ID ASC';
	}
	elseif(strpos($_GET['Sort'],'PEI_DATUM')!==false)
	{
		if(strpos($_GET['Sort'],'~'))
		{
			$ORDERBY = ' ORDER BY PEI_DATUM DESC, PEI_XBN_KEY, PEI_VONXTZ_ID DESC';
		}
		else
		{
			$ORDERBY = ' ORDER BY PEI_DATUM ASC, PEI_XBN_KEY, PEI_VONXTZ_ID ASC';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	$SQL = 'SELECT DISTINCT Personaleinsaetze.*, xbn_name || coalesce(\', \'||xbn_vorname,\'\') AS Mitarbeiter, XBN_KON_KEY';
	$SQL .= ', XTZVON.XTZ_TAGESZEIT AS ZEITVON,  XTZBIS.XTZ_TAGESZEIT AS ZEITBIS, PEA_BEZEICHNUNG, FIL_BEZ, PBE_ABKUERZUNG, PBE_BEZEICHNUNG, PBE_ANZEIGE';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Personaleinsaetze';
	$SQL .= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';
	$SQL .= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	$SQL .= ' LEFT OUTER JOIN Tageszeiten XTZVON ON XTZVON.XTZ_ID = PEI_VONXTZ_ID';
	$SQL .= ' LEFT OUTER JOIN Tageszeiten XTZBIS ON XTZBIS.XTZ_ID = PEI_BISXTZ_ID';
	$SQL .= ' LEFT OUTER JOIN Filialen ON PEI_FIL_ID = FIL_ID';
	$SQL .= ' LEFT OUTER JOIN Personaleinsberichte ON PEI_PBE_KEY = PBE_KEY';
	$SQL .= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY';

	$Bedingung=_BedingungErstellen($Param);

	$SQL.= ' WHERE PEI_PLANUNGSTYP=4';
	
	if($Bedingung!='')
	{
		$SQL .= ' AND ' . substr($Bedingung,4);
	}

	$SQL .= $ORDERBY;

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_PEI',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}
		
	// Sortierung nach dem Ermitteln der Datenanzahl
	$Form->DebugAusgabe(1,$SQL);
	$rsPEI = $DB->RecordSetOeffnen($SQL);


	//**************************************
	// Rechte des Benutzers ermitteln
	//**************************************
	$SQL = 'SELECT DISTINCT PBR_PBZ_KEY, PBM_PEB_KEY ';
	$SQL .= ' FROM PerseinsbereicheRechteVergabe';
	$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBR_PBM_KEY = PBM_KEY';
	$SQL .= ' INNER JOIN benutzer ON PBM_XBN_KEY = XBN_KEY AND XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
	//Perseinsaetze Rechte pr�fen
	$SQL .= ' WHERE PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	//$SQL .= ' AND PBM_PEB_KEY = 0'.$Param['PEB_KEY'];
	if($Param['PBM_XBN_KEY']!=0)
	{
		$SQL .= ' AND PBM_PEB_KEY IN (SELECT PBM_PEB_KEY FROM PersEinsBereicheMitglieder ';
		$SQL .= ' WHERE PBM_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY']).')';
	}
	else
	{
		$SQL .= " AND EXISTS (SELECT *";
		$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
		$SQL .= "       FROM Perseinsbereichemitglieder";
		$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
		$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
		$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
		$SQL .= " GROUP BY PEB_KEY) Bereiche";
		//$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key ";
		//Perseinsaetze Rechte pr�fen
		$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate"; //TR 04.02.11
		$SQL .= " AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
		$SQL .= ' WHERE pbm_xbn_key = XBN_KEY';
		$SQL .= ' )';
	}

	$SQL .= ' AND (XBN_STATUS = \'A\' OR XBN_KEY = 0'.$AWISBenutzer->BenutzerID().')';
	$SQL .= ' ORDER BY PBR_PBZ_KEY ';
	$rsPBR = $DB->RecordSetOeffnen($SQL);
	$RechtPEI = 0;
	while(!$rsPBR->EOF())
	{
		$RechtPEI = $RechtPEI  | pow(2,($rsPBR->FeldInhalt('PBR_PBZ_KEY')-1));
		$rsPBR->DSWeiter();
	}

	//**********************************************************
	//* Neue Daten hinzuf�gen
	//**********************************************************
	if(($RechtPEI&6)!=0)
	{
		$Form->Formular_Start();
		$Form->SchreibeHTMLCode('<form name=frmPersonaleinsaetze action=./personaleinsaetze_Main.php?cmdAktion=Wochenplanung method=POST  enctype="multipart/form-data">');
		
		$Form->Erstelle_HiddenFeld('PEI_PLANUNGSTYP',4);

		//$AWIS_KEY1 = $rsPEI->FeldInhalt('PEI_KEY');

		// Laden den zu bearbeitenden Datensatz
		$rsPEIEDIT = $DB->RecordSetOeffnen('SELECT * FROM Personaleinsaetze WHERE PEI_KEY = 0'.$AWIS_KEY1);

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_PEI',serialize($Param));

		$Form->Erstelle_HiddenFeld('PEI_KEY',$AWIS_KEY1);
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./personaleinsaetze_Main.php?cmdAktion=Wochenplanung&PEIListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPEIEDIT->FeldInhalt('PEI_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsPEIEDIT->FeldInhalt('PEI_USERDAT')));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht4500&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		// �berschrift
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_XBN_KEY'],170);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_DATUM'],120);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_PEA_KEY'],180);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_FIL_ID'],80);

		$Form->ZeileEnde();

		//***********************************
		// Eingabezeile
		//***********************************
		if((int)$RechtPEI & 4)		// Hinzuf�gen
		{
			$Form->ZeileStart();

			$SQL = "SELECT DISTINCT xbn_key, xbn_name || coalesce(' '||xbn_vorname,'') AS Anwender ";
			$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$SQL .= "       FROM Perseinsbereichemitglieder";
			$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			$SQL .= " GROUP BY PEB_KEY) Bereiche";
			//$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			//Perseinsaetze Rechte pr�fen
			$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key"; //TR 04.02.11
			$SQL .= " INNER JOIN Benutzer ON pbm_xbn_key = xbn_key";
			$SQL .= ' ORDER BY 2';
			$Form->Erstelle_SelectFeld('PEI_XBN_KEY',($rsPEI->FeldInhalt('PEI_XBN_KEY')==''?($Param['PBM_XBN_KEY']==''?'':$Param['PBM_XBN_KEY']):$rsPEI->FeldInhalt('PEI_XBN_KEY')),170,true,$SQL,'','','','',$AWISBenutzer->BenutzerID(),'');
			$AWISCursorPosition='txtPEI_XBN_KEY';
			
			$Montag = GetMontag();
			$Form->Erstelle_TextFeld('PEI_DATUM',($rsPEIEDIT->FeldInhalt('PEI_DATUM')==''?(isset($_POST['txtPEI_DATUM'])?$_POST['txtPEI_DATUM']:$Montag):$rsPEIEDIT->FeldInhalt('PEI_DATUM')),10,123,$EditRecht,'','','','D','','',($Param['PEI_DATUM_VOM']==''?$Form->PruefeDatum(date('d.m.Y')):$Form->PruefeDatum($Param['PEI_DATUM_VOM'])));

			// Einsatzarten
			$SQL = 'SELECT PEA_KEY, PEA_BEZEICHNUNG FROM PerseinsArten ORDER BY PEA_BEZEICHNUNG';
			$Form->Erstelle_SelectFeld('PEI_PEA_KEY',$rsPEIEDIT->FeldInhalt('PEI_PEA_KEY'),180,$EditRecht,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'8');

			// Filiale
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
			{
				$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
				$SQL .= ' FROM Filialen ';
				$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
				$SQL .= ' ORDER BY FIL_BEZ';
				$Form->Erstelle_SelectFeld('PEI_FIL_ID',$rsPEIEDIT->FeldInhalt('PEI_FIL_ID'),80,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			}
			else
			{
				$Form->Erstelle_TextFeld('PEI_FIL_ID',$rsPEIEDIT->FeldInhalt('PEI_FIL_ID'),5,80,true,'','','','T','L','','',10);
			}
			$Form->ZeileEnde();

			// Bemerkung
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_BEMERKUNG'].':',170);
			$Form->Erstelle_TextFeld('PEI_BEMERKUNG',$rsPEIEDIT->FeldInhalt('PEI_BEMERKUNG'),80,400,$EditRecht,'','','','T','','','',1000);
			$Form->ZeileEnde();
			
			//Bericht
			if(($Recht4500&32)!=0)
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_BERICHTTYP'].':',170);
				$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
				$SQL = 'SELECT PBE_KEY, PBE_BEZEICHNUNG as BEZ';
				$SQL .= ' FROM Personaleinsberichte ';
				$SQL .= ' ORDER BY PBE_BEZEICHNUNG';
				if ($EditRecht==true)
				{
					if ($rsPEIEDIT->FeldInhalt('PEI_PEZ_STATUS')=='1')
					{
						$EditRecht = false;
					}
				}			
				$Form->Erstelle_SelectFeld('PEI_PBE_KEY',$rsPEIEDIT->FeldInhalt('PEI_PBE_KEY'),400,$EditRecht,$SQL,$OptionBitteWaehlen,'','','','','');						
				$Form->ZeileEnde();
			}
			
			/*Berichte k�nnen erst in der Detailplanung eingegeben werden
			if(!isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=0 AND (floatval($rsPEI->FeldInhalt('PEI_PBE_KEY'))>0))
			{
				$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Berichte'));
				$SubReg = new awisRegister(4502);
				$SubReg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:'Berichte'));
			}
			*/			
		}

		if(!isset($_GET['PEI_KEY']))
		{
			//**********************************************
			// Vorhandene Daten anzeigen
			//**********************************************
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_XBN_KEY'],158,'','');
			$Link = './personaleinsaetze_Main.php?cmdAktion=Wochenplanung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=PEI_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEI_DATUM'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_DATUM'],90,'',$Link);			
			$Link = './personaleinsaetze_Main.php?cmdAktion=Wochenplanung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=PEA_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEA_BEZEICHNUNG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_PEA_KEY'],90,'',$Link);
			$Link = './personaleinsaetze_Main.php?cmdAktion=Wochenplanung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=PEI_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEI_FIL_ID'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_FIL_ID'],100,'',$Link);
			$Link = './personaleinsaetze_Main.php?cmdAktion=Wochenplanung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=PEI_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PEI_BEMERKUNG'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PEI']['PEI_BEMERKUNG'],120,'',$Link);
			$Form->ZeileEnde();

			$PEIZeile=0;

			$Pruefung = array();	// Pr�fung der eizelnen Werte

			while(!$rsPEI->EOF())
			{
				$EditRechtErweitert=false;
				
				$Icons = array();
				if(($RechtPEI&4)!=0)	// �ndernrecht
				{
					//TODO: �ndern auf PEI-Recht!!
					if(($Recht4500&16)!=0 OR $rsPEI->FeldInhalt('PEI_XBN_KEY')==$AWISBenutzer->BenutzerID())
					{
						if((($RechtPEI&64)!=0 OR $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTage,date('Y')))
						OR (($RechtPEI&512)!=0 AND $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTageErweitert,date('Y'))))
						{
							$EditRechtErweitert=true;
							
							$Icons[] = array('edit','./personaleinsaetze_Main.php?cmdAktion=Wochenplanung&PEI_KEY='.$rsPEI->FeldInhalt('PEI_KEY'));
						}
					}
				}
				if(($RechtPEI&8)!=0)	// L�schberechtigung
				{
					if(($Recht4500&16)!=0 OR $rsPEI->FeldInhalt('PEI_XBN_KEY')==$AWISBenutzer->BenutzerID())
					{
						if((($RechtPEI&64)!=0 OR $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTage,date('Y')))
						OR (($RechtPEI&512)!=0 AND $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),false,false,true)>=mktime(0,0,0,date('m'),date('d')-$MAXEditTageErweitert,date('Y'))))
						{
							$Icons[] = array('delete','./personaleinsaetze_Main.php?cmdAktion=Wochenplanung&Del='.$rsPEI->FeldInhalt('PEI_KEY'));
						}
					}
				}

				
				$Pruefung['Test']=1;
				
				$FilBez = ($rsPEI->FeldInhalt('PEI_FIL_ID')!=''?' ('.$rsPEI->FeldInhalt('FIL_BEZ').')':'');

				$Form->ZeileStart();
				$Form->Erstelle_ListeIcons($Icons,36,($PEIZeile%2));
				$Link = ($rsPEI->FeldInhalt('XBN_KON_KEY')==''?'':'/telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey='.$rsPEI->FeldInhalt('XBN_KON_KEY'));
				$Form->Erstelle_ListenFeld('MITARBEITER',$rsPEI->FeldInhalt('MITARBEITER'),0,120,false,(($Pruefung['Test'])==0?2:($PEIZeile%2)),'',$Link,'T');
				$Link = '';
				$Form->Erstelle_ListenFeld('PEI_DATUM',$rsPEI->FeldInhalt('PEI_DATUM'),0,90,false,(($Pruefung['Test'])==0?2:($PEIZeile%2)),'','','D');
				$Form->Erstelle_ListenFeld('PEA_BEZEICHNUNG',$rsPEI->FeldInhalt('PEA_BEZEICHNUNG'),0,90,false,(($Pruefung['Test'])==0?2:($PEIZeile%2)),'','','T','L',$rsPEI->FeldInhalt('PEA_BEZEICHNUNG'));
				$Form->Erstelle_ListenFeld('PEI_FIL_ID',$rsPEI->FeldInhalt('PEI_FIL_ID').$FilBez,0,100,false,(($Pruefung['Test'])==0?2:($PEIZeile%2)),'','','T');
				$Form->Erstelle_ListenFeld('PEI_BEMERKUNG',$rsPEI->FeldInhalt('PEI_BEMERKUNG'),0,120,false,(($Pruefung['Test'])==0?2:($PEIZeile%2)),'','','T');
				$Link='./personaleinsaetze_Main.php?cmdAktion=Wochenplanung&PEI_KEY='.$rsPEI->FeldInhalt('PEI_KEY');
				
				if(($Recht4500&32)!=0)
				{
					$Form->Erstelle_ListenFeld('PBE_ANZEIGE',$rsPEI->FeldInhalt('PBE_ANZEIGE'),0,28,false,(($Pruefung['Test'])==0?2:($PEIZeile%2)),'',$Link,'T','L',$rsPEI->FeldInhalt('PBE_BEZEICHNUNG'));
					
					if(floatval($rsPEI->FeldInhalt('PEI_PBE_KEY'))>0)
					{
						$Icons = array();
						$Icons[0] = array('report','./personaleinsaetze_PDF_'.$rsPEI->FeldInhalt('PBE_ABKUERZUNG').'_Blanko.php?PEI_KEY=0');
						$Form->Erstelle_ListeIcons($Icons,18,(($Pruefung['Test'])==0?2:($PEIZeile%2)));
						
						if ($rsPEI->FeldInhalt('PEI_PEZ_STATUS')=='1')
						{
							$Icons = array();
							$Icons[1] = array('pdf','./personaleinsaetze_PDF_'.$rsPEI->FeldInhalt('PBE_ABKUERZUNG').'_Blanko.php?PEI_KEY='.$rsPEI->FeldInhalt('PEI_KEY'));				
							$Form->Erstelle_ListeIcons($Icons,18,(($Pruefung['Test'])==0?2:($PEIZeile%2)));
						}
					}
				}
				$Form->ZeileEnde();

				$rsPEI->DSWeiter();
				$PEIZeile++;
			}
			$Link = './personaleinsaetze_Main.php?cmdAktion=Wochenplanung';
			$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		}

		$Form->Formular_Ende();

		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();

		$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

		if(($Recht4500&6)!=0)		//
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		}

		//$Form->Schaltflaeche('href', 'cmdDrucken', './personaleinsaetze_Drucken_Liste.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');

		//$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=personaleinsaetze&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

		$Form->SchaltflaechenEnde();

		$Form->SchreibeHTMLCode('</form>');
	}

	//$Form->DebugAusgabe(1, $Param, $Bedingung, $rsPEI, $_POST, $rsPEI, $SQL, $AWISSprache);
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PEI_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(($AWISBenutzer->HatDasRecht(4500)&16)==0)		// Nur den eigenen Bereich anzeigen
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM PerseinsBereicheMitglieder WHERE ';
		$Bedingung .= ' PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ')';
	}

	// Einschr�nken nach Bereich
	if(isset($Param['PEB_KEY']) AND $Param['PEB_KEY']!='')
	{
		$Bedingung .= ' AND PEI_PEB_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PEB_KEY'],false);
	}
	// Einschr�nken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
		if($Param['PBM_XBN_KEY']==0)
		{
			$Bedingung .= " AND EXISTS (SELECT *";
			$Bedingung .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$Bedingung .= "       FROM Perseinsbereichemitglieder";
			$Bedingung .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$Bedingung .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$Bedingung .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			$Bedingung .= " GROUP BY PEB_KEY) Bereiche";
			//Perseinsaetze Rechte pr�fen
			$Bedingung .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key and pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key"; //TR 04.02.11
			//$Bedingung .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			$Bedingung .= ' WHERE pbm_xbn_key = PEI_XBN_KEY';
			$Bedingung .= ' )';
		}
		else
		{
			$Bedingung .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY'],false);
		}
	}
	/*
	// Einschr�nken nach Datum
	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$Bedingung .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$Bedingung .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}

	if(isset($Param['PEI_PEA_KEY']) AND $Param['PEI_PEA_KEY']!='0')
	{
		$Bedingung .= ' AND PEI_PEA_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PEI_PEA_KEY'],false);
	}
	*/
	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$Bedingung .= ' AND PEI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID'],false);
	}

	if(isset($Param['FER_KEY']) AND $Param['FER_KEY']!='0')
	{

		$Bedingung .= ' AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN';
		$Bedingung .= ' WHERE xx1_fer_key =0'.$DB->FeldInhaltFormat('N0',$Param['FER_KEY'],false);
		$Bedingung .= ' AND xx1_kon_key = KON_KEY)';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}

function GetMontag()
{
$integerTimestamp = time (); 

list ($stringWeek, $integerWeekday) = explode (' ',  date ('W/Y w',  $integerTimestamp + 604800));  //7 * 24 * 60 * 60 = 604800 ); 

if ($integerWeekday == 0) 
{ 
  $integerWeekday = 7; 
} 

$integerDaysTillMonday = 8 - $integerWeekday; 

$stringDateMonday = date ('d.m.Y', $integerTimestamp + ($integerDaysTillMonday * 86400)); // 24 * 60 * 60 = 86400 

//$stringDateFriday = date ('d.m.Y', $integerTimestamp + (($integerDaysTillMonday + 4) * 86400)); // 24 * 60 * 60 = 86400 

return $stringDateMonday;
}
?>