<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisProtokoll.inc';

/**
 * Enth�lt Metoden, die systemnahe Funktionen ausf�hren
 * @author Sacha Kerres
 *
 */
class awisSystem
{
	/**
	 * Datenbankverbindung
	 * @var awisDatenbank
	 */
	private $_DB = null;	
	/**
	 * Benutzer
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	/**
	 * Debuglevel f�r Ausgaben
	 * @var int
	 */
	private $_DebugLevel = 0;
	/**
	 * Protokoll-Objekt
	 * @var awisProtokoll
	 */
	private $_Protokoll = null;
	/**
	 * Initialisierung der Klasse
	 * @param awisDatenbank $DB
	 * @param awisBenutzer $AWISBenutzer
	 */
	public function __construct(awisDatenbank $DB, awisBenutzer $AWISBenutzer)
	{
		$this->_DB = $DB;
		$this->_AWISBenutzer = $AWISBenutzer;
		$this->_Protokoll = new awisProtokoll($this->_DB, $this->_AWISBenutzer);
		
	}
	/**
	 * Liest oder setzt den Debuglevel im Objekt
	 * @param int $NeuerLevel
	 * @return number
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = (int)$NeuerLevel;
		}
		
		return $this->_DebugLevel;
	}
	
	/**
	 * Setzt eine oder mehrere Sequenzen zur�ck
	 * @param array $Tabellenkuerzel
	 */
    public function SequenzZuruecksetzen(array $Tabellenkuerzel)
    {
        $this->_Protokoll->Init($this->_DebugLevel, __METHOD__, __CLASS__);
        $this->_Protokoll->KonsolenAusgabe('Seqzenzen '.implode(',', $Tabellenkuerzel).' zuruecksetzen', 5);
                
        try 
        {
            foreach($Tabellenkuerzel AS $Tabelle)
            {
                $SQL = 'SELECT *';
                $SQL .= ' FROM user_sequences';
                $SQL .= ' WHERE SEQUENCE_NAME LIKE '.$this->_DB->WertSetzen('SEQ', 'T', 'SEQ_'.$Tabelle.'_KEY');
                $rsSEQ = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('SEQ'));
                
                if(!$rsSEQ->EOF())
                {
                    $SQL = 'SELECT *';
                    $SQL .= ' FROM TABELLENKUERZEL';
                    $SQL .= ' WHERE XTN_KUERZEL = '.$this->_DB->WertSetzen('XTN', 'T', $Tabelle);
                    $rsXTN = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('XTN'));
                    if(!$rsXTN->EOF())
                    {
                        $SQL = 'SELECT MAX('.$Tabelle.'_KEY) AS LetzteNr';
                        $SQL .= ' FROM '.$rsXTN->FeldInhalt('XTN_TABELLENNAME');
                        $rsDaten = $this->_DB->RecordSetOeffnen($SQL);
                        
                        if($rsDaten->FeldInhalt('LETZTENR')>$rsSEQ->FeldInhalt('LAST_NUMBER'))
                        {
                            $this->_Protokoll->KonsolenAusgabe('Sequenz '.$rsSEQ->FeldInhalt('SEQUENCE_NAME').' loeschen.',50);
                            $SQL = 'DROP SEQUENCE '.$rsSEQ->FeldInhalt('SEQUENCE_NAME');
                            $this->_DB->Ausfuehren($SQL,'',true);
                            
                            $this->_Protokoll->KonsolenAusgabe('Sequenz '.$rsSEQ->FeldInhalt('SEQUENCE_NAME').' neu anlegen.',50);
                            $SQL = 'CREATE SEQUENCE AWIS.'.$rsSEQ->FeldInhalt('SEQUENCE_NAME');
                            $SQL .= ' START WITH '.($rsDaten->FeldInhalt('LETZTENR')+5);
                            $SQL .= ' INCREMENT BY 1';
                            $SQL .= ' MAXVALUE 999999999999999999999999999';
                            $SQL .= ' MINVALUE 1';
                            $SQL .= ' NOCYCLE';
                            $SQL .= ' NOCACHE';
                            $SQL .= ' NOORDER';
                            
                            $this->_Protokoll->KonsolenAusgabe('SQL:'.$SQL,900);
                            
                            $this->_DB->Ausfuehren($SQL,'',true);
                            
                            $this->_Protokoll->KonsolenAusgabe('Sequenz '.$rsSEQ->FeldInhalt('SEQUENCE_NAME').' wurde angepasst. Neuer Wert: '.($rsDaten->FeldInhalt('LETZTENR')+5),5);
                        }
                        else 
                        {
                            $this->_Protokoll->KonsolenAusgabe('Sequenz '.$rsSEQ->FeldInhalt('SEQUENCE_NAME').' passt.',5);
                        }
                    }
                    else 
                    {
                        $this->_Protokoll->KonsolenAusgabe('Tabelle nicht gefunden: '.$Tabelle,0);
                    }
                }
                else
                {
                    $this->_Protokoll->KonsolenAusgabe('Seqzenzen nicht gefunden: '.'SEQ_'.$Tabelle.'_KEY', 0);
                }
            }
        }
        catch(Exception $ex)
        {
            $this->_Protokoll->KonsolenAusgabe('FEHLER : '.$ex->getMessage(),0, awisProtokoll::AUSGABETYP_FATALERFEHLER);
            $this->_Protokoll->KonsolenAusgabe(' -Code : '.$ex->getcode(),0, awisProtokoll::AUSGABETYP_FATALERFEHLER);
            $this->_Protokoll->KonsolenAusgabe(' -Zeile: '.$ex->getLine(),0, awisProtokoll::AUSGABETYP_FATALERFEHLER);
        }
    }    
}