<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisAusdruck.php');


$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$AWISBenutzer = awisBenutzer::Init();
$Form = new awisFormular();

$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');
/*
$RechteStufe = $AWISBenutzer->HatDasRecht(4500);
if($RechteStufe==0)
{
	$Form->DebugAusgabe(1,$AWISBenutzer->BenutzerName());
	$Form->Fehler_KeineRechte();
}
*/
$TextKonserven=array();
$TextKonserven[]=array('WTP','%');
$TextKonserven[]=array('Wort','wrd_Stand');
$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,$AWISSprachKonserven['WTP']['tit_Bericht_1']);

$Zeile = $Ausdruck->NeueSeite(0,1);
$Ausdruck->_pdf->SetFont('Arial','',6);
$Ausdruck->_pdf->setXY(10,200);
$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);

$Ausdruck->_pdf->setXY(10,12);
$Ausdruck->_pdf->SetFont('Arial','',6);
$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'].': ' . date('d.m.Y H:i'),0,0,'L',0);

//
// Daten
//

$SQL = 'SELECT KON_KEY, KON_NAME1, KON_NAME2';
$SQL .= ' FROM KONTAKTE';
$SQL .= ' WHERE KON_KEY = '.$_GET['KON_KEY'];
$rsKON = $DB->RecordSetOeffnen($SQL);

// Ermitteln, welche Gebiete man sehen darf

$Recht11000 = $AWISBenutzer->HatDasRecht(11000);
$Recht11000 = 1;
$GebieteListe = ',0';
if(($Recht11000&2)!=2)
{
    $SQL = 'SELECT FEB_KEY, FEB_BEZEICHNUNG';
    $SQL .= ' FROM Filialebenen';
    $SQL .= ' WHERE trunc(FEB_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEB_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
    $SQL .= ' AND EXISTS';
    $SQL .= '  (select DISTINCT XX1_FEB_KEY';
    $SQL .= '  FROM V_FILIALEBENENROLLEN';
    $SQL .= '  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY AND KON_STATUS = \'A\'';
    $SQL .= '  INNER JOIN FilialebenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY AND trunc(FRZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FRZ_GUELTIGBIS) >= trunc(sysdate) ';
    //$SQL .= '  WHERE XX1_KON_KEY = '.'2375';      // Zum Testen: Ott (GBL)
    $SQL .= '  WHERE XX1_KON_KEY = '.$AWISBenutzer->BenutzerKontaktKEY();
    $SQL .= '  AND  XX1_FEB_KEY = FEB_KEY';
    $SQL .= ')';
    $SQL .= ' ORDER BY 2';
    $rsFEB = $DB->RecordSetOeffnen($SQL);
    $GebieteListe = '';
    while(!$rsFEB->EOF())
    {
        $GebieteListe .= ','.$rsFEB->FeldInhalt('FEB_KEY');
        $rsFEB->DSWeiter();
    }
    unset($rsFEB);
    if($GebieteListe=='')
    {
        $GebieteListe = ',0';
    }
}

$Zeile = 30;
$Ausdruck->_pdf->setXY(10,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$rsKON->FeldInhalt('KON_NAME1').' '.$rsKON->FeldInhalt('KON_NAME2'),0,0,'L',0);
$Ausdruck->_pdf->cell(10,6,$Form->Format('D',$_GET['TAG']),0,0,'L',0);
$Zeile +=6;

$Ausdruck->_pdf->setXY(40,$Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(30,6,'Filiale',0,0,'L',0);
$Ausdruck->_pdf->cell(30,6,'WA-Nr',0,0,'L',0);
$Ausdruck->_pdf->cell(30,6,'Umsatz',0,0,'L',0);
$Ausdruck->_pdf->cell(30,6,'Stunden',0,0,'L',0);
$Zeile+=5;

$SQL ='SELECT DISTINCT WTP_VK, WTP_ZEITMIN, WTE_FIL_ID, WTE_WANR';
$SQL .= ' FROM WerkstattterminePlanungen';
$SQL .= ' INNER JOIN WerkstattterminePositionen ON WTP_WPO_KEY = WPO_KEY';
$SQL .= ' INNER JOIN Werkstatttermine ON WPO_WTE_KEY = WTE_KEY';
if(($Recht11000&2)!=2)
{
    $SQL .= ' INNER JOIN FILIALEBENENZUORDNUNGEN ON WTE_FIL_ID = FEZ_FIL_ID AND FEZ_FEB_KEY IN ('.substr($GebieteListe,1).') AND trunc(FEZ_GUELTIGAB) <= trunc(SYSDATE) AND trunc(FEZ_GUELTIGBIS) ' . $SRFunktionen->GueltigBis();
}
$SQL .= ' WHERE WTP_KON_KEY = '.$rsKON->FeldInhalt('KON_KEY');
$SQL .= ' AND WTP_TAG = '.$DB->FeldInhaltFormat('D',$_GET['TAG']);
$SQL .= ' ORDER BY WTE_FIL_ID, WTE_WANR';
$rsWTP = $DB->RecordSetOeffnen($SQL);
$LetzteFIL = '';
$Summen = array();

while(!$rsWTP->EOF())
{
	$Ausdruck->_pdf->setXY(40,$Zeile);
	if($LetzteFIL!=$rsWTP->FeldInhalt('WTE_FIL_ID'))
	{
		if($LetzteFIL!='')
		{
			$Ausdruck->_pdf->setXY(70,$Zeile);
			$Ausdruck->_pdf->cell(30,6,'Summe:',0,0,'L',0);
			$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteFIL]['Umsatz']),0,0,'L',0);
			$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteFIL]['Zeit'])/60),0,0,'L',0);
		}
		$LetzteFIL=$rsWTP->FeldInhalt('WTE_FIL_ID');
		$Ausdruck->_pdf->cell(30,6,$rsWTP->FeldInhalt('WTE_FIL_ID'),0,0,'L',0);
		$Summen[$rsWTP->FeldInhalt('WTE_FIL_ID')]['Umsatz']=0;
		$Summen[$rsWTP->FeldInhalt('WTE_FIL_ID')]['Zeit']=0;
	}
	else
	{
		$Ausdruck->_pdf->cell(30,6,'',0,0,'L',0);
	}

	$Ausdruck->_pdf->cell(30,6,$rsWTP->FeldInhalt('WTE_WANR'),0,0,'L',0);
	$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$rsWTP->FeldInhalt('WTP_VK')),0,0,'L',0);
	$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($rsWTP->FeldInhalt('WTP_ZEITMIN'))/60),0,0,'L',0);

	$Summen[$rsWTP->FeldInhalt('WTE_FIL_ID')]['Umsatz']+=$rsWTP->FeldInhalt('WTP_VK');
	$Summen[$rsWTP->FeldInhalt('WTE_FIL_ID')]['Zeit']+=$rsWTP->FeldInhalt('WTP_ZEITMIN');

	$Zeile+=5;
    if($Zeile > 100)
    {
        $Ausdruck->NeueSeite();
        $Zeile = 30;
        $Ausdruck->_pdf->setXY(10,$Zeile);
        $Ausdruck->_pdf->SetFont('Arial','',12);
        $Ausdruck->_pdf->cell(50,6,$rsKON->FeldInhalt('KON_NAME1').' '.$rsKON->FeldInhalt('KON_NAME2'),0,0,'L',0);
        $Ausdruck->_pdf->cell(10,6,$Form->Format('D',$_GET['TAG']),0,0,'L',0);
        $Zeile +=6;

        $Ausdruck->_pdf->setXY(40,$Zeile);
        $Ausdruck->_pdf->SetFont('Arial','',10);
        $Ausdruck->_pdf->cell(30,6,'Filiale',0,0,'L',0);
        $Ausdruck->_pdf->cell(30,6,'WA-Nr',0,0,'L',0);
        $Ausdruck->_pdf->cell(30,6,'Umsatz',0,0,'L',0);
        $Ausdruck->_pdf->cell(30,6,'Stunden',0,0,'L',0);
        $Zeile+=5;

    }

	$rsWTP->DSWeiter();
}
// Summe der letzten Filiale
$Ausdruck->_pdf->setXY(70,$Zeile);
$Ausdruck->_pdf->cell(30,6,'Summe:',0,0,'L',0);
$Ausdruck->_pdf->cell(30,6,$Form->Format('N2T',$Summen[$LetzteFIL]['Umsatz']),0,0,'L',0);
$Ausdruck->_pdf->cell(30,6,$Form->Format('N1',($Summen[$LetzteFIL]['Zeit'])/60),0,0,'L',0);

$Ausdruck->Anzeigen();