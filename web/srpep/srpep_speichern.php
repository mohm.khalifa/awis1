<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('KVO','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_DatumEndeVorAnfang');
$TextKonserven[]=array('Fehler','err_DatumAbstandTage');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');



try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	
	
	
	//*********************************************************************************************
	//** Kontakteverfuegbarkeiten
	//*********************************************************************************************

	$Felder = array_merge($Form->NameInArray($_POST,'txtKOV_PEK_KEY_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFORMular::NAMEINARRAY_SUCHTYP_ANFANG)
					,$Form->NameInArray($_POST,'txtKOV_FEB_KEY_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFORMular::NAMEINARRAY_SUCHTYP_ANFANG));

	if(!empty($Felder) AND $Felder[0]!='')
	{
		$FeldListe = array();
		foreach($Felder as $Feld)
		{
			$FehlerListe=array();
			$FeldTeile = explode('_',$Feld);
			$Key = $FeldTeile[4];
			$FeldName = substr($FeldTeile[0],3).'_'.$FeldTeile[1].'_'.$FeldTeile[2].'_'.$FeldTeile[3];

			$SQL = 'SELECT * FROM KONTAKTEVERFUEGBARKEITEN';
			$SQL .= ' WHERE KOV_KEY = :var_kov_key';
    		$BindeVariablen=array();
    		$BindeVariablen['var_kov_key']=$DB->FeldInhaltFormat('N0',$Key);
			$rsKOV = $DB->RecordSetOeffnen($SQL,$BindeVariablen);

			if(!isset($FeldListe[$Key]))
			{
				$FeldListe[$Key]='';
			}
			$WertNeu=$DB->FeldInhaltFormat($rsKOV->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
			$WertAlt=$DB->FeldInhaltFormat($rsKOV->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName.'_'.$Key],true);
			$WertDB=$DB->FeldInhaltFormat($rsKOV->FeldInfo($FeldName,'TypKZ'),$rsKOV->FeldInhalt($FeldName),true);

			if(isset($_POST['old'.$FeldName.'_'.$Key]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
			{
				if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
				{
					$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
				}
				else
				{
					$FeldListe[$Key] .= ', '.$FeldName.'=';

					if($_POST[$Feld]=='')	// Leere Felder immer als NULL
					{
						$FeldListe[$Key].=' null';
					}
					else
					{
						$FeldListe[$Key].=$WertNeu;
					}
				}
			}
		}

		// Jetzt alle einzeln speichern
		foreach($FeldListe AS $Key=>$Liste)
		{
			$SQL = '';
			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsKOV->FeldInhalt('KOV_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($Liste!='')
			{
				$SQL = 'UPDATE KontakteVerfuegbarkeiten SET';
				$SQL .= substr($Liste,1);
				$SQL .= ', KOV_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
				$SQL .= ', KOV_USERDAT = SYSDATE';
				$SQL .= ' WHERE KOV_KEY = '.$DB->FeldInhaltFormat('N0',$Key);

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',0912221707,$SQL,2);
				}
			}
		}
	}

	//**************************************************************************************
	//**  ATU Dienstleistungsnummern
	//**************************************************************************************

	$Felder = $Form->NameInArray($_POST,'txtXHW_WERTTXT_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFORMular::NAMEINARRAY_SUCHTYP_ANFANG);
	if(!empty($Felder) AND $Felder[0]!='')
	{
		$FeldListe = array();
		foreach($Felder as $Feld)
		{
			$FehlerListe=array();
			$FeldTeile = explode('_',$Feld);
			$Key = $FeldTeile[2];

			if($Key=='0')
			{
				if($_POST[$Feld]!='')
				{
					$rsXHW = $DB->RecordSetOeffnen('SELECT * FROM HILFSWERTE WHERE XHW_BEREICH = \'SRPLANUNG\' AND XHW_WERTTXT = '.$DB->FeldInhaltFormat('T',$_POST[$Feld]));
					if($rsXHW->EOF())
					{
						$SQL = 'INSERT INTO HILFSWERTE(XHW_ID,XHW_BEREICH,XHW_FELDTYP,XHW_WERTTXT,XHW_USER,XHW_USERDAT)';
						$SQL .=' VALUES(';
						$SQL .= ' '.$DB->FeldInhaltFormat('T',$_POST[$Feld]);
						$SQL .= ', '.$DB->FeldInhaltFormat('T','SRPLANUNG');
						$SQL .= ', '.$DB->FeldInhaltFormat('T','T');
						$SQL .= ', '.$DB->FeldInhaltFormat('T',$_POST[$Feld]);
						$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
						$SQL .= ', SYSDATE)';

						if($DB->Ausfuehren($SQL)===false)
						{
							throw new awisException('Fehler beim Speichern',1006221004,$SQL,2);
						}
					}
					else
					{
						// Hinweis anzeigen
						$Form->Hinweistext('');
					}
				}
			}
			else
			{
				$FeldName = 'XHW_WERTTXT';

				$rsXHW = $DB->RecordSetOeffnen('SELECT * FROM HILFSWERTE WHERE XHW_KEY = '.$DB->FeldInhaltFormat('N0',$Key));

				if($_POST[$Feld]=='')
				{
					if(($AWISBenutzer->HatDasRecht(11003)&8)==8)
					{
						$SQL = 'DELETE FROM HILFSWERTE';
						$SQL .= ' WHERE XHW_KEY = 0'.$Key;
						if($DB->Ausfuehren($SQL)===false)
						{
							throw new awisException('Fehler beim Speichern',1006221004,$SQL,2);
						}
					}
				}
				else
				{
					$WertNeu=$DB->FeldInhaltFormat($rsXHW->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsXHW->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName.'_'.$Key],true);
					$WertDB=$DB->FeldInhaltFormat($rsXHW->FeldInfo($FeldName,'TypKZ'),$rsXHW->FeldInhalt($FeldName),true);

					$FeldListe = '';

					if(isset($_POST['old'.$FeldName.'_'.$Key]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}

						$SQL = '';
						if(count($FehlerListe)>0)
						{
							$Meldung = str_replace('%1',$rsXHW->FeldInhalt('XHW_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
							foreach($FehlerListe AS $Fehler)
							{
								$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
								$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
							}
							$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
						}
						elseif($FeldListe!='')
						{
							$SQL = 'UPDATE HILFSWERTE SET';
							$SQL .= substr($FeldListe,1);
							$SQL .= ', XHW_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
							$SQL .= ', XHW_USERDAT = SYSDATE';
							$SQL .= ' WHERE XHW_KEY = '.$DB->FeldInhaltFormat('N0',$Key);

							if($DB->Ausfuehren($SQL)===false)
							{
								throw new awisException('Fehler beim Speichern',1006221004,$SQL,2);
							}
						}
					}
				}
			}
		}
	}

	//***********************************************************************************************
	//** Planung
	//***********************************************************************************************

	$Felder = $Form->NameInArray($_POST,'txtCHK_');
	if(!empty($Felder))
	{
		$Kombinationen = array();

		$Felder = $Form->NameInArray($_POST,'txtCHK_WPO',2,1);
		foreach($Felder as $Feld)
		{
			$FeldTeile = explode('_',$Feld);

			$Kontakte = $Form->NameInArray($_POST,'txtCHK_KON',2,1);

            if($FeldTeile[0]!='' AND $Kontakte[0]!='')
			{
				$SQL = 'SELECT COUNT(*) AS ANZ FROM WerkstattterminePlanungen';
                $SQL .= ' WHERE WTP_WPO_KEY = '.$FeldTeile[2];
                $SQL .= ' AND WTP_STATUS < 8';
                $rsANZ = $DB->RecordSetOeffnen($SQL);
				$Anz = $DB->FeldInhaltFormat('N0',$rsANZ->FeldInhalt('ANZ'));

                foreach($Kontakte as $Kontakt)
				{
					if(++$Anz>2)
					{
						break;
					}
                    $KontaktTeile = explode('_',$Kontakt);

					$DB->TransaktionBegin();

                    $SQL = 'INSERT INTO WerkstattterminePlanungen';
                    $SQL .= '(WTP_WPO_KEY,WTP_TAG,WTP_KON_KEY,WTP_ANFAHRTSZEIT,WTP_STATUS';
                    $SQL .= ',WTP_VK,WTP_ZEITMIN,WTP_FOLGETAG,WTP_FOLGEMIN,WTP_AUSLEIHE,WTP_USER,WTP_USERDAT)';
                    $SQL .= ' SELECT '.$FeldTeile[2];
                    $SQL .= ', '.$DB->FeldInhaltFormat('D',$_POST['txtPLAN_TAG'],false);
                    $SQL .= ', '.$KontaktTeile[2];
                    $SQL .= ', '.$DB->FeldInhaltFormat('N0',$_POST['txtFAHRZEIT_'.$KontaktTeile[2]],false);			// Anfahrtszeit aus der Maske!!
                    $_POST['txtFAHRZEIT_'.$KontaktTeile[2]]=0;			// Immer nur einmal berechnen!
                    $SQL .= ', 1';
                    $SQL .= ', WPO_VK';
                    $SQL .= ', WPO_ZEITMIN';
                    $SQL .= ', null';		// Folgetag aus der Maske
                    $SQL .= ', 0';			// Minuten aus der Maske
                    $SQL .= ', '.$DB->FeldInhaltFormat('N0',$KontaktTeile[3],false);
                    $SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
                    $SQL .= ', SYSDATE';
                    $SQL .= ' FROM WerkstattterminePositionen ';
                    $SQL .= ' INNER JOIN Werkstatttermine ON WPO_WTE_KEY = WTE_KEY';
                    $SQL .= ' LEFT OUTER JOIN WerkstattterminePlanungen ON WTP_WPO_KEY = WPO_KEY AND WTP_STATUS < 8 AND WTP_KON_KEY='.$KontaktTeile[2];
                    $SQL .= ' WHERE WPO_KEY='.$FeldTeile[2];
					$SQL .= ' AND WTP_KEY IS NULL';
$Form->DebugAusgabe(1,$SQL,$KontaktTeile,$FeldTeile);
                    if($DB->Ausfuehren($SQL)===false)
					{
                    	throw new awisException('Fehler beim Speichern',1007061847,$SQL,2);
                    }

                    $SQL = ' UPDATE WerkstattterminePositionen';
                    $SQL .= ' SET WPO_STATUS = 1';		// Verplant
					$SQL .= ' WHERE WPO_KEY='.$FeldTeile[2];
					if($DB->Ausfuehren($SQL)===false)
					{
						throw new awisException('Fehler beim Speichern',1007061847,$SQL,2);
                    }

                	$DB->TransaktionCommit();
				}
			}
		}
	}

	
	
	//*********************************************************************************************
	//** Langzeitausleihe
	//*********************************************************************************************
	
	$Felder = $Form->NameInArray($_POST,'txtWTG_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFORMular::NAMEINARRAY_SUCHTYP_ANFANG);
	
	if(!empty($Felder) AND $Felder[0]!='')
	{
		$FeldListe = array();
		$FehlerListe=array();

		$SQL = 'SELECT * FROM WERKSTATTTERMINEAUSLEIHE';
		$SQL .= ' WHERE WTG_KEY = :var_wtg_key';
		$DB->SetzeBindevariable('WTG', 'var_wtg_key', $_POST['txtWTG_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
		$rsWTG = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('WTG'));

		$TextKonserven[]=array('WTG','WTG_%');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		// Pflichtfelder pr�fen			
		$Fehler = '';
		$Pflichtfelder = array('WTG_FEB_KEY','WTG_KON_KEY','WTG_DATUMAB','WTG_DATUMBIS');
		
		foreach($Pflichtfelder AS $Pflichtfeld)
		{
			if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['WTG'][$Pflichtfeld].'<br>';
			}
		}
		
		if($_POST['txtWTG_DATUMVOM']!=$_POST['oldWTG_DATUMVOM'] AND $Form->PruefeDatum($_POST['txtWTG_DATUMVOM'],false,false,true)<strtotime('today 0:00'))
		{
			$Fehler .= ' Startdatum '.$TXT_Speichern['WTG']['WTG_DATUMVOM'].' darf nicht in der Vergangenheit liegen!<br>';				
		}
		if($_POST['txtWTG_DATUMBIS']!=$_POST['oldWTG_DATUMBIS'] AND $Form->PruefeDatum($_POST['txtWTG_DATUMBIS'],false,false,true)<strtotime('today 0:00'))
		{
			$Fehler .= ' Endedatum '.$TXT_Speichern['WTG']['WTG_DATUMBIS'].' darf nicht in der Vergangenheit liegen!<br>';				
		}
		
		// Differenz zwischen den Zeitr�umen pr�fen
		$MaxDiff = $AWISBenutzer->ParameterLesen('SmartRepair: Langzeitausleihe MaxTage');
		$Diff = $Form->DatumsDifferenz($_POST['txtWTG_DATUMVOM'],$_POST['txtWTG_DATUMBIS'],awisFormular::DATUMS_DIFFERENZ_TAGE,false);
		if($Diff<0)	// Endedatum vor Anfang?
		{
			$Fehler .= $TXT_Speichern['Fehler']['err_DatumEndeVorAnfang'].'<br>';
		}
		elseif($Diff>$MaxDiff)
		{
			$Fehler .= str_replace('$1', (int)$MaxDiff, $TXT_Speichern['Fehler']['err_DatumAbstandTage']).'<br>';
		}
		
			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
		$FeldListe='';
		$SQL = '';

		if(floatval($_POST['txtWTG_KEY'])==0)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO WERKSTATTTERMINEAUSLEIHE';
			$SQL .= '(WTG_KON_KEY,WTG_DATUMVOM,WTG_DATUMBIS,WTG_FEB_KEY';
			$SQL .= ',WTG_USER, WTG_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtWTG_KON_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtWTG_DATUMVOM'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtWTG_DATUMBIS'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtWTG_FEB_KEY'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';


			$DB->Ausfuehren($SQL);
		}
		else 					// ge�nderte Zuordnung
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
			// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsWTG->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsWTG->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsWTG->FeldInfo($FeldName,'TypKZ'),$rsWTG->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsWTG->FeldInhalt('WTG_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE WERKSTATTTERMINEAUSLEIHE SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', WTG_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', WTG_userdat=sysdate';
				$SQL .= ' WHERE WTG_key=0' . $_POST['txtWTG_KEY'] . '';
				$DB->Ausfuehren($SQL);
			}
		}
	}

	

	//*********************************************************************************************
	//** Parkshaden
	//*********************************************************************************************
	
	$Felder = $Form->NameInArray($_POST,'txtPKS_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFORMular::NAMEINARRAY_SUCHTYP_ANFANG);


	if(!empty($Felder))
	{
        $FehlerListe=array();
    
        $TextKonserven[]=array('PKS','%');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);
        
        //Status pr�fen
        $SQL =  ' SELECT * ';
        $SQL .= ' FROM parkschaeden  ';
        $SQL .= ' where pks_key = :var_pks_key ';
    
        $DB->SetzeBindevariable('PKS', 'var_pks_key', $_POST['txtPKS_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
        $rsPKS = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('PKS'));
         
        //Alle Felder (Wegen den Ajaxfeldern, so nicht aus den $_POST zu holen leider.)
        //$UpdateInsertFelder = array('PKS_ERFDATUM','PKS_GROESSE','PKS_VORNAME','PKS_NACHNAME','PKS_STRASSE','PKS_PLZ','PKS_ORT','PKS_FIL_ID', 'PKS_VERSSCHEINNR', 'PKS_KFZKENN','PKS_VERSNR');
        $UpdateInsertFelder[0]['Feldname'] = 'PKS_ERFDATUM';
        $UpdateInsertFelder[0]['Format'] = 'D';
        
        $UpdateInsertFelder[1]['Feldname'] = 'PKS_GROESSE';
        $UpdateInsertFelder[1]['Format'] = 'CMB';   
        
        $UpdateInsertFelder[2]['Feldname'] = 'PKS_VORNAME';
        $UpdateInsertFelder[2]['Format'] = 'T';
        
        $UpdateInsertFelder[3]['Feldname'] = 'PKS_NACHNAME';
        $UpdateInsertFelder[3]['Format'] = 'T';
        
        $UpdateInsertFelder[4]['Feldname'] = 'PKS_STRASSE';
        $UpdateInsertFelder[4]['Format'] = 'T';
        
        $UpdateInsertFelder[5]['Feldname'] = 'PKS_PLZ';
        $UpdateInsertFelder[5]['Format'] = 'PLZ';
       
        $UpdateInsertFelder[6]['Feldname'] = 'PKS_ORT';
        $UpdateInsertFelder[6]['Format'] = 'T';
        
        $UpdateInsertFelder[7]['Feldname'] = 'PKS_FIL_ID';
        $UpdateInsertFelder[7]['Format'] = 'FIL';
        
        $UpdateInsertFelder[8]['Feldname'] = 'PKS_VERSSCHEINNR';
        $UpdateInsertFelder[8]['Format'] = 'T';
        
        $UpdateInsertFelder[9]['Feldname'] = 'PKS_KFZKENN';
        $UpdateInsertFelder[9]['Format'] = 'T';
        
        $UpdateInsertFelder[10]['Feldname'] = 'PKS_VERSNR';
        $UpdateInsertFelder[10]['Format'] = 'CMB';
    
        $UpdateInsertFelder[11]['Feldname'] = 'PKS_STATUS';
        $UpdateInsertFelder[11]['Format'] = 'N0';
        
        $UpdateInsertFelder[12]['Feldname'] = 'PKS_SCHADENART';
        $UpdateInsertFelder[12]['Format'] = 'CMB';
        
        $UpdateInsertFelder[13]['Feldname'] = 'PKS_SCHADENPOS';
        $UpdateInsertFelder[13]['Format'] = 'CMB';
        
        $UpdateInsertFelder[14]['Feldname'] = 'PKS_FARBE';
        $UpdateInsertFelder[14]['Format'] = 'CMB';
        
        $UpdateInsertFelder[15]['Feldname'] = 'PKS_NOTIZ';
        $UpdateInsertFelder[15]['Format'] = 'T';
        
        $UpdateInsertFelder[16]['Feldname'] = 'PKS_DATUMRR';
        $UpdateInsertFelder[16]['Format'] = 'D';
        
        $UpdateInsertFelder[17]['Feldname'] = 'PKS_STAND';
        $UpdateInsertFelder[17]['Format'] = 'CMB';
        
        $UpdateInsertFelder[18]['Feldname'] = 'PKS_DATUMBES';
        $UpdateInsertFelder[18]['Format'] = 'DHZ';
        
        $UpdateInsertFelder[19]['Feldname'] = 'PKS_GRUNDABL';
        $UpdateInsertFelder[19]['Format'] = 'T';
        
        $UpdateInsertFelder[20]['Feldname'] = 'PKS_TELEFON';
        $UpdateInsertFelder[20]['Format'] = 'T';
        
        $UpdateInsertFelder[21]['Feldname'] = 'PKS_MOBIL';
        $UpdateInsertFelder[21]['Format'] = 'T';
        
        $UpdateInsertFelder[22]['Feldname'] = 'PKS_GRUNDSTORNO';
        $UpdateInsertFelder[22]['Format'] = 'T';
    
        $UpdateInsertFelder[23]['Feldname'] = 'PKS_KHERNR';
        $UpdateInsertFelder[23]['Format'] = 'CMB';
        
        $UpdateInsertFelder[24]['Feldname'] = 'PKS_KMODNR';
        $UpdateInsertFelder[24]['Format'] = 'CMB';
        
        $UpdateInsertFelder[25]['Feldname'] = 'PKS_KTYPNR';
        $UpdateInsertFelder[25]['Format'] = 'CMB';
        
        // Pflichtfelder festlegen
        $KeinWertFehler = '';
        $Formatfehler ='';
        $SpeicherFehler =false;
        
        $PKS_STATUS = isset($_POST['txtPKS_STATUS'])?$_POST['txtPKS_STATUS']:$rsPKS->FeldInhalt('PKS_STATUS');
        
        $MailTyp = '';
    
        if($PKS_STATUS==0 or ($_POST['txtPKS_KEY'] == '0'  and $PKS_STATUS == 0)) //offen/neuanlage
        {
           $Pflichtfelder = array('PKS_VORNAME','PKS_NACHNAME','PKS_ERFDATUM','PKS_FIL_ID');
        }
        elseif ($PKS_STATUS==1) //Servicecenter setzt DS auf abgeschlossen. 
        {
            $Pflichtfelder = array('PKS_ERFDATUM','PKS_GROESSE','PKS_VORNAME','PKS_NACHNAME','PKS_STRASSE','PKS_PLZ','PKS_ORT','PKS_FIL_ID', 'PKS_VERSSCHEINNR', 'PKS_VERSNR');
            if ($rsPKS->FeldInhalt('PKS_STATUS')!= 1)
            {
                $MailTyp = 'LL'; //-->Mail an Leiter Lack
            }
        }
        elseif($PKS_STATUS==2) //Leiterlack setzt DS auf abgeschlossen 
        {
            $Pflichtfelder = array('PKS_DATUMRR', 'PKS_STAND');
        }
        elseif($PKS_STATUS==3) //Storniert DS
        {
            $Pflichtfelder = array('PKS_VERSNR','PKS_GRUNDSTORNO', 'PKS_VORNAME','PKS_NACHNAME','PKS_ERFDATUM','PKS_FIL_ID');
            $MailTyp = 'S';
            //-->Mail an Versicherung, dass Auftrag storniert. 
        }	    
        
        //Standardm��ige Pflichtfelder
        foreach($Pflichtfelder AS $Pflichtfeld)
        {
            $Praefix = (isset($_POST['txt'.$Pflichtfeld])?'txt':'suc');
            if(isset($_POST[$Praefix.$Pflichtfeld]) AND ($_POST[$Praefix.$Pflichtfeld]=='' or $_POST[$Praefix.$Pflichtfeld] == '::bitte w�hlen::'))
            {             
                $KeinWertFehler .= $TXT_Speichern['PKS'][$Pflichtfeld].', ';
            }
        }
        
        //Abh�ngige Pflichtfelder
        if($PKS_STATUS==1)
        {
            if((isset($_POST['txtPKS_TELEFON']) AND $_POST['txtPKS_TELEFON'] == '' ) AND (isset($_POST['txtPKS_MOBIL']) AND $_POST['txtPKS_MOBIL'] == '') )
            {
               //Eins von beiden muss da sein. ..
               $KeinWertFehler .= $TXT_Speichern['PKS']['PKS_ERR_TELHANDY'].'.';      
            }
                    
            
        }
        elseif($PKS_STATUS==2)
        {
            if($_POST['txtPKS_STAND']=='1')
            {
                //Besichtigung
                if(isset($_POST['txtPKS_DATUMBES']) and $_POST['txtPKS_DATUMBES'] == '')
                {
                    $KeinWertFehler .= $TXT_Speichern['PKS']['PKS_DATUMBES'].',.';
                }
            }
            elseif($_POST['txtPKS_STAND']=='0')
            {
                //Ablehunungsgrund
                if(isset($_POST['txtPKS_GRUNDABL']) and $_POST['txtPKS_GRUNDABL'] == '')
                {
                    $KeinWertFehler .= $TXT_Speichern['PKS']['PKS_GRUNDABL'].',.';
                }
                $MailTyp = 'A';
            }     
        }
        
    
        //Formate Pr�fen
        foreach($UpdateInsertFelder AS $Element => $Inhalt)
        {
            $Feld = $Inhalt['Feldname'];
            $Format = $Inhalt['Format'];
    
            $Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
            
            if (isset($_POST[$Praefix.$Feld]) AND $_POST[$Praefix.$Feld] != '' and $_POST[$Praefix.$Feld] != '::bitte w�hlen::' )
            {
                if ($Format == 'N0')
                {
                     //Numerisch
                     if(!is_numeric($_POST[$Praefix.$Feld]))
                     {
                         $Formatfehler = $TXT_Speichern['PKS']['PKS_ERR_NUMMER'] . $TXT_Speichern['PKS'][$Feld] . '<br>'; 
                     }
                }
                elseif (substr($Format,0,1) == 'D')
                {
                    
                    //Datum
                    if($Form->Format('D',$_POST[$Praefix.$Feld],false) == '01.01.1970') 
                    {
                        $Formatfehler = $TXT_Speichern['PKS']['PKS_ERR_DATUM'] . $TXT_Speichern['PKS'][$Feld] . '<br>';
                    }
                    else
                    {                
                        //Datum heute bis Zukunft
                        if ($Format == 'DHZ') //Datum heute bis Zukunft
                        {                  
                            if ((strtotime($_POST[$Praefix.$Feld]) < strtotime(date('d.m.Y',time()))))
                            {       
                                $Formatfehler = $TXT_Speichern['PKS']['PKS_ERR_DATUM_VERGANGENHEIT'] .' ' . $TXT_Speichern['PKS'][$Feld] . '<br>';
                            }
                        }     
                    }
                }
                elseif($Format=='FIL')
                {     
                    if (isset($_POST[$Praefix.$Feld]))
                    {
                        $SQL  = ' select FEB_BEZEICHNUNG,(select FIL_BEZ from Filialen where FIL_ID = :var_FIL_ID) as FIL_BEZ from filialebenen where feb_key = ';
                        $SQL .= ' (select distinct feb_feb_key from v_filialebenenrollen_aktuell  ';
                        $SQL .= ' inner join Filialebenen on xx1_feb_key = feb_key ';
                        $SQL .= ' where xx1_fil_id =:var_FIL_ID)';
                        $DB->SetzeBindevariable('PKS', 'var_FIL_ID' , intval($_POST[$Praefix.$Feld]));
                      
                        if ($DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('PKS',true))==0)
                        {
                            $Formatfehler = $TXT_Speichern['PKS']['PKS_ERR_NUMMER'] . $TXT_Speichern['PKS'][$Feld] . '<br>';
                        }
                    
                        //Schau nach ob ein Zust�ndiger gefunden wurde.
                        $SQLZustaend  ='select ';
                        $SQLZustaend .='  (select KON_NAME2||\' \'||KON_NAME1 as KON_NAME from v_filialebenenrollen_aktuell ';
                        $SQLZustaend .='   inner join kontakte on xx1_kon_key = kon_key';
                        $SQLZustaend .='   where xx1_fil_id = :var_FIL_ID';
                        $SQLZustaend .='   and xx1_fer_key = 25) GBL,  ';
                        $SQLZustaend .='   (select kko_wert from v_filialebenenrollen_aktuell ';
                        $SQLZustaend .='   inner join kontaktekommunikation on xx1_kon_key = kko_kon_key';
                        $SQLZustaend .='   where xx1_fil_id = :var_FIL_ID';
                        $SQLZustaend .='   and xx1_fer_key = 25 and kko_kot_key in (7) and rownum = 1) GBL_Mail  ';
                        //$SQLZustaend .='  (select KON_NAME2||\' \'||KON_NAME1 as KON_NAME from v_filialebenenrollen_aktuell';
                        //$SQLZustaend .='   inner join kontakte on xx1_kon_key = kon_key';
                        //$SQLZustaend .='   where xx1_fil_id = :var_FIL_ID';
                        //$SQLZustaend .='   and xx1_fer_key = 47';
                        //$SQLZustaend .='  ) LL,';
                        //$SQLZustaend .='   (select kko_wert from v_filialebenenrollen_aktuell ';
                        //$SQLZustaend .='   inner join kontaktekommunikation on xx1_kon_key = kko_kon_key';
                        //$SQLZustaend .='   where xx1_fil_id = :var_FIL_ID';
                        //$SQLZustaend .='   and xx1_fer_key = 47 and kko_kot_key in (7) and rownum = 1) LL_MAIL';
                        $SQLZustaend .='  from dual';
                        $DB->SetzeBindevariable('PKS', 'var_FIL_ID' , intval($_POST[$Praefix.$Feld]));
                        
                        //RS wird auch f�r Emailversand verwendet. 
                        $rsZustaendigkeit = $DB->RecordSetOeffnen($SQLZustaend,$DB->Bindevariablen('PKS',true));
                        
                        if ($rsZustaendigkeit->FeldInhalt('GBL') == '' and $rsZustaendigkeit->FeldInhalt('LL') == '' )
                        {                    
                            $Formatfehler .= $TXT_Speichern['PKS']['PKS_ERR_KEINZUSTAENDIGER'];
                        }
                    }                           
                }
                elseif($Format=='CMB')
                {
                    if ($_POST[$Praefix.$Feld] == 'B')
                    {
                        $_POST[$Praefix.$Feld] = '';
                        //var_dump($_POST[$Praefix.$Feld]);
                    }
                } 
            } 
        }
    
        
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if($KeinWertFehler!='')
        {
            $SpeicherFehler = true;
            $Form->ZeileStart();
            echo('<span class=HinweisText>'.$TXT_Speichern['Fehler']['err_KeinWert'].' '.substr($KeinWertFehler,0, strlen($KeinWertFehler)-2).'.</span>');
            $Form->ZeileEnde();
        }
        if($Formatfehler != '')
        {
            $SpeicherFehler = true;
            $Form->ZeileStart();
            echo('<span class=HinweisText>'.$Formatfehler.'</span>');
            $Form->ZeileEnde();
        }
        if ($KeinWertFehler=='' and $Formatfehler=='' )
        {
           $SQL = '';
           if($_POST['txtPKS_KEY']=='0') //Neuer DS
            {   
                $SQL = 'INSERT INTO PARKSCHAEDEN';
                $SQL .= '(pks_user, pks_userdat';
                $SQLFelder = '';
                $SQLWerte = '';
                foreach($UpdateInsertFelder AS $Element => $Inhalt)
                {
                    $Feld = $Inhalt['Feldname'];
                    $Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
                     
                    if (isset($_POST[$Praefix.$Feld]) and $_POST[$Praefix.$Feld] != '' and $_POST[$Praefix.$Feld] != '::bitte w�hlen::')
                    {   
                        $SQLFelder .= ', '.$Feld;
                        $SQLWerte  .= ', \''.$_POST[$Praefix.$Feld].'\'';
                    }
                }
                $SQL .= $SQLFelder; //Relevante Datenfelder dazuf�gen
                $SQL .= ')VALUES (';
                $SQL .= '\'' .$AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= $SQLWerte; //Relevante Datenwerte dazuf�gen.
                $SQL .= ')';
                
                $DB->Ausfuehren($SQL);
                
                $SEQSQL = 'select seq_pks_key.CURRVAL AS Key from dual';
                $rsSEQ = $DB->RecordSetOeffnen($SEQSQL);
                $SEQ = $rsSEQ->FeldInhalt('KEY');
                
                
                $Form->Hinweistext($TXT_Speichern['PKS']['PKS_INSERTOK']);
            }
            else // ge�nderterDS
            {
                //var_dump($_POST);
                $FeldListe = '';
                foreach($UpdateInsertFelder AS $Element => $Inhalt)
                {
                    $Feld = $Inhalt['Feldname'];
                    $Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
                    //echo $Praefix.$Feld.'<br>';
                    if((isset($_POST['old'.$Feld]) or $Praefix == 'suc')and isset($_POST[$Praefix.$Feld])) //Sucfelder haben kein old
                    {                  
                        $WertNeu=$_POST[$Praefix.$Feld]!='::bitte w�hlen::'?$_POST[$Praefix.$Feld]:'';
                        $WertAlt=$Praefix=='txt'?$_POST['old'.$Feld]:'';
                        $WertDB=$rsPKS->FeldInhalt($Feld);
                        
                        if((isset($_POST['old'.$Feld])or $Praefix=='suc')) //AND ($WertDB=='null'))// OR $WertAlt!=$WertNeu))
                        {                       
                            //echo $Feld . "<br>";
                            $FeldListe .= ', '.$Feld.'=';            
                            if($WertNeu=='')	// Leere Felder immer als NULL
                            {
                                $FeldListe.=' null';
                            }
                            else
                            {
                                $FeldListe.= '\''.$WertNeu . '\'';
                            }
                             
                        }
                    }
                }  
                
                if($FeldListe!='')
                {
                	$SQL = 'UPDATE parkschaeden SET';
                    $SQL .= substr($FeldListe,1);
                    $SQL .= ', PKS_user=\''.$AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ', PKS_userdat=sysdate';
                    $SQL .= ' WHERE PKS_key=0' . $_POST['txtPKS_KEY'] . '';
                    //var_dump($SQL);
                    $DB->Ausfuehren($SQL);
                    $Form->Hinweistext($TXT_Speichern['PKS']['PKS_UPDATEOK']);
                    $SEQ = $_POST['txtPKS_KEY'];
                }
            }
    
            //hier Mails verschicken, da kein Fehler im DS gefunden wurde und der DS so in den entsprechenden 
            //Status gesetzt wurde
            
            if($MailTyp != '')
            {
            	include('srpep_Parkschaden_Email.php');
            }
        }        
    }

	//Mitarbeiterzuordnung

    if(isset($_GET['cmdAktion']) and $_GET['cmdAktion'] == 'Mitarbeiter'){
        $Felder = $Form->NameInArray($_POST,'txtKON_PER_NR__',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG);
        $SR = new srpep_funktionen($AWISBenutzer,$DB);
        $Geandert = 0;
        $UnbekanntePersnr = array();
        foreach ($Felder as $Feld ){
            $PersNr = $_POST[$Feld];
            $FIL_ID = explode('__',$Feld)[1];

            $Status = $SR->UpdateFRZ($PersNr,$FIL_ID);

            switch ($Status){
                case srpep_funktionen::STATUS_OK_GEANDERT:
                    $Geandert++;
                    break;
                case srpep_funktionen::STATUS_ERROR_PERSNR:
                    $UnbekanntePersnr[$FIL_ID] = $PersNr;
                    break;
                case srpep_funktionen::STATUS_OK_NICHTS_GEAENDERT:
                default:
                    break;
            }

        }

        if($Geandert > 0){
            $Form->ZeileStart();
            $Form->Hinweistext($Geandert . ' Datens�tze ge�ndert.',awisFormular::HINWEISTEXT_OK);
            $Form->ZeileEnde();
        }

        if(count($UnbekanntePersnr) > 0){
            $Text = 'Folgende Personalnummern sind unbekannt oder k�nnen nicht eindeutig zugeordnet werden: <br>';

            foreach ($UnbekanntePersnr as $Fil => $Persnr){
                $Text .= 'Filiale ' . $Fil . ': ' . $Persnr . '<br>';
            }
            $Text .= 'Kontaktieren Sie ggf. die EDV-Hotline.';
            $Form->ZeileStart();
            $Form->Hinweistext($Text, awisFormular::HINWEISTEXT_FEHLER);
            $Form->ZeileEnde();
        }

    }

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}



?>