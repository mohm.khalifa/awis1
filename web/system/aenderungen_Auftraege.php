<?php
	global $HTTP_POST_VARS;
	global $HTTP_GET_VARS;
	global $PHP_AUTH_USER;

	global $RechteStufe;

	$ListeZeigen=FALSE;
/***************************************************************************************************
* 
* Daten L�SCHEN
* 
****************************************************************************************************/	

	foreach($HTTP_POST_VARS as $Eintrag => $EintragWert)		
	{
		if(strpos($Eintrag,"cmdDelXSD_")!==FALSE)
		{
			$SQL = (substr($Eintrag,10));
			$SQL = substr($SQL,0,strlen($SQL)-2);
			$SQL = "DELETE FROM DatenAenderungenAuftraege WHERE XSD_KEY=0" . $SQL;
			
			awisExecute($con, $SQL);

			$ListeZeigen = True;
			break;	// Nicht weiter suchen
		}


	   if(strpos($Eintrag,"cmdSpeichern")!==FALSE)
	   {

			If($HTTP_POST_VARS["txtXSD_KEY"]==0)
			{
				$SQL = "INSERT INTO DatenAenderungenAuftraege(";
				$SQL .= "XSD_,XSD_USER,XSD_USERDAT)";
				$SQL .= "VALUES( '" . $HTTP_POST_VARS["txtXSD_"] . "'";
				$SQL .= ", '" . $HTTP_POST_VARS["txtXSD_"] . "'";

				$SQL .= ", '" . $PHP_AUTH_USER . "'";				
				$SQL .= ", SYSDATE)";				

				awisExecute($con, $SQL);
				$ListeZeigen=True;
			}
			else
			{
				$SQL = "UPDATE DatenAenderungenAuftraege SET";
				$SQL .= " XSD_='" . $HTTP_POST_VARS["txtXSD_"] . "'";

				$SQL .= ", XSD_USER='" . $PHP_AUTH_USER . "'";				
				$SQL .= ", XSD_USERDAT=SYSDATE";
				$SQL .= " WHERE XSD_KEY=" . intval($HTTP_POST_VARS["txtXSD_KEY"]) . "";

				awisExecute($con, $SQL);
				$ListeZeigen=True;
			}

			break;
	   }

	}	
	
/***************************************************************************************************
* 
* Daten anzeigen
* 
****************************************************************************************************/	

	$SQL = "SELECT DATENAENDERUNGENAUFTRAEGE.*, XSA_BEZEICHNUNG, XSA_AUSWAHLSQL ";
	$SQL .= " FROM DATENAENDERUNGENAUFTRAEGE, DATENAENDERUNGEN, FELDNAMEN, TABELLENKUERZEL";
	$SQL .= " WHERE XSA_XFN_FELD = XFN_FELD AND XSA_XXX_KUERZEL = XXX_KUERZEL ";
	$SQL .= " AND XSD_XSA_KEY = XSA_KEY";
	
	if($HTTP_GET_VARS["txtXSD_KEY"] != '')
	{
		$SQL .= " AND XSD_KEY=" . $HTTP_GET_VARS["txtXSD_KEY"];
	}
	
	$rsDatenAenderung = awisOpenRecordset($con, $SQL);
	$rsDatenAenderungZeilen=$awisRSZeilen;

		// Feldnamen holen
	$Felder = array_Keys($rsDatenAenderung);
	for($i=0;$i<sizeof($Felder);$i++)
	{
		$FeldListe[$i] = $Felder[$i];
	}
	$FeldListe = awis_FeldNamenListe($con, $FeldListe, $SatzNr = 1);
	

	If(($HTTP_GET_VARS["txtXSD_KEY"]=='' AND $HTTP_POST_VARS["txtXSD_KEY"]=='') OR $ListeZeigen)
	{
		// �berschrift aufbauen
	
		print "<form method=post name=frmDatenAenderungen action=./aenderungen_Main.php?CmdAktion=Auftraege>";
		
		print "<table  width=100% id=DatenTabelle border=1><tr>";
		
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XSD_"][0] . "</span></td>";
		print "<td><span class=DatenFeldNormalFett>" . $FeldListe["XSD_"][0] . "</span></td>";

		print "<td></td>";
		
		print "</tr>";
		

		for($i=0;$i<$rsDatenAenderungZeilen;$i++)
		{
			print "<tr>";
	
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";
			print "<input type=hidden name=txtXSD_KEY value=" . $rsDatenAenderung["XSD_KEY"][$i] . ">";
			print $rsDatenAenderung["XSA_BEZEICHUNG"][$i] . "</td>";			
			print "<td  " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsDatenAenderung["XSA_BEZEICHUNG"][$i] . "</td>";			
	
			if($RechteStufe > 1)
			{
				print "<td>";
			}

			if(($RechteStufe & 2)==2)
			{
	        	print "<input type=image alt='Feld �ndern' src=/bilder/aendern.png name=cmdAendern_" . $rsDatenAenderung["XSD_KEY"][$i] . " onclick=submit();>";
			}
	
			if(($RechteStufe & 4)==4)
			{
	        	print "<input type=image alt='Feld l�schen' src=/bilder/muelleimer.png name=cmdDelXSA_" . $rsDatenAenderung["XSD_KEY"][$i] . " onclick=submit();>";
			}

			if($RechteStufe > 1)
			{
				print "</td>";
			}

			print "</tr>";
		}
		
		print "</table>";
	}
	else			// Daten bearbeiten
	{
		print "<form method=post name=frmDatenAenderungen action=./aenderungen_Main.php?CmdAktion=Auftraege>";
		
		print "<input name=cmdAktion value=Auftraege type=hidden>";
		print "<table  width=100% id=DatenTabelle border=1>";

		$rsFelder = awisOpenRecordset($con, "SELECT XSA_Key, XSA_Bezeichnung FROM DatenAenderungen WHERE XSA_Aktiv<>0 ORDER BY XSA_Bezeichnung");
		print "\n<tr>";
		
		print "<td><span class=FeldBez>" . $FeldListe["XSA_BEZEICHNUNG"][0] . "</span></td>";
		print "<td><select name=txtXSD_XSA_Key onchange=submit();>";
		print "<option value=0>Bitte w�hlen...</option>";
		for($i=0;$i<$awisRSZeilen;$i++)
		{
			print "<option ";
			print " value=" . $rsFelder["XSA_Key"][$i] . ">";
			print $rsFelder["XSA_BEZEICHNUNG"][$i] . "</option>";
		}
		print "</select></tr>";	
		unset($rsFelder);
		
		if($rsDatenAenderung["XSA_AUSWAHLSQL"][0]!="")
		{
				// Datensatz
			print "\n\r<tr>";
			print "<td><span class=FeldBez>" . $FeldListe["XSD_KEYWERT"][0] . "</span></td>";
			$rsFelder = awisOpenRecordset($con, $rsDatenAenderung["XSA_AUSWAHLSQL"][0]);
		
			print "<td><input name=txtXSD_KEYWERT type=Text size=60 Value=" . $rsDatenAenderung["XSD_KEYWERT"][0] . "></td>";
			print "</tr>";
	
			print "</table>";
	
    	    print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
		}
	}	
	
	print "</form>";
	unset($rsDatenAenderung);
	
?>