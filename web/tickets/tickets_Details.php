<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisOT.inc');
try
{
    // Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('OTT','%');
	$TextKonserven[]=array('OFO','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

    $Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht17000 = $AWISBenutzer->HatDasRecht(17000);
	if($Recht17000==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_OTTickets'));
	if(empty($Param))
	{
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='OTT_KEY DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['OTT_BETREFF'] = $_POST['sucOTT_BETREFF'];
		$Param['OTT_TICKETID'] = ''; // Voerst nicht $_POST['sucOTT_TICKETID'];
		$Param['OTT_TICKETNR'] = $_POST['sucOTT_TICKETNR'];
		$Param['OTT_OFO_KEY'] = $_POST['sucOTT_OFO_KEY'];
		$Param['DATUMBIS'] = '';//$_POST['sucDATUMBIS'];
		$Param['DATUMVOM'] = '';//$_POST['sucDATUMVOM'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='OTT_KEY DESC';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_OTTickets",serialize($Param));
	}
	elseif(isset($_POST['cmdDSNeu_x']) OR isset($_GET['Neu']))
	{
		$AWIS_KEY1=-1;
		$_POST['cmdDSNeu_x']=1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_OTTickets'));
		$Param['KEY']='';
	}
	elseif(isset($_GET['OTT_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['OTT_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_OTTickets'));
	}
	elseif($AWIS_KEY1==0) 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='OTT_KEY DESC';
			$AWISBenutzer->ParameterSchreiben('Formular_OTTickets',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

    $SpeichernOK = false;
    if(isset($_POST['cmdSpeichern_x']))
    {
        include('./tickets_speichern.php');
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_OTTickets'));
        $Param['OTT_TICKETNR'] = $AWIS_KEY2;
        if($AWIS_KEY1>0){
            $SpeichernOK = true;
        }
    }

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = ' ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY OTT_KEY DESC';
			$Param['ORDER']='OTT_KEY DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT OTTickets.*';
	$SQL .= ', OFO_OTPFAD, OFO_ANZEIGEFELDER';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM OTTickets';
	$SQL .= ' INNER JOIN OTFORMULARE ON OTT_OFO_KEY = OFO_KEY';
	
	// Standard: Nur eigene Tickets anzeigen
	if(($Recht17000&16)!=16)
	{
	    $Bedingung .= ' AND OTT_XBN_KEY = '.$DB->WertSetzen('OTT', 'N0', $AWISBenutzer->BenutzerID());
	}
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_OTTickets',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('OTT', false));
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsOTT = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('OTT'));
	$AWISBenutzer->ParameterSchreiben('Formular_OTTickets',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name="frmOTTickets" action="./tickets_Main.php?cmdAktion=Details' . (isset($_GET['OFO_KEY']) ? '&OFO_KEY=' . $_GET['OFO_KEY'] : '') . '" method="POST" enctype="multipart/form-data">');

	if(isset($_POST['cmdDSNeu_x']))        // Neues Ticket hinzuf�gen, Schritt 1 - Auswahl an Formularen
	{
        $SQL = 'SELECT DISTINCT OFO_KEY, OFO_BEZEICHNUNG, OFO_BEMERKUNG';
        $SQL .= ' FROM OTFormulare';
    	$SQL .= ' INNER JOIN v_AccountRechte ON OFO_XRC_ID = XRC_ID AND BITAND(XBA_STUFE,OFO_RechteStufe) = OFO_RechteStufe AND XBN_KEY = '.$DB->WertSetzen('OFO', 'N0', $AWISBenutzer->BenutzerID());
    	$SQL .= ' WHERE OFO_STATUS = \'A\'';
    	$SQL .= ' ORDER BY OFO_BEZEICHNUNG';
    	$rsOFO = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('OFO'));

    	if(!$rsOFO->EOF())
    	{
    	    $Form->ZeileStart();
    	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OFO']['OFO_BEZEICHNUNG'], 450);
    	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OFO']['OFO_BEMERKUNG'], 450);

    	    $Form->ZeileEnde();
    	}

    	$DS=0;
    	while(!$rsOFO->EOF())
    	{
    	    $Link = './tickets_Main.php?cmdAktion=Details&OFO_KEY='.$rsOFO->FeldInhalt('OFO_KEY');
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('OFO_BEZEICHNUNG', $rsOFO->FeldInhalt('OFO_BEZEICHNUNG'), 1, 450,false,($DS%2),'',$Link);
            $Form->Erstelle_ListenFeld('OFO_BEMERKUNG', $rsOFO->FeldInhalt('OFO_BEMERKUNG'), 1, 450,false,($DS%2),'','','T','L',$rsOFO->FeldInhalt('OFO_BEMERKUNG'));
            $Form->ZeileEnde();

            $DS++;
    	    $rsOFO->DSWeiter();
    	}
	}
	elseif(isset($_GET['OFO_KEY']) and !$SpeichernOK)		// Neues Ticket anlegen, Schritt 2
	{
        $DetailAnsicht = true;
        require_once './ticket_funktionen.inc';
        $TicketFunktionen = new awis_ticket_funktionen($DB, $AWISBenutzer);
        $AWISCursorPosition = '';
        
        $OTObj = new awisOT($AWISBenutzer);
        if($OTObj->VerbindungsTest($AWISBenutzer->ParameterLesen('OT-SVCTest-Ordner'))===false)
        {
        	$Form->Erstelle_TextLabel('Keine Verbindung zum OmniTracker Server. Hotline ist verst&auml;ndigt. Setzen Sie ihre Bearbeitung fort.',800);
        	$MailObj = new awisMailer($DB, $AWISBenutzer);
        	$MailObj->Betreff('OmniTracker WebService: Kein Zugriff');
        	$MailObj->Absender('shuttle@de.atu.eu');
        	$MailObj->AdressListe(awisMailer::TYP_TO,'matthias.sischka@de.atu.eu');
        	$MailObj->Text('Kein Zugriff auf den WebService des OmniTrackers '.$OTObj->ServerName(),awisMailer::FORMAT_TEXT,true);
        	
        	$MailObj->MailSenden();
        }

	    $SQL = 'SELECT OFO_BEZEICHNUNG, OFO_BEMERKUNG,OFO_HINWEISE,OFO_KENNUNG';
	    $SQL .= ', OTFORMULARFELDER.*';
	    $SQL .= ', OTFELDER.*';
	    $SQL .= ' FROM OTFORMULARE';
	    $SQL .= ' INNER JOIN OTFORMULARFELDER ON OFO_KEY = OFF_OFO_KEY';
	    $SQL .= ' INNER JOIN OTFELDER ON OTF_KEY = OFF_OTF_KEY';
	    $SQL .= ' WHERE OFO_KEY = '.$DB->FeldInhaltFormat('N0',$_GET['OFO_KEY'],false);
	    $SQL .= ' ORDER BY OFF_BEREICH, OFF_GRUPPE, OFF_SORTIERUNG';

	    // Formular-ID
	    $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['OFO_KEY'],false);
        $Form->Erstelle_HiddenFeld('OFO_KEY', $AWIS_KEY1);

	    $rsOFF = $DB->RecordSetOeffnen($SQL);
        $LetzterBereich ='';
        $LetzteGruppe='';
        $FormularKennung = $rsOFF->FeldInhalt('OFO_KENNUNG');
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($rsOFF->FeldInhalt('OFO_BEZEICHNUNG'), 800,'Ueberschrift');
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->SchreibeHTMLCode($rsOFF->FeldInhalt('OFO_HINWEISE'));
        $Form->ZeileEnde();

        $Script = '';
		while(!$rsOFF->EOF())
        {
	        $EditModus = ($rsOFF->FeldInhalt('OFF_SCHREIBGESCHUETZT')==1?false:true);

			if($LetzterBereich!=$rsOFF->FeldInhalt('OFF_BEREICH') AND $rsOFF->FeldInhalt('OFF_SICHTBAR')>=1)
            {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($rsOFF->FeldInhalt('OFF_BEREICH'), 800,'Ueberschrift');
                $Form->ZeileEnde();
                $LetzterBereich=$rsOFF->FeldInhalt('OFF_BEREICH');
            }

	        if($LetzteGruppe!=$rsOFF->FeldInhalt('OFF_GRUPPE'))
            {
                if($LetzteGruppe!='')
                {
                    $Form->ZeileEnde();
                }
                $Form->ZeileStart('padding-bottom:20px;border:0px','divOFF_GRUPPE_'.$rsOFF->FeldInhalt('OFF_GRUPPE'));
                $LetzteGruppe=$rsOFF->FeldInhalt('OFF_GRUPPE');
            }

            if($rsOFF->FeldInhalt('OFF_SICHTBAR')>=1)
            {
                // Standardwert ermitteln 
                // Bei =xxx wird eine PHP Funktion verwendet
                $Standardwert = $rsOFF->FeldInhalt('OFF_STANDARDWERT');
                if(substr($Standardwert,0,1)=='=')
                {
                    $STW='';
                    eval('$STW='.substr($Standardwert,1));
                    $Standardwert = $STW;
                }
                
                $Style='';
                if($rsOFF->FeldInhalt('OFF_SICHTBAR')==2)       // Feld ist beim Start unsichtbar
                {
                    $Style = 'visibility:hidden;';
                    
                    // Div Bereich ausblenden
                    echo '<script type="text/javascript">';
                    echo '$( document ).ready(function() {';
                    echo '$("#divOTF_'.$rsOFF->FeldInhalt('OTF_KEY').'").hide();';
                    echo '});';
                    echo '</script>';
                }
                if($rsOFF->FeldInhalt('OFF_NEUEZEILE')){
                    $Form->ZeileStart('','divOTF_'.$rsOFF->FeldInhalt('OTF_KEY'));
                }
                $Form->Erstelle_TextLabel($rsOFF->FeldInhalt('OTF_BEZEICHNUNG').($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'<font color=#FF0000>*</font>':'').':', 300,$Style,$rsOFF->FeldInhalt('OFF_LINK'),$rsOFF->FeldInhalt('OFF_TOOLTIPP'),'lblOTF_'.$rsOFF->FeldInhalt('OTF_KEY'));

                
                $InputFeldName = 'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY');
                $InputFeldListe = array();
                $InputFeldListe[] = 'txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY');
                $CheckBoxFelder = array();

                // Pflichtfeld
                $Pflicht = $rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'!':'';
                
    			// Eingabefeld erzeugen
    			if($rsOFF->FeldInhalt('OTF_DATENQUELLE')!='')
    			{
    				switch(substr($rsOFF->FeldInhalt('OTF_DATENQUELLE'),0,3))
    				{
    					case 'TXT':
    						$Felder = explode(':',$rsOFF->FeldInhalt('OTF_DATENQUELLE'));
    						$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
    						$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
    						//Selectfeld Angefordert f�r*:
    						$Form->Erstelle_SelectFeld($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,'',($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']),$Standardwert,'','',$Daten, '', '' , array() , '' , 'AWIS', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));

    						$Script .= '<script>$( document ).ready(function() {$("#' . 'txtOTF_WERT_' . $rsOFF->FeldInhalt('OTF_KEY') . '").trigger(\'change\');});</script>';
    						break;
    					case 'CHK':		// Checkboxen aus Textkonserven
    						$Felder = explode(':',$rsOFF->FeldInhalt('OTF_DATENQUELLE'));
    						$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])),$AWISBenutzer->BenutzerSprache());
    						$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
    						$i=0;
    						$InputFeldListe = array();
    						$Form->ZeileEnde();
    						
    						foreach($Daten AS $Eintraege)
    						{
    							$Wert = explode('~',$Eintraege);
    							$Form->ZeileStart('','divOTF_'.$rsOFF->FeldInhalt('OTF_KEY').'_'.($i));
                				$Form->Erstelle_TextLabel('', 50,$Style);
                				$Form->Erstelle_TextLabel($Wert[1], 250,$Style);
                				$Form->Erstelle_Checkbox('OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY').'_'.($i), (isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY').'_'.($i)])?'on':'off'), $rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,$Wert[0],$Style,'','','Gruppe'.$rsOFF->FeldInhalt('OTF_KEY'), '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
                				
                				$InputFeldListe[$i]='txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY').'_'.($i);
    							
                				if($rsOFF->FeldInhalt('OFF_SICHTBAR')==2)       // Feld ist beim Start unsichtbar
                				{
                				    $Style = 'visibility:hidden;';
                				
                				    // Div Bereich ausblenden
                				    echo '<script type="text/javascript">';
                				    echo '$( document ).ready(function() {';
                				    echo '$("#divOTF_'.$rsOFF->FeldInhalt('OTF_KEY').'_'.($i).'").hide();';
                				    echo '});';
                				    echo '</script>';
                				    
                				    $CheckBoxFelder[] = '#divOTF_'.$rsOFF->FeldInhalt('OTF_KEY').'_'.($i);
                				}

                				$i++;
                				$Form->ZeileEnde(); 
    						}
    						$Form->ZeileStart();
    						//$Form->Erstelle_SelectFeld('OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),'',$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,'',($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']),'','','',$Daten);
    						break;
    					case 'FLT':
    						$Felder = explode(':',$rsOFF->FeldInhalt('OTF_DATENQUELLE'));
    						$Form->Erstelle_TextFeld('*OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),'',10,100,$EditModus,'','background-color:#22FF22', '', '', '', '', '', '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    						if($AWISCursorPosition=='' AND $EditModus)
    						{
    							$AWISCursorPosition='sucOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY');
    						}
    						$InputFeldName = 'sucOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY');
    						
                            $Form->Erstelle_SelectFeld($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,'*F*'.$Felder[1].';txt'.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY').';'.$Felder[2].'=0','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '', '', '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    					    break;
    					case 'LST':        // Listenfeld mit Filterfeld
    						$Funktion = explode(':',$rsOFF->FeldInhalt('OTF_DATENQUELLE'));
    						$Funktion = explode('#~#',$Funktion[1]);
    						$Erg = $TicketFunktionen->Funktion($Funktion[0], isset($Funktion[1])?$Funktion[1]:'');
    						$Eintraege=0;
    						$Daten = array();
    						foreach($Erg as $Eintrag)
    						{
    						    $DatenListe = '';
    						    foreach($Eintrag AS $FeldName=>$FeldWert)
    						    {
    						        if($FeldName=='id')
    						        {
    						            $Daten[$Eintraege]=$Eintrag['id']=$FeldWert;
    						        }
    						        else
    						        {
    						            $DatenListe .= ', '.$FeldWert;
    						        }
    						    }
    						    $Daten[$Eintraege++] .= '~'.substr($DatenListe,2);
    						}

    						$Form->Erstelle_SelectFeld($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,'',($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']),'','','',$Daten, '', '', '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    						break;
    					case 'SQL':
    						$Felder = explode(':',$rsOFF->FeldInhalt('OTF_DATENQUELLE'));
    						// Dynamische Felder ersetzen
    						$SQL = str_replace('$OFO_KEY$',$AWIS_KEY1,$Felder[1]);
                            $SQL = str_replace('~~','::',$SQL);

    						$Form->Erstelle_SelectFeld($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,$SQL,($rsOFF->FeldInhalt('OFF_PFLICHTFELD')==1?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']), '', '', '', array(), '', '', array(), '', 'AWIS', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    						break;
    					case 'MEM':        // Memofeld
    						$Form->Erstelle_Textarea($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_BREITE'),$rsOFF->FeldInhalt('OTF_ZEICHEN'),3,$EditModus, '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    						break;
    					case 'FIL':
    					    $Form->Erstelle_DateiUpload('OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),600,30);
    						break;
    					case 'FKT':
    						$Funktion = explode(':',$rsOFF->FeldInhalt('OTF_DATENQUELLE'));
    						$Funktion = explode('#~#',$Funktion[1]);
    						$Erg = $TicketFunktionen->Funktion($Funktion[0], isset($Funktion[1])?$Funktion[1]:'');
                            if($EditModus)
                            {
                                $Form->Erstelle_TextFeld($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),$Erg,$rsOFF->FeldInhalt('OTF_ZEICHEN'),$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,'','','',$rsOFF->FeldInhalt('OTF_FORMAT'), '', '', '', '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
                            }
                            else
                            {
                                $Form->SchreibeHTMLCode($Erg);    // Einfach malen, aber ein INPUT Feld erzeugen zum speichern
                                $Form->Erstelle_HiddenFeld('OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),$Erg);
                            }

    						break;
    					case 'LNK':
    							// Erzeugt einen Link mit einem Bild oder einem Text
    						$Felder = explode(':',$rsOFF->FeldInhalt('OTF_DATENQUELLE'));

    						if(substr($Felder[1],0,4)=='ICO~')
    						{
    							$Datenquelle = explode('~',$Felder[1]);
    							$Bild = $Datenquelle[1];
								$Link = $Datenquelle[2];
								$Form->Erstelle_Bild('href','*LINK',$Link,$Bild);
    						}
    						elseif(substr($Felder[1],0,4)=='TXT~')
    						{
    							$Text = explode('~',$Felder[1]);
    							$Form->Erstelle_TextFeld('*LINK',$Text[1],0,$rsOFF->FeldInhalt('OTF_BREITE'),'','','',$Link,'T', '', '', '', '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    						}
    						else
    						{
    							$Text = explode('~',$Felder[1]);
    							$Text = $Form->LadeTextBaustein($Text[0],$Text[1]);
    							$Form->Erstelle_TextFeld('*LINK',$Text,0,$rsOFF->FeldInhalt('OTF_BREITE'),'','','',$Link,'T', '', '', '', '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    						}
    						break;
    					default:
    					    
    					    $Standardwert = $rsOFF->FeldInhalt('OTF_DATENQUELLE');
    						$Form->Erstelle_TextFeld($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_ZEICHEN'),$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,'','','',$rsOFF->FeldInhalt('OTF_FORMAT'),'','',$Standardwert, '', '', '', '', $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    						break;
    				}
    			}
    			else
    			{
    				if(strpos($rsOFF->FeldInhalt('OTF_ZEICHEN'),',')===false)
    				{

    				    $Pattern = '';
    				    $TTT = '';
    				    if($rsOFF->FeldInhalt('OTF_OTFELD')=='KostenstelleATU'){

                            $Pattern = 'pattern="[0-9]{4,12}"';
                            $TTT = $AWISSprachKonserven['OFO']['OFO_TTT_FORMAT_KOSTENSTELLE'];
                        }

                        //Felder: Vorname, Nachname, Kostenstelle
    					$Form->Erstelle_TextFeld($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_ZEICHEN'),$rsOFF->FeldInhalt('OTF_BREITE'),$EditModus,$Style,'','',$rsOFF->FeldInhalt('OTF_FORMAT'),'',$TTT,$Standardwert,0,'','',$Pattern, $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    				}
    				else
    				{
    					$Breiten = explode(',',$rsOFF->FeldInhalt('OTF_ZEICHEN'));
    					$Spalten = $Breiten[0];
    					$Zeilen = $Breiten[1];
    						
    					$Form->Erstelle_Textarea($Pflicht.'OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),(isset($_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')])?$_POST['txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY')]:''),$rsOFF->FeldInhalt('OTF_BREITE'),$Spalten,$Zeilen,$EditModus,$Style,'','',$rsOFF->FeldInhalt('OTF_FORMAT'), $rsOFF->FeldInhalt('OTF_FELD_ZUSATZKLASSE'));
    				}
    			}
    			
    			if($AWISCursorPosition=='' AND $EditModus)
    			{
    				$AWISCursorPosition='txtOTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY');
    			}
    			
    			//*******************************************************
    			// Dynamische Pflichtfelder
    			//*******************************************************
    			
    			if($rsOFF->FeldInhalt('OFF_PFLICHTFELDER')!='')
    			{
    			    $FeldTyp = substr($rsOFF->FeldInhalt('OTF_DATENQUELLE'),0,3);
    			    
    			    foreach($InputFeldListe AS $InputFeldID=>$InputFeld)
    			    {
    			        echo '<!--IFELD:'.$InputFeld.'-->'.PHP_EOL;
    			        $InputFeldName = $InputFeld;
    			        $Pflichtfelder = $rsOFF->FeldInhalt('OFF_PFLICHTFELDER');
    			        
        			    $Felder = explode(',',$Pflichtfelder);

        			    echo '<script type="text/javascript">'.PHP_EOL;
        			    
        			    echo '$("#'.$InputFeldName.'").change(function(){'.PHP_EOL;
        			    echo 'var WertGesetzt;'.PHP_EOL;
        			    
        			    //echo 'alert($("#'.$InputFeldName.'").prop("type"));'.PHP_EOL;
        			    
        			    foreach($Felder AS $Feld)
        			    {
        			        echo 'WertGesetzt=false;'.PHP_EOL;
        			        $FokusGesetzt = false;
        			        	
        			        $Modus = 0;
        			        $i=0;
        			        while(isset($Feld[$i]))
       			            {
                                switch($Feld[$i])
                                {
                                    case '!':       // Pflichtfeld
                                        $Modus |= 1;
                                        break;
                                    case '+':       // Sichtbar machen
                                        $Modus |= 2;
                                        break;
                                    case '-':       // Unsichtbar machen
                                        $Modus |= 4;
                                        break;
                                    default:
                                        break 2;
                                }
       			                $i++;
       			            }
       			            $Feld = substr($Feld,$i,999);

       			            if($Modus>0)      // Gibt es eine Aktion?
       			            {
       			                switch($FeldTyp)
       			                {
       			                    case 'SQL':   // SelectFelder
       			                    case 'TXT':
       			                    case '':
       			                        if(strpos($Feld,'-')!==false)         // Nur, wenn ein bestimmter Wert enthalten ist?
       			                        {
											$FeldTeile = explode('-',$Feld);
											$FeldTeileWerte = explode('~',$FeldTeile[1]);
											foreach($FeldTeileWerte AS $FeldTeileWert)
											{
												echo '/* '.serialize($FeldTeile).'*/'.PHP_EOL;
												echo 'if($("#'.$InputFeldName.'").val()=='.$FeldTeileWert.')'.PHP_EOL;
												echo '{'.PHP_EOL;
												echo '    WertGesetzt=true;'.PHP_EOL;
												echo '}'.PHP_EOL;
											}
											$Feld = $FeldTeile[0];
       			                        }
       			                        break;
       			                    case 'MEM':   // Memofelder
   			                            echo 'if($("#'.$InputFeldName.'").val()!="")'.PHP_EOL;
   			                            echo '{'.PHP_EOL;
   			                            echo '    WertGesetzt=true;'.PHP_EOL;
   			                            echo '}'.PHP_EOL;
       			                        break;
       			                    case 'CHK':   // Check-Boxen
                                        $Bed='';
               			                if(strpos($Feld,'-')!==false)
               			                {
               			                    $FeldTeile = explode('-',$Feld);
               			                    
                   			                foreach($InputFeldListe AS $ChkFeldID=>$ChkFeld)
                   			                {
                   			                    if($ChkFeldID==$FeldTeile[1] OR $FeldTeile[1]=='*')
                   			                    {
                   			                        if(in_array($FeldTyp , array('TXT','SQL')))
                   			                        {
                   			                            $Bed .= ' || $("#'.$ChkFeld.'").val()=='.$FeldTeile[1].'';
                   			                        }
                   			                        else 
                   			                        {
                                                        $Bed .= ' || $("#'.$ChkFeld.'").prop("checked")==true';
                   			                        }
                   			                    }
                   			                }
                   			                
                   			                $Feld = $FeldTeile[0];
               			                }
               			                else 
               			                {
               			                    foreach($InputFeldListe AS $ChkFeldID=>$ChkFeld)
               			                    {
           			                            $Bed .= ' || $("#'.$ChkFeld.'").prop("checked")==true';
               			                    }
               			                }       			                
               			                echo '       if('.PHP_EOL;
               			                 
               			                echo substr($Bed,3).PHP_EOL;
               			                 
               			                echo '         )'.PHP_EOL;
               			                echo '       {'.PHP_EOL;
               			                echo '           WertGesetzt=true;'.PHP_EOL;
               			                echo '       };'.PHP_EOL;
               			                
       			                    default:
       			                        break;
       			                }
       			                
       			                echo 'if(WertGesetzt)'.PHP_EOL;
       			                echo ' {'.PHP_EOL;
       			                if(($Modus & 1)==1)
       			                {
       			                    echo '         $("#txtOTF_WERT_'.$Feld.'").attr("required", true);'.PHP_EOL;
       			                    echo '         $("#txtOTF_WERT_'.$Feld.'").css({"border-color": "red"});'.PHP_EOL;
       			                }
       			                if(($Modus & 2)==2)
       			                {
       			                    echo '         $("#txtOTF_WERT_'.$Feld.'").css({"visibility": "visible"});'.PHP_EOL;
       			                    echo '         $("#lblOTF_'.$Feld.'").css({"visibility": "visible"});'.PHP_EOL;
       			                    echo '         $("#divOTF_'.$Feld.'").show();'.PHP_EOL;
       			                    
       			                    foreach($CheckBoxFelder AS $CheckBoxFeld)
       			                    {
       			                        echo '         $("#'.$CheckBoxFeld.'").show();'.PHP_EOL;
       			                    }
       			                    
       			                    if(!$FokusGesetzt)
       			                    {
        			                    $FokusGesetzt = true;
                                        echo '              $("#txtOTF_WERT_'.$Feld.'").focus();'.PHP_EOL;
       			                    }
       			                }
       			                echo ' }'.PHP_EOL;
       			                echo ' else'.PHP_EOL;
       			                echo ' {'.PHP_EOL;
       			                if(($Modus & 1)==1)
       			                {
       			                    echo '         $("#txtOTF_WERT_'.$Feld.'").attr("required", false);'.PHP_EOL;
       			                    echo '         $("#txtOTF_WERT_'.$Feld.'").css({"border-color": "gray"});'.PHP_EOL;
       			                }
       			                if(($Modus & 2)==2)
       			                {
       			                    echo '         $("#txtOTF_WERT_'.$Feld.'").css({"visibility": "hidden"});'.PHP_EOL;
       			                    echo '         $("#txtOTF_WERT_'.$Feld.'").val("");'.PHP_EOL;
       			                    echo '         $("#lblOTF_'.$Feld.'").css({"visibility": "hidden"});'.PHP_EOL;
       			                    echo '         $("#divOTF_'.$Feld.'").hide();';
       			                    
       			                }
       			                echo ' }'.PHP_EOL;
       			            }
        			    }
        			    echo '});'.PHP_EOL;
        			    echo '</script>'.PHP_EOL;
        			     
    			    }
    			}

                $rsOFF->DSWeiter();
    			if($rsOFF->FeldInhalt('OFF_NEUEZEILE')){
                    $Form->ZeileEnde();
                }
                $rsOFF->DSZurueck();


            }
            else         // unsichtbare Felder
            {
				$Form->Erstelle_HiddenFeld('OTF_WERT_'.$rsOFF->FeldInhalt('OTF_KEY'),$rsOFF->FeldInhalt('OFF_STANDARDWERT'));
            }
            $rsOFF->DSWeiter();
        }
        //Script zum triggern der Change-Events ausgeben
        $Form->SchreibeHTMLCode($Script);

		// Detailfelder einblenden
        if(file_exists('/daten/web/tickets/off_'.$FormularKennung.'.php'))
        {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel('3 - Details', 800,'Ueberschrift');
            $Form->ZeileEnde();
            
            include '/daten/web/tickets/off_'.$FormularKennung.'.php';
        }
	}
	elseif($rsOTT->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsOTT->AnzahlDatensaetze()>1 AND !isset($_GET['OTT_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './tickets_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=OTT_BETREFF'.((isset($_GET['Sort']) AND ($_GET['Sort']=='OTT_BETREFF'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OTT']['OTT_BETREFF'],590,'',$Link);
		$Link = './tickets_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=OTT_TICKETNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='OTT_TICKETNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OTT']['OTT_TICKETNR'],130,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsOTT->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './tickets_Main.php?cmdAktion=Details&OTT_KEY=0'.$rsOTT->FeldInhalt('OTT_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('#OTT_BETREFF',$rsOTT->FeldInhalt('OTT_BETREFF'),0,590,false,($DS%2),'',$Link);
			$Form->Erstelle_ListenFeld('#OTT_TICKETNR',$rsOTT->FeldInhalt('OTT_TICKETNR'),0,130,false,($DS%2));

			$Form->ZeileEnde();

			$rsOTT->DSWeiter();
			$DS++;
		}

		$Link = './tickets_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsOTT->FeldInhalt('OTT_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_OTTickets',serialize($Param));

		$Form->Erstelle_HiddenFeld('OTT_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./tickets_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsOTT->FeldInhalt('OTT_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsOTT->FeldInhalt('OTT_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht17000&2)!=0);
        $EditRecht=false;        // Vorerst mal nur zum lesen!

        $OTObj = new awisOT($AWISBenutzer);
        $TicketDaten = $OTObj->ObjektListe($rsOTT->FeldInhalt('OFO_OTPFAD'), '#'.$rsOTT->FeldInhalt('OTT_TICKETID'), explode(',',$rsOTT->FeldInhalt('OFO_ANZEIGEFELDER')),1);
        
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['OTT']['OTT_BETREFF'].':',200,'','');
		$Form->Erstelle_TextFeld('OTT_BETREFF',$rsOTT->FeldInhalt('OTT_BETREFF'),60,600,$EditRecht);
		$AWISCursorPosition = 'txtOTT_BETREFF';
		$Form->ZeileEnde();

		$Form->Trennzeile();

		$OTObj = new awisOT($AWISBenutzer);
		$TicketDaten = $OTObj->ObjektListe($rsOTT->FeldInhalt('OFO_OTPFAD'), '#'.$rsOTT->FeldInhalt('OTT_TICKETID'), explode(',',$rsOTT->FeldInhalt('OFO_ANZEIGEFELDER')),1);

		foreach($TicketDaten AS $Ticketfelder)
		{
		    foreach($Ticketfelder AS $Ticketfeld=>$Feldinhalt)
		    {
		    	$Text = $Form->LadeTextBaustein('OTT', 'Feld_'.$Ticketfeld,$AWISBenutzer->BenutzerSprache());
		    	switch($Ticketfeld)
		    	{
		    		case 'id':			// Interne ID, nicht darstellen
		    			break;
		    		case 'Mitarbeiter':	// Objekt-ID -> Suchen in Personen
		    			$SQL = 'SELECT OTP_BEZEICHNUNG';
		    			$SQL .= ' FROM OTPersonen';
		    			$SQL .= ' WHERE OTP_ID = :var_N0_OTP_ID';
		    			$BindeVariablen = array();
		    			$BindeVariablen['var_N0_OTP_ID']=$Feldinhalt;
		    			$rsOTP = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
		    			
		    			$Form->ZeileStart();
		    			$Form->Erstelle_TextLabel($Text.':',200,'','');
		    			$Form->Erstelle_TextFeld('*'.$Ticketfeld,$rsOTP->FeldInhalt('OTP_BEZEICHNUNG'),60,600,false);
		    			$Form->ZeileEnde();
		    			 
		    			break;
		    		case 'CreationDate':
		    		case 'LastChange':
		        		$Form->ZeileStart();
		        		$Form->Erstelle_TextLabel($Text.':',200,'','');
		        		$Form->Erstelle_TextFeld('*'.$Ticketfeld,$Feldinhalt,60,600,false);
		        		$Form->ZeileEnde();
		    			break;
		    		case 'Number':			// Ticketnummer (immer aus A-)
		        		$Form->ZeileStart();
		        		$Form->Erstelle_TextLabel($Text.':',200,'','');
		        		$Form->Erstelle_TextFeld('*'.$Ticketfeld,'A-'.str_pad($Feldinhalt,6,'0',STR_PAD_LEFT),60,600,false);
		        		$Form->ZeileEnde();
		    			break;
	    			default:
		        		$Form->ZeileStart();
		        		$Form->Erstelle_TextLabel($Text.':',200,'','');
		        		$Form->Erstelle_TextFeld('*'.$Ticketfeld,$Feldinhalt,60,600,false);
		        		$Form->ZeileEnde();
		    	}
		    }
		}

		$Form->Formular_Ende();
	}

	//awis_Debug(1, $Param, $Bedingung, $rsOTT, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht17000&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if((($Recht17000&4) == 4) and ($AWIS_KEY1 == ''))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N','',27,27,'',true);
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110231158");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110231157");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND OTT_KEY = '.$DB->WertSetzen('OTT', 'N0', $AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['OTT_TICKETID']) AND $Param['OTT_TICKETID']!='')
	{
		$Bedingung .= ' AND (OTT_TICKETID ' . $DB->LIKEoderIST($Param['OTT_TICKETID'],awisDatenbank::AWIS_LIKE_UPPER,'OTT') . ')';
	}

	if(isset($Param['OTT_TICKETNR']) AND $Param['OTT_TICKETNR']!='')
	{
		$Bedingung .= ' AND (OTT_TICKETNR ' . $DB->LIKEoderIST($Param['OTT_TICKETNR'],awisDatenbank::AWIS_LIKE_UPPER,'OTT') . ')';
	}

	if(isset($Param['OTT_BETREFF']) AND $Param['OTT_BETREFF']!='')
	{
		$Bedingung .= ' AND (UPPER(OTT_BETREFF) ' . $DB->LIKEoderIST($Param['OTT_BETREFF'],awisDatenbank::AWIS_LIKE_UPPER,'OTT') . ')';
	}

	if(isset($Param['OTT_OFO_KEY']) AND $Param['OTT_OFO_KEY']!='')
	{
		$Bedingung .= ' AND (OTT_OFO_KEY = ' . $DB->WertSetzen('OTT', 'N0', $Param['OTT_OFO_KEY']). ')';
	}

	return $Bedingung;
}




function USER()
{
    global $AWISBenutzer;
    return $AWISBenutzer->BenutzerName();
}


?>