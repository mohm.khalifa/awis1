<?php

global $VBA;

try
{
    if($VBA->Recht9800==0)
    {
        echo "<span class=HinweisText>".$VBA->AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
        echo "<br><br><input type=image title='".$VBA->AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }


    //**********************************************************
    // Export der aktuellen Daten
    //**********************************************************
    ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
    ini_set('max_execution_time', 600);
    require_once('PHPExcel.php');




    $ExportFormat = $VBA->AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);


    @ob_end_clean();
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Expires: 01 01 2030");
    header('Pragma: public');
    header('Cache-Control: max-age=0');


    switch ($ExportFormat)
    {
        case 1:                 // Excel 5.0
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Auswertung_Versvermittlungen.xls"');
            break;
        case 2:                 // Excel 2007
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="Auswertung_Versvermittlungen.xlsx"');
            break;
    }

    $XLSXObj = new PHPExcel();
    $XLSXObj->getProperties()->setCreator(utf8_encode($VBA->AWISBenutzer->BenutzerName()));
    $XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($VBA->AWISBenutzer->BenutzerName()));
    $XLSXObj->getProperties()->setTitle(utf8_encode('Auswertung_Versvermittlungen'));
    $XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
    $XLSXObj->getProperties()->setDescription(utf8_encode('Auswertung_Versvermittlungen'));

    $XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');

    if (isset($VBA->Param['ORDER']) AND $VBA->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $VBA->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY VBA_ERSTELLDATUM DESC,VBA_KEY DESC';
        $VBA->Param['ORDER'] = ' VBA_ERSTELLDATUM DESC,VBA_KEY DESC ';
    }


    //********************************************************
    // SQL Vorbereiten: Bedingung
    //********************************************************
    $Bedingung = $VBA->BedingungErstellen();

    //********************************************************
    // SQL der Seite
    //********************************************************

    $SQL = ' SELECT';
    $SQL .= ' VBA.VBA_KEY,';
    $SQL .= ' VBA.VBA_DATEINAME,';
    $SQL .= ' VBA.VBA_NACHRICHTENTYP,';
    $SQL .= ' VBA.VBA_ORDNUNGSNR,';
    $SQL .= ' VBA.VBA_ERSTELLDATUM,';
    $SQL .= ' VBA.VBA_VUNUMMER,';
    $SQL .= ' VBA.VBA_STATUS,';
    $SQL .= ' VBA.VBA_AX_VORGANGNR,';
    $SQL .= ' VBA.VBA_VSNR,';
    $SQL .= ' VBA.VBA_SNR,';
    $SQL .= ' VBA.VBA_AUFTRAGSBESCHREIBUNG,';
    $SQL .= ' VBA.VBA_AKTENZEICHEN,';
    $SQL .= ' VBA.VBA_SCHADENDATUM,';
    $SQL .= ' VBA.VBA_SCHADENTYP,';
    $SQL .= ' VBA.VBA_VORSTABZUGKZ,';
    $SQL .= ' VBA.VBA_VNANREDE,';
    $SQL .= ' VBA.VBA_VNNAME1,';
    $SQL .= ' VBA.VBA_VNNAME2,';
    $SQL .= ' VBA.VBA_VNNAME3,';
    $SQL .= ' VBA.VBA_VNTITEL,';
    $SQL .= ' VBA.VBA_VNLAENDERKZ,';
    $SQL .= ' VBA.VBA_VNPLZ,';
    $SQL .= ' VBA.VBA_VNORT,';
    $SQL .= ' VBA.VBA_VNSTRASSE,';
    $SQL .= ' VBA.VBA_VNKOMM1,';
    $SQL .= ' VBA.VBA_VNKOMM2,';
    $SQL .= ' VBA.VBA_KFZKZ,';
    $SQL .= ' VBA.VBA_KFZHERSTELLER,';
    $SQL .= ' VBA.VBA_KFZMODELL,';
    $SQL .= ' VBA.VBA_KFZERSTZULASSUNG,';
    $SQL .= ' VBA.VBA_KFZHSN,';
    $SQL .= ' VBA.VBA_KFZTSN,';
    $SQL .= ' VBA.VBA_KFZVIN,';
    $SQL .= ' VBA.VBA_USER,';
    $SQL .= ' VBA.VBA_USERDAT,';
    $SQL .= ' VBA.VBA_KEEPALIVE,';
    $SQL .= ' VBA.VBA_KEEPALIVE_USER,';
    $SQL .= ' VBA.VBA_BEMERKUNG,';
    $SQL .= ' VBA.VBA_BEMERKUNG_USER,';
    $SQL .= ' VBA.VBA_BEMERKUNG_USERDAT,';
    $SQL .= ' CASE WHEN (VBA_KEEPALIVE is null or (VBA_KEEPALIVE <= sysdate - 19*(1/24/60/60))) ';
    $SQL .= ' then';
    $SQL .= ' 0 else 1 end as Sperre,';
    $SQL .= ' VVE.VVE_VERSNR,';
    $SQL .= ' VVE.VVE_BEZEICHNUNG,';
    $SQL .= ' VDV.VDV_ERFASS_MANUELL,';
    $SQL .= ' row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM VERSBEAUFTRAGUNGEN VBA';
    $SQL .= ' INNER JOIN VERSDIREKTVERMITTLUNGREF VDV';
    $SQL .= ' ON VDV.VDV_VU_NUMMER = VBA.VBA_VUNUMMER';
    $SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
    $SQL .= ' ON VVE.VVE_VERSNR = VDV.VDV_VERS_NR';

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $rsVBA = $VBA->DB->RecordsetOeffnen($SQL, $VBA->DB->Bindevariablen('VBA', true));


    $XLSXObj->setActiveSheetIndex(0);
    $XLSXObj->getActiveSheet()->setTitle(utf8_encode('Vermittlungen'));


    $SpaltenNr = 0;
    $ZeilenNr = 1;

    //Überschrift
    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Vermittlungen'));
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y G:i',time()));
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');

    $ZeilenNr++;
    $ZeilenNr++;
    $SpaltenNr = 0;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Versicherungsunternehmen');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'KFZ-Kennzeichen');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'VerschicherungsscheinNr');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Schadennummer');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Erstelldatum');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Zuordnung zu AX-Auftrag');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    if (($VBA->Recht9800 & 8) == 8) {
        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr, $ZeilenNr, 'Bearbeiter');
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr, $ZeilenNr)->getFont()->setBold(true);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr, $ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr, $ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
        $SpaltenNr++;
    }

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr, $ZeilenNr, 'Bemerkung');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr, $ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr, $ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr, $ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $ZeilenNr++;
    $SpaltenNr=0;

    while(!$rsVBA->EOF())
    {
        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($VBA->Form->Format('T',$rsVBA->FeldInhalt('VVE_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($VBA->Form->Format('T',$rsVBA->FeldInhalt('VBA_KFZKZ'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($VBA->Form->Format('T',$rsVBA->FeldInhalt('VBA_VSNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($VBA->Form->Format('T',$rsVBA->FeldInhalt('VBA_SNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $Datum=$VBA->Form->PruefeDatum($rsVBA->FeldInhalt('VBA_ERSTELLDATUM'),false,false,true);
        $XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr,PHPExcel_Shared_Date::PHPToExcel($Datum));
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($VBA->Form->Format('T',$rsVBA->FeldInhalt('VBA_AX_VORGANGNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        if (($VBA->Recht9800 & 8) == 8) {
            $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($VBA->Form->Format('T',$rsVBA->FeldInhalt('VBA_USER'))),PHPExcel_Cell_DataType::TYPE_STRING);
            $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        }

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($VBA->Form->Format('T',$rsVBA->FeldInhalt('VBA_BEMERKUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $ZeilenNr++;
        $SpaltenNr=0;
        $rsVBA->DSWeiter();
    }



    for($S='A';$S<='L';$S++)
    {
        $XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
    }

    $XLSXObj->setActiveSheetIndex(0);

    // Verschiedene Formate erfordern andere Objekte
    switch ($ExportFormat)
    {
        case 1:                 // Excel 5.0
            $objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
            break;
        case 2:                 // Excel 2007
            $objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
            break;
    }

    $objWriter->save('php://output');
    //$objWriter->save('/daten/web/Vorlage.xls');
    $XLSXObj->disconnectWorksheets();



}
catch(exception $ex)
{
    $Form->DebugAusgabe(1,$ex->getMessage());
}

?>
