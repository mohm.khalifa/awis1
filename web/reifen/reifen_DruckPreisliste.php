<?php
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');

try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	$TextKonserven = array();
	$TextKonserven[]=array('LIK','%');
	$TextKonserven[]=array('AST','AST_BEZEICHNUNGWW');
	$TextKonserven[]=array('Wort','EANNummer');
	$TextKonserven[]=array('Wort','Menge');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	$TextKonserven[]=array('Ausdruck','txtHinweisVertraulich');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht3600 = $AWISBenutzer->HatDasRecht(3600);
	if($Recht3600==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	//$Vorlagen = array('PDF_klein_Logo_farbig_2008.pdf');	
	$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');	

	$Param = $AWISBenutzer->ParameterLesen("Formular_Reifen_Preislisten");
	//$Param = $AWISBenutzer->awis_BenutzerParameter($con, "Formular_Reifen_Preislisten",$AWISBenutzer->BenutzerName());

	$Spalte = 5;
	$Zeile = 10;
	$Ausdruck = null;
	$Seite = '';
	$AnzZeilen=0;
	
	$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Preisliste');
	
	$Zeile=_Erzeuge_Seite_Preisliste();

	//Land zu der eingegebenen Filiale ermitteln
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id']=$Param['FIL_ID'];
	
	$SQL = 'SELECT LAN_CODE';
	$SQL.= ' FROM FILIALEN';
	$SQL.= ' INNER JOIN LAENDER ON LAN_WWSKENN = FIL_LAN_WWSKENN';
	$SQL.= ' WHERE FIL_ID = :var_N0_fil_id';
	
	$rsLAN = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
	if ($rsLAN->AnzahlDatensaetze()==0)
	{
		$Form->Hinweistext('Ung�ltige Filiale!');
		die();		
	}
	else 
	{
		$LAN_CODE = $rsLAN->FeldInhalt('LAN_CODE');
	}
	
	// Abfrage mit den Kunden, f�r die etwas gedruckt werden soll
	$SQL= 'SELECT b.* , rpr_vk, rpr_rabatt, rpr_aktion, rpr_aktiontext,';
  	$SQL.=' (SELECT COUNT(*)';
    $SQL.=' FROM ARTIKELSTAMMINFOS';
    //$SQL.=' INNER JOIN V_REIFENLISTE v on REI_AST_ATUNR = ASI_AST_ATUNR AND RPR_LAN_CODE = \'DE\'';
    $SQL.=' INNER JOIN REIFEN r on REI_AST_ATUNR = ASI_AST_ATUNR AND REI_KBPREIS = -1';
    $SQL.=' INNER JOIN REIFENPREISE rp on RPR_REI_ID = REI_ID AND RPR_LAN_CODE = \''.$LAN_CODE.'\'';    
    $SQL.=' INNER JOIN ARTIKELSTAMM A ON A.AST_ATUNR = ASI_AST_ATUNR';
    $SQL.=' INNER JOIN V_ARTIKELSTAMMWGR AA ON AA.AST_ATUNR = ASI_AST_ATUNR';
    if (isset($Param['WGR_ID']) and $Param['WGR_ID']!='')
	{
		$SQL.= ' AND AST_WGR_ID = \''.$Param['WGR_ID'].'\'';
	}    
    $SQL.=' WHERE ASI_AIT_ID=140 AND ASI_WERT = B.Sort';
    $SQL.=' ) AS ANZAHL';
	$SQL.=' FROM';
    $SQL.=' (SELECT r.*, rpr_vk, rpr_rabatt, rpr_aktion, rpr_aktiontext,';
    $SQL.=' a.*,';
    $SQL.=' (SELECT ASI_WERT FROM ARTIKELSTAMMINFOS WHERE ASI_AST_ATUNR = REI_AST_ATUNR AND ASI_AIT_ID = 140) AS Sort';
  	//$SQL.=' FROM V_REIFENLISTE v';
  	$SQL.=' FROM REIFEN r';
  	$SQL.=' INNER JOIN REIFENPREISE rp on RPR_REI_ID = REI_ID AND RPR_LAN_CODE = \''.$LAN_CODE.'\'';    
  	$SQL.=' INNER JOIN ARTIKELSTAMM A ON A.AST_ATUNR = REI_AST_ATUNR';
  	$SQL.=' INNER JOIN V_ARTIKELSTAMMWGR AA ON AA.AST_ATUNR  = REI_AST_ATUNR';
  	$SQL.=' WHERE REI_KBPREIS = -1';
  	if (isset($Param['WGR_ID']) and $Param['WGR_ID']!='')
	{
		$SQL.= ' AND AST_WGR_ID = \''.$Param['WGR_ID'].'\'';
	}	
  	$SQL.=' ) b';
	$SQL.=' ORDER BY Sort';		

	//echo $SQL;
	//die();
	
	$rsREI = $DB->RecordSetOeffnen($SQL);
	$rsREIZeilen = $rsREI->AnzahlDatensaetze();

	if($rsREIZeilen==0)
	{
		$Form->Hinweistext('Keine Daten gefunden!');
		die();
	}

	$Sort = $rsREI->FeldInhalt('SORT');	
	
	$Ausdruck->_pdf->SetFillColor(255,255,000);			
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	$Ausdruck->_pdf->cell(285,5,$rsREI->FeldInhalt('SORT'),1,0,'C',1);	
	$Zeile+=5;
	$AnzZeilen++;
	
	for($REIZeile=0;$REIZeile<$rsREIZeilen;$REIZeile++)
	{					
		if ($Sort != $rsREI->FeldInhalt('SORT'))
		{					
			if($AnzZeilen + 2 + $DB->FeldInhaltFormat('Z',$rsREI->FeldInhalt('ANZAHL')) >= 32)
			{
				$AnzZeilen=0;
				$Zeile=_Erzeuge_Seite_Preisliste();
			}
			else 
			{
				//Leerzeile			
				$Zeile+=5;	
			}			
						
			$AnzZeilen++;		
			$Ausdruck->_pdf->SetFont('Arial','B',8);		
			$Ausdruck->_pdf->SetFillColor(255,255,000);			
			$Ausdruck->_pdf->setXY($Spalte,$Zeile);
			$Ausdruck->_pdf->cell(285,5,$rsREI->FeldInhalt('SORT'),1,0,'C',1);	
			$Zeile+=5;	
			$AnzZeilen++;		
		}
		
		//if(($Ausdruck->SeitenHoehe()-20)<$Zeile)		
		if($AnzZeilen >= 32)
		{
			$AnzZeilen=0;
			$Zeile=_Erzeuge_Seite_Preisliste();
			$AnzZeilen++;		
		}

		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte,$Zeile);
		$Ausdruck->_pdf->cell(15,5,$rsREI->FeldInhalt('REI_AST_ATUNR'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','',8);			
		$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
		$Ausdruck->_pdf->cell(30,5,$rsREI->FeldInhalt('REI_HERSTELLER'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+45,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_BREITE'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+55,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_QUERSCHNITT'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+65,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_BAUART'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+75,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_INNENDURCHMESSER'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+85,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_LLKW'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+95,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_LOADINDEX'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+105,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_LOADINDEX2'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+115,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_SPI_SYMBOL'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte+125,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_LOADINDEX3'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+135,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_SPI_SYMBOL2'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte+145,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_RF'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+155,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_ZUSBEM2'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','',8);			
		$Ausdruck->_pdf->setXY($Spalte+165,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_ZUSBEM3'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','B',8);			
		$Ausdruck->_pdf->setXY($Spalte+175,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_ZUSATZBEMERKUNG'),1,0,'L',0);	
		
		$Ausdruck->_pdf->SetFont('Arial','',8);			
		$Ausdruck->_pdf->setXY($Spalte+185,$Zeile);
		$Ausdruck->_pdf->cell(50,5,$rsREI->FeldInhalt('REI_BEZEICHNUNG'),1,0,'L',0);	
		
		$Ausdruck->_pdf->setXY($Spalte+235,$Zeile);
		$Ausdruck->_pdf->cell(10,5,$rsREI->FeldInhalt('REI_K'),1,0,'L',0);	
				
		$VK = $rsREI->FeldInhalt('RPR_VK'); 
		
		if($Param['FIL_ID']!="")
		{
			//eventuelle AST_VK hernehmen??
			$BindeVariablen=array();
			$BindeVariablen['var_T_ast_atunr']=$rsREI->FeldInhalt('REI_AST_ATUNR');
			$BindeVariablen['var_N0_fil_id']=$Param['FIL_ID'];
			
			$SQL="SELECT FIB_VK FROM FILIALBESTAND WHERE FIB_AST_ATUNR=:var_T_ast_atunr AND FIB_FIL_ID=:var_N0_fil_id";
			$rsVKFil = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
			$rsVKAnz = $rsVKFil->AnzahlDatensaetze();		
			
			if ($rsVKAnz > 0)
			{
				$VK = $rsVKFil->FeldInhalt('FIB_VK');
			}		
		}				
		
		$BindeVariablen=array();
		$BindeVariablen['var_T_ast_atunr']=$rsREI->FeldInhalt('REI_AST_ATUNR');
		
		$rsSuch2 = $DB->RecordSetOeffnen("SELECT AST_SUCH2 FROM ArtikelStamm WHERE AST_ATUNR=:var_T_ast_atunr",$BindeVariablen);
		
		if ($rsSuch2->FeldInhalt('AST_SUCH2')!='')
		{
			if($rsREI->FeldInhalt('RPR_AKTION') != '0')
			{
				$Ausdruck->_pdf->SetFont('Arial','B',8);	
				$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
				$Ausdruck->_pdf->cell(10,5,'i.V.',1,0,'L',0);						
			}
			else
			{
				$Ausdruck->_pdf->SetFont('Arial','B',8);	
				$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
				$Ausdruck->_pdf->cell(10,5,'',1,0,'L',0);						
			}
		}
		else
		{
			$Ausdruck->_pdf->SetFont('Arial','B',8);	
			$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
			$Ausdruck->_pdf->cell(10,5,awisFormular::Format('N2',$VK),1,0,'L',0);		
		}
		
		$Ausdruck->_pdf->setXY($Spalte+255,$Zeile);
		$Ausdruck->_pdf->cell(10,5,($rsREI->FeldInhalt('RPR_RABATT')=='0'?'':$rsREI->FeldInhalt('RPR_RABATT')),1,0,'L',0);	
		
		
		if($rsREI->FeldInhalt('RPR_AKTIONTEXT')!='')
		{
			$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
			$Ausdruck->_pdf->cell(20,5,$rsREI->FeldInhalt('RPR_AKTIONTEXT'),1,0,'L',0);	
		}
		elseif($rsSuch2->FeldInhalt('AST_SUCH2')!='')
		{
			if ($rsREI->FeldInhalt('RPR_VK') == '0')
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,'i.V.',1,0,'L',0);	
				
			}
			elseif($rsREI->FeldInhalt('RPR_AKTION') != '0')
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,awisFormular::Format('N2',$rsREI->FeldInhalt('RPR_AKTION')),1,0,'L',0);	
			}
			else
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,'',1,0,'L',0);	
			}
		}
		else
		{
			$Aktion=0;
			//Spalte Aktion berechnen
			if ($rsREI->FeldInhalt('RPR_RABATT') != '0')
			{
				$Aktion = (100 - $rsREI->FeldInhalt('RPR_RABATT')) / 100; 
				$Aktion = str_replace(',','.',$Aktion) * str_replace(',','.',$VK);
				
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,($Aktion==0?'':awisFormular::Format('N2',$Aktion)),1,0,'L',0);	
			}
			else 
			{
				$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
				$Ausdruck->_pdf->cell(20,5,'',1,0,'L',0);	
			}		
		}
				
		$Zeile+=5;
		$AnzZeilen++;
		
		$Sort = $rsREI->FeldInhalt('SORT');
		
		$rsREI->DSWeiter();
	}	
	
	$Ausdruck->Anzeigen();	
}			
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}


function _Erzeuge_Seite_Preisliste()
{
	global $AWISSprachKonserven;
	global $Spalte;
	global $Seite;	
	global $Ausdruck;	
	global $Form;
	global $AWIS_KEY1;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Ausdruck->NeueSeite(0,1);
	$Seite++;	
	//$Ausdruck->_pdf->SetFillColor(210,210,210);
	$Ausdruck->_pdf->SetFillColor(255,165,000);
	
	$Zeile = 10;
	
	$Ausdruck->_pdf->SetFont('Arial','B',15);		
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	//$Ausdruck->_pdf->cell(15,5,$AWISSprachKonserven['LIK']['LIK_AST_ATUNR'],1,0,'L',1);				
	$Ausdruck->_pdf->cell(50,10,'Preisliste',0,0,'L',0);				
	
	$Zeile = 10;
	
	// Fu�zeile
	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(10,8,'Seite: '.$Seite,0,0,'L',0);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20),8,$AWISSprachKonserven['Ausdruck']['txtHinweisVertraulich'],0,0,'C',0);
	$Ausdruck->_pdf->SetXY(($Ausdruck->SeitenBreite()-30),$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(20,8,date('d.m.Y'),0,0,'L',0);
		
	$Zeile+=5;
	
	// Listen�berschrift
	$Ausdruck->_pdf->SetFont('Arial','B',8);		
	$Zeile=$Zeile+10;		
	$Ausdruck->_pdf->setXY($Spalte,$Zeile);
	//$Ausdruck->_pdf->cell(15,5,$AWISSprachKonserven['LIK']['LIK_AST_ATUNR'],1,0,'L',1);				
	$Ausdruck->_pdf->cell(15,5,'ATU-Nr',1,0,'L',1);
		
	$Ausdruck->_pdf->setXY($Spalte+15,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(170,5,'Reifenkennzeichen',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+185,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(50,5,'Bezeichnung',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+235,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(10,5,'K',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+245,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(10,5,'VK',1,0,'L',1);		
	
	$Ausdruck->_pdf->setXY($Spalte+255,$Zeile);	
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(10,5,'%',1,0,'L',1);	

	$Ausdruck->_pdf->setXY($Spalte+265,$Zeile);
	//$Ausdruck->_pdf->cell(90,5,$AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'],1,0,'L',1);		
	$Ausdruck->_pdf->cell(20,5,'Aktion',1,0,'L',1);		
				
	$Zeile+=5;	
	
	return $Zeile;
}

?>