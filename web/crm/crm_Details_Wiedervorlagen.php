<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('CWV','CWV_%');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3703 = awisBenutzerRecht($con,3703,$AWISBenutzer->BenutzerName());
if($Recht3703==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

awis_FORM_FormularStart();

$EditRecht=(($Recht3703&6)!=0);

$SQL = 'SELECT CRMWiedervorlagen.*, KON_NAME1 || \', \' || KON_Name2 AS KONName ';
$SQL .= ' FROM CRMWiedervorlagen ';
$SQL .= ' LEFT OUTER JOIN Kontakte ON CWV_KON_KEY = KON_KEY';
$SQL .= ' WHERE CWV_CAD_KEY=0'.$AWIS_KEY1;

$Bedingung = '';
if(isset($_GET['CWVKEY']))
{
	$Bedingung  .= ' AND CWV_KEY=0'.intval($_GET['CWVKEY']);
}
if(isset($AWIS_KEY2) AND $AWIS_KEY2>0)
{
	$Bedingung .= ' AND CWV_KEY=0'.intval($AWIS_KEY2);
}

$SQL .= $Bedingung;

if(!isset($_GET['CWVSort']))
{
	$SQL .= ' ORDER BY CWV_DATUM';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CWVSort']);
}

$rsCWV = awisOpenRecordset($con,$SQL);
$rsCWVZeilen = $awisRSZeilen;


if(!isset($_GET['CWVKEY']))					// Liste anzeigen
{

	awis_FORM_ZeileStart();

	if(($Recht3703&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&CWVKEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen';
	$Link .= '&CWVSort=CWV_DATUM'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CWV_DATUM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CWV']['CWV_DATUM'],150,'',$Link);

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen';
	$Link .= '&CWVSort=CWV_BEMERKUNG'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CWV_BEMERKUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CWV']['CWV_BEMERKUNG'],350,'',$Link);

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen';
	$Link .= '&CWVSort=CWV_KON_KEY'.((isset($_GET['CWVSort']) AND ($_GET['CWVSort']=='CWV_KON_KEY'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CWV']['CWV_KON_KEY'],250,'',$Link);

	awis_FORM_ZeileEnde();

	for($CWVZeile=0;$CWVZeile<$rsCWVZeilen;$CWVZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3703&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&CWVKEY='.$rsCWV['CWV_KEY'][$CWVZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&Del='.$rsCWV['CWV_KEY'][$CWVZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CWVZeile%2));

	awis_FORM_Erstelle_ListenFeld('CWV_DATUM',$rsCWV['CWV_DATUM'][$CWVZeile],20,150,false,($CWVZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('CWV_BEMERKUNG',$rsCWV['CWV_BEMERKUNG'][$CWVZeile],20,350,false,($CWVZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('KONNAME',$rsCWV['KONNAME'][$CWVZeile],20,250,false,($CWVZeile%2),'','','T');
	awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();

	echo '<input name=txtCWV_KEY type=hidden value=0'.(isset($rsCWV['CWV_KEY'][0])?$rsCWV['CWV_KEY'][0]:'').'>';
	$AWIS_KEY2 = (isset($rsCWV['CWV_KEY'][0])?$rsCWV['CWV_KEY'][0]:'');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&CWVListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCWV['CWV_KEY'][0])?$rsCWV['CWV_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsCWV['CWV_KEY'][0])?$rsCWV['CWV_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CWV']['CWV_KON_KEY'].'',150);
/*	$SQL = 'SELECT KON_KEY, KON_NAME1 || \', \' || KON_Name2 AS Name FROM Kontakte';
	$SQL .= ' INNER JOIN KontakteAbteilungenZuordnungen ON KON_KEY = KZA_KON_KEY';
	$SQL .= ' WHERE KZA_KAB_KEY=268';
	$SQL .= ' ORDER BY KON_NAME1 , KON_NAME2';
*/
    // Gleicher Select wie Betreuer in CAD, SK, 25.09.2012
    $SQL = "select DISTINCT * FROM (SELECT  kon_key, kon_name1 || ', ' || COALESCE(kon_name2,'') AS kontaktName";
    $SQL .= ' FROM kontakte';
    $SQL .= ' INNER JOIN benutzer B on kon_key = B.xbn_kon_key';// and xbn_status=\'A\'';
    $SQL .= ' INNER JOIN V_ACCOUNTRECHTE R on B.xbn_key = R.xbn_key';
    $SQL .= ' WHERE R.XRC_ID = 3704 AND BITAND(XBA_STUFE,16)=16 and kon_status=\'A\'';
    $SQL .= (isset($rsCAQ['CWV_KON_KEY'][0])?' OR XBN_KEY = 0'.$rsCAQ['CWV_KON_KEY'][0]:'');		// wenn er schon mal vergeben wurde, lassen
    $SQL .= ' )ORDER BY 2';

	if(isset($rsCWV['CWV_KON_KEY'][0]))
	{
		$Wert = $rsCWV['CWV_KON_KEY'][0];
	}
	else
	{
		$rsCAD = awisOpenRecordset($con, 'SELECT CAD_KON_KEY FROM CRMAdressen WHERE CAD_KEY=0'.$AWIS_KEY1);
		$Wert = $rsCAD['CAD_KON_KEY'][0];
	}
	awis_FORM_Erstelle_SelectFeld('CWV_KON_KEY',$Wert,300,$EditRecht,$con,$SQL,'','','','','','');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CWV']['CWV_BEMERKUNG'].'',150);
	awis_FORM_Erstelle_TextFeld('CWV_BEMERKUNG',(isset($rsCWV['CWV_BEMERKUNG'][0])?$rsCWV['CWV_BEMERKUNG'][0]:''),70,300,($Recht3703&6));
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CWV']['CWV_DATUM'].'',150);
	awis_FORM_Erstelle_TextFeld('CWV_DATUM',(isset($rsCWV['CWV_DATUM'][0])?$rsCWV['CWV_DATUM'][0]:''),10,120,($Recht3703&6),'','','','D');
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
}
?>