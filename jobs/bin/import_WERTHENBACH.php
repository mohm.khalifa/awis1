<?php
require_once('/daten/include/awisZukauf_WERTHENBACH.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;
    $WANummer = '';
    $Abfrage = 'Bestellung';

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--testsystem':
                            $Testsystem = true;
                            break;
                    case '--wanummer':
                            $WANummer = $argv[++$i];
                            break;
                    case '--jobid':
                            $JobId = $argv[++$i];
                            break;
                    case '--starttag':
                            $StartTag = $argv[++$i];
                            break;
                    case '--endtag':
                            $EndTag = $argv[++$i];
                            break;
                    case '--abfrage':
                            $Abfrage = $argv[++$i];
                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --testsystem                       Daten aus dem Testsystem abfragen.'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                            echo '  --wanummer  <Nr>                   WANummer fuer die Abfrage.'.PHP_EOL;
                            echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.'.PHP_EOL;
                            echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.'.PHP_EOL;
                            echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung, Lieferschein oder Rechnung.'.PHP_EOL;

                            echo PHP_EOL;
                            die();
            }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    
    

    if($JobId!=0)
    {
        $Job = new awisJobProtokoll($JobId, 'Starte WERTHENBACH - Import.');
    }

    $ZUK = new awisZukauf_WERTHENBACH($DebugLevel,$Testsystem);


    if($JobId!=0)
    {
        $Job->SchreibeLog(3,1,'Suche offene '.$Abfrage.'...');
    }

    switch($Abfrage)
    {
        case 'Bestellung':
            if($ZUK->LeseBestellungen($WANummer,'','',($WANummer!=''?'A':''))>0)
            {
                if($JobId!=0)
                {
                    $Job->SchreibeLog(3,1,'Speichere Bestellungen...');
                }
                $ZUK->SpeichereBestellungen();
            }
            break;
        case 'Lieferschein':
            if($ZUK->LeseLieferscheine($WANummer,'','',($WANummer!=''?'A':''))>0)
            {
                if($JobId!=0)
                {
                    $Job->SchreibeLog(3,1,'Speichere Lieferschein...');
                }
                $ZUK->SpeichereLieferscheine();
            }
            break;
        case 'Rechnung':
	    $Runde=0;
            while($ZUK->LeseRechnungen($WANummer,'','',($WANummer!=''?'A':''))>0)
            {
                if($JobId!=0)
                {
                    $Job->SchreibeLog(3,1,'Speichere Rechnungen...');
                }
                $ZUK->SpeichereRechnungen();


	        if(++$Runde > 20)
		{
		    break;
		}
            }
            break;
    }

    if($JobId!=0)
    {
        $Job->SchreibeLog(1,0,'Ende des Jobs.');
    }
}
catch (Exception $ex)
{
    echo 'FEHLER:'.$ex->getMessage().PHP_EOL;
    echo 'CODE:  '.$ex->getCode().PHP_EOL;
    echo 'Zeile: '.$ex->getLine().PHP_EOL;
    echo 'Datei: '.$ex->getFile().PHP_EOL;
    if($JobId!=0)
    {
        $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
    }
}
?>
