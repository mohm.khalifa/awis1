<?php
global $SEP;
require_once 'seminarplan_Emails_funktionen.inc';
try {
    $SEP = new seminarplan_Emails_funktionen();

    $RegisterSeite = (isset($_GET['Seite']) ? $_GET['Seite'] : (isset($_POST['Seite']) ? $_POST['Seite'] : 'Texte'));

    $Aktion = './seminarplan_Main.php?cmdAktion=Emails' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');

    if (isset($_GET['MVT_KEY']) and !isset($_GET['Liste'])) { //War man auf Mailtexten?
        if (isset($_POST['cmdSpeichern_x'])) { //Und wurde vlt sogar gespeichert?
            include('seminarplan_Emails_Texte_speichern.php');
        }
        $Aktion .= '&MVT_KEY=' . $_GET['MVT_KEY'];
    } elseif (isset($_GET['SEM_KEY']) and !isset($_GET['Liste'])) { //War man auf den Zuordnungen?
        if (isset($_POST['cmdSpeichern_x'])) { //Und wurde vlt sogar gespeichert?
            include('seminarplan_Emails_Zuordnung_speichern.php');
        }
        $Aktion .= '&SEM_KEY=' . $_GET['SEM_KEY'];
    } elseif (isset($_GET['DEL'])) {
        include('seminarplan_Emails_Zuordnung_loeschen.php');
    }


    $SEP->Form->SchreibeHTMLCode('<form name=frmEmails action=' . $Aktion . ' method=POST>');

    $RegDet = new awisRegister(4207);
    $RegDet->ZeichneRegister($RegisterSeite);

} catch (awisException $ex) {
    if ($SEP->Form instanceof awisFormular) {
        $SEP->Form->DebugAusgabe(1, $ex->getSQL());
        $SEP->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}

?>