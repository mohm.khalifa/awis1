<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['sucBES_FIL']) and !isset($_GET['sucBES_VUFIL']))
{
	var_dump($_GET);
	die();
}


try
{
	$DS=0;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','Datum');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	
	if(isset($_GET['sucBES_FIL']))
	{
		if(ctype_digit(urldecode(trim($_GET['sucBES_FIL']))))
		{
			$SQL  = ' Select * from( ';
			$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
			$SQL .= ' where fil_gruppe IS NOT NULL OR';
			$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
			$SQL .= ' union';
			$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
			$SQL .= ' FROM schaddev09.pseudofilialen)';
	        $SQL .= ' where fil_id =0'.urldecode(trim($_GET['sucBES_FIL']));
			
	        
			$rsFIL = $DB->RecordSetOeffnen($SQL);
		
				
			$Fehler = '';
			if($rsFIL->AnzahlDatensaetze() == 0)
			{
				
				$Fehler = 'keine g�ltige Fililale';
			}
		}
		else 
		{
			$Fehler = 'keine g�ltige Fililale';
		}
		if($_GET['sucBES_FIL'] == '')
		{
			$Fehler =  $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
		}
		
		
			
		
		
		
		/**
		 * Standort nur lesen!
		 */
		
		$Form->Erstelle_TextFeld('BES_FIL_NAME',$Fehler == '' ?$rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,499, false, '', '', '', 'T', 'L','','',50);

	}
	
	if(isset($_GET['sucBES_VUFIL']))
	{
		if(ctype_digit(urldecode(trim($_GET['sucBES_VUFIL']))))
		{
			$SQL  = ' Select * from( ';
			$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
			$SQL .= ' where fil_gruppe IS NOT NULL OR';
			$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
			$SQL .= ' union';
			$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
			$SQL .= ' FROM schaddev09.pseudofilialen)';
			$SQL .= ' where fil_id =0'.urldecode(trim($_GET['sucBES_VUFIL']));
		
		
			$rsFIL = $DB->RecordSetOeffnen($SQL);
			
				
			$Fehler = '';
			if($rsFIL->AnzahlDatensaetze() == 0)
			{
					
				$Fehler = 'keine g�ltige Fililale';
			}
		}
		else 
		{
			
			$Fehler = 'keine g�ltige Fililale';
		}
		
		if(ctype_digit(urldecode(trim($_GET['sucBES_VUFIL']))))
		{
			$SQL2  = 'select * from v_filialebenenrollen_aktuell';
			$SQL2  .= ' inner join kontakte on xx1_kon_key = kon_key';
			$SQL2  .= ' where xx1_fil_id=0'.urldecode(trim($_GET['sucBES_VUFIL']));
			$SQL2  .= ' and xx1_fer_key = 25';
			
			$rsGBL = $DB->RecordSetOeffnen($SQL2);
		}
		if($_GET['sucBES_VUFIL'] == '')
		{
			$Fehler =  $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
		}
	
	
			
	
	
	
		/**
		 * Standort nur lesen!
		 */
	
		if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
		{
			$Top = '16pt';
			$Left = '-187pt';
		}
		else
		{
			$Top = '17pt';
			$Left = '-188pt';
		}
		
		$Form->Erstelle_TextFeld('BES_VUFIL_NAME',$Fehler == '' ?$rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,250, false, '', '', '', 'T', 'L','','',50);
		$Form->Erstelle_TextFeld('BES_GBL',$Fehler == '' ? $rsGBL->FeldInhalt('KON_NAME1'):$Fehler, 40,250, false,'position:relative;left:'.$Left.'; top:'.$Top.';', '', '', 'T', 'L','','',50);
	}
	

}
catch (Exception $ex)
{
	$Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>