<?php
require_once 'awisDatenbank.inc';
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 18.09.2016
 * Time: 11:59
 */


class awisSlackProtokoll
{
    const LOG_ZIEL_DATENBANK = 1;
    const LOG_ZIEL_DATEI = 2;

    private $_LogLevel = 0;
    private $_LogDatei = '/var/logs/awis/awisSlack.log';
    private $_LogFilePointer = false;
    private $_FehlerCount = 0;
    private $_AWISBenutzer;

    private $_XSP_NACHRICHT;
    private $_XSP_EMPFAENGER;
    private $_XSP_ABSENDER;
    private $_XSP_XSB_KEY;
    private $_XSP_VERSANDSTATUS;
    private $_XSP_VERSANDPROTOKOLL;

    function __construct(awisBenutzer $AWISBenutzer)
    {
        $this->_AWISBenutzer = &$AWISBenutzer;
    }

    function __destruct()
    {
        if($this->_LogFilePointer){
            fclose($this->_LogFilePointer);
        }
    }

    public function LogEintrag($Logeintrag, $MinLevel,$Ziel){
        if($this->_LogLevel>=$MinLevel){
            $Logeintrag = date('d.m.Y H:i:s') . $Logeintrag;

            switch($Ziel){
                case $this::LOG_ZIEL_DATENBANK:
                    break;
                case $this::LOG_ZIEL_DATEI:
                    $Logeintrag .= PHP_EOL;
                    $this->schreibeEintragInDatei($Logeintrag);
                    break;
            }
        }
    }

    private function schreibeEintragInDatei($Logeintrag){
        if(fwrite($this->_LogFilePointer,$Logeintrag)===false and $this->_FehlerCount<=50){
            $this->_FehlerCount++;
            fclose($this->_LogFilePointer);
            $this->_LogFilePointer = fopen($this->_LogDatei,'a');
            $this->schreibeEintragInDatei($Logeintrag);
        }elseif($this->_FehlerCount>=50){
            echo 'Konnte nach 50 Versuchen nicht in die Logdatei schreiben.. Gebe auf..'.PHP_EOL;
        }else{
            $this->_FehlerCount = 0;
        }
    }

    private function schreibeEintragInDatenbank($Logeintrag){

        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();

        $Insert  ='INSERT';
        $Insert .=' INTO SLACKPROTOKOLL';
        $Insert .='   (';
        $Insert .='     XSP_NACHRICHT,';
        $Insert .='     XSP_EMPFAENGER,';
        $Insert .='     XSP_ABSENDER,';
        $Insert .='     XSP_XSB_KEY,';
        $Insert .='     XSP_VERSANDSTATUS,';
        $Insert .='     XSP_VERSANDPROTOKOLL,';
        $Insert .='     XSP_USER';
        $Insert .='   )';
        $Insert .='   VALUES';
        $Insert .='   (';
        $Insert .= $DB->WertSetzen('XSP','T',$Logeintrag);
        $Insert .='     :v1,';
        $Insert .='     :v2,';
        $Insert .='     :v3,';
        $Insert .='     :v4,';
        $Insert .='     :v5,';
        $Insert .='     :v6,';
        $Insert .='     :v7';
        $Insert .='   )';

        $DB->Ausfuehren($Insert);
    }

    public function setzeLogDatei($Datei){
        if($Datei!=''){
            if(strpos($Datei,'/')===false){
                $Datei = '/var/log/awis/' . $Datei;
            }
            $this->_LogDatei=$Datei;
        }
    }

}

