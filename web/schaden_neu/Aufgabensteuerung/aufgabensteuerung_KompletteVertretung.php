<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
require_once 'aufgabensteuerung_funktionen.php';
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;
global $SpeichernFrame1;
global $SpeichernFrame2;
global $SpeichernFrame3;

try
{
    //var_dump($_REQUEST);
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SBV','%');
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	
	$Recht40016 = $AWISBenutzer->HatDasRecht(40016);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SBV'));
	
	$Fehler = 0;

	
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1); 
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'zuv_sbname';
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
		
	if(isset($_POST['cmdSpeichern_x']))
	{
	    $SpeichernOK[1]=true;
	    $SpeichernOK[2]=true;
		include('./aufgabensteuerung_speichern.php');	
	}
	elseif(isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./aufgabensteuerung_loeschen.php');
	}

	
	$Form->SchreibeHTMLCode('<form name="frmSBVKompletteVertretung" action="./aufgabensteuerung_Main.php?cmdAktion=KompletteVertretung" method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	$Script = "
            <script type='text/javascript'>
   
            function EinAusklappen(frame,Seite) {
	
    	 	if(document.getElementById('FRAME'+frame).style.display == 'block')
        	{
        	     document.getElementById('FRAME'+frame).style.display = 'none';
    			 if(frame != 5)
    			 {
    				document.getElementById('UEB'+frame).style.borderBottom = '';
    			 }
    			 document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_plus.png';
    	    }
    		else
    		{
    			  document.getElementById('FRAME'+frame).style.display = 'block';
    			  if(frame != 5)
    			  {
    			  	document.getElementById('UEB'+frame).style.borderBottom = '1px solid black';
    			  }
    			  document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_minus.png';
    		}
	   
    	}
	    
	    function WVInhaberUebernehmen()
            {
                if (document.getElementsByName('txtSBV_WV_ZUVERTRETEN')[0].value != '::bitte w�hlen::')
                {
                    document.getElementsByName('sucSBV_WV_ID')[0].value = document.getElementsByName('txtSBV_WV_ZUVERTRETEN')[0].value;
                
                }
                else
                {
                     document.getElementsByName('sucSBV_WV_ID')[0].value = '';
                }
        
                //alert(document.getElementsByName('txtBEW_BPK')[0].value);       
            }
	    
	       function VorgangUebernehmen()
            {
	            if (document.getElementsByName('txtSBV_oWV_ZUVERTRETEN')[0].value != '::bitte w�hlen::')
                {
	               document.getElementsByName('sucSBV_oWV_ID')[0].value = document.getElementsByName('txtSBV_oWV_ZUVERTRETEN')[0].value;
                
                }
                else
                {
                     document.getElementsByName('sucSBV_oWV_ID')[0].value = '';
                }
        
                //alert(document.getElementsByName('txtBEW_BPK')[0].value);       
            }
     
	
             </script>
            ";
	
	echo $Script;
	
	
	//Start Frame 1	
	$Frame1 = isset($_GET['Frame1'])?false:true;
	
	
	if($Frame1)
	{
	    $Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border: 1px solid black;','UEB1"ID="UEB1');
	    $Form->Schaltflaeche('script', 'cmdKlappen','onclick="EinAusklappen(1)"', '/bilder/icon_minus.png"ID="IMG_LOAD1','Ein-/Ausklappen', 'W','width:30px',17,17);
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SBV']['SBV_KOMPLETTE_VERTRETUNG'], 300, 'color:#FFFFFF;font-weight:bolder');
	    $Form->ZeileEnde();
	    $Form->ZeileStart('display:block','FRM1"ID="FRAME1');
	}
	else
	{
	    $Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;','UEB1"ID="UEB1');
	    $Form->Schaltflaeche('script', 'cmdKlappen','onclick="EinAusklappen(1)"', '/bilder/icon_plus.png"ID="IMG_LOAD1','Ein-/Ausklappen', 'W','width:30px',17,17);
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['SBV']['SBV_KOMPLETTE_VERTRETUNG'], 300, 'color:#FFFFFF;font-weight:bolder');
	    $Form->ZeileEnde();
	    $Form->ZeileStart('display:none','FRM1"ID="FRAME1');
	}
	
	if($SpeichernFrame1!='')
	{
	    $Form->ZeileStart();
	    $Form->Hinweistext($SpeichernFrame1);
	    $Form->ZeileEnde();   
	}
	
    //Frame 1: Kompletter Vertreter
	$SQL  ='SELECT';
	$SQL .='   sbv_key,';
	$SQL .='   sbv_vertreter_sb_id,';
	$SQL .='   sbv_zuvertreten_sb_id,';
	$SQL .='   zuv_sbname,';
	$SQL .='   zuv_sbvorname,';
	$SQL .='   zuv_sbkuerzel,';
	$SQL .='   ver_sbname,';
	$SQL .='   ver_sbvorname ,';
	$SQL .='   ver_sbkuerzel,';
	$SQL .='   sbv_gueltig_ab,';
	$SQL .='   sbv_gueltig_bis';
	$SQL .= ' ,row_number() OVER ( ';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	$SQL  .='SELECT';
	$SQL .='   A.sbv_key,';
	$SQL .='   A.sbv_vertreter_sb_id,';
	$SQL .='   A.sbv_zuvertreten_sb_id,';
	$SQL .='   zuvertreten.sbname    AS zuv_sbname,';
	$SQL .='   zuvertreten.sbvorname AS zuv_sbvorname,';
	$SQL .='   zuvertreten.sbkuerzel AS zuv_sbkuerzel,';
	$SQL .='   vertreter.sbname      AS ver_sbname,';
	$SQL .='   vertreter.sbvorname   AS ver_sbvorname ,';
	$SQL .='   vertreter.sbkuerzel   AS ver_sbkuerzel,';
	$SQL .='   A.sbv_gueltig_ab,';
	$SQL .='   A.sbv_gueltig_bis';
	$SQL .=' FROM';
	$SQL .='   sachbearbeitervertreter A';
	$SQL .=' INNER JOIN sachbearbeiter vertreter';
	$SQL .=' ON';
	$SQL .='   A.sbv_vertreter_sb_id = vertreter.ID';
	$SQL .=' INNER JOIN sachbearbeiter zuvertreten';
	$SQL .=' ON';
	$SQL .='   A.SBV_ZUVERTRETEN_SB_ID = zuvertreten.ID WHERE (a.SBV_GUELTIG_BIS >= trunc(sysdate) or a.SBV_GUELTIG_BIS is null )and a.SBV_GUELTIG_AB <= trunc(sysdate) )';
	
	$Form->DebugAusgabe(1,$SQL);
	$rsSBV = $DBSCHAD->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['SBV_ZUVERTRETEN'] = 250;
	$FeldBreiten['SBV_VERTRETER'] = 250;
	$FeldBreiten['ICON'] = 19;
	
	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 3);
	}
	
	//$Gesamtbreite += 15;
	
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ICON'], '', '');
	$Link = './aufgabensteuerung_Main.php?Sort=VER_SBNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VER_SBNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBV']['SBV_VERTRETER'], $FeldBreiten['SBV_VERTRETER'], '', $Link);
	$Link = './aufgabensteuerung_Main.php?Sort=ZUV_SBNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUV_SBNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBV']['SBV_ZUVERTRETEN'], $FeldBreiten['SBV_ZUVERTRETEN']+ ($rsSBV->AnzahlDatensaetze() > 11?23:0), '',$Link);
	
	$Form->ZeileEnde();
	
	if($rsSBV->AnzahlDatensaetze() > 11)
	{
		$Form->SchreibeHTMLCode('<div id=scrKomVert style="width: 554px; height: 160px;  overflow-y: scroll;">');
	}
	
	$DS = 0;	// f�r Hintergrundfarbumschaltung
	while(! $rsSBV->EOF())
	{
	    $Icons = array();
	    $Icons[] = array('delete','./aufgabensteuerung_Main.php?Del='.$rsSBV->FeldInhalt('SBV_KEY').'&DelTyp=SBV');
	   
		$Form->ZeileStart('');
		$Form->Erstelle_ListeIcons($Icons,$FeldBreiten['ICON'],($DS%2));
		 
		$TTT = $rsSBV->FeldInhalt('VER_SBNAME');
		$Form->Erstelle_ListenFeld('SBV_VERTRETER', $rsSBV->FeldInhalt('VER_SBNAME'), 0, $FeldBreiten['SBV_VERTRETER'], false, ($DS%2), '','', 'T', 'L', $TTT);
		
		$TTT =  $rsSBV->FeldInhalt('SBV_VERTRETER');
		$Form->Erstelle_ListenFeld('SBV_ZUVERTRETEN',$rsSBV->FeldInhalt('ZUV_SBNAME'), 0, $FeldBreiten['SBV_ZUVERTRETEN'], false, ($DS%2), '','', 'T', 'L', $TTT);
		$LinkAktiv = '';
			
		$Form->ZeileEnde();
		$DS++;
		$rsSBV->DSWeiter();
	}
	
	if($rsSBV->AnzahlDatensaetze() > 11)
	{
		$Form->SchreibeHTMLCode('</div>') ;
	}
			
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].': '.$rsSBV->AnzahlDatensaetze(), $Gesamtbreite+ ($rsSBV->AnzahlDatensaetze() > 11?22:0), 'font-weight:bolder;');
	//$Link = './versarten_Main.php?cmdAktion=Details';
	//$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	$Form->ZeileEnde();
	
	$SQLVertreter  ='SELECT distinct ';
	$SQLVertreter .='   a.id,';
	$SQLVertreter .='   a.sbname';
	$SQLVertreter .='   ||\' \'';
	$SQLVertreter .='   ||A.SBVORNAME';
	$SQLVertreter .='   ||\'(\'';
	$SQLVertreter .='   ||A.SBKUERZEL';
	$SQLVertreter .='   ||\')\' AS Name';
	$SQLVertreter .=' FROM';
	$SQLVertreter .='   sachbearbeiter a';
	$SQLVertreter .=' LEFT JOIN SACHBEARBEITERVERTRETER b';
	$SQLVertreter .=' ON';
	$SQLVertreter .='   A.ID               =b.SBV_ZUVERTRETEN_SB_ID';
	$SQLVertreter .=' AND b.SBV_GUELTIG_AB<=TRUNC(sysdate)';
	$SQLVertreter .=' AND';
	$SQLVertreter .='   (';
	$SQLVertreter .='     b.SBV_GUELTIG_BIS  >= TRUNC(sysdate)';
	$SQLVertreter .='   OR b.SBV_GUELTIG_BIS IS NULL';
	$SQLVertreter .='   )';
	$SQLVertreter .=' WHERE';
	//$SQLVertreter .='   b.SBV_KEY        IS NULL'; //Nur welche, die nicht von jemanden anders Vertreten werden
	$SQLVertreter .='  A.AUSGESCHIEDEN = 0';
	$SQLVertreter .=' ORDER BY';
	$SQLVertreter .='   name';
	
	
	$SQLZuVertreten  ='SELECT';
	$SQLZuVertreten .='   a.id,';
	$SQLZuVertreten .='   a.sbname';
	$SQLZuVertreten .='   ||\' \'';
	$SQLZuVertreten .='   ||A.SBVORNAME';
	$SQLZuVertreten .='   ||\'(\'';
	$SQLZuVertreten .='   ||A.SBKUERZEL';
	$SQLZuVertreten .='   ||\')\' AS Name';
	$SQLZuVertreten .=' FROM';
	$SQLZuVertreten .='   sachbearbeiter a';
	$SQLZuVertreten .=' WHERE';
	$SQLZuVertreten .='   A.AUSGESCHIEDEN = 0';
	$SQLZuVertreten .=' ORDER BY';
	$SQLZuVertreten .='   name';
	$AWISCursorPosition = 'txtSBV_VERTRETER';
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('', 26);
	$Form->Erstelle_SelectFeld('SBV_VERTRETER', ((!$SpeichernOK[1] and isset($_POST['txtSBV_VERTRETER']))?$_POST['txtSBV_VERTRETER']:''), $FeldBreiten['SBV_VERTRETER'],true,$SQLVertreter,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'',null,'','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel('', 5);
	$Form->Erstelle_SelectFeld('SBV_ZUVERTRETEN', ((!$SpeichernOK[1] and isset($_POST['txtSBV_ZUVERTRETEN']))?$_POST['txtSBV_ZUVERTRETEN']:''), $FeldBreiten['SBV_ZUVERTRETEN'],true,$SQLZuVertreten,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'',null,'','',array(),'','SCHAD');
	$Form->ZeileEnde();
	
	
	$Form->ZeileEnde(); //Frame1 Ende
   
	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if(($Recht40016&2)==2)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SBV',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

?>