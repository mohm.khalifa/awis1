
<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Tabelle= '';





if(isset($FelderDel) and $FelderDel != '') // Wenn rotes X bei einer Massnahme gedr�ckt
{
	switch($_GET['cmdAktion'])
	{
		case 'Details':
			$Tabelle = 'QMM';
			
			$Felder = explode(';',$FelderDel);
			$Feld = explode('_',$Felder[1]);
			$Key=$Feld[2];
			
			$SQL = 'SELECT * ';     // Massnahme die gel�scht werden soll holen
			$SQL.= ' FROM QMMASSNAHMENZUORD ';
			$SQL.= ' INNER JOIN QMMASSNAHMEN ON QMZ_QMM_KEY = QMM_KEY ';
			$SQL.= ' WHERE QMZ_KEY='.$Key;
			$rsDaten = $DB->RecordSetOeffnen($SQL);
				
			/**
			 * Werte f�r Frage 'Wirklich l�schen?' holen
			 **/
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('QMM','QMM_MASSNAHME'),$rsDaten->FeldInhalt('QMM_BEZEICHNUNG'));
			$Felder[]=array($Form->LadeTextBaustein('QMM','QMM_BESCHREIBUNG'),$rsDaten->FeldInhalt('QMZ_BEMERKUNG'));
			$Felder[]=array($Form->LadeTextBaustein('QMM','QMM_FRIST'),$Form->Format('D',$rsDaten->FeldInhalt('QMZ_FRIST')));
			break;
	}
}


if(isset($_GET['Del'])) // Wenn rotes X bei einer Massnahme gedr�ckt
{
		switch($_GET['cmdAktion'])
		{
			case 'Stammdaten':
				if(isset($_GET['Unterseite']))
				{
					$Tabelle = 'QMG';
					$Key=$_GET['Del'];
						
					$SQL  = ' Select * from QMKRITERIENGUELTIG ';
					$SQL .= ' inner join qmkriterien on qmk_key = qmg_qmk_key';
					$SQL .= ' where qmg_key='.$_GET['Del'];
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					
					/**
					 * Werte f�r Frage 'Wirklich l�schen?' holen
					**/
					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('QMM','QMK_LANG'),$rsDaten->FeldInhalt('QMK_LANG'));
					$Felder[]=array($Form->LadeTextBaustein('QMM','QMG_GUELTIGAB'),$Form->Format('D',$rsDaten->FeldInhalt('QMG_GUELTIGAB')));
					$Felder[]=array($Form->LadeTextBaustein('QMM','QMG_GUELTIGBIS'),$Form->Format('D',$rsDaten->FeldInhalt('QMG_GUELTIGBIS')));
				}
				else 
				{
					if($_GET['Seite'] == 'Massnahmen')
					{
						$Tabelle = 'QMP';
						$Key=$_GET['Del'];
						
						$SQL  = ' Select * from QMMASSNAHMEN ';
						$SQL .= ' where qmm_key='.$_GET['Del'];
						$rsDaten = $DB->RecordSetOeffnen($SQL);
							
						/**
						 * Werte f�r Frage 'Wirklich l�schen?' holen
						**/
						$Felder=array();
						$Felder[]=array($Form->LadeTextBaustein('QMM','QMM_LANG'),$rsDaten->FeldInhalt('QMM_BEZEICHNUNG'));
					}
					else 
					{
						$Tabelle = 'QMK';
						$Key=$_GET['Del'];
								
						$SQL = 'select qmk_key,qmk_lang,qmk_aktiv,qmk_user,qmk_userdat,to_char(qmkriterien_minmax(\'min\',gueltigkeiten.qmk_key),\'DD.MM.YYYY\') as qmg_gueltigab, ';     
						$SQL.= ' to_char(qmkriterien_minmax(\'max\',gueltigkeiten.qmk_key),\'DD.MM.YYYY\') as qmg_gueltigbis ';
						$SQL.= ' from(select * from qmkriterien) gueltigkeiten ';
						$SQL.= ' WHERE QMK_KEY=\''.$Form->Format('T',$_GET['Del']).'\'';
						$rsDaten = $DB->RecordSetOeffnen($SQL);
							
						/**
						 * Werte f�r Frage 'Wirklich l�schen?' holen
						**/
						$Felder=array();
						$Felder[]=array($Form->LadeTextBaustein('QMM','QMK_KEY'),$rsDaten->FeldInhalt('QMK_KEY'));
						$Felder[]=array($Form->LadeTextBaustein('QMM','QMK_LANG'),$rsDaten->FeldInhalt('QMK_LANG'));
					}
				}
			break;
				
		}
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren wenn auf Frage 'Wirklich l�schen?' ok
{
	switch ($_POST['txtTabelle'])
	{
		case 'QMM':
			
			$SQL = 'SELECT * FROM QMMASSNAHMENZUORD';
			$SQL .= ' inner join QMMASSNAHMEN ON QMZ_QMM_KEY = QMM_KEY ';
			$SQL .= ' inner join QMSTATUS ON QMZ_STATUS = QMS_KEY ';
			$SQL .= ' WHERE qmz_key='.$_POST['txtKey'];
			$rsDaten = $DB->RecordSetOeffnen($SQL);
			
			$SQL = 'DELETE FROM QMMASSNAHMENZUORD WHERE qmz_key='.$_POST['txtKey']; // Massnahme aus Zuordnungen l�schen
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
				
			
			$SQL2 = 'INSERT INTO QMHISTORY ';
			$SQL2 .=  '(QMH_TABELLE,QMH_KEY,QMH_AKTION,QMH_ALT,QMH_NEU,QMH_USER,QMH_USERDAT,QMH_QMP_KEY)';
			$SQL2 .=  ' VALUES';
			$SQL2 .=  ' (\'QMMASSNAHMENZUORD\','.$_POST['txtKey'].',\'Ma�nahme -\',';
			$SQL2 .=  '\'Ma�nahme:'.$rsDaten->FeldInhalt('QMM_BEZEICHNUNG').',Beschreibung:'.$rsDaten->FeldInhalt('QMZ_BEMERKUNG').',Frist:'.$Form->Format('D',$rsDaten->FeldInhalt('QMZ_FRIST')).',Status:'.$rsDaten->FeldInhalt('QMS_BEZEICHNUNG').'\',';		
			$SQL2 .=  '\'\',';
			$SQL2 .=  '\''.$AWISBenutzer->BenutzerName().'\',sysdate,'.$rsDaten->FeldInhalt('QMZ_QMP_KEY').')';
	
			
			if($DB->Ausfuehren($SQL2)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
			
			break;	
		case 'QMK':
			$SQL = 'DELETE FROM QMKRITERIEN WHERE qmk_key=\''.$_POST['txtQMK_KEY'].'\''; // Massnahme aus Zuordnungen l�schen
		
				
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
				
			break;
		case 'QMG':
			$SQL = 'DELETE FROM QMKRITERIENGUELTIG WHERE qmg_key=\''.$_POST['txtQMG_KEY'].'\''; // Massnahme aus Zuordnungen l�schen
		
		
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		
			break;
		case 'QMP':
			$SQL = 'DELETE FROM QMMASSNAHMEN WHERE qmm_key=\''.$_POST['txtQMM_KEY'].'\''; // Massnahme aus Zuordnungen l�schen
		
		
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}		
			break;
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['cmdAktion'])
	{
		case 'Details':
			$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./qualitaetsmaengel_Main.php?cmdAktion=Details&QMP_KEY='.$_POST['txtQMP_KEY'].' method=post>');
		
			$Form->Formular_Start();
			
			$Form->ZeileStart();		
			$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
			$Form->ZeileEnde();
		
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
			 **/
			$Form->Erstelle_HiddenFeld('Key',$Key);
			$Form->Erstelle_HiddenFeld('FESTSTELLUNG',$_POST['txtFESTSTELLUNG']);
			$Form->Erstelle_HiddenFeld('QMM_MASSNAHME',$_POST['txtQMM_MASSNAHME']);
			$Form->Erstelle_HiddenFeld('QMZ_BESCHREIBUNG',$_POST['txtQMZ_BESCHREIBUNG']);
			$Form->Erstelle_HiddenFeld('QMM_FRIST',$_POST['txtQMM_FRIST']);
			$Form->Erstelle_HiddenFeld('QMM_Persnr',$_POST['txtQMM_Persnr']);
			$Form->Erstelle_HiddenFeld('QMM_STATUS',$_POST['txtQMM_STATUS']);
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		
			$Form->Trennzeile();
		
			/**
			 * L�schen ja/nein
			 **/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],''); 
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();
		
			$Form->SchreibeHTMLCode('</form>');
		
			$Form->Formular_Ende();
		
			die();
			break;
		case 'Stammdaten':
			if(isset($_GET['Unterseite']))
			{
				$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].'&Unterseite='.$_GET['Unterseite'].'&QMK_KEY='.$rsDaten->FeldInhalt('QMK_KEY').  ' method=post>');
				
				$Form->Formular_Start();
					
				$Form->ZeileStart();
				$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
				$Form->ZeileEnde();
				
				$Form->Trennzeile('O');
					
				foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
				{
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($Feld[0].':',200);
					$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
					$Form->ZeileEnde();
				}
					
				/**
				 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
				 **/
				$Form->Erstelle_HiddenFeld('QMG_KEY',$_GET['Del']);
				$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
				
				$Form->Trennzeile();
				
				/**
				 * L�schen ja/nein
				**/
				$Form->ZeileStart();
				$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
				$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
				$Form->ZeileEnde();
				
				$Form->SchreibeHTMLCode('</form>');
				
				$Form->Formular_Ende();
				
				die();
			}
			else
			{
				if($_GET['Seite'] == 'Massnahmen')
				{
					$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].' method=post>');
						
					$Form->Formular_Start();
					
					$Form->ZeileStart();
					$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
					$Form->ZeileEnde();
						
					$Form->Trennzeile('O');
					
					foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
					{
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel($Feld[0].':',200);
						$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
						$Form->ZeileEnde();
					}
					
					/**
					 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
					 **/
					$Form->Erstelle_HiddenFeld('QMM_KEY',$_GET['Del']);
					$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
						
					$Form->Trennzeile();
						
					/**
					 * L�schen ja/nein
					**/
					$Form->ZeileStart();
					$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
					$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
					$Form->ZeileEnde();
						
					$Form->SchreibeHTMLCode('</form>');
						
					$Form->Formular_Ende();
						
					die();
				}
				else
				{		
					$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./qualitaetsmaengel_Main.php?cmdAktion=Stammdaten&Seite='.$_GET['Seite'].' method=post>');
					
					$Form->Formular_Start();
						
					$Form->ZeileStart();
					$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	// Fragen ob wirklich gel�scht werden soll
					$Form->ZeileEnde();
					
					$Form->Trennzeile('O');
						
					foreach($Felder AS $Feld) // Kennfelder f�r Massnahmen anzeigen.
					{
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel($Feld[0].':',200);
						$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
						$Form->ZeileEnde();
					}
						
					/**
					 * Hiddenfelder f�r Anzeigen und zum l�schen des Datensatzes per Hidden �bergeben.
					 **/
					$Form->Erstelle_HiddenFeld('QMK_KEY',$_GET['Del']);
					$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
					
					$Form->Trennzeile();
					
					/**
					 * L�schen ja/nein
					**/
					$Form->ZeileStart();
					$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
					$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
					$Form->ZeileEnde();
					
					$Form->SchreibeHTMLCode('</form>');
					
					$Form->Formular_Ende();
					
					die();
				}
			}
			break;
	}
}

?>