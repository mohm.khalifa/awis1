#!/bin/bash
#################################################################################
#
# Job-Scheluder Aufruf
#
# Dieses Skript wird in der CRONTAB des Users wwwrun gestartet. Dieses
# ruft wiederum den JobScheduler PHP auf, der dann die eigentliche Arbeit macht
#
# Autor: Sacha Kerres
# Datum: 19.10.2007
#
#################################################################################

PIDFILE=/daten/jobs/pid/jobscheduler.pid

source /etc/profile

echo "Scheduler startet "$(date +%T)
if [ ! -f $PIDFILE ]
then
  echo $$ > $PIDFILE
  echo Starte JobScheduler.php $(date +%T) >> /var/log/jobscheduler.out
  cd /daten/jobs/bin
  /usr/local/php/bin/php -c /etc -f /daten/jobs/bin/jobscheduler.php >> /var/log/jobscheduler.out
  rm $PIDFILE
else
  PID=$(<$PIDFILE)
  PSCOUNT=$(ps -j $PID | grep $PID -c)
  if [ $PSCOUNT -ne 1 ]
  then
    echo "Prozess laeuft nicht mehr..."
    rm $PIDFILE
  else
    echo "Laeuft schon..."
  fi
fi

