<?php
// Code gepr�ft, Sacha Kerres, 22.07.2005
global $con;
global $awisRSZeilen;
global $AWISBenutzer;

if (awisBenutzerRecht($con, 117) == 0) {
    awisEreignis(3, 1000, 'Filialen: Lieferungen', $AWISBenutzer->BenutzerName(), '', '', '');
    die("Keine ausreichenden Rechte!");
} 
// Lokale Variablen
$Bedingung = ''; // Bedingung f�r SQL
$SQL = ''; // SQL Abfragestring
awis_Debug(1,$_POST["txtParameter"]);
if(isset($_POST["txtParameter"]))
{
	$Bedingung = $_POST["txtParameter"];
	awis_BenutzerParameterSpeichern($con, "Filialen_Lieferungen" ,$AWISBenutzer->BenutzerName(), $Bedingung);
}
else
{
	$Bedingung = awis_BenutzerParameter($con, "Filialen_Lieferungen" , $AWISBenutzer->BenutzerName());
}

print "</form><form name=frmSuche method=post action=./filialinfo_Main.php>";
print "<input type=hidden name=cmdAktion value=Lieferungen>";
print "<table boder=0>";
print "<tr><td>Filialen:</td>";
print "<td valign=top><input name=txtParameter size=60 value=" . $Bedingung . "></td>";
print "<td><input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
print "</td></tr></table>";
print "</form>";

//print "<hr>"; 
// Daten f�r die Infoseite sammeln
$SQL = "SELECT FILIALEN.*, FIF_WERT
				FROM filialinfos, filialinfostypen, filialinfostypengruppen, filialen
				 WHERE ((filialinfostypen.fit_id = filialinfos.fif_fit_id)
				 	AND FILIALEN.FIL_ID = filialinfos.FIF_FIL_ID
        			AND (filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id)
					AND (FIF_FIT_ID=49))";

$SQL .= " AND FIL_ID IN (" . awisListenParameter($Bedingung) . ")";


If(isset($_GET["Sortierung"]))
{
	if($_GET["Sortierung"]=="Tag")
	{
		$SQL .= " ORDER BY FIF_WERT, FIL_Bez";
	}
	elseif($_GET["Sortierung"]=="Lager")
	{
		$SQL .= " ORDER BY FIL_LAGERKZ, FIL_Bez";
	}
	else
	{
		$SQL .= " ORDER BY FIL_Bez";
	}
}
else
{
	$SQL .= " ORDER BY FIL_Bez";
}


$rsFiliale = awisOpenRecordset($con, $SQL);
$rsFilialeAnz = $awisRSZeilen;

If ($rsFilialeAnz >= 1) 
{ // Liste gefunden
   print "<table border=0 width=100%>";
   print "<tr><td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Lieferungen&Sortierung=Filiale>Filiale</a></td>";
   print "<td id=FeldBez>Anschrift</td>";
   print "<td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Lieferungen&Sortierung=Tag>Anlieferungstag</a></td>";
   print "<td id=FeldBez><a href=./filialinfo_Main.php?cmdAktion=Lieferungen&Sortierung=Lager>Lager</a></td></tr>";
   
   for($i = 0;$i < $rsFilialeAnz;$i++) 
   {
        print "<tr><td  " . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . "><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
        print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
        print "<td>" . $rsFiliale["FIL_STRASSE"][$i] . ", " . $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td>";
        print "<td>" . $rsFiliale["FIF_WERT"][$i] . "</td>";
        print "<td>" . awis_LagerText($rsFiliale["FIL_LAGERKZ"][$i]) . "</td>"; // Bezeichnung laden
        print "</tr>";
    } 
    print "</table>";
}
else 
{
    print "<span class=HinweisText>Es sind keine Filialen zum Anzeigen angegeben oder es wurden keine Daten zu den angegebenen gefunden!</span>";
} 




?>