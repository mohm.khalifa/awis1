<?php

require_once 'awisBerichte.inc';

class ber_ReifenReklamationen
    extends awisBerichte
{
    /**
     * @var reifen_funktionen
     */
    private $REX;

    public function init(array $Parameter)
    {

        global $REX;
        $this->REX = $REX;

        $REX_KEYs = implode(', ', $_POST['txtDruckKey']);

        $SQL = 'SELECT ';
        $SQL .= '   REX_KEY,';
        $SQL .= '   REX_REKLAMATIONSDATUM,';
        $SQL .= '   REX_WERK,';
        $SQL .= '   upper(REX_AST_ATUNR) as REX_AST_ATUNR,';
        $SQL .= '   REX_HERSTELLER,';
        $SQL .= '   REX_P,';
        $SQL .= '   REX_EM,';
        $SQL .= '   REX_RA,';
        $SQL .= '   REX_LAN_CODE,';
        $SQL .= '   REX_PROFILTIEFE,';
        $SQL .= '   REX_LAR_NR,';
        $SQL .= '   REX_STATUS,';
        $SQL .= '   REX_USER,';
        $SQL .= '   REX_USERDAT,';
        $SQL .= '   AST_BEZEICHNUNGWW ';
        $SQL .= ' FROM REIFENREKLAMATIONEN ';
        $SQL .= ' left join  artikelstamm ';
        $SQL .= ' on ast_atunr = upper(rex_ast_atunr) ';
        $SQL .= 'WHERE REX_KEY in ( ' . $REX_KEYs . ')';
        $SQL .= ' order by REX_HERSTELLER asc ';

        $REX->DB->TransaktionBegin();
        $this->_rec = $this->_DB->RecordSetOeffnen($SQL);
        $SQL = 'UPDATE REIFENREKLAMATIONEN set REX_STATUS = \'G\'';
        $SQL .= 'WHERE REX_KEY in ( ' . $REX_KEYs . ')';
        $this->REX->DB->Ausfuehren($SQL);
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        $this->SetTitle('Reifenreklamation');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISBenutzer->BenutzerName());
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);
        $this->NeueSeite();

        $LetzterHersteller = '';
        $PosCnt = 1;
        while (!$this->_rec->EOF()) {
            $AktuellerHersteller = $this->_rec->FeldInhalt('REX_HERSTELLER');
            if ($AktuellerHersteller != $LetzterHersteller or $PosCnt >= 56) {
                $PosCnt = 1;
                if ($LetzterHersteller != '') { //Nicht beim ersten Hersteller
                    $this->NeueSeite();
                }
                //*************************************************************
                //* Herstellerüberschrift
                //*************************************************************
                $LetzterHersteller = $this->_rec->FeldInhalt('REX_HERSTELLER');

                $this->_Zeile = $this->_Parameter['SeiteOben'];
                $this->_Spalte = $this->_Parameter['LinkerRand'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseUeberschrift']);

                $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoeheUeberschrift'], $AktuellerHersteller, 0, 0, 'L');


                $this->_Zeile = $this->_Parameter['ZeilenAbstandKopf'] * 1.5;
                $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesse']);
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $AnfangKopfY = $this->GetY();
                $AnfangKopfX = $this->GetX();

                //*************************************************************
                //* Kopf
                //*************************************************************
                $Felder = array(
                    'REX_RUECKSENDEBELEG',
                    'REX_KUNDEN_NR',
                    'REX_B',
                    'REX_V',
                    'REX_GA',
                    'REX_AL',
                    'REX_KndA',
                    'REX_PK',
                    'REX_AUSSTELLER',
                    'REX_Kontrollnr',
                    'REX_InAbfallMitMRS',
                    'REX_RUECKSENDUNGmitLS',
                    'REX_ERSATZmitGutschrift',
                    'REX_ErsatzMitLsNr'
                );

                foreach ($Felder as $Feld) {
                    if ($Feld == 'REX_InAbfallMitMRS') { //Wann soll die zweite Zeile angefangen werden?
                        //Obere dicke Line malen
                        $this->SetLineWidth(0.4);
                        $this->Line($AnfangKopfX,$this->GetY(),$this->GetX(),$this->GetY());
                        $this->SetLineWidth(0.2);

                        $this->_Zeile += $this->_Parameter['ZeilenHoeheKopf'];
                        $this->SetXY($this->_Spalte, $this->_Zeile);
                    }
                    //Text rausschreiben
                    $XVorher = $this->GetX();
                    $YVorher = $this->GetY();
                    $this->SetXY($XVorher, $YVorher - 3);

                    //Rahmen rummalen
                    $this->Cell($this->_Parameter[$Feld], $this->_Parameter['ZeilenHoeheKopf'], $this->REX->AWISSprachKonserven['REX'][$Feld], 0, 0, 'L');
                    $this->SetXY($XVorher, $this->_Zeile);
                    $this->Cell($this->_Parameter[$Feld], $this->_Parameter['ZeilenHoeheKopf'], '', 1, 0, 'L');
                }


                $EndeKopfx = $this->getX();

                //*************************************************************
                //* Spaltenüberschriften
                //*************************************************************
                $this->_Zeile += $this->_Parameter['ZeilenHoeheKopf'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->SetLineWidth(0.4);
                $this->Line($AnfangKopfX,$AnfangKopfY,$AnfangKopfX,$this->GetY());
                $this->SetLineWidth(0.2);
                $Felder = array(
                    'REX_WERK',
                    'REX_AST_ATUNR',
                    'REX_AST_BEZEICHNUNG',
                    'REX_ARTIKEL_NR',
                    'REX_NR',
                    'REX_ME',
                    'REX_P',
                    'REX_EM',
                    'REX_RA',
                    'REX_LK',
                    'REX_PROFIL',
                    'REX_VERMERKE'

                );
                foreach ($Felder as $Feld) {
                    //Text rausschreiben
                    $XVorher = $this->GetX();
                    $YVorher = $this->GetY();
                    $this->Cell($this->_Parameter[$Feld], $this->_Parameter['ZeilenHoehe'], $this->REX->AWISSprachKonserven['REX'][$Feld], 1, 0, 'L');

                }
                $this->SetLineWidth(0.4);
                $this->Line($AnfangKopfX,$this->GetY()-1,$this->GetX(),$this->GetY()-1); //Dicke Linie über den Spaltennamen
                $this->Line($AnfangKopfX,$AnfangKopfY,$AnfangKopfX,$this->GetY()+$this->_Parameter['ZeilenHoehe']); //Dicke Linie links neben Spaltennamen
                $this->Line($this->GetX(),$AnfangKopfY,$this->GetX(),$this->GetY()+$this->_Parameter['ZeilenHoehe']); //Dicke Linie rechts neben Spaltennamen
               $this->Line($AnfangKopfX,$this->GetY()+$this->_Parameter['ZeilenHoehe'],$this->GetX(),$this->GetY()+$this->_Parameter['ZeilenHoehe']);//Dicke Linie unter den Spaltennamen


            }
            $this->SetLineWidth(0.2);
            //*************************************************************
            //* Positionsdaten
            //*************************************************************
            $this->_Zeile += $this->_Parameter['ZeilenHoehe'];
            $this->SetXY($this->_Spalte, $this->_Zeile);


            $this->Cell($this->_Parameter['REX_WERK'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_WERK'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_AST_ATUNR'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_AST_ATUNR'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_AST_BEZEICHNUNG'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('AST_BEZEICHNUNGWW'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_ARTIKEL_NR'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_LAR_NR'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_NR'], $this->_Parameter['ZeilenHoehe'],$PosCnt++, 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_ME'], $this->_Parameter['ZeilenHoehe'], 1, 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_P'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_P'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_EM'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_EM'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_RA'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_RA'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_LK'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_LAN_CODE'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_PROFIL'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_PROFILTIEFE'), 1, 0, 'L');
            $this->Cell($this->_Parameter['REX_VERMERKE'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('REX_VERMERKE'), 1, 0, 'L');


            $this->_rec->DSWeiter();
        }

        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************
        $this->REX->DB->TransaktionCommit();
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    public function footer(){
        $this->_Zeile = $this->_Parameter['FooterPos'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Felder = array(
            'REX_REKLABWICKLUNGUEBERFA',
            'REX_AUSSENDIENST',
            'REX_DATUM',
            'REX_UNTERSCHRIFT'
        );
        foreach ($Felder as $Feld){
            $this->Cell($this->_Parameter[$Feld], $this->_Parameter['ZeilenHoehe'], $this->REX->AWISSprachKonserven['REX'][$Feld], 'T', 0, 'C');
            $this->SetX($this->GetX()+10);
        }
    }


    private function NeueSeite()
    {
        $this->AddPage('P');
    }
}