<?php
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 1100);
if($RechteStufe<256)
{
   awisEreignis(3,1100,'Hilfs- und Betriebsstoffe Auswertungen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

/***********************************************************************
* Auswertungen starten
***********************************************************************/

switch(isset($_POST['txtAuswertung']))
{
	case 1:
		include './hibe_Auswertungen_Uebersicht.php';
		echo '<br><hr>';
		break;
	case 2:
		include './hibe_Auswertungen_Uebersicht.php';
		echo '<br><hr>';
		break;
	case 3:
		include './hibe_Auswertungen_Uebersicht.php';
		echo '<br><hr>';
		break;
}

/***********************************************************************
* Formular aufbauen
***********************************************************************/

echo '<form name=frmAuswertung method=post action=./hibe_Main.php?cmdAktion=Auswertungen>';

echo '<table border=1>';

$EingabeFeld = 'txtAuswertung';
echo '<tr>';
// Übersicht
echo '<td width=200 colspan=2 id=FeldBez>Auswertung:</td><td id=FeldBez>Parameter</td></tr>';
echo '<tr><td align=center><input type=radio name=txtAuswertung value=1 checked></td><td>Übersicht</td>';
echo '<td><table border=0>';

echo '<tr><td width=150>TKDL:</td><td><select name=txtMIT_KEY>';
echo '<option value=-1 selected>::alle::</option>';

$rsMIT = awisOpenRecordset($con, "SELECT * FROM Mitarbeiter WHERE MIT_MTA_ID = 30 AND MIT_Status='A' ORDER BY MIT_BEZEICHNUNG");
$rsMITZeilen = $awisRSZeilen;
for($i=0;$i<$rsMITZeilen;$i++)
{
	echo '<option value=' . $rsMIT['MIT_KEY'][$i] . '>' . $rsMIT['MIT_BEZEICHNUNG'][$i] . '</option>';
}

echo '</select>';
echo '</td></tr>';
echo '<tr><td>auch alte:</td><td><input type=checkbox name=txtAuchAlte value=on></td></tr>';
echo '</table>';

echo '</td></tr>';

// Änderungsliste
echo '<tr><td align=center><input type=radio name=txtAuswertung value=2></td><td>Änderungen</td>';
echo '<td><table border=0>';
echo '<tr><td width=150>Datum vom:</td><td><input name=txtDatumVom size=10 value=' . date('d.m.Y') . '></td></tr>';
echo '<tr><td>Datum bis:</td><td><input name=txtDatumBis size=10 value=' . date('d.m.Y') . '></td></tr>';
echo '</table>';
echo '</td></tr>';

// CA 27.09.07 - Export - Einsätze nach Filiale als CSV
echo '<tr></tr>';
echo '<tr><td align=center><input type=radio name=txtAuswertung value=3></td><td>Export - Einsatz Filiale</td>';
echo '<td><table border=0>';

echo '<tr><td width=150>Filiale:</td><td><select name=txtFIL_ID>';
echo '<option value=-1 selected>::alle::</option>';

$rsFilialen = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ FROM V_FILIALEN");
$rsFilialenZeilen = $awisRSZeilen;
for($i=0;$i<$rsFilialenZeilen;$i++)
{
	echo '<option value=' . $rsFilialen['FIL_ID'][$i] . '>' . $rsFilialen['FIL_ID'][$i] . ' - ' . $rsFilialen['FIL_BEZ'][$i] . '</option>';
}
echo '</select>';
echo '</td></tr>';
echo '</table>';


echo '</table>';


/******* Schaltflächen *******/

echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png alt='Formularinhalt löschen' name=cmdReset onclick=location.href='./hibe_Main.php?cmdAktion=Auswertungen&Reset=True';>";

echo '</form>';


/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('" . $EingabeFeld . "')[0].focus();";
	echo "\n</Script>";
}
?>