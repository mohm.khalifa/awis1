<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 201110
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('OTT','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht17000=$AWISBenutzer->HatDasRecht(17000);
	if($Recht17000==0)
	{
	    awisEreignis(3,1000,'MBW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./tickets_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_OTTickets'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['OTT']['OTT_BETREFF'].':',190);
	$Form->Erstelle_TextFeld('*OTT_BETREFF',($Param['SPEICHERN']=='on'?$Param['OTT_BETREFF']:''),25,200,true);
	$AWISCursorPosition='sucOTT_BETREFF';
	$Form->ZeileEnde();

/*
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
	$Form->Erstelle_TextFeld('*DATUMVOM',($Param['SPEICHERN']=='on'?$Param['DATUMVOM']:''),25,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',190);
	$Form->Erstelle_TextFeld('*DATUMBIS',($Param['SPEICHERN']=='on'?$Param['DATUMBIS']:''),25,200,true);
	$Form->ZeileEnde();
*/

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['OTT']['OTT_TICKETNR'].':',190);
	$Form->Erstelle_TextFeld('*OTT_TICKETNR',($Param['SPEICHERN']=='on'?$Param['OTT_TICKETNR']:''),25,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['OTT']['OTT_OFO_KEY'].':',190);
	$SQL = 'SELECT DISTINCT OFO_KEY, OFO_BEZEICHNUNG';
	$SQL .= ' FROM OTFORMULARE';
	$SQL .= ' INNER JOIN v_AccountRechte ON OFO_XRC_ID = XRC_ID AND BITAND(XBA_STUFE,OFO_RechteStufe) = OFO_RechteStufe';
	$SQL .= ' ORDER BY OFO_BEZEICHNUNG';
	$Form->Erstelle_SelectFeld('*OTT_OFO_KEY',($Param['SPEICHERN']=='on'?$Param['OTT_OFO_KEY']:''),220,true,$SQL,'','','','',array('~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']));
	$Form->ZeileEnde();


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	if(($Recht17000&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221210");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221211");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>