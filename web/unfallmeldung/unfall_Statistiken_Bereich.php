<?php
require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;
global $StatParam;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('UNF','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort', 'AlleAnzeigen');    
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht21031 = $AWISBenutzer->HatDasRecht(21031);
	if($Recht21031 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
    
    $Seite = 'Bereich';

    $FeldBreiten = array();
    $FeldBreiten['Rang'] = 60;
    $FeldBreiten['Info'] = 320;
    $FeldBreiten['Zahl'] = 120;    

    //************************************************************
	//* Hier nochmal Parameter anzeigen, die als Filter benutzt werden
	//************************************************************
    $Form->Trennzeile('O');
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_FIL_ID'].':', 200);
    $Form->Erstelle_TextFeld('UNF_FIL_ID', $StatParam['STAT_UNF_FilNr'], 5, 200, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['VON'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DAUERVON', $StatParam['STAT_UNF_DatumVon'], 6, 86, false, '', '', '', 'D');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UNF_DATUM'] . ' ' . $AWISSprachKonserven['UNF']['BIS'] . ':', 200);
    $Form->Erstelle_TextFeld('*UNF_DAUERBIS', $StatParam['STAT_UNF_DatumBis'], 6, 70, false, '', '', '', 'D');
    $Form->ZeileEnde();        
    
    $StatParam['STAT_UNF_Bereich'] = $StatParam['STAT_UNF_BereichId'];
    // wenn ein Bereich gew�hlt wurde
    if ($StatParam['STAT_UNF_BereichId'] != $AWISSprachKonserven['Wort']['AlleAnzeigen'])
    {   // Bereichsbezeichnung zum Darstellen holen
        $SQL =  'select uft_key, uft_information ' .
                'from unfallmeldunginfotypen ' .
                'where uft_status = 1 ' .
                'and uft_ueberschrift = 1 ' .
                'and uft_bereichid = ' . $StatParam['STAT_UNF_BereichId'];
        $rsBereich = $DB->RecordSetOeffnen($SQL);
        $StatParam['STAT_UNF_Bereich'] = $rsBereich->FeldInhalt('UFT_INFORMATION');
    }
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEREICH'].':', 200);
    $Form->Erstelle_TextFeld('UNF_BEREICHID', $StatParam['STAT_UNF_Bereich'], 100, 200, false);
    $Form->ZeileEnde();    
    
    $Form->Trennzeile('O');
    
    
    //************************************************************
	//* �berschriftenzeile der Listenansicht
	//************************************************************
    $Form->ZeileStart();    
    // �berschrift der Listenansicht mit Sortierungslink: Rang
    $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite='.$Seite.'&STATSort=rownum'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='rownum'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['RANG'], $FeldBreiten['Rang'], '', $Link);
    // �berschrift der Listenansicht mit Sortierungslink: Info
    $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite='.$Seite.'&STATSort=bezeichnung'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='bezeichnung'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['INFO'], $FeldBreiten['Info'], '', $Link);			
    // �berschrift der Listenansicht mit Sortierungslink: Zahl
    $Link = './unfall_Main.php?cmdAktion=Statistiken&Seite='.$Seite.'&STATSort=anzahl'.((isset($_GET['STATSort']) AND ($_GET['STATSort']=='anzahl'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ZAHL'], $FeldBreiten['Zahl'], '', $Link);
    $Form->ZeileEnde();    
    
    //************************************************************
	//* Haupt-SQL-Zusammenbau-Bereich: Start
	//************************************************************
    $Bedingung = '';        // Var zum Zusammenbauen der Filter
    $Bedingung1 = '';       // Var f�r Bedingung f�r 1:1-Beziehungs-Datens�tze
    $Bedingung2 = '';       // Var f�r Bedingung f�r 1:n-Beziehungs-Datens�tze
    // immer wenn Parameter da, mit and an Bedingungsstring anh�ngen
    if (! empty($StatParam['STAT_UNF_FilNr']))
    {
        $Bedingung .= ' and unf.unf_fil_id = ' . $StatParam['STAT_UNF_FilNr'];
    }
    if (! empty($StatParam['STAT_UNF_DatumVon']))
    {
        $Bedingung .= ' and unf.unf_datum >= ' . $DB->FeldInhaltFormat('D', $StatParam['STAT_UNF_DatumVon']);    
    }
    if (! empty($StatParam['STAT_UNF_DatumBis']))
    {
        $Bedingung .= ' and unf.unf_datum <= ' . $DB->FeldInhaltFormat('D', $StatParam['STAT_UNF_DatumBis']);    
    }
	if ($Bedingung != '')
	{
		$Bedingung1 = ' WHERE ' . substr($Bedingung, 4) . ' ';  // hier: das 1. and durch where ersetzen
        $Bedingung2 = $Bedingung . ' ';                               // hier: pass das 1. and
	}    
    
    // Abfrage der verschiedenen Bereiche �ber switch-Statement gel�st, da sonst der SQL zu abgefahren wird (Wartbarkeit!!!)
    // Abfrage k�nnte auch wie bei 1:n-Beziehung stattfinden (vgl.: default-Pfad) => dann zwar dynamisch
    // aber dann w�rden bei 1:1-Beziehungen die "nicht zugeordneten" Bereiche fehlen
    $SQL = '';
    $SQL .= 'select rownum, sub.* from (';
    $SQL .= '    select count(*) as Anzahl ';
    switch ($StatParam['STAT_UNF_BereichId'])
    {
        case awis_unfall_funktionen::_UnfallBereich :
            $SQL .= '    , unf.uft_bereichid || \' \' || unf.uft_bereich as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_bereichkey, unf.uft_bereichid || \' \' || unf.uft_bereich ';
        break;
        case awis_unfall_funktionen::_Beschaeftigung :
            $SQL .= '    , unf.uft_beschaeftigungid || \' \' || unf.uft_beschaeftigung as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_beschaeftigungkey, unf.uft_beschaeftigungid || \' \' || unf.uft_beschaeftigung ';   
        break;
        case awis_unfall_funktionen::_Art :
            $SQL .= '    , unf.uft_artid || \' \' || unf.uft_art as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_artkey, unf.uft_artid || \' \' || unf.uft_art ';
        break;
        case awis_unfall_funktionen::_UnfallOrt :
            $SQL .= '    , unf.uft_ortid || \' \' || unf.uft_ort as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_ortkey, unf.uft_ortid || \' \' || unf.uft_ort '; 
        break;
        case awis_unfall_funktionen::_Ursache :
            $SQL .= '    , unf.uft_ursacheid || \' \' || unf.uft_ursache as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_ursachekey, unf.uft_ursacheid || \' \' || unf.uft_ursache '; 
        break;
        case awis_unfall_funktionen::_UrsacheTUEV :
            $SQL .= '    , unf.uft_ursachetuevid || \' \' || unf.uft_ursachetuev as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_ursachetuevkey, unf.uft_ursachetuevid || \' \' || unf.uft_ursachetuev ';             
        break;
        case awis_unfall_funktionen::_Kategorie :
            $SQL .= '    , unf.uft_kategorieid || \' \' || unf.uft_kategorie as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_kategoriekey, unf.uft_kategorieid || \' \' || unf.uft_kategorie ';
        break;
        case awis_unfall_funktionen::_UrsacheSpez :
            $SQL .= '    , unf.uft_ursachespezid || \' \' || unf.uft_ursachespez as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_ursachespezkey, unf.uft_ursachespezid || \' \' || unf.uft_ursachespez '; 
        break;
        case awis_unfall_funktionen::_ArtSpez :
            $SQL .= '    , unf.uft_artspezid || \' \' || unf.uft_artspez as Bezeichnung from v_unfallmeldung unf ' . $Bedingung1;
            $SQL .= '    group by unf.uft_artspezkey, unf.uft_artspezid || \' \' || unf.uft_artspez '; 
        break;    
        default:
            // bei 1:n-Beziehungen, anderer Sub-SQL
            if (($StatParam['STAT_UNF_BereichId'] == awis_unfall_funktionen::_Koerperteile)
            or ($StatParam['STAT_UNF_BereichId'] == awis_unfall_funktionen::_Verletzung)
            or ($StatParam['STAT_UNF_BereichId'] == awis_unfall_funktionen::_Ausruestung))
            {
                $SQL .= '    , uft.uft_information as Bezeichnung from unfallmeldunginfos ufi ';
                $SQL .= '    inner join unfallmeldunginfotypen uft on uft.uft_key = ufi.ufi_wert ';
                $SQL .= '    inner join unfallmeldung unf on ufi.ufi_unf_key = unf.unf_key ' . $Bedingung2;
                $SQL .= '    where ufi.ufi_uft_key = ' . $StatParam['STAT_UNF_BereichId'] . ' ';
                $SQL .= '    group by uft.uft_information';                
            }
            else
            {
                // wenn gar kein Bereich gew�hlt ist
                $SQL .= '      from v_unfallmeldung unf ' . $Bedingung1;
            }            
        break;
    }
    $SQL .= '    order by anzahl desc) sub ';
    if (isset($_GET['STATSort']))
    {
        $SQL .= 'order by ' . str_replace('~',' DESC ', $_GET['STATSort']);
    }
    else
    {
        $SQL .= 'order by rownum';
    }

    //************************************************************
	//* Haupt-SQL-Zusammenbau-Bereich: Ende
	//************************************************************    
    $rs = $DB->RecordSetOeffnen($SQL);

    //************************************************************
	//* Listen in Schleife aufbauen
	//************************************************************        
    $DS = 0;	// f�r Hintergrundfarbumschaltung
    $AnzahlGesamt = 0;
    while(! $rs->EOF())
    {
        $Form->ZeileStart();
        $Info = trim($rs->FeldInhalt('BEZEICHNUNG'));       // Trimmen hier wichtig, da FeldInhalt aus KENNUNG + Leerzeichen + BEZEICHNUNG besteht. Wird nicht getrimmt, schl�gt das folgende IF mit empty() nicht an
        $Form->Erstelle_ListenFeld('*RANG', $rs->FeldInhalt('ROWNUM'), 7, $FeldBreiten['Rang'], false, ($DS%2), '', '', 'N0', 'L');            
        if (empty($Info))
        {   // diesem Datensatz ist kein Bereich zugeordnet, dann Sprachkonserve
            $Form->Erstelle_ListenFeld('*INFO', $AWISSprachKonserven['UNF']['NICHTZUGEORDNET'], 50, $FeldBreiten['Info'], false, ($DS%2), 'font-style:italic', '', 'T', 'L');
        }
        else
        {   
            $Form->Erstelle_ListenFeld('*INFO', $Info, 50, $FeldBreiten['Info'], false, ($DS%2), '', '', 'T', 'L');
        }
        $Form->Erstelle_ListenFeld('*ZAHL', $rs->FeldInhalt('ANZAHL'), 10, $FeldBreiten['Zahl'], false, ($DS%2), '', '', 'N0', 'L');		
        $AnzahlGesamt += $rs->FeldInhalt('ANZAHL');     // Gesamtzahl hochaddieren
        $Form->ZeileEnde();
        $DS++;
        $rs->DSWeiter();
    }
    // Gesamtzahl noch hinpinseln
    $Form->ZeileStart();
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['ANZAHLGESAMT'].': '.$AnzahlGesamt, $FeldBreiten['Rang']+$FeldBreiten['Info']+$FeldBreiten['Zahl']+8, 'font-weight:bolder');
    $Form->ZeileEnde();
  
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>