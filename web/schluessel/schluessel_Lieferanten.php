<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

$Rechtestufe = awisBenutzerRecht($con, 1501);
		//  1=Einsehen
		//	2=Hinzuf�gen
		//	4=Bearbeiten
		//  8=L�schen

if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schluessel',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}


$txtSLLKEY=(isset($_REQUEST['txtSLL_KEY'])?$_REQUEST['txtSLL_KEY']:''); //Kein Unterschied ob POST oder GET

if(isset($_POST['cmdTrefferliste_x'])) // Anzeigen der Trefferliste wenn DS hinzugef�gt wurde
{
$txtSLLKEY=0;
}

if(isset($_REQUEST['txtSLL_KEY_loeschen']) && $_REQUEST['txtSLL_KEY_loeschen']!='')
{
		$SQL='SELECT * FROM SCHLUESSELNUMMERN WHERE SLN_SLL_KEY=0'.$_REQUEST['txtSLL_KEY_loeschen'];
		$rsSLL_KEY_LOESCHEN = awisOpenRecordset($con,$SQL);
		$rsSLL_KEY_LOESCHENZeilen = $awisRSZeilen;
		
		if($rsSLL_KEY_LOESCHENZeilen>0)
		{
				echo "<span class=HinweisText>Datensatz kann nicht gel&ouml;scht werden. L&ouml;sen Sie erst die Zuordnung auf.</span>";
		}
		else
		{
				$SQL='DELETE FROM SCHLUESSELLIEFERANTEN WHERE SLL_KEY=0'.$_REQUEST['txtSLL_KEY_loeschen'];
				
				if(awisExecute($con, $SQL)===FALSE)
				{
				awisErrorMailLink("schluessel_Lieferanten.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
				}
		}
}


if(isset($_POST['cmdSpeichern_x'])) //Speichern mit Fallunterschied ob INSERT oder UPDATE
{
		if($txtSLLKEY>0) // UPDATE-Anweisung
		{		
				//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
				//--->
		
				$SQL='SELECT * FROM SCHLUESSELLIEFERANTEN WHERE SLL_KEY='.$txtSLLKEY;
				$rsSchluessellieferanten = awisOpenRecordset($con,$SQL);
				$rsSchluessellieferantenZeilen = $awisRSZeilen;
				
				$SQL='';
				$txtHinweis='';
				
				if($rsSchluessellieferantenZeilen==0)		// Keine Daten
				{
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde gel�scht!</span>");
				}
				
				if($_POST['txtSLL_BEZEICHNUNG'] != $_POST['txtSLL_BEZEICHNUNG_old'])
				{
						if($rsSchluessellieferanten['SLL_BEZEICHNUNG'][0] != $_POST['txtSLL_BEZEICHNUNG_old'])
						{
								$txtHinweis.=',Bezeichnung von '.$_POST['txtSLL_BEZEICHNUNG_old']. ' auf '.$rsSchluessellieferanten['SLL_BEZEICHNUNG'][0];
						}
						else
						{
								$SQL.=',SLL_BEZEICHNUNG=\''.$_POST['txtSLL_BEZEICHNUNG'].'\'';
						}
				}
				
				if($_POST['txtSLL_STRASSE'] != $_POST['txtSLL_STRASSE_old'])
				{
						if($rsSchluessellieferanten['SLL_STRASSE'][0] != $_POST['txtSLL_STRASSE_old'])
						{
								$txtHinweis.=',Strasse von '.$_POST['txtSLL_STRASSE_old']. ' auf '.$rsSchluessellieferanten['SLL_STRASSE'][0];
						}
						else
						{
								$SQL.=',SLL_STRASSE=\''.$_POST['txtSLL_STRASSE'].'\'';
						}
				}
				
				
				if($_POST['txtSLL_PLZ'] != $_POST['txtSLL_PLZ_old'])
				{
						if($rsSchluessellieferanten['SLL_PLZ'][0] != $_POST['txtSLL_PLZ_old'])
						{
								$txtHinweis.=',PLZ von '.$_POST['txtSLL_PLZ_old']. ' auf '.$rsSchluessellieferanten['SLL_PLZ'][0];
						}
						else
						{
								$SQL.=',SLL_PLZ=\''.$_POST['txtSLL_PLZ'].'\'';
						}
				}
				
				if($_POST['txtSLL_ORT'] != $_POST['txtSLL_ORT_old'])
				{
						if($rsSchluessellieferanten['SLL_ORT'][0] != $_POST['txtSLL_ORT_old'])
						{
								$txtHinweis.=',ORT von '.$_POST['txtSLL_ORT_old']. ' auf '.$rsSchluessellieferanten['SLL_ORT'][0];
						}
						else
						{
								$SQL.=',SLL_ORT=\''.$_POST['txtSLL_ORT'].'\'';
						}
				}
				
				if($_POST['txtSLL_TELEFON'] != $_POST['txtSLL_TELEFON_old'])
				{
						if($rsSchluessellieferanten['SLL_TELEFON'][0] != $_POST['txtSLL_TELEFON_old'])
						{
								$txtHinweis.=',TELEFON von '.$_POST['txtSLL_TELEFON_old']. ' auf '.$rsSchluessellieferanten['SLL_TELEFON'][0];
						}
						else
						{
								$SQL.=',SLL_TELEFON=\''.$_POST['txtSLL_TELEFON'].'\'';
						}
				}
				
				if($_POST['txtSLL_FAX'] != $_POST['txtSLL_FAX_old'])
				{
						if($rsSchluessellieferanten['SLL_FAX'][0] != $_POST['txtSLL_FAX_old'])
						{
								$txtHinweis.=',FAX von '.$_POST['txtSLL_FAX_old']. ' auf '.$rsSchluessellieferanten['SLL_FAX'][0];
						}
						else
						{
								$SQL.=',SLL_FAX=\''.$_POST['txtSLL_FAX'].'\'';
						}
				}
				
				if($_POST['txtSLL_EMAIL'] != $_POST['txtSLL_EMAIL_old'])
				{
						if($rsSchluessellieferanten['SLL_EMAIL'][0] != $_POST['txtSLL_EMAIL_old'])
						{
								$txtHinweis.=',EMAIL von '.$_POST['txtSLL_EMAIL_old']. ' auf '.$rsSchluessellieferanten['SLL_EMAIL'][0];
						}
						else
						{
								$SQL.=',SLL_EMAIL=\''.$_POST['txtSLL_EMAIL'].'\'';
						}
				}
				
				if($_POST['txtSLL_BEMERKUNG'] != $_POST['txtSLL_BEMERKUNG_old'])
				{
						if($rsSchluessellieferanten['SLL_BEMERKUNG'][0] != $_POST['txtSLL_BEMERKUNG_old'])
						{
								$txtHinweis.=',BEMERKUNG von '.$_POST['txtSLL_BEMERKUNG_old']. ' auf '.$rsSchluessellieferanten['SLL_BEMERKUNG'][0];
						}
						else
						{
								$SQL.=',SLL_BEMERKUNG=\''.substr($_POST['txtSLL_BEMERKUNG'],0,255).'\'';
						}
				}
				
				
				//<---
				
				// Update- Befehl
				
				if($txtHinweis=='' && $SQL!='')
				{
						
						$SQL.=',SLL_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						
						$SQL.=',SLL_USERDAT=SYSDATE';
				
						$SQL='UPDATE SCHLUESSELLIEFERANTEN SET ' .substr($SQL,1).' WHERE SLL_KEY=0'.$txtSLLKEY;
						
						if(awisExecute($con, $SQL)===FALSE)
						{
							awisErrorMailLink("schluessel_Lieferanten.php", 2, $awisDBFehler);
							awisLogoff($con);
							die();
						}
				}
				elseif($txtHinweis!='')
				{
						echo $txtHinweis;
						awislogoff($con);
						die("<span class=HinweisText>Datensatz wurde von Benutzer ". $rsSchluessellieferanten['SLL_USER'][0] ." ge�ndert !</span>");
				}
		}
		else // INSERT-Anweisung
		{

				if($_POST['txtSLL_BEZEICHNUNG']=='')
				{
						awislogoff($con);
						die("<span class=HinweisText>Sie m�ssen eine Bezeichnung angeben!</span>");
				}
		
				$SQL='INSERT INTO SCHLUESSELLIEFERANTEN (SLL_BEZEICHNUNG,SLL_STRASSE,SLL_PLZ,SLL_ORT,SLL_TELEFON,SLL_FAX,SLL_EMAIL,SLL_BEMERKUNG,SLL_USER,SLL_USERDAT) ';
				$SQL.='VALUES(\''.$_POST['txtSLL_BEZEICHNUNG'].'\',\''.$_POST['txtSLL_STRASSE'].'\',\''.$_POST['txtSLL_PLZ'].'\',\''.$_POST['txtSLL_ORT'].'\',\''.$_POST['txtSLL_TELEFON'].'\',\''.$_POST['txtSLL_FAX'].'\',\''.$_POST['txtSLL_EMAIL'].'\',\''.substr($_POST['txtSLL_BEMERKUNG'],0,255).'\',\''.$AWISBenutzer->BenutzerName().'\',SYSDATE)';
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schluessel_Lieferanten.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
				
				$rsSLL_KEY = awisOpenRecordset($con,'SELECT SEQ_SLL_KEY.CURRVAL AS KEY FROM DUAL');
				
				$txtSLLKEY=$rsSLL_KEY['KEY'][0];
		}
}

if($txtSLLKEY!=0 && ($Rechtestufe&2)==2) // Formular f�r die Bearbeitung der Datens�tze
{
		$SQL='SELECT * FROM SCHLUESSELLIEFERANTEN WHERE SLL_KEY='.$txtSLLKEY;
		
		$rsSchluessellieferanten = awisOpenRecordset($con,$SQL);
		$rsSchluessellieferantenZeilen = $awisRSZeilen;
		
		if($rsSchluessellieferantenZeilen==0)		// Keine Daten
		{
				echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
		}
		else
		{
				echo '<form name=frmLieferanten method=post action=./schluessel_Main.php?cmdAktion=Lieferanten>';
				
				echo "<table  width=100% id=DatenTabelle border=1><tr>";
				echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png accesskey=t name=cmdTrefferliste title='Trefferliste (Alt+T)' onclick=location.href='./schluessel_Main.php?cmdAktion=Lieferanten'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez width=20%>Name</td>";
				echo "<td id=TabellenZeileGrau width=80%><input name=txtSLL_BEZEICHNUNG size=68 value='" . $rsSchluessellieferanten['SLL_BEZEICHNUNG'][0] . "'><input type=hidden	name=txtSLL_BEZEICHNUNG_old  value='" . $rsSchluessellieferanten['SLL_BEZEICHNUNG'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>Strasse</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLL_STRASSE size=50 value='" . $rsSchluessellieferanten['SLL_STRASSE'][0] . "'><input type=hidden name=txtSLL_STRASSE_old  value='" . $rsSchluessellieferanten['SLL_STRASSE'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>PLZ / Ort</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLL_PLZ size=8 value='" . $rsSchluessellieferanten['SLL_PLZ'][0] . "'>&nbsp;<input name=txtSLL_ORT size=50 value='" . $rsSchluessellieferanten['SLL_ORT'][0] . "'></td><input type=hidden name=txtSLL_PLZ_old  value='" . $rsSchluessellieferanten['SLL_PLZ'][0] . "'></td><input type=hidden name=txtSLL_ORT_old  value='" . $rsSchluessellieferanten['SLL_ORT'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>Telefon</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLL_TELEFON size=30 value='" . $rsSchluessellieferanten['SLL_TELEFON'][0] . "'></td><input type=hidden name=txtSLL_TELEFON_old  value='" . $rsSchluessellieferanten['SLL_TELEFON'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>FAX</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLL_FAX size=30 value='" . $rsSchluessellieferanten['SLL_FAX'][0] . "'></td><input type=hidden name=txtSLL_FAX_old  value='" . $rsSchluessellieferanten['SLL_FAX'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez>E-Mail</td>";
				echo "<td id=TabellenZeileGrau><input name=txtSLL_EMAIL size=50 value='" . $rsSchluessellieferanten['SLL_EMAIL'][0] . "'></td><input type=hidden name=txtSLL_EMAIL_old  value='" . $rsSchluessellieferanten['SLL_EMAIL'][0] . "'></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td id=FeldBez valign=top>Bemerkung</td>";
				echo "<td id=TabellenZeileGrau><textarea name=txtSLL_BEMERKUNG cols=50 rows=5>" . $rsSchluessellieferanten['SLL_BEMERKUNG'][0] . "</textarea></td><input type=hidden name=txtSLL_BEMERKUNG_old  value='" . $rsSchluessellieferanten['SLL_BEMERKUNG'][0] . "'></td>";
				echo "</tr>";
				echo "</table>";
				
				echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
				echo "<input type=hidden name=txtSLL_KEY value=".$rsSchluessellieferanten['SLL_KEY'][0].">"; // Aufruf der gleichen Seite nach dem speichern
				echo "</form>";
		
		}
}
elseif((isset($_POST['cmdHinzufuegen_x']) && $_POST['cmdHinzufuegen_x']==TRUE)  && ($Rechtestufe&4)==4) // Formular f�r das Hinzuf�gen von Datens�tzen
{
		echo '<form name=frmLieferanten method=post action=./schluessel_Main.php?cmdAktion=Lieferanten>';
		
		echo "<table  width=100% id=DatenTabelle border=1><tr>";
		echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png accesskey=t name=cmdTrefferliste title='Trefferliste (Alt+T)' onclick=location.href='./schluessel_Main.php?cmdAktion=Lieferanten'></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez width=20%>Name</td>";
		echo "<td id=TabellenZeileGrau width=80%><input name=txtSLL_BEZEICHNUNG size=68></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Strasse</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLL_STRASSE size=50></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>PLZ / Ort</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLL_PLZ size=8>&nbsp;<input name=txtSLL_ORT size=50></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>Telefon</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLL_TELEFON size=30></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>FAX</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLL_FAX size=30></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez>E-Mail</td>";
		echo "<td id=TabellenZeileGrau><input name=txtSLL_EMAIL size=50></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td id=FeldBez valign=top>Bemerkung</td>";
		echo "<td id=TabellenZeileGrau><textarea name=txtSLL_BEMERKUNG cols=50 rows=5></textarea></td>";
		echo "</tr>";
		echo "</table>";
					
		echo "<br><hr><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
		echo "<input type=hidden name=txtSLL_KEY value=0>";
		echo "</form>";
}
else // Formular f�r die Ausgabe der Lieferanten-Liste
{

		echo '<form name=frmLieferanten method=post action=./schluessel_Main.php?cmdAktion=Lieferanten>';
		
		if(($Rechtestufe&4)==4) // Falls Recht ADD-Button hinzuf�gen
		{
				echo "<input type=image border=0 src=/bilder/plus.png accesskey=n name=cmdHinzufuegen title='Hinzuf�gen (Alt+N)' onclick=location.href='./schluessel_Main.php?cmdAktion=Lieferanten'>";
				echo "<hr>";
		}
		
		$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
		
		$SQL='SELECT * FROM SCHLUESSELLIEFERANTEN ORDER BY SLL_BEZEICHNUNG ASC';
		
		$rsSchluessellieferanten = awisOpenRecordset($con,$SQL);
		$rsSchluessellieferantenZeilen = $awisRSZeilen;
		
		if($rsSchluessellieferantenZeilen==0)		// Keine Daten
		{
			echo "<span class=HinweisText>Es wurde kein Eintrag gefunden.</span>";
		}
		else
		{
			// �berschrift aufbauen
			echo "<table  width=100% id=DatenTabelle border=1><tr>";
			
			echo "<td id=FeldBez>Name</td>";
			echo "<td id=FeldBez>Strasse</td>";
			echo "<td id=FeldBez>PLZ</td>";
			echo "<td id=FeldBez>Ort</td>";
			echo "<td id=FeldBez>Telefon</td>";
			echo "<td id=FeldBez>FAX</td>";
			echo "<td id=FeldBez>E-Mail</td>";
			echo "<td id=FeldBez>Bemerkung</td>";
			if(($Rechtestufe&8)==8){
				echo "<td id=FeldBez></td>";}
			echo "</tr>";
			
			for($i=0;$i<$rsSchluessellieferantenZeilen;$i++)
			{
				if($i<$AwisBenuPara)
				{
					echo '<tr>';
					if(($Rechtestufe&2)==2) // Link f�r Berabeitung setzen
					{
							echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=./schluessel_Main.php?cmdAktion=Lieferanten&txtSLL_KEY=' . $rsSchluessellieferanten['SLL_KEY'][$i] . '>' . $rsSchluessellieferanten['SLL_BEZEICHNUNG'][$i] . '</a></td>';
					}
					else
					{
							echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_BEZEICHNUNG'][$i] . '</td>';
					}
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_STRASSE'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_PLZ'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_ORT'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_TELEFON'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_FAX'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_EMAIL'][$i] . '</td>';
					echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsSchluessellieferanten['SLL_BEMERKUNG'][$i] . '</td>';
					if(($Rechtestufe&8)==8){
							echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><a href='./schluessel_Main.php?cmdAktion=Lieferanten&txtSLL_KEY_loeschen=". $rsSchluessellieferanten['SLL_KEY'][$i] ."'><img border=0 src=/bilder/muelleimer.png name=cmdLoeschen title='L&ouml;schen'></a></td>";
					}
					echo '</tr>';
				}				
			}
				
			print "</table>";
			
			if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
			}
			else
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
			}
		}
		
		echo '</form>';
}

?>
