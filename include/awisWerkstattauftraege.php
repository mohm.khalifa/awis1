<?php
require_once('awisINFO.php');
require_once('db.inc.php');
require_once('awis_forms.inc.php');

/**
 * Klasse zum Verabeiten von Werkstattauftraegen
 *
 * @author Sacha Kerres
 * @version 20070926
 * @copyright ATU Auto Teile Unger
 */
class awisWerkstattAuftraege
{
	/**
	 * Datenbank Verbindung
	 *
	 * @var ressource
	 */
	private $_con = null;



	public function __construct()
	{
		$this->_con = awisLogon();
	}


	public function __destruct()
	{
		if(!is_null($this->_con))
		{
			awisLogoff($this->_con);
		}
	}

	/**
	 * Importiert alle WA Auftraege, die von der Filiale kommen
	 *
	 * @author Sacha Kerres
	 * @version 20070926
	 *
	 */
	public function ImportiereWAAuftrage()
	{
		global $awisRSZeilen;


		echo ' Importiere...'.PHP_EOL;
		// Infos aus der Konfigurationsdatei lesen
		$Info = new awisINFO();
		$Pfad = $Info->LeseAWISParameter('werkstattauftrag.conf','PFAD_WADATEIEN');
		$DateiMuster = $Info->LeseAWISParameter('werkstattauftrag.conf','DATEI_MUSTER');
		$AnzahlFelder = $Info->LeseAWISParameter('werkstattauftrag.conf','ANZAHL_FELDER');
		if(!is_dir($Pfad))
		{
			throw new Exception('Kann das Verzeichnis '.$Pfad.' nicht oeffnen.',200709260949);
		}

		echo '   --> '.$Pfad.PHP_EOL;

		$Verzeichnis = new DirectoryIterator($Pfad);
		foreach ($Verzeichnis AS $Datei)
		{
			if(preg_match('/'.$DateiMuster.'/',$Datei))
			{
				echo 'Datei '.$Datei.' passt.'.PHP_EOL;

				$Datei = $Pfad.'/'.$Datei;

				$Pruefsumme = md5_file($Datei);
				
				$BindeVariablen=array();
                $BindeVariablen['var_T_xdi_bereich']='WA';
                $BindeVariablen['var_T_xdi_md5']=$Pruefsumme;

				$SQL = 'SELECT *';
				$SQL .= ' FROM ImportProtokoll';
				$SQL .= ' WHERE XDI_Bereich = :var_T_xdi_bereich';
				$SQL .= ' AND XDI_MD5 = :var_T_xdi_md5';
				$rsXDI = awisOpenRecordset($this->_con,$SQL,true,false,$BindeVariablen);
				$rsXDIZeilen = $awisRSZeilen;
				
				if($rsXDIZeilen>0)
				{
					echo '-->Schon importiert.'.PHP_EOL;
				}
				else
				{
					echo '-->Noch nicht importiert.'.PHP_EOL;
					
					$BindeVariablen=array();
                	$BindeVariablen['var_T_xdi_bereich']='WA';
                	$BindeVariablen['var_T_xdi_dateiname']=$Datei;
                	$BindeVariablen['var_T_xdi_md5']=$Pruefsumme;

					$SQL = 'INSERT INTO ImportProtokoll';
					$SQL .= '(XDI_Bereich,XDI_Dateiname,XDI_Datum,XDI_MD5)';
					$SQL .= ' VALUES(:var_T_xdi_bereich, :var_T_xdi_dateiname, SYSDATE, :var_T_xdi_md5)';
					if(awisExecute($this->_con,$SQL,true,false,0,$BindeVariablen)===false)
					{
						throw new Exception('Speicherfehler bei '.$SQL,200709271534);
					}
					else
					{
						$SQL = 'SELECT seq_XDI_KEY.CurrVal AS KEY FROM DUAL';
						$rsKey = awisOpenRecordset($this->_con,$SQL);
						$XDIKEY=$rsKey['KEY'][0];
					}

					if($fp = fopen($Datei,'r'))
					{
						$Zeile = fgetcsv($fp,null,';');
						while($Zeile!==false)
						{
							if(sizeof($Zeile)<$AnzahlFelder-2)
							{
								echo '--->Anzahl der Zeilen in der Datei stimmt nicht. Erwarte '.$AnzahlFelder.', habe '.sizeof($Zeile).PHP_EOL;
							}
							else
							{
								$WANr = $Zeile[0];
								if(strlen($WANr)==6)	// Alte Nummer
								{
									$WANrMit = " ((trim(to_char(".trim($Zeile[1]).",'0999')) ";
									$WANrMit  .= "  || trim(to_char(".$WANr.",'099999')) ";
									$WANrMit  .= " || pruefziffer_2of5il(trim(to_char(".trim($Zeile[1]).",'0999')) ";
									$WANrMit  .= " || trim(to_char(".$WANr.",'099999'))))) ";
									$WANr = $WANrMit;
								}

								$SQL = 'INSERT INTO ZukaufWerkstattAuftraege';
								$SQL .= ' (ZWA_WANr,ZWA_FIL_ID,ZWA_KBANR,ZWA_KTyp,ZWA_FahrgestellNr,ZWA_ATUNummer';
								$SQL .= ',ZWA_Bezeichnung,ZWA_Menge,ZWA_VK,ZWA_Datum,ZWA_Umsatz,ZWA_Nachlass';
								$SQL .= ',ZWA_User,ZWA_UserDat) VALUES (';
								$SQL .= '  '.$WANr;								// WA-Nr
								$SQL .= ', '.awisFeldFormat('Z',$Zeile[1]);		// Filiale
								$SQL .= ', '.awisFeldFormat('T',$Zeile[2]);		// KBA
								$SQL .= ', '.awisFeldFormat('T',$Zeile[3]);		// KTyp
								$SQL .= ', '.awisFeldFormat('T',$Zeile[4]);		// FahrgestellNr
								$SQL .= ', '.awisFeldFormat('T',$Zeile[5]);		// Sortiment
								$SQL .= ', '.awisFeldFormat('T',($Zeile[6]));		// Bezeichnung
								$SQL .= ', '.awisFeldFormat('N',$Zeile[7]);		// Menge
								$SQL .= ', '.awisFeldFormat('N2',$Zeile[8]);	// VK
								$SQL .= ', '.awisFeldFormat('D',$Zeile[9]);		// WA Datum
								$SQL .= ', '.awisFeldFormat('N2',$Zeile[11]);		// Umsatz
								$SQL .= ', '.awisFeldFormat('N2',$Zeile[12]);		// Nachlass
								$SQL .= ', '.awisFeldFormat('T',$XDIKEY);		// ImportID
								$SQL .= ', SYSDATE';
								$SQL .= ')';

								$SQL = ($SQL);

								if(awisExecute($this->_con,$SQL)===false)
								{
									throw new Exception('Fehler beim Speichern:'.$SQL,200709271032);
									echo 'Fehler beim Speichern:';
								}
							}
							$Zeile = fgetcsv($fp,null,';');
						}
					}
					else
					{
						throw new Exception('Kann Datei '.$Pfad.' nicht �ffnen',200709271026);
					}
				}
			}
		}
	}
	/**
	 * Ordnet einem WA in einer Filiale eine Bestellung zu
	 *
	 * @param int $Filiale
	 * @param int $WAuftrag
	 */
	public function BestellungenZuordnen($Filiale='', $WAuftrag='')
	{
		global $awisRSZeilen;

		$SQL = 'SELECT *';
		$SQL .= ' FROM ZUKAUFWERKSTATTAUFTRAEGE';
		$SQL .= ' WHERE ZWA_ZUB_KEY IS NULL';
		$SQL .= ' AND ZWA_DATUM >= TO_DATE(\'06.11.2007\',\'DD.MM.RRRR\')';
		if($WAuftrag!='')
		{
			$SQL .= ' AND ZWA_WANR = '.awis_FeldInhaltFormat('T',$WAuftrag);
		}
		$SQL .= ' ORDER BY ZWA_FIL_ID, ZWA_USERDAT';

		$rsZWA = awisOpenRecordset($this->_con,$SQL);
		$rsZWAZeilen = $awisRSZeilen;

		echo 'Habe '.$rsZWAZeilen . ' offene WAs...'.PHP_EOL;

		for($ZWAZeile=0;$ZWAZeile<$rsZWAZeilen;$ZWAZeile++)
		{
			//echo $rsZWA['ZWA_BEZEICHNUNG'][$ZWAZeile].PHP_EOL;

			if(strpos($rsZWA['ZWA_BEZEICHNUNG'][$ZWAZeile],'~')!==false)
			{
				echo 'Habe einen Text mit Nummer.'.PHP_EOL;
				$KEY = intval(substr($rsZWA['ZWA_BEZEICHNUNG'][$ZWAZeile],strpos($rsZWA['ZWA_BEZEICHNUNG'][$ZWAZeile],'~')+1));

				$BindeVariablen=array();
				$BindeVariablen['var_N0_zub_key']=$KEY;
				$BindeVariablen['var_N0_zwa_key']=$rsZWA['ZWA_KEY'][$ZWAZeile];
				
				$SQL = 'UPDATE ZUKAUFWERKSTATTAUFTRAEGE';
				$SQL .= ' SET ZWA_ZUB_KEY=(SELECT ZUB_KEY FROM ZUKAUFBestellungen WHERE ZUB_KEY=:var_N0_zub_key)';
				$SQL .= ' WHERE ZWA_KEY=:var_N0_zwa_key';

				if(awisExecute($this->_con,$SQL,true,false,0,$BindeVariablen)===false)
				{
					throw new Exception('Fehler beim Zuweisen der Bestellung zum Werkstattauftrag.',200716101503);
				}
			}
			else
			{
				echo 'Suche in den alten Auftraegen/Bestellungen nach '.$rsZWA['ZWA_WANR'][$ZWAZeile].PHP_EOL;

				$BindeVariablen=array();
				$BindeVariablen['var_N0_fil_id']=$rsZWA['ZWA_FIL_ID'][$ZWAZeile];
				$BindeVariablen['var_T_zwa_wanr']=$rsZWA['ZWA_WANR'][$ZWAZeile];
				
				$SQL = 'SELECT MIN(ZWA_KEY) AS Key, COUNT(*) AS Anz';
				$SQL .= ' FROM ZUKAUFWERKSTATTAUFTRAEGE';
				$SQL .= ' WHERE ZWA_FIL_ID=:var_N0_fil_id';
				$SQL .= ' AND ZWA_WANR=:var_T_zwa_wanr';
				$SQL .= ' AND ZWA_ATUNUMMER = \'ONL001\'';
				$rsDAT = awisOpenRecordset($this->_con,$SQL,true,false,$BindeVariablen);
				if($awisRSZeilen==1 AND $rsDAT['ANZ'][0]==1)		// Nur eine ONL-Position
				{
					echo 'Habe nur 1 Position. Suche eine Bestellung.'.PHP_EOL;

					$BindeVariablen=array();
					$BindeVariablen['var_T_zub_wanr']=$rsZWA['ZWA_WANR'][$ZWAZeile];
					$BindeVariablen['var_N0_fil_id']=$rsZWA['ZWA_FIL_ID'][$ZWAZeile];					
					
					$SQL = 'SELECT ZUB_KEY';
					$SQL .= ' FROM ZukaufBestellungen ';
					$SQL .= ' WHERE ZUB_WANR = :var_T_zub_wanr';
					$SQL .= ' AND ZUB_FIL_ID = :var_N0_fil_id';

					$rsZUB = awisOpenRecordset($this->_con,$SQL,true,false,$BindeVariablen);
					if($awisRSZeilen==1)		// einen Eintrag gefunden
					{
						echo '-->Gefunden. Key:'.$rsZUB['ZUB_KEY'][0].PHP_EOL;

						$BindeVariablen=array();
						$BindeVariablen['var_N0_zwa_zub_key']=$rsZUB['ZUB_KEY'][0];
						$BindeVariablen['var_N0_zwa_key']=$rsDAT['KEY'][0];
						
						$SQL = 'UPDATE ZUKAUFWERKSTATTAUFTRAEGE';
						$SQL .= ' SET ZWA_ZUB_KEY=:var_N0_zwa_zub_key';
						$SQL .= ' WHERE ZWA_KEY=:var_N0_zwa_key';

						if(awisExecute($this->_con,$SQL,true,false,0,$BindeVariablen)===false)
						{
							throw new Exception('Fehler beim Zuweisen der Bestellung zum Werkstattauftrag.',200716101503);
						}
					}
					elseif($awisRSZeilen==0)
					{
						echo ' -->Keinen Datensatz.'.PHP_EOL;
					}
				}
			}
		}
	}
}
