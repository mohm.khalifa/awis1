<?php
/**
 * Detailmaske f�r die Auswahl eines Datenexportes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 201405
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Param;


require_once('datenexporte_erstelleabfrage.php');

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XDE','%');
	$TextKonserven[]=array('XAB','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Feldname');
	$TextKonserven[]=array('Wort','Bedingung');
	$TextKonserven[]=array('Wort','EinmalAbfrage');
	$TextKonserven[]=array('Wort','GespeicherteAbfrage');
	$TextKonserven[]=array('Wort','BedingungHinzufuegen');
	$TextKonserven[]=array('Wort','NeueAbfrage');
	$TextKonserven[]=array('Wort','Ausgabe');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','DateiOeffnenLink');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Liste','txtVerknuepfung');
	$TextKonserven[]=array('Liste','Bedingungen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','AbfrageTeilen');
			
    $Form->DebugAusgabe(2,$_POST);
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht4001=$AWISBenutzer->HatDasRecht(4001);
	if($Recht4001==0)
	{
		awisEreignis(3,1000,'XDA',$AWISBenutzer->BenutzerName(),'','','');
		echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	$Form->SchreibeHTMLCode('<form name="frmDatenexport" method="post" action="./datenexporte_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').'"');
	
	/**********************************************
	 * * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Datenexporte'));


	$AWIS_KEY1 = 0;

	if(isset($_POST['cmdSpeichern_x']))
	{
		include './datenexporte_speichern.php';
	}
	elseif(isset($_GET['Del']) OR isset($_POST['cmdLoeschen_x']) OR isset($_POST['cmdLoeschenOK']))
	{
		include './datenexporte_loeschen.php';
	}
	$Bedingung = '';

	// Neue Abfrage auf Basis einer Vorlage erstellen
	if((isset($_POST['sucXDE_KEY']) AND $_POST['sucXDE_KEY']!=0))
	{
		$Bedingung .= ' AND XDE_KEY = '.$DB->WertSetzen('XDE','N0',$_POST['sucXDE_KEY']);
		$AWIS_KEY1 = -1;
		$XDE_KEY = $_POST['sucXDE_KEY'];
	}
	elseif((isset($_POST['sucXAB_KEY']) AND $_POST['sucXAB_KEY']!=0))	// Gespeicherte Abfrage
	{
		$Bedingung .= ' AND XAB_KEY = '.$DB->WertSetzen('XDE','N0',$_POST['sucXAB_KEY']);
		$AWIS_KEY1 = intval($_POST['sucXAB_KEY']);
		$XDE_KEY = 0;
	}
	else
	{
				// Wenn beim L�schen abgebrochen wird
		if(isset($_POST['txtKey']) AND isset($_POST['cmdLoeschenAbbrechen']))
		{
			$Bedingung .= ' AND XAB_KEY = '.$DB->WertSetzen('XDE','N0',$_POST['txtKey']);
			$AWIS_KEY2 = $_POST['txtKey'];
		}
		elseif($AWIS_KEY1!=0)
		{
			$Bedingung .= ' AND XAB_KEY = '.$DB->WertSetzen('XDE','N0',$AWIS_KEY1);
		}
		elseif($AWIS_KEY2!=0)
		{
			$Bedingung .= ' AND XDE_KEY = '.$DB->WertSetzen('XDE','N0',$AWIS_KEY2);
		}
		else
		{
			$AWIS_KEY1 = $Param['KEY'];
			$Bedingung .= ' AND XAB_KEY = '.$DB->WertSetzen('XDE','N0',$AWIS_KEY1);
		}
	}
	
	$SQL = 'SELECT * ';
	$SQL .= ' FROM ExportDaten ';
	$SQL .= ' LEFT OUTER JOIN ExportAbfragen ON XAB_XDE_KEY = XDE_KEY AND XAB_KEY = '.$DB->WertSetzen('XDE','N0',$AWIS_KEY1);
	$SQL .= ' INNER JOIN V_ACCOUNTRECHTE ON XDE_XRC_ID = XBA_XRC_ID AND BITAND(XBA_STUFE,XDE_STUFE)=XDE_STUFE';
	$SQL .= ' WHERE XBL_LOGIN = '.$DB->WertSetzen('XDE','T',strtoupper($AWISBenutzer->BenutzerName())).'';
	$SQL .= ' AND XDE_STATUS = \'A\'';
	$SQL .= $Bedingung;
	
	
	$rsXDE = $DB->RecordSetOeffnen($SQL, $DB->BindeVariablen('XDE'));

	if($rsXDE->EOF())
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten'],awisFormular::HINWEISTEXT_HINWEIS);
	}
	else
	{
		$Param['KEY'] = $AWIS_KEY1 = $rsXDE->FeldInhalt('XAB_KEY');
		$Param['XDE_KEY'] = $XDE_KEY = $rsXDE->FeldInhalt('XDE_KEY');
	
		$Form->SchreibeHTMLCode('<form name="frmDatenexporte" method="post" action="./datenexporte_Main.php?cmdAktion=Details">');
	
		$Form->Formular_Start();
	
		$Form->Erstelle_HiddenFeld('XDE_KEY', $XDE_KEY);
		$Form->Erstelle_HiddenFeld('XAB_KEY', $AWIS_KEY1);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XDE']['XDE_BEZEICHNUNG'].':',190,'');
		$Form->Erstelle_TextFeld('XDE_BEZEICHNUNG',$rsXDE->FeldInhalt('XDE_BEZEICHNUNG'),1,0,false);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XDE']['XDE_BESCHREIBUNG'].':',190,'');
		$Form->Erstelle_Textarea('XDE_BESCHREIBUNG',$rsXDE->FeldInhalt('XDE_BESCHREIBUNG'),0,100,3,false);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XAB']['XAB_BEZEICHNUNG'].':',190,'');
		$Form->Erstelle_TextFeld('XAB_BEZEICHNUNG',$rsXDE->FeldInhalt('XAB_BEZEICHNUNG'),50,300,true);
		$AWISCursorPosition = 'txtXAB_BEZEICHNUNG';
		$Form->ZeileEnde();

		if($AWIS_KEY1>=1)
		{
			if(($Recht4001&4)==4)
			{					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['XAB']['XAB_UEBERSCHRIFT'].':',190,'');
				$Liste = explode('|',$AWISSprachKonserven['XAB']['XAB_LST_UEBERSCHRIFT']);
				$Form->Erstelle_SelectFeld('XAB_UEBERSCHRIFT',$rsXDE->FeldInhalt('XAB_UEBERSCHRIFT'),100,true,'','',0,'','',$Liste);
				$Form->ZeileEnde();
			}
			
			if(($Recht4001&8)==8)
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['XAB']['XAB_TRENNZEICHEN_CSV'].':',190,'');
				$Form->Erstelle_TextFeld('!XAB_TRENNZEICHEN_CSV', $rsXDE->FeldInhalt('XAB_TRENNZEICHEN_CSV'), 11, 100,true);
				
				$Form->ZeileEnde();
			}

            $Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XAB']['XAB_VORSCHAU'].':',190,'');
			$Liste = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
			$Form->Erstelle_SelectFeld('XAB_VORSCHAU',$rsXDE->FeldInhalt('XAB_VORSCHAU'),100,true,'','',0,'','',$Liste);
			$Form->ZeileEnde();

			$SQL = 'SELECT XAN_XBN_KEY, KON_NAME1, KON_NAME2 FROM ExportAbfragenBenutzer INNER JOIN BENUTZER ';
			$SQL .= 'ON XAN_XBN_KEY = XBN_KEY ';
			$SQL .= 'INNER JOIN KONTAKTE ';
			$SQL .= 'ON XBN_KON_KEY = KON_KEY ';
			$SQL .= 'WHERE XAN_XAB_KEY = ' . $DB->WertSetzen('XAN','N0',$AWIS_KEY1);

			$rsXAN = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('XAN'));

			$arrXBN = [];

			while (!$rsXAN->EOF()) {
			    $arrXBN[] = $rsXAN->FeldInhalt('XAN_XBN_KEY');
			    $rsXAN->DSWeiter();
            }

            $Form->ZeileStart('', 'xbndiv');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AbfrageTeilen'], 190);
            $Form->Erstelle_MehrfachSelectFeld('XBN_KEY',  $arrXBN, '350:350', true, 'SELECT XBN_KEY, KON_NAME1||\' \'||KON_NAME2 FROM KONTAKTE INNER JOIN BENUTZER ON KON_KEY = XBN_KON_KEY WHERE KON_STATUS = \'A\' ORDER BY KON_NAME1', '', '', '', '', '', '', '', [], '', 'AWIS', '', '', 50);
            $Form->ZeileEnde();

        }
		
		$Form->Formular_Ende();
		
		if($AWIS_KEY1 > 0)
		{
			$Reg = new awisRegister(4005);
			$Reg->ZeichneRegister(isset($_GET['Seite'])?$_GET['Seite']:'');
		}
		else 
		{
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S','',27,27);
			$Form->SchaltflaechenEnde();
		}

		$Form->Formular_Ende();
		echo '</form>';
		$Form->SetzeCursor($AWISCursorPosition);

		$AWISBenutzer->ParameterSchreiben('Formular_Datenexporte',serialize($Param));
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(2, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221210");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221211");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>