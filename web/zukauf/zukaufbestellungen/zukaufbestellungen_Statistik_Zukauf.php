<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
include ("jpgraph/jpgraph.php");
include ("jpgraph/jpgraph_line.php");

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();


for($i=0;$i<=53;$i++)
{
	$Daten['1742'][$i]=0;
	$Daten['8164'][$i]=0;
	$Daten['2415'][$i]=0;
	$Daten['0255'][$i]=0;
	$Daten['8219'][$i]=0;
	$Daten['9157'][$i]=0;
	$Daten['2620'][$i]=0;
	$Daten['8252'][$i]=0;
}

$Jahr = (isset($_GET['Jahr'])?$DB->FeldInhaltFormat('N0',$_GET['Jahr']):date('Y'));

$SQL = "select to_char(ZUB_Bestelldatum,'WW') AS Woche, zub_lie_nr, count(*) AS Anzahl";
$SQL .= ' from ZUKAUFBESTELLUNGEN';
$SQL .= ' WHERE ZUB_BESTELLDATUM >= TO_DATE(\'01.01.'.$Jahr.'\',\'DD.MM.YYYY\')';
$SQL .= ' AND ZUB_BESTELLDATUM < TO_DATE(\'01.01.'.($Jahr+1).'\',\'DD.MM.YYYY\')';
$SQL .= ' ';
$SQL .= " group by zub_lie_nr, to_char(ZUB_Bestelldatum,'WW')";
$SQL .= ' order by 1,2';

$rsSTAT = $DB->RecordSetOeffnen($SQL);
$Woche = 0;
while(!$rsSTAT->EOF())
{
	$Daten[$rsSTAT->FeldInhalt('ZUB_LIE_NR')][(int)$rsSTAT->FeldInhalt('WOCHE')-1]=$rsSTAT->FeldInhalt('ANZAHL');

	$rsSTAT->DSWeiter();
}

$graph = new Graph(1000,400,"auto");
$graph->SetScale("textlin");
$graph->SetShadow();
$graph->img->SetMargin(60,30,20,40);

$p1 = new LinePlot($Daten['1742']);
$p2 = new LinePlot($Daten['8164']);
$p3 = new LinePlot($Daten['2415']);
$p4 = new LinePlot($Daten['0255']);
$p5 = new LinePlot($Daten['8219']);
$p6 = new LinePlot($Daten['9157']);
$p7 = new LinePlot($Daten['2620']);
$p8 = new LinePlot($Daten['8252']);

$p1->SetLegend ("Hess");
$p2->SetLegend ("Wuetschner");
$p3->SetLegend ("PV");
$p4->SetLegend ("TROST");
$p5->SetLegend ("KSM-DE");		// 8219
$p6->SetLegend ("KSM-AT");		// 9157
$p7->SetLegend ("Birner-AT");		// 2620
$p8->SetLegend ("Werthenbach");		// 8252

$graph->title->Set("Zuk�ufe gesamt in ".$Jahr);
$graph->xaxis->title->Set("Woche");
$graph->yaxis->title->Set("Bestellpositionen");
$graph->yaxis->SetTitleMargin(40);

$ap = new AccLinePlot(array($p1, $p2,$p3,$p4,$p5,$p6,$p7,$p8));

// Add the accumulated line plot to the graph
$graph->Add( $ap);

$p1->SetFillColor ("red");
$p2->SetFillColor( "blue");
$p3->SetFillColor( "green");
$p4->SetFillColor( "yellow");
$p5->SetFillColor( "orange");
$p6->SetFillColor( "brown");
$p7->SetFillColor( "lightgray");
$p8->SetFillColor( "silver");

$graph ->legend->Pos( 0.02,0.05,"right","top");


$SQL = 'select count(*) AS Anzahl, to_char(FRK_DATUM,\'WW\') AS Woche';
$SQL .= ' from fremdkaeufe';
$SQL .= ' inner join filialen on frk_fil_id = fil_id';
$SQL .= ' WHERE fil_lan_wwskenn = \'BRD\'';
//$SQL .= ' WHERE (fil_lan_wwskenn = \'BRD\' OR fil_lan_wwskenn = \'OES\')';
$SQL .= ' AND FRK_DATUM >= TO_DATE(\'01.01.'.$Jahr.'\',\'DD.MM.YYYY\')';
$SQL .= ' AND FRK_DATUM < TO_DATE(\'01.01.'.($Jahr+1).'\',\'DD.MM.YYYY\')';
$SQL .= ' and frk_grund is null';
$SQL .= ' group BY to_char(FRK_DATUM,\'WW\')';
$SQL .= ' order by 2';

$rsSTAT = $DB->RecordSetOeffnen($SQL);
$Woche = 0;
$Daten = array();
while(!$rsSTAT->EOF())
{
	$Daten[]=$rsSTAT->FeldInhalt('ANZAHL');
	$rsSTAT->DSWeiter();
}
if(sizeof($Daten)==0)
{
	$Daten = array(0);
}

$l1plot=new LinePlot($Daten);
$l1plot->SetColor("black");
$l1plot->SetWeight(2);
$l1plot->SetLegend("Fremdk�ufe");

$graph->Add($l1plot);

$graph->Stroke();
?>