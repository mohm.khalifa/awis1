<?php
require_once 'PHPExcel.php';
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 05.12.2016
 * Time: 10:31
 */
class awisExcel
{

    const DateiFormat_XLS = 'Excel5';

    const DateiFormat_XLSX = 'Excel2007';

    const DateiFormat_CSV = 'CSV';

    function __construct()
    {
    }

    public function Excel2CSV($ExcelDateiPfad, $DateiFormat = self::DateiFormat_XLS, $CSVDatei, $Tabellenblatt='',$Trennzeichen=';',$Textbegrenzer=''){
        $objReader = PHPExcel_IOFactory::createReader($DateiFormat);
        $objPHPExcelReader = $objReader->load($ExcelDateiPfad);

        $Tabellenblaetter = $objPHPExcelReader->getSheetNames();

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelReader, self::DateiFormat_CSV);
        $objWriter->setDelimiter($Trennzeichen);
        $objWriter->setEnclosure($Textbegrenzer);

        if($Tabellenblatt===''){ //Kein Tabellenblatt angegeben? --> Alle einzeln
            $Datei = explode('/',$CSVDatei);
            $Datei = $Datei[count($Datei)-1]; //Letztes Element im Array ist der Dateiname
            $Pfad = str_replace($Datei,'',$CSVDatei); //Dateinamen ersetzen damit man den Pfad hat

            foreach($Tabellenblaetter as $Index => $Tabellenblattname) {
                $objWriter->setSheetIndex($Index);
                $objWriter->save(($Pfad. $Tabellenblattname . '_'.$Datei));
            }
        }else{
            if(!is_numeric($Tabellenblatt)){ //Name angegeben? Schauen was er f�r ein Index hat
                $Index = array_search($Tabellenblatt,$Tabellenblaetter);
                if($Index===false){
                    return false;
                }
            }else{
                $Index = $Tabellenblatt;
            }

            $objWriter->setSheetIndex($Index);

            $objWriter->save($CSVDatei);
        }

        return true;

    }

}