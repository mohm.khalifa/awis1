<?php
global $GUT;
global $AWIS_KEY1;
try {
    if ($AWIS_KEY1==-1) { //Neuer DS
        $SQL  ='INSERT';
        $SQL .=' INTO GUTSCHEINAKTION';
        $SQL .='   (';
        $SQL .='     GUT_VORGANGSNUMMER,';
        $SQL .='     GUT_BETRAG,';
        $SQL .='     GUT_NACHNAME,';
        $SQL .='     GUT_VORNAME,';
        $SQL .='     GUT_STRASSE,';
        $SQL .='     GUT_PLZ,';
        $SQL .='     GUT_BIC,';
        $SQL .='     GUT_IBAN,';
        $SQL .='     GUT_GUTSCHEINNR,';
        $SQL .='     GUT_FIL_ID,';
        $SQL .='     GUT_ORT,';
        $SQL .='     GUT_KREDITINSITUT,';
        $SQL .='     GUT_IBAN_LAND,';
        $SQL .='     GUT_STATUS,';
        $SQL .='     GUT_USER,';
        $SQL .='     GUT_USERDAT';
        $SQL .='   )';
        $SQL .='   VALUES';
        $SQL .='   (';
        $SQL .= $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_VORGANGSNUMMER'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','N2',$_POST['txtGUT_BETRAG'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_NACHNAME'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_VORNAME'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_STRASSE'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_PLZ'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',@$_POST['txtBLZ_BIC'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['sucGUT_IBAN'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_GUTSCHEINNR'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_FIL_ID'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_ORT'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',@$_POST['txtBLZ_BEZEICHNUNG'],false);
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',@$_POST['txtGUT_IBAN_LAND'],false);
        $SQL .=',     1';
        $SQL .=','.$GUT->DB->WertSetzen('GUT','T',$GUT->AWISBenutzer->BenutzerName());
        $SQL .=',     sysdate';
        $SQL .='   )';

        try{
            $GUT->DB->Ausfuehren($SQL, '', true, $GUT->DB->Bindevariablen('GUT'));
            $SQL = 'SELECT seq_GUT_KEY.CurrVal AS KEY FROM DUAL';
            $rsKey = $GUT->DB->RecordSetOeffnen($SQL);
            $AWIS_KEY1 = $rsKey->FeldInhalt('KEY');
            $Meldung = $GUT->AWISSprachKonserven['GUT']['GUT_INSERT_OK'];
        }catch(awisException $ex){
            if(strpos($ex->getMessage(),'PK_GUT_VORGANGSNUMMER')!==false){
                $Meldung =  $GUT->AWISSprachKonserven['GUT']['GUT_ERR_PK_GUT_VORGANGSNUMMER'];
            }elseif(strpos($ex->getMessage(),'ORA-01400') !== false){ //Einf�gen von null nicht m�glich
                $Meldung =  $GUT->AWISSprachKonserven['GUT']['GUT_ERR_KEIN_WERT'];
            }elseif(strpos($ex->getMessage(),'UID_GUT_GUTSCHEINNR') !== false){ //Doppelte Gutscheinnr
                $Meldung =  $GUT->AWISSprachKonserven['GUT']['GUT_ERR_UID_GUT_GUTSCHEINNR'];
            }else
            {
                throw new awisException($ex->getMessage(),$ex->getCode(),$ex->getSQL(),$ex->getKategorie());
            }
        }


    } else { //ge�nderter DS

        $GUT->RechteMeldung(4); //Darf der User speichern?

        $SQL  ='UPDATE GUTSCHEINAKTION';
        $SQL .=' SET ';
        $SQL .='  GUT_VORGANGSNUMMER = ' . $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_VORGANGSNUMMER'],false);
        $SQL .=' , GUT_BETRAG         = '. $GUT->DB->WertSetzen('GUT','N2',$_POST['txtGUT_BETRAG'],false);
        $SQL .=' , GUT_NACHNAME       = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_NACHNAME'],false);
        $SQL .=' , GUT_VORNAME        = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_VORNAME'],false);
        $SQL .=' , GUT_STRASSE        = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_STRASSE'],false);
        $SQL .=' , GUT_PLZ            = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_PLZ'],false);
        $SQL .=' , GUT_BIC            = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtBLZ_BIC'],false);
        $SQL .=' , GUT_IBAN           = '. $GUT->DB->WertSetzen('GUT','T',$_POST['sucGUT_IBAN'],false);
        $SQL .=' , GUT_IBAN_LAND      = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_IBAN_LAND'],false);
        $SQL .=' , GUT_USER           = '. $GUT->DB->WertSetzen('GUT','T',$GUT->AWISBenutzer->BenutzerName(),false);
        $SQL .=' , GUT_USERDAT        = sysdate';
        $SQL .=' , GUT_STATUS         = 2';
        $SQL .=' , GUT_GUTSCHEINNR    = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_GUTSCHEINNR'],false);
        $SQL .=' , GUT_FIL_ID         = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_FIL_ID'],false);
        $SQL .=' , GUT_ORT            = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtGUT_ORT'],false);
        $SQL .=' , GUT_KREDITINSITUT  = '. $GUT->DB->WertSetzen('GUT','T',$_POST['txtBLZ_BEZEICHNUNG'],false);
        $SQL .=' WHERE GUT_KEY          = '. $GUT->DB->WertSetzen('GUT','N0',$AWIS_KEY1,false);
        $SQL .=' ';

        $GUT->DB->Ausfuehren($SQL, '', true, $GUT->DB->Bindevariablen('GUT'));
        $Meldung = $GUT->AWISSprachKonserven['GUT']['GUT_UPDATE_OK'];
    }
    if($Meldung){
        $GUT->Form->Hinweistext($Meldung);
    }

} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $GUT->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $GUT->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $GUT->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>