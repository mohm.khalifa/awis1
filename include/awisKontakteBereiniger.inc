<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisFormular.inc';
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 29.09.2016
 * Time: 14:12
 */
class awisKontakteBereiniger
{

    private $_DB;
    private $_Benutzer;
    private $_Form;
    function __construct($Benutzer)
    {
        $this->_Benutzer = awisBenutzer::Init(($Benutzer));
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS_PROD');
        $this->_DB->Oeffnen();
        $this->_Form = new awisFormular();
    }

    public function Simulation(){
        $this->_HoleBenutzerMitInaktivenKontakt(true);
        $this->_HoledoppelteBenutzer(true);
        $this->_HoleLoginsDieNichtZumKontaktPassen(true);
        $this->_KontakteMitPersonalnummernProblem();
    }

    private function _Umlautsreplace($String){
        $String = str_replace('�','AE',$String);
        $String = str_replace('�','OE',$String);
        $String = str_replace('�','UE',$String);
        $String = str_replace('�','SS',$String);

        return $String;

    }

    private function _InitialenVonDoppelNamen($Name,$ReverseReturn = false){
        $Initialien = false;

        if(strpos($Name, ' ')){
            $Initialien = explode(' ',$Name);
        }elseif(strpos($Name, '-')){
            $Initialien = explode('-',$Name);
        }


        $Name1 = @$Initialien[0];
        $Name2 = @$Initialien[1];

        if($ReverseReturn){
            $Initialien = @$Name1[0] . @$Name2[0];

        }else{
            $Initialien = @$Name2[0] . @$Name1[0];
        }

        return $Initialien;
    }

    public function _TauscheDoppelName($DoppelName, $AndererName){

        if(strpos($DoppelName, ' ')){
            $DoppelName = explode(' ',$DoppelName);
        }elseif(strpos($DoppelName, '-')){
            $DoppelName = explode('-',$DoppelName);
        }


        return $this->_StandardMoeglichkeiten($DoppelName[1]. ' ' . $DoppelName[0],$AndererName);
    }

    private function _pruefeAufDoppelname($Name){
        if(strpos($Name, ' ')){
            return true;
        }elseif(strpos($Name, '-')){
            return true;
        }
        return false;
    }


    private function _StandardMoeglichkeiten($Name1,$Name2){
        $Moeglichkeiten = array();

        //Kompletter Nachname, Ein Buchstabe des Vornamens (Und anders rum)
        $Moeglichkeiten[] = $Name1 . '_' . substr($Name2,0,1);
        $Moeglichkeiten[] = $Name2 . '_' .  substr($Name1,0,1);

        //Kompletter Nachname, Zwei Buchstabe des Vornamens (Und anders rum)
        $Moeglichkeiten[] = $Name1 . '_' . substr($Name2,0,2);
        $Moeglichkeiten[] = $Name2 . '_' .  substr($Name1,0,2);

        //Kompletter Nachname, erster und letzter  Buchstabe des Vornamens (Und anders rum)
        $Moeglichkeiten[] =$Name1 . '_' . @$Name2[0] .  substr($Name2,strlen($Name2)-1);
        $Moeglichkeiten[] = $Name2 . '_' .@$Name1[0]. substr($Name1,strlen($Name1)-1);


        return $Moeglichkeiten;

    }

    public function _alleMoeglichkeiten($Name1, $Name2){

        $Moeglichkeiten = array();

        $Moeglichkeiten = array_merge($Moeglichkeiten,$this->_StandardMoeglichkeiten($Name1,$Name2));

        //Oldscooler die nur den Vor oder Nachnamen als Login haben
        $Moeglichkeiten[] = $Name1;
        $Moeglichkeiten[] = $Name2;

        //Kompletter Nachname, Zwei Buchstabe des Vornamens bei Doppelnamen (Und anders rum) ZB: Daniel-Pascal Heining = Heining_DP, Heining_PD usw.
        if($this->_pruefeAufDoppelname($Name2)){
            $Moeglichkeiten[] = $Name1 . '_' . $this->_InitialenVonDoppelNamen($Name2);
            $Moeglichkeiten[] = $Name1 . '_' . $this->_InitialenVonDoppelNamen($Name2,true);
            $Moeglichkeiten = array_merge($Moeglichkeiten,$this->_TauscheDoppelName($Name2,$Name1));
        }
        if($this->_pruefeAufDoppelname($Name1)){
            $Moeglichkeiten[] =$Name2. '_' . $this->_InitialenVonDoppelNamen($Name1);
            $Moeglichkeiten[] = $Name2 . '_' . $this->_InitialenVonDoppelNamen($Name1,true);
            $Moeglichkeiten = array_merge($Moeglichkeiten,$this->_TauscheDoppelName($Name1,$Name2));
        }




        return $Moeglichkeiten;
    }

    private function _StringBereinigen($String){
        $String = trim(mb_strtoupper($String));
        $String = str_replace(' ','',$String);
        $String = $this->_Umlautsreplace($String);
        $String = str_replace('_','',$String);
        $String = str_replace('-','',$String);

        return $String;
    }

    private function _langeNamen($Login, $Name1, $Name2){

        $ersterTeil = explode('_',$Login)[0];

        if(strpos($Name1,$ersterTeil)!==false or strpos($Name2, $ersterTeil)!==false ){
            return true;
        }
        return false;
    }

    private function _HoleLoginsDieNichtZumKontaktPassen($Sim){
        $SQL  ='SELECT kon_name1 as nachname, kon_name2 as vorname, benutzer.*';
        $SQL .=' FROM kontakte a';
        $SQL .=' INNER JOIN';
        $SQL .='   (SELECT a.xbn_key,';
        $SQL .='     a.XBN_KON_KEY,';
        $SQL .='     B.XBL_KEY,';
        $SQL .='     B.XBL_LOGIN';
        $SQL .='   FROM benutzer a';
        $SQL .='   INNER JOIN BENUTZERLOGINS b';
        $SQL .='   ON a.XBN_KEY = B.XBL_XBN_KEY';
        $SQL .='   AND upper( B.XBL_LOGIN) NOT LIKE \'FIL-%\'';
        $SQL .='   ) benutzer ON a.KON_KEY = benutzer.XBN_KON_KEY';
        $SQL .=' WHERE KON_STATUS          = \'A\'';

        $rs = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rs->EOF()){


            $Moeglichkeiten = $this->_alleMoeglichkeiten( $rs->FeldInhalt('VORNAME'),$rs->FeldInhalt('NACHNAME'));


            $Login = $rs->FeldInhalt('XBL_LOGIN');
            $Login = $this->_StringBereinigen($Login);
            $Treffer = false;
            foreach($Moeglichkeiten as $Moeglichkeit) {
                $Moeglichkeit = $this->_StringBereinigen($Moeglichkeit);

                //Login entspricht der M�glichkeit
                if ($Login == $Moeglichkeit) {
                    $Treffer = true;
                    break;
                }

            }

            //Schauen ob es Vlt ein langer Name ist...
            if($this->_langeNamen($rs->FeldInhalt('XBL_LOGIN'),$rs->FeldInhalt('KON_NAME1'),$rs->FeldInhalt('KON_NAME2'))){
                $Treffer = true;
            }


            if(!$Treffer){
                echo 'Login: ' .  $rs->FeldInhalt('XBL_LOGIN') . ' passt nicht zum Kontakt ' . $rs->FeldInhalt(1) . ' ' . $rs->FeldInhalt(2) . '<br>';
            }

            $rs->DSWeiter();
        }

    }

    private function _HoleBenutzerMitInaktivenKontakt($Sim=false){
        $SQL  ='select XBN_KEY, XBN_KON_KEY from benutzer a';
        $SQL .=' inner join kontakte b';
        $SQL .=' on a.XBN_KON_KEY = B.KON_KEY';
        $SQL .=' where a.XBN_STATUS = \'A\' and B.KON_STATUS = \'I\'';

        $rs = $this->_DB->RecordSetOeffnen($SQL);


        if($Sim){
            while (!$rs->EOF()){
                echo 'XBN: ' . $rs->FeldInhalt(1) . ' sein Konkey ist inaktiv: ' . $rs->FeldInhalt(2) . '<br>';
                $rs->DSWeiter();
            }
        }

        return $rs;

    }

    private function _HoledoppelteBenutzer($Sim=false) {

        $SQL  ='SELECT xbn_kon_key,';
        $SQL .='   COUNT(*)';
        $SQL .=' FROM benutzer';
        $SQL .=' where XBN_STATUS = \'A\'';
        $SQL .=' GROUP BY xbn_kon_key';
        $SQL .=' HAVING COUNT(*)  > 1';
        $SQL .=' AND xbn_kon_key IS NOT NULL';

        $rs = $this->_DB->RecordSetOeffnen($SQL);
        if($Sim){
            while (!$rs->EOF()){
                echo 'Die XBNs : ';
                $SQL  ='SELECT * FROM benutzer';
                $SQL .=' WHERE XBN_STATUS = \'A\' and ';
                $SQL .=' xbn_kon_key =' .$this->_DB->WertSetzen('SQL','N0',$rs->FeldInhalt(1));

                $rs2 = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('SQL'));
                while(!$rs2->EOF()){
                    echo $rs2->FeldInhalt(1) . ' ';
                    $rs2->DSWeiter();
                }
                echo ' stehen alle auf Kon ' .$rs->FeldInhalt(1) . ' <br>';
                $rs->DSWeiter();
            }

        }
    }

    private function _KontakteMitPersonalnummernProblem(){

        $SQL  ='SELECT kon_key,';
        $SQL .='   kon_name1,';
        $SQL .='   kon_name2,';
        $SQL .='   a.KON_PER_NR,';
        $SQL .='   B.name    AS per_name,';
        $SQL .='   B.VORNAME AS per_vorname,';
        $SQL .='   B.DATUM_AUSTRITT, ';
        $SQL .='   B.PERSNR';
        $SQL .=' FROM kontakte a';
        $SQL .=' LEFT JOIN V_PERSONAL_KOMPLETT b';
        $SQL .=' ON a.kon_per_nr       = b.persnr';
        $SQL .=' WHERE KON_STATUS      = \'A\'';
        $SQL .=' AND ( B.DATUM_AUSTRITT >= sysdate';
        $SQL .=' OR B.DATUM_AUSTRITT  IS NULL)';

        $rs = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rs->EOF()){

            if($rs->FeldInhalt('PERSNR')==''){
                echo $rs->FeldInhalt('KON_KEY') . ' hat keinen g�ltigen Personaldatensatz. <br>';
            }elseif($this->_VergleicheNameVon2DS($rs->FeldInhalt('KON_NAME1'),$rs->FeldInhalt('KON_NAME2'),
                    $rs->FeldInhalt('PER_NAME'), $rs->FeldInhalt('PER_VORNAME')) < 90){
                echo $rs->FeldInhalt('KON_KEY') . ' seine Personalnummer passt nicht zum Personaldatensatz. <br>';
            }



            $rs->DSWeiter();
        }

    }

    private function _VergleicheNameVon2DS($DS1Name1, $DS1Name2, $DS2Name1, $DS2Name2){

        $DS1[] = $DS1Name1;
        $DS1[] = $DS1Name2;
        $DS2[] = $DS2Name1;
        $DS2[] = $DS2Name2;
        $MaxPercent = 0;

        foreach ($DS1 as $DS1Name){
            $DS1Name = $this->_StringBereinigen($DS1Name);
            foreach ($DS2 as $DS2Name) {
                $DS2Name = $this->_StringBereinigen($DS2Name);
                similar_text($DS1Name,$DS2Name,$Percent);
                if($Percent > $MaxPercent ){
                    $MaxPercent = $Percent;
                }

            }
        }
        return $MaxPercent;
    }
}