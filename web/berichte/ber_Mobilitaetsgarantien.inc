<?php
require_once 'awisBerichte.inc';
require_once '../mobilitaetsgarantie/mobilitaetsgarantie_funktionen.inc';

class ber_Mobilitaetsgarantien
    extends awisBerichte
{

    private $MaxSeitenAnzahl = 0;
    private $_rsMOB;

    /**
     * Prozedur zum Initialisieren des Berichts
     *
     * @see awisBerichte::init()
     */
    public function SetzeDateipfad($DateiPfad)
    {
        $this->_Dateiname = $DateiPfad;
    }

    public function init(array $Parameter)
    {

        $TextKonserven[] = array('Fehler', 'err_KeineDaten');
        $TextKonserven[] = array('Liste', 'lst_ALLE_0');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');

        $TextKonserven[] = array('MOB', '*');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);

        $SQL = 'SELECT ';
        $SQL .= '  MOB_KEY,';
        $SQL .= '   MOB_NAME1,';
        $SQL .= '   MOB_NAME2,';
        $SQL .= '   MOB_NAME3,';
        $SQL .= '   MOB_FIL_ID,';
        $SQL .= '   MOB_DATUM,';
        $SQL .= '   MOB_STRASSE,';
        $SQL .= '   MOB_POSTLEITZAHL,';
        $SQL .= '   MOB_ORT,';
        $SQL .= '   MOB_LAN_ATUSTAATS_NR,';
        $SQL .= '   MOB_KFZ_KENNZEICHEN,';
        $SQL .= '   MOB_ERSTZULASSUNG,';
        $SQL .= '   MOB_KM_STAND,';
        $SQL .= '   MOB_VERS_NUMMER,';
        $SQL .= '   MOB_VERS_GUELTIG_AB,';
        $SQL .= '   MOB_VERS_GUELTIG_BIS,';
        $SQL .= '   MOB_GESAMTUMSATZ,';
        $SQL .= '   MOB_BEMERKUNG,';
        $SQL .= '   MOB_ATUCARDKUNDE,';
        $SQL .= '   MOB_USER,';
        $SQL .= '   MOB_USERDAT,';
        $SQL .= '   MOB_CREADAT,';
        $SQL .= '   MOB_EXPORTDAT,';
        $SQL .= '   MOB_EXPORTSTATUS,';
        $SQL .= '   MOB_ANR_ID,';
        $SQL .= '   MOB_TITEL,';
        $SQL .= '   MOB_KFZ_KENNZEICHEN_KOMP,';
        $SQL .= '   MOB_BSA,';
        $SQL .= '   MOB_VORGANGSNUMMER';
        $SQL .= ' FROM MOBILITAETSGARANTIEN ';
        $SQL .= ' WHERE MOB_KEY = ' . $this->_DB->WertSetzen('MOB', 'Z', $_POST['txtMOB_KEY']);

        $this->_rsMOB = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('MOB'));
    }


    /**
     * Eigentliches PDF erzeugen
     *
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {
        // PDF Attribute setzen
        $this->SetTitle($this->_BerichtsName);
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins($this->_Parameter['LinkerRand'], 50, $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(true, 50);

        $this->NeueSeite();
        $this->Rechnungskopf();

        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:         // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    private function Rechnungskopf()
    {
        $this->_Zeile = $this->_Parameter['SeiteOben'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $EinheitsFelder = array(
            'MOB_KFZ_KENNZEICHEN',
            'MOB_VERS_GUELTIG_AB',
            'MOB_VERS_GUELTIG_BIS',
            'MOB_TITEL',
            'MOB_NAME1',
            'MOB_NAME2',
            'MOB_NAME3',
            'MOB_STRASSE',
            'MOB_POSTLEITZAHL',
            'MOB_ORT',
            'MOB_VERS_NUMMER',
            'MOB_FIL_ID',
            'MOB_VORGANGSNUMMER',
            'MOB_BEMERKUNG'
        );

        foreach ($EinheitsFelder as $Feld) {
            $this->_Zeile += ($this->_Parameter['ZeilenHoehe'] );
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['LabelBreite'], $this->_Parameter['ZeilenHoehe'], $this->_AWISSprachkonserven['MOB'][$Feld] , 1, 0, 'L', 0);

            $this->_Zeile = $this->GetY();
            $this->_Spalte += $this->_Parameter['LabelBreite'];
            $this->SetXY($this->_Spalte, $this->_Zeile);

            $this->Cell($this->_Parameter['WertBreite'], $this->_Parameter['ZeilenHoehe'], $this->_rsMOB->FeldInhalt($Feld), 1, 0, 'L', 0);
        }
    }


    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage();
    }


    /**
     * Automatische Kopfzeile
     *
     * @see TCPDF::Header()
     */
    public function Header()
    {
        if ($this->_BerichtsVorlage != '') {
            $pdf_vorlage = $this->ImportPage(($this->PageNo() == 1?1:2));        // Erste oder zweite Seite importieren
            $this->useTemplate($pdf_vorlage);
        }

        $this->MaxSeitenAnzahl++;
    }
}

?>