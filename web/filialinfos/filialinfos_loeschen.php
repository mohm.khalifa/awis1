<?php
/**
 * L�schen von Informationen auf der Filialinfo
 *
 * @author Sacha Kerres
 * @version 20090813
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);
	
	$Tabelle= '';


	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		$Tabelle = 'FIL';
		$Key=$_POST['txtFIL_KEY'];
		$FILKey=$_POST['txtFIL_KEY'];

		$AWIS_KEY1 = $Key;

		$Felder=array();

		$Felder[]=array($Form->LadeTextBaustein('FIL','FIL_BEZ'),$_POST['txtFIL_BEZ']);
		$Felder[]=array($Form->LadeTextBaustein('FIL','FIL_ID'),$_POST['txtFIL_ID']);
	}
	elseif(!isset($_POST['cmdLoeschenOK']) AND isset($_GET['cmdAktion']) AND $_GET['cmdAktion']=='AX-Filialen')
	{
		$Tabelle = 'FIF';
		$Key=$_GET['Del'];

		$SQL = 'SELECT FIF_KEY, FIF_FIL_ID, FIF_WERT';
		$SQL .= ' FROM Filialinfos';
		$SQL .= ' WHERE FIF_KEY=0'.$Key;

		$rsDaten = $DB->RecordsetOeffnen($SQL);

		$FILKey = $rsDaten->FeldInhalt('FIF_FIL_ID');

		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('FIL','FIL_ID'),$rsDaten->FeldInhalt('FIF_FIL_ID'));
		$Felder[]=array($Form->LadeTextBaustein('Filialinfo','fit_lbl_915'),$rsDaten->FeldInhalt('FIF_WERT'));
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];

					$SQL = 'SELECT ';
					$SQL .= ' FROM ';
					$SQL .= ' WHERE ..._KEY=0'.$Key;
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'','',$AWISSprache),$rsDaten->FeldInhalt(''));
					break;
			}
		}
		else
		{
			switch($_GET['Seite'])
			{
				case 'Mitbewerber':
					$Tabelle = 'MFI';
					$Key=$_GET['Del'];

					$SQL = 'SELECT MFI_KEY, MFI_FIL_ID, MBW_NAME1 ';
					$SQL .= ' FROM MitbewerberFilialen';
					$SQL .= ' INNER JOIN Mitbewerber ON MFI_MBW_KEY = MBW_KEY';
					$SQL .= ' WHERE MFI_KEY=0'.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$FILKey = $rsDaten->FeldInhalt('MFI_FIL_ID');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('MBW','MBW_NAME1'),$rsDaten->FeldInhalt('MBW_NAME1'));
					break;
				case 'Zukauflieferanten':
					$Tabelle = 'FLI';
					$Key=$_GET['Del'];

					$SQL = 'SELECT FLI_KEY, FLI_FIL_ID, FLI_GUELTIGAB, FLI_GUELTIGBIS, LVK_NAME1 ';
					$SQL .= ' FROM FILIALENLIEFERANTEN';
					$SQL .= ' INNER JOIN LIEFERANTENVERKAUFSHAEUSER ON FLI_LVK_KEY = LVK_KEY';
					$SQL .= ' WHERE FLI_KEY=0'.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$FILKey = $rsDaten->FeldInhalt('FLI_FIL_ID');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('FLI','FLI_LVK_KEY'),$rsDaten->FeldInhalt('LVK_NAME1'));
					$Felder[]=array($Form->LadeTextBaustein('FLI','FLI_GUELTIGAB'),$rsDaten->FeldInhalt('FLI_GUELTIGAB'));
					$Felder[]=array($Form->LadeTextBaustein('FLI','FLI_GUELTIGBIS'),$rsDaten->FeldInhalt('FLI_GUELTIGBIS'));
					break;
				case 'BetreuungExtern':
					$Tabelle = 'FFA';
					$Key=$_GET['Del'];

					$SQL = 'SELECT FFA_KEY, FFA_FIL_ID, FFA_GUELTIGAB, FFA_GUELTIGBIS, FAB_BEREICH, FAB_NAME1, FAB_ANSPRECHP1';
					$SQL .= ' FROM FACHBETRIEBEFILIALEN';
					$SQL .= ' INNER JOIN FACHBETRIEBE ON FFA_FAB_KEY = FAB_KEY';
					$SQL .= ' WHERE FFA_KEY=0'.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$FILKey = $rsDaten->FeldInhalt('FFA_FIL_ID');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_BEREICH'),$rsDaten->FeldInhalt('FAB_BEREICH'));
					if ($rsDaten->FeldInhalt('FAB_BEREICH')=='Buchbinder')
					{
						$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_ANSPRECHP1'),$rsDaten->FeldInhalt('FAB_ANSPRECHP1'));
					}
					else
					{
						$Felder[]=array($Form->LadeTextBaustein('FAB','FAB_NAME1'),$rsDaten->FeldInhalt('FAB_NAME1'));
					}
					$Felder[]=array($Form->LadeTextBaustein('FFA','FFA_GUELTIGAB'),$Form->Format('D',$rsDaten->FeldInhalt('FFA_GUELTIGAB'),true));
					$Felder[]=array($Form->LadeTextBaustein('FFA','FFA_GUELTIGBIS'),$Form->Format('D',$rsDaten->FeldInhalt('FFA_GUELTIGBIS'),true));
					break;
				case 'Stromvertrag':
					case 'Gasvertrag':
						$Tabelle = 'GSI';
						$Key=$_GET['Del'];

						$SQL = 'SELECT *';
						$SQL .= ' FROM GSVRAHMENVERTRAEGE';
						$SQL .= ' WHERE GSR_KEY='.$DB->WertSetzen('RAV', 'N0',$Key);

						$rsDaten = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('RAV', true));

						$FILKey = $_GET['FIL_ID'];

						$Felder=array();
						$Felder[]=array($Form->LadeTextBaustein('RAV','RAV_RAHMENVERTR'),$rsDaten->FeldInhalt('GSR_RAHMENVERTR'));
						break;
                case 'Filialkompetenzen':
                    $Tabelle = 'FIK';
                    $Key=$_GET['Del'];

                    $SQL = 'SELECT *';
                    $SQL .= ' FROM FILIALINFOS';
                    $SQL .= ' INNER JOIN FILIALKOMPETENZENTYPEN';
                    $SQL .= ' ON FILIALINFOS.FIF_WERT = FILIALKOMPETENZENTYPEN.FIK_KEY';
                    $SQL .= ' WHERE FIF_FIT_ID = 920';
                    $SQL .= ' AND FIF_KEY = '.$DB->WertSetzen('FIF', 'N0', $Key);

                    $rsFIK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FIF'));

                    $FILKey = $rsFIK->FeldInhalt('FIF_FIL_ID');

                    $Felder=array();
                    $Felder[]=array($Form->LadeTextBaustein('FIK','FIK_BEZEICHNUNG'),$rsFIK->FeldInhalt('FIK_BEZEICHNUNG'));
                    break;
                case 'Oeffnungszeiten':
                    $Tabelle = 'FZP';
                    $Key=$_GET['Del'];

                    $SQL = 'select *';
                    $SQL .= ' from FILIALENOEFFNUNGSZEITENPFLEGE';
                    $SQL .= ' left join FILIALENOEFFNUNGSZEITENMODELLE';
                    $SQL .= ' on FZP_FOZ_KEY = FOZ_KEY';
                    $SQL .= ' where FZP_KEY = ' . $DB->WertSetzen('FZP', 'N0', $Key );


                    $rsFZP = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FZP'));

                    $FILKey = $rsFZP->FeldInhalt('FZP_FIL_ID');

                    $Felder=array();
                    $Felder[]=array($Form->LadeTextBaustein('FOZ','FOZ_MODELL'),$rsFZP->FeldInhalt('FOZ_BEZEICHNUNG'));
                    $Felder[]=array($Form->LadeTextBaustein('FOZ','FOZ_GUELTIGAB'),$rsFZP->FeldInhalt('FZP_GUELTIG_AB','D'));
                    break;
			}

		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'MFI':
				$SQL = 'DELETE FROM MitbewerberFilialen WHERE mfi_key=0'.$DB->WertSetzen('DEL', 'N0', $_POST['txtKey']).' AND MFI_FIL_ID = '.$DB->WertSetzen('DEL', 'N0', $_POST['txtFILKey']);
				$AWIS_KEY1=$_POST['txtFILKey'];
				break;
			case 'FLI':
				$SQL = 'DELETE FROM FILIALENLIEFERANTEN WHERE fli_key=0'.$DB->WertSetzen('DEL', 'N0', $_POST['txtKey']).' AND FLI_FIL_ID = '.$DB->WertSetzen('DEL', 'N0', $_POST['txtFILKey']);
				$AWIS_KEY1=$_POST['txtFILKey'];
				break;
			case 'FIF':
				$SQL = 'DELETE FROM Filialinfos WHERE fif_key=0'.$DB->WertSetzen('DEL', 'N0', $_POST['txtKey']).' AND FIF_FIL_ID = '.$DB->WertSetzen('DEL', 'N0', $_POST['txtFILKey']);
				$AWIS_KEY1=0;
				break;
			case 'FFA':
				$SQL = 'DELETE FROM FACHBETRIEBEFILIALEN WHERE ffa_key=0'.$DB->WertSetzen('DEL', 'N0', $_POST['txtKey']).' AND FFA_FIL_ID = '.$DB->WertSetzen('DEL', 'N0', $_POST['txtFILKey']);
				$AWIS_KEY1=$_POST['txtFILKey'];
				$Form->DebugAusgabe(1,$SQL);
				break;
			case 'GSI':
				$SQL = 'DELETE FROM GSVINFOS WHERE gsi_gsr_key='.$DB->WertSetzen('DEL', 'N0', $_POST['txtKey']).' AND gsi_xxx_key = '.$DB->WertSetzen('DEL', 'N0', $_POST['txtFILKey']);
				$AWIS_KEY1=$_POST['txtFILKey'];
				$Form->DebugAusgabe(1,$SQL);
				break;
            case 'FIK':
                $SQL = 'DELETE FROM FILIALINFOS WHERE FIF_KEY = '. $DB->WertSetzen('DEL', 'N0', $_POST['txtKey']);
                $AWIS_KEY1=$_POST['txtFILKey'];
                break;
            case 'FZP':
                $SQL = 'DELETE FROM FILIALENOEFFNUNGSZEITENPFLEGE WHERE FZP_KEY = '. $DB->WertSetzen('DEL', 'N0', $_POST['txtKey']);
                $AWIS_KEY1=$_POST['txtFILKey'];
                break;
			default:
				break;
		}

		if($SQL !='')
		{
			$DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('DEL'));
		}
	}

	if($Tabelle!='')
	{

		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./filialinfos_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('FILKey',$FILKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>