<?php
global $AWIS_KEY1;
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");

$Trenner = ';';
$LF = PHP_EOL;

try {
    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();

    $TextKonserven = array();
    $TextKonserven[] = array('LIK', '%');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven, ($AWISBenutzer->BenutzerSprache() == 'CZ' ? 'DE' : $AWISBenutzer->BenutzerSprache()));

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $Recht3600 = $AWISBenutzer->HatDasRecht(3600);
    if ($Recht3600 == 0) {
        $Form->Fehler_KeineRechte();
    }

    if (isset($_GET['LKK_KEY'])) {
        $AWIS_KEY1 = intval($_GET['LKK_KEY']);
    } else {
        $Form->Fehler_Anzeigen('INTERN', 'LKK_KEY fehlt', 'MELDEN', 1, '201909130948');
    }

    /***************************************
     * Hole Daten per SQL
     ***************************************/
    $SQL = 'SELECT lkk.*, lik.*, ast_vk, NVL(AST_BEZEICHNUNGWW, AST_BEZEICHNUNG) as AST_BEZEICHNUNG ';
    $SQL .= ' FROM Lieferkontrollenkopf LKK';
    $SQL .= ' INNER JOIN Lieferkontrollen LIK on LIK_LKK_KEY = LKK_KEY';
    $SQL .= ' LEFT JOIN ARTIKELSTAMM ON AST_ATUNR = LIK_AST_ATUNR';
    $SQL .= ' WHERE LKK_KEY= ' . $DB->WertSetzen('LKK','Z',$AWIS_KEY1);
    $SQL .= ' AND LIK_MENGESOLL <> LIK_MENGEIST';
    $SQL .= ' ORDER BY LIK_AST_ATUNR ASC';

    $rsLIK = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('LKK'));
    $Form->DebugAusgabe(1, $DB->LetzterSQL());

    if ($rsLIK->AnzahlDatensaetze() == 0)        // Keine Daten
    {
        $Form->Hinweistext('Keine Daten gefunden!');
        die();
    }

	$handle = fopen("lkk.csv", "w");

    $Zeile = 'LIK_QUELLE'.$Trenner;
    $Zeile .= 'LIK_FIL_ID'.$Trenner;
    $Zeile .= 'LIK_AST_ATUNR'.$Trenner;
    $Zeile .= 'AST_BEZEICHNUNG'.$Trenner;
    $Zeile .= 'LIK_DATUMKOMM'.$Trenner;
    $Zeile .= 'LIK_MENGESOLL'.$Trenner;
    $Zeile .= 'LIK_MENGEIST'.$Trenner;
    $Zeile .= 'DIFFERENZ'.$Trenner;
    $Zeile .= 'LIK_SOLLBESTAND'.$Trenner;
    $Zeile .= 'LIK_ISTBESTAND'.$Trenner;
    $Zeile .= 'AST_VK';
	$Zeile .= $LF;
	fwrite($handle, $Zeile);
    while (!$rsLIK->EOF()) {

        $Zeile = $rsLIK->FeldInhalt('LIK_QUELLE').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_FIL_ID').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_AST_ATUNR').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('AST_BEZEICHNUNG').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_DATUMKOMM').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_MENGESOLL').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_MENGEIST').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_MENGEIST') - $rsLIK->FeldInhalt('LIK_MENGESOLL').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_SOLLBESTAND').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('LIK_ISTBESTAND').$Trenner;
        $Zeile .= $rsLIK->FeldInhalt('AST_VK');

        $Zeile .= $LF;

		fwrite($handle, $Zeile);
        $rsLIK->DSWeiter();
    }

	fclose($handle);

	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename('lkk.csv'));
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize('lkk.csv'));
	readfile('lkk.csv');
} catch (awisException $ex) {
    echo 'AWIS-Fehler:' . $ex->getMessage();
} catch (Exception $ex) {
    echo 'allg. Fehler:' . $ex->getMessage();
}

?>

