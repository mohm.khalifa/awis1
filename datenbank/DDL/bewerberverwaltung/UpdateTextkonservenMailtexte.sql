SET DEFINE OFF;

/*
#########
Mailversandtexte
 */

UPDATE
  MAILVERSANDTEXTE
SET
  MVT_TEXT = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta name=Generator content="Microsoft Word 11 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"MS Mincho";
	panose-1:2 2 6 9 4 2 5 8 3 4;}
@font-face
	{font-family:"\@MS Mincho";
	panose-1:0 0 0 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:navy;
	text-decoration:underline;}
p.txt, li.txt, div.txt
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	line-height:13.0pt;
	text-autospace:none;
	font-size:10.0pt;
	font-family:Arial;}
span.E-MailFormatvorlage18
	{mso-style-type:personal;
	font-family:Arial;
	color:windowtext;}
span.E-MailFormatvorlage19
	{mso-style-type:personal-reply;
	font-family:Arial;
	color:navy;}
@page Section1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;}
div.Section1
	{page:Section1;}
-->
</style>

</head>

<body lang=DE link=blue vlink=navy>

<div class=Section1>

<p class=txt style=''margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal''><font
size=2 color=black face=Arial><span style=''font-size:10.0pt;color:black''>Sehr
#GEEHERT# #ANREDE# #HRK_TITEL# #HRK_VORNAME# #HRK_NACHNAME#,<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''>wir bedanken uns heute f�r das angenehme
und interessante Gespr�ch, welches wir mit Ihnen in unserem Hause f�hren durften.
Wir haben uns gefreut, Sie in diesem Gespr�ch kennen zu lernen und uns von Ihnen
einen ausf�hrlichen Eindruck zu verschaffen. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''>Nach Abw�gung aller Faktoren sind wir zu
dem Ergebnis gelangt, dass wir Ihnen f�r diese Stelle leider kein Angebot
unterbreiten k�nnen. Sehen Sie dieses Ergebnis nicht als Wertung Ihrer Person
oder Qualifikation. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''>Wir w�nschen Ihnen, dass Sie bald eine Ihren
Vorstellungen entsprechende Position finden und m�chten Sie dennoch ermutigen sich
bei einer weiteren Gelegenheit erneut bei A.T.U. zu bewerben. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''>All unsere vakanten Stellen finden Sie
tagesaktuell auf der Karriereseite unserer Homepage. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

</div>

</body>

</html>'
WHERE
  MVT_BEREICH = 'BEW_ABSAGE_VORSTELLUNGSGESPRAECH';


UPDATE
  MAILVERSANDTEXTE
SET
  MVT_TEXT = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta name=Generator content="Microsoft Word 11 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"MS Mincho";
	panose-1:2 2 6 9 4 2 5 8 3 4;}
@font-face
	{font-family:"\@MS Mincho";
	panose-1:0 0 0 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:navy;
	text-decoration:underline;}
span.E-MailFormatvorlage17
	{mso-style-type:personal-compose;
	font-family:Arial;
	color:windowtext;}
p.txt, li.txt, div.txt
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	line-height:13.0pt;
	text-autospace:none;
	font-size:10.0pt;
	font-family:Arial;}
@page Section1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;}
div.Section1
	{page:Section1;}
-->
</style>

</head>

<body lang=DE link=blue vlink=navy>

<div class=Section1>

<p class=txt><font size=2 color=black face=Arial><span style=''font-size:10.0pt;
color:black''>Sehr #GEEHERT# #ANREDE# #HRK_TITEL# #HRK_NACHNAME#,<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=txt><font size=2 color=black face=Arial><span style=''font-size:10.0pt;
color:black''>Wir bedanken uns f�r Ihre Bewerbung und dem damit an unserem
Unternehmen verbundenen Interesse. <o:p></o:p></span></font></p>

<p class=txt><font size=2 color=black face=Arial><span style=''font-size:10.0pt;
color:black''>Wir haben Ihre Unterlagen aufmerksam gelesen und f�r die gew�nschte
Vakanz sorgf�ltig gepr�ft. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=txt><font size=2 color=black face=Arial><span style=''font-size:10.0pt;
color:black''>Als Ergebnis dieser Pr�fung m�ssen wir Ihnen heute leider mitteilen,
dass wir Ihre Bewerbung in unserem weiteren Bewerbungsprozess nicht mehr
weiterverfolgen k�nnen. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''>Wir bedauern Ihnen hier an dieser Stelle
keine anderslautende Nachricht �bermitteln zu k�nnen.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''>F�r Ihre berufliche Neuorientierung
w�nschen wir Ihnen alles Gute und viel Erfolg. <o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''>Wir w�rden uns freuen, wenn Sie bei einer
erneuten Gelegenheit noch einmal  bei A.T.U. bewerben m�chten. All unsere vakanten
Stellen sehen Sie tagesaktuell auf unserer Homepage unter www.atu.de.<o:p></o:p></span></font></p>

<p class=MsoNormal><font size=2 color=black face=Arial><span style=''font-size:
10.0pt;font-family:Arial;color:black''><o:p>&nbsp;</o:p></span></font></p>

</div>

</body>

</html>'
WHERE
  MVT_BEREICH = 'BEW_ABSAGE';

/*
#########
Textkonserven
 */

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt',
XTX_TEXT_AT = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt',
  XTX_TEXT_CH = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt',
  XTX_TEXT_CZ = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt',
  XTX_TEXT_FR = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt',
  XTX_TEXT_IT = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt',
  XTX_TEXT_NL = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 1312<br>
UST-ID Nr. DE134043104 <br>
Pers�nlich haftende Gesellschafterin:<br>
A.T.U Auto-Teile-Unger GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 745<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),
Andreas Schmidt'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_MAIL_SIGNATUR_1';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_AT = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_CH = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_CZ = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_FR = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_IT = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_NL = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team <br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_MAIL_SIGNATUR_2';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_AT = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_CH = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_CZ = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_FR = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_IT = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_NL = '<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_MAIL_SIGNATUR_4';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U Auto-Teile-Unger Handels GmbH & Co. KG
karriere@de.atu.eu
www.atu.eu
'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_PDF_SIGNATUR_1';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U Auto-Teile-Unger Handels GmbH & Co. KG
karriere@de.atu.eu
www.atu.eu

'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_PDF_SIGNATUR_2';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U Auto-Teile-Unger Handels GmbH & Co. KG
karriere@de.atu.eu
www.atu.eu

'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_PDF_SIGNATUR_3';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U Auto-Teile-Unger Handels GmbH & Co. KG
karriere@de.atu.eu
www.atu.eu

'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_PDF_SIGNATUR_4';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U Auto-Teile-Unger Handels GmbH & Co. KG
karriere@de.atu.eu
www.atu.eu

'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_PDF_SIGNATUR_5';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = 'Mit freundlichen Gr��en

Ihr Recruiting Team

A.T.U Auto-Teile-Unger Handels GmbH & Co. KG
karriere@de.atu.eu
www.atu.eu

'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_PDF_SIGNATUR_6';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_AT = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_CH = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_CZ = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_FR = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_IT = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye',
  XTX_TEXT_NL = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert, <br>
Arnaud Vuye'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_SIGNATUR_ATU';

UPDATE TEXTKONSERVEN SET XTX_TEXT_DE = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_AT = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_CH = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_CZ = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_FR = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_IT = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert',
  XTX_TEXT_NL = '<br>
--<br>
Freundliche Gr��e<br>
<br>
Ihr Recruiting Team<br>
___________________________________________________________<br>
<br>
A.T.U Auto-Teile-Unger<br>
Handels GmbH & Co. KG<br>
Dr.-Kilian-Str. 11<br>
92637 Weiden i. d. OPf.<br>
<br>
Tel. Frau Susanne Reber: +49 (0) 9 61 � 3 06 � 5416<br>
<br>
karriere@de.atu.eu<br>
www.atu.eu<br>
<br>
<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRA 2012<br>
UST-ID Nr. DE814195392, WEEE-Nr. DE53789710<br>
Pers�nlich haftende Gesellschafterin:<br>
AFM Autofahrerfachmarkt Gesch�ftsf�hrungs GmbH<br>
Sitz: Weiden i. d. OPf., Amtsgericht Weiden i. d. OPf., HRB 2842<br>
Gesch�ftsf�hrer: J�rn Werner (Vorsitzender der Gesch�ftsf�hrung),<br>
Ralph Goedecke, Andreas Schmidt, Thorsten Stradt, Katrin Teichert'
WHERE XTX_BEREICH = 'BEW' AND XTX_KENNUNG = 'BEW_SIGNATUR_ESTATO';
