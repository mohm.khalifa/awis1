<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RRP','*');
	$TextKonserven[]=array('REH','*');
	$TextKonserven[]=array('RST','*');
	$TextKonserven[]=array('RPP','*');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	if($Recht34000==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='' and ($Recht34000&2)==2)
	{
		$Param['QMM_FIL_ID']=$Form->Format('T',$_POST['txtQMM_FIL_ID'],true);
	}
	else
	{
		if(isset($_POST['sucQMM_FIL_ID']))
		{
			$Param['QMM_FIL_ID']=$Form->Format('N0',$_POST['sucQMM_FIL_ID'],true);
			if($Param['QMM_FIL_ID'] == 0)
			{
				$Param['QMM_FIL_ID'] = '';
			}
		}
		else
		{
			$Param['QMM_FIL_ID']=0;
		}
		if(($Recht34000&4)==4 or ($Recht34000&8)==8)
		{
			$Param['FIL_GEBIET'] = $Form->Format('N0',$_POST['sucFIL_GEBIET'],true);
		}
	}
	$Param['QMP_VOM'] = $Form->Format('D',$_POST['sucQMP_ERHEBDAT_VON'],true);
	$Param['QMP_BIS'] = $Form->Format('D',$_POST['sucQMP_ERHEBDAT_BIS'],true);
	$Param['QMM_QMK_KEY'] = $Form->Format('T',$_POST['sucQMK_KRITERIUM'],true);
	$Param['QMM_STATUS'] = 	$Form->Format('N0',$_POST['sucQMM_STATUS'],true);
	$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	$AWISBenutzer->ParameterSchreiben('Formular_QMM_Export',serialize($Param));
	
	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	
	
	
	$ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);

	
	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Mar 2015");
	header('Pragma: public');
	header('Cache-Control: max-age=0');

	
	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="Auswertung_QMM.xls"');
			break;
		case 2:                 // Excel 2007
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="Auswertung_QMM.xlsx"');
			break;
	}
	
	$XLSXObj = new PHPExcel();
	$XLSXObj->getProperties()->setCreator(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setTitle(utf8_encode('Auswertung_QMM'));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode('Auswertung_QMM'));
	
	$XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');

	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
			$SQL = 'select qms_folgestatus,qmp_key,qmp_qmk_key,qmp_fil_id,qmp_persnr,qmp_feststellung,qmk_lang,qmp_erhebdat,qmp_ergebnis,frist,qms_bezeichnung,case when anz is null then 0 else anz end as Anz';
			$SQL .= ' from( ' ;
			$SQL .= 'select qmp.*,qmk.*,qms.*, ';
			$SQL .=' case when qms_folgestatus = 0 and qms_massnahme = 0';
			$SQL .='   then (select frist from(';
			$SQL .='           SELECT qmz_qmp_key,MIN(qmz_frist) AS frist';
			$SQL .='           FROM qmmassnahmenzuord qmz';
			$SQL .='           INNER JOIN qmmassnahmen qmm';
			$SQL .='           ON qmz.qmz_qmm_key = qmm.qmm_key';
			$SQL .='           INNER JOIN qmstatus qms';
			$SQL .='           ON qmz.qmz_status   = qms.qms_key';
			$SQL .='           AND qms_folgestatus is not null';
			$SQL .='           AND qms_massnahme   = 1';
			$SQL .='           GROUP BY qmz_qmp_key)';
			$SQL .='           WHERE qmz_qmp_key   = qmp.qmp_key) ';
			$SQL .='   else qmp_eskalationsdat+qms_frist ';
			$SQL .='   end as frist,';
			$SQL .=' (select Anz from(';
			$SQL .='            SELECT qmz_qmp_key,count(qmz_key) AS Anz';
			$SQL .='            FROM qmmassnahmenzuord qmz';
			$SQL .='            INNER JOIN qmmassnahmen qmm';
			$SQL .='            ON qmz.qmz_qmm_key = qmm.qmm_key';
			$SQL .='            INNER JOIN qmstatus qms';
			$SQL .='            ON qmz.qmz_status   = qms.qms_key';
			$SQL .='            AND qms_massnahme   = 1';
			$SQL .='            GROUP BY qmz_qmp_key)';
			$SQL .='            WHERE qmz_qmp_key   = qmp.qmp_key) as Anz';
			$SQL .= ' from qmplaene qmp ';
			$SQL .= ' inner join qmkriterien qmk on qmp.qmp_qmk_key = qmk.qmk_key and qmk_aktiv = 1';
			$SQL .= ' inner join qmstatus qms on qmp.qmp_status = qms.qms_key)';
	
			if ($Bedingung != '')
			{
				$SQL .= ' WHERE ' . substr($Bedingung, 4);
			}
			$SQL .= ' ORDER BY QMK_LANG';
			
		
			
	
			$rsQMM = $DB->RecordSetOeffnen($SQL);

			
	$XLSXObj->setActiveSheetIndex(0);
	$XLSXObj->getActiveSheet()->setTitle(utf8_encode('Pl�ne'));
	$XLSXObj->createSheet(1)->setTitle(utf8_encode('Ma�nahmen'));
	$XLSXObj->createSheet(2)->setTitle(utf8_encode('Pl�ne+Ma�nahmen'));

	
	$SpaltenNr = 0;
	$ZeilenNr = 1;		

		//�berschrift
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		$SpaltenNr++;
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Pl�ne'));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y G:i',time()));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
		
		$ZeilenNr++;
		$ZeilenNr++;
		$SpaltenNr = 0;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Kriterium');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Erhebdat');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Ergebnis');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;

		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Filiale');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Feststellung');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Personalnummer');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Status');
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
		$SpaltenNr++;		
		
	
		$ZeilenNr++;
		$SpaltenNr=0;
			while(!$rsQMM->EOF())
			{
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMM->FeldInhalt('QMK_LANG'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
				
				$Datum=$Form->PruefeDatum($Form->Format('D',$rsQMM->FeldInhalt('QMP_ERHEBDAT')),false,false,true);
				$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr,PHPExcel_Shared_Date::PHPToExcel($Datum+86400)); // Warum ein Tag versetzt? SK Fragen
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
				
				if($rsQMM->FeldInhalt('QMP_QMK_KEY') != 'BeschwerdeQ')
				{
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,str_replace(',','.',$rsQMM->FeldInhalt('QMP_ERGEBNIS'))/100,PHPExcel_Cell_DataType::TYPE_NUMERIC);
					$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00);
				}
				else 
				{
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,str_replace(',','.',$rsQMM->FeldInhalt('QMP_ERGEBNIS')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
					$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode('#,##0.00000000');
				}
				
				//$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,($rsQMM->FeldInhalt('QMP_QMK_KEY') != 'BeschwerdeQ' ?$Form->Format('N2',$rsQMM->FeldInhalt('QMP_ERGEBNIS')).' %':$Form->Format('N8',$rsQMM->FeldInhalt('QMP_ERGEBNIS'))),PHPExcel_Cell_DataType::TYPE_STRING);
				//$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMM->FeldInhalt('QMP_FIL_ID'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
				
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMM->FeldInhalt('QMP_FESTSTELLUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
				
				if($rsQMM->FeldInhalt('QMP_PERSNR') != '')
				{
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,$rsQMM->FeldInhalt('QMP_PERSNR'),PHPExcel_Cell_DataType::TYPE_NUMERIC);
					$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
				}
				else
				{
					$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'',PHPExcel_Cell_DataType::TYPE_NULL);
					$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
				}
				$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode($Form->Format('T',$rsQMM->FeldInhalt('QMS_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
				$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			
				$ZeilenNr++;
				$SpaltenNr=0;	
				$rsQMM->DSWeiter();
			}
	
	for($S='A';$S<='G';$S++)
	{
		$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	
	
	$XLSXObj->setActiveSheetIndex(1);
	
	$SpaltenNr = 0;
	$ZeilenNr = 1;
	

	//�berschrift
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Ma�nahmen'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y G:i',time()));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	$ZeilenNr++;
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode('Filiale'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode('Ma�nahme'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Beschreibung');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Frist');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Status');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Ma�nahme umgesetzt'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	
	$ZeilenNr++;
	$SpaltenNr=0;
	
	
	$SQL  = 'select QMP_FIL_ID,QMM_BEZEICHNUNG,QMZ_BEMERKUNG,QMZ_FRIST,mas.qms_bezeichnung MASSTATUS,func_textkonserve(\'lst_MassErfuellt\',\'Liste\',\'\',qmz_erfuellt) as Erfuellt from qmmassnahmenzuord ';
	$SQL .= 'inner join qmplaene on qmp_key = qmz_qmp_key ';
	$SQL .= 'inner join qmmassnahmen on qmm_key = qmz_qmm_key ';
	$SQL .= 'inner join qmstatus mas on qmz_status = mas.qms_key';
	
	
	if ($Bedingung != '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	$SQL .= ' ORDER BY QMP_FIL_ID,QMM_BEZEICHNUNG';
		
	
		
	
	$rsQMZ = $DB->RecordSetOeffnen($SQL);
	
	
	while(!$rsQMZ->EOF())
	{
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMZ->FeldInhalt('QMP_FIL_ID'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMZ->FeldInhalt('QMM_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMZ->FeldInhalt('QMZ_BEMERKUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	
		$Datum=$Form->PruefeDatum($rsQMZ->FeldInhalt('QMZ_FRIST'),false,false,true);
		$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr,PHPExcel_Shared_Date::PHPToExcel($Datum+86400));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
	
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMZ->FeldInhalt('MASSTATUS'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMZ->FeldInhalt('ERFUELLT'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	
		$ZeilenNr++;
		$SpaltenNr=0;
		$rsQMZ->DSWeiter();
	}
	

	for($S='A';$S<='G';$S++)
	{
	$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	
	
	$XLSXObj->setActiveSheetIndex(2);

	
	$SpaltenNr = 0;
	$ZeilenNr = 1;
	
	//�berschrift
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Pl�ne+Ma�nahmen'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y G:i',time()));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	
	$ZeilenNr++;
	$ZeilenNr++;
	$SpaltenNr = 0;
	
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Kriterium');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Erhebdat');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Ergebnis');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Filiale');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Feststellung');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Personalnummer');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Planstatus');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode('Ma�nahme'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Beschreibung');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Frist');
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Ma�nahmenstatus'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	$SpaltenNr++;
	
	$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Ma�nahme umgesetzt'));
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	
	$ZeilenNr++;
	$SpaltenNr=0;
	
	$SQL  = 'select QMK_LANG,QMP_ERHEBDAT,QMP_ERGEBNIS,QMP_FIL_ID,QMP_FESTSTELLUNG,QMP_PERSNR,pla.qms_bezeichnung PLANSTATUS,QMM_BEZEICHNUNG,QMZ_BEMERKUNG,QMZ_FRIST,mas.qms_bezeichnung MASSTATUS,func_textkonserve(\'lst_MassErfuellt\',\'Liste\',\'\',qmz_erfuellt) as Erfuellt  from qmmassnahmenzuord ';
	$SQL .= 'inner join qmplaene on qmp_key = qmz_qmp_key ';
	$SQL .= 'inner join qmmassnahmen on qmm_key = qmz_qmm_key ';
	$SQL .= 'inner join qmstatus mas on qmz_status = mas.qms_key ';
	$SQL .= 'inner join QMKRITERIEN on qmp_qmk_key = qmk_key ';
	$SQL .= 'inner join qmstatus pla on qmp_status = pla.qms_key';
	
	
	if ($Bedingung != '')
	{
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	$SQL .= ' ORDER BY QMP_FIL_ID,QMK_LANG';
	
	$rsQMP = $DB->RecordSetOeffnen($SQL);
	
	while(!$rsQMP->EOF())
	{
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('QMK_LANG'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	
	
		$Datum=$Form->PruefeDatum($rsQMP->FeldInhalt('QMP_ERHEBDAT'),false,false,true);
		$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr,PHPExcel_Shared_Date::PHPToExcel($Datum+86400));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
	
		if($rsQMM->FeldInhalt('QMP_QMK_KEY') != 'BeschwerdeQ')
		{
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,str_replace(',','.',$rsQMP->FeldInhalt('QMP_ERGEBNIS'))/100,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00);
		}
		else
		{
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,str_replace(',','.',$rsQMP->FeldInhalt('QMP_ERGEBNIS')),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode('#,##0.00000000');
		}
	
		//$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,($rsQMM->FeldInhalt('QMP_QMK_KEY') != 'BeschwerdeQ' ?$Form->Format('N2',$rsQMM->FeldInhalt('QMP_ERGEBNIS')).' %':$Form->Format('N8',$rsQMM->FeldInhalt('QMP_ERGEBNIS'))),PHPExcel_Cell_DataType::TYPE_STRING);
		//$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('QMP_FIL_ID'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
	
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('QMP_FESTSTELLUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	
		if($rsQMP->FeldInhalt('QMP_PERSNR') != '')
		{
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,$rsQMP->FeldInhalt('QMP_PERSNR'),PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
		}
		else
		{
			$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,'',PHPExcel_Cell_DataType::TYPE_NULL);
			$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
		}
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('PLANSTATUS'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('QMM_BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('QMZ_BEMERKUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
		$Datum=$Form->PruefeDatum($rsQMP->FeldInhalt('QMZ_FRIST'),false,false,true);
		$XLSXObj->getActiveSheet()->setCellValueByColumnAndRow($SpaltenNr,$ZeilenNr,PHPExcel_Shared_Date::PHPToExcel($Datum+86400));
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('MASSTATUS'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
		$XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsQMP->FeldInhalt('ERFUELLT'))),PHPExcel_Cell_DataType::TYPE_STRING);
		$XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
		
		$ZeilenNr++;
		$SpaltenNr=0;
		$rsQMP->DSWeiter();
	}
	
	
	for($S='A';$S<='L';$S++)
	{
	$XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	}
	
	$XLSXObj->setActiveSheetIndex(0);
	
	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			$objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
			break;
		case 2:                 // Excel 2007
			$objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
			break;
	}
	
	$objWriter->save('php://output');
	//$objWriter->save('/daten/web/Vorlage.xls');
	$XLSXObj->disconnectWorksheets();
	
	
	
}
catch(exception $ex)
{
	$Form->DebugAusgabe(1,$ex->getMessage());
}



/**
 * Bedingung zusammenbauen
 *
 * @param string $_POST
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Bedingung = '';
	
	$Recht34000 = $AWISBenutzer->HatDasRecht(34000);
	
	if(($Recht34000&4)==4)
	{
		/*
			$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
					$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
					$Bedingung .=' where xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
					$Bedingung .=' group by xx1_fil_id) ';
		*/
	
		//Neuer SQL f�r RGZ 14.01.2015 ST
		$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
		$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
		$Bedingung .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
		$Bedingung .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'and xx1_fer_key in (23,25,57))';
		$Bedingung .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
		$Bedingung .=' and trunc(frv_datumvon) <= trunc(sysdate)';
		$Bedingung .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57))';
		$Bedingung .=' group by xx1_fil_id) ';
	
	}
	
	
	if (isset($Param['QMP_VOM']) AND (!empty($Param['QMP_VOM'])))
	{
		$Bedingung .= ' AND (QMP_ERHEBDAT >= ' . $DB->FeldInhaltFormat('D', $Param['QMP_VOM']) . ')';
	}
	 
	if (isset($Param['QMP_BIS']) AND (!empty($Param['QMP_BIS'])))
	{
		$Bedingung .= ' AND (QMP_ERHEBDAT <= ' . $DB->FeldInhaltFormat('D', $Param['QMP_BIS']) . ')';
	}
	
	if(isset($Param['QMM_FIL_ID']) AND $Param['QMM_FIL_ID']!='' AND $Param['QMM_FIL_ID']!=0)
	{
		$Bedingung .= 'AND QMP_FIL_ID ' . $DB->LikeOderIst($Param['QMM_FIL_ID']) . ' ';
	}
	
	if(isset($Param['QMM_QMK_KEY']) AND $Param['QMM_QMK_KEY']!='' AND $Param['QMM_QMK_KEY'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
	{
		$Bedingung .= 'AND QMP_QMK_KEY ' . $DB->LikeOderIst($Param['QMM_QMK_KEY']) . ' ';
	}
	if(isset($Param['QMM_STATUS']) AND $Param['QMM_STATUS']!='' AND $Param['QMM_STATUS']!='0')
	{
		$Bedingung .= 'AND QMP_STATUS ' . $DB->LikeOderIst($Param['QMM_STATUS']) . ' ';
	}
	if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET'] !='' AND $Param['FIL_GEBIET'] !='0')
	{
		if(($Recht34000&4)==4)
		{
			/*
				$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
						$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
						$Bedingung .=' where xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY();
						$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
						$Bedingung .=' group by xx1_fil_id) ';
			*/
				
			//Neuer SQL f�r RGZ 14.01.2015 ST
			$Bedingung .= 'AND QMP_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
			$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
			$Bedingung .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
			$Bedingung .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().' and xx1_fer_key in (23,25,57)';
			$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=')';
			$Bedingung .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
			$Bedingung .=' and trunc(frv_datumvon) <= trunc(sysdate)';
			$Bedingung .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (23,25,57)';
			$Bedingung .=' and feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=')';
			$Bedingung .=' group by xx1_fil_id) ';
				
		}
		elseif(($Recht34000&8)==8)
		{
			//Fehler behoben G�ltigkeiten hinzugef�gt 14.01.2015 ST
			$Bedingung .= 'AND QMP_FIL_ID in( select fez_fil_id from filialebenenzuordnungen ';
			$Bedingung .=' where fez_feb_key ='.$Param['FIL_GEBIET'];
			$Bedingung .=' and trunc(fez_gueltigab) <= trunc(sysdate)';
			$Bedingung .=' and trunc(fez_gueltigbis) >= trunc(sysdate))';
		}
	}
	
	
	
	return $Bedingung;
}

?>