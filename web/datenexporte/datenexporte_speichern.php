<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

global $AWIS_KEY1;
global $AWIS_KEY2;


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TXT_Speichern = $Form->LadeTexte($TextKonserven, $AWISBenutzer->BenutzerSprache());
	
	$XAB_KEY = $_POST['txtXDE_KEY'];
	$AWIS_KEY1 = (isset($_POST['txtXAB_KEY'])?$_POST['txtXAB_KEY']:'');
	// Neue Abfrage erstellen
	if($AWIS_KEY1<=0)
	{
		$SQL = 'INSERT INTO ExportAbfragen(';
		$SQL .= ' XAB_Bezeichnung, XAB_XBN_KEY, XAB_XDE_Key, XAB_VORSCHAU, XAB_UEBERSCHRIFT, XAB_FELDLISTE, XAB_User, XAB_UserDat) VALUES (';
		$SQL .= ''.$DB->WertSetzen('XAB','T',$_POST['txtXAB_BEZEICHNUNG']);
		$SQL .= ','.$DB->WertSetzen('XAB','N0',$AWISBenutzer->BenutzerID());
		$SQL .= ', '.$DB->WertSetzen('XAB','N0',$_POST['txtXDE_KEY']);
		$SQL .= ', '.$DB->WertSetzen('XAB','N0',0,false);		// Immer ohne Vorschau anlegen
		$SQL .= ', '.$DB->WertSetzen('XAB','N0',0,false);		// Ueverschrift auf Standard
		$SQL .= ', (SELECT LISTAGG(XDF_KEY,\',\') WITHIN GROUP (ORDER BY XDF_SORTIERUNG, XDF_KEY) FROM EXPORTDATENFELDER WHERE XDF_XDE_KEY = '.$DB->WertSetzen('XAB', 'N0', $_POST['txtXDE_KEY']).")";
		$SQL .= ','.$DB->WertSetzen('XAB','T',$AWISBenutzer->BenutzerName());
		$SQL .= ',SYSDATE)';
		
		$DB->Ausfuehren($SQL,'',true,$DB->BindeVariablen('XAB'));
		
		$SQL = 'SELECT seq_XAB_KEY.CurrVal AS KEY FROM DUAL';
		$rsKey = $DB->RecordSetOeffnen($SQL);
		
		$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
	}
	else 
	{
		$SQL = 'UPDATE ExportAbfragen';
		$SQL .= ' SET XAB_VORSCHAU = '.$DB->WertSetzen('XAB','N0',$_POST['txtXAB_VORSCHAU'],false);
		$SQL .= ' , XAB_BEZEICHNUNG = '.$DB->WertSetzen('XAB','T',$_POST['txtXAB_BEZEICHNUNG'],false);
		if(isset($_POST['txtXAB_UEBERSCHRIFT']))
		{
			$SQL .= ' , XAB_UEBERSCHRIFT = '.$DB->WertSetzen('XAB','T',$_POST['txtXAB_UEBERSCHRIFT'],false);			
		}
		if(isset($_POST['txtXAB_TRENNZEICHEN_CSV']))
		{
			$SQL .= ' , XAB_TRENNZEICHEN_CSV = \''.str_replace("'", '"', $_POST['txtXAB_TRENNZEICHEN_CSV']).'\'';
		}


		$SQL .= ', XAB_USER = '.$DB->WertSetzen('XAB','T',$AWISBenutzer->BenutzerName());
		$SQL .= ', XAB_USERDAT = SYSDATE';
		$SQL .= ' WHERE XAB_KEY = '.$DB->WertSetzen('XAB','N0',$AWIS_KEY1);
		$DB->Ausfuehren($SQL,'',true,$DB->BindeVariablen('XAB'));

        $SQL = 'DELETE FROM ExportAbfragenBenutzer WHERE XAN_XAB_KEY = ' . $DB->WertSetzen('XAN', 'N0', $AWIS_KEY1);
        $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('XAN'));

		if (isset($_POST['txtXBN_KEY'])) {

            $arrBenutzer = $_POST['txtXBN_KEY'];

            foreach ($arrBenutzer as $key => $xbn_key) {

                $SQL = 'INSERT INTO ExportAbfragenBenutzer (';
                $SQL .= '  XAN_XBN_KEY';
                $SQL .= ' ,XAN_XAB_KEY';
                $SQL .= ' ,XAN_USER';
                $SQL .= ' ,XAN_USERDAT';
                $SQL .= ') VALUES (';
                $SQL .= '' . $DB->WertSetzen('XAN', 'N0', $xbn_key);
                $SQL .= ',' . $DB->WertSetzen('XAN', 'N0', $AWIS_KEY1);
                $SQL .= ',' . $DB->WertSetzen('XAN', 'T', $AWISBenutzer->BenutzerName());
                $SQL .= ',SYSDATE)';

                $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('XAN'));
            }
        }
	}

	//
	// Bedingungen hinzuf�gen
	//
	$Felder = $Form->NameInArray($_POST, 'txtBedingung_',1,1);
	if($AWIS_KEY1 != '' AND $Felder!='')
	{
		$SQL = 'SELECT max(XAK_GRUPPE) AS Gruppe, max(XAK_Reihenfolge) AS Reihenfolge ';
		$SQL .= ' FROM ExportAbfragenKriterien ';
		$SQL .= ' WHERE XAK_XAB_KEY='.$DB->WertSetzen('XAK','N0',$AWIS_KEY1);
		$rsXAK = $DB->RecordSetOeffnen($SQL,$DB->BindeVariablen('XAK'));
		$Gruppe = $rsXAK->FeldInhalt('GRUPPE')+1;
		$Reihenfolge = $rsXAK->FeldInhalt('REIHENFOLGE')+1;
		
		$Felder = explode(';',$Felder);
		foreach($Felder as $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			$Key = $FeldTeile[1];
			if(intval($_POST['txtBedingung_'.$Key])>0 OR ($_POST['txtBedingung_'.$Key]==-1 AND $_POST['txtWert_'.$Key]!=''))
			{
				$SQL = 'INSERT INTO ExportAbfragenKriterien';
				$SQL .= '(XAK_XAB_KEY, XAK_XDF_KEY, XAK_VERKNUEPFUNG, XAK_WERT, XAK_GRUPPE';
				$SQL .= ',XAK_BEDINGUNGGRUPPE, XAK_REIHENFOLGE';
				$SQL .= ',XAK_USER, XAK_USERDAT';
				$SQL .= ')VALUES ('.$DB->WertSetzen('XAK','N0',$AWIS_KEY1);
				$SQL .= ', '.$DB->WertSetzen('XAK','N0',$Key);
				$SQL .= ', '.$DB->WertSetzen('XAK','N0',abs($_POST['txtBedingung_'.$Key]));		// -1 ist f�r die Pflichtparameter
				$SQL .= ', '.$DB->WertSetzen('XAK','T',$_POST['txtWert_'.$Key]);
				$SQL .= ', '.$DB->WertSetzen('XAK','N0',$Gruppe);
				$SQL .= ', '.$DB->WertSetzen('XAK','T',$_POST['txtBedingungGruppe']);
				$SQL .= ', '.$DB->WertSetzen('XAK','N0',$Reihenfolge++);
				$SQL .= ', '.$DB->WertSetzen('XAK','T',$AWISBenutzer->BenutzerName());
				$SQL .= ',SYSDATE';
				$SQL .= ')';
		
				$DB->Ausfuehren($SQL,'',true,$DB->BindeVariablen('XAK'));
			}
		}
	}

	//
	// Anzuzeigende Felder speichern
	//
	$Felder = $Form->NameInArray($_POST, 'txtAnzeigen_',1,1);
	if($AWIS_KEY1 != '' AND $Felder!='')
	{
		$Felder = explode(';',$Felder);
		$FeldListe='';
		foreach($Felder as $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			if($_POST[$Feld]==1)
			{
				$FeldListe .= ','.$FeldTeile[1];
			}
		}
		
		$SQL = 'UPDATE ExportAbfragen';
		$SQL .= ' SET XAB_FELDLISTE = '.$DB->WertSetzen('XAB','T',substr($FeldListe,1));
		$SQL .= ' WHERE XAB_KEY = '.$DB->WertSetzen('XAB','N0',$AWIS_KEY1);
		$DB->Ausfuehren($SQL,'',true,$DB->BindeVariablen('XAB'));
	}
	
	// Gruppierungen
	$Felder = $Form->NameInArray($_POST, 'txtgrp_',1,1);
	if($AWIS_KEY1 != '' AND $Felder!='')
	{
		$Felder = explode(';',$Felder);
		$FeldListe='';
		foreach($Felder as $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			if($_POST[$Feld]=='on')
			{
				$FeldListe .= ','.$FeldTeile[1];
			}
		}
		
		$SQL = 'UPDATE ExportAbfragenKriterien';
		$SQL .= ' SET XAK_GRUPPE = (SELECT MIN(XAK_GRUPPE) FROM ExportAbfragenKriterien WHERE XAK_KEY IN ('.substr($FeldListe,1).'))';
		$SQL .= '  WHERE XAK_KEY IN ('.substr($FeldListe,1).')';
		$DB->Ausfuehren($SQL,'',true);
	}
	
	//************************************************
	// Datenexport Termine
	//************************************************
	$Felder = $Form->NameInArray($_POST, 'txtXDZ_', 2,1);
	if(!empty($Felder))
	{
	    
		// Servicetage zusammensetzen
		$TageDerKW = $Form->NameInArray($_POST, 'txtTage_',2,1);
		$Laenge = 8;
		$Form->DebugAusgabe(2,$_POST,$TageDerKW);
		if(empty($TageDerKW))
		{
		    $TageDerKW = $Form->NameInArray($_POST, 'txtXDZ_Tage_',2,1);
		    $Laenge = 12;
		}
		$ServiceTage = '';
		foreach($TageDerKW as $Tag)
		{
			$ServiceTage .= ';'.substr($Tag, $Laenge);	
		}
		if($ServiceTage=='')
		{
			$ServiceTage=6;			// Wenn nix angegeben wurde auf Sonntag
		}
		$Form->DebugAusgabe(2,$TageDerKW,$ServiceTage);
		
		if($_POST['txtXDZ_KEY']=='')
		{
			$SQL = 'INSERT INTO EXPORTABFRAGENZEITPUNKTE(';
			$SQL .= 'XDZ_XAB_KEY,XDZ_PRIORITAET,XDZ_NAECHSTERSTART,XDZ_STANDARDINTERVALL';
			$SQL .= ',XDZ_SERVICETAGE,XDZ_INTERVALL,XDZ_ZEITVON,XDZ_ZEITBIS,XDZ_XBN_KEY';
			$SQL .= ',XDZ_BENACHRICHTIGUNG,XDZ_DATEINAME';
			$SQL .= ',XDZ_USER,XDZ_USERDAT)';
			$SQL .= ' VALUES (';
			$SQL .= ' '.$DB->WertSetzen('XDZ', 'N0', $_POST['txtXAB_KEY']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'N0', $_POST['txtXDZ_PRIORITAET']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'DU', $_POST['txtXDZ_NAECHSTERSTART']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'N0', $_POST['txtXDZ_STANDARDINTERVALL']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'T', $ServiceTage);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'N0', $_POST['txtXDZ_INTERVALL']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'T', isset($_POST['txtXDZ_ZEITVON'])?$_POST['txtXDZ_ZEITVON']:'02:00',false);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'T', isset($_POST['txtXDZ_ZEITBIS'])?$_POST['txtXDZ_ZEITBIS']:'23:00',false);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'N0', $_POST['txtXDZ_XBN_KEY']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'N0', $_POST['txtXDZ_BENACHRICHTIGUNG']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'T', $_POST['txtXDZ_DATEINAME']);
			$SQL .= ', '.$DB->WertSetzen('XDZ', 'N0', $AWISBenutzer->BenutzerName());
			$SQL .= ', SYSDATE)';
			
			$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('XDZ'));
			
			$SQL = 'SELECT seq_XDZ_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			
			$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
		}
		else 
		{
			$FehlerListe = array();
			$UpdateFelder = '';

			$rsXDZ = $DB->RecordSetOeffnen('SELECT * FROM EXPORTABFRAGENZEITPUNKTE WHERE XDZ_KEY=' . $_POST['txtXDZ_KEY'] . '');

			$_POST['txtXDZ_SERVICETAGE']=$ServiceTage;
			$_POST['oldXDZ_SERVICETAGE']=$rsXDZ->FeldInhalt('XDZ_SERVICETAGE');
			$Felder[] = 'txtXDZ_SERVICETAGE';
			
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$DBFormat = $rsXDZ->FeldInfo($FeldName,'TypKZ');
					if($FeldName=='XDZ_NAECHSTERSTART')
					{
					    $DBFormat='DU';
					}
					
					$WertNeu=$DB->FeldInhaltFormat($DBFormat,$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($DBFormat,$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($DBFormat,$rsXDZ->FeldInhalt($FeldName),true);
					//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';
			
							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}
			
			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsXDZ->FeldInhalt('XDZ_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE EXPORTABFRAGENZEITPUNKTE SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', XDZ_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', XDZ_userdat=sysdate';
				$SQL .= ' WHERE XDZ_key=0' . $_POST['txtXDZ_KEY'] . '';
				$DB->Ausfuehren($SQL,'',true);
			}
			
			$AWIS_KEY2 = $_POST['txtXDZ_KEY'];
		}		
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>