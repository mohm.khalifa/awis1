<?php
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;

try
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('SCH', '%');
    $TextKonserven[]=array('Wort', 'lbl_zurueck');
    $TextKonserven[]=array('Wort', 'lbl_suche');
    $TextKonserven[]=array('Wort', 'lbl_trefferliste');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Fehler', 'err_keineDaten');
    $TextKonserven[]=array('PVS','lst_Waehlen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht25040=$AWISBenutzer->HatDasRecht(25040);
    if($Recht25040 == 0)
    {
        $Form->Fehler_KeineRechte();
    }
    elseif(isset($_POST['cmdSpeichern_x']))
    {
        include('schluesselzentrale_Speichern.php');
    }
    
    echo "<form name=frmDetails_Bearbeiten method=post action=./schluesselzentrale_Speichern.php>";
    
    if(isset($_POST['cmdSuche_x']))
    {
        $Param = array();
        $Param['KEY']=0;			

        $Param['SCH_Key']=$_POST['sucSCH_Key'];	
        $Param['TUR_Key']=$_POST['sucTUR_Key'];	
        $Param['KON_Key']=$_POST['sucKON_Key'];	
        
        $Param['SCH_Historie']=(isset($_POST['sucSCH_Historie'])?'on':'');
        $Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');

        $Param['ORDERBY']='';
        $Param['BLOCK']='';
        $Param['KEY']=0;	
        
        $AWISBenutzer->ParameterSchreiben("Formular_SCH",serialize($Param));
    }
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_SCH"));

    //Infozeile mit Daten zur letzten �nderung durch einen Mitarbeiter und Button zur�ck zur Suchseite
    $Felder = array();
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./schluesselzentrale_Main.php?cmdAktion=Details&Seite=Listenansicht' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=T title=' . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0? $rsUNF->FeldInhalt('UNF_USER'): $AWISBenutzer->BenutzerName()));	// bei Edit => User aus DB; bei New => aktUser 
    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 > 0? $rsUNF->FeldInhalt('UNF_USERDAT'): date('d.m.Y H:i:s')));			// bei Edit => Dat aus DB; bei New => aktDat
    $Form->InfoZeile($Felder, '');

    $Fehler = '';
    
    $SCH_Key = '';
    $MAS_Key = '';
    if(isset($_GET['SCH_KEY']))
    {
        $SCH_Key = $_GET['SCH_KEY'];
    }

    elseif(!isset($_GET['SCH_KEY']))	
    {
        // Pr�fen ob MAS_KEY �bergeben wird
        if(!isset($_GET['MAS_KEY']))	
        {
            //Wen kein Parameter �bergeben wurde Fehlermeldung zeigen und Skript abbrechen  
            $Fehler .= $AWISSprachKonserven['Fehler']['err_keineDaten'].'<br>';
            die('<span class=HinweisText>'.$Fehler.'</span>');
        }
        else 
        {
            //Wenn Parameter �bergeben wurden MAS_Key aus Listenansicht in Variable schreiben
            $MAS_Key = $_GET['MAS_KEY'];
        }
    }
    
    if(!isset($_GET['Sort']))
    {        
        if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
        {
                $ORDERBY = $Param['ORDERBY'];
        }
        else
        {
            if (isset($Param['KON_Key']))
            {
                $ORDERBY = ' SCH_Bezeichnung';
            }
            elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1)
            {
                $ORDERBY = ' KON_Name1';
            }
        }		
    }
    else
    {
        $ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
    }

    $Form->Formular_Start();
    
    $BreiteTL_vorne = 120;
    $BreiteTF_vorne = 300;
    //Unterscheidung nach welchen Parametern gesucht wurde, nach Schl�ssel bzw. T�r oder nach Mitarbeiter
    if($SCH_Key=='')
    {
        if ($Param['KON_Key']!=0)
        {
            //SQL f�r die Daten, die in der zweiten Registerebene stehen sollen
            $SQL = 'SELECT sch.SCH_Bezeichnung, sgr.SGR_Bezeichnung, tur.TUR_Bezeichnung, ';
            $SQL.= '\'Bauteil \' ||tur.TUR_Bauteil || \', Stockwerk \' || tur.TUR_Stockwerk || \', B�ro \' || tur.TUR_Raum, ';
            $SQL.= 'mas.MAS_Ausgabedatum, mas.MAS_Rueckgabedatum, mas.MAS_Verlustdatum, mas.MAS_Bemerkung ';
            $SQL.= 'FROM SchlSchluessel sch Join SchlMitarbeiterSchluessel mas on sch.SCH_Key = mas.MAS_SCH_Key ';
            $SQL.= 'JOIN SchlSchluesselGruppen sgr on sch.SCH_SGR_Key = sgr.SGR_Key ';
            $SQL.= 'JOIN SchlTueren tur on sch.SCH_TUR_Key = tur.TUR_Key ';
            $SQL.= 'WHERE mas.MAS_Key ='.$MAS_Key;

            $rsPEI  = $DB->RecordSetOeffnen($SQL);

            //Erstellen der Label und Felder auf der zweiten Registerkartenebene
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_SchluesselNr'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('SchluesselNr', $rsPEI->Feldinhalt('1'), '', $BreiteTF_vorne);
            $Form->Erstelle_HiddenFeld('Key', 1);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Schliessgruppe'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Schliessgruppe', $rsPEI->Feldinhalt('2'), '', $BreiteTF_vorne);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Tuer'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Tuer', $rsPEI->Feldinhalt('4'), '', $BreiteTF_vorne);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_TuerBemerkung'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('TuerBemerkung', $rsPEI->Feldinhalt('3'), '', $BreiteTF_vorne);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Ausgabedatum'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Ausgabedatum', $rsPEI->FeldInhalt('5'), '', $BreiteTF_vorne, false,'','','', $Format='D');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Rueckgabedatum'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Rueckgabedatum', $rsPEI->FeldInhalt('6'), '', $BreiteTF_vorne, true,'','','', $Format='D');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Verlustdatum'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Verlustdatum', $rsPEI->FeldInhalt('7'), '', $BreiteTF_vorne, true,'','','', $Format='D');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_TuerBemerkung'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Bemerkung', $rsPEI->FeldInhalt('8'), '50', 200, true,'','','', $Format='T');
            $Form->ZeileEnde();
        }   
        elseif (is_numeric($Param['SCH_Key'])==1  or is_numeric($Param['TUR_Key'])==1 )
        {
            var_dump($_POST);
            //SQL f�r die Daten, die in der zweiten Registerebene stehen sollen
            $SQL = 'SELECT kon.KON_Name1, kon.KON_Name2, mas.MAS_Ausgabedatum, mas.MAS_Rueckgabedatum, ';
            $SQL.= 'mas.MAS_Verlustdatum, mas.MAS_Bemerkung ';
            $SQL.= 'FROM Kontakte kon Join SchlMitarbeiterSChluessel mas on kon.KON_Key = mas.MAS_KON_Key ';
            $SQL.= 'WHERE mas.MAS_Key ='.$MAS_Key;

            $rsPEI  = $DB->RecordSetOeffnen($SQL);

            //vorhandenen DS bearbeiten
            $Flag = true;

            //Erstellen der Label und Felder auf der zweiten Registerkartenebene
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Nachname'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Nachname', $rsPEI->Feldinhalt('1'), '', $BreiteTF_vorne);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Vorname'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Vorname', $rsPEI->FeldInhalt('2'), '', $BreiteTF_vorne);
            $Form->ZeileEnde();
        
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Ausgabedatum'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Ausgabedatum', $rsPEI->FeldInhalt('3'), 20, $BreiteTF_vorne, $Flag,'','','', 'D');
            $Form->Erstelle_HiddenFeld('MAS_Key', $MAS_Key);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Rueckgabedatum'].':',$BreiteTL_vorne);
            //Wenn entweder das R�ckgabe- oder das Verlustdatum gef�llt ist d�rfen diese beiden Felder nicht mehr ge�ndert werden!
            /*if ($rs->FeldInhalt('4')!= '' or $rs->FeldInhalt('5') != '')
            {
                $Flag = false;
            }
            else
            {
                $Flag = true;
            }*/
            $Form->Erstelle_TextFeld('Rueckgabedatum', $rsPEI->FeldInhalt('4'), 20, $BreiteTF_vorne, $Flag,'','','', 'D');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Verlustdatum'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Verlustdatum', $rsPEI->FeldInhalt('5'), 20, $BreiteTF_vorne, $Flag,'','','', 'D');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_TuerBemerkung'].':',$BreiteTL_vorne);
            $Form->Erstelle_TextFeld('Bemerkung', $rsPEI->FeldInhalt('6'), 20, 200, true,'','','', 'T');
            $Form->ZeileEnde();
        }
    }
    elseif(isset($_GET['SCH_KEY']))
    {
        //hier dann Formular f�r die Neuanlage erstellen!!
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Mitarbeiter'].':',$BreiteTL_vorne);
         
        $SQL = 'SELECT KON_Key, KON_Name1|| \', \' || KON_Name2 FROM Kontakte WHERE KON_Name2 != \' \'AND KON_STATUS = \'A\' ORDER BY KON_Name1 ';
        $rsPEI = $DB->RecordSetOeffnen($SQL);   
        $Form->Erstelle_SelectFeld('*KON_Key',(isset($Param['SPEICHERN']) && $Param['SPEICHERN']=='on'?$Param['KON_Key']:''),50,true,$SQL,$AWISSprachKonserven['PVS']['lst_Waehlen']);
        $Form->Erstelle_HiddenFeld('SCH_Key', $SCH_Key);
        
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Ausgabedatum'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('Ausgabedatum', '', '', $BreiteTF_vorne, true,'','','', 'D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Rueckgabedatum'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('Rueckgabedatum', '', '', $BreiteTF_vorne, true,'','','', 'D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_Verlustdatum'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('Verlustdatum', '', '', $BreiteTF_vorne, true,'','','', 'D');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SCH']['SCH_TuerBemerkung'].':',$BreiteTL_vorne);
        $Form->Erstelle_TextFeld('Bemerkung', '', '50', 200, true,'','','', 'T');
        $Form->ZeileEnde();
    }
    //Schaltfl�chen
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    //$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['SCH']['SCH_Speichern']);
   
    if ($Recht25040 >= 0)
    {                                             
        
    }       
    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');
    $Form->SetzeCursor($AWISCursorPosition);
    $Form->Formular_Ende();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
            $Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
    }
    else
    {
            echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>