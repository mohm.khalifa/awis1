<?php
require_once('awisTecdocExport.php');
require_once 'awisWerkzeuge.inc';
try {

    $Benutzer = 'awis_jobs';
    $DebugLevel = 1;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $Export = new awisTecdocExport($Benutzer);
    $AWISWerkzeug = new awisWerkzeuge();
    $Export->DebugLevel($DebugLevel);

    $Dateiname = 's4t_tecdoc_mapping_awis_' . date('Ymd') . '.csv';
    if ($AWISWerkzeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV or $AWISWerkzeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_SHUTTLE) {
        $Exportpfad = '/win/data_in/AWIS_REDSYS/';
    } else {
        $Exportpfad = '/daten/';
    }

    $Exportpfad .= $Dateiname;

    $Export->ExportMapping($Exportpfad, ';', PHP_EOL, true);
} catch (Exception $ex) {
    echo PHP_EOL . 'Export Tecdocmapping S4T: Fehler: ' . $ex->getMessage() . PHP_EOL;

    $AWISWerkzeug = new awisWerkzeuge();
    $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'), 'Export Tecdocmapping S4T', 'Fehler :' . $ex->getMessage(), 3, '', 'awis@de.atu.eu');

    echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
    if (is_a($ex, awisException::class)) {
        echo 'FEHLER:' . $ex->getSQL() . PHP_EOL;
    }
    echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
    echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
    echo 'Datei: ' . $ex->getFile() . PHP_EOL;
}

?>