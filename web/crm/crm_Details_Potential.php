<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $CursorFeld;

$TextKonserven = array();
$TextKonserven[]=array('CPO','%');
$TextKonserven[]=array('CFZ','%');
$TextKonserven[]=array('CFT','%');
$TextKonserven[]=array('CVG','%');
$TextKonserven[]=array('Wort','Summe');
$TextKonserven[]=array('Liste','lst_JaNein');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3706 = awisBenutzerRecht($con,3706,$AWISBenutzer->BenutzerName());
if($Recht3706==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//$AWIS_KEY1 = awis_BenutzerParameter($con, "AktuellerCAD" , $AWISBenutzer->BenutzerName());

$SQL = 'SELECT CRMPotential.*';
$SQL .= ' FROM CRMPotential';
$SQL .= ' WHERE CPO_CAD_KEY=:var_N0_CPO_CAD_KEY';
$BindeVariablen = array();
$BindeVariablen['var_N0_CPO_CAD_KEY']=$AWIS_KEY1;
if(isset($_GET['CPO_KEY']))
{
	$SQL .= ' AND CPO_KEY=:var_N0_CPO_KEY';
	$BindeVariablen['var_N0_CPO_KEY']=intval($_GET['CPO_KEY']);
}
$SQL .= ' ORDER BY CPO_STAND DESC';
$rsCPO = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
$rsCPOZeilen = $awisRSZeilen;

// Potential da?
if(!isset($rsCPO['CPO_KEY'][0]) OR isset($_POST['cmdCPONeu_x']))
{
	$SQL = 'INSERT INTO CRMPotential';
	$SQL .= '(CPO_CAD_KEY, CPO_STAND, CPO_USER, CPO_UserDat)';
	$SQL .= 'VALUES(0'.$AWIS_KEY1.', SYSDATE, \''.$AWISBenutzer->BenutzerName().'\', SYSDATE)';
	if(awisExecute($con,$SQL)==False)
	{
		awisErrorMailLink('200711101519',1,$awisDBError['message'],'CPO1');
		awisLogoff($con);
		die();
	}
	$SQL = 'SELECT seq_CPO_KEY.CurrVal AS KEY FROM DUAL';
	$rsKey = awisOpenRecordset($con,$SQL);
	$AWIS_KEY2=$rsKey['KEY'][0];
	
	$SQL = 'SELECT CRMPotential.*';
	$SQL .= ' FROM CRMPotential';
	$SQL .= ' WHERE CPO_KEY=:var_N0_CPO_KEY';
	$SQL .= ' ORDER BY CPO_STAND DESC';
	$BindeVariablen = array();
	$BindeVariablen['var_N0_CPO_KEY']=$AWIS_KEY2;
	$rsCPO = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);
	$rsCPOZeilen = 1;
}
else 
{
	$AWIS_KEY2=$rsCPO['CPO_KEY'][0];
}

// Jetzt Daten da?
if(isset($rsCPO['CPO_KEY'][0]))
{
	// Pruefen, ob alle Angaben da sind
	// Teil1: Alle Fahrzeuge fuer alle Vertriebsgruppen
	$SQL = "BEGIN CRMPOTENTIALAUBAUEN(" . $AWIS_KEY2 .");";
	$SQL .= " END;";

	$Erg = awisExecute($con, $SQL);
	if($Erg===FALSE)
	{
		awisErrorMailLink("200711101545",1,$awisDBError['message'],'CPO2');
		awisLogoff($con);
		die();
	}
}

//$EditModus=($Recht3706%6);
$EditModus=(($Recht3706&6)!=0);
//awis_Debug(1,$EditModus,$Recht3706);
// Fahrzeugtypen oeffen fuer spaeter
$SQL = 'SELECT CFT_ID, CFT_BEZEICHNUNG ';
$SQL .= ' FROM CRMFahrzeugtypen';
$SQL .= ' WHERE CFT_STATUS=\'A\'';
$SQL .= ' ORDER BY CFT_ID';
$rsCFT = awisOpenRecordset($con, $SQL);
$rsCFTZeilen = $awisRSZeilen;

awis_FORM_FormularStart();
echo '<input type=hidden name=txtCPO_KEY value=0'.$rsCPO['CPO_KEY'][0].'>';

// Algemeine Infos
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['CPO_STAND'].':',220);
awis_FORM_Erstelle_TextFeld('CPO_STAND',$rsCPO['CPO_STAND'][0],10,190,$EditModus,'','','','D');
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['CPO_BEMERKUNG'].':',220);
awis_FORM_Erstelle_TextFeld('CPO_BEMERKUNG',$rsCPO['CPO_BEMERKUNG'][0],80,500,$EditModus,'','','','T');
$CursorFeld='txtCPO_BEMERKUNG';
awis_FORM_ZeileEnde();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['CPO_FAHRZEUGMARKEN'].':',220);
awis_FORM_Erstelle_TextFeld('CPO_FAHRZEUGMARKEN',$rsCPO['CPO_FAHRZEUGMARKEN'][0],80,500,$EditModus,'','','','T');
awis_FORM_ZeileEnde();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['CPO_KMLEISTUNG'].':',220);
awis_FORM_Erstelle_TextFeld('CPO_KMLEISTUNG',$rsCPO['CPO_KMLEISTUNG'][0],20,190,$EditModus,'','','','T');
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['CPO_DURCHSCHNITTSALTER'].':',220);
awis_FORM_Erstelle_TextFeld('CPO_DURCHSCHNITTSALTER',$rsCPO['CPO_DURCHSCHNITTSALTER'][0],20,200,$EditModus,'','','','T');
awis_FORM_ZeileEnde();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['CPO_LEASINGGEBER'].':',220);
awis_FORM_Erstelle_TextFeld('CPO_LEASINGGEBER',$rsCPO['CPO_LEASINGGEBER'][0],80,500,$EditModus,'','','','T');
awis_FORM_ZeileEnde();
awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['CPO_ABWICKLUNG'].':',220);
awis_FORM_Erstelle_TextFeld('CPO_ABWICKLUNG',$rsCPO['CPO_ABWICKLUNG'][0],80,500,$EditModus,'','','','T');
awis_FORM_ZeileEnde();

// Falls mehr Versionen da sind, alle anzeigen
if($rsCPOZeilen>1)
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPO']['AlteStaende'].':',220);
	for($CPOZeile=1;$CPOZeile<$rsCPOZeilen;$CPOZeile++)
	{
		$Link = './crm_Main.php?cmdAktion=Details&Seite=Potential&CPO_KEY=0'.$rsCPO['CPO_KEY'][$CPOZeile];
		awis_FORM_Erstelle_TextFeld('#CPO_STAND'.$CPOZeile,$rsCPO['CPO_STAND'][$CPOZeile],10,120,false,'','',$Link,'T');
	}
	awis_FORM_ZeileEnde();
}


awis_FORM_Trennzeile();

awis_FORM_ZeileStart();

awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CVG']['CVG_BEZEICHNUNG'],220);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CFT']['CFT_BEZEICHNUNG'],80);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CFZ']['CFZ_ANZAHL'],90);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CFZ']['CFZ_ANTEIL'],90);

// Einzelne Bereiche als Ueberschrift
$SQL = 'SELECT * ';
$SQL .= ' FROM CRMLeistungsBereiche';
$SQL .= ' WHERE CLB_STATUS = \'A\'';
$SQL .= ' ORDER BY CLB_ID';
$rsCLB = awisOpenRecordset($con, $SQL);
$rsCLBZeilen = $awisRSZeilen;
$Breite=intval((1000-510)/$rsCLBZeilen); // Breite f�r eine Spalte

for($CLBZeile=0;$CLBZeile<$rsCLBZeilen;$CLBZeile++)
{
	awis_FORM_Erstelle_Liste_Ueberschrift($rsCLB['CLB_BEZEICHNUNG'][$CLBZeile],$Breite);	
}
// Summen
//awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPO']['UmsatzFahrzeug'],90);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPO']['UmsatzGesamt'],90);
awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPO']['UmsatzATU'],90);

awis_FORM_ZeileEnde();

$SQL = 'SELECT *';
$SQL .= ' FROM vCRMPotential';
$SQL .= ' WHERE CFZ_CPO_KEY=:var_N0_CFZ_CPO_KEY';
$SQL .= ' ORDER BY CVG_ID, CFT_ID, CLB_ID';		// NICHT aendern!
$BindeVariablen = array();
$BindeVariablen['var_N0_CFZ_CPO_KEY']=$AWIS_KEY2;
$rsCFZ = awisOpenRecordset($con, $SQL,'','',$BindeVariablen);
$rsCFZZeilen = $awisRSZeilen;

$UmsatzSummen = array();
$UmsatzSummen['Gesamt']=0;
$UmsatzSummen['ATU']=0;

for($CFZZeile=0;$CFZZeile<$rsCFZZeilen;$CFZZeile++)
{
	$UmsatzProFahrzeug=0;

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_ListenFeld('#CVG_BEZEICHNUNG',$rsCFZ['CVG_BEZEICHNUNG'][$CFZZeile],5,220,false,($CFZZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('#CFT_BEZEICHNUNG',$rsCFZ['CFT_BEZEICHNUNG'][$CFZZeile],5,80,false,($CFZZeile%2),'','','T');
	awis_FORM_Erstelle_ListenFeld('CFZ_ANZAHL_'.$rsCFZ['CFZ_KEY'][$CFZZeile],$rsCFZ['CFZ_ANZAHL'][$CFZZeile],5,90,$EditModus,($CFZZeile%2),'','','N0');
	awis_FORM_Erstelle_ListenFeld('CFZ_ANTEIL_'.$rsCFZ['CFZ_KEY'][$CFZZeile],$rsCFZ['CFZ_ANTEIL'][$CFZZeile],5,90,$EditModus,($CFZZeile%2),'','','N0');
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	for($i=0;$i<$rsCLBZeilen;$i++)		// F�r alle Bereiche eine Spalte
	{
		if($rsCFZ['CPD_STATUS'][$CFZZeile]==1)		// Umsatz f�r diesen Bereich m�glich?
		{
			$UmsatzProFahrzeug+=((isset($rsCFZ['CPV_BETRAG'][$CFZZeile])AND $rsCFZ['CPV_BETRAG'][$CFZZeile]>0)?$rsCFZ['CPV_BETRAG'][$CFZZeile]:$rsCFZ['CLV_BETRAG'][$CFZZeile]);
		}

		awis_FORM_Erstelle_SelectFeld('CPD_STATUS_'.$rsCFZ['CPD_KEY'][$CFZZeile],$rsCFZ['CPD_STATUS'][$CFZZeile],$Breite+4,$EditModus,null,'','','','','',$Daten,'');
		$CFZZeile++;
	}
	$CFZZeile--;		//Wieder eins abziehen wg. der Schleife
	
		// Ums�tze berechnen
	//awis_FORM_Erstelle_ListenFeld('#UmsatzProFz',$UmsatzProFahrzeug,5,90,false,($CFZZeile%2),'','','N2','R');
	$UmsatzGesamt = ($UmsatzProFahrzeug*$rsCFZ['CFZ_ANZAHL'][$CFZZeile]);
	$UmsatzATU = ($rsCFZ['CFZ_ANTEIL'][$CFZZeile]==0?0:($UmsatzProFahrzeug*$rsCFZ['CFZ_ANZAHL'][$CFZZeile])*($rsCFZ['CFZ_ANTEIL'][$CFZZeile]/100.0));
	
	$UmsatzSummen['Gesamt']+=$UmsatzGesamt;
	$UmsatzSummen['ATU']+=$UmsatzATU;
	
	awis_FORM_Erstelle_ListenFeld('#UmsatzGES',$UmsatzGesamt,5,90,false,($CFZZeile%2),'','','N0','R',$AWISSprachKonserven['CPO']['UmsatzFahrzeug'].': '.awis_FeldInhaltFormat('N2',$UmsatzProFahrzeug));
	awis_FORM_Erstelle_ListenFeld('#UmsatzATU',$UmsatzATU,5,90,false,($CFZZeile%2),'','','N0','R');
	
	awis_FORM_ZeileEnde();
}

awis_FORM_ZeileStart();
$SQL = 'SELECT SUM(CFZ_ANZAHL) AS Anzahl from (SELECT DISTINCT CFZ_ANZAHL, CFZ_KEY, CPO_CAD_KEY FROM CRMPOTENTIAL
           INNER JOIN CRMPOTENTIALDETAILS ON CPD_CPO_KEY = CPO_KEY
           INNER JOIN CRMFAHRZEUGE ON CPD_CFZ_KEY = CFZ_KEY
) Daten WHERE CPO_CAD_KEY = :var_N0_CPO_CAD_KEY GROUP BY CPO_CAD_KEY';
$BindeVariablen = array();
$BindeVariablen['var_N0_CPO_CAD_KEY']=$AWIS_KEY1;
$rsSumme = awisOpenRecordset($con,$SQL,'','',$BindeVariablen);

awis_FORM_Erstelle_ListenFeld('#AnzahlKFZ',$rsSumme['ANZAHL'][0],5,360,false,($CFZZeile%2),'','','N0','R');
awis_FORM_Erstelle_ListenFeld('#UmsatzSumme',$AWISSprachKonserven['Wort']['Summe'].':',5,($rsCLBZeilen*90)+180,false,($CFZZeile%2),'','','T','R');
awis_FORM_Erstelle_ListenFeld('#UmsatzSumGES',$UmsatzSummen['Gesamt'],5,90,false,($CFZZeile%2),'','','N0','R');
awis_FORM_Erstelle_ListenFeld('#UmsatzSumATU',$UmsatzSummen['ATU'],5,90,false,($CFZZeile%2),'','','N0','R');
awis_FORM_ZeileEnde();

//************************************************************************************
//Umsatzvorgaben pro Fahrzeug
//************************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM CRMPotentialVorgaben';
$SQL .= ' WHERE CPV_CPO_KEY=0'.$AWIS_KEY2;
$rsCPV = awisOpenRecordset($con, $SQL);

for($j=0;$j<$rsCFTZeilen;$j++)		// Alle Fahrzeugtypen 
{
	awis_FORM_ZeileStart();

	awis_FORM_Erstelle_ListenFeld('#CFT_BEZEICHNUNG',$rsCFT['CFT_BEZEICHNUNG'][$j],5,488,false,($j%2),'','','T','R');
	
	$SQL = 'SELECT CRMLeistungsBereichVorgaben.*, CFT_BEZEICHNUNG, CLB_BEZEICHNUNG, CPV_BETRAG, CPV_BEMERKUNG';
	$SQL .= ' FROM CRMLeistungsBereichVorgaben';
	$SQL .= ' INNER JOIN  CRMLeistungsbereiche ON CLV_CLB_ID = CLB_ID AND CLB_STATUS=\'A\'';
	$SQL .= ' INNER JOIN  CRMFahrzeugTypen ON CLV_CFT_ID = CFT_ID AND CFT_STATUS=\'A\'';
	$SQL .= ' LEFT OUTER JOIN CRMPotentialVorgaben ON CPV_CPO_KEY=0'.$AWIS_KEY2.' AND CPV_CFT_ID=CLV_CFT_ID AND CPV_CLB_ID = CLV_CLB_ID';
	$SQL .= ' WHERE CLV_CFT_ID = 0'.$rsCFT['CFT_ID'][$j]. '';
	$SQL .= ' ORDER BY CLV_CLB_ID';
	$rsCLV = awisOpenRecordset($con, $SQL);
	$rsCLVZeilen = $awisRSZeilen;
	for($i=0;$i<$rsCLBZeilen;$i++)		// Alle aktiven Bereiche
	{
		for($k=0;$k<$rsCLVZeilen;$k++)
		{
			if($rsCLV['CLV_CLB_ID'][$k]==$rsCLB['CLB_ID'][$i] AND $rsCLV['CLV_CFT_ID'][$k]==$rsCFT['CFT_ID'][$j])
			{
				if(isset($rsCLV['CPV_BETRAG'][$k]))
				{
					$ToolTipp = $rsCLV['CLB_BEZEICHNUNG'][$i].'/'.$rsCLV['CFT_BEZEICHNUNG'][$i];
					awis_FORM_Erstelle_ListenFeld('CPV_BETRAG_'.$rsCLV['CLV_CLB_ID'][$k].'_'.$rsCLV['CLV_CFT_ID'][$k],(isset($rsCLV['CPV_BETRAG'][$i])?$rsCLV['CPV_BETRAG'][$i]:$rsCLV['CLV_BETRAG'][$i]),5,$Breite,false,($j%2),'','','N2',$ToolTipp);
				}
				else
				{
					$ToolTipp = $rsCLV['CLB_BEZEICHNUNG'][$i].'/'.$rsCLV['CFT_BEZEICHNUNG'][$i];
					awis_FORM_Erstelle_ListenFeld('CPV_BETRAG_'.$rsCLV['CLV_CLB_ID'][$k].'_'.$rsCLV['CLV_CFT_ID'][$k],(isset($rsCLV['CLV_BETRAG'][$i])?$rsCLV['CLV_BETRAG'][$i]:0),5,$Breite,false,($j%2),'','','N2',$ToolTipp);
				}
				break;
			}
		}
		if($k>=$rsCLVZeilen)
		{
			awis_FORM_Erstelle_ListenFeld('#CLV_BETRAG',0,5,$Breite,$EditModus,($j%2),'','','N2');
		}
	}

	awis_FORM_ZeileEnde();
}

// Sonderfelder, 26.04.2012, SK
// Schnelle L�sung gebaut mit Infofeldern um Volumen und Anteil zu Speichern und Potential anzuzeigen

awis_FORM_Trennzeile();

$SQL = 'SELECT *';
$SQL .= ' FROM CRMInfoTypen';
$SQL .= ' LEFT OUTER JOIN CRMInfos ON CIT_ID = CIN_CIT_ID AND CIN_CAD_KEY=:var_N0_CIN_CAD_KEY';
$SQL .= ' WHERE CIT_SEITE = \'Potential\' AND CIT_BEREICH = \'TGH\' AND CIT_STATUS=\'A\'';
$SQL .= ' ORDER BY CIT_SORTIERUNG';
$BindeVariablen = array();
$BindeVariablen['var_N0_CIN_CAD_KEY']=$AWIS_KEY1;
$rsCIN = awisOpenRecordset($con, $SQL,'','',$BindeVariablen);
$rsCINZeilen = $awisRSZeilen;

$Volumen = 0;
$Anteil = 0;
awis_FORM_ZeileStart();
for($CINZeile=0;$CINZeile<$rsCINZeilen;$CINZeile++)
{
	awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][$CINZeile].':',200);
	awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_ZEICHEN'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,'','','',$rsCIN['CIT_FORMAT'][$CINZeile]);
	switch($rsCIN['CIT_ID'][$CINZeile])
	{
		case 7000:
			$Volumen = awisFeldFormat('N2', $rsCIN['CIN_WERT'][$CINZeile]); 
			break;
		case 7001:
			$Anteil = awisFeldFormat('N2', $rsCIN['CIN_WERT'][$CINZeile])/100.0;
			break;
	}
}
awis_FORM_Erstelle_TextLabel('Potential-TGH:',200);
awis_FORM_Erstelle_TextFeld('#PotentialTGH',$Volumen*$Anteil,0,150,false,'','','','N2');
awis_FORM_ZeileEnde();

awis_FORM_Trennzeile();

$SQL = 'SELECT *';
$SQL .= ' FROM CRMInfoTypen';
$SQL .= ' LEFT OUTER JOIN CRMInfos ON CIT_ID = CIN_CIT_ID AND CIN_CAD_KEY=:var_N0_CIN_CAD_KEY';
$SQL .= ' WHERE CIT_SEITE = \'Potential\' AND CIT_BEREICH = \'MIS_Umsatz\' AND CIT_STATUS=\'A\'';
$SQL .= ' ORDER BY CIT_SORTIERUNG';
$BindeVariablen = array();
$BindeVariablen['var_N0_CIN_CAD_KEY']=$AWIS_KEY1;
$rsCIN = awisOpenRecordset($con, $SQL,'','',$BindeVariablen);
$rsCINZeilen = $awisRSZeilen;

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift('MIS - UMSATZ (Stand: ' . $rsCIN['CIN_WERT'][0] . ')',0,'','');
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
for($CINZeile=1;$CINZeile<$rsCINZeilen;$CINZeile++)
{
	awis_FORM_Erstelle_TextLabel($rsCIN['CIT_BEZEICHNUNG'][$CINZeile].':',200);
	awis_FORM_Erstelle_TextFeld('CIN_WERT_'.$rsCIN['CIT_ID'][$CINZeile].'_'.$rsCIN['CIN_KEY'][$CINZeile],$rsCIN['CIN_WERT'][$CINZeile],$rsCIN['CIT_ZEICHEN'][$CINZeile],$rsCIN['CIT_BREITE'][$CINZeile],$EditModus,'','','',$rsCIN['CIT_FORMAT'][$CINZeile]);
	
}

awis_FORM_ZeileEnde();

awis_FORM_FormularEnde();
?>