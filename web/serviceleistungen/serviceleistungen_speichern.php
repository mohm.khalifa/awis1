<?php
global $AWIS_KEY1;
global $AWIS_KEY2;


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	if(isset($_POST['txtSVB_KEY']))
	{
		$AWIS_KEY1=$_POST['txtSVB_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtSVB_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('SVB','SVB_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('SVB_VERTRAGSNUMMER','SVB_CAD_KEY','SVB_DATUMAB','SVB_DATUMBIS','SVB_SVA_KEY');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SVB'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtSVB_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO svcvereinbarungen';
				$SQL .= '(SVB_SVA_KEY,SVB_CAD_KEY,SVB_DATUMAB,SVB_DATUMBIS,SVB_VERTRAGSNUMMER,SVB_KUNDENID,SVB_MELDE_CAD_KEY';
				$SQL .= ',SVB_USER,SVB_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtSVB_SVA_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVB_CAD_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtSVB_DATUMAB'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtSVB_DATUMBIS'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVB_VERTRAGSNUMMER'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVB_KUNDENID'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVB_MELDE_CAD_KEY'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				
				$DB->Ausfuehren($SQL);

				$SQL = 'SELECT seq_SVB_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');				
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsSVT = $DB->RecordSetOeffnen('SELECT * FROM svcvereinbarungen WHERE SVB_Key=' . $_POST['txtSVB_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$rsSVT->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVT->FeldInhalt('SVB_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE svcvereinbarungen SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', SVB_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SVB_userdat=sysdate';
					$SQL .= ' WHERE SVB_KEY=0' . $_POST['txtSVB_KEY'] . '';
					$DB->Ausfuehren($SQL);
					
					if($Form->PruefeDatum($_POST['txtSVB_DATUMBIS'],false,true,true)<>$Form->PruefeDatum($_POST['oldSVB_DATUMBIS'],false,true,true))
					{
						$SQL = 'UPDATE SVCTEILNEHMER';
						$SQL .= ' SET SVT_DATUMBIS = '.$DB->FeldInhaltFormat('D',$_POST['txtSVB_DATUMBIS'],false);
						$SQL .= ' WHERE SVT_DATUMBIS > '.$DB->FeldInhaltFormat('D',$_POST['txtSVB_DATUMBIS'],false);
						$SQL .= ' AND SVT_SVB_KEY = '.$_POST['txtSVB_KEY'] . '';
						$DB->Ausfuehren($SQL);
					}
					
				}
			}
		}
	}

	//***********************************************
	// svcteilnehmer
	//***********************************************
	if(isset($_POST['txtSVT_KEY']))
	{
		$AWIS_KEY2=$_POST['txtSVT_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtSVT_',1,1);

		if($Felder!='')
		{
			$Form->DebugAusgabe(1,$DB->FeldInhaltFormat('T0',$_POST['txtSVT_SMSNUMMER']));
			$_POST['txtSVT_SMSNUMMER']=$Form->Format('T0',$_POST['txtSVT_SMSNUMMER']);
			$Form->DebugAusgabe(1,$_POST['txtSVT_SMSNUMMER']);
				
			
			// Benachrichtigung zusammenbauen
			$BFelder = $Form->NameInArray($_POST, 'txtBENACHRICHTIGUNG',2,1);
			$Pflichtfelder = array('SVT_KENNUNG','SVT_DATUMAB','SVT_DATUMBIS','SVT_BENACHRICHTIGUNG');
			$Benachrichtigung = '';
			foreach($BFelder as $BFeld)
			{
				if($_POST[$BFeld]=='on')
				{
					$IDs = explode('_',$BFeld);
					$Benachrichtigung |= $IDs[1];
					switch($IDs[1])
					{
						case '1':			// Mail
							$Pflichtfelder[]='SVT_MAILADRESSE';
							break;
						case '2':			// SMS
							$Pflichtfelder[]='SVT_SMSNUMMER';
							break;
						case '4':			// Brief
							$Pflichtfelder[]='SVT_NAME';
							$Pflichtfelder[]='SVT_VORNAME';
							$Pflichtfelder[]='SVT_STRASSE';
							$Pflichtfelder[]='SVT_PLZ';
							$Pflichtfelder[]='SVT_ORT';
							break;
					}
				}
			}
			$_POST['txtSVT_BENACHRICHTIGUNG'] = ($Benachrichtigung==''?'':$Benachrichtigung);

			
			// EAN Code auf 13 Stellen f�llen
//			$_POST['txtSVT_KENNUNG'] = str_pad($_POST['txtSVT_KENNUNG'], 13,'0',STR_PAD_LEFT);
				
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('SVT','SVT_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			// Pflichtfelder pr�fen			
			$Fehler = '';
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SVT'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtSVT_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO svcteilnehmer';
				$SQL .= '(SVT_SVB_KEY,SVT_BEZEICHNUNG,SVT_KENNUNG,SVT_DATUMAB,SVT_DATUMBIS,SVT_BEMERKUNG,SVT_BENACHRICHTIGUNG,SVT_MAILADRESSE';
				$SQL .= ',SVT_SMSNUMMER,SVT_NAME,SVT_VORNAME,SVT_ANREDE,SVT_ADRESSZUSATZ,SVT_STRASSE,SVT_HAUSNUMMER,SVT_LAN_CODE,SVT_PLZ';
				$SQL .= ',SVT_ORT,SVT_KFZKENNZEICHEN,SVT_FIN';
				$SQL .= ',SVT_USER, SVT_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtSVT_SVB_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_BEZEICHNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_KENNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtSVT_DATUMAB'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtSVT_DATUMBIS'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_BEMERKUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVT_BENACHRICHTIGUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_MAILADRESSE'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_SMSNUMMER'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_NAME'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_VORNAME'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSVT_ANREDE'])?$_POST['txtSVT_ANREDE']:''),true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_ADRESSZUSATZ'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_STRASSE'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_HAUSNUMMER'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_LAN_CODE'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_PLZ'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVT_ORT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSVT_KFZKENNZEICHEN'])?$_POST['txtSVT_KFZKENNZEICHEN']:''),true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSVT_FIN'])?$_POST['txtSVT_FIN']:''),true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$DB->Ausfuehren($SQL);
				$SQL = 'SELECT seq_SVT_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$BindeVariablen=array();
            	$BindeVariablen['var_N0_SVT_KEY']=$_POST['txtSVT_KEY'];
				$rsSVT = $DB->RecordSetOeffnen('SELECT * FROM svcteilnehmer WHERE SVT_key=:var_N0_SVT_KEY',$BindeVariablen);

				// Ist ein zusammengebautes Feld
				$_POST['oldSVT_BENACHRICHTIGUNG'] = $rsSVT->FeldInhalt('SVT_BENACHRICHTIGUNG');
				$Felder[]='txtSVT_BENACHRICHTIGUNG';
					
				
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$rsSVT->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVT->FeldInhalt('SVT_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE svcteilnehmer SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', SVT_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SVT_userdat=sysdate';
					$SQL .= ' WHERE SVT_key=0' . $_POST['txtSVT_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}

	//***********************************************
	// svcleistungen
	//***********************************************
	if(isset($_POST['txtSVL_KEY']))
	{
		$AWIS_KEY2=$_POST['txtSVL_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtSVL_',1,1);

		if($Felder!='')
		{
			// EAN Code auf 13 Stellen f�llen
			if(isset($_POST['txtSVL_KENNUNG']))
			{
				$_POST['txtSVL_KENNUNG'] = str_pad($_POST['txtSVL_KENNUNG'], 13,'0',STR_PAD_LEFT);
			}
				
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('SVL','SVL_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			// Pflichtfelder pr�fen			
			$Fehler = '';
			$Pflichtfelder = array('SVL_KENNUNG','SVL_DATUMAB','SVL_DATUMBIS','SVL_MAILADRESSE','SVL_BENACHRICHTIGUNG');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SVL'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtSVL_KEY'])==0)
			{
				// Gibt es die Kennung schon einmal?
				$SQL = 'SELECT svt_svb_key';
				$SQL .= ' FROM SVCTeilnehmer';
				$SQL .= ' WHERE SVT_KENNUNG = :var_T_SVT_KENNUNG ';
				$SQL .= ' AND SVT_DATUMAB <= SYSDATE ';
				$SQL .= ' AND SVT_DATUMBIS >= SYSDATE ';
					
				$BindeVariablen = array();
				$BindeVariablen['var_T_SVT_KENNUNG']=$_POST['txtSVL_KENNUNG'];
				$rsPruefung = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
				if(!$rsPruefung->EOF())
				{
					die('Die Kennung '.$_POST['txtSVL_KENNUNG']. ' exisitiert schon im Vertrag-Key: '.$rsPruefung->FeldInhalt('SVT_SVB_KEY'));
				}
				
				$Fehler = '';
				$SQL = 'INSERT INTO svcleistungen';
				$SQL .= '(SVL_SVT_KEY,SVL_DATUM,SVL_FIL_ID,SVL_WANR,SVL_AST_ATUNR,SVL_BEZEICHNUNG,SVL_MENGE';
				$SQL .= ',SVL_VKPREIS,SVL_UMSATZ,SVL_KENNZEICHEN,SVL_KTYPNR,SVL_KUNDENNAME1,SVL_KUNDENNAME2,SVL_PERSNR';
				$SQL .= ',SVL_KENNUNG,SVL_IMPORTZEITPUNKT,SVL_STATUS,SVL_MELDEZEITPUNKT';
				$SQL .= ',SVL_IMPORTID';
				$SQL .= ',SVL_USER, SVL_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtSVT_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtSVL_DATUM'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVL_FIL_ID'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVL_WANR'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVL_AST_ATUNR'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVL_BEZEICHNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVL_MENGE'],false);
				$SQL .= ',' . $DB->FeldInhaltFormat('N2',$_POST['txtSVL_VKPREIS'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N2',$_POST['txtSVL_UMSATZ'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVL_UMSATZ'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVL_KTYPNR'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVL_KUNDENNAME1'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVL_KUNDENNAME2'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVL_PERSNR'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVL_KENNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('DU',$_POST['txtSVL_IMPORTZEITPUNKT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSVL_STATUS'],false);
				$SQL .= ',' . $DB->FeldInhaltFormat('DU',$_POST['txtSVL_MELDEZEITPUNKT'],true);
				$SQL .= ',\'' . microtime() . '\'';
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';

				$DB->Ausfuehren($SQL);
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				if($_POST['txtSVL_SVT_KEY']!='')		// Wurde ein Teilnehmer zugeordnet?
				{
					$_POST['txtSVL_STATUS']=9;			// Auf zugeordnet setzen
				}
				
				$BindeVariablen=array();
            	$BindeVariablen['var_N0_SVL_KEY']=$_POST['txtSVL_KEY'];
				$rsSVL = $DB->RecordSetOeffnen('SELECT * FROM svcleistungen WHERE SVL_key=:var_N0_SVL_KEY',$BindeVariablen);
				
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVL->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVL->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVL->FeldInfo($FeldName,'TypKZ'),$rsSVL->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVL->FeldInhalt('SVL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE svcleistungen SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', SVL_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SVL_userdat=sysdate';
					$SQL .= ' WHERE SVL_key=0' . $_POST['txtSVL_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
			$AWIS_KEY2=$_POST['txtSVL_KEY'];
		}
	}
	
	
	//*************************************************
	// Serviceartikel
	//*************************************************	
	
	if(isset($_POST['txtSVA_KEY']))
	{
		$AWIS_KEY1=$_POST['txtSVA_KEY'];
	
		$Felder = $Form->NameInArray($_POST, 'txtSVA_',1,1);
	
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('SVA','SVA_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	
			$Fehler = '';
			$Pflichtfelder = array('SVA_VERTRAGSNUMMER','SVA_AST_ATUNR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SVA'][$Pflichtfeld].'<br>';
				}
			}
	
			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
	
			if(floatval($_POST['txtSVA_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO svcartikel';
				$SQL .= '(SVA_BEZEICHNUNG,SVA_AST_ATUNR,SVA_BEMERKUNG,SVA_STATUS,SVA_MANDANT,SVA_SYSTEMKENNUNG,SVA_DATENART';
				$SQL .= ',SVA_USER,SVA_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtSVA_BEZEICHNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVA_AST_ATUNR'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVA_BEMERKUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVA_STATUS'],true);
				$SQL .= ',1';	// Mandant, falls mehr Bereiche so was brauchen....
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVA_SYSTEMKENNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSVA_DATENART'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
	
				$DB->Ausfuehren($SQL);
	
				$SQL = 'SELECT seq_SVA_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';
	
				$rsSVT = $DB->RecordSetOeffnen('SELECT * FROM svcartikel WHERE SVA_Key=' . $_POST['txtSVA_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$rsSVT->FeldInhalt($FeldName),true);
						//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
	
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
	
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVT->FeldInhalt('SVA_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE svcartikel SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', SVA_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SVA_userdat=sysdate';
					$SQL .= ' WHERE SVA_KEY=0' . $_POST['txtSVA_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}

	
	//*************************************************
	// Serviceartikelbenachrichtigungen
	//*************************************************	
	
	if(isset($_POST['txtSAB_KEY']))
	{
		$AWIS_KEY1=$_POST['txtSAB_SVA_KEY'];
	
		$Felder = $Form->NameInArray($_POST, 'txtSAB_',1,1);
	
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('SAB','SAB_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	
			$Fehler = '';
			$Pflichtfelder = array('SAB_VERTRAGSNUMMER','SAB_AST_ATUNR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SAB'][$Pflichtfeld].'<br>';
				}
			}
	
			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

	
			if(floatval($_POST['txtSAB_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO svcartikelbenachrichtigungen';
				$SQL .= '(SAB_SVA_KEY,SAB_SBZ_KEY,SAB_BEZEICHNUNG,SAB_STUFE,SAB_TAG,SAB_MONAT,SAB_WARTEZEIT,SAB_MAILBETREFF';
				$SQL .= ',SAB_MAILTEXT,SAB_MAILTEXT2,SAB_SMSTEXT,SAB_BRIEFTEXT,SAB_EMPFAENGER,SAB_STATUS';
				$SQL .= ',SAB_USER,SAB_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtSAB_SVA_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAB_SBZ_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAB_BEZEICHNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAB_STUFE'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAB_TAG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAB_MONAT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAB_WARTEZEIT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAB_MAILBETREFF'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('TT',$_POST['txtSAB_MAILTEXT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('TT',$_POST['txtSAB_MAILTEXT2'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAB_SMSTEXT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAB_BRIEFTEXT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAB_EMPFAENGER'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAB_STATUS'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
	
				$DB->Ausfuehren($SQL);
	
				$SQL = 'SELECT seq_SAB_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';
					
				$rsSVT = $DB->RecordSetOeffnen('SELECT * FROM svcartikelbenachrichtigungen WHERE SAB_Key=' . $_POST['txtSAB_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$rsSVT->FeldInhalt($FeldName),true);
						//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
	
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
				
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVT->FeldInhalt('SAB_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE svcartikelbenachrichtigungen SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', SAB_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SAB_userdat=sysdate';
					$SQL .= ' WHERE SAB_KEY=0' . $_POST['txtSAB_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}

	
	
	//*************************************************
	// Serviceartikelbenachrichtigungen
	//*************************************************	
	
	if(isset($_POST['txtSTB_KEY']))
	{
		$AWIS_KEY2=$_POST['txtSTB_SVT_KEY'];
	
		$Felder = $Form->NameInArray($_POST, 'txtSTB_',1,1);
	
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('STB','STB_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	
			$Fehler = '';
			$Pflichtfelder = array('STB_VERTRAGSNUMMER','STB_AST_ATUNR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['STB'][$Pflichtfeld].'<br>';
				}
			}
	
			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
	
			if(floatval($_POST['txtSTB_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO SVCTEILNBENACHRICHTUNGEN';
				$SQL .= '(STB_SVT_KEY,STB_DATUM,STB_SAB_KEY,STB_MAILBETREFF';
				$SQL .= ',STB_MAILTEXT,STB_SMSTEXT,STB_BRIEFTEXT,STB_EMPFAENGER';
				$SQL .= ',STB_USER,STB_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtSTB_SVT_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtSTB_DATUM'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSTB_SAB_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSTB_MAILBETREFF'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('TT',$_POST['txtSTB_MAILTEXT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSTB_SMSTEXT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSTB_BRIEFTEXT'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSTB_EMPFAENGER'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
	
				$DB->Ausfuehren($SQL);
	
				$SQL = 'SELECT seq_STB_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';
	
				$rsSVT = $DB->RecordSetOeffnen('SELECT * FROM SVCTEILNBENACHRICHTUNGEN WHERE STB_Key=' . $_POST['txtSTB_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$rsSVT->FeldInhalt($FeldName),true);
						//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
	
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
	
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVT->FeldInhalt('STB_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE SVCTEILNBENACHRICHTUNGEN SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', STB_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', STB_userdat=sysdate';
					$SQL .= ' WHERE STB_KEY=0' . $_POST['txtSTB_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
		$_GET['Unterseite']='Benachrichtigungen';
	}

	
	
	
	
	
	//***********************************************************************************
	//** Infos speichern
	//***********************************************************************************
	
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtSVI_WERT',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			$SQL ='';
			$IDs = explode('_',$Feld);
	
			$rsSVI = $DB->RecordSetOeffnen('SELECT SVI_KEY, ITY_ANZAHL FROM SVCVEREINBARUNGENINFOS INNER JOIN InformationsTypen ON SVI_ITY_KEY = ITY_KEY WHERE SVI_ITY_KEY = 0'.$IDs[2].' AND SVI_SVB_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));
	
			if($IDs[3]=='' AND $_POST[$Feld]!=='')			// Neuer Datensatz
			{
				$SQL = 'SELECT Count(*) FROM SVCVEREINBARUNGENINFOS';
				$SQL .= ' WHERE SVI_SVB_KEY=' . $DB->FeldInhaltFormat('T',$AWIS_KEY1);
				$SQL .= ' AND SVI_ITY_KEY =' . $DB->FeldInhaltFormat('N0',$IDs[2]);
				$rsDaten = $DB->RecordSetOeffnen($SQL);
	
				if(!$rsSVI->EOF() AND $rsSVI->FeldInhalt('ITY_ANZAHL') <= $rsDaten->AnzahlDatensaetze())
				{
					$Form->Fehler_Anzeigen('ZuVieleOptionen','','PRUEFEN',5,'201003261103');
				}
				else
				{
					$SQL = 'INSERT INTO SVCVEREINBARUNGENINFOS';
					$SQL .= '(SVI_WERT, SVI_ITY_KEY, SVI_SVB_KEY, SVI_IMQ_ID, SVI_USER, SVI_USERDAT)';
					$SQL .= 'VALUES(';
					$SQL .= ' '.$DB->FeldInhaltFormat($IDs[4],$_POST[$Feld]);
					$SQL .= ','.$IDs[2];
					$SQL .= ','.$DB->FeldInhaltFormat('T',$AWIS_KEY1);
					$SQL .= ',4';
					$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ',sysdate';
					$SQL .= ')';
				}
			}
			elseif($IDs[3]!='' AND $_POST[$Feld]=='')		// L�schen
			{
				$SQL = 'DELETE from SVCVEREINBARUNGENINFOS';
				$SQL .= ' WHERE SVI_KEY = 0'.$IDs[3].' AND SVI_SVB_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1);
			}
			elseif($_POST[$Feld]!='')						// �ndern
			{
				$WertAlt=$DB->FeldInhaltFormat($IDs[4],$_POST['old'.substr($Feld,3)]);
				$WertNeu=$DB->FeldInhaltFormat($IDs[4],$_POST[$Feld]);
				if($WertAlt!=$WertNeu)
				{
					$SQL = 'UPDATE SVCVEREINBARUNGENINFOS ';
					$SQL .= 'SET SVI_WERT= '.$DB->FeldInhaltFormat($IDs[4],$_POST[$Feld]);
					$SQL .= ',SVI_USER='.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ',SVI_USERDAT=sysdate';
					$SQL .= ' WHERE SVI_KEY = 0'.$IDs[3].' AND SVI_SVB_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1);
				}
			}
			if($SQL!='')
			{
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',1,'MELDEN',5,'201202181713');
				}
			}
		}
		$_GET['Seite']='Optionen';		// Damit er auf der Optionenseite bleibt
	}
	
	
	
	//*************************************************
	// Serviceartikelbenachrichtigungen
	//*************************************************
	
	if(isset($_POST['txtSAA_KEY']))
	{
		$AWIS_KEY2=$_POST['txtSAA_SVA_KEY'];
	
		$Felder = $Form->NameInArray($_POST, 'txtSAA_',1,1);
	
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('SAA','SAA_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	
			$Fehler = '';
			$Pflichtfelder = array('SAA_VERTRAGSNUMMER','SAA_AST_ATUNR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SAA'][$Pflichtfeld].'<br>';
				}
			}
	
			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
	
			if(floatval($_POST['txtSAA_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO SVCARTIKELANGABEN';
				$SQL .= '(SAA_SVA_KEY,SAA_BEZEICHNUNG,SAA_TYP,SAA_KENNUNG,SAA_KENNUNGFELD,SAA_INFOFELD,SAA_POSVON,SAA_LAENGE';
				$SQL .= ',SAA_USER,SAA_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtSAA_SVA_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAA_BEZEICHNUNG'],false);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAA_TYP'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAA_KENNUNG'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAA_KENNUNGFELD'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSAA_INFOFELD'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAA_POSVON'],false);
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSAA_LAENGE'],false);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
	
				$DB->Ausfuehren($SQL);
	
				$SQL = 'SELECT seq_SAA_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';
	
				$rsSVT = $DB->RecordSetOeffnen('SELECT * FROM SVCARTIKELANGABEN WHERE SAA_Key=' . $_POST['txtSAA_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSVT->FeldInfo($FeldName,'TypKZ'),$rsSVT->FeldInhalt($FeldName),true);
						//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
	
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
	
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSVT->FeldInhalt('SAA_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE SVCARTIKELANGABEN SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', SAA_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SAA_userdat=sysdate';
					$SQL .= ' WHERE SAA_KEY=0' . $_POST['txtSAA_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
		$_GET['Seite']='Parameter';
	}
	
	//********************************	
	// Datenimport	
	//********************************	
	if(isset($_FILES['IMPORT']['name']))
	{
		include 'serviceleistungen_importieren.php';
	}
$Form->DebugAusgabe(1,$AWIS_KEY1,$AWIS_KEY2);	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>