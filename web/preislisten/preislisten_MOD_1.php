<?php
/**
 * Pr�fmodul: 	001
 * Zweck:		Dieses Modul pr�ft die Anzahl der Datens�tze
 *
 * Parameter:	SCHWELLE_NEU, Anzahl von neuen Datens�tzen
 * 				SCHWELLE_FEHL, Anzahl fehlender Datens�tzen
 *
 */

global $con;					// Verbindung zum AWIS Server
global $comcon;					// Verbindung zum COM Server
global $PreisListe;				// Preislisten-ID
global $TransaktionsID;			// Transaktions-ID
global $rsPTA;					// Testarten
global $PTAZeile;				// Aktuelle Zeile in den Testarten-RS
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

//*****************************************************************************
// Parameter lesen
//*****************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY AND PLP_KEY = PPP_PLP_KEY';
$SQL .= ' WHERE PLP_ID_FPLB=' . $PreisListe;
$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

$Params=array();

$rsPAR = awisOpenRecordset($comcon,$SQL);
$rsPARZeilen = $awisRSZeilen;
if($rsPARZeilen>0)
{
	for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
	{
		$Params[$rsPAR['PTP_PARAMETER'][$PARZeile]]=($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]);
	}
}

//*****************************
// Historie schreiben
//*****************************
$SQL = ' INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' (PPH_PLP_KEY,PPH_TRANSAKTIONSID,PPH_BEGINN,PPH_USER,PPH_STATUS)';
$SQL .= ' VALUES(' . $rsPTA['PLP_KEY'][$PTAZeile] . '';
$SQL .= ', ' . $TransaktionsID ;
$SQL .= ', SYSDATE';
$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
$SQL .= ', 0';
$SQL .= ')';

if(!awisExecute($comcon,$SQL))
{
	echo '<span class=HinweisTex>Fehler: '. $SQL . '</span>';
}
$rsPPH = awisOpenRecordset($comcon,'SELECT MAX(PPH_Key) AS KEY FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $TransaktionsID . ' AND PPH_User=\'' . $AWISBenutzer->BenutzerName() . '\'');
$PPHKey = $rsPPH['KEY'][0];
unset($rsPPH);

$TestErgebnis = 1;		// Testergebnis (0=Kein Ergebnis, 1=Erfolg, 2=Fehler)

//###############################################################################
//###############################################################################
//########################   B E G I N N   T E S T S ############################
//###############################################################################
//###############################################################################


//*****************************************************************************
// TEST 1
// Pr�fung, welche Datens�tze sind in V=0 enthalten, aber nicht in V=1
//*****************************************************************************

//sleep(2);

$SQL = 'SELECT NEU.PLT_ARTNR, NEU.PLT_TRANSAKTIONSID, NEU.PLT_ID_FPLB';
$SQL .= ' FROM ';
$SQL .= ' (SELECT PLT_ARTNR, PLT_TRANSAKTIONSID, PLT_ID_FPLB ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_ID_FPLB=' . $PreisListe . ') NEU';
$SQL .= ' LEFT OUTER JOIN ';
$SQL .= ' (SELECT PLT_ARTNR, PLT_TRANSAKTIONSID, PLT_ID_FPLB ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=1 AND PLT_ID_FPLB=' . $PreisListe . ') AKT';
$SQL .= ' ON NEU.PLT_ARTNR = AKT.PLT_ARTNR';
$SQL .= ' WHERE AKT.PLT_ARTNR IS NULL';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>: Neue Artikel</b><br>';
/*for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile];

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'Neuer Artikel: ' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\'' ;
	$SQL .= ',1';
	$SQL .= ', \'' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\')' ;
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}

}*/

	$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_PLP_MOD_1_T_1('.$PreisListe.','.$PPHKey.'); END;';

	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}

if($Params['SCHWELLE_NEU'] >= $rsTSTZeilen)
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $Params['SCHWELLE_NEU'] .' erlaubt, ' . $rsTSTZeilen . ' gefunden. Bewertung: <font color=green>I.O.</font></div>';
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $Params['SCHWELLE_NEU'] .' erlaubt, ' . $rsTSTZeilen . ' gefunden. Bewertung: <font color=red>Fehler.</font></div>';
	$TestErgebnis = 2;
}
flush();
unset($rsTST);




// TEST 2
// Pr�fung, welche Datens�tze sind in V=1 enthalten, aber nicht in V=0

//sleep(2);

$SQL = 'SELECT AKT.PLT_ARTNR, NEU.PLT_TRANSAKTIONSID, NEU.PLT_ID_FPLB';
$SQL .= ' FROM ';
$SQL .= ' (SELECT PLT_ARTNR, PLT_TRANSAKTIONSID, PLT_ID_FPLB ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_ID_FPLB=' . $PreisListe . ') NEU';
$SQL .= ' RIGHT OUTER JOIN ';
$SQL .= ' (SELECT PLT_ARTNR, PLT_TRANSAKTIONSID, PLT_ID_FPLB ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=1 AND PLT_ID_FPLB=' . $PreisListe . ') AKT';
$SQL .= ' ON NEU.PLT_ARTNR = AKT.PLT_ARTNR';
$SQL .= ' WHERE NEU.PLT_ARTNR IS NULL';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br>&nbsp;&nbsp;<b>TEST 2</b>: Fehlende Artikel</b><br>';
/*for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['PLT_ARTNR'][$TSTZeile];

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'Fehlender Artikel: ' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\'' ;
	$SQL .= ',2';
	$SQL .= ', \'' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\')' ;
	
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
}*/

	$SQL='BEGIN EXPERIAN_ATU.P_PREISLISTE.PROC_PLP_MOD_1_T_2('.$PreisListe.','.$PPHKey.'); END;';

	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}

if($Params['SCHWELLE_FEHL'] >= $rsTSTZeilen)
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $Params['SCHWELLE_FEHL'] .' erlaubt, ' . $rsTSTZeilen . ' gefunden. Bewertung: <font color=green>I.O.</font></div>';
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $Params['SCHWELLE_FEHL'] .' erlaubt, ' . $rsTSTZeilen . ' gefunden. Bewertung: <font color=red>Fehler.</font></div>';
	$TestErgebnis = 2;
}

flush();




//###############################################################################
//###############################################################################
//##########################   E N D E   T E S T S ##############################
//###############################################################################
//###############################################################################


//*********************************************
// Historie aktualisieren
//*********************************************
$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' SET PPH_ENDE = SYSDATE, PPH_STATUS = ' . $TestErgebnis;
$SQL .= ' WHERE PPH_Key = ' . $PPHKey;
if(!awisExecute($comcon,$SQL))
{
	awisErrorMailLink('PreislistePr�fung',1,$awisDBFehler);
}


unset($rsTST);

echo '<hr>';

?>