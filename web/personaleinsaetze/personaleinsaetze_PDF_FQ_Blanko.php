<?php
require_once ('NeuePersBerichte.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$AWISBenutzer = awisBenutzer::Init();

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Form = new awisFormular();

$TextKonserven[] = array('IBG', '%');
$TextKonserven[] = array('CV', '%');
$TextKonserven[] = array('FQ', '%');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


$SQL = 'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS ';
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= 'WHERE PEZ_PEI_KEY=' . $DB->FeldInhaltFormat('NO', $_GET['PEI_KEY']);

$rsBericht = $DB->RecordSetOeffnen($SQL);


$NeuePersBerichte = new NeuePersBerichte($DB,$AWISBenutzer,$AWISSprachKonserven,$rsBericht);
$NeuePersBerichte->HeaderDaten('Filialbesuch GBL Quartal', 'Filialbericht GBL', '01.02.2019', '1.0',611,612,613);
$NeuePersBerichte->FooterDaten('Filialbesuch GBL Quartal ' . date('m-Y'));
$NeuePersBerichte->NeueSeite();

$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_A'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_A1'],615);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_A2'],616);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_A3'],617);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_A4'],618);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_A5'],619);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B1'],621);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B2'],622);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B3'],623);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B4'],624);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B5'],625);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B6'],626);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B7'],627);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_B8'],628);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_C'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_C1'],630);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_C2'],631);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_C3'],632);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_C4'],633);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_C5'],634);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D1'],636);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D2'],637);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D3'],638);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D4'],639);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D5'],640);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D6'],641);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D7'],642);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D8'],643);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D9'],644);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D10'],645);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D11'],646);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D12'],647);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D13'],648);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D14'],649);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D15'],650);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D16'],651);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_D17'],652);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_E'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_E1'],654);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_E2'],655);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_E3'],656);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F1'],658);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F2'],659);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F3'],660);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F4'],661);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F5'],662);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F6'],663);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F7'],664);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F8'],665);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F9'],666);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F10'],667);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F11'],668);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F12'],669);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F13'],670);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F14'],671);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F15'],672);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F16'],673);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F17'],674);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F18'],675);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F19'],676);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F20'],677);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F21'],678);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_F22'],679);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_G'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_G1'],681);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_G2'],682);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_H'],683);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_H1'],684);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_H2'],685);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_H3'],686);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_H4'],687);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I1'],689);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I2'],690);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I3'],691);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I4'],692);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I5'],693);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I6'],694);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_I7'],695);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_J'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_J1'],697,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_J2'],704,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_J3'],705,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_K'],0);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_K1'],699,false);
$NeuePersBerichte->ErstelleFrage($AWISSprachKonserven['FQ']['FQ_K2'],700,false);


$NeuePersBerichte->Anzeigen();



?>