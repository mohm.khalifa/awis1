<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht40011 = $AWISBenutzer->HatDasRecht(40011);
	$EditRecht = ($Recht40011&2) == 2;
	$HinzuRecht = ($Recht40011&4) == 4;
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SCHAD_SKE'));
	
	$Fehler = 0;
	$Save = 0;
	$FehlerFest = false;
	$NeuAnlage = 0;	
	
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1);
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	

	
    if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
    {
		$Param['ORDER'] = 'KENNUNG';
 	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht40011==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	
	if(isset($_GET['KENN_KEY']))
	{
		$AWIS_KEY1 = $_GET['KENN_KEY'];
	}
	
	
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{	
		$Param['BES_KENNUNG'] = 	$Form->Format('T',$_POST['sucBES_KENNUNG'],true);
		$Param['BES_VERALTET']= $Form->Format('N0',$_POST['sucBES_VERALTET'],true);
		$Param['BES_FILIALEN']= $Form->Format('N0',$_POST['sucBES_FILIALEN'],true);
		$Param['BES_AUSWERTUNG']= $Form->Format('N0',$_POST['sucBES_AUSWERTUNG'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
		
	//Speichernbutton oder Klick aufs F�hnchen
	if(isset($_POST['cmdSpeichern_x']) or isset($_GET['button']))
	{
		include('./kennungen_speichern.php');
	}

	/* Daten beschaffen */
	
	$SQL = ' select KENNUNG, KENNUNG_BEZEICHNUNG, VERALTET, SICHTBAR_FILIALEN, KENN_KEY, KENN_USER, KENN_USERDAT, AKTIV_FUER_AUSWERTUNG, row_number() OVER ( ';
	$SQL .= ' order by ' . $Param['ORDER'];
	$SQL .= ') AS ZeilenNr from( ';
	
	$SQL .= ' select KENNUNG, KENNUNG_BEZEICHNUNG, VERALTET, NVL(SICHTBAR_FILIALEN,\'0\') as SICHTBAR_FILIALEN, KENN_KEY, KENN_USER, KENN_USERDAT, AKTIV_FUER_AUSWERTUNG ';
	$SQL .= ' from( ';
	$SQL .= 'select * ';
	$SQL .=	' from kennungen)) ';

	
	if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '' or $SpeichernOK)
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		//if ($Bedingung != '' and (!isset($_POST['cmdHinzufuegen_x'])) and (!isset($_POST['cmdDSNeu_x'])) and !isset($_GET['HRT_KEY']) and $NeuAnlage != 1 and !isset($_POST['txtHRT_KEY']))
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}

	if($AWIS_KEY1 != '' and !$SpeichernOK)
	{
		if($AWIS_KEY1 <> '')
		{
			$SQL .= ' WHERE KENN_KEY='.$AWIS_KEY1;
		}
		else 
		{
			$SQL .= ' AND KENN_KEY='.$AWIS_KEY1;
		}
		
	}
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	
	if($AWIS_KEY1 == '' or (isset($_POST['cmdSpeichern_x']) and $SpeichernOK))
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsSBE = $DBSCHAD->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['KENNUNG'] = 200;
	$FeldBreiten['BES_KENNUNGSBEZEICHNUNG'] = 700;
	
	$FeldBreiten['BES_VERALTET'] = 20;
	$FeldBreiten['BES_FILIALEN'] = 20;
	$FeldBreiten['BES_AUSWERTUNG'] = 25;

	
	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 3);
	}
	
	$Form->SchreibeHTMLCode('<form name="frmSKEDetails" action="./kennungen_Main.php?cmdAktion=Details' . ($AWIS_KEY1!=''?'&KENN_KEY=' .$AWIS_KEY1:'').' " method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	if (($rsSBE->AnzahlDatensaetze() > 1))
	{
		$Form->ZeileStart();
		// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
	    $Link = './kennungen_Main.php?cmdAktion=Details&Sort=KENNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KENNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');

		
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KENNUNG'], $FeldBreiten['KENNUNG'], '', $Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KENNUNGSBEZEICHNUNG'], $FeldBreiten['BES_KENNUNGSBEZEICHNUNG'], '', $Link);
		
		
		if (($Recht40011&2) == 2)
		{
			$Link = './kennungen_Main.php?cmdAktion=Details&Sort=VERALTET'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VERALTET'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBAKTIV'];
			$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['BES_VERALTET'], '',$Link,$TTT);
		
			$Link = './kennungen_Main.php?cmdAktion=Details&Sort=SICHTBAR_FILIALEN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SICHTBAR_FILIALEN'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBFILIALEN'];
			$Form->Erstelle_Liste_Ueberschrift('S', $FeldBreiten['BES_FILIALEN'], '',$Link,$TTT);

			$Link = './kennungen_Main.php?cmdAktion=Details&Sort=AKTIV_FUER_AUSWERTUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AKTIV_FUER_AUSWERTUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBAUSWERTUNG'];
			$Form->Erstelle_Liste_Ueberschrift('AU', $FeldBreiten['BES_AUSWERTUNG'], '',$Link,$TTT);
		}
		/*
		if(($Recht34000&8)==8)
		{
			// �berschrift der Listenansicht: History
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_HIST'], $FeldBreiten['Hist'], '', '');
		}*/
		$Form->ZeileEnde();
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsSBE->EOF())
		{
			$Form->ZeileStart();
			if ($EditRecht)
			{
			    //Link zum Datensatz
			    $Link = './kennungen_Main.php?cmdAktion=Details&KENN_KEY='.$rsSBE->FeldInhalt('KENN_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			}
			else 
			{
				$Link = '';
			}
			$TTT = $rsSBE->FeldInhalt('KENNUNG');
			
			$Form->Erstelle_ListenFeld('BES_KENNUNG', $rsSBE->FeldInhalt('KENNUNG'), 0, $FeldBreiten['KENNUNG'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
			$Link = '';
			$Form->Erstelle_ListenFeld('BES_KENNUNGSBEZEICHNUNG', $rsSBE->FeldInhalt('KENNUNG_BEZEICHNUNG'), 0, $FeldBreiten['BES_KENNUNGSBEZEICHNUNG'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
				
			
			$LinkAktiv = '';
			if (($Recht40011&2) == 2)
			{
				$LinkAktiv .=  './kennungen_Main.php?cmdAktion=Details&button='.$rsSBE->FeldInhalt('KENN_KEY').'&VERALTET='.$rsSBE->FeldInhalt('VERALTET');
				if ($rsSBE->FeldInhalt('VERALTET') == '1')
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Aktiv']);
				}
				else
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Inaktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['BES_VERALTET'] , ($DS%2));
				$IconsArray = '';
			}
			
			$LinkAktiv = '';
			if (($Recht40011&2) == 2)
			{
				$LinkAktiv .=  './kennungen_Main.php?cmdAktion=Details&button='.$rsSBE->FeldInhalt('KENN_KEY').'&FILIALEN='.$rsSBE->FeldInhalt('SICHTBAR_FILIALEN');
				if ($rsSBE->FeldInhalt('SICHTBAR_FILIALEN') != '1')
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_NSichtbar']);
				}
				else
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Sichtbar']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['BES_FILIALEN'] , ($DS%2));
				$IconsArray = '';
			}

			$LinkAktiv = '';
			if (($Recht40011&2) == 2)
			{
				$LinkAktiv .=  './kennungen_Main.php?cmdAktion=Details&button='.$rsSBE->FeldInhalt('KENN_KEY').'&AUSWERTUNG='.$rsSBE->FeldInhalt('AKTIV_FUER_AUSWERTUNG');
				if ($rsSBE->FeldInhalt('AKTIV_FUER_AUSWERTUNG') != '1')
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_AuswertungRot']);
				}
				else
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_AuswertungGruen']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['BES_AUSWERTUNG'] , ($DS%2));
				$IconsArray = '';
			}

			$Form->ZeileEnde();
			$DS++;
			$rsSBE->DSWeiter();
		}
		
		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsSBE->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './kennungen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
		
		//$Link = './kennungen_Main.php?cmdAktion=Details';
		//$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	elseif(($rsSBE->AnzahlDatensaetze() == 1) or $AWIS_KEY1 == '-1')
	{
			// Infozeile zusammenbauen
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./kennungen_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBE->FeldInhalt('KENN_USER'));
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBE->FeldInhalt('KENN_USERDAT'));
			$Form->InfoZeile($Felder,'');
			
	    if($AWIS_KEY1 == '-1')
	    {
	        $AWISCursorPosition = 'txtBES_KENNUNG';
	    }
	    else 
	    {
	        $AWISCursorPosition = 'txtBES_KENNUNGSBEZEICHNUNG';
	    }
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KENNUNG'] . ':',140);
		$Form->Erstelle_TextFeld('!BES_KENNUNG', isset($_POST['txtBES_KENNUNG'])?$_POST['txtBES_KENNUNG']:$rsSBE->FeldInhalt('KENNUNG'), 90, 90,$EditRecht and $AWIS_KEY1=='-1');
		$Form->ZeileEnde();
			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KENNUNGSBEZEICHNUNG'] . ':',140);
		
		$Form->Erstelle_Textarea('!BES_KENNUNGSBEZEICHNUNG',isset($_POST['txtBES_KENNUNGSBEZEICHNUNG'])?$_POST['txtBES_KENNUNGSBEZEICHNUNG']:$rsSBE->FeldInhalt('KENNUNG_BEZEICHNUNG'),750,90,3,true);
		$Form->ZeileEnde();
	
		if (isset($_POST['txtBES_GRUND']))
		{
			if (isset($_POST['txtCHK_AKTIV']))
			{
				$CHK_AKT = 1;
			}
			else
			{
				$CHK_AKT = 0;
			}
			if (isset($_POST['txtCHK_FILIALEN']))
			{
				$CHK_FIL = 1;
			}
			else
			{
				$CHK_FIL = 0;
			}
			if (isset($_POST['txtCHK_AUSWERTUNG']))
			{
				$CHK_AUS = 1;
			}
			else
			{
				$CHK_AUS = 0;
			}
		
		}
		else if($AWIS_KEY1 == '-1')
		{
			$CHK_AKT = 1;
			$CHK_FIL = 1;
			$CHK_AUS = 1;
		}
		else
		{
			$CHK_AKT =$rsSBE->FeldInhalt('VERALTET')==1?0:1;
			$CHK_FIL =$rsSBE->FeldInhalt('SICHTBAR_FILIALEN')==1?1:0;
			$CHK_AUS =$rsSBE->FeldInhalt('AKTIV_FUER_AUSWERTUNG')==1?1:0;
		}
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVSUCH'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIV',$CHK_AKT,40,true,1);
		$Form->ZeileEnde();	
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SGR_AKTIVFIL'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_FILIALEN',$CHK_FIL,40,true,1);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SGR_AUSWERTUNG'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AUSWERTUNG',$CHK_AUS,40,true,1);
		$Form->ZeileEnde();
		
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
    $Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht40011&4)==4 and ($AWIS_KEY1 != '-1' and $rsSBE->AnzahlDatensaetze() != 1))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht40011&2)==2 and ($AWIS_KEY1 == '-1' or $rsSBE->AnzahlDatensaetze() == 1))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SCHAD_SKE',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	if(isset($Param['BES_KENNUNG']) AND $Param['BES_KENNUNG']!='')
	{
		$Bedingung .= 'AND UPPER(KENNUNG) ' . $DB->LikeOderIst($Param['BES_KENNUNG'], awisDatenbank::AWIS_LIKE_UPPER);
	}
	if(isset($Param['BES_VERALTET']) AND $Param['BES_VERALTET']!='')
	{
		$Bedingung .= 'AND VERALTET ' . $DB->LikeOderIst($Param['BES_VERALTET']) . ' ';
	}
	if(isset($Param['BES_FILIALEN']) AND $Param['BES_FILIALEN']!='')
	{
		if($Param['BES_FILIALEN'] == '0')
		{
			$Bedingung .= 'AND (SICHTBAR_FILIALEN ' . $DB->LikeOderIst($Param['BES_FILIALEN']) . ' or SICHTBAR_FILIALEN is null) ';
		}
		else
		{
			$Bedingung .= 'AND SICHTBAR_FILIALEN ' . $DB->LikeOderIst($Param['BES_FILIALEN']) . ' ';
		}
	}
	if(isset($Param['BES_AUSWERTUNG']) AND $Param['BES_AUSWERTUNG']!='')
	{
		$Bedingung .= 'AND AKTIV_FUER_AUSWERTUNG ' . $DB->LikeOderIst($Param['BES_AUSWERTUNG']) . ' ';
	}
	
	return $Bedingung;
}

?>