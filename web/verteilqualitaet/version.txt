#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Verteilqualitaet
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
1.00.00;Modul eingefuehrt; 01.01.2011;<a href=mailto:Stefan.Oppl@de.atu.eu>Stefan Oppl</a>
1.00.01;Mail auf Ansprechpartner Frau Barbara Gruber ge�ndert; 12.10.2011;<a href=mailto:Henry.Ott@de.atu.eu>Henry Ott</a>

