<?php

global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $rsErgebnis;
global $AWISSprachKonserven;
global $meldung;
global $Fehler;
global $SuchKritOK;
 
try
{
	$SuchKritOK = false;
	$script = "<script type='text/javascript'>
	
	function EinAusklappen(frame,Seite) {
	
	 	if(document.getElementById('FRAME'+frame).style.display == 'block') 
    	{ 
    	     document.getElementById('FRAME'+frame).style.display = 'none'; 
			 if(frame != 5)
			 { 
				document.getElementById('UEB'+frame).style.borderBottom = '';
			 }
			 document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_plus.png';                 
	    }
		else 
		{
			  document.getElementById('FRAME'+frame).style.display = 'block';	
			  if(frame != 5)
			  { 
			  	document.getElementById('UEB'+frame).style.borderBottom = '1px solid black';
			  }
			  document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_minus.png'; 
		}		
	}	
			
	function ladeSelectTitles(pName, pForm) {
		var sel = pForm.elements[pName];
		sel.title = sel.options[sel.selectedIndex].title;
	}
	</script>";
	
	echo $script;
	
	
	$script = "<script type='text/javascript'>
	
	function textareaAuf(obj) {
			 
    	     obj.rows ='10';
	}
	
	function textareaZu(obj) {
	
    	     obj.rows ='1';
	}
		
	function aktuellesDatum(obj) {
			 var heute = new Date();
			
    	     obj.value=heute.getDate()+'.'+('0' + (heute.getMonth()+1)).slice(-2)+'.' + heute.getFullYear();
	}
	
	function VorgangUebernehmen()
    {
        if (document.getElementsByName('txtBES_BEARBEITER')[0].value != '::bitte w�hlen::')
        {
           document.getElementsByName('sucBES_BEARBEITER_AB')[0].value = document.getElementsByName('txtBES_BEARBEITER')[0].value;    
        }
        else
        {
             document.getElementsByName('sucBES_BEARBEITER_AB')[0].value = '';
        }
    }
	
	</script>";
	
	echo $script;
	
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','lbl_suche');

	
	$AWISBenutzer = awisBenutzer::Init();
		
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Seite = 1;
	$Fehler = 0;
	$Neuanlage = 0;
	$Edit = 0;
	
	$Frames = unserialize($AWISBenutzer->ParameterLesen('Schad_Frame'));
	
	$Frame1 = $Frames[0] == 1? true:false;
	$Frame2 = $Frames[1] == 1? true:false;
	$Frame3 = $Frames[2] == 1? true:false;
	$Frame4 = $Frames[3] == 1? true:false;
	$Frame5 = $Frames[4] == 1? true:false;
	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht40000 = $AWISBenutzer->HatDasRecht(40000);
	
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BES'));
	$SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Schad_Schnellsuche');
	

	if(isset($_POST['cmdSuche_x']))
	{
		$Param['ORDER'] = '';
	}
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'EINGABEAM DESC';
	}
	if((isset($_POST['sucBES_SUCHE']) and $_POST['sucBES_SUCHE'] != 2))
	{
		if(strpos($Param['ORDER'],'WV_GRUND') !== false)
		{
			$Param['ORDER'] = 'EINGABEAM DESC';
		}
	}
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if($Recht40000==0)
	{
		$Form->Fehler_KeineRechte();
		die;
	}
	
	if(isset($_POST['txtBEARBEITUNGSNR']))
	{
		$AWIS_KEY1 = $_POST['txtBEARBEITUNGSNR'];
	}
	
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('schaden_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		if(isset($_GET['Seite']))
		{
			if($_GET['Seite'] == 'Konto')
			{
				if(!is_numeric(str_replace(',','.',$_POST['txtBES_BETRAG'])))
				{
					$Fehler += 1;
				}
				if(strtotime($_POST['txtBES_DATUM'])==False)
				{
					$Fehler += 2;	
				}
				
				
				if($Fehler == 0)
				{
					include('schaden_speichern.php');
				}
				else 
				{
					$AWIS_KEY2 = $_POST['txtZKEY'];
				}
			}
			else if($_GET['Seite'] == 'Info')
			{
			    if($_POST['txtBES_BETRAG'] != '' and !is_numeric(str_replace(',','.',$_POST['txtBES_BETRAG'])))
				{
					$Fehler += 1;
				}
				if(strtotime($_POST['txtBES_DATUM'])==False and $_POST['txtBES_DATUM'] != '')
				{
					$Fehler += 2;
					$_POST['txtBES_DATUM'] = date('d.m.Y',time());
				}
				
				if($Fehler == 0)
				{
				    include('schaden_speichern.php');
				}
				else 
				{
					$AWIS_KEY2 = $_POST['txtID'];
				}
			}
			else if($_GET['Seite'] == 'WV')
			{
				if(strtotime($_POST['txtBES_DATUM'])==False and $_POST['txtBES_DATUM'] != '')
				{
					$Fehler += 1;
					$_POST['txtBES_DATUM'] = date('d.m.Y',time());
				}
					
				if($Fehler == 0)
				{
					include('schaden_speichern.php');
				}
				else
				{
					$AWIS_KEY2 = $_POST['txtID'];
				}
			}
			else if($_GET['Seite'] == 'Haupt')
			{
				if($Fehler == 0)
				{
					$Fehler = 0;
					if($_POST['txtBES_GESCHLAM'] != '' and ($_POST['txtBES_KENNUNG'] == '' or $_POST['txtBES_KENNUNG'] == 'FEHLT'))
					{
						$Form->ZeileStart();
						$Form->Hinweistext('Kennung darf nicht \'bitte w�hlen\' oder \'FEHLT\' sein wenn Akte geschlossen wird.');
						$Form->ZeileEnde();
						$Fehler++;
					}
					if($_POST['txtBES_GESCHLAM'] != '' and ($_POST['txtBES_STAND'] == 0 or $_POST['txtBES_STAND'] == 22))
					{
						$Form->ZeileStart();
						$Form->Hinweistext('B-Stand darf nicht \'bitte w�hlen\' oder \'OFFEN\' sein wenn Akte geschlossen wird.');
						$Form->ZeileEnde();
						$Fehler++;
					}
					if(isset($_POST['txtBES_AART2']) and $_POST['txtBES_AART2'] == '')
					{
						$Form->ZeileStart();
						$Form->Hinweistext('Nur g�ltige Gro�-Kd-Nr m�glich!');
						$Form->ZeileEnde();
						$Fehler++;
					}
					if($Fehler == 0)
					{
						include('schaden_speichern.php');
					    $Form->ZeileStart();
					    $Form->Hinweistext($meldung);
					    $Form->ZeileEnde();
					    $Edit = 1;
					}
				}

			}
		}
		else if($AWIS_KEY1 == '-1')
		{
			if(isset($_POST['txtBES_AART2']) and $_POST['txtBES_AART2'] == '')
			{
				$Form->ZeileStart();
				$Form->Hinweistext('Nur g�ltige Gro�-Kd-Nr m�glich!');
				$Form->ZeileEnde();
				$Fehler++;
				$AWIS_KEY1 = '-1';
			}
			if($Fehler == 0)
			{
				include('schaden_speichern.php');
				$Form->ZeileStart();
				$Form->Hinweistext($meldung);
				$Form->ZeileEnde();
				$Neuanlage = 1;
			}
		}
		
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
	    $AWIS_KEY1 = '';
	    if(isset($_POST['sucBES_SUCHE']))
		{
		    if($_POST['sucBES_SUCHE'] == 1)
			{
				if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
				{
					$Param['BES_FIL_ID']=$Form->Format('N0',$_POST['txtBES_FIL_ID'],true);
				}
				else
				{
					$Param['BES_DATUMVON']=       isset($_POST['sucBES_DATUMVON'])?$Form->Format('T',$_POST['sucBES_DATUMVON'],true):'';
					$Param['BES_DATUMBIS']=       isset($_POST['sucBES_DATUMBIS'])?$Form->Format('T',$_POST['sucBES_DATUMBIS'],true):'';
					$Param['BES_KUNDE']=          isset($_POST['sucBES_KUNDE'])?$Form->Format('T',$_POST['sucBES_KUNDE'],true):'';
					$Param['BES_STRASSE']=        isset($_POST['sucBES_STRASSE'])?$Form->Format('T',$_POST['sucBES_STRASSE'],true):'';
					$Param['BES_PLZ']=            isset($_POST['sucBES_PLZ'])?$Form->Format('T',$_POST['sucBES_PLZ'],true):'';
					$Param['BES_ORT']=            isset($_POST['sucBES_ORT'])?$Form->Format('T',$_POST['sucBES_ORT'],true):'';
					$Param['BES_TELEFON']=        isset($_POST['sucBES_TELEFON'])?$Form->Format('T',$_POST['sucBES_TELEFON'],true):'';
					$Param['BES_EMAIL']=          isset($_POST['sucBES_EMAIL'])?$Form->Format('T',$_POST['sucBES_EMAIL'],true):'';
					$Param['BES_ATUCARD']=        isset($_POST['sucBES_ATUCARD'])?$Form->Format('T',$_POST['sucBES_ATUCARD'],true):'';
					$Param['BES_SACHBEARBEITER']= isset($_POST['sucBES_SACHBEARBEITER'])?$Form->Format('T',$_POST['sucBES_SACHBEARBEITER'],true):'';
					$Param['BES_SCHADENJAHR']= isset($_POST['sucBES_SCHADENJAHR'])?$Form->Format('T',$_POST['sucBES_SCHADENJAHR'],true):'';
					$Param['BES_VERSNR']= isset($_POST['sucBES_VERSNR'])?$Form->Format('T',$_POST['sucBES_VERSNR'],true):'';
					$Param['BES_FIL_ID']=$Form->Format('N0',$_POST['sucBES_FIL_ID'],true);

				}
				$Param['BES_BEARBEITUNG'] = 	 isset($_POST['sucBES_BEARBEITUNG'])?$Form->Format('T',$_POST['sucBES_BEARBEITUNG'],true):'';
				$Param['BES_KFZ'] = 		     isset($_POST['sucBES_KFZ'])?$Form->Format('T',$_POST['sucBES_KFZ'],true):'';
				$Param['BES_VORGANG'] = 	     isset($_POST['sucBES_VORGANG'])?$Form->Format('T',$_POST['sucBES_VORGANG'],true):'';
				$Param['BES_AART']=			     isset($_POST['sucBES_AART'])?$Form->Format('N0',$_POST['sucBES_AART'],true):'';
				$Param['BES_AZ']=			     isset($_POST['sucBES_AZ'])?$Form->Format('T',$_POST['sucBES_AZ'],true):'';
				$Param['BES_REKLANR']=			 isset($_POST['sucBES_REKLANR'])?$Form->Format('T',$_POST['sucBES_REKLANR'],true):'';
				$Param['BES_GROSSKDNR']=			 isset($_POST['sucBES_GROSSKDNR'])?$Form->Format('T',$_POST['sucBES_GROSSKDNR'],true):'';
				$Param['SPEICHERN']=		     isset($_POST['sucAuswahlSpeichern'])?'on':'';
				$Param['BES_SUCHE']=isset($_POST['sucBES_SUCHE'])?$Form->Format('T',$_POST['sucBES_SUCHE'],true):'';
				$Param['BES_AUFGABE']=0;
				$Param['KEY']='';
				$Seite = 1;
				$Param['BLOCK'] = 1;
			}
			else 
			{
				$Param['BES_DATUMVON']=       '';
				$Param['BES_DATUMBIS']=       '';
				$Param['BES_KUNDE']=          '';
				$Param['BES_STRASSE']=        '';
				$Param['BES_PLZ']=            '';
				$Param['BES_ORT']=            '';
				$Param['BES_TELEFON']=        '';
				$Param['BES_EMAIL']=          '';
				$Param['BES_ATUCARD']=        '';
				$Param['BES_SACHBEARBEITER']= '';
				$Param['BES_VERSNR']= '';
				$Param['BES_REKLANR']= '';
				$Param['BES_AZ']= '';
				$Param['BES_FIL_ID']=         '';
				$Param['BES_BEARBEITUNG'] = 	 '';
				$Param['BES_KFZ'] = 		     '';
				$Param['BES_VORGANG'] = 	     '';
				$Param['BES_AART']=			     '';
				$Param['BES_GROSSKDNR']=	'';
				$Param['SPEICHERN']=		     isset($_POST['sucAuswahlSpeichern'])?'on':'';
				$Param['KEY']='';
				$Param['BES_SCHADENJAHR']= '';
					
				$Param['BES_SUCHE']=isset($_POST['sucBES_SUCHE'])?$Form->Format('T',$_POST['sucBES_SUCHE'],true):'';
				$Param['BES_AUFGABE']=isset($_POST['sucBES_AUFGABE'])?$Form->Format('T',$_POST['sucBES_AUFGABE'],true):'';
				$Param['BLOCK'] = 1;
			}
		}
		else 
		{
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
			{
				$Param['BES_FIL_ID']=$Form->Format('N0',$_POST['txtBES_FIL_ID'],true);
			}
			else
			{
				$Param['BES_DATUMVON']=       isset($_POST['sucBES_DATUMVON'])?$Form->Format('T',$_POST['sucBES_DATUMVON'],true):'';
				$Param['BES_DATUMBIS']=       isset($_POST['sucBES_DATUMBIS'])?$Form->Format('T',$_POST['sucBES_DATUMBIS'],true):'';
				$Param['BES_KUNDE']=          isset($_POST['sucBES_KUNDE'])?$Form->Format('T',$_POST['sucBES_KUNDE'],true):'';
				$Param['BES_STRASSE']=        isset($_POST['sucBES_STRASSE'])?$Form->Format('T',$_POST['sucBES_STRASSE'],true):'';
				$Param['BES_PLZ']=            isset($_POST['sucBES_PLZ'])?$Form->Format('T',$_POST['sucBES_PLZ'],true):'';
				$Param['BES_ORT']=            isset($_POST['sucBES_ORT'])?$Form->Format('T',$_POST['sucBES_ORT'],true):'';
				$Param['BES_TELEFON']=        isset($_POST['sucBES_TELEFON'])?$Form->Format('T',$_POST['sucBES_TELEFON'],true):'';
				$Param['BES_EMAIL']=          isset($_POST['sucBES_EMAIL'])?$Form->Format('T',$_POST['sucBES_EMAIL'],true):'';
				$Param['BES_ATUCARD']=        isset($_POST['sucBES_ATUCARD'])?$Form->Format('T',$_POST['sucBES_ATUCARD'],true):'';
				$Param['BES_SACHBEARBEITER']= isset($_POST['sucBES_SACHBEARBEITER'])?$Form->Format('T',$_POST['sucBES_SACHBEARBEITER'],true):'';
				$Param['BES_VERSNR']= isset($_POST['sucBES_VERSNR'])?$Form->Format('T',$_POST['sucBES_VERSNR'],true):'';
				if(isset($_POST['sucBES_FIL_ID']))
				{
					$Param['BES_FIL_ID']=$Form->Format('N0',$_POST['sucBES_FIL_ID'],true);
					if($Param['BES_FIL_ID'] == 0)
					{
						$Param['BES_FIL_ID'] = '';
					}
				}
				else
				{
					$Param['BES_FIL_ID']=0;
				}
			}
			$Param['BES_BEARBEITUNG'] = 	 isset($_POST['sucBES_BEARBEITUNG'])?$Form->Format('T',$_POST['sucBES_BEARBEITUNG'],true):'';
			$Param['BES_KFZ'] = 		     isset($_POST['sucBES_KFZ'])?$Form->Format('T',$_POST['sucBES_KFZ'],true):'';
			$Param['BES_VORGANG'] = 	     isset($_POST['sucBES_VORGANG'])?$Form->Format('T',$_POST['sucBES_VORGANG'],true):'';
			$Param['BES_AART']=			     isset($_POST['sucBES_AART'])?$Form->Format('N0',$_POST['sucBES_AART'],true):'';
			$Param['BES_AZ']=			     isset($_POST['sucBES_AZ'])?$Form->Format('T',$_POST['sucBES_AZ'],true):'';
			$Param['BES_REKLANR']=			     isset($_POST['sucBES_REKLANR'])?$Form->Format('T',$_POST['sucBES_REKLANR'],true):'';
			$Param['BES_GROSSKDNR']=			 isset($_POST['sucBES_GROSSKDNR'])?$Form->Format('T',$_POST['sucBES_GROSSKDNR'],true):'';
			$Param['SPEICHERN']=		     isset($_POST['sucAuswahlSpeichern'])?'on':'';
			$Param['BES_SCHADENJAHR'] = 	     isset($_POST['sucBES_SCHADENJAHR'])?$Form->Format('T',$_POST['sucBES_SCHADENJAHR'],true):'';
				
			$Param['KEY']='';
			$Seite = 1;
			$Param['BLOCK'] = 1;
			$Param['BES_SUCHE'] = '';
		}
	}
	
	
	if(isset($_GET['BEARBEITUNGSNR']) and $AWIS_KEY1 == '')
	{
		$AWIS_KEY1 = $_GET['BEARBEITUNGSNR'];
	}

	$Bedingung = '';
	$SQL = 'select ANZEIGEFIL,BEARBNRNEU, ART, to_clob(FEHLERBESCHREIBUNG) AS REKL_TEXT, FILNR, VERURS_FILIALE, BEARBEITUNGSNR, BEARBEITER AS SACHBEARBEITER, GESCHLECHT, KUNDENNAME, VORNAME, ';
	$SQL .= "DIAGNOSE_WERKSTATTLEITER, DIAGNOSE_TKDL,REKLASCHEINNR,REKLASCHEINVAX,EINGABEAM,DATUMEREIGNIS,BEARBEITUNGSNRFIL,GEWICHTUNG,FABRIKAT,TYP,KFZTYP,ERSTZULASSUNG,KM,BEZEICHNUNG,MECHANIKER,AUFTRAGSDATUM,DATUMREGULIERUNG,BONNRREGULIERUNG,VERK�UFER,FORDERUNG,WEITERGELEITET_HERSTELLER, ";
	$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, GROSSKDNR, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, AKTEGESCHLOSSENAM,BESICHTIGT,KBANR,KAUFDATUM,BONNR,LIEFERANT,LIEFARTNR,VORGANGSNR,FGLIEFERANT,FGMARKE,ZUSATZINFO,FEHLERBESCHREIBUNG,BEMINTERN,ANWALTSSACHE,WEITERGELEITETVERSICHERUNG, ";
	$SQL .= "AUFTRAGSART_ATU_NEU, BID, SCHADENSGRUND, TERMIN_KUNDE_1, TERMIN_KUNDE_2, TERMIN_KUNDE_WER_1, TERMIN_KUNDE_WER_2, KONTAKT_KUNDE, INFO_AN_GBL, AUSFALLURSACHE, EINGABEDURCH_FIL, EINGABEDURCH_FILPERSNR, ATUNR, WERT,SBNAME,ORTEREIGNIS,ANTRAGART,AKTENZEICHENRA,RAFORDERUNG,STANDBEIGERICHT,AKTENZEITENGERICHT,VERSNR, ";
	if(isset($Param['BES_SUCHE']) and $Param['BES_SUCHE'] == 2 and $AWIS_KEY1 == '')
	{
	    $SQL .= 'WV_GRUND, ';
	    $SQL .='row_number() OVER (';
	    $SQL .= ' order by ' . $Param['ORDER'];
	}
	else if(isset($Param['BES_SUCHE']) and $Param['BES_SUCHE'] == 2 and $AWIS_KEY1 != '')
	{
	    $SQL .='row_number() OVER (';
	    $SQL .= ' order by bearbeitungsnr';
	}
	else
	{
	    $SQL .='row_number() OVER (';
	    $SQL .= ' order by ' . $Param['ORDER'];
	}
	
	$SQL .= ') AS ZeilenNr from( ';
	$SQL .= "SELECT distinct S.*,AA.*,SB.*,(select listagg(v.schadensnr,',') within group(order by 1) from zahlungen z left join vers_new v on z.zuvsid = v.VS_ID where z.bearbeitungsnr = s.bearbeitungsnr and S.ART = 'S') as VERSNR ";
	if(isset($Param['BES_SUCHE']) and $Param['BES_SUCHE'] == 2 and $AWIS_KEY1 == '')
	{
	    $SQL .= ',W.WV_GRUND ';
	}
	$SQL .= "FROM SCHAEDEN_NEW S ";
	$SQL .= "LEFT JOIN ANTRAGART AA ON ANTRAGART = AA.ID ";
	$SQL .= "LEFT JOIN SACHBEARBEITER SB ON BEARBEITER = SB.ID ";
	if(isset($Param['BES_SUCHE']) and $Param['BES_SUCHE'] == 2 and $AWIS_KEY1 == '')
	{
		$SuchKritOK =true;
		if($Param['BES_AUFGABE'] == 0)
		{
			$SQL2 = ' Select * from Sachbearbeiter where AWIS_USER='. $DB->WertSetzen('SAC','N0',$AWISBenutzer ->BenutzerID());
			$rsSB = $DBSCHAD->RecordSetOeffnen($SQL2,$DB->Bindevariablen('SAC'));
			$User = $rsSB->FeldInhalt('SBNAME'); 
		}
		else 
		{
			$SQL2 = ' Select * from Sachbearbeiter where ID='. $DB->WertSetzen('SAC','N0',$Param['BES_AUFGABE']);
			$rsSB = $DBSCHAD->RecordSetOeffnen($SQL2,$DB->Bindevariablen('SAC'));
			$User = $rsSB->FeldInhalt('SBNAME');
		}
		
		$SQL .=	"LEFT JOIN WIEDERVORLAGEN_NEW W on W.BEARBEITUNGSNR = S.BEARBEITUNGSNR and S.ART = 'S' ";
		$SQL .=	"where WV_AN ='".$User."'";
		$SQL .=	" and (trunc(WV_DATUM) = trunc(sysdate) or trunc(WV_DATUM) < trunc(sysdate))";
	}
	$SQL .= ") ";
	if($AWIS_KEY1 != '' and !isset($_POST['cmdSuche_x']))
	{
		$Bedingung .= ' AND ART = \'S\' AND bearbeitungsnr= '.$DB->WertSetzen('BES','T',$AWIS_KEY1);
	}
	
	if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
	{
		$Bedingung.= " AND BEARBEITER in (SELECT ID FROM SACHBEARBEITER WHERE UPPER(SBNAME) LIKE 'FIL/%' OR UPPER(SBNAME) = 'FILIALEN')";				
	    $Bedingung.= " AND ANTRAGART in (10,3,18,17)";		
	}
	
	if(($Neuanlage != 1 and $AWIS_KEY1 == '') or isset($_POST['cmdSuche_x']))
	{
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
	}
	if ($Bedingung != '')
	{
		$SuchKritOK = true;
		$SQL .= ' WHERE ' . substr($Bedingung, 4);
	}
	
	$MaxDS = 1;
	$ZeilenProSeite=1;
	$Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
			$Param['BLOCK'] = $Form->Format('N0', $_REQUEST['Block'], false); 

	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;


	if ($SuchKritOK===true)
	{
		$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('BES',false));
		if($AWIS_KEY1 == '')
		{
			$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
		}
		$rsErgebnis = $DBSCHAD->RecordSetOeffnen($SQL,$DB->Bindevariablen('BES'));
		$Form->DebugAusgabe(1,$DBSCHAD->LetzterSQL());
		
	}
	else
	{
		$Form->Hinweistext('Es muss mindestens ein Suchkriterium eingegeben werden!',$Form::HINWEISTEXT_HINWEIS);
		$rsErgebnis = $DBSCHAD->RecordSetOeffnen('Select * from dual where rownum =0');
	}
	if(isset($_GET['Seite']))
	{
        if(isset($_POST['cmdSuche_x'])) {
            $AWIS_KEY1 = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
        }
	    $Seite= '&Seite='.$_GET['Seite'].'&BEARBEITUNGSNR='.$AWIS_KEY1;
	}
	else 
	{
	    if(isset($_POST['cmdSpeichern_x']) or $rsErgebnis->AnzahlDatensaetze() == 1)
		{
			//Wenn aus der Neuanlage in Hauptreiter gewechselt wird.
			$Seite = '&Seite=Haupt&BEARBEITUNGSNR='.$AWIS_KEY1;
		}
		else
		{
			$Seite= '';
		}
	}
	
	$Form->SchreibeHTMLCode('<form name="frmBESDetails" action="./schaden_Main.php?cmdAktion=Details'.$Seite.'" method="POST"  enctype="multipart/form-data">');
	$Form->Formular_Start();
	
	
	if($AWIS_KEY1 == '')
	{
		$AWIS_KEY1 = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
	}
	
	
	if (($rsErgebnis->AnzahlDatensaetze() > 1))
	{
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		$AWIS_KEY1 = '';	
		$Gesamtbreite = '';
			// Spaltenbreiten f�r Listenansicht
			$FeldBreiten = array();
			$FeldBreiten['BEARBEITUNG'] = 130;
			$FeldBreiten['EINGABEDAT'] = 110;
			$FeldBreiten['AART'] = 230;
			$FeldBreiten['KUNDE'] = 250;
			
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
			{
				$FeldBreiten['FILNR'] = 80;
				if(isset($Param['BES_SUCHE']) and $Param['BES_SUCHE'] == 2)
				{
				    $FeldBreiten['WVGRUND'] = 345;
				}
				$FeldBreiten['KFZ'] = 100;
			}
			
			$count = 0;
			foreach ($FeldBreiten as $value)
			{
				$Gesamtbreite += $value;
				$count++;
			}
			if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
			{
				$Gesamtbreite += ($count * 3.4);
			}
			
			$Form->ZeileStart();
			// �berschrift der Listenansicht mit Sortierungslink: Filial;
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=BEARBEITUNGSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='BEARBEITUNGSNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_BEARB'], $FeldBreiten['BEARBEITUNG'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=EINGABEAM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='EINGABEAM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_EINGABEAM'], $FeldBreiten['EINGABEDAT'], '', $Link);
			
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=WERT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WERT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_AART'], $FeldBreiten['AART'], '', $Link);
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))=='')
			{
				// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
				$Link = './schaden_Main.php?cmdAktion=Details&Sort=FILNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FILNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_FILNR'], $FeldBreiten['FILNR'], '', $Link);
			}
			// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
			$Link = './schaden_Main.php?cmdAktion=Details&Sort=KUNDENNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KUNDENNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KUNDE'], $FeldBreiten['KUNDE'], '', $Link);
			if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
			{
				// �berschrift der Listenansicht mit Sortierungslink: Nachname
				$Link = './schaden_Main.php?cmdAktion=Details&Sort=KFZ_KENNZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KFZ_KENNZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_KFZKZ'], $FeldBreiten['KFZ'], '', $Link);
				if(isset($Param['BES_SUCHE']) and $Param['BES_SUCHE'] == 2)
				{
				    $Link = './schaden_Main.php?cmdAktion=Details&Sort=WV_GRUND'.((isset($_GET['Sort']) AND ($_GET['Sort']=='WV_GRUND'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
				    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_WVGRUND'], $FeldBreiten['WVGRUND'], '', $Link);
				}
			}
			$Form->ZeileEnde();
			
			while(! $rsErgebnis->EOF())
			{
				$Form->ZeileStart();
				
				
				

				if($rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM') <> '')
				{
					$Geschl = 'style="color:red"';
				}
				else
				{
					$Geschl = '';
				}
			
				
				$Hintergrund = ($DS%2);
				$Link = '"./schaden_Main.php?cmdAktion=Details&BEARBEITUNGSNR='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:''.'&Seite=Haupt').'"'.$Geschl;
				$TTT = $rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
				$Form->Erstelle_ListenFeld('BES_BEARBEITUNG', $rsErgebnis->FeldInhalt('BEARBEITUNGSNR'), 0, $FeldBreiten['BEARBEITUNG'], false, $Hintergrund,'color:red;',$Link, 'T', 'L', $TTT);
				$TTT =  $Form->Format('D',$rsErgebnis->FeldInhalt('EINGABEAM'));
				$Form->Erstelle_ListenFeld('BES_EINGABEAM',$Form->Format('D',$rsErgebnis->FeldInhalt('EINGABEAM')), 0, $FeldBreiten['EINGABEDAT'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				$TTT =  $rsErgebnis->FeldInhalt('WERT');
				$Form->Erstelle_ListenFeld('BES_ART',$rsErgebnis->FeldInhalt('WERT'), 0, $FeldBreiten['AART'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
				{
					$TTT =  $rsErgebnis->FeldInhalt('FILNR');
					$Form->Erstelle_ListenFeld('BES_FILNR',$rsErgebnis->FeldInhalt('FILNR'), 0, $FeldBreiten['FILNR'], false, $Hintergrund, '','', 'T', 'L', $TTT);
				}
				$TTT = $rsErgebnis->FeldInhalt('KUNDENNAME').' '.$rsErgebnis->FeldInhalt('VORNAME');
				$Form->Erstelle_ListenFeld('BES_KUNDE',$rsErgebnis->FeldInhalt('KUNDENNAME').' '.$rsErgebnis->FeldInhalt('VORNAME'), 0, $FeldBreiten['KUNDE'], false, $Hintergrund,'','', 'T', 'L', $TTT);
				if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))	=='')
				{
					$TTT =  $rsErgebnis->FeldInhalt('KFZ_KENNZ');
					$Form->Erstelle_ListenFeld('BES_KFZKZ',$rsErgebnis->FeldInhalt('KFZ_KENNZ'), 0, $FeldBreiten['KFZ'], false, $Hintergrund, '','', 'T', 'L', $TTT);
					if(isset($Param['BES_SUCHE']) and $Param['BES_SUCHE'] == 2)
					{
					   $TTT =  $rsErgebnis->FeldInhalt('EMAIL');
					   $Form->Erstelle_ListenFeld('BES_WVGRUND',$rsErgebnis->FeldInhalt('WV_GRUND'), 0, $FeldBreiten['WVGRUND'], false, $Hintergrund, '','', 'T', 'L', $TTT);
					}
				}
				
				$Form->ZeileEnde();
				$DS++;
				$rsErgebnis->DSWeiter();
			}
			
			$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
			$Form->ZeileStart();
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsErgebnis->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
			$Link = './schaden_Main.php?cmdAktion=Details';
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
			$Form->ZeileEnde();			
	}
	else if (($rsErgebnis->AnzahlDatensaetze() == 1))
	{
		$SQL2 = 'select * from(';
		$SQL2 .=  'select historie_kennung,historie_datum ';
		$SQL2 .= 'from historie';
		$SQL2 .= ' where historie_bearbnr ='.$AWIS_KEY1;
		$SQL2 .= ' order by historie_datum desc)';
		$SQL2 .= ' where rownum = 1';
		$rsHist = $DBSCHAD->RecordSetOeffnen($SQL2);
		
		
		if(!isset($_GET['Seite']))
		{
			$Recht40004 = $AWISBenutzer->HatDasRecht(40004);
			$Recht40005 = $AWISBenutzer->HatDasRecht(40005);
			$Recht40006 = $AWISBenutzer->HatDasRecht(40006);
			$Recht40007 = $AWISBenutzer->HatDasRecht(40007);
			if($Recht40004!=0)
			{
				$Seite = 'Haupt';
			}
			elseif($Recht40005!=0)
			{
				$Seite = 'Info';
			}
			elseif($Recht40006!=0)
			{
				$Seite = 'WV';
			}
			elseif($Recht40007!=0)
			{
				$Seite = 'Konto';
			}
			else 
			{
				$Form->Fehler_KeineRechte();
				die;
			}
		}
		else
		{
			$Seite = $_GET['Seite'];
			switch ($Seite)
			{
				case 'Haupt':
					$Recht = 40004;
					break;
				case 'Info':
					$Recht = 40005;
					break;
				case 'WV':
					$Recht = 40006;
					break;
				case 'Konto':
					$Recht = 40007;
					break;
			}
			
			
			$RechtTest = $AWISBenutzer->HatDasRecht($Recht);
			
			
			if($RechtTest==0)
			{
				$Form->Fehler_KeineRechte();
				die;
			}
		}
		
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./schaden_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHist->FeldInhalt('HISTORIE_KENNUNG'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsHist->FeldInhalt('HISTORIE_DATUM'));
		$Form->ZeileStart();
		$Form->InfoZeile($Felder,'');
		$Form->ZeileEnde();
		
		
		
		$RegDet = new awisRegister(40002,'&BEARBEITUNGSNR='.$AWIS_KEY1);
		
		
		if($Seite != 'Haupt')
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARB'] . ':',130);
			$Form->Erstelle_TextFeld('BES_BEARBNR',$rsErgebnis->FeldInhalt('BEARBEITUNGSNR'), 10, 110,false, '','', '','', 'L');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FIL'] . ':',30);
			$Form->Erstelle_TextFeld('BES_FILILAE',$rsErgebnis->FeldInhalt('FILNR'), 40,100, false, '', '', '', 'T', 'L','','',50);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_NAME'] . ':',90);
			$Form->Erstelle_TextFeld('BES_NAME',$rsErgebnis->FeldInhalt('KUNDENNAME'), 25, 200,false, '','', '','', 'L');
			$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORNAME'] . ':',73);
			$Form->Erstelle_TextFeld('BES_VORNAME',$rsErgebnis->FeldInhalt('VORNAME'), 25, 380,false, '','', '','', 'L');
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
		}
		
		$RegDet->ZeichneRegister($Seite);
		$Param['KEY']=$AWIS_KEY1;
			
	}
	else if($AWIS_KEY1 == '-1')
	{
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SCHADENJAHR'] . ':',130);
		$Form->Erstelle_TextFeld('!BES_SCHADENSJAHR',date('Y',time()),1,100, true, '', '', '', 'T', 'L','','',4);
		$Form->Trennzeile('O');
	    $AWISCursorPosition = 'txtBES_SCHADENSJAHR';
	
		
		include('schaden_Haupt.php');
	}
	else 
	{
		if($SuchKritOK===true)
		{
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
			$Form->ZeileEnde();
		}
		
	}
	
	$Form->Formular_Ende();
	
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	//nur f�r Hauptreiter und wenn letzter Datensatz
	if($SchnellSucheFeld and $rsErgebnis->AnzahlDatensaetze() == 1 and ((isset($_GET['Seite']) and ($_GET['Seite'] == 'Haupt') or !isset($_GET['Seite']))))
	{
	    $Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['txt_SchnellsucheBearb'].':',150,'text-align:right','',$AWISSprachKonserven['BES']['ttt_SchnellsucheBearb']);
		$Form->Erstelle_TextFeld('*BES_BEARBEITUNG','',10,0,true,'','','','T','L','','',10,'','f');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['txt_SchnellsucheKFZ'].':',45,'text-align:right','',$AWISSprachKonserven['BES']['ttt_SchnellsucheKFZ']);
		$Form->Erstelle_TextFeld('*BES_KFZ','',15,0,true,'','','','T','L','','',15,'','f');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['txt_SchnellsucheKUNDE'].':',60,'text-align:right','',$AWISSprachKonserven['BES']['ttt_SchnellsucheKUNDE']);
		$Form->Erstelle_TextFeld('*BES_KUNDE','',30,0,true,'','','','T','L','','',30,'','f');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FIL'].':',40,'text-align:right','',$AWISSprachKonserven['BES']['ttt_SchnellsucheFil']);
		$Form->Erstelle_TextFeld('*BES_FIL_ID','',5,0,true,'','','','T','L','','',4,'','f');
		
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	}
	
	
	if((($Recht40000&2)==2) and (($Seite =='Konto' and isset($_GET['ZKEY']) or $AWIS_KEY2 != '') or ($Seite =='Info' and isset($_GET['ID']) or $AWIS_KEY2 != '') or ($Seite =='WV' and isset($_GET['ID'])) or ($AWIS_KEY1 == '-1') or ($Seite =='Haupt') ))
	{
	   
	    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');    
	}
	
	
	//Wenn der User auf einem Datensatz ist, und auf keinem anderen Reiter als dem Hauptreiter ist. (Wenn $_GET['Seite'] nicht gesetzt ist, befindet er sich am Hauptreiter)
	if(($Recht40000&8)==8 and $AWIS_KEY1 > 0 and ((isset($_GET['Seite']) and $_GET['Seite'] == 'Haupt' ) or !isset($_GET['Seite'])))
	{
	    $Form->Schaltflaeche('href', 'cmdDrucken', '/berichte/drucken.php?XRE=59&ID='.base64_encode("&BEARB_ID="  . $AWIS_KEY1.''), '/bilder/cmd_pdf.png');
	    
	}
	if(($Recht40000&4)==4 and (($AWIS_KEY1 != '-1' and $rsErgebnis->AnzahlDatensaetze() > 1) and $Neuanlage != 1) )
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->ZeileEnde();
	$Form->SchaltflaechenEnde();
	$Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_BES',serialize($Param));
}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{

	global $DB;

	$Bedingung = '';

	if(isset($Param['BES_FIL_ID']) AND $Param['BES_FIL_ID']!='')
	{
		$Bedingung .= 'AND FILNR ' . $DB->LikeOderIst($Param['BES_FIL_ID'],0,'BES') . ' ';
	}
	
	if(isset($Param['BES_BEARBEITUNG']) AND $Param['BES_BEARBEITUNG']!='')
	{
		$Bedingung .= 'AND BEARBEITUNGSNR ' . $DB->LikeOderIst($Param['BES_BEARBEITUNG'],0,'BES') . ' ';
	}
	if(isset($Param['BES_VORGANG']))
	{
		if($Param['BES_VORGANG']=='O')
		{
			$Bedingung .= " AND (AKTEGESCHLOSSENAM is null or AKTEGESCHLOSSENAM='')";
		}
		elseif($Param['BES_VORGANG']=='G')
		{
			$Bedingung .= " AND AKTEGESCHLOSSENAM is not null";
		}
	}
	if(isset($Param['BES_AART']) AND $Param['BES_AART']!='' AND $Param['BES_AART'] != '0')
	{
		$Bedingung .= ' AND ANTRAGART ' . $DB->LikeOderIst($Param['BES_AART'],0,'BES') . ' ';
	}
	if(isset($Param['BES_AZ']) AND $Param['BES_AZ']!='')
	{
		$Bedingung .= ' AND  AKTENZEITENGERICHT '. $DB->LikeOderIst('%' . $Param['BES_AZ'] . '%',0 , 'BES' ) ;
	}
	if(isset($Param['BES_REKLANR']) AND $Param['BES_REKLANR']!='')
	{
		$Bedingung .= ' AND  REKLASCHEINNR ' . $DB->LikeOderIst($Param['BES_REKLANR'],0,'BES') . ' ';
	}
	
	if (isset($Param['BES_SCHADENJAHR']) AND (!empty($Param['BES_SCHADENJAHR'])))
	{
		$Bedingung .= ' AND (eingabeam >= ' . $DB->WertSetzen('BES','D', '01.01.'.$Param['BES_SCHADENJAHR']) . ')';
		$Bedingung .= ' AND (eingabeam <= ' . $DB->WertSetzen('BES','D', '31.12.'.$Param['BES_SCHADENJAHR']) . ')';
		
	}
	
	if (isset($Param['BES_DATUMVON']) AND (!empty($Param['BES_DATUMVON'])))
	{
		$Bedingung .= ' AND (eingabeam >= ' . $DB->WertSetzen('BES','D', $Param['BES_DATUMVON']) . ')';
	}
	 
	if (isset($Param['BES_DATUMBIS']) AND (!empty($Param['BES_DATUMBIS'])))
	{
		$Bedingung .= ' AND (eingabeam <= ' . $DB->WertSetzen('BES','D', $Param['BES_DATUMBIS']) . ')';
	}
	
	if(isset($Param['BES_KUNDE']) AND $Param['BES_KUNDE']!='')
	{
		$Namen = explode(' ',$Param['BES_KUNDE']);
		
			foreach ($Namen as $Name)
			{
				if($Name != '')
				{
				
					$Bedingung .= 'AND (UPPER(KUNDENNAME) ' . $DB->LikeOderIst('%'.$Name.'%',1,'BES') . ' ';
					$Bedingung .= 'OR UPPER(VORNAME) ' . $DB->LikeOderIst('%'.$Name.'%',1,'BES') . ') ';
				}
			}
	}
	if(isset($Param['BES_STRASSE']) AND $Param['BES_STRASSE']!='')
	{
		$Bedingung .= 'AND UPPER(STRASSE) ' . $DB->LikeOderIst($Param['BES_STRASSE'],1,'BES') . ' ';
	}
	if(isset($Param['BES_PLZ']) AND $Param['BES_PLZ']!='')
	{
		$Bedingung .= 'AND UPPER(PLZ) ' . $DB->LikeOderIst($Param['BES_PLZ'],1,'BES') . ' ';
	}
	if(isset($Param['BES_ORT']) AND $Param['BES_ORT']!='')
	{
		$Bedingung .= 'AND UPPER(ORT) ' . $DB->LikeOderIst($Param['BES_ORT'],1,'BES') . ' ';
	}
	if(isset($Param['BES_TELEFON']) AND $Param['BES_TELEFON']!='')
	{
		$Bedingung .= 'AND UPPER(TELEFON) ' . $DB->LikeOderIst($Param['BES_TELEFON'],1,'BES') . ' ';
	}
	if(isset($Param['BES_EMAIL']) AND $Param['BES_EMAIL']!='')
	{
		$Bedingung .= 'AND UPPER(EMAIL) ' . $DB->LikeOderIst($Param['BES_EMAIL'],1,'BES') . ' ';
	}
	if(isset($Param['BES_CARD']) AND $Param['BES_CARD']!='')
	{
		$Bedingung .= 'AND UPPER(PAN) ' . $DB->LikeOderIst($Param['BES_CARD'],1,'BES') . ' ';
	}
	if(isset($Param['BES_SACHBEARBEITER']) AND $Param['BES_SACHBEARBEITER']!='')
	{
		$Bedingung .= 'AND UPPER(SBNAME) ' . $DB->LikeOderIst($Param['BES_SACHBEARBEITER'],1,'BES') . ' ';
	}
	if(isset($Param['BES_VERSNR']) AND $Param['BES_VERSNR']!='')
	{
		$Bedingung .= 'AND UPPER(VERSNR)  ' . $DB->LikeOderIst('%'.$Param['BES_VERSNR'].'%',1,'BES')  ;
	}
	if (isset($Param['BES_KFZ']) AND (! empty($Param['BES_KFZ'])))
	{
		$Bedingung .= ' AND (UPPER(lartnrkomp(KFZ_KENNZ)) like UPPER(\'%\'||lartnrkomp(\'' . $Param['BES_KFZ'] . '\')||\'%\'))';
	}	
	if(isset($Param['BES_GROSSKDNR']) AND $Param['BES_GROSSKDNR']!='')
	{
		$Bedingung .= 'AND UPPER(GROSSKDNR) ' . $DB->LikeOderIst($Param['BES_GROSSKDNR'],1,'BES') . ' ';
	}
	
	return $Bedingung;
}

?>