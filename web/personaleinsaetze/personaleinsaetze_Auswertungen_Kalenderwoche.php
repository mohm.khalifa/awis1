<?php

/***************************************
* Urlaubsplan
***************************************/
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");
require_once("awisBenutzer.inc");
require_once('awisAusdruck.php');


global $AWIS_KEY1;

try 
{	
	$AWISBenutzer = awisBenutzer::Init();	
	$Form = new awisFormular();
	
	$TextKonserven = array();				
	$TextKonserven[]=array('Ausdruck','txt_KeineDatenGefunden');	
	$TextKonserven[]=array('Ausdruck','txt_HinweisAusdruckIntern');
	$TextKonserven[]=array('Wort','wrd_Stand');	
	$TextKonserven[]=array('Wort','Seite');
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven,($AWISBenutzer->BenutzerSprache()=='CZ'?'DE':$AWISBenutzer->BenutzerSprache()));
	
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Recht4505 = $AWISBenutzer->HatDasRecht(4505);
	if($Recht4505==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI_Auswertung'));					
	
	
	//**************************************
	// Rechte des Benutzers ermitteln
	//**************************************
	$SQL = 'SELECT DISTINCT PBR_PBZ_KEY ';
	$SQL .= ' FROM PerseinsbereicheRechteVergabe';
	$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBR_PBM_KEY = PBM_KEY';
	$SQL .= ' INNER JOIN benutzer ON PBM_XBN_KEY = XBN_KEY AND XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
	$SQL .= ' WHERE PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	if(isset($Param['PEB_KEY']) AND $Param['PEB_KEY']!=0)
	{
		$SQL .= ' AND PBM_PEB_KEY = 0'.$Param['PEB_KEY'];
	}
	$SQL .= ' AND (XBN_STATUS = \'A\' OR XBN_KEY = 0'.$AWISBenutzer->BenutzerID().')';
	$rsPBR = $DB->RecordSetOeffnen($SQL);
	$RechtPEI = 0;
	while(!$rsPBR->EOF())
	{
		$RechtPEI = $RechtPEI  | pow(2,($rsPBR->FeldInhalt('PBR_PBZ_KEY')-1));

		$rsPBR->DSWeiter();
	}
		
	
	//Mitarbeiter
	$SQL = "SELECT DISTINCT xbn_key, xbn_name ";
	$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
	$SQL .= "       FROM Perseinsbereichemitglieder";
	$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
	$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
	$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ' AND PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	$SQL .= " GROUP BY PEB_KEY) Bereiche";
	//	$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
	$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND pbm_gueltigab <= sysdate and pbm_gueltigbis >= sysdate and CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key ";	//TR 070211
	// 04.06.2009 SK	
	//$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key ";
	//$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGAB END <= SYSDATE ';
	//$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGBIS END >= SYSDATE ';
	$SQL .= " INNER JOIN Benutzer ON pbm_xbn_key = xbn_key";
	/*
	$SQL .= " AND EXISTS(
						    select * from(
						      SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT, PBM_GUELTIGAB, PBM_GUELTIGBIS
						      FROM Perseinsbereichemitglieder
						      INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY
						      INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY
						      WHERE PBM_XBN_KEY = 0".$AWISBenutzer->BenutzerID() . "";
	$SQL .= '     GROUP BY PEB_KEY, PBM_GUELTIGAB, PBM_GUELTIGBIS';
	$SQL .= ' ) MeineBereiche WHERE PEB_KEY = PBM_PEB_KEY';
	$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGAB END <= SYSDATE ';
	$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGBIS END >= SYSDATE ';

	$SQL .= ' )';
	*/
	$SQL .= ' ORDER BY 2';
	
	//$Form->DebugAusgabe(1,$SQL);
	//die();

	$rsMIT = $DB->RecordSetOeffnen($SQL);
	$rsMITZeilen = $rsMIT->AnzahlDatensaetze();
	
	$Mitarbeiter=array();
	
	while(!$rsMIT->EOF())
	{
		$Mitarbeiter[$rsMIT->FeldInhalt('XBN_KEY')]=array('Anzeige'=>0,'Name'=>$rsMIT->FeldInhalt('XBN_NAME'));
		$rsMIT->DSWeiter();
	}
	
	$SQL = "SELECT distinct xbn_name AS Mitarbeiter, XBN_KEY, PEA_ABKUERZUNG, PEI_DATUM, PEA_KEY, PEA_BEZEICHNUNG, PEA_ABKUERZUNG, PEI_FIL_ID,";				 		
	$SQL .= " to_char(PEI_DATUM,'RRRR-iw') AS KW, PEI_VONXTZ_ID ";	
	//$SQL .= ' INNER JOIN Regionalzentren ON MIT_REZ_ID = REZ_ID';			
	$SQL .= ' FROM Personaleinsaetze';
	$SQL .= ' INNER JOIN Benutzer ON PEI_XBN_KEY = XBN_KEY';	
	$SQL .= ' LEFT OUTER JOIN Kontakte ON XBN_KON_KEY = KON_KEY';
	$SQL .= ' INNER JOIN PersEinsArten ON PEA_KEY = PEI_PEA_KEY'; //AND BITAND(PEA_AUSWERTUNG,1)=1';		
	
	// SK - 04.06.2009
	if(($RechtPEI&64)==0)	// Nur Bereichsverwalter sehen alle
	{
		$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBM_XBN_KEY = XBN_KEY AND PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	}
	else
	{
		$SQL .= ' INNER JOIN PerseinsbereicheMitglieder ON PBM_XBN_KEY = XBN_KEY AND PBM_GUELTIGAB <= SYSDATE AND PBM_GUELTIGBIS >= SYSDATE';
	}
	
	$SQL .= ' WHERE PEA_KEY <> 9 AND (PEI_PLANUNGSTYP is null or PEI_PLANUNGSTYP = 2)';
	//$SQL .= ' WHERE PEA_KEY <> 9';
		
	//$SQL .= " MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";

	if(isset($Param['PEI_DATUM_VOM']) AND $Param['PEI_DATUM_VOM']!='')
	{
		$SQL .= ' AND PEI_DATUM >= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_VOM'],false);
	}
	
	if(isset($Param['PEI_DATUM_BIS']) AND $Param['PEI_DATUM_BIS']!='')
	{
		$SQL .= ' AND PEI_DATUM <= '.$DB->FeldInhaltFormat('D',$Param['PEI_DATUM_BIS'],false);
	}
	
	if(isset($Param['PEI_FIL_ID']) AND $Param['PEI_FIL_ID']!='')
	{
		$SQL .= ' AND PEI_FIL_ID = '.$DB->FeldInhaltFormat('N0',$Param['PEI_FIL_ID'],false);
	}
	
	// Einschränken nach Benutzer
	if(isset($Param['PBM_XBN_KEY']) AND $Param['PBM_XBN_KEY']!='')
	{
		if($Param['PBM_XBN_KEY']==0)
		{
			
			$SQL .= " AND EXISTS(
						    select * from(
						      SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT, PBM_GUELTIGAB, PBM_GUELTIGBIS
						      FROM Perseinsbereichemitglieder
						      INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY
						      INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY
						      WHERE PBM_XBN_KEY = 0".$AWISBenutzer->BenutzerID() . "";
			$SQL .= '     GROUP BY PEB_KEY, PBM_GUELTIGAB, PBM_GUELTIGBIS';
			$SQL .= ' ) MeineBereiche WHERE PEB_KEY = PBM_PEB_KEY';
			$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGAB END <= SYSDATE ';
			$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGBIS END >= SYSDATE ';

			$SQL .= ' )';
			
			/*
			$SQL .= " AND EXISTS (SELECT *";
			$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
			$SQL .= "       FROM Perseinsbereichemitglieder";
			$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
			$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
			$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
			$SQL .= " GROUP BY PEB_KEY) Bereiche";
			$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
			$SQL .= ' WHERE pbm_xbn_key = PEI_XBN_KEY';
			$SQL .= ' )';
			*/
		}
		else
		{
			$SQL .= ' AND PEI_XBN_KEY = 0'.$DB->FeldInhaltFormat('N0',$Param['PBM_XBN_KEY'],false);
		}
	}
	
	if(isset($Param['FER_KEY']) AND $Param['FER_KEY']!='0')
	{

		$SQL .= ' AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN';
		$SQL .= ' WHERE xx1_fer_key =0'.$DB->FeldInhaltFormat('N0',$Param['FER_KEY'],false);
		$SQL .= ' AND xx1_kon_key = KON_KEY)';
		/*
		  $Bedingung = " AND EXISTS (SELECT *
		  FROM (
		  SELECT KON_KEY, XX1_FER_KEY, FEB_GUELTIGAB, FRZ_GUELTIGAB, FEZ_GUELTIGAB, FER_GUELTIGAB,
		  FEB_GUELTIGBIS, FRZ_GUELTIGBIS, FEZ_GUELTIGBIS, FER_GUELTIGBIS
		  FROM v_FilialEbenenRollen
		  INNER JOIN Kontakte ON XX1_KON_KEY = KON_KEY
		  INNER JOIN FilialEbenen ON XX1_FEB_KEY = FEB_KEY
		  INNER JOIN FilialEbenenRollenZuordnungen ON XX1_FRZ_KEY = FRZ_KEY
		  INNER JOIN FilialEbenenZuordnungen ON XX1_FEZ_KEY = FEZ_KEY
		  INNER JOIN FilialEbenenRollen ON XX1_FER_KEY = FER_KEY
		  INNER JOIN FilialEbenenRollenBereiche ON FER_FRB_KEY = FRB_KEY
		  WHERE XX1_FER_KEY =".$Param['FER_KEY']."
		  ORDER BY XX1_STUFE, FRZ_GUELTIGBIS, FRZ_USERDAT DESC
		  ) Daten WHERE KON_KEY = XBN_KON_KEY
		  AND FEB_GUELTIGAB <= PEI_DATUM AND FEB_GUELTIGBIS >= PEI_DATUM
		  AND FRZ_GUELTIGAB <= PEI_DATUM AND FRZ_GUELTIGBIS >= PEI_DATUM
		  AND FEZ_GUELTIGAB <= PEI_DATUM AND FEZ_GUELTIGBIS >= PEI_DATUM
		  AND FER_GUELTIGAB <= PEI_DATUM AND FER_GUELTIGBIS >= PEI_DATUM
		  ) ";
		*/
	}
/*		
	if($_POST['txtMIT_MTA_ID']!='-1')
	{
		$SQL .= " AND MIT_MTA_ID=" . $_POST['txtMIT_MTA_ID'] . "";
	}
	
	if(($RechteStufe&2)==2)	// Einschränkung Technik-Übersicht
	{
		$SQL .= ' AND MIT_MTA_ID IN (30,40,50)';
	}
*/	
	$SQL .= " ";
	$SQL .= " ORDER BY to_char(PEI_DATUM,'RRRR-iw'), XBN_NAME, XBN_KEY, PEI_DATUM, PEI_VONXTZ_ID";

	//$Form->DebugAusgabe(1,$SQL);
	//die();
	

	$rsPEI = $DB->RecordSetOeffnen($SQL);
	$rsPEIZeilen = $rsPEI->AnzahlDatensaetze();
	
	$Vorlagen = array('BriefpapierATU_DE_Seite_2_quer.pdf');

	$Ausdruck = new awisAusdruck('L','A4',$Vorlagen,'Personaleinsatz');

	//$Ausdruck->_pdf->SetAutoPageBreak(true,2);
	$Ausdruck->_pdf->SetAutoPageBreak(false,'');
	//$pdf->SetAuthor($AWISBenutzer->BenutzerName());
	
	if($rsPEIZeilen==0)
	{
		$Ausdruck->NeueSeite(0,1);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_KeineDatenGefunden'],0,0,'C',0);		
	}
		
	$RasterZeile=9999;
	$LetzterXBN_KEY = '';
	$LetzteKW = '';
	$Legende = array();
	$NeueZeile=false;
	//$MaxTag='';
		
	while(!$rsPEI->EOF())
	{		
		$NeueZeile=false;
		if($LetzteKW!=$rsPEI->FeldInhalt('KW') OR $RasterZeile>180)
		{			
			if($rsPEI->DSNummer()!=0)
			{
				/*
				foreach($Mitarbeiter AS $Eintrag)
				{
					if($Eintrag['Anzeige']==0)
					{
						
						if ($RasterZeile>180)
						{
							$Ausdruck->NeueSeite(0,1);
							
							$Ausdruck->_pdf->SetFont('Arial','',6);
							$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
					
							$Ausdruck->_pdf->setXY(10,12);
							$Ausdruck->_pdf->SetFont('Arial','',6);
							$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);							
					
							$RasterZeile = 10;
							$Ausdruck->_pdf->SetFont('Arial','',20);
							$Ausdruck->_pdf->text(10,$RasterZeile,"Urlaubstage " . $rsPEI->FeldInhalt('MONAT'));
							$Ausdruck->_RasterZeile = 25;
								
							$RasterZeile = 30;
							$Ausdruck->_pdf->SetFillColor(240,240,240);
							$Ausdruck->_pdf->SetDrawColor(0,0,0);
							$Ausdruck->_pdf->setXY(10,$RasterZeile);
						
							$Ausdruck->_pdf->SetFont('Arial','',10);
							$Ausdruck->_pdf->cell(80,5,'Mitarbeitername',1,0,'L',1);			
							$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
							$MaxTag = date('d',(mktime(0,0,0,date('m',$Datum)+1,0,date('Y',$Datum))));
							
							for($Tag = 1;$Tag<=$MaxTag;$Tag++)
							{
								$Ausdruck->_pdf->cell(5,5,$Tag,1,0,'C',1);	
							}
							
						}
						
						$RasterZeile+=5;
						
						$Ausdruck->_pdf->SetFillColor(240,240,240);
						$Ausdruck->_pdf->SetDrawColor(0,0,0);
						$Ausdruck->_pdf->setXY(10,$RasterZeile);
				
						$Ausdruck->_pdf->SetFont('Arial','',10);
						$Ausdruck->_pdf->cell(80,5,$Eintrag['Name'],1,0,'L',1);
						for($Tag = 1;$Tag<=$MaxTag;$Tag++)
						{
							$Ausdruck->_pdf->cell(5,5,'',1,0,'C',0);	
						}
					}
				}
				*/
				
				//$Ausdruck->_pdf->SetXY(15,20);
				//$Ausdruck->_pdf->SetDrawColor(0,0,0);
				//$Ausdruck->_pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
				//$Legende = array();
			}
			
			$Ausdruck->NeueSeite(0,1);
				
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
	
			$Ausdruck->_pdf->setXY(10,12);
			$Ausdruck->_pdf->SetFont('Arial','',6);
			$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);							
	
			$RasterZeile = 10;
			$Ausdruck->_pdf->SetFont('Arial','',20);
			$Ausdruck->_pdf->Text(10,$RasterZeile,"Wochenplan " . $rsPEI->FeldInhalt('KW'));
			$Ausdruck->_RasterZeile = 25;
			
			$RasterZeile = 30;
			$Ausdruck->_pdf->SetFillColor(240,240,240);
			$Ausdruck->_pdf->SetDrawColor(0,0,0);
			$Ausdruck->_pdf->setXY(10,$RasterZeile);
	
			$Ausdruck->_pdf->SetFont('Arial','',10);
			$Ausdruck->_pdf->cell(30,5,'Mitarbeitername',1,0,'L',1);			
			/*$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
			$MaxTag = date('d',(mktime(0,0,0,date('m',$Datum)+1,0,date('Y',$Datum))));
			
			for($Tag = 1;$Tag<=$MaxTag;$Tag++)
			{
				$Ausdruck->_pdf->cell(5,5,$Tag,1,0,'C',1);	
			}
			*/
			$Ausdruck->_pdf->cell(35,5,'Montag',1,0,'C',1);	
			$Ausdruck->_pdf->cell(35,5,'Dienstag',1,0,'C',1);	
			$Ausdruck->_pdf->cell(35,5,'Mittwoch',1,0,'C',1);	
			$Ausdruck->_pdf->cell(35,5,'Donnerstag',1,0,'C',1);	
			$Ausdruck->_pdf->cell(35,5,'Freitag',1,0,'C',1);	
			$Ausdruck->_pdf->cell(35,5,'Samstag',1,0,'C',1);	
			$Ausdruck->_pdf->cell(35,5,'Sonntag',1,0,'C',1);	
			
			$LetzteKW=$rsPEI->FeldInhalt('KW');			
					
			$NeueZeile=true;
	
			// Alle Eintraege zurücksetzen
			foreach($Mitarbeiter AS $Eintrag=>$Werte)
			{
				$Mitarbeiter[$Eintrag]['Anzeige']=0;
			}
		}
				
		// Neuer Mitarbeiter -> neue Zeile
		if($LetzterXBN_KEY!=$rsPEI->FeldInhalt('XBN_KEY') or $NeueZeile==true)
		{
			$RasterZeile += 5;
			
			$Ausdruck->_pdf->SetFillColor(240,240,240);
			$Ausdruck->_pdf->SetDrawColor(0,0,0);
			$Ausdruck->_pdf->setXY(10,$RasterZeile);
	
			$Ausdruck->_pdf->SetFont('Arial','',10);
			$Ausdruck->_pdf->cell(30,5,$rsPEI->FeldInhalt('MITARBEITER'),1,0,'L',1);
			$Mitarbeiter[$rsPEI->FeldInhalt('XBN_KEY')]['Anzeige']=1;
			/*
			for($Tag = 1;$Tag<=$MaxTag;$Tag++)
			{
				$Ausdruck->_pdf->cell(5,5,'',1,0,'C',0);	
			}
			*/
			$LetzterXBN_KEY=$rsPEI->FeldInhalt('XBN_KEY');
		}
						
		$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
		$Tag = date('d',$Datum);
		$Wtag = date('w', $Datum);			
						
		$Ausdruck->_pdf->SetFont('Arial','',6);		
		$Ausgabe='';		
		$LetzterXBN_KEY = $rsPEI->FeldInhalt('XBN_KEY');			
				
		//Alle Daten für einen Tag zusammenfassen
		while (!$rsPEI->EOF() AND $LetzterXBN_KEY == $rsPEI->FeldInhalt('XBN_KEY') and ($Datum == $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true)))
		{						
			If ($Ausgabe != '')
			{
				$Ausgabe .= ' / ';
			}
						
			//$Ausgabe.= ($rsPEI->FeldInhalt('PEA_KEY')=='8'?$rsPEI->FeldInhalt('PEI_FIL_ID'):$rsPEI->FeldInhalt('PEA_BEZEICHNUNG'));
			$Ausgabe.= ($rsPEI->FeldInhalt('PEA_KEY')=='8'?$rsPEI->FeldInhalt('PEI_FIL_ID'):$rsPEI->FeldInhalt('PEA_ABKUERZUNG'));
			
			$LetzterXBN_KEY = $rsPEI->FeldInhalt('XBN_KEY');				
			$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
			
			$rsPEI->DSWeiter();
		}							
				
		if ($Wtag == '1')
		{
			$Ausdruck->_pdf->setXY(40,$RasterZeile);			
		}
		elseif ($Wtag == '2')
		{
			$Ausdruck->_pdf->setXY(40+35,$RasterZeile);
		}
		elseif ($Wtag == '3')
		{
			$Ausdruck->_pdf->setXY(40+70,$RasterZeile);
		}
		elseif ($Wtag == '4')
		{
			$Ausdruck->_pdf->setXY(40+105,$RasterZeile);
		}
		elseif ($Wtag == '5')
		{
			$Ausdruck->_pdf->setXY(40+140,$RasterZeile);
		}
		elseif ($Wtag == '6')
		{
			$Ausdruck->_pdf->setXY(40+175,$RasterZeile);
		}
		elseif ($Wtag == '0')
		{
			$Ausdruck->_pdf->setXY(40+210,$RasterZeile);
		}
		
		$Ausdruck->_pdf->cell(35,5,$Ausgabe,1,0,'C',0);			
		
		//$rsPEI->DSWeiter();
	}
			
	if($rsPEI->DSNummer()>0)
	{
		foreach($Mitarbeiter AS $Eintrag)
		{
			if($Eintrag['Anzeige']==0)
			{
				if ($RasterZeile>180)
				{
					$Ausdruck->NeueSeite(0,1);
					
					$Ausdruck->_pdf->SetFont('Arial','',6);
					$Ausdruck->_pdf->cell(270,6,$AWISSprachKonserven['Ausdruck']['txt_HinweisAusdruckIntern'],0,0,'C',0);
			
					$Ausdruck->_pdf->setXY(10,12);
					$Ausdruck->_pdf->SetFont('Arial','',6);
					$Ausdruck->_pdf->cell(10,6,$AWISSprachKonserven['Wort']['wrd_Stand'] . date('d.m.Y'),0,0,'L',0);							
			
					$RasterZeile = 10;
					$Ausdruck->_pdf->SetFont('Arial','',20);
					$Ausdruck->_pdf->text(10,$RasterZeile,"Wochenplan " . $rsPEI->FeldInhalt('KW'));
					$Ausdruck->_RasterZeile = 25;
						
					$RasterZeile = 30;
					$Ausdruck->_pdf->SetFillColor(240,240,240);
					$Ausdruck->_pdf->SetDrawColor(0,0,0);
					$Ausdruck->_pdf->setXY(10,$RasterZeile);
				
					$Ausdruck->_pdf->SetFont('Arial','',10);
					$Ausdruck->_pdf->cell(80,5,'Mitarbeitername',1,0,'L',1);			
					$Datum = $Form->PruefeDatum($rsPEI->FeldInhalt('PEI_DATUM'),0,'',true);
					$MaxTag = date('d',(mktime(0,0,0,date('m',$Datum)+1,0,date('Y',$Datum))));
					
					for($Tag = 1;$Tag<=$MaxTag;$Tag++)
					{
						$Ausdruck->_pdf->cell(5,5,$Tag,1,0,'C',1);	
					}
					
				}
				
				$RasterZeile+=5;
				
				$Ausdruck->_pdf->SetFillColor(240,240,240);
				$Ausdruck->_pdf->SetDrawColor(0,0,0);
				$Ausdruck->_pdf->setXY(10,$RasterZeile);
		
				$Ausdruck->_pdf->SetFont('Arial','',10);
				$Ausdruck->_pdf->cell(80,5,$Eintrag['Name'],1,0,'L',1);
				for($Tag = 1;$Tag<=$MaxTag;$Tag++)
				{
					$Ausdruck->_pdf->cell(5,5,'',1,0,'C',0);					
				}					
			}
		}
		
		//$Ausdruck->_pdf->SetXY(15,20);
		//$Ausdruck->_pdf->SetDrawColor(0,0,0);
		//$Ausdruck->_pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
		//$Legende = array();
		
		//$Ausdruck->_pdf->SetXY(15,20);
		//$Ausdruck->_pdf->SetDrawColor(0,0,0);
		//$Ausdruck->_pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
	}
	
	$Ausdruck->Anzeigen();	
}
			
	
catch (awisException $ex)
{
	echo 'AWIS-Fehler:'.$ex->getMessage();
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}

?>	