<?php
/**
 * Suche Stammdatenpflege GDV-Felder
 *
 * @author Sch�ffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607131457
 * @todo
 */
global $AWISBenutzer;
global $AWISCursorPosition;

try
{
	$TextKonserven = array();
	$TextKonserven[]=array('GDV','*');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','AlleAnzeigen');

	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht44001 = $AWISBenutzer->HatDasRecht(44001);

	if($Recht44001==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	$Form->SchreibeHTMLCode('<form name="frmFelder" action="felder_Main.php?cmdAktion=Details" method="POST"  >');
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_GDV_FE'));

	$Form->Formular_Start();
	/**
	 *  Versicherung
	 */
	/* Daten beschaffen */
	$SQL  ='SELECT ';
	$SQL .='VVE_VERSNR,VVE_BEZEICHNUNG ';
	$SQL .=' FROM ';
	$SQL .='versversandweg ';
	$SQL .=' inner join versversicherungen on VVW_VERSNR = VVE_VERSNR';
	$SQL .=' where VVW_RELEASE is not null';
	$SQL .=' order by VVE_BEZEICHNUNG ';


	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['GDV']['VVW_VERSICHERUNG'].':',200);
	$AWISCursorPosition= 'sucVMV_VERSICHERUNG';
	$Form->Erstelle_SelectFeld('*VMV_VERSICHERUNG',($Param['SPEICHERN']=='on'?$Param['VMV_VERSICHERUNG']:''),150,true,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'',2,'','','','',array(),'');
	$Form->ZeileEnde();

	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();
	$Form->Formular_Ende();

	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','/gdvService/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	$Form->SchaltflaechenEnde();

	$Form->SetzeCursor($AWISCursorPosition);
	$Form->SchreibeHTMLCode('</form>');
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>