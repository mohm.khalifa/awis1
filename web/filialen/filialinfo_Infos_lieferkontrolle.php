<?php
//********************************************************************************************
// Lieferkontrolle
//********************************************************************************************
//
// Dieses Modul erm�glicht die Anzeige und das Bearbeiten der Lieferkontrollen f�r die
// Warenanlieferungen an eine Filiale
//
//********************************************************************************************
// Autor: Sacha Kerres
// Datum: 17.07.2007
//********************************************************************************************
// �nderungen:
// WER                   WANN      WAS
//
//
//********************************************************************************************
require_once("awis_forms.inc.php");

global $con;
global $awisRSZeilen;
global $AWISSprache;
global $AWISBenutzer;
global $SpeichernButton;

$TXT_Baustein = array();
$TXT_Baustein[]=array('LIK','LIK_%');
$TXT_Baustein[]=array('LIK','LieferscheinAbschliessen');
$TXT_Baustein[]=array('LIK','lst_Lieferkontrollenstatus');
$TXT_Baustein[]=array('Liste','lst_LIK_STATUS');
$TXT_Baustein[]=array('Wort','KeineLieferkontrollen');
$TXT_Baustein[]=array('Wort','Abgeschlossen');
$TXT_Baustein[]=array('Wort','NichtAbgeschlossen');
$TXT_Baustein[]=array('Wort','Uploaddatei');
$TXT_Baustein[]=array('Wort','PDFErzeugen');
$TXT_Baustein[]=array('Wort','AlleAnzeigen');
$TXT_Baustein[]=array('Wort','KeineBerechtigungLieferungskontrollen');
$TXT_Baustein[]=array('Wort','lbl_weiter');
$TXT_Baustein[]=array('Wort','Ja');
$TXT_Baustein[]=array('Wort','Nein');
$TXT_Baustein[]=array('LIK','WirklichAbschliessen');
$TXT_Baustein[]=array('Wort','HinweisSpeichern');
$TXT_Baustein[]=array('LIK','HinweisDifferenzen');
$TXT_Baustein[]=array('Wort','Zwangsabschluss');
$TXT_Baustein[]=array('LIK','HinweisAbgeschlossen');
$TXT_Baustein[]=array('Wort','Zurueck');
$TXT_Baustein[]=array('Wort','Gesamtbestand');
$TXT_Baustein[]=array('Wort','Urspruengliche');


$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);

// Aktueller Lieferschein?
$LSNR = (isset($_GET['LSNR'])?$_GET['LSNR']:(isset($_POST['txtLIK_LIEFERSCHEINNR'])?$_POST['txtLIK_LIEFERSCHEINNR']:''));
$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
$FIL_ID = $FIL_ID[1];

$RechteStufe = awisBenutzerRecht($con,3600);	// MDE Lieferkontrolle
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");     
}

$SpeichernButton = False;

if(($RechteStufe & 2)==2 or ($RechteStufe & 4)==4)
{
	$SpeichernButton = True;
}


// Beschr�nkung auf eine Filiale?
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
//var_dump($UserFilialen); 

if($UserFilialen!='' AND intval($FIL_ID) <> intval($UserFilialen))
{
	awis_FORM_Hinweistext($TXT_Baustein['Wort']['KeineBerechtigungLieferungskontrollen'],1);
	die;
}

if(isset($_POST['cmdSpeichern_x']))
{
	include './filialinfo_speichern.php';
}
elseif(isset($_POST['cmdAbschliessen_x']))
{
	include './filialinfo_speichern.php';
	
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id=']=intval($FIL_ID);
	$BindeVariablen['var_N0_lkk_lieferscheinnr=']=intval($LSNR);
	
	$SQL = 'SELECT *';
	$SQL .=' FROM Lieferkontrollenkopf LKK';
	$SQL .=' INNER JOIN Lieferkontrollen LIK ON LIK_LKK_KEY = LKK_KEY';
	$SQL .=' WHERE LKK_FIL_ID=:var_N0_fil_id';
	$SQL .= ' AND LKK_LieferscheinNr=:var_N0_lkk_lieferscheinnr';
	$SQL .= ' AND LIK_MENGESOLL <> LIK_MENGEIST';
	$SQL .= ' AND LIK_STATUS < 12';
	$SQL .= ' AND (LIK_STATUSLIEFERUNG is null or LIK_STATUSLIEFERUNG <> 1) ';
	$SQL .= ' ORDER BY LIK_AST_ATUNR ASC';
	
	awis_Debug(1,$SQL);
	
	$rsLIK=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
	$rsLIKZeilen = $awisRSZeilen;
	
	if ($rsLIKZeilen > 0)
	{
		echo '<span class=HinweisText>'.$TXT_Baustein['LIK']['HinweisDifferenzen'].'&nbsp;' .$rsLIKZeilen .'</span>';
		echo "<br><br>";
	}
	
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id=']=intval($FIL_ID);
	$BindeVariablen['var_N0_lik_lieferscheinnr=']=intval($LSNR);
	
	$SQL = 'SELECT distinct lik_statuslieferung';
	$SQL .= ' ,(SELECT MIN(LIK_STATUS) AS STATUS FROM Lieferkontrollen WHERE Lieferkontrollen.LIK_LieferscheinNr=LIK.LIK_LieferscheinNr AND Lieferkontrollen.LIK_FIL_ID = LIK.LIK_FIL_ID) AS LIK_STATUS';
	$SQL .=' FROM Lieferkontrollen LIK';
	$SQL .=' WHERE LIK_FIL_ID=:var_N0_fil_id';
	$SQL .= ' AND LIK_LieferscheinNr=:var_N0_lik_lieferscheinnr';
	
	awis_Debug(1,$SQL);
	
	$rsLIK=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
	$rsLIKZeilen = $awisRSZeilen;
	
	if ($rsLIK['LIK_STATUSLIEFERUNG'][0]=='1' or $rsLIK['LIK_STATUS'][0]>=10)
	{
		echo '<span class=HinweisText>'.$TXT_Baustein['LIK']['HinweisAbgeschlossen'].'</span>';
		echo "<br><br>";
		
		echo '<form name=frmBestaetigen action=./filialinfo_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
		//echo '<span class=HinweisText>'.$TXT_Baustein['LIK']['WirklichAbschliessen'].'</span>';
	
		//echo '<br><input type=submit name=cmdAbschliessenOK style="width:50px" value='.$TXT_Baustein['Wort']['Ja'].'>';
		echo '<input type=submit name=cmdAbschliessenAbbrechen style="width:50px" value='.$TXT_Baustein['Wort']['Zurueck'].'>';
	
		echo '<input type=hidden name=txtLIK_LIEFERSCHEINNR value='.$LSNR.'>';
		echo '</form>';
		
	}
	else 
	{
		echo '<form name=frmBestaetigen action=./filialinfo_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
		echo '<span class=HinweisText>'.$TXT_Baustein['LIK']['WirklichAbschliessen'].'</span>';
	
		echo '<br><input type=submit name=cmdAbschliessenOK style="width:50px" value='.$TXT_Baustein['Wort']['Ja'].'>';
		echo '&nbsp;<input type=submit name=cmdAbschliessenAbbrechen style="width:50px" value='.$TXT_Baustein['Wort']['Nein'].'>';
	
		echo '<input type=hidden name=txtLIK_LIEFERSCHEINNR value='.$LSNR.'>';
		echo '</form>';
	}
	
	awisLogoff($con);
	die();
}
elseif(isset($_POST['cmdAbschliessenOK']))
{
	//Kopf auf Abgeschlossen setzen
	$SQL = 'UPDATE Lieferkontrollenkopf';
	$SQL .= ' SET LKK_Status=\'A\'';
	$SQL .= ', LKK_user=\''.$AWISBenutzer->BenutzerName().'\'';			
	$SQL .= ' , LKK_UserDat=SYSDATE';	
	$SQL .= ' WHERE LKK_FIL_ID='.$FIL_ID;
	$SQL .= ' AND LKK_LIEFERSCHEINNR=0'.intval($_POST['txtLIK_LIEFERSCHEINNR']);
	
	if(awisExecute($con, $SQL)===false)
	{
		awisErrorMailLink('Lieferkontrollen abschliessen',1,'Fehler beim Speichern',$SQL);
	}
						
	$SQL = 'UPDATE Lieferkontrollen';
	$SQL .= ' SET LIK_Status=12';
	$SQL .= ', LIK_user=\''.$AWISBenutzer->BenutzerName().'\'';			
	$SQL .= ' , LIK_UserDat=SYSDATE';
	$SQL .= ' WHERE LIK_Status < 10 ';//AND LIK_LKK_KEY=0'.$AWIS_KEY1;		
	$SQL .= ' AND LIK_FIL_ID='.$FIL_ID;
	$SQL .= ' AND LIK_LIEFERSCHEINNR=0'.intval($_POST['txtLIK_LIEFERSCHEINNR']);
	
	if(awisExecute($con, $SQL)===false)
	{
		awisErrorMailLink('Lieferkontrollen abschliessen',1,'Fehler beim Speichern',$SQL);
	}
				
	$SQL = 'UPDATE Lieferkontrollen';
	$SQL .= ' SET LIK_StatusLieferung=1';
	$SQL .= ' , LIK_DatumAbschluss=SYSDATE';
	$SQL .= ', LIK_user=\''.$AWISBenutzer->BenutzerName().'\'';						
	$SQL .= ' , LIK_UserDat=SYSDATE';
	//$SQL .= ' WHERE LIK_LKK_KEY='.$AWIS_KEY1;	
	$SQL .= ' WHERE LIK_FIL_ID='.$FIL_ID;
	$SQL .= ' AND LIK_LIEFERSCHEINNR=0'.intval($_POST['txtLIK_LIEFERSCHEINNR']);
		
	if(awisExecute($con, $SQL)===false)
	{
		awisErrorMailLink('Lieferkontrollen abschliessen',1,'Fehler beim Speichern',$SQL);
	}			
}

if($LSNR!='')
{
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id=']=intval($FIL_ID);
	$BindeVariablen['var_N0_lkk_lieferscheinnr=']=intval($LSNR);
	
	$SQL = 'SELECT *';
	$SQL .=' FROM Lieferkontrollenkopf LKK';
	$SQL .=' INNER JOIN Lieferkontrollen LIK ON LIK_LKK_KEY = LKK_KEY';
	$SQL .=' WHERE LKK_FIL_ID=:var_N0_fil_id';
	$SQL .= ' AND LKK_LieferscheinNr=:var_N0_lkk_lieferscheinnr';
	if(!isset($_POST['txtAlleAnzeigen']))
	{
		//$SQL .= ' AND (LIK_MENGESOLL <> (LIK_MENGEIST+LIK_MENGEKORREKTUR)';
		$SQL .= ' AND LIK_MENGESOLL <> LIK_MENGEIST';
		$SQL .= ' AND LIK_STATUS < 12';
	}
	$SQL .= ' ORDER BY LIK_AST_ATUNR ASC';
	
	awis_Debug(1,$SQL);
}
else 
{
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id=']=intval($FIL_ID);
	
	$SQL = 'SELECT LKK_KEY, LKK_LieferscheinNr, LKK_DatumKomm, LKK_Status';
	//$SQL .= ' ,(SELECT MIN(LIK_STATUS) AS STATUS FROM Lieferkontrollen WHERE Lieferkontrollen.LIK_LieferscheinNr=LIK.LIK_LieferscheinNr AND Lieferkontrollen.LIK_FIL_ID = LIK.LIK_FIL_ID) AS LIK_STATUS';
	//$SQL .= ' ,(SELECT COUNT(*) AS ANZAHL FROM Lieferkontrollen WHERE Lieferkontrollen.LIK_LKK_KEY=LKK.LKK_KEY AND LIK_MENGEIST<>LIK_MENGESOLL) AS ANZAHL_DIFFERENZEN';	
	$SQL .=' FROM Lieferkontrollenkopf LKK';
	$SQL .=' WHERE LKK_FIL_ID=:var_N0_fil_id';
	$SQL .=' ORDER BY 3 DESC';
}

$rsLIK=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
$rsLIKZeilen = $awisRSZeilen;

awis_FORM_FormularStart();
/*
if($rsLIKZeilen==0)
{
	awis_FORM_Hinweistext($TXT_Baustein['Wort']['KeineLieferkontrollen'],1);
}
else
*/
if($LSNR=='')			// �bersicht zeigen
{
	awis_FORM_ZeileStart();	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_LIEFERSCHEINNR'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_DATUMKOMM'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_STATUS'],200);
	awis_FORM_ZeileEnde();
	
	for($LIKZeile=0;$LIKZeile<$rsLIKZeilen;$LIKZeile++)
	{
		awis_FORM_ZeileStart();					
		$Link='./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=Lieferkontrolle&LSNR='.$rsLIK['LKK_LIEFERSCHEINNR'][$LIKZeile].'';
		awis_FORM_Erstelle_ListenFeld('LIK_LIEFERSCHEINNR',$rsLIK['LKK_LIEFERSCHEINNR'][$LIKZeile],20,200,false,($LIKZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('LIK_DATUMKOMM',$rsLIK['LKK_DATUMKOMM'][$LIKZeile],20,200,false,($LIKZeile%2),'','','D');
		//$Status= (($rsLIK['LIK_STATUS'][$LIKZeile]>=10)?$TXT_Baustein['Wort']['Abgeschlossen']:$TXT_Baustein['Wort']['NichtAbgeschlossen']);
		$Status= $rsLIK['LKK_STATUS'][$LIKZeile];
		//$StatusLieferung= $rsLIK['LIK_STATUSLIEFERUNG'][$LIKZeile];
		
		$BindeVariablen=array();
		$BindeVariablen['var_N0_lik_lkk_key=']=$rsLIK['LKK_KEY'][$LIKZeile];
		
		$SQL = 'SELECT COUNT(*) AS ANZAHLDIFF FROM Lieferkontrollen WHERE LIK_LKK_KEY = :var_N0_lik_lkk_key AND LIK_MENGEIST<>LIK_MENGESOLL ';
		$rsLIKDiff=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);
		$rsLIKDiffZeilen = $awisRSZeilen;
		$AnzahlDifferenzen=$rsLIKDiff['ANZAHLDIFF'][0];
		
		//$AnzahlDifferenzen= $rsLIK['ANZAHL_DIFFERENZEN'][$LIKZeile];
		//$AnzahlDifferenzen=0;
		
		$LKKStatus='';
		$StatusText = explode("|",$TXT_Baustein['LIK']['lst_Lieferkontrollenstatus']);			
		foreach ($StatusText as $Status)
		{
			$LKStatus = explode("~",$Status);								
			if ($LKStatus[0]==$rsLIK['LKK_STATUS'][$LIKZeile])
			{
				$LKKStatus = $LKStatus[1];					
			}				
		}		
		awis_FORM_Erstelle_ListenFeld('Status',$LKKStatus,20,200,false,($LIKZeile%2),'','','T');
		
		//if (($RechteStufe & 32)==32 AND $Status>=10 AND $AnzahlDifferenzen>0)
		if (($RechteStufe & 32)==32 AND ($rsLIK['LKK_STATUS'][$LIKZeile]=='A' or $rsLIK['LKK_STATUS'][$LIKZeile] == 'Z') AND $AnzahlDifferenzen>0)
		{
			$PDF_Link='./filialinfo_Infos_lieferkontrolle_pdf.php?LKK_LIEFERSCHEINNR='.$rsLIK['LKK_LIEFERSCHEINNR'][$LIKZeile].'&LKK_FIL_ID='.$FIL_ID; 
			echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf.png title='".$TXT_Baustein['Wort']['PDFErzeugen']."'></a>";
		}
		
		awis_FORM_ZeileEnde();
	} 
}
else 						// Details anzeigen
{		
	if($awisRSZeilen==0)
	{
		$BindeVariablen=array();
		$BindeVariablen['var_N0_fil_id=']=intval($FIL_ID);
		$BindeVariablen['var_N0_lkk_lieferscheinnr=']=intval($LSNR);
		
		$SQL = 'SELECT LKK_LieferscheinNr, LKK_DatumKomm, \'########\' AS LIK_AST_ATUNR, LIK_QUELLE';		
		$SQL .=' FROM Lieferkontrollenkopf LKK';
		$SQL .=' INNER JOIN Lieferkontrollen LIK ON LIK_LKK_KEY = LKK_KEY';
		$SQL .=' WHERE LKK_FIL_ID=:var_N0_fil_id';
		$SQL .= ' AND LKK_LieferscheinNr=:var_N0_lkk_lieferscheinnr';		
		$SQL .=' ORDER BY LIK_QUELLE ';
		$rsLIK=awisOpenRecordset($con,$SQL,true,false,$BindeVariablen);	
	}
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['LIK_LIEFERSCHEINNR'].':',250);
	awis_FORM_Erstelle_TextFeld('LIK_LIEFERSCHEINNR',$rsLIK['LKK_LIEFERSCHEINNR'][0],0,100,false);
	echo '<input type=hidden name=txtLIK_LIEFERSCHEINNR value=0'.$rsLIK['LKK_LIEFERSCHEINNR'][0].'>';
	$PDF_Link='./filialinfo_Infos_lieferkontrolle_ls_pdf.php?LKK_LIEFERSCHEINNR='.$rsLIK['LKK_LIEFERSCHEINNR'][0].'&LKK_FIL_ID='.$FIL_ID; 	
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo 'Kompletter Lieferschein: ';
	echo "<a href=".$PDF_Link."><img border=0 src=/bilder/icon_pdf.png title='".$TXT_Baustein['Wort']['PDFErzeugen']."'></a>";
	
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['LIK_DATUMKOMM'].':',250);
	awis_FORM_Erstelle_TextFeld('LIK_DATUMKOMM',$rsLIK['LKK_DATUMKOMM'][0],0,100,false,'','','','D');
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['LIK']['LIK_QUELLE'].':',250);
	awis_FORM_Erstelle_TextFeld('LIK_QUELLE',$rsLIK['LIK_QUELLE'][0],0,100,false);
	awis_FORM_ZeileEnde();
	
	//awis_FORM_ZeileStart();
	//awis_FORM_Erstelle_TextLabel($TXT_Baustein['Wort']['Uploaddatei'].':',150);
	//awis_FORM_Erstelle_DateiUpload('dat_Dokument',60,80, 8000000,'');
	//awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['Wort']['AlleAnzeigen'].':',250);
	awis_FORM_Erstelle_Checkbox('AlleAnzeigen','',50,true,'on','');
	awis_FORM_ZeileEnde();

	$PDF_Link='./filialinfo_Infos_lieferkontrolle_pdf.php?LKK_LIEFERSCHEINNR='.$rsLIK['LKK_LIEFERSCHEINNR'][0].'&LKK_FIL_ID='.$FIL_ID; 
	echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='".$TXT_Baustein['Wort']['PDFErzeugen']."'></a>";
	
	if(($RechteStufe & 2)==2)
	{
		echo "&nbsp;<input name=cmdAbschliessen type=image border=0 src=/bilder/bestaetigen.png title='".$TXT_Baustein['LIK']['LieferscheinAbschliessen']."'>";
	}
	
	echo "&nbsp;<input type=image border=0 src=/bilder/eingabe_ok.png title='".$TXT_Baustein['Wort']['lbl_weiter']."'>";
	
	//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
	if((($RechteStufe&2)!=0) AND $rsLIKZeilen > 0)
	{
		echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;";
		echo '<span class=Hinweistext>'.$TXT_Baustein['Wort']['HinweisSpeichern'].'</span>';
	}

	awis_FORM_Trennzeile();

	if($rsLIK['LIK_AST_ATUNR'][0]!=='########')
	{
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift('',100);	
		awis_FORM_Erstelle_Liste_Ueberschrift('',120);	
		awis_FORM_Erstelle_Liste_Ueberschrift('',100);	
		awis_FORM_Erstelle_Liste_Ueberschrift('',100);	
//		awis_FORM_Erstelle_Liste_Ueberschrift('',100);	
//		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_KORREKTURGRUND'],180);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Gesamtbestand'],150);	
		awis_FORM_Erstelle_Liste_Ueberschrift('',50);	
		if(($RechteStufe & 8)==8)		
		{
			awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Urspruengliche'],130);	
		}
		
		awis_FORM_ZeileEnde();
		
		
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_AST_ATUNR'],100);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_DATUMSCAN'],120);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_MENGESOLL'],100);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_MENGEIST'],100);	
//		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_MENGEKORREKTUR'],100);	
//		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_KORREKTURGRUND'],180);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_SOLLBESTAND'],100);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_ISTBESTAND'],100);	
		if(($RechteStufe & 8)==8)		
		{
			awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_MENGEIST'],130);	
		}
		
		awis_FORM_ZeileEnde();
		
		
		
		for($LIKZeile=0;$LIKZeile<$rsLIKZeilen;$LIKZeile++)
		{
			//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
			//if((($RechteStufe&2)!=0) AND $rsLIK['LIK_STATUS'][$LIKZeile]>=10 AND (($RechteStufe&4)!=4))
			if(((($RechteStufe&2)==2) AND $rsLIK['LIK_STATUS'][$LIKZeile]<10) OR (($RechteStufe&4)==4))
			{
				$EditModus=true;
				//$EditModus=false;
			}
			else
			{
				$EditModus=false;
				//$EditModus=true;
			}
		
			awis_FORM_ZeileStart();
			$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsLIK['LIK_AST_ATUNR'][$LIKZeile].'&cmdAktion=ArtikelInfos';
			awis_FORM_Erstelle_ListenFeld('LIK_AST_ATUNR',$rsLIK['LIK_AST_ATUNR'][$LIKZeile],10,100,false,($LIKZeile%2),'',$Link,'T','L');
			awis_FORM_Erstelle_ListenFeld('LIK_DATUMSCAN',$rsLIK['LIK_DATUMSCAN'][$LIKZeile],10,120,false,($LIKZeile%2),'','','D','L');
			awis_FORM_Erstelle_ListenFeld('LIK_MENGESOLL',$rsLIK['LIK_MENGESOLL'][$LIKZeile],10,100,false,($LIKZeile%2),'','','N','L');
//			awis_FORM_Erstelle_ListenFeld('LIK_MENGEIST',$rsLIK['LIK_MENGEIST'][$LIKZeile],10,100,false,($LIKZeile%2),'','','N','L');
			awis_FORM_Erstelle_ListenFeld('LIK_MENGEIST_'.$rsLIK['LIK_KEY'][$LIKZeile].'_'.$rsLIK['LIK_LKK_KEY'][$LIKZeile],$rsLIK['LIK_MENGEIST'][$LIKZeile],10,100,$EditModus,($LIKZeile%2),'','','Z','L');
//			awis_FORM_Erstelle_ListenFeld('LIK_MENGEKORREKTUR_'.$rsLIK['LIK_KEY'][$LIKZeile],$rsLIK['LIK_MENGEKORREKTUR'][$LIKZeile],10,100,$EditModus,($LIKZeile%2),'','','Z','L');
//			awis_FORM_Erstelle_ListenFeld('LIK_KORREKTURGRUND_'.$rsLIK['LIK_KEY'][$LIKZeile],$rsLIK['LIK_KORREKTURGRUND'][$LIKZeile],20,180,$EditModus,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_SOLLBESTAND_'.$rsLIK['LIK_KEY'][$LIKZeile],$rsLIK['LIK_SOLLBESTAND'][$LIKZeile],10,100,$EditModus,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_ISTBESTAND_'.$rsLIK['LIK_KEY'][$LIKZeile],$rsLIK['LIK_ISTBESTAND'][$LIKZeile],10,100,$EditModus,($LIKZeile%2),'','','Z','L');
			if(($RechteStufe & 8)==8)			
			{
				awis_FORM_Erstelle_ListenFeld('LIK_MENGESCAN',$rsLIK['LIK_MENGESCAN'][$LIKZeile],10,130,false,($LIKZeile%2),'','','N','L');
			}
			
			awis_FORM_ZeileEnde();
		}
	}
	
}
awis_FORM_FormularEnde();

?>
