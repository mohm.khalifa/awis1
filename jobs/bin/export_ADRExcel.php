<?php
require_once 'awisDatenbank.inc';
require_once 'awisMailer.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{

    $AWISBenutzer = awisBenutzer::Init('GEBHARDT_P');
    $Form = new awisFormular();
   
    $DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Mail = new awisMailer($DB, $AWISBenutzer);
	
	//$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	
	//**********************************************************
	// Export der aktuellen Daten
	//**********************************************************
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 600);
	require_once('PHPExcel.php');

	$ExportFormat = 1;
	$Anzahl = 0;
	$SQLFiliale = 'Select FIL_ID from V_Filialen';
	$rsFIL = $DB->RecordSetOeffnen($SQLFiliale);
	while(!$rsFIL->EOF())
	{
	    
	    $PEIKey = $rsFIL->FeldInhalt('FIL_ID');
	    
	    $DateiName = 'Abfallensorgung_FIL_' . $PEIKey;
	    
	    
	    @ob_end_clean();
	    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	    header("Expires: 01 Jan 2000");
	    header('Pragma: public');
	    header('Cache-Control: max-age=0');
	    
	    switch ($ExportFormat)
	    {
	        case 1:                 // Excel 5.0
	            header('Content-Type: application/vnd.ms-excel');
	            header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
	            break;
	        case 2:                 // Excel 2007
	            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	            header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
	            break;
	    }
	    
	    $XLSXObj = new PHPExcel();
	    $XLSXObj->getProperties()->setCreator(utf8_encode('AWIS'));
	    $XLSXObj->getProperties()->setLastModifiedBy(utf8_encode('AWIS'));
	    $XLSXObj->getProperties()->setTitle(utf8_encode('Abfallentsorgung'));
	    $XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	    $XLSXObj->getProperties()->setDescription(utf8_encode('Abfallentsorgung'));
	    	    
	    $SQL  ='SELECT';
	    $SQL .='  ADS_EWCCODE as AVV,';
	    $SQL .='  ADS_EWCBEZ as Bezeichnung,  ';
	    $SQL .='  erz.fif_wert as Erzeugernummer,';
	    $SQL .='  \'ATU \' || ERZNAME.FIL_ORT as Erzeugername, ';
	    $SQL .='  ADP_GEWICHT AS Gewicht_in_KG, nvl(ADK_LAGERKZ,\'unbekannt\') as LAGERKZ,';
	    $SQL .='  to_char(trunc(ADK_DATUMABSCHLUSS + 1),\'DD.MM.YYYY\') as Annahme_am ';
	    $SQL .=' FROM';
	    $SQL .='  adrpos';
	    $SQL .=' INNER JOIN adrkopf';
	    $SQL .=' ON';
	    $SQL .='  adp_adk_key = adk_key';
	    $SQL .=' INNER JOIN adrstamm';
	    $SQL .=' ON';
	    $SQL .='  ADP_ADS_KEY = ads_key';
	    $SQL .=' INNER JOIN FILIALINFOS erz';
	    $SQL .=' on erz.FIF_FIT_ID = 911 and erz.FIF_FIL_ID = ADK_FIL_ID';
	    $SQL .=' INNER JOIN filialen erzname';
	    $SQL .=' on ERZNAME.FIL_ID = ADK_FIL_ID ';
	    $SQL .=' where ADK_DATUMABSCHLUSS >= sysdate - 7 and ADK_DATUMABSCHLUSS <= sysdate and ads_ewccode not like \'%130205%\' and ADK_FIL_ID = ' . $PEIKey;
	    $SQL .=' order by ADK_DATUMABSCHLUSS asc';
	    
	    
	    $rsFelder = $DB->RecordSetOeffnen($SQL);
	    
	    //Wenn kein DS kommt, dann unbekannt. 
	    $Lagerkennung = $rsFelder->FeldInhalt('LAGERKZ')!=''?$rsFelder->FeldInhalt('LAGERKZ'):'unbekannt';
        
	    $XLSXObj->setActiveSheetIndex(0);
	    $XLSXObj->getActiveSheet()->setTitle(utf8_encode('Abfallentsorgung'));
	    
	    $SpaltenNr = 0;
	    $ZeilenNr = 1;
	    
	    //�berschrift
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Filiale: ' . $PEIKey));
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y',time()));
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
	    
	    $ZeilenNr++;
	    $ZeilenNr++;
	    $SpaltenNr = 0;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AVV');
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Bezeichnung');
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Erzeugernummer');
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Erzeugername');
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Gewicht(KG)');
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	    $SpaltenNr++;
	    
	    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Annahme am');
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
	    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
	    $SpaltenNr++;
	    
	    $ZeilenNr++;
	    $SpaltenNr=0;
	    while(!$rsFelder->EOF())
	    {
	       
	        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsFelder->FeldInhalt('AVV'))),PHPExcel_Cell_DataType::TYPE_STRING);
	        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsFelder->FeldInhalt('BEZEICHNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
	        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsFelder->FeldInhalt('ERZEUGERNUMMER'))),PHPExcel_Cell_DataType::TYPE_STRING);
	        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	        	
	        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsFelder->FeldInhalt('ERZEUGERNAME'))),PHPExcel_Cell_DataType::TYPE_STRING);
	        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	        	
	        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsFelder->FeldInhalt('GEWICHT_IN_KG'))),PHPExcel_Cell_DataType::TYPE_STRING);
	        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsFelder->FeldInhalt('ANNAHME_AM'))),PHPExcel_Cell_DataType::TYPE_STRING);
	        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	    
	        $ZeilenNr++;
	        $SpaltenNr=0;
	        $rsFelder->DSWeiter();
	    }
	    
	    for($S='A';$S<='F';$S++)
	    {
	       $XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
	    }
	    
	    switch ($ExportFormat)
	    {
	        case 1:                 // Excel 5.0
	            header('Content-Type: application/vnd.ms-excel');
	            header('Content-Disposition: attachment; filename="Abfallentsorgung_FIL_' . $PEIKey . '_' . date('d.m.y') . '.xls"');
	            $DateiEndung = '.xls';
	            break;
	        case 2:                 // Excel 2007
	    
	            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	            header('Content-Disposition: attachment; filename="Abfallentsorgung_FIL_' . $PEIKey . '_' . date('d.m.y') . '.xlsx"');
	            $DateiEndung = '.xlsx';
	            break;
	    }
	    
  
	    // Verschiedene Formate erfordern andere Objekte
    	// Verschiedene Formate erfordern andere Objekte
    	switch ($ExportFormat)
    	{
    		case 1:                 // Excel 5.0
    			$objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
    			break;
    		case 2:                 // Excel 2007
    			$objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
    			break;
    	}
    	//var_dump($Lagerkennung);
    	$objWriter->save('/daten/daten/pccommon/Gebhardt/ADR/' . $Lagerkennung .'/' . $DateiName . '.xls');
    	$XLSXObj->disconnectWorksheets();
    	$Anzahl++;
    	$rsFIL->DSWeiter();
	}
	$Absender = 'awisstag@de.atu.eu';
	$Betreff = "OK ADR-Export-Excel " . date('ymd');
	$Text = $Anzahl . ' Daten exportiert  >>/var/log/awis/adrexport.log';
	$Empfaenger = 'shuttle@de.atu.eu';
	$Mail->AnhaengeLoeschen();
	$Mail->LoescheAdressListe();
	$Mail->DebugLevel(0);
	$Mail->AdressListe(awisMailer::TYP_TO, $Empfaenger, false, false);
	$Mail->Absender($Absender);
	$Mail->Betreff($Betreff);
	$Mail->Text($Text, awisMailer::FORMAT_HTML, true); //Text in HTML Format setzen
	$Mail->SetzeBezug('TES',0); // QMP KEY in Email setzen um Bezug zum Massnahmenplan herstellen zu k�nnen.
	$Mail->SetzeVersandPrioritaet(10); // Versandpriorit�t in der Warteschlange nach Systememails setzen
	$Mail->MailSenden();
}
catch(exception $ex)
{
	echo $ex->getMessage();
}




?>