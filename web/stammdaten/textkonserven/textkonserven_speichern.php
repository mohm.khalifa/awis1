<?php
global $XTX;
global $AWIS_KEY1;
try {
    foreach($_POST as $Param => $val){
        if(substr($Param,0,3)=='txt' and strpos($Param,'_Y_') !== false){

            $SQL = 'update TEXTKONSERVEN set ';

            $Feld = $Param;
            //txt Pr�fix wegschneiden
            $Feld = substr($Feld,3);

            //_Y_ ist der Feldtrenner: "XTX_BEREICH"_Y_"XTX_KENNUNG"_Y_"Sprache"
            $Feld = explode('_Y_',$Feld);
            $XTX_BEREICH = $Feld[0];
            $XTX_KENNNUNG = $Feld[1];
            $Sprache = $Feld[2];

            $WertNeu = $val;
            $WertAlt = $_POST['old' . substr($Param,3)];

            //Nichts ge�ndert?
            if($WertAlt==$WertNeu){
                continue;
            }

            $SQL .= '  XTX_TEXT_' .$Sprache . ' = ' . $XTX->DB->WertSetzen('XTX','T',$WertNeu);
            $SQL .= ' ,XTX_USERDAT = sysdate';

            $SQL .= ' WHERE XTX_KENNUNG = ' .$XTX->DB->WertSetzen('XTX','T',$XTX_KENNNUNG);
            $SQL .= ' AND XTX_BEREICH = ' . $XTX->DB->WertSetzen('XTX','T',$XTX_BEREICH);

            $XTX->DB->Ausfuehren($SQL,'',false,$XTX->DB->Bindevariablen('XTX'));
        }
    }

    $Meldung = $XTX->AWISSprachKonserven['XTX']['XTX_UPDATE_OK'];
    $XTX->Form->Hinweistext($Meldung,awisFormular::HINWEISTEXT_OK);


} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $XTX->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $XTX->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $XTX->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>