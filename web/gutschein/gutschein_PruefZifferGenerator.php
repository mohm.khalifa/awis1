<?php
global $AWIS_KEY1;
require_once 'awisGiftcard.inc';


try {

    $TextKonserven = array();
    $TextKonserven[]=array('GSD','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_export');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','Status');
    $TextKonserven[]=array('Wort','Dateiname');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht28000=$AWISBenutzer->HatDasRecht(28000);

    if($Recht28000==0)
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
        die();
    }

    if (isset($_FILES['Importdatei']))
    {
        //Fehlerausgabe
        if ($Erg ===true)
        {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['GSD']['GSD_IMPORT_OK'],awisFormular::HINWEISTEXT_OK);
            $Form->ZeileEnde();
        }
        else
        {
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['GSD']['GSD_IMPORT_ERR'],awisFormular::HINWEISTEXT_FEHLER);
            $Form->ZeileEnde();
        }
    }

    $Form->Formular_Start();
    echo '<form name=frmSuche action=./gutschein_csv.php?PZGENERATOR=true method=POST  enctype="multipart/form-data">';

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Import:',170,'font-weight: bold; font-size:15px');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'] . ':', 150);
    $Form->Erstelle_DateiUpload('Importdatei', 600, 30,2000000,'','.csv,.xls,.xlsx,.txt');
    $Form->ZeileEnde();

    $Form->Formular_Ende();
    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode('</form>');
}
catch (awisException $e)
{
    $Text='';
    $Text= 'Gutscheindruck Zentrale: Fehler: ' . $e->getMessage() . PHP_EOL;
    $Text.=PHP_EOL.'FEHLER:' . $e->getMessage() . PHP_EOL;
    $Text.= 'CODE:  ' . $e->getCode() . PHP_EOL;
    $Text.= 'Zeile: ' . $e->getLine() . PHP_EOL;
    $Text.= 'Datei: ' . $e->getFile() . PHP_EOL;
    $Text.= $this->_DB->LetzterSQL().PHP_EOL;

    $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - PruefZifferGenerator', $Text, 2, '', 'shuttle@de.atu.eu');

    die();
}

?>