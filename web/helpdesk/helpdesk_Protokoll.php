<?php
global $awisRSZeilen;
global $awisDBFehler;


$RechteStufe = awisBenutzerRecht($con, 1900);

if(($RechteStufe & 32)==0)
{
    awisEreignis(3,1900,'Helpdesk-Protokoll',$_SERVER['PHP_AUTH_USER'],'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}

$MaxZeilen = (isset($_POST['txtAnzEintraege'])?$_POST['txtAnzEintraege']:100);

echo '<form name=frmProtokoll action=./helpdesk_Main.php?cmdAktion=Protokoll method=post>';
echo '<br><table border=0 width=300>';
echo '<tr><td>Anzahl Eintr�ge:</td><td><input type=text size=6 name=txtAnzEintraege value='.$MaxZeilen.'></td></tr>';
echo '</table>';
echo '</form>';

echo '<hr>';


$SQL = 'SELECT HDP.*, XBN_Name, XBB_Bez FROM ';
$SQL .= ' (SELECT * FROM HelpDeskProtokoll ORDER BY HDP_USERDAT DESC) HDP';
$SQL .= ' LEFT OUTER JOIN Benutzer ON HDP_XXX_KEY = XBN_KEY';
$SQL .= ' LEFT OUTER JOIN BenutzerGruppenBez ON HDP_XXX_KEY = XBB_KEY';
$SQL .= ' WHERE ROWNUM <= ' . $MaxZeilen ;

$rsHDP = awisOpenRecordset($con,$SQL);
$rsHDPZeilen = $awisRSZeilen;

echo '<table border=1 width=100%>';

echo '<tr><td id=FeldBez>Benutzer/Gruppe</td>';
echo '<td id=FeldBez>�nderung</td>';
echo '<td id=FeldBez>Bemerkung</td>';
echo '<td id=FeldBez>Helpdesk-Mitarbeiter</td>';
echo '<td id=FeldBez>Ge�ndert am</td>';
echo '</tr>';

for($Zeile=0;$Zeile<$rsHDPZeilen;$Zeile++)
{

	echo '<tr>';

	echo '<td>' . $rsHDP['XBN_NAME'][$Zeile] . $rsHDP['XBB_BEZ'][$Zeile] . '</td>';
	echo '<td>' . $rsHDP['HDP_AENDERUNG'][$Zeile] . '</td>';
	echo '<td>' . $rsHDP['HDP_BEMERKUNG'][$Zeile] . '</td>';
	echo '<td>' . $rsHDP['HDP_USER'][$Zeile] . '</td>';
	echo '<td>' . $rsHDP['HDP_USERDAT'][$Zeile] . '</td>';

	echo '</tr>';

}


echo '</table>';



?>
<script type="text/javascript" language="JSCRIPT">
document.getElementsByName('txtAnzEintraege')[0].focus();
</script>