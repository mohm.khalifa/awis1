<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

$arrFeldlaenge = array(5,24,12,8,8,8,23,3,3,27,27,27,27,35,23,23,27,27,12,24);  //entspricht den Feldl�ngen in der Importdatei
$arrFeldtyp = array(1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,0,0,0,0);					// 0 = Text oder Datum, 1 = Zahl
$arrFeldnamen = array("IKA_AUSZUGNR","IKA_KONTONR","IKA_BANK","IKA_AUSZUGSDATUM",
"IKA_BUCHUNGSDATUM","IKA_VALUTADATUM","IKA_BETRAG","IKA_WAEHRUNG","IKA_TEXTSCHLUESSEL",
"IKA_BUCHUNGSTEXT","IKA_VERWENDUNGSZWECK1","IKA_VERWENDUNGSZWECK2","IKA_VERWENDUNGSZWECK3",
"IKA_KUNDENREFERENZ","IKA_STARTSALDO","IKA_SCHLUSSALDO","IKA_AUFTRAGGEBER1","IKA_AUFTRAGGEBER2",
"IKA_AUFTRAGGEBER_BLZ","IKA_AUFTRAGGEBER_KONTO");

$strDatei=$_SERVER["PFADLOKALKONTOAUSZUG"]."/".$_SERVER["LOKALDATEI"];

$intZeilenzaehler = 0;

if(is_file($strDatei)&&is_readable($strDatei))	//Wenn Datei vorhanden und lesbar ist
{
	$intSchalterDoppelt = 0;
	$handlerLesen=fopen($strDatei,"r");	//Datei zum Lesen �ffnen

	while(($strZeile=fgets($handlerLesen,10000))&&!$intSchalterDoppelt)	//Zeilenweise lesen
	{
		$intZeilenzaehler++;	//Z�hlt die Zeilen
		$intPoszaehler = 0;		//Position innerhalb der Zeile
		$strSqlPruef = "select count(*) anzahl from kontoauszuege where ";		//Anfang SQL-Statement Pruefung ob DS schon vorhanden
		$strSqlAnf = "insert into kontoauszuege(ika_id,"; 		//Anfang SQL-Statement
		$strSqlEnde = " values(seq_ika_id.nextval,";			//Ende SQL-Statement
		
		for($i=0;$i<count($arrFeldlaenge);$i++)
		{
			$divWert = trim(substr($strZeile,$intPoszaehler,$arrFeldlaenge[$i]));
			$intNull = empty($divWert);		//Testet ob Nullwert vorliegt
					
			if($arrFeldtyp[$i])	//Zahl
			{
				$divWert = str_replace(",",".",$divWert);
			}
			else	//Text oder Datum
			{
				$divWert = "'".str_replace("'","''",$divWert)."'";
			}
			
			if($intNull)	//Aufbau von $strSqlPruef gem�ss Variable $intNull
			{
				$strSqlPruef .= $arrFeldnamen[$i]." is null and ";
			}
			else
			{
				$strSqlPruef .= $arrFeldnamen[$i]."=".$divWert." and ";
			}
											
			$strSqlAnf .= $arrFeldnamen[$i].",";
			$strSqlEnde .= $divWert.",";
			
			$intPoszaehler += $arrFeldlaenge[$i]+1;		//Postitionszaehler hochsetzen
		}
		
		$strSqlPruef = substr($strSqlPruef,0,strlen($strSqlPruef)-4);	//letztes and wegschneiden
		
		$arrPruef = abfrage("ora",$strSqlPruef);
		
		if($arrPruef["trefferarray"]["ANZAHL"][0]>0)	//Wenn doppelter Datensatz vorhanden ist
		{
			$intSchalterDoppelt=1;
		}
		else
		{
			$strSqlAnf .= "IKA_AEND_BENUTZER,IKA_AEND_DATUM)";
			//$strSqlAnf = substr($strSqlAnf,0,strlen($strSqlAnf)-1).")";	//letztes Komma weg und Klammer setzen
			$strSqlEnde .= "'".$PHP_AUTH_USER."',sysdate)";
			//$strSqlEnde = substr($strSqlEnde,0,strlen($strSqlEnde)-1).")";	//letztes Komma weg und Klammer setzen
			
			$strSql = $strSqlAnf.$strSqlEnde;
							
			$arrRueckgabe = abfrage("ora",$strSql,1);		//1 = Es wird vorerst kein Commit gesetzt
		}
	}	//Ende while-Schleife

	if($intSchalterDoppelt)		//Wenn bereits identische Datens�tze importiert worden sind
	{
		@ocirollback($hdlOraVerbindung);
		$strAusgabe="Die Datei \"".$strDatei."\" enth�lt Datens�tze, welche bereits importiert worden sind! �berpr�fen Sie bitte die Datei!\n";
	}
	else
	{		
		$intRueck = @ocicommit($hdlOraVerbindung) or orafehler($hdlOraAnweisung,$strSql);  //Commit wird gesetzt, wenn alle Datens�tze eingelesen sind
	
		if($intRueck)
		{
			rename($strDatei,$strDatei."_ok");	//Importdatei umbenennen
			$strAusgabe= "OK: ".$intZeilenzaehler." Datens�tze erfolgreich importiert!\n";
		}
	}
	
	fclose($handlerLesen);		//Dateihandler schlie�en
}
else
{
	$strAusgabe="Stellen Sie bitte die Datei \"".$strDatei."\" bereit!\n";
}

echo $strAusgabe;
