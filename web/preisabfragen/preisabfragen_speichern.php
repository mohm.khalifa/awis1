<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once 'awisFormular.inc';
require_once 'awisDatenbank.inc';

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	//***********************************************************************************
	//** Preisabfragen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPAF_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('PAF','%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtPAF_KEY'];

		$rsPAF = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragen WHERE PAF_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsPAF->EOF())
		{
			$Fehler = '';
			$Pflichtfelder = array('PAF_DATUM','PAF_BEZEICHNUNG');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PAF'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}


			$SQL = 'INSERT INTO Preisabfragen';
			$SQL .= '(PAF_BEZEICHNUNG,PAF_DATUM,PAF_BEMERKUNG,PAF_BEMERKUNGINTERN,PAF_STATUS,';
			$SQL .= 'PAF_USER,PAF_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtPAF_BEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtPAF_DATUM'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPAF_BEMERKUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPAF_BEMERKUNGINTERN'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPAF_STATUS'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_PAF_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$PAF_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$PAF_KEY;
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';

			$rsPAF = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragen WHERE PAF_key=' . $_POST['txtPAF_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPAF->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPAF->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPAF->FeldInfo($FeldName,'TypKZ'),$rsPAF->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPAF->FeldInhalt('PAF_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Preisabfragen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PAF_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PAF_userdat=sysdate';
				$SQL .= ' WHERE PAF_key=0' . $_POST['txtPAF_KEY'] . '';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',1004201723,$SQL,2);
				}
			}
		}
	}// Preisabfragen


	
	//***********************************************************************************
	//** Preisabfragenfahrzeuginfos
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPFI_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('PFI','%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtPFI_KEY'];

		$rsPFI = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragenfahrzeuginfos WHERE PFI_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsPFI->EOF())
		{
			$Fehler = '';
			$Pflichtfelder = array('PFI_KTYPNR','PFI_BEZEICHNUNG');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PFI'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}


			$SQL = 'INSERT INTO Preisabfragenfahrzeuginfos';
			$SQL .= '(PFI_BEZEICHNUNG,PFI_KTYPNR,PFI_BEMERKUNG,PFI_BAUJAHR,PFI_KMSTAND,';
			$SQL .= 'PFI_USER,PFI_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtPFI_BEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPFI_KTYPNR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPFI_BEMERKUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPFI_BAUJAHR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPFI_KMSTAND'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 201105311420);
			}
			$SQL = 'SELECT seq_PFI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$PFI_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$PFI_KEY;
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';

			$rsPFI = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragenfahrzeuginfos WHERE PFI_key=' . $_POST['txtPFI_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPFI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPFI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPFI->FeldInfo($FeldName,'TypKZ'),$rsPFI->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPFI->FeldInhalt('PFI_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Preisabfragenfahrzeuginfos SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PFI_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PFI_userdat=sysdate';
				$SQL .= ' WHERE PFI_key=0' . $_POST['txtPFI_KEY'] . '';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',1004201723,$SQL,2);
				}
			}
		}
	}// Preisabfragenfahrzeuginfos


	//***********************************************************************************
	//** Preisabfragengruppen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPFG_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('PFG','%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtPFG_KEY'];

		$rsPFG = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragengruppen WHERE PFG_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsPFG->EOF())
		{
			$Fehler = '';
			$Pflichtfelder = array('PFG_PFI_KEY','PFG_BEZEICHNUNG');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PFG'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}


			$SQL = 'INSERT INTO Preisabfragengruppen';
			$SQL .= '(PFG_BEZEICHNUNG,PFG_PFI_KEY,PFG_BEMERKUNG,';
			$SQL .= 'PFG_USER,PFG_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtPFG_BEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPFG_PFI_KEY'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPFG_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 201105311420);
			}
			$SQL = 'SELECT seq_PFG_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$PFG_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$PFG_KEY;
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';

			$rsPFG = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragengruppen WHERE PFG_key=' . $_POST['txtPFG_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPFG->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPFG->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPFG->FeldInfo($FeldName,'TypKZ'),$rsPFG->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPFG->FeldInhalt('PFG_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Preisabfragengruppen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PFG_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PFG_userdat=sysdate';
				$SQL .= ' WHERE PFG_key=0' . $_POST['txtPFG_KEY'] . '';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',1004201723,$SQL,2);
				}
			}
		}
	}// Preisabfragengruppen


	//***********************************************************************************
	//** Preisabfragenartikelpakete
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPAP_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('PAP','%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtPAP_KEY'];

		$rsPAP = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragenartikelpakete WHERE PAP_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsPAP->EOF())
		{
			$Fehler = '';
			$Pflichtfelder = array('PAP_BEZEICHNUNG');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PAP'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}


			$SQL = 'INSERT INTO Preisabfragenartikelpakete';
			$SQL .= '(PAP_BEZEICHNUNG,PAP_BEMERKUNG,';
			$SQL .= 'PAP_USER,PAP_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtPAP_BEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPAP_BEMERKUNG'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 201105311420);
			}
			$SQL = 'SELECT seq_PAP_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$PAP_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$PAP_KEY;
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';

			$rsPAP = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragenartikelpakete WHERE PAP_key=' . $_POST['txtPAP_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPAP->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPAP->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPAP->FeldInfo($FeldName,'TypKZ'),$rsPAP->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPAP->FeldInhalt('PAP_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Preisabfragenartikelpakete SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PAP_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PAP_userdat=sysdate';
				$SQL .= ' WHERE PAP_key=0' . $_POST['txtPAP_KEY'] . '';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',1004201723,$SQL,2);
				}
			}
		}
	}// Preisabfragenartikelpakete



	//***********************************************************************************
	//** Preisabfragendatenartikel
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPAA_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('PAA','%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY2 = $_POST['txtPAA_KEY'];

		$rsPAA = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragendatenartikel WHERE PAA_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY2));

		if($rsPAA->EOF())
		{
			$Fehler = '';
			$Pflichtfelder = array('PAA_AST_ATUNR','PAA_GRUPPE','PAA_HAUPTGRUPPE','PAA_PFG_KEY');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['PAA'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}

			$SQL = 'INSERT INTO Preisabfragendatenartikel';
			$SQL .= '(PAA_PAP_KEY,PAA_AST_ATUNR,PAA_GRUPPE,PAA_HAUPTGRUPPE,PAA_SORTIERUNG,PAA_DLNR,PAA_KOMPLETTNR,PAA_PFG_KEY';
			$SQL .= ',PAA_USER,PAA_USERDAT';
			$SQL .= ')VALUES('.$AWIS_KEY1;
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPAA_AST_ATUNR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPAA_GRUPPE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPAA_HAUPTGRUPPE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPAA_SORTIERUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPAA_DLNR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtPAA_KOMPLETTNR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtPAA_PFG_KEY'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 201105311420);
			}
			$SQL = 'SELECT seq_PAA_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$PAA_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY2=$PAA_KEY;
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';

			$rsPAA = $DB->RecordSetOeffnen('SELECT * FROM Preisabfragendatenartikel WHERE PAA_key=' . $_POST['txtPAA_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsPAA->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsPAA->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsPAA->FeldInfo($FeldName,'TypKZ'),$rsPAA->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsPAA->FeldInhalt('PAA_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Preisabfragendatenartikel SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', PAA_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', PAA_userdat=sysdate';
				$SQL .= ' WHERE PAA_key=0' . $_POST['txtPAA_KEY'] . '';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',1004201723,$SQL,2);
				}
			}
		}
		$AWIS_KEY2='';		// Nach dem Speichern in die Liste
		$AWIS_KEY1=$_POST['txtPAP_KEY'];	
		
	}// Preisabfragendatenartikel


	
	//**********************************************************
	// Datenimport f�r die Preisabfragen
	//**********************************************************
	if(!empty($_FILES))
	{
		if(array_search($_FILES['PREISDATEI']['type'],array('application/csv','application/vnd.ms-excel','text/x-comma-separated-values'))!==false)
		{
			if(($fd = fopen($_FILES['PREISDATEI']['tmp_name'],'r'))!==false)
			{
				$Zeile = fgets($fd);
				$ZeilenDaten = explode("\t",$Zeile);
				if(trim($ZeilenDaten[0])=='##AWIS##' AND trim($ZeilenDaten[1])=='PREISIMPORT' AND trim($ZeilenDaten[2])=='V1')
				{
					$Zeile = fgets($fd);
					$FeldListe = explode("\t",trim($Zeile));
					$FeldListe = array_flip($FeldListe);

					if(isset($FeldListe['PFA_FIL_ID']) AND isset($FeldListe['PFA_AST_ATUNR'])AND isset($FeldListe['PFA_MBW_KEY']))
					{
						$DB->Ausfuehren('DELETE FROM PREISABFRAGENPREISE WHERE PFA_PAF_KEY = '.$AWIS_KEY1);

						$Zeile = fgets($fd);
						while($Zeile!=false)
						{
							$ZeilenDaten = explode("\t",trim($Zeile));
							$SQL = 'INSERT INTO PREISABFRAGENPREISE';
							$SQL .= '(PFA_FIL_ID,PFA_PAF_KEY,PFA_AST_ATUNR,PFA_ARTIKELBEZEICHNUNG';
							$SQL .= ',PFA_BESCHREIBUNG,PFA_MBW_KEY,PFA_DLNR, PFA_KOMPLETTNR, PFA_PFG_KEY';
							$SQL .= ',PFA_USER,PFA_USERDAT)';
							$SQL .= ' VALUES (';
							$SQL .= ' '.$DB->FeldInhaltFormat('N0',$ZeilenDaten[$FeldListe['PFA_FIL_ID']]);
							$SQL .= ', '.$AWIS_KEY1;
							$SQL .= ', '.$DB->FeldInhaltFormat('T',$ZeilenDaten[$FeldListe['PFA_AST_ATUNR']]);
							$SQL .= ', '.$DB->FeldInhaltFormat('T',$ZeilenDaten[$FeldListe['PFA_ARTIKELBEZEICHNUNG']]);
							$SQL .= ', '.$DB->FeldInhaltFormat('T',$ZeilenDaten[$FeldListe['PFA_BESCHREIBUNG']]);
							$SQL .= ', '.$DB->FeldInhaltFormat('N0',$ZeilenDaten[$FeldListe['PFA_MBW_KEY']]);
							$SQL .= ', '.$DB->FeldInhaltFormat('N0',(isset($ZeilenDaten[$FeldListe['PAA_DLNR']])?$ZeilenDaten[$FeldListe['PAA_DLNR']]:''));
							$SQL .= ', '.$DB->FeldInhaltFormat('N0',isset($ZeilenDaten[$FeldListe['PAA_KOMPLETTNR']])?$ZeilenDaten[$FeldListe['PAA_KOMPLETTNR']]:'');
							$SQL .= ', '.$DB->FeldInhaltFormat('N0',isset($ZeilenDaten[$FeldListe['PAA_GRUPPENID']])?$ZeilenDaten[$FeldListe['PAA_GRUPPENID']]:'');
							$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
							$SQL .= ', SYSDATE)';
							
							if($DB->Ausfuehren($SQL)===false)
							{
								throw new awisException('Fehler beim Speichern',1006161400,$SQL,2);
							}

							$Zeile = fgets($fd);
						}
					}
					else
					{
						$Form->ZeileStart();
						$Form->HinweisText('Falscher Dateiaufbau',awisFormular::HINWEISTEXT_HINWEIS);
						$Form->ZeileEnde();
					}



				}
				else
				{
					$Form->ZeileStart();
					$Form->HinweisText('Falsche Datei',awisFormular::HINWEISTEXT_HINWEIS);
					$Form->ZeileEnde();
				}
			}
			else
			{




			}
		}
		else
		{
			$Form->ZeileStart();
			$Form->Hinweistext(str_replace('$1','CSV',$TXT_Speichern['FEHLER']['err_UngueltigesUploadDateiFormat']),'Warnung','');
			$Form->ZeileEnde();
		}
	}

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>