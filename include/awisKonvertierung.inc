<?php 
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisMailer.inc';
/**
 * Konvertierung von Dateien von XML in CSV
 * @author kerres_s
 * @version 201303
 *
 */
class awisKonvertierung
{
	/**
	 * Datenbankobjekt
	 * @var awisDatenbank
	 */
	private $_DB = null;
	/**
	 * AWIS-Benutzer
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	/**
	 * Debuglevel f�r Ausgaben
	 * @var int
	 */
	private $_DebugLevel = 0;
	/**
	 * Mail-Objekt
	 * @var awisMailer
	 */
	private $_MailObj = null;
	/**
	 * Werkzeuge
	 * @var awisWerkzeuge
	 */
	private $_AWISWerkzeug = null;
	/**
	 * Fehler, wenn der User keinen Zugriff hat
	 * @var int
	 */
	const ERR_ZUGRIFF = 1;
	
	/**
	 * Formularobjekt
	 * @var awisFormular
	 */
	private $_Form = null;
	/**
	 * Initialisiert die Klasse
	 * @param string $Benutzer              Loginname zum Ausf�hren des Jobs
	 * @throws Exception
	 */
	
	public function __construct(awisBenutzer $AWISBenutzer, awisDatenbank $DB)
	{
		$this->_AWISBenutzer = $AWISBenutzer;
		$this->_DB = $DB;
		$this->_Form = new awisFormular();
		$this->_MailObj = new awisMailer($this->_DB, $this->_AWISBenutzer);
		$this->_AWISWerkzeug = new awisWerkzeuge();
	}
	
	/**
	 * Liest oder setzt den Debuglevel
	 * @param int [optional] $NeuerLevel            Neuer Debuglevel
	 * @return number
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = intval($NeuerLevel);
		}
	
		return $this->_DebugLevel;
	}

	/**
	 * Konvertierungsmodul f�r TRADEBYTE zu WAWI
	 */
	const KMODUL_TRADEBYTE_01 = 'TB01';
	
	/**
	 * Tradebyte Bestand
	 * @var unknown
	 */
	const KMODUL_TRADEBYTE_02 = 'TB02';
	
	/**
	 * Konvertiert alle TradeByte Dateien im Team Laufwerk
	 */
	public function KonvertiereTradeByte()
	{
		$QuellPfad = $this->_AWISBenutzer->ParameterLesen('XML-IN');			// '/daten/daten/teams/E-Commerce/08-Tradebyte/01-ATU/01-IN';
		$ZielPfad = $this->_AWISBenutzer->ParameterLesen('XML-OUT');			// '/daten/daten/teams/E-Commerce/08-Tradebyte/01-ATU/02-OUT';
		$ArchivPfad = $this->_AWISBenutzer->ParameterLesen('XML-ARCHIV');		// '/daten/daten/teams/E-Commerce/08-Tradebyte/01-ATU/03-Archiv';

		$DirObj = new DirectoryIterator($QuellPfad);
		foreach($DirObj as $Datei)
		{
			if(substr($Datei->getFilename(),0,9)=='1148_cat_')		// Artikelstamm
			{
				try 
				{
					// Beispiel: 1148_cat_20130322_040002.xml
					$DateiTeile = explode('_',substr($Datei->getFilename(),0,-4));
					$this->Konvertiere($Datei->getPathname(), $ZielPfad.'/'.'TB_ARTI_'.$DateiTeile[2].'_'.$DateiTeile[3].'.csv',awisKonvertierung::KMODUL_TRADEBYTE_01);
					copy($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.ok');
					unlink($Datei->getPathname());
					//rename($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.ok');
				}
				catch(Exception $ex)
				{
					copy($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.fehler');
					unlink($Datei->getPathname());
					//rename($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.fehler');
				}				
			}
			elseif(substr($Datei->getFilename(),0,6)=='stock_')		// Bestandsdatei
			{
				try
				{
					// Beispiel: stock_201300405_130005.xml
					$DateiTeile = explode('_',substr($Datei->getFilename(),0,-4));
					$this->Konvertiere($Datei->getPathname(), $ZielPfad.'/'.'TB_BEST_'.$DateiTeile[1].'_'.$DateiTeile[2].'.csv',awisKonvertierung::KMODUL_TRADEBYTE_02);
					copy($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.ok');
					unlink($Datei->getPathname());
					//rename($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.ok');
				}
				catch(Exception $ex)
				{
					copy($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.fehler');
					unlink($Datei->getPathname());
					//rename($Datei->getPathname(), $ArchivPfad.'/'.$Datei->getFilename().'.fehler');
				}
			}
		}
	}
	
	
	/**
	 * Konvertiert eine Datei mit einem Konvertermodul
	 * @param string $Dateiname
	 * @param string $KonverterModul
	 */
	public function Konvertiere($Quelldatei, $Zieldatei, $KonverterModul='')
	{
		if(!is_file($Quelldatei))
		{
			throw new Exception('Quelldatei '.$Quelldatei.' nicht gefunden');
		}

		try 
		{
			$MD5 = md5_file($Quelldatei);
			$SQL = 'SELECT XDI_Dateiname';
			$SQL .= ' FROM IMPORTPROTOKOLL';
			$SQL .= ' WHERE XDI_BEREICH = \'XML-'.$KonverterModul.'\'';
			$SQL .= ' AND XDI_MD5 = \''.$MD5.'\'';
			$rsXDI = $this->_DB->RecordsetOeffnen($SQL);
			if($rsXDI->EOF())			// Neue Datei
			{
				switch ($KonverterModul)
				{
					case self::KMODUL_TRADEBYTE_01 :
						$this->_KonvertiereTB01($Quelldatei, $Zieldatei);
						break;
					case self::KMODUL_TRADEBYTE_02 :
						$this->_KonvertiereTB02($Quelldatei, $Zieldatei);
						break;
					default: 
						break;
				}
				
				//************************************************
				//* Importdatei wegschreiben
				//************************************************
				$SQL = 'INSERT INTO IMPORTPROTOKOLL';
				$SQL .= '(XDI_BEREICH, XDI_DATEINAME, XDI_DATUM, XDI_MD5, XDI_USER, XDI_USERDAT)';
				$SQL .= 'VALUES (';
				$SQL .= ' \'XML-'.$KonverterModul.'\'';
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$Quelldatei);
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('D',date('d.m.Y',filectime($Quelldatei)));
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$MD5);
				$SQL .= ', '.$this->_DB->FeldInhaltFormat('T',$this->_AWISBenutzer->BenutzerName());
				$SQL .= ', SYSDATE)';
				
			//	$this->_DB->Ausfuehren($SQL,'',true);
			}
			else			// Datei wurde bereits konvertiert.  
			{
				if($this->_DebugLevel>0)
				{
					echo 'Datei '.$Quelldatei.' wurde bereits importiert. Wird ignoriert.';
				}
			}
		}
		catch(Exception $ex)
		{
			if($this->_DebugLevel>5)
			{
				echo 'Fachbereich benachrichtigen...'.PHP_EOL;
			}
			$this->_MailObj->AnhaengeLoeschen();
			$this->_MailObj->LoescheAdressListe();
			
			$this->_MailObj->Absender('awis@de.atu.eu');
			// TODO: Mailempf�mger f�r PROD anpassen
			
			$MailEmpfaenger = explode(';',$this->_AWISBenutzer->ParameterLesen('XML-MailEmpfaenger'));
			foreach($MailEmpfaenger AS $Empfaenger)
			{
				$this->_MailObj->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
			}
			$this->_MailObj->Betreff('Konvertierung XML-Datei - FEHLER');
			$this->_MailObj->Prioritaet(awisMailer::PRIO_HOCH);
			$Text = '<b>Problem bei der Konvertierung der nachfolgenden Datei:</b><br><br>';
			$Text .= 'Dateiname: <b>'.$Quelldatei.'</b><br>';
			$Text .= 'Fehler: <b>'.$ex->getMessage().'</b><br>';
			$Text .= 'Datei wurde in das Archivverzeichnis verschoben.<br><br>';
			$Text .= 'Ihr AWIS Team.<br>';
			$this->_MailObj->Text($Text,awisMailer::FORMAT_HTML,true);
			if($this->_MailObj->MailSenden()===false)
			{
				$this->_MailObj->MailInWarteschlange('awis');
			}
			
			return false;
		}			
		
		return true;
	}

	/**
	 * Konvertiere den Artikelstamm von TradeByte
	 * @param string $Dateiname
	 * @param string $Zieldatei
	 */
	private function _KonvertiereTB01($Quelldatei, $Zieldatei)
	{
		$fp = fopen($Zieldatei.'.tmp','w');
		if($fp===false)
		{
			throw new Exception('Zieldatei '.$Zieldatei.' kann nicht angelegt werden.');
		}

		if($this->_DebugLevel>10)
		{
			echo 'Konvertiere '.$Quelldatei.PHP_EOL;
		}
		try 
		{
			$XML = simplexml_load_file($Quelldatei);
			if($XML===false)
			{
				throw new Exception('XML kann nicht geparst werden.');
			}
			//echo $XML->asXML().PHP_EOL;
			
			if(!isset($XML->SUPPLIER) OR !isset($XML->CLASSIFICATION) OR !isset($XML->PRODUCTDATA))
			{
				throw new Exception('Die XML Datei hat den falschen Aufbau.');
			}
	
			/*
			$Attribute = $XML->attributes();
			if((string)$Attribute['channel'] !== 'cuaturoller')
			{
				throw new Exception('Die Datei hat den falschen Channel:'.(string)$XML->channel);
			}
			*/
			
			$WerkZeug = new awisWerkzeuge();
			
			//********************************************
			// Zun�chst alle Lieferanten lesen
			//********************************************
			
			$Lieferanten = array();
			foreach( $XML->CLASSIFICATION->SUB_SUPPLIERS->SUB_SUPPLIER AS $Lieferant)
			{
				$Lieferanten[(string)$Lieferant->ID]=(string)$Lieferant->KEY;
			}
			
			if($this->_DebugLevel>50)
			{
				echo 'Lieferanten-IDs:'.PHP_EOL;
				var_dump($Lieferanten);
			}
			
			if(empty($Lieferanten))
			{
				throw new Exception('Die XML Datei hat keine Lieferanteninformationen.');
			}
			
			$Trenner = ";";
			if(!isset($XML->PRODUCTDATA->PRODUCT))
			{
				// Wenn dieser Fall eintritt handelt es sich um eine leere Delta-Datei
				// In diesem Fall keine Datei erzeugen, nur das Quellfile verschieben
				
				fclose($fp);
				if(is_file($Zieldatei.'.tmp'))		// Temp Datei wieder l�schen, wenn keine Daten
				{
					unlink($Zieldatei.'.tmp');
				}
				return;
				
				// Kompletter Fehler, wenn nicht mal ein Produkt im XML ist
				//throw new Exception('Dateiaufbau bei PRODUCTDATA->PRODUCT pruefen.');
			}

			// Jetzt alle Artikel laden, die schon mal durch den Konverter gelaufen sind
			$Verzeichnis = dirname($Quelldatei);
			if(file_exists($Verzeichnis.'/artikelids.txt'))
			{
				$ArtikelIDs = unserialize(file_get_contents($Verzeichnis.'/artikelids.txt'));
			}
			else
			{
				$ArtikelIDs = array();
			}
			
			foreach($XML->PRODUCTDATA->PRODUCT AS $Produkt)
			{
				$ATUNR = '';
				
				if(!isset($Produkt->ARTICLEDATA->ARTICLE->USERDEFINED->VALUE))
				{
					if(isset($Produkt->P_NR))		// Gibt es eine Produktnummer?
					{
						$Attribute = $Produkt->attributes();
						if(isset($Attribute['mode']))
						{
							if((string)$Attribute['mode']=='delete')				// L�schanforderung
							{
								$ATUNR = '::unbekannt::';
								if(isset($ArtikelIDs[(string)$Produkt->P_NR]))		// Kenn ich die ATU Nummer?
								{
									$ATUNR = $ArtikelIDs[(string)$Produkt->P_NR];
								}
								
								$this->_MailObj->AnhaengeLoeschen();
								$this->_MailObj->LoescheAdressListe();
								$this->_MailObj->Absender('awis@de.atu.eu');
								$MailEmpfaenger = explode(';',$this->_AWISBenutzer->ParameterLesen('XML-MailEmpfaenger'));
								foreach($MailEmpfaenger AS $Empfaenger)
								{
									$this->_MailObj->AdressListe(awisMailer::TYP_TO,$Empfaenger,awisMailer::PRUEFE_NICHTS,awisMailer::PRUEFAKTION_KEINE);
								}
								$this->_MailObj->Betreff('Tradebyte Import - Hinweis');
								$this->_MailObj->Prioritaet(awisMailer::PRIO_HOCH);
								$Text = '<b>Bei der Konvertierung der nachfolgenden Datei wurde ein L&ouml;schauftrag gefunden:</b><br><br>';
								$Text .= 'Dateiname    : <b>'.$Quelldatei.'</b><br>';
								$Text .= 'Produkt-Id   : <b>'.(string)$Produkt->P_NR.'</b><br>';
								$Text .= 'ATU-Nummer : <b>'.$ATUNR.'</b><br><br>';
								$Text .= 'Bitte entsprechende Massnahmen einleiten.<br><br>';
								$Text .= 'Ihr AWIS Team.<br>';
								$this->_MailObj->Text($Text,awisMailer::FORMAT_HTML,true);
								if($this->_MailObj->MailSenden()===false)
								{
									$this->_MailObj->MailInWarteschlange('awis');
								}
								
								continue;
							}
						}	
					}
					
					// Es fehlt die ATU Nummer zum Artikel und es ist kein L�schauftrag => Abbruch
					throw new Exception('Dateiaufbau bei ARTICLEDATA->ARTICLE->USERDEFINED->VALUE pruefen.');
				}
				/*
				- <USERDEFINED>
				<VALUE key="atu_articlenumber" type="xsd:string">QA0001</VALUE>
				</USERDEFINED>
				*/
				foreach($Produkt->ARTICLEDATA->ARTICLE->USERDEFINED->VALUE AS $ZusatzfeldListe)
				{
					$Zusatzfelder = $ZusatzfeldListe->attributes();
					if((string)$Zusatzfelder['key']=='atu_articlenumber')
					{
						$ATUNR = trim((string)$ZusatzfeldListe[0]); 
					}
				}
				
				if($this->_DebugLevel>10)
				{
					echo '*** ATU-Nr: '.$ATUNR.PHP_EOL;
				}
				
				if(strlen($ATUNR)!=6)
				{
					throw new Exception('Die ATU Nummer '.$ATUNR.' hat keine 6 Zeichen.');
				}
				
				
				$ArtikelIDs[(string)$Produkt->P_NR] = $ATUNR;				
				
				//ARTNR
				$Zeile = $ATUNR;
				
				// EANNR
				// TODO; EAN pr�fen, formatieren
				//       wenn Fehler -> Datei verwerfen!
				if((string)$Produkt->ARTICLEDATA->ARTICLE->A_EAN!='')
				{
					if($WerkZeug->BerechnePruefziffer((string)$Produkt->ARTICLEDATA->ARTICLE->A_EAN,awisWerkzeuge::PRUEFZIIFER_TYP_EAN,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN)===false)
					{
						throw new Exception('EAN '.(string)$Produkt->ARTICLEDATA->ARTICLE->A_EAN.' ist ungueltig');
					}
				}
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_EAN;
				// WGSORT
				$Attribute = $Produkt->P_CATEGORIES->P_CATEGORY->attributes();
				$Zeile .= $Trenner.(string)$Attribute['key'];
				
				// ARTBEZL
				$Text = iconv('utf-8','ASCII//TRANSLIT',(string)$Produkt->P_NAME);
				$Text = str_replace(';', ' ', $Text);
				$Zeile .= $Trenner.$Text;
				// LFNR
				// TODO: richtigen Tag laden!!!
				
				$Attribute = $Produkt->P_SUB_SUPPLIER->attributes();
				if(!isset($Lieferanten[(string)$Attribute['key']]))
				{
					throw new Exception('Es konnte kein passender Lieferant fuer den Key '.(string)$Attribute['key'].'  ermittelt werden.');
				}
				$Zeile .= $Trenner.$Lieferanten[(string)$Attribute['key']];
				
				// LARTNR
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_NR;
				// EK1
				$Zeile .= $Trenner.$this->_Form->Format('N2',(string)$Produkt->ARTICLEDATA->ARTICLE->A_PRICEDATA->A_PRICE->A_EK);	// FEHLT
				// EKDAT1
				//$Zeile .= $Trenner.date('d.m.Y',(string)$Produkt->ARTICLEDATA->ARTICLE->A_CHANGEDATE);
				// BRUTTO
				$Zeile .= $Trenner.$this->_Form->Format('N2',(string)$Produkt->ARTICLEDATA->ARTICLE->A_PRICEDATA->A_PRICE->A_VK);
				// LZEIT
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_REPLACEMENT_TIME;
				// PPKGEW
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_ACTIVE;
				// GFKENN
				$Zeile .= $Trenner.'N';
				// Zolltarif Exportnr
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_TRADESTAT->A_TRADESTAT_NR;  //FEHLT
				// Roh-Gewicht
				$Zeile .= $Trenner.$this->_Form->Format('N2',(string)$Produkt->ARTICLEDATA->ARTICLE->A_PARCEL->A_WEIGHT);
				// AKTIV1
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_ACTIVE;
				// AKTIV2				
				$Zeile .= $Trenner.'0';
				// AKTIV3				
				$Zeile .= $Trenner.'0';
				// AKTIV4				
				$Zeile .= $Trenner.'0';
				// AKTIV5				
				$Zeile .= $Trenner.'0';
				// AKTIV6				
				$Zeile .= $Trenner.'0';
				// AKTIV7				
				$Zeile .= $Trenner.'0';
				// MWST				
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_PRICEDATA->A_PRICE->A_MWST;
				// MWST_OES
				$Zeile .= $Trenner.'2';
				// MWST_CZE
				$Zeile .= $Trenner.'2';
				// MWST_NL
				$Zeile .= $Trenner.'2';
				// MWST_ITA
				$Zeile .= $Trenner.'2';
				// MWST_SUI
				$Zeile .= $Trenner.'2';
				// MWST_7
				$Zeile .= $Trenner.'2';
				// LAENGE
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_PARCEL->A_LENGTH;
				// BREITE
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_PARCEL->A_WIDTH;
				// HOEHE
				$Zeile .= $Trenner.(string)$Produkt->ARTICLEDATA->ARTICLE->A_PARCEL->A_HEIGHT;
				// VERPACKUNG
				$Zeile .= $Trenner.'N';
				
				// PPKGEW
				$Zeile .= $Trenner.'';
				// FOLIEGEW
				$Zeile .= $Trenner.'';
				// KUNSTSTOFFGEW
				$Zeile .= $Trenner.'';
				// ALUMINIUMGEW
				$Zeile .= $Trenner.'';
				// STYROPORGEW
				$Zeile .= $Trenner.'';
				// KZ_ELEKTROGES
				$Zeile .= $Trenner.'';

				// SCHWARZBLECHGEW
				$Zeile .= $Trenner.'';
				// WEISSBLECHGEW
				$Zeile .= $Trenner.'';
				// VP_LIZENZ
				$Zeile .= $Trenner.'N';
				
				fputs($fp, $Zeile."\r\n");
			}
			echo PHP_EOL;
			fclose($fp);
			
			rename($Zieldatei.'.tmp', $Zieldatei);
			
	
			// Jetzt die Artikel-Ids wegschreiben
			if(!empty($ArtikelIDs))
			{
				file_put_contents($Verzeichnis.'/artikelids.txt',serialize($ArtikelIDs));
			}
		}
		catch(Exception $ex)
		{
			if(is_file($Zieldatei.'.tmp'))		// Temp Datei l�schen im Fehlerfall
			{
				unlink($Zieldatei.'.tmp');
			}
			throw new Exception($ex->getMessage());
		}	
	}
	/**
	 * Konvertiere den Bestandsdaten von TradeByte
	 * @param string $Dateiname
	 * @param string $Zieldatei
	 */
	private function _KonvertiereTB02($Quelldatei, $Zieldatei)
	{
		$fp = fopen($Zieldatei.'.tmp','w');
		if($fp===false)
		{
			throw new Exception('Zieldatei '.$Zieldatei.' kann nicht angelegt werden.');
		}

		try 
		{
			if($this->_DebugLevel>40)
			{
				echo '  Konvertiere Datei '.$Quelldatei.PHP_EOL;
			}
			
			$XML = simplexml_load_file($Quelldatei);
			if($XML===false)
			{
				throw new Exception('XML kann nicht geparst werden.');
			}
			echo $XML->asXML().PHP_EOL;

			/*
			- <stock-update>
			- <item>
			  <sku>SP64098</sku> 
			  <quantity>99</quantity> 
			  <channel-sku>Q00001</channel-sku> 
			  </item>
			- <item>
			  <sku>SP64099</sku> 
			  <quantity>1</quantity> 
			  <channel-sku>Q00002</channel-sku> 
			  </item>
			  </stock-update>
			  */			
			
			if(!isset($XML->item))
			{
				throw new Exception('Die XML Datei hat den falschen Aufbau.');
			}
	
			$Trenner = ";";
			foreach($XML->item AS $Produkt)
			{
				foreach($Produkt->children() AS $Feld)
				{
					if($Feld->getName()=='channel_sku')
					{
						$Daten['ATUNR']=$Feld;
					}
					if($Feld->getName()=='quantity')
					{
						$Daten['BESTAND']=$Feld;
					}
				}
				$Zeile = $Daten['ATUNR'].$Trenner.$Daten['BESTAND'];
				fputs($fp, $Zeile."\r\n");
			}
			echo PHP_EOL;
			fclose($fp);
			
			rename($Zieldatei.'.tmp', $Zieldatei);
		}
		catch(Exception $ex)
		{
			if(is_file($Zieldatei.'.tmp'))		// Temp Datei l�schen im Fehlerfall
			{
				unlink($Zieldatei.'.tmp');
			}
			throw new Exception($ex->getMessage());
		}	
	}
}
?>