<?php
/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 17.09.2016
 * Time: 10:32
 */

require __DIR__ . "/vendor/autoload.php";
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisSlack.php';

use React\EventLoop;
use Slack\RealTimeClient;

class awisSlackBot
    extends awisSlack
{
    private $_Token = '';
    private $_Befehle;
    private $_SlackRTMClient;
    private $_LetzterInit;
    private $_PID_Datei = '/daten/jobs/pid/awisSlackBot.pid';
    private $_AWIS_BOT_Username;

    function __construct()
    {
        parent::__construct();
        $this->setzeToken(array("TYP" => 'XPP', 'Wert' => 'SLACK_AWIS_BOT_TOKEN'));
        $this->_AWIS_BOT_Username = $this->_AWISBenutzer->ParameterLesen('SLACK_AWIS_BOT_USERNAME');
        $this->initBefehle();
    }

    function __destruct()
    {
        parent::__destruct();
        echo 'awisSlackBot beendet. L�sche PID';
        unlink($this->_PID_Datei);
    }

    /**
     * Pr�ft, ob bereits ein Bot l�uft, und Startet ihn ggf. an
     *
     * @return bool
     */
    public function Start()
    {
        $AktuellePID = getmypid();;

        if (is_file($this->_PID_Datei)) //Schauen ob eine PID da ist
        {
            $PID = file_get_contents($this->_PID_Datei); //PID aus Datei holen

            if (system('ps -j ' . $PID . ' | grep ' . $PID . ' -c') == '0') {
                echo 'PID: ' . $PID . ' war nicht mehr vorhanden. ';
                unlink($this->_PID_Datei);
            } else {
                echo 'Anderer Bot l�uft bereits. --> Beende mich nun selbst';
                return false;
            }
        }
        file_put_contents($this->_PID_Datei, $AktuellePID);
        $this->_Start();
    }

    private function _Start()
    {
        $loop = React\EventLoop\Factory::create();
        $client = new Slack\RealTimeClient($loop);

        $client->setToken($this->_Token);

        $client->on("message", function($data) use ($client) {
            $EingangsText = mb_strtoupper($data['text']);
            if (strpos($EingangsText, $this->_AWIS_BOT_Username) === 1) {//nur Nachrichten beantworten, die an mich sind.
                $EingangsText = trim(str_replace(mb_strtoupper('<' . $this->_AWIS_BOT_Username . '>'), '', $EingangsText));
                echo "Eingehende Nachricht registriert: " . $EingangsText . PHP_EOL;
                $EingangsText = str_replace('@awis_bot', '', $EingangsText);

                $client->getChannelGroupOrDMByID($data["channel"])->then(function($channel) use ($client, $EingangsText) {
                    return $client->send($this->verarbeiteNachricht($EingangsText,$client), $channel)->then(null, function(\Exception $exception) {

                        echo "Nachricht konnte nicht versendet werden." . PHP_EOL . $exception . PHP_EOL;
                    });
                }, function(\Exception $exception) {
                    echo "User/Channel nicht gefunden." . PHP_EOL . $exception . PHP_EOL;
                });
            }
        });

        $client->connect()->then(function() {
            echo "Mit slack verbunden!";
        });

        $loop->run();
    }

    /**
     * Setzt das API-Token
     *
     * @param $Token String | Array('TYP' => 'XXX', 'Wert' => 'xxx');
     */
    public function setzeToken($Token)
    {
        if (isset($Token['TYP'])) {
            switch ($Token['TYP']) {
                case 'XPP':
                    $this->_Token = $this->_AWISBenutzer->ParameterLesen($Token['Wert']);
                    break;
                default:
                    break;
            }
        } else {
            $this->_Token = $Token;
        }
    }


    /**
     * Verarbeitet die eingehende Nachricht
     *
     * @param $Nachricht string
     */
    private function verarbeiteNachricht($Nachricht,$Client)
    {
        global $Parameter;
        $Parameter = '';
        //Eingehende Nachricht in Befehl + Paraemter aufteilen
        if (strpos($Nachricht, ' ')) {
            $Split = explode(' ', $Nachricht, 2);
            $Nachricht = $Split[0];
            $Parameter = $Split[1];
            unset($Split);
        }

        //Letzter Befehlsinit �lter als 15 Minuten? --> Neuinitialisieren
        if ($this->_LetzterInit <= time() - 60 * 15) {
            $this->initBefehle();
        }

        //Schauen ob ich den Befehl kenne
        if (isset($this->_Befehle[$Nachricht]['Key'])) {

            try{
                //Datei:Klasse:Methode?
                if (strpos(($this->_Befehle[$Nachricht]['Datei']), ':')) {
                    $Split = explode(':', $this->_Befehle[$Nachricht]['Datei']);
                    $Datei = $Split[0];
                    $Klasse = $Split[1];
                    $Methode = $Split[2];
                    unset($Split);
                    ob_start();
                    require_once $Datei;
                    $Obj = new $Klasse($Parameter);
                    $Obj->$Methode($Parameter);
                    $Antwort = ob_get_clean();
                    unset($Obj);
                    unset($Datei);
                    unset($Klasse);
                    unset($Methode);
                } else { //Einfache PHP Datei
                    ob_start();
                    include($this->_Befehle[$Nachricht]['Datei']);
                    $Antwort = ob_get_clean();
                }
            }catch(Exception $exception)
            {
                echo $exception->getCode() . PHP_EOL;
                echo $exception->getMessage() . PHP_EOL;
                echo $Datei;
            }


            //Absender f�r diesen Befehl festlegen
            if ($this->_Befehle[$Nachricht]['Absender'] != '') {
                $this->Absender($this->_Befehle[$Nachricht]['Absender']);
            }

            //Icon festlegen
            if ($this->_Befehle[$Nachricht]['Icon'] != '') {
                $this->Icon($this->_Befehle[$Nachricht]['Icon']);
            }
        } else {//Befehl nicht in Tabelle SLACKBOT
            //Schauen,ob es ein Systembefehl ist
            if ($Nachricht == 'AWISBOT' and $Parameter == 'STOP') {
                $Client->disconnect();
                $Antwort = 'Ich beende mich';
            } elseif ($Nachricht == 'AWISBOT' and $Parameter == 'INIT') {
                $this->initBefehle();
                $Antwort = 'Habe mich neu initialisiert.';
            } else {
                $Antwort = 'Unbekannter Befehl';
            }
        }

        return $Antwort;

    }

    private function initBefehle()
    {
        echo 'Initialisiere neue Befehle';
        $this->_Befehle = array();
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();

        $SQL = 'SELECT XSB_KEY,';
        $SQL .= '   XSB_BEFEHL,';
        $SQL .= '   XSB_DATEI,';
        $SQL .= '   XSB_ABSENDER,';
        $SQL .= '   XSB_ICON,';
        $SQL .= '   XSB_PROTOKOLL';
        $SQL .= ' FROM SLACKBOT ';
        $SQL .= ' where XSB_STATUS = 1';

        $rsBefehle = $DB->RecordSetOeffnen($SQL);

        while (!$rsBefehle->EOF()) {
            $Befehl = $rsBefehle->FeldInhalt('XSB_BEFEHL');
            $Params = false;
            if (strpos($Befehl, ':')) {
                $Befehl = explode(':', $Befehl)[0];
                $Params = true;
            }
            $Befehl = mb_strtoupper(trim($Befehl));

            $this->_Befehle[$Befehl]['Key'] = $rsBefehle->FeldInhalt('XSB_KEY');
            $this->_Befehle[$Befehl]['Params'] = $Params;
            $this->_Befehle[$Befehl]['Datei'] = $rsBefehle->FeldInhalt('XSB_DATEI');
            $this->_Befehle[$Befehl]['Absender'] = $rsBefehle->FeldInhalt('XSB_ABSENDER');
            $this->_Befehle[$Befehl]['Icon'] = $rsBefehle->FeldInhalt('XSB_ICON');
            $this->_Befehle[$Befehl]['Protokoll'] = $rsBefehle->FeldInhalt('XSB_PROTOKOLL');

            $rsBefehle->DSWeiter();
        }
        $this->_LetzterInit = time();
    }

    /**
     *
     * Noch ohne Sinn.
     *
     **/
    public function InitUser()
    {
        $service_url = 'https://slack.com/api/users.list?token=' . $this->_Token;
        $curl = curl_init($service_url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $decoded = json_decode($curl_response, true);

        foreach ($decoded['members'] as $Member) {
            echo $Member['id'] . ' ' . $Member['name'] . '<br>';
        }
    }
}