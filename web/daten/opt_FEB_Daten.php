<?php
//**************************************************************
// Liefert die Daten f�r das Testformular
//**************************************************************
//**************************************************************
require_once('db.inc.php');
require_once('awis_forms.inc.php');

if(!isset($_GET['FEB_EBENE']))
{
	die('##PARAM##');
}
$con = awisLogon();

$SQL = "SELECT FEB_KEY, FEB_BEZEICHNUNG || ' ( > ' || FEB_GUELTIGAB || ')' AS BEZEICHNUNG";
$SQL .= " FROM FILIALEBENEN ";
$SQL .= ' WHERE feb_ebene+1=0'.$_GET['FEB_EBENE'];
$SQL .= ' AND trunc(FEB_GUELTIGBIS) >= trunc(SYSDATE)';       // AND FEB_GUELTIGAB <= SYSDATE';
$SQL .= ' ORDER BY FEB_SORTIERUNG';

$rsFEB=awisOpenRecordset($con,$SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
for($FEBZeile=0;$FEBZeile<$awisRSZeilen;$FEBZeile++)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsFEB['FEB_KEY'][$FEBZeile].'">'.$rsFEB['BEZEICHNUNG'][$FEBZeile].'</option>';
	}
	else 
	{
		echo '#+#'.$rsFEB['FEB_KEY'][$FEBZeile].'#+#'.$rsFEB['BEZEICHNUNG'][$FEBZeile].'';
	}
}
?>