<?php
require_once('awisFilialen.inc');
global $con;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FER','%');
	$TextKonserven[]=array('FRZ','%');
	$TextKonserven[]=array('KON','KON_NAME');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5014= $AWISBenutzer->HatDasRecht(5014);
	$Recht150 = $AWISBenutzer->HatDasRecht(150);
	if(($Recht5014&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}


	//********************************************************
	// Daten suchen
	//********************************************************
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');


    $FeldBreiten['FER_BEZEICHNUNG']=330;
    $FeldBreiten['KON_NAME']=250;
    $FeldBreiten['FRZ_BEMERKUNG']=270;


	$Form->Formular_Start();

	$PEIKey = new awisFilialen($AWIS_KEY1);
	$BetreuerListe = $PEIKey->BetreuerListe(0,'',false);
	$Form->ZeileStart();

	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'],$FeldBreiten['FER_BEZEICHNUNG']);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME'],$FeldBreiten['KON_NAME']);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_BEMERKUNG'],$FeldBreiten['FRZ_BEMERKUNG']);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'],100);

	$Form->ZeileEnde();
	$DS = 0;
	foreach($BetreuerListe AS $Betreuer)
	{
		$Form->ZeileStart();

		$Form->Erstelle_ListenFeld('#FER_BEZEICHNUNG',$Betreuer['FER_BEZEICHNUNG'],0,$FeldBreiten['FER_BEZEICHNUNG'],false,($DS%2),'','','T','L',$Betreuer['FER_BEMERKUNG']);		
		//Link zu Telefon nicht immer anzeigen
		if(($Recht150&64)!=0)
		{
			$SQL = 'SELECT KON_LISTEN FROM KONTAKTE WHERE KON_KEY = 0'.$Betreuer['KON_KEY'];
			$rsKON = $DB->RecordSetOeffnen($SQL);
		
			if (($rsKON->FeldInhalt('KON_LISTEN')&2)!=0)
			{
				$Link = '/telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey=0'.$Betreuer['KON_KEY'];
			}
			else 
			{
				$Link='';
			}
		}
		else 
		{
			$Link = '/telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey=0'.$Betreuer['KON_KEY'];
		}
		$Form->Erstelle_ListenFeld('#KON_NAME',$Betreuer['KON_NAME1'].' '.$Betreuer['KON_NAME2'],0,$FeldBreiten['KON_NAME'],false,($DS%2),'',$Link,'T','L');		
		$Form->Erstelle_ListenFeld('#FRZ_BEMERKUNG',$Betreuer['FRZ_BEMERKUNG'],0,$FeldBreiten['FRZ_BEMERKUNG'],false,($DS%2),'','','T','L');
		$Form->Erstelle_ListenFeld('#FRZ_GUELTIGAB',$Betreuer['FRZ_GUELTIGAB'],0,100,false,($DS%2),'','','D','L');

		$Form->ZeileEnde();
		$DS++;
	}

	$Form->Formular_Ende();

	if($CursorFeld!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200910250928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200910250927");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>