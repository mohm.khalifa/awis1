<?php
require_once('awis_mobilitaetsgarantien.inc');

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 27.09.2016
 * Time: 15:23
 */
class awisMOBDateiImport extends awis_mobilitaetsgarantien
{
    //F�r Import
    const ImpPfad = '/daten/daten/werkstattauftraege/MOBILITAETSGARANTIE/daten/';

    //F�r Export
    private $_ExpPfad = array();
    private $_ExpDateinamen = array();

    private $_Sektionen = array();


    function __construct($Benutzer)
    {
        parent::__construct(false,$Benutzer);
        $this->_ExpPfad['MOB'] = '/win/applicationdata2/AWIS/05-Mobilitaetsgarantien/';
        $this->_ExpPfad['VER'] = '/daten/daten/pcedv11/DATEN/Vers/';
        $this->_ExpPfad['FIN'] = '/daten/daten/pcedv11/DATEN/Vers/';
        $this->_ExpPfad['EST'] = '/daten/daten/pcedv11/DATEN/Estermann/';


        $Datum = date('ymd');

        $this->_ExpDateinamen['MOB'] = "mob$Datum.txt";
        $this->_ExpDateinamen['VER'] = "vers$Datum.txt";
        $this->_ExpDateinamen['FIN'] = "vers$Datum.txt";
        $this->_ExpDateinamen['EST'] = "est$Datum.txt";

        $this->_Sektionen = array('MOB', 'VER', 'FIN', 'EST');
    }

    function __destruct()
    {
        parent::__destruct();
    }


    /**
     * Extrahiert die Mobdatei in verschiedene Dateien
     */
    public function Extrahieren()
    {
        $Werkzeug = new awisWerkzeuge();

        $Protokoll['DateiANZ'] = 0;

        $MOBDateien = scandir(self::ImpPfad); //Ordner "files" auslesen

        foreach ($MOBDateien as $Datei) { // Ausgabeschleife
            if (substr($Datei, 0, 3) == 'mob' and (substr($Datei, -3) == 'all' or is_numeric(substr($Datei, -3)) )) {
                //Pr�fsumme der Datei pr�fen
                if ($this->_PruefeImportProtokoll(self::ImpPfad . $Datei)) {
                    $this->_SchreibeImportProtokoll(self::ImpPfad . $Datei);

                    $this->debugAusgabe('Importiere Datei: ' . $Datei, 1, 'Info');
                    $Protokoll['DateiANZ']++;

                    //Export und Importdateien �ffnen
                    $fd = fopen(self::ImpPfad . $Datei, 'r');

                    $ExpDateien = array();
                    foreach ($this->_Sektionen as $Sek) {
                        $ExpDateien[$Sek] = fopen($this->_ExpPfad[$Sek] . $this->_ExpDateinamen[$Sek] . '.tmp', 'a+');
                    }

                    $AktSektion = '';

                    while (!feof($fd)) {//Zeilenweise durchgehen
                        $Zeile = fgets($fd);

                        if ($Zeile == '') { //Leerzeilen igonieren
                            continue;
                        }
                        if (substr($Zeile, 0, 1) == '@') {//Neue Sektionen beginnen mit einem @
                            $AktSektion = trim(substr($Zeile, 5, 3));
                        } else {
                            fwrite($ExpDateien[$AktSektion], $Zeile); //Inhalt in die entsprechende Datei schreiben
                        }
                    }
                }
                rename(self::ImpPfad . $Datei,self::ImpPfad . '/old/'.$Datei);
            }
        }

        //Tempdateien umbenennen
        foreach ($this->_Sektionen as $Sek) {
            @rename($this->_ExpPfad[$Sek] . $this->_ExpDateinamen[$Sek] . '.tmp', $this->_ExpPfad[$Sek] . $this->_ExpDateinamen[$Sek]);
        }


        if ($Protokoll['DateiANZ'] == 0) {
            $this->debugAusgabe('Ich konnte keine Datei finden :( ', 1, 'Info');
            $Werkzeug->EMail('shuttle@de.atu.eu', 'WARNING MOB-Datei', 'Keine Datei zum extrahieren vorhanden...' . PHP_EOL . $this->_DebugText, 2, '', 'awis@de.atu.eu');
        } else {
            $this->debugAusgabe('Fertig! Anzahl Dateien ' . $Protokoll['DateiANZ'], 1, 'Info');
            $Werkzeug->EMail('shuttle@de.atu.eu', 'OK MOB-Datei', $this->_DebugText. PHP_EOL . $this->_DebugText, 1, '', 'awis@de.atu.eu');
        }
    }
}