<?php
//global $AWIS_KEY1;
//global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Meldung','UngueltigerArtikel');
$TextKonserven[]=array('Meldung','Voraussetzung%');
$LoeschDelta=array();

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Werkzeuge = new awisWerkzeuge();
	
	$Form = new awisFormular();

	$AWISSprachKonservenSpeichern = $Form->LadeTexte($TextKonserven);	
	$Recht13001 = $AWISBenutzer->HatDasRecht(13001);
	if($Recht13001==0)
	{
	    awisEreignis(3,1000,'KundenKfzInfos',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonservenSpeichern['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonservenSpeichern['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	
	$RemoteDB='';					
	$RemoteConn='';
	
	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
	{
		$RemoteDB='@COM_DE.ATU.DE';
		$RemoteConn='COM_DE';
	}
	else 
	{
		$RemoteDB='@COM_TEST_DE.ATU.DE';
		$RemoteConn='COM_TEST_DE';
	}	
	
	if (($Recht13001&2)!=0 AND isset($_POST['txtEditDS']) AND $_POST['txtEditDS']==true)
	{
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);

		if (isset($_POST['txtVERTRAGSNUMMER']))
		{
			$Speichern=true;
		}else {
			$Speichern=false;
		}
		
		$SQL = '';
	
		if($Speichern)
		{
			$SQL = 'UPDATE EXPERIAN_ATU.EXP_KUNDEN_KFZ_DELTA_TYP'.$RemoteDB.' SET ';			
			$SQL .= ' VERTRAGSNUMMER = '.$DB->FeldInhaltFormat('Z',$_POST['txtVERTRAGSNUMMER'],false);			
			$SQL .= ', DELTATYP = '.$DB->FeldInhaltFormat('Z',$_POST['txtDELTATYP'],false);			
			$SQL .= ', IMPORTZYKLUS_TAGE = '.$DB->FeldInhaltFormat('Z',$_POST['txtIMPORTZYKLUS_TAGE'],true);			
			$SQL .= ', CREAUSER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
			$SQL .= ', USERDAT = sysdate';
			$SQL .= ' WHERE VERTRAGSNUMMER = '.$DB->FeldInhaltFormat('Z',$_POST['oldVERTRAGSNUMMER']);				
		}
		
		if($SQL!='')
		{
			$Form->DebugAusgabe(1,$SQL);
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),201201231422,$SQL,2);
			}
			$Form->Formular_Start();
				$Form->Hinweistext('KundenInfos f�r Vertragsnummer:'.$DB->FeldInhaltFormat('Z',$_POST['txtVERTRAGSNUMMER'],false).' wurden ge�ndert.',1,'');								
			$Form->Formular_Ende();		
			
		}							
	}
	elseif(($Recht13001&4)!=0 AND isset($_POST['txtAddDS']) AND $_POST['txtAddDS']==true)
	{
		$SQL='INSERT INTO EXPERIAN_ATU.EXP_KUNDEN_KFZ_DELTA_TYP'.$RemoteDB.' (VERTRAGSNUMMER, DELTATYP, IMPORTZYKLUS_TAGE) VALUES ';
		$SQL.='('.$DB->FeldInhaltFormat('Z',$_POST['txtVERTRAGSNUMMER'],false).','.$DB->FeldInhaltFormat('Z',$_POST['txtDELTATYP'],false).','.$DB->FeldInhaltFormat('Z',$_POST['txtIMPORTZYKLUS_TAGE'],true).')';
		
		if($SQL!='')
		{
			$Form->DebugAusgabe(1,$SQL);
			
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),201201231435,$SQL,2);
			}
		}		
		
		$Form->Formular_Start();
			$Form->Hinweistext('KundenInfos f�r Vertragsnummer:'.$DB->FeldInhaltFormat('Z',$_POST['txtVERTRAGSNUMMER'],false).' wurden erstellt.',1,'');	
		$Form->Formular_Ende();				
	}	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>