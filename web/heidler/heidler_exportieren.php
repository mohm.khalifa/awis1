<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
    require_once 'heidler_funktionen.php';

    $Functions = new heidler_funktionen();
    $mwstSatzDE = 100 + $Functions->mwstSatz();

    $TextKonserven = array();
    $TextKonserven[]=array('HEIDLER','*');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('FEHLER','err_UngueltigesUploadDateiFormat');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht45000 = $AWISBenutzer->HatDasRecht(45000);
    if($Recht45000==0)
    {
        echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
        echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $Param['LAGER'] = $Form->Format('T',$_POST['sucLAGER'],true);
    $Param['DATUM_KOMMISSIONIERUNG_VON'] = $Form->Format('D',$_POST['sucDATUM_KOMMISSIONIERUNG_VON'],true);
    $Param['DATUM_KOMMISSIONIERUNG_BIS'] = $Form->Format('D',$_POST['sucDATUM_KOMMISSIONIERUNG_BIS'],true);
    $Param['VERSANDART'] = 	$Form->Format('T',$_POST['sucVERSANDART'],true);
    $Param['SATZKENNUNG'] = $Form->Format('T',$_POST['sucSATZKENNUNG'],true);
    $Param['MANDANT']=$Form->Format('T',$_POST['sucMANDANT'],true);
    $Param['HEIDLER_MANDANT']=$Form->Format('T', $_POST['sucHEIDLER_MANDANT'], true);
    $Param['ZUGESTELLT']=$Form->Format('T',$_POST['sucZUGESTELLT'],true);
    $Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
    $Param['OFFENE']=isset($_POST['sucOffenePosten']) && isset($_POST['sucAuswahlSpeichern'])?'on':'';
    $AWISBenutzer->ParameterSchreiben('Formular_Heidler_Export',serialize($Param));


    //**********************************************************
    // Export der aktuellen Daten
    //**********************************************************
    ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
    ini_set('max_execution_time', 600);
    require_once('PHPExcel.php');

    $ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);

    @ob_end_clean();
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Expires: 01 Mar 2015");
    header('Pragma: public');
    header('Cache-Control: max-age=0');

    switch ($ExportFormat)
    {
        case 1:                 // Excel 5.0
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="Auswertung_Heidler.xls"');
            break;
        case 2:                 // Excel 2007
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="Auswertung_Heidler.xlsx"');
            break;
    }

    $XLSXObj = new PHPExcel();
    $XLSXObj->getProperties()->setCreator(utf8_encode($AWISBenutzer->BenutzerName()));
    $XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($AWISBenutzer->BenutzerName()));
    $XLSXObj->getProperties()->setTitle(utf8_encode('Auswertung_Heidler'));
    $XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
    $XLSXObj->getProperties()->setDescription(utf8_encode('Auswertung_Heidler'));

    $XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');

    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);

    $SQL = 'select ';
    $SQL .= 'trim(mv.SATZKENNUNG) as SATZKENNUNG';
    $SQL .= ', mv.KNDNR';
    $SQL .= ', mv.EMPF_NAME1';
    $SQL .= ', mv.EMPF_NAME2';
    $SQL .= ', mv.EMPF_NAME3';
    $SQL .= ', mv.EMPF_STRASSE';
    $SQL .= ', mv.EMPF_LKZ';
    $SQL .= ', mv.EMPF_PLZ';
    $SQL .= ', mv.EMPF_ORT';
    $SQL .= ', mv.TRACKING_NR';
    $SQL .= ', mv.ATU_NR';
    $SQL .= ', (nvl(artstamm.AST_VK,0) / '. $mwstSatzDE .') * 80 as PREIS';
    $SQL .= ', mv.REFERENZNR';
    $SQL .= ', mv.KENN1';
    $SQL .= ', mv.LAGERKZ';
    $SQL .= ', mv.DATUM_KOMMISSIONIERUNG';
    $SQL .= ', mv.MENGE';
    $SQL .= ', mv.VERBUCHTE_MENGE';
    $SQL .= ', mv.AUFTRAG_ID';
    $SQL .= ', mv.LABEL_GENDATE';
    $SQL .= ', mv.VERSANDART';
    $SQL .= ', mv.GEWICHT_NETTO';
    $SQL .= ', mv.PACKSTKNR';
    $SQL .= ', mv.MANDANT';
    $SQL .= ', mv.HEIDLER_MANDANT';
    $SQL .= ', status.BEZ';
    $SQL .= ', status.ZUGESTELLT';
    $SQL .= ' from MV_HEIDLER mv';
    $SQL .= ' LEFT JOIN ARTIKELSTAMM artstamm ON ATU_NR = AST_ATUNR';
    $SQL .= ' LEFT OUTER JOIN ';
    $SQL .= ' (';
    $SQL .= '   SELECT status.TRACKING_NR as TRACKING_NR, feld.BESCHREIBUNG as BEZ, feld.ZUGESTELLT as ZUGESTELLT';
    $SQL .= '   FROM ';
    $SQL .= '   (';
    $SQL .= '       select nox2.* from';
    $SQL .= '       (';
    $SQL .= '           select tracking_nr, max(datum) as datum';
    $SQL .= '           from nox_status@nvswen';
    $SQL .= '           group by tracking_nr';
    $SQL .= '       ) nox1';
    $SQL .= '   left join nox_status@nvswen nox2';
    $SQL .= '   on nox1.tracking_nr = nox2.tracking_nr and nox1.datum = nox2.datum';
    $SQL .= '   ) status';
    $SQL .= '   LEFT JOIN NOX_STATUS_SCAN_FELD@NVSWEN feld on status.SCAN_FELD = feld.SCAN_FELD';
    $SQL .= '   union all';
    $SQL .= '   (';
    $SQL .= '       SELECT dpd2.TRACKING_NR as TRACKING_NR, dpd2.HEADLINE || \', \' || dpd2.LAST_DESCRIPTION as BEZ, case when dpd2.DELIVERED_STATUS = 1 THEN \'J\' ELSE \'N\' END as ZUGESTELLT';
    $SQL .= '       from';
    $SQL .= '       (';
    $SQL .= '           select tracking_nr, max(import_date) as IMPORT_DATE';
    $SQL .= '           from dpd_status@nvswen';
    $SQL .= '           group by tracking_nr';
    $SQL .= '       ) dpd1';
    $SQL .= '       left join dpd_status@nvswen dpd2';
    $SQL .= '       on dpd1.tracking_nr = dpd2.tracking_nr and dpd1.IMPORT_DATE = dpd2.IMPORT_DATE';
    $SQL .= '   )';
    $SQL .= ' ) status on mv.TRACKING_NR = status.TRACKING_NR ';


    if ($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $rsHeidler = $DB->RecordSetOeffnen($SQL);

    $XLSXObj->setActiveSheetIndex(0);
    $XLSXObj->getActiveSheet()->setTitle(utf8_encode('Heidler'));

    $SpaltenNr = 0;
    $ZeilenNr = 1;

    //Überschrift
    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AWIS');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode('Erstellt'));
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, date('d.m.Y G:i',time()));
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setSize(12);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFFF0000');

    $ZeilenNr++;
    $ZeilenNr++;
    $SpaltenNr = 0;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Satzkennung');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'KundenNr');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Name1');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Name2');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Strasse');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Land');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'PLZ');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Ort');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Trackingnummer');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'ATU-Nr');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Preis');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Referenznummer');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Lager');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'DatumKommissionierung');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Menge');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'VerbuchteMenge');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'AuftragID');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Versandart');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'GewichtNetto');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'F-Mandant');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'H-Mandant');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Zugestellt');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, 'Letzter_Status');
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFont()->setBold(true);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr,$ZeilenNr)->getFill()->getStartColor()->setARGB('FFE0E0E0');
    $SpaltenNr++;

    $ZeilenNr++;
    $SpaltenNr=0;

    while(!$rsHeidler->EOF())
    {
        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('SATZKENNUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('KNDNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('EMPF_NAME1'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('EMPF_NAME2'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('EMPF_STRASSE'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('EMPF_LKZ'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('EMPF_PLZ'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('EMPF_ORT'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('TRACKING_NR'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('ATU_NR'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('PREIS', 'N2'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('REFERENZNR'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('LAGERKZ'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr,utf8_encode($Form->Format('D',$rsHeidler->FeldInhalt('DATUM_KOMMISSIONIERUNG'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('MENGE'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('VERBUCHTE_MENGE'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('AUFTRAG_ID'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('VERSANDART'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('GEWICHT_NETTO'))),PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('MANDANT'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('HEIDLER_MANDANT'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('ZUGESTELLT'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $XLSXObj->getActiveSheet()->setCellValueExplicitByColumnAndRow($SpaltenNr,$ZeilenNr, utf8_encode($Form->Format('T',$rsHeidler->FeldInhalt('BEZ'))),PHPExcel_Cell_DataType::TYPE_STRING);
        $XLSXObj->getActiveSheet()->getStyleByColumnAndRow($SpaltenNr++,$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $ZeilenNr++;
        $SpaltenNr=0;
        $rsHeidler->DSWeiter();
    }

    for($S='A';$S<='G';$S++)
    {
        $XLSXObj->getActiveSheet()->getColumnDimension($S)->setAutoSize(true);
    }

    $XLSXObj->setActiveSheetIndex(0);

    switch ($ExportFormat)
    {
        case 1:                 // Excel 5.0
            $objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
            break;
        case 2:                 // Excel 2007
            $objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
            break;
    }

    $objWriter->save('php://output');
    $XLSXObj->disconnectWorksheets();

}
catch(exception $ex)
{
    $Form->DebugAusgabe(1,$ex->getMessage());
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $_POST
 * @return string
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;
    global $Form;

    $TextKonserven = array();
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Bedingung = '';

    if(isset($Param['LAGER']) AND $Param['LAGER']!='')
    {
        $Bedingung .= 'AND LAGERKZ ' . $DB->LikeOderIst($Param['LAGER']) . ' ';
    }
    if(isset($Param['DATUM_KOMMISSIONIERUNG_VON']) AND $Param['DATUM_KOMMISSIONIERUNG_VON']!='')
    {
        $Bedingung .= 'AND TRUNC(DATUM_KOMMISSIONIERUNG) >= to_date(\'' . $Param['DATUM_KOMMISSIONIERUNG_VON'] . '\') ';
    }
    if(isset($Param['DATUM_KOMMISSIONIERUNG_BIS']) AND $Param['DATUM_KOMMISSIONIERUNG_BIS']!='')
    {
        $Bedingung .= 'AND TRUNC(DATUM_KOMMISSIONIERUNG) <= to_date(\'' . $Param['DATUM_KOMMISSIONIERUNG_BIS'] . '\') ';
    }
    if(isset($Param['VERSANDART']) AND $Param['VERSANDART']!='' AND $Param['VERSANDART']!='%')
    {
        $Bedingung .= 'AND VERSANDART IN (' . $Param['VERSANDART'] . ') ';
    }
    if(isset($Param['SATZKENNUNG']) AND $Param['SATZKENNUNG']!='' AND $Param['SATZKENNUNG'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
    {
        $Bedingung .= 'AND TRIM(SATZKENNUNG) ' . $DB->LikeOderIst($Param['SATZKENNUNG']) . ' ';
    }
    if(isset($Param['MANDANT']) AND $Param['MANDANT']!='')
    {
        $Bedingung .= 'AND MANDANT ' . $DB->LikeOderIst($Param['MANDANT']) . ' ';
    }
    if(isset($Param['HEIDLER_MANDANT']) AND $Param['HEIDLER_MANDANT']!='')
    {
        $Bedingung .= 'AND HEIDLER_MANDANT ' . $DB->LikeOderIst($Param['HEIDLER_MANDANT']) . ' ';
    }
    if(isset($Param['ZUGESTELLT']) AND $Param['ZUGESTELLT']!='' AND $Param['ZUGESTELLT'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])
    {
        $Bedingung .= 'AND TRIM(ZUGESTELLT) ' . $DB->LikeOderIst($Param['ZUGESTELLT']) . ' ';
    }
    if(isset($_POST['sucOffenePosten'])) {
        if($_POST['sucOffenePosten'] != '') {
            $Bedingung .= 'AND MENGE <> VERBUCHTE_MENGE';
        }
    }
    return $Bedingung;
}
?>