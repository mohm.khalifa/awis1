<?php
global $con;
global $awisRSZeilen;
global $AWISBenutzer;

require_once('fpdi.php');
require_once('awis_forms.inc.php');

$Farbe[3][0]=200;	// Urlaub
$Farbe[3][1]=0;
$Farbe[3][2]=0;
$Farbe[16][0]=255;	// Urlaub Neu
$Farbe[16][1]=255;
$Farbe[16][2]=0;
$Farbe[17][0]=100;	// Urlaub Alt
$Farbe[17][1]=200;
$Farbe[17][2]=200;
$Farbe[18][0]=200;	// Urlaub Plan
$Farbe[18][1]=200;
$Farbe[18][2]=200;
$Farbe[19][0]=0;	// Überstunden
$Farbe[19][1]=100;
$Farbe[19][2]=0;
$Farbe[21][0]=255;	// Überstunden
$Farbe[21][1]=165;
$Farbe[21][2]=000;


/*
$Farbe[3][0]=200;	// Urlaub
$Farbe[3][1]=0;
$Farbe[3][2]=0;
$Farbe[16][0]=200;	// Urlaub Neu
$Farbe[16][1]=200;
$Farbe[16][2]=0;
$Farbe[17][0]=200;	// Urlaub Alt
$Farbe[17][1]=200;
$Farbe[17][2]=200;
$Farbe[18][0]=100;	// Urlaub Plan
$Farbe[18][1]=200;
$Farbe[18][2]=200;
$Farbe[19][0]=100;	// Überstunden
$Farbe[19][1]=100;
$Farbe[19][2]=100;
*/
$SQL = "select MIT_BEZEICHNUNG, MIT_KEY";
$SQL .= ' FROM Mitarbeiter ';
$SQL .= ' WHERE ';
$SQL .= " MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";
$SQL .= ' ORDER BY MIT_BEZEICHNUNG';
$rsMIT = awisOpenRecordset($con, $SQL);
$rsMITZeilen = $awisRSZeilen;
$Mitarbeiter=array();
for($MITZeile=0;$MITZeile<$rsMITZeilen;$MITZeile++)
{
	$Mitarbeiter[$rsMIT['MIT_KEY'][$MITZeile]]=array('Anzeige'=>0,'Name'=>$rsMIT['MIT_BEZEICHNUNG'][$MITZeile]);
}

$SQL = "select MIT_BEZEICHNUNG, MIT_KEY, MEA_ABKUERZUNG, MEI_TAG, MEA_KEY, MEA_BEZEICHNUNG, ";
$SQL .= " TO_CHAR(MEI_TAG,'RRRR-MM') AS Monat ";
$SQL .= ' FROM Mitarbeiter ';
$SQL .= ' INNER JOIN MitarbeiterEinsaetze ON MIT_KEY = MEI_MIT_KEY';
$SQL .= ' INNER JOIN MitarbeiterEinsaetzeArten ON MEI_MEA_KEY = MEA_KEY AND BITAND(MEA_AUSWERTUNG,1)=1';
$SQL .= ' WHERE ';
$SQL .= " MIT_BEREICH = '" . $_POST['txtMIT_BEREICH'] . "'";

if($_POST['txtDatumVom']!='')		//txtDatumVom
{
	$SQL .= " AND MEI_TAG>=TO_DATE('" . $_POST['txtDatumVom'] . "','DD.MM.RRRR')";
}	
if($_POST['txtDatumBis']!='')		//txtDatumBis
{
	$SQL .= " AND MEI_TAG<=TO_DATE('" . $_POST['txtDatumBis'] . "','DD.MM.RRRR')";
}	
if($_POST['txtMEI_MIT_KEY']!='-1')
{
	$SQL .= " AND MEI_MIT_KEY=" . $_POST['txtMEI_MIT_KEY'] . "";
}
if($_POST['txtMIT_MTA_ID']!='-1')
{
	$SQL .= " AND MIT_MTA_ID=" . $_POST['txtMIT_MTA_ID'] . "";
}

if(($RechteStufe&2)==2)	// Einschränkung Technik-Übersicht
{
	$SQL .= ' AND MIT_MTA_ID IN (30,40,50)';
}
$SQL .= " ";
$SQL .= " ORDER BY to_char(MEI_TAG,'RRRR-MM'),MIT_BEZEICHNUNG, MIT_KEY, MEI_TAG";

awis_Debug(1,$SQL);

$rsMEI = awisOpenRecordset($con, $SQL );
$rsMEIZeilen = $awisRSZeilen;


$pdf = new fpdi('l','mm','a4');
$pdf->open();
$pdf->setSourceFile("../bilder/atulogo_neu.pdf");
$pdf->SetAutoPageBreak(false,0);
$pdf->SetAuthor($AWISBenutzer->BenutzerName());

$ATULogo = $pdf->ImportPage(1);				

if($rsMEIZeilen==0)
{
	$pdf->addpage();
	$pdf->SetFont('Arial','',10);
	$pdf->cell(270,6,'Es konnten keine Daten zum Drucken gefunden werden.',0,0,'C',0);
}
		
$RasterZeile=9999;
$LetzterMIT_KEY = '';
$LetzterMonat = '';
$Legende = array();
$NeueZeile=false;
		
for($MEIZeile=0;$MEIZeile<$rsMEIZeilen;$MEIZeile++)
{
	$NeueZeile=false;
	if($LetzterMonat!=$rsMEI['MONAT'][$MEIZeile] OR $RasterZeile>180)
	{
		if($MEIZeile!=0)
		{
			foreach($Mitarbeiter AS $Eintrag)
			{
				if($Eintrag['Anzeige']==0)
				{
					$RasterZeile+=5;
					
					$pdf->SetFillColor(240,240,240);
					$pdf->SetDrawColor(0,0,0);
					$pdf->setXY(10,$RasterZeile);
			
					$pdf->SetFont('Arial','',10);
					$pdf->cell(80,5,$Eintrag['Name'],1,0,'L',1);
					for($Tag = 1;$Tag<=$MaxTag;$Tag++)
					{
						$pdf->cell(5,5,'',1,0,'C',0);	
					}
				}
			}
			
			$pdf->SetXY(15,20);
			$pdf->SetDrawColor(0,0,0);
			$pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
			$Legende = array();
		}
		$pdf->addpage();
		$pdf->useTemplate($ATULogo,270,4,20);

		$pdf->SetFont('Arial','',6);
		$pdf->cell(270,6,'Diese Liste ist nur für interne Zwecke und darf nicht an Dritte weitergegeben werden.',0,0,'C',0);

		$RasterZeile = 10;
		$pdf->SetFont('Arial','',20);
		$pdf->text(10,$RasterZeile,"Urlaubstage " . $rsMEI['MONAT'][$MEIZeile]);
		$RasterZeile = 25;
		
		$pdf->SetFillColor(240,240,240);
		$pdf->SetDrawColor(0,0,0);
		$pdf->setXY(10,$RasterZeile);

		$pdf->SetFont('Arial','',10);
		$pdf->cell(80,5,'Mitarbeitername',1,0,'L',1);
		$Datum = awis_PruefeDatum($rsMEI['MEI_TAG'][$MEIZeile],0,1);
		$MaxTag = date('d',(mktime(0,0,0,date('m',$Datum)+1,0,date('Y',$Datum))));
		
		for($Tag = 1;$Tag<=$MaxTag;$Tag++)
		{
			$pdf->cell(5,5,$Tag,1,0,'C',1);	
		}
		$LetzterMonat=$rsMEI['MONAT'][$MEIZeile];
				
		$NeueZeile=true;

		// Alle Eintraege zurücksetzen
		foreach($Mitarbeiter AS $Eintrag=>$Werte)
		{
			$Mitarbeiter[$Eintrag]['Anzeige']=0;
		}
	}
	
	
	// Neuer Mitarbeiter -> neue Zeile
	if($LetzterMIT_KEY!=$rsMEI['MIT_KEY'][$MEIZeile] or $NeueZeile==true)
	{
		$RasterZeile += 5;
		
		$pdf->SetFillColor(240,240,240);
		$pdf->SetDrawColor(0,0,0);
		$pdf->setXY(10,$RasterZeile);

		$pdf->SetFont('Arial','',10);
		$pdf->cell(80,5,$rsMEI['MIT_BEZEICHNUNG'][$MEIZeile],1,0,'L',1);
		$Mitarbeiter[$rsMEI['MIT_KEY'][$MEIZeile]]['Anzeige']=1;

		for($Tag = 1;$Tag<=$MaxTag;$Tag++)
		{
			$pdf->cell(5,5,'',1,0,'C',0);	
		}
		
		$LetzterMIT_KEY=$rsMEI['MIT_KEY'][$MEIZeile];
	}
	
	
	$Datum = awis_PruefeDatum($rsMEI['MEI_TAG'][$MEIZeile],0,1);
	$Tag = date('d',$Datum);
	$pdf->setXY(90+(5*($Tag-1)),$RasterZeile);
	$pdf->SetFillColor($Farbe[$rsMEI['MEA_KEY'][$MEIZeile]][0],$Farbe[$rsMEI['MEA_KEY'][$MEIZeile]][1],$Farbe[$rsMEI['MEA_KEY'][$MEIZeile]][2]);
	$pdf->SetFont('Arial','',4);
	$pdf->cell(5,5,$rsMEI['MEA_ABKUERZUNG'][$MEIZeile],1,0,'C',1);	
	
	if(array_search($rsMEI['MEA_ABKUERZUNG'][$MEIZeile].' - ' .$rsMEI['MEA_BEZEICHNUNG'][$MEIZeile],$Legende)===false)
	{
		$Legende[$rsMEI['MEA_ABKUERZUNG'][$MEIZeile]]=$rsMEI['MEA_ABKUERZUNG'][$MEIZeile] . ' - ' .$rsMEI['MEA_BEZEICHNUNG'][$MEIZeile];
	}
}
if($MEIZeile>0)
{
	foreach($Mitarbeiter AS $Eintrag)
	{
		if($Eintrag['Anzeige']==0)
		{
			$RasterZeile+=5;
			
			$pdf->SetFillColor(240,240,240);
			$pdf->SetDrawColor(0,0,0);
			$pdf->setXY(10,$RasterZeile);
	
			$pdf->SetFont('Arial','',10);
			$pdf->cell(80,5,$Eintrag['Name'],1,0,'L',1);
			for($Tag = 1;$Tag<=$MaxTag;$Tag++)
			{
				$pdf->cell(5,5,'',1,0,'C',0);	
			}
		}
	}
	
	$pdf->SetXY(15,20);
	$pdf->SetDrawColor(0,0,0);
	$pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
	$Legende = array();
	
	$pdf->SetXY(15,20);
	$pdf->SetDrawColor(0,0,0);
	$pdf->Cell(200,4,'Legende:'.implode(',',$Legende),0,0,'L',0);
}
?>