<?php
global $con;
global $awisRSZeilen;
global $CursorFeld;

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$_SERVER['PHP_AUTH_USER']);
$Param = awis_BenutzerParameter($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER']);
$BSBreite = awis_BenutzerParameter($con, "BildschirmBreite" , $_SERVER['PHP_AUTH_USER']);

$TextKonserven = array();
$TextKonserven[]=array('MBH','MBH_%');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3010 = awisBenutzerRecht($con,3010);

$MaxDSAnzahl=500;

$SQL = 'SELECT *';
$SQL .= ' FROM mitbewerbhersteller INNER JOIN hersteller ON mbh_her_id = her_id';
$SQL .= ' WHERE mbh_mbw_key=0'.$Param;
if(isset($_GET['Edit']))
{
	$SQL .= ' AND mbh_key=0'.$_GET['Edit'];
}

awis_Debug(1,$SQL);
$rsMBH = awisOpenRecordset($con, $SQL);
$rsMBHZeilen = $awisRSZeilen;

//**************************************
// Liste anzeigen
//**************************************
if($rsMBHZeilen > 1 OR !isset($_GET['Edit']))
{
	awis_FORM_FormularStart();	
	
	awis_FORM_ZeileStart();
	if(($Recht3010&256)==256)	// Hersteller zuordnen
	{
		$Icons[] = array('new','./mitbewerber_Main.php?cmdAktion=Details&Seite=Hersteller&Edit=0');
		awis_FORM_Erstelle_ListeIcons($Icons,34,-1);
	}
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBH']['MBH_HER_ID'],400);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBH']['MBH_BEMERKUNG'],($BSBreite<1024?250:400));
	awis_FORM_ZeileEnde();
	
	if(!$rsMBHZeilen>$MaxDSAnzahl)
	{
		$rsMBHZeilen=$MaxDSAnzahl;
	}
	for($rsMBHZeile=0;$rsMBHZeile<$rsMBHZeilen;$rsMBHZeile++)
	{
		awis_FORM_ZeileStart();
		
		if(($Recht3010&256)==256)	// Ändernrecht
		{
			$Icons = array();
			$Icons[0] = array('edit','./mitbewerber_Main.php?cmdAktion=Details&Seite=Hersteller&Edit='.$rsMBH['MBH_KEY'][$rsMBHZeile]);
			$Icons[1] = array('delete','./mitbewerber_Main.php?cmdAktion=Details&Seite=Hersteller&Del='.$rsMBH['MBH_KEY'][$rsMBHZeile]);
			awis_FORM_Erstelle_ListeIcons($Icons,34,($rsMBHZeile%2));
		}
	
		awis_FORM_Erstelle_ListenFeld('HER_BEZEICHNUNG',$rsMBH['HER_BEZEICHNUNG'][$rsMBHZeile],0,400,false,($rsMBHZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('MBH_BEMERKUNG',$rsMBH['MBH_BEMERKUNG'][$rsMBHZeile],0,($BSBreite<1024?250:400),false,($rsMBHZeile%2),'','');
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}
else 			// einen Datensatz anzeigen
{
	awis_FORM_FormularStart();

	echo '<input type=hidden name=txtMBH_KEY value='.(isset($rsMBH['MBH_KEY'][0])?$rsMBH['MBH_KEY'][0]:'').'>';
	echo '<input type=hidden name=txtMBH_MBW_KEY value='.$Param.'>';
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBH']['MBH_HER_ID'],150,'');
	$SQL = 'SELECT HER_ID,HER_BEZEICHNUNG FROM Hersteller WHERE BITAND(HER_VERWENDUNG,1)=1 ';
	$SQL .= ' AND HER_ID NOT IN (SELECT MBH_HER_ID FROM MitbewerbHersteller WHERE MBH_MBW_KEY=0'.$Param.' AND MBH_HER_ID <> 0'.(isset($rsMBH['MBH_HER_ID'][0])?$rsMBH['MBH_HER_ID'][0]:'').')';
	$SQL .= ' ORDER BY HER_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('MBH_HER_ID',(isset($rsMBH['MBH_HER_ID'][0])?$rsMBH['MBH_HER_ID'][0]:''),300,($Recht3010&256),$con,$SQL,'','','HER_BEZEICHNUNG','','','');
	$CursorFeld='txtMBH_HER_ID';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBH']['MBH_BEMERKUNG'],150,'');
	awis_FORM_Erstelle_TextFeld('MBH_BEMERKUNG',(isset($rsMBH['MBH_BEMERKUNG'][0])?$rsMBH['MBH_BEMERKUNG'][0]:''),80,0,($Recht3010&256),'','','');
	awis_FORM_ZeileEnde();
	
	awis_FORM_FormularEnde();
}




?>