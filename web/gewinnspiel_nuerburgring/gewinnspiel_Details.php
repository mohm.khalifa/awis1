<?php
global $AWISBenutzer;
global $AWISCursorPosition;

try {
    $TextKonserven = array();
    $TextKonserven[] = array('GWN', '*');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'Personalnummer');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht62000 = $AWISBenutzer->HatDasRecht(62000);

    if ($Recht62000 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    //Ajax beim Dokumentenladen ansto�en
    $Script = ' <script>';
    $Script .= ' $(document).ready(function(){' . PHP_EOL;
    $Script .= ' key_GWN_PERSNR(event); ' . PHP_EOL;
    $Script .= '});' . PHP_EOL;
    $Script .= '</script>';
    $Form->SchreibeHTMLCode($Script);

    $Form->SchreibeHTMLCode('<form name="frmGewinnspielNuerburgring" action="" method="POST" >');

    $LabelBreite = 220;
    $FeldBreite = 200;
    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Hinweistext($AWISSprachKonserven['GWN']['GWN_UEBERSCHRIFT'], awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);

    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_GEWINNFRAGE'], 1000, 'font-weight: bold;');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_ANTWORT'], $LabelBreite);
    $Form->Erstelle_SelectFeld('!GWN_ANTWORT', (isset($_POST['txtGWN_ANTWORT'])?$_POST['txtGWN_ANTWORT']:''), $FeldBreite, true, '', $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', explode('|', $AWISSprachKonserven['GWN']['GWN_LST_FAHRZEUGE']));
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Trennzeile('L');
    $Form->ZeileEnde();

    //Hidden-Feld, damit Abteilung/FilNr nicht verworfen wenn Fehler fliegt
    $Form->Erstelle_HiddenFeld('GWN_HERKUNFT_HIDDEN', isset($_POST['txtGWN_HERKUNFT'])?$_POST['txtGWN_HERKUNFT']:'');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Personalnummer'], $LabelBreite);
    $Form->AuswahlBox('!GWN_PERSNR', 'box1', '', 'GWN_Daten', 'txtGWN_HERKUNFT_HIDDEN', $FeldBreite, 200, (isset($_POST['sucGWN_PERSNR'])?$_POST['sucGWN_PERSNR']:''), 'T', true, '', '', '', '', '', '', '', 0);
    $Form->ZeileEnde();

    $Form->AuswahlBoxHuelle('box1');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_NAME'], $LabelBreite);
    $Form->Erstelle_TextFeld('!GWN_VORNAME', (isset($_POST['txtGWN_VORNAME'])?$_POST['txtGWN_VORNAME']:''), 200, $FeldBreite, true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_VORNAME'], $LabelBreite);
    $Form->Erstelle_TextFeld('!GWN_NAME', (isset($_POST['txtGWN_NAME'])?$_POST['txtGWN_NAME']:''), 200, $FeldBreite, true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_SHIRTGROESSE'], $LabelBreite);
    $Form->Erstelle_SelectFeld('!GWN_SHIRTGROESSE', (isset($_POST['txtGWN_SHIRTGROESSE'])?$_POST['txtGWN_SHIRTGROESSE']:''), $FeldBreite, true, '', $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', explode('|', $AWISSprachKonserven['GWN']['GWN_LST_SHIRTGROESSE']));
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_TELNR'], $LabelBreite);
    $Form->Erstelle_TextFeld('!GWN_TELNR', (isset($_POST['txtGWN_TELNR'])?$_POST['txtGWN_TELNR']:''), 200, $FeldBreite, true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_MAIL'], $LabelBreite);
    $Form->Erstelle_TextFeld('GWN_MAIL', (isset($_POST['txtGWN_MAIL'])?$_POST['txtGWN_MAIL']:''), 200, $FeldBreite, true);
    $Form->ZeileEnde();

    if (isset($_POST['cmdSpeichern_x'])) {
        include_once './gewinnspiel_speichern.php';
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['GWN']['GWN_TEILNAHMEBEDINGUNGEN'], 800, 'font-size: 10px');
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
    $Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241613");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}