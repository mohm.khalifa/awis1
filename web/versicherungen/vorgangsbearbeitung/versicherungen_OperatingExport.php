<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('VEP', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $SQL = 'SELECT * ';
    $SQL .= 'FROM versexportprotokoll INNER JOIN versjobid ';
    $SQL .= 'ON vep_jobid = vji_jobid ';

    if($DB->ErmittleZeilenAnzahl($SQL) >= 1)
    {
        $rsVEP = $DB->RecordSetOeffnen($SQL);

        $Form->Formular_Start();
        $Form->ZeileStart();

        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VEP']['VEP_JOBID'], 80, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VEP']['VEP_DATEINAME'], 220, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VEP']['VEP_PRUEFSUMME'], 300, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VEP']['VEP_DATUMSTART'], 170, '', false);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['VEP']['VEP_DATUMENDE'], 170, '', false);

        $Form->ZeileEnde();

        $VEPZeile = 0;

        while(!$rsVEP->EOF())
        {
            $Form->ZeileStart();
            $Form->Erstelle_ListenFeld('VEP_JOBID', $rsVEP->FeldInhalt('VEP_JOBID'), 0, 80, false, ($VEPZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VEP_DATEINAME', $rsVEP->FeldInhalt('VEP_DATEINAME'), 0, 220, false, ($VEPZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VEP_PRUEFSUMME', $rsVEP->FeldInhalt('VEP_PRUEFSUMME'), 0, 300, false, ($VEPZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VEP_DATUMSTART', $rsVEP->FeldInhalt('VEP_DATUMSTART'), 0, 170, false, ($VEPZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('VEP_DATUMENDE', $rsVEP->FeldInhalt('VEP_DATUMENDE'), 0, 170, false, ($VEPZeile % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsVEP->DSWeiter();
            $VEPZeile++;
        }
    }
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906231637");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>
