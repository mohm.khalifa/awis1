<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $Recht30000;

require_once('awisFilialen.inc');
require_once('register.inc.php');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$Email = false;
	$TextKonserven = array();
	$TextKonserven[]=array('PMA','*');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','Personalnummer');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','FalschePerNr');
	$TextKonserven[]=array('Wort','KeineMail');
	$TextKonserven[]=array('Wort','IdentPruefung');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_reset');
	$TextKonserven[]=array('Wort','NameVoll');
	$TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$TextKonserven[]=array('Liste', 'lst_MA_Entwicklung');
	
	
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht30000 = $AWISBenutzer->HatDasRecht(30000);

	if($Recht30000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	
	
	$Form->DebugAusgabe(1,$_POST);

		$Form->SchreibeHTMLCode('<form name="frmProtokollMADetails" action="./protokoll_MA_Main.php?cmdAktion=Suche" method="POST"  enctype="multipart/form-data">');
		
		$Form->Formular_Start();
		
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_MITARBEITER'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Personalnummer'].':',200);
		$Form->Erstelle_TextFeld('!PMA_PERSNR','',6,240,true,'','','','T','L','','',8);	
		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['NameVoll'].':',200);
		$Form->Erstelle_TextFeld('!PMA_NAMEMA','',10,80,true,'','','','T','L','','',40);
		$Form->Erstelle_TextFeld('!PMA_VORNAMEMA','',10,150,true,'','','','T','L','','',40);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FUNKTION'].':',200);
		$Form->Erstelle_TextFeld('!PMA_FUNCMA','',15,240,true,'','','','T','L','','',40);
		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_ABTEILUNG'].':',200);
		$Form->Erstelle_TextFeld('!PMA_ABTEILUNG','',15,150,true,'','','','T','L','','',40);
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_VORGESETZTER'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['NameVoll'].':',200);
		$Form->Erstelle_TextFeld('!PMA_NAMEVO','',10,80,true,'','','','T','L','','',40);
		$Form->Erstelle_TextFeld('!PMA_VORNAMEVO','',10,145,true,'','','','T','L','','',40);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FUNKTION'].':',200);
		$Form->Erstelle_TextFeld('!PMA_FUNCVO','',15,150,true,'','','','T','L','','',40);
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FRAGE1'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_UEBERSCHRIFT1'], 800, 'font-weight:bold;font-size:0.8em');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT1','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_UEBERSCHRIFT2'], 800, 'font-weight:bold;font-size:0.8em');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT2','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FRAGE2'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT3','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FRAGE3'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT4','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FRAGE4'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT5','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_UEBERSCHRIFT3'], 800, 'font-weight:bold;font-size:0.8em');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT6','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FRAGE5'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_UEBERSCHRIFT4'].':', 800, 'font-weight:bold;font-size:0.8em');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT7','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_UEBERSCHRIFT5'].':', 800, 'font-weight:bold;font-size:0.8em');
		$Form->ZeileEnde();
				
		/*
		$Form->ZeileStart();
		$Form->Erstelle_Checkbox('CHK_ANSTEIGEND', '', 10,true);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_ANSTEIGEND'], 240, 'font-weight:bold;font-size:0.8em');
		$Form->Erstelle_Checkbox('CHK_GLEICHBLEIBEND', '', 10,true);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_GLEICHBLEIBEND'], 240, 'font-weight:bold;font-size:0.8em');
		$Form->Erstelle_Checkbox('CHK_NACHLASSEND', '', 10,true);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_NACHLASSEND'], 240, 'font-weight:bold;font-size:0.8em');
		$Form->ZeileEnde();
		*/
		
		$Form->Trennzeile('O');
		$Form->ZeileStart();
		$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_MA_Entwicklung']);
		$Form->Erstelle_SelectFeld('ENTWICK','', 700,true,'','', '', '','',$Daten);
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PMA']['PMA_FRAGE6'], 800, 'font-weight:bolder');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextArea('!PMA_ANTWORT8','',6,75,2,true,'','');
		$Form->ZeileEnde();
		
		$Form->Trennzeile('O');
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		//$LinkPDF = '/berichte/drucken.php?XRE=40&ID='.base64_encode($Param = 'PMA_KEY=' . urlencode('=~') .'TEST');
		//$Form->Schaltflaeche('href', 'cmdDrucken',$LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');
		$Form->Schaltflaeche('image', 'cmdDrucken', './protokoll_senden.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'M');
		$Form->Schaltflaeche('image', 'cmdReset', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_reset'], 'R');
		$Form->SchaltflaechenEnde();

		$Form->SchreibeHTMLCode('</form>');
		
		

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
		
	
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>