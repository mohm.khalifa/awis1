<?php
/**
 * Suchmaske f�r Parameterhistorie
 *
 * @author Tamara Bannert
 * @copyright ATU
 * @version 201408
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
try 
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('PRH','%');
    $TextKonserven[] = array('Wort','Auswahl_ALLE');
    $TextKonserven[] = array('Wort','txt_BitteWaehlen');
    $TextKonserven[] = array('Wort','AuswahlSpeichern');
    $TextKonserven[] = array('Wort','ttt_AuswahlSpeichern');
    $TextKonserven[] = array('Wort','lbl_hinzufuegen');
    $TextKonserven[] = array('Wort','lbl_suche');
    $TextKonserven[] = array('Wort','lbl_zurueck');
    $TextKonserven[] = array('Wort','lbl_weiter');
    $TextKonserven[] = array('Wort','lbl_hilfe');
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    
    $Recht33000 = $AWISBenutzer->HatDasRecht(33000);
    if ($Recht33000 == 0) 
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
        die();
    }
    
    
    
    /**
     * ********************************************
     * * Eingabemaske
     * *********************************************
     */
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Parameterhistorie'));
    
    if (! isset($Param['SPEICHERN'])) {
        $Param['SPEICHERN'] = 'off';
    }
    

    $Form->Formular_Start();
    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./parameterhistorie_Main.php?cmdAktion=Details>");
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PRH']['PRH_DATUM'] . ' von:', 190);
    $Form->Erstelle_TextFeld('*PRH_DATUM_VON', ($Param['SPEICHERN'] == 'on' ? $Param['PRH_DATUM_VON'] : ''), 25, 230, true, '', '', '', 'D', 'L');
    
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PRH']['PRH_DATUM'] . ' bis:', 90);
    $Form->Erstelle_TextFeld('*PRH_DATUM_BIS', ($Param['SPEICHERN'] == 'on' ? $Param['PRH_DATUM_BIS'] : ''), 25, 230, true, '', '', '', 'D', 'L');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PRH']['PRH_TABELLE'].':',190);
    $SQL = 'SELECT TABLE_NAME AS TABLEID, TABLE_NAME AS TABELLE';
    $SQL .= ' FROM ZZZ_TEST_COND_HISTORIE';
    $SQL .= ' GROUP BY TABLE_NAME';
    $Form->Erstelle_SelectFeld('*PRH_TABELLE', ($Param['SPEICHERN'] == 'on' ? $Param['PRH_TABELLE'] : ''), '40:270',true,$SQL,'','','','',array(array('',$AWISSprachKonserven['Wort']['Auswahl_ALLE'])));
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PRH']['PRH_ID']. ' :', 190);
    $Form->Erstelle_TextFeld('*PRH_ID', ($Param['SPEICHERN'] == 'on' ? $Param['PRH_ID'] : ''), 40, 250, true);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PRH']['PRH_AKTION'] . ' :', 190);
    $Form->Erstelle_TextFeld('*PRH_AKTION', ($Param['SPEICHERN'] == 'on' ? $Param['PRH_AKTION'] : ''), 40, 250, true);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PRH']['PRH_DETAIL'] . ' :', 190);
    $Form->Erstelle_TextFeld('*PRH_DETAIL', ($Param['SPEICHERN'] == 'on' ? $Param['PRH_DETAIL'] : ''), 40, 250, true);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['PRH']['PRH_BEARBEITER'] . ' :', 190);
    $Form->Erstelle_TextFeld('*PRH_BEARBEITER', ($Param['SPEICHERN'] == 'on' ? $Param['PRH_BEARBEITER'] : ''), 40, 250, true);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 190);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param['SPEICHERN'] == 'on' ? 'on' : ''), 30, true, 'on', '', $AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
    $Form->ZeileEnde();
    
    $Form->Formular_Ende();
  
    
    // ************************************************************
    // * Schaltfl�chen
    // ************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    
    $Form->SchaltflaechenEnde();
    $Form->SchreibeHTMLCode("</form>");
    if ($AWISCursorPosition != '') 
    {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();";
        echo '</Script>';
    }
} 
catch (awisException $ex) 
{
    if ($Form instanceof awisFormular) 
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201405091111");
    } 
    else 
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} 
catch (Exception $ex) 
{
    if ($Form instanceof awisFormular) 
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201405091111");
    } 
    else 
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>