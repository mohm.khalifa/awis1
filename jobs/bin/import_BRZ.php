<?php
require_once('awisBRZImport.inc');

try {
    $Benutzer = 'awis_jobs';
    $DebugLevel = 0;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $PIP = new awisBRZImport($Benutzer);
    $PIP->DebugLevel($DebugLevel);
    $PIP->StarteImport();

    $PIP->_Werkzeuge->EMail($PIP->EMAIL_Infos, 'OK PERSONALIMPORT BRZ', 'Habe ' . $PIP->AnzImp . 'Datens�tze verarbeitet');
} catch (awisException $ex) {
    $PIP->debugAusgabe('FEHLER: ' . $ex->getMessage(), 0, 'ERROR');
    $PIP->debugAusgabe('SQL: ' . $ex->getSQL());
    $PIP->_Werkzeuge->EMail($PIP->EMAIL_Infos, 'ERROR PERSONALIMPORT BRZ', $ex->getMessage() . $ex->getSQL() . PHP_EOL . ' Bitte Log pr�fen. Ggf. Debuglevel auf 999 setzen und erneut ausf�hren.');
} catch (Exception $ex) {
    $PIP->debugAusgabe('FEHLER: ' . $ex->getMessage(), 0, 'ERROR');
    $PIP->_Werkzeuge->EMail($PIP->EMAIL_Infos, 'ERROR PERSONALIMPORT BRZ', $ex->getMessage() . PHP_EOL . 'Bitte Log pr�fen. Ggf. Debuglevel auf 999 setzen und erneut ausf�hren.');
}
?>

