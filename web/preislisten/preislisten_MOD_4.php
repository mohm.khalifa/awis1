<?php
/**
 * Pr�fmodul: 	4
 * Zweck:		Dieses Modul pr�ft Datumsbereiche in einer Preisliste
 *
 * Parameter:
 *
 *
 */

global $con;					// Verbindung zum AWIS Server
global $comcon;					// Verbindung zum COM Server
global $PreisListe;				// Preislisten-ID
global $TransaktionsID;			// Transaktions-ID
global $rsPTA;					// Testarten
global $PTAZeile;				// Aktuelle Zeile in den Testarten-RS
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

//*****************************************************************************
// Parameter lesen
//*****************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY AND PLP_KEY = PPP_PLP_KEY';
$SQL .= ' WHERE PLP_ID_FPLB=' . $PreisListe;
$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

$Params=array();

$rsPAR = awisOpenRecordset($comcon,$SQL);
$rsPARZeilen = $awisRSZeilen;
if($rsPARZeilen>0)
{
	for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
	{
		$Params[$rsPAR['PTP_PARAMETER'][$PARZeile]]=($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]);
	}
}

//*****************************
// Historie schreiben
//*****************************
$SQL = ' INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' (PPH_PLP_KEY,PPH_TRANSAKTIONSID,PPH_BEGINN,PPH_USER,PPH_STATUS)';
$SQL .= ' VALUES(' . $rsPTA['PLP_KEY'][$PTAZeile] . '';
$SQL .= ', ' . $TransaktionsID ;
$SQL .= ', SYSDATE';
$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
$SQL .= ', 0';
$SQL .= ')';

if(!awisExecute($comcon,$SQL))
{
	echo '<span class=HinweisTex>Fehler: '. $SQL . '</span>';
}
$rsPPH = awisOpenRecordset($comcon,'SELECT MAX(PPH_Key) AS KEY FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $TransaktionsID . ' AND PPH_User=\'' . $AWISBenutzer->BenutzerName() . '\'');
$PPHKey = $rsPPH['KEY'][0];
unset($rsPPH);

$TestErgebnis = 1;	// Erfolg als Standard


//###############################################################################
//###############################################################################
//########################   B E G I N N   T E S T S ############################
//###############################################################################
//###############################################################################


$AnzAuff=0;

// Aktuelles Datum ermitteln
$SQL = ' SELECT PLP_TRANSAKTIONSSTATUS.*, TO_CHAR(PTS_GUELTIG_VON,\'RRRRMMDD\') AS NEUVON, TO_CHAR(PTS_GUELTIG_BIS,\'RRRRMMDD\') AS NEUBIS FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS WHERE PTS_TRANSAKTIONSID='.$TransaktionsID;
$rsDatum = awisOpenRecordset($comcon,$SQL);


//*****************************************************************************
// TEST 1
//*****************************************************************************

$SQL = 'SELECT PLT_ARTNR, PLT_TRANSAKTIONSID, PLT_ID_FPLB, PLT_GUELTIG_VON, PLT_GUELTIG_BIS ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_ID_FPLB=' . $PreisListe . '';
$SQL .= ' AND ( ';
$SQL .= '     PLT_GUELTIG_VON <> ';
$SQL .= '         (SELECT * FROM (SELECT PLT_GUELTIG_VON FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_ID_FPLB=' . $PreisListe . ' GROUP BY PLT_GUELTIG_VON ORDER BY COUNT(*) DESC) WHERE ROWNUM=1)';
$SQL .= '     OR PLT_GUELTIG_BIS <> ';
$SQL .= '         (SELECT * FROM (SELECT PLT_GUELTIG_BIS FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_ID_FPLB=' . $PreisListe . ' GROUP BY PLT_GUELTIG_BIS ORDER BY COUNT(*) DESC) WHERE ROWNUM=1)';
$SQL .= ')';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>: Sind alle G�ltigkeitsbereiche in der Liste identisch?</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;vermutlicher Datumsbereich: <b>' . substr($rsDatum['PTS_GUELTIG_VON'][0],0,10) . '</b> bis <b>' . substr($rsDatum['PTS_GUELTIG_BIS'][0],0,10) . '</b><br>';

for($TSTZeile=0;$TSTZeile<$rsTSTZeilen;$TSTZeile++)
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Artikel ' . $rsTST['PLT_ARTNR'][$TSTZeile] . '. G�ltig von '.substr($rsTST['PLT_GUELTIG_VON'][$TSTZeile],0,10).' bis '.substr($rsTST['PLT_GUELTIG_BIS'][$TSTZeile],0,10).'. -> <font color=red>Fehler</font>' ;;

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'Falsches G�ltigkeitsdatum bei Artikel ' . $rsTST['PLT_ARTNR'][$TSTZeile] . '. G�ltig von '.substr($rsTST['PLT_GUELTIG_VON'][$TSTZeile],0,10).' bis '.substr($rsTST['PLT_GUELTIG_BIS'][$TSTZeile],0,10).'.\'' ;
	$SQL .= ', 1';
	$SQL .= ', \'' . $rsTST['PLT_ARTNR'][$TSTZeile] . '\')' ;
	
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	$TestErgebnis = 2;		// Fehler
	$AnzAuff++;
}

flush();
unset($rsTST);



//*****************************************************************************
// TEST 2
//*****************************************************************************

$SQL = 'SELECT * FROM (SELECT PLP_TRANSAKTIONSSTATUS.*, TO_CHAR(PTS_GUELTIG_VON,\'RRRRMMDD\') AS AKTVON, TO_CHAR(PTS_GUELTIG_BIS,\'RRRRMMDD\') AS AKTBIS FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS WHERE PTS_ID_FPLB=' . $PreisListe . ' AND PTS_TRANSAKTIONSID<'.$TransaktionsID . ' ORDER BY PTS_TRANSAKTIONSID DESC) WHERE ROWNUM = 1';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 2</b>: G�ltigkeitsbereich im Vergleich zur Vorg�ngerliste</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;Datumsbereich der neuen Liste: <b>' . substr($rsDatum['PTS_GUELTIG_VON'][0],0,10) . '</b> bis <b>' . substr($rsDatum['PTS_GUELTIG_BIS'][0],0,10) . '</b><br>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;Datumsbereich der aktuellen Liste: <b>' . substr($rsTST['PTS_GUELTIG_VON'][0],0,10) . '</b> bis <b>' . substr($rsTST['PTS_GUELTIG_BIS'][0],0,10) . '</b><br>';
flush();

// Neue Liste darf nicht vor der aktuellen beginnen.
if($rsDatum['NEUVON'][0] < $rsTST['AKTVON'][0])
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Aktuelle Liste beginnt am ' . substr($rsTST['PTS_GUELTIG_VON'][0],0,10) . '. Die neue Liste soll ab dem ' . substr($rsDatum['PTS_GUELTIG_BIS'][0],0,10) . ' gelten. -> <font color=red>Fehler</font>';

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'Aktuelle Liste beginnt am ' . substr($rsTST['PTS_GUELTIG_VON'][0],0,10) . '. Die neue Liste soll ab dem ' . substr($rsDatum['PTS_GUELTIG_VON'][0],0,10) . ' gelten.\'' ;
	$SQL .= ', 2';
	$SQL .= ', null)' ;
	
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	$TestErgebnis = 2;		// Fehler
	$AnzAuff++;
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Aktuelle Liste beginnt am ' . substr($rsTST['PTS_GUELTIG_VON'][0],0,10) . '. Die neue Liste soll ab dem ' . substr($rsDatum['PTS_GUELTIG_VON'][0],0,10) . ' gelten. -> <font color=green>I.O.</font>';
}


//*****************************************************************************
// TEST 3
// Neue Liste darf nicht NACH der aktuellen beginnen.
//*****************************************************************************

echo '<br><br>&nbsp;&nbsp;<b>TEST 3</b>: Neue Liste darf keine G�ltigkeitsdatums-L�cke aufweisen</b><br>';
flush();

$EndeAkt = mktime(0,0,0,substr($rsTST['AKTBIS'][0],4,2),substr($rsTST['AKTBIS'][0],6,2),substr($rsTST['AKTBIS'][0],0,4));
$BeginnNeu = mktime(0,0,0,substr($rsDatum['NEUVON'][0],4,2),substr($rsDatum['NEUVON'][0],6,2),substr($rsDatum['NEUVON'][0],0,4));

if($BeginnNeu>$EndeAkt AND ($BeginnNeu-$EndeAkt)>(60*60*24))
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Aktuelle Liste endet am ' . substr($rsTST['PTS_GUELTIG_BIS'][0],0,10) . '. Die neue Liste soll ab dem ' . substr($rsDatum['PTS_GUELTIG_VON'][0],0,10) . ' gelten. -> <font color=red>Fehler</font>';

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'Aktuelle Liste endet am ' . substr($rsTST['PTS_GUELTIG_BIS'][0],0,10) . '. Die neue Liste soll ab dem ' . substr($rsDatum['PTS_GUELTIG_VON'][0],0,10) . ' gelten.\'' ;
	$SQL .= ', 3';
	$SQL .= ', null)' ;
	
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	$TestErgebnis = 2;		// Fehler
	$AnzAuff++;
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Aktuelle Liste endet am ' . substr($rsTST['PTS_GUELTIG_BIS'][0],0,10) . '. Die neue Liste soll ab dem ' . substr($rsDatum['PTS_GUELTIG_VON'][0],0,10) . ' gelten. -> <font color=green>I.O.</font>';
}
flush();
unset($rsTST);


//*****************************************************************************
// TEST 4
// G�ltig BIS darf nicht vor HEUTE liegen
//*****************************************************************************

echo '<br><br>&nbsp;&nbsp;<b>TEST 4</b>: Neue Liste darf nicht vor dem heutigen Datum enden.</b><br>';

$EndeNeu = mktime(0,0,0,substr($rsDatum['NEUBIS'][0],4,2),substr($rsDatum['NEUBIS'][0],6,2),substr($rsDatum['NEUBIS'][0],0,4));
if($EndeNeu<mktime(0,0,0,date('m'),date('d'),date('Y')))
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;NeueListe endet am ' . substr($rsDatum['PTS_GUELTIG_BIS'][0],0,10) . '. Dieses Datum liegt in der Vergangenheit. -> <font color=red>Fehler</font>';

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT,PAU_DATEN)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'Neue Liste endet am ' . substr($rsDatum['PTS_GUELTIG_BIS'][0],0,10) . '. Dieses Datum liegt in der Vergangenheit.\'' ;
	$SQL .= ', 4';
	$SQL .= ', null)' ;
	
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	$TestErgebnis = 2;		// Fehler
	$AnzAuff++;
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;Neue Liste endet am ' . substr($rsDatum['PTS_GUELTIG_BIS'][0],0,10) . '. -> <font color=green>I.O.</font>';
}
flush();
unset($rsTST);






// Testbewertung

if($TestErgebnis==1)
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: Keine Fehler gefunden. Bewertung: <font color=green>I.O.</font></div>';
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: ' . $AnzAuff . ' Fehler gefunden. Bewertung: <font color=red>Fehler.</font></div>';
}


//###############################################################################
//###############################################################################
//##########################   E N D E   T E S T S ##############################
//###############################################################################
//###############################################################################


//*********************************************
// Historie aktualisieren
//*********************************************
$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' SET PPH_ENDE = SYSDATE, PPH_STATUS = ' . $TestErgebnis;
$SQL .= ' WHERE PPH_Key = ' . $PPHKey;
if(!awisExecute($comcon,$SQL))
{
	awisErrorMailLink('PreislistePr�fung',1,$awisDBFehler);
}


unset($rsTST);

echo '<hr>';

?>