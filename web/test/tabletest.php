<?php
	function blow( &$table ) {
		for ( $c = 0; $c < sizeof( $table->colnames ); $c++ ) {
			for ( $r = 0; $r < sizeof( $table->rownames ); $r++ ) {
				$table->cell[ $c ][ $r ]->pc4p_set_margin( 5 );
			}
		}
	}

	function permutations ( $exp ) {
		$perms = array();
		for ( $i = 0; $i < pow( 2, $exp ); $i++ ) {
			$perms[] = explode( ",", substr( preg_replace( array( "/0/", "/1/" ), array( "single,", "double," ), str_pad( decbin( $i ), $exp, "0", STR_PAD_LEFT ) ), 0, -1 ) );
		}
		return $perms;
	}

	include "pc4p_init.inc";

	set_time_limit( 0 );

	$perms = permutations( 12 );

	define( "PC4PGRID", 5 );
	// define( "PC4PDEBUG", 1 );

	$PDF = &pc4p_create_pdf( array( "Author" => "Alexander Wirtz", "Title" => "PC4P-Test" ) );
	$PDF->pc4p_set_tempfile( "/tmp" );
	$PDF->pc4p_set_compatibility( "1.4" );
	$PDF->pc4p_set_compression( 9 );
	$PDF->pc4p_set_savepdf( "inline", "pc4p.pdf" );

	$Page = &pc4p_create_page( $PDF, "a4" );

	$master = &pc4p_create_object( $Page, "table" );
	$master->pc4p_create_tablematrix( 8 );

	$from   = 0;
	$length = 128;
//	$length = 1024;
	
	for ( $i = $from; $i < ( $from + $length ); $i++ ) {
		if ( ( $i - $from ) % 8 == 0 ) {
			$master->pc4p_add_tablerow();
		}

		$perm = $perms[ $i ];

		$table = &pc4p_create_object( $master->cell[ ( $i - $from ) % 8 ][ ( $i - $from ) / 8 ], "table");
		$table->pc4p_create_tablematrix( 2, 2 );
		blow( $table );
		$table->pc4p_set_cellborder( $table->cell[ 0 ][ 0 ],
			"all", 
			array( $perm[ 0 ], $perm[ 2 ], $perm[ 6 ], $perm[ 8 ] ),
			5.0
		);
		$table->pc4p_set_cellborder( $table->cell[ 1 ][ 0 ],
			"all",
			array( $perm[ 1 ], $perm[ 3 ], $perm[ 8 ], $perm[ 10 ] ),
			5.0
		);
		$table->pc4p_set_cellborder( $table->cell[ 0 ][ 1 ],
			"all", 
			array( $perm[ 2 ], $perm[ 4 ], $perm[ 7 ], $perm[ 9 ] ),
			5.0
		);
		$table->pc4p_set_cellborder( $table->cell[ 1 ][ 1 ],
			"all", 
			array( $perm[ 3 ], $perm[ 5 ], $perm[ 9 ], $perm[ 11 ] ),
			5.0
		);
	}

	blow( $master );

	$PDF->pc4p_draw();
?>
