<?php
/****************************************************************************
 *								            *
 *                          VERARBEITUNGSSCRIPT V1.0			    *
 *                          @author Stefan Oppl				    *
 *                       						    *
 *                      	@todo Versicherungsdaten verarbeiten        *
 *                                                              	    *
 *  									    *
 ****************************************************************************/

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');


class VersicherungenVerarbeiten
{
    private $_DB;
    private $_AWISBenutzer;
    private $_SQL;
    private $_FORM;
    private $_WaNr;
    private $_FilNr;
    private $_FilNrAkt;
    private $_WaNrAkt;
    private $_PfadExportVers = '/daten/daten/versicherungen/export/';
    private $_DateinameExportVers = 'export_vers_';
    private $_DateinameExportDebeka = 'export_debeka_';
    private $_DateinameExportDBVW = 'export_dbvw_';
    private $_DateinameExportWV = 'export_wue_';
    private $_DateinameExportWGV = 'export_wgv_';
    private $_DateinameExportAXA = 'export_axa_';
    private $_DateinameExportHDI = 'export_hdi_';
    private $_DateinameExportHUK = 'export_huk_';
    private $_DateinameExportVHV = 'export_vhv_';
    private $_DateinameExportRuV = 'export_ruv_';
    private $_DateinameExportLVM = 'export_lvm_';
    private $_DateinameExportCONTI = 'export_conti_';
    private $_DateinameExportOESA = 'export_oesa_';
    private $_DateinameExportZuerich = 'export_zuerich_';
    
    /**
     * Initialisierung
     */
    function __construct()
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_FORM = new awisFormular();
    }

    /**
     * Scheibentauschvorgaenge pruefen (sind AFG-Daten vorhanden,
     * passt das KFZ-Kennz mit den AFG-Daten ueberein und stimmt
     * der Betrag mit den AFG-Daten ueberein. Au�erdem darf
     * neben GLAS01 kein anderer Artikel auf dem WA stehen
     * (au�er Feinstaubplaketen) 
     */
    public function PruefSKZ_E()
    {
    // Pr�fen bei welchen Status noch selektiert werden muss
        $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vp.artkz, vp.bez, ';
        $this->_SQL .= 'vp.menge, vp.vk_preis, vp.umsatz, vp.rabattkenn ';
        $this->_SQL .= 'FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
        $this->_SQL .= 'ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
        $this->_SQL .= 'WHERE vs.vvs_ven_key = 0 ';
        $this->_SQL .= 'AND vs.vvs_vcn_key = 0 ';
        $this->_SQL .= 'AND vs.vvs_schadenkz = \'E\' ';
        $this->_SQL .= 'ORDER BY vp.filnr, vp.wanr, vp.position';

        // Pr�fen ob Vorg�nge zum pr�fen vorhanden sind

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
        {
            $this->SchreibeModulStatus('PruefSKZ_E', 'Pr�fe E Vorg�nge ...');

            $RsSKZ_E = $this->_DB->RecordSetOeffnen($this->_SQL);

            $this->_FilNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_E->FeldInhalt('FILNR'),false);
            $this->_WaNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_E->FeldInhalt('WANR'),false);

            //Z�hler f�r die Inidzierung der Arrays
            $i_GlasPos = 0;
            $i_SonstPos = 0;

            //Array in die alle Pos eines Vorgangs geschrieben werden
            //die in Ordnung sind
            $GlasPos = array('ARTNR'=>array(),'BETRAG'=>array());

            //Array in die alle Pos eines Vorgangs geschrieben werden die
            //genauer Untersucht werden m�ssen
            $SonstPos = array('ARTNR'=>array(),'BETRAG'=>array());

            while(!$RsSKZ_E->EOF())
            {
            // Werte in Variablen sichern
                $this->_FilNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_E->FeldInhalt('FILNR'),false);
                $this->_WaNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_E->FeldInhalt('WANR'),false);

                $Menge = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_E->FeldInhalt('MENGE'),false);
                $Betrag = $this->_DB->FeldInhaltFormat('N2',$RsSKZ_E->FeldInhalt('VK_PREIS'),false);

                //*******************************************************************************************
                //*******************************************************************************************
                if($this->_FilNr <> $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt || $this->_FilNr <> $this->_FilNrAkt && $this->_WaNr == $this->_WaNrAkt || $this->_FilNr == $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt)
                {
                //*******************************************************************************************
                //*******************************************************************************************
                // neuer Vorgang hat begonnen!!!


                // vorheringen Vorgang auswerten ob OK
                    if((count($SonstPos,1)-2)/2 <> 0)
                    {
                    // Falsche Pos sind vorhanden, pr�fen ob diese sich
                    // durch Preisnachlass wieder aufheben

                        $Summe = 0;

                        for($x=0;$x<(count($SonstPos,1)-2)/2;$x++)
                        {
                            $Summe = $Summe + $SonstPos['BETRAG'][$x];
                        }

                        if($Summe == 0)
                        {
                        // Die falschen Positionen heben sich durch
                        // z.B. Preissenkung auf und der Vorgang
                        // ist somit in Ordnung
                            $this->SetzeCheckStatus(100);
                            $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                        }
                        else
                        {
                        // Die falschen Positionen heben sich nicht
                        // durch z.B. Preissenkung auf und der Vorgang
                        // muss daher gesperrt werden
                            $this->SetzeStatus(1);
                        }

                    }
                    else
                    {
                    // Keine Falschen Positionen vorhanden
                    // Status sezten das Pr�fung keine Fehler ergab
                        $this->SetzeCheckStatus(100);
                        $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                    }
                    //*******************************************************************************************
                    //*******************************************************************************************
                    // Z�hlervariablen zur�cksetzen + Arrays leeren
                    $i_GlasPos = 0;
                    $i_SonstPos = 0;

                    //Array in die alle Pos eines Vorgangs geschrieben werden
                    //die in Ordnung sind
                    $GlasPos = array('ARTNR'=>array(),'BETRAG'=>array());

                    //Array in die alle Pos eines Vorgangs geschrieben werden die
                    //genauer Untersucht werden m�ssen
                    $SonstPos = array('ARTNR'=>array(),'BETRAG'=>array());

                    // Filnr und Wanr wieder sichern
                    $this->_FilNr = $this->_FilNrAkt;
                    $this->_WaNr = $this->_WaNrAkt;

                    //*******************************************************************************************
                    //*******************************************************************************************
                    // Erste Pos des Vorgangs Pr�fen und ins entsprechende Array schreiben
                    
                    if($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'GLAS01\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN04\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN01\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN02\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN03\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    else
                    {
                        $SonstPos['ARTNR'][$i_SonstPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $SonstPos['BETRAG'][$i_SonstPos] = $Menge * $Betrag;
                        $i_SonstPos++;
                    }

                }
                else
                {
                // noch gleicher Vorgang
                // weitere Pos des Vorgangs Pr�fen und ins entsprechende Array schreiben
                    if($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'GLAS01\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN04\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN01\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN02\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false) == '\'FEIN03\'')
                    {
                        $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                        $i_GlasPos++;
                    }
                    else
                    {
                        $SonstPos['ARTNR'][$i_SonstPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_E->FeldInhalt('ARTNR'),false);
                        $SonstPos['BETRAG'][$i_SonstPos] = $Menge * $Betrag;
                        $i_SonstPos++;
                    }

                }

                $RsSKZ_E->DSWeiter();
            }

            // Vorgang der zuletzt Gelesen wurde auswerten ob OK
            if((count($SonstPos,1)-2)/2 <> 0)
            {
            // Falsche Pos sind vorhanden, pr�fen ob diese sich
            // durch Preisnachlass wieder aufheben

                $Summe = 0;

                for($x=0;$x<(count($SonstPos,1)-2)/2;$x++)
                {
                    $Summe = $Summe + $SonstPos['BETRAG'][$x];
                }
                if($Summe == 0)
                {
                // Die falschen Positionen heben sich durch
                // z.B. Preissenkung auf und der Vorgang
                // ist somit in Ordnung
                    $this->SetzeCheckStatus(100);
                    $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                }
                else
                {
                // Die falschen Positionen heben sich nicht
                // durch z.B. Preissenkung auf und der Vorgang
                // muss daher gesperrt werden
                    $this->SetzeStatus(1);
                }

            }
            else
            {
            // Keine Falschen Positionen vorhanden
            // Status sezten das Pr�fung keine Fehler ergab
                $this->SetzeCheckStatus(100);
                $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
            }
        }

        $this->SchreibeModulStatus('PruefSKZ_E', 'Pr�fung abgeschlossen');

    }

    public function PruefSKZ_S()
    {
        try
        {
			// Pr�fen bei welchen Status noch selektiert werden muss
        	// Selectiert werden hier auch T-F�lle - Methode entscheidet dann ob T oder S Fall
	        $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vp.artkz, vp.bez, ';
	        $this->_SQL .= 'vp.menge, vp.vk_preis, vp.umsatz, vp.rabattkenn, vs.vvs_schadenkz ';
	        $this->_SQL .= 'FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
	        $this->_SQL .= 'ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
	        $this->_SQL .= 'WHERE vs.vvs_ven_key = 0 ';
	        $this->_SQL .= 'AND vs.vvs_vcn_key = 0 ';
	        $this->_SQL .= 'AND vs.vvs_schadenkz in (\'S\',\'T\') ';
	        $this->_SQL .= 'ORDER BY vp.filnr, vp.wanr, vp.position';
	        
	        // Pr�fen ob Vorg�nge zum pr�fen vorhanden sind

	        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
	        {
	            $this->SchreibeModulStatus('PruefSKZ_S', 'Pr�fe S Vorg�nge ...');
	
	            $RsSKZ_S = $this->_DB->RecordSetOeffnen($this->_SQL);
	            
	            $this->_FilNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_S->FeldInhalt('FILNR'),false);
	            $this->_WaNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_S->FeldInhalt('WANR'),false);
	
	            //Z�hler f�r die Inidzierung der Arrays
	            $i_RepPos = 0;
	            $i_SonstPos = 0;
	
	            //Array in die alle Pos eines Vorgangs geschrieben werden
	            //die in Ordnung sind
	            $RepPos = array('ARTNR'=>array(),'BETRAG'=>array());
	
	            //Array in die alle Pos eines Vorgangs geschrieben werden die
	            //genauer Untersucht werden m�ssen
	            $SonstPos = array('ARTNR'=>array(),'BETRAG'=>array());

	            while(!$RsSKZ_S->EOF())
	            {
	
	                echo($RsSKZ_S->FeldInhalt('FILNR'));
	                echo"  ";
	                echo($RsSKZ_S->FeldInhalt('WANR'));
	                echo"  ";
	                echo($RsSKZ_S->FeldInhalt('POSITION'));
	                echo"  ";
	                echo($RsSKZ_S->FeldInhalt('ARTNR'));
	                echo"  ";
	                echo($RsSKZ_S->FeldInhalt('MENGE'));
	                echo"  ";
	                echo($RsSKZ_S->FeldInhalt('VK_PREIS'));
	                echo"  ";
	                echo($RsSKZ_S->FeldInhalt('UMSATZ'));
	                echo"  ";
	                echo($RsSKZ_S->FeldInhalt('VVS_SCHADENKZ'));
	                echo"\n";
	
	                // Werte in Variablen sichern
	                $this->_FilNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_S->FeldInhalt('FILNR'),false);
	                $this->_WaNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_S->FeldInhalt('WANR'),false);
	
	                $Menge = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_S->FeldInhalt('MENGE'),false);
	                $Betrag = $this->_DB->FeldInhaltFormat('N2',$RsSKZ_S->FeldInhalt('VK_PREIS'),false);
	
	                //*******************************************************************************************
	                //*******************************************************************************************
	                if($this->_FilNr <> $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt || $this->_FilNr <> $this->_FilNrAkt && $this->_WaNr == $this->_WaNrAkt || $this->_FilNr == $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt)
	                {
		                
		                //*******************************************************************************************
		                //*******************************************************************************************
		                // neuer Vorgang hat begonnen!!!
		
		                // vorheringen Vorgang auswerten ob OK
	                    if((count($SonstPos,1)-2)/2 <> 0)
	                    {
		                    // Falsche Pos sind vorhanden, pr�fen ob diese sich
		                    // durch Preisnachlass wieder aufheben
	
	                        $Summe = 0;
	
	                        for($x=0;$x<(count($SonstPos,1)-2)/2;$x++)
	                        {
	                            $Summe = $Summe + $SonstPos['BETRAG'][$x];
	                        }
	
	                        if($Summe == 0)
	                        {
	                            // Die falschen Positionen heben sich durch
	                            // z.B. Preissenkung auf und der Vorgang
	                            // ist somit in Ordnung
	                            $this->SetzeCheckStatus(101);
	
	                            // Schadenskennzeichen auf S updaten, da auch T-F�lle mit
	                            // selektiert werden und wenn diese hier als S-F�lle durchgehen
	                            // muss das SKZ richtigerweise auf S ge�ndert werden
	                            $SQL = "UPDATE versvorgangsstatus ";
	                            $SQL .= "SET vvs_schadenkz = 'S' ";
	                            $SQL .= "WHERE vvs_filid = ".$this->_FilNr." ";
	                            $SQL .= "AND vvs_wanr = ".$this->_WaNr;
								
	                            $this->_DB->Ausfuehren($SQL,'',true);
	
	                            $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
	                            $this->SchreibeRepInDWH();
	                        }
	                        else
	                        {
	                            // Die falschen Positionen heben sich nicht
	                            // durch z.B. Preissenkung auf und der Vorgang
	                            // muss daher gesperrt werden
	                            $this->SetzeStatus(2);
	                        }
	                    }
	                    else
	                    {
	                        // Keine Falschen Positionen vorhanden
	                        // Status sezten das Pr�fung keine Fehler ergab
	                        $this->SetzeCheckStatus(101);
	
	                        // Schadenskennzeichen auf S updaten, da auch T-F�lle mit
	                        // selektiert werden und wenn diese hier als S-F�lle durchgehen
	                        // muss das SKZ richtigerweise auf S ge�ndert werden
	                        
	                        $BindeVariablen=array();
	                        $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
	                        $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
	                        
	                        $SQL = "UPDATE versvorgangsstatus ";
	                        $SQL .= "SET vvs_schadenkz = 'S' ";
	                        $SQL .= "WHERE vvs_filid = :var_N0_fil_id ";
	                        $SQL .= "AND vvs_wanr = :var_N0_vvs_wanr";
				
	                        $this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
	
	                        $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
	                        $this->SchreibeRepInDWH();
	                    }
	                    //*******************************************************************************************
	                    //*******************************************************************************************
	                    // Z�hlervariablen zur�cksetzen + Arrays leeren
	                    $i_RepPos = 0;
	                    $i_SonstPos = 0;
	
	                    //Array in die alle Pos eines Vorgangs geschrieben werden
	                    //die in Ordnung sind
	                    $RepPos = array('ARTNR'=>array(),'BETRAG'=>array());
	
	                    //Array in die alle Pos eines Vorgangs geschrieben werden die
	                    //genauer Untersucht werden m�ssen
	                    $SonstPos = array('ARTNR'=>array(),'BETRAG'=>array());
	
	                    // Filnr und Wanr wieder sichern
	                    $this->_FilNr = $this->_FilNrAkt;
	                    $this->_WaNr = $this->_WaNrAkt;
	
	                    //*******************************************************************************************
	                    //*******************************************************************************************
	                    // Erste Pos des Vorgangs Pr�fen und ins entsprechende Array schreiben
	                    if($this->_DB->FeldInhaltFormat('T',$RsSKZ_S->FeldInhalt('ARTNR'),false) == '\'1EIN69\'')
	                    {
	                        $RepPos['ARTNR'][$i_RepPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_S->FeldInhalt('ARTNR'),false);
	                        $RepPos['BETRAG'][$i_RepPos] = $Menge * $Betrag;
	                        $i_RepPos++;
	                    }
	                    else
	                    {
	                        $SonstPos['ARTNR'][$i_SonstPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_S->FeldInhalt('ARTNR'),false);
	                        $SonstPos['BETRAG'][$i_SonstPos] = $Menge * $Betrag;
	                        $i_SonstPos++;
	                    }
	
	                }
	                else
	                {
						// noch gleicher Vorgang
						// weitere Pos des Vorgangs Pr�fen und ins entsprechende Array schreiben
	                    if($this->_DB->FeldInhaltFormat('T',$RsSKZ_S->FeldInhalt('ARTNR'),false) == '\'1EIN69\'')
	                    {
	                        $RepPos['ARTNR'][$i_RepPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_S->FeldInhalt('ARTNR'),false);
	                        $RepPos['BETRAG'][$i_RepPos] = $Menge * $Betrag;
	                        $i_RepPos++;
	                    }
	                    else
	                    {
	                        $SonstPos['ARTNR'][$i_SonstPos] = $this->_DB->FeldInhaltFormat('T',$RsSKZ_S->FeldInhalt('ARTNR'),false);
	                        $SonstPos['BETRAG'][$i_SonstPos] = $Menge * $Betrag;
	                        $i_SonstPos++;
	                    }
	
	                }
	
	                $RsSKZ_S->DSWeiter();
	            }
	
	            // Vorgang der zuletzt Gelesen wurde auswerten ob OK
	            if((count($SonstPos,1)-2)/2 <> 0)
	            {
					// Falsche Pos sind vorhanden, pr�fen ob diese sich
					// durch Preisnachlass wieder aufheben
	                $Summe = 0;
	
	                for($x=0;$x<(count($SonstPos,1)-2)/2;$x++)
	                {
	                    $Summe = $Summe + $SonstPos['BETRAG'][$x];
	                }
	
	                if($Summe == 0)
	                {
		                // Die falschen Positionen heben sich durch
		                // z.B. Preissenkung auf und der Vorgang
		                // ist somit in Ordnung
	                    $this->SetzeCheckStatus(101);
	
	                    // Schadenskennzeichen auf S updaten, da auch T-F�lle mit
	                    // selektiert werden und wenn diese hier als S-F�lle durchgehen
	                    // muss das SKZ richtigerweise auf S ge�ndert werden
	                    
	                    $BindeVariablen=array();
	                    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
	                    $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
	                    
	                    $SQL = 'UPDATE versvorgangsstatus ';
	                    $SQL .= 'SET vvs_schadenkz = \'S\' ';
	                    $SQL .= 'WHERE vvs_filid = :var_N0_fil_id ';
	                    $SQL .= 'AND vvs_wanr = :var_N0_vvs_wanr';
	
	                    $this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
	
	                    $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
	                    $this->SchreibeRepInDWH();
	                }
	                else
	                {
		                // Die falschen Positionen heben sich nicht
		                // durch z.B. Preissenkung auf und der Vorgang
		                // muss daher gesperrt werden
	                    $this->SetzeStatus(2);
	                }
	            }
	            else
	            {
		            // Keine Falschen Positionen vorhanden
		            // Status sezten das Pr�fung keine Fehler ergab
	                $this->SetzeCheckStatus(101);
	
	                // Schadenskennzeichen auf S updaten, da auch T-F�lle mit
	                // selektiert werden und wenn diese hier als S-F�lle durchgehen
	                // muss das SKZ richtigerweise auf S ge�ndert werden
	                
	                $BindeVariablen=array();
	                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
	                $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
	                
	                $SQL = 'UPDATE versvorgangsstatus ';
	                $SQL .= 'SET vvs_schadenkz = \'S\' ';
	                $SQL .= 'WHERE vvs_filid = :var_N0_fil_id ';
	                $SQL .= 'AND vvs_wanr = :var_N0_vvs_wanr';
			
			        $this->_DB->Ausfuehren($SQL,'',true,$BindeVariablen);
	
	                $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
	                $this->SchreibeRepInDWH();
	            }
	        }
	
	        $this->SchreibeModulStatus('PruefSKZ_S', 'Pr�fung abgeschlossen');
	
	        }
	        catch(Exception $e)
	        {
	            echo ('Exception:'.$e->getMessage());
	            echo $this->_DB->LetzterSQL();
	            die();
	        }
	        catch(awisException $awis)
	        {
	            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
	            echo $this->_DB->LetzterSQL();
	            die();
	        }
    }

    public function SetzeSKZ_T()
    {
    // Funktion setzt Vorgaenge die neben der Steinschlagreparatur
    // noch andere (sich nicht aufhebende) Artikel haben auf SKZ T
        $this->_SQL = 'SELECT vvs_key, vvs_filid,vvs_wanr ';
        $this->_SQL .= 'FROM versvorgangsstatus ';
        $this->_SQL .= 'WHERE vvs_ven_key = 2';

        $this->SchreibeModulStatus('SetzeSKZ_T', 'Suche Vorgaenge ...');

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL))
        {
            $this->SchreibeModulStatus('SetzeSKZ_T', 'Setze SKZ T ...');

            $rsSetzeT = $this->_DB->RecordSetOeffnen($this->_SQL);

            while(!$rsSetzeT->EOF())
            {
                $BindeVariablen=array();
                $BindeVariablen['var_N0_vvs_key']=$rsSetzeT->FeldInhalt('VVS_KEY');
            	
            	$this->_SQL = 'UPDATE versvorgangsstatus ';
                $this->_SQL .= 'SET vvs_schadenkz = \'T\', ';
                $this->_SQL .= 'vvs_ven_key = 0 ';
                $this->_SQL .= 'WHERE vvs_key = :var_N0_vvs_key';

                if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)
                {
                //throw new awisException('',,$SQL,2);
                }

                $this->SetzeExportDWHStatus('VVS_SCHADENKZ', $rsSetzeT->FeldInhalt('VVS_FILID'), $rsSetzeT->FeldInhalt('VVS_WANR'));

                $rsSetzeT->DSWeiter();
            }
        }

        $this->SchreibeModulStatus('SetzeSKZ_T', 'Setzen abgeschlossen');

    }

    public function PruefSKZ_T()
    {

    // Pruefen bei welchen Status noch selektiert werden muss
        $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vp.artkz, vp.bez, ';
        $this->_SQL .= 'vp.menge, vp.vk_preis, vp.umsatz, vp.rabattkenn ';
        $this->_SQL .= 'FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
        $this->_SQL .= 'ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
        $this->_SQL .= 'WHERE vs.vvs_ven_key = 0 ';
        $this->_SQL .= 'AND vs.vvs_vcn_key = 0 ';
        $this->_SQL .= 'AND vs.vvs_schadenkz = \'T\' ';
        $this->_SQL .= 'ORDER BY vp.filnr, vp.wanr, vp.position';

        // Pruefen ob Vorgaenge zum pruefen vorhanden sind

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
        {
            $this->SchreibeModulStatus('PruefSKZ_T', 'Pruefe T Vorgaenge ...');

            $RsSKZ_T = $this->_DB->RecordSetOeffnen($this->_SQL);

            $this->_FilNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_T->FeldInhalt('FILNR'),false);
            $this->_WaNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_T->FeldInhalt('WANR'),false);

            //Flag das Anzeigt ob im T-Fall falsche Pos vorhanden sind auf false setzen
            $FlagFalschPos = false;

            while(!$RsSKZ_T->EOF())
            {
            // Werte in Variablen sichern
                $this->_FilNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_T->FeldInhalt('FILNR'),false);
                $this->_WaNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_T->FeldInhalt('WANR'),false);

                //*******************************************************************************************
                //*******************************************************************************************
                if($this->_FilNr <> $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt || $this->_FilNr <> $this->_FilNrAkt && $this->_WaNr == $this->_WaNrAkt || $this->_FilNr == $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt)
                {
                //*******************************************************************************************
                //*******************************************************************************************
                // neuer Vorgang hat begonnen!!!

                    if($FlagFalschPos === false)
                    {
                    // Es gibt keine falschen Positionen im T-Fall
                        $this->SetzeCheckStatus(102);
                        $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                    }
                    else
                    {
                    // Es gibt falsche Positionen im T-Fall (GLAS01/1EIN69)
                        $this->SetzeStatus(4);
                    }

                    //*******************************************************************************************
                    //*******************************************************************************************
                    // Zaehlervariablen zuruecksetzen + Arrays leeren
                    $FlagFalschPos = false;

                    // Filnr und Wanr wieder sichern
                    $this->_FilNr = $this->_FilNrAkt;
                    $this->_WaNr = $this->_WaNrAkt;

                    //*******************************************************************************************
                    //*******************************************************************************************
                    // Erste Pos des Vorgangs Pruefen

                    if($this->_DB->FeldInhaltFormat('T',$RsSKZ_T->FeldInhalt('ARTNR'),false) == '\'1EIN69\'')
                    {
                        $FlagFalschPos = true;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_T->FeldInhalt('ARTNR'),false) == '\'GLAS01\'')
                    {
                        $FlagFalschPos = true;
                    }
                    else
                    {

                    }

                }
                else
                {
                // noch gleicher Vorgang
                // weitere Pos des Vorgangs Pruefen

                    if($this->_DB->FeldInhaltFormat('T',$RsSKZ_T->FeldInhalt('ARTNR'),false) == '\'1EIN69\'')
                    {
                        $FlagFalschPos = true;
                    }
                    elseif($this->_DB->FeldInhaltFormat('T',$RsSKZ_T->FeldInhalt('ARTNR'),false) == '\'GLAS01\'')
                    {
                        $FlagFalschPos = true;
                    }
                    else
                    {

                    }

                }
                
                $RsSKZ_T->DSWeiter();
            }

            // Vorgang der zuletzt Gelesen wurde auswerten ob OK
            if($FlagFalschPos === false)
            {
            // Es gibt keine falschen Positionen im T-Fall
                $this->SetzeCheckStatus(102);
                $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
            }
            else
            {
            // Es gibt falsche Positionen im T-Fall (GLAS01/1EIN69)
                $this->SetzeStatus(4);
            }

        }

        $this->SchreibeModulStatus('PruefSKZ_T', 'Pr�fung abgeschlossen');
    }

    public function SperreSKZ_T()
    {
        $this->SchreibeModulStatus('SperreSKZ_T', 'Sperre T-Vorgaenge ...');

        $this->_SQL = 'UPDATE versvorgangsstatus ';
        $this->_SQL .= 'SET vvs_ven_key = 14 ';
        $this->_SQL .= 'WHERE vvs_schadenkz = \'T\' ';
        $this->_SQL .= 'AND vvs_vcn_key = 102 ';
        $this->_SQL .= 'AND vvs_versnr IN ';
        $this->_SQL .= '(SELECT vve_versnr ';
        $this->_SQL .= 'FROM versversicherungen ';
        $this->_SQL .= 'WHERE vve_kuerzel <> \'DEVK\')';

        if($this->_DB->Ausfuehren($this->_SQL)===false)
        {
        //throw new awisException('',,$SQL,2);
        }

        $this->SchreibeModulStatus('SperreSKZ_T', 'Sperrung Abgeschlossen');
    }

    public function PruefFalscheSKZ()
    {
    // Pr�fen bei welchen Status noch selektiert werden muss
        $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vp.artkz, vp.bez, ';
        $this->_SQL .= 'vp.menge, vp.vk_preis, vp.umsatz, vp.rabattkenn ';
        $this->_SQL .= 'FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
        $this->_SQL .= 'ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
        $this->_SQL .= 'WHERE vs.vvs_ven_key = 0 ';
        $this->_SQL .= 'AND vs.vvs_vcn_key = 0 ';
        $this->_SQL .= 'AND vs.vvs_schadenkz NOT IN (\'S\',\'T\',\'E\') ';
        $this->_SQL .= 'ORDER BY vp.filnr, vp.wanr, vp.position';

        // Pr�fen ob Vorg�nge zum pr�fen vorhanden sind

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
        {
            $this->SchreibeModulStatus('PruefFalscheSKZ', 'Ermittle falsche SKZ ...');

            $RsSKZ_Falsch = $this->_DB->RecordSetOeffnen($this->_SQL);


            while(!$RsSKZ_Falsch->EOF())
            {

                $this->_FilNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_Falsch->FeldInhalt('FILNR'),false);
                $this->_WaNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_Falsch->FeldInhalt('WANR'),false);

                $this->SetzeStatus(3);

                $RsSKZ_Falsch->DSWeiter();
            }
        }

        $this->SchreibeModulStatus('PruefFalscheSKZ', 'Pr�fung abgeschlossen');
    }

    public function SetzeSKZ_P()
    {
        $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr ';
        $this->_SQL .= 'FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
        $this->_SQL .= 'ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
        $this->_SQL .= 'INNER JOIN versversicherungen vve ON vve.vve_versnr = vs.vvs_versnr ';
        $this->_SQL .= 'WHERE vs.vvs_vcn_key = 102 ';
        $this->_SQL .= 'AND vs.vvs_schadenkz = \'T\' ';
        $this->_SQL .= 'AND vve.vve_kuerzel = \'DEVK\' ';
        $this->_SQL .= 'ORDER BY vp.filnr, vp.wanr, vp.position';

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
        {
            $this->SchreibeModulStatus('SetzeSKZ_P', 'Setze SKZ P ...');

            $RsSKZ_P = $this->_DB->RecordSetOeffnen($this->_SQL);

            $this->_FilNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_P->FeldInhalt('FILNR'),false);
            $this->_WaNr = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_P->FeldInhalt('WANR'),false);

            $FlagSetzeP = false;

            while(!$RsSKZ_P->EOF())
            {
            // Werte in Variablen sichern
                $this->_FilNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_P->FeldInhalt('FILNR'),false);
                $this->_WaNrAkt = $this->_DB->FeldInhaltFormat('N0',$RsSKZ_P->FeldInhalt('WANR'),false);

                //*******************************************************************************************
                //*******************************************************************************************
                if($this->_FilNr <> $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt || $this->_FilNr <> $this->_FilNrAkt && $this->_WaNr == $this->_WaNrAkt || $this->_FilNr == $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt)
                {
                //*******************************************************************************************
                //*******************************************************************************************
                // neuer Vorgang hat begonnen!!!

                    if($FlagSetzeP === true)
                    {
                        $this->SetzeCheckStatus(103);
                        $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                    }

                    //*******************************************************************************************
                    //*******************************************************************************************
                    // Zaehlervariablen zuruecksetzen + Arrays leeren
                    $FlagSetzeP = false;

                    // Filnr und Wanr wieder sichern
                    $this->_FilNr = $this->_FilNrAkt;
                    $this->_WaNr = $this->_WaNrAkt;

                    //*******************************************************************************************
                    //*******************************************************************************************
                    // Erste Pos des Vorgangs Pruefen

                    if(substr($this->_DB->FeldInhaltFormat('T',$RsSKZ_P->FeldInhalt('ARTNR'),false),1,3) == '1ES')
                    {
                        $FlagSetzeP = true;
                    }
                    else
                    {

                    }
                }
                else
                {
                // noch gleicher Vorgang
                // weitere Pos des Vorgangs Pruefen

                    if(substr($this->_DB->FeldInhaltFormat('T',$RsSKZ_P->FeldInhalt('ARTNR'),false),1,3) == '1ES')
                    {
                        $FlagSetzeP = true;
                    }
                    else
                    {

                    }
                }

                $RsSKZ_P->DSWeiter();
            }

            if($FlagSetzeP === true)
            {
                $this->SetzeCheckStatus(103);
                $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
            }

            $this->_SQL = 'UPDATE versvorgangsstatus ';
            $this->_SQL .= 'SET vvs_schadenkz = \'P\' ,vvs_vcn_key = 104, vvs_export_dwh = 1 + vvs_export_dwh ';
            $this->_SQL .= 'WHERE vvs_vcn_key = 103';

            if($this->_DB->Ausfuehren($this->_SQL)===false)
            {
            //throw new awisException('',,$SQL,2);
            }
        }

        $this->SchreibeModulStatus('SetzeSKZ_P', 'Setzen abgeschlossen');
    }
	
	public function SetzteStatusUmgebucht()
	{
	    $this->SchreibeModulStatus('SetzteStatusUmgebucht', 'Setze Status');
	    
		$this->_SQL  ='update versvorgangsstatus vvs';
		$this->_SQL .=' set vvs.vvs_vcn_key = 120';
		$this->_SQL .=' where vvs.vvs_key in (';
		$this->_SQL .=' SELECT vvs.vvs_key';
		$this->_SQL .=' FROM versvorgangsstatus vvs';
		$this->_SQL .=' INNER JOIN fkassendaten fka';
		$this->_SQL .=' ON vvs.vvs_vorgangnr     = fka.fka_aemnr';
		$this->_SQL .=' AND vvs.vvs_filid        = fka.fka_filid';
		$this->_SQL .=' AND vvs.vvs_wanr         = fka.fka_wanr';
		$this->_SQL .=' WHERE fka.fka_menge      = -1';
		$this->_SQL .=' AND vvs.vvs_vcn_key NOT IN (110, 111, 112, 113, 114, 115, 116, 117, 118, 119,120,124))';
		
		$this->_DB->Ausfuehren($this->_SQL);
		
		$this->SchreibeModulStatus('SetzteStatusUmgebucht', 'Setzen abgeschlossen');
		
	}
	
    public function BearbSKZ_E()
    {
    	try {
    		
    	
		//19.10.2010 OP: In WHERE Klausel (bei vvs_vcn_key not in) in Klammer "120" ergaenzt = Status fuer Umgebucht
		$this->_SQL  ='SELECT vfr.VORST_BETRAG,vek.SELBST_BETRAG,vvs.VVS_VERSNR, vvs.vvs_vorgangnr, vvs.vvs_filid, vvs.vvs_wanr, vvs.vvs_kfzkennz,vek.umsatz_gesamt ';
		$this->_SQL .=' FROM versvorgangsstatus vvs ';
		$this->_SQL .=' INNER JOIN v_vers_form_rechnungsdaten vfr';
		$this->_SQL .=' ON vvs.vvs_filid = vfr.filid AND vvs.vvs_wanr = vfr.wanr';
		$this->_SQL .=' INNER JOIN v_vers_exp_kopf vek ';
		$this->_SQL .=' on vvs.vvs_filid = vek.filid AND vvs.vvs_wanr = vek.wanr';
		$this->_SQL .=' WHERE vvs_ven_key in (5,6,7,8,10,11,12,13,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45) ';
		$this->_SQL .=' AND vvs_vcn_key not in (106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,124)';
		$this->_SQL .=' OR vvs_vcn_key = 100';

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
        {
            $this->SchreibeModulStatus('BearbSKZ_E', 'Bearbeite E Vorg�nge ...');

            $RsBearb_E = $this->_DB->RecordSetOeffnen($this->_SQL);

            while(!$RsBearb_E->EOF())
            {
                
                $KFZKennzKasse = $this->KennzeichenWandeln($this->KennzeichenWandeln($this->_DB->FeldInhaltFormat('T',$RsBearb_E->FeldInhalt('VVS_KFZKENNZ'),false)));
                $BetragKasse = $this->_DB->FeldInhaltFormat('N2',$RsBearb_E->FeldInhalt('UMSATZ_GESAMT'),false);
                $VorgangNr = $RsBearb_E->FeldInhalt('VVS_VORGANGNR');
                $this->_FilNr = $this->_DB->FeldInhaltFormat('N0',$RsBearb_E->FeldInhalt('VVS_FILID'),false);
                $this->_WaNr = $this->_DB->FeldInhaltFormat('N0',$RsBearb_E->FeldInhalt('VVS_WANR'),false);
                
				$this->_SQL  ='SELECT round(sum(((FGP_ANZAHL * FGP_OEMPREIS)*1.19)),2) as Betrag, ';
				$this->_SQL .='  fgk.fgk_kfzkennz, fgk.fgk_vorgangnr, fgk.fgk_zeitstempel, fgk.fgk_sb, fgk.FGK_MWST, fgk.FGK_VERSNR ';
				$this->_SQL .='  FROM fgpositionsdaten fgp INNER JOIN fgkopfdaten fgk ';
				$this->_SQL .='  ON fgp.fgp_vorgangnr = fgk.fgk_vorgangnr ';
				//$this->_SQL .='  AND fgp.fgp_zeitstempel = fgk.fgk_zeitstempel ';
				$this->_SQL .='  WHERE fgp.FGP_VORGANGNR = \''.$VorgangNr.'\' ';
				$this->_SQL .='  GROUP BY fgk.fgk_kfzkennz, fgk.fgk_vorgangnr, fgk_zeitstempel, fgk.fgk_sb, fgk.FGK_VERSNR,fgk.FGK_MWST';
                
                $rsFGKOPF = $this->_DB->RecordSetOeffnen($this->_SQL);
               
                if($rsFGKOPF->AnzahlDatensaetze() == 0)
                {
	                // Vorgang in den Vorgangstabellen nicht gefunden
	                // Vorgang auf Fehler setzen
                    $this->SetzeStatus(5);
                }
                else
                {
                	
                	// pruefen ob die Abwicklung zwischen Autoglas- & Kassendaten die gleiche ist
                	// z.B. Autoglas = Selbstzahler & Kasse = Versicherung
                	//		Autoglas = Versnr. 81 & Kasse = Versnr = 89
					
                                	
                	if($rsFGKOPF->FeldInhalt('FGK_VERSNR') != $RsBearb_E->FeldInhalt('VVS_VERSNR'))
                	{
                		// Fehler --> Abwicklung stimmmt zwischen Autoglas und Kasse nicht ueberein
                		$this->SetzeStatus(20);
                	}
                	else
                	{
                	    

                	    
                		$KFZKennzFG = $this->_DB->FeldInhaltFormat('T',$rsFGKOPF->FeldInhalt('FGK_KFZKENNZ'),false);
                		$BetragFG = $this->_DB->FeldInhaltFormat('N2',$rsFGKOPF->FeldInhalt('BETRAG'),false);
                		$Zeitstempel = $this->_DB->FeldInhaltFormat('T',$rsFGKOPF->FeldInhalt('FGK_ZEITSTEMPEL'),false);
                		$SB_AUTOGLAS = $this->_DB->FeldInhaltFormat('N0',$rsFGKOPF->FeldInhalt('FGK_SB'),false);
                		$SB_KASSE = $this->_DB->FeldInhaltFormat('N0',$RsBearb_E->FeldInhalt('SELBST_BETRAG'),false);
                		$MWST_AUTOGLAS = $this->_DB->FeldInhaltFormat('N2',$rsFGKOPF->FeldInhalt('FGK_MWST'),false);
                		$MWST_KASSE = $this->_DB->FeldInhaltFormat('N2',$RsBearb_E->FeldInhalt('VORST_BETRAG'),false);
                		
                		$FlagKFZKennz = false;
                		$FlagBetrag = false;
                		$FlagSB = false;
                		$FlagMWST = false;
                		
                		$BetragVergleich = $BetragFG-$BetragKasse;
                		
                		if($KFZKennzFG == $KFZKennzKasse)
                		{
                		    $FlagKFZKennz = true;
                		}
                		
                		// Differenzgrenze auf Wunsch von Hr. Koller abgeschafft (siehe A-012716)
                		// Betr�ge m�ssen jetzt auf den Cent genau passen OP 30.07.2014
                		//if($BetragVergleich >= -10 && $BetragVergleich <= 10)
                		//{
                		//	$FlagBetrag = true;
                		//}
                		if($BetragVergleich == 0)
                		{
                		    $FlagBetrag = true;
                		}
                		
                		if($SB_AUTOGLAS == $SB_KASSE)
                		{
                			//vergleicht die SB zwischen Autoglas und Kasse
                			$FlagSB = true;
                		}
                		
                		$this->_SQL  ='select *';
                		$this->_SQL .=' from fgkopfdaten';
                		$this->_SQL .=' where fgk_vorgangnr = \''.$VorgangNr.'\' ';
                		$this->_SQL .=' and fgk_zeitstempel > \'21.08.2015\'';
                		
                		if ($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
                		{
                			if($MWST_AUTOGLAS == $MWST_KASSE)
                			{
                				//vergleicht die MWST zwischen Autoglas und Kasse
                				$FlagMWST = true;
                			}
                		}
                		else 
                		{
                			$FlagMWST = true;
                		}

                		if($FlagBetrag == true && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == true)
                		{
                		    // Keine Fehler gefunden
                		    $this->SetzeCheckStatus(105);
                		    $this->SetzeStatus(0);
                		
                		    $BindeVariablen=array();
                		    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                		    $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
                		
                		    $this->_SQL = 'UPDATE versvorgangsstatus ';
                		    $this->_SQL .= 'SET vvs_datumfg = '.$Zeitstempel.' ';
                		    $this->_SQL .= 'WHERE vvs_filid = :var_N0_fil_id ';
                		    $this->_SQL .= 'AND vvs_wanr = :var_N0_vvs_wanr';
                		
                		    $this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen);
                		}
                		elseif($FlagBetrag == false && $FlagKFZKennz == false && $FlagSB == true && $FlagMWST == true)
                		{
                		    //FALSCH	Betrag 
                        	//FALSCH	KFZ-Kennzeichen 
                        	//OK		SB 
                        	//OK		MWST
                        	
                		    $this->SetzeStatus(8);
                		}
                		elseif($FlagBetrag == true && $FlagSB == true && $FlagKFZKennz == false && $FlagMWST == true)
                		{
                		    //OK		Betrag 
                        	//FALSCH	KFZ-Kennzeichen 
                        	//OK		SB 
                        	//OK		MWST
                        	
                		    $this->SetzeStatus(7);
                		}
                		elseif($FlagBetrag == false && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == true)
                		{
                		    //FALSCH	Betrag 
                        	//OK		KFZ-Kennzeichen 
                        	//OK		SB 
                        	//OK		MWST
                        	
                		    $this->SetzeStatus(6);
                		}
                		elseif($FlagSB == false && $FlagBetrag == true && $FlagKFZKennz == true && $FlagMWST == true)
                		{
                			//OK		Betrag 
                        	//OK		KFZ-Kennzeichen 
                        	//FALSCH	SB 
                        	//OK		MWST
                        	
                			$this->SetzeStatus(21);
                		}
                		elseif($FlagSB == false && $FlagBetrag == false && $FlagKFZKennz == true && $FlagMWST == true)
                		{
                			//FALSCH	Betrag 
                        	//OK		KFZ-Kennzeichen 
                        	//FALSCH	SB 
                        	//OK		MWST
                        	
                			$this->SetzeStatus(22);
                		}
                		elseif($FlagSB == false && $FlagBetrag == true && $FlagKFZKennz == false && $FlagMWST == true)
                		{
                			//OK		Betrag 
                        	//FALSCH	KFZ-Kennzeichen 
                        	//FALSCH	SB 
                        	//OK		MWST
                        	
                			$this->SetzeStatus(23);
                		}        		
                		elseif($FlagBetrag == false && $FlagKFZKennz == false && $FlagSB == false && $FlagMWST == true)
                		{
                		    //FALSCH	Betrag
                		    //FALSCH	KFZ-Kennzeichen
                		    //FALSCH	SB
                		    //OK		MWST
                		
                		    $this->SetzeStatus(24);
                		}
                		elseif($FlagBetrag == false && $FlagKFZKennz == false && $FlagSB == true && $FlagMWST == false)
                		{
                		    //FALSCH	Betrag
                		    //FALSCH	KFZ-Kennzeichen
                		    //OK		SB
                		    //FALSCH	MWST
                		
                		    $this->SetzeStatus(36);
                		}
                		elseif($FlagBetrag == false && $FlagKFZKennz == true && $FlagSB == false && $FlagMWST == false)
                		{
                		    //FALSCH	Betrag
                		    //OK	KFZ-Kennzeichen
                		    //FALSCH	SB
                		    //FALSCH	MWST
                		
                		    $this->SetzeStatus(34);
                		}
                		elseif($FlagBetrag == true && $FlagKFZKennz == false && $FlagSB == false && $FlagMWST == false)
                		{
                		    //OK		Betrag
                		    //FALSCH	KFZ-Kennzeichen
                		    //FALSCH	SB
                		    //FALSCH	MWST
                		
                		    $this->SetzeStatus(35);
                		}
                		elseif($FlagBetrag == false && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == false)
                		{
                		    //FALSCH	Betrag
                		    //OK		KFZ-Kennzeichen
                		    //OK		SB
                		    //FALSCH	MWST
                		
                		    $this->SetzeStatus(32);
                		}
                		elseif($FlagBetrag == true && $FlagKFZKennz == false && $FlagSB == true && $FlagMWST == false)
                		{
                		    //OK	Betrag
                		    //FALSCH		KFZ-Kennzeichen
                		    //OK		SB
                		    //FALSCH	MWST
                		
                		    $this->SetzeStatus(33);
                		}
                		elseif($FlagBetrag == true && $FlagKFZKennz == true && $FlagSB == false && $FlagMWST == false)
                		{
                		    //OK		Betrag
                		    //OK		KFZ-Kennzeichen
                		    //FALSCH	SB
                		    //FALSCH	MWST
                		
                		    $this->SetzeStatus(31);
                		}
                		elseif($FlagBetrag == true && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == false)
                		{
                		    //OK		Betrag
                		    //OK		KFZ-Kennzeichen
                		    //OK		SB
                		    //FALSCH	MWST
                		
                		    $this->SetzeStatus(30);
                		}
                		elseif($FlagSB == false && $FlagBetrag == false && $FlagKFZKennz == false && $FlagMWST == false)
                		{
                			//FALSCH	Betrag 
                        	//FALSCH	KFZ-Kennzeichen 
                        	//FALSCH	SB 
                        	//FALSCH	MWST
                        	
                			$this->SetzeStatus(37);
                		}
                		
                	}
                	
                }

                $RsBearb_E->DSWeiter();
            }
        }

        $this->SchreibeModulStatus('BearbSKZ_E', 'Bearbeitung abgeschlossen');
    	}
    	catch(Exception $e)
        {
            echo ('Exception:'.$e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }

    public function BearbKennzeichen()
    {
        $this->_SQL = 'SELECT vvs_filid, vvs_wanr, vvs_kfzkennz, vvs_ven_key, vvs_vcn_key ';
        $this->_SQL .= 'FROM versvorgangsstatus ';
        $this->_SQL .= 'WHERE vvs_ven_key in (5,6,7,8,21,22,23,24,30,31,32,33,34,35,36,37) ';
        $this->_SQL .= 'OR vvs_vcn_key in (101,104,105)';

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
        {
            $this->SchreibeModulStatus('BearbKennzeichen', 'Bearbeite Kennzeichen ...');

            $RsKennzeichen = $this->_DB->RecordSetOeffnen($this->_SQL);

            while(!$RsKennzeichen->EOF())
            {
                $Kennzeichen = $RsKennzeichen->FeldInhalt('VVS_KFZKENNZ');
                $FILNR = $RsKennzeichen->FeldInhalt('VVS_FILID');
                $WANR = $RsKennzeichen->FeldInhalt('VVS_WANR');

                $FormKennzeichen = $this->FormatiereKennzeichen($Kennzeichen,$FILNR,$WANR);

                $Status = $RsKennzeichen->FeldInhalt('VVS_VEN_KEY');
                $CheckStatus = $RsKennzeichen->FeldInhalt('VVS_VCN_KEY');

                if($FormKennzeichen === false)
                {
                //Kennzeichen nicht wandelbar
                    if($Status == 5)
                    {
                        $this->SetzeStatus(13, $FILNR, $WANR);
                    }
                    elseif($Status == 6)
                    {
                        $this->SetzeStatus(10, $FILNR, $WANR);
                    }
                    elseif($Status == 7)
                    {
                        $this->SetzeStatus(11, $FILNR, $WANR);
                    }
                    elseif($Status == 8)
                    {
                        $this->SetzeStatus(12, $FILNR, $WANR);
                    }
                    elseif($Status == 21)
                    {
                    	$this->SetzeStatus(25, $FILNR, $WANR);
                    }
                    elseif($Status == 22)
                    {
                    	$this->SetzeStatus(26, $FILNR, $WANR);
                    }
                    elseif($Status == 23)
                    {
                    	$this->SetzeStatus(27, $FILNR, $WANR);
                    }
                    elseif($Status == 24)
                    {
                    	$this->SetzeStatus(28, $FILNR, $WANR);
                    }
                    elseif($Status == 30)
                    {
                    	$this->SetzeStatus(38, $FILNR, $WANR);
                    }
                    elseif($Status == 31)
                    {
                        $this->SetzeStatus(39, $FILNR, $WANR);
                    }
                    elseif($Status == 32)
                    {
                        $this->SetzeStatus(40, $FILNR, $WANR);
                    }
                    elseif($Status == 33)
                    {
                        $this->SetzeStatus(41, $FILNR, $WANR);
                    }
                    elseif($Status == 34)
                    {
                        $this->SetzeStatus(42, $FILNR, $WANR);
                    }
                    elseif($Status == 35)
                    {
                        $this->SetzeStatus(43, $FILNR, $WANR);
                    }
                    elseif($Status == 36)
                    {
                        $this->SetzeStatus(44, $FILNR, $WANR);
                    }
                    elseif($Status == 37)
                    {
                        $this->SetzeStatus(45, $FILNR, $WANR);
                    }
                    else
                    {
                        $this->SetzeStatus(9, $FILNR, $WANR);
                    }
                }
                else
                {
                    if($CheckStatus == 101 || $CheckStatus == 102 || $CheckStatus == 104 || $CheckStatus == 105)
                    {
						// Bei diesen Vorg�ngen waren die Pr�fungen bisher in Ordnung
                        $this->SetzeCheckStatus(106, $FILNR, $WANR);
                        $this->SetzeExportDWHStatus('VVS_KFZKENNZ',$FILNR,$WANR);
                        
                        $BindeVariablen=array();
                        $BindeVariablen['var_T_vvs_kfzkennz']=$FormKennzeichen;
                        $BindeVariablen['var_N0_fil_id']=$FILNR;
                        $BindeVariablen['var_N0_vvs_wanr']=$WANR;
                        
                        $this->_SQL = 'UPDATE versvorgangsstatus ';
                        $this->_SQL .= 'SET VVS_KFZKENNZ = :var_T_vvs_kfzkennz, ';
                        $this->_SQL .= 'VVS_LAENDERSCHL = \'D\' ';
                        $this->_SQL .= 'WHERE VVS_FILID = :var_N0_fil_id ';
                        $this->_SQL .= 'AND VVS_WANR = :var_N0_vvs_wanr';

                        if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)
                        {
                        //throw new awisException('',,$SQL,2);
                        }
                    }
                    else
                    {
                    	// Bei diesen Vorg�ngen war min. eine Pr�fung fehlerhaft
                    	// Status braucht nicht ge�ndert werden da KFZ-Kennz i.O.
                        
                    	$this->SetzeExportDWHStatus('VVS_KFZKENNZ',$FILNR,$WANR);
                    	
                    	$BindeVariablen=array();
                        $BindeVariablen['var_T_vvs_kfzkennz']=$FormKennzeichen;
                        $BindeVariablen['var_N0_fil_id']=$FILNR;
                        $BindeVariablen['var_N0_vvs_wanr']=$WANR;
                    	
                        $this->_SQL = 'UPDATE versvorgangsstatus ';
                        $this->_SQL .= 'SET VVS_KFZKENNZ = :var_T_vvs_kfzkennz, ';
                        $this->_SQL .= 'VVS_LAENDERSCHL = \'D\' ';
                        $this->_SQL .= 'WHERE VVS_FILID = :var_N0_fil_id ';
                        $this->_SQL .= 'AND VVS_WANR = :var_N0_vvs_wanr';

                        if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)
                        {
                        //throw new awisException('',,$SQL,2);
                        }
                    }
                }

                $RsKennzeichen->DSWeiter();
            }
        }

        $this->SchreibeModulStatus('BearbKennzeichen', 'Bearbeitung abgeschlossen');
    }

    public function FormatiereKennzeichen($Kennzeichen,$FilID,$WANR)
    {
    // Variable die zur�ckgegeben wird (Standartwert: false)
        $Rueckgabe = false;

        //####################################################################
        //Alle Leerzeichen entfernen
        $suchen = array(' ');
        $ersetzen = array('');

        for($x=0;$x < count($suchen,0);$x++)
        {
            $Kennzeichen = str_replace($suchen[$x],$ersetzen[$x],$Kennzeichen);
        }
        //####################################################################

        $PosBindestrich = strpos($Kennzeichen,'-');

        // Ein Kennzeichen kann einfach Formatiert werden wenn
        // 1. ein Bindestrich vorhanden ist
        // 2. dieser Bindestrich nach dem Unterscheidungskennzeichen steht (Pos 1,2,3)
        // 3. gleich nach dem Bindestrich keine Zahl kommt
        if($PosBindestrich !== false && $PosBindestrich <= 3 && !is_numeric(substr($Kennzeichen,$PosBindestrich+1,1)))
        {
            $FormKennzeichen = '';
            $FlagNumeric = false;

            for($x=0;$x < strlen($Kennzeichen);$x++)
            {
                $Zeichen = substr($Kennzeichen,$x,1);
                if(is_numeric($Zeichen) === true && $FlagNumeric === false)
                {
                    $FormKennzeichen .= ' '.$Zeichen;
                    $FlagNumeric = true;
                }
                else
                {
                    $FormKennzeichen .= $Zeichen;
                }
            }

            $Rueckgabe = $FormKennzeichen;
        }
        else
        {
        //$this->_FilNr = 90;
        //$this->_WaNr = 264559;

        	$BindeVariablen=array();
        	$BindeVariablen['var_N0_fil_id']=$FilID;
        	$BindeVariablen['var_N0_wanr']=$WANR;
        
        	$this->_SQL = 'SELECT kunde_plz, gk_plz, atucard_plz ';
            $this->_SQL .= 'FROM V_VERS_VERARB_PLZ ';
            $this->_SQL .= 'WHERE fil_id = :var_N0_fil_id ';
            $this->_SQL .= 'AND wanr = :var_N0_wanr';

            $RsPLZ = $this->_DB->RecordSetOeffnen($this->_SQL,$BindeVariablen);

            if(!is_null($RsPLZ->FeldInhalt('KUNDE_PLZ')) && $RsPLZ->FeldInhalt('KUNDE_PLZ') != '')
            {
				// Wenn Kunden-PLZ gef�llt ist
                $PLZ = $RsPLZ->FeldInhalt('KUNDE_PLZ');
            }
            elseif(!is_null($RsPLZ->FeldInhalt('GK_PLZ')) && $RsPLZ->FeldInhalt('GK_PLZ') != '')
            {
				// Wenn Kunden-PLZ nicht gef�llt ist GK-PLZ nehmen
                $PLZ = $RsPLZ->FeldInhalt('GK_PLZ');
            }
            elseif(!is_null($RsPLZ->FeldInhalt('ATUCARD_PLZ')) && $RsPLZ->FeldInhalt('ATUCARD_PLZ') != '')
            {
				// Wenn Kunden-PLZ + GK-PLZ nicht gef�llt ATUCard-PLZ nehmen
                $PLZ = $RsPLZ->FeldInhalt('ATUCARD_PLZ');
            }
            else
            {
                $PLZ = '00000';
            }
            $BindeVariablen=array();
            $BindeVariablen['var_T_vkl_plz']=$PLZ;
            
            $this->_SQL = 'SELECT VKL_KFZKENNZ ';
            $this->_SQL .= 'FROM VERSKFZLEXIKON ';
            $this->_SQL .= 'WHERE VKL_PLZ = :var_T_vkl_plz ';
            $this->_SQL .= 'ORDER BY VKL_HIERARCHIE';


            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,$BindeVariablen) > 0)
            {
                $RsLkrKennz = $this->_DB->RecordSetOeffnen($this->_SQL,$BindeVariablen);

                $FlagGefunden = false;
                
                while(!$RsLkrKennz->EOF())
                {
                    if($RsLkrKennz->FeldInhalt('VKL_KFZKENNZ') == substr($Kennzeichen, 0, strlen($RsLkrKennz->FeldInhalt('VKL_KFZKENNZ'))))
                    {
                    // KZF-Kennzeichen kann gewandelt werden
                        $FlagGefunden = true;
                        $FormKennzeichen = $RsLkrKennz->FeldInhalt('VKL_KFZKENNZ');
                    }

                    $RsLkrKennz->DSWeiter();
                }

                if($FlagGefunden === true)
                {

                    $FlagNumeric = false;
                    $Laenge = strlen($FormKennzeichen);
                    $FormKennzeichen .= '-';

                    for($x=$Laenge;$x < strlen($Kennzeichen);$x++)
                    {
                        $Zeichen = substr($Kennzeichen,$x,1);
                        if(is_numeric($Zeichen) === true && $FlagNumeric === false)
                        {
                            $FormKennzeichen .= ' '.$Zeichen;
                            $FlagNumeric = true;
                        }
                        else
                        {
                            $FormKennzeichen .= $Zeichen;
                        }
                    }

                    $Rueckgabe = $FormKennzeichen;
                }
            }
        }

        return $Rueckgabe;
    }

    public function PruefeDopplung()
    {
    
        $this->SchreibeModulStatus('PruefeDopplung', 'Setze Status doppelter Rechnungsversand');
    
        // Vorg�nge auf doppelten Versand pruefen
        // 19.10.2010 OP: In Where Klausel bei vvs_vcn_key not in in Klammer "120" ergaenzt = Status fuer Umgebucht
        // 07.08.2015 BT: VVS_VEN_KEY 124 ergaenzt = Vorgang blockiert, doppelter Rechnungsversand
        $this->_SQL = 'SELECT vvs_filid, vvs_wanr ';
        $this->_SQL .= 'FROM versvorgangsstatus ';
        $this->_SQL .= 'WHERE vvs_vcn_key = 106 ';
        $this->_SQL .= 'OR vvs_vcn_key not in (106,110,111,112,113,114,115,116,117,118,119,120,124) ';
        $this->_SQL .= 'AND vvs_freigabe = 1';
    
        $rsDopplung = $this->_DB->RecordsetOeffnen($this->_SQL);
    
        
        
        while(!$rsDopplung->EOF())
        {
            $this->_FilNr = $rsDopplung->FeldInhalt('VVS_FILID');
            $this->_WaNr = $rsDopplung->FeldInhalt('VVS_WANR');
    
            $BindeVariablen=array();
            $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
            $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
            
            //Update auf VVS_VCN_KEY 124
            $this->_SQL  ='Update versvorgangsstatus ver set ver.vvs_ven_key = 29, ver.vvs_vcn_key = 0';
            $this->_SQL .=' Where ver.vvs_key in (';
            $this->_SQL .='   Select vvs.vvs_key';
            $this->_SQL .='   From versvorgangsstatus vvs';
            $this->_SQL .='   Where vvs.vvs_vorgangnr in (';
            $this->_SQL .='     Select vers.vvs_vorgangnr';
            $this->_SQL .='     From versvorgangsstatus vers';
            $this->_SQL .='     where vers.vvs_vcn_key in (107,108,109,110,111,112,113,114,115,116,117,118,119)';
            $this->_SQL .='   group by vers.vvs_vorgangnr)';
            $this->_SQL .='   and vvs.vvs_filid = :var_N0_fil_id';
            $this->_SQL .='   and vvs.vvs_wanr = :var_N0_vvs_wanr';
            $this->_SQL .=' )';
    
            $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
            $rsDopplung->DSWeiter();
    
        }
        $this->SchreibeModulStatus('PruefeDopplung', 'Ende Setze Status doppelter Rechnungsversand');
    }
    
    /**
     * Schreibt alle vorher erfolgreich gemachten Vorg�nge in eine Exporttabelle
     * und exportiert diese Daten dann in Dateien. F�r die beiden Versicherungen
     * (DBV-Winterthur & DEBEKA) gibt es jeweils eine eigenen Exportdatei. 
     * 
     * @version 1.0
     * @author Stefan Oppl
     * 
     * 02.03.2012 BT : Bei Feld "SB" im Kopfdatensatz die Kommastellen wegschneiden
     */
    public function ExportiereVorgaenge()
    {
        $this->SchreibeModulStatus('ExportiereVorg�nge', 'Suche unvollst�ndig Exportierte Vorg�nge');
    
        // Pruefen ob Vorgaenge vorhanden sind bei denen der Export
        // begonnen wurde aber nicht abgeschlossen werden konnte
        $this->_SQL = 'SELECT vvs_filid, vvs_wanr ';
        $this->_SQL .= 'FROM versvorgangsstatus ';
        $this->_SQL .= 'WHERE vvs_vcn_key = 110';

        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
        {
            $this->SchreibeModulStatus('ExportiereVorgaenge', 'L�sche unvollst�ndige...');
            // Es wurden unvollstaendig exportierte Vorgaenge gefunden
            // diese wieder aus der Exporttabelle loeschen und Status zuruecksetzen
            $rsFehlerExport = $this->_DB->RecordsetOeffnen($this->_SQL);

            while(!$rsFehlerExport->EOF())
            {
                $this->_FilNr = $rsFehlerExport->FeldInhalt('VVS_FILID');
                $this->_WaNr = $rsFehlerExport->FeldInhalt('VVS_WANR');
                
                $BindeVariablen=array();
                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                $BindeVariablen['var_N0_vek_wanr']=$this->_WaNr;

                $this->_SQL = 'DELETE ';
                $this->_SQL .= 'FROM versexport ';
                $this->_SQL .= 'WHERE vek_filid = :var_N0_fil_id ';
                $this->_SQL .= 'AND vek_wanr = :var_N0_vek_wanr';

                $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);

                $this->SetzeCheckStatus(106);

                $rsFehlerExport->DSWeiter();
            }
        }

        $this->SchreibeModulStatus('ExportiereVorgaenge', 'Vorg�nge f�r Export suchen...');

        // Vorg�nge in Exporttabelle schreiben
		// 19.10.2010 OP: In Where Klausel bei vvs_vcn_key not in in Klammer "120" ergaenzt = Status fuer Umgebucht
		// 07.08.2015 BT: VVS_VCN_KEY 124 ergaenzt = Vorgang blockiert, doppelter Rechnungsversand
        $this->_SQL = 'SELECT vvs_filid, vvs_wanr ';
        $this->_SQL .= 'FROM versvorgangsstatus ';
        $this->_SQL .= 'WHERE vvs_vcn_key = 106 ';
        $this->_SQL .= 'OR vvs_vcn_key not in (106,110,111,112,113,114,115,116,117,118,119,120,124) ';
        $this->_SQL .= 'AND vvs_freigabe = 1';

        $AnzExpVorgaenge = $this->_DB->ErmittleZeilenAnzahl($this->_SQL);
        $ZaehlerExpVorgaenge = 1;

        if($AnzExpVorgaenge > 0)
        {
            $rsExport = $this->_DB->RecordsetOeffnen($this->_SQL);

            while(!$rsExport->EOF())
            {
                $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Vorgang '.$ZaehlerExpVorgaenge.' von '.$AnzExpVorgaenge.' in Exporttabelle');

                $this->_FilNr = $rsExport->FeldInhalt('VVS_FILID');
                $this->_WaNr = $rsExport->FeldInhalt('VVS_WANR');

                $BindeVariablen=array();
                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;

                $this->SetzeCheckStatus(110);

                //###########################################################################
                //KOPFSATZ
                //###########################################################################

                $this->_SQL = 'SELECT ve.kennung,vv.vvs_filid,vv.vvs_wanr,ve.kunden_art,ve.firku_art,ve.zahl_art, ';
                $this->_SQL .= 've.vorst_abzug_kz,ve.vorst_betrag,ve.selbst_kz,ve.selbst_betrag,v.vve_versnr_export,ve.vers_bez, ';
                $this->_SQL .= 've.vers_strasse,ve.vers_hnr,ve.vers_plz,ve.vers_ort,ve.vers_staat_nr,ve.vers_telefon, ';
                $this->_SQL .= 've.versscheinnr,ve.schaden_nr,ve.schaden_datum,vv.vvs_schadenkz,ve.schaden_bez,ve.schaden_art, ';
                $this->_SQL .= 've.umsatz_gesamt,ve.umsatz_kunde,ve.umsatz_vers,vv.vvs_vorgangnr,vv.vvs_datumfg,ve.anrede,ve.titel,ve.name1,ve.name2, ';
                $this->_SQL .= 've.name3,ve.strasse,ve.hnr,ve.plz,ve.ort,ve.rufnr1,ve.rufnr2,ve.staat_nr,ve.gk_anrede, ';
                $this->_SQL .= 've.gk_titel,ve.gk_name1,ve.gk_name2,ve.gk_name3,ve.gk_strasse,ve.gk_hnr,ve.gk_plz,ve.gk_ort, ';
                $this->_SQL .= 've.gk_rufnr1,ve.gk_rufnr2,ve.gk_staat_nr,ve.ac_anrede,ve.ac_titel,ve.ac_name1,ve.ac_name2, ';
                $this->_SQL .= 've.ac_strasse,ve.ac_hnr,ve.ac_plz,ve.ac_ort,ve.ac_rufnr1,ve.ac_rufnr2,ve.ac_staat_nr,vv.vvs_kfzkennz, ';
                $this->_SQL .= 've.kfz_hersteller,ve.kfz_modell,ve.kfz_typ,ve.fahrgestellnr,ve.kfz_herstnr,vv.vvs_laenderschl,ve.ktypnr,vv.vvs_ust_schl, ';
                $this->_SQL .= 'f.fil_strasse,f.fil_plz,f.fil_ort,ve.kfz_km,v.vve_kuerzel, vv.vvs_versnr ';
                $this->_SQL .= 'FROM V_VERS_EXP_KOPF ve INNER JOIN versvorgangsstatus vv ';
                $this->_SQL .= 'ON ve.filid = vv.vvs_filid AND ve.wanr = vv.vvs_wanr ';
                $this->_SQL .= 'INNER JOIN versversicherungen v ';
                $this->_SQL .= 'ON v.vve_versnr = vv.vvs_versnr ';
                $this->_SQL .= 'INNER JOIN filialen f ';
                $this->_SQL .= 'ON f.fil_id = vv.vvs_filid ';
                $this->_SQL .= 'WHERE vv.vvs_filid = :var_N0_fil_id ';
                $this->_SQL .= 'AND vv.vvs_wanr = :var_N0_vvs_wanr';

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,$BindeVariablen) == 0)
                {
                // Kopfsatzdaten wurden nicht gefunden (Quelle DWH)
                    $this->SetzeCheckStatus(107);
                }
                else
                {
                // Kopfsatz kann in Exporttabelle geschrieben werden
                    $rsExpKopf = $this->_DB->RecordsetOeffnen($this->_SQL,$BindeVariablen);

                    $ExpStringKopf = '';

                    // Felder in Exportstring schreiben (Semikolon getrennt)
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('KENNUNG').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_FILID').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_WANR').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('KUNDEN_ART').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIRKU_ART').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('ZAHL_ART').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VORST_ABZUG_KZ').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VORST_BETRAG').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('SELBST_KZ').';';
                    
                    //SB in integer umwandeln/casten damit Nachkommastellen wegfallen
                    $sb = $rsExpKopf->FeldInhalt('SELBST_BETRAG');
                    $ExpStringKopf .= intval($sb).';';

                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVE_VERSNR_EXPORT').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_BEZ').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_STRASSE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_HNR').';';
                    $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('VERS_PLZ'),5,'0',STR_PAD_LEFT).';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_ORT').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_STAAT_NR').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_TELEFON').';';
                    //####################################################################################################
                    if($rsExpKopf->FeldInhalt('VVE_KUERZEL') != 'HUK')
                    {
                        $ExpStringKopf .= $this->EntferneBuchstaben($rsExpKopf->FeldInhalt('VERSSCHEINNR')).';';
                    }
                    else
                    {
                        $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('VERSSCHEINNR'),0,17).';';
                    }
                    //####################################################################################################
                    if($rsExpKopf->FeldInhalt('VVE_KUERZEL') == 'WGV')
                    {
                    // Schadennummer f�r die WGV formatieren 07-xxxxxxx
                        $ExpStringKopf .= $this->FormatiereSchadenNr($rsExpKopf->FeldInhalt('SCHADEN_NR')).';';
                    }
                    else
                    {
                        if($rsExpKopf->FeldInhalt('SCHADEN_NR') != '' && is_null($rsExpKopf->FeldInhalt('SCHADEN_NR')) != true)
                        {
                            $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('SCHADEN_NR'),0,20).';';
                        }
                        else
                        {
                            $ExpStringKopf .= ';';
                        }
                    }
                    
                    //####################################################################################################
                    if($rsExpKopf->FeldInhalt('SCHADEN_DATUM') != '' && is_null($rsExpKopf->FeldInhalt('SCHADEN_DATUM')) != true)
                    {                        
                        $tmpSchadenDatum = $this->pruefeSchadenDatum($rsExpKopf->FeldInhalt('SCHADEN_DATUM'),$this->_FilNr,$this->_WaNr);
                        
                        $ExpStringKopf .= $this->_FORM->PruefeDatum($tmpSchadenDatum, false).';';
                    }
                    else
                    {
                        $ExpStringKopf .= $this->pruefeSchadenDatum('01.01.1970',$this->_FilNr,$this->_WaNr).';';
                    }
                    
                    //####################################################################################################
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_SCHADENKZ').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('SCHADEN_BEZ').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('SCHADEN_ART').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_GESAMT').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_KUNDE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_VERS').';';
                    //####################################################################################################
                    if($rsExpKopf->FeldInhalt('VVS_SCHADENKZ') == 'E')
                    {
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_VORGANGNR').';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_DATUMFG').';';
                    }
                    else
                    {
                        $ExpStringKopf .= ';';
                        $ExpStringKopf .= ';';
                    }
                    //####################################################################################################
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('ANREDE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('TITEL').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME1').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME2').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME3').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('STRASSE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('HNR').';';
                    //####################################################################################################
                    if($rsExpKopf->FeldInhalt('PLZ') != '' && is_null($rsExpKopf->FeldInhalt('PLZ')) != true)
                    {
                        $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('PLZ'),5,'0',STR_PAD_LEFT).';';
                    }
                    else
                    {
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('PLZ').';';
                    }
                    //####################################################################################################
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('ORT').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('RUFNR1').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('RUFNR2').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('STAAT_NR').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_ANREDE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_TITEL').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_NAME1').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_NAME2').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_NAME3').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_STRASSE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_HNR').';';
                    //####################################################################################################
                    if($rsExpKopf->FeldInhalt('GK_PLZ') != '' && is_null($rsExpKopf->FeldInhalt('GK_PLZ')) != true)
                    {
                        $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('GK_PLZ'),5,'0',STR_PAD_LEFT).';';
                    }
                    else
                    {
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_PLZ').';';
                    }
                    //####################################################################################################
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_ORT').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_RUFNR1').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_RUFNR2').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_STAAT_NR').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_ANREDE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_TITEL').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_NAME1').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_NAME2').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_STRASSE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_HNR').';';
                    //####################################################################################################
                    if($rsExpKopf->FeldInhalt('AC_PLZ') != '' && is_null($rsExpKopf->FeldInhalt('AC_PLZ')) != true)
                    {
                        $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('AC_PLZ'),5,'0',STR_PAD_LEFT).';';
                    }
                    else
                    {
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_PLZ').';';
                    }
                    //####################################################################################################
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_ORT').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_RUFNR1').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_RUFNR2').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_STAAT_NR').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_KFZKENNZ').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_HERSTELLER').';';
                    $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('KFZ_MODELL'),0,55).';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_TYP').';';
                    $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('FAHRGESTELLNR'),0,17).';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_HERSTNR').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_LAENDERSCHL').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('KTYPNR').';';
                    //$ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_UST_SCHL').';';
                    $ExpStringKopf .= '19,0;';

                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIL_STRASSE').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIL_PLZ').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIL_ORT').';';
                    $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_KM');

                    $ExpStringKopf = str_replace("'", "�", $ExpStringKopf);

                    $BindeVariablen=array();
                    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                    $BindeVariablen['var_N0_vek_wanr']=$this->_WaNr;
                    $BindeVariablen['var_N0_vek_versnr']=$rsExpKopf->FeldInhalt('VVS_VERSNR');                    
                    $BindeVariablen['var_vek_datensatz']=$ExpStringKopf;
                    
                    $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                    $this->_SQL .= 'VALUES (:var_N0_fil_id, :var_N0_vek_wanr, :var_N0_vek_versnr, :var_vek_datensatz)';

                    $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
                }

                //###########################################################################
                //POSITIONSSATZ
                //###########################################################################
                $BindeVariablen=array();
                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
                
                $this->_SQL = 'SELECT \'P\' as kennung,vv.vvs_filid,vv.vvs_wanr,vp.position,vp.artnr,vp.artkz,vp.bez,vp.menge,vp.vk_preis, ';
                $this->_SQL .= 'vp.umsatz,vp.rabattkenn,vv.vvs_versnr,vv.vvs_schadenkz ';
                $this->_SQL .= 'FROM V_VERS_FORM_POSDATEN vp INNER JOIN versvorgangsstatus vv ';
                $this->_SQL .= 'ON vp.filnr = vv.vvs_filid AND vp.wanr = vv.vvs_wanr ';
                $this->_SQL .= 'WHERE vv.vvs_filid = :var_N0_fil_id ';
                $this->_SQL .= 'AND vv.vvs_wanr = :var_N0_vvs_wanr';

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,$BindeVariablen) == 0)
                {
                // Possatzdaten wurden nicht gefunden (Quelle DWH)
                    $this->SetzeCheckStatus(108);
                }
                else
                {
                // Kopfsatz kann in Exporttabelle geschrieben werden
                    $rsExpPos = $this->_DB->RecordsetOeffnen($this->_SQL,$BindeVariablen);

                    while(!$rsExpPos->EOF())
                    {
                        if($rsExpPos->FeldInhalt('VVS_SCHADENKZ') == 'S' && $rsExpPos->FeldInhalt('ARTNR') == '1EIN69' || $rsExpPos->FeldInhalt('VVS_SCHADENKZ') == 'E' && $rsExpPos->FeldInhalt('ARTNR') == 'GLAS01')
                        {

                            $ExpStringPos = '';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('KENNUNG').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('VVS_FILID').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('VVS_WANR').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('POSITION').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('ARTNR').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('ARTKZ').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('BEZ').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('MENGE').';';
                            $ExpStringPos .= $rsExpPos->FeldInhalt('VK_PREIS').';';
                            if(strpos($rsExpPos->FeldInhalt('UMSATZ'),',') !== false)
                            {
                                $ExpStringPos .= substr($rsExpPos->FeldInhalt('UMSATZ'),0,strpos($rsExpPos->FeldInhalt('UMSATZ'),',')).';';
                            }
                            else
                            {
                                $ExpStringPos .= $rsExpPos->FeldInhalt('UMSATZ').';';
                            }
                            //$ExpStringPos .= $rsExpPos->FeldInhalt('RABATTKENN');
                            $ExpStringPos .= 'N';

                            $ExpStringPos = str_replace("'", "�", $ExpStringPos);
                            
                            $BindeVariablen=array();
		                    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
		                    $BindeVariablen['var_N0_vek_wanr']=$this->_WaNr;
		                    $BindeVariablen['var_N0_vek_versnr']=$rsExpPos->FeldInhalt('VVS_VERSNR');                    
		                    $BindeVariablen['var_vek_datensatz']=$ExpStringPos; 

                            $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                            $this->_SQL .= 'VALUES (:var_N0_fil_id, :var_N0_vek_wanr, :var_N0_vek_versnr, :var_vek_datensatz)';

                            $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
                        }
                        $rsExpPos->DSWeiter();
                    }
                }

                //###########################################################################
                //BEARBEITERSATZ
                //###########################################################################

                // V-Satz wegschreiben
                $BindeVariablen=array();
                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
                
                $this->_SQL = 'SELECT \'B\' as kennung,vv.vvs_filid,vv.vvs_wanr,vb.status, ';
                $this->_SQL .= 'vb.datum,vb.zeit,vb.verkaeuferart,vb.name,vv.vvs_versnr ';
                $this->_SQL .= 'FROM V_VERS_FORM_BEARBDATEN vb INNER JOIN versvorgangsstatus vv ';
                $this->_SQL .= 'ON vb.filnr = vv.vvs_filid AND vb.wanr = vv.vvs_wanr ';
                $this->_SQL .= 'WHERE vb.verkaeuferart = \'V\' ';
                $this->_SQL .= 'AND vv.vvs_filid = :var_N0_fil_id ';
                $this->_SQL .= 'AND vv.vvs_wanr = :var_N0_vvs_wanr';

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,$BindeVariablen) == 0)
                {
                // Bearbeitersatzdaten wurden nicht gefunden (Quelle DWH)
                    $this->SetzeCheckStatus(109);
                }
                else
                {
                    // Kopfsatz kann in Exporttabelle geschrieben werden
                    $rsExpBearb = $this->_DB->RecordsetOeffnen($this->_SQL,$BindeVariablen);

                    $ExpStringBearb = '';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('KENNUNG').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_FILID').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_WANR').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('STATUS').';';
                    $ExpStringBearb .= substr($rsExpBearb->FeldInhalt('DATUM'),0,10).';';

                    if(strlen($rsExpBearb->FeldInhalt('ZEIT')) < 8)
                    {
                        $ExpStringBearb .= str_pad($rsExpBearb->FeldInhalt('ZEIT'),8,'0',STR_PAD_LEFT).';';
                    }
                    else
                    {
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('ZEIT').';';
                    }

                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VERKAEUFERART').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('NAME');

                    $ExpStringBearb = str_replace("'", "�", $ExpStringBearb);
                    
                    $BindeVariablen=array();
                    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                    $BindeVariablen['var_N0_vek_wanr']=$this->_WaNr;
                    $BindeVariablen['var_N0_vek_versnr']=$rsExpBearb->FeldInhalt('VVS_VERSNR');                    
                    $BindeVariablen['var_vek_datensatz']=$ExpStringBearb; 

                    $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                    $this->_SQL .= 'VALUES (:var_N0_fil_id, :var_N0_vek_wanr, :var_N0_vek_versnr, :var_vek_datensatz)';


                    $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
                }

                // M-Satz wegschreiben
                $BindeVariablen=array();
                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
                
                $this->_SQL = 'SELECT \'B\' as kennung,vv.vvs_filid,vv.vvs_wanr,vb.status, ';
                $this->_SQL .= 'vb.datum,vb.zeit,vb.verkaeuferart,vb.name,vv.vvs_versnr ';
                $this->_SQL .= 'FROM V_VERS_FORM_BEARBDATEN vb INNER JOIN versvorgangsstatus vv ';
                $this->_SQL .= 'ON vb.filnr = vv.vvs_filid AND vb.wanr = vv.vvs_wanr ';
                $this->_SQL .= 'WHERE vb.verkaeuferart = \'M\' ';
                $this->_SQL .= 'AND vv.vvs_filid = :var_N0_fil_id ';
                $this->_SQL .= 'AND vv.vvs_wanr = :var_N0_vvs_wanr';

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,$BindeVariablen) == 0)
                {
                // Bearbeitersatzdaten wurden nicht gefunden (Quelle DWH)
                    $this->SetzeCheckStatus(109);
                }
                else
                {
                    // Kopfsatz kann in Exporttabelle geschrieben werden
                    $rsExpBearb = $this->_DB->RecordsetOeffnen($this->_SQL,$BindeVariablen);

                    $ExpStringBearb = '';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('KENNUNG').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_FILID').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_WANR').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('STATUS').';';
                    $ExpStringBearb .= substr($rsExpBearb->FeldInhalt('DATUM'),0,10).';';

                    if(strlen($rsExpBearb->FeldInhalt('ZEIT')) < 8)
                    {
                        $ExpStringBearb .= str_pad($rsExpBearb->FeldInhalt('ZEIT'),8,'0',STR_PAD_LEFT).';';
                    }
                    else
                    {
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('ZEIT').';';
                    }

                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VERKAEUFERART').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('NAME');

                    $ExpStringBearb = str_replace("'", "�", $ExpStringBearb);
                    
                    $BindeVariablen=array();
                    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                    $BindeVariablen['var_N0_vek_wanr']=$this->_WaNr;
                    $BindeVariablen['var_N0_vek_versnr']=$rsExpBearb->FeldInhalt('VVS_VERSNR');                    
                    $BindeVariablen['var_vek_datensatz']=$ExpStringBearb; 

                    $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                    $this->_SQL .= 'VALUES (:var_N0_fil_id, :var_N0_vek_wanr, :var_N0_vek_versnr, :var_vek_datensatz)';

                    $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
                }

                // K-Satz wegschreiben
                $BindeVariablen=array();
                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
                
                $this->_SQL = 'SELECT \'B\' as kennung,vv.vvs_filid,vv.vvs_wanr,vb.status, ';
                $this->_SQL .= 'vb.datum,vb.zeit,vb.verkaeuferart,vb.name,vv.vvs_versnr ';
                $this->_SQL .= 'FROM V_VERS_FORM_BEARBDATEN vb INNER JOIN versvorgangsstatus vv ';
                $this->_SQL .= 'ON vb.filnr = vv.vvs_filid AND vb.wanr = vv.vvs_wanr ';
                $this->_SQL .= 'WHERE vb.verkaeuferart = \'K\' ';
                $this->_SQL .= 'AND vv.vvs_filid = :var_N0_fil_id ';
                $this->_SQL .= 'AND vv.vvs_wanr = :var_N0_vvs_wanr';

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,$BindeVariablen) == 0)
                {
                // Bearbeitersatzdaten wurden nicht gefunden (Quelle DWH)
                    $this->SetzeCheckStatus(109);
                }
                else
                {
                    // Kopfsatz kann in Exporttabelle geschrieben werden
                    $rsExpBearb = $this->_DB->RecordsetOeffnen($this->_SQL,$BindeVariablen);

                    $ExpStringBearb = '';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('KENNUNG').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_FILID').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_WANR').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('STATUS').';';
                    $ExpStringBearb .= substr($rsExpBearb->FeldInhalt('DATUM'),0,10).';';

                    if(strlen($rsExpBearb->FeldInhalt('ZEIT')) < 8)
                    {
                        $ExpStringBearb .= str_pad($rsExpBearb->FeldInhalt('ZEIT'),8,'0',STR_PAD_LEFT).';';
                    }
                    else
                    {
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('ZEIT').';';
                    }

                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('VERKAEUFERART').';';
                    $ExpStringBearb .= $rsExpBearb->FeldInhalt('NAME');

                    $ExpStringBearb = str_replace("'", "�", $ExpStringBearb);
                    
                    $BindeVariablen=array();
                    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
                    $BindeVariablen['var_N0_vek_wanr']=$this->_WaNr;
                    $BindeVariablen['var_N0_vek_versnr']=$rsExpBearb->FeldInhalt('VVS_VERSNR');                    
                    $BindeVariablen['var_vek_datensatz']=$ExpStringBearb; 

                    $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                    $this->_SQL .= 'VALUES (:var_N0_fil_id, :var_N0_vek_wanr, :var_N0_vek_versnr, :var_vek_datensatz)';

                    $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
                }

                $this->SetzeCheckStatus(111);

                $rsExport->DSWeiter();

                $ZaehlerExpVorgaenge++;
            }
        }
        //###########################################################################
        //###########################################################################
        //Daten wegschreiben
        //###########################################################################
        //###########################################################################

        //###########################################################################
        //###########################################################################
        // Datum ermitteln --> genommen wird das Datum wann die Verarbeitung (also FirstGlas)
        // begonnen hat zu Verarbeiten (Tabelle: versjobid)

        $SQL = 'SELECT vji_datumjobid ';
        $SQL .= 'FROM versjobid ';

        $rsDatum = $this->_DB->RecordSetOeffnen($SQL);

        $Datum = substr($rsDatum->FeldInhalt('VJI_DATUMJOBID'),8,2).substr($rsDatum->FeldInhalt('VJI_DATUMJOBID'),3,2).substr($rsDatum->FeldInhalt('VJI_DATUMJOBID'),0,2);

        //###########################################################################
        //###########################################################################

		$this->_DateinameExportVers .= $Datum . '.txt';
		$this->_DateinameExportDebeka .= $Datum . '.txt';
		$this->_DateinameExportDBVW .= $Datum . '.txt';
		$this->_DateinameExportWV .= $Datum . '.txt';
		$this->_DateinameExportWGV .= $Datum . '.txt';
		$this->_DateinameExportAXA .= $Datum . '.txt';
		$this->_DateinameExportHDI .= $Datum . '.txt';
		$this->_DateinameExportHUK .= $Datum . '.txt';
		$this->_DateinameExportVHV .= $Datum . '.txt';
		$this->_DateinameExportRuV .= $Datum . '.txt';
		$this->_DateinameExportLVM .= $Datum . '.txt';
		$this->_DateinameExportCONTI .= $Datum . '.txt';
		$this->_DateinameExportOESA .= $Datum . '.txt';
		$this->_DateinameExportZuerich .= $Datum . '.txt';

        // Pr�fen ob f�r den aktuellen Tag bereits Exportdateien erzeugt wurden
        
		if ($this->CheckExportStatus ( $this->_DateinameExportVers ) === false && 
		    $this->CheckExportStatus ( $this->_DateinameExportDebeka ) === false && 
		    $this->CheckExportStatus ( $this->_DateinameExportDBVW ) === false && 
		    $this->CheckExportStatus ( $this->_DateinameExportWV ) === false && 
		    $this->CheckExportStatus ( $this->_DateinameExportWGV ) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportAXA) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportHDI) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportHUK) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportVHV) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportRuV) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportLVM) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportCONTI) === false &&
			$this->CheckExportStatus ( $this->_DateinameExportOESA) === false &&
		    $this->CheckExportStatus ( $this->_DateinameExportZuerich) === false)
        {
            //###########################################################################
            //Datei export_vers_ schreiben
            //###########################################################################

            if($this->CheckExportStatus($this->_DateinameExportVers) === false)
            {
         
               	$this->_SQL = 'SELECT ve.vek_datensatz ';
                $this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
                $this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
                $this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_vers_\' ';
                $this->_SQL .= 'ORDER BY vek_key';

                $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Vers-Datei');
                $this->SchreibeExportStatus($this->_DateinameExportVers, 'Start');

                if(($fd = fopen($this->_PfadExportVers.$this->_DateinameExportVers,'w')) === false)
                {
                    echo $this->error_msg = "Fehler beim Erstellen der Datei";
                    die();
                }

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
                {
                    $rsExportVers = $this->_DB->RecordSetOeffnen($this->_SQL);

                    while(!$rsExportVers->EOF())
                    {
                        fputs($fd,$rsExportVers->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
                        $rsExportVers->DSWeiter();
                    }

                }
                fclose($fd);
                $this->SchreibeExportStatus($this->_DateinameExportVers, 'Ende', md5_file($this->_PfadExportVers.$this->_DateinameExportVers));
            }
            
            
            //###########################################################################
            //Datei export_wue_ schreiben
            //###########################################################################
            if($this->CheckExportStatus($this->_DateinameExportWV) === false)
            {
            	echo"WV begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_wue_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            
            	$this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe WV-Datei');
            	$this->SchreibeExportStatus($this->_DateinameExportWV, 'Start');
            
            	if(($fd = fopen($this->_PfadExportVers.$this->_DateinameExportWV,'w')) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die();
            	}
            
            	if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
            	{
            		$rsExportWV = $this->_DB->RecordSetOeffnen($this->_SQL);
            
            		while(!$rsExportWV->EOF())
            		{
            			fputs($fd,$rsExportWV->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
            			$rsExportWV->DSWeiter();
            		}
            	}
            	fclose($fd);
            	$this->SchreibeExportStatus($this->_DateinameExportWV, 'Ende', md5_file($this->_PfadExportVers.$this->_DateinameExportWV));
            }


            // ###########################################################################
            // Datei
            // export_wgv_
            // schreiben
            // ###########################################################################
            if ($this->CheckExportStatus ( $this->_DateinameExportWGV ) === false)
            {
            	echo "WGV begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_wgv_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            
            	$this->SchreibeModulStatus ( 'ExportiereVorgaenge', 'Schreibe WGV-Datei' );
            	$this->SchreibeExportStatus ( $this->_DateinameExportWGV, 'Start' );
            
            	if (($fd = fopen ( $this->_PfadExportVers . $this->_DateinameExportWGV, 'w' )) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die ();
            		echo "WGV fehler";
            	}
            
            	if ($this->_DB->ErmittleZeilenAnzahl ( $this->_SQL ) > 0)
            	{
            		$rsExportWGV = $this->_DB->RecordSetOeffnen ( $this->_SQL );
            			
            		while ( ! $rsExportWGV->EOF () )
            		{
            			fputs ( $fd, $rsExportWGV->FeldInhalt ( 'VEK_DATENSATZ' ) . chr ( 13 ) . chr ( 10 ) );
            			$rsExportWGV->DSWeiter ();
            		}
            	}
            	fclose ( $fd );
            	$this->SchreibeExportStatus ( $this->_DateinameExportWGV, 'Ende', md5_file ( $this->_PfadExportVers . $this->_DateinameExportWGV ) );
            }
            	
            // ###########################################################################
            // Datei
            // export_axa_
            // schreiben
            // ###########################################################################
            if ($this->CheckExportStatus ( $this->_DateinameExportAXA ) === false)
            {
            	echo "axa begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_axa_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            		
            	$this->SchreibeModulStatus ( 'ExportiereVorgaenge', 'Schreibe AXA-Datei' );
            	$this->SchreibeExportStatus ( $this->_DateinameExportAXA, 'Start' );
            		
            	if (($fd = fopen ( $this->_PfadExportVers . $this->_DateinameExportAXA, 'w' )) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die ();
            	}
            		
            	if ($this->_DB->ErmittleZeilenAnzahl ( $this->_SQL ) > 0)
            	{
            		$rsExportAXA = $this->_DB->RecordSetOeffnen ( $this->_SQL );
            
            		while ( ! $rsExportAXA->EOF () )
            		{
            			fputs ( $fd, $rsExportAXA->FeldInhalt ( 'VEK_DATENSATZ' ) . chr ( 13 ) . chr ( 10 ) );
            			$rsExportAXA->DSWeiter ();
            		}
            	}
            	fclose ( $fd );
            	$this->SchreibeExportStatus ( $this->_DateinameExportAXA, 'Ende', md5_file ( $this->_PfadExportVers . $this->_DateinameExportAXA ) );
            }
            	
            // ###########################################################################
            // Datei
            // export_hdi_
            // schreiben
            // ###########################################################################
            if ($this->CheckExportStatus ( $this->_DateinameExportHDI ) === false)
            {
            	echo "hdi begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_hdi_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            		
            	$this->SchreibeModulStatus ( 'ExportiereVorgaenge', 'Schreibe HDI-Datei' );
            	$this->SchreibeExportStatus ( $this->_DateinameExportHDI, 'Start' );
            		
            	if (($fd = fopen ( $this->_PfadExportVers . $this->_DateinameExportHDI, 'w' )) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die ();
            	}
            		
            	if ($this->_DB->ErmittleZeilenAnzahl ( $this->_SQL ) > 0)
            	{
            		$rsExportHDI = $this->_DB->RecordSetOeffnen ( $this->_SQL );
            			
            		while ( ! $rsExportHDI->EOF () )
            		{
            			fputs ( $fd, $rsExportHDI->FeldInhalt ( 'VEK_DATENSATZ' ) . chr ( 13 ) . chr ( 10 ) );
            			$rsExportHDI->DSWeiter ();
            		}
            	}
            	fclose ( $fd );
            	$this->SchreibeExportStatus ( $this->_DateinameExportHDI, 'Ende', md5_file ( $this->_PfadExportVers . $this->_DateinameExportHDI ) );
            }
            	
            // ###########################################################################
            // Datei
            // export_huk_
            // schreiben
            // ###########################################################################
            if ($this->CheckExportStatus ( $this->_DateinameExportHUK ) === false)
            {
            	echo "huk begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_huk_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            		
            	$this->SchreibeModulStatus ( 'ExportiereVorgaenge', 'Schreibe HUK-Datei' );
            	$this->SchreibeExportStatus ( $this->_DateinameExportHUK, 'Start' );
            		
            	if (($fd = fopen ( $this->_PfadExportVers . $this->_DateinameExportHUK, 'w' )) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die ();
            	}
            		
            	if ($this->_DB->ErmittleZeilenAnzahl ( $this->_SQL ) > 0)
            	{
            		$rsExportHUK = $this->_DB->RecordSetOeffnen ( $this->_SQL );
            
            		while ( ! $rsExportHUK->EOF () )
            		{
            			fputs ( $fd, $rsExportHUK->FeldInhalt ( 'VEK_DATENSATZ' ) . chr ( 13 ) . chr ( 10 ) );
            			$rsExportHUK->DSWeiter ();
            		}
            	}
            	fclose ( $fd );
            	$this->SchreibeExportStatus ( $this->_DateinameExportHUK, 'Ende', md5_file ( $this->_PfadExportVers . $this->_DateinameExportHUK ) );
            }
            	
            // ###########################################################################
            // Datei
            // export_VHV_
            // schreiben
            // ###########################################################################
            if ($this->CheckExportStatus ( $this->_DateinameExportVHV ) === false)
            {
            	echo "VHV begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_vhv_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            		
            	$this->SchreibeModulStatus ( 'ExportiereVorgaenge', 'Schreibe VHV-Datei' );
            	$this->SchreibeExportStatus ( $this->_DateinameExportVHV, 'Start' );
            		
            	if (($fd = fopen ( $this->_PfadExportVers . $this->_DateinameExportVHV, 'w' )) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die ();
            	}
            		
            	if ($this->_DB->ErmittleZeilenAnzahl ( $this->_SQL ) > 0)
            	{
            		$rsExportVHV = $this->_DB->RecordSetOeffnen ( $this->_SQL );
            			
            		while ( ! $rsExportVHV->EOF () )
            		{
            			fputs ( $fd, $rsExportVHV->FeldInhalt ( 'VEK_DATENSATZ' ) . chr ( 13 ) . chr ( 10 ) );
            			$rsExportVHV->DSWeiter ();
            		}
            	}
            	fclose ( $fd );
            	$this->SchreibeExportStatus ( $this->_DateinameExportVHV, 'Ende', md5_file ( $this->_PfadExportVers . $this->_DateinameExportVHV ) );
            }
            	
            // ###########################################################################
            // Datei
            // export_ruv_
            // schreiben
            // ###########################################################################
            if ($this->CheckExportStatus ( $this->_DateinameExportRuV ) === false)
            {
            	echo "RuV begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_ruv_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            		
            	$this->SchreibeModulStatus ( 'ExportiereVorgaenge', 'Schreibe RuV-Datei' );
            	$this->SchreibeExportStatus ( $this->_DateinameExportRuV, 'Start' );
            		
            	if (($fd = fopen ( $this->_PfadExportVers . $this->_DateinameExportRuV, 'w' )) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die ();
            	}
            		
            	if ($this->_DB->ErmittleZeilenAnzahl ( $this->_SQL ) > 0)
            	{
            		$rsExportRuV = $this->_DB->RecordSetOeffnen ( $this->_SQL );
            
            		while ( ! $rsExportRuV->EOF () )
            		{
            			fputs ( $fd, $rsExportRuV->FeldInhalt ( 'VEK_DATENSATZ' ) . chr ( 13 ) . chr ( 10 ) );
            			$rsExportRuV->DSWeiter ();
            		}
            	}
            	fclose ( $fd );
            	$this->SchreibeExportStatus ( $this->_DateinameExportRuV, 'Ende', md5_file ( $this->_PfadExportVers . $this->_DateinameExportRuV ) );
            }
            	
            // ###########################################################################
            // Datei
            // export_lvm_
            // schreiben
            // ###########################################################################
            
            //$this->ErstelleExportDatei ($this->_DateinameExportLVM, $this->_DateinameExportLVM);
            
            if ($this->CheckExportStatus ( $this->_DateinameExportLVM ) === false)
            {
            	echo "LVM begin";
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_lvm_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            		
            	$this->SchreibeModulStatus ( 'ExportiereVorgaenge', 'Schreibe LVM-Datei' );
            	$this->SchreibeExportStatus ( $this->_DateinameExportLVM, 'Start' );
            		
            	if (($fd = fopen ( $this->_PfadExportVers . $this->_DateinameExportLVM, 'w' )) === false)
            	{
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die ();
            	}
            		
            	if ($this->_DB->ErmittleZeilenAnzahl ( $this->_SQL ) > 0)
            	{
            		$rsExportLVM= $this->_DB->RecordSetOeffnen ( $this->_SQL );
            			
            		while ( ! $rsExportLVM->EOF () )
            		{
            			fputs ( $fd, $rsExportLVM->FeldInhalt ( 'VEK_DATENSATZ' ) . chr ( 13 ) . chr ( 10 ) );
            			$rsExportLVM->DSWeiter ();
            		}
            	}
            	fclose ( $fd );
            	$this->SchreibeExportStatus ( $this->_DateinameExportLVM, 'Ende', md5_file ( $this->_PfadExportVers . $this->_DateinameExportLVM ) );
            }

            
            //###########################################################################
            //Datei export_debeka_ schreiben
            //###########################################################################

            if($this->CheckExportStatus($this->_DateinameExportDebeka) === false)
            {
                $this->_SQL = 'SELECT ve.vek_datensatz ';
                $this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
                $this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
                $this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_debeka_\' ';
                $this->_SQL .= 'ORDER BY vek_key';

                $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Debeka-Datei');
                $this->SchreibeExportStatus($this->_DateinameExportDebeka, 'Start');

                if(($fd = fopen($this->_PfadExportVers.$this->_DateinameExportDebeka,'w')) === false)
                {
                    // bevor Script abgew�rgt wird noch die davor erstellte Exportdatei gel�scht
                    unlink($this->_PfadExportVers.$this->_DateinameExportVers);

                    echo $this->error_msg = "Fehler beim Erstellen der Datei";
                    die();
                }

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
                {

                    $rsExportDebeka = $this->_DB->RecordSetOeffnen($this->_SQL);

                    while(!$rsExportDebeka->EOF())
                    {
                        fputs($fd,$rsExportDebeka->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
                        $rsExportDebeka->DSWeiter();
                    }

                }
                fclose($fd);
                $this->SchreibeExportStatus($this->_DateinameExportDebeka, 'Ende', md5_file($this->_PfadExportVers.$this->_DateinameExportDebeka));
            }
            //###########################################################################
            //Datei export_dbvw_ schreiben
            //###########################################################################

            
            if($this->CheckExportStatus($this->_DateinameExportDBVW) === false)
            {

                $this->_SQL = 'SELECT ve.vek_datensatz ';
                $this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
                $this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
                $this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_dbvw_\' ';
                $this->_SQL .= 'ORDER BY vek_key';

                $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe DBVW-Datei');
                $this->SchreibeExportStatus($this->_DateinameExportDBVW, 'Start');

                if(($fd = fopen($this->_PfadExportVers.$this->_DateinameExportDBVW,'w')) === false)
                {
                    // bevor Script abgew�rgt wird noch die davor erstellte Exportdatei gel�scht
                    unlink($this->_PfadExportVers.$this->_DateinameExportVers);
                    unlink($this->_PfadExportVers.$this->_DateinameExportDebeka);

                    echo $this->error_msg = "Fehler beim Erstellen der Datei";
                    die();
                }

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
                {

                    $rsExportDBVW = $this->_DB->RecordSetOeffnen($this->_SQL);

                    while(!$rsExportDBVW->EOF())
                    {
                        fputs($fd,$rsExportDBVW->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
                        $rsExportDBVW->DSWeiter();
                    }
                }
                fclose($fd);
                $this->SchreibeExportStatus($this->_DateinameExportDBVW, 'Ende', md5_file($this->_PfadExportVers.$this->_DateinameExportDBVW));
            }
           
            if($this->CheckExportStatus($this->_DateinameExportCONTI) === false)
            {
            
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_conti_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            
            	$this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe CONTI-Datei');
            	$this->SchreibeExportStatus($this->_DateinameExportCONTI, 'Start');
            
            	if(($fd = fopen($this->_PfadExportVers.$this->_DateinameExportCONTI,'w')) === false)
            	{
            		// bevor Script abgew�rgt wird noch die davor erstellte Exportdatei gel�scht
            		//unlink($this->_PfadExportVers.$this->_DateinameExportDebeka);
            
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die();
            	}
            
            	if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
            	{
            
            		$rsExportConti = $this->_DB->RecordSetOeffnen($this->_SQL);
            
            		while(!$rsExportConti->EOF())
            		{
            			fputs($fd,$rsExportConti->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
            			$rsExportConti->DSWeiter();
            		}
            	}
            	fclose($fd);
            	$this->SchreibeExportStatus($this->_DateinameExportCONTI, 'Ende', md5_file($this->_PfadExportVers.$this->_DateinameExportCONTI));
            }
            
            if($this->CheckExportStatus($this->_DateinameExportOESA) === false)
            {
            
            	$this->_SQL = 'SELECT ve.vek_datensatz ';
            	$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
            	$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
            	$this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_oesa_\' ';
            	$this->_SQL .= 'ORDER BY vek_key';
            
            	$this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Oesa-Datei');
            	$this->SchreibeExportStatus($this->_DateinameExportOESA, 'Start');
            
            	if(($fd = fopen($this->_PfadExportVers.$this->_DateinameExportOESA,'w')) === false)
            	{
            		// bevor Script abgew�rgt wird noch die davor erstellte Exportdatei gel�scht
            		//unlink($this->_PfadExportVers.$this->_DateinameExportDebeka);
            
            		echo $this->error_msg = "Fehler beim Erstellen der Datei";
            		die();
            	}
            
            	if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
            	{
            
            		$rsExportOesa = $this->_DB->RecordSetOeffnen($this->_SQL);
            
            		while(!$rsExportOesa->EOF())
            		{
            			fputs($fd,$rsExportOesa->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
            			$rsExportOesa->DSWeiter();
            		}
            	}
            	fclose($fd);
            	$this->SchreibeExportStatus($this->_DateinameExportOESA, 'Ende', md5_file($this->_PfadExportVers.$this->_DateinameExportOESA));
            }
            if($this->CheckExportStatus($this->_DateinameExportZuerich) === false)
            {
            
                $this->_SQL = 'SELECT ve.vek_datensatz ';
                $this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
                $this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
                $this->_SQL .= 'WHERE vv.vve_exportdatei = \'export_zuerich_\' ';
                $this->_SQL .= 'ORDER BY vek_key';
            
                $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Zuerich-Datei');
                $this->SchreibeExportStatus($this->_DateinameExportZuerich, 'Start');
            
                if(($fd = fopen($this->_PfadExportVers.$this->_DateinameExportZuerich,'w')) === false)
                {
                    // bevor Script abgew�rgt wird noch die davor erstellte Exportdatei gel�scht
                    //unlink($this->_PfadExportVers.$this->_DateinameExportDebeka);
            
                    echo $this->error_msg = "Fehler beim Erstellen der Datei";
                    die();
                }
            
                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
                {
            
                    $rsExportZuerich = $this->_DB->RecordSetOeffnen($this->_SQL);
            
                    while(!$rsExportZuerich->EOF())
                    {
                        fputs($fd,$rsExportZuerich->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
                        $rsExportZuerich->DSWeiter();
                    }
                }
                fclose($fd);
                $this->SchreibeExportStatus($this->_DateinameExportZuerich, 'Ende', md5_file($this->_PfadExportVers.$this->_DateinameExportZuerich));
            }

            // alle exportierten Vorg�nge als Erledigt markieren
            $this->_SQL = 'UPDATE versvorgangsstatus ';
            $this->_SQL .= 'SET vvs_vcn_key = 112, vvs_datumexport = sysdate ';
            $this->_SQL .= 'WHERE vvs_vcn_key = 111';

            $this->_DB->Ausfuehren($this->_SQL,'',true);

            $this->_SQL = 'INSERT INTO versexporthist (veh_filid,veh_wanr,veh_versnr,veh_datensatz,veh_datumexport) ';
            $this->_SQL .= 'SELECT vek_filid, vek_wanr, vek_versnr, vek_datensatz, sysdate ';
            $this->_SQL .= 'FROM versexport';

            $this->_DB->Ausfuehren($this->_SQL,'',true);

            $this->_SQL = 'truncate table versexport';

            $this->_DB->Ausfuehren($this->_SQL,'',true);

        }

        $this->SchreibeModulStatus('ExportiereVorgaenge', 'Export beendet');
    }

    public function RueckschreibenDWH()
    {
        $this->SchreibeModulStatus('RueckschreibenDWH', 'Suche Daten zum zur�ckschreiben...');

        $this->SQL = 'SELECT vvs_key,vvs_filid,vvs_wanr,vvs_schadenkz,vvs_vorgangnr,vvs_kfzkennz,vvs_export_dwh ';
        $this->SQL .= 'FROM versvorgangsstatus ';
        $this->SQL .= 'WHERE vvs_export_dwh <> 0';

        $rsVVS = $this->_DB->RecordSetOeffnen($this->SQL);

        while(!$rsVVS->EOF())
        {
            $this->SchreibeModulStatus('RueckschreibenDWH', 'Daten ins DWH schreiben');

            $BindeVariablen=array();
            $BindeVariablen['var_N0_fil_id']=$rsVVS->FeldInhalt('VVS_FILID');
            $BindeVariablen['var_N0_wanr']=$rsVVS->FeldInhalt('VVS_WANR');
            
            $this->_SQL = 'SELECT filnr, wanr ';
            $this->_SQL .= 'FROM dwh.wa_glas@DWH ';
            $this->_SQL .= 'WHERE filnr = :var_N0_fil_id ';
            $this->_SQL .= 'AND wanr = :var_N0_wanr';

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL,$BindeVariablen) == 0)
            {
            // Werkstattauftrag noch nicht in Tabelle vorhanden (INSERT)
            	
            	$BindeVariablen=array();
            	$BindeVariablen['var_N0_fil_id']=$rsVVS->FeldInhalt('VVS_FILID');
            	$BindeVariablen['var_N0_wanr']=$rsVVS->FeldInhalt('VVS_WANR');
            	$BindeVariablen['var_T_vorgangsnr']=$rsVVS->FeldInhalt('VVS_VORGANGNR');
            	$BindeVariablen['var_T_kfz_kennz']=$rsVVS->FeldInhalt('VVS_KFZKENNZ');
            	$BindeVariablen['var_T_schadenkz']=$rsVVS->FeldInhalt('VVS_SCHADENKZ');
            
                $this->_SQL = 'INSERT INTO dwh.wa_glas@DWH (filnr,wanr,vorgangsnr,kfz_kennz,schadenkz) ';
                $this->_SQL .= 'VALUES (:var_N0_fil_id, :var_N0_wanr, :var_T_vorgangsnr, :var_T_kfz_kennz, :var_T_schadenkz)';

                if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)
                {
                    //throw new awisException('',,$SQL,2);
                }
            }
            else
            {
            // Werkstattauftrag bereits in Tabelle vorhanden (UPDATE)
                
            	$BindeVariablen=array();
            	$BindeVariablen['var_N0_fil_id']=$rsVVS->FeldInhalt('VVS_FILID');
            	$BindeVariablen['var_N0_wanr']=$rsVVS->FeldInhalt('VVS_WANR');
            
            	$this->_SQL = 'UPDATE dwh.wa_glas@DWH SET ';
                $Bedingung = '';
                if($rsVVS->FeldInhalt('VVS_EXPORT_DWH') & 1)
                {
                    $BindeVariablen['var_T_schadenkz']=$rsVVS->FeldInhalt('VVS_SCHADENKZ');
                    
                	$Bedingung .= ',SCHADENKZ = :var_T_schadenkz ';
                }
                if($rsVVS->FeldInhalt('VVS_EXPORT_DWH') & 2)
                {
                    $BindeVariablen['var_T_vorgangsnr']=$rsVVS->FeldInhalt('VVS_VORGANGNR');
                    
                	$Bedingung .= ',VORGANGSNR = :var_T_vorgangsnr ';
                }
                if($rsVVS->FeldInhalt('VVS_EXPORT_DWH') & 4)
                {
                    $BindeVariablen['var_T_kfz_kennz']=$rsVVS->FeldInhalt('VVS_KFZKENNZ');
                    
                	$Bedingung .= ',KFZ_KENNZ = :var_T_kfz_kennz ';
                }

                $this->_SQL .= substr($Bedingung,1);
                $this->_SQL .= ' WHERE filnr = :var_N0_fil_id ';
                $this->_SQL .= 'AND wanr = :var_N0_wanr';

                if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)
                {
                //throw new awisException('',,$SQL,2);
                }
            }

            $BindeVariablen=array();
            $BindeVariablen['var_N0_vvs_key']=$rsVVS->FeldInhalt('VVS_KEY');
            
            $this->_SQL = 'UPDATE versvorgangsstatus ';
            $this->_SQL .= 'SET vvs_export_dwh = 0 ';
            $this->_SQL .= 'WHERE vvs_key = :var_N0_vvs_key';

            if($this->_DB->Ausfuehren($this->_SQL,'',false,$BindeVariablen)===false)
            {
            //throw new awisException('',,$SQL,2);
            }

            $rsVVS->DSWeiter();
        }

        $this->SchreibeModulStatus('RueckschreibenDWH', 'Daten zur�ckschreiben abgeschlossen');
    }

    public function KennzeichenWandeln($Kennzeichen)
    {
        $suchen = array(' ', '-','/','.');
        $ersetzen = array('','','','');

        for($x=0;$x < count($suchen,0);$x++)
        {
            $Kennzeichen = str_replace($suchen[$x],$ersetzen[$x],$Kennzeichen);
        }

        return $Kennzeichen;
    }

    public function SetzeStatus($StatusNr,$FilNr='',$WANr='')
    {
    	try
    	{
	    	$BindeVariablen=array();
	        $BindeVariablen['var_N0_vvs_ven_key']=$StatusNr;
	        
	    	$this->_SQL = 'UPDATE versvorgangsstatus ';
	        $this->_SQL .= 'SET vvs_ven_key = :var_N0_vvs_ven_key ';
	
	        if($FilNr == '')
	        {
	            $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
	            $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
	        }
	        else
	        {
	        	$BindeVariablen['var_N0_fil_id']=$FilNr;
	            $BindeVariablen['var_N0_vvs_wanr']=$WANr;
	        }
	        
	        $this->_SQL .= 'WHERE vvs_filid = :var_N0_fil_id ';
	        $this->_SQL .= 'AND vvs_wanr = :var_N0_vvs_wanr';
	        
	
	        $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
    	}
    	catch(Exception $e)
        {
            echo ('Exception:'.$e->getMessage());
            die();
        }
        catch(awisException $awis)
        {
            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
            die();
        }
    }

    public function SetzeCheckStatus($StatusNr,$FilNr='',$WANr='')
    {
    	try
    	{
	    	$BindeVariablen=array();
	        $BindeVariablen['var_N0_vvs_vcn_key']=$StatusNr;
	    	
	    	$this->_SQL = 'UPDATE versvorgangsstatus ';
	        $this->_SQL .= 'SET vvs_vcn_key = :var_N0_vvs_vcn_key ';
	
	        if($FilNr == '')
	        {
	            $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
	            $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
	        }
	        else
	        {
	            $BindeVariablen['var_N0_fil_id']=$FilNr;
	            $BindeVariablen['var_N0_vvs_wanr']=$WANr;        	
	        }
	
	        $this->_SQL .= 'WHERE vvs_filid = :var_N0_fil_id ';
			$this->_SQL .= 'AND vvs_wanr = :var_N0_vvs_wanr';
	            
	        $this->_DB->Ausfuehren($this->_SQL,'',true,$BindeVariablen);
    	}
    	catch(Exception $e)
        {
            echo ('Exception:'.$e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
        
    }

    public function SetzeExportDWHStatus($Feldname,$FilNr='',$WANr='')
    {
    	try
    	{
    		/*
		    $BindeVariablen=array();
		    $BindeVariablen['var_T_vvd_feldname']=$Feldname;
		    
		    $this->_DB->SetzeBindevariable('VVD', 'var_T_vvd_feldname',$Feldname , awisDatenbank::VAR_TYP_TEXT);
		    */
	    	
	    	$this->_SQL = 'SELECT vvd_bit ';
	        $this->_SQL .= 'FROM versvorgangsstatusexport ';
	        //$this->_SQL .= 'WHERE vvd_feldname = :var_T_vvd_feldname';
	        $this->_SQL .= 'WHERE vvd_feldname'.$this->_DB->LikeOderIst($Feldname);
	
	        if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
	        {
	            $rsBit = $this->_DB->RecordSetOeffnen($this->_SQL,$this->_DB->Bindevariablen('VVD',false));
	
	            $this->_SQL = 'SELECT vvs_export_dwh ';
	            $this->_SQL .= 'FROM versvorgangsstatus ';
	
	            if($FilNr == '')
	            {
	            	/*
	                $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
	            	$BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
	            	*/
	            	$this->_DB->SetzeBindevariable('VVS', 'var_N0_fil_id',$this->_FilNr , awisDatenbank::VAR_TYP_GANZEZAHL);
	            	$this->_DB->SetzeBindevariable('VVS', 'var_N0_vvs_wanr',$this->_WaNr , awisDatenbank::VAR_TYP_GANZEZAHL);
	            }
	            else
	            {
	            	$this->_DB->SetzeBindevariable('VVS', 'var_N0_fil_id',$FilNr , awisDatenbank::VAR_TYP_GANZEZAHL);
	            	$this->_DB->SetzeBindevariable('VVS', 'var_N0_vvs_wanr',$WANr , awisDatenbank::VAR_TYP_GANZEZAHL);
	            	/*
	                $BindeVariablen['var_N0_fil_id']=$FilNr;
	            	$BindeVariablen['var_N0_vvs_wanr']=$WANr;
	            	*/            	
	            }
	            
	            $this->_SQL .= 'WHERE vvs_filid = :var_N0_fil_id ';
	            $this->_SQL .= 'AND vvs_wanr = :var_N0_vvs_wanr';
	
	            $rsVVSBit = $this->_DB->RecordSetOeffnen($this->_SQL,$this->_DB->Bindevariablen('VVS',true));
	
	            if($rsVVSBit->FeldInhalt('VVS_EXPORT_DWH') & $rsBit->FeldInhalt('VVD_BIT'))
	            {
	            }
	            else
	            {
	            	/*
	                $BindeVariablen=array();
					$BindeVariablen['var_N0_vvd_bit']=$rsBit->FeldInhalt('VVD_BIT');
					*/
					$this->_DB->SetzeBindevariable('VVS', 'var_N0_vvd_bit',$rsBit->FeldInhalt('VVD_BIT') , awisDatenbank::VAR_TYP_GANZEZAHL);
					
					$this->_SQL = 'UPDATE versvorgangsstatus ';
	                $this->_SQL .= 'SET vvs_export_dwh = :var_N0_vvd_bit + vvs_export_dwh ';
	
	                if($FilNr == '')
	                {
	                	/*	                    
					    $BindeVariablen['var_N0_fil_id']=$this->_FilNr;
					    $BindeVariablen['var_N0_vvs_wanr']=$this->_WaNr;
	                	*/
	                	$this->_DB->SetzeBindevariable('VVS', 'var_N0_fil_id',$this->_FilNr , awisDatenbank::VAR_TYP_GANZEZAHL);
	                	$this->_DB->SetzeBindevariable('VVS', 'var_N0_vvs_wanr',$this->_WaNr , awisDatenbank::VAR_TYP_GANZEZAHL);
	                	
	                }
	                else
	                {
	                	/*
					    $BindeVariablen['var_N0_fil_id']=$FilNr;
					    $BindeVariablen['var_N0_vvs_wanr']=$WANr; 			    
	                	*/
	                	$this->_DB->SetzeBindevariable('VVS', 'var_N0_fil_id',$FilNr , awisDatenbank::VAR_TYP_GANZEZAHL);
	                	$this->_DB->SetzeBindevariable('VVS', 'var_N0_vvs_wanr',$WANr , awisDatenbank::VAR_TYP_GANZEZAHL);
	                	 
	                	
	                }
			    		$this->_SQL .= 'WHERE vvs_filid = :var_N0_fil_id ';
	                    $this->_SQL .= 'AND vvs_wanr = :var_N0_vvs_wanr';
	            }
				
	            $this->_DB->Ausfuehren($this->_SQL,'',true,$this->_DB->Bindevariablen('VVS',true));
	           	    
	        }
	        
    	}
    	catch(Exception $e)
        {
            echo ('Exception:'.$e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }

    /**
     * Schreibt �nderungen die an Scheibenrep.Vorgaengen vorgenommen
     * wurden ins DWH zurueck.
     * @param int $FilNr
     * @param int $WANr
     */
    public function SchreibeRepInDWH($FilNr='',$WANr='')
    {
    	try
    	{
			if($FilNr == '')
	        {
	            $FilNr = $this->_FilNr;
	            $WANr = $this->_WaNr;            
	        }
	        
	        $BindeVariablen=array();
	        $BindeVariablen['var_N0_fil_id']=$FilNr;
	        $BindeVariablen['var_N0_wanr']=$WANr;
	        
	    	$SQL = 'SELECT p.filnr, p.wanr, sum(p.vk_preis * p.menge) as BETRAG_KASSE, v.vvs_versnr ';
	        $SQL .= 'FROM v_vers_form_posdaten p INNER JOIN versvorgangsstatus v ';
	        $SQL .= 'ON p.filnr = v.vvs_filid AND p.wanr = v.vvs_wanr ';        
	        $SQL .= 'WHERE filnr = :var_N0_fil_id ';
	        $SQL .= 'AND wanr = :var_N0_wanr ';
	        $SQL .= 'AND artnr = \'1EIN69\' ';        
	        $SQL .= 'GROUP BY p.filnr, p.wanr, v.vvs_versnr';
		
		    if($this->_DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen) > 0)
	        {
	            $rsRepDWH = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);
	
	            while(!$rsRepDWH->EOF())
	            {
	                /*
	                 * pr�fen ob Vorgang schon mal in glasrep weggeschrieben wurde,
	                 * wenn ja, dann nur Betrag updaten, wenn nein dann Insert
	                 */
	
	                $BindeVariablen=array();
	        		$BindeVariablen['var_N0_fil_id']=$FilNr;
	        		$BindeVariablen['var_N0_wanr']=$WANr;
	        
	                $SQL = 'SELECT filnr, wanr, vers_nr, betrag_kasse, ausbuchung ';
	                $SQL .= 'FROM dwh.glasrep@DWH ';
	                $SQL .= 'WHERE filnr = :var_N0_fil_id ';
	                $SQL .= 'AND wanr = :var_N0_wanr';
			
	                if($this->_DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen) > 0)
	                {
	                    $SQL = 'UPDATE dwh.glasrep@DWH ';
	                    $SQL .= 'SET betrag_kasse = '.$this->_DB->FeldInhaltFormat('N2', $rsRepDWH->FeldInhalt('BETRAG_KASSE')).', ';
	                    $SQL .= "glasrep_user = 'awis', ";
    					$SQL .= "glasrep_userdat = sysdate, ";
                        $SQL .= "verclient = 'V0', ";
                        $SQL .= "ausb_srep = 0, ";
                        $SQL .= "filnl_srep = 0 ";
	                    $SQL .= 'WHERE filnr = '.$FilNr.' ';
	                    $SQL .= 'AND wanr = '.$WANr;
	                }
	                else
	                {
						// Betrag_Kasse wird als Komma-getrennte Zahl geliefert was beim Insert zu Feldverschiebungen f�hren w�rde
						// daher Komma durch Punkt ersetzen
						$betragKasse = str_replace(",", ".", $rsRepDWH->FeldInhalt('BETRAG_KASSE'));
			
						$SQL = "INSERT INTO dwh.glasrep@DWH (filnr,wanr,vers_nr,betrag_kasse,glasrep_user,glasrep_userdat,verclient,ausb_srep,filnl_srep) ";
						$SQL .= "VALUES (";
						$SQL .= $rsRepDWH->FeldInhalt('FILNR').", ";
						$SQL .= $rsRepDWH->FeldInhalt('WANR').", ";
						$SQL .= $rsRepDWH->FeldInhalt('VVS_VERSNR').", ";
						$SQL .= $betragKasse.', ';
						$SQL .= "'awis', ";
						$SQL .= "sysdate, ";
                        $SQL .= "'V0', ";
                        $SQL .= "0, ";
                        $SQL .= "0)";
			        }
	
	                $this->_DB->Ausfuehren($SQL,'',true);
	
	                $rsRepDWH->DSWeiter();
	            }
	        }
    	}
    	catch(Exception $e)
        {
            echo ('Exception:'.$e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }

    public function SchreibeModulStatus($Modulname,$Statustext)
    {
    	try
    	{
	        $BindeVariablen=array();
	        $BindeVariablen['var_T_vjs_modulstatus']=$Statustext;
	        $BindeVariablen['var_T_vjs_modul']=$Modulname;        
	    	
	    	$SQL = 'UPDATE versjobsteuerung ';
	        $SQL .= 'SET vjs_modulstatus = :var_T_vjs_modulstatus ';
	        $SQL .= 'WHERE vjs_modul = :var_T_vjs_modul';
	
	        $this->_DB->Ausfuehren($SQL,'',false,$BindeVariablen);
    	}
    	catch(Exception $e)
        {
            echo ('Exception:'.$e->getMessage());
            die();
        }
        catch(awisException $awis)
        {
            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
            die();
        }
    }

    public function SchreibeExportStatus($ExpDateiname,$Status,$Pruefsumme='')
    {
    	try
    	{
	        // aktuelle JobID ermitteln
	        $SQL = 'SELECT * ';
	        $SQL .= 'FROM versjobid';
	
	        $rsJobID = $this->_DB->RecordSetOeffnen($SQL);
	        $JobId = $rsJobID->FeldInhalt('VJI_JOBID');
	
	        if($Status == 'Start')
	        {
	            $BindeVariablen=array();
	            $BindeVariablen['var_N0_vep_jobid']=$JobId;
	            $BindeVariablen['var_T_vep_dateiname']=$ExpDateiname;
	        	
	        	$SQL = 'INSERT INTO versexportprotokoll (vep_jobid,vep_dateiname,vep_datumstart) ';
	            $SQL .= 'VALUES (:var_N0_vep_jobid, :var_T_vep_dateiname, sysdate)';
	        }
	        elseif($Status == 'Ende')
	        {
	            $BindeVariablen=array();
	            $BindeVariablen['var_T_vep_pruefsumme']=$Pruefsumme;
	            $BindeVariablen['var_T_vep_dateiname']=$ExpDateiname;
	            
	        	$SQL = 'UPDATE versexportprotokoll ';
	            $SQL .= 'SET vep_datumende = sysdate, vep_pruefsumme = :var_T_vep_pruefsumme ';
	            $SQL .= 'WHERE vep_dateiname = :var_T_vep_dateiname';
	        }
	
	        $this->_DB->Ausfuehren($SQL,'',false,$BindeVariablen);
    	}
    	catch(Exception $e)
        {
            echo ('Exception:'.$e->getMessage());
            die();
        }
        catch(awisException $awis)
        {
            echo ('Exception:'.$awis->getMessage().$awis->getSQL());
            die();
        }
    }

    public function CheckExportStatus($ExpDateiname)
    {
        /*
        $SQL = 'SELECT * ';
        $SQL .= 'FROM versexportprotokoll INNER JOIN versjobid ';
        $SQL .= 'ON vep_jobid = vji_jobid ';
        $SQL .= 'WHERE vep_dateiname = \''.$ExpDateiname.'\' ';
        $SQL .= 'AND vep_datumende is not null';
        */
        
        $BindeVariablen=array();
        $BindeVariablen['var_T_vep_dateiname']=$ExpDateiname;
            
        $SQL = 'SELECT * ';
        $SQL .= 'FROM versexportprotokoll ';
        $SQL .= 'WHERE vep_dateiname = :var_T_vep_dateiname ';
        $SQL .= 'AND vep_datumende is not null';

        if($this->_DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen) == 0)
        {
            $Rueckgabe = false;
        }
        else
        {
            $Rueckgabe = true;
        }

        return $Rueckgabe;
    }
    
    public function ErstelleExportDatei($DateinameExport, $ExpNameTabelle)
    {
    	$BindeVariablen=array();
    	$BindeVariablen['var_T_vve_exportdatei']=$ExpNameTabelle;
    	
    	if($this->CheckExportStatus($DateinameExport) === false)
    	{
    		$this->_SQL = 'SELECT ve.vek_datensatz ';
    		$this->_SQL .= 'FROM versversicherungen vv INNER JOIN versexport ve ';
    		$this->_SQL .= 'ON vv.vve_versnr = ve.vek_versnr ';
    		$this->_SQL .= 'WHERE vv.vve_exportdatei = :var_T_vve_exportdatei';
    		$this->_SQL .= 'ORDER BY vek_key';
    		 
    		$this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Vers-Datei');
    		$this->SchreibeExportStatus($DateinameExport, 'Start');
    		 
    		if(($fd = fopen($this->_PfadExportVers.$DateinameExport,'w')) === false)
    		{
    			echo $this->error_msg = "Fehler beim Erstellen der Datei";
    			die();
    		}
    		 
    		if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
    		{
    			$rsExport = $this->_DB->RecordSetOeffnen($this->_SQL);
    			 
    			while(!$rsExport->EOF())
    			{
    				fputs($fd,$rsExport->FeldInhalt('VEK_DATENSATZ').chr(13).chr(10));
    				$rsExport->DSWeiter();
    			}
    			 
    		}
    		fclose($fd);
    		$this->SchreibeExportStatus($DateinameExport, 'Ende', md5_file($this->_PfadExportVers.$DateinameExport));
    	}
    }
    public function EntferneBuchstaben($QuellString)
    {
        $Laenge = strlen($QuellString);
        $ZielString = '';

        for($i=0;$i<$Laenge;$i++)
        {
            $Zeichen = substr($QuellString, $i, 1);
            if(ord($Zeichen) >= 65 && ord($Zeichen) <= 90 || ord($Zeichen) >= 97 && ord($Zeichen) <= 122)
            {

            }
            else
            {
                $ZielString .= $Zeichen;
            }
        }

        return $ZielString;
    }

    public function FormatiereSchadenNr($SchadenNr)
    {
        $suchen = array(' ', '-','/','.');
        $ersetzen = array('','','','');

        for($x=0;$x < count($suchen,0);$x++)
        {
            $SchadenNr = str_replace($suchen[$x],$ersetzen[$x],$SchadenNr);
        }

        if(substr($SchadenNr,0,2) == '07')
        {
            $SchadenNr = '07'.substr($SchadenNr, 2, 7);
        }
        else
        {
            $SchadenNr = '';
        }

        return $SchadenNr;
    }

    /**
     * Methode pr�ft ob das Schadendatum erheblich vom Filialannahmedatum abweicht (> 10 Jahre)
     * Wenn ja dann wird WA-Anlagedatum aus der Filiale zur�ckgegeben.
     *
     * @param date $SchadenDatum
     * @param int $FilNr
     * @param int $VorgangNr
     * @return date $SchadenDatum
     */
    public function pruefeSchadenDatum($SchadenDatum,$FilNr,$VorgangNr)
    {
        // Grenze in Jahren (ab wieviel Jahren soll nicht das Schadendatum aus Fil
        // sondern aus Anlagedatum des WA genommen werden.
        $diffGrenze = 10;

        $BindeVariablen=array();
        $BindeVariablen['var_N0_fil_id']=$FilNr;
        $BindeVariablen['var_N0_wanr']=$VorgangNr;
        
        $SQL = "SELECT min(a.datum) as datum ";
        $SQL .= "FROM v_vers_form_bearbdaten a ";
        $SQL .= "WHERE a.filnr = :var_N0_fil_id and a.wanr = :var_N0_wanr";

        $rsDatum = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        $datetime1 = strtotime($SchadenDatum);
        $datetime2 = strtotime($rsDatum->FeldInhalt('DATUM'));

        $differenz = $datetime1-$datetime2;
        
        if($differenz/86400 < $diffGrenze * -365)
        {
            $SchadenDatum = $rsDatum->FeldInhalt('DATUM');
        }
        
        return $SchadenDatum;
    }

	

}

?>


