<?php
require_once 'reifen_funktionen.inc';

try
{
    $REX = new reifen_funktionen();
    $Speichern = isset($REX->Param['SPEICHERN']) && $REX->Param['SPEICHERN'] == 'on'?true:false;

    $REX->RechteMeldung(0);//Anzeigen Recht
	echo "<form name=frmSuche method=post action=./reifen_Main.php?cmdAktion=Details>";

	$REX->Form->Formular_Start();
    
	$REX->Form->ZeileStart();
	$REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['AST']['AST_ATUNR'].':',190);
	$REX->Form->Erstelle_TextFeld('*REX_AST_ATUNR',($Speichern?$REX->Param['REX_AST_ATUNR']:''),10,110,true);
	$REX->AWISCursorPosition='sucREX_AST_ATUNR';
	$REX->Form->ZeileEnde();


    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['Wort']['DatumVom'].':',190);
    $REX->Form->Erstelle_TextFeld('*REX_REKLAMATIONSDATUM_VON',($Speichern?$REX->Param['REX_REKLAMATIONSDATUM_VON']:''),10,150,true,'','','','D');
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['Wort']['DatumBis'].':',100);
    $REX->Form->Erstelle_TextFeld('*REX_REKLAMATIONSDATUM_BIS',($Speichern?$REX->Param['REX_REKLAMATIONSDATUM_BIS']:''),10,150,true,'','','','D');
    $REX->Form->ZeileEnde();

    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['REX']['REX_WERK'].':',190);
    $REX->Form->Erstelle_TextFeld('*REX_WERK',($Speichern?$REX->Param['REX_WERK']:''),10,110,true);
    $REX->Form->ZeileEnde();


    // Auswahl kann gespeichert werden
    $REX->Form->ZeileStart();
    $REX->Form->Erstelle_TextLabel($REX->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $REX->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $REX->Form->ZeileEnde();

	$REX->Form->Formular_Ende();

	$REX->Form->SchaltflaechenStart();
	$REX->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$REX->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$REX->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $REX->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($REX->Recht52000&4) == 4){
		$REX->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $REX->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$REX->Form->SchaltflaechenEnde();

	$REX->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($REX->Form instanceof awisFormular)
	{
		$REX->Form->DebugAusgabe(1, $ex->getSQL());
		$REX->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$REX->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($REX->Form instanceof awisFormular)
	{
		$REX->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>