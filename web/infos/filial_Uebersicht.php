<?php
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;

require_once("db.inc.php");		// DB-Befehle
require_once("register.inc.php");
require_once("sicherheit.inc.php");
require_once("sicherheit.inc.php");
								// Benutzerdefinierte CSS Datei
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";


include ("ATU_Header.php");	// Kopfzeile

define('FPDF_FONTPATH','font/');
require 'fpdi.php';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung möglich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}


$RechteStufe = awisBenutzerRecht($con,1202);	// Informationen --> Filialübersicht
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Filialuebersicht', $AWISBenutzer->BenutzerName(), '', '', '');
     die("Keine ausreichenden Rechte!");
}

if(isset($_POST['cmdUebersicht_x']))
{
	echo '<h3>Gebietsübersicht wird erstellt...</h3>';
	flush();
	
	// In dieser Datei wird das Objekt pdf erstellt und befüllt.
	// Anschließend muss es nur noch gespeichert werden...
	include "../personaleinsatz/perseins_Auswertungen_Gebietsuebersicht.php";
	
	// PDF Datei speichern
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->saveas($DateiName);
	echo "<br><a target=_new href=$DateiNameLink>Gebietsübersicht als PDF Datei öffnen</a>";
	
	echo "<br><hr><input type=image alt=Zurück src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./filial_Uebersicht.php';>";
	
	awislogoff($con);
}
elseif (isset($_POST['cmdUebersichtAusland_x']))
{
	echo '<h3>Gebietsübersicht wird erstellt...</h3>';
	flush();
	
	// In dieser Datei wird das Objekt pdf erstellt und befüllt.
	// Anschließend muss es nur noch gespeichert werden...
	include "../personaleinsatz/perseins_Auswertungen_GebietsuebersichtAusland.php";
	
	// PDF Datei speichern
	
	$DateiName = awis_UserExportDateiName('.pdf');
	$DateiNameLink = pathinfo($DateiName);
	$DateiNameLink = '/export/' . $DateiNameLink['basename'];
	$pdf->saveas($DateiName);
	echo "<br><a target=_new href=$DateiNameLink>Gebietsübersicht (Ausland) als PDF Datei öffnen</a>";
	
	echo "<br><hr><input type=image alt=Zurück src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./filial_Uebersicht.php';>";
	
	awislogoff($con);
}
else
{
	echo '<form name=frmUebersicht method=post action=./filial_Uebersicht.php>';
	
	echo "<h1>Gebiets&uuml;bersicht der Filialen</h1><p>";
	
	echo "<table width=100% id=DatenTabelle>";
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr>";
	echo "<td width=50>&nbsp;<input type=image border=0 src=/bilder/national.png name=cmdUebersicht title='Gebiets&uuml;bersicht Deutschland'></td>";
	echo "<td>Gebiets&uuml;bersicht Deutschland erstellen</td>";
	echo "</tr>";
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr>";
	echo "<td width=50>&nbsp;<input type=image border=0 src=/bilder/europa.png name=cmdUebersichtAusland title='Gebiets&uuml;bersicht Ausland'></td>";
	echo "<td>Gebiets&uuml;bersicht Ausland erstellen</td>";
	echo "</tr>";
	echo "<tr><td>&nbsp;</td></tr>";
	echo '</table>';
	
	echo '</form>';
	echo "<hr><input type=image alt=Zurück src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./infos_Main.php';>";		
}
?>