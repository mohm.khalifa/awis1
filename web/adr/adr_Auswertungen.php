<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $Recht4303;

require_once('awisFilialen.inc');
require_once('register.inc.php');
require_once('db.inc.php');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	
    $TextKonserven = array();
    $TextKonserven[]=array('ADR','*');
    $TextKonserven[]=array('ADK','*');
    $TextKonserven[]=array('ADP','*');
    $TextKonserven[]=array('ADH','*');
    $TextKonserven[]=array('ADS','*');
    $TextKonserven[]=array('ADX','*');
    
    $TextKonserven[]=array('BUL','BUL_BUNDESLAND');
    $TextKonserven[]=array('FIL','FIL_ID');
    $TextKonserven[]=array('FIL','FIL_GEBIET');
    $TextKonserven[]=array('FIL','FIL_BEZ');
    
    $TextKonserven[]=array('FIB','FIB_BESTAND');
    $TextKonserven[]=array('FIB','FIB_BESTAND_kurz');
    
    $TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','Soll');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','Altoel');
	
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	
    $Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4303 = $AWISBenutzer->HatDasRecht(4303);

	if(($Recht4303&1)==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	$Form->DebugAusgabe(1,$_GET);
	$Form->DebugAusgabe(1,$_POST);
	
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_AdrAuswertungen'));
	$Form->DebugAusgabe(1,$Param);
	if(!isset($Param['KEY']))
	{
		$Param = array();
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['SPEICHERN']='off';
		$Param['DATUM_VOM']='';
		$Param['DATUM_BIS']='';
	}
    
    $Form->SchreibeHTMLCode('<form name="frmAdrAuswertungen" action="./adr_Main.php?cmdAktion=Auswertungen" method="POST">');
			
	$Form->Formular_Start();	
	
	// Wiederholt auf der Seite oder neuer Aufruf?
	$WerteVorblenden='on';
	if (isset($_POST['sucADK_FIL_ID']))		// Neue Werte eingegeben
	{
		$Param['ADK_FIL_ID']=$Form->Format('T',$_POST['sucADK_FIL_ID'],true);
		$Param['BUL_ID']=$Form->Format('T',$_POST['sucBUL_ID'],true);
		$Param['DATUM_VOM']=isset($_POST['sucDATUM_VOM'])?$Form->Format('D',$_POST['sucDATUM_VOM'],true):$Param['DATUM_VOM'];
		$Param['DATUM_BIS']=isset($_POST['sucDATUM_BIS'])?$Form->Format('D',$_POST['sucDATUM_BIS'],true):$Param['DATUM_BIS'];
		$Param['ADK_LAGERKZ']=$Form->Format('T',$_POST['sucADK_LAGERKZ'],true);
		$Param['ALTOEL']=isset($_POST['sucAltoel'])?$_POST['sucAltoel']:'';
		$Param['SPEICHERN']=isset($_POST['txtAuswahlSpeichern'])?'on':'off';
	}
	elseif(isset($_GET['Neu']) AND $Param['SPEICHERN']=='off')
	{
		$Param['ADK_FIL_ID']='';
		$Param['BUL_ID']='';
		$Param['DATUM_VOM']=date('d.m.Y', mktime(0,0,0,date('m')-1,date('d'),date('Y')));
		$Param['DATUM_BIS']=date("d.m.Y",mktime(0,0,0,date('m'),date('d'),date('Y')));
		$Param['ADK_LAGERKZ']='';
		$Param['ALTOEL']='';
		$Param['ORDER']='';
		$Param['ORDER2']='';
		$Param['BLOCK']='';
		$WerteVorblenden='off';
	}		
	
	$Form->DebugAusgabe(1,$Param);
    
	$AWISCursorPosition = 'txtADK_FIL_ID';

	//Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_FIL_ID'].':',190);
	$FilialListe=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	if($FilialListe!='')
	{
		$SQL = 'SELECT FIL_ID, FIL_ID || \' - \' || FIL_BEZ';
		$SQL .= ' FROM FILIALEN';
		$SQL .= ' WHERE FIL_ID IN('.$FilialListe.')';
		$SQL .= ' ORDER BY 1';
		$Form->Erstelle_SelectFeld('*ADK_FIL_ID',($WerteVorblenden=='on'?$Param['ADK_FIL_ID']:'0'),400,true,$SQL);
	}
	else
	{
		$Form->Erstelle_TextFeld('*ADK_FIL_ID',$WerteVorblenden=='on'?$Param['ADK_FIL_ID']:'',20,200,true,'','','','T','L','','',10);
	}
	$Form->ZeileEnde();
	
	// Bundesland
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BUL']['BUL_BUNDESLAND'].':',190);
	$SQL = "SELECT BUL_ID, BUL_BUNDESLAND FROM BUNDESLAENDER WHERE BUL_LAND IS NOT NULL AND BUL_LAND='D' ORDER BY BUL_BUNDESLAND";
	$Form->Erstelle_SelectFeld('*BUL_ID',($WerteVorblenden=='on'?$Param['BUL_ID']:'0'),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
	$Form->ZeileEnde();
	
	// TODO: Vorbelegung Datumswerte �ber Parameter steuern
    // Datumsbereich vom festlegen, aktuell 1.1.YYYY
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
	$Form->Erstelle_TextFeld('*DATUM_VOM',$Param['DATUM_VOM'],20,200,true,'','','','D','L','','',10);
	$Form->ZeileEnde();
	
	// Datumsbereich bis festlegen, aktuell: Tagesdatum - 1Monat
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',190);
	$Form->Erstelle_TextFeld('*DATUM_BIS',$Param['DATUM_BIS'],20,200,true,'','','','D','L','','',10);		
	$Form->ZeileEnde();

	$FilialListe=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	//$FilialListe='90,100';
	
	//LagerKennzeichen
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_LAGERKZ'].':',190);
	$Lagerkz = explode("|",$AWISSprachKonserven['ADK']['lst_ADK_LAGERKZ']);	
	$Form->Erstelle_SelectFeld('*ADK_LAGERKZ',($WerteVorblenden=='on'?$Param['ADK_LAGERKZ']:''),150,true,'','0~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE'],'','','',$Lagerkz);		
	$Form->ZeileEnde();
		
	// Nur Anzeige Alt�l
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Altoel'].':',190);
	$Altoel = explode("|",$AWISSprachKonserven['ADR']['lst_AdrAltoel']);	
	$Form->Erstelle_SelectFeld('*Altoel',($WerteVorblenden=='on'?$Param['ALTOEL']:''),150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Altoel);		
	//$Form->Erstelle_Checkbox('*Altoel',($WerteVorblenden=='on'?$Param['ALTOEL']:'off'),30,true,'on','',$AWISSprachKonserven['Wort']['Altoel']);
	$Form->ZeileEnde();
	
	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',187);
	$Form->Erstelle_Checkbox('AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':'off'),30,true,'on','',$AWISSprachKonserven['Wort']['AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	//Neu ist nur bei erstem Aufruf gesetzt, d.h. nichts machen
	if (!isset($_GET['Neu']))
	{

		//*****************************************************************
		// Sortierung aufbauen
		//*****************************************************************
		if (!isset($_GET['Sort']))
		{
			if (isset($Param['ORDER']) and $Param['ORDER'] != '')
			{
				$ORDERBY = $Param['ORDER'];
			}
			else
			{
				$ORDERBY = ' ADS_EWCCODE ASC';
			}
			if (isset($Param['ORDER2']) and $Param['ORDER2'] != '')
			{
				$ORDERBY2 = $Param['ORDER2'];
			}
			else
			{
				$ORDERBY2 = ' ADK_FIL_ID ASC, adk.adk_datumabschluss DESC';
			}
		}
		else
		{
			$ORDERBY = ' ' . str_replace ( '~', ' DESC ', $_GET ['Sort'] );
			$ORDERBY2 = ' ' . str_replace ( '~', ' DESC ', $_GET ['Sort'] );
		}
		$Param['ORDER'] = $ORDERBY;
		$Param['ORDER2'] = $ORDERBY2;
		$AWISBenutzer->ParameterSchreiben('Formular_AdrAuswertungen',serialize($Param));

		//********************************************************
		// Bedingung erstellen
		//********************************************************
		$BindeVariablen = array();
		$Bedingung = _BedingungErstellen($Param,$BindeVariablen);
		
		//********************************************************
		// SQL erstellen
		//********************************************************
		if(isset($Param['ALTOEL']) AND $Param['ALTOEL']=='off')
		{
    		$SQL  = 'SELECT';
    		$SQL .= ' DAT.*,';
            $SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY . ') AS ZEILENNR';
    		$SQL .= ' FROM ';
    		$SQL .= ' (';
    		$SQL .= ' SELECT ';
            $SQL .= ' ADS_LAGACODE,';
            $SQL .= ' ADS_LAGABEZ,';
            $SQL .= ' ADS_EWCCODE,';
            $SQL .= ' ADS_EWCBEZ,';
            $SQL .= ' SUM(ADP_MENGE) AS ADP_MENGE,';
            $SQL .= ' ROUND(SUM(ADP_GEWICHT)) AS ADP_GEWICHT';
            if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
            {
                $SQL .= ' ,ADK_LAGERKZ';
            }
            $SQL .= ' FROM ADRKOPF adk';
            $SQL .= ' INNER JOIN ADRPOS adp';
            $SQL .= ' ON adp.adp_adk_key=adk.adk_key';
            $SQL .= ' INNER JOIN ADRSTAMM ads';
            $SQL .= ' ON ads.ads_key=adp.adp_ads_key';
    		if($Bedingung!='')
    		{
    			$SQL .= ' WHERE ' . substr($Bedingung,4);
    		}		
            $SQL .= ' GROUP BY';
            $SQL .= ' ADS_LAGACODE,';
            $SQL .= ' ADS_LAGABEZ,';
            $SQL .= ' ADS_EWCCODE,';
            $SQL .= ' ADS_EWCBEZ';
            if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
            {
                $SQL .= ' ,ADK_LAGERKZ';
            }
    		$SQL .= ' ) DAT';
    		$SQL .= ' ORDER BY ' .$ORDERBY;
		}
		else
		{
            $SQL  = ' SELECT';
            $SQL .= ' ADK_FIL_ID,';
            $SQL .= ' ADK_DATUMABSCHLUSS,';
            $SQL .= ' ADK_LAGERKZ,';
            $SQL .= ' ADS_EWCCODE,';
            $SQL .= ' ADS_EWCBEZ,';
            $SQL .= ' ADP_MENGE,';
            $SQL .= ' ROW_NUMBER () OVER (ORDER BY ' .$ORDERBY2 . ') AS ZEILENNR';
            $SQL .= ' FROM ADRKOPF adk';
            $SQL .= ' INNER JOIN ADRPOS adp';
            $SQL .= ' ON adp.adp_adk_key=adk.adk_key';
            $SQL .= ' INNER JOIN ADRSTAMM ads';
            $SQL .= ' ON ads.ads_key=adp.adp_ads_key';
            if($Bedingung!='')
            {
            	$SQL .= ' WHERE ' . substr($Bedingung,4);
            }		
            $SQL .= ' ORDER BY ' .$ORDERBY2;
		}
		
		//*****************************************************************
		// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
		//*****************************************************************
		if(($AWIS_KEY1<=0))
		{
			//************************************************
			// Aktuellen Datenblock festlegen
			//************************************************
			$Block = 1;
			if(isset($_REQUEST['Block']))
			{
				$Block=$Form->Format('N0',$_REQUEST['Block'],false);
				$Param['BLOCK']=$Block;
			}
			elseif(isset($Param['BLOCK']) and ($Param['BLOCK'] != ''))
			{
				$Block=intval($Param['BLOCK']);
			}
	
			//************************************************
			// Zeilen begrenzen
			//************************************************
			$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
			$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
			$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);

			$BindeVariablen['var_N0_ZEILE_VON'] = $Form->Format('Z',$StartZeile,false);
			$BindeVariablen['var_N0_ZEILE_BIS'] = $Form->Format('Z',$StartZeile+$ZeilenProSeite,false) ;
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr >= :var_N0_ZEILE_VON AND  ZeilenNr < :var_N0_ZEILE_BIS';
			//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
			$Form->DebugAusgabe(1,$BindeVariablen);
		}
		else
		{
			$MaxDS = 1;
			$ZeilenProSeite=1;
			$Block = 1;
		}
		
		$rsAdr = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
		$Form->DebugAusgabe(1,$DB->LetzterSQL());
		
    	// Anzeige Erzeugernummer bei Auswahl einer Filiale
    	if(isset($Param['ADK_FIL_ID']) AND $Param['ADK_FIL_ID']!='')
    	{
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['ADK']['ADK_ERZEUGERNUMMER'].':',187);
        	$BindeVariablen=array();
        	$BindeVariablen['var_N_FIL_ID']=$Param['ADK_FIL_ID'];
        	$SQL = 'SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIT_ID=911 AND FIF_FIL_ID = :var_N_FIL_ID';
        	$rsFil = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
        	$Form->Erstelle_TextFeld('ERZEUGERNUMMER',$rsFil->FeldInhalt('FIF_WERT'),20,200,false,'','','','T','L','','',10);		
        	$Form->ZeileEnde();
    	}
		
	    if($rsAdr->EOF())
		{
			echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
		} 
		elseif (($rsAdr->AnzahlDatensaetze()>0) or (isset($_REQUEST['Liste']))) // Liste anzeigen
		{
			$Form->Formular_Start();

			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			if(isset($Param['ALTOEL']) AND $Param['ALTOEL']=='off')
			{
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADS_LAGACODE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADS_LAGACODE'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['ADS']['ADS_LAGACODE_kurz'], 70, '', $Link );
    	
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADS_LAGABEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADS_LAGABEZ'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_LAGABEZ'],350,'',$Link);			
    			
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADS_EWCCODE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADS_EWCCODE'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_EWCCODE_kurz'],70,'',$Link);			
    			
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADS_EWCBEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADS_EWCBEZ'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_EWCBEZ'],350,'text-align:left',$Link);			
    			
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADP_MENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADP_MENGE'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADP']['ADP_MENGE'],80,'text-align:right',$Link);			
    			
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADP_GEWICHT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADP_GEWICHT'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADP']['ADP_GEWICHT'],80,'text-align:right',$Link);			
    			
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADK_LAGERKZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADK_LAGERKZ'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_LAGERKZ'],80,'text-align:right',$Link);			
    			
    			$Form->ZeileEnde ();
    
    			$DS = 0;
    			while (!$rsAdr->EOF())
    			{
    				$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
    				
    				$Form->Erstelle_ListenFeld('ADS_LAGACODE',$rsAdr->FeldInhalt('ADS_LAGACODE'),0,70,false,($DS%2),'','','T','L');
    				$Form->Erstelle_ListenFeld('ADS_LAGABEZ',$rsAdr->FeldInhalt('ADS_LAGABEZ'),0,350,false,($DS%2),'','','T','L');
    				$Form->Erstelle_ListenFeld('ADS_EWCCODE',$rsAdr->FeldInhalt('ADS_EWCCODE'),0,70,false,($DS%2),'','','T','L');
    				$Form->Erstelle_ListenFeld('ADS_EWCBEZ',$rsAdr->FeldInhalt('ADS_EWCBEZ'),0,350,false,($DS%2),'','','T','L');
    				$Form->Erstelle_ListenFeld('ADP_MENGE',$rsAdr->FeldInhalt('ADP_MENGE'),0,80,false,($DS%2),'','','T','R');
    				$Form->Erstelle_ListenFeld('ADP_GEWICHT',$rsAdr->FeldInhalt('ADP_GEWICHT'),0,80,false,($DS%2),'','','T','R');
                    if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
                    {
                        $LKZ=$rsAdr->FeldInhalt('ADK_LAGERKZ');
                    }
                    else
                    {
                        $LKZ='alle';
                    }
    				$Form->Erstelle_ListenFeld('ADK_LAGERKZ',$LKZ,0,80,false,($DS%2),'','','T','R');
    				
    				$Form->ZeileEnde();
    				
    				$rsAdr->DSWeiter();
    				$DS ++;
    			}
			}
			else
			{
    			$Link = './adr_Main.php?cmdAktion=Auswertungen'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
    			$Link .= '&Sort=ADK_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADK_FIL_ID'))?'~':'');
    			$Form->Erstelle_Liste_Ueberschrift ( $AWISSprachKonserven ['ADK']['ADK_FIL_ID'], 65, '',$Link);
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_EWCCODE_kurz'],65,'');			
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADS']['ADS_EWCBEZ'],350,'text-align:left');			
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_DATUMABSCHLUSS'],120,'text-align:right');			
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADP']['ADP_MENGE'],80,'text-align:right');			
    			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADK']['ADK_LAGERKZ'],80,'text-align:right');			
    			
    			$Form->ZeileEnde ();
    
    			$DS = 0;
    			while (!$rsAdr->EOF())
    			{
    				$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
    				
    				$Form->Erstelle_ListenFeld('ADK_FIL_ID',$rsAdr->FeldInhalt('ADK_FIL_ID'),0,65,false,($DS%2),'','','T','L');
    				$Form->Erstelle_ListenFeld('ADS_EWCCODE',$rsAdr->FeldInhalt('ADS_EWCCODE'),0,65,false,($DS%2),'','','T','L');
    				$Form->Erstelle_ListenFeld('ADS_EWCBEZ',$rsAdr->FeldInhalt('ADS_EWCBEZ'),0,350,false,($DS%2),'','','T','L');
    				$Form->Erstelle_ListenFeld('ADK_DATUMABSCHLUSS',$rsAdr->FeldInhalt('ADK_DATUMABSCHLUSS'),0,120,false,($DS%2),'','','T','R');
    				$Form->Erstelle_ListenFeld('ADP_MENGE',$rsAdr->FeldInhalt('ADP_MENGE'),0,80,false,($DS%2),'','','T','R');
    				$Form->Erstelle_ListenFeld('ADK_LAGERKZ',$rsAdr->FeldInhalt('ADK_LAGERKZ'),0,80,false,($DS%2),'','','T','R');
    				
    				$Form->ZeileEnde();
    				
    				$rsAdr->DSWeiter();
    				$DS ++;
    			}
			}	
			$Link = './adr_Main.php?cmdAktion=Auswertungen&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
			$Form->BlaetternZeile ($MaxDS, $ZeilenProSeite, $Link, $Block, '');

			$Form->Formular_Ende();
		}
	}
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	
	$Form->SchaltflaechenStart();	
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');		
    if (!isset($_GET['Neu']))
    {
    	if(isset($Param['ALTOEL']) AND $Param['ALTOEL']=='')
        {
        	if (($Recht4303&2)==2)
        	{
        		$LinkPDF = '/berichte/drucken.php?XRE=30&ID';
        		$Form->Schaltflaeche('href', 'cmdPDF', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen'], 'P');		
		        $LinkCSV = './adr_Auswertungen_Export.php?cmdAktion=CSV';
		        $Form->Schaltflaeche('href', 'cmdCSV', $LinkCSV, '/bilder/cmd_korb_runter.png', $AWISSprachKonserven['ADR']['ttt_ExportRuecknahmen'], 'C');		
        	}
        }
        else if(isset($Param['ALTOEL']) AND $Param['ALTOEL']!='')
        {
        	if (($Recht4303&4)==4)
        	{
        		$LinkPDF = '/berichte/drucken.php?XRE=31&ID';
        		$Form->Schaltflaeche('href', 'cmdPDF', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen'], 'P');		
        	}
        }
    }
	$LinkErzeuger = './adr_Auswertungen_Export.php?cmdAktion=Erzeuger';
	$Form->Schaltflaeche('href', 'cmdCSV', $LinkErzeuger, '/bilder/cmd_recycling.png', $AWISSprachKonserven['ADR']['ttt_ExportErzeuger'], 'E');		
    $Form->SchaltflaechenEnde();

	$AWISBenutzer->ParameterSchreiben('Formular_AdrAuswertungen',serialize($Param));
	
	$Form->SetzeCursor ($AWISCursorPosition );
	$Form->SchreibeHTMLCode ('</form>');
}

catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201205161124");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201205161125");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

function _BedingungErstellen($Param, &$BindeVariablen)
{
	global $Form;
	global $AWISBenutzer;
	global $DB;
	
	$Bedingung = '';
	if(isset($Param['ADK_FIL_ID']) AND $Param['ADK_FIL_ID']!='')
	{
		$Bedingung .= ' AND ADK_FIL_ID = :var_N0_ADK_FIL_ID';
		$BindeVariablen['var_N0_ADK_FIL_ID'] = $Form->Format('N0',$Param['ADK_FIL_ID'],false);
	}
	
	if(isset($Param['BUL_ID']) AND $Param['BUL_ID']!='0')
	{
	    $Bedingung .= ' AND adk.adk_fil_id IN (SELECT FIF_FIL_ID FROM FILIALINFOS WHERE FIF_FIT_ID=65 AND FIF_WERT='.$Form->Format('T',$Param['BUL_ID'],false).' AND FIF_IMQ_ID=8)';
	}
	
	if(isset($Param['DATUM_VOM']) AND $Param['DATUM_VOM']!='')
	{
		$Bedingung .= ' AND TRUNC(adk_datumabschluss) >= :var_D_DATUM_VOM';
		$BindeVariablen['var_D_DATUM_VOM'] = $Form->Format('D',$Param['DATUM_VOM'],false);
	}
	
	if(isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS']!='')
	{
		$Bedingung .= ' AND TRUNC(adk_datumabschluss) <= :var_D_DATUM_BIS';
		$BindeVariablen['var_D_DATUM_BIS'] = $Form->Format('D',$Param['DATUM_BIS'],false);
	}

	if(isset($Param['ADK_LAGERKZ']) AND $Param['ADK_LAGERKZ']!='0')
	{
		$Bedingung .= ' AND ADK_LAGERKZ = :var_T_ADK_LAGERKZ';
		$BindeVariablen['var_T_ADK_LAGERKZ'] = $Form->Format('T',$Param['ADK_LAGERKZ'],false);
	}
	
	if(isset($Param['ALTOEL']) AND $Param['ALTOEL']=='')
	{
		$Bedingung .= " AND ADS_EWCCODE <> '130205*'";
	}
	
	if(isset($Param['ALTOEL']) AND $Param['ALTOEL']!='')
	{
		$Bedingung .= " AND ADS_EWCCODE = '130205*'";
		$Bedingung .= ' AND ADS_ADRNR = ' .$Form->Format('Z',$Param['ALTOEL'],false);
	}
	return $Bedingung;
}

?>

