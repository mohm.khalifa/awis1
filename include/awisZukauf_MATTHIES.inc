<?php
/**
 * Erweiterung der Klasse f�r den Lieferant MATTHIES
 * => Projekt wurde eingestellt
 * @author Sacha Kerres
 * @copyright ATU Weiden
 * @version 200911
 * @deprecated
 */
class awisZukauf_MATTHIES extends awisZukauf
{
    const LIE_NR = '8190';
    /**
     * WSDL Datei f�r die Abrufe der Daten
     *
     */
    const WSDL_DATEI = 'http://ws.matthies.de/jmoV2WS/services.wsdl';

    /**
     * Test-Kundennummer bei Matthies
     *
     */
    const KUNDENNUMMER = '677777009';

    /**
     * Testkennwort f�r einen Kundenaccount (Filiale)
     *
     */
    const KUNENKENNWORT = 'MBIT';

    /**
     * Benutzer bei Matthies f�r den SOAP Dienst
     *
     */
    const USERID= 'atu';
    /**
     * Kennwort bei Matthies f�r den SOAP Dienst
     *
     */
    const PASSWORT = 'ft66hE3cC';

    /**
     * SOAP Client
     *
     * @var SoapClient
     */
    private $_SoapClient = null;

    /**
     * Formobejkt f�r die Formatierung von Daten
     *
     * @var awisFormular
     */
    protected $_Form = null;

    /**
     * Preiseinheitentabelle
     *
     * @var array
     */
    private $_PreisEinheiten = array();

    /**
     * SOAP Transaction-ID f�r die Daten�bertragung
     *
     * @var string
    */
    private $_TransactionID = '';
    /**
     * Initialisierung
     *
     * @param int $DebugLevel
     * @param boolean $TestModus
     */
    public function __construct($DebugLevel = 0, $TestModus = false)
    {
        $this->_Form = new awisFormular();			// Zum Formatieren der Werte
        // Eigenschaften initialisieren
        $this->_LIE_NR = self::LIE_NR;
        $this->_DebugLevel = $DebugLevel;

        if($this->_DebugLevel>2)
        {
            echo 'Initialisiere MATTHIES...'.PHP_EOL;
            echo '   WSDL: '.($TestModus?self::WSDL_DATEI_TEST:self::WSDL_DATEI).PHP_EOL;
            echo '   Proxy:'.self::ZUK_PROXY_HOST.'/'.self::ZUK_PROXY_PORT.PHP_EOL;
        }

        parent::__construct();			// DB, etc. initialisieren

        ini_set('soap.wsdl_cache_enabled',0);

        ini_set('allow_call_time_pass_reference','true');

        $Params = array('trace'=>1,						// Protokoll
            'proxy_host' => self::ZUK_PROXY_HOST		// Proxy-Server
            ,'proxy_port' => self::ZUK_PROXY_PORT		// Port fuer den Proxy
            , 'encoding'=>'utf-8'
            , 'exceptions' => 0
        );

        $this->_SOAPClient = new SoapClient(self::WSDL_DATEI,$Params);
    }

    /**
     * Eine Bestellung einlesen
     *
     * @param string $WANummer
     * @param string $BestellDatumVom
     * @param string $BestellDatumBis
     * @param string $Status
     * @param string $Filiale
     * @return array
     */
    public function LeseBestellungen($WANummer='',$BestellDatumVom='',$BestellDatumBis='',$Status='', $Filiale='')
    {
        if($this->_DebugLevel>2)
        {
            echo 'Starte Abfrage bei MATTHIES...'.$this->_TransactionID.PHP_EOL;
        }

        $this->_Bestellungen = array();
        $this->_BestellungenAnzahl = 0;

        //******************************************
        //******************************************
        // Anfrage zusammenbauen
        //******************************************
        //******************************************
        $Befehl = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><jmoServiceRequest/>');
        $Anfrage = $Befehl->addChild('getorderstatus');
        $Anfrage->addAttribute('version','2.0.0.1');

        $Option = $Anfrage->addChild('order');
        $Option->addAttribute('type',1);

        if($this->_DebugLevel>8)
        {
            echo 'Rufe Daten ab mit:'.PHP_EOL;
            var_dump($Befehl);
        }
        $FehlerCode=0;
        $FehlerMeldung='';

        $Kennwort = sha1(self::PASSWORT);
        $Params = array(
            'userName'=>self::USERID
            ,'password'=>$Kennwort
            ,'version'=>'2.0.0.1'
            ,'method'=>'getorderstatus'
            ,'xmlDocument'=>$Befehl->asXML()
        );

        $Antwort = $this->_SOAPClient->JMOProcessRequest($Params);

        // Neue Artikel(trffer)liste aufbauen

        //var_dump($Antwort);
        $Erg = simplexml_load_string($Antwort->xmlDocument);

        if(((int)$this->_SucheAttribut($Erg->getorderstatus->orders,'lines'))<=0)
        {
            if($this->_DebugLevel>8)
            {
                echo 'Keine offenen Bestellungen...'.PHP_EOL;
            }

            return 0;
        }

        foreach($Erg->getorderstatus->orders->order AS $Bestellung)
        {
            if($this->_DebugLevel>4)
            {
                echo '  - Lese Bestellung '.$this->_Form->Format('T',$this->_SucheAttribut($Bestellung->belegnr,'value')).PHP_EOL;
            }

            foreach($Bestellung->items->item AS $Bestellposition)
            {
                var_dump($Bestellposition);

                if($this->_DebugLevel>2)
                {
                    echo '   --> Verarbeite Position '.$this->_SucheAttribut($Bestellposition,'reference').PHP_EOL;
                }

                $this->_Bestellungen[$this->_BestellungenAnzahl]['LIENR']=$this->_LIE_NR;
                $this->_Bestellungen[$this->_BestellungenAnzahl]['EXTERNEID']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition,'reference'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['UNTERLIEFERANT']='';


                $this->_Bestellungen[$this->_BestellungenAnzahl]['BESTELLDATUM']=$this->_Form->Format('D',$this->_SucheAttribut($Bestellung->bestelldatum,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['BESTELLZEITPUNKT']=$this->_Form->Format('DU',$this->_SucheAttribut($Bestellung->bestelldatum,'value').' '.$this->_SucheAttribut($Bestellung->bestellzeit,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['FIL_ID']=$this->EntferneLeerzeichen($this->_Form->Format('T',$this->_SucheAttribut($Bestellung->fil_id,'value')));
                if($this->_DB->FeldInhaltFormat('N0',(string)$this->_Bestellungen[$this->_BestellungenAnzahl]['FIL_ID'],false)==0)
                {
                    $this->_Bestellungen[$this->_BestellungenAnzahl]['FIL_ID']='0000';
                }
                $this->_Bestellungen[$this->_BestellungenAnzahl]['WANR']=str_pad(substr(awisFormular::Format('N0',$this->EntferneLeerzeichen($this->_Form->Format('T',$this->_SucheAttribut($Bestellung,'reference'))),false),0,11),11,'0',STR_PAD_LEFT);

                $ReferenzNummern = explode('/',$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition,'reference')));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['AUFTRAGSNREXTERN']=$ReferenzNummern[1];
                $this->_Bestellungen[$this->_BestellungenAnzahl]['AUFTRAGSPOSEXTERN']=$ReferenzNummern[2];
                $this->_Bestellungen[$this->_BestellungenAnzahl]['AUFTRAGSUPOSEXTERN']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->auftragsupos_ext,'value'),false);
                $this->_Bestellungen[$this->_BestellungenAnzahl]['ARTIKELNUMMER']=$this->_Form->Format('TU',$this->_SucheAttribut($Bestellposition->artikelnr,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['ARTIKELBEZEICHNUNG']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->artikelbezeichnung,'value'));

                // Hersteller wird direkt �bernommen!
                $this->_Bestellungen[$this->_BestellungenAnzahl]['HERSTELLER_CODE']='';
                $this->_Bestellungen[$this->_BestellungenAnzahl]['HERSTELLER_NAME']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->hersteller_name,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['HERSTELLER_TECDOCID']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->hersteller_code,'value'));

                $this->_Bestellungen[$this->_BestellungenAnzahl]['VERSANDARTBEZ']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellung->versandartbez,'value'),false);

                $this->_Bestellungen[$this->_BestellungenAnzahl]['KATALOGNUMMER']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->katalognummer,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['EANNUMMER']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->eannummer,'value'));

                $this->_Bestellungen[$this->_BestellungenAnzahl]['BESTELLMENGE']=$this->_Form->Format('N3',$this->_SucheAttribut($Bestellposition->bestellmenge,'value'));

                $this->_Bestellungen[$this->_BestellungenAnzahl]['FAKTORWERTSTELLUNG']=1;

                $this->_Bestellungen[$this->_BestellungenAnzahl]['MENGENEINHEITEN']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->mengeneinheit,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['MENGENEINHEITENBEZ']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->mengeneinheitbez,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['TECHNIKEINHEITEN']=0;
                $this->_Bestellungen[$this->_BestellungenAnzahl]['TECHNIKEINHEITENBEZ']='';

                $this->_Bestellungen[$this->_BestellungenAnzahl]['TAUSCHTEILKENNZEICHEN']=$this->_Form->Format('N0',$this->_SucheAttribut($Bestellposition->tauschteil,'value'));

                // EKPreis = Brutto-VK (verwenden wir jetzt als EKPR, PR�FEN!)
                // VKPreis = EK NACH Rabatten
                $EKPR = $this->_DB->FeldInhaltFormat('N2',$this->_SucheAttribut($Bestellposition->vkpreis,'value'));
                $EKPR *= (1-(abs($this->_DB->FeldInhaltFormat('N3',$this->_SucheAttribut($Bestellposition->rabatt,'value'),false))/100));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['EKPREIS']=$EKPR;
                $this->_Bestellungen[$this->_BestellungenAnzahl]['VKPREIS']=$this->_DB->FeldInhaltFormat('N2',$this->_SucheAttribut($Bestellposition->ekpreis,'value'));

                $this->_Bestellungen[$this->_BestellungenAnzahl]['PREISEINHEIT']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->preiseinheit,'value'));
                if($this->_Bestellungen[$this->_BestellungenAnzahl]['PREISEINHEIT']==0)
                {
                    $this->_Bestellungen[$this->_BestellungenAnzahl]['PREISEINHEIT']=1;
                }
                $this->_Bestellungen[$this->_BestellungenAnzahl]['PREISEINHEITBEZ']='';

                $this->_Bestellungen[$this->_BestellungenAnzahl]['WAEHRUNG']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellung->waehrung,'value'));

                $this->_Bestellungen[$this->_BestellungenAnzahl]['RABATT1']=abs($this->_DB->FeldInhaltFormat('N3',$this->_SucheAttribut($Bestellposition->rabatt,'value'),false));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['RABATT2']=0;

                $this->_Bestellungen[$this->_BestellungenAnzahl]['ALTTEILWERT']=$this->_DB->FeldInhaltFormat('N3',$this->_SucheAttribut($Bestellposition->altteil_wert,'value'),false);
                $this->_Bestellungen[$this->_BestellungenAnzahl]['ALTTEILBELASTUNGSMENGE']=$this->_DB->FeldInhaltFormat('N3',$this->_SucheAttribut($Bestellposition->altteilbelastungs_menge,'value'),false);

                $this->_Bestellungen[$this->_BestellungenAnzahl]['WGR1']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->warengruppe,'value'),false);
                $this->_Bestellungen[$this->_BestellungenAnzahl]['WGR2']='';
                $this->_Bestellungen[$this->_BestellungenAnzahl]['WGR3']='';

                // Erst Standard, dann MwSt genau anzeigen
                $this->_Bestellungen[$this->_BestellungenAnzahl]['MWST']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->mwst,'value'));
                $this->_Bestellungen[$this->_BestellungenAnzahl]['MWSTID']=self::MWST_ID_STANDARD;


                if($this->_Bestellungen[$this->_BestellungenAnzahl]['TAUSCHTEILKENNZEICHEN']==0)
                {
                    if($this->_DB->FeldInhaltFormat('N2',$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->mwst,'value')))==self::MWST_SATZ_STANDARD)
                    {
                        $this->_Bestellungen[$this->_BestellungenAnzahl]['MWST']=self::MWST_SATZ_STANDARD;
                        $this->_Bestellungen[$this->_BestellungenAnzahl]['MWSTID']=self::MWST_ID_STANDARD;
                    }
                    else
                    {
                        //TODO: MwSt?
                    }
                }
                else
                {
                    if($this->_DB->FeldInhaltFormat('N2',$this->_Form->Format('T',$this->_SucheAttribut($Bestellposition->mwst,'value')))==self::MWST_SATZ_STANDARD)
                    {
                        $this->_Bestellungen[$this->_BestellungenAnzahl]['MWST']=self::MWST_SATZ_STANDARD_ALTTEIL;
                        $this->_Bestellungen[$this->_BestellungenAnzahl]['MWSTID']=self::MWST_ID_STANDARD_ALTTEIL;
                    }
                }

                $this->_Bestellungen[$this->_BestellungenAnzahl]['LAND']=$this->_Form->Format('T',$this->_SucheAttribut($Bestellung->land,'value'));
                var_dump($Bestellung);
                var_dump($this->_Bestellungen[$this->_BestellungenAnzahl]);
                $this->_BestellungenAnzahl++;
            }	// Jede Position einer Bestellung
        }	// Alle Bestellungen

        return $this->_BestellungenAnzahl;
    }

    /**
     * Bestaetigt den Download einer Bestellung
     *
     * @param string $ID
     * @author Sacha Kerres
     * @version 200708091925
     */
    protected function BestaetigeBestellung($ID)
    {
        return false;

        //******************************************
        $Befehl = new SimpleXMLElement('<onlinebestellung></onlinebestellung>');
        $Anfrage = $Befehl->addChild('request');
        $Anfrage->addChild('id',730);		// Anfrage Artikel
        $Anfrage->addChild('transaction',$this->_TransactionID);
        $Anfrage->addChild('data','*SET');
        $Info = $Befehl->addChild('info');
        $Info = $Info->addChild('belegarten');

        $Beleg = $Info->addChild('belegart');
        $Beleg->addChild('text',substr($ID,1,8));
        $Beleg->addChild('art',substr($ID,0,1));
        $BefehlsText = $Befehl->asXML();

        $FehlerCode=0;
        $FehlerMeldung='';
        //echo 'Bestaetige:'.$BefehlsText.PHP_EOL;
        $Erg = $this->_SoapClient->Execute(self::SOAP_Benutzer.'|'.self::SOAP_Kennwort,$BefehlsText,$FehlerCode,$FehlerMeldung);
        $Erg = trim($Erg['XML']);
        $Erg = html_entity_decode($Erg);
        $Erg = utf8_decode($Erg);
        $Erg = simplexml_load_string($Erg);

        if(!isset($Erg->response->errcode) OR ((int)$Erg->response->errcode!=0))
        {
            echo 'FEHLER: Rueckmeldung von HESS fuer Auftrag .'.$ID.' ist fehlgeschlagen.'.PHP_EOL;
            $this->_AWISWerkzeug->EMail($this->_EMAIL_Fehler,'ZUKAUF-Importproblem bei HESS', 'Rueckmeldung von HESS fuer Auftrag .'.$ID.' ist fehlgeschlagen.'.((string)$Erg->response->errmsg),2,'shuttle@de.atu.eu');
        }
    }

    protected function BestaetigeLieferschein($ID)
    {

    }

    protected function BestaetigeRechnung($ID)
    {
    }


    private function _SucheAttribut(SimpleXMLElement $XML, $Attribut)
    {
        if(!isset($XML))
        {

            return '';
        }

        foreach($XML->attributes() AS $Name=>$Wert)
        {
            if($Name == $Attribut)
            {
                return $Wert;
            }
        }

        return '';
    }

}

?>