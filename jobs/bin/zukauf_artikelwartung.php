<?php
require_once('/daten/include/awisZukaufArtikelwartung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 999;
    $JobId = 0;
    $StartTag = '';
    $EndTag = '';
    $Testsystem = false;

    $Stammdaten = false;
    $Bestellungen = false;
    $Lieferscheine = false;
    $Artikelzuordnungen = false;


    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
		    case '--stammdaten':
			    $Stammdaten = true;
			    break;
		    case '--lieferscheine':
			    $Lieferscheine = true;
			    break;
		    case '--bestellungen':
			    $Bestellungen = true;
			    break;
		    case '--artikelzuordnung':
			    $Artikelzuordnungen = true;
			    break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
     			    echo '  --stammdaten                       Artikelstammdaten pruefen.'.PHP_EOL;
     			    echo '  --lieferscheine                    Lieferscheine einer Rechnung zuordnen.'.PHP_EOL;
			    echo '  --bestellungen                     Bestellungen einem Lieferschein zuordnen.'.PHP_EOL;
			    echo '  --artikelzuordnung                 ATU-Nummern direkt den Zukaufartikeln zuordnen.'.PHP_EOL;
                            echo PHP_EOL;
                            die();
            }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $ZUK = new awisZukaufArtikelwartung($DB, $AWISBenutzer);
    $ZUK->DebugLevel($DebugLevel);

    if($Stammdaten)
    {
        $ZUK->StammmdatenPruefungen();
    } 
    if($Lieferscheine)
    {
        $ZUK->LieferscheineZuordnen();
    }
    if($Bestellungen)
    {
        $ZUK->BestellungenZuordnen();
    }
    if($Artikelzuordnungen)
    {
        $ZUK->ZuordnungATUArtikel();
    }
}
catch (Exception $ex)
{
    echo $ex->getMessage().PHP_EOL;
    echo 'Zeile:'.$ex->getLine().PHP_EOL;
    echo 'Datei:'.$ex->getFile().PHP_EOL;
    if(isset($DB))
    {
        echo $DB->LetzterSQL().PHP_EOL;
    }
}
?>
