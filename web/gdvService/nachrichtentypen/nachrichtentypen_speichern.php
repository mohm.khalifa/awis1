<?php
/**
 * Speichern Stammdatenpflege GDV-Nachrichtentypen
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607061358
 * @todo
 */

require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
    
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	if (isset($_POST['txtKey']) and $_POST['txtKey'] == '-1') //Neuanlage
	{
        $SQL =' insert into versnachrichtentypen ';
	    $SQL .=' (VNT_VERSNR,VNT_GDN_NUMMER,VNT_USER,VNT_USERDAT)';
	    $SQL .=' values';
		$SQL .='( '.$DB->FeldInhaltFormat('N0',$_POST['txtVVW_VERSICHERUNG']);
		$SQL .=', '.$DB->FeldInhaltFormat('N0',$_POST['txtGDN_NACHRICHTENTYPEN']);
		$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
	    $SQL .=',sysdate ';
	    $SQL .= ' )';
	    	
	    
	    $DB->Ausfuehren($SQL,'',true);
	    
	    $SpeichernOK = true;
	    $Form->Hinweistext($AWISSprachKonserven['GDV']['MSG_NEUANLAGE']);
	}
	if ($FelderNew != '') //Neuanlage
	{
		$SQL ='insert into versnachrichtentypen ';
		$SQL .=' (VNT_VERSNR,VNT_GDN_NUMMER,VNT_USER,VNT_USERDAT)';
		$SQL .=' values';
		$SQL .='( '.$DB->FeldInhaltFormat('N0',$_POST['txtAWIS_KEY1']);
		$SQL .=', '.$DB->FeldInhaltFormat('N0',$_POST['txtGDN_NACHRICHTENTYPEN']);
		$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .=',sysdate ';
		$SQL .= ' )';

		$DB->Ausfuehren($SQL,'',true);
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
