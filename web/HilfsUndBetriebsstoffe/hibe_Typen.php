<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $_GET;
global $_POST;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $AWISBenutzer;
global $NeuAngelegt;
global $HBST;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<?php
/****************************************************************************************************
* 
* 	HilfsUndBetriebsstoffTypen
* 
* 	Pflege der HilfsUndBetriebsstoffTypen
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Okt 2004
* 
****************************************************************************************************/

$Rechtestufe = awisBenutzerRecht($con, 1101);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'HilfsUndBetriebsstoffTypen',$AWISBenutzer->BenutzerName(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

//var_dump($_POST);
echo '<form name=frmHBT method=post action=./hibe_Main.php?cmdAktion=Typen>';

/**************************************************************************
* Aktuelle Zeile l�schen
**************************************************************************/

awis_Debug(1,$_REQUEST);

if(isset($_POST['cmdLoeschen_x']))
{
//	var_dump($_POST);
	if(isset($_POST["cmdLoeschBestaetigung"]))
	{

		$SQL = "DELETE FROM HILFSUNDBETRIEBSSTOFFTypen WHERE HBT_KEY=0" . $_POST["txtHBT_KEY_old"];
		$Erg = awisExecute($con, $SQL );
		awisLogoff($con);
		if($Erg==FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler['message']);
			die();
		}
		
		die('<br>Der Typ wurde erfolgreich gel�scht<br><br><a href=./hibe_Main.php?cmdAktion=Typen&Liste=True>Weiter</a>');
	}
	elseif(empty($_POST["cmdLoeschAbbruch"]))
	{
		print "<form name=frmHBT method=post>";

		print "<input type=hidden name=cmdLoeschen_x>";
		print "<input type=hidden name=txtHBT_KEY_old value=" . $_POST["txtHBT_KEY_old"] . ">";

		print "<span class=HinweisText>Sind Sie wirklich sicher, dass Sie den Hilfs- und Betriebsstofftyp l�schen m�chten?</span><br><br>";
		print "<input type=submit value=\"Ja, l�schen\" name=cmdLoeschBestaetigung>";
		print "<input type=submit value=\"Nein, nicht l�schen\" name=cmdLoeschAbbruch>";
		
		print "</form>";
		awisLogoff($con);
		die();			
	}	
}
/************************************
* Daten speichern
************************************/

if(isset($_POST['cmdSpeichern_x']))
{
	$SQL = '';
	$NeuAngelegt=0;
			
	if($_POST['txtHBT_KEY_old']=='')
	{
		$SQL = "INSERT INTO HilfsUndBetriebsstoffTypen";
		$SQL .= "(HBT_HER_ID, HBT_BEZEICHNUNG, HBT_HBG_KEY, HBT_BILD, HBT_HANDBUCH, HBT_TECHDATEN, HBT_BEMERKUNG, HBT_USER, HBT_USERDAT) ";
		$SQL .= "VALUES(";
		$SQL .= "'" . $_POST['txtHBT_HER_ID'] . "'";
		$SQL .= ",'" . $_POST['txtHBT_BEZEICHNUNG'] . "'";
		$SQL .= "," . $_POST['txtHBT_HBG_KEY'] . "";
		$SQL .= ",'" . $_POST['txtHBT_BILD'] . "'";
		$SQL .= ",'" . $_POST['txtHBT_HANDBUCH'] . "'";
		$SQL .= ",'" . $_POST['txtHBT_TECHDATEN'] . "'";
		$SQL .= ",'" . $_POST['txtHBT_BEMERKUNG'] . "'";
		$SQL .= ",'" . $AWISBenutzer->BenutzerName() . "'";
		$SQL .= ",SYSDATE)";

		$NeuAngelegt=1;
	}
	else
	{
		$Aenderungen = '';
		If($_POST['txtHBT_HER_ID'] != (isset($_POST['txtHBT_HER_ID_old'])?$_POST['txtHBT_HER_ID_old']:'')	)
		{
			$Aenderungen = ", HBT_HER_ID = '" . $_POST['txtHBT_HER_ID'] . "'";
		}
		If($_POST['txtHBT_BEZEICHNUNG'] != $_POST['txtHBT_BEZEICHNUNG_old'])
		{
			$Aenderungen = ", HBT_BEZEICHNUNG = '" . $_POST['txtHBT_BEZEICHNUNG'] . "'";
		}
		If($_POST['txtHBT_BEMERKUNG'].'-' != $_POST['txtHBT_BEMERKUNG_old'].'-')
		{
			$Aenderungen .= ", HBT_BEMERKUNG = '" . $_POST['txtHBT_BEMERKUNG'] . "'";
		}
		If($_POST['txtHBT_HBG_KEY'].'-' != (isset($_POST['txtHBT_HBG_KEY_old'])?$_POST['txtHBT_HBG_KEY_old'].'-':'-'))
		{
			$Aenderungen .= ", HBT_HBG_KEY = '" . $_POST['txtHBT_HBG_KEY'] . "'";
		}
		If($_POST['txtHBT_BILD'].'-' != $_POST['txtHBT_BILD_old'].'-')
		{
			$Aenderungen .= ", HBT_BILD = '" . $_POST['txtHBT_BILD'] . "'";
		}
		If($_POST['txtHBT_HANDBUCH'].'-' != $_POST['txtHBT_HANDBUCH_old'].'-')
		{
			$Aenderungen .= ", HBT_HANDBUCH = '" . $_POST['txtHBT_HANDBUCH'] . "'";
		}
		If($_POST['txtHBT_TECHDATEN'].'-' != $_POST['txtHBT_TECHDATEN_old'].'-')
		{
			$Aenderungen .= ", HBT_TECHDATEN = '" . $_POST['txtHBT_TECHDATEN'] . "'";
		}
	
		If($Aenderungen!='')
		{
			$SQL = "UPDATE HilfsUndBetriebsstoffTypen SET " . substr($Aenderungen,2);
			$SQL .= ", HBT_USER='" . $AWISBenutzer->BenutzerName() . "', HBT_USERDAT=SYSDATE";
			$SQL .= " WHERE HBT_KEY=0" . $_POST['txtHBT_KEY_old'];
		}
	}	// Neu oder �ndern

	if($SQL != '')
	{
		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
		$HBST = '';
		if($NeuAngelegt==1)
		{
			$rsKEY = awisOpenRecordset($con, "select seq_HBT_Key.currval AS KEY from dual");
			$HBST = $rsKEY['KEY'][0];
			if($HBST!='')
			{
				$NeuAngelegt=0;
			}
		}
	}	
		//*********************************************
		// �nderungen der InfoTypen speichern
		//*********************************************
		
	if(isset($_POST['txtHIT_INFORMATION_0']) && $_POST['txtHIT_INFORMATION_0']!='')		// Neuer Datensatz
	{
		$SQL = 'INSERT INTO AWIS.HILFSUNDBETRIEBSSTOFFINFOTYPEN (';
		$SQL .= 'HIT_INFORMATION, HIT_BEMERKUNG, HIT_SORTIERUNG, HIT_HBT_KEYLISTE)';
		$SQL .= 'VALUES (';
		$SQL .= "'" . $_POST['txtHIT_INFORMATION_0'] . "'";
		$SQL .= ",'" . $_POST['txtHIT_BEMERKUNG_0'] . "'";
		$SQL .= ",'" . $_POST['txtHIT_SORTIERUNG_0'] . "'";
		$SQL .= ",'" . $_POST['txtHBT_KEY_old'] . "'";
		$SQL .= ")"	;
		
		$Erg = awisExecute($con, $SQL);
		if($Erg===FALSE)
		{
			awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}

	$Eintraege = explode(';',awis_NameInArray($_POST, 'txtHIT_INFORMATION', true));
	for($Nr=0;$Nr < count($Eintraege);$Nr++)
	{
		if(!strchr($Eintraege[$Nr],"_old"))
		{
			$ID = substr($Eintraege[$Nr],19);
			if($ID != 0)
			{
				$SQL = 'UPDATE AWIS.HILFSUNDBETRIEBSSTOFFINFOTYPEN ';
				$Aenderungen = '';
				
				If($_POST['txtHIT_INFORMATION_' . $ID].'-' != $_POST['txtHIT_INFORMATION_' . $ID . '_old'].'-')
				{
					$Aenderungen .= ", HIT_INFORMATION = '" . $_POST['txtHIT_INFORMATION_' . $ID] . "'";
				}
				If($_POST['txtHIT_BEMERKUNG_' . $ID].'-' != $_POST['txtHIT_BEMERKUNG_' . $ID . '_old'].'-')
				{
					$Aenderungen .= ", HIT_BEMERKUNG = '" . $_POST['txtHIT_BEMERKUNG_' . $ID] . "'";
				}
				If($_POST['txtHIT_SORTIERUNG_' . $ID].'-' != $_POST['txtHIT_SORTIERUNG_' . $ID . '_old'].'-')
				{
					$Aenderungen .= ", HIT_SORTIERUNG = '" . $_POST['txtHIT_SORTIERUNG_' . $ID] . "'";
				}

				if($Aenderungen!='')			
				{
					$SQL .= 'SET ' . substr($Aenderungen,2);
					$SQL .= ' WHERE HIT_KEY=' . $ID;

					$Erg = awisExecute($con, $SQL);
					if($Erg===FALSE)
					{
						awisErrorMailLink("hibe_Main.php", 2, $awisDBFehler);
						awisLogoff($con);
						die();
					}
				}
			}
		}
	}
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

	//**************************************************
	// Kein spezieller angegeben -> suchen
	//**************************************************
		
if((!isset($_GET['HBT_KEY']) and !isset($_POST['txtHBT_KEY_old'])) or isset($_POST['cmdTrefferListe_x']) or $NeuAngelegt==1)
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffTypen, HERSTELLER, HilfsUndBetriebsstoffGruppen";
	$SQL .= " WHERE HBT_HER_ID = HER_ID AND HBT_HBG_KEY = HBG_KEY";
}
else
{
	$SQL = "SELECT * FROM HilfsUndBetriebsstoffTypen, HERSTELLER, HilfsUndBetriebsstoffGruppen";
	$SQL .= " WHERE HBT_HER_ID = HER_ID AND HBT_HBG_KEY = HBG_KEY";
	$SQL .= " AND HBT_KEY=0". (isset($_GET['HBT_KEY'])?$_GET['HBT_KEY']:'') . (isset($_POST['txtHBT_KEY_old'])?$_POST['txtHBT_KEY_old']:'') . $HBST . "";
}

if(isset($_GET['Sort']))
{
	$SQL .= " ORDER BY " . $_GET['Sort'];
}
else
{
	$SQL .= " ORDER BY HBT_BEZEICHNUNG";
}

//$rsHBT ='';

if(!isset($_GET['Hinzufuegen']))
{
	$rsHBT = awisOpenRecordset($con, $SQL);
	$rsHBTZeilen = $awisRSZeilen;
}
else
{
	$rsHBTZeilen=-1;
}


	//**************************************************
	// Daten ausgeben
	//**************************************************
//var_dump($SQL);
if($rsHBTZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<tr><td width=100 id=FeldBez><a href=./hibe_Main.php?cmdAktion=Typen&Sort=HBT_KEY>Nummer</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=Typen&Sort=HBT_BEZEICHNUNG>Bezeichnung</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=Typen&Sort=HER_BEZEICHNUNG>Hersteller</a></td>';
	echo '<td id=FeldBez><a href=./hibe_Main.php?cmdAktion=Typen&Sort=HBT_BEMERKUNG>Bemerkung</a></td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsHBTZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./hibe_Main.php?cmdAktion=Typen&HBT_KEY=" . $rsHBT['HBT_KEY'][$Zeile] . ">" . $rsHBT['HBT_KEY'][$Zeile] . "</a></td>";
		echo "<td id=DatenFeld>" . $rsHBT['HBT_BEZEICHNUNG'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" . $rsHBT['HER_BEZEICHNUNG'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" . $rsHBT['HBT_BEMERKUNG'][$Zeile] . "</td>";
		
		echo '</tr>';
	}
	
	echo '</table>';
}
else					// einzelnen hersteller anzeigen
{
	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Hilfs- und Betriebsstoff Typen</h3></td>';
	echo '<td><font size=1>' . (isset($rsHBT['HBT_USER'][0])?$rsHBT['HBT_USER'][0]:'') . '<br>' . (isset($rsHBT['HBT_USERDAT'][0])?$rsHBT['HBT_USERDAT'][0]:'') . '</font></td>';

    echo "<td width=40 backcolor=#000000 align=right><input name=cmdTrefferListe type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./hibe_Main.php?cmdAktion=Gruppen&Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=*></colgroup>';

	echo '<tr>';
	echo '<td id=FeldBez>ID</td>';
	echo '<td>' . (isset($rsHBT['HBT_KEY'][0])?$rsHBT['HBT_KEY'][0]:'') . '';
	echo '<input type=hidden name=txtHBT_KEY_old value=' . (isset($rsHBT['HBT_KEY'][0])?$rsHBT['HBT_KEY'][0]:'') . '></td>';
	echo '</tr>';

			// Zeile 2
	echo '<tr>';
	echo '<td id=FeldBez>Bezeichnung</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBT_BEZEICHNUNG size=40 value='" . (isset($rsHBT['HBT_BEZEICHNUNG'][0])?$rsHBT['HBT_BEZEICHNUNG'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBT_BEZEICHNUNG_old value='" . (isset($rsHBT['HBT_BEZEICHNUNG'][0])?$rsHBT['HBT_BEZEICHNUNG'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBT['HBT_BEZEICHNUNG'][0])?$rsHBT['HBT_BEZEICHNUNG'][0]:'') . '</td>';
	}
	echo '</tr>';

	
		// Hersteller
	echo '<tr>';
	echo '<td id=FeldBez>Hersteller</td>';
	if(($Rechtestufe&2)==2)
	{
		echo '<td><select name=txtHBT_HER_ID>';

		$rsHER = awisopenRecordset($con,'SELECT HER_ID, HER_BEZEICHNUNG FROM Hersteller WHERE BITAND(HER_TYP,4)=4');
		for($ZeilenHER=0;$ZeilenHER<$awisRSZeilen;$ZeilenHER++)
		{
			echo '<option value=' . $rsHER['HER_ID'][$ZeilenHER];
			if(isset($rsHBT['HBT_HER_ID'][0]) AND $rsHER['HER_ID'][$ZeilenHER] == $rsHBT['HBT_HER_ID'][0])
			{
				echo ' selected ';
			}
			echo '>' . $rsHER['HER_BEZEICHNUNG'][$ZeilenHER] . '</option>';
		}
		
		echo '</select>';
	}
	else	// Nur lesen
	{
		echo '<td>' . (isset($rsHBT['HER_BEZEICHNUNG'][0])?$rsHBT['HER_BEZEICHNUNG'][0]:'') . '</td>';
	}
	echo '</tr>';


		// Gruppe
	echo '<tr>';
	echo '<td id=FeldBez>Gruppe</td>';
	if(($Rechtestufe&2)==2)
	{
		echo '<td><select name=txtHBT_HBG_KEY>';

		$rsHBG = awisopenRecordset($con,'SELECT * FROM HILFSUNDBETRIEBSSTOFFGRUPPEN');
		for($ZeilenHBG=0;$ZeilenHBG<$awisRSZeilen;$ZeilenHBG++)
		{
			echo '<option value=' . $rsHBG['HBG_KEY'][$ZeilenHBG];
			if(isset($rsHBT['HBT_HBG_KEY'][0]) AND $rsHBG['HBG_KEY'][$ZeilenHBG] == $rsHBT['HBT_HBG_KEY'][0])
			{
				echo ' selected ';
			}
			echo '>' . $rsHBG['HBG_BEZEICHNUNG'][$ZeilenHBG] . '</option>';
		}
		
		echo '</select>';
	}
	else	// Nur lesen
	{
		echo '<td>' . (isset($rsHBT['HBG_BEZEICHNUNG'][0])?$rsHBT['HBG_BEZEICHNUNG'][0]:'') . '</td>';
	}
	echo '</tr>';
	
			// Pfad f�r Abbilungen
	echo '<tr>';
	echo '<td id=FeldBez>Bemerkung</td>';
	echo '<td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<input type=hidden name=txtHBT_BEMERKUNG_old value='" . (isset($rsHBT['HBT_BEMERKUNG'][0])?$rsHBT['HBT_BEMERKUNG'][0]:'') . "'>";

		echo '<textarea name=txtHBT_BEMERKUNG cols=60 rows=5>';
		echo (isset($rsHBT['HBT_BEMERKUNG'][0])?$rsHBT['HBT_BEMERKUNG'][0]:'') . "</textarea>";
	}
	else
	{
		echo (isset($rsHBT['HBT_BEMERKUNG'][0])?$rsHBT['HBT_BEMERKUNG'][0]:'');
	}
	echo '</td>';
	echo '</tr>';

			// Pfad f�r die Abbildung
	echo '<tr>';
	echo '<td id=FeldBez>Abbildung</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBT_BILD size=40 value='" . (isset($rsHBT['HBT_BILD'][0])?$rsHBT['HBT_BILD'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBT_BILD_old value='" . (isset($rsHBT['HBT_BILD'][0])?$rsHBT['HBT_BILD'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBT['HBT_BILD'][0])?$rsHBT['HBT_BILD'][0]:'') . '</td>';
	}
	echo '</tr>';

			// Pfad f�r das Handbuch
	echo '<tr>';
	echo '<td id=FeldBez>Handbuch</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBT_HANDBUCH size=40 value='" . (isset($rsHBT['HBT_HANDBUCH'][0])?$rsHBT['HBT_HANDBUCH'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBT_HANDBUCH_old value='" . (isset($rsHBT['HBT_HANDBUCH'][0])?$rsHBT['HBT_HANDBUCH'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBT['HBT_HANDBUCH'][0])?$rsHBT['HBT_HANDBUCH'][0]:'') . '</td>';
	}
	echo '</tr>';
	

			// Pfad f�r die techn. Daten
	echo '<tr>';
	echo '<td id=FeldBez>techn. Datenblatt</td>';
	if(($Rechtestufe&2)==2)
	{
		echo "<td><input name=txtHBT_TECHDATEN size=40 value='" . (isset($rsHBT['HBT_TECHDATEN'][0])?$rsHBT['HBT_TECHDATEN'][0]:'') . "'>";
		echo "<input type=hidden name=txtHBT_TECHDATEN_old value='" . (isset($rsHBT['HBT_TECHDATEN'][0])?$rsHBT['HBT_TECHDATEN'][0]:'') . "'></td>";
	}
	else
	{
		echo '<td>' . (isset($rsHBT['HBT_TECHDATEN'][0])?$rsHBT['HBT_TECHDATEN'][0]:'') . '</td>';
	}
	echo '</tr>';
	echo '</table>'	;

	/***********************************
	* InfoTypen festlegen
	***********************************/
	if(isset($rsHBT['HBT_KEY'][0]) AND $rsHBT['HBT_KEY'][0]!='')		// Nur bei vorhandenen Datens�tzen, nicht beim neuen!
	{
		echo '<table border=1 width=100%>';
		echo '<tr><td id=FeldBez colspan=3>Zusatzinfos</td></tr>';
		echo '<tr><td id=FeldBez>Information</td>';
		echo '<td id=FeldBez>Bemerkung</td>';
		echo '<td id=FeldBez>Sortierung</td></tr>';

		$SQL = "SELECT * FROM HilfsUndBetriebsstoffInfoTypen WHERE HIT_HBT_KeyListe='" . $rsHBT['HBT_KEY'][0] . "'";

		$rsHIT = awisOpenRecordset($con, $SQL);
		$rsHITZeilen = $awisRSZeilen;
		for($HITZeile=0;$HITZeile<$rsHITZeilen;$HITZeile++)
		{
			if(($Rechtestufe&2)==2)
			{
				echo '<tr>';
				echo "<td><input name=txtHIT_INFORMATION_" . $rsHIT['HIT_KEY'][$HITZeile] . " size=40 value='" . $rsHIT['HIT_INFORMATION'][$HITZeile] . "'>";
				echo "<input type=hidden name=txtHIT_INFORMATION_" . $rsHIT['HIT_KEY'][$HITZeile] . "_old value='" . $rsHIT['HIT_INFORMATION'][$HITZeile] . "'></td>";

				echo "<td><input name=txtHIT_BEMERKUNG_" . $rsHIT['HIT_KEY'][$HITZeile] . " size=60 value='" . $rsHIT['HIT_BEMERKUNG'][$HITZeile] . "'>";
				echo "<input type=hidden name=txtHIT_BEMERKUNG_" . $rsHIT['HIT_KEY'][$HITZeile] . "_old value='" . $rsHIT['HIT_BEMERKUNG'][$HITZeile] . "'></td>";

				echo "<td><input name=txtHIT_SORTIERUNG_" . $rsHIT['HIT_KEY'][$HITZeile] . " size=5 value='" . $rsHIT['HIT_SORTIERUNG'][$HITZeile] . "'>";
				echo "<input type=hidden name=txtHIT_SORTIERUNG_" . $rsHIT['HIT_KEY'][$HITZeile] . "_old value='" . $rsHIT['HIT_SORTIERUNG'][$HITZeile] . "'></td>";

				echo '</tr>';
			}
			else
			{
				echo '<tr><td>' . $rsHIT['HIT_INFORMATION'][0] . '</td></tr>';
			}
		}
		if(($Rechtestufe&4)==4)		// Neuer Datensatz
		{
			echo '<tr>';
			echo "<td><input name=txtHIT_INFORMATION_0 size=40 value=''></td>";
			echo "<td><input name=txtHIT_BEMERKUNG_0 size=60 value=''></td>";
			echo "<td><input name=txtHIT_SORTIERUNG_0 size=5 value='100'></td>";
			echo '</tr>';
		}
		echo '</table>';
	}


	if($Rechtestufe>1)
	{
		if(($Rechtestufe&8)==8)
		{
			print " <input type=image accesskey=x alt='L�schen (Alt+X)' src=/bilder/Muelleimer_gross.png name=cmdLoeschen>";
		}
//		print "<input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	}
	
	echo '</form>';
				// Cursor in das erste Feld setzen
	print "<Script Language=JavaScript>";
	print "document.getElementsByName(\"txtHBT_BEZEICHNUNG\")[0].focus();";
	print "</Script>";

}

/*
* Zur�ck - Link
* 
* */

if(isset($_POST['txtZurueck']) &&  $_POST['txtZurueck']!='')
{
	$ZurueckLink = $_POST['txtZurueck'];
}
else
{
	if(isset($_GET['Zurueck']) && $_GET['Zurueck']=='artikel_LIEF')
	{
		$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $_GET['ASTKEY'] . '&cmdAktion=ArtikelInfos';
	}
	elseif(!isset($_GET['Zurueck']))
	{
		$ZurueckLink = '/index.php';
	}
	else
	{
		$ZurueckLink = str_replace('~~9~~', '&', $_GET['Zurueck']);
	}
}

echo '<input type=hidden name=txtZurueck value=' . $ZurueckLink . '>';

if(!isset($_GET['Hinzufuegen']))
{
	if(($Rechtestufe&4)==4)
	{
		echo "<a accesskey=n href=./hibe_Main.php?cmdAktion=Typen&Hinzufuegen=True><img src=/bilder/plus.png border=0 alt='Hinzuf�gen (Alt+N)'></a>";
	}
}

echo '</form>';

?>