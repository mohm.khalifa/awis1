<?php
require_once 'awisBerichte.inc';
require_once('awisDatenbank.inc');

class ber_Kartenzahlungen
	extends awisBerichte
{
	private $pdf_vorlage = '';
	private $Zeilenmerker = '';
	private $Spaltenmerker = '';
	private $letzte_positon = '';
	private $Summe = 0.00;
	
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	
	
	public function init(array $Parameter)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		
		// Daten f�r den Bericht laden
       	$SQL = 'SELECT knz.*, fil.fil_bez ';
		$SQL .= 'FROM KARTENZAHLUNGEN knz
 			INNER JOIN FILIALEN fil
 			ON fil.FIL_ID = knz.KNZ_FILNR';
        
		// Alle Parameter auswerten
		$Bedingung = "";
		
		if (isset($Parameter['KNZ_KEY']) AND $Parameter['KNZ_KEY'] != '' )
		{
			$Param = explode('~',$Parameter['KNZ_KEY']);
			
			$foo = str_replace(';', ',', $Parameter['KNZ_KEY']);
			$Bedingung .= " AND KNZ_KEY in (" . $foo . ")";
		}

		if (isset($Parameter['KNZ_FILNR']) AND $Parameter['KNZ_FILNR'] != '')
		{
			$Bedingung .= " AND KNZ_FILNR = " . $Parameter['KNZ_FILNR'];
		}
		if(isset($Parameter['KNZ_DATUM_VON']) AND $Parameter['KNZ_DATUM_VON']!='')
		{
			$Bedingung .= ' AND TRUNC(KNZ_DATUM) >= ' . $this->_DB->FeldInhaltFormat('D',$Parameter['KNZ_DATUM_VON']);
		}
		if (isset($Parameter['KNZ_DATUM_BIS']) AND $Parameter['KNZ_DATUM_BIS'] != '')
		{
			$Bedingung .= ' AND TRUNC(KNZ_DATUM) <= ' . $this->_DB->FeldInhaltFormat('D',$Parameter['KNZ_DATUM_BIS']);
		}
		
		if(isset($Parameter['KNZ_BETRAG']) AND $Parameter['KNZ_BETRAG']!='')
		{
			$Bedingung .= ' AND KNZ_BETRAG = ' . $this->_DB->FeldInhaltFormat('N2',$Parameter['KNZ_BETRAG']);
		}
		if(isset($Parameter['KNZ_ART']) AND $Parameter['KNZ_ART']!='')
		{
			$Bedingung .= ' AND TRIM(KNZ_ART) = ' . $this->_DB->FeldInhaltFormat('T',$Parameter['KNZ_ART']);
		}
		
		if(isset($Parameter['KNZ_BLZ']) AND $Parameter['KNZ_BLZ']!='')
		{
			$Bedingung .= ' AND KNZ_BLZ = ' . $this->_DB->FeldInhaltFormat('T',$Parameter['KNZ_BLZ']);
		}
		
		if(isset($Parameter['KNZ_KTONR']) AND $Parameter['KNZ_KTONR']!='')
		{
			$Bedingung .= ' AND KNZ_KTONR = ' . $this->_DB->FeldInhaltFormat('T',$Parameter['KNZ_KTONR']);
		}
		
		if ($Bedingung != "")
		{
			$SQL .= " WHERE " . substr($Bedingung, 4);
		}
		
		$SQL .= " ORDER BY KNZ_DATUM ASC";
		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL);
		
		//var_dump($SQL);
		// Alle Textbausteine laden
		$TextKonserven[]=array('KNZ','*');
		$TextKonserven[]=array('Fehler','err_KeineDaten');
		$TextKonserven[]=array('Wort','Seite');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eigentliches PDF erzeugen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		
		$this->AliasNbPages();
		$this->SetAutoPageBreak(true,50);
		
		// Startpositionen setzen
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		$this->NeueSeite();
	
		$this->Datendrucken();
		$this->schreibeSumme();
		$this->FertigStellen($Ausgabeart);        
	}

	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
	    $this->AddPage();
	}
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
		//$this->HauptrahmenAnfangY = $this->_Parameter['StartUeberschrift']+$this->_Parameter['Zeilenhoehe'];
		$this->HauptrahmenAnfangY = $this->_Parameter['StartUeberschrift'];
		$this->HauptrahmenEndeY = $this->GetY();
		$this->HauptrahmenEndeX = self::Seitenbreite() - ($this->_Parameter['LinkerRand']-1.25);
		
		//Linker Rand
		$this->Line($this->_Parameter['LinkerRand'],$this->HauptrahmenEndeY,$this->_Parameter['LinkerRand'],$this->HauptrahmenAnfangY);
		
		//Senkrechten Linien zeichnen
		$this->_Spalte = $this->_Parameter['LinkerRand']+ $this->_Parameter['BreiteFilNr'];
		$this->Line($this->_Spalte,$this->HauptrahmenEndeY,$this->_Spalte,$this->HauptrahmenAnfangY);
		
		$this->_Spalte += $this->_Parameter['BreiteFiliale'];
		$this->Line($this->_Spalte,$this->HauptrahmenEndeY,$this->_Spalte,$this->HauptrahmenAnfangY);
		
		$this->_Spalte += $this->_Parameter['BreiteDatum'];
		$this->Line($this->_Spalte,$this->HauptrahmenEndeY,$this->_Spalte,$this->HauptrahmenAnfangY);
		
		$this->_Spalte += $this->_Parameter['BreiteArt'];
		$this->Line($this->_Spalte,$this->HauptrahmenEndeY,$this->_Spalte,$this->HauptrahmenAnfangY);
		
		$this->_Spalte += $this->_Parameter['BreiteTerminalID'];
		$this->Line($this->_Spalte,$this->HauptrahmenEndeY,$this->_Spalte,$this->HauptrahmenAnfangY);

		$this->_Spalte += $this->_Parameter['BreiteKTO'];
		$this->Line($this->_Spalte,$this->HauptrahmenEndeY,$this->_Spalte,$this->HauptrahmenAnfangY);
		
		//Rechter Rand
		$this->Line($this->HauptrahmenEndeX,$this->HauptrahmenEndeY,$this->HauptrahmenEndeX,$this->HauptrahmenAnfangY);
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{
		if($this->_BerichtsVorlage!='')
		{
			$pdf_vorlage = $this->ImportPage(1);		// Erste Seite aus der Vorlage importieren
			$this->useTemplate($pdf_vorlage);
		}
		
		$this->_Zeile = $this->_Parameter['SeitenzaehlerOben'];
		$this->_Spalte = $this->_Parameter['SeitenzaehlerRechts'];
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse_klein']);
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$this->Cell(10, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['Wort']['Seite']).' '.$this->PageNo().' von {nb}',0 ,0 ,'L');

		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$this->zeichneSpaltenUeberschriften();

	}
	
	public function FertigStellen($Ausgabeart)
	{
	    //*********************************************************************
	    // Berichtsende -> Zur�ckliefern
	    //*********************************************************************
	    switch($Ausgabeart)
	    {
	        case 1:			// PDF im extra Fenster -> Download
	            $this->Output($this->_BerichtsName.'.pdf','D');
	            break;
	        case 2:			// Standard-Stream
	            $this->Output($this->_BerichtsName.'.pdf','I');
	            break;
	        case 3:			// Als String zur�ckliefern
	            return($this->Output($this->_BerichtsName.'.pdf','S'));
	            break;
	        case 4:         // Als Datei speichern
	            $this->Output($this->_Dateiname,'F');
	            break;
	    }
	}
	
	public function Datendrucken()
	{
		$this->SetFont($this->_Parameter['SchriftartListe'], '', $this->_Parameter['Schriftgroesse']);
		$this->HauptrahmenEndeX = self::Seitenbreite() - ($this->_Parameter['LinkerRand']-1.25);
		
		$this->_Summe = 0.00;
		
		while(!$this->_rec->EOF()) //Werte in PDF Drucken
		{
			$Zellenbreite = self::Seitenbreite() - (2*$this->_Parameter['LinkerRand']-1.25);
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			$this->_Spalte = $this->_Parameter['LinkerRand'];
			
			$this->SetXY($this->_Spalte, $this->_Zeile);
			$this->Cell($this->_Parameter['BreiteFilNr'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('KNZ_FILNR'), 0, 0, 'R', 0);
						
			$this->Cell($this->_Parameter['BreiteFiliale'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('FIL_BEZ'), 0, 0, 'L', 0);
			$this->Cell($this->_Parameter['BreiteDatum'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('KNZ_DATUM'), 0, 0, 'L', 0);
			$this->Cell($this->_Parameter['BreiteArt'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('KNZ_ART'), 0, 0, 'L', 0);
			$this->Cell($this->_Parameter['BreiteTerminalID'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('KNZ_TERMINALID'), 0, 0, 'L', 0);
			$this->Cell($this->_Parameter['BreiteKTO'], $this->_Parameter['Zeilenhoehe'], $this->_rec->FeldInhalt('KNZ_KTONR'), 0, 0, 'L', 0);
			$this->Cell($this->_Parameter['BreiteBetrag'], $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N2', $this->_rec->FeldInhalt('KNZ_BETRAG')) . ' �', 0, 0, 'R', 0);
			
			$this->_Summe += str_replace(',','.',$this->_rec->FeldInhalt('KNZ_BETRAG'));
			
			$this->Line($this->_Parameter['LinkerRand'],$this->GetY()+$this->_Parameter['Zeilenhoehe'],$this->HauptrahmenEndeX,$this->GetY()+$this->_Parameter['Zeilenhoehe']);
			
			$this->_rec->DSWeiter();
			
		}
	}
	
	public function zeichneSpaltenUeberschriften()
	{
		$this->SetFillColor(209,209,209);
		$this->SetFont($this->_Parameter['SchriftartListe'], 'B', $this->_Parameter['Schriftgroesse']);
		
		$Zellenbreite = self::Seitenbreite() - (2*$this->_Parameter['LinkerRand']-1.25);
		$this->_Zeile = $this->_Parameter['StartUeberschrift'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
		$this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], '', 1, 0, 'C', 1);
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$this->Cell($this->_Parameter['BreiteFilNr'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['KNZ']['KNZ_PDF_FILNR'], 0, 0, 'R', 0);
		$this->Cell($this->_Parameter['BreiteFiliale'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['KNZ']['KNZ_FILIALE'], 0, 0, 'L', 0);
		$this->Cell($this->_Parameter['BreiteDatum'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['KNZ']['KNZ_DATUM'], 0, 0, 'L', 0);
		$this->Cell($this->_Parameter['BreiteArt'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['KNZ']['KNZ_ART'], 0, 0, 'L', 0);
		$this->Cell($this->_Parameter['BreiteTerminalID'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['KNZ']['KNZ_PDF_TERMINALID'], 0, 0, 'L', 0);
		$this->Cell($this->_Parameter['BreiteKTO'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['KNZ']['KNZ_KTONR'], 0, 0, 'L', 0);
		$this->Cell($this->_Parameter['BreiteBetrag'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['KNZ']['KNZ_BETRAG']. ' �', 0, 0, 'R', 0);
		
		if($this->PageNo()!=1)
		{
			$this->_Zeile = $this->_Zeile + $this->_Parameter['Zeilenhoehe'];
		}
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
	}
	
	public function schreibeSumme()
	{
	
		$this->SetFont($this->_Parameter['SchriftartListe'], 'B', $this->_Parameter['Schriftgroesse']);

		$Zellenbreite = self::Seitenbreite() - (2*$this->_Parameter['LinkerRand']-1.25);
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		$this->SetXY($this->_Spalte, $this->_Zeile);
		
		$BreiteBetragSumme =  self::Seitenbreite() - (2*$this->_Parameter['LinkerRand']-1.25) - $this->_Parameter['BreiteGesSumme'];

		$this->SetFillColor(209,209,209);
		$this->Cell($this->_Parameter['BreiteGesSumme'], $this->_Parameter['Zeilenhoehe'], 'Gesamtsumme:', 1, 0, 'R', 1);
		$this->Cell($BreiteBetragSumme, $this->_Parameter['Zeilenhoehe'], $this->_Form->Format('N2', $this->_Summe) . ' �', 1, 0, 'R', 0);
		
		if($this->PageNo()!=1)
		{
			$this->_Zeile = $this->_Zeile + $this->_Parameter['Zeilenhoehe'];
		}
	}
}

?>