<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

class abteilungsbearbeitung_funktionen {

    /**
     * @var awisFormular
     */
    public $Form;

    /**
     * @var awisDatenbank
     */
    public $DB;

    /**
     * @var awisBenutzer
     */
    public $AWISBenutzer;

    /**
     * Recht f�r die Maske
     *
     * @var int
     */
    public $Recht650;

    /**
     * AWISSprachKonserven
     *
     * @var array
     */
    public $AWISSprachKonserven;

    /**
     * CursorPosition der Seite
     *
     * @var String
     */
    public $AWISCursorPosition;

    /**
     * Datensatzkey
     *
     * @var string
     */
    public $AWIS_KEY1;

    function __construct()
    {
        $this->AWISBenutzer = awisBenutzer::Init();
        $this->DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->DB->Oeffnen();
        $this->Form = new awisFormular();
        $this->Recht650 = $this->AWISBenutzer->HatDasRecht(650);

        $this->Param = @unserialize($this->AWISBenutzer->ParameterLesen('Formular_KAB'));

        $TextKonserven = array();
        $TextKonserven[]=array('KON','*');
        $TextKonserven[]=array('KAB','*');
        $TextKonserven[]=array('TITEL','tit_ABTEILUNGSBEARBEITUNG');
        $TextKonserven[]=array('Wort','lbl_zurueck');
        $TextKonserven[]=array('Wort','lbl_speichern');
        $TextKonserven[]=array('Wort','lbl_hinzufuegen');
        $TextKonserven[]=array('Wort','lbl_loeschen');
        $TextKonserven[]=array('Wort','lbl_trefferliste');
        $TextKonserven[]=array('Wort','ttt_PositionHinzufuegen');
        $TextKonserven[]=array('Wort','ttt_PositionLoeschen');
        $TextKonserven[]=array('Wort','Achtung');
        $TextKonserven[]=array('Wort','lbl_suche');
        $TextKonserven[]=array('Wort','AuswahlSpeichern');
        $TextKonserven[]=array('Fehler','err_keineRechte');

        $this->AWISSprachKonserven = $this->Form->LadeTexte($TextKonserven,$this->AWISBenutzer->BenutzerSprache());
    }

    function __destruct()
    {
        $this->Form->SetzeCursor($this->AWISCursorPosition);
        $this->AWISBenutzer->ParameterSchreiben('Formular_KAB', serialize($this->Param));
    }

    /**
     * Pr�ft die Rechte
     *
     * @param int $Bit
     */
    public function RechteMeldung($Bit = 0)
    {
        if (($this->Recht650 & $Bit) != $Bit) {
            $this->DB->EreignisSchreiben(1000, awisDatenbank::EREIGNIS_FEHLER, array($this->AWISBenutzer->BenutzerName(), 'KAB'));
            $this->Form->Hinweistext($this->AWISSprachKonserven['Fehler']['err_keineRechte']);
            $this->Form->SchaltflaechenStart();
            $this->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', '', 'Z');
            $this->Form->SchaltflaechenEnde();
            die();
        }
    }

    public function BedingungErstellen(){
        $return = '';

        if ($this->AWIS_KEY1 <> 0) {
            $return .= " and KAB_KEY =" . $this->DB->WertSetzen('KAB', 'Z', $this->AWIS_KEY1);
        }

        if(isset($_POST['sucKON_NAME']) or isset($_POST['sucKAB_ABTEILUNG']) or ($this->Param['SPEICHERN'] == 'on')) {
            if ($this->Param['KAB_ABTEILUNG'] <> "" and !(isset($_POST['cmdSpeichern_x']))) {
                $return .= " and upper(KAB_ABTEILUNG) " . $this->DB->LikeOderIst("%" . $this->Param['KAB_ABTEILUNG'] . "%", awisDatenbank::AWIS_LIKE_UPPER, 'KAB');
            }

            if ($this->Param['KON_NAME'] <> "" and !(isset($_POST['cmdSpeichern_x']))) {
                $return .= " and ( upper(KON_NAME1) " . $this->DB->LikeOderIst("%" . $this->Param['KON_NAME'] . "%", awisDatenbank::AWIS_LIKE_UPPER, 'KAB');
                $return .= " or upper(KON_NAME2) " . $this->DB->LikeOderIst("%" . $this->Param['KON_NAME'] . "%", awisDatenbank::AWIS_LIKE_UPPER, 'KAB') . " ) ";
            }
        }

        if ($return <> "") {
            $return = substr_replace($return, " where ", 0, 4);
        }
        return $return;
    }

    public function LadePersonenHinzufuegen(){
        //Versteckt die Mehrfachselectfelder bei der Einzel-DS-Ansicht
        //bzw. zeigt sie an beim Dr�cken des Hinzuf�gen-Buttons
        $Script = "";
        $Script .= "<script>".PHP_EOL;
        $Script .= "$(document).ready(function(){".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= " $('#KAB_ABTEILUNGSMITGLIED_DIV').hide();".PHP_EOL;
        $Script .= " $('#KAB_ABTEILUNGSBETREUERIT_DIV').hide();".PHP_EOL;
        $Script .= " $('#KAB_ABTEILUNGSMITGLIED_ADD').click(function(){".PHP_EOL;
        $Script .= "  OeffneMitglied();".PHP_EOL;
        $Script .= " })".PHP_EOL;
        $Script .= " $('#KAB_ABTEILUNGSBETREUERIT_ADD').click(function(){".PHP_EOL;
        $Script .= "  OeffneBetreuerIT();".PHP_EOL;
        $Script .= " })".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= " function OeffneMitglied(){".PHP_EOL;
        $Script .= "  $('#KAB_ABTEILUNGSMITGLIED_DIV').show();".PHP_EOL;
        $Script .= " }".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= " function OeffneBetreuerIT(){".PHP_EOL;
        $Script .= "  $('#KAB_ABTEILUNGSBETREUERIT_DIV').show();".PHP_EOL;
        $Script .= " }".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "".PHP_EOL;
        $Script .= "});".PHP_EOL;
        $Script .= "</script>";
        echo $Script;
    }





}