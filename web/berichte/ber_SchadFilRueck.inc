<?php
require_once 'awisBerichte.inc';
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

global $AWISBenutzer;

class ber_SchadFilRueck
    extends awisBerichte
{
    private $pdf_vorlage = '';
    private $Zeilenmerker = '';
    private $Spaltenmerker = '';
    private $letzte_positon = '';
    private $Summe = 0.00;
    private $FIL_ID = '';
    private $GEBIET = '';
    private $DATUM_VOM = '';
    private $DATUM_BIS = '';
    private $SPEICHERN = '';

    /**
     * Prozedur zum Initialisieren des Berichts
     *
     * @see awisBerichte::init()
     */
    public function init(array $Parameter)
    {
        $TextKonserven = array();
        $TextKonserven[] = array('BES', '%');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Liste', 'lst_OffenMass');
        $TextKonserven[] = array('Ausdruck', 'txt_HinweisFormularIntern');
        $TextKonserven[] = array('Wort', 'Seite');
        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);

        $this->_DB = awisDatenbank::NeueVerbindung('SCHAD');
        $this->_DB->oeffnen();


        $this->_GEBIET = 0;
        $this->_FIL_ID = $Parameter['FIL_ID'];
        $this->_DATUM_VOM = $Parameter['DATUM_VOM'];
        $this->_DATUM_BIS = $Parameter['DATUM_BIS'];
        $this->_SPEICHERN = $Parameter['SPEICHERN'] == 'on'?'on':'';
        $Parameter['SPEICHERN'] = $this->_SPEICHERN;

        $SQL = 'select VERURS_FILIALE,EINGABEAM,BEARBEITUNGSNR,WERT,KFZ_KENNZ,FILNR,AUFTRAGSART,GRUND,SCHLAGWORT,AUSFALL,KENNUNG_BEZEICHNUNG,SUM(coalesce(BETRAG,0)) as FALLKOSTEN ';
        $SQL .= ' from( ';
        $SQL .= ' SELECT S.*,AA.*,SB.*,AB.AUFTRAGSART_ATU as AUFTRAGSART,SG.GRUND as GRUND,BS.SCHLAGWORT,AU.WERT as AUSFALL,KENNUNG_BEZEICHNUNG,Z.BETRAG, K.AKTIV_FUER_AUSWERTUNG';
        $SQL .= ' FROM SCHAEDEN_NEW S';
        $SQL .= ' LEFT JOIN ANTRAGART AA ON ANTRAGART = AA.ID ';
        $SQL .= ' LEFT JOIN AUFTRAGSARTEN AB ON AUFTRAGSART_ATU_NEU = AB.ART_ID ';
        $SQL .= ' LEFT JOIN SACHBEARBEITER SB ON BEARBEITER = SB.ID';
        $SQL .= ' LEFT JOIN SCHADENSGRUND SG ON SCHADENSGRUND = SG.ID';
        $SQL .= ' LEFT JOIN BESCHAEDIGT BS ON S.BID = BS.BID';
        $SQL .= ' LEFT JOIN AUSFALLURSACHE AU ON AUSFALLURSACHE = AU.ID';
        $SQL .= ' LEFT JOIN KENNUNGEN K ON S.KENNUNG = K.KENNUNG';
        $SQL .= ' LEFT JOIN ZAHLUNGEN Z ON S.BEARBEITUNGSNR = Z.BEARBEITUNGSNR)';
        
        $Bedingung = '';
        $Bedingung .= _BedingungErstellenPDF($Parameter);       // mit dem Rest

        $Bedingung .= " AND KENNUNG <> 'Z-STORNO' AND KENNUNG <> 'FEHLT' ";
        $Bedingung .= " AND AKTIV_FUER_AUSWERTUNG = 1";

        if ($Bedingung != '') {
            $SQL .= ' WHERE ' . substr($Bedingung, 4);
        }
        $SQL .= ' GROUP BY VERURS_FILIALE,EINGABEAM,BEARBEITUNGSNR,WERT,KFZ_KENNZ,FILNR,AUFTRAGSART,GRUND,SCHLAGWORT,AUSFALL,KENNUNG_BEZEICHNUNG';
        $SQL .= ' order by KENNUNG_BEZEICHNUNG,BEARBEITUNGSNR';

        $this->_MaxDS = $this->_DB->ErmittleZeilenAnzahl($SQL);
        $this->_rsSchad = $this->_DB->RecordSetOeffnen($SQL);
        $this->_AWISBenutzer->ParameterSchreiben('Formular_BES_Auswertung', serialize($Parameter));
    }

    /**
     * Eigentliches PDF erzeugen
     *
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {

        // PDF Attribute setzen
        $this->SetTitle($this->_BerichtsName);
        $this->SetSubject('');
        //$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());

        $this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);

        $this->AliasNbPages();
        $this->SetAutoPageBreak(true, 35);

        // Startpositionen setzen

        $this->NeueSeite();

        $this->SetFillColor(210, 210, 210);

        $Summe_Anzahl = 1;
        $ZwischenSumme_Fallkosten = 0;
        $KENNUNG_BEZEICHNUNG = '';
        $Summe_Gesamt = 0;
        $Anzahl_Gesamt = 0;
        $this->_Zeile += $this->_Parameter['Tabellenfeldhoehe'];

        while (!$this->_rsSchad->EOF()) {
            if ($KENNUNG_BEZEICHNUNG != '' and $KENNUNG_BEZEICHNUNG <> $this->_rsSchad->Feldinhalt('KENNUNG_BEZEICHNUNG')) {
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
                $this->_Spalte = $this->_Parameter['Seite1-Oben'];
                $this->_Zeile += $this->_Parameter['Tabellenfeldhoehe'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell($this->_Parameter['BreiteZwischenPos1'], $this->_Parameter['Tabellenfeldhoehe'], '', 'LR', 0, 'C', 0);
                $this->Cell($this->_Parameter['BreiteZwischenPos2'], $this->_Parameter['Tabellenfeldhoehe'], '', 'BLR', 0, 'C', 0);
                $this->Cell($this->_Parameter['BreiteBearbFil'], $this->_Parameter['Tabellenfeldhoehe'], $Summe_Anzahl, 1, 0, 'R', 0);
                $this->Cell($this->_Parameter['BreiteZwischenPos3'], $this->_Parameter['Tabellenfeldhoehe'], '', 1, 0, 'C', 0);
                $this->Cell($this->_Parameter['BreiteFallkosten'], $this->_Parameter['Tabellenfeldhoehe'],
                    $this->_Form->Format('N2', str_replace('.', ',', $ZwischenSumme_Fallkosten)) . ' �', 1, 0, 'R', 1);
                $this->Cell($this->_Parameter['BreiteZwischenPos4'], $this->_Parameter['Tabellenfeldhoehe'], '', 1, 0, 'C', 0);
                $Summe_Gesamt += $ZwischenSumme_Fallkosten;
                $Anzahl_Gesamt += $Summe_Anzahl;
                $ZwischenSumme_Fallkosten = str_replace(',', '.', $this->_rsSchad->Feldinhalt('FALLKOSTEN'));
            }
            $Kennung = true;
            $VUFIL = true;
            $this->_Zeile += $this->_Parameter['Tabellenfeldhoehe'];
            $this->_Spalte = $this->_Parameter['Seite1-Oben'];
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse2']);
            $this->SetXY($this->_Spalte, $this->_Zeile);
            if ($this->_rsSchad->DSNummer() == 0) {
                $this->Cell($this->_Parameter['BreiteVUFIL'], $this->_Parameter['Tabellenfeldhoehe'], $this->_rsSchad->Feldinhalt('VERURS_FILIALE'), 'L', 0, 'C', 0);
            } else {
                $this->Cell($this->_Parameter['BreiteVUFIL'], $this->_Parameter['Tabellenfeldhoehe'], '', 'L', 0, 'C', 0);
            }
            if ($KENNUNG_BEZEICHNUNG == '' or $KENNUNG_BEZEICHNUNG <> $this->_rsSchad->Feldinhalt('KENNUNG_BEZEICHNUNG')) {
                if ($KENNUNG_BEZEICHNUNG == '') {
                    $ZwischenSumme_Fallkosten = str_replace(',', '.', $this->_rsSchad->Feldinhalt('FALLKOSTEN'));
                }
                $KENNUNG_BEZEICHNUNG = str_replace(',', '.', $this->_rsSchad->Feldinhalt('KENNUNG_BEZEICHNUNG'));
                $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteVUFIL'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell($this->_Parameter['BreiteKennung'], $this->_Parameter['Tabellenfeldhoehe'], substr($this->_rsSchad->Feldinhalt('KENNUNG_BEZEICHNUNG'), 0, 35), 'LT', 0,
                    'C', 0);
                $Summe_Anzahl = 1;
            } else {
                $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteVUFIL'];
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell($this->_Parameter['BreiteKennung'], $this->_Parameter['Tabellenfeldhoehe'], '', 'L', 0, 'C', 0);
                $Summe_Anzahl++;
                $ZwischenSumme_Fallkosten += str_replace(',', '.', $this->_rsSchad->Feldinhalt('FALLKOSTEN'));
            }
            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteKennung'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteBEARBNR'], $this->_Parameter['Tabellenfeldhoehe'], $this->_rsSchad->Feldinhalt('BEARBEITUNGSNR'), 1, 0, 'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteBEARBNR'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteAntragsart'], $this->_Parameter['Tabellenfeldhoehe'], $this->_rsSchad->Feldinhalt('WERT'), 1, 0, 'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteAntragsart'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteBearbFil'], $this->_Parameter['Tabellenfeldhoehe'], $this->_rsSchad->Feldinhalt('FILNR'), 1, 0, 'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteBearbFil'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteFallkosten'], $this->_Parameter['Tabellenfeldhoehe'], $this->_Form->Format('N2', $this->_rsSchad->Feldinhalt('FALLKOSTEN')) . ' �',
                1, 0, 'R', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteFallkosten'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteUrsprAuftrag'], $this->_Parameter['Tabellenfeldhoehe'], $this->_rsSchad->Feldinhalt('AUFTRAGSART'), 1, 0, 'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteUrsprAuftrag'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteEingabedatum'], $this->_Parameter['Tabellenfeldhoehe'], $this->_Form->Format('D', $this->_rsSchad->Feldinhalt('EINGABEAM')), 1, 0,
                'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteEingabedatum'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteWorangearb'], $this->_Parameter['Tabellenfeldhoehe'], substr($this->_rsSchad->Feldinhalt('GRUND'), 0, 18), 1, 0, 'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteWorangearb'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteWasBesch'], $this->_Parameter['Tabellenfeldhoehe'], substr($this->_rsSchad->Feldinhalt('SCHLAGWORT'), 0, 19), 1, 0, 'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteWasBesch'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteAusfallUrsach'], $this->_Parameter['Tabellenfeldhoehe'], substr($this->_rsSchad->Feldinhalt('AUSFALL'), 0, 19), 1, 0, 'C', 0);

            $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteAusfallUrsach'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteKFZ'], $this->_Parameter['Tabellenfeldhoehe'], $this->_rsSchad->Feldinhalt('KFZ_KENNZ'), 1, 0, 'C', 0);
            $this->_rsSchad->DSWeiter();
        }
        $Summe_Gesamt += $ZwischenSumme_Fallkosten;
        $Anzahl_Gesamt += $Summe_Anzahl;
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $this->SetX($this->_Spalte);
        $this->Cell($this->_Parameter['BreiteVUFIL'], $this->_Parameter['Tabellenfeldhoehe'], '', 'B', 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteKennung'], $this->_Parameter['Tabellenfeldhoehe'], '', 'B', 0, 'C', 0);
        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $this->_Zeile += $this->_Parameter['Tabellenfeldhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteZwischenPos1'], $this->_Parameter['Tabellenfeldhoehe'], '', 'LRB', 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteZwischenPos2'], $this->_Parameter['Tabellenfeldhoehe'], '', 'BLR', 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteBearbFil'], $this->_Parameter['Tabellenfeldhoehe'], $Summe_Anzahl, 1, 0, 'R', 0);
        $this->Cell($this->_Parameter['BreiteZwischenPos3'], $this->_Parameter['Tabellenfeldhoehe'], '', 1, 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteFallkosten'], $this->_Parameter['Tabellenfeldhoehe'],
            $this->_Form->Format('N2', str_replace('.', ',', $ZwischenSumme_Fallkosten)) . ' �', 1, 0, 'R', 1);
        $this->Cell($this->_Parameter['BreiteZwischenPos4'], $this->_Parameter['Tabellenfeldhoehe'], '', 1, 0, 'C', 0);

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse'] + 2);
        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $this->SetX($this->_Spalte);
        $this->Cell($this->_Parameter['BreiteVUFIL'], $this->_Parameter['Tabellenfeldhoehe'], '', 'B', 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteKennung'], $this->_Parameter['Tabellenfeldhoehe'], '', 'B', 0, 'C', 0);
        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $this->_Zeile += $this->_Parameter['Tabellenfeldhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteZwischenPos1'], $this->_Parameter['Tabellenfeldhoehe'], '', 'LRB', 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteZwischenPos2'], $this->_Parameter['Tabellenfeldhoehe'], '', 'BLR', 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteBearbFil'], $this->_Parameter['Tabellenfeldhoehe'], $Anzahl_Gesamt, 1, 0, 'R', 0);
        $this->Cell($this->_Parameter['BreiteZwischenPos3'], $this->_Parameter['Tabellenfeldhoehe'], '', 1, 0, 'C', 0);
        $this->Cell($this->_Parameter['BreiteFallkosten'], $this->_Parameter['Tabellenfeldhoehe'], $this->_Form->Format('N2', str_replace('.', ',', $Summe_Gesamt)) . ' �', 1, 0,
            'R', 1);
        $this->Cell($this->_Parameter['BreiteZwischenPos4'], $this->_Parameter['Tabellenfeldhoehe'], '', 1, 0, 'C', 0);

        $this->FertigStellen($Ausgabeart);
    }


    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage('L');
    }

    /**
     * Automatische Fu�zeile
     *
     * @see TCPDF::Footer()
     */
    public function Footer()
    {

        $this->_Zeile = $this->_Parameter['FooterStart'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];

        //Legende
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse2']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(120, 4, $this->_AWISSprachkonserven['BES']['PDF_FOOTER_HINWEIS'], 0, 0, 'L', 0);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] / 2;
        $this->SetY($this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse2']);
        $this->Cell(120, 4, $this->_AWISSprachkonserven['BES']['PDF_FOOTER_LEGENDE'], 0, 0, 'L', 0);

        // Fu�zeile
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse3']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] / 2;
        $this->SetY($this->_Zeile);
        $this->Cell(10, 8, 'Seite: ' . $this->PageNo(), 0, 0, 'L', 0);
        $this->_Spalte = $this->_Parameter['LinkerRand'] + $this->_Parameter['FooterEigentum'];
        $this->SetX($this->_Spalte);
        $this->Cell(40, 8, $this->_AWISSprachkonserven['BES']['PDF_FOOTER_EIGENTUM'], 0, 0, 'C', 0);
        $this->_Spalte = $this->_Parameter['LinkerRand'] + $this->_Parameter['FooterErstellt'];
        $this->SetX($this->_Spalte);
        $this->Cell(30, 8, 'Erstellt: ' . $this->_AWISBenutzer->BenutzerName() . '  ' . date('d.m.Y'), 0, 0, 'R', 0);
    }

    /**
     * Automatische Kopfzeile
     *
     * @see TCPDF::Header()
     */
    public function Header()
    {
        if ($this->_BerichtsVorlage != '') {
            $pdf_vorlage = $this->ImportPage(1);        // Erste Seite aus der Vorlage importieren
            $this->useTemplate($pdf_vorlage);
        }

        $this->_Zeile = $this->GETY();
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Image('/daten/web/bilder/atu_brand.png', $this->_Parameter['PosLogo'], $this->_Zeile, 125 / 3, 60 / 3);

        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $Zeile = $this->_Parameter['Seite1-Ueberschrift'];

        $this->SetFont('Arial', '', 8);

        // �berschrift
        $this->SetFont('Arial', 'B', 14);
        $this->SetXY($this->_Spalte, $Zeile);
        $this->Cell(180, 6, 'Beschwerdereport Filiale ' . $this->_rsSchad->Feldinhalt('FILIALBEZ'), 0, 0, 'L', 0);

        $this->SetFont('Arial', '', 10);
        $this->SetXY($this->_Spalte, $Zeile + 6);

        $Erstellung = '';
        if ($this->_DATUM_VOM != '' or $this->_DATUM_BIS != '') {
            $Erstellung .= 'Zeitraum: ';
        }
        if ($this->_DATUM_VOM != '') {
            $Erstellung .= ' von ' . $this->_DATUM_VOM;
        }
        if ($this->_DATUM_BIS != '') {
            $Erstellung .= ' bis ' . $this->_DATUM_BIS;
        }
        $Erstellung .= ' Erstellt am: ' . date('d.m.Y');

        $this->Cell(50, 6, $Erstellung, 0, 0, 'L', 0);

        $this->_Zeile = $this->GETY() + $this->_Parameter['Zeilenhoehe'] * 4;
        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->SetFillColor(210, 210, 210);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse2']);

        $this->Cell($this->_Parameter['BreiteVUFIL'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_VUFIL'], 1, 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteVUFIL'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteKennung'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_KENNUNG'], 1, 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteKennung'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteBEARBNR'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_BEARBNR'], 'TL', 0, 'C', 1);
        $this->SetXY($this->_Spalte, $this->_Zeile + 4);
        $this->Cell($this->_Parameter['BreiteBEARBNR'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_AWIS'], 'BL', 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteBEARBNR'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteAntragsart'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_AART'], 1, 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteAntragsart'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteBearbFil'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_BEARBFIL'], 1, 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteBearbFil'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteFallkosten'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_FALLKOSTEN'], 1, 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteFallkosten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteUrsprAuftrag'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_URSPR'], 'TL', 0, 'C', 1);
        $this->SetXY($this->_Spalte, $this->_Zeile + 4);
        $this->Cell($this->_Parameter['BreiteUrsprAuftrag'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_AUFTRAG'], 'BL', 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteUrsprAuftrag'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteEingabedatum'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_EINGABEDATUM'], 1, 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteEingabedatum'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteWorangearb'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_WORAN'], 'TL', 0, 'C', 1);
        $this->SetXY($this->_Spalte, $this->_Zeile + 4);
        $this->Cell($this->_Parameter['BreiteWorangearb'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_GEARBEITET'], 'BL', 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteWorangearb'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteWasBesch'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_WAS'], 'TL', 0, 'C', 1);
        $this->SetXY($this->_Spalte, $this->_Zeile + 4);
        $this->Cell($this->_Parameter['BreiteWasBesch'], $this->_Parameter['Tabellenhoehe'] / 2, $this->_AWISSprachkonserven['BES']['BES_BESCHAEDIGT'], 'BL', 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteWasBesch'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteAusfallUrsach'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_AUSFALLUR'], 1, 0, 'C', 1);

        $this->_Spalte = $this->_Spalte + $this->_Parameter['BreiteAusfallUrsach'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteKFZ'], $this->_Parameter['Tabellenhoehe'], $this->_AWISSprachkonserven['BES']['BES_KFZKZ'], 1, 0, 'C', 1);

        if ($this->PageNo() == 1) {
            $this->_Zeile = $this->GETY();
        } else {
            $this->_Zeile = $this->GETY() + $this->_Parameter['Zeilenhoehe'] + 3;
        }
        $this->_Spalte = $this->_Parameter['Seite1-Oben'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
    }

    public function FertigStellen($Ausgabeart)
    {
        //*********************************************************************
        // Berichtsende -> Zur�ckliefern
        //*********************************************************************
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zur�ckliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:         // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }
}

function _BedingungErstellenPDF($Param)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;
    global $Form;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Bedingung = '';

    $Recht40015 = $AWISBenutzer->HatDasRecht(40015);

    if (isset($Param['FIL_ID']) AND $Param['FIL_ID'] != '' AND $Param['FIL_ID'] != 0) {
        $Bedingung .= ' AND VERURS_FILIALE ' . $DB->LikeOderIst($Param['FIL_ID']) . ' ';
        //$Bedingung .= " AND ANZEIGEFIL = 1";
    }

    if (isset($Param['DATUM_VOM']) AND $Param['DATUM_VOM'] != '') {
        $Bedingung .= " AND EINGABEAM >= TO_DATE('" . $Param['DATUM_VOM'] . "','DD.MM.RRRR')";
    }
    if (isset($Param['DATUM_BIS']) AND $Param['DATUM_BIS'] != '') {
        $Bedingung .= " AND EINGABEAM <= TO_DATE('" . $Param['DATUM_BIS'] . "','DD.MM.RRRR')";
    }

    return $Bedingung;
}

?>