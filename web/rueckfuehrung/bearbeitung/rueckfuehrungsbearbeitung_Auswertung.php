<?php

global $AWISCursorPosition;
global $AWIS_KEY1;

try
{
	$AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
    $TextKonserven = array();
    $TextKonserven[]=array('RFF','*');    
    $TextKonserven[]=array('RFK','*');    
    $TextKonserven[]=array('RFS','lst_RFS_STATUS');
    $TextKonserven[]=array('Wort','Abschlussdatum');
    $TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');

    $Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht3940 = $AWISBenutzer->HatDasRecht(3940);

	if($Recht3940==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$Form->DebugAusgabe(1,$_POST);

	$Param = unserialize($AWISBenutzer->ParameterLesen("RFKAuswertungSuche"));
	$ParamSuche = unserialize($AWISBenutzer->ParameterLesen("RFKSuche"));
	
	$Form->DebugAusgabe(1,$Param);
	
	if(!isset($_POST['cmdSuche_x']) and !isset($_POST['sucRFF_AST_ATUNR'])
		and !isset($_GET['Liste']) and !isset($_POST['Block']) 
		and !isset($_GET['Sort']) and !isset($_GET['Block']))
	{	
		$Form->SchreibeHTMLCode('<form name="frmRueckfuehrungAuswertungSuche" action="./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung" method="POST"  enctype="multipart/form-data">');
				
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_AST_ATUNR'].':',200);
		$Form->Erstelle_TextFeld('*RFF_AST_ATUNR',($ParamSuche['SPEICHERN']=='on'?$ParamSuche['RFF_AST_ATUNR']:''),20,200,true);
		$AWISCursorPosition='sucRFF_AST_ATUNR';
		$Form->ZeileEnde();		
		$Form->Formular_Ende();
	
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');		
		$Form->SchaltflaechenEnde();
		
		$Form->SchreibeHTMLCode('</form>');
	}		
	else 
	{							
		if(isset($_POST['cmdSuche_x']) or isset($_POST['sucRFF_AST_ATUNR']))
		{
			$Param['KEY']=0;			// Key
						
			$Param['RFF_AST_ATUNR']=$_POST['sucRFF_AST_ATUNR'];				
			
			$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
		
			$Param['ORDERBY']='';
			$Param['BLOCK']='';
			$Param['KEY']=0;
		
			$AWISBenutzer->ParameterSchreiben("RFKAuswertungSuche",serialize($Param));			
		}
		
				
		//********************************************************
		// Bedingung erstellen
		//********************************************************	
		$Bedingung = _BedingungErstellen($Param);
		
		//*****************************************************************
		// Sortierung aufbauen
		//*****************************************************************
		if(!isset($_GET['Sort']))
		{
			if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
			{
				$ORDERBY = $Param['ORDERBY'];
			}
			else
			{
				$ORDERBY = ' RFK_RUECKFUEHRUNGSNR, RFF_AST_ATUNR';
			}		
		}
		else
		{
			$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
		}
		
		
		//********************************************************
		// Daten suchen
		//********************************************************
		
		$SQL = 'SELECT AA.*, row_number() over (order by '.$ORDERBY.') AS ZeilenNr FROM';
		$SQL .= ' (SELECT RFK.RFK_KEY, RFK.RFK_FIL_ID, RFK.RFK_RUECKFUEHRUNGSNR, RFK.RFK_DATUMABSCHLUSSZEN';
		$SQL .= ' , RFF.RFF_AST_ATUNR, SUM(RFF.RFF_MENGEFILIALE) AS RFF_MENGEFILIALE, SUM(RFF.RFF_MENGEZENTRALE) AS RFF_MENGEZENTRALE';
		$SQL .= ' FROM RueckFuehrungsFilialMengenKopf RFK ';
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSSTATUS RFS ON (RFK.RFK_RFS_KEY=RFS.RFS_KEY AND RFS.RFS_STATUS=33)';		 
		$SQL .= ' INNER JOIN RUECKFUEHRUNGSFILIALMENGENPOS RFF ON (RFK.RFK_KEY=RFF.RFF_RFK_KEY)';
		$SQL .= ' GROUP BY RFK.RFK_KEY,RFK.RFK_FIL_ID,RFK.RFK_RUECKFUEHRUNGSNR,RFK.RFK_DATUMABSCHLUSSZEN,RFF.RFF_AST_ATUNR) AA';
		$SQL .= ' WHERE RFF_MENGEZENTRALE <> RFF_MENGEFILIALE';
		
		if($Bedingung!='')
		{			
			$SQL .= ' AND ' . substr($Bedingung,4);
		}	
		
		$SQL .= ' ORDER BY '.$ORDERBY;
		$Form->DebugAusgabe(1,$SQL);				
			
	
		//************************************************
		// Aktuellen Datenblock festlegen
		//************************************************
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		}
		elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
		{
			$Block = intval($Param['BLOCK']);
		}
		
		//************************************************
		// Zeilen begrenzen
		//************************************************
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
			
		//*****************************************************************
		// Nicht einschränken, wenn nur 1 DS angezeigt werden soll
		//*****************************************************************
		if($AWIS_KEY1<=0)
		{
			$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
		}
		
		$rsRFK = $DB->RecordSetOeffnen($SQL);
		
		//*****************************************************************
		// Aktuelle Parameter sichern
		//*****************************************************************	
		$Param['ORDERBY']=$ORDERBY;
		$Param['KEY']=$AWIS_KEY1;
		$Param['BLOCK']=$Block;
		$AWISBenutzer->ParameterSchreiben("RFKAuswertungSuche",serialize($Param));
				
		$Form->SchreibeHTMLCode('<form name=frmRueckfuehrungAuswertung action=./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
		
		//********************************************************
		// Daten anzeigen
		//********************************************************	
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFF']['RFF_AST_ATUNR'].':',200);
		$Form->Erstelle_TextFeld('*RFF_AST_ATUNR',(isset($Param['RFF_AST_ATUNR'])?$Param['RFF_AST_ATUNR']:''),20,200,false);		
		$Form->ZeileEnde();		
		$Form->Trennzeile();
		$Form->Formular_Ende();		
		
		if($rsRFK->EOF())
		{
			echo '<span class=HinweisText>Es wurden keine Datensätze gefunden.</span>';
		}		
		elseif(($rsRFK->AnzahlDatensaetze()>0) or (isset($_GET['Liste'])))						// Liste anzeigen
		{
			$Form->Formular_Start();
		
			$Form->ZeileStart();
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFK_RUECKFUEHRUNGSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFK_RUECKFUEHRUNGSNR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFK']['RFK_RUECKFUEHRUNGSNR'],190,'',$Link);
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFF_AST_ATUNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFF_AST_ATUNR'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFF']['RFF_AST_ATUNR'],100,'',$Link);			
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFF_MENGEFILIALE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFF_MENGEFILIALE'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFF']['RFF_MENGEFILIALE'],140,'',$Link);
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFF_MENGEZENTRALE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFF_MENGEZENTRALE'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFF']['RFF_MENGEZENTRALE'],140,'',$Link);
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
			$Link .= '&Sort=RFK_DATUMABSCHLUSSZEN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFK_DATUMABSCHLUSSZEN'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFK']['RFK_DATUMABSCHLUSSZEN'],190,'',$Link);
			
			$Form->ZeileEnde();
			
			$RFKZeile=0;
			
			while(!$rsRFK->EOF())
			{
				$Form->ZeileStart();
				$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$rsRFK->FeldInhalt('RFK_KEY').'';
				$Form->Erstelle_ListenFeld('RFK_RUECKFUEHRUNGSNR',$rsRFK->FeldInhalt('RFK_RUECKFUEHRUNGSNR'),0,190,false,($RFKZeile%2),'',$Link);
				$Link = '/artikelstamm/artikelstamm_Main.php?AST_ATUNR='.$rsRFK->FeldInhalt('RFF_AST_ATUNR').'&cmdAktion=Artikelinfo target="_blank"';
				$Form->Erstelle_ListenFeld('RFF_AST_ATUNR',$rsRFK->FeldInhalt('RFF_AST_ATUNR'),0,100,false,($RFKZeile%2),'',$Link);
				$Form->Erstelle_ListenFeld('RFF_MENGEFILIALE',$rsRFK->FeldInhalt('RFF_MENGEFILIALE'),0,140,false,($RFKZeile%2),'','');
				$Form->Erstelle_ListenFeld('RFF_MENGEZENTRALE',$rsRFK->FeldInhalt('RFF_MENGEZENTRALE'),0,140,false,($RFKZeile%2),'','');				
				$Form->Erstelle_ListenFeld('RFK_DATUMABSCHLUSSZEN',$rsRFK->FeldInhalt('RFK_DATUMABSCHLUSSZEN'),0,190,false,($RFKZeile%2),'','','D');				
				$Form->ZeileEnde();
		
				$rsRFK->DSWeiter();
				$RFKZeile++;
			}
		
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
			$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		
			$Form->Formular_Ende();									
		}
		
		//***************************************
		// Schaltflächen für dieses Register
		//***************************************
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/rueckfuehrung/bearbeitung/rueckfuehrungsbearbeitung_Main.php?cmdAktion=Auswertung','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');								
		$Form->SchaltflaechenEnde();
		
		$Form->SchreibeHTMLCode('</form>');
		
	}
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
			
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;	
	global $DB;

	$Bedingung = '';
	
	if(isset($Param['RFF_AST_ATUNR']) AND $Param['RFF_AST_ATUNR']!='')
	{				
		$Bedingung .= 'AND RFF_AST_ATUNR ' . $DB->LikeOderIst($Param['RFF_AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';			
	}		
	
	return $Bedingung;
}

?>
