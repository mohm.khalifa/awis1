<?php
global $con;
global $Recht3010;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('MBW','MBW_%');
$TextKonserven[]=array('MBW','txt_LetztePruefung');
$TextKonserven[]=array('MBW','txtAnredenListe');
$TextKonserven[]=array('MBW','AnzAenderungen');
$TextKonserven[]=array('MBW','txtMitbewerberGroesse');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');
$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3010 = awisBenutzerRecht($con,3010,$_SERVER['PHP_AUTH_USER']);
$Recht3011 = awisBenutzerRecht($con,3011,$_SERVER['PHP_AUTH_USER']);

// Für die Suchfunktionen
$INET_LINK_Telefon =  awis_BenutzerParameter($con,'INET_SUCHE_Telefon',$_SERVER['PHP_AUTH_USER']);
$INET_LINK_Karte =  awis_BenutzerParameter($con,'INET_SUCHE_Karte',$_SERVER['PHP_AUTH_USER']);

//awis_Debug(1,$_POST);

//***********************************************
//* Änderungen übernehmen
//***********************************************
if(isset($_GET['Seite']) AND $_GET['Seite']=='Aenderungen' AND isset($_GET['Set']) AND ($Recht3011&8)==8)
{
	$SQL = 'SELECT *';
	$SQL .= ' FROM mitbewerbaenderungen ';
	$SQL .= ' WHERE mae_key='.intval($_GET['Set']).'';

	$rsMAE = awisOpenRecordset($con, $SQL);

	if(isset($rsMAE['MAE_KEY'][0]))
	{
		$FeldName = $rsMAE['MAE_FELDNAME'][0];

		$rsMBW=awisOpenRecordset($con,'SELECT * FROM Mitbewerber WHERE MBW_KEY=0'.$rsMAE['MAE_MBW_KEY'][0]);
		$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsMAE['MAE_NEUERWERT'][0],true);

		$SQL = 'UPDATE Mitbewerber';
		$SQL .= ' SET '.$FeldName.' = '.$WertNeu;
		$SQL .= ' , MBW_USER=\''.$_SERVER['PHP_AUTH_USER'].'\'';
		$SQL .= ' , MBW_USERDAT=SYSDATE';
		$SQL .= ' WHERE MBW_KEY=0'.$rsMAE['MAE_MBW_KEY'][0];
//awis_Debug(1,$SQL);
		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('mitbewerber_aendern',1,'Fehler beim Speichern',$SQL);
		}
		else
		{
			$SQL = 'UPDATE MitbewerbAenderungen SET MAE_STATUS=\'I\',MAE_USERDAT=SYSDATE,MAE_USER=\''.$_SERVER['PHP_AUTH_USER'].'\' WHERE mae_key='.intval($_GET['Set']);
			if(awisExecute($con,$SQL)===false)
			{
				awisErrorMailLink('mitbewerber_aendern',1,$awisDBError['messages'],'');
			}
		}
	}
}


//awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
//awis_Debug(1,$_POST);
if(isset($_POST['cmdSuche_x']))
{
//awis_Debug(1,$_POST);
	$Param = '';
	$Param .= ';'.$_POST['sucSuchName'];
	$Param .= ';'.$_POST['sucSuchPLZ'];
	$Param .= ';'.$_POST['sucSuchOrt'];
	$Param .= ';'.$_POST['sucSuchFiliale'];
	$Param .= ';'.$_POST['sucHerstellerID'];
	$Param .= ';'.$_POST['sucUTyp'];
	$Param .= ';'.$_POST['sucProdukte'];
	$Param .= ';'.$_POST['sucEntfernung'];
	$Param .= ';'.$_POST['sucSucheStrasse'];

	awis_BenutzerParameterSpeichern($con, "MitbewerberSuche" , $_SERVER['PHP_AUTH_USER'] , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER'] , '');
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	$Param = ';;;';
	include('./mitbewerber_loeschen.php');
	$Param = awis_BenutzerParameter($con, "MitbewerberSuche" , $_SERVER['PHP_AUTH_USER']);
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	$Param = ';;;';
	include('./mitbewerber_speichern.php');
	$Param = awis_BenutzerParameter($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER']);
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$Param = '-1;;;';
}
elseif(isset($_GET['MBW_KEY']))
{
	$Param = ''.$_GET['MBW_KEY'].';;;';		// Nur den Key speiechern
}
elseif(isset($_POST['txtMBW_KEY']))
{
	$Param = ''.$_POST['txtMBW_KEY'].';;;';		// Nur den Key speiechern
}
else 		// Nicht über die Suche gekommen, letzte Adresse abfragen
{
	$Param='';
	if(!isset($_GET['Liste']))
	{
		$Param = awis_BenutzerParameter($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER']);
	}
	if($Param=='' OR $Param=='0')
	{
		$Param = awis_BenutzerParameter($con, 'MitbewerberSuche', $_SERVER['PHP_AUTH_USER']);
	}
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = '';
$Param = explode(';',$Param);

// Name angegeben?
if($Param[0]!='')		// Key
{
	$Bedingung .= 'AND MBW_Key = ' . intval($Param[0]) . ' ';
}
if(isset($Param[1]) AND $Param[1]!='')
{
	$Bedingung .= 'AND (UPPER(MBW_Name1) ' . awisLIKEoderIST($Param[1].'%',1) . ' ';
	$Bedingung .= 'OR  UPPER(MBW_Name2) ' . awisLIKEoderIST($Param[1].'%',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[2]) AND $Param[2]!='')		// PLZ
{
	$Bedingung .= 'AND MBW_PLZ = ' . $Param[2] . ' ';
}
if(isset($Param[3]) AND $Param[3]!='')		// Ort
{
	$Bedingung .= 'AND UPPER(MBW_ORT) ' . awisLIKEoderIST($Param[3],1) . ' ';
}
if(isset($Param[4]) AND $Param[4]!='')		// Filiale
{
	$Bedingung .= 'AND MBW_KEY IN (SELECT MFI_MBW_KEY FROM MitbewerberFilialen ';
	$Bedingung .= ' WHERE MFI_FIL_ID = ' . intval($Param[4]);
	if(isset($Param[8]) AND $Param[8]!='')		// Entfernung zur Filiale
	{
		$Bedingung .= ' AND MFI_ENTFERNUNG <= ' . awisDBFeldFormat($Param[8],'N2') . ' ';
	}
	$Bedingung .=  ') ';
}
if(isset($Param[5]) AND $Param[5]!='0')		// Hersteller
{
	$Bedingung .= 'AND MBW_KEY IN (SELECT MBH_MBW_KEY FROM MitbewerbHersteller WHERE MBH_HER_ID = ' . intval($Param[5]) . ') ';
}
if(isset($Param[6]) AND $Param[6]!='0')		// U-Typ
{
	$Bedingung .= 'AND MBW_KEY IN (SELECT MIU_MBW_KEY FROM MitbewerbUTypen WHERE MIU_MUT_KEY = ' . intval($Param[6]) . ') ';
}
if(isset($Param[7]) AND $Param[7]!='0')		// Produktgruppen
{
	$Bedingung .= 'AND MBW_KEY IN (SELECT MPG_MBW_KEY FROM MitbewerbProdGruppen WHERE MPG_MGT_ID = ' . intval($Param[7]) . ') ';
}
if(isset($Param[9]) AND $Param[9]!='')		// Strasse
{
	$Bedingung .= 'AND UPPER(MBW_STRASSE) ' . awisLikeoderIst($Param[9],1) . '';
}


$SQL = 'SELECT DISTINCT Mitbewerber.*';
$SQL .= ' FROM Mitbewerber';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY MBW_Name1, MBW_Name2';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$_SERVER['PHP_AUTH_USER']);
//$SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ROWNUM <='.$MaxDSAnzahl;

$rsMBW = awisOpenRecordset($con, $SQL);
$rsMBWZeilen = $awisRSZeilen;
awis_Debug(1,$SQL,$Param,$_POST);
//********************************************************
// Daten anzeigen
//********************************************************
if($rsMBWZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datensätzen!
{
	echo '<span class=HinweisText>Es wurden keine passenden Mitbewerber gefunden.</span>';
}
elseif($rsMBWZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './mitbewerber_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=MBW_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_NAME1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_NAME1'].'/'.$TXT_TextBaustein['MBW']['MBW_NAME2'],350,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_STRASSE'],250);
	$Link = './mitbewerber_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=MBW_PLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_PLZ'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_PLZ'],100,'',$Link);
	$Link = './mitbewerber_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=MBW_ORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_ORT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_ORT'],200,'',$Link);
	$Link = './mitbewerber_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=MBW_PRUEFUNGAM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='MBW_PRUEFUNGAM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_PRUEFUNGAM'],120,'',$Link);
	awis_FORM_ZeileEnde();

	if($rsMBWZeilen>$MaxDSAnzahl)
	{
//		awis_FORM_Hinweistext('Ausgabe begrenzt auf '.$MaxDSAnzahl.'. Insgesamt '.$rsMBWZeilen.' Zeilen gefunden.');
//		$rsMBWZeilen=$MaxDSAnzahl;
	}

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises blättern
	if($rsMBWZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['Wort']['Seite'],50,'');

		for($i=0;$i<($rsMBWZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./mitbewerber_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	for($ADRZeile=$StartZeile;$ADRZeile<$rsMBWZeilen and $ADRZeile<$StartZeile+$MaxDSAnzahl;$ADRZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = './mitbewerber_Main.php?cmdAktion=Details&&MBW_KEY='.$rsMBW['MBW_KEY'][$ADRZeile].'';
		awis_FORM_Erstelle_ListenFeld('MBW_NAME1',$rsMBW['MBW_NAME1'][$ADRZeile].($rsMBW['MBW_NAME2'][$ADRZeile]==''?'':' '.$rsMBW['MBW_NAME2'][$ADRZeile]),0,350,false,($ADRZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('MBW_STRASSE',$rsMBW['MBW_STRASSE'][$ADRZeile].' '.$rsMBW['MBW_HAUSNUMMER'][$ADRZeile],0,250,false,($ADRZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('MBW_PLZ',$rsMBW['MBW_LAN_CODE'][$ADRZeile].'-'.$rsMBW['MBW_PLZ'][$ADRZeile],0,100,false,($ADRZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('MBW_ORT',$rsMBW['MBW_ORT'][$ADRZeile],0,200,false,($ADRZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('MBW_PRUEFUNGAM',$rsMBW['MBW_PRUEFUNGAM'][$ADRZeile],0,120,false,($ADRZeile%2),'','','D');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Adresse
{
	echo '<form name=frmMitbewerber action=./mitbewerber_Main.php?cmdAktion=Details method=POST>';
	//echo '<table>';
	$MBWKEY = (isset($rsMBW['MBW_KEY'][0])?$rsMBW['MBW_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER'] , $MBWKEY);
	//awis_BenutzerParameterSpeichern($con, "MitbewerberSuche" , $_SERVER['PHP_AUTH_USER'] , ';;'.$rsMBW['MBW_KEY'][0]);

	echo '<input type=hidden name=txtMBW_KEY value='.$MBWKEY. '>';

	awis_FORM_FormularStart();

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./mitbewerber_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$TXT_TextBaustein['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($MBWKEY===0?'':$rsMBW['MBW_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($MBWKEY===0?'':$rsMBW['MBW_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

		// Änderungsantrag
	if(($Recht3011&2)==2 AND isset($_GET['AENDERN']))
	{
		$TabellenKuerzel='MAE';
		$EditRecht = true;
	}
	else
	{
		$TabellenKuerzel='MBW';
		$EditRecht=(($Recht3010&2)!=0);

		if($MBWKEY==0)
		{
			$EditRecht=true;
		}

	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_ANREDE'].':',150);
	$AnredenText = explode("|",$TXT_TextBaustein['MBW']['txtAnredenListe']);
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel.'_ANREDE',($MBWKEY===0?'':$rsMBW['MBW_ANREDE'][0]),300,$EditRecht,$con,'','','3','','',$AnredenText,'');
	if($EditRecht)
	{
		$CursorFeld='txtMBW_ANREDE';
	}

	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_STATUS'].':',70);
	$StatusText = explode("|",$TXT_TextBaustein['Liste']['lst_AktivInaktiv']);
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel.'_STATUS',($MBWKEY===0?'':$rsMBW['MBW_STATUS'][0]),300,$EditRecht,$con,'','','A','','',$StatusText,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_KONZERN'].':',150);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_KONZERN',($MBWKEY===0?'':$rsMBW['MBW_KONZERN'][0]),20,300,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_NAME1'].':',150);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_NAME1',($MBWKEY===0?'':$rsMBW['MBW_NAME1'][0]),50,350,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_NAME2'].':',150);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_NAME2',($MBWKEY===0?'':$rsMBW['MBW_NAME2'][0]),50,350,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_STRASSE'].'/'.$TXT_TextBaustein['MBW']['MBW_HAUSNUMMER'].':',150);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_STRASSE',($MBWKEY===0?'':$rsMBW['MBW_STRASSE'][0]),50,330,$EditRecht);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_HAUSNUMMER',($MBWKEY===0?'':$rsMBW['MBW_HAUSNUMMER'][0]),10,200,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	if($MBWKEY===0)
	{
		$Parameter=array();
	}
	else
	{
		$Parameter = array('$NAME1'=>urlencode($rsMBW['MBW_NAME1'][0]),
					   '$NAME2'=>urlencode($rsMBW['MBW_NAME2'][0]),
					   '$STRASSE'=>urlencode($rsMBW['MBW_STRASSE'][0]),
					   '$HAUSNUMMER'=>urlencode($rsMBW['MBW_HAUSNUMMER'][0]),
					   '$PLZ'=>urlencode($rsMBW['MBW_PLZ'][0]),
					   '$ORT'=>urlencode($rsMBW['MBW_ORT'][0]),
					   '$LAN_CODE'=>urlencode($rsMBW['MBW_LAN_CODE'][0])
					   );
	}

	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_LAN_CODE'].'/'.$TXT_TextBaustein['MBW']['MBW_PLZ'].'/'.$TXT_TextBaustein['MBW']['MBW_ORT'].':',150);
	$SQL = 'SELECT LAN_CODE, LAN_LAND FROM Laender ORDER BY LAN_LAND';
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel.'_LAN_CODE',($MBWKEY===0?'':$rsMBW['MBW_LAN_CODE'][0]),-40,$EditRecht,$con,$SQL,false,(($MBWKEY===0?'':$rsMBW['MBW_LAN_CODE'][0])==''?'DE':''),'LAN_CODE', '','','');
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_PLZ',($MBWKEY===0?'':$rsMBW['MBW_PLZ'][0]),8,80,$EditRecht);
	$Link = ($MBWKEY===0?'':strtr($INET_LINK_Karte,$Parameter));
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_ORT',($MBWKEY===0?'':$rsMBW['MBW_ORT'][0]),50,400,$EditRecht,'','',$Link);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_EROEFFNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_EROEFFNUNG',($MBWKEY===0?'':$rsMBW['MBW_EROEFFNUNG'][0]),10,150,$EditRecht,'','','','D');
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_SCHLIESSUNG'].':',150);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_SCHLIESSUNG',($MBWKEY===0?'':$rsMBW['MBW_SCHLIESSUNG'][0]),10,350,$EditRecht,'','','','D');
	awis_FORM_ZeileEnde();


	if($Recht3010&4096)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_MBK_KEY'].':',150);
		$SQL = 'SELECT MBK_KEY, MBK_KLASSIFIZIERUNG FROM MitbewerberKlassifizierungen ORDER BY MBK_KLASSIFIZIERUNG';
		awis_FORM_Erstelle_SelectFeld('MBW_MBK_KEY',($MBWKEY===0?'':$rsMBW['MBW_MBK_KEY'][0]),300,(($Recht3010&6)!=0),$con,$SQL,'','','MBK_KLASSIFIZIERUNG','','','');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_TELEFON'].':',150);
	$Link = strtr($INET_LINK_Telefon,$Parameter);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_TELEFON',($MBWKEY===0?'':$rsMBW['MBW_TELEFON'][0]),50,400,$EditRecht,'','',$Link);
	awis_FORM_ZeileEnde();

		// Unternehmensgröße in Mitarbeitern
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_GROESSE'].':',150);
	$Liste = explode("|",$TXT_TextBaustein['MBW']['txtMitbewerberGroesse']);
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel.'_GROESSE',($MBWKEY===0?'':$rsMBW['MBW_GROESSE'][0]),300,$EditRecht,null,'','','','','',$Liste,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel('',520);
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_WERKSTATTLEISTUNG'].':',160);
	$Liste = explode("|",$TXT_TextBaustein['Liste']['lst_JaNeinUnbekannt']);
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel.'_WERKSTATTLEISTUNG',($MBWKEY===0?'':$rsMBW['MBW_WERKSTATTLEISTUNG'][0]),200,$EditRecht,null,'','','','','',$Liste,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_BOXEN'].':',150);
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_BOXEN',($MBWKEY===0?'':$rsMBW['MBW_BOXEN'][0]),4,300,$EditRecht);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_WEBSEITE'].':',150);
	$Link = '';
	if(($MBWKEY===0?'':$rsMBW['MBW_WEBSEITE'][0])!='')
	{
		$Link = ($rsMBW['MBW_WEBSEITE'][0]);
	}
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_WEBSEITE',($MBWKEY===0?'':$rsMBW['MBW_WEBSEITE'][0]),100,850,$EditRecht,'','',$Link);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_ONLINESHOP'].':',150);
	$Link = '';
	if(($MBWKEY===0?'':$rsMBW['MBW_ONLINESHOP'][0])!='')
	{
		$Link = ($rsMBW['MBW_ONLINESHOP'][0]);
	}
	awis_FORM_Erstelle_TextFeld($TabellenKuerzel.'_ONLINESHOP',($MBWKEY===0?'':$rsMBW['MBW_ONLINESHOP'][0]),100,850,$EditRecht,'','',$Link);
	awis_FORM_ZeileEnde();



	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_Textarea('MBW_BEMERKUNG',($MBWKEY===0?'':$rsMBW['MBW_BEMERKUNG'][0]),300,80,4,(($Recht3010&16384)!=0),'border-style=solid;');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_PRUEFUNGDURCH'].':',150);
	awis_FORM_Erstelle_TextFeld('MBW_PRUEFUNGDURCH',($MBWKEY===0?'':$rsMBW['MBW_PRUEFUNGDURCH'][0]),40,300,true,'');
	awis_FORM_Erstelle_TextLabel($TXT_TextBaustein['MBW']['MBW_PRUEFUNGAM'].':',150);
	awis_FORM_Erstelle_Textfeld('MBW_PRUEFUNGAM',($MBWKEY===0?'':$rsMBW['MBW_PRUEFUNGAM'][0]),10,80,true,'');
	awis_FORM_ZeileEnde();


	if(($Recht3011&1)==1 AND $MBWKEY!==0)		// Änderungsanträge anzeigen
	{
		awis_FORM_ZeileStart();
		$rsMAE=awisOpenRecordset($con,'SELECT COUNT(*) AS ANZ FROM MitbewerbAenderungen WHERE MAE_STATUS = \'O\' AND MAE_MBW_Key=0'.$rsMBW['MBW_KEY'][0]);
		if($rsMAE['ANZ'][0]!=0)
		{
			awis_FORM_Hinweistext(str_replace('%1',$rsMAE['ANZ'][0],$TXT_TextBaustein['MBW']['AnzAenderungen']),1);
		}
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();

	if(!isset($_POST['cmdDSNeu_x']))
	{
		$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Filialen'));
		echo '<input type=hidden name=Seite value='.awisFeldFormat('T',$RegisterSeite,'DB',false).'>';

		awis_RegisterErstellen(3020, $con, $RegisterSeite);
	}

	echo '<hr>';

	if(($Recht3010&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzufügen erlaubt?
	{
		print "&nbsp;<input tabindex=90 type=image name=cmdDSNeu accesskey=n title='".$TXT_TextBaustein['Wort']['lbl_hinzufuegen']."' src=/bilder/plus.png name=cmdDSNeu>";// onclick=location.href='./mitbewerber_Main.php?cmdAktion=Liste&txtADRKey=-1&DSNeu=True'>";
	}

	if(($Recht3010&(2+4+256))!==0)		//
	{
		echo "&nbsp;<input tabindex=91 accesskey=s type=image src=/bilder/diskette.png title='".$TXT_TextBaustein['Wort']['lbl_speichern']." (ALT+S)' name=cmdSpeichern>";
	}

	if(($Recht3010&8)!==0 AND !isset($_GET['AENDERN']))
	{
		echo "&nbsp;<input tabindeaccesskey=x type=image src=/bilder/Muelleimer_gross.png title='".$TXT_TextBaustein['Wort']['lbl_loeschen']." (ALT+X)' name=cmdLoeschen>";
	}

	if(($Recht3011&2) AND !(isset($_GET['AENDERN'])) AND !isset($_POST['cmdDSNeu_x']))		// Änderungsantrag stellen
	{
		echo "&nbsp;<a href=./mitbewerber_Main.php?cmdAktion=Details&AENDERN><img border=0 src=/bilder/schraubenschluessel.png titel=".$TXT_TextBaustein['Wort']['lbl_aendern']."</a>";
	}

	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsMBW, $_POST, $rsAZG, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}