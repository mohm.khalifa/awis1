<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	//var_dump($_REQUEST);
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZMM','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	$BindeVariablen=array();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht37000 = $AWISBenutzer->HatDasRecht(37000);
	
	$Param = array();
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Sort = 'ZMM_WERTIGKEIT desc';
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{
	  // wenn GET-Sort, dann nach diesen Feld sortieren
	 $Sort = str_replace('~',' DESC ', $_GET['Sort']);
	}
	
	//var_dump ($_POST);
	
	if($Recht37000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_POST['cmdSuche_x']))
	{
	    $Param['ZMM_ZAMINR'] = $Form->Format('T',$_POST['sucZMM_ZAMINR'],true);
	    $Param['ZMM_ZAMIBEZEICHNUNG'] = $Form->Format('T',$_POST['sucZMM_ZAMIBEZEICHNUNG'],true);
	    $Param['ZMM_GUELTIG'] = $_POST['sucZMM_GUELTIG']!=0 ? $Form->Format('T',$_POST['sucZMM_GUELTIG'], true):'';

	    $Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	    $AWIS_KEY1 = '';
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
	    $AWIS_KEY1 = '0'; 
	}
	elseif(isset($_POST['cmdLoeschenOK']))
	{
	    $AWIS_KEY1 = '';
	    include('./zahlungsmittelmatrix_loeschen.php');
	}
	elseif(isset($_GET['ZMM_KEY'])) //Von Detail zu Unterdetail
	{
		$AWIS_KEY1 = $_GET['ZMM_KEY'];
	}
	elseif(isset($_POST['cmdSpeichern_x']) )
	{
	    //var_dump("Bla");
	    //Feldpr�fungen vor dem speichern
	    $AWIS_KEY1 = $_POST['txtZMM_KEY'];
	    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Zahlungsmittelmatrix'));
	    include('./zahlungsmittelmatrix_speichern.php');
	}
	elseif(isset($_POST['cmdLoeschen_x']))
	{
	    $AWIS_KEY1 = $_POST['txtZMM_KEY'];
	    include('./zahlungsmittelmatrix_loeschen.php');   
	}
	else //nicht �ber die Suche gekommen.
	{
	    
	    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Zahlungsmittelmatrix'));       
	}
	if (!isset($_POST['cmdLoeschen_x']))
	{
	    //Grund SELECT: Alle Derzeit G�ltigen Datens�tze
	    $SQL  ='SELECT';
	    $SQL .='   zmm_key ,';
	    $SQL .='   zmm_zaminr ,';
	    $SQL .='   zmm_zamibezeichnung ,';
	    $SQL .='   zmm_rechnungstext ,';
	    $SQL .='   zmm_wertigkeit ,';
	    $SQL .='   zmm_gueltig_von ,';
	    $SQL .='   zmm_gueltig_bis ,';
	    $SQL .='   zmm_user ,';
	    $SQL .='   zmm_skonto ,';
	    $SQL  .='  zmm_zahlungsziel,';
	    $SQL .='   zmm_userdat';
	    $SQL .=' FROM';
	    $SQL .='   zahlungsmittelmatrix';
	    
	    $Bedingung = '';
	    
	    //Bedinnung erstellen KDI_FILID
	    
	    
	    $Bedingung = _BedingungErstellen($Param);
	     
	    if ($Bedingung != '')
	    {
	        $SQL .= ' WHERE ' . $Bedingung;
	    }
	    
	    if (isset($Sort))
	    {
	        $SQL .= ' ORDER BY ' . $Sort;
	    }
	    
	    //var_dump($SQL);
	    $MaxDS = 1;
	    $ZeilenProSeite=1;
	    $Block = 1;
	    
	    // Zum Bl�ttern in den Daten
	    if (isset($_REQUEST['Block']))
	    {
	        if (! isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x']))
	        {
	            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
	        }
	    }
	    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	    
	    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('ZMM',false));
	    //	$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	    $Form->DebugAusgabe(1, $SQL);
	    
	    $rsZMM = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('ZMM',true));
	    
	    // Spaltenbreiten f�r Listenansicht
	    $FeldBreiten = array();
	    $FeldBreiten['ZMM_WERTIGKEIT'] = 100;
	    $FeldBreiten['ZMM_ZAMINR'] = 180;
	    $FeldBreiten['ZMM_ZAMIBEZEICHNUNG'] = 220;
	   // $FeldBreiten['ZMM_RECHNUNGSTEXT'] = 220;
	    $FeldBreiten['ZMM_GUELTIG_VON'] = 90;
	    $FeldBreiten['ZMM_GUELTIG_BIS'] = 90;
	     
	    
	    
	    $Form->SchreibeHTMLCode("<form name=frmDetails method=post action=./zahlungsmittelmatrix_Main.php?cmdAktion=Details>");
	    $Form->Formular_Start();

	    if($AWIS_KEY1 == '')
	    {
	        if (($rsZMM->AnzahlDatensaetze() >= 1))
	        {
	            //Listenansicht
	    
	            $Form->ZeileStart();
	            $Link = './zahlungsmittelmatrix_Main.php?cmdAktion=Details&Sort=ZMM_WERTIGKEIT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZMM_WERTIGKEIT'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZMM']['ZMM_WERTIGKEIT'], $FeldBreiten['ZMM_WERTIGKEIT'], '', $Link);
	            	
	            $Link = './zahlungsmittelmatrix_Main.php?cmdAktion=Details&Sort=ZMM_ZAMINR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZMM_ZAMINR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZMM']['ZMM_ZAMINR'], $FeldBreiten['ZMM_ZAMINR'], '', $Link);
	            	
	            $Link = './zahlungsmittelmatrix_Main.php?cmdAktion=Details&Sort=ZMM_ZAMIBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZMM_ZAMIBEZEICHNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZMM']['ZMM_ZAMIBEZEICHNUNG'], $FeldBreiten['ZMM_ZAMIBEZEICHNUNG'], '', $Link);
	    
	            $Link = './zahlungsmittelmatrix_Main.php?cmdAktion=Details&Sort=ZMM_GUELTIG_VON'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZMM_GUELTIG_VON'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZMM']['ZMM_GUELTIG_VON'], $FeldBreiten['ZMM_GUELTIG_VON'], '', $Link);
	    
	            $Link = './zahlungsmittelmatrix_Main.php?cmdAktion=Details&Sort=ZMM_GUELTIG_BIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZMM_GUELTIG_BIS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZMM']['ZMM_GUELTIG_BIS'], $FeldBreiten['ZMM_GUELTIG_BIS'], '', $Link);
	    
	            	
	            $Form->ZeileEnde();
	            $DS = 0;	// f�r Hintergrundfarbumschaltung
	            while(! $rsZMM->EOF())
	            {
	                //Bei historischen Datensatz
	                if($rsZMM->FeldInhalt('TBL') == 'HIST')
	                {
	                    $Link = './zahlungsmittelmatrix_Main.php?cmdAktion=Details&ZMM_KEY=0'.$rsZMM->FeldInhalt('ZMM_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . '&TBL=HIST';
	                    $Hintergrund = 3;
	                }
	                else
	                {
	                    //Normaler DS
	                    $Link = './zahlungsmittelmatrix_Main.php?cmdAktion=Details&ZMM_KEY=0'.$rsZMM->FeldInhalt('ZMM_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
	                    $Hintergrund = $DS%2;
	                }
	    
	                $Form->ZeileStart();
	    
	                $TTT = $rsZMM->FeldInhalt('ZMM_WERTIGKEIT');
	                $Form->Erstelle_ListenFeld('ZMM_WERTIGKEIT', $rsZMM->FeldInhalt('ZMM_WERTIGKEIT'), 0, $FeldBreiten['ZMM_WERTIGKEIT'], false, $Hintergrund, '', $Link, 'T', 'L', $TTT);
	    
	                $TTT = $rsZMM->FeldInhalt('ZMM_ZAMINR');
	                $Form->Erstelle_ListenFeld('ZMM_ZAMINR', $rsZMM->FeldInhalt('ZMM_ZAMINR'), 0, $FeldBreiten['ZMM_ZAMINR'], false, $Hintergrund, '', '', 'T', 'L', $TTT);
	    
	                $TTT = $rsZMM->FeldInhalt('ZMM_ZAMIBEZEICHNUNG');
	                $Form->Erstelle_ListenFeld('ZMM_ZAMIBEZEICHNUNG', $rsZMM->FeldInhalt('ZMM_ZAMIBEZEICHNUNG'), 0, $FeldBreiten['ZMM_ZAMIBEZEICHNUNG'], false, $Hintergrund, '', '', 'T', 'L', $TTT);
	    
	                $TTT = $rsZMM->FeldInhalt('ZMM_GUELTIG_VON');
	                $Form->Erstelle_ListenFeld('ZMM_GUELTIG_VON', $rsZMM->FeldInhalt('ZMM_GUELTIG_VON'), 0, $FeldBreiten['ZMM_GUELTIG_VON'], false, $Hintergrund, '', '', 'D', 'L', $TTT);
	    
	                $TTT = $rsZMM->FeldInhalt('ZMM_GUELTIG_BIS');
	                $Form->Erstelle_ListenFeld('ZMM_GUELTIG_BIS', $rsZMM->FeldInhalt('ZMM_GUELTIG_BIS'), 0, $FeldBreiten['ZMM_GUELTIG_BIS'], false, $Hintergrund, '', '', 'D', 'L', $TTT);
	    
	                	
	                $Form->ZeileEnde();
	                $DS++;
	                $rsZMM->DSWeiter();
	            }
	            $Link = './Zahlungsmittelmatrix_Main.php?cmdAktion=Details';
	            $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	        }
	        else
	        {
	            $Form->ZeileStart();
	            $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	            $Form->ZeileEnde();
	        }
	    }
	    elseif (isset($_POST['cmdDSNeu_x']) or $AWIS_KEY1 >= 0) //NeuerDS oder UnterDetailsAnsicht
	    {
	        // Spaltenbreiten
	        $FeldBreiten = array();
	        $FeldBreiten['ZMM_WERTIGKEIT'] = 10;
	        $FeldBreiten['ZMM_ZAMINR'] = 10;
	        $FeldBreiten['ZMM_ZAMIBEZEICHNUNG'] = 30;
	        $FeldBreiten['ZMM_RECHNUNGSTEXT'] = 30;
	        $FeldBreiten['ZMM_ZAHLUNGSZIEL'] = 10;
	        $FeldBreiten['ZMM_SKONTO'] = 10;
	        $FeldBreiten['ZMM_GUELTIG_VON'] = 20;
	        $FeldBreiten['ZMM_GUELTIG_BIS'] = 20;
	        $FeldBreiten['Labels'] = 220;
	    
	        $Felder = array();
	        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zahlungsmittelmatrix_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
	        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZMM->FeldInhalt('ZMM_USER'));
	        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZMM->FeldInhalt('ZMM_USERDAT'));
	        $Form->InfoZeile($Felder,'');
	    
	        //$AWIS_KEY1 = isset($_GET['ZMM_KEY'])?$_GET['ZMM_KEY']:'0';
	    
	        //Key f�rs speichern in hiddenfeld stecken
	        $SEQ = isset($SEQ)?$SEQ:0; //Wenn bereits gespeichert wurde
	        $Form->Erstelle_HiddenFeld('ZMM_KEY',  isset($_GET['ZMM_KEY'])?$_GET['ZMM_KEY']:$SEQ);
	    
	        if (($Recht37000&4) == 4)
	        {
	            $Aendern = true;
	        }
	        else
	        {
	            $Aendern = false;
	        }
	         
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_WERTIGKEIT'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_WERTIGKEIT',isset($_POST['txtZMM_WERTIGKEIT'])?$_POST['txtZMM_WERTIGKEIT']:$rsZMM->FeldInhalt('ZMM_WERTIGKEIT'),$FeldBreiten['ZMM_WERTIGKEIT'],$FeldBreiten['ZMM_WERTIGKEIT'],$Aendern,'T','','','T');
	        $Form->ZeileEnde();
	         
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_ZAMINR'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_ZAMINR',isset($_POST['txtZMM_ZAMINR'])?$_POST['txtZMM_ZAMINR']:$rsZMM->FeldInhalt('ZMM_ZAMINR'),$FeldBreiten['ZMM_ZAMINR'],$FeldBreiten['ZMM_ZAMINR'],$Aendern,'T','','','T');
	        $Form->ZeileEnde();
	         
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_ZAMIBEZEICHNUNG'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_ZAMIBEZEICHNUNG',isset($_POST['txtZMM_ZAMIBEZEICHNUNG'])?$_POST['txtZMM_ZAMIBEZEICHNUNG']:$rsZMM->FeldInhalt('ZMM_ZAMIBEZEICHNUNG'),$FeldBreiten['ZMM_ZAMIBEZEICHNUNG'],$FeldBreiten['ZMM_ZAMIBEZEICHNUNG'],$Aendern,'T','','','T');
	        $Form->ZeileEnde();
	        
	        $Form->Trennzeile('L');
	        
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_ZAHLUNGSZIEL'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_ZAHLUNGSZIEL',isset($_POST['txtZMM_ZAHLUNGSZIEL'])?$_POST['txtZMM_ZAHLUNGSZIEL']:$rsZMM->FeldInhalt('ZMM_ZAHLUNGSZIEL'),$FeldBreiten['ZMM_ZAHLUNGSZIEL'],$FeldBreiten['ZMM_ZAHLUNGSZIEL'],$Aendern,'T','','','T');
	        $Form->ZeileEnde();
	         
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_SKONTO'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_SKONTO',isset($_POST['txtZMM_SKONTO'])?$_POST['txtZMM_SKONTO']:$rsZMM->FeldInhalt('ZMM_SKONTO'),$FeldBreiten['ZMM_SKONTO'],$FeldBreiten['ZMM_SKONTO'],$Aendern,'T','','','T');
	        $Form->ZeileEnde();
	         
	        /*
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_RECHNUNGSTEXT'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_RECHNUNGSTEXT',isset($_POST['txtZMM_RECHNUNGSTEXT'])?$_POST['txtZMM_RECHNUNGSTEXT']:$rsZMM->FeldInhalt('ZMM_RECHNUNGSTEXT'),$FeldBreiten['ZMM_RECHNUNGSTEXT'],$FeldBreiten['ZMM_RECHNUNGSTEXT'],$Aendern,'T','','','T');
	        $Form->ZeileEnde();
			*/
	        $Form->Trennzeile('L');
	        
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_GUELTIG_VON'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_GUELTIG_VON',isset($_POST['txtZMM_GUELTIG_VON'])?$_POST['txtZMM_GUELTIG_VON']:$rsZMM->FeldInhalt('ZMM_GUELTIG_VON'),$FeldBreiten['ZMM_GUELTIG_VON'],$FeldBreiten['ZMM_GUELTIG_VON']+160,$Aendern,'D','','','D');
	        $Form->ZeileEnde();
	         
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['ZMM']['ZMM_GUELTIG_BIS'].':',$FeldBreiten['Labels']);
	        $Form->Erstelle_TextFeld('!ZMM_GUELTIG_BIS',isset($_POST['txtZMM_GUELTIG_BIS'])?$_POST['txtZMM_GUELTIG_BIS']:$rsZMM->FeldInhalt('ZMM_GUELTIG_BIS'),$FeldBreiten['ZMM_GUELTIG_BIS'],$FeldBreiten['ZMM_GUELTIG_BIS']+160,$Aendern,'D','','','D');
	        $Form->ZeileEnde();
	         
	    }
	      
	}
	
	
	
	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
		
	//Speichernbutton wenn: ZMM_KEY: Im DetailDatensatz | DSNeu: Bei neu | cmdSpeichern: Nachdem gespeichert wurde.
	if (($Recht37000&4)  == 4 and ((isset($_GET['ZMM_KEY']) or isset($_POST['cmdDSNeu_x']) or isset($_POST['cmdSpeichern_x'])))) 
	{
	    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	    if(($Recht37000&8)==8 AND $AWIS_KEY1>0)
	    {
	        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	    }
	}
	elseif($Recht37000&2 == 2) // Hinzuf�gen immer dann, wenn Recht und kein Speichern da ist
	{
	    $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	if(!isset($_GET['ZMM_KEY']))
	{
	   $AWISBenutzer->ParameterSchreiben('Formular_Zahlungsmittelmatrix',serialize($Param));
	}
}
catch (awisException $ex)
{
    echo $DB->LetzterSQL();
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

function _BedingungErstellen($Param)
{
    global $DB;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $AWISBenutzer;
    $Bedingung = '';
    
    if ($AWIS_KEY1 >= '0')
    {
        $Bedingung .= ' ZMM_KEY='.$AWIS_KEY1;
        return $Bedingung;
    }
    
    if(isset($Param['ZMM_ZAMINR']) and $Param['ZMM_ZAMINR'] != '')
    {
    	$Bedingung .= ' ZMM_ZAMINR='.$DB->WertSetzen('ZMM','N0',$Param['ZMM_ZAMINR']);
    }  
        
    if(isset($Param['ZMM_ZAMIBEZEICHNUNG']) and $Param['ZMM_ZAMIBEZEICHNUNG'] != '')
    {
        $Bedingung = $Bedingung!=''?$Bedingung . ' and ':'';
        $Bedingung .= ' ZMM_ZAMIBEZEICHNUNG='.$DB->WertSetzen('ZMM','T',$Param['ZMM_ZAMIBEZEICHNUNG']);
    }
    

    if(isset($Param['ZMM_GUELTIG']) and $Param['ZMM_GUELTIG'] != '')
    {
        $Bedingung = $Bedingung!=''?$Bedingung . ' and ':'';
        $Bedingung .= ' ZMM_GUELTIG_VON>='.$DB->WertSetzen('ZMM','D',date("d.m.Y",time()));
        $Bedingung .= ' and ZMM_GUELTIG_BIS<='.$DB->WertSetzen('ZMM','D',date("d.m.Y",time()));       
    }

    return $Bedingung;
}

?>