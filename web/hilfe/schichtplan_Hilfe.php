<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $awisRSZeilen;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>
</head>

<body>
<?php

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}
$HilfeRecht = awisBenutzerRecht($con,115);

if($HilfeRecht==0)
{
    awisEreignis(3,1000,'Schichtplan',"$PHP_AUTH_USER",'','','');
    die("Keine ausreichenden Rechte!");
}


/*********************************
* Legende ausgeben
* *******************************/

print "<span class=DatenFeldGross>Legende f�r den Schichtplan</span><br>";

$rsTaetigkeit = awisOpenRecordset($con,"SELECT * FROM PKUERZEL WHERE PEK_ANZEIGESTUFE <= $HilfeRecht ORDER BY PEK_ID, PEK_LAN_WWS_KENN");
$rsTaetigkeitZeilen = $awisRSZeilen;

print "<table border=1>";
print "<tr><td>K&uuml;rzel</td><td>Bezeichnung</td><td>Land</td></tr>";
for($j=0;$j<$rsTaetigkeitZeilen;$j++)
{
	print "<tr><td><b>" . $rsTaetigkeit["PEK_ID"][$j] . "</b></td><td>" . $rsTaetigkeit["PEK_TEXT"][$j] . "</td><td>" . $rsTaetigkeit["PEK_LAN_WWS_KENN"][$j] . "</td></tr>";
}

print "</table>";

awislogoff($con);

print "<br><hr><img alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=history.back();>";

?>
</body>
</html>
