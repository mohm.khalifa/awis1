<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $awisRSZeilen;
global $awisDBFehler;
global $Pflicht_Felder;
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>
<body>
<?php

$Rechtestufe = awisBenutzerRecht($con, 3300);
		//  1=Suchen
		//	2=�ndern
		//	4=Hinzuf�gen
		//	8=L�schen
		// 16=PDF erzeugen
		// 32=Beschwerdebearbeitung Filiale
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Schadens-DB',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$PDF_Link='';
$txt_Bearb_ID=(isset($_REQUEST['txt_Bearb_ID_Save'])?$_REQUEST['txt_Bearb_ID_Save']:'');
$ART=(isset($_REQUEST['ART'])?$_REQUEST['ART']:'');

awis_Debug(1,$_REQUEST);

$Save_Felder=true;
//$Pflicht_Felder='';

if ($_POST['txtSQL_Anweisung']!='INSERT' and $_POST['txtPruefung']!='N' and ($Rechtestufe&32)==32) 
{
	echo "<br>";	
	if (isset($_POST['cmdSpeichern_x']) and Pruefe_Felder()==false)
	{		
		$Save_Felder=false;	
	}	
}

awis_Debug(1,$Pflicht_Felder);

if(isset($_REQUEST['cmdTrotzdemSpeichern'])) 
{
	$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER']));
	
	$SQL='';
		
	if ($ART=='S')
	{					           			
		$SQL_LFDNR="SELECT MAX(LFDNR)+1 AS LFNR FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE FILNR=".$Params[0]." AND SCHADENSJAHR='".date("Y")."'";
			
		$rsResult = awisOpenRecordset($con,$SQL_LFDNR);
		$rsResultZeilen = $awisRSZeilen;
		
		if($rsResultZeilen==0)		// Keine Daten
		{
			die("<center><span class=HinweisText>Datensatz konnte nicht gespeichert werden!</span></center>");
		}
		else
		{
				$LFDNR=str_pad($rsResult['LFNR'][0], 4, "0", STR_PAD_LEFT);
		}
			
		$BEARBNR_NEU=date("y").str_pad($Params[0], 3, "0", STR_PAD_LEFT).$LFDNR;
		$SCHADJAHR=date("Y");
		$STAND=22; //Bearbeitungsstand auf Offen immer vorbelegen
			
		awis_Debug(1,$LFDNR,$BEARBNR_NEU,$SCHADJAHR);
			
		$SQL='INSERT INTO SCHAEDEN_NEW@SCHAD.ATU.DE (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,EINGABEAM,EINGABEDURCH)';
		$SQL.='VALUES(\''.$BEARBNR_NEU.'\',' . $LFDNR . ',' . $Params[0] . ',' . $Params[0] . ','.$Params[16].',\''.$Params[1].'\',\''.$Params[2].'\',\''.$Params[3].'\',\''.$Params[4].'\',\''.$Params[5].'\',\''.$Params[6].'\',\''.$Params[7].'\',\''.$Params[8].'\',\''.$Params[9].'\',\''.$Params[10].'\',\''.$Params[11].'\',\''.$Params[12].'\','.$Params[13].','.($Params[14]!=''?$Params[14]:'NULL').','.$SCHADJAHR.','.$Params[15].',\''."FEHLT".'\',' . $STAND . ',SYSDATE,\''.$_SERVER['PHP_AUTH_USER'].'\')';
	}
			
	awis_Debug(1,$SQL);
	
	if(awisExecute($con, $SQL)===FALSE)
	{
		awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
		echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
		awisLogoff($con);
		die();
	}
	
	awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER'],"");

	if ($Params[15]==303){
		$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
		$SQL.=' VALUES(TRUNC(SYSDATE),\''.$BEARBNR_NEU.'\',\''."BackOffice".'\')';	
	}
	else {
		$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
		$SQL.=' VALUES(TRUNC(SYSDATE)+1,\''.$BEARBNR_NEU.'\',\''."Filialen".'\')';	
	}
	
	if(awisExecute($con, $SQL)===FALSE)
	{
		awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
		echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Wiedervorlage konnte nicht erstellt werden. Zur�ck zur Eingabe</a><p>";
		awisLogoff($con);
		die();
	}
	
	$PDF_Link='./schaden_pdf.php?BEARB_NR='.$BEARBNR_NEU;
	$Hinzufuegen=TRUE;
}

if(isset($_POST['cmdSpeichern_x']) && $Save_Felder==true)
{	
	if ($_POST['txtSQL_Anweisung']=='INSERT')
	{
		$Params[0]=$_POST['txtFIL_ID'];
		$Params[1]=$_POST['txtKundenname'];
		$Params[2]=$_POST['txtKundenvorname'];
		$Params[3]=$_POST['txtStrasse'];
		$Params[4]=$_POST['txtPLZ'];
		$Params[5]=$_POST['txtOrt'];
		$Params[6]=$_POST['txtTelefon'];
		$Params[7]=$_POST['txtTelefon2'];
		$Params[8]=$_POST['txtFax'];
		$Params[9]=$_POST['txtEMail'];
		$Params[10]=$_POST['txtPAN'];
		$Params[11]=$_POST['txtKFZ_Kennz'];
		$Params[12]=$_POST['txtRekl_Text'];
		$Params[13]=$_POST['txtEingang'];
		$Params[14]=$_POST['txtWA_NR'];
		$Params[15]=$_POST['txtSachbearbeiter'];
		$Params[16]=$_POST['txtAnrede'];
		
		$Params[14]=str_replace(" ","",$Params[14]);
		
		awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER'], implode(';',$Params));
		
		awis_Debug(1,$Params);
		
		$SQL='';
		
		if ($ART=='S')
		{

			if (strlen($_POST['txtRekl_Text'])>2500)
			{
				echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
				die("<span class=HinweisText>Der Text der Fehlerbeschreibung ist l�nger als 2500 Zeichen.</span>");
			}
					
			$SQL="select 'S_'||BEARBEITUNGSNR BEARBEITUNGSNR, KUNDENNAME, FILNR";
            $SQL.=" from SCHAEDEN_NEW@SCHAD.ATU.DE where filnr=".$_POST['txtFIL_ID']." and";            
            $SQL.=" schadensjahr = TO_CHAR(sysdate, 'YYYY') and";
            $SQL.=" upper(trim(' ' from kundenname)) like '".strtoupper(trim($Params[1]))."' union";
            $SQL.=" select 'B_'||BEARBEITUNGSNR BEARBEITUNGSNR, KUNDENNAME, FILNR";
            $SQL.=" from BESCHWERDE@SCHAD.ATU.DE where filnr=".$_POST['txtFIL_ID']." and";
            $SQL.=" beschwerdejahr = TO_CHAR(sysdate, 'YYYY') and";
            $SQL.=" upper(trim(' ' from kundenname)) like '".strtoupper(trim($Params[1]))."'";
            
            awis_Debug(1,$SQL);

			$rsErgebnis = awisOpenRecordset($con,$SQL);
			$rsErgebnisZeilen = $awisRSZeilen;
			
			if ($rsErgebnisZeilen > 0)
			{
				$Hinweis =  "F�r die Filiale Nr. ".$_POST['txtFIL_ID'] ."<br>";
				
				if ($rsErgebnisZeilen==1)
				{
					$Hinweis.= "existiert bereits ein Eintrag unter folgender Bearbeitungsnummer: ".$rsErgebnis['BEARBEITUNGSNR'][0];		
				}
				
				else 
				{
					$Hinweis.= "existieren bereits Eintr�ge unter folgender Bearbeitungsnummer: <br>";
					for($i=0;$i<$rsErgebnisZeilen;$i++)
					{
						$Hinweis.= $rsErgebnis['BEARBEITUNGSNR'][$i] ."<br>";
					}	
				}
				echo $Hinweis;						
				echo "<br><img title=zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./schaden_Main.php?cmdAktion=Suche'>";
				echo "&nbsp;<input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdTrotzdemSpeichern onclick=location.href='./schaden_Main.php?cmdAktion=Liste&cmdTrotzdemSpeichern=J&ART=S'>";
				//echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
				die();
			}

		
			
			$SQL_LFDNR="SELECT MAX(LFDNR)+1 AS LFNR FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE FILNR=".$_POST['txtFIL_ID']." AND SCHADENSJAHR='".date("Y")."'";
			
			$rsResult = awisOpenRecordset($con,$SQL_LFDNR);
			$rsResultZeilen = $awisRSZeilen;
		
			if($rsResultZeilen==0)		// Keine Daten
			{
				die("<center><span class=HinweisText>Datensatz konnte nicht gespeichert werden!</span></center>");
			}
			else
			{
					$LFDNR=str_pad($rsResult['LFNR'][0], 4, "0", STR_PAD_LEFT);
			}
			
			$BEARBNR_NEU=date("y").str_pad($_POST['txtFIL_ID'], 3, "0", STR_PAD_LEFT).$LFDNR;
			$SCHADJAHR=date("Y");
			$STAND=22; //Bearbeitungsstand auf OFFEN immer vorbelegen
			
			awis_Debug(1,$LFDNR,$BEARBNR_NEU,$SCHADJAHR);
				
			$SQL='INSERT INTO SCHAEDEN_NEW@SCHAD.ATU.DE (BEARBEITUNGSNR,LFDNR,FILNR,VERURS_FILIALE,GESCHLECHT,KUNDENNAME,VORNAME,STRASSE,PLZ,ORT,TELEFON,TELEFON2,FAX,EMAIL,PAN,KFZ_KENNZ,FEHLERBESCHREIBUNG,EINGANGPER,WANR,SCHADENSJAHR,BEARBEITER,KENNUNG,STAND,EINGABEAM,EINGABEDURCH)';
			$SQL.='VALUES(\''.$BEARBNR_NEU.'\',' . $LFDNR . ',' . $_POST['txtFIL_ID'] . ',' . $_POST['txtFIL_ID'] . ','.$_POST['txtAnrede'].',\''.$_POST['txtKundenname'].'\',\''.$_POST['txtKundenvorname'].'\',\''.$_POST['txtStrasse'].'\',\''.$_POST['txtPLZ'].'\',\''.$_POST['txtOrt'].'\',\''.$_POST['txtTelefon'].'\',\''.$_POST['txtTelefon2'].'\',\''.$_POST['txtFax'].'\',\''.$_POST['txtEMail'].'\',\''.$_POST['txtPAN'].'\',\''.$_POST['txtKFZ_Kennz'].'\',\''.$_POST['txtRekl_Text'].'\','.$_POST['txtEingang'].','.($_POST['txtWA_NR']!=''?$_POST['txtWA_NR']:'NULL').','.$SCHADJAHR.','.$_POST['txtSachbearbeiter'].',\''."FEHLT".'\',' . $STAND . ',SYSDATE,\''.$_SERVER['PHP_AUTH_USER'].'\')';
		}
		
		/*if ($ART=='B')
		{
			
		}*/

		awis_Debug(1,$SQL);
		
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
			echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Zur�ck zur Eingabe</a><p>";
			awisLogoff($con);
			die();
		}
		
		awis_BenutzerParameterSpeichern($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER'],"");

		if ($_POST['txtSachbearbeiter']==303){
			$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL.=' VALUES(TRUNC(SYSDATE),\''.$BEARBNR_NEU.'\',\''."BackOffice".'\')';	
		}
		else {
			$SQL='INSERT INTO WIEDERVORLAGEN_NEW@SCHAD.ATU.DE (WV_DATUM, BEARBEITUNGSNR, WV_AN)';	
			$SQL.=' VALUES(TRUNC(SYSDATE)+1,\''.$BEARBNR_NEU.'\',\''."Filialen".'\')';	
		}
		
		if(awisExecute($con, $SQL)===FALSE)
		{
			awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
			echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&cmdHinzufuegen=TRUE'>Wiedervorlage konnte nicht erstellt werden. Zur�ck zur Eingabe</a><p>";
			awisLogoff($con);
			die();
		}
		
		$PDF_Link='./schaden_pdf.php?BEARB_NR='.$BEARBNR_NEU;
		$Hinzufuegen=TRUE;

	}
	elseif ($_POST['txtSQL_Anweisung']=='UPDATE')
	{
		
		//Pr�fung ob sich die Maskenwerte ge�ndert haben oder ein anderer Benutzer das Feld mitlerweile bearbeitet hat
		//--->
	
		$SQL='';
		
		if ($ART=='S')
		{
			if (strlen($_POST['txtRekl_Text'])>2500)
			{
				echo "<br>&nbsp;<a href='http://ap-srv03/schaden/schaden_Main.php?cmdAktion=Liste&BEARB_ID=".$txt_Bearb_ID."&ART=S'>Zur�ck zur Eingabe</a><p>";	
				die("<span class=HinweisText>Der Text der Fehlerbeschreibung ist l�nger als 2500 Zeichen.</span>");				
			}
			
			$SQL="SELECT * FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
		}else{
			$SQL="SELECT * FROM BESCHWERDE@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
		}
		
		$rsSchad_Save = awisOpenRecordset($con,$SQL);
		$rsSchad_SaveZeilen = $awisRSZeilen;
		
		$SQL='';
		$txtHinweis='';
		
		if($rsSchad_SaveZeilen==0)		// Keine Daten
		{
				awislogoff($con);
				die("<span class=HinweisText>Datensatz wurde nicht gefunden!</span>");
		}
		
		if($_POST['txtEingang'] != $_POST['txtEingang_old'])
		{
			if($rsSchad_Save['EINGANGPER'][0] != $_POST['txtEingang_old'])
			{
				$txtHinweis.=',EINGANGPER von '.$_POST['txtEingang_old']. ' auf '.$rsSchad_Save['EINGANGPER'][0];
			}
			else
			{
				$SQL.=',EINGANGPER='.$_POST['txtEingang'];
			}
		}
		
		if($_POST['txtAnrede'] != $_POST['txtAnrede_old'])
		{
			if($rsSchad_Save['GESCHLECHT'][0] != $_POST['txtAnrede_old'])
			{
				$txtHinweis.=',GESCHLECHT von '.$_POST['txtAnrede_old']. ' auf '.$rsSchad_Save['GESCHLECHT'][0];
			}
			else
			{
				$SQL.=',GESCHLECHT='.$_POST['txtAnrede'];
			}
		}
		
		if($_POST['txtKundenname'] != $_POST['txtKundenname_old'])
		{
			if($rsSchad_Save['KUNDENNAME'][0] != $_POST['txtKundenname_old'])
			{
				$txtHinweis.=',KUNDENNAME von '.$_POST['txtKundenname_old']. ' auf '.$rsSchad_Save['KUNDENNAME'][0];
			}
			else
			{
				$SQL.=',KUNDENNAME=\''.$_POST['txtKundenname'].'\'';
			}
		}
		
		if($_POST['txtKundenvorname'] != $_POST['txtKundenvorname_old'])
		{
			if($rsSchad_Save['VORNAME'][0] != $_POST['txtKundenvorname_old'])
			{
				$txtHinweis.=',VORNAME von '.$_POST['txtKundenvorname_old']. ' auf '.$rsSchad_Save['VORNAME'][0];
			}
			else
			{
				$SQL.=',VORNAME=\''.$_POST['txtKundenvorname'].'\'';
			}
		}
			
		if($_POST['txtStrasse'] != $_POST['txtStrasse_old'])
		{
			if($rsSchad_Save['STRASSE'][0] != $_POST['txtStrasse_old'])
			{
				$txtHinweis.=',STRASSE von '.$_POST['txtStrasse_old']. ' auf '.$rsSchad_Save['STRASSE'][0];
			}
			else
			{
				$SQL.=',STRASSE=\''.$_POST['txtStrasse'].'\'';
			}
		}
		
		if($_POST['txtPLZ'] != $_POST['txtPLZ_old'])
		{
			if($rsSchad_Save['PLZ'][0] != $_POST['txtPLZ_old'])
			{
				$txtHinweis.=',PLZ von '.$_POST['txtPLZ_old']. ' auf '.$rsSchad_Save['PLZ'][0];
			}
			else
			{
				$SQL.=',PLZ=\''.$_POST['txtPLZ'].'\'';
			}
		}
		
		if($_POST['txtOrt'] != $_POST['txtOrt_old'])
		{
			if($rsSchad_Save['ORT'][0] != $_POST['txtOrt_old'])
			{
				$txtHinweis.=',ORT von '.$_POST['txtOrt_old']. ' auf '.$rsSchad_Save['ORT'][0];
			}
			else
			{
				$SQL.=',ORT=\''.$_POST['txtOrt'].'\'';
			}
		}
		
		if($_POST['txtTelefon'] != $_POST['txtTelefon_old'])
		{
			if($rsSchad_Save['TELEFON'][0] != $_POST['txtTelefon_old'])
			{
				$txtHinweis.=',TELEFON von '.$_POST['txtTelefon_old']. ' auf '.$rsSchad_Save['TELEFON'][0];
			}
			else
			{
				$SQL.=',TELEFON=\''.$_POST['txtTelefon'].'\'';
			}
		}
	
		if($_POST['txtTelefon2'] != $_POST['txtTelefon2_old'])
		{
			if($rsSchad_Save['TELEFON2'][0] != $_POST['txtTelefon2_old'])
			{
				$txtHinweis.=',TELEFON2 von '.$_POST['txtTelefon2_old']. ' auf '.$rsSchad_Save['TELEFON2'][0];
			}
			else
			{
				$SQL.=',TELEFON2=\''.$_POST['txtTelefon2'].'\'';
			}
		}
		
		if($_POST['txtEMail'] != $_POST['txtEMail_old'])
		{
			if($rsSchad_Save['EMAIL'][0] != $_POST['txtEMail_old'])
			{
				$txtHinweis.=',EMAIL von '.$_POST['txtEMail_old']. ' auf '.$rsSchad_Save['EMAIL'][0];
			}
			else
			{
				$SQL.=',EMAIL=\''.$_POST['txtEMail'].'\'';
			}
		}
		
		if($_POST['txtPAN'] != $_POST['txtPAN_old'])
		{
			if($rsSchad_Save['PAN'][0] != $_POST['txtPAN_old'])
			{
				$txtHinweis.=',PAN von '.$_POST['txtPAN_old']. ' auf '.$rsSchad_Save['PAN'][0];
			}
			else
			{
				$SQL.=',PAN=\''.$_POST['txtPAN'].'\'';
			}
		}
		
		if ($ART=='S') // �nderungen welche die Tabelle der Sch�den betrifft
		{
			if($_POST['txtFax'] != $_POST['txtFax_old'])
			{
				if($rsSchad_Save['FAX'][0] != $_POST['txtFax_old'])
				{
					$txtHinweis.=',FAX von '.$_POST['txtFax_old']. ' auf '.$rsSchad_Save['FAX'][0];
				}
				else
				{
					$SQL.=',FAX=\''.$_POST['txtFax'].'\'';
				}
			}	
			
			if($_POST['txtKFZ_Kennz'] != $_POST['txtKFZ_Kennz_old'])
			{
				if($rsSchad_Save['KFZ_KENNZ'][0] != $_POST['txtKFZ_Kennz_old'])
				{
					$txtHinweis.=',KFZ_KENNZ von '.$_POST['txtKFZ_Kennz_old']. ' auf '.$rsSchad_Save['KFZ_KENNZ'][0];
				}
				else
				{
					$SQL.=',KFZ_KENNZ=\''.$_POST['txtKFZ_Kennz'].'\'';
				}
			}
			
			if($_POST['txtWANR'] != $_POST['txtWANR_old'])
			{
				if($rsSchad_Save['WANR'][0] != $_POST['txtWANR_old'])
				{
					$txtHinweis.=',WANR von '.$_POST['txtWANR_old']. ' auf '.$rsSchad_Save['WANR'][0];
				}
				else
				{
					$SQL.=',WANR=\''.($_POST['txtWANR']!=''?$_POST['txtWANR']:NULL).'\'';
				}
			}
			
			if($_POST['txtRekl_Text'] != $_POST['txtRekl_Text_old'])
			{
				if($rsSchad_Save['FEHLERBESCHREIBUNG'][0] != $_POST['txtRekl_Text_old'])
				{
					$txtHinweis.=',FEHLERBESCHREIBUNG von '.$_POST['txtRekl_Text_old']. ' auf '.$rsSchad_Save['FEHLERBESCHREIBUNG'][0];
				}
				else
				{
					$SQL.=',FEHLERBESCHREIBUNG=\''.$_POST['txtRekl_Text'].'\'';
				}
			}
			
			if($_POST['txtSachbearbeiter'] != $_POST['txtSachbearbeiter_old'])
			{
				if($rsSchad_Save['BEARBEITER'][0] != $_POST['txtSachbearbeiter_old'])
				{
					$txtHinweis.=',BEARBEITER von '.$_POST['txtSachbearbeiter_old']. ' auf '.$rsSchad_Save['BEARBEITER'][0];
				}
				else
				{
					$SQL.=',BEARBEITER='.$_POST['txtSachbearbeiter'];
				}
			}
			
			if($_POST['txtKennung'] != $_POST['txtKennung_old'])
			{
				if($rsSchad_Save['KENNUNG'][0] != $_POST['txtKennung_old'])
				{
					$txtHinweis.=',Kennung von '.$_POST['txtKennung_old']. ' auf '.$rsSchad_Save['KENNUNG'][0];
				}
				else
				{
					$SQL.=',KENNUNG=\''.$_POST['txtKennung'].'\'';
				}
			}
						
			if($_POST['txtBearbStand'] != $_POST['txtBearbStand_old'])
			{
				if($rsSchad_Save['STAND'][0] != $_POST['txtBearbStand_old'])
				{
					$txtHinweis.=',Stand von '.$_POST['txtBearbStand_old']. ' auf '.$rsSchad_Save['STAND'][0];
				}
				else
				{
					$SQL.=',STAND='.$_POST['txtBearbStand'];
				}
			}
			
			if($_POST['txtUrsprAuftrag'] != $_POST['txtUrsprAuftrag_old'])
			{
				if($rsSchad_Save['AUFTRAGSART_ATU_NEU'][0] != $_POST['txtUrsprAuftrag_old'])
				{
					$txtHinweis.=',Auftragsart von '.$_POST['txtUrsprAuftrag_old']. ' auf '.$rsSchad_Save['AUFTRAGSART_ATU_NEU'][0];
				}
				else
				{
					$SQL.=',AUFTRAGSART_ATU_NEU='.$_POST['txtUrsprAuftrag'];
				}
			}
			
			if($_POST['txtWasBeschaedigt'] != $_POST['txtWasBeschaedigt_old'])
			{
				if($rsSchad_Save['BID'][0] != $_POST['txtWasBeschaedigt_old'])
				{
					$txtHinweis.=',Was besch�digt von '.$_POST['txtWasBeschaedigt_old']. ' auf '.$rsSchad_Save['BID'][0];
				}
				else
				{
					$SQL.=',BID='.$_POST['txtWasBeschaedigt'];
				}
			}
			
			if($_POST['txtWoranGearbeitet'] != $_POST['txtWoranGearbeitet_old'])
			{
				if($rsSchad_Save['SCHADENSGRUND'][0] != $_POST['txtWoranGearbeitet_old'])
				{
					$txtHinweis.=',Woran gearbeitet von '.$_POST['txtWoranGearbeitet_old']. ' auf '.$rsSchad_Save['SCHADENSGRUND'][0];
				}
				else
				{
					if ($_POST['txtWoranGearbeitet'] != '0')
					{
						$SQL.=',SCHADENSGRUND='.$_POST['txtWoranGearbeitet'];	
					}
					else 
					{
						$SQL.=',SCHADENSGRUND=null';
					}
					
				}
			}
			
			if($_POST['txtKontaktKunde'] != $_POST['txtKontaktKunde_old'])
			{
				if($rsSchad_Save['KONTAKT_KUNDE'][0] != $_POST['txtKontaktKunde_old'])
				{
					$txtHinweis.=',Kontakt mit Kunde von '.$_POST['txtKontaktKunde_old']. ' auf '.$rsSchad_Save['KONTAKT_KUNDE'][0];
				}
				else
				{
					$SQL.=',KONTAKT_KUNDE='.$_POST['txtKontaktKunde'];
				}
			}
			
			if($_POST['txtTerminKunde1'] != $_POST['txtTerminKunde1_old'])
			{
				if($rsSchad_Save['TERMIN_KUNDE_1'][0] != $_POST['txtTerminKunde1_old'])
				{
					$txtHinweis.=',Termin 1 mit Kunde von '.$_POST['txtTerminKunde1_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_1'][0];
				}
				else
				{
					$SQL.=',TERMIN_KUNDE_1=\''.$_POST['txtTerminKunde1'].'\'';					
				}
			}
			
			if($_POST['txtTerminKundeWer1'] != $_POST['txtTerminKundeWer1_old'])
			{
				if($rsSchad_Save['TERMIN_KUNDE_WER_1'][0] != $_POST['txtTerminKundeWer1_old'])
				{
					$txtHinweis.=',Termin (Wer) 1 mit Kunde von '.$_POST['txtTerminKundeWer1_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_WER_1'][0];
				}
				else
				{
					$SQL.=',TERMIN_KUNDE_WER_1=\''.$_POST['txtTerminKundeWer1'].'\'';
				}
			}
			
			if($_POST['txtTerminKunde2'] != $_POST['txtTerminKunde2_old'])
			{
				if($rsSchad_Save['TERMIN_KUNDE_2'][0] != $_POST['txtTerminKunde2_old'])
				{
					$txtHinweis.=',Termin 2 mit Kunde von '.$_POST['txtTerminKunde2_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_2'][0];
				}
				else
				{
					$SQL.=',TERMIN_KUNDE_2=\''.$_POST['txtTerminKunde2'].'\'';					
				}
			}
						
			if($_POST['txtTerminKundeWer2'] != $_POST['txtTerminKundeWer2_old'])
			{
				if($rsSchad_Save['TERMIN_KUNDE_WER_2'][0] != $_POST['txtTerminKundeWer2_old'])
				{
					$txtHinweis.=',Termin (Wer) 2 mit Kunde von '.$_POST['txtTerminKundeWer2_old']. ' auf '.$rsSchad_Save['TERMIN_KUNDE_WER_2'][0];
				}
				else
				{
					$SQL.=',TERMIN_KUNDE_WER_2=\''.$_POST['txtTerminKundeWer2'].'\'';
				}
			}
			
			if($_POST['txtInfoGBL'] != $_POST['txtInfoGBL_old'])
			{
				if($rsSchad_Save['INFO_AN_GBL'][0] != $_POST['txtInfoGBL_old'])
				{
					$txtHinweis.=',Info an GBL von '.$_POST['txtInfoGBL_old']. ' auf '.$rsSchad_Save['INFO_AN_GBL'][0];
				}
				else
				{
					$SQL.=',INFO_AN_GBL='.$_POST['txtInfoGBL'];
				}
			}
			
			if($_POST['txtEingabedurch'] != $_POST['txtEingabedurch_old'])
			{
				if($rsSchad_Save['EINGABEDURCH_FIL'][0] != $_POST['txtEingabedurch_old'])
				{
					$txtHinweis.=',Eingabedurch von '.$_POST['txtEingabedurch_old']. ' auf '.$rsSchad_Save['EINGABEDURCH_FIL'][0];
				}
				else
				{
					$SQL.=',EINGABEDURCH_FIL=\''.$_POST['txtEingabedurch'].'\'';
				}
			}
			
			
			
			if($_POST['txtErg_Text'] != '')
			{
				$SQL_BSTANDNEW= 'insert into BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE (BEARBNRNEU,DATUM,BEMERKUNGEN,EINTRAG,BETRAG)';
				$SQL_BSTANDNEW.=' values (\''.$rsSchad_Save['BEARBNRNEU'][0].'\',SYSDATE,\''.$_POST['txtErg_Text'].'\'';
				$SQL_BSTANDNEW.=',\''.$_POST['txtEingabedurch'].'\'';
				$SQL_BSTANDNEW.=',\''.$_POST['txtZahlungWert'].'\')';
								
				if ($_POST['txtZahlungWert'] != '' and $_POST['txtZahlungArt']!= '')
				{
					$SQL_ZAHLUNG= 'insert into ZAHLUNGEN@SCHAD.ATU.DE (zkey,bearbeitungsnr,bid,betrag,verwendungszweck,datum)';
					$SQL_ZAHLUNG.= ' values (zkey_zahlungen.nextval@schad.atu.de,\''.$txt_Bearb_ID.'\',3,\''.$_POST['txtZahlungWert'].'\',\''.$_POST['txtZahlungArt'].'\',SYSDATE)';						
				}
			}				

			if($_POST['txtAusfallursache'] != $_POST['txtAusfallursache_old'])
			{
				if($rsSchad_Save['AUSFALLURSACHE'][0] != $_POST['txtAusfallursache_old'])
				{
					$txtHinweis.=',Ausfallursache von '.$_POST['txtAusfallursache_old']. ' auf '.$rsSchad_Save['AUSFALLURSACHE'][0];
				}
				else 
				{				
					$SQL.=',AUSFALLURSACHE='.$_POST['txtAusfallursache'];
				}
			}
			
	}
		
		if ($ART=='B') // �nderungen welche die Tabelle der Bechwerden betrifft
		{
			if($_POST['txtSachbearbeiter'] != $_POST['txtSachbearbeiter_old'])
			{
				if($rsSchad_Save['SACHBEARBEITER'][0] != $_POST['txtSachbearbeiter_old'])
				{
					$txtHinweis.=',SACHBEARBEITER von '.$_POST['txtSachbearbeiter_old']. ' auf '.$rsSchad_Save['SACHBEARBEITER'][0];
				}
				else
				{
					$SQL.=',SACHBEARBEITER='.$_POST['txtSachbearbeiter'];
				}
			}
			
			if($_POST['txtRekl_Text'] != $_POST['txtRekl_Text_old'])
			{
				if($rsSchad_Save['BESCHWERDEGRUND'][0] != $_POST['txtRekl_Text_old'])
				{
					$txtHinweis.=',BESCHWERDEGRUND von '.$_POST['txtRekl_Text_old']. ' auf '.$rsSchad_Save['BESCHWERDEGRUND'][0];
				}
				else
				{
					$SQL.=',BESCHWERDEGRUND='.$_POST['txtRekl_Text'];	
				}
			}
		}
			
		//<---
		
		if($SQL_BSTANDNEW!='')
		{
			awis_Debug(1,$SQL_BSTANDNEW);
				
			if(awisExecute($con, $SQL_BSTANDNEW)===FALSE)
			{
				awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}			
		}
		
		if($SQL_ZAHLUNG!='')
		{
			awis_Debug(1,$SQL_ZAHLUNG);
				
			if(awisExecute($con, $SQL_ZAHLUNG)===FALSE)
			{
				awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
				awisLogoff($con);
				die();
			}			
		}
		
		// Update- Befehl		
		
		if($txtHinweis=='' && $SQL!='')
		{
				
				$SQL.=',EINGABEAM=SYSDATE';
		
				if ($ART=='S')
				{
					$SQL.=',EINGABEDURCH=\''.$_SERVER['PHP_AUTH_USER'].'\'';
					$SQL="UPDATE SCHAEDEN_NEW@SCHAD.ATU.DE SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
				}
				else 
				{
					$SQL="UPDATE BESCHWERDE@SCHAD.ATU.DE SET " .substr($SQL,1)." WHERE BEARBEITUNGSNR='".$txt_Bearb_ID."'";
				}
				
				awis_Debug(1,$_REQUEST,$SQL);
				
				if(awisExecute($con, $SQL)===FALSE)
				{
					awisErrorMailLink("schaden_Liste.php", 2, $awisDBFehler);
					awisLogoff($con);
					die();
				}
		}
		elseif($txtHinweis!='')
		{
				echo $txtHinweis;
				awislogoff($con);
				
				die("<br><span class=HinweisText>Datensatz wurde von einen anderen Benutzer ge�ndert !</span>");
		}
	}
}
if ((isset($_POST['cmdHinzufuegen_x']) && ($Rechtestufe&4)==4) || (((isset($Hinzufuegen) && $Hinzufuegen==TRUE) || (isset($_REQUEST['cmdHinzufuegen']) && $_REQUEST['cmdHinzufuegen']==TRUE)) && ($Rechtestufe&4)==4))
{
	if($PDF_Link!='' && ($Rechtestufe&16)==16)
	{
		echo "<br>";
		echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a>&nbsp;PDF-Datei erzeugen";
		echo "<p>";		
	}
	
	$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Speichern", $_SERVER['PHP_AUTH_USER']));
	
	if($_REQUEST['txtAuftragsart_NEU']!='')
	{
		$ART=$_REQUEST['txtAuftragsart_NEU'];
	}
	else {$ART='S';}
	
	awis_Debug(1,$Params);
	
	echo "<form name=frmSchadenListe method=post action=./schaden_Main.php?cmdAktion=Liste>";
	echo "<input type=hidden name=ART  value='" . $ART . "'>";
	echo "<input type=hidden name=txtSQL_Anweisung value='INSERT'>";
	echo "<input type=hidden name=txtPruefung value='N'>";
	
	
	echo "<table width=100% border=1>";
	echo "<tr>";
	echo "<td colspan=2 align=right><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>&nbsp;<input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td>";
		echo "<table border=0>";
		echo '<tr>';
		echo '<td  id=FeldBez>Eingang per</td>';
		echo "<td><select name=txtEingang>";
			echo "<option value='NULL'>Bitte w�hlen...</option>";
			$rsEingang = awisOpenRecordset($con, 'SELECT id, initcap(mittel) as Eingang FROM EINGANGPER@SCHAD.ATU.DE ORDER BY 1');
			$rsEingangZeilen = $awisRSZeilen;
			
			if(empty($Params[13]))
			{
				for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
				{
					echo '<option value=' . $rsEingang['ID'][$EingangZeile] . '>' . $rsEingang['EINGANG'][$EingangZeile] . '</option>';
				}
			}
			else 
			{
				for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
				{
					if ($Params[13]==$rsEingang['ID'][$EingangZeile])
					{
						echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
					}
					else
					{
						echo '<option value=' . $rsEingang['ID'][$EingangZeile] . '>' . $rsEingang['EINGANG'][$EingangZeile] . '</option>';
					}
				}
			}
			echo '</select></td>';
			unset($rsEingang);
		echo '</tr>';
		echo '<tr><td><br></td></tr>';
		
				
		if ($ART=='S')
		{
			echo '<tr>';
			echo '<td id=FeldBez>KFZ-Kennzeichen</td>';
			echo "<td><input type=text name=txtKFZ_Kennz value='".(empty($Params[11])?"":$Params[11]) ."' size=30></td>";
			echo '</tr>';
			echo '<tr>';
			echo '<td  id=FeldBez>Filiale</td>';
			echo "<td><select name=txtFIL_ID>";
				echo "<option value='999'>Bitte w�hlen...</option>";
				$rsFIL = awisOpenRecordset($con, "SELECT FIL_ID, FIL_BEZ, NVL2(FIL_ORTSTEIL,FIL_ORT ||'-'|| FIL_ORTSTEIL,FIL_ORT) AS ORT, FIL_ORT, FIL_ORTSTEIL, FIL_STRASSE FROM V_FIL_VKL_GBL ORDER BY FIL_ORT");
				$rsFILZeilen = $awisRSZeilen;
				
				if(empty($Params[0]))
				{
					for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
					{
							echo '<option value=' . $rsFIL['FIL_ID'][$FILZeile] . '>' . $rsFIL['ORT'][$FILZeile] . ' | ' . $rsFIL['FIL_STRASSE'][$FILZeile] . ' | ' . $rsFIL['FIL_ID'][$FILZeile] . '</option>';
					}
				}
				else 
				{
					for($FILZeile=0;$FILZeile<$rsFILZeilen;$FILZeile++)
					{
						if($Params[0]==$rsFIL['FIL_ID'][$FILZeile])
						{
							echo "<option value=" . $rsFIL['FIL_ID'][$FILZeile] . " selected='selected'>" . $rsFIL['ORT'][$FILZeile] . " | " . $rsFIL['FIL_STRASSE'][$FILZeile] . " | " . $rsFIL['FIL_ID'][$FILZeile] . "</option>";	
						}
						else 
						{
							echo '<option value=' . $rsFIL['FIL_ID'][$FILZeile] . '>' . $rsFIL['ORT'][$FILZeile] . ' | ' . $rsFIL['FIL_STRASSE'][$FILZeile] . ' | ' . $rsFIL['FIL_ID'][$FILZeile] . '</option>';
						}
					}
				}
				echo '</select></td>';
				unset($rsFIL);
			echo '</tr>';
			echo '<tr>';
			echo '<td id=FeldBez>WA-Nummer</td>';
			echo "<td><input type=text name=txtWA_NR value='".(empty($Params[14])?"":$Params[14]) ."' size=10></td>";
			echo '</tr>';
			
		}
		else
		{
			echo '<tr>';
			echo '<td  id=FeldBez>Beschwerdegrund</td>';
			echo "<td><select name=txtRekl_Text>";
				echo '<option value=-1>Bitte w�hlen...</option>';
				$rsRekl_Text = awisOpenRecordset($con, 'SELECT ID, BESCHWERDEGRUND FROM BESCHWERDEGRUENDE@SCHAD.ATU.DE ORDER BY 1');
				$rsRekl_TextZeilen = $awisRSZeilen;
				for($Rekl_TextZeile=0;$Rekl_TextZeile<$rsRekl_TextZeilen;$Rekl_TextZeile++)
				{
						echo '<option value=' . $rsRekl_Text['ID'][$Rekl_TextZeile] . '>' . $rsRekl_Text['BESCHWERDEGRUND'][$Rekl_TextZeile] . '</option>';
				}
				echo '</select></td>';
				unset($rsRekl_Text);
			echo '</tr>';
			
		}
		
		echo '<tr><td><br></td></tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Sachbearbeiter</td>';
		echo "<td><select name=txtSachbearbeiter>";
			echo '<option value=0>Bitte w�hlen...</option>';
			$rsSB = awisOpenRecordset($con, 'SELECT ID, SBNAME,SBVORNAME FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE ID IN (303,304) ORDER BY 1');
			$rsSBZeilen = $awisRSZeilen;
			
			if(empty($Params[15]))
			{
				for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
				{
						echo '<option value=' . $rsSB['ID'][$SBZeile] . '>' . $rsSB['SBNAME'][$SBZeile] . '' . $rsSB['SBVORNAME'][$SBZeile] . '</option>';
				}
			}
			else 
			{
				for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
				{
					if($Params[15]==$rsSB['ID'][$SBZeile])
					{
						echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile] . "" . $rsSB['SBVORNAME'][$SBZeile] . "</option>";	
					}
					else 
					{
						echo '<option value=' . $rsSB['ID'][$SBZeile] . '>' . $rsSB['SBNAME'][$SBZeile] . '' . $rsSB['SBVORNAME'][$SBZeile] . '</option>';
					}
				}
			}
			echo '</select></td>';
			unset($rsSB);
		echo '</tr>';
		
		echo '</table>';
	
	
	
	
	echo "</td>";
	
	echo "<td>";
		echo "<table border=0";
		
		echo '<tr>';
		echo '<td  id=FeldBez>Anrede</td>';
		echo "<td><select name=txtAnrede>";
			$rsAnrede = awisOpenRecordset($con, 'SELECT ID, NAME FROM ANREDE@SCHAD.ATU.DE ORDER BY 1');
			$rsAnredeZeilen = $awisRSZeilen;
			
			if(empty($Params[16]))
			{
				for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
				{
					echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
				}
			}
			else 
			{
				for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
				{
					if ($Params[16]==$rsAnrede['ID'][$AnredeZeile])
					{
						echo "<option value=" . $rsAnrede['ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['NAME'][$AnredeZeile] . "</option>";
					}
					else
					{
						echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
					}
				}
			}
			echo '</select></td>';
			unset($rsAnrede);
		echo '</tr>';
		echo '<tr>';
		echo '<td  id=FeldBez>Kundenname / -vorname</td>';
		echo "<td><input type=text name=txtKundenname value='".(empty($Params[1])?"":$Params[1]) ."' size=30>&nbsp;<input type=text name=txtKundenvorname value='".(empty($Params[2])?"":$Params[2]) ."' size=30></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>Strasse</td>';
		echo "<td><input type=text name=txtStrasse value='".(empty($Params[3])?"":$Params[3]) ."' size=30></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>PLZ / Ort</td>';
		echo "<td><input type=text name=txtPLZ value='".(empty($Params[4])?"":$Params[4]) ."' size=10>&nbsp;<input type=text name=txtOrt value='".(empty($Params[5])?"":$Params[5]) ."' size=30></td>";
		echo '</tr>';
		echo '<tr>';
		echo "<td id=FeldBez>Telefon / Telefon 2</td>";
		echo "<td><input type=text name=txtTelefon value='".(empty($Params[6])?"":$Params[6]) ."' size=30>&nbsp;<input type=text name=txtTelefon2 value='".(empty($Params[7])?"":$Params[7]) ."' size=30></td>";
		echo '</tr>';
		if ($ART=='S')
		{
			echo '<tr>';
			echo '<td id=FeldBez>Fax</td>';
			echo "<td><input type=text name=txtFax value='".(empty($Params[8])?"":$Params[8]) ."' size=30></td>";
			echo '</tr>';
		}
		echo '<tr>';
		echo '<td id=FeldBez>E-Mail</td>';
		echo "<td><input type=text name=txtEMail value='".(empty($Params[9])?"":$Params[9]) ."' size=30></td>";
		echo '</tr>';
		echo '<tr>';
		echo '<td id=FeldBez>ATU-Card-Nummer</td>'; 
		echo "<td><input type=text name=txtPAN value='".(empty($Params[10])?"":$Params[10]) ."' size=30></td>";
		echo '</tr>';
		
		echo "</table>";
	
	echo "</td>";
	echo "</tr>";	
	echo "</table>";
	
	if ($ART=='S'){
		echo "<table border=0>";
		echo '<tr>';
		echo '<td id=FeldBez>Beschwerdebeschreibung (max. 2500 Zeichen)</td>';
		echo '</tr>';
		echo '<tr>';
		echo "<td><textarea name=txtRekl_Text rows=7 cols=150>".(empty($Params[12])?"":$Params[12]) ."</textarea></td>";
		echo '</tr>';
		echo "</table>";
	}
	
	echo "</form>";
	
}
elseif ((isset($_REQUEST['BEARB_ID']) || (isset($_POST['BEARB_ID']) && $_POST['BEARB_ID']!='')) && ! isset($_POST['cmdAnzeigen_x'])) //Anzeigen der Details f�r Bearbeitungsnummer 
{
	if (isset($_POST['BEARB_ID']) && $_POST['BEARB_ID']!='')
	{
		$BEARB_ID=$_POST['BEARB_ID'];
	}
	else 
	{
		$BEARB_ID=$_REQUEST['BEARB_ID'];
	}
	
	if (isset($_POST['ART']) && $_POST['ART']!='')
	{
		$ART=$_POST['ART'];
	}
	else 
	{
		$ART=$_REQUEST['ART'];
	}
	
	awis_Debug(1,$_REQUEST);
	
	echo "<form name=frmSchadenListe method=post action=./schaden_Main.php?cmdAktion=Liste>";
	echo "<table width=100% border=0><tr>";
	if (($Rechtestufe&2)==2)
	{
		echo "<td colspan=2 align=right><input type=image accesskey=S title='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>&nbsp;<input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	}else
	{
		echo "<td colspan=2 align=right><input type=image border=0 src=/bilder/NeueListe.png name=cmdAnzeigen title='Trefferliste'></td>";
	}
	echo "</tr>";
	echo "</table>";
	echo "<input type=hidden name=txt_Bearb_ID_Save  value='" . $BEARB_ID . "'>";
	echo "<input type=hidden name=ART  value='" . $ART . "'>";
	echo "<input type=hidden name=BEARB_ID value='" . $BEARB_ID . "'>";
	echo "<input type=hidden name=txtSQL_Anweisung value='UPDATE'>";
	echo "<input type=hidden name=txtPruefung value='J'>";
	
	//echo "<center><h3>HiFi-Reklamationsauftrag</h3></center>";
	
	if ($ART=='S')
	{
		$SQL = "SELECT ART, FEHLERBESCHREIBUNG AS REKL_TEXT, FILNR, BEARBEITUNGSNR, BEARBEITER AS SACHBEARBEITER, GESCHLECHT, KUNDENNAME, VORNAME, ";
		$SQL .= "STRASSE, PLZ, ORT, TELEFON, TELEFON2, FAX, EMAIL, PAN, KFZ_KENNZ, EINGANGPER, WANR, STAND, KENNUNG, ";
		$SQL .= "AUFTRAGSART_ATU_NEU, BID, SCHADENSGRUND, TERMIN_KUNDE_1, TERMIN_KUNDE_2, TERMIN_KUNDE_WER_1, TERMIN_KUNDE_WER_2, KONTAKT_KUNDE, INFO_AN_GBL, AUSFALLURSACHE, EINGABEDURCH_FIL ";
		$SQL .= "FROM SCHAEDEN_NEW@SCHAD.ATU.DE ";
	}
	else 
	{
		$SQL = "SELECT ART, BESCHWERDEGRUND AS REKL_TEXT, FILNR, BEARBEITUNGSNR, SACHBEARBEITER, KUNDENNAME, VORNAME, ";
		$SQL .= "STRASSE, PLZ, ORT, TELEFON, EMAIL, PAN, EINGANGPER ";
		$SQL .= "FROM BESCHWERDE@SCHAD.ATU.DE ";
	}

	//$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
	//$FIL_ID = $FIL_ID[1];


	// Beschr�nkung auf eine Filiale?
	$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);

	//if($UserFilialen!='' AND intval($FIL_ID) <> intval($UserFilialen))
	//{
	//	awis_FORM_Hinweistext($TXT_Baustein['Wort']['KeineBerechtigungLieferungskontrollen'],1);
	//	die;
	//}
	
	$Fil_User=substr($_SERVER['PHP_AUTH_USER'],0,4);
	//$Fil_Nr==substr($_SERVER['PHP_AUTH_USER'],4,4);
	$FIL_ID='064';
	
	$SQL.="WHERE BEARBEITUNGSNR='". $BEARB_ID ."'";
	
	if ($Fil_User=='fil-')
	{
		$SQL.= " AND FILNR='".intval($FIL_ID)."'";
		$SQL.= " AND BEARBEITER in (304)";
	}
	
	awis_Debug(1,$SQL);
	
	$rsSchad = awisOpenRecordset($con,$SQL,true,true);
	$rsSchadZeilen = $awisRSZeilen;	
	
	if ($rsSchadZeilen==0)
	{
		echo "<center><span class=HinweisText>F&uuml;r die angegebenen Bearbeitungsnummer wurde kein Ergebniss gefunden!</span></center>";
	}
	else
	{
		for($i=0;$i<$rsSchadZeilen;$i++)
		{
			if ($rsSchad['STAND'][$i]==22)
			{
				$aendern=true;
			}
			else 
			{
				$aender=false;
			}
			
			echo "<table width=100% border=1>";
			echo '<tr>';
			echo '<td>';
				echo "<table border=0>";
				echo '<tr>';
				echo '<td  id=FeldBez>Auftragsart</td>';
				echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . ($rsSchad['ART'][$i]=='S'?'Schaden':'Beschwerde') . "</td>";
				echo '</tr>';
				echo '<tr>';
				echo '<td  id=FeldBez>Bearbeitungs-Nr</td>';
				echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchad['BEARBEITUNGSNR'][$i] . "</td>";
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Eingang per</td>';
				echo "<td id=TabellenZeileGrau><select name=txtEingang>";
					$rsEingang = awisOpenRecordset($con, 'SELECT ID, INITCAP(MITTEL) AS EINGANG FROM EINGANGPER@SCHAD.ATU.DE');
					$rsEingangZeilen = $awisRSZeilen;
					
					echo "<option value='NULL' selected='selected'>:: Kein Angabe ::</option>";
					
					for($EingangZeile=0;$EingangZeile<$rsEingangZeilen;$EingangZeile++)
					{
						if($rsEingang['ID'][$EingangZeile]==$rsSchad['EINGANGPER'][$i])
						{
							echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
						}
						else if($Save_Felder==False and $Pflicht_Felder[20] != '' and $Pflicht_Felder[20]==$rsEingang['ID'][$EingangZeile])
						{
							echo "<option value=" . $rsEingang['ID'][$EingangZeile] . " selected='selected'>" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
						}
						else 
						{
							echo "<option value=" . $rsEingang['ID'][$EingangZeile] . ">" . $rsEingang['EINGANG'][$EingangZeile] . "</option>";
						}
					}
					
				echo "</select><input type=hidden name=txtEingang_old value=".$rsSchad['EINGANGPER'][$i] .">";
				echo '<tr><td><br></td></tr>';
				echo '<tr>';
				echo '<td id=FeldBez>KFZ-Kennzeichen</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKFZ_Kennz size=30 value='" . $Pflicht_Felder[21] . "'><input type=hidden name=txtKFZ_Kennz_old  value='" . $rsSchad['KFZ_KENNZ'][$i] . "'></td>";
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKFZ_Kennz size=30 value='" . $rsSchad['KFZ_KENNZ'][$i] . "'><input type=hidden name=txtKFZ_Kennz_old  value='" . $rsSchad['KFZ_KENNZ'][$i] . "'></td>";
				}
				echo '</tr>';
				echo '<tr>';
				echo '<td  id=FeldBez>Filial-Nr</td>';
				echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchad['FILNR'][$i] . "</td>";
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>WA-Nummer</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtWANR size=10 value='" . $Pflicht_Felder[22] . "'><input type=hidden name=txtWANR_old  value='" . $rsSchad['WANR'][$i] . "'></td>";					
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtWANR size=10 value='" . $rsSchad['WANR'][$i] . "'><input type=hidden name=txtWANR_old  value='" . $rsSchad['WANR'][$i] . "'></td>";					
				}				
				echo '</tr>';
				echo '<tr><td><br></td></tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Sachbearbeiter</td>';
				echo "<td id=TabellenZeileGrau><select name=txtSachbearbeiter>";
					$rsSB = awisOpenRecordset($con, 'SELECT ID, SBNAME, SBVORNAME FROM SACHBEARBEITER@SCHAD.ATU.DE WHERE ID IN (303,304)');
					$rsSBZeilen = $awisRSZeilen;
					
					echo "<option value='NULL'>:: Kein Angabe ::</option>";
					
					for($SBZeile=0;$SBZeile<$rsSBZeilen;$SBZeile++)
					{
						if($rsSB['ID'][$SBZeile]==$rsSchad['SACHBEARBEITER'][$i])
						{
							echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
						}						
						
						else if($Save_Felder==False and $Pflicht_Felder[23] != '' and $Pflicht_Felder[23]==$rsSB['ID'][$SBZeile])
						{
							echo "<option value=" . $rsSB['ID'][$SBZeile] . " selected='selected'>" . $rsSB['SBNAME'][$SBZeile] ." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";	
						}
						else 
						{
							echo "<option value=" . $rsSB['ID'][$SBZeile] . ">" . $rsSB['SBNAME'][$SBZeile]." " .$rsSB['SBVORNAME'][$SBZeile] . "</option>";
						}
					}
					
				echo "</select><input type=hidden name=txtSachbearbeiter_old value=".$rsSchad['SACHBEARBEITER'][$i] ."></td>";
				echo '</tr>';
				echo "</table>";
			echo '</td>';
			echo '<td>';
				echo "<table border=0>";
				
				echo '<tr>';
				echo '<td  id=FeldBez>Anrede</td>';
				echo "<td><select name=txtAnrede>";
					$rsAnrede = awisOpenRecordset($con, 'SELECT ID, NAME FROM ANREDE@SCHAD.ATU.DE ORDER BY 1');
					$rsAnredeZeilen = $awisRSZeilen;
					
						for($AnredeZeile=0;$AnredeZeile<$rsAnredeZeilen;$AnredeZeile++)
						{
							if ($rsSchad['GESCHLECHT'][$i]==$rsAnrede['ID'][$AnredeZeile])
							{
								echo "<option value=" . $rsAnrede['ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['NAME'][$AnredeZeile] . "</option>";
							}
							else if($Save_Felder==False and $Pflicht_Felder[24] != '' and $Pflicht_Felder[24]==$rsAnrede['ID'][$AnredeZeile])
							{
								echo "<option value=" . $rsAnrede['ID'][$AnredeZeile] . " selected='selected'>" . $rsAnrede['NAME'][$AnredeZeile] . "</option>";
							}
							else
							{
								echo '<option value=' . $rsAnrede['ID'][$AnredeZeile] . '>' . $rsAnrede['NAME'][$AnredeZeile] . '</option>';
							}
						}
					echo "</select><input type=hidden name=txtAnrede_old value=".$rsSchad['GESCHLECHT'][$i] ."></td>";
					unset($rsAnrede);
				echo '</tr>';
				echo '<tr>';
				echo '<td  id=FeldBez>Kundenname / -vorname</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKundenname size=30 value='" . $Pflicht_Felder[25] . "'><input type=hidden name=txtKundenname_old  value='" . $rsSchad['KUNDENNAME'][$i] . "'>";
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtKundenname size=30 value='" . $rsSchad['KUNDENNAME'][$i] . "'><input type=hidden name=txtKundenname_old  value='" . $rsSchad['KUNDENNAME'][$i] . "'>";					
				}				
				echo "&nbsp;";
				if($Save_Felder==False)
				{
					echo "<input type=text name=txtKundenvorname size=30 value='" . $Pflicht_Felder[26] . "'><input type=hidden name=txtKundenvorname_old  value='" . $rsSchad['VORNAME'][$i] . "'></td>";
				}
				else 
				{
					echo "<input type=text name=txtKundenvorname size=30 value='" . $rsSchad['VORNAME'][$i] . "'><input type=hidden name=txtKundenvorname_old  value='" . $rsSchad['VORNAME'][$i] . "'></td>";					
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Strasse</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtStrasse size=30 value='" . $Pflicht_Felder[27] . "'><input type=hidden name=txtStrasse_old  value='" . $rsSchad['STRASSE'][$i] . "'></td>";
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtStrasse size=30 value='" . $rsSchad['STRASSE'][$i] . "'><input type=hidden name=txtStrasse_old  value='" . $rsSchad['STRASSE'][$i] . "'></td>";
				}
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>PLZ / Ort</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPLZ size=10 value='" . $Pflicht_Felder[28] . "'><input type=hidden name=txtPLZ_old  value='" . $rsSchad['PLZ'][$i] . "'>";					
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPLZ size=10 value='" . $rsSchad['PLZ'][$i] . "'><input type=hidden name=txtPLZ_old  value='" . $rsSchad['PLZ'][$i] . "'>";					
				}				
				echo '&nbsp;';
				if($Save_Felder==False)
				{
					echo "<input type=text name=txtOrt size=30 value='" . $Pflicht_Felder[29] . "'><input type=hidden name=txtOrt_old  value='" . $rsSchad['ORT'][$i] . "'></td>";
				}
				else 
				{
					echo "<input type=text name=txtOrt size=30 value='" . $rsSchad['ORT'][$i] . "'><input type=hidden name=txtOrt_old  value='" . $rsSchad['ORT'][$i] . "'></td>";
				}
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Telefon / Telefon 2</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtTelefon size=30 value='" . $Pflicht_Felder[30] . "'><input type=hidden name=txtTelefon_old  value='" . $rsSchad['TELEFON'][$i] . "'>";
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtTelefon size=30 value='" . $rsSchad['TELEFON'][$i] . "'><input type=hidden name=txtTelefon_old  value='" . $rsSchad['TELEFON'][$i] . "'>";					
				}				
				echo "&nbsp;";
				if($Save_Felder==False)
				{
					echo "<input type=text name=txtTelefon2 size=30 value=" . $Pflicht_Felder[31] . "><input type=hidden name=txtTelefon2_old  value='" . $rsSchad['TELEFON2'][$i] . "'></td>";
				}
				else 
				{
					echo "<input type=text name=txtTelefon2 size=30 value=" . $rsSchad['TELEFON2'][$i] . "><input type=hidden name=txtTelefon2_old  value='" . $rsSchad['TELEFON2'][$i] . "'></td>";	
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>Fax</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtFax size=30 value='" . $Pflicht_Felder[32] . "'><input type=hidden name=txtFax_old  value='" . $rsSchad['FAX'][$i] . "'></td>";
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtFax size=30 value='" . $rsSchad['FAX'][$i] . "'><input type=hidden name=txtFax_old  value='" . $rsSchad['FAX'][$i] . "'></td>";					
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>E-Mail</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtEMail size=30 value='" . $Pflicht_Felder[33] . "'><input type=hidden name=txtEMail_old  value='" . $rsSchad['EMAIL'][$i] . "'></td>";
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtEMail size=30 value='" . $rsSchad['EMAIL'][$i] . "'><input type=hidden name=txtEMail_old  value='" . $rsSchad['EMAIL'][$i] . "'></td>";					
				}				
				echo '</tr>';
				echo '<tr>';
				echo '<td id=FeldBez>ATU-Card-Nummer</td>';
				if($Save_Felder==False)
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPAN size=30 value='" . $Pflicht_Felder[34] . "'><input type=hidden name=txtPAN_old  value='" . $rsSchad['PAN'][$i] . "'></td>";
				}
				else 
				{
					echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><input type=text name=txtPAN size=30 value='" . $rsSchad['PAN'][$i] . "'><input type=hidden name=txtPAN_old  value='" . $rsSchad['PAN'][$i] . "'></td>";					
				}				
				echo '</tr>';
				echo "</table>";
			echo '</td>';
			echo '</tr>';
			echo "</table>";
			
			echo "<table border=0>";
			echo '<tr>';
			echo '<td id=FeldBez>Beschwerdebeschreibung (max. 2500 Zeichen)</td>';
			echo '</tr>';
			echo '<tr>';
			if (($Rechtestufe&32)==32)
			{
				echo '<td>'. $rsSchad['REKL_TEXT'][$i].'</td>';
			}
			else 
			{
				echo "<td " . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') ."><textarea name=txtRekl_Text  rows=7 cols=150>" . $rsSchad['REKL_TEXT'][$i] . "</textarea><input type=hidden name=txtRekl_Text_old  value='" . $rsSchad['REKL_TEXT'][$i] . "'></td>";			
			}
			echo '</tr>';
			echo "</table>";
		
		if (($Rechtestufe&32)==32)
		{		
			echo "<br><hr noshade><b>Beschwerdebearbeitung / R�ckmeldung Filiale</b>";
			echo "<br><br>";
			
			echo "<table width=80% border=0>";
			echo "<tr>";
			echo "<td>";
				echo "<table border=0>";			
				echo "<tr>";
				echo "<td id=FeldBez>Kontakt mit Kunde*</td>";
				echo "<td><select name=txtKontaktKunde>";
				$rsKontaktKunde = awisOpenRecordset($con, 'SELECT * FROM KONTAKT_KUNDE@SCHAD.ATU.DE ORDER BY WERT');
				$rsKontaktKundeZeilen = $awisRSZeilen;
				
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
				
				if($Save_Felder==False)
				{
					for($KontaktKundeZeile=0;$KontaktKundeZeile<$rsKontaktKundeZeilen;$KontaktKundeZeile++)
					{
						if($Pflicht_Felder[3]==$rsKontaktKunde['ID'][$KontaktKundeZeile])
						{
							echo "<option value=" . $rsKontaktKunde['ID'][$KontaktKundeZeile] . " selected='selected'>" . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . "</option>";							
						}						
						else
						{
							echo '<option value=' . $rsKontaktKunde['ID'][$KontaktKundeZeile] . '>' . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($KontaktKundeZeile=0;$KontaktKundeZeile<$rsKontaktKundeZeilen;$KontaktKundeZeile++)
					{
						if ($rsSchad['KONTAKT_KUNDE'][0]==$rsKontaktKunde['ID'][$KontaktKundeZeile])
						{
							echo "<option value=" . $rsKontaktKunde['ID'][$KontaktKundeZeile] . " selected='selected'>" . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . "</option>";
						}					
						else 
						{
							echo '<option value=' . $rsKontaktKunde['ID'][$KontaktKundeZeile] . '>' . $rsKontaktKunde['WERT'][$KontaktKundeZeile] . '</option>';
						}
					}										
				}
								
				echo "</select><input type=hidden name=txtKontaktKunde_old value=".$rsSchad['KONTAKT_KUNDE'][$i] ."></td>";
				unset($rsKontaktKunde);	
				echo "</tr>";								
				
				echo "<tr>";
				echo '<td id=FeldBez>Termin mit Kunden am</td>';
				if($Save_Felder==False)
				{
					echo "<td><input type=text name=txtTerminKunde1 size=10 value='" . $Pflicht_Felder[13] . "'><input type=hidden name=txtTerminKunde1_old  value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'>";
				}
				else 
				{
					echo "<td><input type=text name=txtTerminKunde1 size=10 value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'><input type=hidden name=txtTerminKunde1_old  value='" . $rsSchad['TERMIN_KUNDE_1'][$i] . "'>";
				}			
				
				if($Save_Felder==False)
				{
					echo "Wer:<input type=text name=txtTerminKundeWer1 size=20 value='" . $Pflicht_Felder[14] . "'><input type=hidden name=txtTerminKundeWer1_old  value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'>";
				}
				else 
				{
					echo "Wer:<input type=text name=txtTerminKundeWer1 size=20 value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'><input type=hidden name=txtTerminKundeWer1_old  value='" . $rsSchad['TERMIN_KUNDE_WER_1'][$i] . "'>";
				}
				echo "</td></tr>";
				
				echo "<tr>";
				echo '<td id=FeldBez>Termin mit Kunden am</td>';
				if($Save_Felder==False)
				{
					echo "<td><input type=text name=txtTerminKunde2 size=10 value='" . $Pflicht_Felder[15] . "'><input type=hidden name=txtTerminKunde2_old  value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'>";
				}
				else 
				{
					echo "<td><input type=text name=txtTerminKunde2 size=10 value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'><input type=hidden name=txtTerminKunde2_old  value='" . $rsSchad['TERMIN_KUNDE_2'][$i] . "'>";
				}			
				
				if($Save_Felder==False)
				{
					echo "Wer:<input type=text name=txtTerminKundeWer2 size=20 value='" . $Pflicht_Felder[16] . "'><input type=hidden name=txtTerminKundeWer2_old  value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'>";
				}
				else 
				{
					echo "Wer:<input type=text name=txtTerminKundeWer2 size=20 value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'><input type=hidden name=txtTerminKundeWer2_old  value='" . $rsSchad['TERMIN_KUNDE_WER_2'][$i] . "'>";
				}
				echo "</td></tr>";			
				
				echo "<tr>";
				echo "<td id=FeldBez>Info an GBL*</td>";			
				echo "<td><select name=txtInfoGBL>";			
				$rsInfoGBL = awisOpenRecordset($con, 'SELECT * FROM INFO_AN_GBL@SCHAD.ATU.DE ORDER BY WERT');
				$rsInfoGBLZeilen = $awisRSZeilen;
							
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
								
				if($Save_Felder==False)
				{
					for($InfoGBLZeile=0;$InfoGBLZeile<$rsInfoGBLZeilen;$InfoGBLZeile++)
					{
						if($Pflicht_Felder[4]==$rsInfoGBL['ID'][$InfoGBLZeile])
						{
							echo "<option value=" . $rsInfoGBL['ID'][$InfoGBLZeile] . " selected='selected'>" . $rsInfoGBL['WERT'][$InfoGBLZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsInfoGBL['ID'][$InfoGBLZeile] . '>' . $rsInfoGBL['WERT'][$InfoGBLZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($InfoGBLZeile=0;$InfoGBLZeile<$rsInfoGBLZeilen;$InfoGBLZeile++)
					{
						if ($rsSchad['INFO_AN_GBL'][0]==$rsInfoGBL['ID'][$InfoGBLZeile])
						{
							echo "<option value=" . $rsInfoGBL['ID'][$InfoGBLZeile] . " selected='selected'>" . $rsInfoGBL['WERT'][$InfoGBLZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsInfoGBL['ID'][$InfoGBLZeile] . '>' . $rsInfoGBL['WERT'][$InfoGBLZeile] . '</option>';
						}
					}										
				}
				
				echo "</select></td><input type=hidden name=txtInfoGBL_old value=".$rsSchad['INFO_AN_GBL'][$i] .">&nbsp;&nbsp;</td>";
				unset($rsInfoGBL);
				echo "</select></td>";
				echo "</tr>";										
				
				echo "<tr>";
				echo "<td id=FeldBez>Ggf. Zahlung</td>";
				$SQLZL="SELECT * FROM ZAHLUNGEN@SCHAD.ATU.DE WHERE EINGABEDURCH_FILIALE=1 and BEARBEITUNGSNR= '". $rsSchad['BEARBEITUNGSNR'][$i] ."' order by ZKEY DESC";
				$rsZahlung = awisOpenRecordset($con, $SQLZL);
				$rsZahlungZeilen = $awisRSZeilen;
				if ($rsZahlungZeilen > 0)						
				{
					echo "<td>Wert: <input type=text name=txtZahlungWert value='" .$rsZahlung['BETRAG'][0]. "' size=10>";			
				}
				else 
				{
					if($Save_Felder==False)
					{
						echo "<td>Wert: <input type=text name=txtZahlungWert value='" .$Pflicht_Felder[12]."' size=10>";			
					}
					else 
					{
						echo "<td>Wert: <input type=text name=txtZahlungWert size=10>";			
					}
				}
				
				echo "<select name=txtZahlungArt>";
				$rsZahlungDurch = awisOpenRecordset($con, 'SELECT * FROM ZAHLUNG_DURCH@SCHAD.ATU.DE ORDER BY WERT');
				$rsZahlungDurchZeilen = $awisRSZeilen;
				
				echo "<option value='' selected='selected'>:: Bitte w�hlen ::</option>";

				for($ZahlungDurchZeile=0;$ZahlungDurchZeile<$rsZahlungDurchZeilen;$ZahlungDurchZeile++)
				{
					if ($rsZahlungZeilen > 0 AND $rsZahlung['VERWENDUNGSZWECK'][0]==$rsZahlungDurch['WERT'][$ZahlungDurchZeile])
					{
						echo "<option value=" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . " selected='selected'>" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . "</option>";
					}
					else if($Save_Felder==False and $Pflicht_Felder[11] != '' and $Pflicht_Felder[11]==$rsZahlungDurch['WERT'][$ZahlungDurchZeile])
					{
						echo "<option value=" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . " selected='selected'>" . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . "</option>";						
					}
					else 
					{
						echo '<option value=' . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . '>' . $rsZahlungDurch['WERT'][$ZahlungDurchZeile] . '</option>';
					}
				}
			    //echo "</select><input type=hidden name=txtKennung_old value=".$rsSchad['KENNUNG'][$i] ."></td>";			
				echo "</select></td>";
			    unset($rsZahlungDurch);					
				echo "</tr>";
				echo "</table>";
				
			echo "</td>";
			echo "<td>";
				echo "<table border=0>";						
				echo "<tr>";						
				echo "<td id=FeldBez>Kennung*</td>";
				echo "<td><select name=txtKennung>";
				$rsKennung = awisOpenRecordset($con, 'SELECT * FROM KENNUNGEN@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY KENNUNG');
				$rsKennungZeilen = $awisRSZeilen;
				
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
				
				if($Save_Felder==False)
				{
					for($KennungZeile=0;$KennungZeile<$rsKennungZeilen;$KennungZeile++)
					{
						if($Pflicht_Felder[1]==$rsKennung['KENNUNG'][$KennungZeile])
						{
							echo "<option value=" . $rsKennung['KENNUNG'][$KennungZeile] . " selected='selected'>" . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . "</option>";													
						}						
						else
						{
							echo '<option value=' . $rsKennung['KENNUNG'][$KennungZeile] . '>' . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($KennungZeile=0;$KennungZeile<$rsKennungZeilen;$KennungZeile++)
					{
						if ($rsSchad['KENNUNG'][0]==$rsKennung['KENNUNG'][$KennungZeile])	
						{
							echo "<option value=" . $rsKennung['KENNUNG'][$KennungZeile] . " selected='selected'>" . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . "</option>";
						}	
						else 
						{
							echo '<option value=' . $rsKennung['KENNUNG'][$KennungZeile] . '>' . $rsKennung['KENNUNG_BEZEICHNUNG'][$KennungZeile] . '</option>';
						}			
					}										
				}
				
				echo "</select><input type=hidden name=txtKennung_old value=".$rsSchad['KENNUNG'][$i] ."></td>";			
				unset($rsKennung);	
				echo "</tr>";
	
				echo "<tr>";
				echo "<td id=FeldBez>Ursrp. Auftrag</td>";
				echo "<td><select name=txtUrsprAuftrag>";
				$rsAuftragsarten = awisOpenRecordset($con, 'SELECT * FROM AUFTRAGSARTEN@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY AUFTRAGSART_ATU');
				$rsAuftragsartenZeilen = $awisRSZeilen;
					
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
					
				if($Save_Felder==False)
				{
					for($AuftragsartenZeile=0;$AuftragsartenZeile<$rsAuftragsartenZeilen;$AuftragsartenZeile++)
					{
						if($Pflicht_Felder[17]==$rsAuftragsarten['ART_ID'][$AuftragsartenZeile])
						{
							echo "<option value=" . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . " selected='selected'>" . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . "</option>";							
						}						
						else
						{
							echo '<option value=' . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . '>' . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($AuftragsartenZeile=0;$AuftragsartenZeile<$rsAuftragsartenZeilen;$AuftragsartenZeile++)
					{
						if ($rsSchad['AUFTRAGSART_ATU_NEU'][0]==$rsAuftragsarten['ART_ID'][$AuftragsartenZeile])
						{
							echo "<option value=" . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . " selected='selected'>" . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . "</option>";							
						}	
						else 
						{
							echo '<option value=' . $rsAuftragsarten['ART_ID'][$AuftragsartenZeile] . '>' . $rsAuftragsarten['AUFTRAGSART_ATU'][$AuftragsartenZeile] . '</option>';
						}			
					}										
				}
					
				echo "</select><input type=hidden name=txtUrsprAuftrag_old value=".$rsSchad['AUFTRAGSART_ATU_NEU'][$i] ."></td>";
				unset($rsAuftragsarten);
				echo "</tr>";
				
				echo "<tr>";
				echo "<td id=FeldBez>Woran gearbeitet</td>";
				echo "<td><select name=txtWoranGearbeitet>";
				$rsSchadensgrund = awisOpenRecordset($con, 'SELECT * FROM SCHADENSGRUND@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY GRUND');
				$rsSchadensgrundZeilen = $awisRSZeilen;
					
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
					
				if($Save_Felder==False)
				{
					for($SchadensgrundZeile=0;$SchadensgrundZeile<$rsSchadensgrundZeilen;$SchadensgrundZeile++)
					{
						if($Pflicht_Felder[18]==$rsSchadensgrund['ID'][$SchadensgrundZeile])
						{
							echo "<option value=" . $rsSchadensgrund['ID'][$SchadensgrundZeile] . " selected='selected'>" . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsSchadensgrund['ID'][$SchadensgrundZeile] . '>' . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($SchadensgrundZeile=0;$SchadensgrundZeile<$rsSchadensgrundZeilen;$SchadensgrundZeile++)
					{
						if ($rsSchad['SCHADENSGRUND'][0]==$rsSchadensgrund['ID'][$SchadensgrundZeile])
						{
							echo "<option value=" . $rsSchadensgrund['ID'][$SchadensgrundZeile] . " selected='selected'>" . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . "</option>";
						}
						else
						{
							echo '<option value=' . $rsSchadensgrund['ID'][$SchadensgrundZeile] . '>' . $rsSchadensgrund['GRUND'][$SchadensgrundZeile] . '</option>';
						}					}										
				}	

				echo "</select><input type=hidden name=txtWoranGearbeitet_old value=".$rsSchad['SCHADENSGRUND'][$i] ."></td>";
				unset($rsAuftragsarten);
				echo "</tr>";
				
				echo "<tr>";
				echo "<td id=FeldBez>Was besch�digt</td>";
				echo "<td><select name=txtWasBeschaedigt>";
				$rsBeschaedigt = awisOpenRecordset($con, 'SELECT * FROM BESCHAEDIGT@SCHAD.ATU.DE WHERE VERALTET=0 AND SICHTBAR_FILIALEN=1 ORDER BY SCHLAGWORT');
				$rsBeschaedigtZeilen = $awisRSZeilen;
					
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
				
				if($Save_Felder==False)
				{
					for($BeschaedigtZeile=0;$BeschaedigtZeile<$rsBeschaedigtZeilen;$BeschaedigtZeile++)
					{
						if($Pflicht_Felder[19]==$rsBeschaedigt['BID'][$BeschaedigtZeile])
						{
							echo "<option value=" . $rsBeschaedigt['BID'][$BeschaedigtZeile] . " selected='selected'>" . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . "</option>";
						}						
						else
						{						
							echo '<option value=' . $rsBeschaedigt['BID'][$BeschaedigtZeile] . '>' . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($BeschaedigtZeile=0;$BeschaedigtZeile<$rsBeschaedigtZeilen;$BeschaedigtZeile++)
					{
						if ($rsSchad['BID'][0]==$rsBeschaedigt['BID'][$BeschaedigtZeile])
						{
							echo "<option value=" . $rsBeschaedigt['BID'][$BeschaedigtZeile] . " selected='selected'>" . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . "</option>";
						}
						else
						{
							echo '<option value=' . $rsBeschaedigt['BID'][$BeschaedigtZeile] . '>' . $rsBeschaedigt['SCHLAGWORT'][$BeschaedigtZeile] . '</option>';
						}
					}
				}	
						
				echo "</select><input type=hidden name=txtWasBeschaedigt_old value=".$rsSchad['BID'][$i] ."></td>";
				unset($rsBeschaedigt);
				echo "</tr>";
				
				//Wird nur in bestimmten F�llen angezeigt
				if (isset($Pflicht_Felder[6]) or isset($Pflicht_Felder[7]) or isset($Pflicht_Felder[8]) or isset($Pflicht_Felder[9]) or isset($Pflicht_Felder[10]))
				{
					echo "<tr>";
					echo "<td id=FeldBez>Ausfallursache</td>";
					echo "<td><select name=txtAusfallursache>";
						if ($Pflicht_Felder[6]==38 AND $Pflicht_Felder[7]=='' AND $Pflicht_Felder[8]=='' AND $Pflicht_Felder[9]=='' AND $Pflicht_Felder[10]=='')
						{
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE_BREMSE@SCHAD.ATU.DE ORDER BY WERT";
						}
						elseif ($Pflicht_Felder[7]==51 AND $Pflicht_Felder[6]=='' AND $Pflicht_Felder[8]=='' AND $Pflicht_Felder[9]=='' AND $Pflicht_Felder[10]=='')
						{
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE_MOTOR@SCHAD.ATU.DE ORDER BY WERT";
						}
						elseif ($Pflicht_Felder[8]==53 AND $Pflicht_Felder[7]=='' AND $Pflicht_Felder[6]=='' AND $Pflicht_Felder[9]=='' AND $Pflicht_Felder[10]=='')
						{
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE_REIFEN@SCHAD.ATU.DE ORDER BY WERT";
						}
						elseif ($Pflicht_Felder[9]==59 AND $Pflicht_Felder[7]=='' AND $Pflicht_Felder[8]=='' AND $Pflicht_Felder[6]=='' AND $Pflicht_Felder[10]=='')
						{
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE_ZAHNRIEMEN@SCHAD.ATU.DE ORDER BY WERT";
						}
						else
						{
							$SQLAusfallursache="SELECT * FROM AUSFALLURSACHE_OELWECHSEL@SCHAD.ATU.DE ORDER BY WERT";
						}
						//awis_Debug(1,$SQLAusfallursache);
						$rsAusfallursache = awisOpenRecordset($con, $SQLAusfallursache);
						$rsAusfallursacheZeilen = $awisRSZeilen;
						
						echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";
						
							for($AusfallursacheZeile=0;$AusfallursacheZeile<$rsAusfallursacheZeilen;$AusfallursacheZeile++)
							{
								echo '<option value=' . $rsAusfallursache['CODE'][$AusfallursacheZeile] . '>' . $rsAusfallursache['WERT'][$AusfallursacheZeile] . '</option>';
							}
						echo "</select><input type=hidden name=txtAusfallursache_old value=".$rsSchad['AUSFALLURSACHE'][$i] ."></td>";
						unset($rsAusfallursache);
					echo "</tr>";	
				}
				
					
				echo "</table>";
				
			echo "</td>";
			echo "</table>";
			
			$SQL = "SELECT * FROM BEARBEITUNGSSTAND_NEW@SCHAD.ATU.DE ";
			$SQL .= "WHERE BEARBNRNEU=(SELECT BEARBNRNEU FROM SCHAEDEN_NEW@SCHAD.ATU.DE WHERE BEARBEITUNGSNR='". $BEARB_ID ."')";
		
			$rsSchadInfo = awisOpenRecordset($con,$SQL,true,true);
			$rsSchadInfoZeilen = $awisRSZeilen;	
			
			echo "Diagnose / Ergebnis und besprochene Ma�nahme (max. 500 Zeichen)*";
			
			if ($rsSchadInfoZeilen==0)
			{
				echo "<table border=0>";
				echo '<tr>';				
				if($Save_Felder==False and $Pflicht_Felder[0]!='')
				{
					echo "<td><textarea name=txtErg_Text rows=2 cols=150>".$Pflicht_Felder[0]."</textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
				}
				else 
				{				
					echo "<td><textarea name=txtErg_Text  rows=2 cols=150></textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
				}
				
				echo '</tr>';
				echo "</table>";
			}
			else
			{
				echo "<table border=0>";
				
				echo '<colgroup>';
			    echo '<col width="100">';
			    echo '<col width="800">';
			    echo '<col width="100">';
			  	echo '</colgroup>';
			  	
				echo '<tr>';
				echo "<td id=FeldBez width=100>Datum</td>";
				echo "<td id=FeldBez width=800>Diagnose / Ergebnis</td>";
				echo "<td id=FeldBez width=100>Betrag</td>";			
				echo '</tr>';
				
				for($ii=0;$ii<$rsSchadInfoZeilen;$ii++)
				{
					echo '<tr>';
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">". $rsSchadInfo['DATUM'][$ii] ."</td>";
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" . $rsSchadInfo['BEMERKUNGEN'][$ii] . "</td>";
					echo "<td " . (($ii%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .">" .awis_format($rsSchadInfo["BETRAG"][$ii],"Currency")."</td>";			
					echo '</tr>';	
				}
				echo "</table>";
				
				echo "<table border=0>";
				echo "<tr>";
				echo "<td id=FeldBez>Neuer Eintrag*</td>";
				echo "</tr>";
				echo "<tr>";				
				if($Save_Felder==False and $Pflicht_Felder[0]!='')
				{
					echo "<td><textarea name=txtErg_Text rows=2 cols=150>".$Pflicht_Felder[0]."</textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
				}
				else 
				{				
					echo "<td><textarea name=txtErg_Text  rows=2 cols=150></textarea><input type=hidden name=txtErg_Text_old  value=''></td>";			
				}
				
				
				echo "</tr>";
				
				echo "</table>";
				
			}
	
			echo "<table>";
			echo "<tr>";
			echo "<td id=FeldBez>Bearbeitungsstand*</td>";
			echo "&nbsp;<td><select name=txtBearbStand>";
						
				$rsStand = awisOpenRecordset($con, 'SELECT * FROM STAND@SCHAD.ATU.DE WHERE STANDID IN (14,15,19,22) ORDER BY BESCHREIBUNG');
				$rsStandZeilen = $awisRSZeilen;
				
				echo "<option value=0 selected='selected'>:: Bitte w�hlen ::</option>";

				if($Save_Felder==False)
				{
					for($StandZeile=0;$StandZeile<$rsStandZeilen;$StandZeile++)
					{
						if($Pflicht_Felder[2]==$rsStand['STANDID'][$StandZeile])
						{
							echo "<option value=" . $rsStand['STANDID'][$StandZeile] . " selected='selected'>" . $rsStand['BESCHREIBUNG'][$StandZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsStand['STANDID'][$StandZeile] . '>' . $rsStand['BESCHREIBUNG'][$StandZeile] . '</option>';
						}
					}				
				}
				else 
				{
					for($StandZeile=0;$StandZeile<$rsStandZeilen;$StandZeile++)
					{
						if ($rsSchad['STAND'][0]==$rsStand['STANDID'][$StandZeile])
						{
							echo "<option value=" . $rsStand['STANDID'][$StandZeile] . " selected='selected'>" . $rsStand['BESCHREIBUNG'][$StandZeile] . "</option>";
						}						
						else
						{
							echo '<option value=' . $rsStand['STANDID'][$StandZeile] . '>' . $rsStand['BESCHREIBUNG'][$StandZeile] . '</option>';
						}
					}				
					
				}
				echo "</select></td><input type=hidden name=txtBearbStand_old value=".$rsSchad['STAND'][$i] .">&nbsp;&nbsp;</td>";
				unset($rsStand);
			
			echo "<td>";
			echo '<td id=FeldBez>Eingabe / Bearbeitet durch*</td>';
			if($Save_Felder==False and $Pflicht_Felder[5]!='')
			{
				echo "<td><input type=text name=txtEingabedurch size=20 value=".$Pflicht_Felder[5].">";							
			}
			else 
			{			
				echo "<td><input type=text name=txtEingabedurch size=20 value=".$rsSchad['EINGABEDURCH_FIL'][$i].">";
			}
			echo "<input type=hidden name=txtEingabedurch_old value=".$rsSchad['EINGABEDURCH_FIL'][$i].">";
			echo "</td></tr>";
			echo "</table>";
		}
		}
	}

	
		
	echo "</form>";
	
	if ($ART=='S' && ($Rechtestufe&16)==16)
	{
		echo "<form name=frmSchadenListe method=post action=./schaden_pdf.php>";
		echo "<input type=image align=right accesskey=P title='PDF erstellen (Alt+P)' src=/bilder/pdf_gross.png name=cmdProtokoll>";
		echo "<input type=hidden name=BEARB_NR value='".$BEARB_ID."'>";
		echo "</form>";
	}
}
else //Zweig f�r Listen-Aufbau nach Suche
{
		if(isset($_POST['cmdSuche_x']))			
		{
			$Params[0] = $_POST['txtAuftragsart'];
			$Params[1] = $_POST['txtBearbeitungsnummer'];
			$Params[2] = $_POST['txtFilialNr'];
			$Params[3] = $_POST['txtKundenname'];
			$Params[4] = $_POST['txtStrasse'];
			$Params[5] = $_POST['txtPLZ'];
			$Params[6] = $_POST['txtOrt'];
			$Params[7] = $_POST['txtTelefon'];
			$Params[8] = $_POST['txtEMail'];
			$Params[9] = $_POST['txtPAN'];
			$Params[10] = $_POST['txtSachbearbeiter'];
			
			
			awis_BenutzerParameterSpeichern($con, "Schaden-Suche", $_SERVER['PHP_AUTH_USER'], implode(';',$Params));
		}
		else
		{
				$Params=explode(';',awis_BenutzerParameter($con, "Schaden-Suche", $_SERVER['PHP_AUTH_USER']));
		}
		
		awis_ZeitMessung(0);
		
		$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$_SERVER['PHP_AUTH_USER']);

		if ($Params[0]=='S')
		{
			$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
			$SQL .= " DECODE(NVL(TELEFON2,'-1'),'-1',TELEFON,TELEFON||' ; '||TELEFON2) AS TELNR, EMAIL,";
			$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
			$SQL .= " FROM SCHAEDEN_NEW@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (BEARBEITER=ID)";
			
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND (TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0) . ' OR TELEFON2 ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='(TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0) . ' OR TELEFON2 ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
		}
		elseif ($Params[0]=='B')
		{
			$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
			$SQL .= " TELEFON AS TELNR, EMAIL,";
			$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
			$SQL .= " FROM BESCHWERDE@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (SACHBEARBEITER=ID)";
			
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
		}
		else
		{
			$Bedingung='';
			$Zusatz=FALSE;
			
			/*SQL-Zusatz Bearbeitungsnummer*/
			if($Params[1]!='')
			{
					$Bedingung='BEARBEITUNGSNR ' . awisLIKEoderIST($Params[1],TRUE,FALSE,FALSE,0);
					$Zusatz=TRUE;
			}
			
			/*SQL-Zusatz Filial-Nummer*/
			if($Zusatz)
			{
					if($Params[2]!='')
					{
							$Bedingung.=' AND FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[2]!='')
					{
							$Bedingung='FILNR ' . awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Kundenname*/
			if($Zusatz)
			{
					if($Params[3]!='')
					{
							$Bedingung.=' AND (UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[3]!='')
					{
							$Bedingung='(UPPER(KUNDENNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0) . ' OR UPPER(VORNAME) ' . awisLIKEoderIST($Params[3],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Strasse*/
			if($Zusatz)
			{
					if($Params[4]!='')
					{
							$Bedingung.=' AND UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[4]!='')
					{
							$Bedingung='UPPER(STRASSE) ' . awisLIKEoderIST($Params[4],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PLZ*/
			if($Zusatz)
			{
					if($Params[5]!='')
					{
							$Bedingung.=' AND PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[5]!='')
					{
							$Bedingung='PLZ ' . awisLIKEoderIST($Params[5],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Ort*/
			if($Zusatz)
			{
					if($Params[6]!='')
					{
							$Bedingung.=' AND UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[6]!='')
					{
							$Bedingung='UPPER(ORT) ' . awisLIKEoderIST($Params[6],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Telefon*/
			if($Zusatz)
			{
					if($Params[7]!='')
					{
							$Bedingung.=' AND TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[7]!='')
					{
							$Bedingung='TELEFON ' . awisLIKEoderIST($Params[7],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz E-Mail*/
			if($Zusatz)
			{
					if($Params[8]!='')
					{
							$Bedingung.=' AND UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[8]!='')
					{
							$Bedingung='UPPER(EMAIL) ' . awisLIKEoderIST($Params[8],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz PAM*/
			if($Zusatz)
			{
					if($Params[9]!='')
					{
							$Bedingung.=' AND PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[9]!='')
					{
							$Bedingung='PAN ' . awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0);
							$Zusatz=TRUE;
					}
			}
			
			/*SQL-Zusatz Sachbearbeiter*/
			if($Zusatz)
			{
					if($Params[10]!='')
					{
							$Bedingung.=' AND (UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
			else
			{
					if($Params[10]!='')
					{
							$Bedingung='(UPPER(SBNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0) . ' OR UPPER(SBVORNAME) ' . awisLIKEoderIST($Params[10],TRUE,FALSE,FALSE,0).')';
							$Zusatz=TRUE;
					}
			}
		}
		
		
		if($Bedingung!='')
		{
			if ($Params[0]!='A')
			{
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
			}
			else 
			{
				$SQL = "SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
				$SQL .= " DECODE(NVL(TELEFON2,'-1'),'-1',TELEFON,TELEFON||' ; '||TELEFON2) AS TELNR, EMAIL,";
				$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
				$SQL .= " FROM SCHAEDEN_NEW@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (BEARBEITER=ID)";
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
				$SQL .= ' UNION';
				$SQL .= " SELECT ART, BEARBEITUNGSNR, FILNR, KUNDENNAME ||' '||VORNAME AS NAME, STRASSE, PLZ, ORT,";
				$SQL .= " TELEFON AS TELNR, EMAIL,";
				$SQL .= " PAN, SBVORNAME ||' '|| SBNAME AS SACHBEARBEITER";
				$SQL .= " FROM BESCHWERDE@SCHAD.ATU.DE LEFT JOIN SACHBEARBEITER@SCHAD.ATU.DE ON (SACHBEARBEITER=ID)";
				$SQL .= ' WHERE ' . $Bedingung . ' AND ROWNUM<=' . $AwisBenuPara;
			}
		}
		else
		{
				//$SQL .= ' WHERE ROWNUM<=' . $AwisBenuPara;
				echo "<br>";
				die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		
		$SQL .= ' ORDER BY FILNR, BEARBEITUNGSNR';
		
		awis_Debug(1,$SQL);
		
		$rsResult = awisOpenRecordset($con,$SQL);
		$rsResultZeilen = $awisRSZeilen;
		
		if($rsResultZeilen==0)		// Keine Daten
		{
			//echo 'Es wurde kein Eintrag gefunden.';
			echo "<br>";
			die("<center><span class=HinweisText>F&uuml;r die angegebenen Suchparameter wurde kein Ergebniss gefunden!</span></center>");
		}
		else
		{
			// �berschrift aufbauen
			echo "<table  width=100% id=DatenTabelle border=1><tr>";
		
			if(($Rechtestufe&16)==16)
			{
				echo "<td id=FeldBez></td>";
			}
			echo "<td id=FeldBez>Auftragsart</td>";
			echo "<td id=FeldBez>Bearbeitungs-Nr</td>";
			echo "<td id=FeldBez>Filial-Nr</td>";
			echo "<td id=FeldBez>Kunde</td>";
			echo "<td id=FeldBez>Strasse</td>";
			echo "<td id=FeldBez>PLZ</td>";
			echo "<td id=FeldBez>Ort</td>";
			echo "<td id=FeldBez>Telefon</td>";
			echo "<td id=FeldBez>E-Mail</td>";
			echo "<td id=FeldBez>ATU-Card-Nummer</td>";
			echo "<td id=FeldBez>Sachbearbeiter</td>";
			echo "</tr>";
			
			for($i=0;$i<$rsResultZeilen;$i++)
			{
				echo '<tr>';
				if(($Rechtestufe&16)==16 && $rsResult['ART'][$i]=='S')
				{
					echo "<td id=TabellenZeileGrau><a href=./schaden_pdf.php?BEARB_NR=" . $rsResult['BEARBEITUNGSNR'][$i] . "><img border=0 src=/bilder/pdf_gross.png title='PDF erzeugen'></a>";
				}else {
					echo "<td></td>";
				}
				echo '<td id=TabellenZeileGrau>' . ($rsResult['ART'][$i]=='S'?'Schaden':'Beschwerde') . '</td>';
				echo '<td id=TabellenZeileGrau><a href=./schaden_Main.php?cmdAktion=Liste&BEARB_ID=' . $rsResult['BEARBEITUNGSNR'][$i] . '&ART=' . $rsResult['ART'][$i] . '>' . $rsResult['BEARBEITUNGSNR'][$i] . '</a></td>';
				echo '<td id=TabellenZeileGrau><a href=../filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID=' . $rsResult['FILNR'][$i] . '>' . $rsResult['FILNR'][$i] .  '</a></td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['NAME'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['STRASSE'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['PLZ'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['ORT'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['TELNR'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['EMAIL'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['PAN'][$i] . '</td>';
				echo '<td id=TabellenZeileGrau>' . $rsResult['SACHBEARBEITER'][$i] . '</td>';
				echo '</tr>';
			}
				
			print "</table>";
			
			if($i<$AwisBenuPara)
			{
				echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden!';
			}
			else
			{
			echo '<font size=2>Es wurden ' . $i . ' Datens�tze in ' . awis_ZeitMessung(1) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datens�tze eingeschr�nkt!';
			}
		}
}

function Pruefe_Felder()
{
	global $Pflicht_Felder;
	//$Pflicht_Felder=array();
	
	$Fehler_Anzahl=0;

	if (! isset($_REQUEST['txtErg_Text']) || ! $_REQUEST['txtErg_Text']!='')
	{
		echo "<center><span class=HinweisText>Es wurde kein Diagnose/Ergebnis-Text angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[0]=$_REQUEST['txtErg_Text'];
	}
	
	if (! isset($_REQUEST['txtKennung']) || ! $_REQUEST['txtKennung']!='0')
	{
		echo "<center><span class=HinweisText>Es wurde keine Kennung angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[1]=$_REQUEST['txtKennung'];
	}
	
	if (! isset($_REQUEST['txtBearbStand']) || $_REQUEST['txtBearbStand']=='0' || $_REQUEST['txtBearbStand']=='22')
	{
		echo "<center><span class=HinweisText>Es wurde kein Bearbeitungsstand angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[2]=$_REQUEST['txtBearbStand'];
	}
	
	if (! isset($_REQUEST['txtKontaktKunde']) || ! $_REQUEST['txtKontaktKunde']!='0')
	{
		echo "<center><span class=HinweisText>Es wurde kein Wert beim Feld Kontakt mit Kunde angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[3]=$_REQUEST['txtKontaktKunde'];
	}
	
	if (! isset($_REQUEST['txtInfoGBL']) || ! $_REQUEST['txtInfoGBL']!='0')
	{
		echo "<center><span class=HinweisText>Es wurde kein Wert beim Feld Info an GBL angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[4]=$_REQUEST['txtInfoGBL'];
	}
	
	if (! isset($_REQUEST['txtEingabedurch']) || ! $_REQUEST['txtEingabedurch']!='')
	{
		echo "<center><span class=HinweisText>Es wurde kein Wert beim Feld Eingabe / Bearbeitet durch angegeben!</span></center><br>";
		$Fehler_Anzahl++;
	}
	else{
		$Pflicht_Felder[5]=$_REQUEST['txtEingabedurch'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '38') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!=''))
	{
		echo "<center><span class=HinweisText>Es muss noch eine Ausfallursache (Bremse) angegeben werden!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[6]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '51') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!=''))
	{
		echo "<center><span class=HinweisText>Es muss noch eine Ausfallursache (Motor) angegeben werden!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[7]=$_REQUEST['txtWoranGearbeitet'];
	}

	if (($_REQUEST['txtWoranGearbeitet'] == '53') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!=''))
	{
		echo "<center><span class=HinweisText>Es muss noch eine Ausfallursache (R�der/Reifen) angegeben werden!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[8]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '59') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!=''))
	{
		echo "<center><span class=HinweisText>Es muss noch eine Ausfallursache (Zahnriemen) angegeben werden!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[9]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if (($_REQUEST['txtWoranGearbeitet'] == '12') AND (! isset($_REQUEST['txtAusfallursache']) || ! $_REQUEST['txtAusfallursache']!=''))
	{
		echo "<center><span class=HinweisText>Es muss noch eine Ausfallursache (�lwechsel) angegeben werden!</span></center><br>";
		$Fehler_Anzahl++;
		$Pflicht_Felder[10]=$_REQUEST['txtWoranGearbeitet'];
	}
	
	if ((! isset($_REQUEST['txtZahlungArt']) || ! $_REQUEST['txtZahlungArt']!='') and $_REQUEST['txtZahlungWert']!='')
	{
		echo "<center><span class=HinweisText>Es wurde kein Wert beim Feld Zahlungsart angegeben!</span></center><br>";
		$Fehler_Anzahl++;						
		$Pflicht_Felder[12]=$_REQUEST['txtZahlungWert'];		
	}
	else{
		$Pflicht_Felder[11]=$_REQUEST['txtZahlungArt'];
		$Pflicht_Felder[12]=$_REQUEST['txtZahlungWert'];		
	}		 
	
	
	$Pflicht_Felder[13]=$_REQUEST['txtTerminKunde1'];
	$Pflicht_Felder[14]=$_REQUEST['txtTerminKundeWer1'];
	$Pflicht_Felder[15]=$_REQUEST['txtTerminKunde2'];
	$Pflicht_Felder[16]=$_REQUEST['txtTerminKundeWer2'];
	$Pflicht_Felder[17]=$_REQUEST['txtUrsprAuftrag'];
	$Pflicht_Felder[18]=$_REQUEST['txtWoranGearbeitet'];
	$Pflicht_Felder[19]=$_REQUEST['txtWasBeschaedigt'];
	
	$Pflicht_Felder[20]=$_REQUEST['txtEingang'];
	$Pflicht_Felder[21]=$_REQUEST['txtKFZ_Kennz'];
	$Pflicht_Felder[22]=$_REQUEST['txtWANR'];
	$Pflicht_Felder[23]=$_REQUEST['txtSachbearbeiter'];
	$Pflicht_Felder[24]=$_REQUEST['txtAnrede'];
	$Pflicht_Felder[25]=$_REQUEST['txtKundenname'];
	$Pflicht_Felder[26]=$_REQUEST['txtKundenvorname'];
	$Pflicht_Felder[27]=$_REQUEST['txtStrasse'];
	$Pflicht_Felder[28]=$_REQUEST['txtPLZ'];
	$Pflicht_Felder[29]=$_REQUEST['txtOrt'];
	$Pflicht_Felder[30]=$_REQUEST['txtTelefon'];
	$Pflicht_Felder[31]=$_REQUEST['txtTelefon2'];
	$Pflicht_Felder[32]=$_REQUEST['txtFax'];
	$Pflicht_Felder[33]=$_REQUEST['txtEMail'];
	$Pflicht_Felder[34]=$_REQUEST['txtPAN'];
		
		
	if ($Fehler_Anzahl==0)
	{
		$retval=true;
	}else{
		$retval=false;
	}
	
	return $retval;
}

?>
</body>
</html>

