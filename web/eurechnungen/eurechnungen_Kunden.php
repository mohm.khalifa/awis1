<?php
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Fehler;
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('ERE', '%');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Liste', 'lst_OffenMass');
    $TextKonserven[] = array('Wort', 'PDFErzeugen');
    $TextKonserven[] = array('Wort', 'DatumVom');
    $TextKonserven[] = array('Wort', 'Datum');
    $TextKonserven[] = array('FIL', 'FIL_GEBIET');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');

    $AWIS_KEY1 = '';
    $AWISBenutzer = awisBenutzer::Init();
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_EURechnungen'));
    $Form = new awisFormular();
    $Form->Formular_Start();

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht47000 = $AWISBenutzer->HatDasRecht(47000);
    if ($Recht47000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Form->SchreibeHTMLCode('<form name="frmEKU" action="./eurechnungen_Main.php?cmdAktion=Kunden" method="POST" enctype="multipart/form-data">');
    $Block = 1;
    if (!isset($Param['ORDER']) or $Param['ORDER'] == '') {
        $Param['ORDER'] = 'EKU_fil_id';
    }
    $Seite = 'Suche';
    if (isset($_GET['Sort']) and $_GET['Sort'] != '') {        // wenn GET-Sort, dann nach diesen Feld sortieren
        $Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    }

    //Ein einzelner Datensatz
    if (isset($_GET['EKU_KEY'])) {
        $AWIS_KEY1 = $_GET['EKU_KEY'];
        $Seite = 'Details';
    } elseif (isset($_POST['txtEKU_KEY'])) {
        $AWIS_KEY1 = $_POST['txtEKU_KEY'];
        $Seite = 'Details';
    }

    if (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
        $Seite = 'Details';
    }

    if (isset($_GET['Sort'])) {
        $AWIS_KEY1 = -1;
        $Seite = 'Details';
    }

    if (isset($_POST['cmdSpeichern_x'])) {
        include('./eurechnungen_Kunden_speichern.php');
    }

    if ($Recht47000 == 0) {
        $Form->Fehler_KeineRechte();
    }

    if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen
        $Param['EKU_NAME'] = $Form->Format('T', $_POST['sucEKU_NAME'], true);
        $Param['EKU_USTIDNR'] = $Form->Format('T', $_POST['sucEKU_USTIDNR'], true);
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';

        $Seite = 'Details';
    }

    if (isset($_REQUEST['Block'])) { //Gebl�ttert oder "Zur�ck zur Liste"
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        $Seite = 'Details';
    }

    if ($Seite == 'Details') { //Detailseite anzeigen

        $Form->Erstelle_HiddenFeld('EKU_KEY', $AWIS_KEY1);

        //Grund Select
        $SQL = 'SELECT EKU_KEY,';
        $SQL .= '   EKU_USTIDNR,';
        $SQL .= '   EKU_ANREDE,';
        $SQL .= '   EKU_NAME,';
        $SQL .= '   EKU_STRASSE,';
        $SQL .= '   EKU_LAN_NR,';
        $SQL .= '   EKU_PLZ,';
        $SQL .= '   EKU_ORT,';
        $SQL .= '   EKU_GUELTIG,';
        $SQL .= '   EKU_PRUEFDATUM,';
        $SQL .= '   EKU_USER,';
        $SQL .= '   EKU_USERDAT';
        $SQL .= ' FROM EUKUNDEN ';
        $Bedingung = '';
        $Bedingung .= _BedingungErstellen($Param);       // mit dem Rest

        if ($Bedingung != '') {
            $SQL .= ' WHERE ' . substr($Bedingung, 4);
        }
        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('EKU', false));

        $rsEKU = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('EKU', true));

        if ($AWIS_KEY1 == '' and $rsEKU->AnzahlDatensaetze() == 1) {
            $AWIS_KEY1 = $rsEKU->FeldInhalt('EKU_KEY');
        }

        if (($rsEKU->AnzahlDatensaetze() > 1) or ($rsEKU->AnzahlDatensaetze() == 1 and (isset($_REQUEST['Block']) or isset($_REQUEST['Sort'])))) { //Liste anzeigen
            $Seite = 'Liste';
            $Felder = array();
            $Felder[] = array(
                'Style' => 'font-size:smaller;',
                'Inhalt' => '<a href="./eurechnungen_Main.php?cmdAktion=Kunden" accesskey="S" title=' . '><img border=0 src=/bilder/cmd_trefferliste.png></a>'
            );
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => '');
            $Form->InfoZeile($Felder, '');

            $FeldBreiten['ICONS'] = 40;
            $FeldBreiten['EKU_NAME'] = 250;
            $FeldBreiten['EKU_USTIDNR'] = 150;
            $FeldBreiten['EKU_PLZ'] = 60;
            $FeldBreiten['EKU_ORT'] = 150;
            $FeldBreiten['EKU_PRUEFDATUM'] = 80;

            $Form->ZeileStart();

            $Link = './eurechnungen_Main.php?cmdAktion=Kunden&Sort=EKU_NAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'EKU_NAME'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ERE']['EKU_NAME'], $FeldBreiten['EKU_NAME'], '', $Link);
            $Link = './eurechnungen_Main.php?cmdAktion=Kunden&Sort=EKU_USTIDNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'EKU_USTIDNR'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ERE']['EKU_USTIDNR'], $FeldBreiten['EKU_USTIDNR'], '', $Link);
            $Link = './eurechnungen_Main.php?cmdAktion=Kunden&Sort=EKU_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'EKU_PLZ'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ERE']['EKU_PLZ'], $FeldBreiten['EKU_PLZ'], '', $Link);
            $Link = './eurechnungen_Main.php?cmdAktion=Kunden&Sort=EKU_ORT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'EKU_ORT'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ERE']['EKU_ORT'], $FeldBreiten['EKU_ORT'], '', $Link);
            $Link = './eurechnungen_Main.php?cmdAktion=Kunden&Sort=EKU_PRUEFDATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'EKU_PRUEFDATUM'))?'~':'') . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ERE']['EKU_PRUEFDATUM'], $FeldBreiten['EKU_PRUEFDATUM'], '', $Link);
            $Form->ZeileEnde();
            $DS = 0;
            while (!$rsEKU->EOF()) {
                $HG = ($DS % 2);

                $Edit = './eurechnungen_Main.php?cmdAktion=Kunden&DEL&EKU_KEY=' . $rsEKU->FeldInhalt('EKU_KEY');

                $Form->ZeileStart();

                $Form->Erstelle_ListenFeld('EKU_NAME', $rsEKU->FeldInhalt('EKU_NAME'), $FeldBreiten['EKU_NAME'] / 10, $FeldBreiten['EKU_NAME'], false, $HG, '', $Edit);
                $Form->Erstelle_ListenFeld('EKU_USTIDNR', $rsEKU->FeldInhalt('EKU_USTIDNR'), $FeldBreiten['EKU_USTIDNR'] / 10, $FeldBreiten['EKU_USTIDNR'], false, $HG);
                $Form->Erstelle_ListenFeld('EKU_PLZ', $rsEKU->FeldInhalt('EKU_PLZ'), $FeldBreiten['EKU_PLZ'] / 10, $FeldBreiten['EKU_PLZ'], false, $HG);
                $Form->Erstelle_ListenFeld('EKU_ORT', $rsEKU->FeldInhalt('EKU_ORT'), $FeldBreiten['EKU_ORT'] / 10, $FeldBreiten['EKU_ORT'], false, $HG);
                $Form->Erstelle_ListenFeld('EKU_PRUEFDATUM', $rsEKU->FeldInhalt('EKU_PRUEFDATUM', 'D'), $FeldBreiten['EKU_PRUEFDATUM'] / 10, $FeldBreiten['EKU_PRUEFDATUM'], false,
                    $HG);

                $Form->ZeileEnde();
                $rsEKU->DSWeiter();
            }
            $Link = './eurechnungen_Main.php?cmdAktion=Kunden';
            $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
        } elseif ($AWIS_KEY1 != '') { //Neuer oder bestehender Kunde
            $Seite = 'Details';
            $Felder = array();
            if($AWIS_KEY1!=-1){
                $Form->Erstelle_HiddenFeld('EKU_KEY', $rsEKU->FeldInhalt('EKU_KEY'));
            }

            $Felder[] = array(
                'Style' => 'font-size:smaller;',
                'Inhalt' => '<a href="' . './eurechnungen_Main.php?cmdAktion=Kunden&Block=1' . '" accesskey="S" title=' . '><img border=0 src=/bilder/cmd_trefferliste.png></a>'
            );
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => '');
            $Form->InfoZeile($Felder, '');

            $aendern = true;

            $FeldBreiten['Labels'] = 200;
            $FeldBreiten['Werte'] = 200;
            $FeldBreiten['EKU_PLZ'] = 60;
            $FeldBreiten['EKU_ORT_LABEL'] = 30;
            $FeldBreiten['EKU_ORT'] = $FeldBreiten['Werte'] - $FeldBreiten['EKU_ORT_LABEL'] - $FeldBreiten['EKU_PLZ'] - $FeldBreiten['EKU_ORT_LABEL'] + 3;
            $FeldBreiten['EKU_PRUEFDATUM'] = 100;

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_USTIDNR'] . ':', $FeldBreiten['Labels']);
            $Form->Erstelle_TextFeld('!EKU_USTIDNR', isset($_POST['txtEKU_USTIDNR'])?$_POST['txtEKU_USTIDNR']:$rsEKU->FeldInhalt('EKU_USTIDNR'), $FeldBreiten['Werte'] / 10,
                $FeldBreiten['Werte'], $aendern, '', '', '', 'T');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_ANREDE'] . ':', $FeldBreiten['Labels']);
            $SQL = 'Select anr_id, anr_anrede from anreden';
            $Form->Erstelle_SelectFeld('!EKU_ANREDE', isset($_POST['txtEKU_ANREDE'])?$_POST['txtEKU_ANREDE']:$rsEKU->FeldInhalt('EKU_ANREDE'),
                $FeldBreiten['Werte'] . ':' . ($FeldBreiten['Werte'] - 25), $aendern, $SQL, '', 3);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_NAME'] . ':', $FeldBreiten['Labels']);
            $Form->Erstelle_TextFeld('!EKU_NAME', isset($_POST['txtEKU_NAME'])?$_POST['txtEKU_NAME']:$rsEKU->FeldInhalt('EKU_NAME'), $FeldBreiten['Werte'] / 10,
                $FeldBreiten['Werte'], $aendern, '', '', '', 'T');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_STRASSE'] . ':', $FeldBreiten['Labels']);
            $Form->Erstelle_TextFeld('!EKU_STRASSE', isset($_POST['txtEKU_STRASSE'])?$_POST['txtEKU_STRASSE']:$rsEKU->FeldInhalt('EKU_STRASSE'), $FeldBreiten['Labels'] / 10,
                $FeldBreiten['Labels'], $aendern, '', '', '', 'T');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_LAN_NR'] . ':', $FeldBreiten['Labels']);
            $SQL = 'select lan_nr, lan_land from LAENDER';
            $Form->Erstelle_SelectFeld('!EKU_LAN_NR', isset($_POST['txtEKU_LAN_NR'])?$_POST['txtEKU_LAN_NR']:$rsEKU->FeldInhalt('EKU_LAN_NR'),
                $FeldBreiten['Werte'] . ':' . ($FeldBreiten['Werte'] - 25), $aendern, $SQL, '', 276);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_PLZ'] . ':', $FeldBreiten['Labels']);
            $Form->Erstelle_TextFeld('!EKU_PLZ', isset($_POST['txtEKU_PLZ'])?$_POST['txtEKU_PLZ']:$rsEKU->FeldInhalt('EKU_PLZ'), $FeldBreiten['EKU_PLZ'] / 10,
                $FeldBreiten['EKU_PLZ'], $aendern, '', '', '', 'T');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_ORT'] . ':', $FeldBreiten['EKU_ORT_LABEL']);
            $Form->Erstelle_TextFeld('!EKU_ORT', isset($_POST['txtEKU_ORT'])?$_POST['txtEKU_ORT']:$rsEKU->FeldInhalt('EKU_ORT'), $FeldBreiten['EKU_ORT'] / 10,
                $FeldBreiten['EKU_ORT'], $aendern, '', '', '', 'T');
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_PRUEFDATUM'] . ':', $FeldBreiten['Labels']);
            $Form->Erstelle_TextFeld('!EKU_PRUEFDATUM', isset($_POST['txtEKU_PRUEFDATUM'])?$_POST['txtEKU_PRUEFDATUM']:$rsEKU->FeldInhalt('EKU_PRUEFDATUM'), 8,
                $FeldBreiten['EKU_PRUEFDATUM'] + 20, $aendern, '', '', '', 'D', 'L', '', date('d.m.Y'));
            if (isset($_POST['txtEKU_GUELTIG'])) {
                $Wert = 1;
            } elseif ($rsEKU->FeldInhalt('EKU_GUELTIG') != '') {
                $Wert = $rsEKU->FeldInhalt('EKU_GUELTIG');
            } else { //Default
                $Wert = 1;
            }
            $Form->Erstelle_Checkbox('EKU_GUELTIG', $Wert, 15, $aendern, 1);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_GUELTIG'], $FeldBreiten['EKU_ORT_LABEL']);

            $Form->ZeileEnde();
        } else {
            $Seite = 'Fehler';
            $Form->ZeileStart();
            $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
            $Form->ZeileEnde();
        }
    } elseif ($Seite == 'Suche')//Suchmaske
    {
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['ERE_SUCHE'], 300, 'font-weight: bold');
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_NAME'] . ':', 200);
        $Form->Erstelle_TextFeld('*EKU_NAME', (@$Param['SPEICHERN'] == 'on'?$Param['EKU_NAME']:''), 15, 150, true, '', '', '', 'T', 'L', '', '', 50);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ERE']['EKU_USTIDNR'] . ':', 200);
        $Form->Erstelle_TextFeld('*EKU_USTIDNR', (@$Param['SPEICHERN'] == 'on'?$Param['EKU_USTIDNR']:''), 15, 150, true, '', '', '', 'T', 'L', '', '', 50);
        $Form->ZeileEnde();

        // Auswahl kann gespeichert werden
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
        $Form->Erstelle_Checkbox('*AuswahlSpeichern', (@$Param['SPEICHERN'] == 'on'?'on':''), 20, true, 'on');
        $Form->ZeileEnde();
    }

    $AWISBenutzer->ParameterSchreiben('Formular_EURechnungen', serialize($Param));
    $Form->Formular_Ende();
    //************************************************************
    //* Schaltfl�chen
    //************************************************************
    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�

    $ZurueckLink = './eurechnungen_Main.php?cmdAktion=Kunden';
    if ($Seite == 'Suche') {
        $ZurueckLink = '/index.php';
    }

    $Form->Schaltflaeche('href', 'cmd_zurueck', $ZurueckLink, '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    if ($Seite == 'Suche') {
        $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    }
    if ($Seite == 'Liste' or $Seite == 'Suche') {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    if ($Seite == 'Details') {

        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');
    $Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {

    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {

        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201211161605");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

function _BedingungErstellen($Param)
{

    global $DB;
    global $AWIS_KEY1;

    $Bedingung = '';

    if ($AWIS_KEY1 != '') {
        $Bedingung .= ' and EKU_KEY= ' . $DB->WertSetzen('EKU', 'N0', $AWIS_KEY1);
    }

    if (isset($Param['EKU_NAME']) AND $Param['EKU_NAME'] != '')
    {
        $Bedingung .= ' AND upper(EKU_NAME) like ' . $DB->WertSetzen('EKU', 'T', mb_strtoupper('%' . $Param['EKU_NAME'] . '%'));
    }


    if (isset($Param['EKU_USTIDNR']) AND $Param['EKU_USTIDNR'] != '')
    {
        $Bedingung .= ' AND upper(EKU_USTIDNR) like ' . $DB->WertSetzen('EKU', 'T', mb_strtoupper('%' . $Param['EKU_USTIDNR'] . '%'));
    }



    return $Bedingung;
}

?>