<?php
global $AWISCursorPosition;		// Zum Cursor setzen
//global $AWIS_KEY1;
//global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KundenKfzSuche','%');
	$TextKonserven[]=array('KundenKfzZuordnungen','KKZ_LIEFERANT');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Werkzeuge = new awisWerkzeuge();

	$RemoteDB='';					
	$RemoteConn='';
	
	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
	{
		$RemoteDB='@COM_DE.ATU.DE';
		$RemoteConn='COM_DE';
	}
	else 
	{
        $RemoteDB='@COM_TEST_DE.ATU.DE';
        $RemoteConn='COM_TEST_DE';
	}	
	
	$DBCom = awisDatenbank::NeueVerbindung($RemoteConn);
	$DBCom->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht13001 = $AWISBenutzer->HatDasRecht(13001);
	if($Recht13001==0)
	{
	    awisEreignis(3,1000,'KundenKfzInfos',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;

	//********************************************************
	// Speichern
	//********************************************************
	if (isset($_POST['cmd_speichern_x']))
	{
		include('./kundenkfzsuche_Kundeninfos_speichern.php');
	}	
	
	//********************************************************
	// Parameter ?
	//********************************************************

	if(isset($_GET['VERTRAGSNUMMER']) AND $_GET['VERTRAGSNUMMER']!=='')
	{
		if ($_GET['VERTRAGSNUMMER']==0)
		{
			$AddDS=true;
			$EditDS=false;
		}
		else{			
			$Param['VERTRAGSNUMMER']=$_GET['VERTRAGSNUMMER'];		
			$EditDS=true;
			$AddDS=false;
		}		
	}
	else 
	{
		if(isset($Param['VERTRAGSNUMMER']))
		{
			$Param['WHERE']='';	
			$Param['VERTRAGSNUMMER']='';
		}
		$EditDS=false;
		$AddDS=false;
	}
	
	if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
	{
		$Param['KEY']=0;		
	}

	//$AWIS_KEY1=$Param['KEY'];

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if(isset($Param['ORDER']) and  $Param['ORDER'] !='')
		{
			$ORDERBY = ' ORDER BY '.$Param['ORDER'].', KKZ_LIEFERANT ASC NULLS FIRST, VERTRAGSNUMMER ASC';
		}
		else
		{
			$ORDERBY = ' ORDER BY FAELLIG ASC, KKZ_LIEFERANT ASC NULLS FIRST, VERTRAGSNUMMER ASC';
			$Param['ORDER']='FAELLIG ASC, KKZ_LIEFERANT ASC NULLS FIRST, VERTRAGSNUMMER ASC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'].', KKZ_LIEFERANT ASC NULLS FIRST, VERTRAGSNUMMER ASC';
	}
	
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	$SQL="SELECT K.*, ROW_NUMBER() OVER (ORDER BY K.FAELLIG, K.VERTRAGSNUMMER) AS ZEILENNR FROM ( ";
	$SQL.="SELECT * FROM EXPERIAN_ATU.V_EXP_KUNDEN_KFZ_INFO) K ".$Bedingung;
	
/*	var_dump($SQL);
	var_dump($Param);
	var_dump($_GET);
	var_dump($_POST);
	var_dump($EditDS);*/
	
//	if($AWIS_KEY1<=0)
//	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DBCom->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
//	}

	$SQL .= $ORDERBY;

	$Form->DebugAusgabe(1,$Param,$_GET,$_POST);
	$Form->DebugAusgabe(1,$SQL);
		
	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsKFZInfo = $DBCom->RecordsetOeffnen($SQL);

	
	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmKundenKfzInfos action=./kundenkfzsuche_Main.php?cmdAktion=Kundeninfos method=POST>';

	if($rsKFZInfo->EOF() AND $AddDS==false)		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsKFZInfo->AnzahlDatensaetze()>=1 AND $EditDS==false AND $AddDS==false)						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();
	
		$Form->ZeileStart();
			$Form->Erstelle_ListenBild('href','cmd_refresh','./kundenkfzsuche_Main.php?cmdAktion=Kundeninfos','/bilder/icon_table_refresh.png','Refresh Table',0,'width: 100%; background-color:transparent;',20,20,'','R');
		$Form->ZeileEnde();
		
		$Form->ZeileStart('font-size:11pt');

		$Icons = array();
		$Icons[] = array('new','./kundenkfzsuche_Main.php?cmdAktion=Kundeninfos&VERTRAGSNUMMER=0');

		$Form->Erstelle_ListeIcons($Icons,20,-1);
		$Link = './kundenkfzsuche_Main.php?cmdAktion=Kundeninfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=VERTRAGSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VERTRAGSNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['VERTRAGSNUMMER'],150,'',$Link);				
		$Link = './kundenkfzsuche_Main.php?cmdAktion=Kundeninfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KKZ_LIEFERANT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KKZ_LIEFERANT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzZuordnungen']['KKZ_LIEFERANT'],100,'',$Link);				
		$Link = './kundenkfzsuche_Main.php?cmdAktion=Kundeninfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');		
		$Link .= '&Sort=LAST_CHANGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_CHANGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['LAST_CHANGE'],150,'',$Link);
		$Link = './kundenkfzsuche_Main.php?cmdAktion=Kundeninfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');		
		$Link .= '&Sort=LAST_IMPORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LAST_IMPORT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['LAST_IMPORT'],150,'',$Link);
		$Link = './kundenkfzsuche_Main.php?cmdAktion=Kundeninfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=NEXT_IMPORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='NEXT_IMPORT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['NEXT_IMPORT'],150,'',$Link);
		$Link = './kundenkfzsuche_Main.php?cmdAktion=Kundeninfos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=FAELLIG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FAELLIG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['FAELLIG'],50,'',$Link);				
			
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsKFZInfo->EOF())
		{
			//$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
			$Form->ZeileStart('font-size:11pt');		

			if($rsKFZInfo->FeldInhalt('FAELLIG')=='Ja')
			{
				$style='font-weight: bold; text-decoration: blink; color: red;';
			}
			else{
				$style='';
			}

			$Icons = array();
			$Icons[] = array('edit','./kundenkfzsuche_Main.php?cmdAktion=Kundeninfos&VERTRAGSNUMMER='.$rsKFZInfo->FeldInhalt('VERTRAGSNUMMER'));

			$Form->Erstelle_ListeIcons($Icons,20,($DS%2));		
			$Form->Erstelle_ListenFeld('VERTRAGSNUMMER',$rsKFZInfo->FeldInhalt('VERTRAGSNUMMER'),12,150,false,($DS%2),'','','Z');			
			$Form->Erstelle_ListenFeld('KKZ_LIEFERANT',$rsKFZInfo->FeldInhalt('KKZ_LIEFERANT'),12,100,false,($DS%2),'','','T');			
			$Form->Erstelle_ListenFeld('LAST_CHANGE',$rsKFZInfo->FeldInhalt('LAST_CHANGE'),12,150,false,($DS%2),'','','D');			
			$Form->Erstelle_ListenFeld('LAST_IMPORT',$rsKFZInfo->FeldInhalt('LAST_IMPORT'),12,150,false,($DS%2),'','','D');			
			$Form->Erstelle_ListenFeld('NEXT_IMPORT',$rsKFZInfo->FeldInhalt('NEXT_IMPORT'),12,150,false,($DS%2),'','','D');			
			$Form->Erstelle_ListenFeld('FAELLIG',$rsKFZInfo->FeldInhalt('FAELLIG'),12,50,false,($DS%2),$style,'','T');			
			
					
			$Form->ZeileEnde();

			$rsKFZInfo->DSWeiter();
			$DS++;
		}

		$Link = './kundenkfzsuche_Main.php?cmdAktion=Kundeninfos';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}elseif($rsKFZInfo->AnzahlDatensaetze()>=1 AND $EditDS==true AND $AddDS==false AND ($Recht13001&2)!=0)
	{
		$Form->Formular_Start();
		
		$Form->ZeileStart();
			$Form->Erstelle_ListenBild('href','cmd_refresh','./kundenkfzsuche_Main.php?cmdAktion=Kundeninfos','/bilder/icon_table_refresh.png','Refresh Table',0,'width: 100%; background-color:transparent;',20,20,'','R');
		$Form->ZeileEnde();
		
		$Form->ZeileStart('font-size:11pt');

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['VERTRAGSNUMMER'],150,'','');				
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['DELTATYP'],150,'','');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['IMPORTZYKLUS_TAGE'],150,'','');				
			
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsKFZInfo->EOF())
		{
			$Form->ZeileStart('font-size:11pt');		

			$Form->Erstelle_ListenFeld('VERTRAGSNUMMER',$rsKFZInfo->FeldInhalt('VERTRAGSNUMMER'),12,150,true,($DS%2),'','','Z');			
			$Form->Erstelle_ListenFeld('DELTATYP',$rsKFZInfo->FeldInhalt('DELTATYP'),12,150,true,($DS%2),'','','Z');			
			$Form->Erstelle_ListenFeld('IMPORTZYKLUS_TAGE',$rsKFZInfo->FeldInhalt('IMPORTZYKLUS_TAGE'),12,150,true,($DS%2),'','','Z');			
			
			$Form->Erstelle_HiddenFeld('EditDS',$EditDS);
			
			$Form->SchaltflaechenStart('background-color:transparent;');

			$Form->Schaltflaeche('image','cmd_speichern','','/bilder/icon_save.png',$AWISSprachKonserven['Wort']['lbl_hinzufuegen'],'','',20,20);
		
			$Form->SchaltflaechenEnde();
			
			$Form->ZeileEnde();

			$rsKFZInfo->DSWeiter();
			$DS++;
		}
		
		$Form->Formular_Ende();
	}elseif($AddDS==true AND ($Recht13001&4)!=0)
	{
		$Form->Formular_Start();
		
		$Form->ZeileStart();
			$Form->Erstelle_ListenBild('href','cmd_refresh','./kundenkfzsuche_Main.php?cmdAktion=Kundeninfos','/bilder/icon_table_refresh.png','Refresh Table',0,'width: 100%; background-color:transparent;',20,20,'','R');
		$Form->ZeileEnde();
		
		$Form->ZeileStart('font-size:11pt');

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['VERTRAGSNUMMER'],150,'','');				
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['DELTATYP'],150,'','');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KundenKfzSuche']['IMPORTZYKLUS_TAGE'],150,'','');				
			
		$Form->ZeileEnde();

			$Form->ZeileStart('font-size:11pt');		

			$Form->Erstelle_ListenFeld('VERTRAGSNUMMER','',12,150,true,'','','','Z');			
			$Form->Erstelle_ListenFeld('DELTATYP','0',12,150,true,'','','','Z');			
			$Form->Erstelle_ListenFeld('IMPORTZYKLUS_TAGE','',12,150,true,'','','','Z');			
		
			$Form->Erstelle_HiddenFeld('AddDS',$AddDS);			
			
			$Form->SchaltflaechenStart('background-color:transparent;');

			$Form->Schaltflaeche('image','cmd_speichern','','/bilder/icon_save.png',$AWISSprachKonserven['Wort']['lbl_hinzufuegen'],'','',20,20);
		
			$Form->SchaltflaechenEnde();

			
			$Form->ZeileEnde();
			
		$Form->Formular_Ende();
	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071633");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071634");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	//global $AWIS_KEY1;
	//global $AWISBenutzer;
	global $DB;

	$Bedingung = '';
	
	if(isset($Param['VERTRAGSNUMMER']) AND $Param['VERTRAGSNUMMER']!='')
	{
		$Bedingung .= ' WHERE VERTRAGSNUMMER=' . $DB->FeldInhaltFormat('Z',$Param['VERTRAGSNUMMER'],false). ' ';
	}
	
	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>