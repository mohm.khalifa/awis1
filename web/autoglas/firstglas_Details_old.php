<?php

global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_HISTKEY;
global $AWISCursorPosition;
global $EditModus;
global $DetailAnsicht;

try {

    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');


    $TextKonserven = array();
    $TextKonserven[]=array('FGK','*');
    $TextKonserven[]=array('FKH','*');
    $TextKonserven[]=array('FKA','*');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','lbl_drucken');
    $TextKonserven[]=array('Wort','lbl_Hilfe');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Wort','lbl_AEMNachdruck');

    $Form = new awisFormular();

    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9100 = $AWISBenutzer->HatDasRecht(9100);

    if($Recht9100==0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Form->DebugAusgabe(1,$_POST);	

    $Form->Formular_Start();

    $AWIS_KEY1=-1;

    $SQL = 'SELECT * FROM laender WHERE lan_code = \''.$AWISBenutzer->BenutzerSprache().'\'';
    $rsUSERLAND = $DB->RecordSetOeffnen($SQL);

    //if($FKAListe !=0 || !isset($_POST['FKA_LISTE']) == 1 || !isset($Param['CheckSucKassendaten']) == true || !isset($_GET['FKA_KEY']) || !isset($_GET['FKAListe']) || !isset($Param['FKAListe'])== 1)

    if(isset($_POST['sucKassendatenSuchen'])!='on' && isset($_GET['FKAListe'])!=True && !isset($_GET['FKA_KEY']) && !isset($_REQUEST['FKABLOCK']) && !isset($_POST['cmdSpeichernKassendaten_x']))
    //if (isset($_POST['sucKassendatenSuchen'])!='on' || !isset($_GET['FKA_KEY']) || !isset($_GET['FKAListe']) || !isset($_POST['txtFKA_AEMNR']) || $FKAListe != 0)
    {
        if((isset($_POST['sucFGK_VORGANGNR']) AND $_POST['sucFGK_VORGANGNR']!=''))	// Schellsuche-Feld
        {
            if($_POST['sucFGK_VORGANGNR']!='') {
                $SQL = 'SELECT FGK_KEY FROM FGKOPFDATEN WHERE FGK_VORGANGNR = '.$DB->FeldInhaltFormat('T',$_POST['sucFGK_VORGANGNR'],true).'';
                $rsFGK = $DB->RecordSetOeffnen($SQL);
                if($rsFGK->FeldInhalt('FGK_KEY')!='') {
                    $AWIS_KEY1=$rsFGK->FeldInhalt('FGK_KEY');
                    //echo $AWIS_KEY1;
                }
            }
        }
        elseif(isset($_POST['cmdSuche_x']))	// Neue Suche gestartet
        {

        //$Param = array_fill(0,10,'');


            $Param[0] = isset($_POST['sucAuswahlSpeichern'])?$_POST['sucAuswahlSpeichern']:'off';

            $Param[1] = (isset($_POST['sucFKG_VORGANGNR'])?trim($_POST['sucFKG_VORGANGNR']):'');
            $Param[2] = isset($_POST['sucFGK_FILID'])?trim($_POST['sucFGK_FILID']):'';
            $Param[3] = isset($_POST['sucFGK_KFZKENNZ'])?trim($_POST['sucFGK_KFZKENNZ']):'';


            $Param['KEY']='';
            $Param['Block']=1;

            $AWISBenutzer->ParameterSchreiben('FirstglasDetails',implode(';',$Param));

        //$AWISBenutzer->ParameterSchreiben('FirstglasDetails',implode(';',$Param));
        }
        elseif(isset($_POST['cmdSpeichern_x'])) {
             include './firstglas_speichern.php';
        //$Form->DebugAusgabe(1,$_POST);

        }
        elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
            //$AWISBenutzer->ParameterSchreiben('FirstglasDetails',implode(';',$Param));
            include './firstglas_loeschen.php';
        }
        elseif(isset($_POST['cmdLoeschenAbbrechen'])) {

            //$Form->DebugAusgabe(1,$_REQUEST);
            //$Form->DebugAusgabe(1,$_POST);
            $AWIS_KEY1 = $_REQUEST['txtFGK_KEY'];
            
        }
        elseif(isset($_GET['Key']))			// FGK_KEY wurde angegeben (alte Syntax)
        {
            $AWIS_KEY1=floatval($_GET['Key']);
        }
        elseif(isset($_GET['FGK_KEY']))			// FGK_KEY wurde angegeben (neue Syntax)
        {
            $AWIS_KEY1=floatval($_GET['FGK_KEY']);
        }
        elseif(isset($_GET['FGK_VORGANGNR']))		// FGK_KEY wurde angegeben (neue Syntax)
        {
            $AWIS_KEY1=0;
            $SQL = 'SELECT FGK_KEY FROM FGKOPFDATEN WHERE FGK_KEY = '.$DB->FeldInhaltFormat('T',$_POST['txtFGK_KEY'],true).'';
            $rsFGK = $DB->RecordSetOeffnen($SQL);
            if(!$rsFGK->EOF()) {
                $AWIS_KEY1=$rsFGK->FeldInhalt('FGK_KEY');
            }
        }
        elseif(isset($_POST['cmdDSWeiter_x']))			// N�chster Datensatz
        {
            $AWIS_KEY1=floatval($_POST['txtFGK_KEY']);	// Letzter Datensatz

            $SQL = 'SELECT * FROM (SELECT FGK_KEY FROM FGKOPFDATEN WHERE FGK_KEY >'.$DB->FeldInhaltFormat('T',$_POST['txtFGK_KEY'],false).' ORDER BY nlssort(FGK_KEY,\'NLS_SORT=BINARY\')) WHERE ROWNUM = 1';
            $rsFGK = $DB->RecordSetOeffnen($SQL);
            if(!$rsFGK->EOF()) {
                $AWIS_KEY1=$rsFGK->FeldInhalt('FGK_KEY');
            }
        }
        elseif(isset($_POST['cmdDSZurueck_x']))			// N�chster Datensatz
        {
            $AWIS_KEY1=floatval($_POST['txtFGK_KEY']);	// Letzter Datensatz

            $SQL = 'SELECT * FROM (SELECT FGK_KEY FROM FGKOPFDATEN WHERE FGK_KEY <'.$DB->FeldInhaltFormat('T',$_POST['txtFGK_KEY'],false).' ORDER BY nlssort(FGK_KEY,\'NLS_SORT=BINARY\') DESC) WHERE ROWNUM = 1';
            $rsFGK = $DB->RecordSetOeffnen($SQL);
            if(!$rsFGK->EOF()) {
                $AWIS_KEY1=$rsFGK->FeldInhalt('FGK_KEY');
            }
        }
        elseif(isset($_POST['cmdDSNeu_x'])) {
            $AWIS_KEY1=0;			// Neuen Artikel anlegen
        }
        if(!isset($_GET['Seite']) AND (isset($_GET['Block']) OR isset($_GET['Sort']))) {
        //@$Param = unserialize(';',$AWISBenutzer->ParameterLesen('FirstglasDetails'));
            @$Param = unserialize(';',$AWISBenutzer->ParameterLesen('FirstglasDetails'));
        //$Form->DebugAusgabe(1,$Param);
        }


        if(!isset($Param) AND $AWIS_KEY1==-1)		// Keine Auswahl
        {
            if(!isset($_GET['Liste']) AND !isset($_REQUEST['Block'])) {
            //$Param = unserialize(';',$AWISBenutzer->ParameterLesen('FirstglasDetails'));

                @$Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
                @$AWIS_KEY1 = $Param["KEY"];
            //$Form->DebugAusgabe(1,$Param);
            //$Param =  unserialize(';',$AWISBenutzer->ParameterLesen('FirstglasDetails'));
            }

        //$Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
        //$AWIS_KEY1 = $Param["KEY"];
        //$Form->DebugAusgabe(1,$Param);

        }

        if(isset($_GET['FGKListe'])) {
            $AWIS_KEY1 =-1;
            $AWIS_KEY2 =-1;
            $AWIS_HISTKEY=-1;
            unset($Param);
        }


        if(isset($_GET['FKH_KEY'])) {
        //History - Tabelle

            $Form->Erstelle_Liste_Ueberschrift("Historisierung",1253);


            $Param['FKH_KEY'] = $_GET['FKH_KEY'];
            $AWIS_HISTKEY = $_GET['FKH_KEY'];
            //$Form->DebugAusgabe(1,$Param);
            //Parameter serialisieren
            $AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));
        //$AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param['FGK_KEY']));
        }


        //*************************************************************************************
        // Daten suchen
        //*************************************************************************************

        $Bedingung = '';

        if(!isset($_GET['Sort'])) {
            if(!isset($_GET['FKH_KEY'])) {
                $ORDERBY = ' ORDER BY FGK_KEY';
            }
            else {
                $ORDERBY = ' ORDER BY FKH_KEY';
            }
        }
        else {
            if(!isset($_GET['FKH_KEY'])) {
                $ORDERBY = ' ORDER BY FGK_KEY';
            }
            else {
                $ORDERBY = ' ORDER BY FKH_KEY';
            }

        }


        if($AWIS_KEY1!=-1) {
            if(!isset($_GET['FKH_KEY'])) {
            //echo "Hallo";
                $Bedingung .= ' AND FGK_KEY = 0'.floatval($AWIS_KEY1);

            }
            else {
                $Bedingung .= ' AND FKH_KEY = 0'.floatval($AWIS_HISTKEY);
            }
        }
        else {
        //$Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
            if(!isset($Param))			// Keine Parameter angegeben
            {
                $Param = array_fill(0,10,'');
            }
            else {
                if(isset($_GET['FGKListe'])) {
                }
                else {
                    if($Param[1]!='')		// FilialID
                    {
                        $Bedingung .= ' FGK_VORGANGNR '.$DB->LikeOderIst($Param[1],awisDatenbank::AWIS_LIKE_UPPER);
                    }
                    if($Param[2]!='')		// Bezeichnung
                    {
                        $Bedingung .= ' AND FGK_FILID ='.$DB->FeldInhaltFormat('N0',$Param[2]);
                    }
                    if($Param[3]!='') {
                        $Bedingung .= ' AND FGK_KFZKENNZ '.$DB->LIKEoderIST($Param[3],awisDatenbank::AWIS_LIKE_UPPER);
                    }

                //$AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));
                }
            }
        }

        echo '<form name=frmFirstglasDetails method=post action=./autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').'>';


        if($AWIS_KEY1!=-1 &&(!isset($_GET['FGKListe'])) && (!isset($_GET['FKH_KEY']))) {
        //echo "Hallo_1";

            $DetailAnsicht = true;

            $SQL = 'SELECT FGKOPFDATEN.*';
            $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
            $SQL .= ' from FGKOPFDATEN';
            $SQL .= ' WHERE '.substr($Bedingung,4);
        //echo $SQL;

        }
        else {  // && !isset(Param[1]) || !isset(Param[2]) || !isset(Param[3])
        // && (!isset($Param[1])) && (!isset($Param[2])) &&(!isset($Param[3])


            if(!isset($_GET['FKH_KEY']) && ($AWIS_KEY1==-1) && (!isset($Param[1])) && (!isset($Param[2])) &&(!isset($Param[3])) || (isset($_GET['FGKListe'])))
            //if(!isset($_GET['FKH_KEY']) && ($AWIS_KEY1==-1) && (!isset($Param[1])) && (!isset($Param[2])) &&(!isset($Param[3])) &&(!isset($_REQUEST['BLOCK'])) || (isset($_GET['FGKListe'])))
            {
            //echo "Hallo_2";

                $SQL = 'SELECT FGKOPFDATEN.*';
                $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                $SQL .= ' from FGKOPFDATEN';
            //$SQL .= ' WHERE '.substr($Bedingung,4);
            //echo $SQL;
            }
            else {
                if(isset($_GET['FKH_KEY'])) {
                    $SQL = 'SELECT FGKOPFDATEN_HIST.*';
                    $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                    $SQL .= ' from FGKOPFDATEN_HIST WHERE FKH_KEY='.$AWIS_HISTKEY;
                //echo $SQL;

                }
                else {
                    $SQL = 'SELECT FGKOPFDATEN.*';
                    $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
                    $SQL .= ' from FGKOPFDATEN';
                    if(substr($Bedingung,4)!='') {
                        $SQL .= ' WHERE '.substr($Bedingung,4);
                    }

                }
            //echo $SQL;
            }


        }


        //Block - FORMAT
        if($AWIS_KEY1<=0 && (!isset($_GET['FKH_KEY'])) && (!isset($_GET['FGK_KEY']))) {
        // Zum Bl�ttern in den Daten
        //echo "Bl�tter - Funktion";
            $Block = 1;
            if(isset($_REQUEST['Block'])) {
                $Block=$Form->Format('N0',$_REQUEST['Block'],false);
                $Param['Block']=$Block;
                $AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));
            }
            elseif(isset($Param['Block'])) {
                $Block=$Param['Block'];
            }

            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

            $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
        //echo $SQL;
        //$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
        }
        else {
            $MaxDS = 1;
            $ZeilenProSeite=1;
            $Block = 1;
        }

/*
$Block = 1;
if(isset($_REQUEST['Block']))
{
	$Block=$Form->Format('N0',$_REQUEST['Block'],false);
}
elseif(isset($Param['Block']) AND $Param['Block']!='')
{
	$Block = intval($Param['Block']);
}

$Param['ORDERBY']=$ORDERBY;
$Param['KEY']=$AWIS_KEY1;
$Param['Block']=$Block;
*/

        if(!isset($_GET['FKH_KEY'])) {
            $rsFGK = $DB->RecordSetOeffnen($SQL);

            if($rsFGK->AnzahlDatensaetze()>1) {
                $AWIS_KEY1 = 0;
            }
            else {
                $AWIS_KEY1 = $rsFGK->FeldInhalt("FGK_KEY");
            }


            //$Form->DebugAusgabe('1',$rsFGK);
            //$Form->Formular_Start();

            if($rsFGK->AnzahlDatensaetze()>1 AND $AWIS_KEY1==0)						// Liste anzeigen
            {
                $Form->SchreibeHTMLCode('<form name="frmFirstglasDetails" action="./autoglas_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');

                

                $Form->ZeileStart();
                $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
                $Link .= '&Sort=FGK_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FGK_KEY'))?'~':'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_KEY'],120,'',$Link);
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_FILID'],120,'',$Link);
                $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
                $Link .= '&Sort=FGK_FILID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FGK_FILID'))?'~':'');
                $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
                $Link .= '&Sort=FGK_VORGANGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FGK_VORGANGNR'))?'~':'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_VORGANGNR'],200,'',$Link);
                $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
                $Link .= '&Sort=FGK_VORGANGNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='YGK_VORGANGNR'))?'~':'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_KFZKENNZ'],200,'',$Link);
                $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
                $Link .= '&Sort=FGK_KFZKENNZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FGK_KFZKENNZ'))?'~':'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_KFZBEZ'],400,'',$Link);
                $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
                $Link .= '&Sort=FGK_KFZBEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FGK_KFZBEZ'))?'~':'');


                $Form->ZeileEnde();

                $FGKZeile=0;
                while(!$rsFGK->EOF()) {
                    $Form->ZeileStart();
                    $Link = './autoglas_Main.php?cmdAktion=Details&FGK_KEY='.$rsFGK->FeldInhalt('FGK_KEY').'';
                    $Form->Erstelle_ListenFeld('FGK_KEY',$rsFGK->FeldInhalt('FGK_KEY'),0,120,false,($FGKZeile%2),'',$Link,'T');
                    $Form->Erstelle_ListenFeld('FGK_FILID',$rsFGK->FeldInhalt('FGK_FILID'),0,120,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('FGK_VORGANGNR',$rsFGK->FeldInhalt('FGK_VORGANGNR'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('FGK_KFZKENNZ',$rsFGK->FeldInhalt('FGK_KFZKENNZ'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('FGK_KFZBEZ',$rsFGK->FeldInhalt('FGK_KFZBEZ'),0,400,false,($FGKZeile%2),'','');
                    $Form->ZeileEnde();

                    $rsFGK->DSWeiter();
                    $FGKZeile++;
                }

                $Link = './autoglas_Main.php?cmdAktion=Details';
                $Form->SchreibeHTMLCode("<input type='hidden' name='BLOCK'>");
                $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

                //$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

                
                //***************************************
                $Form->SchaltflaechenStart();
                $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                if(($Recht9100&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
                {
                }

                //$Form->Schaltflaeche('href', 'cmd_DruckRueckFuehrungsLieferschein','./rueckfuehrungsbearbeitung_Druck_RFS.php?DruckArt=2&RFK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_RueckfuehrungsscheinDrucken'],'.');
                $Form->SchaltflaechenEnde();

                $Form->SchreibeHTMLCode('</form>');
            }
            else {


                //$Form->Formular_Start();
                $Form->SchreibeHTMLCode('<form name=frmFirstGlasDetails action=./autoglas_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');


                if ($AWIS_KEY1 !=-1) {
                    $AWIS_KEY1 = $rsFGK->FeldInhalt('FGK_KEY');
                    $Param['KEY']=$AWIS_KEY1;
                    $AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));
                    $Form->Erstelle_HiddenFeld('FGK_KEY',$AWIS_KEY1);
                    $OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
                }
                else {
                    $AWIS_KEY1 = -1;
                    $AWIS_KEY2 = -1;
                }

                // Infozeile zusammenbauen
                $Felder = array();
                $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./autoglas_Main.php?cmdAktion=Details&FGKListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
                $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFGK->FeldInhalt('FGK_USER'));
                $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsFGK->FeldInhalt('FGK_USERDAT')));
                $Form->InfoZeile($Felder,'');


                $EditRecht=(($Recht9100&2)!=0);

                //echo $AWIS_KEY1;

                if($AWIS_KEY1>=0) {
                    //$EditRecht=true;

                    $SQL  ='SELECT fgd_fgb_key, fgb_bezeichnung, max(status) as STATUS ';
                    $SQL .='FROM ( ';
                    $SQL .='SELECT fgd.fgd_fgb_key, \'X\' as status, fgb.fgb_bezeichnung ';
                    $SQL .='FROM FGKOPFDATEN fgk INNER JOIN FGSTATUSDEFINITIONEN fgd ON fgk.fgk_fgn_key = fgd.fgd_fgn_key ';
                    $SQL .='INNER JOIN FGSTATUSBESCHREIBUNGEN fgb ON fgd.fgd_fgb_key = fgb_key ';
                    $SQL .='WHERE FGK_VORGANGNR ='.$DB->FeldInhaltFormat('T',$rsFGK->FeldInhalt('FGK_VORGANGNR'));
                    $SQL .=' UNION';
                    $SQL .=' SELECT fgb.fgb_key, \'\' as status, fgb.fgb_bezeichnung';
                    $SQL .=' FROM FGSTATUSBESCHREIBUNGEN fgb )';
                    $SQL .=' group by fgd_fgb_key, fgb_bezeichnung';
                    $SQL .=' order by fgd_fgb_key';

                    $rsCheckBoxen = $DB->RecordSetOeffnen($SQL);

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KEY'].':',150);
                    $Form->Erstelle_TextFeld('FGK_KEY',$rsFGK->FeldInhalt('FGK_KEY'),20,50,false);
                    $Form->ZeileEnde();

                    $Form->Trennzeile('O');

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KENNUNG'].':',150);
                    $Form->Erstelle_TextFeld('FGK_KENNUNG',$rsFGK->FeldInhalt('FGK_KENNUNG'),10,200,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_FILID'].':',150);
                    $Form->Erstelle_TextFeld('FGK_FILID',$rsFGK->FeldInhalt('FGK_FILID'),5,100,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VORGANGNR'].':',150);
                    $Form->Erstelle_TextFeld('FGK_VORGANGNR',$rsFGK->FeldInhalt('FGK_VORGANGNR'),50,50,false);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KFZKENNZ'].':',150);
                    $Form->Erstelle_TextFeld('FGK_KFZKENNZ',$rsFGK->FeldInhalt('FGK_KFZKENNZ'),10,200,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KBANR'].':',150);
                    $Form->Erstelle_TextFeld('FGK_KBANR',$rsFGK->FeldInhalt('FGK_KBANR'),7,100,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_FAHRGESTELLNR'].':',150);
                    $Form->Erstelle_TextFeld('FGK_FAHRGESTELLNR',$rsFGK->FeldInhalt('FGK_FAHRGESTELLNR'),30,100,false);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KFZBEZ'].':',150);
                    $Form->Erstelle_TextFeld('FGK_KFZBEZ',$rsFGK->FeldInhalt('FGK_KFZBEZ'),150,600,false);
                    $Form->ZeileEnde();

                    $Form->Trennzeile('O');

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VERSICHERUNG'].':',150);
                    $Form->Erstelle_TextFeld('FGK_VERSICHERUNG',$rsFGK->FeldInhalt('FGK_VERSICHERUNG'),150,600,false);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VERSSCHEINNR'].':',150);
                    $Form->Erstelle_TextFeld('FGK_VERSSCHEINNR',$rsFGK->FeldInhalt('FGK_VERSSCHEINNR'),20,200,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_SB'].':',150);
                    $Form->Erstelle_TextFeld('FGK_SB',$rsFGK->FeldInhalt('FGK_SB'),7,100,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_MONTAGEDATUM'].':',150);
                    $Form->Erstelle_TextFeld('FGK_MONTAGEDATUM',$rsFGK->FeldInhalt('FGK_MONTAGEDATUM'),30,200,false);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_ZEITSTEMPEL'].':',150);
                    $Form->Erstelle_TextFeld('FGK_ZEITSTEMPEL',$rsFGK->FeldInhalt('FGK_ZEITSTEMPEL'),20,200,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_STEUERSATZ'].':',150);
                    $Form->Erstelle_TextFeld('FGK_STEUERSATZ',$rsFGK->FeldInhalt('FGK_STEUERSATZ'),7,100,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KUNDENNAME'].':',150);
                    $Form->Erstelle_TextFeld('FGK_KUNDENNAME',$rsFGK->FeldInhalt('FGK_KUNDENNAME'),30,200,false);
                    $Form->ZeileEnde();

                    $Form->Trennzeile('O');

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_ZEITSTEMPEL_KASSIERT'].':',150);
                    $Form->Erstelle_TextFeld('FGK_ZEITSTEMPEL_KASSIERT',$rsFGK->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'),20,200,false);
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_FREIGABEGRUND'].':',150);
                    $Form->Erstelle_TextFeld('FGK_FREIGABEGRUND',$rsFGK->FeldInhalt('FGK_FREIGABEGRUND'),92,100,$EditRecht);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_DATUMZUGANG'].':',150);
                    $Form->Erstelle_TextFeld('FGK_DATUMZUGANG',$rsFGK->FeldInhalt('FGK_DATUMZUGANG'),20,50,false);
                    $Form->ZeileEnde();


                    $Form->Trennzeile('P');
                    //Status anzeigen
                    echo "<br>";
                    $zeilenNr = 0;

                    $Form->ZeileStart();

                    //$Form->DebugAusgabe(1,$rsCheckBoxen);

                    $zeilenNr = 0;
                    $StornoCount = 0;
                    $ZugangCount = 0;
                    $count = 0;

                    while(!$rsCheckBoxen->EOF()) {
                    //echo $zeilenNr;
                        if ($zeilenNr != 3) {

                            if($rsCheckBoxen->FeldInhalt('FGB_BEZEICHNUNG') == 'Vorgang_Blank')
                            {
                                
                            }
                            else
                            {
                                if($count == 0)
                                {
                                    $Form->Erstelle_TextLabel($rsCheckBoxen->FeldInhalt('FGB_BEZEICHNUNG').':',280);
                                    $zeilenNr = 0;
                                    $count++;
                                }
                                else
                                {
                                    $Form->Erstelle_TextLabel($rsCheckBoxen->FeldInhalt('FGB_BEZEICHNUNG').':',280);
                                }

                            }
                            
                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '1') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,$EditRecht,'','','1','','',$Daten);
                            }

                            //Kassendaten vorhanden
                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '2') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }

                            //Abgeglichen mit Kasse
                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '3') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '4') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '5') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '6') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }


                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '7') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,$EditRecht,'','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '8') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '9') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '10') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '11') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,$EditRecht,'','','1','','',$Daten);
                            }

                            if($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY')== '12') {
                                $Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
                                $Form->Erstelle_SelectFeld('FeldSpeichern'.$rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'),($rsCheckBoxen->FeldInhalt('STATUS')=='X'?1:0),70,'','','','1','','',$Daten);
                            }




                            //$Form->ZeileEnde();
                            $zeilenNr++;
                        }
                        else {
                            $Form->ZeileEnde();
                            $zeilenNr = 0;
                            $Form->ZeileStart();
                            $rsCheckBoxen->DSZurueck();
                        }

                        $rsCheckBoxen->DSWeiter();
                    }


                    $Form->Trennzeile('O');
                }

                

                //Erstelle Unterregister
                if ($AWIS_KEY1 !=0) {
                    $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
                    $SubReg = new awisRegister(9101);
                    $SubReg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
                }
                //$Form->Formular_Ende();

                $Form->SchaltflaechenStart();
                $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                if(($Recht9100&4)==4) {
                    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
                }
                //$Form->Schaltflaeche('href', 'cmd_DruckRueckFuehrungsLieferschein','./rueckfuehrungsbearbeitung_Druck_RFS.php?DruckArt=2&RFK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_AEMNachdruck'],'.');
                $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
                $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');

                $Form->SchaltflaechenEnde();

                $Form->SchreibeHTMLCode('</form>');

                


        }//Ende Else


        }
        else {
        //WENN FKH_KEY gesetzt wurde
            $rsFKH = $DB->RecordSetOeffnen($SQL);


            //$Form->Formular_Start();
            $Form->SchreibeHTMLCode('<form name=frmFirstGlasDetails action=./autoglas_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');


            // Infozeile zusammenbauen
            $Felder = array();
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./autoglas_Main.php?cmdAktion=Details&FGKListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFKH->FeldInhalt('FGK_USER'));
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsFKH->FeldInhalt('FGK_USERDAT')));
            $Form->InfoZeile($Felder,'');


            $EditRecht=(($Recht9100&2)!=0);

            if($AWIS_KEY1>=0) {
                $EditRecht=true;

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KEY'].':',150);
                $Form->Erstelle_TextFeld('FKH_KEY',$rsFKH->FeldInhalt('FKH_KEY'),20,50,false);
                $Form->ZeileEnde();

                $Form->Trennzeile('L');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KENNUNG'].':',150);
                $Form->Erstelle_TextFeld('FKH_KENNUNG',$rsFKH->FeldInhalt('FKH_KENNUNG'),10,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_FILID'].':',150);
                $Form->Erstelle_TextFeld('FKH_FILID',$rsFKH->FeldInhalt('FKH_FILID'),5,100,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_VORGANGNR'].':',150);
                $Form->Erstelle_TextFeld('FKH_VORGANGNR',$rsFKH->FeldInhalt('FKH_VORGANGNR'),50,50,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KFZKENNZ'].':',150);
                $Form->Erstelle_TextFeld('FKH_KFZKENNZ',$rsFKH->FeldInhalt('FKH_KFZKENNZ'),10,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KBANR'].':',150);
                $Form->Erstelle_TextFeld('FKH_KBANR',$rsFKH->FeldInhalt('FKH_KBANR'),7,100,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_FAHRGESTELLNR'].':',150);
                $Form->Erstelle_TextFeld('FKH_FAHRGESTELLNR',$rsFKH->FeldInhalt('FKH_FAHRGESTELLNR'),30,100,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KFZBEZ'].':',150);
                $Form->Erstelle_TextFeld('FKH_KFZBEZ',$rsFKH->FeldInhalt('FKH_KFZBEZ'),150,600,false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_VERSICHERUNG'].':',150);
                $Form->Erstelle_TextFeld('FKH_VERSICHERUNG',$rsFKH->FeldInhalt('FKH_VERSICHERUNG'),150,600,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_VERSSCHEINNR'].':',150);
                $Form->Erstelle_TextFeld('FKH_VERSSCHEINNR',$rsFKH->FeldInhalt('FKH_VERSSCHEINNR'),20,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_SB'].':',150);
                $Form->Erstelle_TextFeld('FKH_SB',$rsFKH->FeldInhalt('FKH_SB'),7,100,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_MONTAGEDATUM'].':',150);
                $Form->Erstelle_TextFeld('FKH_MONTAGEDATUM',$rsFKH->FeldInhalt('FKH_MONTAGEDATUM'),30,100,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_ZEITSTEMPEL'].':',150);
                $Form->Erstelle_TextFeld('FKH_ZEITSTEMPEL',$rsFKH->FeldInhalt('FKH_ZEITSTEMPEL'),20,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_STEUERSATZ'].':',150);
                $Form->Erstelle_TextFeld('FKH_STEUERSATZ',$rsFKH->FeldInhalt('FKH_STEUERSATZ'),7,100,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KUNDENNAME'].':',150);
                $Form->Erstelle_TextFeld('FKH_KUNDENNAME',$rsFKH->FeldInhalt('FKH_KUNDENNAME'),30,200,false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_ZEITSTEMPEL_KASSIERT'].':',150);
                $Form->Erstelle_TextFeld('FKH_ZEITSTEMPEL_KASSIERT',$rsFKH->FeldInhalt('FKH_ZEITSTEMPEL_KASSIERT'),20,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_FREIGABEGRUND'].':',150);
                $Form->Erstelle_TextFeld('FKH_FREIGABEGRUND',$rsFKH->FeldInhalt('FKH_FREIGABEGRUND'),92,100,$EditRecht);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_DATUMZUGANG'].':',150);
                $Form->Erstelle_TextFeld('FKH_DATUMZUGANG',$rsFKH->FeldInhalt('FKH_DATUMZUGANG'),20,50,false);
                $Form->ZeileEnde();


                $Form->Trennzeile('O');
            //Status anzeigen
            }

            //Erstelle Unterregister
            if ($AWIS_HISTKEY !=0) {
                $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:''));
                $SubReg = new awisRegister(9101);
                $SubReg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
            }

            //$Form->Formular_Ende();

            $Form->SchaltflaechenStart();
            $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
            //$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
            //$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
            if(($Recht9100&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
            {
            }
            $Form->SchaltflaechenEnde();

            $Form->SchreibeHTMLCode('</form>');

            

        }
    }
    else {
    //$Form->DebugAusgabe(1,$_REQUEST);

        if((isset($_POST['sucFGK_VORGANGNR']) AND $_POST['sucFGK_VORGANGNR']!=''))	// Schellsuche-Feld
        {
            if($_POST['sucFGK_VORGANGNR']!='') {
                $SQL = 'SELECT FKA_KEY FROM FKASSENDATEN WHERE FKA_AEMNR = '.$DB->FeldInhaltFormat('T',$_POST['sucFGK_VORGANGNR'],true).'';
                $rsFGK = $DB->RecordSetOeffnen($SQL);
                if($rsFGK->FeldInhalt('FKA_KEY')!='') {
                    $AWIS_KEY1=$rsFGK->FeldInhalt('FKA_KEY');
                }
            }
        }
        elseif(isset($_POST['cmdSuche_x']))	// Neue Suche gestartet
        {

            $Param = array_fill(0,10,'');

            $Param[0] = isset($_POST['sucAuswahlSpeichern'])?$_POST['sucAuswahlSpeichern']:'off';
            @$Param[1] = (isset($_POST['sucFGK_VORGANGNR'])?trim($_POST['sucFKG_VORGANGNR']):'');
            $Param[2] = isset($_POST['sucFGK_FILID'])?trim($_POST['sucFGK_FILID']):'';
            $Param[3] = isset($_POST['sucFGK_KFZKENNZ'])?trim($_POST['sucFGK_KFZKENNZ']):'';

            $AWISBenutzer->ParameterSchreiben('FirstglasDetails',implode(';',$Param));


        }
        elseif(isset($_POST['cmdSpeichernKassendaten_x'])) {
            include './firstglas_kassendaten_speichern.php';

            $TextKonserven = array();
            $TextKonserven[]=array('FGK','*');
            $TextKonserven[]=array('FKH','*');
            $TextKonserven[]=array('FKA','*');
            $TextKonserven[]=array('Wort','Seite');
            $TextKonserven[]=array('Wort','lbl_suche');
            $TextKonserven[]=array('Wort','lbl_weiter');
            $TextKonserven[]=array('Wort','lbl_speichern');
            $TextKonserven[]=array('Wort','lbl_trefferliste');
            $TextKonserven[]=array('Wort','lbl_aendern');
            $TextKonserven[]=array('Wort','lbl_hilfe');
            $TextKonserven[]=array('Wort','lbl_hinzufuegen');
            $TextKonserven[]=array('Wort','lbl_loeschen');
            $TextKonserven[]=array('Wort','lbl_zurueck');
            $TextKonserven[]=array('Wort','lbl_DSZurueck');
            $TextKonserven[]=array('Wort','lbl_DSWeiter');
            $TextKonserven[]=array('Wort','lbl_drucken');
            $TextKonserven[]=array('Wort','lbl_Hilfe');
            $TextKonserven[]=array('Wort','txt_BitteWaehlen');
            $TextKonserven[]=array('Liste','lst_JaNein');
            $TextKonserven[]=array('Wort','lbl_AEMNachdruck');




            $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

        //$Form->DebugAusgabe(1,$_REQUEST);
        //echo "HALLO_AUS";
        }
        elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
            include './firstglas_loeschen.php';
        //$Form->DebugAusgabe(1,$_REQUEST);

        }
        elseif(isset($_POST['cmdLoeschenAbbrechen'])) {
            $AWIS_KEY1 = $_POST['txtFKAKey'];
        }
        elseif(isset($_GET['Key']))			// FKA_KEY wurde angegeben (alte Syntax)
        {
            $AWIS_KEY1=floatval($_GET['Key']);
        }
        elseif(isset($_GET['FKA_KEY']))			// FKA_KEY wurde angegeben (neue Syntax)
        {
            $AWIS_KEY1=floatval($_GET['FKA_KEY']);
        }
        elseif(isset($_GET['FKA_AEMNR']))		// FGK_KEY wurde angegeben (neue Syntax)
        {
            $AWIS_KEY1=0;
            $SQL = 'SELECT FKA_KEY FROM FKASSENDATEN WHERE FKA_KEY = '.$DB->FeldInhaltFormat('T',$_POST['txtFKA_KEY'],true).'';
            $rsFGK = $DB->RecordSetOeffnen($SQL);
            if(!$rsFGK->EOF()) {
                $AWIS_KEY1=$rsFGK->FeldInhalt('FKA_KEY');
            }
        }
        elseif(isset($_POST['txtFKA_AEMNR'])!='') {
            $AWIS_KEY1=0;
            $SQL = 'SELECT FKA_KEY FROM FKASSENDATEN WHERE FKA_AEMNR = '.$DB->FeldInhaltFormat('T',$_POST['txtFKA_AEMNR'],true).'';
            //echo $SQL;
            $rsFGK = $DB->RecordSetOeffnen($SQL);
            if(!$rsFGK->EOF()) {
                $AWIS_KEY1=$rsFGK->FeldInhalt('FKA_KEY');
            }
        }
        elseif(isset($_POST['cmdDSWeiter_x']))			// N�chster Datensatz
        {
            $AWIS_KEY1=floatval($_POST['txtFKA_KEY']);	// Letzter Datensatz

            $SQL = 'SELECT * FROM (SELECT FKA_KEY FROM FKASSENDATEN WHERE FKA_AEMNR >'.$DB->FeldInhaltFormat('T',$_POST['txtFKA_AEMNR'],false).' ORDER BY nlssort(FKA_KEY,\'NLS_SORT=BINARY\')) WHERE ROWNUM = 1';

            $rsFGK = $DB->RecordSetOeffnen($SQL);
            if(!$rsFGK->EOF()) {
                $AWIS_KEY1=$rsFGK->FeldInhalt('FKA_KEY');
            }
        }
        elseif(isset($_POST['cmdDSZurueck_x']))			// N�chster Datensatz
        {
            $AWIS_KEY1=floatval($_POST['txtFKA_KEY']);	// Letzter Datensatz

            $SQL = 'SELECT * FROM (SELECT FKA_KEY FROM FKASSENDATEN WHERE FKA_AEMNR <'.$DB->FeldInhaltFormat('T',$_POST['txtFKA_AEMNR'],false).' ORDER BY nlssort(FKA_KEY,\'NLS_SORT=BINARY\') DESC) WHERE ROWNUM = 1';
            $rsFGK = $DB->RecordSetOeffnen($SQL);
            if(!$rsFGK->EOF()) {
                $AWIS_KEY1=$rsFGK->FeldInhalt('FKA_KEY');
            }
        }
        elseif(isset($_POST['cmdDSNeu_x'])) {
            $AWIS_KEY1=0;			// Neuen Artikel anlegen
        }
        if(!isset($_GET['Seite']) AND (isset($_GET['FKABlock']) OR isset($_GET['Sort']))) {
            $Param = unserialize(';',$AWISBenutzer->ParameterLesen('FirstglasDetails'));
        }
    /*
    $FKABlock = 1;
	if(isset($_REQUEST['FKABLOCK']))
	{
		$Block=$Form->Format('N0',$_REQUEST['FKABLOCK'],false);
	}
	elseif(isset($Param['FKABLOCK']) AND $Param['FKABLOCK']!='')
	{
		$FKABlock = intval($Param['FKABLOCK']);
	}
    */
        if(!isset($Param) AND $AWIS_KEY1==-1)		// Keine Auswahl
        {
            if(!isset($_GET['FKAListe'])) {
            //$Param = unserialize(';',$AWISBenutzer->ParameterLesen('FirstglasDetails'));
                $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
                $AWIS_KEY1 = $Param["KEY"];
            }
        }

        //FGKListe --> FKAListe
        if(isset($_GET['FKAListe'])) {
            $AWIS_KEY1 =-1;
            $AWIS_KEY2 =-1;
            $AWIS_HISTKEY=-1;
            unset($Param);
        }

        $Bedingung = '';


        if(!isset($_GET['Sort'])) {
            if(!isset($_GET['FKA_KEY'])) {
                $ORDERBY = ' ORDER BY FKA_KEY';
            }
            else {
                $ORDERBY = ' ORDER BY FKA_KEY';
            }
        }
        else {
            if(!isset($_GET['FKA_KEY'])) {
                $ORDERBY = ' ORDER BY FKA_KEY';
            }
            else {
                $ORDERBY = ' ORDER BY FKA_KEY';
            }
        }
        //SQL BAUEN

        if($AWIS_KEY1!=-1) {

            if(isset($_GET['FKA_KEY'])) {
                $Bedingung .= ' AND FKA_KEY = 0'.floatval($AWIS_KEY1);
            }
            else {
                $Bedingung .= ' AND FKA_KEY = 0'.floatval($AWIS_KEY1);
            }
        }
        else {
        //$Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
            if(!isset($Param))			// Keine Parameter angegeben
            {
            //echo "Hallo";
                $Param = array_fill(0,10,'');
            }
            else {
                if(isset($_GET['FKAListe'])) {
                }
                else {
                //echo "Hallo";
                    if($Param[1]!='')		// FilialID
                    {
                        $Bedingung .= ' FKA_AEMNR '.$DB->LikeOderIst($Param[1],awisDatenbank::AWIS_LIKE_UPPER);
                    }
                    if($Param[2]!='')		// Bezeichnung
                    {
                        $Bedingung .= ' AND FKA_FILID '.$DB->FeldInhalt('NO',$Param[2]);
                    }
                    if($Param[3]!='') {
                        $Bedingung .= ' AND FKA_KFZKENNZ '.$DB->LIKEoderIST($Param[3],awisDatenbank::AWIS_LIKE_UPPER);
                    }

                //$AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));
                }
            }
        }
        echo '<form name=frmFirstglasDetails method=post action=./autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').'>';

        if($AWIS_KEY1!=-1 &&(!isset($_GET['FKAListe'])) && !isset($_REQUEST['FKABLOCK'])) {
            $DetailAnsicht = true;

            $SQL = 'SELECT FKASSENDATEN.*';
            $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
            $SQL .= ' from FKASSENDATEN';
            $SQL .= ' WHERE '.substr($Bedingung,4);
        //echo $SQL;
        }
        else {
            $SQL = 'SELECT FKASSENDATEN.*';
            $SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
            $SQL .= ' from FKASSENDATEN';
            //$SQL .= ' WHERE '.substr($Bedingung,4);
            //echo $SQL;
            if(substr($Bedingung,4)!='') {
                $SQL .= ' WHERE '.substr($Bedingung,4);
            }
        } //Ende - Else
        $rsFGK = $DB->RecordSetOeffnen($SQL);

        if($AWIS_KEY1<=0) {
        // Zum Bl�ttern in den Daten
        //echo "Bl�tter - Funktion";
        //$Form->DebugAusgabe(1,$_REQUEST);
            $Block = 1;
            if(isset($_REQUEST['FKABLOCK'])) {
                $Block=$Form->Format('N0',$_REQUEST['Block'],false);
                $Param['Block']=$Block;
                $AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));
            }
            elseif(isset($Param['Block'])) {
                $Block=$Param['Block'];
            }

            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

            $StartZeile = (($Block-1)*$ZeilenProSeite)+1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = 'SELECT * FROM ('.$SQL.') DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
        //echo $SQL;
        //$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
        }
        else {
            $MaxDS = 1;
            $ZeilenProSeite=1;
            $Block = 1;
        }

        if(!isset($_GET['FKA_KEY'])) {
            $rsFGK = $DB->RecordSetOeffnen($SQL);

            if($rsFGK->AnzahlDatensaetze()>1) {
                $AWIS_KEY1 = 0;
            }
            else {
                $AWIS_KEY1 = $rsFGK->FeldInhalt("FKA_KEY");
            }
        }


        if($rsFGK->AnzahlDatensaetze()>1 AND $AWIS_KEY1==0)						// Liste anzeigen
        {
            $Form->SchreibeHTMLCode('<form name="frmFirstglasDetails" action="./autoglas_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');

            $Param['BLOCK']=$Block;
            $Param['KEY']='';

            $AWISBenutzer->ParameterSchreiben('FirstglasDetails', $Param);

            //$Form->Formular_Start();

            $Form->ZeileStart();
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_KEY'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KEY'],60,'',$Link);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_FILID'],60,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_FILID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_FILID'))?'~':'');
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_AEMNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_AEMNR'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_AEMNR'],150,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_AEMNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_AEMNR'))?'~':'');
            $Link .= '&Sort=FKA_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_DATUM'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_DATUM'],160,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_UHRZEIT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_UHRZEIT'))?'~':'');
            $Link .= '&Sort=FKA_UHRZEIT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_UHRZEIT'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_UHRZEIT'],100,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_UHRZEIT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_UHRZEIT'))?'~':'');
            $Link .= '&Sort=FKA_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_WANR'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_WANR'],100,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_WANR'))?'~':'');
            $Link .= '&Sort=FKA_MENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_MENGE'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_MENGE'],70,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_MENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_MENGE'))?'~':'');
            $Link .= '&Sort=FKA_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_BETRAG'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BETRAG'],100,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_BETRAG'))?'~':'');
            $Link .= '&Sort=FKA_BETRAG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_BETRAG'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'],160,'',$Link);
            $Link = './autoglas_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=FKA_KFZKENNZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FKA_KFZKENNZ'))?'~':'');

            $Form->ZeileEnde();

            $FGKZeile=0;
            while(!$rsFGK->EOF()) {
                $Form->ZeileStart();
                $Link = './autoglas_Main.php?cmdAktion=Details&FKA_KEY='.$rsFGK->FeldInhalt('FKA_KEY').'';
                $Form->Erstelle_ListenFeld('FKA_KEY',$rsFGK->FeldInhalt('FKA_KEY'),0,60,false,($FGKZeile%2),'',$Link,'T');
                $Form->Erstelle_ListenFeld('FKA_FILID',$rsFGK->FeldInhalt('FKA_FILID'),0,60,false,($FGKZeile%2),'','');
                $Form->Erstelle_ListenFeld('FKA_AEMNR',$rsFGK->FeldInhalt('FKA_AEMNR'),0,150,false,($FGKZeile%2),'','');
                $Form->Erstelle_ListenFeld('FKA_DATUM',$rsFGK->FeldInhalt('FKA_DATUM'),0,160,false,($FGKZeile%2),'','');
                $Form->Erstelle_ListenFeld('FKA_UHRZEIT',$rsFGK->FeldInhalt('FKA_UHRZEIT'),0,100,false,($FGKZeile%2),'','');
                $Form->Erstelle_ListenFeld('FKA_WANR',$rsFGK->FeldInhalt('FKA_WANR'),0,100,false,($FGKZeile%2),'','');
                $Form->Erstelle_ListenFeld('FKA_MENGE',$rsFGK->FeldInhalt('FKA_MENGE'),0,70,false,($FGKZeile%2),'','');
                $Form->Erstelle_ListenFeld('FKA_BETRAG',$rsFGK->FeldInhalt('FKA_BETRAG'),0,100,false,($FGKZeile%2),'','');
                $Form->Erstelle_ListenFeld('FKA_KFZKENNZ',$rsFGK->FeldInhalt('FKA_KFZKENNZ'),0,160,false,($FGKZeile%2),'','');

                $Form->ZeileEnde();

                $rsFGK->DSWeiter();
                $FGKZeile++;
            }

            //----------------------------------------------------------------------
            //Bl�tterfunktion
            //----------------------------------------------------------------------

            $Link = './autoglas_Main.php?cmdAktion=Details&FKABLOCK&FKAListe=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');

            $Param['FKAListe'] = 1;
            $Param['CheckSucKassendaten'] = True;
            //Parameter f�r FKAListe schreiben
            $AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));

            //$Block ersetzen mit FKABlock - Variable
            $Form->SchreibeHTMLCode("<input type='hidden' name='FKABLOCK'>");
            $Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

            //$Form->Formular_Ende();

            //***************************************
            $Form->SchaltflaechenStart();
            $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
            if(($Recht9100&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
            {
            }
            $Form->SchaltflaechenEnde();

            $Form->SchreibeHTMLCode('</form>');
        }else//Detail - Ansicht der Datensaetze
        {
            //$Form->Formular_Start();
            $Form->SchreibeHTMLCode('<form name=frmFirstGlasDetails action=./autoglas_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');


            if ($AWIS_KEY1 !=-1) {
                $AWIS_KEY1 = $rsFGK->FeldInhalt('FKA_KEY');
                $Param['KEY']=$AWIS_KEY1;
                $AWISBenutzer->ParameterSchreiben('FirstglasDetails',serialize($Param));
                $Form->Erstelle_HiddenFeld('FKA_KEY',$AWIS_KEY1);
                $OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
            }
            else {
                $AWIS_KEY1 = -1;
                $AWIS_KEY2 = -1;
            }

            // Infozeile zusammenbauen
            $Felder = array();
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./autoglas_Main.php?cmdAktion=Details&FKAListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsFGK->FeldInhalt('FGK_USER'));
            $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',$rsFGK->FeldInhalt('FGK_USERDAT')));
            $Form->InfoZeile($Felder,'');


            $EditRecht=(($Recht9100&2)!=0);

            //echo $AWIS_KEY1;

            if($AWIS_KEY1>=0) {
                $EditRecht=true;

                $rsCheckBoxen = $DB->RecordSetOeffnen($SQL);

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KEY'].':',150);
                $Form->Erstelle_TextFeld('FKA_KEY',$rsFGK->FeldInhalt('FKA_KEY'),20,50,false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KENNUNG'].':',150);
                $Form->Erstelle_TextFeld('FKA_KENNUNG',$rsFGK->FeldInhalt('FKA_KENNUNG'),10,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_FILID'].':',150);
                $Form->Erstelle_TextFeld('FKA_FILID',$rsFGK->FeldInhalt('FKA_FILID'),5,100,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_AEMNR'].':',150);
                $Form->Erstelle_TextFeld('FKA_AEMNR',$rsFGK->FeldInhalt('FKA_AEMNR'),50,50,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_DATUM'].':',150);
                $Form->Erstelle_TextFeld('FKA_DATUM',$rsFGK->FeldInhalt('FKA_DATUM'),10,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_UHRZEIT'].':',150);
                $Form->Erstelle_TextFeld('FKA_UHRZEIT',$rsFGK->FeldInhalt('FKA_UHRZEIT'),7,100,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BSA'].':',150);
                $Form->Erstelle_TextFeld('FKA_BSA',$rsFGK->FeldInhalt('FKA_BSA'),30,100,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_WANR'].':',150);
                $Form->Erstelle_TextFeld('FKA_WANR',$rsFGK->FeldInhalt('FKA_WANR'),150,600,false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_AEMNR'].':',150);

                $Form->Erstelle_HiddenFeld('FKA_OLDAEMNR', $rsFGK->FeldInhalt('FKA_AEMNR'));
                $Form->Erstelle_TextFeld('FKA_AEMNR',$rsFGK->FeldInhalt('FKA_AEMNR'),13,600,True);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_ATUNR'].':',150);
                $Form->Erstelle_TextFeld('FKA_ATUNR',$rsFGK->FeldInhalt('FKA_ATUNR'),20,200,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_MENGE'].':',150);
                $Form->Erstelle_TextFeld('FKA_MENGE',$rsFGK->FeldInhalt('FKA_MENGE'),7,100,false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BETRAG'].':',150);
                $Form->Erstelle_TextFeld('FKA_BETRAG',$rsFGK->FeldInhalt('FKA_BETRAG'),30,200,false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'].':',150);
                $Form->Erstelle_TextFeld('FKA_KFZKENNZ',$rsFGK->FeldInhalt('FKA_KFZKENNZ'),20,200,false);
                //$Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BEMERKUNG'].':',150);
                //$Form->Erstelle_TextFeld('FKA_BEMERKUNG',$rsFGK->FeldInhalt('FKA_BEMERKUNG'),80,300,True);

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_STATUS'].':',150);
                $Form->Erstelle_TextFeld('FKA_STATUS',$rsFGK->FeldInhalt('FKA_STATUS'),20,200,false);
                $Form->ZeileEnde();

                //$Form->Formular_Ende();


                $Form->SchaltflaechenStart();
                $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
                if(($Recht9100&4)==4) {
                    $Form->Schaltflaeche('image', 'cmdSpeichernKassendaten', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
                }
                $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
                $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');

                $Form->SchaltflaechenEnde();

                $Form->SchreibeHTMLCode('</form>');

                
            }
        }


    }

    $Form->Formular_Ende();

}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>