<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $BSW;

$DetailAnsicht = false;
$POSTbenutzen = true;

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$AWIS_KEY1 = '';
if (isset($_GET['BSW_KEY'])) {
    $AWIS_KEY1 = $BSW->DB->FeldInhaltFormat('N0', $_GET['BSW_KEY']);
} elseif (isset($_POST['txtBSW_KEY'])) {
    $AWIS_KEY1 = $BSW->DB->FeldInhaltFormat('N0', $_POST['txtBSW_KEY']);
}
if (isset($_POST['cmdDSNeu_x'])) {
    $AWIS_KEY1 = -1;
    $_POST = array();
}
$BSW->Param['KEY'] = $AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $POSTbenutzen = false;
    $BSW->Param = array();
    $BSW->Param['BSW_KARTENNUMMER'] = $_POST['sucBSW_KARTENNUMMER'];
    $BSW->Param['BSW_BONNR'] = $_POST['sucBSW_BONNR'];
    $BSW->Param['BSW_DATUM_VON'] = $_POST['sucBSW_DATUM_VON'];
    $BSW->Param['BSW_DATUM_BIS'] = $_POST['sucBSW_DATUM_BIS'];


    $BSW->Param['KEY'] = '';
    $BSW->Param['WHERE'] = '';
    $BSW->Param['ORDER'] = '';
    $BSW->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
} elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) { //Irgendwas mit l�schen?
    include('./bsw_loeschen.php');
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include('./bsw_speichern.php');
}else
{ //User hat den Reiter gewechselt.
    $AWIS_KEY1 = $BSW->Param['KEY'];
}

//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************
if (!isset($_GET['Sort'])) {
    if (isset($BSW->Param['ORDER']) AND $BSW->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $BSW->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY BSW_DATUM DESC';
        $BSW->Param['ORDER'] = ' BSW_DATUM DESC ';
    }
} else {
    $BSW->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $BSW->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $BSW->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************
$SQL = 'SELECT ';
$SQL  .=' BSW_BONNR ';
$SQL .=', BSW_BSA';
$SQL .=', BSW_DATUM';
$SQL .=', BSW_EXPORTDATUM';
$SQL .=', BSW_FIL_ID';
$SQL .=', BSW_ZEIT';
$SQL .=', BSW_KARTENNUMMER';
$SQL .=', BSW_KEY';
$SQL .=', BSW_MWST';
$SQL .=', BSW_UMSATZ';
$SQL .=', BSW_FIL_ANSPRECHPARTNER';
$SQL .=', BSW_INFO_DURCH';
$SQL .=', BSW_SONSTIGES';
$SQL .=', BSW_XDI_KEY';
$SQL .=', BSW_IMQ_ID';
$SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
$SQL .= ' FROM BSW ';

if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($AWIS_KEY1 == '') { //Liste?
    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $BSW->Form->Format('N0', $_REQUEST['Block'], false);
        $BSW->Param['BLOCK'] = $Block;
    } elseif (isset($BSW->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $BSW->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $BSW->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $BSW->DB->ErmittleZeilenAnzahl($SQL, $BSW->DB->Bindevariablen('BSW', false));
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $BSW->DB->WertSetzen('BSW', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $BSW->DB->WertSetzen('BSW', 'N0', ($StartZeile + $ZeilenProSeite));
}

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsBSW = $BSW->DB->RecordsetOeffnen($SQL, $BSW->DB->Bindevariablen('BSW', true));
$BSW->Form->DebugAusgabe(1, $BSW->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$BSW->Form->Formular_Start();
echo '<form name=frmbsw action=./bsw_Main.php?cmdAktion=Details method=POST>';

if ($rsBSW->EOF() and $AWIS_KEY1 == '') { //Nichts gefunden!
    $BSW->Form->Hinweistext($BSW->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsBSW->AnzahlDatensaetze() > 1) {// Liste

    $FeldBreiten = array();
    $FeldBreiten['BSW_DATUM'] = 150;
    $FeldBreiten['BSW_KARTENNUMMER'] = 300;
    $FeldBreiten['BSW_BONNR'] = 150;
    $FeldBreiten['BSW_UMSATZ'] = 150;
    $FeldBreiten['BSW_XDI_KEY'] = 50;

    $BSW->Form->ZeileStart();
    $Link = './bsw_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=BSW_DATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'BSW_DATUM'))?'~':'');
    $BSW->Form->Erstelle_Liste_Ueberschrift($BSW->AWISSprachKonserven['BSW']['BSW_DATUM'], $FeldBreiten['BSW_DATUM'], '', $Link);
    $Link = './bsw_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=BSW_KARTENNUMMER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'BSW_KARTENNUMMER'))?'~':'');
    $BSW->Form->Erstelle_Liste_Ueberschrift($BSW->AWISSprachKonserven['BSW']['BSW_KARTENNUMMER'], $FeldBreiten['BSW_KARTENNUMMER'], '', $Link);
    $Link = './bsw_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=BSW_BONNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'BSW_BONNR'))?'~':'');
    $BSW->Form->Erstelle_Liste_Ueberschrift($BSW->AWISSprachKonserven['BSW']['BSW_BONNR'], $FeldBreiten['BSW_BONNR'], '', $Link);
    $Link = './bsw_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=BSW_UMSATZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'BSW_UMSATZ'))?'~':'');
    $BSW->Form->Erstelle_Liste_Ueberschrift($BSW->AWISSprachKonserven['BSW']['BSW_UMSATZ'], $FeldBreiten['BSW_UMSATZ'], '', $Link);
     $BSW->Form->ZeileEnde();

    $DS = 0;
    while (!$rsBSW->EOF()) {
        $HG = ($DS % 2);
        $BSW->Form->ZeileStart();
        $Link = './bsw_Main.php?cmdAktion=Details&BSW_KEY=0' . $rsBSW->FeldInhalt('BSW_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $BSW->Form->Erstelle_ListenFeld('BSW_DATUM', $rsBSW->FeldInhalt('BSW_DATUM'), 0, $FeldBreiten['BSW_DATUM'], false, $HG, '', $Link, 'D');
        $BSW->Form->Erstelle_ListenFeld('BSW_KARTENNUMMER', $rsBSW->FeldInhalt('BSW_KARTENNUMMER'), 0, $FeldBreiten['BSW_KARTENNUMMER'], false, $HG, '', '', 'T', 'L');
        $BSW->Form->Erstelle_ListenFeld('BSW_BONNR', $rsBSW->FeldInhalt('BSW_BONNR'), 0, $FeldBreiten['BSW_BONNR'], false, $HG, '', '', 'T', 'L');
        $BSW->Form->Erstelle_ListenFeld('BSW_UMSATZ', $rsBSW->FeldInhalt('BSW_UMSATZ'), 0, $FeldBreiten['BSW_UMSATZ'], false, $HG, '', '', 'T', 'L');


         $BSW->Form->ZeileEnde();

        $rsBSW->DSWeiter();
        $DS++;
    }

    $Link = './bsw_Main.php?cmdAktion=Details';
    $BSW->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
} else { //Ein Datensatz
    $DetailAnsicht = true;
    $BSW->Form->Erstelle_HiddenFeld('BSW_KEY', $AWIS_KEY1);

    //�ber Post gekommen? In hiddenfeld schreiben, um ihn in die AJAX Calls mit zu �bergeben
    if(isset($_POST)){
        $BSW->Form->Erstelle_HiddenFeld('POST',$BSW->POST2String());
    }else{
        $BSW->Form->Erstelle_HiddenFeld('POST','');
    }

    $Zusatztext = '';
    $Gesperrt = false;
    //Darf der user Kassen und Onlineshop daten nachbearbeiten? Wenn nein, Datensatz auf gesperrt setzen
    if(($BSW->Recht54000&256 )!=256 ){
        $Gesperrt = true;
        $Zusatztext = $BSW->AWISSprachKonserven['BSW']['BSW_ZUR_NACHBEARBEITUNG_GESPERRT'];
    }

    $BSW->Form->ZeileStart();
    switch ($rsBSW->FeldInhalt('BSW_IMQ_ID')){
        case 131072:
            $BSW->Form->Hinweistext($BSW->AWISSprachKonserven['BSW']['BSW_DS_AUS_ONLINE']. ' ' . $Zusatztext);
            break;
        case 262144:
            $BSW->Form->Hinweistext($BSW->AWISSprachKonserven['BSW']['BSW_DS_AUS_KASSE'] . ' ' . $Zusatztext);
            break;
        default:
            $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_MANUELLE_NACHERFASSUNG'],200);
            $Gesperrt = false;
            break;
    }
    $BSW->Form->ZeileEnde();

    //Exportierte Datens�tze d�rfen nicht bearbeitet werden, au�er man hat das n�tige Recht.
    if($rsBSW->FeldInhalt('BSW_EXPORTDATUM')!='' and ($BSW->Recht54000&512) != 512 ){
        $Gesperrt = true;
        $BSW->Form->ZeileStart();
        $BSW->Form->Hinweistext($BSW->AWISSprachKonserven['BSW']['BSW_BEREITS_EXPORTIERT'] . ' ' . $BSW->AWISSprachKonserven['BSW']['BSW_ZUR_NACHBEARBEITUNG_GESPERRT'] );
        $BSW->Form->ZeileEnde();
    }

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./bsw_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $BSW->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsBSW->FeldInhalt('BSW_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsBSW->FeldInhalt('BSW_USERDAT'));
    $BSW->Form->InfoZeile($Felder, '');

    $EditRecht = (($BSW->Recht54000 & 2) != 0);
    //Ajax beim Dokuemtnladen ansto�en
    echo ' <script>';
    echo ' $(document).ready(function(){' . PHP_EOL;
    echo ' key_BSW_KARTENNUMMER(event); ' . PHP_EOL;
    echo ' key_BSW_FIL_ID(event); ' . PHP_EOL;
    echo ' key_BonDetails(event); ' . PHP_EOL;
    echo '});' . PHP_EOL;
    echo '</script>';

    //Kunden
    $BSW->Form->FormularBereichStart();
    $BSW->Form->FormularBereichInhaltStart('Kunden',true,10);
    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_KARTENNUMMER'] . ':', 170);

    $Nummer =$rsBSW->FeldOderPOST('BSW_KARTENNUMMER','T',$POSTbenutzen);
    if(strpos($Nummer,'928000132') === false ){
        $Nummer = '928000132' . $Nummer;
    }
    $BSW->Form->AuswahlBox('!BSW_KARTENNUMMER','box1','','BSK_Daten','txtPOST',220,20,$Nummer,'T',$EditRecht,'','','','','','','',0,'','pattern="[0-9]{19}"');
    $BSW->Form->ZeileEnde();
    $BSW->Form->AuswahlBoxHuelle('box1');
    $BSW->Form->FormularBereichInhaltEnde();
    $BSW->Form->FormularBereichEnde();

    //Filialen
    $BSW->Form->FormularBereichStart();
    $BSW->Form->FormularBereichInhaltStart('Filialdaten',true,10);
    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_FIL_ID'] . ':', 170);
    $BSW->Form->AuswahlBox('!BSW_FIL_ID','box2','','BSW_FIL_ID','txtPOST',110,10,$rsBSW->FeldOderPOST('BSW_FIL_ID','T',$POSTbenutzen),'T',$EditRecht,'','','','','','','',1);
    $BSW->Form->ZeileEnde();

    $BSW->Form->AuswahlBoxHuelle('box2');

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_FIL_ANSPRECHPARTNER'] . ':', 170);
    $BSW->Form->Erstelle_TextFeld('BSW_FIL_ANSPRECHPARTNER',$rsBSW->FeldOderPOST('BSW_FIL_ANSPRECHPARTNER','T',$POSTbenutzen),10,110,true);
    $BSW->Form->ZeileEnde();
    $BSW->Form->FormularBereichInhaltEnde();
    $BSW->Form->FormularBereichEnde();


    //Einkauf
    $BSW->Form->FormularBereichStart();
    $BSW->Form->FormularBereichInhaltStart('Einkauf',true,10);

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_DATUM'],170);
    $BSW->Form->Erstelle_TextFeld('BSW_DATUM',$rsBSW->FeldOderPOST('BSW_DATUM','D'),10,150,true,'','','','D');
    $BSW->Form->ZeileEnde();

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_UMSATZ'],170);
    $BSW->Form->Erstelle_TextFeld('BSW_UMSATZ',$rsBSW->FeldOderPOST('BSW_UMSATZ','T',$POSTbenutzen),10,110,true);
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_MWST'],170);
    $BSW->Form->Erstelle_TextFeld('BSW_MWST',$rsBSW->FeldOderPOST('BSW_MWST','T',$POSTbenutzen),10,110,true);
    $BSW->Form->ZeileEnde();

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_BONNR'],170);
    $BSW->Form->Erstelle_TextFeld('BSW_BONNR',$rsBSW->FeldOderPOST('BSW_BONNR','T',$POSTbenutzen),10,110,true);
    $BSW->Form->ZeileEnde();

    $BSW->Form->FormularBereichInhaltEnde();
    $BSW->Form->FormularBereichEnde();

    //Sonstige Infos
    $BSW->Form->FormularBereichStart();
    $BSW->Form->FormularBereichInhaltStart('Sonstige Infos',false,10);

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_INFO_DURCH'],170);
    $BSW->Form->Erstelle_Textarea('BSW_INFO_DURCH',$rsBSW->FeldOderPOST('BSW_INFO_DURCH','T',$POSTbenutzen),110,30,3,true);
    $BSW->Form->ZeileEnde();

    $BSW->Form->ZeileStart();
    $BSW->Form->Erstelle_TextLabel($BSW->AWISSprachKonserven['BSW']['BSW_SONSTIGES'],170);
    $BSW->Form->Erstelle_Textarea('BSW_SONSTIGES',$rsBSW->FeldInhalt('BSW_SONSTIGES','T',$POSTbenutzen),110,30,3,true);
    $BSW->Form->ZeileEnde();

    $BSW->Form->FormularBereichInhaltEnde();
    $BSW->Form->FormularBereichEnde();

    if($rsBSW->FeldInhalt('BSW_IMQ_ID') == '131072'){
        echo '<div style="display:none;">';
    }
    //Bons in dieser Filiale an diesem Tag
    $BSW->Form->FormularBereichStart();
    $BSW->Form->FormularBereichInhaltStart('Bons in dieser Filiale an diesem Tag',false,10);
    $BSW->Form->ZeileStart();
    $BSW->Form->Schaltflaeche('script','bon_aktuallisieren','onClick="key_BonDetails(event);"','/bilder/cmd_recycling.png','Lade Daten');
    $BSW->Form->ZeileEnde();
    $BSW->Form->AuswahlBox('BonDetails','box3','','BSW_BON_DETAILS','txtBSW_DATUM,sucBSW_FIL_ID',10,10,'','T',true,'','','display:none','','','','',0,'display:none');
    $BSW->Form->AuswahlBoxHuelle('box3');

    $BSW->Form->FormularBereichInhaltEnde();
    $BSW->Form->FormularBereichEnde();
    echo '</div>';

}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$BSW->Form->Formular_Ende();

$BSW->Form->SchaltflaechenStart();

$BSW->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $BSW->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');


    if (isset($Gesperrt) and $Gesperrt === false) {
        $BSW->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $BSW->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    if (($BSW->Recht54000 & 32 ) == 32 ) {       // Hinzuf�gen erlaubt?
        $BSW->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $BSW->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N','',27,27,'',true);
    }
    if (($BSW->Recht54000 & 64 ) == 64  AND isset($Gesperrt) and $Gesperrt === false) {
        $BSW->Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $BSW->AWISSprachKonserven['Wort']['lbl_loeschen'], '');
    }


$BSW->Form->SchaltflaechenEnde();

$BSW->Form->SchreibeHTMLCode('</form>');

?>