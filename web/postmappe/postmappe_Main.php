<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">
    <?php
    require_once('awisDatenbank.inc');
    require_once('awisBenutzer.inc');
    require_once('awisFormular.inc');

    global $AWISCursorPosition;        // Aus AWISFormular

    try {
        $Version = 3;
        $DB = awisDatenbank::NeueVerbindung('AWIS');
        $DB->Oeffnen();
        $AWISBenutzer = awisBenutzer::Init();
        $Form = new awisFormular();
        echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei($Version) . ">";
    } catch (Exception $ex) {
        die($ex->getMessage());
    }

    //Wenn FIL_ID gesetzt oder alle Filialen gedruckt werden sollen
    if (isset($_POST['cmdPDF_x'])) {
        $_GET['XRE'] = 63;
        $_GET['ID'] = 'bla';

        require_once '/daten/web/berichte/drucken.php';
    }

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('TITEL', 'tit_Postmappe');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_reset');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    echo '<title>' . $AWISSprachKonserven['TITEL']['tit_Postmappe'] . '</title>';

    if (isset($_GET['cmdAktion']) and $_GET['cmdAktion'] == 'OffenePos') {
        echo '<meta http-equiv="refresh" content="5">';
    }
    ?>
</head>
<body>
<?php
include("awisHeader$Version.inc");    // Kopfzeile

try {

    if ($AWISBenutzer->HatDasRecht(48000) == 0) {
        $Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', -9, "200812180900");
        die();
    }

    $Register = new awisRegister(48000);
    $Register->ZeichneRegister((isset($_GET['cmdAktion'])?$_GET['cmdAktion']:''));

    $Form->SetzeCursor($AWISCursorPosition);
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180830");
    } else {
        $Form->SchreibeHTMLCode('AWIS: ' . $ex->getMessage());
    }
}
?>
</body>
</html>