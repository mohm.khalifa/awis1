<?php
require_once '/daten/web/mobilitaetsgarantie/mobilitaetsgarantie_funktionen.inc';
require_once 'awisMailer.inc';

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 06.12.2016
 * Time: 09:20
 */
class awis_mobilitaetsgarantien
{
    private $_MOB;

    public $MD5Check = true;

    protected $_DebugText = '';

    protected $_DebugLevel = 0;

    private $_ProtokollDB;

    private $_Maske;

    const ImpPfad = '/win/applicationdata2/AWIS/05-Mobilitaetsgarantien/';

    const MobTemp = '/tmp/mob/';


    function __construct($Maske = false, $Benutzer = '')
    {
        $this->_Maske = $Maske;
        $this->_ProtokollDB = awisDatenbank::NeueVerbindung('AWIS');

        $this->_ProtokollDB->Oeffnen();
        $this->_MOB = new mobilitaetsgarantie_funktionen($Benutzer);
        //Debugs nur, wenn der User das Recht hat

        if (!$this->_MOB->AWISBenutzer->BenutzerOption(1)) {
            $this->DebugLevel(0);
        } else {
            $this->DebugLevel(999);
        }

    }

    function __destruct()
    {
        if ($this->_Maske) {
            $this->_MOB->Form->DebugAusgabe(1, $this->_DebugText);
        } else {
            echo $this->_DebugText;
        }
    }


    /**
     * Setzt oder returnt das Debuglevel
     *
     * @param string $Level
     * @return int|string
     */
    public function DebugLevel($Level = '')
    {
        if ($Level != '') {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }


    /**
     * Generiert einen DebugEintrag
     *
     * @param string $Text
     * @param number $Level
     * @param string $Typ
     */
    public function debugAusgabe($Text, $Level = 1, $Typ = 'Debug')
    {
        if ($Level <= $this->DebugLevel()) {
            $this->_DebugText .= date('d.m.Y H:i:s');
            $this->_DebugText .= ': ' . $Typ;
            $this->_DebugText .= ': ' . $Text;
            $this->_DebugText .= PHP_EOL;
        }
    }

    public function ImportAusMaske($TmpDatei, $UploadName = '')
    {
        if ($UploadName == '') {
            $UploadName = $TmpDatei;
        }

        $Info = pathinfo($TmpDatei);
        $Pfad = $Info['dirname'] . '/';
        $Datei = $Info['filename'];

        switch (mb_strtoupper(substr($UploadName, -4))) {
            case '.CSV':
                //TMP Datei richtig bennenen
                rename($Pfad . $Datei, $Pfad . $UploadName);
                break;
            case '.XLS': //In CSV Umwandeln
                require_once 'awisExcel.inc';
                $AWISExcel = new awisExcel();
                $AWISExcel->Excel2CSV($Pfad . $Datei, awisExcel::DateiFormat_XLS, $Pfad . $UploadName, 0, ';', '');
                break;
            case 'XLSX': //In CSV Umwandeln
                require_once 'awisExcel.inc';
                $AWISExcel = new awisExcel();
                $AWISExcel->Excel2CSV($Pfad . $Datei, awisExcel::DateiFormat_XLSX, $Pfad . $UploadName, 0, ';', '');
                break;
            default:
                return 'MOB_ERR_DATEIFORMAT'; //Unbekanntes Format
        }
        //Fertige CSV Datei, die nun importiert werden kann.
        $Datei = $Pfad . $UploadName;

        //Pruefen ob die Datei schonmal da war
        if ($this->_PruefeImportProtokoll($Datei)) {
            $this->_MOB->DB->TransaktionBegin();

            //Pruefsumme wegschreiben
            $this->_SchreibeImportProtokoll($Datei);

            //Datei �ffnen und Zeilenweise durchgehen
            $fd = fopen($Datei, 'r');
            if (!$fd) {
                $this->_MOB->DB->TransaktionRollback();

                return 'MOB_ERR_OEFFNEN';
            }
            //Ueberschrift in Zeil 1:
            $Zeile = fgets($fd);

            $Zeile = mb_strtoupper($Zeile);
            $Ueberschriften = explode(";", $Zeile);
            $Ueberschriften = array_flip($Ueberschriften);

            //Nachfolgend werden Ueberschriften definiert, die �berhaupt f�r den Import relevant sind. Wenn true = Pflicht
            $Pflicht = array();
            $Pflicht['Name1'] = true;
            $Pflicht['VSNR'] = true;
            $Pflicht['VSBeginn'] = true;
            $Pflicht['VSEnde'] = true;
            $Pflicht['KFZKennz'] = true;

            //Schauen, ob alle Pflicht�berschriften gesetzt sind.
            foreach ($Pflicht as $Feld => $Wert) {
                $Feld = mb_strtoupper($Feld);
                if (!isset($Ueberschriften[$Feld])) {
                    $this->debugAusgabe($Feld . ' fehlt als �berschrift', 1, 'ERROR');

                    return 'MOB_ERR_UEBERSCHRIFT';
                }
            }

            //Datei Zeile f�r Zeile durchgehen
            while (!feof($fd)) {
                $Zeile = fgets($fd);
                $Zeile = utf8_decode($Zeile);
                if (preg_replace('/[^\da-z]/i', '', $Zeile) == '') { //Leerzeilen igonieren
                    continue;
                }

                $Daten = explode(";", $Zeile);

                $SQL = 'INSERT';
                $SQL .= ' INTO MOBILITAETSGARANTIEN';
                $SQL .= '   (';
                $SQL .= '     MOB_NAME1,';
                $SQL .= '     MOB_NAME2,';
                $SQL .= '     MOB_NAME3,';
                $SQL .= '     MOB_FIL_ID,';
                $SQL .= '     MOB_DATUM,';
                $SQL .= '     MOB_STRASSE,';
                $SQL .= '     MOB_POSTLEITZAHL,';
                $SQL .= '     MOB_ORT,';
                $SQL .= '     MOB_LAN_ATUSTAATS_NR,';
                $SQL .= '     MOB_KFZ_KENNZEICHEN,';
                $SQL .= '     MOB_ERSTZULASSUNG,';
                $SQL .= '     MOB_KM_STAND,';
                $SQL .= '     MOB_VERS_NUMMER,';
                $SQL .= '     MOB_VERS_GUELTIG_AB,';
                $SQL .= '     MOB_VERS_GUELTIG_BIS,';
                $SQL .= '     MOB_GESAMTUMSATZ,';
                $SQL .= '     MOB_BEMERKUNG,';
                $SQL .= '     MOB_ATUCARDKUNDE,';
                $SQL .= '     MOB_USER,';
                $SQL .= '     MOB_ANR_ID,';
                $SQL .= '     MOB_TITEL,';
                $SQL .= '     MOB_KFZ_KENNZEICHEN_KOMP,';
                $SQL .= '     MOB_BSA,';
                $SQL .= '     MOB_VORGANGSNUMMER';
                $SQL .= '   )';
                $SQL .= '   VALUES';
                $SQL .= '   (';
                $SQL .= $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['NAME1']], false);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['NAME2']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['NAME3']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['FILIALE']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'DU',
                        @$Daten[$Ueberschriften['DATUM']] . ' ' . substr(@$Daten[$Ueberschriften['ZEIT']], 2, 2) . ':' . substr(@$Daten[$Ueberschriften['ZEIT']], 4,
                            2) . ':' . substr(@$Daten[$Ueberschriften['ZEIT']], 6, 2));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['STRASSE']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['PLZ']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['ORT']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['STAAT_NR']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['KFZKENNZ']], false);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['txtMOB_ERSTZULASSUNG']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['KM']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['VSNR']], false);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'D', @$Daten[$Ueberschriften['VSBEGINN']], false);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'D', @$Daten[$Ueberschriften['VSENDE']], false);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['GESAMTUMSATZ']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['BEMERKUNGEN']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['ATUCARDKUNDE']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_MOB->AWISBenutzer->BenutzerName());
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['ANREDE']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['TITEL']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', awisWerkzeuge::ArtNrKomprimiert(@$Daten[$Ueberschriften['KFZKENNZ']]), false);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['BSA']]);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', @$Daten[$Ueberschriften['VORGANGSNR']]);
                $SQL .= '   )';
                try {
                    $this->_MOB->DB->Ausfuehren($SQL, '', true, $this->_MOB->DB->Bindevariablen('MOB'));
                } catch (awisException $e) {
                    $this->_MOB->DB->TransaktionRollback();
                    if (strpos($e->getMessage(), 'ORA-01400') !== false and strpos($e->getMessage(), 'MOB_KFZ_KENNZEICHEN') !== false) {
                        return 'MOB_ERR_LEERKENNZ';
                    } else {
                        throw $e;
                    }
                }
            }

            $this->_MOB->DB->TransaktionCommit();
        } else {
            return 'MOB_ERR_PRUEFSUMME'; //Pr�fsumme der Datei bereits vorhanden
        }

        return true;
    }

    public function ImportAusSchnittstelle()
    {

        $this->_MOB->DB->TransaktionBegin();

        $Commit = true;
        $Protokoll = array();
        $Protokoll['DSANZ'] = 0;

        $Datei = glob(self::ImpPfad . 'mob*.txt');

        if(count($Datei)){

            $DateiFP = fopen($Datei[0], 'r');

            while (!feof($DateiFP)) {

                $Zeile = fgets($DateiFP);
                if ($Zeile == '') { //Leerzeilen igonieren
                    continue;
                }

                $Start = 1;
                $Laenge = 3;
                $MOB_FIL_ID = intval(substr($Zeile, $Start, $Laenge));

                $Start += $Laenge;
                $Laenge = 8;
                $MOB_DATUM = substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 2; //00

                $Start += $Laenge;
                $Laenge = 2; //Stunde
                $HH = substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 2; //Minute
                $Mi = substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 2; //Sekunde
                $SS = substr($Zeile, $Start, $Laenge);

                $MOB_DATUM .= ' ' . $HH . ':'.$Mi .':'.$SS; //Datum Zeit


                $Start += $Laenge;
                $Laenge = 2;
                $MOB_BSA =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 7;
                $MOB_VORGANGSNUMMER =  intval(substr($Zeile, $Start, $Laenge));

                $Start += $Laenge;
                $Laenge = 1;
                $MOB_ANR_ID =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 19;
                $MOB_TITEL =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 32;
                $MOB_NAME1 =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 32;
                $MOB_NAME2 =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 32;
                $MOB_NAME3 =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 32 + 25;
                $MOB_STRASSE =  substr($Zeile, $Start, $Laenge); //Leerzeichen raus

                $Start += $Laenge;
                $Laenge = 10;
                $MOB_POSTLEITZAHL = substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 25;
                $MOB_ORT =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 3;
                $MOB_LAN_ATUSTAATS_NR =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 20;
                $MOB_KFZ_KENNZEICHEN =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 8;
                $MOB_ERSTZULASSUNG =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 7;
                $MOB_KM_STAND =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 8;
                $MOB_VERS_GUELTIG_AB =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 8;
                $MOB_VERS_GUELTIG_BIS =  substr($Zeile, $Start, $Laenge);

                $Start += $Laenge;
                $Laenge = 10;
                $MOB_GESAMTUMSATZ =  substr($Zeile, $Start, $Laenge)/100;

                $Start += $Laenge;
                $Laenge = 1;
                //Vorgangsart, 1stellig

                $Start += $Laenge;
                $Laenge = 1;
                $MOB_ATUCARDKUNDE = strtoupper( substr($Zeile, $Start, $Laenge))=='J'?1:0;

                $Start += $Laenge;
                $Laenge = 3;
                $MOB_VERS_NUMMER =  substr($Zeile, $Start, $Laenge) . str_pad($MOB_FIL_ID,4,'0', STR_PAD_LEFT) . str_pad($MOB_VORGANGSNUMMER,10,'0', STR_PAD_LEFT);


                $SQL = 'INSERT';
                $SQL .= ' INTO MOBILITAETSGARANTIEN';
                $SQL .= '   (';
                $SQL .= '     MOB_NAME1,';
                $SQL .= '     MOB_NAME2,';
                $SQL .= '     MOB_NAME3,';
                $SQL .= '     MOB_FIL_ID,';
                $SQL .= '     MOB_DATUM,';
                $SQL .= '     MOB_STRASSE,';
                $SQL .= '     MOB_POSTLEITZAHL,';
                $SQL .= '     MOB_ORT,';
                $SQL .= '     MOB_LAN_ATUSTAATS_NR,';
                $SQL .= '     MOB_KFZ_KENNZEICHEN,';
                $SQL .= '     MOB_ERSTZULASSUNG,';
                $SQL .= '     MOB_KM_STAND,';
                $SQL .= '     MOB_VERS_NUMMER,';
                $SQL .= '     MOB_VERS_GUELTIG_AB,';
                $SQL .= '     MOB_VERS_GUELTIG_BIS,';
                $SQL .= '     MOB_GESAMTUMSATZ,';
                $SQL .= '     MOB_ATUCARDKUNDE,';
                $SQL .= '     MOB_USER,';
                $SQL .= '     MOB_ANR_ID,';
                $SQL .= '     MOB_TITEL,';
                $SQL .= '     MOB_KFZ_KENNZEICHEN_KOMP,';
                $SQL .= '     MOB_BSA,';
                $SQL .= '     MOB_VORGANGSNUMMER';
                $SQL .= '   )';
                $SQL .= '   VALUES';
                $SQL .= '   (';
                $SQL .= $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_NAME1));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_NAME2));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_NAME3));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'N0', $this->_trim($MOB_FIL_ID));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'DU', $MOB_DATUM);
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_STRASSE));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_POSTLEITZAHL));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_ORT));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'N0', $this->_trim($MOB_LAN_ATUSTAATS_NR));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_KFZ_KENNZEICHEN));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'D', $this->_trim($MOB_ERSTZULASSUNG));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'N0', $this->_trim($MOB_KM_STAND));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_VERS_NUMMER));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'D', $this->_trim($MOB_VERS_GUELTIG_AB));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'D', $this->_trim($MOB_VERS_GUELTIG_BIS));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'N2', $this->_trim($MOB_GESAMTUMSATZ));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'N0', $this->_trim($MOB_ATUCARDKUNDE));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', 'MOB-Import');
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_ANR_ID));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_TITEL));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim(awisWerkzeuge::ArtNrKomprimiert($MOB_KFZ_KENNZEICHEN)));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_BSA));
                $SQL .= ', ' . $this->_MOB->DB->WertSetzen('MOB', 'T', $this->_trim($MOB_VORGANGSNUMMER));
                $SQL .= '   )';

                try{
                    $Protokoll['DSANZ']++;
                    $this->_MOB->DB->Ausfuehren($SQL, '', true, $this->_MOB->DB->Bindevariablen('MOB'));
                }catch (awisException $e){
                    $Commit = false;
                    echo $e->getMessage();
                    $this->_handleErrors($e,$Zeile);
                }catch (Exception $e){
                    $this->_handleErrors($e,$Zeile);
                    $Commit = false;
                }


            }

            if($Commit) {
                $this->_MOB->DB->TransaktionCommit();
                $this->debugAusgabe('Fertig und commitet! Anzahl Datens�tze ' . $Protokoll['DSANZ'], 1, 'Info');
                $this->_MOB->AWISWerkzeug->EMail('shuttle@de.atu.eu', 'OK MOBI-Import', $this->_DebugText, 1, '', 'awis@de.atu.eu');
                @rename($Datei[0],$Datei[0].'importiert');
            }else{
                $this->_MOB->DB->TransaktionRollback();
                $this->debugAusgabe('Fehler aufgetreten und rollbackt! Logdatei komplett durchlesen! Anzahl Datens�tze ' . $Protokoll['DSANZ'], 1, 'FEHLER');
                $this->_MOB->AWISWerkzeug->EMail('shuttle@de.atu.eu', 'ERROR MOBI-Import', 'RUNTER SCROLLEN!' . PHP_EOL .  $this->_DebugText, 1, '', 'awis@de.atu.eu');
            }

        }else{
            $this->debugAusgabe('Keien Datei gefunden. :(',1,'WARNING');
            $this->_MOB->AWISWerkzeug->EMail('shuttle@de.atu.eu', 'WARNING MOBI-Import', 'Keine Datei zum importieren vorhanden...', 2, '', 'awis@de.atu.eu');
        }
    }

    /**
     * @param $e awisException
     */
    private function _handleErrors($e, $Zusatzinfo = '')
    {
        echo 'Nicht behandelter Fehler aufgetreten! ' . $e->getMessage();

        if($Zusatzinfo!=''){
           echo $Zusatzinfo;
        }

    }

    private function _trim($String){
        $String = preg_replace ('#\s+#' , ' ' , $String);
        $String = trim ($String);
        return $String;
    }

    protected function _PruefeImportProtokoll($Dateiname)
    {

        if ($this->MD5Check == false) {
            return true;
        }

        $SQL = 'SELECT XDI_KEY,';
        $SQL .= '   XDI_BEREICH,';
        $SQL .= '   XDI_DATEINAME,';
        $SQL .= '   XDI_DATUM,';
        $SQL .= '   XDI_MD5,';
        $SQL .= '   XDI_BEMERKUNG,';
        $SQL .= '   XDI_USER,';
        $SQL .= '   XDI_USERDAT';
        $SQL .= ' FROM IMPORTPROTOKOLL ';
        $SQL .= ' WHERE XDI_BEREICH = \'MOB\' ';
        $SQL .= ' AND XDI_MD5 = ' . $this->_ProtokollDB->WertSetzen('XDI', 'T', sha1_file($Dateiname));

        $rsXDI = $this->_ProtokollDB->RecordSetOeffnen($SQL, $this->_ProtokollDB->Bindevariablen('XDI'));

        if ($rsXDI->AnzahlDatensaetze() > 0) {
            $this->debugAusgabe('Laut Pr�fsumme, wurde die Datei ' . $Dateiname . ' bereits importiert. 
            XDI_KEY: ' . $rsXDI->FeldInhalt('XDI_KEY') . ' 
            XDI_DATEINAME: ' . $rsXDI->FeldInhalt('XDI_DATEINAME') . ' 
            XDI_DATUM: ' . $rsXDI->FeldInhalt('XDI_DATUM') . ' 
            Sollte es sich hierbei um einen Fehler handeln, Job erneut mit dem Parameter --ignore md5 aufrufen', 1, 'Fehler');

            return false;
        } else {
            $this->debugAusgabe('Pr�fsumme der Datei unbekannt. ', 999, 'Debug');

            return true;
        }
    }

    /**
     * Schreibt die MD5 ins ImportProtokoll
     *
     * @param $Dateiname
     */
    protected function _SchreibeImportProtokoll($Dateiname)
    {
        
        $SQL = 'INSERT';
        $SQL .= ' INTO IMPORTPROTOKOLL';
        $SQL .= '   (';
        $SQL .= '     XDI_BEREICH,';
        $SQL .= '     XDI_DATEINAME,';
        $SQL .= '     XDI_DATUM,';
        $SQL .= '     XDI_MD5,';
        $SQL .= '     XDI_USER,';
        $SQL .= '     XDI_USERDAT';
        $SQL .= '   )';
        $SQL .= '   VALUES';
        $SQL .= '   (';
        $SQL .= '     \'MOB\'';
        $SQL .= ',' . $this->_ProtokollDB->WertSetzen('XDI', 'T', $Dateiname);
        $SQL .= ', sysdate ';
        $SQL .= ',' . $this->_ProtokollDB->WertSetzen('XDI', 'T', sha1_file($Dateiname));
        $SQL .= ', \'AWIS\'';
        $SQL .= ', sysdate ';
        $SQL .= '   )';

        $this->_ProtokollDB->Ausfuehren($SQL, '', true, $this->_ProtokollDB->Bindevariablen('XDI'));
        $this->debugAusgabe('Importiere Datei:' . $Dateiname, 1, 'Info');
        $SQL = ' select seq_xdi_key.currval from dual';

        return $this->_ProtokollDB->RecordSetOeffnen($SQL)->FeldInhalt(1);
    }

    /**
     * Exportiert alle Mobidaten an den ADAC und legt diese auf dem FTP ab
     */
    public function ExportADACVoll(){

        $MobiArten = array();

        //Temp Ordner leeren
        system('rm '.self::MobTemp.'* -rf');
        @mkdir(self::MobTemp);
        foreach ($MobiArten as $Art){
            $fd = fopen(self::MobTemp.'atu_dat_'.$Art.'_adac.txt','w+');

            //Jede Filiale muss einzeln selektiert werden, damit der Arbeitsspeicher nicht gesprengt wird.
            $SQLFil = 'select distinct MOB_FIL_ID from mobilitaetsgarantien where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
            $Fil = $this->_MOB->DB->RecordSetOeffnen($SQLFil);

            while(!$Fil->EOF()){
                $SQL = $this->_ExportGrundSelect($Art);
                $SQL .= ' WHERE MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('MOB','N0',$Fil->FeldInhalt(1));
                $SQL .= ' AND MOB_VERS_GUELTIG_BIS >= trunc(sysdate)';
                $rsADAC = $this->_MOB->DB->RecordSetOeffnen($SQL,$this->_MOB->DB->Bindevariablen('MOB'));

                while(!$rsADAC->EOF()){
                    $Zeile = '';
                    $Zeile .= str_pad(substr($rsADAC->FeldInhalt('MOB_VERS_NUMMER'),0,18),18,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsADAC->FeldInhalt('MOB_NAME1'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsADAC->FeldInhalt('MOB_NAME2'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsADAC->FeldInhalt('MOB_STRASSE'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsADAC->FeldInhalt('MOB_POSTLEITZAHL'),0,10),10,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsADAC->FeldInhalt('MOB_ORT'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsADAC->FeldInhalt('MOB_KFZ_KENNZEICHEN'),0,11),11,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('Y',strtotime($rsADAC->FeldInhalt('MOB_ERSTZULASSUNG'))),4,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('dmY',strtotime($rsADAC->FeldInhalt('MOB_VERS_GUELTIG_AB'))),8,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('dmY',strtotime($rsADAC->FeldInhalt('MOB_VERS_GUELTIG_BIS'))),10,' ',STR_PAD_RIGHT);
                    $Zeile .= $rsADAC->FeldInhalt('MOB_LAN_ATUSTAATS_NR');
                    $Zeile .=  chr(10);

                    fwrite($fd,$Zeile);

                    $rsADAC->DSWeiter();
                }
                $Update = ' update mobilitaetsgarantien set  MOB_EXPORTSTATUS = 9 where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
                $Update .= ' and MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('UPD','N0',$Fil->FeldInhalt(1));
                $this->_MOB->DB->Ausfuehren($Update,'',true,$this->_MOB->DB->Bindevariablen('UPD'));
                $Fil->DSWeiter();
            }

            fclose($fd);
        }
        $FTPVerbindung = ftp_connect($this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_SERVER'));
        $Login = ftp_login($FTPVerbindung, $this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_USER'), $this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_PASSWORT'));
        ftp_pasv($FTPVerbindung,true);

        if($Login!==false){
            $MOBDateien = scandir(self::MobTemp); //Ordner "files" auslesen
            $RemotePfad = $this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_PFAD');
            foreach ($MOBDateien as $Datei) { // Ausgabeschleife
                if(substr($Datei,0,3)=='atu'){
                    if (ftp_put($FTPVerbindung,$RemotePfad.$Datei ,self::MobTemp .  $Datei, FTP_ASCII)) {
                        $this->debugAusgabe('Datei an ADAC gesendet: ' . $Datei,1,'INFO');
                    } else {
                        $this->debugAusgabe( "Ein Fehler trat beim Hochladen von $Datei auf\n");
                    }
                }
            }
        }else{
            $this->debugAusgabe('Keine FTP Verbindung zum ADAC m�glich',1,'ERROR');
        }

        ftp_close($FTPVerbindung);
    }


    /**
     * Exportiert alle Mobidaten an den ADAC und legt diese auf dem FTP ab
     */
    public function ExportADACVollCSV(){

        $MobiArten = array();
        $Trenner = ';';

        //Temp Ordner leeren
        system('rm '.self::MobTemp.'* -rf');
        @mkdir(self::MobTemp);
        foreach ($MobiArten as $Art){
            $fd = fopen(self::MobTemp.'atu_dat_'.$Art.'_adac.txt','w+');

            //Jede Filiale muss einzeln selektiert werden, damit der Arbeitsspeicher nicht gesprengt wird.
            $SQLFil = 'select distinct MOB_FIL_ID from mobilitaetsgarantien where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
            $Fil = $this->_MOB->DB->RecordSetOeffnen($SQLFil);

            while(!$Fil->EOF()){
                $SQL = $this->_ExportGrundSelect($Art);
                $SQL .= ' WHERE MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('MOB','N0',$Fil->FeldInhalt(1));
                $SQL .= ' AND MOB_VERS_GUELTIG_BIS >= trunc(sysdate)';
                $rsADAC = $this->_MOB->DB->RecordSetOeffnen($SQL,$this->_MOB->DB->Bindevariablen('MOB'));

                while(!$rsADAC->EOF()){
                    $Zeile = '';
                    $Zeile .= $rsADAC->FeldInhalt('MOB_VERS_NUMMER') .$Trenner ;
                    $Zeile .= $rsADAC->FeldInhalt('MOB_NAME1').$Trenner;
                    $Zeile .= $rsADAC->FeldInhalt('MOB_NAME2').$Trenner;
                    $Zeile .= $rsADAC->FeldInhalt('MOB_STRASSE').$Trenner;
                    $Zeile .= $rsADAC->FeldInhalt('MOB_POSTLEITZAHL').$Trenner;
                    $Zeile .= $rsADAC->FeldInhalt('MOB_ORT').$Trenner;
                    $Zeile .= $rsADAC->FeldInhalt('MOB_KFZ_KENNZEICHEN').$Trenner;
                    $Zeile .= date('Y',strtotime($rsADAC->FeldInhalt('MOB_ERSTZULASSUNG'))).$Trenner;
                    $Zeile .= date('dmY',strtotime($rsADAC->FeldInhalt('MOB_VERS_GUELTIG_AB'))).$Trenner;
                    $Zeile .= date('dmY',strtotime($rsADAC->FeldInhalt('MOB_VERS_GUELTIG_BIS'))).$Trenner;
                    $Zeile .= $rsADAC->FeldInhalt('MOB_LAN_ATUSTAATS_NR');
                    $Zeile .=  chr(10);

                    fwrite($fd,$Zeile);

                    $rsADAC->DSWeiter();
                }
                $Update = ' update mobilitaetsgarantien set  MOB_EXPORTSTATUS = 9 where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
                $Update .= ' and MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('UPD','N0',$Fil->FeldInhalt(1));
                $this->_MOB->DB->Ausfuehren($Update,'',true,$this->_MOB->DB->Bindevariablen('UPD'));
                $Fil->DSWeiter();
            }

            fclose($fd);
        }
        $FTPVerbindung = ftp_connect($this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_SERVER'));
        $Login = ftp_login($FTPVerbindung, $this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_USER'), $this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_PASSWORT'));
        ftp_pasv($FTPVerbindung,true);

        if($Login!==false){
            $MOBDateien = scandir(self::MobTemp); //Ordner "files" auslesen
            $RemotePfad = $this->_MOB->AWISBenutzer->ParameterLesen('ADAC_FTP_PFAD');
            foreach ($MOBDateien as $Datei) { // Ausgabeschleife
                if(substr($Datei,0,3)=='atu'){
                    if (ftp_put($FTPVerbindung,$RemotePfad.$Datei ,self::MobTemp .  $Datei, FTP_ASCII)) {
                        $this->debugAusgabe('Datei an ADAC gesendet: ' . $Datei,1,'INFO');
                    } else {
                        $this->debugAusgabe( "Ein Fehler trat beim Hochladen von $Datei auf\n");
                    }
                }
            }
        }else{
            $this->debugAusgabe('Keine FTP Verbindung zum ADAC m�glich',1,'ERROR');
        }

        ftp_close($FTPVerbindung);
    }

    /**
     * Monatlicher Export an den AVD
     */
    public function ExportAVDMonatlich(){
        $MobiArten = array(660,666,667,668, 770,776,777);
        $Anhaenge = array();
        //Temp Ordner leeren
        system('rm '.self::MobTemp.'* -rf');

        foreach ($MobiArten as $Art){
            $fd = fopen(self::MobTemp.'atu_dat_'.$Art.'_avd.txt','w');
            $Anhaenge[] = self::MobTemp.'atu_dat_'.$Art.'_avd.txt';
            //Jede Filiale muss einzeln selektiert werden, damit der Arbeitsspeicher nicht gesprengt wird.
            $SQLFil = 'select distinct MOB_FIL_ID from mobilitaetsgarantien where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
            $Fil = $this->_MOB->DB->RecordSetOeffnen($SQLFil);

            while(!$Fil->EOF()){
                $SQL = $this->_ExportGrundSelect($Art);
                $SQL .= ' WHERE MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('MOB','N0',$Fil->FeldInhalt(1));
                $SQL .= ' AND MOB_VERS_GUELTIG_AB >= ' . $this->_MOB->DB->WertSetzen('MOB','D','01'.$this->_letztesMonat() . $this->_letztesJahr());
                $SQL .= ' AND MOB_VERS_GUELTIG_AB < ' . $this->_MOB->DB->WertSetzen('MOB','D','01'.date('m.Y'));
                $rsAVD = $this->_MOB->DB->RecordSetOeffnen($SQL,$this->_MOB->DB->Bindevariablen('MOB'));

                while(!$rsAVD->EOF()){
                    $Zeile = '';
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_VERS_NUMMER'),0,18),18,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_NAME1'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_NAME2'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_STRASSE'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_POSTLEITZAHL'),0,10),10,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_ORT'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_KFZ_KENNZEICHEN'),0,11),11,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('Y',strtotime($rsAVD->FeldInhalt('MOB_ERSTZULASSUNG'))),4,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('dmY',strtotime($rsAVD->FeldInhalt('MOB_VERS_GUELTIG_AB'))),8,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('dmY',strtotime($rsAVD->FeldInhalt('MOB_VERS_GUELTIG_BIS'))),9,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad($rsAVD->FeldInhalt('MOB_LAN_ATUSTAATS_NR'),2,' ',STR_PAD_LEFT);
                    $Zeile .=  chr(10);

                    fwrite($fd,$Zeile);

                    $rsAVD->DSWeiter();
                }
                $Update = ' update mobilitaetsgarantien set  MOB_EXPORTSTATUS = 9 where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
                $Update .= ' and MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('UPD','N0',$Fil->FeldInhalt(1));
                $this->_MOB->DB->Ausfuehren($Update,'',true,$this->_MOB->DB->Bindevariablen('UPD'));
                $Fil->DSWeiter();
            }

            fclose($fd);
        }
        $this->_EmailAVD($Anhaenge);
    }


    private function _letztesMonat(){
        if((date('m')) == 1){ //Letztes Monat im Dezember? Dann mimm das letzte Jahr
            $Monat = 12;
        }else{
            $Monat =  date('m') - 1;
        }
        return $Monat;
    }

    private function _letztesJahr(){
        if((date('m')) == 1){ //Jetzt Januar? Dann war letztes Monat ein Jahr weniger
            $Jahr =  date('Y') -1;
        }else{
            $Jahr =  date('Y');
        }
        return $Jahr;
    }

    public function ExportAVDDelta(){
        $MobiArten = array(660,666,667,668, 770,776,777);
        $Anhaenge = array();
        //Temp Ordner leeren
        system('rm '.self::MobTemp.'* -rf');

        foreach ($MobiArten as $Art){
            $fd = fopen(self::MobTemp.'atu_dat_'.$Art.'_avd.txt','w');
            $Anhaenge[] = self::MobTemp.'atu_dat_'.$Art.'_avd.txt';
            //Jede Filiale muss einzeln selektiert werden, damit der Arbeitsspeicher nicht gesprengt wird.
            $SQLFil = 'select distinct MOB_FIL_ID from mobilitaetsgarantien where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
            $Fil = $this->_MOB->DB->RecordSetOeffnen($SQLFil);

            while(!$Fil->EOF()){
                $SQL = $this->_ExportGrundSelect($Art);
                $SQL .= ' WHERE MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('MOB','N0',$Fil->FeldInhalt(1));
                $SQL .= ' AND MOB_EXPORTSTATUS  = 1 ';
                $rsAVD = $this->_MOB->DB->RecordSetOeffnen($SQL,$this->_MOB->DB->Bindevariablen('MOB'));

                while(!$rsAVD->EOF()){
                    $Zeile = '';
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_VERS_NUMMER'),0,18),18,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_NAME1'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_NAME2'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_STRASSE'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_POSTLEITZAHL'),0,10),10,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_ORT'),0,32),32,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(substr($rsAVD->FeldInhalt('MOB_KFZ_KENNZEICHEN'),0,11),11,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('Y',strtotime($rsAVD->FeldInhalt('MOB_ERSTZULASSUNG'))),4,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('dmY',strtotime($rsAVD->FeldInhalt('MOB_VERS_GUELTIG_AB'))),8,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad(date('dmY',strtotime($rsAVD->FeldInhalt('MOB_VERS_GUELTIG_BIS'))),9,' ',STR_PAD_RIGHT);
                    $Zeile .= str_pad($rsAVD->FeldInhalt('MOB_LAN_ATUSTAATS_NR'),2,' ',STR_PAD_LEFT);
                    $Zeile .=  chr(10);
                    fwrite($fd,$Zeile);

                    $rsAVD->DSWeiter();
                }
                $Update = ' update mobilitaetsgarantien set  MOB_EXPORTSTATUS = 5 where MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($Art.'%');
                $Update .= ' and MOB_FIL_ID = ' . $this->_MOB->DB->WertSetzen('UPD','N0',$Fil->FeldInhalt(1));
                $this->_MOB->DB->Ausfuehren($Update,'',true,$this->_MOB->DB->Bindevariablen('UPD'));
                $Fil->DSWeiter();
            }

            fclose($fd);
        }
        $this->_EmailAVD($Anhaenge);

    }

    private function _EmailAVD(array $Anhang){
        $Mail = new awisMailer($this->_MOB->DB,$this->_MOB->AWISBenutzer);
        $Mail->Absender($this->_MOB->AWISBenutzer->ParameterLesen('MOB_AVD_ABSENDER'));
        $Mail->AdressListe($Mail::TYP_TO,$this->_MOB->AWISBenutzer->ParameterLesen('MOB_AVD_EMPFAENGER'));
        $Mail->Text($Mail->MailText('MOB_AVD_MAIL','DE',awisMailer::MAILTEXT_TEXT),awisMailer::FORMAT_HTML);
        $Mail->Betreff($Mail->MailText('MOB_AVD_MAIL','DE',awisMailer::MAILTEXT_BETREFF));

        foreach ($Anhang as $Hang){
            $Mail->Anhaenge($Hang,substr($Hang,7));
        }

        $Mail->MailSenden();
    }

    private function _ExportGrundSelect($VersLike){
        $SQL  = 'Select * from (';
        $SQL .='select MOB_KEY ,';
        $SQL .=' MOB_NAME1 ,';
        $SQL .=' MOB_NAME2 ,';
        $SQL .=' MOB_STRASSE ,';
        $SQL .=' MOB_POSTLEITZAHL ,';
        $SQL .=' MOB_ORT ,';
        $SQL .=' MOB_LAN_ATUSTAATS_NR ,';
        $SQL .=' MOB_FIL_ID ,';
        $SQL .=' func_kfz_kennz(MOB_KFZ_KENNZEICHEN) as MOB_KFZ_KENNZEICHEN  ,';
        $SQL .=' MOB_ERSTZULASSUNG ,';
        $SQL .=' MOB_VERS_NUMMER ,';
        $SQL .=' MOB_VERS_GUELTIG_AB ,';
        $SQL .=' MOB_VERS_GUELTIG_BIS ,';
        $SQL .=' MOB_EXPORTSTATUS ';
        $SQL .='  from MOBILITAETSGARANTIEN ';
        $SQL .= ' WHERE MOB_VERS_NUMMER ' . $this->_MOB->DB->LikeOderIst($VersLike.'%');
        $SQL .= ' ORDER BY MOB_VERS_NUMMER';
        $SQL .= ' ) ';

        return $SQL;

    }

}