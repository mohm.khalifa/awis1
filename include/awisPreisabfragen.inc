<?php
/**
 * Abstrakte Klasse f�r die Zuk�ufe
 *
 * @author Sacha Kerres
 * @version 200901
 * @copyright ATU Auto Teile Unger
 *
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');


class awisPreisabfragen
{
	/**
	 * Datenbankverbindung
	 *
	 * @var awisDatenbank
	 */
	protected $_DB = null;

	/**
	 * AWIS Werkzeugobjekt
	 *
	 * @var awisWerkzeuge
	 */
	protected $_AWISWerkzeug = null;

	/**
	 * AWIS Benutzer
	 *
	 * @var awisBenutzer
	 */
	protected $_AWISBenutzer = null;

	/**
	 * Werkzeuge
	 * @var awisWerkzeuge
	 */
	private $_AWISWerkzeuge = null;

	/**
	 * Debugausgaben
	 *
	 * @var int
	 */
	protected $_DebugLevel = 0;

	/**
	 * Lister der Klassifizierungen
	 * @var array
	 */
	private $_Klassifizierungen = array();

	/**
	 * Liste mit Gebieten, aus denen Filialen kommen sollen
	 * @var array
	 */
	private $_Filialebenen = array();

	/**
	 * Liste mit zu verwendenden Herstellern
	 * @var array
	 */
	private $_Hersteller = array();
    /**
     * Filialgruppen-Nr, die verwendet werden sollen
     * @var array
     */
    private $_FilialGruppen = array();

    /**
     * Anzahl der Datens�tze, auf die aufgef�llt werden soll
     * @var int
     */
    private $_AuffuellMenge = 0;
    /**
     * Liste mit Kategorien, die f�r das Auff�llen verwendet werden sollen.
     * @var string
     */
    private $_AuffuellKategorien = array();
    /**
     * Liste mit Rankings, die f�r die Abfragen verwendet werden sollen
     * @var array
     */
	private $_Ranking = array();

    /**
     * Protokolldatei f�r die Ausgaben
     */
	const PROTOKOLLDATEI = '/home/awis/preisabfragen.log';

	/**
	 * Konstruktor
	 *
	 * @param string $Benutzer
	 */
	public function __construct($Benutzer)
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_DB->Oeffnen();

		$this->_AWISWerkzeug = new awisWerkzeuge();
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
	}

	/**
	 * Liest oder setzt den Debuglevel
	 *
	 * @param int $NeuerLevel
	 * @return int
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);

			if($this->_DebugLevel > 0)
			{
				file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'Debuglevel auf '.$this->_DebugLevel.' gesetzt'.PHP_EOL,FILE_APPEND);
			}
		}

		return $this->_DebugLevel;
	}

	/**
	 * Filialebenen f�r die Auswahl von Filialen setzen
	 * @param string $Ebene
	 */
	public function SetzeFilialEbene($Ebene)
	{
		$SQL = 'SELECT FEB_KEY';
		$SQL .= ' FROM FILIALEBENEN';
		$SQL .= ' WHERE FEB_BEZEICHNUNG = '.$this->_DB->FeldInhaltFormat('T',$Ebene);
		$SQL .= ' AND FEB_GUELTIGAB <= SYSDATE AND FEB_GUELTIGBIS >= SYSDATE';
		$rsFEB = $this->_DB->RecordSetOeffnen($SQL);
		if($rsFEB->EOF())
		{
			return false;
		}
		$this->_Filialebenen[]=$rsFEB->FeldInhalt('FEB_KEY');
		return true;
	}

    /**
     * Setzt die Klassifizierung f�r die Abfrage
     * @param int $MBK_KEY  Klassifizierung
     * @param int $Anzahl   Anzahl zu verwendende Mitbewerber
     */
	public function SetzeKlassifizierung($MBK_KEY, $Anzahl)
	{
        if($Anzahl==0)
        {
            if(isset($this->_Klassifizierungen[$MBK_KEY]))
            {
                unset($this->_Klassifizierungen[$MBK_KEY]);
            }
        }
        else

        {
            $this->_Klassifizierungen[$MBK_KEY]=$Anzahl;
        }
	}

    /**
     * Setzt eine Gruppen-ID fuer die Abfrage
     * @param int $GruppenID 
     */
	public function SetzeFilialGruppe($GruppenID)
	{
		$this->_FilialGruppen[]=$this->_DB->FeldInhaltFormat('T',$GruppenID);
	}

	/**
	 * Setzt das Ranking
	 * @param string $RankingGruppe
	 * @param string $RankingVon
	 * @param string $RankingBis
	 */
	public function SetzeRanking($RankingGruppe, $RankingVon=1, $RankingBis=9)
	{
		$this->_Ranking[$RankingGruppe]['min']=$RankingVon;
		$this->_Ranking[$RankingGruppe]['max']=$RankingBis;
	}

    /**
     * Setzt die zu verwendende Auffpllmenge
     * @param int $Anzahl
     */
    public function SetzeAufuellmenge($Anzahl)
    {
        $this->_AuffuellMenge = $Anzahl;
    }

    /**
     * Setzt den zu verwendenen Hersteller bei Vertragswerkst�tten
     * @param int $HER_ID
     */
    public function SetzeHersteller($HER_ID)
    {
    	$this->_Hersteller[] = $HER_ID;
    }
    /**
     * Setze eine Kategorie, die f�r das Auff�llen von Mitbewerberzeilen verwendet werden soll
     * @param int $Kategorie
     */
    public function SetzeAuffuellkategorien($Kategorie)
    {
        $this->_AuffuellKategorien[] = $Kategorie;
    }


	public function ErstelleMitbewerberListe($ID)
	{
		
		if($this->_DebugLevel>50)
		{
			file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'Start der Ermittlung'.PHP_EOL,FILE_APPEND);
		}

        $SQL = 'DELETE FROM PreisabfragenDatenauswahl WHERE PAD_ID = '.$ID;
        $this->_DB->Ausfuehren($SQL);

        // Alle Klassifizierungen durchgehen
        foreach($this->_Klassifizierungen AS $Klassifizierung=>$KlassifizierungenAnzahl)
        {
            $SQL1 = 'SELECT mbw_key, mfi_fil_id, MFR_MRG_KEY, MBW_MBK_KEY, MBK_KENNUNG';
            $SQL1 .= ', ROW_NUMBER() OVER(PARTITION BY mfi_fil_id ORDER BY MFR_RANKING DESC, MFI_ENTFERNUNG ASC) AS Zeile';
            $SQL1 .= ' FROM MitbewerberFilialen';
            $SQL1 .= ' INNER JOIN mitbewerber ON MBW_KEY = MFI_MBW_KEY AND MBW_STATUS = \'A\'';
            $SQL1 .= ' INNER JOIN mitbewerberklassifizierungen ON MBW_MBK_KEY = MBK_KEY';

            // Ranking beachten
            $SQL1 .= ' INNER JOIN mitbewerberfilialenRanking ON MFI_KEY = MFR_MFI_KEY';
            foreach($this->_Ranking AS $Ranking=>$RankingBereich)
            {
                $SQL1 .= ' AND (MFR_MRG_KEY = '.$Ranking.' AND MFR_RANKING >= '.$RankingBereich['min'].' AND MFR_RANKING <= '.$RankingBereich['max'].')';
            }
            
            // HerstellerListe
            if(!empty($this->_Hersteller) AND $Klassifizierung=='28')
            {
            	$SQL1 .= ' INNER JOIN MITBEWERBUTYPEN ON MIU_MBW_KEY = MBW_KEY';
            	$SQL1 .= ' INNER JOIN MITBEWERBUTYPENTYPEN ON MIU_MUT_KEY = MUT_KEY';
            	$SQL1 .= '           AND MUT_HER_ID = '.$this->_DB->FeldInhaltFormat('N0',$this->_Hersteller[0],true).'';
            }
            
            // Nur aktive Filialen nehmen
            $SQL1 .= ' WHERE (SELECT FIF_WERT FROM FILIALINFOS WHERE FIF_FIL_ID = MFI_FIL_ID AND FIF_FIT_ID = 38) IS NULL';
            // Wenn nur eine oder mehrere Filialgruppen gew�nscht sind, einschr�nken
            if(!empty($this->_FilialGruppen))
            {
                $SQL1 .= ' AND EXISTS(SELECT * FROM filialinfos WHERE fif_fil_id = mfi_fil_id AND fif_fit_id = 150 AND fif_wert IN ('.implode(',',$this->_FilialGruppen).'))';
            }
            //$SQL1 .= ' AND MFI_SICHTBAR = 1';
			file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'SQL:'.$SQL.PHP_EOL,FILE_APPEND);
            // TODO: Auswahl nach Filialebene einbauen!

            // Klassifizierung
            $SQL1 .= ' AND MBK_KENNUNG = '.$Klassifizierung;

            
            // Endg�ltiger SQL
            $SQL = ' INSERT INTO PreisabfragenDatenauswahl';
            $SQL .= '(PAD_ID, PAD_FIL_ID, PAD_MBW_KEY, PAD_MRG_KEY, PAD_MBK_KEY, PAD_STATUS)';
            $SQL .= 'SELECT '.$ID.', mfi_fil_id, mbw_key, MFR_MRG_KEY, MBW_MBK_KEY, 1';
            $SQL .= ' FROM ('.$SQL1.') DATEN ';
            $SQL .= ' WHERE zeile <= '.$KlassifizierungenAnzahl;
		            
            if($this->_DebugLevel>90)
            {
                file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'SQL:'.$SQL.PHP_EOL,FILE_APPEND);
            }
            $this->_DB->Ausfuehren($SQL);
		$X = new awisFormular();
		$X->DebugAusgabe(1,$SQL);
            //******************************************************************
            //* Doppelte Daten suchen
            //******************************************************************
            if($this->_DebugLevel>50)
            {
                file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'Doppelte suchen...'.PHP_EOL,FILE_APPEND);
            }

            $SQL = 'SELECT *';
            $SQL .= ' FROM (';
            $SQL .= ' SELECT PAD_KEY, PAD_FIL_ID, PAD_MBW_KEY';
            $SQL .= ' , ROW_NUMBER() OVER(PARTITION BY PAD_MBW_KEY ORDER BY PAD_KEY) as Zeile';
            $SQL .= ' FROM PreisabfragenDatenauswahl';
            $SQL .= ' WHERE PAD_ID = '.$ID;
            $SQL .= ') DATEN';
            $SQL .= ' WHERE zeile > 1';
            if($this->_DebugLevel>90)
            {
                file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'SQL:'.$SQL.PHP_EOL,FILE_APPEND);
            }

            $rsPAD = $this->_DB->RecordSetOeffnen($SQL);
            while(!$rsPAD->EOF())
            {
                if($this->_DebugLevel>50)
                {
                    file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'KEY:'.$rsPAD->FeldInhalt('PAD_KEY').PHP_EOL,FILE_APPEND);
                }
                $SQL = 'UPDATE PreisabfragenDatenauswahl';
                $SQL .= ' SET pad_status = 5';      // raus, weil doppelt
                $SQL .= ' WHERE pad_key = '.$rsPAD->FeldInhalt('PAD_KEY');
                $this->_DB->Ausfuehren($SQL);

                $SQL = ' INSERT INTO PreisabfragenDatenauswahl';
                $SQL .= '(PAD_ID, PAD_FIL_ID, PAD_MBW_KEY, PAD_MRG_KEY, PAD_MBK_KEY, PAD_STATUS)';
                $SQL .= ' SELECT '.$ID.', mfi_fil_id, mbw_key, MFR_MRG_KEY, MBW_MBK_KEY, 2';
                $SQL .= ' FROM ('.$SQL1.') DATEN';
                $SQL .= ' LEFT OUTER JOIN PreisabfragenDatenauswahl ON PAD_MBW_KEY = MBW_KEY';
                $SQL .= ' WHERE PAD_FIL_ID = '.$rsPAD->FeldInhalt('PAD_FIL_ID');
                $SQL .= ' AND PAD_KEY IS NULL';
                if($this->_DebugLevel>50)
                {
                    file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'Alternative:'.$SQL.PHP_EOL,FILE_APPEND);
                }
                $this->_DB->Ausfuehren($SQL);
                $rsPAD->DSWeiter();
            }

            if($this->_DebugLevel>50)
            {
                file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'Doppelte suchen beendet.'.PHP_EOL,FILE_APPEND);
            }

        }


        // Gesammelte Daten mit den Artikeln vermischen und Daten exportieren


        $fp=fopen('/tmp/Datenabfrage_'.$ID.'.csv', 'w+');

        fputcsv($fp,array('##AWIS##','PREISIMPORT','V1'),"\t",PHP_EOL);
        fputcsv($fp,array('PFA_FIL_ID','PFA_AST_ATUNR','PFA_ARTIKELBEZEICHNUNG','PFA_BESCHREIBUNG','PFA_MBW_KEY','MITBEWERBER','PAA_DLNR','PAA_KOMPLETTNR','PAA_GRUPPENID','GRUPPENNAME'),"\t",PHP_EOL);

        $ArtikelListe = array();
        $SQL = 'SELECT PAA_KEY, PAA_HAUPTGRUPPE, PAA_GRUPPE, PAA_AST_ATUNR, AST_BEZEICHNUNGWW FROM (';
        $SQL .= ' SELECT PAA_KEY, PAA_HAUPTGRUPPE, PAA_GRUPPE, COALESCE(PAA_AST_ATUNR,DPR_NUMMER) AS PAA_AST_ATUNR, COALESCE(AST_BEZEICHNUNGWW,DPR_BEZEICHNUNG) AS AST_BEZEICHNUNGWW';
        $SQL .= ' FROM PreisAbfragenDatenArtikel ';
        $SQL .= ' LEFT OUTER JOIN Artikelstamm ON AST_ATUNR = PAA_AST_ATUNR ';
        $SQL .= ' LEFT OUTER JOIN (SELECT DISTINCT DPR_NUMMER, DPR_BEZEICHNUNG FROM Dienstleistungspreise ) DL ON DPR_NUMMER = PAA_AST_ATUNR ';
        $SQL .= ' WHERE PAA_PAP_KEY = '.$ID.'';
        $SQL .= ' ) DATEN WHERE PAA_AST_ATUNR IS NOT NULL';
        $SQL .= '  ORDER BY PAA_HAUPTGRUPPE,PAA_GRUPPE';

        $rsPAA = $this->_DB->RecordSetOeffnen($SQL);
        while(!$rsPAA->EOF())
        {
            $ArtikelListe[$rsPAA->FeldInhalt('PAA_HAUPTGRUPPE')][$rsPAA->FeldInhalt('PAA_GRUPPE')][$rsPAA->FeldInhalt('PAA_AST_ATUNR')]=$rsPAA->FeldInhalt('PAA_KEY');
            
            // Anzahl der Gruppen pro Hauptgruppe
            $AnzGruppen[$rsPAA->FeldInhalt('PAA_HAUPTGRUPPE')] = $rsPAA->FeldInhalt('PAA_GRUPPE');
            $rsPAA->DSWeiter();
        }
        unset($rsPAA);

        // Anzahl der Hauptgruppen
        $Hauptgruppen = sizeof($ArtikelListe);

        // Wenn es mehrere Hauptgruppen gibt, m�ssen die Filialen zuf�llig aufgeteilt werden.
        if($Hauptgruppen>1)
        {
        	// TODO: Wenn das so bleiben kann-> umbauen einen einzelnen SQL!
            $SQL = 'select pad_fil_id, ntile('.$Hauptgruppen.') over(order by dbms_random.value) as gruppe';
            $SQL .= ' FROM (select distinct pad_fil_id from PreisabfragenDatenauswahl WHERE pad_id = '.$ID.') PAD1';
            $rsDaten = $this->_DB->RecordSetOeffnen($SQL);
            while(!$rsDaten->EOF())
            {
                $SQL = 'UPDATE PreisabfragenDatenauswahl';
                $SQL .=' SET PAD_HAUPTGRUPPE = '.$rsDaten->FeldInhalt('GRUPPE');
                $SQL .= ' WHERE PAD_FIL_ID = '.$rsDaten->FeldInhalt('PAD_FIL_ID');
                $this->_DB->Ausfuehren($SQL);

                $rsDaten->DSWeiter();
            }
        }

        // Daten raussuchen mit allen Mitbewerbern
        $SQL = 'SELECT PAD_FIL_ID, PAD_MBW_KEY, MBW_NAME1, PAD_HAUPTGRUPPE ';
        $SQL .= ' FROM PreisAbfragenDatenAuswahl ';
        $SQL .= ' INNER JOIN MITBEWERBER ON PAD_MBW_KEY = MBW_KEY ';
        $SQL .= ' WHERE PAD_ID = '.$ID;
        $SQL .= ' AND PAD_STATUS < 5';          // 5 sind doppelte
        $rsPAD = $this->_DB->RecordSetOeffnen($SQL);

        while(!$rsPAD->EOF())
        {
            $GruppenNr = rand(1,$AnzGruppen[$rsPAD->FeldInhalt('PAD_HAUPTGRUPPE')]);
            //var_dump($rsPAD->FeldInhalt('PAD_HAUPTGRUPPE').$GruppenNr);
            //$bla = (isset($rsPAD->FeldInhalt('PAD_HAUPTGRUPPE')) ? $rsPAD->FeldInhalt('PAD_HAUPTGRUPPE') : '');
       
            foreach($ArtikelListe[$rsPAD->FeldInhalt('PAD_HAUPTGRUPPE')][$GruppenNr] AS $ATUNR=>$PAA_KEY)
            {
				$SQL = 'SELECT PreisabfragenDatenArtikel.*, PFG_BEZEICHNUNG';
				$SQL .= ' ,COALESCE(AST_BEZEICHNUNGWW,DPR_BEZEICHNUNG) AS AST_BEZEICHNUNGWW';
				$SQL .= ' FROM PreisabfragenDatenArtikel';
        		$SQL .= ' LEFT OUTER JOIN Preisabfragengruppen ON PFG_KEY = PAA_PFG_KEY ';
        		$SQL .= ' LEFT OUTER JOIN Artikelstamm ON AST_ATUNR = PAA_AST_ATUNR ';
        		$SQL .= ' LEFT OUTER JOIN (SELECT DISTINCT DPR_NUMMER, DPR_BEZEICHNUNG FROM Dienstleistungspreise ) DL ON DPR_NUMMER = PAA_AST_ATUNR ';
        		$SQL .= ' WHERE PAA_KEY = :var_N0_paa_key';
				$BindeVariablen = array();
				$BindeVariablen['var_N0_paa_key']=$PAA_KEY;
				$rsPAA = $this->_DB->RecordSetOeffnen($SQL, $BindeVariablen);
					            	
                $Daten = $rsPAD->FeldInhalt('PAD_FIL_ID');
                $Daten .= "\t".$ATUNR;
                $Daten .= "\t".trim($rsPAA->FeldInhalt('AST_BEZEICHNUNGWW'));
                $Daten .= "\t".'';
                $Daten .= "\t".$rsPAD->FeldInhalt('PAD_MBW_KEY');
                $Daten .= "\t".$rsPAD->FeldInhalt('MBW_NAME1');
                $Daten .= "\t".$rsPAA->FeldInhalt('PAA_DLNR');
                $Daten .= "\t".$rsPAA->FeldInhalt('PAA_KOMPLETTNR');
                $Daten .= "\t".$rsPAA->FeldInhalt('PAA_PFG_KEY');
                $Daten .= "\t".$rsPAA->FeldInhalt('PFG_BEZEICHNUNG');
                fputs($fp, $Daten.PHP_EOL);
            }
            
            $rsPAD->DSWeiter();
        }
        
        fclose($fp);

		if($this->_DebugLevel>50)
		{
			file_put_contents(self::PROTOKOLLDATEI,date('c').':'.'Ende der Ermittlung'.PHP_EOL,FILE_APPEND);
		}
	}
}


/*
$X = new awisPreisabfragen('skerres');

$X->DebugLevel(99);

$X->SetzeKlassifizierung(21,1);
$X->SetzeKlassifizierung(22,2);
$X->SetzeKlassifizierung(24,1);
$X->SetzeKlassifizierung(28,1);

$X->SetzeFilialEbene('D');

$X->SetzeRanking(1,3,5);

$X->ErstelleMitbewerberListe();
*/