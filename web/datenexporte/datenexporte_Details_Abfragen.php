<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Param;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XDE','%');
	$TextKonserven[]=array('XAB','XAB_BEZEICHNUNG');
	$TextKonserven[]=array('XAB','XAB_VORSCHAU');
	$TextKonserven[]=array('XAB','ttt_Pflichtparameter');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','Feldname');
	$TextKonserven[]=array('Wort','Bedingung');
	$TextKonserven[]=array('Wort','EinmalAbfrage');
	$TextKonserven[]=array('Wort','GespeicherteAbfrage');
	$TextKonserven[]=array('Wort','BedingungHinzufuegen');
	$TextKonserven[]=array('Wort','NeueAbfrage');
	$TextKonserven[]=array('Wort','Ausgabe');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','DateiOeffnenLink');
	$TextKonserven[]=array('Liste','Bedingungen');
	$TextKonserven[]=array('Liste','txtVerknuepfung');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_export*%');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht4001 = $AWISBenutzer->HatDasRecht(4001);
	if(($Recht4001)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	//Code f�r "Alle anchecken"
	$Script = '
	<script>
	$( document ).ready(function() {
        $("#txtcheckAlle").change(function() {
         $(".anzeigen_chk").prop(\'checked\',  $("#txtcheckAlle").prop(\'checked\'));
        });  
    });  
	</script>
			
			
			';
	
	echo $Script;
		
	$Form->ZeileStart();
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Ausgabe'],80);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Feldname'],220);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Bedingung'],642);
	$Form->ZeileEnde();
	
	$SQLAlle  = ' select LISTAGG(XDF_KEY,\',\') within group (order by 1) as ALLEFELDER ';
	$SQLAlle .= ' from EXPORTDATENFELDER where XDF_FILTERFELD = 1 and XDF_XDE_KEY ='.$DB->WertSetzen('CHK', 'N0', $Param['XDE_KEY']);
	$rsChkAlle = $DB->RecordSetOeffnen($SQLAlle,$DB->Bindevariablen('CHK',true));
	
	$Form->ZeileStart(); //Zweite �berschriftenzeile.. 
	ob_start(); //Checkbox in Buffer zeichnen
	$Form->Erstelle_Checkbox('checkAlle', 0, 20,true,1,'','','');
	$Inhalt = ob_get_clean();
	$Form->Erstelle_Liste_Ueberschrift('',20);
	$Form->Erstelle_Liste_Ueberschrift($Inhalt,27);
	$Form->Erstelle_Liste_Ueberschrift('',895);

	
	$Form->ZeileEnde();

	// Exportdaten mit den Abfragen
	$SQL = 'SELECT * ';
	$SQL .= ' FROM ExportDaten ';
	$SQL .= ' LEFT OUTER JOIN ExportAbfragen ON XAB_XDE_KEY = XDE_KEY';
	$SQL .= ' WHERE XAB_KEY = '.$DB->WertSetzen('XDE','N0',$AWIS_KEY1);
	$rsXDE = $DB->RecordSetOeffnen($SQL, $DB->BindeVariablen('XDE'));
	
	$FeldAuswahl = explode(',',$rsXDE->FeldInhalt('XAB_FELDLISTE'));		// Anzuzeigende Felder
	
	
	
	// Die Felder zum Anzeigen
	$SQL = 'SELECT *';
	$SQL .= ' FROM ExportDatenFelder';
	$SQL .= ' WHERE XDF_XDE_KEY = '.$DB->WertSetzen('XDF','N0',$Param['XDE_KEY']);
	$SQL .= ' AND XDF_FILTERFELD = 1';
	$SQL .= ' ORDER BY XDF_SORTIERUNG';
	$rsXDF = $DB->RecordSetOeffnen($SQL, $DB->BindeVariablen('XDF'));

	
	$Bedingungen = explode('|',$AWISSprachKonserven['Liste']['Bedingungen']);
	$LetzterBereich = '';
	
	$Form->ScrollBereichStart('Felder', 250, 942, true, false);
	while(!$rsXDF->EOF())
	{
		$Form->ZeileStart();
		echo '<input type=hidden name=txtXDF_KEY value=0'.$rsXDF->FeldInhalt('XDF_KEY').'>';

		// Auswahl j/n
		$Liste = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
		if($rsXDE->FeldInhalt('XAB_FELDLISTE')=='')
		{
			$Wert = 1;		// Standardwert soll anzeigen sein bei einer neuen Abfrage
		}
		else
		{
			if(in_array($rsXDF->FeldInhalt('XDF_KEY'),$FeldAuswahl))
			{
				$Wert = 1;
			}
			else
			{
				$Wert = 0;
			}
		}
	    $Form->Erstelle_TextLabel('', 23);
		$Form->Erstelle_Checkbox('Anzeigen_'.$rsXDF->FeldInhalt('XDF_KEY'),$Wert,74,true,1,'','','','',true,'','anzeigen_chk');
		
		// Feldname
		if($rsXDF->FeldInhalt('XDF_TEXTKONSERVE')!='')
		{
			$FeldKonserve = explode(':',$rsXDF->FeldInhalt('XDF_TEXTKONSERVE'));
			$TextKonserve = $Form->LadeTexte(array($FeldKonserve));
			$Label = $TextKonserve[$FeldKonserve[0]][$FeldKonserve[1]];
		}
		else
		{
			//$Label = awis_LadeTextKonserven($con,array(array(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3),$rsXDF->FeldInhalt('XDF_FELDNAME'))),$AWISSprache);
			$Label = $Form->LadeTexte(array(array(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3),$rsXDF->FeldInhalt('XDF_FELDNAME'))));
			$Label = $Label[substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3)][$rsXDF->FeldInhalt('XDF_FELDNAME')];
		}
			
		$Form->Erstelle_TextLabel($Label.':',220,(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,2)=='$$'?'font-weight:bold;color:red;':''),'',(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,2)=='$$'?$AWISSprachKonserven['XAB']['ttt_Pflichtparameter']:''));
	
		// Pflichtparameter werden immer als = erfasst
		if(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,2)=='$$')
		{
			echo '<input type="hidden" name="txtBedingung_'.$rsXDF->FeldInhalt('XDF_KEY').'" value="-1">';
			echo '<input type="hidden" name="oldBedingung_'.$rsXDF->FeldInhalt('XDF_KEY').'" value="-1">';
			$Form->Erstelle_TextLabel('=',150);
		}
		else
		{
			// Bedingung
			$Form->Erstelle_SelectFeld('Bedingung_'.$rsXDF->FeldInhalt('XDF_KEY'),'',150,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Bedingungen);
		}
	
		
		switch(substr($rsXDF->FeldInhalt('XDF_DATENQUELLE'),0,3))
		{
			case 'TXT':
				$Felder = explode(':',$rsXDF->FeldInhalt('XDF_DATENQUELLE'));
				$Daten = $Label = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
				$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
				$Form->Erstelle_SelectFeld('Wert_'.$rsXDF->FeldInhalt('XDF_KEY'),'',$rsXDF->FeldInhalt('XDF_SPALTENBREITE'),true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				break;
			case 'SQL':		
				//PG: 11.01.16 Explode zerst�rt SQL, wenn im SQL ein Doppelpunkt vorkommt.
				$Felder = substr($rsXDF->FeldInhalt('XDF_DATENQUELLE'),strpos($rsXDF->FeldInhalt('XDF_DATENQUELLE'),':')+1);				
				//$Felder[1] ersetzt durch $Felder
				$Form->Erstelle_SelectFeld('Wert_'.$rsXDF->FeldInhalt('XDF_KEY'),'',$rsXDF->FeldInhalt('XDF_SPALTENBREITE'),true,$Felder,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
				break;
			default:
				$Form->Erstelle_TextFeld('Wert_'.$rsXDF->FeldInhalt('XDF_KEY'),'',$rsXDF->FeldInhalt('XDF_FELDLAENGE'),$rsXDF->FeldInhalt('XDF_SPALTENBREITE'),true,'','','',$rsXDF->FeldInhalt('XDF_DATENTYP'),'','',$rsXDF->FeldInhalt('XDF_DATENQUELLE'));
				break;
		}
		$Form->ZeileEnde();
			
		$rsXDF->DSWeiter();
			
	}
	
	$Form->ScrollBereichEnde();
	
	$rsXDF->DSErster();

    $Form->ZeileStart();
	$Form->Trennzeile();
    $Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['BedingungHinzufuegen'],0);
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Daten = explode("|",$AWISSprachKonserven['Liste']['txtVerknuepfung']);
	$Form->Erstelle_SelectFeld('BedingungGruppe','AND',60,true,'','','','','',$Daten);;
	$Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Trennzeile();
    $Form->ZeileEnde();

    //******************************************
	//* Bedingungen anzeigen
	//******************************************
	$SQL = 'SELECT *';
	$SQL .= ' FROM ExportAbfragenKriterien';
	$SQL .= ' INNER JOIN ExportDatenFelder ON XAK_XDF_KEY = XDF_KEY';
	$SQL .= ' WHERE XAK_XAB_KEY ='.$DB->WertSetzen('XAK','N0',$AWIS_KEY1);
	$SQL .= ' ORDER BY XAK_Gruppe, XAK_Reihenfolge';
	
	$rsXAK = $DB->RecordSetOeffnen($SQL, $DB->BindeVariablen('XAK'));

	$GruppenNr = 0;
	$GruppenTeile=0;
	$Klammern = 0;
	$DS=0;
	while(!$rsXAK->EOF())
	{
		if($GruppenNr!=$rsXAK->FeldInhalt('XAK_GRUPPE'))
		{
			$Form->ZeileStart();
			$Icons=array();
			$Icons[] = array('delete','./datenexporte_Main.php?cmdAktion=Details&Del='.$rsXAK->FeldInhalt('XAK_GRUPPE').'&XAB_KEY='.$rsXAK->FeldInhalt('XAK_XAB_KEY'));
			$Form->Erstelle_ListeIcons($Icons,34,-1);
			$Form->Erstelle_Checkbox('grp_'.$rsXAK->FeldInhalt('XAK_KEY'),'',20,true,'on');
			$Form->Erstelle_TextLabel('',($Klammern++*40));
			if($DS++>0)	// Nicht beim ersten Block
			{
				$Form->Erstelle_TextLabel($rsXAK->FeldInhalt('XAK_BEDINGUNGGRUPPE') . ' (',100);
			}
			else
			{
				$Form->Erstelle_TextLabel('(',100);
			}
			$Form->ZeileEnde();
	
			$GruppenNr=$rsXAK->FeldInhalt('XAK_GRUPPE');
			$GruppenTeile=0;
		}
		else
		{
			$GruppenTeile++;
		}
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('',($Klammern*40));
		if($rsXAK->FeldInhalt('XDF_TEXTKONSERVE')!='')
		{
			$FeldKonserve = explode(':',$rsXAK->FeldInhalt('XDF_TEXTKONSERVE'));
			$TextKonserve = $Form->LadeTexte(array($FeldKonserve));
			$Label = $TextKonserve[$FeldKonserve[0]][$FeldKonserve[1]];
		}
		else
		{
			$Label = $Form->LadeTexte(array(array(substr($rsXAK->FeldInhalt('XDF_FELDNAME'),0,3),$rsXAK->FeldInhalt('XDF_FELDNAME'))));
			$Label = $Label[substr($rsXAK->FeldInhalt('XDF_FELDNAME'),0,3)][$rsXAK->FeldInhalt('XDF_FELDNAME')];
		}
		if($GruppenTeile>0)
		{
			$Label = $rsXAK->FeldInhalt('XAK_BEDINGUNGGRUPPE').' '.$Label;
		}
		$Form->Erstelle_TextLabel($Label,250);
		$Form->Erstelle_SelectFeld('Bed',$rsXAK->FeldInhalt('XAK_VERKNUEPFUNG'),130,false,'','','','','',$Bedingungen);
	
		$PflichtFeld = ($rsXAK->FeldInhalt('XDF_PFLICHTFELD')==1?'!':'');
		
		switch(substr($rsXAK->FeldInhalt('XDF_DATENQUELLE'),0,3))
		{
			case 'TXT':
				$Felder = explode(':',$rsXAK->FeldInhalt('XDF_DATENQUELLE'));
				$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
				$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
				$Form->Erstelle_SelectFeld('Feld',$rsXAK->FeldInhalt('XAK_WERT'),500,false,'','','','','',$Daten);
				break;
			case 'SQL':
				//$Felder = explode(':',$rsXAK->FeldInhalt('XDF_DATENQUELLE'));
				//PG: 18.01.16 Explode zerst�rt SQL, wenn im SQL ein Doppelpunkt vorkommt.
				$Felder = substr($rsXAK->FeldInhalt('XDF_DATENQUELLE'),strpos($rsXAK->FeldInhalt('XDF_DATENQUELLE'),':')+1);
				$Form->Erstelle_SelectFeld('Feld',$rsXAK->FeldInhalt('XAK_WERT'),500,false,$Felder);
				break;
			default:
				$Wert = ErsetzeParameter($rsXAK->FeldInhalt('XAK_WERT'));
				if($rsXAK->FeldInhalt('XAK_WERT')!=$Wert)
				{
					$Anzeige = $rsXAK->FeldInhalt('XAK_WERT'). '('.$Wert.')';
				}
				else
				{
					$Anzeige = $Wert;
				}
					
				$Form->Erstelle_TextFeld('Feld',$Anzeige,0,500,false,'','','',$rsXAK->FeldInhalt('XDF_DATENTYP'),'','',$rsXAK->FeldInhalt('XDF_DATENQUELLE'));
				break;
		}
	
		$Form->ZeileEnde();
	
		if($GruppenNr!=$rsXAK->FeldInhalt('XAK_GRUPPE'))
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('&nbsp;',($Klammern--*40));
			$Form->Erstelle_TextLabel(')',40);
			$Form->ZeileEnde();
		}
			
		$rsXAK->DSWeiter();
	}

	// Will der Benutzer eine Vorschau w�hrend dem Bearbeiten der Abfrage?
	if($rsXDE->FeldInhalt('XAB_VORSCHAU')==1)
	{
		$Form->Trennzeile();
		
		//*********************************************************
		//* Beispieldaten
		//*********************************************************
		$Abfrage = ErstelleAbfrage($DB, $Param['XDE_KEY'], $AWIS_KEY1);
		$AnzDatensaetze = $DB->ErmittleZeilenanzahl($Abfrage);
	
		//***********************************************************************
		// Liste mit den Feldnamen aufbauen
		//***********************************************************************
		$FeldLabels = array();
		$rsXDF->DSErster();
		while(!$rsXDF->EOF())
		{
			if($rsXDF->FeldInhalt('XDF_TEXTKONSERVE')!='')
			{
				$FeldKonserve = explode(':',$rsXDF->FeldInhalt('XDF_TEXTKONSERVE'));
				$Label = $Form->LadeTexte(array(array($FeldKonserve[0],$FeldKonserve[1])));
				$FeldLabels[$rsXDF->FeldInhalt('XDF_FELDNAME')]=$Label[$FeldKonserve[0]][$FeldKonserve[1]];
			}
			else
			{
				$Label = $Form->LadeTexte(array(array(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3),$rsXDF->FeldInhalt('XDF_FELDNAME'))));
				$FeldLabels[$rsXDF->FeldInhalt('XDF_FELDNAME')]=$Label[substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3)][$rsXDF->FeldInhalt('XDF_FELDNAME')];
			}
	
			$rsXDF->DSWeiter();
		}
	
		$Label = $Form->LadeTexte(array(array('XXX','XXX_ROWNUM')));
		$FeldLabels['XXX_ROWNUM']=$Label['XXX']['XXX_ROWNUM'];
			
		//$SQL = 'SELECT * FROM (SELECT Vorschau.*, row_number() over (order by 1) AS zeile FROM ('.ErstelleAbfrage($DB, $AWIS_KEY1, $AWIS_KEY2).' ) Vorschau) WHERE zeile <= 3';
		$SQL = 'SELECT * FROM (SELECT Vorschau.*, row_number() over (order by 1) AS zeile FROM ('.$Abfrage.' ) Vorschau) WHERE zeile <= 3';
		$rsDaten = $DB->RecordsetOeffnen($SQL);
		$DS=0;
	
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift('Musterdaten', 930);
		$Form->ZeileEnde();
		$Form->ScrollBereichStart('Felder', 200, 932, true, false);
		
		// Spaltenweise Darstellung
		for($i=1;$i<=$rsDaten->AnzahlSpalten();$i++)
		{
			
			$rsDaten->DSErster();
		
			$FeldName = $rsDaten->FeldInfo($i, 'Name');
			if($FeldName!='ZEILE')
				{
				$Form->ZeileStart();
				$Form->Erstelle_ListenFeld('n1',$FeldLabels[$FeldName],0,200,false,($DS%2));

				while(!$rsDaten->EOF())
				{
					$Form->Erstelle_ListenFeld('f1',$rsDaten->FeldInhalt($FeldName),0,200,false,($DS%2));
					$rsDaten->DSWeiter();
				}
				$Form->ZeileEnde();
				$DS++;
			}
		}
			
		$Form->ScrollBereichEnde();
		$Form->Trennzeile();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_AnzahlDSZeilen'].': '.$AnzDatensaetze,0);
		$Form->ZeileEnde();
							
		$rsXAK->DSWeiter();
	}

	$Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S','',27,27);
	if($AWIS_KEY1!=0)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'C','',27,27);
		$Form->Schaltflaeche('href', 'cmdExportCSV', './datenexporte_csv.php?XDE_KEY='.$rsXDE->FeldInhalt('XDE_KEY').'&XAB_KEY='.$rsXDE->FeldInhalt('XAB_KEY'), '/bilder/tabelle.png', $AWISSprachKonserven['Wort']['lbl_export'], 'C','',27,27);
//		$Form->Schaltflaeche('href', 'cmdExportCSV', './datenexporte_csv.php?XDE_KEY='.$rsXDE->FeldInhalt('XDE_KEY').'&XAB_KEY='.$rsXDE->FeldInhalt('XAB_KEY'), '/bilder/cmd_typcsv.png', $AWISSprachKonserven['Wort']['lbl_export'], 'C','',27,27);
		$Form->Schaltflaeche('href', 'cmdExportXLS', './datenexporte_xls.php?XDE_KEY='.$rsXDE->FeldInhalt('XDE_KEY').'&XAB_KEY='.$rsXDE->FeldInhalt('XAB_KEY'), '/bilder/export_xls.png', $AWISSprachKonserven['Wort']['lbl_exportxls'], 'E','',27,27);
//		$Form->Schaltflaeche('href', 'cmdExportXLS', './datenexporte_xls.php?XDE_KEY='.$rsXDE->FeldInhalt('XDE_KEY').'&XAB_KEY='.$rsXDE->FeldInhalt('XAB_KEY'), '/bilder/cmd_typxls.png', $AWISSprachKonserven['Wort']['lbl_exportxls'], 'E','',27,27);
	}
	$Form->SchaltflaechenEnde();
	
	
	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>