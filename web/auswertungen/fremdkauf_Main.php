<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $awisDBFehler;

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,501);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Auswertungen Fremdkauf',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}
	// Wurde ein g�ltiger Parameter angegeben?
		// -> sofort Liste anzeigen
//$ParAktiv=false;
//$ParAktiv=((isset($_REQUEST['OEN_NUMMER']) AND $_REQUEST['OEN_NUMMER']!='')
//			OR (isset($_REQUEST['KBA_NR']) AND $_REQUEST['KBA_NR']!='')
//		 	OR (isset($_REQUEST['LFDNR']) AND $_REQUEST['LFDNR']!='')
//		 	OR (isset($_REQUEST['FIL_ID']) AND $_REQUEST['FIL_ID']!=''));

if((isset($_REQUEST['cmdAktion']) AND $_REQUEST['cmdAktion']=='Liste'))
{
	$cmdAktion = 'Liste';
	awis_RegisterErstellen(51, $con, $cmdAktion);
}
elseif(isset($_REQUEST['cmdAktion']) AND ($_REQUEST['cmdAktion']=='EKAT' OR $_REQUEST['cmdAktion']=='EKATALLE'))
{
	$cmdAktion = 'EKAT';
	awis_RegisterErstellen(51, $con, $cmdAktion);
}
elseif(isset($_REQUEST['cmdAktion']) AND $_REQUEST['cmdAktion']=='Suche')
{
	if((isset($_REQUEST['OEN_NUMMER']) AND $_REQUEST['OEN_NUMMER']!='')
	OR (isset($_REQUEST['KBA_NR']) AND $_REQUEST['KBA_NR']!='')
 	OR (isset($_REQUEST['LFDNR']) AND $_REQUEST['LFDNR']!='')
 	OR (isset($_REQUEST['FIL_ID']) AND $_REQUEST['FIL_ID']!=''))
	{
		$cmdAktion = 'Liste';
	}
	else
	{
		$cmdAktion = '';
	}
	awis_RegisterErstellen(51, $con, $cmdAktion);
}
else
{
	$cmdAktion = isset($_REQUEST['cmdAktion'])?$_REQUEST['cmdAktion']:'';
	awis_RegisterErstellen(51, $con, $cmdAktion);
}

//awis_RegisterErstellen(51, $con, ($ParAktiv?'Liste':''));

if(!isset($_GET['ZurueckLink']) or $_GET['ZurueckLink']=='')
{
	if($ParAktiv)
	{
		print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=history.back();>";
	}
	else
	{
		print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./auswertungen_Main.php';>";
	}
}
else
{
	print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='" . $_GET['ZurueckLink'] . "';>";
}
print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

?>
</body>
</html>

