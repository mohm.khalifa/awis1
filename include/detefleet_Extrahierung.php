<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);

date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class detefleet_Extrahierung 
{
	
    private $_Funktionen;
    private $_DB;
    protected $_AWISBenutzer;
    protected $kurzText = array();
    protected $flagImport = false;
    protected $preisListe = '';

    /**
     * detefleet_Extrahierung constructor.
     * @param bool $flagImport
     * @param string $PreisListeID
     * @throws awisException
     */
    public function __construct($flagImport = false, $PreisListeID = '') {
      $this->_AWISBenutzer = awisBenutzer::Init();
      $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
      $this->_DB->Oeffnen();
      $this->flagImport = $flagImport;
      $this->preisListe = $PreisListeID;
    }

    /**
     * Extrahiert die Importierten Grunddaten
     */
    public function extrahierung() {
        $BindeVariablen=array();
        $BindeVariablen['var_N0_gkp_key']=$this->preisListe;

        $SQL = 'Select GKP_KEY FROM GKPREISLISTE WHERE GKP_KEY=:var_N0_gkp_key';

        $rsGKP_KEY = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        $BindeVariablen=array();
        $BindeVariablen['var_N0_gkz_gkp_key']=$rsGKP_KEY->FeldInhalt('GKP_KEY');

        $SQL = 'Select * from GKPREISLISTEZUORD WHERE GKZ_GKP_KEY=:var_N0_gkz_gkp_key';
        $SQL .= ' AND GKZ_AUTO='.$this->_DB->FeldInhaltFormat('T','j');

        $rsExtrahierung = $this->_DB->RecordSetOeffnen($SQL,$BindeVariablen);

        while(!$rsExtrahierung->EOF()) {

            $KEY = $rsExtrahierung->FeldInhalt('GKZ_KEY');

            $this->kurzText = explode(" ",$rsExtrahierung->FeldInhalt('GKZ_KURZTEXT'));

            if($this->kurzText[0].' '.$this->kurzText[1] == 'Fz Reifen') {
                $lenBreite = strpos($this->kurzText[2],'/');

                $Breite = substr($this->kurzText[2],0,$lenBreite);
                $Querschnitt = substr($this->kurzText[2],$lenBreite + 1);
                $Bauart = $this->kurzText[3];
                $Innendurchmesser = $this->kurzText[4];
                $Blank = $this->kurzText[5];

                if(strpos($this->kurzText[6],'/') == false)
                {
                    $LoadIndex1 = $this->kurzText[6];
                    $LoadIndex2 = '';
                }
                else
                {
                    $lenLoadIndex = strpos($this->kurzText[6],'/');
                    $LoadIndex1 =  substr($this->kurzText[6],0,$lenLoadIndex);
                    $LoadIndex2 = substr($this->kurzText[6],$lenLoadIndex + 1);
                }

                $SP = $this->kurzText[7];
                $GWS = $this->kurzText[8];
                $SR = $this->kurzText[9];


                $SQL =  'UPDATE GKPREISLISTEZUORD SET GKZ_BREITE='.$this->_DB->FeldInhaltFormat('T',$Breite);
                $SQL .= ',GKZ_QUERSCHNITT='.$this->_DB->FeldInhaltFormat('T',$Querschnitt);
                $SQL .= ',GKZ_BAUART='.$this->_DB->FeldInhaltFormat('T',$Bauart);
                $SQL .= ',GKZ_INNENDURCHMESSER='.$this->_DB->FeldInhaltFormat('T',$Innendurchmesser);
                $SQL .= ',GKZ_LOADINDEX1='.$this->_DB->FeldInhaltFormat('T',$LoadIndex1);
                $SQL .= ',GKZ_LOADINDEX2='.$this->_DB->FeldInhaltFormat('T',$LoadIndex2);
                $SQL .= ',GKZ_SPEEDINDEX='.$this->_DB->FeldInhaltFormat('T',$SP);
                $SQL .= ',GKZ_GWS='.$this->_DB->FeldInhaltFormat('T',$GWS);
                if($this->kurzText[9] == "ROF")
                {
                    $SQL .= ',GKZ_ROF='.$this->_DB->FeldInhaltFormat('T',$SR);
                }
                elseif($this->kurzText[9] == "SEAL")
                {
                    $SQL .= ',GKZ_SEAL='.$this->_DB->FeldInhaltFormat('T',$SR);
                }
                $SQL .= ',GKZ_RX='.$this->_DB->FeldInhaltFormat('T','R1');
                $SQL .= ',GKZ_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                $SQL .= ',GKZ_USERDAT=sysdate';
                $SQL .= ' WHERE GKZ_KEY='.$this->_DB->FeldInhaltFormat('NO',$KEY);

                $this->_DB->Ausfuehren($SQL);

            }
            elseif($this->kurzText[0] == 'R1' || $this->kurzText[0] == 'R2' || $this->kurzText[0] == 'R3')
            {
                $RX = $this->kurzText[0];

                if($this->kurzText[1].' '.$this->kurzText[2] == 'Fz Reifen')
                {
                    $lenBreite = strpos($this->kurzText[3],'/');

                    $Breite = substr($this->kurzText[3],0,3);
                    $Querschnitt = substr($this->kurzText[3],4);
                    $Bauart = $this->kurzText[4];
                    $Innendurchmesser = $this->kurzText[5];
                    $Blank = $this->kurzText[6];

                    if(strpos($this->kurzText[7],'/') == false)
                    {
                        $LoadIndex1 = $this->kurzText[7];
                        $LoadIndex2='';
                    }
                    else
                    {
                        $lenLoadIndex = strpos($this->kurzText[7],'/');
                        $LoadIndex1 =  substr($this->kurzText[7],0,$lenLoadIndex);
                        $LoadIndex2 = substr($this->kurzText[7],$lenLoadIndex + 1);
                    }

                    $SP = $this->kurzText[8];
                    $GWS = $this->kurzText[9];
                    $SR = $this->kurzText[10];

                    $SQL =  'UPDATE GKPREISLISTEZUORD SET GKZ_BREITE='.$this->_DB->FeldInhaltFormat('T',$Breite);
                    $SQL .= ',GKZ_QUERSCHNITT='.$this->_DB->FeldInhaltFormat('T',$Querschnitt);
                    $SQL .= ',GKZ_BAUART='.$this->_DB->FeldInhaltFormat('T',$Bauart);
                    $SQL .= ',GKZ_INNENDURCHMESSER='.$this->_DB->FeldInhaltFormat('T',$Innendurchmesser);
                    $SQL .= ',GKZ_LOADINDEX1='.$this->_DB->FeldInhaltFormat('T',$LoadIndex1);
                    $SQL .= ',GKZ_LOADINDEX2='.$this->_DB->FeldInhaltFormat('T',$LoadIndex2);
                    $SQL .= ',GKZ_SPEEDINDEX='.$this->_DB->FeldInhaltFormat('T',$SP);
                    $SQL .= ',GKZ_GWS='.$this->_DB->FeldInhaltFormat('T',$GWS);
                    if($this->kurzText[10] == "ROF")
                    {
                        $SQL .= ',GKZ_ROF='.$this->_DB->FeldInhaltFormat('T',$SR);
                    }
                    elseif($this->kurzText[10] == "SEAL")
                    {
                        $SQL .= ',GKZ_SEAL='.$this->_DB->FeldInhaltFormat('T',$SR);
                    }
                    $SQL .= ',GKZ_RX='.$this->_DB->FeldInhaltFormat('T',$RX);
                    $SQL .= ',GKZ_USER=\''.$this->_AWISBenutzer->BenutzerName().'\'';
                    $SQL .= ',GKZ_USERDAT=sysdate';
                    $SQL .= ' WHERE GKZ_KEY='.$this->_DB->FeldInhaltFormat('NO',$KEY);

                    if($this->_DB->Ausfuehren($SQL)===false) {
                    }
                }
            }elseif($this->kurzText[0].' '.$this->kurzText[1] == 'FZ Leichtmetallfelgen' || $this->kurzText[0].' '.$this->kurzText[1] == 'Fz Leichtmetallfelgen')
            {
                for($i=0;$i<count($this->kurzText);$i++)
                {
                    if(is_numeric($this->kurzText[$i]))
                    {
                        $SQL = 'UPDATE GKPREISLISTEZUORD SET GKZ_INNENDURCHMESSER='.$this->_DB->FeldInhaltFormat('T',$this->kurzText[$i]);
                        $SQL .= ',GKZ_WG='.$this->_DB->FeldInhaltFormat('T','04');
                        $SQL .= ',GKZ_SG='.$this->_DB->FeldInhaltFormat('T','1'.$this->kurzText[$i]);
                        $SQL .= 'WHERE GKZ_KEY='.$this->_DB->FeldInhaltFormat('NO',$KEY);

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }
                    }

                }
            }
            elseif($this->kurzText[0].' '.$this->kurzText[1] == 'Fz Stahlfelgen' || $this->kurzText[0].' '.$this->kurzText[1] == 'FZ Stahlfelgen')
            {
                for($i=0;$i<count($this->kurzText);$i++)
                {
                    if(is_numeric($this->kurzText[$i]))
                    {
                        $SQL = 'UPDATE GKPREISLISTEZUORD SET GKZ_INNENDURCHMESSER='.$this->_DB->FeldInhaltFormat('T',$this->kurzText[$i]);
                        $SQL .= ',GKZ_WG='.$this->_DB->FeldInhaltFormat('T','14');
                        $SQL .= ',GKZ_SG='.$this->_DB->FeldInhaltFormat('T','1'.$this->kurzText[$i]);
                        $SQL .= 'WHERE GKZ_KEY='.$this->_DB->FeldInhaltFormat('NO',$KEY);

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }
                    }
                }
            }
            elseif($this->kurzText[0].' '.$this->kurzText[1] == 'Fz Radkappen')
            {
                for($i=0;$i<count($this->kurzText);$i++)
                {
                    if(is_numeric($this->kurzText[$i]))
                    {
                        $SQL = 'UPDATE GKPREISLISTEZUORD SET GKZ_INNENDURCHMESSER='.$this->_DB->FeldInhaltFormat('T',$this->kurzText[$i]);
                        $SQL .= ',GKZ_WG='.$this->_DB->FeldInhaltFormat('T','07');
                        $SQL .= ',GKZ_SG='.$this->_DB->FeldInhaltFormat('T','5'.$this->kurzText[$i]);
                        $SQL .= 'WHERE GKZ_KEY='.$this->_DB->FeldInhaltFormat('NO',$KEY);

                        if($this->_DB->Ausfuehren($SQL)===false) {
                        }
                    }

                }
            }

            $rsExtrahierung->DSWeiter();
        }

    }

}

?>