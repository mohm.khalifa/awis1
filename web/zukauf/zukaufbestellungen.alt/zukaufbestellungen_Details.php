<?php
global $con;
global $Recht10003;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('ZUB','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('AST','AST_%');
$TextKonserven[]=array('ZLH','ZLH_KUERZEL');
$TextKonserven[]=array('ZLA','ZLA_AST_ATUNR');
$TextKonserven[]=array('Wort','Status');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht10003 = awisBenutzerRecht($con,10003,$AWISBenutzer->BenutzerName());
if($Recht10003==0)
{
    awisEreignis(3,1000,'ZUB',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

//awis_Debug(1,$_POST,$_GET);
//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdSuche_x']))
{
//awis_Debug(1,$_POST);
	$Param = '';
	$Param .= ';'.$_POST['sucZUB_ARTIKELNUMMER'];			//1
	$Param .= ';'.$_POST['sucZUB_ARTIKELBEZEICHNUNG'];		//2
	$Param .= ';'.$_POST['sucZUB_ZLI_KEY'];					//3
	$Param .= ';'.$_POST['sucZUB_ZLH_KEY'];					//4
	$Param .= ';'.$_POST['sucZSZ_SORTIMENT'];				//5
	$Param .= ';'.$_POST['sucAST_ATUNR'];					//6
	$Param .= ';'.$_POST['sucZUB_FIL_ID'];					//7
	$Param .= ';'.$_POST['sucDATUMVOM'];					//8
	$Param .= ';'.$_POST['sucDATUMBIS'];					//9
	$Param .= ';'.$_POST['sucZUB_WANR'];					//10

	awis_BenutzerParameterSpeichern($con, "ZukaufBestellungen" , $AWISBenutzer->BenutzerName() , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() , '');
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	$Param = ';;;;;;;;;';
	include('./zukaufbestellungen_loeschen.php');
	if($AWIS_KEY1==0)
	{
		$Param = awis_BenutzerParameter($con, "ZukaufBestellungen" , $AWISBenutzer->BenutzerName());
	}
	else
	{
		$Param = $AWIS_KEY1;
	}
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	$Param = ';;;';
	include('./zukaufbestellungen_speichern.php');
	$Param = awis_BenutzerParameter($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName());
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$Param = '-1;;;';
}
elseif(isset($_GET['ZUB_KEY']))
{
	$Param = ''.$_GET['ZUB_KEY'].';;;';		// Nur den Key speiechern
}
elseif(isset($_POST['txtZUB_KEY']))
{
	$Param = ''.$_POST['txtZUB_KEY'].';;;';		// Nur den Key speiechern
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	$Param='';
	if(!isset($_GET['Liste']))
	{
		$Param = awis_BenutzerParameter($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName());
	}
	else
	{
		awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() , '');
	}
	if($Param=='' OR $Param=='0')
	{
		$Param = awis_BenutzerParameter($con, 'ZukaufBestellungen', $AWISBenutzer->BenutzerName());
	}
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = '';
$Param = explode(';',$Param);

// Name angegeben?
if($Param[0]!='')		// Key
{
	$Bedingung .= 'AND ZUB_Key = ' . intval($Param[0]) . ' ';
}
if(isset($Param[1]) AND $Param[1]!='')
{
	$Bedingung .= 'AND (UPPER(ZUB_ARTIKELNUMMER) ' . awisLIKEoderIST($Param[1].'%',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[2]) AND $Param[2]!='')
{
	$Bedingung .= 'AND (UPPER(ZUB_ARTIKELBEZEICHNUNG) ' . awisLIKEoderIST($Param[2].'%',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[3]) AND $Param[3]>0)
{
	$Bedingung .= 'AND ZUB_ZLI_KEY = ' . intval($Param[3]) . ' ';
}
if(isset($Param[4]) AND $Param[4]!='')
{
	$Bedingung .= 'AND ZUB_HERSTELLER = ' .awis_FeldInhaltFormat('T',$Param[4]) . ' ';
}
if(isset($Param[5]) AND $Param[5]!='')
{
	$Bedingung .= 'AND ZUB_Sortiment = ' . awis_FeldInhaltFormat('T',$Param[5]) . ' ';
}
if(isset($Param[6]) AND $Param[6]!='')
{
	$Bedingung .= 'AND (UPPER(ZLA_AST_ATUNR) ' . awisLIKEoderIST($Param[6].'%',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[7]) AND $Param[7]!='')
{
	$Bedingung .= 'AND ZUB_FIL_ID = 0' . intval($Param[7]) . ' ';
}
if(isset($Param[8]) AND $Param[8]!='')
{
	$Bedingung .= 'AND ZUB_BESTELLDATUM >= ' . awisFeldFormat('DU',$Param[8]) . ' ';
}
if(isset($Param[9]) AND $Param[9]!='')
{
	$Bedingung .= 'AND ZUB_BESTELLDATUM <= ' . awisFeldFormat('DU',$Param[9]) . ' ';
}
if(isset($Param[10]) AND $Param[10]!='')
{
	$Bedingung .= 'AND ZUB_WANR ' . awisLIKEoderIST($Param[10]) . ' ';
}

$SQL = 'SELECT DISTINCT ZukaufBestellungen.*, vZukaufArtikel.*, TEMOT_NR, ZWA_VK, FIL_BEZ';
$SQL .= ' FROM ZukaufBestellungen ';
$SQL .= ' LEFT OUTER JOIN vZukaufArtikel ON ZUB_ArtikelNummer = ZLA_Artikelnummer AND ZUB_HERSTELLER = ZLH_KUERZEL AND ZUB_ZLI_KEY = ZLA_ZLI_KEY';
$SQL .= ' LEFT OUTER JOIN TEMOT_BESTELL@FILSYS.ATU.DE FILSYS ON BESTELL_KEY = ZUB_KEY';
$SQL .= ' LEFT OUTER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
$SQL .= ' LEFT OUTER JOIN FILIALEN ON FIL_ID = ZUB_FIL_ID';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY ZUB_BESTELLDATUM DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
awis_Debug(1,$SQL);
if(awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'")===false)
{
	echo '####';
}

$rsZUB = awisOpenRecordset($con, $SQL);
$rsZUBZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
if($rsZUBZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>'.$AWISSprachKonserven['Fehler']['err_keineDaten'].'</span>';
}
elseif($rsZUBZeilen>1 AND !isset($_GET['ZUB_KEY']))						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZUB_BESTELLDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_BESTELLDATUM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'],160,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZUB_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_WANR'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_WANR'],130,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZUB_BESTELLMENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_BESTELLMENGE'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'],90,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZUB_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_ARTIKELNUMMER'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'],180,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],60,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZUB_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_ARTIKELBEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_ARTIKELBEZEICHNUNG'],390,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZUB_PREISBRUTTO'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUB_PREISBRUTTO'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'],90,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],60,'','',$AWISSprachKonserven['ZUB']['ttt_ZUBSTATUS']);
	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZUBZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZUBZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./zukaufbestellungen_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	for($ZUBZeile=$StartZeile;$ZUBZeile<$rsZUBZeilen and $ZUBZeile<$StartZeile+$MaxDSAnzahl;$ZUBZeile++)
	{
		$Status = '';
		awis_FORM_ZeileStart();

		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=0'.$rsZUB['ZUB_KEY'][$ZUBZeile];
		awis_FORM_Erstelle_ListenFeld('ZUB_BESTELLDATUM',$rsZUB['ZUB_BESTELLDATUM'][$ZUBZeile],0,160,false,($ZUBZeile%2),'',$Link,'DU');

		$Link = '';
		if($rsZUB['ZUB_WANRKORR'][$ZUBZeile]=='')
		{
			awis_FORM_Erstelle_ListenFeld('ZUB_WANR',$rsZUB['ZUB_WANR'][$ZUBZeile],0,130,false,($ZUBZeile%2),'',$Link,'T');
		}
		else
		{
			awis_FORM_Erstelle_ListenFeld('ZUB_WANR',$rsZUB['ZUB_WANRKORR'][$ZUBZeile],0,130,false,($ZUBZeile%2),'color:red;',$Link,'T','',$rsZUB['ZUB_WANR'][$ZUBZeile]);
		}

		$Link = '';
		awis_FORM_Erstelle_ListenFeld('ZUB_BESTELLMENGE',$rsZUB['ZUB_BESTELLMENGE'][$ZUBZeile],0,90,false,($ZUBZeile%2),'',$Link,'N3','R');

		$Link = '../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY=0'.$rsZUB['ZLA_KEY'][$ZUBZeile];
		awis_FORM_Erstelle_ListenFeld('ZUB_ARTIKELNUMMER',$rsZUB['ZUB_ARTIKELNUMMER'][$ZUBZeile],0,180,false,($ZUBZeile%2),'',$Link);

		$Link='';	// TODO: Sp�ter auf die Hersteller
		awis_FORM_Erstelle_ListenFeld('ZLH_KUERZEL',$rsZUB['ZLH_KUERZEL'][$ZUBZeile],0,60,false,($ZUBZeile%2),'',$Link,'T','L',$rsZUB['ZLH_BEZEICHNUNG'][$ZUBZeile]);
		$Link='';
		awis_FORM_Erstelle_ListenFeld('ZUB_ARTIKELBEZEICHNUNG',$rsZUB['ZUB_ARTIKELBEZEICHNUNG'][$ZUBZeile],0,390,false,($ZUBZeile%2),'',$Link);

		$TippText = $AWISSprachKonserven['ZUB']['ZUB_PREISANGELIEFERT'].':'.awisFeldFormat('N2',$rsZUB['ZUB_PREISANGELIEFERT'][$ZUBZeile]);
		$Link='';

		awis_FORM_Erstelle_ListenFeld('ZUB_PREISBRUTTO',$rsZUB['ZUB_PREISBRUTTO'][$ZUBZeile],0,90,false,($ZUBZeile%2),'',$Link,'N2','',$TippText);

		//****************************************
		// Status
		//****************************************
		// Abweichungen berechnen
		if($rsZUB['ZUB_PREISANGELIEFERT'][$ZUBZeile]!='')
		{
			$Abweichung = (awisFeldFormat('N2',$rsZUB['ZUB_PREISBRUTTO'][$ZUBZeile]) - awisFeldFormat('N2',$rsZUB['ZUB_PREISANGELIEFERT'][$ZUBZeile]))/awisFeldFormat('N2',$rsZUB['ZUB_PREISANGELIEFERT'][$ZUBZeile]);
			if(abs($Abweichung) > 0.3)
			{
				$Status .= 'P';
			}
			elseif(abs($Abweichung) > 0.2)
			{
				$Status .= 'p';
			}
		}
		// Gibt es einen ATU Artikel
		if($rsZUB['ZLA_AST_ATUNR'][$ZUBZeile]!='')
		{
			$Status .= 'A';
		}
		// Ist die Bestellung bei den Filialen zu sehen?
		if($rsZUB['TEMOT_NR'][$ZUBZeile]=='')
		{
			$Status .= '!';
		}
		// Ist die Bestellung abgerechnet?
		if($rsZUB['ZWA_VK'][$ZUBZeile]!='')
		{
			$Status .= 'B';
		}

		$TippText = '';
		awis_FORM_Erstelle_ListenFeld('STATUS',$Status,0,60,false,($ZUBZeile%2),'','','T','',$TippText);

		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Bestellung
{
	echo '<form name=frmZukaufBestellungen action=./zukaufbestellungen_Main.php?cmdAktion=Details method=POST>';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsZUB['ZUB_KEY'][0])?$rsZUB['ZUB_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() , $AWIS_KEY1);

	echo '<input type=hidden name=txtZUB_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufbestellungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZUB['ZUB_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZUB['ZUB_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$EditRecht=(($Recht10003&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=($Recht10003&6);
	}


	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLI_KEY'].':',200);
	if($EditRecht)
	{
		$CursorFeld='txtZUB_ZLI_KEY';
	}
	$SQL = 'SELECT ZLI_KEY, ZLI_BEZEICHNUNG FROM ZUKAUFLIEFERANTEN';
	$SQL .= ' ORDER BY ZLI_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ZUB_ZLI_KEY',($AWIS_KEY1===0?'':$rsZUB['ZUB_ZLI_KEY'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_EXTERNEID'].':',190);
	awis_FORM_Erstelle_TextFeld('ZUB_EXTERNEID',($AWIS_KEY1===0?'':$rsZUB['ZUB_EXTERNEID'][0]),20,200,false,'','','','T');
	awis_FORM_ZeileEnde();

		// Bestellinformationen
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_BESTELLDATUM'].':',200);
	awis_FORM_Erstelle_TextFeld('ZUB_BESTELLDATUM',($AWIS_KEY1===0?'':$rsZUB['ZUB_BESTELLDATUM'][0]),20,200,$EditRecht,'','','','DU');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_FIL_ID'].':',190);
	awis_FORM_Erstelle_TextFeld('ZUB_FIL_ID',($AWIS_KEY1===0?'':$rsZUB['ZUB_FIL_ID'][0]),20,200,$EditRecht,'','','','T');
	$Link = '/filialen/filialinfo_Main.php?cmdAktion=Filialinfos&FIL_ID='.($AWIS_KEY1===0?'':$rsZUB['ZUB_FIL_ID'][0]);
	awis_FORM_Erstelle_TextFeld('#FIL_BEZ',($AWIS_KEY1===0?'':$rsZUB['FIL_BEZ'][0]),20,200,false,'','',$Link,'T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANR'].':',200);
 	awis_FORM_Erstelle_TextFeld('ZUB_WANR',($AWIS_KEY1===0?'':$rsZUB['ZUB_WANR'][0]),20,200,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANRKORR'].':',190);
 	awis_FORM_Erstelle_TextFeld('ZUB_WANRKORR',($AWIS_KEY1===0?'':$rsZUB['ZUB_WANRKORR'][0]),20,200,$EditRecht,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_VERSANDARTBEZ'].':',200);
	awis_FORM_Erstelle_TextFeld('ZUB_VERSANDARTBEZ',($AWIS_KEY1===0?'':$rsZUB['ZUB_VERSANDARTBEZ'][0]),20,200,$EditRecht,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WAEHRUNG'].':',190);
	awis_FORM_Erstelle_TextFeld('ZUB_WAEHRUNG',($AWIS_KEY1===0?'':$rsZUB['ZUB_WAEHRUNG'][0]),10,200,$EditRecht,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_BESTELLMENGE'].':',200);
	awis_FORM_Erstelle_TextFeld('ZUB_BESTELLMENGE',($AWIS_KEY1===0?'':$rsZUB['ZUB_BESTELLMENGE'][0]),10,200,$EditRecht,'','','','N3');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_MENGENEINHEITEN'].':',190);
	awis_FORM_Erstelle_TextFeld('ZUB_MENGENEINHEITEN',($AWIS_KEY1===0?'':$rsZUB['ZUB_MENGENEINHEITEN'][0]),10,200,$EditRecht,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_EINHEITENPROPREIS'].':',190);
	awis_FORM_Erstelle_TextFeld('ZUB_EINHEITENPROPREIS',($AWIS_KEY1===0?'':$rsZUB['ZUB_EINHEITENPROPREIS'][0]),10,200,$EditRecht,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_PREISBRUTTO'].':',200);
	awis_FORM_Erstelle_TextFeld('ZUB_PREISBRUTTO',($AWIS_KEY1===0?'':$rsZUB['ZUB_PREISBRUTTO'][0]),10,200,$EditRecht,'','','','N2');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_PREISANGELIEFERT'].':',190);
	awis_FORM_Erstelle_TextFeld('ZUB_PREISANGELIEFERT',($AWIS_KEY1===0?'':$rsZUB['ZUB_PREISANGELIEFERT'][0]),10,200,$EditRecht,'','','','N2');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ALTTEILWERT'].':',190);
	awis_FORM_Erstelle_TextFeld('ZUB_ALTTEILWERT',($AWIS_KEY1===0?'':$rsZUB['ZUB_ALTTEILWERT'][0]),10,200,$EditRecht,'','','','N2');
	awis_FORM_ZeileEnde();

		// Nummer und Hersteller
	awis_FORM_ZeileStart();
	$Link = '/zukauf/zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUB['ZUB_ZLA_KEY'][0];
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELNUMMER'].':',200,'',$Link);
	awis_FORM_Erstelle_TextFeld('ZUB_ARTIKELNUMMER',($AWIS_KEY1===0?'':$rsZUB['ZUB_ARTIKELNUMMER'][0]),20,200,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ZLH_KEY'].':',190);
	$SQL = 'SELECT ZLH_KUERZEL, ZLH_KUERZEL || \' - \' || ZLH_BEZEICHNUNG AS ANZEIGE FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_KUERZEL, ZLH_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ZUB_HERSTELLER',($AWIS_KEY1===0?'':$rsZUB['ZUB_HERSTELLER'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_ZeileEnde();

		// Bezeichnung
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ARTIKELBEZEICHNUNG'].':',200);
	awis_FORM_Erstelle_TextFeld('ZUB_ARTIKELBEZEICHNUNG',($AWIS_KEY1===0?'':$rsZUB['ZUB_ARTIKELBEZEICHNUNG'][0]),50,200,$EditRecht);
	awis_FORM_ZeileEnde();

		// ATU Daten
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_SORTIMENT'].':',200);
	$SQL = 'SELECT DISTINCT ZSZ_SORTIMENT AS KEY, ZSZ_SORTIMENT AS SORTIMENT FROM ZUKAUFSORTIMENTZUORDNUNGEN';
	$SQL .= ' WHERE ZSZ_ZLI_KEY = 0'.(isset($rsZUB['ZUB_ZLI_KEY'][0])?$rsZUB['ZUB_ZLI_KEY'][0]:'1');
	$SQL .= ' ORDER BY ZSZ_SORTIMENT';
	awis_FORM_Erstelle_SelectFeld('ZUB_SORTIMENT',($AWIS_KEY1===0?'':$rsZUB['ZUB_SORTIMENT'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_TAUSCHTEIL'].':',190);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	awis_FORM_Erstelle_SelectFeld('ZUB_TAUSCHTEIL',($AWIS_KEY1===0?'':$rsZUB['ZUB_TAUSCHTEIL'][0]),200,$EditRecht,null,'','','',1,'',$Daten);
	awis_FORM_ZeileEnde();

			// Abschluss
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ABSCHLUSSART'].':',200);
	$Liste = explode("|",$AWISSprachKonserven['ZUB']['lst_ZUB_ABSCHLUSSART']);
	awis_FORM_Erstelle_SelectFeld('ZUB_ABSCHLUSSART',($AWIS_KEY1===0?'':$rsZUB['ZUB_ABSCHLUSSART'][0]),200,$EditRecht,$con,'','','3','','',$Liste,'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_ABSCHLUSSGRUND'].':',200);
	awis_FORM_Erstelle_TextFeld('ZUB_ABSCHLUSSGRUND',($AWIS_KEY1===0?'':$rsZUB['ZUB_ABSCHLUSSGRUND'][0]),50,200,$EditRecht);
	awis_FORM_ZeileEnde();

	if(isset($rsZUB['ZLA_AST_ATUNR'][0]) AND $rsZUB['ZLA_AST_ATUNR'][0]!='')
	{
		awis_FORM_Trennzeile();

		$SQL = 'SELECT Artikelstamm.* ';
		$SQL .= ',(SELECT ATW_BETRAG FROM Altteilwerte WHERE ATW_KENNUNG = (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND asi_ast_atunr=\''.$rsZUB['ZLA_AST_ATUNR'][0].'\' AND asi_ait_id=190) AND ATW_LAN_CODE=\'DE\') AS AST_ALTTEILWERT';
		$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND  asi_ast_atunr=\''.$rsZUB['ZLA_AST_ATUNR'][0].'\' AND asi_ait_id=191) AS AST_VPE';
		$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR='.awisFeldFormat('T',$rsZUB['ZLA_AST_ATUNR'][0]);
		$rsAST = awisOpenRecordset($con,$SQL);
		if($awisRSZeilen==0)
		{
		}
		else
		{
			awis_FORM_ZeileStart();
			$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsAST['AST_ATUNR'][0].'&cmdAktion=ArtikelInfos';
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'');
			awis_FORM_Erstelle_TextFeld('AST_ATUNR',$rsAST['AST_ATUNR'][0],20,200,false,'','',$Link);
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_BEZEICHNUNGWW',$rsAST['AST_BEZEICHNUNGWW'][0],20,500,false);
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_VK',$rsAST['AST_VK'][0],20,200,false,'','','','N2');
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ALTTEILWERT'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_ALTTEILWERT',$rsAST['AST_ALTTEILWERT'][0],20,200,false,'','','','T');
			awis_FORM_ZeileEnde();

			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VPE'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_VPE',$rsAST['AST_VPE'][0],20,200,false,'','','','N0');
			awis_FORM_ZeileEnde();
		}
	}

	awis_FORM_FormularEnde();


	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht10003&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if(($Recht10003&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht10003&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}
	awis_FORM_SchaltflaechenEnde();


	echo '</form>';
}

//awis_Debug(1, $Param, $Bedingung, $rsZUB, $_POST, $rsAZG, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}