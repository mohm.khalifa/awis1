<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZSA','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','wrt_alle');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');


	$Form = new awisFormular();
	$Form->Formular_Start();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10006 = $AWISBenutzer->HatDasRecht(10006);
	if($Recht10006==0)
	{
	    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$DetailAnsicht=false;
	$MaxDSAnzahl = 1;

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./zukaufbestellungen_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukaufbestellungen_speichern.php');
	}

	//********************************************************
	// Daten suchen
	//********************************************************

	$SQL = 'SELECT  ZukaufSonderanfragen.*, LIE_NAME1';
	$SQL .= ' FROM ZukaufSonderanfragen ';
	$SQL .= ' LEFT OUTER JOIN Lieferanten ON ZSA_FIRMA = LIE_NR';

	if(isset($_POST['cmdDSNeu_x']))
	{
		$SQL .= ' WHERE ZSA_KEY is null ';
	}

	if(!isset($_GET['Sort']))
	{
		$SQL .= ' ORDER BY ZSA_USERDAT ASC';
	}

	$rsZSA = $DB->RecordSetOeffnen($SQL);

	echo '<form name=frmZukaufSonderanfragen action=./zukaufbestellungen_Main.php?cmdAktion=Sonderanfragen method=POST>';

//********************************************************
// Daten anzeigen
//********************************************************
if(!$rsZSA->EOF() OR isset($_GET['Liste']))						// Liste anzeigen
{

	$Form->ZeileStart();
	$Link = '';
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_WANR'],240,'',$Link);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_DATUM_VON'],150,'',$Link);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_DATUM_BIS'],150,'',$Link);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_FIRMA'],130,'',$Link);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_USERDAT'],200,'',$Link);
	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZSA']['ZSA_USER'],200,'',$Link);
	$Form->ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

	$DS=0;
	while(!$rsZSA->EOF())
	{
		$Status = '';
		$Form->ZeileStart();

		$Link = '';
		$Form->Erstelle_ListenFeld('ZSA_WANR',$rsZSA->FeldInhalt('ZSA_WANR'),0,240,false,($DS%2),'',$Link,'T');
		$Form->Erstelle_ListenFeld('ZSA_DATUM_VON',$rsZSA->FeldInhalt('ZSA_DATUM_VON','D'),0,150,false,($DS%2),'',$Link,'T');
		$Form->Erstelle_ListenFeld('ZSA_DATUM_BIS',$rsZSA->FeldInhalt('ZSA_DATUM_BIS','D'),0,150,false,($DS%2),'',$Link,'T');

		$Link = '';
		$Form->Erstelle_ListenFeld('ZSA_FIRMA',($rsZSA->FeldInhalt('LIE_NAME1')==''?$AWISSprachKonserven['Wort']['wrt_alle']:$rsZSA->FeldInhalt('LIE_NAME1')),0,130,false,($DS%2),'',$Link,'T','',$rsZSA->FeldInhalt('ZSA_FIRMA'));
		$Form->Erstelle_ListenFeld('ZSA_USERDAT',$rsZSA->FeldInhalt('ZSA_USERDAT'),0,200,false,($DS%2),'',$Link,'DU');
		$Form->Erstelle_ListenFeld('ZSA_USERDAT',$rsZSA->FeldInhalt('ZSA_USER'),0,200,false,($DS%2),'',$Link,'T');

		$Form->ZeileEnde();
		$rsZSA->DSWeiter();
		$DS++;
	}

}			// Eine einzelne Adresse
elseif(($Recht10006&2)!==0)											// Eine einzelne oder neue Bestellung
{
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufbestellungen_Main.php?cmdAktion=Sonderanfragen&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>(isset($_POST['cmdDSNeu_x'])?'':$rsZSA->FeldInhalt('ZSA_USER')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>(isset($_POST['cmdDSNeu_x'])?'':$rsZSA->FeldInhalt('ZSA_USERDAT')));
	$Form->InfoZeile($Felder,'');

	$EditRecht=(($Recht10006&2)!=0);

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSA']['ZSA_WANR'].':',250);
 	$Form->Erstelle_TextFeld('ZSA_WANR',(isset($_POST['cmdDSNeu_x'])?'':$rsZSA->FeldInhalt('ZSA_WANR')),20,200,$EditRecht,'','','','T','','','',11);
 	$AWISCursorPosition='txtZSA_WANR';
	$Form->ZeileEnde();


    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZSA']['ZSA_DATUM_VON'].':',250);
    $Form->Erstelle_TextFeld('!ZSA_DATUM_VON',$rsZSA->FeldOderPOST('ZSA_DATUM_VON'),20,200,$EditRecht,'','','','D','','','',11);
    $Form->ZeileEnde();


    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZSA']['ZSA_DATUM_BIS'].':',250);
    $Form->Erstelle_TextFeld('!ZSA_DATUM_BIS',$rsZSA->FeldOderPOST('ZSA_DATUM_BIS'),20,200,$EditRecht,'','','','D','','','',11);
    $Form->ZeileEnde();

		// Lieferant
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['ZSA']['ZSA_FIRMA'].':',250);
	$SQL = 'SELECT LIE_NR, LIE_NAME1';
	$SQL .= ' FROM Lieferanten';
	$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
	$SQL .= ' AND EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 501 AND LIN_WERT is not null AND LIN_LIE_NR = LIE_NR)';
	$SQL .= ' ORDER BY LIE_NAME1';
	$Form->Erstelle_SelectFeld('!ZSA_FIRMA','',200,true,$SQL,'~::Bitte w�hlen::');
	$Form->ZeileEnde();



}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$Form->Formular_Ende();
$Form->SchaltflaechenStart();
if(($Recht10006&2)!==0)		//
{
	$Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(isset($_POST['cmdDSNeu_x']) OR $rsZSA->EOF())
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');

}
$Form->SchaltflaechenEnde();

echo '</form>';

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
