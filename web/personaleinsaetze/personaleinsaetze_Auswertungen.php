<?php

global $AWISCursorPosition;
global $AWIS_KEY1;

try
{	
	$AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
    $TextKonserven = array();
    $TextKonserven[]=array('PEI','*');        
    $TextKonserven[]=array('PBM','PBM_XBN_KEY');
    $TextKonserven[]=array('PEB','PEB_BEZEICHNUNG');
    $TextKonserven[]=array('FER','FER_BEZEICHNUNG');
    $TextKonserven[]=array('Wort','Abschlussdatum');
    $TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Auswertung');
	$TextKonserven[]=array('Liste','lst_PEI_AUSWERTUNGEN');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');

    $Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht4505 = $AWISBenutzer->HatDasRecht(4505);

	if($Recht4505==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$Form->DebugAusgabe(1,$_POST);

	//if(!isset($_POST['cmdSuche_x']) and !isset($_POST['sucRFF_AST_ATUNR'])
//		and !isset($_GET['Liste']) and !isset($_POST['Block']) 
		//and !isset($_GET['Sort']) and !isset($_GET['Block']))
	
	if(!isset($_POST['sucPEI_AUSWERTUNG']))
	{	
		
		$Form->SchreibeHTMLCode('<form name="frmPersonaleinsatzAuswertungSuche" action="./personaleinsaetze_Main.php?cmdAktion=Auswertungen" method="POST"  enctype="multipart/form-data">');
		
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PEI_Auswertung'));
		
		if(!isset($Param['SPEICHERN']))
		{
			$Param['SPEICHERN']='off';
			$Param['PBM_XBN_KEY']=$AWISBenutzer->BenutzerID();
		}
		
		$Form->Formular_Start();	
		
		// Datumsbereich festlegen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',200);
		$Form->Erstelle_TextFeld('*PEI_DATUM_VOM',($Param['SPEICHERN']=='on'?$Param['PEI_DATUM_VOM']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('01.01.Y'))),10);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',200);
		$Form->Erstelle_TextFeld('*PEI_DATUM_BIS',($Param['SPEICHERN']=='on'?$Param['PEI_DATUM_BIS']:''),10,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':$Form->PruefeDatum(date('31.12.Y'))),10);		
		$Form->ZeileEnde();
		
		//Filiale
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_FIL_ID'].':',200);
		if(($FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING))!='')
		{
			$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
			$SQL .= ' FROM Filialen ';
			$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
			$SQL .= ' ORDER BY FIL_BEZ';
			$Form->Erstelle_SelectFeld('*PEI_FIL_ID',($Param['SPEICHERN']=='on'?$Param['PEI_FIL_ID']:''),200,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		}
		else
		{
			$Form->Erstelle_TextFeld('*PEI_FIL_ID',($Param['SPEICHERN']=='on'?$Param['PEI_FIL_ID']:''),10,150,true,'','','','T','L','','',10);
		}
		$Form->ZeileEnde();
		
		
		//Mitarbeiter
		$SQL = "SELECT DISTINCT xbn_key, xbn_name || coalesce(' '||xbn_vorname,'') AS Anwender ";
		$SQL .= " FROM (SELECT PEB_KEY, SUM(POWER(2,(PBR_PBZ_KEY-1))) AS RECHT";
		$SQL .= "       FROM Perseinsbereichemitglieder";
		$SQL .= "		INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY ";
		$SQL .= "		INNER JOIN PERSEINSBEREICHERECHTEVERGABE ON PBR_PBM_KEY = PBM_KEY";
		$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
		$SQL .= " GROUP BY PEB_KEY) Bereiche";
	//	$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key";
	// 04.06.2009 SK
		$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND pbm_gueltigab <= sysdate and pbm_gueltigbis >=sysdate and CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key "; //TR 07.02.11
		//$SQL .= " INNER JOIN Perseinsbereichemitglieder ON pbm_peb_key = peb_key AND CASE WHEN BITAND(RECHT,POWER(2,4)) = 0 THEN 0".$AWISBenutzer->BenutzerID() ." ELSE pbm_xbn_key END = pbm_xbn_key ";
		$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGAB END <= SYSDATE ';
		$SQL .= '    AND CASE WHEN BITAND(RECHT,64) = 64 THEN SYSDATE ELSE PBM_GUELTIGBIS END >= SYSDATE ';
		$SQL .= " INNER JOIN Benutzer ON pbm_xbn_key = xbn_key";
		$SQL .= ' ORDER BY 2';
		//$Form->DebugAusgabe(1,$SQL);
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PBM']['PBM_XBN_KEY'].':',200);
		$Form->Erstelle_SelectFeld('PBM_XBN_KEY',($Param['SPEICHERN']=='on'?$Param['PBM_XBN_KEY']:$AWISBenutzer->BenutzerID()),100,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0'],'','','',$AWISBenutzer->BenutzerID(),'');
		$AWISCursorPosition = 'txtPBM_XBN_KEY';
		$Form->ZeileEnde();

		// Rolle
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'].':',200);
		$SQL = 'SELECT FER_KEY, FER_BEZEICHNUNG FROM FilialEbenenRollen ORDER BY FER_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('*FER_KEY',($Param['SPEICHERN']=='on'?$Param['FER_KEY']:''),200,true,$SQL,'0~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
		
		
		/*
		// Bearbeitungsbereich		
		$SQL = 'SELECT peb_key, peb_bezeichnung';
		$SQL .= ' FROM Perseinsbereichemitglieder ';
		$SQL .= ' INNER JOIN PerseinsBereiche ON PBM_PEB_KEY = PEB_KEY';
		$SQL .= ' INNER JOIN PersEinsBereicheRechteVergabe ON PBM_KEY = PBR_PBM_KEY AND PBR_PBZ_KEY = 5';
		$SQL .= ' LEFT OUTER JOIN Benutzer ON pbm_xbn_key = xbn_key';
		$SQL .= ' WHERE PBM_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . '';
		$SQL .= ' AND PBM_GUELTIGAB <= SYSDATE';
		$SQL .= ' AND PBM_GUELTIGBIS >= SYSDATE';
		$SQL .= ' ORDER BY peb_bezeichnung';
		$rsPEB = $DB->RecordSetOeffnen($SQL);
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PEB']['PEB_BEZEICHNUNG'].':',200);
		$Form->Erstelle_SelectFeld('*PEB_KEY','',100,true,$SQL,'','','','','','');
		$AWISCursorPosition = 'sucPEB_KEY';
		$Form->ZeileEnde();
		*/
		
		//Auswertung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Auswertung'].':',200);
		$SQL = 'SELECT PAW_KEY, PAW_BEZEICHNUNG';
		$SQL .= ' FROM Perseinsauswertungen ';
		$SQL .= ' WHERE PAW_XRC_ID = 4505';			
		
		$Bedingung='';
		if ($Recht4505&1)
		{
			$Bedingung .= ' OR PAW_XRS_BIT = 1';	
		}
		if ($Recht4505&2)
		{
			$Bedingung .= ' OR PAW_XRS_BIT = 2';				
		}
		if ($Recht4505&4)
		{
			$Bedingung .= ' OR PAW_XRS_BIT = 4';				
		}
		if ($Recht4505&8)
		{
			$Bedingung .= ' OR PAW_XRS_BIT = 8';				
		}		
		
		if($Bedingung!='')
		{
			$SQL .= ' AND (' . substr($Bedingung,3). ')';
		}
		
		$SQL .= ' ORDER BY PAW_BEZEICHNUNG';
		
		//$Form->DebugAusgabe(1,$SQL);		
		//$Auswertung = explode("|",$AWISSprachKonserven['Liste']['lst_PEI_AUSWERTUNGEN']);				
		$Form->Erstelle_SelectFeld('*PEI_AUSWERTUNG',($Param['SPEICHERN']=='on'?$Param['PEI_AUSWERTUNG']:''),200,true,$SQL,'','','','','','');
		$AWISCursorPosition='sucPEI_AUSWERTUNG';
		$Form->ZeileEnde();		
				
		//$Form->Trennzeile();

		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		$Form->SchaltflaechenStart();	
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');		
		$Form->SchaltflaechenEnde();
		
		$Form->SchreibeHTMLCode('</form>');		
	}	
	else 
	{	
		$Param = array();
	
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';

		//$Param['PEB_KEY']=$Form->Format('N0',$_POST['txtPEB_KEY'],true);
		$Param['PBM_XBN_KEY']=$Form->Format('N0',$_POST['txtPBM_XBN_KEY'],true);
		$Param['PEI_DATUM_VOM']=$Form->Format('D',$_POST['sucPEI_DATUM_VOM'],true);
		$Param['PEI_DATUM_BIS']=$Form->Format('D',$_POST['sucPEI_DATUM_BIS'],true);
		$Param['PEI_FIL_ID']=$Form->Format('N0',$_POST['sucPEI_FIL_ID'],true);
		//Param['PEI_PEA_KEY']=$Form->Format('N0',$_POST['sucPEI_PEA_KEY'],true);
		$Param['FER_KEY']=$Form->Format('N0',$_POST['sucFER_KEY'],true);
		$Param['PEI_AUSWERTUNG']=$Form->Format('N0',$_POST['sucPEI_AUSWERTUNG'],true);

		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$AWISBenutzer->ParameterSchreiben('Formular_PEI_Auswertung',serialize($Param));
					
		if ($_POST['sucPEI_AUSWERTUNG'] == 1) 
		{																		
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('href','cmd_zurueck','./personaleinsaetze_Main.php?cmdAktion=Auswertungen','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			$Form->Schaltflaeche('href', 'cmdDrucken', './personaleinsaetze_Auswertungen_Jahresuebersicht.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');				
			$Form->SchaltflaechenEnde();				
		}
		elseif ($_POST['sucPEI_AUSWERTUNG'] == 2) 
		{																	
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('href','cmd_zurueck','./personaleinsaetze_Main.php?cmdAktion=Auswertungen','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			$Form->Schaltflaeche('href', 'cmdDrucken', './personaleinsaetze_Auswertungen_Samstagsauswertung.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');				
			$Form->SchaltflaechenEnde();				
		}
		elseif ($_POST['sucPEI_AUSWERTUNG'] == 3)
		{													
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('href','cmd_zurueck','./personaleinsaetze_Main.php?cmdAktion=Auswertungen','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			$Form->Schaltflaeche('href', 'cmdDrucken', './personaleinsaetze_Auswertungen_Urlaubsplan.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');				
			$Form->SchaltflaechenEnde();								
		}
		elseif ($_POST['sucPEI_AUSWERTUNG'] == 4)
		{
			$Form->SchaltflaechenStart();
			$Form->Schaltflaeche('href','cmd_zurueck','./personaleinsaetze_Main.php?cmdAktion=Auswertungen','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
			$Form->Schaltflaeche('href', 'cmdDrucken', './personaleinsaetze_Auswertungen_Kalenderwoche.php', '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_drucken'], 'X');				
			$Form->SchaltflaechenEnde();								
		}	
	}
				
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
			
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}




?>
