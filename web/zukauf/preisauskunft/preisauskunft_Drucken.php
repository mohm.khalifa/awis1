<?php
require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

if(!isset($_GET['ID']))
{
	die();
}

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('ZPA','%');
$TextKonserven[]=array('ZLB','%');
$TextKonserven[]=array('LVK','%');
$TextKonserven[]=array('Liste','ZLB_VERSANDART');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Spalte = 15;
$Zeile = 20;

$Keys = explode('~',base64_decode($_GET['ID']));

$Ausdruck = new awisAusdruck('L','A4',array('BriefpapierATU_DE_Seite_2_quer.pdf'));

$Ausdruck->NeueSeite(0,1);

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','B',16);
$Ausdruck->_pdf->cell(180,6,$AWISSprachKonserven['ZPA']['txt_Bestelluebersicht'],0,0,'L',0);

//pr�fen wegen Datumsformatierung
$SQL = 'SELECT ZukaufLieferantenBestellungen.*, Filialen.*, LIE_NAME1';
$SQL .= ', MAX(ZLB_USERDAT) OVER(PARTITION BY ZLB_WANR) AS BESTELLZEITPUNKT';
$SQL .= ', LVK_NAME1, LVK_NAME2, LVK_STRASSE, LVK_PLZ, LVK_ORT, LVK_TELEFON, LVK_FAX, LVK_EMAIL';
$SQL .=' FROM ZukaufLieferantenBestellungen';
$SQL .=' INNER JOIN Filialen ON ZLB_FIL_ID = FIL_ID';
$SQL .=' INNER JOIN Lieferanten ON ZLB_LIE_NR = LIE_NR';
$SQL .=' LEFT OUTER JOIN Zukauflieferantenartikel ON ZLB_ZLA_KEY = ZLA_KEY';
$SQL .=' LEFT OUTER JOIN FilialenLieferanten ON FLI_LIE_NR = LIE_NR AND FLI_FIL_ID = FIL_ID';
$SQL .=' LEFT OUTER JOIN LieferantenVerkaufshaeuser ON FLI_LVK_KEY = LVK_KEY AND FLI_LIE_NR = LIE_NR';
$SQL .=' WHERE ZLB_WANR = '.$DB->FeldInhaltFormat('T',$Keys[1]);
$SQL .=' AND ZLB_LIE_NR = '.$DB->FeldInhaltFormat('N0',$Keys[0]);
$SQL .=' AND FLI_GUELTIGAB <= ZLB_USERDAT';
$SQL .=' AND FLI_GUELTIGBIS >= ZLB_USERDAT';
$SQL .=' ORDER BY ZLB_HERSTELLERCODE, ZLB_ARTIKELBEZEICHNUNG';
$rsZLB = $DB->RecordSetOeffnen($SQL);

$Zeile = 40;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(180,6,$rsZLB->FeldInhalt('FIL_BEZ'),0,0,'L',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(180,6,$rsZLB->FeldInhalt('FIL_STRASSE'),0,0,'L',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(180,6,$rsZLB->FeldInhalt('FIL_PLZ').' '.$rsZLB->FeldInhalt('FIL_ORT'),0,0,'L',0);
$Zeile+=5;

//**********************************************************
//* Lieferanten
//**********************************************************

$Zeile=40;
$Ausdruck->_pdf->setXY($Spalte+100, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['ZPA']['ZPA_LIE_NR'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(80,6,($rsZLB->FeldInhalt('LVK_NAME1')!=''?$rsZLB->FeldInhalt('LVK_NAME1'):$rsZLB->FeldInhalt('LIE_NAME1')),0,0,'L',0);
if($rsZLB->FeldInhalt('LVK_NAME2')!='')
{
	$Zeile+=5;
	$Ausdruck->_pdf->setXY($Spalte+150, $Zeile);
	$Ausdruck->_pdf->cell(80,6,$rsZLB->FeldInhalt('LVK_NAME2'),0,0,'L',0);
}
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte+150, $Zeile);
$Ausdruck->_pdf->cell(80,6,$rsZLB->FeldInhalt('LVK_STRASSE'),0,0,'L',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte+150, $Zeile);
$Ausdruck->_pdf->cell(80,6,$rsZLB->FeldInhalt('LVK_PLZ').' '.$rsZLB->FeldInhalt('LVK_ORT'),0,0,'L',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte+100, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['LVK']['LVK_TELEFON'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(80,6,$rsZLB->FeldInhalt('LVK_TELEFON'),0,0,'L',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte+100, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['LVK']['LVK_FAX'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(80,6,$rsZLB->FeldInhalt('LVK_FAX'),0,0,'L',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte+100, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['LVK']['LVK_EMAIL'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(80,6,$rsZLB->FeldInhalt('LVK_EMAIL'),0,0,'L',0);
$Link = $Ausdruck->_pdf->AddLink();

$Zeile+=5;


//***********************************************************
//* WA Daten
//***********************************************************
$Zeile = 70;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['ZLB']['ZLB_WANR'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(50,6,$rsZLB->FeldInhalt('ZLB_WANR'),0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['ZLB']['ZLB_VERSANDART'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(80,6,$Form->WerteListe($AWISSprachKonserven['Liste']['ZLB_VERSANDART'],$rsZLB->FeldInhalt('ZLB_VERSANDART')),0,0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['ZLB']['ZLB_USER'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(50,6,$rsZLB->FeldInhalt('ZLB_USER'),0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','',12);
$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['ZLB']['ZLB_USERDAT'].':',0,0,'L',0);
$Ausdruck->_pdf->SetFont('Arial','B',12);
$Ausdruck->_pdf->cell(80,6,$Form->Format('DU',$rsZLB->FeldInhalt('BESTELLZEITPUNKT')),0,0,'L',0);

if($rsZLB->FeldInhalt('ZLB_AUFTRAGSNR')!='')
{
	$Zeile+=5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',12);
	$Ausdruck->_pdf->cell(50,6,$AWISSprachKonserven['ZLB']['ZLB_AUFTRAGSNR'].':',0,0,'L',0);
	$Ausdruck->_pdf->SetFont('Arial','B',12);
	$Ausdruck->_pdf->cell(50,6,$rsZLB->FeldInhalt('ZLB_AUFTRAGSNR'),0,0,'L',0);
}

$Zeile+=10;

$Ausdruck->_pdf->SetFillColor(200,200,200);
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->cell(45,6,$AWISSprachKonserven['ZLB']['ZLB_BESTELLNUMMER'],0,0,'L',1);
$Ausdruck->_pdf->cell(120,6,$AWISSprachKonserven['ZLB']['ZLB_ARTIKELBEZEICHNUNG'],0,0,'L',1);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['ZLB']['ZLB_BESTELLMENGE'],0,0,'L',1);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['ZLB']['ZLB_LIEFERMENGE'],0,0,'L',1);
$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['ZLB']['ZLB_RUECKSTAND'],0,0,'L',1);
$Ausdruck->_pdf->cell(40,6,$AWISSprachKonserven['ZLB']['ZLB_USERDAT'],0,0,'L',1);
$Zeile+=5;

$Ausdruck->_pdf->SetFont('Arial','',10);
$Ausdruck->_pdf->SetFillColor(255,255,255);
while(!$rsZLB->EOF())
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell(45,6,$rsZLB->FeldInhalt('ZLB_BESTELLNUMMER'),0,0,'L',0);
	$Ausdruck->_pdf->cell(120,6,$rsZLB->FeldInhalt('ZLB_ARTIKELBEZEICHNUNG'),0,0,'L',0);
	$Ausdruck->_pdf->cell(23,6,$rsZLB->FeldInhalt('ZLB_BESTELLMENGE'),0,0,'L',0);
	$Ausdruck->_pdf->cell(23,6,$rsZLB->FeldInhalt('ZLB_LIEFERMENGE'),0,0,'L',0);
	$Ausdruck->_pdf->cell(23,6,$rsZLB->FeldInhalt('ZLB_RUECKSTAND'),0,0,'L',0);
	$Ausdruck->_pdf->cell(40,6,$rsZLB->FeldInhalt('ZLB_USERDAT'),0,0,'DU',0);

	$Zeile+=4;
	$Ausdruck->_pdf->setXY($Spalte+45, $Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->cell(190,6,utf8_decode($rsZLB->FeldInhalt('ZLB_STATUSTEXT')),0,0,'L',0);

	if($Zeile>$Ausdruck->SeitenHoehe()-20)
	{
		$Ausdruck->NeueSeite(0,1);

		$Zeile=20;
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont('Arial','B',16);
		$Ausdruck->_pdf->cell(180,6,$AWISSprachKonserven['ZPA']['txt_Bestelluebersicht'],0,0,'L',0);

		$Zeile=30;
		$Ausdruck->_pdf->SetFillColor(200,200,200);
		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->cell(45,6,$AWISSprachKonserven['ZLB']['ZLB_BESTELLNUMMER'],0,0,'L',1);
		$Ausdruck->_pdf->cell(120,6,$AWISSprachKonserven['ZLB']['ZLB_ARTIKELBEZEICHNUNG'],0,0,'L',1);
		$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['ZLB']['ZLB_BESTELLMENGE'],0,0,'L',1);
		$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['ZLB']['ZLB_LIEFERMENGE'],0,0,'L',1);
		$Ausdruck->_pdf->cell(23,6,$AWISSprachKonserven['ZLB']['ZLB_RUECKSTAND'],0,0,'L',1);
		$Ausdruck->_pdf->cell(40,6,$AWISSprachKonserven['ZLB']['ZLB_USERDAT'],0,0,'L',1);
		$Zeile+=5;
	}
	$Zeile+=5;
	$rsZLB->DSWeiter();
}

$Ausdruck->Anzeigen();
?>