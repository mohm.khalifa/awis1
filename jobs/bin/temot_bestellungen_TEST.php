<?php
require_once('awisJobVerwaltung.php');
require_once('db.inc.php');
require_once('awisTemot.php');

try 
{
	$con = awisLogon();

	$Job = new awisJobProtokoll(2, $con, 'Starte Import.');
	
	$Firma = 'PV';
	
	if(isset($argv[1]))
	{
		$Firma = $argv[1];
	}
 	$Temot = new awisTemot(1,$Firma);
 	
 	//$Temot->LeseBestellungen('','','','','','');

 	$Datum = strtotime('07/01/2007');
 	while($Datum < time())
 	{
 		$Datum = mktime(0,0,0,date('m',$Datum),date('d',$Datum)+1,date('Y',$Datum));
 		echo '***************************************************************'.PHP_EOL;
 		echo 'Lese '.date('Ymd',$Datum).PHP_EOL;
 		echo '***************************************************************'.PHP_EOL;
 		$Temot->LeseBestellungen('','',date('Ymd',$Datum),date('Ymd',$Datum),'','A');
 		
 	}
 	//$Temot->LeseBestellungen('','','','','','');
 	
 	
 	
 	$Job->SchreibeLog(1,0,'Ende des Jobs');
}
catch (Exception $ex)
{
	echo $ex->getMessage().PHP_EOL;
 	$Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.awisFeldFormat('T',$ex->getMessage()));
}

?>
