<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtXAB_KEY']))
		{
			$Tabelle = 'XAB';
			$Key=$_POST['txtXAB_KEY'];

			$SQL = 'SELECT XAB_KEY, XAB_BEZEICHNUNG';
			$SQL .= ' FROM ExportAbfragen ';
			$SQL .= ' WHERE XAB_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$HauptKey = $rsDaten->FeldInhalt('XAB_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('XAB','XAB_BEZEICHNUNG'),$rsDaten->FeldInhalt('XAB_BEZEICHNUNG'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_GET['Seite']) AND !isset($_GET['File']))
	{
		$SQL = 'DELETE FROM ExportAbfragenKriterien ';
		$SQL .= ' WHERE xak_xab_key = '.$DB->WertSetzen('XAK','N0',$_GET['XAB_KEY']);
		$SQL .= ' AND xak_gruppe='.$DB->WertSetzen('XAK','N0',$_GET['Del']);
		$DB->Ausfuehren($SQL,'',true,$DB->BindeVariablen('XAK'));
		$AWIS_KEY1 = $_GET['XAB_KEY'];
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']) AND !isset($_GET['File']))
	{
		switch ($_GET['Seite'])
		{
			case 'Zeitplan':
				$Tabelle = 'XDZ';
				$Key=$_GET['Del'];

				$SQL = 'SELECT XDZ_KEY, XDZ_XAB_KEY, XDZ_NAECHSTERSTART';
				$SQL .= ' FROM EXPORTABFRAGENZEITPUNKTE';
				$SQL .= ' WHERE XDZ_KEY=0'.$Key . '';
				$rsDaten = $DB->RecordsetOeffnen($SQL);

				$HauptKey=$rsDaten->FeldInhalt('XDZ_XAB_KEY');
				$Felder=array();

				$Felder[]=array($Form->LadeTextBaustein('XDZ','XDZ_NAECHSTERSTART'),$rsDaten->FeldInhalt('XDZ_NAECHSTERSTART'));
				break;
			default:
				break;
		}
	}
	elseif(isset($_GET['Del']) AND isset($_GET['File'])) {
		unlink('/daten/web/dokumente/datenexport/'.$_GET['Del'].'/'.urldecode($_GET['File']));
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'XAB':
				$SQL = 'DELETE FROM ExportAbfragen WHERE XAB_KEY=0'.$_POST['txtKey'];
				$AWIS_KEY1=-1;
				break;
			case 'XDZ':
				$SQL = 'DELETE FROM EXPORTABFRAGENZEITPUNKTE WHERE XDZ_XAB_KEY=0'.$_POST['txtHauptKey'].' AND XDZ_KEY = '.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtHauptKey'];
				$AWIS_KEY2='';
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				throw new awisException('Fehler beim Loeschen',201202181536,$SQL,awisException::AWIS_ERR_SYSTEM);
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./serviceleistungen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
		if(isset($SubKey))
		{
			$Form->Erstelle_HiddenFeld('SubKey',$SubKey);			// Bei den Unterseiten
		}
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>


<?php
/*global $Param;
global $awisRSInfo;
global $awisDBError;
global $awisRSInfoName;
global $AWISSprache;
global $con;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

//awis_Debug(1,$_POST,$_GET);
$Tabelle= '';


if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))		// gesamter
{
	$Tabelle = 'XAB';
	$Key=$_POST['txtXAB_KEY'];

	$SQL = 'SELECT XAB_KEY, XAB_BEZEICHNUNG FROM ExportAbfragen WHERE XAB_KEY=0'.$Key;
	$rsDaten = $DB->RecordSetOeffnen($SQL);
	$Felder=array();
	$Felder[]=array(awis_TextKonserve($con,'XAB_BEZEICHNUNG','XAB',$AWISSprache),$rsDaten['XAB_BEZEICHNUNG'][0]);
}
elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
{
	$Tabelle = '';	// MUss leer sein, um keine Abfrage zu erhalten
	$Key=$_GET['Del'];
	$XAB_KEY = $_GET['XAB_KEY'];
	
//	$SQL = 'SELECT xak_xab_key FROM ExportAbfragenKriterien WHERE xak_xab_key = '.$XAB_KEY.' AND xak_gruppe=0'.$Key;
//	$rsXAK = awisOpenRecordset($con,$SQL);
	$AWIS_KEY1	= 0;
	$AWIS_KEY2	= $XAB_KEY;

//awis_Debug(1,$AWIS_KEY2,$AWIS_KEY1);

	$SQL = 'DELETE FROM ExportAbfragenKriterien WHERE xak_xab_key = '.$AWIS_KEY2.' AND xak_gruppe=0'.$Key;
	if(awisExecute($con,$SQL)===false)
	{
		awisErrorMailLink('datenexporte_loeschen_1',1,$awisDBError['messages'],'');
	}
	$SQL = '';
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{

//awis_Debug(1,$_POST);
	$SQL = '';
	switch ($_POST['txtTabelle']) {
		case 'XAB':
			$SQL = 'DELETE FROM ExportAbfragen WHERE xab_key=0'.$_POST['txtKey'];
			break;
		default:
			break;
	}
//awis_Debug(1,$SQL);

	if($SQL !='')
	{
		if(awisExecute($con,$SQL)===false)
		{
			awisErrorMailLink('datenexporte_loeschen_1',1,$awisDBError['messages'],'');
		}
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

	echo '<form name=frmLoeschen action=./datenexporte_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>';
	echo '<span class=HinweisText>'.$TXT_AdrLoeschen['Wort']['WirklichLoeschen'].'</span>';

	foreach($Felder AS $Feld)
	{
		echo '<br>'.$Feld[0].': '.$Feld[1];
	}

	echo '<input type=hidden name=txtTabelle value="'.$Tabelle.'">';
	echo '<input type=hidden name=txtKey value="'.$Key.'">';

	echo '<br><input type=submit name=cmdLoeschenOK value='.$TXT_AdrLoeschen['Wort']['Ja'].'>';
	echo '&nbsp;<input type=submit name=cmdLoeschenAbbrechen value='.$TXT_AdrLoeschen['Wort']['Nein'].'>';

	echo '</form>';
	awisLogoff($con);
	die();
}
*/
?>