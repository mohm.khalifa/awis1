<?php
global $CRM;

try
{

    if(isset($_POST['cmdSpeichern_x'])){
        require_once 'crm_speichern.php';
    }

	echo "<form name=frmSuche method=post action=crm_Main.php?cmdAktion=GebietBetreuerImport enctype='multipart/form-data'>";

	$CRM->Form->Formular_Start();

	$CRM->Form->FormularBereichStart();
	$CRM->Form->FormularBereichInhaltStart($CRM->AWISSprachKonserven['CRM']['CRM_VORBEREITUNGEN']);

	$CRM->Form->ZeileStart();
	$CRM->Form->Erstelle_TextLabel($CRM->AWISSprachKonserven['CRM']['CRM_KON_KEY_SUCHE'] . ':',150);
	$CRM->Form->Erstelle_MehrfachSelectFeld('CRM_KON_KEY',[],'300:300',true,$CRM->KON_SQL(),'','','','',null,'','',[],'','AWIS','','',1);
    $CRM->Form->Erstelle_Bild('href','cmdExport','./crm_Keys_Downloaden.php?DL=KON','/bilder/cmd_export_xls.png',$CRM->AWISSprachKonserven['CRM']['CRM_DOWNLOAD'],'',27,27);
    $CRM->Form->ZeileEnde();

	$CRM->Form->ZeileStart();
	$CRM->Form->Erstelle_TextLabel($CRM->AWISSprachKonserven['CRM']['CRM_CGB_KEY_SUCHE'] . ':',150);
    $CRM->Form->Erstelle_MehrfachSelectFeld('CRM_CGB_KEY',[],'300:300',true,$CRM->CGB_SQL(),'','','','',null,'','',[],'','AWIS','','',1);
    $CRM->Form->Erstelle_Bild('href','cmdExport','./crm_Keys_Downloaden.php?DL=CGB','/bilder/cmd_export_xls.png',$CRM->AWISSprachKonserven['CRM']['CRM_DOWNLOAD'],'',27,27);
    $CRM->Form->ZeileEnde();

    $CRM->Form->ZeileStart();
    $CRM->Form->Erstelle_TextLabel($CRM->AWISSprachKonserven['CRM']['CRM_MUSTERDATEI'] . ':',150);
    $CRM->Form->Erstelle_TextLabel('<a href="template_GebietBetreuer.csv" target="_blank">'.$CRM->AWISSprachKonserven['CRM']['CRM_DOWNLOAD'].'</a>',300);
    $CRM->Form->ZeileEnde();

    $CRM->Form->ZeileStart();
    $CRM->Form->Erstelle_TextLabel($CRM->AWISSprachKonserven['CRM']['CRM_MUSTERDATEI'] . ':',150);
    $CRM->Form->Erstelle_TextLabel($CRM->leseTemplateUeberschrift('template_GebietBetreuer.csv',true),600);
    $CRM->Form->ZeileEnde();


    $CRM->Form->FormularBereichInhaltEnde();
	$CRM->Form->FormularBereichEnde();

    $CRM->Form->FormularBereichStart();
    $CRM->Form->FormularBereichInhaltStart($CRM->AWISSprachKonserven['CRM']['CRM_UPLOAD'],true);

    $CRM->Form->ZeileStart();
    $CRM->Form->Erstelle_TextLabel($CRM->AWISSprachKonserven['CRM']['CRM_UPLOAD'].':',150);
    $CRM->Form->Erstelle_DateiUpload('CRM_UPLOAD','500',50,10*1024*1024);
    $CRM->Form->ZeileEnde();

    $CRM->Form->FormularBereichInhaltEnde();
    $CRM->Form->FormularBereichEnde();


    $CRM->Form->Formular_Ende();

	$CRM->Form->SchaltflaechenStart();
	$CRM->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$CRM->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$CRM->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_weiter.png', $CRM->AWISSprachKonserven['Wort']['lbl_speichern'], 'W');
	$CRM->Form->SchaltflaechenEnde();

	$CRM->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($CRM->Form instanceof awisFormular)
	{
		$CRM->Form->DebugAusgabe(1, $ex->getSQL());
		$CRM->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($CRM->Form instanceof awisFormular)
	{
		$CRM->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>