<?php

/**
 * 
 * @author Henry Ott
 * @copyright ATU Auto Teile Unger
 * @version 20130502
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('REH', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Liste', 'lst_KEINE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[]=array('Liste','lst_JaNein');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht15400 = $AWISBenutzer->HatDasRecht(15400);
    if ($Recht15400 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Form->DebugAusgabe(1, $_POST);
    $Form->DebugAusgabe(1, $_GET);
    
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_REH"));
	
    $Form->DebugAusgabe(1,$Param);
    
    if (isset($_POST['cmdSuche_x']))
    {
        $Param['KEY'] = 0;
        $Param['ORDER'] = '';
        $Param['BLOCK'] = '';
        
        $Param['REH_KEY'] = $_POST['sucREH_KEY'];
        
        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');

        $AWISBenutzer->ParameterSchreiben("Formular_REH", serialize($Param));
    }
    elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
        include('./reifenhersteller_loeschen.php');
    }
    elseif (isset($_POST['cmdSpeichern_x']) or isset($_GET['ok']))
    {
        include('./reifenhersteller_speichern.php');
    }
    elseif (isset($_POST['cmdDSNeu_x']))
    {
        $AWIS_KEY1 = -1;
    }
    elseif (isset($_GET['REH_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['REH_KEY']);
    }
    else   // Letzten Benutzer suchen
    {
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_REH',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
    }

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (!isset($_GET['Sort']))
    {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '')
        {
            $ORDERBY = $Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' REH_BEZEICHNUNG';
        	$Param['ORDER'] = $ORDERBY;
        }
    }
    else
    {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        $Param['ORDER'] = $ORDERBY;
    }

    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $BindeVariablen = array();
    $Bedingung = _BedingungErstellen($Param, $BindeVariablen);

	//********************************************************
	// Daten suchen
	//********************************************************
	
    $SQL  = ' SELECT reh.*';
    if ($AWIS_KEY1 <= 0)
    {
        $SQL .= ' ,row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr';
    }
    $SQL .= ' FROM reifenhersteller reh';    
    
    if ($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $SQL .= ' ORDER BY ' . $ORDERBY;
    //$Form->DebugAusgabe(1,$SQL);
    
    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if (isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        $Param['BLOCK']=$Block;
    }
    elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
    {
        $Block = intval($Param['BLOCK']);
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $Form->DebugAusgabe(1,$SQL);
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if ($AWIS_KEY1 <= 0)
    {
        $SQL = 'SELECT * FROM (' . $SQL . ') DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    }
    
    $rsREH = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
    $Form->DebugAusgabe(1,$DB->LetzterSQL());

    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->SchreibeHTMLCode('<form name=frmReifenhersteller action=./reifenhersteller_Main.php?cmdAktion=Details' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . '' . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . ' method=post>');
    
	if ($rsREH->EOF() AND !isset($_POST['cmdDSNeu_x']) AND !isset($_GET['REH_KEY']))  // Keine Meldung bei neuen Datens�tzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';

		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht15400 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	    $Form->SchaltflaechenEnde();
    }
    elseif (($rsREH->AnzahlDatensaetze() > 1) or (isset($_GET['Liste'])))      // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();//('font-size:10pt');
    	
        $Link = './reifenhersteller_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=REH_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REH_BEZEICHNUNG')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'], 300, '', $Link);

        $Link = './reifenhersteller_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=REH_KURZBEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REH_KURZBEZEICHNUNG')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REH']['REH_KURZBEZEICHNUNG'], 200	, '', $Link);

        $Link = './reifenhersteller_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=REH_SONDERBESTNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REH_SONDERBESTNR')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REH']['REH_SONDERBESTNR'], 300, '', $Link);

        
        $Form->ZeileEnde();

        $DS = 0;

        while (!$rsREH->EOF())
        {
            $Form->ZeileStart();//('font-size:10pt');
            $Link = './reifenhersteller_Main.php?cmdAktion=Details&REH_KEY=' . $rsREH->FeldInhalt('REH_KEY') . '';

            $Style = '';//$rsREH->FeldInhalt('STYLE');
            $Form->Erstelle_ListenFeld('REH_BEZEICHNUNG', $rsREH->FeldInhalt('REH_BEZEICHNUNG'), 50, 300, false, ($DS % 2), $Style, $Link, '', '', '');
            $Form->Erstelle_ListenFeld('REH_KURZBEZEICHNUNG', $rsREH->FeldInhalt('REH_KURZBEZEICHNUNG'), 8, 200, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('REH_SONDERBESTNR', $rsREH->FeldInhalt('REH_SONDERBESTNR'), 40, 300, false, ($DS % 2), $Style, '', '', '', '');

			$Form->ZeileEnde();

            $rsREH->DSWeiter();
            $DS++;
        }

        $Link = './reifenhersteller_Main.php?cmdAktion=Details&Liste=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        if (($Recht15400 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
        {
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }
        $Form->SchaltflaechenEnde();
    }
    else          // Eine einzelner Datensatz
    {
        $Form->SchreibeHTMLCode('<form name=frmReifenhersteller action=./reifenhersteller_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

        $AWIS_KEY1 = $rsREH->FeldInhalt('REH_KEY');
        $Param['KEY']=$AWIS_KEY1;
        $AWISBenutzer->ParameterSchreiben("Formular_REH", serialize($Param));
        $EditRecht = ($Recht15400&2!=0);
        $Form->Erstelle_HiddenFeld('REH_KEY', $AWIS_KEY1);

        $Form->Formular_Start();
        $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

    	$Form->DebugAusgabe(1,$AWIS_KEY1);

        // Infozeile
        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./reifenhersteller_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsREH->FeldInhalt('REH_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsREH->FeldInhalt('REH_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'] . ':', 150);
        $Form->Erstelle_TextFeld('!REH_BEZEICHNUNG', ($rsREH->FeldInhalt('REH_BEZEICHNUNG')), 50, 100, $EditRecht,'','','','','','','',50);
        $Form->ZeileEnde();
        $AWISCursorPosition = 'txtREH_BEZEICHNUNG';

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_KURZBEZEICHNUNG'] . ':', 150);
        $Form->Erstelle_TextFeld('!REH_KURZBEZEICHNUNG', ($rsREH->FeldInhalt('REH_KURZBEZEICHNUNG')),10, 200, $EditRecht,'','','','','','','',8);
        $Form->ZeileEnde();
        
        $Form->Trennzeile('O');
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_ATUNRPRAEFIX'] . ':', 150);
        $Form->Erstelle_TextFeld('!REH_ATUNRPRAEFIX', ($rsREH->FeldInhalt('REH_ATUNRPRAEFIX')),100, 300, $EditRecht);
        $Form->ZeileEnde();
        $Form->ZeileStart('font-size:8pt');
        $Form->Erstelle_TextLabel('&nbsp',150);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['txt_REH_ATUNRPRAEFIX'],600);
        $Form->ZeileEnde();

        if ($AWIS_KEY1!='')
        {
	        $Form->Trennzeile('O');
	        
	        $Form->ZeileStart();
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['REH_SONDERBESTNR'] . ':', 150);
	        $Form->Erstelle_TextFeld('REH_SONDERBESTNR', ($rsREH->FeldInhalt('REH_SONDERBESTNR')), 100, 300, $EditRecht);
	        $Form->ZeileEnde();
	        $Form->ZeileStart('font-size:8pt');
	        $Form->Erstelle_TextLabel('&nbsp',150);
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['txt_REH_SONDERBESTNR'],600);
	        $Form->ZeileEnde();
        }
            
        $Form->Trennzeile('O');

		//***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht15400 & 6) != 0) // Bearbeiten/Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	    }
	
	    if (($Recht15400 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	
	    if (($Recht15400 & 8) == 8 AND !isset($_POST['cmdDSNeu_x']))
	    {
	        $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	    }
	
	    $Form->SchaltflaechenEnde();
    }

    $Form->SchreibeHTMLCode('</form>');
    
    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $AWISBenutzer->ParameterSchreiben("Formular_REH", serialize($Param));

    $Form->SetzeCursor($AWISCursorPosition);
    
}
catch (awisException $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201306241012");
    }
    else
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}
catch (Exception $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201306241013");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param, &$BindeVariablen)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if (floatval($AWIS_KEY1) != 0)
    {
        $Bedingung.= 'AND REH_KEY = :var_N0_REH_KEY';
        $BindeVariablen['var_N0_REH_KEY'] = $AWIS_KEY1;
        return $Bedingung;
    }
    
    if (isset($Param['REH_KEY']) AND $Param['REH_KEY'] != '-1')
    {
    	$Bedingung .= 'AND REH_KEY = :var_N0_REH_KEY';
        $BindeVariablen['var_N0_REH_KEY'] = $Param['REH_KEY'];
   	}
    
    return $Bedingung;
}

?>