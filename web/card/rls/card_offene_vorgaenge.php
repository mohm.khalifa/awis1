<?php
ob_start();  //Einschalten der Pufferung von Ausgaben

require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

//Funktionen

//Gibt Firmenname zur�ck, bei �bergabe der Kundennr

function firma_ermitteln($strKundennr)
{
	global $hdlDb2Verbindung;
	global $tabKunden;
	global $tabAdressen;
	
	//Firma ermitteln
	$strSql='select a."Kunden_Art",b."Firma",b."Abteilung",b."Titel",b."Name",b."Vorname",b."Geschlecht"
			from '.$tabKunden.' as a left outer join '.$tabAdressen.' as b
			on a."Kunden_Nr"=b."Kunden_Nr" and b."Adress_Art"=\'01\'
			where a."Kunden_Nr"=\''.$strKundennr.'\'';
	
	$arrAdressen=abfrage("db2",$strSql);
	
	$strFirma='';
	
	if($arrAdressen["zeilen"])
	{
		$arrAdressen=$arrAdressen["trefferarray"];
		$strAdressfeld = '';
		
		if(trim($arrAdressen["Kunden_Art"][0])=='PR')
		{
			if(trim($arrAdressen["Firma"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Firma"][0])." ";
				
			if(trim($arrAdressen["Abteilung"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Abteilung"][0]);
		}
		
		$strAdressfeld=trim($strAdressfeld);
		
		//$strGeschlecht=trim($arrAdressen["Geschlecht"][0]);
		
		if(trim($arrAdressen["Vorname"][0])!=''||trim($arrAdressen["Name"][0])!='')
		{
			if(trim($arrAdressen["Firma"][0])!=''||trim($arrAdressen["Abteilung"][0])!='')
				$strAdressfeld.=" / ";
		
			/*if($strGeschlecht=='1')
				$strAdressfeld.="Herrn ".$strAnredeEnde;
			elseif($strGeschlecht=='2')
				$strAdressfeld.="Frau ".$strAnredeEnde;
			
			if(trim($arrAdressen["Titel"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Titel"][0])." ";*/
			
			if(trim($arrAdressen["Name"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Name"][0])." ";
			
			if(trim($arrAdressen["Vorname"][0])!='')
				$strAdressfeld.=trim($arrAdressen["Vorname"][0]);
		}
		
		$strFirma=$strAdressfeld;
	}
	return $strFirma;
}

//Ende Funktionen
	

if(isset($strDatVon))		//Abfrage erstellen
{
	$strWhere='';
	$strUntertitel='(';
	
	//DTA
	
	if(isset($strDatVon)&&isset($strDatBis))
	{
		$strWhere.=" and to_date(vor_rgdatum,'DD.MM.RR') between '".$strDatVon."' and '".$strDatBis."'";
		
		if($strDatVon==$strDatBis)
			$strUntertitel.=" Abrechnungszeitraum: ".$strDatVon." /";
		else
			$strUntertitel.=" Abrechnungszeitraum: ".$strDatVon." - ".$strDatBis." /";
	}
	
	//Zahlungsziel
	
	if(isset($strZahlZiel))
	{
		$strUntertitel.=" Zahlungsziel: ";
		$arrZahlZiel=explode(",",$strZahlZiel);
		
		if(count($arrZahlZiel)==1)
		{
			if($arrZahlZiel[0]=='zzu')
			{
				$strWhere.=" and to_date(sysdate,'DD.MM.RR')>vor_zahlungsziel";
				$strUntertitel.="�berschritten /";
			}
			elseif($arrZahlZiel[0]=='zzn')
			{
				$strWhere.=" and to_date(sysdate,'DD.MM.RR')<=vor_zahlungsziel";
				$strUntertitel.="noch nicht erreicht /";
			}
		}
		else
		{
			$strUntertitel.="noch nicht erreicht + �berschritten /";
		}
	}
	
	//Zahlung
	if(isset($strZahlung))
	{
		$strUntertitel.=" Zahlung: ";
		$arrZahlung=explode(",",$strZahlung);
		
		if(count($arrZahlung)==1)
		{
			if($arrZahlung[0]=='zahla')
			{
				$strWhere.=" and vor_gesamtbetrag<0";
				$strUntertitel.="ausstehend /";
			}
			elseif($arrZahlung[0]=='zahlu')
			{
				$strWhere.=" and vor_gesamtbetrag>0";
				$strUntertitel.="�berbezahlt /";
			}
		}
		else
		{
			$strUntertitel.="ausstehend + �berbezahlt /";
		}
		
	}
	
	//Mahnstand
	
	if(isset($strMahnstand))
	{
		$strUntertitel.=" Mahnstand: ";
		
		$strSql="select nvl(mst_text,'') mahnstufe from mahnstufen where mst_id in(".$strMahnstand.") order by mst_id";
	
		$arrMST = abfrage("ora",$strSql);
		
		for($i=0;$i<$arrMST["zeilen"];$i++)
		{
			$strUntertitel.=$arrMST["trefferarray"]["MAHNSTUFE"][$i].", ";
		}
		
		$strUntertitel=trim($strUntertitel);
		$strUntertitel=substr($strUntertitel,0,strlen($strUntertitel)-1)." /";
		
		$strWhere.=" and vor_mahnstufe in(".$strMahnstand.")";
	}
	
	$strUntertitel=substr($strUntertitel,0,strlen($strUntertitel)-1);
	
	$strUntertitel.=")";
	
	//Sortierung nach
	
	if(isset($strSort)&&$strSort=='dta')
		$strOrderBy='vor_rgdatum,vor_kunden_nr,vor_rnr';
	else
		$strOrderBy='vor_kunden_nr,vor_rgdatum,vor_rnr';
		
	$strSql="select VOR_RNR, VOR_KUNDEN_NR, VOR_RGDATUM, to_char(VOR_ZAHLUNGSZIEL,'DD.MM.YYYY') VOR_ZAHLUNGSZIEL, VOR_FORDERUNG,
	VOR_ZAHLUNGSEINGANG, VOR_MG_MINUS, VOR_MG_PLUS, VOR_GESAMTBETRAG VOR_OFFEN, (VOR_FORDERUNG + VOR_MG_MINUS) VOR_GESAMTFORDERUNG,nvl(mst_text,'') vor_mahnstufe
	from view_offene_rechnungen,mahnstufen where vor_mahnstufe=mst_id(+)".$strWhere." order by ".$strOrderBy;
	
	$arrOffenRG = abfrage("ora",$strSql);
	$intZeilen = $arrOffenRG["zeilen"];
	$arrOffenRG = $arrOffenRG["trefferarray"];
}


if(!isset($strDatVon)||$intZeilen==0)
{
?>
<html>
<head>
	<title>Offene Vorg&auml;nge</title>
	<?php include("card/css/css_card_master.php"); ?>
	<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
	<script language="JavaScript">
	
	function init()		//Aufruf �ber <body onload="init();">
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>parent.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
		document.ondblclick = zoom;
	}
	
	
	//Bei Klick auf Button PDF erstellen
	function pdf_erstellen()
	{
		//Pr�fung Zahlung
		var anzahl = 0;
		var obj=document.getElementsByName('zahl');
		
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].checked)
				anzahl++;
		}
		
		if(anzahl==0)
		{
			alert(unescape("Mindestens ein Eintrag %22Zahlung%22 muss ausgew%E4hlt werden!"));
			return
		}
		
		//Pr�fung Zahlungsziel
		var anzahl = 0;
		var obj=document.getElementsByName('zz');
		
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].checked)
				anzahl++;
		}
		
		if(anzahl==0)
		{
			alert(unescape("Mindestens ein Eintrag %22Zahlungsziel%22 muss ausgew%E4hlt werden!"));
			return
		}
		
		//Pr�fung Mahnstand
		var anzahl = 0;
		var obj=document.getElementsByName('mst');
		
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].checked)
				anzahl++;
		}
		
		if(anzahl==0)
		{
			alert(unescape("Mindestens ein Eintrag %22Mahnstand%22 muss ausgew%E4hlt werden!"));
			return
		}
		
		//Werte abfragen
		
		//Abrechnungszeitraum
			
		var obj=document.getElementById('RgDatumVon');
		var strDatVon=obj.options[obj.selectedIndex].text;
		
		obj=document.getElementById('RgDatumBis');
		var strDatBis=obj.options[obj.selectedIndex].text;
		
		//Sortierung nach
		
		if(document.getElementById('sortabrzeitraum').checked)
			strSort='dta';
		else
			strSort='kundennr';
		
		//Zahlung
		
		var strZahlung='';
		var obj=document.getElementsByName('zahl');
			
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].checked)
				strZahlung+=obj[i].id+',';
		}
		
		strZahlung=strZahlung.substr(0,strZahlung.length-1);
		
		//Zahlungsziel
		
		var strZahlZiel='';
		var obj=document.getElementsByName('zz');
			
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].checked)
				strZahlZiel+=obj[i].id+',';
		}
		
		strZahlZiel=strZahlZiel.substr(0,strZahlZiel.length-1);
		
		//Mahnstand
		
		var strMahnstand='';
		var obj=document.getElementsByName('mst');
			
		for(i=0;i<obj.length;i++)
		{
			if(obj[i].checked)
				strMahnstand+=obj[i].id+',';
		}
		
		strMahnstand=strMahnstand.substr(0,strMahnstand.length-1);
		
		self.location.href="<?php echo $PHP_SELF;?>?strDatVon="+strDatVon+"&strDatBis="+strDatBis+"&strSort="+strSort+"&strZahlung="+
		strZahlung+"&strZahlZiel="+strZahlZiel+"&strMahnstand="+strMahnstand;
		
	}
	
	</script>
</head>
<?php
if(isset($intZeilen)&&$intZeilen==0)
	echo "<body onload=\"init();alert(unescape('Zur aktuellen Abfrage liegen keine Datens%E4tze vor!'));\">";
else
	echo "<body onload=\"init();\">";

$strSql="select distinct VOR_RGDATUM
from view_offene_rechnungen 
order by vor_rgdatum";

$arrRGDatum = abfrage("ora",$strSql);
$intZeilen = $arrRGDatum["zeilen"];
$arrRGDatum = $arrRGDatum["trefferarray"];

$strRgDatumVon='';
$strRgDatumBis='';

for($i=0;$i<$intZeilen;$i++)
{
	if($i==0)
		$strRgDatumVon.="<option selected>";
	else
		$strRgDatumVon.="<option>";
		
	if($i==$intZeilen-1)
		$strRgDatumBis.="<option selected>";
	else
		$strRgDatumBis.="<option>";
		
	$strRgDatumVon.=$arrRGDatum["VOR_RGDATUM"][$i]."</option>";
	$strRgDatumBis.=$arrRGDatum["VOR_RGDATUM"][$i]."</option>";
}

?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="100%" >
<tr><td align="center">

	<table border="0" cellpadding="0" cellspacing="0" align="center" id="tabelleaussen">
		<tr><td colspan="9" class="fett" align="center">Offene Vorg&auml;nge</td></tr>
		<tr><td colspan="9" class="unten">&nbsp;</td></tr>
		<tr>
			<td rowspan="2" class="fett">Abrechnungszeitraum</td>
			<td rowspan="2" class="rechts">&nbsp;</td>
			<td class="rl">von:</td>
			<td>
			<select id="RgDatumVon">
			<?php echo $strRgDatumVon; ?>
			</select>
			</td>
			<td rowspan="2" class="links">&nbsp;</td>
			<td rowspan="2" class="fett">Zahlungsziel</td>
			<td rowspan="2" class="rechts">&nbsp;</td>
			<td>noch nicht erreicht</td>
			<td width="100" align="center"><input type="checkbox" id="zzn" name="zz" checked></td>
		</tr>
		<tr>
			<td class="rl">bis:</td>
			<td>
			<select id="RgDatumBis">
			<?php echo $strRgDatumBis; ?>
			</select>
			</td>
			<td>&uuml;berschritten</td>
			<td align="center"><input type="checkbox" id="zzu" name="zz" checked></td>
		</tr>
		<tr><td colspan="9" class="obenunten">&nbsp;</td></tr>
		<tr>
			<td rowspan="2" class="fett">Sortierung nach</td>
			<td rowspan="2">&nbsp;</td>
			<td>DTA</td>
			<td align="center"><input type="checkbox" id="sortabrzeitraum" onclick="document.getElementById('sortkundennr').click();" checked></td>
			<td rowspan="5" class="links">&nbsp;</td>
			<td rowspan="5" class="fett">Mahnstand</td>
			<td rowspan="5" class="rechts">&nbsp;</td>
			<td>Rechnung</td>
			<td align="center"><input type="checkbox" id="0" name="mst" checked></td>
		</tr>
		<tr>
			<td>Kundennr</td>
			<td align="center"><input type="checkbox" id="sortkundennr" onclick="document.getElementById('sortabrzeitraum').click();"></td>
			<td>Zahlungserinnerung</td>
			<td align="center"><input type="checkbox" id="1" name="mst" checked></td>
		</tr>
		<tr>
			<td colspan="4" class="obenunten">&nbsp;</td>
			<td>2. Mahnung</td>
			<td align="center"><input type="checkbox" id="2" name="mst" checked></td>
		</tr>
		<tr>
			<td rowspan="2" class="fett">Zahlung</td>
			<td rowspan="2">&nbsp;</td>
			<td>ausstehend</td>
			<td align="center"><input type="checkbox" id="zahla" name="zahl" checked></td>
			<td>3. Mahnung</td>
			<td align="center"><input type="checkbox" id="3" name="mst" checked></td>
		</tr>
		<tr>
			<td>&uuml;berbezahlt</td>
			<td align="center"><input type="checkbox" id="zahlu" name="zahl" checked></td>
			<td>Inkassoabgabe</td>
			<td align="center"><input type="checkbox" id="4" name="mst" checked></td>
		</tr>
		<tr>
			<td colspan="9" class="oben">&nbsp;</td>
		</tr>
		<tr><td colspan="9" align="center"><button type="button" onclick="pdf_erstellen();">PDF erstellen</button></td></tr>
	</table>
</td></tr>
</table>
</body>
</html>

<?php

flush();		//Ausgabe-Puffer leeren
}
else
{
	require_once("card/card_pdf_offene_vorgaenge.inc.php");	// PDF
	
	ob_end_clean();	//Verwirft den Inhalt des Ausgabe-Puffers und deaktiviert die Pufferung
		
	$dblSeitenrandLinks=10;
	$dblSeitenrandOben=10;
	$dblSeitenrandRechts=10;
	$dblSeitenrandUnten=10;
	
	if(isset($strSort)&&$strSort=='dta')	//Sortierung nach DTA
	{
		$strTitel="Offene Vorg�nge RLS nach DTA";
	}
	else
	{
		$strTitel="Offene Vorg�nge RLS nach KdNr";
	}
	
	$pdf=new PDF_OV('L','mm','A4',0,0,$dblSeitenrandLinks,$dblSeitenrandOben,$dblSeitenrandRechts,$dblSeitenrandUnten,$strTitel,$strUntertitel);	//A4-Seite im Querformat mit Seitenrand 10 mm
	
	if(isset($strSort)&&$strSort=='dta')	//Sortierung nach DTA
	{	
		$dblSumDTARgBetrag=0;
		$dblSumDTAMgMinus=0;
		$dblSumDTAGesamtBetrag=0;
		$dblSumDTAMgPlus=0;
		$dblSumDTAZahlungseingang=0;
		$dblSumDTAOffen=0;
		
		$dblSumDTARgBetragGesamt=0;
		$dblSumDTAMgMinusGesamt=0;
		$dblSumDTAGesamtBetragGesamt=0;
		$dblSumDTAMgPlusGesamt=0;
		$dblSumDTAZahlungseingangGesamt=0;
		$dblSumDTAOffenGesamt=0;
		
		for($i=0;$i<$intZeilen;$i++)
		{
			$dblSumDTARgBetrag+=punktersetztkomma($arrOffenRG["VOR_FORDERUNG"][$i]);
			$dblSumDTAMgMinus+=punktersetztkomma($arrOffenRG["VOR_MG_MINUS"][$i]);
			$dblSumDTAGesamtBetrag+=punktersetztkomma($arrOffenRG["VOR_GESAMTFORDERUNG"][$i]);
			$dblSumDTAMgPlus+=punktersetztkomma($arrOffenRG["VOR_MG_PLUS"][$i]);
			$dblSumDTAZahlungseingang+=punktersetztkomma($arrOffenRG["VOR_ZAHLUNGSEINGANG"][$i]);
			$dblSumDTAOffen+=punktersetztkomma($arrOffenRG["VOR_OFFEN"][$i]);
			
			$pdf->pruefUmbruch($arrOffenRG["VOR_RGDATUM"][$i],$arrOffenRG["VOR_RGDATUM"][$i-1],$arrOffenRG["VOR_RGDATUM"][$i+1]);	//Pr�fen, ob neue Seite begonnen werden soll
			
			//Zeile mit DTA und Datum
			if($arrOffenRG["VOR_RGDATUM"][$i]!=$arrOffenRG["VOR_RGDATUM"][$i-1])
			{
				$dblSchriftgroesse=12;
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+2;
				
				$strDTA="DTA ".$arrOffenRG["VOR_RGDATUM"][$i];
				$pdf->Cell(0,$dblZeilenhoehe,$strDTA,'B',1,'L');
				
				if($pdf->dblHoeheDTA==0)
					$pdf->dblHoeheDTA=$dblZeilenhoehe;
			}
			
			//Firma ermitteln
			
			$strFirma=firma_ermitteln($arrOffenRG["VOR_KUNDEN_NR"][$i]);
			
			//Zeile mit Kunden-Nr und Firmenname
			
			$dblSchriftgroesse=10.5;
			$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
			$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+2;
			
			$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,array(40,0),
			array($arrOffenRG["VOR_KUNDEN_NR"][$i],$strFirma),
			array(0,0),array(0,1),array('L','L'));
			
			if($pdf->dblHoeheName==0)
				$pdf->dblHoeheName=$dblZeilenhoehe;
			
			//Datenzeile offener Vorgang
			
			$dblSchriftgroesse=10.5;
			$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
			$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
			
			if($pdf->dblHoeheDaten==0)
				$pdf->dblHoeheDaten=$dblZeilenhoehe;
			
			$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array($arrOffenRG["VOR_RNR"][$i],eurobetrag($arrOffenRG["VOR_FORDERUNG"][$i]),eurobetrag($arrOffenRG["VOR_MG_MINUS"][$i]),
				eurobetrag($arrOffenRG["VOR_GESAMTFORDERUNG"][$i]),eurobetrag($arrOffenRG["VOR_MG_PLUS"][$i]),
				eurobetrag($arrOffenRG["VOR_ZAHLUNGSEINGANG"][$i]),eurobetrag($arrOffenRG["VOR_OFFEN"][$i]),$arrOffenRG["VOR_ZAHLUNGSZIEL"][$i],
				$arrOffenRG["VOR_MAHNSTUFE"][$i]),
				array(1,1,1,1,1,1,1,1,1),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','C','L'));
			
			while($arrOffenRG["VOR_KUNDEN_NR"][$i]==$arrOffenRG["VOR_KUNDEN_NR"][$i+1])
			{
				$pdf->pruefUmbruchSchleife($pdf->dblHoeheDaten);	//Pr�fen, ob neue Seite begonnen werden soll
				
				$i++;
				$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array($arrOffenRG["VOR_RNR"][$i],eurobetrag($arrOffenRG["VOR_FORDERUNG"][$i]),eurobetrag($arrOffenRG["VOR_MG_MINUS"][$i]),
				eurobetrag($arrOffenRG["VOR_GESAMTFORDERUNG"][$i]),eurobetrag($arrOffenRG["VOR_MG_PLUS"][$i]),
				eurobetrag($arrOffenRG["VOR_ZAHLUNGSEINGANG"][$i]),eurobetrag($arrOffenRG["VOR_OFFEN"][$i]),$arrOffenRG["VOR_ZAHLUNGSZIEL"][$i],
				$arrOffenRG["VOR_MAHNSTUFE"][$i]),
				array(1,1,1,1,1,1,1,1,1),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','C','L'));
				
				$dblSumDTARgBetrag+=punktersetztkomma($arrOffenRG["VOR_FORDERUNG"][$i]);
				$dblSumDTAMgMinus+=punktersetztkomma($arrOffenRG["VOR_MG_MINUS"][$i]);
				$dblSumDTAGesamtBetrag+=punktersetztkomma($arrOffenRG["VOR_GESAMTFORDERUNG"][$i]);
				$dblSumDTAMgPlus+=punktersetztkomma($arrOffenRG["VOR_MG_PLUS"][$i]);
				$dblSumDTAZahlungseingang+=punktersetztkomma($arrOffenRG["VOR_ZAHLUNGSEINGANG"][$i]);
				$dblSumDTAOffen+=punktersetztkomma($arrOffenRG["VOR_OFFEN"][$i]);
			}
			
			//Summenzeile DTA
			
			if($arrOffenRG["VOR_RGDATUM"][$i]!=$arrOffenRG["VOR_RGDATUM"][$i+1])
			{
				$pdf->SetY($pdf->GetY()+$pdf->dblLinienStaerke);
				
				$dblSchriftgroesse=10.5;
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
				
				$pdf->pruefUmbruchSchleife($dblZeilenhoehe);	//Pr�fen, ob neue Seite begonnen werden soll
				
				$aktX=$pdf->GetX();
				$aktY=$pdf->GetY();
				
				$pdf->SetFillColor(192,192,192);
				$pdf->Cell(0,$dblZeilenhoehe,"",'B',1,'L',1);
				
				$pdf->SetXY($aktX,$aktY);
				
				$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Summe ".$arrOffenRG["VOR_RGDATUM"][$i].":",eurobetrag($dblSumDTARgBetrag),eurobetrag($dblSumDTAMgMinus),
				eurobetrag($dblSumDTAGesamtBetrag),eurobetrag($dblSumDTAMgPlus),
				eurobetrag($dblSumDTAZahlungseingang),eurobetrag($dblSumDTAOffen),""),
				array(0,0,0,0,0,0,0,0),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','L'));
				
				if($pdf->dblHoeheSumme==0)
					$pdf->dblHoeheSumme=$dblZeilenhoehe;
				
				$dblSumDTARgBetragGesamt+=$dblSumDTARgBetrag;
				$dblSumDTAMgMinusGesamt+=$dblSumDTAMgMinus;
				$dblSumDTAGesamtBetragGesamt+=$dblSumDTAGesamtBetrag;
				$dblSumDTAMgPlusGesamt+=$dblSumDTAMgPlus;
				$dblSumDTAZahlungseingangGesamt+=$dblSumDTAZahlungseingang;
				$dblSumDTAOffenGesamt+=$dblSumDTAOffen;
				
				$dblSumDTARgBetrag=0;
				$dblSumDTAMgMinus=0;
				$dblSumDTAGesamtBetrag=0;
				$dblSumDTAMgPlus=0;
				$dblSumDTAZahlungseingang=0;
				$dblSumDTAOffen=0;
			}
		}
		
		//Gesamtsumme
		
		$dblSchriftgroesse=10.5;
		$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
		$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
		
		$pdf->pruefUmbruchSchleife(2*$dblZeilenhoehe);	//Pr�fen, ob neue Seite begonnen werden soll
		
		$pdf->Ln($dblZeilenhoehe); //Zeilenumbruch
		
		$pdf->SetLineWidth(0.4);
		
		$aktX=$pdf->GetX();
		$aktY=$pdf->GetY();
				
		$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',1);
				
		$pdf->SetXY($aktX,$aktY);
		
		$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Gesamtsumme",eurobetrag($dblSumDTARgBetragGesamt),eurobetrag($dblSumDTAMgMinusGesamt),
				eurobetrag($dblSumDTAGesamtBetragGesamt),eurobetrag($dblSumDTAMgPlusGesamt),
				eurobetrag($dblSumDTAZahlungseingangGesamt),eurobetrag($dblSumDTAOffenGesamt),""),
				array(0,0,0,0,0,0,0,0),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','L'));
	}
	else	//Sortierung nach Kundennr
	{
		$dblSumDTARgBetrag=0;
		$dblSumDTAMgMinus=0;
		$dblSumDTAGesamtBetrag=0;
		$dblSumDTAMgPlus=0;
		$dblSumDTAZahlungseingang=0;
		$dblSumDTAOffen=0;
		
		$strSql="select distinct VOR_RGDATUM from view_offene_rechnungen where 1=1".$strWhere." order by VOR_RGDATUM";
		$arrDTA = abfrage("ora",$strSql);
		
		$arrGesamtSummeDTA=array();
		
		for($i=0;$i<$arrDTA["zeilen"];$i++)
		{
			$datDTA=$arrDTA["trefferarray"]["VOR_RGDATUM"][$i];
			
			$arrGesamtSummeDTA[$datDTA]=array();
			
			$arrGesamtSummeDTA[$datDTA]["RgBetrag"]=0;
			$arrGesamtSummeDTA[$datDTA]["MgMinus"]=0;
			$arrGesamtSummeDTA[$datDTA]["GesamtBetrag"]=0;
			$arrGesamtSummeDTA[$datDTA]["MgPlus"]=0;
			$arrGesamtSummeDTA[$datDTA]["Zahlungseingang"]=0;
			$arrGesamtSummeDTA[$datDTA]["Offen"]=0;
		}
					
		for($i=0;$i<$intZeilen;$i++)
		{
			$dblSumDTARgBetrag+=punktersetztkomma($arrOffenRG["VOR_FORDERUNG"][$i]);
			$dblSumDTAMgMinus+=punktersetztkomma($arrOffenRG["VOR_MG_MINUS"][$i]);
			$dblSumDTAGesamtBetrag+=punktersetztkomma($arrOffenRG["VOR_GESAMTFORDERUNG"][$i]);
			$dblSumDTAMgPlus+=punktersetztkomma($arrOffenRG["VOR_MG_PLUS"][$i]);
			$dblSumDTAZahlungseingang+=punktersetztkomma($arrOffenRG["VOR_ZAHLUNGSEINGANG"][$i]);
			$dblSumDTAOffen+=punktersetztkomma($arrOffenRG["VOR_OFFEN"][$i]);
			
			$pdf->SetLineWidth($pdf->dblLinienStaerke);
		
			$pdf->pruefUmbruch($arrOffenRG["VOR_RGDATUM"][$i],$arrOffenRG["VOR_RGDATUM"][$i-1],$arrOffenRG["VOR_RGDATUM"][$i+1],1,
			$arrOffenRG["VOR_KUNDEN_NR"][$i],$arrOffenRG["VOR_KUNDEN_NR"][$i-1],$arrOffenRG["VOR_KUNDEN_NR"][$i+1]);	//Pr�fen, ob neue Seite begonnen werden soll
			
			//Zeile Kundennr / Firma
				
			if($arrOffenRG["VOR_KUNDEN_NR"][$i]!=$arrOffenRG["VOR_KUNDEN_NR"][$i-1])
			{
				//Firma ermitteln
				$strFirma=firma_ermitteln($arrOffenRG["VOR_KUNDEN_NR"][$i]);
			
				$dblSchriftgroesse=10.5;
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
				
				if($pdf->dblHoeheName==0)
					$pdf->dblHoeheName=$dblZeilenhoehe;
					
				$aktX=$pdf->GetX();
				$aktY=$pdf->GetY();
				
				$pdf->SetFillColor(192,192,192);
				$pdf->Cell(0,$dblZeilenhoehe,"",'T',1,'L',1);
				
				$pdf->SetXY($aktX,$aktY);
			
				$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,array(40,0),
				array($arrOffenRG["VOR_KUNDEN_NR"][$i],$strFirma),
				array(0,0),array(0,1),array('L','L'));
			}
			
			//Zeile mit DTA
			
			if($arrOffenRG["VOR_KUNDEN_NR"][$i]!=$arrOffenRG["VOR_KUNDEN_NR"][$i-1]||$arrOffenRG["VOR_RGDATUM"][$i]!=$arrOffenRG["VOR_RGDATUM"][$i-1])
			{
				$dblSchriftgroesse=10.5;
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
						
				$strDTA="DTA ".$arrOffenRG["VOR_RGDATUM"][$i];
				$pdf->Cell(0,$dblZeilenhoehe,$strDTA,'T',1,'L');
						
				if($pdf->dblHoeheDTA==0)
					$pdf->dblHoeheDTA=$dblZeilenhoehe;
			}
						
			//Datenzeile offener Vorgang
			
			$dblSchriftgroesse=10.5;
			$pdf->SetFont('Helvetica','',$dblSchriftgroesse);
			$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
			
			if($pdf->dblHoeheDaten==0)
				$pdf->dblHoeheDaten=$dblZeilenhoehe;
			
			$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array($arrOffenRG["VOR_RNR"][$i],eurobetrag($arrOffenRG["VOR_FORDERUNG"][$i]),eurobetrag($arrOffenRG["VOR_MG_MINUS"][$i]),
				eurobetrag($arrOffenRG["VOR_GESAMTFORDERUNG"][$i]),eurobetrag($arrOffenRG["VOR_MG_PLUS"][$i]),
				eurobetrag($arrOffenRG["VOR_ZAHLUNGSEINGANG"][$i]),eurobetrag($arrOffenRG["VOR_OFFEN"][$i]),$arrOffenRG["VOR_ZAHLUNGSZIEL"][$i],
				$arrOffenRG["VOR_MAHNSTUFE"][$i]),
				array(1,1,1,1,1,1,1,1,1),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','C','L'));
			
			//Summenzeile DTA
			
			if($arrOffenRG["VOR_KUNDEN_NR"][$i]!=$arrOffenRG["VOR_KUNDEN_NR"][$i+1]||$arrOffenRG["VOR_RGDATUM"][$i]!=$arrOffenRG["VOR_RGDATUM"][$i+1])
			{
				$pdf->SetY($pdf->GetY()+$pdf->dblLinienStaerke);
				
				$dblSchriftgroesse=10.5;
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
				
				$aktX=$pdf->GetX();
				$aktY=$pdf->GetY();
						
				$pdf->Cell(0,$dblZeilenhoehe,"",'B',1,'L',0);
						
				$pdf->SetXY($aktX,$aktY);
				
				
				$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
				array("Summe ".$arrOffenRG["VOR_RGDATUM"][$i].":",eurobetrag($dblSumDTARgBetrag),eurobetrag($dblSumDTAMgMinus),
				eurobetrag($dblSumDTAGesamtBetrag),eurobetrag($dblSumDTAMgPlus),
				eurobetrag($dblSumDTAZahlungseingang),eurobetrag($dblSumDTAOffen),""),
				array(0,0,0,0,0,0,0,0),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','L'));
				
				if($pdf->dblHoeheSumme==0)
					$pdf->dblHoeheSumme=$dblZeilenhoehe;
				
				//Summe f�r Kunden_Nr
				
				$dblSumDTARgBetragGesamt+=$dblSumDTARgBetrag;
				$dblSumDTAMgMinusGesamt+=$dblSumDTAMgMinus;
				$dblSumDTAGesamtBetragGesamt+=$dblSumDTAGesamtBetrag;
				$dblSumDTAMgPlusGesamt+=$dblSumDTAMgPlus;
				$dblSumDTAZahlungseingangGesamt+=$dblSumDTAZahlungseingang;
				$dblSumDTAOffenGesamt+=$dblSumDTAOffen;
				
				//Summe f�r Abrechnungszeitr�ume
				
				$arrGesamtSummeDTA[$arrOffenRG["VOR_RGDATUM"][$i]]["RgBetrag"]+=$dblSumDTARgBetrag;
				$arrGesamtSummeDTA[$arrOffenRG["VOR_RGDATUM"][$i]]["MgMinus"]+=$dblSumDTAMgMinus;
				$arrGesamtSummeDTA[$arrOffenRG["VOR_RGDATUM"][$i]]["GesamtBetrag"]+=$dblSumDTAGesamtBetrag;
				$arrGesamtSummeDTA[$arrOffenRG["VOR_RGDATUM"][$i]]["MgPlus"]+=$dblSumDTAMgPlus;
				$arrGesamtSummeDTA[$arrOffenRG["VOR_RGDATUM"][$i]]["Zahlungseingang"]+=$dblSumDTAZahlungseingang;
				$arrGesamtSummeDTA[$arrOffenRG["VOR_RGDATUM"][$i]]["Offen"]+=$dblSumDTAOffen;
									
				$dblSumDTARgBetrag=0;
				$dblSumDTAMgMinus=0;
				$dblSumDTAGesamtBetrag=0;
				$dblSumDTAMgPlus=0;
				$dblSumDTAZahlungseingang=0;
				$dblSumDTAOffen=0;		
			}
			
			
			//Summe Kundennr
			
			if($arrOffenRG["VOR_KUNDEN_NR"][$i]!=$arrOffenRG["VOR_KUNDEN_NR"][$i+1])
			{
				//Gesamtsumme
		
				$dblSchriftgroesse=10.5;
				$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
				$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
				
				if($pdf->dblHoeheSummeKd==0)
					$pdf->dblHoeheSummeKd=$dblZeilenhoehe;
				
				$pdf->SetFillColor(192,192,192);
				
				$aktX=$pdf->GetX();
				$aktY=$pdf->GetY();
						
				$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',1);
						
				$pdf->SetXY($aktX,$aktY);
				
				$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
						array("Summe ".$arrOffenRG["VOR_KUNDEN_NR"][$i],eurobetrag($dblSumDTARgBetragGesamt),eurobetrag($dblSumDTAMgMinusGesamt),
						eurobetrag($dblSumDTAGesamtBetragGesamt),eurobetrag($dblSumDTAMgPlusGesamt),
						eurobetrag($dblSumDTAZahlungseingangGesamt),eurobetrag($dblSumDTAOffenGesamt),""),
						array(0,0,0,0,0,0,0,0),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','L'));	
						
				$pdf->Ln($dblZeilenhoehe); //Zeilenumbruch
				
				$dblSumDTARgBetragGesamt=0;
				$dblSumDTAMgMinusGesamt=0;
				$dblSumDTAGesamtBetragGesamt=0;
				$dblSumDTAMgPlusGesamt=0;
				$dblSumDTAZahlungseingangGesamt=0;
				$dblSumDTAOffenGesamt=0;
			}
		}
		
		$dblSummeRgBetragGesamt=0;
		$dblSummeMgMinusGesamt=0;
		$dblSummeGesamtBetragGesamt=0;
		$dblSummeMgPlusGesamt=0;
		$dblSummeZahlungseingangGesamt=0;
		$dblSummeOffenGesamt=0;
				
		//Summen DTA-Daten
		
		$dblSchriftgroesse=10.5;
		$pdf->SetFont('Helvetica','B',$dblSchriftgroesse);
		$dblZeilenhoehe=$pdf->ptinmm($dblSchriftgroesse)+1;
		
		//$pdf->Ln($dblZeilenhoehe); //Zeilenumbruch
		
		$pdf->SetLineWidth(0.4);
		
		$intSchalter=1;
		
		foreach($arrGesamtSummeDTA as $datDTA => $arrBetraege)
		{
			$pdf->pruefUmbruchSchleife(2*$dblZeilenhoehe);	//Pr�fen, ob neue Seite begonnen werden soll
			
			if($intSchalter)
			{
				$pdf->Cell(0,$dblZeilenhoehe,"",'B',1,'L',0);
				$intSchalter=0;
			}
			
			$pdf->SetLineWidth($pdf->dblLinienStaerke);
			
			$aktX=$pdf->GetX();
			$aktY=$pdf->GetY();
					
			$pdf->Cell(0,$dblZeilenhoehe,"",'B',1,'L',0);
					
			$pdf->SetXY($aktX,$aktY);
			
			$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
					array("Summe ".$datDTA.":",eurobetrag($arrBetraege["RgBetrag"]),eurobetrag($arrBetraege["MgMinus"]),
					eurobetrag($arrBetraege["GesamtBetrag"]),eurobetrag($arrBetraege["MgPlus"]),
					eurobetrag($arrBetraege["Zahlungseingang"]),eurobetrag($arrBetraege["Offen"]),""),
					array(0,0,0,0,0,0,0,0),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','L'));
					
					
			$dblSummeRgBetragGesamt+=$arrBetraege["RgBetrag"];
			$dblSummeMgMinusGesamt+=$arrBetraege["MgMinus"];
			$dblSummeGesamtBetragGesamt+=$arrBetraege["GesamtBetrag"];
			$dblSummeMgPlusGesamt+=$arrBetraege["MgPlus"];
			$dblSummeZahlungseingangGesamt+=$arrBetraege["Zahlungseingang"];
			$dblSummeOffenGesamt+=$arrBetraege["Offen"];
		}
		
		$pdf->pruefUmbruchSchleife(2*$dblZeilenhoehe);	//Pr�fen, ob neue Seite begonnen werden soll
		
		$pdf->SetLineWidth(0.4);
		
		$pdf->Cell(0,$dblZeilenhoehe,"",'T',1,'L',0);
		
		$aktX=$pdf->GetX();
		$aktY=$pdf->GetY();
				
		$pdf->Cell(0,$dblZeilenhoehe,"",'T,B',1,'L',1);
				
		$pdf->SetXY($aktX,$aktY);
		
		$pdf->zeileOffene_Vorgaenge($dblZeilenhoehe,$pdf->dblKopfFeldAbstand,$pdf->arrKopfSpaltenbreiten,
					array("Gesamtsumme",eurobetrag($dblSummeRgBetragGesamt),eurobetrag($dblSummeMgMinusGesamt),
					eurobetrag($dblSummeGesamtBetragGesamt),eurobetrag($dblSummeMgPlusGesamt),
					eurobetrag($dblSummeZahlungseingangGesamt),eurobetrag($dblSummeOffenGesamt),""),
					array(0,0,0,0,0,0,0,0),$pdf->arrKopfPos,array('L','R','R','R','R','R','R','L'));
	}
	
	$pdf->SetDisplayMode('fullwidth','continuous');
	
	$pdf->Output();
}	//Ende else



?>