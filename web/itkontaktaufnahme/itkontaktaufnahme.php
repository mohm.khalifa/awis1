<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=WIN1252">
    <meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
    <meta http-equiv="author" content="ATU">

<?php

require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisBenutzer.inc';

$AWISBenutzer = awisBenutzer::Init('');
$Form = new awisFormular();
$DB = awisDatenbank::NeueVerbindung('AWIS');
include("awisHeader3.inc");    // Kopfzeile

$TextKonserven = array();
$TextKonserven[] = array('KONTAKTAUFNAHME', '%');
$TextKonserven[] = array('Wort','lbl_hilfe');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

echo '<title>' . $AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_TITEL"] . '</title>';

if ($AWISBenutzer->HatDasRecht(58000) == 0) {
    $Form->Fehler_Anzeigen('Rechte', '', 'MELDEN', -9, "200809161548");
    die();
}

echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei(3) . ">";
echo '<div class="RegisterInhalt" id="reg_inhalt">';

$Form->Formular_Start();
//Infotafeln liegen unter /include/infotafeln
$Form->awisInfotafel(6, $AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_TICKETSUCHE"], 1, 350, '', 'height:38px; text-align: center; line-height: 1; padding-left: 0; padding-top: 0', '', $AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_TT1"]);
$Form->awisInfotafel(4, $AWISSprachKonserven['KONTAKTAUFNAHME']["KONTAKTAUFNAHME_ERSTELLEN"], 1, 350, '', 'height:38px; text-align: center; line-height: 1; padding-left: 0; padding-top: 0', '', $AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_TT2"]);
$Form->awisInfotafel(5, $AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_SUPPORT"], 1, 350, '', 'height:38px; text-align: center; line-height: 1; padding-left: 0; padding-top: 0', '', $AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_TT3"]);
$Form->Formular_Ende();

$Form->SchaltflaechenStart();
$Form->Schaltflaeche('href', 'cmdHilfe', "/hilfe/hilfe_Main.php?ID=HB_IT-Kontaktaufnahme_V1_0", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
$Form->SchaltflaechenEnde();

echo '</div>';



