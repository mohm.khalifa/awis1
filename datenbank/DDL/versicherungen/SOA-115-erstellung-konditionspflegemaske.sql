/***************************************************************************
* Tabellendefinitionen : VERSKONDREF
* Erstellt am : 17.03.2017 16:29:42
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DECLARE Anz integer;
BEGIN
  SELECT Count(*) INTO Anz FROM USER_TABLES WHERE TABLE_NAME='VERSKONDREF';
  If (Anz > 0) Then
    Execute IMMEDIATE 'DROP TABLE VERSKONDREF CASCADE CONSTRAINTS PURGE';
  END IF;
END;
/
CREATE TABLE VERSKONDREF(
VKR_KEY NUMBER CONSTRAINT CNN_VKR_KEY NOT NULL,
VKR_VVE_KEY NUMBER DEFAULT '0',
VKR_SKZ VARCHAR(10),
VKR_GUELTIG_AB DATE,
VKR_GUELTIG_BIS DATE,
CONSTRAINT PK_VKR_KEY PRIMARY KEY(VKR_KEY) USING Index TABLESPACE INDX) 
;


/***************************************************************************
* Kommentare : VERSKONDREF
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/



/***************************************************************************
* Indexdefinitionen : VERSKONDREF
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Sequenz erstellen : VERSKONDREF
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DECLARE Anz integer;
BEGIN
  SELECT Count(*) INTO Anz FROM USER_SEQUENCES WHERE SEQUENCE_NAME='SEQ_VKR_KEY';
  If (Anz > 0) Then
    Execute IMMEDIATE 'DROP SEQUENCE SEQ_VKR_KEY';
  END IF;
END;
/
CREATE SEQUENCE SEQ_VKR_KEY INCREMENT BY 1 START WITH 1 NOCACHE;

/*DROP TRIGGER trg_seq_VKR_KEY;*/

CREATE OR REPLACE TRIGGER trg_seq_VKR_KEY
     BEFORE INSERT ON VERSKONDREF
     REFERENCING NEW AS NEW OLD AS OLD
     FOR EACH ROW
       BEGIN
         :NEW.VKR_KEY := seq_VKR_KEY.NEXTVAL ;
     END;
/ 


/***************************************************************************
* FOREIGN KEYS
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/


/***************************************************************************
* Tabelle dokumentieren
* Erstellt am : 17.03.2017 16:29:42
****************************************************************************
*    Syntax: ORACLE 10.x
***************************************************************************/

DELETE FROM TabellenKuerzel WHERE XTN_KUERZEL='VKR';

INSERT INTO TabellenKuerzel(XTN_KUERZEL, XTN_TABELLENNAME, XTN_DATENQUELLE)
 VALUES('VKR', 'VERSKONDREF', 'ORACLE')
;
DELETE FROM Textkonserven WHERE XTX_KENNUNG='VKR' AND XTX_BEREICH='Tabelle';

INSERT INTO Textkonserven(XTX_KENNUNG, XTX_BEREICH, XTX_Bemerkung, XTX_TEXT_DE)
 VALUES('VKR','Tabelle', 'VERSKONDREF', 'VERSKONDREF')
;


/***************************************************************************
* Ende des Skripts VERSKONDREF
****************************************************************************
* Fertig am : 17.03.2017 16:29:42
***************************************************************************/

COMMIT;

--Feld fuer Referenztabelle hinzufuegen
ALTER TABLE VERSKONDITIONEN 
ADD (VKD_VKR_KEY NUMBER );


--Referenztabelle befuellen
INSERT
INTO VERSKONDREF
  (
    VKR_VVE_KEY,
    VKR_GUELTIG_AB,
    VKR_GUELTIG_BIS,
    VKR_SKZ
  )
SELECT DISTINCT VVE_KEY, TRUNC(VKD_GUELTIG_AB), TRUNC(VKD_GUELTIG_BIS), VERSKONDITIONEN.VKD_SKZ
FROM VERSVERSICHERUNGEN
INNER JOIN VERSKONDITIONEN
ON VVE_VERSNR = VKD_VVE_VERSNR;

COMMIT;

--Merge in die Tabelle Verskonditionen fuer die Referenz
MERGE INTO VERSKONDITIONEN VKD USING
(SELECT *
FROM VERSKONDREF VKR
INNER JOIN VERSVERSICHERUNGEN VVE
ON VKR.VKR_VVE_KEY       = VVE.VVE_KEY
) VKR ON (VKR.VVE_VERSNR = VKD.VKD_VVE_VERSNR 
          AND VKR.VKR_SKZ = VKD.VKD_SKZ 
          AND TRUNC(VKR.VKR_GUELTIG_BIS) = TRUNC(VKD.VKD_GUELTIG_BIS)
          AND TRUNC(VKR.VKR_GUELTIG_AB) = TRUNC(VKD.VKD_GUELTIG_AB))
WHEN MATCHED THEN
  UPDATE SET VKD.VKD_VKR_KEY = VKR.VKR_KEY;
COMMIT;

INSERT
INTO "AWIS"."RECHTE"
  (
    XRC_ID,
    XRC_RECHT
  )
  VALUES
  (
    '9400',
    'Versicherungen Konditionspflege'
  );
INSERT
INTO "AWIS"."RECHTE"
  (
    XRC_ID,
    XRC_RECHT
  )
  VALUES
  (
    '350',
    'Versicherungen: Men�punkt'
  );
INSERT
INTO "AWIS"."RECHTESTUFEN"
  (
    XRS_XRC_ID,
    XRS_BIT,
    XRS_BESCHREIBUNG
  )
  VALUES
  (
    '350',
    '0',
    'Anzeigen'
  );
INSERT
INTO "AWIS"."RECHTESTUFEN"
  (
    XRS_XRC_ID,
    XRS_BIT,
    XRS_BESCHREIBUNG
  )
  VALUES
  (
    '9400',
    '0',
    'Anzeigen der Daten'
  );
INSERT
INTO "AWIS"."RECHTESTUFEN"
  (
    XRS_XRC_ID,
    XRS_BIT,
    XRS_BESCHREIBUNG
  )
  VALUES
  (
    '9400',
    '1',
    'Bearbeiten der Daten'
  );
INSERT
INTO "AWIS"."WEBMENUE"
  (
    XME_KEY,
    XME_BEZEICHNUNG,
    XME_USER,
    XME_USERDAT
  )
  VALUES
  (
    14,
    'Versicherungen',
    'Nina R�sch',
    TO_DATE('2017-03-15 11:04:25', 'YYYY-MM-DD HH24:MI:SS')
  );
  
UPDATE WEBMENUEPUNKTE
SET XMP_XME_KEY =
  (SELECT xme_key FROM webmenue WHERE xme_bezeichnung = 'Versicherungen'
  ),
  XMP_BEZEICHNUNG = 'Vorgangsbearbeitung',
  XMP_LINK        = '/versicherungen/vorgangsbearbeitung/versicherungen_Main.php'
WHERE XMP_XRC_ID  = '9500';

INSERT
INTO "AWIS"."WEBMENUEPUNKTE"
  (
    XMP_XME_KEY,
    XMP_BEZEICHNUNG,
    XMP_XRC_ID,
    XMP_STUFE,
    XMP_GRUPPENID,
    XMP_SORTIERUNG,
    XMP_LINK,
    XMP_USER,
    XMP_USERDAT
  )
  VALUES
  (
    (SELECT xme_key FROM webmenue WHERE xme_bezeichnung = 'Versicherungen'
    ),
    'Konditionspflege',
    '9400',
    '1',
    '0',
    '211',
    '/versicherungen/konditionen/verskonditionen_Main.php',
    'Nina R�sch',
    TO_DATE('2017-03-15 11:04:25', 'YYYY-MM-DD HH24:MI:SS')
  );
  
INSERT
INTO "AWIS"."WEBMENUEPUNKTE"
  (
    XMP_XME_KEY,
    XMP_BEZEICHNUNG,
    XMP_XRC_ID,
    XMP_STUFE,
    XMP_GRUPPENID,
    XMP_SORTIERUNG,
    XMP_LINK,
    XMP_USER,
    XMP_USERDAT
  )
  VALUES
  (
    '1',
    'Versicherungen',
    '350',
    '1',
    '0',
    '555',
    '/versicherungen/index.php',
    'Nina R�sch',
    TO_DATE('2017-03-15 10:24:52', 'YYYY-MM-DD HH24:MI:SS')
  );
  
  UPDATE "AWIS"."TEXTKONSERVEN" SET XTX_TEXT_DE = 'Vorgangsbearbeitung' WHERE XTX_KENNUNG = 'MENUE_39_TEXT';
  
  INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_TEXT_DE, XTX_TEXT_CZ, XTX_TEXT_NL, XTX_TEXT_IT, XTX_USERDAT) VALUES ('MENUE_218_TEXT', 'MENUE', 'Versicherungen', 'Poji�ten�', 'Verzekeringen', 'Assicurazioni', TO_DATE('2017-03-15 14:56:50', 'YYYY-MM-DD HH24:MI:SS'));
  INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('MENUE_14', 'MENUE', 'Versicherungen', 'Versicherungen');
  INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('MENUE_218_HINWEIS', 'MENUE', 'Versicherungen', 'Vorgangsbearbeitung und Konditionspflege');
  INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('MENUE_222_TEXT', 'MENUE', 'Versicherungskonditionen', 'Versicherungskonditionen', TO_DATE('2016-07-05 12:38:08', 'YYYY-MM-DD HH24:MI:SS'));
  INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('MENUE_222_HINWEIS', 'MENUE', 'Versicherungskonditionen', 'Pflege der Versicherungskonditionen', TO_DATE('2017-03-15 15:12:34', 'YYYY-MM-DD HH24:MI:SS'));
  
  INSERT INTO "AWIS"."WEBMASKEN" (XWM_ID, XWM_BEZEICHNUNG, XWM_BEMERKUNG, XWM_USER, XWM_USERDAT, XWM_STATUS) VALUES ('9400', 'Versicherungen Konditionen', 'Onlineversicherungen', 'Nina R�sch', TO_DATE('2017-03-15 15:41:00', 'YYYY-MM-DD HH24:MI:SS'), 'A');
  INSERT INTO "AWIS"."WEBMASKENREGISTER" (XRG_ID, XRG_XWM_ID, XRG_BEZEICHNUNG, XRG_BESCHRIFTUNG, XRG_AKTION, XRG_SEITE, XRG_BEMERKUNG, XRG_XRC_ID, XRG_SORTIERUNG, XRG_NEUEHILFE) VALUES ('94001', '9400', 'Details', '<u>D</u>etails', 'verskonditionen_Main.php?cmdAktion=Details', './verskonditionen_Details.php', 'Zeigt Details zu den Versicherungskonditionen', '9400', '1', '0');
  INSERT INTO "AWIS"."WEBMASKENREGISTER" (XRG_ID, XRG_XWM_ID, XRG_BEZEICHNUNG, XRG_BESCHRIFTUNG, XRG_AKTION, XRG_SEITE, XRG_BEMERKUNG, XRG_XRC_ID, XRG_SORTIERUNG, XRG_NEUEHILFE) VALUES ('94000', '9400', 'Suche', '<u>S</u>che', 'verskonditionen_Main.php?cmdAktion=Suche', './verskonditionen_Suche.php', 'Suchseite Versicherungskonditionen', '9400', '1', '0');
  INSERT INTO "AWIS"."PROGRAMMPARAMETER" (XPP_ID, XPP_BEZEICHNUNG, XPP_DEFAULT, XPP_UEBERSCHREIBBAR, XPP_BEMERKUNG, XPP_KODIERT, XPP_XRC_ID, XPP_DATENTYP) VALUES ('200131', 'Formular_VKD', 'a:0:{}', '255', 'Parameter f�r die Parameterhistorie', '1', '0', 'T');
    
UPDATE "AWIS"."TEXTKONSERVEN" SET XTX_BEMERKUNG = 'Versicherungskonzern', XTX_TEXT_DE = 'Konzern' WHERE XTX_KENNUNG = 'VVE_KUERZEL';
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('VVE_VERSICHERUNG', 'VVE', 'Versicherung', 'Versicherung', TO_DATE('2017-03-15 16:58:15', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_SCHADENSTYP', 'VKD', 'Schadenstyp', 'Schadenstyp');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('tit_verskonditionen', 'TITEL', 'Versicherungskonditionen', 'Versicherungskonditionen');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_ARTNR', 'VKD', 'Aritkelnummer', 'Artnr.');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_GUELTIG_AB', 'VKD', 'Gueltig ab', 'G�ltig ab');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_GUELTIG_BIS', 'VKD', 'Gueltig bis', 'G�ltig bis');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VVE_AKTIV', 'VVE', 'Online-Versicherung', 'Online-Versicherung');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VVE_VERARBWEG', 'VVE', 'Erstellung Versicherungsrechnung', 'Erstellung Versicherungsrechnung');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_POSITION', 'VKD', 'Position', 'Position');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_PREIS_NETTO', 'VKD', 'Preis Netto', 'Preis Netto');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_PREIS_BRUTTO', 'VKD', 'Preis Brutto', 'Preis Brutto');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_VSTABZUG', 'VKD', 'Sonderkonditon f�r VSt-Abzugsberechtigte', 'Sonderkonditon f�r VSt-Abzugsberechtigte');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_STEUERSATZ', 'VKD', 'Steuersatz', 'MWST (%)');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_MAX_AUSBUCHUNG', 'VKD', 'Max. Ausb.', 'Max. Ausb.');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_MIN_AUSBUCHUNG', 'VKD', 'Min. Ausb.', 'Min. Ausb.');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_SB', 'VKD', 'H�he Selbstbeteiligung', 'H�he SB');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_HOECHSTGRENZE', 'VKD', 'Max. Kassenbetr.', 'Max. Kassenbetr.');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('VKD_KOSTENLOS', 'VKD', 'kostenlos', 'kostenlos');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE) VALUES ('lst_VKD_STEUERSATZ', 'VKD', 'Steuersatz  in Konditionen', '19~19|7~7');
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('reg_94000', 'Register', 'Suche', 'S<u>u</u>che', TO_DATE('2017-03-15 16:27:33', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('reg_94001', 'Register', 'Details', '<u>D</u>etails', TO_DATE('2017-03-15 16:27:33', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO "AWIS"."TEXTKONSERVEN" (XTX_KENNUNG, XTX_BEREICH, XTX_BEMERKUNG, XTX_TEXT_DE, XTX_USERDAT) VALUES ('VVE_VERSNR', 'VVE', 'Debitorennummer', 'Debitorennr.', TO_DATE('2017-03-17 08:30:31', 'YYYY-MM-DD HH24:MI:SS'));

COMMIT;





