<?php
require_once 'awisBerichte.inc';

class ber_CRMKonditionsBlattMail extends awisBerichte
{
	
	private $_UebschriftGueltig = '';
	
	private $_ZusatzSommer = '';
	
	private $_ZusatzWinter = '';
	
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
		$this->_Dateiname = $Parameter['DATEINAME'];
		$this->_BerichtsVorlage= '/daten/web/dokumente/vorlagen/' . $Parameter['VORLAGE'];
		$this->_UebschriftGueltig = $Parameter['UEBERSCHRIFT_GUELTIG'];
		$this->_ZusatzSommer = $Parameter['ZUSATZ_SOMMER'];
		$this->_ZusatzWinter = $Parameter['ZUSATZ_WINTER'];

		if(file_exists($this->_BerichtsVorlage))
		{
		    $this->setSourceFile($this->_BerichtsVorlage);
		}
		else
		{
		    $this->_BerichtsVorlage='';
		}
		
		//*************************************
		// Daten fuer den Bericht laden
		//*************************************
		$SQL = "SELECT CAD_NAME1,CAD_NAME2,CAD_STRASSE,CAD_HAUSNUMMER,CAD_PLZ,CAD_LAN_CODE,CAD_ORT,";
		$SQL .= " CAD_KEY,CAD_STATUS," . ($Parameter['BEREICH'] == 0 ? "CKO_LETZTEAKTION," : "") . "CAQ_DATUM,CAQ_VERTRAGSNUMMER,KEYWG,KEYDL,KEYRH,";
		$SQL .= " KEYTH,TEXT_SOMMER,TEXT_WINTER,REIFENBEZ,PORTAL_USER,PORTAL_USERPWD,KON_NAME1,KON_NAME2,LAND";
		$SQL .= " FROM (";
		$SQL .= " SELECT CAD_NAME1,CAD_NAME2,CAD_STRASSE,CAD_HAUSNUMMER,CAD_PLZ,CAD_LAN_CODE,CAD_ORT,CAD_KEY,";
		$SQL .= " CAD_STATUS," . ($Parameter['BEREICH'] == 0 ? "CKO_LETZTEAKTION," : "") . "CAQ_DATUM,CAQ_VERTRAGSNUMMER,NVL(KEYWG,0) as KEYWG,";
		$SQL .= " NVL(KEYDL,0) as KEYDL,NVL(KEYRH,0) as KEYRH,NVL(KEYTH,0) as KEYTH,TEXT_SOMMER,TEXT_WINTER,";
		$SQL .= " REIFENBEZ,PORTAL_USER,PORTAL_USERPWD,KON_NAME1,KON_NAME2,KON_KEY,LAND";
		$SQL .= " FROM (";
		$SQL .= " SELECT CAD_NAME1,CAD_NAME2,CAD_STRASSE,CAD_HAUSNUMMER,CAD_PLZ,CAD_LAN_CODE,CAD_ORT,";
		$SQL .= " CAD_KEY,CAD_STATUS," . ($Parameter['BEREICH'] == 0 ? "CKO_LETZTEAKTION," : "") . "CAQ_DATUM,CAQ_VERTRAGSNUMMER,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1100 AND ROWNUM=1) AS KEYWG,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1110 AND ROWNUM=1) AS KEYDL,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1) AS KEYRH,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1121 AND ROWNUM=1) AS KEYTH,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1018 AND ROWNUM=1) AS TEXT_SOMMER,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1019 AND ROWNUM=1) AS TEXT_WINTER,";
		$SQL .= " (select CKM_BEZEICHNUNG from crmkonditionsmodelle where ckm_key =";
		$SQL .= " (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1120 AND ROWNUM=1)) AS REIFENBEZ,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1002 AND ROWNUM=1) AS PORTAL_USER,";
		$SQL .= " (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1003 AND ROWNUM=1) AS PORTAL_USERPWD,";
		$SQL .= " KON_NAME1,KON_NAME2,KON_KEY,";
		$SQL .= " (SELECT CIN_WERT FROM CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1013 AND ROWNUM = 1) AS LAND";
		$SQL .= " FROM crmAdressen";
		if ($Parameter['BEREICH'] == 0)
		{
			$SQL .= " INNER JOIN crmAktionen ON cad_key = cko_xxx_key AND cko_cat_key = 6";
		}
		$SQL .= " LEFT OUTER JOIN crmAkquisestand ON cad_key = caq_cad_key";
		$SQL .= " LEFT OUTER JOIN Kontakte ON KON_KEY = CAD_KON_KEY";
		$SQL .= " )";
		$SQL .= " ) LEFT OUTER JOIN V_CRM_KONDITIONEN ON CIN_WG = KEYWG AND CIN_DL = KEYDL AND CIN_RH = KEYRH";
		$SQL .= " WHERE cad_key = " . $this->_DB->WertSetzen('CAD','N0',$Parameter['CAD_KEY']);

		// Daten laden
		$this->_rec = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('CAD'));
		
		//*************************************
		// Alle Textbausteine laden
		//*************************************
		$TextKonserven[]=array('CAD','*');
		$TextKonserven[]=array('Ausdruck','txtFirmaVertraulich');
		$TextKonserven[]=array('Ausdruck','txtKonditionen');
		$TextKonserven[]=array('SYSTEM','PROGNAME');
		$TextKonserven[]=array('Ausdruck','txtUeberschriftFlottenkundenKonditionen');
		$TextKonserven[]=array('Ausdruck','txtHinweisPortal');
		$TextKonserven[]=array('Wort','Benutzername');
		$TextKonserven[]=array('Wort','Kennwort');
		$TextKonserven[]=array('Ausdruck','txtAlleWertezzglMwSt');
		$TextKonserven[]=array('Ausdruck','txtKonditionenSchluss');
		$TextKonserven[]=array('Ausdruck','txtKonditionenHinweisReifen');
		$TextKonserven[]=array('Wort','Sommer');
		$TextKonserven[]=array('Wort','Winter');
		
		$this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
	}
	
	/**
	 * Eigentliches PDF erzeugen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// Startpositionen setzen
		$this->_Zeile = $this->_Parameter['Seite1-Oben'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins(0,0,0);
		$this->AliasNbPages();
		$this->SetAutoPageBreak(false);
		
		$this->NeueSeite();
		
		// Fusszeile Seite 1
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse-Klein']);
		$this->SetXY(10,self::SeitenHoehe()-3);
		$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L');
		$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtKonditionen'],0,0,'C');
		$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],date('d.m.Y'),0,0,'R');
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('CAD_NAME1')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('CAD_NAME2')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('CAD_STRASSE'). ' ' .$this->_rec->FeldInhalt('CAD_HAUSNUMMER')),0,0,'L');
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rec->FeldInhalt('CAD_PLZ'). ' ' .$this->_rec->FeldInhalt('CAD_ORT')),0,0,'L');

		// Abstand von Adresse zu Überschrift
		$this->_Zeile += $this->_Parameter['Zeilenhoehe-Gross'];

		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Ueberschrift-Groesse']);
		
		$UebSchrift = str_replace('#DATUM#', $this->_UebschriftGueltig,$this->_AWISSprachkonserven['Ausdruck']['txtUeberschriftFlottenkundenKonditionen']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$UebSchrift,0,0,'L');
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Adresse-Groesse']);
		$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T','Vertragsnummer: ' . $this->_rec->FeldInhalt('CAQ_VERTRAGSNUMMER')) ,0,0,'L');
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$Bereiche = array('KEYWG','KEYDL','KEYTH','KEYRH');
		$flagReifen = false;	
		
		foreach ($Bereiche as $Bereich)
		{
			$SQL = "SELECT CKD_BEREICH, CKD_BEZEICHNUNG, CKM_BASIS, CKD_TEXT, CKD_TEXT2,"; 
			$SQL .= " CKM_KEY, CKD_BEMERKUNG, CKM_BEZEICHNUNG";
			$SQL .= " FROM CRMKONDITIONSMODELLE";
			$SQL .= " LEFT OUTER JOIN CRMKONDITIONSMODELLDETAILS ON CKM_KEY = CKD_CKM_KEY";
			$SQL .= " WHERE CKM_KEY = ". $this->_DB->WertSetzen('CKD','N0',$this->_rec->FeldInhalt($Bereich));
			$SQL .= " AND CKD_STATUS = " . $this->_DB->WertSetzen('CKD','T','A');
			$SQL .= " ORDER BY CKD_SORTIERUNG";
			
			$rsKonMod = $this->_DB->RecordSetOeffnen($SQL,$this->_DB->Bindevariablen('CKD'));
			$anz = $rsKonMod->AnzahlDatensaetze();
			
			$LetzterBereich = '';	
			
			while(!$rsKonMod->EOF() AND $anz > 0)
			{				
				if($LetzterBereich!=$rsKonMod->FeldInhalt('CKD_BEREICH'))
				{
					$this->SetFillColor(200,200,200);
					
					if($Bereich=='KEYRH' AND $rsKonMod->FeldInhalt('CKM_BEZEICHNUNG')!='RBest')
					{
						// wenn Konditionen Reifen R1,R2 und groesser hinterlegt ist dann
						// muss weiterer Zusatztext angezeigt werden (daher Flag auf true)
						$flagReifen = true;
						
						$this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
						$this->SetXY($this->_Spalte,$this->_Zeile);
						$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
						$this->Cell(50,$this->_Parameter['Zeilenhoehe'],'Reifen: siehe Seite 2',0,0,'L');
						
						$this->NeueSeite();
						$this->_Zeile = $this->_Parameter['Zeilenhoehe'] * 7;
						
						// Fusszeile Seite 2
						$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse-Klein']);
						$this->SetXY(10,self::SeitenHoehe()-3);
						$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L');
						$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtKonditionen'],0,0,'C');
						$this->Cell((self::SeitenBreite()-20)/3,$this->_Parameter['Zeilenhoehe'],date('d.m.Y'),0,0,'R');
					}
					else
					{
						$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
					}
					
					$this->SetXY($this->_Spalte,$this->_Zeile);
					$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['ZwUeberschrift-Groesse']);
					$this->Cell(self::SeitenBreite()-30,$this->_Parameter['Zeilenhoehe'],$rsKonMod->FeldInhalt('CKD_BEREICH').''.($rsKonMod->FeldInhalt('CKM_BASIS')!=''?': '.$rsKonMod->FeldInhalt('CKM_BASIS'):''),0,0,'L',true);
					
					if($Bereich=='KEYRH')
					{
						$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
						$this->SetXY($this->_Spalte+self::SeitenBreite()-100,$this->_Zeile);
						$this->SetFont($this->_Parameter['Schriftart'],'B',$this->_Parameter['Text-Groesse']);
						$this->Cell(35,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Wort']['Sommer'].' '.$this->_ZusatzSommer,0,0,'R');
						$this->Cell(35,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Wort']['Winter'].' '.$this->_ZusatzWinter,0,0,'R');
					}
					$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
					
					$LetzterBereich=$rsKonMod->FeldInhalt('CKD_BEREICH');					
				}
				
				$this->SetXY($this->_Spalte,$this->_Zeile);
				$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
				//if($rsKonMod->FeldInhalt('CKD_TEXT2')=='')
					
				if(is_null($this->_DB->FeldInhaltFormat('T',$rsKonMod->FeldInhalt('CKM_BASIS')))
				   OR $this->_DB->FeldInhaltFormat('T',$rsKonMod->FeldInhalt('CKM_BASIS')) == 'null')					
				{					
					if($Bereich=='KEYWG')
					{
						$Text = $rsKonMod->FeldInhalt('CKD_BEZEICHNUNG').' '.$rsKonMod->FeldInhalt('CKD_BEMERKUNG');
					}
					else
					{
						$Text = $rsKonMod->FeldInhalt('CKD_BEZEICHNUNG');
					}
					$this->Cell(self::SeitenBreite()-60,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('TP',$Text),0,0,'L');
					
					$this->Cell(30,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',str_replace(chr(164), chr(128), $rsKonMod->FeldInhalt('CKD_TEXT'))),0,0,'R');					
				}
				else	// bei Reifen gibt es zwei Bereiche
				{
					if($Bereich=='KEYRH')
					{
						$this->Cell(self::SeitenBreite()-180,$this->_Parameter['Zeilenhoehe'],$rsKonMod->FeldInhalt('CKD_BEZEICHNUNG'),0,0,'L');
						$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse-Klein']);
						$this->Cell(80,$this->_Parameter['Zeilenhoehe'],$rsKonMod->FeldInhalt('CKD_BEMERKUNG'),0,0,'L');
						$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
					}
					else
					{
						$this->Cell(self::SeitenBreite()-100,$this->_Parameter['Zeilenhoehe'],$rsKonMod->FeldInhalt('CKD_BEZEICHNUNG'),0,0,'L');
					}
					
					// Sommer = Text2
					$this->Cell(35,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',str_replace(chr(164), chr(128), $rsKonMod->FeldInhalt('CKD_TEXT2'))),0,0,'R');
					// Winter = Text
					$this->Cell(35,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',str_replace(chr(164), chr(128), $rsKonMod->FeldInhalt('CKD_TEXT'))),0,0,'R');
				}
				
				$this->_Zeile += $this->_Parameter['Zeilenhoehe'];

				$rsKonMod->DSWeiter();
			}
			
		}	
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		
		// Zugang zum Portal
		if($this->_rec->FeldInhalt('PORTAL_USER')!='')
		{
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
			$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtHinweisPortal'],0,0,'L');
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Wort']['Benutzername'],0,0,'L');
			$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_rec->FeldInhalt('PORTAL_USER'),0,0,'L');
			$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
			
			$this->SetXY($this->_Spalte,$this->_Zeile);
			$this->Cell(40,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Wort']['Kennwort'],0,0,'L');
			$this->Cell(100,$this->_Parameter['Zeilenhoehe'],$this->_rec->FeldInhalt('PORTAL_USERPWD'),0,0,'L');
		}
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->SetFont($this->_Parameter['Schriftart'],'',$this->_Parameter['Text-Groesse']);
		$this->Cell(self::SeitenBreite()-30,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtAlleWertezzglMwSt'],0,0,'L');
		$this->SetXY($this->_Spalte,$this->_Zeile);
		
		$this->_Zeile += $this->_Parameter['Zeilenhoehe'];
		
		$this->SetXY($this->_Spalte,$this->_Zeile);
		$this->MultiCell(self::Seitenbreite()-30,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtKonditionenSchluss'],0,'L',0);
		
		if($flagReifen)
		{
			$this->SetXY($this->_Spalte,$this->GetY());
			$this->MultiCell(self::Seitenbreite()-30,$this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['Ausdruck']['txtKonditionenHinweisReifen'],0,'L',0);
		}
		
		//*********************************************************************
		// Berichtsende -> Zurückliefern
		//*********************************************************************
		
		switch($Ausgabeart)
		{
			case 1:			// PDF im extra Fenster -> Download
				$this->Output($this->_BerichtsName.'.pdf','D');
				break;
			case 2:			// Standard-Stream
				$this->Output($this->_BerichtsName.'.pdf','I');
				break;
			case 3:			// Als String zurückliefern
				return($this->Output($this->_BerichtsName.'.pdf','S'));
				break;
			case 4:				// Als Datei speichern
				$this->Output($this->_Dateiname,'F');
				break;
		}
	}
	
	private function NeueSeite()
	{
		$this->AddPage();
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	public function Header()
	{	
	    if($this->_BerichtsVorlage!='')
	    {
	        $pdf_vorlage = $this->ImportPage(1);		// Erste Seite aus der Vorlage importieren
	        $this->useTemplate($pdf_vorlage);
	    }
	}
}