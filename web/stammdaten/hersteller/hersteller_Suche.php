<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('HER', '%');
    $TextKonserven[] = array('HER', 'HER_ID');
    $TextKonserven[] = array('WUG', 'WUG_ID');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht602 = $AWISBenutzer->HatDasRecht(602);
    if ($Recht602 == 0) {
        awisEreignis(3, 1000, 'CRM', $AWISBenutzer->BenutzerName(), '', '', '');
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        echo "<br><br><input type=image title='" . $AWISSprachKonserven['Wort']['lbl_zurueck'] . "' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    echo "<br>";
    echo "<form name=frmSuche method=post action=./hersteller_Main.php?cmdAktion=Details>";

    /**********************************************
     * Eingabemaske
     ***********************************************/

    $Form->Formular_Start();

    $Form->ZeileStart();
    $AWISCursorPosition = 'sucHER_ID';
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HER']['HER_ID'] . ':', 190);
    $SQL = 'SELECT HER_ID, HER_ID || \' - \' || HER_BEZEICHNUNG as HER_BEZEICHNUNG FROM HERSTELLER';
    $SQL .= ' ORDER BY HER_ID';
    $Form->Erstelle_SelectFeld('~HER_ID', '', '300:300', true, $SQL, '~' . $AWISSprachKonserven['Wort']['Auswahl_ALLE']);
    $AWISCursorPosition = 'sucHER_ARTIKELNUMMER';
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>