<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20090220
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KEW','%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','ttt_AuswahlSpeichern');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht9200=$AWISBenutzer->HatDasRecht(9200);
	if($Recht9200==0)
	{
	    awisEreignis(3,1000,'KEW',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./kennwoerter_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KEW'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
	}

	$Form->Formular_Start();


	// Name des Mitbewerbers
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BEZEICHNUNG'].':',190);
	$Form->Erstelle_TextFeld('*KEW_BEZEICHNUNG',($Param['SPEICHERN']=='on'?$Param['KEW_BEZEICHNUNG']:''),25,200,true);
	$AWISCursorPosition='sucKEW_BEZEICHNUNG';
	$Form->ZeileEnde();

	// Strasse
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BEREICH'].':',190);
	$Form->Erstelle_TextFeld('*KEW_BEREICH',($Param['SPEICHERN']=='on'?$Param['KEW_BEREICH']:''),25,200,true);
	$Form->ZeileEnde();

	// Ort
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BENUTZERNAME'].':',190);
	$Form->Erstelle_TextFeld('*KEW_BENUTZERNAME',($Param['SPEICHERN']=='on'?$Param['KEW_BENUTZERNAME']:''),25,200,true);
	$Form->ZeileEnde();

	// Postleitzahl
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_KUNDENNUMMER'].':',190);
	$Form->Erstelle_TextFeld('*KEW_KUNDENNUMMER',($Param['SPEICHERN']=='on'?$Param['KEW_KUNDENNUMMER']:''),5,200,true);
	$Form->ZeileEnde();

	// Filiale
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BEMERKUNG'].':',190);
	$Form->Erstelle_TextFeld('*KEW_BEMERKUNG',($Param['SPEICHERN']=='on'?$Param['KEW_BEMERKUNG']:''),5,200,true);
	$Form->ZeileEnde();

	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	if(($Recht9200&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	/*
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=mitbewerb&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
	*/
	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151301");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151302");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>