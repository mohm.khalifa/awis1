/* css_kazeinzelsatz_haupt.php */


<?php
switch($intSchriftgroesse)
{
	case 12:	//Werte f�r die Breite der Spalten

				$intDat=65;						//Breite der Spalten Datum
				$intBank=30;					//Breite des Feldes Bank
				$intKaz=102;					//Breite des Feldes Kontoauszug
				$intVerw=235;					//Breite der Spalte Verwendungszweck
				$intTA2=38;						//H�he einer Textarea mit 2 Zeilen
				$intTA3=55;						//H�he einer Textarea mit 3 Zeilen
				$intBa=235;						//Breite der Spalte Buchungstext
				$intAg=235;						//Breite der Spalte Auftraggeber
				$intAgb=235;					//Breite der Spalte Auftraggeberbank
				$intBetr=89;					//Breite der Spalte Betrag
				$intBetrrls=89;					//Breite der Spalte RLSBetrag
				$intBetroffen=89;				//Breite des Feldes Betrag offen
				$intZwZeile=8;					//H�he der Zwischenraumzeile
				$intZeile2=16;					//H�he der Zeile 2 (innerhalb Kontoauszugblock pro Datensatz)
				$intDataft=70;					//Breite der Spalten Datum Aufteilung
				$intRnr=150;					//Breite der Spalte Rechnungsnr
				$intBua=300;					//Breite der Spalte Buchungsart
				break;				
	case 16:	//Werte f�r die Breite der Spalten

				$intDat=80;						//Breite der Spalten Datum
				$intBank=35;					//Breite des Feldes Bank
				$intKaz=127;					//Breite des Feldes Kontoauszug
				$intVerw=300;					//Breite der Spalte Verwendungszweck
				$intTA2=42;						//H�he einer Textarea mit 2 Zeilen
				$intTA3=60;						//H�he einer Textarea mit 3 Zeilen
				$intBa=300;						//Breite der Spalte Buchungstext
				$intAg=300;						//Breite der Spalte Auftraggeber
				$intAgb=300;					//Breite der Spalte Auftraggeberbank
				$intBetr=120;					//Breite der Spalte Betrag
				$intBetrrls=120;				//Breite der Spalte RLSBetrag
				$intBetroffen=120;				//Breite des Feldes Betrag offen
				$intZwZeile=10;					//H�he der Zwischenraumzeile
				$intZeile2=18;					//H�he der Zeile 2 (innerhalb Kontoauszugblock pro Datensatz)
				$intDataft=85;					//Breite der Spalten Datum Aufteilung
				$intRnr=190;					//Breite der Spalte Rechnungsnr
				$intBua=400;					//Breite der Spalte Buchungsart
				break;
			
	default:	//Werte f�r die Breite der Spalten

				$intDat=80;						//Breite der Spalten Datum
				$intBank=35;					//Breite des Feldes Bank
				$intKaz=127;					//Breite des Feldes Kontoauszug
				$intVerw=300;					//Breite der Spalte Verwendungszweck
				$intTA2=39;						//H�he einer Textarea mit 2 Zeilen
				$intTA3=57;						//H�he einer Textarea mit 3 Zeilen
				$intBa=300;						//Breite der Spalte Buchungstext
				$intAg=300;						//Breite der Spalte Auftraggeber
				$intAgb=300;					//Breite der Spalte Auftraggeberbank
				$intBetr=110;					//Breite der Spalte Betrag
				$intBetrrls=110;				//Breite der Spalte RLSBetrag
				$intBetroffen=110;				//Breite des Feldes Betrag offen
				$intZwZeile=10;					//H�he der Zwischenraumzeile
				$intZeile2=18;					//H�he der Zeile 2 (innerhalb Kontoauszugblock pro Datensatz)
				$intDataft=85;					//Breite der Spalten Datum Aufteilung
				$intRnr=200;					//Breite der Spalte Rechnungsnr
				$intBua=300;					//Breite der Spalte Buchungsart
}
?>
									
.text								{	vertical-align:top;	}

.titel								{	font-weight:bold; }
td.titel							{	text-align:center; }

.dat,.val,#td_dat					{	width:<?php echo $intDat; ?>px; }

.bank								{	width:<?php echo $intBank; ?>px;
										text-align:center;
										font-weight:bold; }

.kaz								{	width:<?php echo $intKaz; ?>px;
										text-align:center; }

.verw								{	width:<?php echo $intVerw; ?>px;
										height:<?php echo $intTA3; ?>px; }

.ba									{	width:<?php echo $intBa; ?>px; }

.ag 								{	width:<?php echo $intAg; ?>px;
										height:<?php echo $intTA2; ?>px; }

.agb 								{	width:<?php echo $intAgb; ?>px; 
										height:<?php echo $intTA2; ?>px;}

.betr								{	width:<?php echo $intBetr; ?>px; 
										text-align:right; }

.betrrls							{	width:<?php echo $intBetrrls; ?>px; 
										text-align:right; }
										
.betroffen							{	width:<?php echo $intBetroffen; ?>px; 
										text-align:right;
										font-weight:bold; }
										
.zwzeile							{	height:<?php echo $intZwZeile; ?>px; 
										font-size:1px; }
									
/* Wird zur Positionierung der einzelnen Kontoauszugsbl�cke gebraucht */

.zeile2								{	height:<?php echo $intZeile2; ?>px;
										font-size:1px;	}

.dataft								{	width:<?php echo $intDataft; ?>px;
										text-align:center; }									
												
.rnr								{	width:<?php echo $intRnr; ?>px;
										text-align:center; }
										
.bua								{	width:<?php echo $intBua; ?>px; }

.speibut							{	height:<?php echo $intElementhoehe; ?>;
										width:<?php echo $intElementhoehe; ?>;
										display:none; }
									
#dat								{	display:none;
										font-weight:bold; }			/*Felder neue Zeile sind versteckt*/
#bet,#bua,#rnr,#but					{	visibility:hidden;
										font-weight:bold; }
