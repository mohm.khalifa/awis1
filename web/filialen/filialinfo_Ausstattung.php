<?php
// Code gepr�ft, Sacha Kerres, 22.07.2005
global $SpeichernButton; // Anzeige eines Speichern-Knopfs
global $con;
global $awisRSZeilen;
global $AWISBenutzer;

$RechteStufe = awisBenutzerRecht($con, 114);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Filialen: Ausstattung',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

$SQL = "SELECT * FROM FILIALEN WHERE FIL_GRUPPE IS NOT NULL ";

	// FIL_ID als Parameter oder aus dem Form raus
$FILID =(isset($_REQUEST['FIL_ID'])?$_REQUEST['FIL_ID']:'');

// Alle Filialen anzeigen
If(!isset($_GET["Liste"]) AND $FILID == '')
{
	$foo = awis_BenutzerParameter($con, "_HILFSPARAM", strtoupper($AWISBenutzer->BenutzerName()));
	if(substr($foo,0,7)=="FIL_ID=")
	{
		$FILID=substr($foo,7,99);
	}
}

// Bedingung ausf�llen -> ID �bernehmen

if($FILID != '')
{
    $SQL .= " AND ( FIL_ID=0" . $FILID . ")";
}
else
{
 	$Params = explode(";", awis_BenutzerParameter($con, "FilialSuche", $AWISBenutzer->BenutzerName()));
    if($Params[0] != '')
    {
        $Bedingung .= "AND FIL_ID " . awisLIKEoderIST($Params[0], 0);
    }
     if($Params[1] != '')
    {
    	$Bedingung .= "AND UPPER(FIL_BEZ) " . awisLIKEoderIST("%" . $Params[1] . "%", 1, 0, 1);
    }
     if($Params[2] != '')
    {
    	$Bedingung .= "AND UPPER(FIL_ORT) " . awisLIKEoderIST('%'.$Params[2].'%', 1, 0, 1);
    }
    if($Params[3] != '')
    {
        $Bedingung .= "AND FIL_PLZ " . awisLIKEoderIST($Params[3], 0);
	}
	if($Params[5]!='')
	{
		$Bedingung .= "AND FIL_ID IN (SELECT DISTINCT PER_FIL_ID FROM PERSONAL WHERE SUCHWORT(PER_NACHNAME) " . awisLIKEoderIST("%" . $Params[5]. "%",1,0,1) . ")";
	}

    if($Bedingung != '')
    {
        $SQL .= " AND ( " . substr($Bedingung, 4) . ")";
    }
}

$rsFiliale = awisOpenRecordset($con, $SQL);
$rsFilialeAnz = $awisRSZeilen;

If($rsFilialeAnz > 1)
{ // Liste gefunden -> Liste anzeigen
 	print "<table border=0 width=100%>";
    for($i = 0;$i < $rsFilialeAnz;$i++)
    {
         print "<tr><td><a onmouseover=\"window.status='" . $rsFiliale["FIL_BEZ"][$i] . "';return true;\" href=./filialinfo_Main.php?cmdAktion=Ausstattung&FIL_ID=" . $rsFiliale["FIL_ID"][$i] . ">";
         print "" . $rsFiliale["FIL_BEZ"][$i] . " (" . $rsFiliale["FIL_ID"][$i] . ")</a></td>";
         print "<td>" . $rsFiliale["FIL_STRASSE"][$i] . "</td><td>" . $rsFiliale["FIL_PLZ"][$i] . " " . $rsFiliale["FIL_ORT"][$i] . " " . $rsFiliale["FIL_ORTSTEIL"][$i] . "</td><tr>";
    }
    print "</table>";
}
elseIf($rsFilialeAnz == 0)
{
	print "<p class=HinweisText>Es konnte keine Ausstattung f�r die gesuchte Filiale gefunden werden</p>";
}
else
{ // Eine Filiale
	// Aktuelle Filiale speichern
	awis_BenutzerParameterSpeichern($con, "_HILFSPARAM", $AWISBenutzer->BenutzerName(), "FIL_ID=" . $rsFiliale["FIL_ID"][0]);
		// Daten speichern
	if(isset($_POST["cmdSpeichern_x"]))
	{
		foreach($_POST as $Eintrag => $EintragWert)
		{
			if(strstr($Eintrag,"txtFeld_")!='')
			{
				$Nr = substr($Eintrag,8);
				if($EintragWert=='')
				{
					$SQL = "DELETE FROM FILIALINFOS WHERE FIF_FIL_ID=" . $_POST["FIL_ID"] . " AND FIF_FIT_ID=" . $Nr . "";
				}
				else
				{
			        $SQL = "BEGIN AWIS.AENDERN_FILIALINFOS(" . $FILID . ",$Nr,'" . $EintragWert . "','" . $AWISBenutzer->BenutzerName() . "'); END;";
			        if(awisExecute($con, $SQL)===false)
			        {
			        	echo '<br><span class=HinweisText>Fehler beim Speichern</span>';
			        }
				}
			}
		}
		print "<hr>";
	}

	     /**
         * Daten anzeigen
         */

        // Daten f�r die Infoseite sammeln
	$SQL = "SELECT  filialinfostypengruppen.ftg_id, filialinfostypengruppen.ftg_gruppe,
		       filialinfostypen.fit_information, FIT_ID, FIT_IMQ_ID, FIT_WERTE
				FROM filialinfostypen, filialinfostypengruppen
				 WHERE filialinfostypengruppen.ftg_id = filialinfostypen.fit_ftg_id
					AND (FTG_ID=8)
				 ORDER BY FTG_ID, fit_sortierung

			";	// Nur Telefonnummern
	$rsFilialInfo = awisOpenRecordset($con, $SQL);
	$rsFilialInfoAnz = $awisRSZeilen;

	print "<input name=FIL_ID type=hidden value=" . $rsFiliale["FIL_ID"][0] . ">";
    print "<input type=hidden name=cmdAktion value=Ausstattung>";

     // �berschrift schreiben
	print "<table width=100% border=0><tr><td align=left>";
    print "<span class=Ueberschrift>Filiale " . $rsFiliale["FIL_ID"][0] . "</span>";
    print "</td><td backcolor=#000000 align=right><img src=/bilder/NeueListe.png accesskey='T' alt='Trefferliste (Alt+T)' onclick=location.href='./filialinfo_Main.php?cmdAktion=Ausstattung&Liste=True';></td></tr></table>";
    print "<hr>";

	print "<table width=100%>";
    for($i=0;$i<$rsFilialInfoAnz;$i++)
	{
		print "<tr>";

		print "<td width=200 " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . $rsFilialInfo["FIT_INFORMATION"][$i] . "</td>";
		$rsFilialDaten = awisOpenRecordset($con, "SELECT * FROM FilialInfos WHERE FIF_FIL_ID=" . $rsFiliale["FIL_ID"][0] . " AND FIF_FIT_ID=" . $rsFilialInfo["FIT_ID"][$i]);

		if(($RechteStufe & 2)==2 AND $rsFilialInfo["FIT_IMQ_ID"][$i]==4)
		{

			if($rsFilialInfo["FIT_WERTE"][$i]!='')
				{
					echo "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">";

					echo '<select name=txtFeld_' . $rsFilialInfo["FIT_ID"][$i] . '>';
					echo "<option value=''>::Bitte w�hlen::</option>";

					if(substr($rsFilialInfo["FIT_WERTE"][$i],0,1)=='#')		// Werteliste
					{
						$WertListe = explode('#',substr($rsFilialInfo["FIT_WERTE"][$i],1));

						foreach($WertListe As $Eintrag)
						{
							if(strpos($Eintrag,';')===false)			// Keine Wert->Anzeige Kombination
							{
								echo '<option ';
								if(isset($rsFilialDaten["FIF_WERT"][0]) AND $rsFilialDaten["FIF_WERT"][0] == $Eintrag)
								{
									echo ' selected ';
								}
								echo ' value='. $Eintrag . '>' . $Eintrag . '</option>';
							}
							else										// Wert und Anzeige in zwei Feldern
							{
								$EintragListe = explode(';',$Eintrag);
								echo '<option ';
								if($rsFilialDaten["FIF_WERT"][0] == $EintragListe[0])
								{
									echo ' selected ';
								}

								echo ' value='. $EintragListe[0] . '>' . $EintragListe[1] . '</option>';
							}

						}
					}
					else 		// SQL Anweisung
					{
						$rsEintragsWert = awisOpenRecordset($con, $rsFilialInfo["FIT_WERTE"][$i]);
						for($EintragsWertZeile=0;$EintragsWertZeile<$awisRSZeilen;$EintragsWertZeile++)
						{
							if($awisRSSpalten==1)			// Keine Wert->Anzeige Kombination
							{
								echo '<option ';
								if($rsFilialDaten["FIF_WERT"][0] == $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile])
								{
									echo ' selected ';
								}
								echo ' value='. $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile] . '>' . $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile] . '</option>';
							}
							else										// Wert und Anzeige in zwei Feldern
							{
								echo '<option ';
								if($rsFilialDaten["FIF_WERT"][0] == $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile])
								{
									echo ' selected ';
								}
								echo ' value='. $rsEintragsWert[$awisRSInfo[1]['Name']][$EintragsWertZeile] . '>' . $rsEintragsWert[$awisRSInfo[2]['Name']][$EintragsWertZeile] . '</option>';
							}
						}
					}
					echo '</select>';

					echo '</td>';
				}
				else
				{
					print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") ."><input name=txtFeld_" . $rsFilialInfo["FIT_ID"][$i] . " type=text size=5 value=" . number_format((isset($rsFilialDaten["FIF_WERT"][0])?$rsFilialDaten["FIF_WERT"][0]:0),0) . ">";
				}
			print "</td>";
			$SpeichernButton=True;
		}
		else
		{
			print "<td " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . number_format($rsFilialDaten["FIF_WERT"][0],0) . "</td>";
		}
		print "</tr>";
	}
    print "</table>";

    echo '<hr>';
    
    
	$SQL = "select HBT_Bezeichnung, COUNT(HBT_Bezeichnung) AS Anzahl
			from HILFSUNDBETRIEBSSTOFFE INNER JOIN HILFSUNDBETRIEBSSTOFFTYPEN ON HILFSUNDBETRIEBSSTOFFE.HBS_HBT_KEY = HILFSUNDBETRIEBSSTOFFTYPEN.HBT_KEY
			INNER JOIN HILFSUNDBETRIEBSSTOFFEINSAETZE ON HILFSUNDBETRIEBSSTOFFE.HBS_KEY = HILFSUNDBETRIEBSSTOFFEINSAETZE.HBE_HBS_KEY
			WHERE HILFSUNDBETRIEBSSTOFFEINSAETZE.HBE_FIL_ID = " .$FILID. " AND HBE_DATUMBIS >= SYSDATE
			GROUP BY HBT_Bezeichnung
			";
	$rsFilialInfo = awisOpenRecordset($con, $SQL);
	$rsFilialInfoAnz = $awisRSZeilen;

	echo '<table border=1>';
	echo '<tr><td class=FeldBez>Ger�tetyp</td><td class=FeldBez>Anzahl</td></tr>';
    for($i=0;$i<$rsFilialInfoAnz;$i++)
    {
		echo '<tr>';    	
    	
		echo '<td ' . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>' . $rsFilialInfo['HBT_BEZEICHNUNG'][$i] . '</td>';
		echo '<td ' . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>' . $rsFilialInfo['ANZAHL'][$i] . '</td>';
		
    	echo '</tr>';
    }
    echo '</table>';
    
    
	print "</tr></table>";

}	// Auswahl oder Details

?>