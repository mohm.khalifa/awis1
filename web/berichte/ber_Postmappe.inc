<?php

require_once 'awisBerichte.inc';

class ber_Postmappe
    extends awisBerichte
{

    private $_Druckauftraege;
    private $_AlleMappen;

    public function init(array $Parameter)
    {
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');
        $TextKonserven[] = array('PMP', '*');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);

        //****************************************
        // Mappenkennung zu Mappennamen
        //****************************************
        $Daten = explode('|', $this->_AWISSprachkonserven['PMP']['PMP_MAPPE_LST']);

        $this->AlleMappen = array();

        foreach ($Daten as $row) {
            $Daten2 = explode('~', $row);
            $this->_AlleMappen[$Daten2[0]] = $Daten2[1];
        }

        $SQL = 'SELECT * FROM POSTMAPPE WHERE PMP_XBN_KEY =' . $this->_DB->WertSetzen('PMP','Z',$this->_AWISBenutzer->BenutzerID());

        $this->_rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('PMP', true));

        $this->_Druckauftraege = array();

        while (!$this->_rec->EOF()) {

            $SQL = 'SELECT FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, GBL_FEB_BEZEICHNUNG ';
            $SQL .= 'FROM V_FILIALEN_AKTUELL ';

            $Filialen = @unserialize($this->_rec->FeldInhalt('PMP_FIL_ID'));

            if ($Filialen[0] != 'Alle') {
                $SQL .= 'WHERE FIL_ID IN (' . implode(',', $Filialen) . ')';
            }

            $SQL .= ' GROUP BY FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, GBL_FEB_BEZEICHNUNG';
            $SQL .= ' ORDER BY FIL_ID';

            $this->_recFiliale = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FIL', true));

            while (!$this->_recFiliale->EOF()) {

                if ($this->_rec->FeldInhalt('PMP_MAP_KZ') == 'ALLE0') {
                    foreach ($this->_AlleMappen as $key => $Mappe) {
                        if ($key == 'ALLE0') {
                            continue;
                        }
                        $this->ErstelleDruckauftrag($Mappe);
                    }
                } else {

                    $this->ErstelleDruckauftrag($this->_AlleMappen[$this->_rec->FeldInhalt('PMP_MAP_KZ')]);
                }

                $this->_recFiliale->DSWeiter();
            }

            $this->_rec->DSWeiter();
        }
    }

    private function ErstelleDruckauftrag($Mappe)
    {
        $Auftrag = array();
        $Auftrag['FIL_ID'] = $this->_recFiliale->FeldInhalt('FIL_ID');
        $Auftrag['FIL_BEZ'] = $this->_recFiliale->FeldInhalt('FIL_BEZ');
        $Auftrag['FIL_STRASSE'] = $this->_recFiliale->FeldInhalt('FIL_STRASSE');
        $Auftrag['FIL_PLZ'] = $this->_recFiliale->FeldInhalt('FIL_PLZ');
        $Auftrag['FIL_ORT'] = $this->_recFiliale->FeldInhalt('FIL_ORT');
        $Auftrag['GBL_FEB_BEZEICHNUNG'] = $this->_recFiliale->FeldInhalt('GBL_FEB_BEZEICHNUNG');
        $Auftrag['PMP_LAG_KZ'] = $this->_rec->FeldInhalt('PMP_LAG_KZ');
        $Auftrag['MAPPE'] = $Mappe;

        $this->_Druckauftraege[] = $Auftrag;
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        $this->SetTitle('Postmappe');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);

        $DSNrAufSeite = 1;

        foreach ($this->_Druckauftraege as $Auftrag) {

            switch ($DSNrAufSeite) {
                case 1:
                    $this->NeueSeite();
                    $this->_Zeile = $this->_Parameter['SeiteOben'];
                    $this->_Spalte = $this->_Parameter['LinkerRand'];
                    break;
                case 2:
                    $this->_Zeile = $this->_Parameter['SeiteOben2'];
                    $this->_Spalte = $this->_Parameter['LinkerRand'];
                    break;
                case 3:
                    $this->_Zeile = $this->_Parameter['SeiteOben'];
                    $this->_Spalte = $this->_Parameter['LinkerRand2'];
                    break;
                case 4:
                    $this->_Zeile = $this->_Parameter['SeiteOben2'];
                    $this->_Spalte = $this->_Parameter['LinkerRand2'];
                    $DSNrAufSeite = 0;
                    break;
            }

            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseUeberschrift']);
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoeheUeberschrift'], str_pad($Auftrag['FIL_ID'], 3, 0, STR_PAD_LEFT), 0, 0, 'C');
            $this->Cell($this->_Parameter['AbstandLager'], $this->_Parameter['ZeilenHoeheUeberschrift'], $Auftrag['PMP_LAG_KZ'], 0, 0, 'R');

            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesse']);

            $this->_Zeile += $this->_Parameter['ZeilenAbstand'];

            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $Auftrag['FIL_BEZ'], 0, 0, 'C');

            if (substr($Auftrag["GBL_FEB_BEZEICHNUNG"],0,2) == "BZ"){
                $this->SetXY($this->_Spalte, $this->_Zeile + $this->_Parameter["ZeilenAbstand2"]);
                $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], "Bezirk ". substr($Auftrag['GBL_FEB_BEZEICHNUNG'],2) , 0, 0, 'C');
            }

            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesse']);

            $this->_Zeile += $this->_Parameter['ZeilenAbstand'];

            //Bild einfügen, falls DIANA
            if (strpos($Auftrag['MAPPE'], 'DIANA') !== false) {
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Image($this->_AWISBenutzer->ParameterLesen('SystemPfadWeb') . '/bilder/diana_logo.PNG');
            } else {
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $Auftrag['MAPPE'], 0, 0, 'C');
            }

            $DSNrAufSeite += 1;
        }

        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************

        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    private function NeueSeite()
    {
        $this->AddPage('L');
    }
}