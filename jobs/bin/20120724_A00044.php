<link rel=stylesheet type=text/css href=/css/awis.css>
<?php

require_once('awisDatenbank.inc');

/*
 * Anforderung A00044 - Anlage Lieferantenartikel und Verkn�pfung zu Artikelstamm
*/

function Protokoll($fp, $ProtArray)
{
	$zeile  = $ProtArray['LAR_LARTNR'] .';';
	$zeile .= $ProtArray['AST_ATUNR'] .';';
	$zeile .= $ProtArray['KATEGORIE'] .';';
	$zeile .= $ProtArray['AKTION'] .';';
	$zeile .= $ProtArray['SQL'] .";\n";
	fputs($fp,$zeile);
	
	return 0;
}

try
{
	$Anforderung = 'A00044';
	$Anforderer = 'EDV';
	$QuellTabelle = 'XXX_'.$Anforderung;
	$Lieferant = '8684';
	$Bemerkung = '';

	//$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20120724 Staging A00044-PROTOKOLL.csv';
	$ProtokollDatei = '/daten/daten/pccommon/Ott_H/20120724 Produktiv A00044-PROTOKOLL.csv';
	$fp = fopen($ProtokollDatei,'w+');
	$zeile = "LAR_LARTNR;AST_ATUNR;KATEGORIE;AKTION;SQL;\n";
	fputs($fp,$zeile);
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	// alle zu ersetzenden LARTNR aus temp. Tabelle laden
	$SQL = 'SELECT * FROM '.$QuellTabelle;
	$SQL .= ' WHERE ERFOLGREICH IS NULL';
	$SQL .= ' OR ERFOLGREICH = 0';
	
	$rsQuelle = $DB->RecordSetOeffnen($SQL);
	
	$DB->TransaktionBegin();
	
	while (!$rsQuelle->EOF())
	{
		$ProtokollArray = array();
		$ProtokollArray['LAR_LARTNR'] = $rsQuelle->Feldinhalt('LAR_LARTNR');
		$ProtokollArray['AST_ATUNR'] = $rsQuelle->Feldinhalt('AST_ATUNR');
		$ProtokollArray['AKTION'] = '';
		$ProtokollArray['SQL'] = '';
		
		//Gibt es Eintr�ge im Lieferantenartikel f�r LAR_LARTNR_ALT und Lieferant xy
		$SQL = 'SELECT * FROM LIEFERANTENARTIKEL';
		$SQL .= ' WHERE LAR_SUCH_LARTNR ' .$DB->LIKEoderIST($rsQuelle->Feldinhalt('LAR_LARTNR'), awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
		$SQL .= ' AND LAR_LIE_NR = ' .$DB->FeldInhaltFormat('T',$Lieferant, false);

		$ProtokollArray['AKTION'] = 'Pr�fung ob Lieferantenartikel bereits vorhanden';
		$ProtokollArray['SQL'] = $SQL; 		
		$ProtokollArray['KATEGORIE'] = 'INFO'; 		
		Protokoll($fp, $ProtokollArray);
		
		$rsLAR = $DB->RecordSetOeffnen($SQL);
		
		if ($rsLAR->EOF())
		{
			$SQLINS = 'INSERT INTO LIEFERANTENARTIKEL (';
			$SQLINS .= 'LAR_LARTNR,';
			$SQLINS .= 'LAR_LIE_NR,';
			$SQLINS .= 'LAR_BEMERKUNGEN,';
			$SQLINS .= 'LAR_BEKANNTWW,';
			$SQLINS .= 'LAR_IMQ_ID,';
			$SQLINS .= 'LAR_SUCH_LARTNR,';
			$SQLINS .= 'LAR_USER,';
			$SQLINS .= 'LAR_USERDAT';
			$SQLINS .= ') VALUES (';
			$SQLINS .= $DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('LAR_LARTNR'),false) .',';
			$SQLINS .= $DB->FeldInhaltFormat('T',$Lieferant,false) .',';
			$SQLINS .= $DB->FeldInhaltFormat('T',$Bemerkung,false) .',';
			$SQLINS .= '0,';
			$SQLINS .= '4,';
			$SQLINS .= 'asciiwort(' .$DB->FeldInhaltFormat('TU',$rsQuelle->Feldinhalt('LAR_LARTNR'),false) .'),';
			$SQLINS .= $DB->FeldInhaltFormat('T',$Anforderer,false) .',';
			$SQLINS .= 'SYSDATE';
			$SQLINS .= ')';
			
			$ProtokollArray['AKTION'] = 'Insert Lieferantenartikel';
			$ProtokollArray['SQL'] = $SQLINS; 		
			$ProtokollArray['KATEGORIE'] = 'INSERT'; 		
			Protokoll($fp, $ProtokollArray);
			
			$DB->Ausfuehren($SQLINS,'',true);

			$ProtokollArray['AKTION'] = 'Laden Lieferantenartikel nach Neuanlage';
			$ProtokollArray['SQL'] = $SQL; 		
			$ProtokollArray['KATEGORIE'] = 'INFO'; 		
			Protokoll($fp, $ProtokollArray);
				
			//nach Insert jetzt nochmal den LiefDS f�r weitere Bearbeitung laden
			$rsLAR = $DB->RecordSetOeffnen($SQL);
			
			if (!$rsLAR->EOF())
			{
				//UNDO Datensatz im Protokoll
				$SQL  = 'DELETE FROM LIEFERANTENARTIKEL';
				$SQL .= ' WHERE LAR_KEY = ' .$DB->FeldInhaltFormat('Z',$rsLAR->Feldinhalt('LAR_KEY'));
				$ProtokollArray['AKTION'] = 'Undo Insert Lieferantenartikel';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
				Protokoll($fp, $ProtokollArray);
				
				//Artikelstammsatz laden
				$SQL = 'SELECT * FROM ARTIKELSTAMM';
				$SQL .= ' WHERE AST_ATUNR=' .$DB->FeldInhaltFormat('T',$rsQuelle->Feldinhalt('AST_ATUNR'));

				$ProtokollArray['AKTION'] = 'Laden Artikelstammdaten';
				$ProtokollArray['SQL'] = $SQL; 		
				$ProtokollArray['KATEGORIE'] = 'INFO'; 		
				Protokoll($fp, $ProtokollArray);
				
				$rsAST = $DB->RecordSetOeffnen($SQL);
				
				if (!$rsAST->EOF())
				{
					//Gibt es Eintr�ge in Teileinfos f�r AST/LAR LAR_LARTNR
					$SQL = 'SELECT * FROM TEILEINFOS';
					$SQL .= ' WHERE TEI_ITY_ID1 = \'AST\' AND';
					$SQL .= ' TEI_KEY1 = ' .$DB->FeldInhaltFormat('Z',$rsAST->Feldinhalt('AST_KEY')) .' AND';
					$SQL .= ' TEI_ITY_ID2 = \'LAR\' AND';
					$SQL .= ' TEI_KEY2 = ' .$DB->FeldInhaltFormat('Z',$rsLAR->Feldinhalt('LAR_KEY'));
				
					$ProtokollArray['AKTION'] = 'Laden Teileinfos Kombination AST/LAR';
					$ProtokollArray['SQL'] = $SQL; 		
					$ProtokollArray['KATEGORIE'] = 'INFO'; 		
					Protokoll($fp, $ProtokollArray);
					
					$rsTEI = $DB->RecordSetOeffnen($SQL);
					
					if ($rsTEI->EOF())
					{
						$SQL = 'INSERT INTO TEILEINFOS (';
						$SQL .= 'TEI_ITY_ID1,';
						$SQL .= 'TEI_KEY1,';
						$SQL .= 'TEI_SUCH1,';
						$SQL .= 'TEI_WERT1,';
						$SQL .= 'TEI_ITY_ID2,';
						$SQL .= 'TEI_KEY2,';
						$SQL .= 'TEI_SUCH2,';
						$SQL .= 'TEI_WERT2,';
						$SQL .= 'TEI_USER,';
						$SQL .= 'TEI_USERDAT';
						$SQL .= ') VALUES (';
						// AST Teil = alte Werte �bernehmen
						$SQL .= "'AST',";
						$SQL .= $DB->FeldInhaltFormat('Z',$rsAST->Feldinhalt('AST_KEY')).',';
						$SQL .= 'suchwort(\''.$rsAST->FeldInhalt('AST_ATUNR').'\'),';
						$SQL .= $DB->FeldInhaltFormat('TU',$rsAST->FeldInhalt('AST_ATUNR'),false) .',';
						// LAR Teil = neue Werte bef�llen
						$SQL .= "'LAR',";
						$SQL .= $DB->FeldInhaltFormat('Z',$rsLAR->Feldinhalt('LAR_KEY')).',';
						$SQL .= $DB->FeldInhaltFormat('T',$rsLAR->Feldinhalt('LAR_SUCH_LARTNR')).',';
						$SQL .= $DB->FeldInhaltFormat('T',$rsLAR->Feldinhalt('LAR_LARTNR')).',';
						$SQL .= $DB->FeldInhaltFormat('T',$Anforderer,false) .',';
						$SQL .= 'SYSDATE';
						$SQL .= ')';

						$ProtokollArray['AKTION'] = 'Insert Teileinfos Kombination AST/LAR';
						$ProtokollArray['SQL'] = $SQL; 		
						$ProtokollArray['KATEGORIE'] = 'INSERT'; 		
						Protokoll($fp, $ProtokollArray);
						
						$DB->Ausfuehren($SQL,'',true);
						
						$SQL = "SELECT seq_TEI_KEY.CurrVal AS KEY FROM DUAL";
						$rsSeqTEIKey = $DB->RecordSetOeffnen ($SQL);

						$SQL  = 'DELETE FROM TEILEINFOS';
						$SQL .= ' WHERE TEI_KEY = ' .$DB->FeldInhaltFormat('Z',$rsSeqTEIKey->FeldInhalt('KEY'));
						$ProtokollArray['AKTION'] = 'Undo Insert Teileinfos Kombination AST/LAR';
						$ProtokollArray['SQL'] = $SQL; 		
						$ProtokollArray['KATEGORIE'] = 'UNDO'; 		
						Protokoll($fp, $ProtokollArray);
						
						$SQL = 'UPDATE ' .$QuellTabelle;
						$SQL .= ' SET erfolgreich = 1 ';
						$SQL .= ' WHERE LAR_LARTNR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LARTNR'),false);
						$DB->Ausfuehren($SQL,'',true);

						$ProtokollArray['AKTION'] = 'Vorgang erfolgreich abgeschlossen';
						$ProtokollArray['SQL'] = $SQL; 		
						$ProtokollArray['KATEGORIE'] = 'OK'; 		
						Protokoll($fp, $ProtokollArray);
					}
					else
					{
						//nichts machen
						$ProtokollArray['AKTION'] = 'Gefunden Teileinfos Kombination AST/LAR bereits vorhanden, nichts machen';
						$ProtokollArray['SQL'] = '';
						$ProtokollArray['KATEGORIE'] = 'WARNING'; 		
						Protokoll($fp, $ProtokollArray);
					}
				}
				else
				{
					$ProtokollArray['AKTION'] = 'Fehler, Artikelstammsatz nicht gefunden';
					$ProtokollArray['SQL'] = $SQL; 
					$ProtokollArray['KATEGORIE'] = 'ERROR'; 		
					Protokoll($fp, $ProtokollArray);
		
					$SQL = 'UPDATE ' .$QuellTabelle;
					$SQL .= ' SET erfolgreich = 0 ';
					$SQL .= ' WHERE LAR_LARTNR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LARTNR'),false);
					$DB->Ausfuehren($SQL,'',true);
				}
			}
			else
			{
				$ProtokollArray['AKTION'] = 'Fehler, Lieferantenartikel wurde nicht angelegt';
				$ProtokollArray['SQL'] = $SQL; 
				$ProtokollArray['KATEGORIE'] = 'ERROR'; 		
				Protokoll($fp, $ProtokollArray);
	
				$SQL = 'UPDATE ' .$QuellTabelle;
				$SQL .= ' SET erfolgreich = 0 ';
				$SQL .= ' WHERE LAR_LARTNR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LARTNR'),false);
				$DB->Ausfuehren($SQL,'',true);
			}
		}
		else
		{
			$ProtokollArray['AKTION'] = 'Fehler, Lieferantenartikel bereits vorhanden';
			$ProtokollArray['SQL'] = $SQL; 
			$ProtokollArray['KATEGORIE'] = 'ERROR'; 		
			Protokoll($fp, $ProtokollArray);

			$SQL = 'UPDATE ' .$QuellTabelle;
			$SQL .= ' SET erfolgreich = 0 ';
			$SQL .= ' WHERE LAR_LARTNR = ' .$DB->FeldInhaltFormat('T',$rsQuelle->FeldInhalt('LAR_LARTNR'),false);
			$DB->Ausfuehren($SQL,'',true);
		}
		
		$rsQuelle->DSWeiter();
	}

	//$DB->TransaktionRollback();
	$DB->TransaktionCommit();
	$DB->Schliessen();
	fclose($fp);
}

catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	$DB->TransaktionRollback();
	$DB->Schliessen();
	fclose($fp);
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$DB->TransaktionRollback();
	$DB->Schliessen();
	fclose($fp);
}

?>
