<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
    
	$AWISBenutzer = awisBenutzer::Init();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	
	if ($AWIS_KEY1 == '-1') //Neuanlage
	{
	    $SQL ='insert into SCHADENSGRUND ';
	    $SQL .=' (ID,GRUND,VERALTET,SICHTBAR_FILIALEN,GRUND_USER,GRUND_USERDAT)';
	    $SQL .=' values';
	    $SQL .='(seq_beschaedigt_durch_id.nextval';
	    $SQL .= ','.$DB->FeldInhaltFormat('T',$_POST['txtBES_GRUND']);
	  
	    if(isset($_POST['txtCHK_AKTIV']))
	    {
	        $SQL .= ',0';
	    }
	    else
	    {
	        $SQL .= ',1';
	    }
	    if(isset($_POST['txtCHK_FILIALEN']))
	    {
	    	$SQL .= ',1';
	    }
	    else
	    {
	    	$SQL .= ',0';
	    }
	    $SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
	    $SQL .=',sysdate)';
	    	
	    $DBSCHAD->Ausfuehren($SQL,'',true);
	    $SpeichernOK = true;
	    $Form->Hinweistext($AWISSprachKonserven['BES']['MSG_NEUANLAGE']);
	}
	elseif($AWIS_KEY1 > '0')
	{		
			$SQL ='update SCHADENSGRUND ';
			//$SQL .= 'set KENNUNG='.$DB->FeldInhaltFormat('T',$_POST['txtBES_KENNUNG']);
			$SQL .= ' set GRUND='.$DB->FeldInhaltFormat('T',$_POST['txtBES_GRUND']);
				
			if(isset($_POST['txtCHK_AKTIV']))
			{
				$SQL .= ', VERALTET=0';
			}
			else
			{
				$SQL .= ', VERALTET=1';
			}
			if(isset($_POST['txtCHK_FILIALEN']))
			{
				$SQL .= ', SICHTBAR_FILIALEN=1';
			}
			else
			{
				$SQL .= ', SICHTBAR_FILIALEN=0';
			}
			$SQL .= ', GRUND_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', GRUND_USERDAT=SYSDATE';
			$SQL .=' where ID='.$AWIS_KEY1;
			$DBSCHAD->Ausfuehren($SQL,'',true);		
			$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_SPEICHERN']);
			$SpeichernOK = true;
	}
}
catch (awisException $ex)
{
    /*
    if(strpos($ex->getMessage(),'ORA-00001: Unique Constraint') == 0) // F5 Problem als Hinweistext ausgeben.
    {
        $Fehler = 1;
        $Hinweis = 'Die Kennung befindet sich schon in der Datenbank.';
       
        $Form->Hinweistext($Hinweis);
    }
    else*/
    //{
        $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
        $Form->DebugAusgabe(1,$ex->getSQL());
   // }


	
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
