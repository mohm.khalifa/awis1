<?php
require_once('awisFormular.inc');
require_once('awisFilialen.inc');

function FilialInfos($Bereich, awisFilialen $Filiale, $ReadOnly=0)
{
	global $AWISCursorPosition;

	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	// Dynamische Infos
	$SQL = 'SELECT * FROM FilialInfosTypen2 ';
	$SQL .= ' WHERE FIT_BEREICH LIKE \'%'.$Bereich.'%\'';
	$SQL .= ' AND FIT_STATUS = \'A\' ';
	$SQL .= ' ORDER BY FIT_BEREICH, FIT_GRUPPE, FIT_SORTIERUNG';
//$Form->DebugAusgabe(1, $SQL);
	$rsFIT = $DB->RecordSetOeffnen($SQL);
	$LetzteGruppe = '';
	while(!$rsFIT->EOF())
	{
		// Wert ermitteln
		$InfoWert = $Filiale->FilialInfo($rsFIT->FeldInhalt('FIT_ID'),$rsFIT->FeldInhalt('FIT_IMQ_ID'),awisFilialen::FILIALINFO_FELD_WERT);
		$InfoKey = $Filiale->FilialInfo($rsFIT->FeldInhalt('FIT_ID'),$rsFIT->FeldInhalt('FIT_IMQ_ID'),awisFilialen::FILIALINFO_FELD_KEY);
		$InfoUserDat = $Filiale->FilialInfo($rsFIT->FeldInhalt('FIT_ID'),$rsFIT->FeldInhalt('FIT_IMQ_ID'),awisFilialen::FILIALINFO_FELD_USERDAT);

		$Recht = $AWISBenutzer->HatDasRecht($rsFIT->FeldInhalt('FIT_XRC_ID'));
		if(($Recht&$rsFIT->FeldInhalt('FIT_STUFEANZEIGE'))!=0)
		{
			$EditModus = $ReadOnly?0:($rsFIT->FeldInhalt('FIT_STUFEBEARBEITEN')==0?false:(($Recht&($rsFIT->FeldInhalt('FIT_STUFEBEARBEITEN')))&$rsFIT->FeldInhalt('FIT_STUFEBEARBEITEN')));
			if($LetzteGruppe!=$rsFIT->FeldInhalt('FIT_GRUPPE'))
			{
				if($LetzteGruppe!='')
				{
					$Form->Trennzeile('L');
				}
				$LetzteGruppe=$rsFIT->FeldInhalt('FIT_GRUPPE');

			}

			// Eine �berschrift anzeigen
			if($rsFIT->FeldInhalt('FIT_UEBERSCHRIFT')==1)
			{
				$Info = $Form->LadeTextBaustein('Filialinfo','fit_ueb_'.$rsFIT->FeldInhalt('FIT_ID'));
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Info,0,'font-weight:bold;font-size:larger;');
				$Form->ZeileEnde();
			}

			// Soll eine neue Zeile angezeigt werden?
			if($rsFIT->FeldInhalt('FIT_UMBRUCH')==='' OR ($rsFIT->FeldInhalt('FIT_UMBRUCH')&1))
			{
				$Form->ZeileStart();
			}

			// Bezeichnung vor das Feld schreiben?
			if($rsFIT->FeldInhalt('FIT_LABEL')==1)
			{
				$Info = $Form->LadeTextBaustein('Filialinfo','fit_lbl_'.$rsFIT->FeldInhalt('FIT_ID'));
				$InfoTTT = ($rsFIT->FeldInhalt('FIT_TOOLTIPP')==0?'':$Form->LadeTextBaustein('Filialinfo','fit_ttt_'.$rsFIT->FeldInhalt('FIT_ID')));
				$InfoStyle = $rsFIT->FeldInhalt('FIT_LABELSTYLE'); 
				$Form->Erstelle_TextLabel($Info.($rsFIT->FeldInhalt('FIT_LABELDOPPELPUNKT')==1?':':''),$rsFIT->FeldInhalt('FIT_LABELBREITE'),$InfoStyle,'',$InfoTTT);
			}

			// Eingabefeld erzeugen
			if($rsFIT->FeldInhalt('FIT_DATENQUELLE')!='')
			{
				switch(substr($rsFIT->FeldInhalt('FIT_DATENQUELLE'),0,3))
				{
					case 'TXT':
						$Felder = explode(':',$rsFIT->FeldInhalt('FIT_DATENQUELLE'));
						$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])));
						$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
						$Form->Erstelle_SelectFeld('FIT_WERT_'.$rsFIT->FeldInhalt('FIT_ID').'_'.$InfoKey.'_'.$rsFIT->FeldInhalt('FIT_FORMAT'),$InfoWert,$rsFIT->FeldInhalt('FIT_BREITE'),$EditModus,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
						break;
					case 'FLD':		// Feld aus der FilialInfo
						$Felder = explode(':',$rsFIT->FeldInhalt('FIT_DATENQUELLE'));
						$InfoWert = $Filiale->FilialInfo($Felder[1]);
						$Form->Erstelle_TextFeld('FLD_'.$Felder[1].'_'.$rsFIT->FeldInhalt('FIT_FORMAT'),$InfoWert,$rsFIT->FeldInhalt('FIT_ZEICHEN'),$rsFIT->FeldInhalt('FIT_BREITE'),$EditModus,'','','',$rsFIT->FeldInhalt('FIT_FORMAT'));
						break;
					case 'SQL':
						$Felder = explode(':',$rsFIT->FeldInhalt('FIT_DATENQUELLE'));
						$Felder[1] = $Filiale->ErsetzeFilalInfos($Felder[1]);
						$Form->Erstelle_SelectFeld('FIT_WERT_'.$rsFIT->FeldInhalt('FIT_ID').'_'.$InfoKey.'_'.$rsFIT->FeldInhalt('FIT_FORMAT'),$InfoWert,$rsFIT->FeldInhalt('FIT_BREITE'),$EditModus,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
						break;
					case 'FKT':
						$Funktion = explode(':',$rsFIT->FeldInhalt('FIT_DATENQUELLE'));
						$Funktion = explode('#~#',$Funktion[1]);					
						$Erg = $Funktion[0]($Filiale, (isset($Funktion[1])?$Funktion[1]:''),(isset($Funktion[2])?$Funktion[2]:''),(isset($Funktion[3])?$Funktion[3]:''));

						$Form->SchreibeHTMLCode($Erg);

						break;
					case 'LNK':
							// Erzeugt einen Link mit einem Bild oder einem Text
						$Felder = explode(':',$rsFIT->FeldInhalt('FIT_DATENQUELLE'));

						$Link = $Filiale->ErsetzeFilalInfos($Felder[2]);
						$Felder[1] = $Filiale->ErsetzeFilalInfos($Felder[1]);

						if(substr($Felder[1],0,4)=='ICO~')
						{
							$Bild = explode('~',$Felder[1]);
							$Form->Erstelle_Bild('href','*LINK',$Link,$Bild[1]);
						}
						elseif(substr($Felder[1],0,4)=='TXT~')
						{
							$Text = explode('~',$Felder[1]);
							$Form->Erstelle_TextFeld('*LINK',$Text[1],0,$rsFIT->FeldInhalt('FIT_BREITE'),'','','',$Link,'T');
						}
						else
						{
							$Text = explode('~',$Felder[1]);
							$Text = $Form->LadeTextBaustein($Text[0],$Text[1]);
							$Form->Erstelle_TextFeld('*LINK',$Text,0,$rsFIT->FeldInhalt('FIT_BREITE'),'','','',$Link,'T');
						}
						break;
                    case 'WYS':
                        $TinyMCE = $Form->TinyMCE('FIT_WERT_'.$rsFIT->FeldInhalt('FIT_ID').'_'.$InfoKey.'_'.$rsFIT->FeldInhalt('FIT_FORMAT'), $InfoWert, 1000, 15, $EditModus,true, true,true);
                        $TinyMCE->Menubar()->MitStandardfunktionen();
                        $TinyMCE->Menubar()->MitTools();
                        break;
					default:
						$Form->Erstelle_TextFeld('FIT_WERT_'.$rsFIT->FeldInhalt('FIT_ID').'_'.$InfoKey.'_'.$rsFIT->FeldInhalt('FIT_FORMAT'),$InfoWert,$rsFIT->FeldInhalt('FIT_ZEICHEN'),$rsFIT->FeldInhalt('FIT_BREITE'),$EditModus,'','','',$rsFIT->FeldInhalt('FIT_FORMAT'),'','',$rsFIT->FeldInhalt('FIT_DATENQUELLE'));
						break;
				}
			}
			else
			{
				if(strpos($rsFIT->FeldInhalt('FIT_ZEICHEN'),',')===false)
				{
					$Form->Erstelle_TextFeld('FIT_WERT_'.$rsFIT->FeldInhalt('FIT_ID').'_'.$InfoKey.'_'.$rsFIT->FeldInhalt('FIT_FORMAT'),$InfoWert,$rsFIT->FeldInhalt('FIT_ZEICHEN'),$rsFIT->FeldInhalt('FIT_BREITE'),$EditModus,'','','',$rsFIT->FeldInhalt('FIT_FORMAT'));
				}
				else
				{
					$Breiten = explode(',',$rsFIT->FeldInhalt('FIT_ZEICHEN'));
					$Spalten = $Breiten[0];
					$Zeilen = $Breiten[1];

					$Form->Erstelle_Textarea('FIT_WERT_'.$rsFIT->FeldInhalt('FIT_ID').'_'.$InfoKey.'_'.$rsFIT->FeldInhalt('FIT_FORMAT'),$InfoWert,$rsFIT->FeldInhalt('FIT_BREITE'),$Spalten,$Zeilen,$EditModus,'','','',$rsFIT->FeldInhalt('FIT_FORMAT'));
				}
			}

			if($rsFIT->FeldInhalt('FIT_UMBRUCH')=='' OR ((int)$rsFIT->FeldInhalt('FIT_UMBRUCH')&2)==2)
			{
				$Form->ZeileEnde();
			}

			if($AWISCursorPosition=='' AND $EditModus)
			{
				$AWISCursorPosition = 'txtFIT_WERT_'.$rsFIT->FeldInhalt('FIT_ID').'_'.$InfoKey.'_'.$rsFIT->FeldInhalt('FIT_FORMAT');
			}
		}
        $rsFIT->DSWeiter();

	}
}

/**
 * Liefert Links mit den aktuellen Gebietszuordnungen.
 * Diese Funktion wird automatisch �ber die Filialinfos2 aufgerufen - daher 4 Parameter
 *
 * @param awisFilialen $FilObj
 * @param mixed $Param1
 * @param mixed $Param2
 * @param mixed $Param3
 * @return string
 */
function Gebietszuordnungen($FilObj, $Param1, $Param2, $Param3)
{
	$Zuordnungen = $FilObj->Zuordnungen();
	$Text = '';
	foreach($Zuordnungen AS $Zuordnung)
	{
		$Text .= ' - <a href="./filialinfos_Main.php?cmdAktion=Details&FEB_KEY='.$Zuordnung['FEB_KEY'].'" title="'.$Zuordnung['FEB_BEMERKUNG'].'">'.$Zuordnung['FEB_BEZEICHNUNG'].'</a>';
	}
	$Text = substr($Text,2);

	$Text = '<div class=Eingabefeld>'.$Text.'</div>';

	return $Text;
}


/**
 * Zeigt eine Liste mit Feiertage
 *
 * @param awisFilialen $FilObj
 * @param string $Param1
 * @param string $Param2
 * @param string $Param3
 * @return array
 */
function Feiertage($FilObj, $Param1, $Param2, $Param3)
{
	$DatumVom = date('d.m.Y',mktime(0,0,0,date('m'),date('d')-1,date('Y')));
	$DatumBis = date('d.m.Y',mktime(0,0,0,date('m'),date('d')+30,date('Y')));

	$Feiertage = $FilObj->Feiertage($DatumVom, $DatumBis);
	$Text = '';

	foreach($Feiertage AS $Feiertag)
	{
		$Text .= '<br>'.$Feiertag['Datum'].' - '.$Feiertag['Bezeichnung'];
	}

	$Text = '<div class=Eingabefeld>'.substr($Text,4).'</div>';

	return $Text;
}

/**
 * Zeigt eine Liste mit Lacktagen
 *
 * @param awisFilialen $FilObj
 * @param string $Param1
 * @param string $Param2
 * @param string $Param3
 * @return array
 */
function LacktageEingabe($FilObj, $Param1, $Param2, $Param3)
{
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
     
    $TextKonserven[]=array('Wort','Wochentag_*_Kurz');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    
    
    $SQL = 'SELECT FIF_WERT FROM Filialinfos ';
    $SQL .= ' WHERE FIF_FIT_ID = '.$DB->WertSetzen('FIF','N0',851).' AND FIF_FIL_ID = '.$DB->WertSetzen('FIF', 'N0',$FilObj->FilialID(),true);
    $rsFIF = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('FIF'));

    // Lacktage einzeln auff�hren
    $Recht5019 = $AWISBenutzer->HatDasRecht(5019);
    $EditModus = ($Recht5019&2); 
    $Form->Erstelle_Checkbox('Lacktage_1',(($rsFIF->FeldInhalt('FIF_WERT')&1)==1?1:0), 20, $EditModus, '1');
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Wochentag_Montag_Kurz'],30);
    $Form->Erstelle_Checkbox('Lacktage_2',(($rsFIF->FeldInhalt('FIF_WERT')&2)==2?1:0), 20, $EditModus, '1');
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Wochentag_Dienstag_Kurz'],30);
    $Form->Erstelle_Checkbox('Lacktage_4',(($rsFIF->FeldInhalt('FIF_WERT')&4)==4?1:0), 20, $EditModus, '1');
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Wochentag_Mittwoch_Kurz'],30);
    $Form->Erstelle_Checkbox('Lacktage_8',(($rsFIF->FeldInhalt('FIF_WERT')&8)==8?1:0), 20, $EditModus, '1');
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Wochentag_Donnerstag_Kurz'],30);
    $Form->Erstelle_Checkbox('Lacktage_16',(($rsFIF->FeldInhalt('FIF_WERT')&16)==16?1:0), 20, $EditModus, '1');
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Wochentag_Freitag_Kurz'],30);
    
    
	return '';
}

/**
 * Anzeige der n�chsten Lacktage in der Filiale
 * @param awisFilialen $FilObj
 * @param int $Param1           Anzahl der Tage, die angezeigt werden sollen
 * @param unknown $Param2
 * @param unknown $Param3
 * @return string
 */
function NaechsteLacktage(awisFilialen $FilObj, $Param1, $Param2, $Param3)
{
    $AnzeigeTage = '';
    $Tage = $FilObj->NaechsteLacktage($FilObj->FilialID(),$Param1);
    
    //PG: 20151104
    //NaechsteLacktage liefert false, wenn kein Tag ermittelt werden konnte-->Keine Auswertung notwendig
    if ($Tage!=false)
    {
        foreach($Tage as $Tag)
        {
            $AnzeigeTage .= ','.date('d.m.Y',$Tag);
        }
        
    }
    
    return substr($AnzeigeTage,1);
}


/**
 * @param awisFiliale $FilObj
 * @param mixed $Param1
 * @param mixed $Param2
 * @param mixed $Param3
 * @return string
*/
function OeffnungsZeiten($FilObj, $Param1, $Param2, $Param3)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$TextKonserven[]=array('FOH','wrd_HinweisOeffnungszeit');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$SQL = 'SELECT *';
	$SQL .= ' FROM FILIALENOEFFNUNGSHINWEISE';
	$SQL .= ' WHERE FOH_FIL_ID = :var_N0_FOH_FIL_ID';
	$SQL .= ' AND trunc(FOH_DATUM) >= trunc(SYSDATE) AND trunc(FOH_DATUM) <= trunc(SYSDATE+30)';
	$SQL .= ' ORDER BY FOH_DATUM';
	
	$DB->SetzeBindevariable('FOH', 'var_N0_FOH_FIL_ID', $FilObj->FilialID(), awisDatenbank::VAR_TYP_GANZEZAHL);
	$rsFOH = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FOH'));

	if(!$rsFOH->EOF())
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FOH']['wrd_HinweisOeffnungszeit'].':',$Param1);

		$DS = 0;
		while(!$rsFOH->EOF())
		{
			if($DS>0)
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('&nbsp;',$Param1);
			}
			$Form->Erstelle_TextFeld('#FOH_DATUM',$Form->Format('D',$rsFOH->FeldInhalt('FOH_DATUM')).' - '.$rsFOH->FeldInhalt('FOH_HINWEIS'),10,500,false);

			$rsFOH->DSWeiter();

			$Form->ZeileEnde();
			$DS++;
		}
	}
}

/**
 * Liste mit Pr�ofganisationen
 *
 * @param awisFiliale $FilObj
 * @param mixed $Param1
 * @param mixed $Param2
 * @param mixed $Param3
 * @return string
 */
function PruefOrganisationenListe($FilObj, $Param1, $Param2, $Param3)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	if($Param1==1) //HU
	{
		$SQL = "SELECT MIN(PRF_Preis) AS Preis_MIN, MAX(PRF_Preis) AS Preis_MAX FROM PruefFilialen";
		$SQL .= " WHERE PRF_FIL_ID = 0" . $FilObj->FilialID();
		$SQL .= " AND PRF_PRA_ID = 1";
		$rsPreise = $DB->RecordSetOeffnen($SQL);
		$Ausgabe = $rsPreise->FeldInhalt("PREIS_MIN");
		if($Ausgabe <> $rsPreise->FeldInhalt("PREIS_MAX") and $rsPreise->FeldInhalt("PREIS_MAX")!=0)
		{
			$Ausgabe = $Form->Format('N2',$rsPreise->FeldInhalt("PREIS_MIN")).' - '.$Form->Format('N2',$rsPreise->FeldInhalt("PREIS_MAX"));
		}
		else
		{
			$Ausgabe = $Form->Format('N2',$rsPreise->FeldInhalt("PREIS_MIN"));
		}

		return $Ausgabe;
	}
	else
	{
		$TextKonserven[]=array('PRG','%');
		$TextKonserven[]=array('PRB','%');
		$TextKonserven[]=array('PRF','%');
		$TextKonserven[]=array('PRA','%');
		
		$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

		$SQL = "SELECT * FROM PruefGesellschaften, PruefArten, PruefFilialen, PruefBemerkungen";
		$SQL .= " WHERE PRF_PRG_ID = PRG_ID AND PRF_PRA_ID = PRA_ID";
		$SQL .= " AND (PRF_FIL_ID=PRB_FIL_ID(+) AND PRF_PRG_ID=PRB_PRG_ID(+))";
		$SQL .= " AND PRF_FIL_ID = 0" . $FilObj->FilialID();
		$SQL .= " AND PRA_ID IN (1,11)";
		$SQL .= " ORDER BY PRG_ID, PRA_ID";
		
		$rsTUEVInfos = $DB->RecordSetOeffnen($SQL);
		$Ausgabe = '<div class=EingabeZeile>';
		$Ausgabe .= '<div class=EingabeUeberschrift style="width:60">'.$AWISSprachKonserven['PRG']['PRG_ID'].'</div>';
		$Ausgabe .= '<div class=EingabeUeberschrift style="width:300">'.$AWISSprachKonserven['PRG']['PRG_BEZEICHNUNG'].'</div>';
		$Ausgabe .= '<div class=EingabeUeberschrift style="width:250">'.$AWISSprachKonserven['PRA']['PRA_BEZEICHNUNG'].'</div>';
		$Ausgabe .= '<div class=EingabeUeberschrift style="width:100">'.$AWISSprachKonserven['PRF']['PRF_PREIS'].'</div>';
		$Ausgabe .= '<div class=EingabeUeberschrift style="width:400">'.$AWISSprachKonserven['PRB']['PRB_BEMERKUNG'].'</div>';
		$Ausgabe .= '</div>';

		$DS=0;
		while(!$rsTUEVInfos->EOF())
		{
			$Ausgabe .= '<div class=EingabeZeile>';

			$Ausgabe .= '<div class='.($DS%2==0?'ListenFeldDunkel':'ListenFeldHell').' style="width:60">'.$Form->Format('N0',$rsTUEVInfos->FeldInhalt('PRG_ID')).'</div>';
			$Ausgabe .= '<div class='.($DS%2==0?'ListenFeldDunkel':'ListenFeldHell').' style="width:300">'.$Form->Format('T',$rsTUEVInfos->FeldInhalt('PRG_BEZEICHNUNG')).'</div>';
			$Ausgabe .= '<div class='.($DS%2==0?'ListenFeldDunkel':'ListenFeldHell').' style="width:250">'.$Form->Format('T',$rsTUEVInfos->FeldInhalt('PRA_BEZEICHNUNG')).'</div>';
			$Ausgabe .= '<div class='.($DS%2==0?'ListenFeldDunkel':'ListenFeldHell').' style="width:100">'.$Form->Format('N2',$rsTUEVInfos->FeldInhalt('PRF_PREIS')).'</div>';
			$Ausgabe .= '<div class='.($DS%2==0?'ListenFeldDunkel':'ListenFeldHell').' style="width:400">'.$Form->Format('T',$rsTUEVInfos->FeldInhalt('PRB_BEMERKUNG')).'</div>';

			$DS++;
			$Ausgabe .= '</div>';
			$rsTUEVInfos->DSWeiter();
		}	// Daten vorhanden?

		return $Ausgabe;
	}
}

/**
 * F�hrt eine SQL Anweisung aus, um ein Recordset zu �ffnen
 *
 * @param awisFiliale $FilObj
 * @param string $Param1
 * @param mixed $Param2
 * @param mixed $Param3
 * @return string
 */
function SQLAusfuehren($FilObj, $Param1, $Param2, $Param3)
{
	try
	{
		$DB = awisDatenbank::NeueVerbindung('AWIS');
		$DB->Oeffnen();
		$Form = new awisFormular();

		$SQL = $FilObj->ErsetzeFilalInfos($Param1);
		$rsDaten = $DB->RecordSetOeffnen($SQL);

		return $rsDaten->FeldInhalt('ANZEIGE');
	}
	catch (awisException $ex)
	{
		return $ex->getMessage();
	}
}