<?php
global $AWISBenutzer;
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Operating','OAE_%');

$OAE_ERR_KEY = $_POST['txtOAE_ERR_KEY'];

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());
$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if(intval($_POST['txtOAE_ERR_KEY'])===0)		// Neuer Eintrag
{

}
else 					// gešnderter Errlog-Eintrag
{
	$Felder = explode(';',awis_NameInArray($_POST, 'txt',1,1));
	$FehlerListe = array();
	$UpdateFelder = '';

	$SQL='SELECT * FROM ERR_LOG WHERE ERR_KEY=' . $_POST['txtOAE_ERR_KEY'] . '';
	$rsERL = awisOpenRecordset($con,$SQL);
	awis_debug(1,$rsERL);
	$FeldListe = '';
	foreach($Felder AS $Feld)
	{
		$FeldName = substr($Feld,3);
		if(isset($_POST['old'.$FeldName]))
		{
			// Alten und neuen Wert umformatieren!!
			$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
			$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsERL[$FeldName][0],true);
			if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
			{
				if($WertAlt != $WertDB AND $WertDB!='null')
				{
					$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
				}
				else
				{
					$FeldListe .= ', '.$FeldName.'=';

					if($_POST[$Feld]=='')	// Leere Felder immer als NULL
					{
						$FeldListe.=' null';
					}
					else
					{
						$FeldListe.=$WertNeu;
					}
				}
			}
		}
	}

	if(count($FehlerListe)>0)
	{
		
		$Meldung = str_replace('%1',$rsERL['USERNAME'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
		foreach($FehlerListe AS $Fehler)
		{
			$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
		}
		awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
	}
	elseif($FeldListe!='')
	{
		$SQL = 'UPDATE ERR_LOG SET';
		$SQL .= substr($FeldListe,1);
		$SQL .= ', USERNAME=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', USERDAT=sysdate';
		$SQL .= ' WHERE ERR_KEY=0' . $_POST['txtOAE_ERR_KEY'] . '';

		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('Operating_AWIS_Errlog',1,'Fehler beim Speichern',$SQL);
		}
	}

	$ERRKEY=$_POST['txtOAE_ERR_KEY'];
//awis_Debug(1,$FeldListe,$SQL,$Felder);
}

?>