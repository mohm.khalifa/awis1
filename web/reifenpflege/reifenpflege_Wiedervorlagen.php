<?php

/**
 * 
 * @author Henry Ott
 * @copyright ATU Auto Teile Unger
 * @version 20130502
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('RRP', '%');
    $TextKonserven[] = array('WVL', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Liste', 'lst_KEINE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[]=array('Liste','lst_JaNein');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht15000 = $AWISBenutzer->HatDasRecht(15000);
    if ($Recht15000 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Form->DebugAusgabe(1, $_POST);
    $Form->DebugAusgabe(1, $_GET);
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_RRP_WVL"));
    
    $Form->DebugAusgabe(1,$Param);

    if (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
    	$Form->DebugAusgabe(1,'geh ins l�schen');
    	include('./reifenpflege_loeschen.php');
    }
    elseif (isset($_GET['WVL_KEY']))
    {
    	$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['WVL_KEY']);
    }
    else   // Letzten Benutzer suchen
    {
    	if(!isset($Param['KEY']))
    	{
    		$Param['KEY']='';
    		$Param['WHERE']='';
    		$Param['ORDER']='';
    	}
    
    	if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
    	{
    		$Param['KEY']=0;
    	}
    
    	$AWIS_KEY1=$Param['KEY'];
    }
    
    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (!isset($_GET['Sort']))
    {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '')
        {
            $ORDERBY = $Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' WVL_DATUM DESC, RRP_ATUNR ASC';
        	$Param['ORDER'] = $ORDERBY;
        }
    }
    else
    {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        $Param['ORDER'] = $ORDERBY;
    }
    
    if (isset($_GET['SortExt']))
    {
    	$ORDERBY .= ','.$_GET['SortExt'];
    }
    
	//********************************************************
	// Daten suchen
	//********************************************************
	
    $SQL  = ' SELECT WVL.*,RRP.RRP_atunr';
    $SQL .= ' ,row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM WIEDERVORLAGEN WVL';    
    $SQL .= ' INNER JOIN REIFENPFLEGE RRP ON RRP_KEY=WVL_XXX_KEY';    
    $SQL .= " WHERE WVL_xtn_kuerzel='RRP'";
    $SQL .= ' AND TRUNC(WVL_datum)<=TRUNC(SYSDATE)';
    $SQL .= ' ORDER BY ' . $ORDERBY;
    //$Form->DebugAusgabe(1,$SQL);
    
    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if (isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        $Param['BLOCK']=$Block;
    }
    /*
    elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
    {
        $Block = 1;
    }
	*/
    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $Form->DebugAusgabe(1,$SQL);
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if ($AWIS_KEY1 <= 0)
    {
        $SQL = 'SELECT * FROM (' . $SQL . ') DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    }
    
    $rsWVL = $DB->RecordSetOeffnen($SQL);
    $Form->DebugAusgabe(1,$DB->LetzterSQL());

    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->SchreibeHTMLCode('<form name=frmReifenpflegeWiedervorlagen action=./reifenpflege_Main.php?cmdAktion=Wiedervorlagen' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . '' . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . ' method=post>');
    
	if ($rsWVL->EOF()) 
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
    }
    else // Liste anzeigen
    {
        $Form->Formular_Start();

        //$Form->ZeileStart('font-size:10pt');
        $Form->ZeileStart();
        
        //TODO $Form->Erstelle_Checkbox('Auswahl_ALLE', 'off', 20, true, 'on', '' );
        $Link = './reifenpflege_Main.php?cmdAktion=Wiedervorlagen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=WVL_DATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'WVL_DATUM')) ? '~' : '');
        $Link .= '&SortExt=RRP_ATUNR ASC';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_DATUM'], 120, '', $Link);

        $Link = './reifenpflege_Main.php?cmdAktion=Wiedervorlagen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_ATUNR')) ? '~' : '');
        $Link .= '&SortExt=WVL_DATUM ASC';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_ATUNR'], 100, '', $Link);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Wiedervorlagen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=WVL_TEXT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'WVL_TEXT')) ? '~' : '');
        $Link .= '&SortExt=WVL_DATUM ASC,RRP_ATUNR ASC';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_TEXT'], 550, '', $Link);

        $Link = './reifenpflege_Main.php?cmdAktion=Wiedervorlagen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=WVL_BEARBEITER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'WVL_BEARBEITER')) ? '~' : '');
        $Link .= '&SortExt=WVL_DATUM ASC,RRP_ATUNR ASC';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_BEARBEITER'],100, '', $Link);
        
        $Link = './reifenpflege_Main.php?cmdAktion=Wiedervorlagen' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=WVL_TYP' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'WVL_TYP')) ? '~' : '');
        $Link .= '&SortExt=WVL_DATUM ASC,RRP_ATUNR ASC';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WVL']['WVL_TYP'], 100, '', $Link);
        
        $Form->ZeileEnde();

        $DS = 0;

        while (!$rsWVL->EOF())
        {
            //$Form->ZeileStart('font-size:10pt');
            $Form->ZeileStart();
            
            
            $Link = './reifenpflege_Main.php?cmdAktion=Details&Seite=Wiedervorlagen&RRP_KEY='. $rsWVL->FeldInhalt('WVL_XXX_KEY') . '&WVL_KEY=' . $rsWVL->FeldInhalt('WVL_KEY').'&Wiedervorlagen';

            //TODO $Style = $rsWVL->FeldInhalt('STYLE');
            $Style = '';
            //TODO $Form->Erstelle_Checkbox('Auswahl_'.$rsWVL->FeldInhalt('WVL_KEY'), 'off', 20, true, 'on', '' );
            
            $Form->Erstelle_ListenFeld('WVL_DATUM', $rsWVL->FeldInhalt('WVL_DATUM'), 10, 120, false, ($DS % 2), $Style, $Link, 'T');
            $Form->Erstelle_ListenFeld('RRP_ATUNR', $rsWVL->FeldInhalt('RRP_ATUNR'), 6, 100, false, ($DS % 2), $Style, '', 'T');
            $Form->Erstelle_ListenFeld('WVL_TEXT', $rsWVL->FeldInhalt('WVL_TEXT'), 100, 550, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('WVL_BEARBEITER', $rsWVL->FeldInhalt('WVL_BEARBEITER'), 40, 100, false, ($DS % 2), $Style, '', '', '', '');
            $Form->Erstelle_ListenFeld('WVL_TYP', $rsWVL->FeldInhalt('WVL_TYP'), 40, 100, false, ($DS % 2), $Style, '', '', '', '');
            
			if($rsWVL->FeldInhalt('WVL_TEXT')!='')
			{
				$Form->Erstelle_HinweisIcon('info', 20, '',$rsWVL->FeldInhalt('WVL_TEXT'));
			}
				
			$Form->ZeileEnde();

            $rsWVL->DSWeiter();
            $DS++;
        }

        $Link = './reifenpflege_Main.php?cmdAktion=Wiedervorlagen&Liste=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();

    }

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if ($Recht15000&8==8)
    {
        //TODO $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
    }

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $AWISBenutzer->ParameterSchreiben("Formular_RRP_WVL", serialize($Param));

    //$Form->SetzeCursor($AWISCursorPosition);
    
}
catch (awisException $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201305021123");
    }
    else
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}
catch (Exception $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201305021234");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

?>