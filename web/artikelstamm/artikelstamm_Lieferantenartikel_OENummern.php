<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('OEN', '%');
    $TextKonserven[] = array('LIE', 'LIE_NAME1');
    $TextKonserven[] = array('HER', 'HER_BEZEICHNUNG');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht454 = $AWISBenutzer->HatDasRecht(454);            // Recht f�r die Lieferantenartikel
    if ($Recht454 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $JaNeinFeld = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);

    $Bedingung = '';
    $SQL = 'SELECT OENummern.*, TEI_USER, TEI_USERDAT, TEI_KEY, HER_BEZEICHNUNG';
    $SQL .= ' FROM OENummern';
    $SQL .= ' INNER JOIN Teileinfos ON TEI_KEY2 = OEN_KEY and TEI_KEY1 = 0' . $AWIS_KEY2 . ' ';
    $SQL .= ' INNER JOIN Hersteller ON OEN_HER_ID = HER_ID';

    if (isset($_GET['OEN_KEY'])) {
        $Bedingung .= ' AND OEN_KEY = 0' . $DB->FeldInhaltFormat('Z', $_GET['OEN_KEY']);
    }

    $SQL .= ($Bedingung != ''?' WHERE ' . substr($Bedingung, 4):'');

    if (isset($_GET['OENSort'])) {
        $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['OENSort']);
    } else {
        $SQL .= ' ORDER BY HER_BEZEICHNUNG, OEN_NUMMER';
    }

    $rsOEN = $DB->RecordSetOeffnen($SQL);

    if (!isset($_GET['OEN_KEY']))                    // Liste anzeigen
    {
        $Form->ZeileStart();

        if (($Recht454 & 4)) {
            $Icons[] = array('new', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern&LAR_KEY=' . $AWIS_KEY2 . '&OEN_KEY=0');
            $Form->Erstelle_ListeIcons($Icons, 38, -1);
        }

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern';
        $Link .= '&OENSort=OEN_NUMMER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_NUMMER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_NUMMER'], 250, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern';
        $Link .= '&OENSort=HER_BEZEICHNUNG' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'HER_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['HER']['HER_BEZEICHNUNG'], 150, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern';
        $Link .= '&OENSort=OEN_MUSTER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_MUSTER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_MUSTER'], 100, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern';
        $Link .= '&OENSort=OEN_BEMERKUNG' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_BEMERKUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_BEMERKUNG'], 350, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern';
        $Link .= '&OENSort=OEN_USER' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_USER'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_USER'], 100, '', $Link);
        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern';
        $Link .= '&OENSort=OEN_USERDAT' . ((isset($_GET['OENSort']) AND ($_GET['OENSort'] == 'OEN_USERDAT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['OEN']['OEN_USERDAT'], 100, '', $Link);

        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsOEN->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            if (($Recht454 & 2))    // �ndernrecht
            {
                $Icons[] = array('edit', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern&OEN_KEY=' . $rsOEN->FeldInhalt('OEN_KEY'));
            }
            if (($Recht454 & 4))    // L�schenrecht
            {
                $Icons[] = array(
                    'delete',
                    './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern&Del=' . $rsOEN->FeldInhalt('TEI_KEY') . '&Key=' . $AWIS_KEY1 . '&LAR_KEY=' . $AWIS_KEY2
                );
            }
            $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));

            $Form->Erstelle_ListenFeld('*OEN_NUMMER', $rsOEN->FeldInhalt('OEN_NUMMER'), 10, 250, false, ($DS % 2), '', '', 'T', 'C');
            $Form->Erstelle_ListenFeld('*OEN_HER_ID', $rsOEN->FeldInhalt('HER_BEZEICHNUNG'), 10, 150, false, ($DS % 2), '', '', 'T', 'C');
            $Muster = '--';
            foreach ($JaNeinFeld AS $JaNeinAnzeige) {
                if (substr($JaNeinAnzeige, 0, 1) == $rsOEN->FeldInhalt('OEN_MUSTER')) {
                    $Muster = substr($JaNeinAnzeige, 2);
                    break;
                }
            }
            $Form->Erstelle_ListenFeld('*OEN_MUSTER', $Muster, 10, 100, false, ($DS % 2), '', '', 'T', 'C', $AWISSprachKonserven['OEN']['ttt_OEN_MUSTER']);
            $Form->Erstelle_ListenFeld('*OEN_BEMERKUNG', $rsOEN->FeldInhalt('OEN_BEMERKUNG'), 10, 350, false, ($DS % 2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('*OEN_USER', $rsOEN->FeldInhalt('OEN_USER'), 10, 100, false, ($DS % 2), '', '', 'T', 'L');
            $Form->Erstelle_ListenFeld('*OEN_USERDAT', $rsOEN->FeldInhalt('OEN_USERDAT'), 10, 100, false, ($DS % 2), '', '', 'D', 'L');

            $Form->ZeileEnde();
            $DS++;

            $rsOEN->DSWeiter();
        }
        $Form->Formular_Ende();
    } else        // Einer oder keiner
    {
        $Form->Formular_Start();

        echo '<input name=txtOEN_KEY type=hidden value=0' . ($rsOEN->FeldInhalt('OEN_KEY')) . '>';
        echo '<input name=txtOEN_IMQ_ID type=hidden value=' . ($rsOEN->FeldInhalt('OEN_IMQ_ID') == ''?$rsOEN->FeldInhalt('OEN_IMQ_ID'):'4') . '>';

        $AWIS_KEY2 = $rsOEN->FeldInhalt('OEN_KEY');
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Lieferantenartikel&Unterseite=OENummern&OENListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0?'':$rsOEN->FeldInhalt('OEN_USER')));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => ($AWIS_KEY1 === 0?'':$rsOEN->FeldInhalt('OEN_USERDAT')));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();

        $EditModus = ($rsOEN->FeldInhalt('OEN_USER') != 'WWS'?($Recht454 & 6):false);

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_NUMMER'] . ':', 150);
        $Form->Erstelle_TextFeld('OEN_NUMMER', $rsOEN->FeldInhalt('OEN_NUMMER'), 20, 200, $EditModus);
        $AWISCursorPosition = ($EditModus?'txtOEN_NUMMER':$AWISCursorPosition);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_HER_ID'] . ':', 150);
        $SQL = 'SELECT HER_ID, HER_BEZEICHNUNG FROM Hersteller WHERE HER_TYP = 1 ORDER BY HER_BEZEICHNUNG';
        $Form->Erstelle_SelectFeld('OEN_HER_ID', $rsOEN->FeldInhalt('OEN_HER_ID'), 200, $EditModus, $SQL, '0~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_MUSTER'] . ':', 150);
        $Form->Erstelle_SelectFeld('OEN_MUSTER', $rsOEN->FeldInhalt('OEN_MUSTER'), 70, $EditModus, '', '', '0', '', '', $JaNeinFeld);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['OEN']['OEN_BEMERKUNG'] . ':', 150);
        $Form->Erstelle_Textarea('OEN_BEMERKUNG', $rsOEN->FeldInhalt('OEN_BEMERKUNG'), 200, 100, 3, $EditModus);
        $Form->ZeileEnde();

        $SQL = '';

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901151024");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200901151025");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>