<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XBV','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_DS%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','XBV_GRUND');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19500 = $AWISBenutzer->HatDasRecht(19500);
	if($Recht19500==0)
	{
	    awisEreignis(3,1000,'Benutzervertretung',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Benutzervertretung'));

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['SuchName'] = $_POST['sucSuchName'];
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./benutzervertretung_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./benutzervertretung_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_POST['cmdDSZurueck_x']))
	{
		$AWIS_KEY1 = $_POST['txtXBV_KEY'];
		$SQL = 'SELECT XBV_KEY';
		$SQL .= ' FROM BENUTZERVERTRETER';
		$SQL .= ' WHERE XBV_KEY < :var_N0_XBV_KEY';
		$SQL .= ' ORDER BY XBV_KEY DESC';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_XBV_KEY'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsXBV = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
		
		if(!$rsXBV->EOF())
		{
			$AWIS_KEY1=$rsXBV->FeldInhalt('XBV_KEY');
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
		$AWIS_KEY1 = $_POST['txtXBV_KEY'];
		$SQL = 'SELECT XBV_KEY';
		$SQL .= ' FROM BENUTZERVERTRETER';
		$SQL .= ' WHERE XBV_KEY > :var_N0_XBV_KEY';
		$SQL .= ' ORDER BY XBV_KEY ASC';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_XBV_KEY'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsXBV = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
		if(!$rsXBV->EOF())
		{
			$AWIS_KEY1=$rsXBV->FeldInhalt('XBV_KEY');
		}
	}
	elseif(isset($_GET['XBV_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['XBV_KEY']);
	}
	elseif(isset($_POST['txtXBV_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtXBV_KEY']);
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_Benutzervertretung',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY XBV_DATUMVOM DESC';
			$Param['ORDER']='XBV_DATUMVOM DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT BENUTZERVERTRETER.*';
	$SQL .= ', XBN_VORNAME, XBN_NAME, XBN_VOLLERNAME';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM BENUTZERVERTRETER';
	$SQL .= ' INNER JOIN BENUTZER XBN ON XBN_KEY = XBV_VERTRETER_XBN_KEY';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
//	$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1==0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_Benutzervertretung',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= ' '.$ORDERBY;

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsXBV = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_Benutzervertretung',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmBenutzervertreter action=./benutzervertretung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST enctype="multipart/form-data">';

	if($rsXBV->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif(($rsXBV->AnzahlDatensaetze()>1 AND (!isset($_GET['XBV_KEY']) OR isset($_GET['Liste']))))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './benutzervertretung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XBV_DATUMVOM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBV_DATUMVOM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBV']['XBV_DATUMVOM'],130,'',$Link);
		$Link = './benutzervertretung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XBV_DATUMBIS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBV_DATUMBIS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBV']['XBV_DATUMBIS'],130,'',$Link);
		$Link = './benutzervertretung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=XBN_NAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBN_NAME'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBV']['XBV_VERTRETER_XBN_KEY'],350,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsXBV->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Style = ($rsXBV->FeldInhalt('ABGELAUFEN')==1?'font-style:italic;color:#FF0000;':'');
			$Link = './benutzervertretung_Main.php?cmdAktion=Details&XBV_KEY=0'.$rsXBV->FeldInhalt('XBV_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('XBV_DATUMVOM',$rsXBV->FeldInhalt('XBV_DATUMVOM'),0,130,false,($DS%2),$Style,$Link,'D','L');
			$Form->Erstelle_ListenFeld('XBV_DATUMBIS',$rsXBV->FeldInhalt('XBV_DATUMBIS'),0,130,false,($DS%2),$Style,'','D','L');
			$Form->Erstelle_ListenFeld('XBN_NAME',$rsXBV->FeldInhalt('XBN_NAME').', '.$rsXBV->FeldInhalt('XBN_VORNAME'),0,350,false,($DS%2),$Style,'','T','L');
			$Form->ZeileEnde();

			$rsXBV->DSWeiter();
			$DS++;
		}

		$Link = './benutzervertretung_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsXBV->FeldInhalt('XBV_KEY');

		$Param['KEY']=$AWIS_KEY1;
        $EditModus = ($Recht19500&6);

		$Form->Erstelle_HiddenFeld('XBV_KEY', $AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./benutzervertretung_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBV->FeldInhalt('XBV_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsXBV->FeldInhalt('XBV_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$AWISCursorPosition = 'txtXBV_DATUMVOM';
		if(((int)$Recht19500&32)==32)     // Alle Mitarbeiter
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['XBV']['XBV_XBN_KEY'].':',190);
		    $Form->Erstelle_TextFeld('*XBN_KEY','',10,100,$EditModus,'','background-color:#22FF22');
		    $AktuelleDaten = ($AWIS_KEY1===0?'':($rsXBV->FeldInhalt('XBV_XBN_KEY')==''?'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']:array($rsXBV->FeldInhalt('XBV_XBN_KEY').'~'.$rsXBV->FeldInhalt('XBN_NAME'))));
		    $Form->Erstelle_SelectFeld('!XBV_XBN_KEY',$rsXBV->FeldInhalt('XBV_XBN_KEY'),'300:270',$EditModus,'*F*XBN_Daten;sucXBN_KEY;XBN_KEY='.$rsXBV->FeldInhalt('XBV_XBN_KEY').';;','','','','',$AktuelleDaten);
		    $Form->ZeileEnde();
		    $AWISCursorPosition='sucXBN_KEY';
		}
		elseif(((int)$Recht19500&16)==16) // Alle Mitarbeiter in der Abteilung (nach Kontakten)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['XBV']['XBV_XBN_KEY'].':',190);
		    $Form->Erstelle_TextFeld('*XBN_KEY','',10,100,$EditModus,'','background-color:#22FF22');
		    $AktuelleDaten = ($AWIS_KEY1===0?'':($rsXBV->FeldInhalt('XBV_XBN_KEY')==''?'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']:array($rsXBV->FeldInhalt('XBV_XBN_KEY').'~'.$rsXBV->FeldInhalt('XBN_NAME'))));
		    $Form->Erstelle_SelectFeld('!XBV_XBN_KEY',$rsXBV->FeldInhalt('XBV_XBN_KEY'),'300:270',$EditModus,'*F*XBN_Daten;sucXBN_KEY;XBN_KEY='.$rsXBV->FeldInhalt('XBV_XBN_KEY').'&KON_KEY='.$AWISBenutzer->BenutzerKontaktKEY().';;','','','','',$AktuelleDaten);
		    $Form->ZeileEnde();
		    $AWISCursorPosition='sucXBN_KEY';
		}
		else      // Nur f�r eigene Mitarbeiter
		{
            $Form->Erstelle_HiddenFeld('XBV_XBN_KEY', $AWISBenutzer->BenutzerID());
		}


		$EditRecht=(($Recht19500&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht19500&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBV']['XBV_DATUMVOM'].':',190);
		$Form->Erstelle_TextFeld('!XBV_DATUMVOM',$rsXBV->FeldInhalt('XBV_DATUMVOM'),10,150,$EditRecht,'','','','D','L','',date('d.m.Y'));
		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBV']['XBV_DATUMBIS'].':',120);
		$Form->Erstelle_TextFeld('!XBV_DATUMBIS',$rsXBV->FeldInhalt('XBV_DATUMBIS'),10,150,$EditRecht,'','','','D','L','',date('d.m.Y',time()+(60*60*24*14)));
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBV']['XBV_VERTRETER_XBN_KEY'].':',190);
		$Form->Erstelle_TextFeld('*VERTR_XBN_KEY','',10,100,$EditRecht,'','background-color:#22FF22');
		$AktuelleDaten = ($AWIS_KEY1===0?'':($rsXBV->FeldInhalt('XBV_VERTRETER_XBN_KEY')==''?'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']:array($rsXBV->FeldInhalt('XBV_VERTRETER_XBN_KEY').'~'.$rsXBV->FeldInhalt('XBN_NAME'))));
		$Form->Erstelle_SelectFeld('!XBV_VERTRETER_XBN_KEY',$rsXBV->FeldInhalt('XBV_VERTRETER_XBN_KEY'),'300:270',$EditRecht,'*F*XBN_Daten;sucVERTR_XBN_KEY;XBN_KEY='.$rsXBV->FeldInhalt('XBV_VERTRETER_XBN_KEY').';;','','','','',$AktuelleDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBV']['XBV_GRUND'].':',190);
		$Gruende = explode("|",$AWISSprachKonserven['Liste']['XBV_GRUND']);
		$Form->Erstelle_SelectFeld('!XBV_GRUND',($rsXBV->FeldInhalt('XBV_GRUND')),'300:200',$EditRecht,'',$OptionBitteWaehlen,'','','',$Gruende);
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$AWISBenutzer->ParameterSchreiben('Formular_Benutzervertretung',serialize($Param));
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht19500&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if($DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
		$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
	}

	if(($Recht19500&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	if(($Recht19500&8)==8 AND $DetailAnsicht AND $AWIS_KEY1>0)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWIS_KEY2;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND XBV_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['XBV_DATUMVOM']) AND $Param['XBV_DATUMVOM']!='')
	{
		$Bedingung .= ' AND (XBV_DATUMVOM ' . $DB->LIKEoderIST($Param['XBV_DATUMVOM'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['XBV_KUNDENID']) AND $Param['XBV_KUNDENID']!='')
	{
		$Bedingung .= ' AND XBV_KUNDENID = ' . $DB->FeldInhaltFormat('T',$Param['XBV_KUNDENID']) . ' ';
	}

	if(isset($Param['SVT_KENNUNG']) AND $Param['SVT_KENNUNG']!='')
	{
		// TODO: Da muss noch die Abfrage um das Date�m erweitert werden
		$Bedingung .= ' AND EXISTS(SELECT * FROM SVCTeilnehmer WHERE SVT_KENNUNG = ' . $DB->FeldInhaltFormat('T',$Param['SVT_KENNUNG']) . ' AND SVT_XBV_KEY = XBV_KEY) ';
		// Um in das Detail springen zu k�nnen!
		$SQL = ' SELECT SVT_KEY FROM SVCTeilnehmer WHERE SVT_KENNUNG = ' . $DB->FeldInhaltFormat('T',$Param['SVT_KENNUNG']);
		$rsSVT = $DB->RecordSetOeffnen($SQL);
		if($rsSVT->AnzahlDatensaetze()==1)
		{
			$AWIS_KEY2 = $rsSVT->FeldInhalt('SVT_KEY');
		}
	}

	if(isset($Param['SuchName']) AND $Param['SuchName']!='')
	{
		$Bedingung .= 'AND (UPPER(CA1_Name1) ' . $DB->LIKEoderIST($Param['SuchName'].'%',1) . ' ';
		$Bedingung .= 'OR UPPER(CA2_Name1) ' . $DB->LIKEoderIST($Param['SuchName'].'%',1) . ' ';
		$Bedingung .= ')';
	}	

	return $Bedingung;
}
?>