<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'DistributionCenter.php';

/**
 * Class awisDITTreeAPI
 *
 * Muss DRINGEND überarbeitet werden. Wurde nur Quick and Dirty gemacht, damit der Sprint nicht in Gefahr ist ;)
 *
 */

class awisDITTreeAPI
{
    private $DB;
    private $Benutzer;
    private $Tree;


    function __construct()
    {
        $this->Benutzer = awisBenutzer::Init('shuttle');
        $this->DB = awisDatenbank::NeueVerbindung('AWIS_PROD');
        $this->DB->Oeffnen();
        $this->Tree = array();

        $this->VerarbeiteDaten();

    }

    private function VerarbeiteDaten()
    {
        $DistributionCenter = new awisDistributionCenter($this->DB, 369);
        $Center = $DistributionCenter->getDistributionCenter(false, true, true);

        //Die Erste Ebene "ATU":
        $this->Tree['text'] = $Center->text;
        $this->Tree['key'] = '';
        $this->Tree['children'] = array();

        $Regionen1 = array();
        //Regionen von ATU durchgehen
        foreach ($Center->children as $Regionen) {
            //Region bennenen
            $Region = array();
            $Region['text'] = $Regionen->text;
            $Region['key'] = '';
            $Region['children'] = array();
            if (isset($Regionen->children)) {
                $Gebiete1 = array();
                //Gebiete der Region durchgehen
                foreach ($Regionen->children as $Gebiete) {
                    $Gebiet = array();
                    $Gebiet['text'] = $Gebiete->text;
                    $Gebiet['key'] = '';
                    $Gebiet['children'] = array();
                    if (isset($Gebiete->children)) {
                        $Filialen1 = array();
                        //Filialen der Region durchgehen
                        foreach ($Gebiete->children as $Filialen) {

                            $Filiale = array();
                            $Filiale['text'] = $Filialen->text;
                            $Filiale['key'] = 'FIL_'.$Filialen->key;

                            //Vertriebspersonal auf die Filiale nehmen und so benennen
                            $Bezeichner = array();
                            $Bezeichner['text'] = 'Filialpersonal';
                            $Bezeichner['key'] = 'filialpersonal';
                            $Bezeichner['children'] = array();
                            $Filiale['children'][] = $Bezeichner;

                            $Filialen1[] = $Filiale;


                        }
                        //Filialen auf das Gebiet nehmen und so benennen
                        $Bezeichner = array();
                        $Bezeichner['text'] = 'Filialen';
                        $Bezeichner['key'] = 'filiale';
                        $Bezeichner['children'] = $Filialen1;
                        $Gebiet['children'][] = $Bezeichner;


                        //Vertriebspersonal auf das Gebiet nehmen und so benennen
                        $Bezeichner = array();
                        $Bezeichner['text'] = 'Vertriebspersonal';
                        $Bezeichner['key'] = 'Vertriebspersonal';
                        $Vertriebspersonal1 = array();

                        if(isset($Gebiete->SalesForce)){
                            foreach($Gebiete->SalesForce as $SalesForce){
                                $Vertriebspersonal = array();
                                $Vertriebspersonal['text'] = $SalesForce->FirstName . ' ' . $SalesForce->LastName .' ('.$SalesForce->Role.')';
                                $Vertriebspersonal['key'] = $SalesForce->Key;
                                $Vertriebspersonal1[] = $Vertriebspersonal;
                            }
                        }

                        $Bezeichner['children'] = $Vertriebspersonal1;
                        $Gebiet['children'][] = $Bezeichner;
                    }
                    $Gebiete1[] = $Gebiet;

                }

                //Alle Gebiete auf die Region nehmen und so bennenen.
                $Bezeichner = array();
                $Bezeichner['text'] = 'Gebiete';
                $Bezeichner['key'] = 'gebiete';
                $Bezeichner['children'] = $Gebiete1;
                $Region['children'][] = $Bezeichner;

                //Vertriebspersonal auf die Region nehmen
                $Bezeichner = array();
                $Bezeichner['text'] = 'Vertriebspersonal';
                $Bezeichner['key'] = 'vertriebspersonal1';

                $Vertriebspersonal1 = array();

                if(isset($Regionen->SalesForce)){
                    foreach($Regionen->SalesForce as $SalesForce){
                        $Vertriebspersonal = array();
                        $Vertriebspersonal['text'] = $SalesForce->FirstName . ' ' . $SalesForce->LastName .' ('.$SalesForce->Role.')';
                        $Vertriebspersonal['key'] = $SalesForce->Key;
                        $Vertriebspersonal1[] = $Vertriebspersonal;
                    }
                }

                $Bezeichner['children'] = $Vertriebspersonal1;
                $Region['children'][] = $Bezeichner;

            }

            $Regionen1[] = $Region;

        }
        $Bezeichner = array();
        $Bezeichner['text'] = 'Regionen';
        $Bezeichner['key'] = 'regionen';
        $Bezeichner['children'] = $Regionen1;
        $this->Tree['children'][] = $Bezeichner;

    }

    public function getTree()
    {
        return $this->Tree;
    }




}