<?php
//**************************************************************
// Liefert die Teilnehmer f�r Servicevereinbarungen
// 
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['SUCHNAME']))
{
	die('##PARAM##');
}
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// Auswahlliste in Abh�ngigkeit der Rechte anzeigen
$SQL = 'SELECT SVT_KEY AS KEY, SVT_BEZEICHNUNG || \' - \' || CAD_NAME1 || CASE WHEN SVT_DATUMBIS < SYSDATE THEN \' (I)\' ELSE \'\' END AS ANZEIGE';
$SQL .= ' FROM SVCTEILNEHMER';
$SQL .= ' INNER JOIN SVCVEREINBARUNGEN ON SVT_SVB_KEY = SVB_KEY';
$SQL .= ' INNER JOIN CRMADRESSEN ON SVB_CAD_KEY = CAD_KEY';
$SQL .= ' WHERE UPPER(SVT_BEZEICHNUNG) LIKE :var_T_SVT_BEZEICHNUNG';
$SQL .= ' ORDER BY 2';
$DB->SetzeBindevariable('SVT', 'var_T_SVT_BEZEICHNUNG', $_GET['SUCHNAME'].'*', awisDatenbank::VAR_TYP_TEXT,'TU');
$rsPEB = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SVT'));

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
if($_GET['Zusatz'])
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">'.$_GET['Zusatz'].'</option>';
	}
	else
	{
		echo '#+##+#'.$_GET['Zusatz'].'';
	}
}
$DS=0;
while(!$rsPEB->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsPEB->FeldInhalt('KEY').'">'.$rsPEB->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsPEB->FeldInhalt('KEY').'#+#'.$rsPEB->FeldInhalt('ANZEIGE').'';
	}

	if(++$DS>200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="">...</option>';
		}
		else
		{
			echo '#+##+#...';
		}
		break;
	}
	$rsPEB->DSWeiter();
	
	
}
?>