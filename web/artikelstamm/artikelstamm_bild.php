<?php
require_once('artikelstamm_bild.inc');

$Bild = new awisArtikelstammBild($_GET['ATUNR']);
if ($Bild->BildVorhanden()) {
    $Bild->BildAnzeigen();
} else {
    echo 'Kein Bild';
}
?>