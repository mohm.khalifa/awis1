<?php
global $AWIS_KEY2;				// UnfallMeldungenInfoTypen: UFT_KEY
global $Aktion;					// Merker/Wert f�r GET: cmdAktion (Reiter)
global $Seite;					// Merker/Wert f�r GET: Seite (Subreiter)
global $ListeAnzeigen;			// Flag: Liste anzeigen (nicht Detailansicht)

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	$TextKonserven[]=array('Wort','WirklichLoeschen');
	$TextKonserven[]=array('Wort','Ja');
	$TextKonserven[]=array('Wort','Nein');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);	

	// UFT_KEY (=AWIS_KEY2) holen (Post, aus diesem Formular => siehe else-Zweig weiter unten; Get aus Liste von unfall_Stammdaten_Seite.php)
	$AWIS_KEY2 = (isset($_POST['txtUFT_KEY'])? $_POST['txtUFT_KEY']: (isset($_GET['UFT_KEY'])? $_GET['UFT_KEY']: 0));
	
	if (isset($_POST['cmdLoeschenOK']))		
	{	// wenn Best�tigung zum L�schen (cmdLoeschenOK => siehe diesen else-Zweig), dann durchf�hren
		$SQL =	'update unfallmeldunginfotypen set ' .
				'uft_status = 0 ' .						// nicht L�schen, sondern ausblenden, wegen Referenzen auf andere Tabellen (leider kein FK m�glich, wegen InfoTypen) 
				'where uft_key = :var_UftKey';			 
		$BindeVariablen = array();
		$BindeVariablen['var_UftKey'] = $AWIS_KEY2;
		$DB->Ausfuehren($SQL, '', true, $BindeVariablen);
		$AWIS_KEY2 = 0;				// zur�cksetzen
		$ListeAnzeigen = true;		// Flag, damit Liste angezeigt wird (unfall_Stammdaten_Seite.php)		
	}
	elseif (isset($_POST['cmdLoeschenAbbrechen']))
	{	// wenn L�schen-Abbruch (cmdLoeschenAbbrechen => siehe diesen else-Zweig), dann durchf�hren
		$AWIS_KEY2 = 0;				// zur�cksetzen
		$ListeAnzeigen = true;		// Flag, damit Liste angezeigt wird (unfall_Stammdaten_Seite.php)		
	}	
	else 
	{	// Sicherheitsfrage, ob Daten gel�scht werden sollen
		$Form->SchreibeHTMLCode("<form name=frmLoeschen action=./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite method=post>");
		$Form->Formular_Start();
		
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		// Infos zum hinschreiben holen
		$SQL =  'select * ' . 
				'from unfallmeldunginfotypen ' . 
				'where uft_key = :var_UftKey';
		$BindeVariablen = array();
		$BindeVariablen['var_UftKey'] = $AWIS_KEY2;
		$rsUft = $DB->RecordSetOeffnen($SQL, $BindeVariablen);	
	
		$Form->Erstelle_HiddenFeld('UFT_KEY', $AWIS_KEY2);		// �bergabe f�r dieses Formular (siehe if- und elseif-Zweig)
		
		$Form->ZeileStart();		// ID (DB: Kennung)
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['KENNUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('UFT_KENNUNG', $rsUft->FeldInhalt('UFT_KENNUNG'), 100, 500, false);
		$Form->ZeileEnde();		

		$Form->ZeileStart();		// Bezeichnung (DB: Information)
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEZEICHNUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('UFT_INFORMATION', $rsUft->FeldInhalt('UFT_INFORMATION'), 100, 500, false);
		$Form->ZeileEnde();		

		$Form->ZeileStart();		// Bemerkung
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEMERKUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('UFT_BEMERKUNG', $rsUft->FeldInhalt('UFT_BEMERKUNG'), 100, 500, false);
		$Form->ZeileEnde();		
		
		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit', 'cmdLoeschenOK', '', '', $AWISSprachKonserven['Wort']['Ja'], '');
		$Form->Schaltflaeche('submit', 'cmdLoeschenAbbrechen', '', '', $AWISSprachKonserven['Wort']['Nein'], '');
		$Form->ZeileEnde();

		$Form->Formular_Ende();
		$Form->SchreibeHTMLCode('</form>');
		die();		
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>