<?php
require_once("awisJobs.inc");
require_once("awisVersicherungenFunktionen.php");
require_once("awisDatenbank.inc");
require_once("awisFormular.inc");

/**
 * Created by PhpStorm.
 * User: roesch_n
 * Date: 22.11.2016
 * Time: 10:46
 */
class VersDatenAbholung
{
    private $_DebugLevel;
    private $_Werkzeug;
    private $_JobID;
    private $_EMAIL_Infos = array('shuttle@de.atu.eu');
    private $_awisJobs;
    private $_errMsg;
    private $_transNummern = [];
    private $_COM_Link = '';

    function __construct($DebugLevel = 0, $Benutzer)
    {

        $AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Functions = new VersicherungenFunktionen();
        $this->_DebugLevel = $DebugLevel;
        $this->_Werkzeug = new awisWerkzeuge();
        $this->_awisJobs = new awisJobs($Benutzer);
        $this->_setzeComServerLink();
    }

    private function _setzeComServerLink () {
        $level = $this->_Werkzeug->awisLevel();

        if($level == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV or $level == awisWerkzeuge::AWIS_LEVEL_SHUTTLE) {
            $this->_COM_Link = '@COM_DE.ATU.DE';
        } elseif ($level == awisWerkzeuge::AWIS_LEVEL_STAGING) {
            $this->_COM_Link = '@COM_TEST_DE.ATU.DE';
        } else {
            $this->_COM_Link = '';
        }
    }

    public function HoleVersDaten($Methode)
    {
        try
        {
            $SQL = 'Select VJI_KEY,VJI_JOBID,VJI_DATUMJOBID from VERSJOBID ORDER BY VJI_KEY';
            $rsJobID = $this->_DB->RecordSetOeffnen($SQL);

            $this->_JobID = $rsJobID->FeldInhalt('VJI_JOBID');

            $jobSQL = 'SELECT *';
            $jobSQL .= '   FROM VersJobSteuerung vjs';
            $jobSQL .= '   INNER JOIN VersJobID vji';
            $jobSQL .= '   ON vjs.VJS_JOBID     < vji.VJI_JOBID';

            if($this->_DB->ErmittleZeilenAnzahl($jobSQL) == 0)
            {
                $this->_JobID++;

                $SQL = 'UPDATE VERSJOBID SET ';
                $SQL .= ' VJI_JOBID=' . $this->_DB->FeldInhaltFormat('NO', $this->_JobID, false) . ', ';
                $SQL .= ' VJI_DATUMJOBID=sysdate';

                $this->_DB->Ausfuehren($SQL);
            }

            if($Methode == 'transaction')
            {
                $SQL = 'SELECT * FROM VERS_DWHTRANS WHERE VDT_ARBEITSSTATUS = \'L\'';

                $rsLoad = $this->_DB->RecordSetOeffnen($SQL);

                $starteLoad = false;
                if($rsLoad->AnzahlDatensaetze() > 0)
                {
                    $starteLoad = true;
                }
            }
            else
            {
                //Wenn ein Full-Load erfolgen soll
                //muss nicht auf die COM-Loads abgeprueft werden,
                //da dann der Load immer sofort erfolgen kann
                $starteLoad = true;
                $this->_Functions->SchreibeModulStatus('Pruefe_COMLoad', 'Keine Pruefung, da Full-Load vom DWH.');
            }

            $this->debugAusgabe("START: Pruefe_COMLoad", 10);

            if($this->_Functions->PruefeAnstehend('Pruefe_COMLoad', 'DatenLoad'))
            {
                $this->_Functions->SchreibeStartZeit('Pruefe_COMLoad');

                if($starteLoad)
                {
                    $this->_Functions->SchreibeModulStatus('Pruefe_COMLoad', 'Pruefung beendet. Daten stehen zum Abholen bereit.');
                    $this->_Functions->SchreibeEndeZeit('Pruefe_COMLoad', $this->_JobID);
                }
                else
                {
                    $this->_Functions->SchreibeModulStatus('Pruefe_COMLoad', 'Noch keine Loads verfuebar');
                    $this->debugAusgabe("Es sind derzeit noch keine abgeschlossenen Loads verfuegbar", 10);
                }
            }
            $this->debugAusgabe("ENDE: Pruefe_COMLoad", 10);

            $this->debugAusgabe("START: Import_Daten", 10);

            if($this->_Functions->PruefeAnstehend('Import_Daten', 'DatenLoad'))
            {
                $this->_Functions->SchreibeStartZeit('Import_Daten');
                $this->_Functions->SchreibeModulStatus('Import_Daten', 'Start: Import Daten');

                if($Methode == 'transaction')
                {
                    //hier sollte gesichert sein, dass in dem Recordset auch Daten enthalten sind,
                    //sonst wuerde der Job, nicht bis zu dieser Stelle gelangen
                    while(!$rsLoad->EOF())
                    {
                        $transid = $rsLoad->FeldInhalt('VDT_TRANSID');
                        $anzVersuche = 1;
                        $this->_transNummern[] = $transid;
                        while(!$this->TransaktionsLoad($this->_COM_Link, $transid))
                        {
                            sleep(300);
                            if($anzVersuche == 5)
                            {
                                $SQL = 'SELECT XJO_KEY';
                                $SQL .= ' FROM JOBLISTE';
                                $SQL .= ' WHERE XJO_JOBBEZEICHNUNG = \'HoleVersDaten\'';

                                $rsXJOKEY = $this->_DB->RecordSetOeffnen($SQL);

                                $this->_awisJobs->SetzeNaechstenStart($rsXJOKEY->FeldInhalt('XJO_KEY'), '');
                                $this->_errMsg .= 'WICHITG!! Der Job wurde nach ' . $anzVersuche . ' Ausfuerungsversuchen deaktiviert (Jobliste -> NaechsterStart = null).' . PHP_EOL;
                                $this->_Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERS_TRANSAKTIONSLOAD', $this->_errMsg, 2, '', 'shuttle@de.atu.eu');

                                die();
                            }
                            $anzVersuche++;
                        }

                        //Wenn der Import erfolgreich war, wird die entsprechende TransId auf den Status 'T'
                        // fuer transferiert gesetzt
                        $SQL = 'UPDATE VERS_DWHTRANS';
                        $SQL .= ' SET VDT_ARBEITSSTATUS = \'T\'';
                        $SQL .= ' WHERE VDT_TRANSID = ' . $this->_DB->WertSetzen('VDT', 'T', $transid);

                        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VDT', true));

                        $rsLoad->DSWeiter();
                    }
                }
                else
                {
                    $this->FullLoad();
                }
                $this->_Functions->SchreibeModulStatus('Import_Daten', 'Ende: Import Daten');
                $this->_Functions->SchreibeEndeZeit('Import_Daten', $this->_JobID);
            }
            $this->debugAusgabe("ENDE: Import_Daten", 10);
        }
        catch(awisException $ex)
        {
            $this->_errMsg = date('d.m.y H:i:s ') . ': awisVersDatenAbholung: Fehler: ' . $ex->getMessage() . PHP_EOL;
            $this->_errMsg .= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            $this->_errMsg .= 'CODE:  ' . $ex->getCode() . PHP_EOL;
            $this->_errMsg .= 'Zeile: ' . $ex->getLine() . PHP_EOL;
            $this->_errMsg .= 'Datei: ' . $ex->getFile() . PHP_EOL;
            $this->_errMsg .= 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            echo $this->_errMsg;

            $this->_Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSDATENDATENABHOLUNG', $this->_errMsg, 2, '', 'shuttle@de.atu.eu');
            die();
        }
        catch(Exception $ex)
        {
            $this->_errMsg = date('d.m.y H:i:s ') . ': awisVersDatenAbholung: Fehler: ' . $ex->getMessage() . PHP_EOL;
            $this->_errMsg .= 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            $this->_errMsg .= 'CODE:  ' . $ex->getCode() . PHP_EOL;
            $this->_errMsg .= 'Zeile: ' . $ex->getLine() . PHP_EOL;
            $this->_errMsg .= 'Datei: ' . $ex->getFile() . PHP_EOL;
            $this->_errMsg .= 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            echo $this->_errMsg;

            $this->_Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSDATENDATENABHOLUNG', $this->_errMsg, 2, '', 'shuttle@de.atu.eu');

            die();
        }
    }

    public function TransaktionsLoad($COM_Link, $transid)
    {
        try
        {
            $this->_DB->TransaktionBegin();

            $this->debugAusgabe("START: IMPORT in Tabelle VERS_EXP_KOPF", 10);

            $SQL = 'MERGE INTO VERS_EXP_KOPF vek USING';
            $SQL .= ' (SELECT *';
            $SQL .= ' FROM V_VERS_EXP_KOPF' . $COM_Link;
            $SQL .= ' WHERE TRANSAKTIONSID =' . $this->_DB->WertSetzen('VEK', 'T', $transid);
            $SQL .= ' ) vcom ON (vek.TRANSAKTIONSID = vcom.TRANSAKTIONSID)';
            $SQL .= ' WHEN NOT MATCHED THEN';
            $SQL .= '   INSERT';
            $SQL .= '     (';
            $SQL .= '       KENNUNG,';
            $SQL .= '       FILID,';
            $SQL .= '       WANR,';
            $SQL .= '       KUNDEN_ART,';
            $SQL .= '       FIRKU_ART,';
            $SQL .= '       ZAHL_ART,';
            $SQL .= '       VORST_ABZUG_KZ,';
            $SQL .= '       VORST_BETRAG,';
            $SQL .= '       SELBST_KZ,';
            $SQL .= '       SELBST_BETRAG,';
            $SQL .= '       VERS_NR,';
            $SQL .= '       VERS_BEZ,';
            $SQL .= '       VERS_STRASSE,';
            $SQL .= '       VERS_HNR,';
            $SQL .= '       VERS_PLZ,';
            $SQL .= '       VERS_ORT,';
            $SQL .= '       VERS_STAAT_NR,';
            $SQL .= '       VERS_TELEFON,';
            $SQL .= '       VERSSCHEINNR,';
            $SQL .= '       SCHADEN_NR,';
            $SQL .= '       SCHADEN_DATUM,';
            $SQL .= '       SCHADEN_KZ,';
            $SQL .= '       SCHADEN_BEZ,';
            $SQL .= '       SCHADEN_ART,';
            $SQL .= '       UMSATZ_GESAMT,';
            $SQL .= '       UMSATZ_KUNDE,';
            $SQL .= '       UMSATZ_VERS,';
            $SQL .= '       AEM_NR,';
            $SQL .= '       ANREDE,';
            $SQL .= '       TITEL,';
            $SQL .= '       NAME1,';
            $SQL .= '       NAME2,';
            $SQL .= '       NAME3,';
            $SQL .= '       STRASSE,';
            $SQL .= '       HNR,';
            $SQL .= '       PLZ,';
            $SQL .= '       ORT,';
            $SQL .= '       RUFNR1,';
            $SQL .= '       RUFNR2,';
            $SQL .= '       STAAT_NR,';
            $SQL .= '       GK_ANREDE,';
            $SQL .= '       GK_TITEL,';
            $SQL .= '       GK_NAME1,';
            $SQL .= '       GK_NAME2,';
            $SQL .= '       GK_NAME3,';
            $SQL .= '       GK_STRASSE,';
            $SQL .= '       GK_HNR,';
            $SQL .= '       GK_PLZ,';
            $SQL .= '       GK_ORT,';
            $SQL .= '       GK_RUFNR1,';
            $SQL .= '       GK_RUFNR2,';
            $SQL .= '       GK_STAAT_NR,';
            $SQL .= '       AC_ANREDE,';
            $SQL .= '       AC_TITEL,';
            $SQL .= '       AC_NAME1,';
            $SQL .= '       AC_NAME2,';
            $SQL .= '       AC_STRASSE,';
            $SQL .= '       AC_HNR,';
            $SQL .= '       AC_PLZ,';
            $SQL .= '       AC_ORT,';
            $SQL .= '       AC_RUFNR1,';
            $SQL .= '       AC_RUFNR2,';
            $SQL .= '       AC_STAAT_NR,';
            $SQL .= '       KFZ_KZ,';
            $SQL .= '       KFZ_HERSTELLER,';
            $SQL .= '       KFZ_MODELL,';
            $SQL .= '       KFZ_TYP,';
            $SQL .= '       FAHRGESTELLNR,';
            $SQL .= '       KFZ_HERSTNR,';
            $SQL .= '       KTYPNR,';
            $SQL .= '       KFZ_KM,';
            $SQL .= '       QUELLE,';
            $SQL .= '       TRANSAKTIONSID,';
            $SQL .= '       INSDATE,';
            $SQL .= '       KFZ_EZ';
            $SQL .= '     )';
            $SQL .= '     VALUES';
            $SQL .= '     (';
            $SQL .= '       vcom.KENNUNG,';
            $SQL .= '       vcom.FILID,';
            $SQL .= '       vcom.WANR,';
            $SQL .= '       vcom.KUNDEN_ART,';
            $SQL .= '       vcom.FIRKU_ART,';
            $SQL .= '       vcom.ZAHL_ART,';
            $SQL .= '       vcom.VORST_ABZUG_KZ,';
            $SQL .= '       vcom.VORST_BETRAG,';
            $SQL .= '       vcom.SELBST_KZ,';
            $SQL .= '       vcom.SELBST_BETRAG,';
            $SQL .= '       vcom.VERS_NR,';
            $SQL .= '       vcom.VERS_BEZ,';
            $SQL .= '       vcom.VERS_STRASSE,';
            $SQL .= '       vcom.VERS_HNR,';
            $SQL .= '       vcom.VERS_PLZ,';
            $SQL .= '       vcom.VERS_ORT,';
            $SQL .= '       vcom.VERS_STAAT_NR,';
            $SQL .= '       vcom.VERS_TELEFON,';
            $SQL .= '       vcom.VERSSCHEINNR,';
            $SQL .= '       vcom.SCHADEN_NR,';
            $SQL .= '       vcom.SCHADEN_DATUM,';
            $SQL .= '       vcom.SCHADEN_KZ,';
            $SQL .= '       vcom.SCHADEN_BEZ,';
            $SQL .= '       vcom.SCHADEN_ART,';
            $SQL .= '       vcom.UMSATZ_GESAMT,';
            $SQL .= '       vcom.UMSATZ_KUNDE,';
            $SQL .= '       vcom.UMSATZ_VERS,';
            $SQL .= '       vcom.AEM_NR,';
            $SQL .= '       vcom.ANREDE,';
            $SQL .= '       vcom.TITEL,';
            $SQL .= '       vcom.NAME1,';
            $SQL .= '       vcom.NAME2,';
            $SQL .= '       vcom.NAME3,';
            $SQL .= '       vcom.STRASSE,';
            $SQL .= '       vcom.HNR,';
            $SQL .= '       vcom.PLZ,';
            $SQL .= '       vcom.ORT,';
            $SQL .= '       vcom.RUFNR1,';
            $SQL .= '       vcom.RUFNR2,';
            $SQL .= '       vcom.STAAT_NR,';
            $SQL .= '       vcom.GK_ANREDE,';
            $SQL .= '       vcom.GK_TITEL,';
            $SQL .= '       vcom.GK_NAME1,';
            $SQL .= '       vcom.GK_NAME2,';
            $SQL .= '       vcom.GK_NAME3,';
            $SQL .= '       vcom.GK_STRASSE,';
            $SQL .= '       vcom.GK_HNR,';
            $SQL .= '       vcom.GK_PLZ,';
            $SQL .= '       vcom.GK_ORT,';
            $SQL .= '       vcom.GK_RUFNR1,';
            $SQL .= '       vcom.GK_RUFNR2,';
            $SQL .= '       vcom.GK_STAAT_NR,';
            $SQL .= '       vcom.AC_ANREDE,';
            $SQL .= '       vcom.AC_TITEL,';
            $SQL .= '       vcom.AC_NAME1,';
            $SQL .= '       vcom.AC_NAME2,';
            $SQL .= '       vcom.AC_STRASSE,';
            $SQL .= '       vcom.AC_HNR,';
            $SQL .= '       vcom.AC_PLZ,';
            $SQL .= '       vcom.AC_ORT,';
            $SQL .= '       vcom.AC_RUFNR1,';
            $SQL .= '       vcom.AC_RUFNR2,';
            $SQL .= '       vcom.AC_STAAT_NR,';
            $SQL .= '       vcom.KFZ_KZ,';
            $SQL .= '       vcom.KFZ_HERSTELLER,';
            $SQL .= '       vcom.KFZ_MODELL,';
            $SQL .= '       vcom.KFZ_TYP,';
            $SQL .= '       vcom.FAHRGESTELLNR,';
            $SQL .= '       vcom.KFZ_HERSTNR,';
            $SQL .= '       vcom.KTYPNR,';
            $SQL .= '       vcom.KFZ_KM,';
            $SQL .= '       vcom.QUELLE,';
            $SQL .= '       vcom.TRANSAKTIONSID,';
            $SQL .= '       vcom.DATUMZEIT,';
            $SQL .= '       vcom.KFZ_EZ';
            $SQL .= '     )';

            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VEK', true));

            $this->debugAusgabe("ENDE: IMPORT in Tabelle VERS_EXP_KOPF", 10);

            $this->debugAusgabe("START: IMPORT in Tabelle VERS_FORM_BEARBDATEN", 10);

            $SQL = 'MERGE INTO VERS_FORM_BEARBDATEN vbe USING';
            $SQL .= '              (    SELECT vbe.FILNR,';
            $SQL .= '                    vbe.WANR,';
            $SQL .= '                    vbe.STATUS,';
            $SQL .= '                    vbe.DATUM,';
            $SQL .= '                    vbe.ZEIT,';
            $SQL .= '                    vbe.VERKAEUFERART,';
            $SQL .= '                    pers.NAME,';
            $SQL .= '                    vbe.QUELLE,';
            $SQL .= '                    vbe.TRANSAKTIONSID,';
            $SQL .= '                    vbe.DATUMZEIT';
            $SQL .= '              FROM V_VERS_FORM_BEARBDATEN' . $COM_Link . ' vbe';
            $SQL .= '              LEFT JOIN PERSONAL_KOMPLETT pers';
            $SQL .= '              ON vbe.PERSNR = pers.PERSNR';
            $SQL .= '              WHERE TRANSAKTIONSID =' . $this->_DB->WertSetzen('VBE', 'T', $transid);
            $SQL .= '              ) vcom ON (vbe.TRANSAKTIONSID = vcom.TRANSAKTIONSID)';
            $SQL .= '              WHEN NOT MATCHED THEN';
            $SQL .= '                INSERT';
            $SQL .= '                  (';
            $SQL .= '                    FILNR,';
            $SQL .= '                    WANR,';
            $SQL .= '                    STATUS,';
            $SQL .= '                    DATUM,';
            $SQL .= '                    ZEIT,';
            $SQL .= '                    VERKAEUFERART,';
            $SQL .= '                    NAME,';
            $SQL .= '                    QUELLE,';
            $SQL .= '                    TRANSAKTIONSID,';
            $SQL .= '                    INSDATE';
            $SQL .= '                  )';
            $SQL .= '                  VALUES';
            $SQL .= '                  (';
            $SQL .= '                    vcom.FILNR,';
            $SQL .= '                    vcom.WANR,';
            $SQL .= '                    vcom.STATUS,';
            $SQL .= '                    vcom.DATUM,';
            $SQL .= '                    vcom.ZEIT,';
            $SQL .= '                    vcom.VERKAEUFERART,';
            $SQL .= '                    vcom.NAME,';
            $SQL .= '                    vcom.QUELLE,';
            $SQL .= '                    vcom.TRANSAKTIONSID,';
            $SQL .= '                    vcom.DATUMZEIT';
            $SQL .= '                  )';

            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VBE', true));

            $this->debugAusgabe("ENDE: IMPORT in Tabelle VERS_FORM_BEARBDATEN", 10);

            $this->debugAusgabe("START: IMPORT in Tabelle VERS_FORM_POSDATEN", 10);

            //Hier ist die Datenquelle die AX-Tabelle "AX__SALESLINE" aus dem DWH ODS-Schema
            //da in dieser Tabelle die Positionsdaten in der optimalen Form zur Verf�gung stehen.
            $SQL  ='MERGE INTO VERS_FORM_POSDATEN vp USING';
            $SQL .=' (SELECT vep.FILID,';
            $SQL .='   vep.WANR,';
            $SQL .='   vep.ATUPOSNUM,';
            $SQL .='   vep.ITEMID,';
            $SQL .='   vep.ATUUSABILITYCODEID,';
            $SQL .='   vep.NAME,';
            $SQL .='   vep.QTYORDERED,';
            $SQL .='   vep.SALESPRICE-LINEDISC,';
            $SQL .='   vep.LINEAMOUNT,';
            $SQL .='   vep.LINEDISC,';
            $SQL .='   vep.ATUSALESLINEDISCMAN,';
            $SQL .='   vep.ATUSALESLINEDISCEMPL,';
            $SQL .='   vep.ATUSALESLINEDISCAUTO,';
            $SQL .='   vep.SALESPRICE,';
            $SQL .='   vep.DWH$INSDATE';
            $SQL .=' FROM dwh.V_VERS_POS_SALESLINE@dwh vep';
            $SQL .=' INNER JOIN VERS_EXP_KOPF vek';
            $SQL .=' ON vep.FILID             = vek.FILID';
            $SQL .=' AND vep.WANR             = vek.WANR';
            $SQL .=' WHERE vep.DWH$INSDATE   > add_months(sysdate, -2)';
            $SQL .=' ) vep ON (vp.FILNR       = vep.FILID AND vp.WANR = vep.WANR)';
            $SQL .=' WHEN NOT MATCHED THEN';
            $SQL .='   INSERT';
            $SQL .='     (';
            $SQL .='       FILNR,';
            $SQL .='       WANR,';
            $SQL .='       POSITION,';
            $SQL .='       ARTNR,';
            $SQL .='       ARTKZ,';
            $SQL .='       BEZ,';
            $SQL .='       MENGE,';
            $SQL .='       VK_PREIS,';
            $SQL .='       UMSATZ,';
            $SQL .='       RABATTKENN,';
            $SQL .='       GES_RABATT,';
            $SQL .='       MAN_RABATT,';
            $SQL .='       PERS_RABATT,';
            $SQL .='       AUTO_RABATT,';
            $SQL .='       EINZELPREIS,';
            $SQL .='       INSDATE,';
            $SQL .='       QUELLE';
            $SQL .='     )';
            $SQL .='     VALUES';
            $SQL .='     (';
            $SQL .='       vep.FILID,';
            $SQL .='       vep.WANR,';
            $SQL .='       vep.ATUPOSNUM,';
            $SQL .='       vep.ITEMID,';
            $SQL .='       vep.ATUUSABILITYCODEID,';
            $SQL .='       vep.NAME,';
            $SQL .='       vep.QTYORDERED,';
            $SQL .='       vep.SALESPRICE-LINEDISC,';
            $SQL .='       vep.LINEAMOUNT,';
            $SQL .='       \'N\',';
            $SQL .='       vep.LINEDISC,';
            $SQL .='       vep.ATUSALESLINEDISCMAN,';
            $SQL .='       vep.ATUSALESLINEDISCEMPL,';
            $SQL .='       vep.ATUSALESLINEDISCAUTO,';
            $SQL .='       vep.SALESPRICE,';
            $SQL .='       vep.DWH$INSDATE,';
            $SQL .='       \'D\'';
            $SQL .='     )';


            $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VEP', true));
            $this->debugAusgabe("ENDE: IMPORT in Tabelle VERS_FORM_POSDATEN", 10);

            $this->_DB->TransaktionCommit();

            return true;
        }
        catch(Exception $ex)
        {
            $this->_DB->TransaktionRollback();

            $this->_errMsg = date('d.m.y H:i:s ') . ': Fehler beim Transaktionsload: ' . PHP_EOL;
            $this->_errMsg .= 'Es konnten keine Daten importiert werden. ' . PHP_EOL;

            if(strpos($ex->getMessage(), 'ORA-02396') !== false)
            {
                $this->_errMsg .= ' Im Moment kann keine Verbindung zum COM-Server aufgebaut werden.' . PHP_EOL;
                $this->_errMsg .= ' Bitte pruefen und Herrn Sischka Matthias kontaktieren.' . PHP_EOL;
            }

            $this->_errMsg .= 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
            $this->_errMsg .= 'Datei: /include/awisVersDatenAbholung.inc' . PHP_EOL;

            echo $this->_errMsg;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            return false;
        }
    }


    public function FullLoad()
    {
        //Der Full-Load darf nur uebers DWH laufen,
        //da ein Full-Load auf dem COM-Server eine zu hohe Auslastung bewirken wuerde
        $arrCOMViews = array();
        $arrCOMViews[0] = 'VERS_EXP_KOPF';
        $arrCOMViews[1] = 'VERS_FORM_BEARBDATEN';

        foreach($arrCOMViews as $Element)
        {
            $this->debugAusgabe("START: IMPORT in Tabelle " . $Element, 10);

            $SQL = 'TRUNCATE TABLE ' . $Element;

            $this->_DB->Ausfuehren($SQL, '', true);

            $SQL = "INSERT /*+ APPEND */ INTO " . $Element . ' ';
            $SQL .= ' SELECT *';
            $SQL .= ' FROM dwh.V_' . $Element . '@DWH';

            $this->_DB->Ausfuehren($SQL, '', true);

            $this->debugAusgabe("ENDE: IMPORT in Tabelle " .  -$Element, 10);
        }

        $this->debugAusgabe("START: IMPORT in Tabelle VERS_FORM_POSDATEN", 10);

        $SQL = 'TRUNCATE TABLE VERS_FORM_POSDATEN';
        $this->_DB->Ausfuehren($SQL, '', true);

        $SQL = "INSERT /*+ APPEND */ INTO VERS_FORM_POSDATEN ";
        $SQL .=' SELECT vpos.FILID           AS FILNR,';
        $SQL .='   vpos.WANR                 AS WANR,';
        $SQL .='   vpos.ATUPOSNUM            AS POSITION,';
        $SQL .='   vpos.ITEMID               AS ARTNR,';
        $SQL .='   vpos.ATUUSABILITYCODEID   AS ARTKZ,';
        $SQL .='   vpos.NAME                 AS BEZ,';
        $SQL .='   vpos.QTYORDERED           AS MENGE,';
        $SQL .='   vpos.SALESPRICE-LINEDISC  AS VK_PREIS,';
        $SQL .='   vpos.LINEAMOUNT           AS UMSATZ,';
        $SQL .='   \'N\'                AS RABATTKENN,';
        $SQL .='   vpos.LINEDISC             AS GES_RABATT,';
        $SQL .='   vpos.ATUSALESLINEDISCMAN  AS MAN_RABATT,';
        $SQL .='   vpos.ATUSALESLINEDISCEMPL AS PERS_RABATT,';
        $SQL .='   vpos.ATUSALESLINEDISCAUTO AS AUTO_RABATT,';
        $SQL .='   vpos.SALESPRICE           AS EINZELPREIS,';
        $SQL .='   vpos.DWH$INSDATE          AS INSDATE,';
        $SQL .='   \'D\'               AS QUELLE';
        $SQL .=' FROM dwh.V_VERS_POS_SALESLINE@dwh vpos';
        $SQL .=' INNER JOIN ODS.WA_VERS_KOPF@dwh vs';
        $SQL .='   ON vs.FIL_ID = vpos.FILID';
        $SQL .='   AND vs.VORGANG_NR = vpos.WANR';
        $SQL .=' WHERE vpos.DWH$INSDATE   > add_months(sysdate, -60)';

        $this->_DB->Ausfuehren($SQL, '', true);
        $this->debugAusgabe("ENDE: IMPORT in Tabelle VERS_FORM_POSDATEN", 10);
    }

    /**
     * @param string $Text  der Ausgegeben werden soll
     * @param number $Level Level welches Ben�tigt wird
     */
    public function debugAusgabe($Text = '', $Level = 500)
    {
        if($this->_DebugLevel >= $Level)
        {
            echo date('d.m.y H:i:s ') . $Text . PHP_EOL;
        }
    }

    /**
     * Prueft ob mit den Transaktionsnummern auch Datensaetze vom COM-Server abgeholt wurden.
     * Wenn das nicht der Fall ist wird eine Mail ueber die Mailwarteschlange an Shuttle versendet
     */
    public function pruefeLoads ()
    {
        try {
            $this->debugAusgabe("START: PRUEFE LOAD", 10);
            foreach ($this->_transNummern as $key => $transID) {
                $this->debugAusgabe("Gepruefte Transaktion: " . $transID, 10);

                $SQL = "SELECT count(*) AS anzahlDS";
                $SQL .= " FROM vers_exp_kopf";
                $SQL .= " WHERE TRANSAKTIONSID = " . $this->_DB->WertSetzen('TRANS', 'T', $transID);

                $rsTrans = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TRANS'));
                $this->debugAusgabe("Anzahl AWIS-Daten selektiert (VERS_EXP_KOPF)", 10);

                $SQL = "  SELECT count(*) AS anzahlDS";
                $SQL .= " FROM V_VERS_EXP_KOPF" . $this->_COM_Link;
                $SQL .= " WHERE TRANSAKTIONSID = " . $this->_DB->WertSetzen('TRANS_COM', 'T', $transID);

                $rsTransCOM = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('TRANS_COM'));
                $this->debugAusgabe("Anzahl COM-Server Daten selektiert (V_VERS_EXP_KOPF -> COM)", 10);

                if ($rsTrans->FeldInhalt('anzahlDS') != $rsTransCOM->FeldInhalt('anzahlDS')) {
                    $this->debugAusgabe("Anzahl COM und AWIS unterscheiden sich --> ERROR-MAIL", 10);

                    // Die Anzahl zwischen COM-Server und der AWIS Tabelle unterscheidet sich --> ERRORMAIL
                    $errMsg = 'Die Anzahl der Vorgänge unterscheidet sich zwischen AWIS und COM-Server. Bitte prüfen!!! ';
                    $errMsg .= 'Script: awisVersDatenAbholung.inc';
                    $this->_Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERS_TRANSAKTIONSLOAD', $this->_errMsg, 2, '', 'shuttle@de.atu.eu');
                }
            }

            $SQL = "SELECT count(*) AS anz";
            $SQL .= " FROM vers_exp_kopf LEFT JOIN versvorgangsstatus";
            $SQL .= " ON filid = vvs_filid";
            $SQL .= " AND wanr = vvs_wanr";
            $SQL .= " WHERE vvs_wanr is null";

            $rsKopf = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('KOPF'));
            $this->debugAusgabe("Anzahl AWIS-Daten selektiert (VERS_EXP_KOPF) -> Pruefung auf Schwellwert", 10);

            if (intval($rsKopf->FeldInhalt('ANZ')) < 50) {
                $this->debugAusgabe("Schwellwert wurde unterschritten --> ERRORMAIL", 10);

                // Die Anzahl der geladenen Vorgänge unterschreitet den Schwellwert von 50 --> ERRORMAIL
                $errMsg = 'Anzahl der Vorgänge vom COM-Server unterschreitet 50. Bitte prüfen ob ein Fehler vorliegt!!! ';
                $errMsg .= 'Script: awisVersDatenAbholung.inc';
                $this->_Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERS_TRANSAKTIONSLOAD', $errMsg, 2, '', 'shuttle@de.atu.eu');
            }
            $this->debugAusgabe("ENDE: PRUEFE LOAD", 10);
        }catch (Exception $ex) {
            echo 'ERROR: ' . $ex->getMessage();
        }

    }
}

?>