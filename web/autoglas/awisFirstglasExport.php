<?php


class FirstglasExport
{

private $_AWISBenutzer;
private $_DB;
private $_Form;
private $_Werkzeug;
private $_PfadStornoKasse = '/daten/daten/versicherungen/firstglas/Export/';
private $_DateinameStornoKasse;
private $_PfadKasseohneAuftraege='/daten/daten/versicherungen/firstglas/Export/';
private $_DateinameKasseohneAuftraege;
private $_PfadFreigabeRechnungen ='/daten/daten/versicherungen/firstglas/Export/';
private $_DateinameFreigabeRechnungen;


//Kassendatum

function __construct()
{
  $this->_AWISBenutzer = awisBenutzer::Init();
  $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
  $this->_DB->Oeffnen();
  $this->_Form = new awisFormular();
  $this->_Werkzeug = new awisWerkzeuge();
}


function ExportStornoKasse()
{

  /*
  $this->_DateinameStornoKasse = 'ExportStornoKasse.csv';

  $SQL =  "SELECT * FROM ( ";
  $SQL .= " SELECT FKA_AEMNR, FKA_FILID, FKA_BETRAG, FGK_KFZKENNZ, FKA_WANR, sum(FGP_ANZAHL*FGP_OEMPREIS)*1.19 as PosBetrag, FGP_VORGANGNR, FGK_FGN_KEY, FGK_ZEITSTEMPEL";
  $SQL .= " FROM FGKOPFDATEN INNER JOIN FGPOSITIONSDATEN ON FGK_KEY = FGP_FGK_KEY";
  $SQL .= " LEFT JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR";
  $SQL .= " GROUP BY FKA_AEMNR, FKA_FILID, FKA_BETRAG, FGK_KFZKENNZ, FKA_WANR,FGP_VORGANGNR, FGK_FGN_KEY, FGK_ZEITSTEMPEL)";
  $SQL .= " WHERE FGK_KFZKENNZ IN";
  $SQL .= " ( SELECT FGK_KFZKENNZ FROM FGKOPFDATEN INNER JOIN FGPOSITIONSDATEN ON FGK_KEY = FGP_FGK_KEY";
  $SQL .= " LEFT JOIN FKASSENDATEN ON FGK_VORGANGNR = FKA_AEMNR";
  $SQL .= " WHERE FGK_FGN_KEY = 2 )";
  $SQL .= " ORDER BY FGP_VORGANGNR";

  $rsZugang = $this->_DB->RecordSetOeffnen($SQL);

  if(($fd = fopen($this->_PfadStornoKasse.$this->_DateinameStornoKasse,'w')) === false)
  {
	 echo $this->error_msg = "Fehler beim Erstellen der Datei";
  }
  else
  {
    while(!$rsZugang->EOF())
    {
       $Line = array($rsZugang->FeldInhalt('FKA_AEMNR').';'.$rsZugang->FeldInhalt('FKA_FILID').';'.$rsZugang->FeldInhalt('FKA_BETRAG').';'.$rsZugang->FeldInhalt('FGK_KFZKENNZ').';'.$rsZugang->FeldInhalt('FKA_WANR').';'.$rsZugang->FeldInhalt('POSBETRAG')
       .';'.$rsZugang->FeldInhalt('FGP_VORGANGNR').';'.$rsZugang->FeldInhalt('FGK_FGN_KEY').';'.$rsZugang->FeldInhalt('FGK_ZEITSTEMPEL').chr(13).chr(10));

       foreach($Line as $Zeile)
       {
          fwrite($fd,$Zeile);
       }
        


    $rsZugang->DSWeiter();
    }
    fclose($fd);
  }
  */

//----------------------------------------------------------------------------
//----------------------Aufruf --> (PDF)--------------------------------------
//----------------------------------------------------------------------------





















}//Ende Export



function ExportKassendatenohneAuftraege()
{

  $this->_DateinameKasseohneAuftraege = 'ExportKassendatenohneAuftraege.csv';

  $SQL  = 'Select FKASSENDATEN.*,FGK_VORGANGNR from FKASSENDATEN ';
  $SQL .= ' LEFT JOIN FGKOPFDATEN ON FKA_AEMNR = FGK_VORGANGNR';
  $SQL .= ' WHERE FGK_VORGANGNR IS NULL';

  $rsZugang = $this->_DB->RecordSetOeffnen($SQL);

  echo $this->_PfadKasseohneAuftraege.$this->_DateinameKasseohneAuftraege;

  if(($fd = fopen($this->_PfadKasseohneAuftraege.$this->_DateinameKasseohneAuftraege,'w')) === false)
  {
	 echo $this->error_msg = "Fehler beim Erstellen der Datei";
  }
  else
  {
    while(!$rsZugang->EOF())
    {
       $Line = array($rsZugang->FeldInhalt('FKA_KENNUNG').';'.$rsZugang->FeldInhalt('FKA_FILID').';'.$rsZugang->FeldInhalt('FKA_DATUM').';'.$rsZugang->FeldInhalt('FKA_UHRZEIT').';'.$rsZugang->FeldInhalt('FKA_BSA').';'.$rsZugang->FeldInhalt('FKA_WANR')
       .';'.$rsZugang->FeldInhalt('FKA_AEMNR').';'.$rsZugang->FeldInhalt('FKA_ATUNR').';'.$rsZugang->FeldInhalt('FKA_MENGE').';'.$rsZugang->FeldInhalt('FKA_BETRAG').';'.$rsZugang->FeldInhalt('FKA_KFZKENNZ').';'.$rsZugang->FeldInhalt('FKA_BEMERKUNG').';'.$rsZugang->FeldInhalt('FKA_IMP_KEY').';'.$rsZugang->FeldInhalt('FKA_STATUS').chr(13).chr(10));

       foreach($Line as $Zeile)
       {
          //echo $Zeile;
          fwrite($fd,$Zeile);
       }

        $rsZugang->DSWeiter();
    }
    fclose($fd);
  }

}//Ende Export


function ExportFreigabeRechnungen()
{

  $this->_DateinameFreigabeRechnungen = 'ExportFreigabeRechnungen.csv';

  $SQL = 'Select * from FGFREIGABERECHNUNGEN';

    $rsZugang = $this->_DB->RecordSetOeffnen($SQL);

  //echo $this->_PfadKasseohneAuftraege.$this->_DateinameKasseohneAuftraege;

  if(($fd = fopen($this->_PfadFreigabeRechnungen.$this->_DateinameFreigabeRechnungen,'w')) === false)
  {
	 echo $this->error_msg = "Fehler beim Erstellen der Datei";
  }
  else
  {
    while(!$rsZugang->EOF())
    {
       $Line = array($rsZugang->FeldInhalt('FGR_VORGANGNR').';'.$rsZugang->FeldInhalt('FGR_KASSIERDATUM').chr(13).chr(10));

       foreach($Line as $Zeile)
       {
          fwrite($fd,$Zeile);
       }

        $rsZugang->DSWeiter();
    }
    fclose($fd);
  }

}//Ende Export























}//Ende Klasse FirstglasExport

?>
