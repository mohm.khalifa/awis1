#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Datenexporte
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
#Target=_self
ErrorMail=sacha.kerres@de.atu.eu
Sprachen=DE,FR,CZ,NL,IT

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.02.00;Exporte wurden geaendert auf direkten Download;08.07.2008;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>
0.01.00;Erste Testversion;07.01.2008;<a href=mailto:sacha.kerres@de.atu.eu>Sacha Kerres</a>

