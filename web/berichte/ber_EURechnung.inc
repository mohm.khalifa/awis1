<?php
require_once 'awisBerichte.inc';
require_once '../eurechnungen/eurechnungen_funktionen.php';

class ber_EURechnung
    extends awisBerichte
{

    private $PosVorhanden = true;
    private $MaxSeitenAnzahl = 0;
    private $_EU;
    private $_rsRechnung;
    private $_rsPositionen;
    private $_Gesamtbetrag=0;
    public $_AWIS_KEY1 = '';

    /**
     * Prozedur zum Initialisieren des Berichts
     *
     * @see awisBerichte::init()
     */
    public function SetzeDateipfad($DateiPfad)
    {
        $this->_Dateiname = $DateiPfad;
    }

    public function init(array $Parameter)
    {
        global $AWIS_KEY1;
        $this->_AWIS_KEY1 = $_GET['ERK_ID'];
        $AWIS_KEY1 = $this->_AWIS_KEY1;

        $TextKonserven[] = array('Fehler', 'err_KeineDaten');
        $TextKonserven[] = array('Liste', 'lst_ALLE_0');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Soll');
        $TextKonserven[] = array('Wort', 'Anschrift_ATU_Filiale');
        $TextKonserven[] = array('Wort', 'PersoenlHaftGesell_ATU_Filiale');
        $TextKonserven[] = array('Wort', 'Geschaeftsfuehrer_ATU_Filiale');
        $TextKonserven[] = array('Wort', 'Bankverb_ATU_Filiale');
        $TextKonserven[] = array('FGK', 'txt_BruttoOEM');
        $TextKonserven[] = array('SYSTEM', 'PROGNAME');
        $TextKonserven[] = array('Ausdruck', 'txt_HinweisAusdruckIntern');

        $TextKonserven[] = array('ERE', '*');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
        $this->_EU = new eurechnungen_funktionen();

        //Daten beschaffen
        $this->_EU->BedingungErstellen(); //Setzt $AWIS_KEY1
        $this->_rsRechnung = $this->_EU->rsERE(eurechnungen_funktionen::ReturnRecordset);
        $this->_rsPositionen = $this->_EU->rsERP(eurechnungen_funktionen::ReturnRecordset);

    }


    /**
     * Eigentliches PDF erzeugen
     *
     * @see awisBerichte::ErzeugePDF()
     */
    public function ErzeugePDF($Ausgabeart = 1)
    {
        // PDF Attribute setzen
        $this->SetTitle($this->_BerichtsName);
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins($this->_Parameter['LinkerRand'], 50, $this->_Parameter['LinkerRand']);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(true, 50);

        $this->NeueSeite();
        $this->Rechnungskopf();
        $this->Adressdaten();
        $this->Kundendaten();
        $this->Fahrzeugdaten();

        $this->PosUeberschrift();
        $this->Positionsdaten();
        $this->Betragsblock();
       $this->HinweisBlock();

        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zur�ckliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:         // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
        //Immer zusaetzlich auch in Dokuware ablegen!
        $this->Output('/win/docuware/EU_Rechnungen/' . $this->_AWIS_KEY1 . '_' . date('YmdHis') . '.pdf','F');

    }

    private function Rechnungskopf()
    {
        $this->_Zeile = $this->_Parameter['YPosRechnungsKopf'];
        $this->_Spalte = $this->_Parameter['LinkerRand'] ;

        //Rechnungsnummer
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERE_RECHNUNGSNUMMER'] . ':', 0, 0, 'L', 0);

        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsRechnung->FeldInhalt('ERK_ID'), 0, 0, 'L', 0);



        //Liefer-/Rechnungsdatum
        $this->_Spalte = $this->_Parameter['LinkerRand'] ;
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERE_RECHNUNGSDATUM'] . ':', 0, 0, 'L', 0);

        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], substr($this->_rsRechnung->FeldInhalt('ERK_DATUM'), 0, 10), 0, 0, 'L', 0);

        //Filiale
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERK_PDF_FILIALE'] . ':', 0, 0, 'L', 0);

        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 40;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsRechnung->FeldInhalt('ERK_FIL_ID') . ' - ' . $this->_rsRechnung->FeldInhalt('FIL_BEZ'), 0, 0, 'L', 0);



        $this->_Zeile += 5;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $Zellenbreite = 30;
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsRechnung->FeldInhalt('NAME'), 0, 0, 'L', 0);
    }

    private function Adressdaten()
    {
        $this->_Zeile = $this->_Parameter['YPosKastenMitte'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $Zellenbreite = 82;

        //Anschrift ATU
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $Anschrift = $this->_Form->LadeTextBaustein('PDF','PDF_ANSCHRIFT_ATU',$this->_rsRechnung->FeldInhalt('LAN_CODE_FIL'));

        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $Anschrift, 0, 0, 'L', 0);

        //Anschrift des Kunden
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseHervorgehoben']);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] + 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsRechnung->FeldInhalt('ANR_ANREDE'), 0, 0, 'L', 0);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsRechnung->FeldInhalt('EKU_NAME'), 0, 0, 'L', 0);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_rsRechnung->FeldInhalt('EKU_STRASSE'), 0, 0, 'L', 0);

        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'],
            $this->_rsRechnung->FeldInhalt('LAN_CODE') . ' - ' . $this->_rsRechnung->FeldInhalt('EKU_PLZ') . ' ' . $this->_rsRechnung->FeldInhalt('EKU_ORT'), 0, 0, 'L', 0);


        $this->_Zeile += $this->_Parameter['Zeilenhoehe']*2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['EKU_USTIDNR']. ': ' .$this->_rsRechnung->FeldInhalt('EKU_USTIDNR'), 0, 0, 'L', 0);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'],$this->_AWISSprachkonserven['ERE']['ERE_PDF_INNERGEMEINSCHAFT'], 0, 0, 'L', 0);



    }

    private function Kundendaten()
    {
        $this->_Zeile = $this->_Parameter['YPosKastenMitte'];
        $this->_Spalte = $this->_Parameter['LinkerRand'] + $this->_Parameter['XPosKastenSpalte2'];
    }

    private function Fahrzeugdaten()
    {
        $this->_Zeile = $this->_Parameter['YPosFZ-Daten'];
        $this->_Spalte = $this->_Parameter['LinkerRand'] + 3;
        $Zellenbreite = 15;

        $this->_Spalte += $Zellenbreite;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(50, $this->_Parameter['Zeilenhoehe'], $this->_rsRechnung->FeldInhalt('FAHRGESTELLNR'), 0, 0, 'L', 0);
    }

    private function PosUeberschrift()
    {
        if ($this->PageNo() == 1) {
            $this->_Zeile = $this->_Parameter['YPosPositionsdaten'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
        } else {
            $this->_Zeile = 40;
            $this->_Spalte = $this->_Parameter['LinkerRand'];
        }

        $Zellenbreite = self::Seitenbreite() - (2 * $this->_Parameter['LinkerRand']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($Zellenbreite, $this->_Parameter['Zeilenhoehe'], '', 1, 0, 'C', 0);

        //�berschrift Positionen

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreitePos'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERP_POSITION'], 0, 0, 'C', 0);

        $this->_Spalte += $this->_Parameter['BreitePos'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteATU-Nr'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERP_AST_ATUNR'], 0, 0, 'L', 0);

        $this->_Spalte += $this->_Parameter['BreiteATU-Nr'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteTeilebezeichnung'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERP_PDF_BEZEICHNUNG'], 0, 0, 'L', 0);

        $this->_Spalte += $this->_Parameter['BreiteTeilebezeichnung'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteMenge'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERP_MENGE'], 0, 0, 'R', 0);

        $this->_Spalte += $this->_Parameter['BreiteMenge'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteVKPreis'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERP_PDF_VKNETTO'], 0, 0, 'R', 0);

        $this->_Spalte += $this->_Parameter['BreiteVKPreis'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell($this->_Parameter['BreiteBetragEUR'], $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERP_BETRAG'], 0, 0, 'R', 0);
    }

    //Positionsdaten
    private function Positionsdaten()
    {
        $ParkschadenPos = false;

        while (!$this->_rsPositionen->EOF()) {

            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreitePos'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->DSNummer()+1, 0, 0, 'C', 0);

            $this->_Spalte += $this->_Parameter['BreitePos'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteATU-Nr'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->FeldInhalt('ERP_AST_ATUNR'), 0, 0, 'L', 0);

            $this->_Spalte += $this->_Parameter['BreiteATU-Nr'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteTeilebezeichnung'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->FeldInhalt('ERP_ZUSATZTEXT'), 0, 0, 'L', 0);

            $this->_Spalte += $this->_Parameter['BreiteTeilebezeichnung'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteMenge'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->FeldInhalt('ERP_MENGE'), 0, 0, 'R', 0);

            $this->_Spalte += $this->_Parameter['BreiteMenge'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['BreiteVKPreis'], $this->_Parameter['Zeilenhoehe'], $this->_rsPositionen->FeldInhalt('ERP_VK_NETTO','N2'), 0, 0, 'R', 0);

            $this->_Spalte += $this->_Parameter['BreiteVKPreis'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $Betrag = $this->_Form->Format('N2',str_replace(',', '.', $this->_rsPositionen->FeldInhalt('ERP_MENGE', 'N2')) * str_replace(',', '.', $this->_rsPositionen->FeldInhalt('ERP_VK_NETTO', 'N2')));

            $this->_Gesamtbetrag += str_replace(',', '.', $this->_rsPositionen->FeldInhalt('ERP_MENGE', 'N2')) * str_replace(',', '.', $this->_rsPositionen->FeldInhalt('ERP_VK_NETTO', 'N2'));
            $this->Cell($this->_Parameter['BreiteBetragEUR'], $this->_Parameter['Zeilenhoehe'],
                str_replace('.',',',$Betrag), 0, 0, 'R', 0);

            $this->_rsPositionen->DSWeiter();
            $this->_Zeile = $this->GetY();
            $this->SetXY($this->_Spalte, $this->_Zeile);
        }
        $this->PosVorhanden = false;
    }

    private function Betragsblock()
    {
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseMittel']);
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;

        $this->SetXY($this->_Spalte, $this->_Zeile);


        //GESAMTZahlbetrag NETTO
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetFillColor(209, 209, 209);
        $this->_Spalte = $this->_Parameter['LinkerRand'] + (self::Seitenbreite() - ($this->_Parameter['LinkerRand'] * 2) - $this->_Parameter['BreiteZahlbetraege']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(28, $this->_Parameter['Zeilenhoehe'] + 2, $this->_AWISSprachkonserven['ERE']['ERP_BETRAG'], '', 0, 'L', true);

        $this->SetFillColor(209, 209, 209);
        $this->_Spalte = $this->_Parameter['LinkerRand'] + (self::Seitenbreite() - ($this->_Parameter['LinkerRand'] * 2) - ($this->_Parameter['BreiteZahlbetraege'] - 28));
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $BruttoBetrag = str_replace(',', '.', $this->_rsRechnung->FeldInhalt('VKD_PREIS_BRUTTO'));
        $this->Cell(22, $this->_Parameter['Zeilenhoehe'] + 2, $this->_Form->Format('N2', $this->_Gesamtbetrag), '', 0, 'R', true);
    }

    private function HinweisBlock()
    {
        $this->_Zeile += $this->_Parameter['Zeilenhoehe'] * 2;
        $this->_Spalte = $this->_Parameter['LinkerRand'];

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(80, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['ERE']['ERP_PDF_GUTEFAHRT'], 0, 0, 'L', 0);


    }


    /**
     * NeueSeite()
     * Erstellt eine neue PDF Seite
     *
     * @return int
     */
    private function NeueSeite()
    {
        $this->AddPage();
    }

    /**
     * Automatische Fu�zeile
     *
     * @see TCPDF::Footer()
     */
    public function Footer()
    {
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->_Zeile = $this->_Parameter['YPosFussZeile'];

        $BreiteFussZeile = (self::Seitenbreite() - ($this->_Parameter['LinkerRand'] * 2)) / 3;

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKlein']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_Form->LadeTextBaustein('Wort', 'Anschrift_ATU_Filiale',$this->_rsRechnung->FeldInhalt('LAN_CODE_FIL')), 0, 'L', 0);

        $this->_Spalte += $BreiteFussZeile;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_Form->LadeTextBaustein('Wort', 'PersoenlHaftGesell_ATU_Filiale',$this->_rsRechnung->FeldInhalt('LAN_CODE_FIL')) , 0, 'L', 0);
        $this->_Zeile = $this->GetY() + 2;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        if($this->_rsRechnung->FeldInhalt('LAN_CODE_FIL')!='AT'){ //In AT Braucht man keine Gesch�ftsf�hrer
            $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_AWISSprachkonserven['Wort']['Geschaeftsfuehrer_ATU_Filiale'], 0, 'L', 0);
        }


        $this->_Spalte += $BreiteFussZeile;
        $this->_Zeile = $this->_Parameter['YPosFussZeile'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->MultiCell($BreiteFussZeile, $this->_Parameter['AbstandZeile'], $this->_Form->LadeTextBaustein('Wort', 'Bankverb_ATU_Filiale',$this->_rsRechnung->FeldInhalt('LAN_CODE_FIL'))  , 0, 'L', 0);
    }


    /**
     * Automatische Kopfzeile
     *
     * @see TCPDF::Header()
     */
    public function Header()
    {
        $pdf_vorlage = $this->ImportPage(1);        // Erste oder zweite Seite importieren
        $this->useTemplate($pdf_vorlage);

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['Schriftgroesse']+20);
        $this->_Zeile = 15;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(80, $this->_Parameter['Zeilenhoehe'], 'Rechnung', 0, 0, 'L', 0);

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
        $this->_Zeile = 30;
        $this->_Spalte = $this->_Parameter['LinkerRand'] + 160;

        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(80, $this->_Parameter['Zeilenhoehe'], 'Seite: ' . $this->PageNo() . ' von {nb}', 0, 0, 'L', 0);

        if ($this->PosVorhanden == true) {
            $this->PosUeberschrift();
            $this->_Zeile += $this->_Parameter['Zeilenhoehe'];
        } else {
            $this->_Zeile = 40;
        }

        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->MaxSeitenAnzahl++;
    }
}

?>