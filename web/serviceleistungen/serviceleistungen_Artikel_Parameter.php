<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SAA','%');
	$TextKonserven[]=array('Liste','SAA_%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Wort','EANPruefung%');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19001 = $AWISBenutzer->HatDasRecht(19001);
	if(($Recht19001)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$SAAParam = unserialize($AWISBenutzer->ParameterLesen('Formular_ServiceArtikelAngaben'));
	if(!isset($SAAParam['BLOCK']))
	{
		$SAAParam['BLOCK']=1;
		$SAAParam['ORDERBY']='SAA_BEZEICHNUNG';
		$SAAParam['KEY']='';
		$SAAParam['SAAKEY']=$AWIS_KEY1;
	}

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY SAA_BEZEICHNUNG';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}
	
	$SQL = 'SELECT SVCARTIKELANGABEN.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM SVCARTIKELANGABEN ';
	$SQL .= ' WHERE SAA_SVA_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['SAA_KEY']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['SAA_KEY']);
	}
	elseif($SAAParam['KEY']!='' AND !isset($_GET['SListe']))
	{
		$AWIS_KEY2 = ($SAAParam['SAAKEY']==$AWIS_KEY1?$SAAParam['KEY']:0);
	}

	// Um zu wissen, ob es gleichen Artikel ist
	$SAAParam['SAAKEY']=$AWIS_KEY1;
	
	if($AWIS_KEY2<>0)
	{
		$SQL .= ' AND SAA_KEY = 0'.$AWIS_KEY2;
		$_GET['SAA_KEY']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}


	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;
	$rsSAA = $DB->RecordsetOeffnen($SQL);

    $Form->Formular_Start();

	if($rsSAA->AnzahlDatensaetze()>1 OR isset($_GET['SListe']) OR !isset($_GET['SAA_KEY']))
	{
		$Form->ZeileStart();
		$Icons=array();
		if((intval($Recht19001)&6)!=0)
		{
			$Icons[] = array('new','./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Parameter&SVA_KEY='.$AWIS_KEY1.'&SAA_KEY=-1');
  		}

  		$Form->Erstelle_ListeIcons($Icons,38,-1);

  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SAA_BEZEICHNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SAA_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAA']['SAA_BEZEICHNUNG'],350,'',$Link);
		
  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SAA_KENNUNGFELD'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SAA_KENNUNGFELD'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAA']['SAA_KENNUNGFELD'],250,'',$Link);
		
  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SAA_KENNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SAA_KENNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAA']['SAA_KENNUNG'],150,'',$Link);
		
  		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=SAA_INFOFELD'.((isset($_GET['SSort']) AND ($_GET['SSort']=='SAA_INFOFELD'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SAA']['SAA_INFOFELD'],150,'',$Link);
		
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$SAAParam['KEY']='';
		
		$DS=0;
		while(!$rsSAA->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht19001&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Parameter&SVA_KEY='.$AWIS_KEY1.'&SAA_KEY='.$rsSAA->FeldInhalt('SAA_KEY'));
			}
			if(intval($Recht19001&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Parameter&SVA_KEY='.$AWIS_KEY1.'&Del='.$rsSAA->FeldInhalt('SAA_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('*SAA_BEZEICHNUNG',$rsSAA->FeldInhalt('SAA_BEZEICHNUNG'),20,350,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*SAA_KENNUNGFELD',$rsSAA->FeldInhalt('SAA_KENNUNGFELD'),20,250,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*SAA_KENNUNG',$rsSAA->FeldInhalt('SAA_KENNUNG'),20,250,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*SAA_INFOFELD',$rsSAA->FeldInhalt('SAA_INFOFELD'),20,150,false,($DS%2),'','','T');
			$Form->ZeileEnde();

			$rsSAA->DSWeiter();
			$DS++;
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Historie';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsSAA->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht19001&6);
		$AWIS_KEY2 = $rsSAA->FeldInhalt('SAA_KEY');
		$SAAParam['KEY']=$AWIS_KEY2;
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
		
		$Form->Erstelle_HiddenFeld('SAA_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('SAA_SVA_KEY',$AWIS_KEY1);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Artikel&Seite=Parameter&SListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSAA->FeldInhalt('SAA_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSAA->FeldInhalt('SAA_USERDAT'));
		$Form->InfoZeile($Felder,'');
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('!SAA_BEZEICHNUNG',$rsSAA->FeldInhalt('SAA_BEZEICHNUNG'),50,500,$EditModus,'','','','T');
		if($EditModus)
		{
			$AWISCursorPosition='txtSAA_BEZEICHNUNG';
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_TYP'].':',200);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SAA_TYP']);
		$Form->Erstelle_SelectFeld('!SAA_TYP',$rsSAA->FeldInhalt('SAA_TYP'),"200:190",$EditModus,'',$OptionBitteWaehlen,'A','','',$ListenDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_KENNUNG'].':',200);
		$Form->Erstelle_TextFeld('!SAA_KENNUNG',$rsSAA->FeldInhalt('SAA_KENNUNG'),80,100,$EditModus,'','','','T');
		$Form->ZeileEnde();
		
		$ImportSpalten = 'SELECT COLUMN_NAME AS KEY, COLUMN_NAME AS Anzeige';
		$ImportSpalten .= ' FROM sys.all_tab_columns';
		$ImportSpalten .= ' WHERE TABLE_NAME = \'V_SERVICELEISTUNGEN_QUELLE\'';
		$ImportSpalten .= ' ORDER BY 1';
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_KENNUNGFELD'].':',200);
		//$Form->Erstelle_TextFeld('!SAA_KENNUNGFELD',$rsSAA->FeldInhalt('SAA_KENNUNGFELD'),80,100,$EditModus,'','','','T');
		$Form->Erstelle_SelectFeld('!SAA_KENNUNGFELD',$rsSAA->FeldInhalt('SAA_KENNUNGFELD'),"200:190",$EditModus,$ImportSpalten,'','');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_INFOFELD'].':',200);
		//$Form->Erstelle_TextFeld('!SAA_INFOFELD',$rsSAA->FeldInhalt('SAA_INFOFELD'),80,100,$EditModus,'','','','T');
		$Form->Erstelle_SelectFeld('!SAA_INFOFELD',$rsSAA->FeldInhalt('SAA_INFOFELD'),"200:190",$EditModus,$ImportSpalten,'','');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_POSVON'].':',200);
		$Form->Erstelle_TextFeld('SAA_POSVON',$rsSAA->FeldInhalt('SAA_POSVON'),10,200,$EditModus,'','','','N0');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_LAENGE'].':',200);
		$Form->Erstelle_TextFeld('SAA_LAENGE',$rsSAA->FeldInhalt('SAA_LAENGE'),10,100,$EditModus,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_FELDTRENNER'].':',200);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SAA_FELDTRENNER']);
		$Form->Erstelle_SelectFeld('!FELDTRENNER',$rsSAA->FeldInhalt('SAA_FELDTRENNER'),"200:190",$EditModus,'','','A','','',$ListenDaten);
		//$Form->Erstelle_TextFeld('FELDTRENNER',$rsSAA->FeldInhalt('SAA_FELDTRENNER'),10,100,$EditModus,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SAA']['SAA_FELDNUMMER'].':',200);
		$Form->Erstelle_TextFeld('SAA_FELDNUMMER',$rsSAA->FeldInhalt('SAA_FELDNUMMER'),10,100,$EditModus,'','','','N0');
		$Form->ZeileEnde();
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}
	$AWISBenutzer->ParameterSchreiben('Formular_ServiceArtikelAngaben',serialize($SAAParam));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202190830");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202190831");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>