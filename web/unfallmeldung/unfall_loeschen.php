<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$Tabelle= '';

if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
{
	$Tabelle = 'UNF';
	$Key=$_POST['txtUNF_KEY'];
	
	$AWIS_KEY1 = $Key;
	
	$Felder=array();
	$Felder[]=array($Form->LadeTextBaustein('UNF','UNF_NAME'),$_POST['txtUNF_NAME']);
	$Felder[]=array($Form->LadeTextBaustein('UNF','UNF_VORNAME'),$_POST['txtUNF_VORNAME']);
	$Felder[]=array($Form->LadeTextBaustein('UNF','UNF_KEY'),$_POST['txtUNF_KEY']);
}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
{
	$SQL = '';
	switch ($_POST['txtTabelle'])
	{
		case 'UNF':
			$SQL = 'DELETE FROM UNFALLMELDUNG WHERE unf_key=0'.$_POST['txtKey'];
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
			}
			
			$SQL = 'DELETE FROM UNFALLMELDUNGINFOS WHERE ufi_unf_key=0'.$_POST['txtKey'];
			$AWIS_KEY1=0;
				
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('seminarplan_loeschen_1',1,$awisDBError['messages'],'');
			}
				
			break;
	}
}

if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

	$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./unfall_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

	$Form->Formular_Start();
	
	$Form->ZeileStart();		
	$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	
	$Form->ZeileEnde();

	foreach($Felder AS $Feld)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($Feld[0].':',200);
		$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
		$Form->ZeileEnde();
	}

	$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
	$Form->Erstelle_HiddenFeld('Key',$Key);

	$Form->Trennzeile();

	$Form->ZeileStart();
	$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
	$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
	$Form->ZeileEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->Formular_Ende();

	die();
}

?>