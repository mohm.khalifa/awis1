<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

try 
{
    require_once('awisDatenbank.inc');
    require_once('awisFormular.inc');
    
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('AAP','%');
    $TextKonserven[]=array('ADR','%');
    $TextKonserven[]=array('ADI','ADI_%');
    $TextKonserven[]=array('ADT','ADT_%');
	$TextKonserven[]=array('MBW','txtAnredenListe');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht3000 = $AWISBenutzer->HatDasRecht(3000);
	if($Recht3000==0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}
	
	// F�r die Suchfunktionen
	$INET_LINK_Telefon = $AWISBenutzer->ParameterLesen('INET_SUCHE_Telefon');
	$INET_LINK_Karte = $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');

	$Param = unserialize($AWISBenutzer->ParameterLesen('AdressenSuche'));
	
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['NAME'] = $_POST['sucSuchName'];
		$Param['KONTAKT'] = $_POST['sucSuchKontakt'];
		$Param['ADR_AZG_KEY'] = $_POST['sucADR_AZG_KEY'];
		$Param['ADR_ORT'] = $_POST['sucADR_ORT'];
		$Param['ADR_BEMERKUNG'] = $_POST['sucADR_BEMERKUNG'];
		
		// Allgemeine Parameter
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';	
	}
	elseif(isset($_POST['cmdDSZurueck_x']))
	{
	    $AWIS_KEY1 = $_POST['txtADR_KEY'];
		$SQL = 'SELECT ADR_KEY FROM (SELECT ADR_KEY FROM Adressen';
		$SQL .= ' WHERE ADR_KEY < :var_N0_ADR_KEY';
		$SQL .= ' ORDER BY ADR_NAME1 DESC, ADR_NAME2 DESC';
		$SQL .= ') WHERE ROWNUM = 1';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_ADR_KEY'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsFelder = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
		$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);
		if(!$rsFelder->EOF())
		{
		    $AWIS_KEY1=$rsFelder->FeldInhalt('ADR_KEY');
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
	    $AWIS_KEY1 = $_POST['txtADR_KEY'];
		$SQL = 'SELECT ADR_KEY FROM (SELECT ADR_KEY FROM Adressen';
		$SQL .= ' WHERE ADR_KEY > :var_N0_ADR_KEY';
		$SQL .= ' ORDER BY ADR_NAME1 ASC, ADR_NAME2 ASC';
		$SQL .= ') WHERE ROWNUM = 1';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_ADR_KEY'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsFelder = $DB->RecordSetOeffnen($SQL,$BindeVariablen);

		if(!$rsFelder->EOF())
		{
		    $AWIS_KEY1=$rsFelder->FeldInhalt('ADR_KEY');
		}
		$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./adressen_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
    	include('./adressen_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['ADR_KEY']))
	{
	    $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ADR_KEY']);
	}
	elseif(isset($_POST['txtADR_KEY']))
	{
	    $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtADR_KEY']);
	}
	else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
	{    
	    if(!isset($Param['KEY']))
	    {
	        $Param['KEY']='';
	        $Param['WHERE']='';
	        $Param['ORDER']='';
	    }
	    
	    if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
	    {
	        $Param['KEY']=0;
	    }
	    $AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
	    if($Param['ORDER']!='')
	    {
	        $ORDERBY = 'ORDER BY '.$Param['ORDER'];
	    }
	    else
	    {
	        $ORDERBY = ' ORDER BY ADR_Name1, ADR_Name2 DESC';
	        $Param['ORDER']='ADR_Name1, ADR_Name2 DESC';
	    }
	}
	else
	{
	    $Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
	    $ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}	
	
	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	$SQL = 'SELECT DISTINCT Adressen.*';
	$SQL .= ' FROM Adressen';
	$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADR_AZG_KEY = AZG_KEY ';
	$SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON (AZM_AZG_KEY=AZG_KEY OR AZG_KEY=1)';
	$SQL .= ' WHERE (AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
	$SQL .= ' OR AZG_KEY = 1)';		// oder �ffentlich!
	
	if($Bedingung!='')
	{
	    $SQL .= ' AND ' . substr($Bedingung,4);
	}
	
	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsFelder = $DB->RecordSetOeffnen($SQL);
	$rsADRZeilen = $rsFelder->AnzahlDatensaetze();
	
	$DetailAnsicht = false;
	//********************************************************
	// Daten anzeigen
	//********************************************************
	if($rsADRZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsADRZeilen>1)						// Liste anzeigen
	{
		$DetailAnsicht = false;
		
		$Form->Formular_Start();
		$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./adressen_Main.php?cmdAktion=Details>");
		$Form->ZeileStart();
		$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADR_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADR']['ADR_NAME1'].'/'.$AWISSprachKonserven['ADR']['ADR_NAME2'],450,'',$Link);
		$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADR_STRASSE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_STRASSE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADR']['ADR_STRASSE'],250);
		$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADR_PLZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_PLZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADR']['ADR_PLZ'],100,'',$Link);
		$Link = './adressen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ADR_ORT'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADR_ORT'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADR']['ADR_ORT'],200,'',$Link);
		$Form->ZeileEnde();
	
		// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}
	
		// Seitenweises bl�ttern
		if($rsADRZeilen>$MaxDSAnzahl)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'].':',50,'');
	
			for($i=0;$i<($rsADRZeilen/$MaxDSAnzahl);$i++)
			{
				if($i!=($StartZeile/$MaxDSAnzahl))
				{
					$Text = '&nbsp;<a href=./adressen_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
					$Form->Erstelle_TextLabel($Text,30,'');
				}
				else
				{
					$Text = '&nbsp;'.($i+1).'';
					$Form->Erstelle_TextLabel($Text,30,'');
				}
			}
			$Form->ZeileEnde();
		}
	
		for($ADRZeile=$StartZeile;$ADRZeile<$rsADRZeilen and $ADRZeile<$StartZeile+$MaxDSAnzahl;$ADRZeile++)
		{
			$Form->ZeileStart();
			$Link = './adressen_Main.php?cmdAktion=Details&ADR_KEY='.$rsFelder->FeldInhalt('ADR_KEY');
			$Form->Erstelle_ListenFeld('ADR_NAME1',$rsFelder->FeldInhalt('ADR_NAME1'). ' '.$rsFelder->FeldInhalt('ADR_NAME2'),0,450,false,($ADRZeile%2),'',$Link);
			$Form->Erstelle_ListenFeld('ADR_STRASSE',$rsFelder->FeldInhalt('ADR_STRASSE'). ' '.$rsFelder->FeldInhalt('ADR_HAUSNUMMER'),0,250,false,($ADRZeile%2),'','');
			$Form->Erstelle_ListenFeld('ADR_PLZ',$rsFelder->FeldInhalt('ADR_LAN_CODE'). '-'.$rsFelder->FeldInhalt('ADR_PLZ'),0,100,false,($ADRZeile%2),'','');
			$Form->Erstelle_ListenFeld('ADR_ORT',$rsFelder->FeldInhalt('ADR_ORT'),0,200,false,($ADRZeile%2),'','');
			$Form->ZeileEnde();
			
			$rsFelder->DSWeiter();
		}
	
		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		echo '<form name=frmAdressen action=./adressen_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">';
		$DetailAnsicht = true;
		
		$AWIS_KEY1 = $rsFelder->FeldInhalt('ADR_KEY')!= ''?$rsFelder->FeldInhalt('ADR_KEY'):0;
		$Param['KEY']=$AWIS_KEY1;
		
		echo '<input type=hidden name=txtADR_KEY value='.$AWIS_KEY1. '>';
	
		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_USER')));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_USERDAT')));
		$Form->InfoZeile($Felder,'');
		
		$EditRecht=(($Recht3000&2)!=0);
	
		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_ANREDE'].':',150);
		$AnredenText = explode("|",$AWISSprachKonserven['ADR']['txtAnredeListe']);
		$Form->Erstelle_SelectFeld('ADR_ANREDE',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_ANREDE')),100,$EditRecht,'','','3','','',$AnredenText,'');
		
		if($EditRecht)
		{
			$CursorFeld='txtADR_ANREDE';
		}
	
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_STATUS'].':',70);
		$KateogrieAlt = explode("|",$AWISSprachKonserven['ADR']['lst_ADR_STATUS']);
		$Form->Erstelle_SelectFeld('ADR_STATUS',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_STATUS')),130,$EditRecht,'','','A','','',$KateogrieAlt,'');
		$Form->ZeileEnde();	
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_AZG_KEY'].':',150);
		$SQL = 'SELECT AZG_KEY, AZG_BEZEICHNUNG AS Bereich';
		$SQL .= ' FROM ADRESSENZUGRIFFSGRUPPEN';
		$SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
		$SQL .= ' INNER JOIN KONTAKTEABTEILUNGEN ON AZG_KAB_KEY=KAB_KEY';
		$SQL .= ' WHERE AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
		$SQL .= ' OR AZG_KEY = 1';		// oder �ffentlich!
		$SQL .= ' ORDER BY AZG_BEZEICHNUNG';
		$Form->Erstelle_SelectFeld('ADR_AZG_KEY',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_AZG_KEY')),300,$EditRecht,$SQL,$OptionBitteWaehlen,'','','','','');
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_NAME1'].':',150);
		$Form->Erstelle_TextFeld('ADR_NAME1',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_NAME1')),50,350,$EditRecht);
		$Form->ZeileEnde();
			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_NAME2'].':',150);
		$Form->Erstelle_TextFeld('ADR_NAME2',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_NAME2')),50,350,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_KONZERN'].':',150);
		$Form->Erstelle_TextFeld('ADR_KONZERN',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_KONZERN')),50,350,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_STRASSE'].'/'.$AWISSprachKonserven['ADR']['ADR_HAUSNUMMER'].':',150);
		$Form->Erstelle_TextFeld('ADR_STRASSE',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_STRASSE')),50,330,$EditRecht);
		$Form->Erstelle_TextFeld('ADR_HAUSNUMMER',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_HAUSNUMMER')),10,200,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		if($AWIS_KEY1===0)
		{
			$Parameter=array();
		}
		else
		{
			$Parameter = array('$NAME1'=>urlencode($rsFelder->FeldInhalt('ADR_NAME1')),
					'$NAME2'=>urlencode($rsFelder->FeldInhalt('ADR_NAME2')),
					'$STRASSE'=>urlencode($rsFelder->FeldInhalt('ADR_STRASSE')),
					'$HAUSNUMMER'=>urlencode($rsFelder->FeldInhalt('ADR_HAUSNUMMER')),
					'$PLZ'=>urlencode($rsFelder->FeldInhalt('ADR_PLZ')),
					'$ORT'=>urlencode($rsFelder->FeldInhalt('ADR_ORT')),
					'$LAN_CODE'=>urlencode($rsFelder->FeldInhalt('ADR_LAN_CODE'))
			);
		}
		
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_LAN_CODE'].'/'.$AWISSprachKonserven['ADR']['ADR_PLZ'].'/'.$AWISSprachKonserven['ADR']['ADR_ORT'].':',150);
		$SQL = 'SELECT LAN_CODE, LAN_LAND FROM Laender ORDER BY LAN_LAND';
		$Form->Erstelle_SelectFeld('ADR_LAN_CODE',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_LAN_CODE')),-40,$EditRecht,$SQL,false,(($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_LAN_CODE'))==''?'DE':''),'LAN_CODE','','','');
		$Form->Erstelle_TextFeld('ADR_PLZ',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_PLZ')),8,80,$EditRecht);
		$Link = ($AWIS_KEY1===0?'':strtr($INET_LINK_Karte,$Parameter));
		$Form->Erstelle_TextFeld('ADR_ORT',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_ORT')),50,400,$EditRecht,'','',$Link);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_POSTFACH'].':',150);
		$Form->Erstelle_TextFeld('ADR_POSTFACH',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_POSTFACH')),10,120,$EditRecht);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_POSTFACHPLZ'].':',150);
		$Form->Erstelle_TextFeld('ADR_POSTFACHPLZ',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_POSTFACHPLZ')),10,120,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_TELEFON'].':',150);
		$Form->Erstelle_TextFeld('ADR_TELEFON',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_TELEFON')),20,220,$EditRecht);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_TELEFAX'].':',150);
		$Form->Erstelle_TextFeld('ADR_TELEFAX',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_TELEFAX')),20,220,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_MOBILTELEFON'].':',150);
		$Form->Erstelle_TextFeld('ADR_MOBILTELEFON',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_MOBILTELEFON')),20,220,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_STEUERNUMMER'].':',150);
		$Form->Erstelle_TextFeld('ADR_STEUERNUMMER',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_STEUERNUMMER')),20,220,$EditRecht);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_USTIDNR'].':',150);
		$Form->Erstelle_TextFeld('ADR_USTIDNR',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_USTIDNR')),20,220,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_HANDELSREGISTER'].':',150);
		$Form->Erstelle_TextFeld('ADR_HANDELSREGISTER',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_HANDELSREGISTER')),20,220,$EditRecht);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_EMAIL'].':',150);
		
		$Link = $rsFelder->FeldInhalt('ADR_EMAIL') != ''?'mailto:'.$rsFelder->FeldInhalt('ADR_EMAIL'):'';
		$Form->Erstelle_TextFeld('ADR_EMAIL',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_EMAIL')),50,520,$EditRecht,'','',$Link);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_WEBSEITE'].':',150);
		$Link = $rsFelder->FeldInhalt('ADR_WEBSEITE')!= ''?$rsFelder->FeldInhalt('ADR_WEBSEITE'):'';
		$Form->Erstelle_TextFeld('ADR_WEBSEITE',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_WEBSEITE')),80,820,$EditRecht,'','',$Link);
		$Form->ZeileEnde();
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ADR']['ADR_BEMERKUNG'].':',150);
		$Form->Erstelle_Textarea('ADR_BEMERKUNG',($AWIS_KEY1===0?'':$rsFelder->FeldInhalt('ADR_BEMERKUNG')),300,80,4,(($Recht3000&16384)!=0),'border-style=solid;');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
		
		if(!isset($_POST['cmdDSNeu_x']))
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Infos'));
			echo '<input type=hidden name=Seite value='.$Form->Format('T',$RegisterSeite,'DB',false).'>';
			
			$Reg = new awisRegister(3001);
			$Reg->ZeichneRegister($RegisterSeite);
		}
	}			

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();
	if($DetailAnsicht AND ($Recht3000&(2+4+256))!==0)		//
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	if(($Recht3000&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if($DetailAnsicht AND ($Recht3000&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}
	
	if($DetailAnsicht AND !isset($_POST['cmdDSNeu_x']))
	{
		$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_DSZurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
	}
	$Form->SchaltflaechenEnde();
	
	echo '</form>';
	$AWISBenutzer->ParameterSchreiben('AdressenSuche', serialize($Param));
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		$Form->Hinweistext('allg. Fehler:'.$ex->getMessage());
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
    global $AWISBenutzer;
    global $AWIS_KEY1;
    global $AWIS_KEY2;
    global $DB;
    
	$Bedingung = '';
	
    if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ADR_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}
	if(isset($Param['NAME']) AND $Param['NAME']!='')			// Name
	{
		$Bedingung .= 'AND (UPPER(ADR_Name1) ' . $DB->LIKEoderIST($Param['NAME'].'%',1) . ' ';
		$Bedingung .= 'OR  UPPER(ADR_Name2) ' . $DB->LIKEoderIST($Param['NAME'].'%',1) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['KONTAKT']) AND $Param['KONTAKT']!='')			// Kontakt
	{
		$Bedingung .= 'AND EXISTS (SELECT * FROM AdressenAnsprechpartner';
		$Bedingung .= ' WHERE (UPPER(AAP_NACHNAME) ' . $DB->LIKEoderIST($Param['KONTAKT'].'%',1) . ' ';
		$Bedingung .= ' OR UPPER(AAP_VORNAME) ' . $DB->LIKEoderIST($Param['KONTAKT'].'%',1) . ')';
		$Bedingung .= ' AND AAP_ADR_KEY = Adressen.ADR_KEY)';
	}
	if(isset($Param['ADR_AZG_KEY']) AND intval($Param['ADR_AZG_KEY'])!=0)		// Zugriffsgruppe
	{
		$Bedingung .= 'AND ADR_AZG_KEY = ' . intval($Param['ADR_AZG_KEY']) . '';
		
	}
	if(isset($Param['ADR_ORT']) AND $Param['ADR_ORT']!='')		// Ort
	{
		$Bedingung .= 'AND (UPPER(ADR_ORT) ' . $DB->LIKEoderIST($Param['ADR_ORT'].'%',1) . ' ';
		$Bedingung .= ' OR ADR_PLZ ' . $DB->LIKEoderIST($Param['ADR_ORT'].'%',1) . ')';
	}
	if(isset($Param['ADR_BEMERKUNG']) AND $Param['ADR_BEMERKUNG']!='')		// Bemerkung
	{
		$Bedingung .= 'AND (UPPER(ADR_BEMERKUNG) ' . $DB->LIKEoderIST('%'.$Param['ADR_BEMERKUNG'].'%',1) . ' ';
		$Bedingung .= ' OR UPPER(ADR_BEMERKUNG) ' . $DB->LIKEoderIST('%'.$Param['ADR_BEMERKUNG'].'%',1) . ')';
	}
	return $Bedingung;
}