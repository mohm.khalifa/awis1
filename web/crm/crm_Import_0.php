<?php
/**
 * Import der Adressdaten, Quelle Dataforce
 * 
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20070904
 * 
 */

global $awisRSZeilen;

$Trennzeichen = ',';		// Trennzeichen in der Datei
$HochKommaEntfernen = true;

$TextKonserven = array();
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Fehler','err_keineUploadDatei');
$TextKonserven[]=array('Fehler','err_FalschesFormat');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','Import%');

$AWISSprachKonservenImport = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

// Recht vorhanden?
$Recht3701=awisBenutzerRecht($con,3701);
if(($Recht3701&1)!==1)
{
    awisEreignis(3,709040930,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_keineRechte']."</span>";
	die();
}

// Versuche, die Datei zu �ffnen
$Datei = $_FILES['datAdressen']['tmp_name'];
$fp = @fopen($Datei, 'r');
if($fp===false)
{
    awisEreignis(3,709040931,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_keineUploadDatei']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

// Erste Zeile enth�lt die �berschrift
$Zeile = fgets($fp);
if($Zeile===false)
{
    awisEreignis(3,709040933,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_keineUploadDatei']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

// Stimmt die �berschrift, d.h. ist es die richtige Datei?
$Zeile = ($HochKommaEntfernen?str_replace('"','',$Zeile):$Zeile);
$Felder = explode($Trennzeichen,$Zeile);
for($i=0;$i<sizeof($Felder);$i++)
{
	$Felder[$i] = strtoupper(trim(($HochKommaEntfernen?str_replace('"','',$Felder[$i]):$Felder[$i])));
}
if($Felder[0]!=='FIRMA_ID')
{
    awisEreignis(3,709040937,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_FalschesFormat']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$Zeile = fgets($fp);
$AnzZeilen = 0;					// Anzahl Zeilen f�r die Info-Ausgabe
$AnzZeilenFehler = 0;				// Anzahl der fehlerhaften Zeilen f�r die Info-Ausgabe
$AnzZeilenNEU=0;
while($Zeile!==false) 
{
	
	$Daten = explode('","',substr($Zeile,1).',"');
	// TODO: bei PHP5 ersetzen durch array_combine()
	$AdrDaten = array();
	for($i=0;$i<sizeof($Felder);$i++)
	{
		$AdrDaten[$Felder[$i]]=$Zeile = ($HochKommaEntfernen?str_replace('"','',$Daten[$i]):$Daten[$i]);
	}
awis_Debug(1,$AdrDaten);
	$SQL = 'SELECT BRA_KEY FROM Branchen WHERE UPPER(BRA_BEZEICHNUNG)=' . awisFeldFormat('T',strtoupper($AdrDaten['BRANCHE']));
	$rsBRA = awisOpenRecordset($con,$SQL);
	if($awisRSZeilen==0 AND $AdrDaten['BRANCHE']!='')
	{
		$SQL = 'INSERT INTO Branchen(BRA_Bezeichnung, BRA_User, BRA_UserDat)';
		$SQL .= ' VALUES(';
		$SQL .= ''.awisFeldFormat('T',$AdrDaten['BRANCHE']);
		$SQL .= ' ,\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ' ,SYSDATE';
		$SQL .= ')';

		if(awisExecute($con,$SQL)===false)
		{
		    awisEreignis(3,709040937,'CRM',$AWISBenutzer->BenutzerName(),'','','');
		    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_FalschesFormat']."</span>";
			echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
			die();
		}
		
		// Key f�r die Branche ermitteln
		$SQL = 'SELECT SEQ_BRA_KEY.CURRVAL AS KEY FROM DUAL';
		$rsBRA = awisOpenRecordset($con,$SQL);
		$BRAKEY = $rsBRA['KEY'][0];
	}
	else 
	{
		$BRAKEY = ($AdrDaten['BRANCHE']==''?'':$rsBRA['BRA_KEY'][0]);
	}

		// ***********************************************
		// Gebiet suchen
		// ***********************************************
		
	$SQL = 'SELECT * FROM CRMGebiete';
	$rsCGB=awisOpenRecordset($con,$SQL);
	$rsCGBZeilen = $awisRSZeilen;
	$CGBKEY='';
	for($CGBZeile=0;$CGBZeile<$rsCGBZeilen;$CGBZeile++)
	{
		if(ereg($rsCGB['CGB_KENNUNG'][$CGBZeile],$AdrDaten['PLZ'])!==FALSE)
		{
			$CGBKEY=$rsCGB['CGB_KEY'][$CGBZeile];
			break;
		}
	}
	
		// ***********************************************
		// Gibt es schon die Adresse
		// ***********************************************
		
	$SQL = 'SELECT * FROM CRMAdressen ';
	$SQL .= ' WHERE CAD_IMPORTID='.awisFeldFormat('T',$AdrDaten['FIRMA_ID']).' AND CAD_IMQ_ID=2048';
	$rsCAD = awisOpenRecordset($con, $SQL);
	$Modus = 'NEU';				// Modus: NEU=Insert, ALT=Update
	if($awisRSZeilen==0)		// Adresse noch nicht vorhanden
	{
		$SQL = 'INSERT INTO CRMAdressen';
		$SQL .= '(CAD_Name1, CAD_Name2, CAD_Strasse, CAD_Hausnummer, CAD_PLZ, CAD_Ort';
		$SQL .= ',CAD_Anrede, CAD_LAN_CODE, CAD_Telefon, CAD_Telefax,CAD_Mitarbeiter';
		$SQL .= ',CAD_CGB_Key, CAD_ImportID, CAD_IMQ_ID, CAD_WebSeite, CAD_Bemerkung';
		$SQL .= ',CAD_User, CAD_UserDat';
		$SQL .= ')VALUES(';
		$SQL .= ''.awisFeldFormat('T',$AdrDaten['FIRMA1']);
		$SQL .= ','.awisFeldFormat('T',$AdrDaten['FIRMA2']);
		
		$Strasse=awis_ZerlegeStrasse($AdrDaten['STRASSE']);
		$SQL .= ','.awisFeldFormat('T',$Strasse['strasse']);
		$SQL .= ','.awisFeldFormat('T',$Strasse['hausnummer']);
		
		$SQL .= ','.awisFeldFormat('T',$AdrDaten['PLZ']);
		$SQL .= ','.awisFeldFormat('T',$AdrDaten['STADT']);
		
		$SQL .= ',3';			// Firma
		$SQL .= ',\'DE\'';			// immer Deutschland

		$SQL .= ','.awisFeldFormat('T','+49'.$AdrDaten['VORWAHL'].$AdrDaten['RUFNUMMER']);
		if($AdrDaten['TELEFAX']!='')
		{
			$SQL .= ','.awisFeldFormat('T','+49'.$AdrDaten['VORWAHL'].$AdrDaten['TELEFAX']);
		}
		else 
		{
			$SQL .= ',null';
		}

		$SQL .= ','.awisFeldFormat('T',$AdrDaten['ANZMA']);
		$SQL .= ','.awisFeldFormat('Z',$CGBKEY,'DB',true);
		$SQL .= ','.awisFeldFormat('T',$AdrDaten['FIRMA_ID'],'DB',true);
		$SQL .= ',2048';
		
		$WebSeite = $AdrDaten['HTTPWWW'];
		if($WebSeite!='' AND strtolower(substr($WebSeite,0,4))!='http')
		{
			$WebSeite = 'http://'.$AdrDaten['HTTPWWW'];
		}
		$SQL .= ','.awisFeldFormat('T',$WebSeite,'DB',true);
		$SQL .= ','.awisFeldFormat('T',$AdrDaten['KOMMENTAR'],'DB',true);
		$SQL .= ',\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', SYSDATE';
		$SQL .= ')';
		
		if(awisExecute($con,$SQL)===false)
		{
		    awisEreignis(3,709040937,'CRM',$AWISBenutzer->BenutzerName(),'','','');
		    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_FalschesFormat']."</span>";
			echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
			$AnzZeilenFehler++;
			$CADKEY=0;
		}
		else 
		{
			// Key f�r die Adresse ermitteln
			$SQL = 'SELECT SEQ_CAD_KEY.CURRVAL AS KEY FROM DUAL';
			$rsBRA = awisOpenRecordset($con,$SQL);
			$CADKEY = $rsBRA['KEY'][0];
			$AnzZeilenNEU++;
		}		
		
		if($CADKEY!==0)		// Konnte die Adresse gefunden werden?
		{
			$APFelder=array('01','02','03','04');
			foreach($APFelder AS $APFeld)
			{
				if($AdrDaten['NNAME'.$APFeld]=='')
				{
					continue;		// Wenn kein Name angegeben wurde, AP ignorieren
				}
				$SQL = 'INSERT INTO CRMAnsprechpartner';
				$SQL .= ' (CAP_CAD_KEY, CAP_ANREDE, CAP_VORNAME, CAP_NACHNAME, CAP_FUNKTION';
				$SQL .= ', CAP_USER, CAP_USERDAT)VALUES(';
				$SQL .= ''.$CADKEY;
				$SQL .= ','.awis_FeldInhaltFormat('Z',($AdrDaten['ANREDE'.$APFeld]=='Herr'?1:2));
				$SQL .= ','.awis_FeldInhaltFormat('T',$AdrDaten['VNAME'.$APFeld],true);
				$SQL .= ','.awis_FeldInhaltFormat('T',$AdrDaten['NNAME'.$APFeld],true);
				$SQL .= ','.awis_FeldInhaltFormat('T',$AdrDaten['APPOS'.$APFeld],true);
				$SQL .= ','.awis_FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),true);
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				
				if(awisExecute($con,$SQL)===false)
				{
				    awisEreignis(3,0709051344,'CRM-AP',$AWISBenutzer->BenutzerName(),'','','');
				    echo "<span class=HinweisText>".$AWISSprachKonservenImport['Fehler']['err_FalschesFormat']."</span>";
					echo "<br><br><input type=image title='".$AWISSprachKonservenImport['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
					$AnzZeilenFehler++;
				}
			}	
		}
		
		
	}
	else 
	{
		$AnzZeilen++;
	}
	
	$Zeile = fgets($fp);
}

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Ueberschrift($AWISSprachKonservenImport['Wort']['ImportBericht'],200);
awis_FORM_ZeileEnde();

awis_FORM_Trennzeile('O');

awis_FORM_ZeileStart();
$Text = str_replace('%1',$AnzZeilen,$AWISSprachKonservenImport['Wort']['ImportVerarbeitet']);
awis_FORM_Erstelle_TextLabel($Text,500);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
$Text = str_replace('%1',$AnzZeilenNEU,$AWISSprachKonservenImport['Wort']['ImportNeu']);
awis_FORM_Erstelle_TextLabel($Text,500);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
$Text = str_replace('%1',$AnzZeilenFehler,$AWISSprachKonservenImport['Wort']['ImportFehler']);
awis_FORM_Erstelle_TextLabel($Text,500);
awis_FORM_ZeileEnde();


awis_FORM_FormularEnde();

?>