<?php
global $Funktionen;

$Funktionen->RechteMeldung(0);

$Funktionen->Form->SchreibeHTMLCode("<form method=post action=./abteilungsbearbeitung_Main.php?cmdAktion=Details>");
$Funktionen->Form->Formular_Start();

$LabelBreite = 200;
$FeldBreite = 200;

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KAB']['KAB_ABTEILUNG'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*KAB_ABTEILUNG', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['KAB_ABTEILUNG']:''), 40, $FeldBreite, true);
$Funktionen->Form->ZeileEnde();
$Funktionen->AWISCursorPosition = "sucKAB_ABTEILUNG";

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['KON']['KON_NAME'].":", $LabelBreite);
$Funktionen->Form->Erstelle_TextFeld('*KON_NAME', (isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?$Funktionen->Param['KON_NAME']:''), 40, $FeldBreite, true);
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->ZeileStart();
$Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',180);
$Funktionen->Form->Erstelle_Checkbox('*AuswahlSpeichern',(isset($Funktionen->Param['SPEICHERN']) && $Funktionen->Param['SPEICHERN']=='on'?'on':''),50,true,'on');
$Funktionen->Form->ZeileEnde();

$Funktionen->Form->Formular_Ende();

$Funktionen->Form->SchaltflaechenStart();

$Funktionen->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Funktionen->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
if(($Funktionen->AWISBenutzer->HatDasRecht(650)& 4) == 4) {
    $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

$Funktionen->Form->SchaltflaechenEnde();

$Funktionen->Form->SchreibeHTMLCode("</form>");