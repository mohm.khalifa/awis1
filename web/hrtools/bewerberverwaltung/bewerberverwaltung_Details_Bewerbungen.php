<?php
global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;
global $Form;
global $Gespeichert;
global $Speichernmeldung;
global $SEQBWH;
global $EingabeOK;
global $Folgestatus;
global $rsHRK;
global $Speichernausblenden;
global $Dokument;

require_once('bewerberverwaltung_Details_Bewerbungen_Funktionen.php');
$Script = "
            <script type='text/javascript'>
           
            

        
            function postFirm()
            {   
                  document.getElementsByName('cmdFirmierungsChange')[0].click();
            }
                
            function postStatus()
            {   
                  document.getElementsByName('cmdStatusChange')[0].click();
            }
            
            function emailUebernehmen()
            {
                if (document.getElementsByName('txtBEW_BPK')[0].value != '::bitte w�hlen::')
                {
                    document.getElementsByName('txtBEW_BPK_SONST')[0].value = document.getElementsByName('txtBEW_BPK')[0].value;
                
                }
                else
                {
                     document.getElementsByName('txtBEW_BPK_SONST')[0].value = '';
                }
        
                //alert(document.getElementsByName('txtBEW_BPK')[0].value);       
            }
        
            function gesendetAnUebernehmen()
            {
                if (document.getElementsByName('txtBEW_GESENDET_AN')[0].value != '::bitte w�hlen::')
                {
                        
                     document.getElementById('txtEskalationsGesendetAn').value =  document.getElementsByName('txtBEW_GESENDET_AN')[0].value;
                
                }
                else
                {
                     document.getElementsByName('txtEskalationsGesendetAn')[0].value = '';
                }          
            }
        
            function gesendetAnUebernehmenZentrale()
            {
                if (document.getElementsByName('txtGesendetAnCMB')[0].value != '::bitte w�hlen::')
                {
                        
                     document.getElementById('txtGesendetAn').value =  document.getElementsByName('txtGesendetAnCMB')[0].value;
                
                }
                else
                {
                     document.getElementsByName('txtGesendetAn')[0].value = '';
                }          
            }
        
            function EinAusklappen(frame,Seite) {
	
    	 	if(document.getElementById('FRAME'+frame).style.display == 'block') 
        	{ 
        	     document.getElementById('FRAME'+frame).style.display = 'none'; 
    			 if(frame != 5)
    			 { 
    				document.getElementById('UEB'+frame).style.borderBottom = '';
    			 }
    			 document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_plus.png';                 
    	    }
    		else 
    		{
    			  document.getElementById('FRAME'+frame).style.display = 'block';	
    			  if(frame != 5)
    			  { 
    			  	document.getElementById('UEB'+frame).style.borderBottom = '1px solid black';
    			  }
    			  document.getElementById('IMG_LOAD'+frame).src = '/bilder/icon_minus.png'; 
    		}		
    	}	
        
        
        //Schauen, ob Browser indexOf kann, wenn nicht mach ichs selbst. 
        if (!Array.indexOf) 
        {    
            Array.prototype.indexOf = function (obj, start) 
            {
                 for (var i = (start || 0); i < this.length; i++) 
                 {
                    if (this[i] == obj) 
                    {
                        return i;
                    }
                 }
                 return -1;
            }
        }

        
        
        function divsklappen(divID, AufklappWerte, cmbName,ZielfeldName)
        {
            var ARRAufklappWerte = AufklappWerte.split(',');
            if(ARRAufklappWerte.indexOf(String(document.getElementsByName(cmbName)[0].selectedIndex)) != -1 )
            {               
                document.getElementById(divID).style.display = 'block';	
            }
            else
            {
                document.getElementById(divID).style.display = 'none';
                document.getElementsByName(ZielfeldName)[0].value ='';	
            }
        }
            
             </script>
            ";

echo $Script;
// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('BEW', '%');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Fehler', 'err_keineDatenbank');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
$Recht39001 = $AWISBenutzer->HatDasRecht(39001);
$EditRecht = (($Recht39001 & 2) == 2);

//$AWIS_KEY2=isset($_GET['BWH_KEY'])?$_GET['BWH_KEY']:0;

$Form->Formular_Start();

if (isset($Speichernmeldung) and $Speichernmeldung != '') {
    $Form->Hinweistext($Speichernmeldung);
}

if (isset($_GET['BewSort']) and $_GET['BewSort'] != '') {        // wenn GET-Sort, dann nach diesen Feld sortieren
    $Param['BEWORDER'] = str_replace('~', ' DESC ', $_GET['BewSort']);
}
//Allgemeiner SQL f�r Liste + Detaildatensatz.
$SQL = 'SELECT';
$SQL .= '   HRT_KEY ,';
$SQL .= '   BWH_KEY , BWH_USER, BWH_USERDAT,';
$SQL .= '   HRT_SUCH , BWS_KEY, BWS_STEUER_ID,';
$SQL .= '  HRF_KEY, HRF_FIRMIERUNG , HRK_KEY, BWS_KEY, FUNC_TEXTKONSERVE(XTX_KENNUNG, XTX_BEREICH, \'' . $AWISBenutzer->BenutzerSprache() . '\') as BWS_TEXT, ';
$SQL .= '   EINGANGSDATUM';
$SQL .= '   ,row_number() OVER ( ';
$SQL .= '   order by ' . ((isset($Param['BEWORDER']) and $Param['BEWORDER'] != '')?$Param['BEWORDER']:1) . ') as ZEILENNR';
$SQL .= ' FROM';
$SQL .= '   V_BEWERBERVERWALTUNG';
$SQL .= ' WHERE HRK_KEY ' . $DB->LikeOderIst($AWIS_KEY1);

$ZeigeDatensatz = true;
if ($EingabeOK and !issetCMDStatusWeiter() and !$Gespeichert) {
    $SQLStatus = 'SELECT';
    $SQLStatus .= '   BWS_KEY, ';
    $SQLStatus .= '   BWS_BEZEICHNUNG,';
    $SQLStatus .= '   BWS_TEXTKONSERVE,';
    $SQLStatus .= '   BWS_AKTIV,';
    $SQLStatus .= '   BWS_DATENSATZAKTIV,';
    $SQLStatus .= '   BWS_STEUER_ID,';
    $SQLStatus .= '   BWS_ZWISCHENSEITENTYP';
    $SQLStatus .= ' FROM';
    $SQLStatus .= '   BEWERBERSTATUS ';
    $SQLStatus .= ' WHERE BWS_AKTIV = \'A\'';
    $SQLStatus .= ' AND BWS_KEY' . $DB->LikeOderIst($Folgestatus); //$Folgestatus kommt aus der Speichernseite

    $rsStatus = $DB->RecordSetOeffnen($SQLStatus);

    if (isset($_POST['txtBWS_KEY']) and holeAktuellenStatus() != $_POST['txtBWS_KEY']) {
        //Eingangsart holen
        $rsEingangsart = $DB->RecordSetOeffnen(baueBIFWertSQL(9, $AWIS_KEY2));

        $EmailAnbieten = false;
        if ($rsEingangsart->FeldInhalt('BIF_WERT') == 2 or $rsHRK->FeldInhalt('HRK_EMAIL') != '') {
            $EmailAnbieten = true;
        }

        switch ($rsStatus->FeldInhalt('BWS_ZWISCHENSEITENTYP')) {
            case '0':
                $Speichernausblenden = false;
                break;
            case '1':
                $Speichernausblenden = true;
                $Form->ZeileStart();
                $Form->Hinweistext($AWISSprachKonserven['BEW']['BEW_MSG_STATUSWEITER']);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                if ($EmailAnbieten) {
                    $Form->Schaltflaeche('submit', 'cmdStatusWeiterM', '', '', 'ja, Email', '', '', 50);
                    // $Form->Schaltflaeche('submit','cmdStatusWeiterO', '', '','ja, Outlook','','',50);
                }

                $Form->Schaltflaeche('submit', 'cmdStatusWeiterS', '', '', 'ja, Serienbrief', '', '', 50);

                $Form->Schaltflaeche('submit', 'cmdStatusNein', '', '', 'nein', '', '', 50);
                $Form->ZeileEnde();
                
                $Form->ZeileStart();
                $Form->Trennzeile('O');
                $Form->ZeileEnde();
                post2hidden();
                break;
            case '2':
                $Speichernausblenden = true;
                if ($rsStatus->FeldInhalt('BWS_KEY') == 8) {
                    $Form->ZeileStart();
                    $Form->Hinweistext($AWISSprachKonserven['BEW']['BEW_MSG_EINGABEFERTIG']);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    if ($EmailAnbieten) {
                        $Form->Schaltflaeche('submit', 'cmdStatusWeiterM', '', '', 'ja, Email', '', '', 50);
                        //$Form->Schaltflaeche('submit','cmdStatusWeiterO', '', '','ja, Outlook','','',50);
                    }

                    $Form->Schaltflaeche('submit', 'cmdStatusWeiterO', '', '', 'ja, keine Email', '', '', 50);

                    $Form->Schaltflaeche('submit', 'cmdStatusWeiterS', '', '', 'ja, Serienbrief', '', '', 50);
                    $Form->Schaltflaeche('submit', 'cmdStatusNein', '', '', 'nein', '', '', 50);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    $Form->Trennzeile('O');
                    $Form->ZeileEnde();
                    post2hidden();
                } elseif ($rsStatus->FeldInhalt('BWS_KEY') == 9) {
                    $Form->ZeileStart();
                    $Form->Hinweistext($AWISSprachKonserven['BEW']['BEW_MSG_STATUSWEITER']);
                    $Form->ZeileEnde();

                    $Form->ZeileStart();
                    if ($EmailAnbieten) {
                        $Form->Schaltflaeche('submit', 'cmdStatusWeiterO', '', '', 'ja, Outlook', '', '', 50);
                    }

                    $Form->Schaltflaeche('submit', 'cmdStatusWeiterS', '', '', 'ja, Serienbrief', '', '', 50);
                    $Form->Schaltflaeche('submit', 'cmdStatusNein', '', '', 'nein', '', '', 50);
                    $Form->ZeileEnde();
                    $Form->ZeileStart();
                    $Form->Trennzeile('O');
                    $Form->ZeileEnde();
                    post2hidden();
                }
                break;
            default:

                break;
        }
        $ZeigeDatensatz = false;
    } else {
        $ZeigeDatensatz = true;
    }
}

if (($AWIS_KEY2 != '' and $ZeigeDatensatz))// or $DB->ErmittleZeilenAnzahl($SQL) == 0) //Detaildatensatz
{
    $SQL .= ' and bwh_key ' . $DB->LikeOderIst($AWIS_KEY2);
    $Form->DebugAusgabe(1, "SQL Bewerbungen: " . $SQL);
    $rsBewerbungen = $DB->RecordSetOeffnen($SQL);

    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./bewerberverwaltung_Main.php?cmdAktion=Details&HRK_KEY=0" . $_GET['HRK_KEY'] . " accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsBewerbungen->FeldInhalt('BWH_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsBewerbungen->FeldInhalt('BWH_USERDAT'));
    $Form->InfoZeile($Felder, '');

    $Form->Erstelle_HiddenFeld('HRK_KEY', $AWIS_KEY1);

    $SQLDetail = 'select';
    $SQLDetail .= ' a.BIT_KEY ,';
    $SQLDetail .= ' a.BIT_INFORMATION ,';
    $SQLDetail .= ' a.BIT_BEMERKUNG ,';
    $SQLDetail .= ' a.BIT_HRF_KEY ,';
    $SQLDetail .= ' a.BIT_XRC_ID_ANZEIGE ,';
    $SQLDetail .= ' a.BIT_RECHTEBIT_ANZEIGE ,';
    $SQLDetail .= ' a.BIT_STATUS ,';
    $SQLDetail .= ' SUBSTR(bit_textkonserve,0,instr(bit_textkonserve,\':\')-1) AS  TXTBEREICH,
                       SUBSTR(bit_textkonserve,instr(bit_textkonserve,\':\')+1) AS TXTKENNUNG ,';
    $SQLDetail .= ' a.BIT_DATENQUELLE ,';
    $SQLDetail .= ' a.BIT_FORMAT ,';
    $SQLDetail .= ' a.BIT_FELDTYP ,';
    $SQLDetail .= ' a.BIT_LABELBREITE ,';
    $SQLDetail .= ' a.BIT_FELDBREITE ,';
    $SQLDetail .= ' a.BIT_FELDLAENGE ,';
    $SQLDetail .= ' a.BIT_PFLICHT ,';
    $SQLDetail .= ' a.BIT_USER ,';
    $SQLDetail .= ' a.BIT_USERDAT ,';
    $SQLDetail .= ' a.BIT_SICHTBAR ,';
    $SQLDetail .= ' a.BIT_RECHTEBIT_BEARBEITEN ,';
    $SQLDetail .= ' a.BIT_ZUORDKUERZEL ,';
    $SQLDetail .= ' a.BIT_ZUORDNUNG,';
    $SQLDetail .= ' a.BIT_SORTIERUNG, a.BIT_LETZTEZEILE';
    $SQLDetail .= ' from';
    $SQLDetail .= '   BEWERBERINFOSTYPEN a';
    $SQLDetail .= ' where';
    $SQLDetail .= '   a.BIT_STATUS     = \'A\'';
    $SQLDetail .= ' and a.BIT_SICHTBAR = 1';
    $SQLDetail .= ' and ( a.BIT_HRF_KEY = 0 or 
            a.BIT_HRF_KEY ' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('HRF_KEY') > 0?$rsBewerbungen->FeldInhalt('HRF_KEY'):0);

    $SQLDetail .= ') order by a.BIT_SORTIERUNG asc';
    $Form->DebugAusgabe(1, 'SQL DETAIL: ' . $SQLDetail);
    $rsDetail = $DB->RecordSetOeffnen($SQLDetail);

    //Alle verwendeten Textkonserven laden.
    $SQLKonserven = 'select distinct';
    $SQLKonserven .= ' SUBSTR(bit_textkonserve,0,instr(bit_textkonserve,\':\')-1) AS  TEXTKONSERVENBEREICHE';
    $SQLKonserven .= ' from';
    $SQLKonserven .= '   BEWERBERINFOSTYPEN a';
    $SQLKonserven .= ' where';
    $SQLKonserven .= '   a.BIT_STATUS     = \'A\'';
    $SQLKonserven .= ' and a.BIT_SICHTBAR = 1';
    $SQLKonserven .= ' and ( a.BIT_HRF_KEY ' . $DB->LikeOderIst($rsBewerbungen->FeldInhalt('HRF_KEY') > 0?$rsBewerbungen->FeldInhalt('HRF_KEY'):0);
    $SQLKonserven .= ' or a.BIT_HRF_KEY = 0 )';
    $Form->DebugAusgabe(1, 'SQL Textkonserven: ' . $SQLKonserven);
    $rsKonserven = $DB->RecordSetOeffnen($SQLKonserven);
    $KonservenBIT = array();
    while (!$rsKonserven->EOF()) {
        $KonservenBIT[] = array($rsKonserven->FeldInhalt('TEXTKONSERVENBEREICHE'), '%');
        //var_dump($rsKonserven->FeldInhalt('TEXTKONSERVENBEREICHE'));
        $rsKonserven->DSWeiter();
    }

    //Firmierung nicht �nderbar, au�er bei kompletter neuanlage.
    $FeldAendern = false;

    if ($rsBewerbungen->FeldInhalt('BWS_KEY') == '') {
        $FeldAendern = true;
    }

    //Wenn das Feld nicht per POST kommt, dann hat er gerade aufs Plus geklickt oder ist in einen bestehnden Datensatz drin. Wenn es da ist, und Bitte w�hlen kommt, dann wurde von einer Firmirung zu Bitte w�hlen gewechselt (Speichern ausblenden, da kein Sinn).
    if ((!isset($_POST['txtBEW_FIRM']) and ($rsBewerbungen->FeldInhalt('HRF_KEY') == '' or $rsBewerbungen->FeldInhalt('HRF_KEY') == 0)) or (isset($_POST['txtBEW_FIRM']) and $_POST['txtBEW_FIRM'] == $AWISSprachKonserven['Wort']['txt_BitteWaehlen'])) {
        $Speichernausblenden = true;
    }

    $Konserven = $Form->LadeTexte($KonservenBIT);
    $Form->ZeileStart();
    $SQLFirm = 'SELECT HRF_KEY, HRF_FIRMIERUNG from HRFIRMIERUNG where HRF_AKTIV = 1';
    $Form->Erstelle_TextLabel($Konserven['BEW']['BWH_HRF_KEY'] . ': ', 250);
    $Form->Erstelle_SelectFeld('BEW_FIRM',
        isset($_POST['txtBEW_FIRM'])?$_POST['txtBEW_FIRM']:$rsBewerbungen->FeldInhalt('HRF_KEY'), 300, $FeldAendern,
        $SQLFirm, '', '1', '', '', '', 'onchange="postFirm();"');
    $Form->ZeileEnde();

    if ((isset($_POST['txtBEW_FIRM']) and $_POST['txtBEW_FIRM'] != '' and $_POST['txtBEW_FIRM'] != $AWISSprachKonserven['Wort']['txt_BitteWaehlen']) or $rsBewerbungen->FeldInhalt('HRF_KEY') > 0) {
        ladeBewerbungenDetail(isset($_POST['txtBEW_FIRM'])?$_POST['txtBEW_FIRM']:$rsBewerbungen->FeldInhalt('HRF_KEY'),
            $AWIS_KEY1, $AWIS_KEY2);
    } else {
        ladeBewerbungenDetail(1, $AWIS_KEY1, $AWIS_KEY2, true);
        $Form->Erstelle_HiddenFeld('BWS_KEY', 8);
        $Speichernausblenden = false;
    }
} elseif ($AWIS_KEY2 == '') //Liste
{
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe', $AWISBenutzer->BenutzerName());
    $Form->DebugAusgabe(1, $SQL);
    $rsBewerbungen = $DB->RecordSetOeffnen($SQL);

    $Form->ZeileStart();

    $FeldBreiten['ICONS'] = 20;
    $FeldBreiten['HRT_SUCH'] = 250;
    $FeldBreiten['HRF_FIRMIERUNG'] = 250;
    $FeldBreiten['EINGANGSDATUM'] = 200;
    $FeldBreiten['BWS_TEXT'] = 260;
    $FeldBreiten['Hist'] = 40;

    if (($Recht39001 & 6) > 0) {
        $Icons[] = array(
            'new',
            './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&BWH_KEY=-1&HRK_KEY=0' . $AWIS_KEY1
        );
        $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['ICONS'], -1);
    } else {
        $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ICONS'], '', '');
    }

    $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=' . $AWIS_KEY1;
    $Link .= '&BewSort=HRT_SUCH' . ((isset($_GET['BewSort']) AND ($_GET['BewSort'] == 'HRT_SUCH'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_TAETIGKEITEN'], $FeldBreiten['HRT_SUCH'], '',
        $Link);

    $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=' . $AWIS_KEY1;
    $Link .= '&BewSort=HRF_FIRMIERUNG' . ((isset($_GET['BewSort']) AND ($_GET['BewSort'] == 'HRF_FIRMIERUNG'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['HRT_FIRMIERUNG'], $FeldBreiten['HRF_FIRMIERUNG'],
        '', $Link);

    $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=' . $AWIS_KEY1;
    $Link .= '&BewSort=EINGANGSDATUM' . ((isset($_GET['BewSort']) AND ($_GET['BewSort'] == 'EINGANGSDATUM'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_EINGANGSDATUM'], $FeldBreiten['EINGANGSDATUM'],
        '', $Link);

    $Link = './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=' . $AWIS_KEY1;
    $Link .= '&BewSort=BWS_KEY' . ((isset($_GET['BewSort']) AND ($_GET['BewSort'] == 'BWS_KEY'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_STATUS'], $FeldBreiten['BWS_TEXT'], '', $Link);

    if (($Recht39001 & 8) == 8) {
        // �berschrift der Listenansicht: History
        //$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BEW']['BEW_HIST'], $FeldBreiten['Hist'], '', '');
    }

    $Form->ZeileEnde();

    $DS = 0;
    while (!$rsBewerbungen->EOF()) {
        $Form->ZeileStart('', 'ZeileBWH"ID="ZeileBWH_' . $rsBewerbungen->FeldInhalt('BWH_KEY'));
        $Icons = array();

        $Icons[] = array(
            'edit',
            './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&BWH_KEY=0' . $rsBewerbungen->FeldInhalt('BWH_KEY') . '&HRK_KEY=0' . $AWIS_KEY1
        );
        $Form->Erstelle_ListeIcons($Icons, $FeldBreiten['ICONS'], ($DS % 2));

        $Form->Erstelle_ListenFeld('HRT_SUCH', $rsBewerbungen->FeldInhalt('HRT_SUCH'), 20, $FeldBreiten['HRT_SUCH'],
            false, ($DS % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('HRF_FIRMIERUNG', $rsBewerbungen->FeldInhalt('HRF_FIRMIERUNG'), 20,
            $FeldBreiten['HRF_FIRMIERUNG'], false, ($DS % 2), '', '', 'T');
        $Form->Erstelle_ListenFeld('EINGANGSDATUM', $rsBewerbungen->FeldInhalt('EINGANGSDATUM'), 20,
            $FeldBreiten['EINGANGSDATUM'], false, ($DS % 2), '', '', 'T');

        $Form->Erstelle_ListenFeld('BWS_TEXT', $rsBewerbungen->FeldInhalt('BWS_TEXT'), 20, $FeldBreiten['BWS_TEXT'],
            false, ($DS % 2), '', '', 'T');

        if (($Recht39001 & 8) == 8) {
            $SQL = ' Select * from BEWERBERHISTORY ';
            $SQL .= ' where BEH_HRK_KEY=' . $AWIS_KEY1;
            $SQL .= ' and BEH_KEY =' . $rsBewerbungen->FeldInhalt('BWH_KEY');
            $SQL .= ' and BEH_AKTION = \'Status\'';

            $rsHist = $DB->RecordSetOeffnen($SQL);
            $Bild = '';

            if ($rsHist->AnzahlDatensaetze() > 0) {
                $Bild = '/bilder/attach.png';
                $Form->Erstelle_ListenBild('script', 'BWH_HIST',
                    'onclick="ladeDiv(' . $rsBewerbungen->FeldInhalt('BWH_KEY') . ',\'BWH\');"', $Bild, '', ($DS % 2),
                    '', 16, 18, $FeldBreiten['Hist'], 'C');
            } else {
                $Form->Erstelle_ListenFeld('BWH_HIST', '', 0, $FeldBreiten['Hist'], false, ($DS % 2), '', '', 'T', 'C',
                    '');
            }
        }

        $Form->ZeileEnde();

        $rsBewerbungen->DSWeiter();
        $DS++;
    }

    $FeldBreiten['Aktion'] = 100;
    $FeldBreiten['Alt'] = 170;
    $FeldBreiten['Neu'] = 170;
    $FeldBreiten['User'] = 150;
    $FeldBreiten['Userdat'] = 150;
    if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false) {
        $Rand = 20;
    } else {
        $Rand = 10;
    }
    $Breite = $FeldBreiten['Aktion'] + $FeldBreiten['Alt'] + $FeldBreiten['Neu'] + $FeldBreiten['User'] + $FeldBreiten['Userdat'] + $Rand + 20;

    //DIV NOCH INS FRAMEWORK PACKEN!!!
    echo '<div id="Hist_BWH" style="display:none; position:fixed; border:5px outset #81bef7;border-radius:5px; z-index:1; background-color:#81bef7; left:100px;  width:' . $Breite . 'px;">';
    echo '</div>';
}

$Form->Formular_Ende();
echo $Dokument;
?>