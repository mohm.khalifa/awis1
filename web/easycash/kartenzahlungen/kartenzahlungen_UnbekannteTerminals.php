<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
    
   // Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KNZ','%');
	$TextKonserven[]=array('Wort','OffeneMenge');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_DS%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','CSVErzeugen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht31000 = $AWISBenutzer->HatDasRecht(31000);
	
	if($Recht31000==0) //wenn kein recht 
	{
	    awisEreignis(3,1000,'Kartenzahlungen',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}
	$Form->Formular_Start();
	
	
	if (isset($_GET['TerminalID']))
	{
	    $SQL = 'Select knz_Terminalid, knz_datum 
	            from kartenzahlungen 
	            where knz_terminalid = :var_N0_knz_terminalid
	            order by knz_datum desc';
	    
	    $DB->SetzeBindevariable('KNZ', 'var_N0_knz_terminalid', $_GET['TerminalID'], awisDatenbank::VAR_TYP_TEXT);
	    
	    $rsKNZ = $DB->RecordsetOeffnen($SQL,$DB->Bindevariablen('KNZ',true));
	    

	    
	    $FeldBreiten = array();
	    $FeldBreiten['KNZ_TERMINALID'] = 120;
	    $FeldBreiten['KNZ_DATUM'] = 150;
	    
	    $Form->ZeileStart();

	    $Felder = array();
	    $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./kartenzahlungen_Main.php?cmdAktion=UnbekannteTerminals' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=T title=' . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
	    $Form->InfoZeile($Felder, '');
	    
	    $Link = './kartenzahlungen_Main.php?cmdAktion=UnbekannteTerminals'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	    $Link .= '&Sort=KNZ_TERMINALID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_TERMINALID'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'].':',$FeldBreiten['KNZ_TERMINALID'],'',$Link);
	    
	    $Link = './kartenzahlungen_Main.php?cmdAktion=UnbekannteTerminals'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	    $Link .= '&Sort=LETZTEBUCHUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LETZTEBUCHUNG'))?'~':'');
	    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_LETZTEBUCHUNG'].':',$FeldBreiten['KNZ_DATUM'],'',$Link);
	    
	    $DS=1;
	    $Form->ZeileEnde();
    
	    while (!$rsKNZ->EOF())
	    {
	        $Form->ZeileStart();
	        
	        $Form->Erstelle_ListenFeld('!KNZ_TERMINALID',$rsKNZ->FeldInhalt('KNZ_TERMINALID'),4,$FeldBreiten['KNZ_TERMINALID'],false,($DS%2),'');
	        $Form->Erstelle_ListenFeld('!KNZ_DATUM',$rsKNZ->FeldInhalt('KNZ_DATUM'),24,$FeldBreiten['KNZ_DATUM'],false,($DS%2));
	        
	        $Form->ZeileEnde();
	        
	        $rsKNZ->DSWeiter();
	        $DS++;
	    }
	    
	}
	else //Liste anzeigen
	{
	    if (isset($_GET['Sort']))
	    {
	        if(str_replace('~','',$_GET['Sort']) != 'KNZ_CHECKED')
	        {
	            $Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
	        }
	        $ORDERBY = ' ORDER BY '.$Param['ORDER'];
	    }
	    
	    
	    //********************************************************
	    // Daten suchen
	    //********************************************************
	    
	    
	    $SQL = 'SELECT
             knz_terminalid, max(KNZ_DATUM) as LetzteBuchung
             FROM
             kartenzahlungen
             WHERE
             KNZ_FILNR IS NULL
             OR knz_filnr = \'0\' group by knz_terminalid
	    ';
	    
	    
	    // Zeilen begrenzen
	    $Form->DebugAusgabe(1,$SQL);
	    $rsKNZ = $DB->RecordsetOeffnen($SQL);
	    
	    //********************************************************
	    // Daten anzeigen
	    //********************************************************
	    
	    
	    $FeldBreiten = array();
	    $FeldBreiten['KNZ_TERMINALID'] = 120;
	    $FeldBreiten['LETZTEBUCHUNG'] = 150;
	    
	    if($rsKNZ->AnzahlDatensaetze() > 0)
	    {  	
	        
            $Form->ZeileStart();
            	
            $Link = './kartenzahlungen_Main.php?cmdAktion=UnbekannteTerminals'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=KNZ_TERMINALID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KNZ_TERMINALID'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_TERMINALID'].':',$FeldBreiten['KNZ_TERMINALID'],'',$Link);
    
            $Link = './kartenzahlungen_Main.php?cmdAktion=UnbekannteTerminals'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
            $Link .= '&Sort=LETZTEBUCHUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LETZTEBUCHUNG'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KNZ']['KNZ_LETZTEBUCHUNG'].':',$FeldBreiten['LETZTEBUCHUNG'],'',$Link);
        
	        $DS=1;
	        $Form->ZeileEnde();
	        while(!$rsKNZ->EOF())
	        {
	    
	            $Form->ZeileStart();
	            	
	            $Link = './kartenzahlungen_Main.php?cmdAktion=UnbekannteTerminals&TerminalID=' . $rsKNZ->FeldInhalt('KNZ_TERMINALID');
	            $Form->Erstelle_ListenFeld('!KNZ_TERMINALID',$rsKNZ->FeldInhalt('KNZ_TERMINALID'),4,$FeldBreiten['KNZ_TERMINALID'],false,($DS%2),'',$Link);
	            $Form->Erstelle_ListenFeld('!LETZTEBUCHUNG',$rsKNZ->FeldInhalt('LETZTEBUCHUNG'),24,$FeldBreiten['LETZTEBUCHUNG'],false,($DS%2));
	            	
	            $Form->ZeileEnde();
	            $DS++;
	            $rsKNZ->DSWeiter();
	        }
	    
	        $Form->Formular_Ende();
	    }
	    else
	    {
	        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	    }
	        
	}
	

   
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	
	$Form->SchaltflaechenEnde();

	$Form->Formular_Ende();

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405151651");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405151652");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWIS_KEY2;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';


	if(isset($Param['KNZ_FILNR']) AND $Param['KNZ_FILNR']!='')
	{
		$DB->SetzeBindevariable('KNZ', 'var_N0_knz_filnr', $Param['KNZ_FILNR'], awisDatenbank::VAR_TYP_GANZEZAHL);
		$Bedingung .= ' AND KNZ_FILNR = :var_N0_knz_filnr';
	}
	
	if(isset($Param['KNZ_DATUM_VON']) OR isset($Param['KNZ_DATUM_BIS']))
	{
		if(isset($Param['KNZ_DATUM_BIS']) AND $Param['KNZ_DATUM_BIS'] != '')
		{
			$DB->SetzeBindevariable('KNZ', 'var_D_knz_datum_von', $Param['KNZ_DATUM_VON'], awisDatenbank::VAR_TYP_DATUM);
			$Bedingung .= ' AND TRUNC(KNZ_DATUM) >= :var_D_knz_datum_von';

			$DB->SetzeBindevariable('KNZ', 'var_D_knz_datum_bis', $Param['KNZ_DATUM_BIS'], awisDatenbank::VAR_TYP_DATUM);
			$Bedingung .= ' AND TRUNC(KNZ_DATUM) <= :var_D_knz_datum_bis';
		}
		else
		{
			if($Param['KNZ_DATUM_VON'] != '')
			{
				$DB->SetzeBindevariable('KNZ', 'var_D_knz_datum_von', $Param['KNZ_DATUM_VON'], awisDatenbank::VAR_TYP_DATUM);
				$Bedingung .= ' AND TRUNC(KNZ_DATUM) = :var_D_knz_datum_von';
			}
		}
	}
	
	if(isset($Param['KNZ_BETRAG']) AND $Param['KNZ_BETRAG']!='')
	{
		$DB->SetzeBindevariable('KNZ', 'var_knz_betrag', $Param['KNZ_BETRAG'], awisDatenbank::VAR_TYP_UNBEKANNT);
		$Bedingung .= ' AND KNZ_BETRAG = :var_knz_betrag';
	}
	
	if(isset($Param['KNZ_ART']) AND $Param['KNZ_ART']!='' AND $Param['KNZ_ART']!=1)
	{
		$DB->SetzeBindevariable('KNZ', 'var_T_knz_art', $Param['KNZ_ART'], awisDatenbank::VAR_TYP_TEXT);
		$Bedingung .= ' AND KNZ_ART = :var_T_knz_art';
	}
	
	/*
	if(isset($Param['KNZ_BLZ']) AND $Param['KNZ_BLZ']!='')
	{
		$Bedingung .= ' AND KNZ_BLZ = ' . $DB->FeldInhaltFormat('T',$Param['KNZ_BLZ']);
	}
	*/
	
	if(isset($Param['KNZ_KTONR']) AND $Param['KNZ_KTONR']!='')
	{
		$DB->SetzeBindevariable('KNZ', 'var_T_knz_ktonr', $Param['KNZ_KTONR'].'$', awisDatenbank::VAR_TYP_TEXT);
		$Bedingung .= ' AND REGEXP_LIKE(KNZ_KTONR, :var_T_knz_ktonr)';
	}
	
	if(isset($Param['KNZ_TERMINALID']) AND $Param['KNZ_TERMINALID']!='')
	{
	    $DB->SetzeBindevariable('KNZ', 'var_T_KNZ_TERMINALID', $Param['KNZ_TERMINALID'].'$', awisDatenbank::VAR_TYP_TEXT);
	    $Bedingung .= ' AND REGEXP_LIKE(KNZ_TERMINALID, :var_T_KNZ_TERMINALID)';
	}

	return $Bedingung;
}



?>