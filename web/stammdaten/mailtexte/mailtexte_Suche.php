<?php
global $MVT;
try {

    $MVT->Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./mailtexte_Main.php?cmdAktion=Details>");

    /**********************************************
     * Eingabemaske
     ***********************************************/

    $MVT->Form->Formular_Start();

    $MVT->Form->ZeileStart();
    $MVT->AWISCursorPosition = 'sucMVT_BEREICH';
    $MVT->Form->Erstelle_TextLabel($MVT->AWISSprachKonserven['MVT']['MVT_GRUPPE'] . ':', 190);
    $MVT->Form->Erstelle_TextFeld('MVT_BEREICH', '', 30,300, true);
    $MVT->Form->ZeileEnde();

    $MVT->Form->Formular_Ende();

    $MVT->Form->SchaltflaechenStart();
    $MVT->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $MVT->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $MVT->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $MVT->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    if (($MVT->Recht640 & 4) == 4) {
        $MVT->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $MVT->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $MVT->Form->SchaltflaechenEnde();

    $MVT->Form->SchreibeHTMLCode('</form>');

} catch (awisException $ex) {
    if ($MVT->Form instanceof awisFormular) {
        $MVT->Form->DebugAusgabe(1, $ex->getSQL());
        $MVT->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($MVT->Form instanceof awisFormular) {
        $MVT->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>