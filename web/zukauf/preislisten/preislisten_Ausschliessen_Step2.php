<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;
global $ZPL;
global $Aus;

$GespeicherteFelder = $ZPL->Param['GespeicherteFelder'];
$SQLFelder = array();

//Gespeicherte Felder sind unsortiert im Feld, diese m�ssen nun in eine geordnete Struktur gebracht werden(nach Feld gruppiert).
foreach ($GespeicherteFelder as $Key => $Feld) {

    $Feldtyp = substr($Feld['Feld'], 0, 3);
    $Feldname = substr($Feld['Feld'], 3);
    $Operator = $Feld['Operator'];
    $Wert = $Feld['Wert'];

    $SQLFelder[$Feldname][] = array('Wert' => $Wert, 'Operator' => $Operator);
}

$WHERE = '';
$DebugInfo = '';
foreach ($SQLFelder as $Feldname => $FeldArray) {
    $WHERE .= ' AND (';
    $DebugInfo .= ' AND (';
    $SubWhere = '';
    foreach ($FeldArray as $EinzelFeld) {
        if (strpos($EinzelFeld['Operator'], 'like') !== false) {
            $EinzelFeld['Wert'] = str_replace('*', '%', mb_strtoupper($EinzelFeld['Wert']));
        }
        if($EinzelFeld['Operator']=='<>' or $EinzelFeld['Operator']=='not like'){
            $Verknuepfung = ' AND ';
        }else{
            $Verknuepfung = ' OR ';
        }
        $SubWhere .= $Verknuepfung . $Feldname . ' ' . $EinzelFeld['Operator'] . ' ' . $ZPL->DBi->WertSetzen('ZPL', 'T', $EinzelFeld['Wert']);
        $DebugInfo .= $Verknuepfung . $Feldname . ' ' .$EinzelFeld['Operator'] . ' ' . $EinzelFeld['Wert'];
    }
    $SubWhere = substr($SubWhere, 4);

    $WHERE .= $SubWhere;
    $WHERE .= ')';
    $DebugInfo .= ')';
}

$WHERE = substr($WHERE, 4);
$WHERE = ' WHERE ' . $WHERE;

$SQL = 'Select * From ZUKAUFIMPORT';

$SQL .= $WHERE;

$SQL .= ' AND ZPL_KEY =' . $ZPL->DBi->WertSetzen('ZPL','Z',$AWIS_KEY1);
$CheckBoxEdit = true;
if($ZPL->DBi->ErmittleZeilenAnzahl($SQL,$ZPL->DBi->Bindevariablen('ZPL',false)) > 5000){
    $SQL = 'Select * from (' . $SQL . ') where rownum < 5000';
    $CheckBoxEdit = false;
    $ZPL->Form->Hinweistext($ZPL->AWISSprachKonserven['ZPL']['HINWEIS_ZUVIELEDS'],awisFormular::HINWEISTEXT_WARNUNG);
    $ZPL->Param['ZUVIELEDS'] = true;
}

$ZPL->Param['SQL'] = $SQL;
$ZPL->Param['Bindevariablen'] = $ZPL->DBi->Bindevariablen('ZPL',false);

$rsZPL = $ZPL->DBi->RecordSetOeffnen($SQL,$ZPL->DBi->Bindevariablen('ZPL'));


$ZPL->Form->Formular_Start();
$ZPL->Form->DebugAusgabe(1,$ZPL->DB->LetzterSQL());

$FeldBreiten = array();
$FeldBreiten['SPERRE'] = 30;
$FeldBreiten['HSK'] = 40;
$FeldBreiten['TECDOCID'] = 80;
$FeldBreiten['TECDOCARTNR'] = 100;
$FeldBreiten['BEZEICHNUNG'] = 250;
$FeldBreiten['ATUNR'] = 100;
$FeldBreiten['BESTELLNUMMER'] = 100;

echo '<script> ' . PHP_EOL;
echo ' //Alle anchecken.. ' . PHP_EOL;
echo '$( document ).ready(function() { ' . PHP_EOL;
echo '  $("#txtchkAlle").change(function() { ' . PHP_EOL;
echo '      $(".chkGruppe").prop(\'checked\',  $("#txtchkAlle").prop(\'checked\')); ' . PHP_EOL;
echo '  });  ' . PHP_EOL;
echo '});  ' . PHP_EOL;
echo '</script> ' . PHP_EOL;

$ZPL->Form->ZeileStart();
$ZPL->Form->Erstelle_Textarea('Where','Verkn�pfung ' . PHP_EOL . $DebugInfo,300,30,1,true,'','','','disabled');
$ZPL->Form->ZeileEnde();

$ZPL->Form->ZeileStart();

$ZPL->Form->Erstelle_ListenCheckbox('chkAlle', 1, $FeldBreiten['SPERRE'], true, 1, '', '', '', '', -2);
$ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['HSK'], $FeldBreiten['HSK']);
$ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['TECDOCID'], $FeldBreiten['TECDOCID']);
$ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['TECDOCARTNR'], $FeldBreiten['TECDOCARTNR']);
$ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['BEZEICHNUNG'], $FeldBreiten['BEZEICHNUNG']);
$ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['ATUNR'], $FeldBreiten['ATUNR']);
$ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPL']['BESTELLNUMMER'], $FeldBreiten['BESTELLNUMMER']);
$ZPL->Form->ZeileEnde();

$DS = 0;
while (!$rsZPL->EOF()) {
    $DS++;
    $HG = $DS % 2;
    $ZPL->Form->ZeileStart();

    $Wert = 0;
    if(isset($_POST['txtSPERRE_'.$rsZPL->FeldInhalt('IMP_KEY')])){ //Checkbox kommt im POST mit --> Haken war drin
        $Wert = $_POST['txtSPERRE_'.$rsZPL->FeldInhalt('IMP_KEY')];
    }elseif(isset($_POST['oldSPERRE_'.$rsZPL->FeldInhalt('IMP_KEY')])){ //Old kommt mit, TXT aber nicht --> Haken ist drau�en
        $Wert = 0;
    }else{//Kommt �berhaupt nicht -->Nimm den Wert aus der Datenbank
        $Wert = $rsZPL->FeldInhalt('SPERRE');
    }

    $ZPL->Form->Erstelle_ListenCheckbox('SPERRE_' . $rsZPL->FeldInhalt('IMP_KEY'),$Wert, $FeldBreiten['SPERRE'], $CheckBoxEdit, 1, '', '',
        '', '', $HG, true, '', 'chkGruppe');
    $ZPL->Form->Erstelle_ListenFeld('HSK', $rsZPL->FeldInhalt('HSK'), 100, $FeldBreiten['HSK'], false, $HG);
    $ZPL->Form->Erstelle_ListenFeld('TECDOCID', $rsZPL->FeldInhalt('TECDOCID'), 100, $FeldBreiten['TECDOCID'], false, $HG);
    $ZPL->Form->Erstelle_ListenFeld('TECDOCARTNR', $rsZPL->FeldInhalt('TECDOCARTNR'), 100, $FeldBreiten['TECDOCARTNR'], false, $HG);
    $ZPL->Form->Erstelle_ListenFeld('BEZEICHNUNG', $rsZPL->FeldInhalt('BEZEICHNUNG'), 100, $FeldBreiten['BEZEICHNUNG'], false, $HG);
    $ZPL->Form->Erstelle_ListenFeld('ATUNR', $rsZPL->FeldInhalt('ATUNR'), 100, $FeldBreiten['ATUNR'], false, $HG);
    $ZPL->Form->Erstelle_ListenFeld('BESTELLNUMMER', $rsZPL->FeldInhalt('BESTELLNUMMER'), 100, $FeldBreiten['BESTELLNUMMER'], false, $HG);
    $ZPL->Form->ZeileEnde();

    $rsZPL->DSWeiter();
}

$ZPL->Form->Formular_Ende();

echo '</div>';


$ZPL->Form->SchaltflaechenStart();
$ZPL->Form->Schaltflaeche('image', 'cmdStep_1', '', '/bilder/cmd_dszurueck.png', $ZPL->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
$ZPL->Form->Schaltflaeche('image', 'cmdStep_3', '', '/bilder/cmd_dsweiter.png', $ZPL->AWISSprachKonserven['Wort']['lbl_weiter'], 'W');

$ZPL->Form->SchaltflaechenEnde();


$ZPL->Form->SchreibeHTMLCode('</form>');

?>