<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisBenutzer.inc');

$AWISBenutzer = awisBenutzer::Init();

// Textkonserven laden

$TextKonserven = array();
$TextKonserven [] = array('Wort', 'lbl_speichern');
$TextKonserven [] = array('Wort', 'lbl_abbrechen');

$Form = new awisFormular ();

$Recht9500 = $AWISBenutzer->HatDasRecht(9500);

$DB = awisDatenbank::NeueVerbindung('AWIS');

$DB->Oeffnen();

$Keys = substr($_GET['VVS_KEY'], 0, strlen($_GET['VVS_KEY']) - 1);

$SperrSQL = 'Update Versicherungen_dubletten ';
$SperrSQL .= ' set VDB_STATUS = 555 ';
$SperrSQL .= ' , VDB_USERDAT = sysdate ';
$SperrSQL .= ' where VDB_REF in (' . $Keys . ')';
$SperrSQL .= ' or vdb_vvs_key in ( ' . $Keys . ')';

$DB->Ausfuehren($SperrSQL);

echo $DB->LetzterSQL();
?>

