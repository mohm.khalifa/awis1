<?php
require_once('/daten/include/awis_forms.inc.php');
global $AWISSprache;
global $con;
global $Recht408;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FBA','FBA%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('AST','AST_%');
$TextKonserven[]=array('Wort','Status');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);


$Recht408 = awisBenutzerRecht($con,408,$AWISBenutzer->BenutzerName());
if(($Recht408&8)==8)
{
    awisEreignis(3,1000,'FilialBestand',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

awis_FORM_FormularStart();
echo '<form name=frmFBA action=./artikel_Main.php?cmdAktion=Filialbestandabfragen method=post>';

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FBA']['FBA_AST_ATUNR'].':',150);
awis_FORM_Erstelle_TextFeld('FBA_AST_ATUNR',(isset($_POST['txtFBA_AST_ATUNR'])?$_POST['txtFBA_AST_ATUNR']:''),10,100,true);
awis_FORM_ZeileEnde();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FBA']['FBA_USER'].':',150);
awis_FORM_Erstelle_TextFeld('FBA_USER',(isset($_POST['txtFBA_USER'])?$_POST['txtFBA_USER']:''),30,100,true);
awis_FORM_ZeileEnde();



awis_FORM_FormularEnde();



echo '</form>';




die();

if(0)
{








//********************************************************
// Parameter ?
//********************************************************
if(isset($_POST['cmdSuche_x']))
{
//awis_Debug(1,$_POST);
	$Param = '';
	$Param .= ';'.$_POST['sucFBA_ARTIKELNUMMER'];
	$Param .= ';'.$_POST['sucFBA_ARTIKELBEZEICHNUNG'];
	$Param .= ';'.$_POST['sucFBA_ZLI_KEY'];
	$Param .= ';'.$_POST['sucFBA_ZLH_KEY'];
	$Param .= ';'.$_POST['sucZSZ_SORTIMENT'];
	$Param .= ';'.$_POST['sucAST_ATUNR'];
	$Param .= ';'.$_POST['sucFBA_FIL_ID'];
	$Param .= ';'.$_POST['sucDATUMVOM'];
	$Param .= ';'.$_POST['sucDATUMBIS'];

	awis_BenutzerParameterSpeichern($con, "ZukaufBestellungen" , $AWISBenutzer->BenutzerName() , $Param );
	awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() , '');
}
elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	$Param = ';;;;;;;;;';
	include('./zukaufbestellungen_loeschen.php');
	if($AWIS_KEY1==0)
	{
		$Param = awis_BenutzerParameter($con, "ZukaufBestellungen" , $AWISBenutzer->BenutzerName());
	}
	else 
	{
		$Param = $AWIS_KEY1;
	}
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	$Param = ';;;';
	include('./zukaufbestellungen_speichern.php');
	$Param = awis_BenutzerParameter($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName());
}
elseif(isset($_POST['cmdDSNeu_x']))
{
	$Param = '-1;;;';
}
elseif(isset($_GET['FBA_KEY']))
{
	$Param = ''.$_GET['FBA_KEY'].';;;';		// Nur den Key speiechern
}
elseif(isset($_POST['txtFBA_KEY']))
{
	$Param = ''.$_POST['txtFBA_KEY'].';;;';		// Nur den Key speiechern
}
else 		// Nicht �ber die Suche gekommen, letzte Adresse abfragen
{
	$Param='';
	if(!isset($_GET['Liste']))
	{
		$Param = awis_BenutzerParameter($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName());
	}
	else
	{
		awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() , '');
	}
	if($Param=='' OR $Param=='0')
	{
		$Param = awis_BenutzerParameter($con, 'ZukaufBestellungen', $AWISBenutzer->BenutzerName());
	}
}

//********************************************************
// Daten suchen
//********************************************************
$Bedingung = '';
$Param = explode(';',$Param);

// Name angegeben?
if($Param[0]!='')		// Key
{
	$Bedingung .= 'AND FBA_Key = ' . intval($Param[0]) . ' ';
}
if(isset($Param[1]) AND $Param[1]!='')
{
	$Bedingung .= 'AND (UPPER(FBA_ARTIKELNUMMER) ' . awisLIKEoderIST($Param[1].'%',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[2]) AND $Param[2]!='')
{
	$Bedingung .= 'AND (UPPER(FBA_ARTIKELBEZEICHNUNG) ' . awisLIKEoderIST($Param[2].'%',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[3]) AND $Param[3]>0)
{
	$Bedingung .= 'AND FBA_ZLI_KEY = ' . intval($Param[3]) . ' ';
}
if(isset($Param[4]) AND $Param[4]>0)
{
	$Bedingung .= 'AND FBA_ZLH_KEY = ' . intval($Param[4]) . ' ';
}
if(isset($Param[5]) AND $Param[5]>0)
{
	$Bedingung .= 'AND FBA_ZSZ_KEY= ' . intval($Param[5]) . ' ';
}
if(isset($Param[6]) AND $Param[6]!='')
{
	$Bedingung .= 'AND (UPPER(ZLA_AST_ATUNR) ' . awisLIKEoderIST($Param[6].'%',1) . ' ';
	$Bedingung .= ')';
}
if(isset($Param[7]) AND $Param[7]!='')
{
	$Bedingung .= 'AND FBA_FIL_ID = 0' . intval($Param[7]) . ' ';
}
if(isset($Param[8]) AND $Param[8]!='')
{
	$Bedingung .= 'AND FBA_BESTELLDATUM >= ' . awisFeldFormat('DU',$Param[8]) . ' ';
}
if(isset($Param[9]) AND $Param[9]!='')
{
	$Bedingung .= 'AND FBA_BESTELLDATUM <= ' . awisFeldFormat('DU',$Param[9]) . ' ';
}

$SQL = 'SELECT *';
$SQL .= ' FROM ZukaufBestellungen ';
$SQL .= ' LEFT OUTER JOIN vZukaufArtikel ON FBA_ArtikelNummer = ZLA_Artikelnummer AND FBA_HERSTELLER = ZLH_KUERZEL AND FBA_ZLI_KEY = ZLA_ZLI_KEY';
$SQL .= ' LEFT OUTER JOIN TEMOT_BESTELL@FILSYS.ATU.DE FILSYS ON BESTELL_KEY = FBA_KEY';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,3);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY FBA_BESTELLDATUM DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
awis_Debug(1,$SQL);
if(awisExecute($con, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'")===false)
{
	echo '####';
}

$rsZUB = awisOpenRecordset($con, $SQL);
$rsZUBZeilen = $awisRSZeilen;

//********************************************************
// Daten anzeigen
//********************************************************
if($rsZUBZeilen==0 AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
{
	echo '<span class=HinweisText>'.$AWISSprachKonserven['Fehler']['err_keineDaten'].'</span>';
}
elseif($rsZUBZeilen>1)						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=FBA_BESTELLDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FBA_BESTELLDATUM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['FBA_BESTELLDATUM'],160,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=FBA_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FBA_WANR'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['FBA_WANR'],130,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=FBA_BESTELLMENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FBA_BESTELLMENGE'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['FBA_BESTELLMENGE'],90,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=FBA_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FBA_ARTIKELNUMMER'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['FBA_ARTIKELNUMMER'],180,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],60,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=FBA_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FBA_ARTIKELBEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['FBA_ARTIKELBEZEICHNUNG'],390,'',$Link);
	$Link = './zukaufbestellungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
	$Link .= '&Sort=FBA_PREISBRUTTO'.((isset($_GET['Sort']) AND ($_GET['Sort']=='FBA_PREISBRUTTO'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUB']['FBA_PREISBRUTTO'],90,'',$Link);
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],60,'','',$AWISSprachKonserven['ZUB']['ttt_ZUBSTATUS']);
	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises bl�ttern
	if($rsZUBZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsZUBZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./zukaufbestellungen_Main.php?cmdAktion=Details&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}
 
	for($ZUBZeile=$StartZeile;$ZUBZeile<$rsZUBZeilen and $ZUBZeile<$StartZeile+$MaxDSAnzahl;$ZUBZeile++)
	{
		$Status = '';
		awis_FORM_ZeileStart();

		$Link = './zukaufbestellungen_Main.php?cmdAktion=Details&FBA_KEY=0'.$rsZUB['FBA_KEY'][$ZUBZeile];
		awis_FORM_Erstelle_ListenFeld('FBA_BESTELLDATUM',$rsZUB['FBA_BESTELLDATUM'][$ZUBZeile],0,160,false,($ZUBZeile%2),'',$Link,'DU');

		$Link = '';
		awis_FORM_Erstelle_ListenFeld('FBA_WANR',$rsZUB['FBA_WANR'][$ZUBZeile],0,130,false,($ZUBZeile%2),'',$Link,'T');

		$Link = ''; 
		awis_FORM_Erstelle_ListenFeld('FBA_BESTELLMENGE',$rsZUB['FBA_BESTELLMENGE'][$ZUBZeile],0,90,false,($ZUBZeile%2),'',$Link,'N3','R');

		$Link = '../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY=0'.$rsZUB['ZLA_KEY'][$ZUBZeile];
		awis_FORM_Erstelle_ListenFeld('FBA_ARTIKELNUMMER',$rsZUB['FBA_ARTIKELNUMMER'][$ZUBZeile],0,180,false,($ZUBZeile%2),'',$Link);
		
		$Link='';	// TODO: Sp�ter auf die Hersteller
		awis_FORM_Erstelle_ListenFeld('ZLH_KUERZEL',$rsZUB['ZLH_KUERZEL'][$ZUBZeile],0,60,false,($ZUBZeile%2),'',$Link,'T','L',$rsZUB['ZLH_BEZEICHNUNG'][$ZUBZeile]);
		$Link='';
		awis_FORM_Erstelle_ListenFeld('FBA_ARTIKELBEZEICHNUNG',$rsZUB['FBA_ARTIKELBEZEICHNUNG'][$ZUBZeile],0,390,false,($ZUBZeile%2),'',$Link);
		
		$TippText = $AWISSprachKonserven['ZUB']['FBA_PREISANGELIEFERT'].':'.awisFeldFormat('N2',$rsZUB['FBA_PREISANGELIEFERT'][$ZUBZeile]);
		$Link='';
		
		awis_FORM_Erstelle_ListenFeld('FBA_PREISBRUTTO',$rsZUB['FBA_PREISBRUTTO'][$ZUBZeile],0,90,false,($ZUBZeile%2),'',$Link,'N2','',$TippText);
		
		//****************************************
		// Status
		//****************************************
		// Abweichungen berechnen
		$Abweichung = (awisFeldFormat('N2',$rsZUB['FBA_PREISBRUTTO'][$ZUBZeile]) - awisFeldFormat('N2',$rsZUB['FBA_PREISANGELIEFERT'][$ZUBZeile]))/awisFeldFormat('N2',$rsZUB['FBA_PREISANGELIEFERT'][$ZUBZeile]);
		if(abs($Abweichung) > 0.3)
		{
			$Status .= 'P';
		}
		elseif(abs($Abweichung) > 0.2)
		{
			$Status .= 'p';
		}
		// Gibt es einen ATU Artikel
		if($rsZUB['ZLA_AST_ATUNR'][$ZUBZeile]!='')
		{
			$Status .= 'A';
		}
		// Ist die Bestellung bei den Filialen zu sehen?
		if($rsZUB['TEMOT_NR'][$ZUBZeile]=='')
		{
			$Status .= '!';
		}
		$TippText = '';
		awis_FORM_Erstelle_ListenFeld('STATUS',$Status,0,60,false,($ZUBZeile%2),'','','T','',$TippText);
		
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}			// Eine einzelne Adresse
else										// Eine einzelne oder neue Bestellung
{
	echo '<form name=frmZukaufBestellungen action=./zukaufbestellungen_Main.php?cmdAktion=Details method=POST>';
	//echo '<table>';
	$AWIS_KEY1 = (isset($rsZUB['FBA_KEY'][0])?$rsZUB['FBA_KEY'][0]:0);

	awis_BenutzerParameterSpeichern($con, "AktuellerZUB" , $AWISBenutzer->BenutzerName() , $AWIS_KEY1);

	echo '<input type=hidden name=txtFBA_KEY value='.$AWIS_KEY1. '>';

	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufbestellungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZUB['FBA_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsZUB['FBA_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');

	$EditRecht=(($Recht408&2)!=0);

	if($AWIS_KEY1==0)
	{
		$EditRecht=($Recht408&6);
	}

	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_ZLI_KEY'].':',200);
	if($EditRecht)
	{
		$CursorFeld='txtFBA_ZLI_KEY';
	}
	$SQL = 'SELECT ZLI_KEY, ZLI_BEZEICHNUNG FROM ZUKAUFLIEFERANTEN';
	$SQL .= ' ORDER BY ZLI_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('FBA_ZLI_KEY',($AWIS_KEY1===0?'':$rsZUB['FBA_ZLI_KEY'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_EXTERNEID'].':',190);
	awis_FORM_Erstelle_TextFeld('FBA_EXTERNEID',($AWIS_KEY1===0?'':$rsZUB['FBA_EXTERNEID'][0]),20,200,false,'','','','T');
	awis_FORM_ZeileEnde();

		// Bestellinformationen
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_BESTELLDATUM'].':',200);
	awis_FORM_Erstelle_TextFeld('FBA_BESTELLDATUM',($AWIS_KEY1===0?'':$rsZUB['FBA_BESTELLDATUM'][0]),20,200,$EditRecht,'','','','DU');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_WANR'].':',190);
	awis_FORM_Erstelle_TextFeld('FBA_WANR',($AWIS_KEY1===0?'':$rsZUB['FBA_WANR'][0]),20,200,$EditRecht,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_FIL_ID'].':',190);
	awis_FORM_Erstelle_TextFeld('FBA_FIL_ID',($AWIS_KEY1===0?'':$rsZUB['FBA_FIL_ID'][0]),20,200,$EditRecht,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_VERSANDARTBEZ'].':',200);
	awis_FORM_Erstelle_TextFeld('FBA_VERSANDARTBEZ',($AWIS_KEY1===0?'':$rsZUB['FBA_VERSANDARTBEZ'][0]),20,200,$EditRecht,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_BESTELLMENGE'].':',200);
	awis_FORM_Erstelle_TextFeld('FBA_BESTELLMENGE',($AWIS_KEY1===0?'':$rsZUB['FBA_BESTELLMENGE'][0]),10,200,$EditRecht,'','','','N3');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_MENGENEINHEITEN'].':',190);
	awis_FORM_Erstelle_TextFeld('FBA_MENGENEINHEITEN',($AWIS_KEY1===0?'':$rsZUB['FBA_MENGENEINHEITEN'][0]),10,200,$EditRecht,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_EINHEITENPROPREIS'].':',190);
	awis_FORM_Erstelle_TextFeld('FBA_EINHEITENPROPREIS',($AWIS_KEY1===0?'':$rsZUB['FBA_EINHEITENPROPREIS'][0]),10,200,$EditRecht,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_PREISBRUTTO'].':',200);
	awis_FORM_Erstelle_TextFeld('FBA_PREISBRUTTO',($AWIS_KEY1===0?'':$rsZUB['FBA_PREISBRUTTO'][0]),10,200,$EditRecht,'','','','N2');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_PREISANGELIEFERT'].':',190);
	awis_FORM_Erstelle_TextFeld('FBA_PREISANGELIEFERT',($AWIS_KEY1===0?'':$rsZUB['FBA_PREISANGELIEFERT'][0]),10,200,$EditRecht,'','','','N2');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_WAEHRUNG'].':',190);
	awis_FORM_Erstelle_TextFeld('FBA_WAEHRUNG',($AWIS_KEY1===0?'':$rsZUB['FBA_WAEHRUNG'][0]),10,200,$EditRecht,'','','','T');
	awis_FORM_ZeileEnde();

		// Nummer und Hersteller
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_ARTIKELNUMMER'].':',200);
	awis_FORM_Erstelle_TextFeld('FBA_ARTIKELNUMMER',($AWIS_KEY1===0?'':$rsZUB['FBA_ARTIKELNUMMER'][0]),20,200,$EditRecht);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_ZLH_KEY'].':',190);
	$SQL = 'SELECT ZLH_KEY, ZLH_BEZEICHNUNG FROM ZUKAUFLIEFERANTENHERSTELLER';
	$SQL .= ' ORDER BY ZLH_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('ZLH_KEY',($AWIS_KEY1===0?'':$rsZUB['ZLH_KEY'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_ZeileEnde();

		// Bezeichnung
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_ARTIKELBEZEICHNUNG'].':',200);
	awis_FORM_Erstelle_TextFeld('FBA_ARTIKELBEZEICHNUNG',($AWIS_KEY1===0?'':$rsZUB['FBA_ARTIKELBEZEICHNUNG'][0]),50,200,$EditRecht);
	awis_FORM_ZeileEnde();

		// ATU Daten
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_SORTIMENT'].':',200);
	$SQL = 'SELECT DISTINCT ZSZ_SORTIMENT AS KEY, ZSZ_SORTIMENT AS SORTIMENT FROM ZUKAUFSORTIMENTZUORDNUNGEN';
	$SQL .= ' WHERE ZSZ_ZLI_KEY = 0'.(isset($rsZUB['FBA_ZLI_KEY'][0])?$rsZUB['FBA_ZLI_KEY'][0]:'1');
	$SQL .= ' ORDER BY ZSZ_SORTIMENT';
	awis_FORM_Erstelle_SelectFeld('FBA_SORTIMENT',($AWIS_KEY1===0?'':$rsZUB['FBA_SORTIMENT'][0]),200,$EditRecht,$con,$SQL);
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZUB']['FBA_TAUSCHTEIL'].':',190);
	$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
	awis_FORM_Erstelle_SelectFeld('FBA_TAUSCHTEIL',($AWIS_KEY1===0?'':$rsZUB['FBA_TAUSCHTEIL'][0]),200,$EditRecht,null,'','','',1,'',$Daten);
	awis_FORM_ZeileEnde();

	if(isset($rsZUB['ZLA_AST_ATUNR'][0]) AND $rsZUB['ZLA_AST_ATUNR'][0]!='')
	{
		awis_FORM_Trennzeile();

		$SQL = 'SELECT Artikelstamm.* ';
		$SQL .= ',(SELECT ATW_BETRAG FROM Altteilwerte WHERE ATW_KENNUNG = (SELECT ASI_WERT FROM ArtikelStammInfos WHERE asi_ast_atunr=\''.$rsZUB['ZLA_AST_ATUNR'][0].'\' AND asi_ait_id=190) AND ATW_LAN_CODE=\'DE\') AS AST_ALTTEILWERT';
		$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos WHERE asi_ast_atunr=\''.$rsZUB['ZLA_AST_ATUNR'][0].'\' AND asi_ait_id=191) AS AST_VPE';
		$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR='.awisFeldFormat('T',$rsZUB['ZLA_AST_ATUNR'][0]);
		$rsAST = awisOpenRecordset($con,$SQL);
		if($awisRSZeilen==0)
		{
		}
		else 
		{
			awis_FORM_ZeileStart();
			$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsAST['AST_ATUNR'][0].'&cmdAktion=ArtikelInfos';
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'');
			awis_FORM_Erstelle_TextFeld('AST_ATUNR',$rsAST['AST_ATUNR'][0],20,200,false,'','',$Link);
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_BEZEICHNUNGWW',$rsAST['AST_BEZEICHNUNGWW'][0],20,500,false);
			awis_FORM_ZeileEnde();
			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_VK',$rsAST['AST_VK'][0],20,200,false,'','','','N2');
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ALTTEILWERT'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_ALTTEILWERT',$rsAST['AST_ALTTEILWERT'][0],20,200,false,'','','','T');
			awis_FORM_ZeileEnde();
			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VPE'].':',200);
			awis_FORM_Erstelle_TextFeld('AST_VPE',$rsAST['AST_VPE'][0],20,200,false,'','','','N0');
			awis_FORM_ZeileEnde();
		}
		
	}
	
	
	awis_FORM_FormularEnde();


	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht408&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if(($Recht408&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		awis_FORM_Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/plus.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht408&8)!==0 AND !isset($_POST['cmdDSNeu_x']))
	{
		awis_FORM_Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/Muelleimer_gross.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}	
	awis_FORM_SchaltflaechenEnde();

	
	echo '</form>';
}
}
//awis_Debug(1, $Param, $Bedingung, $rsZUB, $_POST, $rsAZG, $SQL, $AWISSprache);

if($CursorFeld!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$CursorFeld."\")[0].focus();";
	echo '</Script>';
}