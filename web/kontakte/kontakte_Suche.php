<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20091215
 *
 *
 */
global $AWISCursorPosition;			// Zum Setzen des Cursors �ber Java-Script (in *_Main.php)


try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	$AWISBenutzer = awisBenutzer::Init();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KON','%');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ALLE_NULL');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht2200=$AWISBenutzer->HatDasRecht(2200);		// Kontakte Rechte
	$Recht2201=$AWISBenutzer->HatDasRecht(2201);		// Kontakte-Optionen
	if($Recht2200==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./kontakte_Main.php?cmdAktion=Details>");

	/**********************************************
	* Eingabemaske
	***********************************************/
	$Form->Formular_Start();


	// Artikelnummer
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['KON']['SUCHFELD'].':',160);
	$Form->Erstelle_TextFeld('*SUCHFELD','',30,150,true,'','','','T','L');
	$AWISCursorPosition=($AWISCursorPosition==''?'sucSUCHFELD':$AWISCursorPosition);
	$Form->ZeileEnde();

	$Form->Trennzeile();
	
	//
	// Suche nach Personalnummer nur Empfang
	//
	if(($Recht2201&1)!=0)
	{
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KON']['KON_PER_NR'].':',160);
		$Form->Erstelle_TextFeld('*KON_PER_NR','',10,150,true,'','','','T','L');
		$Form->ZeileEnde();
	}

	//
	// Weitere Suchparameter aus den Infos
	//
	$SQL = 'SELECT DISTINCT informationstypen.* ';
	$SQL .= ' FROM informationstypen';
	$SQL .= ' INNER JOIN V_ACCOUNTRECHTE ON XBA_XRC_ID = ITY_XRC_ID';
	$SQL .= ' WHERE ity_xtn_kuerzel = \'KON\'';
	$SQL .= ' AND ity_suchfeld = 1';
	$SQL .= ' AND BITAND(ITY_RECHTESTUFE,XBA_STUFE)=ITY_RECHTESTUFE';
	$rsITY = $DB->RecordSetOeffnen($SQL);

	if(!$rsITY->EOF())
	{
		$Form->Trennzeile();
	}
	while(!$rsITY->EOF())
	{
		$Recht = $AWISBenutzer->HatDasRecht($rsITY->FeldInhalt('ITY_XRC_ID'));
		if(($Recht&$rsITY->FeldInhalt('ITY_RECHTESTUFE'))==$rsITY->FeldInhalt('ITY_RECHTESTUFE'))
		{
			$Label = $Form->LadeTextBaustein('Infofeld','ity_lbl_'.$rsITY->FeldInhalt('ITY_KEY'));
			$Tooltipptext = $Form->LadeTextBaustein('Infofeld','ity_ttt_'.$rsITY->FeldInhalt('ITY_KEY'));
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Label.':',160,'','',$Tooltipptext);
			
			$FeldTyp = substr($rsITY->FeldInhalt('ITY_DATENQUELLE'),0,3); 
			switch($FeldTyp)
			{
				case '':
					$Form->Erstelle_TextFeld('*ITY_'.$rsITY->FeldInhalt('ITY_KEY'),'',10,150,true,'','','','T','L');
					break;
				case 'TXT':
					$DatenFelder = explode(':',$rsITY->FeldInhalt('ITY_DATENQUELLE'));
					$Daten = explode('|',$Form->LadeTextBaustein($DatenFelder[1],$DatenFelder[2]));
					$Daten[] = $AWISSprachKonserven['Liste']['lst_ALLE_NULL'];
					$Form->Erstelle_SelectFeld('*ITY_'.$rsITY->FeldInhalt('ITY_KEY'),$rsITY->FeldInhalt('ITY_WERT'),$rsITY->FeldInhalt('ITY_BREITE'),true,'','','','','',$Daten);
					break;
				case 'SQL':
					$DatenFelder = explode(':',$rsITY->FeldInhalt('ITY_DATENQUELLE'));
					$Daten = array($AWISSprachKonserven['Liste']['lst_ALLE_NULL']);
					$Form->Erstelle_SelectFeld('*ITY_'.$rsITY->FeldInhalt('ITY_KEY'),$rsITY->FeldInhalt('ITY_WERT'),$rsITY->FeldInhalt('ITY_BREITE'),true,$DatenFelder[1],'','','','',$Daten);
					break;
			}
			$Form->ZeileEnde();
		}
		$rsITY->DSWeiter();
	}
	
	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

	if((($Recht2200&4)!=0))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200921041807");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',3,"200921041808");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>