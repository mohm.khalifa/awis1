<?php
require_once('fpdf_personal.php');
require_once('fpdi.php');
define('FPDF_FONTPATH','/daten/include/font/');

class awisAusdruck
{
	/**
	 * PDF Objekt
	 *
	 * @var fpdi
	 */
	var $_pdf = null;

	/**
	 * Ausrichtung der Seite
	 *
	 * @var string
	 */
	var $_Ausrichtung = 'P';		// P/L


	/**
	 * Papierformat als Text (A4, A3)
	 *
	 * @var string
	 */
	var $_Papiergroesse = 'A4';

	/**
	 * Vorlagen f�r die Seiten
	 *
	 * @var array
	 */
	var $_Vorlagen = array();

	/**
	 * Auszugebender Dateiname
	 *
	 * @var string
	 */
	var $_Dateiname = '';

	/**
	 * Pfad zu den AWIS Dateivorlagen
	 *
	 * @var string
	 */
	var $_VorlagenPfad = '/daten/web/dokumente/vorlagen/';

	/**
	 * Initialisierung.
	 * Vorlagen: ein array mit Vorlagen f�r die einzelnen Seiten (0=Erste Seite, 1=zweite Seite,...).
	 * 			wenn keine Vorlagen mehr da sind, wird f�r alle folgenden Seiten die Vorlage verwendet.
	 *
	 * @param string $Ausrichtung(P/L)
	 * @param string $Papiergroesse(A3/A4)
	 * @param array $Vorlagen
	 * @return awisAusdruck
	 */

	//public __construct($Ausrichtung = 'P', $Papiergroesse='A4' array $Vorlagen)
	function awisAusdruck($Ausrichtung = 'P', $Papiergroesse='A4', $Vorlagen, $Titel='awis')
	{
		if(array_search($Ausrichtung,array('P','L'))===false)
		{
			//throw new exception('Falsche Ausrichtung. Nur L oder P.',200813031337);
		}
		if(array_search($Papiergroesse,array('A3','A4'))===false)
		{
			//throw new exception('Falsches Papierformat. Nur A4 oder A3 erlaubt.',200813031337);
		}
		//try
		{
			$this->_Ausrichtung = $Ausrichtung;
			$this->_Papiergroese = $Papiergroesse;

			$this->_pdf = new fpdi($Ausrichtung,'mm',$Papiergroesse);

			$this->_pdf->SetAuthor('ATU Auto-Teile-Unger');
			$this->_pdf->SetCreator('AWIS 3.0');
			$this->_pdf->SetTitle($Titel);
			$this->_pdf->SetFont('Arial');

			$this->_Dateiname = $Titel.'.pdf';

			$this->_pdf->SetAutoPageBreak(false,0);

			// Vorlagen speichern
			if(is_array($Vorlagen))
			{
				$this->_Vorlagen = $Vorlagen;
			}
		}
		/*
		catch (Exception $ex)
		{
			throw new Exception('awisAusdruck:'.$ex->getMessage(), $ex->getCode());
		}
		*/
	}

	/**
	 * Erstellt eine neue Seite mit einer optionalen Vorlage
	 *
	 * @param int $Vorlage (>=0)
	 * @param int $Vorlagenseite (>=1)
	 */
	function NeueSeite($Vorlage = -1, $Vorlagenseite = 0)
	{
		$VorlagenID='';

		// Vorlage vorhanden?
		if($Vorlage>=0 AND isset($this->_Vorlagen[$Vorlage]))
		{
			$this->_pdf->open();
			$Seiten = $this->_pdf->setSourceFile($this->_VorlagenPfad . $this->_Vorlagen[$Vorlage]);

			if($Seiten>0 AND $Seiten>=$Vorlagenseite)
			{
				$VorlagenID = $this->_pdf->ImportPage($Vorlagenseite);
				$Groesse = $this->_pdf->getTemplateSize($VorlagenID);
			}
		}

		$this->_pdf->AddPage($this->_Ausrichtung);

		if($VorlagenID!=='')
		{
			$this->_pdf->useTemplate($VorlagenID,0,0);
		}
	}


	function Anzeigen()
	{
		@ob_clean();
		$this->_pdf->Output($this->_Dateiname,'D');
	}


	/**
	 * Liefert die Seitenbreite in mm
	 *
	 * @return int
	 */
	function SeitenBreite()
	//private function SeitenBreite()
	{
		switch ($this->_Papiergroesse)
		{
			case 'A4':
				return ($this->_Ausrichtung=='P'?210:297);
				break;
		}
	}

	/**
	 * Liefert die Seitenhoehe in mm
	 *
	 * @return int
	 */
	function SeitenHoehe()
	//private function SeitenBreite()
	{
		switch ($this->_Papiergroesse)
		{
			case 'A4':
				return ($this->_Ausrichtung=='P'?297:210);
				break;
		}
	}

	/**
	 * Setzt oder liest den aktuellen Vorlagenpfad
	 *
	 * @param string $NeuerPfad
	 * @return string
	 */
	function VorlagenPfad($NeuerPfad)
	{
		if($NeuerPfad!='')
		{
			if(is_dir($NeuerPfad))
			{
				$this->_VorlagenPfad=$NeuerPfad;
			}
		}
		return $this->_VorlagenPfad;
	}
}
?>