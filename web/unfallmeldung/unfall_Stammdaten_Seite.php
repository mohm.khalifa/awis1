<?php
// require_once('unfall_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;				// UnfallMeldungenInfoTypen: UFT_BEREICHID
global $AWIS_KEY2;				// UnfallMeldungenInfoTypen: UFT_KEY
global $Aktion;					// Merker/Wert f�r GET: cmdAktion (Reiter)
global $Seite;					// Merker/Wert f�r GET: Seite (Subreiter)
global $FehlerAugetreten;		// Flag: Beim Speichern ist ein Fehler aufgetreten (z.B. Pflichtfeld nicht gef�llt)
global $ListeAnzeigen;			// Flag: Liste anzeigen (nicht Detailansicht) 

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('UNF', '%');
	$TextKonserven[]=array('Wort', 'lbl_zurueck');
	$TextKonserven[]=array('Wort', 'lbl_trefferliste');
	$TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	// Rechte f�r alle Reiter holen
	$Recht21040 = $AWISBenutzer->HatDasRecht(21040);		// Stammdaten
	$Recht21041 = $AWISBenutzer->HatDasRecht(21041);		// Unfallbereich
	$Recht21042 = $AWISBenutzer->HatDasRecht(21042);		// Besch�ftigungsart
	$Recht21043 = $AWISBenutzer->HatDasRecht(21043);		// Unfallart
	$Recht21052	= $AWISBenutzer->HatDasRecht(21052);		// Artspezifikation
	$Recht21044 = $AWISBenutzer->HatDasRecht(21044);		// K�rperteile
	$Recht21045 = $AWISBenutzer->HatDasRecht(21045);		// Verletzungsart
	$Recht21046 = $AWISBenutzer->HatDasRecht(21046);		// Schutzausr�stung
	$Recht21047 = $AWISBenutzer->HatDasRecht(21047);		// Unfallort
	$Recht21050 = $AWISBenutzer->HatDasRecht(21050);		// Unfallkategorie
	$Recht21048 = $AWISBenutzer->HatDasRecht(21048);		// Unfallursache
	$Recht21051 = $AWISBenutzer->HatDasRecht(21051);		// Ursachenspezifikation
	$Recht21049 = $AWISBenutzer->HatDasRecht(21049);		// Unfallursache T�V
	
	if($Recht21040 == 0)		// Recht f�r Stammdaten allgemein
	{
	    $Form->Fehler_KeineRechte();
	}
	
	// wenn nicht Liste anzeigen => UFT_KEY (=AWIS_KEY2) holen
	if ((!$ListeAnzeigen) or (empty($ListeAnzeigen))) 
	{
		$AWIS_KEY2 = (isset($_POST['txtUFT_KEY'])? $_POST['txtUFT_KEY']: (isset($_GET['UFT_KEY'])? $_GET['UFT_KEY']: 0));
	}
	
	// Den ersten Bereich merken , der berechtigt ist,
	// da keine GET-Seite-Variable beim ersten Aufruf mitkommt
	// (bei GET: w�rde die Seite-Variable gesetzt werden, vgl. n�chstes SWITCH).
	if ($Recht21041 != 0)		// Recht f�r Unfallbereich 
	{
		$AWIS_KEY1 = 1;
	}
	elseif ($Recht21042 != 0)	// Recht f�r Besch�ftigungsart
	{
		$AWIS_KEY1 = 2;
	}
	elseif ($Recht21043 != 0)	// Recht f�r Unfallart
	{
		$AWIS_KEY1 = 3;
	}
	elseif ($Recht21044 != 0)	// Recht f�r K�rperteile
	{
		$AWIS_KEY1 = 4;
	}
	elseif ($Recht21045 != 0)	// Recht f�r Verletzungsart
	{
		$AWIS_KEY1 = 5;
	}
	elseif ($Recht21046 != 0)	// Recht f�r Schutzausr�stung
	{
		$AWIS_KEY1 = 6;
	}
	elseif ($Recht21047 != 0)	// Recht f�r Unfallort
	{
		$AWIS_KEY1 = 7;
	}
	elseif ($Recht21048 != 0)	// Recht f�r Unfallursache
	{
		$AWIS_KEY1 = 8;	
	} 
	elseif ($Recht21049 != 0)	// Recht f�r Unfallursache T�V
	{
		$AWIS_KEY1 = 9;
	}
	elseif ($Recht21050 != 0)	// Recht f�r Unfallkategorie
	{
		$AWIS_KEY1 = 10;		
	}	
	elseif ($Recht21051 != 0)	// Recht f�r Ursachenspezifikation
	{
		$AWIS_KEY1 = 11;
	}	
	elseif ($Recht21052 != 0)	// Recht f�r Artspezifikation
	{
		$AWIS_KEY1 = 12;
	}		
	else
	{
		$AWIS_KEY1 = 0;
	}
	
	switch ($Seite)
	{
		case 'Bereich':				// Unfallbereich
			if($Recht21041 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 1;
		break;
		case 'Beschaeftigung':		// Besch�ftigungsart
			if($Recht21042 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 2;
		break;		
		case 'Art':					// Unfallart
			if($Recht21043 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 3;
		break;	
		case 'Koerperteile':		// K�rperteile
			if($Recht21044 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 4;
		break;		
		case 'Verletzung':			// Verletzungsart
			if($Recht21045 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 5;
		break;		
		case 'Ausruestung':			// Schutzausr�stung
			if($Recht21046 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 6;
		break;		
		case 'Ort':					// Unfallort
			if($Recht21047 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 7;
		break;
		case 'Ursache':				// Unfallursache
			if($Recht21048 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 8;
		break;
		case 'UrsacheTUEV':			// Unfallursache T�V
			if($Recht21049 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 9;
		break;
		case 'Kategorie':			// Unfallkategorie
			if($Recht21050 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 10;
		break;		
		case 'UrsacheSpez':			// Ursachenspezifikation
			if($Recht21051 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 11;
		break;		
		case 'ArtSpez':				// Artspezifikation
			if($Recht21052 == 0)
			{
			    $Form->Fehler_KeineRechte();
			}
			$AWIS_KEY1 = 12;
		break;	
		default:
			;
		break;
	}

	// Aktive Daten zum jeweiligen Bereich holen
	$SQL = 'select uft.*, uft_self.uft_information as gehoertzu ' .
		   'from unfallmeldunginfotypen uft ' .
		   'left join unfallmeldunginfotypen uft_self ' .  		// self join
		   'on uft_self.uft_key = uft.uft_gehoertzuuftkey ' . 	// �bergeordneter Kategoriekey
		   'where uft.uft_bereichid = :var_UftBereichId ' .		// nur im aktuellen Stammdatenbereich
		   'and uft.uft_ueberschrift = 0 ' .					// nur Stammdatentypen, keine Stammdatentypen-�berschriften
		   'and uft.uft_status = 1';							// nur aktive Datens�tze	
	$BindeVariablen = array();	
	if ($AWIS_KEY2 > 0)										
	{														// wenn UTF_KEY vorhanden
		$SQL .= ' and uft.uft_key = :var_UftKey';			// dann nach UFT_KEY filtern
		$BindeVariablen['var_UftKey'] = $AWIS_KEY2;
	}
	if (isset($_GET['UFTSort']))
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$SQL .= ' order by UFT.' . str_replace('~',' DESC ', $_GET['UFTSort']);
	}
	else 							
	{		// sonst pauschal nach kennung, bei gleicher bzw. leerer Kennung, nach Bezeichnung
		$SQL .= ' order by uft.uft_kennung, uft.uft_information';		
	}
	$BindeVariablen['var_UftBereichId'] = $AWIS_KEY1;
	$rsUFT = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

	// �berschriften und wenn vorhanden (sonst NULL) den �bergeordneten Bereich holen
	$SQL =  'select uft_self.uft_bereich as ueber_uft_bereich, ' .
			'uft_self.uft_bereichid as ueber_uft_bereichid, uft.* ' .
		   	'from unfallmeldunginfotypen uft ' .
				'left join unfallmeldunginfotypen uft_self ' . 		// self join
				'on  uft_self.uft_bereichid = uft.uft_abhaengigvonbereichid ' .		// wenn �bergeordneter Bereich
				'and uft_self.uft_ueberschrift = 1 ' .								// und nur �berschrift 
				'and uft_self.uft_status = 1 ' . 									// und nur aktiv
	       	'where uft.uft_bereichid = :var_UftBereichId ' .		// Stammdatenbereich
	       	'and uft.uft_ueberschrift = 1 ' .					// nur �berschrift
	       	'and uft.uft_status = 1';							// nur aktive Datens�tze
	$BindeVariablen = array();
	$BindeVariablen['var_UftBereichId'] = $AWIS_KEY1;
	$rsUeberschrift = $DB->RecordSetOeffnen($SQL, $BindeVariablen);	
	
	$UeberBereich = $rsUeberschrift->FeldInhalt('UEBER_UFT_BEREICH');	// in Variable schreiben, da (siehe weiter unten) die PHP-Funktion 'empty' bei Funktionsr�ckgaben einen Fehler schmei�t, bei Variablen nicht
	
	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->Formular_Start();
	
	$Form->ZeileStart();		// �berschrift
	$Form->Erstelle_TextLabel($rsUeberschrift->FeldInhalt('UFT_INFORMATION'), 800, 'font-weight:bolder');
	$Form->Trennzeile('O');
	$Form->ZeileEnde();	

	// Detailansicht (Neu/Bearbeiten)
	// - wenn UFT_KEY da => Detailansicht
	// - bei Fehler (z.B. Pflichtfeld ohne Wert) => Detailansicht
	// - bei New oder Edit => Detailansicht
	if ($AWIS_KEY2 > 0 or $FehlerAugetreten == 1 or (isset($_GET['New']) or isset($_GET['Edit'])))			
	{
		$Form->Erstelle_HiddenFeld('UFT_KEY', $AWIS_KEY2);											// Merker f�r Speichern
		$Form->Erstelle_HiddenFeld('UFT_BEREICHID', $rsUeberschrift->FeldInhalt('UFT_BEREICHID'));	// Merker f�r Speichern
		
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => "<a href=./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY2 > 0? $rsUFT->FeldInhalt('UFT_USER'): $AWISBenutzer->BenutzerName()));	// bei Edit => User aus DB; bei New => aktUser 
		$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => ($AWIS_KEY2 > 0? $rsUFT->FeldInhalt('UFT_USERDAT'): date('d.m.Y H:i:s')));			// bei Edit => Dat aus DB; bei New => aktDat
		$Form->InfoZeile($Felder, '');

		// Bearbeitungsrecht setzen
		if ((isset($_GET['New']) or isset($_POST['txtNEW'])) and ($Recht21040 & 4) != 0)
		{	// Post-Variable erstellen und abfragen, damit im Fehlerfall (hier kommt keine Get-Variable) der EditModus richtig gesetzt wird
			$Form->Erstelle_HiddenFeld('NEW', 'New');		
			$EditModus = true;
		}
		elseif ((isset($_GET['Edit']) or isset($_POST['txtEDIT'])) and ($Recht21040 & 2) != 0)
		{	// Post-Variable erstellen und abfragen, damit im Fehlerfall (hier kommt keine Get-Variable) der EditModus richtig gesetzt wird
			$Form->Erstelle_HiddenFeld('EDIT', 'Edit');
			$EditModus = true;
		}
		else 
		{
			$EditModus = false;
		}
		
		$Form->ZeileStart();		// ID (DB: Kennung)
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['KENNUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('UFT_KENNUNG', (isset($_POST['oldUFT_KENNUNG'])? $_POST['oldUFT_KENNUNG']: ($AWIS_KEY2 > 0? $rsUFT->FeldInhalt('UFT_KENNUNG'): '')), 5, 200, $EditModus);	// old-Wert, wegen => vgl unfall_speichern_stammdaten.php => Code bei Kommentar "// ID bereits vorhanden"
		$AWISCursorPosition = ($EditModus? 'txtUFT_KENNUNG': $AWISCursorPosition);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();		// Bezeichnung (DB: Information)
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEZEICHNUNG'] . ':', 150);
		$Form->Erstelle_TextFeld('UFT_BEZEICHNUNG', (isset($_POST['txtUFT_BEZEICHNUNG'])? $_POST['txtUFT_BEZEICHNUNG']: ($AWIS_KEY2 > 0? $rsUFT->FeldInhalt('UFT_INFORMATION'): '')), 50, 800, $EditModus);
		$Form->ZeileEnde();
		
		// wenn �bergeordneter Bereich nicht NULL
		if (! empty($UeberBereich))
		{	
			// Aktive Daten zum �bergeordneten Bereich holen	
			$SQL =  'SELECT uft_key, uft_kennung || \'  \' || uft_information as uft_information ' . 
					'FROM unfallmeldunginfotypen ' .
					'WHERE uft_bereichid  = ' . $rsUeberschrift->FeldInhalt('UEBER_UFT_BEREICHID') . ' ' .	// �bergeordneter Bereich
					'AND uft_ueberschrift = 0 ' .		// nur Stammdatentypen, keine Stammdatentypen-�berschriften
					'AND uft_status       = 1' .		// nur aktive Datens�tze
					'ORDER BY uft_information';
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel('', 150);		// einger�ckt
			$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['UEBERKATEGORIE'] . ':', 600);
			$Form->ZeileEnde();
			$Form->ZeileStart();		// Kategorie / GehoertZuUftKey (DB: self_join.Information / uft_gehoertzuuftkey)
			$Form->Erstelle_TextLabel('', 150);		// einger�ckt
			$Form->Erstelle_SelectFeld('UFT_GEHOERTZU', (isset($_POST['txtUFT_GEHOERTZU'])? $_POST['txtUFT_GEHOERTZU']: ($AWIS_KEY2 > 0? $rsUFT->FeldInhalt('UFT_GEHOERTZUUFTKEY'): '')), 100, true, $SQL, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
			$Form->ZeileEnde();
		}

		$Form->ZeileStart();		// Bemerkung
		$Form->Erstelle_TextLabel($AWISSprachKonserven['UNF']['BEMERKUNG'] . ':', 150);
		$Form->Erstelle_Textarea('UFT_BEMERKUNG', (isset($_POST['txtUFT_BEMERKUNG'])? $_POST['txtUFT_BEMERKUNG']: ($AWIS_KEY2 > 0? $rsUFT->FeldInhalt('UFT_BEMERKUNG'): '')), 800, 39, 5, $EditModus);
		$Form->ZeileEnde();
	}
	else		// Listenansicht 
	{
		$Form->ZeileStart();
		
		$Icons = array();
		if (($Recht21040 & 4) != 0)
		{	// New-Icon anzeigen, wenn berechtigt
			$Icons[] = array('new', "./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite&UFT_KEY=0&New=" . $rsUeberschrift->FeldInhalt('UFT_INFORMATION'));
		}
		$Form->Erstelle_ListeIcons($Icons, 40, -1);
		
		// Spaltenbreiten f�r Listenansicht
		$FeldBreiten = array();
		$FeldBreiten['Kennung'] = 50;
		$FeldBreiten['Bezeichnung'] = 150;
		// wenn �bergeordneter Bereich nicht NULL
		if (! empty($UeberBereich))
		{	
			$FeldBreiten['Bemerkung'] = 370;
			$FeldBreiten['Ueberkategorie'] = 150;
		}
		else 
		{
			$FeldBreiten['Bemerkung'] = 520;
			$FeldBreiten['Ueberkategorie'] = 0;			
		} 

		// �berschrift der Listenansicht mit Sortierungslink: ID
		$Link = "./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite";
		$Link .= '&UFTSort=UFT_KENNUNG'.((isset($_GET['UFTSort']) AND ($_GET['UFTSort']=='UFT_KENNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['KENNUNG'], $FeldBreiten['Kennung'], '', $Link);
		// �berschrift der Listenansicht mit Sortierungslink: Bezeichnung
		$Link = "./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite";
		$Link .= '&UFTSort=UFT_INFORMATION'.((isset($_GET['UFTSort']) AND ($_GET['UFTSort']=='UFT_INFORMATION'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['BEZEICHNUNG'], $FeldBreiten['Bezeichnung'], '', $Link);			
		// �berschrift der Listenansicht mit Sortierungslink: Bemerkung
		$Link = "./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite";
		$Link .= '&UFTSort=UFT_BEMERKUNG'.((isset($_GET['UFTSort']) AND ($_GET['UFTSort']=='UFT_BEMERKUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['BEMERKUNG'], $FeldBreiten['Bemerkung'], '', $Link);
		// wenn �bergeordneter Bereich nicht NULL
		if (! empty($UeberBereich))
		{			
			// �berschrift der Listenansicht mit Sortierungslink: �berkategorie
			$Link = "./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite";
			$Link .= '&UFTSort=GEHOERTZU'.((isset($_GET['UFTSort']) AND ($_GET['UFTSort']=='GEHOERTZU'))?'~':'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['UNF']['KATEGORIE'], $FeldBreiten['Ueberkategorie'], '', $Link);
		}
		
		$Form->ZeileEnde();
		
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsUFT->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if (($Recht21040 & 2) != 0)	// �ndernrecht
			{	// �ndericon anzeigen, wenn berechtigt
				$Icons[] = array('edit', "./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite&UFT_KEY=" . $rsUFT->FeldInhalt('UFT_KEY') . "&Edit=" . $rsUFT->FeldInhalt('UFT_INFORMATION'));
			}
			if (($Recht21040 & 8) != 0)	// L�schenrecht
			{	// L�schicon anzeigen, wenn berechtigt
				$Icons[] = array('delete', "./unfall_Main.php?cmdAktion=$Aktion&Seite=$Seite&UFT_KEY=" . $rsUFT->FeldInhalt('UFT_KEY') . "&Del=" . $rsUFT->FeldInhalt('UFT_INFORMATION'));
			}
			$Form->Erstelle_ListeIcons($Icons, 40, -1); //($DS%2));
			$Form->Erstelle_ListenFeld('*UFT_KENNUNG', $rsUFT->FeldInhalt('UFT_KENNUNG'), 5, $FeldBreiten['Kennung'], false, ($DS%2), '', '', 'T', 'L');
			$Form->Erstelle_ListenFeld('*UFT_BEZEICHNUNG', $rsUFT->FeldInhalt('UFT_INFORMATION'), 50, $FeldBreiten['Bezeichnung'], false, ($DS%2), '', '', 'T', 'L', $rsUFT->FeldInhalt('UFT_INFORMATION'));
			$Form->Erstelle_ListenFeld('*UFT_BEMERKUNG', $rsUFT->FeldInhalt('UFT_BEMERKUNG'), 50, $FeldBreiten['Bemerkung'], false, ($DS%2), '', '', 'T', 'L', $rsUFT->FeldInhalt('UFT_BEMERKUNG'));
			// wenn �bergeordneter Bereich nicht NULL
			if (! empty($UeberBereich))
			{			
				$Form->Erstelle_ListenFeld('*UFT_GEHOERTZU', $rsUFT->FeldInhalt('GEHOERTZU'), 50, $FeldBreiten['Ueberkategorie'], false, ($DS%2), '', '', 'T', 'L', $rsUFT->FeldInhalt('GEHOERTZU'));
			}
			$Form->ZeileEnde();
			$DS++;
			$rsUFT->DSWeiter();
		}		
	}
	
	$Form->Formular_Ende();
    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>