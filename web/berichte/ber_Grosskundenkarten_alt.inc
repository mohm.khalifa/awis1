<?php

require_once 'awisBerichte.inc';
require_once 'awisBarcode.inc';
require_once 'awisDatenbank.inc';

require_once("register.inc.php");
require_once('jpgraph/jpgraph.php');
require_once('jpgraph/jpgraph_barcode.php');
require_once('jpgraph/jpgraph_canvas.php');

class ber_Grosskundenkarten_alt
    extends awisBerichte
{

    var $angle = 0;

    private $_TBL_GROSSKUNDENKARTEN = "";
    private $_TBL_GROSSKUNDEST = "";

    private $_AWISWerkzeuge;

    public function init(array $Parameter)
    {
        $this->_AWISWerkzeuge = new awisWerkzeuge();

        if($this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK or $this->_AWISWerkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_STAGING){
            $this->_TBL_GROSSKUNDENKARTEN = " TESTGROSSKUNDENKARTEN ";
            $this->_TBL_GROSSKUNDEST = " TESTGROSSKUNDEST ";
        } else {
            $this->_TBL_GROSSKUNDENKARTEN = " EXPERIAN_KOM.EXPERIAN_GROSSKUNDENKARTEN@COM_DE.ATU.DE ";
            $this->_TBL_GROSSKUNDEST = " EXPGW.ATU_GROSSKUNDEST@COM_DE.ATU.DE ";
        }

        //Daten laden
        $SQL = 'SELECT DISTINCT KND.EAN_NR, KND.KUNDEN_NR, KND.KARTEN_NR, KND.NAME1, KND.NAME2, KND.NAME3, KND.GUELTIGKEIT, ERZ_KEY, LAND.GKB_STAAT FROM '.$this->_TBL_GROSSKUNDENKARTEN.' KND
                LEFT JOIN EXPRABATTZUORDNUNGEN ON ERZ_KUNDEN_NR = KND.KUNDEN_NR 
                LEFT JOIN '.$this->_TBL_GROSSKUNDEST.' LAND ON KND.KUNDEN_NR = LAND.KUNDEN_NR 
                WHERE KND.KUNDEN_NR IN (' . implode(',', $_POST["txtKUNDEN"]) . ') AND KND.GEDRUCKT = 1';

        $this->_rec = $this->_DB->RecordSetOeffnen($SQL);

        //COM-Server-Update
        $SQL = 'UPDATE '.$this->_TBL_GROSSKUNDENKARTEN.' SET GEDRUCKT = 2, DRUCK_DATUM = ' . $this->_DB->WertSetzen('COM', 'D', date('d.m.Y H:i:s'));
        $SQL .= ' WHERE KUNDEN_NR IN (' . implode(',', $_POST["txtKUNDEN"]) . ') AND GEDRUCKT = 1';
        $this->_DB->Ausfuehren($SQL, 'COM', true, $this->_DB->Bindevariablen('COM', true));

        $TextKonserven[] = array('SYSTEM', 'PROGNAME');
        $TextKonserven[] = array('ERM', '*');

        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        $this->SetTitle('Grosskundenkarten');
        $this->SetSubject('');
        $this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
        $this->SetAuthor($this->_AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);
        $Barcode = new awisBarcode($this);

        $this->AddFont('Arial', 'B', 'ariblk.php'); //Arial (Bold) mit Arial Black ersetzen (aufpassen: Zeichenabstand breiter als bei Arial)

        while (!$this->_rec->EOF()) {
            $this->NeueSeite();
            $this->_Zeile = $this->_Parameter['SeiteOben'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];

            //Schauen ob Rabattmodell hinterlegt
            if ($this->_rec->FeldInhalt('ERZ_KEY') != "") {
                //Überschrift
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->_Zeile);
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseUeberschrift']);
                $this->Cell($this->_Parameter['FeldBreiteUeberschrift'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_UEBERSCHRIFT'], 0, 0, 'L');

                //Daten
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseDaten']);
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->y + $this->_Parameter['ZeilenAbstandUeberschrift']);
                $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME1'), 0, 0, 'L');
                $this->Cell($this->_Parameter['FeldBreiteDaten'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_KDNR'], 0, 0, 'L');
                $this->SetX($this->x - $this->_Parameter['FeldBreiteDaten']);
                $this->Cell($this->_Parameter['FeldBreiteDaten'], 0, $this->_rec->FeldInhalt('KUNDEN_NR'), 0, 0, 'R');
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->y + $this->_Parameter['ZeilenAbstandDaten']);
                $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME2'), 0, 0, 'L');
                $this->Cell($this->_Parameter['FeldBreiteDaten'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_GUELTIGBIS'], 0, 0, 'L');
                $this->SetX($this->x - $this->_Parameter['FeldBreiteDaten']);
                $gueltigkeit = substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 0, 2) . '.' . substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 2, 2);
                $this->Cell($this->_Parameter['FeldBreiteDaten'], 0, $gueltigkeit, 0, 0, 'R');
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKunde'], $this->y + $this->_Parameter['ZeilenAbstandDaten']);
                $this->Cell($this->_Parameter['FeldBreiteName'], 0, $this->_rec->FeldInhalt('NAME3'), 0, 0, 'L');

                //Kontakt
                $Laenderkuerzel = ($this->_rec->FeldInhalt('GKB_STAAT') == '38'?'_AT':'');
                $Laenderkuerzel = '_AT';
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseHotline']);
                if ($Laenderkuerzel == '') {
                    $this->SetXY($this->_Spalte, $this->y + $this->_Parameter['ZeilenAbstandDaten']);
                    $this->Cell($this->_Parameter['FeldBreiteKontakt'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_HOTLINE' . $Laenderkuerzel], 0, 0, 'C');
                    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseKontakt']);
                    $this->SetXY($this->_Spalte, $this->y + $this->_Parameter['ZeilenAbstandKontakt']);
                    $this->Cell($this->_Parameter['FeldBreiteKontakt'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_PREISE'], 0, 0, 'C');
                } else {
                    $this->SetXY($this->_Spalte, $this->y + 2 * $this->_Parameter['ZeilenAbstandDaten']);
                    $this->Cell($this->_Parameter['FeldBreiteKontakt'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_HOTLINE' . $Laenderkuerzel], 0, 0, 'C');
                    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseKontakt']);
                }
                $this->SetXY($this->_Spalte + $this->_Parameter['AbstandMail'], $this->y + $this->_Parameter['ZeilenAbstandMail']);
                $this->Cell($this->_Parameter['FeldBreiteKontakt'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_WEBSITE' . $Laenderkuerzel], 0, 0, 'L');
                $this->SetXY($this->_Spalte + $this->_Parameter['AbstandMail'], $this->y + $this->_Parameter['ZeilenAbstandMail']);
                $this->Cell($this->_Parameter['FeldBreiteKontakt'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_MAIL' . $Laenderkuerzel], 0, 0, 'L');

                //Barcode
                $this->SetXY($this->_Spalte + $this->_Parameter['AbstandBarcodeLinks'], $this->_Zeile + $this->_Parameter['AbstandBarcodeOben']);
                $Barcode->EAN13($this->x, $this->y, substr($this->_rec->FeldInhalt('EAN_NR'), 0, 12), 11, 0.4);

                //Rückseite
                $this->NeueSeite();
                $this->_Zeile = $this->_Parameter['SeiteObenRueckseite'];
                $this->_Spalte = $this->_Parameter['LinkerRand'];

                //Infos
                $this->SetXY($this->_Spalte + $this->_Parameter['AbstandInfos'], $this->_Zeile);
                $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseInfos']);
                $this->RotierterText($this->_Parameter['FeldBreiteInfosRueckseite'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_INFOS1'], 0, 0, 'C', false, '', -90);
                $this->SetX($this->x - $this->_Parameter['ZeilenAbstandInfosRueckseite'] - $this->_Parameter['FeldBreiteInfosRueckseite']);
                $this->RotierterText($this->_Parameter['FeldBreiteInfosRueckseite'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_INFOS2'], 0, 0, 'C', false, '', -90);
                $this->SetX($this->x - $this->_Parameter['ZeilenAbstandInfosRueckseite'] - $this->_Parameter['FeldBreiteInfosRueckseite']);
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseAchtung']);
                $this->RotierterText($this->_Parameter['FeldBreiteInfosRueckseite'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_INFOS3'], 0, 0, 'C', false, '', -90);
                $this->SetFont($this->_Parameter['Schriftart'], '');

                //Barcodes
                $SQL = 'SELECT EES.EES_BILDPFAD, ERD.ERD_BEZEICHNUNG, ERD.ERD_WG, ERM.ERM_RABATTSATZ FROM EXPEANSWITCH EES
                    INNER JOIN EXPRABATTMODELLE ERM ON ERM.ERM_RABATTSATZ = EES.EES_EANID
                    INNER JOIN EXPRABATTZUORDNUNGEN ERZ ON ERZ.ERZ_ERM_NR = ERM.ERM_NR
                    INNER JOIN EXPRABATTDETAILS ERD ON ERD.ERD_KEY = ERM.ERM_ERD_KEY
                    WHERE ERZ.ERZ_KUNDEN_NR = ' . $this->_DB->WertSetzen('ERZ', 'TN', $this->_rec->FeldInhalt('KUNDEN_NR'));
                $rsBarcodes = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ERZ', true));

                $BarcodeX = $this->_Spalte + $this->_Parameter['AbstandObersterBarcodeLinks'];
                while (!$rsBarcodes->EOF()) {
                    $this->Image($rsBarcodes->FeldInhalt('EES_BILDPFAD'), $BarcodeX, $this->_Zeile - $this->_Parameter['VeschubBarcodeRueckseite'], $this->_Parameter['BarcodeBreite'], $this->_Parameter['BarcodeHoehe']);

                    $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseBarcodeInfos']);
                    $this->SetXY($BarcodeX + $this->_Parameter['AbstandBarcodeInfos'], $this->_Zeile + $this->_Parameter['BarcodeHoehe']);
                    $this->RotierterText($this->_Parameter['FeldBreiteBarcodeInfos'], 0, $rsBarcodes->FeldInhalt('ERD_BEZEICHNUNG'), 0, 0, 'L', false, '', -90, 'L');
                    $this->SetX($this->x - $this->_Parameter['ZeilenAbstandBarcodeInfos'] - $this->_Parameter['FeldBreiteBarcodeInfos']);
                    $this->RotierterText($this->_Parameter['FeldBreiteBarcodeInfos'], 0, 'WG ' . $rsBarcodes->FeldInhalt('ERD_WG'), 0, 0, 'L', false, '', -90, 'L');
                    $this->SetX($this->x - $this->_Parameter['ZeilenAbstandBarcodeInfos'] - $this->_Parameter['FeldBreiteBarcodeInfos']);
                    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseRabattsatz']);
                    $this->RotierterText($this->_Parameter['FeldBreiteBarcodeInfos'], 0, $rsBarcodes->FeldInhalt('ERM_RABATTSATZ') . ' %', 0, 0, 'R', false, '', -90, 'L');

                    $BarcodeX = $BarcodeX - $this->_Parameter['AbstandBarcodes'] - $this->_Parameter['BarcodeBreite'];
                    $rsBarcodes->DSWeiter();
                }
            } else {
                //Überschrift
                $this->SetXY($this->_Spalte+ $this->_Parameter['EinrueckungKundeRabattlos'], $this->_Zeile);
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseUeberschrift']);
                $this->Cell($this->_Parameter['FeldBreiteUeberschrift'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_UEBERSCHRIFT'], 0, 0, 'L');

                //Namen
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseNameRabattlos']);
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKundeRabattlos'], $this->y + $this->_Parameter['ZeilenAbstandUeberschriftRabattlos']);
                $this->Cell($this->_Parameter['FeldBreiteNameRabattlos'], 0, $this->_rec->FeldInhalt('NAME1'), 0, 0, 'L');
                $this->Cell($this->_Parameter['FeldBreiteDaten'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_KUNDENNUMMER'], 0, 0, 'L');
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKundeRabattlos'], $this->y + $this->_Parameter['ZeilenAbstandDaten']);
                $this->Cell($this->_Parameter['FeldBreiteNameRabattlos'], 0, $this->_rec->FeldInhalt('NAME2'), 0, 0, 'L');
                $this->Cell($this->_Parameter['FeldBreiteDaten'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_GUELTIGBIS'], 0, 0, 'L');
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKundeRabattlos'], $this->y + $this->_Parameter['ZeilenAbstandDaten']);
                $this->Cell($this->_Parameter['FeldBreiteNameRabattlos'], 0, $this->_rec->FeldInhalt('NAME3'), 0, 0, 'L');

                //KndNr & GültigBis
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseKundennummerRabattlos']);
                $this->SetXY($this->_Spalte + $this->_Parameter['EinrueckungKundeRabattlos'], $this->y + $this->_Parameter['ZeilenAbstandKundennummerRabattlos']);
                $this->Cell($this->_Parameter['FeldBreiteKndNrLabelRabattlos'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_KUNDENNUMMER'], 0, 0, 'L');
                $this->Cell($this->_Parameter['FeldBreiteKndNrRabattlos'], 0, $this->_rec->FeldInhalt('KUNDEN_NR'), 0, 0, 'L');
                $this->Cell($this->_Parameter['FeldBreiteGueltigLabelRabattlos'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_GUELTIGBIS'], 0, 0, 'L');
                $gueltigkeit = substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 0, 2) . '.' . substr($this->_rec->FeldInhalt('GUELTIGKEIT'), 2, 2);
                $this->Cell($this->_Parameter['FeldBreiteGueltigLabelRabattlos'], 0, $gueltigkeit, 0, 0, 'L');

                //Barcode
                $this->SetXY($this->_Spalte + $this->_Parameter['AbstandBarcodeLinks'], $this->_Zeile + $this->_Parameter['AbstandBarcodeOben']);
                $Barcode->EAN13($this->x, $this->y, substr($this->_rec->FeldInhalt('EAN_NR'), 0, 12), 10, 0.3);

                //Rückseite
                $this->NeueSeite();
                $this->_Zeile = $this->_Parameter['SeiteObenRueckseiteRabattlos'];
                $this->_Spalte = $this->_Parameter['LinkerRand'];


                //Kontakt
                $Laenderkuerzel = ($this->_rec->FeldInhalt('GKB_STAAT') == '38'?'_AT':'');
                $this->AddFont('Arial', '', 'arialbd.php'); //Arial mit Arial (Bold) ersetzen, da ich die erste kein zweites Mal ersetzen kann
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseKontaktRabattlos']);
                $this->SetXY($this->_Spalte, $this->_Zeile);
                $this->Cell($this->_Parameter['FeldBreiteKontaktRabattlos'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_WEBSITE' . $Laenderkuerzel], 0, 0, 'C');
                $this->SetXY($this->_Spalte, $this->y + $this->_Parameter['ZeilenAbstandKontaktRabattlos']);
                $this->Cell($this->_Parameter['FeldBreiteKontaktRabattlos'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_MAIL' . $Laenderkuerzel], 0, 0, 'C');
                $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesseHotlineRabattlos']);
                $this->SetXY($this->_Spalte, $this->y + $this->_Parameter['ZeilenAbstandKontaktRabattlos']);
                $this->Cell($this->_Parameter['FeldBreiteKontaktRabattlos'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_HOTLINE' . $Laenderkuerzel], 0, 0, 'C');
                $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroessePreiseRabattlos']);
                $this->y += 1;
                if ($Laenderkuerzel == '') {
                    $this->SetXY($this->_Spalte, $this->y + $this->_Parameter['ZeilenAbstandInfosRabattlos']);
                    $this->Cell($this->_Parameter['FeldBreiteKontaktRabattlos'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_PREISE'], 0, 0, 'C');
                    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseInfosRabattlos']);
                }
                $this->SetXY($this->_Spalte, $this->y + $this->_Parameter['ZeilenAbstandInfosRabattlos']);
                $this->Cell($this->_Parameter['FeldBreiteKontaktRabattlos'], 0, $this->_AWISSprachkonserven['ERM']['ERM_PDF_INFOS3'], 0, 0, 'C');
            }

            $this->_rec->DSWeiter();
        }

        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************

        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    private function NeueSeite()
    {
        $this->AddPage('L', [85.60, 53.98]);
    }

    /**
     * @param        $w
     * @param int    $h
     * @param string $txt
     * @param int    $border
     * @param int    $ln
     * @param string $align
     * @param bool   $fill
     * @param string $link
     * @param int    $angle         Winkel der Rotation
     * @param string $rotationPoint An welchem Punkt gedreht werden so (L/C/R)
     */
    function RotierterText($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '', $angle = 0, $rotationPoint = 'L')
    {
        $y = $this->y + $h / 2;
        switch ($rotationPoint) {
            default:
            case 'C':
                $x = $this->x + $w / 2;
                break;
            case 'R':
                $x = $this->x + $w;
                break;
            case 'L':
                $x = $this->x;
                break;
        }
        $this->Rotate($angle, $x, $y);
        $this->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
        $this->Rotate(0, $x, $y);
    }

    function Rotate($angle, $x = -1, $y = -1)
    {
        if ($x == -1) {
            $x = $this->x;
        }
        if ($y == -1) {
            $y = $this->y;
        }
        if ($this->angle != 0) {
            $this->_out('Q');
        }
        $this->angle = $angle;
        if ($angle != 0) {
            $angle *= M_PI / 180;
            $c = cos($angle);
            $s = sin($angle);
            $cx = $x * $this->k;
            $cy = ($this->h - $y) * $this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        }
    }

    function _endpage()
    {
        if ($this->angle != 0) {
            $this->angle = 0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

}