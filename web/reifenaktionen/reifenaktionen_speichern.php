<?php

require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
require_once('awisDatenbank.inc');
require_once('awisFilialEbenen.inc');

global $AWIS_KEY1;
global $AWIS_KEY2;
global $RUECKFUEHRUNGSTYP;
global $LAND;

$TextKonserven = array();
$TextKonserven[] = array('RRA', '*');
$TextKonserven[] = array('Fehler', 'err_KeinWert');
$TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
$TextKonserven[] = array('Fehler', 'err_keinegueltigeATUNR');
$TextKonserven[] = array('Fehler', 'err_keinegueltigeFILIALE');
$TextKonserven[] = array('Fehler', 'err_keingueltigesDatum');
$TextKonserven[] = array('Wort', 'geaendert_von');
$TextKonserven[] = array('Wort', 'geaendert_auf');
$TextKonserven[] = array('Meldung', 'DSVeraendert');
$TextKonserven[] = array('Meldung', 'EingabeWiederholen');
$TextKonserven[] = array('Wort', 'WirklichAendern');
$TextKonserven[] = array('Wort', 'Ja');
$TextKonserven[] = array('Wort', 'Nein');
$TextKonserven[] = array('Wort', 'FilialenAnlegen');
$TextKonserven[] = array('Wort', 'lbl_weiter');

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    if (isset($_POST['txtRRA_KEY']))
    {
    	if (floatval($_POST['txtRRA_KEY'])==0)
        {
        	$AWIS_KEY1=-1;
        }
        else
        {
        	$AWIS_KEY1=floatval($_POST['txtRRA_KEY']);
        }

        $Felder = $Form->NameInArray($_POST, 'txtRRA_', 1, 1);

	    if ($Felder != '')
    	{
	        $Felder = explode(';', $Felder);
	        $TextKonserven[] = array('RRA', 'RRA_%');
	        $TXT_Speichern = $Form->LadeTexte($TextKonserven);
        
            // Daten auf Vollst�ndigkeit pr�fen
            $Fehler = '';
            $Pflichtfelder = array('RRA_AKTIONSNAME','RRA_GUELTIGAB','RRA_GUELTIGBIS');
            foreach ($Pflichtfelder AS $Pflichtfeld)
            {
                if (isset($_POST['txt' . $Pflichtfeld]) and $_POST['txt' . $Pflichtfeld] == '')
                {
                    $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . $TXT_Speichern['RRA'][$Pflichtfeld] . '<br>';
                }
            }
            
            // Wurden Fehler entdeckt? => Speichern abbrechen
            if($Fehler!='')
            {
         		$Form->ZeileStart();
           		$Form->Hinweistext($Fehler);
           		$Form->ZeileEnde();
            
           		$Link = './reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY=' . $AWIS_KEY1;
           		$Form->SchaltflaechenStart();
           		$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
           		$Form->SchaltflaechenEnde();

           		die();
            }
            
            $FeldListe='';
            $SQL = '';
            
            $count = 1;
            
            //Pr�fen, ob Warenuntergruppe ohne Warengruppe
            if (isset($_POST['txtRRA_WUG_ID']) and $_POST['txtRRA_WUG_ID'] != '')
            {
            	if (isset($_POST['txtRRA_WGR_ID']) and $_POST['txtRRA_WGR_ID'] != '')
            	{
            		$count = 2;
            	}
            	else
            	{
            		$Form->ZeileStart();
            		$Form->Hinweistext('Es wurde eine Warenuntergruppe ohne Warengruppe angegeben!<br>');
            		$Form->ZeileEnde();
            
            		$Link = './reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY=' . $AWIS_KEY1;
            		$Form->SchaltflaechenStart();
            		$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
            		$Form->SchaltflaechenEnde();
            
            		die();
            	}
            }
            
            //Pr�fen, ob mind. ein relevantes Feld gef�llt wurde
            $Prueffelder = array(
            		0 => 'RRA_ATUNRLISTE',
            		1 => 'RRA_WGR_ID',
            		2 => 'RRA_WUG_ID',
            		3 => 'RRA_KENN1',
            		6 => 'RRA_REH_KEY');
            
            $FeldNamen = array();
            
            foreach ($Prueffelder AS $Feld)
            {
            	if (isset($_POST['txt' . $Feld]) and ($_POST['txt' . $Feld]) == '')
            	{
            		$FeldNamen[] = $TXT_Speichern['RRA'][$Feld];
            	}
            }
            
            if (count($FeldNamen) == count($Prueffelder))
            {
            	$Hinweistext = 'Es muss mindestens ein Feld gef�llt sein!<br>';
            	$Hinweistext.= 'Folgende Felder sind nicht gef�llt:<br>';
            
            	foreach ($FeldNamen as $Feld)
            	{
            		$Hinweistext.= $Feld . '<br>';
            	}
            
            	$Form->ZeileStart();
            	$Form->Hinweistext($Hinweistext);
            	$Form->ZeileEnde();
            
            	$Link = './reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY=' . $AWIS_KEY1;
            	$Form->SchaltflaechenStart();
            	$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
            	$Form->SchaltflaechenEnde();
            
            	die();
            }
            
            // Pr�fen ob der Aktionszeitraum korrekt eingegeben wurde
          	$pruefDatumAb = $Form->PruefeDatum($_POST['txtRRA_GUELTIGAB'], false, false, true);
           	$pruefDatumBis = $Form->PruefeDatum($_POST['txtRRA_GUELTIGBIS'], false, false, true);
            
           	if($pruefDatumAb > $pruefDatumBis)
           	{
           		$Hinweistext = 'G�ltigkeitszeitraum falsch: Ab-Datum muss vor Bis-Datum liegen.';
           
           		$Form->ZeileStart();
           		$Form->Hinweistext($Hinweistext);
           		$Form->ZeileEnde();
            
           		$Link = './reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY=' . $AWIS_KEY1;
           		$Form->SchaltflaechenStart();
           		$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
           		$Form->SchaltflaechenEnde();
           
           		die();
           	}

            if($AWIS_KEY1<=0)
            {
            	//pr�fen, ob nicht gleiche Aktion bereits vorhanden
            	$SQL = 'SELECT *';
            	$SQL .= ' FROM REIFENAKTIONEN';
            	$SQL .= ' WHERE RRA_AKTIONSNAME ='.$DB->FeldInhaltFormat('T', $_POST['txtRRA_AKTIONSNAME'], true);
            	$SQL .= ' AND RRA_GUELTIGAB ='.$DB->FeldInhaltFormat('D', $_POST['txtRRA_GUELTIGAB'], true);
            	$SQL .= ' AND RRA_GUELTIGBIS ='.$DB->FeldInhaltFormat('D', $_POST['txtRRA_GUELTIGBIS'], true);
            	$SQL .= ' AND RRA_LAN_CODE ='.$DB->FeldInhaltFormat('T', $_POST['txtRRA_LAN_CODE'], true);

            	$rsRRA = $DB->RecordSetOeffnen($SQL);
            	if (!$rsRRA->EOF())
            	{
            		$AWIS_KEY1=$rsRRA->FeldInhalt('RRA_KEY');
            		$Hinweistext = 'Gleiche Aktion ist bereits vorhanden und wird nicht neu angelegt!';
            		
            		$Form->ZeileStart();
            		$Form->Hinweistext($Hinweistext);
            		$Form->ZeileEnde();
            		
            		$Link = './reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY=' . $AWIS_KEY1;
            		$Form->SchaltflaechenStart();
            		$Form->Schaltflaeche('href', 'cmdReifenAktionen', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
            		$Form->SchaltflaechenEnde();
            		
            		die();
            	} 
            }

            if($AWIS_KEY1<=0)
            {
            	$Fehler = '';
            	$SQL  = 'INSERT';
            	$SQL .= ' INTO REIFENAKTIONEN (';
            	$SQL .= ' RRA_ATUNRLISTE,';
            	$SQL .= ' RRA_WGR_ID,';
            	$SQL .= ' RRA_WUG_ID,';
            	$SQL .= ' RRA_KENN1,';
            	$SQL .= ' RRA_REH_KEY,';
            	$SQL .= ' RRA_AKTIONSNAME,';
            	$SQL .= ' RRA_AKTIONSPROZENT,';
            	$SQL .= ' RRA_AKTIONSTEXT,';
            	$SQL .= ' RRA_GUELTIGAB,';
            	$SQL .= ' RRA_GUELTIGBIS,';
            	$SQL .= ' RRA_LAN_CODE,';
            	$SQL .= ' RRA_BEMERKUNG,';
            	$SQL .= ' RRA_USER,';
            	$SQL .= ' RRA_USERDAT';
            	$SQL .= ' ) VALUES (';
            	
            	if($_POST['txtRRA_ATUNRLISTE'] == '' || is_null($_POST['txtRRA_ATUNRLISTE']))
            	{
            		$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_ATUNRLISTE'], true).',';
            	}
            	else 
            	{
            		$DB->SetzeBindevariable('RRA', 'var_CL_rra_atunrliste', $_POST['txtRRA_ATUNRLISTE']);
            		$SQL .= ':var_CL_rra_atunrliste,';
            	}
            	
            	$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_WGR_ID'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_WUG_ID'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_KENN1'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('N0', $_POST['txtRRA_REH_KEY'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_AKTIONSNAME'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('N0', $_POST['txtRRA_AKTIONSPROZENT'], false).',';
            	$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_AKTIONSTEXT'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('D', $_POST['txtRRA_GUELTIGAB'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('D', $_POST['txtRRA_GUELTIGBIS'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_LAN_CODE'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('T', $_POST['txtRRA_BEMERKUNG'], true).',';
            	$SQL .= $DB->FeldInhaltFormat('T', $AWISBenutzer->BenutzerName(), true).',';
            	$SQL .= 'SYSDATE';
            	$SQL .= ')';
            	            	
            	$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('RRA'));
                $Form->DebugAusgabe(1,$DB->LetzterSQL());
                
                $SQL = 'SELECT seq_RRA_KEY.CurrVal AS KEY FROM DUAL';
                $rsKey = $DB->RecordSetOeffnen($SQL);
                $AWIS_KEY1 = $rsKey->FeldInhalt('KEY');
            }
        	else      // ge�nderte Zuordnung
        	{
	            $FehlerListe = array();
	            $UpdateFelder = '';
	
	            $rsRRA = $DB->RecordSetOeffnen('SELECT * FROM REIFENAKTIONEN WHERE RRA_key=' . $_POST['txtRRA_KEY'] . '');
	            $FeldListe = '';
	
	            if (isset($_POST['txtRRA_LAN_CODE']) AND $_POST['txtRRA_LAN_CODE']=='0')
            	{
	           		$_POST['txtRRA_LAN_CODE']='';
	            }
	            	            
	            if (isset($_POST['txtRRA_ATUNRLISTE']) AND $_POST['txtRRA_ATUNRLISTE']!='')
	            {
	            	if (isset($_POST['oldRRA_ATUNRLISTE']) AND $_POST['txtRRA_ATUNRLISTE']!=$_POST['oldRRA_ATUNRLISTE'])
	            	{
	            		$_POST['txtRRA_ATUNRLISTE']=str_replace("\r\n",',',$_POST['txtRRA_ATUNRLISTE']);
	            		$_POST['txtRRA_ATUNRLISTE']=str_replace("\t",',',$_POST['txtRRA_ATUNRLISTE']);
	            		$_POST['txtRRA_ATUNRLISTE']=str_replace(' ','',$_POST['txtRRA_ATUNRLISTE']);
	            		$_POST['txtRRA_ATUNRLISTE']=str_replace(';',',',$_POST['txtRRA_ATUNRLISTE']);
	            		$_POST['txtRRA_ATUNRLISTE']=str_replace('/',',',$_POST['txtRRA_ATUNRLISTE']);
	            		$_POST['txtRRA_ATUNRLISTE']=strtoupper($_POST['txtRRA_ATUNRLISTE']);

	            		$i = 1;
	            		while ($i>0)
	            		{
	            			$_POST['txtRRA_ATUNRLISTE']=str_replace(',,',',',$_POST['txtRRA_ATUNRLISTE'],$i);
	            		}
	            		
	            		
	            		$SortArr=array();
	            		$SortArr=explode(',',$_POST['txtRRA_ATUNRLISTE']);
	            		$SortArr=array_unique($SortArr);
	            		asort($SortArr);
	            		$_POST['txtRRA_ATUNRLISTE']=implode(',', $SortArr);
	            		$_POST['txtRRA_ATUNRLISTE']=trim($_POST['txtRRA_ATUNRLISTE'],',');
	            	}
	            }
	             
	            foreach ($Felder AS $Feld)
	            {
	                $FeldName = substr($Feld, 3);
	                if (isset($_POST['old' . $FeldName]))
	                {
	                    // Alten und neuen Wert umformatieren!!
	                    $WertNeu = $DB->FeldInhaltFormat($rsRRA->FeldInfo($FeldName, 'TypKZ'), $_POST[$Feld], true);
	                    $WertAlt = $DB->FeldInhaltFormat($rsRRA->FeldInfo($FeldName, 'TypKZ'), $_POST['old' . $FeldName], true);
	                    $WertDB = $DB->FeldInhaltFormat($rsRRA->FeldInfo($FeldName, 'TypKZ'), $rsRRA->FeldInhalt($FeldName), true);
	                    if (isset($_POST['old' . $FeldName]) AND ($WertDB == 'null' OR $WertAlt != $WertNeu) AND !(strlen($FeldName) == 7 AND substr($FeldName, -4, 4) == '_KEY'))
	                    {
	                        if ($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB != 'null')
	                        {
	                            $FehlerListe[] = array($FeldName, $WertAlt, $WertDB);
	                        }
	                        else
	                        {
	                            $FeldListe .= ', ' . $FeldName . '=';
	
	                            if ($_POST[$Feld] == '') // Leere Felder immer als NULL
	                            {
	                                $FeldListe.=' null';
	                            }
	                            else
	                            {
	                            	if ($FeldName=='RRA_ATUNRLISTE')
	                            	{
	                            		$FeldListe.=':var_CL_rra_atunrliste';
	                            		$DB->SetzeBindevariable('RRA', 'var_CL_rra_atunrliste', $WertNeu);
	                            	}
	                            	else
	                            	{
	                                	$FeldListe.=$WertNeu;
	                            	}
	                            }
	                        }
	                    }
	                }
	            }
	
	            if (count($FehlerListe) > 0)
	            {
	                $Meldung = str_replace('%1', $rsRRA->FeldInhalt('RRA_USER'), $TXT_Speichern['Meldung']['DSVeraendert']);
	                foreach ($FehlerListe AS $Fehler)
	                {
	                    $Meldung .= '<br>&nbsp;' . $Fehler[0] . ': \'' . $Fehler[1] . '\' ==> \'' . $Fehler[2] . '\'';
	                }
	                $Form->Fehler_Anzeigen(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
	            }
	            elseif ($FeldListe != '')
	            {
	                $SQL = 'UPDATE REIFENAKTIONEN SET';
	                $SQL .= substr($FeldListe, 1);
	                $SQL .= ', RRA_user=\'' . $AWISBenutzer->BenutzerName() . '\'';
	                $SQL .= ', RRA_userdat=sysdate';
	                $SQL .= ' WHERE RRA_key=0' . $_POST['txtRRA_KEY'] . '';
	
	                $DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('RRA'));
	                $Form->DebugAusgabe(1,$DB->LetzterSQL());
	            }
        	}
        }
    }
}
catch (awisException $ex)
{
    $Form->DebugAusgabe(1, $ex->getSQL());
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
catch (Exception $ex)
{
    $Form->DebugAusgabe(1, $ex->getSQL());
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>