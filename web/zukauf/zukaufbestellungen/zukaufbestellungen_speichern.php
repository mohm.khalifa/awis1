<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
	$Werkzeuge = new awisWerkzeuge();

	//***********************************************************************************
	//** Bestellungen �ndern / anlegen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZUB_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('ZUB','ZUB_%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtZUB_KEY'];


		if(strlen($_POST['txtZUB_WANRKORR'])==10)
		{
			$_POST['txtZUB_WANRKORR'] = $Werkzeuge->BerechnePruefziffer($_POST['txtZUB_WANRKORR'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_ERMITTELN);
		}

		$rsZUB = $DB->RecordSetOeffnen('SELECT * FROM Zukaufbestellungen WHERE ZUB_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));
		if($rsZUB->EOF())
		{
			if(isset($_POST['txtZUB_ZAL_KEY']) AND $_POST['txtZUB_ZAL_KEY']!='')
			{
				$SQL = 'SELECT ZLA_KEY, ZAL_KEY, ZLA_BEZEICHNUNG, ZLA_ARTIKELNUMMER, ZAL_BESTELLNUMMER';
				$SQL .= ' FROM Zukaufartikel';
				$SQL .= ' INNER JOIN Zukaufartikellieferanten ON ZLA_KEY = ZAL_ZLA_KEY AND ZAL_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUB_LIE_NR'],true);
				$SQL .= ' WHERE ZAL_KEY = :var_N0_ZAL_KEY';
				$DB->SetzeBindevariable('ZLA','var_N0_ZAL_KEY',$_POST['txtZUB_ZAL_KEY'],awisDatenbank::VAR_TYP_GANZEZAHL);
				$rsZLA = $DB->RecordSetOeffnen($SQL, $DB->BindeVariablen('ZLA'));
			}
			else
			{
				// Artikel suchen
				$SQL = 'SELECT ZLA_KEY, ZAL_KEY, ZLA_BEZEICHNUNG, ZLA_ARTIKELNUMMER, ZAL_BESTELLNUMMER';
				$SQL .= ' FROM Zukaufartikel';
				$SQL .= ' INNER JOIN Zukaufartikellieferanten ON ZLA_KEY = ZAL_ZLA_KEY AND ZAL_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUB_LIE_NR'],true);
				$SQL .= ' INNER JOIN ZUKAUFHERSTELLERCODES ON ZLA_ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUB_LIE_NR'],false);
				//PG 08.06.2017: Bei Anlage einer neuen Bestellung gibt es kein txtZUB_ARTIKELNUMMER, da das Feld bei Neuanlage ein sucFeld ist => sucARTNUMMER
				if(isset($_POST['txtZUB_ARTIKELNUMMER'])){
				    $ArtNr = $_POST['txtZUB_ARTIKELNUMMER'];
                }elseif(isset($_POST['sucARTNUMMER'])){
                    $ArtNr = $_POST['sucARTNUMMER'];
                }else{
                    $ArtNr = '';
                }
				$SQL .= ' WHERE ZLA_ARTIKELNUMMER = '.$DB->FeldInhaltFormat('TU',$ArtNr,false);
				$SQL .= ' AND ZHK_CODE = '.$DB->FeldInhaltFormat('TU',$_POST['txtZUB_HERSTELLER'],false);
				$rsZLA = $DB->RecordSetOeffnen($SQL);
			}
			
			// Wenn nicht, rausl�schen, dann l�uft er in die Fehler
			if($rsZLA->EOF())
			{
				$_POST['txtZUB_ARTIKELNUMMER'] = '';
			}
			else
			{
				// Artikelnummer zum �berschreiben
				//   Hier muss pro Lieferant eingetragen werden, welche Nummer angeliefert wird (TecDoc / eigen)
				switch($_POST['txtZUB_LIE_NR'])
				{
					case '2620':			// Birner
						$_POST['txtZUB_ARTIKELNUMMER']=$rsZLA->FeldInhalt('ZLA_ARTIKELNUMMER');
						break;
					default:
						$_POST['txtZUB_ARTIKELNUMMER']=$rsZLA->FeldInhalt('ZAL_BESTELLNUMMER');
				} 
			}

			// Bezeichnung aus dem Artikelstamm, wenn sie angegeben wurde
			$_POST['txtZUB_ARTIKELBEZEICHNUNG'] = ($_POST['txtZUB_ARTIKELBEZEICHNUNG']==''?$rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'):$_POST['txtZUB_ARTIKELBEZEICHNUNG']);


			$Fehler = '';
			$Pflichtfelder = array('ZUB_FIL_ID','ZUB_LIE_NR','ZUB_HERSTELLER','ZUB_BESTELLDATUM','ZUB_ARTIKELNUMMER','ZUB_ARTIKELBEZEICHNUNG','ZUB_WANRKORR','ZUB_BESTELLMENGE','ZUB_PREISBRUTTO');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZUB'][$Pflichtfeld].'<br>';
				}
			}
			if($_POST['txtZUB_WANRKORR']!='')
			{
				if(!$Werkzeuge->BerechnePruefziffer($_POST['txtZUB_WANRKORR'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_PruefzifferFalsch'].' '.$TXT_Speichern['ZUB']['ZUB_WANRKORR'].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}

			$SQL = 'INSERT INTO ZukaufBestellungen';
			$SQL .= '(ZUB_BESTELLDATUM,ZUB_FIL_ID,ZUB_LIE_NR,ZUB_WANR,ZUB_ZAL_KEY,ZUB_ZLA_KEY,';
			$SQL .= 'ZUB_ARTIKELNUMMER,ZUB_ARTIKELBEZEICHNUNG,ZUB_HERSTELLER,ZUB_VERSANDARTBEZ,';
			$SQL .= 'ZUB_SORTIMENT,ZUB_BESTELLMENGE,ZUB_MENGENEINHEITEN,ZUB_TAUSCHTEIL,ZUB_PREISBRUTTO,';
			$SQL .= 'ZUB_PREISANGELIEFERT,ZUB_ALTTEILWERT,ZUB_EINHEITENPROPREIS,ZUB_WAEHRUNG,';
			$SQL .= 'ZUB_ABSCHLUSSART,ZUB_ABSCHLUSSGRUND,';
			$SQL .= 'ZUB_USER,ZUB_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('DU',$_POST['txtZUB_BESTELLDATUM'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUB_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_WANRKORR'],true);		// in das richtige Feld speichern!!
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsZLA->FeldInhalt('ZAL_KEY'),true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsZLA->FeldInhalt('ZLA_KEY'),true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_ARTIKELNUMMER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_ARTIKELBEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_HERSTELLER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_VERSANDARTBEZ'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_SORTIMENT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUB_BESTELLMENGE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_MENGENEINHEITEN'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZUB_TAUSCHTEIL'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUB_PREISBRUTTO'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUB_PREISANGELIEFERT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUB_ALTTEILWERT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUB_EINHEITENPROPREIS'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_WAEHRUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUB_ABSCHLUSSART'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUB_ABSCHLUSSGRUND'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_ZUB_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$ZUB_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$ZUB_KEY;

			if(isset($ZUBZUBKEY) AND $ZUBZUBKEY!=='')
			{
				$SQL = 'UPDATE Zukaufbestellungen SET ZUB_ZUB_KEY = '.$ZUB_KEY;
				$SQL .= ' WHERE ZUB_KEY = '.$ZUBZUBKEY;

				if($DB->Ausfuehren($SQL,'',true)===false)
				{
					self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Aktualisieren einer verkn�ften Bestellung von '.$this->_LIE_NR.'. ZUBKEY:'.$ZUBZUBKEY.' sollte auf '.$ZUB_KEY.' verknpuepft werden.',$_POST['EXTERNEID']);
				}
			}
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';

			if($_POST['txtZUB_WANRKORR']!='')
			{
				if(!$Werkzeuge->BerechnePruefziffer($_POST['txtZUB_WANRKORR'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_PruefzifferFalsch'].': '.$TXT_Speichern['ZUB']['ZUB_WANRKORR'].'<br>';
				}
			}
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}


			$rsZUB = $DB->RecordSetOeffnen('SELECT * FROM zukaufbestellungen WHERE ZUB_key=' . $_POST['txtZUB_KEY'] . '');
			$FeldListe = '';
            if(isset($_POST['txtGEPRUEFT']))
            {
                $FeldListe = ', ZUB_GEPRUEFT = '.$DB->WertSetzen('ZUB', 'T', $AWISBenutzer->BenutzerName().', '.date('d.m.Y H:i'));
            }
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
                    //PG 03.01.2017 FeldinhaltFormat entfernt, da Bindevariablen benutzt werden und es sonst zu gewaltigen Problemen kommt.
                    //PG 16.01.2017: Format hinzuge�gt, da ansonsten die Alt<>Neu Logik nicht funktioniert.
                    $WertNeu=$Form->Format($rsZUB->FeldInfo($FeldName,'TypKZ'), $_POST[$Feld]);
                    $WertAlt=$Form->Format($rsZUB->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName]);
                    $WertDB=$Form->Format($rsZUB->FeldInfo($FeldName,'TypKZ'),$rsZUB->FeldInhalt($FeldName));

					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$DB->WertSetzen('ZUB', $rsZUB->FeldInfo($FeldName,'TypKZ'), $WertNeu);
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsZUB->FeldInhalt('ZUB_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE zukaufbestellungen SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', ZUB_user='.$DB->WertSetzen('ZUB','T',$AWISBenutzer->BenutzerName());
				$SQL .= ', ZUB_userdat=sysdate';
				$SQL .= ' WHERE ZUB_key=' . $DB->WertSetzen('ZUB','N0',$_POST['txtZUB_KEY']);

				$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ZUB'));
	
				$Form->DebugAusgabe(3,$DB->LetzterSQL(),$SQL);
			}
		}
	}// Zukaufbestellung




	//*******************************************************
	// Sonderanfragen
	//*******************************************************
	if(isset($_POST['txtZSA_WANR']))
	{
		$SQL = 'INSERT INTO ZukaufSonderanfragen';
		$SQL .= '(ZSA_WANR,ZSA_FIRMA, ZSA_DATUM_VON, ZSA_DATUM_BIS, ZSA_USER,ZSA_USERDAT';
		$SQL .= ')VALUES( ';
		$SQL .=  $DB->WertSetzen('ZSA','T',$_POST['txtZSA_WANR'],true);
		$SQL .= ', '. $DB->WertSetzen('ZSA','T',$_POST['txtZSA_FIRMA'],false);
		$SQL .= ', '. $DB->WertSetzen('ZSA','T',$_POST['txtZSA_DATUM_VON'],false);
		$SQL .= ', '. $DB->WertSetzen('ZSA','T',$_POST['txtZSA_DATUM_BIS'],false);
        $SQL .= ', '. $DB->WertSetzen('ZSA','T',$AWISBenutzer->BenutzerName(),true);
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ZSA'));
	}

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>