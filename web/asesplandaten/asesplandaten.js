$(document).ready(function () {
    key_APD_CLIENT(this);
    $("#txtAPD_PERS_NR").change(function () {
        pruefeAPD_PERS_NR();
    });


    $("#txtAPD_CLIENT_NO").change(function () {
        key_APD_CLIENT(this);
    });


});

function showMessage(message) {
    $("#"+ message).show();
    $("#cmdWeiter").hide();
}

function hideMessage(message) {
    $("#"+ message).hide();
    $("#cmdWeiter").show();
}

function pruefeDatum() {
    var datumVon =  $("#txtAPD_DATUM_VON").datepicker('getDate');
    var datumBis =  $("#txtAPD_DATUM_BIS").datepicker('getDate');

    var tageDiff = (datumBis -  datumVon) / (24*60*60*1000);

    if(tageDiff < 0 || tageDiff > 28){
        showMessage("FORMAT_DATUM");
        return false;
    }else{
        hideMessage("FORMAT_DATUM");
    }

    return true;
}

function pruefeAPD_PERS_NR() {
    var regExp = new RegExp("^[0-9,\\s]*$");

    if(!regExp.test( $("#txtAPD_PERS_NR").val())){
        showMessage("FORMAT_PERSNR");
        return false;
    }else{
        hideMessage("FORMAT_PERSNR");
    }
    return true;
}
