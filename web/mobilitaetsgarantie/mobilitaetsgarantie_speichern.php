<?php
global $MOB;
global $AWIS_KEY1;
try {
    if ($AWIS_KEY1==-1) { //Neuer DS

        $MOB->RechteMeldung(4);

        $SQL  ='INSERT';
        $SQL .=' INTO MOBILITAETSGARANTIEN';
        $SQL .='   (';
        $SQL .='     MOB_NAME1,';
        $SQL .='     MOB_NAME2,';
        $SQL .='     MOB_NAME3,';
        $SQL .='     MOB_FIL_ID,';
        $SQL .='     MOB_DATUM,';
        $SQL .='     MOB_STRASSE,';
        $SQL .='     MOB_POSTLEITZAHL,';
        $SQL .='     MOB_ORT,';
        $SQL .='     MOB_LAN_ATUSTAATS_NR,';
        $SQL .='     MOB_KFZ_KENNZEICHEN,';
        $SQL .='     MOB_ERSTZULASSUNG,';
        $SQL .='     MOB_KM_STAND,';
        $SQL .='     MOB_VERS_NUMMER,';
        $SQL .='     MOB_VERS_GUELTIG_AB,';
        $SQL .='     MOB_VERS_GUELTIG_BIS,';
        $SQL .='     MOB_GESAMTUMSATZ,';
        $SQL .='     MOB_BEMERKUNG,';
        $SQL .='     MOB_ATUCARDKUNDE,';
        $SQL .='     MOB_USER,';
        $SQL .='     MOB_ANR_ID,';
        $SQL .='     MOB_TITEL,';
        $SQL .='     MOB_KFZ_KENNZEICHEN_KOMP,';
        $SQL .='     MOB_BSA,';
        $SQL .='     MOB_VORGANGSNUMMER';
        $SQL .='   )';
        $SQL .='   VALUES';
        $SQL .='   (';
        $SQL .=  $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_NAME1']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_NAME2']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_NAME3']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_FIL_ID']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','DU',$_POST['txtMOB_DATUM']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_STRASSE']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_POSTLEITZAHL']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_ORT']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_LAN_ATUSTAATS_NR']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_KFZ_KENNZEICHEN']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_ERSTZULASSUNG']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_KM_STAND']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_VERS_NUMMER']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_VERS_GUELTIG_AB']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_VERS_GUELTIG_BIS']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_GESAMTUMSATZ']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_BEMERKUNG']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',isset($_POST['txtMOB_ATUCARDKUNDE'])?'1':'0');
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$MOB->AWISBenutzer->BenutzerName());
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_ANR_ID']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_TITEL']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',awisWerkzeuge::ArtNrKomprimiert($_POST['txtMOB_KFZ_KENNZEICHEN']));
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_BSA']);
        $SQL .= ', ' . $MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_VORGANGSNUMMER']);
        $SQL .='   )';
        $MOB->DB->Ausfuehren($SQL, '', true, $MOB->DB->Bindevariablen('MOB'));

        $SQL = 'SELECT seq_MOB_KEY.CurrVal AS KEY FROM DUAL';
        $rsKey = $MOB->DB->RecordSetOeffnen($SQL);
        $AWIS_KEY1 = $rsKey->FeldInhalt('KEY');
        $Meldung = $MOB->AWISSprachKonserven['MOB']['MOB_INSERT_OK'];
    } else { //gešnderter DS
        $MOB->RechteMeldung(2);
        $SQL  ='UPDATE MOBILITAETSGARANTIEN';
        $SQL .=' SET';
        $SQL .='   MOB_NAME1                = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_NAME1']);
        $SQL .=' , MOB_NAME2                = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_NAME2']);
        $SQL .=' , MOB_NAME3                = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_NAME3']);
        $SQL .=' , MOB_FIL_ID               = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_FIL_ID']);
        $SQL .=' , MOB_DATUM                = '.$MOB->DB->WertSetzen('MOB','DU',$_POST['txtMOB_DATUM']);
        $SQL .=' , MOB_STRASSE              = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_STRASSE']);
        $SQL .=' , MOB_POSTLEITZAHL         = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_POSTLEITZAHL']);
        $SQL .=' , MOB_ORT                  = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_ORT']);
        $SQL .=' , MOB_LAN_ATUSTAATS_NR     = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_LAN_ATUSTAATS_NR']);
        $SQL .=' , MOB_KFZ_KENNZEICHEN      = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_KFZ_KENNZEICHEN']);
        $SQL .=' , MOB_ERSTZULASSUNG        = '.$MOB->DB->WertSetzen('MOB','D',$_POST['txtMOB_ERSTZULASSUNG']);
        $SQL .=' , MOB_KM_STAND             = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_KM_STAND']);
        $SQL .=' , MOB_VERS_NUMMER          = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_VERS_NUMMER']);
        $SQL .=' , MOB_VERS_GUELTIG_AB      = '.$MOB->DB->WertSetzen('MOB','D',$_POST['txtMOB_VERS_GUELTIG_AB']);
        $SQL .=' , MOB_VERS_GUELTIG_BIS     = '.$MOB->DB->WertSetzen('MOB','D',$_POST['txtMOB_VERS_GUELTIG_BIS']);
        $SQL .=' , MOB_GESAMTUMSATZ         = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_GESAMTUMSATZ']);
        $SQL .=' , MOB_BEMERKUNG            = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_BEMERKUNG']);
        $SQL .=' , MOB_ATUCARDKUNDE         = '.$MOB->DB->WertSetzen('MOB','T',isset($_POST['txtMOB_ATUCARDKUNDE'])?'1':'0');
        $SQL .=' , MOB_USER                 = ' . $MOB->DB->WertSetzen('MOB','T',$MOB->AWISBenutzer->BenutzerName()) . '';
        $SQL .=' , MOB_USERDAT              = sysdate';
        $SQL .=' , MOB_ANR_ID               = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_ANR_ID']);
        $SQL .=' , MOB_TITEL                = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_TITEL']);
        $SQL .=' , MOB_KFZ_KENNZEICHEN_KOMP = '.$MOB->DB->WertSetzen('MOB','T',awisWerkzeuge::ArtNrKomprimiert($_POST['txtMOB_KFZ_KENNZEICHEN']));
        $SQL .=' , MOB_BSA                  = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_BSA']);
        $SQL .=' , MOB_VORGANGSNUMMER       = '.$MOB->DB->WertSetzen('MOB','T',$_POST['txtMOB_VORGANGSNUMMER']);
        $SQL .=' WHERE MOB_KEY                = ' . $MOB->DB->WertSetzen('MOB','N0',$AWIS_KEY1,false);

        $MOB->DB->Ausfuehren($SQL, '', true, $MOB->DB->Bindevariablen('MOB'));
        $Meldung = $MOB->AWISSprachKonserven['MOB']['MOB_UPDATE_OK'];
    }
    if($Meldung){
        $MOB->Form->Hinweistext($Meldung,awisFormular::HINWEISTEXT_OK);
    }

} catch (awisException $ex) {
    $Information = '<br>Zeile: ' . $ex->getLine();
    $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $MOB->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
    $MOB->Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $MOB->Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}
?>