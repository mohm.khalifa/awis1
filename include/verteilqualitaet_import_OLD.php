<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

ini_set('max_execution_time', 0);
date_default_timezone_set('Europe/Berlin');

error_reporting("E_ALL");

class VERTEILQUALITAETIMPORT
{

private static $instance = NULL;
private $_DB;            
protected $_AWISBenutzer;
private $flagIsSunday = false;	
//private $dirPath = '../../../daten/daten/pccommon/Beierl/FILPEPIS';	
private $dirPath = '/daten/daten/pcedv11/Daten/FILPEPIS';
private $importFile = '';
private $WerbeFilPers = array();
protected $aktZeile;

 public function __construct() {
  $this->_AWISBenutzer = awisBenutzer::Init();
  $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
  $this->_DB->Oeffnen();
}

  
public function getInstance() {
   if(self::$instance === NULL) {
      self::$instance = new self;
   }

   return self::$instance;
}

private function __clone() {}
  


public function import()
{
	$this->isSunday();

	if($this->flagIsSunday == true)
	{
		$this->identifyFile();	
		if($this->importFile != '')
		{
			$this->delWERBEFILPERS();
			$this->loadFile();
			$this->bereinigeDS();
		}
	}
	else 
	{
		echo "KEIN RUN EXIT-->CODE(0)";		
	}
	
}

private function loadFile()
{
	$fd = null;
	
	echo "LOADFILE";

	echo "PFAD_DIR".$this->dirPath;
	echo "Importdatei".$this->importFile;
	
    if(($fd = fopen($this->dirPath."/".$this->importFile,'r')) === false) {
      echo "Fehler beim �ffnen der Datei";      
    }
    else {
		   while(! feof($fd)) {
                $this->aktZeile = fgets($fd);
                $this->WerbeFilPers = explode(';',$this->aktZeile);
				$this->safeDS();
                //var_dump($this->WerbeFilPers);
			}
            fclose($fd);   	
    }    
		
}

private function delWERBEFILPERS()
{
	$SQL = "TRUNCATE TABLE WERBEFILPERS";
	
 	if($this->_DB->Ausfuehren($SQL)===false) {
    }
}

public function bereinigeDS()
{
	echo "Bereinige";
	
	$SQL = 'Select DISTINCT WPF_KUERZEL FROM WERBEFILTERPERS';
	
	$rsKuerzel = $this->_DB->RecordSetOeffnen($SQL);
	
	while(!$rsKuerzel->EOF())
	{
		$SQL = 'Delete from WERBEFILPERS WHERE WFP_PERSNR IN  ';
		$SQL .= '(Select WFP_PERSNR FROM WERBEFILPERS WHERE ';
		$SQL .= ' WFP_MO ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_DI ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));	
		$SQL .= ' AND WFP_MI ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_DO ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_FR ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' AND WFP_SA ='.$this->_DB->FeldInhaltFormat('T',$rsKuerzel->FeldInhalt('WPF_KUERZEL'));
		$SQL .= ' )';
		
		if($this->_DB->Ausfuehren($SQL)===false) {
    	}
		
		$rsKuerzel->DSWeiter();
	}
	
	//$SQL = 'Delete from WERBEFILPERS WHERE WFP_PERSNR IN  ';
	$SQL = 'Select WFP_FILNR,WFP_PERSNR FROM WERBEFILPERS WHERE ';
	$SQL .= ' WFP_MO is null ';
	$SQL .= ' AND WFP_DI is null ';	
	$SQL .= ' AND WFP_MI is null ';
	$SQL .= ' AND WFP_DO is null ';
	$SQL .= ' AND WFP_FR is null ';
	$SQL .= ' AND WFP_SA is null ';
	//$SQL .= ' )';
	
	echo $SQL;
	
	$rsLoesche = $this->_DB->RecordSetOeffnen($SQL);
	
	while(!$rsLoesche->EOF())
	{
		$SQL = 'Delete from WERBEFILPERS WHERE WFP_FILNR='.$this->_DB->FeldInhaltFormat('NO',$rsLoesche->FeldInhalt('WFP_FILNR'));
		$SQL .= ' AND WFP_PERSNR='.$this->_DB->FeldInhaltFormat('T',$rsLoesche->FeldInhalt('WFP_PERSNR'));
		
		echo $SQL;
		
		if($this->_DB->Ausfuehren($SQL)===false) {
    	}
		
		$rsLoesche->DSWeiter();
	}
	
	
	echo $SQL;
		
	if($this->_DB->Ausfuehren($SQL)===false) {
    }
	
}


private function safeDS()
{
	$SQL = "INSERT INTO WERBEFILPERS (WFP_FILNR,WFP_PERSNR,WFP_NAME,WFP_MO,";
    $SQL .= "WFP_DI,WFP_MI,WFP_DO,WFP_FR,WFP_SA,";
    $SQL .= "WFP_USER,WFP_USERDAT) VALUES (";
    $SQL .= ' ' . $this->_DB->FeldInhaltFormat('NO',$this->WerbeFilPers[0],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[1],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[2],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[3],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[4],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[5],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[6],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[7],false);
    $SQL .= ',' . $this->_DB->FeldInhaltFormat('T',$this->WerbeFilPers[8],false);
    $SQL .= ',\'' . $this->_AWISBenutzer->BenutzerName() . '\'';
    $SQL .= ',SYSDATE';
    $SQL .= ')';
	
     if($this->_DB->Ausfuehren($SQL)===false) {
     }

    array_splice($this->WerbeFilPers,0);
    
}



private function identifyFile()
{
	$FILEDATE = date('ymd');
	$MAXFILE = '';
	echo $FILEDATE - 6;
	
	echo "Aktuelles_DATUM:".$FILEDATE."\n";
	
	for($i = ($FILEDATE -6);$i<=$FILEDATE;$i++)
	{
		echo "Variable_I:".$i;
		
		if ($handle = opendir($this->dirPath)) {
    	while (false !== ($file = readdir($handle))) {
        	if ($file != "." && $file != "..") {
        		
        		echo "SUBSTR:".substr($file,10,6)."\n";
        		
        		if($MAXFILE >= ($FILEDATE -6))
        		{		
            		$MAXFILE = substr($file,10,6);
        			$this->importFile = $file;
        		}
        		elseif($MAXFILE == '')
        		{
        			$MAXFILE = substr($file,10,6);
        			$this->importFile = $file;
        		}
        		
        }
       }
    
       closedir($handle);
    }		
	}

	echo "AKT_IMPORT_FILE:".$this->importFile;
} 


private function isSunday()
{
	echo "Datum:".date('D');
	
	if(date('D') == 'Mon')
	{
		$this->flagIsSunday = true;
	}
	else 
	{
		$this->flagIsSunday = false;		
	}
}



}

?>