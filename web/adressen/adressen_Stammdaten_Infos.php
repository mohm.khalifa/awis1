<?php
global $Recht3002;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $AWISCursorPosition;	// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

try 
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('AZG','%');
    $TextKonserven[]=array('ADT','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Liste','lst_JaNein');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht3002 = $AWISBenutzer->HatDasRecht(3002);    
    
    if($Recht3002==0)
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
        die();
    }

    $Bedingung = '';
    
    //********************************************************
    // Parameter ?
    //********************************************************
    if(isset($_POST['cmdDSZurueck_x']))
    {
        $SQL = 'SELECT ADT_KEY FROM (SELECT ADT_KEY FROM ADRESSENINFOTYPEN';
        $SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
        $SQL .= ' OR ADT_AZG_KEY = 1';
        $SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON (AZM_AZG_KEY=AZG_KEY';
        $SQL .= ' and AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
        $SQL .= ') or AZM_AZG_KEY = 1';
        $SQL .= ' AND UPPER(ADT_INFORMATION) < '. strtoupper($Form->Format('T',$_POST['txtADT_INFORMATION']));
        $SQL .= ' ORDER BY ADT_INFORMATION DESC';
        $SQL .= ') WHERE ROWNUM = 1';
    
        $rsADT = $DB->RecordSetOeffnen($SQL);
        if(isset($rsADT['ADT_KEY'][0]))
        {
            $AWIS_KEY1=$rsADT['ADT_KEY'][0];
        }
        else
        {
            $AWIS_KEY1 = $_POST['txtADT_KEY'];
        }
    }
    elseif(isset($_POST['cmdDSWeiter_x']))
    {
        $SQL = 'SELECT ADT_KEY FROM (SELECT ADT_KEY FROM ADRESSENINFOTYPEN';
        $SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
        $SQL .= ' OR ADT_AZG_KEY = 1';
        $SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON (AZM_AZG_KEY=AZG_KEY';
        $SQL .= ' and AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
        $SQL .= ') or AZM_AZG_KEY = 1';
        $SQL .= ' AND UPPER(ADT_INFORMATION) > '.strtoupper($Form->Format('T',$_POST['txtADT_INFORMATION']));
        $SQL .= ' ORDER BY ADT_INFORMATION ASC';
        $SQL .= ') WHERE ROWNUM = 1';
        $rsADT = $DB->RecordSetOeffnen($SQL);
    
        if(isset($rsADT['ADT_KEY'][0]) AND $rsADT['ADT_KEY'][0]!='')
        {
            $AWIS_KEY1=$rsADT['ADT_KEY'][0];
        }
        else
        {
            $AWIS_KEY1 = $_POST['txtADT_KEY'];
        }
    }
    if($AWIS_KEY1>0)
    {
        $Bedingung .= ' AND ADT_KEY='.$Form->Format('Z',$AWIS_KEY1).'';
    }
    elseif(isset($_GET['ADT_KEY']))
    {
        $Bedingung .= ' AND ADT_KEY ='.$Form->Format('Z',$_GET['ADT_KEY']).'';
    }
    elseif(isset($_POST['txtADT_KEY']))
    {
        $Bedingung .= ' AND ADT_KEY  ='.$Form->Format('Z',$_POST['txtADT_KEY']).'';
    }
    
    //********************************************************
    // Daten suchen
    //********************************************************
    $SQL = 'SELECT *';
    $SQL .= ' FROM ADRESSENINFOTYPEN';
    $SQL .= ' INNER JOIN ADRESSENZUGRIFFSGRUPPEN ON ADT_AZG_KEY = AZG_KEY';
    $SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON (AZM_AZG_KEY=AZG_KEY';
    $SQL .= ' and AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
    $SQL .= ') or AZM_AZG_KEY = 1';
    
    if($Bedingung!='')
    {
        $SQL .= ' WHERE ' . substr($Bedingung,4);
    }
    if(!isset($_GET['Sort']))
    {
        $SQL .= ' ORDER BY ADT_VERWENDUNG, ADT_INFORMATION';
    }
    else
    {
        $SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
    }

    $rsADT = $DB->RecordSetOeffnen($SQL);
    $rsADTZeilen = $rsADT->AnzahlDatensaetze();

    //********************************************************
    // Daten anzeigen
    //********************************************************
    if(($rsADTZeilen<>1 AND !isset($_GET['ADT_KEY']))OR isset($_GET['Liste']))						// Liste anzeigen
    {
        $Form->Formular_Start();
        $Form->ZeileStart();
        
        if(($Recht3002&6)>0)
        {
            $Icons[] = array('new','./adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos&ADT_KEY=0');
            $Form->Erstelle_ListeIcons($Icons,38,-1);
        }
    
        $Link = './adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=ADT_INFORMATION'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADT_INFORMATION'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADT']['ADT_INFORMATION'],200,'',$Link);
        
        $Link = './adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=ADT_VERWENDUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ADT_VERWENDUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ADT']['ADT_VERWENDUNG'],300,'',$Link);
        
        $Link = './adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
        $Link .= '&Sort=AZG_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AZG_BEZEICHNUNG'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['AZG']['AZG_BEZEICHNUNG'],300,'',$Link);
        $Form->ZeileEnde();
    
        for($ADTZeile=0;$ADTZeile<$rsADTZeilen;$ADTZeile++)
        {
            $Form->ZeileStart();
            $Icons = array();
            if(($Recht3002&6)>0)	// �ndernrecht
            {
                $Icons[] = array('edit','./adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos&ADT_KEY='.$rsADT->FeldInhalt('ADT_KEY'));
                $Icons[] = array('delete','./adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos&Del='.$rsADT->FeldInhalt('ADT_KEY'));
            }
            $Form->Erstelle_ListeIcons($Icons,38,($ADTZeile%2));
            $Form->Erstelle_ListenFeld('ADT_INFORMATION',$rsADT->FeldInhalt('ADT_INFORMATION'),0,200,false,($ADTZeile%2));
            $Form->Erstelle_ListenFeld('ADT_VERWENDUNG',$Form->WerteListe($AWISSprachKonserven['ADT']['lst_ADT_VERWENDUNG'],$rsADT->FeldInhalt('ADT_VERWENDUNG'),''),0,300,false,($ADTZeile%2));
            $Form->Erstelle_ListenFeld('AZG_BEZEICHNUNG',$rsADT->FeldInhalt('AZG_BEZEICHNUNG'),0,300,false,($ADTZeile%2));
            $Form->ZeileEnde();
    		
    		$rsADT->DSWeiter();
        }
    
        $Form->Formular_Ende();
    }			// Eine einzelne Adresse
    else										// Eine einzelne oder neue Adresse
    {
        echo '<form name=frmADRESSENINFOTYPEN action=./adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos method=POST  enctype="multipart/form-data">';
        $AWIS_KEY1 = $rsADT->FeldInhalt('ADT_KEY')!=''?$_GET['ADT_KEY']:0;
        echo '<input type=hidden name=txtADT_KEY value='.$AWIS_KEY1. '>';
        
        $Form->Formular_Start();
        $OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
            
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./adressen_Main.php?cmdAktion=Stammdaten&Seite=Infos&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_USER')));
        $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_USERDAT')));
        $Form->InfoZeile($Felder,'');
        
        $TabellenKuerzel='ADT';
        $EditRecht=(($Recht3002&2)!=0);
        if($AWIS_KEY1==0)
        {
            $EditRecht=true;
        }

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_INFORMATION'].':',150);
        $Form->Erstelle_TextFeld('ADT_INFORMATION',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_INFORMATION')),50,350,$EditRecht);
        $AWISCursorPosition='txtADT_INFORMATION';
        $Form->ZeileEnde();
            
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_FORMAT'].':',150);
        $Liste=explode('|',$AWISSprachKonserven['ADT']['lst_ADT_FORMAT']);
        $Form->Erstelle_SelectFeld('ADT_FORMAT',$rsADT->FeldInhalt('ADT_KEY')!=''?$rsADT->FeldInhalt('ADT_FORMAT'):'',200,$EditRecht,'','','','','',$Liste);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_VERWENDUNG'].':',150);
        $Liste=explode('|',$AWISSprachKonserven['ADT']['lst_ADT_VERWENDUNG']);
        $Form->Erstelle_SelectFeld('ADT_VERWENDUNG',$rsADT->FeldInhalt('ADT_KEY')!=''?$rsADT->FeldInhalt('ADT_VERWENDUNG'):'',200,$EditRecht,'','','','',1,$Liste);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_ANZAHL'].':',150);
        $Form->Erstelle_TextFeld('ADT_ANZAHL',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_ANZAHL')),3,100,$EditRecht,'','','','N0','','',1);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_SORTIERUNG'].':',150);
        $Form->Erstelle_TextFeld('ADT_SORTIERUNG',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_SORTIERUNG')),3,100,$EditRecht,'','','','N0','','',10);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_ZEICHEN'].':',150);
        $Form->Erstelle_TextFeld('ADT_ZEICHEN',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_ZEICHEN')),3,100,$EditRecht,'','','','N0','','',20);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_BREITE'].':',150);
        $Form->Erstelle_TextFeld('ADT_BREITE',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_BREITE')),3,100,$EditRecht,'','','','N0','','',220);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_BEMERKUNG'].':',150);
        $Form->Erstelle_TextFeld('ADT_BEMERKUNG',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_BEMERKUNG')),100,100,$EditRecht,'','','','T');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_DATENQUELLE'].':',150);
        $Form->Erstelle_Textarea('ADT_DATENQUELLE',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_DATENQUELLE')),100,100,5,$EditRecht);
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_SUCHFELD'].':',150);
        $KateogrieAlt = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
        $Form->Erstelle_SelectFeld('ADT_SUCHFELD',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_SUCHFELD')),130,$EditRecht,'','','','A','',$KateogrieAlt,'');
        $Form->ZeileEnde();
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_STATUS'].':',150);
        $KateogrieAlt = explode("|",$AWISSprachKonserven['ADT']['lst_ADT_STATUS']);
        $Form->Erstelle_SelectFeld('ADT_STATUS',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_STATUS')),130,$EditRecht,'','','','A','',$KateogrieAlt,'');
        $Form->ZeileEnde();
            
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ADT']['ADT_AZG_KEY'].':',150);
        $SQL = 'SELECT AZG_KEY, AZG_BEZEICHNUNG AS Bereich';
        $SQL .= ' FROM ADRESSENZUGRIFFSGRUPPEN';
        $SQL .= ' LEFT OUTER JOIN ADRESSENZUGRIFFSGRPMITGLIEDER ON AZM_AZG_KEY=AZG_KEY';
        $SQL .= ' WHERE AZM_XBN_Key=0' . $AWISBenutzer->BenutzerID();
        $SQL .= ' OR AZG_KEY = 1';		// oder �ffentlich!
        $SQL .= ' ORDER BY AZG_BEZEICHNUNG';
        $Form->Erstelle_SelectFeld('ADT_AZG_KEY',($AWIS_KEY1===0?'':$rsADT->FeldInhalt('ADT_AZG_KEY')),200,true,$SQL);
        $Form->ZeileEnde();
        $Form->Formular_Ende();
        
        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();
        if(($Recht3002&(2+4+256))!==0)		//
        {
            $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
        }
            
        $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_DSZurueck'],'Z');
		$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
            
        $Form->SchaltflaechenEnde();
        echo '</form>';
    }

	if($AWISCursorPosition!='')
    {
       echo '<Script Language=JavaScript>';
	   echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	   echo '</Script>';
    }  
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>