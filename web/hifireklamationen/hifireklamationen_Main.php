<?php
echo "<html>";
echo "<head>";
echo '<meta http-equiv="cache-control" content="no-cache">';
echo '<meta http-equiv="cache-control" content="no-store">';
echo '<meta http-equiv="cache-control" content="must-revalidate">';
echo '<meta http-equiv="cache-control" content="post-check=0, pre-check=0, false">';
echo '<meta http-equiv="pragma" content="no-cache">';
echo "<title>Awis - ATU webbasierendes Informationssystem</title>";

require_once("db.inc.php");		// DB-Befehle
require_once("register.inc.php");
require_once("sicherheit.inc.php");

global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

echo "</head>";
echo "<body>";

global $cmdAktion;

clearstatcache();

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1400);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Hifireklamationen',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$cmdAktion='';

if (isset($_REQUEST['cmdAktion']))
{
	$cmdAktion=$_REQUEST['cmdAktion'];
}

awis_RegisterErstellen(1400, $con, $cmdAktion);

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

echo "</body>";
echo "</html>";
?>