<?php
/**
 * Job_import.php
 * 
 * Mit diesem Job werden die Seminar und Seminarteilnehmer in AWIS eingelesen
 * 
 * @author Thomas Riedl
 * @copyright ATU Auto-Teile-Unger
 * @version 20100420
 */
require_once 'awis_personaleinsaetze.inc';
require_once 'awisJobVerwaltung.inc';
require_once('awisDatenbank.inc');

try
{
	$Job = new awisJobProtokoll(14,'Starte Import.');
	
	//$Seminarplan = new awis_Seminarplan('riedl_t');
	$Personaleinsatz = new awis_Personaleinsaetze('riedl_t');
	//$Job->SchreibeLog(awisJobProtokoll::TYP_START,1,'Import beginnt.');	

	$Personaleinsatz->DebugLevel(10);
		
	//Teilnehmer importieren
	$Personaleinsatz->PersonaleinsatzExport();	
	
	//$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,1,'Import abgeschlossen.');	
	
}
catch (Exception $ex)
{
#	echo PHP_EOL.'Seminarplanimport: Fehler: '.$ex->getMessage().PHP_EOL;	

	if(isset($Job))
	{
		awisWerkzeuge::EMail(array('shuttle@de.atu.eu'),'ERROR SEMINARPLANIMPORT','',3,'','awis@de.atu.eu');
		//$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,3,'Fehler:'.$ex->getMessage());
	}
	
}
?>
