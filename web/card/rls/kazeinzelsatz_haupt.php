<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

$intBestAend=$intKazeinzelsatzHauptBestDsAendern;		//Recht wird in card_rechte.inc.php gesetzt

//echo "intMagId: ".$intMagId."<br>";
//echo "intKaa_tabelle: ".$intKaa_tabelle."<br>";

//Neuen Datensatz Aufteilung Kontoauszug in den Tabellen Kontoauszuege_Aufteilung und in Mahngebuehren bzw. Rechnungen_Konto eintragen

if(isset($dblKaa_betrag))
{
	//Insert-Trigger TRG_KONTOAUSZUEGE_AUFTEILUNG f�gt ein in mag_id von mahngebuehren / rek_buchid von rechnungen_konto
	
	if(isset($intUpdateId))	//Wenn $intUpdateId �bergeben worden ist, so wird bestehender Datensatz ge�ndert
	{
		$strSql = "update KONTOAUSZUEGE_AUFTEILUNG 
					set kaa_betrag=".$dblKaa_betrag.",kaa_aend_benutzer='".$PHP_AUTH_USER."',kaa_aend_datum=sysdate
					where kaa_id=".$intUpdateId;
	}
	else	//neuer Datensatz wird eingef�gt
	{
		$strSql = "insert into KONTOAUSZUEGE_AUFTEILUNG (KAA_ID, KAA_TABELLE, KAA_FREMDID, KAA_BETRAG, KAA_KONTOAUSZUGID,
				KAA_DATUM, KAA_AEND_BENUTZER, KAA_AEND_DATUM, KAA_LOESCH)
				values(seq_kaa_id.nextval,".$intKaa_tabelle.",".$intMagId.",".$dblKaa_betrag.",".$intKazId.",'".
				$strKaa_datum."','".$PHP_AUTH_USER."',sysdate,0)";
	}
	
	$arrKaa = abfrage("ora",$strSql,1);
}

//Datensatz soll gel�scht werden

if(isset($intLoeschId))
{
	//Gel�scht wird in den Tabellen Rechnungen_Konto sowie Mahngebuehren, welche durch einen Trigger
	//die L�schung in der Tabelle Kontoauszuege_Aufteilung ausf�hren


	if($intTabelle==1)
	{
		$strSql = "update RECHNUNGEN_KONTO set rek_kontoauszugid=0,rek_aend_benutzer='".$PHP_AUTH_USER."',
				rek_aend_datum=sysdate where rek_buchid=".$intLoeschId;
	}
	else
	{
		$strSql = "update MAHNGEBUEHREN set mag_kontoauszugid=0,mag_aend_benutzer='".$PHP_AUTH_USER."',
				mag_aend_datum=sysdate where mag_id=".$intLoeschId;
	}

	abfrage("ora",$strSql);
}

if(isset($dblBetrrls))
{
	$strSql = "update kontoauszuege set ika_rlsbetrag=".$dblBetrrls.",ika_aend_benutzer='".$PHP_AUTH_USER."',
				ika_aend_datum=sysdate where ika_id=".$intKazId;
	
	abfrage("ora",$strSql);


	//echo $dblBetrrls."<br>";
	//echo $intKazId."<br>";
}

?>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
	echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.startzeit();	//Zeitmessung
<?php } ?>
</script>
<html>
<head>
<title>Kontoauszug Id <?php echo $intKazId; ?></title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript">
</script>
<?php

//Abfrage f�r Daten Kontoauszug

$strSql = "select IKA_ID, IKA_AUSZUGNR, IKA_KONTONR, IKA_BANK, IKA_AUSZUGSDATUM, IKA_BUCHUNGSDATUM, 
IKA_VALUTADATUM, IKA_BETRAG, IKA_WAEHRUNG, IKA_TEXTSCHLUESSEL, IKA_BUCHUNGSTEXT, IKA_VERWENDUNGSZWECK1, 
IKA_VERWENDUNGSZWECK2, IKA_VERWENDUNGSZWECK3, IKA_KUNDENREFERENZ, IKA_STARTSALDO, IKA_SCHLUSSALDO, IKA_AUFTRAGGEBER1, 
IKA_AUFTRAGGEBER2, IKA_AUFTRAGGEBER_BLZ, IKA_AUFTRAGGEBER_KONTO, IKA_RLSBETRAG,
nvl(BA_BUCHUNGSART,'Buchungsart fehlt') BA_BUCHUNGSART, IKA_TRANSFER_IN_KONTOAUSZUEGE,
nvl(ban_kuerzel,'') ban_kuerzel
from kontoauszuege, buchungsarten, bank
where IKA_BUCHUNGSART = BA_BUCHUNGSARTID(+)
and ika_transfer_in_kontoauszuege in (1,3) 
and ika_loesch = 0
and (ika_kontonr=ban_kontonr(+) and ika_bank=ban_blz(+))
and ika_id = ".$intKazId;


$arrKaz = abfrage("ora",$strSql);

if($arrKaz["zeilen"]!=0)
{
	$arrKaz=$arrKaz["trefferarray"];
	
	$intAlt=0;
	
	if($arrKaz["IKA_TRANSFER_IN_KONTOAUSZUEGE"][0]==3)
		$intAlt=1;
	
	//Abfrage Summe Aufteilung Kontoauszug
	
	$strSql="select nvl(sum(KAA_BETRAG),0) summe
	from KONTOAUSZUEGE_AUFTEILUNG
	where KAA_KONTOAUSZUGID=".$intKazId." and KAA_LOESCH=0";
	
	$arrSum = abfrage("ora",$strSql);
	$dblSumme=$arrSum["trefferarray"]["SUMME"][0];
	
	//Abfrage Daten Aufteilung Kontoauszug
	
	$strDb = "ora";
	
	$strSql="select KAA_ID,KAA_DATUM,KAA_TABELLE,KAA_FREMDID,KAA_BETRAG
	from KONTOAUSZUEGE_AUFTEILUNG
	where KAA_KONTOAUSZUGID=".$intKazId." and KAA_LOESCH=0
	order by KAA_DATUM";
	
	include("card/card_blaettern_haupt.inc.php");	//Bl�ttern
	
	$arrKazAuft = abfrage("ora",$strSqlScroll);
	
	
	$intKazAuftZeilen = $arrKazAuft["zeilen"];
	
	$arrKazAuft=$arrKazAuft["trefferarray"];
	?>
	<script language="javascript">
	
	function init()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.endezeit();	//Zeitmessung
		<?php } ?>
			
		document.ondblclick = zoom;		//Zoomfenster
	}
	
	function aenderungzeilerest()  //Wird aus Funktion aenderungzeile() in card_allgmeinefunktionen.js aufgerufen
	{
		//try{ document.getElementById('neu').setAttribute("onclick","");	} catch(e){}	//Funktion onclick auf Button neuer Dastensatz ausschalten
		
		document.getElementById('but'+window.event.srcElement.id.slice(3)).style.display='inline';		//Speicher-Button wird sichtbar
	}
	
	//Blendet Speicherbutton f�r Feld Betrag RLS ein
	
	function betragrls()
	{
		document.getElementById("butrlsbetr").style.display='inline';
	}
	
	//Wird bei Klick auf Speicherbutton f�r Feld Betrag RLS ausgef�hrt
	
	function rlsbetragsend()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("RLS Betrag �ndern");	//Zeitmessung
		<?php } ?>
		
		var objBetrrls = document.getElementById("betrrls");
		if(!betragspruef(objBetrrls,8))
		return;
				
		var dblBetrrls = datenbankbetrag(objBetrrls.value);
				
		var objBetrkaz = document.getElementById("betrkaz");
		var dblBetrkaz = datenbankbetrag(objBetrkaz.value);
		
		//Wenn Kontoauszugsbetrag negativ ist, muss negativer Betrag eingegeben werden
		//Wenn Kontoauszugsbetrag positiv ist, muss positiver Betrag eingegeben werden
		
		var strAusgabe = "";
		
		if(dblBetrkaz>=0 && dblBetrrls < 0)
			strAusgabe = "positiv";
		
		if(dblBetrkaz<0 && dblBetrrls >= 0)
			strAusgabe = "negativ";
			
		if(strAusgabe!="")
		{
			alert("Geben Sie bitte einen "+strAusgabe+"en Betrag ein!");
			objBetrrls.select();
			return;	
		}
		
		if(dblBetrrls>=0)
		{	
			if(dblBetrrls > dblBetrkaz)
			{
				alert("RLS-Betrag darf nicht gr��er als der Betrag des Kontoauszuges ("+objBetrkaz.value+") sein!");
				objBetrrls.select();
				return;
			}
			if(dblBetrrls < <?php echo str_replace(",",".",$dblSumme); ?>)
			{
				alert("RLS-Betrag muss mindestens <?php echo eurobetrag($dblSumme); ?> betragen!");
				objBetrrls.value =	'<?php echo eurobetrag($dblSumme); ?>';
				objBetrrls.select();
				return;
			}
		}
		else
		{
			if(dblBetrrls < dblBetrkaz)
			{
				alert("RLS-Betrag darf nicht kleiner als der Betrag des Kontoauszuges ("+objBetrkaz.value+") sein!");
				objBetrrls.select();
				return;
			}
			if(dblBetrrls > <?php echo str_replace(",",".",$dblSumme); ?>)
			{
				alert("RLS-Betrag muss mindestens <?php echo eurobetrag($dblSumme); ?> betragen!");
				objBetrrls.value =	'<?php echo eurobetrag($dblSumme); ?>';
				objBetrrls.select();
				return;
			}
		}
		
		var parameter = "&dblBetrrls="+dblBetrrls;
		
		location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+parameter;
	}
	
	
	//Wird aufgerufen wenn neuer Datensatz angef�gt werden soll
	
	function neu()
	{
		if(document.getElementById("butrlsbetr").style.display!='inline')	//Wenn der Button zum Senden des Feldes RLS-Betrag nicht eingeblendet ist
		{
		
			//Werte aus Rechnungen_Konto bzw. Mahngebuehren �bernehmen
			var arrFelder = new Array("dat","bet","rnr");
			
			for(i=0;i<arrFelder.length;i++)
			{
				<?php 
				if($intKaa_tabelle==1)		//Tabelle Rechnungen_Konto
				{	?>
				document.getElementById(arrFelder[i]).value = top.dialogArguments.parent.cardiframe.document.getElementById(arrFelder[i]+"<?php echo $intMagId;?>").value;
				<?php }	
				
				if($intKaa_tabelle==2)		//Tabelle Mahngebuehren
				{	?>
				document.getElementById(arrFelder[i]).value = top.dialogArguments.parent.mahngebuehren_haupt.document.getElementById(arrFelder[i]+"<?php echo $intMagId;?>").value;
				<?php }	?>
			}
			
			<?php 
			if($intKaa_tabelle==1)		//Tabelle Rechnungen_Konto
			{	?>
			document.getElementById("bua").value=listeneintrag(top.dialogArguments.parent.cardiframe.document.getElementById("bua<?php echo $intMagId;?>"));
			<?php }	
			
			if($intKaa_tabelle==2)		//Tabelle Mahngebuehren
			{	?>
			document.getElementById("bua").value=listeneintrag(top.dialogArguments.parent.mahngebuehren_haupt.document.getElementById("mat<?php echo $intMagId;?>"));
			<?php }	?>
			
			aenderungzeile('aenderungzeilerest');	//nicht betroffene Felder ausgrauen
			
			document.getElementById('but'+window.event.srcElement.id.slice(3)).style.display='inline';		//Speicher-Button wird sichtbar
			
			window.event.srcElement.style.display = "none";		//Button neu ausblenden
			
			document.getElementById("dat").style.display = "inline";
			
			neuezeileein();
			
			//document.getElementById("bet").select();		//Fokus auf Betragsfeld setzen
		}
		else
		{
			alert("�nderung RLS-Betrag muss zuerst best�tigt werden!");
		}
	}
	
	function aendsend()
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Einzelsatz Kontoauszug �ndern");	//Zeitmessung
		<?php } ?>
				
		var obj = window.event.srcElement;
		
		var buchid = obj.id.slice(3);
		
		var datobj = document.getElementById("dat"+buchid);
		var betobj = document.getElementById("bet"+buchid);
		
		var objBetrrls	= document.getElementById("betrrls");
		var dblBetrrls = datenbankbetrag(objBetrrls.value);
		
		if(dblBetrrls==0)
		{
			alert("Geben Sie bitte einen RLS-Betrag ein!");
			document.getElementById("dat").style.display = "none";
			neuezeileaus();
			document.getElementById("neu").style.display = "inline";		//Button neu ausblenden
			objBetrrls.value = document.getElementById("betrkaz").value;
			objBetrrls.select();
			document.getElementById("butrlsbetr").style.display='inline';	//Speicherbutton einblenden
			return;
		}
		
		//Betragspr�fung
		if(!betragspruef(betobj,8))
			return;
		
		var objBetrKaz = document.getElementById("betrkaz");
		var dblBetrKaz = datenbankbetrag(objBetrKaz.value);
		
		var dblBetrneu = datenbankbetrag(betobj.value);
		
		//Wenn Kontoauszugsbetrag negativ ist, muss negativer Betrag eingegeben werden
		//Wenn Kontoauszugsbetrag positiv ist, muss positiver Betrag eingegeben werden
		
		var strAusgabe = "";
		
		if(dblBetrKaz>=0 && dblBetrneu < 0)
			strAusgabe = "positiv";
		
		if(dblBetrKaz<0 && dblBetrneu >= 0)
			strAusgabe = "negativ";
			
		if(strAusgabe!="")
		{
			alert("Geben Sie bitte einen "+strAusgabe+"en Betrag ein!");
			betobj.select();
			return;	
		}
		
		if(obj.id=='but')
			var dblAlterBetrag = 0;
		else
			var dblAlterBetrag = arrAlteBetraege[buchid];
		
		var dblKazSummeNeu = nachkommaweg(<?php echo str_replace(",",".",$dblSumme); ?> - dblAlterBetrag + dblBetrneu,2);		//Nachkommastellen ab 3. Stelle wegschneiden
		
		if(dblBetrKaz>=0 && dblKazSummeNeu > dblBetrrls)
		{
			alert("Durch den aktuellen Betrag ("+eurobetrag(dblBetrneu+'')+") wird der RLS-Betrag ("+eurobetrag(dblBetrrls+'')+") gesamthaft �berschritten!");
			betobj.select();
			return;
		}
		
		if(dblBetrKaz<0 && dblKazSummeNeu < dblBetrrls)
		{
			alert("Durch den aktuellen Betrag ("+eurobetrag(dblBetrneu+'')+") wird der RLS-Betrag ("+eurobetrag(dblBetrrls+'')+") gesamthaft unterschritten!");
			betobj.select();
			return;
		}
		
		
		//var dblBetroffen = datenbankbetrag(document.getElementById("betroffen").value);
		
		var kaa_datum=datobj.value;
		var kaa_betrag=datenbankbetrag(betobj.value);
		
		var parameter = "&dblKaa_betrag="+kaa_betrag;
		
		if(buchid=='')	//Datensatzneuanlage
		{
			querystringerweitern("&intNeuSperre=1");		//Query-String erg�nzen, damit Button neuer Datensatz nicht mehr angezeigt wird
			parameter += "&strKaa_datum="+kaa_datum;
		}
		else
		{
			parameter += "&intUpdateId="+buchid;
		}
		
		location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+parameter;
	}
	
	function loeschen(strParameter)
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Einzelsatz Kontoauszug l�schen");	//Zeitmessung
		<?php } ?>
				
		var arrParameter = strParameter.split(';');
		
		intTabelle = arrParameter[0];
		intLoeschId = arrParameter[1];	
		
		var obj = window.event.srcElement;
		
		var buchid = obj.id.slice(3);
		
		var betobj = document.getElementById("bet"+buchid).value;
		var datobj = document.getElementById("dat"+buchid).value;
		var buaobj = document.getElementById("bua"+buchid).value;
		var rnrobj = document.getElementById("rnr"+buchid).value;
		
		var rueck = confirm(unescape("Soll der Datensatz\n\nDatum: "+datobj+"\nBetrag: "+betobj+"\nBuchungsart: "+buaobj+"\nRechnungs-Nr: "+rnrobj+"\n\nwirklich gel%F6scht werden?"));
		
		if(rueck)
		{
			querystringverkuerzen("&intNeuSperre=1");
			
			location.href="<?php echo $PHP_SELF; ?>?"+querystringlesen()+"&intLoeschId="+intLoeschId+"&intTabelle="+intTabelle;
		}
	}
	
	//Betrag aus Feld Betrag wird in Feld Betrag-RLS �bernommen, wenn Doppelklick auf Feld Betrag-RLS ausgef�hrt wird, sofern vorher 0 darin stand
	
	function rlsbetragsetzen()
	{
		var obj = window.event.srcElement;
		if(datenbankbetrag(obj.value)==0)
		{
			obj.value = document.getElementById("betrkaz").value;
			betragrls();
			window.event.cancelBubble = true;
		}
	}
	
	
	</script>
	</head>
	<body onload="init();" style="overflow:hidden;">
	<table cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr><td align="center" valign="top">
		<table cellspacing="0" cellpadding="0">
			<tr><td colspan="6" class="zwzeile"><hr></td></tr>
			<tr><td class="titel">Valuta:</td><td class="titel">Datum:</td><td class="titel">Auftraggeber/Bankverbindung:</td>
			<td class="titel">Verwendungszweck:</td>
			<td class="titel">Betrag:</td>
			<td class="titel">RLS-Betrag:</td>
			</tr>
			<tr><td colspan="6" class="zwzeile"><hr></td></tr>
			
				<?php
				//Zeile 1
				echo "<tr>";
				echo "<td class=\"text\"><input type=\"text\" class=\"val\" value=\"".
				$arrKaz["IKA_VALUTADATUM"][0]."\" readonly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"dat\" value=\"".
				$arrKaz["IKA_BUCHUNGSDATUM"][0]."\" readonly></td>";
				echo "<td class=\"text\" rowspan=\"2\"><textarea class=\"ag\" readonly>".
				($intAlt?"":$arrKaz["IKA_AUFTRAGGEBER1"][0])."\n".
				($intAlt?"":$arrKaz["IKA_AUFTRAGGEBER2"][0])."</textarea></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"ba\" value=\"".
				$arrKaz["BA_BUCHUNGSART"][0]."\" readonly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"betr\" id=\"betrkaz\" value=\"".
				eurobetrag($arrKaz["IKA_BETRAG"][0])."\" readOnly></td>";
				echo "<td class=\"text\"><input type=\"text\" class=\"betrrls\" id=\"betrrls\" value=\"".
				eurobetrag($arrKaz["IKA_RLSBETRAG"][0])."\" maxlength=\"15\"";
				if(!$intAlt)
					echo " onkeyup=\"aenderungzeile('betragrls');\" ondblclick=\"rlsbetragsetzen();\"";
				else
					echo " readOnly";
				echo "></td>";
				echo "</tr>";
				//Zeile 2
				echo "<tr>";
				echo "<td colspan=\"2\" class=\"text\" rowspan=\"2\" align=\"center\" style=\"vertical-align:bottom;\">";
				echo "<span class=\"titel\">Bank/KAZ:</span><br>";
				echo "<input type=\"text\" class=\"bank\" value=\"".$arrKaz["BAN_KUERZEL"][0]."\" readonly>";
				echo "<input type=\"text\" class=\"kaz\" value=\"";
				if(!$intAlt)
					echo $arrKaz["IKA_AUSZUGNR"][0]."/".$arrKaz["IKA_AUSZUGSDATUM"][0]."\"";
				else
					echo "RLS alt\" style=\"font-weight:bold;color:red;\"";
				echo " readonly></td>";	
				echo "<td class=\"text\" rowspan=\"2\"><textarea class=\"verw\" readonly>".
				($intAlt?"":$arrKaz["IKA_VERWENDUNGSZWECK1"][0])."\n".
				($intAlt?"":$arrKaz["IKA_VERWENDUNGSZWECK2"][0])."\n".
				($intAlt?"":$arrKaz["IKA_VERWENDUNGSZWECK3"][0])."</textarea></td>";
				$dblBetroffen = str_replace(".",",",(str_replace(",",".",$arrKaz["IKA_RLSBETRAG"][0])-str_replace(",",".",$dblSumme)));
				echo "<td rowspan=\"2\" class=\"text\" style=\"text-align:center;vertical-align:bottom;\"";
				echo "<span style=\"font-weight:bold;\">offen:</span><br><input type=\"text\" class=\"betroffen\" id=\"betroffen\" value=\"".
				($intAlt?"":eurobetrag($dblBetroffen))."\"";
				if($dblBetroffen!=0)
					echo " style=\"color:red;\"";
				echo " readOnly></td>";
				echo "<td class=\"zeile2\">&nbsp;</td></tr>";
				//Zeile 3
				echo "<tr>";
				echo "<td class=\"text\"><textarea class=\"agb\" readonly>".
				($intAlt?"":$arrKaz["IKA_AUFTRAGGEBER_KONTO"][0])."\n".
				($intAlt?"":$arrKaz["IKA_AUFTRAGGEBER_BLZ"][0])."</textarea></td>";
				echo "<td align=\"center\">&nbsp;";
				echo "<img src=\"/bilder/diskette.png\" class=\"speibut\" id=\"butrlsbetr\" alt=\"speichern (Alt+S)\" onclick=\"rlsbetragsend();\">";
				echo "</td></tr>";
				echo "<tr><td colspan=\"6\" class=\"zwzeile\"><hr></td></tr>";		
				?>
			</table>
			<?php
			if(!$intAlt&&(!$intKeinNeuButton||$intKazAuftZeilen))
			{
			?>
			Datens&auml;tze:
			<span class="titel">
			<?php echo $strPosition; ?>
			</span><br>&nbsp;
			<table cellspacing="0" cellpadding="0">
			<tr>
			<td class="titel">Datum:</td>
			<td class="titel">Betrag:</td>
			<td class="titel">Buchungsart:</td>
			<td class="titel">Rechnungs-Nr:</td>
			<td class="quadrat">&nbsp;</td>
			</tr>
			<?php 
				$arrAlteBetraege = array();
			
				for($i=0;$i<$intKazAuftZeilen;$i++)
				{
					$arrAlteBetraege[$arrKazAuft["KAA_ID"][$i]]=$arrKazAuft["KAA_BETRAG"][$i];
					
					//1 = RECHNUNGEN_KONTO, 2 = MAHNGEBUEHREN
				
					if($arrKazAuft["KAA_TABELLE"][$i]==1)
					{
						$strSql = "select
						rek_datum datum,
						rek_betrag betrag,
						(select nvl(bez_bezahlt_durch,'') from bezahltdurch where rek_betragart=bez_bezahlt_id(+)) buchungsart,
						rek_rnr rnr
						from rechnungen_konto
						where rek_buchid=".$arrKazAuft["KAA_FREMDID"][$i];
					}
					else
					{
						$strSql = "select
						mag_datum datum,
						mag_betrag betrag,
						(select nvl(mat_text,'') from mahngebuehren_text where mag_text=mat_id(+)) buchungsart,
						mag_rnr rnr
						from mahngebuehren
						where mag_id=".$arrKazAuft["KAA_FREMDID"][$i];
					}
					
					$arrKazDynamisch = abfrage("ora",$strSql);
					$arrKazDynamisch=$arrKazDynamisch["trefferarray"];
					
					$strFett="";
					
					if($arrKazAuft["KAA_TABELLE"][$i]==$intKaa_tabelle&&$arrKazAuft["KAA_FREMDID"][$i]==$intMagId)		//aktuelle Zeile wird fett dargestellt
					{
						$strFett=" style=\"font-weight:bold;\"";
					}
					
					echo "<tr>";
					echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"dataft\" value=\"".
					$arrKazAuft["KAA_DATUM"][$i]."\" id=\"dat".$arrKazAuft["KAA_ID"][$i]."\" readOnly".$strFett."></td>";
					echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"betr\" maxlength=\"15\" value=\"".
					eurobetrag($arrKazAuft["KAA_BETRAG"][$i])."\" id=\"bet".$arrKazAuft["KAA_ID"][$i]."\"".$strFett."";
					//if(!$intBestAend)
					if(true)
						echo " readOnly";
					else
						echo " onkeyup=\"aenderungzeile('aenderungzeilerest');\"";
					echo "></td>";
					echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"bua\" value=\"".
					$arrKazDynamisch["BUCHUNGSART"][0]."\"  id=\"bua".$arrKazAuft["KAA_ID"][$i]."\" readonly".$strFett."></td>";
					echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"rnr\" value=\"".
					$arrKazDynamisch["RNR"][0]."\" id=\"rnr".$arrKazAuft["KAA_ID"][$i]."\" readOnly".$strFett."></td>";
					
					if(!$intBestAend)
					{
						echo "<td>&nbsp;</td>";
					}
					else
					{
						echo "<td><input type=\"image\" src=\"/bilder/diskette.png\" accesskey=\"S\" class=\"speibut\" id=\"but".$arrKazAuft["KAA_ID"][$i].
						"\" alt=\"speichern (Alt+S)\" onclick=\"aendsend();\"><input type=\"image\" src=\"/bilder/muelleimer.png\" accesskey=\"L\" name=\"loeschen\" class=\"quadrat\" id=\"loe".$arrKazAuft["KAA_ID"][$i].
						"\" alt=\"l&ouml;schen (Alt+L)\" onclick=\"loeschen('".$arrKazAuft["KAA_TABELLE"][$i].";".$arrKazAuft["KAA_FREMDID"][$i]."');\"></td>";
					}
					echo "</tr>";
				}
				
				if(!$intNeuSperre&&!$intKeinNeuButton)		//Neuer Datensatz kann nur einmal angelegt werden
				{
					?>
					<tr>
					<td class="text"><input type="image" src="/bilder/plus.png" id="neu" class="quadrat" alt="neuer Datensatz" onclick="neu();"><input type="text" class="dataft" name="neuzeile" id="dat" readOnly></td>
					<td class="text"><input type="text" class="betr" name="neuzeile" id="bet" maxlength="15" readOnly></td>
					<td class="text"><input type="text" class="bua" name="neuzeile" id="bua" readOnly></td>
					<td class="text"><input type="text" class="rnr" name="neuzeile" id="rnr" readOnly></td>
					<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="speibut" name="neuzeile" id="but" alt="speichern (Alt+S)" onclick="aendsend();"></td>
					</tr><?php
				}	//Ende if
			?></table>
			<?php
			}	//Ende if
			?>
			
	</td></tr></table>
	<script language="JavaScript">
	
	//Array mit alten Betr�gen f�r die Datens�tze der Kontoauszugaufteilung
	var arrAlteBetraege = new Array();
	<?php
	foreach($arrAlteBetraege as $index => $wert)
	{
		echo "arrAlteBetraege[".$index."]=".str_replace(",",".",$wert).";";
	}
	?>
	
	</script>
<?php
}	//Ende if
else
{
	?>
	</head>
	<body>
	<table cellspacing="0" cellpadding="0" width="100%" height="100%">
	<tr><td align="center" valign="top">
		<br><span class="titel">Zur Kontoauszugs-Id <?php echo $intKazId;?><br>sind keine Datens�tze vorhanden!</span>
	</td></tr>
	</table>
	<?php
}
?>
</body>
</html>
<?php 
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>