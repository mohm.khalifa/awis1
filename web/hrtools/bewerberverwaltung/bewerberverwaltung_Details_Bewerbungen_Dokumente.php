<?php
global $AWISBenutzer;
global $CursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $DB;
global $Form;

echo '<a name="Dokumente" />';

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[] = array('DOC', '%');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'Uploaddatei');
$TextKonserven[] = array('Wort', 'DateiOeffnen');
$TextKonserven[] = array('Wort', 'KeineDatenVorhanden');
$TextKonserven[] = array('Wort', 'Typ');
$TextKonserven[] = array('Fehler', 'err_keineDaten');
$TextKonserven[] = array('Fehler', 'err_keineDatenbank');
$TextKonserven[] = array('Fehler', 'err_keineRechte');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
$Recht39001 = $AWISBenutzer->HatDasRecht(39001);

//$Form->Formular_Start();

$EditRecht = (($Recht39001 & 128) != 0);

$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe', $AWISBenutzer->BenutzerName());

$BindeVariablen = array();
$BindeVariablen['var_N0_doz_xxx_key'] = intval('0' . $AWIS_KEY2);

$SQL = 'SELECT Dokumente.*, xbn_name FROM Dokumente';
$SQL .= ' INNER JOIN dokumentZuordnungen ON doc_key = doz_doc_key';
$SQL .= ' INNER JOIN Benutzer ON doc_xbn_key = xbn_key';
$SQL .= ' WHERE doz_xxx_key=:var_N0_doz_xxx_key';
$SQL .= ' AND doz_xxx_kuerzel=\'BEW\'';
if (isset($_GET['DOCKEY'])) {
    $BindeVariablen['var_N0_doc_key'] = intval('0' . $_GET['DOCKEY']);
    $SQL .= ' AND doc_key=:var_N0_doc_key';
}
$SQL .= ' ORDER BY doc_datum desc';

$rsDOC = $DB->RecordSetOeffnen($SQL, $BindeVariablen);

if (($rsDOC->AnzahlDatensaetze() > 1 or isset($_GET['DOCListe']))) {
    $Form->ZeileStart();

    if (($Recht39001 & 64) > 0) {
        $Icons = '';
        $Icons[] = array(
            'new',
            './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=' . $AWIS_KEY1 . '&BWH_KEY=' . $AWIS_KEY2 . '&Dokumente=1&DOCKEY=0#Dokumente'
        );
        $Form->Erstelle_ListeIcons($Icons, 38, -1);
    }

    $Link = "./bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=" . $AWIS_KEY1 . "&BWH_KEY=" . $AWIS_KEY2 . "&Dokumente=1";
    $Link .= '&DOCSort=DOC_DATUM' . ((isset($_GET['DOCSort']) AND ($_GET['DOCSort'] == 'DOC_DATUM'))?'~':'');
    $Link .= '#Dokumente';
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_DATUM'], 150, '', $Link);

    $Link = "./bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=" . $AWIS_KEY1 . "&BWH_KEY=" . $AWIS_KEY2 . "&Dokumente=1";
    $Link .= '&DOCSort=DOC_BEZEICHNUNG' . ((isset($_GET['DOCSort']) AND ($_GET['DOCSort'] == 'DOC_BEZEICHNUNG'))?'~':'');
    $Link .= '#Dokumente';
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'], 550, '', $Link);

    $Link = "./bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=" . $AWIS_KEY1 . "&BWH_KEY=" . $AWIS_KEY2 . "&Dokumente=1";
    $Link .= '&DOCSort=XBN_NAME' . ((isset($_GET['DOCSort']) AND ($_GET['DOCSort'] == 'XBN_NAME'))?'~':'');
    $Link .= '#Dokumente';
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_XBN_KEY'], 200, '', $Link);

    $Form->Erstelle_Liste_Ueberschrift('', 30, '', $Link);

    $Form->ZeileEnde();

    $DS = 0;

    while (!$rsDOC->EOF()) {
        $Form->ZeileStart();
        $Icons = array();

        if ($EditRecht) {
            $Icons[] = array(
                'edit',
                './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=' . $AWIS_KEY1 . '&BWH_KEY=' . $AWIS_KEY2 . '&Dokumente=1&DOCKEY=' . $rsDOC->FeldInhalt('DOC_KEY') . '#Dokumente'
            );
        }

        if (($Recht39001 & 256) > 0) {
            $Icons[] = array(
                'delete',
                './bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&HRK_KEY=' . $AWIS_KEY1 . '&BWH_KEY=' . $AWIS_KEY2 . '&Dokumente=1&Del=' . $rsDOC->FeldInhalt('DOC_KEY') . '#Dokumente'
            );
        }

        $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));

        $Form->Erstelle_ListenFeld('#DOC_DATUM', $rsDOC->FeldInhalt('DOC_DATUM'), 20, 150, false, ($DS % 2), '', '',
            'D');
        $Form->Erstelle_ListenFeld('#DOC_BEZEICHNUNG', $rsDOC->FeldInhalt('DOC_BEZEICHNUNG'), 20, 550, false, ($DS % 2),
            '', '', 'T');
        $Form->Erstelle_ListenFeld('#XBN_NAME', $rsDOC->FeldInhalt('XBN_NAME'), 20, 180, false, ($DS % 2), '', '', 'T',
            '');
        $Link = '/dokumentanzeigen.php?doctab=BEW&dockey=' . $rsDOC->FeldInhalt('DOC_KEY') . '';

        if (($Recht39001 & 32) > 0) {
            $Form->Erstelle_ListenBild('href', 'DOC', $Link, '/bilder/dateioeffnen_klein.png',
                $AWISSprachKonserven['Wort']['DateiOeffnen'], 0, '', 17, 17, 18);
        }
        $Form->ZeileEnde();

        $rsDOC->DSWeiter();
        $DS++;
    }
} else        // Einer oder keiner
{
    echo '<input name=txtDOC_KEY type=hidden value=0' . $rsDOC->FeldInhalt('DOC_KEY') . '>';

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./bewerberverwaltung_Main.php?cmdAktion=Details&Seite=Bewerbungen&Dokumente=1&DOCListe=1&HRK_KEY=" . $AWIS_KEY1 . "&BWH_KEY=" . $AWIS_KEY2 . "#Dokumente accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => ($AWIS_KEY1 === 0?'':$rsDOC->FeldInhalt('DOC_USER'))
    );
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => ($AWIS_KEY1 === 0?'':$rsDOC->FeldInhalt('DOC_USERDAT'))
    );
    $Form->InfoZeile($Felder, '');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_DATUM'] . ':', 150);
    $Form->Erstelle_TextFeld('!DOC_DATUM', $rsDOC->FeldInhalt('DOC_DATUM'), 10, 200, ($Recht39001 & 64), '', '', '',
        'D', '', '', date('d.m.Y'));

    $CursorFeld = 'txtDOC_DATUM';

    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'] . ':', 150);
    $Form->Erstelle_TextFeld('DOC_BEZEICHNUNG', $rsDOC->FeldInhalt('DOC_BEZEICHNUNG'), 40, 400, ($Recht39001 & 64));
    $Form->ZeileEnde();

    if ((isset($_GET['DOCKEY']) and $_GET['DOCKEY'] <= 0) or !isset($_GET['DOCKEY'])) {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'] . ':', 150);
        $Form->Erstelle_DateiUpload('DOC', 330, 800);
        $Form->ZeileEnde();
    } else {

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'] . ':', 150);
        $Link = '/dokumentanzeigen.php?doctab=BEW&dockey=' . $rsDOC->FeldInhalt('DOC_KEY') . '';
        $Form->Erstelle_ListenBild('href', 'DOC', $Link, '/bilder/dateioeffnen_klein.png',
            $AWISSprachKonserven['Wort']['DateiOeffnen'], 0, '', 17, 17, 17);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Typ'] . ':', 40);
        $Form->Erstelle_TextLabel($rsDOC->FeldInhalt('DOC_ERWEITERUNG'), 100);
        $Form->ZeileEnde();
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEMERKUNG'] . ':', 150);
    $Form->Erstelle_Textarea('DOC_BEMERKUNG', $rsDOC->FeldInhalt('DOC_BEMERKUNG'), 400, 80, 3, ($Recht39001 & 64));
    $Form->ZeileEnde();
}
//$Form->Formular_Ende();
?>