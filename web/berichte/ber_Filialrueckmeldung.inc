<?php

require_once 'awisBerichte.inc';
require_once('awisDatenbank.inc');

global $AWISBenutzer;

class ber_Filialrueckmeldung
	extends awisBerichte
{
    private $pdf_vorlage = '';
	private $Zeilenmerker = '';
	private $Spaltenmerker = '';
	private $letzte_positon = '';
	/**
	 * Prozedur zum Initialisieren des Berichts
	 * @see awisBerichte::init()
	 */
	public function init(array $Parameter)
	{
	  
		$this->_DB = awisDatenbank::NeueVerbindung('SCHAD');
	    $this->_DB->oeffnen();
 		
	   

	    // Textkonserven laden
	    $TextKonserven = array();
	    $TextKonserven[]=array('BES','%');
        $this->_AWISSprachkonserven = $this->_Form->LadeTexte($TextKonserven);


		$this->_BerichtsName = intval($Parameter['BEARB_ID']);


        // Daten f�r den Bericht laden
        $SQL = "";
        $SQL="SELECT SCH.BEARBEITUNGSNR, AKTEGESCHLOSSENAM, SCH.EINGABEAM, SCH.FILNR, SCH.GESCHLECHT, SCH.KFZ_KENNZ, AUFTRAGSART_ATU_NEU, AUFTRAGSDATUM, MECHANIKER, LIEFARTNR, FORDERUNG, ";
        $SQL.=" SCH.KUNDENNAME, SCH.PLZ, SCH.ORT, SCH.STRASSE, SCH.TELEFON, SCH.TELEFON2, SCH.DIAGNOSE_WERKSTATTLEITER, VERK�UFER, SCH.DIAGNOSE_TKDL, SCH.BEMINTERN, RAFORDERUNG, AKTENZEICHENRA, AKTENZEITENGERICHT, ";
        $SQL.=" SCH.FAX, SCH.WANR, SCH.PAN, SCH.EMAIL, SCH.VORNAME, SCH.TERMIN_KUNDE_1, SCH.ATUNR, KAUFDATUM, ";
        $SQL.=" SCH.TERMIN_KUNDE_2, SCH.TERMIN_KUNDE_WER_1, SCH.TERMIN_KUNDE_WER_2, SCH.EINGABEDURCH_FIL, VERURS_FILIALE, DATUMREGULIERUNG, WEITERGELEITET_HERSTELLER, ";
        $SQL.=" SB.SBNAME as SACHBEARBEITER, EP.MITTEL as EINGANG, KK.WERT as KONTAKT, BONNR, WEITERGELEITETVERSICHERUNG,STANDBEIGERICHT, ";
        $SQL.=" IGBL.WERT as INFOGBL, KEN.KENNUNG_BEZEICHNUNG as KENBEZEICHNUNG, LIEFERANT, AUF.AUFTRAGSART_ATU as AUFTRAGSART, KFZTYP, ";
        $SQL.=" SGR.GRUND as SCHADENSGRUND, BES.SCHLAGWORT as BESCHAEDIGT, AU.WERT as AUSFALLURSACHE, SCH.GROSSKDNR as GROSSKDNR, ERSTZULASSUNG, ";
        $SQL.=" STA.BESCHREIBUNG as STAND, STA.STANDBEM as STANDKURZ, to_clob(SCH.FEHLERBESCHREIBUNG) as FEHLERBESCHREIBUNG, BONNRREGULIERUNG, REKLASCHEINNR, FABRIKAT, KBANR,KEN.KENNUNG,AA.WERT as ANTRAGART";
        $SQL.=" FROM SCHAEDEN_NEW SCH";
        $SQL.=" LEFT JOIN SACHBEARBEITER SB ON SB.ID = BEARBEITER";
        $SQL.=" LEFT JOIN EINGANGPER EP ON EP.ID = EINGANGPER";
        $SQL.=" LEFT JOIN KONTAKT_KUNDE KK ON KK.ID = KONTAKT_KUNDE";
        $SQL.=" LEFT JOIN INFO_AN_GBL IGBL ON IGBL.ID = INFO_AN_GBL";
        $SQL.=" LEFT JOIN KENNUNGEN KEN ON KEN.KENNUNG = SCH.KENNUNG";
        $SQL.=" LEFT JOIN AUFTRAGSARTEN AUF ON ART_ID = AUFTRAGSART_ATU_NEU";
        $SQL.=" LEFT JOIN SCHADENSGRUND SGR ON SGR.ID = SCHADENSGRUND";
        $SQL.=" LEFT JOIN BESCHAEDIGT BES ON BES.BID = SCH.BID";
        $SQL.=" LEFT JOIN AUSFALLURSACHE AU ON AU.ID = AUSFALLURSACHE";
        $SQL.=" LEFT JOIN STAND STA ON STANDID = STAND";
        $SQL.=" LEFT JOIN ANTRAGART AA ON AA.ID = ANTRAGART";

        // Alle Parameter auswerten
        $Bedingung = "";



        if (isset($Parameter['BEARB_ID']))
        {

            $Bedingung .= " AND BEARBEITUNGSNR = '" . substr($Parameter['BEARB_ID'],1) ."'";
        }

        if ($Bedingung != "")
        {
            $SQL .= " WHERE " . substr($Bedingung, 4);
        }
        // Daten laden


        $this->_rsSchad = $this->_DB->RecordSetOeffnen($SQL);



        $SQL = "SELECT B.BEARBNRNEU, to_clob(B.BEMERKUNGEN) as BEMERKUNG, B.DATUM, B.BETRAG, B.BNUSER, B.ID, B.EINTRAG, B.ZAHLUNGSART, B.EINGABEDURCH_FILPERSNR FROM BEARBEITUNGSSTAND_NEW B";
        $SQL .= " WHERE BEARBNRNEU=(SELECT BEARBNRNEU FROM SCHAEDEN_NEW WHERE BEARBEITUNGSNR='". substr($Parameter['BEARB_ID'],1) ."') ORDER BY ID"; //BY TO_DATE(DATUM, 'DD.MM.YYYY'), ID";

        //echo $SQL;
        $this->_rsSchadInfo = $this->_DB->RecordSetOeffnen($SQL);

        $SQLWV  ='SELECT * FROM(';
        $SQLWV .='SELECT';
        $SQLWV .='  wv_datum as wv_datum';
        $SQLWV .=' FROM';
        $SQLWV .='   WIEDERVORLAGEN_NEW';
        $SQLWV .=' WHERE';
        $SQLWV .='   BEARBEITUNGSNR     = '. $Parameter['BEARB_ID'];
        $SQLWV .=' AND TRUNC(wv_datum) >= TRUNC(sysdate)';
        $SQLWV .=' ORDER BY';
        $SQLWV .='   WV_DATUM ASC) ';
        $SQLWV .=' WHERE rownum           = 1';


        $this->_rsSchadNaechsteWV = $this->_DB->RecordSetOeffnen($SQLWV);


		
	}
	
	/**
	 * Eigentliches PDF erzeugen
	 * @see awisBerichte::ErzeugePDF()
	 */
	public function ErzeugePDF($Ausgabeart = 1)
	{
		// PDF Attribute setzen
		$this->SetTitle($this->_BerichtsName);
		$this->SetSubject('');
		//$this->SetCreator($this->_AWISSprachkonserven['SYSTEM']['PROGNAME']);
		$this->SetAuthor($this->_AWISBenutzer->BenutzerName());
		$this->SetMargins($this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand'], $this->_Parameter['LinkerRand']);
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['Schriftgroesse']);
		$this->AliasNbPages();
		
		$this->ErstelleSchadBlatt01();
		//usw.
		
		$this->FertigStellen($Ausgabeart);        
	}

	/**
	 * NeueSeite()
	 * Erstellt eine neue PDF Seite
	 *
	 * @return int
	 */
	private function NeueSeite()
	{
	    $this->AddPage();
	}
	
	/**
	 * Automatische Fu�zeile
	 * @see TCPDF::Footer()
	 */
	public function Footer()
	{
	    //BES_PDF_FOOTER
	    $this->SetFont($this->_Parameter['Schriftart'], '', 6);
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->_Zeile = 290;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	  				// Cursor setzen
	    $this->Cell(180, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_FOOTER']) . date('d.m.Y'),0 ,0 ,'C');
	    
	    //$Ausdruck->_pdf->Cell(180,3,'Dieses Formular ist nur f�r interne Zwecke bestimmt und darf nicht an Dritte weitergegeben werden. Stand: ' . date('d.m.Y'),0,0,'C',0);
	    
	}
	
	/**
	 * Automatische Kopfzeile
	 * @see TCPDF::Header()
	 */
	
	
	public function FertigStellen($Ausgabeart)
	{
	    //*********************************************************************
	    // Berichtsende -> Zur�ckliefern
	    //*********************************************************************
	    
	    switch($Ausgabeart)
	    {
	        case 1:			// PDF im extra Fenster -> Download
	            $this->Output($this->_BerichtsName.'.pdf','D');
	            break;
	        case 2:			// Standard-Stream
	            $this->Output($this->_BerichtsName.'.pdf','I');
	            break;
	        case 3:			// Als String zur�ckliefern
	            return($this->Output($this->_BerichtsName.'.pdf','S'));
	            break;
	        case 4:         // Als Datei speichern
	            $this->Output($this->_Dateiname,'F');
	            break;
	    }
	}
	
	protected function EditierBreite()
	{
		return self::SeitenBreite() - (self::RECHTERRAND + self::LINKERRAND);
	}	
	
	
	
	private function ErstelleSchadBlatt01()
	{		

	    
	    $this->NeueSeite();
	    $this->Kopfdaten();
	    $this->Diagnose();
    
	    
	}
	
	public function Header()
	{
	     if($this->_BerichtsVorlage!='')
	    {
	        $pdf_vorlage = $this->ImportPage(1);		// Erste Seite aus der Vorlage importieren
	        $this->useTemplate($pdf_vorlage);
	    }
	    
	    $this->_Zeile = $this->_Parameter['Seite1-Oben'];
		$this->_Spalte = $this->_Parameter['StartUeberschrift'];
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseHeader']);
				 
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	
	    $this->Cell(100, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_UEBERSCHRIFTSCHADDB']),0 ,0 ,'C');
	   
	    $this->_Zeile = $this->_Parameter['Seite1-Oben'] + 17;
	        
	    $this->SetXY($this->_Spalte, $this->_Zeile);

	}
	
	private function Kopfdaten()
	{
	    //Kopfdaten Block 1
	    $this->_Zeile = $this->_Parameter['Seite1-Oben'] + 30;
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdatenUeberschrift']);
	    	
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    
	    //$this->MultiCell(130, $this->_Parameter['Zeilenhoehe'], $this->_AWISSprachkonserven['BES']['BES_PDF_KUNDENDATEN']);
	    $this->Cell(130, $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_KUNDENDATEN']),1 ,0 ,'L',0);
	    
	    $this->_Zeile = $this->_Parameter['Seite1-Oben'] + 30;
	    $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    
	    $this->Cell(50, $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_BEARBEITUNGSDATEN']),1, 0 ,'L');


	    $this->_Zeile = $this->GetY() + $this->_Parameter['ZeilenhoeheUeberschrift'];
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_NAME']),0 ,0 ,'L');
	    $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBezDatKopfdaten'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('KUNDENNAME'),0 ,0 ,'L');
	    
	    $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_WANR']),0 ,0 ,'L');
	    $this->_Spalte = $this->_Parameter['AbstandSpalte2Kopfdaten']+$this->_Parameter['AbstandBezDatKopfdaten']+ 17;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('WANR'),0 ,0 ,'L');
	     
	    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);
	    $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_BEARBNR']),0 ,0 ,'L');
	    $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('BEARBEITUNGSNR'),0 ,0 ,'L');


	    //Zeile 2:
	    $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_VORNAME']),0 ,0 ,'L');
	    $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBezDatKopfdaten'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('VORNAME'),0 ,0 ,'L');
	     
	    $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_FZKENNZ']),0 ,0 ,'L');
	    $this->_Spalte = $this->_Parameter['AbstandSpalte2Kopfdaten']+$this->_Parameter['AbstandBezDatKopfdaten']+ 17;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('KFZ_KENNZ'),0 ,0 ,'L');
	    
	
	    $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_SACHBEARB']),0 ,0 ,'L');
	    $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('SACHBEARBEITER'),0 ,0 ,'L');

        //Zeile 3
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_STRASSE']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBezDatKopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('STRASSE'),0 ,0 ,'L');

        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_ATUCARD']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte2Kopfdaten']+$this->_Parameter['AbstandBezDatKopfdaten']+ 17;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('PAN'),0 ,0 ,'L');


        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_FILIALNR']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('FILNR'),0 ,0 ,'L');



        //Zeile 4:
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_PLZORT']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBezDatKopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('PLZ') . ' ' . $this->_rsSchad->FeldInhalt('ORT'),0 ,0 ,'L');



        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_EMAIL']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte2Kopfdaten']+$this->_Parameter['AbstandBezDatKopfdaten']+17;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('EMAIL'),0 ,0 ,'L');



        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
		$this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);
		$this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_VUFIL']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('VERURS_FILIALE'),0 ,0 ,'L');

        //Zeile5:
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_TEL1']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBezDatKopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('TELEFON'),0 ,0 ,'L');

        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_FAX']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte2Kopfdaten']+$this->_Parameter['AbstandBezDatKopfdaten']+ 17;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('FAX'),0 ,0 ,'L');


        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_ERFASSTAM']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D0',$this->_rsSchad->FeldInhalt('EINGABEAM')),0 ,0 ,'L');


        //Zeile6:
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_TEL2']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandBezDatKopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('TELEFON2'),0 ,0 ,'L');

        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_GROSSKDNR']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte2Kopfdaten']+$this->_Parameter['AbstandBezDatKopfdaten']+ 17;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('GROSSKDNR'),0 ,0 ,'L');


        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],'',0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],'',0 ,0 ,'L');

        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_EINGANGPER']),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rsSchad->FeldInhalt('EINGANG')),0 ,0 ,'L');


        //Zeile 7:
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_FAHRZEUG'] . ': '),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['LinkerRand'] +$this->_Parameter['AbstandBezDatKopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rsSchad->FeldInhalt('FABRIKAT')),0 ,0 ,'L');


        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_KBANR']) . ": ",0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte2Kopfdaten']+$this->_Parameter['AbstandBezDatKopfdaten']+ 17;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('KBANR'),0 ,0 ,'L');


        $this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_ERSTZULASSUNG']) . ": ",0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['AbstandSpalte3Kopfdaten'] +$this->_Parameter['AbstandBezDatKopfdaten'] + 20;
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D0',$this->_rsSchad->FeldInhalt('ERSTZULASSUNG')),0 ,0 ,'L');

        //Zeile 8:
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_TYP'] . ': '),0 ,0 ,'L');
        $this->_Spalte = $this->_Parameter['LinkerRand'] +$this->_Parameter['AbstandBezDatKopfdaten'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_rsSchad->FeldInhalt('KFZTYP')),0 ,0 ,'L');




        //Block Auftrag ATU
        //�berschriften

        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe']*2;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdatenUeberschrift']);

        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell(180, $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_AUFTRAGATU']),1 ,0 ,'L',0);


        //Zeile1
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'] *1.5;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['AuftragATUSpalte1'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_URSPRAUFTRAG']),1 ,0 ,'L',0);

        $this->_Spalte = $this->GetX();
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $SQL7 ='select ART_ID,AUFTRAGSART_ATU,null,AUFTRAGSART_ATU from AUFTRAGSARTEN where ART_ID' . $this->_DB->LikeOderIst($this->_rsSchad->FeldInhalt('AUFTRAGSART_ATU_NEU'));
        $rs7 = $this->_DB->RecordSetOeffnen($SQL7);
        $this->Cell($this->_Parameter['AuftragATUSpalte2'] , $this->_Parameter['ZeilenhoeheUeberschrift'],$rs7->FeldInhalt('AUFTRAGSART_ATU'),1 ,0 ,'L',0);


       // $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'] *1.5;
       // $this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->_Spalte = $this->GetX() +3;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['AuftragATUSpalte1'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_VU_WA']),1 ,0 ,'L',0);


        $this->_Spalte = $this->GetX();
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->Cell($this->_Parameter['AuftragATUSpalte2'] , $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_rsSchad->FeldInhalt('WANR'),1 ,0 ,'L',0);


        $this->_Spalte = $this->GetX() + $this->_Parameter['AuftragATUZwischenabstand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['AuftragATUSpalte3'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_VU_ATUNR']),1, 0 ,'L');

        $this->_Spalte = $this->GetX();
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['AuftragATUSpalte4'] , $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_rsSchad->FeldInhalt('ATUNR'),1 ,0 ,'L',0);


		//Zeile2
		$this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'] *1.5;
		$this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['AuftragATUSpalte5'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_KENNUNG']),1, 0 ,'L');

        $this->_Spalte = $this->GetX();
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['AuftragATUSpalte6'] , $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_rsSchad->FeldInhalt('KENNUNG'),1 ,0 ,'L',0);


        $this->_Spalte = $this->GetX() +3;
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['AuftragATUSpalte1'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_ANTRAGART']),1 ,0 ,'L',0);


        $this->_Spalte = $this->GetX();
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->Cell($this->_Parameter['AuftragATUSpalte2']+61 , $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_rsSchad->FeldInhalt('ANTRAGART'),1 ,0 ,'L',0);


        //Kopfdaten Block 2
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe']*3;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdatenUeberschrift']);

        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell(180, $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_BESCHWERDEKOMMENTAR']),1 ,0 ,'L',0);

		$this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetXY($this->_Spalte, $this->_Zeile);

		$this->_Zeile = $this->GetY() + 2;
		$this->_Spalte = $this->_Parameter['LinkerRand'];
		$this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
		$this->SetXY($this->_Spalte, $this->_Zeile);

		$Fehlerbeschreibung=preg_replace('/[\r\n]+/',' ',$this->_rsSchad->FeldInhalt('FEHLERBESCHREIBUNG'));

		$this->MultiCell(180, $this->_Parameter['Zeilenhoehe'], $Fehlerbeschreibung,0 );


        //Kopfdaten Block 3:
        //�berschrift:
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdatenUeberschrift']);

        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell(180, $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_BESCHWERDEBEARBEITUNG']),1 ,0 ,'L',0);


        //Zeile 1:
        $this->_Zeile = $this->GetY() + $this->_Parameter['ZeilenhoeheUeberschrift'];
        $this->_Spalte = $this->_Parameter['LinkerRand'];


        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(27, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_WORANGEARBEITET']),0 ,0 ,'L');
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('SCHADENSGRUND') ,0 ,0 ,'L');



        //Zeile 6:
		$this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte2Kopfdaten'];
        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(22, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_URSPRAUFTRAG']),0 ,0 ,'L');
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('AUFTRAGSART') ,0 ,0 ,'L');


		$this->_Spalte = $this->_Parameter['LinkerRand']+$this->_Parameter['AbstandSpalte3Kopfdaten'];
		$this->SetXY($this->_Spalte, $this->_Zeile);
        $this->Cell(25, $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_WASBESCHAEDIGT']),0 ,0 ,'L');
        $this->Cell(35, $this->_Parameter['Zeilenhoehe'],$this->_rsSchad->FeldInhalt('BESCHAEDIGT') ,0 ,0 ,'L');


        //Block Bearbeitungsstand
        //�berschriften
        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'] *2;;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdatenUeberschrift']);

        $this->Cell($this->_Parameter['BeaForBlock2Start'] -$this->_Spalte = $this->_Parameter['LinkerRand'] , $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_BEARBEITUNGSSTAND']),1 ,0 ,'L',0);


        $this->_Spalte = $this->_Parameter['BeaForBlock2Start'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->Cell($this->_Parameter['BeaForBlock2Ende'] - $this->_Parameter['BeaForBlock2Start'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_rsSchad->FeldInhalt('STANDKURZ'),1 ,0 ,'L',0);


        $this->_Spalte = $this->_Parameter['BeaForBlock2Start'];
        $this->SetXY($this->_Spalte, $this->_Zeile);

        $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);



		$this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'] *1.5;;
        $this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['BeaForBlock3Start'], $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_EINGABEAM']),1 ,0 ,'L',0);


        $this->Cell($this->_Parameter['BeaForBlock3Ende'] , $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D0',$this->_rsSchad->FeldInhalt('EINGABEAM')),1 ,0 ,'L',0);


		$this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'] *1;;
		$this->_Spalte = $this->_Parameter['LinkerRand'];
        $this->SetXY($this->_Spalte, $this->_Zeile);
        $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);

        $this->Cell($this->_Parameter['BeaForBlock4Start'], $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_GESCHLOSSENAM']),1, 0 ,'L');
		$this->Cell($this->_Parameter['BeaForBlock4Ende'] ,$this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D0',$this->_rsSchad->FeldInhalt('AKTEGESCHLOSSENAM')),1 ,0 ,'L',0);
	}
	
	private function Diagnose()
	{
	    //�berschrift
	    $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe']*2;
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdatenUeberschrift']); 
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    
	    $this->Cell(180, $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_DIAGNOSEERGEBNIS']),1 ,0 ,'L',0);
	    
	    $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'] - 1;
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	   
	    //Kopfzeile Tabelle
	    $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['SchriftgroesseKopfdaten']);	    
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell($this->_Parameter['BreiteDatum'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_DATUM']),1 ,0 ,'L',0); 
	    
	    //Spalte 1
	    $this->_Spalte = $this->GetX();
	    $this->SetXY($this->_Spalte, $this->_Zeile);	    
	    $this->Cell($this->_Parameter['BreiteEintrag'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_EINTRAG']),1 ,0 ,'L',0);

	  	//Spalte 3
	    $this->_Spalte = $this->GetX();
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell($this->_Parameter['BreiteBetrag'], $this->_Parameter['ZeilenhoeheUeberschrift'],'Betrag',1,0,'L',0);
	    
	    
	    //Spalte 4
	    $this->_Spalte = $this->GetX();
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    $this->Cell($this->_Parameter['BreiteDiagnose'], $this->_Parameter['ZeilenhoeheUeberschrift'],$this->_Form->Format('T',$this->_AWISSprachkonserven['BES']['BES_PDF_DIAGNOSE']),1 ,0 ,'L',0);
	    
	    //$this->_Zeile = $this->GetY() + $this->_Parameter['ZeilenhoeheUeberschrift'];
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	     
	    $rsSchadInfoZeilen = $this->_rsSchadInfo->AnzahlDatensaetze();

	    $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['SchriftgroesseKopfdaten']);
 
	    $this->_Zeile = $this->GetY() + $this->_Parameter['ZeilenhoeheUeberschrift'];
	    $this->_Spalte = $this->_Parameter['LinkerRand'];
	    $this->SetXY($this->_Spalte, $this->_Zeile);
	    
	    while(! $this->_rsSchadInfo->EOF()) //Alle "kommentare" des Kopfdatensatzes
	    {
	        //Daten Reindrucken
	        
	        //Spalte 1
	        $this->MultiCell($this->_Parameter['BreiteDatum'], $this->_Parameter['Zeilenhoehe'],$this->_Form->Format('D0',$this->_rsSchadInfo->FeldInhalt('DATUM')),0 ,'L' ,0,0);
	     	        
	        //Spalte 2
	        $this->_Spalte = $this->GetX()+20;
	        $this->SetXY($this->_Spalte, $this->_Zeile);
	        $this->MultiCell($this->_Parameter['BreiteEintrag'], $this->_Parameter['Zeilenhoehe'],$this->_rsSchadInfo->FeldInhalt('EINTRAG'),0 ,'L' ,0,0);
	         
	     
	        
	        //Spalte 3
	        $this->_Spalte = $this->GetX()+55;
	        $this->SetXY($this->_Spalte, $this->_Zeile);
	        $this->MultiCell($this->_Parameter['BreiteBetrag'], $this->_Parameter['Zeilenhoehe'],$this->_rsSchadInfo->FeldInhalt('BETRAG')== ''?'':$this->_rsSchadInfo->FeldInhalt('BETRAG').'�',0 ,'L' ,0,0);
	        
	        //Spalte 5
	        $this->_Spalte = $this->GetX() +70;
	        $this->SetXY($this->_Spalte, $this->_Zeile);
	        
	       
	        $this->MultiCell($this->_Parameter['BreiteDiagnose'], $this->_Parameter['Zeilenhoehe'],str_replace(chr(164),chr(128),$this->_rsSchadInfo->FeldInhalt('BEMERKUNG')),0 ,'L' ,0,0);
	        //$this->MultiCell($this->_Parameter['BreiteDiagnose'] + 15, $this->_Parameter['Zeilenhoehe'],iconv()$this->_rsSchadInfo->FeldInhalt('BEMERKUNG'),0 ,'L' ,0,0);
	       
            
	        //Zeieln Anfang wieder setzen
	        $this->_Zeile = $this->GetY() + $this->_Parameter['Zeilenhoehe'];
	        $this->_Spalte = $this->_Parameter['LinkerRand'];
	        $this->SetXY($this->_Spalte, $this->_Zeile);
	        $this->_rsSchadInfo->DSWeiter();  
	    }
	}
}
?>