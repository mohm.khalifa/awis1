<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
$con = awisLogon();
$RechteStufe = awisBenutzerRecht($con, 604);
?>

<title></title>
</head>

<body>

<h2>AWIS - Hilfs- und Betriebsstoffgruppen</h2>

In dieser Stammdatenseite werden alle Hilfs- und Betriebsstoffgruppen angezeigt. Diese Gruppen 
werden bei der Anlage der Hilfs- und Betriebsstoffe verwendet.<br>
<br>
Es werden die fortlaufende Nummer (die f�r interne Zwecke verwendet wird), die Bezeichnung (die
eindeutig sein muss) und die Bemerkung angezeigt. Um die Daten zu sortieren, k�nnen Sie auf die 
�berschriftszeilen klicken.<br>

<?php
if(($RechteStufe&2)==2)		// �ndern erlaubt?
{
echo '<br>�ber den Link auf die eindeutige ID k�nnen Sie in die Bearbeitungsmaske f�r die Gruppe
gelangen. Dabei erhalten Sie die M�glichkeit, die Bezeichnung und die Bemerkung zu �ndern. Die
eindeutige Nummer kann nicht ge�ndert werden. �ber das Diskettensymbol (<img src=/bilder/diskette.png alt=Speichern>)
k�nnen Sie einen Datensatz speichern.<br>';
}

if(($RechteStufe&4)==4)		// Hinzuf�gen?
{
echo '<br>Mit dem Hinzuf�gensymbol (<img src=/bilder/plus.png alt=Hinzuf�gen>) k�nnen Sie einen neuen Datnsatz hinzuf�gen. 
Sie m�ssen die Bezeichnung und optional eine Bemerkung ausf�llen.<br>';
}

if(($RechteStufe&8)==8)		// L�schen?
{
echo '<br>Mit Hilfe des L�schsymbols (<img src=/bilder/Muelleimer_gross.png alt=L�schen>) k�nnen Sie einen Datensatz
l�schen. Es wird noch einmal nachgefragt, bevor der Datensatz endg�ltig gel�scht wird.<br>';
}

awislogoff($con);
?>

</body>

</html>
