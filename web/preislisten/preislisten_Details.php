<?php
global $RechteStufe;
global $con;
global $awisRSZeilen;
global $awisDBError;
global $AWISBenutzer;

if(($RechteStufe&2)!=2)
{
	awisLogoff($con);
	die();
}

//**********************************************************
// Preisliste suchen
//**********************************************************
if(awis_NameInArray($_POST,'cmdDetails_',0)!='' OR isset($_POST['cmdSpeichern_x']))
{
	$Param = array();
	if( isset($_POST['cmdSpeichern_x']))
	{
		$Param[0] = $_POST['txtPREISLISTE'];
	}
	else
	{
		$Param[0] = intval(substr(awis_NameInArray($_POST,'cmdDetails_',0),11));
	}


	awis_BenutzerParameterSpeichern($con,'Preislisten_Suche',$AWISBenutzer->BenutzerName(),implode(';',$Param));
}
else
{
	$Param = explode(';',awis_BenutzerParameter($con,'Preislisten_Suche',$AWISBenutzer->BenutzerName()));
}

if($Param[0]=='')
{
	echo '<span class=HinweisText>Sie haben keine Preisliste gew�hlt.</span>';
	awisLogoff($con);
	die();
}



$comcon = awisLogonComCluster('DE',false);
awisExecute($comcon, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'");

//***********************************************************************
// Neuen Status speichern
//***********************************************************************

if(isset($_POST['cmdSpeichern_x']))
{
	$Felder = '';
	If($_POST['txtNeuerStatus']!==-1)
	{
		$Felder .= ', PTS_PST_ID=' . $_POST['txtNeuerStatus'];
	}
	if($_POST['txtBemerkung']!=='')
	{
		$Felder .= ', PTS_BEMERKUNG=\'' . $_POST['txtBemerkung'] . '\'';
	}

	If($Felder!=='')
	{
		if($Felder != '' AND  $_POST['txtBemerkung']!='' )
		{
			$SQL = 'UPDATE EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS';
			$SQL .= ' SET ' . substr($Felder,2);
			$SQL .= ' , PTS_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ' , PTS_USERDAT=SYSDATE';
			$SQL .= ' WHERE PTS_TRANSAKTIONSID=' . $_POST['txtTID'];

			if(!awisExecute($comcon,$SQL))
			{
				awisErrorMailLink('06091200-preislisten_Details','Fehler',$awisDBError['message']);
			}

			$SQL = 'UPDATE EXPGW.EXPGW_EXPORT';
			switch ($_POST['txtNeuerStatus']) {
				case 95:		// Sperres
					$SQL .= ' SET ARBEITSSTATUS=\'E\'';
					break;
				default:
					$SQL .= ' SET ARBEITSSTATUS=\'C\'';
					break;
			}

			$SQL .= ' WHERE TRANSAKTIONSID=' . $_POST['txtTID'];

			if(!awisExecute($comcon,$SQL))
			{
				awisErrorMailLink('06091249preislisten_Details','Fehler',$awisDBError['message']);
			}
		}
		else
		{
			echo '<span class=HinweisText>Sie m�ssen eine Bemerkung eingeben, wenn Sie den Status einer Transaktion �ndern m�chten!</span>';

		}

	}
}





//***********************************************************************
//Daten anzeigem
//***********************************************************************
$BindeVariablen=array();
$BindeVariablen['var_N0_id_fplb']=$Param[0];

$SQL = 'SELECT BEZEICHNUNG, PST_ID, PST_STATUS, PTS_USERDAT, PTS_TRANSAKTIONSID, PTS_ID_FPLB, PTS_USER, PTS_BEMERKUNG, ARBEITSSTATUS, HERSTELLERPREISLISTE';
$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS ';
$SQL .= ' LEFT OUTER JOIN EXPGW.EXPGW_EXPORT ON PTS_TRANSAKTIONSID=TRANSAKTIONSID';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTS_PST_ID = PST_ID';
$SQL .= ' INNER JOIN EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB ON ID_FPLB = PTS_ID_FPLB';
$SQL .= ' WHERE PTS_ID_FPLB=:var_N0_id_fplb';
$SQL .= ' ORDER BY  PTS_TRANSAKTIONSID DESC';

$rsPLI = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);

echo '<form name=frmBestaetigung action=./preislisten_Main.php?cmdAktion=Details method=POST>';

echo '<input type=hidden name=txtTID value=' . $rsPLI['PTS_TRANSAKTIONSID'][0] . '>';
echo '<input type=hidden name=txtPREISLISTE value=' . $rsPLI['PTS_ID_FPLB'][0] . '>';

echo '<table border=1 width=100%>';
echo '<tr>';
echo '<td class=FeldBez width=200>Preisliste:</td><td>' . $rsPLI['BEZEICHNUNG'][0] . '(' . $rsPLI['PTS_ID_FPLB'][0] . ')</td>';
echo '</tr>';

echo '<tr>';
echo '<td class=FeldBez>Letzte Transaktion:</td><td>' . $rsPLI['PTS_TRANSAKTIONSID'][0] . '</td>';
echo '</tr>';

echo '<tr>';
echo '<td class=FeldBez>Status:</td><td>' . $rsPLI['PST_STATUS'][0] . '</td>';
echo '</tr>';

echo '<tr>';
echo '<td class=FeldBez>Letzte Status�nderung:</td><td>' . $rsPLI['PTS_USERDAT'][0] . ' (' . $rsPLI['PTS_USER'][0] . ')</td>';
echo '</tr>';

echo '<tr>';
echo '<td class=FeldBez>Bemerkung:</td><td>' . $rsPLI['PTS_BEMERKUNG'][0] . '</td>';
echo '</tr>';

if(($RechteStufe&32)==32 AND ($rsPLI['ARBEITSSTATUS'][0]!='M' AND $rsPLI['ARBEITSSTATUS'][0]!='T'))
{
	echo '<tr>';
	echo '<td class=FeldBez>Neuer Status:</td><td>';
	echo '<select name=txtNeuerStatus>';
	echo '<option value=-1>::Bitte w�hlen::</option>';
	if($rsPLI['PST_ID'][0]!=15)
	{
		echo '<option value=15>Pr�fung abgeschlossen</option>';
	}
	if($rsPLI['PST_ID'][0]!=95)
	{
		echo '<option value=95>Gesperrt - Liste verwerfen</option>';
	}
	if($rsPLI['PST_ID'][0]!=40)
	{
		echo '<option value=40>Freigabe - Liste f�r die DF� freigeben</option>';
	}
	echo '</select>';
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td  title=Pflichtfeld class=FeldBez valign=top>Bemerkung:</td><td>';
	echo '<textarea name=txtBemerkung cols=80 rows=3></textarea>';
	echo '</td>';
	echo '</tr>';


	echo '<tr><td colspan=2>';
	echo " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern>";
	echo " <input type=image accesskey=X alt='Export (Alt+X)' src=/bilder/tabelle.png name=cmdExport>";
	echo '</td></tr>';

}

echo '</table>';


echo '<br><b>Folgende Pr�fungen wurden durchgef�hrt:</b>';
// Alle Pr�fungen anzeigen
echo '<table border=1 width=100%>';

$BindeVariablen=array();
$BindeVariablen['var_N0_id_fplb']=$rsPLI['PTS_ID_FPLB'][0];
$BindeVariablen['var_tid']=$rsPLI['PTS_TRANSAKTIONSID'][0];


$SQL = 'SELECT PTA.PTA_BEMERKUNG, PTA.PTA_BEZEICHNUNG, PPH.PPH_STATUS, PPH.PPH_ENDE, PPH.PPH_USER, PPH.PPH_KEY, PLP.PLP_ReihenFolge,';
$SQL .= ' (SELECT COUNT(*) FROM EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT PAU WHERE PAU.PAU_PPH_KEY=PPH.PPH_KEY) AS AUFF';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN PLP';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTEN PTA ON PLP.PLP_PTA_ID=PTA.PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST PPH ON PPH.PPH_PLP_KEY=PLP.PLP_Key';
$SQL .= ' WHERE PLP.PLP_ID_FPLB=:var_N0_id_fplb';
$SQL .= ' AND PPH.PPH_TRANSAKTIONSID=:var_tid';
$SQL .= ' ORDER BY PLP.PLP_ReihenFolge';

$rsPTA = awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
$rsPTAZeilen = $awisRSZeilen;

echo '<tr><td class=FeldBez width=200>Pr�fung</td>';
echo '<td class=FeldBez>Ergebnis</td>';
echo '<td class=FeldBez>Auff�lligkeiten</td>';
echo '<td class=FeldBez>Pr�fungsende</td>';
echo '<td class=FeldBez>Gepr�ft durch</td>';
echo '</tr>';

for($PTAZeile=0;$PTAZeile<$rsPTAZeilen;$PTAZeile++)
{
	echo '<tr>';
	echo '<td title=\'' . $rsPTA['PTA_BEMERKUNG'][$PTAZeile] . '\'><b>' . $rsPTA['PTA_BEZEICHNUNG'][$PTAZeile] . '</b></td>';
	echo '<td>' . PPHStatusText($rsPTA['PPH_STATUS'][$PTAZeile]) . '</td>';

	if($rsPTA['AUFF'][$PTAZeile]>0)
	{
		echo "<td title='Klicken Sie auf die Zahl, um die Details anzuzeigen'><a onclick=window.open('./preislisten_Auffaelligkeiten.php?PPHKEY=" . $rsPTA['PPH_KEY'][$PTAZeile] . "','Preislistenpr�fungdetails','toolbar=no,menubar=no,dependent=yes,status=no,resizable=yes,fullscreen=yes,scrollbars=yes');>" . $rsPTA['AUFF'][$PTAZeile] . '</a></td>';
	}
	else
	{
		echo '<td>' . $rsPTA['AUFF'][$PTAZeile] . '</td>';
	}

	echo '<td>' . $rsPTA['PPH_ENDE'][$PTAZeile] . '</td>';
	echo '<td>' . $rsPTA['PPH_USER'][$PTAZeile] . '</td>';

	echo '</tr>';

}

//***********************************************************************
// Daten exportieren
//***********************************************************************

if(isset($_POST['cmdExport_x']))
{
	$FeldListe = array('ARTNR',	'ARTBEZL',	'ARTBEZK',	'WLAGER1',	'WLAGER2',	'LFNR',	'LARTNR',
		'WG',	'SG',	'BRUTTO',	'MBDAT',	'FILORT',	'KENN1',	'NACHLASSKZ',	'NACHLASSKZCZ',
		'NACHLASSKZOES',	'NACHLASSKZNL',	'NACHLASSPR',	'NACHLASSPRCZ',	'NACHLASSPROES',	'NACHLASSPRNL',
		'VK',	'VKCZ',	'VKOES',	'VKNL',	'WKZ_VK',	'WKZ_VKCZ',	'WKZ_VKOES',	'WKZ_VKNL',
		'AKTIV1',	'AKTIV2',	'AKTIV3',	'AKTIV4',	'AKTIV5',	'AKTIV6',	'AKTIV7',	'AKTIV8',	'AKTIV9',
		'AKTIV10',	'AKTIV11',	'AKTIV12',	'AKTIV13',	'AKTIV14',	'AKTIV15',	'AKTIV16',	'AKTIV17',	'AKTIV18',
		'AKTIV19',	'AKTIV20',
		'MWST_KZ_D',	'MWST_KZ_CZ',	'MWST_KZ_OES',	'MWST_KZ_NL',	'IMPDAT',
		'TRANSAKTIONSID',
		'NACHLASSKZITA',	'NACHLASSPRITA',	'VKITA',	'WKZ_VKITA',	'MWST_KZ_ITA',	'PAU_AUFFAELLIGKEIT',
		'PAU_SCHRITT');

	$SQL = 'select exp_artikel.*, PAU_AUFFAELLIGKEIT, PAU_SCHRITT, PAU_DATEN, PAU_FELD';
	if($rsPLI['HERSTELLERPREISLISTE'][0]==1)
	{
		$SQL .= ' from EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ';
		$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST ON (PPH_TRANSAKTIONSID = PAZ_TRANSAKTIONSID)';
		$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT ON (PPH_KEY = PAU_PPH_KEY AND PAZ_ARTNR = PAU_DATEN)';
		$SQL .= ' INNER JOIN experian_atu.exp_artikel ON (ARTNR = PAZ_ARTNR AND IMPDAT = PAZ_IMPDAT)';
		$SQL .= ' WHERE PAZ_TRANSAKTIONSID = '.$rsPLI['PTS_TRANSAKTIONSID'][0].'';
		$SQL .= ' AND PAZ_VERSION = 0';

/*

SELECT DISTINCT NVL(ARTNR,PAZ_ARTNR) AS ATUNR
, CASE WHEN PAZ_VERSION = 1 THEN 'AktDaten' ELSE 'NeueDaten' END AS Version
,PAU_AUFFAELLIGKEIT,PAU_SCHRITT
,PAZ_HERSTELLER,PAZ_HERSTELLER_LANG,PAZ_RG,PAZ_ZUSINF_RG,PAZ_TYP1,PAZ_TYP2,
PAZ_TYP3, PAZ_WINSOM, PAZ_BREITE, PAZ_QUERSCHNITT, PAZ_BAUART,PAZ_INNENDM,
PAZ_LLKW,PAZ_LOAD1, PAZ_LOAD2, PAZ_SP, PAZ_RF, PAZ_ZUSBEM,ARTBEZL,ARTBEZK,
WLAGER1,WLAGER2,LFNR,LARTNR,WG,SG,BRUTTO,MBDAT,FILORT,KENN1,NACHLASSKZ,
VK,MWST_KZ_D
FROM (
SELECT * FROM
experian_atu.exp_artikel ART
FULL OUTER JOIN experian_atu.PLP_artikel_zusatz ZUS
      ON PAZ_IMPDAT = IMPDAT AND PAZ_ARTNR = ARTNR AND PAZ_VERSION = 0
WHERE PAZ_TRANSAKTIONSID = '1000000006380'
AND (ART.ARTNR IN
(
SELECT distinct PAU_DATEN
FROM "EXPERIAN_ATU"."PLP_PREISLISTENAUFFAELLIGKEIT"
INNER JOIN "EXPERIAN_ATU"."PLP_PREISLISTENPRUEFUNGENHIST"
      ON PAU_PPH_KEY = PPH_KEY
WHERE PPH_TRANSAKTIONSID = '1000000006380'
)
OR PAZ_ARTNR IN
(
SELECT distinct PAU_DATEN
FROM "EXPERIAN_ATU"."PLP_PREISLISTENAUFFAELLIGKEIT"
INNER JOIN "EXPERIAN_ATU"."PLP_PREISLISTENPRUEFUNGENHIST"
      ON PAU_PPH_KEY = PPH_KEY
WHERE PPH_TRANSAKTIONSID = '1000000006380'
))
UNION  ALL
SELECT * FROM
experian_atu.exp_artikel ART
FULL OUTER JOIN experian_atu.PLP_artikel_zusatz ZUS
      ON PAZ_IMPDAT = IMPDAT AND PAZ_ARTNR = ARTNR AND PAZ_VERSION = 1
WHERE PAZ_TRANSAKTIONSID = '1000000005920'
AND (ART.ARTNR IN
(
SELECT distinct PAU_DATEN
FROM "EXPERIAN_ATU"."PLP_PREISLISTENAUFFAELLIGKEIT"
INNER JOIN "EXPERIAN_ATU"."PLP_PREISLISTENPRUEFUNGENHIST"
      ON PAU_PPH_KEY = PPH_KEY
WHERE PPH_TRANSAKTIONSID = '1000000006380'
)
OR PAZ_ARTNR IN
(
SELECT distinct PAU_DATEN
FROM "EXPERIAN_ATU"."PLP_PREISLISTENAUFFAELLIGKEIT"
INNER JOIN "EXPERIAN_ATU"."PLP_PREISLISTENPRUEFUNGENHIST"
      ON PAU_PPH_KEY = PPH_KEY
WHERE PPH_TRANSAKTIONSID = '1000000006380'
))
) FEHLER
INNER JOIN EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT
      ON PAU_DATEN = NVL(ARTNR,PAZ_ARTNR)
INNER JOIN "EXPERIAN_ATU"."PLP_PREISLISTENPRUEFUNGENHIST"
      ON PAU_PPH_KEY = PPH_KEY AND PPH_TRANSAKTIONSID = '1000000006380'
ORDER BY NVL(ARTNR,PAZ_ARTNR), PAU_AUFFAELLIGKEIT

*/
	}
	else
	{
		$SQL = ' FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN';
		$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST ON (PPH_TRANSAKTIONSID = PLT_TRANSAKTIONSID)';
		$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT ON (PPH_KEY = PAU_PPH_KEY AND PLT_ARTNR = PAU_DATEN)';
		$SQL .= ' INNER JOIN experian_atu.exp_artikel ON (ARTNR = PLT_ARTNR AND IMPDAT = PLT_IMPDAT)';
		$SQL .= ' WHERE PLT_TRANSAKTIONSID = '.$rsPLI['PTS_TRANSAKTIONSID'][0].'';
		$SQL .= ' AND PLT_VERSION = 0';
	}

	$rsDaten= awisOpenRecordset($comcon, $SQL);
	$rsDatenZeilen= $awisRSZeilen;

	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );

	// Verworfene in eine eigene Datei
	$fd1 = fopen('/daten/web/export/kbpreisliste_'.date('Y_m_d').'.csv','w+' );

	foreach($FeldListe AS $Feld)
	{
		fputs($fd,$Feld.";");
		fputs($fd1,$Feld.";");
	}
	fputs($fd,"\n");
	fputs($fd1,"\n");

			// Jeden Datensatz pruefen, bevor er weitergeht
	for($DSNr=0;$DSNr<$rsDatenZeilen;$DSNr++)
	{
		// 28.02.2008: Anweisung von Hr, Zeitler
		$Aktion = 'Fehler';
		if($rsDaten['KENN1'][$DSNr]=='E')
		{
			$Aktion = 'Filter';
		}

		// Bestimmte Kennungen raus
		if($rsDaten['PAU_SCHRITT'][$DSNr]=='3'
				AND (array_search(substr($rsDaten['ARTNR'][$DSNr],0,2),array('FU','GL','GN','VR','VI','NA','CN','BA'))!==false))
		{
			$Aktion = 'Filter';
		}
		// Manuell angelegte durch die CARD Abteilung
		if($rsDaten['PAU_SCHRITT'][$DSNr]=='1'
				AND substr($rsDaten['ARTNR'][$DSNr],0,4)=='CARD')
		{
			$Aktion = 'Filter';
		}
		if($rsDaten['PAU_SCHRITT'][$DSNr]=='1'
				AND (strpos($rsDaten['PAU_AUFFAELLIGKEIT'][$DSNr],'TYP2')!==false
				OR strpos($rsDaten['PAU_AUFFAELLIGKEIT'][$DSNr],'TYP3')!==false))
		{
			$Aktion = 'Filter';
		}

		// Daten pr�fen, ob sie vorher schon da waren
		if($rsDaten['PAU_SCHRITT'][$DSNr]=='4' AND $rsDaten['PAU_FELD'][$DSNr]!='' )
		{
			$SQL = 'SELECT PAZ_'.$rsDaten['PAU_FELD'][$DSNr];
			$SQL .= ' FROM EXPERIAN_ATU.PLP_ARTIKEL_ZUSATZ';
			$SQL .= ' WHERE PAZ_ARTNR = \''.$rsDaten['ARTNR'][$DSNr].'\'';
			$SQL .= ' ORDER BY PAZ_TRANSAKTIONSID';
//awis_Debug(1,$SQL);
			$rsPAZ = awisOpenRecordset($comcon,$SQL);
			$rsPAZZeilen = $awisRSZeilen;
			if($rsPAZZeilen==0)
			{
				$Aktion='Filter';
			}
			elseif($rsPAZ[$rsDaten['PAU_FELD'][0]]==$rsPAZ[$rsDaten['PAU_FELD'][1]])
			{
				$Aktion='Filter';
			}

		}

		if($Aktion=='Filter')
		{
			// Datensatz wegschreiben, aber nicht exportieren
			foreach($FeldListe AS $Feld)
			{
				fputs($fd1, $rsDaten[$Feld][$DSNr] . ";");
			}

			fputs($fd1,"\n");
		}
		else
		{
			// Datensatz muss manuell geprueft werden.
			foreach($FeldListe AS $Feld)
			{
				fputs($fd, $rsDaten[$Feld][$DSNr] . ";");
			}

			fputs($fd,"\n");
		}
	}

	fclose($fd);
	fclose($fd1);

	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Pr�f-Datei �ffnen</a>';
	echo '<br><a href=/export/kbpreisliste_'.date('Y_m_d').'.csv' . '>Filter-Datei �ffnen</a>';
}
?>