<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZHK','%');
	$TextKonserven[]=array('LIE','LIE_NAME1');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10012 = $AWISBenutzer->HatDasRecht(10012);		// Artikelstamm
	if(($Recht10012)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZHK_LIE_NR DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}

	$SQL = 'SELECT ZUKAUFHERSTELLERCODES.*, LIE_NAME1, LIE_NAME2';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM ZUKAUFHERSTELLERCODES ';
	$SQL .= ' INNER JOIN Lieferanten ON ZHK_LIE_NR = LIE_NR';
	$SQL .= ' WHERE ZHK_ZLH_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['Edit']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['Edit']);
		$SQL .= ' AND ZHK_KEY = 0'.$AWIS_KEY2;
	}



	// Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Blättern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsZHK = $DB->RecordsetOeffnen($SQL);
	$Form->Formular_Start();

	if(!isset($_GET['Edit']))
	{
		$Form->ZeileStart();
		if((intval($Recht10012)&6)!=0)
		{
			$Icons[] = array('new','./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Lieferanten&ZLH_KEY='.$AWIS_KEY1.'&Edit=-1');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
  		}

  		$Link = './zukaufhersteller_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZHK_LIE_NR'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZHK_LIE_NR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZHK']['ZHK_LIE_NR'],100,'',$Link);
  		$Link = './zukaufhersteller_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=LIE_NAME1'.((isset($_GET['SSort']) AND ($_GET['SSort']=='LIE_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LIE']['LIE_NAME1'],290,'',$Link);
  		$Link = './zukaufhersteller_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZHK_CODE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZHK_CODE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZHK']['ZHK_CODE'],150,'',$Link);
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZHK->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht10012&2)>0)	// Ändernrecht
			{
				$Icons[] = array('edit','./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Lieferanten&ZLH_KEY='.$AWIS_KEY1.'&Edit='.$rsZHK->FeldInhalt('ZHK_KEY'));
			}
			if(intval($Recht10012&4)>0)	// Ändernrecht
			{
				$Icons[] = array('delete','./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Lieferanten&ZLH_KEY='.$AWIS_KEY1.'&Del='.$rsZHK->FeldInhalt('ZHK_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('*ZHK_LIE_NR',$rsZHK->FeldInhalt('ZHK_LIE_NR'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*LIE_NAME1',substr($rsZHK->FeldInhalt('LIE_NAME1'),0,20),20,290,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZHK_CODE',$rsZHK->FeldInhalt('ZHK_CODE'),20,150,false,($DS%2),'','','T');

			$Form->ZeileEnde();

			$rsZHK->DSWeiter();
			$DS++;
		}


		$Link = './zukaufhersteller_Main.php?cmdAktion=Details&Seite=Historie';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
	}
	elseif($rsZHK->AnzahlDatensaetze()==1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht10012&6);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufhersteller_Main.php?cmdAktion=Details&Seite=Lieferanten&SListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZHK->FeldInhalt('ZHK_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZHK->FeldInhalt('ZHK_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->Erstelle_HiddenFeld('ZHK_KEY',$rsZHK->Feldinhalt('ZHK_KEY'));
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZHK']['ZHK_LIE_NR'].':',200);
		$SQL = 'SELECT LIE_NR, LIE_NAME1 || COALESCE(\', \' || LIE_NAME2, \'\') AS Lieferant';
		$SQL .= ' FROM Lieferanten';
		$SQL .= ' WHERE (EXISTS(SELECT * FROM lieferanteninfos WHERE lin_lie_nr = lie_nr AND lin_ity_key = 1)';
		$SQL .= ' AND NOT EXISTS(SELECT * FROM zukaufherstellercodes WHERE zhk_zlh_key = '.$DB->WertSetzen('LIE','N0',$AWIS_KEY1).' AND zhk_lie_nr = lie_nr)';
		$SQL .= ' ) OR LIE_Nr = '.$DB->WertSetzen('LIE','T',$rsZHK->FeldInhalt('ZHK_LIE_NR')).'';
		$SQL .= ' ORDER BY LIE_NAME1, LIE_NAME2';

		$Form->Erstelle_SelectFeld('!ZHK_LIE_NR',$rsZHK->FeldInhalt('ZHK_LIE_NR'),300,$EditModus,$SQL,'','', '', '', '', '', '', $DB->Bindevariablen('LIE'));
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZHK']['ZHK_CODE'].':',200);
		$Form->Erstelle_TextFeld('!ZHK_CODE',$rsZHK->FeldInhalt('ZHK_CODE'),30,300,$EditModus);
		$Form->ZeileEnde();

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>