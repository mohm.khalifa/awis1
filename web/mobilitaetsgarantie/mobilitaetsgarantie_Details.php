<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $MOB;

$DetailAnsicht = false;
$POSTsBenutzen = true;

//********************************************************
// AWIS_KEY1 setzen
//********************************************************
$AWIS_KEY1 = '';
if (isset($_GET['MOB_KEY'])) {
    $AWIS_KEY1 = $_GET['MOB_KEY'];
} elseif (isset($_POST['txtMOB_KEY'])) {
    $AWIS_KEY1 = $_POST['txtMOB_KEY'];
}

if (isset($_POST['cmdDSNeu_x'])) { //Neuanlage?
    $AWIS_KEY1 = -1;
    $_POST = array(); //Falls in nem bestehenden Datensatz auf "Neu" geklickt wurde, muss der POST geleert werden, da diese dann in die Felder geschrieben werden
}
if($AWIS_KEY1!=''){
    $MOB->Param['KEY'] = $AWIS_KEY1; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..
}

//********************************************************
// Parameter setzen und Seiten inkludieren
//********************************************************
if (isset($_POST['cmdSuche_x'])) { //�ber die Suche gekommen?
    $MOB->Param = array();
    $MOB->Param['MOB_NAME'] = $_POST['sucMOB_NAME'];
    $MOB->Param['MOB_VERS_NUMMER'] = $_POST['sucMOB_VERS_NUMMER'];
    $MOB->Param['MOB_DATUM_VON'] = $_POST['sucMOB_DATUM_VON'];
    $MOB->Param['MOB_DATUM_BIS'] = $_POST['sucMOB_DATUM_BIS'];
    $MOB->Param['MOB_FIL_ID'] = $_POST['sucMOB_FIL_ID'];
    $MOB->Param['MOB_VORGANGSNUMMER'] = $_POST['sucMOB_VORGANGSNUMMER'];
    $MOB->Param['MOB_KFZ_KENNZEICHEN'] = $_POST['sucMOB_KFZ_KENNZEICHEN'];


    $MOB->Param['KEY'] = '';
    $MOB->Param['WHERE'] = '';
    $MOB->Param['ORDER'] = '';
    $MOB->Param['BLOCK'] = 1;
    $MOB->Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    $POSTsBenutzen = false;
} elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) { //Irgendwas mit l�schen?
    include('./mobilitaetsgarantie_loeschen.php');
} elseif (isset($_POST['cmdSpeichern_x'])) { //Oder Speichern?
    include('./mobilitaetsgarantie_speichern.php');
}elseif(isset($_GET['Liste'])){
    //Liste anzeigen
    $AWIS_KEY1 = '';
}
else{ //User hat den Reiter gewechselt.
    $AWIS_KEY1 = $MOB->Param['KEY'];
}

//*********************************************************
//* SQL Vorbereiten: Sortierung
//*********************************************************
if (!isset($_GET['Sort'])) {
    if (isset($MOB->Param['ORDER']) AND $MOB->Param['ORDER'] != '') {
        $ORDERBY = ' ORDER BY ' . $MOB->Param['ORDER'];
    } else {
        $ORDERBY = ' ORDER BY MOB_NAME1 DESC';
        $MOB->Param['ORDER'] = ' MOB_NAME1 DESC ';
    }
} else {
    $MOB->Param['ORDER'] = str_replace('~', ' DESC ', $_GET['Sort']);
    $ORDERBY = ' ORDER BY ' . $MOB->Param['ORDER'];
}

//********************************************************
// SQL Vorbereiten: Bedingung
//********************************************************
$Bedingung = $MOB->BedingungErstellen();

//********************************************************
// SQL der Seite
//********************************************************
$SQL  ='SELECT ';
$SQL .='  MOB_KEY,';
$SQL .='   MOB_NAME1,';
$SQL .='   MOB_NAME2,';
$SQL .='   MOB_NAME3,';
$SQL .='   MOB_FIL_ID,';
$SQL .='   MOB_DATUM,';
$SQL .='   MOB_STRASSE,';
$SQL .='   MOB_POSTLEITZAHL,';
$SQL .='   MOB_ORT,';
$SQL .='   MOB_LAN_ATUSTAATS_NR,';
$SQL .='   MOB_KFZ_KENNZEICHEN,';
$SQL .='   MOB_ERSTZULASSUNG,';
$SQL .='   MOB_KM_STAND,';
$SQL .='   MOB_VERS_NUMMER,';
$SQL .='   MOB_VERS_GUELTIG_AB,';
$SQL .='   MOB_VERS_GUELTIG_BIS,';
$SQL .='   MOB_GESAMTUMSATZ,';
$SQL .='   MOB_BEMERKUNG,';
$SQL .='   MOB_ATUCARDKUNDE,';
$SQL .='   MOB_USER,';
$SQL .='   MOB_USERDAT,';
$SQL .='   MOB_CREADAT,';
$SQL .='   MOB_EXPORTDAT,';
$SQL .='   MOB_EXPORTSTATUS,';
$SQL .='   MOB_ANR_ID,';
$SQL .='   MOB_TITEL,';
$SQL .='   MOB_KFZ_KENNZEICHEN_KOMP,';
$SQL .='   MOB_BSA,';
$SQL .='   MOB_VORGANGSNUMMER';
$SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
$SQL .=' FROM MOBILITAETSGARANTIEN ';

if ($Bedingung != '') {
    $SQL .= ' WHERE ' . substr($Bedingung, 4);
}

//********************************************************
// SQL Nachbereiten: Bl�tternfunktion
//********************************************************
if ($AWIS_KEY1 == '') { //Liste?
    if (isset($_REQUEST['Block'])) { //Wurde gebl�ttert?
        $Block = $MOB->Form->Format('N0', $_REQUEST['Block'], false);
        $MOB->Param['BLOCK'] = $Block;
    } elseif (isset($MOB->Param['BLOCK'])) { //Zur�ck zur Liste, Tab gewechselt..
        $Block = $MOB->Param['BLOCK'];
    } else { //�ber die Suche gekommen
        $Block = 1;
    }

    $ZeilenProSeite = $MOB->AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $MOB->DB->ErmittleZeilenAnzahl($SQL, $MOB->DB->Bindevariablen('MOB', false));
    $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $MOB->DB->WertSetzen('MOB', 'N0', $StartZeile);
    $SQL .= ' AND  ZeilenNr<' . $MOB->DB->WertSetzen('MOB', 'N0', ($StartZeile + $ZeilenProSeite));
}

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsMOB = $MOB->DB->RecordsetOeffnen($SQL, $MOB->DB->Bindevariablen('MOB', true));
$MOB->Form->DebugAusgabe(1, $MOB->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************
$MOB->Form->Formular_Start();
echo '<form name=frmmobilitaetsgarantie action=./mobilitaetsgarantie_Main.php?cmdAktion=Details method=POST>';

if ($rsMOB->EOF() and $AWIS_KEY1 == '') { //Nichts gefunden!
    $MOB->Form->Hinweistext($MOB->AWISSprachKonserven['Fehler']['err_keineDaten']);
} elseif ($rsMOB->AnzahlDatensaetze() > 1) {// Liste

    $FeldBreiten = array();
    $FeldBreiten['MOB_VERS_NUMMER'] = 150;
    $FeldBreiten['MOB_NAME1'] = 150;
    $FeldBreiten['MOB_NAME2'] = 150;
    $FeldBreiten['MOB_NAME3'] = 150;
    $FeldBreiten['MOB_KFZ_KENNZEICHEN'] = 150;
    $FeldBreiten['MOB_POSTLEITZAHL'] = 150;

    $MOB->Form->ZeileStart();
    $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=MOB_VERS_NUMMER' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MOB_VERS_NUMMER'))?'~':'');
    $MOB->Form->Erstelle_Liste_Ueberschrift($MOB->AWISSprachKonserven['MOB']['MOB_KURZ_VERSNR'], $FeldBreiten['MOB_VERS_NUMMER'], '', $Link);

    $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=MOB_NAME1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MOB_NAME1'))?'~':'');
    $MOB->Form->Erstelle_Liste_Ueberschrift($MOB->AWISSprachKonserven['MOB']['MOB_NAME1'], $FeldBreiten['MOB_NAME1'], '', $Link);

    $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=MOB_NAME2' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MOB_NAME2'))?'~':'');
    $MOB->Form->Erstelle_Liste_Ueberschrift($MOB->AWISSprachKonserven['MOB']['MOB_NAME2'], $FeldBreiten['MOB_NAME2'], '', $Link);

    $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=MOB_NAME3' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MOB_NAME3'))?'~':'');
    $MOB->Form->Erstelle_Liste_Ueberschrift($MOB->AWISSprachKonserven['MOB']['MOB_NAME3'], $FeldBreiten['MOB_NAME3'], '', $Link);

    $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=MOB_KFZ_KENNZEICHEN' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MOB_KFZ_KENNZEICHEN'))?'~':'');
    $MOB->Form->Erstelle_Liste_Ueberschrift($MOB->AWISSprachKonserven['MOB']['MOB_KFZ_KENNZEICHEN'], $FeldBreiten['MOB_KFZ_KENNZEICHEN'], '', $Link);

    $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
    $Link .= '&Sort=MOB_POSTLEITZAHL' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'MOB_POSTLEITZAHL'))?'~':'');
    $MOB->Form->Erstelle_Liste_Ueberschrift($MOB->AWISSprachKonserven['MOB']['MOB_POSTLEITZAHL'], $FeldBreiten['MOB_POSTLEITZAHL'], '', $Link);

    $MOB->Form->ZeileEnde();

    $DS = 0;
    while (!$rsMOB->EOF()) {
        $HG = ($DS % 2);
        $MOB->Form->ZeileStart();
        $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details&MOB_KEY=0' . $rsMOB->FeldInhalt('MOB_KEY') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $MOB->Form->Erstelle_ListenFeld('MOB_VERS_NUMMER', $rsMOB->FeldInhalt('MOB_VERS_NUMMER'), 0, $FeldBreiten['MOB_VERS_NUMMER'], false, $HG, '', $Link, 'T', 'L',$rsMOB->FeldInhalt('MOB_VERS_NUMMER'));
        $MOB->Form->Erstelle_ListenFeld('MOB_NAME1', $rsMOB->FeldInhalt('MOB_NAME1'), 0, $FeldBreiten['MOB_NAME1'], false, $HG, '', '', 'T', 'L',$rsMOB->FeldInhalt('MOB_NAME1'));
        $MOB->Form->Erstelle_ListenFeld('MOB_NAME2', $rsMOB->FeldInhalt('MOB_NAME2'), 0, $FeldBreiten['MOB_NAME2'], false, $HG, '', '', 'T', 'L',$rsMOB->FeldInhalt('MOB_NAME2'));
        $MOB->Form->Erstelle_ListenFeld('MOB_NAME3', $rsMOB->FeldInhalt('MOB_NAME3'), 0, $FeldBreiten['MOB_NAME3'], false, $HG, '', '', 'T', 'L',$rsMOB->FeldInhalt('MOB_NAME3'));
        $MOB->Form->Erstelle_ListenFeld('MOB_KFZ_KENNZEICHEN', $rsMOB->FeldInhalt('MOB_KFZ_KENNZEICHEN'), 0, $FeldBreiten['MOB_KFZ_KENNZEICHEN'], false, $HG, '', '', 'T', 'L',$rsMOB->FeldInhalt('MOB_KFZ_KENNZEICHEN'));
        $MOB->Form->Erstelle_ListenFeld('MOB_POSTLEITZAHL', $rsMOB->FeldInhalt('MOB_POSTLEITZAHL'), 0, $FeldBreiten['MOB_POSTLEITZAHL'], false, $HG, '', '', 'T', 'L',$rsMOB->FeldInhalt('MOB_POSTLEITZAHL'));

        $MOB->Form->ZeileEnde();

        $rsMOB->DSWeiter();
        $DS++;
    }

    $Link = './mobilitaetsgarantie_Main.php?cmdAktion=Details';
    $MOB->Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
} else { //Ein Datensatz
    if($rsMOB->FeldInhalt('MOB_KEY')!=''){
        $AWIS_KEY1 = $rsMOB->FeldInhalt('MOB_KEY');
    }

    $MOB->Form->Erstelle_HiddenFeld('MOB_KEY', $AWIS_KEY1);

    // Infozeile zusammenbauen
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./mobilitaetsgarantie_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $MOB->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMOB->FeldInhalt('MOB_USER'));
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMOB->FeldInhalt('MOB_USERDAT'));
    $MOB->Form->InfoZeile($Felder, '');

    $EditRecht = (($MOB->Recht56000 & 2) != 0);


    $MOB->Form->FormularBereichStart();
    $MOB->Form->FormularBereichInhaltStart('Versicherungsdaten',true);

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_VERS_NUMMER'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_VERS_NUMMER', $rsMOB->FeldOderPOST('MOB_VERS_NUMMER','',$POSTsBenutzen), 15, 150, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_VERS_GUELTIG_AB'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_VERS_GUELTIG_AB', $rsMOB->FeldOderPOST('MOB_VERS_GUELTIG_AB','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'D', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_VERS_GUELTIG_BIS'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_VERS_GUELTIG_BIS', $rsMOB->FeldOderPOST('MOB_VERS_GUELTIG_BIS','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'D', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->FormularBereichInhaltEnde();
    $MOB->Form->FormularBereichEnde();


    $MOB->Form->FormularBereichStart();
    $MOB->Form->FormularBereichInhaltStart('Kundendaten',true);

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_KFZ_KENNZEICHEN'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_KFZ_KENNZEICHEN', $rsMOB->FeldOderPOST('MOB_KFZ_KENNZEICHEN','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_ANR_ID'] . ':', 170);
    $SQL = ' select anr_id, anr_anrede from anreden ';
    $MOB->Form->Erstelle_SelectFeld('MOB_ANR_ID',$rsMOB->FeldOderPOST('MOB_ANR_ID','',$POSTsBenutzen),'150:110',$EditRecht,$SQL,$MOB->OptionBitteWaehlen);
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_TITEL'] . ':', 53);
    $MOB->Form->Erstelle_TextFeld('MOB_TITEL', $rsMOB->FeldOderPOST('MOB_TITEL','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();


    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_NAME1'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('MOB_NAME1', $rsMOB->FeldOderPOST('MOB_NAME1','',$POSTsBenutzen), 45, 320, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_NAME2'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('MOB_NAME2', $rsMOB->FeldOderPOST('MOB_NAME2','',$POSTsBenutzen), 45, 320, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_NAME3'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('MOB_NAME3', $rsMOB->FeldOderPOST('MOB_NAME3','',$POSTsBenutzen), 45, 320, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_STRASSE'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('MOB_STRASSE', $rsMOB->FeldOderPOST('MOB_STRASSE','',$POSTsBenutzen), 45, 320, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_POSTLEITZAHL'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('MOB_POSTLEITZAHL', $rsMOB->FeldOderPOST('MOB_POSTLEITZAHL','',$POSTsBenutzen), 6, 150, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_ORT'] . ':', 53);
    $MOB->Form->Erstelle_TextFeld('MOB_ORT', $rsMOB->FeldOderPOST('MOB_ORT','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();


    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_LAN_ATUSTAATS_NR'] . ':', 170);
    $SQL = 'select LAN_ATU_STAAT_NR, LAN_LAND from LAENDER where LAN_ATU_STAAT_NR is not null order by LAN_LAND asc ';
    $MOB->Form->Erstelle_SelectFeld('MOB_LAN_ATUSTAATS_NR', $rsMOB->FeldOderPOST('MOB_LAN_ATUSTAATS_NR','',$POSTsBenutzen),'313:313',$EditRecht,$SQL,'',4);
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_ATUCARDKUNDE'] . ':', 170);
    $MOB->Form->Erstelle_Checkbox('MOB_ATUCARDKUNDE', $rsMOB->FeldOderPOST('MOB_LAN_ATUSTAATS_NR','',$POSTsBenutzen),40,$EditRecht,1);
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_ERSTZULASSUNG'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('MOB_ERSTZULASSUNG', $rsMOB->FeldOderPOST('MOB_ERSTZULASSUNG','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'D', '', '');
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_KM_STAND'] . ':', 53);
    $MOB->Form->Erstelle_TextFeld('!MOB_KM_STAND', $rsMOB->FeldOderPOST('MOB_KM_STAND','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'T', '', $MOB->AWISSprachKonserven['MOB']['MOB_FORMAT_KM'],'','','','','pattern="\d+"');
    $MOB->Form->ZeileEnde();



    $MOB->Form->FormularBereichInhaltEnde();
    $MOB->Form->FormularBereichEnde();

    $MOB->Form->FormularBereichStart();
    $MOB->Form->FormularBereichInhaltStart('Kassendaten',($AWIS_KEY1===-1?true:false));

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_FIL_ID'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_FIL_ID', $rsMOB->FeldOderPOST('MOB_FIL_ID','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_DATUM'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_DATUM', $rsMOB->FeldOderPOST('MOB_DATUM','',$POSTsBenutzen), 40, 320, $EditRecht, '', '', '', 'DU', '', '');
    $MOB->Form->ZeileEnde();


    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_VORGANGSNUMMER'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_VORGANGSNUMMER', $rsMOB->FeldOderPOST('MOB_VORGANGSNUMMER','',$POSTsBenutzen), 45, 320, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_BSA'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('!MOB_BSA', $rsMOB->FeldOderPOST('MOB_BSA','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'T', '', '');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_GESAMTUMSATZ'] . ':', 170);
    $MOB->Form->Erstelle_TextFeld('MOB_GESAMTUMSATZ', $rsMOB->FeldOderPOST('MOB_GESAMTUMSATZ','',$POSTsBenutzen), 12, 150, $EditRecht, '', '', '', 'T', '', $MOB->AWISSprachKonserven['MOB']['MOB_FORMAT_BETRAG'],'','','','','pattern="[0-9]+(\\,[0-9][0-9]?)?"');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_BEMERKUNG'] . ':', 170);
    $MOB->Form->Erstelle_Textarea('MOB_BEMERKUNG',$rsMOB->FeldOderPOST('MOB_BEMERKUNG','',$POSTsBenutzen),300,30,3,$EditRecht);
    $MOB->Form->ZeileEnde();

    $MOB->Form->FormularBereichInhaltEnde();
    $MOB->Form->FormularBereichEnde();



}

//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$MOB->Form->Formular_Ende();
$MOB->Form->SchaltflaechenStart();

$MOB->Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $MOB->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

if (($MOB->Recht56000 & 2) ==2 AND $AWIS_KEY1!='') { //Speichern erlaubt + in einen Datensatz?
    $MOB->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $MOB->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
}

if (($MOB->Recht56000 & 4) == 4 AND $AWIS_KEY1!=-1) {// Hinzuf�gen erlaubt + nicht gerade in einer Neuanlage?
    $MOB->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $MOB->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

if (($MOB->Recht56000 & 64) == 64 AND  $AWIS_KEY1>0) { //L�schen erlaubt + in einen Datensatz?
    $MOB->Form->Schaltflaeche('image', 'cmdPDF', '', '/bilder/cmd_PDF.png', $MOB->AWISSprachKonserven['Wort']['lbl_loeschen'], '');
}

$MOB->Form->SchaltflaechenEnde();

$MOB->Form->SchreibeHTMLCode('</form>');

?>