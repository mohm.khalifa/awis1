<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $EmailMeldung;
require_once "awisMailer.inc";
require_once "awisDokument.inc";
require_once "awisSeminarplanEmailHotel.php";


$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_keinPersonal');
$TextKonserven[]=array('Fehler','err_keinegueltigeATUNR');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('SEP','SEP_%');
$TextKonserven[]=array('SER','SER_%');
$TextKonserven[]=array('SRG','SRG_%');
$TextKonserven[]=array('SEH','SEH_%');
$TextKonserven[]=array('SHZ','%');
$TextKonserven[]=array('Wort','lbl_weiter');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);

	//***********************************************
	// Seminarplan
	//***********************************************
	$Form->DebugAusgabe(1,$_POST);

	if (isset($_GET['ok']))
	{
		$Key=$_GET['ok'];
		$Status = 'Gebucht';

		$SQL = 'SELECT SET_SEP_KEY, SET_NACHNAME, SET_VORNAME, SEP_TRAINING';
		$SQL.= ' FROM SEMINARTEILNEHMER ';
		$SQL.= ' INNER JOIN SEMINARPLAN ON SEP_KEY = SET_SEP_KEY WHERE SET_KEY = 0'.$Key. '';
		$rsDaten = $DB->RecordSetOeffnen($SQL);
		$AWIS_KEY1 = $rsDaten->FeldInhalt('SET_SEP_KEY');

		$SQL = 'UPDATE Seminarteilnehmer SET';
		$SQL .= ' SET_STATUS = '.$DB->FeldInhaltFormat('T',$Status,false);
		$SQL .= ', SET_USER = \''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', SET_USERDAT = SYSDATE';
		$SQL .= ' WHERE SET_KEY = 0'.$Key. '';

		$Form->DebugAusgabe(1,$SQL);
		if($DB->Ausfuehren($SQL)===false)
		{
			$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
		}
		$AWIS_KEY2=0;//Liste zeigen
	}

	if(isset($_POST['txtSEP_KEY']))
	{
		$AWIS_KEY1=$_POST['txtSEP_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtSEP_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('SEP','SEP_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtSEP_KEY'])==0)		//Neue R�ckf�hrung
			{
				//Daten auf Vollst�ndigkeit pr�fen
				$Fehler = '';
				$Pflichtfelder = array('SEP_SEMINAR', 'SEP_TRAINING');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if($_POST['txt'.$Pflichtfeld]=='')	// Filiale muss angegeben werden
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SEP'][$Pflichtfeld].'<br>';
					}
				}

				// Wurden Fehler entdeckt? => Speichern abbrechen
				if($Fehler!='')
				{
					$Form->ZeileStart();
					$Form->Hinweistext($Fehler);
					$Form->ZeileEnde();

					die();

					$Speichern = false;
				}
				else
				{
					$Speichern = true;
				}

				if($Speichern == true)
				{
					$Fehler = '';
					$SQL = 'INSERT INTO SEMINARPLAN';
					$SQL .= '(SEP_SEMINAR, SEP_BESCHREIBUNG, SEP_TRAINING, SEP_VON, SEP_BIS,';
					$SQL .= ' SEP_FREIEPLAETZE, SEP_SER_KEY, SEP_SRG_KEY, SEP_ZIELGRUPPE, SEP_TRAINER, SEP_STATUS, SEP_SCHULUNGSART, SEP_USER, SEP_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= '' . $DB->FeldInhaltFormat('T',$_POST['txtSEP_SEMINAR'],true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSEP_BESCHREIBUNG'])?$_POST['txtSEP_BESCHREIBUNG']:''),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',$_POST['txtSEP_TRAINING'],true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('DU',(isset($_POST['txtSEP_VON'])?$_POST['txtSEP_VON']:''),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('DU',(isset($_POST['txtSEP_BIS'])?$_POST['txtSEP_BIS']:''),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('Z',(isset($_POST['txtSEP_FREIEPLAETZE'])?$_POST['txtSEP_FREIEPLAETZE']:''),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('Z',(isset($_POST['txtSEP_SER_KEY'])?$_POST['txtSEP_SER_KEY']:''),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('Z',(isset($_POST['txtSEP_SRG_KEY'])?$_POST['txtSEP_SRG_KEY']:''),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSEP_ZIELGRUPPE'])?$_POST['txtSEP_ZIELGRUPPE']:''),true);
					$SQL .= ' ,' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSEP_TRAINER'])?$_POST['txtSEP_TRAINER']:''),true);
					$SQL .= ',1';
					$SQL .= ' ,' . $DB->FeldInhaltFormat('Z',(isset($_POST['txtSEP_SCHULUNGSART'])?$_POST['txtSEP_SCHULUNGSART']:''),true);
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					try{
                        $DB->Ausfuehren($SQL);
					}catch (Exception $ex){
                        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201909120941");
                        die();
                    }
					$SQL = 'SELECT seq_SEP_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');

					if(isset($_POST['txtSEP_SER_KEY']) and $_POST['txtSEP_SER_KEY'] == '-99'){
                        MergeSeminarinfo($DB,$AWISBenutzer, 604,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SER_KEY']);
                    }

                    if(isset($_POST['txtSEP_SRG_KEY']) and $_POST['txtSEP_SRG_KEY'] == '-99'){
                        MergeSeminarinfo($DB,$AWISBenutzer, 600,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SRG_KEY']);
                        MergeSeminarinfo($DB,$AWISBenutzer, 601,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SPA_PLZ']);
                        MergeSeminarinfo($DB,$AWISBenutzer, 602,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SPA_ORT']);
                        MergeSeminarinfo($DB,$AWISBenutzer, 603,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SPA_STRASSE']);
                    }

				}
			}
			else
			{
				$Felder = explode(';',$Form->NameInArray($_POST, 'txtSEP_',1,1));
				$FehlerListe = array();
				$UpdateFelder = '';
				$Fehler = '';

				$rsSEP = $DB->RecordSetOeffnen('SELECT * FROM SEMINARPLAN WHERE SEP_key=' . $_POST['txtSEP_KEY'] . '');

				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);

					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						switch ($FeldName)
						{
							default:
							    $Typ = $rsSEP->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT);
							    if($FeldName=='SEP_VON' or $FeldName=='SEP_BIS'){
							        $Typ='DU';
                                }
								$WertNeu=$DB->FeldInhaltFormat($Typ,$_POST[$Feld],true);
						}

						$WertAlt=$DB->FeldInhaltFormat($rsSEP->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsSEP->FeldInfo($FeldName,awisRecordset::FELDINFO_FORMAT),$rsSEP->FeldInhalt($FeldName),true);

						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}


				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsSEP->FeldInhalt('SEP_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE SEMINARPLAN SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', SEP_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SEP_userdat=sysdate';
					$SQL .= ' WHERE SEP_key=0' . $_POST['txtSEP_KEY'] . '';
					if($DB->Ausfuehren($SQL)===false)
					{
						$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'908040943');
						throw new awisException('Fehler beim Speichern',908040943,$SQL,2);
					}
				}
                if(isset($_POST['txtSEP_SER_KEY']) and $_POST['txtSEP_SER_KEY'] == '-99'){
                    MergeSeminarinfo($DB,$AWISBenutzer, 604,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SER_KEY']);
                }

                if(isset($_POST['txtSEP_SRG_KEY']) and $_POST['txtSEP_SRG_KEY'] == '-99'){
                    MergeSeminarinfo($DB,$AWISBenutzer, 600,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SRG_KEY']);
                    MergeSeminarinfo($DB,$AWISBenutzer, 601,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SPA_PLZ']);
                    MergeSeminarinfo($DB,$AWISBenutzer, 602,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SPA_ORT']);
                    MergeSeminarinfo($DB,$AWISBenutzer, 603,$AWIS_KEY1,'SEP',$_POST['txtSONST_SEP_SPA_STRASSE']);
                }

			}
		}
	}

	//***********************************************************************************
	//** Seminarteilnehmer speichern
	//***********************************************************************************
	if(isset($_POST['txtSET_KEY']))
	{
		if(intval($_POST['txtSET_KEY'])==0)		// Neuer Teilnehmer
		{
			$Fehler = '';

			//Pr�fen, ob diese Teilnehmer bereits vorhanden ist
			$SQL = 'SELECT SET_KEY';
			$SQL .= ' FROM SEMINARPLAN ';
			$SQL .= ' LEFT JOIN SEMINARTEILNEHMER on SET_SEP_KEY = SEP_KEY';
			$SQL .= ' WHERE SET_PER_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtSET_PER_NR'],false);
			$SQL .= ' AND SEP_KEY = '.$AWIS_KEY1;

			$rsSET = $DB->RecordSetOeffnen($SQL);

			if ($rsSET->AnzahlDatensaetze()>0)
			{
				$Fehler .= 'Personalnummer bereits vorhanden: '.$_POST['txtSET_PER_NR'].'<br>';
			}

			// Daten auf Vollst�ndigkeit pr�fen
			$Pflichtfelder = array('SET_PER_NR');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Personalnummer muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['SET'][$Pflichtfeld].'<br>';
				}
			}

			//Wenn Personalnummer eingegeben wurde, pr�fen ob diese eine g�ltige ist
			if ($_POST['txtSET_PER_NR']!='')
			{
				$SQL = 'SELECT NAME, VORNAME, ABTEILUNG, PERSNR ';
				$SQL .= 'FROM PERSONAL_KOMPLETT ';
				$SQL .= 'WHERE (DATUM_EINTRITT <= SYSDATE AND ';
				$SQL .= '(DATUM_AUSTRITT > SYSDATE OR DATUM_AUSTRITT IS NULL)) ';
				$SQL .= 'AND PERSNR = '.$DB->FeldInhaltFormat('NO',$_POST['txtSET_PER_NR'],false);

				$rsPER = $DB->RecordSetOeffnen($SQL);
				if($rsPER->AnzahlDatensaetze()==0)
				{
					$Fehler .= 'Keine g�ltige Personalnummer:'.$_POST['txtSET_PER_NR'].'<br>';
				}
			}

			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->ZeileStart();
				$Form->Hinweistext($Fehler);
				$Form->ZeileEnde();
				$Speichern=false;

				$Link='./seminarplan_Main.php?cmdAktion=Details&SET_KEY='.$AWIS_KEY1;
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdTeilnehmer',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();
				die();
			}
			else
			{
				$Speichern = true;
			}

			if ($Speichern == true)
			{
                //Pr�fen, ob noch freie Pl�tze zu diesem Seminar vorhanden sind
                $SQL = 'SELECT SEP_FREIEPLAETZE, SEP_TRAINING ';
                $SQL .= 'FROM SEMINARPLAN ';
                $SQL .= 'WHERE SEP_KEY = '.$DB->WertSetzen('SEP','Z',$AWIS_KEY1);

                $rsSEP = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SEP'));
                $Training = $rsSEP->FeldInhalt('SEP_TRAINING');

                $freiePlaetze = true;
                if($DB->FeldInhaltFormat('NO', $rsSEP->FeldInhalt('SEP_FREIEPLAETZE')) <= 0) {
                    $freiePlaetze = false;
                }

                $SQL = 'INSERT INTO Seminarteilnehmer ';
				$SQL .= '(SET_SEP_KEY, SET_PER_NR, SET_NACHNAME, SET_VORNAME, SET_ANMELDETAG, SET_STATUS, SET_FILIALE';
				$SQL .= ', SET_STATUSTEILNEHMER, SET_USER, SET_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSET_PER_NR'])?$_POST['txtSET_PER_NR']:''),false);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$rsPER->FeldInhalt('NAME'),false);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$rsPER->FeldInhalt('VORNAME'),false);
				$SQL .= ',SYSDATE';
				$SQL .= ',' . $DB->FeldInhaltFormat('T',($freiePlaetze)?"Gebucht":'Warteliste',false);
				$SQL .= ',' . $DB->FeldInhaltFormat('T','Zentrale',false);
				$SQL .= ',1';
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				$Form->DebugAusgabe(1,$SQL);
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
				}
				$AWIS_KEY2=0;//Liste zeigen

                if($freiePlaetze) {
                    //Freie pl�tze reduzieren
                    $SQL = 'UPDATE Seminarplan SET';
                    $SQL .= ' SEP_FREIEPLAETZE = SEP_FREIEPLAETZE - 1';
                    $SQL .= ', SEP_USER = ' . $DB->WertSetzen('SEP','T',$AWISBenutzer->BenutzerName());
                    $SQL .= ', SEP_USERDAT = SYSDATE';
                    $SQL .= ' WHERE SEP_KEY = ' . $DB->WertSetzen('SEP','Z',$AWIS_KEY1);
                    $Form->DebugAusgabe(1, $SQL);
                    $DB->Ausfuehren($SQL,'',false,$DB->Bindevariablen('SEP'));

                    $Text = 'Training: ' . $Training . chr(10);
                    $Text .= 'Name: ' . $rsPER->FeldInhalt('NAME') . ' ' . $rsPER->FeldInhalt('VORNAME');

                    if (awisWerkzeuge::awisLevel() == 'ENTW') {
                        $Email = 'shuttle@de.atu.eu';
                    } else {
                        $Email = 'ATUAcademy@de.atu.eu';
                    }

                    $SQL = 'BEGIN P_AWIS_MAIL.PROC_SEND_MAIL(' . $DB->WertSetzen('SEP','T',$Text) . ', ' . $DB->WertSetzen('SEP','T',$Email) . ', \'Neuer Seminarteilnehmer\'); COMMIT; END;';
                    $Form->DebugAusgabe(1, $SQL);

                    $DB->Ausfuehren($SQL,'', false,$DB->Bindevariablen('SEP'));
                } else {
                    $Info = 'Keine freien Pl�tze zu dem Seminar '.$Training.' mehr vorhanden<br> Sie wurden jedoch auf die Warteliste gesetzt, und werden kontaktiert falls ein Platz frei wird<br>';
                    $Form->ZeileStart();
                    $Form->Hinweistext($Info);
                    $Form->ZeileEnde();

                    $Link='./seminarplan_Main.php?cmdAktion=Details&SET_KEY='.$AWIS_KEY1;
                    $Form->SchaltflaechenStart();
                    $Form->Schaltflaeche('href','cmdTeilnehmer',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
                    $Form->SchaltflaechenEnde();
                    die();
                }
			}
		}
	}

	//***********************************************************************************
	//** Seminarfeedbacks speichern
	//***********************************************************************************
	if(isset($_POST['txtSEE_KEY']))
	{
		if(intval($_POST['txtSEE_KEY'])==0)		// Neuer Teilnehmer
		{
			$Fehler = '';
			//Pr�fen, ob dieses Feedback bereits vorhanden ist
			$SQL = 'SELECT SEE_BEZEICHNUNG,SEE_KEY';
			$SQL .= ' FROM SEMINARFEEDBACK ';
			$SQL .= ' WHERE UPPER(SEE_BEZEICHNUNG) = UPPER('.$DB->FeldInhaltFormat('T',$_POST['txtSEE_BEZEICHNUNG'],false).')';

			$rsFail = $DB->RecordSetOeffnen($SQL);

			if ($rsFail->AnzahlDatensaetze()>0)
			{
				//$Fehler .= $TXT_Speichern['Fehler']['err_ATUNRBereitsVorhanden'].': '.$_POST['txtRFF_AST_ATUNR'].'<br>';
				$Fehler .= 'Feedback bereits vorhanden: '.$_POST['txtSEE_BEZEICHNUNG'].'<br>';
			}
			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->ZeileStart();
				$Form->Hinweistext($Fehler);
				$Form->ZeileEnde();
				$Speichern=false;

				$Link='./seminarplan_Main.php?cmdAktion=Details&SEE_KEY='.$rsFail->FeldInhalt('SEE_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href','cmdTeilnehmer',$Link,'/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();
				die();
			}
			else
			{
				$Speichern = true;
			}

			if ($Speichern == true)
			{
				$SQL = 'INSERT INTO SEMINARFEEDBACK';
				$SQL .= '(SEE_SEP_KEY, SEE_BEZEICHNUNG, SEE_USER,SEE_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',(isset($_POST['txtSEE_BEZEICHNUNG'])?$_POST['txtSEE_BEZEICHNUNG']:''),false);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				$Form->DebugAusgabe(1,$SQL);
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
				}
			}
		}
		else
		{
			$SQL = 'UPDATE SEMINARFEEDBACK SET';
			$SQL.= ' SEE_BEZEICHNUNG = '. $DB->FeldInhaltFormat('T',(isset($_POST['txtSEE_BEZEICHNUNG'])?$_POST['txtSEE_BEZEICHNUNG']:''),false);
			$SQL.= ', SEE_USER = \''. $AWISBenutzer->BenutzerName() .'\'';
			$SQL.= ', SEE_USERDAT = SYSDATE';
			$SQL.=' WHERE SEE_KEY = 0'.$_POST['txtSEE_KEY'];
			$Form->DebugAusgabe(1,$SQL);
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
			}
		}
		$Felder = explode(';',$Form->NameInArray($_POST, 'txtSEF_',1,1));
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			if($FeldTeile[3] == '')
			{

			}
			else
			{
				//var_dump($FeldTeile[3]);
				$SQL = 'UPDATE SEMINARZUORD SET';
				$SQL.= ' SEZ_WERT = '. $DB->FeldInhaltFormat('T',(isset($_POST[$Feld])?substr($_POST[$Feld],0,3999):''),false);
				$SQL.= ', SEZ_USER = \''. $AWISBenutzer->BenutzerName() .'\'';
				$SQL.= ', SEZ_USERDAT = SYSDATE';
				$SQL.=' WHERE SEZ_KEY = 0'.$FeldTeile[3];
				$SQL.=' AND SEZ_SEF_ID = 0'.$FeldTeile[2];
				//$Form->DebugAusgabe(1,$SQL);
				if($DB->Ausfuehren($SQL)===false)
				{
					$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
				}
			}
			$AWIS_KEY2=0;//Liste zeigen
		}
	}


	//***********************************************************************************
	//** Seminarkriteriumsabschluss speichern
	//***********************************************************************************
	if(isset($_POST['txtSEZ_KEY']) and isset($_POST['txtSKK_ABSCHLUSS']) and $_POST['txtSEZ_KEY'] != '' and $_POST['txtSKK_ABSCHLUSS'] != '')
	{
		$SQL = 'UPDATE SEMINARZUORD SET';
		$SQL.= ' SEZ_ABGESCHL = '. $DB->FeldInhaltFormat('N0',$_POST['txtSKK_ABSCHLUSS'],false);
		$SQL.= ', SEZ_USER = \''. $AWISBenutzer->BenutzerName() .'\'';
		$SQL.= ', SEZ_USERDAT = SYSDATE';
		$SQL.=' WHERE SEZ_KEY = 0'.$_POST['txtSEZ_KEY'];
		$Form->DebugAusgabe(1,$SQL);
		if($DB->Ausfuehren($SQL)===false)
		{
			$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
		}
	}

	//***********************************************************************************
	//** Seminarkriterium speichern
	//***********************************************************************************
	if(isset($_POST['txtSKK_KEY']))
	{
		if($_POST['txtSKK_KEY'] > -1)
		{
			$SQL = 'UPDATE SEMINARKRITERIUM SET';
			$SQL.= ' SKK_KRITERIUM1 = '. $DB->FeldInhaltFormat('N0',$_POST['txtSKK_KRITERIUM1'],true);
			$SQL.= ', SKK_KRITERIUM2 = '. $DB->FeldInhaltFormat('N0',$_POST['txtSKK_KRITERIUM2'],true);
			$SQL.= ', SKK_KRITERIUM3 = '. $DB->FeldInhaltFormat('N0',$_POST['txtSKK_KRITERIUM3'],true);
			$SQL.= ', SKK_USER = \''. $AWISBenutzer->BenutzerName() .'\'';
			$SQL.= ', SKK_USERDAT = SYSDATE';
			$SQL.=' WHERE SKK_KEY = 0'.$_POST['txtSKK_KEY'];
			$Form->DebugAusgabe(1,$SQL);
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
			}
		}
		else
		{
			$SQL = 'INSERT INTO SEMINARKRITERIUM';
			$SQL .= '(SKK_SEZ_ID,SKK_KRITERIUM1,SKK_KRITERIUM2,SKK_KRITERIUM3,SKK_USER,SKK_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtSEZ_KEY'],false);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSKK_KRITERIUM1'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSKK_KRITERIUM2'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtSKK_KRITERIUM3'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			//$Form->DebugAusgabe(1,$SQL);
			if($DB->Ausfuehren($SQL)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201011041515');
			}
		}
	}

    //***********************************************************************************
    //** Seminarkriterium speichern
    //***********************************************************************************
    if(isset($_POST['txtSEA_KEY']))
    {
        if($_POST['txtSEA_KEY'] > -1)
        {
            $SQL = 'UPDATE SEMINARANZEIGE SET';
            $SQL.= ' SEA_BEREICH = '. $DB->FeldInhaltFormat('T',$_POST['txtSEA_BEREICH'],false);
            $SQL.= ', SEA_BESCHREIBUNG = '. $DB->FeldInhaltFormat('T',$_POST['txtSEA_BEZEICHNUNG'],true);
            $SQL.= ', SEA_USER = \''. $AWISBenutzer->BenutzerName() .'\'';
            $SQL.= ', SEA_USERDAT = SYSDATE';
            $SQL.=' WHERE SEA_KEY = '.$_POST['txtSEA_KEY'];
            $Form->DebugAusgabe(1,$SQL);
            if($DB->Ausfuehren($SQL)===false)
            {
                $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201802200954');
            }
        }
        else
        {
            $SQL = 'INSERT INTO SEMINARANZEIGE';
            $SQL .= '(SEA_BEREICH,SEA_BESCHREIBUNG,SEA_USER,SEA_USERDAT';
            $SQL .= ')VALUES (';
            $SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtSEA_BEREICH'],false);
            $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtSEA_BEZEICHNUNG'],true);
            $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= ')';
            //$Form->DebugAusgabe(1,$SQL);
            if($DB->Ausfuehren($SQL)===false)
            {
                $Form->Fehler_Anzeigen('SpeicherFehler',$awisDBFehler['message'],'WIEDERHOLEN',5,'201802200957');
            }
        }
    }

    //***********************************************************************************
    //** Hotelinformationen speichern
    //***********************************************************************************
    if (isset($_POST['txtSEH_NAME'])) {

        if ($AWIS_KEY1 == '') {
            $SQL = 'insert into SEMINARHOTEL';
            $SQL .= ' (SEH_LAN_CODE, SEH_NAME, SEH_ORT, SEH_PLZ, SEH_STRASSE, SEH_HAUSNR, SEH_HOMEPAGE, SEH_USER, SEH_USERDAT)';
            $SQL .= ' VALUES (';
            $SQL .= ' ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_LAN_CODE'], false);
            $SQL .= ', ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_NAME'], false);
            $SQL .= ', ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_ORT'], true);
            $SQL .= ', ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_PLZ'], true);
            $SQL .= ', ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_STRASSE'], true);
            $SQL .= ', ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_HAUSNR'], true);
            $SQL .= ', ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_HOMEPAGE'], true);
            $SQL .= ', ' . $DB->WertSetzen('SEH', 'T', $AWISBenutzer->BenutzerName(), true);
            $SQL .= ', SYSDATE)';
        } else {
            $SQL = 'update SEMINARHOTEL set';
            $SQL .= ' SEH_LAN_CODE = ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_LAN_CODE'], false);
            $SQL .= ', SEH_NAME = ' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_NAME'], false);
            $SQL .= ', SEH_ORT = '. $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_ORT'], true);
            $SQL .= ', SEH_PLZ = '. $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_PLZ'], true);
            $SQL .= ', SEH_STRASSE = '. $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_STRASSE'], true);
            $SQL .= ', SEH_HAUSNR = '. $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_HAUSNR'], true);
            $SQL .= ', SEH_HOMEPAGE =' . $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_HOMEPAGE'], true);
            $SQL .= ', SEH_USER = '. $DB->WertSetzen('SEH', 'T', $AWISBenutzer->BenutzerName(), true);
            $SQL .= ', SEH_USERDAT = SYSDATE';
            $SQL .= ', SEH_STATUS = '. $DB->WertSetzen('SEH', 'T', $_POST['txtSEH_STATUS'], true);
            $SQL .= ' where SEH_KEY = '.$_POST['txtSEH_KEY'];
        }
        $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SEH'));

        $Form->Hinweistext($AWISSprachKonserven['SEH']['SEH_DATEN_GESPEICHERT'], 3);

        if(isset($_POST['txtBacklink'])){
            echo '<script>'.PHP_EOL;

            echo 'window.location.assign("'. $_POST['txtBacklink'] . '");';
            echo '</script>' .PHP_EOL;
        }
    }


    //***********************************************************************************
    //** Teilnehmerhotelinformationen speichern
    //***********************************************************************************
    if(isset($_POST['txtSHZ_SET_KEY'])) {

        $SpeicherSETs = array($_POST['txtSHZ_SET_KEY']);

        if (isset($_POST['txtSHZ_HOTEL']) and $_POST['txtSHZ_HOTEL'] != '') {

            $SQL = 'select * from seminarhotelzuord where shz_set_key = '.$DB->WertSetzen('SHZ', 'N0', $_GET['HOTEL_SET_KEY'], true).' and shz_sep_key = '.$DB->WertSetzen('SHZ', 'N0', $AWIS_KEY1, true);
            $rsSEH = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SHZ'));

            if ($rsSEH->Feldinhalt('SHZ_SEH_KEY') == '') {
                $SQL = 'insert into SEMINARHOTELZUORD';
                $SQL .= ' (SHZ_SET_KEY, SHZ_SEH_KEY, SHZ_SEP_KEY, SHZ_HOTELVON, SHZ_HOTELBIS, SHZ_ZUSATZINFO, SHZ_USER, SHZ_USERDAT)';
                $SQL .= ' VALUES (';
                $SQL .= ' ' . $DB->WertSetzen('SHZ', 'N0', $_POST['txtSHZ_SET_KEY'], false);
                if (isset($_POST['txtSHZ_HOTEL']) and $_POST['txtSHZ_HOTEL'] > 0) {
                    $SQL .= ', ' . $DB->WertSetzen('SHZ', 'N0', $_POST['txtSHZ_HOTEL'], false);
                } else {
                    $SQL .= ', seq_seh_key.currval';
                }
                $SQL .= ', ' . $DB->WertSetzen('SHZ', 'N0', $AWIS_KEY1, false);
                $SQL .= ', ' . $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELVON'], true);
                $SQL .= ', ' . $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELBIS'], true);
                $SQL .= ', ' . $DB->WertSetzen('SHZ', 'T', $_POST['txtSHZ_ZUSATZINFO'], true);
                $SQL .= ', ' . $DB->WertSetzen('SHZ', 'T', $AWISBenutzer->BenutzerName(), true);
                $SQL .= ', SYSDATE)';
            } else {
                $SQL = 'update SEMINARHOTELZUORD set';
                $SQL .= ' SHZ_HOTELVON = '. $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELVON'], true);
                $SQL .= ', SHZ_HOTELBIS = '. $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELBIS'], true);
                $SQL .= ', SHZ_ZUSATZINFO = '. $DB->WertSetzen('SHZ', 'T', $_POST['txtSHZ_ZUSATZINFO'], true);
                $SQL .= ', SHZ_USER = '. $DB->WertSetzen('SHZ', 'T', $AWISBenutzer->BenutzerName(), true);
                $SQL .= ', SHZ_USERDAT = SYSDATE';
                $SQL .= ', SHZ_SEH_KEY = '. $DB->WertSetzen('SHZ', 'N0', $_POST['txtSHZ_HOTEL'], true);
                $SQL .= ' where shz_set_key = '. $DB->WertSetzen('SHZ', 'N0', $_GET['HOTEL_SET_KEY'], true);
                $SQL .= ' and shz_sep_key = '. $DB->WertSetzen('SHZ', 'N0', $AWIS_KEY1, true);
            }
            $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SHZ'));

            $Form->Hinweistext($AWISSprachKonserven['SEH']['SEH_DATEN_GESPEICHERT'], 3);
        }

        //Beim Hotelinformationen hinzuf�gen gibt es eine Kopierfunktion, mitwelcher man die Infos gleich auf andere Teilnehmer kopieren kann
        if(isset($_POST['txtZUSATZ_SHZ_SET_KEY']) and count($_POST['txtZUSATZ_SHZ_SET_KEY']) > 0 ){
            foreach ($_POST['txtZUSATZ_SHZ_SET_KEY'] as $SHZ_SET_KEY) {

                $SQL = "Select shz_seh_key from seminarhotelzuord ";
                $SQL .= " where shz_set_key = ". $DB->WertSetzen('SHZ', 'N0', $SHZ_SET_KEY, false);
                $SQL .= " and SHZ_SEP_KEY = ". $DB->WertSetzen('SHZ', 'N0', $AWIS_KEY1, false);

                $rsSHZ = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SHZ'));

                if($rsSHZ->AnzahlDatensaetze() == 0) {

                    $SQL = 'INSERT INTO SEMINARHOTELZUORD ';
                    $SQL .= ' (SHZ_SET_KEY, SHZ_SEH_KEY, SHZ_SEP_KEY, SHZ_HOTELVON, SHZ_HOTELBIS, SHZ_ZUSATZINFO, SHZ_USER, SHZ_USERDAT)';
                    $SQL .= ' VALUES (';
                    $SQL .= ' ' . $DB->WertSetzen('SHZ', 'N0', $SHZ_SET_KEY, false);
                    if (isset($_POST['txtSHZ_HOTEL']) and $_POST['txtSHZ_HOTEL'] > 0) {
                        $SQL .= ', ' . $DB->WertSetzen('SHZ', 'N0', $_POST['txtSHZ_HOTEL'], false);
                    } else {
                        $SQL .= ', seq_seh_key.currval';
                    }
                    $SQL .= ', ' . $DB->WertSetzen('SHZ', 'N0', $AWIS_KEY1, false);
                    $SQL .= ', ' . $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELVON'], true);
                    $SQL .= ', ' . $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELBIS'], true);
                    $SQL .= ', ' . $DB->WertSetzen('SHZ', 'T', $_POST['txtSHZ_ZUSATZINFO'], true);
                    $SQL .= ', ' . $DB->WertSetzen('SHZ', 'T', $AWISBenutzer->BenutzerName(), true);
                    $SQL .= ', SYSDATE)';

                    $DB->Ausfuehren($SQL, '', false, $DB->Bindevariablen('SHZ'));

                    $SpeicherSETs[] = $SHZ_SET_KEY;
                } else {
                    $SQL = 'update SEMINARHOTELZUORD set';
                    $SQL .= ' SHZ_HOTELVON = '. $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELVON'], true);
                    $SQL .= ', SHZ_HOTELBIS = '. $DB->WertSetzen('SHZ', 'D', $_POST['txtSHZ_HOTELBIS'], true);
                    $SQL .= ', SHZ_ZUSATZINFO = '. $DB->WertSetzen('SHZ', 'T', $_POST['txtSHZ_ZUSATZINFO'], true);
                    $SQL .= ', SHZ_USER = '. $DB->WertSetzen('SHZ', 'T', $AWISBenutzer->BenutzerName(), true);
                    $SQL .= ', SHZ_USERDAT = SYSDATE';
                    $SQL .= ', SHZ_SEH_KEY = '. $DB->WertSetzen('SHZ', 'N0', $_POST['txtSHZ_HOTEL'], true);
                    $SQL .= ' where shz_set_key = '. $DB->WertSetzen('SHZ', 'N0', $SHZ_SET_KEY, true);
                    $SQL .= ' and shz_sep_key = '. $DB->WertSetzen('SHZ', 'N0', $AWIS_KEY1, true);

                    $DB->Ausfuehren($SQL, '', true, $DB->Bindevariablen('SHZ'));

                    $SpeicherSETs[] = $SHZ_SET_KEY;
                }
            }
        }


        // *****************************************************
        // Dokumente speichern
        // *****************************************************
        if(isset($_FILES['DOC']['name']) and $_FILES['DOC']['name'] != ''){
            $DOC = new awisDokument($DB, $AWISBenutzer);
            foreach ($SpeicherSETs as $SET_KEY){
                $Zuordnungen[] = array('SEH', $SET_KEY);
            }

            $DateiInfo = pathinfo($_FILES['DOC']['name']);
            $Erweiterung = strtolower(isset($DateiInfo['extension']) ? $DateiInfo['extension'] : '');


            $Dokument = $DOC->ErzeugeDokumentenPfad($_POST['txtDOC_BEZEICHNUNG'], null, date('c'), '', $Erweiterung, $Zuordnungen);

            if (move_uploaded_file($_FILES['DOC']['tmp_name'], $Dokument['pfad']) === false) {
                $Form->DebugAusgabe(1, $_FILES, $Dokument);
            } else {
                $Form->Hinweistext($AWISSprachKonserven['SEP']['SEP_HOCHGELADEN'], 1, '');
            }
        }
        elseif (isset($_POST['txtZUSATZ_SEH_SET_KEY'])){

            $DOC = new awisDokument($DB, $AWISBenutzer);

            $Zusatzverteiler = $_POST['txtZUSATZ_SEH_SET_KEY'];

            if ($_POST['txtZUSATZ_SEH_SET_KEY'] != null) { //Multiselect mit dem Verteiler

                $DoksAktuellerTeilnehmer = $DOC->DokumenteEinerZuordnung('SEH',$_POST['txtSEH_SET_KEY']);

                foreach ($Zusatzverteiler as $SET_KEY) {
                    foreach($DoksAktuellerTeilnehmer as $DOK){
                        $DOC->DokumentZuordnen($DOK['DOC_KEY'],'SEH',$SET_KEY);
                    }
                }

                $Form->Hinweistext($AWISSprachKonserven['SEP']['SEP_HOCHGELADEN'], 1, '');
            }
        }

        if(isset($_POST['txtEmailsenden']) or count($Form->NameInArray($_POST,'emailadresse_',awisFormular::NAMEINARRAY_LISTE_ARRAY,awisFormular::NAMEINARRAY_SUCHTYP_ANFANG))>0){
            foreach ($SpeicherSETs as $SET_KEY){

                // Wenn vorher ein Fehler auftreten ist, kann es sein,
                // dass bereits Emails korrekt versendet wurden.
                // Diese d�rfen nicht mehr versendet werden
                if(!isset($_POST['txtemail_ok_'.$SET_KEY])){

                    $HotelMail = new awisSeminarplanEmailHotel($SET_KEY);
                    //Schauen, ob ein anderer Empf�nger angegeben wurde:
                    $Empfaenger = '';
                    if(isset($_POST['txtemailadresse_'.$SET_KEY])){
                        $Empfaenger = $_POST['txtemailadresse_'.$SET_KEY];
                    }

                    if ($HotelMail->Verarbeitung($Empfaenger)) {
                        $Form->Erstelle_HiddenFeld('email_ok_'.$SET_KEY,true);
                    } else { //Fehler aufgetreten. Keine Mailadresse gefunden.
                        $SQL = "Select SET_VORNAME, SET_NACHNAME, SET_PER_NR, SET_FIL_NR ";
                        $SQL .= " from seminarteilnehmer ";
                        $SQL .= " where set_key = ".$DB->WertSetzen('SET','N0',$SET_KEY);

                        $rsSET = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SET'));

                        $HinweisText = '';
                        $HinweisText .= $TXT_Speichern['SEH']['SEH_FEHLERMAILINFOTEXT'];
                        $HinweisText = str_replace('#SEH_TEILNEHMER#',$rsSET->FeldInhalt('SET_VORNAME') . ' ' . $rsSET->FeldInhalt('SET_NACHNAME') .' ('. $rsSET->FeldInhalt('SET_PER_NR') .') ' . $rsSET->FeldInhalt('SET_FIL_NR'),$HinweisText);
                        ob_start();
                        $Form->ZeileStart();
                        $Form->Erstelle_TextFeld('emailadresse_'.$SET_KEY,'academy@de.atu.eu',20,200,true);
                        $Form->ZeileEnde();
                        $HinweisText .= ob_get_clean();
                        ob_start();
                        $Form->ZeileStart();
                        $Form->Hinweistext($HinweisText,awisFormular::HINWEISTEXT_FEHLER);
                        $Form->ZeileEnde();
                        $EmailMeldung .= ob_get_clean();
                    }
                }
            }
        }
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

function MergeSeminarinfo(awisDatenbank $DB, awisBenutzer $AWISBenutzer, $ITY_KEY, $XXX_KEY, $XTN_KUERZEL, $Wert, $Format='T'){

    if($Wert != ''){
        $SQL  ='merge into SEMINARPLANINFOS DEST';
        $SQL .=' using (';
        $SQL .='           select';
        $SQL .= $DB->WertSetzen('SEI','Z', $ITY_KEY) . ' as ITY_KEY,';
        $SQL .= $DB->WertSetzen('SEI','Z', $XXX_KEY) . ' as XXX_KEY,';
        $SQL .= $DB->WertSetzen('SEI','T', $XTN_KUERZEL) . ' as XTN_KUERZEL,';
        $SQL .= $DB->WertSetzen('SEI',$Format, $Wert) . '  as WERT,';
        $SQL .= $DB->WertSetzen('SEI','T', $AWISBenutzer->BenutzerName()) . ' as AWIS_USER,';
        $SQL .= '               SYSDATE as USERDAT';
        $SQL .='           from';
        $SQL .='               DUAL';
        $SQL .='       )';
        $SQL .=' SRC on ( SRC.XXX_KEY = DEST.SEI_XXX_KEY';
        $SQL .='          and SRC.XTN_KUERZEL = DEST.SEI_XTN_KUERZEL';
        $SQL .='          and SRC.ITY_KEY = DEST.SEI_ITY_KEY )';
        $SQL .=' when matched then update';
        $SQL .=' set DEST.SEI_WERT = SRC.WERT,';
        $SQL .='     DEST.SEI_USERDAT = SRC.USERDAT,';
        $SQL .='     DEST.SEI_USER = SRC.AWIS_USER';
        $SQL .=' when not matched then';
        $SQL .=' insert (DEST.SEI_XTN_KUERZEL,';
        $SQL .='     DEST.SEI_XXX_KEY,';
        $SQL .='     DEST.SEI_ITY_KEY,';
        $SQL .='     DEST.SEI_WERT,';
        $SQL .='     DEST.SEI_USER,';
        $SQL .='     DEST.SEI_USERDAT )';
        $SQL .=' values';
        $SQL .='     ( SRC.XTN_KUERZEL,';
        $SQL .='       SRC.XXX_KEY,';
        $SQL .='       SRC.ITY_KEY,';
        $SQL .='       SRC.WERT,';
        $SQL .='       SRC.AWIS_USER,';
        $SQL .='       SRC.USERDAT )';

    }else{
        $SQL = 'delete from SEMINARPLANINFOS WHERE';
        $SQL .= ' SEI_ITY_KEY = ' . $DB->WertSetzen('SEI','Z', $ITY_KEY);
        $SQL .= ' AND SEI_XXX_KEY = ' . $DB->WertSetzen('SEI','Z', $XXX_KEY);
        $SQL .= ' AND SEI_XTN_KUERZEl = ' .$DB->WertSetzen('SEI','T', $XTN_KUERZEL);
    }

    $DB->Ausfuehren($SQL,'',true, $DB->Bindevariablen('SEI'));
}

?>