<?php
global $AWISBenutzer;
global $AWIS_KEY1;

$TextKonserven = array();
$TextKonserven[]=array('CAD','%');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3700 = awisBenutzerRecht($con,3700,$AWISBenutzer->BenutzerName());
if($Recht3700==0)
{
    awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$SQL = 'SELECT CRMAdressen.*';
$SQL .= ' FROM CRMAdressen';
$SQL .= ' WHERE CAD_CAD_KEY=0'.$AWIS_KEY1;
$rsCAD = awisOpenRecordset($con,$SQL);
$rsCADZeilen=$awisRSZeilen;

if(isset($rsCAD['CAD_KEY'][0]))
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Verbund';
	$Link .= '&VSort=CAD_NAME1'.((isset($_GET['VSort']) AND ($_GET['VSort']=='CAD_NAME1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_NAME1'],450,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Verbund';
	$Link .= '&VSort=CAD_PLZ'.((isset($_GET['VSort']) AND ($_GET['VSort']=='CAD_PLZ'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_PLZ'],100,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Verbund';
	$Link .= '&VSort=CAD_ORT'.((isset($_GET['VSort']) AND ($_GET['VSort']=='CAD_ORT'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_ORT'],200,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Verbund';
	$Link .= '&VSort=CAD_VERBUNDHINWEIS'.((isset($_GET['VSort']) AND ($_GET['VSort']=='CAD_VERBUNDHINWEIS'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CAD']['CAD_VERBUNDHINWEIS'],400,'',$Link);
	awis_FORM_ZeileEnde();

	for($CADZeile=0;$CADZeile<$rsCADZeilen;$CADZeile++)
	{
		awis_FORM_ZeileStart();
		$Link = './crm_Main.php?cmdAktion=Details&CAD_KEY='.$rsCAD['CAD_KEY'][$CADZeile].'';
		awis_FORM_Erstelle_ListenFeld('CAD_NAME1',$rsCAD['CAD_NAME1'][$CADZeile].($rsCAD['CAD_NAME2'][$CADZeile]==''?'':' '.$rsCAD['CAD_NAME2'][$CADZeile]),0,450,false,($CADZeile%2),'',$Link);
		awis_FORM_Erstelle_ListenFeld('CAD_PLZ',$rsCAD['CAD_LAN_CODE'][$CADZeile].'-'.$rsCAD['CAD_PLZ'][$CADZeile],0,100,false,($CADZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('CAD_ORT',$rsCAD['CAD_ORT'][$CADZeile],0,200,false,($CADZeile%2),'','');
		awis_FORM_Erstelle_ListenFeld('CAD_VERBUNDHINWEIS',$rsCAD['CAD_VERBUNDHINWEIS'][$CADZeile],0,400,false,($CADZeile%2),'','');
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}
else 
{
	awis_FORM_FormularStart();	
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_KeineToechter'],600);
	awis_FORM_FormularEnde();
}
?>