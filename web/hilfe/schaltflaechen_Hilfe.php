<h3>Schaltfl�chen</h3>
<br>
In AWIS werden eine Reihe von Schaltfl�chen eingesetzt. Je nach dem, welche Rechte Sie in einem Modul besitzen
erscheinen mehr oder weniger Schaltfl�chen. F�r die Schaltfl�chen werden auch (fast) immer Tastenk�rzel unterst�tzt.
Um ein Tastenk�rzel zu ermitteln, muss die Maus auf eine Schaltfl�che gesetzt werden, Nach kurzer Zeit erscheint eine
kleine Informationen �ber die Funktion und ein Tastenk�rzel.<br>
<br>
Im Folgenden sind alle m�glichen Schaltfl�chen aufgef�hrt.
<br>
<br>

<table border=1 width=100%>

<tr>
<td id=FeldBez>Schaltfl�che</td>
<td id=FeldBez>Bezeichung</td>
<td id=FeldBez>Beschreibung</td>
</tr>


<tr>
<td><img src="../bilder/aendern.png"></td>
<td>Datensatz �ndern</td>
<td>Em�glicht eine �nderung des angezeigten Datensatzes.</td>
</tr>

<tr>
<td><img src="../bilder/dateioeffnen.png"></td>
<td>Datei �ffnen</td>
<td>�ffnet eine vorhandene Datei. Dateien werden vor allem f�r die Exporte verwendet.</td>
</tr>


<tr>
<td><img src="../bilder/diskette.png"></td>
<td>Speichern</td>
<td>Speichert eine Information in einer Maske ab.</td>
</tr>

<tr>
<td><img src="../bilder/eingabe_ok.png"></td>
<td>Eingabe best�tigen</td>
<td>Best�tigt die Eingabe in Suchmasken und l�st eine Aktion aus.</td>
</tr>

<tr>
<td><img src="../bilder/hilfe.png"></td>
<td>Hilfe</td>
<td>�ffnet eine Hilfeseite in einem separaten Fenster.</td>
</tr>

<tr>
<td><img src="../bilder/kopieren.png"></td>
<td>Kopieren</td>
<td>Duplizieren von Datens�tzen.</td>
</tr>

<tr>
<td><img src="../bilder/Muelleimer_gross.png"></td>
<td>L�schen</td>
<td>L�scht einen Datensatz.</td>
</tr>

<tr>
<td><img src="../bilder/pdf.png"></td>
<td>PDF �ffnen</td>
<td>�ffnet eine PDF Datei.</td>
</tr>

<tr>
<td><img src="../bilder/plus.png"></td>
<td>Hinzuf�gen</td>
<td>Hinzuf�gen eines neuen Datensatzes.</td>
</tr>

<tr>
<td><img src="../bilder/NeueListe.png"></td>
<td>Listenanzeige</td>
<td>Zeigt die vorhergehende Liste an.</td>
</tr>

<tr>
<td><img src="../bilder/radierer.png"></td>
<td>Reset</td>
<td>L�scht die Felder in den Suchmasken.</td>
</tr>

<tr>
<td><img src="../bilder/sortierien.png"></td>
<td>Sortieren</td>
<td>Sortiert die Daten aufsteigend oder absteigend.</td>
</tr>

<tr>
<td><img src="../bilder/suche.png"></td>
<td>Suche</td>
<td>Zeigt Detailinformationen in einem eigenen Modul an.</td>
</tr>

<tr>
<td><img src="../bilder/zurueck.png"></td>
<td>Zur�ck</td>
<td>Geht zum vorherigen Men� zur�ck.</td>
</tr>

</table>
