<?php

class BestaetigungPosition extends Serialize
{

    /**
     * @var int $PositionsNr
     */
    protected $PositionsNr = null;

    /**
     * @var int $EinspeiserNr
     */
    protected $EinspeiserNr = null;

    /**
     * @var string $EinspeiserArtikelNr
     */
    protected $EinspeiserArtikelNr = null;

    /**
     * @var float $BestellMenge
     */
    protected $BestellMenge = null;

    /**
     * @var boolean $BestellPositionAngelegt
     */
    protected $BestellPositionAngelegt = null;

    /**
     * @var string $Feedback
     */
    protected $Feedback = null;

    /**
     * @param int $PositionsNr
     * @param int $EinspeiserNr
     * @param float $BestellMenge
     * @param boolean $BestellPositionAngelegt
     */
    public function __construct($PositionsNr, $EinspeiserNr, $BestellMenge, $BestellPositionAngelegt)
    {
      $this->PositionsNr = $PositionsNr;
      $this->EinspeiserNr = $EinspeiserNr;
      $this->BestellMenge = $BestellMenge;
      $this->BestellPositionAngelegt = $BestellPositionAngelegt;
    }

    /**
     * @return int
     */
    public function getPositionsNr()
    {
      return $this->PositionsNr;
    }

    /**
     * @param int $PositionsNr
     * @return BestaetigungPosition
     */
    public function setPositionsNr($PositionsNr)
    {
      $this->PositionsNr = $PositionsNr;
      return $this;
    }

    /**
     * @return int
     */
    public function getEinspeiserNr()
    {
      return $this->EinspeiserNr;
    }

    /**
     * @param int $EinspeiserNr
     * @return BestaetigungPosition
     */
    public function setEinspeiserNr($EinspeiserNr)
    {
      $this->EinspeiserNr = $EinspeiserNr;
      return $this;
    }

    /**
     * @return string
     */
    public function getEinspeiserArtikelNr()
    {
      return $this->EinspeiserArtikelNr;
    }

    /**
     * @param string $EinspeiserArtikelNr
     * @return BestaetigungPosition
     */
    public function setEinspeiserArtikelNr($EinspeiserArtikelNr)
    {
      $this->EinspeiserArtikelNr = $EinspeiserArtikelNr;
      return $this;
    }

    /**
     * @return float
     */
    public function getBestellMenge()
    {
      return $this->BestellMenge;
    }

    /**
     * @param float $BestellMenge
     * @return BestaetigungPosition
     */
    public function setBestellMenge($BestellMenge)
    {
      $this->BestellMenge = $BestellMenge;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBestellPositionAngelegt()
    {
      return $this->BestellPositionAngelegt;
    }

    /**
     * @param boolean $BestellPositionAngelegt
     * @return BestaetigungPosition
     */
    public function setBestellPositionAngelegt($BestellPositionAngelegt)
    {
      $this->BestellPositionAngelegt = $BestellPositionAngelegt;
      return $this;
    }

    /**
     * @return string
     */
    public function getFeedback()
    {
      return $this->Feedback;
    }

    /**
     * @param string $Feedback
     * @return BestaetigungPosition
     */
    public function setFeedback($Feedback)
    {
      $this->Feedback = $Feedback;
      return $this;
    }

}
