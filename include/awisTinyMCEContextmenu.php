<?php

/**
 * Klasse zum erezugen des TinyMCE-Contextmenu (Rechtsklick)
 */
class awisTinyMCEContextmenu
{
    /**
     * String f�r das Contextmenu
     *
     * @var String
     */
    private $_Contextmenu = "";

    /**
     * awisTinyMCEContextmenu constructor
     */
    function __construct()
    {
        $this->_Contextmenu = "";
    }

    /**
     * Rechtsklickmenu mit Verlinkung
     */
    public function MitRechtsklickMenu()
    {
        $this->_Contextmenu = 'link';
    }

    /**
     * Liefert das Toolbararray
     *
     * @return String
     */
    public function getContextmenu()
    {
        return $this->_Contextmenu;
    }
}