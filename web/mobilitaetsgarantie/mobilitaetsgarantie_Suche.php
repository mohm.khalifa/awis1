<?php
require_once 'mobilitaetsgarantie_funktionen.inc';

try
{
    $MOB = new mobilitaetsgarantie_funktionen();
    $Speichern = isset($MOB->Param['SPEICHERN']) && $MOB->Param['SPEICHERN'] == 'on'?true:false;

    $MOB->RechteMeldung(0);//Anzeigen Recht
	echo "<form name=frmSuche method=post action=./mobilitaetsgarantie_Main.php?cmdAktion=Details>";

	$MOB->Form->Formular_Start();
    
	$MOB->Form->ZeileStart();
	$MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_NAME'].':',190);
	$MOB->Form->Erstelle_TextFeld('*MOB_NAME',($Speichern?$MOB->Param['MOB_NAME']:''),10,110,true);
	$MOB->AWISCursorPosition='sucMOB_NAME';
	$MOB->Form->ZeileEnde();


    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_VERS_NUMMER'].':',190);
    $MOB->Form->Erstelle_TextFeld('*MOB_VERS_NUMMER',($Speichern?$MOB->Param['MOB_VERS_NUMMER']:''),10,110,true);
    $MOB->Form->ZeileEnde();


    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_DATUM_VON'].':',190);
    $MOB->Form->Erstelle_TextFeld('*MOB_DATUM_VON',($Speichern?$MOB->Param['MOB_DATUM_VON']:''),10,150,true,'','','','D');
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_DATUM_BIS'].':',100);
    $MOB->Form->Erstelle_TextFeld('*MOB_DATUM_BIS',($Speichern?$MOB->Param['MOB_DATUM_BIS']:''),10,150,true,'','','','D');
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_FIL_ID'].':',190);
    $MOB->Form->Erstelle_TextFeld('*MOB_FIL_ID',($Speichern?$MOB->Param['MOB_FIL_ID']:''),10,110,true);
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_VORGANGSNUMMER'].':',190);
    $MOB->Form->Erstelle_TextFeld('*MOB_VORGANGSNUMMER',($Speichern?$MOB->Param['MOB_VORGANGSNUMMER']:''),10,110,true);
    $MOB->Form->ZeileEnde();

    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['MOB']['MOB_KFZ_KENNZEICHEN'].':',190);
    $MOB->Form->Erstelle_TextFeld('*MOB_KFZ_KENNZEICHEN',($Speichern?$MOB->Param['MOB_KFZ_KENNZEICHEN']:''),10,110,true);
    $MOB->Form->ZeileEnde();


    // Auswahl kann gespeichert werden
    $MOB->Form->ZeileStart();
    $MOB->Form->Erstelle_TextLabel($MOB->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $MOB->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $MOB->Form->ZeileEnde();

	$MOB->Form->Formular_Ende();

	$MOB->Form->SchaltflaechenStart();
	$MOB->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$MOB->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$MOB->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $MOB->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($MOB->Recht56000&4) == 4){
		$MOB->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $MOB->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$MOB->Form->SchaltflaechenEnde();

	$MOB->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
	if($MOB->Form instanceof awisFormular)
	{
		$MOB->Form->DebugAusgabe(1, $ex->getSQL());
		$MOB->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$MOB->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($MOB->Form instanceof awisFormular)
	{
		$MOB->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>