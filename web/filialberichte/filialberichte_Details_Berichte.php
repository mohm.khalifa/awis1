<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PBM','%');
	$TextKonserven[]=array('PEI','lst_PEI_PEZ_STATUS');
	$TextKonserven[]=array('PEI','PEI_PEZ_STATUS');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Zugriffsrechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('CV','%');
	$TextKonserven[]=array('MQC','%');
	$TextKonserven[]=array('CBT','%');
	$TextKonserven[]=array('CBG','%');
	$TextKonserven[]=array('IBT','%');
	$TextKonserven[]=array('IBG','%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht16000 = $AWISBenutzer->HatDasRecht(16000);
	if($Recht16000==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	$SQL = 'SELECT FLB_PEZ_STATUS FROM FILIALBERICHTE WHERE FLB_KEY = 0'.$AWIS_KEY1;
	$rsPEI = $DB->RecordSetOeffnen($SQL);
	
	$EditRecht=(($Recht16000&2)!=0);
	
	if ($EditRecht==true)
	{
		if ($rsPEI->FeldInhalt('FLB_PEZ_STATUS')=='1')
		{
			$EditRecht = false;
		}
	}	
	
	$SQL = 'SELECT *';
	$SQL .= ' FROM Filialberichte';
	$SQL .= ' INNER JOIN Personaleinsberichte on PBE_KEY = FLB_PBE_KEY';
	$SQL .= ' INNER JOIN Personaleinsberichtsfelder ON PBF_PBE_KEY = PBE_KEY';
	//$SQL .= ' LEFT OUTER JOIN Personaleinsberichtszuord ON PEZ_PBF_ID = PBF_ID AND PEZ_PEI_KEY='.$AWIS_KEY1;
	$SQL .= ' WHERE FLB_KEY = 0'.$AWIS_KEY1 .' AND PBF_STATUS=\'A\' AND PBF_PBF_ID=0';
	$SQL .= ' ORDER BY PBF_SORTIERUNG';
	
	$rsPBF = $DB->RecordSetOeffnen($SQL);
    $Form->DebugAusgabe(1,$SQL);
    
	$i = 1;
	while (!$rsPBF->EOF())
	{		
		$SQL = 'SELECT *';
		$SQL .= ' FROM Filialberichte';
		$SQL .= ' INNER JOIN Personaleinsberichte on PBE_KEY = FLB_PBE_KEY';
		$SQL .= ' INNER JOIN Personaleinsberichtsfelder ON PBF_PBE_KEY = PBE_KEY AND PBF_ID <> 46 AND PBF_ID <> 54';		
		$SQL .= ' LEFT OUTER JOIN Personaleinsberichtszuord ON PEZ_PBF_ID = PBF_ID AND PEZ_XXX_KEY='.$AWIS_KEY1;
		$SQL .= ' AND PEZ_XTN_KUERZEL=\'FLB\'';
		$SQL .= ' WHERE FLB_KEY = 0'.$AWIS_KEY1 .' AND PBF_STATUS=\'A\' AND (PBF_ID = '.$rsPBF->FeldInhalt('PBF_ID').' OR PBF_PBF_ID = '.$rsPBF->FeldInhalt('PBF_ID').')';
		$SQL .= ' ORDER BY PBF_SORTIERUNG';

		$rsPEZ = $DB->RecordSetOeffnen($SQL);

		while (!$rsPEZ->EOF())
		{								
			$Farbe = '';
			if ($rsPEZ->FeldInhalt('PEZ_WERT')=='0')
			{
				$Farbe = 'color:#FF0000';
			}
			
			
			if($rsPEZ->FeldInhalt('PBF_PBF_ID') == '0' AND $i <> 1)
			{
				$Form->Trennzeile();
			}
					
			//$Form->ZeileStart();
			$Form->ZeileStart('font-size:10pt');
			//echo '<input type=hidden name=txtPEZ_KEY value=0'.$rsPEZ->FeldInhalt('PEZ_KEY').'>';			
	
			//Überschrift
			if($rsPEZ->FeldInhalt('PBF_SPALTEN')=='0')
			{
				$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven[$rsPEZ->FeldInhalt('PBE_TEXTKONSERVE')][$rsPEZ->FeldInhalt('PBF_BEZEICHNUNG')].':',400);
			}
			else 
			{
				$Form->Erstelle_TextLabel($AWISSprachKonserven[$rsPEZ->FeldInhalt('PBE_TEXTKONSERVE')][$rsPEZ->FeldInhalt('PBF_BEZEICHNUNG')].':',400,$Farbe);
			}
						
			//Feld Wert anzeigen
			if(($rsPEZ->FeldInhalt('PBF_SPALTEN')&1)==1)
			{
				if($rsPEZ->FeldInhalt('PBF_DATENQUELLE')!='')
				{		
					switch(substr($rsPEZ->FeldInhalt('PBF_DATENQUELLE'),0,3))
					{
						case 'TXT':
							$Felder = explode(':',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							$Daten = $Form->LadeTexte(array(array($Felder[1],$Felder[2])), $AWISBenutzer->BenutzerSprache());
							$Daten = explode('|',$Daten[$Felder[1]][$Felder[2]]);
							$Form->Erstelle_SelectFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
							//var_dump($rsPEZ->FeldInhalt('PEZ_WERT'));
							break;
						case 'SQL':
							$Felder = explode(':',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							$Form->Erstelle_SelectFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,$Felder[1],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','');
							break;					
						default:
							$Form->Erstelle_TextFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_ZEICHEN'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','','',$rsPEZ->FeldInhalt('PBF_FORMAT'),'','',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));
							break;			
					}		
			
				}
				elseif(strpos($rsPEZ->FeldInhalt('PBF_ZEICHEN'),',')===false)
				{
					$Form->Erstelle_TextFeld('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_ZEICHEN'),$rsPEZ->FeldInhalt('PBF_BREITE'),$EditRecht,'','','',$rsPEZ->FeldInhalt('PBF_FORMAT'),'','',$rsPEZ->FeldInhalt('PBF_DATENQUELLE'));					
				}
				else
				{
					$Groesse = explode(',',$rsPEZ->FeldInhalt('PBF_ZEICHEN'));
					$Form->Erstelle_Textarea('PEZ_WERT_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_WERT'),$rsPEZ->FeldInhalt('PBF_BREITE'),$Groesse[0],$Groesse[1],$EditRecht);
				}				
			}
			
			//Feld Bemerkung anzeigen			
			if(($rsPEZ->FeldInhalt('PBF_SPALTEN')&2)==2)
			{
				if ($rsPEZ->FeldInhalt('PBF_BEMERKUNG') != '')
				{				
					$Form->Erstelle_TextLabel($rsPEZ->FeldInhalt('PBF_BEMERKUNG'),80,''); //$AWISSprachKonserven[$rsPEZ->FeldInhalt('PBE_TEXTKONSERVE')][$rsPEZ->FeldInhalt('PBF_BEZEICHNUNG')].':',400);
					$Form->Erstelle_TextFeld('PEZ_BEMERKUNG_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_BEMERKUNG'),5,20,$EditRecht,'','','','N2','','','',5);							
				}
				else
				{
					if ($rsPEZ->FeldInhalt('PEZ_WERT')=='0' or (($rsPEZ->FeldInhalt('PBF_SPALTEN')&1)!=1 AND ($rsPEZ->FeldInhalt('PBF_SPALTEN')&2)==2))
					{
						$Form->Erstelle_TextFeld('PEZ_BEMERKUNG_'.$rsPEZ->FeldInhalt('PBF_ID').'_'.$rsPEZ->FeldInhalt('PEZ_KEY'),$rsPEZ->FeldInhalt('PEZ_BEMERKUNG'),20,40,$EditRecht,'','','','T','','','',70);							
					}										
				}
			}					
			
			$Form->ZeileEnde();
			
			$i++;
			$rsPEZ->DSWeiter();
		}
	$rsPBF->DSWeiter();	
	}		
	
	$Form->Trennzeile();
	//$Form->ZeileStart();
	$Form->ZeileStart('font-size:10pt');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PEI']['PEI_PEZ_STATUS'].':',200);	
	$KateogrieAlt = explode("|",$AWISSprachKonserven['PEI']['lst_PEI_PEZ_STATUS']);
	$Form->Erstelle_SelectFeld('FLB_PEZ_STATUS',$rsPEI->FeldInhalt('FLB_PEZ_STATUS'),200,$EditRecht,'','','','','',$KateogrieAlt,'');						
	$Form->ZeileEnde();		
	
	$Form->Formular_Ende();	
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('PROBLEM',$ex->getMessage(),'MELDEN',3,1102031407);
}
catch (Exception $ex)
{
	echo 'allg. Fehler:'.$ex->getMessage();
}
?>