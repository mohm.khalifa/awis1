<html>
<head>
<link rel="SHORTCUT ICON" href="../favicon.ico">
<title>Awis - ATU webbasierendes Informationssystem</title>
</head>

<body>
<?php
require_once("db.inc.php");		// DB-Befehle
require_once("register.inc.php");
require_once("sicherheit.inc.php");
global $AWISBenutzer;
								// Benutzerdefinierte CSS Datei
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();

        // Beginn Hauptmen�
print "<table border=0 width=100%><tr><td><h1 id=SeitenTitel>Auswertungen</h1></td><td align=right>Anmeldename: " . $AWISBenutzer->BenutzerName() . "</td></tr></table>";

        // �berschrift der Tabelle
print "<table border=0 width=100%><tr><th align=left id=FeldBez>Aktion</th><th align=left id=FeldBez>Beschreibung</th></tr>";

if(awisBenutzerRecht($con,502)>0)
{
	print "<tr><td><a accesskey=S href=./absatzSQL_Main.php>Absatz <u>S</u>QL</a></td><td>SQL Jobs f�r Absatzauswertungen (OSUMME).</td></tr>";
}

if(awisBenutzerRecht($con,200)>0)
{
    print "<tr><td><a accesskey=a href=../reifen/absatz_main.php?cmdAktion=Suchen><u>A</u>bsatzinfo</a></td><td>Informationen f�r Reifenabs�tze anzeigen.</td></tr>";
}

if(awisBenutzerRecht($con,505)>0)
{
	print "<tr><td><a accesskey=o href=./artnr_ohne_oenr_Main.php>Artnr-<u>O</u>enr</a></td><td>Artikelnummern der WG 03 ohne zugeordnete OE-Nummer.</td></tr>";
}

if(awisBenutzerRecht($con,500)>0)
{
	print "<tr><td><a accesskey=F href=./filialen_auswertung_Main.php><u>F</u>ilialen</a></td><td>Auswertungen und Exporte f�r Filialdaten.</td></tr>";
	print "<tr><td><a href=./nachbarfilialen.php>Nachbarfilialen</a></td><td>Exporte der Nachbarfilialen.</td></tr>";
}

if(awisBenutzerRecht($con,501)>0)
{
	print "<tr><td><a accesskey=F href=./fremdkauf_Main.php><u>F</u>remdkauf</a></td><td>Fremdk�ufe f�r Artikel.</td></tr>";
	print "<tr><td><a accesskey=F href=./fremdkauf/fremdkauf_Main.php><u>F</u>remdkauf Neu</a></td><td>Fremdk�ufe f�r Artikel.</td></tr>";
}

if(awisBenutzerRecht($con,507)>0)
{
	print "<tr><td><a accesskey=F href=./lieferkontrolle_Main.php><u>L</u>ieferkontrolle</a></td><td>Lieferkontrolle in den Filialen.</td></tr>";
}

if(awisBenutzerRecht($con,540)>0)
{
	print "<tr><td><a accesskey=N href=./nachgefrArt_Main.php><u>N</u>achgefragte Artikel</a></td><td>Anzeige der nachgefragte Artikel.</td></tr>";
}

if(awisBenutzerRecht($con,506)>0)
{
	print "<tr><td><a accesskey=M href=./oenummernimport.php>Oenu<u>m</u>mernimport</a></td><td>Statusmeldungen der OENummernimporte.</td></tr>";
}

if((awisBenutzerRecht($con,150) & 16)==16)
{
	print "<tr><td><a accesskey=T href=./telefon_Main.php><u>T</u>elefonlisten</a></td><td>Telefonlisten ausdrucken.</td></tr>";
}

if(awisBenutzerRecht($con,407)>0)
{
	print "<tr><td><a accesskey=P href=./artikelpruefungen_Main.php>Artikel<u>p</u>r�fungen</a></td><td>Datenexport der Artikelpr�fungen.</td></tr>";
}


print "</table>";	//Ende Hauptmen�

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";

awisLogoff($con);

?>

</body>
</html>

