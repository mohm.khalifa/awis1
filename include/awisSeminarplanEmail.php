<?php
require_once 'awisMailer.inc';

/**
 * Created by PhpStorm.
 * User: patrickgebhardt
 * Date: 10.07.17
 * Time: 17:10
 */
class awisSeminarplanEmail
{
    /**
     * aktueller Teilneherkey
     *
     * @var int
     */
    protected $_SET_KEY;

    /**
     * Aktuelle Filialnummer
     *
     * @var string
     */
    protected $_FIL_ID;

    /**
     * Betreff er Email
     *
     * @var string
     */
    protected $_Betreff;

    /**
     * Text der Email
     *
     * @var string
     */
    protected $_Text;

    /**
     * @var awisRecordset
     */
    protected $_rsSET;

    /**
     * @var awisDatenbank
     */
    protected $_DB;

    /**
     * @var awisMailer
     */
    protected $_Mailer;

    /**
     * @var awisBenutzer
     */
    protected $_Benutzer;

    /**
     * @var awisFormular
     */
    protected $_Form;

    /**
     * @var awisWerkzeuge
     */
    protected $_Werkzeuge;

    /**
     * @var string
     */
    private $_Empfaenger;

    /**
     * AwisSprachKonserven
     */
    private $_AWISSprachKonserven;

    /**
     * awisSeminarplanEmail constructor.
     * @param $SET_KEY
     */
    function __construct($SET_KEY)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Benutzer = awisBenutzer::Init();
        $this->_Mailer = new awisMailer($this->_DB, $this->_Benutzer);
        $this->_Mailer->SetzeBezug('SET', $SET_KEY);
        $this->_Mailer->SetzeVersandPrioritaet(10);
        $this->_Mailer->AnhaengeLoeschen();
        $this->_Form = new awisFormular();
        $this->_Werkzeuge = new awisWerkzeuge();
        $TextKonserven = array();
        $TextKonserven[]=array('SEH','mail_%');
        $this->_AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven);

        $SQL = 'SELECT';
        $SQL .= '   lpad(SET_FIL_NR,4,\'0\') AS FILIALE,';
        $SQL .= '   SEP_KEY, SPA_LOKATIONSNAME,';
        $SQL .= '   SPA_POSTLEITZAHL, SPA_ORT, SPA_STRASSE,';
        $SQL .= '   SEP_SEMINAR, SRG_REGION, ';
        $SQL .= '   SET_PER_NR, ';
        $SQL .= '   SRG_KEY,';
        $SQL .= '   SEP_BESCHREIBUNG,';
        $SQL .= '   SEP_TRAINING,';
        $SQL .= '   SET_KEY,';
        $SQL .= '   SET_NACHNAME,';
        $SQL .= '   SET_VORNAME,';
        $SQL .= '   SEP_SEMINARSTATUS,';
        $SQL .= '   SET_STATUSEMAIL,';
        $SQL .= '   SET_TEILNAHME,';
        $SQL .= '   SET_HOTEL_VON, SET_HOTEL_BIS,';
        $SQL .= '   SET_TEILNEHMERVORSCHLAG,';
        $SQL .= '   SER_RAUM,';
        $SQL .= '   SEP_VON,';
        $SQL .= '   SEP_BIS, ';
        $SQL .= '   SET_PER_NR,';
        $SQL .= '   SEH_HOMEPAGE,';
        $SQL .= '   (SELECT lower(LAENDER.LAN_CODE) AS LAN_CODE FROM AWIS.FILIALEN';
        $SQL .= '       INNER JOIN AWIS.LAENDER ON FIL_LAN_WWSKENN = LAN_WWSKENN';
        $SQL .= '       WHERE FIL_ID = SET_FIL_NR) AS MAILLAND';
        $SQL .= ' FROM';
        $SQL .= '   AWIS.SEMINARPLAN';
        $SQL .= ' INNER JOIN SEMINARTEILNEHMER';
        $SQL .= ' ON';
        $SQL .= '   SEP_KEY = SET_SEP_KEY';
        $SQL .= ' INNER JOIN (';
        $SQL .= '   SELECT r.SER_RAUM AS SER_RAUM, p.SEP_KEY AS SER_SEP_KEY';
        $SQL .= '       FROM SEMINARRAEUME r INNER JOIN';
        $SQL .= '       SEMINARPLAN p ON r.SER_KEY = p.SEP_SER_KEY ';
        $SQL .= '   UNION';
        $SQL .= '       SELECT SEI_WERT AS SER_RAUM, SEI_XXX_KEY AS SER_SEP_KEY';
        $SQL .= '       FROM SEMINARPLANINFOS';
        $SQL .= '       WHERE SEI_ITY_KEY = 604';
        $SQL .= ' )';
        $SQL .= '   ON SER_SEP_KEY = SEP_KEY';
        $SQL .= ' INNER JOIN ( ';
        $SQL .= "   SELECT SEP_KEY AS SPA_SEP_KEY, ";
        $SQL .= "   CASE";
        $SQL .= "       WHEN SEP_SRG_KEY = 51 AND SPA_LOKATIONSNAME IS NULL THEN 'ATU-Zentrale'";
        $SQL .= "       ELSE SPA_LOKATIONSNAME";
        $SQL .= "   END AS SPA_LOKATIONSNAME,";
        $SQL .= "   CASE";
        $SQL .= "       WHEN SEP_SRG_KEY = 51 AND SPA_POSTLEITZAHL IS NULL THEN '92637'";
        $SQL .= "       ELSE SPA_POSTLEITZAHL";
        $SQL .= "   END AS SPA_POSTLEITZAHL,";
        $SQL .= "   CASE";
        $SQL .= "       WHEN SEP_SRG_KEY = 51 AND SPA_ORT IS NULL THEN 'Weiden'";
        $SQL .= "       ELSE SPA_ORT";
        $SQL .= "   END AS SPA_ORT,";
        $SQL .= "   CASE";
        $SQL .= "       WHEN SEP_SRG_KEY = 51 AND SPA_STRASSE IS NULL THEN 'Dr. Kilian Strasse 11'";
        $SQL .= "       ELSE SPA_STRASSE";
        $SQL .= "   END AS SPA_STRASSE";
        $SQL .= "   FROM SEMINARRAUMADRESSEN";
        $SQL .= "   RIGHT JOIN SEMINARRAEUME";
        $SQL .= "       ON SER_RAUMNR = SPA_KEY";
        $SQL .= "   RIGHT JOIN SEMINARPLAN";
        $SQL .= "       ON SEP_SER_KEY = SER_KEY";
        $SQL .= "   WHERE SEP_SRG_KEY <> -99";
        $SQL .= '    UNION ';
        $SQL .= '    SELECT SEP_KEY AS SPA_SEP_KEY, ';
        $SQL .= '       (SELECT SEI_WERT FROM SEMINARPLANINFOS WHERE SEI_ITY_KEY = 600 ';
        $SQL .= '           AND SEI_XTN_KUERZEL = \'SEP\' AND SEI_XXX_KEY = SEP_KEY) AS SPA_LOKATIONSNAME, ';
        $SQL .= '       (SELECT SEI_WERT FROM SEMINARPLANINFOS WHERE SEI_ITY_KEY = 601 ';
        $SQL .= '           AND SEI_XTN_KUERZEL = \'SEP\' AND SEI_XXX_KEY = SEP_KEY) AS SPA_POSTLEITZAHL, ';
        $SQL .= '       (SELECT SEI_WERT FROM SEMINARPLANINFOS WHERE SEI_ITY_KEY = 602 ';
        $SQL .= '           AND SEI_XTN_KUERZEL = \'SEP\' AND SEI_XXX_KEY = SEP_KEY) AS SPA_ORT, ';
        $SQL .= '       (SELECT SEI_WERT FROM SEMINARPLANINFOS WHERE SEI_ITY_KEY = 603 ';
        $SQL .= '           AND SEI_XTN_KUERZEL = \'SEP\' AND SEI_XXX_KEY = SEP_KEY) AS SPA_STRASSE ';
        $SQL .= '       FROM SEMINARPLAN ';
        $SQL .= '       LEFT JOIN SEMINARTEILNEHMER ';
        $SQL .= '       ON SET_SEP_KEY = SEP_KEY ';
        $SQL .= '       WHERE SET_KEY = '. $this->_DB->WertSetzen('SET', 'N0', $SET_KEY);
        $SQL .= '       AND SEP_SRG_KEY = -99 ';
        $SQL .= '  ) ';
        $SQL .= '   ON SEP_KEY = SPA_SEP_KEY ';
        $SQL .= ' LEFT JOIN (';
        $SQL .= '   SELECT SRG_REGION, SEP_KEY AS SRG_SEP_KEY, SRG_KEY ';
        $SQL .= '       FROM SEMINARREGIONEN ';
        $SQL .= '       LEFT JOIN SEMINARPLAN';
        $SQL .= '           ON SRG_KEY = SEP_SRG_KEY';
        $SQL .= '       WHERE SRG_STATUS <> 3';
        $SQL .= '   UNION';
        $SQL .= '   SELECT SEI_WERT AS SRG_REGION, SEP_KEY AS SRG_SEP_KEY, SEP_SRG_KEY AS SRG_KEY';
        $SQL .= '       FROM SEMINARPLANINFOS';
        $SQL .= '       LEFT JOIN SEMINARPLAN';
        $SQL .= '           ON SEI_XXX_KEY = SEP_KEY';
        $SQL .= '       WHERE SEI_ITY_KEY = 600';
        $SQL .= '       AND SEI_XTN_KUERZEL = \'SEP\'';
        $SQL .= ' ) ';
        $SQL .= '   ON SRG_SEP_KEY = SEP_KEY';
        $SQL .= ' LEFT JOIN SEMINARHOTELZUORD';
        $SQL .= '   ON SEMINARTEILNEHMER.SET_KEY = SEMINARHOTELZUORD.SHZ_SET_KEY';
        $SQL .= ' LEFT JOIN SEMINARHOTEL';
        $SQL .= '   ON SEMINARHOTELZUORD.SHZ_SEH_KEY = SEMINARHOTEL.SEH_KEY';
        $SQL .= ' WHERE';
        $SQL .= '  SET_KEY = ' . $this->_DB->WertSetzen('SET', 'N0', $SET_KEY);

        $this->_rsSET = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('SET'));

        $this->_SET_KEY = $SET_KEY;
        $this->_FIL_ID = $this->_rsSET->FeldInhalt('FILIALE');
    }

    /**
     * Pr�ft die Emailadresse
     *
     * @param $Adresse
     * @return bool|int
     */
    public function checkEmail($Adresse)
    {
        return $this->_Mailer->checkEmail($Adresse, awisMailer::PRUEFE_LOGIK);
    }

    /*
     * Setzt den Text. Platzhalter werden automatisch ersetzt
     */
    public function Text($Text)
    {
        $this->_Text = $this->ErsetzePlatzhalter($Text);
        return $this->_Text;
    }

    /**
     * Setzt den Betreff. Platzhalter werden automatisch ersetzt
     *
     * @param $Betreff
     * @return mixed
     */
    public function Betreff($Betreff)
    {
        $this->_Betreff = $this->ErsetzePlatzhalter($Betreff);
        return $this->_Betreff;
    }

    /**
     * L�st bekannte Platzhalter in einem String auf
     *
     * @param $StringMitPlatzhalter
     * @return mixed
     */
    public function ErsetzePlatzhalter($StringMitPlatzhalter)
    {
        $Text =& $StringMitPlatzhalter;

        $Text = str_replace('#Terminnummer#', $this->_rsSET->FeldInhalt('SEP_TRAINING'), $Text);
        $Text = str_replace('#Seminartitel#', $this->_rsSET->FeldInhalt('SEP_BESCHREIBUNG'), $Text);
        $Text = str_replace('#Vorname Nachname#', $this->_rsSET->FeldInhalt('SET_VORNAME') . ' ' . $this->_rsSET->FeldInhalt('SET_NACHNAME'), $Text);
        $Text = str_replace('#Seminarnummer#', $this->_rsSET->FeldInhalt('SEP_SEMINAR'), $Text);
        $Text = str_replace('#Personalnummer#', $this->_rsSET->FeldInhalt('SET_PER_NR'), $Text);
        $Text = str_replace('#Filialnummer#', $this->_FIL_ID, $Text);
        $Text = str_replace('#Datumvon#', $this->_Form->Format('DU', $this->_rsSET->FeldInhalt('SEP_VON')), $Text);
        $Text = str_replace('#Datumbis#', $this->_Form->Format('DU', $this->_rsSET->FeldInhalt('SEP_BIS')), $Text);
        $KW = date('W', strtotime($this->_rsSET->FeldInhalt('SEP_VON')));
        $Text = str_replace('#KW#', $KW, $Text);
        $Text = str_replace('#Schulungsort#', $this->_rsSET->FeldInhalt('SER_RAUM'), $Text);
        $Text = str_replace('#Lokationsname#', $this->_rsSET->FeldInhalt('SPA_LOKATIONSNAME'), $Text);
        $Text = str_replace('#Region#', $this->_rsSET->FeldInhalt('SRG_REGION'), $Text);
        $Text = str_replace('#SET_KEY#', $this->_rsSET->FeldInhalt('SET_KEY'), $Text);
        $Text = str_replace('#Strasse#', $this->_rsSET->FeldInhalt('SPA_STRASSE'), $Text);
        $Text = str_replace('#Ort#', $this->_rsSET->FeldInhalt('SPA_ORT'), $Text);
        $Text = str_replace('#PLZ#', $this->_rsSET->FeldInhalt('SPA_POSTLEITZAHL'), $Text);
        $Text = str_replace('#TauschText#', $this->_rsSET->FeldInhalt('SET_TEILNEHMERVORSCHLAG'), $Text);
        $Text = str_replace('#HOTELHomepage#', $this->_rsSET->FeldInhalt('SEH_HOMEPAGE'), $Text);

        $Text = str_replace('#HOTELDatumvon#', $this->_Form->Format('D', $this->_rsSET->FeldInhalt('SET_HOTEL_VON')), $Text);
        $Text = str_replace('#HOTELDatumbis#', $this->_Form->Format('D', $this->_rsSET->FeldInhalt('SET_HOTEL_BIS')), $Text);

        if(isset($_POST['txtSET_ANAB_KEY'])) { //Nur Werte aus der Maske
            $Text = str_replace('#HOTELDatumvonALT#', $this->_Form->Format('D', (isset($_POST['oldSET_ANREISE'])?$_POST['oldSET_ANREISE']:"")), $Text);
            $Text = str_replace('#HOTELDatumbisALT#', $this->_Form->Format('D', (isset($_POST['oldSET_ABREISE'])?$_POST['oldSET_ABREISE']:"")), $Text);
            $KW = date('W', strtotime($this->_rsSET->FeldInhalt('SET_HOTEL_VON')));
            $Text = str_replace('#KWHotel#', $KW, $Text);
            $KW = date('W', isset($_POST['oldSET_ANREISE'])?strtotime($_POST['oldSET_ANREISE']):"");
            $Text = str_replace('#KWHotelALT#', $KW, $Text);
        }

        if(strpos($Text,'#LINKAFB#')!==false and $this->_rsSET->FeldInhalt('SRG_KEY') != '-99'){
            $SQLAnfahrtsbeschreibung = 'Select SRG_REGION from Seminarregionen where SRG_KEY = ' . $this->_DB->WertSetzen('SEP', 'Z', $this->_rsSET->FeldInhalt('SRG_KEY'));
            $rs = $this->_DB->RecordSetOeffnen($SQLAnfahrtsbeschreibung, $this->_DB->Bindevariablen('SEP'));
            $AFB = '<a href="#LINK#/dokumentanzeigen.php?dateiname='.$rs->FeldInhalt(1).'&erweiterung=pdf&bereich=seminarplananfahrt">Download Anfahrtsbeschreibung</a>';
            $Text = str_replace('#LINKAFB#', $AFB, $Text);
        } else {
            $Text = str_replace('#LINKAFB#', $this->_Form->Format('D', "Leider konnte keine Anfahrtsbeschreibung f�r diesen Ort gefunden werden."), $Text);
        }

        if(strpos($Text,'#LINKLZB#')!==false){
            $LZB = '<a href="#LINK#/dokumentanzeigen.php?dateiname=LZB_'.$this->_rsSET->FeldInhalt('SEP_SEMINAR').'&erweiterung=pdf&bereich=seminarziele">Download Lernzielblatt</a>';
            $Text = str_replace('#LINKLZB#', $LZB, $Text);
        }

        if(strpos($Text,'#LINKTH#')!==false){
            $TH = '<a href="'.$this->_AWISSprachKonserven['SEH']['mail_Link'].'">Zu den Hotelinformationen</a>';
            $Text = str_replace('#LINKTH#',$TH,$Text);
            $Text = str_replace('#SEH_SET_KEY#',$this->_rsSET->FeldInhalt('SET_KEY'),$Text);
        }

        switch ($this->_Werkzeuge->awisLevel()) {
            case awisWerkzeuge::AWIS_LEVEL_PRODUKTIV:
                $Text = str_replace('#LINK#', 'https://awis.server.atu.de', $Text);
                break;
            case awisWerkzeuge::AWIS_LEVEL_SHUTTLE:
                $Text = str_replace('#LINK#', 'https://awis.server.atu.de', $Text);
                break;
            case awisWerkzeuge::AWIS_LEVEL_STAGING:
                $Text = str_replace('#LINK#', 'https://awis-staging.server.atu.de', $Text);
                break;
            case awisWerkzeuge::AWIS_LEVEL_ENTWICK:
            default:
                $Text = str_replace('#LINK#', 'https://awis-entwick.server.atu.de', $Text);
                break;
        }

        return $Text;
    }

    /*
     * Sendet die Email
     */
    public function Senden()
    {
        $this->_Mailer->Text($this->_Text, awisMailer::FORMAT_HTML);

        if(isset($this->_Empfaenger['TO']) and $this->_Empfaenger['TO'] != ''){
            $this->_Mailer->Betreff($this->_Betreff);
        }else{
            $this->_Mailer->Betreff('NO_DATA_FOUND: Seminareinladung: Keine Email f�r SET_KEY ' . $this->_SET_KEY . ' ' . $this->_rsSET->FeldInhalt('SET_PER_NR') . ' FIL_ID: ' . $this->_FIL_ID);
            $this->Adressliste('academy.einladung@de.atu.eu','TO');
        }

        return $this->_Mailer->MailInWarteschlange();
    }

    /**
     * Setzt einen Empf�nger
     *
     * @param $Empfaenger
     * @param string $Typ = TO,CC,BCC
     */
    public function Adressliste($Empfaenger, $Typ = 'TO')
    {
        $this->_Empfaenger[$Typ] = $Empfaenger;
        $this->_Mailer->AdressListe($Typ, $Empfaenger);
    }

    /**
     * Liefert die Adressliste
     *
     * @param string $Typ TO;CC;BCC
     * @return array|mixed
     */
    public function getAdressliste($Typ = 'TO')
    {
        return $this->_Mailer->getAdressListe($Typ);
    }

    /**
     * Setzt einen Anhang im Mailer
     *
     * @param array $Datei
     */
    public function Anhaenge(array $Datei)
    {
        $this->_Mailer->Anhaenge($Datei[0], $Datei[1], false);
    }

    /**
     * Setzt den Empf�nger
     *
     * @param $Absender
     */
    public function Absender($Absender)
    {
        $this->_Mailer->Absender($Absender);
    }

    /**
     * L�st das Empf�ngerarray aus den Zuordnungen zu echten Emailadressen aus
     *
     * @param $SerializedEmpfaenger
     * @return array
     */
    protected function EmpfaengerAufloesen($SerializedEmpfaenger)
    {
        $Empfaenger = unserialize($SerializedEmpfaenger);
        if (count($Empfaenger) == 0 or $Empfaenger == false) {
            return array();
        }

        $Return = array();
        $SQL = 'SELECT FIL_ID, LAN_CODE, RL_EMAIL, GBL_EMAIL from V_FILIALEN_AKTUELL where FIL_ID = ' . $this->_DB->WertSetzen('FIL', 'Z', $this->_FIL_ID);
        $rsDaten = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FIL'));


        foreach ($Empfaenger as $Emp) {
            if ($Emp == 'TEILNEHMER' and $rsDaten->FeldInhalt(1) != '') { //Eine echter Filialteilnehmer
                $Return[] = str_pad($this->_FIL_ID, 4, '0', STR_PAD_LEFT) . '@' . ($rsDaten->FeldInhalt('LAN_CODE') != '' ? $rsDaten->FeldInhalt('LAN_CODE') : 'de') . '.atu.eu';
            } elseif ($Emp == 'TEILNEHMER') { //Vlt eine personalisierte Email?
                $SQL = 'select kko_wert as email from kontakte ';
                $SQL .= ' inner join KONTAKTEKOMMUNIKATION ';
                $SQL .= ' on kon_key = KKO_KON_KEY ';
                $SQL .= ' where kko_kot_key = 7 and rownum = 1 and';
                $SQL .= ' KON_PER_NR = ' . $this->_DB->WertSetzen('SET', 'T', $this->_rsSET->FeldInhalt('SET_PER_NR'));

                $rsKONMail = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('SET'));

                if ($rsKONMail->FeldInhalt(1) != '') {
                    $Return[] = $rsKONMail->FeldInhalt(1);
                }
            } elseif ($Emp == 'WL' and $rsDaten->FeldInhalt(1) != '') {
                $Return[] = 'WL-' . str_pad($this->_FIL_ID, 4, '0', STR_PAD_LEFT) . '@' . ($rsDaten->FeldInhalt('LAN_CODE') != '' ? $rsDaten->FeldInhalt('LAN_CODE') : 'de') . '.atu.eu';
            } elseif ($Emp == 'GL' and $rsDaten->FeldInhalt(1) != '') {
                $Return[] = 'GL-' . str_pad($this->_FIL_ID, 4, '0', STR_PAD_LEFT) . '@' . ($rsDaten->FeldInhalt('LAN_CODE') != '' ? $rsDaten->FeldInhalt('LAN_CODE') : 'de') . '.atu.eu';
            } elseif ($Emp == 'GBL' and $rsDaten->FeldInhalt(1) != '') {
                if ($rsDaten->FeldInhalt($Emp . '_EMAIL') != '') {
                    $Return[] = $rsDaten->FeldInhalt('GBL_EMAIL');
                } else {
                    $this->_Werkzeuge->EMail(array('shuttle@de.atu.eu'), 'WARNUNG - Seminarplan Email: Keine Email f�r ' . $Emp . ' von Filiale ' . $this->_FIL_ID, 'Pflegefehler?', 2, '', 'awis@de.atu.eu');
                }
            } elseif ($Emp == 'RL' and $rsDaten->FeldInhalt(1) != '') {
                if ($rsDaten->FeldInhalt($Emp . '_EMAIL') != '') {
                    $Return[] = $rsDaten->FeldInhalt('RL_EMAIL');
                } else {
                    $this->_Werkzeuge->EMail(array('shuttle@de.atu.eu'), 'WARNUNG - Seminarplan Email: Keine Email f�r ' . $Emp . ' von Filiale ' . $this->_FIL_ID, 'Pflegefehler?', 2, '', 'awis@de.atu.eu');
                }
            } elseif ($Emp == 'ACADEMY') {
                $Return[] = 'academy.einladung@de.atu.eu';
            } elseif ($Emp == 'ACADEMY_HOTEL') {
                $Return[] = 'academy.hotel@de.atu.eu';
            } elseif ($Emp == 'ACADEMY_STORNO') {
                $Return[] = 'academy.storno@de.atu.eu';
            } else {
                $this->_Werkzeuge->EMail(array('shuttle@de.atu.eu'), 'WARNUNG - Seminarplan Email: Keine Email f�r ' . $Emp . ' von Filiale ' . $this->_FIL_ID, 'Pflegefehler?', 2, '', 'awis@de.atu.eu');
            }

        }
        return $Return;
    }

    /**
     * L�st das Anhangsarray aus den Zuordnungen zu echten Pfaden auf
     *
     * @param $SerializedAnhaenge
     * @return array
     */
    protected function AnhaengeAufloesen($SerializedAnhaenge)
    {
        $Anhaenge = unserialize($SerializedAnhaenge);
        if (count($Anhaenge) == 0 or $Anhaenge == false) {
            return array();
        }
        $Return = array();

        foreach ($Anhaenge as $Anhang) {
            switch (substr($Anhang, 0, 3)) {
                case 'LZB':
                    $Return[] = array('/daten/web/dokumente/seminarplan/Lernzielblaetter/LZB_' . $this->_rsSET->FeldInhalt('SEP_SEMINAR') . '.pdf', 'Lernzielblatt.pdf');
                    break;
                case 'AFB':
                    if ($this->_rsSET->FeldInhalt('SRG_KEY') != '-99') {
                        $SQLAnfahrtsbeschreibung = 'Select SRG_REGION from Seminarregionen where SRG_KEY = ' . $this->_DB->WertSetzen('SEP', 'Z', $this->_rsSET->FeldInhalt('SRG_KEY'));
                        $rs = $this->_DB->RecordSetOeffnen($SQLAnfahrtsbeschreibung, $this->_DB->Bindevariablen('SEP'));
                        $Datei = '/daten/web/dokumente/seminarplan/Anfahrtsbeschreibungen/' . $rs->FeldInhalt(1) . '.pdf';
                        if (is_file($Datei)) {
                            $Return[] = array($Datei, 'Anfahrtsbeschreibung.pdf');
                        }
                    }
                    break;
                case 'DAT':
                    break;
            }
        }


        return $Return;

    }


    /**
     * L�scht die Adressliste
     */
    public function LoescheAdressListe()
    {
        $this->_Mailer->LoescheAdressListe();
    }


}