<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisMailer.inc';
require_once 'awisProtokoll.inc';

/**
 * Versendet die Praktikumsvereinbarungs-Mails
 */
class awisPraktikum_Mail
{

    /**
     * Datenbankverbindung
     *
     * @var awisDatenbank
     */
    private $_DB = null;
    /**
     * Benutzer
     *
     * @var awisBenutzer
     */
    private $_AWISBenutzer;

    /**
     * Mail-Verbindung
     *
     * @var awisMailer
     */
    private $_Mailer;
    /**
     * @var awisWerkzeuge
     */
    private $_awisWerkzeug;
    /**
     * @var awisFormular
     */
    private $_Form;
    /**
     * @var AWISSprachKonserven
     */
    private $_AWISSprachKonserven;

    /**
     * @var awisProtokoll
     */
    private $_Protokoll;

    /**
     * @var bool
     */
    private $_Error = false;
    /**
     * Debugausgaben
     *
     * @var int
     */
    protected $_DebugLevel = 0;

    /**
     * @var awisRecordset
     */
    protected $_rsPVS = "";

    /**
     * Absender der Mails
     * @var string
     */
    private $_MailAbsender;

    /**
     * Empf�nger der AbschlussMail
     * @var string
     */
    private $_AbschlussEmpfaenger;

    function __construct($DebugLevel = 0, $Benutzer = '')
    {
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DebugLevel = $DebugLevel;
        $this->_Form = new awisFormular();
        $this->_awisWerkzeug = new awisWerkzeuge();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Mailer = new awisMailer($this->_DB, $this->_AWISBenutzer);
        $this->_Protokoll = new awisProtokoll($this->_DB,$this->_AWISBenutzer);
        $this->_Protokoll->Init($DebugLevel, 'PraktikumsvereinbarungsMail','');
        $this->_MailAbsender = $this->_AWISBenutzer->ParameterLesen('PVS_MAIL_ABSENDER');
        $this->_AbschlussEmpfaenger = $this->_awisWerkzeug->LeseAWISParameter('awis.conf','FEHLER_MAIL_EMPFAENGER');

        $TextKonserven[] = array('PVS', '%');

        $this->_AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven);
    }


    function __destruct()
    {
        echo implode($this->_Protokoll->HoleBuffer(),PHP_EOL);
    }

    /**
     * Startet die Verarbeitung
     */
    function StarteVerarbeitung()
    {

        $SQL = "Select PVS_KEY, PVS_ANR_ID, PVS_NAME, PVS_DATUMVON, PVS_DATUMBIS, PVS_FIL_ID, lpad(PVS_FIL_ID,4,0) || FUNC_EMAILENDUNG_FILIALE(PVS_FIL_ID) as FIL_MAIL from PRAKTIKUMSVEREINBARUNG";
        $SQL .= " where PVS_DATUMBIS <= " . $this->_DB->WertSetzen('PVS', 'D', date('d.m.y'));
        $SQL .= " and PVS_AKTIV = 'A'";
        $SQL .= " and coalesce(PVS_MAILVERSAND,0) <> 1";
        $SQL .= " and PVS_EIGNUNG IS NULL";
        $this->_rsPVS = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('PVS'));


        $DebugMailText = "Ausgef�hrter SQL:" . chr(13) . chr(10);
        $DebugMailText .= $this->_DB->LetzterSQL() . chr(13) . chr(10);

        $this->_Protokoll->KonsolenAusgabe($DebugMailText,50,self::PROTOKOLL_INFO,true);
        $this->_MailsVersenden();    //Mails m�ssen noch versand werden

    }

    /**
     * Versendet die Mails zu den Filialen
     */
    private function _MailsVersenden()
    {
        while (!$this->_rsPVS->EOF()) {
            try {
                $this->_DB->TransaktionBegin();

                if ($this->_rsPVS->FeldInhalt('PVS_ANR_ID') == 2) {
                    $Betreff = $this->_AWISSprachKonserven['PVS']['mail_Erinnerung_BetreffFrau'];
                } else {
                    $Betreff = $this->_AWISSprachKonserven['PVS']['mail_Erinnerung_BetreffHerr'];
                }

                $Betreff .= " " . $this->_rsPVS->FeldInhalt('PVS_NAME');

                $Text = str_replace('#PVS_VON#', substr($this->_rsPVS->FeldInhalt('PVS_DATUMVON'), 0, 10), $this->_AWISSprachKonserven['PVS']['PVS_MAILVERSAND_TEXT']);
                $Text = str_replace('#PVS_BIS#', substr($this->_rsPVS->FeldInhalt('PVS_DATUMBIS'), 0, 10), $Text);
                if ($this->_rsPVS->FeldInhalt('PVS_ANR_ID') == 2) {
                    $Anrede = $this->_AWISSprachKonserven['PVS']['mail_Erinnerung_AnredeFrau'];
                } else {
                    $Anrede = $this->_AWISSprachKonserven['PVS']['mail_Erinnerung_AnredeHerr'];
                }
                $Text = str_replace('#PVS_ANREDE#', $Anrede, $Text);
                $Text = str_replace('#PVS_NAME#', $this->_rsPVS->FeldInhalt('PVS_NAME'), $Text);
                $Text = str_replace('#PVS_LINK#', $this->_AWISSprachKonserven['PVS']['mail_Erinnerung_Link'] . strval($this->_rsPVS->FeldInhalt('PVS_KEY')), $Text);

                $this->_Mailer->AnhaengeLoeschen();
                $this->_Mailer->Absender($this->_MailAbsender);
                $this->_Mailer->AdressListe(awisMailer::TYP_TO, $this->_rsPVS->FeldInhalt('FIL_MAIL'));
                $this->_Mailer->Text($Text, awismailer::FORMAT_HTML, true);
                $this->_Mailer->Betreff($Betreff);
                $this->_Mailer->SetzeBezug('PVS', $this->_rsPVS->FeldInhalt('PVS_KEY'));
                $this->_Mailer->MailInWarteschlange();
                $SQL = "Update PRAKTIKUMSVEREINBARUNG set PVS_MAILVERSAND = ".$this->_DB->WertSetzen('PVS','N0',1);
                $SQL .= "  where PVS_KEY = " . $this->_DB->WertSetzen('PVS','N0',$this->_DB->FeldInhaltFormat('N0', $this->_rsPVS->FeldInhalt('PVS_KEY')));
                $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('PVS'));
                $this->_DB->TransaktionCommit();
            } catch (Exception $exception) {
                $this->_DB->TransaktionRollback();
                $this->_Error = true;

                $DebugMailText = "FEHLER BEI DATENSATZ MIT DEM KEY: " . $this->_rsPVS->FeldInhalt('PVS_KEY');
                $DebugMailText .= " Exception:" . chr(13) . chr(10) . $exception;
                $this->_Protokoll->KonsolenAusgabe($DebugMailText,1,self::PROTOKOLL_FEHLER,true);
                break;
            }
            $this->_Mailer->LoescheAdressListe();
            $this->_rsPVS->DSWeiter();
        }

        $this->_Protokoll->KonsolenAusgabe("Es wurde(n) " . strval($this->_rsPVS->AnzahlDatensaetze()) . " Datensatz/-s�tze verarbeitet.",1,self::PROTOKOLL_INFO,true);
        $this->_awisWerkzeug->EMail($this->_AbschlussEmpfaenger, (($this->_Error)?("ERROR"):("OK"))." PraktikumsvereinbarungsMail - DATUM " . date('Ymd'), implode($this->_Protokoll->HoleBuffer(),PHP_EOL), 2, '',
            'awis' . strval(($this->_awisWerkzeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_SHUTTLE) ? (awisWerkzeuge::AWIS_LEVEL_SHUTTLE) : (awisWerkzeuge::AWIS_LEVEL_ENTWICK)) . '@de.atu.eu');

    }


    /**
     * Liest oder setzt den DebugLevel im Programm
     *
     * @param int $NeuerLevel Neuer DebugLevel als Zahl
     */
    public function DebugLevel($NeuerLevel = null)
    {
        if (!is_null($NeuerLevel)) {
            $this->_DebugLevel = intval($NeuerLevel);
        }
    }

    /**
     * Legt eine INFO f�r die DebugAusgabe() fest
     * @var int
     */
    const PROTOKOLL_INFO = 1;
    /**
     * Legt eine WARNUNG f�r die DebugAusgabe() fest
     * @var int
     */
    const PROTOKOLL_WARNUNG = 2;
    /**
     * Legt einen FEHLER f�r die DebugAusgabe() fest
     * @var int
     */
    const PROTOKOLL_FEHLER = 3;

}