<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Helpdesk</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1900);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'Helpdesk',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

awis_RegisterErstellen(1900, $con, (isset($_POST['cmdAktion'])?$_POST['cmdAktion']:(isset($_GET['cmdAktion'])?$_GET['cmdAktion']:'')));

print "<br><hr><input type=image title=Zur�ck alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='/index.php';>";
print "&nbsp;<input type=image title='Hilfe (Alt+h)' alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=helpdesk&HilfeBereich=" . (isset($_POST['cmdAktion'])?$_POST['cmdAktion']:(isset($_GET['cmdAktion'])?$_GET['cmdAktion']:'')) . "','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awislogoff($con);

?>
</body>
</html>

