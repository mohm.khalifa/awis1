<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['txtSHZ_HOTEL']))
{
    var_dump($_GET);
    die();
}

try {
    $DS = 0;

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();

    $Recht4200 = $AWISBenutzer->HatDasRecht(4200);
    $EditRecht = (($Recht4200 & 524288) == 524288) ? true : false;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('SEH', '%');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    if (isset($_GET['txtSHZ_HOTEL']) and $_GET['txtSHZ_HOTEL'] > 0) {

        $SQL = 'select * from SEMINARHOTEL where SEH_KEY = '.$DB->WertSetzen('SEH', 'N0', $_GET['txtSHZ_HOTEL'], true);
        $rsSEH = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SEH'));

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_PLZ'], 100);
        $Form->Erstelle_TextLabel($rsSEH->FeldOderPOST('SEH_PLZ'), 500);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_ORT'], 100);
        $Form->Erstelle_TextLabel($rsSEH->FeldOderPOST('SEH_ORT'), 500);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_STRASSE'], 100);
        $Form->Erstelle_TextLabel($rsSEH->FeldOderPOST('SEH_STRASSE'), 500);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HAUSNUMMER'], 100);
        $Form->Erstelle_TextLabel($rsSEH->FeldOderPOST('SEH_HAUSNR'), 500);
        $Form->ZeileEnde();

        if ($rsSEH->FeldOderPOST('SEH_HOMEPAGE') != null) {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['SEH']['SEH_HOMEPAGE'], 100);
            if (substr($rsSEH->FeldOderPOST('SEH_HOMEPAGE'), 0, 4) != 'http') {
                $Form->Erstelle_TextLabel($rsSEH->FeldOderPOST('SEH_HOMEPAGE'), 500, '', '*http://'.$rsSEH->FeldOderPOST('SEH_HOMEPAGE'));
            } else {
                $Form->Erstelle_TextLabel($rsSEH->FeldOderPOST('SEH_HOMEPAGE'), 500, '', '*'.$rsSEH->FeldOderPOST('SEH_HOMEPAGE'));
            }
            $Form->ZeileEnde();
        }
    }
}
catch (Exception $ex)
{
    $Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>
