<?php
/**
 * Pr�fmodul: 	0
 * Zweck:		Dieses Modul liefert IMMER einen Erfolg (mit Hinweis)
 *
 * Parameter:
 *
 *
 */

global $con;					// Verbindung zum AWIS Server
global $comcon;					// Verbindung zum COM Server
global $PreisListe;				// Preislisten-ID
global $TransaktionsID;			// Transaktions-ID
global $rsPTA;					// Testarten
global $PTAZeile;				// Aktuelle Zeile in den Testarten-RS
global $awisRSZeilen;
global $awisDBFehler;
global $AWISBenutzer;

//*****************************************************************************
// Parameter lesen
//*****************************************************************************
$SQL = 'SELECT *';
$SQL .= ' FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGEN';
$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TESTARTENPARAMETER ON PLP_PTA_ID=PTP_PTA_ID';
$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENPARAM ON PPP_PTP_KEY=PTP_KEY AND PLP_KEY = PPP_PLP_KEY';
$SQL .= ' WHERE PLP_ID_FPLB=' . $PreisListe;
$SQL .= ' AND PLP_PTA_ID=' . $rsPTA['PLP_PTA_ID'][$PTAZeile];

$Params=array();

$rsPAR = awisOpenRecordset($comcon,$SQL);
$rsPARZeilen = $awisRSZeilen;
if($rsPARZeilen>0)
{
	for($PARZeile=0;$PARZeile<$rsPARZeilen;$PARZeile++)
	{
		$Params[$rsPAR['PTP_PARAMETER'][$PARZeile]]=($rsPAR['PPP_WERT'][$PARZeile]==''?$rsPAR['PTP_STANDARDWERT'][$PARZeile]:$rsPAR['PPP_WERT'][$PARZeile]);
	}
}

//*****************************
// Historie schreiben
//*****************************
$SQL = ' INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' (PPH_PLP_KEY,PPH_TRANSAKTIONSID,PPH_BEGINN,PPH_USER,PPH_STATUS)';
$SQL .= ' VALUES(' . $rsPTA['PLP_KEY'][$PTAZeile] . '';
$SQL .= ', ' . $TransaktionsID ;
$SQL .= ', SYSDATE';
$SQL .= ', \'' . $AWISBenutzer->BenutzerName() . '\'';
$SQL .= ', 0';
$SQL .= ')';

if(!awisExecute($comcon,$SQL))
{
	echo '<span class=HinweisTex>Fehler: '. $SQL . '</span>';
}
$rsPPH = awisOpenRecordset($comcon,'SELECT MAX(PPH_Key) AS KEY FROM EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST WHERE PPH_TRANSAKTIONSID=' . $TransaktionsID . ' AND PPH_User=\'' . $AWISBenutzer->BenutzerName() . '\'');
$PPHKey = $rsPPH['KEY'][0];
unset($rsPPH);

$TestErgebnis = 1;		// Testergebnis (0=Kein Ergebnis, 1=Erfolg, 2=Fehler)
$AnzAuff=0;			// Anzahl der Auff�lligkeiten

//###############################################################################
//###############################################################################
//########################   B E G I N N   T E S T S ############################
//###############################################################################
//###############################################################################


//*****************************************************************************
// TEST 1
//*****************************************************************************


$SQL .= 'SELECT COUNT(PLT_ARTNR) AS ANZ';
$SQL .= '   FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_Version=0 AND PLT_ID_FPLB=' . $PreisListe . '';

$rsTST = awisOpenRecordset($comcon,$SQL);
$rsTSTZeilen = $awisRSZeilen;

echo '<br><br>&nbsp;&nbsp;<b>TEST 1</b>: XXX</b><br>';
if($rsTST['ANZ'][0]==0)
{
	echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;' . $rsTST['XXX'][$TSTZeilen];

	$SQL = 'INSERT INTO EXPERIAN_ATU.PLP_PREISLISTENAUFFAELLIGKEIT(PAU_PPH_KEY,PAU_AUFFAELLIGKEIT,PAU_SCHRITT)';
	$SQL .= ' VALUES(' . $PPHKey;
	$SQL .= ', \'Keine Datens�tze gefunden\'' ;
	$SQL .= ', 1)';
	if(!awisExecute($comcon,$SQL))
	{
		echo '<br><span class=HinweisText>Fehler beim Auff�lligkeiten schreiben:>'.$SQL.'</span><br>';
	}
	$TestErgebnis = 2;
}

flush();
unset($rsTST);


if($TestErgebnis==1)
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: Es wurden ' . $rsTST['ANZ'][0] . ' Datens�tze gefunden. Bewertung: <font color=green>I.O.</font></div>';
}
else
{
	echo '<br>&nbsp;&nbsp;&nbsp;<div class=PruefErgebnis>-> Ergebnis: Es wurden keine Datens�tze gefunden. Bewertung: <font color=red>Fehler.</font></div>';
}


//###############################################################################
//###############################################################################
//##########################   E N D E   T E S T S ##############################
//###############################################################################
//###############################################################################


//*********************************************
// Historie aktualisieren
//*********************************************
$SQL = 'UPDATE EXPERIAN_ATU.PLP_PREISLISTENPRUEFUNGENHIST';
$SQL .= ' SET PPH_ENDE = SYSDATE, PPH_STATUS = '.$TestErgebnis;
$SQL .= ' WHERE PPH_Key = ' . $PPHKey;
if(!awisExecute($comcon,$SQL))
{
	awisErrorMailLink('PreislistePr�fung',1,$awisDBFehler);
}


unset($rsTST);

echo '<hr>';

?>