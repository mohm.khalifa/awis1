#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung fr ein Modul
#
#	Abschnitt
#	[Header]	Infos fr die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=Autoglas
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT,CH

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.00.04;Bezeichnungen und Begruendungstexte der Differenzbearbeitung angepasst.;26.08.2015;<a href=mailto:tamara.bannert@de.atu.eu>Tamara Bannert</a>
0.00.03;Umfirmierung Firstglass zu Autoglas.;12.12.2013;<a href=mailto:nina.roesch@de.atu.eu>Nina R�sch</a>
0.00.02;PDF-Liste der gesperrten Vorg�nge angepasst.;15.06.2011;<a href=mailto:stefan.oppl@de.atu.eu>Stefan Oppl</a>
0.00.01;Erste Version;18.08.2009;<a href=mailto:stefan.oppl@de.atu.eu>Stefan Oppl</a>

