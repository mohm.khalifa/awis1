<?php
/**
 * Details zu den Benutzergruppenbezn
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XBB','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_drucken');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht25 = $AWISBenutzer->HatDasRecht(25);
	if($Recht25==0)
	{
		$Form->Fehler_KeineRechte();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	//********************************************************
	// Parameter verarbeiten
	//********************************************************
	if(isset($_GET['Del']) or isset($_POST['cmdLoeschen_x']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./gruppenverwaltung_loeschen.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./gruppenverwaltung_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));
	}
	elseif(isset($_GET['XBB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['XBB_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));
	}
	elseif(isset($_GET['XBG_XBB_KEY']))
	{
		$Param['XBG_XBB_KEY']=$DB->FeldInhaltFormat('N0',$_GET['XBG_XBB_KEY']);
	}
	elseif(isset($_POST['cmdSuche_x']))
	{
		$Param = array();

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';

		$Param['XBB_BEZ']=$Form->Format('T',$_POST['sucXBB_BEZ'],true);

		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
	}
	else
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));

		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
		}

		if(isset($_GET['Liste']))
		{
			$Param['KEY']='';
		}
		else
		{
			$AWIS_KEY1=$Param['KEY'];
		}

		if(isset($_POST['txtXBB_KEY']))
		{
			$Param['KEY']=$Form->Format('N0',$_POST['txtXBB_KEY']);
			$AWIS_KEY1=$Param['KEY'];
		}
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	if(!isset($_GET['Sort']))
	{
		$ORDERBY = ' ORDER BY XBB_BEZ';
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}

	$SQL = 'SELECT DISTINCT XBB_KEY, XBB_BEZ, XBB_BEMERKUNG, XBB_SYSTEMGRUPPE';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Benutzergruppenbez';

	$Bedingung=_BedingungErstellen($Param);

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

	$SQL .= $ORDERBY;

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	// Sortierung nach dem Ermitteln der Datenanzahl
	$rsXBB = $DB->RecordSetOeffnen($SQL);

	
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./gruppenverwaltung_Main.php?cmdAktion=Details".(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').">");

	//********************************************************
	// Daten anzeigen
	//********************************************************
	if($rsXBB->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsXBB->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$Param['KEY']='';
		
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './gruppenverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=XBB_BEZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBB_BEZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBB']['XBB_BEZ'],300,'',$Link);
		$Link = './gruppenverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=XBB_BEMERKUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XBB_BEMERKUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XBB']['XBB_BEMERKUNG'],550,'',$Link);
		$Form->ZeileEnde();
		$DS=0;

		while(!$rsXBB->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
			$Link = './gruppenverwaltung_Main.php?cmdAktion=Details&XBB_KEY='.$rsXBB->FeldInhalt('XBB_KEY').'';
			$Form->Erstelle_ListenFeld('XBB_BEZ',$rsXBB->FeldInhalt('XBB_BEZ'),0,300,false,($DS%2),'',$Link);
			$Form->Erstelle_ListenFeld('XBB_BEMERKUNG',$rsXBB->FeldInhalt('XBB_BEMERKUNG'),0,550,false,($DS%2),'','');
			$Form->ZeileEnde();

			$rsXBB->DSWeiter();
		}

		$Link = './gruppenverwaltung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();

	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$AWIS_KEY1 = $rsXBB->FeldInhalt('XBB_KEY');
		$Param['KEY']=$AWIS_KEY1;

		$Form->Erstelle_HiddenFeld('XBB_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./gruppenverwaltung_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsXBB->FeldInhalt('XBB_USER')));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($rsXBB->FeldInhalt('XBB_USERDAT')));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht25&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=true;
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBB']['XBB_BEZ'].':',150);
		$Form->Erstelle_TextFeld('XBB_BEZ',($rsXBB->FeldInhalt('XBB_BEZ')),50,350,$EditRecht);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['XBB']['XBB_BEMERKUNG'].':',150);
		$Form->Erstelle_TextFeld('XBB_BEMERKUNG',($rsXBB->FeldInhalt('XBB_BEMERKUNG')),50,350,$EditRecht);
		$Form->ZeileEnde();
		if((floatval($Recht25)&64)!=0)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['XBB']['XBB_SYSTEMGRUPPE'].':',150);
			$JaNeinAnzeige = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
			$Form->Erstelle_SelectFeld('XBB_SYSTEMGRUPPE',($rsXBB->FeldInhalt('XBB_SYSTEMGRUPPE')),100,$EditRecht,'',$OptionBitteWaehlen,'','','',$JaNeinAnzeige);
			$Form->ZeileEnde();
		}
		elseif($AWIS_KEY1==0)
		{
			$Form->Erstelle_HiddenFeld('XBB_SYSTEMGRUPPE',0);
		}

		if(!isset($_POST['cmdDSNeu_x']))
		{
			$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Mitglieder'));

			$RegDet = new awisRegister(26);
			$RegDet->ZeichneRegister($RegisterSeite);
		}
	}

	//$Form->DebugAusgabe(1, $Param, $Bedingung, $rsXBB, $_POST, $rsXBB, $SQL, $AWISSprache);
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}

	$Form->Formular_Ende();

	$AWISBenutzer->ParameterSchreiben('Formular_XBB',serialize($Param));
	
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	if(($Recht25&6)>0 AND ($rsXBB->AnzahlDatensaetze()<=1))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	if(($Recht25&4) == 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	if(($Recht25&8) == 8 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	}

	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=gruppenverwaltung&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Recht25;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND XBB_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	// Einschr�nken nach Gruppenname
	if(isset($Param['XBB_BEZ']) AND $Param['XBB_BEZ']!='')
	{
		$Bedingung .= ' AND UPPER(XBB_BEZ) '.$DB->LikeOderIst($Param['XBB_BEZ'],awisDatenbank::AWIS_LIKE_UPPER);
	}

	if((floatval($Recht25)&64)!=0)
	{
		$Bedingung .= ' AND (XBB_SYSTEMGRUPPE = 0 OR XBB_SYSTEMGRUPPE is null)';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}

?>