<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisArtikelstammLieferantenartikelFunktionen.inc';

try {
    $TecDoc = new awisArtikelstammLieferantenartikelFunktionen();
    $TecDoc->TecDocHinweis($_GET['txtLAR_LIE_NR']);
} catch (Exception $ex) {
    echo 'Fehler beim Abrufen der Daten:' . $ex->getMessage();
}
?>