<?php
//**************************************************************
// Liefert die Kontakte, die eine Durchwahl haben
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

if(!isset($_GET['OTP_ID']))
{
	die('##PARAM##');
}
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$SQL = 'SELECT OTP_ID, OTP_BEZEICHNUNG ';
$SQL .= ' FROM OTPERSONEN ';
$Bedingung = '';

if($_GET['FILTER']!='')
{
	$Bedingung .= ' AND ASCIIWORT(UPPER(OTP_BEZEICHNUNG)) '.$DB->LikeOderIst(strtoupper($_GET['FILTER']).'*',awisDatenbank::AWIS_LIKE_ASCIIWORT+awisDatenbank::AWIS_LIKE_UPPER);
}
if($_GET['WERT']!='')
{
	$Bedingung .=' OR  OTP_ID = '.$_GET['WERT'];
}

if($Bedingung!='')
{
	$SQL .= ' WHERE '.substr($Bedingung,4);
}
$SQL .= " ORDER BY 2";

$rsOTP=$DB->RecordSetOeffnen($SQL);

if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo $SQL.'#~#';		// Trennzeichen
$DS=0;
if(isset($_GET['ZUSATZ']) AND $_GET['ZUSATZ']!='')
{
	$Zusatz = explode('~',urldecode($_GET['ZUSATZ']));
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$Zusatz[0].'">'.$Zusatz[1].'</option>';
	}
	else
	{
		echo '#+#'.$Zusatz[0].'#+#'.$Zusatz[1].'';
	}
}

if($rsOTP->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">::--::</option>';
	}
	else
	{
		echo '#+##+#::--::';
	}
}

while(!$rsOTP->EOF())
{
	if(++$DS>200 AND $rsOTP->FeldInhalt('OTP_ID')==$_GET['WERT'])
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsOTP->FeldInhalt('OTP_ID').'">'.$rsOTP->FeldInhalt('OTP_BEZEICHNUNG').'</option>';
		}
		else
		{
			echo '#+#'.$rsOTP->FeldInhalt('OTP_ID').'#+#'.$rsOTP->FeldInhalt('OTP_BEZEICHNUNG').'';
		}
	}
	elseif($DS<200)
	{
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
		{
			echo '<option value="'.$rsOTP->FeldInhalt('OTP_ID').'">'.$rsOTP->FeldInhalt('OTP_BEZEICHNUNG').'</option>';
		}
		else
		{
			echo '#+#'.$rsOTP->FeldInhalt('OTP_ID').'#+#'.$rsOTP->FeldInhalt('OTP_BEZEICHNUNG').'';
		}
	}

	$rsOTP->DSWeiter();
}
// Wenn die Liste l�nger ist, noch eine Kennung dazu setzen
if(++$DS>200)
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="">...</option>';
	}
	else
	{
		echo '#+##+#...';
	}
}
?>