<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";

$con = awisLogon();
$RechteStufe = awisBenutzerRecht($con,6);
?>

<title></title>
</head>

<body>

<h2>AWIS - Shortcutleiste</h2>

In diesem Formular k�nnen Sie eine Shortcutleiste im Kopfbereich erstellen und anpassen.<br><br>
Sie haben maximal sechs Schaltfl�chen, auf die ein LINK gelegt werden kann. Ein LINK ist eine Adresszeile,
die im Browser eingegeben werden muss. <br><br>
Um einen LINK einzugeben, f�hren Sie folgende Schritte durch:
<ol>
<li>�ffnen Sie einem neuen Browser die gew�nschte Zielseite</li>
<li>Klicken Sie in die Adresszeile oder dr�cken Sie die F6 Taste</li>
<li>Kopieren Sie die Adresszeile mit dem Men�punkt ::Bearbeiten::->::Kopieren:: oder mit STRG+C</li></li>
<li>Schlie�en Sie den Browser</li></li>
<li>F�gen Sie die Adresszeile mit dem Men�punkt ::Bearbeiten::->::Einf�gen:: oder mit STRG+V in das Feld LINK ein</li></li>
</ol>
<br>
Anschlie�end k�nnen Sie festlegen, ob die Zielseite in einem neuen Fenster oder im aktuellen 
Fenster ge�ffnet werden soll. Die POPUP Fenster werden nur einmal ge�ffnet.<br>
In der dritten Spalte k�nnen Sie ein Bild f�r die Schaltfl�che ausw�hlen. Es existiert jedoch keine Vorschau 
f�r diese Bilder. <br>
In der abschlie�enden Bemerkung k�nnen Sie einen Text festlegen, der als Information f�r die Schaltfl�chen
angezeigt wird.



<?php

awisLogoff($con);

?>
</body>

</html>
