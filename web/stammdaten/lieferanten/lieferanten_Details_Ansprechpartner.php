<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LAP', '%');
    $TextKonserven[] = array('XBN', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Zugriffsrechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht609 = $AWISBenutzer->HatDasRecht(609);
    if ($Recht609 == 0) {
        $Form->Fehler_KeineRechte();
    }

    if (!isset($_GET['SSort'])) {
        $ORDERBY = ' ORDER BY LAP_NACHNAME, LAP_VORNAME';
    } else {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['SSort']);
    }

    // Daten ermitteln
    $SQL = 'SELECT lieferantenap.*';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM lieferantenap ';
    $SQL .= ' LEFT OUTER JOIN lieferantenapZugriffsGrp ON LAP_LZG_key = LZG_key';
    $SQL .= ' WHERE LAP_LIE_NR = ' . $DB->FeldInhaltFormat('T', $AWIS_KEY1);

    $AWIS_KEY2 = 0;
    if (isset($_GET['LAP_KEY'])) {
        $AWIS_KEY2 = $DB->FeldInhaltFormat('N0', $_GET['LAP_KEY']);
        $SQL .= ' AND LAP_KEY = ' . $AWIS_KEY2;
    }

    $SQL .= $ORDERBY;

    // Wenn ein DS ausgewählt wurde, muss nicht geblättert werden
    if ($AWIS_KEY2 <= 0) {
        // Zum Blättern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
            $AWISBenutzer->ParameterSchreiben('Formular_LAP', serialize($Param));
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $rsLAP = $DB->RecordSetOeffnen($SQL);

    if ($rsLAP->AnzahlDatensaetze() > 1 OR $AWIS_KEY2 == 0)                        // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart();

        if ((intval($Recht609) & 4) != 0) {
            $Icons[] = array('new', './lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $AWIS_KEY1 . '&Seite=Ansprechpartner&LAP_KEY=-1');
            $Form->Erstelle_ListeIcons($Icons, 58, -1);
        }

        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Ansprechpartner' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&LIE_NR=' . $AWIS_KEY1 . '&SSort=LAP_NACHNAME' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'LAP_NACHNAME'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAP']['LAP_NACHNAME'] . ',' . $AWISSprachKonserven['LAP']['LAP_VORNAME'], 350, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Ansprechpartner' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&LIE_NR=' . $AWIS_KEY1 . '&SSort=LAP_TAETIGKEIT' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'LAP_TAETIGKEIT'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAP']['LAP_TAETIGKEIT'], 350, '', $Link);
        $Link = './lieferanten_Main.php?cmdAktion=Details&Seite=Ansprechpartner' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'');
        $Link .= '&LIE_NR=' . $AWIS_KEY1 . '&SSort=LAP_TELEFON' . ((isset($_GET['SSort']) AND ($_GET['SSort'] == 'LAP_TELEFON'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['LAP']['LAP_TELEFON'], 350, '', $Link);

        $Form->ZeileEnde();

        $PEBZeile = 0;
        $DS = 0;
        while (!$rsLAP->EOF()) {
            $Form->ZeileStart();
            $Icons = array();
            if (intval($Recht609 & 4) > 0)    // Ändernrecht
            {
                $Icons[] = array('edit', './lieferanten_Main.php?cmdAktion=Details&Seite=Ansprechpartner&LAP_KEY=' . $rsLAP->FeldInhalt('LAP_KEY'));
            }
            if (intval($Recht609 & 4) > 0)    // Ändernrecht
            {
                $Icons[] = array('delete', './lieferanten_Main.php?cmdAktion=Details&Seite=Ansprechpartner&Del=' . $rsLAP->FeldInhalt('LAP_KEY'));
            }
            if ($rsLAP->FeldInhalt('LAP_EMAIL') != '')    // EMail vorhanden?
            {
                $Icons[] = array('mail', 'mailto:' . $rsLAP->FeldInhalt('LAP_EMAIL'));
            }
            $Form->Erstelle_ListeIcons($Icons, 58, ($DS % 2));

            $Form->Erstelle_ListenFeld('#LAP_NACHNAME', $rsLAP->FeldInhalt('LAP_NACHNAME') . ' ' . $rsLAP->FeldInhalt('LAP_VORNAME'), 0, 350, false, ($PEBZeile % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('#LAP_TAETIGKEIT', $rsLAP->FeldInhalt('LAP_TAETIGKEIT'), 0, 350, false, ($PEBZeile % 2), '', '', 'T', '',
                $AWISSprachKonserven['LAP']['LAP_POSITION'] . ': ' . ($rsLAP->FeldInhalt('LAP_POSITION') == ''?'-':$rsLAP->FeldInhalt('LAP_POSITION')));
            $Form->Erstelle_ListenFeld('#LAP_TELEFON', $rsLAP->FeldInhalt('LAP_TELEFON'), 0, 350, false, ($PEBZeile % 2), '', '', 'T');
            $Form->ZeileEnde();

            $rsLAP->DSWeiter();
            $PEBZeile++;
        }

        $Link = './lieferanten_Main.php?cmdAktion=Details&LIE_NR=' . $AWIS_KEY1 . '' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }            // Eine einzelne Adresse
    else                                        // Eine einzelne oder neue Adresse
    {
        $Form->Formular_Start();

        $AWIS_KEY2 = $rsLAP->FeldInhalt('LAP_KEY');

        $Form->Erstelle_HiddenFeld('LAP_KEY', $AWIS_KEY2);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a class=BilderLink href=./lieferanten_Main.php?cmdAktion=Details&LAPListe=1&Seite=Ansprechpartner accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLAP->FeldInhalt('LAP_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsLAP->FeldInhalt('LAP_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht609 & 2) != 0);

        if ($AWIS_KEY1 == 0) {
            $EditRecht = true;
        }

        $Form->ZeileStart();

        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_ANR_ID'] . ':', 150);
        $SQL = 'SELECT anr_id, anr_anrede';
        $SQL .= ' FROM Anreden';
        $SQL .= ' ORDER BY anr_anrede';
        $Form->Erstelle_SelectFeld('LAP_ANR_ID', $rsLAP->FeldInhalt('LAP_ANR_ID'), 50, $EditRecht, $SQL, '0~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '');
        $AWISCursorPosition = 'txtLAP_ANR_ID';
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_NACHNAME'] . ':', 150);
        $Form->Erstelle_TextFeld('LAP_NACHNAME', $rsLAP->FeldInhalt('LAP_NACHNAME'), 30, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_VORNAME'] . ':', 150);
        $Form->Erstelle_TextFeld('LAP_VORNAME', $rsLAP->FeldInhalt('LAP_VORNAME'), 30, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_TAETIGKEIT'] . ':', 150);
        $Form->Erstelle_TextFeld('LAP_TAETIGKEIT', $rsLAP->FeldInhalt('LAP_TAETIGKEIT'), 30, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_POSITION'] . ':', 150);
        $Form->Erstelle_TextFeld('LAP_POSITION', $rsLAP->FeldInhalt('LAP_POSITION'), 30, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_TELEFON'] . ':', 150);
        $Form->Erstelle_TextFeld('LAP_TELEFON', $rsLAP->FeldInhalt('LAP_TELEFON'), 20, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_FAXNUMMER'] . ':', 150);
        $Form->Erstelle_TextFeld('LAP_FAXNUMMER', $rsLAP->FeldInhalt('LAP_FAXNUMMER'), 20, 300, $EditRecht, '', '', '', 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_EMAIL'] . ':', 150);
        $Link = 'mailto:' . $rsLAP->FeldInhalt('LAP_EMAIL');
        $Form->Erstelle_TextFeld('LAP_EMAIL', $rsLAP->FeldInhalt('LAP_EMAIL'), 80, 900, $EditRecht, '', '', $Link, 'T');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_LZG_KEY'] . ':', 150);
        $SQL = 'SELECT LZG_KEY, LZG_BEZEICHNUNG';
        $SQL .= ' FROM LieferantenAPZugriffsGrp';
        $SQL .= ' INNER JOIN LIEFERANTENAPZUGRIFFSGRPBEN ON LGM_LZG_KEY = LZG_KEY AND LGM_XBN_KEY = ' . $AWISBenutzer->BenutzerID();
        $SQL .= ' ORDER BY LZG_BEZEICHNUNG';
        $Form->Erstelle_SelectFeld('LAP_LZG_KEY', $rsLAP->FeldInhalt('LAP_LZG_KEY'), 50, $EditRecht, $SQL, '~' . $AWISSprachKonserven['Wort']['Auswahl_ALLE'], '', '');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['LAP']['LAP_BEMERKUNG'] . ':', 150);
        $Form->Erstelle_Textarea('LAP_BEMERKUNG', $rsLAP->FeldInhalt('LAP_BEMERKUNG'), 300, 80, 5, $EditRecht);
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('PROBLEM', $ex->getMessage(), 'MELDEN', 3, 200809111043);
} catch (Exception $ex) {
    echo 'allg. Fehler:' . $ex->getMessage();
}
?>