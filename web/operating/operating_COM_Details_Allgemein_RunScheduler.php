<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="expires" content="01 Dec 2007 GMT">

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("awis_forms.inc.php");

global $con;
global $comcon;
global $AWISBenutzer;
global $awisDBFehler;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
print "<link rel=stylesheet type=text/css href=/css/awis_forms.css>";

$awisRSZeilen='';
$AWISSprachKonserven='';

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>".$AWISSprachKonserven['Fehler']['err_keineDatenbank'].".</font></h2>");
}

$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Operating_COM_Allgemein','OCA_%');
$TextKonserven[]=array('TITEL','tit_Operating_COM_Allgemein');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_anzeigen');
$TextKonserven[]=array('Wort','lbl_ausfuehren');
$TextKonserven[]=array('Wort','lbl_liste');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Verarbeitung_%');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Operating','PKT_Operating_COM_Allgemein');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

echo '<title>Awis - '.$AWISSprachKonserven['TITEL']['tit_Operating_COM_Allgemein'].'</title>';
?>
</head>
<body>
<?php

$Recht3111 = awisBenutzerRecht($con,3111);
if($Recht3111==0)
{
	awisEreignis(3,1000,'Operating-COM',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$JOBNAME='';

if (isset($_GET['JOBNAME']))
{
	$JOBNAME=$_GET['JOBNAME'];
}

$comcon = awisLogonComCluster('DE',false);

$SQL="ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR HH24:MI:SS'";
awisExecute($comcon,$SQL);

echo '<form name=frmOperating_Allgemein_RunScheduler action=./operating_COM_Details_Allgemein_RunScheduler.php method=POST>';

awis_FORM_FormularStart();

echo "<p><center><h2><b>COM - DBMS_SCHEDULER - Job</b></h2></center><p>";

If(($Recht3111&16384 )==16384 ){
	
	if ($JOBNAME!=='')
	{
		echo "<h3><b>Scheduler-Job: ".$JOBNAME."</b><h3><p>";
		
		//$SCHEDJOB=explode('.',$JOBNAME);
		
		//$SQL="SELECT * FROM ATU2EXP_SCHEDULER WHERE OWNER='".$SCHEDJOB[0]."' AND JOB_NAME='".$SCHEDJOB[1]."'";
		
		$BindeVariablen=array();
		$BindeVariablen['var_T_job_name']=$JOBNAME;
		
		$SQL="SELECT * FROM ATU2EXP_SCHEDULER WHERE OWNER=DELIMITED.GET_CHAR_WORD(:var_T_job_name,1,'.') AND JOB_NAME=DELIMITED.GET_CHAR_WORD(:var_T_job_name,2,'.')";
		echo "Bla";
		var_dump($comcon);
		$rsSHJ=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsSHJZeilen=$awisRSZeilen;
		
		if ($rsSHJZeilen>0)
		{
			echo "<h4 style='margin: 0px;'><b>Aktueller Jobstatus:</b></h4>";
			awis_FORM_FormularStart();
			
			awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('AKTIVIERT',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('STATUS',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('LETZTER STATUS',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('N�CHSTE AUSF�HRUNG',200);
			awis_FORM_ZeileEnde();
			
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_ListenFeld('',$rsSHJ['ENABLED'][0],'',150);
			awis_FORM_Erstelle_ListenFeld('',$rsSHJ['STATE'][0],'',150);
			awis_FORM_Erstelle_ListenFeld('',$rsSHJ['LAST_STATUS'][0],'',150);
			awis_FORM_Erstelle_ListenFeld('',$rsSHJ['NEXT_START'][0],'',200);
			awis_FORM_ZeileEnde();
			
			awis_FORM_FormularEnde();
			
		}		
		
		$BindeVariablen=array();
		$BindeVariablen['var_T_job_name']=$JOBNAME;		
		
		$SQL="SELECT DECODE(AA.OWNER,NULL,'NOT AVAILABLE',AA.OWNER||'.'||AA.SCHEDULE_NAME) AS SCHEDULE_NAME, NVL(AA.SCHEDULE_TYPE,BB.SCHEDULE_TYPE) AS SCHEDULE_TYPE, NVL(AA.REPEAT_INTERVAL,BB.REPEAT_INTERVAL) AS REPEAT_INTERVAL ";
		$SQL.="FROM  SYS.ALL_SCHEDULER_SCHEDULES AA, ";
		$SQL.="(SELECT * FROM SYS.ALL_SCHEDULER_JOBS ";
		$SQL.="WHERE OWNER=DELIMITED.GET_CHAR_WORD(:var_T_job_name,1,'.') ";
		$SQL.="AND JOB_NAME=DELIMITED.GET_CHAR_WORD(:var_T_job_name,2,'.')) BB ";
		$SQL.="WHERE AA.OWNER(+)=BB.SCHEDULE_OWNER ";
		$SQL.="AND AA.SCHEDULE_NAME(+)=BB.SCHEDULE_NAME";
		
		$rsSHS=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsSHSZeilen=$awisRSZeilen;
		
		if ($rsSHSZeilen>0)
		{
			echo "<p><h5 style='margin: 0px;'>Job - Schedule:</h5>";
			
			$style='font-size: 80%';
			awis_FORM_FormularStart($style);
			
			awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('SCHEDULE_NAME',300);
				awis_FORM_Erstelle_Liste_Ueberschrift('SCHEDULE_TYPE',100);
				awis_FORM_Erstelle_Liste_Ueberschrift('REPEAT_INTERVAL',500);
			awis_FORM_ZeileEnde();
			
			for($SHSZeile=0;$SHSZeile<$rsSHSZeilen;$SHSZeile++)
			{
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('',$rsSHS['SCHEDULE_NAME'][$SHSZeile],'',300,false,($SHSZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHS['SCHEDULE_TYPE'][$SHSZeile],'',100,false,($SHSZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHS['REPEAT_INTERVAL'][$SHSZeile],'',500,false,($SHSZeile%2));
				awis_FORM_ZeileEnde();
			}
			
			awis_FORM_FormularEnde();
			
		}
			
		$BindeVariablen=array();
		$BindeVariablen['var_T_job_name']=$JOBNAME;
		
		$SQL="SELECT PROGRAM_NAME, PROGRAM_TYPE, PROGRAM_ACTION, NUMBER_OF_ARGUMENTS, ENABLED ";
		$SQL.="FROM SYS.ALL_SCHEDULER_PROGRAMS ";
		$SQL.="WHERE (OWNER, PROGRAM_NAME) IN ";
		$SQL.="(SELECT PROGRAM_OWNER, PROGRAM_NAME ";
		$SQL.="FROM SYS.ALL_SCHEDULER_JOBS ";
		$SQL.="WHERE OWNER=DELIMITED.GET_CHAR_WORD(:var_T_job_name,1,'.') AND JOB_NAME=DELIMITED.GET_CHAR_WORD(:var_T_job_name,2,'.'))";		
		
		$rsSHP=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsSHPZeilen=$awisRSZeilen;
		
		if ($rsSHPZeilen>0)
		{
			
			echo "<p><h5 style='margin: 0px;'>Job - Program:</h5>";
			
			$style='font-size: 80%';
			awis_FORM_FormularStart($style);
			
			awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('NAME',220);
				awis_FORM_Erstelle_Liste_Ueberschrift('TYPE',100);
				awis_FORM_Erstelle_Liste_Ueberschrift('ACTION',400);
				awis_FORM_Erstelle_Liste_Ueberschrift('NR_ARGS',70);
				awis_FORM_Erstelle_Liste_Ueberschrift('ENABLED',70);
			awis_FORM_ZeileEnde();
			
			for($SHPZeile=0;$SHPZeile<$rsSHPZeilen;$SHPZeile++)
			{
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('',$rsSHP['PROGRAM_NAME'][$SHPZeile],'',220,false,($SHPZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHP['PROGRAM_TYPE'][$SHPZeile],'',100,false,($SHPZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHP['PROGRAM_ACTION'][$SHPZeile],'',400,false,($SHPZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHP['NUMBER_OF_ARGUMENTS'][$SHPZeile],'',70,false,($SHPZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHP['ENABLED'][$SHPZeile],'',70,false,($SHPZeile%2));
				awis_FORM_ZeileEnde();
			}
			
			awis_FORM_FormularEnde();
			
		}
		
		$BindeVariablen=array();
		$BindeVariablen['var_T_job_name']=$JOBNAME;
		
		$SQL="SELECT ARGUMENT_NAME, ARGUMENT_POSITION, ARGUMENT_TYPE, VALUE ";
		$SQL.="FROM SYS.ALL_SCHEDULER_JOB_ARGS ";
		$SQL.="WHERE OWNER=DELIMITED.GET_CHAR_WORD(:var_T_job_name,1,'.') AND JOB_NAME=DELIMITED.GET_CHAR_WORD(:var_T_job_name,2,'.') ";
		$SQL.="ORDER BY ARGUMENT_POSITION ASC";
		
		$rsSHA=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsSHAZeilen=$awisRSZeilen;
		
		if ($rsSHAZeilen>0)
		{
			
			echo "<p><h5 style='margin: 0px;'>Job - Arguments:</h5>";
			
			$style='font-size: 80%';
			awis_FORM_FormularStart($style);
			
			awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('NAME',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('POSITION',70);
				awis_FORM_Erstelle_Liste_Ueberschrift('TYPE',120);
				awis_FORM_Erstelle_Liste_Ueberschrift('VALUE',400);
			awis_FORM_ZeileEnde();
			
			for($SHAZeile=0;$SHAZeile<$rsSHAZeilen;$SHAZeile++)
			{
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('',$rsSHA['ARGUMENT_NAME'][$SHAZeile],'',150,false,($SHAZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHA['ARGUMENT_POSITION'][$SHAZeile],'',70,false,($SHAZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHA['ARGUMENT_TYPE'][$SHAZeile],'',120,false,($SHAZeile%2));
				awis_FORM_Erstelle_ListenFeld('',$rsSHA['VALUE'][$SHAZeile],'',400,false,($SHAZeile%2));
				awis_FORM_ZeileEnde();
			}
			
			awis_FORM_FormularEnde();
			
		}		
		
		echo "<h4><b>W�hlen sie die Aktion aus die sie ausf�hren m�chten !!!</b><h4><p>";
		
		if ($rsSHJ['STATE'][0]==='RUNNING')
		{
			awis_FORM_Hinweistext('Der Scheduler-Job wird aktuell ausgef�hrt. Schlie�en sie das Fenster und aktualisieren (F5) sie die �bersicht!');	
		}
		else
		{		
			echo "<input type=submit name='cmdRunScheduler' title='RUN_JOB - Startet den Scheduler-Job:".$JOBNAME."' value='Ausf�hren' style='margin: 0px 20px;'>";
			if($rsSHJ['ENABLED'][0]==='FALSE')
			{
				echo "<input type=submit name='cmdEnableScheduler' title='ENABLE - Aktiviert den Scheduler-Job:".$JOBNAME."' value='Aktivieren' style='margin: 0px 20px;'>";
			}elseif($rsSHJ['ENABLED'][0]==='TRUE')
			{
				echo "<input type=submit name='cmdDisableScheduler' title='DISABLE - Deaktiviert den Scheduler-Job:".$JOBNAME."' value='Deaktivieren' style='margin: 0px 20px;'>";
			}			
		}
		
		awis_FORM_Trennzeile('P');
		
		echo "<input type=button value='Schlie�en' style='margin: 20px 20px;' onclick=\"javascript:opener.location.reload();self.close();\">";
		echo "<input type=hidden name='JOBNAME' value='".$JOBNAME."'>";
	
		awis_FORM_Trennzeile('D');
		
		$BindeVariablen=array();
		$BindeVariablen['var_T_job_name']=$JOBNAME;
		
		$SQL="SELECT * FROM (SELECT AA.*, ROW_NUMBER() OVER (ORDER BY AA.USERDAT DESC) AS RNUM FROM AWIS_SCHEDULER_LOG AA WHERE JOB_NAME=:var_T_job_name) WHERE RNUM<6";
		
		$rsSHL=awisOpenRecordset($comcon,$SQL,true,false,$BindeVariablen);
		$rsSHLZeilen=$awisRSZeilen;
		
		if ($rsSHLZeilen>0)
		{
			echo "<p><h4 style='margin: 0px;'><b>Letzte Ausf�hrungen (5) per AWIS:</b></h4>";
			awis_FORM_FormularStart();
			
			awis_FORM_ZeileStart();
				awis_FORM_Erstelle_Liste_Ueberschrift('COMMAND',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('USERNAME',150);
				awis_FORM_Erstelle_Liste_Ueberschrift('USERDAT',150);				
			awis_FORM_ZeileEnde();

			for($SHLZeile=0;$SHLZeile<$rsSHLZeilen;$SHLZeile++)
			{
				awis_FORM_ZeileStart();
				awis_FORM_Erstelle_ListenFeld('',$rsSHL['COMMAND'][$SHLZeile],'',150);
				awis_FORM_Erstelle_ListenFeld('',$rsSHL['USERNAME'][$SHLZeile],'',150);
				awis_FORM_Erstelle_ListenFeld('',$rsSHL['USERDAT'][$SHLZeile],'',150);			
				awis_FORM_ZeileEnde();
			}
			awis_FORM_FormularEnde();
		}
	}
	
	if (isset($_POST['JOBNAME']) && $_POST['JOBNAME']!=='')
	{
		$SQL='';
		$RunCommand='';
		echo str_repeat(" ",4096)."<pre></pre>";
		
		echo '<br><center><h2>Scheduler-Job: <b>'.$_POST['JOBNAME'].'</b> wird verarbeitet</b>...</h2></center>';
		echo "<br><center><input type=image title='Scheduler-Job' src=/bilder/loading_bar.gif name=cmdLoading></center>";
		echo "<br><center>Working...</center>";
		
		ob_flush();
		flush();
		usleep(50000);
		
		if (isset($_POST['cmdRunScheduler']) && $_POST['cmdRunScheduler']!=='')
		{
			$RunCommand='RUN_JOB';
			$SQL="BEGIN SYS.DBMS_SCHEDULER.RUN_JOB('".$_POST['JOBNAME']."',FALSE); END;";
		}elseif (isset($_POST['cmdEnableScheduler']) && $_POST['cmdEnableScheduler']!=='')
		{
			$RunCommand='ENABLE_JOB';
			$SQL="BEGIN SYS.DBMS_SCHEDULER.ENABLE('".$_POST['JOBNAME']."'); END;";
		}elseif (isset($_POST['cmdDisableScheduler']) && $_POST['cmdDisableScheduler']!=='')
		{
			$RunCommand='DISABLE_JOB';
			$SQL="BEGIN SYS.DBMS_SCHEDULER.DISABLE('".$_POST['JOBNAME']."',FALSE); END;";
		}
		
		if($SQL!=='')
		{
			if(awisExecute($comcon, $SQL)===false)
			{
				awisErrorMailLink('201201051152-operating_COM_Details_Allgemein_RunScheduler.php',3,$awisDBFehler['message']);
				die();
			}
			else{
				sleep(5);
				echo "<p><h2><b>Verarbeitung f�r Scheduler-Job: ".$_POST['JOBNAME']." l�uft !!!</b></h2>";
				echo '<p><input type=button value="Schlie�en" onclick="javascript:opener.location.reload();self.close();">';
				$SQL="INSERT INTO AWIS_SCHEDULER_LOG (COMMAND, USERNAME, JOB_NAME) VALUES('".$RunCommand."','".awisBenutzerName()."','".$_POST['JOBNAME']."')";
				if(awisExecute($comcon, $SQL)===false)
				{
					awisErrorMailLink('201202021442-operating_COM_Details_Allgemein_RunScheduler.php',3,$awisDBFehler['message']);
					die();
				}
			}
		}
	}
	
}
else {
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
}

awis_FORM_FormularEnde();

echo "</form>";

awislogoff($comcon);
awislogoff($con);
?>
</body>
</html>

