<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('PRA','%');
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Meldung','UngueltigerArtikel');
$TextKonserven[]=array('Meldung','Voraussetzung%');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	$FIL_ID = $_POST['txtFIL_ID'];

	//***********************************************************************************
	//** Problemartikel einf�gen
	//***********************************************************************************
	if(isset($_POST['txtPRA_AST_ATUNR']) AND $_POST['txtPRA_AST_ATUNR']!='')
	{
		$SQL = 'SELECT AST_ATUNR, PRA_KEY';
		$SQL .= ' FROM Artikelstamm';
		$SQL .= ' INNER JOIN Filialbestand ON AST_ATUNR = FIB_AST_ATUNR AND FIB_FIL_ID = 0'.$FIL_ID;
		$SQL .= ' INNER JOIN Warenuntergruppen ON AST_WUG_KEY = WUG_KEY';
		$SQL .= ' INNER JOIN REIFEN ON REI_AST_ATUNR = AST_ATUNR';
		$SQL .= ' LEFT OUTER JOIN Problemartikel ON AST_ATUNR = PRA_AST_ATUNR AND PRA_KATEGORIE='.$DB->FeldInhaltFormat('N0',$_POST['txtPRA_KATEGORIE']);
		$SQL .= ' WHERE AST_ATUNR = '.$DB->FeldInhaltFormat('TU',$_POST['txtPRA_AST_ATUNR']);
		$SQL .= " AND WUG_WGR_ID IN ('01','02','23')";
		$SQL .= ' AND REI_BREITE <= 185';

		$rsAST = $DB->RecordSetOeffnen($SQL);
		if($rsAST->EOF())
		{
			$Form->Formular_Start();

			$Form->ZeileStart();
			$Form->Hinweistext($TXT_Speichern['PRA']['UngueltigerArtikel'],1);
			$Form->ZeileEnde();

			$Form->Trennzeile();

			$Form->ZeileStart();
			$Form->Hinweistext($TXT_Speichern['PRA']['Voraussetzung1'],0);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Hinweistext($TXT_Speichern['PRA']['Voraussetzung2'],0);
			$Form->ZeileEnde();
			$Form->ZeileStart();
			$Form->Hinweistext($TXT_Speichern['PRA']['Voraussetzung3'],0);
			$Form->ZeileEnde();

			$Form->Trennzeile();

			$Form->Formular_Ende();
		}
		elseif($rsAST->FeldInhalt('PRA_KEY')=='')
		{
			$SQL = 'INSERT INTO Problemartikel';
			$SQL .= '(PRA_AST_ATUNR, PRA_KATEGORIE, PRA_STATUS, PRA_USER, PRA_USERDAT)';
			$SQL .= ' VALUES (';
			$SQL .= ' '.$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_ATUNR'));
			$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$_POST['txtPRA_KATEGORIE']);
			$SQL .= ' ,\'A\'';
			$SQL .= ' , '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
			$SQL .= ' , SYSDATE)';

			if($DB->Ausfuehren($SQL,'',true)===false)
			{
				$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
			}
		}
	}



	$Felder = explode(';',$Form->NameInArray($_POST, 'txtPAB_ISTBESTAND_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		foreach($Felder AS $Feld)
		{
			$FeldTeile = explode('_',$Feld);
			$BemerkungsFeld = str_replace('ISTBESTAND','BEMERKUNG',$Feld);

			$SQL = 'SELECT * FROM ProblemArtikelbestand WHERE PAB_KEY = 0'.$FeldTeile[3];
			$SQL .= ' OR (PAB_PRA_KEY='.$FeldTeile[2].' AND PAB_DATUM=TRUNC(SYSDATE) AND PAB_FIL_ID='.$_POST['txtFIL_ID'].')';
			$rsPAB = $DB->RecordSetOeffnen($SQL);
			if(!$rsPAB->EOF())
			{
				$FeldTeile[3]=$rsPAB->FeldInhalt('PAB_KEY');
			}

			$Speichern = false;

			$FeldName = 'PAB_ISTBESTAND';
			$WertNeu=$DB->FeldInhaltFormat($rsPAB->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
			$WertAlt=$DB->FeldInhaltFormat($rsPAB->FeldInfo($FeldName,'TypKZ'),$_POST['old'.substr($Feld,3)],true);
			if($WertAlt!=$WertNeu)
			{
				$Speichern = true;
			}

			$FeldName = 'PAB_BEMERKUNG';
			$WertNeu=$DB->FeldInhaltFormat($rsPAB->FeldInfo($FeldName,'TypKZ'),$_POST[$BemerkungsFeld],true);
			$WertAlt=$DB->FeldInhaltFormat($rsPAB->FeldInfo($FeldName,'TypKZ'),$_POST['old'.substr($BemerkungsFeld,3)],true);
			if($WertAlt!=$WertNeu)
			{
				$Speichern = true;
			}

			$SQL = '';
			if($DB->FeldInhaltFormat('N0',$FeldTeile[3],false)==0)		// Neu
			{
				if($DB->FeldInhaltFormat('N0',$_POST[$Feld],true)!='null'
					OR $DB->FeldInhaltFormat('T',$_POST[$BemerkungsFeld],true)!='null'
					)
				{
					if($Speichern)
					{
						$SQL = 'INSERT INTO ProblemArtikelbestand';
						$SQL .= '(PAB_PRA_KEY, PAB_FIL_ID, PAB_DATUM, PAB_ISTBESTAND,PAB_SOLLBESTAND, PAB_BEMERKUNG, PAB_USER, PAB_USERDAT)';
						$SQL .= ' VALUES(';
						$SQL .= ' '.$FeldTeile[2];
						$SQL .= ', '.$DB->FeldInhaltFormat('N0',$_POST['txtFIL_ID']);
						$SQL .= ', TRUNC(SYSDATE)';
						$SQL .= ', '.$DB->FeldInhaltFormat('N0',$_POST[$Feld],true);
						$SQL .= ', '.$DB->FeldInhaltFormat('N0',$_POST['txtPAB_FIBBESTAND_'.$FeldTeile[2]],false);
						$SQL .= ', '.$DB->FeldInhaltFormat('T',$_POST[$BemerkungsFeld]);
						$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
						$SQL .= ', sysdate)';
					}
				}
			}
			else		// �ndern
			{
				if($Speichern)
				{
					$SQL = 'UPDATE ProblemArtikelbestand SET ';
					$SQL .= ' PAB_ISTBESTAND= '.$DB->FeldInhaltFormat('N0',$_POST[$Feld]);
					$SQL .= ', PAB_SOLLBESTAND = '.$DB->FeldInhaltFormat('N0',$_POST['txtPAB_FIBBESTAND_'.$FeldTeile[2]],false);
					$SQL .= ', PAB_BEMERKUNG = '.$DB->FeldInhaltFormat('T',$_POST[$BemerkungsFeld]);
					$SQL .= ', PAB_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', PAB_USERDAT = sysdate';
					$SQL .= ' WHERE PAB_KEY = 0'.$FeldTeile[3];
				}
			}

			if($SQL!='')
			{
				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException($Form->LadeTextBaustein('FEHLER','SpeicherFehler'),200901151754,$SQL,2);
				}
			}
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>