<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try
{
	$TextKonserven = array();
    $TextKonserven[]=array('HEIDLER','*');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	

	$AWISBenutzer = awisBenutzer::Init();
    $UserFilialen = $AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
    $AWISCursorPosition = 'sucKNDNR';

	$Recht45000 = $AWISBenutzer->HatDasRecht(45000);

	if($Recht45000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

    $Form->SchreibeHTMLCode('<form name="frmHeidlerSuche" action="./heidler_Main.php?cmdAktion=Details" method="POST"  >');
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Heidler'));

    $Form->Formular_Start();

    if ($UserFilialen == '') {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['HEIDLER_LAGER'] . ':', 200);
        $Daten = explode('|', $AWISSprachKonserven['HEIDLER']['HEIDLER_LST_LAGER']);
        $Form->Erstelle_SelectFeld('*LAGER', ($Param['SPEICHERN'] == 'on'?$Param['LAGER']:''), 220, true, '', '', '', '', '', $Daten);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['KNDNR'] . ':', 200);
        $Form->Erstelle_TextFeld('*KNDNR', ($Param['SPEICHERN'] == 'on'?$Param['KNDNR']:''), 10, 220, true);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['EMPF_LKZ'] . ':', 200);
        $Form->Erstelle_TextFeld('*EMPF_LKZ', ($Param['SPEICHERN'] == 'on'?$Param['EMPF_LKZ']:''), 10, 220, true);
        $Form->ZeileEnde();
    }
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['TRACKING_NR'] . ':',200);
    $Form->Erstelle_TextFeld('*TRACKING_NR',($Param['SPEICHERN']=='on'?$Param['TRACKING_NR']:''),50,200,true);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['ATU_NR'] . ':',200);
    $Form->Erstelle_TextFeld('*ATU_NR',($Param['SPEICHERN']=='on'?$Param['ATU_NR']:''),10,220,true);
    $Form->ZeileEnde();
    
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['DATUM_KOMMISSIONIERUNG'] . ' von:',200);
    $Form->Erstelle_TextFeld('*DATUM_KOMMISSIONIERUNG_VON',($Param['SPEICHERN']=='on'?$Param['DATUM_KOMMISSIONIERUNG_VON']:''),10,220,true, '', '', '', 'D');
    $Form->Erstelle_TextLabel('bis:',30);
    $Form->Erstelle_TextFeld('*DATUM_KOMMISSIONIERUNG_BIS',($Param['SPEICHERN']=='on'?$Param['DATUM_KOMMISSIONIERUNG_BIS']:''),10,220,true, '', '', '', 'D');
    $Form->ZeileEnde();

    if ($UserFilialen == '') {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['HEIDLER_VERSANDART'] . ':', 200);
        $Daten = explode('|', $AWISSprachKonserven['HEIDLER']['HEIDLER_LST_VERSANDART']);
        $Form->Erstelle_SelectFeld('*VERSANDART', ($Param['SPEICHERN'] == 'on'?$Param['VERSANDART']:''), 220, true, '', '', '', '', '', $Daten);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['SATZKENNUNG'] . ':', 200);
        $SQL_lst = 'SELECT trim(SATZKENNUNG), trim(SATZKENNUNG)';
        $SQL_lst .= ' FROM MV_HEIDLER';
        $SQL_lst .= ' GROUP BY trim(SATZKENNUNG)';
        $Form->Erstelle_SelectFeld('*SATZKENNUNG', ($Param['SPEICHERN'] == 'on'?$Param['SATZKENNUNG']:''), 220, true, $SQL_lst, $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['MANDANT'] . ':', 200);
        $Form->Erstelle_TextFeld('*MANDANT', ($Param['SPEICHERN'] == 'on'?$Param['MANDANT']:''), 10, 220, true);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['HEIDLER']['HEIDLER_MANDANT'] . ':', 200);
        $Form->Erstelle_TextFeld('*HEIDLER_MANDANT', ($Param['SPEICHERN'] == 'on'?$Param['HEIDLER_MANDANT']:''), 10, 220, true);
        $Form->ZeileEnde();

        // Auswahl kann gespeichert werden
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
        $Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
        $Form->ZeileEnde();
    }


    
    $Form->Formular_Ende();
    
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
    $Form->SchreibeHTMLCode('</form>');
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200906241613");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>