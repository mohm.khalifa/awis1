<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

//echo $intSeq;

$intClsRechnung = 19;   //Wert welcher in der Tabelle "Rechnungen_konto" Spalte "Rek_Betragart" dem Wert "Rechnung CLS" aus der Tabelle "Bezahltdurch" entspricht
$intBestDsAend = $intCardIFrameBestDsAendern;

$strSql="select rek_buchid,rek_rnr,rek_betrag,rek_datum,rek_betragart,rek_kontoauszugid
from rechnungen_konto where rek_rnr='".$strRec_rnr."' and rek_loesch=0 order by rek_datum,rek_buchid";
$arrRegKonto=abfrage("ora",$strSql);


//Daten f�r Listenfeld Buchungsart (exklusiv Mahngeb�hren (25), Storno Mahngeb�hren (24) )
$strSql="select bez_bezahlt_id,bez_bezahlt_durch from bezahltdurch where bez_bezahlt_id not in(24,25) order by bez_bezahlt_durch";

$arrBezahltDurch=abfrage("ora",$strSql);

?>
<html>
<head>
<title></title>
<?php include("card/css/css_card_master.php"); ?>
<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
<script language="JavaScript">
function aenderungzeilerest()	//Zus�tzlich zu der Funktion aenderungzeile()
{
	document.getElementById('neu').setAttribute("onclick","");		//Funktion onclick auf Button neuer Dastensatz ausschalten
	parent.document.getElementById('rechnungen').setAttribute("onclick","");		//Funktion onclick Schriftzug "Rechnungen:" ausschalten
	
	//Alle Felder in der Hauptseite werden disabled gesetzt
	var obj=parent.document.getElementsByName("grau");
	
	for(i=0;i<obj.length;i++)
	{
		obj[i].disabled = true;
	}
	
	document.getElementById('but'+window.event.srcElement.id.slice(3)).style.display='inline';		//Button wird sichtbar
	//top.cardnavigation.document.getElementById('speichern').style.visibility='visible';		//Speicherbutton in Navigationsleiste wird sichtbar		
}

function ifrsend(parameter)
{
	var obj = window.event.srcElement;
	
	var buchid = obj.id.slice(3);
		
	var betobj = document.getElementById("bet"+buchid);
	var datobj = document.getElementById("dat"+buchid);
	var buaobj = document.getElementById("bua"+buchid);
	var kazobj = document.getElementById("kaz"+buchid);
	
	
	
	//Betragspr�fung
	if(!betragspruef(betobj,9))		//Zweiter Parameter = Anzahl erlaubter Vorkommastellen
		return;
	
	//Datumspr�fung
	if(!datumspruefung(datobj))
	{
		return;
	}
	
	//Pr�fung ob Auswahl in Liste Buchungsart erfolgt ist
	
	var buchungsart = listeneintrag(buaobj,"value");
	
	if(buchungsart==-1)
	{
		alert(unescape("W%E4hlen Sie bitte einen Eintrag aus der Liste \"Buchungsart\"!"));
		buaobj.focus();
		return;
	}
	
	var betrag=datenbankbetrag(betobj.value);
	var datum=datobj.value;
	var kontoauszugid = kazobj.value;
	
	<?php
	if($intZeitmessung)
	{
	?>
		top.card_ladezeit.sendezeit("IFrame �ndern");	//Zeitmessung
	<?php } ?>
		
	var weitereParam = "";
	
	if(parameter=="ohneKaz")		//Wenn Kontoauszug vorerst nicht eingetragen werden soll
	{
		var weitereParam = "&intOhneKaz=1";
	}
	
	parent.location.href="card_haupt.php?strRec_rnr=<?php echo $strRec_rnr; ?>&strRek_Aend_Benutzer=<?php echo $PHP_AUTH_USER; ?>
	&intRek_buchid="+buchid+"&dblRek_betrag="+betrag+"&datRek_datum="+datum+"&intRek_betragart="+buchungsart+"&intRek_kontoauszugid="+kontoauszugid+weitereParam;
}

//Wird aufgerufen wenn neuer Datensatz angef�gt werden soll

function neu()
{
	aenderungzeile('aenderungzeilerest');	//nicht betroffene Felder ausgrauen
	
	window.event.srcElement.style.display = "none";
	
	var obj = document.getElementsByName("neuzeile");
	
	for(i=0;i<obj.length;i++)
	{
		obj[i].style.visibility = "visible";
	}
	document.getElementById("rnr").style.display = "inline";
	
	document.getElementById("bet").focus();
}

function init()		//Aufruf �ber <body onload="init();">
{
	document.ondblclick = zoom;
	
	//document.getElementById('neu').focus();
	
	<?php	if($intSeq&&$intOhneKaz)		//Wenn neuer Datensatz vorerst ohne Kontoauszug angelegt werden soll
			{	?>
				document.getElementById("kaz"+<?php echo $intSeq;?>).click();		//Dadurch wird Fenster mahngebuehren ge�ffnet
	<?php	} ?>
	
	<?php
	
		if($arrRegKonto["zeilen"]>=1)
		{
			?>
				try{
					parent.parent.card_navigation.document.getElementById("aktrnr").value='<?php echo $arrRegKonto["trefferarray"]["REK_RNR"][0];?>';
				}catch(e){}
		<?php
		}
		else
		{
			?>
			try{
					parent.parent.card_navigation.document.getElementById("aktrnr").value='';
				}catch(e){}
		<?php
		}
	?>
}

//Wird aufgerufen, wenn Datensatz gel�scht werden soll

function loeschen()
{
	var obj = window.event.srcElement;
	
	var buchid = obj.id.slice(3);
	
	var datobj = document.getElementById("dat"+buchid).value;
	var betobj = document.getElementById("bet"+buchid).value;
	var buaobj = listeneintrag(document.getElementById("bua"+buchid));
	var kazobj = document.getElementById("kaz"+buchid).value;
	
	var rueck = confirm(unescape("Soll der Datensatz\n\nDatum: "+datobj+"\nBetrag: "+betobj+"\nBuchungsart: "+buaobj+"\nKontoauszug: "+kazobj+"\n\nwirklich gel%F6scht werden?"));
	
	if(rueck)
	{
		<?php
		if($intZeitmessung)
		{
		?>
			top.card_ladezeit.sendezeit("IFrame �ndern");	//Zeitmessung
		<?php } ?>
		
		parent.location.href="card_haupt.php?strRec_rnr=<?php echo $strRec_rnr; ?>&strRek_Aend_Benutzer=<?php 
		echo $PHP_AUTH_USER; ?>&intRek_buchid="+buchid+"&intRek_loesch=1";
	}
}

//Wird bei Klick auf das Feld Kontoauszug ausgef�hrt

function kontoauszug()
{

	var AktObj = window.event.srcElement;
	var idAktObj = AktObj.id;
	var intKazId = AktObj.value;
	
	if(idAktObj == 'kaz')	//Wenn gerade neuer Datensatz angelegt wird
	{
		ifrsend("ohneKaz");
		return;
	}
	
		
	var parameter = "intDialog=1&intKaa_tabelle=1";			// 1 entspricht der Tabelle Rechnungen_Konto
		
	if(intKazId == 0)
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("�bersicht Kontoausz�ge anzeigen");	//Zeitmessung
		<?php } 
		
		echo dialog("kazuebersicht_frameset.php"); ?>
		
		if(rueckgabe!='undefined')
		{
			intKazId = rueckgabe;
		}
	}
	else
	{
		parameter += "&intNeuSperre=1";
	}
	
	if(intKazId!=0)
	{
		<?php
		if($intZeitmessung)
		{
			echo str_repeat("top.dialogArguments.",$intDialog); ?>top.card_ladezeit.sendezeit("Einzelner Kontoauszug anzeigen");	//Zeitmessung
		<?php } ?>
			
		var intMagId = idAktObj.slice(3);
	
		parameter += "&intKazId="+intKazId+"&intMagId="+intMagId;
		
		<?php echo dialog("kazeinzelsatz_frameset.php"); ?>
		
		parent.top.card_navigation.aktualisieren();		//Fenster neu einlesen
	}
}

</script>
</head>
<body onload="init();">
<table border="0" cellpadding="0" cellspacing="0" align="left">

<?php
for($i=0;$i<$arrRegKonto["zeilen"];$i++)
{
	$intBuchid = feldwert($arrRegKonto,"REK_BUCHID",$i);
	
	//Datens�tze, welche dem Rechnungssatz entsprechen, sind grunds�tzlich readonly
	
	$intBestAend = $intBestDsAend;
	
	if(feldwert($arrRegKonto,"REK_BETRAGART",$i)==$intClsRechnung)
	{
		$intBestAend = 0;
	}
	
	
	echo "<tr>";
	echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"ifr_rgnr\" id=\"rnr".$intBuchid."\" value=\"".feldwert($arrRegKonto,"REK_RNR",$i)."\" readOnly></td>";
	
	if(!$intBestAend)
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"ifr_dat\" id=\"dat".$intBuchid."\" maxlength=\"8\" value=\"".feldwert($arrRegKonto,"REK_DATUM",$i)."\" readonly></td>";
	else
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"ifr_dat\" id=\"dat".$intBuchid."\" maxlength=\"8\" onkeyup=\"aenderungzeile('aenderungzeilerest');\" value=\"".feldwert($arrRegKonto,"REK_DATUM",$i)."\"></td>";
		
	if(!$intBestAend)
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"ifr_betr\" id=\"bet".$intBuchid."\" maxlength=\"17\" value=\"".eurobetrag(feldwert($arrRegKonto,"REK_BETRAG",$i))."\" readonly></td>";
	else	
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" class=\"ifr_betr\" id=\"bet".$intBuchid."\" maxlength=\"17\" onkeyup=\"aenderungzeile('aenderungzeilerest');\" value=\"".eurobetrag(feldwert($arrRegKonto,"REK_BETRAG",$i))."\"></td>";
		
	$arr = $arrBezahltDurch["trefferarray"];
	
	echo "<td>";
	
	if(!$intBestAend)
	{
		echo element(array(
		"type"=>"select",
		"class"=>"ifr_buart",
		"name"=>"grau",
		"id"=>"bua".$intBuchid,
		"readOnly"=>"true",
		"optionsarray"=>$arrBezahltDurch["trefferarray"],
		"optionstextfeld"=>"BEZ_BEZAHLT_DURCH",
		"optionswertfeld"=>"BEZ_BEZAHLT_ID",
		"optionsvorauswahlwert"=>$arrRegKonto["trefferarray"]["REK_BETRAGART"][$i],
		"optionsvorauswahlfeld"=>"BEZ_BEZAHLT_ID"
		));
	}
	else
	{
		echo element(array(
		"type"=>"select",
		"class"=>"ifr_buart",
		"name"=>"grau",
		"id"=>"bua".$intBuchid,
		"onchange"=>"aenderungzeile('aenderungzeilerest');",
		"optionsarray"=>$arrBezahltDurch["trefferarray"],
		"optionstextfeld"=>"BEZ_BEZAHLT_DURCH",
		"optionswertfeld"=>"BEZ_BEZAHLT_ID",
		"optionsvorauswahlwert"=>$arrRegKonto["trefferarray"]["REK_BETRAGART"][$i],
		"optionsvorauswahlfeld"=>"BEZ_BEZAHLT_ID"
		));
	}
	echo "</td>";
	
	if(!$intBestAend)
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" id=\"kaz".$intBuchid.
		"\" class=\"ifr_ktoaz\" value=\"".$arrRegKonto["trefferarray"]["REK_KONTOAUSZUGID"][$i]."\" readOnly>";
	else
		echo "<td class=\"text\"><input type=\"text\" name=\"grau\" id=\"kaz".$intBuchid.
		"\" class=\"ifr_ktoaz\" value=\"".$arrRegKonto["trefferarray"]["REK_KONTOAUSZUGID"][$i]."\" style=\"cursor: hand;\"
		onclick=\"kontoauszug();\" readOnly>";
	
	echo "</td>";
	
	if(!$intBestAend)
		echo "<td class=\"quadrat\">&nbsp;</td>";
	else
		echo "<td><input type=\"image\" src=\"/bilder/diskette.png\" accesskey=\"S\" class=\"ifr_speibut\" id=\"but".$intBuchid."\" alt=\"speichern (Alt+S)\" onclick=\"ifrsend();\"><input type=\"image\" src=\"/bilder/muelleimer.png\" accesskey=\"L\" name=\"loeschen\" class=\"quadrat\" id=\"loe".$intBuchid.
		"\" alt=\"l&ouml;schen (Alt+L)\" onclick=\"loeschen();\"></td>";
	echo "</tr>";
}


?>
<tr>
<td class="text"><input type="image" src="/bilder/plus.png" id="neu" class="quadrat" alt="neuer Datensatz" onclick="neu();"><input type="text" name="neuzeile" class="ifr_rgnr" id="rnr" value="<?php echo feldwert($arrRegKonto,"REK_RNR"); ?>" readOnly></td>
<td class="text"><input type="text" class="ifr_dat" name="neuzeile" id="dat" maxlength="8" value="<?php echo date("d.m.y"); ?>"></td>
<td class="text"><input type="text" class="ifr_betr" name="neuzeile" id="bet" maxlength="17"></td>
<td>
<?php

echo "<select class=\"ifr_buart\" name=\"neuzeile\" id=\"bua\">";
echo "<option value=\"-1\"></option>";
for($k=0;$k<count($arr["BEZ_BEZAHLT_ID"]);$k++)
{
	if($arr["BEZ_BEZAHLT_ID"][$k]!=$intClsRechnung)
	{
		echo "<option value=\"".$arr["BEZ_BEZAHLT_ID"][$k]."\">".$arr["BEZ_BEZAHLT_DURCH"][$k]."</option>";
	}
}
echo "</select>";
?>
</td>
<td class="text"><input type="text" name="neuzeile" id="kaz"" class="ifr_ktoaz" value="0" style="cursor: hand;"	onclick="kontoauszug();" readOnly></td>
<td><input type="image" src="/bilder/diskette.png" accesskey="S" class="ifr_speibut" name="neuzeile" id="but" alt="speichern (Alt+S)" onclick="ifrsend();"></td>
</tr>
</table>
</body>
</html>

	
