<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $rsErgebnis;
global $AWISSprachKonserven;

try
{

	
	$AWISBenutzer = awisBenutzer::Init();
		
	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Seite = 1;
	
	$Frames = unserialize($AWISBenutzer->ParameterLesen('Schad_Frame'));
	
	$Frame1 = $Frames[0] == 1? true:false;
	$Frame2 = $Frames[1] == 1? true:false;
	$Frame3 = $Frames[2] == 1? true:false;
	$Frame4 = $Frames[3] == 1? true:false;
	$Frame5 = $Frames[4] == 1? true:false;
	
	
	$Recht40000 = $AWISBenutzer->HatDasRecht(40000);
	$Recht40004 = $AWISBenutzer->HatDasRecht(40004);

	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_BES'));
	$SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Schad_Schnellsuche');
	
	if($Frame1)
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border: 1px solid black;','UEB1"ID="UEB1');
		$Form->Schaltflaeche('script', 'cmdKlappen','onclick="EinAusklappen(1)"', '/bilder/icon_minus.png"ID="IMG_LOAD1','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_ALLGMEIN'], 76, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:block','FRM1"ID="FRAME1');
	}
	else
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;','UEB1"ID="UEB1');
		$Form->Schaltflaeche('script', 'cmdKlappen','onclick="EinAusklappen(1)"', '/bilder/icon_plus.png"ID="IMG_LOAD1','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_ALLGMEIN'], 76, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:none','FRM1"ID="FRAME1');
	}
	
	
	//PG: Wenn �ber die Schnellsuche gesucht wurde, wurden die alten Werte vom letzten Vorgang in die Felder zur�ckgeschrieben, was zu enormen Fehler gef�hrt hat.
	if (isset($_POST['cmdSuche_x']))
	{
		unset($_POST);
	}
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARB'] . ':',130);
	$Form->Erstelle_TextFeld('BES_BEARBNR',$rsErgebnis->FeldInhalt('BEARBEITUNGSNR'), 10, 150,false, '','', '','', 'L');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FIL'] . ':',30);
	$Form->AuswahlBox('!BES_FIL', 'box1','', 'BES_FIL', '', 60,2,isset($_POST['sucBES_FIL'])?$_POST['sucBES_FIL']:$rsErgebnis->FeldInhalt('FILNR'),'T',true,'','','',4,'','','',0,'','autocomplete=off','onInput;');
	$Form->Erstelle_HiddenFeld('oldBES_FIL',$rsErgebnis->FeldInhalt('FILNR'));
	
	
	
	
	
	if($AWIS_KEY1 != '-1')
	{
	   $AWISCursorPosition = 'sucBES_FIL';
	}
	
	$Inhalt = '';
	$Fehler = '';
	$FIL_ID = '';
	ob_start();
	
	
	if($rsErgebnis->AnzahlDatensaetze() == 0 and (!isset($_POST['sucBES_FIL']) or $_POST['sucBES_FIL'] == ''))
	{
	
		$FIL_ID = '0';
		$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
	}
	else
	{
		if(isset($_POST['sucBES_FIL']) and $_POST['sucBES_FIL']!= '')
		{
			$FIL_ID= intval($_POST['sucBES_FIL']);
			if($_POST['sucBES_FIL'] == '')
			{
				$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
			}
		}
		else
		{
			if(isset($_POST['sucBES_FIL']) and $_POST['sucBES_FIL'] == '')
			{
				$FIL_ID = '0';
				$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
			}
			else
			{
				$FIL_ID = intval($rsErgebnis->FeldInhalt('FILNR'));
			}
		}
	
		if($FIL_ID == '0' and $Fehler == '')
		{
			$Fehler == 'keine g&uuml;ltige Fililale';
		}
	}
	
	$SQL  = ' Select * from( ';
	$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
	$SQL .= ' where fil_gruppe IS NOT NULL OR';
	$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
	$SQL .= ' union';
	$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
	$SQL .= ' FROM schaddev09.pseudofilialen)';
	$SQL .= ' where fil_id =0'.$FIL_ID;
	
	$rsFIL = $DB->RecordSetOeffnen($SQL);
		
	if($rsFIL->AnzahlDatensaetze() == 0 and $Fehler == '')
	{
		$Fehler = 'keine g&uuml;ltige Fililale';
	}
	
	//Eingabefeld AJAX Loader
	$Form->Erstelle_TextFeld('BES_FILILAE',$Fehler == '' ? $rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,499, false, '', '', '', 'T', 'L','','',50);
	
	$Inhalt = ob_get_contents();
	ob_end_clean();
	
	$Form->AuswahlBoxHuelle('box1','AuswahlListe','',$Inhalt);
	
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ORTEREIGNIS'] . ':',101);
	$Form->Erstelle_TextFeld('BES_ORTEREIGNIS',$rsErgebnis->FeldInhalt('ORTEREIGNIS'), 10, 150,true, '','', '','', 'L','','',50);
	$Form->ZeileEnde();
	
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ARCHIVNR'] . ':',130);
	$Form->Erstelle_TextFeld('BES_ARCHIVNR',$rsErgebnis->FeldInhalt('VORGANGSNR'), 10, 124,true, '','', '','', 'L','','',20,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUFIL'] . ':',56);
	$Form->AuswahlBox('!BES_VUFIL', 'box2','', 'BES_FIL', '', 60,2,isset($_POST['sucBES_VUFIL'])?$_POST['sucBES_VUFIL']:$rsErgebnis->FeldInhalt('VERURS_FILIALE'),'T',true,'','','',4,'','','',0,'','autocomplete=off','onInput;');
	
	
	ob_start();
	
	
	if($rsErgebnis->AnzahlDatensaetze() == 0 and (!isset($_POST['sucBES_VUFIL']) or $_POST['sucBES_VUFIL'] == ''))
	{
	
		$VUFIL_ID = '0';
		$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
	}
	else
	{
		if(isset($_POST['sucBES_FIL']) and $_POST['sucBES_VUFIL']!= '')
		{
			$VUFIL_ID= intval($_POST['sucBES_VUFIL']);
			if($_POST['sucBES_VUFIL'] == '')
			{
				$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
			}
		}
		else
		{
			if(isset($_POST['sucBES_VUFIL']) and $_POST['sucBES_VUFIL'] == '')
			{
				$VUFIL_ID = '0';
				$Fehler = $AWISSprachKonserven['BES']['MSG_FILIALEINGABE'];
			}
			else
			{
				$VUFIL_ID = intval($rsErgebnis->FeldInhalt('VERURS_FILIALE'));
			}
		}
	
		if($FIL_ID == '0' and $Fehler == '')
		{
			$Fehler == 'keine g�ltige Fililale';
		}
	}
	
	$SQL  = ' Select * from( ';
	$SQL .= ' SELECT distinct substr(lpad(fil_id,4,0),-3) as FIL_ID,FIL_BEZ FROM Filialen ';
	$SQL .= ' where fil_gruppe IS NOT NULL OR';
	$SQL .= ' fil_id IN (8841,8877,8940,8971,8995,8996,8997,8999,8838)';
	$SQL .= ' union';
	$SQL .= ' SELECT substr(lpad(filialnr,4,0),-3) AS FIL_ID,filialname';
	$SQL .= ' FROM schaddev09.pseudofilialen)';
	$SQL .= ' where fil_id =0'.$VUFIL_ID;
	
	$rsFIL = $DB->RecordSetOeffnen($SQL);
		
	if($rsFIL->AnzahlDatensaetze() == 0 and $Fehler == '')
	{
		$Fehler = 'keine g�ltige Fililale';
	}
	
	$SQL2  = 'select * from v_filialebenenrollen_aktuell';
	$SQL2  .= ' inner join kontakte on xx1_kon_key = kon_key';
	$SQL2  .= ' where xx1_fil_id='.$VUFIL_ID;
	$SQL2  .= ' and xx1_fer_key = 25';
	
	$rsGBL = $DB->RecordSetOeffnen($SQL2);
	
	//position:absolute;left:388pt; top:213pt;
	
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Top = '16pt';
		$Left = '-187pt';
	}
	else
	{
		$Top = '17pt';
		$Left = '-188pt';
	}
	$Form->Erstelle_TextFeld('BES_VUFIL_NAME',$Fehler == '' ? $rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,250, false, '', '', '', 'T', 'L','','',50);
	$Form->Erstelle_TextFeld('BES_GBL',$Fehler == '' ? $rsGBL->FeldInhalt('KON_NAME1'):$Fehler, 40,250, false,'position:relative;left:'.$Left.'; top:'.$Top.';', '', '', 'T', 'L','','',50);
	
	$Inhalt = ob_get_contents();
	ob_end_clean();
	
	$Form->AuswahlBoxHuelle('box2','AuswahlListe','',$Inhalt);
	
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EINGANGPER'] . ':',100);
	$SQL2 ='select ID,MITTEL from eingangper ';
	$Form->Erstelle_SelectFeld('!BES_EINGANGPER',$rsErgebnis->FeldInhalt('EINGANGPER'),120,true,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_RSCHEINNR'] . ':',130);
	$Form->Erstelle_TextFeld('BES_RSCHEINNR',$AWIS_KEY1 == '-1'?'000000':$rsErgebnis->FeldInhalt('REKLASCHEINNR'), 15, 190,true, '','', '','', 'L','','',20,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GBL'] . ':',300);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EINGABEAM'] . ':',100);
	$Form->Erstelle_TextFeld('!BES_EINGABEAM',$AWIS_KEY1 == '-1'?date('d.m.Y',time()):$rsErgebnis->FeldInhalt('EINGABEAM'), 10, 150,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SB'] . ':',100);
	$SQL2 ='select ID,SBNAME from SACHBEARBEITER where ausgeschieden = 0 order by SBNAME';
	$Form->Erstelle_SelectFeld('!BES_BEARBEITER',$rsErgebnis->FeldInhalt('SACHBEARBEITER'),130,true,$SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','onchange="VorgangUebernehmen();key_BES_BEARBEITER_AB(event)"','',array(),'','SCHAD');

	if(($Recht40004&2)==2) {

		$aendern = true;

	}
	else
	{
		$aendern = false;
	}

	ob_start();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANZEIGEFIL'] . ':', 130);
	$Form->Erstelle_Checkbox('CHK_ANZEIGEFIL', $rsErgebnis->FeldInhalt('ANZEIGEFIL'), 65,$aendern,1);
	$Form->Erstelle_HiddenFeld('ANZEIGEFIL',$rsErgebnis->FeldInhalt('ANZEIGEFIL'));
	$Inhalt = ob_get_contents();
	ob_end_clean();

	$Form->AuswahlBoxHuelle('box_aktivFil','AuswahlListe','',$Inhalt);

	echo "<div style='display:none'>";
	$Form->AuswahlBox('BES_BEARBEITER_AB', 'box_aktivFil', '', 'BES_BEARBEITER_AB', '', 25, 10,
		isset($_POST['sucBES_BEARBEITER_AB'])?$_POST['sucBES_BEARBEITER_AB']:$rsErgebnis->FeldInhalt('ANZEIGEFIL'), 'T', true, '', '', '','', '', '', '', 0, '');
	echo "</div>";

	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_RVAXNR'] . ':',130);
	$Form->Erstelle_TextFeld('BES_RVAXNR',$rsErgebnis->FeldInhalt('REKLASCHEINVAX'), 10, 150,true, '','', '','', 'L','','',13,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANTRAGART'] . ':',90);

	ob_start();
	if(isset($_POST['txtBES_AART2']) and $_POST['txtBES_AART2'] == '')
	{
		$Form->Erstelle_TextFeld('BES_AART','keine g�ltige Gro�-Kd-Nr', 40,250, false,'', '', '', 'T', 'L','','',50);
		$Form->Erstelle_HiddenFeld('BES_AART2','');
	}
	elseif($rsErgebnis->FeldInhalt('ANTRAGART') == 18 or $rsErgebnis->FeldInhalt('ANTRAGART') == 20)
	{
		$SQL2 ='select ID,WERT from ANTRAGART where id in (18,20) order by wert ';
		$Form->Erstelle_SelectFeld('!BES_AART2',$rsErgebnis->FeldInhalt('ANTRAGART'),250,false,$SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
		$Form->Erstelle_HiddenFeld('BES_AART',$rsErgebnis->FeldInhalt('ANTRAGART'));
	}
	else
	{
		$SQL2 ='select ID,WERT from ANTRAGART where id not in (18,20) order by wert ';
		$Form->Erstelle_SelectFeld('!BES_AART',$rsErgebnis->FeldInhalt('ANTRAGART'),250,true,$SQL2,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	}
	$Inhalt = ob_get_contents();
	ob_end_clean();

	$Form->AuswahlBoxHuelle('box4','AuswahlListe','',$Inhalt);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GESCHLAM'] . ':',100);
	$Form->Erstelle_TextFeld('BES_GESCHLAM',isset($_POST['txtBES_GESCHLAM'])?$_POST['txtBES_GESCHLAM']:$rsErgebnis->FeldInhalt('AKTEGESCHLOSSENAM'), 10, 150,true, '','', '','D', 'L','','',10,'ondblclick="aktuellesDatum(this)";','','autocomplete=off','onKeyup;');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SDAT'] . ':',100);
	$Form->Erstelle_TextFeld('BES_SBDAT',$rsErgebnis->FeldInhalt('DATUMEREIGNIS'), 10, 130,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEARBNRFIL'] . ':',130);
	$Form->Erstelle_TextFeld('BES_BEARBNRFIL',$rsErgebnis->FeldInhalt('BEARBEITUNGSNRFIL'), 29, 279,false, '','', '','', 'L');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GEW'] . ':',50);
	$SQL3 ='select ID,WERT from GEWICHTUNG ';
	$Form->Erstelle_SelectFeld('BES_GEWICHTUNG',$rsErgebnis->FeldInhalt('GEWICHTUNG'),161,true,$SQL3,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	if($AWIS_KEY1 != '-1')
	{
		$SQL4 ='select * from WIEDERVORLAGEN_NEW  ';
		$SQL4 .= 'where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL4 .= ' and rownum = 1';
		$SQL4 .= ' order by WV_DATUM asc';
		$rsWV = $DBSCHAD->RecordSetOeffnen($SQL4);
		$WVDATUM = $Form->Format('D',$rsWV->FeldInhalt('WV_DATUM'));
	}
	else
	{
		$WVDATUM = '';
	}
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WVAM'] . ':',100);
	$Form->Erstelle_TextFeld('BES_WVAM',$WVDATUM,10, 150,false, '','', '','', 'L');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BESICHTIGT'] . ':',100);
	$Form->Erstelle_TextFeld('BES_BESICHTIGT',$rsErgebnis->FeldInhalt('BESICHTIGT'), 10, 110,true, '','', '','', 'L','','',20,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileEnde();
	
	if($Frame2)
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border: 1px solid black;','UEB2"ID="UEB2');
		$Form->Schaltflaeche('script', 'cmdKlappen2','onclick="EinAusklappen(2)"', '/bilder/icon_minus.png"ID="IMG_LOAD2','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_KUNDEKFZ'], 100, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:block','FRM2"ID="FRAME2');
	}
	else
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;','UEB2"ID="UEB2');
		$Form->Schaltflaeche('script', 'cmdKlappen2','onclick="EinAusklappen(2)"', '/bilder/icon_plus.png"ID="IMG_LOAD2','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_KUNDEKFZ'], 100, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:none','FRM2"ID="FRAME2');
	}
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANREDE'] . ':',130);
	$SQL5 ='select ID,NAME from anrede ';
	$Form->Erstelle_SelectFeld('BES_ANREDE',$rsErgebnis->FeldInhalt('GESCHLECHT'),380,true,$SQL5,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FABRIKAT'] . ':',110);
	$Form->Erstelle_TextFeld('BES_FABRIKAT',$rsErgebnis->FeldInhalt('FABRIKAT'), 73, 100,true, '','', '','', 'L','','',30,'','','autocomplete=off');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_NAME'] . ':',130);
	$Form->Erstelle_TextFeld('!BES_NAME',$rsErgebnis->FeldInhalt('KUNDENNAME'), 25, 380,true, '','', '','', 'L','','',50,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_MODELL'] . ':',110);
	$Form->Erstelle_TextFeld('BES_MODELL',$rsErgebnis->FeldInhalt('TYP'), 73, 190,true, '','', '','', 'L','','',30,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORNAME'] . ':',130);
	$Form->Erstelle_TextFeld('BES_VORNAME',$rsErgebnis->FeldInhalt('VORNAME'), 25, 380,true, '','', '','', 'L','','',50,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TYP'] . ':',110);
	$Form->Erstelle_TextFeld('BES_TYP',$rsErgebnis->FeldInhalt('KFZTYP'), 73, 190,true, '','', '','', 'L','','',60,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STRASSE'] . ':',130);
	$Form->Erstelle_TextFeld('BES_STRASSE',$rsErgebnis->FeldInhalt('STRASSE'), 25, 380,true, '','', '','', 'L','','',60,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KBA'] . ':',110);
	$Form->Erstelle_TextFeld('BES_KBA',$rsErgebnis->FeldInhalt('KBANR'), 73, 190,true, '','', '','', 'L','','',15);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_PLZORT'] . ':',130);
	$Form->Erstelle_TextFeld('BES_PLZ',$rsErgebnis->FeldInhalt('PLZ'), 3, 55,true, '','', '','', 'L','','',8,'','','autocomplete=off');
	$Form->Erstelle_TextFeld('BES_ORT',$rsErgebnis->FeldInhalt('ORT'), 25, 325,true, '','', '','', 'L','','',50,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EZ'] . ':',110);
	$Form->Erstelle_TextFeld('BES_EZ',$rsErgebnis->FeldInhalt('ERSTZULASSUNG'),10, 190,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON'] . ':',130);
	$Form->Erstelle_TextFeld('BES_TELEFON',$rsErgebnis->FeldInhalt('TELEFON'), 25, 380,true, '','', '','', 'L','','',35,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KM'] . ':',110);
	$Form->Erstelle_TextFeld('BES_KM',$rsErgebnis->FeldInhalt('KM'), 73, 190,true, '','', '','', 'L','','',10,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TELEFON2'] . ':',130);
	$Form->Erstelle_TextFeld('BES_TELEFON2',$rsErgebnis->FeldInhalt('TELEFON2'), 25, 380,true, '','', '','', 'L','','',35,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KFZKZ'] . ':',110);
	$Form->Erstelle_TextFeld('BES_KENNZ',$rsErgebnis->FeldInhalt('KFZ_KENNZ'), 73, 190,true, '','', '','', 'L','','',11,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_EMAIL'] . ':',130);
	$Form->Erstelle_TextFeld('BES_EMAIL',$rsErgebnis->FeldInhalt('EMAIL'), 25, 380,true, '','', '','', 'L','','',50,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FAX'] . ':',110);
	$Form->Erstelle_TextFeld('BES_FAX',$rsErgebnis->FeldInhalt('FAX'), 73, 190,true, '','', '','', 'L','','',25,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GROSSKDNR'] . ':',130);
	//$Form->Erstelle_TextFeld('BES_GROSSKDNR',$rsErgebnis->FeldInhalt('GROSSKDNR'), 25, 380,true, '','', '','', 'L',40,'','','','','autocomplete=off');
	$Form->AuswahlBox('BES_GROSSKDNR_D', 'box4','', 'BES_GKN', '', 380,25,isset($_POST['sucBES_GROSSKDNR_D'])?$_POST['sucBES_GROSSKDNR_D']:$rsErgebnis->FeldInhalt('GROSSKDNR'),'T',true,'','','',40,'','','',0,'','autocomplete=off','onInput;');
	$Form->Erstelle_HiddenFeld('oldBES_FIL',$rsErgebnis->FeldInhalt('FILNR'));
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ATUCARDNR'] . ':',110);
	$Form->Erstelle_TextFeld('BES_PAN',$rsErgebnis->FeldInhalt('PAN'), 73, 190,true, '','', '','', 'L','','',24,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileEnde();
	
	
	if($Frame3)
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border: 1px solid black;','UEB3"ID="UEB3');
		$Form->Schaltflaeche('script', 'cmdKlappen3','onclick="EinAusklappen(3)"', '/bilder/icon_minus.png"ID="IMG_LOAD3','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_AUFTRAG'],100, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:block','FRM3"ID="FRAME3');
	}
	else
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;','UEB3"ID="UEB3');
		$Form->Schaltflaeche('script', 'cmdKlappen3','onclick="EinAusklappen(3)"', '/bilder/icon_plus.png"ID="IMG_LOAD3','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_AUFTRAG'], 100, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:none','FRM3"ID="FRAME3');
	}
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUFTRAG'] . ':',130);
	$Form->Erstelle_TextFeld('BES_AUFTRAG',$rsErgebnis->FeldInhalt('BEZEICHNUNG'), 155, 200,true, '','', '','', 'L','','',255,'','','autocomplete=off');
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KENNUNG'] . ':',130);
	$SQL6 ='select KENNUNG,KENNUNG,null,KENNUNG_BEZEICHNUNG from KENNUNGEN where VERALTET = 0 order by KENNUNG_BEZEICHNUNG ';
	
	$Form->Erstelle_SelectFeld('BES_KENNUNG',($AWIS_KEY1 == '-1')?'FEHLT':(isset($_POST['txtBES_KENNUNG'])?$_POST['txtBES_KENNUNG']:$rsErgebnis->FeldInhalt('KENNUNG')),348,true,$SQL6,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_MECHANIKER'] . ':',80);
	$Form->Erstelle_TextFeld('BES_MECHANIKER',$rsErgebnis->FeldInhalt('MECHANIKER'), 20, 165,true, '','', '','', 'L','','',30,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_UAUFTRAG'] . ':',120);
	$SQL7 ='select ART_ID,AUFTRAGSART_ATU,null,AUFTRAGSART_ATU as Titel from AUFTRAGSARTEN where veraltet = 0 order by AUFTRAGSART_ATU ';
	$Form->Erstelle_SelectFeld('BES_AUFTRAGSART_NEU',$rsErgebnis->FeldInhalt('AUFTRAGSART_ATU_NEU'),'350:235',true,$SQL7,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUFTRAGSDATUM'] . ':',130);
	$Form->Erstelle_TextFeld('BES_AUFTRAGSDATUM',$rsErgebnis->FeldInhalt('AUFTRAGSDATUM'), 10, 165,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KAUFDATUM'] . ':',100);
	$Form->Erstelle_TextFeld('BES_KAUFDATUM',$rsErgebnis->FeldInhalt('KAUFDATUM'), 10, 220,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUWA'] . ':',100);
	$Form->Erstelle_TextFeld('BES_VUWA',$AWIS_KEY1 =='-1'?'0':$rsErgebnis->FeldInhalt('WANR'), 10, 163,true, '','', '','', 'L','','',14,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BONNR'] . ':',100);
	$Form->Erstelle_TextFeld('BES_BONNR',$rsErgebnis->FeldInhalt('BONNR'), 11, 165,true, '','', '','', 'L','','',40,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VUATUNR'] . ':',130);
	$Form->Erstelle_TextFeld('BES_ATUNR',$rsErgebnis->FeldInhalt('ATUNR'), 10, 135,true, '','', '','', 'L','','',6,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_LIEFERANT'] . ':',130);
	
	
	$Form->AuswahlBox('BES_LIEFERANT', 'box3','', 'BES_LIEF', '', 60,2,isset($_POST['sucBES_LIEFERANT'])?$_POST['sucBES_LIEFERANT']:$rsErgebnis->FeldInhalt('LIEFERANT'),'T',true,'','','',4,'','','',0,'','autocomplete=off','onKeyup;');

	$Inhalt = '';
	$Fehler = '';
	$LIE_NR = '';
	ob_start();
	
	
	if($rsErgebnis->AnzahlDatensaetze() == 0 and (!isset($_POST['sucBES_LIEFERANT']) or $_POST['sucBES_LIEFERANT'] == ''))
	{
		$LIE_NR = '0';
		$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
	}
	else
	{
		if(isset($_POST['sucBES_LIEFERANT']) and $_POST['sucBES_LIEFERANT']!= '')
		{
			$LIE_NR= intval($_POST['sucBES_LIEFERANT']);
			if($_POST['sucBES_LIEFERANT'] == '')
			{
				$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
			}
		}
		else
		{
			if(isset($_POST['sucBES_LIEFERANT']) and $_POST['sucBES_LIEFERANT'] == '')
			{
				$LIE_NR = '0';
				$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
			}
			else
			{
				$LIE_NR = intval($rsErgebnis->FeldInhalt('LIEFERANT'));
			}
		}
	
		if($LIE_NR == '0' and $Fehler == '')
		{
			$Fehler == 'kein g�ltiger Lieferant';
		}
	}
	
	$SQL  = ' Select * from LIEFERANTEN WHERE LIE_NR=\''.$rsErgebnis->FeldInhalt('LIEFERANT').'\'';
	
	$rsLIE = $DB->RecordSetOeffnen($SQL);
		
	if($rsLIE->AnzahlDatensaetze() == 0 and $Fehler == '')
	{
		$Fehler = $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
	}
	$Form->Erstelle_TextFeld('BES_LIENAME',$Fehler == '' ? $rsLIE->FeldInhalt('LIE_NAME1'):$Fehler, 200,423, false, '', '', '', 'T', 'L','','',50,'','','autocomplete=off');
	
	$Inhalt = ob_get_contents();
	ob_end_clean();
	
	$Form->AuswahlBoxHuelle('box3','AuswahlListe','',$Inhalt);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_LIEFARTNR'] . ':',100);
	$Form->Erstelle_TextFeld('BES_LIEFARTNR',$rsErgebnis->FeldInhalt('LIEFARTNR'), 11, 135,true, '','', '','', 'L','','',20,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_DATUMREG'] . ':',130);
	$Form->Erstelle_TextFeld('BES_DATUMREG',$rsErgebnis->FeldInhalt('DATUMREGULIERUNG'), 10, 142,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BONREG'] . ':',123);
	$Form->Erstelle_TextFeld('BES_BONNRREG',$rsErgebnis->FeldInhalt('BONNRREGULIERUNG'), 10, 483,true, '','', '','', 'L','','',20,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VERKAEUFER'] . ':',100);
	$Form->Erstelle_TextFeld('BES_VERKAEUFER',$rsErgebnis->FeldInhalt('VERK�UFER'), 11, 163,true, '','', '','', 'L','','',25,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileEnde();
	
	
	if($Frame4)
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border: 1px solid black;','UEB4"ID="UEB4');
		$Form->Schaltflaeche('script', 'cmdKlappen4','onclick="EinAusklappen(4)"', '/bilder/icon_minus.png"ID="IMG_LOAD4','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_FEHLERDIAG'], 76, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:block','FRM4"ID="FRAME4');
	}
	else
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;','UEB4"ID="UEB4');
		$Form->Schaltflaeche('script', 'cmdKlappen4','onclick="EinAusklappen(4)"', '/bilder/icon_plus.png"ID="IMG_LOAD4','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_FEHLERDIAG'], 76, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:none','FRM4"ID="FRAME4');
	}
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WORANARB'] . ':',130);
	$SQL7 ='select ID,GRUND from SCHADENSGRUND where veraltet = 0 order by Grund';
	$Form->Erstelle_SelectFeld('BES_WORANARB',$rsErgebnis->FeldInhalt('SCHADENSGRUND'),220,true,$SQL7,'~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WASBESCH'] . ':',130);
	$SQL8 ='select BID,SCHLAGWORT as Wert,null,SCHLAGWORT from BESCHAEDIGT where veraltet = 0 order by Schlagwort';
	
	$Form->Erstelle_SelectFeld('BES_WASBESCH',$rsErgebnis->FeldInhalt('BID'),'260:200',true,$SQL8,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','onmouseover="ladeSelectTitles(this.name, this.form)"','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUSFALLURSACHE'] . ':',63);
	$SQL9 ='select ID,WERT from AUSFALLURSACHE order by wert ';
	$Form->Erstelle_SelectFeld('BES_AUSFALLURSACHE',$rsErgebnis->FeldInhalt('AUSFALLURSACHE'),348,true,$SQL9,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FGLIEFERANT'] . ':',130);
	$SQL10 ='select ID,WERT from FGLIEFERANT ';
	$Form->Erstelle_SelectFeld('BES_FGLIEFERANT',$rsErgebnis->FeldInhalt('FGLIEFERANT'),220,true,$SQL10,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FGMARKE'] . ':',130);
	$SQL10 ='select ID,WERT from FGMARKE ';
	$Form->Erstelle_SelectFeld('BES_FGMARKE',$rsErgebnis->FeldInhalt('FGMARKE'),173,true,$SQL10,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ZUSATZINFO'] . ':',100);
	$Form->Erstelle_TextFeld('BES_ZUSATZINFO',$rsErgebnis->FeldInhalt('ZUSATZINFO'), 50, 190,true, '','', '','', 'L','','',500,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FEHLER'] . ':',130);

	$Form->Erstelle_Textarea('BES_FEHLER', $rsErgebnis->FeldInhalt('FEHLERBESCHREIBUNG'), 156, 116, 1,true,'','','onFocus="textareaAuf(this)"onFocusout="textareaZu(this)";');
	//$Form->Erstelle_TextFeld('BES_FEHLER',$rsErgebnis->FeldInhalt('FEHLERBESCHREIBUNG'), 156, 190,true, '','', '','', 'L',$rsErgebnis->FeldInhalt('FEHLERBESCHREIBUNG'),'',4000);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_DIAGWL'] . ':',130);
	$Form->Erstelle_Textarea('BES_DIAGWL', $rsErgebnis->FeldInhalt('DIAGNOSE_WERKSTATTLEITER'), 156, 116, 1,true,'','','onFocus="textareaAuf(this)"onFocusout="textareaZu(this)";');
	
	//$Form->Erstelle_TextFeld('BES_DIAGWL',$rsErgebnis->FeldInhalt('DIAGNOSE_WERKSTATTLEITER'), 156, 190,true, '','', '','', 'L','','',4000);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BESPMA'] . ':',130);
	$Form->Erstelle_Textarea('BES_BESPMA', $rsErgebnis->FeldInhalt('DIAGNOSE_TKDL'), 156, 116, 1,true,'','','onFocus="textareaAuf(this)"onFocusout="textareaZu(this)";');
	
	//$Form->Erstelle_TextFeld('BES_BESPMA',$rsErgebnis->FeldInhalt('DIAGNOSE_TKDL'), 156, 190,true, '','', '','', 'L','','',4000);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BEMINTERN'] . ':',130);
	$Form->Erstelle_Textarea('BES_BEMINTERN', $rsErgebnis->FeldInhalt('BEMINTERN'), 156, 116, 1,true,'','','onFocus="textareaAuf(this)"onFocusout="textareaZu(this)";');
	
	//$Form->Erstelle_TextFeld('BES_BEMINTERN',$rsErgebnis->FeldInhalt('BEMINTERN'), 156, 190,true, '','', '','', 'L','','',4000);
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_KONKUNDE'] . ':',130);
	$SQL10 ='select ID,WERT from KONTAKT_KUNDE ';
	$Form->Erstelle_SelectFeld('BES_KONKUNDE',$rsErgebnis->FeldInhalt('KONTAKT_KUNDE'),220,true,$SQL10,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TERMIN1'] . ':',130);
	$Form->Erstelle_TextFeld('BES_TERMIN1',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_1'), 10, 175,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WER1'] . ':',92);
	$Form->Erstelle_TextFeld('BES_WER1',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_WER_1'), 51, 120,true, '','', '','', 'L','','',50,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GBLINFO'] . ':',130);
	$Daten = explode('|',$AWISSprachKonserven['BES']['BES_LST_JANEIN']);
	$Form->Erstelle_SelectFeld('BES_GBLINFO',$rsErgebnis->FeldInhalt('INFO_AN_GBL'),220,true,'','~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_TERMIN2'] . ':',130);
	$Form->Erstelle_TextFeld('BES_TERMIN2',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_2'), 10, 175,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WER2'] . ':',92);
	$Form->Erstelle_TextFeld('BES_WER2',$rsErgebnis->FeldInhalt('TERMIN_KUNDE_WER_2'), 51, 120,true, '','', '','', 'L','','',50,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_STAND'].':', 130, '');
	$SQL2 ='select STANDID,STANDBEM from STAND where ORT=\'ATU\' and veraltet = 0 order by standbem ';
	$Form->Erstelle_SelectFeld('BES_STAND',($AWIS_KEY1 == '-1')?22:(isset($_POST['txtBES_STAND'])?$_POST['txtBES_STAND']:$rsErgebnis->FeldInhalt('STAND')),110,true,$SQL2,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->ZeileEnde();
	$Form->ZeileEnde();
	
	
	if($Frame5)
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border: 1px solid black;','UEB5"ID="UEB5');
		$Form->Schaltflaeche('script', 'cmdKlappen5','onclick="EinAusklappen(5)"', '/bilder/icon_minus.png"ID="IMG_LOAD5','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_FORDERUNG'], 76, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:block','FRM5"ID="FRAME5');
	}
	else
	{
		$Form->ZeileStart('position:relative;width:79%;background-color:#848484;height:18px;border-top: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;','UEB5"ID="UEB5');
		$Form->Schaltflaeche('script', 'cmdKlappen5','onclick="EinAusklappen(5)"', '/bilder/icon_plus.png"ID="IMG_LOAD5','Ein-/Ausklappen', 'W','width:30px',17,17);
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['UEBERSCHRIFT_FORDERUNG'], 76, 'color:#FFFFFF;font-weight:bolder');
		$Form->ZeileEnde();
		$Form->ZeileStart('display:none','FRM5"ID="FRAME5');
	}
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WHERSTELLER'] . ':',130);
	$Form->Erstelle_TextFeld('BES_WHERSTELLER',$rsErgebnis->FeldInhalt('WEITERGELEITET_HERSTELLER'), 10, 223,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	if($AWIS_KEY1 != '-1')
	{
		$SQL11='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL11.=' and bid=1';
		$rsHerst = $DBSCHAD->RecordSetOeffnen($SQL11);
		if($rsHerst->FeldInhalt('BETRAG') == '')
		{
			$HBetrag = 0;
		}
		else
		{
			$HBetrag = $rsHerst->FeldInhalt('BETRAG');
		}
	}
	else
	{
		$HBetrag = 0;
	}
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRHER'] . ':',130);
	$Form->Erstelle_TextFeld('BES_BETRHER',$Form->Format('N2',$HBetrag).'&euro;', 25, 250,false,'','', '','T', 'L','','','','','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_ANWALT'] . ':',80);
	$Form->Erstelle_Checkbox('CHK_AKTIV',$rsErgebnis->FeldInhalt('ANWALTSSACHE'),65,true,-1);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AZ'] . ':',100);
	$Form->Erstelle_TextFeld('BES_AZ',$rsErgebnis->FeldInhalt('AKTENZEICHENRA'), 11, 165,true, '','', '','', 'L','','',30,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_WVERS'] . ':',130);
	$Form->Erstelle_TextFeld('BES_WVERS',$rsErgebnis->FeldInhalt('WEITERGELEITETVERSICHERUNG'), 10, 223,true, '','', '','D', 'L','','',10,'','','autocomplete=off');
	if($AWIS_KEY1 != '-1')
	{
		$SQL12='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL12.=' and bid=2';
		$rsVERS = $DBSCHAD->RecordSetOeffnen($SQL12);
		if($rsVERS->FeldInhalt('BETRAG') == '')
		{
			$VBetrag = 0;
		}
		else
		{
			$VBetrag = $rsVERS->FeldInhalt('BETRAG');
		}
	}
	else
	{
		$VBetrag = '0';
	}
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRVERS'] . ':',130);
	$Form->Erstelle_TextFeld('BES_BETRVERS',$Form->Format('N2',$VBetrag).'&euro;', 25, 395,false, '','', '','', 'L','','','','','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_RAFORD'] . ':',100);
	$Form->Erstelle_TextFeld('BES_RAFORDERUNG',$Form->Format('N2',$AWIS_KEY1=='-1'?0:$rsErgebnis->FeldInhalt('RAFORDERUNG')).'&euro;', 11, 165,true, '','', '','', 'L','','',13,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	if($AWIS_KEY1 != '-1')
	{
		$SQL13='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL13.=' and bid=6';
		$rsVORK = $DBSCHAD->RecordSetOeffnen($SQL13);
		if($rsVORK->FeldInhalt('BETRAG') == '')
		{
			$VORBetrag = 0;
		}
		else
		{
			$VORBetrag = $rsVORK->FeldInhalt('BETRAG');
		}
	}
	else 
	{
		$VORBetrag = '0';
	}
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_VORKATU'] . ':',130);
	$Form->Erstelle_TextFeld('BES_VORKATU',$Form->Format('N2',$VORBetrag).'&euro;', 25, 223,false, '','', '','', 'L','','','','','','autocomplete=off');
	if($AWIS_KEY1 != '-1')
	{
		$SQL14='select sum(betrag) as betrag from zahlungen where bearbeitungsnr='.$rsErgebnis->FeldInhalt('BEARBEITUNGSNR');
		$SQL14.=' and bid=3';
		$rsVORK = $DBSCHAD->RecordSetOeffnen($SQL14);
		if($rsVORK->FeldInhalt('BETRAG') == '')
		{
			$BETBetrag = 0;
		}
		else
		{
			$BETBetrag = $rsVORK->FeldInhalt('BETRAG');
		}
	}
	else 
	{
		$BETBetrag = '0';
	}
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_BETRAGATU'] . ':',130);
	$Form->Erstelle_TextFeld('BES_BETRAGATU',$Form->Format('N2',$BETBetrag).'&euro;', 25, 395,false, '','', '','', 'L','','','','','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_GERICHT'].':', 100, '');
	$SQL15 ='select STANDID,STANDBEM from STAND where ORT=\'Gericht\' and veraltet = 0 order by STANDBEM ';
	$Form->Erstelle_SelectFeld('BES_GERICHT',$rsErgebnis->FeldInhalt('STANDBEIGERICHT'),110,true,$SQL15,'0~' .$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),'','SCHAD');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_FORDERUNG'] . ':',130);
	$Form->Erstelle_TextFeld('BES_FORDERUNG',$Form->Format('N2',$AWIS_KEY1 == '-1'?0:$rsErgebnis->FeldInhalt('FORDERUNG')).'&euro;', 25, 223,true, '','', '','', 'L','','',13,'','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SUMSCHAD'] . ':',130);
	$Form->Erstelle_TextFeld('BES_BETRAGATU',$Form->Format('N2',str_replace(',','.',$HBetrag)+str_replace(',','.',$VBetrag)+str_replace(',','.',$BETBetrag)).'&euro;', 25, 395,false, '','', '','', 'L','','','','','','autocomplete=off');
	$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AZGERICHT'] . ':',100);
	$Form->Erstelle_TextFeld('BES_AZGERICHT',$rsErgebnis->FeldInhalt('AKTENZEITENGERICHT'), 11, 165,true, '','', '','', 'L','','',30,'','','autocomplete=off');
	$Form->ZeileEnde();
	$Form->ZeileEnde();
	
$Form->Erstelle_HiddenFeld('BEARBEITUNGSNR',$AWIS_KEY1);
$Form->Erstelle_HiddenFeld('BEARBNRNEU',$rsErgebnis->FeldInhalt('BEARBNRNEU'));

}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>