<?php

global $AWISCursorPosition;
global $AWISBenutzer;

try {

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FIL', '%');
    $TextKonserven[] = array('Wort', 'ANZAHL_PAL');
    $TextKonserven[] = array('Wort', 'FILIALSUCHE');
    $TextKonserven[] = array('Wort', 'FREIER_TEXT');
    $TextKonserven[] = array('Wort', 'EINHEIT');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'DatumVom');
    $TextKonserven[] = array('Wort', 'DatumBis');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'ttt_AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht46000 = $AWISBenutzer->HatDasRecht(46000);
    if ($Recht46000 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    if (!isset($Param['SPEICHERN'])) {
        $Param['SPEICHERN'] = 'off';
    }
    
    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./palettenscheine_Main.php?cmdAktion=Suche>");

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['FILIALSUCHE'],150);
    $Form->AuswahlBox('!PAL_FIL_ID', 'box1', '', 'PAL_Daten', '',  150,  20, isset($_POST['txtPAL_FIL_ID'])?$_POST['txtPAL_FIL_ID']:'', 'T', true,
        '', '', '', 0, '', '', '', 0, '', 'autocomplete=off');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['ANZAHL_PAL'],150);
    $Form->Erstelle_TextFeld('!PAL_ANZ','','20',150,true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['EINHEIT'],150);
    $Form->Erstelle_TextFeld('PAL_EINHEIT','Paletten','20',150,false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['FREIER_TEXT'],150);
    $Form->Erstelle_TextFeld('PAL_TEXT','','40',150,true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->AuswahlBoxHuelle('box1');
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    //************************************************************
    //* Schaltflaechen
    //************************************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Form->SchaltflaechenEnde();

    if($AWISCursorPosition!='')
    {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
        echo '</Script>';
    }
}
catch (awisException $ex) {
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
    }
    else
    {
        echo 'AWIS-Fehler:'.$ex->getMessage();
    }
}
catch (Exception $ex) {
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201405091111");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}

?>