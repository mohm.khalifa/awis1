<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("phpCheckTime.class.php");
global $AWISBenutzer;
print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $ResultZeile;

$RechteStufe = awisBenutzerRecht($con, 10);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Admin: DB_Paramter',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}


$awis_link='';
$db = awisDBServer();
/*
if (!empty($_GET["txtDB"]))
{
	$awis_link = "@" . strtolower($_GET["txtDB"]) . ".atu.de";
	$db = strtolower($_GET["txtDB"]);
}
else
{
	$db = strtolower(awisDBServer());
	$awis_link= "@" . $db . ".atu.de";
}
*/

echo '<form name=frmSuche method=get action=' . $_SERVER["PHP_SELF"] . '>';
echo '<input type="hidden" name="cmdAktion" value="Parameter">';

echo '<br>';
echo '<table border=1>';
echo '<tr><td id=FeldBez colspan=4>';
echo '<h3>Datenbanken</h3></td></tr>';
echo '<tr>';
echo '<td id=FeldBez><u>D</u>atenbank</td>';
echo '<td colspan=3><input name=txtDB readonly accesskey=d value="'.awisDBServer().'"></td>';
// Keine Auswahl mehr, SK 14.02.2008
/*
echo '<td colspan=3><select accesskey=d name="txtDB">';

$rsResult = awisOpenRecordset($con,'SELECT * from mcp.datenbanken');
$rsResultZeilen = $awisRSZeilen;

for($DBNr=0;$DBNr<$rsResultZeilen;$DBNr++)
{
	echo '<option value="' . $rsResult['DBK_NAME'][$DBNr] . '"';
	if ($db == strtolower($rsResult['DBK_NAME'][$DBNr]))
	{
		echo ' selected="selected"';
	}
	echo '>' . $rsResult['DBK_NAME'][$DBNr] . '</option>';
}

echo '</select></td>';
*/
echo '</tr>';
echo '</table>';
print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo '</form>';

if(isset($_REQUEST['cmdSuche_x'])||$awis_link!='')
{

/*
 ###########################################################
 #
 #	Session-Parameter
 #
 ###########################################################
 */
print "<h1 id=SeitenTitel>Session-Parameter</h1>";

$SQL = "Select * ";
$SQL .= "From NLS_SESSION_PARAMETERS".$awis_link;
$rsResult = awisOpenRecordset($con, $SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_parameter.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;

print "<table id=DatenTabelle width=100% border=1>";
print "<tr>";
print "<td id=FeldBez>Parameter</td>";
print "<td id=FeldBez>Wert</td>";
print "</tr>";
	
for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
{
	echo '<tr>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['PARAMETER'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['VALUE'][$ResultZeile] . '</td>';
	echo '</tr>';
}
	
print "</table>";
print "<br>";

/*
 ###########################################################
 #
 #	Datenbank-Parameter
 #
 ###########################################################
 */

print "<h1 id=SeitenTitel>Database-Parameter</h1>";

$SQL = "Select * ";
$SQL .= "From NLS_DATABASE_PARAMETERS".$awis_link;
$rsResult = awisOpenRecordset($con, $SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_parameter.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;

print "<table id=DatenTabelle width=100% border=1>";
print "<tr>";
print "<td id=FeldBez>Parameter</td>";
print "<td id=FeldBez>Wert</td>";
print "</tr>";
	
for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
{
	echo '<tr>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['PARAMETER'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['VALUE'][$ResultZeile] . '</td>';
	echo '</tr>';
}
	
print "</table>";
print "<br>";

/*
 ###########################################################
 #
 #	V$NLS_PARAMETERS
 #
 ###########################################################
 */

print "<h1 id=SeitenTitel>V\$NLS_PARAMETERS</h1>";

$SQL = "Select * ";
$SQL .= "From V\$NLS_PARAMETERS ";
$rsResult = awisOpenRecordset($con, $SQL);

if($rsResult==FALSE)
{
	awisErrorMailLink("db_parameter.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsResultZeilen=$awisRSZeilen;

print "<table id=DatenTabelle width=100% border=1>";
print "<tr>";
print "<td id=FeldBez>Parameter</td>";
print "<td id=FeldBez>Wert</td>";
print "</tr>";
	
for($ResultZeile=0;$ResultZeile<$rsResultZeilen;$ResultZeile++)
{
	echo '<tr>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['PARAMETER'][$ResultZeile] . '</td>';
	echo '<td ' . (($ResultZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsResult['VALUE'][$ResultZeile] . '</td>';
	echo '</tr>';
}
	
print "</table>";

unset($rsResult);
}
?>

