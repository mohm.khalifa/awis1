/*
Hinzuf�gen der neuen Tabelenspalten
 */

ALTER TABLE PRAKTIKUMSVEREINBARUNG ADD PVS_EIGNUNG number;
COMMENT ON COLUMN PRAKTIKUMSVEREINBARUNG.PVS_EIGNUNG IS 'Die Eignung des Praktikanten; 0= nicht geeignet, 1= historisch (Damit Sortierung in Mitte), 2 = Geeignet';
ALTER TABLE PRAKTIKUMSVEREINBARUNG ADD PVS_MAILVERSAND number;
COMMENT ON COLUMN PRAKTIKUMSVEREINBARUNG.PVS_MAILVERSAND IS 'Hier wird angegeben, ob die Mail am letzten Tag des Praktikums versand wurde oder nicht; 1 = versand/erstellt vor Erinnerungssystem, 0 = nicht versand';

/*
  Textkonserven von Mail + Maske
 */

INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Praktikant f�r Beruf geeignet','PVS_Eignung','PVS','Praktikant f�r Beruf geeignet');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Eig','PVS_Eignung_Short','PVS','Praktikant f�r Beruf geeignet K�rzel');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Hier klicken um nach der Eignung zu sortieren.  Erneut klicken, um die Sortierreihenfolge umzukehren.','ttt_Eignung','PVS','Tooltip: Listenfeld Eignung');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Es wurde noch kein Wert gesetzt.','PVS_Eignung_Leer','PVS','Bezeichnung, wenn Eignung leer oder null ist');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('0~Nein|2~Ja','lst_Eignung','PVS','Listenwerte: 2: Ja, 0: Nein, (1 = Historisch, keine Anzeige bei Selectfeld');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Suchmaske:','PVS_Hauptueberschrift_Suche','PVS','Haupt�berschrift in Suchenmaske');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Ihre Praktikantin Frau','mail_Erinnerung_BetreffFrau','PVS','Betreff bei einer weiblichen Person bei der Erinnerungsmail');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Ihr Praktikant Herr','mail_Erinnerung_BetreffHerr','PVS','Betreff bei einer maennlichen Person bei der Erinnerungsmail');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Herr','mail_Erinnerung_AnredeHerr','PVS','Anrede bei einer maennlichen Person bei der Erinnerungsmail');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Frau','mail_Erinnerung_AnredeFrau','PVS','Anrede bei einer weiblichen Person bei der Erinnerungsmail');

/*
Textkonserven der pdf-Datei
 */

INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('An die Sorgeberechtigten des Kindes','pdf_Sorgeberechtigte','PVS','�berschrift beim Empf�nger');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('m�chte sich bitte in der Filiale bei','pdf_InDerFiliale','PVS','Teil des Mittelteils der Best�tigungsseite');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('melden.','pdf_Melden','PVS','Das letzte Wort beim Mittelteil der Best�tigungsseite');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('/daten/web/dokumente/vorlagen/BoysGirlsDay.pdf','pdf_Dateipfad','PVS','Dateipfad der Vorlage zur Best�tigungsseite');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Praktikum im Rahmen des Girls / Boys-Day von ','pdf_GBDUeberschrift','PVS','�berschrift bei der Best�tigungsseite');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Praktikum im Rahmen des freiwilligen Praktikum von ','pdf_FreiUeberschrift','PVS','�berschrift bei der Best�tigungsseite');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Praktikum im Rahmen des OiB von ','pdf_OiBUeberschrift','PVS','�berschrift bei der Best�tigungsseite');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Praktikum im Rahmen der Berufsvorbereitenden Ma�nahme von ','pdf_BvbUeberschrift','PVS','�berschrift bei der Best�tigungsseite');
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Praktikum im Rahmen der Lehrstellenbewerbung von ','pdf_LsbUeberschrift','PVS','�berschrift bei der Best�tigungsseite');

/*
Berichteparameter
 */

INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('8','StartSeite4','50','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('11','StartSeite4','50','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('6','StartSeite5','50','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('13','StartSeite5','50','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('72','StartSeite6','50','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('6','BreiteAussen','19','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('8','BreiteAussen','19','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('11','BreiteAussen','19','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('13','BreiteAussen','19','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('6','Schriftklein','9','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('8','Schriftklein','9','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('11','Schriftklein','9','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('13','Schriftklein','9','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('6','Zellengroesse','45','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('8','Zellengroesse','45','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('11','Zellengroesse','45','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('13','Zellengroesse','45','Andr� Meyer');
INSERT INTO BERICHTEPARAMETER (XRP_XRE_KEY,XRP_PARAMETER,XRP_WERT,XRP_USER) VALUES ('72','Zellengroesse','45','Andr� Meyer');

/*
Rechtestufe
 */

INSERT INTO RECHTESTUFEN (XRS_XRC_ID, XRS_BIT,XRS_BESCHREIBUNG) VALUES ('18000','6','Praktikumsvereinbarung Eignungs-Spalte anzeigen');
INSERT INTO RECHTESTUFEN (XRS_XRC_ID, XRS_BIT,XRS_BESCHREIBUNG) VALUES ('18000','7','Alle Suchfelder');

INSERT INTO PROGRAMMPARAMETER (XPP_ID,XPP_BEZEICHNUNG,XPP_DEFAULT,XPP_UEBERSCHREIBBAR,XPP_BEMERKUNG,XPP_KODIERT,XPP_DATENTYP) VALUES ('711','PVS_MAIL_ABSENDER','jenny.froebel@de.atu.eu',255,'Absender der E-Mails an die Filialen',0,'T');

/*
Weitere Textkonserven von Mail + Maske, die jedoch bestimmte Sonderzeichen enthalten
 */

SET DEFINE OFF;
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('https://awis.server.atu.de/praktikumsvereinbarung/praktikum_Main.php?cmdAktion=Details&PVS_KEY=','mail_Erinnerung_Link','PVS','Der eingesetzte Link bei der Erinnerungsmail');
SET DEFINE OFF;
INSERT INTO TEXTKONSERVEN (XTX_TEXT_DE,XTX_KENNUNG,XTX_BEREICH, XTX_BEMERKUNG) values ('Sehr geehrter GL / WL,

in der Zeit vom #PVS_VON# bis #PVS_BIS# hat #PVS_ANREDE# #PVS_NAME# ein Praktikum in Ihrer Filiale absolviert.

Da heute der letzte Tag des Praktikums ist, m�chten wir gern ein Feedback zum Verlauf des Praktikums von Ihnen haben. Insbesondere m�chten wir gern nachhalten, ob der Praktikant f�r eine m�gliche Ausbildung geeignet ist oder nicht.

Bitte geben Sie uns unter folgendem Link ihr Feedback: #PVS_LINK#

Wir danken Ihnen f�r die Unterst�tzung und w�nschen weiterhin viel Erfolg.

Mit freundlichen Gr��en

A.T.U Auto-Teile-Unger GmbH & Co. KG
Tel.:	+49 (0)961 306-5627
Fax:	+49 (0)961 306-5867
Mail:	jenny.froebel@de.atu.eu



i. A. Jenny Fr�bel
Koordinatorin Ausbildung
Personalentwicklung','PVS_MAILVERSAND_TEXT','PVS','Der Text der Versand werden soll. Enth�lt Platzhalter');