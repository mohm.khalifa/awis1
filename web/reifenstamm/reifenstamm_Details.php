<?php

/**
 * 
 * @author Henry Ott
 * @copyright ATU Auto Teile Unger
 * @version 20130430
 */
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('RST', '%');
    $TextKonserven[] = array('RRP', '%');
    $TextKonserven[] = array('REH', '%');
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('XDI', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[]=array('Liste','lst_JaNein');
    $TextKonserven[]=array('Liste','lst_ROLLRES');
    $TextKonserven[]=array('Liste','lst_WETGRIP');
    $TextKonserven[]=array('Liste','lst_NOISECL');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht15300 = $AWISBenutzer->HatDasRecht(15300);
    if ($Recht15300 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Form->DebugAusgabe(1, $_POST);
    $Form->DebugAusgabe(1, $_GET);
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_RST"));
	
    if (isset($_POST['cmdSuche_x']))
    {
        $Param['KEY'] = 0;
        $Param['ORDER'] = '';
        $Param['BLOCK'] = '';
        
        $Param['RST_LIE_NR'] = $_POST['sucRST_LIE_NR'];
        $Param['RST_LARTNR'] = $_POST['sucRST_LARTNR'];
        $Param['RST_BREITE'] = $_POST['sucRST_BREITE'];
        $Param['RST_QUERSCHNITT'] = $_POST['sucRST_QUERSCHNITT'];
        $Param['RST_BAUART'] = $_POST['sucRST_BAUART'];
        $Param['RST_INNENDURCHMESSER'] = $_POST['sucRST_INNENDURCHMESSER'];
        $Param['RST_LLKW'] = $_POST['sucRST_LLKW'];
        $Param['RST_LOADINDEX'] = $_POST['sucRST_LOADINDEX'];
        $Param['RST_LOADINDEX2'] = $_POST['sucRST_LOADINDEX2'];
        $Param['RST_LOADINDEX3'] = $_POST['sucRST_LOADINDEX3'];
        $Param['RST_RF'] = $_POST['sucRST_RF'];
        $Param['RST_SPEEDINDEX'] = $_POST['sucRST_SPEEDINDEX'];
        $Param['RST_SPEEDINDEX2'] = $_POST['sucRST_SPEEDINDEX2'];
        $Param['RST_REH_KEY'] = $_POST['sucRST_REH_KEY'];
        $Param['RST_BEZEICHNUNG'] = $_POST['sucRST_BEZEICHNUNG'];
        $Param['RST_TYP'] = $_POST['sucRST_TYP'];
        $Param['RST_TYP2'] = $_POST['sucRST_TYP2'];
        $Param['RST_TYP3'] = $_POST['sucRST_TYP3'];
        $Param['RST_ROLLRES'] = $_POST['sucRST_ROLLRES'];
        $Param['RST_WETGRIP'] = $_POST['sucRST_WETGRIP'];
        $Param['RST_NOISEPE'] = $_POST['sucRST_NOISEPE'];
        $Param['RST_NOISECL'] = $_POST['sucRST_NOISECL'];
        $Param['RST_IMPORTSTATUS'] = $_POST['sucRST_IMPORTSTATUS'];
        $Param['RST_STATUS'] = $_POST['sucRST_STATUS'];
        $Param['GELISTET'] = $_POST['sucGELISTET'];
        $Param['RRP_ATUNR'] = $_POST['sucRRP_ATUNR'];
        $Param['RST_EAN'] = $_POST['sucRST_EAN'];
        
        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');

		$AWISBenutzer->ParameterSchreiben("Formular_RST",serialize($Param));
    }
    elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
        include('./reifenstamm_loeschen.php');
    }
    elseif (isset($_POST['cmdSpeichern_x']) or isset($_GET['ok']))
    {
        include('./reifenstamm_speichern.php');
    }
    elseif (isset($_POST['cmdDSNeu_x']))
    {
        $AWIS_KEY1 = -1;
    }
    elseif (isset($_GET['RST_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['RST_KEY']);
        if (isset($_GET['Wiedervorlage']))
	    {
	    	$Param = array();
	    	$Param['KEY']=0;
	    	$Param['WHERE']='';
	    	$Param['ORDER']='';
	    }
    }
    else   // Letzten Benutzer suchen
    {
		if(!isset($Param['KEY']))
		{
			$Param['KEY']=0;
			$Param['WHERE']='';
			$Param['ORDER']='';
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (!isset($_GET['Sort']))
    {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '')
        {
            $ORDERBY = $Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' REH_BEZEICHNUNG, RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
            $Param['ORDER']= $ORDERBY;
        }
    }
    else
    {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        $Param['ORDER']= $ORDERBY;
    }

    if (isset($_GET['SortExt']))
    {
    	$ORDERBY .= ','.$_GET['SortExt'];
    }
    
    //********************************************************
    // Bedingung erstellen
    //********************************************************
    $BindeVariablen = array();
    $Bedingung = _BedingungErstellen($Param, $BindeVariablen);
    
	//********************************************************
	// Daten suchen
	//********************************************************
    $SQL  = 'SELECT RST_KEY,';
    $SQL .= ' RST_BEZEICHNUNG,';
    $SQL .= ' RST_REH_KEY,';
    $SQL .= ' REH_BEZEICHNUNG,';
    $SQL .= ' RST_LIE_NR,';
    $SQL .= ' RST_LARTNR,';
    $SQL .= ' RST_BREITE,';
    $SQL .= ' RST_QUERSCHNITT,';
    $SQL .= ' RST_BAUART,';
    $SQL .= ' RST_INNENDURCHMESSER,';
    $SQL .= ' RST_LLKW,';
    $SQL .= ' RST_LOADINDEX,';
    $SQL .= ' RST_LOADINDEX2,';
    $SQL .= ' RST_LOADINDEX3,';
    $SQL .= ' RST_RF,';
    $SQL .= ' RST_SPEEDINDEX,';
    $SQL .= ' RST_SPEEDINDEX2,';
    $SQL .= ' RST_ZUSATZBEMERKUNG,';
    $SQL .= ' RST_ZUSATZBEMERKUNG2,';
    $SQL .= ' RST_ZUSATZBEMERKUNG3,';
    $SQL .= ' RST_TYP,';
    $SQL .= ' RST_TYP2,';
    $SQL .= ' RST_TYP3,';
    $SQL .= ' RST_ROLLRES,';
    $SQL .= ' RST_WETGRIP,';
    $SQL .= ' RST_NOISEPE,';
    $SQL .= ' RST_NOISECL,';
    $SQL .= ' RST_KBPREIS,';
    $SQL .= ' RST_EAN,';
    $SQL .= ' RST_FELGENRIPPE,';
    $SQL .= ' RST_XDI_KEY,';
    $SQL .= ' RST_IMPORTSTATUS,';
    $SQL .= ' RST_STATUS,';
    $SQL .= ' RST_BEMERKUNG,';
    $SQL .= ' RST_WINTERTAUGLICHKEIT,';
    $SQL .= ' RST_EISGRIFF,';
    $SQL .= ' RST_INFO,';
    $SQL .= ' RST_URL5,';
    $SQL .= ' RST_URL4,';
    $SQL .= ' RST_USER,';
    $SQL .= ' RST_USERDAT,';
    $SQL .= ' RRP_KEY,';
    $SQL .= ' RRP_ATUNR,';
    $SQL .= ' RRP_AST_KEY,';
    $SQL .= ' RRP_KENN1,';
    $SQL .= ' RRP_KENN2,';
    $SQL .= ' RRP_KENN_L2,';
    $SQL .= ' XDI_DATUM,';
    $SQL .= ' XDI_DATEINAME';
    if ($AWIS_KEY1 <= 0)
    {
        $SQL .= ' ,row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr';
    }

    $SQL .= ' FROM REIFENSTAMM';
    $SQL .= ' LEFT JOIN REIFENPFLEGE ON RST_KEY = RRP_RST_KEY';
    $SQL .= ' INNER JOIN REIFENHERSTELLER ON REH_KEY = RST_REH_KEY';
    $SQL .= ' LEFT OUTER JOIN Importprotokoll ON RST_XDI_KEY = XDI_KEY';
    
    
    if ($Bedingung != '')
    {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }

    $SQL .= ' ORDER BY ' . $ORDERBY;
    
    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    $Block = 1;
    if (isset($_REQUEST['Block']))
    {
        $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        $Param['BLOCK'] = $Block;
    }
    elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
    {
        $Block = intval($Param['BLOCK']);
    }

    //************************************************
    // Zeilen begrenzen
    //************************************************
    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);

    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    if ($AWIS_KEY1 <= 0)
    {
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    }
    $rsRST = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->SchreibeHTMLCode('<form name=frmReifenstamm action=./reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . '' . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . ' method=post>');
    
    if ($rsRST->EOF() AND !isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=-1)  // Keine Meldung bei neuen Datens�tzen!
    {
        echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';

	    //***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht15300 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	    $Form->SchaltflaechenEnde();
    }
    elseif (($rsRST->AnzahlDatensaetze() > 1) or (isset($_GET['Liste'])))      // Liste anzeigen
    {
        $Form->Formular_Start();

        $Form->ZeileStart('font-size:10pt');

        $Link = './reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=REH_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'REH_BEZEICHNUNG')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['REH']['REH_BEZEICHNUNG'], 120, '', $Link);
        
        $Link = './reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RST_LIE_NR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RST_LIE_NR')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_LIE_NR'], 70, '', $Link);

        $Link = './reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RST_LARTNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RST_LARTNR')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_LARTNR'], 140, '', $Link);
        
        $Link = './reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RRP_ATUNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_ATUNR')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_ATUNR'], 70, '', $Link);
        
        $Link = './reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RST_BREITE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RST_BREITE')) ? '~' : '');
        $Link .= '&SortExt=RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['txt_Reifenkennzeichen'],283, '', $Link);

        $Link = './reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&Sort=RST_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RST_BEZEICHNUNG')) ? '~' : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RST']['RST_BEZEICHNUNG'],280, '', $Link);

        $Link = './reifenstamm_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
        $Link .= '&SortExt=RST_BREITE,RST_QUERSCHNITT,RST_BAUART,RST_INNENDURCHMESSER,RST_LOADINDEX';
        $Link .= '&Sort=RRP_KENN1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRP_KENN1')) ? '~' : '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_KENN1_kurz'],60, '', $Link);

        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRP']['RRP_KENN2_kurz'], 60);
        $Form->ZeileEnde();
        
        $DS = 0;

        while (!$rsRST->EOF())
        {
            $Form->ZeileStart('font-size:10pt');
            $Link = './reifenstamm_Main.php?cmdAktion=Details&RST_KEY=' . $rsRST->FeldInhalt('RST_KEY') . '';
            $Form->Erstelle_ListenFeld('REH_BEZEICHNUNG', $rsRST->FeldInhalt('REH_BEZEICHNUNG'), 0, 120, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('RST_LIE_NR', $rsRST->FeldInhalt('RST_LIE_NR'), 0, 70, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('RST_LARTNR', $rsRST->FeldInhalt('RST_LARTNR'), 0, 140, false, ($DS % 2), '', $Link, 'T');
            $Form->Erstelle_ListenFeld('RRP_ATUNR', $rsRST->FeldInhalt('RRP_ATUNR'), 0, 70, false, ($DS % 2), '', '', 'T');
            $Form->Erstelle_ListenFeld('RST_BREITE', $rsRST->FeldInhalt('RST_BREITE'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
            $Form->Erstelle_ListenFeld('RST_QUERSCHNITT', $rsRST->FeldInhalt('RST_QUERSCHNITT'), 0, 30, false, ($DS % 2), '', '', 'T', 'L', '');
            $Form->Erstelle_ListenFeld('RST_BAUART', $rsRST->FeldInhalt('RST_BAUART'), 0, 30, false, ($DS % 2), '', '', 'T', 'L', '');
            $Form->Erstelle_ListenFeld('RST_INNENDURCHMESSER', $rsRST->FeldInhalt('RST_INNENDURCHMESSER'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
            $Form->Erstelle_ListenFeld('RST_LOADINDEX', $rsRST->FeldInhalt('RST_LOADINDEX'), 0, 30, false, ($DS % 2), '', '', 'T', '', '');
            $Form->Erstelle_ListenFeld('RST_SPEEDINDEX', $rsRST->FeldInhalt('RST_SPEEDINDEX'), 0, 30, false, ($DS % 2), '', '', 'T', '', '');
			
            $Zusatz = '';
            if ($rsRST->FeldInhalt('RST_RF')!='')
            {
            	$Zusatz  = $rsRST->FeldInhalt('RST_RF');
            	if ($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
            	{
            		$Zusatz .= '/'.$rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG');
            	}
            }
            elseif ($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
            {
            	$Zusatz = $rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG');
            }
            
            $Form->Erstelle_ListenFeld('ZUSATZ',$Zusatz, 0, 80, false, ($DS % 2), '', '', 'T', '', '');
            
            $Form->Erstelle_ListenFeld('RST_BEZEICHNUNG', $rsRST->FeldInhalt('RST_BEZEICHNUNG'), 0, 280, false, ($DS % 2), '', '', 'T', '', '');
            $Form->Erstelle_ListenFeld('RRP_KENN1', $rsRST->FeldInhalt('RRP_KENN1'), 0, 60, false, ($DS % 2), '', '', 'T', '', '');
            $Form->Erstelle_ListenFeld('RRP_KENN2', $rsRST->FeldInhalt('RRP_KENN2').'/'.$rsRST->FeldInhalt('RRP_KENN_L2'), 0, 50, false, ($DS % 2), '', '', '', '', '');
            if($rsRST->FeldInhalt('RST_BEMERKUNG')!='')
            {
            	$Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsRST->FeldInhalt('RST_BEMERKUNG'));
            }
            
            $Form->ZeileEnde();

            $rsRST->DSWeiter();
            $DS++;
        }

        $Link = './reifenstamm_Main.php?cmdAktion=Details&Liste=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();

        //***************************************
        // Schaltfl�chen f�r dieses Register
        //***************************************
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
        if (($Recht15300 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
        {
            $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
        }
        $Form->SchaltflaechenEnde();
    }
    else          // Ein einzelner Datensatz
    {
    	$LabelbreiteSP1=160;
    	$DatenbreiteSP1=160;
    	$LabelbreiteSP2=160;
    	$DatenbreiteSP2=160;
    	$LabelbreiteSP3=160;
    	$DatenbreiteSP3=160;
    	$LabelbreiteSP4=160;
    	$DatenbreiteSP4=160;
    	 
        $Form->SchreibeHTMLCode('<form name=frmReifenstamm action=./reifenstamm_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

        $AWIS_KEY1 = $rsRST->FeldInhalt('RST_KEY');
        $Param['KEY']=$AWIS_KEY1;
        $AWISBenutzer->ParameterSchreiben("Formular_RST", serialize($Param));
        $EditRecht = (($Recht15300&2)!=0);
        $EditErlaubt = ($EditRecht AND $rsRST->FeldInhalt('RRP_AST_KEY')=='');
        
        $Form->Erstelle_HiddenFeld('RST_KEY', $AWIS_KEY1);

        $Form->Formular_Start();
        $OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
        $Form->DebugAusgabe(1,$AWIS_KEY1);
        
        // Infozeile
        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./reifenstamm_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsRST->FeldInhalt('RST_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsRST->FeldInhalt('RST_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_STATUS'] . ':', $LabelbreiteSP1);
        $Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_STATUS']);
        $Form->Erstelle_SelectFeld('RST_STATUS',$rsRST->FeldInhalt('RST_STATUS')!=''?$rsRST->FeldInhalt('RST_STATUS'):'10' , $DatenbreiteSP1,($AWIS_KEY1==''?false:($Recht15300&2)!=0), '', '','','','',$Daten);
        $AWISCursorPosition = 'txtRST_STATUS';
        
        if ($AWIS_KEY1!='')
        {
	        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_IMPORTSTATUS'] . ':', $LabelbreiteSP2);
	        $Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_IMPORTSTATUS']);
	        $Form->Erstelle_SelectFeld('RST_IMPORTSTATUS',$rsRST->FeldInhalt('RST_IMPORTSTATUS')!=''?$rsRST->FeldInhalt('RST_IMPORTSTATUS'):'10' , $DatenbreiteSP2, false, '', '','','','',$Daten);
        }
        $Form->ZeileEnde();
        
        $Form->Trennzeile('O');

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['REH']['HERSTELLER'] . ':', $LabelbreiteSP1);
        $SQL = "SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ";
        $Form->Erstelle_SelectFeld('RST_REH_KEY', $rsRST->FeldInhalt('RST_REH_KEY'), $DatenbreiteSP1, $EditRecht, $SQL, $OptionBitteWaehlen);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LIE_NR'] . ':', $LabelbreiteSP2);
        $Form->Erstelle_TextFeld('RST_LIE_NR', ($rsRST->FeldInhalt('RST_LIE_NR')), 4, $DatenbreiteSP2, $EditErlaubt,'','','','','','','',4);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LARTNR'] . ':', $LabelbreiteSP3);
        $Form->Erstelle_TextFeld('RST_LARTNR', ($rsRST->FeldInhalt('RST_LARTNR')), 30, 240, $EditErlaubt,'','','','','','','',20);
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_EAN'] . ':', $LabelbreiteSP1);
        $Form->Erstelle_TextFeld('RST_EAN', ($rsRST->FeldInhalt('RST_EAN')), 20, $DatenbreiteSP1, $EditErlaubt,'','','','','','','',13);
        $Form->ZeileEnde();
        
        if ($AWIS_KEY1!='')
        {
	        $Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BEZEICHNUNG'] . ':', $LabelbreiteSP1);
	        $Form->Erstelle_TextFeld('RST_BEZEICHNUNG', ($rsRST->FeldInhalt('RST_BEZEICHNUNG')), 50, $DatenbreiteSP1, $EditRecht,'','','','','','','',50);
	        $Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP'] . ':', $LabelbreiteSP1);
        	$SQL = "SELECT RET_ID, RET_TYP FROM REIFENTYP ";
        	$Form->Erstelle_SelectFeld('RST_TYP',$rsRST->FeldInhalt('RST_TYP'),$DatenbreiteSP1,$EditRecht,$SQL,$OptionBitteWaehlen);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP2'] . ':', $LabelbreiteSP2);
        	$Form->Erstelle_SelectFeld('RST_TYP2',$rsRST->FeldInhalt('RST_TYP2'), $DatenbreiteSP2,$EditRecht,$SQL,$OptionBitteWaehlen);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_TYP3'] . ':', $LabelbreiteSP3);
        	$Form->Erstelle_SelectFeld('RST_TYP3',$rsRST->FeldInhalt('RST_TYP3'), $DatenbreiteSP3,$EditRecht,$SQL,$OptionBitteWaehlen);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BREITE'] . ':', $LabelbreiteSP1);
        	$Form->Erstelle_TextFeld('RST_BREITE', ($rsRST->FeldInhalt('RST_BREITE')), 4, $DatenbreiteSP1, $EditRecht);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_QUERSCHNITT'] . ':', $LabelbreiteSP2);
        	$Form->Erstelle_TextFeld('RST_QUERSCHNITT', ($rsRST->FeldInhalt('RST_QUERSCHNITT')), 4, $DatenbreiteSP2, $EditRecht);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_INNENDURCHMESSER'] . ':', $LabelbreiteSP3);
        	$Form->Erstelle_TextFeld('RST_INNENDURCHMESSER', ($rsRST->FeldInhalt('RST_INNENDURCHMESSER')), 4, $DatenbreiteSP3, $EditRecht);
        	$Form->ZeileEnde();
        	
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BAUART'] . ':', $LabelbreiteSP1);
        	$Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_BAUART']);
        	$Form->Erstelle_SelectFeld('RST_BAUART',$rsRST->FeldInhalt('RST_BAUART'), $DatenbreiteSP1, $EditRecht, '',$OptionBitteWaehlen,'','','',$Daten);
        	 
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LLKW'] . ':', $LabelbreiteSP2);
        	$Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_LLKW']);
        	$Form->Erstelle_SelectFeld('RST_LLKW',$rsRST->FeldInhalt('RST_LLKW'), $DatenbreiteSP1, $EditRecht, '',$OptionBitteWaehlen,'','','',$Daten);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX'] . ':', $LabelbreiteSP1);
        	$Form->Erstelle_TextFeld('RST_LOADINDEX',($rsRST->FeldInhalt('RST_LOADINDEX')),10, $DatenbreiteSP1,$EditRecht,'','','','','','','',6);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX2'] . ':', $LabelbreiteSP2);
        	$Form->Erstelle_TextFeld('RST_LOADINDEX2',($rsRST->FeldInhalt('RST_LOADINDEX2')),10, $LabelbreiteSP2,$EditRecht);
        	$Form->ZeileEnde();
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_LOADINDEX3'] . ':', $LabelbreiteSP1);
        	$Form->Erstelle_TextFeld('RST_LOADINDEX3',($rsRST->FeldInhalt('RST_LOADINDEX3')),10, $DatenbreiteSP1,$EditRecht);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_RF'] . ':', $LabelbreiteSP2);
        	$Daten = explode("|",$AWISSprachKonserven['RST']['lst_RST_RF']);
        	$Form->Erstelle_SelectFeld('RST_RF',$rsRST->FeldInhalt('RST_RF'), $DatenbreiteSP2,$EditRecht,'',$OptionBitteWaehlen,'','','',$Daten);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_SPEEDINDEX_kurz'] . ':', $LabelbreiteSP1);
        	$SQL = "SELECT SPI_SYMBOL AS KEY,SPI_SYMBOL AS WERT FROM SPEEDINDEX ";
        	$Form->Erstelle_SelectFeld('RST_SPEEDINDEX',$rsRST->FeldInhalt('RST_SPEEDINDEX'), $DatenbreiteSP1,$EditRecht,$SQL,$OptionBitteWaehlen);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_SPEEDINDEX2_kurz'] . ':', $LabelbreiteSP2);
        	$Form->Erstelle_SelectFeld('RST_SPEEDINDEX2',$rsRST->FeldInhalt('RST_SPEEDINDEX2'), $DatenbreiteSP2,$EditRecht,$SQL,$OptionBitteWaehlen);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_FELGENRIPPE'] . ':', $LabelbreiteSP1);
            $Daten = explode("|",$AWISSprachKonserven['RST']['lst_FELGENRIPPE']);
            $Form->Erstelle_SelectFeld('RST_FELGENRIPPE',($rsRST->FeldInhalt('RST_FELGENRIPPE')!== NULL and $rsRST->FeldInhalt('RST_FELGENRIPPE') == 'FR')?$rsRST->FeldInhalt('RST_FELGENRIPPE'):' ',$DatenbreiteSP1,$EditRecht,'','','','','',$Daten);
            $Form->ZeileEnde();
        	 
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ZUSATZBEMERKUNG'] . ':', $LabelbreiteSP1);
        	$Form->Erstelle_TextFeld('RST_ZUSATZBEMERKUNG',($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')),10, $DatenbreiteSP1,$EditRecht,'','','','','','','',6);
        	$Form->ZeileEnde();
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ZUSATZBEMERKUNG2'] . ':', $LabelbreiteSP1);
        	$Form->Erstelle_TextFeld('RST_ZUSATZBEMERKUNG2',($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG2')),10, $LabelbreiteSP1,$EditRecht,'','','','','','','',3);
        	$Form->ZeileEnde();
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ZUSATZBEMERKUNG3'] . ':', $LabelbreiteSP1);
        	$Form->Erstelle_TextFeld('RST_ZUSATZBEMERKUNG3',($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG3')),10, $DatenbreiteSP1,$EditRecht,'','','','','','','',4);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_ROLLRES'] . ':', $LabelbreiteSP1);
        	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_ROLLRES']);
        	$Form->Erstelle_SelectFeld('RST_ROLLRES',$rsRST->FeldInhalt('RST_ROLLRES') , "160:150", $EditErlaubt, '', $OptionBitteWaehlen,'','','',$Daten);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_WETGRIP'] . ':', $LabelbreiteSP2);
        	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_WETGRIP']);
        	$Form->Erstelle_SelectFeld('RST_WETGRIP',$rsRST->FeldInhalt('RST_WETGRIP') , "160:150", $EditErlaubt, '', $OptionBitteWaehlen,'','','',$Daten);
        	$Form->ZeileEnde();
        	 
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_NOISECL'] . ':', $LabelbreiteSP1);
        	$Daten = explode("|",$AWISSprachKonserven['Liste']['lst_NOISECL']);
        	$Form->Erstelle_SelectFeld('RST_NOISECL',$rsRST->FeldInhalt('RST_NOISECL') , "160:150", $EditErlaubt, '', $OptionBitteWaehlen,'','','',$Daten);
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_NOISEPE'] . ':', $LabelbreiteSP2);
        	$Noisepe = '';
        	for ($i = 1; $i < 100; $i++)
        	{
        		$Noisepe = $Noisepe.'|'.$i.'~'.$i;
        	}
        	$Noisepe = substr($Noisepe,1);
        	$Daten = explode("|",$Noisepe);
        	$Form->Erstelle_SelectFeld('RST_NOISEPE',$rsRST->FeldInhalt('RST_NOISEPE'), "160:150", $EditErlaubt, '', $OptionBitteWaehlen,'','','',$Daten);
        	$Form->ZeileEnde();
        	
        	$Form->ZeileStart();
        	$Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_KBPREIS'] . ':', $LabelbreiteSP1);
        	$Form->Erstelle_TextFeld('RST_KBPREIS',($rsRST->FeldInhalt('RST_KBPREIS')),10, $DatenbreiteSP1,$EditErlaubt,'','','','N2');
        	$Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_WINTERTAUGLICHKEIT'] . ':', $LabelbreiteSP1);
            $Daten = explode("|",$AWISSprachKonserven['RST']['lst_WINTERTAUGLICHKEIT']);
            $Form->Erstelle_SelectFeld('RST_WINTERTAUGLICHKEIT',($rsRST->FeldInhalt('RST_WINTERTAUGLICHKEIT')!== NULL and $rsRST->FeldInhalt('RST_WINTERTAUGLICHKEIT') == 'X')?$rsRST->FeldInhalt('RST_WINTERTAUGLICHKEIT'):' ',$DatenbreiteSP1,$EditRecht,'','','','','',$Daten);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_EISGRIFF'] . ':', $LabelbreiteSP1);
            $Daten = explode("|",$AWISSprachKonserven['RST']['lst_EISGRIFF']);
            $Form->Erstelle_SelectFeld('RST_EISGRIFF',($rsRST->FeldInhalt('RST_EISGRIFF')!== NULL and $rsRST->FeldInhalt('RST_EISGRIFF') == 'X')?$rsRST->FeldInhalt('RST_EISGRIFF'):' ',$DatenbreiteSP1,$EditRecht,'','','','','',$Daten);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_INFO'] . ':', $LabelbreiteSP1);
            $Form->Erstelle_TextFeld('RST_INFO',($rsRST->FeldInhalt('RST_INFO')),15, $DatenbreiteSP1,$EditRecht,'','','','T','L','','',15);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_URL4'] . ':', $LabelbreiteSP1);
            $Form->Erstelle_TextFeld('RST_URL4',($rsRST->FeldInhalt('RST_URL4')),50, $DatenbreiteSP1,$EditRecht,'','','','T','L','','',90);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_URL5'] . ':', $LabelbreiteSP1);
            $Form->Erstelle_TextFeld('RST_URL5',($rsRST->FeldInhalt('RST_URL5')),50, $DatenbreiteSP1,$EditRecht,'','','','T','L','','',90);
            $Form->ZeileEnde();
        }
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['RST']['RST_BEMERKUNG'].':',$LabelbreiteSP1);
        $Form->Erstelle_TextArea('RST_BEMERKUNG',$rsRST->FeldInhalt('RST_BEMERKUNG'),800,103,5,$EditRecht);
        $Form->ZeileEnde();
        
        $Form->Trennzeile('O');
        
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['XDI']['txt_XDI_DATUM'].':',$LabelbreiteSP1);
        $Form->Erstelle_TextFeld('*XDI_DATUM',$rsRST->FeldInhalt('XDI_DATUM'),10,200,false);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['XDI']['XDI_DATEINAME'].':',120);
        $Form->Erstelle_TextFeld('*XDI_DATEINAME',$rsRST->FeldInhalt('XDI_DATEINAME'),50,400,false);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');
        
        if ($AWIS_KEY1 != 0)
        {
            $RegisterSeite = (isset($_GET['Seite']) ? $_GET['Seite'] : (isset($_POST['Seite']) ? $_POST['Seite'] : ''));
            $SubReg = new awisRegister(15310);
            $SubReg->ZeichneRegister($RegisterSeite);
        }

	    //***************************************
	    // Schaltfl�chen f�r dieses Register
	    //***************************************
	    $Form->SchaltflaechenStart();
	
	    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	    if (($Recht15300 & 6) != 0 or ($Recht15300 & 128) != 0)
	    {
	        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	    }
	
	    if (($Recht15300 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
	    {
	        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	    }
	
	    if (($Recht15300 & 8) == 8 AND !isset($_POST['cmdDSNeu_x']))
	    {
	         $Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
	    }
	
	    $Form->SchaltflaechenEnde();
    }


    $Form->SchreibeHTMLCode('</form>');
    
    //*****************************************************************
    // Aktuelle Parameter sichern
    //*****************************************************************
    $AWISBenutzer->ParameterSchreiben("Formular_RST", serialize($Param));

    $Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201304301400");
    }
    else
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}
catch (Exception $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201304301401");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param, &$BindeVariablen)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    if (floatval($AWIS_KEY1) != 0)
    {
        $Bedingung.= 'AND RST_KEY = :var_N0_RST_KEY';
        $BindeVariablen['var_N0_RST_KEY'] = $AWIS_KEY1; 
        return $Bedingung;
    }

    if (isset($Param['RST_STATUS']) AND $Param['RST_STATUS'] != '' AND $Param['RST_STATUS'] != -1)
    {
    	$Bedingung .= 'AND RST_STATUS = :var_N0_RST_STATUS ';
    	$BindeVariablen['var_N0_RST_STATUS'] = $Param['RST_STATUS'];
    }
    
    if (isset($Param['RST_IMPORTSTATUS']) AND $Param['RST_IMPORTSTATUS'] != '' AND $Param['RST_IMPORTSTATUS'] != -1)
    {
    	$Bedingung .= 'AND RST_IMPORTSTATUS = :var_N0_RST_IMPORTSTATUS ';
    	$BindeVariablen['var_N0_RST_IMPORTSTATUS'] = $Param['RST_IMPORTSTATUS'];
    }
    
    if (isset($Param['GELISTET']) AND $Param['GELISTET'] != '' AND $Param['GELISTET'] != -1)
    {
    	if ($Param['GELISTET']<2)
    	{
    		$Bedingung .= 'AND RRP_AST_KEY IS '.($Param['GELISTET']==0?'':'NOT').' NULL ';
    	}
    	elseif ($Param['GELISTET']==2)
    	{
    		$Bedingung .= 'AND RRP_KEY IS NULL ';
    	}
    }
    
    if (isset($Param['RST_REH_KEY']) AND $Param['RST_REH_KEY'] != '-1')
    {
        $Bedingung .= 'AND RST_REH_KEY = :var_N0_RST_REH_KEY '; 
		$BindeVariablen['var_N0_RST_REH_KEY'] = $Param['RST_REH_KEY'];
    }
    
    if (isset($Param['RRP_ATUNR']) AND $Param['RRP_ATUNR'] != '')
    {
    	$Bedingung .= 'AND RRP_ATUNR = :var_T_RRP_ATUNR ';
    	$BindeVariablen['var_T_RRP_ATUNR'] = $Param['RRP_ATUNR'];
    }
    
    if (isset($Param['RST_LIE_NR']) AND $Param['RST_LIE_NR'] != '')
    {
        $Bedingung .= 'AND RST_LIE_NR = :var_T_RST_LIE_NR ';
        $BindeVariablen['var_T_RST_LIE_NR'] = $Param['RST_LIE_NR'];
    }
    
    if (isset($Param['RST_LARTNR']) AND $Param['RST_LARTNR'] != '')
    {
        $Bedingung .= 'AND UPPER(ASCIIWORT(RST_LARTNR)) = UPPER(ASCIIWORT(:var_T_RST_LARTNR)) ';
        $BindeVariablen['var_T_RST_LARTNR'] = $Param['RST_LARTNR'];
    }
    
    if (isset($Param['RST_EAN']) AND $Param['RST_EAN'] != '')
    {
    	$Bedingung .= 'AND RST_EAN = :var_T_RST_EAN ';
    	$BindeVariablen['var_T_RST_EAN'] = $Param['RST_EAN'];
    }

    if (isset($Param['RST_TYP']) AND $Param['RST_TYP'] != '-1')
    {
    	$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP ';
    	$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP ';
    	$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP) ';
    	$BindeVariablen['var_N0_RST_TYP'] = $Param['RST_TYP'];
    }
    
    if (isset($Param['RST_TYP2']) AND $Param['RST_TYP2'] != '-1')
    {
    	$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP2 ';
    	$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP2 ';
    	$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP2) ';
    	$BindeVariablen['var_N0_RST_TYP2'] = $Param['RST_TYP2'];
    }
    
    if (isset($Param['RST_TYP3']) AND $Param['RST_TYP3'] != '-1')
    {
    	$Bedingung .= 'AND (RST_TYP = :var_N0_RST_TYP3 ';
    	$Bedingung .= 'OR RST_TYP2 = :var_N0_RST_TYP3 ';
    	$Bedingung .= 'OR RST_TYP3 = :var_N0_RST_TYP3) ';
    	$BindeVariablen['var_N0_RST_TYP3'] = $Param['RST_TYP3'];
    }
    
    if (isset($Param['RST_BREITE']) AND $Param['RST_BREITE'] != '')
    {
        $Bedingung .= 'AND RST_BREITE = :var_N0_RST_BREITE ';
        $BindeVariablen['var_N0_RST_BREITE'] = $Param['RST_BREITE'];
    }

    if (isset($Param['RST_QUERSCHNITT']) AND $Param['RST_QUERSCHNITT'] != '')
    {
        $Bedingung .= 'AND RST_QUERSCHNITT = :var_T_RST_QUERSCHNITT ';
        $BindeVariablen['var_T_RST_QUERSCHNITT'] = $Param['RST_QUERSCHNITT'];
    }

    if (isset($Param['RST_BAUART']) AND $Param['RST_BAUART'] != '' AND $Param['RST_BAUART'] != -1)
    {
        $Bedingung .= 'AND RST_BAUART = :var_T_RST_BAUART ';
        $BindeVariablen['var_T_RST_BAUART'] = $Param['RST_BAUART'];
    }
    
    if (isset($Param['RST_INNENDURCHMESSER']) AND $Param['RST_INNENDURCHMESSER'] != '')
    {
        $Bedingung .= 'AND RST_INNENDURCHMESSER = :var_N0_RST_INNENDURCHMESSER ';
        $BindeVariablen['var_N0_RST_INNENDURCHMESSER'] = $Param['RST_INNENDURCHMESSER'];
    }

    if (isset($Param['RST_SPEEDINDEX']) AND $Param['RST_SPEEDINDEX'] != '-1')
    {
        $BindeVariablen2 = array();
    	$SQL = "SELECT SPI_SYMBOL FROM SPEEDINDEX WHERE SPI_ID = :var_N0_SPI_ID";
    	$BindeVariablen2['var_N0_SPI_ID'] = $Param['RST_SPEEDINDEX'];
        $rsSpeed = $DB->RecordSetOeffnen($SQL,$BindeVariablen2);
        
        $Bedingung .= 'AND RST_SPEEDINDEX = :var_T_RST_SPEEDINDEX ';
		$BindeVariablen['var_T_RST_SPEEDINDEX'] = $rsSpeed->FeldInhalt('SPI_SYMBOL');
    }

    if (isset($Param['RST_SPEEDINDEX2']) AND $Param['RST_SPEEDINDEX2'] != '-1')
    {
        $BindeVariablen2 = array();
    	$SQL = "SELECT SPI_SYMBOL FROM SPEEDINDEX WHERE SPI_ID = :var_N0_SPI_ID";
    	$BindeVariablen2['var_N0_SPI_ID'] = $Param['RST_SPEEDINDEX2'];
        $rsSpeed = $DB->RecordSetOeffnen($SQL,$BindeVariablen2);
        
        $Bedingung .= 'AND RST_SPEEDINDEX2 = :var_T_RST_SPEEDINDEX2 ';
		$BindeVariablen['var_T_RST_SPEEDINDEX2'] = $rsSpeed->FeldInhalt('SPI_SYMBOL');
    }

    if (isset($Param['RST_LLKW']) AND $Param['RST_LLKW'] != '')
    {
        $Bedingung .= 'AND RST_LLKW = :var_T_RST_LLKW ';
		$BindeVariablen['var_T_RST_LLKW'] = $Param['RST_LLKW'];
    }

    if (isset($Param['RST_LOADINDEX']) AND $Param['RST_LOADINDEX'] != '')
    {
        $Bedingung .= 'AND RST_LOADINDEX = :var_T_RST_LOADINDEX ';
		$BindeVariablen['var_T_RST_LOADINDEX'] = $Param['RST_LOADINDEX'];
    }

    if (isset($Param['RST_LOADINDEX2']) AND $Param['RST_LOADINDEX2'] != '')
    {
        $Bedingung .= 'AND RST_LOADINDEX2 = :var_N0_RST_LOADINDEX2 ';
		$BindeVariablen['var_N0_RST_LOADINDEX2'] = $Param['RST_LOADINDEX2'];
    }
    
    if (isset($Param['RST_LOADINDEX3']) AND $Param['RST_LOADINDEX3'] != '')
    {
    	$Bedingung .= 'AND RST_LOADINDEX3 = :var_N0_RST_LOADINDEX3 ';
    	$BindeVariablen['var_N0_RST_LOADINDEX3'] = $Param['RST_LOADINDEX3'];
    }

    if (isset($Param['RST_RF']) AND $Param['RST_RF'] != '-1')
    {
    	$Bedingung .= 'AND RST_RF = :var_T_RST_RF ';
    	$BindeVariablen['var_T_RST_RF'] = $Param['RST_RF'];
    }
    
    if (isset($Param['RST_BEZEICHNUNG']) AND $Param['RST_BEZEICHNUNG'] != '')
    {
    	$Bedingung .= 'AND TRIM(UPPER(RST_BEZEICHNUNG)) '. $DB->LIKEoderIST($Param['RST_BEZEICHNUNG'],awisDatenbank::AWIS_LIKE_UPPER);
    }
    
    if (isset($Param['RST_ROLLRES']) AND $Param['RST_ROLLRES'] != '-1')
    {
    	$Bedingung .= 'AND RST_ROLLRES = :var_T_RST_ROLLRES ';
    	$BindeVariablen['var_T_RST_ROLLRES'] = $Param['RST_ROLLRES'];
    }
    
    if (isset($Param['RST_WETGRIP']) AND $Param['RST_WETGRIP'] != '-1')
    {
    	$Bedingung .= 'AND RST_WETGRIP = :var_T_RST_WETGRIP ';
    	$BindeVariablen['var_T_RST_WETGRIP'] = $Param['RST_WETGRIP'];
    }
    
    if (isset($Param['RST_NOISEPE']) AND $Param['RST_NOISEPE'] != '-1')
    {
    	$Bedingung .= 'AND RST_NOISEPE = :var_T_RST_NOISEPE ';
    	$BindeVariablen['var_T_RST_NOISEPE'] = $Param['RST_NOISEPE'];
    }
    
    if (isset($Param['RST_NOISECL']) AND $Param['RST_NOISECL'] != '-1')
    {
    	$Bedingung .= 'AND RST_NOISECL = :var_T_RST_NOISECL ';
    	$BindeVariablen['var_T_RST_NOISECL'] = $Param['RST_NOISECL'];
    }

    return $Bedingung;
}
?>