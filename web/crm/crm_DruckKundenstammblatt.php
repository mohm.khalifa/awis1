<?php
/**
 * crm_DruckKundenstammblatt.php
 *
 * Dieses Programm erstellt den Ausdruck f�r das Kundenstammblatt
 *
 *
 * @author Sacha Kerres
 * @version 20080422
 * @copyright ATU Auto Teile Unger
 *
 */
require_once('db.inc.php');
require_once('register.inc.php');
require_once('awisAusdruck.php');
require_once('awis_forms.inc.php');


global $awisRSZeilen;

ini_set('max_execution_time','9000');

$AWISBenutzer = new awisUser();

$Vorlagen = array();

$DruckArt = 1;		// Standard: 1 Datei pro Kunde
$CADKEY = 0;		// Alle offenen drucken

if(isset($_GET['Druckart']) AND $_GET['Druckart']==2)
{
	$DruckArt=2;								// Alles in eine Datei drucken
}
if(isset($_GET['CADKEY']))
{
	$CADKEY=floatval($_GET['CADKEY']);			// nur f�r einen bestimmten Kunden drucken
}

$con = awisLogon();

$AWISSprache = awis_BenutzerParameter($con, "AnzeigeSprache",$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('Ausdruck','txtUeberschriftFlottenkundenKonditionen');
$TextKonserven[]=array('Ausdruck','txtFirmaVertraulich');
$TextKonserven[]=array('Ausdruck','txtKonditionen');
$TextKonserven[]=array('Ausdruck','txtKundenstammblatt%');
$TextKonserven[]=array('CAP','lst_CAP_ANREDE');
$TextKonserven[]=array('CAD','lst_Rechnungsstellung');
$TextKonserven[]=array('CAD','lst_Rechnungsart');
$TextKonserven[]=array('CAD','lst_Zahlungsziel');
$TextKonserven[]=array('CAD','CAD_%');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

// Abfrage mit den Kunden, f�r die etwas gedruckt werden soll
$SQL = 'SELECT CAD_NAME1, CAD_NAME2, CAD_STRASSE, CAD_HAUSNUMMER, CAD_PLZ, CAD_LAN_CODE, CAD_ORT, CAD_KEY, CAD_STEUERNUMMER';
$SQL .= ', CAD_USTIDNR, CAD_HANDELSREGISTER, CAD_TELEFON, CAD_TELEFAX, CAD_EMAIL, CAQ_VERTRAGSNUMMER';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 102 AND ROWNUM=1) AS CAD_BANKNAME';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 100 AND ROWNUM=1) AS CAD_BLZ';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 101 AND ROWNUM=1) AS CAD_KONTO';
$SQL .= ', (select CKM_BEZEICHNUNG FROM CRMKonditionsModelle INNER JOIN CRMINFOS ON CIN_WERT = CKM_KEY AND CIN_CIT_ID = 1100 WHERE CIN_CAD_KEY=CAD_KEY AND ROWNUM=1) AS WG';
$SQL .= ', (select CKM_BEZEICHNUNG FROM CRMKonditionsModelle INNER JOIN CRMINFOS ON CIN_WERT = CKM_KEY AND CIN_CIT_ID = 1110 WHERE CIN_CAD_KEY=CAD_KEY AND ROWNUM=1) AS DL';
$SQL .= ', (select CKM_BEZEICHNUNG FROM CRMKonditionsModelle INNER JOIN CRMINFOS ON CIN_WERT = CKM_KEY AND CIN_CIT_ID = 1121 WHERE CIN_CAD_KEY=CAD_KEY AND ROWNUM=1) AS TH';
$SQL .= ', (select CKM_BEZEICHNUNG_OPAL FROM CRMKonditionsModelle INNER JOIN CRMINFOS ON CIN_WERT = CKM_KEY AND CIN_CIT_ID = 1110 WHERE CIN_CAD_KEY=CAD_KEY AND ROWNUM=1) AS DL_OPAL';
$SQL .= ', (select CKM_BEZEICHNUNG FROM CRMKonditionsModelle INNER JOIN CRMINFOS ON CIN_WERT = CKM_KEY AND CIN_CIT_ID = 1120 WHERE CIN_CAD_KEY=CAD_KEY AND ROWNUM=1) AS RH';
$SQL .= ', (SELECT ROUND(SUM(CFZ_ANZAHL)) from (SELECT DISTINCT CFZ_ANZAHL*(CFZ_ANTEIL/100) AS CFZ_ANZAHL, CFZ_KEY, CPO_CAD_KEY FROM CRMPOTENTIAL INNER JOIN CRMPOTENTIALDETAILS ON CPD_CPO_KEY = CPO_KEY INNER JOIN CRMFAHRZEUGE ON CPD_CFZ_KEY = CFZ_KEY AND CFZ_CFT_ID = 100) Daten WHERE CPO_CAD_KEY = CAD_KEY GROUP BY CPO_CAD_KEY) AS CPO_ANZPKW';
$SQL .= ', (SELECT ROUND(SUM(CFZ_ANZAHL)) from (SELECT DISTINCT CFZ_ANZAHL*(CFZ_ANTEIL/100) AS CFZ_ANZAHL, CFZ_KEY, CPO_CAD_KEY FROM CRMPOTENTIAL INNER JOIN CRMPOTENTIALDETAILS ON CPD_CPO_KEY = CPO_KEY INNER JOIN CRMFAHRZEUGE ON CPD_CFZ_KEY = CFZ_KEY AND CFZ_CFT_ID = 110) Daten WHERE CPO_CAD_KEY = CAD_KEY GROUP BY CPO_CAD_KEY) AS CPO_ANZTRA';
$SQL .= ', CAP_ANREDE, CAP_NACHNAME, CAP_VORNAME';
$SQL .= ', (SELECT CAK_WERT from CRMANSPRECHPARTNERKONTAKTE WHERE CAK_CAP_KEY=CAP_KEY AND CAK_KOT_KEY = 7 AND ROWNUM=1) AS CAP_EMAIL';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1001 AND ROWNUM=1) AS RAHMENVERTRAG';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1002 AND ROWNUM=1) AS PORTALFIRMAUSER';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1003 AND ROWNUM=1) AS PORTALFIRMAPWD';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1010 AND ROWNUM=1) AS PORTALMAUSER';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1011 AND ROWNUM=1) AS PORTALMAPWD';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1012 AND ROWNUM=1) AS BARZAHLER';
$SQL .= ', (select LAN_LAND FROM LAENDER WHERE LAN_CODE = (SELECT CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1013 AND ROWNUM=1)) AS LAND';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1004 AND ROWNUM=1) AS GROSSKUNDENAUSW';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1005 AND ROWNUM=1) AS ATUCARD';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1130 AND ROWNUM=1) AS UMSATZBONUS';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1140 AND ROWNUM=1) AS FESTERBONUS';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1150 AND ROWNUM=1) AS BONUS1AB';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1152 AND ROWNUM=1) AS BONUS2AB';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1154 AND ROWNUM=1) AS BONUS3AB';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1151 AND ROWNUM=1) AS BONUS1';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1153 AND ROWNUM=1) AS BONUS2';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1155 AND ROWNUM=1) AS BONUS3';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1141 AND ROWNUM=1) AS BONUSBEM';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1180 AND ROWNUM=1) AS RECHNUNGSSTELLUNG';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1181 AND ROWNUM=1) AS ZAHLUNGSZIEL';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1182 AND ROWNUM=1) AS SKONTO';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1183 AND ROWNUM=1) AS RECHNUNGSART';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1184 AND ROWNUM=1) AS EDI';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 2000 AND ROWNUM=1) AS INFO_SERVICE';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 2005 AND ROWNUM=1) AS INFO_BEACHTEN';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 2010 AND ROWNUM=1) AS INFO_FREIGABEN';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 2015 AND ROWNUM=1) AS INFO_ABRECHNUNG';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 103 AND ROWNUM=1) AS AMTSGERICHT';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1185 AND ROWNUM=1) AS KOMPLETTRAEDER';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1186 AND ROWNUM=1) AS KOMPLETT';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 4000 AND ROWNUM=1) AS HAUSNRZUSATZ';
$SQL .= ', func_textkonserve(\'lst_CIT_Kartenart\',\'Liste\',\'DE\',coalesce((select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1024 AND ROWNUM=1),\'0\')) AS KARTENART';
$SQL .= ', (select CIN_WERT from CRMINFOS WHERE CIN_CAD_KEY=CAD_KEY AND CIN_CIT_ID = 1025 AND ROWNUM=1) AS KARTEN ';
$SQL .= ', (SELECT CGB_GEBIET FROM CRMGEBIETE WHERE CGB_KEY = CAD_CGB_KEY) AS GEBIET';
$SQL .= ', (SELECT CGB_KENNUNG FROM CRMGEBIETE WHERE CGB_KEY = CAD_CGB_KEY) AS GEBIETSKENNUNG';
$SQL .= ', KON_NAME1, KON_NAME2';
$SQL .= ' FROM crmAdressen';
$SQL .= ' INNER JOIN CRMAkquiseStand ON CAQ_CAD_KEY = CAD_KEY';
$SQL .= ' LEFT OUTER JOIN Kontakte ON KON_KEY = CAD_KON_KEY';
$SQL .= ' LEFT OUTER JOIN CRMAnsprechpartner ON CAQ_CAP_KEY = CAP_KEY';
if($CADKEY==0)		// Alle abh�ngig vom letzten Termin
{
	$SQL .= ' 		AND 0=1';
}
else 				// Nur einen einzelnen, ohne eine Aktion zu setzen
{
	$SQL .= ' WHERE CAD_KEY = 0'.$CADKEY;
}
$SQL .= ' ORDER BY CAD_NAME1, CAD_NAME2';

$rsCAD = awisOpenRecordset($con, $SQL);
$rsCADZeilen = $awisRSZeilen;
if($rsCADZeilen==0)
{
	awis_Debug(1,$SQL);
	die();
}

$Spalte = 20;
$Zeile = 20;
$Ausdruck = null;

for($CADZeile=0;$CADZeile<$rsCADZeilen;$CADZeile++)
{

	if($DruckArt==1)
	{
		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Kundenstammblatt f�r '.awisFormat('TP',$rsCAD['CAD_NAME1'][$CADZeile]));
	}
	elseif($Ausdruck==null AND $DruckArt==2)	// Eine Seite pro Kunde oder alles in eine)
	{
		$Ausdruck = new awisAusdruck('P','A4',$Vorlagen,'Kundenstammblatt');
	}

	$Ausdruck->NeueSeite();
	$Zeile = 20;

	$Ausdruck->_pdf->SetFont('Arial','',8);
	$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,$AWISSprachKonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L',0);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,'Kundenstammblatt',0,0,'C',0);
	$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,date('d.m.Y'),0,0,'R',0);


	$Ausdruck->_pdf->SetFont('Arial','B',16);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(100,5,$AWISSprachKonserven['Ausdruck']['txtKundenstammblattUeberschrift'] .($rsCAD['CAQ_VERTRAGSNUMMER'][$CADZeile]==''?'':(', Vertrag: '.$rsCAD['CAQ_VERTRAGSNUMMER'][$CADZeile])),0,0,'L',0);
	$Zeile+=7;


	// Firmenname
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,$AWISSprachKonserven['CAD']['CAD_NAME1'].':',0,0,'L',0);
	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_NAME1'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,$AWISSprachKonserven['CAD']['CAD_NAME2'].':',0,0,'L',0);
	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_NAME2'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,$AWISSprachKonserven['CAD']['CAD_STRASSE'].':',0,0,'L',0);
	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_STRASSE'][$CADZeile].' '.$rsCAD['CAD_HAUSNUMMER'][$CADZeile].' '.$rsCAD['HAUSNRZUSATZ'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,$AWISSprachKonserven['CAD']['CAD_PLZ'].'/'.$AWISSprachKonserven['CAD']['CAD_ORT'].':',0,0,'L',0);
	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_PLZ'][$CADZeile].' '.$rsCAD['CAD_ORT'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'Steuernummer:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_STEUERNUMMER'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'Ust.Ident-Nr:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_USTIDNR'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'Handelsregister:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_HANDELSREGISTER'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'bei:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['AMTSGERICHT'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'Bankverbindung:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(100,5,awisFormat('TP',$rsCAD['CAD_BANKNAME'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'BIC:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_BLZ'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'IBAN:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_KONTO'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'Telefon:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_TELEFON'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'Telefax:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_TELEFAX'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'E-Mail:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAD_EMAIL'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'Betreuer:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['KON_NAME1'][$CADZeile].', '.$rsCAD['KON_NAME2'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'Land:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['LAND'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;
	
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'Gebiet:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['GEBIET'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	//**************************************************************
	//** Ansprechpartner
	//**************************************************************
	$Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
	$Zeile++;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(30,5,'Ansprechpartner:',0,0,'L',0);
	$Anrede = awis_WertListe($AWISSprachKonserven['CAP']['lst_CAP_ANREDE'],awisFormat('TP',$rsCAD['CAP_ANREDE'][$CADZeile]));
	$Ausdruck->_pdf->Cell(60,5,$Anrede.' '.awisFormat('TP',$rsCAD['CAP_VORNAME'][$CADZeile].' '.$rsCAD['CAP_NACHNAME'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'E-Mail:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(60,5,awisFormat('TP',$rsCAD['CAP_EMAIL'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	//**************************************************************
	//** Fahrzeuginfos
	//**************************************************************

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);

	$Ausdruck->_pdf->Cell(50,5,'Anzahl Fahrzeuge f�r ATU:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(40,5,$rsCAD['CPO_ANZPKW'][$CADZeile].' PKW',0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,$rsCAD['CPO_ANZTRA'][$CADZeile].' Transporter',0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
	$Zeile++;

	//**************************************************************
	//** Art der Vereinbarung
	//**************************************************************

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','B',12);
	$Ausdruck->_pdf->Cell(100,5,'Art der Vereinbarung',0,0,'L',0);
	$Zeile+=6;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->Cell(50,5,'Rahmenvertrag:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(40,5,($rsCAD['RAHMENVERTRAG'][$CADZeile]==1?'Ja':'Nein'),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'Barzahler:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,($rsCAD['BARZAHLER'][$CADZeile]==1?'Ja':'Nein'),0,0,'L',0);

	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(60,5,'Zugang Flottenportal Firma:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,'Benutzer: '.awisFormat('TP',$rsCAD['PORTALFIRMAUSER'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,'Kennwort: '.awisFormat('TP',$rsCAD['PORTALFIRMAPWD'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(60,5,'Zugang Flottenportal Mitarbeiter:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,'Benutzer: '.awisFormat('TP',$rsCAD['PORTALMAUSER'][$CADZeile]),0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,'Kennwort: '.awisFormat('TP',$rsCAD['PORTALMAPWD'][$CADZeile]),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(50,5,'Legimitierung:',0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(50,5,'Gro�kundenausweis:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(40,5,($rsCAD['GROSSKUNDENAUSW'][$CADZeile]>0?awisFormat('TP',$rsCAD['GROSSKUNDENAUSW'][$CADZeile]):'Keine'),0,0,'L',0);
	$Ausdruck->_pdf->Cell(25,5,'ATU Card:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,($rsCAD['ATUCARD'][$CADZeile]==1?'Ja, laut beigef�gter Excel Liste':'Nein'),0,0,'L',0);
	$Zeile+=5;

    $Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    $Ausdruck->_pdf->Cell(50,5,'Kartenart:',0,0,'L',0);
    $Ausdruck->_pdf->Cell(20,5,$rsCAD['KARTENART'][$CADZeile],0,0,'L',0);
    $Zeile+=5;

    $Ausdruck->_pdf->SetXY($Spalte,$Zeile);
    $Ausdruck->_pdf->Cell(50,5,'Karten:',0,0,'L',0);
    $Ausdruck->_pdf->MultiCell(120,5,$rsCAD['KARTEN'][$CADZeile],0,'L',false);

    $Zeile = $Ausdruck->_pdf->getY();
    $Zeile += 5;
    
    $Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
	$Zeile++;
	
	//**************************************************************
	//** Konditionen
	//**************************************************************

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','B',12);
	$Ausdruck->_pdf->Cell(100,5,'Konditionen',0,0,'L',0);
	$Zeile+=6;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->Cell(50,5,'Rabattgruppen:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(35,5,'WG: '.$rsCAD['WG'][$CADZeile],0,0,'L',0);
	$Ausdruck->_pdf->Cell(35,5,'DL: '.$rsCAD['DL'][$CADZeile],0,0,'L',0);
	$Ausdruck->_pdf->Cell(35,5,'RH: '.$rsCAD['RH'][$CADZeile],0,0,'L',0);
	$Ausdruck->_pdf->Cell(35,5,'TH: '.$rsCAD['TH'][$CADZeile],0,0,'L',0);
	$Zeile+=5;
	
	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->Cell(50,5,'Komplettr�der freischalten:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,($rsCAD['KOMPLETTRAEDER'][$CADZeile]==1?'Ja':'Nein'),0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,'Komplett freischalten:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,($rsCAD['KOMPLETT'][$CADZeile]==1?'Ja':'Nein'),0,0,'L',0);
	$Zeile+=5;

	// Spezielle Kennzeichnung f�r OPAL
	if($rsCAD['DL_OPAL'][$CADZeile]!='')
	{
		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->Cell(50,5,'OPAL-DL: ',0,0,'L',0);
		$Ausdruck->_pdf->MultiCell(120,5,$rsCAD['DL_OPAL'][$CADZeile],0,'L',0);
		$Zeile = $Ausdruck->_pdf->GetY();
	}

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(50,5,'Umsatzbonus:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,($rsCAD['UMSATZBONUS'][$CADZeile]==1?'Ja':'Nein'),0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,'Fester Bonussatz:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,$rsCAD['FESTERBONUS'][$CADZeile].' %',0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(50,5,'Bonus gestaffelt:',0,0,'L',0);
	if($rsCAD['BONUS1'][$CADZeile]!='0,00')
	{
		$Ausdruck->_pdf->Cell(30,5,'Ab '.awisFormat('N2',$rsCAD['BONUS1AB'][$CADZeile]),0,0,'L',0);
		$Ausdruck->_pdf->Cell(20,5,$rsCAD['BONUS1'][$CADZeile].' %',0,0,'L',0);
	}
	else
	{
		$Ausdruck->_pdf->Cell(30,5,'Nein',0,0,'L',0);
	}
	$Zeile+=5;

	if($rsCAD['BONUS2'][$CADZeile]!='0,00')
	{
		$Ausdruck->_pdf->SetXY($Spalte+50,$Zeile);
		$Ausdruck->_pdf->Cell(30,5,'Ab '.awisFormat('N2',$rsCAD['BONUS2AB'][$CADZeile]),0,0,'L',0);
		$Ausdruck->_pdf->Cell(20,5,$rsCAD['BONUS2'][$CADZeile].' %',0,0,'L',0);
		$Zeile+=5;
	}

	if($rsCAD['BONUS3'][$CADZeile]!='0,00')
	{
		$Ausdruck->_pdf->SetXY($Spalte+50,$Zeile);
		$Ausdruck->_pdf->Cell(30,5,'Ab '.awisFormat('N2',$rsCAD['BONUS3AB'][$CADZeile]),0,0,'L',0);
		$Ausdruck->_pdf->Cell(20,5,$rsCAD['BONUS3'][$CADZeile].' %',0,0,'L',0);
		$Zeile+=5;
	}

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(50,5,'Zu beachten:',0,0,'L',0);
	$Ausdruck->_pdf->MultiCell(120,5,awisFormat('TP',$rsCAD['BONUSBEM'][$CADZeile]),0,'L',0);
	$Zeile = $Ausdruck->_pdf->GetY();

	$Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
	$Zeile++;

	//**************************************************************
	//** Rechnungsstellung
	//**************************************************************

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','B',12);
	$Ausdruck->_pdf->Cell(100,5,'Rechnungsstellung',0,0,'L',0);
	$Zeile+=6;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->Cell(50,5,'Rechnungsstellung:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,awis_WertListe($AWISSprachKonserven['CAD']['lst_Rechnungsstellung'],$rsCAD['RECHNUNGSSTELLUNG'][$CADZeile],'nicht festgelegt'),0,0,'L',0);
	$Ausdruck->_pdf->Cell(30,5,'Rechnungsart:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(30,5,awis_WertListe($AWISSprachKonserven['CAD']['lst_Rechnungsart'],$rsCAD['RECHNUNGSART'][$CADZeile],'nicht festgelegt'),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->Cell(50,5,'Zus�tzlich EDI:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,($rsCAD['EDI'][$CADZeile]==1?'Ja':'Nein'),0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','',10);
	$Ausdruck->_pdf->Cell(50,5,'Zahlungsziel:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(50,5,awis_WertListe($AWISSprachKonserven['CAD']['lst_Zahlungsziel'],$rsCAD['ZAHLUNGSZIEL'][$CADZeile],'nicht festgelegt'),0,0,'L',0);
	$Ausdruck->_pdf->Cell(30,5,'Skonto:',0,0,'L',0);
	$Ausdruck->_pdf->Cell(20,5,$rsCAD['SKONTO'][$CADZeile].' %',0,0,'L',0);
	$Zeile+=5;

	$Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
	$Zeile++;

	//**************************************************************
	//** Info f�r Filialen
	//**************************************************************

	$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
	$Ausdruck->_pdf->SetFont('Arial','B',12);
	$Ausdruck->_pdf->Cell(100,5,'Info f�r Filialen',0,0,'L',0);
	$Zeile+=6;

	$Felder = array('Serviceumfang'=>'INFO_SERVICE',
					'zu beachten'=>'INFO_BEACHTEN',
					'Freigaben'=>'INFO_FREIGABEN',
					'Abrechnung'=>'INFO_ABRECHNUNG');


	foreach($Felder AS $Bez=>$Feld)
	{
		$Ausdruck->_pdf->SetAutoPageBreak(true,15);
		$StartZeile = $Zeile;

		$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
		$Ausdruck->_pdf->SetFont('Arial','',10);
		$Ausdruck->_pdf->Cell(30,5,$Bez.':',0,0,'L',0);
		$Ausdruck->_pdf->MultiCell(140,5,awisFormat('TP',$rsCAD[$Feld][$CADZeile]),0,'L',0);
		$Zeile = $Ausdruck->_pdf->GetY();
		if($Zeile<$StartZeile)
		{
			$Ausdruck->_pdf->SetAutoPageBreak(false);
			$Ausdruck->_pdf->SetFont('Arial','',8);
			$Ausdruck->_pdf->SetXY($Ausdruck->SeitenBreite()-20,10);
			$Ausdruck->_pdf->Cell(20,5,'Seite '.$Ausdruck->_pdf->PageNo());
			$Ausdruck->_pdf->SetXY(10,$Ausdruck->SeitenHoehe()-10);
			$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,$AWISSprachKonserven['Ausdruck']['txtFirmaVertraulich'],0,0,'L',0);
			$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,'Kundenstammblatt',0,0,'C',0);
			$Ausdruck->_pdf->Cell(($Ausdruck->SeitenBreite()-20)/3,5,date('d.m.Y'),0,0,'R',0);
		}
	}

	//**************************************************************
	//** Ausdruck liefern
	//**************************************************************
	$Ausdruck->_pdf->Line(20,$Zeile,$Ausdruck->SeitenBreite()-20,$Zeile);
	$Zeile++;

	if($DruckArt==1)
	{
		$Ausdruck->Anzeigen();
	}
}	// Jeden Kunden

// Ende des Massendrucks?
if($DruckArt==2 AND $Ausdruck!=null)
{
	@ob_clean();
	$Ausdruck->Anzeigen();
}
awisLogoff($con);
?>