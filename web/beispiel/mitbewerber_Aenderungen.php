<?php
global $con;
global $Recht3010;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('MBW','MBW_%');
$TextKonserven[]=array('MAE','MAE_%');
$TextKonserven[]=array('Wort','AnzAenderungen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Liste','lst_AktivInaktiv');

$TXT_TextBaustein = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3010 = awisBenutzerRecht($con,3010,$_SERVER['PHP_AUTH_USER']);
$Recht3011 = awisBenutzerRecht($con,3011,$_SERVER['PHP_AUTH_USER']);

if($Recht3011==0)
{
	die();
}


$SQL = 'SELECT count(*) AS ANZ, MBW_KEY, MBW_NAME1';
$SQL .= ' FROM MitbewerbAenderungen ';
$SQL .= ' INNER JOIN Mitbewerber ON MBW_KEY = MAE_MBW_KEY';
$Bedingung='';
if(!isset($_GET['ALLE']))
{
	$Bedingung .= ' AND MAE_STATUS=\'O\'';
}
if(($Recht3011&40)==0)
{
	$Bedingung .= ' AND MAE_ANTRAGSTELLER=\''.$_SERVER['PHP_AUTH_USER'].'\'';
}

if($Bedingung!='')
{
	$SQL .= ' WHERE '.substr($Bedingung,4);
}
$SQL .= ' GROUP BY MBW_KEY, MBW_NAME1';
$SQL .= ' ORDER BY 1 DESC';

$rsMAE = awisOpenRecordset($con,$SQL);
$rsMAEZeilen = $awisRSZeilen;

awis_FORM_FormularStart();

awis_FORM_ZeileStart();
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['MBW']['MBW_NAME1'],200);
awis_FORM_Erstelle_Liste_Ueberschrift($TXT_TextBaustein['Wort']['AnzAenderungen'],200);
awis_FORM_ZeileEnde();

for($MAEZeile=0;$MAEZeile<$rsMAEZeilen;$MAEZeile++)
{
	awis_FORM_ZeileStart();
	
	$Link = './mitbewerber_Main.php?cmdAktion=Details&Seite=Aenderungen&MBW_KEY='.$rsMAE['MBW_KEY'][$MAEZeile];
	awis_FORM_Erstelle_ListenFeld('MBW_NAME1',$rsMAE['MBW_NAME1'][$MAEZeile],20,200,false,($MAEZeile%2),'',$Link,'T','');
	awis_FORM_Erstelle_ListenFeld('*ANZAHL',$rsMAE['ANZ'][$MAEZeile],20,200,false,($MAEZeile%2),'','','Z','');
	
	awis_FORM_ZeileEnde();
}



awis_FORM_FormularEnde();

?>