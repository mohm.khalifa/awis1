<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";

$con = awisLogon();
$RechteStufe = awisBenutzerRecht($con,6);
?>

<title></title>
</head>

<body>

<h2>AWIS - Benutzerparameter</h2>

In diesem Formular k�nnen Sie die benutzerabh�ngigen Programmparameter �ndern.
<table border=1 width=100%>
<tr>
	<td id=FeldBez>Parameter</td>
	<td id=FeldBez>Erl�uterung</td>
</tr>

<tr>
	<td>BildschrimBreite</td>
	<td>Hier wird die Breite des Bildschirms in Pixeln festgelegt.<br>
	    Sie k�nnen hier den Wert aus Ihrer Bildschirmaufl�sung angeben, d.h. bei 1024*762 eine 1024.<br>
		Dieser Wert wirkt sich auf die Darstellung der Tabellen in einigen Masken aus (z.B. Reifenliste).
	</td>
</tr>
<tr></tr>
	<td>Reifentyp</td>
	<td>Mit diesem Wert wird die Vorauswahl bei der Reifensuche festgelegt.<br>
		M�gliche Werte sind:<br>
		1=Sommer<br>
		2=Winter<br>
	</td>
</tr>
</table>

<?php
if($RechteStufe>=2)
{
echo <<<STUFE2
<br>Folgende Parameter der Stufe 2 sind verf�gbar.<br>
<table border=1 width=100%>
<tr>
	<td id=FeldBez>Parameter</td>
	<td id=FeldBez>Erl�uterung</td>
</tr>


<tr>
	<td>FilialInfo_Version</td>
	<td>Ausgabeformat der Filialinfo. M�gliche Werte sind:<br>
		000:	Standardformat.<br>
		Achtung: Wird eine unbekannte Version eingegeben, so kann die Filialinfo nicht mehr ge�ffnet werden.</b></td>
</tr>
</table>

STUFE2;
}

awisLogoff($con);

?>
</body>

</html>
