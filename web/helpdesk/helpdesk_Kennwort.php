<?php
global $awisDBFehler;
global $awisRSZeilen;

echo '<table border=0 width=400>';
echo '</table>';


$RechteStufe = awisBenutzerRecht($con, 1900);

if(($RechteStufe & 2)==0)
{
    awisEreignis(3,1900,'Helpdesk-Kennw�rter',$_SERVER['PHP_AUTH_USER'],'','','');
	awisLogoff($con);
    die("Keine ausreichenden Rechte!");
}



echo '<span class=HinweisText>';
echo 'Durch die Umstellung auf Dom&auml;nenauthentifizierung ist keine &Auml;nderung des Kennworts &uuml;ber die AWIS Oberfl&auml;che mehr m&ouml;glich!';
echo '</span>';
die();


//***********************************
// Einen Benutzer aussuchen
//***********************************
if(!isset($_POST['txtXBL_Key']) OR (isset($_POST['txtXBL_Key']) AND $_POST['txtXBL_Key']==-1))
{
	echo '<br>';
    echo "<form name=frmBenutzer method=post action=./helpdesk_Main.php?cmdAktion=Kennwort>";

    echo '<span class=Ueberschrift>Zur�cksetzen eines Kennworts</span>';
	echo '<br/><br/>';
    echo "<table boder=1>";


    echo '<tr><td>Benut<u>z</u>er:</td>';
    echo "<td><select accesskey=z name=txtXBL_Key size=1>";
    echo '<option value=-1>::Benutzer ausw�hlen::</option>';
	$EingabeFeld = 'txtXBL_Key';
    $rsBenutzer = awisOpenRecordset($con, "SELECT XBL_Key, XBL_XBN_Key, XBL_Login, XBN_VOLLERNAME FROM AWIS.BenutzerLogins INNER JOIN Benutzer ON XBL_XBN_Key = XBN_Key WHERE UPPER(SUBSTR(XBL_LOGIN,1,4)) <> 'FIL-' ORDER BY XBL_Login");
    $rsBenutzerZeilen = $awisRSZeilen;

    for($i = 0; $i<$rsBenutzerZeilen; $i++)
    {
        echo "<option value=" . $rsBenutzer["XBL_KEY"][$i] . ">" . $rsBenutzer["XBL_LOGIN"][$i] . " - " . $rsBenutzer["XBN_VOLLERNAME"][$i] . "</option>";
    }

    echo "</select></td></tr>";

    echo '<tr><td colspan=2>';
	echo "<br>&nbsp;<input tabindex=99 type=image src=/bilder/eingabe_ok.png title='Kennwort zur�cksetzen' name=cmdSuche value=\"Aktualisieren\">";
    echo "</td></tr></table>";

    echo "</form>";

}
else
{
	$recXBN = awisOpenRecordset($con, "SELECT * FROM Benutzer INNR JOIN BenutzerLogins ON XBN_Key = XBL_XBN_Key WHERE XBL_Key=" . $_POST['txtXBL_Key']);

	if($recXBN !== false)
	{
		$NeuesKennwort = date('dmHis');

		// Meldung unterdr�cken
		@system("/daten/zugang/newuser.sh " . strtolower($recXBN['XBL_LOGIN'][0]) . " \"" . $NeuesKennwort . "\"",$erg);

		sleep(2);
		if($erg != 0)
		{
			echo "<br><br>Fehler beim Kennwort �ndern. Fehlercode: $erg<br><br>";
		}
		else
		{
				// Protokoll schreiben
			$SQL = ' INSERT INTO AWIS.HELPDESKPROTOKOLL (HDP_XXX_KEY, HDP_AENDERUNG, HDP_BEMERKUNG, HDP_USER, HDP_USERDAT)';
			$SQL .= ' VALUES(';
			$SQL .= "".$recXBN['XBN_KEY'][0];
			$SQL .= ",'" . "Das Kennwort des Anwenders ". $recXBN['XBN_VOLLERNAME'][0] ." wurde ge�ndet zu " . $NeuesKennwort . ".'";
			$SQL .= ", ''";
			$SQL .= ", '" . $_SERVER['PHP_AUTH_USER'] . "'";
			$SQL .= ", SYSDATE";
			$SQL .= ")";

			if(awisExecute($con,$SQL)==false)
			{
				awisErrorMailLink("17080857-helpdesk_Kennwort.php", 2, $awisDBFehler['message']);
				awis_Debug(1,$SQL);
			}

			// Passwort muss ge�ndert werden			
			awis_BenutzerParameterSpeichern($con,"Passwortaendern",$recXBN['XBL_LOGIN'][0],1);
			$dat = getdate();
			echo "<br><br>Das Kennwort des Anwenders " . $recXBN['XBL_LOGIN'][0] . " wurde ge�ndet zu <font size=larger color=#FF5555>" . $NeuesKennwort . "</font>.<br>";

			echo "<br><br>";

			$SQL = 'SELECT KKO_Wert FROM KontakteKommunikation';
			$SQL .= ' WHERE KKO_KOT_Key=7 AND KKO_KON_Key=0' . $recXBN['XBN_KON_KEY'][0];
			$recKOT = awisOpenRecordset($con,$SQL);

			if(isset($recKOT['KKO_WERT'][0]))
			{
				echo "<a href=mailto:" . $recKOT['KKO_WERT'][0] . "?Subject=AWIS:%20Helpdesk";
			}
			else
			{
				echo "<a href=mailto:" . $recXBN['XBL_LOGIN'][0] . "?Subject=AWIS:%20Helpdesk";
			}

			echo "&Body=";
			echo "Ihr%20Kennwort%20wurde%20zurueckgesetzt.%20";
			echo "Bitte%20verwenden%20Sie%20das%20Kennwort%20";
			echo $NeuesKennwort;
			echo "%20um%20sich%20anzumelden.";
			echo "%20%20%20";
			echo "Ihr%20AWIS%20Team.";
			echo ">";
			echo "Benutzer per E-Mail benachrichtigen</a>";
		}
	}

	echo '<br><br><a title=Zur�ck href=./helpdesk_Main.php?cmdAktion=Kennwort>Zur�ck</a>';
}


if(isset($EingabeFeld))
{
	echo '<script language="JAVAScript" type="text/javascript">';
	echo 'document.getElementsByName(' . $EingabeFeld . ')[0].focus();';
	echo '</script>';
}
?>
