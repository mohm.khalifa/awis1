
<?php

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');
include_once('awisFirstglasImport.inc.php');

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FFR', '%');
    $TextKonserven[] = array('FIL', '%');
    $TextKonserven[] = array('FRG', '%');
    //$TextKonserven[]=array('FIL','*');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungsscheinDrucken');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungsdifferenzenDrucken');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungslisteDrucken');
    $TextKonserven[] = array('Wort', 'KeineBerechtigungRueckfuehrung');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[] = array('Wort', 'Abschlussdatum');
    $TextKonserven[] = array('RFS', 'lst_RFS_STATUS');
    $TextKonserven[] = array('Wort', 'Text');


    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $speichern = false;

    $Recht9101 = $AWISBenutzer->HatDasRecht(9101);
    if (($Recht9101) == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    $Form->DebugAusgabe(1,$_POST);

    $FRG_KEY = "";
    $AWIS_KEY1 = -1;

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details>");

    $Form->Formular_Start();

    $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasRueckfuehrung'));

    //$Form->DebugAusgabe(1,$_POST);
    //$Form->DebugAusgabe(1,$_POST);
    //var_dump($Param);

    if (isset($_GET['FFR_KEY']))
    {
        $AWIS_KEY1 = $_GET['FFR_KEY'];
    }
    elseif (isset($_REQUEST['txtFFR_KEY']))
    {
        $AWIS_KEY1 = $_REQUEST['txtFFR_KEY'];
    }

    if (isset($_REQUEST['txtFRG_KEY']))
    {

        $FRG_KEY = $_REQUEST['txtFRG_KEY'];
        $Form->Erstelle_HiddenFeld('FRG_KEY', $FRG_KEY);
    }
    if (isset($_POST['cmd_weiter_x']) || isset($_POST['txtspeichern']) || isset($_POST['cmdSpeichernFirstglasOK']) || isset($_POST['cmdSpeichernFirstglasGruendeOK']))
    {
        $speichern = true;

        if (isset($_POST['cmd_weiter_x']))
        {
            include './firstglasfilrueckfuehrung_speichern.php';
            $AWIS_KEY1 = -1;
            $speichern = true;
        }
        if (isset($_POST['cmdSpeichernFirstglasOK']))
        {
            include './firstglasfilrueckfuehrung_speichern.php';
            $AWIS_KEY1 = -1;
            $speichern = true;
        }
        /*
          if(@$_POST['speichern'] == 0)
          {
          $speichern = false;
          }
         */

        if (isset($_POST["cmdSpeichernFirstglasGruendeNEIN"]))
        {
            $speichern = false;
        }

        if (@$_POST["txtSonstiges"] == "")
        {
            $speichern = true;
        }
        else
        {
            $speichern = false;
        }

        if (isset($_POST["cmdSpeichernFirstglasNEIN"]))
        {
            $speichern = false;
        }

        if (isset($_POST['cmdSpeichernFirstglasGruendeOK']))
        {
            include './firstglasfilrueckfuehrung_speichern.php';
            $AWIS_KEY1 = -1;

            $speichern = false;
        }

        if (isset($_POST["cmdSpeichernFirstglasGruendeNEIN"]))
        {
            $speichern = false;
        }
        /*
          if(isset($_POST["txtspeichern"]) == 0)
          {
          $speichern = false;
          }
         */

        //$Form->DebugAusgabe(1,$_POST);
    }

    $TextKonserven = array();
    $TextKonserven[] = array('FFR', '%');
    $TextKonserven[] = array('FIL', '%');
    $TextKonserven[] = array('FRG', '%');
    //$TextKonserven[]=array('FIL','*');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungsscheinDrucken');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungsdifferenzenDrucken');
    $TextKonserven[] = array('Wort', 'lbl_RueckfuehrungslisteDrucken');
    $TextKonserven[] = array('Wort', 'KeineBerechtigungRueckfuehrung');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');
    $TextKonserven[] = array('Wort', 'Abschliessen');
    $TextKonserven[] = array('Wort', 'Abbrechen');
    $TextKonserven[] = array('Wort', 'Abschlussdatum');
    $TextKonserven[] = array('RFS', 'lst_RFS_STATUS');
    $TextKonserven[] = array('Wort', 'Text');

    //$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    //First
    if ($speichern == false)
    {
        if (!isset($_GET['Sort']))
        {
            $ORDERBY = ' ORDER BY FFR_FILID DESC';
        }
        else
        {
            $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
        }
        $Param['FIL_ID'] = (isset($_POST['sucFIL_ID']) ? isset($_POST['sucFIL_ID']) : $Param['FIL_ID']);
        $Param['ORDERBY'] = $ORDERBY;
        $Param['BLOCK'] = (isset($_REQUEST['Block']) ? $_REQUEST['Block'] : 1);

        $AWISBenutzer->ParameterSchreiben('FirstglasDetails', implode(';', $Param));
        $Form->DebugAusgabe(1,$Param);

        $FilZugriff = $AWISBenutzer->FilialZugriff(null, awisBenutzer::FILIALZUGRIFF_STRING);

        if ($FilZugriff != '')
        {
            if (strpos($FilZugriff, ',') !== false)
            {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'] . ':', 140);
                $SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
                $SQL .= ' FROM Filialen ';
                $SQL .= ' WHERE FIL_ID IN (' . $FilZugriff . ')';
                $SQL .= ' ORDER BY FIL_BEZ';
                $Form->Erstelle_SelectFeld('*FIL_ID', $Param['FIL_ID'], 150, true, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
                $Form->ZeileEnde();
                $AWISCursorPosition = 'sucFIL_ID';
            }
            elseif ($Param['FIL_ID'] == '')
            {
                $Param['FIL_ID'] = $FilZugriff;
            }
        }
        else
        {
            if ($AWIS_KEY1 == -1)
            {
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'] . ':', 140);
                $Form->Erstelle_TextFeld('*FIL_ID', $Param['FIL_ID'], 10, 180, true, '', '', '', 'T', 'L', '', '', 10);
                $Form->ZeileEnde();
                $AWISCursorPosition = 'sucFIL_ID';
            }
        }
        //echo $AWIS_KEY1;
        if ($Param['FIL_ID'] != '' && $AWIS_KEY1 == -1)
        {
            $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung', serialize($Param));

            $Form->Trennzeile();

            $SQL = 'SELECT FGRUECKFUEHRUNG.*';
            $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
            $SQL .= ' FROM FGRUECKFUEHRUNG';
            $SQL .= ' WHERE FFR_FILID = 0' . $DB->FeldInhaltFormat('N0', $Param['FIL_ID']);
            $SQL .= ' AND FFR_STATUS=0';
            $SQL .= ' ' . $ORDERBY;

            //echo $SQL;

            $Block = 1;
            if (isset($_REQUEST['Block']))
            {
                $Block = $Form->Format('N0', $_REQUEST['Block'], false);
                $Param['BLOCK'] = $Block;
                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung', serialize($Param));
            }
            elseif (isset($Param['BLOCK']))
            {
                $Block = $Param['BLOCK'];
            }

            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

            $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);

            $rsFFR = $DB->RecordSetOeffnen($SQL);
            //$Form->DebugAusgabe(1,$rsFFR);

            if ($rsFFR->EOF())
            {
                $Form->ZeileStart();
                //$Form->Hinweistext($AWISSprachKonserven['FFR']['err_KeineBestellungen']);
                echo "Keine Datensaetze gefunden";
                $Form->ZeileEnde();
            }
            else
            {
                $Form->ZeileStart();
                $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
                $Link .= '&Sort=FFR_KEY' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_KEY')) ? '~' : '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KEY'], 80, '');
                $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
                $Link .= '&Sort=FFR_FILID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_FILID')) ? '~' : '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_FILID'], 100, '');
                $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
                $Link .= '&Sort=FFR_WANR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_WANR')) ? '~' : '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_WANR'], 100, '');
                $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
                $Link .= '&Sort=FFR_AEMBETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_AEMBETRAG')) ? '~' : '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'], 110, '');
                $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
                $Link .= '&Sort=FFR_KASSENBETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_KASSENBETRAG')) ? '~' : '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'], 110, '');
                $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
                $Link .= '&Sort=FFR_DIFFERENZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FFR_DIFFERENZ')) ? '~' : '');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'], 110, '');
                $Form->ZeileEnde();

                $DS = 0;
                while (!$rsFFR->EOF())
                {
                    $Form->ZeileStart();
                    $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details&FFR_KEY=' . $rsFFR->FeldInhalt('FFR_KEY') . '';
                    $Form->Erstelle_ListenFeld('*FFR_KEY', $rsFFR->FeldInhalt('FFR_KEY'), 0, 80, false, ($DS % 2), '', $Link, 'NO');
                    $Form->Erstelle_ListenFeld('*FFR_FILID', $rsFFR->FeldInhalt('FFR_FILID'), 0, 100, false, ($DS % 2), '', '', 'NO');
                    $Form->Erstelle_ListenFeld('*FFR_WANR', $rsFFR->FeldInhalt('FFR_WANR'), 0, 100, false, ($DS % 2), '', '', 'T');
                    $Form->Erstelle_ListenFeld('*FFR_AEMBETRAG', $rsFFR->FeldInhalt('FFR_AEMBETRAG'), 0, 110, false, ($DS % 2), '', '', 'N2');
                    $Form->Erstelle_ListenFeld('*FFR_KASSENBETRAG', $rsFFR->FeldInhalt('FFR_KASSENBETRAG'), 0, 110, false, ($DS % 2), '', '', 'N2');
                    $Form->Erstelle_ListenFeld('*FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZ'), 0, 110, false, ($DS % 2), '', '', 'N2');
                    $Form->ZeileEnde();

                    $rsFFR->DSWeiter();
                    $DS++;
                }
                $Link = './firstglasfilrueckfuehrung_Main.php?cmdAktion=Details';
                $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

                $Form->Formular_Ende();

                $Form->SchaltflaechenStart();
                $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
                $Form->SchaltflaechenEnde();
            }
            $Form->SchreibeHTMLCode('</form>');
        }
        else
        {
            $Form->Formular_Start();
            $Form->SchreibeHTMLCode('<form name=frmFirstGlasRueckfuehrung action=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

            $SQL = 'Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_KEY1) . ' AND FFR_STATUS=0';

            $rsFFR = $DB->RecordSetOeffnen($SQL);

            if ($AWIS_KEY1 != -1)
            {
                $AWIS_KEY1 = $rsFFR->FeldInhalt('FFR_KEY');
                $Param['KEY'] = $AWIS_KEY1;
                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung', serialize($Param));
                $Form->Erstelle_HiddenFeld('FFR_KEY', $AWIS_KEY1);
            }
            else
            {
                $AWIS_KEY1 = -1;
                $AWIS_KEY2 = -1;
            }

            // Infozeile zusammenbauen

            $Felder = array();
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details&FFRListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $AWISBenutzer->BenutzerName());
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', date('Ymd H:i:s')));
            $Form->InfoZeile($Felder, '');

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Scheibenaustausch durch Autoglas', 0, 400, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Sehr geehrte Damen und Herren,', 0, 400, false);
            $Form->ZeileEnde();
            
            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Text = ' bei der Kontrolle dieser Autoglas Rechnung wurde festgestellt, dass bei folgendem Scheibenaustausch';
            $Text .= ' der von Ihnen über den Werkstattauftrag berechnete Betrag von der Autoglas Auftragsbestätigung';
            $Text .= ' abweicht.';
            $Form->Erstelle_Textarea('TEXT', $Text, 700, 70, 4, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');
            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_FILID'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_FILID', $rsFFR->FeldInhalt('FFR_FILID'), 10, 150, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_WANR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_WANR', $rsFFR->FeldInhalt('FFR_WANR'), 10, 120, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_VORGANGNR', $rsFFR->FeldInhalt('FFR_VORGANGNR'), 50, 150, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KFZKENNZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_KFZKENNZ', $rsFFR->FeldInhalt('FFR_KFZKENNZ'), 10, 120, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_AEMBETRAG', $rsFFR->FeldInhalt('FFR_AEMBETRAG'), 7, 150, false, '', '', '', 'N2');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'] . ':', 130);
            $Form->Erstelle_TextFeld('FFR_KASSENBETRAG', $rsFFR->FeldInhalt('FFR_KASSENBETRAG'), 30, 100, false, '', '', '', 'N2');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'] . ':', 100);
            $Form->Erstelle_TextFeld('FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZ'), 30, 70, false, '', '', '', 'N2');
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FRG']['FRG_BEZEICHNUNG'] . ':', 100);
            $SQL = 'SELECT FRG_KEY, CASE WHEN LENGTH(FRG_BEZEICHNUNG)>100 THEN SUBSTR(FRG_BEZEICHNUNG,1,100)||\'..\' ELSE FRG_BEZEICHNUNG END AS TEXT FROM FGRUECKFUEHRUNGSGRUENDE';
            $SQL .= ' WHERE FRG_AKTIV = 1';
            $SQL .= ' ORDER BY FRG_KEY';
            $Form->Erstelle_SelectFeld('FRG_KEY', '', 200, true, $SQL, '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');
            $Form->Trennzeile('O');
            $Form->Trennzeile('O');
            $Form->Trennzeile('O');
            $Form->Trennzeile('O');

            $Form->Formular_Ende();
            $Form->SchaltflaechenStart();
            $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
            $Form->Schaltflaeche('image', 'cmd_weiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');

            if (($Recht9101 & 4) == 4)
            {
                $Form->Erstelle_HiddenFeld('FFR_KEY', $rsFFR->FeldInhalt('FFR_KEY'));
            }
            //$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=preisauskunft&Aktion=protokoll','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

            $Form->SchaltflaechenEnde();

            $Form->SchreibeHTMLCode('</form>');
        }
    }
}
catch (Exception $ex)
{
    die($ex->getMessage());
}
?>
