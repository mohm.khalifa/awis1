<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	if(isset($_POST['txtZLA_KEY']))
	{
		//***********************************************
		// zukaufartikel
		//***********************************************
		$AWIS_KEY1=$_POST['txtZLA_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtZLA_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ZLA','ZLA_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('ZLA_ARTIKELNUMMER','ZLA_ZLH_KEY');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZLA'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				//die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			$_POST['txtZLA_ARTIKELNUMMER'] = strtoupper($_POST['txtZLA_ARTIKELNUMMER']);
			
			if(floatval($_POST['txtZLA_KEY'])==0)
			{
				$Speichern = true;

				if($Speichern)
				{
					$Fehler = '';
					$SQL = 'INSERT INTO zukaufartikel';
					$SQL .= '(ZLA_ARTIKELNUMMER,ZLA_ZLH_KEY,ZLA_BEZEICHNUNG,ZLA_TAUSCHTEIL,ZLA_GUELTIGAB,ZLA_GUELTIGBIS';
					$SQL .= ',ZLA_STATUS,ZLA_AID_NR';
					$SQL .= ',ZLA_USER, ZLA_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtZLA_ARTIKELNUMMER'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZLA_ZLH_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZLA_BEZEICHNUNG'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZLA_TAUSCHTEIL'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtZLA_GUELTIGAB'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtZLA_GUELTIGBIS'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZLA_STATUS'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZLA_AID_NR'],true);
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$DB->Ausfuehren($SQL,'',false);
					$SQL = 'SELECT seq_ZLA_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
				}
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM zukaufartikel WHERE ZLA_key=' . $_POST['txtZLA_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('ZLA_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE zukaufartikel SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ZLA_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ZLA_userdat=sysdate';
					$SQL .= ' WHERE ZLA_key=0' . $_POST['txtZLA_KEY'] . '';
					$DB->Ausfuehren($SQL,'',false);
					
					// ATU Nummern Zuordnung wurde ge�ndert -> Historie in IDs schreiben
					if($_POST['oldZLA_AST_ATUNR']!='' AND $_POST['oldZLA_AST_ATUNR']!=$_POST['txtZLA_AST_ATUNR'])
					{
						$SQL = 'INSERT INTO ZUKAUFLIEFERANTENARTIKELIDS';
						$SQL .= '(ZAI_ZLA_KEY, ZAI_AID_NR, ZAI_ID, ZAI_IMQ_ID, ZAI_USER)';
						$SQL .= ' VALUES (';
						$SQL .= ' '.$_POST['txtZLA_KEY'];
						$SQL .= ', 6'; 		// Ausgelaufene ATU-Nr
						$SQL .= ', '.$DB->FeldInhaltFormat('T',$_POST['oldZLA_AST_ATUNR'],false);
						$SQL .= ', 4';		// Manuell
						$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
						$SQL .= ')';
						$DB->Ausfuehren($SQL,'',false);					
					}
				}
			}
		}
	}

	//***********************************************
	// zukaufartikellieferanten
	//***********************************************
	if(isset($_POST['txtZAL_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtZLA_KEY'],false);
$Form->DebugAusgabe(1,$_POST);
		$AWIS_KEY2=$_POST['txtZAL_KEY'];

		$Felder = $Form->NameInArray($_POST, 'txtZAL_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ZAL','ZAL_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('ZAL_LIE_NR','ZAL_MENGENEINHEIT','ZAL_EINHEITENPROVK','ZAL_BESTELLNUMMER');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZAL'][$Pflichtfeld].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if(floatval($_POST['txtZAL_KEY'])==0)
			{
				$Speichern = true;

				if($Speichern)
				{
					$Fehler = '';
					$SQL = 'INSERT INTO ZUKAUFARTIKELLIEFERANTEN';
					$SQL .='(ZAL_ZLA_KEY,  ZAL_LIE_NR,  ZAL_BEZEICHNUNG,  ZAL_WG1,  ZAL_WG2,  ZAL_WG3';
					$SQL .= ',ZAL_EINHEITENPROVK,  ZAL_MENGENEINHEIT,  ZAL_BESTELLNUMMER,  ZAL_BEWERTUNG';
					$SQL .= ',ZAL_BEMERKUNG, ZAL_SPERRE,  ZAL_SPERRGRUND,  ZAL_USER, ZAL_CREADAT, ZAL_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$AWIS_KEY1,true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_LIE_NR'],false);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_BEZEICHNUNG'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_WG1'],false);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_WG2'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_WG3'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZAL_EINHEITENPROVK'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_MENGENEINHEIT'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_BESTELLNUMMER'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZAL_BEWERTUNG'],false);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_BEMERKUNG'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZAL_SPERRE'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAL_SPERRGRUND'],true);
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$DB->Ausfuehren($SQL,'',false);
					$SQL = 'SELECT seq_ZAL_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
				}
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM zukaufartikellieferanten WHERE ZAL_key=' . $_POST['txtZAL_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('ZAL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE zukaufartikellieferanten SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ZAL_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ZAL_userdat=sysdate';
					$SQL .= ' WHERE ZAL_key=0' . $_POST['txtZAL_KEY'] . '';
					$DB->Ausfuehren($SQL,'',false);
				}
			}
		}
	}


	//*******************************************************************
	//* Zusatzids speichern
	//*******************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZAI_ID_',1,1));
	if(count($Felder)>0)
	{
		$Form->DebugAusgabe(1,$_POST);
		foreach($Felder AS $Feld)
		{
			if($DB->FeldInhaltFormat('T',$_POST[$Feld])!==$DB->FeldInhaltFormat('T',$_POST['old'.substr($Feld,3)]))
			{
				$_POST[$Feld] = strtoupper($_POST[$Feld]);
				
				$ID = explode('_',$Feld);
				if($ID[2]==0)
				{
					$SQL = 'INSERT INTO ZukaufLieferantenArtikelIDs';
					$SQL .= '(ZAI_ZLA_KEY,ZAI_AID_NR,ZAI_ID,ZAI_USER,ZAI_USERDAT,ZAI_IMQ_ID)';
					$SQL .= 'VALUES(';
					$SQL .= ''.$DB->WertSetzen('ZAI','N0',$AWIS_KEY1);
					$SQL .= ', '.$DB->WertSetzen('ZAI', 'N0', $ID[3]); // ATUNR
					$SQL .= ', '.$DB->WertSetzen('ZAI','T',$_POST[$Feld]);
					$SQL .= ', '.$DB->WertSetzen('ZAI','T',$AWISBenutzer->BenutzerName());
					$SQL .= ', sysdate';
					$SQL .= ', 4';	// Importquelle (manuell)
					$SQL .= ')';

					$DB->Ausfuehren($SQL,'',true,$DB->Bindevariablen('ZAI'));
				}
				else
				{
					if($_POST[$Feld]==='')
					{
						$SQL = 'DELETE FROM ZukaufLieferantenArtikelIds';
						$SQL .= ' WHERE ZAI_KEY = 0'.$DB->FeldInhaltFormat('N0',$ID[2]);
					}
					else
					{
						$SQL = 'UPDATE ZukaufLieferantenArtikelIds';
						$SQL .= ' SET ZAI_ID = '.$DB->FeldInhaltFormat('T',$_POST[$Feld]);
						$SQL .= ' , ZAI_USER = '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
						$SQL .= ' , ZAI_UserDat = sysdate';
						$SQL .= ' , ZAI_IMQ_ID = 4';
						$SQL .= ' WHERE ZAI_KEY = 0'.$DB->FeldInhaltFormat('N0',$ID[2]);
					}
					$DB->Ausfuehren($SQL,'',false);
				}
			}
		}
	}


	//*************************************************************************************
	// Preise
	//*************************************************************************************
	if(isset($_POST['txtZAP_KEY']))
	{
		$Felder = $Form->NameInArray($_POST, 'txtZAP_',1,1);

		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('ZAP','ZAP_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);

			$Fehler = '';
			$Pflichtfelder = array('ZAP_VKPR','ZAP_ZAL_KEY','ZAP_MWS_ID','ZAP_EKPR','ZAP_EINHEITENPROVK','ZAP_RABATT1','ZAP_RABATT2','ZAP_ALTTEILWERT');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZAP'][$Pflichtfeld].'<br>';
				}
			}
				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';

			if($DB->FeldInhaltFormat('N0',$_POST['txtZAP_KEY'],false)==0)
			{
				$Speichern = true;

				// Steuern berechnen
				$MWST = 0;
				$SQL = 'SELECT 1.0+(MWS_SATZ/100.0) as satz';
				$SQL .= ' FROM Mehrwertsteuersaetze';
				$SQL .= ' WHERE mws_id = 0'.$DB->FeldInhaltFormat('N0',$_POST['txtZAP_MWS_ID'],false);
				$rsMWS = $DB->RecordSetOeffnen($SQL);
				if($rsMWS->EOF())
				{
					$Speichern = false;
				}
				else
				{
					$MWST = $DB->FeldInhaltFormat('N4',$rsMWS->FeldInhalt('SATZ'),false);
				}

				$VKBrutto = $DB->FeldInhaltFormat('N4',$_POST['txtZAP_VKPR'],false) * $MWST;
				$EK = $DB->FeldInhaltFormat('N4',$_POST['txtZAP_EKPR'],false)*(1-($DB->FeldInhaltFormat('N2',$_POST['txtZAP_RABATT1'],false)/100))*(1-($DB->FeldInhaltFormat('N2',$_POST['txtZAP_RABATT2'],false)/100));
//awisFormular::DebugAusgabe(1,$SQL,$MWST,$VKBrutto);
				if($Speichern)
				{
					$Fehler = '';
					$SQL = 'INSERT INTO Zukaufartikellieferantenpreise';
					$SQL .= '(ZAP_ZAL_KEY,ZAP_GUELTIGAB,ZAP_VKPR,ZAP_EKPR,ZAP_RABATT1,ZAP_RABATT2';
					$SQL .= ',ZAP_EK,ZAP_PREISBINDUNGVON,ZAP_PREISBINDUNGBIS,ZAP_LAN_CODE,ZAP_SPERRE';
					$SQL .= ',ZAP_SPERRGRUND,ZAP_IMQ_ID,ZAP_VKBRUTTO,ZAP_MWS_ID,ZAP_EINHEITENPROVK';
					$SQL .= ',ZAP_ALTTEILWERT,ZAP_USER,ZAP_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= ' ' . $DB->FeldInhaltFormat('N0',$_POST['txtZAP_ZAL_KEY'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtZAP_GUELTIGAB'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N4',$_POST['txtZAP_VKPR'],false);
					$SQL .= ',' . $DB->FeldInhaltFormat('N4',$_POST['txtZAP_EKPR'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N2',$_POST['txtZAP_RABATT1'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N2',$_POST['txtZAP_RABATT2'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N4',$EK,false);
					$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtZAP_PREISBINDUNGVON'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtZAP_PREISBINDUNGBIS'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAP_LAN_CODE'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZAP_SPERRE'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZAP_SPERRGRUND'],true);
					$SQL .= ',4';		// manuell
					$SQL .= ',' . $DB->FeldInhaltFormat('N4',$VKBrutto,true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZAP_MWS_ID'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtZAP_EINHEITENPROVK'],true);
					$SQL .= ',' . $DB->FeldInhaltFormat('N4',$_POST['txtZAP_ALTTEILWERT'],true);
					$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					$DB->Ausfuehren($SQL,'',false);
				}
			}
			else 					// ge�nderte Zuordnung
			{
				$FehlerListe = array();
				$UpdateFelder = '';

				$rsPEI = $DB->RecordSetOeffnen('SELECT * FROM Zukaufartikellieferantenpreise WHERE ZAP_key=' . $DB->FeldInhaltFormat('N0',$_POST['txtZAP_KEY'],false) . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
				// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsPEI->FeldInfo($FeldName,'TypKZ'),$rsPEI->FeldInhalt($FeldName),true);
				//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';

								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}

				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsPEI->FeldInhalt('ZAP_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE Zukaufartikellieferantenpreise SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', ZAP_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', ZAP_userdat=sysdate';
					$SQL .= ' WHERE ZAP_key=0' . $_POST['txtZAP_KEY'] . '';
					$DB->Ausfuehren($SQL,'',false);
				}
			}
		}
	}

	
	
	//**************************************************************************************************
	//* Aufl�sen von Dubletten
	//**************************************************************************************************
	
	$ZUKArtikelDubl = $Form->NameInArray($_POST, 'txtDUBL_',2,1);
	
	if(!empty($ZUKArtikelDubl) AND $ZUKArtikelDubl[0]!='')
	{
		$DB->TransaktionBegin();
		
		
		// Schritt 1: Ermitteln, welche Lieferanten den AKTUELLEN Artikel haben
		$SQL = 'SELECT zal_key, zal_lie_nr, zal_bestellnummer';
		$SQL .= ' FROM zukaufartikellieferanten';
		$SQL .= ' WHERE zal_zla_key = :var_N0_zal_zla_key';
		
		$DB->SetzeBindevariable('ZAL', 'var_N0_zal_zla_key', $AWIS_KEY1,awisDatenbank::VAR_TYP_GANZEZAHL);
		$rsZIEL_ZAL = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZAL'));
		$ZALZIEL = array();
		while(!$rsZIEL_ZAL->EOF())
		{
			$ZALZIEL[$rsZIEL_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY']=$rsZIEL_ZAL->FeldInhalt('ZAL_KEY');
			$ZALZIEL[$rsZIEL_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_BESTELLNUMMER']=$rsZIEL_ZAL->FeldInhalt('ZAL_BESTELLNUMMER');
			
			$rsZIEL_ZAL->DSWeiter();
		}
		
var_dump($ZALZIEL);		
echo '<hr>';		
		// Jetzt die anderen Artikel untersuchen

		$ZALQUELLE = array();
		foreach($ZUKArtikelDubl AS $ZUKArtikel)
		{
			$Abbruch = 0;

			$DoppleteZAL = array();
			$FeldTeile = explode('_', $ZUKArtikel);
			$ZLA_KEY = $FeldTeile[1];
			
			echo 'Dublette '.$ZLA_KEY.' wird geladen...<br>';
			
			$SQL = 'SELECT zal_key, zal_lie_nr, ZAL_BESTELLNUMMER';
			$SQL .= ' FROM zukaufartikellieferanten';
			$SQL .= ' WHERE zal_zla_key = :var_N0_zal_zla_key';
			
			$DB->SetzeBindevariable('ZAL', 'var_N0_zal_zla_key', $ZLA_KEY,awisDatenbank::VAR_TYP_GANZEZAHL);
			$rsQUELLE_ZAL = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ZAL'));
			
			while(!$rsQUELLE_ZAL->EOF())
			{
				$ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'] = $rsQUELLE_ZAL->FeldInhalt('ZAL_KEY');
				$ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_BESTELLNUMMER'] = $rsQUELLE_ZAL->FeldInhalt('ZAL_BESTELLNUMMER');

				// Pr�fen, ob es einen Lieferantenartikel doppelt gibt
				// DAZU: Erst mal pr�fen, ob ein Lieferant+ZAL doppelt ist
				if(isset($ZALZIEL[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY']))
				{
					// Nun pr�fen, ob die Bestellnummer auch gleich ist
					if($ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_BESTELLNUMMER'] == $ZALZIEL[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_BESTELLNUMMER'])
					{
						echo ' -- Bestellnummer ist identisch fuer '.$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR').'<br>';
						// Wenn beide gleich sind, m�ssen die Preise umgeh�ngt werden
						// und dann alle Referenzen getauscht werden
						$SQL = 'UPDATE ZUKAUFARTIKELLIEFERANTENPREISE';
						$SQL .= ' SET ZAP_ZAL_KEY = :var_N0_ZIEL';
						$SQL .= ' WHERE ZAP_ZAL_KEY = :var_N0_QUELLE';
						$DB->SetzeBindevariable('ZAP', 'var_N0_ZIEL', $ZALZIEL[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
						$DB->SetzeBindevariable('ZAP', 'var_N0_QUELLE', $ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'], awisDatenbank::VAR_TYP_GANZEZAHL);
						$DB->Ausfuehren($SQL, '', '', $DB->Bindevariablen('ZAP'));

						// Merken, dass dieser nicht umgeh�ngt werden muss
						$DoppleteZAL[] = $ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'];
						
						echo ' -- Alle Preissaetze werden umgehaegt...<br>';

						echo ' -- Rechnungen anpassen...<br>';

						$SQL = 'UPDATE Zukaufrechnungen';
						$SQL .= ' SET ZUR_ZAL_KEY = '.$ZALZIEL[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'];
						$SQL .= ' WHERE ZUR_ZAL_KEY = '.$ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'];
						$DB->Ausfuehren($SQL);
						
						echo ' -- Rechnungen umgeh&auml;ngt...<br>';
						
						echo ' -- Lieferscheine anpassen...<br>';
						$SQL = 'UPDATE Zukauflieferscheine';
						$SQL .= ' SET ZUL_ZAL_KEY = '.$ZALZIEL[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'];
						$SQL .= ' WHERE ZUL_ZAL_KEY = '.$ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'];
						$DB->Ausfuehren($SQL);
						echo ' -- Lieferschein umgeh&auml;ngt...<br>';
						
						echo ' -- Bestellungen anpassen...<br>';
						$SQL = 'UPDATE Zukaufbestellungen';
						$SQL .= ' SET ZUB_ZAL_KEY = '.$ZALZIEL[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'];
						$SQL .= ' WHERE ZUB_ZAL_KEY = '.$ZALQUELLE[$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR')]['ZAL_KEY'];
						$DB->Ausfuehren($SQL);
						echo ' -- Bestellungen umgeh&auml;ngt...<br>';
					}
					else
					{
						echo ' -- Lieferant '.$rsQUELLE_ZAL->FeldInhalt('ZAL_LIE_NR').' doppelt<br>';
						$Abbruch = 1;		// Weil Bestellnummer unterschiedlich bei gleichem Artikel
					}
				}
				$rsQUELLE_ZAL->DSWeiter();
			}
			
	var_dump($ZALQUELLE);		

			$Form->Formular_Start();
			
			if($Abbruch==0)
			{
				// Umsetzen
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('Beginne Umsetzung...',200,'Hinweis');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('   Bestellungen.',200,'');
				$Form->ZeileEnde();
				
				$SQL = 'UPDATE Zukaufbestellungen ';
				$SQL .= ' SET ZUB_ZLA_KEY = '.$AWIS_KEY1;
				$SQL .= ' WHERE ZUB_ZLA_KEY = '.$ZLA_KEY;
				$DB->Ausfuehren($SQL);

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('   Zukauflieferantenartikelarchiv.',200,'');
				$Form->ZeileEnde();
				
				$SQL = 'UPDATE ZUKAUFLIEFERANTENARTIKELARCHIV';
				$SQL .= ' SET ZAH_ZLA_KEY = '.$AWIS_KEY1;
				$SQL .= ' WHERE ZAH_ZLA_KEY = '.$ZLA_KEY;
				$DB->Ausfuehren($SQL);
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('   Zukauflieferantenbestellungen.',200,'');
				$Form->ZeileEnde();
				
				$SQL = 'UPDATE Zukauflieferantenbestellungen';
				$SQL .= ' SET ZLB_ZLA_KEY = '.$AWIS_KEY1;
				$SQL .= ' WHERE ZLB_ZLA_KEY = '.$ZLA_KEY;
				$DB->Ausfuehren($SQL);
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('   Zukauflieferantenartikelids.',200,'');
				$Form->ZeileEnde();
				
				$SQL = 'UPDATE ZUKAUFLIEFERANTENARTIKELIDS A';
				$SQL .= ' SET A.ZAI_ZLA_KEY = '.$AWIS_KEY1;
				$SQL .= ' WHERE A.ZAI_ZLA_KEY = '.$ZLA_KEY;
				$SQL .= ' AND NOT EXISTS(SELECT * FROM ZUKAUFLIEFERANTENARTIKELIDS B WHERE B.ZAI_ZLA_KEY = '.$AWIS_KEY1.' AND B.ZAI_AID_Nr = A.ZAI_AID_Nr)';
				$DB->Ausfuehren($SQL);
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel('   Zukaufpreisauskunft.',200,'');
				$Form->ZeileEnde();
				
				$SQL = 'UPDATE Zukaufpreisauskunft';
				$SQL .= ' SET ZPA_ZLA_KEY = '.$AWIS_KEY1;
				$SQL .= ' WHERE ZPA_ZLA_KEY = '.$ZLA_KEY;
				$DB->Ausfuehren($SQL);

				
				// Die zu l�schenden Artikel suchen
				foreach($ZALQUELLE AS $Lieferant=>$ZAL_KEYs)
				{
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel('   Zukaufartikellieferanten.',200,'');
					$Form->ZeileEnde();
					
					if(array_search($ZAL_KEYs['ZAL_KEY'], $DoppleteZAL)!==false)
					{
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel('     -- '.$ZAL_KEYs['ZAL_KEY'].' nicht umhaengen. gibt es schon als '.$ZALZIEL[$Lieferant]['ZAL_KEY'],200,'');
						$Form->ZeileEnde();
					}
					else
					{
						// Artikel des Lieferanten unter einen neuen Stammartikel
						$SQL = 'UPDATE Zukaufartikellieferanten';
						$SQL .= ' SET ZAL_ZLA_KEY = '.$AWIS_KEY1;
						$SQL .= ' WHERE ZAL_KEY = '.$ZAL_KEYs['ZAL_KEY'];
						$DB->Ausfuehren($SQL);
					}
				}
				
				
				
				
				$SQL = 'DELETE FROM Zukaufartikel';
				$SQL .= ' WHERE ZLA_KEY = '.$ZLA_KEY;
				$DB->Ausfuehren($SQL);
				
				//$DB->TransaktionRollback();
				$DB->TransaktionCommit();
			}
			else 
			{
				switch($Abbruch)
				{
					case 1:
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel('Lieferant doppelt. Kann nicht automatisch umge&auml;ngt werden...',200,'Hinweis');
						$Form->ZeileEnde();
						break;
				}
				
				
				$DB->TransaktionRollback();
				
			}
	
			$Form->Formular_Ende();
		}
	}		
}
catch (awisException $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>