<?php 
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try 
{
	$TextKonserven = array();
	$TextKonserven[] = array('FGK', '*');
	$TextKonserven[] = array('FKH', '*');
	$TextKonserven[] = array('FKA', '*');
	$TextKonserven[] = array('Wort', 'Seite');
	$TextKonserven[] = array('Wort', 'lbl_suche');
	$TextKonserven[] = array('Wort', 'lbl_weiter');
	$TextKonserven[] = array('Wort', 'lbl_speichern');
	$TextKonserven[] = array('Wort', 'lbl_trefferliste');
	$TextKonserven[] = array('Wort', 'lbl_aendern');
	$TextKonserven[] = array('Wort', 'lbl_hilfe');
	$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
	$TextKonserven[] = array('Wort', 'lbl_loeschen');
	$TextKonserven[] = array('Wort', 'lbl_zurueck');
	$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
	$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
	$TextKonserven[] = array('Wort', 'lbl_drucken');
	$TextKonserven[] = array('Wort', 'lbl_Hilfe');
	$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
	$TextKonserven[] = array('Liste', 'lst_JaNein');
	$TextKonserven[] = array('Wort', 'lbl_AEMNachdruck');
	$TextKonserven[] = array('Wort', 'Umbuchung');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht9100 = $AWISBenutzer->HatDasRecht(9100);
	
	if ($Recht9100 == 0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}
	
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
	$SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
	$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
	
	
	
}
catch (Exception $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}


?>