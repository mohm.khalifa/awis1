<?php
/**
 * Formular: 05 - Austritt Mitarbeiter
 * @author Sacha Kerres
 *
 */


$TextKonserven = array();
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'TTT_*');
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'txt_*');
$TextFormular = $Form->LadeTexte($TextKonserven);
$ToolTipps = $Form->LadeTexte($TextKonserven, $AWISBenutzer->BenutzerSprache());

//**************************************************************
// Anpassung des Labels für die Kostenstelle
// und Berücksichtigung der Eingabeformate
//**************************************************************
echo '<script type="text/javascript">'.PHP_EOL;

echo '$("#txtOTF_WERT_133").change(function(){'.PHP_EOL;
echo 'if($("#txtOTF_WERT_133").val()=="FILIALE")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $("#lblOTF_27").html("Filiale (4-stellig)<font color=#FF0000>*</font>:");'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $("#lblOTF_27").html("Kostenstelle<font color=#FF0000>*</font>:");'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

// Bei Kostenstellen 4-12 Zeichen
// bei Filiale auf 4 Stellen erweitern / schneiden
echo '$("#txtOTF_WERT_27").change(function(){'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    if($("#txtOTF_WERT_133").val()=="FILIALE")';
echo '    {'.PHP_EOL;
echo '            $("#txtOTF_WERT_27").val(String("0000"+$("#txtOTF_WERT_27").val()).slice(-4));';
echo '    }'.PHP_EOL;
echo '    else'.PHP_EOL;
echo '    {'.PHP_EOL;
echo '        if($("#txtOTF_WERT_27").val().length<4)'.PHP_EOL;
echo '        {'.PHP_EOL;
echo '            alert("Bitte mindestens 4 Zeichen angeben");'.PHP_EOL;
echo '            $("#txtOTF_WERT_27").focus();';
echo '        }'.PHP_EOL;
echo '        else if($("#txtOTF_WERT_27").val().length>12)'.PHP_EOL;
echo '        {'.PHP_EOL;
echo '            alert("Bitte maximal 12 Zeichen angeben");'.PHP_EOL;
echo '            $("#txtOTF_WERT_27").focus();';
echo '        }'.PHP_EOL;
echo '    }'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;

echo '</script>'.PHP_EOL;

//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

$SQL = ' select ADE_MAIL, ADE_SURNAME || COALESCE(\', \'||ADE_GIVENNAME,\'\') AS KON_NAME from ACTIVEDIRECTORYEXPORT ';

$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Benachrichtigungan'], 299,'','','');
$Form->Erstelle_MehrfachSelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn',isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn']:array(),'450:450',true,$SQL,'','','','','');
$Form->ZeileEnde();

//**************************************************
// Account Sperren ab
//**************************************************
$Block='100';
$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Accountsperrenab'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_SPERREAB']);
$Form->Erstelle_SelectFeld('!AccountSperrenAb',(isset($_POST['txtAccountSperrenAb'])?$_POST['txtAccountSperrenAb']:''),'200:190',true,'','',0,'','',array('~::bitte w&auml;hlen::','1~sofort','2~Austrittsdatum','3~Sperrdatum'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Accountsperreab'], 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_SPERREABDATUM']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-AccountSperrenAb', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-AccountSperrenAb'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-AccountSperrenAb']:''), 10, 140, true,'','','','D');
$Form->ZeileEnde();


echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtAccountSperrenAb").change(function(){'.PHP_EOL;
echo 'if($("#txtAccountSperrenAb").val()==3)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-AccountSperrenAb").attr("required", true)'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-AccountSperrenAb").attr("required", false)'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtAccountSperrenAb").change()});';

echo '</script>'.PHP_EOL;



//**************************************************
// EMails
//**************************************************
$Block='70';
$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Mailuebertragen'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MAILUEBERTRAG']);
$Form->Erstelle_SelectFeld('EMails',(isset($_POST['txtEMails'])?$_POST['txtEMails']:''),'190:180',true,'','',0,'','',array('1~Ja','0~Nein, l&ouml;schen'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Uebertragenan'], 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MAILUEBERTRAGAN']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn']:''), 30, 300, true);
$Form->ZeileEnde();
$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Uebertragenab'], 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MAILUEBERTRAGAB']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb']:''), 10, 150, true, '', '', '', 'D');
$Form->ZeileEnde();


echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$("Leitfadenhinweistext").hide();';
echo '});';

echo '$("#txtEMails").change(function(){'.PHP_EOL;
echo 'if($("#txtEMails").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $(".Leitfadenhinweistext").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb").attr("required", true)'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    if ($("#txtEMails").val()==1 || $("#txtEMailWeiter").val()==1)';
echo '    {'.PHP_EOL;
echo '      $(".Leitfadenhinweistext").show();'.PHP_EOL;
echo '    }else';
echo '    {'.PHP_EOL;
echo '      $(".Leitfadenhinweistext").hide();'.PHP_EOL;
echo'     }'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb").attr("required", false)'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtEMails").change()});';

echo '</script>'.PHP_EOL;




//**************************************************
// EMails weiterleiten
//**************************************************
$Block='80';
$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Mailweiterleiten'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MAILWEITER']);
$Form->Erstelle_SelectFeld('EMailWeiter',(isset($_POST['txtEMailWeiter'])?$_POST['txtEMailWeiter']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('display:none;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Weiterleitenan'], 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MAILWEITERAN']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn']:''), 30, 300, true);
$Form->ZeileEnde();
$Form->ZeileStart('display:none;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Weiterleitenab'], 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MAILWEITERAB']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb']:''), 10, 150, true, '', '', '', 'D');
$Form->ZeileEnde();
$Form->ZeileStart('display:none','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Dauertage'], 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_MAILWEITERTAGE']);
$Tage=array();
for($i=1;$i<=30;$i++)
{
    $Tage[]=$i.'~'.$i;
}
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenDauer',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenDauer'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenDauer']:''),'80:70',true,'','','30','','',$Tage);
$Form->ZeileEnde();


echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$("Leitfadenhinweistext").hide();';
echo '});';

echo '$("#txtEMailWeiter").change(function(){'.PHP_EOL;
echo 'if($("#txtEMailWeiter").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb").attr("required", true)'.PHP_EOL;
echo '    $(".Leitfadenhinweistext").show();'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    if ($("#txtEMails").val()==1 || $("#txtEMailWeiter").val()==1)';
echo '    {'.PHP_EOL;
echo '      $(".Leitfadenhinweistext").show();'.PHP_EOL;
echo '    }else';
echo '    {'.PHP_EOL;
echo '      $(".Leitfadenhinweistext").hide();'.PHP_EOL;
echo'     }'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb").attr("required", false)'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtEMailWeiter").change()});';

echo '</script>'.PHP_EOL;


$Form->ZeileStart('display:block;','','Leitfadenhinweistext');
//$Form->Erstelle_TextLabel('&nbsp;', 290,'','','');
$Form->Hinweistext($TextFormular['OTF_FRM_'.$FormularKennung]['txt_LEITFADENPOSTFACH'],awisFormular::HINWEISTEXT_BENACHRICHTIGUNG,'width: 800; margin-left: 0px');
$Form->ZeileEnde();

//**************************************************
// Google Drive Daten übertragen
//**************************************************
$Block='90';
$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_GoogleDriveuebertragen'], 300,'','','');
$Form->Erstelle_SelectFeld('GoogleDriveuebertragen',(isset($_POST['txtGoogleDriveuebertragen'])?$_POST['txtGoogleDriveuebertragen']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('display:none;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Weiterleitenan'], 250,'','','');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn']:''), 30, 300, true);
$Form->ZeileEnde();
$Form->ZeileStart('display:none;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_Weiterleitenab'], 250,'','','');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb']:''), 10, 150, true, '', '', '', 'D');
$Form->ZeileEnde();

echo '<script type="text/javascript">'.PHP_EOL;
echo '$("#txtGoogleDriveuebertragen").change(function(){'.PHP_EOL;
echo 'if($("#txtGoogleDriveuebertragen").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb").attr("required", true)'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAn").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-UebertragenAb").attr("required", false)'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;
echo '$( document ).ready(function() {$("#txtGoogleDriveuebertragen").change()});';
echo '</script>'.PHP_EOL;


//*********************************************************************
//* HW vorhanden?
//*********************************************************************

$Block='800';
$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_HwVorhanden'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_HWVORHANDEN']);
$Form->Erstelle_SelectFeld('!HWVorhanden',(isset($_POST['txtHWVorhanden'])?$_POST['txtHWVorhanden']:''),'160:150',true,'','',0,'','',array('~::bitte w&auml;hlen::','1~Ja','2~Nein'));
$Form->ZeileEnde();


$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Hinweistext($TextFormular['OTF_FRM_'.$FormularKennung]['txt_HWVORHANDEN'],awisFormular::HINWEISTEXT_BENACHRICHTIGUNG,'width: 800; margin-left: 0px');
$Form->ZeileEnde();


echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtHWVorhanden").change(function(){'.PHP_EOL;
echo 'if($("#txtHWVorhanden").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtHWVorhanden").change()});';

echo '</script>'.PHP_EOL;


//**************************************************
// Bemerkungen
//**************************************************
$Block='900';

$Form->ZeileStart();
$Form->Erstelle_TextLabel($TextFormular['OTF_FRM_'.$FormularKennung]['txt_AllgBemerkungen'], 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_BEMERKUNGEN_ALLG']);
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']:''), 600, 60, 3, true);
$Form->ZeileEnde();
?>