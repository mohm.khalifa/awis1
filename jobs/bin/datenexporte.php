<?php
require_once('/daten/include/awisDatenexporte.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $Anzahl = 1;
    $ExporteAufraeumen = false;

    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--anzahl':
                            $Anzahl = $argv[++$i];
                            break;
                    case '--exporteaufraeumen';
                            $ExporteAufraeumen = $argv[++$i];
                            break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --anzahl                           Anzahl gleichzeitige Exporte'.PHP_EOL;

                            echo PHP_EOL;
                            die();
            }
    }

    $AWISBenutzer = awisBenutzer::Init($Benutzer);
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $ExportObj  = new awisDatenexporte($DB, $AWISBenutzer);
    $ExportObj->DebugLevel($DebugLevel);
    $ExportObj->StarteExporte($Anzahl);

    if($ExporteAufraeumen){
        $ExportObj->LoescheAlteDateien();
    }

}
catch (awisException $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'SQL: '.$ex->getSQL().PHP_EOL;
}
catch (Exception $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
}
?>
