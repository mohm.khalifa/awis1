<?php

/**
 * ***********************************************************************
 * Mit diesen Script hat man die Moeglichkeit einige kleine Anforderungen
 * des Tagesgeschaefts zu machen. Die Teile die man nicht braucht werden
 * auskommentiert. Ein zusammen geh�render Block wird immer durch eine
 * Zeile aus Sternchen eingegrenzt.
 * ***********************************************************************
 * Erstellt:
 * @author 17.11.2011 Stefan Oppl
 * @version 1.0
 * �nderungen:
 * 12.12.2011 OP : Block2 -> Auslaufkennung bei LiefArtNr setzen erstellt
 * 13.01.2012 OP : Block3 -> @ od. � ArtNr. in ATU-Artikel umsetzen erstellt
 * 21.02.2012 OP : Block3 -> Protokollierung eingef�hrt.
 * ***********************************************************************
 */

require_once 'awisVerschiedenes.php';
require_once 'fileCSV.php';
require_once 'awisDatenbank.inc';

$objVerschiedenes = new awisVerschiedenes();

/************************************************************************/
/*BLOCK1: */
/*Bei Lieferantenartikel eine alte Lieferantennummer durch
 * eine neue Lieferantennummer ersetzen*/
/*
try{
	$liefNrAlt = '9990'; //Lieferantennummer die ersetzt werden soll
	$liefNrNeu = '0040'; //neue Lieferantennummer die gesetzt werden soll
	$objVerschiedenes->ersetzeLiefNrInLiefArtNr($liefNrAlt, $liefNrNeu);	
}
catch (awisException $e){
	echo $e->getMessage().'/'.$e->getSQL();
}
catch (Exception $e){
	echo $e->getMessage().'/'.$e->getCode();
}
*/
/************************************************************************/
/************************************************************************/
/*BLOCK2: */
/*
 * Setzt fuer vordefinierte Artikel die Auslaufkennung + eine Bemerkung.
 * Die Artikel die gekennzeichnet werden sollen m�ssen in der Tabelle
 * xxx_auslauf_tmp hinterlegt werden.
 */
/*
try
{
	$db = awisDatenbank::NeueVerbindung('AWIS');
	$db->Oeffnen();
	$fileCSV = new fileCSV();
	$fileCSV->setDateiPfad('/daten/daten/auslauf/');
	$fileCSV->setDateiName('LiefArtNrAuslauf_'.strftime('%y%m%d%H%M%S').'.csv');
	$fileCSV->setCSVSeperator(';');
	$fileCSV->oeffneDatei('a');
	
	// nur DS mit Status = 0 --> sind noch nicht verarbeitet
	$SQL = 'SELECT * ';
	$SQL .= 'FROM xxx_auslauf_tmp ';
	$SQL .= 'WHERE status = 0';
	
	if($db->ErmittleZeilenAnzahl($SQL) == 0)
	{
		throw new Exception('xxx_auslauf_Tabelle_ist_leer', '201112121333');
	}
	
	$rsAuslaufArt = $db->RecordSetOeffnen($SQL);
	
	while(!$rsAuslaufArt->EOF())
	{
		// Auslaufkennzeichen + Bemerkung setzen
		$result = $objVerschiedenes->setAuslaufLiefArtNr($rsAuslaufArt->FeldInhalt('LIEFNR'), $rsAuslaufArt->FeldInhalt('LIEFARTNR'),$rsAuslaufArt->FeldInhalt('BEMERKUNG'));

		if(!$result)
		{
			$fehlerText = array();
			$fehlerText[0] = 'LiefNr: '.$rsAuslaufArt->FeldInhalt('LIEFNR');
			$fehlerText[1] = 'LiefArtNr: '.$rsAuslaufArt->FeldInhalt('LIEFARTNR');
			$fehlerText[2] = 'Kombination LiefNr + LiefArtNr existiert nicht';
			$fileCSV->schreibeCSVDatensatz($fehlerText);

			// Datensatz auf Status "Fehler = 2" setzen
			$SQL = 'UPDATE xxx_auslauf_tmp SET status = 2 ';
			$SQL .= "WHERE liefnr = '".$rsAuslaufArt->FeldInhalt('LIEFNR')."' ";
			$SQL .= "AND liefartnr = '".$rsAuslaufArt->FeldInhalt('LIEFARTNR')."'";
			$db->Ausfuehren($SQL);
		}
		else
		{
			// Datensatz auf Status "OK = 1" setzen
			$SQL = 'UPDATE xxx_auslauf_tmp SET status = 1';
			$SQL .= "WHERE liefnr = '".$rsAuslaufArt->FeldInhalt('LIEFNR')."' ";
			$SQL .= "AND liefartnr = '".$rsAuslaufArt->FeldInhalt('LIEFARTNR')."'";
			$db->Ausfuehren($SQL);
		}
		
		$rsAuslaufArt->DSWeiter();
	}
	
	$fileCSV->schliesseDatei();
	
}
catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
}
*/
/************************************************************************/
/************************************************************************/
/*Block3: */
/*
 * Wandelt einen Crossing-Artikel (Artikel mit � oder @) in einen
 * ATU-Artikel um. Au�erdem werden einige Felder mit vorbelegt. 
 * Daten m�ssen in Tabelle xxx_A00003 hinterlegt werden.
 */

try
{
	$db = awisDatenbank::NeueVerbindung('AWIS');
	$db->Oeffnen();

	//Alle Selektieren die neu in Tabelle importiert wurden (null)
	//oder die bei dem letzten Lauf nicht erfolgreich waren (0)
	$SQL = 'SELECT * ';
	$SQL .= 'FROM xxx_A00003 ';
	$SQL .= 'WHERE erfolgreich is null ';
	$SQL .= 'or erfolgreich = 0';
		
	$rsQuellDat = $db->RecordSetOeffnen($SQL);
	
	while(!$rsQuellDat->EOF())
	{
		$SQL = 'SELECT ast_key, ast_atunr ';
		$SQL .= 'FROM artikelstamm ';
		$SQL .= "WHERE ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "'";

		$aktion = 'Suche Pseudo-ArtNr.: ' . $rsQuellDat->FeldInhalt('ATU_ALT');
		$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
		$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
		$db->Ausfuehren($sqlLog,'',true);
		//echo $SQL.'<br>';
		
		if($db->ErmittleZeilenAnzahl($SQL) > 0)
		{
			$rsArtikelstamm = $db->RecordSetOeffnen($SQL);

			$db->TransaktionBegin();
			
			// Felder Ast_Bezeichnung, Ast_Kennunvorschlag und ast_atunr
			// in Artikelstammtabelle updaten
			$SQL = 'UPDATE artikelstamm ';
			$SQL .= "SET ast_bezeichnung = '" . $rsQuellDat->FeldInhalt('BEZ_KATALOG') . "', ";
			$SQL .= "ast_kennungvorschlag = '" . $rsQuellDat->FeldInhalt('KENN_VORSCHLAG') . "', ";
			$SQL .= "ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
			$SQL .= "WHERE ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "'";

			$db->Ausfuehren($SQL,'',true);
			
			$aktion = 'Felder AST_BEZEICHNUNG, AST_KENNUNGVORSCHLAG, AST_ATUNR in ARTIKELSTAMM geaendert';
			$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
			$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
			$db->Ausfuehren($sqlLog,'',true);
			//echo $SQL.'<br>';
			
			// Feld Einkauf_am AIT_ID = 100 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr ';
			$SQL .= 'FROM artikelstamminfos ';
			$SQL .= "WHERE asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "' ";
			$SQL .= 'AND asi_ait_id = 100 ';
			$SQL .= "OR asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
			$SQL .= 'AND asi_ait_id = 100 ';
			
			if($db->ErmittleZeilenAnzahl($SQL) == 0)
			{
				$SQL = 'INSERT INTO artikelstamminfos ';
				$SQL .= '(asi_ast_atunr,asi_ait_id,asi_wert,asi_readonly, ';
				$SQL .= 'asi_imq_id,asi_user,asi_userdat) ';
				$SQL .= "values ('" . $rsQuellDat->FeldInhalt('ATU_NR') . "',100, ";
				$SQL .= "'". $rsQuellDat->FeldInhalt('EINKAUF_AM') ."', ";
				$SQL .= "1,4,'awis',sysdate)";
				
				$db->Ausfuehren($SQL,'',true);
				
				$aktion = 'Arstikelstamminfo ASI_AIT_ID: 100 eingefuegt';
				$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
				$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
				$db->Ausfuehren($sqlLog,'',true);
				//echo $SQL.'<br>';					
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos ';
				$SQL .= "SET asi_wert = '" . $rsQuellDat->FeldInhalt('EINKAUF_AM') . "' ";
				$SQL .= "WHERE asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
				$SQL .= 'AND asi_ait_id = 100';
				
				$db->Ausfuehren($SQL,'',true);
				
				$aktion = 'Arstikelstamminfo ASI_AIT_ID: 100 geaendert';
				$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
				$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
				$db->Ausfuehren($sqlLog,'',true);
				
				//echo $SQL.'<br>';
			}
			
			// Feld Verpackungseinheit AIT_ID = 60 einf�gen oder updaten
			
			$SQL = 'SELECT asi_ast_atunr ';
			$SQL .= 'FROM artikelstamminfos ';
			$SQL .= "WHERE asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "' ";
			$SQL .= 'AND asi_ait_id = 60 ';
			$SQL .= "OR asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
			$SQL .= 'AND asi_ait_id = 60';
			
			if($db->ErmittleZeilenAnzahl($SQL) == 0)
			{
				$SQL = 'INSERT INTO artikelstamminfos ';
				$SQL .= '(asi_ast_atunr,asi_ait_id,asi_wert,asi_readonly, ';
				$SQL .= 'asi_imq_id,asi_user,asi_userdat) ';
				$SQL .= "values ('" . $rsQuellDat->FeldInhalt('ATU_NR') . "',60, ";
				$SQL .= "'". $rsQuellDat->FeldInhalt('VPE') ."', ";
				$SQL .= "1,4,'awis',sysdate)";
				
				$db->Ausfuehren($SQL,'',true);
				
				$aktion = 'Arstikelstamminfo ASI_AIT_ID: 60 eingefuegt';
				$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
				$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
				$db->Ausfuehren($sqlLog,'',true);
				
				//echo $SQL.'<br>';					
			}
			else
			{
				$SQL = 'UPDATE artikelstamminfos ';
				$SQL .= "SET asi_wert = '" . $rsQuellDat->FeldInhalt('VPE') . "' ";
				$SQL .= "WHERE asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
				$SQL .= 'AND asi_ait_id = 60';
				
				$db->Ausfuehren($SQL,'',true);
				
				$aktion = 'Arstikelstamminfo ASI_AIT_ID: 60 eingefuegt';
				$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
				$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
				$db->Ausfuehren($sqlLog,'',true);
				
				//echo $SQL.'<br>';
			}
			
			// Feld Verpackungseinheit AIT_ID = 101 einf�gen oder updaten
			
			//Aus Feld MELDUNG_DURCH den Nachnamen & Vornamen herausschneiden
			//echo $rsQuellDat->FeldInhalt('MELDUNG_DURCH').'<br>';
			$nachname = trim(substr($rsQuellDat->FeldInhalt('MELDUNG_DURCH'), 0,strpos($rsQuellDat->FeldInhalt('MELDUNG_DURCH'), ',')));
			$vorname = trim(substr($rsQuellDat->FeldInhalt('MELDUNG_DURCH'), strpos($rsQuellDat->FeldInhalt('MELDUNG_DURCH'), ',')+1));
			
			$SQL = 'SELECT app_id ';
			$SQL .= 'FROM artikelpruefpersonal ';
			$SQL .= 'WHERE app_aktivbis > sysdate ';
			$SQL .= "AND app_name like '%". $nachname ."%". $vorname ."%'";
			
			//echo $SQL.'<br>';
			
			if($db->ErmittleZeilenAnzahl($SQL) == 1)
			{
				$rsArtPruefPers = $db->RecordSetOeffnen($SQL);
				
				$SQL = 'SELECT asi_ast_atunr ';
				$SQL .= 'FROM artikelstamminfos ';
				$SQL .= "WHERE asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "' ";
				$SQL .= 'AND asi_ait_id = 101 ';
				$SQL .= "OR asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
				$SQL .= 'AND asi_ait_id = 101';
				
				if($db->ErmittleZeilenAnzahl($SQL) == 0)
				{
					$SQL = 'INSERT INTO artikelstamminfos ';
					$SQL .= '(asi_ast_atunr,asi_ait_id,asi_wert,asi_readonly, ';
					$SQL .= 'asi_imq_id,asi_user,asi_userdat) ';
					$SQL .= "values ('" . $rsQuellDat->FeldInhalt('ATU_NR') . "',101, ";
					$SQL .= "'". $rsArtPruefPers->FeldInhalt('APP_ID') ."', ";
					$SQL .= "1,4,'awis',sysdate)";
					
					$db->Ausfuehren($SQL,'',true);

					$aktion = 'Arstikelstamminfo ASI_AIT_ID: 101 eingefuegt';
					$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
					$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
					$db->Ausfuehren($sqlLog,'',true);
					
					//echo $SQL.'<br>';
				}
				else
				{
					$SQL = 'UPDATE artikelstamminfos ';
					$SQL .= "SET asi_wert = '" . $rsArtPruefPers->FeldInhalt('APP_ID') . "' ";
					$SQL .= "WHERE asi_ast_atunr = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
					$SQL .= 'AND asi_ait_id = 101';
					
					$db->Ausfuehren($SQL,'',true);
					
					$aktion = 'Arstikelstamminfo ASI_AIT_ID: 101 geaendert';
					$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
					$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
					$db->Ausfuehren($sqlLog,'',true);
				
					//echo $SQL.'<br>';
				}
				
				$SQL = 'SELECT tei_key ';
				$SQL .= 'FROM teileinfos ';
				$SQL .= "WHERE tei_ity_id1 = 'AST' ";
				$SQL .= "AND tei_key1 = '" . $rsArtikelstamm->FeldInhalt('AST_KEY') . "'";
				
				if($db->ErmittleZeilenAnzahl($SQL) > 0)
				{
					$SQL = 'UPDATE teileinfos ';
					$SQL .= "SET tei_such1 = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "', ";
					$SQL .= "tei_wert1 = '" . $rsQuellDat->FeldInhalt('ATU_NR') . "' ";
					$SQL .= "WHERE tei_ity_id1 = 'AST' ";
					$SQL .= "AND tei_key1 = " . $rsArtikelstamm->FeldInhalt('AST_KEY');
					
					$db->Ausfuehren($SQL,'',true);
					
					$aktion = 'Teileinfo geandert';
					$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
					$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
					$db->Ausfuehren($sqlLog,'',true);
					
					//echo $SQL.'<br>';
				}
				
				//Datensatz als erfolgreich markieren
				$SQL = 'UPDATE xxx_A00003 ';
				$SQL .= 'SET erfolgreich = 1 ';
				$SQL .= "WHERE atu_alt = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "'";
				$db->Ausfuehren($SQL,'',true);
				
				$aktion = 'Datensatz als erfolgreich markiert';
				$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
				$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
				$db->Ausfuehren($sqlLog,'',true);
				
				$db->TransaktionCommit();
			}
			else
			{
				// Name in Artikelpr�fpersonal nicht gefunden
				// oder mehrere mit diesem Namen gefunden.
				// Datensatz als nicht erfolgreich markieren
				$SQL = 'UPDATE xxx_A00003 ';
				$SQL .= 'SET erfolgreich = 0 ';
				$SQL .= "WHERE atu_alt = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "'";
				
				// Da ein Teil der Updates/Inserts schon gelaufen sind, wird hier ein
				// Rollback durchgef�hrt
				$db->TransaktionRollback();
				$db->Ausfuehren($SQL,'',true);
				
				$aktion = 'Datensatz als nicht erfolgreich markiert';
				$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
				$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
				$db->Ausfuehren($sqlLog,'',true);
				
				//echo $SQL.'<br>'; 
			}
			
		}
		else 
		{
			// (� od. @)-Artikelnummer nicht gefunden
			// Datensatz als nicht erfolgreich markieren
			$SQL = 'UPDATE xxx_A00003 ';
			$SQL .= 'SET erfolgreich = 0 ';
			$SQL .= "WHERE atu_alt = '" . $rsQuellDat->FeldInhalt('ATU_ALT') . "'";
			
			$db->Ausfuehren($SQL,'',true);
			
			$aktion = 'Datensatz als nicht erfolgreich markiert (� od. @ - Art. nicht gefunden)';
			$sqlLog = 'INSERT INTO xxx_A00003_Protokoll (ATU_ALT,AKTION,SQL) values ';
			$sqlLog .= "('" . $rsQuellDat->FeldInhalt('ATU_ALT') . "','" . $aktion . "','" . str_replace("'","''",$SQL) . "')";
			$db->Ausfuehren($sqlLog,'',true);
			
			//echo $SQL.'<br>';
		}
		
		$rsQuellDat->DSWeiter();
	}
}
catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
	//Wenn ein SQL fehlschl�gt die Transaktion zur�ckrollen
	$db->TransaktionRollback();
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
	$db->TransaktionRollback();
}	

/************************************************************************/
/************************************************************************/
/*Block4: */
/*
 * Auswertung: Zu eine bestimmten Warengruppe + Sortiment folgende Felder
 * selektieren:
 * ATUNR;Warengruppe;Sortiment;Artikelbezeichnung;Hersteller;Modell;
 * BJVON;BJBIS;Baugruppe;Zulassungen 
 */
/*
try
{
	$warenGruppe = '03'; // welche Warengruppe soll selektiert werden
	$sortiment = '100'; // welches Sortiment soll selektiert werden
	
	//$fd = fopen('/daten/daten', 'a');
	
	$db = awisDatenbank::NeueVerbindung('AWIS');
	$db->Oeffnen();

	// Ueberschriftszeile schreiben
	echo 'ATUNR;WG;SORT;ARTBEZ;HERSTELLER;MODELL;BJVON;BJBIS;BAUGRUPPE;ZULASSUNGEN';
	
	$SQL = 'SELECT ast_atunr,wug_wgr_id,wug_id,ast_bezeichnung ';
	$SQL .= 'FROM artikelstamm ast INNER JOIN warenuntergruppen wug ';
	$SQL .= 'ON ast.ast_wug_key = wug.wug_key ';
	$SQL .= "WHERE wug.wug_wgr_id = '" . $warenGruppe . "' ";
	$SQL .= "AND wug.wug_id = '" . $sortiment . "'";
	
	$rsArtikel = $db->RecordSetOeffnen($SQL);
	
	while(!$rsArtikel->EOF())
	{
		$rsKFZ = $objVerschiedenes->getKFZDaten($rsArtikel->FeldInhalt('AST_ATUNR'));
		
		while(!$rsKFZ->EOF())
		{
			$zeile = $rsArtikel->FeldInhalt('AST_ATUNR').';';
			$zeile .= $rsArtikel->FeldInhalt('WUG_WGR_ID').';';
			$zeile .= $rsArtikel->FeldInhalt('WUG_ID').';';
			$zeile .= $rsArtikel->FeldInhalt('AST_BEZEICHNUNG').';';
			$zeile .= $rsKFZ->FeldInhalt('HERSTELLER').';';
			$zeile .= $rsKFZ->FeldInhalt('MODELL').';';
			$zeile .= $rsKFZ->FeldInhalt('BJVON').';';
			$zeile .= $rsKFZ->FeldInhalt('BJBIS').';';
			$zeile .= $rsKFZ->FeldInhalt('BAUGRUPPE').';';
			$zeile .= $rsKFZ->FeldInhalt('ZULASSUNGEN').'<br>';
			
			echo $zeile;
			
			$rsKFZ->DSWeiter();
		}
		
		$rsArtikel->DSWeiter();
	}
	
}
catch (awisException $e)
{
	echo $e->getMessage(). '/' . $e->getSQL();
}
catch (Exception $e)
{
	echo $e->getMessage() . '/' . $e->getCode();
}
*/

?>