<?php
// Variablen
global $_POST;
global $_GET;
global $awisDBFehler;

$RechteStufe = awisBenutzerRecht($con, 1300);
if($RechteStufe==0)
{
   awisEreignis(3,1100,'Personaleinsätze',$_SERVER['PHP_AUTH_USER'],'','','');
   die("Keine ausreichenden Rechte!");
}

$Params = explode(";",awis_BenutzerParameter($con, "Personaleinsatz_Suche", $_SERVER['PHP_AUTH_USER']));

$RZListeStufe = awisBenutzerRecht($con, 1302);		// Einschränkungen nach RZ
$RZListe = explode(";",awis_BenutzerParameter($con, "Mitarbeiter_RZListe_Rechte", $_SERVER['PHP_AUTH_USER']));
$RZUserListe = '';
for($i=0;$i<16;$i++)
{
	if(($RZListeStufe&pow(2,$i))==pow(2,$i))
	{
		$RZUserListe .= ';' . $RZListe[$i+1];
	}
}
$RZUserListe .= ';';
//var_dump($RZUserListe);
echo "<form name=frmSuche method=post action=./perseins_Main.php>";

echo '<br>';
echo '<table border=1 width=100%>';

/******* Personal *******/
$EingabeFeld='txtPER_NR';
echo '<tr><td width=190>Mitar<u>b</u>eiter</td>';
echo '<td><select name=txtMIT_KEY accesskey=b>';

$rsPER = awisOpenRecordset($con, "SELECT * FROM Mitarbeiter ORDER BY MIT_BEZEICHNUNG");
$rsPERZeilen = $awisRSZeilen;

echo "<option value=0>::alle::</option>";
for($PerZeile=0;$PerZeile<$rsPERZeilen;$PerZeile++)
{
	if($RZUserListe!=';')
	{
		if(strstr($RZUserListe,';'.$rsPER['MIT_REZ_ID'][$PerZeile].';')=='')
		{
			continue;
		}
	}
	echo "<option value='" . $rsPER['MIT_KEY'][$PerZeile] . "'";
	
	if($Params[0]=="on")
	{
		if($Params[1]==$rsPER['MIT_KEY'][$PerZeile])		
		{
			echo ' selected ';
		}
	}
	echo ">" . $rsPER['MIT_BEZEICHNUNG'][$PerZeile] . "</option>";
}

echo "</select></td></tr>";

/******* Datum *******/
echo '<tr>';
echo '<td>Datum vom</td>';
echo '<td><input name=txtDatumVom size=8 tabindex=50 value=' . ($Params[0]=="on"?$Params[2]:date('d.m.Y')) . '>';
echo '&nbsp;bis&nbsp;<input name=txtDatumBis size=8 tabindex=60 value=' . ($Params[0]=="on"?$Params[3]:date('d.m.Y')) . '></td>';
echo '</tr>';

/******* Filiale *******/
echo '<tr>';
echo '<td>Filiale</td>';
echo '<td><input name=txtFIL_ID size=5 tabindex=30 value=' . ($Params[0]=="on"?$Params[4]:'')  . '></td>';
echo '</tr>';

/******* Regionalzentren *******/

echo '<tr>';
echo '<td>Regionalzentrum</td>';
echo '<td>';
$rsREZ = awisOpenRecordset($con,"SELECT * FROM Regionalzentren ORDER BY REZ_BEZEICHNUNG");
$rsREZZeilen = $awisRSZeilen;
echo "<select name=txtMIT_REZ_ID>";

if($RZUserListe==';')
{
	echo '<option value=0>::alle Regionalzentren::</option>';
}

for($i=0; $i<$rsREZZeilen;$i++)
{
	if($RZUserListe!=';')
	{
		if(strstr($RZUserListe,';'.$rsREZ["REZ_ID"][$i].';')=='')
		{
			continue;
		}
	}
	echo "<option ";
	if($rsREZ["REZ_ID"][$i] == ($Params[0]=="on"?$Params[5]:''))
	{
  		echo " selected ";
	}
	echo "value=" . $rsREZ["REZ_ID"][$i] . ">". $rsREZ["REZ_BEZEICHNUNG"][$i] . "</option>";
}
echo "</select></td></tr>";




/******* Auswahl speichern *******/
echo '<tr><td colspan=2>&nbsp;</td></tr>';
print "<tr><td width=190>Auswahl <u>s</u>peichern:</td>";
print "<td><input type=checkbox value=on " . ($Params[0]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=80></td></tr>";
	
echo '</table>';

	// Weiter und Reset - Buttons
echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png alt='Formularinhalt löschen' name=cmdReset onclick=location.href='./perseins_Main.php?Reset=True';>";
print "<input type=hidden name=cmdAktion value=Einsaetze>";

echo '</form>';


/*********************************
* Cursor positionieren
**********************************/

if($EingabeFeld!='')
{
	echo "\n<Script Language=JavaScript>";
	echo "document.getElementsByName('txtMIT_KEY')[0].focus();";
	echo "\n</Script>";
}

?>
