<?php
require_once 'bewerberverwaltung_Details_Bewerbungen_Funktionen.php';
require_once('awisDokument.inc');
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Speichernmeldung;
global $Aktionsmeldung;
global $Formatfehler;
global $KeinWertFehler;
global $EingabeOK;
global $Folgestatus;
global $Gespeichert;
global $Duplikat;
global $SEQBWH;
global $SEQ;
global $ScrollPosition;

try {
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();

    $AWIS_KEY1 = isset($_POST['txtHRK_KEY'])?$_POST['txtHRK_KEY']:$SEQ;
    $Form->DebugAusgabe(1, 'AWIS_KEY1: ' . $AWIS_KEY1);

    $TextKonserven = array();
    $TextKonserven[] = array(
        'HRK',
        '%'
    );
    $TextKonserven[] = array(
        'BEW',
        '%'
    );
    $TextKonserven[] = array(
        'Fehler',
        'err_KeinWert'
    );
    $TextKonserven[] = array(
        'Fehler',
        'err_FelderVeraendert'
    );
    $TextKonserven[] = array(
        'Fehler',
        'err_DatumEndeVorAnfang'
    );
    $TextKonserven[] = array(
        'Fehler',
        'err_DatumAbstandTage'
    );
    $TextKonserven[] = array(
        'Wort',
        'geaendert_von'
    );
    $TextKonserven[] = array(
        'Wort',
        'geaendert_auf'
    );
    $TextKonserven[] = array(
        'Meldung',
        'DSVeraendert'
    );
    $TextKonserven[] = array(
        'Meldung',
        'EingabeWiederholen'
    );
    $TextKonserven[] = array(
        'Wort',
        'txt_BitteWaehlen'
    );

    $FehlerListe = array();

    $TXT_Speichern = $Form->LadeTexte($TextKonserven);
    // Anfang Kopfdaten
    // Alle Felder die geupdatet oder geinsertet werden sollen
    $UpdateInsertFelder[0]['Feldname'] = 'HRK_VORNAME';
    $UpdateInsertFelder[0]['Format'] = 'T';
    $UpdateInsertFelder[0]['Pflicht'] = '1';

    $UpdateInsertFelder[1]['Feldname'] = 'HRK_TITEL';
    $UpdateInsertFelder[1]['Format'] = 'T';
    $UpdateInsertFelder[1]['Pflicht'] = '0';

    $UpdateInsertFelder[2]['Feldname'] = 'HRK_TELEFON';
    $UpdateInsertFelder[2]['Format'] = 'T';
    $UpdateInsertFelder[2]['Pflicht'] = '0';

    $UpdateInsertFelder[3]['Feldname'] = 'HRK_STRASSEHNR';
    $UpdateInsertFelder[3]['Format'] = 'T';
    $UpdateInsertFelder[3]['Pflicht'] = '0';

    $UpdateInsertFelder[4]['Feldname'] = 'HRK_PLZ';
    $UpdateInsertFelder[4]['Format'] = 'PLZ';
    $UpdateInsertFelder[4]['Pflicht'] = '0';

    $UpdateInsertFelder[5]['Feldname'] = 'HRK_ORT';
    $UpdateInsertFelder[5]['Format'] = 'T';
    $UpdateInsertFelder[5]['Pflicht'] = '0';

    $UpdateInsertFelder[6]['Feldname'] = 'HRK_NACHNAME';
    $UpdateInsertFelder[6]['Format'] = 'T';
    $UpdateInsertFelder[6]['Pflicht'] = '1';

    $UpdateInsertFelder[7]['Feldname'] = 'HRK_MOBIL';
    $UpdateInsertFelder[7]['Format'] = 'T';
    $UpdateInsertFelder[7]['Pflicht'] = '0';

    $UpdateInsertFelder[8]['Feldname'] = 'HRK_LAN_NR';
    $UpdateInsertFelder[8]['Format'] = 'N0';
    $UpdateInsertFelder[8]['Pflicht'] = '1';

    $UpdateInsertFelder[9]['Feldname'] = 'HRK_GEBURTSDATUM';
    $UpdateInsertFelder[9]['Format'] = 'D';
    $UpdateInsertFelder[9]['Pflicht'] = '1';

    $UpdateInsertFelder[10]['Feldname'] = 'HRK_EMAIL';
    $UpdateInsertFelder[10]['Format'] = 'M';

    // Wenn die Bewerbung per Emaileingegangen wurde, ist die Email pflicht.
    if (isset($_POST['txtEingangsArt']) and $_POST['txtEingangsArt'] == 2) {
        $UpdateInsertFelder[10]['Pflicht'] = '1';
    } else {
        $UpdateInsertFelder[10]['Pflicht'] = '0';
    }

    $UpdateInsertFelder[11]['Feldname'] = 'HRK_ANR_ID';
    $UpdateInsertFelder[11]['Format'] = 'N0';
    $UpdateInsertFelder[11]['Pflicht'] = '1';

    $KeinWertFehler = '';
    $Formatfehler = '';
    $ZamiNrFehler = '';
    $ZamiWertigkeitFehler = '';
    $SpeicherFehler = false;

    $Duplikat = false;

    // Formate Pr�fen
    foreach ($UpdateInsertFelder as $Element => $Inhalt) {
        $Feld = $Inhalt['Feldname'];
        $Format = $Inhalt['Format'];
        $Pflicht = $Inhalt['Pflicht'];

        $Praefix = (isset($_POST['txt' . $Feld])?'txt':'suc');

        // Pflichtfelder pr�fen
        if ($Pflicht == 1) {

            if (isset($_POST[$Praefix . $Feld]) and ($_POST[$Praefix . $Feld] == '' or $_POST[$Praefix . $Feld] == '::bitte w�hlen::')) {
                $KeinWertFehler .= $TXT_Speichern['HRK'][$Feld] . ', ';
                continue; // Formatpr�fung macht kein Sinn.. gleich n�chstes Element anschauen.
            }
        }

        // Formate Pr�fen
        if (isset($_POST[$Praefix . $Feld]) and $_POST[$Praefix . $Feld] != '') {
            pruefeWert($_POST[$Praefix . $Feld], $Feld, 'HRK', '', $Format, $TXT_Speichern);
        }
    }

    // Wurden Fehler entdeckt? => Speichern abbrechen
    if ($KeinWertFehler != '') {
        $SpeicherFehler = true;
        $Form->ZeileStart();
        $Form->Hinweistext($TXT_Speichern['Fehler']['err_KeinWert'] . ' ' . substr($KeinWertFehler, 0, strlen($KeinWertFehler) - 2), 1, '');
        $Form->ZeileEnde();
    }
    if ($Formatfehler != '') {
        $SpeicherFehler = true;
        $Form->ZeileStart();
        $Form->Hinweistext($Formatfehler, 1, '');
        $Form->ZeileEnde();
    }
    if ($AWIS_KEY1 == '-1' and $Formatfehler == '' and $KeinWertFehler == '' and !isset($_POST['cmdDuplikat'])) {
        // Schauen ob es einen �hnlichen Datensatz schon gibt
        $SQLDuplikat = 'select HRK_KEY ,';
        $SQLDuplikat .= ' HRK_ANR_ID ,';
        $SQLDuplikat .= ' HRK_TITEL ,';
        $SQLDuplikat .= ' HRK_VORNAME ,';
        $SQLDuplikat .= ' HRK_NACHNAME ,';
        $SQLDuplikat .= ' HRK_GEBURTSDATUM ,';
        $SQLDuplikat .= ' HRK_USER ,';
        $SQLDuplikat .= ' HRK_USERDAT ,';
        $SQLDuplikat .= ' HRK_STRASSEHNR ,';
        $SQLDuplikat .= ' HRK_PLZ ,';
        $SQLDuplikat .= ' HRK_ORT ,';
        $SQLDuplikat .= ' HRK_LAN_NR ,';
        $SQLDuplikat .= ' HRK_EMAIL ,';
        $SQLDuplikat .= ' HRK_TELEFON ,';
        $SQLDuplikat .= ' HRK_MOBIL  from HRKOPF ';
        $SQLDuplikat .= ' where SOUNDEXD(HRK_VORNAME) = SOUNDEXD(\'' . $_POST['txtHRK_VORNAME'] . '\')';
        $SQLDuplikat .= ' and SOUNDEXD(HRK_NACHNAME) = SOUNDEXD(\'' . $_POST['txtHRK_NACHNAME'] . '\')';
        $SQLDuplikat .= ' and HRK_GEBURTSDATUM = \'' . $_POST['txtHRK_GEBURTSDATUM'] . '\'';
        if ($DB->ErmittleZeilenAnzahl($SQLDuplikat) > 0) {
            $Duplikat = true;
            $SpeicherFehler = true;
            $Form->ZeileStart();
            $Form->Hinweistext($TXT_Speichern['HRK']['HRK_ERR_DUPLIKAT'] . ' ' . substr($KeinWertFehler, 0, strlen($KeinWertFehler) - 2), 1, '');
            $Form->ZeileEnde();
        }
    }

    // AWIS_KEY1 ist in jedem Fall gesetzt.
    $SEQ = $AWIS_KEY1;
    if ($KeinWertFehler == '' and $Formatfehler == '' and $Duplikat == false) {
        $SQL = '';
        if ($AWIS_KEY1 == '-1')         // Neuer DS
        {
            $SQL = 'INSERT INTO HRKOPF';
            $SQL .= '(HRK_USER, HRK_USERDAT';
            $SQLFelder = '';
            $SQLWerte = '';
            foreach ($UpdateInsertFelder as $Element => $Inhalt) {
                $Feld = $Inhalt['Feldname'];
                $Praefix = (isset($_POST['txt' . $Feld])?'txt':'suc');

                if (isset($_POST[$Praefix . $Feld]) and $_POST[$Praefix . $Feld] != '' and $_POST[$Praefix . $Feld] != '::bitte w�hlen::') {
                    $SQLFelder .= ', ' . $Feld;
                    $SQLWerte .= ', \'' . $_POST[$Praefix . $Feld] . '\'';
                }
            }
            $SQL .= $SQLFelder; // Relevante Datenfelder dazuf�gen
            $SQL .= ')VALUES (';
            $SQL .= '\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ',SYSDATE';
            $SQL .= $SQLWerte; // Relevante Datenwerte dazuf�gen.
            $SQL .= ')';

            $DB->Ausfuehren($SQL);

            $SEQSQL = 'select seq_hrk_key.CURRVAL AS Key from dual';
            $rsSEQ = $DB->RecordSetOeffnen($SEQSQL);
            $SEQ = $rsSEQ->FeldInhalt('KEY');
            $Form->Hinweistext($TXT_Speichern['HRK']['HRK_INSERTOK']);
        } else         // ge�nderterDS
        {
            $FeldListe = '';
            foreach ($UpdateInsertFelder as $Element => $Inhalt) {
                $Feld = $Inhalt['Feldname'];
                $Praefix = (isset($_POST['txt' . $Feld])?'txt':'suc');
                if ((isset($_POST['old' . $Feld]) or $Praefix == 'suc') and isset($_POST[$Praefix . $Feld]))                 // Sucfelder haben kein old
                {
                    $WertNeu = $_POST[$Praefix . $Feld] != $TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST[$Praefix . $Feld]:'';
                    $WertAlt = $Praefix == 'txt'?$_POST['old' . $Feld]:'';

                    if ($WertAlt != $WertNeu) {
                        // echo $Feld . "<br>";
                        $FeldListe .= ', ' . $Feld . '=';
                        if ($WertNeu == '')                         // Leere Felder immer als NULL
                        {
                            $FeldListe .= ' null';
                        } else {
                            $FeldListe .= '\'' . $WertNeu . '\'';
                        }
                    }
                }
            }
            if ($FeldListe != '') {
                $Form->DebugAusgabe(1, $AWIS_KEY1);
                $SQL = 'UPDATE HRKOPF SET';
                $SQL .= substr($FeldListe, 1);
                $SQL .= ', HRK_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ', HRK_USERDAT=sysdate';
                $SQL .= ' WHERE HRK_KEY=0' . $AWIS_KEY1 . '';
                $Form->DebugAusgabe(1, $SQL);
                $DB->Ausfuehren($SQL);
                $Form->Hinweistext($TXT_Speichern['HRK']['HRK_UPDATEOK']);
                $SEQ = $AWIS_KEY1;
            }
        }
    }
    // Ende Kopfdaten

    // Anfang Detaildaten

    // Pr�fen ob es ein Pr�flauf oder eine echte Speicherung ist.
    if (issetCMDStatusWeiter()) {
        $Speichern = true;
    } elseif (isset($_POST['txtBWS_KEY'])) {
        $Folgetatus = '';

        $SQL = 'SELECT';
        $SQL .= '   BWS_KEY,';
        $SQL .= '   BWS_BEZEICHNUNG,';
        $SQL .= '   BWS_TEXTKONSERVE,';
        $SQL .= '   BWS_AKTIV,';
        $SQL .= '   BWS_DATENSATZAKTIV,';
        $SQL .= '   BWS_STEUER_ID,';
        $SQL .= '   BWS_ZWISCHENSEITENTYP, BWS_AKTIONSSCRIPT';
        $SQL .= ' FROM';
        $SQL .= '   BEWERBERSTATUS ';
        $SQL .= ' WHERE BWS_AKTIV = \'A\'';
        $SQL .= ' AND BWS_KEY' . $DB->LikeOderIst($_POST['txtBWS_KEY'] != $TXT_Speichern['Wort']['txt_BitteWaehlen']?$_POST['txtBWS_KEY']:'0');

        $rsStatus = $DB->RecordSetOeffnen($SQL);

        if ($rsStatus->FeldInhalt('BWS_ZWISCHENSEITENTYP') == 0) {
            $Speichern = true;
        } elseif ($rsStatus->FeldInhalt('BWS_ZWISCHENSEITENTYP') == 1 or $rsStatus->FeldInhalt('BWS_ZWISCHENSEITENTYP') == 2) {
            $Folgestatus = $_POST['txtBWS_KEY'];
            $Speichern = false;
        }
    } else {
        $Speichern = false;
    }

    if ($AWIS_KEY1 > 0)     // Nicht bei Kopfneuanlage
    {
        if ($AWIS_KEY2 == '-1')         // Datensatzneuanlage
        {
            if ($Speichern)             // Nur wenn ich wirklich speichern muss, und es kein Pr�flauf ist..
            {
                $DB->TransaktionBegin();
                $InsertZuord = 'INSERT';
                $InsertZuord .= ' INTO';
                $InsertZuord .= '   bewerberhrzuord';
                $InsertZuord .= '   (';
                $InsertZuord .= '     BWH_BWS_KEY,';
                $InsertZuord .= '     BWH_HRF_KEY,';
                $InsertZuord .= '     BWH_HRK_KEY,';
                $InsertZuord .= '     BWH_USER,';
                $InsertZuord .= '     BWH_USERDAT';
                $InsertZuord .= '   )';
                $InsertZuord .= '   VALUES';
                $InsertZuord .= '   (';
                $InsertZuord .= '     1,';
                $InsertZuord .= $_POST['txtBEW_FIRM'] . ', ';
                $InsertZuord .= $AWIS_KEY1 . ', ';
                $InsertZuord .= '\'' . $AWISBenutzer->BenutzerName() . '\', ';
                $InsertZuord .= '    sysdate';
                $InsertZuord .= '   )';

                $Form->DebugAusgabe(1, 'Insert Bewerberzuordnung: ', $InsertZuord);

                $DB->Ausfuehren($InsertZuord);

                $SEQBWHSQL = 'select seq_bwh_key.CURRVAL AS Key from dual';
                $rsBWHSEQ = $DB->RecordSetOeffnen($SEQBWHSQL);
                $SEQBWH = $rsBWHSEQ->FeldInhalt('KEY');
                $AWIS_KEY2 = $SEQBWH;
            }
        }

        // Allgemeine Daten f�r den Datensatz
        $SQL = 'SELECT';
        $SQL .= '   HRT_KEY ,';
        $SQL .= '   BWH_KEY , BWH_USER, BWH_USERDAT,';
        $SQL .= '   HRT_SUCH ,';
        $SQL .= '  HRF_KEY, HRF_FIRMIERUNG , HRK_KEY, BWS_KEY, FUNC_TEXTKONSERVE(XTX_KENNUNG, XTX_BEREICH, \'' . $AWISBenutzer->BenutzerSprache() . '\') as BWS_TEXT, ';
        $SQL .= '   EINGANGSDATUM';
        $SQL .= ' FROM';
        $SQL .= '   V_BEWERBERVERWALTUNG';
        $SQL .= ' WHERE HRK_KEY ' . $DB->LikeOderIst($AWIS_KEY1);
        $SQL .= ' and bwh_key ' . $DB->LikeOderIst(isset($_GET['BWH_KEY']) && $_GET['BWH_KEY'] > 0?$_GET['BWH_KEY']:$SEQBWH);
        $Form->DebugAusgabe(1, "SQL Bewerbungen: " . $SQL);
        $rsBewerbungen = $DB->RecordSetOeffnen($SQL);

        // Alle Felder aus den Infostypen holen
        $SQLDetail = 'select';
        $SQLDetail .= ' a.BIT_KEY ,';
        $SQLDetail .= ' a.BIT_INFORMATION ,';
        $SQLDetail .= ' a.BIT_BEMERKUNG ,';
        $SQLDetail .= ' a.BIT_HRF_KEY ,';
        $SQLDetail .= ' a.BIT_XRC_ID_ANZEIGE ,';
        $SQLDetail .= ' a.BIT_RECHTEBIT_ANZEIGE ,';
        $SQLDetail .= ' a.BIT_STATUS ,';
        $SQLDetail .= ' SUBSTR(bit_textkonserve,0,instr(bit_textkonserve,\':\')-1) AS  TXTBEREICH,
               SUBSTR(bit_textkonserve,instr(bit_textkonserve,\':\')+1) AS TXTKENNUNG ,';
        $SQLDetail .= ' a.BIT_DATENQUELLE ,';
        $SQLDetail .= ' a.BIT_FORMAT ,';
        $SQLDetail .= ' a.BIT_FELDTYP ,';
        $SQLDetail .= ' a.BIT_LABELBREITE ,';
        $SQLDetail .= ' a.BIT_FELDBREITE ,';
        $SQLDetail .= ' a.BIT_FELDLAENGE ,';
        $SQLDetail .= ' a.BIT_PFLICHT ,';
        $SQLDetail .= ' a.BIT_USER ,';
        $SQLDetail .= ' a.BIT_USERDAT ,';
        $SQLDetail .= ' a.BIT_SICHTBAR ,';
        $SQLDetail .= ' a.BIT_RECHTEBIT_BEARBEITEN ,';
        $SQLDetail .= ' a.BIT_ZUORDKUERZEL ,';
        $SQLDetail .= ' a.BIT_ZUORDNUNG,';
        $SQLDetail .= ' a.BIT_SORTIERUNG, a.BIT_LETZTEZEILE';
        $SQLDetail .= ' from';
        $SQLDetail .= '   BEWERBERINFOSTYPEN a';
        $SQLDetail .= ' where';
        $SQLDetail .= '   a.BIT_STATUS     = \'A\'';
        $SQLDetail .= ' and a.BIT_SICHTBAR = 1';
        $SQLDetail .= ' and ( a.BIT_HRF_KEY = 0 or
                          a.BIT_HRF_KEY ' . $DB->LikeOderIst(($rsBewerbungen->FeldInhalt('HRF_KEY') > 0?$rsBewerbungen->FeldInhalt('HRF_KEY'):(isset($_POST['txtBEW_FIRM'])?$_POST['txtBEW_FIRM']:0)));
        $SQLDetail .= ')';
        $Form->DebugAusgabe(1, 'SQL DETAIL Speichern: ' . $SQLDetail);
        $rsDetail = $DB->RecordSetOeffnen($SQLDetail);

        if ($Speichern)         // Nur wenn es wirklich ums speichern geht.. da es ansonsten nur ein Pr�flauf ist zum Statusweiterschubsen.
        {
            // Firmierung kommt nicht aus den Infostypen.. deswegen einzeln speichern.
            if (isset($_POST['txtBEW_FIRM']) and $_POST['txtBEW_FIRM'] != $TXT_Speichern['Wort']['txt_BitteWaehlen']) {
                $UpdateBWH = ' Update BEWERBERHRZUORD';
                $UpdateBWH .= ' set BWH_HRF_KEY = ' . $_POST['txtBEW_FIRM'];
                $UpdateBWH .= ' WHERE BWH_KEY ' . $DB->LikeOderIst($_GET['BWH_KEY'] > 0?$_GET['BWH_KEY']:$SEQBWH);

                $Form->DebugAusgabe(2, "Update BWH: " . $UpdateBWH);
                $DB->Ausfuehren($UpdateBWH);
            }

            $Rollback = false;
            
            while (!$rsDetail->EOF()) {
                $Feld = $rsDetail->FeldInhalt('BIT_INFORMATION');
                $Format = $rsDetail->FeldInhalt('BIT_FORMAT');
                $Typ = $rsDetail->FeldInhalt('BIT_FELDTYP');
                $Praefix = (isset($_POST['txt' . $Feld])?'txt':'suc');

                $WertNeu = isset($_POST[$Praefix . $Feld])?$_POST[$Praefix . $Feld]:'';
                
                
                if (isset($_POST[$Praefix . $Feld])) {
                    if (pruefeWert($WertNeu, $Feld, $rsDetail->FeldInhalt('TXTBEREICH'), $rsDetail->FeldInhalt('TXTKENNUNG'), $Format, $TXT_Speichern,
                        $rsDetail->FeldInhalt('BIT_PFLICHT'))) {
                        if (is_array($WertNeu)) {
                            $WertArray = $WertNeu;
                        } else {
                            $WertArray = array($WertNeu);
                        }

                        $SQLPruefeBIF = 'SELECT bif_key, BIF_Wert,BIF_IMQ_ID from BEWERBERINFOS ';
                        $SQLPruefeBIF .= ' where BIF_BWH_KEY' . $DB->LikeOderIst($AWIS_KEY2);
                        $SQLPruefeBIF .= ' AND BIF_BIT_ID' . $DB->LikeOderIst($rsDetail->FeldInhalt('BIT_KEY'));

                        $rsPruefeBIF = $DB->RecordSetOeffnen($SQLPruefeBIF);
                        $Form->DebugAusgabe(1, $DB->LetzterSQL());
                        foreach ($WertArray as $WertNeu) {
                            $WertNeu = ($WertNeu == $TXT_Speichern['Wort']['txt_BitteWaehlen']?0:$WertNeu);
                            if ($rsPruefeBIF->AnzahlDatensaetze() == 0 and count($WertArray) == 1)                         // Keine Bewerberinfo f�r diesen Infotypen vorhanden
                            {
                                // Nur Inserten wenn auch Wirklich a Wert da ist.
                                if ($WertNeu != '' and $WertNeu != $TXT_Speichern['Wort']['txt_BitteWaehlen']) {
                                    $InsertBIF = 'insert into BEWERBERINFOS ';
                                    $InsertBIF .= ' (BIF_BIT_ID,';
                                    $InsertBIF .= ' BIF_BWH_KEY,';
                                    $InsertBIF .= ' BIF_IMQ_ID,';
                                    $InsertBIF .= ' BIF_WERT,';
                                    $InsertBIF .= ' BIF_USER,';
                                    $InsertBIF .= ' BIF_USERDAT';
                                    $InsertBIF .= ' )';
                                    $InsertBIF .= ' values';
                                    $InsertBIF .= ' (' . $rsDetail->FeldInhalt('BIT_KEY') . ',';
                                    $InsertBIF .= $AWIS_KEY2 . ',';
                                    $InsertBIF .= ' 4,';
                                    $InsertBIF .= '\'' . $WertNeu . '\',';
                                    $InsertBIF .= '\'' . $AWISBenutzer->BenutzerName() . '\',';
                                    $InsertBIF .= ' sysdate';
                                    $InsertBIF .= ' )';

                                    $DB->Ausfuehren($InsertBIF);
                                }
                            } else                         // Bestehende Bewerberinfo Updaten
                            {
                                $WertAlt = $rsPruefeBIF->FeldInhalt('BIF_WERT');
                                if ($WertAlt != $WertNeu) {
                                    $Form->DebugAusgabe(2, "Feld: " . $Praefix . $Feld . "<br> Wert alt: " . $WertAlt . "<br> Wert neu: $WertNeu");

                                    if ($rsPruefeBIF->AnzahlDatensaetze() > 1 or count($WertArray) > 1) {
                                        while (!$rsPruefeBIF->EOF()) {
                                            $DeleteBIF = "DELETE FROM BEWERBERINFOS";
                                            $DeleteBIF .= " WHERE BIF_KEY " . $DB->LikeOderIst($rsPruefeBIF->FeldInhalt('BIF_KEY'));
                                            $DB->Ausfuehren($DeleteBIF);
                                            $rsPruefeBIF->DSWeiter();
                                        }
                                        $rsPruefeBIF->DSErster();

                                        if ($WertNeu != '' and $WertNeu != $TXT_Speichern['Wort']['txt_BitteWaehlen']) {
                                            $InsertBIF = 'insert into BEWERBERINFOS ';
                                            $InsertBIF .= ' (BIF_BIT_ID,';
                                            $InsertBIF .= ' BIF_BWH_KEY,';
                                            $InsertBIF .= ' BIF_IMQ_ID,';
                                            $InsertBIF .= ' BIF_WERT,';
                                            $InsertBIF .= ' BIF_USER,';
                                            $InsertBIF .= ' BIF_USERDAT';
                                            $InsertBIF .= ' )';
                                            $InsertBIF .= ' values';
                                            $InsertBIF .= ' (' . $rsDetail->FeldInhalt('BIT_KEY') . ',';
                                            $InsertBIF .= $AWIS_KEY2 . ',';
                                            $InsertBIF .= ' 4,';
                                            $InsertBIF .= '\'' . $WertNeu . '\',';
                                            $InsertBIF .= '\'' . $AWISBenutzer->BenutzerName() . '\',';
                                            $InsertBIF .= ' sysdate';
                                            $InsertBIF .= ' )';

                                            $DB->Ausfuehren($InsertBIF);
                                        }
                                    } else {
                                        $UpdateBIF = 'update';
                                        $UpdateBIF .= '   BEWERBERINFOS';
                                        $UpdateBIF .= ' set';
                                        $UpdateBIF .= '   BIF_BIT_ID  = \'' . $rsDetail->FeldInhalt('BIT_KEY') . '\',';
                                        $UpdateBIF .= '   BIF_WERT = \'' . $WertNeu . '\',';
                                        $UpdateBIF .= '   BIF_BWH_KEY = \'' . $AWIS_KEY2 . '\',';
                                        $UpdateBIF .= '   BIF_IMQ_ID  = \'' . ($rsPruefeBIF->FeldInhalt('BIF_IMQ_ID') == '4'?'4':$rsPruefeBIF->FeldInhalt('BIF_IMQ_ID') + '4') . '\',';
                                        $UpdateBIF .= '   BIF_USER    = \'' . $AWISBenutzer->BenutzerName() . '\',';
                                        $UpdateBIF .= '   BIF_USERDAT = sysdate';
                                        $UpdateBIF .= ' where';
                                        $UpdateBIF .= '   BIF_KEY ' . $DB->LikeOderIst($rsPruefeBIF->FeldInhalt('BIF_KEY'));
                                        $DB->Ausfuehren($UpdateBIF);
                                    }
                                }
                            }
                        }
                    } else {
                        $Rollback = true;
                    }
                }

                $rsDetail->DSWeiter();
            }

            // Status kommt nicht aus den Infostypen..
            $BWS_KEY = '';
            if (isset($_POST['txtBWS_KEY']) and $_POST['txtBWS_KEY'] != $TXT_Speichern['Wort']['txt_BitteWaehlen']) {

                $BWS_KEY = $_POST['txtBWS_KEY'];
            }

            if ($BWS_KEY != '') {
                // Hat sich der Status auch wirklich ver�ndert?
                if ($rsBewerbungen->FeldInhalt('BWS_KEY') != $BWS_KEY) {
                    $UpdateBWH = ' Update BEWERBERHRZUORD';
                    $UpdateBWH .= ' set BWH_BWS_KEY = ' . $BWS_KEY;
                    $UpdateBWH .= ' , BWH_USER    = \'' . $AWISBenutzer->BenutzerName() . '\'';
                    $UpdateBWH .= ' , BWH_USERDAT = sysdate';
                    $UpdateBWH .= ' WHERE BWH_KEY ' . $DB->LikeOderIst($_GET['BWH_KEY'] > 0?$_GET['BWH_KEY']:$SEQBWH);

                    $Form->DebugAusgabe(2, "Update BWH: " . $UpdateBWH);
                    $DB->Ausfuehren($UpdateBWH);

                    $SQL = 'SELECT';
                    $SQL .= '   BWS_KEY,';
                    $SQL .= '   BWS_BEZEICHNUNG,';
                    $SQL .= '   BWS_TEXTKONSERVE,';
                    $SQL .= '   BWS_AKTIV,';
                    $SQL .= '   BWS_DATENSATZAKTIV,';
                    $SQL .= '   BWS_STEUER_ID,';
                    $SQL .= '   BWS_ZWISCHENSEITENTYP, BWS_AKTIONSSCRIPT';
                    $SQL .= ' FROM';
                    $SQL .= '   BEWERBERSTATUS ';
                    $SQL .= ' WHERE BWS_AKTIV = \'A\'';
                    $SQL .= ' AND BWS_KEY' . $DB->LikeOderIst($_POST['txtBWS_KEY']);

                    $rsStatus = $DB->RecordSetOeffnen($SQL);

                    // Muss ich Aktionen ausl�sen?
                    if ($rsStatus->FeldInhalt('BWS_AKTIONSSCRIPT') != '') {
                        include($rsStatus->FeldInhalt('BWS_AKTIONSSCRIPT'));
                    }

                    $ScrollPosition = 'Status';
                }
            } else {
                $Rollback = false;
            }

            // F�r die Filiale soll nach 4 Wochen eskaliert werden.
            // Wenn der POST gesetzt ist, dann handelt es sich um einen Filialdatensatz
            // und es muss entsprechend geinsertet werden.
            if (isset($_POST['txtEskalationsGesendetAn']) and !isset($_POST['txtBEW_STATUS'])) {
                insertBIF($AWIS_KEY2, 46, 0);
                insertBIF($AWIS_KEY2, 47, date("d.m.Y"));
            }

            if ($Rollback) {
                $DB->TransaktionRollback();
            } else {
                $DB->TransaktionCommit();
                $Gespeichert = true;
            }
        } else         // Die Daten nur �berpr�fen, umzuschauen, ob der Datensatz komplett ist.
        {

            while (!$rsDetail->EOF()) {
                $Feld = $rsDetail->FeldInhalt('BIT_INFORMATION');
                $Format = $rsDetail->FeldInhalt('BIT_FORMAT');
                $Typ = $rsDetail->FeldInhalt('BIT_FELDTYP');
                $Praefix = (isset($_POST['txt' . $Feld])?'txt':'suc');
                $WertNeu = isset($_POST[$Praefix . $Feld])?$_POST[$Praefix . $Feld]:'';

                if (is_array($WertNeu)) {
                    $WertArray = $WertNeu;
                } else {
                    $WertArray = array($WertNeu);
                }
                if(isset($_POST['txtGesendetAnCMB']) and $Feld == 'GesendetAn' and $_POST['txtGesendetAnCMB'] != $TXT_Speichern['Wort']['txt_BitteWaehlen'] ){
                    $WertNeu .= ($WertNeu!=''?';':'').$_POST['txtGesendetAnCMB'];
               
                    $_POST[$Praefix . $Feld] = $WertNeu;
                }

                if (isset($_POST[$Praefix . $Feld])) {
                    pruefeWert($WertNeu, $Feld, $rsDetail->FeldInhalt('TXTBEREICH'), $rsDetail->FeldInhalt('TXTKENNUNG'), $Format, $TXT_Speichern,
                        $rsDetail->FeldInhalt('BIT_PFLICHT'));
                }

                $rsDetail->DSWeiter();
            }
        }

        $EingabeOK = true;

        if ($KeinWertFehler != '') {
            $Speichernmeldung .= $TXT_Speichern['BEW']['BEW_ERR_KEINWERT'] . $KeinWertFehler . '<br>';

            $EingabeOK = false;
        }
        if ($Formatfehler != '') {
            $Speichernmeldung .= $Formatfehler;
            $EingabeOK = false;
        }

        if ($EingabeOK and $Speichern) {
            $Speichernmeldung .= $TXT_Speichern['BEW']['MSG_SPEICHERN'] . $Aktionsmeldung;
        }
    }

    // Ende Detaildaten

    // *****************************************************
    // Dokumente speichern
    // *****************************************************
    $Felder = $Form->NameInArray($_POST, 'txtDOC_', 1, 1);
    if ($Felder != '') {
        $Felder = explode(';', $Felder);
        $DOC = new awisDokument($DB, $AWISBenutzer);
        $Zuordnungen[] = array(
            'BEW',
            $AWIS_KEY2
        );
        // $AWIS_KEY2 = $DOC->DokumentSpeichern($_POST['txtDOC_KEY'],'DOC',$_POST['txtDOC_BEZEICHNUNG'],$_POST['txtDOC_DATUM'],$_POST['txtDOC_BEMERKUNG'],$AWISBenutzer->BenutzerID(),$Zuordnungen);

        if (isset($_FILES['DOC']['name'])) {
            $DateiInfo = pathinfo($_FILES['DOC']['name']);
            $Erweiterung = strtolower(isset($DateiInfo['extension'])?$DateiInfo['extension']:'');

            $Dokument = $DOC->ErzeugeDokumentenPfad($_POST['txtDOC_BEZEICHNUNG'], null, $_POST['txtDOC_DATUM'], $_POST['txtDOC_BEMERKUNG'], $Erweiterung, $Zuordnungen);
            if (move_uploaded_file($_FILES['DOC']['tmp_name'], $Dokument['pfad']) === false) {
                $Form->DebugAusgabe(1, $_FILES, $Dokument);
            } else {
                $Form->Hinweistext('Datei wurde Hochgeladen', 1, '');
            }
        } else {
            $DOC->DokumentenDatenAendern($_POST['txtDOC_KEY'], $_POST['txtDOC_DATUM'], $_POST['txtDOC_BEZEICHNUNG'], $_POST['txtDOC_BEMERKUNG']);
        }
    }
} catch (awisException $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
    $Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Form->Fehler_Anzeigen('SpeicherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}

function pruefeWert(
    $Wert,
    $Feld,
    $Konservenbereich = 'BEW',
    $Konservenfeld,
    $Format,
    $TXT_Speichern = array(),
    $Pflicht = false
) {
    global $Formatfehler;
    global $KeinWertFehler;
    $WertOK = true;
    $Form = new awisFormular();

    if ($Konservenfeld == '') {
        $Konservenfeld = $Feld;
    }
    // echo $Feld . '<br>';

    if (is_array($Wert)) {
        $WertArray = $Wert;
        $IsArray = true;
    } else {
        $WertArray = array($Wert);
        $IsArray = false;
    }

    foreach ($WertArray as $Wert) {
        // Schau ob Pflicht
        if (($Pflicht and $Wert == '') or ($Pflicht and $Wert == $TXT_Speichern['Wort']['txt_BitteWaehlen'] and !$IsArray) or ($IsArray and count($WertArray) == 0)) {
            $KeinWertFehler .= $TXT_Speichern[$Konservenbereich][$Konservenfeld] . ', ';
            $WertOK = false;
        }

        if ($Format == 'N0') {
            // Numerisch
            if (!ctype_digit($Wert)) {
                $WertOK = false;
                $Formatfehler .= $TXT_Speichern['BEW']['BEW_ERR_NUMMER'] . $TXT_Speichern[$Konservenbereich][$Konservenfeld] . '<br>';
            }
        } elseif (substr($Format, 0, 1) == 'D') {

            // Datum
            if ($Form->Format('D', $Wert, true) == '') {
                $WertOK = false;
                $Formatfehler .= $TXT_Speichern['BEW']['BEW_ERR_DATUM'] . $TXT_Speichern[$Konservenbereich][$Konservenfeld] . '<br>';
            } else {
                // Datum heute bis Zukunft
                if ($Format == 'DHZ')             // Datum heute bis Zukunft
                {
                    if ((strtotime($Wert) < strtotime(date('d.m.Y', time())))) {
                        $WertOK = false;
                        $Formatfehler .= $TXT_Speichern['BEW']['BEW_ERR_DATUM_VERGANGENHEIT'] . ' ' . $TXT_Speichern[$Konservenbereich][$Konservenfeld] . '<br>';
                    }
                }
            }
        } elseif (substr($Format, 0, 1) == 'M') {
            if (!zerlegeUndCheckEmails($Wert)) {
                $WertOK = false;
                $Formatfehler .= "Die eingegebene Emailadresse ist falsch";
            }
        } elseif ($Format == 'PLZ') {

            if (isset($_POST['txtHRK_LAN_NR'])) {
                $Length = 0;
                $Numerisch = false;

                if ($_POST['txtHRK_LAN_NR'] == 276)             // Deutschland
                {
                    $Length = 5;
                    $Numerisch = true;
                } elseif ($_POST['txtHRK_LAN_NR'] == 40)             // �sterreich
                {
                    $Length = 4;
                    $Numerisch = true;
                } elseif ($_POST['txtHRK_LAN_NR'] == 756)             // Schweiz
                {
                    $Length = 4;
                    $Numerisch = true;
                }
            }

            // Muss die PLZ Numerisch sein? Ist er nicht Nummerisch? Fehler
            if ($Numerisch and !ctype_digit($Wert)) {
                $WertOK = false;
                $Formatfehler .= $TXT_Speichern['BEW']['BEW_ERR_PLZ'] . $TXT_Speichern[$Konservenbereich][$Konservenfeld] . '<br>';
            } // Ist eine gewisse l�nge vorgegeben? Ist er nicht genauso lang? Fehler.
            elseif ($Length > 0 and strlen($Wert) != $Length) {
                $WertOK = false;
                $Formatfehler .= $TXT_Speichern['BEW']['BEW_ERR_PLZ'] . $TXT_Speichern[$Konservenbereich][$Konservenfeld] . '<br>';
            }
        }
    }

    return $WertOK;
}

?>