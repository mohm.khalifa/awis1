<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
global $AWISCursorPosition;
global $AWISBenutzer;

//$AWISSprache = awis_BenutzerParameter($con,'AnzeigeSprache',$AWISBenutzer->BenutzerName());

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('CAD','txt_ImportTyp');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','Uploaddatei');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

//$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);


$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

$AWISWerkzeuge = new awisWerkzeuge();

$Recht3701 = $AWISBenutzer->HatDasRecht(3701);
//$Recht3701=awisBenutzerRecht($con,3701);

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


$Form->SchreibeHTMLCode('<form name="frmSuche" method="POST" action="./crm_Main.php?cmdAktion=Adressimport" enctype="multipart/form-data">');

if(isset($_POST['sucImportTyp']))
{
	if ($_POST['sucImportTyp'] == '0')
	{
		include './crm_Import_0.php';
	}
	elseif ($_POST['sucImportTyp'] == '1')
	{		
		include './crm_Import_1.php';
	}	
	elseif ($_POST['sucImportTyp'] == '2')		// Hoppenstedt - Adressen
	{		
		include './crm_Import_2.php';
	}	
	elseif ($_POST['sucImportTyp'] == '3')		// Standard-Importdatei
	{
		include './crm_Import_3.php';
	}	
}
else 
{
	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_ImportTyp'].':',150);
	$SQL = 'SELECT XRS_BIT, XRS_BESCHREIBUNG';
	$SQL .= ' FROM Rechtestufen';
	//$SQL .= ' WHERE BITAND(POW(XRS_BIT,2),'.$Recht3701.'))>0';
	$SQL .= ' WHERE XRS_XRC_ID=3701';
	$Form->Erstelle_SelectFeld('*ImportTyp','',0,true,$SQL,'','','XRS_BESCHREIBUNG');
	$CursorFeld='sucImportTyp';
	$Form->ZeileEnde();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'].':',150);
	$Form->Erstelle_DateiUpload('datAdressen',200,20,7*1024*1024);
	$Form->ZeileEnde();

	$Form->Trennzeile();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['CAD']['txt_ImportAktion'].':',150);
	$ImportAktionen = explode("|",$AWISSprachKonserven['CAD']['lst_ImportAktionen']);
	$Form->Erstelle_SelectFeld('Aktion',1,100,true,'','','3','','',$ImportAktionen,'');
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_KON_KEY'].':',150);
	$SQL = "select DISTINCT * FROM (SELECT  kon_key, kon_name1 || ', ' || COALESCE(kon_name2,'') AS kontaktName";
	$SQL .= ' FROM kontakte';
	$SQL .= ' INNER JOIN benutzer B on kon_key = B.xbn_kon_key and xbn_status=\'A\'';
	$SQL .= ' INNER JOIN V_ACCOUNTRECHTE R on B.xbn_key = R.xbn_key';
	$SQL .= ' WHERE (R.XRC_ID = 3704 AND BITAND(XBA_STUFE,16)=16 and kon_status=\'A\') ';
	$SQL .= ' ) ORDER BY kontaktName';
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	$Form->Erstelle_SelectFeld('CAD_KON_KEY','',300,true,$SQL,$OptionBitteWaehlen,'','','','','');
	$Form->ZeileEnde();

	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Importkennung:',150);
	$Form->Erstelle_TextFeld('ImportKennung','',30,300,true,'','','','T','L','','',100);
	$Form->ZeileEnde();
	
	
	
	$Form->Formular_Ende($CursorFeld);
}
//***************************************
// Schaltfl�chen f�r dieses Register
//***************************************
$Form->SchaltflaechenStart();
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
$Form->SchaltflaechenEnde();

echo '</form>';

$Form->SetzeCursor($AWISCursorPosition);
?>