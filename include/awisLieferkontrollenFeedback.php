<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';
require_once 'awisMailer.inc';

class awisLieferkontrollenFeedback
{
    //AblageOrt
    const ExpPfad = '/daten/daten/tmp/';

    /**
     * @var awisDatenbank
     */
    protected $_DB;

    /**
     * @var awisBenutzer
     */
    protected $_Benutzer;

    /**
     * @var awisWerkzeuge
     */
    protected $_Werkzeuge;

    /**
     * @var int
     */
    protected $_DebugLevel = 0;

    /**
     * @var String
     */
    protected $_DebugText = "";

    /**
     * @var awisMailer
     */
    protected $_Mailer;

    /**
     * @var awisRecordset
     */
    protected $_rsLFK;

    protected $_ErrorCounter = 0;

    /**
     * awisCoreStoreData constructor.
     * @param string $Benutzer
     * @throws awisException
     */
    function __construct($Benutzer = 'awis_jobs')
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Benutzer = awisBenutzer::Init($Benutzer);
        $this->_Werkzeuge = new awisWerkzeuge();
        $this->_Mailer = new awisMailer($this->_DB,$this->_Benutzer);
    }

    /**
     * Erstellt die Exportdatei und versendet diese per Mail
     */
    public function export () {

        $SQL = "SELECT";
        $SQL .= " lkk.LKK_KEY,";
        $SQL .= " lkk.LKK_FIL_ID,";
        $SQL .= " lkk.LKK_LIEFERSCHEINNR,";
        $SQL .= " lkf.LKF_ZEIT,";
        $SQL .= " lkf.LKF_ZUFRIEDEN,";
        $SQL .= " lkf.LKF_SCHADEN,";
        $SQL .= " lkf.LKF_TEXT,";
        $SQL .= " lkf.LKF_DATUM,";
        $SQL .= " lkk.LKK_DATUMKOMM,";
        $SQL .= " (lkf.LKF_DATUM - lkk.LKK_DATUMKOMM) as LKF_DIFFTAGE,";
        $SQL .= " (select count(LIK_KEY) from LIEFERKONTROLLEN";
        $SQL .= "   where LIK_LIEFERSCHEINNR = lkk.LKK_LIEFERSCHEINNR";
        $SQL .= "   and LIK_FIL_ID = lkk.LKK_FIL_ID";
        $SQL .= "   and LIK_MENGESOLL != LIK_MENGEIST) as LKF_DIFFLIEF,";
        $SQL .= " lkk.LKK_STATUS";
        $SQL .= " FROM LIEFERKONTROLLENFEEDBACK lkf";
        $SQL .= " LEFT JOIN LIEFERKONTROLLENKOPF lkk";
        $SQL .= " ON lkf.LKF_LKK_KEY = lkk.LKK_KEY";
        $SQL .= " where lkf.LKF_STATUS = 0";
        $SQL .= " and (lkk.LKK_STATUS = 'A' ";
        $SQL .= " or lkk.LKK_STATUS = 'Z') ";

        $this->_rsLFK = $this->_DB->RecordSetOeffnen($SQL);

        $this->_Mailer->LoescheAdressListe();
        $this->_Mailer->Absender('shuttle@de.atu.eu');

        $this->debugAusgabe('Lade AdressListe', 99);

        foreach(explode(';',$this->_Benutzer->ParameterLesen('LIEFERFEEDBACK_MAIL_EMPFAENGER')) as $Empfaenger) {
            $this->_Mailer->AdressListe(awisMailer::TYP_TO,$Empfaenger);
        }

        $this->_Mailer->SetzeBezug('LKF', 0);

        if($this->_rsLFK->AnzahlDatensaetze()>0) {

            $this->debugAusgabe('Habe Daten. Schreibe Mail mit Anhang', 99);
            $this->_Mailer->Text('Anbei die Differenz-Uebersicht', awisMailer::FORMAT_TEXT);
            $this->_Mailer->Betreff('OK - Lieferkontrollen Feedbacks - ' . date('dmY'));

            $this->ladeAnhang();

        } else {
            $this->debugAusgabe('Habe keine Daten. Schreibe OK-Mail ohne Anhang', 99);
            $this->_Mailer->Text('Es wurden keine Daten gefunden/Keine Feedbacks wurden angelegt; Keine Datei in Anhang', awisMailer::FORMAT_TEXT);
            $this->_Mailer->Betreff('INFO - Lieferkontrollen Feedbacks - ' . date('dmY'));
        }

        $this->_Mailer->MailInWarteschlange();
    }
    /**
     *  Nimmt die Daten des Recordsets und setzt sie in eine csv, die dann als Anhang hinzugefuegt wird.
     */
    private function ladeAnhang() {

        $_rsLFK = $this->_rsLFK;

        $this->debugAusgabe('Beginne Export:', 1);
        $ExpDatei = 'atu_LieferFeedback_' . date('dmY');
        if (file_exists(self::ExpPfad . $ExpDatei)) {
            $this->debugAusgabe('Es liegen Reste vom letzten Export herum. Breche ab. Pfad der Reste: ' . self::ExpPfad, 1);
            die;
        }
        $this->debugAusgabe('Erstelle jetzt Datei', 99);
        $Datei = fopen(self::ExpPfad . $ExpDatei . '.csv', 'w+');

        $Zeile = '';
        $Zeile .= '"FILNR";';
        $Zeile .= '"LIEFERSCHEINNR";';
        $Zeile .= '"LIEFERZEITPUNKT";';
        $Zeile .= '"BESCHAEDIGUNG";';
        $Zeile .= '"ZUFRIEDENHEIT";';
        $Zeile .= '"BEMERKUNG";';
        $Zeile .= '"DATUM_KOMMISIONIERUNG";';
        $Zeile .= '"DATUM_LIEFERUNG";';
        $Zeile .= '"DIFFERENZ_TAG";';
        $Zeile .= '"DIFFERENZ_LIEF_POS";';
        $Zeile .= '"STATUS"';
        $Zeile .= chr(13) . chr(10);
        fwrite($Datei, $Zeile);

        $this->debugAusgabe('Schreibe jetzt Dateizeilen', 99);

        while (!$_rsLFK->EOF()) {
            $Zeile = "";

            $Zeile .= $_rsLFK->FeldInhalt('LKK_FIL_ID') . ';';
            $Zeile .= $_rsLFK->FeldInhalt('LKK_LIEFERSCHEINNR') . ';';
            //Zeitangabe zu Text
            if($_rsLFK->FeldInhalt('LKF_ZEIT')==1){
                $Zeile .= '"zu frueh";';
            }elseif($_rsLFK->FeldInhalt('LKF_ZEIT')==2){
                $Zeile .= '"puenktlich";';
            }elseif($_rsLFK->FeldInhalt('LKF_ZEIT')==3){
                $Zeile .= '"zu spaet";';
            }else{
                $Zeile .= '"keine Angabe";';
            }
            //Schaden zu Text
            if($_rsLFK->FeldInhalt('LKF_SCHADEN')==1){
                $Zeile .= '"Schaden";';
            }elseif($_rsLFK->FeldInhalt('LKF_SCHADEN')==0){
                $Zeile .= '"kein Schaden";';
            }else{
                $Zeile .= '"keine Angabe";';
            }
            //Zufriedenheit zu Text
            if($_rsLFK->FeldInhalt('LKF_ZUFRIEDEN')==1){
                $Zeile .= '"zufrieden";';
            }elseif($_rsLFK->FeldInhalt('LKF_ZUFRIEDEN')==0){
                $Zeile .= '"nicht zufrieden";';
            }else{
                $Zeile .= '"keine Angabe";';
            }
            $Zeile .= '"'.str_replace(array("\r\n", "\n", "\r"),' ',$_rsLFK->FeldInhalt('LKF_TEXT')) . '";';
            $Zeile .= $_rsLFK->FeldInhalt('LKK_DATUMKOMM') . ';';
            $Zeile .= $_rsLFK->FeldInhalt('LKF_DATUM') . ';';
            $Zeile .= $_rsLFK->FeldInhalt('LKF_DIFFTAGE') . ';';
            $Zeile .= $_rsLFK->FeldInhalt('LKF_DIFFLIEF') . ';';
            //Status zu Text
            if($_rsLFK->FeldInhalt('LKK_STATUS')=='A'){
                $Zeile .= 'Abgeschlossen';
            }elseif($_rsLFK->FeldInhalt('LKK_STATUS')=='Z'){
                $Zeile .= 'Zwangsabschluss';
            }else{
                $Zeile .= 'Fehler';
            }

            //LineFeed
            $Zeile .= chr(13) . chr(10);

            fwrite($Datei, $Zeile);

            $this->UpdateDatensatz($_rsLFK->FeldInhalt('LKK_KEY'));

            $_rsLFK->DSWeiter();
        }
        $this->debugAusgabe('Habe alle Zeilen geschrieben. Haenge als Anhang an', 99);

        fclose($Datei);
        $this->_Mailer->AnhaengeLoeschen();
        $this->_Mailer->Anhaenge(self::ExpPfad . $ExpDatei . '.csv', $ExpDatei . '.csv', true);
    }

    /**
     * Updated den Status der exportierten Datensaetze
     *
     * @param int $LKK_KEY
     */
    public function UpdateDatensatz($LKK_KEY){
        $this->debugAusgabe('Update KEY: '.$LKK_KEY, 99);

        $SQL = "UPDATE";
        $SQL .= " LIEFERKONTROLLENFEEDBACK";
        $SQL .= " SET ";
        $SQL .= " LKF_STATUS = ".$this->_DB->WertSetzen('LKF','Z',1);
        $SQL .= " WHERE";
        $SQL .= " LKF_LKK_KEY = ".$this->_DB->WertSetzen('LKF','Z',$LKK_KEY);

        $this->_DB->Ausfuehren($SQL,'',true,$this->_DB->Bindevariablen('LKF'));
    }

    /**
     * Setzt und returnt das Debuglevel
     *
     * @param int $Level
     * @return int
     */
    public function DebugLevel($Level = 0)
    {
        if ($Level != 0) {
            $this->_DebugLevel = $Level;
        }

        return $this->_DebugLevel;
    }

    /**
     * Debugausgabe ausgeben
     *
     * @param string $text
     * @param int $debugLevel
     */
    private function debugAusgabe($text, $debugLevel) {

        $this->_DebugText = $this->_DebugText.$text."\r\n";
        if ($this->_DebugLevel >= $debugLevel) {
            echo $text . ((substr($text, -(strlen(PHP_EOL))) == PHP_EOL)?'':PHP_EOL);
        }
    }
}