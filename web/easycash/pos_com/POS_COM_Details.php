<?php
global $AWISCursorPosition;		// Zum Cursor setzen
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PosComSuche','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_KeinWert');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Werkzeuge = new awisWerkzeuge();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht13020 = $AWISBenutzer->HatDasRecht(13020);
	if($Recht13020==0)
	{
	    awisEreignis(3,1000,'KundenKfzSuche',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PosComSuche'));

	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_GET['Liste']))
	{	
		$Param = array();
		$Param['FILIALE'] = $_GET['FILIALE'];
		$Param['BONDATUM'] = $_GET['BONDATUM'];
	}
	
	if(isset($_GET['FILIALE']) AND isset($_GET['BONDATUM']) AND isset($_GET['BONZEIT']) AND isset($_GET['BSA']))
	{	
		$Param = array();
		$Param['FILIALE'] = $_GET['FILIALE'];
		$Param['BONDATUM'] = $_GET['BONDATUM'];
		$Param['BONZEIT'] = $_GET['BONZEIT'];
		$Param['BSA'] = $_GET['BSA'];
	}
	
	if(isset($_POST['cmdSuche_x']))
	{	
		$Param = array();
		$Param['FILIALE'] = $_POST['sucFILIALE'];
		$Param['BONDATUM'] = $_POST['sucBONDATUM'];
		
		
		$errmsg='';
		foreach ($Param as $i => $Val)
		{
			if($Val==='')
			{
				$errmsg.="| $i ";
			}
		}

		if($errmsg!=='')
		{
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_KeinWert'].' '.$errmsg);		
			die();
		}
		
		$Param['BONZEIT'] = $_POST['sucBONZEIT'];
		
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_PosComSuche",serialize($Param));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_PosComSuche',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		//$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY FILIALE, BONDATUM, BONZEIT, BSA';
			$Param['ORDER']='FILIALE, BONDATUM, BONZEIT, BSA';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	
	global $BindeVariablen;
	
	$Bedingung = _BedingungErstellen($Param);

	$SQL= 'SELECT k.* ';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	
	if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
	{
		$SQL .= ' FROM EXPERIAN_ATU.POS_KOPF@COM_DE.ATU.DE k';		
	}
	else 
	{
		$SQL .= ' FROM EXPERIAN_ATU.POS_KOPF@COM_TEST_DE.ATU.DE k';
	}

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
		$Param['BLOCK']=$Block;
		$AWISBenutzer->ParameterSchreiben('Formular_PosComSuche',serialize($Param));
	}
	elseif(isset($Param['BLOCK']))
	{
		$Block=$Param['BLOCK'];
	}

	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);
	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);

	$SQL .= $ORDERBY;
	
	$Form->DebugAusgabe(1,$BindeVariablen);
	$Form->DebugAusgabe(1,$SQL);
	
	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsPOKSuche = $DB->RecordsetOeffnen($SQL,$BindeVariablen);

	$AWISBenutzer->ParameterSchreiben('Formular_PosComSuche',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmPosComSuche action=./POS_COM_Main.php?cmdAktion=Details method=POST>';
	
	if($rsPOKSuche->EOF())		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);		
	}
	elseif($rsPOKSuche->AnzahlDatensaetze()>1)						// Liste anzeigen
	{
		$style='font-size:1em';
		$style_mpl=1;	
		
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($style);

		$Form->Erstelle_Liste_Ueberschrift('',15*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FILIALE'],40*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONDATUM'],70*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONZEIT'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BSA'],30*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['KARTENNR'],160*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['VERTRAGSNR'],80*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONWERT'],55*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['LIEFERRG'],80*$style_mpl,$style,'');
		
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsPOKSuche->EOF())
		{
			$Form->ZeileStart($style);		

			$Form->Erstelle_ListenBild('href','Details','./POS_COM_Main.php?cmdAktion=Details&FILIALE='.$rsPOKSuche->FeldInhalt('FILIALE').'&BONDATUM='.$rsPOKSuche->FeldInhalt('BONDATUM').'&BONZEIT='.$rsPOKSuche->FeldInhalt('BONZEIT').'&BSA='.$rsPOKSuche->FeldInhalt('BSA'),'/bilder/icon_lupe.png','Details anzeigen',($DS%2));
			$Form->Erstelle_ListenFeld('FILIALE',$rsPOKSuche->FeldInhalt('FILIALE'),12,40*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONDATUM',$rsPOKSuche->FeldInhalt('BONDATUM'),12,70*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONZEIT',$rsPOKSuche->FeldInhalt('BONZEIT'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BSA',$rsPOKSuche->FeldInhalt('BSA'),12,30*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('KARTENNR',$rsPOKSuche->FeldInhalt('KARTENNR'),12,160*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('VERTRAGSNR',$rsPOKSuche->FeldInhalt('VERTRAGSNR'),12,80*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('BONWERT',$rsPOKSuche->FeldInhalt('BONWERT'),12,55*$style_mpl,false,($DS%2),$style,'','N2');
			$Form->Erstelle_ListenFeld('BONWERT',$rsPOKSuche->FeldInhalt('LIEFERRG'),12,80*$style_mpl,false,($DS%2),$style,'','Z');
			
			$Form->ZeileEnde();
			$rsPOKSuche->DSWeiter();
			$DS++;
		}	
	}
	else
	{
		$style='font-size:0.82em';
		$style_mpl=0.86;
		
		$DetailAnsicht = true;
		$Form->Formular_Start();

		$Form->ZeileStart();
		
			$Felder = array();
			$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./POS_COM_Main.php?cmdAktion=Details&FILIALE=".$rsPOKSuche->FeldInhalt('FILIALE')."&BONDATUM=".$rsPOKSuche->FeldInhalt('BONDATUM').(isset($_GET['Block'])?'&Block='.$_GET['Block']:'')."".(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'')."&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
			$Form->InfoZeile($Felder,'');
		
		$Form->ZeileEnde();
		
		$Form->ZeileStart($style);

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FILIALE'],40*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONDATUM'],70*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONZEIT'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BSA'],30*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['KARTENNR'],160*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['VERTRAGSNR'],80*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['KTYPNR'],70*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FAHRGESTELLNR'],170*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['KFZ'],80*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['KM'],50*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ERSTZULASSUNG'],90*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['AUFTRAGGEBER'],175*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['LEASINGNR'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FREIGABE'],55*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['LIEFERRG'],55*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONWERT'],55*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONTYP'],50*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['URSPRUNGSFILIALE'],100*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['URSPRUNGSLIEFERRG'],120*$style_mpl,$style,'');
		
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsPOKSuche->EOF())
		{
			$Form->ZeileStart($style);		

			$Form->Erstelle_ListenFeld('FILIALE',$rsPOKSuche->FeldInhalt('FILIALE'),12,40*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONDATUM',$rsPOKSuche->FeldInhalt('BONDATUM'),12,70*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONZEIT',$rsPOKSuche->FeldInhalt('BONZEIT'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BSA',$rsPOKSuche->FeldInhalt('BSA'),12,30*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('KARTENNR',$rsPOKSuche->FeldInhalt('KARTENNR'),12,160*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('VERTRAGSNR',$rsPOKSuche->FeldInhalt('VERTRAGSNR'),12,80*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('KTYPNR',$rsPOKSuche->FeldInhalt('KTYPNR'),12,70*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('FAHRGESTELLNR',$rsPOKSuche->FeldInhalt('FAHRGESTELLNR'),12,170*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('KFZ',$rsPOKSuche->FeldInhalt('KFZ'),12,80*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('KM',$rsPOKSuche->FeldInhalt('KM'),12,50*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('ERSTZULASSUNG',$rsPOKSuche->FeldInhalt('ERSTZULASSUNG'),12,90*$style_mpl,false,($DS%2),$style,'','D');
			$Form->Erstelle_ListenFeld('AUFTRAGGEBER',$rsPOKSuche->FeldInhalt('AUFTRAGGEBER'),12,175*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('LEASINGNR',$rsPOKSuche->FeldInhalt('LEASINGNR'),12,60*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('FREIGABE',$rsPOKSuche->FeldInhalt('FREIGABE'),12,55*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('LIEFERRG',$rsPOKSuche->FeldInhalt('LIEFERRG'),12,55*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('BONWERT',$rsPOKSuche->FeldInhalt('BONWERT'),12,55*$style_mpl,false,($DS%2),$style,'','N2');
			$Form->Erstelle_ListenFeld('BONTYP',$rsPOKSuche->FeldInhalt('BONTYP'),12,50*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('URSPRUNGSFILIALE',$rsPOKSuche->FeldInhalt('URSPRUNGSFILIALE'),12,100*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('URSPRUNGSLIEFERRG',$rsPOKSuche->FeldInhalt('URSPRUNGSLIEFERRG'),12,120*$style_mpl,false,($DS%2),$style,'','T');
					
			$Form->ZeileEnde();

			$rsPOKSuche->DSWeiter();
			$DS++;
		}
		
		$Form->Trennzeile('D');

		$ORDERBY = ' ORDER BY FILIALE, BONDATUM, BONZEIT, BSA, BONPOS, SUBBONPOS';
		
		$SQL= 'SELECT k.* ';
		$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
		if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
		{
			$SQL .= ' FROM EXPERIAN_ATU.POS_DETAIL@COM_DE.ATU.DE k';		
		}
		else 
		{
			$SQL .= ' FROM EXPERIAN_ATU.POS_DETAIL@COM_TEST_DE.ATU.DE k';
		}
	
		if($Bedingung!='')
		{
			$SQL .= ' WHERE ' . substr($Bedingung,4);
		}		
		
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
		$SQL .= $ORDERBY;
		
		$rsPODSuche = $DB->RecordsetOeffnen($SQL,$BindeVariablen);
		
		$Form->ZeileStart($style);

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FILIALE'],40*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONDATUM'],70*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONZEIT'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BSA'],30*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONPOS'],50*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['SUBBONPOS'],70*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['TRXKENN'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ARTIKELNR'],55*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ARTIKELBEZ'],400*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['UST'],30*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ME'],30*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['MENGE'],50*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BETRAG'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ARTIKELGESAMTPREIS'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONUS'],50*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ANZAHLNR'],100*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['WG'],50*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['SG'],50*$style_mpl,$style,'');
		//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['NETTOEK'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['KENN1'],40*$style_mpl,$style,'');
		
		$Form->ZeileEnde();
		
		$DS=0;
		while(!$rsPODSuche->EOF())
		{
			$Form->ZeileStart($style);		

			$Form->Erstelle_ListenFeld('FILIALE',$rsPODSuche->FeldInhalt('FILIALE'),12,40*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONDATUM',$rsPODSuche->FeldInhalt('BONDATUM'),12,70*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONZEIT',$rsPODSuche->FeldInhalt('BONZEIT'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BSA',$rsPODSuche->FeldInhalt('BSA'),12,30*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('BONPOS',$rsPODSuche->FeldInhalt('BONPOS'),12,50*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('SUBBONPOS',$rsPODSuche->FeldInhalt('SUBBONPOS'),12,70*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('TRXKENN',$rsPODSuche->FeldInhalt('TRXKENN'),12,60*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('ARTIKELNR',$rsPODSuche->FeldInhalt('ARTIKELNR'),12,55*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('ARTIKELBEZ',$rsPODSuche->FeldInhalt('ARTIKELBEZ'),12,400*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('UST',$rsPODSuche->FeldInhalt('UST'),12,30*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('ME',$rsPODSuche->FeldInhalt('ME'),12,30*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('MENGE',$rsPODSuche->FeldInhalt('MENGE'),12,50*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BETRAG',$rsPODSuche->FeldInhalt('BETRAG'),12,60*$style_mpl,false,($DS%2),$style,'','N2');
			$Form->Erstelle_ListenFeld('ARTIKELGESAMTPREIS',$rsPODSuche->FeldInhalt('ARTIKELGESAMTPREIS'),12,60*$style_mpl,false,($DS%2),$style,'','N2');
			$Form->Erstelle_ListenFeld('BONUS',$rsPODSuche->FeldInhalt('BONUS'),12,50*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('ANZAHLNR',$rsPODSuche->FeldInhalt('ANZAHLNR'),12,100*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('WG',$rsPODSuche->FeldInhalt('WG'),12,50*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('SG',$rsPODSuche->FeldInhalt('SG'),12,50*$style_mpl,false,($DS%2),$style,'','T');
			//$Form->Erstelle_ListenFeld('NETTOEK',$rsPODSuche->FeldInhalt('NETTOEK'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('KENN1',$rsPODSuche->FeldInhalt('KENN1'),12,40*$style_mpl,false,($DS%2),$style,'','T');
					
			$Form->ZeileEnde();

			$rsPODSuche->DSWeiter();
			$DS++;
		}
		
		$Form->Trennzeile('D');
		
		$ORDERBY = ' ORDER BY FILIALE, BONDATUM, BONZEIT, BSA, ZAHLUNGSART';
				
		$SQL= 'SELECT k.* ';
		$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
		if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
		{
			$SQL .= ' FROM EXPERIAN_ATU.POS_ZAHLUNGSARTEN@COM_DE.ATU.DE k';		
		}
		else 
		{
			$SQL .= ' FROM EXPERIAN_ATU.POS_ZAHLUNGSARTEN@COM_TEST_DE.ATU.DE k';
		}
	
		if($Bedingung!='')
		{
			$SQL .= ' WHERE ' . substr($Bedingung,4);
		}
				
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
		$SQL .= $ORDERBY;
		
		$rsPOZSuche = $DB->RecordsetOeffnen($SQL,$BindeVariablen);
		
		$Form->ZeileStart($style);

		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FILIALE'],40*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONDATUM'],70*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONZEIT'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BSA'],30*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['TRXKENN'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ZAHLUNGSART'],70*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['WAEHRUNG'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BETRAG'],60*$style_mpl,$style,'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ZUSATZINFOS'],300*$style_mpl,$style,'');
		
		$Form->ZeileEnde();
		
		$DS=0;
		while(!$rsPOZSuche->EOF())
		{
			$Form->ZeileStart($style);		

			$Form->Erstelle_ListenFeld('FILIALE',$rsPOZSuche->FeldInhalt('FILIALE'),12,40*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONDATUM',$rsPOZSuche->FeldInhalt('BONDATUM'),12,70*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BONZEIT',$rsPOZSuche->FeldInhalt('BONZEIT'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BSA',$rsPOZSuche->FeldInhalt('BSA'),12,30*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('TRXKENN',$rsPOZSuche->FeldInhalt('TRXKENN'),12,60*$style_mpl,false,($DS%2),$style,'','T');
			$Form->Erstelle_ListenFeld('ZAHLUNGSART',$rsPOZSuche->FeldInhalt('ZAHLUNGSART'),12,70*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('WAEHRUNG',$rsPOZSuche->FeldInhalt('WAEHRUNG'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
			$Form->Erstelle_ListenFeld('BETRAG',$rsPOZSuche->FeldInhalt('BETRAG'),12,60*$style_mpl,false,($DS%2),$style,'','N2');
			$Form->Erstelle_ListenFeld('ZUSATZINFOS',$rsPOZSuche->FeldInhalt('ZUSATZINFOS'),12,300*$style_mpl,false,($DS%2),$style,'','T');			
					
			$Form->ZeileEnde();

			$rsPOZSuche->DSWeiter();
			$DS++;
		}
		
		$Form->Trennzeile('D');
		
		$ORDERBY = ' ORDER BY FILIALE, BONDATUM, BONZEIT, BSA';
				
		$SQL= 'SELECT k.* ';
		$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
		if ($Werkzeuge->awisLevel() == awisWerkzeuge::AWIS_LEVEL_PRODUKTIV)
		{
			$SQL .= ' FROM EXPERIAN_ATU.POS_VALIDIERUNG_BON@COM_DE.ATU.DE k';		
		}
		else 
		{
			$SQL .= ' FROM EXPERIAN_ATU.POS_VALIDIERUNG_BON@COM_TEST_DE.ATU.DE k';
		}
	
		if($Bedingung!='')
		{
			$SQL .= ' WHERE ' . substr($Bedingung,4);
		}
				
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$BindeVariablen);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
		$SQL .= $ORDERBY;
		
		$rsPOVSuche = $DB->RecordsetOeffnen($SQL,$BindeVariablen);
		
		if($rsPOVSuche->AnzahlDatensaetze()>=1)
		{
			$Form->ZeileStart($style);
	
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FILIALE'],40*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONDATUM'],70*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONZEIT'],60*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BSA'],30*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['BONWERT'],55*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['RUECKGABEN'],70*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['GUTSCHEINKAUF'],85*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ZAMI_52'],55*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ZAMI_3'],55*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ZAMI_REST'],70*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['VERSICHERUNG'],80*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['GUTSCHEIN'],60*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['URSPRUNGSAUFTRAG'],100*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['URSPRUNGSFILIALE'],95*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ANZAHL_BONUSPUNKTE'],75*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['FLATRATE'],55*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['KENNUNG'],60*$style_mpl,$style,'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PosComSuche']['ZAMI_56'],55*$style_mpl,$style,'');
			
			$Form->ZeileEnde();

			$DS=0;
			while(!$rsPOVSuche->EOF())
			{
				$Form->ZeileStart($style);		
	
				$Form->Erstelle_ListenFeld('FILIALE',$rsPOVSuche->FeldInhalt('FILIALE'),12,40*$style_mpl,false,($DS%2),$style,'','Z');
				$Form->Erstelle_ListenFeld('BONDATUM',$rsPOVSuche->FeldInhalt('BONDATUM'),12,70*$style_mpl,false,($DS%2),$style,'','Z');
				$Form->Erstelle_ListenFeld('BONZEIT',$rsPOVSuche->FeldInhalt('BONZEIT'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
				$Form->Erstelle_ListenFeld('BSA',$rsPOVSuche->FeldInhalt('BSA'),12,30*$style_mpl,false,($DS%2),$style,'','T');
				$Form->Erstelle_ListenFeld('BONWERT',$rsPOVSuche->FeldInhalt('BONWERT'),12,55*$style_mpl,false,($DS%2),$style,'','N2');
				$Form->Erstelle_ListenFeld('RUECKGABEN',$rsPOVSuche->FeldInhalt('SUMME_RUECKGABEN'),12,70*$style_mpl,false,($DS%2),$style,'','N2');
				$Form->Erstelle_ListenFeld('GUTSCHEINKAUF',$rsPOVSuche->FeldInhalt('SUMME_GUTSCHEINKAUF'),12,85*$style_mpl,false,($DS%2),$style,'','N2');
				$Form->Erstelle_ListenFeld('ZAMI_52',$rsPOVSuche->FeldInhalt('SUMME_ZAMI_52'),12,55*$style_mpl,false,($DS%2),$style,'','N2');
				$Form->Erstelle_ListenFeld('ZAMI_3',$rsPOVSuche->FeldInhalt('SUMME_ZAMI_3'),12,55*$style_mpl,false,($DS%2),$style,'','N2');			
				$Form->Erstelle_ListenFeld('ZAMI_REST',$rsPOVSuche->FeldInhalt('SUMME_ZAMI_REST'),12,70*$style_mpl,false,($DS%2),$style,'','N2');
				$Form->Erstelle_ListenFeld('VERSICHERUNG',$rsPOVSuche->FeldInhalt('SUMME_VERSICHERUNG'),12,80*$style_mpl,false,($DS%2),$style,'','N2');
				$Form->Erstelle_ListenFeld('GUTSCHEIN',$rsPOVSuche->FeldInhalt('SUMME_GUTSCHEIN'),12,60*$style_mpl,false,($DS%2),$style,'','N2');			
				$Form->Erstelle_ListenFeld('URSPRUNGSAUFTRAG',$rsPOVSuche->FeldInhalt('URSPRUNGSAUFTRAG'),12,100*$style_mpl,false,($DS%2),$style,'','T');
				$Form->Erstelle_ListenFeld('URSPRUNGSFILIALE',$rsPOVSuche->FeldInhalt('URSPRUNGSFILIALE'),12,95*$style_mpl,false,($DS%2),$style,'','Z');
				$Form->Erstelle_ListenFeld('ANZAHL_BONUSPUNKTE',$rsPOVSuche->FeldInhalt('ANZAHL_BONUSPUNKTE'),12,75*$style_mpl,false,($DS%2),$style,'','Z');			
				$Form->Erstelle_ListenFeld('FLATRATE',$rsPOVSuche->FeldInhalt('SUMME_FLATRATE'),12,55*$style_mpl,false,($DS%2),$style,'','N2');
				$Form->Erstelle_ListenFeld('KENNUNG',$rsPOVSuche->FeldInhalt('KENNUNG'),12,60*$style_mpl,false,($DS%2),$style,'','Z');
				$Form->Erstelle_ListenFeld('ZAMI_56',$rsPOVSuche->FeldInhalt('SUMME_ZAMI_56'),12,55*$style_mpl,false,($DS%2),$style,'','N2');			
						
				$Form->ZeileEnde();
	
				$rsPOVSuche->DSWeiter();
				$DS++;
			}
		}
		
		$Link = './POS_COM_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071633");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201109071634");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $DB;
	global $BindeVariablen;

	$Bedingung = '';
	
	if(isset($Param['FILIALE']) AND $Param['FILIALE']!='')
	{
		$BindeVariablen['var_N0_fil_id']=$DB->FeldInhaltFormat('Z',$Param['FILIALE']);
		$Bedingung .= ' AND FILIALE=:var_N0_fil_id ';
	}
	
	if(isset($Param['BONDATUM']) AND $Param['BONDATUM']!='')
	{
		$BindeVariablen['var_N0_bondatum']=$DB->FeldInhaltFormat('Z',$Param['BONDATUM']);
		$Bedingung .= ' AND BONDATUM=:var_N0_bondatum ';
	}
	
	if(isset($Param['BONZEIT']) AND $Param['BONZEIT']!='')
	{
		$BindeVariablen['var_N0_bonzeit']=$DB->FeldInhaltFormat('Z',$Param['BONZEIT']);
		$Bedingung .= ' AND BONZEIT=:var_N0_bonzeit ';
	}
	
	if(isset($Param['BSA']) AND $Param['BSA']!='')
	{
		$BindeVariablen['var_T_bsa']=$Param['BSA'];
		$Bedingung .= ' AND BSA=:var_T_bsa ';
	}
	
	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>