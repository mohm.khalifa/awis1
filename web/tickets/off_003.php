<?php
/**
 * Formular: 03 - Umzug, Funktionsänderung
 * @author Sacha Kerres
 *
 */

$TextKonserven = array();
$TextKonserven[]=array('OTF_FRM_'.$FormularKennung,'TTT_*');
$TextKonserven[]=array('TTT_BEMERKUNGEN_ALLG','OTF');
$ToolTipps = $Form->LadeTexte($TextKonserven, $AWISBenutzer->BenutzerSprache());


//**************************************************************
// Anpassung des Labels für die Kostenstelle
// und Berücksichtigung der Eingabeformate
//**************************************************************
echo '<script type="text/javascript">'.PHP_EOL;

echo '$("#txtOTF_WERT_133").change(function(){'.PHP_EOL;
echo 'if($("#txtOTF_WERT_133").val()=="FILIALE")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $("#lblOTF_27").html("Filiale (4-stellig)<font color=#FF0000>*</font>:");'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $("#lblOTF_27").html("Kostenstelle<font color=#FF0000>*</font>:");'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

// Bei Kostenstellen 4-12 Zeichen
// bei Filiale auf 4 Stellen erweitern / schneiden
echo '$("#txtOTF_WERT_27").change(function(){'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    if($("#txtOTF_WERT_133").val()=="FILIALE")';
echo '    {'.PHP_EOL;
echo '            $("#txtOTF_WERT_27").val(String("0000"+$("#txtOTF_WERT_27").val()).slice(-4));';
echo '    }'.PHP_EOL;
echo '    else'.PHP_EOL;
echo '    {'.PHP_EOL;
echo '        if($("#txtOTF_WERT_27").val().length<4)'.PHP_EOL;
echo '        {'.PHP_EOL;
echo '            alert("Bitte mindestens 4 Zeichen angeben");'.PHP_EOL;
echo '            $("#txtOTF_WERT_27").focus();';
echo '        }'.PHP_EOL;
echo '        else if($("#txtOTF_WERT_27").val().length>12)'.PHP_EOL;
echo '        {'.PHP_EOL;
echo '            alert("Bitte maximal 12 Zeichen angeben");'.PHP_EOL;
echo '            $("#txtOTF_WERT_27").focus();';
echo '        }'.PHP_EOL;
echo '    }'.PHP_EOL;
echo '}'.PHP_EOL;
echo '});'.PHP_EOL;

echo '</script>'.PHP_EOL;



//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

$SQL = ' select ADE_MAIL, ADE_SURNAME || COALESCE(\', \'||ADE_GIVENNAME,\'\') AS KON_NAME from ACTIVEDIRECTORYEXPORT ';

$Form->ZeileStart();
$Form->Erstelle_TextLabel('Benachrichtigung an:', 299,'','','');
$Form->Erstelle_MehrfachSelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn',isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn'])?$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-BenachAn']:array(),'450:450',true,$SQL,'','','','','');
$Form->ZeileEnde();


//**************************************************
// Arbeitsplatzumzug
//**************************************************
$Block='40';
$Form->ZeileStart();
$Form->Erstelle_TextLabel('Arbeitsplatzumzug', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_UMZUG']);
$Form->Erstelle_SelectFeld('ArbeitsplatzumzugAuswahl',(isset($_POST['txtArbeitsplatzumzugAuswahl'])?$_POST['txtArbeitsplatzumzugAuswahl']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('PC:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_PC']);
$Form->ZeileEnde();
$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 80,'','','');
$Form->Erstelle_TextLabel('ATUDOM', 220,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_ATUDOM']);
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-PC-ATUDOM', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-ATUDOM'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-ATUDOM']:''), 100, true, '1');
$Form->ZeileEnde();
$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 80,'','','');
$Form->Erstelle_TextLabel('DSL', 220,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_DSL']);
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-PC-DSL', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-DSL'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-DSL']:''), 100, true, '1');
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('PC-Kennung:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_PCKENNUNG']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-PC-Kennung', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Kennung'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Kennung']:''), 30, 300, true);
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Peripherie:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_PERIPHERIE']);
$Form->ZeileEnde();
$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 80,'','','');
$Form->Erstelle_TextLabel('Drucker', 220,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_DRUCKER']);
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-PC-Drucker', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Drucker'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Drucker']:''), 100, true, '1');
$Form->ZeileEnde();
$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 80,'','','');
$Form->Erstelle_TextLabel('Scanner', 220,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_SCANNER']);
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-PC-Scanner', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Scanner'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Scanner']:''), 100, true, '1');
$Form->ZeileEnde();
$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 80,'','','');
$Form->Erstelle_TextLabel('Fax', 220,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_FAX']);
$Form->Erstelle_Checkbox('DETAILS_'.$FormularKennung.'_'.$Block.'-PC-Fax', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Fax'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Fax']:''), 100, true, '1');
$Form->ZeileEnde();


$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Sonstiges:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_UMZUG_SONST']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges']:''), 30, 300, true);
$Form->ZeileEnde();


echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtArbeitsplatzumzugAuswahl").change(function(){'.PHP_EOL;
echo 'if($("#txtArbeitsplatzumzugAuswahl").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Kennung").attr("required", true)'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-PC-Kennung").attr("required", false)'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtArbeitsplatzumzugAuswahl").change()});';

echo '</script>'.PHP_EOL;

//**************************************************
// Funktionswechsel
//**************************************************
$Block='45';
$Form->ZeileStart();
$Form->Erstelle_TextLabel('Funktionswechsel', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_FKTWECHSEL']);
$Form->Erstelle_SelectFeld('FunktionswechselAuswahl',(isset($_POST['txtFunktionswechselAuswahl'])?$_POST['txtFunktionswechselAuswahl']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Funktion alt:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_FKT_ALT']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Alt', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Alt'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Alt']:''), 30, 300, true);
$Form->Erstelle_TextLabel('Funktion neu:', 150,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_FKT_NEU']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Neu', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Neu'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Neu']:''), 30, 300, true);
$Form->ZeileEnde();


echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtFunktionswechselAuswahl").change(function(){'.PHP_EOL;
echo 'if($("#txtFunktionswechselAuswahl").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Alt").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Neu").attr("required", true)'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Alt").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Funktion-Neu").attr("required", false)'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtFunktionswechselAuswahl").change()});';

echo '</script>'.PHP_EOL;

//**************************************************
// Abteilungswechsel
//**************************************************
$Block='50';
$Form->ZeileStart();
$Form->Erstelle_TextLabel('Abteilungswechsel', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_ABTWECHSEL']);
$Form->Erstelle_SelectFeld('AbteilungswechselAuswahl',(isset($_POST['txtAbteilungswechselAuswahl'])?$_POST['txtAbteilungswechselAuswahl']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Abteilung alt:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_ABTWECHSEL_ABTALT']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Alt', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Alt'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Alt']:''), 30, 300, true);
$Form->Erstelle_TextLabel('Abteilung neu:', 150,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_ABTWECHSEL_ABTNEU']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Neu', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Neu'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Neu']:''), 30, 300, true);
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Kostenstelle alt:', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_ABTWECHSEL_KSTALT']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Alt', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Alt'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Alt']:''), 30, 300, true);
$Form->Erstelle_TextLabel('Kostenstelle neu:', 150,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_ABTWECHSEL_KSTNEU']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Neu', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Neu'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Neu']:''), 30, 300, true);
$Form->ZeileEnde();


echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtAbteilungswechselAuswahl").change(function(){'.PHP_EOL;
echo 'if($("#txtAbteilungswechselAuswahl").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Alt").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Neu").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Alt").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Neu").attr("required", true)'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Alt").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Abteilung-Neu").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Alt").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-Kostenstelle-Neu").attr("required", false)'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtAbteilungswechselAuswahl").change()});';

echo '</script>'.PHP_EOL;


//**************************************************
// Applikationen
//**************************************************
$Block='60';
$Form->ZeileStart('','',' BlockREFUser');
$Form->Erstelle_TextLabel('Applikationen', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_APPLIKATIONEN']);
$Form->Erstelle_SelectFeld('ApplikationenAuswahl',(isset($_POST['txtApplikationenAuswahl'])?$_POST['txtApplikationenAuswahl']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

for($i=1;$i<5;$i++)
{
    $Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
    $Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
    $Form->Erstelle_TextLabel('Applikation '.$i, 250,'','','');
    $Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Applikationen_'.$i, (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Applikationen_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Applikationen_'.$i]:''), 30, 300, true);
    $Form->ZeileEnde();
}
echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtApplikationenAuswahl").change(function(){'.PHP_EOL;
echo 'if($("#txtApplikationenAuswahl").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtApplikationenAuswahl").change()});';

echo '</script>'.PHP_EOL;


//**************************************************
// Laufwerke
//**************************************************
$Block = '70';
$Form->ZeileStart('','',' BlockREFUser');
$Form->Erstelle_TextLabel('Laufwerke', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_LAUFWERKE']);
$Form->Erstelle_SelectFeld('LaufwerkeAuswahl',(isset($_POST['txtLaufwerkeAuswahl'])?$_POST['txtLaufwerkeAuswahl']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
$Form->Erstelle_TextLabel('&nbsp;', 30,'','','');
$Form->Erstelle_TextLabel('Laufwerk T:', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_LW_T']);
$Form->ZeileEnde();

for($i=1;$i<5;$i++)
{
    $Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
    $Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
    $Form->Erstelle_TextLabel('Verzeichnis ', 250,'','','');
    $Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_T_'.$i, (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_T_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_T_'.$i]:''), 30, 300, true);
    $Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_TModus_'.$i,(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_TModus_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_TModus_'.$i]:''),'200:190',true,'','','RO','','',array('RO~Lesend','RW~Schreibend'));
    $Form->ZeileEnde();
}

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
$Form->Erstelle_TextLabel('&nbsp;', 30,'','','');
$Form->Erstelle_TextLabel('Laufwerk V:', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_LW_V']);
$Form->ZeileEnde();

for($i=1;$i<5;$i++)
{
    $Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
    $Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
    $Form->Erstelle_TextLabel('Verzeichnis ', 250,'','','');
    $Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_V_'.$i, (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_V_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_V_'.$i]:''), 30, 300, true);
    $Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_VModus_'.$i,(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_VModus_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_VModus_'.$i]:''),'200:190',true,'','','RO','','',array('RO~Lesend','RW~Schreibend'));
    $Form->ZeileEnde();
}

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
$Form->Erstelle_TextLabel('&nbsp;', 30,'','','');
$Form->Erstelle_TextLabel('Sonstige Laufwerke:', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_LW_SONST']);
$Form->ZeileEnde();

for($i=1;$i<5;$i++)
{
    $Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
    $Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
    $Form->Erstelle_TextLabel('Verzeichnis ', 250,'','','');
    $Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_Sonstige_'.$i, (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_Sonstige_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_Sonstige_'.$i]:''), 30, 300, true);
    $Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_SonstigeModus_'.$i,(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_SonstigeModus_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_SonstigeModus_'.$i]:''),'200:190',true,'','','RO','','',array('RO~Lesend','RW~Schreibend'));
    $Form->ZeileEnde();
}

echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtLaufwerkeAuswahl").change(function(){'.PHP_EOL;
echo 'if($("#txtLaufwerkeAuswahl").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtLaufwerkeAuswahl").change()});';

echo '</script>'.PHP_EOL;



//**************************************************
// Postfaecher
//**************************************************
$Block='80';
$Form->ZeileStart('','',' BlockREFUser');
$Form->Erstelle_TextLabel('Postf&auml;cher (au&szlig;er pers&ouml;nliches)', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_POSTFAECHER']);
$Form->Erstelle_SelectFeld('PostfaecherAuswahl',(isset($_POST['txtPostfaecherAuswahl'])?$_POST['txtPostfaecherAuswahl']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

for($i=1;$i<5;$i++)
{
    $Form->ZeileStart('visibility:hidden;','','Block'.$Block.'  BlockREFUser');
    $Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
    $Form->Erstelle_TextLabel('Postfaecher '.$i, 250,'','','');
    $Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Postfaecher_'.$i, (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Postfaecher_'.$i])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Postfaecher_'.$i]:''), 30, 300, true);
    $Form->ZeileEnde();
}
echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtPostfaecherAuswahl").change(function(){'.PHP_EOL;
echo 'if($("#txtPostfaecherAuswahl").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtPostfaecherAuswahl").change()});';

echo '</script>'.PHP_EOL;


//**************************************************
// VAX Account
//**************************************************
$Block='90';
$Form->ZeileStart();
$Form->Erstelle_TextLabel('VAX Account', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX']);
$Form->Erstelle_SelectFeld('VAXAuswahl',(isset($_POST['txtVAXAuswahl'])?$_POST['txtVAXAuswahl']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Referenzbenutzer', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX_REFERENZ']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-VAX-Referenzbenutzer', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-VAX-Referenzbenutzer'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-VAX-Referenzbenutzer']:''), 30, 300, true);
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('EK-Referenzbenutzer', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX_EKREFERENZ']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer']:''), 30, 300, true);
$Form->ZeileEnde();

echo '<script type="text/javascript">'.PHP_EOL;
echo '$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer").change(function(){'.PHP_EOL;
echo 'if($("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer").val()!="")'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer-genehmigt-von").attr("required", true)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer-genehmigt-von").addClass("InputTextPflicht")'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer-genehmigt-von").attr("required", false)'.PHP_EOL;
echo '    $("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer-genehmigt-von").removeClass("InputTextPflicht")'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer").trigger(\'change\')});';

echo '</script>'.PHP_EOL;

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('EK-Berechtigung genehmigt von', 250,'','','');
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer-genehmigt-von', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer-genehmigt-von'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-EK-Referenzbenutzer-genehmigt-von']:''), 30, 300, true);
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Faxberechtigung', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX_FAX']);
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Faxberechtigung',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Faxberechtigung'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Faxberechtigung']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Programmwahl 4701', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX_4701']);
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Programmwahl4701',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Programmwahl4701'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Programmwahl4701']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Bestellung ausl&ouml;sen', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX_BESTELLUNG']);
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Bestellungen',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bestellungen'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bestellungen']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Programmwahl JOU', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX_JOU']);
$Form->Erstelle_SelectFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-ProgrammwahlJOU',(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-ProgrammwahlJOU'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-ProgrammwahlJOU']:''),'100:90',true,'','',0,'','',array('1~Ja','0~Nein'));
$Form->ZeileEnde();

$Form->ZeileStart('visibility:hidden;','','Block'.$Block.'');
$Form->Erstelle_TextLabel('&nbsp;', 50,'','','');
$Form->Erstelle_TextLabel('Sonstiges', 250,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_VAX_SONST']);
$Form->Erstelle_TextFeld('DETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Sonstiges']:''), 30, 300, true);
$Form->ZeileEnde();



echo '<script type="text/javascript">'.PHP_EOL;

echo '$( document ).ready(function() {';
echo '$(".Block'.$Block.'").hide();';
echo '});';

echo '$("#txtVAXAuswahl").change(function(){'.PHP_EOL;
echo 'if($("#txtVAXAuswahl").val()==1)'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "visible"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").show();'.PHP_EOL;
echo '}else'.PHP_EOL;
echo '{'.PHP_EOL;
echo '    $(".Block'.$Block.'").css({"visibility": "hidden"});'.PHP_EOL;
echo '    $(".Block'.$Block.'").hide();'.PHP_EOL;
echo '}';
echo '});'.PHP_EOL;

echo '$( document ).ready(function() {$("#txtVAXAuswahl").change()});';

echo '</script>'.PHP_EOL;

//**************************************************
// Bemerkungen
//**************************************************
$Block='900';

$Form->ZeileStart();
$Form->Erstelle_TextLabel('Allgemeine Bemerkungen:', 300,'','',$ToolTipps['OTF_FRM_'.$FormularKennung]['TTT_BEMERKUNGEN_ALLG']);
$Form->Erstelle_Textarea('DETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung', (isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung'])?$_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']:''), 600, 60, 3, true);
$Form->ZeileEnde();
?>