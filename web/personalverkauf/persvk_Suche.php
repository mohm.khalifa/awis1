<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
//@ob_clean();

require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

// Variablen
global $con;
global $awisDBFehler;
global $awisRSZeilen;
global $AWISBenutzer;
global $Params;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>
<body>
<?php

$RechteStufe = awisBenutzerRecht($con, 1600);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Personalverkauf',$AWISBenutzer->BenutzerName(),'','','');
   die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

awis_Debug(1,$_REQUEST);

if(isset($_POST['cmdSuche_x'])|| isset($_POST['cmdAnzeigen_x']))
{	
	if(isset($_POST['cmdAnzeigen_x']))
	{
		$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
	}
	else 
	{
		if(empty($_REQUEST['txtSuche_DatumVom']) && empty($_REQUEST['txtSuche_DatumBis']))
		{
			$Params[0]=0;
		}
		else
		{
			$Params[0]=1;
		}
		$Params[1]=$_REQUEST['txtSuche_PersNr'];
		$Params[2]=$_REQUEST['txtSuche_Mitarbeiter'];
		$Params[3]=$_REQUEST['txtSuche_Filiale'];
		$Params[4]=awis_format($_REQUEST['txtSuche_DatumVom'],'Datum');
		$Params[5]=awis_format($_REQUEST['txtSuche_DatumBis'],'Datum');
		$Params[6]="";
		$Params[7]="";
		$Params[8]="";
		$Params[9]=$_REQUEST['txtSuche_Artbez'];
		$Params[10]="";//$_REQUEST['txtSuche_Betrag'];
		$Params[11]=$_REQUEST['txtSuche_ATUNR'];
		$Params[12]="";
			
		awis_BenutzerParameterSpeichern($con, "Persvk_Suche", $AWISBenutzer->BenutzerName(), implode(';',$Params));
	}
	
	$Zusatz=FALSE;
	$Such2=FALSE;
	
	if($Params[2]=='')
	{
		$Such2=TRUE;
	}
	
	if($Such2)
	{
		$SQL="SELECT personalverkaeufe.pev_personalnummer, personalverkaeufe.pev_kaeufername, ";		  
	}
	else
	{
		$SQL="SELECT personalverkaeufe.pev_fil_id, personalverkaeufe.pev_personalnummer, personalverkaeufe.pev_kaeufername, ";
	}
	
	$SQL.="sum(personalverkaeufe.pev_menge) AS menge, sum((personalverkaeufe.pev_vk*personalverkaeufe.pev_menge)) as vk, ";
	$SQL.="sum(personalverkaeufe.pev_persrabatt) as rabatt, ";
	$SQL.="sum(nvl(personalverkaeufe.pev_manrabatt,0)) AS manrabatt, ";
	$SQL.= "sum(nvl(personalverkaeufe.pev_persrabatt,0)+nvl(personalverkaeufe.pev_manrabatt,0)) as rabattgesamt ";
	$SQL.="FROM personalverkaeufe ";
	
	$Bedingung='';
	
	if($Params[1]!='')
	{
		$Bedingung.="WHERE (personalverkaeufe.pev_personalnummer ".awisLIKEoderIST($Params[1],0,0,0,0).")";
		$Zusatz=TRUE;	
	}
	
	If($Params[2]!='')
	{	
		if($Zusatz)
		{
			$Bedingung.=" AND ( upper(personalverkaeufe.pev_kaeufername) ". awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0).")";
		}
		else
		{
			$Bedingung.="WHERE ( upper(personalverkaeufe.pev_kaeufername) ". awisLIKEoderIST($Params[2],TRUE,FALSE,FALSE,0).")";
			$Zusatz=TRUE;
		}
	}
	
	If($Params[3]!='')
	{	
		if($Zusatz)
		{
			$Bedingung.=" AND (personalverkaeufe.pev_fil_id=". $Params[3] .")";
		}
		else
		{
			$Bedingung.="WHERE (personalverkaeufe.pev_fil_id=". $Params[3] .")";
			$Zusatz=TRUE;
		}
	}
	
	If($Params[4]!='')
	{	
		If($Params[5]=='')
		{
			if($Zusatz)
			{
				$Bedingung.=" AND (personalverkaeufe.pev_kaufdatum=to_date('". $Params[4] ."','DD.MM.RR'))";
			}
			else
			{
				$Bedingung.="WHERE (personalverkaeufe.pev_kaufdatum=to_date('". $Params[4] ."','DD.MM.RR'))";
				$Zusatz=TRUE;
			}
		}
		else
		{
			if($Zusatz)
			{
				$Bedingung.=" AND (personalverkaeufe.pev_kaufdatum>=to_date('". $Params[4] ."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('". $Params[5] ."','DD.MM.RR'))";
			}
			else
			{
				$Bedingung.="WHERE (personalverkaeufe.pev_kaufdatum>=to_date('". $Params[4] ."','DD.MM.RR')) AND (personalverkaeufe.pev_kaufdatum<=to_date('". $Params[5] ."','DD.MM.RR'))";
				$Zusatz=TRUE;
			}
		}
	}
	
	//Eingefügt TR 11.06.07
	If($Params[9]!='')
	{
	   	if(substr($Params[9],-1,1)!='*' AND substr($Params[9],-1,1)!='%')
    	{
    		$Params[9] .='%';
    	}
    	
    	if(substr($Params[9],1,1)!='*' AND substr($Params[9],1,1)!='%')
    	{
    		$Params[9] = '%'.$Params[9];
    	}

		
		
		if($Zusatz)
		{
			$Bedingung.="AND ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0).")"; 
		}
		else
		{
			$Bedingung.="WHERE ( upper(personalverkaeufe.pev_artikelbezeichnung) ". awisLIKEoderIST($Params[9],TRUE,FALSE,FALSE,0).")";
			$Zusatz=TRUE;
		}
	}
	
	If($Params[11]!='')
	{	
		if($Zusatz)
		{
			$Bedingung.=" AND (personalverkaeufe.pev_ast_atunr ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0) .")";
		}
		else
		{
			$Bedingung.="WHERE (personalverkaeufe.pev_ast_atunr ". awisLIKEoderIST($Params[11],TRUE,FALSE,FALSE,0) .")";
			$Zusatz=TRUE;
		}
	}
	
	if($Bedingung=='' AND $Params[10]=='')
	{
		die("<span class=HinweisText>Kein Suchkriterium angegeben!</span>");
	}
	else
	{
		$SQL=$SQL." ".$Bedingung;
	}
	
	If($Such2)
	{
		If ($Params[10]!='')
		{
			$SQL.=" GROUP BY personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername having sum(pev_persrabatt) >". $Params[10] ;//to_char(pev_kaufdatum, 'YYYY') having sum(pev_persrabatt) >". $Params[10] ;
			$SQL.=" ORDER BY rabatt DESC";
		}
		else
		{
			$SQL.=" GROUP BY personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername ";
			$SQL.=" ORDER BY personalverkaeufe.pev_personalnummer ASC";	
		}
	}
	else
	{
		If ($Params[10]!='')
		{
			$SQL.=" GROUP BY personalverkaeufe.pev_fil_id , personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername having sum(pev_persrabatt) >". $Params[10] ;// to_char(pev_kaufdatum, 'YYYY') having sum(pev_persrabatt) >". $Params[10] ;
			$SQL.=" ORDER BY rabatt DESC";
		}
		else
		{
			$SQL.=" GROUP BY personalverkaeufe.pev_fil_id , personalverkaeufe.pev_personalnummer , personalverkaeufe.pev_kaeufername ";		
			$SQL.=" ORDER BY personalverkaeufe.pev_fil_id ASC, personalverkaeufe.pev_personalnummer ASC";
		}
	}
	
	awis_Debug(1,$SQL);
	
	$AwisBenuPara=awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());
				
	$rsPersvksuche = awisOpenRecordset($con,$SQL);
	$rsPersvksucheZeilen = $awisRSZeilen;
	
	// Überschrift aufbauen
	echo "<table  width=100% id=DatenTabelle border=1><tr>";					
	echo "<td id=FeldBez></td>";
	if(!$Such2)
	{
		echo "<td id=FeldBez>Filial-Nummer</td>";
	}
	echo "<td id=FeldBez>Personal-Nummer</td>";
	echo "<td id=FeldBez>Personal-K&auml;ufername</td>";
	echo "<td id=FeldBez>Menge</td>";
	echo "<td id=FeldBez>VK</td>";
	echo "<td id=FeldBez>Rabatt</td>";
	echo "<td id=FeldBez>Nachlass</td>";
	echo "<td id=FeldBez>Rabatt-Gesamt</td>";	echo '</tr>';
		
	for($i=0;$i<$rsPersvksucheZeilen;$i++)
	{	
		//if($i<$AwisBenuPara)
		//{
			echo '<tr>';
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=./persvk_Main.php?cmdAktion=Detail&txtPERNR=' . $rsPersvksuche['PEV_PERSONALNUMMER'][$i] . '><img border=0 src=/bilder/info.png name=cmdAnzeigen title="Anzeigen"></a></td>';
			if(!$Such2)
			{
				echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_FIL_ID'][$i] . '</td>';
			}
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['PEV_PERSONALNUMMER'][$i] . '</td>';
			echo '<td ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . (isset($rsPersvksuche['PEV_KAEUFERNAME'][$i])?$rsPersvksuche['PEV_KAEUFERNAME'][$i]:'') . '</td>';
			echo '<td align=right ' . (($i%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsPersvksuche['MENGE'][$i] . '</td>';
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["VK"][$i],'Currency') . "</td>";
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["RABATT"][$i],'Currency') . "</td>";
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["MANRABATT"][$i],'Currency') . "</td>";
			echo "<td align=right " . (($i%2)==0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") .">" . awis_format($rsPersvksuche["RABATTGESAMT"][$i],'Currency') . "</td>";
			echo '</tr>';
		//}
	}
	print "</table>";
	
	//if($i<$AwisBenuPara) // Ausgabe der Laufzeit auf das Hauptformular
	//{
		echo '<br><font size=2>Es wurden ' . $i . ' Datensätze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden!';
	//}
	//else
	//{
	//	echo '<br><font size=2>Es wurden ' . $i . ' Datensätze in ' . sprintf ("%.5f", awis_ZeitMessung(1)) . ' Sekunden gefunden! Die Anzahl wurden auf ' . $AwisBenuPara . ' Datensätze eingeschränkt!';
	//}
	
	echo "<hr>&nbsp;<img tabindex=98 src=/bilder/sc_suche2.png title='Neue Suche' name=cmdNeueSuche onclick=location.href='./persvk_Main.php';>";
	
}
else
{
	
if(!isset($_REQUEST['Reset']))
{
	$Params=explode(';',awis_BenutzerParameter($con, "Persvk_Suche", $AWISBenutzer->BenutzerName()));
}

echo "<form name=frmSuche method=post action=./persvk_Main.php>";

echo "<table  width=100% id=SuchMaske border=1><tr>";
	echo "<td id=FeldBez width=150>Personal-Nummer</td>";
	echo "<td id=TabellenZeileGrau><input name=txtSuche_PersNr size=25 value=".$Params[1]."></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>Mitarbeiter</td>";
	echo "<td id=TabellenZeileGrau><input name=txtSuche_Mitarbeiter size=50 value=".$Params[2]."></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>Filiale</td>";
	echo "<td id=TabellenZeileGrau><input name=txtSuche_Filiale size=50 value=".$Params[3]."></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>Datum vom</td><td><input name=txtSuche_DatumVom size=8 value=".$Params[4].">&nbsp;bis&nbsp;<input name=txtSuche_DatumBis size=8 value=".$Params[5]."></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>ATUNR</td><td><input name=txtSuche_ATUNR size=50 value=\"".$Params[11]."\"></td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td id=FeldBez width=150>Artikelbezeichnung</td><td><input name=txtSuche_Artbez size=50 value=\"".$Params[9]."\"></td>";
	echo "</tr>";
	//echo "<tr>";
	//echo "<td id=FeldBez width=150>Betrag größer als</td><td><input name=txtSuche_Betrag size=50 value=".$Params[10]."></td>";
	//echo "</tr>";
echo "</table>";

echo "<br>&nbsp;<input tabindex=95 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=98 src=/bilder/radierer.png title='Formularinhalt löschen' name=cmdReset onclick=location.href='./persvk_Main.php?Reset=True';>";
//print "<input type=hidden name=cmdAktion value=Suche>";

echo '</form>';
}

echo "</body>";
echo "</html>";
?>