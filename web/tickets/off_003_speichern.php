<?php
/**
 * Formular: 03 - Umzug, Funktionsänderung
 * @author Sacha Kerres
 *
 */
$FormularKennung = '003';
$DetailBlock = '';
$DetailBlock110 = '';

$DetailFelder = $Form->NameInArray($_POST, 'txtDETAILS', 2, 0);
$Form->DebugAusgabe(3,$DetailFelder);
$OT_EOL = "\r\n";

//****************************
// Arbeitsplatzumzug
//****************************
$Block='40';
$Daten='';

if(isset($_POST['txtArbeitsplatzumzugAuswahl']) AND $_POST['txtArbeitsplatzumzugAuswahl']==1)
{
    $DetailBlock .= $OT_EOL.'Arbeitsplatzumzug:'.$OT_EOL;

    $DetailBlock1 = '';
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PC-DSL']))
    {
        $DetailBlock1 .= '     DSL: ja'.$OT_EOL;
    }
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PC-ATUDOM']))
    {
        $DetailBlock1 .= '     ATUDOM: ja'.$OT_EOL;
    }
    if($DetailBlock1!='')
    {
        $DetailBlock .= '  PC:'.$OT_EOL.$DetailBlock1;
    }

    $DetailBlock .= '  PC-Kennung: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PC-Kennung'].$OT_EOL;
    
    $DetailBlock1 = '';
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PC-Drucker']))
    {
        $DetailBlock1 .= '     Drucker: ja'.$OT_EOL;
    }
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PC-Scanner']))
    {
        $DetailBlock1 .= '     Scanner: ja'.$OT_EOL;
    }
    if(isset($_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-PC-Fax']))
    {
        $DetailBlock1 .= '     Fax: ja'.$OT_EOL;
    }
    if($DetailBlock1!='')
    {
        $DetailBlock .= '  Peripherie: '.$OT_EOL.$DetailBlock1;
    }
    
    
    $DetailBlock .= '  Sonstiges: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Sonstiges'].$OT_EOL;
    
}


//****************************
// Funktionswechsel
//****************************
$Block='45';
$Daten='';

if(isset($_POST['txtFunktionswechselAuswahl']) AND $_POST['txtFunktionswechselAuswahl']==1)
{
    $DetailBlock .= $OT_EOL.'Funktionswechsel:'.$OT_EOL;
    
    $DetailBlock .= '  Funktion alt: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Funktion-Alt'].$OT_EOL;
    $DetailBlock .= '  Funktion neu: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Funktion-Neu'].$OT_EOL;
    
}

//****************************
// Abteilungswechsel
//****************************

$Block='50';
$Daten='';

if(isset($_POST['txtAbteilungswechselAuswahl']) AND $_POST['txtAbteilungswechselAuswahl']==1)
{
    $DetailBlock .= $OT_EOL.'Abteilungswechsel:'.$OT_EOL;
    
    $DetailBlock .= '  Abteilung alt: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Abteilung-Alt'].$OT_EOL;
    $DetailBlock .= '  Kostenstelle alt: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Kostenstelle-Alt'].$OT_EOL;
    $DetailBlock .= '  Abteilung neu: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Abteilung-Neu'].$OT_EOL;
    $DetailBlock .= '  Kostenstelle neu: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Kostenstelle-Neu'].$OT_EOL;
}

if($Daten!='')
{
    $DetailBlock .= $OT_EOL.'Abteilungswechsel:'.$OT_EOL;
    $DetailBlock .= $Daten;
}


//****************************
// Referenzbenutzer
//****************************

$Block='55';
$Daten='';

if(isset($_POST['txtReferenzUserAuswahl']) AND $_POST['txtReferenzUserAuswahl']==1)
{
    $DetailBlock .= $OT_EOL.'Referenzbenutzer:'.$OT_EOL;
    
    $DetailBlock .= '  Benutzer: '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Referenzbenutzer'].$OT_EOL;
}



//****************************
// Applikationen
//****************************

$Block='60';
$Applikationen='';
if(isset($_POST['txtApplikationenAuswahl']) AND $_POST['txtApplikationenAuswahl']==1)
{
    for($i=1;$i<=99;$i++)
    {
        if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Applikationen_'.$i]) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Applikationen_'.$i]!='')
        {
            $Applikationen .= '  '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Applikationen_'.$i].$OT_EOL;
        }
    }
    
    if($Applikationen!='')
    {
        $DetailBlock .= $OT_EOL.'Applikationen: '.$OT_EOL;
        $DetailBlock .= $Applikationen;
    }
}
    
//***************************
// Laufwerke
//***************************
$Block='70';
$Laufwerke=array();

if(isset($_POST['txtLaufwerkeAuswahl']) AND $_POST['txtLaufwerkeAuswahl']==1)
{
    $LaufWerksFelder = $Form->NameInArray($_POST, 'txtDETAILS_'.$FormularKennung.'_'.$Block.'-Laufwerke_',2,0);
    
    foreach($LaufWerksFelder AS $Feld)
    {
        if($_POST[$Feld]!='' AND strpos($Feld,'Modus')===false)
        {
            $FeldTeile = explode('_',$Feld);

            switch($_POST[str_replace('_'.$FeldTeile[3].'_', '_'.$FeldTeile[3].'Modus_', $Feld)])
            {
                case 'RW':
                    $Anzeige = 'schreibend';
                    break;
                case 'RO':
                    $Anzeige = 'lesend';
                    break;
                default:
                    $Anzeige = '::unbekannt::';
                    break;
            }
            $Laufwerke[$FeldTeile[3]][] = '  '.$_POST[$Feld].', '.$Anzeige;
        }
    }
    
    if(!empty($Laufwerke))
    {
        $DetailBlock .= $OT_EOL.'Laufwerke: '.$OT_EOL;
        foreach($Laufwerke AS $Laufwerk=>$Verzeichnisse)
        {
            $DetailBlock .= '   '.$Laufwerk.':'.$OT_EOL;
            foreach($Verzeichnisse AS $Verzeichnis)
            {
                $DetailBlock .= '      '.$Verzeichnis.$OT_EOL;
            }
        }
    }
}

//****************************
// Postfächer
//****************************
$Block='80';
$Postfaecher='';

if(isset($_POST['txtPostfaecherAuswahl']) AND $_POST['txtPostfaecherAuswahl']==1)
{
    for($i=1;$i<=99;$i++)
    {
        if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Postfaecher_'.$i]) AND $_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Postfaecher_'.$i]!='')
        {
            $Postfaecher .= '  '.$_POST['txtDETAILS_'.$FormularKennung .'_'.$Block.'-Postfaecher_'.$i].$OT_EOL;
        }
    }
    
    if($Postfaecher!='')
    {
        $DetailBlock .= $OT_EOL.'Postfaecher:'.$OT_EOL;
        $DetailBlock .= $Postfaecher;
    }
}

//****************************
// VAX User
//****************************
$Block='90';
$VAX ='';

if(isset($_POST['txtVAXAuswahl']) AND $_POST['txtVAXAuswahl']==1)
{
    $VAXFelder = $Form->NameInArray($_POST, 'txtDETAILS_'.$FormularKennung.'_'.$Block,2,0);
    
    foreach($VAXFelder AS $Feld)
    {
        if($_POST[$Feld]!='')
        {
            $FeldTeile = explode('_',$Feld);
            switch(substr($FeldTeile[2],3))
            {
                case 'Faxberechtigung':
                case 'Programmwahl4701':
                case 'Bestellungen':
                case 'ProgrammwahlJOU':
                    if($_POST[$Feld]==1)
                    {
                        $VAX .= '  '.substr($FeldTeile[2],3).': Ja'.$OT_EOL;
                    }
                    break;
                default:
                    $VAX .= '  '.substr($FeldTeile[2],3).': '.$_POST[$Feld].$OT_EOL;
            }
        }
    }
    
    if($VAX!='')
    {
        $DetailBlock .= $OT_EOL.'VAX Benutzer:'.$OT_EOL;
        $DetailBlock .= $VAX;
    }
}

//*********************************************************
//* Bemerkung
//*********************************************************
$Block='900';
if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']!='')
{
    $DetailBlock .= $OT_EOL.'Allgemeine Hinweise:'.$OT_EOL;
    $DetailBlock .= $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-Bemerkung']. $OT_EOL;
}


//**************************************************
// Benachrichtigung am
// ST 12.01.2017
//**************************************************

$Block='110';

if(isset($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']) AND $_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn']!='') {

    $Namelist = '';
    foreach($_POST['txtDETAILS_'.$FormularKennung.'_'.$Block.'-BenachAn'] as $KEY)
    {
        $Namelist .= ';'.$KEY;
    }
    $Namelist = substr($Namelist,1);
    $DetailBlock110 .= $Namelist;

    $DetailBlock .= $OT_EOL.'Benachrichtigung an:'.$OT_EOL;
    $DetailBlock .= $Namelist. $OT_EOL;
}


?>