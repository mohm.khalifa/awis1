<?php
require_once 'awisDatenbank.inc';
require_once 'awisBenutzer.inc';

$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

const TRENNER = ';';
const LINEFEED = "\n";

$SQL = 'Select * from v_filialen_aktuell order by fil_id asc';

$rsFil = $DB->RecordSetOeffnen($SQL);

$ExportDateiName = base64_encode($AWISBenutzer->BenutzerName() . date('c'));
$Datei = fopen('/daten/web/export/'.$ExportDateiName,'w');
$Zeile = 'FIL_ID' . TRENNER;
$Zeile .= 'FIL_BEZ' . TRENNER;
for($i=1;$i<=$_GET['AnzahlNachbarn'];$i++){
    $Zeile .= 'Nachbar_FIL_ID_'.$i . TRENNER;
}
$Zeile .= LINEFEED;
fwrite($Datei,$Zeile);
while(!$rsFil->EOF()){
    $Zeile = '';
    $SQL = 'SELECT DATEN.* ';
    $SQL .= ',(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 34 AND FIF_FIL_ID=FEG_FIL_ID AND ROWNUM = 1) AS EROEFFNUNG';
    $SQL .= ',(SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 104 AND FIF_FIL_ID=FEG_FIL_ID AND ROWNUM = 1) AS UMZUG';
    $SQL .=' FROM(';
    $SQL .= 'SELECT CASE WHEN FEG_FIL_ID_VON = ';
    $SQL .= $DB->WertSetzen('FIL','N0',$rsFil->FeldInhalt('FIL_ID'));
    $SQL .= ' THEN FEG_FIL_ID_NACH';
    $SQL .= ' ELSE FEG_FIL_ID_VON END AS FEG_FIL_ID, FEG_ENTFERNUNG';
    $SQL .= ',FIL_BEZ,FIL_STRASSE,FIL_PLZ,FIL_ORT,FIL_LAGERKZ, FIL_ID';
    $SQL .= ' FROM FILIALENENTFERNUNGEN';
    $SQL .= ' INNER JOIN Filialen ON CASE WHEN FEG_FIL_ID_VON = '.$DB->WertSetzen('FIL','N0',$rsFil->FeldInhalt('FIL_ID')).' THEN FEG_FIL_ID_NACH ELSE FEG_FIL_ID_VON END = FIL_ID AND FIL_ID < 900';
    $SQL .= ' WHERE FEG_FIL_ID_VON = ' .$DB->WertSetzen('FIL','N0',$rsFil->FeldInhalt('FIL_ID'));
    $SQL .= ' OR FEG_FIL_ID_NACH = '.$DB->WertSetzen('FIL','N0',$rsFil->FeldInhalt('FIL_ID'));
    $SQL .= ' ORDER BY FEG_ENTFERNUNG ASC';
    $SQL .= ') DATEN ';
    $SQL .= ' WHERE ROWNUM <= '. $DB->WertSetzen('FIL','N0',$_GET['AnzahlNachbarn']);
    $SQL .= ' AND (SELECT FIF_WERT FROM FilialInfos WHERE FIF_FIT_ID = 34 AND FIF_FIL_ID=FEG_FIL_ID AND FIF_IMQ_ID = 32 AND ROWNUM = 1) IS NOT NULL';
    $SQL .= ' ORDER BY FEG_FIL_ID asc';


    $rsNachbarn = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('FIL'));

    $Zeile .= $rsFil->FeldInhalt('FIL_ID') . TRENNER;
    $Zeile .= $rsFil->FeldInhalt('FIL_BEZ') . TRENNER;

    while(!$rsNachbarn->EOF()){

        $Zeile .= $rsNachbarn->FeldInhalt('FEG_FIL_ID') . TRENNER;

        $rsNachbarn->DSWeiter();
    }

    $Zeile .= LINEFEED;
    fwrite($Datei,$Zeile);
    $rsFil->DSWeiter();
}

fclose($Datei);

header('Pragma: public');
header('Cache-Control: max-age=0');
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename="nachbarn.csv"');
readfile('/daten/web/export/'.$ExportDateiName);
