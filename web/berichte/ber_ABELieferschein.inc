<?php

require_once 'awisBerichte.inc';

class ber_ABELieferschein
    extends awisBerichte
{
    public function init(array $Parameter)
    {
        $ABL_KEYs = implode(', ', $_POST['txtDruckKey']);
        $SQL = 'SELECT FIL_ID, FIL_BEZ, FIL_STRASSE, FIL_PLZ, FIL_ORT, ABL_KEY, ABL_NACH, ABF_FAHRGESTELLNR, ABF_ATU_NR, AST_BEZEICHNUNGWW, AST_BEZEICHNUNG FROM ABELIEFERSCHEINE';
        $SQL .= ' LEFT JOIN FILIALEN ON ABL_NACH = FIL_ID ';
        $SQL .= ' LEFT JOIN ABEFAHRZEUGE ON ABF_KEY = ABL_ABF_KEY';
        $SQL .= ' LEFT JOIN ARTIKELSTAMM ON ABF_ATU_NR = AST_ATUNR';
        $SQL .= ' WHERE ABL_KEY in (' . $ABL_KEYs . ')';
        $SQL .= ' ORDER BY FIL_ID';

        $this->_rec = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('ABF', true));

        $this->_DB->TransaktionBegin();

        if ($this->_rec->AnzahlDatensaetze() > 0) {
            if (file_exists($this->_BerichtsVorlage)) {
                $this->setSourceFile($this->_BerichtsVorlage);
            } else {
                $this->_BerichtsVorlage = '';
            }
        }
    }

    public function ErzeugePDF($Ausgabeart = 1)
    {
        global $ABE;
        $this->SetTitle('Lieferschein');
        $this->SetSubject('');
        $this->SetCreator('AWIS');
        $this->SetAuthor($ABE->AWISBenutzer->BenutzerName());
        $this->SetMargins(0, 0, 0);
        $this->AliasNbPages();
        $this->SetAutoPageBreak(false);
        $pdf_vorlage = $this->ImportPage(1);        // Erste Seite aus der Vorlage importieren

        while (!$this->_rec->EOF()) {
            $this->AddPage();
            $this->useTemplate($pdf_vorlage);

            //Lieferscheinnummer & Datum
            $this->_Zeile = $this->_Parameter['SeiteOben'];
            $this->_Spalte = $this->_Parameter['LinkerRand'];
            $this->SetFont($this->_Parameter['Schriftart'], '', $this->_Parameter['TextGroesse']);
            $this->SetXY($this->_Spalte + $this->_Parameter['AbstandLieferschein'], $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandLieferschein'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('ABL_KEY', 'T'), 0, 0, 'L');
            $this->SetXY($this->_Spalte  + $this->_Parameter['AbstandDatum'], $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandDatum'], $this->_Parameter['ZeilenHoehe'], date('d.m.Y'), 0, 0, 'C');

            //Filialnummer
            $this->_Zeile = $this->_Parameter['SeiteOben'] + $this->_Parameter['AbstandObenAdresse'];
            $this->SetXY($this->_Spalte + $this->_Parameter['AbstandFiliale'], $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandFiliale'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('FIL_ID', 'T'), 0, 0, 'L');

            //Straße
            $this->_Zeile += $this->_Parameter['ZeilenAbstand'];
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('FIL_STRASSE', 'T'), 0, 0, 'L');

            //PLZ & Ort
            $this->_Zeile += $this->_Parameter['ZeilenAbstand'];
            $this->SetFont($this->_Parameter['Schriftart'], 'B', $this->_Parameter['TextGroesseGross']);
            $this->SetXY($this->_Spalte, $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'] + 10, $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('FIL_PLZ', 'T'), 0, 0, 'L');
            $this->Cell($this->_Parameter['LinkerRand'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('FIL_ORT', 'T'), 0, 0, 'L');

            //ATUNR
            $this->_Zeile = $this->_Parameter['SeiteOben'] + $this->_Parameter['AbstandObenFahrzeug'];
            $this->SetXY($this->_Spalte + $this->_Parameter['AbstandATUNR'], $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandATUNR'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('ABF_ATU_NR', 'T'), 0, 0, 'L');

            //Bezeichnung
            $this->SetXY($this->_Spalte + $this->_Parameter['AbstandFahrzeug'], $this->_Zeile);
            $Inhalt = ($this->_rec->FeldInhalt('AST_BEZEICHNUNGWW', 'T') != ''?$this->_rec->FeldInhalt('AST_BEZEICHNUNGWW', 'T'):$this->_rec->FeldInhalt('AST_BEZEICHNUNG', 'T'));
            $this->Cell($this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandFahrzeug'], $this->_Parameter['ZeilenHoehe'], $Inhalt, 0, 0, 'L');

            //Fahrgestellnummer
            $this->_Zeile += $this->_Parameter['ZeilenAbstand'] + 1.5;
            $this->SetXY($this->_Spalte + $this->_Parameter['AbstandFahrgestellnummer'], $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandFahrgestellnummer'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('ABF_FAHRGESTELLNR', 'T'), 0, 0, 'L');

            //Filialnummer
            $this->_Zeile += $this->_Parameter['ZeilenAbstand']-1;
            $this->SetXY($this->_Spalte + $this->_Parameter['AbstandFahrgestellnummer'], $this->_Zeile);
            $this->Cell($this->_Parameter['LinkerRand'] + $this->_Parameter['AbstandFahrgestellnummer'], $this->_Parameter['ZeilenHoehe'], $this->_rec->FeldInhalt('FIL_ID', 'T'), 0, 0, 'L');

            //Gedruckten Datensatz festschreiben
            $SQL = 'UPDATE ABELIEFERSCHEINE SET ABL_WARTEND = 0 WHERE ABL_KEY = ' . $this->_DB->WertSetzen('ABL', 'Z', $this->_rec->FeldInhalt('ABL_KEY'));
            $this->_DB->Ausfuehren($SQL, 'ABL', true, $this->_DB->Bindevariablen('ABL', true));
            $this->_rec->DSWeiter();
        }

        //*********************************************************************
        // Berichtsende -> Zurückliefern
        //*********************************************************************
        $this->_DB->TransaktionCommit();
        switch ($Ausgabeart) {
            case 1:            // PDF im extra Fenster -> Download
                $this->Output($this->_BerichtsName . '.pdf', 'D');
                break;
            case 2:            // Standard-Stream
                $this->Output($this->_BerichtsName . '.pdf', 'I');
                break;
            case 3:            // Als String zurückliefern
                return ($this->Output($this->_BerichtsName . '.pdf', 'S'));
                break;
            case 4:                // Als Datei speichern
                $this->Output($this->_Dateiname, 'F');
                break;
        }
    }

    private function NeueSeite()
    {
        $this->AddPage('L');
    }
}