<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>

<title>Telefonauskunft - Hilfe</title>
</head>
<body>

<h2>AWIS - Telefonverzeichnis</h2>

<h3>Suchmaske</h3>
In der Suchmaske zum Telefonverzeichnis k�nnen unterschiedliche Suchbegriffe ausgew�hlt werden. In der 
nachfolgenden Tabelle sind diese Begriffe n�her erl�utert.<br>
<table border=1 width=100%>
<tr>
	<td id=FeldBez>Suchbegriff</td>
	<td id=FeldBez>Erl�uterung</td>
</tr>

<tr>
	<td>Suche</td>
	<td>Wenn in diesem Feld eine Eingabe vorgenommen wird, wird in den Feldern Name1, Name2, alle Kontakte (sofern
		ausreichende Rechte vorhanden sind) und Abteilungsnamen gesucht. Es wird nach allem gesucht, was mit
		dem angegeben Suchbegriff beginnt, also z.B. bei "Mei" ein "<u>Mei</u>er", "<u>Mei</u>erhofer", ...<br>
		Wenn ein Suchbegriff angegeben werden soll, der in einem der oben beschriebenen Feldern beinhaltet ist, so
		muss ein Stern (*) vorangestellt werden, z.B. <i>"*mei"</i> w�rde auch <i>"Habermeier"</i> finden.
	</td>
</tr>

<tr>
	<td>Name</td>
	<td>Es werden Namen in den Feldern Name1 und Name2 gesucht. Es wird nach einem Teil des angegeben
		Namen gesucht, also z.B. bei "Mei" ein "<u>Mei</u>er", "<u>Mei</u>erhofer", "A<u>mei</u>se", ...<br>
		Wenn die Optionsschaltfl�che <i>�hnliche Namen suchen</i> ausgew�hlt wurde, werden auch Namen angezeigt,
		die �hnlich 'klingen'.
	</td>
</tr>

<tr>
	<td>Kontakt</td>
	<td>Hier wird in den Kontakten gesucht, f�r die eine Berechtigung existiert. Es wird dabei nach Telefon, Fax,
		Durchwahlen, E-Mail und weiteren Kontaktarten gesucht.</td>
</tr>

<tr>
	<td>KFZ-Kennzeichen</td>
	<td>Es wird nach einem KFZ-Kennzeichen gesucht.</td>
</tr>

<tr>
	<td>Fahrzeugtyp</td>
	<td>Hier kann nach einem KFZ-Typ gesucht werden, z.B. <b>Golf</b>.</td>
</tr>

<tr>
	<td>Kategorie</td>
	<td>In dieser Liste kann nach allen Kategorien gesucht werden. Die Kontakte werden in bestimmte Bereiche eingeteilt.
	Dazu z�hlen ATU Mitarbeiter, Externe und andere.</td>
</tr>

<tr>
	<td>Bereich</td>
	<td>In dieser Liste kann nach allen Kategorien gesucht werden. Bei den Bereichen werden Kontakte z.B. zu bestimmten
	Unternehmensbereichen zusammengefasst.</td>
</tr>


</table>

<hr>
<h3>Daten bearbeiten</h3>
<?php
$con = awisLogon();

if(awisBenutzerRecht($con,152)>0)
{
echo '<hr>';
echo 'Um die Kontaktdaten zu �ndern sind unterschiedliche Rechtestufen gepflegt. In der nachfolgenden Tabelle werden diese beschrieben.';
echo '<table border=1 width=100%';

echo '<tr><td colspan=2 id=FeldBez>�nderungsrechte</td></tr>';

echo '<tr><td width=100>-keine-</td><td>Keine Berechtigungen werden f�r den eigenen Kontakt ben�tigt. Hier k�nnen 
			folgende Felder bearbeitet werden:<br>
			<b>Strasse<br>
			Plz, Ort <br>
			Bemerkung zum Kontakt</b></td></tr>';
echo '<tr><td>Stufe 1</td><td>Alle Felder wie bei eigenem Kontakt, jedoch f�r alle Kontakte auf der 
			gleichen Zugriffs-Stufe.</td></tr>';
echo '<tr><td>Stufe 2</td><td>In dieser Stufe k�nnen folgende Felder ge�ndert werden<br>
			<b>Kategorie<br>
			Abteilung(en)<br>
			Zust�ndigkeit</b></td></tr>';
echo '<tr><td>Stufe 4</td><td>In dieser Stufe k�nnen folgende Felder ge�ndert werden:<br>
			<b>Name1, Name2<br>
			Durchwahl, Nebendurchwahl, Fax-Durchwahl<br>
			ATU-Geb�ude<br>
			Interne Bemerkung f�r den Empfang</b></td></tr>';
echo '<tr><td></td><td></td></tr>';
echo '<tr><td></td><td></td></tr>';


echo '</table>';

awislogoff($con);
}




?>

<hr>
<h3>Erreichbarkeit</h3>
<br>
Im unteren Block der Anzeige werden die unterschiedlichen Kontakte aufgef�hrt. Je nach Berechtigung
kann ein Mitarbeiter unterschiedliche Kontakte einsehen. Es wird (bisher) zwischen drei unterschiedlichen
Arten unterschieden, die mit unterschiedlichen Sysmbolen dargestellt werden.<br>

<table border=1 width=100%>
<tr>
	<td id=FeldBez>Kontaktart</td>
	<td id=FeldBez>Erl�uterung</td>
	<td id=FeldBez>Symbol</td>
</tr>


<tr>
	<td><b>�ffentliche Daten</b></td>
	<td>Diese Informationen sind f�r jeden Mitarbeiter, der einen Zugang zum Telefonbuch hat, einzusehen.</td>
	<td align=center><img src=/bilder/Schloss_offen.png alt=�ffentliche_Daten></td>
</tr>

<tr>
	<td><b>Private Daten</b></td>
	<td>Diese Informationen sind nur f�r Mitarbeiter einer h�heren Berechtigungsstufe einzusehen. Dies ist z.B.
		der Empfang. Jeder Mitarbeiter kann "seine" Informationen selbst pflegen.
		<br>�ber die Option <i>Adresse ausblenden</i> kann die beim Kontakt hinterlegte Adresse
		f�r Standardanwender ausgeblendet werden. F�r Anwender mit h�herer Berechtigungsstufe (z.B. Empfang) sind diese sichtbar.</td>
	<td align=center><img src=/bilder/Schloss_zu.png alt=Private_Daten></td>
</tr>
<tr>
	<td><b>�ffentliche, vom Empfang gepflegte Daten</b></td>
	<td>Diese Informationen k�nnen nur vom Empfang gepflegt werden, sind aber f�r jeden Mitarbeiter sichtbar.</td>
	<td align=center><img src=/bilder/Schloss_offen_gold.png alt=Daten_Empfang></td>
</tr>

<tr>
	<td><b>Daten Empfang</b></td>
	<td>Diese Informationen sind nur f�r die h�chste Berechtigungsstufe, z.B. den Empfang einzusehen und zu pflegen.</td>
	<td align=center><img src=/bilder/Schloss_zu_gold.png alt=Daten_Empfang></td>
</tr>
</table>

<hr>
<h3>Tastenk�rzel</h3>
<br>

In den Telefonmasken k�nnen die Schaltfl�chen auch mit Tastenk�rzeln bedient werden. Dazu sind folgende K�rzel definiert.

<table border=1 width=100%>
<tr>
	<td id=FeldBez>K�rzel</td>
	<td id=FeldBez>Erl�uterung</td>
	<td id=FeldBez>Symbol</td>
</tr>
<tr>
	<td>ALT+N</td>
	<td>Neuen Kontakt hinzuf�gen</td>
	<td><img src=/bilder/plus.png></td>
</tr>
<tr>
	<td>ALT+X</td>
	<td>Kontakt l�schen</td>
	<td><img src=/bilder/Muelleimer_gross.png></td>
</tr>
<tr>
	<td>ALT+S</td>
	<td>�nderungen am Kontakt speichern</td>
	<td><img src=/bilder/diskette.png></td></tr>
<tr>
	<td>ALT+H</td>
	<td>Hilfeseite �ffnen</td>
	<td><img src=/bilder/hilfe.png></td></tr>
</tr>
<tr>
	<td>ALT+Z</td>
	<td>Maske verlassen und in das Hauptmen� zur�ckkehren</td>
	<td><img src=/bilder/zurueck.png></td></tr>
</tr>
<tr>
	<td>ALT+R</td>
	<td>Maske zur�cksetzen und alle Eingaben l�schen</td>
	<td><img src=/bilder/radierer.png></td></tr>
</tr>

</table>



</body>

</html>
