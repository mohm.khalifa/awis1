<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PAP','%');
	$TextKonserven[]=array('KFZ','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8003 = $AWISBenutzer->HatDasRecht(8003);
	if($Recht8003==0)
	{
	    awisEreignis(3,1000,'Preisermittlung - ',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PAP'));
	if(!isset($Param['BLOCK']))
	{
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='PAP_BEZEICHNUNG';
		$Param['BLOCK']=1;
	}

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./preisabfragen_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./preisabfragen_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PAP'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PAP'));
	}
	elseif(isset($_GET['PAP_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['PAP_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PAP'));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='PAP_BEZEICHNUNG DESC';
			$AWISBenutzer->ParameterSchreiben('Formular_PAP',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY PAP_BEZEICHNUNG DESC';
			$Param['ORDER']='PAP_BEZEICHNUNG DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT Preisabfragenartikelpakete.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Preisabfragenartikelpakete';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_PAP',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsPAP = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_PAP',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	$Form->SchreibeHTMLCode('<form name=frmPreisabfragenartikelpakete action=./preisabfragen_Main.php?cmdAktion=Artikelpakete method=POST enctype="multipart/form-data">');

	if($rsPAP->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Felder = array();		// Leeres Feld zeigen, zum eine Liste zu erzwingen
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./preisabfragen_Main.php?cmdAktion=Artikelpakete&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Form->InfoZeile($Felder,'');
		
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsPAP->AnzahlDatensaetze()>1 AND !isset($_GET['PAP_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=PAP_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PAP_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PAP']['PAP_BEZEICHNUNG'],250,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsPAP->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete&PAP_KEY=0'.$rsPAP->FeldInhalt('PAP_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('#PAP_BEZEICHNUNG',$rsPAP->FeldInhalt('PAP_BEZEICHNUNG'),0,250,false,($DS%2),'',$Link,'T');
			if($rsPAP->FeldInhalt('PAP_BEMERKUNG')!='')
			{
				$Form->Erstelle_ListenBild('ohne', '', '', '/bilder/icon_info.png',$rsPAP->FeldInhalt('PAP_BEMERKUNG'),($DS%2));
			}
			$Form->ZeileEnde();

			$rsPAP->DSWeiter();
			$DS++;
		}

		$Link = './preisabfragen_Main.php?cmdAktion=Artikelpakete';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsPAP->FeldInhalt('PAP_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_PAP',serialize($Param));

		$Form->Erstelle_HiddenFeld('PAP_KEY',$AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./preisabfragen_Main.php?cmdAktion=Artikelpakete&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPAP->FeldInhalt('PAP_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPAP->FeldInhalt('PAP_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditModus=(($Recht8003&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditModus=($Recht8003&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAP']['PAP_BEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('PAP_BEZEICHNUNG',$rsPAP->FeldInhalt('PAP_BEZEICHNUNG'),50,200,$EditModus,'','','','T');
		$AWISCursorPosition = 'txtPAP_BEZEICHNUNG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PAP']['PAP_BEMERKUNG'].':',200,'','');
		$Form->Erstelle_Textarea('PAP_BEMERKUNG',$rsPAP->FeldInhalt('PAP_BEMERKUNG'),800,100,5,$EditModus);
		$Form->ZeileEnde();

		
		if($rsPAP->FeldInhalt('PAP_KEY')!='')
		{
			$Reg = new awisRegister(8003);
			$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
		}
	}

	//awis_Debug(1, $Param, $Bedingung, $rsPAP, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht8003&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht8003&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201615");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201614");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND PAP_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	return $Bedingung;
}
?>