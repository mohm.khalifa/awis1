<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('TITEL','tit_ZukaufArtikel');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_reset');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');

$Form = new awisFormular();
$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
echo '<title>'.$AWISSprachKonserven['TITEL']['tit_ZukaufArtikel'].'</title>';
?>
</head>
<body>
<?php
try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Form->Formular_Start();
	$Form->SchreibeHTMLCode('<form name=frmFusion action=./zukaufartikel_fusion.php method=POST>');


	if(isset($_POST['cmdSpeichern_x']))
	{
		// Umsetzen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Beginne Umsetzung...',200,'Hinweis');
		$Form->ZeileEnde();

		$DB->TransaktionBegin();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Rechnungen.',200,'');
		$Form->ZeileEnde();

		$SQL = 'UPDATE Zukaufrechnungen';
		$SQL .= ' SET ZUR_ZLA_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZUR_ZLA_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Lieferscheine.',200,'');
		$Form->ZeileEnde();

		$SQL = 'UPDATE Zukauflieferscheine';
		$SQL .= ' SET ZUL_ZLA_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZUL_ZLA_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Zukauflieferantenartikelarchiv.',200,'');
		$Form->ZeileEnde();

		$SQL = 'UPDATE ZUKAUFLIEFERANTENARTIKELARCHIV';
		$SQL .= ' SET ZAH_ZLA_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZAH_ZLA_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Zukauflieferantenbestellungen.',200,'');
		$Form->ZeileEnde();

		$SQL = 'UPDATE Zukauflieferantenbestellungen';
		$SQL .= ' SET ZLB_ZLA_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZLB_ZLA_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Zukaufpreisauskunft.',200,'');
		$Form->ZeileEnde();

		$SQL = 'UPDATE Zukaufpreisauskunft';
		$SQL .= ' SET ZPA_ZLA_KEY = '.$_POST['txtziel'];
		$SQL .= ' WHERE ZPA_ZLA_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);

		// Die zu l�schenden Artikel suchen
		$SQL = 'SELECT *';
		$SQL .= ' FROM Zukaufartikellieferanten';
		$SQL .= ' WHERE ZAL_ZLA_KEY = '.$_POST['txtloeschen'];
		$rsZAL = $DB->RecordSetOeffnen($SQL);
		while(!$rsZAL->EOF())
		{
			$SQL = 'SELECT *';
			$SQL .= ' FROM Zukaufartikellieferanten ';
			$SQL .= ' WHERE ZAL_ZLA_KEY = '.$_POST['txtziel'];
			$SQL .= ' AND ZAL_LIE_NR = \''.$rsZAL->FeldInhalt('ZAL_LIE_NR');
			$rsNEU = $DB->RecordSetOeffnen($SQL);
			if($rsNEU->EOF())		// Gibt es nicht -> umh�ngen
			{
				$SQL = 'UPDATE Zukaufartikellieferanten';
				$SQL .= ' SET ZAL_ZLA_KEY = '.$_POST['txtziel'];
				$DB->Ausfuehren($SQL);
			}
			else 		// Die Preise umh�ngen und den Artikel l�schen
			{
				$SQL = 'UPDATE Zukaufartikellieferantenpreise';
				$SQL .= ' SET ZAP_ZAL_KEY = '.$_POST['txtziel'];
				$SQL .= ' WHERE ZPA_ZLA_KEY = '.$_POST['txtloeschen'];
				$DB->Ausfuehren($SQL);

				$SQL = 'UPDATE Zukaufbestellungen';
				$SQL .= ' SET ZUB_ZAL_KEY = '.$_POST['txtziel'];
				$SQL .= ' WHERE ZUB_ZLA_KEY = '.$_POST['txtloeschen'];
				$DB->Ausfuehren($SQL);

				$SQL = 'UPDATE Zukauflieferscheine';
				$SQL .= ' SET ZUL_ZAL_KEY = '.$_POST['txtziel'];
				$SQL .= ' WHERE ZUL_ZLA_KEY = '.$_POST['txtloeschen'];
				$DB->Ausfuehren($SQL);

				$SQL = 'UPDATE Zukaufrechnungen';
				$SQL .= ' SET ZUR_ZAL_KEY = '.$_POST['txtziel'];
				$SQL .= ' WHERE ZUR_ZLA_KEY = '.$_POST['txtloeschen'];
				$DB->Ausfuehren($SQL);


				$SQL = 'DELETE FROM Zukaufartikellieferanten';
				$SQL .= ' WHERE ZAL_ZLA_KEY = '.$_POST['txtloeschen'];
				$DB->Ausfuehren($SQL);
			}

			$rsZAL->DSWeiter();
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('   Artikel l�schen.',200,'');
		$Form->ZeileEnde();

		$SQL = 'DELETE FROM Zukaufartikel';
		$SQL .= ' WHERE ZLA_KEY = '.$_POST['txtloeschen'];
		$DB->Ausfuehren($SQL);

		$DB->TransaktionCommit();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Umsetzung beendet...',200,'Hinweis');
		$Form->ZeileEnde();
	}

	$Form->ZeileStart();

	$Form->Erstelle_TextLabel('Zu l�schen:',200);
	// Abfrage mit doppelten
	$SQL = ' SELECT zla_key, anzeige, ZLA_ARTIKELNUMMER,ZLH_KUERZEL ';
	$SQL .= ' FROM (';
	$SQL .= ' SELECT zla_key, ZLA_ARTIKELNUMMER,ZLH_KUERZEL';
	$SQL .= ' , zla_key || \' = \' || ZLA_ARTIKELNUMMER || \' - \' || ZLA_BEZEICHNUNG || \' - \' ||  ZLH_KUERZEL || \' - \' || \' ~ \'';
	$SQL .= ' || row_number() over(PARTITION BY zla_artikelnummer, ZLH_KUERZEL ORDER BY zla_key) AS ANZEIGE';
	$SQL .= ' , row_number() over(PARTITION BY zla_artikelnummer, ZLH_KUERZEL ORDER BY zla_key) AS ANZAHL';
	$SQL .= ' from zukaufartikel';
	$SQL .= ' INNER JOIN zukauflieferantenhersteller ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ') DATEN WHERE ANZAHL > 1';
	$SQL .= ' ORDER BY 3,4';

	$Form->Erstelle_SelectFeld('loeschen','',200,true,$SQL);

	$Form->ZeileEnde();

	$Form->ZeileStart();

	$Form->Erstelle_TextLabel('Zuordnen zu:',200);

	$SQL = ' SELECT zla_key, anzeige';
	$SQL .= ' FROM (';
	$SQL .= ' SELECT zla_key';
	$SQL .= ' , zla_key || \' = \' || ZLA_ARTIKELNUMMER || \' - \' || ZLA_BEZEICHNUNG || \' - \' ||  ZLH_KUERZEL || \' - \' || \' ~ \'';
	$SQL .= ' || row_number() over(PARTITION BY zla_artikelnummer, ZLH_KUERZEL ORDER BY zla_key) AS ANZEIGE';
	$SQL .= ' , row_number() over(PARTITION BY zla_artikelnummer, ZLH_KUERZEL ORDER BY zla_key) AS ANZAHL';
	$SQL .= ' from zukaufartikel';
	$SQL .= ' INNER JOIN zukauflieferantenhersteller ON ZLA_ZLH_KEY = ZLH_KEY';
	$SQL .= ') DATEN WHERE ANZAHL > 1';
	$SQL .= ' ORDER BY 2';

	$Form->Erstelle_SelectFeld('ziel',(isset($_POST['txtziel'])?$_POST['txtziel']:''),200,true,$SQL);

	$Form->ZeileEnde();
	


	$Form->Formular_Ende();




	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
}
catch(exception $ex)
{
	echo ' Fehler:'.$ex->getMessage();

	if(isset($DB) AND $DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
}
