<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUB','ttt_ZUBSTATUS');
	$TextKonserven[]=array('ZUB','ZUB_WANR');
	$TextKonserven[]=array('ZUB','LetzterDatensatz');
	$TextKonserven[]=array('ZIL','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_ZIL_TYP');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('XJO','XJO_%');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
 
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10007 = $AWISBenutzer->HatDasRecht(10007);
	if($Recht10007==0)
	{
	    awisEreignis(3,1000,'Zukauf',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Form->SchreibeHTMLCode('<form name="frmZukaufAuswertungen" action="./zukaufbestellungen_Main.php?cmdAktion=Auswertungen" method="POST">');

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUB_AUSW'));
    if(!isset($Param['DATUMVOM']))
    {
        $Param['DATUMVOM']=date('d.m.Y',mktime(0,0,0,1,1,date('Y')));
        $Param['DATUMBIS']=date('d.m.Y');
        $Param['AUSWERTUNG']=1;
    }
    
    if(isset($_POST['cmd_weiter_x']))
    {
        $Param['DATUMVOM']=$_POST['txtDATUMVOM'];
        $Param['DATUMBIS']=$_POST['txtDATUMBIS'];
        $Param['AUSWERTUNG']=$_POST['txtAUSWERTUNG'];
    }

	$Form->Formular_Start();
	
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Datum vom:', 150);
	$Form->Erstelle_TextFeld('!DATUMVOM', $Param['DATUMVOM'], 10, 150,true,'','','','D');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Datum bis:', 150);
	$Form->Erstelle_TextFeld('!DATUMBIS', $Param['DATUMBIS'], 10, 150,true,'','','','D');
	$Form->ZeileEnde();
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel('Auswertung:', 150);
	$Form->Erstelle_SelectFeld('AUSWERTUNG', $Param['AUSWERTUNG'], '300:280',true,'','','','','',array('1~KFZ-Hersteller','2~Sortiment'));
	$Form->ZeileEnde();
	
	if(isset($_POST['cmd_weiter_x']) OR isset($_GET['FILTER']))
	{
	    switch($Param['AUSWERTUNG'])
	    {
	        case 1:            // Nach Herstellern
	            if(isset($_GET['FILTER']))
	            {
	                $Filter = explode('~', $_GET['FILTER']);
	                switch($Filter[0])
	                {
	                    case 'KHERNR':
	                        $SQL = "SELECT KFZ_HERST.BEZ,KFZ_HERST.KHERNR,SUM(ZWA_MENGE) AS Anzahl";
	                        $SQL .= ', KFZ_MODELL.BEZ AS Modell,KFZ_MODELL.KMODNR';
	                        $SQL .= ' FROM ZUKAUFBESTELLUNGEN';
	                        $SQL .= ' INNER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
	                        $SQL .= ' INNER JOIN KFZ_TYP ON ZWA_KTYP = KTYPNR';
	                        $SQL .= ' INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR';
	                        $SQL .= ' INNER JOIN KFZ_HERST ON KFZ_HERST.KHERNR= KFZ_MODELL.KHERNR';
	                        $SQL .= ' WHERE ZUB_BESTELLDATUM >= '.$DB->WertSetzen('STAT', 'D', $Param['DATUMVOM']);
	                        $SQL .= ' AND TRUNC(ZUB_BESTELLDATUM) < '.$DB->WertSetzen('STAT', 'D', $Param['DATUMBIS']);
	                        $SQL .= ' AND KFZ_HERST.KHERNR = '.$DB->WertSetzen('STAT', 'N0', $Filter[1]);
	                        $SQL .= " GROUP BY KFZ_HERST.BEZ,KFZ_HERST.KHERNR, KFZ_MODELL.BEZ, KFZ_MODELL.KMODNR";
	                        $SQL .= ' ORDER BY 3 DESC';
	                        break;
	                    case 'KMODNR':
	                        $SQL = "SELECT KFZ_HERST.BEZ,KFZ_HERST.KHERNR,SUM(ZWA_MENGE) AS Anzahl";
	                        $SQL .= ', KFZ_MODELL.BEZ AS Modell,KFZ_MODELL.KMODNR';
	                        $SQL .= ', ZUB_SORTIMENT';
	                        $SQL .= ' FROM ZUKAUFBESTELLUNGEN';
	                        $SQL .= ' INNER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
	                        $SQL .= ' INNER JOIN KFZ_TYP ON ZWA_KTYP = KTYPNR';
	                        $SQL .= ' INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR';
	                        $SQL .= ' INNER JOIN KFZ_HERST ON KFZ_HERST.KHERNR= KFZ_MODELL.KHERNR';
	                        $SQL .= ' WHERE ZUB_BESTELLDATUM >= '.$DB->WertSetzen('STAT', 'D', $Param['DATUMVOM']);
	                        $SQL .= ' AND TRUNC(ZUB_BESTELLDATUM) < '.$DB->WertSetzen('STAT', 'D', $Param['DATUMBIS']);
	                        $SQL .= ' AND KFZ_MODELL.KMODNR = '.$DB->WertSetzen('STAT', 'N0', $Filter[1]);
	                        $SQL .= " GROUP BY KFZ_HERST.BEZ,KFZ_HERST.KHERNR, KFZ_MODELL.BEZ, KFZ_MODELL.KMODNR, ZUB_SORTIMENT";
	                        $SQL .= ' ORDER BY 3 DESC';
	                        break;
	                    case 'ZUB_SORTIMENT':
	                        $SQL = "SELECT KFZ_HERST.BEZ,KFZ_HERST.KHERNR,SUM(ZWA_MENGE) AS Anzahl";
	                        $SQL .= ', KFZ_MODELL.BEZ AS Modell,KFZ_MODELL.KMODNR';
	                        $SQL .= ', ZUB_SORTIMENT, ZLA_BEZEICHNUNG, ZLA_KEY';
	                        $SQL .= ' FROM ZUKAUFBESTELLUNGEN';
	                        $SQL .= ' INNER JOIN ZUKAUFARTIKEL ON ZUB_ZLA_KEY = ZLA_KEY';
	                        $SQL .= ' INNER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
	                        $SQL .= ' INNER JOIN KFZ_TYP ON ZWA_KTYP = KTYPNR';
	                        $SQL .= ' INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR';
	                        $SQL .= ' INNER JOIN KFZ_HERST ON KFZ_HERST.KHERNR= KFZ_MODELL.KHERNR';
	                        $SQL .= ' WHERE ZUB_BESTELLDATUM >= '.$DB->WertSetzen('STAT', 'D', $Param['DATUMVOM']);
	                        $SQL .= ' AND TRUNC(ZUB_BESTELLDATUM) < '.$DB->WertSetzen('STAT', 'D', $Param['DATUMBIS']);
	                        $SQL .= ' AND ZUB_SORTIMENT = '.$DB->WertSetzen('STAT', 'T', $Filter[1]);
	                        $SQL .= ' AND KFZ_MODELL.KMODNR = '.$DB->WertSetzen('STAT', 'N0', $Filter[3]);
	                        $SQL .= " GROUP BY KFZ_HERST.BEZ,KFZ_HERST.KHERNR, KFZ_MODELL.BEZ, KFZ_MODELL.KMODNR, ZUB_SORTIMENT, ZLA_BEZEICHNUNG, ZLA_KEY";
	                        $SQL .= ' ORDER BY 3 DESC';
	                        break;
	                }
	            }
	            else 
	            {
	                $SQL = "SELECT KFZ_HERST.BEZ,KFZ_HERST.KHERNR, SUM(ZWA_MENGE) AS Anzahl";
	                $SQL .= ' FROM ZUKAUFBESTELLUNGEN';
	                $SQL .= ' INNER JOIN ZUKAUFWERKSTATTAUFTRAEGE ON ZWA_ZUB_KEY = ZUB_KEY';
	                $SQL .= ' INNER JOIN KFZ_TYP ON ZWA_KTYP = KTYPNR';
	                $SQL .= ' INNER JOIN KFZ_MODELL ON KFZ_TYP.KMODNR = KFZ_MODELL.KMODNR';
	                $SQL .= ' INNER JOIN KFZ_HERST ON KFZ_HERST.KHERNR= KFZ_MODELL.KHERNR';
	                $SQL .= ' WHERE ZUB_BESTELLDATUM >= '.$DB->WertSetzen('STAT', 'D', $Param['DATUMVOM']);
	                $SQL .= ' AND TRUNC(ZUB_BESTELLDATUM) < '.$DB->WertSetzen('STAT', 'D', $Param['DATUMBIS']);
	                $SQL .= " GROUP BY KFZ_HERST.BEZ,KFZ_HERST.KHERNR";
	                $SQL .= ' ORDER BY 3 DESC';
	            }
	            
	            $rsSTAT = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('STAT'));
                $DS=0;	             

                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift('KFZ-Hersteller', 300);
                if(isset($_GET['FILTER']))
                {
                    switch($Filter[0])
                    {
                        case 'KHERNR':
                            $Form->Erstelle_Liste_Ueberschrift('Modell', 400);
                            break;
                        case 'KMODNR':
                            $Form->Erstelle_Liste_Ueberschrift('Modell', 400);
                            $Form->Erstelle_Liste_Ueberschrift('Sortiment', 100);
                            break;
                        case 'ZUB_SORTIMENT':
                            $Form->Erstelle_Liste_Ueberschrift('Modell', 300);
                            $Form->Erstelle_Liste_Ueberschrift('Sortiment', 100);
                            $Form->Erstelle_Liste_Ueberschrift('Artikel', 300);
                            break;
                    }
                }
                $Form->Erstelle_Liste_Ueberschrift('Anzahl', 100);
                $Form->ZeileEnde();
                
	            while(!$rsSTAT->EOF())
	            {
	                $Form->ZeileStart();
	                $Link = './zukaufbestellungen_Main.php?cmdAktion=Auswertungen&FILTER=KHERNR~'.$rsSTAT->FeldInhalt('KHERNR');
	                $Form->Erstelle_ListenFeld('Hersteller', $rsSTAT->FeldInhalt('BEZ'), 30, 300,false,($DS%2),'',$Link);
	                if(isset($_GET['FILTER']))
	                {
	                    switch($Filter[0])
	                    {
	                        case 'KHERNR':
	                            $Link = './zukaufbestellungen_Main.php?cmdAktion=Auswertungen&FILTER=KMODNR~'.$rsSTAT->FeldInhalt('KMODNR');
	                            $Form->Erstelle_ListenFeld('Modell', $rsSTAT->FeldInhalt('MODELL'), 4, 400,false,($DS%2),'',$Link);
	                            $Link = '';
                                break;
	                        case 'KMODNR':
	                            $Link = './zukaufbestellungen_Main.php?cmdAktion=Auswertungen&FILTER=KMODNR~'.$rsSTAT->FeldInhalt('KMODNR');
	                            $Form->Erstelle_ListenFeld('Modell', $rsSTAT->FeldInhalt('MODELL'), 4, 400,false,($DS%2),'',$Link);
	                            $Link = './zukaufbestellungen_Main.php?cmdAktion=Auswertungen&FILTER=ZUB_SORTIMENT~'.$rsSTAT->FeldInhalt('ZUB_SORTIMENT').'~KMODNR~'.$rsSTAT->FeldInhalt('KMODNR');
	                            $Form->Erstelle_ListenFeld('Sortiment', $rsSTAT->FeldInhalt('ZUB_SORTIMENT'), 4, 200,false,($DS%2),'',$Link);
	                            $Link = '';
                                break;
	                        case 'ZUB_SORTIMENT':
	                            $Link = './zukaufbestellungen_Main.php?cmdAktion=Auswertungen&FILTER=KMODNR~'.$rsSTAT->FeldInhalt('KMODNR');
	                            $Form->Erstelle_ListenFeld('Modell', $rsSTAT->FeldInhalt('MODELL'), 4, 300,false,($DS%2),'',$Link);
	                            $Link = './zukaufbestellungen_Main.php?cmdAktion=Auswertungen&FILTER=ZUB_SORTIMENT~'.$rsSTAT->FeldInhalt('ZUB_SORTIMENT').'~KMODNR~'.$rsSTAT->FeldInhalt('KMODNR');
	                            $Form->Erstelle_ListenFeld('Sortiment', $rsSTAT->FeldInhalt('ZUB_SORTIMENT'), 4, 100,false,($DS%2),'',$Link);
	                            $Link = '/zukauf/zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsSTAT->FeldInhalt('ZLA_KEY');
	                            $Form->Erstelle_ListenFeld('Artikel', $rsSTAT->FeldInhalt('ZLA_BEZEICHNUNG'), 4, 300,false,($DS%2),'',$Link);
	                            $Link = '';
                                break;
	                    }
	                }
	                $Form->Erstelle_ListenFeld('Anz', $rsSTAT->FeldInhalt('ANZAHL'), 30, 100,false,($DS%2));
	                $Form->ZeileEnde();
	                
	                
	                $rsSTAT->DSWeiter();
	                $DS++;
	            }
	            break;
	    }
	}	
	
	$Form->Formular_Ende();
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();
	
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image','cmd_weiter','','/bilder/cmd_weiter.png',$AWISSprachKonserven['Wort']['lbl_weiter'],'W');
	
	$Form->SchaltflaechenEnde();
	
	$Form->SchreibeHTMLCode('</form>');
	
	$Form->SetzeCursor($AWISCursorPosition);
	$AWISBenutzer->ParameterSchreiben('Formular_ZUB_AUSW', serialize($Param));
	
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>