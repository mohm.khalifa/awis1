<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISBenutzer;

try 
{
    require_once ('awisDatenbank.inc');
    require_once('awisDokument.inc');
    
    $TextKonserven=array();
    $TextKonserven[]=array('Fehler','err_KeinWert');
    $TextKonserven[]=array('Fehler','err_FelderVeraendert');
    $TextKonserven[]=array('Wort','geaendert_von');
    $TextKonserven[]=array('Wort','geaendert_auf');
    $TextKonserven[]=array('Meldung','DSVeraendert');
    $TextKonserven[]=array('Adressen','ADR_%');
    $TextKonserven[]=array('AAP','%');
    $TextKonserven[]=array('ADR','%');
    $TextKonserven[]=array('ADI','ADI_%');
    $TextKonserven[]=array('ADT','ADT_%');
    $TextKonserven[]=array('MBW','txtAnredenListe');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_trefferliste');
    $TextKonserven[]=array('Wort','lbl_aendern');
    $TextKonserven[]=array('Wort','lbl_hinzufuegen');
    $TextKonserven[]=array('Wort','lbl_loeschen');
    $TextKonserven[]=array('Wort','lbl_DSZurueck');
    $TextKonserven[]=array('Wort','lbl_DSWeiter');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
    $TextKonserven[]=array('Fehler','err_keineRechte');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    
    $AWIS_KEY1 = $_POST['txtADR_KEY'];
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    
    $AWISSprache = $AWISBenutzer->ParameterLesen('AnzeigeSprache');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    
    if(intval($_POST['txtADR_KEY'])===0)		// Neue Adresse
    {
        // Daten auf Vollst�ndigkeit pr�fen
        $Fehler = '';
        $Pflichtfelder = array('ADR_NAME1','ADR_STRASSE','ADR_HAUSNUMMER','ADR_PLZ','ADR_ORT','ADR_TELEFON','ADR_AZG_KEY');
        foreach($Pflichtfelder AS $Pflichtfeld)
        {
            if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
            {
                $Fehler .= $AWISSprachKonserven['Fehler']['err_KeinWert'].' '.$AWISSprachKonserven['ADR'][$Pflichtfeld].'<br>';
            }
        }
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if($Fehler!='')
        {
            die('<span class=HinweisText>'.$Fehler.'</span>');
        }
    
        $SQL = 'INSERT INTO Adressen';
        $SQL .= '(ADR_ANREDE,ADR_KONZERN, ADR_NAME1, ADR_NAME2';
        $SQL .= ',ADR_STRASSE, ADR_HAUSNUMMER';
        $SQL .= ',ADR_LAN_CODE, ADR_PLZ, ADR_ORT, ADR_BEMERKUNG, ADR_STATUS';
        $SQL .= ',ADR_TELEFON,ADR_AZG_KEY,ADR_WEBSEITE,ADR_EMAIL,ADR_USTIDNR';
        $SQL .= ',ADR_STEUERNUMMER,ADR_HANDELSREGISTER';
        $SQL .= ',ADR_USER, ADR_USERDAT';
        $SQL .= ')VALUES ('.$DB->FeldInhaltFormat('Z',$_POST['txtADR_ANREDE'],false);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_KONZERN'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_NAME1'],false);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_NAME2'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_STRASSE'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_HAUSNUMMER'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_LAN_CODE'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_PLZ'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_ORT'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',(isset($_POST['txtADR_BEMERKUNG'])?$_POST['txtADR_BEMERKUNG']:''),true);
        $SQL .= ',\'A\'';
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_TELEFON'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('Z',(isset($_POST['txtADR_AZG_KEY'])!=-1?$_POST['txtADR_AZG_KEY']:'1'),true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_WEBSEITE'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_EMAIL'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_USTIDNR'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_STEUERNUMMER'],true);
        $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtADR_HANDELSREGISTER'],true);
        $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
        $SQL .= ',SYSDATE';
        $SQL .= ')';

        if($DB->Ausfuehren($SQL)===false)
        {
            throw new awisException('Fehler beim Speichern',201403240930,$SQL,awisException::AWIS_ERR_SYSTEM);
            die();
        }
        
        $SQL = 'SELECT seq_ADR_KEY.CurrVal AS KEY FROM DUAL';
        $rsKey = $DB->RecordSetOeffnen($SQL);
        $AWIS_KEY1=$rsKey->FeldInhalt('KEY');
        
        $AWISBenutzer->ParameterSchreiben("AktuelleAdresse", $AWIS_KEY1);
    }
    else 					// ge�nderter Adressen
    {
        $Felder = explode(';',$Form->NameInArray($_POST, 'txtADR',1,1));
        $FehlerListe = array();
        $UpdateFelder = '';
    
        $rsFelder = $DB->RecordSetOeffnen('SELECT * FROM Adressen WHERE ADR_key=' . $_POST['txtADR_KEY'] . '');
        $FeldListe = '';
        foreach($Felder AS $Feld)
        {
            $FeldName = substr($Feld,3);
            if(isset($_POST['old'.$FeldName]))
            {
                // Alten und neuen Wert umformatieren!!
                $WertNeu=$DB->FeldInhaltFormat($rsFelder->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
                $WertAlt=$DB->FeldInhaltFormat($rsFelder->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
                $WertDB=$DB->FeldInhaltFormat($rsFelder->FeldInfo($FeldName,'TypKZ'),$rsFelder->FeldInhalt($FeldName),true);
                //echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
                if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
                {
                    if($WertAlt != $WertDB AND $WertDB!='null')
                    {
                        $FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
                    }
                    else
                    {
                        $FeldListe .= ', '.$FeldName.'=';
    
                        if($_POST[$Feld]=='')	// Leere Felder immer als NULL
                        {
                            $FeldListe.=' null';
                        }
                        else
                        {
                            $FeldListe.=$WertNeu;
                        }
                    }
                }
            }
        }
    
        if(count($FehlerListe)>0)
        {
            $Meldung = str_replace('%1',$rsFelder->FeldInhalt('ADR_USER'),$AWISSprachKonserven['Meldung']['DSVeraendert']);
            
            foreach($FehlerListe AS $Fehler)
            {
                $Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
            }
            $Form->Hinweistext($Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
        }
        elseif($FeldListe!='')
        {
            $SQL = 'UPDATE Adressen SET';
            $SQL .= substr($FeldListe,1);
            $SQL .= ', ADR_user=\''.$AWISBenutzer->BenutzerName().'\'';
            $SQL .= ', ADR_userdat=sysdate';
            $SQL .= ' WHERE ADR_key=0' . $_POST['txtADR_KEY'] . '';
            
            if($DB->Ausfuehren($SQL)===false)
            {
                throw new awisException('Fehler beim Speichern',201403251013,$SQL,awisException::AWIS_ERR_SYSTEM);
            }
        }
    }

    //*************************************************************************
    //** Adressen-Infos
    //*************************************************************************
    $Felder = $Form->NameInArray($_POST, 'txtADI_',1,1);
    if($Felder!='')
    {
        $Felder = explode(';',$Felder);
        $TextKonserven[]=array('Wort','geaendert_auf');
        $TextKonserven[]=array('ADI','ADI_%');
        $TextKonserven[]=array('Meldung','DSVeraendert');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);
        $FeldListe='';
        $SQL = '';
    
        $Felder = explode(';',$Form->NameInArray($_POST, 'txtADI_WERT_',1,1));
        foreach($Felder as $Feld)
        {
            $FeldTeile = explode('_',$Feld);	// Format: ADI_WERT_<AIT_ID>_<ADI_KEY>
            if($FeldTeile[3]=='')
            {
                $SQL = 'SELECT ADI_KEY FROM AdressenInformationen';
                $SQL .= ' WHERE ADI_XXX_KEY='.$AWIS_KEY1;
                $SQL .= ' AND ADI_ADT_KEY='. $DB->FeldInhaltFormat('Z',$FeldTeile[2],true);
                $SQL .= ' AND ADI_WERT ' . ($_POST[$Feld]==''?'IS NULL':'=\''.$_POST[$Feld].'\'');
                $rsDaten = $DB->RecordSetOeffnen($SQL);
                	
                if(!$rsDaten->FeldInhalt('ADI_KEY')!='' OR $rsDaten->FeldInhalt('ADI_KEY') AND $rsDaten->FeldInhalt('ADI_KEY')=='')
                {
                    $Fehler = '';
                    $SQL = 'INSERT INTO AdressenInformationen';
                    $SQL .= '(ADI_XXX_KEY, ADI_ADT_KEY, ADI_WERT';
                    $SQL .= ',ADI_USER, ADI_USERDAT';
                    $SQL .= ')VALUES ('.$AWIS_KEY1;
                    $SQL .= ',' . $DB->FeldInhaltFormat('Z',$FeldTeile[2],true);
                    $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST[$Feld],true);
                    $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                    $SQL .= ',SYSDATE';
                    $SQL .= ')';
                    
                    if($DB->Ausfuehren($SQL)===false)
                    {
                        throw new awisException('Fehler beim Speichern',201403240932,$SQL,awisException::AWIS_ERR_SYSTEM);
                        die();
                    }
                }
            }
            else 					// ge�nderter Datensatz
            {
                $FehlerListe = array();
                $UpdateFelder = '';
                $SQL = 'UPDATE AdressenInformationen SET';
                $SQL .= ' ADI_WERT = '.$DB->FeldInhaltFormat('T',$_POST[$Feld]);
                $SQL .= ', ADI_user=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ', ADI_userdat=sysdate';
                $SQL .= ' WHERE ADI_key=0' . $FeldTeile[3] . '';
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403240933,$SQL,awisException::AWIS_ERR_SYSTEM);
                }
            }
        }
    }

    //***********************************************
    // Adressen - Datentabelle
    //***********************************************
    $Felder = $Form->NameInArray($_POST, 'txtXXX_',1,1);
    if($Felder!='')
    {
        $Felder = explode(';',$Felder);
        $TextKonserven[]=array('Wort','geaendert_auf');
        $TextKonserven[]=array('ADI','ADI_%');
        $TextKonserven[]=array('Meldung','DSVeraendert');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);
        $FeldListe='';
        $SQL = '';
        $DatenZeile = 0;
    
        foreach($Felder as $Feld)
        {
            $Keys = explode('_',$Feld);	// 0=txtXXX, 1=xxx, 2=ADT_KEY, 3=DatenZeile, 4=ADI_KEY
            if($Keys[4]=='')
            {
                $Fehler = '';
                	
                if($DatenZeile==0)
                {
                    $DatenZeile = floatval($Keys[3]);
                    if($DatenZeile==0)
                    {
                        $SQL = 'SELECT seq_ADI_KEY.NextVal AS KEY FROM DUAL';
                        $rsKey = $DB->RecordSetOeffnen($SQL);
                        $DatenZeile = $rsKey->FeldInhalt('KEY');
                    }
                }
                $SQL = 'INSERT INTO AdressenInformationen';
                $SQL .= '(ADI_XXX_KEY, ADI_ADT_KEY, ADI_WERT, ADI_DATENZEILE';
                $SQL .= ',ADI_USER, ADI_USERDAT';
                $SQL .= ')VALUES ('.$AWIS_KEY1;
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$Keys[2],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST[$Feld],true);
                $SQL .= ',' . $DatenZeile;
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403240935,$SQL,awisException::AWIS_ERR_SYSTEM);
                    die();
                }
            }
            else 					// ge�nderte Zuordnung
            {
                $FehlerListe = array();
                $UpdateFelder = '';
    
                $SQL = 'UPDATE AdressenInformationen SET';
                $SQL .= ' ADI_WERT = '.$DB->FeldInhaltFormat('T',$_POST[$Feld],true);
                $SQL .= ', ADI_user=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ', ADI_userdat=sysdate';
                $SQL .= ' WHERE ADI_key=0' . $Keys[4] . '';
                	
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403241520,$SQL,awisException::AWIS_ERR_SYSTEM);
                }
            }
        }
    }

    //***********************************************
    // Ansprechpartner
    //***********************************************
    $Felder = $Form->NameInArray($_POST, 'txtAAP_',1,1);
    
    if($Felder!='')
    {
        $AWIS_KEY2=$_POST['txtAAP_KEY'];
        $Felder = explode(';',$Felder);
        $TextKonserven[]=array('Wort','geaendert_auf');
        $TextKonserven[]=array('AAP','%');
        $TextKonserven[]=array('Meldung','DSVeraendert');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);
        $FeldListe='';
        $SQL = '';
               
        if($_POST['txtAAP_KEY']=='0')
        {
            $Speichern = true;
            
            if($Speichern)
            {
                $Fehler = '';
                $SQL = 'INSERT INTO AdressenAnsprechpartner';
                $SQL .= '(AAP_ADR_KEY, AAP_BEMERKUNG';
                $SQL .= ',AAP_ANREDE,AAP_TITEL,AAP_NACHNAME,AAP_VORNAME';
                $SQL .= ',AAP_POSITION,AAP_ABTEILUNG,AAP_GEBURTSDATUM,AAP_DATUMVOM,AAP_DATUMBIS';
                $SQL .= ',AAP_TELEFON,AAP_EMAIL,AAP_AKTIONEN';
                $SQL .= ',AAP_USER, AAP_USERDAT';
                $SQL .= ')VALUES ('.$AWIS_KEY1;
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_BEMERKUNG'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_ANREDE'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_TITEL'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_NACHNAME'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_VORNAME'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_POSITION'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_ABTEILUNG'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtAAP_GEBURTSDATUM'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtAAP_DATUMVOM'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtAAP_DATUMBIS'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_TELEFON'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtAAP_EMAIL'],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtAAP_AKTIONEN'],true);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';

                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403240936,$SQL,awisException::AWIS_ERR_SYSTEM);
                    die();
                }
            }
        }
        else 					// ge�nderte Zuordnung
        {
            $FehlerListe = array();
            $UpdateFelder = '';
            $rsAAP = $DB->RecordSetOeffnen('SELECT * FROM AdressenAnsprechpartner WHERE AAP_key=' . $_POST['txtAAP_KEY'] . '');
            $FeldListe = '';
            
            foreach($Felder AS $Feld)
            {
                $FeldName = substr($Feld,3);
                if(isset($_POST['old'.$FeldName]))
                {
                    // Alten und neuen Wert umformatieren!!
                    $WertNeu=$DB->FeldInhaltFormat($rsAAP->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
                    $WertAlt=$DB->FeldInhaltFormat($rsAAP->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
                    $WertDB=$DB->FeldInhaltFormat($rsAAP->FeldInfo($FeldName,'TypKZ'),$rsAAP->FeldInhalt($FeldName),true);

                    if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
                    {
                        if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
                        {
                            $FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
                        }
                        else
                        {
                            $FeldListe .= ', '.$FeldName.'=';
    
                            if($_POST[$Feld]=='')	// Leere Felder immer als NULL
                            {
                                $FeldListe.=' null';
                            }
                            else
                            {
                                $FeldListe.=$WertNeu;
                            }
                        }
                    }
                }
            }
    
            if(count($FehlerListe)>0)
            {
                $Meldung = str_replace('%1',$rsAAP->FeldInhalt('AAP_USER'),$AWISSprachKonserven['Meldung']['DSVeraendert']);
                foreach($FehlerListe AS $Fehler)
                {
                    $Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
                }
                $Form->Hinweistext($Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
            }
            elseif($FeldListe!='')
            {
                $SQL = 'UPDATE AdressenAnsprechpartner SET';
                $SQL .= substr($FeldListe,1);
                $SQL .= ', AAP_user=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ', AAP_userdat=sysdate';
                $SQL .= ' WHERE AAP_key=0' . $_POST['txtAAP_KEY'] . '';
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403241020,$SQL,awisException::AWIS_ERR_SYSTEM);
                }
            }
        }
    }

    //*************************************************************************
    //** Ansprechpartner-Infos
    //*************************************************************************
    $Felder = $Form->NameInArray($_POST, 'txtAPI_WERT_',1,1);
    if($Felder!='')
    {
        $Felder = explode(';',$Felder);
        $TextKonserven[]=array('Wort','geaendert_auf');
        $TextKonserven[]=array('ADI','ADI_%');
        $TextKonserven[]=array('Meldung','DSVeraendert');
        $TXT_Speichern = $Form->LadeTexte($TextKonserven);
        $FeldListe='';
        $SQL = '';
    
        foreach($Felder as $Feld)
        {
            $FeldTeile = explode('_',$Feld);	// Format: CIN_WERT_<ADT_ID>_<CIN_KEY>
            if($FeldTeile[3]=='')
            {
                $Fehler = '';
                $SQL = 'INSERT INTO AdressenInformationen';
                $SQL .= '(ADI_XXX_KEY, ADI_ADT_KEY, ADI_WERT';
                $SQL .= ',ADI_USER, ADI_USERDAT';
                $SQL .= ')VALUES ('.$AWIS_KEY2;			// AP-ID
                $SQL .= ',' . $DB->FeldInhaltFormat('Z',$FeldTeile[2],true);
                $SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST[$Feld],true);
                $SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
                $SQL .= ',SYSDATE';
                $SQL .= ')';
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403240937,$SQL,awisException::AWIS_ERR_SYSTEM);
                    die();
                }
            }
            else 					// ge�nderter Datensatz
            {
                $FehlerListe = array();
                $UpdateFelder = '';
                $SQL = 'UPDATE AdressenInformationen SET';
                $SQL .= ' ADI_WERT = '.$DB->FeldInhaltFormat('T',$_POST[$Feld]);
                $SQL .= ', ADI_user=\''.$AWISBenutzer->BenutzerName().'\'';
                $SQL .= ', ADI_userdat=sysdate';
                $SQL .= ' WHERE ADI_key=0' . $FeldTeile[3] . '';
                if($DB->Ausfuehren($SQL)===false)
                {
                    throw new awisException('Fehler beim Speichern',201403240938,$SQL,awisException::AWIS_ERR_SYSTEM);
                }
            }  
        }
    }

    //*****************************************************
    // Dokumente speichern
    //*****************************************************
    $Felder = $Form->NameInArray($_POST, 'txtDOC_',1,1);
    $Fehler = '';

    if($Felder!='')
    {
        $Pflichtfelder = array('DOC_DATUM','DOC_BEZEICHNUNG','DOC_BEMERKUNG');
        foreach($Pflichtfelder AS $Pflichtfeld)
        {
            if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
            {
                $Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['GSK'][$Pflichtfeld].'<br>';
            }
        }
        
        // Wurden Fehler entdeckt? => Speichern abbrechen
        if($Fehler!='')
        {
            die('<span class=HinweisText>'.$Fehler.'</span>');
        }
        
    	//$Felder = explode(';',$Felder);
    	$DOC = new awisDokument($DB, $AWISBenutzer);
    	$Zuordnungen=array();
    	$Zuordnungen[]=array('ADR',$_POST['txtADR_KEY']);
//    	$AWIS_KEY2 = $DOC->ErzeugeDokumentenPfad($_POST['txtDOC_BEZEICHNUNG'], 'DOC', $_POST['txtDOC_DATUM'],$_POST['txtDOC_BEMERKUNG'], 'txt', $Zuordnungen);
    	
    	
    	var_dump($_POST['txtDOC_BEZEICHNUNG']);
    	echo"<br><br>";
    	var_dump($_POST['txtDOC_DATUM']);
    	echo"<br><br>";
    	var_dump($_POST['txtDOC_BEMERKUNG']);
    	echo"<br><br>";
    	var_dump($Zuordnungen);
    	echo"<br><br>";
    	var_dump($_POST);
    	echo"<br><br>";
    	var_dump($AWIS_KEY2 = $DOC->ErzeugeDokumentenPfad($_POST['txtDOC_BEZEICHNUNG'], 'DOC', $_POST['txtDOC_DATUM'],$_POST['txtDOC_BEMERKUNG'], 'DOC', $Zuordnungen));
    	
    	//$AWIS_KEY2 = $DOC->DokumentSpeichern($AWIS_KEY2, 'DOC', $_POST['txtDOC_BEZEICHNUNG'], $_POST['txtDOC_DATUM'], $_POST['txtDOC_BEMERKUNG'], $AWISBenutzer->BenutzerID(), $Zuordnungen);
    }
}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>