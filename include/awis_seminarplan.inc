<?php
/**
 * awis_seminarplan.inc
 * 
 * Diese Klasse ist f�r den Import der Seminare bzw. Seminarteilnehmer zust�ndig
 * 
 * @author Thomas Riedl
 * @copyright ATU - Auto-Teile-Unger
 * @version 20100419
 * 
 */
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

class awis_Seminarplan
{
	/**
	 * Datenbankverbindung
	 * 
	 * @var awisDatenbank
	 */
	private $_DB = null;
	
	/**
	 * Aktueller Anwender
	 * @var awisBenutzer
	 */
	private $_AWISBenutzer = null;
	
	/**
	 * Debuglevel f�r Ausgaben an die Konsole
	 * @var int (0=Keine, 9=Alles)
	 */
	private $_DebugLevel = 0;
		
	/**
	 * Importdateiname der Termine
	 * @var string
	 */	
	private $_ImportDateinameTermine = '/win/applicationdata/TC-Academy/Export/Termine.csv';
	
	/**
	 * Importdateiname der Teilnehmer
	 * @var string
	 */	
	private $_ImportDateinameTeilnehmer = '/win/applicationdata/TC-Academy/Export/Teilnehmer.csv';
	
	private $_ImportDateinameLokationen = '/win/applicationdata/TC-Academy/Export/Lokationen.csv';
	
	private $_EMAIL_Infos = array('shuttle@de.atu.eu');
	
	/**
	 * Initialisierung mit einem beliebigen Anwender
	 * @param string $Benutzer (Anmeldename im AWIS)
	 */
	public function __construct($Benutzer='')
	{
		$this->_DB = awisDatenbank::NeueVerbindung('AWIS');
		$this->_AWISBenutzer = awisBenutzer::Init($Benutzer);		
	}
	
	/**
	 * Liest den aktuellen Debuglevel oder setzt ihn neu
	 * @param int $NeuerLevel (0-9)
	 */
	public function DebugLevel($NeuerLevel = null)
	{
		if(!is_null($NeuerLevel))
		{
			$this->_DebugLevel = $this->_DB->FeldInhaltFormat('N0',$NeuerLevel);	
		}
		
		return $this->_DebugLevel;
	}
	
	
	/**
	 * Liest den aktuellen Stand der Benutzerdaten ins AWIS ein
	 * 
	 */
	public function Import_Termine()
	{
		$Werkzeug = new awisWerkzeuge();
		
		$Protokoll['NEURAUM']=0;
		$Protokoll['AENDERUNGRAUM']=0;
		$Protokoll['NEUREGION']=0;
		$Protokoll['AENDERUNGREGION']=0;
		$Protokoll['NEUSEMINAR']=0;
		$Protokoll['AENDERUNGSEMINAR']=0;		
		$Protokoll['FEHLER']=0;						
		
		if(!is_file($this->_ImportDateinameTermine))
		{
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','Keine Datei zum importieren vorhanden...',2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;		
			
			echo 'Keine Datei zum importieren vorhanden...'.PHP_EOL;
			return true;			// Keine Datei da
		}

		$Form = new awisFormular();
		
		if($this->_DebugLevel>5)
		{
			echo '  Seminarimport der Termine startet...'.PHP_EOL;
		}
		
		$fd = fopen($this->_ImportDateinameTermine,'r');
		
		$Zeile = fgets($fd);
		$Zeile = trim(str_replace('"','',$Zeile));	
		
		$Ueberschriften = explode(";",$Zeile);	
		$Ueberschriften = array_flip($Ueberschriften);			

		if(!isset($Ueberschriften['SEMINAR']) or !isset($Ueberschriften['BESCHREIBUNG']) 
			or !isset($Ueberschriften['TRAINING']) or !isset($Ueberschriften['VON'])
			or !isset($Ueberschriften['BIS']) or !isset($Ueberschriften['FREIEPLAETZE'])
			or !isset($Ueberschriften['RAUM']) or !isset($Ueberschriften['ORT'])
			or !isset($Ueberschriften['ZIELGRUPPE']) or !isset($Ueberschriften['TRAINER'])
			or !isset($Ueberschriften['Bereich']))

		{		
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','Falsche �berschrift',2,'','awis@de.atu.eu');			
			die();
		}		
			
		//Alle Seminare auf Status 2 setzen
		$SQL = ' UPDATE SEMINARPLAN SET SEP_STATUS=2 WHERE SEP_SCHULUNGSART=1';
		if($this->_DB->Ausfuehren($SQL)===false)
		{			
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;
		}
			
		//Alle R�ume auf Status 2 setzen
		$SQL = ' UPDATE SEMINARRAEUME SET SER_STATUS=2 WHERE SER_STATUS <> 10';
		if($this->_DB->Ausfuehren($SQL)===false)
		{
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;		
		}
					
		//Alle Regionen auf Status 2 setzen
		$SQL = ' UPDATE SEMINARREGIONEN SET SRG_STATUS=2 WHERE SRG_STATUS <> 10';
		if($this->_DB->Ausfuehren($SQL)===false)
		{		
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;			
		}
								
		while(!feof($fd))
		{
			$Zeile = fgets($fd);	
			$Zeile = trim(str_replace('"','',$Zeile));	
			
			$MeldungRaum='';
			$MeldungRegion='';
		
			$Daten = explode(";",$Zeile);		
			if($Daten[$Ueberschriften['SEMINAR']]!='')
			{			
				if($Daten[$Ueberschriften['RAUM']]!='')
				{							
					//Raum ermitteln
					$SQL = ' SELECT * ';
					$SQL .= ' FROM SEMINARRAEUME ';
					$SQL .= ' WHERE SER_RAUM = '.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RAUM']]));											
					
					$rsSER = $this->_DB->RecordSetOeffnen($SQL);				
								
					//Neuen Raum anlegen
					if ($rsSER->AnzahlDatensaetze()==0)
					{
						$SQL = 'INSERT INTO SEMINARRAEUME';
						$SQL .= '(SER_RAUM, SER_STATUS, SER_USER, SER_USERDAT, SER_RAUMNR';									
						$SQL .= ')VALUES (';
						$SQL .= ' ' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['RAUM']])?$Daten[$Ueberschriften['RAUM']]:''),true));													
						$SQL .= ',1';
						$SQL .= ',\'AWIS\'';									
						$SQL .= ',SYSDATE';
						$SQL .= ','.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RAUMNR']]));
						$SQL .= ')';										
						
						if($this->_DB->Ausfuehren($SQL)===false)
						{
							echo '=> Fehler beim Schreiben des Seminarraums '.$Daten[$Ueberschriften['RAUM']].'.'.PHP_EOL;
							$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
							$Protokoll['FEHLER']++;
										
							continue;				
						}
						else 
						{
							$Protokoll['NEURAUM']++;
						}
					
						$SQL = 'SELECT seq_SER_KEY.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$SERKEY=$rsKey->FeldInhalt('KEY');															
					}
					//Raum vorhanden
					else if ($rsSER->AnzahlDatensaetze()==1)
					{
						$SERKEY=$rsSER->FeldInhalt('SER_KEY');															
						
						//Raum aktuallisieren f�r die Adresszuordnung.
						$SQL = ' UPDATE SEMINARRAEUME SET SER_RAUMNR =' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RAUMNR']])) .  ' WHERE SER_KEY = '.$this->_DB->FeldInhaltFormat('Z',$SERKEY);
						if($this->_DB->Ausfuehren($SQL)===false)
						{
						    echo '=> Fehler beim aktualisieren des Seminarraums '.$Daten[$Ueberschriften['RAUM']].'.'.PHP_EOL;
						    $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
						    $Protokoll['FEHLER']++;
						    	
						    continue;
						}
																	
						//Raum auf Status 1 setzen
						$SQL = ' UPDATE SEMINARRAEUME SET SER_STATUS=1'; 
                        $SQL .= ' WHERE SER_STATUS <> 10 AND SER_KEY = '.$this->_DB->FeldInhaltFormat('Z',$SERKEY);

						if($this->_DB->Ausfuehren($SQL)===false)
						{	
							echo '=> Fehler beim Schreiben des Seminarraums '.$Daten[$Ueberschriften['RAUM']].'.'.PHP_EOL;
							$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
							$Protokoll['FEHLER']++;
							
							continue;
						}					
						else 
						{
							$Protokoll['AENDERUNGRAUM']++;
						}
					}
					else 
					{
						echo '=> Mehrere R�ume vorhanden!'.PHP_EOL;
						$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','Mehrere R�ume vorhanden: '.urlencode($SQL),2,'','awis@de.atu.eu');
						$Protokoll['FEHLER']++;
										
						continue;				
					}
				}
				else 
				{
					$SERKEY=0;
				}			
						
				//Region ermitteln (nur wenn Raum <> Region)
				if ((trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ORT']])) != '') AND
				    (trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ORT']])) <> trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['RAUM']]))))
				{							
					$SQL = ' SELECT * ';
					$SQL .= ' FROM SEMINARREGIONEN ';
					$SQL .= ' WHERE SRG_REGION = '.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ORT']]));			
					
					$rsSRG = $this->_DB->RecordSetOeffnen($SQL);
									
					//Neuen Raum anlegen
					if ($rsSRG->AnzahlDatensaetze()==0)
					{
						$SQL = 'INSERT INTO SEMINARREGIONEN';
						$SQL .= '(SRG_REGION, SRG_STATUS, SRG_USER, SRG_USERDAT';									
						$SQL .= ')VALUES (';
						$SQL .= ' ' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ORT']])?$Daten[$Ueberschriften['ORT']]:''),true));													
						$SQL .= ',1';
						$SQL .= ',\'AWIS\'';						
						$SQL .= ',SYSDATE';
						$SQL .= ')';				
						
						if($this->_DB->Ausfuehren($SQL)===false)
						{
							echo '=> Fehler beim Schreiben der Seminarregion '.$Daten[$Ueberschriften['ORT']].'.'.PHP_EOL;
							$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
							$Protokoll['FEHLER']++;
											
							continue;
						}
						else 
						{
							$Protokoll['NEUREGION']++;
						}
						
						$SQL = 'SELECT seq_SRG_KEY.CurrVal AS KEY FROM DUAL';
						$rsKey = $this->_DB->RecordSetOeffnen($SQL);
						$SRGKEY=$rsKey->FeldInhalt('KEY');															
					}
					//Region vorhanden
					else if ($rsSRG->AnzahlDatensaetze()==1)
					{
						$SRGKEY=$rsSRG->FeldInhalt('SRG_KEY');									
	
						//Region auf Status 1 setzen
						$SQL = ' UPDATE SEMINARREGIONEN SET SRG_STATUS=1 WHERE SRG_STATUS <> 10 AND SRG_KEY = '.$this->_DB->FeldInhaltFormat('Z',$SRGKEY);
						if($this->_DB->Ausfuehren($SQL)===false)
						{				
							echo '=> Fehler beim Schreiben der Seminarregion '.$Daten[$Ueberschriften['ORT']].'.'.PHP_EOL;
							$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
							$Protokoll['FEHLER']++;
						}				
						else 
						{
							$Protokoll['AENDERUNGREGION']++;
						}
					}
					else 
					{
						echo '=> Mehrere Regionen vorhanden!'.PHP_EOL;
						$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','Mehrere Regionen: '.urlencode($SQL),2,'','awis@de.atu.eu');
						$Protokoll['FEHLER']++;
											
						continue;	
					}
				}
				else 
				{
					$SRGKEY=0;
				}	
							
				$SQL = ' SELECT * ';
				$SQL .= ' FROM SEMINARPLAN ';
				$SQL .= ' WHERE SEP_SEMINAR = '.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['SEMINAR']]));			
				$SQL .= ' AND SEP_TRAINING = '.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['TRAINING']]));						
				$SQL .= ' AND SEP_SCHULUNGSART = 1';;						
							
				$rsSEP = $this->_DB->RecordSetOeffnen($SQL);

				//Neues Seminar anlegen
				if($rsSEP->AnzahlDatensaetze()==0)
				{			
					$SQL = 'INSERT INTO SEMINARPLAN';
					$SQL .= '(SEP_SEMINAR, SEP_BESCHREIBUNG, SEP_TRAINING, SEP_VON, SEP_BIS,';
					$SQL .= ' SEP_FREIEPLAETZE, SEP_SER_KEY, SEP_SRG_KEY, SEP_ZIELGRUPPE, SEP_TRAINER, SEP_SEMINARSTATUS, SEP_STATUS, SEP_BEREICH, SEP_ESS, SEP_USER, SEP_USERDAT';
					$SQL .= ')VALUES (';
					$SQL .= '' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['SEMINAR']],true));
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['BESCHREIBUNG']])?$Daten[$Ueberschriften['BESCHREIBUNG']]:''),true));
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['TRAINING']],true));
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('DU',(isset($Daten[$Ueberschriften['VON']])?$Daten[$Ueberschriften['VON']].' ' . $Daten[$Ueberschriften['ANFANG']]:''),true));
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('DU',(isset($Daten[$Ueberschriften['BIS']])?$Daten[$Ueberschriften['BIS']]. ' ' . $Daten[$Ueberschriften['ENDE']]:''),true));
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('Z',(isset($Daten[$Ueberschriften['FREIEPLAETZE']])?$Daten[$Ueberschriften['FREIEPLAETZE']]:''),true));				
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('Z',$SERKEY,true));				
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('Z',$SRGKEY,true));				
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ZIELGRUPPE']])?$Daten[$Ueberschriften['ZIELGRUPPE']]:''),true));					
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['TRAINER']])?$Daten[$Ueberschriften['TRAINER']]:''),true));					
					$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['STATUS']])?$Daten[$Ueberschriften['STATUS']]:''),true));
                    $SQL .= ',1';
                    $SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['Bereich']])?$Daten[$Ueberschriften['Bereich']]:''),true));
                    $SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('Z',(isset($Daten[$Ueberschriften['ESS']])?$Daten[$Ueberschriften['ESS']]:1),true));
					$SQL .= ',\'AWIS\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					if($this->_DB->Ausfuehren($SQL)===false)
					{
						echo '=> Fehler beim Schreiben des Seminars '.$Daten[$Ueberschriften['SEMINAR']].'.'.PHP_EOL;
						$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler : '.urlencode($SQL),2,'','awis@de.atu.eu');
						$Protokoll['FEHLER']++;
											
						continue;
					}
					else 
					{
						$Protokoll['NEUSEMINAR']++;
					}
					
					$SQL = 'SELECT seq_SEP_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $this->_DB->RecordSetOeffnen($SQL);
					$KEY2=$rsKey->FeldInhalt('KEY');											
				}
				//Vorhandenes Seminar updaten
				else if ($rsSEP->AnzahlDatensaetze()==1)
				{
					$KEY2=$rsSEP->FeldInhalt('SEP_KEY');											
					
					$SQL = 'UPDATE SEMINARPLAN SET';
					$SQL .= ' SEP_BESCHREIBUNG = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['BESCHREIBUNG']])?$Daten[$Ueberschriften['BESCHREIBUNG']]:''),true));
					$SQL .= ', SEP_VON = '.trim($this->_DB->FeldInhaltFormat('DU',(isset($Daten[$Ueberschriften['VON']])?$Daten[$Ueberschriften['VON']] .' ' . $Daten[$Ueberschriften['ANFANG']]:''),true));
					$SQL .= ', SEP_BIS = '.trim($this->_DB->FeldInhaltFormat('DU',(isset($Daten[$Ueberschriften['BIS']])?$Daten[$Ueberschriften['BIS']]. ' ' . $Daten[$Ueberschriften['ENDE']]:''),true));
					$SQL .= ', SEP_FREIEPLAETZE = '.trim($this->_DB->FeldInhaltFormat('Z',(isset($Daten[$Ueberschriften['FREIEPLAETZE']])?$Daten[$Ueberschriften['FREIEPLAETZE']]:''),true));
					$SQL .= ', SEP_SER_KEY = '.($this->_DB->FeldInhaltFormat('Z',$SERKEY,true));				
					$SQL .= ', SEP_SRG_KEY = '.($this->_DB->FeldInhaltFormat('Z',$SRGKEY,true));				
					$SQL .= ', SEP_ZIELGRUPPE = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ZIELGRUPPE']])?$Daten[$Ueberschriften['ZIELGRUPPE']]:''),true));
					$SQL .= ', SEP_TRAINER = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['TRAINER']])?$Daten[$Ueberschriften['TRAINER']]:''),true));
					$SQL .= ', SEP_SEMINARSTATUS = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['STATUS']])?$Daten[$Ueberschriften['STATUS']]:''),true));
                    $SQL .= ', SEP_BEREICH = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['Bereich']])?$Daten[$Ueberschriften['Bereich']]:''),true));
                    $SQL .= ', SEP_ESS = '.trim($this->_DB->FeldInhaltFormat('Z',(isset($Daten[$Ueberschriften['ESS']])?$Daten[$Ueberschriften['ESS']]:1),true));
					$SQL .= ', SEP_STATUS = 1';
					$SQL .= ', SEP_USER = \'AWIS\'';
					$SQL .= ', SEP_USERDAT = SYSDATE';
					$SQL .= ' WHERE SEP_KEY ='.$KEY2;

					if($this->_DB->Ausfuehren($SQL)===false)
					{
						echo '=> Fehler beim Schreiben des Seminars '.$Daten[$Ueberschriften['SEMINAR']].'.'.PHP_EOL;
						$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
						$Protokoll['FEHLER']++;
					}
					else 
					{
						$Protokoll['AENDERUNGSEMINAR']++;
					}
				}
				else 
				{
					echo '=> Mehrere Seminare vorhanden!'.PHP_EOL;
					$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','Mehrere Seminare vorhanden: '.urlencode($SQL),2,'','awis@de.atu.eu');
					$Protokoll['FEHLER']++;
											
					continue;	
				}
			}
		}
		
		//Alle nicht vorhandenen Seminare auf Status 3 (inaktiv) setzen
		$SQL = ' UPDATE SEMINARPLAN SET SEP_STATUS=3 WHERE SEP_STATUS=2 AND SEP_SCHULUNGSART=1';
		if($this->_DB->Ausfuehren($SQL)===false)
		{		
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;			
		}
		
		//Alle nicht vorhandenen R�ume auf Status 3 (inaktiv) setzen
		$SQL = ' UPDATE SEMINARRAEUME SET SER_STATUS=3 WHERE SER_STATUS=2';
		if($this->_DB->Ausfuehren($SQL)===false)
		{		
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;			
		}
		
		//Alle nicht vorhandenen Regionen auf Status 3 (inaktiv) setzen
		$SQL = ' UPDATE SEMINARREGIONEN SET SRG_STATUS=3 WHERE SRG_STATUS=2';
		if($this->_DB->Ausfuehren($SQL)===false)
		{	
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARPLANIMPORT','SQL-Fehler beim Import: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;				
		}
	
		$Text = 'Neue R�ume: '.$Protokoll['NEURAUM'].chr(10);
		$Text.= '�nderungen R�ume: '.$Protokoll['AENDERUNGRAUM'].chr(10);
		$Text.= 'Neue Regionen: '.$Protokoll['NEUREGION'].chr(10);
		$Text.= '�nderungen Regionen: '.$Protokoll['AENDERUNGREGION'].chr(10);
		$Text.= 'Neue Seminare: '.$Protokoll['NEUSEMINAR'].chr(10);
		$Text.= '�nderungen Seminare: '.$Protokoll['AENDERUNGSEMINAR'].chr(10);
		$Text.= 'FEHLER: '.$Protokoll['FEHLER'].chr(10);
	}	

	
	public function Import_Teilnehmer()
	{
		$Werkzeug = new awisWerkzeuge();
		
		$Protokoll['NEUTEILNEHMER']=0;
		$Protokoll['AENDERUNGTEILNEHMER']=0;		
		$Protokoll['FEHLER']=0;
		
		if(!is_file($this->_ImportDateinameTeilnehmer))
		{
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','Keine Datei zum importieren vorhanden...',2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;
			
			echo 'Keine Datei zum importieren vorhanden...'.PHP_EOL;
			return true;			// Keine Datei da
		}
		
		$Form = new awisFormular();
			
		if($this->_DebugLevel>5)
		{
			echo '  Seminarimport der Teilnehmer startet...'.PHP_EOL;
		}
						
		$fd = fopen($this->_ImportDateinameTeilnehmer,'r');
			
		$Zeile = fgets($fd);	
		$Zeile = trim(str_replace('"','',$Zeile));	
		
		$Ueberschriften = explode(";",$Zeile);	
		$Ueberschriften = array_flip($Ueberschriften);	
				
		if(!isset($Ueberschriften['ID']) or !isset($Ueberschriften['NACHNAME']) 
			or !isset($Ueberschriften['VORNAME']) or !isset($Ueberschriften['STATUS'])
			or !isset($Ueberschriften['FILIALNR']) or !isset($Ueberschriften['FILIALE'])
			or !isset($Ueberschriften['SEMINAR']) or !isset($Ueberschriften['TRAINING'])
			or !isset($Ueberschriften['ANGEMELDETAM']))		
		{	
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','Falsche �berschrift',2,'','awis@de.atu.eu');			
			die();
		}					
			
		//Alle Mitarbeiter auf Status 2 setzen
		$SQL = 'UPDATE SEMINARTEILNEHMER SET SET_STATUSTEILNEHMER = 2 ';
		$SQL.= ' WHERE SET_SEP_KEY NOT IN (SELECT SEP_KEY FROM SEMINARPLAN WHERE SEP_SCHULUNGSART = 2)';
		if($this->_DB->Ausfuehren($SQL)===false)
		{
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;
		}				
	
		while(!feof($fd))
		{
			$Zeile = fgets($fd);
			$Zeile = trim(str_replace('"','',$Zeile));	
					
			$Daten = explode(";",$Zeile);		
			if($Daten[$Ueberschriften['SEMINAR']]!='' AND $Daten[$Ueberschriften['TRAINING']]!='' AND ($Daten[$Ueberschriften['ID']]!= '' AND $Daten[$Ueberschriften['NACHNAME']] != '' AND $Daten[$Ueberschriften['VORNAME']] != ''))
			{			
				$SQL = ' SELECT * ';
				$SQL .= ' FROM SEMINARPLAN ';
				$SQL .= ' WHERE SEP_SEMINAR = '.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['SEMINAR']]));			
				$SQL .= ' AND SEP_TRAINING = '.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['TRAINING']]));						
				$SQL .= ' AND SEP_SCHULUNGSART = 1';;						
						
				$rsSEP = $this->_DB->RecordSetOeffnen($SQL);
				
				//Seminar vorhanden
				if($rsSEP->AnzahlDatensaetze()==1)
				{								
					//Pr�fen, ob Mitarbeiter schon zu dem Seminar zugeordnet ist
					$SQL = 'SELECT *';
					$SQL.= ' FROM SEMINARTEILNEHMER';
					$SQL.= ' WHERE SET_SEP_KEY = '.$this->_DB->FeldInhaltFormat('Z',$rsSEP->FeldInhalt('SEP_KEY'));
					$SQL.= ' AND SET_PER_NR = '.trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['ID']]));				
					
					$rsSET = $this->_DB->RecordSetOeffnen($SQL);
										
					if ($rsSET->AnzahlDatensaetze()==0)
					{			
						//Seminarteilnehmer hinzuf�gen						
						$SQL = 'INSERT INTO SEMINARTEILNEHMER';
						$SQL .= '(SET_SEP_KEY, SET_PER_NR, SET_NACHNAME, SET_VORNAME, SET_ANMELDETAG, SET_STATUS,';
						$SQL .= ' SET_FIL_NR, SET_FILIALE, SET_STATUSTEILNEHMER, SET_USER, SET_USERDAT,SET_TEILNAHME ';				
						$SQL .= ')VALUES (';
						$SQL .= '' . ($this->_DB->FeldInhaltFormat('Z',$rsSEP->FeldInhalt('SEP_KEY'),true));
						$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['ID']])?$Daten[$Ueberschriften['ID']]:''),true));
						$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['NACHNAME']],true));
						$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['VORNAME']],true));
						$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('D',$Daten[$Ueberschriften['ANGEMELDETAM']],true));
						$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['STATUS']],true));
						$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['FILIALNR']],true));
						$SQL .= ' ,' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['FILIALE']],true));								
						$SQL .= ',1';
						$SQL .= ',\'AWIS\'';
						$SQL .= ',SYSDATE';
						$SQL .= ', -1';
						$SQL .= ')';
											
						if($this->_DB->Ausfuehren($SQL)===false)
						{					
							echo '=> Fehler beim Schreiben der Seminarteilnehmer '.$Daten[$Ueberschriften['ID']].'.'.PHP_EOL;
							$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
							$Protokoll['FEHLER']++;
							
							continue;
						}				
						else 
						{
							$Protokoll['NEUTEILNEHMER']++;					
						}
					}
					else if ($rsSET->AnzahlDatensaetze()==1)
					{
						$KEY2=$rsSET->FeldInhalt('SET_KEY');											
						
						$SQL = 'UPDATE SEMINARTEILNEHMER SET';
						$SQL .= ' SET_NACHNAME = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['NACHNAME']])?$Daten[$Ueberschriften['NACHNAME']]:''),true));
						$SQL .= ', SET_VORNAME = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['VORNAME']])?$Daten[$Ueberschriften['VORNAME']]:''),true));
						$SQL .= ', SET_ANMELDETAG = '.trim($this->_DB->FeldInhaltFormat('D',(isset($Daten[$Ueberschriften['ANGEMELDETAM']])?$Daten[$Ueberschriften['ANGEMELDETAM']]:''),true));
						$SQL .= ', SET_STATUS = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['STATUS']])?$Daten[$Ueberschriften['STATUS']]:''),true));
						$SQL .= ', SET_FIL_NR = '.trim($this->_DB->FeldInhaltFormat('N0',(isset($Daten[$Ueberschriften['FILIALNR']])?$Daten[$Ueberschriften['FILIALNR']]:''),true));										
						$SQL .= ', SET_FILIALE = '.trim($this->_DB->FeldInhaltFormat('T',(isset($Daten[$Ueberschriften['FILIALE']])?$Daten[$Ueberschriften['FILIALE']]:''),true));										
						$SQL .= ', SET_STATUSTEILNEHMER = 1';
						$SQL .= ' WHERE SET_KEY ='.$KEY2;
						
						if($this->_DB->Ausfuehren($SQL)===false)
						{
							echo '=> Fehler beim Schreiben der Seminarteilnehmer '.$Daten[$Ueberschriften['ID']].'.'.PHP_EOL;
							$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
							$Protokoll['FEHLER']++;							
						}
						else 
						{
							$Protokoll['AENDERUNGTEILNEHMER']++;
						}
					}
					else 
					{
						echo 'Seminarteilnehmer mehrfach vorhanden!!!';
						$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','Seminarteilnehmer mehrfach vorhanden',2,'','awis@de.atu.eu');
						$Protokoll['FEHLER']++;		
						
						continue;				
					}
				}
				else 
				{
					echo 'Seminar '. $Daten[$Ueberschriften['SEMINAR']] .', Training '. $Daten[$Ueberschriften['TRAINING']]. ' nicht oder mehrfach vorhanden!!!';
					$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','Seminar '. $Daten[$Ueberschriften['SEMINAR']] .', Training '. $Daten[$Ueberschriften['TRAINING']]. ' nicht oder mehrfach vorhanden!!!',2,'','awis@de.atu.eu');
					$Protokoll['FEHLER']++;		
						
					continue;				
				}								
			}					
		}
		
		//Alle nicht vorhandenen Seminare auf Status 3 (inaktiv) setzen
		$SQL = ' UPDATE SEMINARTEILNEHMER SET SET_STATUSTEILNEHMER=3 WHERE SET_STATUSTEILNEHMER=2';
		$SQL.= ' AND SET_SEP_KEY NOT IN (SELECT SEP_KEY FROM SEMINARPLAN WHERE SEP_SCHULUNGSART = 2)';
		if($this->_DB->Ausfuehren($SQL)===false)
		{				
			$Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARTEILNEHMERIMPORT','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
			$Protokoll['FEHLER']++;
		}
		

		$Text= 'Neue Teilnehmer: '.$Protokoll['NEUTEILNEHMER'].chr(10);
		$Text.= '�nderungen Teilnehmer: '.$Protokoll['AENDERUNGTEILNEHMER'].chr(10);
		$Text.= 'FEHLER: '.$Protokoll['FEHLER'].chr(10);

	}

    public function Import_Lokationen()
    {
        $Werkzeug = new awisWerkzeuge();
    
        $Protokoll['MERGELOKATIONEN']=0;
        $Protokoll['FEHLER']=0;
    
        if(!is_file($this->_ImportDateinameLokationen))
        {
            $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARLOKATIONENIMPORT','Keine Datei zum importieren vorhanden...',2,'','awis@de.atu.eu');
            $Protokoll['FEHLER']++;
            	
            echo 'Keine Datei zum importieren vorhanden...'.PHP_EOL;
            return true;			// Keine Datei da
        }
    
        $Form = new awisFormular();
        	
        if($this->_DebugLevel>5)
        {
            echo '  Seminarimport der Lokationen startet...'.PHP_EOL;
        }
    
        $fd = fopen($this->_ImportDateinameLokationen,'r');
        	
        $Zeile = fgets($fd);
        $Zeile = trim(str_replace('"','',$Zeile));
        
        $Ueberschriften = explode(";",$Zeile);
        $Ueberschriften = array_flip($Ueberschriften);
    
        if(!isset($Ueberschriften['Location']) or !isset($Ueberschriften['Street'])
            or !isset($Ueberschriften['Address']) or !isset($Ueberschriften['Country'])
            or !isset($Ueberschriften['Zip']) or !isset($Ueberschriften['City'])
            or !isset($Ueberschriften['LocNo']) or !isset($Ueberschriften['RoomNo']) 
            or !isset($Ueberschriften['Room']) or !isset($Ueberschriften['Region']) 
            or !isset($Ueberschriften['RegNo']))
        {
            $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SEMINARLOKATIONIMPORT','Falsche �berschrift',2,'','awis@de.atu.eu');
            die();
        }

        while(!feof($fd))
        {
            $Zeile = fgets($fd);
            $Zeile = trim(str_replace('"','',$Zeile));
            $Daten = explode(';',$Zeile);
            if($Zeile != '')
            {
                if($Daten[$Ueberschriften['Location']]!='' AND $Daten[$Ueberschriften['LocNo']]!='' AND ($Daten[$Ueberschriften['RoomNo']]!= ''))
                {
                    $SQL  ='merge INTO seminarraumadressen SPA USING';
                    $SQL .='(';
                    $SQL .='  SELECT';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['LocNo']],true))  . ' AS SPA_LOKATIONSID,';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('N0',$Daten[$Ueberschriften['RoomNo']],true)) . ' AS SPA_RAUMID,';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['Location']],true)) . ' AS SPA_LOKATIONSNAME,';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['Address']],true)) . ' AS SPA_ADRESSE,';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['Street']],true)) . ' AS SPA_STRASSE,';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['Country']],true)) . ' AS SPA_LAND,';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['Zip']],true)) . ' AS SPA_POSTLEITZAHL,';
                    $SQL .='    ' . trim($this->_DB->FeldInhaltFormat('T',$Daten[$Ueberschriften['City']],true)) . ' AS SPA_ORT';
                    $SQL .='  FROM';
                    $SQL .='    dual';
                    $SQL .=')';
                    $SQL .='imp ON';
                    $SQL .='(';
                    $SQL .='  imp.SPA_RAUMID = SPA.SPA_RAUMID';
                    $SQL .=')';
                    $SQL .='WHEN matched THEN';
                    $SQL .='  UPDATE';
                    $SQL .='  SET';
                    $SQL .='    SPA.SPA_LOKATIONSNAME = imp.SPA_LOKATIONSNAME,';
                    $SQL .='    SPA.SPA_ADRESSE       = imp.SPA_ADRESSE,';
                    $SQL .='    SPA.SPA_STRASSE       = imp.SPA_STRASSE,';
                    $SQL .='    SPA.SPA_LAND          = imp.SPA_LAND,';
                    $SQL .='    SPA.SPA_POSTLEITZAHL  = imp.SPA_POSTLEITZAHL,';
                    $SQL .='    SPA.SPA_ORT           = imp.SPA_ORT';
                    $SQL .='    WHEN NOT MATCHED THEN';
                    $SQL .='  INSERT';
                    $SQL .='    (';
                    $SQL .='      SPA_LOKATIONSID,';
                    $SQL .='      SPA_RAUMID,';
                    $SQL .='      SPA_LOKATIONSNAME,';
                    $SQL .='      SPA_ADRESSE,';
                    $SQL .='      SPA_STRASSE,';
                    $SQL .='      SPA_LAND,';
                    $SQL .='      SPA_POSTLEITZAHL,';
                    $SQL .='      SPA_ORT   ';
                    $SQL .='    )';
                    $SQL .='    VALUES';
                    $SQL .='    (';
                    $SQL .='      imp.SPA_LOKATIONSID,';
                    $SQL .='      imp.SPA_RAUMID,';
                    $SQL .='      imp.SPA_LOKATIONSNAME,';
                    $SQL .='      imp.SPA_ADRESSE,';
                    $SQL .='      imp.SPA_STRASSE,';
                    $SQL .='      imp.SPA_LAND,';
                    $SQL .='      imp.SPA_POSTLEITZAHL,';
                    $SQL .='      imp.SPA_ORT';
                    $SQL .='    )';

                    if($this->_DB->Ausfuehren($SQL)===false)
                    {
                        echo '=> Fehler beim Schreiben der SeminarLokationen '.$Daten[$Ueberschriften['LocNo']].'.'.PHP_EOL;
                        $Werkzeug->EMail($this->_EMAIL_Infos,'ERROR SeminarLokationenImport','SQL-Fehler: '.urlencode($SQL),2,'','awis@de.atu.eu');
                        $Protokoll['FEHLER']++;
                         
                        continue;
                    }
                    else
                    {
                        $Protokoll['MERGELOKATIONEN']++;
                    }
                }              
            }              
        }
        
        $Text= 'Gemergte Lokationen: '.$Protokoll['MERGELOKATIONEN'].chr(10);
        $Text.= 'FEHLER: '.$Protokoll['FEHLER'].chr(10);

    }
    
    
    /**
     * �ffnet eine neue Transaktion
     */
    public function TransaktionBegin()
    {
    	$this->_DB->TransaktionBegin();
    }
    
    /**
     * Commited die Transaktion
     */
    public function TransaktionCommit()
    {
    	$this->_DB->TransaktionCommit();
    }
    
    /**
     * Rollbacked die Transaktion
     */
    public function TransaktionRollback()
    {
    	$this->_DB->TransaktionRollback();
    }
    
    
    
}



?>
