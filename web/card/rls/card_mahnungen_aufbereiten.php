<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

?>
<html>
<head>
<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript"></script>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
?>
	try{
	parent.card_ladezeit.startzeit();	//Ladezeitmessung
	} catch(e){}
<?php } ?>



function init()		//Aufruf �ber <body onload="init();">
{
	<?php
	if($intZeitmessung)
	{
	?>parent.card_ladezeit.endezeit();	//Zeitmessung
	<?php } ?>
	document.ondblclick = zoom;
	
	
	<?php
	//Mahnungen als gedruckt buchen
	if(isset($intDruckBuch)&&$intDruckBuch)
	{
		$strSql = "update mahnungen_aufbereitet set maa_verarbeitet=2,maa_aend_benutzer='buchdruck: ".$PHP_AUTH_USER."',maa_aend_datum=sysdate where maa_verarbeitet=1 and maa_mahnstufe<3"; 
		
		$arrDruckBuch = abfrage("ora",$strSql);
		if($arrDruckBuch["zeilen"])
			echo "alert('".$arrDruckBuch["zeilen"]." Datens�tze erfolgreich als \"gedruckt\" gebucht!');";
	}
	
	?>
}


function gedruckt()
{
	var bolRueck = confirm("Sollen die Datens�tze wirklich als \"gedruckt\" gebucht werden?");
	if(bolRueck)
	{
		self.location.href='<?php echo $PHP_SELF; ?>?intDruckBuch=1';
	}

}
</script>
<title>Mahnungen aufbereiten</title>
<?php
include("card/css/css_card_master.php");	//Style-Sheets
?>
</head>
<body onload="init();">
<table cellspacing="0" cellpadding="0" width="100%" height="100%"><tr>
<td align="center" valign="middle">
<table id="tabelleaussen"><tr><td align="center" valign="middle">

<?php


//Mahnungen buchen
if(isset($intMahnBuch)&&$intMahnBuch)	
{
	//$dblBetragMahngebuehren + $intMahnFrist werden in card_allgparam.inc.php gesetzt

	$strProz="MAHNUNGEN_AUFBEREITEN ( '".$PHP_AUTH_USER."',".$dblBetragMahngebuehren.",".$intMahnFrist.",:intErfolg,:strRueckgabetext )";			//Name der aufzurufenden Oracle-Prozedur
	
	$arrParameter=array("intErfolg"=>1,"strRueckgabetext"=>100);	//Array mit Bind-Variablen
	$arrRueck=oracleprozedur($strProz,$arrParameter);				//Oracle-Prozedur aufrufen, R�ckgabewerte werden auf ass. Array zugewiesen
	
	echo "<span class=\"titel\">";
	
	if($arrRueck["intErfolg"])
	{
		echo $arrRueck["strRueckgabetext"]."<br>";
	}
	else
	{
		echo "Vorgang nicht erfolgreich, bitte wiederholen!<br>Fehlertext: ".$arrRueck["strRueckgabetext"]."<br>";
	}
	
	echo "</span>";
}

//Ermittelt Anzahl Datens�tze in mahnungen_aufbereitet, welche auf 0 stehen, d.h. noch nicht gebucht worden sind

$strSql = "select count(*) anzahl from mahnungen_aufbereitet where maa_verarbeitet=0"; 
$arrMahnungenAufbereitet = abfrage("ora",$strSql);

$intAnzahlMahnungenAufbereitet=$arrMahnungenAufbereitet["trefferarray"]["ANZAHL"][0];

if($intAnzahlMahnungenAufbereitet>0)
{
	if($intAnzahlMahnungenAufbereitet==1)
		$strAusgabe = $intAnzahlMahnungenAufbereitet. " Datensatz kann gebucht werden!";
	else
		$strAusgabe = $intAnzahlMahnungenAufbereitet. " Datens&auml;tze k&ouml;nnen gebucht werden!";
	?>
	<span class="titel"><?php echo $strAusgabe; ?></span><br><br>
	<button type="button" onclick="self.location.href='<?php echo $PHP_SELF; ?>?intMahnBuch=1';">Mahnungen buchen</button>
	<?php
}
else	//keine Datens�tze, welche auf 0 stehen (zum Buchen anstehen)
{
	//Ermittelt Anzahl Datens�tze in mahnungen_aufbereitet, welche auf 1 stehen, d.h. noch nicht gedruckt worden sind
	$strSql = "select count(anzahl) mahnungen,sum(anzahl) datensaetze
				from(
				select count(*) anzahl from mahnungen_aufbereitet where maa_verarbeitet=1 and maa_mahnstufe<3
				group by maa_mahnstufe,maa_zahlungsziel,maa_kunden_nr
				)"; 
	
	$arrMahnungenGebucht = abfrage("ora",$strSql);

	$intAnzahlMahnungenGebucht=$arrMahnungenGebucht["trefferarray"]["MAHNUNGEN"][0];
	$intAnzahlDsGebucht=$arrMahnungenGebucht["trefferarray"]["DATENSAETZE"][0];
	
	if($intAnzahlMahnungenGebucht>0)
	{
		if(!(isset($intMahnBuch)&&$intMahnBuch))
		{
			$strDatensatz='';
			
			if($intAnzahlDsGebucht==1)
			{
				$strDatensatz="(".$intAnzahlDsGebucht." Datensatz) ";
			}
			elseif($intAnzahlDsGebucht>1)
			{
				$strDatensatz="(".$intAnzahlDsGebucht." Datens&auml;tze) ";
			}
			
			if($intAnzahlMahnungenGebucht==1)
			{
				$strAusgabe = $intAnzahlMahnungenGebucht. " Mahnung ".$strDatensatz."kann gedruckt werden!";
			}
			else
			{
				$strAusgabe = $intAnzahlMahnungenGebucht. " Mahnungen ".$strDatensatz."k&ouml;nnen gedruckt werden!";
			}
			?>
			<span class="titel"><?php echo $strAusgabe; ?></span><br>
			<?php
		}
		?>
		<br><button type="button" onclick="self.location.href='card_mahnungen_drucken.php';">Mahnungen drucken</button>
		<br><br><button type="button" onclick="gedruckt();">Mahnungen als gedruckt buchen</button>
		<?php
	}
	else	//keine Datens�tze, welche auf 1 stehen (zum Drucken anstehen)
	{
		//Alle Datens�tze deren Zahlungsziel einen Tag vor dem aktuellen Datum liegen, welche nicht bereits auf Mahnstufe 4 (Abgabe an Inkasseo) stehen
		$strSql = "select * from view_offene_rechnungen where sign(trunc(sysdate,'dd')-trunc(vor_zahlungsziel,'dd'))>0 and vor_gesamtbetrag<0 and vor_mahnstufe<4";  
		$arrOffRec = abfrage("ora",$strSql);
		$intZeilen = $arrOffRec["zeilen"];
		
		if($intZeilen)
		{
			if(isset($intMahnAufb)&&$intMahnAufb)
			{
				$intZaehler=0;
				$arrKundennr=array();
				
				for($i=0;$i<$intZeilen;$i++)
				{
					$arrOffRecTreffer = $arrOffRec["trefferarray"];
					$strKundennr=$arrOffRecTreffer["VOR_KUNDEN_NR"][$i];
					$strPan=$arrOffRecTreffer["VOR_GEKAUFTAUFPAN"][$i];
					
					if(empty($arrKundennr[$strKundennr]))
					{
						$strSql='select a."Kunden_Art",b."Firma",b."Abteilung",b."Titel",b."Name",b."Vorname",b."Strasse",b."PLZ",b."Ort",b."Geschlecht",c."Post_Id" 
						from '.$tabKunden.' as a left outer join ('.$tabAdressen.' as b left outer join '.$tabLand.' as c on b."Land_Nr"=c."Land_Nr") 
						on a."Kunden_Nr"=b."Kunden_Nr" and b."Adress_Art"=\'01\'
						where a."Kunden_Nr"=\''.$strKundennr.'\'';
						
						//"Adress_Art"=\'01\' entspricht Hauptkarte
						
						$arrAdressen=abfrage("db2",$strSql);
						$arrAdressen=$arrAdressen["trefferarray"];
						
						$arrKundennr[$strKundennr]=array();
						$arrKundennr[$strKundennr]["Kunden_Art"]=$arrAdressen["Kunden_Art"][0];
						$arrKundennr[$strKundennr]["Firma"]=$arrAdressen["Firma"][0];
						$arrKundennr[$strKundennr]["Abteilung"]=$arrAdressen["Abteilung"][0];
						$arrKundennr[$strKundennr]["Titel"]=$arrAdressen["Titel"][0];
						$arrKundennr[$strKundennr]["Name"]=$arrAdressen["Name"][0];
						$arrKundennr[$strKundennr]["Vorname"]=$arrAdressen["Vorname"][0];
						$arrKundennr[$strKundennr]["Strasse"]=$arrAdressen["Strasse"][0];
						$arrKundennr[$strKundennr]["PLZ"]=$arrAdressen["PLZ"][0];
						$arrKundennr[$strKundennr]["Ort"]=$arrAdressen["Ort"][0];
						$arrKundennr[$strKundennr]["Geschlecht"]=$arrAdressen["Geschlecht"][0];
						$arrKundennr[$strKundennr]["Post_Id"]=$arrAdressen["Post_Id"][0];
					}
					
					$strSql='select coalesce("Name",\'\') as "Name", coalesce("Vorname",\'\') as "Vorname" from '.$tabAdressen.' where PAN=\''.$strPan.'\'';
					$arrEinkaeufer=abfrage("db2",$strSql);
					$arrEinkaeufer=$arrEinkaeufer["trefferarray"];
					$strEinkaeufer=trim($arrEinkaeufer["Name"][0]." ".$arrEinkaeufer["Vorname"][0]);
					
					//Eintrag Daten in Tabelle Mahnungen_aufbereitet
					
					$strSql="insert into mahnungen_aufbereitet (MAA_ID, MAA_RNR, MAA_RG_DATUM, MAA_KUNDEN_NR, MAA_BETRAG_RECHNUNGEN, MAA_BETRAG_RECHNUNGEN_OFF,
					MAA_BETRAG_MAHNUNGEN, MAA_BETRAG_MAHNUNGEN_OFF, MAA_BETRAG_GESAMT, MAA_SKONTOABZUG,
					MAA_ZAHLUNGSZIEL, MAA_MAHNUNG1_DATUM, MAA_MAHNUNG2_DATUM, MAA_MAHNUNG3_DATUM, MAA_INKASSO_DATUM, MAA_MAHNSTUFE,
					MAA_GEKAUFTAUFPAN, MAA_EINKAEUFER, MAA_KUNDENART, MAA_FIRMA, MAA_ABTEILUNG, MAA_TITEL, MAA_NAME, MAA_VORNAME, MAA_STRASSE, MAA_PLZ, MAA_ORT, MAA_LAND,
					MAA_GESCHLECHT, MAA_AEND_BENUTZER, MAA_AEND_DATUM, MAA_LOESCH,MAA_VERARBEITET)
					values (seq_maa_id.nextval,'".str_replace("'","''",$arrOffRecTreffer["VOR_RNR"][$i])."','".$arrOffRecTreffer["VOR_RGDATUM"][$i]."','".str_replace("'","''",$arrOffRecTreffer["VOR_KUNDEN_NR"][$i])."',
					".punktersetztkomma($arrOffRecTreffer["VOR_FORDERUNG"][$i]).",".punktersetztkomma($arrOffRecTreffer["VOR_BETRAG_RECHNUNGEN"][$i]).",
					".punktersetztkomma($arrOffRecTreffer["VOR_MG_MINUS"][$i]).",".punktersetztkomma($arrOffRecTreffer["VOR_BETRAG_MAHNGEBUEHREN"][$i]).",
					".punktersetztkomma($arrOffRecTreffer["VOR_GESAMTBETRAG"][$i]).",".punktersetztkomma($arrOffRecTreffer["VOR_SKONTOABZUG"][$i]).",
					'".$arrOffRecTreffer["VOR_ZAHLUNGSZIEL"][$i]."','".$arrOffRecTreffer["VOR_MAHNUNG1AM"][$i]."',
					'".$arrOffRecTreffer["VOR_MAHNUNG2AM"][$i]."','".$arrOffRecTreffer["VOR_MAHNUNG3AM"][$i]."','".$arrOffRecTreffer["VOR_ABGABEINKASSOAM"][$i]."',
					".$arrOffRecTreffer["VOR_MAHNSTUFE"][$i].",'".str_replace("'","''",$arrOffRecTreffer["VOR_GEKAUFTAUFPAN"][$i])."','".str_replace("'","''",$strEinkaeufer)."','".str_replace("'","''",$arrKundennr[$strKundennr]["Kunden_Art"])."',
					'".str_replace("'","''",$arrKundennr[$strKundennr]["Firma"])."','".str_replace("'","''",$arrKundennr[$strKundennr]["Abteilung"])."',
					'".str_replace("'","''",$arrKundennr[$strKundennr]["Titel"])."','".str_replace("'","''",$arrKundennr[$strKundennr]["Name"])."','".str_replace("'","''",$arrKundennr[$strKundennr]["Vorname"])."','".str_replace("'","''",$arrKundennr[$strKundennr]["Strasse"])."','".str_replace("'","''",$arrKundennr[$strKundennr]["PLZ"])."',
					'".str_replace("'","''",$arrKundennr[$strKundennr]["Ort"])."','".str_replace("'","''",$arrKundennr[$strKundennr]["Post_Id"])."','".str_replace("'","''",$arrKundennr[$strKundennr]["Geschlecht"])."','aufbereiten: ".$PHP_AUTH_USER."',sysdate,0,0)";
					
					abfrage("ora",$strSql,1);
					
					$intZaehler++;
				}
				
				if($intZaehler==$intZeilen)
				{
					@ocicommit($hdlOraVerbindung) or orafehler($hdlOraVerbindung,$hdlOraAnweisung,$strSql);
					
					
					$strAusgabe = $intZaehler." Datens&auml;tze erfolgreich aufbereitet!";
					
					?>
					<span class="titel"><?php echo $strAusgabe; ?></span><br><br>
					<button type="button" onclick="self.location.href='<?php echo $PHP_SELF; ?>?intMahnBuch=1';">Mahnungen buchen</button>
					<?php
					
				}
				else
				{
					@ocirollback($hdlOraVerbindung);
				}
			}
			else
			{
				$strAusgabe = $intZeilen." Datens&auml;tze stehen zur Aufbereitung an.";
					
				?>
				<span class="titel"><?php echo $strAusgabe; ?></span><br><br>
				<button type="button" onclick="self.location.href='<?php echo $PHP_SELF; ?>?intMahnAufb=1';">Mahnungen aufbereiten</button>
				<?php
			}
		}
		else
		{
			?>
			<span class="titel">Zurzeit liegen keine f&auml;lligen Rechnungen vor!</span>
			<?php
		}
	}
}




include("card/card_zeitende.inc.php");			// Zeitmessung
?>
</td></tr></table>
</td></tr></table>
</body>
</html>
