var gloFocused;

function TagsEinfuegen(a_tag, b_tag) {	
	//Pfad auf die Textbox legen
    var textbox_pointer = gloFocused;
	//Inhalt aus der textbox holen
    var textbox_inhalt = textbox_pointer.value;
    
    var rangeStart = textbox_pointer.selectionStart;
    var rangeEnd = textbox_pointer.selectionEnd - rangeStart + a_tag.length + b_tag.length;
    
    //Firefox (und scheinbar IE 11)
    if (window.getSelection) {
        //aus dem Inhalt der Textbox wird der String vom Anfang bis zum Ende der Selektion herausgefischt
        var selektion = textbox_inhalt.substring(textbox_pointer.selectionStart, textbox_pointer.selectionEnd);
    }
    //IE
    else if (document.selection) {
        var selektion = document.selection.createRange().text;
    }
    //T3h rest
    else if (document.getSelection) {
        var selektion = document.getSelection();
    }
    

    //Speichert den Teil des Textes vom Anfang BIS zur Selektion
    var textbox_beginn = textbox_inhalt.substring(0, textbox_pointer.selectionStart);
    //Speichert den Teil ab ENDE der Selektion
    var textbox_ende = textbox_inhalt.substring(textbox_pointer.selectionEnd, textbox_inhalt.length);
    
    
    
    if (selektion != '')
    {
    	if ((selektion.substring(0, a_tag.length) == a_tag) && (selektion.substr(-(b_tag.length), b_tag.length) == b_tag))
		{
	        //Entfernt die Tags vor und hinter dem selektierten Text
	        selektion = selektion.substring(a_tag.length, selektion.length - b_tag.length);
	        rangeEnd = rangeEnd - 2*a_tag.length - 2*b_tag.length;
		}
    	else
    	{
	        //Setzt die Tags vor und hinter dem selektierten Text
	        selektion = a_tag + selektion + b_tag;
    	}
        //Generiert den kompletten Inhalt
        textbox_inhalt = textbox_beginn + selektion + textbox_ende;
        //Schiebt es zurück ins Textfeld
        textbox_pointer.focus();
        textbox_pointer.value = textbox_inhalt;
        endOfBox(textbox_pointer, rangeStart, rangeEnd);
        textArea2Auswahlbox(textbox_pointer);
        key_ajaxBox(event);
    }
}

//Selektion um die Tags erweitern
function endOfBox(box, selectionStart, selectionEnd) {
	box.focus();
	var range = box.createTextRange();
	range.collapse();
	range.moveStart('character', selectionStart);
	range.moveEnd('character', selectionEnd);
	range.select();
	
}

//Ajax ausführn
function textArea2Auswahlbox()
{
	//Ajax befüllen
	box1 = document.getElementsByName('txtInhalt')[0].value;
	box2 = document.getElementsByName('txtInhalt2')[0].value;
	box1 = box1.replace(/\r?\n/g, '<br />');
	box2 = box2.replace(/\r?\n/g, '<br />');
	vorschau = document.getElementsByName('txtVorschau')[0].checked;
	vorschauBox = document.getElementById('AusgabeLive');
	html = box1 + "*BOXMITTE*" + box2; 
	html = encodeURI(html);
	html = encodeURIComponent(html);
	document.getElementById("sucajaxBox1").value = html;
	document.getElementById("sucajaxBox2").value = html;
	
	//Ajax aufrufen
	key_ajaxBox1(event);
	key_ajaxBox2(event);
	
	//Ein- / Ausblenden der Vorschau
	if (vorschau) {
		if (vorschauBox != null) {
			vorschauBox.style.display = 'block';
		}
	}
	else
	{
		
		if (vorschauBox != null) {
			vorschauBox.style.display = 'none';
		}
	}
}

//Zuletz gefokusten TextArea in var wegschreiben
function saveFocused(box){
	gloFocused = box;
}
