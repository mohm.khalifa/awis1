<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('Wort','Dateiname');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_weiter');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht5001= $AWISBenutzer->HatDasRecht(5001);		// Register in Filialen

    $files = [
        'kommissioniertag_weiden' => 'ImpDat_KommWEN',
        'kommissioniertag_werl' => 'ImpDat_KommWER',
        'anlieferung_weiden' => 'ImpDat_AnlWEN',
        'anlieferung_werl' => 'ImpDat_AnlWER'
    ];

    $fehlerText = '';
    $uploadText = '';

    foreach ($files as $datei => $feldname) {
        if (isset($_FILES[$feldname])) {
            if ($_FILES[$feldname]['tmp_name'] != '') {
                if(!move_uploaded_file($_FILES[$feldname]['tmp_name'], '/daten/web/dokumente/sonstiges/' . $_FILES[$feldname]['name'])) {
                    $fehlerText .= "Fehler beim Upload der Datei: " . $_FILES[$feldName]['name'] . "<br>";
                } else {
                    $uploadText .= "Datei <b>" . $_FILES[$feldname]['name'] . "</b> erfolgreich hinterlegt" . "<br>";
                }
            }
        }
    }

    $Form->Formular_Start();
    $Form->SchreibeHTMLCode('<form name=frmfilialinfo action=./filialinfos_Main.php?cmdAktion=Tourenplan method=POST enctype="multipart/form-data">');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel('Upload Tourenplan:',170,'font-weight: bold; font-size:15px');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'] . ' Kommissionierung (Weiden):', 280);
    $Form->Erstelle_DateiUpload('ImpDat_KommWEN', 600, 30, 2000000,'','.pdf');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'] . ' Anlieferung (Weiden):', 280);
    $Form->Erstelle_DateiUpload('ImpDat_AnlWEN', 600, 30, 2000000,'','.pdf');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'] . ' Kommissionierung (Werl):', 280);
    $Form->Erstelle_DateiUpload('ImpDat_KommWER', 600, 30, 2000000,'','.pdf');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Dateiname'] . ' Anlieferung (Werl):', 280);
    $Form->Erstelle_DateiUpload('ImpDat_AnlWER', 600, 30, 2000000,'','.pdf');
    $Form->ZeileEnde();

    if ($fehlerText != '') {
        $Form->ZeileStart();
        $Form->Hinweistext($fehlerText, awisFormular::HINWEISTEXT_FEHLER);
        $Form->ZeileEnde();
    }

    if ($uploadText != '') {
        $Form->ZeileStart();
        $Form->Hinweistext($uploadText, awisFormular::HINWEISTEXT_OK);
        $Form->ZeileEnde();
    }

    $Form->Formular_Ende();

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    $Form->Schaltflaeche('image', 'cmdWeiter', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

}
catch (awisException $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
    }
    else
    {
        $Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
