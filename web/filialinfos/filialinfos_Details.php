<?php
global $AWISCursorPosition;        // Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
global $FilialeGeschlossen;
global $FIL_LAN_CODE;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('awisFilialen.inc');

//require_once("db.inc.php"); // DB-Befehle

require_once('filialinfos_funktionen.inc');
try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('FIL', '%');
    $TextKonserven[] = array('BUL', '%');
    $TextKonserven[] = array('PER', '%');
    $TextKonserven[] = array('Filialinfo', '%');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Link_Maps');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'OeffnungsZeiten');
    $TextKonserven[] = array('Wort', 'MoBisFr');
    $TextKonserven[] = array('Wort', 'Sa');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht5000 = $AWISBenutzer->HatDasRecht(5000);
    $Recht5001 = $AWISBenutzer->HatDasRecht(5001);
    $Recht5002 = $AWISBenutzer->HatDasRecht(5002);
    $Recht5022 = $AWISBenutzer->HatDasRecht(5022);
    if ($Recht5000 == 0) {
        $DB->ProtokollEintrag(awisDatenbank::EREIGNIS_FEHLER, 1000, $AWISBenutzer->BenutzerName(), 'Filialen-Modul',
            $AWISBenutzer->BenutzerName());
        echo "<span class=HinweisText>" . $AWISSprachKonserven['Fehler']['err_keineRechte'] . "</span>";
        die();
    }

    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));
    $DetailAnsicht = false;
    //awis_Debug(1,$_POST,$_GET);
    //********************************************************
    // Parameter ?
    //********************************************************
    if (isset($_POST['cmdSuche_x'])) {
        $Form->DebugAusgabe(1, $_POST);
        $Param['FIL_ID'] = $_POST['sucFIL_ID'];
        $Param['FIL_BEZ'] = $_POST['sucFIL_BEZ'];
        $Param['FIL_STRASSE'] = $_POST['sucFIL_STRASSE'];
        $Param['FIL_ORT'] = $_POST['sucFIL_ORT'];
        $Param['FIL_ORTSTEIL'] = $_POST['sucFIL_ORTSTEIL'];
        $Param['FIL_PLZ'] = $_POST['sucFIL_PLZ'];
        $Param['FIL_LAND'] = ($_POST['sucFIL_LAND'] == '0'?'':$_POST['sucFIL_LAND']);
        $Param['FIL_MITARBEITER'] = (isset($_POST['sucFIL_MITARBEITER'])?$_POST['sucFIL_MITARBEITER']:'');
        $Param['FIL_MITARBEITERVORNAME'] = (isset($_POST['sucFIL_MITARBEITERVORNAME'])?$_POST['sucFIL_MITARBEITERVORNAME']:'');
        $Param['FIL_MITARBEITERPERSNR'] = (isset($_POST['sucFIL_MITARBEITERPERSNR'])?$_POST['sucFIL_MITARBEITERPERSNR']:'');
        $Param['FIL_BETREUER'] = (isset($_POST['sucFIL_BETREUER'])?$_POST['sucFIL_BETREUER']:'0');
        $Param['FIL_GEBIET'] = (isset($_POST['sucFIL_GEBIET'])?$_POST['sucFIL_GEBIET']:'0');
        $Param['FIL_NOTFILIALE'] = (isset($_POST['sucFIL_NOTFILIALE'])?$_POST['sucFIL_NOTFILIALE']:'');
        $Param['FIL_SHOP_GEOFFNET'] = (isset($_POST['sucFIL_SHOP_GEOFFNET'])?$_POST['sucFIL_SHOP_GEOFFNET']:'');

        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['BLOCK'] = 1;
        $Param['SPEICHERN'] = isset($_POST['sucAuswahlSpeichern'])?'on':'';
    } elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK'])) {
        include('./filialinfos_loeschen.php');
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));
    } elseif (isset($_POST['cmdSpeichern_x'])) {
        include('./filialinfos_speichern.php');
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));
    } elseif (isset($_POST['cmdDSNeu_x'])) {
        $AWIS_KEY1 = -1;
        //$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));
    } elseif (isset($_GET['FIL_ID'])) {
        $AWIS_KEY1 = $Form->Format('T', $_GET['FIL_ID']);
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));
        $Param['KEY'] = $AWIS_KEY1;
    } elseif (isset($_GET['FEB_KEY'])) {
        $Param['FIL_ID'] = '';
        $Param['FIL_BEZ'] = '';
        $Param['FIL_STRASSE'] = '';
        $Param['FIL_ORT'] = '';
        $Param['FIL_ORTSTEIL'] = '';
        $Param['FIL_PLZ'] = '';
        $Param['FIL_LAND'] = '';
        $Param['FIL_MITARBEITER'] = '';
        $Param['FIL_MITARBEITERVORNAME'] = '';
        $Param['FIL_MITARBEITERPERSNR'] = '';
        $Param['FIL_BETREUER'] = '0';
        $Param['FIL_NOTFILIALE'] = '';
        $Param['FIL_SHOP_GEOFFNET'] = '';
        $Param['FIL_GEBIET'] = $_GET['FEB_KEY'];

        $Param['KEY'] = '';
        $Param['WHERE'] = '';
        $Param['ORDER'] = '';
        $Param['BLOCK'] = 1;
    } else        // Nicht �ber die Suche gekommen, letzten Key abfragen
    {
        $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_FIL'));

        if (!isset($Param['KEY'])) {
            $Param['KEY'] = '';
            $Param['WHERE'] = '';
            $Param['ORDER'] = '';
        }

        if (isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite']))) {
            $Param['KEY'] = 0;
        }

        $AWIS_KEY1 = $Param['KEY'];
    }

    $Form->DebugAusgabe(3, $Param);

    //*********************************************************
    //* Sortierung
    //*********************************************************
    if (!isset($_GET['Sort'])) {
        $ORDERBY = ' ORDER BY FIL_ID';
    } else {
        $ORDERBY = ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['Sort']);
    }

    //********************************************************
    // Daten suchen
    //********************************************************
    $Bedingung = _BedingungErstellen($Param);

    //20140522-CA: NLS - Anpasunng f�r Sucheinschr�nkung Schlie�datum (FIT_TYP:38 + Programmparamter: 206) in BedingungErstellen
    $SQL = "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RR'";

    $DB->Ausfuehren($SQL);

    $SQL = 'SELECT filialen.*, LAN_CODE, LAN_LAND';
    if ($Param['FIL_MITARBEITER'] . $Param['FIL_MITARBEITERVORNAME'] . $Param['FIL_MITARBEITERPERSNR'] != '') {
        $SQL .= ', PER_NACHNAME, PER_VORNAME, PER_NR';
    }

    // Schlie�datum und FEB_KEY als Kriterium f�r die Schlie�ung
    $SQL .= ', CASE WHEN (SELECT fez_feb_key FROM Filialebenenzuordnungen WHERE fez_fil_id = fil_id AND trunc(fez_gueltigab) <= trunc(SYSDATE) AND trunc(fez_gueltigbis) >= trunc(SYSDATE) AND ROWNUM = 1) IS NULL ';
    $SQL .= ' AND (SELECT FIF_WERT FROM (SELECT * FROM FILIALINFOS WHERE FIF_FIT_ID = 38) WHERE FIL_ID = FIF_FIL_ID AND (FIF_WERT IS NOT NULL OR FIF_WERT < SYSDATE)) IS NOT NULL THEN 1 ELSE 0 END AS GESCHLOSSEN ';

    $SQL .= ', CASE WHEN (SELECT foz_bezeichnung FROM filialenoeffnungszeitenmodelle inner join filialinfos on fif_fit_id = 125 and fif_wert = foz_key where  fif_fil_id = fil_id ) like \'COR%\' ';
    $SQL .= '   THEN 1 ELSE 0 END AS FIL_NOTFILIALE ';

    $SQL .= ', CASE WHEN (SELECT to_date(fif_wert,\'DD.MM.YYYY\') FROM filialinfos where fif_fit_id = \'921\' and fif_fil_id = fil_id and fif_wert is not null ) >= trunc(sysdate) ';
    $SQL .= '   THEN 0 ELSE 1 END AS FIL_SHOP_GEOFFNET ';

    $SQL .= ', CASE WHEN (SELECT to_date(fif_wert,\'DD.MM.YYYY\') FROM filialinfos where fif_fit_id = \'931\' and fif_fil_id = fil_id and fif_wert is not null ) >= trunc(sysdate) ';
    $SQL .= '   THEN 0 ELSE 1 END AS FIL_GESAMT_GEOEFFNET ';

    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM filialen';
    //	$SQL .= ' INNER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
    $SQL .= ' LEFT OUTER JOIN Laender ON FIL_LAN_WWSKENN = LAN_WWSKENN';
    if ($Param['FIL_MITARBEITER'] . $Param['FIL_MITARBEITERVORNAME'] . $Param['FIL_MITARBEITERPERSNR'] != '') {
        $SQL .= ' INNER JOIN Personal ON PER_FIL_ID = FIL_ID AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)';
    }

    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    // Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
    if ($AWIS_KEY1 <= 0) {
        // Zum Bl�ttern in den Daten
        $Block = 1;
        if (isset($_REQUEST['Block'])) {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
            $Param['BLOCK'] = $Block;
        } elseif (isset($Param['BLOCK'])) {
            $Block = $Param['BLOCK'];
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('FIL', false));
        $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
        //$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
    } else {
        $MaxDS = 1;
        $ZeilenProSeite = 1;
        $Block = 1;
    }

    $SQL .= $ORDERBY;
    $rsFIL = $DB->RecordsetOeffnen($SQL, $DB->Bindevariablen('FIL'));

    $Form->DebugAusgabe(1, $DB->LetzterSQL());

    //********************************************************
    // Daten anzeigen
    //********************************************************
    echo '<form name="frmFilialinfos" action="./filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Block'])?'&Block=' . $_GET['Block']:'') . '" method="POST">';

    if ($rsFIL->EOF() AND !isset($_POST['cmdDSNeu_x']))        // Keine Meldung bei neuen Datens�tzen!
    {
        $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
    } elseif ($rsFIL->AnzahlDatensaetze() > 1 AND $AWIS_KEY1 == '')                        // Liste anzeigen
    {
        $DetailAnsicht = false;
        $Form->Formular_Start();

        $Form->ZeileStart();
        $Link = './filialinfos_Main.php?cmdAktion=Details' . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $Link .= '&Sort=FIL_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FIL_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_ID'], 70, '', $Link);
        $Link .= '&Sort=FIL_BEZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FIL_BEZ'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_BEZ'], 200, '', $Link);

        if ($Param['FIL_MITARBEITER'] . $Param['FIL_MITARBEITERVORNAME'] . $Param['FIL_MITARBEITERPERSNR'] != '') {
            $Link .= '&Sort=PER_NACHNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'PER_NACHNAME'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PER']['PER_NACHNAME'], 200, '', $Link);
            $Link .= '&Sort=PER_VORNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'PER_VORNAME'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PER']['PER_VORNAME'], 200, '', $Link);
            $Link .= '&Sort=PER_NR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'PER_NR'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PER']['PER_NR'], 150, '', $Link);
        } else {
            $Link .= '&Sort=FIL_STRASSE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FIL_STRASSE'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_STRASSE'], 230, '', $Link);
            $Link .= '&Sort=FIL_PLZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FIL_PLZ'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_PLZ'], 50, '', $Link);
            $Link .= '&Sort=FIL_ORT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FIL_ORT'))?'~':'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_ORT'], 200, '', $Link);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_NOTFILIALE'], 70, '', $Link);
            $Form->Erstelle_Liste_Ueberschrift('Shop', 70, '', $Link,'Shop ge�ffnet');
        }
        $Form->ZeileEnde();

        $DS = 0;
        while (!$rsFIL->EOF()) {
            $Form->ZeileStart(($rsFIL->FeldInhalt('GESCHLOSSEN') == 1?'font-style:italic;color:red;':''));
            $Link = './filialinfos_Main.php?cmdAktion=Details&FIL_ID=0' . $rsFIL->FeldInhalt('FIL_ID') . (isset($_GET['Block'])?'&Block=' . intval($_GET['Block']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
            $Form->Erstelle_ListenFeld('FIL_ID', $rsFIL->FeldInhalt('FIL_ID'), 0, 70, false, ($DS % 2), '', $Link);
            $Link = '';
            $Form->Erstelle_ListenFeld('FIL_BEZ', $rsFIL->FeldInhalt('FIL_BEZ'), 0, 200, false, ($DS % 2), '', $Link,
                'T', 'L', $rsFIL->FeldInhalt('ZLH_BEZEICHNUNG'));
            if ($Param['FIL_MITARBEITER'] . $Param['FIL_MITARBEITERVORNAME'] . $Param['FIL_MITARBEITERPERSNR'] != '') {
                $Form->Erstelle_ListenFeld('PER_NACHNAME', $rsFIL->FeldInhalt('PER_NACHNAME'), 0, 200, false, ($DS % 2),
                    '', $Link, 'T', 'L', $rsFIL->FeldInhalt('ZLH_BEZEICHNUNG'));
                $Form->Erstelle_ListenFeld('PER_VORNAME', $rsFIL->FeldInhalt('PER_VORNAME'), 0, 200, false, ($DS % 2),
                    '', $Link, 'T', 'L', $rsFIL->FeldInhalt('ZLH_BEZEICHNUNG'));
                $Form->Erstelle_ListenFeld('PER_NR', $rsFIL->FeldInhalt('PER_NR'), 0, 150, false, ($DS % 2), '', $Link);
            } else {
                $Form->Erstelle_ListenFeld('FIL_STRASSE', $rsFIL->FeldInhalt('FIL_STRASSE'), 0, 230, false, ($DS % 2),
                    '', $Link, 'T', 'L', $rsFIL->FeldInhalt('ZLH_BEZEICHNUNG'));
                $Form->Erstelle_ListenFeld('FIL_PLZ', $rsFIL->FeldInhalt('FIL_PLZ'), 0, 50, false, ($DS % 2), '', $Link,
                    'T', 'L', $rsFIL->FeldInhalt('ZLH_BEZEICHNUNG'));
                $Form->Erstelle_ListenFeld('FIL_ORT',
                    $rsFIL->FeldInhalt('FIL_ORT') . ' ' . $rsFIL->FeldInhalt('FIL_ORTSTEIL'), 0, 200, false, ($DS % 2),
                    '', $Link);
                $Form->Erstelle_ListenFeld('FIL_NOTFILIALE',
                    ($rsFIL->FeldInhalt('FIL_NOTFILIALE')==1?'x':'-'), 0, 70, false, ($DS % 2),
                                           '', $Link);
                $Form->Erstelle_ListenFeld('FIL_SHOP_GEOFFNET',
                    ($rsFIL->FeldInhalt('FIL_SHOP_GEOFFNET') == 1?'x':'-'), 0, 70, false, ($DS % 2),
                                           '', $Link);
            }
            $Form->ZeileEnde();

            $rsFIL->DSWeiter();
            $DS++;
        }

        $Link = './filialinfos_Main.php?cmdAktion=Details';
        $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

        $Form->Formular_Ende();
    }            // Eine einzelne Adresse
    else                                        // Eine einzelne oder neue Adresse
    {
        $DetailAnsicht = true;
        $AWIS_KEY1 = $rsFIL->FeldInhalt('FIL_ID');
        $FIL_LAN_CODE = $rsFIL->FeldInhalt('LAN_CODE');
        $Param['KEY'] = $AWIS_KEY1;

        echo '<input type=hidden name=txtFIL_ID value=' . $AWIS_KEY1 . '>';

        $Form->Formular_Start();
        $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

        // Filialobjekt laden
        $PEIKey = new awisFilialen($AWIS_KEY1);

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./filialinfos_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsFIL->FeldInhalt('FIL_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsFIL->FeldInhalt('FIL_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht5000 & 2) != 0);
        $FilialeGeschlossen = false;
        if ($rsFIL->FeldInhalt('GESCHLOSSEN') == '1') {
            $EditRecht = 0;
            $FilialeGeschlossen = true;
        }

       
        // �berschrift
        $Form->Erstelle_HiddenFeld('FIL_ID', $rsFIL->FeldInhalt('FIL_ID'));
        $Form->ZeileStart();
        if ($rsFIL->FeldInhalt('GESCHLOSSEN') == '1') {
            //			$Form->Erstelle_TextFeld('#FIL_ID',$AWISSprachKonserven['FIL']['wrdFiliale'].' '.$rsFIL->FeldInhalt('FIL_ID').' - '.$AWISSprachKonserven['FIL']['wrdFilialeGeschlossen'],25,400,false,'color:red;font-weight:bold;font-size:larger;background-color:yellow;');
            $Form->Erstelle_TextFeld('#FIL_ID',
                $AWISSprachKonserven['FIL']['wrdFiliale'] . ' ' . $rsFIL->FeldInhalt('FIL_ID'), 25, 400, false,
                'font-weight:bold;font-size:larger');
            $Form->Erstelle_TextFeld('#FIL_ID', $AWISSprachKonserven['FIL']['wrdFilialeGeschlossen'], 25, 400, false,
                'color:red;font-weight:bold;font-size:larger;background-color:yellow;', '', '', '', 'Z');
        } else {
            $Form->Erstelle_TextFeld('#FIL_ID',
                $AWISSprachKonserven['FIL']['wrdFiliale'] . ' ' . $rsFIL->FeldInhalt('FIL_ID'), 25, 100, false,
                'font-size:larger;');
        }

        if($rsFIL->FeldInhalt('FIL_NOTFILIALE') == 1){

            $Anzeige = '<div class="EingabeFeld" style="text-align: center; width: 400px; font-weight:bold;font-size:larger;background-color:yellow;">';

            $Anzeige .= '<span style="color:red;">' . $AWISSprachKonserven['FIL']['FIL_NOTFILIALE'] . '</span>';


            if($rsFIL->FeldInhalt('FIL_SHOP_GEOFFNET') != 1){
                $Anzeige .= '<span style="color: red; text-align: right"> - Shop geschlossen </span>';
            }else{
                $Anzeige .= '<span style="color: green; text-align: right"> - Shop ge�ffnet</span>';
            }

            $Anzeige .= '</div>';
            $Form->SchreibeHTMLCode($Anzeige);
        }

        if($rsFIL->FeldInhalt('FIL_GESAMT_GEOEFFNET') == 0) {
            $Anzeige = '<div class="EingabeFeld" style="text-align: center; width: 400px; font-weight:bold;font-size:larger;background-color:yellow;">';
            $Anzeige .= '<span style="color:red;">' . $AWISSprachKonserven['FIL']['FIL_GESCHLOSSENFILIALE'] . '</span>';
            $Anzeige .= '</div>';
            $Form->SchreibeHTMLCode($Anzeige);
        }

        $Form->ZeileEnde();
        $Form->Trennzeile();

        $StyleGeschlossen = ';font-style:italic;';
        //****************************************************
        // Spalte 1
        //****************************************************
        $Form->SchreibeHTMLCode('<div style="width:33%;float:left' . ($rsFIL->FeldInhalt('GESCHLOSSEN') == 1?$StyleGeschlossen:'') . '">');

        // Bezeichnung
        $Form->ZeileStart();
        $Form->Erstelle_TextFeld('#FIL_BEZ', $rsFIL->FeldInhalt('FIL_BEZ'), 55, 0, false);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $INET_LINK_Karte = $AWISBenutzer->ParameterLesen('INET_SUCHE_Karte');
        $Parameter = array(
            '$NAME1' => urlencode($rsFIL->FeldInhalt('FIL_BEZ')),
            '$NAME2' => '',
            '$STRASSE' => urlencode($rsFIL->FeldInhalt('FIL_STRASSE')),
            '$HAUSNUMMER' => '',
            '$PLZ' => urlencode($rsFIL->FeldInhalt('FIL_PLZ')),
            '$ORT' => urlencode($rsFIL->FeldInhalt('FIL_ORT')),
            '$LAN_CODE' => urlencode($rsFIL->FeldInhalt('LAN_CODE'))
        );
        $Link = strtr($INET_LINK_Karte, $Parameter);
        $Form->Erstelle_TextFeld('#FIL_STRASSE', $rsFIL->FeldInhalt('FIL_STRASSE'), 75, 0, false, '', '', '');
        $Form->Erstelle_Bild('href', '#MAPS', $Link, '/bilder/icon_globus.png',
            $AWISSprachKonserven['Wort']['Link_Maps'], '', 16, 16, 20, 'NEU');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextFeld('#FIL_ORT',
            $rsFIL->FeldInhalt('LAN_CODE') . '-' . $rsFIL->FeldInhalt('FIL_PLZ') . ' ' . $rsFIL->FeldInhalt('FIL_ORT'),
            55, 250, false);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        //Ortsteil
        if ($rsFIL->FeldInhalt('FIL_ORTSTEIL') != '') {
            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ORTSTEIL'] . ':', 100);
            $Form->Erstelle_TextFeld('#FIL_ORTSTEIL', $rsFIL->FeldInhalt('FIL_ORTSTEIL'), 55, 0, false);
            $Form->ZeileEnde();
        }

        // Bundesland ermitteln
        $Bundesland = $PEIKey->FilialInfo(65, 8);
        $SQL = 'SELECT BUL_BUNDESLAND, BUL_ID ';
        $SQL .= ' FROM Bundeslaender WHERE BUL_ID=' . $DB->FeldInhaltFormat('N0', $Bundesland, true) . '';
        $rsBUL = $DB->RecordSetOeffnen($SQL);
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['BUL']['BUL_BUNDESLAND'] . ':', 100);
        $Form->Erstelle_TextFeld('#BUL_BUNDESLAND', $rsBUL->FeldInhalt('BUL_BUNDESLAND'), 55, 0, false);
        $Form->ZeileEnde();

        $Form->Trennzeile('O');

        $ExpressFiliale = $PEIKey->FilialInfo(916, 2);

        // Filialtyp
        $Form->ZeileStart();
        if ($ExpressFiliale == 'J') {
            $Text = $AWISSprachKonserven['FIL']['Expressfiliale'];
        } else {
            $Text = ($rsFIL->FeldInhalt('FIL_FILTYP') == 'L'?$AWISSprachKonserven['FIL']['Lightfiliale']:($rsFIL->FeldInhalt('FIL_FILTYP') == 'S'?$AWISSprachKonserven['FIL']['Standardfiliale']:''));
        }
        $Form->Erstelle_TextFeld('#FIL_TYP', $Text, 55, 0, false);
        $Form->ZeileEnde();

        //  Kompetenzfiliale
        if ($rsFIL->FeldInhalt('FIL_AUTOGAS') == 'J') {
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('#FIL_AUTOGAS', $AWISSprachKonserven['FIL']['Kompetenzfiliale'], 55, 0, false);
            $Form->ZeileEnde();
        }

        //AX-Filiale
        if ((($Recht5002 & 16384) == 16384) AND ($PEIKey->Filialinfo(910, 0,
                    awisFilialen::FILIALINFO_FELD_WERT) == 'J')
        ) {
            $Form->ZeileStart();
            //$Form->Erstelle_TextFeld('#AX-Filiale','AX-Filiale',150,0,false,'','','','','','ddd');
            $Form->Erstelle_TextLabel('AX-Filiale', 175, 'font-weight:bold', '', '');
            if (($Recht5002 & 2097152) == 2097152) {
                $Form->Erstelle_TextLabel($PEIKey->Filialinfo(914, 0, awisFilialen::FILIALINFO_FELD_WERT), 70,
                    'font-weight:bold', '', 'Umstellungsdatum auf AX');
            }
            $Form->ZeileEnde();
        }

        // Dynamische Infos anzeigen
        FilialInfos('SPALTE1', $PEIKey, $FilialeGeschlossen);

        $Form->SchreibeHTMLCode('</div>');        // Ende Spalte 1

        //************************************************************
        // Spalte 2
        //************************************************************
        $Form->SchreibeHTMLCode('<div style="width:32%;float:left' . ($rsFIL->FeldInhalt('GESCHLOSSEN') == 1?$StyleGeschlossen:'') . '">');

        $Form->ZeileStart();
        $Text = $Form->LadeTextBaustein('Filialinfo', 'fit_lbl_1');
        $Form->Erstelle_TextLabel($Text, 150);
        $Form->Erstelle_TextFeld('#TEL', $PEIKey->FilialInfo(1, 0, awisFilialen::FILIALINFO_FELD_WERT), 0, 200, false);
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Text = $Form->LadeTextBaustein('Filialinfo', 'fit_lbl_2');
        $Form->Erstelle_TextLabel($Text, 150);
        $Form->Erstelle_TextFeld('#KDFAX', $PEIKey->FilialInfo(2, 0, awisFilialen::FILIALINFO_FELD_WERT), 0, 200,
            false);
        $Form->ZeileEnde();

        FilialInfos('SPALTE2', $PEIKey, $FilialeGeschlossen);

        //Schlie�datum
        if ($PEIKey->FilialInfo(38, 0, awisFilialen::FILIALINFO_FELD_WERT) != '') {
            $Form->ZeileStart();
            $Text = $Form->LadeTextBaustein('Filialinfo', 'fit_lbl_38');
            $TTT = $Form->LadeTextBaustein('Filialinfo', 'fit_ttt_38');
            $Form->Erstelle_TextLabel($Text . ':', 150, '', '', $TTT);
            $Form->Erstelle_TextFeld('#SCHLIESSDATUM', $PEIKey->FilialInfo(38, 0, awisFilialen::FILIALINFO_FELD_WERT),
                0, 200, false, 'color:red;');
            $Form->ZeileEnde();
        }

        $Form->SchreibeHTMLCode('</div>');        // Ende Spalte 2

        //************************************************************
        // Spalte 3
        //************************************************************
        $Form->SchreibeHTMLCode('<div style="width:32%;float:left' . ($rsFIL->FeldInhalt('GESCHLOSSEN') == 1?$StyleGeschlossen:'') . '">');

        if ($rsFIL->FeldInhalt('GESCHLOSSEN') == '0') {
            FilialInfos('SPALTE3', $PEIKey, $FilialeGeschlossen);
        }
        $Form->SchreibeHTMLCode('</div>');        // Ende Spalte 3

        $Form->Formular_Ende();
        if (!isset($_POST['cmdDSNeu_x'])) {
            $RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:'');
            if ($RegisterSeite == '' AND ($Param['FIL_MITARBEITER'] . $Param['FIL_MITARBEITERVORNAME'] . $Param['FIL_MITARBEITERPERSNR'] != '')) {
                $RegisterSeite = 'Personal';
            }
            $Reg = new awisRegister(5001);
            $Reg->ZeichneRegister($RegisterSeite);
        }
    }

    //awis_Debug(1, $Param, $Bedingung, $rsFIL, $_POST, $rsAZG, $SQL, $AWISSprache);

    $AWISBenutzer->ParameterSchreiben('Formular_FIL', serialize($Param));

    //***************************************
    // Schaltfl�chen f�r dieses Register
    //***************************************
    $Form->SchaltflaechenStart();

    $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png',
        $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

    if (((($Recht5000 & 6) != 0) AND $DetailAnsicht) OR (($Recht5002 & 33554432) == 33554432) OR (($Recht5022 & 2) == 2)) {
        $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png',
            $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $Form->SchaltflaechenEnde();

    $Form->SchreibeHTMLCode('</form>');

    $Form->SetzeCursor($AWISCursorPosition);
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        $Form->SchreibeHTMLCode('AWIS-Fehler:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180922");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';

    $FilMonate = $AWISBenutzer->ParameterLesen('FilialInfoSichtbarNachSchliessMonate');

    //20140522-CA: Zweifelhaft ob richtig neue Bedingung funktioniert
    //$Bedingung.= ' AND ( EXISTS(SELECT * FROM (SELECT * FROM FILIALINFOS WHERE FIF_FIT_ID = 38) WHERE FIL_ID = FIF_FIL_ID AND FIF_WERT IS NULL OR FIF_WERT > SYSDATE))';

    // 20140603-SK: Nicht notwendig, weil �ber Gebietszuordnung geregelt!?
    //$Bedingung.= ' AND (EXISTS (SELECT * FROM FILIALINFOS WHERE FIL_ID = FIF_FIL_ID AND FIF_FIT_ID = 38 AND( FIF_WERT IS NULL OR add_months(FIF_WERT,'.$FilMonate.') > SYSDATE)))';

    // Beschr�nkung auf die g�ltigen Filialen, SK 1.7.2014
    // Ausnahme Filial-Gruppe 5 (Franchise-Filialen), OP 18.07.2014
    $Bedingung .= ' AND (EXISTS(SELECT * FROM Filialebenenzuordnungen ';
    $Bedingung .= '              WHERE fez_fil_id = fil_id AND trunc(fez_gueltigab) <= trunc(SYSDATE) AND trunc(fez_gueltigbis) >= trunc(SYSDATE))';
    $Bedingung .= ' OR EXISTS(SELECT * FROM FILIALINFOS ';
    $Bedingung .= '              WHERE FIF_FIT_ID = 38 AND FIL_ID = FIF_FIL_ID ';
    $Bedingung .= '              AND (FIF_WERT IS NOT NULL AND FIF_WERT <= SYSDATE))';
    $Bedingung .= ' OR FIL_GRUPPE = \'5\')';

    if ($AWIS_KEY1 != 0) {
        $Bedingung .= ' AND FIL_ID = ' . $DB->WertSetzen('FIL', 'N0', $AWIS_KEY1);

        return $Bedingung;
    }

    if (isset($Param['FIL_ID']) AND $Param['FIL_ID'] != '') {
        $Bedingung .= ' AND FIL_ID = ' . $DB->WertSetzen('FIL', 'N0', $Param['FIL_ID']);
    }
    
    if (isset($Param['FIL_BEZ']) AND $Param['FIL_BEZ'] != '') {
        $Bedingung .= ' AND ( UPPER(replace(fil_bez,\'ATU \',\'\')) ' . $DB->LIKEoderIST($Param['FIL_BEZ'],awisDatenbank::AWIS_LIKE_UPPER) ;
        $Bedingung .= ' OR SUCHWORT(upper(replace(fil_bez,\'ATU \',\'\'))) ' . $DB->LIKEoderIST(mb_strtoupper($Param['FIL_BEZ']), awisDatenbank::AWIS_LIKE_SUCHWORT,'FIL');
        $Bedingung .= ')';
    }

    if (isset($Param['FIL_STRASSE']) AND $Param['FIL_STRASSE'] != '') {
        //$DB->SetzeBindevariable('FIL','var_T_fil_strasse',$Param['FIL_STRASSE']); // Vor�bergehend angepasste Suche. Sollte noch gekl�rt werden.
        $Bedingung .= ' AND ( regexp_like(FIL_STRASSE,regexp_replace(' . $DB->WertSetzen('FIL', 'T',
                $Param['FIL_STRASSE']) . ',\'[[:punct:]]\',\'\'),\'i\')';
        $Bedingung .= ' OR ( regexp_like(SUCHWORT(FIL_STRASSE),SUCHWORT(regexp_replace(' . $DB->WertSetzen('FIL', 'T',
                $Param['FIL_STRASSE']) . ',\'[[:punct:]]\',\'\')),\'i\')))';
    }

    if (isset($Param['FIL_ORT']) AND $Param['FIL_ORT'] != '') {
        //$DB->SetzeBindevariable('FIL','var_T_fil_ort',$Param['FIL_ORT']); // Vor�bergehend angepasste Suche. Sollte noch gekl�rt werden.
        $Bedingung .= ' AND ( regexp_like(FIL_ORT,regexp_replace(' . $DB->WertSetzen('FIL', 'T',
                $Param['FIL_ORT']) . ',\'[[:punct:]]\',\'\'),\'i\')';
        $Bedingung .= ' OR ( regexp_like(SUCHWORT(FIL_ORT),SUCHWORT(regexp_replace(' . $DB->WertSetzen('FIL', 'T',
                $Param['FIL_ORT']) . ',\'[[:punct:]]\',\'\')),\'i\')))';
    }

    if (isset($Param['FIL_ORTSTEIL']) AND $Param['FIL_ORTSTEIL'] != '') {
        //$DB->SetzeBindevariable('FIL','var_T_fil_ortsteil',$Param['FIL_ORTSTEIL']); // Vor�bergehend angepasste Suche. Sollte noch gekl�rt werden.
        $Bedingung .= ' AND ( regexp_like(FIL_ORTSTEIL,regexp_replace(' . $DB->WertSetzen('FIL', 'T',
                $Param['FIL_ORTSTEIL']) . ',\'[[:punct:]]\',\'\'),\'i\')';
        $Bedingung .= ' OR ( regexp_like(SUCHWORT(FIL_ORTSTEIL),SUCHWORT(regexp_replace(' . $DB->WertSetzen('FIL', 'T',
                $Param['FIL_ORTSTEIL']) . ',\'[[:punct:]]\',\'\')),\'i\')))';
    }

    if (isset($Param['FIL_PLZ']) AND $Param['FIL_PLZ'] != '') {
        $Bedingung .= ' AND (FIL_PLZ ' . $DB->LIKEoderIST($Param['FIL_PLZ'], awisDatenbank::AWIS_LIKE_UPPER,
                'FIL') . ')';
    }

    if (isset($Param['FIL_LAND']) AND $Param['FIL_LAND'] != '') {
        $Bedingung .= ' AND FIL_LAN_WWSKENN = ' . $DB->WertSetzen('FIL', 'T', $Param['FIL_LAND']);
    }

    if ($Param["FIL_MITARBEITER"] != '' OR $Param["FIL_MITARBEITERVORNAME"] != '' OR $Param["FIL_MITARBEITERPERSNR"] != '') {
        if ($Param["FIL_MITARBEITER"] != '') {
            //var_dump($DB->LIKEoderIST($Param["FIL_MITARBEITER"],awisDatenbank::AWIS_LIKE_UPPER,'FIL'));
            $Bedingung .= " AND UPPER(PER_NACHNAME) like " . $DB->WertSetzen('FIL', 'TU', $Param["FIL_MITARBEITER"]);
        }
        if ($Param["FIL_MITARBEITERVORNAME"] != '') {
            $Bedingung .= " AND UPPER(PER_VORNAME)  like" . $DB->WertSetzen('FIL', 'TU',
                    $Param["FIL_MITARBEITERVORNAME"]);
        }
        if ($Param["FIL_MITARBEITERPERSNR"] != '') {
            $Bedingung .= ' AND PER_NR = ' . $DB->WertSetzen('FIL', 'N0', $Param["FIL_MITARBEITERPERSNR"], false);
        }
        $Bedingung .= " AND (PER_AUSTRITT IS NULL OR PER_AUSTRITT >= sysdate)";

        if (($PersAusschluss = $AWISBenutzer->ParameterLesen('Filialinfo: PersNr unterdruecken')) != '') {
            $Nummern = explode(',', $PersAusschluss);
            //$Bedingung .= ' AND PER_NR NOT IN ('.$PersAusschluss.')';

            $i = 1;
            $FeldListe = '';
            foreach ($Nummern as $Nummer) {
                $FeldListe .= ', ' . $DB->WertSetzen('FIL', 'N0', $Nummer);

                //$DB->SetzeBindevariable('FIL', 'var_N0_PERNUMMER'.$i, $Nummer, awisDatenbank::VAR_TYP_GANZEZAHL);
                $i++;
            }
            $Bedingung .= ' AND PER_NR NOT IN (' . substr($FeldListe, 1) . ')';
        }
        //		$Bedingung .= ')';
    }

    if ($Param['FIL_GEBIET'] != '0') {
        $FilObj = new awisFilialen();
        $FilListe = $FilObj->FilialenEinerEbene($DB->FeldInhaltFormat('N0', $Param['FIL_GEBIET'], false));
        $i = 1;
        $FeldListe = '';
        foreach ($FilListe AS $FilialZeile) {
            //$FeldListe .= ', :var_FEZ_FIL_ID_'.$i;
            //$DB->SetzeBindevariable('FIL', 'var_FEZ_FIL_ID_'.$i, $FilialZeile['FEZ_FIL_ID'], awisDatenbank::VAR_TYP_GANZEZAHL);
            $FeldListe .= ', ' . $DB->WertSetzen('FIL', 'N0', $FilialZeile['FEZ_FIL_ID']);
            $i++;
        }
        $Bedingung .= " AND FIL_ID IN (" . substr($FeldListe, 1) . ')';
    }

    if ($Param['FIL_BETREUER'] != '0') {
        // View V_Filialebenenrollen wurde benutzt. Keine G�ltigkeiten abgefragt.
        $Bedingung .= "AND EXISTS(SELECT * FROM V_FILIALEBENENROLLEN_AKTUELL ";
        $Bedingung .= ' WHERE XX1_FIL_ID = FIL_ID AND XX1_KON_KEY = ' . $DB->WertSetzen('FIL', 'N0',
                $Param['FIL_BETREUER']);
        //$DB->SetzeBindevariable('FIL', 'var_N0_XX1_KON_KEY', $Param['FIL_BETREUER'], awisDatenbank::VAR_TYP_GANZEZAHL,'N0',false);
        $Bedingung .= ')';
    }

    //*******************************************
    // Sonderfall: 995 im Entwick anzeigen
    //*******************************************
    $WerkZeug = new awisWerkzeuge();
    if ($WerkZeug->awisLevel() == awisWerkzeuge::AWIS_LEVEL_ENTWICK AND $Param['FIL_ID'] == '995') {
        $Bedingung .= ' AND (' . substr($Bedingung, 4) . ') OR FIL_ID = 995';
    }

    if ($Param['FIL_NOTFILIALE'] == '1') {
        $Bedingung .= ' AND EXISTS (SELECT foz_bezeichnung FROM filialenoeffnungszeitenmodelle inner join filialinfos on fif_fit_id = 125 and fif_wert = foz_key where  fif_fil_id = fil_id and foz_bezeichnung like \'COR%\')  ';
    }elseif ($Param['FIL_NOTFILIALE'] == '0' and $Param['FIL_NOTFILIALE'] !== ''){
        $Bedingung .= ' AND EXISTS (SELECT foz_bezeichnung FROM filialenoeffnungszeitenmodelle inner join filialinfos on fif_fit_id = 125 and fif_wert = foz_key where  fif_fil_id = fil_id and foz_bezeichnung not like \'COR%\')  ';
    }

    if ($Param['FIL_SHOP_GEOFFNET'] == '1') {
        $Bedingung .= ' AND not  exists (SELECT * FROM filialinfos where to_date(fif_wert,\'DD.MM.YYYY\') >= trunc(sysdate) and fif_fit_id = 921 and fif_fil_id = fil_id) ';
    }elseif ($Param['FIL_SHOP_GEOFFNET'] == '0' and $Param['FIL_SHOP_GEOFFNET'] !== ''){
        $Bedingung .= ' AND  exists (SELECT * FROM filialinfos where to_date(fif_wert,\'DD.MM.YYYY\') >= trunc(sysdate) and fif_fit_id = 921 and fif_fil_id = fil_id) ';
    }


    $Param['WHERE'] = $Bedingung;

    return $Bedingung;
}

?>