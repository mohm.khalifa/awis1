<?php
// Variablen
global $ReifenRechte;
global $con;
global $awisDBFehler;
global $AWISBenutzer;
global $awisRSZeilen;

if($ReifenRechte==0)
{
    awisEreignis(3,1000,'Reifen',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}
// lokale Variablen
$filSuche = '';		//Parameter der Anwender

// Ende Variablen

echo "<br>";

echo "<form method=post name=frmSuche action='./reifen_Main.php?cmdAktion=Reifen'>";
echo "<table id=SuchMaske>";

	// Alle Parameter auslesen
	//	Es wird aber nicht alles eingebelndet!
if(isset($_GET['Reset']) AND $_GET['Reset']=='True')
{
	$filSuche = '';
}
else
{
	$filSuche = explode(";",awis_BenutzerParameter($con, "ReifenSuche", $AWISBenutzer->BenutzerName()));
}
	// Auswahl Sortierung
	// Reifentyp als separater Parameter, da nur Sommer oder Winterreifen als Vorauswahl
$DefTyp = awis_BenutzerParameter($con, "ReifenTyp", $AWISBenutzer->BenutzerName());
$AlleReifenTypenMerken = awis_BenutzerParameter($con, "ReifenSucheAlleWerteMerken", $AWISBenutzer->BenutzerName());
/**********************************************
* * Eingabemaske
***********************************************/

// F�r KB-Leute -> Hersteller in der ersten Zeile
if(isset($filSuche[17]) and $filSuche[17]=="on")
{
echo "<tr><td><table width=700 border=1><tr>";
echo "<td width=40%>Herste<u>l</u>ler:</td>";
echo "<td width=40%>Be<u>z</u>eichnung:</td>";
echo "<td width=20%>Auswahl <u>s</u>peichern:</td></tr>";

echo "<tr><td><input type=text name=txtHersteller accesskey='l' size=29 tabindex=1 " . ($filSuche[14]=="on"?"value=" . $filSuche[15]:"") . "></td>";
echo "<td><input type=text id=txtBez name=txtBez accesskey='z' size=29 tabindex=2 " . ($filSuche[14]=="on"?"value=" . $filSuche[9]:"") . "></td>";
/* L�nderauswahl f�r die KB-Preise nicht anzeigen*/
echo "<input type=hidden name=txtLAN_CODE value='DE'>";

echo "<td><input type=checkbox value=on " . ($filSuche[14]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=3></td>";

echo "</tr></table></td></tr>";
}	// Ende KB-Aufbau

echo "<table width=600 border=1>";
echo "<tr><td width=10><u>B</u>reite:</td>";
echo "<td width=10><u>Q</u>uer schnitt:</td>";
echo "<td width=10><u>F</u>elgen gr��e:</td>";
echo "<td width=10>LLK<u>W</u> (C):</td>";
echo "<td width=10><u>L</u>oad Index:</td>";
echo "<td width=10>Load Inde<u>x</u>2:</td>";
echo "<td width=10>Spee<u>d</u> Index:</td>";
echo "<td width=10><u>M</u>ax. VK:</td>";
echo "<td width=90><u>A</u>TU-Nummer:</td>";

//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);
if($UserFilialen=='')
{
	echo "<td width=50><u>F</u>ilialpreis:</td>";	
}

echo "</tr>";

echo "<tr>";
echo "<td><input id=txtBreite name=txtBreite accesskey='b' size=3 tabindex=11 " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[0]:"") . "></td>";
echo "<td><input id=txtQSchnitt name=txtQSchnitt accesskey='q' size=3 tabindex=12 " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[1]:"") . "></td>";
echo "<td><input id=txtDMesser name=txtDMesser accesskey='f' size=5 tabindex=13 " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[2]:"") . "></td>";
echo "<td><input id=txtLLKW name=txtLLKW accesskey='w' size=5 tabindex=14  " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[3]:"") . "></td>";
echo "<td><input id=txtLIndex name=txtLIndex accesskey='l' size=5 tabindex=15  " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[4]:"") . "></td>";
echo "<td><input id=txtLIndex2 name=txtLIndex2 accesskey='x' size=5 tabindex=16  " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[5]:"") . "></td>";

	// Auswahlfeld Speedindex
echo "<td><select id=drpSpeedIndex name=txtSpeedIndex style=height:23;width:90 accesskey='d' size=1 tabindex=17>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='#-#'?"selected":""):"") . " value=#-#>Alle</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='L'?"selected":""):"") . " value=L>L - 120</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='M'?"selected":""):"") . " value=M>M - 130</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='N'?"selected":""):"") . " value=N>N - 140</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='P'?"selected":""):"") . " value=P>P - 150</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='Q'?"selected":""):"") . " value=Q>Q - 160</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='R'?"selected":""):"") . " value=R>R - 170</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='S'?"selected":""):"") . " value=S>S - 180</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='T'?"selected":""):"") . " value=T>T - 190</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='U'?"selected":""):"") . " value=U>U - 200</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='H'?"selected":""):"") . " value=H>H - 210</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='V'?"selected":""):"") . " value=V>V - 240</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='W'?"selected":""):"") . " value=W>W - 270</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='Y'?"selected":""):"") . " value=Y>Y - 300</option>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[6]=='ZR'?"selected":""):"") . " value=ZR>ZR - >240</option>";

echo "</select></td>";
	// Ende Auswahlfeld Speedindex


echo "<td><input id=txtMaxVK name=txtMaxVK accesskey='m' size=5 tabindex=18 " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[7]:"") . "></td>";
echo "<td><input type=text id=txtATUNr name=txtATUNr accesskey='a' size=8 tabindex=19 " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[8]:"") . "></td>";


//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
if($UserFilialen!='')
{	
	echo '<input type=hidden name="txtFiliale" value="'.$UserFilialen.'">';
}
else 
{
	echo "<td><input type=text id=txtFiliale name=txtFiliale accesskey='f' size=8 tabindex=19 " . (isset($filSuche[22]) && $filSuche[22]=="on"?"value=" . $filSuche[22]:"") . "></td>";
}


echo "</tr></table>";

// Ende erste Zeile

// Start zweite Zeile - Reifenlabel
echo "<table border=1>";
echo "<tr>";
echo "<td>Kraftstoffeff<u>i</u>zienz:</td>";
echo "<td>Nass<u>h</u>aftung:</td>";
echo "<td>Rollger�usch<u>k</u>l:</td>";
echo "<td>Roll<u>g</u>er�usch(dB):</td>";
echo "</tr>";
echo "<tr><td><select id=drpROLLRES accesskey='i' name=txtROLLRES style=height:23;width:90 tabindex=20>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[23]=='#-#'?"selected":""):"") . " value=#-#>Alle</option>";
for ($i = 'A'; $i <= 'G'; $i++) 
{
	if ($i=='D') //Kennzeichen D entf�llt
	{
		continue;
	}
	echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[23]==$i?"selected":""):"") . " value=$i>$i</option>";
}
echo "</select></td>";
echo "<td><select id=drpWETGRIP accesskey='u' name=txtWETGRIP style=height:23;width:90 tabindex=21>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[24]=='#-#'?"selected":""):"") . " value=#-#>Alle</option>";
for ($i = 'A'; $i <= 'F'; $i++) 
{
	if ($i=='D') //Kennzeichen D entf�llt
	{
		continue;
	}
	echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[24]==$i?"selected":""):"") . " value=$i>$i</option>";
}
echo "</select></td>";
echo "<td><select id=drpNOISECL accesskey='k' style=height:23;width:90 name=txtNOISECL tabindex=22>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[25]=='#-#'?"selected":""):"") . " value=#-#>Alle</option>";
for ($i = 1; $i <= 3; $i++) 
{
	echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[25]==$i?"selected":""):"") . " value=$i>$i</option>";
}
echo "</select></td>";
echo "<td><select id=drpNOISEPE accesskey='u' name=txtNOISEPE style=height:23;width:90 tabindex=23>";
echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[26]=='#-#'?"selected":""):"") . " value=#-#>Alle</option>";
for ($i = 1; $i < 100; $i++) 
{
	echo "<option " . (isset($filSuche[14]) && $filSuche[14]=="on"?($filSuche[26]==$i?"selected":""):"") . " value=$i>$i</option>";
}
echo "</select></td></tr></table>";
// Ende zweite Zeile


// F�r Nicht-KB Leute die Auswahl jetzt
//if(isset($filSuche[17]) AND $filSuche[17]!="on")
//if($filSuche[17]!="on")
if(!isset($filSuche[17]) OR $filSuche[17]!="on")
{
echo "<table width=600 border=1><tr>";
echo "<td width=40%>Herste<u>l</u>ler:</td>";
echo "<td width=40%>Be<u>z</u>eichnung:</td>";
echo "<td width=40%>Preise f<u>�</u>r:</td>";
echo "<td width=20%>Auswahl <u>s</u>peichern:</td></tr>";

echo "<tr><td><input type=text name=txtHersteller accesskey='l' size=29 tabindex=30 " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[15]:"") . "></td>";
echo "<td><input type=text id=txtBez name=txtBez accesskey='z' size=29 tabindex=31 " . (isset($filSuche[14]) && $filSuche[14]=="on"?"value=" . $filSuche[9]:"") . "></td>";

$LAND='';

if(strtoupper(substr($AWISBenutzer->BenutzerName(),0,4))=='FIL-')
{
	$Fil_Nr=substr($AWISBenutzer->BenutzerName(),4,4);
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id']=intval($Fil_Nr);
	
	$SQLLAND='SELECT FIL_LAN_WWSKENN FROM FILIALEN WHERE FIL_ID = :var_N0_fil_id';
	$rsLAND = awisOpenRecordset($con, $SQLLAND,true,false,$BindeVariablen);
	$rsLANDZeilen = $awisRSZeilen;
	
	if($rsLAND['FIL_LAN_WWSKENN'][0]=='BRD')
	{
		$LAND='BRD';
	}
	elseif($rsLAND['FIL_LAN_WWSKENN'][0]=='OES')
	{
		$LAND='OES';		
	}
	elseif($rsLAND['FIL_LAN_WWSKENN'][0]=='CZE')
	{
		$LAND='CZE';
	}
	elseif($rsLAND['FIL_LAN_WWSKENN'][0]=='ITA')
	{
		$LAND='ITA';
	}
	elseif($rsLAND['FIL_LAN_WWSKENN'][0]=='NED')
	{
		$LAND='NED';
	}
	elseif($rsLAND['FIL_LAN_WWSKENN'][0]=='SUI')
	{
		$LAND='SUI';
	}
	else
	{
		$LAND='BRD';
	}	
}

/* L�nderauswahl f�r die Preise */
echo "<td><select accesskey=� name=txtLAN_CODE tabindex=32>";
if((($ReifenRechte&128)==128 AND strtoupper(substr($AWISBenutzer->BenutzerName(),0,4))!='FIL-') or (($ReifenRechte&128)==128 AND $LAND=='BRD'))
{
	echo "<option value=DE " . (isset($filSuche[18]) && $filSuche[18]=='DE'?"selected":"") . ">Deutschland</option>";
}
if(($ReifenRechte&2048)==2048 or (($ReifenRechte&128)==128 AND $LAND=='ITA'))
{
	echo "<option value=IT " . (isset($filSuche[18]) && $filSuche[18]=='IT'?"selected":"") . ">Italien</option>";
}
if(($ReifenRechte&4096)==4096 or (($ReifenRechte&128)==128 AND $LAND=='NED'))
{
	echo "<option value=NL " . (isset($filSuche[18]) && $filSuche[18]=='NL'?"selected":"") . ">Niederlande</option>";
}
if(($ReifenRechte&256)==256 or (($ReifenRechte&128)==128 AND $LAND=='OES'))
{
	echo "<option value=AT " . (isset($filSuche[18]) && $filSuche[18]=='AT'?"selected":"") . ">�sterreich</option>";
}
if(($ReifenRechte&8192)==8192 or (($ReifenRechte&128)==128 AND $LAND=='SUI'))
{
	echo "<option value=CH " . (isset($filSuche[18]) && $filSuche[18]=='CH'?"selected":"") . ">Schweiz</option>";
}
if(($ReifenRechte&512)==512 or (($ReifenRechte&128)==128 AND $LAND=='CZE'))
{
	echo "<option value=CZ " . (isset($filSuche[18]) && $filSuche[18]=='CZ'?"selected":"") . ">Tschechien</option>";
}


echo "</select></td>";

echo "<td><input type=checkbox value=on " . (isset($filSuche[14]) && $filSuche[14]=="on"?"checked":"") . " name=txtAuswahlSpeichern accesskey='s' tabindex=33></td>";



echo "</tr></table>";
}

// Ende Nicht-KB-Leute


echo  "<table width=700 border=0><tr>";
								// Spalte Sortierung
echo  "<td width=30% valign=top><table border=0><tr>";
echo  "<tr><td>Sortierung nach:</td></tr>";
echo  "<tr><td><input type=radio name=txtSort accesskey='n' value=0 " . (isset($filSuche[10]) && $filSuche[10]==0?"checked":"") . " tabindex=41> ATU-<u>N</u>ummer</td></tr>";
echo  "<tr><td><input type=radio name=txtSort accesskey='v' value=1 " . (isset($filSuche[10]) && $filSuche[10]==1?"checked":"") . " tabindex=42> <u>V</u>K</td></tr>";
echo  "<tr><td><input type=radio name=txtSort accesskey='p' value=2 " . (isset($filSuche[10]) && $filSuche[10]==2?"checked":"") . " tabindex=43> Aktions<u>p</u>reis</td></tr>";
echo  "<tr><td><input type=radio name=txtSort accesskey='i' value=3 " . (isset($filSuche[10]) && $filSuche[10]==3?"checked":"") . " tabindex=44> Speed <u>I</u>ndex</td></tr>";
echo  "<tr><td><input type=radio name=txtSort accesskey='t' value=4 " . (isset($filSuche[10]) && $filSuche[10]==4?"checked":"") . " tabindex=45> Hers<u>t</u>eller</td></tr>";
echo "</table></td>";		// Ende erste Spalte

								// Spalte Reifentyp

echo  "<td width=30% valign=top><table border=0><tr><td>Reifentyp:</td></tr>";
echo  "<tr><td><input type=radio name=txtTyp accesskey='e' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==0?"checked":""):"") . " value=0 tabindex=46> All<u>e</u></td></tr>";
echo  "<tr><td><input type=radio name=txtTyp accesskey='1' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==1?"checked":""):($DefTyp==1?"checked":"")) . " value=1 tabindex=47>Sommerreifen (<u>1</u>)</td></tr>";
echo  "<tr><td><input type=radio name=txtTyp accesskey='2' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==2?"checked":""):($DefTyp==2?"checked":"")) . " value=2 tabindex=48>Winterreifen (<u>2</u>)</td></tr>";
echo  "<tr><td><input type=radio name=txtTyp accesskey='3' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==3?"checked":""):"") . " value=3 tabindex=49>Allwetter (<u>3</u>)</td></tr>" ;
echo  "<tr><td><input type=radio name=txtTyp accesskey='4' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==4?"checked":""):"") . " value=4 tabindex=50>4x4/SUV Sommer (<u>4</u>)</td></tr>" ;
echo  "<tr><td><input type=radio name=txtTyp accesskey='5' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==5?"checked":""):"") . " value=5 tabindex=51>4x4/SUV Winter (<u>5</u>)</td></tr>" ;
echo  "<tr><td><input type=radio name=txtTyp accesskey='6' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==6?"checked":""):"") . " value=6 tabindex=52>LLKW Sommer (<u>6</u>)</td></tr>" ;
echo  "<tr><td><input type=radio name=txtTyp accesskey='7' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==7?"checked":""):"") . " value=7 tabindex=53>LLKW Winter (<u>7</u>)</td></tr>" ;
echo  "<tr><td><input type=radio name=txtTyp accesskey='8' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==8?"checked":""):"") . " value=8 tabindex=54>Notlauf-Sommer (<u>8</u>)</td></tr>" ;
echo  "<tr><td><input type=radio name=txtTyp accesskey='9' " . ($AlleReifenTypenMerken=='1'?($filSuche[11]==9?"checked":""):"") . " value=9 tabindex=55>Notlauf-Winter (<u>9</u>)</td></tr>" ;
echo "</table></td>";		// Ende zweite Spalte

								// Spalte
echo  "<td width=40% valign=top><table border=0><tr><td>&nbsp;</td></tr>";
if(awisBenutzerRecht($con,252)>0)		// Feste CC-Sortierung
{
	echo  "<tr><td><input readonly accesskey=c type=hidden name=txtCCSort checked value=on tabindex=62>&nbsp;&radic;&nbsp;&nbsp;<u>C</u>C-Sortierung</td></tr>";
}
else	// Frei w�hlbar
{
	echo  "<tr><td><input accesskey=c type=checkbox name=txtCCSort " . (isset($filSuche[12]) && $filSuche[12]=="on"?"checked":"") . " value=on tabindex=62><u>C</u>C-Sortierung</td></tr>";
}

echo  "<tr><td><input accesskey=p type=checkbox name=txtReifSpd " . (isset($filSuche[13]) && $filSuche[13]=="on"?"checked":"") . " value=on tabindex=63><u>R</u>eifen mit h�herem S<u>p</u>eedindex</td></tr>";
echo  "<tr><td><input accesskey=l type=checkbox name=txtReifLoad " . (isset($filSuche[20]) && $filSuche[20]=="on"?"checked":"") . " value=on tabindex=64>Reifen mit h�herem <u>L</u>oadindex</td></tr>";

$Recht251 = awisBenutzerRecht($con, 251);


If(($Recht251&1)==1)
{
	echo  "<tr><td><input accesskey=b type=checkbox name=txtKBListe " . (isset($filSuche[16]) && $filSuche[16]=="on"?"checked":"") . " value=on tabindex=64>K<u>B</u>Liste</td></tr>";
	If(($Recht251&4)==4)
	{
		echo  "<tr><td>&nbsp;&nbsp;&nbsp;<input accesskey=r type=checkbox name=txtKBPreise " . (isset($filSuche[21]) && $filSuche[21]=="on"?"checked":"") . " value=on tabindex=64>KB-P<u>r</u>eise</td></tr>";
	}
	//l�schen laut Anforderung 633 von Zeitler (TR 21.05.08)
	//echo  "<tr><td><input accesskey=z type=checkbox name=txtHerstellerZuerst " . (isset($filSuche[17]) && $filSuche[17]=="on"?"checked":"") . " value=on tabindex=65>Hersteller <u>z</u>uerst</td></tr>";
}
/*l�schen laut Anforderung 633 von Zeitler (TR 21.05.08)
if(($Recht251&2) == 2)
{
	echo  "<tr><td><input accesskey=h type=checkbox name=txtFahrzeuge " . (isset($filSuche[19]) && $filSuche[19]=="on"?"checked":"") . " value=on tabindex=69>Fa<u>h</u>rzeugliste ";
	echo  "&nbsp;<select name=txtFahrzeugZiffern>" ;
	echo  "<option value=0>Ziffer 20/21</option>";
	echo  "<option value=1>Ziffer 22/23</option>";
	echo  "<option value=2>Zusatztext</option>";
	echo  "<option value=3 selected>Alle</option>";
	echo  "</select></tr></td>";
}
*/

echo "</table></td>";		// Ende dritte Spalte

echo "</tr></table>";

//echo "<br><input type=submit name=cmdSuche value=\"Aktualisieren\">";
//echo "<input type=submit name=cmdReset accesskey=r value=Reset>";
echo "<input type=hidden name=cmdAktion value=Reifen>";
echo "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png title='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
echo "&nbsp;<img tabindex=99 src=/bilder/radierer.png alt='Formularinhalt l�schen' name=cmdReset onclick=location.href='./reifen_Main.php?Reset=True';>";

echo "</form>";

echo "<Script Language=JavaScript>";
echo "document.getElementsByName(\"txtBreite\")[0].focus();";
echo "</Script>";

?>