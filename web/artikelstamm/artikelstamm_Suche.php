<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author    Sacha Kerres
 * @copyright ATU
 * @version   20081006
 *
 *
 */
global $AWISCursorPosition;

try {
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('Wort', 'Auswahl_ALLE');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'ARTIKELBEZEICHNUNG');
    $TextKonserven[] = array('Wort', 'KOMMENTAR');
    $TextKonserven[] = array('Wort', 'WARENGRUPPE');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'ttt_AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'OEPreiseZeigen');
    $TextKonserven[] = array('Wort', 'FremdkaufZeigen');
    $TextKonserven[] = array('Wort', 'MaximaleDatensatzAnzahl');
    $TextKonserven[] = array('Wort', 'MaximaleDatensatzAbfrage');
    $TextKonserven[] = array('Wort', 'proBereich');
    $TextKonserven[] = array('Wort', 'ZuSuchendeNummer');
    $TextKonserven[] = array('Wort', 'ttt_ZuSuchendeNummer');
    $TextKonserven[] = array('Wort', 'ExakteSuche');
    $TextKonserven[] = array('Wort', 'ttt_ExakteSuche');
    $TextKonserven[] = array('Wort', 'SucheNach');
    $TextKonserven[] = array('Wort', 'EANNummer');
    $TextKonserven[] = array('Wort', 'GebrauchsNummer');
    $TextKonserven[] = array('Wort', 'LieferantenArtikelNummer');
    $TextKonserven[] = array('Wort', 'LieferantenSet');
    $TextKonserven[] = array('Wort', 'OENummer');
    $TextKonserven[] = array('Wort', 'OnlineShopNummer');
    $TextKonserven[] = array('Wort', 'Filialpreis');
    $TextKonserven[] = array('Wort', 'BeliebigeNummer');
    $TextKonserven[] = array('Wort', 'ttt_BeliebigeNummer');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht450 = $AWISBenutzer->HatDasRecht(450);        // Rechte des Mitarbeiters

    $Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./artikelstamm_Main.php?cmdAktion=Artikelinfo>");

    $Recht452 = $AWISBenutzer->HatDasRecht(452);            // Lieferantenartikel

    $Recht403 = $AWISBenutzer->HatDasRecht(403);        // OE-Preise
    $Recht451 = $AWISBenutzer->HatDasRecht(451);        // Artikelstamm (Sonderfelder)

    // Alle Parameter auslesen
    //	Es wird aber nicht alles eingeblendet!
    $Param = explode(";", ($AWISBenutzer->ParameterLesen('ArtikelStammSuche')));

    $AWISBenutzer->ParameterSchreiben('ArtikelStammSucheFiliale', '');

    if (!is_array($Param)) {
        $Param = array_fill(0, 20, '');
    }

    /**********************************************
     * * Eingabemaske
     ***********************************************/

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['txt_AST_ATUNR_SUCHE'] . ':', 240);
    $Java = 'onchange=ClearFeld(1)';
    $Form->Erstelle_TextFeld('*AST_ATUNR', ($Param[0] == 'on'?$Param[1]:''), 10, 200, true, '', '', '', 'T', '', $AWISSprachKonserven['AST']['ttt_AST_ATUNR'], '', 8, $Java, 'U');
    $AWISCursorPosition = 'sucAST_ATUNR';

    //Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
    $UserFilialen = $AWISBenutzer->FilialZugriff(0, awisBenutzer::FILIALZUGRIFF_STRING);
    // Filiale	
    if ($UserFilialen != '') {
        echo '<input type=hidden name="sucFIL_ID" value="' . $UserFilialen . '">';
    } else {
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Filialpreis'] . ':', 100);
        $Form->Erstelle_TextFeld('*FIL_ID', ($Param[0] == 'on'?$Param[16]:''), 10, 200, true, '', '', '', 'T', '', 'Filialpreis f�r Filiale suchen', '', 4);
    }
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['ARTIKELBEZEICHNUNG'] . ':', 240);
    $Form->Erstelle_TextFeld('*AST_BEZEICHNUNG', ($Param[0] == 'on'?$Param[2]:''), 40, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['AST']['ttt_ARTIKELBEZEICHNUNG'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['KOMMENTAR'] . ':', 240);
    $Form->Erstelle_TextFeld('*KOMMENTAR', ($Param[0] == 'on'?$Param[3]:''), 40, 0, true, '', '', '', 'T', '', $AWISSprachKonserven['AST']['ttt_ARTIKELBEZEICHNUNG'], '', 50);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['WARENGRUPPE'] . ':', 240);
    $SQL = 'SELECT * FROM WARENGRUPPEN ';
    $SQL .= ' ORDER BY WGR_Bezeichnung';

    $SQL = 'SELECT DISTINCT WGR_ID, WGR_BEZEICHNUNG ';
    $SQL .= ' FROM WARENGRUPPEN ';
    $SQL .= ' INNER JOIN WARENUNTERGRUPPEN ON WGR_ID = WUG_WGR_ID ';
    $SQL .= ' WHERE (SELECT COUNT(*) FROM Artikelstamm WHERE AST_WUG_KEY = WUG_KEY) > 0';
    $SQL .= ' ORDER BY WGR_Bezeichnung';
    $Form->Erstelle_SelectFeld('*WARENGRUPPE', ($Param[0] == 'on'?$Param[4]:''), 200, true, $SQL, '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '-1', '', '', '', '',
        $AWISSprachKonserven['AST']['ttt_WARENGRUPPE']);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_WUG_KEY'] . ':', 240);
    $SQL = 'SELECT WUG_KEY, WUG_BEZEICHNUNG, WGR_BEZEICHNUNG ';
    $SQL .= ' FROM WARENGRUPPEN ';
    $SQL .= ' INNER JOIN WARENUNTERGRUPPEN ON WGR_ID = WUG_WGR_ID ';
    $SQL .= ' WHERE WUG_KEY > 0 ';
    $SQL .= ' AND (SELECT COUNT(*) FROM Artikelstamm WHERE AST_WUG_KEY = WUG_KEY) > 0';
    $SQL .= ' ORDER BY WGR_Bezeichnung, WUG_Bezeichnung';
    $Form->Erstelle_SelectFeld('*AST_WUG_KEY', ($Param[0] == 'on'?$Param[5]:''), 200, true, $SQL, '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '', '',
        $AWISSprachKonserven['AST']['ttt_AST_WUG_KEY']);
    $Form->ZeileEnde();

    $Form->Trennzeile('L');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['ZuSuchendeNummer'] . ':', 240);
    $Java = 'onchange=ClearFeld(2)';
    $Form->Erstelle_TextFeld('*Nummer', ($Param[0] == 'on'?$Param[6]:''), 30, 250, true, '', '', '', 'T', '', $AWISSprachKonserven['Wort']['ttt_ZuSuchendeNummer'], '', 30, $Java,
        'M');
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['ExakteSuche'] . ':', 150);
    $Form->Erstelle_Checkbox('*ExakteSuche', ($Param[0] == 'on'?$Param[7]:''), 30, true, 'on', '', $AWISSprachKonserven['Wort']['ttt_ExakteSuche']);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['SucheNach'] . ':', 240);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['EANNummer'] . ':', 240);
    $Form->Erstelle_Checkbox('*EANNummer', ($Param[0] == 'on'?$Param[8]:''), 30, true, 'on');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['GebrauchsNummer'] . ':', 240);
    $Form->Erstelle_Checkbox('*GebrauchsNummer', ($Param[0] == 'on'?$Param[9]:''), 30, true, 'on');
    $Form->ZeileEnde();

    if ($Recht452 & 1) {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['LieferantenArtikelNummer'] . ':', 240);
        $Form->Erstelle_Checkbox('*LieferantenArtikelNummer', ($Param[0] == 'on'?$Param[10]:''), 30, true, 'on');
        $Form->ZeileEnde();

        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['LieferantenSet'] . ':', 240);
        $Form->Erstelle_Checkbox('*LieferantenSet', ($Param[0] == 'on'?$Param[11]:''), 30, true, 'on');
        $Form->ZeileEnde();
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['OENummer'] . ':', 240);
    $Form->Erstelle_Checkbox('*OENummer', ($Param[0] == 'on'?$Param[12]:''), 30, true, 'on');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['OnlineShopNummer'] . ':', 240);
    $Form->Erstelle_Checkbox('*OnlineShopNummer', ($Param[0] == 'on'?$Param[20]:''), 30, true, 'on');
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['BeliebigeNummer'] . ':', 240);
    $Form->Erstelle_Checkbox('*BeliebigeNummer', ($Param[0] == 'on'?$Param[13]:''), 30, true, 'on', '', $AWISSprachKonserven['Wort']['ttt_BeliebigeNummer']);
    $Form->ZeileEnde();

    $Form->Trennzeile('L');

    if (($Recht403 & 4) == 4) {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['OEPreiseZeigen'] . ':', 240);
        $Form->Erstelle_Checkbox('*OEPreiseZeigen', ($Param[0] == 'on'?$Param[14]:''), 30, true, 'on');
        $Form->ZeileEnde();
    }

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['FremdkaufZeigen'] . ':', 240);
    $Form->Erstelle_Checkbox('*FremdkaufZeigen', ($Param[0] == 'on'?$Param[15]:''), 30, true, 'on');
    $Form->ZeileEnde();

    $Form->Trennzeile('L');

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 240);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param[0] == 'on'?'on':''), 30, true, 'on', '', $AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
    $Form->ZeileEnde();

    if (($Recht451 & 1) == 1) {
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['MaximaleDatensatzAnzahl'] . ':', 240);
        $MaxDS = ($AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe'));
        $Link = '/BenutzerParameter.php';
        $Form->Erstelle_TextLabel($MaxDS . ' ' . $AWISSprachKonserven['Wort']['proBereich'], 400, '', $Link);
        $Form->ZeileEnde();
    }

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    // Zur�ck zum Men�
    $Form->Schaltflaeche('href', 'cmdZurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    if (($Recht450 & 4) == 4)        // Hinzuf�gen erlaubt?
    {
        $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    $Form->Schaltflaeche('script', 'cmdHilfe',
        "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=artikelstamm&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png',
        $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

    $Form->SchaltflaechenEnde();

    if ($AWISCursorPosition != '') {
        echo '<Script Language=JavaScript>';
        echo "document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();";

        // Javafunktion zum L�schen des Suchefeldes
        echo 'function ClearFeld(Feld)';
        echo '{';
        echo 'if (Feld == 2) {';
        echo "    document.getElementsByName(\"sucAST_ATUNR\")[0].value='';";
        echo '} else {';
        echo "    document.getElementsByName(\"sucNummer\")[0].value='';";
        echo '}';
        echo '  return(0)';
        echo '}';

        echo '</Script>';
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060005");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "20081��61223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>