<?php
/**
 * Bearbeitung der R�ckf�hrungen durch die Filiale bzw. Zentrale
 * 
 * @author Thomas Riedl
 * @copyright ATU Auto Teile Unger
 * @version 200809291144
 */

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try 
{	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('RFK','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_Hilfe');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNeinUnbekannt');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_RueckfuehrungLagerwarescheinErw');
	$TextKonserven[]=array('Fehler','err_RueckfuehrungWarenbestaendeErw');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	$TextKonserven[]=array('Wort','lbl_RueckfuehrungsscheinDrucken');
	$TextKonserven[]=array('Wort','lbl_RueckfuehrungsdifferenzenDrucken');
	$TextKonserven[]=array('Wort','lbl_RueckfuehrungslisteDrucken');
	$TextKonserven[]=array('Wort','KeineBerechtigungRueckfuehrung');
	$TextKonserven[]=array('Wort','AlleAnzeigen');
	$TextKonserven[]=array('Wort','Abschliessen');
	$TextKonserven[]=array('Wort','Abbrechen');
	$TextKonserven[]=array('Wort','Abschlussdatum');
	$TextKonserven[]=array('RFS','lst_RFS_STATUS');
	
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht3920 = $AWISBenutzer->HatDasRecht(3920);
	if($Recht3920==0)
	{
		$Form->Fehler_KeineRechte();
	}
			
	//********************************************************
	// Parameter ?
	//********************************************************
	$Param = array_fill(0,15,'');	
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);

	if(isset($_POST['cmdSuche_x']))
	{
		$Param['KEY']=0;			// Key
		
		$Param['RFK_FIL_ID']=$_POST['sucRFK_FIL_ID'];	
		$Param['RFK_RUECKFUEHRUNGSNR']=$_POST['sucRFK_RUECKFUEHRUNGSNR'];	
		$Param['RFK_BOXART']=$_POST['sucRFK_BOXART'];	
		$Param['RFK_BOXEAN']=$_POST['sucRFK_BOXEAN'];	
		$Param['RFF_AST_ATUNR']=$_POST['sucRFF_AST_ATUNR'];	
		$Param['RFS_STATUS']=$_POST['sucRFS_STATUS'];
		$Param['RFK_DATUMABSCHLUSSFIL']=$_POST['sucRFK_DATUMABSCHLUSSFIL'];	
		$Param['RFK_DATUMABSCHLUSSZEN']=$_POST['sucRFK_DATUMABSCHLUSSZEN'];	
		$Param['RFF_BEMERKUNGZENTRALE']=(isset($_POST['sucRFF_BEMERKUNGZENTRALE'])?$_POST['sucRFF_BEMERKUNGZENTRALE']:'');	

		$Param['SPEICHERN']=(isset($_POST['sucAuswahlSpeichern'])?'on':'');
		$Param['RFOK']='';
	
		$Param['ORDERBY']='';
		$Param['BLOCK']='';
		$Param['KEY']=0;
	
		$AWISBenutzer->ParameterSchreiben("RFKSuche",serialize($Param));
		$AWISBenutzer->ParameterSchreiben("AktuellerRFK",'');					
	}	

	//********************************************************
	// Parameter verarbeiten
	//********************************************************
	if(isset($_POST['cmdDSZurueck_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen("RFKSuche"));
		$Bedingung = _BedingungErstellen($Param);


		if ($_POST['txtRFK_RUECKFUEHRUNGSNR']!='')
		{
			$Bedingung .= ' AND RFK_RUECKFUEHRUNGSNR < '.floatval($_POST['txtRFK_RUECKFUEHRUNGSNR']);	
		}
		else 
		{
			$Bedingung .= ' AND RFK_RUECKFUEHRUNGSNR is not null';	
		}
		
		$SQL = 'SELECT RFK_KEY FROM ';
		$SQL .= '( SELECT RFK_KEY FROM RUECKFUEHRUNGSFILIALMENGENKOPF';
		$SQL .= '  WHERE '.substr($Bedingung,4);
		$SQL .= '  ORDER BY RFK_RUECKFUEHRUNGSNR DESC';
		$SQL .= ') WHERE ROWNUM = 1';
		
		$Form->DebugAusgabe(1,$SQL);
		
		$rsRFK = $DB->RecordSetOeffnen($SQL);
		if ($rsRFK->FeldInhalt('RFK_KEY')!='')
		{
			$AWIS_KEY1 = $rsRFK->FeldInhalt('RFK_KEY');
		}
		else 
		{
			$AWIS_KEY1 = $_POST['txtRFK_KEY'];
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
		$Param = unserialize($AWISBenutzer->ParameterLesen("RFKSuche"));
		$Bedingung = _BedingungErstellen($Param);

		if ($_POST['txtRFK_RUECKFUEHRUNGSNR']!='')
		{
			$Bedingung .= ' AND RFK_RUECKFUEHRUNGSNR > '.floatval($_POST['txtRFK_RUECKFUEHRUNGSNR']);	
			$Bedingung .= ' OR RFK_RUECKFUEHRUNGSNR is null';	
		}
		else 
		{
			$Bedingung .= ' AND RFK_RUECKFUEHRUNGSNR is null';	
		}
		
		$SQL = 'SELECT RFK_KEY FROM ';
		$SQL .= '( SELECT RFK_KEY FROM RUECKFUEHRUNGSFILIALMENGENKOPF';
		$SQL .= '  WHERE '.substr($Bedingung,4);
		$SQL .= '  ORDER BY RFK_RUECKFUEHRUNGSNR ASC';
		$SQL .= ') WHERE ROWNUM = 1';
		$Form->DebugAusgabe(1,$SQL);
		$rsRFK = $DB->RecordSetOeffnen($SQL);

		if ($rsRFK->FeldInhalt('RFK_KEY')!='')
		{
			$AWIS_KEY1 = $rsRFK->FeldInhalt('RFK_KEY');
		}
		else 
		{
			$AWIS_KEY1 = $_POST['txtRFK_KEY'];
		}
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./rueckfuehrungsbearbeitung_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./rueckfuehrungsbearbeitung_speichern.php');
	}
	elseif(isset($_POST['cmd_VerbuchenZentrale_x']) or isset($_POST['cmd_VerbuchenZentrale2_x']))
	{
		include('./rueckfuehrungsbearbeitung_abschliessen.php');
	}
	elseif(isset($_POST['cmdAbschliessenOK']))
	{
		include('./rueckfuehrungsbearbeitung_abschliessen.php');
	}
	elseif(isset($_POST['cmdAbschliessenFilialeOK']) or isset($_POST['cmdAbschliessenZentraleOK']))
	{
		include('./rueckfuehrungsbearbeitung_speichern.php');
		include('./rueckfuehrungsbearbeitung_abschliessen.php');
	}elseif (isset($_POST['cmdAdmin_x'])){
        include('./rueckfuehrungsbearbeitung_speichern.php');
    }
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_GET['RFK_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['RFK_KEY']);
	}

	else 		// Letzten Benutzer suchen
	{			
		if(!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block']))
		{			
			$AWIS_KEY1 = $AWISBenutzer->ParameterLesen("AktuellerRFK");			
						
			if (intval($AWIS_KEY1) <= 0)
			{
				$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
				if ($UserFilialen != '')
				{
					$Param['RFK_FIL_ID']=$UserFilialen;	
					$AWISBenutzer->ParameterSchreiben("AktuellerRFK",0);
					$AWISBenutzer->ParameterSchreiben("RFKSuche",serialize($Param));
				}
			}
		}		
		
		if($Param=='' OR $Param=='0')
		{			
			$AWISBenutzer->ParameterSchreiben("AktuellerRFK",0);			
		}								
	}
	
	$Param = unserialize($AWISBenutzer->ParameterLesen("RFKSuche"));

	if(isset($_POST['cmd_AbschliessenFiliale_x'])) //NUR BEI FIL OR isset($_POST['cmd_AbschliessenZentrale_x']))
	{
		//20130713,ho A006881/A008720
		$SQL = 'SELECT DISTINCT RFK_KEY, RFF_AST_ATUNR, AST_WGR_ID, AST_WUG_ID';
		$SQL .= ' FROM RueckFuehrungsFilialMengenKopf';
		$SQL .= ' LEFT JOIN RueckFuehrungsFilialMengenPos';
		$SQL .= ' ON RFK_KEY = RFF_RFK_KEY';
		$SQL .= ' LEFT JOIN v_artikelstammwgr';
		$SQL .= ' ON ast_atunr=rff_ast_atunr';
		$SQL .= ' WHERE RFK_KEY=0'.$DB->FeldInhaltFormat('N0',$AWIS_KEY1);
		$SQL .= ' ORDER BY RFF_AST_ATUNR';
		$rsRF = $DB->RecordSetOeffnen($SQL);
		$Form->DebugAusgabe(1,$DB->LetzterSQL());
	
		$Fehler = '';
		$T1Param = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelBadWG',true));
		$T2Param = explode(';', $AWISBenutzer->ParameterLesen('RFArtikelBadArtikel',true));
		while (!$rsRF->EOF())
		{
			foreach ($T1Param as $WGSort)
			{
				$WGSort = explode('~',$WGSort);
				if (isset($WGSort[0]) AND $WGSort[0]!='')//WG
				{
					if (isset($WGSort[1]) AND $WGSort[1]!='')//SORT
					{
						if ($rsRF->FeldInhalt('AST_WGR_ID')==$WGSort[0] AND $rsRF->FeldInhalt('AST_WUG_ID') == $WGSort[1])
						{
							$Fehler .= $AWISSprachKonserven['Fehler']['err_RueckfuehrungLagerwarescheinErw'].'<br>';
							$Fehler = str_replace('$ATUNR$',$rsRF->FeldInhalt('RFF_AST_ATUNR'),$Fehler);
							break;
						}
					}
					else
					{
						if ($rsRF->FeldInhalt('AST_WGR_ID')==$WGSort[0])
						{
							$Fehler .= $AWISSprachKonserven['Fehler']['err_RueckfuehrungLagerwarescheinErw'].'<br>';
							$Fehler = str_replace('$ATUNR$',$rsRF->FeldInhalt('RFF_AST_ATUNR'),$Fehler);
							break;
						}
					}
				}
				elseif (isset($WGSort[1]) AND $WGSort[1]!='')//SORT
				{
					if ($rsRF->FeldInhalt('AST_WUG_ID')==$WGSort[1])
					{
						$Fehler .= $AWISSprachKonserven['Fehler']['err_RueckfuehrungLagerwarescheinErw'].'<br>';
						$Fehler = str_replace('$ATUNR$',$rsRF->FeldInhalt('RFF_AST_ATUNR'),$Fehler);
						break;
					}
				}
			}
	
			foreach ($T2Param as $Artikelbereich)
			{
				$Artikelbereich = explode('-',$Artikelbereich);
				if ($rsRF->FeldInhalt('RFF_AST_ATUNR') >= $Artikelbereich[0] AND $rsRF->FeldInhalt('RFF_AST_ATUNR') <= $Artikelbereich[1])
				{
					$Fehler .= $AWISSprachKonserven['Fehler']['err_RueckfuehrungLagerwarescheinErw'].'<br>';
					$Fehler = str_replace('$ATUNR$',$rsRF->FeldInhalt('RFF_AST_ATUNR'),$Fehler);
					break;
				}
			}
							
			$rsRF->DSWeiter();
		}
		
		// Wurden Fehler entdeckt? => Abschlie�en abbrechen
		if($Fehler!='')
		{
			$Form->ZeileStart();
			$Form->Hinweistext($Fehler);
			$Form->ZeileEnde();
			unset($_POST['cmd_AbschliessenFiliale_x']);
		}
	}
	
	//********************************************************
	// Bedingung erstellen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);
	
	//*****************************************************************
	// Sortierung aufbauen
	//*****************************************************************
	if(!isset($_GET['Sort']))
	{
		if(isset($Param['ORDERBY']) AND $Param['ORDERBY']!='')
		{
			$ORDERBY = $Param['ORDERBY'];
		}
		else
		{
			$ORDERBY = ' RFK_KEY DESC';
		}		
	}
	else
	{
		$ORDERBY = ' '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	
	//********************************************************
	// Daten suchen
	//********************************************************
	$SQL = 'SELECT DISTINCT RFK_KEY, RFK_FIL_ID, RFK_RUECKFUEHRUNGSNR, RFK_PERSNRFILIALE';
	$SQL .= ' , RFK_PERSNRZENTRALE, RFK_BEMERKUNGFILIALE, RFK_BEMERKUNGZENTRALE, RFK_BOXART';
	$SQL .= ' , RFK_BOXEAN, RFK_DATUMABSCHLUSSFIL, RFK_DATUMABSCHLUSSZEN, RFK_USER, RFK_USERDAT';
	$SQL .= ' , RFS_BEZEICHNUNG, RFS_STATUS, FILNAME, FILVORNAME, ZENNAME, ZENVORNAME';
	$SQL .= ' , ABSCHLUSSDATUM, RFK_RFS_KEY';
	if($AWIS_KEY1<=0)
	{
		$SQL .= ', row_number() over (order by '.$ORDERBY.') AS ZeilenNr';
	}	
	$SQL .= ' FROM (';	
	$SQL .= 'SELECT DISTINCT RFK.RFK_KEY, RFK.RFK_FIL_ID, RFK.RFK_RUECKFUEHRUNGSNR, RFK.RFK_PERSNRFILIALE';
	$SQL .= ' , RFK.RFK_PERSNRZENTRALE, RFK.RFK_BEMERKUNGFILIALE, RFK.RFK_BEMERKUNGZENTRALE, RFK_BOXART';
	$SQL .= ' , RFK.RFK_BOXEAN, RFK.RFK_DATUMABSCHLUSSFIL, RFK.RFK_DATUMABSCHLUSSZEN, RFK_USER, RFK_USERDAT';
	$SQL .= ' , RFS.RFS_BEZEICHNUNG, RFS.RFS_STATUS, FIL.NAME as FILNAME, FIL.VORNAME as FILVORNAME, ZEN.NAME as ZENNAME, ZEN.VORNAME as ZENVORNAME';
	$SQL .= ' , NVL(RFK_DATUMABSCHLUSSZEN, RFK_DATUMABSCHLUSSFIL) as ABSCHLUSSDATUM, RFK.RFK_RFS_KEY';
	$SQL .= ' FROM RueckFuehrungsFilialMengenKopf RFK ';
	$SQL .= ' LEFT JOIN RUECKFUEHRUNGSSTATUS RFS ON (RFK.RFK_RFS_KEY=RFS.RFS_KEY)';
	$SQL .= ' LEFT JOIN RUECKFUEHRUNGSFILIALMENGENPOS RFF ON (RFK.RFK_KEY=RFF.RFF_RFK_KEY)';
	$SQL .= ' LEFT JOIN V_PERSONAL_KOMPLETT FIL ON (FIL.PERSNR = RFK_PERSNRFILIALE)';
	$SQL .= ' LEFT JOIN V_PERSONAL_KOMPLETT ZEN ON (ZEN.PERSNR = RFK_PERSNRZENTRALE)';
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}	
	
	$SQL .= ' GROUP BY RFK.RFK_KEY, RFK.RFK_FIL_ID, RFK.RFK_RUECKFUEHRUNGSNR, RFK.RFK_PERSNRFILIALE';
	$SQL .= ' , RFK.RFK_PERSNRZENTRALE, RFK.RFK_BEMERKUNGFILIALE, RFK.RFK_BEMERKUNGZENTRALE, RFK_BOXART';
	$SQL .= ' , RFK.RFK_BOXEAN, RFK.RFK_DATUMABSCHLUSSFIL, RFK.RFK_DATUMABSCHLUSSZEN, RFK_USER, RFK_USERDAT';
	$SQL .= ' , RFS.RFS_BEZEICHNUNG, RFS.RFS_STATUS, FIL.NAME, FIL.VORNAME, ZEN.NAME, ZEN.VORNAME';
	$SQL .= ' , NVL(RFK_DATUMABSCHLUSSZEN, RFK_DATUMABSCHLUSSFIL), RFK.RFK_RFS_KEY';
	$SQL .= ' )';
	$SQL .= ' ORDER BY '.$ORDERBY;
	$Form->DebugAusgabe(1,$SQL);				
	
	//************************************************
	// Aktuellen Datenblock festlegen
	//************************************************
	$Block = 1;
	if(isset($_REQUEST['Block']))
	{
		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
	}
	elseif(isset($Param['BLOCK']) AND $Param['BLOCK']!='')
	{
		$Block = intval($Param['BLOCK']);
	}
	
	//************************************************
	// Zeilen begrenzen
	//************************************************
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		
	//*****************************************************************
	// Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
	//*****************************************************************
	if($AWIS_KEY1<=0)
	{
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}

	$rsRFK = $DB->RecordSetOeffnen($SQL);
	
	//*****************************************************************
	// Aktuelle Parameter sichern
	//*****************************************************************	
	$Param['ORDERBY']=$ORDERBY;
	$Param['KEY']=$AWIS_KEY1;
	$Param['BLOCK']=$Block;
	$AWISBenutzer->ParameterSchreiben("RFKSuche",serialize($Param));
	
	$Form->SchreibeHTMLCode('<form name=frmRueckfuehrungsFilialMengen action=./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');
			
	//********************************************************
	// Daten anzeigen
	//********************************************************	
	if($rsRFK->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';
	}
	elseif(($rsRFK->AnzahlDatensaetze()>1) or (isset($_GET['Liste'])))						// Liste anzeigen
	{
		$Form->Formular_Start();
	
		$Form->ZeileStart();
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=RFK_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFK_FIL_ID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFK']['RFK_FIL_ID'],60,'',$Link);
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=RFK_RUECKFUEHRUNGSNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFK_RUECKFUEHRUNGSNR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RFK']['RFK_RUECKFUEHRUNGSNR'],180,'',$Link);
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=RFS_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='RFS_STATUS'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],300,'',$Link);				
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=ABSCHLUSSDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ABSCHLUSSDATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Abschlussdatum'],130,'',$Link);						
		$Form->ZeileEnde();
		
		$RFKZeile=0;
		
		while(!$rsRFK->EOF())
		{
			$Form->ZeileStart();
			$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&RFK_KEY='.$rsRFK->FeldInhalt('RFK_KEY').'';
			$Form->Erstelle_ListenFeld('RFK_FIL_ID',$rsRFK->FeldInhalt('RFK_FIL_ID'),0,60,false,($RFKZeile%2),'',$Link,'T');
			$Form->Erstelle_ListenFeld('RFK_RUECKFUEHRUNGSNR',$rsRFK->FeldInhalt('RFK_RUECKFUEHRUNGSNR'),0,180,false,($RFKZeile%2),'','');

			$RueckStatus='';
			$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFS_STATUS']);			
			foreach ($StatusText as $Status)
			{
				$RFStatus = explode("~",$Status);				
				if (intval($RFStatus[0])==intval($rsRFK->FeldInhalt('RFS_STATUS')))
				{
					$RueckStatus = $RFStatus[1];					
				}				
			}
			$Form->Erstelle_ListenFeld('RFS_STATUS',$RueckStatus,0,300,false,($RFKZeile%2),'','','T','L','');											

			$Form->Erstelle_ListenFeld('ABSCHLUSSDATUM',$rsRFK->FeldInhalt('ABSCHLUSSDATUM'),0,130,false,($RFKZeile%2),'','','D','L','');						
			$Form->ZeileEnde();
	
			$rsRFK->DSWeiter();
			$RFKZeile++;
		}
	
		$Link = './rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Liste=True'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
	
		$Form->Formular_Ende();
	
		//***************************************
		// Schaltfl�chen f�r dieses Register
		//***************************************
		$Form->SchaltflaechenStart();
		$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		if(($Recht3920&4)== 4 AND !isset($_POST['cmdDSNeu_x']))		// Hinzuf�gen erlaubt?
		{
			$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
		}
		$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=rueckfuehrungsbearbeitung&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');
		$Form->SchaltflaechenEnde();
	
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$Form->SchreibeHTMLCode('<form name=frmRueckfuehrungsFilialMengen action=./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');	

		$AWIS_KEY1 = $rsRFK->FeldInhalt('RFK_KEY');		
		
		$AWISBenutzer->ParameterSchreiben("AktuellerRFK",$AWIS_KEY1);
		$Form->Erstelle_HiddenFeld('RFK_KEY',$AWIS_KEY1);	
	
		$Form->Formular_Start();	
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./rueckfuehrungsbearbeitung_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRFK->FeldInhalt('RFK_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsRFK->FeldInhalt('RFK_USERDAT'));
		$Form->InfoZeile($Felder,'');
	
		$EditRecht = false;	
		$EditRechtFiliale=false;
		$EditRechtZentrale=false;
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Filiale) hat und der Status 30 (R�ckf�hrung erstellt) ist
		if (($Recht3920&16)!=0 AND ($AWIS_KEY1 == 0 or $DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) == 30))
		{
			$EditRechtFiliale=true;	
			if ($AWIS_KEY1 == 0)
			{	
				$AWISCursorPosition='txtRFK_FIL_ID';
			}
			else 
			{
				$AWISCursorPosition='txtRFK_BEMERKUNGFILIALE';
			}
		}
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Zentrale) hat und der Status 31 (R�ckf�hrung - abgeschlossen - Filiale) ist
		if (($Recht3920&32)!=0 AND $DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) == 31)
		{
			$EditRechtZentrale=true;
			$AWISCursorPosition='txtRFK_BEMERKUNGZENTRALE';
		}
		
		//Wenn der User das Recht "R�ckf�hrungen-Bearbeiten (Filiale und Zentrale) 
		if (($Recht3920&64)!=0)
		{
			if ($DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) < 32)
			{
				$EditRechtFiliale=true;
				$EditRechtZentrale=true;
			}
			else 
			{
				$EditRechtFiliale=false;
				$EditRechtZentrale=false;				
			}
			
			if ($AWIS_KEY1 == 0)
			{	
				$AWISCursorPosition='txtRFK_FIL_ID';
			}
			else 
			{
				$AWISCursorPosition='txtRFK_BEMERKUNGFILIALE';
			}			
		}	
		
		//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
		$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_FIL_ID'].':',200);
		$SQL = "select FIL_ID, FIL_ID || ' - ' || FIL_BEZ ";
		$SQL .= ' FROM Filialen';
		$SQL .= ' WHERE FIL_GRUPPE IS NOT NULL';
		
		if($UserFilialen!='')
		{
			$SQL .= ' AND FIL_ID in (' .$UserFilialen .')';
		}
		
		
		$SQL .= ' ORDER BY FIL_ID';				
		
		$Form->Erstelle_SelectFeld('RFK_FIL_ID',($AWIS_KEY1==0?'':$rsRFK->FeldInhalt('RFK_FIL_ID')),200,($AWIS_KEY1==0?true:false),$SQL,$OptionBitteWaehlen);		
		$Form->ZeileEnde();
		
		if ($AWIS_KEY1!=0)
		{
			$Form->Erstelle_HiddenFeld('RFK_FIL_ID',$rsRFK->FeldInhalt('RFK_FIL_ID'));
		}
		
		$RueckStatus='';
		$StatusText = explode("|",$AWISSprachKonserven['RFS']['lst_RFS_STATUS']);			
		foreach ($StatusText as $Status)
		{
			$RFStatus = explode("~",$Status);				
			if (intval($RFStatus[0])==intval($rsRFK->FeldInhalt('RFS_STATUS')))
			{
				$RueckStatus = $RFStatus[1];					
			}				
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Status'].':',200);
		$Form->Erstelle_TextFeld('RFS_STATUS',($AWIS_KEY1===0?'':$RueckStatus),50,400,false,'','','','T','L');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_RUECKFUEHRUNGSNR'].':',200);
		$Form->Erstelle_TextFeld('RFK_RUECKFUEHRUNGSNR',($rsRFK->FeldInhalt('RFK_RUECKFUEHRUNGSNR')),50,350,false);
		$Form->Erstelle_HiddenFeld('RFK_RUECKFUEHRUNGSNR',$rsRFK->FeldInhalt('RFK_RUECKFUEHRUNGSNR'));
		$Form->ZeileEnde();	

				
		if (isset($_POST['cmd_AbschliessenFiliale_x']) or $DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) >= 31)
		{
			$Form->Trennzeile();
		
			if (!isset($_POST['cmd_AbschliessenFiliale_x']))
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_DATUMABSCHLUSSFIL'].':',200);
				$Form->Erstelle_TextFeld('RFK_DATUMABSCHLUSSFIL',($rsRFK->FeldInhalt('RFK_DATUMABSCHLUSSFIL')),50,350,false,'','','','D');
				$Form->ZeileEnde();			
			}
		
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_BEMERKUNGFILIALE'].':',200);
			$Form->Erstelle_TextFeld('RFK_BEMERKUNGFILIALE',($rsRFK->FeldInhalt('RFK_BEMERKUNGFILIALE')),50,350,$EditRechtFiliale,'','','','T','L','','',50);
			$Form->ZeileEnde();
			
			$Form->ZeileStart();		
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_BOXART'].':',200);
			$BoxText = explode("|",$AWISSprachKonserven['RFK']['lst_RFK_BOXART']);
			$Form->Erstelle_SelectFeld('RFK_BOXART',($rsRFK->FeldInhalt('RFK_BOXART')),100,$EditRechtFiliale,'',$OptionBitteWaehlen,'','','',$BoxText,'');
			$Form->ZeileEnde();		
			
			$Form->ZeileStart();		
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_BOXEAN'].':',200);
			$Form->Erstelle_TextFeld('RFK_BOXEAN',($rsRFK->FeldInhalt('RFK_BOXEAN')),50,350,$EditRechtFiliale,'','','','T','L','','',50);
			$Form->ZeileEnde();		
			
			if (isset($_POST['cmd_AbschliessenFiliale_x']))
			{
				$Form->ZeileStart();		
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_PERSNRFILIALE'].':',200);
				$Form->Erstelle_TextFeld('RFK_PERSNRFILIALE',($rsRFK->FeldInhalt('RFK_PERSNRFILIALE')),50,350,$EditRechtFiliale ,'','','','Z','L','','',10);
				$Form->ZeileEnde();	
			}
			else 
			{
				$Form->ZeileStart();		
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['BearbeiterFiliale'].':',200);
				$Form->Erstelle_TextFeld('BearbeiterFiliale',$rsRFK->FeldInhalt('FILVORNAME').' '.$rsRFK->FeldInhalt('FILNAME'),50,350,false ,'','','','T','L','','');
				$Form->ZeileEnde();	
			}
		
			if (isset($_POST['cmd_AbschliessenFiliale_x']))
			{	
				echo '<br><input type=submit name=cmdAbschliessenFilialeOK style="width:100px" value='.$AWISSprachKonserven['Wort']['Abschliessen'].'>';
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=submit name=cmdAbbrechen style="width:100px" value='.$AWISSprachKonserven['Wort']['Abbrechen'].'>';
			}
		}	
		
		if (isset($_POST['cmd_AbschliessenZentrale_x']) or $DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) >= 32)
		{
			$Form->Trennzeile();
			
			if (!isset($_POST['cmd_AbschliessenZentrale_x']))
			{				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_DATUMABSCHLUSSZEN'].':',200);
				$Form->Erstelle_TextFeld('RFK_DATUMABSCHLUSSZEN',($rsRFK->FeldInhalt('RFK_DATUMABSCHLUSSZEN')),50,350,false,'','','','D');
				$Form->ZeileEnde();	
			}
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_BEMERKUNGZENTRALE'].':',200);
			$Form->Erstelle_TextFeld('RFK_BEMERKUNGZENTRALE',($rsRFK->FeldInhalt('RFK_BEMERKUNGZENTRALE')),50,350,$EditRechtZentrale,'','','','T','L','','',50);
			$Form->ZeileEnde();						
			
			if (isset($_POST['cmd_AbschliessenZentrale_x']))
			{
				$Form->ZeileStart();		
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['RFK_PERSNRZENTRALE'].':',200);
				$Form->Erstelle_TextFeld('RFK_PERSNRZENTRALE',($rsRFK->FeldInhalt('RFK_PERSNRZENTRALE')),50,350,$EditRechtZentrale,'','','','Z','L','','',10);
				$Form->ZeileEnde();		
			}
			else 
			{
				$Form->ZeileStart();		
				$Form->Erstelle_TextLabel($AWISSprachKonserven['RFK']['BearbeiterZentrale'].':',200);
				$Form->Erstelle_TextFeld('BearbeiterZentrale',$rsRFK->FeldInhalt('ZENVORNAME').' '.$rsRFK->FeldInhalt('ZENNAME'),50,350,false,'','','','T','L','','');
				$Form->ZeileEnde();		
			}
			
			if (isset($_POST['cmd_AbschliessenZentrale_x']))
			{	
				echo '<br><input type=submit name=cmdAbschliessenZentraleOK style="width:100px" value='.$AWISSprachKonserven['Wort']['Abschliessen'].'>';
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=submit name=cmdAbbrechen style="width:100px" value='.$AWISSprachKonserven['Wort']['Abbrechen'].'>';
			}
		}
		
		if (!isset($_POST['cmd_AbschliessenFiliale_x']) and !isset($_POST['cmd_AbschliessenZentrale_x']))
		{
			$Form->Trennzeile();
			
			if($AWIS_KEY1!=0 AND ($Recht3920&32)!=0)
			{
				$Form->ZeileStart();		
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AlleAnzeigen'].':',200);				
				$Form->Erstelle_Checkbox('AlleAnzeigen','',50,true,'on');		
				$Form->ZeileEnde();		
				$Form->Formular_Ende();
			}
	
			if(!isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=0)
			{
				$RegisterSeite = (isset($_GET['Seite'])?$_GET['Seite']:(isset($_POST['Seite'])?$_POST['Seite']:'Positionen'));
				
				$RegDet = new awisRegister(3922);
				$RegDet->ZeichneRegister($RegisterSeite);		
			}
		
			//***************************************
			// Schaltfl�chen f�r dieses Register
			//***************************************
			$Form->SchaltflaechenStart();	
			
			$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		
			if(($Recht3920&6)!=0 AND ($EditRechtFiliale==true or $EditRechtZentrale==true))		//
			{
				$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
			}					
			
			if(($Recht3920&8)==8 AND !isset($_POST['cmdDSNeu_x']) AND ($EditRechtFiliale==true))
			{
				$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
			}
		
			if(!isset($_POST['cmdDSNeu_x']))
			{
				$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], ',');
				$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], '.');
				
				if (($EditRechtZentrale==true and !isset($_POST['cmd_AbschliessenZentrale_x'])) or $DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) >= 32)				
				{
					$Form->Schaltflaeche('image','lblname','','/bilder/cmd_weiter.png',$AWISSprachKonserven['Wort']['lbl_weiter'], 'W');
				}			
				
				if ($EditRechtFiliale==true and !isset($_POST['cmd_AbschliessenFiliale_x']) and $DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) == 30)
				{
					$Form->Schaltflaeche('image', 'cmd_AbschliessenFiliale','','/bilder/cmd_korb_runter.png', $AWISSprachKonserven['RFK']['RueckfuehrungAbschliessenFiliale'],'A');
				}							
				
				if ($EditRechtZentrale==true and !isset($_POST['cmd_AbschliessenZentrale_x']) and $DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) == 31)
				{
				
				    //Pr�fen, ob auf der R�ckf�hrung Artikel vorhanden sind, die nicht das R�ckliefkz "W"
				    $SQL = 'SELECT count(*) as Anzahl';
				    $SQL .= ' FROM RueckFuehrungsFilialMengenPos';
				    $SQL .= ' LEFT JOIN V_ARTIKEL_RUECKLIEFKZ ON RFF_AST_ATUNR = AST_ATUNR AND GUELTIG=0';
				    $SQL .= ' WHERE RFF_RFK_KEY=0'.$AWIS_KEY1;
				    $SQL .= ' AND (RUECKLIEFKZ <> \'W\' OR RUECKLIEFKZ IS NULL)';
				    	
				    $rsAnzahlW = $DB->RecordSetOeffnen($SQL);


					//Pr�fen, ob auf der R�ckf�hrung Artikel vorhanden sind, die nicht das R�ckliefkz "V" 
					$SQL = 'SELECT count(*) as Anzahl';
					$SQL .= ' FROM RueckFuehrungsFilialMengenPos';					
					$SQL .= ' LEFT JOIN V_ARTIKEL_RUECKLIEFKZ ON RFF_AST_ATUNR = AST_ATUNR AND GUELTIG=0';
					$SQL .= ' WHERE RFF_RFK_KEY=0'.$AWIS_KEY1;
					$SQL .= ' AND (RUECKLIEFKZ <> \'V\' OR RUECKLIEFKZ IS NULL)';
									
					$rsAnzahl = $DB->RecordSetOeffnen($SQL);

					$Form->Schaltflaeche('image', 'cmd_AbschliessenZentrale','','/bilder/cmd_korb_rauf.png', $AWISSprachKonserven['RFK']['RueckfuehrungAbschliessenZentrale'],'A');
				}
				if ($DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) < 31)
				{
					$Form->Schaltflaeche('href', 'cmd_DruckRueckFuehrungsListe','./rueckfuehrungsbearbeitung_Druck_RFS.php?DruckArt=1&RFK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_notizblock.png', $AWISSprachKonserven['Wort']['lbl_RueckfuehrungslisteDrucken'],'.');		
				}
				if ($DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) >= 31)
				{
					$Form->Schaltflaeche('href', 'cmd_DruckRueckFuehrungsLieferschein','./rueckfuehrungsbearbeitung_Druck_RFS.php?DruckArt=2&RFK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['lbl_RueckfuehrungsscheinDrucken'],'.');
				}
				
				
				if ($DB->FeldInhaltFormat('Z',$rsRFK->FeldInhalt('RFS_STATUS')) >= 32)
				{
					//Pr�fen ob bei der R�ckf�hrung Differenzen vorhanden sind 
					//Nur wenn Differenzen vorhanden sind, auch den Button f�r Differenzliste anzeigen
					$SQL = 'SELECT RFF_AST_ATUNR, RFF_MENGEFILIALE, RFF_MENGEZENTRALE';
					$SQL.= ' FROM RUECKFUEHRUNGSFILIALMENGENKOPF';
					$SQL.= ' LEFT JOIN RUECKFUEHRUNGSFILIALMENGENPOS ON RFF_RFK_KEY = RFK_KEY';
					$SQL.= ' WHERE RFK_KEY = 0' . $AWIS_KEY1 . '';	
					$SQL.= ' AND RFF_MENGEZENTRALE <> RFF_MENGEFILIALE';
					
					$rsDifferenzen = $DB->RecordSetOeffnen($SQL);
					$rsDifferenzenAnzahl = $rsDifferenzen->AnzahlDatensaetze();
				
					if ($rsDifferenzenAnzahl > 0)
					{
						$Form->Schaltflaeche('href', 'cmd_DruckRueckFuehrungsDifferenzen','./rueckfuehrungsbearbeitung_Druck_RFS.php?DruckArt=3&RFK_KEY=0'.$AWIS_KEY1,'/bilder/cmd_statistik.png', $AWISSprachKonserven['Wort']['lbl_RueckfuehrungsdifferenzenDrucken'],'.');			
					}
				}																	
			}

            $Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=rueckfuehrungsbearbeitung&Aktion=details','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

            /*
             * Adminfunktionen
             */
            if(($Recht3920&2048)==2048){
                $Form->Schaltflaeche('script','Sonderfunktionen',$Form->PopupOeffnen(2),'/bilder/cmd_schluessel.png','Sonderfunktionen','','',27,27,'',true);

                ob_start();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('Alle Typen auf: ',120,'text-align: right; margin-right: 5px');
                $SQL = 'select rft_kennung, rft_kennung || \'-\' || rft_bezeichnung ||\'(\'|| rft_wws_buchungskennziffer || \')\' from RUECKFUEHRUNGSTYP order by rft_kennung asc';
                $Form->Erstelle_SelectFeld('admin_RFT_AENDERN','',100,true,$SQL,'~Keine Ver�nderung');
                $Form->ZeileEnde();


                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('Menge-Zentrale: ',120,'text-align: right;  margin-right: 5px');
                $Optionen = array('~nicht �bernehmen','1~Menge Filiale �bernehmen');
                $Form->Erstelle_SelectFeld('admin_RFF_MENGEZENTRALE','',30,true,'','','','','',$Optionen);
                $Form->ZeileEnde();

                $PopUp = ob_get_clean();

                $Form->PopupDialog('Adminfunktionen',$PopUp,array(array('/bilder/cmd_dsloeschen.png','','close',''),array('/bilder/cmd_weiter.png','cmdAdmin','post','')),awisFormular::POPUP_INFO,2);
            }

            $Form->SchaltflaechenEnde();
		}
	
		$Form->SchreibeHTMLCode('</form>');
	}
	
	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
	 	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}	
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809291147");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200809161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
	
/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';
	
	if(floatval($AWIS_KEY1) != 0)
	{
		//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
		$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
		
		
		// Beschr�nkung auf eine Filiale?
		if ($UserFilialen != '')
		{
			$Bedingung.= 'AND RFK_FIL_ID IN ('.$UserFilialen.') ';
		}
		
		$Bedingung.= 'AND RFK_KEY = '.$AWIS_KEY1;
		return $Bedingung;
	}
			
	
	if(isset($Param['RFK_FIL_ID']) AND $Param['RFK_FIL_ID']!='')
	{
		$Bedingung .= 'AND RFK_FIL_ID in ( ' . $Param['RFK_FIL_ID'] . ') ';			
	}	
	
	if(isset($Param['RFK_RUECKFUEHRUNGSNR']) AND $Param['RFK_RUECKFUEHRUNGSNR']!='')
	{
		$Bedingung .= 'AND to_char(RFK_RUECKFUEHRUNGSNR) ' . $DB->LikeOderIst(str_replace('-','',$Param['RFK_RUECKFUEHRUNGSNR']),0) . ' ';
	}		
	
	if(isset($Param['RFK_BOXART']) AND floatval($Param['RFK_BOXART'])!=0)
	{		
		$Bedingung .= 'AND RFK_BOXART = ' . $DB->FeldInhaltFormat('N0',$Param['RFK_BOXART']) . ' ';			
	}		
	
	if(isset($Param['RFK_BOXEAN']) AND $Param['RFK_BOXEAN']!='')
	{
		$Bedingung .= 'AND RFK_BOXEAN = ' . $DB->FeldInhaltFormat('T',$Param['RFK_BOXEAN']) . ' ';			
	}

	if(isset($Param['RFF_AST_ATUNR']) AND $Param['RFF_AST_ATUNR']!='')
	{				
		$Bedingung .= 'AND RFF_AST_ATUNR ' . $DB->LikeOderIst($Param['RFF_AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';			
	}		
	
	if(isset($Param['RFS_STATUS']) AND $Param['RFS_STATUS']!='')
	{		
		$Bedingung .= 'AND RFK_RFS_KEY = (SELECT RFS_KEY FROM RUECKFUEHRUNGSSTATUS WHERE RFS_STATUS =' . $DB->FeldInhaltFormat('NO',$Param['RFS_STATUS']) . ') ';			
	}		
	
	if(isset($Param['RFK_DATUMABSCHLUSSFIL']) AND $Param['RFK_DATUMABSCHLUSSFIL']!='')		
	{
		$Bedingung .= "AND to_date(to_char(RFK_DATUMABSCHLUSSFIL,'DD.MM.RRRR'),'DD.MM.RRRR') = " . $DB->FeldInhaltFormat('D',$Param['RFK_DATUMABSCHLUSSFIL']) . ' ';
	}
		
	if(isset($Param['RFK_DATUMABSCHLUSSZEN']) AND $Param['RFK_DATUMABSCHLUSSZEN']!='')		
	{
		$Bedingung .= "AND to_date(to_char(RFK_DATUMABSCHLUSSZEN,'DD.MM.RRRR'),'DD.MM.RRRR') = " . $DB->FeldInhaltFormat('D',$Param['RFK_DATUMABSCHLUSSZEN']) . ' ';
	}	
	
	if(isset($Param['RFF_BEMERKUNGZENTRALE']) AND $Param['RFF_BEMERKUNGZENTRALE']!='')
	{				
		$Bedingung .= 'AND UPPER(RFF_BEMERKUNGZENTRALE) ' . $DB->LikeOderIst($Param['RFF_BEMERKUNGZENTRALE'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';			
	}		


	return $Bedingung;
}

?>