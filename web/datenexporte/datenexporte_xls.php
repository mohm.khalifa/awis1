<?php
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
require_once('datenexporte_erstelleabfrage.php');

global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 6000);

	require_once('PHPExcel.php');
	
	if(!isset($_GET['XDE_KEY']))
	{
	        die();
	}

	$SQL = 'SELECT XDF_TEXTKONSERVE,XDF_FELDNAME,XDE_BEZEICHNUNG,XDF_TEXTKONSERVE,XDF_DATENTYP';
	$SQL .= ' FROM ExportDatenFelder';
	$SQL .= ' INNER JOIN ExportDaten ON XDE_KEY = XDF_XDE_KEY';
	$SQL .= ' WHERE XDF_XDE_KEY = 0'.$_GET['XDE_KEY'];
	$SQL .= ' ORDER BY XDF_SORTIERUNG';
	$rsXDF = $DB->RecordSetOeffnen($SQL);

	$FeldLabels = array();
	$DateiName = $rsXDF->FeldInhalt('XDE_BEZEICHNUNG');
	while(!$rsXDF->EOF())
	{
		$FeldName=$rsXDF->FeldInhalt('XDF_FELDNAME');
		
		//Überschriftentyp holen
		$XABSQL = 'SELECT XAB_UEBERSCHRIFT FROM EXPORTABFRAGEN WHERE XAB_KEY='.$DB->WertSetzen('XAB', 'N0', $_GET['XAB_KEY']);
		$rsXAB = $DB->RecordSetOeffnen($XABSQL,$DB->Bindevariablen('XAB',true));
		$UeberschriftenTyp = $rsXAB->FeldInhalt('XAB_UEBERSCHRIFT');
	
		if($UeberschriftenTyp == 0 or $UeberschriftenTyp == 3) //0 = Standard, 3 = Textkonserven
		{
			if($rsXDF->FeldInhalt('XDF_TEXTKONSERVE')!='')
			{
				$FeldKonserve = explode(':',$rsXDF->FeldInhalt('XDF_TEXTKONSERVE'));
				$Label = $Form->LadeTexte(array(array($FeldKonserve[0],$FeldKonserve[1])),$AWISBenutzer->BenutzerSprache());
				$FeldLabels[$FeldName]= $Label[$FeldKonserve[0]][$FeldKonserve[1]];
			}
			else
			{
				$Label = $Form->LadeTexte(array(array(substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3),$rsXDF->FeldInhalt('XDF_FELDNAME'))),$AWISBenutzer->BenutzerSprache());
				$FeldLabels[$rsXDF->FeldInhalt('XDF_FELDNAME')]=$Label[substr($rsXDF->FeldInhalt('XDF_FELDNAME'),0,3)][$rsXDF->FeldInhalt('XDF_FELDNAME')];
			}	
		}
		elseif($UeberschriftenTyp==2) //Feldbezeichnungen aus Datenbank als Überschrift
		{
			$FeldLabels[$rsXDF->FeldInhalt('XDF_FELDNAME')]=$rsXDF->FeldInhalt('XDF_FELDNAME');
		}
		
		
		$SpaltenKennungen[$FeldName]=$rsXDF->FeldInhalt('XDF_TEXTKONSERVE');
		$SpaltenFormat[$FeldName]=$rsXDF->FeldInhalt('XDF_DATENTYP');
		
		$rsXDF->DSWeiter();
	}
	
	$ExportFormat = $AWISBenutzer->ParameterLesen('Datenexporte:Excel Format',true);

	@ob_end_clean();
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: 01 Jan 2000");
	header('Pragma: public');
	header('Cache-Control: max-age=0');
	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xls"');
			break;
		case 2:                 // Excel 2007
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="'.($DateiName).'.xlsx"');
			break;
	}
	
	$XLSXObj = new PHPExcel();
	$XLSXObj->getProperties()->setCreator(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setLastModifiedBy(utf8_encode($AWISBenutzer->BenutzerName()));
	$XLSXObj->getProperties()->setTitle(utf8_encode(substr($DateiName,0,31)));
	$XLSXObj->getProperties()->setSubject("AWIS - Datenexport");
	$XLSXObj->getProperties()->setDescription(utf8_encode($rsXDF->FeldInhalt('XDE_BEMERKUNG')));
	
	$XLSXObj->getProperties()->setCustomProperty('AWIS-Server',$_SERVER['SERVER_NAME'],'s');
	
	$MaxSpaltenNr = '';
	$ZeilenNr = 1;
	
	$igroesse = 5000;
	file_put_contents('/var/log/awis/datenexporte.log', "Neuer Export ".date('c').PHP_EOL, FILE_APPEND);
	for($Block=1;$Block<999;$Block++)
	{
		$Abfrage = ''.ErstelleAbfrage($DB, $_GET['XDE_KEY'], $_GET['XAB_KEY']).'';
		
		$SQL = 'SELECT ABFRAGE2.* FROM (';
		$SQL .= 'SELECT ABFRAGE1.*, row_number() over(order by 1) AS zeile';
		$SQL .= ' FROM ('.$Abfrage.') ABFRAGE1';
		$SQL .= ') ABFRAGE2 WHERE zeile >= '.(($Block-1)*$igroesse).' AND zeile < '.(($Block)*$igroesse);
		
		$rsDaten = $DB->RecordSetOeffnen($SQL);

		file_put_contents('/var/log/awis/datenexporte.log', '*'.date('c').';'.$DB->LetzterSQL().PHP_EOL, FILE_APPEND);
		if($rsDaten->AnzahlDatensaetze()==0)
		{
			break;
		}

		if($Block==1)		// Beim ersten Mal zusätzliche Dinge erledigen
		{
			$XLSXObj->setActiveSheetIndex(0);
			$XLSXObj->getActiveSheet()->setTitle(utf8_encode(substr($DateiName,0,31)));
			$SpaltenNr = 'A';
		
			$Zeile='';
			$FeldListe=array();
			for($i=1;$i<=$rsDaten->AnzahlSpalten();$i++)
			{
				$FeldName = $rsDaten->FeldInfo($i,awisRecordset::FELDINFO_NAME);
				if($FeldName=='ZEILE')
				{
					continue;
				}
				
				if($UeberschriftenTyp!=1)//nur wenn nicht "keine" Überschrift gewählt wurde
				{
					$XLSXObj->getActiveSheet()->SetCellValue($SpaltenNr.$ZeilenNr, utf8_encode($FeldLabels[$FeldName]));
					
					$XLSXObj->getActiveSheet()->getStyle($SpaltenNr.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$XLSXObj->getActiveSheet()->getStyle($SpaltenNr.'1')->getFill()->getStartColor()->setARGB('FFE0E0E0');
					
				}
				
				$SpaltenNr++;
		
				$Typ = ($SpaltenFormat[$FeldName]!=''?$SpaltenFormat[$FeldName]:$rsDaten->FeldInfo($i,awisRecordset::FELDINFO_FORMAT));
				
				$FeldListe[] = array('name'=>$rsDaten->FeldInfo($i,awisRecordset::FELDINFO_NAME),'typ'=>$SpaltenFormat[$FeldName]);				
			}
			$MaxSpaltenNr=$SpaltenNr;
		}
		
		if($UeberschriftenTyp!=1) //Nur wenn gerade eine Überschrift geschrieben wurde
		{
			$ZeilenNr++;
		}
		
		// Breiten automatisch einstellen
		for($SpaltenNr='A';$SpaltenNr<$MaxSpaltenNr;$SpaltenNr++)
		{
			$XLSXObj->getActiveSheet()->getColumnDimension($SpaltenNr)->setAutoSize(true);
		}
		
		while(!$rsDaten->EOF())
		{
			$SpaltenNr='A';
			$Zeile='';
			for($i=0;$i<$rsDaten->AnzahlSpalten();$i++)
			{
				if(isset($FeldListe[$i]['typ']))
				{
					switch(substr($FeldListe[$i]['typ'],0,1))
					{
						case 'T':
							$Wert = substr(utf8_encode($Form->Format($FeldListe[$i]['typ'],$rsDaten->FeldInhalt($FeldListe[$i]['name']))),0,255);
							$XLSXObj->getActiveSheet()->getCell($SpaltenNr.$ZeilenNr)->setValueExplicit($Wert, PHPExcel_Cell_DataType::TYPE_STRING);
							break;
						case 'N':
							$Wert = $DB->FeldInhaltFormat($FeldListe[$i]['typ'],$rsDaten->FeldInhalt($FeldListe[$i]['name']));
							$XLSXObj->getActiveSheet()->getCell($SpaltenNr.$ZeilenNr)->setValueExplicit($Wert, PHPExcel_Cell_DataType::TYPE_NUMERIC);
							$Format = '#,##0';
							if(substr($FeldListe[$i]['typ'],1,1)!='' AND substr($FeldListe[$i]['typ'],1,1)!='0')
							{
								$Format .= '.'.str_pad('', substr($FeldListe[$i]['typ'],1,1),'0');
							}
							$XLSXObj->getActiveSheet()->getStyle($SpaltenNr.$ZeilenNr)->getNumberFormat()->setFormatCode($Format);
							break;
						case 'D':
							if($rsDaten->FeldInhalt($FeldListe[$i]['name']))
							{
								if(isset($FeldListe[$i]['typ'][1]) AND $FeldListe[$i]['typ'][1]=='U')
								{
									$Datum = $Form->PruefeDatum($rsDaten->FeldInhalt($FeldListe[$i]['name']),true,false,true);
								}
								else 
								{
									$Datum = $Form->PruefeDatum($rsDaten->FeldInhalt($FeldListe[$i]['name']),false,false,true);
								}
								$XLSXObj->getActiveSheet()->setCellValue($SpaltenNr.$ZeilenNr, $Form->ZeitZuExcel($Datum));
							}
							$XLSXObj->getActiveSheet()->getStyle($SpaltenNr.$ZeilenNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
							break;
						default:
							$Wert = $Form->Format($FeldListe[$i]['typ'],$rsDaten->FeldInhalt($FeldListe[$i]['name']));
							$XLSXObj->getActiveSheet()->SetCellValue($SpaltenNr.$ZeilenNr, $Wert);
					}
				}
		
				$SpaltenNr++;
			}
			$ZeilenNr++;
			$rsDaten->DSWeiter();
		}
	}
				
	// Verschiedene Formate erfordern andere Objekte
	switch ($ExportFormat)
	{
		case 1:                 // Excel 5.0
			$objWriter = new PHPExcel_Writer_Excel5($XLSXObj);
			file_put_contents('/var/log/awis/datenexporte.log', '*'.date('c').';Speichern als XLS'.PHP_EOL, FILE_APPEND);
			break;
		case 2:                 // Excel 2007
			$objWriter = new PHPExcel_Writer_Excel2007($XLSXObj);
			file_put_contents('/var/log/awis/datenexporte.log', '*'.date('c').';Speichern als XLSX'.PHP_EOL, FILE_APPEND);
			break;
	}
	$objWriter->save('php://output');
	$XLSXObj->disconnectWorksheets();
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
?>							