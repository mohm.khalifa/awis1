<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once '../versicherungen/konditionen/verskonditionen_funktionen.inc';

$VKD = new verskonditionen_funktionen();
$VKD->Param['ORDER_SUCHE'] = '';
if (isset($_GET['txtVERS_KONZERN'])) {
    if (isset($_GET['txtVERS_KONZERN'])) {
        $VKD->Param['VERS_KONZERN'] = $_GET['txtVERS_KONZERN'];
    }
    if (isset($_GET['txtVKD_SKZ'])) {
        $VKD->Param['VKD_SKZ'] = $_GET['txtVKD_SKZ'];
    }
    if (isset($_GET['txtVKD_GUELTIG_BIS'])) {
        $VKD->Param['VKD_GUELTIG_BIS'] = $_GET['txtVKD_GUELTIG_BIS'];
    }

    if (!isset($_GET['Sort'])) {
        if (isset($VKD->Param['ORDER_SUCHE']) AND $VKD->Param['ORDER_SUCHE'] != '') {
            $ORDERBY = ' ORDER BY ' . $VKD->Param['ORDER'];
        } else {
            $ORDERBY = ' ORDER BY VVE_KUERZEL, VVE_BEZEICHNUNG ASC';
            $VKD->Param['ORDER_SUCHE'] = ' VVE_KUERZEL, VVE_BEZEICHNUNG ASC ';
        }
    } else {
        $VKD->Param['ORDER_SUCHE'] = str_replace('~', ' DESC ', $_GET['Sort']);
        $ORDERBY = ' ORDER BY ' . $VKD->Param['ORDER_SUCHE'];
    }

    //********************************************************
    // SQL Vorbereiten: Bedingung
    //********************************************************
    $Bedingung = $VKD->BedingungErstellen();

    //********************************************************
    // SQL der Seite
    //********************************************************

    $SQL = 'SELECT VKR.VKR_KEY,';
    $SQL .= ' VVE.VVE_VERSNR,';
    $SQL .= ' VVE.VVE_KUERZEL,';
    $SQL .= ' VVE.VVE_BEZEICHNUNG,';
    $SQL .= ' VKD.VKD_SKZ,';
    $SQL .= ' VKD.VKD_GUELTIG_AB,';
    $SQL .= ' VKD.VKD_GUELTIG_BIS';
    $SQL .= ', row_number() over (' . $ORDERBY . ') AS ZeilenNr';
    $SQL .= ' FROM VERSKONDREF VKR';
    $SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
    $SQL .= ' ON VKR.VKR_VVE_KEY = VVE.VVE_KEY';
    $SQL .= ' INNER JOIN VERSKONDITIONEN VKD';
    $SQL .= ' ON VKD.VKD_VKR_KEY = VKR.VKR_KEY';
    if ($Bedingung != '') {
        $SQL .= ' WHERE ' . substr($Bedingung, 4);
    }
    $SQL .= ' GROUP BY VKR.VKR_KEY, VVE.VVE_VERSNR,';
    $SQL .= '   vkd.VKD_GUELTIG_AB,';
    $SQL .= '   vkd.VKD_GUELTIG_BIS,';
    $SQL .= '   vkd.VKD_SKZ,';
    $SQL .= '   vve.VVE_BEZEICHNUNG,';
    $SQL .= '   vve.VVE_KUERZEL';

    //********************************************************
    // Fertigen SQL ausf�hren
    //********************************************************
    $rsVKD = $VKD->DB->RecordsetOeffnen($SQL, $VKD->DB->Bindevariablen('VKD', true));
    $VKD->Form->DebugAusgabe(1, $VKD->DB->LetzterSQL());

    $FeldBreiten = array();
    $FeldBreiten['VERS_NR'] = 120;
    $FeldBreiten['VERS_KONZERN'] = 100;
    $FeldBreiten['VERS_BEZ'] = 200;
    $FeldBreiten['VERS_SKZ'] = 50;
    $FeldBreiten['VKD_GUELTIG_AB'] = 100;
    $FeldBreiten['VKD_GUELTIG_BIS'] = 100;

    $VKD->Form->ZeileStart();
    $Link = './verskonditionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VVE_KUERZEL' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVE_KUERZEL')) ? '~' : '');
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VVE']['VVE_KUERZEL'], $FeldBreiten['VERS_KONZERN'], '', $Link);

    $Link = './verskonditionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VKD_VVE_VERSNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VKD_VVE_VERSNR')) ? '~' : '');
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VVE']['VVE_VERSNR'], $FeldBreiten['VERS_NR'], '', $Link);

    $Link = './verskonditionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VVE_BEZEICHNUNG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VVE_BEZEICHHNUNG')) ? '~' : '');
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VVS']['VVS_VERSICHERUNG'], $FeldBreiten['VERS_BEZ'], '', $Link);

    $Link = './verskonditionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VKD_SKZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VKD_SKZ')) ? '~' : '');
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VVS']['TXT_SCHADENKZ'], $FeldBreiten['VERS_SKZ'], '', $Link);

    $Link = './verskonditionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VKD_GUELTIG_AB' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VKD_GUELTIG_AB')) ? '~' : '');
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VKD']['VKD_GUELTIG_AB'], $FeldBreiten['VKD_GUELTIG_AB'], '', $Link);

    $Link = './verskonditionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
    $Link .= '&Sort=VKD_GUELTIG_BIS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'VKD_GUELTIG_BIS')) ? '~' : '');
    $VKD->Form->Erstelle_Liste_Ueberschrift($VKD->AWISSprachKonserven['VKD']['VKD_GUELTIG_BIS'], $FeldBreiten['VKD_GUELTIG_BIS'], '', $Link);

    $VKD->Form->ZeileEnde();

    $DS = 0;
    while (!$rsVKD->EOF()) {
        $HG = ($DS % 2);
        $VKD->Form->ZeileStart();
        $Link = './verskonditionen_Main.php?cmdAktion=Details&VKR_KEY=0' . $rsVKD->FeldInhalt('VKR_KEY') . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '') . (isset($_GET['Seite']) ? '&Seite=' . ($_GET['Seite']) : '');
        $VKD->Form->Erstelle_ListenFeld('VVE_KUERZEL', $rsVKD->FeldInhalt('VVE_KUERZEL'), 0, $FeldBreiten['VERS_KONZERN'], false, $HG, '', $Link, 'T', 'L', $rsVKD->FeldInhalt('VVE_KUERZEL'));
        $VKD->Form->Erstelle_ListenFeld('VKD_VVE_VERSNR', $rsVKD->FeldInhalt('VVE_VERSNR'), 0, $FeldBreiten['VERS_NR'], false, $HG, '', '', 'T', 'L', $rsVKD->FeldInhalt('VKD_VVE_VERSNR'));
        $VKD->Form->Erstelle_ListenFeld('VVE_BEZEICHNUNG', $rsVKD->FeldInhalt('VVE_BEZEICHNUNG'), 0, $FeldBreiten['VERS_BEZ'], false, $HG, '', '', 'T', 'L', $rsVKD->FeldInhalt('VVE_BEZEICHNUNG'));
        $VKD->Form->Erstelle_ListenFeld('VKD_SKZ', $rsVKD->FeldInhalt('VKD_SKZ'), 0, $FeldBreiten['VERS_SKZ'], false, $HG, '', '', 'T', 'L', $rsVKD->FeldInhalt('VKD_SKZ'));
        $VKD->Form->Erstelle_ListenFeld('VKD_GUELTIG_AB', $rsVKD->FeldInhalt('VKD_GUELTIG_AB'), 0, $FeldBreiten['VKD_GUELTIG_AB'], false, $HG, '', '', 'D', 'L', $rsVKD->FeldInhalt('VKD_GUELTIG_AB'));
        $VKD->Form->Erstelle_ListenFeld('VKD_GUELTIG_BIS', $rsVKD->FeldInhalt('VKD_GUELTIG_BIS'), 0, $FeldBreiten['VKD_GUELTIG_BIS'], false, $HG, '', '', 'D', 'L', $rsVKD->FeldInhalt('VKD_GUELTIG_BIS'));
        $VKD->Form->ZeileEnde();

        $rsVKD->DSWeiter();
        $DS++;
    }

}

?>