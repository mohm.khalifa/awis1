<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		$Tabelle = 'XBB';
		$Key=$_POST['txtXBB_KEY'];
		$XBBKey=$_POST['txtXBB_KEY'];

		$AWIS_KEY1 = $Key;

		$Felder=array();

		$Felder[]=array($Form->LadeTextBaustein('XBB','XBB_BEZ'),$_POST['txtXBB_BEZ']);
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];

					$SQL = 'SELECT ';
					$SQL .= ' FROM ';
					$SQL .= ' WHERE ..._KEY=0'.$Key;
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					$Felder=array();

					$Felder[]=array(awis_TextKonserve($con,'','',$AWISSprache),$rsDaten->FeldInhalt(''));
					break;
			}
		}
		else
		{
			switch($_GET['Seite'])
			{
				case 'Mitglieder':
					$Tabelle = 'XBG';
					$Key=explode('~',$_GET['Del']);

					$SQL = 'SELECT XBG_XBB_KEY, XBG_XBN_KEY, XBN_NAME, XBN_VORNAME ';
					$SQL .= ' FROM BenutzerGruppen ';
					$SQL .= ' INNER JOIN Benutzer ON XBG_XBN_KEY = XBN_KEY';
					$SQL .= ' WHERE XBG_XBN_KEY=0'.$Key[1];
					$SQL .= ' AND XBG_XBB_KEY=0'.$Key[0];

					$Key = $Key[0];

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$XBBKey = $rsDaten->FeldInhalt('XBG_XBB_KEY');
					$XBNKey = $rsDaten->FeldInhalt('XBG_XBN_KEY');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_NAME'),$rsDaten->FeldInhalt('XBN_NAME'));
					$Felder[]=array($Form->LadeTextBaustein('XBN','XBN_VORNAME'),$rsDaten->FeldInhalt('XBN_VORNAME'));
					break;
				case 'Rechte':
					$Tabelle = 'XBA';
					$Key=$DB->FeldInhaltFormat('N0',$_GET['Del']);

					$SQL = 'SELECT XBA_KEY, XBA_XXX_KEY, XRC_RECHT ';
					$SQL .= ' FROM BenutzerACLs ';
					$SQL .= ' INNER JOIN Rechte ON XBA_XRC_ID = XRC_ID';
					$SQL .= ' WHERE XBA_KEY=0'.$Key;

					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$XBBKey = $rsDaten->FeldInhalt('XBA_XXX_KEY');

					$Felder=array();
					$Felder[]=array($Form->LadeTextBaustein('XRC','XRC_RECHT'),$rsDaten->FeldInhalt('XRC_RECHT'));
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'XBB':
				$SQL = 'DELETE FROM BENUTZERGRUPPENBEZ WHERE XBB_key=0'.$_POST['txtKey'];
				$AWISBenutzer->ParameterSchreiben("AktuellerXBB",'');
				$AWIS_KEY1=-1;
				break;
			case 'XBA':
				$SQL = 'DELETE FROM BenutzerACLs WHERE xba_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=$_POST['txtXBBKey'];
				break;
			case 'XBG':
				$SQL = 'DELETE FROM Benutzergruppen WHERE xbg_xbn_key=0'.$_POST['txtXBNKey'].' AND xbg_xbb_key = 0'.$_POST['txtXBBKey'];
				$AWIS_KEY1=$_POST['txtXBBKey'];
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('gruppenverwaltung_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{

		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./gruppenverwaltung_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		if(isset($XBBKey))
		{
			$Form->Erstelle_HiddenFeld('XBBKey',$XBBKey);
		}
		if(isset($XBNKey))
		{
			$Form->Erstelle_HiddenFeld('XBNKey',$XBNKey);
		}
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>