<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');

    $TextKonserven = array();
    $TextKonserven[] = array('V_VERS_FORM_SCHADENDATEN', '*');
    $TextKonserven[] = array('VVS', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht9500 = $AWISBenutzer->HatDasRecht(9500);

    if($Recht9500 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }
    $Param = $AWISBenutzer->ParameterLesen('AktuellerVVS');

    $SQL = 'SELECT v.versscheinnr,v.schadennr,v.schadendatum,vv.vvs_schadenkz,v.schadenbez,v.schadenart ';
    $SQL .= 'FROM v_vers_form_schadendaten v, versvorgangsstatus vv ';
    $SQL .= 'WHERE v.filid = vv.vvs_filid AND ';
    $SQL .= 'v.wanr = vv.vvs_wanr AND ';
    $SQL .= 'vv.vvs_key = ' . $DB->WertSetzen('VVS', 'N0', $Param);

    $rsSchadDat = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('VVS'));

    $Form->SchreibeHTMLCode('<form name=frmVersicherungenSchadendaten action=./versicherungen_Details.php?cmdAktion=Details&Seite=Schadendaten' . (isset($_GET['Seite'])?'&Seite=' . $_GET['Seite']:'') . '' . (isset($_GET['Unterseite'])?'&Unterseite=' . $_GET['Unterseite']:'') . ' method=post>');

    $EditRecht = $Recht9500 & 2;

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_SCHADENDATEN']['VERSSCHEINNR'] . ':', 180);
    $Form->Erstelle_TextFeld('VERSSCHEINNR', $rsSchadDat->FeldInhalt('VERSSCHEINNR'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_SCHADENDATEN']['SCHADENNR'] . ':', 180);
    $Form->Erstelle_TextFeld('SCHADENNR', $rsSchadDat->FeldInhalt('SCHADENNR'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_SCHADENDATEN']['SCHADENDATUM'] . ':', 180);
    $Form->Erstelle_TextFeld('SCHADENDATUM', $rsSchadDat->FeldInhalt('SCHADENDATUM'), 360, 360, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['VVS']['VVS_SCHADENKZ'] . ':', 180);
    $Form->Erstelle_TextFeld('VVS_SCHADENKZ', $rsSchadDat->FeldInhalt('VVS_SCHADENKZ'), 2, 360, $EditRecht);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_SCHADENDATEN']['SCHADENBEZ'] . ':', 180);
    $Form->Erstelle_TextFeld('SCHADENBEZ', $rsSchadDat->FeldInhalt('SCHADENBEZ'), 800, 800, false);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['V_VERS_FORM_SCHADENDATEN']['SCHADENART'] . ':', 180);
    $Form->Erstelle_TextFeld('SCHADENART', $rsSchadDat->FeldInhalt('SCHADENART'), 800, 800, false);
    $Form->ZeileEnde();
}
catch(Exception $ex)
{
    if($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200905132008");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>
