<?php

global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven = array();
$TextKonserven[] = array('FFR', 'FFR_*');
$TextKonserven[] = array('Fehler', 'err_KeinWert');
$TextKonserven[] = array('Fehler', 'err_FelderVeraendert');
$TextKonserven[] = array('Wort', 'geaendert_von');
$TextKonserven[] = array('Wort', 'geaendert_auf');
$TextKonserven[] = array('Meldung', 'DSVeraendert');
$TextKonserven[] = array('Meldung', 'EingabeWiederholen');
$TextKonserven[] = array('FKH', '*');
$TextKonserven[] = array('Wort', 'Seite');
$TextKonserven[] = array('Wort', 'lbl_suche');
$TextKonserven[] = array('Wort', 'lbl_weiter');
$TextKonserven[] = array('Wort', 'lbl_speichern');
$TextKonserven[] = array('Wort', 'lbl_trefferliste');
$TextKonserven[] = array('Wort', 'lbl_aendern');
$TextKonserven[] = array('Wort', 'lbl_hilfe');
$TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
$TextKonserven[] = array('Wort', 'lbl_loeschen');
$TextKonserven[] = array('Wort', 'lbl_zurueck');
$TextKonserven[] = array('Wort', 'lbl_DSZurueck');
$TextKonserven[] = array('Wort', 'lbl_DSWeiter');
$TextKonserven[] = array('Wort', 'lbl_drucken');
$TextKonserven[] = array('Wort', 'lbl_Hilfe');
$TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
$TextKonserven[] = array('Liste', 'lst_JaNein');
$TextKonserven[] = array('Wort', 'WirklichSpeichernFirstglas');
$TextKonserven[] = array('Wort', 'WirklichLoeschen');
$TextKonserven[] = array('Wort', 'Ja  ');
$TextKonserven[] = array('Wort', 'Nein');
$TextKonserven[] = array('Wort', 'BegruendungEingeben');


try
{
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();


    //$Form->DebugAusgabe(1,$_POST);

    $pruefeCheckboxen = 0;
    $pruefeBegruendung = 0;
    $countPflichtfelder = 0;
    $zeigeFenster = 1;

    if (isset($_POST['cmdSpeichernFirstglasOK']))
    {
        
        if ($_REQUEST['txtSonstiges'] == '')
        {

            $Form->Formular_Start();


            $SQL = 'Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_KEY1);

            $rsFFR = $DB->RecordSetOeffnen($SQL);

            if ($AWIS_KEY1 != -1)
            {
                $AWIS_KEY1 = $rsFFR->FeldInhalt('FFR_KEY');
                $Param['KEY'] = $AWIS_KEY1;
                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung', serialize($Param));
                $Form->Erstelle_HiddenFeld('FFR_KEY', $AWIS_KEY1);
            }
            else
            {
                $AWIS_KEY1 = -1;
                $AWIS_KEY2 = -1;
            }

            $Felder = array();
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $AWISBenutzer->BenutzerName());
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', date('Ymd H:i:s')));
            $Form->InfoZeile($Felder, '');

            $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
            $Form->Hinweistext($TXT_AdrLoeschen['Wort']['BegruendungEingeben']);
            $Form->SchreibeHTMLCode('<form name=frmSpeichern action=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details method=post>');
            $Form->ZeileStart();
            $Form->ZeileEnde();
            $Form->Erstelle_HiddenFeld('speichern', 1);

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Scheibenaustausch durch Autoglas', 0, 400, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Sehr geehrte Damen und Herren,', 0, 400, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Text = 'bei der Kontrolle dieser Autoglas Rechnung wurde festgestellt, dass bei folgendem Scheibenaustausch';
            $Text .= ' der von Ihnen über den Werkstattauftrag berechnete Betrag von der Autoglas Auftragsbestätigung';
            $Text .= ' erheblich abweicht.';
            $Form->Erstelle_Textarea('TEXT', $Text, 700, 70, 4, false);
            $Form->ZeileEnde();

            /*
              $Form->ZeileStart();
              $Form->Erstelle_TextFeld('','bei der Kontrolle dieser First Glass Rechnung wurde festgestellt, dass bei folgendem Scheibenaustausch',100,700,false);
              $Form->Erstelle_TextFeld('','der von Ihnen über den Werkstattauftrag berechnete Betrag von der First Glass Auftragsbestätigung',100,700,false);
              $Form->Erstelle_TextFeld('','erheblich abweicht.',100,700,false);
              $Form->ZeileEnde();
             */

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_FILID'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_FILID', $rsFFR->FeldInhalt('FFR_FILID'), 10, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_WANR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_WANR', $rsFFR->FeldInhalt('FFR_WANR'), 5, 100, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_VORGANGNR', $rsFFR->FeldInhalt('FFR_VORGANGNR'), 50, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KFZKENNZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_KFZKENNZ', $rsFFR->FeldInhalt('FFR_KFZKENNZ'), 10, 200, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_AEMBETRAG', $rsFFR->FeldInhalt('FFR_AEMBETRAG'), 7, 100, false, '', '', '', 'N2');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_KASSENBETRAG', $rsFFR->FeldInhalt('FFR_KASSENBETRAG'), 30, 100, false, '', '', '', 'N2');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZ'), 30, 100, false, '', '', '', 'N2');
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Begründungstext eingeben', 0, 400, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_Textarea('Sonstiges', '', 200, 90, 4, true);
            $AWISCursorPosition = 'txtSonstiges';
            $Form->ZeileEnde();

            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            /*
              $Form->ZeileStart();
              $Text = 'bei der Kontrolle dieser First Glass Rechnung wurde festgestellt, dass bei folgendem Scheibenaustausch';
              $Text .= ' der von Ihnen über den Werkstattauftrag berechnete Betrag von der First Glass Auftragsbestätigung';
              $Text .= ' erheblich abweicht.';
              $Form->Erstelle_Textarea('TEXT',$Text,700,70,4,false);
              $Form->ZeileEnde();
             */

            $Form->ZeileStart();
            $Text = 'Sofern Sie laut Begründung den Betrag der Bestellung (Montage-Auftrag) erfasst haben, ';
            $Text .= 'weisen wir darauf hin, dass laut Orgaanweisung "Scheibentausch" vom 07.08.2009 immer ';
            $Text .= 'der Betrag der letzten Autoglas Auftragsbestätigung zu verwenden ist. Denken Sie daran, ';
            $Text .= 'dass Sie bei Versicherungsabwicklung eine Kopie der Auftragsbestätigung mit der Rechnung ';
            $Text .= 'an die Versicherung senden';
            $Form->Erstelle_Textarea('TEXT', $Text, 700, 70, 5, false);
            $Form->ZeileEnde();

            /*
              $Form->ZeileStart();
              $Form->Erstelle_TextFeld('','Sofern Sie laut Begründung den Betrag der Bestellung (Montage-Auftrag) erfasst haben,',100,700,false);
              $Form->Erstelle_TextFeld('','weisen wir darauf hin, dass laut Orgaanweisung "Scheibentausch" vom 07.08.2009 immer',100,700,false);
              $Form->Erstelle_TextFeld('','der Betrag der letzten First Glass Auftragsbestätigung zu verwenden ist. Denken Sie daran,',100,700,false);
              $Form->Erstelle_TextFeld('','dass Sie bei Versicherungsabwicklung eine Kopie der Auftragsbestätigung mit der Rechnung',100,700,false);
              $Form->Erstelle_TextFeld('','an die Versicherung senden',100,700,false);
              $Form->ZeileEnde();
             */
            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Organisation', 100, 700, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Tel:', 20, 50, false);
            $Form->Erstelle_TextFeld('', '#111 - 5037', 100, 700, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'FAX: ', 20, 50, false);
            $Form->Erstelle_TextFeld('', '#111 - 934 5891', 100, 700, false);
            $Form->ZeileEnde();

            //$Form->Erstelle_HiddenFeld('speichern', 0);

            $Form->Trennzeile('O');

            $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

            $Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichSpeichernFirstglas']);

            $Form->SchreibeHTMLCode('<form name=frmSpeichern action=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details method=post>');
            $Form->ZeileStart();
            $Form->Schaltflaeche('submit', 'cmdSpeichernFirstglasOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '');
            $Form->Schaltflaeche('submit', 'cmdSpeichernFirstglasNEIN', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '');
            $Form->ZeileEnde();

            $Form->Erstelle_HiddenFeld('speichern', 1);
        }
        else
        {
            $SQL = "Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=" . $DB->FeldInhaltFormat('T', $_REQUEST['txtFFR_KEY']);

            $rsUpdate = $DB->RecordSetOeffnen($SQL);

            $SQL = 'UPDATE FGRUECKFUEHRUNG SET ';
            $SQL .= ' FFR_FILID=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt("FFR_FILID"), false);
            $SQL .= ', FFR_WANR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_WANR"), false);
            $SQL .= ', FFR_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_VORGANGNR"), false);
            $SQL .= ', FFR_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_KFZKENNZ"), false);
            $SQL .= ', FFR_AEMBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_AEMBETRAG"), false);
            $SQL .= ', FFR_KASSENBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_KASSENBETRAG"), false);
            $SQL .= ', FFR_DIFFERENZ=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_DIFFERENZ"), false);
            $SQL .= ', FFR_FRG_KEY=' . $DB->FeldInhaltFormat('T', 6, false);
            $SQL .= ', FFR_BEGRUENDUNG=' . $DB->FeldInhaltFormat('T', $_REQUEST['txtSonstiges'], false);
            $SQL .= ', FFR_STATUS=' . $DB->FeldInhaltFormat('N0', 1, false);
            $SQL .= ', FFR_DIFFERENZDATUM=' . $DB->FeldInhaltFormat('DU', $rsUpdate->FeldInhalt("FFR_DIFFERENZDATUM"), false);
            $SQL .= ', FFR_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
            $SQL .= ', FFR_USERDAT=sysdate';
            $SQL .= ' WHERE FFR_KEY=' . $DB->FeldInhaltFormat('T', $_REQUEST['txtFFR_KEY']);

            $DB->Ausfuehren($SQL);

            $Form->Erstelle_HiddenFeld('speichern', 0);
            $zeigeFenster = 0;

            if ($AWISCursorPosition !== '')
            {
                $Form->SchreibeHTMLCode('<Script Language=JavaScript>');
                $Form->SchreibeHTMLCode("document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();");
                $Form->SchreibeHTMLCode('</Script>');
            }
        }
    }

    if (isset($_POST['cmdSpeichernFirstglasGruendeOK']))
    {

        $SQL = "Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=" . $DB->FeldInhaltFormat('T', $_REQUEST['txtFFR_KEY']);
        $SQLBEMERKUNG = "Select * from FGRUECKFUEHRUNGSGRUENDE WHERE FRG_KEY=" . $_REQUEST['txtFRG_KEY'];

        $rsBemerkung = $DB->RecordSetOeffnen($SQLBEMERKUNG);

        $rsUpdate = $DB->RecordSetOeffnen($SQL);

        $SQL = 'UPDATE FGRUECKFUEHRUNG SET ';
        $SQL .= ' FFR_FILID=' . $DB->FeldInhaltFormat('NO', $rsUpdate->FeldInhalt("FFR_FILID"), false);
        $SQL .= ', FFR_WANR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_WANR"), false);
        $SQL .= ', FFR_VORGANGNR=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_VORGANGNR"), false);
        $SQL .= ', FFR_KFZKENNZ=' . $DB->FeldInhaltFormat('T', $rsUpdate->FeldInhalt("FFR_KFZKENNZ"), false);
        $SQL .= ', FFR_AEMBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_AEMBETRAG"), false);
        $SQL .= ', FFR_KASSENBETRAG=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_KASSENBETRAG"), false);
        $SQL .= ', FFR_DIFFERENZ=' . $DB->FeldInhaltFormat('N2', $rsUpdate->FeldInhalt("FFR_DIFFERENZ"), false);
        $SQL .= ', FFR_FRG_KEY=' . $DB->FeldInhaltFormat('T', $_REQUEST['txtFRG_KEY'], false);
        $SQL .= ', FFR_BEGRUENDUNG=' . $DB->FeldInhaltFormat('T', $rsBemerkung->FeldInhalt('FRG_BEZEICHNUNG'), false);
        $SQL .= ', FFR_STATUS=' . $DB->FeldInhaltFormat('N0', 1, false);
        $SQL .= ', FFR_DIFFERENZDATUM=' . $DB->FeldInhaltFormat('DU', $rsUpdate->FeldInhalt("FFR_DIFFERENZDATUM"), false);
        $SQL .= ', FFR_USER=\'' . $AWISBenutzer->BenutzerName() . '\'';
        $SQL .= ', FFR_USERDAT=sysdate';
        $SQL .= ' WHERE FFR_KEY=' . $DB->FeldInhaltFormat('T', $_REQUEST['txtFFR_KEY']);

        $DB->Ausfuehren($SQL);

        $Form->Erstelle_HiddenFeld('speichern', 0);
    }

    /*
      if(isset($_REQUEST["txtFRG_KEY"]))
      {
      if($_REQUEST["txtFRG_KEY"] == 6 && !isset($_REQUEST['txtSonstiges']))
      {

      $Form->Formular_Start();
      $Form->SchreibeHTMLCode('<form name=frmFirstGlasRueckfuehrung action=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details&FFRListe=1 method=POST  enctype="multipart/form-data">');

      $SQL = 'Select * from FGRUECKFUEHRUNG WHERE FFR_KEY='.$DB->FeldInhaltFormat('NO', $AWIS_KEY1);

      $rsFFR = $DB->RecordSetOeffnen($SQL);

      if ($AWIS_KEY1 !=-1)
      {
      $AWIS_KEY1 = $rsFFR->FeldInhalt('FFR_KEY');
      $Param['KEY']=$AWIS_KEY1;
      $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung',serialize($Param));
      $Form->Erstelle_HiddenFeld('FFR_KEY',$AWIS_KEY1);
      }
      else
      {
      $AWIS_KEY1 = -1;
      $AWIS_KEY2 = -1;
      }

      $Felder = array();
      $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a class=BilderLink href=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details&FFRListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
      $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$AWISBenutzer->BenutzerName());
      $Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$Form->Format('DU',date('Ymd H:i:s')));
      $Form->InfoZeile($Felder,'');

      $Form->ZeileStart();
      $Form->Erstelle_TextFeld('','Scheibenaustausch durch Firstglas',0,400,false);
      $Form->ZeileEnde();

      $Form->Trennzeile('O');

      $Form->ZeileStart();
      $Form->Erstelle_TextFeld('','Sehr geehrte Damen und Herren,',0,400,false);
      $Form->ZeileEnde();

      $Form->Trennzeile('O');

      $Form->ZeileStart();
      $Form->Erstelle_TextFeld('','bei der Kontrolle dieser Firstglas-Rechnung wurde festgestellt,dass bei folgendem Scheibenaustausch',100,800,false);
      $Form->Erstelle_TextFeld('','der von Ihnen über den Werkstattauftrag berechnete Betrag von der Firstglas-Auftragsbestätigung',100,800,false);
      $Form->Erstelle_TextFeld('','erheblich abweicht.',100,800,false);
      $Form->ZeileEnde();

      $Form->Trennzeile('O');

      $Form->ZeileStart();
      $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_FILID'].':',150);
      $Form->Erstelle_TextFeld('FFR_FILID',$rsFFR->FeldInhalt('FFR_FILID'),10,200,false);
      $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_WANR'].':',150);
      $Form->Erstelle_TextFeld('FFR_WANR',$rsFFR->FeldInhalt('FFR_WANR'),5,100,false);
      $Form->ZeileEnde();

      $Form->ZeileStart();
      $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'].':',150);
      $Form->Erstelle_TextFeld('FFR_VORGANGNR',$rsFFR->FeldInhalt('FFR_VORGANGNR'),50,200,false);
      $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KFZKENNZ'].':',150);
      $Form->Erstelle_TextFeld('FFR_KFZKENNZ',$rsFFR->FeldInhalt('FFR_KFZKENNZ'),10,200,false);
      $Form->ZeileEnde();

      $Form->Trennzeile('O');

      $Form->ZeileStart();
      $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'].':',150);
      $Form->Erstelle_TextFeld('FFR_AEMBETRAG',$rsFFR->FeldInhalt('FFR_AEMBETRAG'),7,100,false,'','','','N2');
      $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'].':',150);
      $Form->Erstelle_TextFeld('FFR_KASSENBETRAG',$rsFFR->FeldInhalt('FFR_KASSENBETRAG'),30,100,false,'','','','N2');
      $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'].':',150);
      $Form->Erstelle_TextFeld('FFR_DIFFERENZ',$rsFFR->FeldInhalt('FFR_DIFFERENZ'),30,100,false,'','','','N2');
      $Form->ZeileEnde();

      $Form->Trennzeile('O');
      $Form->Trennzeile('O');

      $Form->ZeileStart();
      $Form->Hinweistext('Bitte Begründungstext eingeben');
      $Form->ZeileEnde();
      $Form->ZeileStart();
      $Form->Erstelle_Textarea('Sonstiges', '', 200, 90, 4,true);
      $Form->ZeileEnde();

      $Form->ZeileEnde();

      $Form->Trennzeile('O');
      $Form->Trennzeile('O');

      $Form->ZeileStart();
      $Form->Erstelle_TextFeld('','Sofern Sie, laut Begründung, in den Filialsystemen den Betrag der Bestellung (Montage-Auftrag)',100,800,false);
      $Form->Erstelle_TextFeld('','erfasst haben, erlauben Sie uns bitte den Hinweis, dass gemäß Rundschreiben Nr. 28 vom',100,800,false);
      $Form->Erstelle_TextFeld('','05.07.2004 der Betrag der 2. Firstglas - Auftragsbestätigung zu verwenden ist. Bedenken Sie,',100,800,false);
      $Form->Erstelle_TextFeld('','dass Sie bei Versicherungsabwicklung eine Kopie der Firstglas - Auftragsbestätigung mit an die',100,800,false);
      $Form->Erstelle_TextFeld('','Versicherung senden. Differenzen zwischen unserer Rechnungssumme und den Firstglas-Angaben',100,800,false);
      $Form->Erstelle_TextFeld('','sind daher schwer zu erklären und sollten eigentlich vermieden werden.',100,800,false);
      $Form->ZeileEnde();

      $Form->Trennzeile('O');

      $Form->ZeileStart();
      $Form->Erstelle_TextFeld('','Revision & Organisation',100,800,false);
      $Form->Erstelle_TextFeld('','Tel:#111 - 5037',100,800,false);
      $Form->Erstelle_TextFeld('','FAX:#111 - 934 5891',100,800,false);
      $Form->ZeileEnde();

      $Form->Erstelle_HiddenFeld('speichern', 1);

      $Form->SchaltflaechenStart();
      // Zurück zum Menü
      $Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
      if(($Recht9100&4)==4)
      {
      $Form->Erstelle_HiddenFeld('FFR_KEY', $rsFFR->FeldInhalt('FFR_KEY'));
      $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
      }
      $Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=preisauskunft&Aktion=protokoll','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

      $Form->SchaltflaechenEnde();

      $Form->SchreibeHTMLCode('</form>');

      $Form->Formular_Ende();
      }

      }
     */
    if (isset($_REQUEST['cmd_weiter_x']) != '')
    {

        if ($_REQUEST['txtFRG_KEY'] == 6)
        {

            $Form->Formular_Start();

            $SQL = 'Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_KEY1);

            $rsFFR = $DB->RecordSetOeffnen($SQL);

            if ($AWIS_KEY1 != -1)
            {
                $AWIS_KEY1 = $rsFFR->FeldInhalt('FFR_KEY');
                $Param['KEY'] = $AWIS_KEY1;
                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung', serialize($Param));
                $Form->Erstelle_HiddenFeld('FFR_KEY', $AWIS_KEY1);
            }
            else
            {
                $AWIS_KEY1 = -1;
                $AWIS_KEY2 = -1;
            }

            $Felder = array();
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $AWISBenutzer->BenutzerName());
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', date('Ymd H:i:s')));
            $Form->InfoZeile($Felder, '');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Scheibenaustausch durch Autoglas', 0, 400, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Sehr geehrte Damen und Herren,', 0, 400, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Text = 'bei der Kontrolle dieser Autoglas Rechnung wurde festgestellt, dass bei folgendem Scheibenaustausch';
            $Text .= ' der von Ihnen über den Werkstattauftrag berechnete Betrag von der Autoglas Auftragsbestätigung';
            $Text .= ' erheblich abweicht.';
            $Form->Erstelle_Textarea('TEXT', $Text, 700, 70, 4, false);
            $Form->ZeileEnde();

            /*
              $Form->ZeileStart();
              $Form->Erstelle_TextFeld('','bei der Kontrolle dieser First Glass Rechnung wurde festgestellt, dass bei folgendem Scheibenaustausch',100,700,false);
              $Form->Erstelle_TextFeld('','der von Ihnen über den Werkstattauftrag berechnete Betrag von der First Glass Auftragsbestätigung',100,700,false);
              $Form->Erstelle_TextFeld('','erheblich abweicht.',100,700,false);
              $Form->ZeileEnde();
             */

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_FILID'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_FILID', $rsFFR->FeldInhalt('FFR_FILID'), 10, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_WANR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_WANR', $rsFFR->FeldInhalt('FFR_WANR'), 5, 100, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_VORGANGNR', $rsFFR->FeldInhalt('FFR_VORGANGNR'), 50, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KFZKENNZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_KFZKENNZ', $rsFFR->FeldInhalt('FFR_KFZKENNZ'), 10, 200, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_AEMBETRAG', $rsFFR->FeldInhalt('FFR_AEMBETRAG'), 7, 100, false, '', '', '', 'N2');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_KASSENBETRAG', $rsFFR->FeldInhalt('FFR_KASSENBETRAG'), 30, 100, false, '', '', '', 'N2');
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZ'), 30, 100, false, '', '', '', 'N2');
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Begründungstext eingeben', 0, 400, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_Textarea('Sonstiges', '', 200, 90, 4, true);
            $AWISCursorPosition = 'txtSonstiges';
            $Form->ZeileEnde();

            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            /*
              $Form->ZeileStart();
              $Form->Erstelle_TextFeld('','Sofern Sie, laut Begründung, in den Filialsystemen den Betrag der Bestellung (Montage-Auftrag)',100,800,false);
              $Form->Erstelle_TextFeld('','erfasst haben, erlauben Sie uns bitte den Hinweis, dass gemäß Rundschreiben Nr. 28 vom',100,800,false);
              $Form->Erstelle_TextFeld('','05.07.2004 der Betrag der 2. First Glass Auftragsbestätigung zu verwenden ist. Bedenken Sie,',100,800,false);
              $Form->Erstelle_TextFeld('','dass Sie bei Versicherungsabwicklung eine Kopie der First Glass Auftragsbestätigung mit an die',100,800,false);
              $Form->Erstelle_TextFeld('','Versicherung senden. Differenzen zwischen unserer Rechnungssumme und den First Glass Angaben',100,800,false);
              $Form->Erstelle_TextFeld('','sind daher schwer zu erklären und sollten eigentlich vermieden werden.',100,800,false);
              $Form->ZeileEnde();
             */

            $Form->ZeileStart();
            $Text = 'Sofern Sie laut Begründung den Betrag der Bestellung (Montage-Auftrag) erfasst haben, ';
            $Text .= 'weisen wir darauf hin, dass laut Orgaanweisung "Scheibentausch" vom 07.08.2009 immer ';
            $Text .= 'der Betrag der letzten Autoglas Auftragsbestätigung zu verwenden ist. Denken Sie daran, ';
            $Text .= 'dass Sie bei Versicherungsabwicklung eine Kopie der Auftragsbestätigung mit der Rechnung ';
            $Text .= 'an die Versicherung senden';
            $Form->Erstelle_Textarea('TEXT', $Text, 700, 70, 5, false);
            $Form->ZeileEnde();


            /*
              $Form->ZeileStart();
              $Form->Erstelle_TextFeld('','Sofern Sie laut Begründung den Betrag der Bestellung (Montage-Auftrag) erfasst haben,',100,700,false);
              $Form->Erstelle_TextFeld('','weisen wir darauf hin, dass laut Orgaanweisung "Scheibentausch" vom 07.08.2009 immer',100,700,false);
              $Form->Erstelle_TextFeld('','der Betrag der letzten First Glass Auftragsbestätigung zu verwenden ist. Denken Sie daran,',100,700,false);
              $Form->Erstelle_TextFeld('','dass Sie bei Versicherungsabwicklung eine Kopie der Auftragsbestätigung mit der Rechnung',100,700,false);
              $Form->Erstelle_TextFeld('','an die Versicherung senden',100,700,false);
              $Form->ZeileEnde();
             */



            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Organisation', 100, 700, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Tel:', 20, 50, false);
            $Form->Erstelle_TextFeld('', '#111 - 5037', 100, 600, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'FAX: ', 20, 50, false);
            $Form->Erstelle_TextFeld('', '#111 - 934 5891', 100, 600, false);
            $Form->ZeileEnde();


            //$Form->Erstelle_HiddenFeld('speichern', 0);

            $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

            $Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichSpeichernFirstglas']);

            $Form->SchreibeHTMLCode('<form name=frmSpeichern action=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details method=post>');
            $Form->ZeileStart();

            $Form->Schaltflaeche('submit', 'cmdSpeichernFirstglasOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '');
            $Form->Schaltflaeche('submit', 'cmdSpeichernFirstglasNEIN', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '');
            $Form->ZeileEnde();

            /*
              $Form->SchaltflaechenStart();
              // Zurück zum Menü
              $Form->SchaltflaechenEnde();

              $Form->SchreibeHTMLCode('</form>');

              $Form->Formular_Ende();
             */
        }
    }

    if (isset($_REQUEST["cmd_weiter_x"]))
    {

        if ($_REQUEST["txtFRG_KEY"] >= 1 && $_REQUEST["txtFRG_KEY"] < 6 || $_REQUEST["txtFRG_KEY"] == 7)
        {
            $Form->Formular_Start();

            $SQL = 'Select * from FGRUECKFUEHRUNG WHERE FFR_KEY=' . $DB->FeldInhaltFormat('NO', $AWIS_KEY1);

            $rsFFR = $DB->RecordSetOeffnen($SQL);

            if ($AWIS_KEY1 != -1)
            {
                $AWIS_KEY1 = $rsFFR->FeldInhalt('FFR_KEY');
                $Param['KEY'] = $AWIS_KEY1;
                $AWISBenutzer->ParameterSchreiben('FirstglasRueckfuehrung', serialize($Param));
                $Form->Erstelle_HiddenFeld('FFR_KEY', $AWIS_KEY1);
            }
            else
            {
                $AWIS_KEY1 = -1;
                $AWIS_KEY2 = -1;
            }

            $Felder = array();
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $AWISBenutzer->BenutzerName());
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', date('Ymd H:i:s')));
            $Form->InfoZeile($Felder, '');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Scheibenaustausch durch Autoglas', 0, 400, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Sehr geehrte Damen und Herren,', 0, 400, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Text = 'bei der Kontrolle dieser Autoglas Rechnung wurde festgestellt, dass bei folgendem Scheibenaustausch';
            $Text .= ' der von Ihnen über den Werkstattauftrag berechnete Betrag von der Autoglas Auftragsbestätigung';
            $Text .= ' erheblich abweicht.';
            $Form->Erstelle_Textarea('TEXT', $Text, 700, 70, 4, false);
            $Form->ZeileEnde();


            /*
              $Form->ZeileStart();
              $Form->Erstelle_TextFeld('','bei der Kontrolle dieser First Glass Rechnung wurde festgestellt,dass bei folgendem Scheibenaustausch',100,700,false);
              $Form->Erstelle_TextFeld('','der von Ihnen über den Werkstattauftrag berechnete Betrag von der First Glass Auftragsbestätigung',100,700,false);
              $Form->Erstelle_TextFeld('','erheblich abweicht.',100,700,false);
              $Form->ZeileEnde();
             */

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_FILID'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_FILID', $rsFFR->FeldInhalt('FFR_FILID'), 10, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_WANR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_WANR', $rsFFR->FeldInhalt('FFR_WANR'), 5, 100, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_VORGANGNR'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_VORGANGNR', $rsFFR->FeldInhalt('FFR_VORGANGNR'), 50, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KFZKENNZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_KFZKENNZ', $rsFFR->FeldInhalt('FFR_KFZKENNZ'), 10, 200, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_AEMBETRAG'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_AEMBETRAG', $rsFFR->FeldInhalt('FFR_AEMBETRAG'), 7, 100, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_KASSENBETRAG'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_KASSENBETRAG', $rsFFR->FeldInhalt('FFR_KASSENBETRAG'), 30, 100, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FFR']['FFR_DIFFERENZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FFR_DIFFERENZ', $rsFFR->FeldInhalt('FFR_DIFFERENZ'), 30, 100, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $SQL = "Select * from FGRUECKFUEHRUNGSGRUENDE WHERE FRG_KEY=" . $_REQUEST["txtFRG_KEY"];

            $rsBemerkung = $DB->RecordSetOeffnen($SQL);

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'ausgewählter Grund:', 400, 400);
            $Form->Trennzeile('O');
            $Form->ZeileEnde();
            $Form->Erstelle_TextFeld('', $rsBemerkung->FeldInhalt('FRG_BEZEICHNUNG'), 400, 400);
            $Form->ZeileEnde();

            $Form->Erstelle_HiddenFeld('FRG_KEY', $_REQUEST["txtFRG_KEY"]);


            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Text = 'Sofern Sie laut Begründung den Betrag der Bestellung (Montage-Auftrag) erfasst haben, ';
            $Text .= 'weisen wir darauf hin, dass laut Orgaanweisung "Scheibentausch" vom 07.08.2009 immer ';
            $Text .= 'der Betrag der letzten Autoglas Auftragsbestätigung zu verwenden ist. Denken Sie daran, ';
            $Text .= 'dass Sie bei Versicherungsabwicklung eine Kopie der Auftragsbestätigung mit der Rechnung ';
            $Text .= 'an die Versicherung senden';
            $Form->Erstelle_Textarea('TEXT', $Text, 700, 70, 5, false);
            $Form->ZeileEnde();



            /*
              $Form->ZeileStart();
              $Form->Erstelle_TextFeld('','Sofern Sie laut Begründung den Betrag der Bestellung (Montage-Auftrag) erfasst haben,',100,700,false);
              $Form->Erstelle_TextFeld('','weisen wir darauf hin, dass laut Orgaanweisung "Scheibentausch" vom 07.08.2009 immer',100,700,false);
              $Form->Erstelle_TextFeld('','der Betrag der letzten First Glass Auftragsbestätigung zu verwenden ist. Denken Sie daran,',100,700,false);
              $Form->Erstelle_TextFeld('','dass Sie bei Versicherungsabwicklung eine Kopie der Auftragsbestätigung mit der Rechnung',100,700,false);
              $Form->Erstelle_TextFeld('','an die Versicherung senden',100,700,false);
              $Form->ZeileEnde();
             */

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Organisation', 100, 700, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'Tel:', 20, 50, false);
            $Form->Erstelle_TextFeld('', '#111 - 5037', 100, 600, false);
            $Form->ZeileEnde();
            $Form->ZeileStart();
            $Form->Erstelle_TextFeld('', 'FAX: ', 20, 50, false);
            $Form->Erstelle_TextFeld('', '#111 - 934 5891', 100, 600, false);
            $Form->ZeileEnde();

            $Form->Erstelle_HiddenFeld('speichern', 0);

            $Form->ZeileStart();

            $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

            $Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichSpeichernFirstglas']);


            $Form->SchreibeHTMLCode('<form name=frmSpeichern action=./firstglasfilrueckfuehrung_Main.php?cmdAktion=Details method=post>');
            $Form->ZeileStart();
            //$Form->Schaltflaeche($Typ, $Name, $Link, $Bilddatei, $Titel, $TastenKuerzel, $Style, $Breite)
            $Form->Schaltflaeche('submit', 'cmdSpeichernFirstglasGruendeOK', '', '', $TXT_AdrLoeschen['Wort']['Ja'], '', '', 100);
            $Form->Schaltflaeche('submit', 'cmdSpeichernFirstglasGruendeNEIN', '', '', $TXT_AdrLoeschen['Wort']['Nein'], '');
            $Form->ZeileEnde();

            $Form->ZeileEnde();

            $Form->SchaltflaechenStart();
            // Zurück zum Menü
            $Form->SchaltflaechenEnde();

            $Form->SchreibeHTMLCode('</form>');

            $Form->Formular_Ende();
        }
    }

    if ($AWISCursorPosition !== '')
    {
        $Form->SchreibeHTMLCode('<Script Language=JavaScript>');
        $Form->SchreibeHTMLCode("document.getElementsByName(\"" . $AWISCursorPosition . "\")[0].focus();");
        $Form->SchreibeHTMLCode('</Script>');
    }
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SYSTcherFehler', $ex->getMessage(), 'HotlineBenachrichtigen', -2);
}
?>
