<?php
global $con;
global $awisRSZeilen;
global $AWISSprache;
global $AWISBenutzer;
global $_POST;
global $_GET;

$TXT_Baustein = array();
$TXT_Baustein[]=array('LIK','LIK_%');
$TXT_Baustein[]=array('Wort','Gesamtbestand');
$TXT_Baustein[]=array('Wort','Urspruengliche');
$TXT_Baustein[]=array('Wort','Datum');
$TXT_Baustein[]=array('Wort','Menge');
$TXT_Baustein[]=array('Wort','Soll');
$TXT_Baustein[]=array('Wort','Ist');
$TXT_Baustein[]=array('Wort','Seite');
$TXT_Baustein[]=array('LIK','Lieferscheinnummer');
$TXT_Baustein[]=array('LIK','Kommissionierung');
$TXT_Baustein[]=array('LIK','Scan');

$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);

$RechteStufe = awisBenutzerRecht($con, 507);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Lieferkontrolle',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

if(isset($_POST['cmdSuche_x'])) 		// Neue suche
{
	$Param = '';
	$Param .= ';' .$_POST['sucSuchFiliale'];
	$Param .= ';'. $_POST['sucSuchDatumVon'];
	$Param .= ';'. $_POST['sucSuchDatumBis'];
	$Param .= ';'. $_POST['sucSuchArtnr'];
	$Param .= ';'. $_POST['sucSuchLSNR'];
	$Param .= ';'. $_POST['sucSuchLager'];
	if (isset($_POST['txtNurDifferenzen']))
	{
		$Param .= ';'. $_POST['txtNurDifferenzen'];
	}	
	
	awis_BenutzerParameterSpeichern($con,'AktuelleLieferkontrolleSuche' , $AWISBenutzer->BenutzerName(), $Param);


	$Params=null;

    if (!isset($_POST['txtAuswahlSpeichern'])) {
        $Params[0] = '';
    } else {
        $Params[0] = $_POST['txtAuswahlSpeichern'];
    }

	$Params[1] = $_POST['sucSuchFiliale'];
	$Params[2] = $_POST['sucSuchDatumVon'];
	$Params[3] = $_POST['sucSuchDatumBis'];
	$Params[4] = $_POST['sucSuchArtnr'];
	$Params[5] = $_POST['sucSuchLSNR'];

	if (!isset($_POST['txtNurDifferenzen'])) {
		$Params[6] = '';
	} else {
		$Params[6] = $_POST['txtNurDifferenzen'];
	}
	$Params[7] = $_POST['sucSuchLager'];
	$Params[8] = $_POST['txtFormat'];

	awis_BenutzerParameterSpeichern($con,'Formular_AUS_LIK' ,  $AWISBenutzer->BenutzerName(), serialize($Params));
}

else	// Letzte gespeicherte Suche
{
	$Param = '';
	$Param = awis_BenutzerParameter($con,'AktuelleLieferkontrolleSuche' , $AWISBenutzer->BenutzerName());
}

$Bedingung='';
$Param = explode(';',$Param);

if($Param[0]!='')
{
	$Bedingung .= 'AND LIK_Key = ' . intval($Param[0]) . ' ';
}

if(isset($Param[1]) AND $Param[1]!='')		// Filiale
{
	$Bedingung .= 'AND LIK_FIL_ID = ' . intval($Param[1]) . ' ';
}

if(isset($Param[2]) AND $Param[2]!='')		// DatumVon
{
	$Bedingung .= 'AND TRUNC(LIK_DATUMKOMM) >= ' . awisFeldFormat('D',$Param[2]) . ' ';
}

if(isset($Param[3]) AND $Param[3]!='')		// DatumBis
{
	$Bedingung .= 'AND TRUNC(LIK_DATUMKOMM) <= ' . awisFeldFormat('D',$Param[3]) . ' ';
}

if(isset($Param[4]) AND $Param[4]!='')		// Artnr
{
	$Bedingung .= 'AND LIK_AST_ATUNR ' . awisLIKEoderIST($Param[4],1) . ' ';
}

if(isset($Param[5]) AND $Param[5]!='')		// Lieferschein-Nr
{
	$Bedingung .= 'AND LIK_Lieferscheinnr ' . awisLikeoderIst($Param[5],0) . ' ';
}

if(isset($Param[6]) AND $Param[6]!='')		// Filiale
{
	$Bedingung .= 'AND FIL_LAGERKZ = \'' . $Param[6] . '\'';
}

if(isset($Param[7]) AND $Param[7]!='')
{
	$Bedingung .= ' AND ((LIK_MENGESOLL <> LIK_MENGEIST)';
	$Bedingung .= ' OR (LIK_MENGEIST <> LIK_MENGESCAN AND LIK_MENGESCAN IS NOT NULL))';
}

$SQL = 'SELECT * FROM (';
$SQL.= ' SELECT LIK_KEY, LIK_FIL_ID, LIK_QUELLE, LIK_AST_ATUNR, LIK_LIEFERSCHEINNR, LIK_DATUMKOMM, LIK_DATUMSCAN';
$SQL.= ', LIK_SOLLBESTAND, LIK_ISTBESTAND, LIK_MENGESCAN, LIK_STATUS, LIK_MENGESOLL, LIK_MENGEIST, FIL_LAGERKZ';
$SQL.= ', LIK_MENGEIST - LIK_MENGESOLL AS DIFFERENZ';
$SQL.= ',(SELECT MIN(LIK_STATUS) AS MIN_STATUS FROM Lieferkontrollen WHERE Lieferkontrollen.LIK_MENGESOLL > 0 and Lieferkontrollen.LIK_LieferscheinNr=LIK.LIK_LieferscheinNr AND Lieferkontrollen.LIK_FIL_ID = LIK.LIK_FIL_ID) AS LIK_STATUS_MIN';
$SQL.= ' FROM LIEFERKONTROLLEN LIK';
$SQL.= ' LEFT JOIN FILIALEN ON FIL_ID = LIK_FIL_ID)';
$SQL.= ' WHERE LIK_STATUS_MIN >= 10';

if ($Bedingung!='')
{
	$SQL .= ' AND '. substr($Bedingung,3);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY LIK_FIL_ID, LIK_AST_ATUNR, LIK_DATUMKOMM';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}

// Zeilen begrenzen
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$rsLIK = awisOpenRecordset($con, $SQL);
$rsLIKZeilen = $awisRSZeilen;
awis_Debug(1,$SQL,$Param,$_POST);

//********************************************************
// Daten anzeigen
//********************************************************
if($rsLIKZeilen==0)		
{
	echo '<span class=HinweisText>Es wurden keine Daten gefunden.</span>';
}
elseif($rsLIKZeilen>=1)
{
	if (!isset($_POST['txtFormat']) or ($_POST['txtFormat']==1 OR $_POST['txtFormat']==''))		// Liste auf dem Bildschirm
	{
		awis_FORM_FormularStart();
		
		awis_FORM_ZeileStart();		
		awis_FORM_Erstelle_Liste_Ueberschrift('',80);	
		awis_FORM_Erstelle_Liste_Ueberschrift('',100);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['Kommissionierung'],170);			
		awis_FORM_Erstelle_Liste_Ueberschrift('',10);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Menge'],60);
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Menge'],60);			
		awis_FORM_Erstelle_Liste_Ueberschrift('',100);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Gesamtbestand'],150);
		awis_FORM_Erstelle_Liste_Ueberschrift('',10);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Urspruengliche'],120);
		awis_FORM_Erstelle_Liste_Ueberschrift('',120);										
		awis_FORM_ZeileEnde();
		
		
		awis_FORM_ZeileStart();
		$Link = './lieferkontrolle_Main.php?cmdAktion=Liste'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LIK_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIK_FIL_ID'))?'~':'');		
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_FIL_ID'],80,'',$Link);	
		$Link = './lieferkontrolle_Main.php?cmdAktion=Liste'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LIK_AST_ATUNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIK_AST_ATUNR'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_AST_ATUNR'],100,'',$Link);	
		$Link = './lieferkontrolle_Main.php?cmdAktion=Liste'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
		$Link .= '&Sort=LIK_DATUMKOMM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='LIK_DATUMKOMM'))?'~':'');
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Datum'],120,'',$Link);			
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['Lieferscheinnummer'],60);					
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['Wort']['Soll'],60);
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['Scan'],60);			
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_DIFFERENZ'],100);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_SOLLBESTAND'],80);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_ISTBESTAND'],80);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_MENGEIST'],120);	
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['LIK']['LIK_DATUMSCAN'],120);
		awis_FORM_ZeileEnde();		
		
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}
		
		//Seitenweises bl�ttern
		if($rsLIKZeilen>$MaxDSAnzahl)
		{
			awis_FORM_ZeileStart();
			awis_FORM_Erstelle_TextLabel($TXT_Baustein['Wort']['Seite'],50);
			
			for ($i=0;$i<($rsLIKZeilen/$MaxDSAnzahl);$i++)
			{
				if($i!=($StartZeile/$MaxDSAnzahl))
				{
					$Text = '&nbsp;<a href=./lieferkontrolle_Main.php?cmdAktion=Liste&Block='.$i.'>'.($i+1).'</a>';
					awis_FORM_Erstelle_TextLabel($Text,30);					
				}
				else 
				{
					$Text = '&nbsp;'.($i+1).'';
					awis_FORM_Erstelle_TextLabel($Text,30);
				}
			}
			awis_FORM_ZeileEnde();
		}
		
		for($LIKZeile=$StartZeile;$LIKZeile<$rsLIKZeilen and $LIKZeile<$StartZeile+$MaxDSAnzahl;$LIKZeile++)
		{	
			awis_FORM_ZeileStart();				
			$Link = '/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsLIK['LIK_FIL_ID'][$LIKZeile];
			awis_FORM_Erstelle_ListenFeld('LIK_FIL_ID',$rsLIK['LIK_FIL_ID'][$LIKZeile],10,80,false,($LIKZeile%2),'',$Link,'T','L');
			$Link = '/ATUArtikel/artikel_Main.php?ATUNR='.$rsLIK['LIK_AST_ATUNR'][$LIKZeile].'&cmdAktion=ArtikelInfos';
			awis_FORM_Erstelle_ListenFeld('LIK_AST_ATUNR',$rsLIK['LIK_AST_ATUNR'][$LIKZeile],10,100,false,($LIKZeile%2),'',$Link,'T','L');
			awis_FORM_Erstelle_ListenFeld('LIK_DATUMKOMM',$rsLIK['LIK_DATUMKOMM'][$LIKZeile],10,120,false,($LIKZeile%2),'','','D','L');
			awis_FORM_Erstelle_ListenFeld('LIK_LIEFERSCHEINNR',$rsLIK['LIK_LIEFERSCHEINNR'][$LIKZeile],10,60,false,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_MENGESOLL',$rsLIK['LIK_MENGESOLL'][$LIKZeile],10,60,false,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_MENGEIST',$rsLIK['LIK_MENGEIST'][$LIKZeile],10,60,false,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_DIFFERENZ',$rsLIK['DIFFERENZ'][$LIKZeile],10,100,false,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_SOLLBESTAND',$rsLIK['LIK_SOLLBESTAND'][$LIKZeile],10,80,false,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_ISTBESTAND',$rsLIK['LIK_ISTBESTAND'][$LIKZeile],40,80,false,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_MENGESCAN',$rsLIK['LIK_MENGESCAN'][$LIKZeile],40,120,false,($LIKZeile%2),'','','Z','L');
			awis_FORM_Erstelle_ListenFeld('LIK_DATUMSCAN',$rsLIK['LIK_DATUMSCAN'][$LIKZeile],10,120,false,($LIKZeile%2),'','','D','L');
			awis_FORM_ZeileEnde();
		}
		
		awis_FORM_FormularEnde();
	}

	elseif($_POST['txtFormat']==2)		// CSV Datei
	{
			// Dateinamen generieren
		$DateiName = awis_UserExportDateiName('.csv');
		$fd = fopen($DateiName,'w' );
	
		fputs($fd, "FILIALE;ATUNR;KOMMISSIONIERUNG;LIEFERSCHEINNR;SOLLMENGE;ISTMENGE;DIFFERENZ;SOLLBESTAND;ISTBESTAND;SCANMENGE;SCANDATUM\n");
	
		for($LIKZeile=0;$LIKZeile<$rsLIKZeilen;$LIKZeile++)
		{			
			fputs($fd,$rsLIK['LIK_FIL_ID'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_AST_ATUNR'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_DATUMKOMM'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_LIEFERSCHEINNR'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_MENGESOLL'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_MENGEIST'][$LIKZeile]);			
			fputs($fd, ';' . $rsLIK['DIFFERENZ'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_SOLLBESTAND'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_ISTBESTAND'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_MENGESCAN'][$LIKZeile]);
			fputs($fd, ';' . $rsLIK['LIK_DATUMSCAN'][$LIKZeile]);
			fputs($fd, "\n");
		}
	
		// Fertig, Datei schlie�en
		fclose($fd);
		$DateiName = pathinfo($DateiName);
		echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
	}
}
?>

