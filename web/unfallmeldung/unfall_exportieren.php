<?php

$filename = "/daten/web/dokumente/sonstiges/unfallmeldung/UnfallmeldungAuswertung.xlsx";

if(file_exists($filename)) {

    $content = file_get_contents($filename);

    Header('Content-Length: ' . strlen($content));
    Header('Content-disposition: attachment; filename=UnfallmeldungAuswertung.xlsx');
    header('Content-Type: application/x-download');
    header('Cache-Control: private, max-age=0, must-revalidate');
    header('Pragma: public');
    if (headers_sent()) {
        die('<B>Download error: </B> Some data has already been output to browser, can\'t send ICS file');
    }
    echo $content;
    sleep(2); //Sleep sonst kann es Probleme beim Herunterladen geben

} else {
    die('<B>Download error: </B> Datei konnte nicht gefunden werden');
}