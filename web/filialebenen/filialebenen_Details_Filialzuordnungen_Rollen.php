<?php
require_once('awisFilialEbenen.inc');

global $con;
global $AWISSprache;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('FRZ','%');
$TextKonserven[]=array('FER','%');
$TextKonserven[]=array('Wort','Filialebene');
$TextKonserven[]=array('KON','KON_NAME');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3805 = awisBenutzerRecht($con,3805,$AWISBenutzer->BenutzerName());
if($Recht3805==0)
{
    awisEreignis(3,1000,'Filialebenenrollen',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

awis_FORM_FormularStart();

$EditRecht=(($Recht3805&6)!=0);

//***********************************
//Kontakte anzeigen
//***********************************

$SQL = 'SELECT FILIALEBENENROLLENZUORDNUNGEN.*, FER_BEZEICHNUNG, KON_NAME1 || COALESCE(\', \' || KON_NAME2,\'\') AS KON_NAME, FEZ_FIL_ID';
$SQL .= ' FROM FILIALEBENENZUORDNUNGEN';
$SQL .= ' INNER JOIN FILIALEBENENROLLENZUORDNUNGEN ON FRZ_XXX_KEY = FEZ_FIL_ID AND FRZ_XTN_KUERZEL = \'FIL\'';
$SQL .= ' INNER JOIN Filialebenenrollen ON FRZ_FER_KEY = FER_KEY';
$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe on fbr_frb_key = fer_frb_key and fbr_xbn_key = '.$AWISBenutzer->BenutzerID();
$SQL .= ' INNER JOIN Kontakte ON FRZ_KON_KEY = KON_KEY';
$SQL .= ' WHERE FEZ_KEY=0'.$AWIS_KEY2;

if(isset($_GET['FRZ_KEY']))
{
	$SQL .= ' AND FRZ_KEY = '.floatval($_GET['FRZ_KEY']);
}

if(!isset($_GET['FRZSort']))
{
	$SQL .= ' ORDER BY FER_BEZEICHNUNG DESC';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['FRZSort']);
}
$rsFRZ = awisOpenRecordset($con,$SQL);
$rsFRZZeilen = $awisRSZeilen;
awis_Debug(1,$SQL,$AWIS_KEY1);

if(!isset($_GET['FRZ_KEY']) OR isset($_GET['FRZListe']))					// Liste anzeigen
{
	awis_FORM_ZeileStart();

	if(($Recht3805&6)>0)
	{
		$Icons[] = array('new','./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FEZ_KEY='.$AWIS_KEY2.'&FRZ_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}

	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FEZ_KEY='.$AWIS_KEY2;
	$Link .= '&FRZSort=FER_BEZEICHNUNG'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='FER_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FER']['FER_BEZEICHNUNG'],300,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FEZ_KEY='.$AWIS_KEY2;	
	$Link .= '&FRZSort=KON_NAME'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='KON_NAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KON']['KON_NAME'],200,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FEZ_KEY='.$AWIS_KEY2;
	$Link .= '&FRZSort=FRZ_GUELTIGAB'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='FRZ_GUELTIGAB'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'],100,'',$Link);
	$Link = './filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FEZ_KEY='.$AWIS_KEY2;
	$Link .= '&FRZSort=FRZ_GUELTIGBIS'.((isset($_GET['FRZSort']) AND ($_GET['FRZSort']=='FRZ_GUELTIGBIS'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FRZ']['FRZ_GUELTIGBIS'],100,'',$Link);	

	awis_FORM_ZeileEnde();

	for($FRZZeile=0;$FRZZeile<$rsFRZZeilen;$FRZZeile++)
	{
		if($rsFRZ['FER_BEZEICHNUNG'][$FRZZeile]=='')
		{
			continue;
		}
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3805&2)>0)	// �ndernrecht
		{
			$Icons[] = array('edit','./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FEZ_KEY='.$AWIS_KEY2.'&FRZ_KEY='.$rsFRZ['FRZ_KEY'][$FRZZeile]);
		}
		if(($Recht3805&4)>0)	// L�schen
		{
			$Icons[] = array('delete','./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FEB_KEY='.$AWIS_KEY1.'&Del='.$rsFRZ['FRZ_KEY'][$FRZZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($FRZZeile%2));

		$Style='';

		if(awis_PruefeDatum($rsFRZ['FRZ_GUELTIGAB'][$FRZZeile],1,1) > time())
		{
			$Style='font-style:italic;color:#FF2020';
		}
		if(awis_PruefeDatum($rsFRZ['FRZ_GUELTIGBIS'][$FRZZeile],1,1) < time())
		{
			$Style='font-style:italic;color:#FF2020';
		}
		awis_FORM_Erstelle_ListenFeld('#FER_BEZEICHNUNG',$rsFRZ['FER_BEZEICHNUNG'][$FRZZeile],20,300,false,($FRZZeile%2),$Style,'','T');
		awis_FORM_Erstelle_ListenFeld('#KON_NAME',$rsFRZ['KON_NAME'][$FRZZeile],20,200,false,($FRZZeile%2),$Style,'','T');
		awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGAB',$rsFRZ['FRZ_GUELTIGAB'][$FRZZeile],20,100,false,($FRZZeile%2),$Style,'','D');
		awis_FORM_Erstelle_ListenFeld('#FRZ_GUELTIGBIS',$rsFRZ['FRZ_GUELTIGBIS'][$FRZZeile],20,100,false,($FRZZeile%2),$Style,'','D');
		awis_FORM_ZeileEnde();
	}

	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	
	if ($_GET['FRZ_KEY']=='0')
	{
		$SQL = 'SELECT FEZ_FIL_ID';
		$SQL .= ' FROM FILIALEBENENZUORDNUNGEN';		
		$SQL .= ' WHERE FEZ_KEY=0'.$AWIS_KEY2;	
		
		$rsFEZ = awisOpenRecordset($con,$SQL);
		$rsFEZZeilen = $awisRSZeilen;			
	}
	
	
	awis_FORM_FormularStart();
//var_dump($rsFRZ['FRZ_KEY'][0],$rsFRZ['FEZ_FIL_ID'][0]);
	echo '<input name=txtFRZ_KEY type=hidden value=0'.(isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_KEY'][0]:'').'>';
	//echo '<input name=txtFRZ_XXX_KEY type=hidden value=0'.$AWIS_KEY1.'>';
	echo '<input name=txtFRZ_XXX_KEY type=hidden value=0'.(isset($rsFRZ['FEZ_FIL_ID'][0])?$rsFRZ['FEZ_FIL_ID'][0]:$rsFEZ['FEZ_FIL_ID'][0]).'>';
	echo '<input name=txtFRZ_XTN_KUERZEL type=hidden value="FIL">';

	
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./filialebenen_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Unterseite=FilialzuordnungenRollen&FRZListe=1&FEZ_KEY=".$AWIS_KEY2." accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");

	$AWIS_KEY2 = (isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_KEY'][0]:'0');
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsFRZ['FRZ_KEY'][0])?$rsFRZ['FRZ_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_FER_KEY'].':',150);
	$SQL = "select FER_KEY, FER_BEZEICHNUNG ";
	$SQL .= ' FROM FilialebenenRollen ';
	$SQL .= ' INNER JOIN Filialebenenrollenbereiche ON FER_FRB_KEY = FRB_KEY';
	$SQL .= ' INNER JOIN Filialebenenrollenberzugriffe ON FBR_FRB_KEY = FRB_KEY AND (FBR_XBN_KEY = 0'.$AWISBenutzer->BenutzerID() . ') AND FBR_RECHT > 0';
	$SQL .= ' ORDER BY FER_BEZEICHNUNG';
	awis_FORM_Erstelle_SelectFeld('FRZ_FER_KEY',($AWIS_KEY2==0?'':$rsFRZ['FRZ_FER_KEY'][0]),200,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_KON_KEY'].':',150);
	$SQL = 'select KON_KEY, KON_NAME1 || COALESCE(\', \'||KON_NAME2,\'\') AS KON_NAME ';
	$SQL .= ' FROM Kontakte ';
	$SQL .= ' WHERE (KON_STATUS=\'A\' OR KON_KEY = '.($AWIS_KEY2==0?'0':$rsFRZ['FRZ_KON_KEY'][0]).')';
	$SQL .= ' AND (KON_PER_NR IS NOT NULL OR KON_KEY = 8479)';
	$SQL .= ' ORDER BY KON_NAME1, KON_NAME2';
	awis_FORM_Erstelle_SelectFeld('FRZ_KON_KEY',($AWIS_KEY2==0?'':$rsFRZ['FRZ_KON_KEY'][0]),300,$EditRecht,$con,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_Textarea('FRZ_BEMERKUNG',(isset($rsFRZ['FRZ_BEMERKUNG'][0])?$rsFRZ['FRZ_BEMERKUNG'][0]:''),600,120,5,($Recht3805&6),'');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_GUELTIGAB'].':',150);
	awis_FORM_Erstelle_TextFeld('FRZ_GUELTIGAB',(isset($rsFRZ['FRZ_GUELTIGAB'][0])?$rsFRZ['FRZ_GUELTIGAB'][0]:''),10,200,($Recht3805&6),'','','','D','','',awis_PruefeDatum(date('d.m.Y')));
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FRZ']['FRZ_GUELTIGBIS'].':',150);
	awis_FORM_Erstelle_TextFeld('FRZ_GUELTIGBIS',(isset($rsFRZ['FRZ_GUELTIGBIS'][0])?$rsFRZ['FRZ_GUELTIGBIS'][0]:''),10,200,($Recht3805&6),'','','','D','','',awis_PruefeDatum('31.12.2030'));
	awis_FORM_ZeileEnde();

	awis_FORM_FormularEnde();
}
?>