<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SVL','%');
	$TextKonserven[]=array('Liste','SVL%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_DSZurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_DSWeiter');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');



	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$Form = new awisFormular();
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$AWISBenutzer = awisBenutzer::Init();
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


	$Recht19000 = $AWISBenutzer->HatDasRecht(19000);			// Recht f�r die Teilnehmer
	if($Recht19000==0)
	{
		$Form->Formular_Start();
		$Form->Fehler_KeineRechte();
		$Form->Formular_Ende();
		die();
	}

	$Bedingung = ' AND SVL_SVT_KEY = '.$AWIS_KEY2;
	$SQL = 'SELECT SVCLeistungen.*, FIL_BEZ, PRS.NAME || \', \' || PRS.VORNAME AS PRS_NAME ';
	$SQL .= ' FROM SVCLeistungen';
	$SQL .= ' INNER JOIN Filialen ON SVL_FIL_ID = FIL_ID';
	$SQL .= ' LEFT OUTER JOIN Personal_Komplett PRS ON SVL_PERSNR = PRS.PERSNR';

	if(isset($_GET['SVL_KEY']))
	{
		$Bedingung .= ' AND SVL_KEY = 0'.$DB->FeldInhaltFormat('Z',$_GET['SVL_KEY']);
	}
	if($AWIS_KEY2!=0)
	{
	//	$Bedingung .= ' AND SVL_KEY = 0'.$AWIS_KEY2;
	}

	$SQL .= ($Bedingung!=''?' WHERE '.substr($Bedingung,4):'');

	if(isset($_GET['SVLSort']))
	{
		$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['SVLSort']);
	}
	else
	{
		$SQL .= ' ORDER BY SVL_DATUM';
	}

	//awis_Debug(1,$SQL,$_GET);
	$rsSVL = $DB->RecordSetOeffnen($SQL);

	if(!isset($_GET['SVL_KEY']))					// Liste anzeigen
	{
		$Form->ZeileStart();

		if(($Recht19000&4))
		{
			$Icons = array();
			$Icons[] = array('new','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Leistungen&SVT_KEY='.$AWIS_KEY2.'&SVL_KEY=0');
			$Form->Erstelle_ListeIcons($Icons,38,-1);
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Leistungen';
		$Link .= '&SVT_KEY='.$AWIS_KEY2.'&SVLSort=SVL_DATUM'.((isset($_GET['SVLSort']) AND ($_GET['SVLSort']=='SVL_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_DATUM'],100,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Leistungen';
		$Link .= '&SVT_KEY='.$AWIS_KEY2.'&SVLSort=SVL_FIL_ID'.((isset($_GET['SVLSort']) AND ($_GET['SVLSort']=='SVL_FIL_ID'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVL']['SVL_FIL_ID'],100,'',$Link);

		$Form->ZeileEnde();

		$DS=0;
		while(!$rsSVL->EOF())
		{
			$Form->ZeileStart();
			$Icons = array();
			if(($Recht19000&2))	// �ndernrecht
			{
				$Icons[] = array('edit','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Leistungen&SVT_KEY='.$AWIS_KEY2.'&SVL_KEY='.$rsSVL->FeldInhalt('SVL_KEY'));
			}
			if(($Recht19000&4))	// L�schenrecht
			{
				$Icons[] = array('delete','./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Leistungen&Del='.$rsSVL->FeldInhalt('SVL_KEY').'&Key='.$AWIS_KEY1.'&SVT_KEY='.$AWIS_KEY2.'&SVL_KEY='.$rsSVL->FeldInhalt('SVL_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));


			$Form->Erstelle_ListenFeld('*SVL_DATUM',$rsSVL->FeldInhalt('SVL_DATUM'),10,100,false,($DS%2),'','','T','C');
			$Link = '/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsSVL->FeldInhalt('SVL_FIL_ID');
			$Form->Erstelle_ListenFeld('*SVL_FIL_ID',$rsSVL->FeldInhalt('SVL_FIL_ID'),10,100,false,($DS%2),'',$Link,'T','L',$rsSVL->FeldInhalt('FIL_BEZ'));


			$Form->ZeileEnde();
			$DS++;

			$rsSVL->DSWeiter();
		}
		$Form->Formular_Ende();
	}
	else 		// Einer oder keiner
	{
		$Form->Formular_Start();

		echo '<input name=txtSVL_KEY type=hidden value=0'.($rsSVL->FeldInhalt('SVL_KEY')).'>';
		echo '<input name=txtSVL_SVT_KEY type=hidden value=0'.$AWIS_KEY2.'>';

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Details&Seite=Fahrzeuge&Unterseite=Leistungen&SVT_KEY=".$AWIS_KEY2."&SVLListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsSVL->FeldInhalt('SVL_USER')));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsSVL->FeldInhalt('SVL_USERDAT')));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();

		$EditModus = ($rsSVL->FeldInhalt('SVL_USER')!='WWS'?($Recht19000&6):false);

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_DATUM'].':',150);
		$Form->Erstelle_TextFeld('SVL_DATUM',$rsSVL->FeldInhalt('SVL_DATUM'),10,200,$EditModus,'','','','D');
		$AWISCursorPosition=($EditModus?'txtSVL_DATUM':$AWISCursorPosition);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_FIL_ID'].':',150);
		$Link = ($rsSVL->FeldInhalt('SVL_FIL_ID')==''?'':'/filialinfos/filialinfos_Main.php?cmdAktion=Details&FIL_ID='.$rsSVL->FeldInhalt('SVL_FIL_ID'));
		$Form->Erstelle_TextFeld('SVL_FIL_ID',$rsSVL->FeldInhalt('SVL_FIL_ID'),5,200,$EditModus,'','','','N0');
		$Form->Erstelle_TextFeld('*FIL_BEZ',$rsSVL->FeldInhalt('FIL_BEZ'),0,200,false,'','',$Link);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_WANR'].':',150);
		$Form->Erstelle_TextFeld('SVL_WANR',$rsSVL->FeldInhalt('SVL_WANR'),15,200,$EditModus,'','','','N0');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_AST_ATUNR'].':',150);
		$Form->Erstelle_TextFeld('SVL_AST_ATUNR',$rsSVL->FeldInhalt('SVL_AST_ATUNR'),15,250,$EditModus,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_BEZEICHNUNG'].':',150);
		$Form->Erstelle_TextFeld('SVL_BEZEICHNUNG',$rsSVL->FeldInhalt('SVL_BEZEICHNUNG'),50,400,$EditModus,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_MENGE'].':',150);
		$Form->Erstelle_TextFeld('SVL_MENGE',$rsSVL->FeldInhalt('SVL_MENGE'),15,200,$EditModus,'','','','N0');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_VKPREIS'].':',150);
		$Form->Erstelle_TextFeld('SVL_VKPREIS',$rsSVL->FeldInhalt('SVL_VKPREIS'),6,250,$EditModus,'','','','N2');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_UMSATZ'].':',150);
		$Form->Erstelle_TextFeld('SVL_UMSATZ',$rsSVL->FeldInhalt('SVL_UMSATZ'),6,200,$EditModus,'','','','N2');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KENNZEICHEN'].':',150);
		$Form->Erstelle_TextFeld('SVL_KENNZEICHEN',$rsSVL->FeldInhalt('SVL_KENNZEICHEN'),15,200,$EditModus,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KTYPNR'].':',150);
		$Form->Erstelle_TextFeld('SVL_KTYPNR',$rsSVL->FeldInhalt('SVL_KTYPNR'),15,200,$EditModus,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KUNDENNAME1'].':',150);
		$Form->Erstelle_TextFeld('SVL_KUNDENNAME1',$rsSVL->FeldInhalt('SVL_KUNDENNAME1'),40,250,$EditModus,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KUNDENNAME2'].':',150);
		$Form->Erstelle_TextFeld('SVL_KUNDENNAME2',$rsSVL->FeldInhalt('SVL_KUNDENNAME2'),40,200,$EditModus,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_PERSNR'].':',150);
		$Form->Erstelle_TextFeld('SVL_PERSNR',$rsSVL->FeldInhalt('SVL_PERSNR'),10,250,$EditModus,'','','','T');
		$Form->Erstelle_TextFeld('*PRS_NAME',$rsSVL->FeldInhalt('PRS_NAME'),40,200,false,'','','','T');
		$Form->ZeileEnde();
		
		$Form->Trennzeile();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_KENNUNG'].':',150);
		$Form->Erstelle_TextFeld('SVL_KENNUNG',$rsSVL->FeldInhalt('SVL_KENNUNG'),35,200,$EditModus,'','','','T');
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_IMPORTZEITPUNKT'].':',150);
		$Form->Erstelle_TextFeld('SVL_IMPORTZEITPUNKT',$rsSVL->FeldInhalt('SVL_IMPORTZEITPUNKT'),20,250,$EditModus,'','','','DU');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_STATUS'].':',150);
		$ListenDaten = explode("|",$AWISSprachKonserven['Liste']['SVL_STATUS']);
		$Form->Erstelle_SelectFeld('SVL_STATUS',$rsSVL->FeldInhalt('SVL_STATUS'),"200:190",$EditModus,'','','A','','',$ListenDaten);
		$Form->ZeileEnde();
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVL']['SVL_MELDEZEITPUNKT'].':',150);
		$Form->Erstelle_TextFeld('SVL_MELDEZEITPUNKT',$rsSVL->FeldInhalt('SVL_MELDEZEITPUNKT'),20,200,$EditModus,'','','','DU');
		$Form->ZeileEnde();
		
		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200901151024");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200901151025");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>