<?php
/****************************************************************************
 *                                            *
 *                          VERARBEITUNGSSCRIPT V1.0                *
 *
 * @author                                                            Stefan Oppl                    *
 *                                                                    *
 * @todo                                                              Versicherungsdaten verarbeiten        *
 *                                                                    *
 *                                                                    *
 *                                                                    ***************************************************************************/

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');


class VersicherungenVerarbeiten
{
    private $_DebugLevel;
    private $_DB;
    private $_AWISBenutzer;
    private $_SQL;
    private $_FORM;
    private $_WaNr;
    private $_FilNr;
    private $_FilNrAkt;
    private $_WaNrAkt;
    private $_PfadExportVers = '/daten/daten/versicherungen/export/';
    private $_DateinameExportVers = 'export_vers_';
    private $_DateinameExportDebeka = 'export_debeka_';
    private $_DateinameExportDBVW = 'export_dbvw_';
    private $_DateinameExportWV = 'export_wue_';
    private $_DateinameExportWGV = 'export_wgv_';
    private $_DateinameExportAXA = 'export_axa_';
    private $_DateinameExportHDI = 'export_hdi_';
    private $_DateinameExportHUK = 'export_huk_';
    private $_DateinameExportVHV = 'export_vhv_';
    private $_DateinameExportRuV = 'export_ruv_';
    private $_DateinameExportLVM = 'export_lvm_';
    private $_DateinameExportCONTI = 'export_conti_';
    private $_DateinameExportOESA = 'export_oesa_';
    private $_DateinameExportZuerich = 'export_zuerich_';
    private $_EMAIL_Infos = array('shuttle@de.atu.eu');
    private $Werkzeug;

    /**
     * Initialisierung
     */
    function __construct($DebugLevel = 0, $Benutzer)
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_AWISBenutzer = awisBenutzer::Init($Benutzer);
        $this->_DebugLevel = $DebugLevel;
        $this->_FORM = new awisFormular();
        $this->Werkzeug = new awisWerkzeuge();
    }

    /**
     * Scheibentauschvorgaenge pruefen (sind AFG-Daten vorhanden,
     * passt das KFZ-Kennz mit den AFG-Daten ueberein und stimmt
     * der Betrag mit den AFG-Daten ueberein. Au�erdem darf
     * neben GLAS01 kein anderer Artikel auf dem WA stehen
     * (au�er Feinstaubplaketen)
     */

    public function SperreVoraengeMitUmsatz0()
    {
        $this->SchreibeModulStatus('SperreVoraengeMitUmsatz0', 'Start: Sperre Voraenge mit Umsatz 0 ...');
        $this->debugAusgabe("Start: SperreVoraengemitUmsatz0", 10);
        try
        {
            $this->debugAusgabe('Update versvorgangsstatus mit Fehlerstatus = 16, und Checkstatus = 120...', 10);

            $SQL = 'UPDATE versvorgangsstatus vvs';
            $SQL .= ' SET vvs.vvs_ven_key = 16,';
            $SQL .= '   vvs.vvs_vcn_key   = 120';
            $SQL .= ' WHERE vvs.vvs_key  IN';
            $SQL .= '   (SELECT vv.vvs_key';
            $SQL .= '   FROM';
            $SQL .= '     (SELECT vvs.vvs_key,';
            $SQL .= '       vvs.vvs_filid,';
            $SQL .= '       vvs.vvs_wanr,';
            $SQL .= '       vep.umsatz';
            $SQL .= '     FROM';
            $SQL .= '       (SELECT SUM(vep.umsatz) AS umsatz,';
            $SQL .= '         vep.filnr,';
            $SQL .= '         vep.wanr';
            $SQL .= '       FROM v_vers_form_posdaten vep';
            $SQL .= '       GROUP BY vep.filnr,';
            $SQL .= '         vep.wanr';
            $SQL .= '       ) vep';
            $SQL .= '     INNER JOIN versvorgangsstatus vvs';
            $SQL .= '     ON vep.FILNR    = vvs.vvs_filid';
            $SQL .= '     AND vep.wanr    = vvs.vvs_wanr';
            $SQL .= '     WHERE umsatz   <= 0';
            $SQL .= '     AND vvs_vcn_key = 0';
            $SQL .= '     AND vvs_ven_key = 0';
            $SQL .= '     ) vv';
            $SQL .= '   )';

            $this->_DB->Ausfuehren($SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->debugAusgabe("Ende: SperreVoraengemitUmsatz0", 10);
        $this->SchreibeModulStatus('SperreVoraengeMitUmsatz0', 'Ende: Sperre Voraenge mit Umsatz 0 ...');
    }

    public function PruefeDubletten()
    {
        //Pr�ft folgende Kriterien auf Gleicheit:
        //Filialnummer, Kennzeichen, Versicherungsnummer, Kassierdatum
        //und setzt den Fehlerstatus: 29 und dubcheck = 1 f�r doppelter Rechnungsversand --> Pruefung durch Fachabteilung
        $this->SchreibeModulStatus('PruefeDubletten', 'Start: Pruefe Dubletten ...');
        $this->debugAusgabe("Start: Pruefe Dubletten", 10);

        try
        {
            $this->debugAusgabe("Updatet Vorgaenge in versvorgangsstatus, mit Fehlerstatus = 29 und dubcheck = 1, bei Gleichheit versch. Kriterien...", 10);

            $SQL = 'SELECT DISTINCT vve_kuerzel FROM versversicherungen WHERE vve_aktiv = 1';
            $rsKuerzel = $this->_DB->RecordSetOeffnen($SQL);
            $search_array = array();

            while(!$rsKuerzel->EOF())
            {
                $SQL = 'SELECT * FROM VERSVORGANGSSTATUS vvs';
                $SQL .= ' INNER JOIN VERSVERSICHERUNGEN vve';
                $SQL .= ' ON vvs.VVS_VERSNR = vve.VVE_VERSNR';
                $SQL .= ' INNER JOIN v_vers_form_bearbdaten b';
                $SQL .= ' ON b.filnr                        = vvs.vvs_filid';
                $SQL .= ' AND b.WANR                        = vvs.vvs_wanr';
                $SQL .= ' AND b.STATUS                      = \'E\'';
                $SQL .= ' INNER JOIN VERSCHECKSTATUSNUMMERN vcn';
                $SQL .= ' ON vvs.vvs_vcn_key = vcn.vcn_key ';
                $SQL .= ' WHERE (VVS_VCN_KEY                = 0';
                $SQL .= ' OR VVS_VCN_KEY NOT               IN (120,124))';
                $SQL .= ' AND VVS_SCHADENKZ                IN (\'E\', \'S\', \'T\')';
                $SQL .= ' AND ceil(TRUNC(sysdate)-b.datum) <= 90';
                $SQL .= ' AND vve.VVE_KUERZEL = \'' . $rsKuerzel->FeldInhalt('VVE_KUERZEL') . '\'';

                $rsDubCheck = $this->_DB->RecordSetOeffnen($SQL);

                $neuerDatensatz = true;
                $pruefKey = null;

                while(!$rsDubCheck->EOF())
                {
                    $aktString = preg_replace('/[^a-zA-Z�������0-9]/i', '', $rsDubCheck->FeldInhalt('VVS_KFZKENNZ') . $rsDubCheck->FeldInhalt('VVS_VERSSCHEINNR'));

                    $DatensatzNrVorher = $rsDubCheck->DSNummer();

                    $aktKey = $rsDubCheck->FeldInhalt('VVS_KEY');
                    $aktKategorie = $rsDubCheck->FeldInhalt('VCN_KATEGORIE');

                    $rsDubCheck->DSErster();

                    while(!$rsDubCheck->EOF())
                    {
                        $pruefKey = $rsDubCheck->FeldInhalt('VVS_KEY');
                        $pruefKategorie = $rsDubCheck->FeldInhalt('VCN_KATEGORIE');

                        $neuerDatensatz = true;

                        if($aktKey == $pruefKey)
                        {
                            $neuerDatensatz = false;
                        }
                        if(isset($search_array[$pruefKey][$aktKey]))
                        {
                            $neuerDatensatz = false;
                        }

                        if($neuerDatensatz)
                        {
                            $pruefString = preg_replace('/[^a-zA-Z�������0-9]/i', '', $rsDubCheck->FeldInhalt('VVS_KFZKENNZ') . $rsDubCheck->FeldInhalt('VVS_VERSSCHEINNR'));

                            similar_text(mb_strtoupper($aktString), mb_strtoupper($pruefString), $percent);

                            if($percent > 90)
                            {
                                $this->_UpdateDubletten($aktKategorie, $aktKey);
                                $this->_UpdateDubletten($pruefKategorie, $pruefKey);

                                $SQL = 'MERGE INTO VERSICHERUNGEN_DUBLETTEN VDB USING';
                                $SQL .= ' (SELECT ' . $aktKey . ' AS AKTKEY, ';
                                $SQL .= '         ' . $pruefKey . ' AS PRUEFKEY ';
                                $SQL .= '  FROM dual) vvs ';
                                $SQL .= '  ON ((vvs.AKTKEY = vdb.vdb_vvs_key AND vvs.PRUEFKEY = vdb.vdb_ref) ';
                                $SQL .= '   OR (vvs.PRUEFKEY = vdb.vdb_vvs_key AND vvs.AKTKEY = vdb.vdb_ref))';
                                $SQL .= ' WHEN NOT MATCHED THEN';
                                $SQL .= '   INSERT';
                                $SQL .= '     ( VDB_VVS_KEY, ';
                                $SQL .= '       VDB_REF';
                                $SQL .= '     ) VALUES';
                                $SQL .= '     ( vvs.AKTKEY, ';
                                $SQL .= '       vvs.PRUEFKEY';
                                $SQL .= '     )';

                                $this->_DB->Ausfuehren($SQL, '', false);
                                $search_array[$aktKey][$pruefKey] = true;
                            }
                        }

                        $rsDubCheck->DSWeiter();
                    }

                    $rsDubCheck->GeheZu($DatensatzNrVorher);

                    $rsDubCheck->DSWeiter();
                }
                $rsKuerzel->DSWeiter();
            }
            $this->debugAusgabe("Updatet versvorgangsstatus, mit Fehlerstatus = 29, bei Vorgaengen mit gleicher Vorgangnr...", 10);

            $SQL = 'update versvorgangsstatus';
            $SQL .= '        set vvs_ven_key = 29';
            $SQL .= '        where vvs_key in(';
            $SQL .= '        select vvs_key';
            $SQL .= '        from versvorgangsstatus vers';
            $SQL .= '        inner join (select v.vvs_vorgangnr';
            $SQL .= '        from versvorgangsstatus v';
            $SQL .= '        where vvs_vcn_key in (107,108,109,110,111,112,113,114,115,116,117,118,119,125,126)) vv ';
            $SQL .= '        on vers.vvs_vorgangnr = vv.vvs_vorgangnr';
            $SQL .= '        where vvs_vcn_key = 0';
            $SQL .= '        and vvs_ven_key = 0';
            $SQL .= '        and vvs_freigabe = 0)';

            $this->_DB->Ausfuehren($SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }

        $this->debugAusgabe("Ende: Pruefe Dubletten", 10);
        $this->SchreibeModulStatus('PruefeDubletten', 'Pruefung abgeschlossen ...');
    }

    private function _UpdateDubletten($StatusKategorie, $UpdateKey)
    {
        $SQL = 'UPDATE versvorgangsstatus';
        $SQL .= ' SET VVS_VEN_KEY    = 29';
        $SQL .= ' WHERE vvs_dubcheck = 0';
        $SQL .= ' AND vvs_key = ' . $UpdateKey;

        $this->_DB->Ausfuehren($SQL, '', true);

        if($StatusKategorie == 'BEARBEITUNG')
        {
            $KeyDubCheck = 1;
        }
        elseif($StatusKategorie == 'VERARBEITET')
        {
            $KeyDubCheck = 2;
        }

        $SQL = ' UPDATE versvorgangsstatus vvs';
        $SQL .= ' SET vvs.vvs_dubcheck   =' . $this->_DB->WertSetzen('VVS', 'N0', $KeyDubCheck);
        $SQL .= ' WHERE vvs.vvs_key =' . $UpdateKey;
        $SQL .= ' AND vvs.vvs_dubcheck = 0';

        $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->Bindevariablen('VVS'));
    }

    public function PruefFalscheSKZ()
    {
        $this->SchreibeModulStatus('PruefFalscheSKZ', 'Ermittle falsche SKZ ...');
        $this->debugAusgabe('Start: PruefFalscheSKZ.', 10);
        try
        {
            $this->debugAusgabe("Pruefe Vorgaenge mit falschen SKZ.. ", 10);
            // Pr�fen bei welchen Status noch selektiert werden muss
            $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vp.artkz, vp.bez, ';
            $this->_SQL .= 'vp.menge, vp.vk_preis, vp.umsatz, vp.rabattkenn ';
            $this->_SQL .= 'FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
            $this->_SQL .= 'ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
            $this->_SQL .= 'WHERE vs.vvs_ven_key = 0 ';
            $this->_SQL .= 'AND vs.vvs_vcn_key = 0 ';
            $this->_SQL .= 'AND vs.vvs_schadenkz NOT IN (\'S\',\'T\',\'E\',\'K\', \'P\') ';
            $this->_SQL .= 'ORDER BY vp.filnr, vp.wanr, vp.position';

            // Pr�fen ob Vorg�nge zum pr�fen vorhanden sind

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
            {
                $RsSKZ_Falsch = $this->_DB->RecordSetOeffnen($this->_SQL);

                while(!$RsSKZ_Falsch->EOF())
                {
                    $this->_FilNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_Falsch->FeldInhalt('FILNR'), false);
                    $this->_WaNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_Falsch->FeldInhalt('WANR'), false);

                    $this->SetzeStatus(3);

                    $RsSKZ_Falsch->DSWeiter();
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->debugAusgabe('Ende: PruefFalscheSKZ.', 10);
        $this->SchreibeModulStatus('PruefFalscheSKZ', 'Pr�fung abgeschlossen');
    }

    public function PruefeUndBearbeiteSKZ_K()
    {
        $this->SchreibeModulStatus('PruefeUndBearbeiteSKZ_K', 'Start: PruefeUndBearbeiteSKZ_K');
        $this->debugAusgabe('Start: PruefeUndBearbeiteSKZ_K', 10);

        try
        {
            $this->debugAusgabe('Setze "K" auf "E", wenn GLAS01 enthalten ...', 10);

            //setzt alle neuen Vorgaenge mit dem SKZ 'K' (Karolack), die GLAS01 enthalten auf das SKZ 'E' (Scheibentausch)
            $this->_SQL = 'update versvorgangsstatus vs';
            $this->_SQL .= ' set vvs_schadenkz = \'E\'';
            $this->_SQL .= ' where EXISTS (';
            $this->_SQL .= ' SELECT * ';
            $this->_SQL .= ' FROM v_vers_form_posdaten vp';
            $this->_SQL .= ' WHERE vs.vvs_filid = vp.filnr ';
            $this->_SQL .= ' AND vs.vvs_wanr = vp.wanr';
            $this->_SQL .= ' AND vs.vvs_schadenkz = \'K\'';
            $this->_SQL .= ' AND vp.ARTNR = \'GLAS01\')';
            $this->_SQL .= ' AND vs.vvs_ven_key = 0';
            $this->_SQL .= ' AND vs.vvs_vcn_key = 0';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            $this->debugAusgabe('PruefeUndBearbeiteSKZ_K', 'Setze "K" auf "S", wenn 1EIN69 enthalten ...', 10);

            //setzt alle neuen Vorgaenge mit dem SKZ 'K' (Karolack), die 1EIN69 enthalten auf das SKZ 'S' (Steinschlag)
            $this->_SQL = 'update versvorgangsstatus vs';
            $this->_SQL .= ' set vvs_schadenkz = \'S\'';
            $this->_SQL .= ' where EXISTS (';
            $this->_SQL .= ' SELECT * ';
            $this->_SQL .= ' FROM v_vers_form_posdaten vp';
            $this->_SQL .= ' WHERE vs.vvs_filid = vp.filnr ';
            $this->_SQL .= ' AND vs.vvs_wanr = vp.wanr';
            $this->_SQL .= ' AND vs.vvs_schadenkz = \'K\'';
            $this->_SQL .= ' AND vp.ARTNR = \'1EIN69\')';
            $this->_SQL .= ' AND vs.vvs_ven_key = 0';
            $this->_SQL .= ' AND vs.vvs_vcn_key = 0';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            $this->debugAusgabe('PruefeUndBearbeiteSKZ_K', 'Setze "K" auf Status 99, wenn "1KAL" enthalten ...', 10);

            //setzt alle neuen Vorgaenge mit dem SKZ 'K' (Karolack), auf den Checkstatus = 99
            // -> echter Karolokack-Vorgang (wird in der weiteren Verarbeitung nicht weiter beachtet)
            //hier wurde ein Checkstatus verwendet und kein Sperrstatus,
            //damit diese Vorgaenge nicht in der Sperrliste zur Bearbeitung f�r die Fachabteilung auftauchen
            $this->_SQL = 'update versvorgangsstatus vs';
            $this->_SQL .= ' set vvs_vcn_key = 99';
            $this->_SQL .= ' where EXISTS (';
            $this->_SQL .= ' SELECT * ';
            $this->_SQL .= ' FROM v_vers_form_posdaten vp';
            $this->_SQL .= ' WHERE vs.vvs_filid = vp.filnr ';
            $this->_SQL .= ' AND vs.vvs_wanr = vp.wanr';
            $this->_SQL .= ' AND vs.vvs_schadenkz = \'K\'';
            $this->_SQL .= ' AND vp.ARTNR LIKE \'1KAL%\')';
            $this->_SQL .= ' AND vs.vvs_ven_key = 0';
            $this->_SQL .= ' AND vs.vvs_vcn_key = 0';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            $this->debugAusgabe('PruefeUndBearbeiteSKZ_K', 'Setze restliche "K" auf "T" ...', 10);

            //alle restlichen Vorgaenge mit dem SKZ 'K' (Karolack), werden auf das SKZ 'T' (sonstiger Kasko) gesetzt
            //da noch kein Status gestzt wurde und kein '1KAL' enthalten ist
            $this->_SQL = 'update versvorgangsstatus vs';
            $this->_SQL .= ' set vvs_schadenkz = \'T\'';
            $this->_SQL .= ' where vs.vvs_schadenkz = \'K\'';
            $this->_SQL .= ' AND vs.vvs_ven_key = 0';
            $this->_SQL .= ' AND vs.vvs_vcn_key = 0';

            $this->_DB->Ausfuehren($this->_SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;
            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->debugAusgabe('Ende: PruefeUndBearbeiteSKZ_K', 10);
        $this->SchreibeModulStatus('PruefeUndBearbeiteSKZ_K', 'Ende: PruefeUndBearbeiteSKZ_K');
    }

    public function PruefSKZ_E()
    {
        $this->SchreibeModulStatus('PruefSKZ_E', 'Pr�fe E Vorg�nge ...');
        $this->debugAusgabe('Start: PruefSKZ_E', 10);

        try
        {
            // Pr�fen bei welchen Status noch selektiert werden muss
            $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vp.artkz, vp.bez, ';
            $this->_SQL .= ' vp.menge, vp.vk_preis, vp.umsatz, vp.rabattkenn ';
            $this->_SQL .= ' FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
            $this->_SQL .= ' ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
            $this->_SQL .= ' WHERE vs.vvs_ven_key = 0 ';
            $this->_SQL .= ' AND vs.vvs_vcn_key = 0 ';
            $this->_SQL .= ' AND vs.vvs_schadenkz = \'E\' ';
            $this->_SQL .= ' ORDER BY vp.filnr, vp.wanr, vp.position';

            // Pr�fen ob Vorg�nge zum pr�fen vorhanden sind

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
            {
                $RsSKZ_E = $this->_DB->RecordSetOeffnen($this->_SQL);

                $this->_FilNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_E->FeldInhalt('FILNR'), false);
                $this->_WaNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_E->FeldInhalt('WANR'), false);

                //Z�hler f�r die Inidzierung der Arrays
                $i_GlasPos = 0;
                $i_SonstPos = 0;

                //Array in die alle Pos eines Vorgangs geschrieben werden
                //die in Ordnung sind
                $GlasPos = array('ARTNR' => array(), 'BETRAG' => array());

                //Array in die alle Pos eines Vorgangs geschrieben werden die
                //genauer Untersucht werden m�ssen
                $SonstPos = array('ARTNR' => array(), 'BETRAG' => array());

                while(!$RsSKZ_E->EOF())
                {
                    // Werte in Variablen sichern
                    $this->_FilNrAkt = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_E->FeldInhalt('FILNR'), false);
                    $this->_WaNrAkt = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_E->FeldInhalt('WANR'), false);

                    $Menge = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_E->FeldInhalt('MENGE'), false);
                    $Betrag = $this->_DB->FeldInhaltFormat('N2', $RsSKZ_E->FeldInhalt('VK_PREIS'), false);

                    //*******************************************************************************************
                    //*******************************************************************************************
                    if($this->_FilNr <> $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt || $this->_FilNr <> $this->_FilNrAkt && $this->_WaNr == $this->_WaNrAkt || $this->_FilNr == $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt)
                    {
                        //*******************************************************************************************
                        //*******************************************************************************************
                        // neuer Vorgang hat begonnen!!!

                        // vorheringen Vorgang auswerten ob OK
                        if((count($SonstPos, 1) - 2) / 2 <> 0)
                        {
                            // Falsche Pos sind vorhanden, pr�fen ob diese sich
                            // durch Preisnachlass wieder aufheben
                            $Summe = 0;

                            for($x = 0; $x < (count($SonstPos, 1) - 2) / 2; $x++)
                            {
                                $Summe = $Summe + $SonstPos['BETRAG'][$x];
                            }

                            if($Summe == 0)
                            {
                                // Die falschen Positionen heben sich durch
                                // z.B. Preissenkung auf und der Vorgang
                                // ist somit in Ordnung
                                $this->SetzeCheckStatus(100);
                                $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                            }
                            else
                            {
                                // Die falschen Positionen heben sich nicht
                                // durch z.B. Preissenkung auf und der Vorgang
                                // muss daher gesperrt werden
                                $this->SetzeStatus(1);
                            }
                        }
                        else
                        {
                            // Keine Falschen Positionen vorhanden
                            // Status sezten das Pr�fung keine Fehler ergab
                            $this->SetzeCheckStatus(100);
                            $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                        }
                        //*******************************************************************************************
                        //*******************************************************************************************
                        // Z�hlervariablen zur�cksetzen + Arrays leeren
                        $i_GlasPos = 0;
                        $i_SonstPos = 0;

                        //Array in die alle Pos eines Vorgangs geschrieben werden
                        //die in Ordnung sind
                        $GlasPos = array('ARTNR' => array(), 'BETRAG' => array());

                        //Array in die alle Pos eines Vorgangs geschrieben werden die
                        //genauer Untersucht werden m�ssen
                        $SonstPos = array('ARTNR' => array(), 'BETRAG' => array());

                        // Filnr und Wanr wieder sichern
                        $this->_FilNr = $this->_FilNrAkt;
                        $this->_WaNr = $this->_WaNrAkt;

                        //*******************************************************************************************
                        //*******************************************************************************************
                        // Erste Pos des Vorgangs Pr�fen und ins entsprechende Array schreiben

                        if($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'GLAS01\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN04\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN01\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN02\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN03\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        else
                        {
                            $SonstPos['ARTNR'][$i_SonstPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $SonstPos['BETRAG'][$i_SonstPos] = $Menge * $Betrag;
                            $i_SonstPos++;
                        }
                    }
                    else
                    {
                        // noch gleicher Vorgang
                        // weitere Pos des Vorgangs Pr�fen und ins entsprechende Array schreiben
                        if($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'GLAS01\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN04\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN01\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN02\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false) == '\'FEIN03\'')
                        {
                            $GlasPos['ARTNR'][$i_GlasPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $GlasPos['BETRAG'][$i_GlasPos] = $Menge * $Betrag;
                            $i_GlasPos++;
                        }
                        else
                        {
                            $SonstPos['ARTNR'][$i_SonstPos] = $this->_DB->FeldInhaltFormat('T', $RsSKZ_E->FeldInhalt('ARTNR'), false);
                            $SonstPos['BETRAG'][$i_SonstPos] = $Menge * $Betrag;
                            $i_SonstPos++;
                        }
                    }
                    $RsSKZ_E->DSWeiter();
                }

                // Vorgang der zuletzt Gelesen wurde auswerten ob OK
                if((count($SonstPos, 1) - 2) / 2 <> 0)
                {
                    // Falsche Pos sind vorhanden, pr�fen ob diese sich
                    // durch Preisnachlass wieder aufheben

                    $Summe = 0;

                    for($x = 0; $x < (count($SonstPos, 1) - 2) / 2; $x++)
                    {
                        $Summe = $Summe + $SonstPos['BETRAG'][$x];
                    }
                    if($Summe == 0)
                    {
                        // Die falschen Positionen heben sich durch
                        // z.B. Preissenkung auf und der Vorgang
                        // ist somit in Ordnung
                        $this->SetzeCheckStatus(100);
                        $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                    }
                    else
                    {
                        // Die falschen Positionen heben sich nicht
                        // durch z.B. Preissenkung auf und der Vorgang
                        // muss daher gesperrt werden
                        $this->SetzeStatus(1);
                    }
                }
                else
                {
                    // Keine Falschen Positionen vorhanden
                    // Status sezten das Pr�fung keine Fehler ergab
                    $this->SetzeCheckStatus(100);
                    $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }

        $this->debugAusgabe('Ende: PruefSKZ_E....', 10);
        $this->SchreibeModulStatus('PruefSKZ_E', 'Pr�fung abgeschlossen');
    }

    public function PruefSKZ_S()
    {
        $this->SchreibeModulStatus('PruefSKZ_S', 'Start: Pr�fe S Vorg�nge ...');
        $this->debugAusgabe('Start: PruefSKZ_S', 10);

        try
        {
            $this->_SQL  ='SELECT *';
            $this->_SQL .='   FROM VERSVORGANGSSTATUS VVS';
            $this->_SQL .='   INNER JOIN VERSVERSICHERUNGEN VVE';
            $this->_SQL .='   ON VVS.VVS_VERSNR          = VVE.VVE_VERSNR';
            $this->_SQL .='   WHERE VVS.VVS_SCHADENKZ = \'S\'';
            $this->_SQL .='   AND VVS.VVS_VEN_KEY     = 0';
            $this->_SQL .='   AND VVS.VVS_VCN_KEY     = 0';

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
            {
                $RsSKZ_S = $this->_DB->RecordSetOeffnen($this->_SQL);

                while(!$RsSKZ_S->EOF())
                {
                    $this->SetzeCheckStatus(101, $RsSKZ_S->FeldInhalt('VVS_FILID'), $RsSKZ_S->FeldInhalt('VVS_WANR'));

                    $this->_SQL  ='SELECT *';
                    $this->_SQL .=' FROM VERSSERVICETRANSAKTIONEN vst';
                    $this->_SQL .=' WHERE vst.VST_BRANCHNUMBER ='.$this->_DB->WertSetzen('VVS', 'N0', $RsSKZ_S->FeldInhalt('VVS_FILID'));
                    $this->_SQL .=' AND TO_NUMBER(SUBSTR(vst.VST_MAINSYSTEMORDERNUMBER,7,7),9999999) ='.$this->_DB->WertSetzen('VVS', 'N0', $RsSKZ_S->FeldInhalt('VVS_WANR'));

                    if($this->_DB->ErmittleZeilenAnzahl($this->_SQL, $this->_DB->Bindevariablen('VVS')) >= 1) {
                        $this->updateRegelwerksAnwendung(1, $RsSKZ_S->FeldInhalt('VVS_FILID'),  $RsSKZ_S->FeldInhalt('VVS_WANR'));
                        $this->SchreibeRepInDWH($RsSKZ_S->FeldInhalt('VVS_FILID'), $RsSKZ_S->FeldInhalt('VVS_WANR'));
                        $this->SetzeExportDWHStatus('VVS_SCHADENKZ', $RsSKZ_S->FeldInhalt('VVS_FILID'), $RsSKZ_S->FeldInhalt('VVS_WANR'));
                    }
                    else {
                        $this->updateRegelwerksAnwendung(99, $RsSKZ_S->FeldInhalt('VVS_FILID'),  $RsSKZ_S->FeldInhalt('VVS_WANR'));
                        $this->bearbeiteSteinschlaegeOhneRegelwerk($RsSKZ_S->FeldInhalt('VVE_VERARBWEG'), $RsSKZ_S->FeldInhalt('VVS_FILID'),  $RsSKZ_S->FeldInhalt('VVS_WANR'));
                    }
                    $RsSKZ_S->DSWeiter();
                }
            }
            $this->debugAusgabe('Update Vorgaenge in versvorgangsstatus, mit Status 127 fuer Export in Webservice-Tabelle...', 10);

            $this->_SQL = 'UPDATE versvorgangsstatus vvs';
            $this->_SQL .= ' SET vvs_vcn_key = 127';
            $this->_SQL .= ' WHERE EXISTS';
            $this->_SQL .= '   (SELECT vve.vve_versnr';
            $this->_SQL .= '   FROM versversicherungen vve';
            $this->_SQL .= '   WHERE vve.vve_versnr  = vvs.vvs_versnr';
            $this->_SQL .= '   AND vve.vve_verarbweg = \'SERVICE\'';
            $this->_SQL .= '   )';
            $this->_SQL .= ' AND (vvs_vcn_key = 101';
            $this->_SQL .= ' AND vvs_ven_key = 0)';
            $this->_SQL .= ' OR (vvs_freigabe = 1';
            $this->_SQL .= ' AND vvs_schadenkz = \'S\')';

            $this->_DB->Ausfuehren($this->_SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;
            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG_STEINSCHLAG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->SchreibeModulStatus('PruefSKZ_S', 'Ende: Pr�fe S Vorg�nge ...');
        $this->debugAusgabe('Ende: PruefSKZ_S', 10);
    }

    protected function updateRegelwerksAnwendung($angewandt, $filid, $wanr)
    {
        $this->_SQL  ='UPDATE VERSVORGANGSSTATUS';
        $this->_SQL .=' SET VVS_REG_ANGEWANDT = ' . $angewandt;
        $this->_SQL .=' WHERE VVS_FILID = '.$this->_DB->WertSetzen('VVS', 'N0', $filid);
        $this->_SQL .=' AND VVS_WANR = '.$this->_DB->WertSetzen('VVS', 'N0', $wanr);

        $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->Bindevariablen('VVS'));

    }
    protected function bearbeiteSteinschlaegeOhneRegelwerk($verabweg, $filid, $wanr)
    {
        if($verabweg == 'BGAG') {

            $this->_SQL ='   SELECT *';
            $this->_SQL .='   FROM VERS_FORM_POSDATEN VEP';
            $this->_SQL .='   WHERE VEP.ARTNR          <> \'1EIN69\'';
            $this->_SQL .='   AND VEP.UMSATZ          > 0';
            $this->_SQL .='   AND VEP.FILNR = '.$this->_DB->WertSetzen('VVS', 'N0', $filid);
            $this->_SQL .='   AND VEP.WANR = '.$this->_DB->WertSetzen('VVS', 'N0', $wanr);

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL, $this->_DB->Bindevariablen('VVS')) >= 1)
            {
                //Wenn weitere Positionen enthalten sind, sperre den Vorgang
                $this->SetzeStatus(2, $filid, $wanr);
                $this->SetzeCheckStatus(0, $filid, $wanr);
            }
            else {
                $this->SchreibeRepInDWH($filid, $wanr);
            }
        }
        elseif ($verabweg == 'SERVICE') {

            $this->SchreibeRepInDWH($filid, $wanr);
            $this->_SQL = 'SELECT mvt_betreff, mvt_text ';
            $this->_SQL .= 'FROM mailversandtexte ';
            $this->_SQL .= 'WHERE mvt_bereich = \'VERS_ERR_STEINSCHLAG\'';

            $rsMVT = $this->_DB->RecordSetOeffnen($this->_SQL);

            $suchen = array('#FILID#', '#WANR#');
            $ersetzen = array($filid, $wanr);
            $MailText = str_replace($suchen, $ersetzen, $rsMVT->FeldInhalt('MVT_TEXT'));

            $this->Werkzeug->EMail($this->_EMAIL_Infos, $rsMVT->FeldInhalt('MVT_BETREFF'), $MailText, 2, '', 'shuttle@de.atu.eu',0, null,null,2);
        }

    }

    public function SetzeSKZ_T()
    {
        $this->SchreibeModulStatus('SetzeSKZ_T', 'Start: SetzeSKZ_T ...');
        $this->debugAusgabe('Start: SetzeSKZ_T', 10);
        try
        {
            // Funktion setzt Vorgaenge die neben der Steinschlagreparatur
            // noch andere (sich nicht aufhebende) Artikel haben auf SKZ T
            $this->_SQL = 'SELECT vvs_key, vvs_filid,vvs_wanr ';
            $this->_SQL .= 'FROM versvorgangsstatus ';
            $this->_SQL .= 'WHERE vvs_ven_key = 2';

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL))
            {
                $rsSetzeT = $this->_DB->RecordSetOeffnen($this->_SQL);

                $this->debugAusgabe('Update Vorgaenge mit vvs_ven_key = 2, auf das SKZ T', 10);

                while(!$rsSetzeT->EOF())
                {
                    $this->_SQL = 'UPDATE versvorgangsstatus ';
                    $this->_SQL .= 'SET vvs_schadenkz = \'T\', ';
                    $this->_SQL .= 'vvs_ven_key = 0 ';
                    $this->_SQL .= 'WHERE vvs_key =' . $this->_DB->WertSetzen('VVS', 'N0', $rsSetzeT->FeldInhalt('VVS_KEY'));

                    $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->Bindevariablen('VVS', true));

                    $this->SetzeExportDWHStatus('VVS_SCHADENKZ', $rsSetzeT->FeldInhalt('VVS_FILID'), $rsSetzeT->FeldInhalt('VVS_WANR'));

                    $rsSetzeT->DSWeiter();
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->debugAusgabe('Ende: SetzeSKZ_T', 10);
        $this->SchreibeModulStatus('SetzeSKZ_T', 'Ende: SetzeSKZ_T');
    }

    public function PruefSKZ_T()
    {
        $this->SchreibeModulStatus('PruefSKZ_T', 'Start: PruefSKZ_T ...');
        $this->debugAusgabe('Start: PruefSKZ_T', 10);

        try
        {
            // Pruefen bei welchen Status noch selektiert werden muss
            $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vp.artkz, vp.bez, ';
            $this->_SQL .= 'vp.menge, vp.vk_preis, vp.umsatz, vp.rabattkenn ';
            $this->_SQL .= 'FROM versvorgangsstatus vs INNER JOIN v_vers_form_posdaten vp ';
            $this->_SQL .= 'ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
            $this->_SQL .= 'WHERE vs.vvs_ven_key = 0 ';
            $this->_SQL .= 'AND vs.vvs_vcn_key = 0 ';
            $this->_SQL .= 'AND vs.vvs_schadenkz = \'T\' ';
            $this->_SQL .= 'ORDER BY vp.filnr, vp.wanr, vp.position';

            // Pruefen ob Vorgaenge zum pruefen vorhanden sind

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
            {
                $RsSKZ_T = $this->_DB->RecordSetOeffnen($this->_SQL);

                $this->debugAusgabe('Pruefe die SKZ T-Vorgaenge und setzt Status = 4, bei falschen Positionen...', 10);
                $this->_FilNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_T->FeldInhalt('FILNR'), false);
                $this->_WaNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_T->FeldInhalt('WANR'), false);

                //Flag das Anzeigt ob im T-Fall falsche Pos vorhanden sind auf false setzen
                $FlagFalschPos = false;

                while(!$RsSKZ_T->EOF())
                {
                    // Werte in Variablen sichern
                    $this->_FilNrAkt = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_T->FeldInhalt('FILNR'), false);
                    $this->_WaNrAkt = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_T->FeldInhalt('WANR'), false);

                    //*******************************************************************************************
                    //*******************************************************************************************
                    if($this->_FilNr <> $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt || $this->_FilNr <> $this->_FilNrAkt && $this->_WaNr == $this->_WaNrAkt || $this->_FilNr == $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt)
                    {
                        // neuer Vorgang hat begonnen!!!

                        if($FlagFalschPos === false)
                        {
                            // Es gibt keine falschen Positionen im T-Fall
                            $this->SetzeCheckStatus(102);
                            $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                        }
                        else
                        {
                            // Es gibt falsche Positionen im T-Fall (GLAS01/1EIN69)
                            $this->SetzeStatus(4);
                        }
                        // Zaehlervariablen zuruecksetzen + Arrays leeren
                        $FlagFalschPos = false;

                        // Filnr und Wanr wieder sichern
                        $this->_FilNr = $this->_FilNrAkt;
                        $this->_WaNr = $this->_WaNrAkt;

                        // Erste Pos des Vorgangs Pruefen
                        if($this->_DB->FeldInhaltFormat('T', $RsSKZ_T->FeldInhalt('ARTNR'), false) == '\'1EIN69\'')
                        {
                            $FlagFalschPos = true;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_T->FeldInhalt('ARTNR'), false) == '\'GLAS01\'')
                        {
                            $FlagFalschPos = true;
                        }
                    }
                    else
                    {
                        // noch gleicher Vorgang
                        // weitere Pos des Vorgangs Pruefen

                        if($this->_DB->FeldInhaltFormat('T', $RsSKZ_T->FeldInhalt('ARTNR'), false) == '\'1EIN69\'')
                        {
                            $FlagFalschPos = true;
                        }
                        elseif($this->_DB->FeldInhaltFormat('T', $RsSKZ_T->FeldInhalt('ARTNR'), false) == '\'GLAS01\'')
                        {
                            $FlagFalschPos = true;
                        }
                    }

                    $RsSKZ_T->DSWeiter();
                }

                // Vorgang der zuletzt Gelesen wurde auswerten ob OK
                if($FlagFalschPos === false)
                {
                    // Es gibt keine falschen Positionen im T-Fall
                    $this->SetzeCheckStatus(102);
                    $this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                }
                else
                {
                    // Es gibt falsche Positionen im T-Fall (GLAS01/1EIN69)
                    $this->SetzeStatus(4);
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->SchreibeModulStatus('PruefSKZ_T', 'Ende: PruefSKZ_T ...');
        $this->debugAusgabe('Ende: PruefSKZ_T', 10);
    }

    public function SetzeSKZ_P()
    {
        $this->SchreibeModulStatus('SetzeSKZ_P', 'Setze SKZ P ...');
        $this->debugAusgabe('Start: SetzeSKZ_P', 10);

        try
        {

            $this->_SQL = 'SELECT vp.filnr, vp.wanr, vp.position, vp.artnr, vs.vvs_versnr, vve_kuerzel, vs.VVS_SCHADENKZ';
            $this->_SQL .= '          FROM versvorgangsstatus vs ';
            $this->_SQL .= '          INNER JOIN v_vers_form_posdaten vp ';
            $this->_SQL .= '          ON vs.vvs_filid = vp.filnr AND vs.vvs_wanr = vp.wanr ';
            $this->_SQL .= '          INNER JOIN v_vers_exp_kopf vek ON vek.filid = vs.vvs_filid';
            $this->_SQL .= '          AND vek.wanr = vs.vvs_wanr';
            $this->_SQL .= '          INNER JOIN versversicherungen vve ON vve.vve_versnr = vs.vvs_versnr ';
            $this->_SQL .= '          INNER JOIN verskonditionen vkd ON vve.vve_versnr = vkd.VKD_VVE_VERSNR';
            $this->_SQL .= '          and vek.selbst_betrag = vkd.VKD_SB';
            $this->_SQL .= '          WHERE vs.vvs_vcn_key = 102 ';
            $this->_SQL .= '          AND vs.vvs_schadenkz = \'T\'';
            $this->_SQL .= '          AND vve.vve_skz_p_versand =  1';
            $this->_SQL .= '          ORDER BY vp.filnr, vp.wanr, vp.position';

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
            {
                $RsSKZ_P = $this->_DB->RecordSetOeffnen($this->_SQL);

                $this->debugAusgabe('Pruefe ob Vorgang ein Parkschaden ist...', 10);

                $this->_FilNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_P->FeldInhalt('FILNR'), false);
                $this->_WaNr = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_P->FeldInhalt('WANR'), false);

                $FlagSetzeP = false;

                while(!$RsSKZ_P->EOF())
                {
                    // Werte in Variablen sichern
                    $this->_FilNrAkt = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_P->FeldInhalt('FILNR'), false);
                    $this->_WaNrAkt = $this->_DB->FeldInhaltFormat('N0', $RsSKZ_P->FeldInhalt('WANR'), false);

                    //*******************************************************************************************
                    //*******************************************************************************************
                    if($this->_FilNr <> $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt || $this->_FilNr <> $this->_FilNrAkt && $this->_WaNr == $this->_WaNrAkt || $this->_FilNr == $this->_FilNrAkt && $this->_WaNr <> $this->_WaNrAkt)
                    {
                        //*******************************************************************************************
                        //*******************************************************************************************
                        // neuer Vorgang hat begonnen!!!

                        if($FlagSetzeP === true)
                        {
                            $this->SetzeCheckStatus(103);
                        }

                        // Zaehlervariablen zuruecksetzen + Arrays leeren
                        $FlagSetzeP = false;

                        // Filnr und Wanr wieder sichern
                        $this->_FilNr = $this->_FilNrAkt;
                        $this->_WaNr = $this->_WaNrAkt;

                        // Erste Pos des Vorgangs Pruefen

                        if(substr($this->_DB->FeldInhaltFormat('T', $RsSKZ_P->FeldInhalt('ARTNR'), false), 1, 3) == '1ES')
                        {
                            $FlagSetzeP = true;
                        }
                    }
                    else
                    {
                        // noch gleicher Vorgang
                        // weitere Pos des Vorgangs Pruefen

                        if(substr($this->_DB->FeldInhaltFormat('T', $RsSKZ_P->FeldInhalt('ARTNR'), false), 1, 3) == '1ES')
                        {
                            $FlagSetzeP = true;
                        }
                    }
                    $RsSKZ_P->DSWeiter();
                }

                if($FlagSetzeP === true)
                {
                    $this->SetzeCheckStatus(103);
                    //$this->SetzeExportDWHStatus('VVS_SCHADENKZ');
                }
            }

            $this->debugAusgabe('Update Vorgaenge in versvorgangsstatus auf SKZ = P und Status 104....', 10);

            $this->_SQL = 'UPDATE versvorgangsstatus ';
            $this->_SQL .= 'SET vvs_schadenkz = \'P\' ,vvs_vcn_key = 104, vvs_export_dwh = 0 ';
            $this->_SQL .= 'WHERE (vvs_vcn_key = 103 OR (vvs_schadenkz = \'P\' AND vvs_vcn_key = 0 AND vvs_ven_key = 0))';

            $this->_DB->Ausfuehren($this->_SQL, '', false);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->debugAusgabe('Ende: SetzeSKZ_P', 10);
        $this->SchreibeModulStatus('SetzeSKZ_P', 'Ende: SetzeSKZ_P');
    }

    public function BearbSKZ_T()
    {
        $this->SchreibeModulStatus('BearbSKZ_T', 'Start: BearbSKZ_T..');
        $this->debugAusgabe('Start: BearbSKZ_T', 10);

        try
        {
            $this->debugAusgabe('Update T-Vorgaenge in versvorgangsstatus, mit Status 127 fuer Export in Webservice-Tabelle (nur DEVK)...', 10);

            $this->_SQL = 'update versvorgangsstatus vvs';
            $this->_SQL .= ' set vvs_vcn_key = 127';
            $this->_SQL .= ' where exists (select * from versversicherungen vve';
            $this->_SQL .= ' where vve.vve_versnr = vvs.vvs_versnr';
            $this->_SQL .= ' and vve.vve_kuerzel = \'DEVK\')';
            $this->_SQL .= ' AND vvs_vcn_key = 102';
            $this->_SQL .= ' AND vvs_ven_key = 0';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            $this->debugAusgabe('Update restliche T-Vorgaenge in versvorgangsstatus, mit Fehlerstatus 14 (kein Export)...');

            $this->_SQL = 'UPDATE versvorgangsstatus ';
            $this->_SQL .= 'SET vvs_vcn_key = 113 ';
            $this->_SQL .= 'WHERE vvs_schadenkz = \'T\' ';
            $this->_SQL .= 'AND vvs_vcn_key = 102 ';

            $this->_DB->Ausfuehren($this->_SQL);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->debugAusgabe('Ende: BearbSKZ_T', 10);
        $this->SchreibeModulStatus('BearbSKZ_T', 'Sperrung Abgeschlossen');
    }

    public function BearbSKZ_P()
    {
        //falsches SKZ P sperren (Zusaetzl. Positionen)
        $this->SchreibeModulStatus('BearbSKZ_P', 'Start: BearbSKZ_P...');
        $this->debugAusgabe('Start: BearbSKZ_P..', 10);

        try
        {

            $this->debugAusgabe('Update P-Vorgaenge mit Fehlerstatus 17, die falsche Positionen enthalten,  ....', 10);

            $this->_SQL = 'UPDATE versvorgangsstatus vs ';
            $this->_SQL .= ' SET vs.vvs_ven_key = 17';
            $this->_SQL .= ' WHERE EXISTS';
            $this->_SQL .= ' (SELECT * FROM v_vers_form_posdaten vp';
            $this->_SQL .= '  WHERE vs.vvs_filid = vp.filnr';
            $this->_SQL .= '  AND vs.vvs_wanr = vp.wanr ';
            $this->_SQL .= '  AND vs.vvs_vcn_key = 104 ';
            $this->_SQL .= '  AND vs.vvs_schadenkz = \'P\' ';
            $this->_SQL .= '  AND vp.artnr NOT LIKE \'1ES%\' ';
            $this->_SQL .= '  AND vp.artnr <> \'HUB900\' ';
            $this->_SQL .= '  AND vp.umsatz <> 0)';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            //Selektiert sich aus der Konditionstabelle die Betrag fuer die Hochstgrenze fuer Parkschaeden
            //Wenn Gesamtsumme ohne HUB900 Hoechstgrenze �berschreitet -->Vorgang sperren;
            $this->debugAusgabe('Update P-Vorgaenge mit Fehlerstatus 18, bei Ges.-Betrag (ohne HUB900) > Hoechstgrenze....', 10);

            $this->_SQL = 'UPDATE versvorgangsstatus';
            $this->_SQL .= ' SET vvs_ven_key = 18';
            $this->_SQL .= ' WHERE vvs_key  IN (';
            $this->_SQL .= ' SELECT vvs_key';
            $this->_SQL .= ' FROM';
            $this->_SQL .= '   (SELECT vp.filnr,';
            $this->_SQL .= '     vp.wanr,';
            $this->_SQL .= '     vs.vvs_versnr,';
            $this->_SQL .= '     SUM(vp.umsatz) AS ges_umsatz,';
            $this->_SQL .= '     vkd.vkd_hoechstgrenze,';
            $this->_SQL .= '     vs.vvs_key';
            $this->_SQL .= '   FROM versvorgangsstatus vs';
            $this->_SQL .= '   INNER JOIN v_vers_form_posdaten vp';
            $this->_SQL .= '   ON vs.vvs_filid = vp.filnr';
            $this->_SQL .= '   AND vs.vvs_wanr = vp.wanr';
            $this->_SQL .= '   INNER JOIN verskonditionen vkd';
            $this->_SQL .= '   ON vkd.vkd_vve_versnr = vs.vvs_versnr';
            $this->_SQL .= '   AND vkd.vkd_gueltig_ab <= sysdate ';
            $this->_SQL .= '   AND vkd.vkd_gueltig_bis >= sysdate ';
            $this->_SQL .= '   WHERE vs.vvs_vcn_key  = 104';
            $this->_SQL .= '   AND vs.vvs_ven_key    = 0';
            $this->_SQL .= '   AND vs.vvs_schadenkz  = \'P\'';
            $this->_SQL .= '   AND vp.artnr         <> \'HUB900\'';
            $this->_SQL .= '   AND vkd_artnr         = \'1ESXXX\'';
            $this->_SQL .= '   GROUP BY vp.filnr,';
            $this->_SQL .= '     vp.wanr,';
            $this->_SQL .= '     vs.vvs_versnr,';
            $this->_SQL .= '     vkd.vkd_preis_brutto,';
            $this->_SQL .= '     vs.vvs_key,';
            $this->_SQL .= '     vkd.VKD_HOECHSTGRENZE';
            $this->_SQL .= '   )';
            $this->_SQL .= ' WHERE ges_umsatz > vkd_hoechstgrenze';
            $this->_SQL .= '   )';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            //Selektiert sich den Brutto-Preis f�r Parkschaeden aus der Konditionstabelle
            //und berechnet: Ausbuchungsbetrag = Gesamtumsatz (aus Kasse) - Brutto-Preis (Konditionstabelle)
            //sollte der Ausbuchungsbetrag die festgelegte max bzw min-Grenze ueberschreiten (Konditionstabelle)
            //wird der Vorgang gesperrt

            $this->debugAusgabe('Update P-Vorgaenge mit Fehlerstatus 19, bei Ueberschreitung max/min Ausbuchungsbetrag....', 10);

            $this->_SQL = 'UPDATE versvorgangsstatus';
            $this->_SQL .= ' SET vvs_ven_key = 19';
            $this->_SQL .= ' WHERE vvs_key  IN (';
            $this->_SQL .= 'SELECT vvs_key';
            $this->_SQL .= ' FROM';
            $this->_SQL .= '   (SELECT versnr,';
            $this->_SQL .= '     vvs.vvs_key,';
            $this->_SQL .= '     vvs.vvs_ven_key,';
            $this->_SQL .= '     vvs.vvs_vcn_key,';
            $this->_SQL .= '     vvs.vvs_schadenkz,';
            $this->_SQL .= '     vek.filid,';
            $this->_SQL .= '     vek.wanr,';
            $this->_SQL .= '     vek.umsatz_gesamt                AS UMSATZ_BRUTTO,';
            $this->_SQL .= '     (vek.umsatz_gesamt-preis_brutto) AS AUSBUCHUNGSBETRAG,';
            $this->_SQL .= '     vkd.vkd_max_ausbuchung,';
            $this->_SQL .= '     vkd.vkd_min_ausbuchung';
            $this->_SQL .= '   FROM';
            $this->_SQL .= '     (SELECT SUM(vkd.vkd_preis_brutto) AS preis_brutto,';
            $this->_SQL .= '       vkd.vkd_artnr,';
            $this->_SQL .= '       vkd.VKD_VVE_VERSNR AS versnr';
            $this->_SQL .= '     FROM VERSKONDITIONEN vkd';
            $this->_SQL .= '     WHERE vkd.vkd_artnr = \'1ESXXX\'';
            $this->_SQL .= '     AND trunc(vkd.vkd_gueltig_ab) <= trunc(sysdate)';
            $this->_SQL .= '     AND trunc(vkd.vkd_gueltig_ab) >= trunc(sysdate)';
            $this->_SQL .= '     GROUP BY vkd.vkd_artnr,';
            $this->_SQL .= '       vkd.vkd_vve_versnr';
            $this->_SQL .= '     )';
            $this->_SQL .= '   INNER JOIN versvorgangsstatus vvs';
            $this->_SQL .= '   ON vvs.vvs_versnr = versnr';
            $this->_SQL .= '   INNER JOIN v_vers_exp_kopf vek';
            $this->_SQL .= '   ON vek.filid = vvs.vvs_filid';
            $this->_SQL .= '   AND vek.wanr = vvs.vvs_wanr';
            $this->_SQL .= '   INNER JOIN VERSKONDITIONEN vkd';
            $this->_SQL .= '   ON vkd.VKD_VVE_VERSNR = vvs.vvs_versnr';
            $this->_SQL .= '   AND vkd.vkd_gueltig_ab <= sysdate ';
            $this->_SQL .= '   AND vkd.vkd_gueltig_bis >= sysdate ';
            $this->_SQL .= '   WHERE vvs_schadenkz   = \'P\'';
            $this->_SQL .= '   AND vkd.vkd_artnr = \'1ESXXX\'';
            $this->_SQL .= '   )';
            $this->_SQL .= ' WHERE vvs_schadenkz     = \'P\'';
            $this->_SQL .= ' AND vvs_ven_key         = 0';
            $this->_SQL .= ' AND vvs_vcn_key         = 104';
            $this->_SQL .= ' AND ( AUSBUCHUNGSBETRAG > vkd_max_ausbuchung';
            $this->_SQL .= ' OR AUSBUCHUNGSBETRAG    < vkd_min_ausbuchung)';
            $this->_SQL .= '   )';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            $this->SchreibeModulStatus('BearbSKZ_P', 'Setze Status zum Export in Webservice-Tabelle');

            $this->debugAusgabe('Update P-Vorgaenge in versvorgangsstatus, Setze Status 127 fuer Export in Webservice-Tabelle....', 10);

            $this->_SQL = 'update versvorgangsstatus ';
            $this->_SQL .= ' set vvs_vcn_key = 127';
            $this->_SQL .= ' where vvs_vcn_key = 104';
            $this->_SQL .= ' and vvs_ven_key = 0';

            $this->_DB->Ausfuehren($this->_SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }

        $this->SchreibeModulStatus('BearbSKZ_P', 'Ende: BearbSKZ_P...');
        $this->debugAusgabe('Ende: BearbSKZ_P..', 10);
    }

    public function BearbSKZ_E()
    {

        $this->SchreibeModulStatus('BearbSKZ_E', 'Start: BearbSKZ_E...');
        $this->debugAusgabe('Start: BearbSKZ_E...', 10);

        try
        {

            //19.10.2010 OP: In WHERE Klausel (bei vvs_vcn_key not in) in Klammer "120" ergaenzt = Status fuer Umgebucht
            $this->_SQL  ='SELECT vek.VORST_BETRAG,';
            $this->_SQL .=' vek.SELBST_BETRAG,';
            $this->_SQL .=' vvs.VVS_VERSNR,';
            $this->_SQL .=' vvs.vvs_vorgangnr,';
            $this->_SQL .=' vvs.vvs_filid,';
            $this->_SQL .=' vvs.vvs_wanr,';
            $this->_SQL .=' vvs.vvs_kfzkennz,';
            $this->_SQL .=' vek.umsatz_gesamt ';
            $this->_SQL .=' FROM versvorgangsstatus vvs ';
            $this->_SQL .=' INNER JOIN vers_exp_kopf vek';
            $this->_SQL .=' ON vvs.vvs_filid = vek.filid';
            $this->_SQL .=' AND vvs.vvs_wanr = vek.wanr ';
            $this->_SQL .=' WHERE vvs_ven_key IN (5,6,7,8,10,11,12,13,20,21,22,23,24,25,26,27,28,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45) ';
            $this->_SQL .=' AND vvs_vcn_key NOT IN (102,103,104,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,124,125,126) ';
            $this->_SQL .=' OR vvs_vcn_key = 100';

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
            {
                $RsBearb_E = $this->_DB->RecordSetOeffnen($this->_SQL);

                $this->debugAusgabe('Beginne mit dem Matching zwischen Kassen- und Auftragsdaten (PD)...', 10);

                while(!$RsBearb_E->EOF())
                {
                    $KFZKennzKasse = $this->KennzeichenWandeln($this->KennzeichenWandeln($this->_DB->FeldInhaltFormat('T', $RsBearb_E->FeldInhalt('VVS_KFZKENNZ'), false)));
                    $BetragKasse = $this->_DB->FeldInhaltFormat('N2', $RsBearb_E->FeldInhalt('UMSATZ_GESAMT'), false);
                    $VorgangNr = $RsBearb_E->FeldInhalt('VVS_VORGANGNR');
                    $this->_FilNr = $this->_DB->FeldInhaltFormat('N0', $RsBearb_E->FeldInhalt('VVS_FILID'), false);
                    $this->_WaNr = $this->_DB->FeldInhaltFormat('N0', $RsBearb_E->FeldInhalt('VVS_WANR'), false);

                    $this->_SQL = 'SELECT round(sum(((FGP_ANZAHL * FGP_OEMPREIS)*(1+(fgk.fgk_steuersatz/100)))),2) as Betrag, ';
                    $this->_SQL .= '  fgk.fgk_kfzkennz, fgk.fgk_vorgangnr, fgk.fgk_zeitstempel, fgk.fgk_sb, fgk.FGK_MWST, fgk.FGK_VERSNR ';
                    $this->_SQL .= '  FROM fgpositionsdaten fgp INNER JOIN fgkopfdaten fgk ';
                    $this->_SQL .= '  ON fgp.fgp_vorgangnr = fgk.fgk_vorgangnr ';
                    //$this->_SQL .='  AND fgp.fgp_zeitstempel = fgk.fgk_zeitstempel ';
                    $this->_SQL .= '  WHERE fgp.FGP_VORGANGNR = ' . $this->_DB->WertSetzen('VVS', 'T', $RsBearb_E->FeldInhalt('VVS_VORGANGNR'));
                    $this->_SQL .= '  GROUP BY fgk.fgk_kfzkennz, fgk.fgk_vorgangnr, fgk_zeitstempel, fgk.fgk_sb, fgk.FGK_VERSNR,fgk.FGK_MWST';

                    $rsFGKOPF = $this->_DB->RecordSetOeffnen($this->_SQL, $this->_DB->Bindevariablen('VVS', true));

                    if($rsFGKOPF->AnzahlDatensaetze() == 0)
                    {
                        // Vorgang in den Vorgangstabellen nicht gefunden
                        // Vorgang auf Fehler setzen
                        $this->SetzeStatus(5);
                    }
                    else
                    {
                        // pruefen ob die Abwicklung zwischen Autoglas- & Kassendaten die gleiche ist
                        // z.B. Autoglas = Selbstzahler & Kasse = Versicherung
                        //		Autoglas = Versnr. 81 & Kasse = Versnr = 89

                        if($rsFGKOPF->FeldInhalt('FGK_VERSNR') != $RsBearb_E->FeldInhalt('VVS_VERSNR'))
                        {
                            // Fehler --> Abwicklung stimmmt zwischen Autoglas und Kasse nicht ueberein
                            $this->SetzeStatus(20);
                        }
                        else
                        {

                            $PruefVers = $this->_DB->FeldInhaltFormat('N0', $rsFGKOPF->FeldInhalt('FGK_VERSNR'), false);
                            $KFZKennzFG = $this->_DB->FeldInhaltFormat('T', $rsFGKOPF->FeldInhalt('FGK_KFZKENNZ'), false);
                            $BetragFG = $this->_DB->FeldInhaltFormat('N2', $rsFGKOPF->FeldInhalt('BETRAG'), false);
                            $Zeitstempel = $this->_DB->FeldInhaltFormat('T', $rsFGKOPF->FeldInhalt('FGK_ZEITSTEMPEL'), false);
                            $SB_AUTOGLAS = $this->_DB->FeldInhaltFormat('N0', $rsFGKOPF->FeldInhalt('FGK_SB'), false);
                            $SB_KASSE = $this->_DB->FeldInhaltFormat('N0', $RsBearb_E->FeldInhalt('SELBST_BETRAG'), false);
                            $MWST_AUTOGLAS = $this->_DB->FeldInhaltFormat('N2', $rsFGKOPF->FeldInhalt('FGK_MWST'), false);
                            $MWST_KASSE = $this->_DB->FeldInhaltFormat('N2', $RsBearb_E->FeldInhalt('VORST_BETRAG'), false);

                            $FlagKFZKennz = false;
                            $FlagBetrag = false;
                            $FlagSB = false;
                            $FlagMWST = false;

                            $BetragVergleich = $BetragFG - $BetragKasse;

                            if($KFZKennzFG == $KFZKennzKasse)
                            {
                                $FlagKFZKennz = true;
                            }

                            // Differenzgrenze auf Wunsch von Hr. Koller abgeschafft (siehe A-012716)
                            // Betr�ge m�ssen jetzt auf den Cent genau passen OP 30.07.2014
                            //if($BetragVergleich >= -10 && $BetragVergleich <= 10)
                            //{
                            //	$FlagBetrag = true;
                            //}

                            //17.03.2015: A-021160 Differenzgrenze soll laut Anforderung nun
                            //auf 2 Cent wieder eingef�hrt werden (Rundungsdifferenzen)

                            if($BetragVergleich >= -0.02 && $BetragVergleich <= 0.02)
                            {
                                $FlagBetrag = true;
                            }

                            if($BetragVergleich == 0)
                            {
                                $FlagBetrag = true;
                            }

                            if($SB_AUTOGLAS == $SB_KASSE)
                            {
                                //vergleicht die SB zwischen Autoglas und Kasse
                                $FlagSB = true;
                            }
                            else
                            {
                                //DEVK-Weiche --> Uebergangsloesung
                                if($PruefVers == 45)
                                {
                                    $FlagSB = true;
                                }
                                else
                                {
                                    $FlagSB = false;
                                }
                            }

                            $this->_SQL = 'select *';
                            $this->_SQL .= ' from fgkopfdaten';
                            $this->_SQL .= ' where fgk_vorgangnr = ' . $this->_DB->WertSetzen('VVS', 'T', $RsBearb_E->FeldInhalt('VVS_VORGANGNR'));
                            $this->_SQL .= ' and fgk_zeitstempel > \'21.08.2015\'';

                            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL, $this->_DB->Bindevariablen('VVS', true)) >= 1)
                            {
                                if($MWST_AUTOGLAS == $MWST_KASSE)
                                {
                                    //vergleicht die MWST zwischen Autoglas und Kasse
                                    $FlagMWST = true;
                                }
                            }
                            else
                            {
                                $FlagMWST = true;
                            }

                            if($FlagBetrag == true && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == true)
                            {
                                // Keine Fehler gefunden
                                $this->SetzeCheckStatus(105);
                                $this->SetzeStatus(0);

                                $this->_SQL = 'UPDATE versvorgangsstatus ';
                                $this->_SQL .= 'SET vvs_datumfg = ' . $Zeitstempel . ' ';
                                $this->_SQL .= ' WHERE vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $this->_FilNr);
                                $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $this->_WaNr);

                                $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->Bindevariablen('VVS', true));
                            }
                            elseif($FlagBetrag == false && $FlagKFZKennz == false && $FlagSB == true && $FlagMWST == true)
                            {
                                //FALSCH	Betrag
                                //FALSCH	KFZ-Kennzeichen
                                //OK		SB
                                //OK		MWST

                                $this->SetzeStatus(8);
                            }
                            elseif($FlagBetrag == true && $FlagSB == true && $FlagKFZKennz == false && $FlagMWST == true)
                            {
                                //OK		Betrag
                                //FALSCH	KFZ-Kennzeichen
                                //OK		SB
                                //OK		MWST

                                $this->SetzeStatus(7);
                            }
                            elseif($FlagBetrag == false && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == true)
                            {
                                //FALSCH	Betrag
                                //OK		KFZ-Kennzeichen
                                //OK		SB
                                //OK		MWST

                                $this->SetzeStatus(6);
                            }
                            elseif($FlagSB == false && $FlagBetrag == true && $FlagKFZKennz == true && $FlagMWST == true)
                            {
                                //OK		Betrag
                                //OK		KFZ-Kennzeichen
                                //FALSCH	SB
                                //OK		MWST

                                $this->SetzeStatus(21);
                            }
                            elseif($FlagSB == false && $FlagBetrag == false && $FlagKFZKennz == true && $FlagMWST == true)
                            {
                                //FALSCH	Betrag
                                //OK		KFZ-Kennzeichen
                                //FALSCH	SB
                                //OK		MWST

                                $this->SetzeStatus(22);
                            }
                            elseif($FlagSB == false && $FlagBetrag == true && $FlagKFZKennz == false && $FlagMWST == true)
                            {
                                //OK		Betrag
                                //FALSCH	KFZ-Kennzeichen
                                //FALSCH	SB
                                //OK		MWST

                                $this->SetzeStatus(23);
                            }
                            elseif($FlagBetrag == false && $FlagKFZKennz == false && $FlagSB == false && $FlagMWST == true)
                            {
                                //FALSCH	Betrag
                                //FALSCH	KFZ-Kennzeichen
                                //FALSCH	SB
                                //OK		MWST

                                $this->SetzeStatus(24);
                            }
                            elseif($FlagBetrag == false && $FlagKFZKennz == false && $FlagSB == true && $FlagMWST == false)
                            {
                                //FALSCH	Betrag
                                //FALSCH	KFZ-Kennzeichen
                                //OK		SB
                                //FALSCH	MWST

                                $this->SetzeStatus(36);
                            }
                            elseif($FlagBetrag == false && $FlagKFZKennz == true && $FlagSB == false && $FlagMWST == false)
                            {
                                //FALSCH	Betrag
                                //OK	KFZ-Kennzeichen
                                //FALSCH	SB
                                //FALSCH	MWST

                                $this->SetzeStatus(34);
                            }
                            elseif($FlagBetrag == true && $FlagKFZKennz == false && $FlagSB == false && $FlagMWST == false)
                            {
                                //OK		Betrag
                                //FALSCH	KFZ-Kennzeichen
                                //FALSCH	SB
                                //FALSCH	MWST

                                $this->SetzeStatus(35);
                            }
                            elseif($FlagBetrag == false && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == false)
                            {
                                //FALSCH	Betrag
                                //OK		KFZ-Kennzeichen
                                //OK		SB
                                //FALSCH	MWST

                                $this->SetzeStatus(32);
                            }
                            elseif($FlagBetrag == true && $FlagKFZKennz == false && $FlagSB == true && $FlagMWST == false)
                            {
                                //OK	Betrag
                                //FALSCH		KFZ-Kennzeichen
                                //OK		SB
                                //FALSCH	MWST

                                $this->SetzeStatus(33);
                            }
                            elseif($FlagBetrag == true && $FlagKFZKennz == true && $FlagSB == false && $FlagMWST == false)
                            {
                                //OK		Betrag
                                //OK		KFZ-Kennzeichen
                                //FALSCH	SB
                                //FALSCH	MWST

                                $this->SetzeStatus(31);
                            }
                            elseif($FlagBetrag == true && $FlagKFZKennz == true && $FlagSB == true && $FlagMWST == false)
                            {
                                //OK		Betrag
                                //OK		KFZ-Kennzeichen
                                //OK		SB
                                //FALSCH	MWST

                                $this->SetzeStatus(30);
                            }
                            elseif($FlagSB == false && $FlagBetrag == false && $FlagKFZKennz == false && $FlagMWST == false)
                            {
                                //FALSCH	Betrag
                                //FALSCH	KFZ-Kennzeichen
                                //FALSCH	SB
                                //FALSCH	MWST

                                $this->SetzeStatus(37);
                            }
                        }
                    }

                    $RsBearb_E->DSWeiter();
                }
            }
            $this->debugAusgabe('Matching abgeschlossen...', 10);

            $this->debugAusgabe('Update E-Vorgaenge in versvorgangsstatus, Setze Status 127 fuer Export in Webservice-Tabelle....', 10);

            $this->_SQL = 'UPDATE versvorgangsstatus vvs';
            $this->_SQL .= ' SET vvs_vcn_key = 127';
            $this->_SQL .= ' WHERE EXISTS';
            $this->_SQL .= '   (SELECT vve.vve_versnr';
            $this->_SQL .= '   FROM versversicherungen vve';
            $this->_SQL .= '   WHERE vve.vve_versnr  = vvs.vvs_versnr';
            $this->_SQL .= '   AND vve.vve_verarbweg = \'SERVICE\'';
            $this->_SQL .= '   )';
            $this->_SQL .= ' AND (vvs_vcn_key = 105';
            $this->_SQL .= ' AND vvs_ven_key = 0)';
            $this->_SQL .= ' OR (vvs_freigabe = 1';
            $this->_SQL .= ' AND vvs_schadenkz = \'E\')';

            $this->_DB->Ausfuehren($this->_SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->SchreibeModulStatus('BearbSKZ_E', 'Start: BearbSKZ_E...');
        $this->debugAusgabe('Ende: BearbSKZ_E...', 10);
    }

    public function SetzteStatusUmgebucht()
    {
        $this->SchreibeModulStatus('SetzteStatusUmgebucht', 'Start: SetzteStatusUmgebucht');
        $this->debugAusgabe('Start: SetzteStatusUmgebucht...', 10);

        try
        {
            $this->debugAusgabe('Update Vorgaenge mit Status 120 -> fuer umgebucht (Menge -1)...', 10);

            $this->_SQL = 'update versvorgangsstatus vvs';
            $this->_SQL .= ' set vvs.vvs_vcn_key = 120';
            $this->_SQL .= ' where vvs.vvs_key in (';
            $this->_SQL .= ' SELECT vvs.vvs_key';
            $this->_SQL .= ' FROM versvorgangsstatus vvs';
            $this->_SQL .= ' INNER JOIN fkassendaten fka';
            $this->_SQL .= ' ON vvs.vvs_vorgangnr     = fka.fka_aemnr';
            $this->_SQL .= ' AND vvs.vvs_filid        = fka.fka_filid';
            $this->_SQL .= ' AND vvs.vvs_wanr         = fka.fka_wanr';
            $this->_SQL .= ' WHERE fka.fka_menge      = -1';
            $this->_SQL .= ' AND vvs.vvs_vcn_key NOT IN (110, 111, 112, 113, 114, 115, 116, 117, 118, 119,120,124,125,126,128,129))';

            $this->_DB->Ausfuehren($this->_SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->SchreibeModulStatus('SetzteStatusUmgebucht', 'Ende: SetzteStatusUmgebucht');
        $this->debugAusgabe('Ende: SetzteStatusUmgebucht...', 10);
    }

    public function BearbKennzeichen()
    {
        $this->SchreibeModulStatus('BearbKennzeichen', 'Start: BearbKennzeichen...');
        $this->debugAusgabe('Start: BearbKennzeichen...', 10);

        try
        {
            $this->_SQL = 'SELECT vvs_filid, vvs_wanr, vvs_kfzkennz, vvs_ven_key, vvs_vcn_key ';
            $this->_SQL .= ' FROM versvorgangsstatus ';
            $this->_SQL .= ' WHERE vvs_ven_key in (5,6,7,8,21,22,23,24,30,31,32,33,34,35,36,37) ';
            $this->_SQL .= ' OR vvs_vcn_key in (101,105,127)';

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) >= 1)
            {
                $RsKennzeichen = $this->_DB->RecordSetOeffnen($this->_SQL);

                while(!$RsKennzeichen->EOF())
                {
                    $Kennzeichen = $RsKennzeichen->FeldInhalt('VVS_KFZKENNZ');
                    $FILNR = $RsKennzeichen->FeldInhalt('VVS_FILID');
                    $WANR = $RsKennzeichen->FeldInhalt('VVS_WANR');

                    $FormKennzeichen = $this->FormatiereKennzeichen($Kennzeichen, $FILNR, $WANR);

                    $Status = $RsKennzeichen->FeldInhalt('VVS_VEN_KEY');
                    $CheckStatus = $RsKennzeichen->FeldInhalt('VVS_VCN_KEY');

                    if($FormKennzeichen === false)
                    {
                        //Kennzeichen nicht wandelbar
                        if($Status == 5)
                        {
                            $this->SetzeStatus(13, $FILNR, $WANR);
                        }
                        elseif($Status == 6)
                        {
                            $this->SetzeStatus(10, $FILNR, $WANR);
                        }
                        elseif($Status == 7)
                        {
                            $this->SetzeStatus(11, $FILNR, $WANR);
                        }
                        elseif($Status == 8)
                        {
                            $this->SetzeStatus(12, $FILNR, $WANR);
                        }
                        elseif($Status == 21)
                        {
                            $this->SetzeStatus(25, $FILNR, $WANR);
                        }
                        elseif($Status == 22)
                        {
                            $this->SetzeStatus(26, $FILNR, $WANR);
                        }
                        elseif($Status == 23)
                        {
                            $this->SetzeStatus(27, $FILNR, $WANR);
                        }
                        elseif($Status == 24)
                        {
                            $this->SetzeStatus(28, $FILNR, $WANR);
                        }
                        elseif($Status == 30)
                        {
                            $this->SetzeStatus(38, $FILNR, $WANR);
                        }
                        elseif($Status == 31)
                        {
                            $this->SetzeStatus(39, $FILNR, $WANR);
                        }
                        elseif($Status == 32)
                        {
                            $this->SetzeStatus(40, $FILNR, $WANR);
                        }
                        elseif($Status == 33)
                        {
                            $this->SetzeStatus(41, $FILNR, $WANR);
                        }
                        elseif($Status == 34)
                        {
                            $this->SetzeStatus(42, $FILNR, $WANR);
                        }
                        elseif($Status == 35)
                        {
                            $this->SetzeStatus(43, $FILNR, $WANR);
                        }
                        elseif($Status == 36)
                        {
                            $this->SetzeStatus(44, $FILNR, $WANR);
                        }
                        elseif($Status == 37)
                        {
                            $this->SetzeStatus(45, $FILNR, $WANR);
                        }
                        else
                        {
                            $this->SetzeStatus(9, $FILNR, $WANR);
                        }
                    }
                    else
                    {
                        //if($CheckStatus == 101 || $CheckStatus == 102 || $CheckStatus == 105)
                        if($CheckStatus != 0)
                        {
                            // Bei diesen Vorg�ngen waren die Pr�fungen bisher in Ordnung
                            //$this->SetzeCheckStatus(106, $FILNR, $WANR);

                            $this->SetzeExportDWHStatus('VVS_KFZKENNZ', $FILNR, $WANR);

                            $this->_SQL = 'UPDATE versvorgangsstatus ';
                            $this->_SQL .= ' SET VVS_KFZKENNZ = ' . $this->_DB->WertSetzen('VVS', 'T', $FormKennzeichen);
                            $this->_SQL .= ',VVS_LAENDERSCHL = \'D\' ';
                            $this->_SQL .= ' WHERE vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $FILNR);
                            $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $WANR);

                            $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->BindeVariablen('VVS', true));

                            $this->_SQL = 'UPDATE versvorgangsstatus';
                            $this->_SQL .= ' SET VVS_VCN_KEY = 106';
                            $this->_SQL .= ' WHERE VVS_VCN_KEY IN (101,105)';
                            $this->_SQL .= ' AND VVS_VEN_KEY = 0';
                            $this->_SQL .= ' AND vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $FILNR);
                            $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $WANR);

                            $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->BindeVariablen('VVS', true));
                        }
                        else
                        {
                            // Bei diesen Vorg�ngen war min. eine Pr�fung fehlerhaft
                            // Status braucht nicht ge�ndert werden da KFZ-Kennz i.O.
                            $this->SetzeExportDWHStatus('VVS_KFZKENNZ', $FILNR, $WANR);

                            $this->_SQL = 'UPDATE versvorgangsstatus ';
                            $this->_SQL .= ' SET VVS_KFZKENNZ = ' . $this->_DB->WertSetzen('VVS', 'T', $FormKennzeichen);
                            $this->_SQL .= ',VVS_LAENDERSCHL = \'D\' ';
                            $this->_SQL .= ' WHERE vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $FILNR);
                            $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $WANR);

                            $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->BindeVariablen('VVS', true));
                        }
                    }

                    $RsKennzeichen->DSWeiter();
                }
            }
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->SchreibeModulStatus('BearbKennzeichen', 'Ende: BearbKennzeichen...');
        $this->debugAusgabe('Ende: BearbKennzeichen...', 10);
    }

    public function FormatiereKennzeichen($Kennzeichen, $FilID, $WANR)
    {
        /**
         * ([A-Z]{1,3}) - Land/Stadt/Landkreis
         * [\s\-] - ein Leerzeichen oder Bindestrich
         * ([A-Z]{1,2})?[\s]? - optional: Mittelbuchstaben (max. 2), gefolgt von ebenso optionalem Leerzeichen (XY123 ist also auch denkbar)
         * ([0-9]+) - mindestens eine Ziffer
         *
         * $matches ergibt:
         * - [0] - die gesamte gefundene Zeichenkette
         * - [1] - Land/Stadt/Landkreis
         * - [2] - sofern vorhanden: Mittelbuchstaben (max. 2)
         * - [3] - abschlie�ende Zifferngruppe
         */
        if(preg_match('/^([A-Z���]{1,3})[\s\-]([A-Z���]{1,2})?[\s]?([0-9]+)$/i', $Kennzeichen, $matches))
        {
            $akzFormattiert = $matches[1] . '-';
            if(!empty($matches[2]))
            {
                // Drei Gruppen: A-BC123, A-BC 123, A BC 123
                $akzFormattiert .= $matches[2] . ' ' . $matches[3];
            }
            else
            {
                // Zwei Gruppen, zweite Gruppe nur Ziffern: A-1234, A 1234
                $akzFormattiert .= $matches[3];
            }
        }
        else
        {
            return false;
        }

        return $akzFormattiert;
    }

    public function ExportiereVorgaengeFuerWebservice()
    {
        $this->SchreibeModulStatus('ExportiereVorgaengeFuerWebservice', 'START: ExportiereVorgaengeFuerWebservice');
        $this->debugAusgabe('Start: ExportiereVorgaengeFuerWebservice...', 10);

        try
        {

            $this->_DB->TransaktionBegin();

            $this->debugAusgabe('Merge in VERSKOPFDATENEXPORT (Tabelle fuer Webservice...', 10);

            $SQL = 'MERGE INTO VERSKOPFDATENEXPORT vke USING';
            $SQL .= '(SELECT *';
            $SQL .= ' FROM V_VERS_EXP_KOPF vek';
            $SQL .= ' INNER JOIN VERSVORGANGSSTATUS VVS';
            $SQL .= ' ON vek.FILID         = vvs.vvs_filid';
            $SQL .= ' AND vek.WANR         = vvs.vvs_wanr';
            $SQL .= ' WHERE (vvs_vcn_key   = 127';
            $SQL .= ' AND vvs_ven_key      = 0)';
            $SQL .= ' OR (vvs_vcn_key      = 127';
            $SQL .= ' AND vvs_ven_key      <> 0';
            $SQL .= ' AND VVS.VVS_FREIGABE = 1) ';
            $SQL .= ' ) vek ON (vek.vvs_filid = vke.vke_filid AND vek.vvs_wanr = vke.vke_wanr)';
            $SQL .= ' WHEN NOT MATCHED THEN';
            $SQL .= '   INSERT';
            $SQL .= '     (';
            $SQL .= '       VKE_KENNUNG,';
            $SQL .= '       VKE_FILID,';
            $SQL .= '       VKE_WANR,';
            $SQL .= '       VKE_KUNDEN_ART,';
            $SQL .= '       VKE_FIRKU_ART,';
            $SQL .= '       VKE_ZAHL_ART,';
            $SQL .= '       VKE_VORST_ABZUG_KZ,';
            $SQL .= '       VKE_VORST_BETRAG,';
            $SQL .= '       VKE_SB_KZ,';
            $SQL .= '       VKE_SB_BETRAG,';
            $SQL .= '       VKE_VERS_NR,';
            $SQL .= '       VKE_VERS_BEZ,';
            $SQL .= '       VKE_VERS_STRASSE,';
            $SQL .= '       VKE_VERS_HNR,';
            $SQL .= '       VKE_VERS_PLZ,';
            $SQL .= '       VKE_VERS_ORT,';
            $SQL .= '       VKE_VERS_STAAT_NR,';
            $SQL .= '       VKE_VERS_TELNR,';
            $SQL .= '       VKE_VERSSCHEINNR,';
            $SQL .= '       VKE_SCHADENNR,';
            $SQL .= '       VKE_SCHADEN_DATUM,';
            $SQL .= '       VKE_SCHADEN_KZ,';
            $SQL .= '       VKE_SCHADEN_BEZ,';
            $SQL .= '       VKE_SCHADEN_ART,';
            $SQL .= '       VKE_UMSATZ_GESAMT,';
            $SQL .= '       VKE_UMSATZ_KUNDE,';
            $SQL .= '       VKE_UMSATZ_VERS,';
            $SQL .= '       VKE_AEM_NR,';
            $SQL .= '       VKE_KD_ANREDE,';
            $SQL .= '       VKE_KD_TITEL,';
            $SQL .= '       VKE_KD_NAME1,';
            $SQL .= '       VKE_KD_NAME2,';
            $SQL .= '       VKE_KD_NAME3,';
            $SQL .= '       VKE_KD_STRASSE,';
            $SQL .= '       VKE_KD_HNR,';
            $SQL .= '       VKE_KD_PLZ,';
            $SQL .= '       VKE_KD_ORT,';
            $SQL .= '       VKE_KD_RUFNR1,';
            $SQL .= '       VKE_KD_RUFNR2,';
            $SQL .= '       VKE_KD_STAAT_NR,';
            $SQL .= '       VKE_GK_ANREDE,';
            $SQL .= '       VKE_GK_TITEL,';
            $SQL .= '       VKE_GK_NAME1,';
            $SQL .= '       VKE_GK_NAME2,';
            $SQL .= '       VKE_GK_NAME3,';
            $SQL .= '       VKE_GK_STRASSE,';
            $SQL .= '       VKE_GK_HNR,';
            $SQL .= '       VKE_GK_PLZ,';
            $SQL .= '       VKE_GK_ORT,';
            $SQL .= '       VKE_GK_RUFNR1,';
            $SQL .= '       VKE_GK_RUFNR2,';
            $SQL .= '       VKE_GK_STAAT_NR,';
            $SQL .= '       VKE_AC_ANREDE,';
            $SQL .= '       VKE_AC_TITEL,';
            $SQL .= '       VKE_AC_NAME1,';
            $SQL .= '       VKE_AC_NAME2,';
            $SQL .= '       VKE_AC_STRASSE,';
            $SQL .= '       VKE_AC_HNR,';
            $SQL .= '       VKE_AC_PLZ,';
            $SQL .= '       VKE_AC_ORT,';
            $SQL .= '       VKE_AC_RUFNR1,';
            $SQL .= '       VKE_AC_RUFNR2,';
            $SQL .= '       VKE_AC_STAAT_NR,';
            $SQL .= '       VKE_KFZ_KZ,';
            $SQL .= '       VKE_KFZ_HERSTELLER,';
            $SQL .= '       VKE_KFZ_MODELL,';
            $SQL .= '       VKE_KFZ_TYP,';
            $SQL .= '       VKE_FAHRGESTELLNR,';
            $SQL .= '       VKE_KFZ_HERSTNR,';
            $SQL .= '       VKE_KTYPNR,';
            $SQL .= '       VKE_KFZ_KM,';
            $SQL .= '       VKE_USER,';
            $SQL .= '       VKE_USERDAT,';
            $SQL .= '       VKE_VWS_KEY,';
            $SQL .= '       VKE_KFZ_EZ';
            $SQL .= '     )';
            $SQL .= '     VALUES';
            $SQL .= '     (';
            $SQL .= '       vek.KENNUNG,';
            $SQL .= '       vek.VVS_FILID,';
            $SQL .= '       vek.VVS_WANR,';
            $SQL .= '       vek.KUNDEN_ART,';
            $SQL .= '       vek.FIRKU_ART,';
            $SQL .= '       vek.ZAHL_ART,';
            $SQL .= '       vek.VORST_ABZUG_KZ,';
            $SQL .= '       vek.VORST_BETRAG,';
            $SQL .= '       vek.SELBST_KZ,';
            $SQL .= '       vek.SELBST_BETRAG,';
            $SQL .= '       vek.VERS_NR,';
            $SQL .= '       vek.VERS_BEZ,';
            $SQL .= '       vek.VERS_STRASSE,';
            $SQL .= '       vek.VERS_HNR,';
            $SQL .= '       vek.VERS_PLZ,';
            $SQL .= '       vek.VERS_ORT,';
            $SQL .= '       vek.VERS_STAAT_NR,';
            $SQL .= '       vek.VERS_TELEFON,';
            $SQL .= '       vek.VERSSCHEINNR,';
            $SQL .= '       vek.SCHADEN_NR,';
            $SQL .= '       vek.SCHADEN_DATUM,';
            $SQL .= '       vek.VVS_SCHADENKZ,';
            $SQL .= '       vek.SCHADEN_BEZ,';
            $SQL .= '       vek.SCHADEN_ART,';
            $SQL .= '       vek.UMSATZ_GESAMT,';
            $SQL .= '       vek.UMSATZ_KUNDE,';
            $SQL .= '       vek.UMSATZ_VERS,';
            $SQL .= '       vek.AEM_NR,';
            $SQL .= '       vek.ANREDE,';
            $SQL .= '       vek.TITEL,';
            $SQL .= '       vek.NAME1,';
            $SQL .= '       vek.NAME2,';
            $SQL .= '       vek.NAME3,';
            $SQL .= '       vek.STRASSE,';
            $SQL .= '       vek.HNR,';
            $SQL .= '       vek.PLZ,';
            $SQL .= '       vek.ORT,';
            $SQL .= '       vek.RUFNR1,';
            $SQL .= '       vek.RUFNR2,';
            $SQL .= '       vek.STAAT_NR,';
            $SQL .= '       vek.GK_ANREDE,';
            $SQL .= '       vek.GK_TITEL,';
            $SQL .= '       vek.GK_NAME1,';
            $SQL .= '       vek.GK_NAME2,';
            $SQL .= '       vek.GK_NAME3,';
            $SQL .= '       vek.GK_STRASSE,';
            $SQL .= '       vek.GK_HNR,';
            $SQL .= '       vek.GK_PLZ,';
            $SQL .= '       vek.GK_ORT,';
            $SQL .= '       vek.GK_RUFNR1,';
            $SQL .= '       vek.GK_RUFNR2,';
            $SQL .= '       vek.GK_STAAT_NR,';
            $SQL .= '       vek.AC_ANREDE,';
            $SQL .= '       vek.AC_TITEL,';
            $SQL .= '       vek.AC_NAME1,';
            $SQL .= '       vek.AC_NAME2,';
            $SQL .= '       vek.AC_STRASSE,';
            $SQL .= '       vek.AC_HNR,';
            $SQL .= '       vek.AC_PLZ,';
            $SQL .= '       vek.AC_ORT,';
            $SQL .= '       vek.AC_RUFNR1,';
            $SQL .= '       vek.AC_RUFNR2,';
            $SQL .= '       vek.AC_STAAT_NR,';
            $SQL .= '       vek.VVS_KFZKENNZ,';
            $SQL .= '       vek.KFZ_HERSTELLER,';
            $SQL .= '       vek.KFZ_MODELL,';
            $SQL .= '       vek.KFZ_TYP,';
            $SQL .= '       vek.FAHRGESTELLNR,';
            $SQL .= '       vek.KFZ_HERSTNR,';
            $SQL .= '       vek.KTYPNR,';
            $SQL .= '       vek.KFZ_KM,';
            $SQL .= '       \'AWIS\',';
            $SQL .= '       sysdate,';
            $SQL .= '       vek.VVS_REG_ANGEWANDT,';
            $SQL .= '       vek.KFZ_EZ';
            $SQL .= '     )';

            $this->_DB->Ausfuehren($SQL, '', true);

            $this->debugAusgabe('Merge in VERSPOSITIONSDATENEXPORT (Tabelle fuer Webservice...', 10);

            $SQL = 'MERGE INTO VERSPOSITIONSDATENEXPORT vpe USING';
            $SQL .= ' 			 (SELECT *';
            $SQL .= ' 			 FROM VERSKOPFDATENEXPORT vke';
            $SQL .= ' 			 INNER JOIN V_VERS_FORM_POSDATEN vep';
            $SQL .= ' 			 ON vep.FILNR        = vke.vke_filid';
            $SQL .= ' 			 AND vep.WANR        = vke.vke_wanr';
            $SQL .= ' 			 ) vv ON (vv.vke_key = vpe.vpe_vke_key)';
            $SQL .= ' 			 WHEN NOT MATCHED THEN';
            $SQL .= ' 			   INSERT';
            $SQL .= ' 			     (';
            $SQL .= ' 			       VPE_VKE_KEY,';
            $SQL .= ' 			       VPE_FILID,';
            $SQL .= ' 			       VPE_WANR,';
            $SQL .= ' 			       VPE_POSITION,';
            $SQL .= ' 			       VPE_ARTNR,';
            $SQL .= ' 			       VPE_ARTKZ,';
            $SQL .= ' 			       VPE_BEZ,';
            $SQL .= ' 			       VPE_MENGE,';
            $SQL .= ' 			       VPE_VK_PREIS,';
            $SQL .= ' 			       VPE_UMSATZ,';
            $SQL .= ' 			       VPE_RABATTKENN,';
            $SQL .= ' 			       VPE_USER,';
            $SQL .= ' 			       VPE_USERDAT';
            $SQL .= ' 			     )';
            $SQL .= ' 			     VALUES';
            $SQL .= ' 			     (';
            $SQL .= ' 			       vv.vke_key,';
            $SQL .= ' 			       vv.FILNR,';
            $SQL .= ' 			       vv.WANR,';
            $SQL .= ' 			       vv.POSITION,';
            $SQL .= ' 			       vv.ARTNR,';
            $SQL .= ' 			       vv.ARTKZ,';
            $SQL .= ' 			       vv.BEZ,';
            $SQL .= ' 			       vv.MENGE,';
            $SQL .= ' 			       vv.VK_PREIS,';
            $SQL .= ' 			       vv.UMSATZ,';
            $SQL .= ' 			       vv.RABATTKENN,';
            $SQL .= ' 			       \'AWIS\',';
            $SQL .= ' 			       sysdate';
            $SQL .= ' 			     )';

            $this->_DB->Ausfuehren($SQL, '', true);

            $this->debugAusgabe('Merge in VERSBEARBEITERDATENEXPORT (Tabelle fuer Webservice)...', 10);

            $SQL = 'MERGE INTO VERSBEARBEITERDATENEXPORT vbe USING';
            $SQL .= ' (SELECT *';
            $SQL .= ' FROM VERSKOPFDATENEXPORT vke';
            $SQL .= ' INNER JOIN V_VERS_FORM_BEARBDATEN veb';
            $SQL .= ' ON veb.filnr        = vke.vke_filid';
            $SQL .= ' AND veb.wanr        = vke.vke_wanr';
            $SQL .= ' ) vv ON (vv.vke_key = vbe.vbe_vke_key)';
            $SQL .= ' WHEN NOT MATCHED THEN';
            $SQL .= '   INSERT';
            $SQL .= '     (';
            $SQL .= '       VBE_VKE_KEY,';
            $SQL .= '       VBE_FILID,';
            $SQL .= '       VBE_WANR,';
            $SQL .= '       VBE_STATUS,';
            $SQL .= '       VBE_DATUM,';
            $SQL .= '       VBE_ZEIT,';
            $SQL .= '       VBE_VERKAEUFERART,';
            $SQL .= '       VBE_NAME,';
            $SQL .= '       VBE_USER,';
            $SQL .= '       VBE_USERDAT';
            $SQL .= '     )';
            $SQL .= '     VALUES';
            $SQL .= '     (';
            $SQL .= '       vv.vke_key,';
            $SQL .= '       vv.filnr,';
            $SQL .= '       vv.wanr,';
            $SQL .= '       vv.status,';
            $SQL .= '       vv.datum,';
            $SQL .= '       vv.zeit,';
            $SQL .= '       vv.verkaeuferart,';
            $SQL .= '       vv.name,';
            $SQL .= '       \'AWIS\',';
            $SQL .= '       sysdate';
            $SQL .= '     )';

            $this->_DB->Ausfuehren($SQL, '', true);

            $this->debugAusgabe('Update Vorgaenge, mit Status 128 die in die Exporttabellen geschrieben wurden ...', 10);

            $SQL = 'update versvorgangsstatus vvs';
            $SQL .= '     set vvs.vvs_vcn_key = 128';
            $SQL .= '     where exists (';
            $SQL .= '     select *';
            $SQL .= '     from VERSKOPFDATENEXPORT vke';
            $SQL .= '     inner join VERSPOSITIONSDATENEXPORT vpe';
            $SQL .= '     on vke.vke_key = vpe.vpe_vke_key';
            $SQL .= '     inner join VERSBEARBEITERDATENEXPORT vbe';
            $SQL .= '     on vke.vke_key = vbe.vbe_vke_key';
            $SQL .= '     where vvs.vvs_filid = vke.vke_filid';
            $SQL .= '     and vvs.vvs_wanr = vke.vke_wanr ';
            $SQL .= '     and vvs.vvs_vcn_key = 127)';

            $this->_DB->Ausfuehren($SQL, '', true);

            $SQL = 'update verskopfdatenexport a ';
            $SQL .= '   set vke_mwstsatz = ';
            $SQL .= '   (';
            $SQL .= '       select func_mehrwertsteuersaetze(vbe_datum,\'DE\',\'v\')';
            $SQL .= '       from verskopfdatenexport b left join versbearbeiterdatenexport c';
            $SQL .= '       on b.vke_key = c.vbe_vke_key';
            $SQL .= '       and c.vbe_status = \'E\'';
            $SQL .= '       where b.vke_vws_key in (0,1)';
            $SQL .= '       and b.vke_key = a.vke_key';
            $SQL .= '   )';
            $SQL .= '   where vke_vws_key in (0,1)';

            $this->_DB->Ausfuehren($SQL, '', true);

            $this->_DB->TransaktionCommit();
        }
        catch(Exception $ex)
        {
            $this->_DB->TransaktionRollback();
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }
        $this->SchreibeModulStatus('ExportiereVorgaengeFuerWebservice', 'Ende: ExportiereVorgaengeFuerWebservice');
        $this->debugAusgabe('Ende: ExportiereVorgaengeFuerWebservice...', 10);
    }

    /**
     * Schreibt alle vorher erfolgreich gemachten Vorg�nge in eine Exporttabelle
     * und exportiert diese Daten dann in Dateien. F�r die beiden Versicherungen
     * (DBV-Winterthur & DEBEKA) gibt es jeweils eine eigenen Exportdatei.
     *
     * @version 1.0
     * @author  Stefan Oppl
     *
     * 02.03.2012 BT : Bei Feld "SB" im Kopfdatensatz die Kommastellen wegschneiden
     */
    public function ExportiereVorgaenge()
    {
        $this->SchreibeModulStatus('ExportiereVorg�nge', 'Start: ExportiereVorgaenge');
        $this->debugAusgabe('Start: ExportiereVorgaenge...', 10);

        try
        {

            // Pruefen ob Vorgaenge vorhanden sind bei denen der Export
            // begonnen wurde aber nicht abgeschlossen werden konnte
            $this->_SQL = 'SELECT vvs_filid, vvs_wanr ';
            $this->_SQL .= ' FROM versvorgangsstatus ';
            $this->_SQL .= ' WHERE vvs_vcn_key = 110';

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
            {
                $this->SchreibeModulStatus('ExportiereVorgaenge', 'L�sche unvollst�ndige...');
                // Es wurden unvollstaendig exportierte Vorgaenge gefunden
                // diese wieder aus der Exporttabelle loeschen und Status zuruecksetzen
                $rsFehlerExport = $this->_DB->RecordsetOeffnen($this->_SQL);

                while(!$rsFehlerExport->EOF())
                {
                    $this->_FilNr = $rsFehlerExport->FeldInhalt('VVS_FILID');
                    $this->_WaNr = $rsFehlerExport->FeldInhalt('VVS_WANR');

                    $this->_SQL = 'DELETE ';
                    $this->_SQL .= ' FROM versexport ';
                    $this->_SQL .= ' WHERE vek_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $this->_FilNr);
                    $this->_SQL .= ' AND vek_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $this->_WaNr);

                    $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VVS', true));

                    $this->SetzeCheckStatus(106);

                    $rsFehlerExport->DSWeiter();
                }
            }
            $this->debugAusgabe('Vorgaenge fuer Export suchen...', 10);

            // Vorg�nge in Exporttabelle schreiben
            // 19.10.2010 OP: In Where Klausel bei vvs_vcn_key not in in Klammer "120" ergaenzt = Status fuer Umgebucht
            // 07.08.2015 BT: VVS_VCN_KEY 124 ergaenzt = Vorgang blockiert, doppelter Rechnungsversand
            $this->_SQL = 'SELECT vvs_key, vvs_filid, vvs_wanr ';
            $this->_SQL .= 'FROM versvorgangsstatus ';
            $this->_SQL .= 'WHERE vvs_vcn_key = 106 ';
            $this->_SQL .= 'OR vvs_vcn_key not in (106,110,111,112,113,114,115,116,117,118,119,120,124,125,126,127,128,129) ';
            $this->_SQL .= 'AND vvs_freigabe = 1';

            $AnzExpVorgaenge = $this->_DB->ErmittleZeilenAnzahl($this->_SQL);
            $ZaehlerExpVorgaenge = 1;

            if($AnzExpVorgaenge > 0)
            {
                $rsExport = $this->_DB->RecordsetOeffnen($this->_SQL);

                while(!$rsExport->EOF())
                {
                    $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Vorgang ' . $ZaehlerExpVorgaenge . ' von ' . $AnzExpVorgaenge . ' in Exporttabelle');

                    $this->_FilNr = $rsExport->FeldInhalt('VVS_FILID');
                    $this->_WaNr = $rsExport->FeldInhalt('VVS_WANR');

                    $this->SetzeCheckStatus(110);

                    //###########################################################################
                    //KOPFSATZ
                    //###########################################################################

                    $this->_SQL = 'SELECT vv.vvs_key, ve.kennung, vv.vvs_filid, vv.vvs_wanr, ve.kunden_art, ve.firku_art, ve.zahl_art, ';
                    $this->_SQL .= ' ve.vorst_abzug_kz, ve.vorst_betrag, ve.selbst_kz, ve.selbst_betrag, v.vve_versnr_export,ve.vers_bez, ';
                    $this->_SQL .= ' ve.vers_strasse,ve.vers_hnr,ve.vers_plz,ve.vers_ort,ve.vers_staat_nr,ve.vers_telefon, ';
                    $this->_SQL .= ' ve.versscheinnr,ve.schaden_nr,ve.schaden_datum,vv.vvs_schadenkz,ve.schaden_bez,ve.schaden_art, ';
                    $this->_SQL .= ' ve.umsatz_gesamt,ve.umsatz_kunde,ve.umsatz_vers,vv.vvs_vorgangnr,vv.vvs_datumfg,ve.anrede,ve.titel,ve.name1,ve.name2, ';
                    $this->_SQL .= ' ve.name3,ve.strasse,ve.hnr,ve.plz,ve.ort,ve.rufnr1,ve.rufnr2,ve.staat_nr,ve.gk_anrede, ';
                    $this->_SQL .= ' ve.gk_titel,ve.gk_name1,ve.gk_name2,ve.gk_name3,ve.gk_strasse,ve.gk_hnr,ve.gk_plz,ve.gk_ort, ';
                    $this->_SQL .= ' ve.gk_rufnr1,ve.gk_rufnr2,ve.gk_staat_nr,ve.ac_anrede,ve.ac_titel,ve.ac_name1,ve.ac_name2, ';
                    $this->_SQL .= ' ve.ac_strasse,ve.ac_hnr,ve.ac_plz,ve.ac_ort,ve.ac_rufnr1,ve.ac_rufnr2,ve.ac_staat_nr,vv.vvs_kfzkennz, ';
                    $this->_SQL .= ' ve.kfz_hersteller,ve.kfz_modell,ve.kfz_typ,ve.fahrgestellnr,ve.kfz_herstnr,vv.vvs_laenderschl,ve.ktypnr,vv.vvs_ust_schl, ';
                    $this->_SQL .= ' f.fil_strasse,f.fil_plz,f.fil_ort,ve.kfz_km,v.vve_kuerzel, vv.vvs_versnr ';
                    $this->_SQL .= ' FROM V_VERS_EXP_KOPF ve INNER JOIN versvorgangsstatus vv ';
                    $this->_SQL .= ' ON ve.filid = vv.vvs_filid AND ve.wanr = vv.vvs_wanr ';
                    $this->_SQL .= ' INNER JOIN versversicherungen v ';
                    $this->_SQL .= ' ON v.vve_versnr = vv.vvs_versnr ';
                    $this->_SQL .= ' INNER JOIN filialen f ';
                    $this->_SQL .= ' ON f.fil_id = vv.vvs_filid ';
                    $this->_SQL .= ' WHERE vv.vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $this->_FilNr);
                    $this->_SQL .= ' AND vv.vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $this->_WaNr);

                    $rsExpKopf = $this->_DB->RecordsetOeffnen($this->_SQL, $this->_DB->BindeVariablen('VVS', true));

                    if($rsExpKopf->AnzahlDatensaetze() == 0)
                    {
                        // Kopfsatzdaten wurden nicht gefunden (Quelle DWH)
                        $this->SetzeCheckStatus(107);
                        throw new Exception("Kein Kopfsatz vorhanden fuer " . $rsExport->FeldInhalt('VVS_KEY'));
                    }
                    else
                    {
                        // Kopfsatz kann in Exporttabelle geschrieben werden

                        $ExpStringKopf = '';

                        // Felder in Exportstring schreiben (Semikolon getrennt)
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('KENNUNG') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_FILID') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_WANR') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('KUNDEN_ART') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIRKU_ART') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('ZAHL_ART') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VORST_ABZUG_KZ') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VORST_BETRAG') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('SELBST_KZ') . ';';

                        //SB in integer umwandeln/casten damit Nachkommastellen wegfallen
                        $sb = $rsExpKopf->FeldInhalt('SELBST_BETRAG');
                        $ExpStringKopf .= intval($sb) . ';';

                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVE_VERSNR_EXPORT') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_BEZ') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_STRASSE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_HNR') . ';';
                        $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('VERS_PLZ'), 5, '0', STR_PAD_LEFT) . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_ORT') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_STAAT_NR') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VERS_TELEFON') . ';';
                        //####################################################################################################
                        if($rsExpKopf->FeldInhalt('VVE_KUERZEL') != 'HUK')
                        {
                            $ExpStringKopf .= $this->EntferneBuchstaben($rsExpKopf->FeldInhalt('VERSSCHEINNR')) . ';';
                        }
                        else
                        {
                            $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('VERSSCHEINNR'), 0, 17) . ';';
                        }
                        //####################################################################################################
                        if($rsExpKopf->FeldInhalt('VVE_KUERZEL') == 'WGV')
                        {
                            // Schadennummer f�r die WGV formatieren 07-xxxxxxx
                            $ExpStringKopf .= $this->FormatiereSchadenNr($rsExpKopf->FeldInhalt('SCHADEN_NR')) . ';';
                        }
                        else
                        {
                            if($rsExpKopf->FeldInhalt('SCHADEN_NR') != '' && is_null($rsExpKopf->FeldInhalt('SCHADEN_NR')) != true)
                            {
                                $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('SCHADEN_NR'), 0, 20) . ';';
                            }
                            else
                            {
                                $ExpStringKopf .= ';';
                            }
                        }

                        //####################################################################################################
                        if($rsExpKopf->FeldInhalt('SCHADEN_DATUM') != '' && is_null($rsExpKopf->FeldInhalt('SCHADEN_DATUM')) != true)
                        {
                            $tmpSchadenDatum = $this->pruefeSchadenDatum($rsExpKopf->FeldInhalt('SCHADEN_DATUM'), $this->_FilNr, $this->_WaNr);

                            $ExpStringKopf .= $this->_FORM->PruefeDatum($tmpSchadenDatum, false) . ';';
                        }
                        else
                        {
                            $ExpStringKopf .= $this->pruefeSchadenDatum('01.01.1970', $this->_FilNr, $this->_WaNr) . ';';
                        }

                        //####################################################################################################
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_SCHADENKZ') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('SCHADEN_BEZ') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('SCHADEN_ART') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_GESAMT') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_KUNDE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('UMSATZ_VERS') . ';';
                        //####################################################################################################
                        if($rsExpKopf->FeldInhalt('VVS_SCHADENKZ') == 'E')
                        {
                            $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_VORGANGNR') . ';';
                            $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_DATUMFG') . ';';
                        }
                        else
                        {
                            $ExpStringKopf .= ';';
                            $ExpStringKopf .= ';';
                        }
                        //####################################################################################################
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('ANREDE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('TITEL') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME1') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME2') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('NAME3') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('STRASSE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('HNR') . ';';
                        //####################################################################################################
                        if($rsExpKopf->FeldInhalt('PLZ') != '' && is_null($rsExpKopf->FeldInhalt('PLZ')) != true)
                        {
                            $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('PLZ'), 5, '0', STR_PAD_LEFT) . ';';
                        }
                        else
                        {
                            $ExpStringKopf .= $rsExpKopf->FeldInhalt('PLZ') . ';';
                        }
                        //####################################################################################################
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('ORT') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('RUFNR1') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('RUFNR2') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('STAAT_NR') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_ANREDE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_TITEL') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_NAME1') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_NAME2') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_NAME3') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_STRASSE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_HNR') . ';';
                        //####################################################################################################
                        if($rsExpKopf->FeldInhalt('GK_PLZ') != '' && is_null($rsExpKopf->FeldInhalt('GK_PLZ')) != true)
                        {
                            $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('GK_PLZ'), 5, '0', STR_PAD_LEFT) . ';';
                        }
                        else
                        {
                            $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_PLZ') . ';';
                        }
                        //####################################################################################################
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_ORT') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_RUFNR1') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_RUFNR2') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('GK_STAAT_NR') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_ANREDE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_TITEL') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_NAME1') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_NAME2') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_STRASSE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_HNR') . ';';
                        //####################################################################################################
                        if($rsExpKopf->FeldInhalt('AC_PLZ') != '' && is_null($rsExpKopf->FeldInhalt('AC_PLZ')) != true)
                        {
                            $ExpStringKopf .= str_pad($rsExpKopf->FeldInhalt('AC_PLZ'), 5, '0', STR_PAD_LEFT) . ';';
                        }
                        else
                        {
                            $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_PLZ') . ';';
                        }
                        //####################################################################################################
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_ORT') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_RUFNR1') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_RUFNR2') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('AC_STAAT_NR') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_KFZKENNZ') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_HERSTELLER') . ';';
                        $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('KFZ_MODELL'), 0, 55) . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_TYP') . ';';
                        $ExpStringKopf .= substr($rsExpKopf->FeldInhalt('FAHRGESTELLNR'), 0, 17) . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_HERSTNR') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_LAENDERSCHL') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('KTYPNR') . ';';
                        //$ExpStringKopf .= $rsExpKopf->FeldInhalt('VVS_UST_SCHL').';';
                        $ExpStringKopf .= '19,0;';

                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIL_STRASSE') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIL_PLZ') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('FIL_ORT') . ';';
                        $ExpStringKopf .= $rsExpKopf->FeldInhalt('KFZ_KM');

                        $ExpStringKopf = str_replace("'", "�", $ExpStringKopf);

                        $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                        $this->_SQL .= ' VALUES (';
                        $this->_SQL .= $this->_DB->WertSetzen('VEK', 'N0', $this->_FilNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VEK', 'N0', $this->_WaNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VEK', 'N0', $rsExpKopf->FeldInhalt('VVS_VERSNR')) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VEK', 'T', $ExpStringKopf);
                        $this->_SQL .= ')';

                        $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VEK', true));
                    }

                    //###########################################################################
                    //POSITIONSSATZ
                    //###########################################################################

                    $this->_SQL = 'SELECT \'P\' as kennung,vv.vvs_filid,vv.vvs_wanr,vp.position,vp.artnr,vp.artkz,vp.bez,vp.menge,vp.vk_preis, ';
                    $this->_SQL .= ' vp.umsatz,vp.rabattkenn,vv.vvs_versnr,vv.vvs_schadenkz ';
                    $this->_SQL .= ' FROM V_VERS_FORM_POSDATEN vp INNER JOIN versvorgangsstatus vv ';
                    $this->_SQL .= ' ON vp.filnr = vv.vvs_filid AND vp.wanr = vv.vvs_wanr ';
                    $this->_SQL .= ' WHERE vv.vvs_filid = ' . $this->_DB->WertSetzen('VEP', 'N0', $this->_FilNr);
                    $this->_SQL .= ' AND vv.vvs_wanr = ' . $this->_DB->WertSetzen('VEP', 'N0', $this->_WaNr);

                    $rsExpPos = $this->_DB->RecordsetOeffnen($this->_SQL, $this->_DB->BindeVariablen('VEP', true));

                    if($rsExpPos->AnzahlDatensaetze() == 0)
                    {
                        // Possatzdaten wurden nicht gefunden (Quelle DWH)
                        $this->SetzeCheckStatus(108);
                        throw new Exception("Kein Possatz vorhanden fuer " . $rsExport->FeldInhalt('VVS_KEY'));
                    }
                    else
                    {
                        // Kopfsatz kann in Exporttabelle geschrieben werden

                        while(!$rsExpPos->EOF())
                        {
                            if($rsExpPos->FeldInhalt('VVS_SCHADENKZ') == 'S' && $rsExpPos->FeldInhalt('ARTNR') == '1EIN69' || $rsExpPos->FeldInhalt('VVS_SCHADENKZ') == 'E' && $rsExpPos->FeldInhalt('ARTNR') == 'GLAS01')
                            {

                                $ExpStringPos = '';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('KENNUNG') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('VVS_FILID') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('VVS_WANR') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('POSITION') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('ARTNR') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('ARTKZ') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('BEZ') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('MENGE') . ';';
                                $ExpStringPos .= $rsExpPos->FeldInhalt('VK_PREIS') . ';';
                                if(strpos($rsExpPos->FeldInhalt('UMSATZ'), ',') !== false)
                                {
                                    $ExpStringPos .= substr($rsExpPos->FeldInhalt('UMSATZ'), 0, strpos($rsExpPos->FeldInhalt('UMSATZ'), ',')) . ';';
                                }
                                else
                                {
                                    $ExpStringPos .= $rsExpPos->FeldInhalt('UMSATZ') . ';';
                                }
                                //$ExpStringPos .= $rsExpPos->FeldInhalt('RABATTKENN');
                                $ExpStringPos .= 'N';

                                $ExpStringPos = str_replace("'", "�", $ExpStringPos);

                                $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                                $this->_SQL .= ' VALUES (';
                                $this->_SQL .= $this->_DB->WertSetzen('VEP', 'N0', $this->_FilNr) . ', ';
                                $this->_SQL .= $this->_DB->WertSetzen('VEP', 'N0', $this->_WaNr) . ', ';
                                $this->_SQL .= $this->_DB->WertSetzen('VEP', 'N0', $rsExpPos->FeldInhalt('VVS_VERSNR')) . ', ';
                                $this->_SQL .= $this->_DB->WertSetzen('VEP', 'T', $ExpStringPos);
                                $this->_SQL .= ')';

                                $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VEP', true));
                            }
                            $rsExpPos->DSWeiter();
                        }
                    }

                    //###########################################################################
                    //BEARBEITERSATZ
                    //###########################################################################

                    // V-Satz wegschreiben

                    $this->_SQL = 'SELECT \'B\' as kennung,vv.vvs_filid,vv.vvs_wanr,vb.status, ';
                    $this->_SQL .= ' vb.datum,vb.zeit,vb.verkaeuferart,vb.name,vv.vvs_versnr ';
                    $this->_SQL .= ' FROM V_VERS_FORM_BEARBDATEN vb INNER JOIN versvorgangsstatus vv ';
                    $this->_SQL .= ' ON vb.filnr = vv.vvs_filid AND vb.wanr = vv.vvs_wanr ';
                    $this->_SQL .= ' WHERE vb.verkaeuferart = \'V\' ';
                    $this->_SQL .= ' AND vv.vvs_filid = ' . $this->_DB->WertSetzen('VFB', 'N0', $this->_FilNr);
                    $this->_SQL .= ' AND vv.vvs_wanr = ' . $this->_DB->WertSetzen('VFB', 'N0', $this->_WaNr);

                    $rsExpBearb = $this->_DB->RecordsetOeffnen($this->_SQL, $this->_DB->BindeVariablen('VFB', true));

                    if($rsExpBearb->AnzahlDatensaetze() == 0)
                    {
                        // Bearbeitersatzdaten wurden nicht gefunden (Quelle DWH)
                        $this->SetzeCheckStatus(109);
                        throw new Exception("Bearbsatz fehlt fuer " . $rsExport->FeldInhalt('VVS_KEY'));
                    }
                    else
                    {
                        // Kopfsatz kann in Exporttabelle geschrieben werden
                        $ExpStringBearb = '';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('KENNUNG') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_FILID') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_WANR') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('STATUS') . ';';
                        $ExpStringBearb .= substr($rsExpBearb->FeldInhalt('DATUM'), 0, 10) . ';';

                        if(strlen($rsExpBearb->FeldInhalt('ZEIT')) < 8)
                        {
                            $ExpStringBearb .= str_pad($rsExpBearb->FeldInhalt('ZEIT'), 8, '0', STR_PAD_LEFT) . ';';
                        }
                        else
                        {
                            $ExpStringBearb .= $rsExpBearb->FeldInhalt('ZEIT') . ';';
                        }

                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VERKAEUFERART') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('NAME');

                        $ExpStringBearb = str_replace("'", "�", $ExpStringBearb);

                        $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                        $this->_SQL .= ' VALUES (';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $this->_FilNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $this->_WaNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $rsExpBearb->FeldInhalt('VVS_VERSNR')) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'T', $ExpStringBearb);
                        $this->_SQL .= ')';

                        $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VFB', true));
                    }

                    // M-Satz wegschreiben

                    $this->_SQL = 'SELECT \'B\' as kennung,vv.vvs_filid,vv.vvs_wanr,vb.status, ';
                    $this->_SQL .= ' vb.datum,vb.zeit,vb.verkaeuferart,vb.name,vv.vvs_versnr ';
                    $this->_SQL .= ' FROM V_VERS_FORM_BEARBDATEN vb INNER JOIN versvorgangsstatus vv ';
                    $this->_SQL .= ' ON vb.filnr = vv.vvs_filid AND vb.wanr = vv.vvs_wanr ';
                    $this->_SQL .= ' WHERE vb.verkaeuferart = \'M\' ';
                    $this->_SQL .= ' AND vv.vvs_filid = ' . $this->_DB->WertSetzen('VFB', 'N0', $this->_FilNr);
                    $this->_SQL .= ' AND vv.vvs_wanr = ' . $this->_DB->WertSetzen('VFB', 'N0', $this->_WaNr);

                    $rsExpBearb = $this->_DB->RecordsetOeffnen($this->_SQL, $this->_DB->BindeVariablen('VFB', true));

                    if($rsExpBearb->AnzahlDatensaetze() == 0)
                    {
                        // Bearbeitersatzdaten wurden nicht gefunden (Quelle DWH)
                        $this->SetzeCheckStatus(109);
                    }
                    else
                    {
                        // Kopfsatz kann in Exporttabelle geschrieben werden

                        $ExpStringBearb = '';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('KENNUNG') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_FILID') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_WANR') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('STATUS') . ';';
                        $ExpStringBearb .= substr($rsExpBearb->FeldInhalt('DATUM'), 0, 10) . ';';

                        if(strlen($rsExpBearb->FeldInhalt('ZEIT')) < 8)
                        {
                            $ExpStringBearb .= str_pad($rsExpBearb->FeldInhalt('ZEIT'), 8, '0', STR_PAD_LEFT) . ';';
                        }
                        else
                        {
                            $ExpStringBearb .= $rsExpBearb->FeldInhalt('ZEIT') . ';';
                        }

                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VERKAEUFERART') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('NAME');

                        $ExpStringBearb = str_replace("'", "�", $ExpStringBearb);

                        $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                        $this->_SQL .= ' VALUES (';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $this->_FilNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $this->_WaNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $rsExpBearb->FeldInhalt('VVS_VERSNR')) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'T', $ExpStringBearb);
                        $this->_SQL .= ')';

                        $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VFB', true));
                    }

                    // K-Satz wegschreiben

                    $this->_SQL = 'SELECT \'B\' as kennung,vv.vvs_filid,vv.vvs_wanr,vb.status, ';
                    $this->_SQL .= ' vb.datum,vb.zeit,vb.verkaeuferart,vb.name,vv.vvs_versnr ';
                    $this->_SQL .= ' FROM V_VERS_FORM_BEARBDATEN vb INNER JOIN versvorgangsstatus vv ';
                    $this->_SQL .= ' ON vb.filnr = vv.vvs_filid AND vb.wanr = vv.vvs_wanr ';
                    $this->_SQL .= ' WHERE vb.verkaeuferart = \'K\' ';
                    $this->_SQL .= ' AND vv.vvs_filid = ' . $this->_DB->WertSetzen('VFB', 'N0', $this->_FilNr);
                    $this->_SQL .= ' AND vv.vvs_wanr = ' . $this->_DB->WertSetzen('VFB', 'N0', $this->_WaNr);

                    $rsExpBearb = $this->_DB->RecordsetOeffnen($this->_SQL, $this->_DB->BindeVariablen('VFB', true));

                    if($rsExpBearb->AnzahlDatensaetze() == 0)
                    {
                        // Bearbeitersatzdaten wurden nicht gefunden (Quelle DWH)
                        $this->SetzeCheckStatus(109);
                    }
                    else
                    {
                        // Kopfsatz kann in Exporttabelle geschrieben werden

                        $ExpStringBearb = '';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('KENNUNG') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_FILID') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VVS_WANR') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('STATUS') . ';';
                        $ExpStringBearb .= substr($rsExpBearb->FeldInhalt('DATUM'), 0, 10) . ';';

                        if(strlen($rsExpBearb->FeldInhalt('ZEIT')) < 8)
                        {
                            $ExpStringBearb .= str_pad($rsExpBearb->FeldInhalt('ZEIT'), 8, '0', STR_PAD_LEFT) . ';';
                        }
                        else
                        {
                            $ExpStringBearb .= $rsExpBearb->FeldInhalt('ZEIT') . ';';
                        }

                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('VERKAEUFERART') . ';';
                        $ExpStringBearb .= $rsExpBearb->FeldInhalt('NAME');

                        $ExpStringBearb = str_replace("'", "�", $ExpStringBearb);

                        $this->_SQL = 'INSERT INTO versexport (vek_filid,vek_wanr,vek_versnr,vek_datensatz) ';
                        $this->_SQL .= ' VALUES (';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $this->_FilNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $this->_WaNr) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'N0', $rsExpBearb->FeldInhalt('VVS_VERSNR')) . ', ';
                        $this->_SQL .= $this->_DB->WertSetzen('VFB', 'T', $ExpStringBearb);
                        $this->_SQL .= ')';

                        $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VFB', true));
                    }

                    $this->SetzeCheckStatus(111);

                    $rsExport->DSWeiter();

                    $ZaehlerExpVorgaenge++;
                }
                $this->debugAusgabe('Vorgaenge in Exporttabellen (fuer Datei) weggeschrieben...', 10);
            }
            //Daten wegschreiben

            // Datum ermitteln --> genommen wird das Datum wann die Verarbeitung (also FirstGlas)
            // begonnen hat zu Verarbeiten (Tabelle: versjobid)

            $SQL = 'SELECT vji_datumjobid ';
            $SQL .= 'FROM versjobid ';

            $rsDatum = $this->_DB->RecordSetOeffnen($SQL);

            $Datum = substr($rsDatum->FeldInhalt('VJI_DATUMJOBID'), 8, 2) . substr($rsDatum->FeldInhalt('VJI_DATUMJOBID'), 3, 2) . substr($rsDatum->FeldInhalt('VJI_DATUMJOBID'), 0, 2);

            $SQL = " SELECT DISTINCT VVE_EXPORTDATEI, VVE_KUERZEL";
            $SQL .= " FROM VERSVERSICHERUNGEN";
            $SQL .= " WHERE VVE_AKTIV = 1 and VVE_EXPORTDATEI IS NOT NULL";

            $rsDateiname = $this->_DB->RecordSetOeffnen($SQL);

            $this->debugAusgabe('Erstelle Export-Dateien...', 10);

            while(!$rsDateiname->EOF())
            {

                $Dateiname = $rsDateiname->FeldInhalt('VVE_EXPORTDATEI') . $Datum;
                $CheckDatei = $rsDateiname->FeldInhalt('VVE_EXPORTDATEI') . $Datum . '.txt';

                if($this->CheckExportStatus($CheckDatei) === false)
                {
                    $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe ' . $rsDateiname->FeldInhalt('VVE_KUERZEL') . '-Datei');
                    $this->SchreibeExportStatus($CheckDatei, 'Start');

                    //Erzeuge zuerst Temp-Dateien
                    if(($fd = fopen($this->_PfadExportVers . $Dateiname . '.tmp', 'w')) === false)
                    {
                        fclose($fd);

                        $err = "Fehler beim Erstellen der Datei: " . $this->_PfadExportVers . $Dateiname . '.tmp' . PHP_EOL;
                        throw new Exception($err);
                    }

                    $SQL = 'SELECT ve.vek_key, ve.vek_datensatz, ve.vek_filid, ve.vek_wanr, ve.vek_versnr  ';
                    $SQL .= ' FROM versversicherungen vv INNER JOIN versexport ve ';
                    $SQL .= ' ON vv.vve_versnr = ve.vek_versnr ';
                    $SQL .= ' WHERE vv.vve_exportdatei = ' . $this->_DB->WertSetzen('VVE', 'T', $rsDateiname->FeldInhalt('VVE_EXPORTDATEI'));
                    $SQL .= ' ORDER BY vek_key';

                    $rsExport = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('VVE'));

                    while(!$rsExport->EOF())
                    {
                        if(fputs($fd, $rsExport->FeldInhalt('VEK_DATENSATZ') . chr(13) . chr(10)))
                        {
                            $SQL = 'UPDATE versvorgangsstatus ';
                            $SQL .= ' SET vvs_exportdatei = \'' . $Dateiname . '\'';
                            $SQL .= ' WHERE vvs_filid = ' . $rsExport->FeldInhalt('VEK_FILID');
                            $SQL .= ' AND vvs_wanr = ' . $rsExport->FeldInhalt('VEK_WANR');

                            $this->_DB->Ausfuehren($SQL, '', true);

                            $this->_SQL = 'UPDATE versexport ';
                            $this->_SQL .= ' SET vek_exportiert = 1';
                            $this->_SQL .= ' WHERE vek_key = ' . $rsExport->FeldInhalt('VEK_KEY');

                            $this->_DB->Ausfuehren($this->_SQL, '', true);

                            $rsExport->DSWeiter();
                        }
                        else
                        {
                            fclose($fd);
                            $err = "Fehler beim Schreiben der Exportdaten in die Datei: " . $this->_PfadExportVers . $Dateiname . '.tmp' . PHP_EOL;
                            throw new Exception($err);
                        }
                    }

                    fclose($fd);

                    //benenne Datei in .txt um
                    rename($this->_PfadExportVers . $Dateiname . '.tmp', $this->_PfadExportVers . $Dateiname . '.txt');

                    $this->SchreibeExportStatus($CheckDatei, 'Ende', md5_file($this->_PfadExportVers . $CheckDatei));
                }

                $rsDateiname->DSWeiter();
            }

            $SQL = 'SELECT ';
            $SQL .= '     vek_filid,';
            $SQL .= '     vek_wanr,';
            $SQL .= '     vek_versnr,';
            $SQL .= '     vek_exportiert';
            $SQL .= '   FROM versexport';
            $SQL .= '   where vek_exportiert = 1';
            $SQL .= '   GROUP BY vek_filid,';
            $SQL .= '     vek_wanr,';
            $SQL .= '     vek_versnr,';
            $SQL .= '     vek_exportiert';

            $rsUpdateVorgaenge = $this->_DB->RecordSetOeffnen($SQL);

            $this->debugAusgabe('Update exportierte Vorgaenge mit Status 112 -> Vorgang in Datei exportiert...', 10);

            while(!$rsUpdateVorgaenge->EOF())
            {
                $this->_SQL = 'UPDATE versvorgangsstatus ';
                $this->_SQL .= ' SET vvs_vcn_key = 112, vvs_datumexport = sysdate ';
                $this->_SQL .= ' WHERE vvs_vcn_key = 111';
                $this->_SQL .= ' AND vvs_filid = ' . $rsUpdateVorgaenge->FeldInhalt('VEK_FILID');
                $this->_SQL .= ' AND vvs_wanr = ' . $rsUpdateVorgaenge->FeldInhalt('VEK_WANR');

                $this->_DB->Ausfuehren($this->_SQL, '', true);

                $rsUpdateVorgaenge->DSWeiter();
            }
            // alle exportierten Vorg�nge als Erledigt markieren
            $this->debugAusgabe('Schreibe exportierte Vorgaenge in der VERSEXPORTHIST weg...', 10);

            $this->_SQL = 'INSERT INTO versexporthist (veh_filid,veh_wanr,veh_versnr,veh_datensatz,veh_datumexport) ';
            $this->_SQL .= ' SELECT vek_filid, vek_wanr, vek_versnr, vek_datensatz, sysdate ';
            $this->_SQL .= ' FROM versexport';
            $this->_SQL .= ' WHERE vek_exportiert = 1';

            $this->_DB->Ausfuehren($this->_SQL, '', true);

            $this->debugAusgabe('Leere Tabelle versexport wieder...', 10);

            $this->_SQL = " DELETE FROM versexport ";
            $this->_SQL .= " WHERE vek_exportiert = 1";

            $this->_DB->Ausfuehren($this->_SQL, '', true);
        }
        catch(Exception $ex)
        {
            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');
        }
        $this->SchreibeModulStatus('ExportiereVorg�nge', 'Ende: ExportiereVorgaenge');
        $this->debugAusgabe('Ende: ExportiereVorgaenge...', 10);
    }

    public function RueckschreibenDWH()
    {
        $this->SchreibeModulStatus('RueckschreibenDWH', 'Start: RueckschreibenDWH...');
        $this->debugAusgabe("Start: RueckschreibenDWH", 10);

        try
        {
            $this->SQL = 'SELECT vvs_key,vvs_filid,vvs_wanr,vvs_schadenkz,vvs_vorgangnr,vvs_kfzkennz,vvs_export_dwh ';
            $this->SQL .= ' FROM versvorgangsstatus ';
            $this->SQL .= ' WHERE vvs_export_dwh <> 0';

            $rsVVS = $this->_DB->RecordSetOeffnen($this->SQL);

            while(!$rsVVS->EOF())
            {
                $this->SchreibeModulStatus('RueckschreibenDWH', 'Daten ins DWH schreiben');

                $this->_SQL = 'SELECT filnr, wanr ';
                $this->_SQL .= ' FROM dwh.wa_glas@DWH ';
                $this->_SQL .= ' WHERE filnr = ' . $this->_DB->WertSetzen('VVS', 'N0', $rsVVS->FeldInhalt('VVS_FILID'));
                $this->_SQL .= ' AND wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $rsVVS->FeldInhalt('VVS_WANR'));

                if($this->_DB->ErmittleZeilenAnzahl($this->_SQL, $this->_DB->Bindevariablen('VVS', true)) == 0)
                {
                    // Werkstattauftrag noch nicht in Tabelle vorhanden (INSERT)

                    $this->_SQL = 'INSERT INTO dwh.wa_glas@DWH (filnr,wanr,vorgangsnr,kfz_kennz,schadenkz) ';
                    $this->_SQL .= 'VALUES ( ';
                    $this->_SQL .= $this->_DB->WertSetzen('VVS', 'N0', $rsVVS->FeldInhalt('VVS_FILID')) . ',';
                    $this->_SQL .= $this->_DB->WertSetzen('VVS', 'N0', $rsVVS->FeldInhalt('VVS_WANR')) . ',';
                    $this->_SQL .= $this->_DB->WertSetzen('VVS', 'T', $rsVVS->FeldInhalt('VVS_VORGANGNR')) . ',';
                    $this->_SQL .= $this->_DB->WertSetzen('VVS', 'T', $rsVVS->FeldInhalt('VVS_KFZKENNZ')) . ',';
                    $this->_SQL .= $this->_DB->WertSetzen('VVS', 'T', $rsVVS->FeldInhalt('VVS_SCHADENKZ'));
                    $this->_SQL .= ')';

                    $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->Bindevariablen('VVS', true));
                }
                else
                {
                    // Werkstattauftrag bereits in Tabelle vorhanden (UPDATE)

                    $this->_SQL = 'UPDATE dwh.wa_glas@DWH SET ';
                    $Bedingung = '';
                    if($rsVVS->FeldInhalt('VVS_EXPORT_DWH') & 1)
                    {
                        $Bedingung .= ',SCHADENKZ = ' . $this->_DB->WertSetzen('VVS', 'T', $rsVVS->FeldInhalt('VVS_SCHADENKZ'));
                    }
                    if($rsVVS->FeldInhalt('VVS_EXPORT_DWH') & 2)
                    {
                        $Bedingung .= ',VORGANGSNR = ' . $this->_DB->WertSetzen('VVS', 'T', $rsVVS->FeldInhalt('VVS_VORGANGNR'));
                    }
                    if($rsVVS->FeldInhalt('VVS_EXPORT_DWH') & 4)
                    {
                        $Bedingung .= ',KFZ_KENNZ = ' . $this->_DB->WertSetzen('VVS', 'T', $rsVVS->FeldInhalt('VVS_KFZKENNZ'));
                    }

                    $this->_SQL .= substr($Bedingung, 1);
                    $this->_SQL .= ' WHERE filnr = ' . $this->_DB->WertSetzen('VVS', 'N0', $rsVVS->FeldInhalt('VVS_FILID'));
                    $this->_SQL .= ' AND wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $rsVVS->FeldInhalt('VVS_WANR'));

                    $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->Bindevariablen('VVS', true));
                }

                $this->_SQL = 'UPDATE versvorgangsstatus ';
                $this->_SQL .= ' SET vvs_export_dwh = 0 ';
                $this->_SQL .= ' WHERE vvs_key = ' . $this->_DB->WertSetzen('VVS', 'N0', $rsVVS->FeldInhalt('VVS_KEY'));

                $this->_DB->Ausfuehren($this->_SQL, '', false, $this->_DB->Bindevariablen('VVS', true));

                $rsVVS->DSWeiter();
            }

            $this->debugAusgabe("Start: SchreibeParkschaeden in DWH", 10);

            $SQL = 'merge INTO dwh.vers_smart_repair@dwh vsr USING';
            $SQL .= ' (SELECT vvs.vvs_filid,';
            $SQL .= '   vvs.vvs_wanr,';
            $SQL .= '   vvs.vvs_versnr,';
            $SQL .= '   vvs.vvs_schadenkz,';
            $SQL .= '   vek.umsatz_gesamt                        AS BETRAG_KASSE,';
            $SQL .= '   (vek.umsatz_gesamt-vkd.vkd_preis_brutto) AS AUSBUCHUNGSBETRAG,';
            $SQL .= '   vek.umsatz_gesamt,';
            $SQL .= '   vek.umsatz_kunde,';
            $SQL .= '   vek.umsatz_vers,';
            $SQL .= '   veb.datum';
            $SQL .= ' FROM v_vers_exp_kopf vek';
            $SQL .= ' INNER JOIN versvorgangsstatus vvs';
            $SQL .= ' ON vek.filid = vvs.vvs_filid';
            $SQL .= ' AND vek.wanr = vvs.vvs_wanr';
            $SQL .= ' INNER JOIN versversicherungen vve';
            $SQL .= ' ON vve.vve_versnr = vvs.vvs_versnr';
            $SQL .= ' INNER JOIN VERSKONDITIONEN vkd';
            $SQL .= ' ON vkd.vkd_vve_versnr = vvs.vvs_versnr';
            $SQL .= ' AND vve.vve_versnr    = vkd.vkd_vve_versnr';
            $SQL .= ' AND vkd.vkd_artnr     = \'1ESXXX\'';
            $SQL .= ' AND vkd.vkd_gueltig_ab <= sysdate';
            $SQL .= ' AND vkd.vkd_gueltig_bis >= sysdate';
            $SQL .= ' INNER JOIN V_VERS_FORM_BEARBDATEN veb';
            $SQL .= ' ON veb.FILNR           = vek.FILID';
            $SQL .= ' AND veb.WANR           = vek.WANR';
            $SQL .= ' AND veb.STATUS         = \'E\'';
            $SQL .= ' WHERE vvs.vvs_vcn_key  = 128';
            $SQL .= ' AND vvs.vvs_schadenkz  = \'P\'';
            $SQL .= ' ) vs ON (vsr.vsr_filnr = vs.vvs_filid AND vsr.vsr_wanr = vs.vvs_wanr)';
            $SQL .= ' WHEN NOT MATCHED THEN';
            $SQL .= '   INSERT';
            $SQL .= '     (';
            $SQL .= '       vsr_filnr,';
            $SQL .= '       vsr_wanr,';
            $SQL .= '       vsr_vers_nr,';
            $SQL .= '       vsr_betrag_kasse,';
            $SQL .= '       vsr_ausbuchung,';
            $SQL .= '       vsr_user,';
            $SQL .= '       vsr_userdat,';
            $SQL .= '       vsr_verclient,';
            $SQL .= '       vsr_schadenkz,';
            $SQL .= '       vsr_umsatz_gesamt,';
            $SQL .= '       vsr_umsatz_kunde,';
            $SQL .= '       vsr_umsatz_vers,';
            $SQL .= '       vsr_datumkasse';
            $SQL .= '     )';
            $SQL .= '     VALUES';
            $SQL .= '     (';
            $SQL .= '       vs.vvs_filid,';
            $SQL .= '       vs.vvs_wanr,';
            $SQL .= '       vs.vvs_versnr,';
            $SQL .= '       vs.BETRAG_KASSE,';
            $SQL .= '       vs.AUSBUCHUNGSBETRAG,';
            $SQL .= '       \'AWIS\',';
            $SQL .= '       sysdate,';
            $SQL .= '       \'V0\',';
            $SQL .= '       vs.vvs_schadenkz,';
            $SQL .= '       vs.umsatz_gesamt,';
            $SQL .= '       vs.umsatz_kunde,';
            $SQL .= '       vs.umsatz_vers,';
            $SQL .= '       vs.datum';
            $SQL .= '     )';
            $SQL .= '     WHEN MATCHED THEN';
            $SQL .= '   UPDATE';
            $SQL .= '   SET vsr_betrag_kasse = vs.BETRAG_KASSE,';
            $SQL .= '     vsr_ausbuchung     = vs.AUSBUCHUNGSBETRAG,';
            $SQL .= '     vsr_user           = \'AWIS\',';
            $SQL .= '     vsr_userdat        = sysdate ';

            $this->_DB->Ausfuehren($SQL, '', true);

            $this->debugAusgabe("Ende: SchreibeParkschaeden in DWH", 10);
        }
        catch(Exception $ex)
        {

            echo PHP_EOL . 'versicherung_Verarbeitung: Fehler: ' . $ex->getMessage() . PHP_EOL;

            echo 'FEHLER:' . $ex->getMessage() . PHP_EOL;
            echo 'CODE:  ' . $ex->getCode() . PHP_EOL;
            echo 'Zeile: ' . $ex->getLine() . PHP_EOL;
            echo 'Datei: ' . $ex->getFile() . PHP_EOL;
            echo 'SQL: ' . $this->_DB->LetzterSQL() . PHP_EOL;

            $this->Werkzeug->EMail($this->_EMAIL_Infos, 'ERROR - VERSICHERUNG_VERARBEITUNG', $ex->getMessage(), 2, '', 'shuttle@de.atu.eu');

            die();
        }

        $this->SchreibeModulStatus('RueckschreibenDWH', 'Ende: RueckschreibenDWH...');
        $this->debugAusgabe("Ende: RueckschreibenDWH", 10);
    }

    public function KennzeichenWandeln($Kennzeichen)
    {
        $suchen = array(' ', '-', '/', '.');
        $ersetzen = array('', '', '', '');

        for($x = 0; $x < count($suchen, 0); $x++)
        {
            $Kennzeichen = str_replace($suchen[$x], $ersetzen[$x], $Kennzeichen);
        }

        return $Kennzeichen;
    }

    public function SetzeStatus($StatusNr, $FilNr = '', $WANr = '')
    {
        try
        {
            $this->_SQL = 'UPDATE versvorgangsstatus ';
            $this->_SQL .= ' SET vvs_ven_key = ' . $this->_DB->WertSetzen('VVS', 'N0', $StatusNr);

            $VVS_FILID = 0;
            $VVS_WANR = 0;

            if($FilNr == '')
            {
                $VVS_FILID = $this->_FilNr;
                $VVS_WANR = $this->_WaNr;
            }
            else
            {
                $VVS_FILID = $FilNr;
                $VVS_WANR = $WANr;
            }

            $this->_SQL .= ' WHERE vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_FILID);
            $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_WANR);;

            $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VVS', true));
        }
        catch(Exception $e)
        {
            echo('Exception:' . $e->getMessage());
            die();
        }
        catch(awisException $awis)
        {
            echo('Exception:' . $awis->getMessage() . $awis->getSQL());
            die();
        }
    }

    public function SetzeCheckStatus($StatusNr, $FilNr = '', $WANr = '')
    {
        try
        {

            $this->_SQL = 'UPDATE versvorgangsstatus ';
            $this->_SQL .= ' SET vvs_vcn_key = ' . $this->_DB->WertSetzen('VVS', 'N0', $StatusNr);

            $VVS_FILID = 0;
            $VVS_WANR = 0;

            if($FilNr == '')
            {
                $VVS_FILID = $this->_FilNr;
                $VVS_WANR = $this->_WaNr;
            }
            else
            {
                $VVS_FILID = $FilNr;
                $VVS_WANR = $WANr;
            }

            $this->_SQL .= ' WHERE vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_FILID);
            $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_WANR);;

            $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->BindeVariablen('VVS', true));
        }
        catch(Exception $e)
        {
            echo('Exception:' . $e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo('Exception:' . $awis->getMessage() . $awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }

    public function SetzeExportDWHStatus($Feldname, $FilNr = '', $WANr = '')
    {
        try
        {

            $this->_SQL = 'SELECT vvd_bit ';
            $this->_SQL .= ' FROM versvorgangsstatusexport ';
            $this->_SQL .= ' WHERE vvd_feldname' . $this->_DB->LikeOderIst($Feldname);

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL) > 0)
            {
                $rsBit = $this->_DB->RecordSetOeffnen($this->_SQL, $this->_DB->Bindevariablen('VVD', false));

                $this->_SQL = 'SELECT vvs_export_dwh ';
                $this->_SQL .= ' FROM versvorgangsstatus ';

                if($FilNr == '')
                {
                    $VVS_FILID = $this->_FilNr;
                    $VVS_WANR = $this->_WaNr;
                }
                else
                {
                    $VVS_FILID = $FilNr;
                    $VVS_WANR = $WANr;
                }

                $this->_SQL .= ' WHERE vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_FILID);
                $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_WANR);

                $rsVVSBit = $this->_DB->RecordSetOeffnen($this->_SQL, $this->_DB->Bindevariablen('VVS', true));

                if($rsVVSBit->FeldInhalt('VVS_EXPORT_DWH') & $rsBit->FeldInhalt('VVD_BIT'))
                {
                }
                else
                {
                    $this->_SQL = 'UPDATE versvorgangsstatus ';
                    $this->_SQL .= ' SET vvs_export_dwh = ' . $this->_DB->WertSetzen('VVS', 'N0', $rsBit->FeldInhalt('VVD_BIT'));
                    $this->_SQL .= ' + vvs_export_dwh ';

                    if($FilNr == '')
                    {
                        $VVS_FILID = $this->_FilNr;
                        $VVS_WANR = $this->_WaNr;
                    }
                    else
                    {
                        $VVS_FILID = $FilNr;
                        $VVS_WANR = $WANr;
                    }
                    $this->_SQL .= ' WHERE vvs_filid = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_FILID);
                    $this->_SQL .= ' AND vvs_wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $VVS_WANR);
                }

                $this->_DB->Ausfuehren($this->_SQL, '', true, $this->_DB->Bindevariablen('VVS', true));
            }
        }
        catch(Exception $e)
        {
            echo('Exception:' . $e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo('Exception:' . $awis->getMessage() . $awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }

    /**
     * Schreibt �nderungen die an Scheibenrep.Vorgaengen vorgenommen
     * wurden ins DWH zurueck.
     *
     * @param int $FilNr
     * @param int $WANr
     */
    public function SchreibeRepInDWH($FilNr = '', $WANr = '')
    {
        try
        {
            if($FilNr == '')
            {
                $FilNr = $this->_FilNr;
                $WANr = $this->_WaNr;
            }

            $SQL = 'SELECT p.filnr, p.wanr, sum(p.vk_preis * p.menge) as BETRAG_KASSE, v.vvs_versnr ';
            $SQL .= ' FROM v_vers_form_posdaten p INNER JOIN versvorgangsstatus v ';
            $SQL .= ' ON p.filnr = v.vvs_filid AND p.wanr = v.vvs_wanr ';
            $SQL .= ' WHERE filnr = ' . $this->_DB->WertSetzen('VVS', 'N0', $FilNr);
            $SQL .= ' AND wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $WANr);
            $SQL .= ' AND artnr = \'1EIN69\' ';
            $SQL .= ' GROUP BY p.filnr, p.wanr, v.vvs_versnr';

            if($this->_DB->ErmittleZeilenAnzahl($SQL, $this->_DB->Bindevariablen('VVS', false)) > 0)
            {
                $rsRepDWH = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('VVS', true));

                while(!$rsRepDWH->EOF())
                {
                    /*
	                 * pr�fen ob Vorgang schon mal in glasrep weggeschrieben wurde,
	                 * wenn ja, dann nur Betrag updaten, wenn nein dann Insert
	                 */

                    $SQL = 'SELECT filnr, wanr, vers_nr, betrag_kasse, ausbuchung ';
                    $SQL .= ' FROM dwh.glasrep@DWH ';
                    $SQL .= ' WHERE filnr = ' . $this->_DB->WertSetzen('VVS', 'N0', $FilNr);
                    $SQL .= ' AND wanr = ' . $this->_DB->WertSetzen('VVS', 'N0', $WANr);

                    if($this->_DB->ErmittleZeilenAnzahl($SQL, $this->_DB->Bindevariablen('VVS', true)) > 0)
                    {
                        $betragKasse = str_replace(",", ".", $rsRepDWH->FeldInhalt('BETRAG_KASSE'));

                        $SQL = 'UPDATE dwh.glasrep@DWH ';
                        $SQL .= ' SET betrag_kasse = ' . $betragKasse . ', ';;
                        $SQL .= ' glasrep_user = \'awis\', ';
                        $SQL .= ' glasrep_userdat = sysdate, ';
                        $SQL .= ' verclient = \'V0\', ';
                        $SQL .= ' ausb_srep = 0, ';
                        $SQL .= ' filnl_srep = 0 ';
                        $SQL .= ' WHERE filnr = ' . $this->_DB->WertSetzen('DWH', 'N0', $FilNr);
                        $SQL .= ' AND wanr = ' . $this->_DB->WertSetzen('DWH', 'N0', $WANr);
                    }
                    else
                    {
                        // Betrag_Kasse wird als Komma-getrennte Zahl geliefert was beim Insert zu Feldverschiebungen f�hren w�rde
                        // daher Komma durch Punkt ersetzen
                        $betragKasse = str_replace(",", ".", $rsRepDWH->FeldInhalt('BETRAG_KASSE'));

                        $SQL = 'INSERT INTO dwh.glasrep@DWH (filnr,wanr,vers_nr,betrag_kasse,glasrep_user,glasrep_userdat,verclient,ausb_srep,filnl_srep) ';
                        $SQL .= 'VALUES (';
                        $SQL .= $this->_DB->WertSetzen('DWH', 'N0', $rsRepDWH->FeldInhalt('FILNR')) . ', ';
                        $SQL .= $this->_DB->WertSetzen('DWH', 'N0', $rsRepDWH->FeldInhalt('WANR')) . ', ';
                        $SQL .= $this->_DB->WertSetzen('DWH', 'N0', $rsRepDWH->FeldInhalt('VVS_VERSNR')) . ', ';
                        $SQL .= $this->_DB->FeldInhaltFormat('W', $rsRepDWH->FeldInhalt('BETRAG_KASSE')) . ' , ';
                        $SQL .= '\'awis\', ';
                        $SQL .= 'sysdate, ';
                        $SQL .= '\'V0\', ';
                        $SQL .= '0, ';
                        $SQL .= '0)';
                    }
                    $this->_DB->Ausfuehren($SQL, '', true, $this->_DB->BindeVariablen('DWH', true));
                    $rsRepDWH->DSWeiter();
                }
            }
        }
        catch(Exception $e)
        {
            echo('Exception:' . $e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo('Exception:' . $awis->getMessage() . $awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }

    public function SchreibeModulStatus($Modulname, $Statustext)
    {
        try
        {
            $SQL = 'UPDATE versjobsteuerung ';
            $SQL .= ' SET vjs_modulstatus = ' . $this->_DB->WertSetzen('VJS', 'T', $Statustext);
            $SQL .= ' WHERE vjs_modul = ' . $this->_DB->WertSetzen('VJS', 'T', $Modulname);

            $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('VJS', true));
        }
        catch(Exception $e)
        {
            echo('Exception:' . $e->getMessage());
            echo $this->_DB->LetzterSQL();
            die();
        }
        catch(awisException $awis)
        {
            echo('Exception:' . $awis->getMessage() . $awis->getSQL());
            echo $this->_DB->LetzterSQL();
            die();
        }
    }

    public function SchreibeExportStatus($ExpDateiname, $Status, $Pruefsumme = '')
    {
        try
        {
            // aktuelle JobID ermitteln
            $SQL = 'SELECT * ';
            $SQL .= 'FROM versjobid';

            $rsJobID = $this->_DB->RecordSetOeffnen($SQL);

            if($Status == 'Start')
            {
                $SQL = 'INSERT INTO versexportprotokoll (vep_jobid,vep_dateiname,vep_datumstart) ';
                $SQL .= 'VALUES (';
                $SQL .= $this->_DB->WertSetzen('VEP', 'N0', $rsJobID->FeldInhalt('VJI_JOBID')) . ', ';
                $SQL .= $this->_DB->WertSetzen('VEP', 'T', $ExpDateiname) . ', ';
                $SQL .= 'sysdate )';
            }
            elseif($Status == 'Ende')
            {
                $SQL = 'UPDATE versexportprotokoll ';
                $SQL .= ' SET vep_datumende = sysdate,';
                $SQL .= ' vep_pruefsumme = ' . $this->_DB->WertSetzen('VEP', 'T', $Pruefsumme);
                $SQL .= ' WHERE vep_dateiname =' . $this->_DB->WertSetzen('VEP', 'T', $ExpDateiname);
            }

            $this->_DB->Ausfuehren($SQL, '', false, $this->_DB->Bindevariablen('VEP', true));
        }
        catch(Exception $e)
        {
            echo('Exception:' . $e->getMessage());
            die();
        }
        catch(awisException $awis)
        {
            echo('Exception:' . $awis->getMessage() . $awis->getSQL());
            die();
        }
    }

    public function CheckExportStatus($ExpDateiname)
    {
        $SQL = 'SELECT * ';
        $SQL .= ' FROM versexportprotokoll ';
        $SQL .= ' WHERE vep_dateiname = ' . $this->_DB->WertSetzen('VEP', 'T', $ExpDateiname);
        $SQL .= ' AND vep_datumende is not null';

        if($this->_DB->ErmittleZeilenAnzahl($SQL, $this->_DB->BindeVariablen('VEP', true)) == 0)
        {
            $Rueckgabe = false;
        }
        else
        {
            $Rueckgabe = true;
        }

        return $Rueckgabe;
    }

    public function ErstelleExportDatei($DateinameExport, $ExpNameTabelle)
    {
        if($this->CheckExportStatus($DateinameExport) === false)
        {
            $this->_SQL = 'SELECT ve.vek_datensatz ';
            $this->_SQL .= ' FROM versversicherungen vv INNER JOIN versexport ve ';
            $this->_SQL .= ' ON vv.vve_versnr = ve.vek_versnr ';
            $this->_SQL .= ' WHERE vv.vve_exportdatei = ' . $this->_DB->WertSetzen('VEP', 'T', $ExpNameTabelle);
            $this->_SQL .= ' ORDER BY vek_key';

            $this->SchreibeModulStatus('ExportiereVorgaenge', 'Schreibe Vers-Datei');
            $this->SchreibeExportStatus($DateinameExport, 'Start');

            if(($fd = fopen($this->_PfadExportVers . $DateinameExport, 'w')) === false)
            {
                echo $this->error_msg = "Fehler beim Erstellen der Datei";
                die();
            }

            if($this->_DB->ErmittleZeilenAnzahl($this->_SQL, $this->_DB->Bindevariablen('VEP', false)) > 0)
            {
                $rsExport = $this->_DB->RecordSetOeffnen($this->_SQL, $this->_DB->Bindevariablen('VEP', false));

                while(!$rsExport->EOF())
                {
                    fputs($fd, $rsExport->FeldInhalt('VEK_DATENSATZ') . chr(13) . chr(10));
                    $rsExport->DSWeiter();
                }
            }
            fclose($fd);
            $this->SchreibeExportStatus($DateinameExport, 'Ende', md5_file($this->_PfadExportVers . $DateinameExport));
        }
    }

    public function EntferneBuchstaben($QuellString)
    {
        $Laenge = strlen($QuellString);
        $ZielString = '';

        for($i = 0; $i < $Laenge; $i++)
        {
            $Zeichen = substr($QuellString, $i, 1);
            if(ord($Zeichen) >= 65 && ord($Zeichen) <= 90 || ord($Zeichen) >= 97 && ord($Zeichen) <= 122)
            {
            }
            else
            {
                $ZielString .= $Zeichen;
            }
        }

        return $ZielString;
    }

    public function FormatiereSchadenNr($SchadenNr)
    {
        $suchen = array(' ', '-', '/', '.');
        $ersetzen = array('', '', '', '');

        for($x = 0; $x < count($suchen, 0); $x++)
        {
            $SchadenNr = str_replace($suchen[$x], $ersetzen[$x], $SchadenNr);
        }

        if(substr($SchadenNr, 0, 2) == '07')
        {
            $SchadenNr = '07' . substr($SchadenNr, 2, 7);
        }
        else
        {
            $SchadenNr = '';
        }

        return $SchadenNr;
    }

    /**
     * Methode pr�ft ob das Schadendatum erheblich vom Filialannahmedatum abweicht (> 10 Jahre)
     * Wenn ja dann wird WA-Anlagedatum aus der Filiale zur�ckgegeben.
     *
     * @param date $SchadenDatum
     * @param int  $FilNr
     * @param int  $VorgangNr
     * @return date $SchadenDatum
     */
    public function pruefeSchadenDatum($SchadenDatum, $FilNr, $WANR)
    {
        // Grenze in Jahren (ab wieviel Jahren soll nicht das Schadendatum aus Fil
        // sondern aus Anlagedatum des WA genommen werden.
        $diffGrenze = 10;

        $SQL = 'SELECT min(a.datum) as datum ';
        $SQL .= ' FROM v_vers_form_bearbdaten a ';
        $SQL .= ' WHERE a.filnr = ' . $this->_DB->Wertsetzen('VFB', 'N0', $FilNr);
        $SQL .= ' and a.wanr = ' . $this->_DB->Wertsetzen('VFB', 'N0', $WANR);

        $rsDatum = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->BindeVariablen('VFB', true));

        $datetime1 = strtotime($SchadenDatum);
        $datetime2 = strtotime($rsDatum->FeldInhalt('DATUM'));

        $differenz = $datetime1 - $datetime2;

        if($differenz / 86400 < $diffGrenze * -365)
        {
            $SchadenDatum = $rsDatum->FeldInhalt('DATUM');
        }

        return $SchadenDatum;
    }

    public function debugAusgabe($Text = '', $Level = 500)
    {
        if($this->_DebugLevel >= $Level)
        {
            echo date('d.m.y H:i:s ') . $Text . PHP_EOL;
        }
    }

}

?>


