<?php
global $AWISBenutzer;
global $CursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('DOC','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','Uploaddatei');
$TextKonserven[]=array('Wort','DateiOeffnen');
$TextKonserven[]=array('Wort','KeineDatenVorhanden');
$TextKonserven[]=array('Wort','Typ');
$TextKonserven[]=array('Fehler','err_keineDaten');
$TextKonserven[]=array('Fehler','err_keineDatenbank');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3705 = awisBenutzerRecht($con,3705,$AWISBenutzer->BenutzerName());		// Artikelstamm
if(($Recht3705&1)==0)
{
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	die();
}

$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

$SQL = 'SELECT Dokumente.*, xbn_name FROM Dokumente';
$SQL .= ' INNER JOIN dokumentZuordnungen ON doc_key = doz_doc_key';
$SQL .= ' INNER JOIN Benutzer ON doc_xbn_key = xbn_key';
$SQL .= ' WHERE doz_xxx_key=0' . $AWIS_KEY1;
$SQL .= ' AND doz_xxx_kuerzel=\'CAD\'';
if(isset($_GET['DOCKEY']))
{
	$SQL .= ' AND doc_key=0'.intval($_GET['DOCKEY']);
}
$SQL .= ' ORDER BY doc_datum desc';
awis_Debug(1,$SQL);
$rsDOC = awisOpenRecordset($con, $SQL);
$rsDOCZeilen = $awisRSZeilen;

if(($rsDOCZeilen > 1 or isset($_GET['DOCListe'])))
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();
	
	if(($Recht3705&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Details&Seite=Dokumente&DOCKEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}
	
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Dokumente';
	$Link .= '&DOCSort=DOC_DATUM'.((isset($_GET['DOCSort']) AND ($_GET['DOCSort']=='DOC_DATUM'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_DATUM'],150,'',$Link);

	$Link = './crm_Main.php?cmdAktion=Details&Seite=Dokumente';
	$Link .= '&DOCSort=DOC_BEZEICHNUNG'.((isset($_GET['DOCSort']) AND ($_GET['DOCSort']=='DOC_BEZEICHNUNG'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'],550,'',$Link);
	
	$Link = './crm_Main.php?cmdAktion=Details&Seite=Dokumente';
	$Link .= '&DOCSort=XBN_NAME'.((isset($_GET['DOCSort']) AND ($_GET['DOCSort']=='XBN_NAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['DOC']['DOC_XBN_KEY'],180,'',$Link);

	awis_FORM_Erstelle_Liste_Ueberschrift('',30,'',$Link);
	
	awis_FORM_ZeileEnde();
	
	for($DOCZeile=0;$DOCZeile<$rsDOCZeilen;$DOCZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		if(($Recht3705&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Details&Seite=Dokumente&DOCKEY='.$rsDOC['DOC_KEY'][$DOCZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Details&Seite=Dokumente&Del='.$rsDOC['DOC_KEY'][$DOCZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($DOCZeile%2));
		
		awis_FORM_Erstelle_ListenFeld('#DOC_DATUM',$rsDOC['DOC_DATUM'][$DOCZeile],20,150,false,($DOCZeile%2),'','','D');
		awis_FORM_Erstelle_ListenFeld('#DOC_BEZEICHNUNG',$rsDOC['DOC_BEZEICHNUNG'][$DOCZeile],20,550,false,($DOCZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#XBN_NAME',$rsDOC['XBN_NAME'][$DOCZeile],20,180,false,($DOCZeile%2),'','','T');
		$Link = '/dokumentanzeigen.php?doctab=CAD&dockey='.$rsDOC['DOC_KEY'][$DOCZeile].'';
		awis_FORM_Erstelle_ListenBild('href','DOC',$Link,'/bilder/dateioeffnen_klein.png',$AWISSprachKonserven['Wort']['DateiOeffnen'],0,'',17,17,30);
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	awis_FORM_FormularStart();
	
	echo '<input name=txtDOC_KEY type=hidden value=0'.(isset($rsDOC['DOC_KEY'][0])?$rsDOC['DOC_KEY'][0]:'').'>';
	$AWIS_KEY2 = (isset($rsDOC['DOC_KEY'][0])?$rsDOC['DOC_KEY'][0]:0);
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Details&Seite=Dokumente&DOCListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsDOC['DOC_KEY'][0])?$rsDOC['DOC_USER'][0]:'')));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':(isset($rsDOC['DOC_KEY'][0])?$rsDOC['DOC_USERDAT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_DATUM'].':',150);
	awis_FORM_Erstelle_TextFeld('DOC_DATUM',(isset($rsDOC['DOC_KEY'][0])?$rsDOC['DOC_DATUM'][0]:''),10,200,($Recht3705&6),'','','','D','','',awis_PruefeDatum(date('d.m.Y')));
	if(($Recht3705&6)>0)
	{
		$CursorFeld='txtDOC_DATUM';
	}
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEZEICHNUNG'].':',150);
	awis_FORM_Erstelle_TextFeld('DOC_BEZEICHNUNG',(isset($rsDOC['DOC_KEY'][0])?$rsDOC['DOC_BEZEICHNUNG'][0]:''),40,400,($Recht3705&6));
	awis_FORM_ZeileEnde();
	
	if($AWIS_KEY2==0)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'].':',150);
		awis_FORM_Erstelle_DateiUpload('DOC',200,50);
		awis_FORM_ZeileEnde();
	}
	else 
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Uploaddatei'].':',150);
		$Link = '/dokumentanzeigen.php?doctab=CAD&dockey='.$rsDOC['DOC_KEY'][0].'';
		awis_FORM_Erstelle_ListenBild('href','DOC',$Link,'/bilder/dateioeffnen_klein.png',$AWISSprachKonserven['Wort']['DateiOeffnen'],0,'',17,17,50);
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Typ'].':',100);
		awis_FORM_Erstelle_TextLabel($rsDOC['DOC_ERWEITERUNG'][0],100);
		awis_FORM_ZeileEnde();
	}

	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['DOC']['DOC_BEMERKUNG'].':',150);
	awis_FORM_Erstelle_Textarea('DOC_BEMERKUNG',(isset($rsDOC['DOC_KEY'][0])?$rsDOC['DOC_BEMERKUNG'][0]:''),400,80,3,($Recht3705&6));
	awis_FORM_ZeileEnde();
	

	awis_FORM_FormularEnde();
}

?>