<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;
global $FelderNew;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
	
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	if(isset($_POST['txtHRT_KEY']))
	{
		if($FelderNew != '')
		{	
		    
			$SQL ='insert into hrtaetzuord ';
			$SQL .=' (htz_hrt_key,htz_firmierung,htz_user,htz_userdat)';
			$SQL .=' values';
			$SQL .='('.$_POST['txtHRT_KEY'];
			$SQL .=','.$_POST['txtHRT_FIRM'];
			$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .=',sysdate)';
			$DB->Ausfuehren($SQL,'',true);
			
			$SQL ='update hrtaetigkeiten ';
			$SQL .= 'set hrt_USER=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', hrt_USERDAT=SYSDATE';
			$SQL .=' where hrt_key='.$_POST['txtHRT_KEY'];
			$DB->Ausfuehren($SQL,'',true);
		}
		elseif(isset($_POST['cmdSpeichern_x']))
		{
			if($_POST['txtHRT_KEY'] <> '-1')
			{
			   
				if($_POST['txtHRT_BEZEICHNUNG'] != '')
				{
				    
					$SQL ='update hrtaetigkeiten ';
					$SQL .= 'set hrt_SUCH='.$DB->FeldInhaltFormat('T',$_POST['txtHRT_BEZEICHNUNG']);
					if(isset($_POST['txtCHK_AKTIV']))
					{
						$SQL .= ', HRT_AKTIV=1';
					}
					else
					{
						$SQL .= ', HRT_AKTIV=0';
					}
					if(isset($_POST['txtCHK_AUSBILD']))
					{
						$SQL .= ', HRT_AUSBILD=1';
					}
					else
					{
						$SQL .= ', HRT_AUSBILD=0';
					}
					$SQL .= ', hrt_USER=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', hrt_USERDAT=SYSDATE';
					$SQL .=' where hrt_key='.$_POST['txtHRT_KEY'];
					$DB->Ausfuehren($SQL,'',true);
				}
				else
				{
					$Fehler = 2;
					$AWIS_KEY1 = $_POST['txtHRT_KEY'];
				}
			}
			else 
			{
				$NeuAnlage = 1;
				
				if($_POST['txtHRT_BEZEICHNUNG'] != '')
				{	
				    
					$SQL = 'INSERT INTO hrtaetigkeiten';
					$SQL .= '(hrt_such,hrt_bezmaenn,hrt_bezweib, hrt_aktiv,hrt_user,hrt_userdat';
					$SQL .= ')VALUES (';
					$SQL .= $DB->FeldInhaltFormat('T',$_POST['txtHRT_BEZEICHNUNG']).',';
					$SQL .= $DB->FeldInhaltFormat('T',$_POST['txtHRT_BEZEICHNUNG']).',';
					$SQL .= $DB->FeldInhaltFormat('T',$_POST['txtHRT_BEZEICHNUNG']).',';
					$SQL .= '1,';
					$SQL .= '\'' .$AWISBenutzer->BenutzerName() . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';
					
			
					$DB->Ausfuehren($SQL);
					
					
					$SEQSQL = 'select seq_hrt_key.CURRVAL AS Key from dual';
					$rsSEQ = $DB->RecordSetOeffnen($SEQSQL);
					$AWIS_KEY1 = $rsSEQ->FeldInhalt('KEY');
				}
				else 
				{
					$Fehler = 2;
					$AWIS_KEY1 = -1;
				}
				
				
			}
		}
	}
}
catch (awisException $ex)
{
	
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_HRT_SUCH) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Hinweis = '"'.$_POST['txtHRT_BEZEICHNUNG'].'" befindet sich schon in der Datenbank.';
		if(isset($_POST['txtHRT_KEY']))
		{
			$AWIS_KEY1 = $_POST['txtHRT_KEY'];
		}
		$Form->Hinweistext($Hinweis);
	}
	elseif($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.PK_HRT_KEY) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Form->Hinweistext('Der einzuf�gende Datensatz befindet sich schon in der Datenbank');
	}
	elseif($ex->getMessage() == 'ORA-00001: Unique Constraint (AWIS.UID_HRT_FIRM) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Form->Hinweistext('Die Firmierungszuordnung existiert bereits in der Datenbank');
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}


?>
