<?php
//require_once('kdtelefonie_funktionen.inc');
require_once 'awisMailer.inc';


global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('PEI','err_Bemerkung');
$TextKonserven[]=array('PEI','err_CheckTyp');

try
{
	
	$AWISBenutzer = awisBenutzer::Init();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	
	if($FelderNew != '')
	{	
		$SQL ='insert into sachbearbeiterzuord ';
		$SQL .=' (SB_KEY,SP_KEY)';
		$SQL .=' values';
		$SQL .='('.$_POST['txtID'];
		$SQL .=','.$_POST['txtBES_ART'].')';
		$DBSCHAD->Ausfuehren($SQL,'',true);
			
		$SQL ='update Sachbearbeiter ';
		$SQL .= 'set SBUSER=\''.$AWISBenutzer->BenutzerName().'\'';
		$SQL .= ', SBUSERDAT=SYSDATE';
		$SQL .=' where ID='.$_POST['txtID'];
		$DBSCHAD->Ausfuehren($SQL,'',true);
		
	}
	else if(isset($_POST['txtID']))
	{
			if($_POST['txtID'] == '-1')
			{	
				if(isset($_POST['txtART']) and $_POST['txtART'] == '0')
				{
					$SQL ='insert into sachbearbeiter ';
					$SQL .=' (ID,SBNAME,SBVORNAME,SBKUERZEL,SCHADENBESCHWERDEN,AUSGESCHIEDEN,SBAKTIVFIL,SBART,AWIS_USER,SBUSER,SBUSERDAT)';
					$SQL .=' values';
					$SQL .= '(seq_sachbearbeiter_id.nextval';
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_NAME']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_VORNAME']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_KUERZEL']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_GUELTIG']);
					if(isset($_POST['txtCHK_AKTIV']))
					{
						$SQL .= ',0';
					}
					else
					{
						$SQL .= ',1';
					}
					if(isset($_POST['txtCHK_AKTIVFIL']))
					{
						$SQL .= ',1';
					}
					else
					{
						$SQL .= ',0';
					}
					$SQL .= ',0';
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_AWISUSR']);
					$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .=',sysdate)';
						
					$DBSCHAD->Ausfuehren($SQL,'',true);
					
					$SEQSQL = 'select seq_sachbearbeiter_id.CURRVAL AS Key from dual';
					$rsSEQ = $DBSCHAD->RecordSetOeffnen($SEQSQL);
					$AWIS_KEY1 = $rsSEQ->FeldInhalt('KEY');
					$Form->Hinweistext($AWISSprachKonserven['BES']['HINWEIS_SPOPT']);
				}
				else if (isset($_POST['txtART']) and $_POST['txtART'] == '1')
				{					
					$SQL ='insert into sachbearbeiter ';
					$SQL .=' (ID,SBNAME,SBVORNAME,SBKUERZEL,SCHADENBESCHWERDEN,AUSGESCHIEDEN,SBAKTIVFIL,SBART,SBUSER,SBUSERDAT)';
					$SQL .=' values';
					$SQL .= '(seq_sachbearbeiter_id.nextval';
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_NAME_S']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_VORNAME_S']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_KUERZEL_S']);
					$SQL .=','.$DB->FeldInhaltFormat('T',$_POST['txtBES_GUELTIG_S']);
					if(isset($_POST['txtCHK_AKTIV_S']))
					{
						$SQL .= ',0';
					}
					else
					{
						$SQL .= ',1';
					}
					if(isset($_POST['txtCHK_AKTIVFIL_S']))
					{
						$SQL .= ',1';
					}
					else
					{
						$SQL .= ',0';
					}
					$SQL .= ',1';
					$SQL .=',\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .=',sysdate)';
					
					$DBSCHAD->Ausfuehren($SQL,'',true);			
					$Form->ZeileStart();
					$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_NEUANLAGE']);
				}
			}
			else if(isset($_POST['txtART']) and $_POST['txtART'] == '0')
			{
					$SQL ='update sachbearbeiter ';
					$SQL .= 'set SBNAME='.$DB->FeldInhaltFormat('T',$_POST['txtBES_NAME']);
					$SQL .= ',SBVORNAME='.$DB->FeldInhaltFormat('T',$_POST['txtBES_VORNAME']);
					$SQL .= ',SBKUERZEL='.$DB->FeldInhaltFormat('T',$_POST['txtBES_KUERZEL']);
					$SQL .= ',SCHADENBESCHWERDEN='.$DB->FeldInhaltFormat('T',$_POST['txtBES_GUELTIG']);
					$SQL .= ',AWIS_USER='.$DB->FeldInhaltFormat('T',$_POST['txtBES_AWISUSR']);
					if(isset($_POST['txtCHK_AKTIV']))
					{
						$SQL .= ', AUSGESCHIEDEN=0';
					}
					else
					{
						$SQL .= ', AUSGESCHIEDEN=1';
					}
					if(isset($_POST['txtCHK_AKTIVFIL']))
					{
						$SQL .= ', SBAKTIVFIL=1';
					}
					else
					{
						$SQL .= ', SBAKTIVFIL=0';
					}
					$SQL .= ', SBUSER=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', SBUSERDAT=SYSDATE';
					$SQL .=' where ID='.$_POST['txtID'];
					$DBSCHAD->Ausfuehren($SQL,'',true);
					$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_SPEICHERN']);
		
			}
			else if(isset($_POST['txtART']) and $_POST['txtART'] == '1')
			{
				$SQL ='update sachbearbeiter ';
				$SQL .= 'set SBNAME='.$DB->FeldInhaltFormat('T',$_POST['txtBES_NAME_S']);
				$SQL .= ',SBVORNAME='.$DB->FeldInhaltFormat('T',$_POST['txtBES_VORNAME_S']);
				$SQL .= ',SBKUERZEL='.$DB->FeldInhaltFormat('T',$_POST['txtBES_KUERZEL_S']);
				$SQL .= ',SCHADENBESCHWERDEN='.$DB->FeldInhaltFormat('T',$_POST['txtBES_GUELTIG_S']);
				if(isset($_POST['txtCHK_AKTIV_S']))
				{
					$SQL .= ', AUSGESCHIEDEN=0';
				}
				else
				{
					$SQL .= ', AUSGESCHIEDEN=1';
				}
				if(isset($_POST['txtCHK_AKTIVFIL_S']))
				{
					$SQL .= ', SBAKTIVFIL=1';
				}
				else
				{
					$SQL .= ', SBAKTIVFIL=0';
				}
				$SQL .= ', SBUSER=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', SBUSERDAT=SYSDATE';
				$SQL .=' where ID='.$_POST['txtID'];
				$DBSCHAD->Ausfuehren($SQL,'',true);		
				$Form->Hinweistext($AWISSprachKonserven['BES']['MSG_SPEICHERN']);
			}
	}
}
catch (awisException $ex)
{
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (SCHADDEV09.IDX_SBKUERZEL) verletzt') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Hinweis = 'Das K�rzel befindet sich schon in der Datenbank.';
				
		if(isset($_POST['txtHRT_KEY']))
		{
			$AWIS_KEY1 = $_POST['txtHRT_KEY'];
		}
		$Form->Hinweistext($Hinweis);
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
		$Form->DebugAusgabe(1,$ex->getSQL());
	}
}
catch (Exception $ex)
{
	if($ex->getMessage() == 'ORA-00001: Unique Constraint (SCHADDEV09.IDX_SBKUERZEL) verletzt.') // F5 Problem als Hinweistext ausgeben.
	{
		$Fehler = 1;
		$Hinweis = 'Das K�rzel befindet sich schon in der Datenbank.';
	
		if(isset($_POST['txtHRT_KEY']))
		{
			$AWIS_KEY1 = $_POST['txtHRT_KEY'];
		}
		$Form->Hinweistext($Hinweis);
	}
	else
	{
		$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	}
}


?>
