<?php

require_once("card/card_db.inc.php");			// DB
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

if($intZeitmessung)
{

	$dblZeitgrenze = 0;  //Ladezeit in Sekunden ab welcher eine Eintragung in die Datenbank erfolgt
	
	if(isset($strBenutzer))
	{
		$strSql = "insert into zeit select seq_zeit_id.nextval,$dblServer,$dblDatenbank,$dblNetz,$dblLaden,$dblGesamt,'".
		$strDatei."','".$strBenutzer."',sysdate,'".$strEreignis."' from dual where $dblGesamt>$dblZeitgrenze";
		
		abfrage("ora",$strSql);
	}
	
	?>
	
	
	
	
	<html>
	<head>
	<title>Ladezeit</title>
	<script language="JavaScript">
	var datSendezeit;
	var datStartzeit;
	var datEndezeit;
	var zeitserver;
	var zeitdatenbank;
	var zeitnetz;
	var zeitladen;
	var zeitgesamt;
	var dateiname;
	var ereignis;
	
	//Formatiert 3 Nachkommastellen bei �bergabe eines Double-Wertes
	
	function dreinachkomma(dblWert)
	{
		var strWert = dblWert.toString(10);
		
		var arr = strWert.split(".");
		if(typeof(arr[1])=='undefined')
		{
			if(typeof(arr[0])=='NaN')
				return '0.000';
			else
				return arr[0]+".000";
		}
		else
		{
			var rueck = arr[1].substr(0,3);
			var anz = 3-rueck.length;
			
			
			for(i=0;i<anz;i++)
			{
				rueck+="0";
			}
			return arr[0]+"."+rueck;
		}
	}
	
	function init()
	{
		delete datSendezeit;
		delete datStartzeit;
		delete datEndezeit;
		zeitserver=0;
		zeitdatenbank=0;
		zeitnetz=0;
		zeitladen=0;
		zeitgesamt=0;
		dateiname='';
	}
	
	function sendezeit(strEreignis)	//Zeit, wann Seite versendet worden ist, als Parameter kann ein Text zur Ereignisbeschreibung �bergeben werden
	{
		if(typeof(strEreignis)=='undefined')
			strEreignis='nicht angegeben';
		datSendezeit = new Date().getTime();
		ereignis = strEreignis;
	}
	
	function startzeit()	//Zeit, wann Seitenaufbau beginnt
	{
		datStartzeit = new Date().getTime();
	}
	
	function endezeit()		//Wird aufgerufen, wenn Seitenaufbau beendet ist
	{
		datEndezeit = new Date().getTime();
		
		if(typeof(datSendezeit)!='undefined')
		{
			zeitgesamt = (datEndezeit - datSendezeit)/1000;
			zeitnetz = (datStartzeit - datSendezeit-zeitserver*1000)/1000;
		}
		
		zeitladen = (datEndezeit - datStartzeit)/1000;
	
		<?php
		if($intZeitmessungAusgabe)	// wird in card_allgparam.inc.php gesetzt
		{
		?>
			alert("Der Seitenaufbau von\n\n"+dateiname+"\n\nlieferte folgende Werte:\n\nEreignis: "+unescape(ereignis)+
			"\nServerzeit: "+dreinachkomma(zeitserver-zeitdatenbank)+
			"\nDatenbankzeit: "+dreinachkomma(zeitdatenbank)+
			"\nNetzzeit: "+dreinachkomma(zeitnetz)+
			"\nLadezeit: "+dreinachkomma(zeitladen)+
			"\nGesamtzeit: "+dreinachkomma(zeitgesamt));
		<?php } ?>
		
		location.href="<?php echo $PHP_SELF; ?>?dblServer="+dreinachkomma(zeitserver-zeitdatenbank)+
		"&dblDatenbank="+dreinachkomma(zeitdatenbank)+
		"&dblNetz="+dreinachkomma(zeitnetz)+
		"&dblLaden="+dreinachkomma(zeitladen)+
		"&dblGesamt="+dreinachkomma(zeitgesamt)+
		"&strDatei="+dateiname+
		"&strBenutzer=<?php echo $PHP_AUTH_USER; ?>"+
		"&strEreignis="+ereignis;
	}
	</script>
	</head>
	<body onload="init();"></body>
	</html>
<?php
}
?>