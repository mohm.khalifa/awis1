<?php
global $RechteStufe;
global $awisRSZeilen;


if($RechteStufe==0)
{
	awisLogoff($con);
	die();
}


$comcon = awisLogonComCluster('DE',false);

if(($RechteStufe&8)==8)		// Rechte f�r ALLE Preislisten?
{
	$SQL = 'SELECT *';
	$SQL .= ' FROM EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB WHERE STATUS=1';
	$SQL .= ' ORDER BY BEZEICHNUNG';
}
else						// Einzelne Preislisten
{
	$SQL = 'SELECT *';
	$SQL .= ' FROM EXPERIAN_ATU.EXP_PREISLISTENBEZEICHNUNG_TAB';
	$SQL .= ' INNER JOIN  EXPERIAN_ATU.PLP_PreisListenBenutzer ON ID_FPLB=PUL_ID_FPLB AND PUL_XBN_KEY='.awisBenutzerID();
	$SQL .= '  WHERE STATUS=1';
	$SQL .= ' ORDER BY BEZEICHNUNG';
}


$rsPLI = awisOpenRecordset($comcon,$SQL);
$rsPLIZeilen = $awisRSZeilen;

echo '<form name=frmPreislisten action=./preislisten_Main.php?cmdAktion=Details method=POST>';

echo '<table class=Datentabelle border=1 width=100%>';

echo '<tr><td class=FeldBez>ID</td>';
echo '<td class=FeldBez>Bezeichnung</td>';
echo '<td class=FeldBez>Typ</td>';
echo '<td class=FeldBez>Status</td>';
echo '<td class=FeldBez>Status�nderung</td>';
echo '<td class=FeldBez>G�ltig vom</td>';
echo '<td class=FeldBez>G�ltig bis</td>';
echo '<td class=FeldBez>Transaktion</td>';
echo '<td class=FeldBez>Aktion</td>';

echo '</tr>';
for($Zeile=0;$Zeile<$rsPLIZeilen;$Zeile++)
{

	echo '<tr>';

	echo '<td id=TabellenZeileWeiss>'. $rsPLI['ID_FPLB'][$Zeile] . '</td>';
	echo '<td id=TabellenZeileWeiss><b>'. $rsPLI['BEZEICHNUNG'][$Zeile] . '</b></td>';


	// Zeit anzeigen!
	awisExecute($comcon, "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.RRRR HH24:MI'");



	//**************************************
	// Zu �bertragende Version
	//**************************************
	echo '<td id=TabellenZeileWeiss title="Status der neu einzuspielenden Liste">NEU</td>';


	$SQL = 'SELECT PST_ID, PST_STATUS, PTS_USERDAT, PTS_TRANSAKTIONSID, PST_BEMERKUNG, ARBEITSSTATUS, PTS_ID_FPLB, PTS_BEMERKUNG';
	$SQL .= ' ,(SELECT MAX(PLT_GUELTIG_VON) FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_TRANSAKTIONSID=PLP.PTS_TRANSAKTIONSID) AS GueltigVon';
	$SQL .= ' ,(SELECT MAX(PLT_GUELTIG_BIS) FROM EXPERIAN_ATU.PLP_FESTPREISLISTEN WHERE PLT_TRANSAKTIONSID=PLP.PTS_TRANSAKTIONSID) AS GueltigBis';
	$SQL .= ' FROM EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS PLP';
	$SQL .= ' LEFT OUTER JOIN EXPGW.EXPGW_EXPORT ON PTS_TRANSAKTIONSID=TRANSAKTIONSID';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_STATUSARTEN ON PTS_PST_ID = PST_ID';
	$SQL .= ' AND PTS_ID_FPLB='.$rsPLI['ID_FPLB'][$Zeile].'';
	$SQL .= ' WHERE (PTS_PST_ID <> 42 AND ARBEITSSTATUS <> \'T\')';
	$SQL .= ' ORDER BY  PTS_TRANSAKTIONSID DESC';

	$rsPTS = awisOpenRecordset($comcon,$SQL);
	echo '<td  id=TabellenZeileWeiss title=\''.$rsPTS['PST_BEMERKUNG'][0].'\'>';
	echo ($rsPTS['PST_STATUS'][0]==''?'::keine Daten geladen::':($rsPTS['PST_STATUS'][0]=='Offen'?'<span style="background-color:yellow;color:green;">Offen</span>':$rsPTS['PST_STATUS'][0])) . '</td>';
	echo '<td id=TabellenZeileWeiss>' . $rsPTS['PTS_USERDAT'][0] . '</td>';
	echo '<td id=TabellenZeileWeiss>' . substr($rsPTS['GUELTIGVON'][0],0,10) . '</td>';
	echo '<td id=TabellenZeileWeiss>' . substr($rsPTS['GUELTIGBIS'][0],0,10) . '</td>';
	echo '<td id=TabellenZeileWeiss title=\''.$rsPTS['PTS_BEMERKUNG'][0].'\'>' . $rsPTS['PTS_TRANSAKTIONSID'][0] . '</td>';

	echo '<td id=TabellenZeileWeiss>';
	switch ($rsPTS['PST_ID'][0])
	{
		case 0:			// Import l�uft
			echo '&nbsp;';
			break;
		case 1:			// Offen
		case 10:		// Pr�fung l�uft
		case 11:		// Pr�fung unvollst�ndig
			print "&nbsp;<img title='Pr�efung starten' src=/bilder/schraubenschluessel.png name=cmdPruefung accesskey=� onclick=window.open('./preislisten_PruefungDurchfuehren.php?TID=" . $rsPTS['PTS_TRANSAKTIONSID'][0] . "','Preislistenpr�fung','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');>";
			break;
		case 15:		// Pr�fung abgeschlossen
			print "&nbsp;<input type=image title='Details untersuchen' src=/bilder/sc_suche2.png name=cmdDetails_" . $rsPLI['ID_FPLB'][$Zeile] . ">";
			print "&nbsp;<img title='Pr�efung starten' src=/bilder/schraubenschluessel.png name=cmdPruefung accesskey=� onclick=window.open('./preislisten_PruefungDurchfuehren.php?TID=" . $rsPTS['PTS_TRANSAKTIONSID'][0] . "','Preislistenpr�fung','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');>";
			break;
		case 95:		// Gesperrt
		case 40:		// Freigabe
			print "&nbsp;<input type=image title='Details untersuchen' src=/bilder/sc_suche2.png name=cmdDetails_" . $rsPLI['ID_FPLB'][$Zeile] . ">";
			print "&nbsp;<img title='Daten �bertragen' src=/bilder/sc_rakete.png name=cmdDatenSenden_" . $rsPTS['PTS_ID_FPLB'][$Zeile] . " onclick=window.open('./preislisten_DatenUebertragen.php?FPLB=" . $rsPTS['PTS_ID_FPLB'][0] . "','Preislistenpr�fung','toolbar=no,menubar=no,dependent=yes,status=no,scrollbars=yes');>";
			break;
		case 42:		// Liste ist weg
			echo '&nbsp;';
			break;
	}
	echo '</td></tr>';
	unset($rsPTS);
	flush();

	//**************************************
	// Aktuelle Preisliste anzeigen
	//**************************************

	$SQL = ' SELECT *';
	$SQL .= ' FROM EXPGW.EXPGW_EXPORT';
	$SQL .= ' INNER JOIN EXPERIAN_ATU.PLP_TRANSAKTIONSSTATUS ON TRANSAKTIONSID = PTS_TRANSAKTIONSID';
	$SQL .= ' AND PTS_ID_FPLB='.$rsPLI['ID_FPLB'][$Zeile].'';
	$SQL .= ' LEFT OUTER JOIN EXPERIAN_ATU.PLP_PREISLISTENPARAMETER ON PFP_ID_FPLB = PTS_ID_FPLB AND PFP_Parameter = \'ABLAUFWARNUNG\'';
	$SQL .= ' WHERE TRANSAKTIONSID<>0' . $rsPTS['PTS_TRANSAKTIONSID'][0]; // Die vorherige Version!!
	$SQL .= ' ORDER BY TRANSAKTIONSID DESC';
awis_Debug(1,$SQL);
flush();
	$rsPTS = awisOpenRecordset($comcon,$SQL);

awis_Debug(1,$awisRSZeilen);
flush();
	echo '<tr>';
	echo '<tr><td colspan=2>&nbsp;</td>';
	echo '<td id=TabellenZeileGrau title="Status der zuletzt eingespielten Liste">AKT</td>';
	echo '<td id=TabellenZeileGrau>' . DFUE_Status($rsPTS['ARBEITSSTATUS'][0]) . '</td>';
	echo '<td id=TabellenZeileGrau>' . $rsPTS['PTS_USERDAT'][0] . '</td>';
	echo '<td id=TabellenZeileGrau>' . substr($rsPTS['PTS_GUELTIG_VON'][0],0,10) . '</td>';
	$AblaufWarnung = mktime(0,0,0,substr($rsPTS['PTS_GUELTIG_BIS'][0],3,2),substr($rsPTS['PTS_GUELTIG_BIS'][0]-$rsPTS['PFP_WERT'][0],0,2),substr($rsPTS['PTS_GUELTIG_BIS'][0],6,4));
	$Ablauf = mktime(0,0,0,substr($rsPTS['PTS_GUELTIG_BIS'][0],3,2),substr($rsPTS['PTS_GUELTIG_BIS'][0],0,2),substr($rsPTS['PTS_GUELTIG_BIS'][0],6,4));
//awis_Debug(1,$AblaufWarnung,$Ablauf,date('d.m.Y',$AblaufWarnung),date('d.m.Y',$Ablauf));
	if(time()>=$AblaufWarnung)
	{
		echo '<td id=TabellenZeileGrau><span class=HinweisText style="text-decoration:blink;">' . substr($rsPTS['PTS_GUELTIG_BIS'][0],0,10) . '</span></td>';
	}
	else
	{
		echo '<td id=TabellenZeileGrau>' . substr($rsPTS['PTS_GUELTIG_BIS'][0],0,10) . '</td>';
	}
	echo '<td id=TabellenZeileGrau title=\''.$rsPTS['PTS_BEMERKUNG'][0].'\'>' . $rsPTS['PTS_TRANSAKTIONSID'][0] . '</td>';
	echo '<td id=TabellenZeileGrau>&nbsp;</td>';

	echo '</tr>';


	echo '<tr><td colspan=9 height=4 bgcolor=#AAAABB></td></tr>';
}

echo '</table>';
echo '</form>';


awisLogoff($comcon);



function DFUE_Status($Status)
{
	switch ($Status) {
		case 'C':
			return 'Transaktion erzeugt.';
		case 'L':
			return 'Neue Daten geladen.';
		case 'M':
			return 'Experian informiert.';
		case 'T':
			return '<font color=green>Datentransfer abgeschlossen.</font>';
		case 'E':
			return '<font color=red>Fehler.</font>';
		case 'F':
			return '<font color=red>Fehler.</font>';
		default:
			return '::unbekannt::';
	}
}
?>