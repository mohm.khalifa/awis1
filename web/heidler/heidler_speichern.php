<?php
global $AWIS_KEY1;

try{
    $Functions->_Form->DebugAusgabe(1, 'AWIS_KEY1: ' . $AWIS_KEY1);
    if(isset($_POST['sucNDK_ANSPRECHPARTNER'])) {
        $AnsprechPartner = $Functions->getAnsprechpartner($_POST['sucNDK_ANSPRECHPARTNER']);
    } else {
        $AnsprechPartner = $_POST['txtNDK_ANSPRECHPARTNER'];
    }

    if($AWIS_KEY1 == -1) { // es gibt noch keine Differenzmeldung fuer diese Filiale
        $Functions->_DB->TransaktionBegin();

        $SQL = '  INSERT INTO nvsdifferenzkopf';
        $SQL .= ' (';
        $SQL .= '    NDK_FIL_ID';
        $SQL .= '   ,NDK_DIENSTLEISTER';
        $SQL .= '   ,NDK_GRUND';
        $SQL .= '   ,NDK_ZUGESTELLT';
        $SQL .= '   ,NDK_ANSPRECHPARTNER';
        $SQL .= '   ,NDK_LIEFERSCHEINDATUM';
        $SQL .= '   ,NDK_USER';
        $SQL .= '   ,NDK_USERDAT';
        $SQL .= ' )';
        $SQL .= ' VALUES';
        $SQL .= ' (';
        $SQL .= '  ' . $Functions->_DB->WertSetzen('NDK','N0',isset($_POST['sucNDK_FIL_ID'])?$_POST['sucNDK_FIL_ID']:$_POST['txtNDK_FIL_ID']);
        $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDK', 'N0', $_POST['txtNDK_DIENSTLEISTER']);
        $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDK', 'N0', $_POST['txtNDK_MELDEGRUND']);
        $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDK', 'N0', isset($_POST['txtNDK_ZUGESTELLT'])?1:0);
        $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDK', 'T', $AnsprechPartner);
        $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDK', 'D', $_POST['txtNDK_LIEFERSCHEINDATUM']);
        $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDK', 'T', $Functions->_AWISBenutzer->BenutzerName());
        $SQL .= ' ,sysdate';
        $SQL .= ' )';

        $Functions->_DB->Ausfuehren($SQL, '', true, $Functions->_DB->Bindevariablen('NDK', true));

        $SQL = 'SELECT seq_NDK_KEY.CurrVal AS KEY FROM DUAL';
        $rsKey = $Functions->_DB->RecordSetOeffnen($SQL);
        $AWIS_KEY1 = $rsKey->FeldInhalt('KEY');

        $i = 0;

        while($i < 20 and getAtuNr($i) != '') {

            $SQL = '  INSERT INTO nvsdifferenzpos';
            $SQL .= ' (';
            $SQL .= '   NDP_NDK_KEY';
            $SQL .= '  ,NDP_AST_ATUNR';
            $SQL .= '  ,NDP_MENGE';
            $SQL .= '  ,NDP_BEMERKUNG';
            $SQL .= '  ,NDP_USER';
            $SQL .= '  ,NDP_USERDAT';
            $SQL .= ' )';
            $SQL .= ' VALUES';
            $SQL .= ' (';
            $SQL .= '  ' . $Functions->_DB->WertSetzen('NDP','T', $AWIS_KEY1);
            $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','T', getAtuNr($i));
            $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','N0', $_POST['txtNDP_MENGE_'.$i]);
            $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','T', $_POST['txtNDP_BEMERKUNG_'.$i]);
            $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','T', $Functions->_AWISBenutzer->BenutzerName());
            $SQL .= ' ,sysdate';
            $SQL .= ' )';

            $Functions->_DB->Ausfuehren($SQL, '', true, $Functions->_DB->Bindevariablen('NDP', true));

            $Functions->_Form->DebugAusgabe(1,$Functions->_DB->LetzterSQL());

            $i++;
        }

        $Functions->_DB->TransaktionCommit();

    } else { // bestehende Differenzmeldung wird geaendert
        $Functions->_DB->TransaktionBegin();

        $SQL = '  UPDATE nvsdifferenzkopf';
        $SQL .= ' SET NDK_DIENSTLEISTER = ' . $Functions->_DB->WertSetzen('NDK', 'N0', $_POST['txtNDK_DIENSTLEISTER']);
        $SQL .= '    ,NDK_GRUND = ' . $Functions->_DB->WertSetzen('NDK', 'N0', $_POST['txtNDK_MELDEGRUND']);
        $SQL .= '    ,NDK_ZUGESTELLT = ' . $Functions->_DB->WertSetzen('NDK', 'N0', isset($_POST['txtNDK_ZUGESTELLT'])?1:0);
        $SQL .= '    ,NDK_ANSPRECHPARTNER = ' . $Functions->_DB->WertSetzen('NDK', 'T', $AnsprechPartner);
        $SQL .= '    ,NDK_LIEFERSCHEINDATUM = ' . $Functions->_DB->WertSetzen('NDK', 'D', $_POST['txtNDK_LIEFERSCHEINDATUM']);
        $SQL .= '    ,NDK_USER = ' . $Functions->_DB->WertSetzen('NDK', 'T', $Functions->_AWISBenutzer->BenutzerName());
        $SQL .= '    ,NDK_USERDAT = sysdate';
        $SQL .= ' WHERE NDK_KEY = ' . $Functions->_DB->WertSetzen('NDK', 'N0', $AWIS_KEY1);

        $Functions->_DB->Ausfuehren($SQL, '', true, $Functions->_DB->Bindevariablen('NDK', true));

        $i = 0;

        while(getAtuNr($i) != '') {
            if(isset($_POST['txtNDP_KEY_'.$i])) { // eine Position ist geaendert worden
                $SQL = '  UPDATE nvsdifferenzpos';
                $SQL .= ' SET NDP_AST_ATUNR = ' . $Functions->_DB->WertSetzen('NDP', 'T', getAtuNr($i));
                $SQL .= '    ,NDP_MENGE = ' . $Functions->_DB->WertSetzen('NDP', 'N0', $_POST['txtNDP_MENGE_'.$i]);
                $SQL .= '    ,NDP_BEMERKUNG = ' . $Functions->_DB->WertSetzen('NDP', 'T', $_POST['txtNDP_BEMERKUNG_'.$i]);
                $SQL .= '    ,NDP_USER = ' . $Functions->_DB->WertSetzen('NDP', 'T', $Functions->_AWISBenutzer->BenutzerName());
                $SQL .= '    ,NDP_USERDAT = sysdate';
                $SQL .= ' WHERE NDP_KEY = ' . $Functions->_DB->WertSetzen('NDP', 'N0', $_POST['txtNDP_KEY_'.$i]);

                $Functions->_DB->Ausfuehren($SQL, '', true, $Functions->_DB->Bindevariablen('NDP', true));

                $Functions->_Form->DebugAusgabe(1, $Functions->_DB->LetzterSQL());

            } else { // eine neue Position ist dazugekommen
                $SQL = '  INSERT INTO nvsdifferenzpos';
                $SQL .= ' (';
                $SQL .= '   NDP_NDK_KEY';
                $SQL .= '  ,NDP_AST_ATUNR';
                $SQL .= '  ,NDP_MENGE';
                $SQL .= '  ,NDP_BEMERKUNG';
                $SQL .= '  ,NDP_USER';
                $SQL .= '  ,NDP_USERDAT';
                $SQL .= ' )';
                $SQL .= ' VALUES';
                $SQL .= ' (';
                $SQL .= '  ' . $Functions->_DB->WertSetzen('NDP','N0', $AWIS_KEY1);
                $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','T', getAtuNr($i));
                $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','N0', $_POST['txtNDP_MENGE_'.$i]);
                $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','T', $_POST['txtNDP_BEMERKUNG_'.$i]);
                $SQL .= ' ,' . $Functions->_DB->WertSetzen('NDP','T', $Functions->_AWISBenutzer->BenutzerName());
                $SQL .= ' ,sysdate';
                $SQL .= ' )';

                $Functions->_DB->Ausfuehren($SQL, '', true, $Functions->_DB->Bindevariablen('NDP', true));

                $Functions->_Form->DebugAusgabe(1,$Functions->_DB->LetzterSQL());
            }

            $i++;
        }

        $i = 0;

        if(isset($_POST['txtARCHIV'])) {
            while(isset($_POST['txtNDP_BEMERKUNG_'.$i])) {
                $SQL = 'UPDATE nvsdifferenzpos';
                $SQL .= ' SET NDP_BEMERKUNG = ' . $Functions->_DB->WertSetzen('NDP', 'T', $_POST['txtNDP_BEMERKUNG_'.$i]);
                $SQL .= ' WHERE NDP_KEY = ' . $Functions->_DB->WertSetzen('NDP', 'T', $_POST['txtNDP_KEY_'.$i]);

                $Functions->_DB->Ausfuehren($SQL, '', true, $Functions->_DB->Bindevariablen('NDP', true));

                $i++;
            }

        }

        $Functions->_DB->TransaktionCommit();
    }

} catch (awisException $ex) {
        $Functions->_DB->TransaktionRollback();
        $Information = '<br>Zeile: ' . $ex->getLine();
        $Information .= '<br>Info: ' . $ex->getMessage() . '<br>';
        $Information .= '<br>AWIS_KEY1: ' . $AWIS_KEY1 . '<br>';
        $Information .= '<br>SQL: ' . $ex->getSQL() . '<br>';
        ob_start();
        var_dump($_REQUEST);
        $Information .= ob_get_clean();
        $Functions->_Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
        $Functions->_Form->DebugAusgabe(1, $ex->getSQL());
} catch (Exception $ex) {
    $Information = 'Zeile: ' . $ex->getLine();
    $Information .= 'Info: ' . $ex->getMessage();
    ob_start();
    var_dump($_REQUEST);
    $Information .= ob_get_clean();
    $Functions->_Form->Fehler_Anzeigen('SpeicherFehler', $Information, 'WIEDERHOLEN', -2);
}

/**
 * Prueft wie ATU-Nr kommt und gibt diese zurueck. Wenn keine kommt, dann wird leerstring zurueckgegeben
 *
 * @param  $i Zaehler aus der Schleife
 * @return string Inhalt von txtNDP_AST_ATUNR oder sucNDP_AST_ATUNR oder wenn beides nicht gesetzt Leerstring
 */
function getAtuNr ($i) {
    if(isset($_POST['txtNDP_AST_ATUNR_'.$i])) {
        $ArtNr = $_POST['txtNDP_AST_ATUNR_'.$i];
    } elseif (isset($_POST['sucNDP_AST_ATUNR_'.$i])) {
        $ArtNr = $_POST['sucNDP_AST_ATUNR_'.$i];
    } else {
        $ArtNr = '';
    }
    return strtoupper($ArtNr);
}