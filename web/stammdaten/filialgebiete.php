<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>

<?php
/****************************************************************************************************
* 
* 	filialgebiete.php
* 
* 	Pflege der Filialgebiete
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Mai 2004
* 
****************************************************************************************************/
// Variablen
global $HTTP_GET_VARS;
global $HTTP_POST_VARS;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();
$Rechtestufe = awisBenutzerRecht($con, 603) ;
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'FilialGebiete',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

echo '<form name=frmFilialGebiete method=post action=./filialgebiete.php>';

if($HTTP_POST_VARS['cmdSpeichern_x']!='')
{
}

/****************************************
* Daten anzeigen
****************************************/

$SQL = 'SELECT * From FilialGebiete, Personal WHERE FIG_PER_NR = PER_NR(+)  ORDER BY FIG_ID';

$rsFIG = awisOpenRecordset($con, $SQL);
$rsFIGZeilen = $awisRSZeilen;

echo '<h3>Filialgebiete</h3>';
echo '<table border=1 width=500>';
echo '<tr><td id=FeldBez>Gebiet</td><td id=FeldBez>Gebietsleiter</td></tr>';

for($i=0;$i<$rsFIGZeilen;$i++)
{
	if(($Rechtestufe&4)==4)			// Hinzuf�gen
	{
	
	}
	if(($Rechtestufe&2)==2)			// Bearbeiten
	{
	
	}
	else		// Nur lesen
	{
		echo '<tr>';
		
		echo '<td>' . $rsFIG['FIG_ID'][$i] . '</td>';
		echo '<td>' . $rsFIG['PER_NACHNAME'][$i] . ', ' . $rsFIG['PER_VORNAME'][$i] . " (" . $rsFIG['FIG_PER_NR'][$i] . ')</td>';
		
		echo '</tr>';
	}
	
}

echo '</table>';




$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

$ZurueckLink = '/index.php';

echo '</form>';

print "<br><hr><input type=image alt='Zur�ck (Alt+z)' src=/bilder/zurueck.png name=cmdZurueck accesskey=z onclick=location.href='$ZurueckLink';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=hersteller','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awisLogoff($con);

?>