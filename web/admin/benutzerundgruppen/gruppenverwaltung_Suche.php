<?php
/**
 * Suchmaske f�r die Auswahl eines Personaleinsatzes
 *
 * @author Sacha Kerres
 * @copyright ATU
 * @version 20081006
 *
 *
 */
global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XBN','%');
	$TextKonserven[]=array('XBB','%');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','DatumBis');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','txt_OhneZuordnung');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_hilfe');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht25=$AWISBenutzer->HatDasRecht(25);		// Rechte des Mitarbeiters

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./gruppenverwaltung_Main.php?cmdAktion=Details>");

	/**********************************************
	* * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_XBB'));

	if(!isset($Param['SPEICHERN']))
	{
		$Param['SPEICHERN']='off';
		$Param['PBM_XBN_KEY']=$AWISBenutzer->BenutzerID();
	}

	$Form->Formular_Start();

	// Datumsbereich festlegen
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBB']['XBB_BEZ'].':',190);
	$Form->Erstelle_TextFeld('*XBB_BEZ',($Param['SPEICHERN']=='on'?$Param['XBB_BEZ']:''),30,150,true,'','','','T','L','','',10);
	$AWISCursorPosition='sucXBB_BEZ';
	$Form->ZeileEnde();

	// Auswahl kann gespeichert werden
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
	$Form->ZeileEnde();


	$Form->Formular_Ende();

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht25&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->Schaltflaeche('script', 'cmdHilfe', "onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=gruppenverwaltung&Aktion=suche','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');", '/bilder/cmd_hilfe.png', $AWISSprachKonserven['Wort']['lbl_hilfe'], 'H');

	$Form->SchaltflaechenEnde();

	if($AWISCursorPosition!='')
	{
		echo '<Script Language=JavaScript>';
		echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
		echo '</Script>';
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200810061222");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"20081��61223");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>