<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SVB','%');
	$TextKonserven[]=array('SVA','%');
	$TextKonserven[]=array('Wort','OffeneMenge');
	$TextKonserven[]=array('CAD','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','lbl_export');
	$TextKonserven[]=array('Wort','lbl_DS%');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','lbl_senden');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','wrd_AnzahlDSZeilen');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19000 = $AWISBenutzer->HatDasRecht(19000);
	$Recht19003 = $AWISBenutzer->HatDasRecht(19003);		// Datenexport
	if($Recht19000==0)
	{
	    awisEreignis(3,1000,'Zukaufrueckgaben',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_Serviceleistungen'));

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['SVB_VERTRAGSNUMMER'] = $_POST['sucSVB_VERTRAGSNUMMER'];
		$Param['SuchName'] = $_POST['sucSuchName'];
		$Param['SVB_KUNDENID'] = $_POST['sucSVB_KUNDENID'];
		$Param['SVT_KENNUNG'] = $_POST['sucSVT_KENNUNG'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_Serviceleistungen",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./serviceleistungen_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./serviceleistungen_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
	}
	elseif(isset($_POST['cmdDSZurueck_x']))
	{
		$AWIS_KEY1 = $_POST['txtSVB_KEY'];
		$SQL = 'SELECT SVB_KEY';
		$SQL .= ' FROM SVCVereinbarungen';
		$SQL .= ' WHERE SVB_KEY < :var_N0_SVB_KEY';
		$SQL .= ' ORDER BY SVB_KEY DESC';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_SVB_KEY'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsSVB = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);		
		if(!$rsSVB->EOF())
		{
			$AWIS_KEY1=$rsSVB->FeldInhalt('SVB_KEY');
		}
	}
	elseif(isset($_POST['cmdDSWeiter_x']))
	{
		$AWIS_KEY1 = $_POST['txtSVB_KEY'];
		$SQL = 'SELECT SVB_KEY';
		$SQL .= ' FROM SVCVereinbarungen';
		$SQL .= ' WHERE SVB_KEY > :var_N0_SVB_KEY';
		$SQL .= ' ORDER BY SVB_KEY ASC';
		$BindeVariablen=array();
		$BindeVariablen['var_N0_SVB_KEY'] = $DB->FeldInhaltFormat('N0',$AWIS_KEY1,false);
		$rsSVB = $DB->RecordSetOeffnen($SQL,$BindeVariablen);
		if(!$rsSVB->EOF())
		{
			$AWIS_KEY1=$rsSVB->FeldInhalt('SVB_KEY');
		}
$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1);		
	}
	elseif(isset($_GET['SVB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['SVB_KEY']);
	}
	elseif(isset($_POST['txtSVB_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_POST['txtSVB_KEY']);
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_Serviceleistungen',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY SVB_VERTRAGSNUMMER DESC';
			$Param['ORDER']='SVB_VERTRAGSNUMMER DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	/* ge�ndert: 25.07.2013
	$SQL = 'SELECT SVCVEREINBARUNGEN.*, SVA_DATENART, SVA_BEZEICHNUNG';
	//$SQL .= ', CA1.CAD_NAME1 || \', \' || CA1.CAD_ORT AS CA1_NAME1';
	$SQL .= ', (SELECT CA1.CAD_NAME1 || \', \' || CA1.CAD_ORT AS CA1_NAME1 FROM CRMAdressen CA1 WHERE SVB_CAD_KEY = CA1.CAD_KEY)AS CA1_NAME1';
	$SQL .= ', (SELECT CA2.CAD_NAME1 || \', \' || CA2.CAD_ORT AS CA2_NAME1 FROM CRMAdressen CA2 WHERE SVB_CAD_KEY = CA2.CAD_KEY)AS CA2_NAME1';
	//$SQL .= ', CA2.CAD_NAME1 || \', \' || CA2.CAD_ORT AS CA2_NAME1';
	$SQL .= ', (SELECT COUNT(*) FROM SVCTeilnehmer WHERE SVT_SVB_KEY = SVB_KEY) AS ANZTN';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM SVCVEREINBARUNGEN';
	$SQL .= ' INNER JOIN SVCARTIKEL ON SVB_SVA_KEY = SVA_KEY';
*/
	
	
	$SQL = 'SELECT v_SVCVereinbarungen.*';
	$SQL .= ', CASE WHEN SVB_DATUMBIS < TRUNC(SYSDATE) THEN 1 ELSE 0 END AS ABGELAUFEN';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM v_SVCVereinbarungen';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	
//	$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1==0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_Serviceleistungen',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= ' '.$ORDERBY;
$Form->DebugAusgabe(1,$SQL,$AWIS_KEY1,$_POST);
	

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	$rsSVB = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_Serviceleistungen',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmServiceLeistungen action=./serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=POST enctype="multipart/form-data">';

	if($rsSVB->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsSVB->AnzahlDatensaetze()>1 AND !isset($_GET['SVB_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVB_VERTRAGSNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVB_VERTRAGSNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVB']['SVB_VERTRAGSNUMMER'],130,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=SVA_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SVA_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVA']['SVA_BEZEICHNUNG'],250,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=CA1_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CA1_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVB']['SVB_CAD_KEY'],250,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=CA2_NAME1'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CA2_NAME1'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SVB']['SVB_MELDE_CAD_KEY'],250,'',$Link);
		$Link = './serviceleistungen_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ANZTN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZTN'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['wrd_AnzahlDSZeilen'],120,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsSVB->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Style = ($rsSVB->FeldInhalt('ABGELAUFEN')==1?'font-style:italic;color:#FF0000;':'');
			$Link = './serviceleistungen_Main.php?cmdAktion=Details&SVB_KEY=0'.$rsSVB->FeldInhalt('SVB_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('SVB_VERTRAGSNUMMER',$rsSVB->FeldInhalt('SVB_VERTRAGSNUMMER'),0,130,false,($DS%2),$Style,$Link,'T','L',$rsSVB->FeldInhalt('SVB_LIEFERSCHEINNR'));
			$Form->Erstelle_ListenFeld('SVA_BEZEICHNUNG',$rsSVB->FeldInhalt('SVA_BEZEICHNUNG'),0,250,false,($DS%2),$Style,'','T','L');
			$Form->Erstelle_ListenFeld('CA1_NAME1',$rsSVB->FeldInhalt('CA1_NAME1'),0,250,false,($DS%2),$Style,'','T','L');
			$Form->Erstelle_ListenFeld('CA2_NAME1',$rsSVB->FeldInhalt('CA2_NAME1'),0,250,false,($DS%2),$Style,'','T','L');
			$Form->Erstelle_ListenFeld('ANZTN',$rsSVB->FeldInhalt('ANZTN'),0,120,false,($DS%2),$Style,'','N0T','R');

			$Form->ZeileEnde();

			$rsSVB->DSWeiter();
			$DS++;
		}

		$Link = './serviceleistungen_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsSVB->FeldInhalt('SVB_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_Serviceleistungen',serialize($Param));
        $EditModus = ($Recht19000&6);

		$Form->Erstelle_HiddenFeld('SVB_KEY', $AWIS_KEY1);

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./serviceleistungen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVB->FeldInhalt('SVB_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSVB->FeldInhalt('SVB_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht19000&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht19000&6);
		}
				
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_SVA_KEY'].':',190);
		if($rsSVB->FeldInhalt('ANZTN')>0)
		{
			$Form->Erstelle_HiddenFeld('SVB_SVA_KEY', $rsSVB->FeldInhalt('SVB_SVA_KEY'));
			$Form->Erstelle_TextFeld('BEZEICHNUNG',$rsSVB->FeldInhalt('SVA_BEZEICHNUNG'),20,200,false,'','','','T');
		}
		else
		{
			if($EditRecht)
			{
				$AWISCursorPosition='txtSVB_SVA_KEY';
			}
			$SQL = 'SELECT SVA_KEY, SVA_BEZEICHNUNG ';
			$SQL .= ' FROM SVCARTIKEL';
			$SQL .= ' WHERE SVA_MANDANT = 1';
			$SQL .= ' ORDER BY SVA_BEZEICHNUNG';
			$Form->Erstelle_SelectFeld('!SVB_SVA_KEY',$rsSVB->FeldInhalt('SVB_SVA_KEY'),"400:390",$EditRecht,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		}
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_VERTRAGSNUMMER'].':',190);
		$Form->Erstelle_TextFeld('!SVB_VERTRAGSNUMMER',$rsSVB->FeldInhalt('SVB_VERTRAGSNUMMER'),20,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Link = ($rsSVB->FeldInhalt('SVB_CAD_KEY')==''?'':'/crm/crm_Main.php?cmdAktion=Details&CAD_KEY='.$rsSVB->FeldInhalt('SVB_CAD_KEY').'');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_CAD_KEY'].':',190,'',$Link);
		$Form->Erstelle_TextFeld('*CAD_KEY','',10,100,$EditRecht,'','background-color:#22FF22');
		$AktuelleDaten = ($AWIS_KEY1===0?'':($rsSVB->FeldInhalt('SVB_CAD_KEY')==''?'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']:array($rsSVB->FeldInhalt('SVB_CAD_KEY').'~'.$rsSVB->FeldInhalt('CA1_NAME1'))));
		$Form->Erstelle_SelectFeld('!SVB_CAD_KEY',$rsSVB->FeldInhalt('SVB_CAD_KEY'),'300:270',$EditRecht,'*F*CAD_Daten;sucCAD_KEY;KON_KEY='.$AWISBenutzer->BenutzerKontaktKEY().';;','','','','',$AktuelleDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Link = ($rsSVB->FeldInhalt('SVB_CAD_KEY')==''?'':'/crm/crm_Main.php?cmdAktion=Details&CAD_KEY='.$rsSVB->FeldInhalt('SVB_MELDE_CAD_KEY').'');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_MELDE_CAD_KEY'].':',190,'',$Link);
		$Form->Erstelle_TextFeld('*MELDE_CAD_KEY','',10,100,$EditRecht,'','background-color:#22FF22');
		$AktuelleDaten = ($AWIS_KEY1===0?'':($rsSVB->FeldInhalt('SVB_MELDE_CAD_KEY')==''?'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']:array($rsSVB->FeldInhalt('SVB_MELDE_CAD_KEY').'~'.$rsSVB->FeldInhalt('CA2_NAME1'))));
		$Form->Erstelle_SelectFeld('SVB_MELDE_CAD_KEY',$rsSVB->FeldInhalt('SVB_MELDE_CAD_KEY'),'300:270',$EditRecht,'*F*CAD_Daten;sucMELDE_CAD_KEY;KON_KEY='.$AWISBenutzer->BenutzerKontaktKEY().';;','','','','',$AktuelleDaten);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_KUNDENID'].':',190);
		$Form->Erstelle_TextFeld('SVB_KUNDENID',($AWIS_KEY1===0?'':$rsSVB->FeldInhalt('SVB_KUNDENID')),20,200,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_DATUMAB'].':',190);
		$Form->Erstelle_TextFeld('!SVB_DATUMAB',($AWIS_KEY1===0?'':$rsSVB->FeldInhalt('SVB_DATUMAB')),10,200,$EditRecht,'','','','D');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SVB']['SVB_DATUMBIS'].':',190);
		$Form->Erstelle_TextFeld('!SVB_DATUMBIS',($AWIS_KEY1===0?'':$rsSVB->FeldInhalt('SVB_DATUMBIS')),10,200,$EditRecht,'','','','D');
		$Form->ZeileEnde();

		$Form->Formular_Ende();

		if($AWIS_KEY1>0)
		{
			switch($rsSVB->FeldInhalt('SVA_DATENART'))
			{
				case 'P':		// Personendaten
					$Reg = new awisRegister(19010);
					break;
				case 'F':		// Fahrzeugdaten
					$Reg = new awisRegister(19020);
					break;
			}
			$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
		}
	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht19000&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if($DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
		$Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');
	}

	if(($Recht19000&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	if(($Recht19000&8)==8 AND $DetailAnsicht AND $AWIS_KEY1>0)
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'L');
	}
	if(($Recht19003&2)==2 AND $DetailAnsicht AND (isset($_GET['Seite']) AND $_GET['Seite']=='ExportTN'))
	{
		$Form->Schaltflaeche('image', 'cmdExportXLSX', './serviceleistungen_exportieren.php?SVB_KEY='.$AWIS_KEY1, '/bilder/cmd_koffer.png', $AWISSprachKonserven['Wort']['lbl_export']);
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081035");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201202081036");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWIS_KEY2;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND SVB_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['SVB_VERTRAGSNUMMER']) AND $Param['SVB_VERTRAGSNUMMER']!='')
	{
		$Bedingung .= ' AND (SVB_VERTRAGSNUMMER ' . $DB->LIKEoderIST($Param['SVB_VERTRAGSNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['SVB_KUNDENID']) AND $Param['SVB_KUNDENID']!='')
	{
		$Bedingung .= ' AND SVB_KUNDENID = ' . $DB->FeldInhaltFormat('T',$Param['SVB_KUNDENID']) . ' ';
	}

	if(isset($Param['SVT_KENNUNG']) AND $Param['SVT_KENNUNG']!='')
	{
		// TODO: Da muss noch die Abfrage um das Date�m erweitert werden
		$Bedingung .= ' AND EXISTS(SELECT * FROM SVCTeilnehmer WHERE SVT_KENNUNG = ' . $DB->FeldInhaltFormat('T',$Param['SVT_KENNUNG']) . ' AND SVT_SVB_KEY = SVB_KEY) ';
		// Um in das Detail springen zu k�nnen!
		$SQL = ' SELECT SVT_KEY FROM SVCTeilnehmer WHERE SVT_KENNUNG = ' . $DB->FeldInhaltFormat('T',$Param['SVT_KENNUNG']);
		$rsSVT = $DB->RecordSetOeffnen($SQL);
		if($rsSVT->AnzahlDatensaetze()==1)
		{
			$AWIS_KEY2 = $rsSVT->FeldInhalt('SVT_KEY');
		}
	}

	if(isset($Param['SuchName']) AND $Param['SuchName']!='')
	{
		$Bedingung .= 'AND (UPPER(CA1_Name1) ' . $DB->LIKEoderIST($Param['SuchName'].'%',1) . ' ';
		$Bedingung .= 'OR UPPER(CA2_Name1) ' . $DB->LIKEoderIST($Param['SuchName'].'%',1) . ' ';
		$Bedingung .= ')';
	}	

	return $Bedingung;
}
?>