--------------------------------------------------------
--  Datei erstellt -Freitag-M�rz-10-2017
--------------------------------------------------------
UPDATE TEXTKONSERVEN
SET XTX_TEXT_DE = 'Konditionen sind mit regionalen und saisonalen Angeboten nicht kombinierbar und gelten nur f�r Waren und Dienstleistungen, welche dem Warensortiment bzw. dem Leistungsumfang von A.T.U entsprechen. Ausgenommen sind somit Fremdleistungen (z. B. Pr�forganisationen), Fremdkauf, Alufelgen von Fremdherstellern (au�er Aluett), externer Zukauf, Sonderbestellungen, Pfand/Altteile und Mietwagen.

Bei Fahrzeugen mit Reifendruckkontrollsystem k�nnen bei Radwechsel oder/und Reifenmontage zus�tzliche Kosten entstehen.',

XTX_TEXT_CZ = 'Konditionen sind mit regionalen und saisonalen Angeboten nicht kombinierbar und gelten nur f�r Waren und Dienstleistungen, welche dem Warensortiment bzw. dem Leistungsumfang von A.T.U entsprechen. Ausgenommen sind somit Fremdleistungen (z. B. Pr�forganisationen), Fremdkauf, Alufelgen von Fremdherstellern (au�er Aluett), externer Zukauf, Sonderbestellungen, Pfand/Altteile und Mietwagen.

Bei Fahrzeugen mit Reifendruckkontrollsystem k�nnen bei Radwechsel oder/und Reifenmontage zus�tzliche Kosten entstehen.',

XTX_TEXT_NL = 'Konditionen sind mit regionalen und saisonalen Angeboten nicht kombinierbar und gelten nur f�r Waren und Dienstleistungen, welche dem Warensortiment bzw. dem Leistungsumfang von A.T.U entsprechen. Ausgenommen sind somit Fremdleistungen (z. B. Pr�forganisationen), Fremdkauf, Alufelgen von Fremdherstellern (au�er Aluett), externer Zukauf, Sonderbestellungen, Pfand/Altteile und Mietwagen.

Bei Fahrzeugen mit Reifendruckkontrollsystem k�nnen bei Radwechsel oder/und Reifenmontage zus�tzliche Kosten entstehen.',

XTX_TEXT_IT = 'Konditionen sind mit regionalen und saisonalen Angeboten nicht kombinierbar und gelten nur f�r Waren und Dienstleistungen, welche dem Warensortiment bzw. dem Leistungsumfang von A.T.U entsprechen. Ausgenommen sind somit Fremdleistungen (z. B. Pr�forganisationen), Fremdkauf, Alufelgen von Fremdherstellern (au�er Aluett), externer Zukauf, Sonderbestellungen, Pfand/Altteile und Mietwagen.

Bei Fahrzeugen mit Reifendruckkontrollsystem k�nnen bei Radwechsel oder/und Reifenmontage zus�tzliche Kosten entstehen.',

XTX_TEXT_FR = 'Konditionen sind mit regionalen und saisonalen Angeboten nicht kombinierbar und gelten nur f�r Waren und Dienstleistungen, welche dem Warensortiment bzw. dem Leistungsumfang von A.T.U entsprechen. Ausgenommen sind somit Fremdleistungen (z. B. Pr�forganisationen), Fremdkauf, Alufelgen von Fremdherstellern (au�er Aluett), externer Zukauf, Sonderbestellungen, Pfand/Altteile und Mietwagen.

Bei Fahrzeugen mit Reifendruckkontrollsystem k�nnen bei Radwechsel oder/und Reifenmontage zus�tzliche Kosten entstehen.',

XTX_TEXT_CH = 'Konditionen sind mit regionalen und saisonalen Angeboten nicht kombinierbar und gelten nur f�r Waren und Dienstleistungen, welche dem Warensortiment bzw. dem Leistungsumfang von A.T.U entsprechen. Ausgenommen sind somit Fremdleistungen (z. B. Pr�forganisationen), Fremdkauf, Alufelgen von Fremdherstellern (au�er Aluett), externer Zukauf, Sonderbestellungen, Pfand/Altteile und Mietwagen.

Bei Fahrzeugen mit Reifendruckkontrollsystem k�nnen bei Radwechsel oder/und Reifenmontage zus�tzliche Kosten entstehen.',

XTX_TEXT_AT = 'Konditionen sind mit regionalen und saisonalen Angeboten nicht kombinierbar und gelten nur f�r Waren und Dienstleistungen, welche dem Warensortiment bzw. dem Leistungsumfang von A.T.U entsprechen. Ausgenommen sind somit Fremdleistungen (z. B. Pr�forganisationen), Fremdkauf, Alufelgen von Fremdherstellern (au�er Aluett), externer Zukauf, Sonderbestellungen, Pfand/Altteile und Mietwagen.

Bei Fahrzeugen mit Reifendruckkontrollsystem k�nnen bei Radwechsel oder/und Reifenmontage zus�tzliche Kosten entstehen.',

XTX_USERDAT = SYSDATE

WHERE XTX_KENNUNG = 'txtKonditionenSchluss';

commit;