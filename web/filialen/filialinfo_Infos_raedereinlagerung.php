<?php
//********************************************************************************************
// Raedereinlagerung
//********************************************************************************************
//
// Dieses Modul ermöglicht die Anzeige und das Bearbeiten der Raedereinlagerungen für 
// eine Filiale
//
//********************************************************************************************
// Autor: Thomas Riedl
// Datum: 18.03.2008
//********************************************************************************************
// Änderungen:
// WER                   WANN      WAS
//
//
//********************************************************************************************
require_once("awis_forms.inc.php");

global $con;
global $awisRSZeilen;
global $AWISSprache;
global $AWISBenutzer;
global $SpeichernButton;

$TXT_Baustein = array();

$TXT_Baustein[]=array('REL','REL_%');
$TXT_Baustein[]=array('REL','HinweisAbgeschlossen');
$TXT_Baustein[]=array('REL','WirklichAbschliessen');
$TXT_Baustein[]=array('REL','LieferscheinAbschliessen');
$TXT_Baustein[]=array('Wort','Abgeschlossen');
$TXT_Baustein[]=array('Wort','NichtAbgeschlossen');
$TXT_Baustein[]=array('Wort','PDFErzeugen');
$TXT_Baustein[]=array('Wort','KeineBerechtigungRaedereinlagerungen');
$TXT_Baustein[]=array('Wort','Zurueck');
$TXT_Baustein[]=array('Wort','Ja');
$TXT_Baustein[]=array('Wort','Nein');
$TXT_Baustein[]=array('Wort','HinweisSpeichern');
$TXT_Baustein[]=array('Liste','lst_JaNein');
$TXT_Baustein[]=array('Wort','txt_BitteWaehlen');

$TXT_Baustein = awis_LadeTextKonserven($con, $TXT_Baustein, $AWISSprache);

// Aktueller Lieferschein?
$LFDNR = (isset($_GET['LFDNR'])?$_GET['LFDNR']:(isset($_POST['txtREL_LFDNR'])?$_POST['txtREL_LFDNR']:''));
$FIL_ID = explode("=",awis_BenutzerParameter($con,'_HILFSPARAM',$AWISBenutzer->BenutzerName()));
$FIL_ID = $FIL_ID[1];

$RechteStufe = awisBenutzerRecht($con,3602);	// MDE Raedereinlagerung
if($RechteStufe==0)
{
     awisEreignis(3, 1000, 'Filialen', $AWISBenutzer->BenutzerName(), '', '', '');
     die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");     
}

$SpeichernButton = False;

if(($RechteStufe & 2)==2 or ($RechteStufe & 4)==4)
{
	$SpeichernButton = True;
}


// Beschränkung auf eine Filiale?
$UserFilialen=awisBenutzerFilialen($con,$AWISBenutzer->BenutzerName(),2);

if($UserFilialen!='' AND intval($FIL_ID) <> intval($UserFilialen))
{
	awis_FORM_Hinweistext($TXT_Baustein['Wort']['KeineBerechtigungRaedereinlagerungen'],1);
	die;
}

if(isset($_POST['cmdSpeichern_x']))
{	
	include './filialinfo_speichern.php';
}
	

elseif(isset($_POST['cmdAbschliessen_x']))
{
	include './filialinfo_speichern.php';	
	
	$SQL = 'SELECT MIN(REL_STATUS) AS STATUS';
	$SQL .=' FROM RAEDEREINLAGERUNGEN';
	$SQL .=' WHERE REL_FIL_ID=0'.intval($FIL_ID);
	$SQL .= ' AND REL_LFDNR=0'.intval($LFDNR);
	
	awis_Debug(1,$SQL);
	
	$rsREL=awisOpenRecordset($con,$SQL);
	$rsRELZeilen = $awisRSZeilen;
	
	if ($rsREL['STATUS'][0]>=10)
	{
		echo '<span class=HinweisText>'.$TXT_Baustein['REL']['HinweisAbgeschlossen'].'</span>';
		echo "<br><br>";
		
		echo '<form name=frmBestaetigen action=./filialinfo_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>';

		echo '<input type=submit name=cmdAbschliessenAbbrechen style="width:50px" value='.$TXT_Baustein['Wort']['Zurueck'].'>';
	
		echo '<input type=hidden name=txtREL_LFDNR value='.$LFDNR.'>';
		echo '</form>';		
	}
	else 
	{
		echo '<form name=frmBestaetigen action=./filialinfo_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>';
		echo '<span class=HinweisText>'.$TXT_Baustein['REL']['WirklichAbschliessen'].'</span>';
	
		echo '<br><input type=submit name=cmdAbschliessenOK style="width:50px" value='.$TXT_Baustein['Wort']['Ja'].'>';
		echo '&nbsp;<input type=submit name=cmdAbschliessenAbbrechen style="width:50px" value='.$TXT_Baustein['Wort']['Nein'].'>';
	
		echo '<input type=hidden name=txtREL_LFDNR value='.$LFDNR.'>';
		echo '</form>';
	}
	
	awisLogoff($con);
	die();
}
elseif(isset($_POST['cmdAbschliessenOK']))
{
	$SQL = 'UPDATE RAEDEREINLAGERUNGEN';
	$SQL .= ' SET REL_Status=12';
	$SQL .= ' , REL_User='.awis_FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
	$SQL .= ' , REL_UserDat=SYSDATE';
	$SQL .= ' , REL_DatumAbschluss=SYSDATE';
	$SQL .= ' WHERE REL_FIL_ID='.$FIL_ID;
	$SQL .= ' AND REL_LFDNR=0'.intval($_POST['txtREL_LFDNR']);
	
	if(awisExecute($con, $SQL)===false)
	{
		awisErrorMailLink('Raedereinlagerungen abschliessen',1,'Fehler beim Speichern',$SQL);
	}			
}

if($LFDNR!='')
{
	$SQL = 'SELECT *';
	$SQL .=' FROM RAEDEREINLAGERUNGEN REL';
	$SQL .=' WHERE REL_FIL_ID=0'.intval($FIL_ID);
	$SQL .= ' AND REL_LFDNR=0'.intval($LFDNR);
	$SQL .= ' ORDER BY REL_NR ASC';		
}
else 
{
	$SQL = 'SELECT DISTINCT REL_LFDNR, REL_STATUSUEBERTRAGEN';
	$SQL .= ' ,(SELECT MIN(REL_STATUS) AS STATUS FROM RAEDEREINLAGERUNGEN WHERE RAEDEREINLAGERUNGEN.REL_LFDNR=REL.REL_LFDNR AND RAEDEREINLAGERUNGEN.REL_FIL_ID = REL.REL_FIL_ID) AS REL_STATUS';	
	$SQL .=' FROM RAEDEREINLAGERUNGEN REL';
	$SQL .=' WHERE REL_FIL_ID=0'.intval($FIL_ID);
	$SQL .=' ORDER BY 1 DESC';
}

$rsREL=awisOpenRecordset($con,$SQL);
$rsRELZeilen = $awisRSZeilen;

awis_FORM_FormularStart();

if($LFDNR=='')			// Übersicht zeigen
{
	awis_FORM_ZeileStart();	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_LFDNR'],200);
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_STATUS'],200);
	awis_FORM_ZeileEnde();
	
	for($RELZeile=0;$RELZeile<$rsRELZeilen;$RELZeile++)
	{
		awis_FORM_ZeileStart();					
		$Link='./filialinfo_Main.php?cmdAktion=Filialinfos&Seite=MDE&Unterseite=Raedereinlagerung&LFDNR='.$rsREL['REL_LFDNR'][$RELZeile].'';
		awis_FORM_Erstelle_ListenFeld('REL_LFDNR',$rsREL['REL_LFDNR'][$RELZeile],20,200,false,($RELZeile%2),'',$Link,'T');
		$Status= (($rsREL['REL_STATUS'][$RELZeile]>=10)?$TXT_Baustein['Wort']['Abgeschlossen']:$TXT_Baustein['Wort']['NichtAbgeschlossen']);						
		awis_FORM_Erstelle_ListenFeld('REL_STATUS',$Status,20,200,false,($RELZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	} 
}
else 						// Details anzeigen
{		
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($TXT_Baustein['REL']['REL_LFDNR'].':',150);
	awis_FORM_Erstelle_TextFeld('REL_LFDNR',$rsREL['REL_LFDNR'][0],0,200,false);
	echo '<input type=hidden name=txtREL_LFDNR value=0'.$rsREL['REL_LFDNR'][0].'>';
	awis_FORM_ZeileEnde();			
	
	if(($RechteStufe & 2)==2)
	{		
		echo "&nbsp;<input name=cmdAbschliessen type=image border=0 src=/bilder/bestaetigen.png title='".$TXT_Baustein['REL']['LieferscheinAbschliessen']."'>";						
	}	
	
	if ($rsREL['REL_STATUS'][0]>=10)
	{
		$PDF_Link='./filialinfo_Infos_raedereinlagerung_pdf.php?REL_LFDNR='.$rsREL['REL_LFDNR'][0].'&REL_FIL_ID='.$FIL_ID; 
		echo "&nbsp;<a href=".$PDF_Link."><img border=0 src=/bilder/pdf_gross.png title='".$TXT_Baustein['Wort']['PDFErzeugen']."'></a>";
	}
	
	//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
	if((($RechteStufe&2)!=0) AND $rsRELZeilen > 0)
	{
		echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;";
		echo '<span class=Hinweistext>'.$TXT_Baustein['Wort']['HinweisSpeichern'].'</span>';
	}

	awis_FORM_Trennzeile();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_NR'],120);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_DATUMSCAN'],120);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_MENGE'],80);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_BESCHAEDIGT'],120);	
	awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_BEMERKUNG'],230);	
	//awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_ZUBEHOER'],230);	

	if(($RechteStufe & 8)==8)		
	{
		awis_FORM_Erstelle_Liste_Ueberschrift($TXT_Baustein['REL']['REL_MENGEIST'],100);	
	}
	
	awis_FORM_ZeileEnde();		
	
	for($RELZeile=0;$RELZeile<$rsRELZeilen;$RELZeile++)
	{
		//EditModus: nur bearbeitbar, wenn noch nicht abgeschlossen
		//if((($RechteStufe&2)!=0) AND $rsLIK['LIK_STATUS'][$LIKZeile]>=10 AND (($RechteStufe&4)!=4))
		if(((($RechteStufe&2)==2) AND $rsREL['REL_STATUS'][$RELZeile]<10))
		{
			$EditModus=true;
		}
		else
		{
			$EditModus=false;
		}
	
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_ListenFeld('REL_NR',$rsREL['REL_NR'][$RELZeile],10,120,false,($RELZeile%2),'','','T','L',$TXT_Baustein['REL']['REL_DATUMSCAN']. ': '. $rsREL['REL_DATUMSCAN'][$RELZeile]);
		awis_FORM_Erstelle_ListenFeld('REL_DATUMSCAN',$rsREL['REL_DATUMSCAN'][$RELZeile],10,120,false,($RELZeile%2),'','','D','L');
		awis_FORM_Erstelle_ListenFeld('REL_MENGE_'.$rsREL['REL_KEY'][$RELZeile],$rsREL['REL_MENGE'][$RELZeile],7,80,$EditModus,($RELZeile%2),'','','N','L');
		awis_FORM_Erstelle_Checkbox('REL_BESCHAEDIGT_VL_'.$rsREL['REL_KEY'][$RELZeile], $rsREL['REL_BESCHAEDIGT_VL'][$RELZeile],30,$EditModus,'on','','Beschädigung vorne links');
		awis_FORM_Erstelle_Checkbox('REL_BESCHAEDIGT_VR_'.$rsREL['REL_KEY'][$RELZeile], $rsREL['REL_BESCHAEDIGT_VR'][$RELZeile],30,$EditModus,'on','','Beschädigung vorne rechts');
		awis_FORM_Erstelle_Checkbox('REL_BESCHAEDIGT_HL_'.$rsREL['REL_KEY'][$RELZeile], $rsREL['REL_BESCHAEDIGT_HL'][$RELZeile],30,$EditModus,'on','','Beschädigung hinten links');
		awis_FORM_Erstelle_Checkbox('REL_BESCHAEDIGT_HR_'.$rsREL['REL_KEY'][$RELZeile], $rsREL['REL_BESCHAEDIGT_HR'][$RELZeile],30,$EditModus,'on','','Beschädigung hinten rechts');
		awis_FORM_Erstelle_ListenFeld('REL_BEMERKUNG_'.$rsREL['REL_KEY'][$RELZeile],$rsREL['REL_BEMERKUNG'][$RELZeile],32,230,$EditModus,($RELZeile%2),'','','T','L');
		//awis_FORM_Erstelle_ListenFeld('REL_ZUBEHOER_'.$rsREL['REL_KEY'][$RELZeile],$rsREL['REL_ZUBEHOER'][$RELZeile],32,230,$EditModus,($RELZeile%2),'','','T','L');
		
		//$Daten = explode('|',$TXT_Baustein['Liste']['lst_JaNein']);
		//awis_FORM_Erstelle_SelectFeld('REL_BESCHAEDIGT_'.$rsREL['REL_KEY'][$RELZeile],$rsREL['REL_BESCHAEDIGT'][0],120,$EditModus,$con,'',$TXT_Baustein['Wort']['txt_BitteWaehlen'],'','','',$Daten);					

		if(($RechteStufe & 8)==8)			
		{
			awis_FORM_Erstelle_ListenFeld('REL_MENGESCAN',$rsREL['REL_MENGESCAN'][$RELZeile],10,100,false,($RELZeile%2),'','','N','L');
		}
		
		awis_FORM_ZeileEnde();
	}
}
	
awis_FORM_FormularEnde();

?>
