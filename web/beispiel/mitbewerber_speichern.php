<?php
global $Param;
global $awisRSInfo;
global $awisRSInfoName;
global $awisDBError;
global $con;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('MBW','MBW_%');

$MBWKEY = $_POST['txtMBW_KEY'];

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$_SERVER['PHP_AUTH_USER']);
$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);

if(intval($_POST['txtMBW_KEY'])===0)		// Neue Adresse
{

		// daten auf Vollst�ndigkeit pr�fen
	$Fehler = '';
	$Pflichtfelder = array('MBW_NAME1','MBW_STRASSE','MBW_HAUSNUMMER','MBW_PLZ','MBW_ORT','MBW_TELEFON');
	foreach($Pflichtfelder AS $Pflichtfeld)
	{
		if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
		{
			$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['MBW'][$Pflichtfeld].'<br>';
		}
	}
		// Wurden Fehler entdeckt? => Speichern abbrechen
	if($Fehler!='')
	{
		die('<span class=HinweisText>'.$Fehler.'</span>');
	}

	$SQL = 'INSERT INTO Mitbewerber';
	$SQL .= '(MBW_ANREDE,MBW_KONZERN, MBW_NAME1, MBW_NAME2';
	$SQL .= ',MBW_STRASSE, MBW_HAUSNUMMER';
	$SQL .= ',MBW_LAN_CODE, MBW_PLZ, MBW_ORT, MBW_BEMERKUNG, MBW_STATUS';
	$SQL .= ',MBW_EROEFFNUNG,MBW_SCHLIESSUNG';
	$SQL .= ',MBW_TELEFON,MBW_MBK_KEY,MBW_WEBSEITE,MBW_ONLINESHOP';

	$SQL .= ',MBW_USER, MBW_USERDAT';
	$SQL .= ')VALUES ('.awis_FeldInhaltFormat('Z',$_POST['txtMBW_ANREDE'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_KONZERN'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_NAME1'],false);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_NAME2'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_STRASSE'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_HAUSNUMMER'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_LAN_CODE'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_PLZ'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_ORT'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',(isset($_POST['txtMBW_BEMERKUNG'])?$_POST['txtMBW_BEMERKUNG']:''),true);
	$SQL .= ',\'A\'';
	$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtMBW_EROEFFNUNG'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtMBW_SCHLIESSUNG'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('TZ',$_POST['txtMBW_TELEFON'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('Z',(isset($_POST['txtMBW_MBK_KEY'])?$_POST['txtMBW_MBK_KEY']:'1'),true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_WEBSEITE'],true);
	$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBW_ONLINESHOP'],true);
	$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
	$SQL .= ',SYSDATE';
	$SQL .= ')';
	if(!awisExecute($con,$SQL))
	{
		awisErrorMailLink('adressen_speichern.php',1,$awisDBError['message'],'NEU/1');
		die();
	}
	$SQL = 'SELECT seq_MBW_KEY.CurrVal AS KEY FROM DUAL';
	$rsKey = awisOpenRecordset($con,$SQL);
	$MBWKEY=$rsKey['KEY'][0];
	awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER'] ,$rsKey['KEY'][0]);
}
else 					// ge�nderter Mitbewerber
{
	$Felder = explode(';',awis_NameInArray($_POST, 'txtMBW',1,1));
	$FehlerListe = array();
	$UpdateFelder = '';

	awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER'] ,$_POST['txtMBW_KEY']);
	$rsMBW = awisOpenRecordset($con,'SELECT * FROM Mitbewerber WHERE mbw_key=' . $_POST['txtMBW_KEY'] . '');
	$FeldListe = '';
	foreach($Felder AS $Feld)
	{
		$FeldName = substr($Feld,3);
		if(isset($_POST['old'.$FeldName]))
		{
			// Alten und neuen Wert umformatieren!!
			if($FeldName=='MBW_TELEFON')		// Telefon nur ZIFFERN
			{
				$WertNeu=awis_FeldInhaltFormat('TZ',$_POST[$Feld],true);
			}
			else
			{
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
			}
			$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
			$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsMBW[$FeldName][0],true);
	//echo '<br>.'.$Feld.'=='.$awisRSInfoName[$FeldName]['TypKZ'],'(ALT:'.$WertAlt.')(NEU:'.$WertNeu.')(DB:'.$WertDB.')';
			if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
			{
				if($WertAlt != $WertDB AND $WertDB!='null')
				{
					$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
				}
				else
				{
					$FeldListe .= ', '.$FeldName.'=';

					if($_POST[$Feld]=='')	// Leere Felder immer als NULL
					{
						$FeldListe.=' null';
					}
					else
					{
						$FeldListe.=$WertNeu;
					}
				}
			}
		}
	}

	if(count($FehlerListe)>0)
	{
		$Meldung = str_replace('%1',$rsMBW['MBW_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
		foreach($FehlerListe AS $Fehler)
		{
			$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
		}
		awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
	}
	elseif($FeldListe!='')
	{
		$SQL = 'UPDATE Mitbewerber SET';
		$SQL .= substr($FeldListe,1);
		$SQL .= ', mbw_user=\''.$_SERVER['PHP_AUTH_USER'].'\'';
		$SQL .= ', mbw_userdat=sysdate';
		$SQL .= ' WHERE mbw_key=0' . $_POST['txtMBW_KEY'] . '';
//awis_Debug(1,$SQL );
		if(awisExecute($con, $SQL)===false)
		{
			awisErrorMailLink('Mitbewerber',1,'Fehler beim Speichern',$SQL);
		}
	}

	$MBWKEY=$_POST['txtMBW_KEY'];
//awis_Debug(1,$FeldListe,$SQL,$Felder);
}



//*******************************************************************************
// Filialzuordnung
//*******************************************************************************
$Felder = awis_NameInArray($_POST, 'txtMFI_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('MFI','MFI_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';

	if($_POST['txtMFI_KEY']=='')
	{

		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler = '';
		if($_POST['txtMFI_FIL_ID']=='')	// Filialen muss angegeben werden
		{
			$Fehler = $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['MFI']['MFI_FIL_ID'].'<br>';
		}
		$rsFIL = awisOpenRecordset($con,'SELECT FIL_ID FROM Filialen WHERE FIL_ID = \''.$_POST['txtMFI_FIL_ID'].'\'');
		if($awisRSZeilen==0)
		{
			$Fehler = $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['MFI']['MFI_FIL_ID'].'<br>';
		}

			// Wurden Fehler entdeckt? => Speichern abbrechen
		if($Fehler!='')
		{
			die('<span class=HinweisText>'.$Fehler.'</span>');
		}
//awis_Debug(1,$_POST);
		$SQL = 'INSERT INTO MitbewerberFilialen';
		$SQL .= '(MFI_MBW_KEY,MFI_FIL_ID, MFI_ENTFERNUNG, MFI_RANKING, MFI_BEMERKUNG, MFI_SICHTBAR';
		$SQL .= ',MFI_USER, MFI_USERDAT';
		$SQL .= ')VALUES ('.$MBWKEY;
		$SQL .= ',(SELECT FIL_ID FROM Filialen WHERE fil_id=0' . awis_FeldInhaltFormat('Z',$_POST['txtMFI_FIL_ID'],false).')';
		$SQL .= ',' . awis_FeldInhaltFormat('N',$_POST['txtMFI_ENTFERNUNG'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N',$_POST['txtMFI_RANKING'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMFI_BEMERKUNG'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('N',$_POST['txtMFI_SICHTBAR'],false);
		$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';

		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('adressen_speichern.php',1,$awisDBError['message'],'NEU/1');
			die();
		}
		$SQL = 'SELECT seq_MFI_KEY.CurrVal AS KEY FROM DUAL';
		$rsKey = awisOpenRecordset($con,$SQL);
		awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER'] ,$rsKey['KEY'][0]);

	}
	else 					// ge�nderter Mitbewerber
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER'] ,$_POST['txtMBW_KEY']);
		$rsMBW = awisOpenRecordset($con,'SELECT * FROM MitbewerberFilialen WHERE mfi_key=' . $_POST['txtMFI_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{

				// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsMBW[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsMBW['MBW_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE MitbewerberFilialen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', mfi_user=\''.$_SERVER['PHP_AUTH_USER'].'\'';
			$SQL .= ', mfi_userdat=sysdate';
			$SQL .= ' WHERE mfi_key=0' . $_POST['txtMFI_KEY'] . '';
//awis_Debug(1,$SQL);
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('Mitbewerber',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}

//*******************************************************************************
// Unternehmenstypen
//*******************************************************************************
$Felder = awis_NameInArray($_POST, 'txtMIU_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('MIU','MIU_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';

	if($_POST['txtMIU_KEY']=='')
	{

		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler = '';
		$SQL = 'INSERT INTO MitbewerbUTypen';
		$SQL .= '(MIU_MBW_KEY,MIU_MUT_KEY, MIU_BEMERKUNG';
		$SQL .= ',MIU_USER, MIU_USERDAT';
		$SQL .= ')VALUES ('.$MBWKEY;
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtMIU_MUT_KEY'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMIU_BEMERKUNG'],true);
		$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('adressen_speichern.php',1,$awisDBError['message'],'NEU/1');

			die();
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsMBW = awisOpenRecordset($con,'SELECT * FROM MitbewerbUTypen WHERE miu_key=' . $_POST['txtMIU_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{

				// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsMBW[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsMBW['MIU_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE MitbewerbUTypen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', miu_user=\''.$_SERVER['PHP_AUTH_USER'].'\'';
			$SQL .= ', miu_userdat=sysdate';
			$SQL .= ' WHERE miu_key=0' . $_POST['txtMIU_KEY'] . '';
//awis_Debug(1,$SQL);
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('Mitbewerber',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}

//*******************************************************************************
// Hersteller
//*******************************************************************************
$Felder = awis_NameInArray($_POST, 'txtMBH_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('MBH','MBH_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';

	if($_POST['txtMBH_KEY']=='')
	{

		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler = '';
		$SQL = 'INSERT INTO MitbewerbHersteller';
		$SQL .= '(MBH_MBW_KEY,MBH_HER_ID, MBH_BEMERKUNG';
		$SQL .= ',MBH_USER, MBH_USERDAT';
		$SQL .= ')VALUES ('.$MBWKEY;
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtMBH_HER_ID'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMBH_BEMERKUNG'],true);
		$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('adressen_speichern.php',1,$awisDBError['message'],'NEU/1');

			die();
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsMBW = awisOpenRecordset($con,'SELECT * FROM MitbewerbHersteller WHERE mbh_key=' . $_POST['txtMBH_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{

				// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsMBW[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsMBW['MBH_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE MitbewerbHersteller SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', mbh_user=\''.$_SERVER['PHP_AUTH_USER'].'\'';
			$SQL .= ', mbh_userdat=sysdate';
			$SQL .= ' WHERE mbh_key=0' . $_POST['txtMBH_KEY'] . '';
//awis_Debug(1,$SQL);
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('Mitbewerber',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}

//*******************************************************************************
// Produktgruppen
//*******************************************************************************
$Felder = awis_NameInArray($_POST, 'txtMPG_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('MPG','MPG_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';

	if($_POST['txtMPG_KEY']=='')
	{

		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler = '';
		$SQL = 'INSERT INTO MitbewerbProdGruppen';
		$SQL .= '(MPG_MBW_KEY,MPG_MGT_ID, MPG_BEMERKUNG';
		$SQL .= ',MPG_USER, MPG_USERDAT';
		$SQL .= ')VALUES ('.$MBWKEY;
		$SQL .= ',' . awis_FeldInhaltFormat('Z',$_POST['txtMPG_MGT_ID'],true);
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMPG_BEMERKUNG'],true);
		$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if(!awisExecute($con,$SQL))
		{
			awisErrorMailLink('mitbewerb_speichern.php',1,$awisDBError['message'],'NEU/1');

			die();
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsMBW = awisOpenRecordset($con,'SELECT * FROM MitbewerbProdGruppen WHERE MPG_key=' . $_POST['txtMPG_KEY'] . '');
	//awis_Debug(1,$_POST,$Felder);
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsMBW[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsMBW['MPG_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, 'Bitte wiederholen Sie Ihre Eingabe');
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE MitbewerbProdGruppen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', MPG_user=\''.$_SERVER['PHP_AUTH_USER'].'\'';
			$SQL .= ', MPG_userdat=sysdate';
			$SQL .= ' WHERE MPG_key=0' . $_POST['txtMPG_KEY'] . '';
//awis_Debug(1,$SQL);
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('Mitbewerber',1,'Fehler beim Speichern',$SQL);
			}
		}

	}
}
//*******************************************************************************
// Produktgruppen-Checkboxen
//*******************************************************************************
$Felder = awis_NameInArray($_POST, 'oldMGTL_',1,1);		// Nicht txt, da die bei CHECKBOX nicht generiert werden
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('MGT','MGT_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';

	$SQL='';
	foreach($Felder as $Feld)
	{
		$FeldName = substr($Feld,3);

		$AktWert = (isset($_POST['txt'.$FeldName])?$_POST['txt'.$FeldName]:'');
		if($_POST['old'.$FeldName] != $AktWert)
		{

			$ID = intval(substr($FeldName,5));
			if($_POST['old'.$FeldName]=='')
			{
				$SQL = 'INSERT INTO MitbewerbProdGruppen';
				$SQL .= '(MPG_MBW_KEY,MPG_MGT_ID';
				$SQL .= ',MPG_USER, MPG_USERDAT';
				$SQL .= ')VALUES ('.$MBWKEY;
				$SQL .= ',' . $ID;
				$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('Mitbewerber',1,'Fehler beim Speichern',$SQL);
				}

			}
			else
			{
				$SQL = 'DELETE FROM MitbewerbProdGruppen';
				$SQL .= ' WHERE mpg_mbw_key=0'.$MBWKEY;
				$SQL .= ' AND mpg_mgt_id=0'.$ID;
				if(awisExecute($con, $SQL)===false)
				{
					awisErrorMailLink('Mitbewerber',1,'Fehler beim Speichern',$SQL);
				}

			}
		}
	}
//awis_Debug(1,$SQL,$Felder);
}

//*******************************************************************************
// Mitbewerber�nderungen
//*******************************************************************************
$Felder = awis_NameInArray($_POST, 'txtMAE_',1,1);
if($Felder!='')
{
//awis_Debug(1,$Felder,$_POST);
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('Wort','geaendert_auf');
	$TextKonserven[]=array('MAE','MAE_%');
	$TextKonserven[]=array('Meldung','DSVeraendert');
	$TextKonserven[]=array('MBW','AenderungsWunschGespeichert');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	if($_POST['txtMBW_KEY']!='')
	{
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat('T',$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat('T',$_POST['old'.$FeldName],true);
				if($WertAlt!=$WertNeu)
				{
					$SQL = 'INSERT INTO MitbewerbAenderungen';
					$SQL .= '(MAE_MBW_KEY,MAE_STATUS, MAE_FELDNAME, MAE_TYP, MAE_ANTRAGDATUM, MAE_ANTRAGSTELLER';
					$SQL .= ',MAE_ALTERWERT, MAE_NEUERWERT';
					$SQL .= ',MAE_USER, MAE_USERDAT';
					$SQL .= ')VALUES ('.$MBWKEY;
					$SQL .= ',\'O\'';
					$SQL .= ',' . awis_FeldInhaltFormat('T',str_replace('MAE_','MBW_',$FeldName),true);
					$SQL .= ',\'A\'';
					$SQL .= ',SYSDATE';
					$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
					$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['old'.$FeldName],true);
					$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txt'.$FeldName],true);
					$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
					$SQL .= ',SYSDATE';
					$SQL .= ')';
					if(awisExecute($con,$SQL)===false)
					{
						awisErrorMailLink('mitbewerb_speichern.php',1,$awisDBError['message'],'NEU/1');

						die();
					}


				}
			}
		}
/*
		// Daten auf Vollst�ndigkeit pr�fen
		$Fehler = '';
		$SQL = 'INSERT INTO MitbewerbAenderungen';
		$SQL .= '(MAE_MBW_KEY,MAE_STATUS, MAE_BESCHREIBUNG, MAE_TYP, MAE_ANTRAGDATUM, MAE_ANTRAGSTELLER';
		$SQL .= ',MAE_USER, MAE_USERDAT';
		$SQL .= ')VALUES ('.$MBWKEY;
		$SQL .= ',\'O\'';
		$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtMAE_BESCHREIBUNG'],true);
		$SQL .= ',\'A\'';
		$SQL .= ',SYSDATE';
		$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
		$SQL .= ',\'' . $_SERVER['PHP_AUTH_USER'] . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if(awisExecute($con,$SQL)===false)
		{
			awisErrorMailLink('mitbewerb_speichern.php',1,$awisDBError['message'],'NEU/1');

			die();
		}
		else
*/
		{
			echo '<span class=HinweisText>'.$TXT_Speichern['MBW']['AenderungsWunschGespeichert'].'</span>';
		}
	}
}


// Aktuellen MBW speichern
awis_BenutzerParameterSpeichern($con, "AktuellerMitbewerber" , $_SERVER['PHP_AUTH_USER'] ,$MBWKEY);

?>