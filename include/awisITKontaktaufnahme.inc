<?php

require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';
require_once 'awisBenutzer.inc';

class awisITKontaktaufnahme
{
    private $_DB;
    private $_Form;
    private $_AWISBenutzer;
    private $_AWISSprachKonserven;

    function __construct()
    {
        $this->_AWISBenutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_Form = new awisFormular();
        $TextKonserven = array();
        $TextKonserven[] = array('OFO', 'OFO_BEZEICHNUNG');
        $TextKonserven[] = array('OFO', 'OFO_BEMERKUNG');
        $TextKonserven[] = array('KONTAKTAUFNAHME', '%');

        $this->_AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven, $this->_AWISBenutzer->BenutzerSprache());
    }

    public function erstelleInfotafeldirekteHilfe()
    {

        $this->_Form->SchreibeHTMLCode('<div id="Infotafel6" style="overflow: hidden">');
        $Recht17000 = $this->_AWISBenutzer->HatDasRecht(17000);
        $SQL = "Select * from ( select * from OTTICKETS ORDER BY OTT_KEY DESC)";                    //Subselect, damit immer die letzten erstellten Tickets abgerufen werden (Kann ohne Subselect zu Problemen f�hren)
        $SQL .= " where ROWNUM <= 4";
        if (($Recht17000 & 16) != 16) {
            $SQL .= " and OTT_XBN_KEY = " . $this->_DB->WertSetzen("TIS", "Z", $this->_AWISBenutzer->BenutzerID());
        }
        $SQL .= " ORDER BY OTT_KEY DESC";
        $rsTickets = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen("TIS"));

        if ($rsTickets->AnzahlDatensaetze() != 0) {
            $i = 0;

            while (!$rsTickets->EOF()) {
                $this->_Form->ZeileStart('overflow:hidden');
                $this->_Form->Erstelle_ListenFeld("Feld", $rsTickets->FeldInhalt("OTT_BETREFF"), 48, 480, false, (($i++) % 2), '', ('../tickets/tickets_Main.php?cmdAktion=Details&OTT_KEY=' . intval($rsTickets->FeldInhalt("OTT_KEY", false))));
                $this->_Form->ZeileEnde();
                $rsTickets->DSWeiter();
            }

            $this->_Form->Trennzeile('O');
            $this->_Form->ZeileStart('width: 100%');
            $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_TICKETSUCHE2'], 500, 'text-align:center', $this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_TICKETSUCHE2LINK']);
            $this->_Form->ZeileEnde();
            $this->_Form->Trennzeile('O');
        } else {
            $this->_Form->Trennzeile('O');
            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_TICKETSUCHEKEINTICKET'], 500, "text-align: center; font-size: 14px");
            $this->_Form->ZeileEnde();
            $this->_Form->Trennzeile('O');
            $this->_Form->Trennzeile('O');
            $this->_Form->Trennzeile('O');
        }
        
        $this->_Form->ZeileStart('width: 100%; padding-left: 0px', '', "awisInfotafelUeberschrift");
        $this->_Form->SchreibeHTMLCode('<div class="awisInfotafelUeberschrift" title="' . $this->_AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_TT1"] . '" style="height: 38px; text-align: center; line-height: 1; padding-top: 0px; padding-left: 0px; display: table;"><div style="vertical-align: middle; display: table-cell;">' . $this->_AWISSprachKonserven["KONTAKTAUFNAHME"]["KONTAKTAUFNAHME_HILFE"] . '</div></div>');
        $this->_Form->ZeileEnde();
        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_ZENTRALE'], 360, 'font-weight: bold');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_FILIALE'], 360, 'font-weight: bold');
        $this->_Form->ZeileEnde();
        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['TELEFONNUMMER_ZENTRALE'], 360);
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['TELEFONNUMMER_FILIALE'], 360);
        $this->_Form->ZeileEnde();
        $this->_Form->Trennzeile('O');
        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_EMAIL'], 360, 'font-weight: bold');
        $this->_Form->ZeileEnde();
        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['EMAIL_KONTAKT'], 360, '', 'mailto:' . $this->_AWISSprachKonserven['KONTAKTAUFNAHME']['EMAIL_KONTAKT']);
        $this->_Form->ZeileEnde();

        $this->_Form->SchreibeHTMLCode('</div>');
    }

    public function erstelleInfotafelTicketErstellen()
    {

        $this->_Form->SchreibeHTMLCode('<div id="Infotafel4" style="overflow: hidden">');

        $SQL = 'SELECT DISTINCT OFO_KEY, OFO_BEZEICHNUNG, OFO_BEMERKUNG';
        $SQL .= ' FROM OTFormulare';
        $SQL .= ' INNER JOIN v_AccountRechte ON OFO_XRC_ID = XRC_ID AND BITAND(XBA_STUFE,OFO_RechteStufe) = OFO_RechteStufe';
        $SQL .= ' WHERE OFO_STATUS = \'A\'';
        $SQL .= ' AND OFO_KEY <> 2';
        $SQL .= ' ORDER BY OFO_BEZEICHNUNG';
        $rsOFO = $this->_DB->RecordSetOeffnen($SQL);

        $DS = 0;
        while (!$rsOFO->EOF()) {
            $Link = '/tickets/tickets_Main.php?cmdAktion=Details&OFO_KEY=' . $rsOFO->FeldInhalt('OFO_KEY');
            $this->_Form->ZeileStart('width: 100%; overflow: hidden');
            $this->_Form->Erstelle_ListenFeld('OFO_BEZEICHNUNG', $rsOFO->FeldInhalt('OFO_BEZEICHNUNG'), 1, 500, false, ($DS % 2), '', $Link);
            $this->_Form->ZeileEnde();
            $this->_Form->ZeileStart('width: 100%; overflow: hidden');
            $this->_Form->Erstelle_ListenFeld('OFO_BEMERKUNG', $rsOFO->FeldInhalt('OFO_BEMERKUNG'), 1, 500, false, ($DS % 2), '', '', 'T', 'L', $rsOFO->FeldInhalt('OFO_BEMERKUNG'));
            $this->_Form->ZeileEnde();

            $DS++;
            $rsOFO->DSWeiter();
        }

        $this->_Form->Trennzeile('O');

        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_EMAIL_RUECK'], 360, 'font-weight: bold');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['EMAIL_KONTAKTAUFNAHME2'], 360, '', 'mailto:' . $this->_AWISSprachKonserven['KONTAKTAUFNAHME']['EMAIL_KONTAKTAUFNAHME2']);
        $this->_Form->ZeileEnde();

        $this->_Form->SchreibeHTMLCode('</div>');
    }

    public function erstelleInfotafelKontaktpersonen()
    {

        $this->_Form->SchreibeHTMLCode('<div id="Infotafel5" style="overflow: hidden">');

        $SQL = ' SELECT OFO_KEY, OFO_BEZEICHNUNG, OFO_BEMERKUNG';
        $SQL .= ' FROM OTFormulare';
        $SQL .= ' WHERE OFO_KEY = 2';
        $rsOFO = $this->_DB->RecordSetOeffnen($SQL);

        $Link = '/tickets/tickets_Main.php?cmdAktion=Details&OFO_KEY=' . $rsOFO->FeldInhalt('OFO_KEY');
        $this->_Form->ZeileStart('width: 100%; overflow: hidden');
        $this->_Form->Erstelle_ListenFeld('OFO_BEZEICHNUNG', $rsOFO->FeldInhalt('OFO_BEZEICHNUNG'), 1, 500, false, '', '', $Link);
        $this->_Form->ZeileEnde();
        $this->_Form->ZeileStart('width: 100%; overflow: hidden');
        $this->_Form->Erstelle_ListenFeld('OFO_BEMERKUNG', $rsOFO->FeldInhalt('OFO_BEMERKUNG'), 1, 500, false, '', '', '', 'T', 'L', $rsOFO->FeldInhalt('OFO_BEMERKUNG'));
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Trennzeile('O');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_EMAIL_RUECK'], 360, 'font-weight: bold');
        $this->_Form->ZeileEnde();
        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['EMAIL_KONTAKTAUFNAHME'], 360, '', 'mailto:' . $this->_AWISSprachKonserven['KONTAKTAUFNAHME']['EMAIL_KONTAKTAUFNAHME']);
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Trennzeile('O');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTPERSONEN_ABTEILUNGEN'], 360, 'font-weight: bold');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart('width: 100%');
        $this->_Form->Trennzeile('O');
        $this->_Form->ZeileEnde();

        $SQL = ' SELECT *';
        $SQL .= ' FROM KONTAKTEINFOS KIN';
        $SQL .= ' INNER JOIN KONTAKTE KON';
        $SQL .= ' ON KON.KON_KEY = KIN.KIN_KON_KEY';
        $SQL .= ' INNER JOIN KONTAKTEABTEILUNGENZUORDNUNGEN KZA';
        $SQL .= ' ON KZA.KZA_KAB_KEY = KIN.KIN_WERT';
        $SQL .= ' INNER JOIN KONTAKTEABTEILUNGEN KAB';
        $SQL .= ' ON KAB.KAB_KEY = KZA.KZA_KAB_KEY';
        $SQL .= ' WHERE KZA.KZA_KON_KEY= ' . $this->_DB->WertSetzen('KIN', 'N0', $this->_AWISBenutzer->BenutzerKontaktKEY());
        $SQL .= ' AND KON.KON_STATUS = \'A\'';

        $rsAbteilung = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('KIN'));

        if ($rsAbteilung->AnzahlDatensaetze() > 0) {

            $AlteAbteilung = '';

            while (!$rsAbteilung->EOF()) {
                if ($AlteAbteilung != $rsAbteilung->FeldInhalt('KAB_ABTEILUNG')) {  //Schauen ob neuer Datensatz = alter Datensatz ist -> wenn ja: keine �berschrift
                    $this->_Form->ZeileStart('width: 100%; background-color: ccc; overflow: hidden');
                    $this->_Form->Erstelle_TextLabel($rsAbteilung->FeldInhalt('KAB_ABTEILUNG'), 360, 'font-weight: bold');
                    $this->_Form->ZeileEnde();
                }
                $AlteAbteilung = $rsAbteilung->FeldInhalt('KAB_ABTEILUNG');

                $this->_Form->ZeileStart('width: 100%; background-color: ddd; overflow: hidden');
                $Link2 = '../telefon/telefon_Main.php?cmdAktion=Liste&txtKONKey=' . $rsAbteilung->FeldInhalt('KON_KEY');
                $this->_Form->Erstelle_TextLabel($rsAbteilung->FeldInhalt('KON_NAME2') . ' ' . $rsAbteilung->FeldInhalt('KON_NAME1'), 360, '', $Link2 . $rsAbteilung->FeldInhalt('KON_NAME2' . 'KON_NAME1'));
                $this->_Form->ZeileEnde();

                $rsAbteilung->DSWeiter();
            }
        } else {

            $this->_Form->ZeileStart();
            $this->_Form->Hinweistext($this->_AWISSprachKonserven['KONTAKTAUFNAHME']['KONTAKTAUFNAHME_FEHLER'], awisFormular::HINWEISTEXT_WARNUNG);
            $this->_Form->ZeileEnde();
        }

        $this->_Form->SchreibeHTMLCode('</div>');
    }

}

?>
