<?php

require_once 'awisBenutzer.inc';
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

/**
 * Created by PhpStorm.
 * User: roesch_n
 * Date: 07.11.2016
 * Time: 11:01
 */
class versicherungen_Funktionen
{
    private $_Form;
    private $_DB;
    private $_Benutzer;
    private $_AWISSprachKonserven;

    function __construct()
    {
        $this->_Benutzer = awisBenutzer::Init();
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
        $this->_Form = new awisFormular();
    }

    public function ErstelleAutoglasUebersicht($AutoglasKey)
    {
        $TextKonserven = array();

        $TextKonserven[] = array('FGP', 'FGP*');
        $TextKonserven[] = array('FPH', 'FPH_*');
        $TextKonserven[] = array('FGK', '*');
        $TextKonserven[] = array('FKH', '*');
        $TextKonserven[] = array('Wort', 'lbl_weiter');
        $TextKonserven[] = array('Wort', 'lbl_speichern');
        $TextKonserven[] = array('Wort', 'lbl_trefferliste');
        $TextKonserven[] = array('Wort', 'lbl_aendern');
        $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
        $TextKonserven[] = array('Wort', 'lbl_loeschen');
        $TextKonserven[] = array('Wort', 'lbl_zurueck');
        $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
        $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
        $TextKonserven[] = array('Wort', 'lbl_Hilfe');
        $TextKonserven[] = array('Wort', 'Seite');
        $TextKonserven[] = array('Wort', 'Zugriffsrechte');
        $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
        $TextKonserven[] = array('Liste', 'lst_JaNeinUnbekannt');
        $TextKonserven[] = array('Fehler', 'err_keineRechte');
        $TextKonserven[] = array('Fehler', 'err_keineDaten');
        $TextKonserven[] = array('Wort', 'txt_FGBruttopreis');

        $this->_AWISSprachKonserven = $this->_Form->LadeTexte($TextKonserven);

        $SQL = 'SELECT kopf.*';
        $SQL .= '      FROM (';
        $SQL .= '      SELECT fgk.FGK_KEY AS KOPF_KEY,';
        $SQL .= '      fgk.FGK_FILID AS FILID,';
        $SQL .= '      fgk.FGK_VORGANGNR AS VORGANGNR,';
        $SQL .= '      fgk.FGK_KFZBEZ AS KFZBEZ,';
        $SQL .= '      fgk.FGK_KFZKENNZ AS KFZKENNZ,';
        $SQL .= '      fgk.FGK_SB AS SB,';
        $SQL .= '      fgk.FGK_MONTAGEDATUM AS MONTAGEDATUM,';
        $SQL .= '      fgk.FGK_ZEITSTEMPEL AS ZEITSTEMPEL,';
        $SQL .= '      fgk.FGK_FREIGABEGRUND AS FREIGABEGRUND,';
        $SQL .= '      fgk.FGK_VERSICHERUNG AS VERSICHERUNG,';
        $SQL .= '      fgk.FGK_VERSNR AS VERSNR,';
        $SQL .= '      fgk.FGK_NOTIZ AS NOTIZ,';
        $SQL .= '      fgk.FGK_BETRAG_NETTO AS BETRAG_NETTO,';
        $SQL .= '      fgk.FGK_MWST AS MWST,';
        $SQL .= '      fgk.FGK_BETRAG_BRUTTO AS BRUTTO';
        $SQL .= '      FROM FGKOPFDATEN fgk';
        $SQL .= '      UNION';
        $SQL .= '      SELECT fkh.fkh_KEY AS KOPF_KEY,';
        $SQL .= '      fkh.fkh_FILID AS FILID,';
        $SQL .= '      fkh.fkh_VORGANGNR AS VORGANGNR,';
        $SQL .= '      fkh.fkh_KFZBEZ AS KFZBEZ,';
        $SQL .= '      fkh.fkh_KFZKENNZ AS KFZKENNZ,';
        $SQL .= '      fkh.fkh_SB AS SB,';
        $SQL .= '      fkh.fkh_MONTAGEDATUM AS MONTAGEDATUM,';
        $SQL .= '      fkh.fkh_ZEITSTEMPEL AS ZEITSTEMPEL,';
        $SQL .= '      fkh.fkh_FREIGABEGRUND AS FREIGABEGRUND,';
        $SQL .= '      fkh.fkh_VERSICHERUNG AS VERSICHERUNG,';
        $SQL .= '      fkh.fkh_VERSNR AS VERSNR,';
        $SQL .= '      fkh.fkh_NOTIZ AS NOTIZ,';
        $SQL .= '      fkh.fkh_BETRAG_NETTO AS BETRAG_NETTO,';
        $SQL .= '      fkh.fkh_MWST AS MWST,';
        $SQL .= '      fkh.fkh_BETRAG_BRUTTO AS BRUTTO';
        $SQL .= '      FROM FGKOPFDATEN_HIST fkh) kopf';
        $SQL .= '      WHERE kopf.KOPF_KEY = ' . $this->_DB->WertSetzen('FGK', 'N0', $AutoglasKey);

        $rsFGK = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FGK'));

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_VERSNR'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('FGK_VERSNR', $rsFGK->FeldInhalt('VERSNR'), 20, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_VERSICHERUNG'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('FGK_VERSICHERUNG', $rsFGK->FeldInhalt('VERSICHERUNG'), 150, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_SB'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('FGK_SB', $rsFGK->FeldInhalt('SB'), 7, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->Trennzeile('O');

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_KFZKENNZ'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('FGK_KFZKENNZ', $rsFGK->FeldInhalt('KFZKENNZ'), 10, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_KFZBEZ'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('FGK_KFZBEZ', $rsFGK->FeldInhalt('KFZBEZ'), 150, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_MONTAGEDATUM'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('FGK_MONTAGEDATUM', $rsFGK->FeldInhalt('MONTAGEDATUM'), 30, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->Trennzeile('O');

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['POSITIONSDATEN'] . ':', 150, 'font-weight:bold; font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['PREISE'] . ':', 150, 'font-weight:bold; font-size:80%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_BETRAG_NETTO'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('BETRAG_NETTO', $rsFGK->FeldInhalt('BETRAG_NETTO'), 30, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_MWST'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('MWST', $rsFGK->FeldInhalt('MWST'), 30, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_TextLabel($this->_AWISSprachKonserven['FGK']['FGK_BETRAG_BRUTTO'] . ':', 150);
        $this->_Form->Erstelle_TextFeld('BRUTTO', $rsFGK->FeldInhalt('BRUTTO'), 30, 300, false, 'font-size:85%');
        $this->_Form->ZeileEnde();

        $this->_Form->Trennzeile('O');

        $SQL = 'SELECT pos.*';
        $SQL .= '      FROM';
        $SQL .= '        ( SELECT fgp.FGP_FGK_KEY                       AS POS_KEY,';
        $SQL .= '        FGP_VORGANGNR                              AS VORGANGNR,';
        $SQL .= '        FGP_ARTNR                                  AS ARTNR,';
        $SQL .= '        FGP_ARTBEZ                                 AS ARTBEZ,';
        $SQL .= '        FGP_ANZAHL                                 AS ANZ,';
        $SQL .= '        FGP_EINHEIT                                AS EINHEIT,';
        $SQL .= '        FGP_ZEITSTEMPEL                            AS ZEITSTEMPEL,';
        $SQL .= '        ROUND((FGP_OEMPREIS / 100) * (100 + 19),2) AS PREIS_BRUTTO,';
        $SQL .= '        FGP_OEMPREIS                               AS OEMPREIS';
        $SQL .= '        FROM FGPOSITIONSDATEN fgp';
        $SQL .= '        INNER JOIN FGKOPFDATEN fgk';
        $SQL .= '        ON fgk.FGK_VORGANGNR = fgp.FGP_VORGANGNR';
        $SQL .= '        UNION';
        $SQL .= '         SELECT fph.FPH_FKH_KEY                           AS POS_KEY,';
        $SQL .= '        fph.FPH_VORGANGNR                              AS VORGANGNR,';
        $SQL .= '        fph.FPH_ARTNR                                  AS ARTNR,';
        $SQL .= '        fph.FPH_ARTBEZ                                 AS ARTBEZ,';
        $SQL .= '        fph.FPH_ANZAHL                                 AS ANZ,';
        $SQL .= '        fph.FPH_EINHEIT                                AS EINHEIT,';
        $SQL .= '        fph.FPH_ZEITSTEMPEL                            AS ZEITSTEMPEL,';
        $SQL .= '        ROUND((fph.FPH_OEMPREIS / 100) * (100 + 19),2) AS PREIS_BRUTTO,';
        $SQL .= '        fph.FPH_OEMPREIS                               AS OEMPREIS';
        $SQL .= '        FROM FGPOSITIONSDATEN_HIST fph';
        $SQL .= '        INNER JOIN FGKOPFDATEN fgk';
        $SQL .= '        ON fgk.FGK_VORGANGNR = fph.FPH_VORGANGNR';
        $SQL .= '        ) pos';
        $SQL .= '      INNER JOIN VERSVORGANGSSTATUS vvs';
        $SQL .= '      ON vvs.VVS_VORGANGNR = pos.VORGANGNR';
        $SQL .= '      WHERE pos.POS_KEY = ' . $this->_DB->WertSetzen('FGP', 'N0', $AutoglasKey);

        $rsFGP = $this->_DB->RecordSetOeffnen($SQL, $this->_DB->Bindevariablen('FGP'));

        $FeldBreiten = array();
        $FeldBreiten['ARTNR'] = 120;
        $FeldBreiten['ARTBEZ'] = 500;
        $FeldBreiten['ANZAHL'] = 100;
        $FeldBreiten['EINHEIT'] = 100;
        $FeldBreiten['OEMPREIS'] = 100;
        $FeldBreiten['PREIS'] = 100;

        $Steuer = '1.' . $rsFGP->FeldInhalt('ZEITSTEMPEL');

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['FGP']['FGP_ARTNR'], $FeldBreiten['ARTNR'], '');
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['FGP']['FGP_ARTBEZ'], $FeldBreiten['ARTBEZ'], '');
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['FGP']['FGP_ANZAHL'], $FeldBreiten['ANZAHL'], '');
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['FGP']['FGP_EINHEIT'], $FeldBreiten['EINHEIT'], '');
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['FGP']['FGP_OEMPREIS'], $FeldBreiten['OEMPREIS'], '');
        $this->_Form->Erstelle_Liste_Ueberschrift($this->_AWISSprachKonserven['Wort']['txt_FGBruttopreis'], $FeldBreiten['PREIS'], '');

        $this->_Form->ZeileEnde();

        $FGKZeile = 0;
        $Gesamtpreis = 0;

        while(!$rsFGP->EOF())
        {
            $Menge = 0.0;

            $this->_Form->ZeileStart();
            $this->_Form->Erstelle_ListenFeld('FGP_ARTNR', $rsFGP->FeldInhalt('ARTNR'), 0, $FeldBreiten['ARTNR'], false, ($FGKZeile % 2), '', '');
            $this->_Form->Erstelle_ListenFeld('FGP_ARTBEZ', $rsFGP->FeldInhalt('ARTBEZ'), 0, $FeldBreiten['ARTBEZ'], false, ($FGKZeile % 2), '', '');
            $this->_Form->Erstelle_ListenFeld('FGP_ANZAHL', $rsFGP->FeldInhalt('ANZ'), 0, $FeldBreiten['ANZAHL'], false, ($FGKZeile % 2), '', '');
            $this->_Form->Erstelle_ListenFeld('FGP_EINHEIT', $rsFGP->FeldInhalt('EINHEIT'), 0, $FeldBreiten['EINHEIT'], false, ($FGKZeile % 2), '', '');
            $this->_Form->Erstelle_ListenFeld('FGP_OEMPREIS', $rsFGP->FeldInhalt('OEMPREIS'), 0, $FeldBreiten['OEMPREIS'], false, ($FGKZeile % 2), '', '', 'N2');
            $this->_Form->Erstelle_ListenFeld('txt_FGBruttopreis', $rsFGP->FeldInhalt('PREIS_BRUTTO'), 0, $FeldBreiten['PREIS'], false, ($FGKZeile % 2), '', '', 'N2');
            $this->_Form->ZeileEnde();

            $Menge = $rsFGP->FeldInhalt('ANZ');
            $Gesamtpreis += ($Menge * str_replace(',', '.', $rsFGP->FeldInhalt('OEMPREIS'))) * 1.19;
            $FGKZeile++;

            $rsFGP->DSWeiter();
        }

        $this->_Form->ZeileStart();
        $this->_Form->Erstelle_ListenFeld('FGP_GEPREIS', 'Gesamtpreis:', 0, $FeldBreiten['ARTNR'], false, ($FGKZeile % 2), 'font-weight:bold', '', '', 'R');
        $this->_Form->Erstelle_ListenFeld('DUMMY', '', 0, $FeldBreiten['ARTBEZ'], false, ($FGKZeile % 2));
        $this->_Form->Erstelle_ListenFeld('DUMMY', '', 0, $FeldBreiten['ANZAHL'], false, ($FGKZeile % 2));
        $this->_Form->Erstelle_ListenFeld('DUMMY', '', 0, $FeldBreiten['EINHEIT'], false, ($FGKZeile % 2));
        $this->_Form->Erstelle_ListenFeld('DUMMY', '', 0, $FeldBreiten['OEMPREIS'], false, ($FGKZeile % 2));
        $this->_Form->Erstelle_ListenFeld('FGP_Brutto', $this->_Form->Format('N2', $Gesamtpreis), 0, $FeldBreiten['PREIS'], false, ($FGKZeile % 2), 'font-weight:bold', '', 'N2');
        $this->_Form->ZeileEnde();
    }

}