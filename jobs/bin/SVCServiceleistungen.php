<?php
require_once('/daten/include/awisJobVerwaltung.inc');
require_once('awisServiceleistungen.inc');

try {
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;

    for ($i = 1; $i < $argc; $i++) {
        switch (strtolower($argv[$i])) {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                echo 'Debug auf ' . $DebugLevel . ' setzen...' . PHP_EOL;
                break;
            case '--jobid':
                $JobId = $argv[++$i];
                break;
            default:
                echo 'Parameter ' . $argv[$i] . ' ist unbekannt.';
            case '--help':
                echo $argv[0] . PHP_EOL;
                echo 'Optionen:' . PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.' . PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).' . PHP_EOL;
                echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.' . PHP_EOL;

                echo PHP_EOL;
                die();
        }
    }

    $AWISUser = awisBenutzer::init($Benutzer);

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    if ($JobId != 0) {
        $Job = new awisJobProtokoll($JobId, 'Starte Serviceleistungen - Import.');
    }

    $SVL = new awisServiceleistungen('awis_jobs');

    $SVL->DebugLevel($DebugLevel);
    $SVL->ImportiereDaten(1);

    if ($JobId != 0) {
        $Job->SchreibeLog(3, 1, 'Daten wurden eingelesen.');
    }

    if ($JobId != 0) {
        $Job->SchreibeLog(1, 0, 'Ende des Jobs.');
    }
} catch (awisException $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'SQL: ' . $ex->getSQL() . PHP_EOL;
    if ($JobId != 0) {
        $Job->SchreibeLog(3, 9, 'Fehler bei der Ausfuehrung:' . $ex->getMessage());
    }
} catch (Exception $ex) {
    echo 'FEHLER: ' . $ex->getMessage() . PHP_EOL;
    echo 'FEHLERZeile: ' . $ex->getLine() . PHP_EOL;
    if ($JobId != 0) {
        $Job->SchreibeLog(3, 9, 'Fehler bei der Ausfuehrung:' . $ex->getMessage());
    }
}
?>

