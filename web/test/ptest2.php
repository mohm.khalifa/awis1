<html>
<head>
<title>Awis - ATU webbasierendes Informationssystem - Filesystemhistorie</title>
<link rel=stylesheet type=text/css href=/ATU.css>
<meta http-equiv="expires" content="0">
</head>

<body>

<?php

include("funktionen.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
require_once("admin.inc.php");
//require_once("image.inc.php");
include ("ATU_Header.php");	// Kopfzeile
include( "pc4p_init.inc" );

//$test = array("Wert1" => array(1, 11), "Wert2" => array(22, 2222), "Wert3" => array(3333, 333));
//print_r($test);
//maxSpaltenbreite($test);

//define( "PC4PDEBUG", 1 );

$admcon = awisAdminLogon();

if($admcon)
{
	$rname = "atlx06su72";
	$rsStat = create_fshist_stat_array($admcon,$rname);
print_r($rsStat);
print "<hr>";


	$PDF = &pc4p_create_pdf( array( "Author" => "Stefan Pongratz", "Title" => "PC4P-Test" ) );
	$PDF->pc4p_set_compatibility( "1.4" );
	$PDF->pc4p_set_compression( 9 );
	$PDF->pc4p_set_savepdf( "drive", "tabelle.pdf" );

//	pdf_set_font($PDF->pdfp, "Times-Roman", 10, "winansi");
print "<hr>";

	$Page1 = &pc4p_create_page( $PDF, "a4", "landscape" );
	
	$Page1->pc4p_set_margin(array("top"=>20, "bottom"=>20, "left"=>30, "right"=>10));
	$Page1->pc4p_set_newpage( 1 );


	$kopf = &pc4p_create_object( $Page1->header, "pagenumber");
	$kopf->pc4p_set_text("- %P -");
	$kopf->pc4p_set_alignment("center");

//	$fuss = &pc4p_create_object( $Page1->footer, "pagenumber");
	$fuss = &pc4p_create_object( $Page1->header, "pagenumber");
	$fuss->pc4p_set_text(strftime("%d.%m.%Y %R"));
//	$fuss->pc4p_set_alignment("right");
	$fuss->pc4p_set_alignment("left");

	$rsbreite = maxSpaltenbreite($PDF->pdfp, $rsStat);
	$rscolsize = calcColsize($rsbreite);

	$table3 = &pc4p_create_object( $Page1, "table" );
	$table3->pc4p_create_tablematrix( count($rsStat) );
	$table3->pc4p_set_width( 400 );
	$table3->pc4p_set_tableheader( 0 );
	$table3->pc4p_add_tablerow();

		for ($i=0; $i<count($rsbreite); $i++)
		{
			$text1 = &pc4p_create_object(  $table3->cell[ $i ][ 0 ], "text" );
			$table3->pc4p_set_cellfillcolor( $table3->cell[ $i ][ 0 ], "#C0C0C0" );
			$text1->pc4p_set_alignment( "left" );
			$text1->pc4p_set_font( "Courier", 8 );
			$text1->pc4p_set_text( key($rsbreite) );
			next($rsbreite);
		}


//	$table4 = &pc4p_create_object( $table3->cell[ 0 ][ 0 ] , "table" );
//	$table4->pc4p_create_tablematrix( count($rsStat) );
/*	$table4->pc4p_set_tablecolsize( $rscolsize );
	$table4->pc4p_set_tableborder();

	$table4->pc4p_set_cellfillcolor( $table4->cell[ 0 ][ 0 ], "lime" );
	$table4->pc4p_set_cellfillcolor( $table4->cell[ 1 ][ 0 ], "#008080" );
		$text1 = &pc4p_create_object( $table4->cell[ 0 ][ 0 ] , "text" );
		$text1->pc4p_set_alignment( "center" );
		$text1->pc4p_set_text( "This" );
*/
	for( $i = 1; $i <= count($rsStat["FS_ID"]); $i++) 
	{
		reset($rsbreite);
		$table3->pc4p_add_tablerow();
		for ($j=0; $j<count($rsbreite); $j++)
		{
			$text1 = &pc4p_create_object(  $table3->cell[ $j ][ $i ], "text" );
			$text1->pc4p_set_alignment( "left" );
			$text1->pc4p_set_font( "Courier", 10 );
//			$text1->pc4p_set_text( "i: ".$i." j: ".$j );
			$text1->pc4p_set_text( $rsStat[key($rsbreite)][$i - 1] );
			next($rsbreite);
		}
	}

	$table3->pc4p_set_tablecolsize( $rscolsize );
	
	$table3->pc4p_set_tableborder();
	
print $text1->fontname."<br>";
print $text1->fontsize."<br>";

	// Now I draw the PDF and (hopefully) get the output
	$PDF->pc4p_draw();

	awisLogoff($admcon);
}
else
{
	print ("Angemeldung fehlgeschlagen!");
} // If($admcon)	

print "Fertsch! ".strftime("%d.%m.%Y %X")."<br>";

//exit;

/* OCIDefineByPos example thies@thieso.net (980219) */

/*
include("config.inc.php");
include("funktionen.inc.php");

//tabelle("Text");

$test = array("Wert1" => 1, "Wert2" => 2, "Wert3" => 3);
tabelle($test, 5);
echo "<p>";
tabelle("select FG_DATUM Datum, FG_FEIERTAG Feiertag from feiertage");
echo "<p>";

$sql = "select EI_KEY Nr, EI_EIGENSCHAFT Eigenschaft, ET_Bez Typ from eigenschaften, eigenschaftstypen";
$sql = $sql." where ei_et_id = et_id";
tabelle($sql);


exit;

*/


/* the define MUST be done BEFORE ociexecute! */
/*
OCIDefineByName($stmt,"FG_DATUM",&$datum);
OCIDefineByName($stmt,"FG_FEIERTAG",&$ftag);

OCIExecute($stmt);

while (OCIFetch($stmt))
{
        echo "Datum:".$datum." ";
        echo "Feiertag:".$ftag."<br>";
}
*/
?>

<a href=tabelle.pdf target=_self>tabelle.pdf</a>
</BODY>
</HTML>

