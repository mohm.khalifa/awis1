#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung fr ein Modul
#
#	Abschnitt
#	[Header]	Infos fr die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History für das Modul
#
#########################################################################

[Header]

Modulname=R&uuml;ckf&uuml;hrungsanforderung
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE,CZ,NL,IT,CH

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
0.00.05;Einführung Rückführungskennung W;12.03.2015;Patrick Gebhardt
0.00.04;Filialauswahl - Franchiser hinzugefuegt;11.02.2015;Patrick Gebhardt
0.00.03;Aktuelles Rückführungskennzeichen mit speichern;21.09.2010;Thomas Riedl
0.00.02;Datumsprüfung bei Import der Rückführungsaufforderung hinzugefügt;15.02.2010;Thomas Riedl
0.00.01;Erste Version;15.10.2008;Christian Argauer

