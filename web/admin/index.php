<html>
<head>
<title>Awis 2.00.00 - ATU webbasierendes Informationssystem</title>
<link rel=stylesheet type=text/css href=ATU.css>
</head>

<body>
<?php
require_once("db.inc.php");		// DB-Befehle
require_once("register.inc.php");
require_once("sicherheit.inc.php");
global $AWISBenutzer;

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";

include ("ATU_Header.php");	// Kopfzeile
$con = awisLogon();

	// Beginn Hauptmen�
print "<table border=0 width=100%><tr><td><h1 id=SeitenTitel>Administration</h1></td><td align=right>Anmeldename:".$AWISBenutzer->BenutzerName()."</td></tr></table>";

					// �berschrift der Tabelle
print "<table border=0 width=100%><tr><th align=left id=FeldBez>Aktion</th><th align=left id=FeldBez>Beschreibung</th></tr>";

//if(awisHatDasRecht(1) || $REMOTE_ADDR=="125.0.3.20")
//{
//}
$con = awislogon();
$RechteStufe1 = awisBenutzerRecht($con, 1);
$RechteStufe3 = awisBenutzerRecht($con, 3);
$RechteStufe4 = awisBenutzerRecht($con, 4);
$RechteStufe10 = awisBenutzerRecht($con, 10);
$RechteStufe2100 = awisBenutzerRecht($con, 2100);
$RechteStufe3124 = awisBenutzerRecht($con, 3124); 
$RechteStufe29000 = awisBenutzerRecht($con, 29000);
awislogoff($con);



	if(($RechteStufe10&1)==1)
	{
					// Apache-Doc
//		print "<tr><td><a href=http://" . getenv('HOST') ."/manual/index.html.en>Apache-Doc</a></td><td>Dokumentation zu Apache</td></tr>";
	}
	
	if(($RechteStufe3124&32)==32)
	{
		print "<tr><td><a href=/admin/appserver/showdir.php?dir=awis>Applikationserver - AWIS</a></td><td>Webzugriff (ReadOnly) auf Applikationserver Verzeichnisse/Dateien des Users AWIS.</td></tr>";		
		print "<tr><td><a href=/admin/appserver/showdir.php?dir=awisimport>Applikationserver - AWISIMPORT</a></td><td>Webzugriff (ReadOnly) auf Applikationserver Verzeichnisse/Dateien des Users AWISIMPORT.</td></tr>";		
	}
	
	if(($RechteStufe4&1)==1)
	{
		print "<tr><td><a href=/admin/Benutzerverwaltung/benutzerverwaltung.php>Benutzerverwaltung (ALT)</a></td><td>Verwaltung von Benutzern und Rechten.</td></tr>";		
	}
	if(($RechteStufe3&1)==1)
	{
		print "<tr><td><a href=./benutzerundgruppen/benutzerverwaltung_Main.php>Benutzerverwaltung</a></td><td>Verwaltung von Benutzern und Rechten.</td></tr>";		
	}
	if(awisHatDasRecht(25))
	{
		print "<tr><td><a href=./benutzerundgruppen/gruppenverwaltung_Main.php>Benutzergruppenverwaltung</a></td><td>Verwaltung von Benutzergruppen.</td></tr>";
	}
	if(($RechteStufe2100&1)==1)
	{
		print "<tr><td><a href=../portalzugaenge/portalzugaenge_Main.php>Portalzug&auml;nge</a></td><td>Zug&auml;nge f�r das ATU Portal verwalten.</td></tr>";
	}
	if(($RechteStufe10&4)==4)
	{
		print "<tr><td><a href=/admin/db_infos_Main.php>DB-Infos</a></td><td>Verschiedene Informationen �ber die Datenbank selbst.</td></tr>";
	}

	if(($RechteStufe10&1)==1)
	{
		print "<tr><td><a href=http://dokuwiki.server.atu.de/mediawiki/index.php/Hauptseite>Dokuwiki</a></td><td>Interner Wiki-Server f�r Dokumentationen.</td></tr>";
	}
	
	if(($RechteStufe29000&1)==1)
	{
		print "<tr><td><a href=../email_portal/portal_mail_Main.php>Email-Portal</a></td><td>Abrufen der zugeordneten Emails zu Personalnummer des Direktzu Portals.</td></tr>";
	}
	

	if(($RechteStufe10&2)==2)
	{
		print "<tr><td><a href=./ereignisse/ereignisse_Main.php>Ereignisse</a></td><td>Zeigt die Ereignisse von AWIS an.</td></tr>";
	}

// 	print "<tr><td><a href=/admin/filesystem.htm>Filesysteme</a></td><td>Aktuelle Daten der versch. Filesysteme.</td></tr>";

// 	print "<tr><td><a href=/admin/filesystemhistorie.htm>Filesystemehistorie</a></td><td>Entwicklung der versch. Filesysteme.</td></tr>";

	if(($RechteStufe10&4)==4)
	{
		print "<tr><td><a href=/admin/journal.php>Journal</a></td><td>Informationen �ber die Cron-Jobs (Im- und Export).</td></tr>";
	}

	if(($RechteStufe10&1)==1)
	{
					// Jpgraph-Doc
//		print "<tr><td><a href=http://" . getenv('HOST') ."/admin/jpgraphdoc/index.html>Jpgraph-Doc</a></td><td>Dokumentation zu Jpgraph</td></tr>";
	}

	if(($RechteStufe10&1)==1)
	{
		print "<tr><td><a href=https://kvmswitch.server.atu.de target=_blank>KVM-Switch</a></td><td>Interner KVM-Server f�r Remotezugriff.</td></tr>";
	}

	if(($RechteStufe10&1)==1)
	{
		print "<tr><td><a href=./loadbalancer.php>Loadbalancer</a></td><td>Zeigt den Zustand des Loadbalancers und das Logfile.</td></tr>";
	}

	if(($RechteStufe10&1)==1)
	{
		print "<tr><td><a href=./nfsmounts.php>NFS-Mounts</a></td><td>Zeigt die gemounteten NFS-Verzeichnisse der einzelnen Rechner an.</td></tr>";
	}

	if(($RechteStufe10&1)==1)
	{
		print "<tr><td><a href=/phpinfo.php>Php info</a></td><td>Infos zum PHP System.</td></tr>";
	}

	if(($RechteStufe10&8)==8)
	{
	    print "<tr><td><a href=./web_reifen_imp.php>Reifenimport</a></td><td>Import aller Reifendateien (nach der Verarbeitung im Shuttle).</td></tr>";
	}


	
	if(($RechteStufe10&8)==8)
	{
		print "<tr><td><a href=./seitenverteilung/seitenverteilung_Main.php>Seitenverteilung</a></td><td>Gleicht die Seiten des DB- und der Web-Server an.</td></tr>";
	}

	if(($RechteStufe10&2)==2)
	{
		print "<tr><td><a href=./systemdaten_xml.php>Systemdaten</a></td><td>Anzeige verschiedener Systemdaten als XML</td></tr>";
	}

	if(($RechteStufe10&2)==2)
	{
		print "<tr><td><a href=./systemdaten_xml_lesen.php>Systemdaten lesen</a></td><td>Sammeln der verschiedenen XML-Systemdaten</td></tr>";
	}

	if(($RechteStufe10&1)==1)
	{
		print "<tr><td><a href=./statistik.php>Zugriffsstatistik</a></td><td>Apachestatistiken.</td></tr>";
	}	
	
	if(($RechteStufe10&1)==1)
	{
		print "<tr><td><a href=../entwicklung/AWIS-Framework/index.html target=_blank>AWIS-Framework</a></td><td>PHP-Doc f�r AWIS-Framework.</td></tr>";
	}
	

print "</table>";	//Ende Hauptmen�
print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='../index.php';>";

if($AWISBenutzer->BenutzerName()=='entwick')
{
 include("debug_info.php");
 //print "<br><a href=/phpinfo.php>php info</a>";
 //print "<br><a href=/stats/index.html>Zugriffsstatistik</a>";
}

?>

</body>
</html>

