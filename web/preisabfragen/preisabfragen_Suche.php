<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PAF','%');
	$TextKonserven[]=array('AST','AST_ATUNR');
	$TextKonserven[]=array('FIL','FIL_ID');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','Datum%');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht8001 = $AWISBenutzer->HatDasRecht(8001);
	if($Recht8001==0)
	{
	    awisEreignis(3,1000,'Preisabfragen',$AWISBenutzer->BenutzerName(),'','','');
		$Form->Formular_Start();
		$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
		$Form->Formular_Ende();
		die();
	}

	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./preisabfragen_Main.php?cmdAktion=Details>");

	/**********************************************
	* Eingabemaske
	***********************************************/

	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['PAF']['PAF_BEZEICHNUNG'].':',190);
	$Form->Erstelle_TextFeld('*PAF_BEZEICHNUNG','',20,200,true);
	$AWISCursorPosition='sucPAF_BEZEICHNUNG';
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ATUNR'].':',190);
	$Form->Erstelle_TextFeld('*AST_ATUNR','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].':',190);
	$Form->Erstelle_TextFeld('*FIL_ID','',20,200,true);
	$Form->ZeileEnde();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',190);
	$Form->Erstelle_TextFeld('*DATUMVOM','',20,200,true);
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumBis'].':',190);
	$Form->Erstelle_TextFeld('*DATUMBIS','',20,200,true);
	$Form->ZeileEnde();

	$Form->Formular_Ende();


	$Form->SchaltflaechenStart();
		// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	if(($Recht8001&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201431");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201004201432");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>