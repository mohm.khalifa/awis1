<html>
<head>
<meta http-equiv="content-type" content="text/html"; charset="windows-1252">
</head>
<?php

global $AWISWebSrv;

require_once("operating_AWIS_config.php");

require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');
require_once('awisFormular.inc');

global $AWISCursorPosition;		// Aus AWISFormular

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$AWISBenutzer = awisBenutzer::Init();
	echo "<link rel=stylesheet type=text/css href=" . $AWISBenutzer->CSSDatei() .">";
}
catch (Exception $ex)
{
	die($ex->getMessage());
}
include ("awisHeader.inc");	// Kopfzeile

try
{
	$Form = new awisFormular();

	if ($AWISBenutzer->HatDasRecht(3124) == 0 || $AWISBenutzer->HatDasRecht(3124)&32 !== 32 )
	{
	    $Form->Fehler_KeineRechte();
	}


if (!empty($_GET["file"])) {
	
	if ($_GET["file"]=="/" || substr($_GET["file"],0,1)=="/") 
	{
		print "<h1>Kein Zugriff auf ROOT-Verzeichnis erlaubt</h1>";
	  	exit (0);	
	}
	elseif (substr($_GET["file"],0,3)=="../" || substr($_GET["file"],0,2)=="..")	
	{
		print "<h1>Pfadwechsel nicht erlaubt.</h1>";
	  	exit (0);	
	}	
	
  print "<h1>". $AWISAppSrv . ' - ' . basename ($_GET["file"]) ."</h1>";
  $myfile = @fopen ($_GET["file"], "r");
  if ($myfile == FALSE) {
    print "<h2>Kann Datei nicht &ouml;ffnen.</h2>";
    exit (0);
  };
} else {
  print "<h1>Keine Datei angegeben</h1>";
  exit (0);
}

$dir_back=substr($_GET["file"],0,strripos($_GET["file"],"/"));
if($dir_back!="/")
{
        echo "<h2><a href=./showdir.php?dir=$dir_back>Verzeichnis nach oben</a></h2>";
}

if (!empty($_GET["search"]))
{
$search=$_GET["search"];
echo "<br><h2>Suchtext: $search<h2><br>";
}
else
{$search="";}

print "<table border=\"0\">";

$line_nr = 0;

while (!feof ($myfile)) {
 if ($search!="")
 {
  $line = fgets ($myfile, 4096);
  if(stristr($line,$search)!== FALSE)
  {
   $line_nr++;
   print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
   print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
  }
 }
 else
 {	
  $line = fgets ($myfile, 4096);
  $line_nr++;
  print "<tr><td bgcolor=\"#d7d7d7\"><b>{$line_nr}</b>&nbsp;</td>";
  print "<td><code>". htmlspecialchars($line) ."</code></td></tr>";
 }
}
fclose($myfile);
print "</table>";

}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'AWIS: '.$ex->getMessage();
	}
}

?>
</html>
