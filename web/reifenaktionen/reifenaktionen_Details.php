<?php

global $AWISBenutzer;
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once 'reifenaktionen_Details_Funktionen.php';

try
{
    $TextKonserven = array();
    $TextKonserven[] = array('RRA', '*');
    $TextKonserven[] = array('RRP', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'Status');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste','lst_KENN1');
    $TextKonserven[] = array('Liste','lst_KENN2');
    $TextKonserven[] = array('Liste','lst_KENN_L2');
    $TextKonserven[] = array('Liste','lst_ALLE_0');
    $TextKonserven[] = array('Liste','lst_ALLE_NULL');
    
    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht15200 = $AWISBenutzer->HatDasRecht(15300);
    if ($Recht15200 == 0)
    {
        $Form->Fehler_KeineRechte();
    }

    //********************************************************
    // Parameter ?
    //********************************************************
    $Form->DebugAusgabe(1, $_POST, $_GET);
//    $Form->DebugAusgabe(1, $_GET);
    $Param = unserialize($AWISBenutzer->ParameterLesen("Formular_RRA"));
    
    if (isset($_POST['cmdSuche_x']))
    {
        $Param['BLOCK'] = 1;
        $Param['KEY'] = 0;

        $Param['ZEITSTATUS'] = $_POST['sucZEITSTATUS'];
        $Param['RRA_ATUNR'] = $_POST['sucRRA_ATUNR'];
        $Param['RRA_AKTIONSNAME'] = $_POST['sucRRA_AKTIONSNAME'];
        $Param['RRA_GUELTIGAB'] = $_POST['sucRRA_GUELTIGAB'];
        $Param['RRA_GUELTIGBIS'] = $_POST['sucRRA_GUELTIGBIS'];
        $Param['RRA_LAN_CODE'] = $_POST['sucRRA_LAN_CODE'];
        $Param['RRA_REH_KEY'] = $_POST['sucRRA_REH_KEY'];
        $Param['RRA_WGR_ID'] = $_POST['txtRRA_WGR_ID'];
        $Param['RRA_WUG_ID'] = $_POST['txtRRA_WUG_ID'];
        $Param['RRA_KENN1'] = $_POST['sucRRA_KENN1'];
        $Param['RRA_AKTIONSTEXT'] = $_POST['sucRRA_AKTIONSTEXT'];

        $Param['SPEICHERN'] = (isset($_POST['sucAuswahlSpeichern'])?'on':'off');

		$AWISBenutzer->ParameterSchreiben("Formular_RRA",serialize($Param));
    }
    elseif (isset($_POST['cmdSpeichern_x']))
    {
        include('reifenaktionen_speichern.php');
    }
    elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
        include './reifenaktionen_loeschen.php';
    }
    elseif (isset($_POST['cmdDSNeu_x']))
    {
         $AWIS_KEY1 = -1;
    }
    elseif (isset($_GET['RRA_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_GET['RRA_KEY']);
    }
    else
    {
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			//$AWISBenutzer->ParameterSchreiben('Formular_RRA',serialize($Param));
		}

		if(isset($_GET['Liste'])) // OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
    }
    
    //*****************************************************************
    // Sortierung aufbauen
    //*****************************************************************
    if (!isset($_GET['Sort']))
    {
        if (isset($Param['ORDER']) AND $Param['ORDER'] != '')
        {
            $ORDERBY = $Param['ORDER'];
        }
        else
        {
            $ORDERBY = ' RRA_GUELTIGBIS DESC';
            $Param['ORDER'] = $ORDERBY;
        }
    }
    else
    {
        $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        $Param['ORDER'] = $ORDERBY;
    }
    
    if (isset($_GET['SortExt']))
    {
    	//$Form->DebugAusgabe(1,$_GET['SortExt']);
    	$ORDERBY .= ','.str_replace('~', ' ',$_GET['SortExt']);
    }
    
    //********************************************************
    // Bedingung erstellen
    //********************************************************
	$Form->DebugAusgabe(1,$Param);
	$Bindevariablen = array();
    $Bedingung = _BedingungErstellen($Param, $Bindevariablen);
    
    //********************************************************
    // Daten suchen
    //********************************************************
    if (isset($Param['RRA_ATUNR']) AND $Param['RRA_ATUNR'] != '')// AND isset($_POST['cmdSuche_x']))
    {
        $SQL = 'Select daten.* ' ;
         
        if ($AWIS_KEY1 <= 0)
        {
            $SQL .= ' ,row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr ';
        }   	
        
        $SQL .= 'From(';
        $SQL  .= 'SELECT * FROM (';
    	$SQL .= ' SELECT rra.*,';
    	$SQL .= ' CASE ';
    	// Beide Seiten brauchen einen Trunc 00:00:00 Problem
    	$SQL .= '   WHEN rra.RRA_GUELTIGAB <= TRUNC(SYSDATE) AND RRA_GUELTIGBIS < TRUNC(SYSDATE) THEN \'abgelaufen\' ';
    	$SQL .= '   WHEN rra.RRA_GUELTIGAB <= TRUNC(SYSDATE) AND RRA_GUELTIGBIS >= TRUNC(SYSDATE) THEN \'aktuell\' ';
    	$SQL .= '   WHEN rra.RRA_GUELTIGAB > TRUNC(SYSDATE) THEN \'geplant\' ELSE \'ungeplant\' ';
    	$SQL .= ' END AS ZEITSTATUS';
    	$SQL .= ' FROM reifenpflege rrp';
    	$SQL .= ' INNER JOIN reifenstamm rst ON rst.rst_key = rrp.rrp_rst_key';
    	$SQL .= ' INNER JOIN reifenhersteller reh ON reh.reh_key = rst.rst_reh_key';
    	$SQL .= ' LEFT JOIN v_artikelstammwgr wgr ON wgr.ast_atunr = rrp.rrp_atunr';
    	$SQL .= ' LEFT JOIN reifenaktionen rra';
    	$SQL .= '   ON ((rra.rra_wgr_id = wgr.ast_wgr_id OR rra.rra_wgr_id IS NULL)';
    	$SQL .= '   AND (rra.rra_wug_id  = wgr.ast_wug_id OR rra.rra_wug_id IS NULL)';
    	$SQL .= '   AND (rra.rra_kenn1   = rrp.rrp_kenn1 OR rra.rra_kenn1 IS NULL)';
    	$SQL .= '   AND (rra.rra_reh_key = rst.rst_reh_key OR rra.rra_reh_key IS NULL)';
    	$SQL .= '   AND (rra.rra_atunrliste LIKE \'%\' || rrp.rrp_atunr || \'%\' OR rra.rra_atunrliste IS NULL)';
    	$SQL .= '   AND rra.rra_aktionsname IS NOT NULL';
    	$SQL .= '   AND ( rra.rra_reh_key     IS NOT NULL ';
    	$SQL .= '   OR rra.rra_wgr_id         IS NOT NULL';
    	$SQL .= '   OR rra.rra_wug_id         IS NOT NULL';
    	$SQL .= '   OR rra.rra_kenn1          IS NOT NULL';
    	$SQL .= '   OR rra.rra_atunrliste     IS NOT NULL ) )';
    	$SQL .= ' WHERE RRA_KEY IS NOT NULL ';
   		$SQL .= ' AND RRP_ATUNR= :var_T_RRP_ATUNR ';
    	$Bindevariablen['var_T_RRP_ATUNR'] = $Param['RRA_ATUNR'];
   		if ($Bedingung != '')
    	{
    		$SQL .= $Bedingung;
    	}
    	if (isset($Param['ZEITSTATUS']) AND $Param['ZEITSTATUS'] != '-1')
    	{
    		$SQL .= ' ) WHERE ZEITSTATUS = :var_T_ZEITSTATUS ';
    		$SQL .= ' ORDER BY ' . $ORDERBY;
    		$Bindevariablen['var_T_ZEITSTATUS'] = $Param['ZEITSTATUS'];
    	}
    	else
    	{
	    	$SQL .= ' ) ORDER BY ' . $ORDERBY;
    	} 
    	$SQL .= ')daten';
    }
    else
    {
    	$SQL = 'Select daten.* ' ;
    	
    	if ($AWIS_KEY1 <= 0)
    	{
    	    $SQL .= ' ,row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr ';
    	}

    	$SQL .= 'From(';
        $SQL  .= 'SELECT * FROM (';
    	$SQL .= 'SELECT';
    	$SQL .= ' RRA_KEY,';
    	$SQL .= ' RRA_ATUNRLISTE,';
    	$SQL .= ' RRA_WGR_ID,';
    	$SQL .= ' RRA_WUG_ID,';
    	$SQL .= ' RRA_KENN1,';
    	$SQL .= ' RRA_REH_KEY,';
    	$SQL .= ' RRA_AKTIONSNAME,';
    	$SQL .= ' RRA_AKTIONSPROZENT,';
    	$SQL .= ' RRA_AKTIONSTEXT,';
    	$SQL .= ' RRA_GUELTIGAB,';
    	$SQL .= ' RRA_GUELTIGBIS,';
    	$SQL .= ' RRA_LAN_CODE,';
    	$SQL .= ' RRA_BEMERKUNG,';
    	$SQL .= ' RRA_USER,';
    	$SQL .= ' RRA_USERDAT,';
    	$SQL .= ' CASE';
    	//Beide Seiten brauchen einen Trunc 00:00:00 Problem
    	$SQL .= '  WHEN RRA_GUELTIGAB <= TRUNC(SYSDATE) AND RRA_GUELTIGBIS < TRUNC(SYSDATE) THEN \'abgelaufen\'';
    	$SQL .= '  WHEN RRA_GUELTIGAB <= TRUNC(SYSDATE) AND RRA_GUELTIGBIS >= TRUNC(SYSDATE) THEN \'aktuell\'';
    	$SQL .= '  WHEN RRA_GUELTIGAB > TRUNC(SYSDATE) THEN \'geplant\'';
    	$SQL .= '  ELSE \'ungeplant\'';
    	$SQL .= ' END AS ZEITSTATUS';
	    
        if (isset($Param['ZEITSTATUS']) AND $Param['ZEITSTATUS'] != '-1')
		{
			$Bedingung .= ' AND ZEITSTATUS = :var_T_ZEITSTATUS ';
			$Bindevariablen['var_T_ZEITSTATUS'] = $Param['ZEITSTATUS'];
		}
	    $SQL .= ' FROM REIFENAKTIONEN)';
        if ($Bedingung != '')
    	{
    		$SQL .= ' WHERE ' . substr($Bedingung, 4);
    	}
		
    	$SQL .= ' ORDER BY ' . $ORDERBY;
    	$SQL .= ')daten';
    }


    
    //************************************************
    // Aktuellen Datenblock festlegen
    //************************************************
    /*
    if($AWIS_KEY1==0)		// Kein Bl�ttern, wenn nur ein Datensatz wird
    {
	    $Block = 1;
	    if (isset($_REQUEST['Block']) and !isset($_REQUEST['Seite']))
	    {
	    	$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	    	$Param['BLOCK']= $Block;
	    }
	    elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
	    {
	    	$Block = intval($Param['BLOCK']);
	    }
    }
    */
    if($AWIS_KEY1==0)
    {
    	// Zum Bl�ttern in den Daten
    	$Block = 1;
    	if(isset($_REQUEST['Block']))
    	{
    		$Block=$Form->Format('N0',$_REQUEST['Block'],false);
    		$Param['BLOCK']=$Block;
    	}
    	elseif(isset($Param['BLOCK']))
    	{
    		$Block=$Param['BLOCK'];
    	}
    
    	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
    
    	$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
    	//echo $SQL;
    	$MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $Bindevariablen);
    	
    	$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
    }
    
 
    //************************************************
    // Zeilen begrenzen
    //************************************************
//    $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//    $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
//    $MaxDS = $DB->ErmittleZeilenAnzahl($SQL,$DB->Bindevariablen('RRA'));
    
    //*****************************************************************
    // Nicht einschr�nken, wenn nur 1 DS angezeigt werden soll
    //*****************************************************************
    //if ($AWIS_KEY1 <= 0)
    {
    	//$SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
    }
    
    
    
    $rsRRA = $DB->RecordSetOeffnen($SQL,$Bindevariablen);
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    //********************************************************
    // Daten anzeigen
    //********************************************************
    $Form->SchreibeHTMLCode('<form name=frmReifenaktionen action=./reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . '' . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . ' method=post>');
    
    if ($rsRRA->EOF() AND !isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1>=0)  // Keine Meldung bei neuen Datens�tzen
    {
    	echo '<span class=HinweisText>Es wurden keine Datens�tze gefunden.</span>';

    	//***************************************
    	// Schaltfl�chen f�r dieses Register
    	//***************************************
    	$Form->SchaltflaechenStart();
    	 
    	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    	 
    	if (($Recht15200 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
    	{
    		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    	}
    	$Form->SchaltflaechenEnde();
    }
    elseif (($rsRRA->AnzahlDatensaetze() > 1) or (isset($_GET['Liste'])))      // Liste anzeigen
    {
    	$Form->Formular_Start();
    	$Form->ZeileStart();
    	
/*
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_KEY' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_KEY')) ? '~' : '');
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_KEY'], 130, '', $Link);
*/    	
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_AKTIONSNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_AKTIONSNAME')) ? '~' : '');
    	$Link .= '&SortExt=RRA_LAN_CODE,RRA_AKTIONSPROZENT,RRA_GUELTIGBIS~DESC';
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_AKTIONSNAME'], 200, '', $Link);
    	 
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_LAN_CODE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_LAN_CODE')) ? '~' : '');
    	$Link .= '&SortExt=RRA_GUELTIGBIS DESC';
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_LAN_CODE'], 80, '', $Link);
    	
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_AKTIONSPROZENT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_AKTIONSPROZENT')) ? '~' : '');
    	$Link .= '&SortExt=RRA_AKTIONSNAME,RRA_LAN_CODE,RRA_GUELTIGBIS~DESC';
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_AKTIONSPROZENT_kurz'], 60, '', $Link);
    	 
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_WGR_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_WGR_ID')) ? '~' : '');
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_WGR_ID_kurz'], 60, '', $Link);
    	
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_WUG_ID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_WUG_ID')) ? '~' : '');
    	$Link .= '&SortExt=RRA_WUG_ID,RRA_GUELTIGBIS~DESC';
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_WUG_ID_kurz'], 60, '', $Link);
    	
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_KENN1' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_KENN1')) ? '~' : '');
    	$Link .= '&SortExt=RRA_AKTIONSNAME,RRA_LAN_CODE,RRA_GUELTIGBIS~DESC';
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_KENN1'], 80, '', $Link);
    	
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_GUELTIGAB' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_GUELTIGAB')) ? '~' : '');
    	$Link .= '&SortExt=RRA_AKTIONSNAME,RRA_LAN_CODE,RRA_GUELTIGBIS~DESC';
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_GUELTIGAB'],100,'',$Link);
        
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
    	$Link .= '&Sort=RRA_GUELTIGBIS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'RRA_GUELTIGBIS')) ? '~' : '');
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['RRA']['RRA_GUELTIGBIS'],100,'',$Link);
    	    	
    	$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'], 200, '', $Link);
    	 
    	$Form->ZeileEnde();
    	 
    	$DS = 0;
    	
    	while (!$rsRRA->EOF())
    	{
    		$Style = '';
    		$Form->ZeileStart();
    		$Link = './reifenaktionen_Main.php?cmdAktion=Details&RRA_KEY=' . $rsRRA->FeldInhalt('RRA_KEY') . '';
    		//$Form->Erstelle_ListenFeld('RRA_KEY', $rsRRA->FeldInhalt('RRA_KEY'), 0, 130, false, ($DS % 2), $Style, $Link, 'T');
    		$Form->Erstelle_ListenFeld('RRA_AKTIONSNAME', $rsRRA->FeldInhalt('RRA_AKTIONSNAME'), 0, 200, false, ($DS % 2), $Style, $Link, 'T');
    		$Form->Erstelle_ListenFeld('RRA_LAN_CODE', $rsRRA->FeldInhalt('RRA_LAN_CODE'), 0, 80, false, ($DS % 2), $Style, '', 'T');
    		$Form->Erstelle_ListenFeld('RRA_AKTIONSPROZENT', $rsRRA->FeldInhalt('RRA_AKTIONSPROZENT'), 0, 60, false, ($DS % 2), $Style, '', 'T');
    		$Form->Erstelle_ListenFeld('RRA_WGR_ID', $rsRRA->FeldInhalt('RRA_WGR_ID'), 0, 60, false, ($DS % 2), $Style, '', 'T');
    		$Form->Erstelle_ListenFeld('RRA_WUG_ID', $rsRRA->FeldInhalt('RRA_WUG_ID'), 0, 60, false, ($DS % 2), $Style, '', 'T');
    		$Form->Erstelle_ListenFeld('RRA_KENN1', $rsRRA->FeldInhalt('RRA_KENN1'), 0, 80, false, ($DS % 2), $Style, '', 'T');
			$Form->Erstelle_ListenFeld('RRA_GUELTIGAB',$rsRRA->FeldInhalt('RRA_GUELTIGAB'),0,100,false,($DS%2),$Style,'','D');
			$Form->Erstelle_ListenFeld('RRA_GUELTIGBIS',$rsRRA->FeldInhalt('RRA_GUELTIGBIS'),0,100,false,($DS%2),$Style,'','D');
    		$Form->Erstelle_ListenFeld('ZEITSTATUS', $rsRRA->FeldInhalt('ZEITSTATUS'), 0, 100, false, ($DS % 2), $Style, '', 'T');

    		$Infotext = '';
			if($rsRRA->FeldInhalt('RRA_AKTIONSTEXT')!='')
			{
				$Infotext  .= 'Aktionstext.: '.$rsRRA->FeldInhalt('RRA_AKTIONSTEXT');
			}
			if($rsRRA->FeldInhalt('RRA_ATUNRLISTE')!='')
			{
				$Infotext .= ' Artikelliste: '.$rsRRA->FeldInhalt('RRA_ATUNRLISTE');
			}
			if($rsRRA->FeldInhalt('RRA_BEMERKUNG')!='')
			{
				$Infotext .= ' Bemerkung: '.$rsRRA->FeldInhalt('RRA_BEMERKUNG');
			}
			if($Infotext!='')
			{
				$Form->Erstelle_HinweisIcon('info', 20, '',$Infotext);
			}
    		$Form->ZeileEnde();
    		
    		$rsRRA->DSWeiter();
    		$DS++;
    	}
    		
    	$Link = './reifenaktionen_Main.php?cmdAktion=Details&Liste=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');
    	$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
    	
    	$Form->Formular_Ende();
    	 
    	//***************************************
    	// Schaltfl�chen f�r dieses Register
    	//***************************************
    	$Form->SchaltflaechenStart();
    	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    	if (($Recht15200 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
    	{
    		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    	}
    	$Form->SchaltflaechenEnde();
    }
    else          // Ein einzelner Datensatz
    {
    	$Form->SchreibeHTMLCode('<form name=frmReifenaktionen action=./reifenaktionen_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');
    	
    	$Param['KEY']=$AWIS_KEY1= $rsRRA->FeldInhalt('RRA_KEY');
    	//$AWISBenutzer->ParameterSchreiben("Formular_RRA", serialize($Param));
    	$EditRecht = (($Recht15200 & 2) != 0);
    	$Form->Erstelle_HiddenFeld('RRA_KEY', $AWIS_KEY1);
    	
    	$Form->DebugAusgabe(1,$AWIS_KEY1);
    	
    	$Form->Formular_Start();
    	$OptionBitteWaehlen = '~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
    	
    	// Infozeile
    	$Felder = array();
    	$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./reifenaktionen_Main.php?cmdAktion=Details&Liste=True accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    	$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsRRA->FeldInhalt('RRA_USER'));
    	$Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsRRA->FeldInhalt('RRA_USERDAT'));
    	$Form->InfoZeile($Felder, '');

    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_AKTIONSNAME'] . ':', 120, '', '', '');
    	$Form->Erstelle_TextFeld('RRA_AKTIONSNAME', $rsRRA->FeldInhalt('RRA_AKTIONSNAME'), 60, 380, $EditRecht);
    	$Form->ZeileEnde();
    	$AWISCursorPosition = 'txtRRA_AKTIONSNAME';
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_GUELTIGAB'] . ':', 120, '', '', '');
    	$Form->Erstelle_TextFeld('RRA_GUELTIGAB', substr($rsRRA->FeldInhalt('RRA_GUELTIGAB'), 0, 10), 10, 120, $EditRecht,'','','','D');
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_GUELTIGBIS'] . ':', 80, '', '', '');
    	$Form->Erstelle_TextFeld('RRA_GUELTIGBIS', substr($rsRRA->FeldInhalt('RRA_GUELTIGBIS'), 0, 10), 10, 120, $EditRecht,'','','','D');
    	$Form->ZeileEnde();
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_LAN_CODE'] . ':', 120, '', '', '');
    	$SQL = "SELECT LAN_CODE, LAN_LAND FROM LAENDER WHERE LAN_WWSKENN IS NOT NULL";
    	$Daten = explode('|', $AWISSprachKonserven['Liste']['lst_ALLE_NULL']);
    	$Form->Erstelle_SelectFeld('RRA_LAN_CODE', $rsRRA->FeldInhalt('RRA_LAN_CODE'), "220:180", $EditRecht, $SQL, '','',0,'',$Daten);
    	$Form->ZeileEnde();

    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_AKTIONSPROZENT'] . ':', 120, '', '', '');
    	$Form->Erstelle_TextFeld('RRA_AKTIONSPROZENT', $rsRRA->FeldInhalt('RRA_AKTIONSPROZENT'), 3, 250, $EditRecht);
    	$Form->ZeileEnde();
    	
    	$Form->Trennzeile('O');
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_REH_KEY'] . ':', 120, '', '', '');
    	$SQL = "SELECT REH_KEY, REH_BEZEICHNUNG FROM REIFENHERSTELLER ORDER BY REH_BEZEICHNUNG";
    	$Form->Erstelle_SelectFeld('RRA_REH_KEY', $rsRRA->FeldInhalt('RRA_REH_KEY'), '200:180', $EditRecht, $SQL, $OptionBitteWaehlen);
    	$Form->ZeileEnde();
    	
    	$Form->Trennzeile('O');
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_WGR_ID'] . ':', 120, '', '', '');
    	$SQL = "SELECT WGR_ID, WGR_ID || ' - ' || WGR_BEZEICHNUNG FROM WARENGRUPPEN WHERE WGR_ID IN ('01','02','23','29') ORDER BY WGR_ID";
    	$Form->Erstelle_SelectFeld('RRA_WGR_ID', $rsRRA->FeldInhalt('RRA_WGR_ID'), '200:180', $EditRecht, $SQL, $OptionBitteWaehlen);
    	
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_WUG_ID'] . ':', 70, '', '', '');
		$SQL = "SELECT DISTINCT WUG_ID AS KEY , WUG_ID || ' - ' || WUG_BEZEICHNUNG AS ANZEIGE ";
		$SQL .= ' FROM WARENUNTERGRUPPEN';
		$SQL .= ' WHERE WUG_WGR_ID='.$DB->FeldInhaltFormat('T',$rsRRA->FeldInhalt('RRA_WGR_ID'),true);
		$SQL .= ' AND WUG_ID='.$DB->FeldInhaltFormat('T',$rsRRA->FeldInhalt('RRA_WUG_ID'),true);
		$SQL .= ' ORDER BY WUG_ID';
		$rsWUG=$DB->RecordSetOeffnen($SQL);
    	$Form->Erstelle_SelectFeld('RRA_WUG_ID',$rsRRA->FeldInhalt('RRA_WUG_ID'),'240:200',$EditRecht,'***WUG_Daten;txtRRA_WUG_ID;WGR=*txtRRA_WGR_ID&Zusatz='.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],($rsWUG->EOF())?$OptionBitteWaehlen:$rsWUG->FeldInhalt('KEY').'~'.$rsWUG->FeldInhalt('ANZEIGE'));
    	$Form->ZeileEnde();
    	
    	$Form->Trennzeile('O');
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_KENN1'] . ':', 120, '', '', '');
    	$Daten = explode("|", $AWISSprachKonserven['Liste']['lst_KENN1']);
    	$Form->Erstelle_SelectFeld('RRA_KENN1', $rsRRA->FeldInhalt('RRA_KENN1'), '200:180', $EditRecht, '', $OptionBitteWaehlen, '', '', '', $Daten);
    	$Form->ZeileEnde();
    	
    	$Form->Trennzeile('O');
    	
    	$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_ATUNRLISTE'].':',120,'','','');
    	$Form->Erstelle_TextArea('RRA_ATUNRLISTE',$rsRRA->FeldInhalt('RRA_ATUNRLISTE'),800,105,5,$EditRecht);
    	 
/*
    	$Form->Erstelle_TextFeld('*ATUNR',($AWIS_KEY1===0?'':$rsRRA->FeldInhalt('RRA_ATUNRLISTE')),6,80,$EditRecht,'','background:#50FF50');
		$Form->Erstelle_SelectFeld('RRA_ATUNRLISTE',$rsRRA->FeldInhalt('RRA_ATUNRLISTE'),"200:180",$EditRecht,'***RRP_ATUNR;txtRRA_ATUNR;ATUNR=*sucATUNR&ZUSATZ=~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', '');
*/
    	$Form->ZeileEnde();
    	
    	$Form->ZeileStart('font-size:8pt');
    	$Form->Erstelle_TextLabel('&nbsp',120);
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['txt_RRA_ATUNRLISTE'],600);
    	$Form->ZeileEnde();
    	 
    	$Form->Trennzeile('O');
    	
    	
    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_AKTIONSTEXT'] . ':', 120, '', '', '');
    	$Form->Erstelle_TextFeld('RRA_AKTIONSTEXT', $rsRRA->FeldInhalt('RRA_AKTIONSTEXT'), 60, 380, $EditRecht);
    	$Form->ZeileEnde();
    	
    	 
    	$Form->Trennzeile('O');

    	$Form->ZeileStart();
    	$Form->Erstelle_TextLabel($AWISSprachKonserven['RRA']['RRA_BEMERKUNG'].':',120);
    	$Form->Erstelle_TextArea('RRA_BEMERKUNG',$rsRRA->FeldInhalt('RRA_BEMERKUNG'),800,105,5,$EditRecht);
    	$Form->ZeileEnde();
    	 
    	
    	$Form->Trennzeile('L');
		
    	
    	//Auswahlbox erstellen, diese aber ausblenden. Diese wird nur ben�tigt dass die Frameworkseitige Ajaxfunktionalit�t verwendet werden kann.
    	echo "<div style='display:none'";
    	$Form->AuswahlBox('RRA_ARTIKEL', 'box1','', 'RRA_ARTIKEL', '', 25,10,$AWIS_KEY1,'T',true,'','','',50,'','','',0,'');
    	echo "</div>";
    
    	//Button der das Script f�rs Ajax antriggert (Erstellt durch Auswahlbox)
    	$Form->ZeileStart();
    	$Form->Trennzeile('O');
    	
    	$Form->SchreibeHTMLCode('<div style="margin-left: 5px;"><button type="button" onclick="key_RRA_ARTIKEL();">Artikelliste laden</button></div>');
    	$Form->Trennzeile('O');
    	$Form->ZeileEnde();
    	
    	ob_start();
    	
    	//Wenn sucRRA_ARTIKEL mitkommt, dann wurde die Artikelliste schon ausgeklappt. Wenn Block mitkommt, dann wurde die Bl�tternfunktion verwendet --> Artikel wieder zeichnen
	    if(isset($_REQUEST['sucRRA_ARTIKEL']) or (isset($_REQUEST['Block'])))
	    {
	    	ArtikelAnzeigen();
	    }
	   
	    $Inhalt = ob_get_contents();
	    ob_end_clean();
	    
	    $Form->ZeileStart();
	    $Form->AuswahlBoxHuelle('box1','AuswahlListe','',$Inhalt);
	    $Form->ZeileEnde();
    	 
    	//***************************************
    	// Schaltfl�chen f�r dieses Register
    	//***************************************
    	$Form->SchaltflaechenStart();
    	 
    	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    	 
    	if (($Recht15200 & 6) != 0 or ($Recht15200 & 128) != 0)  //
    	{
    		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    	}
    	 
    	if (($Recht15200 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))  // Hinzuf�gen erlaubt?
    	{
    		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    	}
    	 
    	if (($Recht15200 & 8) == 8 AND !isset($_POST['cmdDSNeu_x']))
    	{
    		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], 'X');
    	}
    	 
    	$Form->SchaltflaechenEnde();
    }
    
    $Form->SchreibeHTMLCode('</form>');
    
    //*****************************************************************
	// Aktuelle Parameter sichern
	//*****************************************************************
	$AWISBenutzer->ParameterSchreiben("Formular_RRA", serialize($Param));
	
	$Form->SetzeCursor($AWISCursorPosition);
}

catch (awisException $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201304301400");
    }
    else
    {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}
catch (Exception $ex)
{
    $Form->DebugAusgabe(1,$DB->LetzterSQL());
    
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "201304301401");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

function _BedingungErstellen($Param, &$Bindevariablen)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $DB;

    $Bedingung = '';
    
    if (floatval($AWIS_KEY1) != 0)
    {
        $Bedingung.= ' AND RRA_KEY = :var_N0_RRA_KEY ';
    	$Bindevariablen['var_N0_RRA_KEY'] = floatval($AWIS_KEY1); 
        return $Bedingung;
    }
    
    if (isset($Param['RRA_AKTIONSNAME']) AND $Param['RRA_AKTIONSNAME'] != '')
    {
        $Bedingung .= ' AND TRIM(UPPER(RRA_AKTIONSNAME)) ' . $DB->LIKEoderIST($Param['RRA_AKTIONSNAME'],awisDatenbank::AWIS_LIKE_UPPER);
    }
    
    if (isset($Param['RRA_GUELTIGAB']) AND $Param['RRA_GUELTIGAB'] != '')
    {
    	//Uhrzeit vorhanden? oder nicht?
    	$Bedingung .= " AND TRUNC(RRA_GUELTIGAB) >= TRUNC(" . $DB->FeldInhaltFormat('DU', $Param['RRA_GUELTIGAB'], true) .")";
    }
    
    if (isset($Param['RRA_GUELTIGBIS']) AND $Param['RRA_GUELTIGBIS'] != '')
    {
    	$Bedingung .= " AND TRUNC(RRA_GUELTIGBIS) <= TRUNC(" . $DB->FeldInhaltFormat('DU', $Param['RRA_GUELTIGBIS'], true) .")";
    }
    
    if (isset($Param['RRA_LAN_CODE']) AND $Param['RRA_LAN_CODE'] != '-1')
    {
    	$Bedingung .= ' AND RRA_LAN_CODE = :var_T_RRA_LAN_CODE ';
    	$Bindevariablen['var_T_RRA_LAN_CODE'] = $Param['RRA_LAN_CODE'];
    }
    
    if (isset($Param['RRA_REH_KEY']) AND $Param['RRA_REH_KEY'] != '-1')
    {
    	$Bedingung .= ' AND RRA_REH_KEY = :var_N0_RRA_REH_KEY ';
    	$Bindevariablen['var_N0_RRA_REH_KEY'] = $Param['RRA_REH_KEY'];
    }
    
    if (isset($Param['RRA_WGR_ID']) AND $Param['RRA_WGR_ID'] != '-1')
    {
    	$Bedingung .= ' AND RRA_WGR_ID = :var_T_RRA_WGR_ID ';
    	$Bindevariablen['var_T_RRA_WGR_ID'] = $Param['RRA_WGR_ID'];
    }
    
    if (isset($Param['RRA_WUG_ID']) AND $Param['RRA_WUG_ID'] != '-1')
    {
    	$Bedingung .= ' AND RRA_WUG_ID = :var_T_RRA_WUG_ID ';
    	$Bindevariablen['var_T_RRA_WUG_ID'] = $Param['RRA_WUG_ID'];
    }
    
    if (isset($Param['RRA_KENN1']) AND $Param['RRA_KENN1'] != '-1')
    {
    	$Bedingung .= ' AND RRA_KENN1 = :var_T_RRA_KENN1 ';
    	$Bindevariablen['var_T_RRA_KENN1'] = $Param['RRA_KENN1'];
    }
    
    if (isset($Param['RRA_AKTIONSTEXT']) AND $Param['RRA_AKTIONSTEXT'] != '')
    {
    	$Bedingung .= ' AND UPPER(RRA_AKTIONSTEXT) ' . $DB->LIKEoderIST($_POST['RRA_AKTIONSTEXT'],awisDatenbank::AWIS_LIKE_UPPER);
    }
    
    return $Bedingung;
}
?>