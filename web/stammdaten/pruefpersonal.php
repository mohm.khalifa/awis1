<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($_SERVER['PHP_AUTH_USER']) .">";
?>
</head>

<?php
/****************************************************************************************************
* 
* 	ArtikelPr�fpersonal
* 
* 	Pflege der Mitarbeiter f�r die Pr�fung durch... Mitarbeiter
* 
* 	Autor: 	Sacha Kerres
* 	Datum:	Dez. 2003
* 
****************************************************************************************************/
// Variablen
global $HTTP_GET_VARS;
global $HTTP_POST_VARS;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();
$Rechtestufe = awisBenutzerRecht($con, 601) ;
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen
		
if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Artikelpruefpersonal',$_SERVER['PHP_AUTH_USER'],'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';
if(awis_NameInArray($HTTP_POST_VARS,"cmdSpeichern")!='')
{
	$SQL = "UPDATE ArtikelPruefPersonal SET";
	$SQL .= " APP_NAME='" . $HTTP_POST_VARS['txtAPP_NAME'] . "'";
	$SQL .= " ,APP_AKTIVBIS=TO_DATE('" . $HTTP_POST_VARS['txtAPP_AKTIVBIS'] . "','DD.MM.RRRR')";	
	if($HTTP_POST_VARS['txtAPP_KON_KEY']=='0')
	{
		$SQL .= " ,APP_KON_KEY=NULL";
	}
	else
	{
		$SQL .= " ,APP_KON_KEY=" . $HTTP_POST_VARS['txtAPP_KON_KEY'] . "";
	}
	$SQL .= " ,APP_USER='" . $_SERVER['PHP_AUTH_USER'] . "'";
	$SQL .= " ,APP_USERDAT=SYSDATE";
	$SQL .= " WHERE APP_ID=0" . $HTTP_POST_VARS['txtAPP_ID'] . "";
	
	$Erg = awisExecute($con, $SQL);
	if($Erg==FALSE)
	{
		awisErrorMailLink("pruefpersonal.php", 2, $awisDBFehler['message']);
	}

}





	// Kein spezieller angegeben -> suchen
if(!isset($HTTP_GET_VARS['APPID']) or isset($HTTP_GET_VARS['Liste']))
{
	$SQL = "SELECT * FROM ARTIKELPRUEFPERSONAL, KONTAKTE WHERE APP_KON_KEY = KON_KEY(+) AND APP_ID > 0";
}
else
{
	$SQL = "SELECT * FROM ARTIKELPRUEFPERSONAL, KONTAKTE WHERE APP_KON_KEY = KON_KEY(+) AND APP_ID=0". $HTTP_GET_VARS['APPID'] ."";
}

if(isset($HTTP_GET_VARS['Sort']))
{
	$SQL .= " ORDER BY " . $HTTP_GET_VARS['Sort'];
}
else
{
	$SQL .= " ORDER BY APP_NAME";
}


$rsAPP = awisOpenRecordset($con, $SQL);
$rsAPPZeilen = $awisRSZeilen;
if($rsAPPZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<colspec><col width=100><col width=*></colspec>';
	echo '<tr><td id=FeldBez>Nummer</td><td id=FeldBez><a href=./pruefpersonal.php?Sort=APP_NAME>Mitarbeitername</a></td>';
	echo '<td  id=FeldBez>Kontakt</td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsAPPZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./pruefpersonal.php?APPID=" . $rsAPP['APP_ID'][$Zeile] . ">" . $rsAPP['APP_ID'][$Zeile] . "</a></td><td id=DatenFeld>" . $rsAPP['APP_NAME'][$Zeile] . "</td>";
		echo '<td>' . $rsAPP['KON_NAME1'][$Zeile] . ' ' . $rsAPP['KON_NAME2'][$Zeile] . '</td>';
		echo '</tr>';
	}
	
	echo '</table>';
}
else					// einzelnen Lieferanten anzeigen
{


	echo '<form action=./pruefpersonal.php method=post name=frmPruefpersonal>';
	
	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Pr�fpersonal</h3></td>';
	echo '<td><font size=1>' . $rsAPP['APP_USER'][0] . '<br>' . $rsAPP['APP_USERDAT'][0] . '</font></td>';
    echo "<td width=40 backcolor=#000000 align=right><input type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./pruefpersonal.php?Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=400><col width=100><col width=*></colgroup>';
	echo '<tr>';
	echo '<td id=FeldBez>Mitarbeiter-ID</td>';
	echo '<td><input type=hidden name=txtAPP_ID value=' . $rsAPP['APP_ID'][0] . '>' . $rsAPP['APP_ID'][0] . '</td>';
	echo '</tr>';

	echo '<tr>';
	if(($Rechtestufe&2)==2)
	{
		echo '<td id=FeldBez>Name</td>';
		echo '<td><input size=30 name=txtAPP_NAME value=\'' . $rsAPP['APP_NAME'][0] . '\'></td>';
		echo '<td id=FeldBez>Aktiv bis</td>';
		echo '<td><input size=10 name=txtAPP_AKTIVBIS value=\'' . $rsAPP['APP_AKTIVBIS'][0] . '\'></td>';
	}
	else
	{
		echo '<td id=FeldBez>Name</td>';
		echo '<td>' . $rsAPP['APP_NAME'][0] . '</td>';
		echo '<td id=FeldBez>Status</td>';
		echo '<td>' . $rsAPP['APP_AKTIVBIS'][0] . '</td>';
	}
	echo '</tr>';

	$rsKontakt = awisOpenRecordset($con,"SELECT KON_KEY, KON_NAME1 || ', ' || KON_NAME2 || ' - ' || KON_ZUSTAENDIGKEIT AS KONTAKT FROM KONTAKTE WHERE KON_KKA_KEY=1 ORDER BY KON_NAME1, KON_NAME2");
	$rsKontaktZeilen = $awisRSZeilen;
	
    print "<tr><td id=FeldBez>Kontakt:</td><td><select size=1 name=txtAPP_KON_KEY><option value=0>Kontakt ausw�hlen...</option>";
    for($i=0; $i<$rsKontaktZeilen;$i++)
    {
    	print "<option ";
		if($rsKontakt["KON_KEY"][$i] == $rsAPP["APP_KON_KEY"][0])
		{
			print " selected ";
		}
		print "value=" . $rsKontakt["KON_KEY"][$i] . ">". $rsKontakt["KONTAKT"][$i] . "</option>";
	}
	print "</select></td></tr>";

	unset($rsKontakt);
	
	echo '</table>';

	if($Rechtestufe>1)
	{
		print "<input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./pruefpersonal.php?APPID=" . $rsAPP["APP_ID"][0] . "&Speichern=True'>";
	}

	echo '</form>';
}

if($HTTP_GET_VARS['Zurueck']=='artikel_LIEF')
{
	$ZurueckLink = '/ATUArtikel/artikel_Main.php?LARKEY=' . $HTTP_GET_VARS['LARKEY'] . '&cmdAktion=Lieferantenartikel';
}
else
{
	$ZurueckLink = '/index.php';
}

print "<br><hr><input type=image alt='Zur�ck (Alt+z)' src=/bilder/zurueck.png name=cmdZurueck accesskey=z onclick=location.href='$ZurueckLink';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=pruefpersonal','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awisLogoff($con);

?>