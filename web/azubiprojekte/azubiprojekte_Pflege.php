<?php
global $Funktionen;

$Funktionen->RechteMeldung(4);

$Funktionen->AWIS_KEY1 = 0;

if (isset($_POST['cmdDSNeu_x'])) {
    $Funktionen->AWIS_KEY1 = -1;
} elseif (isset($_POST['cmdSpeichern_x'])) {
    include_once './azubiprojekte_speichern.php';
}


if((isset($_GET["AZB_KEY"]) and $_GET["AZB_KEY"] <> "-1") or (!isset($_GET["AZB_KEY"]) and $Funktionen->AWIS_KEY1 <> -1)) {

    if(isset($_GET["AZB_KEY"])){
        $Funktionen->AWIS_KEY1 = $_GET["AZB_KEY"];
    }

    $Bedingung = "";

    $SQL = "SELECT * FROM AZUBIPROJEKTEBEREICHE ";

    $Bedingung = $Funktionen->BedingungErstellen();

    if (!isset($_GET['Sort'])) {
        $Order = ' ORDER BY AZB_STUFE';
    } else {
        $Order = ' ORDER BY AZB_' . str_replace('~', ' DESC ', $_GET['Sort']);
    }

    $SQL = $SQL . $Bedingung . $Order;

    $rsAZB = $Funktionen->DB->RecordSetOeffnen($SQL, $Funktionen->DB->Bindevariablen('AZB'));
} elseif (isset($_GET["AZB_KEY"]) and $_GET["AZB_KEY"] == "-1"){
    $Funktionen->AWIS_KEY1 = -1;
}

$Funktionen->Form->SchreibeHTMLCode("<form method=post action=./azubiprojekte_Main.php?cmdAktion=Pflege>");

if(($Funktionen->AWIS_KEY1 == -1) or (isset($rsAZB) and $rsAZB->AnzahlDatensaetze() == 1)){

    $Felder = array();
    $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./azubiprojekte_Main.php?cmdAktion=Pflege accesskey=T title='" . $Funktionen->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
    if(isset($rsAZB)) {
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAZB->FeldInhalt('AZB_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAZB->FeldInhalt('AZB_USERDAT'));
    }
    $Funktionen->Form->InfoZeile($Felder);

    $Labelbreiten = array();
    $Labelbreiten['AZB_BEREICHNAME']=150;
    $Labelbreiten['AZB_STATUS']=150;
    $Labelbreiten['AZB_INFOTEXT']=150;


    $Feldbreiten = array();
    $Feldbreiten['AZB_BEREICHNAME']=750;
    $Feldbreiten['AZB_STATUS']=250;
    $Feldbreiten['AZB_INFOTEXT']=1000;

    $Funktionen->Form->Formular_Start();

    if(isset($rsAZB)) {
        $Funktionen->Form->Erstelle_HiddenFeld("AZB_KEY", $rsAZB->FeldInhalt("AZB_KEY"));
    } elseif($Funktionen->AWIS_KEY1 == -1){
        $Funktionen->Form->Erstelle_HiddenFeld("AZB_KEY", $Funktionen->AWIS_KEY1);
    }

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZT']['AZT_STUFE'].":",$Labelbreiten['AZB_BEREICHNAME']);
    $Funktionen->Form->Erstelle_TextFeld('!AZB_BEREICHNAME', (isset($rsAZB))?$rsAZB->FeldInhalt('AZB_BEREICHNAME'):"", 400, $Feldbreiten['AZB_BEREICHNAME'],true,'','','','T','L','','',75);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['Wort']['Status'].":",$Labelbreiten['AZB_STATUS']);
    $Daten = explode("|", $Funktionen->AWISSprachKonserven['AZB']['AZB_AKTIVINAKTIV']);
    $Funktionen->Form->Erstelle_SelectFeld('AZB_STATUS', (isset($rsAZB))?$rsAZB->FeldInhalt('AZB_STATUS'):"", 471, true, '', '', 'A', '', '', $Daten);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZB']['AZB_HTMLERKLAERUNG'], awisFormular::HINWEISTEXT_BENACHRICHTIGUNG);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Erstelle_TextLabel($Funktionen->AWISSprachKonserven['AZB']['AZB_INFOTEXT'], $Labelbreiten['AZB_INFOTEXT']);
    $Funktionen->Form->Erstelle_Textarea('!AZB_INFOTEXT', (isset($rsAZB))?$rsAZB->FeldInhalt('AZB_INFOTEXT'):"", $Feldbreiten['AZB_INFOTEXT'], 90, 10, true,'','','','','',1000);
    $Funktionen->Form->ZeileEnde();

    $Funktionen->Form->Formular_Ende();
} elseif(isset($rsAZB) and $rsAZB->AnzahlDatensaetze() > 1) {

    $Feldbreiten = array();
    $Feldbreiten['AZB_BEREICHNAME']=500;
    $Feldbreiten['AZB_STATUS']=60;

    $Funktionen->Form->ZeileStart();

    $Link = './azubiprojekte_Main.php?cmdAktion=Details&Sort=BEREICHNAME' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'BEREICHNAME')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['AZT']['AZT_STUFE'], $Feldbreiten['AZB_BEREICHNAME'], '', $Link);

    $Link = './azubiprojekte_Main.php?cmdAktion=Details&Sort=STATUS' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'STATUS')) ? '~' : '') . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '');
    $Funktionen->Form->Erstelle_Liste_Ueberschrift($Funktionen->AWISSprachKonserven['Wort']['Status'], $Feldbreiten['AZB_STATUS'], '', $Link);

    $Funktionen->Form->ZeileEnde();

    $DS = 0;    // f�r Hintergrundfarbumschaltung
    while (!$rsAZB->EOF()) {
        $Funktionen->Form->ZeileStart();

        $Link = "./azubiprojekte_Main.php?cmdAktion=Pflege&AZB_KEY=" . $rsAZB->FeldInhalt('AZB_KEY');
        $Funktionen->Form->Erstelle_ListenFeld('AZB_BEREICHNAME', $rsAZB->FeldInhalt('AZB_BEREICHNAME'), 0,  $Feldbreiten['AZB_BEREICHNAME'], false, ($DS % 2), '', $Link);

        $Icons = array();
        if($rsAZB->FeldInhalt('AZB_STATUS') == "A"){
            $Icons[] = array('flagge_gruen','');
        }elseif($rsAZB->FeldInhalt('AZB_STATUS') == "I"){
            $Icons[] = array('flagge_rot','');
        }
        $Funktionen->Form->Erstelle_ListeIcons($Icons,$Feldbreiten['AZB_STATUS'],($DS%2),'');

        $DS++;
        $Funktionen->Form->ZeileEnde();
        $rsAZB->DSWeiter();
    }
} else {
    $Funktionen->Form->ZeileStart();
    $Funktionen->Form->Hinweistext($Funktionen->AWISSprachKonserven['AZT']['AZT_KEINE_DS'],awisFormular::HINWEISTEXT_HINWEIS);
    $Funktionen->Form->ZeileEnde();
}

$Funktionen->Form->SchaltflaechenStart();
$Funktionen->Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$Funktionen->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

if(($Funktionen->AWIS_KEY1 == -1) or (isset($rsAZB) and $rsAZB->AnzahlDatensaetze() == 1)){
    $Funktionen->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
} else {
    $Funktionen->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $Funktionen->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

$Funktionen->Form->SchaltflaechenEnde();

$Funktionen->Form->SchreibeHTMLCode("</form>");
