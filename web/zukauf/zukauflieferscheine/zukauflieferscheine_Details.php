<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once 'awisMehrwertsteuer.inc';
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZUL','%');
	$TextKonserven[]=array('ZUB','tit_Zukaufbestellung');
	$TextKonserven[]=array('ZUB','ZUB_WANRKORR');
	$TextKonserven[]=array('ZLH','%');
	$TextKonserven[]=array('ZLA','%');
	$TextKonserven[]=array('AST','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Status');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10002 = $AWISBenutzer->HatDasRecht(10002);
	if($Recht10002==0)
	{
	    awisEreignis(3,1000,'Zukauflieferscheine',$AWISBenutzer->BenutzerName(),'','','');
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$DetailAnsicht=false;
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUL'));

	//awis_Debug(1,$_POST,$_GET);
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
	//awis_Debug(1,$_POST);
		$Param = array();
		$Param['ZUL_ARTIKELNUMMER'] = $_POST['sucZUL_ARTIKELNUMMER'];
		$Param['ZUL_FIL_ID'] = $_POST['sucZUL_FIL_ID'];
		$Param['ZUL_LIE_NR'] = $_POST['sucZUL_LIE_NR'];
		$Param['ZLH_KEY'] = $_POST['sucZLH_KEY'];
		$Param['DATUMBIS'] = $_POST['sucDATUMBIS'];
		$Param['DATUMVOM'] = $_POST['sucDATUMVOM'];
		$Param['AST_ATUNR'] = $_POST['sucAST_ATUNR'];
		$Param['ZUL_EXTERNEID'] = $_POST['sucZUL_EXTERNEID'];
		$Param['ZUL_WANR'] = $_POST['sucZUL_WANR'];
		$Param['ZUL_LIEFERSCHEINNR'] = $_POST['sucZUL_LIEFERSCHEINNR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ZUL",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./zukauflieferscheine_loeschen.php');
		if($AWIS_KEY1==0)
		{
			$Param = $AWISBenutzer->ParameterLesen("Formular_ZUL");
		}
		else
		{
			$Param = $AWIS_KEY1;
		}
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./zukauflieferscheine_speichern.php');
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUL'));
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUL'));
	}
	elseif(isset($_GET['ZUL_KEY']))
	{
		$AWIS_KEY1 = $DB->FeldInhaltFormat('N0',$_GET['ZUL_KEY']);
		$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZUL'));
	}
	elseif(isset($_GET['ZUL_WANR']))
	{
		$Param = array();
		$Param['ZUL_ARTIKELNUMMER'] = '';
		$Param['ZUL_FIL_ID'] = '';
		$Param['ZUL_LIE_NR'] = '';
		$Param['ZLH_KEY'] = '';
		$Param['DATUMBIS'] = '';
		$Param['DATUMVOM'] = '';
		$Param['AST_ATUNR'] = '';
		$Param['ZUL_EXTERNEID'] = '';
		$Param['ZUL_WANR'] = $_GET['ZUL_WANR'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_ZUL",serialize($Param));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_ZUL',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}

		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = 'ORDER BY '.$Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY ZUL_DATUM DESC';
			$Param['ORDER']='ZUL_DATUM DESC';
		}
	}
	else
	{
		$Param['ORDER']=str_replace('~',' DESC ',$_GET['Sort']);
		$ORDERBY = ' ORDER BY '.$Param['ORDER'];
	}

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT Zukauflieferscheine.*, Zukaufbestellungen.*, ZAL_ZLA_KEY, MWS_SATZ, ZLA_ARTIKELNUMMER, ZLH_KEY, ZLH_KUERZEL, ZLH_BEZEICHNUNG';
    $SQL .= ', FIL_BEZ, ZAP_EK, ZAP_VKPR, ZAP_EKPR';
    $SQL .= ', (SELECT Count(*) FROM ZukaufRechnungen WHERE ZUL_KEY = ZUR_ZUL_KEY) AS Rechnungen';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukauflieferscheine';
	$SQL .= ' LEFT OUTER JOIN Filialen ON ZUL_FIL_ID = FIL_ID';
	$SQL .= ' LEFT OUTER JOIN Zukaufbestellungen ON ZUL_ZUB_KEY = ZUB_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTEN ON ZAL_KEY = ZUL_ZAL_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKELLIEFERANTENPREISE ON ZAP_KEY = ZUB_ZAP_KEY';
	$SQL .= ' LEFT OUTER JOIN ZUKAUFARTIKEL ON ZLA_KEY = ZAL_ZLA_KEY';
	$SQL .= ' LEFT OUTER JOIN MEHRWERTSTEUERSAETZE ON MWS_ID = ZUL_MWS_ID';
	$SQL .= ' LEFT OUTER JOIN ZukauflieferantenHersteller ON ZLH_KEY = ZUL_ZLH_KEY';
	
	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}
	$Form->DebugAusgabe(1,$SQL);
	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_ZUL',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

$Form->DebugAusgabe(1,$Param,$_GET,$_POST,$AWIS_KEY1);

	// Zeilen begrenzen
	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
//	$Form->DebugAusgabe(1,$SQL);
	$rsZUL = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_ZUL',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmZukaufLieferscheine action=./zukauflieferscheine_Main.php?cmdAktion=Details method=POST>';

	if($rsZUL->EOF() AND !isset($_POST['cmdDSNeu_x']))		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsZUL->AnzahlDatensaetze()>1 AND !isset($_GET['ZUL_KEY']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_DATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_DATUM'],130,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_WANR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_WANR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_WANR'],120,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_LIEFERMENGE'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_LIEFERMENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_LIEFERMENGE'],90,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_ARTIKELNUMMER'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_ARTIKELNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_ARTIKELNUMMER'],190,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZLH_KUERZEL'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZLH_KUERZEL'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZLH']['ZLH_KUERZEL'],60,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_ARTIKELBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_ARTIKELBEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_ARTIKELBEZEICHNUNG'],350,'',$Link);
		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=ZUL_VKPR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ZUL_VKPR'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_VKPR'],80,'text-align:right;',$Link);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['Status'],55,'','',$AWISSprachKonserven['ZUL']['ttt_ZULSTATUS']);
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZUL']['ZUL_LIE_NR'],60);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsZUL->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Link = './zukauflieferscheine_Main.php?cmdAktion=Details&ZUL_KEY=0'.$rsZUL->FeldInhalt('ZUL_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('ZUL_DATUM',$rsZUL->FeldInhalt('ZUL_DATUM'),0,130,false,($DS%2),'',$Link,'D');
			$Link = '';
			if($rsZUL->FeldInhalt('ZUL_ZUB_KEY')!='')
			{
				$Link = '../zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY='.$rsZUL->FeldInhalt('ZUL_ZUB_KEY');
			}

			//
			if($rsZUL->FeldInhalt('ZUL_WANR')=='')
			{
				$WANr = ($rsZUL->FeldInhalt('ZUB_WANRKORR')!=''?$rsZUL->FeldInhalt('ZUB_WANRKORR'):($rsZUL->FeldInhalt('ZUB_WANR')==''?$rsZUL->FeldInhalt('ZUL_WANR'):$rsZUL->FeldInhalt('ZUB_WANR')));
			}
			else
			{
				$WANr = ($rsZUL->FeldInhalt('ZUL_WANR')==''?'???':$rsZUL->FeldInhalt('ZUL_WANR'));
			}

			$Form->Erstelle_ListenFeld('ZUL_WANR',$WANr.($rsZUL->FeldInhalt('ZUB_WANRKORR')==''?($rsZUL->FeldInhalt('ZUL_WANR')==''?'!':''):'*'),0,120,false,($DS%2),'',$Link,'T','');
			$Form->Erstelle_ListenFeld('ZUL_LIEFERMENGE',$rsZUL->FeldInhalt('ZUL_LIEFERMENGE'),0,90,false,($DS%2),'','','T','L');
			$Link = '';
			if($rsZUL->FeldInhalt('ZAL_ZLA_KEY')!='')
			{
				$Link = '../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUL->FeldInhalt('ZAL_ZLA_KEY');
			}
			$Form->Erstelle_ListenFeld('ZUL_ARTIKELNUMMER',$rsZUL->FeldInhalt('ZUL_ARTIKELNUMMER'),0,190,false,($DS%2),'',$Link);
			$Link='../zukaufhersteller/zukaufhersteller_Main.php?cmdAktion=Details&ZLH_KEY='.$rsZUL->FeldInhalt('ZLH_KEY');
			$Form->Erstelle_ListenFeld('ZLH_KUERZEL',$rsZUL->FeldInhalt('ZLH_KUERZEL'),0,60,false,($DS%2),'',$Link,'T','L',$rsZUL->FeldInhalt('ZLH_BEZEICHNUNG'));
			$Link ='';
			$Form->Erstelle_ListenFeld('ZUL_ARTIKELBEZEICHNUNG',$rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG'),0,350,false,($DS%2),'',$Link,'T','L');
			$Form->Erstelle_ListenFeld('ZUL_VKPR',$rsZUL->FeldInhalt('ZUL_VKPR'),0,80,false,($DS%2),'',$Link,'N2','R');

			//****************************************
			// Status
			//****************************************
			// Abweichungen berechnen
			$Status='';
			$TippText = '';

			if($rsZUL->FeldInhalt('ZUB_KEY')=='')
			{
				$Status .= 'B';
				$TippText .= '/(B)Bestellung nicht gefunden.'."\n";
			}
			if($rsZUL->FeldInhalt('ZUB_KEY')!='' AND ($rsZUL->FeldInhalt('ZUB_EK')!=$rsZUL->FeldInhalt('ZUL_EK')))
			{
				$Status .= 'P';
				$TippText .= '/(P)Preis weicht ab('.$Form->Format('N2',$rsZUL->FeldInhalt('ZUL_EK')).' / '.$Form->Format('N2',$rsZUL->FeldInhalt('ZUB_EK')).')';
			}
			if($rsZUL->FeldInhalt('RECHNUNGEN')>0)
			{
				$Status .= 'R';
				$TippText .= '/(R)Rechnung vorhanden';
			}
			$Form->Erstelle_ListenFeld('STATUS',$Status,0,55,false,($DS%2),'','','T','',substr($TippText,1));
			$Form->Erstelle_ListenFeld('ZUL_LIE_NR',$rsZUL->FeldInhalt('ZUL_LIE_NR'),0,60,false,($DS%2),'',$Link,'T','R',$rsZUL->FeldInhalt('LIE_NAME1'));

			$Form->ZeileEnde();

			$rsZUL->DSWeiter();
			$DS++;
		}

		$Link = './zukauflieferscheine_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	else
	{
		//echo '<table>';
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsZUL->FeldInhalt('ZUL_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_ZUL',serialize($Param));

		echo '<input type=hidden name=txtZUL_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukauflieferscheine_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUL->FeldInhalt('ZUL_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZUL->FeldInhalt('ZUL_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht10002&2)!=0);

		if($AWIS_KEY1==0)
		{
			$EditRecht=($Recht10002&6);
		}

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIE_NR'].':',200);
		if($EditRecht)
		{
			$AWISCursorPosition='txtZUL_LIE_NR';
		}
		$SQL = 'SELECT LIE_NR, LIE_NAME1 ';
		$SQL .= ' FROM LIEFERANTEN';
		$SQL .= ' WHERE EXISTS(SELECT * FROM Lieferanteninfos WHERE LIN_ITY_KEY = 1 AND LIN_WERT <> 0 AND LIN_LIE_NR = LIE_NR)';
		$SQL .= ' ORDER BY LIE_NAME1';
		$Form->Erstelle_SelectFeld('ZUL_LIE_NR',$rsZUL->FeldInhalt('ZUL_LIE_NR'),500,$EditRecht,$SQL);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIEFERSCHEINNR'].':',200);
		$Form->Erstelle_TextFeld('ZUL_LIEFERSCHEINNR',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_LIEFERSCHEINNR')),20,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EXTERNEID'].':',190);
		$Form->Erstelle_TextFeld('ZUL_EXTERNEID',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_EXTERNEID')),20,200,false,'','','','T');
		$Form->ZeileEnde();

			// Bestellinformationen
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_DATUM'].':',200);
		$Form->Erstelle_TextFeld('ZUL_DATUM',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_DATUM')),20,200,$EditRecht,'','','','DU');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_FIL_ID'].':',190);
		$Form->Erstelle_TextFeld('ZUL_FIL_ID',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_FIL_ID')),5,100,$EditRecht,'','','','T');
		$Link = ($rsZUL->FeldInhalt('ZUL_FIL_ID')==''?'':'/filialinfos/filialinfos_Main.php?cmdAktion=Filialinfos&FIL_ID='.$rsZUL->FeldInhalt('ZUL_FIL_ID'));
		$Form->Erstelle_TextFeld('#FIL_BEZ',$rsZUL->FeldInhalt('FIL_BEZ'),20,200,false,'','',$Link,'T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WANR'].':',200);
	 	$Form->Erstelle_TextFeld('ZUL_WANR',$rsZUL->FeldInhalt('ZUL_WANR'),20,200,false,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['ZUB_WANRKORR'].':',200);
	 	$Form->Erstelle_TextFeld('ZUB_WANRKORR',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUB_WANRKORR')),20,200,false,'','','','T');
		$Form->ZeileEnde();

        // Wenn keine WA angegeben ist => aus WA die Nummer in rot anzeigen!		
		if($rsZUL->FeldInhalt('ZUL_WANR')=='')
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WANR'].':',200);
            $Form->Erstelle_TextLabel($rsZUL->FeldInhalt('ZUB_WANRKORR')!=''?$rsZUL->FeldInhalt('ZUB_WANRKORR'):($rsZUL->FeldInhalt('ZUB_WANR')==''?$rsZUL->FeldInhalt('ZUL_WANR'):$rsZUL->FeldInhalt('ZUB_WANR')),200,'color:red');
            $Form->ZeileEnde();
		}
		

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_AUFTRAGSNUMMER'].':',200);
	 	$Form->Erstelle_TextFeld('ZUL_AUFTRAGSNUMMER',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_AUFTRAGSNUMMER')),20,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WAEHRUNG'].':',200);
		$Form->Erstelle_TextFeld('ZUL_WAEHRUNG',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_WAEHRUNG')),10,200,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_MWS_ID'].':',200);
		$SQL = 'SELECT MWS_ID, MWS_BEZEICHNUNG || \' - \' || MWS_LAN_CODE FROM Mehrwertsteuersaetze ORDER BY 2';
		$Form->Erstelle_SelectFeld('ZUL_MWS_ID',$rsZUL->FeldInhalt('ZUL_MWS_ID'),200,$EditRecht,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('',400);
		$Form->Erstelle_TextLabel('|',5);
		$Link = '../zukaufbestellungen/zukaufbestellungen_Main.php?cmdAktion=Details&ZUB_KEY=0'.$rsZUL->FeldInhalt('ZUB_KEY');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUB']['tit_Zukaufbestellung'],200,'',$Link);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_LIEFERMENGE'].':',200);
		$Form->Erstelle_TextFeld('ZUL_LIEFERMENGE',$rsZUL->FeldInhalt('ZUL_LIEFERMENGE'),10,200,$EditRecht,'','','','N3');
		$Form->Erstelle_TextLabel('|',5);
		$Form->Erstelle_TextFeld('ZUB_BESTELLMENGE',$rsZUL->FeldInhalt('ZUB_BESTELLMENGE'),10,200,false,'','','','N3');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EINHEITENPROPREIS'].':',200);
		$Form->Erstelle_TextFeld('ZUL_EINHEITENPROPREIS',$rsZUL->FeldInhalt('ZUL_EINHEITENPROPREIS'),10,200,$EditRecht,'','','','T');
		$Form->Erstelle_TextLabel('|',5);
		$Form->Erstelle_TextFeld('ZUB_EINHEITENPROPREIS',$rsZUL->FeldInhalt('ZUB_EINHEITENPROPREIS'),10,200,false,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_VKPR'].':',200);
		$Form->Erstelle_TextFeld('ZUL_VKPR',$rsZUL->FeldInhalt('ZUL_VKPR'),10,200,$EditRecht,'','','','N2');
		$Form->Erstelle_TextLabel('|',5);
        $MwSt = $rsZUL->FeldInhalt('MWS_SATZ');
        $Style = '';
        if($MwSt=='') {
            $AwisMwst = new awisMehrwertsteuer($DB,$rsZUL->FeldInhalt('ZUL_DATUM'));
            $MwSt = $AwisMwst->Steuersatz('DE',awisMehrwertsteuer::TYP_VOLL,awisMehrwertsteuer::RUECKGABETYP_PROZENT_ARRAY)[0]['wert'];
            $Style = 'Kursiv';
        }
		$VKPR_BEST = $DB->FeldInhaltformat('N4',$rsZUL->FeldInhalt('ZUB_PREISANGELIEFERT'))/(1+($MwSt/100));
		//$Form->Erstelle_TextFeld('ZAP_VKPR',$rsZUL->FeldInhalt('ZAP_VKPR'),10,200,false,$Style,'','','N2');
		$Form->Erstelle_TextFeld('ZUB_PREISANGELIEFERT',$VKPR_BEST,10,200,false,$Style,'','','N2');
		//$Form->Erstelle_TextFeld('MWS_SATZ',$rsZUL->FeldInhalt('MWS_SATZ'),10,200,false,'','','','N2');
		$Form->ZeileEnde();

		if(($Recht10002&16)==16)			// Rechte, um EKS zu sehen?
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EKPR'].':',200);
			$Form->Erstelle_TextFeld('ZUL_EKPR',$rsZUL->FeldInhalt('ZUL_EKPR'),10,200,$EditRecht,'','','','N2');
		    $Form->Erstelle_TextLabel('|',5);
			$Form->Erstelle_TextFeld('#ZAP_EKPR',$rsZUL->FeldInhalt('ZAP_EKPR'),10,200,false,'','','','N2');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_RABATT1'].':',200);
			$Form->Erstelle_TextFeld('ZUL_RABATT1',$rsZUL->FeldInhalt('ZUL_RABATT1'),10,200,$EditRecht,'','','','N2');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_RABATT2'].':',200);
			$Form->Erstelle_TextFeld('ZUL_RABATT2',$rsZUL->FeldInhalt('ZUL_RABATT2'),10,200,$EditRecht,'','','','N2');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_RABATT3'].':',200);
			$Form->Erstelle_TextFeld('ZUL_RABATT3',$rsZUL->FeldInhalt('ZUL_RABATT3'),10,200,$EditRecht,'','','','N2');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$EK = $DB->FeldInhaltFormat('N2',$rsZUL->FeldInhalt('ZUL_EKPR'))*(1-$DB->FeldInhaltFormat('N2',$rsZUL->FeldInhalt('ZUL_RABATT1'))/100)*(1-$DB->FeldInhaltFormat('N2',$rsZUL->FeldInhalt('ZUL_RABATT2'))/100)*(1-$DB->FeldInhaltFormat('N2',$rsZUL->FeldInhalt('ZUL_RABATT3'))/100);
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_EK'].':',200);
			$Form->Erstelle_TextFeld('*EK',$EK,10,200,false,'','','','N2');
			$Form->Erstelle_TextLabel('|',5);
			$EKPR_BEST = $DB->FeldInhaltformat('N4',$rsZUL->FeldInhalt('ZAP_EK'));
			$Form->Erstelle_TextFeld('ZAP_EK',$EKPR_BEST,10,200,false,'','','','N2');
			$Form->ZeileEnde();

			$Form->ZeileStart();
			$AltTeilWert = $DB->FeldInhaltFormat('N2',$rsZUL->FeldInhalt('ZUL_ALTTEILWERT'));
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ALTTEILWERT'].':',200);
			$Form->Erstelle_TextFeld('*AltteilWert',$AltTeilWert,10,200,false,'','','','N2');
			$Form->Erstelle_TextLabel('|',5);
			$ATW_BEST = $DB->FeldInhaltformat('N4',$rsZUL->FeldInhalt('ZUB_ALTTEILWERT'));
			$Form->Erstelle_TextFeld('ZUB_ALTTEILWERT',$ATW_BEST,10,200,false,'','','','N2');
			$Form->ZeileEnde();
		}




			// Nummer und Hersteller
		$Form->ZeileStart();
		$Link = $rsZUL->FeldInhalt('ZAL_ZLA_KEY')==''?'':'../zukaufartikel/zukaufartikel_Main.php?cmdAktion=Details&ZLA_KEY='.$rsZUL->FeldInhalt('ZAL_ZLA_KEY');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELNUMMER'].':',200,'',$Link);
		$Form->Erstelle_TextFeld('ZUL_ARTIKELNUMMER',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_ARTIKELNUMMER')),20,200,$EditRecht);
		$Link = '../zukaufhersteller/zukaufhersteller_Main.php?cmdAktion=Details&ZLH_KEY='.$rsZUL->FeldInhalt('ZUL_ZLH_KEY');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_HERSTELLER'].':',190,'',$Link);
		$Form->Erstelle_TextFeld('ZUL_HERSTELLER',$rsZUL->FeldInhalt('ZUL_HERSTELLER'),7,100,$EditRecht);
        $Form->Erstelle_TextLabel($rsZUL->FeldInhalt('ZLH_BEZEICHNUNG'),190);
		$Form->ZeileEnde();

			// Bezeichnung
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ARTIKELBEZEICHNUNG'].':',200);
		$Form->Erstelle_TextFeld('ZUL_ARTIKELBEZEICHNUNG',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_ARTIKELBEZEICHNUNG')),50,200,$EditRecht);
		$Form->ZeileEnde();

				// Abschluss
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ABSCHLUSSART'].':',200);
		$Liste = explode("|",$AWISSprachKonserven['ZUL']['lst_ZUL_ABSCHLUSSART']);
		$Form->Erstelle_SelectFeld('ZUL_ABSCHLUSSART',$rsZUL->FeldInhalt('ZUL_ABSCHLUSSART'),'200:190',$EditRecht,'','','1','','',$Liste,'');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ABSCHLUSSGRUND'].':',200);
		$Form->Erstelle_TextFeld('ZUL_ABSCHLUSSGRUND',($AWIS_KEY1===0?'':$rsZUL->FeldInhalt('ZUL_ABSCHLUSSGRUND')),50,200,$EditRecht);
		$Form->ZeileEnde();

		
		if($rsZUL->FeldInhalt('ZUL_ZUSATZBETRAG')<>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ZUSATZBETRAG'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_ZUSATZBETRAG',$rsZUL->FeldInhalt('ZUL_ZUSATZBETRAG'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_ZUSATZBETRAGTYP'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_ZUSATZBETRAGTYP',$rsZUL->FeldInhalt('ZUL_ZUSATZBETRAGTYP'),20,200,$EditRecht,'','','','T');
		    $Form->ZeileEnde();
		}
		
		if($rsZUL->FeldInhalt('ZUL_UMWELTPAUSCHALEMENGE')>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_UMWELTPAUSCHALE'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_UMWELTPAUSCHALE',$rsZUL->FeldInhalt('ZUL_UMWELTPAUSCHALE'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_UMWELTPAUSCHALEMENGE'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_UMWELTPAUSCHALEMENGE',$rsZUL->FeldInhalt('ZUL_UMWELTPAUSCHALEMENGE'),5,100,$EditRecht,'','','','Nx');
		    $Form->ZeileEnde();
		}
		
		if($rsZUL->FeldInhalt('ZUL_WIEDEREINLAGERUNGMENGE')>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WIEDEREINLAGERUNG'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_WIEDEREINLAGERUNG',$rsZUL->FeldInhalt('ZUL_WIEDEREINLAGERUNG'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_WIEDEREINLAGERUNGMENGE'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_WIEDEREINLAGERUNGMENGE',$rsZUL->FeldInhalt('ZUL_WIEDEREINLAGERUNGMENGE'),5,100,$EditRecht,'','','','Nx');
		    $Form->ZeileEnde();
		}
		
		if($rsZUL->FeldInhalt('ZUL_SONSTIGEMENGE')>0)
		{
		    $Form->ZeileStart();
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_SONSTIGEKOSTEN'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_SONSTIGEKOSTEN',$rsZUL->FeldInhalt('ZUL_SONSTIGEKOSTEN'),10,200,$EditRecht,'','text-align:right;','','N2');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_SONSTIGEMENGE'].':',200);
		    $Form->Erstelle_TextFeld('ZUL_SONSTIGEMENGE',$rsZUL->FeldInhalt('ZUL_SONSTIGEMENGE'),5,100,$EditRecht,'','','','Nx');
		    $Form->Erstelle_TextLabel($AWISSprachKonserven['ZUL']['ZUL_SONSTIGEBEZEICHNUNG'].':',100);
		    $Form->Erstelle_TextFeld('ZUL_SONSTIGEBEZEICHNUNG',$rsZUL->FeldInhalt('ZUL_SONSTIGEBEZEICHNUNG'),36,200,$EditRecht,'','','','T');
		    $Form->ZeileEnde();
		}
		
		
		if($rsZUL->FeldInhalt('ZLA_AST_ATUNR')!='')
		{
			$Form->Trennzeile();

			$SQL = 'SELECT Artikelstamm.* ';
			$SQL .= ',(SELECT ATW_BETRAG FROM Altteilwerte WHERE ATW_KENNUNG = (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND asi_ast_atunr=\''.$rsZUL->FeldInhalt('ZLA_AST_ATUNR').'\' AND asi_ait_id=190) AND ATW_LAN_CODE=\'DE\') AS AST_ALTTEILWERT';
			$SQL .= ',(SELECT ASI_WERT FROM ArtikelStammInfos WHERE ROWNUM<=1 AND  asi_ast_atunr=\''.$rsZUL->FeldInhalt('ZLA_AST_ATUNR').'\' AND asi_ait_id=191) AS AST_VPE';
			$SQL .= ' FROM Artikelstamm WHERE AST_ATUNR='.$DB->FeldInhaltFormat('T',$rsZUL->FeldInhalt('ZLA_AST_ATUNR'));
			$rsAST = $DB->RecordSetOeffnen($SQL);
			if(!$rsAST->EOF())
			{
				$Form->ZeileStart();
				$Link = '/artikelstamm/artikelstamm_Main.php?ATUNR='.$rsAST->FeldInhalt('AST_ATUNR').'&cmdAktion=ArtikelInfos';
				$Form->Erstelle_TextLabel($AWISSprachKonserven['ZLA']['ZLA_AST_ATUNR'].':',200,'');
				$Form->Erstelle_TextFeld('AST_ATUNR',$rsAST->FeldInhalt('AST_ATUNR'),20,200,false,'','',$Link);
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_BEZEICHNUNGWW'].':',200);
				$Form->Erstelle_TextFeld('AST_BEZEICHNUNGWW',$rsAST->FeldInhalt('AST_BEZEICHNUNGWW'),20,500,false);
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VK'].':',200);
				$Form->Erstelle_TextFeld('AST_VK',$rsAST->FeldInhalt('AST_VK'),20,200,false,'','','','N2');
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_ALTTEILWERT'].':',200);
				$Form->Erstelle_TextFeld('AST_ALTTEILWERT',$rsAST->FeldInhalt('AST_ALTTEILWERT'),20,200,false,'','','','T');
				$Form->ZeileEnde();

				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['AST']['AST_VPE'].':',200);
				$Form->Erstelle_TextFeld('AST_VPE',$rsAST->FeldInhalt('AST_VPE'),20,200,false,'','','','N0');
				$Form->ZeileEnde();
			}
		}

		$Form->Formular_Ende();

		if(!$rsZUL->EOF())
		{
    		$Reg = new awisRegister(10015);
    		$Reg->ZeichneRegister();
		}

	}

	//awis_Debug(1, $Param, $Bedingung, $rsZUL, $_POST, $rsAZG, $SQL, $AWISSprache);

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if(($Recht10002&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if(($Recht10002&4) == 4)		// Hinzuf�gen erlaubt?
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');


	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND ZUL_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['ZUL_ARTIKELNUMMER']) AND $Param['ZUL_ARTIKELNUMMER']!='')
	{
		$Bedingung .= ' AND (ZUL_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUL_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . '';
		$Bedingung .= ' OR ZLA_ARTIKELNUMMER ' . $DB->LIKEoderIST($Param['ZUL_ARTIKELNUMMER'],awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['ZLH_KEY']) AND $Param['ZLH_KEY']>0)
	{
		$Bedingung .= ' AND EXISTS (SELECT * FROM ZUKAUFHERSTELLERCODES';
		$Bedingung .= ' INNER JOIN ZUKAUFLIEFERANTENHERSTELLER ON ZLH_KEY = ZHK_ZLH_KEY';
		$Bedingung .= ' WHERE ZLH_KEY = ' . $DB->FeldInhaltFormat('N0',$Param['ZLH_KEY']);
		$Bedingung .= ' AND ZHK_CODE = ZUL_HERSTELLER';
		$Bedingung .= ' )';
	}
	if(isset($Param['ZUL_FIL_ID']) AND $Param['ZUL_FIL_ID']!='')
	{
		$Bedingung .= ' AND ZUL_FIL_ID = ' . $Param['ZUL_FIL_ID'] . ' ';
	}

	if(isset($Param['ZUL_LIE_NR']) AND $Param['ZUL_LIE_NR']!='')
	{
		$Bedingung .= ' AND ZUL_LIE_NR = ' . $Param['ZUL_LIE_NR'] . ' ';
	}

	if(isset($Param['ZSZ_SORTIMENT']) AND $Param['ZSZ_SORTIMENT']!='')
	{
		$Bedingung .= ' AND ZUL_SORTIMENT = ' . $DB->FeldInhaltFormat('T',$Param['ZSZ_SORTIMENT']) . ' ';
	}

	if(isset($Param['DATUMVOM']) AND $Param['DATUMVOM']!='')
	{
		$Bedingung .= ' AND ZUL_DATUM >= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMVOM']) . ' ';
	}
	if(isset($Param['DATUMBIS']) AND $Param['DATUMBIS']!='')
	{
		$Bedingung .= ' AND ZUL_DATUM <= ' . $DB->FeldInhaltFormat('DU',$Param['DATUMBIS']) . ' ';
	}

	if(isset($Param['ZUL_EXTERNEID']) AND $Param['ZUL_EXTERNEID']!='')
	{
		$Bedingung .= ' AND ZUL_EXTERNEID ' . $DB->LIKEoderIST($Param['ZUL_EXTERNEID'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	if(isset($Param['ZUL_LIEFERSCHEINNR']) AND $Param['ZUL_LIEFERSCHEINNR']!='')
	{
		$Bedingung .= ' AND ZUL_LIEFERSCHEINNR ' . $DB->LIKEoderIST($Param['ZUL_LIEFERSCHEINNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}
	if(isset($Param['ZUL_WANR']) AND $Param['ZUL_WANR']!='')
	{
		$Bedingung .= ' AND ZUL_WANR ' . $DB->LIKEoderIST($Param['ZUL_WANR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
	}

	if(isset($Param['ZUL_ZSZ_KEY']) AND $Param['ZUL_ZSZ_KEY']>0)
	{
		$Bedingung .= ' AND ZUL_ZSZ_KEY= ' . intval($Param['ZUL_ZSZ_KEY']) . ' ';
	}
	if(isset($Param['AST_ATUNR']) AND $Param['AST_ATUNR']!='')
	{
		//$Bedingung .= ' AND (ZAL_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ' AND (ZLA_AST_ATUNR ' . $DB->LIKEoderIST($Param['AST_ATUNR'],awisDatenbank::AWIS_LIKE_UPPER) . ' ';
		$Bedingung .= ')';
	}
	if(isset($Param['ZUL_TAUSCHTEIL']) AND $Param['ZUL_TAUSCHTEIL']>0)
	{
		$Bedingung .= ' AND ZUL_TAUSCHTEIL = ' . intval($Param['ZUL_TAUSCHTEIL']) . ' ';
	}

	$Param['WHERE']=$Bedingung;

	return $Bedingung;
}
?>