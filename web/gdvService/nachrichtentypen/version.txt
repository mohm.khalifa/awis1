#########################################################################
#
# Version des AWIS-Moduls
#
#	optionale Beschreibung f�r ein Modul
#
#	Abschnitt
#	[Header]	Infos f�r die Header-Datei
#	[Versionen]	Aktuelle Modulversion und History f�r das Modul
#
#########################################################################

[Header]

Modulname=GDV-Service-Nachrichtentypen
Produktname=AWIS
Startseite=/index.php
Logo=/bilder/atulogo_neu_gross.png
Sprachen=DE

##########################################################################
# Versionshistorie
#
#  Aktuelle Versionen oben!
#
#version;Versionsbeschreibung;Datum;Autor
#########################################################################

[Versionen]
1.00.01;Erste Version;05.07.2016;<a href=mailto:tobias.schaeffler@de.atu.eu>Tobias Schaeffler</a>

