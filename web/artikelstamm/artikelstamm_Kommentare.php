<?php
global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWISCursorPosition;

try {
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('AST', '%');
    $TextKonserven[] = array('ASI', '%');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Fehler', 'err_keineRechte');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');

    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $Form = new awisFormular();
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $MitReklInfo = $AWISBenutzer->ParameterLesen('ReklamationsInfoLieferanten');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht453 = $AWISBenutzer->HatDasRecht(453);            // Recht für die Kommentare
    if ($Recht453 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
    if ($BildschirmBreite < 1024) {
        $FeldBreiten['KOMMENTAR'] = 500;
        $FeldBreiten['UEBERSCHRIFT'] = 686;
        $FeldBreiten['USER'] = 90;
    } elseif ($BildschirmBreite < 1280) {
        $FeldBreiten['KOMMENTAR'] = 700;
        $FeldBreiten['USER'] = 100;
        $FeldBreiten['UEBERSCHRIFT'] = 896;
    } else {
        $FeldBreiten['KOMMENTAR'] = 910;
        $FeldBreiten['UEBERSCHRIFT'] = 1160;
        $FeldBreiten['USER'] = 150;
    }

    $Bedingung = '';

    $ASIKEYS = '';
    $ANZAHLASIKEYS = 0;

    if (($Recht453 & 2) == 2)            // Katalog anzeigen
    {
        $ASIKEYS .= ',110';
        $ANZAHLASIKEYS++;
    }
    if (($Recht453 & 16) == 16)            // Einkauf anzeigen
    {
        $ASIKEYS .= ',111';
        $ANZAHLASIKEYS++;
    }
    if (($Recht453 & 128) == 128)        // TKD anzeigen
    {
        $ASIKEYS .= ',112';
        $ANZAHLASIKEYS++;
    }

    if (isset($_GET['ASIKEY'])) {
        $Bedingung .= ' AND ASI_AIT_ID IN (' . substr($ASIKEYS, 1) . ')';

        $SQL = 'SELECT *';
        $SQL .= ' FROM ArtikelstammInfos ';
        $SQL .= ' WHERE ASI_KEY = ' . $DB->FeldInhaltFormat('N0', $_GET['ASIKEY'], false);
        $SQL .= $Bedingung;        // Wg. Berechtigungen
    } else {
        $Bedingung .= ' AND AIT_ID IN (' . substr($ASIKEYS, 1) . ')';

        $SQL = "SELECT *";
        $SQL .= ' FROM AWIS.ArtikelstammInfosTypen';
        $SQL .= ' ' . ($Bedingung == ''?'':' WHERE ' . substr($Bedingung, 4));    // Wg. Berechtigungen

        if (isset($_GET['AKOSort'])) {
            $SQL .= ' ORDER BY ' . str_replace('~', ' DESC ', $_GET['AKOSort']);
        } else {
            $SQL .= ' ORDER BY AIT_ID';
        }
    }

    $rsAIT = $DB->RecordSetOeffnen($SQL);

    if ($rsAIT->AnzahlDatensaetze() > 1 or isset($_GET['ASIListe']) or ($ANZAHLASIKEYS == 1 and !isset($_GET['ASIKEY'])))                    // Liste anzeigen
    {
        // Liste anzeigen
        $Form->Formular_Start();

        $Form->ZeileStart();

        $Link = './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Kommentare';
        $Link .= '&AKOSort=AIT_ID' . ((isset($_GET['AKOSort']) AND ($_GET['AKOSort'] == 'AIT_ID'))?'~':'');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ASI']['wrd_Kommentar'], $FeldBreiten['KOMMENTAR'], '', $Link);
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ASI']['ASI_USER'], $FeldBreiten['USER'], '');
        $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ASI']['ASI_USERDAT'], 104, '');

        $Form->ZeileEnde();

        $DS = 0;
        $LetzteASIKEY = 0;

        while (!$rsAIT->EOF()) {
            // Neue Gruppe von Kommentaren?
            if ($LetzteASIKEY != $rsAIT->FeldInhalt('AIT_ID')) {
                $Form->ZeileStart();

                $Icons = array();
                if ((($Recht453 & 4) != 0 AND $rsAIT->FeldInhalt('AIT_ID') == 110)   /* Ändernrecht	Katalog*/ OR (($Recht453 & 32) != 0 AND $rsAIT->FeldInhalt('AIT_ID') == 111)    /* Ändernrecht  Einkauf*/ OR (($Recht453 & 256) != 0 AND $rsAIT->FeldInhalt('AIT_ID') == 112))    /* Ändernrecht TKD */ {
                    $Icons[] = array('new', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Kommentare&ASIKEY=0&AITID=' . $rsAIT->FeldInhalt('AIT_ID'), 'g');
                }
                $Form->Erstelle_ListeIcons($Icons, 18, ($DS % 2));

                $Form->Erstelle_Liste_Ueberschrift($rsAIT->FeldInhalt('AIT_INFORMATION'), $FeldBreiten['UEBERSCHRIFT'] - 10);
                $Form->ZeileEnde();
                $LetzteASIKEY = $rsAIT->FeldInhalt('AIT_ID');
            }

            $SQL = 'SELECT *';
            $SQL .= ' FROM ArtikelstammInfos ';
            $SQL .= ' INNER JOIN Artikelstamm ON AST_ATUNR = ASI_AST_ATUNR AND AST_KEY=' . $DB->WertSetzen('ASI', 'N0', $AWIS_KEY1);
            $SQL .= ' WHERE ASI_AIT_ID = ' . $DB->WertSetzen('ASI', 'N0', $rsAIT->FeldInhalt('AIT_ID'));
            $SQL .= ' ORDER BY ASI_USERDAT DESC';

            $rsASI = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('ASI'));

            while (!$rsASI->EOF()) {
                $Form->ZeileStart();
                $Icons = array();

                // Daten ändern
                if ((($Recht453 & 4) != 0 AND $rsAIT->FeldInhalt('AIT_ID') == 110)    /* Ändernrecht	Katalog*/ OR (($Recht453 & 32) != 0 AND $rsAIT->FeldInhalt('AIT_ID') == 111)    /* Ändernrecht  Einkauf*/ OR (($Recht453 & 512) != 0 AND $rsAIT->FeldInhalt('AIT_ID') == 112))    /* Ändernrecht TKD*/ {
                    $Icons[] = array('edit', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Kommentare&ASIKEY=' . $rsASI->FeldInhalt('ASI_KEY'));
                    $Icons[] = array('delete', './artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Kommentare&Del=' . $rsASI->FeldInhalt('ASI_KEY') . '&Key=' . $AWIS_KEY1);
                }

                $Form->Erstelle_ListeIcons($Icons, 38, ($DS % 2));

                $Form->Erstelle_ListenFeld('*ASI_WERT', $rsASI->FeldInhalt('ASI_WERT'), 10, $FeldBreiten['KOMMENTAR'], false, ($DS % 2), '', '', 'T', 'L',
                    $rsASI->FeldInhalt('ASI_WERT'));
                $Form->Erstelle_ListenFeld('*ASI_USER', $rsASI->FeldInhalt('ASI_USER'), 10, $FeldBreiten['USER'] - 32, false, ($DS % 2), '', '', 'T');
                $Form->Erstelle_ListenFeld('*ASI_USERDAT', $rsASI->FeldInhalt('ASI_USERDAT'), 10, 100, false, ($DS % 2), '', '', 'D');

                $Form->ZeileEnde();
                $DS++;

                $rsASI->DSWeiter();
            }

            $rsAIT->DSWeiter();
        }

        $Form->Formular_Ende();
    } else        // Einer oder keiner
    {
        $Form->Formular_Start();
        $AWIS_KEY2 = ($rsAIT->FeldInhalt('ASI_KEY') != ''?$rsAIT->FeldInhalt('ASI_KEY'):'0');
        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array(
            'Style' => 'font-size:smaller;',
            'Inhalt' => "<a href=./artikelstamm_Main.php?cmdAktion=Artikelinfo&Seite=Kommentare&ASIListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
        );
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAIT->FeldInhalt('ASI_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsAIT->FeldInhalt('ASI_USERDAT'));
        $Form->InfoZeile($Felder, '');

        $Form->ZeileStart();

        $EditModus = false;
        if ($rsAIT->FeldInhalt('ASI_WERT') == '' OR ((($Recht453 & 4) != 0 AND $rsAIT->FeldInhalt('ASI_AIT_ID') == 110)    /* Ändernrecht	Katalog */ OR (($Recht453 & 32) != 0 AND $rsAIT->FeldInhalt('ASI_AIT_ID') == 111)    /* Ändernrecht  Einkauf */ OR (($Recht453 & 256) != 0 AND $rsAIT->FeldInhalt('ASI_AIT_ID') == 112)))    /* Ändernrecht TKD */ {
            $EditModus = true;
        }

        $ASIAITKEY = $DB->FeldInhaltFormat('N0', $rsAIT->FeldInhalt('ASI_AIT_ID') != ''?$rsAIT->FeldInhalt('ASI_AIT_ID'):(isset($_GET['AITID'])?$_GET['AITID']:0));
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['ASI']['ASI_KOMMENTAR'] . ':', 150);
        $Form->Erstelle_Textarea('ASI_' . $ASIAITKEY . '_' . $AWIS_KEY2, $rsAIT->FeldInhalt('ASI_WERT'), 600, 130, 5, $EditModus);
        $AWISCursorPosition = ($EditModus?'txtASI_' . $ASIAITKEY . '_' . $AWIS_KEY2:$AWISCursorPosition);
        $Form->ZeileEnde();

        $Form->Formular_Ende();
    }
} catch (awisException $ex) {
    if ($Form instanceof awisFormular) {
        $Form->DebugAusgabe(1, $ex->getSQL());
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200810060008");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812061223");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>