
<html>
<head>
<title>Awis - Shortcutleiste</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $con;
global $awisRSZeilen;
global $cmdSpeichern;
global $AWISBenutzer;

echo "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php


include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();
							// wie Benutzerparameter
$RechteStufe = awisBenutzerRecht($con,6);
if($RechteStufe<=0)
{
    awisEreignis(3,1000,'ShortCutLeiste',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}


/*************************************************
* Daten speichern
*************************************************/

if($_POST['cmdSpeichern_x']!='')
{
	$ShortCutLeiste='';
	foreach($_POST as $Eintrag=>$Feld)
	{
		if(strstr($Eintrag,"txtFeld"))
		{
			if(strstr($Feld,"%"))
			{
				$Feld = str_replace("%3A",":",$Feld);
				$Feld = str_replace("%3B",";",$Feld);
				$Feld = str_replace("%2C",",",$Feld);
				$Feld = str_replace("+"," ",$Feld);			
			}
			$ShortCutLeiste .= ';' . $Feld;
		}
	}
	awis_BenutzerParameterSpeichern($con,'ShortCutLeiste',$AWISBenutzer->BenutzerName(),substr($ShortCutLeiste, 1));
}

/************************************************
* Daten anzeigen
************************************************/

echo "<FORM name=frmBenutzerParameter method=post action=./ShortCutLeiste.php>";

echo "<table id=Farbig border=1 width=100%>";
echo "<tr><td id=FeldBez>Fensterart</td><td id=FeldBez>Link</td><td id=FeldBez>Symbol</td><td id=FeldBez>Bemerkung</td></tr>";

$ShortCutLeiste = awis_BenutzerParameter($con, 'ShortCutLeiste', $AWISBenutzer->BenutzerName());
$ShortCutLeiste = explode(';',$ShortCutLeiste);

$ShortBilder = explode(';',awis_BenutzerParameter($con, 'ShortCutBilder', $AWISBenutzer->BenutzerName()));

$i=0;
for($Knopf=0;$Knopf<6;$Knopf++)
{
	echo '<tr>';
	echo '<td><select name=txtFeld' . $i . '>';
	echo '<option value=_self ' . ($ShortCutLeiste[$i]=='_self'?'selected':'') . '>Aktuelles Fenster</option>';
	echo '<option value=_blank ' . ($ShortCutLeiste[$i]=='_blank'?'selected':'') . '>Neues Fenster</option>';
	$i++;
	echo '</select></td>';
	
	echo '<td>';
	echo '<input size=80 name=txtFeld' . $i . ' type=text value=' . $ShortCutLeiste[$i++] . '>';
	echo '</td>';
	
	echo '<td><select name=txtFeld' . $i . '>';
	for($Bild=0;$Bild<sizeof($ShortBilder);$Bild++)
	{
		echo '<option value= ' . $ShortBilder[$Bild] . ($ShortCutLeiste[$i]==$ShortBilder[$Bild++]?" selected":'') . '>' . $ShortBilder[$Bild] . '</option>';
	}
	$i++;
	echo '</select></td>';
	
	echo '<td>';
	echo '<input size=30 name=txtFeld' . $i . ' type=text value=\'' . $ShortCutLeiste[$i++] . '\'>';
	echo '</td>';
}

echo "</table>";
echo "<br><input type=image alt=Speichern src=/bilder/diskette.png name=cmdSpeichern>";

echo "</FORM>";

echo "<hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";

echo "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=shortcutleiste','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awislogoff($con);
?>

</body>
</html>
