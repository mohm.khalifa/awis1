<?php
// Variablen
global $PHP_AUTH_USER;	// Anmeldename
global $HTTP_GET_VARS;
global $HTTP_POST_VARS;
global $QUERY_STRING;

// Daten bearbeiten?
if(isset($HTTP_GET_VARS["Edit"]))
{
	$Bearbeiten = TRUE;
	$BearbeitenKey = $HTTP_GET_VARS["Edit"];
}
else
{
	$Bearbeiten = FALSE;
	$BearbeitenKey = 0;
}

/* L�nderListe */
$rsLaender = awisOpenRecordset($con, "SELECT LAN_CODE FROM LAENDER ORDER BY LAN_CODE");
$rsLaenderZeilen = $awisRSZeilen;

/****************************************************************
* Ge�nderte Daten speichern
****************************************************************/



if(isset($HTTP_POST_VARS["cmdSpeichern_x"]))
{
	$Bearbeiten = FALSE;		// Keine Datenbearbeitung
	$BearbeitenKey = 0;

	$SQL = "";
	
	if($HTTP_POST_VARS["txtNAA_KEY"]>0)
	{
		$SQL = "UPDATE NachlassArten SET ";
		$SQL .= "NAA_ID=" . $HTTP_POST_VARS["txtNAA_ID"];
		$SQL .= ", NAA_MODUS='" . $HTTP_POST_VARS["txtNAA_MODUS"] . "'";
		$SQL .= ", NAA_LAN_CODE='" . $HTTP_POST_VARS["txtNAA_LAN_CODE"] . "'";
		$SQL .= ", NAA_BEZ='" . $HTTP_POST_VARS["txtNAA_BEZ"] . "'";
		$SQL .= ", NAA_BEMERKUNG='" . $HTTP_POST_VARS["txtNAA_BEMERKUNG"] . "'";
		$SQL .= ", NAA_AKTIV=" . $HTTP_POST_VARS["txtNAA_AKTIV"];
		$SQL .= ", NAA_BONTEXT='" . $HTTP_POST_VARS["txtNAA_BONTEXT"] . "'";
		$SQL .= ", NAA_START=TO_DATE('" . $HTTP_POST_VARS["txtNAA_START"] . "','dd.mm.rrrr')";
		$SQL .= ", NAA_ENDE=TO_DATE('" . $HTTP_POST_VARS["txtNAA_ENDE"] . "','dd.mm.rrrr')";
		$SQL .= ", NAA_BEGRUENDUNG='" . $HTTP_POST_VARS["txtNAA_BEGRUENDUNG"] . "'";
		$SQL .= ", NAA_GRUPPIERUNG='" . $HTTP_POST_VARS["txtNAA_GRUPPIERUNG"] . "'";
		$SQL .= ", NAA_EAN='" . $HTTP_POST_VARS["txtNAA_EAN"] . "'";
		$SQL .= ", NAA_RABATTPROZ=0" . awis_format($HTTP_POST_VARS["txtNAA_RABATTPROZ"],'US-Zahl');
		$SQL .= ", NAA_RABATTBETRAG=" . awis_format($HTTP_POST_VARS["txtNAA_RABATTBETRAG"],'US-Zahl');
		$SQL .= ", NAA_USERDAT=SYSDATE" ;
		$SQL .= ", NAA_USER='" . $PHP_AUTH_USER . "'";

		$SQL .= " WHERE NAA_KEY=0" . $HTTP_POST_VARS["txtNAA_KEY"];

//print "<hr>$SQL<hr>"		;
		awisExecute($con, $SQL);
	}
	else	// Neu einf�gen
	{
		$SQL = "INSERT INTO NachlassArten(";
		$SQL .= "NAA_ID";
		$SQL .= ", NAA_MODUS";
		$SQL .= ", NAA_LAN_CODE";
		$SQL .= ", NAA_BEZ";
		$SQL .= ", NAA_BEMERKUNG";
		$SQL .= ", NAA_AKTIV";
		$SQL .= ", NAA_BONTEXT";
		$SQL .= ", NAA_START";
		$SQL .= ", NAA_ENDE";
		$SQL .= ", NAA_BEGRUENDUNG";
		$SQL .= ", NAA_GRUPPIERUNG";
		$SQL .= ", NAA_EAN";
		$SQL .= ", NAA_RABATTPROZ";
		$SQL .= ", NAA_RABATTBETRAG";
		$SQL .= ", NAA_USER";
		$SQL .= ", NAA_USERDAT";
		$SQL .= ") VALUES (";
		$SQL .= "'" . $HTTP_POST_VARS["txtNAA_ID"] . "'";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_MODUS"] . "'";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_LAN_CODE"] . "'";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_BEZ"] . "'";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_BEMERKUNG"] . "'";
		$SQL .= "," . $HTTP_POST_VARS["txtNAA_AKTIV"] . "";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_BONTEXT"] . "'";
		$SQL .= ",TO_DATE('" . $HTTP_POST_VARS["txtNAA_START"] . "','dd.mm.rrrr')";
		$SQL .= ",TO_DATE('" . $HTTP_POST_VARS["txtNAA_ENDE"] . "','dd.mm.rrrr')";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_BEGRUENDUNG"] . "'";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_GRUPPIERUNG"] . "'";
		$SQL .= ",'" . $HTTP_POST_VARS["txtNAA_EAN"] . "'";
		$SQL .= "," . awis_format($HTTP_POST_VARS["txtNAA_RABATTPROZ"],'US-Zahl');
		$SQL .= "," . awis_format($HTTP_POST_VARS["txtNAA_RABATTBETRAG"],'US-Zahl');
		$SQL .= ",'" . $PHP_AUTH_USER . "'";
		$SQL .= ",SYSDATE";
		$SQL .= ")";	
	
		awisExecute($con, $SQL);
	}
}

/***************************************************************************************
* 
* Liste aufbauen
* 
****************************************************************************************/

$rsNachlaesse = awisOpenRecordset($con,"SELECT * FROM NachlassArten ORDER BY NAA_AKTIV DESC, NAA_BEZ");
$rsNachlaesseZeilen = $awisRSZeilen;

// Feldbezeichnungen zu den Feldern laden
$Felder = array_Keys($rsNachlaesse);
for($i=0;$i<sizeof($Felder);$i++)
{
	$FeldListe[$i] = $Felder[$i];
}
$FeldListe = awis_FeldNamenListe($con, $FeldListe, $SatzNr = 1);

// �berschrift aufbauen

print "<table  width=100% id=DatenTabelle>";

SchreibeUeberschrift($FeldListe);

print "<form name=frmNachlassarten method=post action=nachlaesse_Main.php?cmdAktion=Nachlassarten>";
if($HTTP_POST_VARS['cmdAlleAnzeigen_x']!='' OR $HTTP_GET_VARS['cmdAlleAnzeigen_x']!='')
{
	echo '<input name=cmdAlleAnzeigen_x type=hidden value=Ja>';
}

// Tabelle aufbauen

/****************************************************************
* Vorhandene Daten kopieren und dann bearbeiten
****************************************************************/

$Key = awis_NameInArray($HTTP_POST_VARS,"cmdKopieren");
if($Key!='')
{
	$Key = intval(substr($Key, strpos($Key,"_")+1));
	for($i=0;$i<$rsNachlaesseZeilen;$i++)
	{
		if($rsNachlaesse["NAA_KEY"][$i]==$Key)
		{
			break;	
		}
	}

	print "<td><input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=submit();></td>";

	print "<td><input type=hidden name=txtNAA_KEY value=0>";
	print "<input tabindex=0 size=3 name=txtNAA_ID value=". $rsNachlaesse["NAA_ID"][$i] ."></td>";
	print "<td align=center><input size=1 name=txtNAA_MODUS value='". $rsNachlaesse["NAA_MODUS"][$i] ."'></td>";

	print "<td align=left><select size=1 name=txtNAA_LAN_CODE>";
	for($j=0;$j<$rsLaenderZeilen;$j++)
	{
		print "<option ";
		print ($rsNachlaesse["NAA_LAN_CODE"][$i]==$rsLaender["LAN_CODE"][$j]?"selected":"") . " value=" . $rsLaender["LAN_CODE"][$j] . ">" . $rsLaender["LAN_CODE"][$j] . "</option>";
	}
	print "</select>";

	print "<td align=left><input size=30 name=txtNAA_BEZ value='". $rsNachlaesse["NAA_BEZ"][$i] ."'></td>";
	print "<td align=left><input size=40 name=txtNAA_BEMERKUNG value='". $rsNachlaesse["NAA_BEMERKUNG"][$i] ."'></td>";

	print "<td align=left><select size=1 name=txtNAA_AKTIV value=1>";
	print "<option value=0>Nein</option>";
	print "<option selected value=1>Ja</option>";		
	print "</select>";

	print "<td><input type=text size=5 name=txtNAA_RABATTPROZ value=". $rsNachlaesse["NAA_RABATTPROZ"][$i] ."></td>";
	print "<td><input type=text size=6 name=txtNAA_RABATTBETRAG value=". $rsNachlaesse["NAA_RABATTBETRAG"][$i] ."></td>";

	print "<td><input type=text size=10 name=txtNAA_START value=". $rsNachlaesse["NAA_START"][$i] ."></td>";
	print "<td><input type=text size=10 name=txtNAA_ENDE value=". $rsNachlaesse["NAA_ENDE"][$i] ."></td>";
	print "<td><input type=text size=20 name=txtNAA_BONTEXT value=". $rsNachlaesse["NAA_BONTEXT"][$i] ."></td>";
	print "<td align=left><select size=1 name=txtNAA_BEGRUENDUNG>";
	print "<option value=0>Nein</option>";
	print "<option selected value=1>Ja</option>";		
	print "</select>";

	print "<td><input type=text size=5 name=txtNAA_GRUPPIERUNG value='". $rsNachlaesse["NAA_GRUPPIERUNG"][$i] ."'></td>";
	print "<td><input type=text size=13 name=txtNAA_EAN value='". $rsNachlaesse["NAA_EAN"][$i] ."'></td></tr>";

}

/****************************************************************
* Vorhandene Daten �ndern
****************************************************************/

//if(!isset($HTTP_GET_VARS["Edit"]) AND !isset($HTTP_GET_VARS["Kopieren"]))		// Kein Bearbeitungsmodus -> neu hinzuf�gen

if(!$Bearbeiten AND awis_NameInArray($HTTP_POST_VARS,"cmdKopieren")=='')		// Kein Bearbeitungsmodus -> neu hinzuf�gen
{
        print "<td><input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=submit();></td>";

		print "<td><input type=hidden name=txtNAA_KEY value=>";
		print "<input tabindex=0 size=3 name=txtNAA_ID value=></td>";
		print "<td align=center><input size=1 name=txtNAA_MODUS value=></td>";

		print "<td align=left><select size=1 name=txtNAA_LAN_CODE>";
		for($j=0;$j<$rsLaenderZeilen;$j++)
		{
			print "<option ";
			print ($rsNachlaesse["NAA_LAN_CODE"][$i]==$rsLaender["LAN_CODE"][$j]?"selected":"") . " value=" . $rsLaender["LAN_CODE"][$j] . ">" . $rsLaender["LAN_CODE"][$j] . "</option>";
		}
		print "</select>";

		print "<td align=left><input size=30 name=txtNAA_BEZ value=></td>";
		print "<td align=left><input size=40 name=txtNAA_BEMERKUNG value=></td>";

		print "<td align=left><select size=1 name=txtNAA_AKTIV>";
		print "<option value=0>Nein</option>";
		print "<option selected value=1>Ja</option>";		
		print "</select>";

		print "<td><input type=text size=5 name=txtNAA_RABATTPROZ value=></td>";
		print "<td><input type=text size=6 name=txtNAA_RABATTBETRAG value=></td>";

		print "<td><input type=text size=10 name=txtNAA_START value=></td>";
		print "<td><input type=text size=10 name=txtNAA_ENDE value=></td>";
		print "<td><input type=text size=20 name=txtNAA_BONTEXT value=></td>";

		print "<td align=left><select size=1 name=txtNAA_BEGRUENDUNG>";
		print "<option value=0>Nein</option>";
		print "<option selected value=1>Ja</option>";		
		print "</select>";

		print "<td><input type=text size=5 name=txtNAA_GRUPPIERUNG value=></td>";
		print "<td><input type=text size=13 name=txtNAA_EAN value=></td>";

}

/***********************************************************************************************
* 
* Liste aufbauen
* 
************************************************************************************************/
for($i=0;$i<$rsNachlaesseZeilen;$i++)
{
	if($rsNachlaesse["NAA_KEY"][$i] != $BearbeitenKey)
	{
		if($rsNachlaesse['NAA_AKTIV'][$i]==0 AND ($HTTP_POST_VARS['cmdAlleAnzeigen_x']=='' AND $HTTP_GET_VARS['cmdAlleAnzeigen_x']==''))
		{
			echo "<tr><td colspan=999><hr></td></tr><tr><td colspan=999><input type=image alt=\"Alle anzeigen\"src=/bilder/filter.png name=cmdAlleAnzeigen value=\"Alle anzeigen\"></td></tr>";
			break;
		}

		print "<tr>";
		if($Bearbeiten)
		{
			print "<td>&nbsp;</td>";
			print "<td>" . $rsNachlaesse["NAA_ID"][$i] . "</td>";
		}
		else
		{
	        print "<td><input type=image accesskey=C alt='Kopieren (Alt+C)' src=/bilder/kopieren.png name=cmdKopieren_" . $rsNachlaesse["NAA_KEY"][$i] . " onclick=submit();></td>";
			print "<td><a href=./nachlaesse_Main.php?cmdAktion=Nachlassarten&Edit=" . $rsNachlaesse["NAA_KEY"][$i] . "&cmdAlleAnzeigen_x=" . $HTTP_POST_VARS['cmdAlleAnzeigen_x'] . "#Bearbeitung>" . $rsNachlaesse["NAA_ID"][$i] . "</td>";
		}
		print "<td align=center>" . $rsNachlaesse["NAA_MODUS"][$i] . "</td>";

		print "<td>" . $rsNachlaesse["NAA_LAN_CODE"][$i] . "</td>";
		print "<td>" . $rsNachlaesse["NAA_BEZ"][$i] . "</td>";
		print "<td>" . $rsNachlaesse["NAA_BEMERKUNG"][$i] . "</td>";
		print "<td>" . ($rsNachlaesse["NAA_AKTIV"][$i]==1?"Ja":"Nein") . "</td>";

		print "<td>" . $rsNachlaesse["NAA_RABATTPROZ"][$i] . "</td>";
		print "<td>" . $rsNachlaesse["NAA_RABATTBETRAG"][$i] . "</td>";
		print "<td>" . $rsNachlaesse["NAA_START"][$i] . "</td>";
		print "<td>" . $rsNachlaesse["NAA_ENDE"][$i] . "</td>";
		print "<td>" . $rsNachlaesse["NAA_BONTEXT"][$i] . "</td>";

		print "<td>" . ($rsNachlaesse["NAA_BEGRUENDUNG"][$i]!=0?"Ja":"Nein") . "</td>";
		print "<td>" . $rsNachlaesse["NAA_GRUPPIERUNG"][$i] . "</td>";
		print "<td>" . $rsNachlaesse["NAA_EAN"][$i] . "</td>";
	
		print "</tr>";
	}
	else
	{
		print "<tr><td colspan=99><a name=Bearbeitung><hr></a></td></tr>";
		SchreibeUeberschrift($FeldListe);
		print "<tr>";

        print "<td><input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=submit();></td>";

		print "<td><input type=hidden name=txtNAA_KEY value=" . $rsNachlaesse["NAA_KEY"][$i] . ">";
		print "<input tabindex=0 size=3 name=txtNAA_ID value='" . $rsNachlaesse["NAA_ID"][$i] . "'></td>";
		print "<td align=center><input size=1 name=txtNAA_MODUS value='" . $rsNachlaesse["NAA_MODUS"][$i] . "'></td>";

		print "<td align=left><select size=1 name=txtNAA_LAN_CODE>";
		for($j=0;$j<$rsLaenderZeilen;$j++)
		{
			print "<option ";
			print ($rsNachlaesse["NAA_LAN_CODE"][$i]==$rsLaender["LAN_CODE"][$j]?"selected":"") . " value=" . $rsLaender["LAN_CODE"][$j] . ">" . $rsLaender["LAN_CODE"][$j] . "</option>";
		}
		print "</select>";

		print "<td align=left><input size=30 name=txtNAA_BEZ value='" . $rsNachlaesse["NAA_BEZ"][$i] . "'></td>";
		print "<td align=left><input size=40 name=txtNAA_BEMERKUNG value='" . $rsNachlaesse["NAA_BEMERKUNG"][$i] . "'></td>";

		print "<td align=left><select size=1 name=txtNAA_AKTIV >";
		print "<option " . ($rsNachlaesse["NAA_AKTIV"][$i]==0?"selected":"") . " value=0>Nein</option>";
		print "<option " . ($rsNachlaesse["NAA_AKTIV"][$i]!=0?"selected":"") . " value=1>Ja</option>";		
		print "</select>";

		print "<td><input type=text size=20 name=txtNAA_RABATTPROZ value='" . $rsNachlaesse["NAA_RABATTPROZ"][$i] . "'></td>";
		print "<td><input type=text size=20 name=txtNAA_RABATTBETRAG value='" . $rsNachlaesse["NAA_RABATTBETRAG"][$i] . "'></td>";

		print "<td><input type=text size=10 name=txtNAA_START value='" . $rsNachlaesse["NAA_START"][$i] . "'></td>";
		print "<td><input type=text size=10 name=txtNAA_ENDE value='" . $rsNachlaesse["NAA_ENDE"][$i] . "'></td>";

		print "<td><input type=text size=20 name=txtNAA_BONTEXT value='" . $rsNachlaesse["NAA_BONTEXT"][$i] . "'></td>";

		print "<td align=left><select size=1 name=txtNAA_BEGRUENDUNG>";
		print "<option " . ($rsNachlaesse["NAA_BEGRUENDUNG"][$i]==0?"Selected":"") . " value=0>Nein</option>";
		print "<option " . ($rsNachlaesse["NAA_BEGRUENDUNG"][$i]!=0?"Selected":"") . " value=1>Ja</option>";
		print "<option selected value=1>Ja</option>";		
		print "</select>";

		print "<td><input type=text size=5 name=txtNAA_GRUPPIERUNG value='" . $rsNachlaesse["NAA_GRUPPIERUNG"][$i] . "'></td>";
		print "<td><input type=text size=13 name=txtNAA_EAN value='" . $rsNachlaesse["NAA_EAN"][$i] . "'></td>";
		
		print "</tr>";
	}
}
	
	
print "</table>";

print "</form>";

/****************************************************************************************************************/

Function SchreibeUeberschrift($FeldListe)
{
	print "<tr>";
	print "<td><span class=DatenFeldNormalFett>Aktion</span></td>";	
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_ID"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_MODUS"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_LAN_CODE"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_BEZ"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_BEMERKUNG"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_AKTIV"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_RABATTPROZ"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_RABATTBETRAG"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_START"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_ENDE"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_BONTEXT"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_BEGRUENDUNG"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_GRUPPIERUNG"][0] . "</span></td>";
	print "<td><span class=DatenFeldNormalFett>" . $FeldListe["NAA_EAN"][0] . "</span></td>";
	print "</tr>";
}


?>

