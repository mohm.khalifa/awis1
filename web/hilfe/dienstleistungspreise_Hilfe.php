<html>
<head>
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<?php
require_once("register.inc.php");
require_once("db.inc.php");
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";
?>

<title>Dienstleistungspreise - Hilfe</title>
</head>

<body>

<h2>AWIS - Dienstleistungspreise</h2>

<?php
$con = awisLogon();

if(awisBenutzerRecht($con,152)>0)
{
echo '<hr>';
echo 'In dieser Maske k�nnen die Dienstleistungspreise abgefragt werden.<br>';
echo 'Die Maske ist in zwei Teile aufgeteilt. Im oberen Bereich k�nnen die Suchparameter festgelegt werden.';
echo ' Dabei kann ausgw�hlt werden, welche Artikel angezeigt werden sollen. Bei der Auswahl �sterreich, werden';
echo ' alle Artikel/Preise angezeigt, deren Nummer mit einem \'O\' beginnt, bei Deutschen entsprechend alle';
echo ' Artikel, die nicht mit \'O\' beginnen.<br>';
echo 'Neben der Landesauswahl kann die Liste noch weiter durch die Eingabe eines Suchbegriffs eingeschr�nkt werden.<br>';
echo 'Alle Einstellungen k�nnen f�r den n�chsten Aufruf gespeichert werden.<br><br>';
echo 'Im unteren Teil der Maske werden die Daten angezeigt.';
echo ' Wenn bei den Preisen 0,00 hinterlegt ist, kann dies zum einen bedeuten, dass die Dienstleistung nach Aufwand';
echo ' berechnet wird, zum anderen kann sie auch tats�chlich kostenlos sein. Aus diesem Grund wird in diesem Fall';
echo ' statt einem Preis ein \'--\' angezeigt.';

}
awislogoff($con);

?>

</body>
</html>
