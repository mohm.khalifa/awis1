<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_WertBereitsVorhanden');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_keinPersonal');
$TextKonserven[]=array('Fehler','err_keinegueltigeATUNR');
$TextKonserven[]=array('Fehler','err_ATUNRFalschesFormat');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');
$TextKonserven[]=array('RST','*');
$TextKonserven[]=array('REH','*');
$TextKonserven[]=array('RRP','*');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_zurueck');


try 
{
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();	
	$TXT_Speichern = $Form->LadeTexte($TextKonserven);	
	
	//***********************************************
	// Reifenstamm
	//***********************************************
	$Form->DebugAusgabe(1,'Speichern:');
	$Form->DebugAusgabe(1,$_POST);

	if(isset($_POST['txtRST_KEY']))
	{
		if($_POST['txtRST_KEY']=='')
		{
			$AWIS_KEY1=-1;
		}
		else
		{
			$AWIS_KEY1=$_POST['txtRST_KEY'];
		}
		
		$Felder = $Form->NameInArray($_POST, 'txtRST_',1,1);
					
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			
			if ($AWIS_KEY1==-1) //Neuer Reifen
			{
				$TextKonserven[]=array('RST','RST_%');
				$TXT_Speichern = $Form->LadeTexte($TextKonserven);
				
				//Daten auf Vollst�ndigkeit pr�fen
				$Fehler = '';			
				$Pflichtfelder = array('RST_REH_KEY','RST_LIE_NR','RST_LARTNR','RST_EAN');
				foreach($Pflichtfelder AS $Pflichtfeld)
				{
					if($_POST['txt'.$Pflichtfeld]=='')	// Filiale muss angegeben werden
					{
						$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RST'][$Pflichtfeld].'<br>';
					}
				}
			
				// Wurden Fehler entdeckt? => Speichern abbrechen
				if($Fehler!='')
				{
					$Form->ZeileStart();
					$Form->Hinweistext($Fehler);
					$Form->ZeileEnde();
	
					$Link = './reifenstamm_Main.php?cmdAktion=Details&RST_KEY='.$AWIS_KEY1;
					$Form->SchaltflaechenStart();
					$Form->Schaltflaeche('href', 'cmdReifenstamm', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
					$Form->SchaltflaechenEnde();
					
					die();					
				}
			
				$Speichern = true;
				
				if(isset($_POST['bestaetigung']))
				{
					$Speichern=isset($_POST['cmdSpeichern_x']);
				}
				else
				{
					$SQL = 'SELECT *';
					$SQL .= ' FROM REIFENSTAMM';
					$SQL .= ' INNER JOIN REIFENHERSTELLER ON REH_KEY = RST_REH_KEY';
					$SQL .= ' WHERE (RST_LIE_NR = :var_T_RST_LIE_NR ';
					$SQL .= ' AND RST_LARTNR = :var_T_RST_LARTNR) ';
					$SQL .= ' OR RST_EAN = :var_T_RST_EAN';
					$BindeVariablen = array();
					$BindeVariablen['var_T_RST_LIE_NR']= $_POST['txtRST_LIE_NR'];
					$BindeVariablen['var_T_RST_LARTNR']= $_POST['txtRST_LARTNR'];
					$BindeVariablen['var_T_RST_EAN']= $_POST['txtRST_EAN'];
					$rsRST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());

					if (!$rsRST->EOF())
					{
						//Reifen bereits vorhanden, Warnung ausgeben
						echo '<form name=frmReifenstamm method=post action=./reifenstamm_Main.php?cmdAktion=Details&RST_KEY='.$AWIS_KEY1.'>';
							
						$Form->Formular_Start();

						$Form->ZeileStart();
						$Link = '';
	
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['REH']['REH_BEZEICHNUNG'], 120, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['RST_LIE_NR'], 70, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['RST_LARTNR'], 140, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['txt_Reifenkennzeichen'],283, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['RST_BEZEICHNUNG'],280, '', $Link);
						
						$Form->ZeileEnde();
							
						$DS = 0;
						while (!$rsRST->EOF())
						{
							$Link = './reifenstamm_Main.php?cmdAktion=Details&RST_KEY=' . $rsRST->FeldInhalt('RST_KEY') . '';

							$Form->ZeileStart();
	
				            $Form->Erstelle_ListenFeld('REH_BEZEICHNUNG', $rsRST->FeldInhalt('REH_BEZEICHNUNG'), 0, 120, false, ($DS % 2), '', '', 'T');
				            $Form->Erstelle_ListenFeld('RST_LIE_NR', $rsRST->FeldInhalt('RST_LIE_NR'), 0, 70, false, ($DS % 2), '', '', 'T');
				            $Form->Erstelle_ListenFeld('RST_LARTNR', $rsRST->FeldInhalt('RST_LARTNR'), 0, 140, false, ($DS % 2), '', $Link, 'T');
				            $Form->Erstelle_ListenFeld('RST_BREITE', $rsRST->FeldInhalt('RST_BREITE'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
				            $Form->Erstelle_ListenFeld('RST_QUERSCHNITT', $rsRST->FeldInhalt('RST_QUERSCHNITT'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
				            $Form->Erstelle_ListenFeld('RST_BAUART', $rsRST->FeldInhalt('RST_BAUART'), 0, 30, false, ($DS % 2), '', '', 'T', 'L', '');
				            $Form->Erstelle_ListenFeld('RST_INNENDURCHMESSER', $rsRST->FeldInhalt('RST_INNENDURCHMESSER'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
				            $Form->Erstelle_ListenFeld('RST_LOADINDEX', $rsRST->FeldInhalt('RST_LOADINDEX'), 0, 30, false, ($DS % 2), '', '', 'T', '', '');
				            $Form->Erstelle_ListenFeld('RST_SPEEDINDEX', $rsRST->FeldInhalt('RST_SPEEDINDEX'), 0, 30, false, ($DS % 2), '', '', 'T', '', '');
							
				            $Zusatz = '';
				            if ($rsRST->FeldInhalt('RST_RF')!='')
				            {
				            	$Zusatz  = $rsRST->FeldInhalt('RST_RF');
				            	if ($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
				            	{
				            		$Zusatz .= '/'.$rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG');
				            	}
				            }
				            elseif ($rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG')!= '')
				            {
				            	$Zusatz = $rsRST->FeldInhalt('RST_ZUSATZBEMERKUNG');
				            }
				            
				            $Form->Erstelle_ListenFeld('ZUSATZ',$Zusatz, 0, 80, false, ($DS % 2), '', '', 'T', '', '');
				            
				            $Form->Erstelle_ListenFeld('RST_BEZEICHNUNG', $rsRST->FeldInhalt('RST_BEZEICHNUNG'), 0, 280, false, ($DS % 2), '', '', 'T', '', '');
				            if($rsRST->FeldInhalt('RST_BEMERKUNG')!='')
				            {
				            	$Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsRST->FeldInhalt('RST_BEMERKUNG'));
				            }
												
							$Form->ZeileEnde();
						
							$rsRST->DSWeiter();
							$DS++;
						}
							
						$Form->Trennzeile('O');
							
						$Form->ZeileStart();
						$Form->Hinweistext($TXT_Speichern['RST']['err_ReifenMehrfachVorhanden']);
						$Form->ZeileEnde();
	
						$Form->Formular_Ende();
							
						$Form->SchaltflaechenStart();
						$Form->Schaltflaeche('image','cmdDSNeu','','/bilder/cmd_zurueck.png',$TXT_Speichern['Wort']['lbl_zurueck'],'O');
						$Form->Schaltflaeche('image','cmdSpeichern','','/bilder/cmd_speichern.png',$TXT_Speichern['Wort']['lbl_speichern'],'S');
						$Form->SchaltflaechenEnde();
							
						// Alle Postwerte speichern
						$Felder = explode(';',$Form->NameInArray($_POST,'txt',1,0));
						foreach ($Felder AS $Feld)
						{
							echo '<input name="'.$Feld.'" value="'.$_POST[$Feld].'" type="hidden">';
						}
						echo '<input name="bestaetigung" value="1" type="hidden">';
						echo '</form>';
							
						die();
					}
				}

				if($Speichern)
				{
					$SQL = 'INSERT INTO REIFENSTAMM (';
					$SQL .= ' RST_REH_KEY,';
					$SQL .= ' RST_LIE_NR,';
					$SQL .= ' RST_LARTNR,';
					$SQL .= ' RST_EAN,';
					$SQL .= ' RST_CREADAT,';
					$SQL .= ' RST_IMPORTSTATUS,';
					$SQL .= ' RST_STATUS,';
					$SQL .= ' RST_BEMERKUNG,';
					$SQL .= ' RST_USER,';
					$SQL .= ' RST_USERDAT';
					$SQL .= ' ) VALUES (';
					$SQL .= '  ' .$DB->FeldInhaltFormat('Z',$_POST['txtRST_REH_KEY'],true);
					$SQL .= ' ,' .$DB->FeldInhaltFormat('T',$_POST['txtRST_LIE_NR'],true);
					$SQL .= ' ,' .$DB->FeldInhaltFormat('T',$_POST['txtRST_LARTNR'],true);
					$SQL .= ' ,' .$DB->FeldInhaltFormat('T',$_POST['txtRST_EAN'],true);
					$SQL .= ' , SYSDATE';
					$SQL .= ' ,10';
					$SQL .= ' ,10';
					$SQL .= ' ,' .$DB->FeldInhaltFormat('T',$_POST['txtRST_BEMERKUNG'],true);
					$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', SYSDATE)';
		
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
					
					$SQL = 'SELECT seq_RST_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY1=$rsKey->FeldInhalt('KEY');
				}
			}
			else // ge�nderter Datensatz
			{
				$FeldListe='';
				$SQL = '';
				$FehlerListe = array();
				$UpdateFelder = '';
				
				$rsRST = $DB->RecordSetOeffnen('SELECT * FROM REIFENSTAMM WHERE RST_key=' . $_POST['txtRST_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsRST->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsRST->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsRST->FeldInfo($FeldName,'TypKZ'),$rsRST->FeldInhalt($FeldName),true);

						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';															
		
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
				
				
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsRST->FeldInhalt('RST_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';						
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE REIFENSTAMM SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', RST_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', RST_userdat=sysdate';
					$SQL .= ' WHERE RST_key=0' . $_POST['txtRST_KEY'] . '';
					
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
				}
			}
		}
	}

	//*************************************************
	// Reifenpflege
	//*************************************************
	if(isset($_POST['txtRRP_KEY']))
	{
		$AWIS_KEY1=isset($_POST['txtRRP_RST_KEY'])?$_POST['txtRRP_RST_KEY']:-1;
		$AWIS_KEY2=-1;

		if($_POST['txtRRP_KEY']=='')
		{
			if (isset($_POST['txtRRP_ATUNR']) AND $_POST['txtRRP_ATUNR']!='')
			{
				$_POST['txtRRP_ATUNR'] = strtoupper($_POST['txtRRP_ATUNR']);
				
				$SQL  = 'SELECT RRP_KEY ';
				$SQL .= ' FROM REIFENPFLEGE';
				$SQL .= ' WHERE RRP_ATUNR = :var_T_RRP_ATUNR';
				$SQL .= ' AND RRP_RST_KEY IS NULL';
				$DB->SetzeBindevariable('RRP','var_T_RRP_ATUNR',$_POST['txtRRP_ATUNR'],awisDatenbank::VAR_TYP_TEXT);
				$rsRRP = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RRP'));
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				if (!$rsRRP->EOF())
				{
					$AWIS_KEY2=floatval($rsRRP->FeldInhalt('RRP_KEY'));
				}
			}
		}
		else
		{
			$AWIS_KEY2=$_POST['txtRRP_KEY'];
		}
		
		$Felder = $Form->NameInArray($_POST, 'txtRRP_',1,1);

		$Form->DebugAusgabe(1,$AWIS_KEY1);
		$Form->DebugAusgabe(1,$AWIS_KEY2);
		
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			
			//Daten auf Vollst�ndigkeit pr�fen
			$Fehler = '';
			if(!isset($_POST['txtRRP_RST_KEY']))
			{
				$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RRP']['RRP_RST_KEY'].'<br>';
			}
				
			$Pflichtfelder = array('RRP_RST_KEY','RRP_ATUNR','RRP_GUELTIGAB');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['RRP'][$Pflichtfeld].'<br>';
				}
			}
			
			//Pr�fung, ob ATUNR korrekte L�nge
			if ($Fehler=='')
			{
				if (strlen($_POST['txtRRP_ATUNR'])!=6)
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_ATUNRFalschesFormat'].' '.$_POST['txtRRP_ATUNR'].'<br>';
				}
			}
				
			//Pr�fung, ob ATUNR zum Hersteller passt
			if ($Fehler=='')
			{
				$SQL = 'SELECT';
				$SQL .= ' REH_ATUNRPRAEFIX,';
				$SQL .= ' LENGTH(REH_ATUNRPRAEFIX) AS LEN,';
				$SQL .= ' REH_SONDERBESTNR';
				$SQL .= ' FROM V_REIFENHERSTELLER';
				$SQL .= ' LEFT JOIN REIFENSTAMM ON RST_REH_KEY=REH_KEY';
				$SQL .= ' WHERE RST_KEY=0'.$DB->FeldInhaltFormat('Z',$AWIS_KEY1);
				$SQL .= ' ORDER BY 2 DESC';
				$rsREH = $DB->RecordSetOeffnen($SQL);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
			
				$ATUNRSonderbestellnr='';
				if (!$rsREH->EOF())
				{
					$PraefixFehler = $TXT_Speichern['RRP']['err_ArtikelpraefixFalsch'].' '.$_POST['txtRRP_ATUNR'].'<br>';
			
					while (!$rsREH->EOF())
					{
						if ($rsREH->FeldInhalt('REH_SONDERBESTNR')==$_POST['txtRRP_ATUNR'])
						{
							$ATUNRSonderbestellnr=$_POST['txtRRP_ATUNR'];
						}
						if ($rsREH->FeldInhalt('REH_ATUNRPRAEFIX')==substr($_POST['txtRRP_ATUNR'],0,$rsREH->FeldInhalt('LEN')))
						{
							$PraefixFehler = '';
						}
						$rsREH->DSWeiter();
					}
				}
					
				$Fehler .= (isset($PraefixFehler)?$PraefixFehler:'');
			}

			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				$Form->ZeileStart();
				$Form->Hinweistext($Fehler);
				$Form->ZeileEnde();
			
				$Link = './reifenstamm_Main.php?cmdAktion=Details&Seite=Artikel&RST_KEY='.$AWIS_KEY1.'&RRP_KEY='.$AWIS_KEY2;
				$Form->SchaltflaechenStart();
				$Form->Schaltflaeche('href', 'cmdDSNeu', $Link, '/bilder/cmd_weiter.png', $TXT_Speichern['Wort']['lbl_weiter'], 'W');
				$Form->SchaltflaechenEnde();
			
				die();
			}
				
			if ($AWIS_KEY2==-1) //Neuer Reifen
			{
				$Speichern = true;
			
				if(isset($_POST['bestaetigung']))
				{
					$Speichern=isset($_POST['cmdSpeichern_x']);
				}
				elseif ($_POST['txtRRP_ATUNR']!=$ATUNRSonderbestellnr)
				{
					$SQL  = 'SELECT *';
					$SQL .= ' FROM REIFENPFLEGE';
					$SQL .= ' WHERE';
					$SQL .= ' (RRP_ATUNR='.$DB->FeldInhaltFormat('T',$_POST['txtRRP_ATUNR']);
					$SQL .= ' AND RRP_RST_KEY IS NOT NULL)';
					$SQL .= ' OR RRP_RST_KEY=0'.$DB->FeldInhaltFormat('Z',$AWIS_KEY1);
					$rsRRP = $DB->RecordSetOeffnen($SQL);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
			
					if (!$rsRRP->EOF()) //Artikel bereits vorhanden
					{
						echo '<form name=frmReifenstamm method=post action=./reifenstamm_Main.php?cmdAktion=Details&Seite=Artikel&RST_KEY='.$AWIS_KEY1.'&RRP_KEY='.$AWIS_KEY2.'>';
			
						$Form->Formular_Start();
							
            			$Link = './reifenpflege_Main.php?cmdAktion=Details&RRP_KEY=' . $rsRRP->FeldInhalt('RRP_KEY') . '';
						$Form->ZeileStart();
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_ATUNR'], 80, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_BEZEICHNUNGWW'], 280, '');
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_KENN1_kurz'], 60, '');
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_KENN2_kurz'], 60);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_SUCH1_kurz'], 90, '');
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_SUCH2'], 90, '');
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_MEHRFACHKENNUNG_kurz'], 60, '');
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_VK'], 100, '', '', '', 'R');
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_GUELTIGAB'],100,'');
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_GUELTIGBIS'],100,'');
						$Form->ZeileEnde();
			
						$DS = 0;
							
						$Vorhanden=0;
						while (!$rsRRP->EOF())
						{
							if ($rsRRP->FeldInhalt('RRP_RST_KEY')==$_POST['txtRRP_RST_KEY'])
							{
								$Vorhanden=1;
								$Style = 'font-weight:bold;color:red;';
							}
							else
							{
								$Style = '';
							}
							$Form->ZeileStart();
							$Form->Erstelle_ListenFeld('RRP_ATUNR', $rsRRP->FeldInhalt('RRP_ATUNR'), 0, 80, false, ($DS % 2), $Style, $Link, 'T');
							$Form->Erstelle_ListenFeld('RRP_BEZEICHNUNGWW', $rsRRP->FeldInhalt('RRP_BEZEICHNUNGWW'), 0, 300, false, ($DS % 2), $Style, '', '', '', '');
							$Form->Erstelle_ListenFeld('RRP_KENN1', $rsRRP->FeldInhalt('RRP_KENN1'), 0, 50, false, ($DS % 2), $Style, '', '', '', '');
							$Form->Erstelle_ListenFeld('RRP_KENN2', $rsRRP->FeldInhalt('RRP_KENN2').'/'.$rsRRP->FeldInhalt('RRP_KENN_L2'), 0, 50, false, ($DS % 2), $Style, '', '', '', '');
							$Form->Erstelle_ListenFeld('RRP_SUCH1', $rsRRP->FeldInhalt('RRP_SUCH1'), 0, 90, false, ($DS % 2),$Style, '', '', '', '');
							$Form->Erstelle_ListenFeld('RRP_SUCH2', $rsRRP->FeldInhalt('RRP_SUCH2'), 0, 90, false, ($DS % 2),$Style, '', '', '', '');
							$Form->Erstelle_ListenFeld('RRP_MEHRFACHKENNUNG', $rsRRP->FeldInhalt('RRP_MEHRFACHKENNUNG'), 0, 60, false, ($DS % 2),$Style, '', '', '', '');
							$Form->Erstelle_ListenFeld('RRP_VK', $rsRRP->FeldInhalt('RRP_VK'), 0, 100, false, ($DS % 2),$Style, '', 'N2', 'R', '');
							$Form->Erstelle_ListenFeld('RRP_GUELTIGAB',$rsRRP->FeldInhalt('RRP_GUELTIGAB'),0,100,false,($DS%2),$Style,'','D');
							$Form->Erstelle_ListenFeld('RRP_GUELTIGBIS',$rsRRP->FeldInhalt('RRP_GUELTIGBIS'),0,100,false,($DS%2),$Style,'','D');
								
							$Form->ZeileEnde();
								
							$rsRRP->DSWeiter();
							$DS++;
						}
							
						$Form->Trennzeile('O');
							
						$Form->ZeileStart();
						if ($Vorhanden!=1)
						{
							$Form->Hinweistext($TXT_Speichern['RRP']['err_Artikelmehrfach']);
							$Form->ZeileEnde();
			
							$Form->Trennzeile('O');
			
							$Form->ZeileStart();
							$Form->Erstelle_TextLabel($TXT_Speichern['RRP']['RRP_MEHRFACHKENNUNG'] . ':', 150);
							$Daten = explode("|",$TXT_Speichern['RRP']['lst_Primaerartikel']);
							$Form->Erstelle_SelectFeld('RRP_MEHRFACHKENNUNG','', 160, true, '','','D','','',$Daten);
						}
						else
						{
							$Form->Hinweistext($TXT_Speichern['RRP']['err_Artikelvorhanden']);
						}
			
						$Form->ZeileEnde();
						$Form->Formular_Ende();
			
						$Form->SchaltflaechenStart();
						$Form->Schaltflaeche('image','cmdZurueck','','/bilder/cmd_zurueck.png',$TXT_Speichern['Wort']['lbl_zurueck'],'O');
						if ($Vorhanden!=1)
						{
							$Form->Schaltflaeche('image','cmdSpeichern','','/bilder/cmd_speichern.png',$TXT_Speichern['Wort']['lbl_speichern'],'S');
						}
						$Form->SchaltflaechenEnde();
			
						// Alle Postwerte speichern
						$Felder = explode(';',$Form->NameInArray($_POST,'txt',1,0));
						foreach ($Felder AS $Feld)
						{
							echo '<input name="'.$Feld.'" value="'.$_POST[$Feld].'" type="hidden">';
						}
						echo '<input name="bestaetigung" value="1" type="hidden">';
						echo '</form>';
			
						die();
					}
				}
				elseif ($_POST['txtRRP_ATUNR']==$ATUNRSonderbestellnr)
				{
					$SQL  = 'SELECT *';
					$SQL .= ' FROM REIFENPFLEGE';
					$SQL .= ' INNER JOIN REIFENSTAMM ON RST_KEY=RRP_RST_KEY';
					$SQL .= ' INNER JOIN REIFENHERSTELLER ON REH_KEY=RST_REH_KEY';
					$SQL .= ' WHERE';
					$SQL .= ' RRP_ATUNR='.$DB->FeldInhaltFormat('T',$_POST['txtRRP_ATUNR']);
					$SQL .= ' AND RRP_RST_KEY=0'.$DB->FeldInhaltFormat('Z',isset($_POST['txtRRP_RST_KEY'])?$_POST['txtRRP_RST_KEY']:0,true);
					$rsDaten = $DB->RecordSetOeffnen($SQL);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
			
					if (!$rsDaten->EOF()) //Artikel bereits vorhanden
					{
						echo '<form name=frmReifenstamm method=post action=./reifenstamm_Main.php?cmdAktion=Details&&Seite=Artikel&RST_KEY='.$AWIS_KEY1.'&RRP_KEY='.$AWIS_KEY2;
			
						$Form->Formular_Start();
			
						$Form->ZeileStart();
						$Form->Hinweistext($TXT_Speichern['RRP']['err_ArtikelmehrfachUngelistet']);
						$Form->ZeileEnde();
							
						$Form->Trennzeile('O');
							
						$Link = '';

						$Form->ZeileStart();
			
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['REH']['REH_BEZEICHNUNG'], 120, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['RST_LIE_NR'], 70, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['RST_LARTNR'], 140, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RRP']['RRP_ATUNR'], 70, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['txt_Reifenkennzeichen'],200, '', $Link);
						$Form->Erstelle_Liste_Ueberschrift($TXT_Speichern['RST']['RST_BEZEICHNUNG'],280, '', $Link);
						$Form->ZeileEnde();
			
						$DS = 0;
							
						while (!$rsDaten->EOF())
						{
							$Link = '';
							$Form->ZeileStart('font-size:10pt');
							$Form->ZeileStart();
							$Form->Erstelle_ListenFeld('REH_BEZEICHNUNG', $rsDaten->FeldInhalt('REH_BEZEICHNUNG'), 0, 120, false, ($DS % 2), '', '', 'T');
							$Form->Erstelle_ListenFeld('RST_LIE_NR', $rsDaten->FeldInhalt('RST_LIE_NR'), 0, 70, false, ($DS % 2), '', '', 'T');
							$Form->Erstelle_ListenFeld('RST_LARTNR', $rsDaten->FeldInhalt('RST_LARTNR'), 0, 140, false, ($DS % 2), '', $Link, 'T');
							$Form->Erstelle_ListenFeld('RRP_ATUNR', $rsDaten->FeldInhalt('RRP_ATUNR'), 0, 70, false, ($DS % 2), '', '', 'T');
							$Form->Erstelle_ListenFeld('RST_BREITE', $rsDaten->FeldInhalt('RST_BREITE'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
							$Form->Erstelle_ListenFeld('RST_QUERSCHNITT', $rsDaten->FeldInhalt('RST_QUERSCHNITT'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
							$Form->Erstelle_ListenFeld('RST_BAUART', $rsDaten->FeldInhalt('RST_BAUART'), 0, 30, false, ($DS % 2), '', '', 'T', 'L', '');
							$Form->Erstelle_ListenFeld('RST_INNENDURCHMESSER', $rsDaten->FeldInhalt('RST_INNENDURCHMESSER'), 0, 30, false, ($DS % 2), '', '', 'N0', 'L', '');
							$Form->Erstelle_ListenFeld('RST_LOADINDEX', $rsDaten->FeldInhalt('RST_LOADINDEX'), 0, 30, false, ($DS % 2), '', '', 'T', '', '');
							$Form->Erstelle_ListenFeld('RST_SPEEDINDEX', $rsDaten->FeldInhalt('RST_SPEEDINDEX'), 0, 30, false, ($DS % 2), '', '', 'T', '', '');
							$Form->Erstelle_ListenFeld('RST_BEZEICHNUNG', $rsDaten->FeldInhalt('RST_BEZEICHNUNG'), 0, 280, false, ($DS % 2), '', '', 'T', '', '');
							if($rsDaten->FeldInhalt('RST_BEMERKUNG')!='')
							{
								$Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsDaten->FeldInhalt('RST_BEMERKUNG'));
							}
							 
							$Form->ZeileEnde();
								
							$rsDaten->DSWeiter();
							$DS++;
						}
							
						$Form->Trennzeile('O');
							
						$Form->Formular_Ende();
			
						$Form->SchaltflaechenStart();
						$Form->Schaltflaeche('image','cmdDSNeu','','/bilder/cmd_zurueck.png',$TXT_Speichern['Wort']['lbl_zurueck'],'O');
						$Form->SchaltflaechenEnde();
			
						die();
					}
				}
					
				if ($Speichern)
				{
					//Check ob ATU Nummer bereits im AST vorhanden
					unset($rsAST);
					if ($_POST['txtRRP_ATUNR']!=$ATUNRSonderbestellnr)
					{
						// AST vorhanden?
						$SQL = 'SELECT *';
						$SQL .= ' FROM v_reifen_ast';
						$SQL .= ' WHERE AST_ATUNR = :var_T_ATUNR';
						$BindeVariablen = array();
						$BindeVariablen['var_T_ATUNR'] = $_POST['txtRRP_ATUNR'];
						$rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());
						
						if ($rsAST->EOF())
						{
							unset($rsAST);
						}
					}

					unset($rsRRP);
					if (isset($_POST['txtRRP_MEHRFACHKENNUNG']))
					{
						$SQL  = 'SELECT *';
						$SQL .= ' FROM REIFENPFLEGE';
						$SQL .= ' WHERE';
						$SQL .= ' RRP_ATUNR='.$DB->FeldInhaltFormat('T',$_POST['txtRRP_ATUNR']);
						$SQL .= ' AND (RRP_MEHRFACHKENNUNG<>\'D\' OR RRP_MEHRFACHKENNUNG IS NULL)';
						$rsRRP = $DB->RecordSetOeffnen($SQL);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());
						
						if ($rsRRP->EOF())
						{
							unset($rsRRP);
						}
					}
					
					$SQL  = 'INSERT';
					$SQL .= ' INTO REIFENPFLEGE (';
					$SQL .= '  RRP_RST_KEY';
					$SQL .= ' ,RRP_AST_KEY';
					$SQL .= ' ,RRP_ATUNR';
					$SQL .= ' ,RRP_BEZEICHNUNGWW';
					$SQL .= ' ,RRP_ARTBEZK';
					$SQL .= ' ,RRP_WGR_ID';
					$SQL .= ' ,RRP_WUG_ID';
					$SQL .= ' ,RRP_KENN1';
					$SQL .= ' ,RRP_KENN2';
					$SQL .= ' ,RRP_KENN_L2';
					$SQL .= ' ,RRP_SUCH1';
					$SQL .= ' ,RRP_SUCH2';
					$SQL .= ' ,RRP_MEHRFACHKENNUNG';
					$SQL .= ' ,RRP_GUELTIGAB';
					$SQL .= ' ,RRP_GUELTIGBIS';
					$SQL .= ' ,RRP_BESTELLKZ';
					$SQL .= ' ,RRP_USER';
					$SQL .= ' ,RRP_USERDAT';
					$SQL .= ' ) VALUES (';

					if (isset($rsRRP))
					{
						//Mehrfachkennung: Insert mit Kopie der Daten des Prim�rreifens
						$SQL .= '  '.$DB->FeldInhaltFormat('Z',$_POST['txtRRP_RST_KEY']);
						$SQL .= ' ,'.$DB->FeldInhaltFormat('Z',$rsRRP->FeldInhalt('RRP_AST_KEY'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtRRP_ATUNR']);
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_BEZEICHNUNGWW'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_ARTBEZK'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_WGR_ID'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_WUG_ID'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_KENN1'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_KENN2'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_KENN_L2'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_SUCH1'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_SUCH2'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($_POST['txtRRP_MEHRFACHKENNUNG'])?$_POST['txtRRP_MEHRFACHKENNUNG']:'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('D',$_POST['txtRRP_GUELTIGAB']);
						$SQL .= ' ,'.$DB->FeldInhaltFormat('D','31.12.2030');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsRRP->FeldInhalt('RRP_BESTELLKZ'));
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
						$SQL .= ' , SYSDATE)';
					}
					else
					{
						$SQL .= '  '.$DB->FeldInhaltFormat('Z',$_POST['txtRRP_RST_KEY']);
						$SQL .= ' ,'.$DB->FeldInhaltFormat('Z',isset($rsAST)?$rsAST->FeldInhalt('AST_KEY'):'',true);
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['txtRRP_ATUNR']);
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_BEZEICHNUNGWW'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_ARTBEZK'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('WUG_WGR_ID'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('WUG_ID'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_KENNUNG'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_KENN2'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_KENN_L2'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_SUCH1'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_SUCH2'):'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($_POST['txtRRP_MEHRFACHKENNUNG'])?$_POST['txtRRP_MEHRFACHKENNUNG']:'');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('D',$_POST['txtRRP_GUELTIGAB']);
						$SQL .= ' ,'.$DB->FeldInhaltFormat('D','31.12.2030');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T','');
						$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
						$SQL .= ' , SYSDATE)';
					}

					$DB->TransaktionBegin();

					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
			
					$SQL = 'SELECT seq_RRP_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = $DB->RecordSetOeffnen($SQL);
					$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			
					if (isset($_POST['txtRRP_MEHRFACHKENNUNG']))
					{
						//Preise des Prim�rreifens �bernehmen
						$SQL  = 'INSERT INTO REIFENPFLEGEPREISE' ;
						$SQL .= ' (';
						$SQL .= ' RPP_RRP_KEY, RPP_LAN_CODE, RPP_VK, RPP_GUELTIGAB,';
						$SQL .= ' RPP_USER, RPP_USERDAT';
						$SQL .= ' )';
						$SQL .= ' SELECT';
						$SQL .= ' '.$DB->FeldInhaltFormat('Z',$AWIS_KEY1,false);
						$SQL .= ' ,RPP_LAN_CODE, RPP_VK, RPP_GUELTIGAB';
						$SQL .= ' ,RPP_USER, RPP_USERDAT';
						$SQL .= ' FROM REIFENPFLEGEPREISE';
						$SQL .= ' WHERE RPP_RRP_KEY=0'.$DB->FeldInhaltFormat('Z',$rsRRP->FeldInhalt('RRP_KEY'),true);

						$DB->Ausfuehren($SQL,'',true);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());
						
						if ($_POST['txtRRP_MEHRFACHKENNUNG']=='')
						{
							$SQL  = 'UPDATE REIFENPFLEGE';
							$SQL .= ' SET RRP_MEHRFACHKENNUNG=\'D\'';
							$SQL .= ', RRP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
							$SQL .= ', RRP_USERDAT=SYSDATE';
							$SQL .= ' WHERE';
							$SQL .= ' RRP_KEY<>0'.$rsKey->FeldInhalt('KEY');
							$SQL .= ' AND RRP_ATUNR='.$DB->FeldInhaltFormat('T',$_POST['txtRRP_ATUNR'],true);
							$DB->Ausfuehren($SQL,'',true);
							$Form->DebugAusgabe(1,$DB->LetzterSQL());
						}
					}
			
					$DB->TransaktionCommit();
				}
					
			} //end Neuer Reifen
			else //ge�nderter Reifen
			{
		$Form->DebugAusgabe(1,$Felder);
				
				$FehlerListe = array();
				$UpdateFelder = '';
					
				$rsRRP= $DB->RecordSetOeffnen('SELECT * FROM REIFENPFLEGE WHERE RRP_key=' . $AWIS_KEY2 . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
			
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsRRP->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsRRP->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsRRP->FeldInfo($FeldName,'TypKZ'),$rsRRP->FeldInhalt($FeldName),true);
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
			
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
					
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsRRP->FeldInhalt('RRP_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTexte(array(array(substr($Fehler[0],0,3),$Fehler[0])));
						$Meldung .= '<br>&nbsp;'.$FeldName[substr($Fehler[0],0,3)][$Fehler[0]].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE REIFENPFLEGE SET';
					$SQL .= substr($FeldListe,1);
					if (isset($_POST['txtRRP_RST_KEY']))
					{
						$SQL .= ', RRP_RST_KEY='.$DB->FeldInhaltFormat('Z',$_POST['txtRRP_RST_KEY'],true);
					}
					$SQL .= ', RRP_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', RRP_userdat=sysdate';
					$SQL .= ' WHERE RRP_key=0' . $AWIS_KEY2 . '';
			
					$DB->Ausfuehren($SQL,'',false);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
				}
			}//End ge�nderter Reifen
		}
	}
		
	//*************************************************
	// Wiedervorlagen
	//*************************************************
	if(isset($_POST['txtWVL_KEY']))
	{
		$Form->DebugAusgabe(1,'speichern Wiedervorlagen');
	
		$AWIS_KEY1=$_POST['txtWVL_XXX_KEY'];
	
		$Felder = $Form->NameInArray($_POST, 'txtWVL_',1,1);
	
		if($Felder!='')
		{
			$Felder = explode(';',$Felder);
			$TextKonserven[]=array('WVL','WVL_%');
			$TXT_Speichern = $Form->LadeTexte($TextKonserven);
	
			$Fehler = '';
			$Pflichtfelder = array('WVL_DATUM');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if(isset($_POST['txt'.$Pflichtfeld]) AND $_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['WVL'][$Pflichtfeld].'<br>';
				}
			}
	
			// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}
			$FeldListe='';
			$SQL = '';
	
			if(floatval($_POST['txtWVL_KEY'])==0)
			{
				$Fehler = '';
				$SQL = 'INSERT INTO WIEDERVORLAGEN (';
				$SQL .= ' WVL_XTN_KUERZEL,WVL_XXX_KEY,WVL_DATUM,WVL_TEXT,WVL_BEARBEITER';
				$SQL .= ',WVL_TYP,WVL_KENNUNG,WVL_STATUS';
				$SQL .= ',WVL_USER,WVL_USERDAT';
				$SQL .= ')VALUES (';
				$SQL .= " 'RST'";
				$SQL .= ',' . $DB->FeldInhaltFormat('N0',$_POST['txtWVL_XXX_KEY'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('D',$_POST['txtWVL_DATUM'],true);
				$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtWVL_TEXT'],true);
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ",'M'";
				$SQL .= ",''";
				$SQL .= ',1';
				$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
				$SQL .= ',SYSDATE';
				$SQL .= ')';
	
				$DB->Ausfuehren($SQL);
	
				$SQL = 'SELECT seq_WVL_KEY.CurrVal AS KEY FROM DUAL';
				$rsKey = $DB->RecordSetOeffnen($SQL);
				$AWIS_KEY2=$rsKey->FeldInhalt('KEY');
			}
			else 					// ge�nderter Lieferschein
			{
				$FehlerListe = array();
				$UpdateFelder = '';
					
				$rsWVL = $DB->RecordSetOeffnen('SELECT * FROM WIEDERVORLAGEN WHERE WVL_Key=' . $_POST['txtWVL_KEY'] . '');
				$FeldListe = '';
				foreach($Felder AS $Feld)
				{
					$FeldName = substr($Feld,3);
					if(isset($_POST['old'.$FeldName]))
					{
						// Alten und neuen Wert umformatieren!!
						$WertNeu=$DB->FeldInhaltFormat($rsWVL->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
						$WertAlt=$DB->FeldInhaltFormat($rsWVL->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
						$WertDB=$DB->FeldInhaltFormat($rsWVL->FeldInfo($FeldName,'TypKZ'),$rsWVL->FeldInhalt($FeldName),true);
						if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
						{
							if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
							{
								$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
							}
							else
							{
								$FeldListe .= ', '.$FeldName.'=';
	
								if($_POST[$Feld]=='')	// Leere Felder immer als NULL
								{
									$FeldListe.=' null';
								}
								else
								{
									$FeldListe.=$WertNeu;
								}
							}
						}
					}
				}
	
				if(count($FehlerListe)>0)
				{
					$Meldung = str_replace('%1',$rsWVL->FeldInhalt('WVL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
					foreach($FehlerListe AS $Fehler)
					{
						$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
						$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
					}
					$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
				}
				elseif($FeldListe!='')
				{
					$SQL = 'UPDATE WIEDERVORLAGEN SET';
					$SQL .= substr($FeldListe,1);
					$SQL .= ', WVL_user=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', WVL_userdat=sysdate';
					$SQL .= ' WHERE WVL_KEY=0' . $_POST['txtWVL_KEY'] . '';
					$DB->Ausfuehren($SQL);
				}
			}
		}
	}
	

}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>