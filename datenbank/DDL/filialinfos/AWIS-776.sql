INSERT INTO tabellenkuerzel (
    xtn_kuerzel,
    xtn_tabellenname,
    xtn_datenquelle,
    xtn_version
) VALUES (
    'FIK',
    'Filialkompetenzentypen',
    'ORACLE',
    1
);

INSERT INTO rechte ( xrc_id,xrc_recht ) VALUES (5020, 'Filialinformationen: Kompetenzen');

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
  5020,
  0,
  'Filialkonpetenzen-Reiter: Anzeigen'
);

INSERT INTO rechtestufen ( xrs_xrc_id,xrs_bit,xrs_beschreibung ) VALUES (
  5020,
  1,
  'Filialkonpetenzen-Reiter: Bearbeiten (Nur Fachabteilung)'
);

Commit;


INSERT INTO filialinfostypen2 (
  fit_id,
  fit_information,
  fit_ftg_id,
  fit_xrc_id,
  fit_stufeanzeige,
  fit_stufebearbeiten,
  fit_status,
  fit_sortierung,
  fit_format,
  fit_datenquelle,
  fit_zeichen,
  fit_breite,
  fit_suchfeld,
  fit_readonly,
  fit_bemerkung,
  fit_user,
  fit_userdat,
  fit_labelbreite,
  fit_label,
  fit_tooltipp,
  fit_gruppe,
  fit_export,
  fit_protokoll,
  fit_labeldoppelpunkt
) VALUES (
  920,
  'Filialkompetenzen',
  10,
  5020,
  0,
  1,
  'A',
  140,
  'T',
  'FKT:SQLAusfuehren#~#SELECT FUNC_GL_WL($$FIL_ID~N0$$,920) as Anzeige FROM Dual',
  0,
  0,
  0,
  0,
  'Reiter, f�r die Kompetenzen der Filiale',
  'AWIS',
  sysdate,
  200,
  1,
  1,
  1,
  0,
  0,
  1
  );

commit;

CREATE TABLE FILIALKOMPETENZENTYPEN
(
    FIK_KEY NUMBER NOT NULL
  , FIK_BEZEICHNUNG VARCHAR2(200) NOT NULL
  , FIK_BESCHREIBUNG VARCHAR2(200)
  , FIK_USER VARCHAR2(20)
  , FIK_USERDAT DATE
  , CONSTRAINT FILIALKOMPETENZENTYPEN_PK PRIMARY KEY
  (
    FIK_KEY
  )
ENABLE
);

COMMENT ON COLUMN FILIALKOMPETENZENTYPEN.FIK_KEY IS 'KEY';
COMMENT ON COLUMN FILIALKOMPETENZENTYPEN.FIK_BEZEICHNUNG IS 'Angezeigter Name der Kompetenz';
COMMENT ON COLUMN FILIALKOMPETENZENTYPEN.FIK_BESCHREIBUNG IS 'Beschreibung f�r die Fachabteilung';

CREATE SEQUENCE SEQ_FIK_KEY;

CREATE TRIGGER TRG_SEQ_FIK_KEY
BEFORE INSERT ON FILIALKOMPETENZENTYPEN
FOR EACH ROW
  BEGIN
    <<COLUMN_SEQUENCES>>
    BEGIN
      IF INSERTING AND :NEW.FIK_KEY IS NULL THEN
        SELECT SEQ_FIK_KEY.NEXTVAL INTO :NEW.FIK_KEY FROM SYS.DUAL;
      END IF;
    END COLUMN_SEQUENCES;
  END;
/

commit;


