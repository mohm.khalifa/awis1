<?php
require_once 'awisDatenbank.inc';
require_once 'awisFormular.inc';

if(!isset($_GET['sucBES_LIEFERANT']))
{
	var_dump($_GET);
	die();
}


try
{
	$DS=0;
	
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISBenutzer = awisBenutzer::Init();
	$Form = new awisFormular();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','Datum');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	
	if(isset($_GET['sucBES_LIEFERANT']))
	{
		if(ctype_digit($_GET['sucBES_LIEFERANT']))
		{
			$SQL  = ' Select * from LIEFERANTEN WHERE LIE_NR=\''.$_GET['sucBES_LIEFERANT'].'\'';
			
			$rsLIE = $DB->RecordSetOeffnen($SQL);
		
				
			$Fehler = '';
			if($rsLIE->AnzahlDatensaetze() == 0)
			{
				
				$Fehler = 'kein g�ltiger Lieferant';
			}
		}
		else 
		{
			$Fehler = 'kein g�ltiger Lieferant';
		}
		if($_GET['sucBES_LIEFERANT'] == '')
		{
			$Fehler =  $AWISSprachKonserven['BES']['MSG_LIEEINGABE'];
		}
		
		
			
		
		
		
		/**
		 * Standort nur lesen!
		 */
		
		$Form->Erstelle_TextFeld('BES_LIENAME',$Fehler == '' ? $rsLIE->FeldInhalt('LIE_NAME1'):$Fehler, 200,423, false, '', '', '', 'T', 'L','','',50);

	}	

}
catch (Exception $ex)
{
	$Form->Hinweistext('Fehler beim Abrufen der Daten:'.$ex->getMessage());
}
?>