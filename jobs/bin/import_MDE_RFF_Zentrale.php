<?php

require_once('awisBenutzer.inc');
require_once('awisMDE.php');
require_once('awisINFO.php');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;

    for($i=1;$i<$argc;$i++)
    {
        switch (strtolower($argv[$i]))
        {
            case '--benutzer':
                $Benutzer = $argv[++$i];
                break;
            case '--debuglevel':
                $DebugLevel = $argv[++$i];
                break;
            default:
                echo 'Parameter '.$argv[$i].' ist unbekannt.';
            case '--help':
                echo $argv[0].PHP_EOL;
                echo 'Optionen:'.PHP_EOL;
                echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                echo '  --methode  [Methodetyp]            [--Scheduler = alles | --LeseLieferabgleich = Lieferscheine]'.PHP_EOL;
                echo PHP_EOL;
                die();
        }
    }

    $MDE = new awisMDE($DebugLevel,$Benutzer);

    while($MDE->PruefePID('RFFZ')){
        $MDE->LeseRueckfuehrungZentrale();
        sleep(2);
    }
}
catch (Exception $ex)
{
    echo PHP_EOL.'awisMDE: Fehler: '.$ex->getMessage().PHP_EOL;
    $AWISInfo = new awisINFO();
    $AWISInfo->EMail(array('shuttle@de.atu.eu','shuttle@de.atu.eu'),'MDE-Fehler','Fehler : '.$ex->getMessage(),3);
}

?>







