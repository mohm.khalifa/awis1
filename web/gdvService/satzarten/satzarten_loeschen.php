<?php
/**
 * Löschen Stammdatenpflege GDV-Satzarten
 *
 * @author Schäffler Tobias
 * @copyright ATU Auto Teile Unger
 * @version 201607131457
 * @todo
 */
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

$Form = new awisFormular();
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
$DBSCHAD->Oeffnen();

$Tabelle= '';


if($FelderDel != '') // Wenn rotes X bei einer Massnahme gedrückt
{
		$Tabelle = 'VSA';
		$Felder = explode(';',$FelderDel);
		$Feld = explode('_',$Felder[1]);
		$Key=$Feld[2];

		$SQL = 'select VSA_VERSNR,VVE_BEZEICHNUNG,VSA_GDN_NUMMER,GDN_BEZEICHNUNG,VSA_GDS_NUMMER,GDS_BEZEICHNUNG from verssatzarten ';
		$SQL .= ' inner join versversandweg on VVW_VERSNR =  VSA_VERSNR';
		$SQL .= ' inner join gdvrelease on GDR_RELEASE =  VVW_RELEASE';
		$SQL .= ' left join gdvnachrichtentypen on gdn_gdr_key = gdr_key and gdn_nummer = VSA_GDN_NUMMER';
		$SQL .= ' left join gdvsatzarten on gds_gdr_key = gdr_key and GDS_NUMMER = VSA_GDS_NUMMER';
		$SQL .= ' inner join versversicherungen on vsa_versnr = vve_versnr';
		$SQL .= ' where vsa_key = '.$Key;
		$SQL .= ' order by VSA_GDN_NUMMER NULLS FIRST,VSA_GDS_NUMMER';

		$rsDaten = $DB->RecordSetOeffnen($SQL);
	
		$Felder=array();
		$Felder[]=array($Form->LadeTextBaustein('GDV','VVW_VERSICHERUNG'),$rsDaten->FeldInhalt('VVE_BEZEICHNUNG'));
		$Felder[]=array($Form->LadeTextBaustein('GDV','GDN_BEZEICHNUNG'),$rsDaten->FeldInhalt('VSA_GDN_NUMMER').'~'.$rsDaten->FeldInhalt('GDN_BEZEICHNUNG'));
		$Felder[]=array($Form->LadeTextBaustein('GDV','GDS_SATZART'),$rsDaten->FeldInhalt('VSA_GDS_NUMMER').'~'.$rsDaten->FeldInhalt('GDS_BEZEICHNUNG'));


}
elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen wenn auf Frage 'Wirklich löschen?' ok
{
	switch ($_POST['txtTabelle']) {
		case 'VSA':

			$SQL = 'DELETE FROM VERSSATZARTEN WHERE VSA_KEY=' . $_POST['txtKEY']; // Sachbearbeiter Zuord l�schen

			if ($DB->Ausfuehren($SQL) === false) {
				throw new awisException('Fehler beim Loeschen', 201202181536, $SQL, awisException::AWIS_ERR_SYSTEM);
			}
			$AWIS_KEY1 = $_POST['txtVERSNR'];
			break;
	}
}
if($Tabelle!='')
{

	$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
	switch($_GET['cmdAktion'])
	{
		case 'Details':

			$Form->SchreibeHTMLCode('<form name=frmLoeschen action="./satzarten_Main.php?cmdAktion=Details&VVW_VERSNR='.$rsDaten->FeldInhalt('VSA_VERSNR').  '" method=post>');
			$Form->Formular_Start();
			$Form->ZeileStart();
			$Form->Hinweistext('M&oumlchten sie die Daten f&uumlr folgende Versicherung wirklich l&oumlschen?');	// Fragen ob wirklich gelöscht werden soll
			$Form->ZeileEnde();
				
			$Form->Trennzeile('O');
			
			foreach($Felder AS $Feld) // Kennfelder für Massnahmen anzeigen.
			{
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($Feld[0].':',200);
				$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
				$Form->ZeileEnde();
			}
			
			/**
			 * Hiddenfelder für Anzeigen und zum löschen des Datensatzes per Hidden übergeben.
			 **/

			$Form->Erstelle_HiddenFeld('KEY',$Key);
			$Form->Erstelle_HiddenFeld('VERSNR',$rsDaten->FeldInhalt('VSA_VERSNR'));
			$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);

			$Form->Trennzeile();
				
			/**
			 * Löschen ja/nein
			**/
			$Form->ZeileStart();
			$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
			$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
			$Form->ZeileEnde();

			$Form->SchreibeHTMLCode('</form>');
				
			$Form->Formular_Ende();
				
			die();
		break;
	}
}

?>