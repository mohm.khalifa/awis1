<?php
//**************************************************************
// Liefert die Benutzer
// 
//**************************************************************
//**************************************************************
require_once('awisDatenbank.inc');
require_once('awisBenutzer.inc');

if(!isset($_GET['XBN_KEY']))
{
	die('##PARAM##');
}
$AWISBenutzer = awisBenutzer::Init();
$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();

// Auswahlliste in Abh�ngigkeit der Rechte anzeigen
$SQL = 'SELECT XBN_KEY AS KEY, COALESCE(XBN_NAME,\'\') || COALESCE(\', \' || XBN_VORNAME,\'\') AS ANZEIGE ';
$SQL .= ' FROM BENUTZER';
$SQL .= ' WHERE XBN_STATUS = \'A\'';
if($_GET['XBN_KEY']!='' AND $_GET['FILTER']=='')
{
	$SQL .= ' AND XBN_KEY = '.$DB->WertSetzen('XBN', 'N0', $_GET['XBN_KEY']);
}

if($_GET['FILTER']!='')
{
	$_GET['FILTER'].='*';
	$SQL .= ' AND (UPPER(XBN_NAME) '.$DB->LikeOderIst($_GET['FILTER'],awisDatenbank::AWIS_LIKE_UPPER,'XBN').'';
	$SQL .= ' OR UPPER(XBN_VORNAME) '.$DB->LikeOderIst($_GET['FILTER'],awisDatenbank::AWIS_LIKE_UPPER,'XBN');
	$SQL .= ' OR UPPER(XBN_VOLLERNAME) '.$DB->LikeOderIst($_GET['FILTER'],awisDatenbank::AWIS_LIKE_UPPER,'XBN');
	$SQL .= ')';
}

// Einschr�nkung f�r Benutzer der eigenen Abteilung(en)
if(isset($_GET['KON_KEY']))
{
    $SQL .= ' AND EXISTS(SELECT * FROM KONTAKTEABTEILUNGENZUORDNUNGEN ';
    $SQL .= '            INNER JOIN KONTAKTE ON KZA_KON_KEY = KON_KEY';
    $SQL .= '               WHERE KON_STATUS = \'A\' AND XBN_KON_KEY = KON_KEY AND KZA_KAB_KEY IN (SELECT KZA_KAB_KEY';
    $SQL .= '                                   FROM KONTAKTEABTEILUNGENZUORDNUNGEN ';
    $SQL .= '                                  WHERE KZA_KON_KEY = '.$DB->WertSetzen('XBN', 'N0', $_GET['KON_KEY']);
    $SQL .= '                                       )';
    $SQL .= '           )';
}

$SQL .= ' ORDER BY 2';

$rsXBN = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('XBN'));
var_dump($DB->LetzterSQL());
if(isset($_GET['WERT']))
{
	echo $_GET['WERT'];
}
echo '#~#';		// Trennzeichen
while(!$rsXBN->EOF())
{
	if(stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') === false AND stripos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0')===false)
	{
		echo '<option value="'.$rsXBN->FeldInhalt('KEY').'">'.$rsXBN->FeldInhalt('ANZEIGE').'</option>';
	}
	else
	{
		echo '#+#'.$rsXBN->FeldInhalt('KEY').'#+#'.$rsXBN->FeldInhalt('ANZEIGE').'';
	}

	$rsXBN->DSWeiter();
}
?>