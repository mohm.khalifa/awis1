<?php
require_once('praktikum_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PVS','%');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','lbl_zurueck');

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Recht18000 = $AWISBenutzer->HatDasRecht(18000);
	if($Recht18000 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
	
	$PV = new awis_praktikum_funktionen($DB, $AWISBenutzer, $Form, $AWISCursorPosition);

	/**********************************************
	* Benutzerparameter
	***********************************************/	
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_PVS')) < 8))
	{
        $Param['SUCH_PVS_FilNr'] = '';
        $Param['SUCH_PVS_Filiale'] = '';
        $Param['SUCH_PVS_Betreuer'] = '';
        $Param['SUCH_PVS_Praktikant'] = '';
        $Param['SUCH_PVS_Praktikumart'] = 0;
        $Param['SUCH_PVS_DatumVon'] = '';
        $Param['SUCH_PVS_DatumBis'] = '';
        $Param['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_PVS", serialize($Param));
	}
    else
    {
	    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_PVS'));
       	if (! isset($Param['SPEICHERN']))
    	{
    		$Param['SPEICHERN'] = 'off';
            $Param['SUCH_PVS_FilNr'] = '';
            $Param['SUCH_PVS_Filiale'] = '';
            $Param['SUCH_PVS_Betreuer'] = '';
            $Param['SUCH_PVS_Praktikant'] = '';
            $Param['SUCH_PVS_Praktikumart'] = 0;
            $Param['SUCH_PVS_DatumVon'] = '';
            $Param['SUCH_PVS_DatumBis'] = '';
            $Param['SPEICHERN']= 'off';
    	    $AWISBenutzer->ParameterSchreiben("Formular_PVS", serialize($Param));    		
    	}
	}

	/**********************************************
	* Eingabemaske
	***********************************************/	
	$Form->SchreibeHTMLCode("<form name=frmPVSSuche method=post action=./praktikum_Main.php?cmdAktion=Details>");
	$Form->Formular_Start();
    $PV->Erstelle_Suche($Param);
	$Form->Formular_Ende();	

	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	// Suche abschicken
	if(($Recht18000&1) == 1)
	{	
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}
    // Hinzuf�gen, wenn erlaubt	
	if(($Recht18000&4) == 4)
	{
	    $Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();
    $Form->SetzeCursor($AWISCursorPosition);
    $Form->SchreibeHTMLCode("</form>");
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221210");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201110221211");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>