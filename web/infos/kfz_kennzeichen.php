<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

global $AWISBenutzer;
global $awisRSZeilen;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?php

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();

if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con,1200);
if($RechteStufe==0)
{
    awisEreignis(3,1000,'KFZ Kennzeichen',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$SQL='';

if(isset($_GET['Sort']) AND $_GET['Sort']!='')
{
	if($_GET['Sort']!='BUL_Bundesland')
	{
		$SQL .= ' ORDER BY ' . $_GET['Sort'];
	}
	else
	{
		$SQL .= ' ORDER BY BUL_Bundesland, LXK_Zulassungsstelle';
	}
}
else
{
	$SQL .= ' ORDER BY LXK_Kennzeichen, LXK_Zulassungsstelle';
}

print '<table border=0 width=100%><tr><td><h1 id=SeitenTitel><a name="aktuelle">Aktuelle KFZ-Kennzeichen</a></h1></td><td align=right>';
print '<a href="#auslaufende">zu den auslaufenden</a></td></tr></table>';

$rs = awisOpenRecordset($con, "SELECT LXK_KENNZEICHEN, LXK_ZULASSUNGSSTELLE, LXK_ID, LXK_ZUSATZID, NVL(BUL_BUNDESLAND, '--') LXK_GEBIET1 FROM LexikonKFZKennzeichen,Bundeslaender where LXK_Aktuell = 1 and LXK_GEBIET = BUL_LXK_GEBIET(+)" . $SQL );
$rsZeilen = $awisRSZeilen;

echo '<table border=1 width=100%>';
echo '<tr><td id=FeldBez><a href=./kfz_kennzeichen.php?Sort=>Kennz</a></td>';
echo '<td id=FeldBez><a href=./kfz_kennzeichen.php?Sort=LXK_Zulassungsstelle>Zulassungsstelle</a></td>';
echo '<td id=FeldBez><a href=./kfz_kennzeichen.php?Sort=BUL_Bundesland>Gebiet</a></td>';
//echo '<td id=FeldBez>ID</td>';
//echo '<td id=FeldBez>Zusatz</td>';
echo '</tr>';

for($Zeile = 0;$Zeile<$rsZeilen;$Zeile++)
{
	echo '<tr>';

	echo '<td ' . (($Zeile % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_KENNZEICHEN'][$Zeile]. '</td>';
	echo '<td ' . (($Zeile % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_ZULASSUNGSSTELLE'][$Zeile]. '</td>';
	echo '<td ' . (($Zeile % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_GEBIET1'][$Zeile]. '</td>';
//	echo '<td ' . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_ID'][$Zeile]. '</td>';
//	echo '<td ' . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_ZUSATZID'][$Zeile]. '</td>';

	echo '</tr>';
}

echo '</table>';

print '<br><table border=0 width=100%><tr><td><h1 id=SeitenTitel><a name="auslaufende">Auslaufende KFZ-Kennzeichen</a></h1></td><td align=right>';
print '<a href="#aktuelle">zu den aktuellen</a></td></tr></table>';

$rs = awisOpenRecordset($con, "SELECT LXK_KENNZEICHEN, LXK_ZULASSUNGSSTELLE, LXK_ID, LXK_ZUSATZID, NVL(BUL_BUNDESLAND, '--') LXK_GEBIET1 FROM LexikonKFZKennzeichen,Bundeslaender where LXK_Aktuell = 0 and LXK_GEBIET = BUL_LXK_GEBIET(+)" . $SQL );
$rsZeilen = $awisRSZeilen;

echo '<table border=1 width=100%>';
echo '<tr><td id=FeldBez><a href=./kfz_kennzeichen.php?Sort=>Kennz</a></td>';
echo '<td id=FeldBez><a href=./kfz_kennzeichen.php?Sort=LXK_Zulassungsstelle>Jetzt daf�r zust�ndige Zulassungsstelle</a></td>';
echo '<td id=FeldBez><a href=./kfz_kennzeichen.php?Sort=BUL_Bundesland>Gebiet</a></td>';
//echo '<td id=FeldBez>ID</td>';
//echo '<td id=FeldBez>Zusatz</td>';
echo '</tr>';

for($Zeile = 0;$Zeile<$rsZeilen;$Zeile++)
{
	echo '<tr>';

	echo '<td ' . (($Zeile % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_KENNZEICHEN'][$Zeile]. '</td>';
	echo '<td ' . (($Zeile % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_ZULASSUNGSSTELLE'][$Zeile]. '</td>';
	echo '<td ' . (($Zeile % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_GEBIET1'][$Zeile]. '</td>';
//	echo '<td ' . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_ID'][$Zeile]. '</td>';
//	echo '<td ' . (($i % 2) == 0?"id=TabellenZeileGrau":"id=TabellenZeileWeiss") . '>'.$rs['LXK_ZUSATZID'][$Zeile]. '</td>';

	echo '</tr>';
}

echo '</table>';

print '<a href="#aktuelle">zur&uuml;ck zu den aktuellen Kennzeichen</a><br>';
print '<a href="#auslaufende">zur&uuml;ck zu den auslaufenden Kennzeichen</a>';

print "<br><hr><input type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='infos_Main.php';>";
print "<input type=hidden name=cmdAktion value=Suche>";

awislogoff($con);

?>
</body>
</html>

