<?php

global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

        $Form->Formular_Start();

        if(isset($_GET['Del']))
	{
		$Tabelle = 'FKA';
		$Key=$_GET['Del'];

                $TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

                $SQL = "Select WBE_KEY,WBE_FILIALEN,WBE_DATVON,WBE_DATBIS FROM WERBEMITTELZUORDNUNG WHERE WBE_KEY=".$_GET['Del'];

                $rsDel = $DB->RecordSetOeffnen($SQL);

                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_KEY'],60,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_FILIALEN'],200,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_DATVON'],200,'');
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['WBE']['WBE_DATBIS'],200,'');
                $Form->ZeileEnde();

                $FGKZeile=0;
                $DS = 0;

                While(!$rsDel->EOF())
                {
		    $Form->ZeileStart();	
                    $Form->Erstelle_ListenFeld('WBE_KEY',$rsDel->FeldInhalt('WBE_KEY'),0,60,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('WBE_FILIALEN',$rsDel->FeldInhalt('WBE_FILIALEN'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('WBE_DATVON',$rsDel->FeldInhalt('WBE_DATVON'),0,200,false,($FGKZeile%2),'','');
                    $Form->Erstelle_ListenFeld('WBE_DATBIS',$rsDel->FeldInhalt('WBE_DATBIS'),0,200,false,($FGKZeile%2),'','');
                    $Form->ZeileEnde();

                    $FGKZeile++;
                    $rsDel->DSWeiter();
                }

                $Form->SchreibeHTMLCode('<form name=frmLoeschen action=./verteilqualitaet_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

                $Form->Erstelle_HiddenFeld('WEB_KEY', $_REQUEST['txtWEB_KEY']);
                $Form->Erstelle_HiddenFeld('WBE_KEY', $Key);
		
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
                
        	$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
        elseif($_REQUEST['cmdLoeschenOK'])
        {
	    $SQL = "SELECT * from WERBEMITTELZUORDNUNG WHERE WBE_KEY=".$DB->FeldInhaltFormat('NO', $_POST['txtWBE_KEY']);
            
            $rsDelDaten = $DB->RecordSetOeffnen($SQL);

            $SQL = "Delete from WERBEMITTELZUORDNUNG WHERE WBE_KEY=".$DB->FeldInhaltFormat('NO',  $_POST['txtWBE_KEY']);

             if($DB->Ausfuehren($SQL)===false)
             {
             }
	     
	     $Fehler = 'Filiale :'.$rsDelDaten->FeldInhalt('WBE_FILIALEN').' wurde gel�scht';
			   
	     if($Fehler!='')
	     {
		echo('<span class=HinweisText>'.$Fehler.'</span>');
	     } 
	     
	      $AWIS_KEY1 = $_POST['txtWEB_KEY'];
	     
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

?>