<?php

global $AWIS_KEY1;
global $AWIS_KEY2;
global $AWIS_HISTKEY;
global $AWISCursorPosition;
global $flagKassendatenSuchen;
global $EditModus;
global $DetailAnsicht;

try
{
    $AWISBenutzer = awisBenutzer::Init();
    $MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

    $ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
    $IconPosition = $AWISBenutzer->ParameterLesen('Schaltflaechen_Position');
    $SchnellSucheFeld = $AWISBenutzer->ParameterLesen('Artikelstamm_Schnellsuche');
    $BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');

    $TextKonserven = array();
    $TextKonserven[] = array('FGK', '*');
    $TextKonserven[] = array('FKH', '*');
    $TextKonserven[] = array('FKA', '*');
    $TextKonserven[] = array('Wort', 'Seite');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'lbl_aendern');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'lbl_loeschen');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
    $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'lbl_Hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_JaNein');
    $TextKonserven[] = array('Wort', 'lbl_AEMNachdruck');
    $TextKonserven[] = array('Wort', 'Umbuchung');

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht9100 = $AWISBenutzer->HatDasRecht(9100);

    if ($Recht9100 == 0)
    {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $flagKassendatenSuchen = false;
    $Form->DebugAusgabe(1, $_POST);
    $Form->DebugAusgabe(1, $_REQUEST);

    $Form->Formular_Start();

    if (isset($_POST['cmdSuche_x']))
    {

        $Param['FGK_VORGANGNR'] = $_POST['sucFGK_VORGANGNR'];
        $Param['FGK_FILID'] = $_POST['sucFGK_FILID'];
        $Param['FGK_KFZKENNZ'] = $_POST['sucFGK_KFZKENNZ'];

        $Param['ORDERBY'] = '';
        $Param['BLOCK'] = '';
        $Param['KEY'] = 0;

        $AWISBenutzer->ParameterSchreiben("FirstglasDetails", serialize($Param));

        //$Form->DebugAusgabe(1,$Param);
        $AWISBenutzer->ParameterSchreiben("AktuellerFGK", '');
    }
    if (isset($_POST['cmdDSZurueck_x']))
    { // vorheriger DS
        $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
        $Bedingung = _BedingungErstellen($Param);

        $Bedingung .= ' AND FGK_KEY < ' . floatval($_POST['txtFGK_KEY']);

        $SQL = 'SELECT * FROM (SELECT FGK_KEY FROM FGKOPFDATEN  ';
        $SQL .= 'WHERE ' . substr($Bedingung, 4) . ' ';
        $SQL .= 'ORDER BY FGK_KEY) WHERE ROWNUM = 1';

        $rsFGK = $DB->RecordSetOeffnen($SQL);
        if ($rsFGK->FeldInhalt('FGK_KEY') != '')
        {
            $AWIS_KEY1 = $rsFGK->FeldInhalt('FGK_KEY');
        }
        else
        {
            $AWIS_KEY1 = $_POST['txtFGK_KEY'];
        }
    }
    elseif (isset($_POST['cmdDSWeiter_x']))
    { // n�chster DS
        $Param = unserialize($AWISBenutzer->ParameterLesen('FirstglasDetails'));
        $Bedingung = _BedingungErstellen($Param);

        $Bedingung .= ' AND FGK_KEY > ' . floatval($_POST['txtFGK_KEY']);

        $SQL = 'SELECT * FROM (SELECT FGK_KEY FROM FGKOPFDATEN  ';
        $SQL .= 'WHERE ' . substr($Bedingung, 4) . ' ';
        $SQL .= 'ORDER BY FGK_KEY) WHERE ROWNUM = 1';

        $rsFGK = $DB->RecordSetOeffnen($SQL);
        if ($rsFGK->FeldInhalt('FGK_KEY') != '')
        {
            $AWIS_KEY1 = $rsFGK->FeldInhalt('FGK_KEY');
        }
        else
        {
            $AWIS_KEY1 = $_POST['txtFGK_KEY'];
        }
    }
    elseif (isset($_POST['cmdSpeichern_x']))
    {
        include './firstglas_speichern.php';
    }
    elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
    {
        include './firstglas_loeschen.php';
    }
    elseif (isset($_POST['cmdLoeschenAbbrechen']))
    {

        $AWIS_KEY1 = $_REQUEST['txtFGK_KEY'];
    }
    elseif (isset($_GET['FGK_KEY']))
    {
        $AWIS_KEY1 = floatval($_GET['FGK_KEY']);
    }
    elseif (isset($_GET['Key']))
    {
        $AWIS_KEY1 = floatval($_GET['Key']);
    }
    elseif (isset($_GET['FGK_KEY']))
    {
        $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $_GET['FGK_KEY']);
        $AWISBenutzer->ParameterSchreiben("AktuellerFGK", $_GET['FGK_KEY']);
    }
    else
    {
        if (!isset($_GET['Liste']) and !isset($_GET['Sort']) and !isset($_GET['Block']) and !isset($_POST['Block']))
        {
            $AWIS_KEY1 = $AWISBenutzer->ParameterLesen("AktuellerFGK");
        }
    }

    if (isset($_GET['FKH_KEY']))
    {
        $Form->Erstelle_Liste_Ueberschrift("Historisierung", 1253);
        $Param['FKH_KEY'] = $_GET['FKH_KEY'];
        $AWIS_HISTKEY = $_GET['FKH_KEY'];
    }

    if (isset($_POST['sucKassendatenSuchen']) == 'on' OR isset($_GET['FKA_KEY']) OR isset($_GET['FKAListe']) OR isset($_POST['cmdSpeichernKassendaten_x']) OR isset($_POST['cmdDSKasseWeiter_x']) OR isset($_POST['cmdDSKasseZurueck_x']) OR isset($_POST['FKABlock']))
    {
        $flagKassendatenSuchen = true;
    }

    @$Param = unserialize($AWISBenutzer->ParameterLesen("FirstglasDetails"));

    if ($flagKassendatenSuchen != true)
    {
        $Bedingung = _BedingungErstellen($Param);

        if (!isset($_GET['Sort']))
        {
            if (isset($Param['ORDERBY']) AND $Param['ORDERBY'] != '')
            {
                $ORDERBY = $Param['ORDERBY'];
            }
            else
            {
                $ORDERBY = ' FGK_KEY';
            }
        }
        else
        {
            $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        }

        $SQL = 'SELECT FGKOPFDATEN.*';
        if ($AWIS_KEY1 <= 0)
        {
            $SQL .= ',row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr ';
        }
        $SQL .= ' FROM FGKOPFDATEN';
        if ($Bedingung != '')
        {
            $SQL .= ' WHERE' . substr($Bedingung, 4);
        }
        $SQL .= ' ORDER BY ' . $ORDERBY;

        $Block = 1;
        if (isset($_REQUEST['Block']))
        {
            $Block = $Form->Format('N0', $_REQUEST['Block'], false);
        }
        elseif (isset($Param['BLOCK']) AND $Param['BLOCK'] != '')
        {
            $Block = intval($Param['BLOCK']);
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

        if ($AWIS_KEY1 <= 0)
        {
            $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
        }

        $rsFGK = $DB->RecordSetOeffnen($SQL);

        $Param['ORDERBY'] = $ORDERBY;
        $Param['KEY'] = $AWIS_KEY1;
        $Param['BLOCK'] = $Block;
        $AWISBenutzer->ParameterSchreiben("FirstglasDetails", serialize($Param));

        echo '<form name=frmFirstglasDetails method=post action=./autoglas_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . $_GET['Block'] : '') . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '') . (isset($_GET['Unterseite']) ? '&Unterseite=' . $_GET['Unterseite'] : '') . '>';
    }

    if (!isset($_GET['FKH_KEY']) && $flagKassendatenSuchen != true)
    {

        if (($rsFGK->AnzahlDatensaetze() > 1) or (isset($_GET['Liste'])))
        {
            $Form->SchreibeHTMLCode('<form name="frmFirstglasDetails" action="./autoglas_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');

            $Form->ZeileStart();
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
            $Link .= '&Sort=FGK_KEY' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FGK_KEY')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_KEY'], 120, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
            $Link .= '&Sort=FGK_FILID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FGK_FILID')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_FILID'], 120, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Blo ck']) : '');
            $Link .= '&Sort=FGK_VORGANGNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FGK_VORGANGNR')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_VORGANGNR'], 200, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
            $Link .= '&Sort=FGK_KFZKENNZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FGK_KFZKENNZ')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_KFZKENNZ'], 200, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['Block']) ? '&Block=' . intval($_GET['Block']) : '');
            $Link .= '&Sort=FGK_KFZBEZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FGK_KFZBEZ')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FGK']['FGK_KFZBEZ'], 400, '', $Link);

            $Form->ZeileEnde();

            $FGKZeile = 0;
            while (!$rsFGK->EOF())
            {
                $Form->ZeileStart();
                $Link = './autoglas_Main.php?cmdAktion=Details&FGK_KEY=' . $rsFGK->FeldInhalt('FGK_KEY') . '';
                $Form->Erstelle_ListenFeld('FGK_KEY', $rsFGK->FeldInhalt('FGK_KEY'), 0, 120, false, ($FGKZeile % 2), '', $Link, 'T');
                $Form->Erstelle_ListenFeld('FGK_FILID', $rsFGK->FeldInhalt('FGK_FILID'), 0, 120, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FGK_VORGANGNR', $rsFGK->FeldInhalt('FGK_VORGANGNR'), 0, 200, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FGK_KFZKENNZ', $rsFGK->FeldInhalt('FGK_KFZKENNZ'), 0, 200, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FGK_KFZBEZ', $rsFGK->FeldInhalt('FGK_KFZBEZ'), 0, 400, false, ($FGKZeile % 2), '', '');
                $Form->ZeileEnde();

                $rsFGK->DSWeiter();
                $FGKZeile++;
            }

            $Link = './autoglas_Main.php?cmdAktion=Details';
            $Form->SchreibeHTMLCode("<input type='hidden' name='BLOCK'>");
            $Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

            $Form->SchaltflaechenStart();
            $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
            $Form->SchaltflaechenEnde();

            $Form->SchreibeHTMLCode('</form>');
        }
        else
        {

            $Form->SchreibeHTMLCode('<form name=frmFirstGlasDetails action=./autoglas_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

            if ($AWIS_KEY1 != -1)
            {
                $AWIS_KEY1 = $rsFGK->FeldInhalt('FGK_KEY');
                $Param['KEY'] = $AWIS_KEY1;
                $AWISBenutzer->ParameterSchreiben('FirstglasDetails', serialize($Param));
                $Form->Erstelle_HiddenFeld('FGK_KEY', $AWIS_KEY1);
                $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
            }
            else
            {
                $AWIS_KEY1 = -1;
                $AWIS_KEY2 = -1;
            }

            $AWIS_KEY1 = $DB->FeldInhaltFormat('N0', $rsFGK->FeldInhalt('FGK_KEY'));
            $AWISBenutzer->ParameterSchreiben("AktuellerFGK", $rsFGK->FeldInhalt('FGK_KEY'));

            $Felder = array();
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./autoglas_Main.php?cmdAktion=Details&Liste=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsFGK->FeldInhalt('FGK_USER'));
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', $rsFGK->FeldInhalt('FGK_USERDAT')));
            $Form->InfoZeile($Felder, '');


            $EditRecht = (($Recht9100 & 2) != 0);

            if ($AWIS_KEY1 >= 0)
            {
                $SQL = 'SELECT fgd_fgb_key, fgb_bezeichnung, max(status) as STATUS ';
                $SQL .='FROM ( ';
                $SQL .='SELECT fgd.fgd_fgb_key, \'X\' as status, fgb.fgb_bezeichnung ';
                $SQL .='FROM FGKOPFDATEN fgk INNER JOIN FGSTATUSDEFINITIONEN fgd ON fgk.fgk_fgn_key = fgd.fgd_fgn_key ';
                $SQL .='INNER JOIN FGSTATUSBESCHREIBUNGEN fgb ON fgd.fgd_fgb_key = fgb_key ';
                $SQL .='WHERE FGK_VORGANGNR =' . $DB->FeldInhaltFormat('T', $rsFGK->FeldInhalt('FGK_VORGANGNR'));
                $SQL .=' UNION';
                $SQL .=' SELECT fgb.fgb_key, \'\' as status, fgb.fgb_bezeichnung';
                $SQL .=' FROM FGSTATUSBESCHREIBUNGEN fgb )';
                $SQL .=' group by fgd_fgb_key, fgb_bezeichnung';
                $SQL .=' order by fgd_fgb_key';

                $rsCheckBoxen = $DB->RecordSetOeffnen($SQL);

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KEY'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_KEY', $rsFGK->FeldInhalt('FGK_KEY'), 20, 50, false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KENNUNG'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_KENNUNG', $rsFGK->FeldInhalt('FGK_KENNUNG'), 10, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_FILID'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_FILID', $rsFGK->FeldInhalt('FGK_FILID'), 5, 100, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VORGANGNR'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_VORGANGNR', $rsFGK->FeldInhalt('FGK_VORGANGNR'), 50, 50, false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KFZKENNZ'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_KFZKENNZ', $rsFGK->FeldInhalt('FGK_KFZKENNZ'), 10, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KBANR'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_KBANR', $rsFGK->FeldInhalt('FGK_KBANR'), 7, 100, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_FAHRGESTELLNR'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_FAHRGESTELLNR', $rsFGK->FeldInhalt('FGK_FAHRGESTELLNR'), 30, 100, false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KFZBEZ'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_KFZBEZ', $rsFGK->FeldInhalt('FGK_KFZBEZ'), 150, 600, false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VERSICHERUNG'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_VERSICHERUNG', $rsFGK->FeldInhalt('FGK_VERSICHERUNG'), 150, 600, false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VERSSCHEINNR'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_VERSSCHEINNR', $rsFGK->FeldInhalt('FGK_VERSSCHEINNR'), 20, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_SB'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_SB', $rsFGK->FeldInhalt('FGK_SB'), 7, 100, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_MONTAGEDATUM'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_MONTAGEDATUM', $rsFGK->FeldInhalt('FGK_MONTAGEDATUM'), 30, 200, false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_ZEITSTEMPEL'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_ZEITSTEMPEL', $rsFGK->FeldInhalt('FGK_ZEITSTEMPEL'), 20, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_STEUERSATZ'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_STEUERSATZ', $rsFGK->FeldInhalt('FGK_STEUERSATZ'), 7, 100, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_KUNDENNAME'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_KUNDENNAME', $rsFGK->FeldInhalt('FGK_KUNDENNAME'), 30, 200, false);
                $Form->ZeileEnde();
                
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_VERSNR'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_VERSNR', $rsFGK->FeldInhalt('FGK_VERSNR'), 20, 200, false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('', 350);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_NOTIZ'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_NOTIZ', $rsFGK->FeldInhalt('FGK_NOTIZ'), 92, 100, $EditRecht, '', '', '', 'T', '', '', '', 255, '', '');
                //$Form->Erstelle_TextFeld('FGK_NOTIZ', $rsFGK->FeldInhalt('FGK_NOTIZ'), 92, 100, $EditRecht);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_ZEITSTEMPEL_KASSIERT'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_ZEITSTEMPEL_KASSIERT', $rsFGK->FeldInhalt('FGK_ZEITSTEMPEL_KASSIERT'), 20, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_FREIGABEGRUND'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_FREIGABEGRUND', $rsFGK->FeldInhalt('FGK_FREIGABEGRUND'), 92, 100, $EditRecht);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FGK']['FGK_DATUMZUGANG'] . ':', 150);
                $Form->Erstelle_TextFeld('FGK_DATUMZUGANG', $rsFGK->FeldInhalt('FGK_DATUMZUGANG'), 30, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Umbuchung'] . ':', 145);

                $SQL = "Select FUM_VORGANGSNR, FUM_BGT_KEY FROM FGUMBUCHUNG ";
                $SQL .= " WHERE FUM_VORGANGSNR=" . $DB->FeldInhaltFormat('T', $rsFGK->FeldInhalt('FGK_VORGANGNR'));

                $rsCheckBox = $DB->RecordSetOeffnen($SQL);

                if ($rsCheckBox->AnzahlDatensaetze() == 0)
                {
                    $Form->Erstelle_Checkbox('checkUmbuchung', '', 30, true, 'off', '', '');
                }
                else
                {
                    $Form->Erstelle_Checkbox('checkUmbuchung', 'on', 30, true, 'on', '', '');
                }

                $SQL = "Select FBG_KEY,FBG_BEGRUENDUNG FROM FGUMBUCHUNGBG";
                $rsUmbuchungsgruende = $DB->RecordSetOeffnen($SQL);
                $Form->Erstelle_SelectFeld('FUM_KEY', $rsCheckBox->FeldInhalt('FUM_BGT_KEY'), 200, $EditRecht, $SQL, '');

                $Form->ZeileEnde();

                $Form->Trennzeile('P');
                echo "<br>";
                $zeilenNr = 0;

                $Form->ZeileStart();

                $zeilenNr = 0;
                $StornoCount = 0;
                $ZugangCount = 0;
                $count = 0;

                while (!$rsCheckBoxen->EOF())
                {
                    if ($zeilenNr != 3)
                    {

                        if ($rsCheckBoxen->FeldInhalt('FGB_BEZEICHNUNG') == 'Vorgang_Blank')
                        {
                            
                        }
                        else
                        {
                            if ($count == 0)
                            {
                                $Form->Erstelle_TextLabel($rsCheckBoxen->FeldInhalt('FGB_BEZEICHNUNG') . ':', 280);
                                $zeilenNr = 0;
                                $count++;
                            }
                            else
                            {
                                $Form->Erstelle_TextLabel($rsCheckBoxen->FeldInhalt('FGB_BEZEICHNUNG') . ':', 280);
                            }
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '1')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, $EditRecht, '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '2')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '3')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '4')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '5')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '6')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }


                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '7')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, $EditRecht, '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '8')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '9')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '10')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '11')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, $EditRecht, '', '', '1', '', '', $Daten);
                        }

                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '12')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }
                        if ($rsCheckBoxen->FeldInhalt('FGD_FGB_KEY') == '18')
                        {
                            $Daten = explode('|', $AWISSprachKonserven['Liste']['lst_JaNein']);
                            $Form->Erstelle_SelectFeld('FeldSpeichern' . $rsCheckBoxen->FeldInhalt('FGD_FGB_KEY'), ($rsCheckBoxen->FeldInhalt('STATUS') == 'X' ? 1 : 0), 70, '', '', '', '1', '', '', $Daten);
                        }
                        $zeilenNr++;
                    }
                    else
                    {
                        $Form->ZeileEnde();
                        $zeilenNr = 0;
                        $Form->ZeileStart();
                        $rsCheckBoxen->DSZurueck();
                    }
                    $rsCheckBoxen->DSWeiter();
                }
                $Form->Trennzeile('O');
            }
            if ($AWIS_KEY1 != 0)
            {
                $RegisterSeite = (isset($_GET['Seite']) ? $_GET['Seite'] : (isset($_POST['Seite']) ? $_POST['Seite'] : ''));
                $SubReg = new awisRegister(9101);
                $SubReg->ZeichneRegister((isset($_GET['Seite']) ? $_GET['Seite'] : ''));
            }

            $Form->SchaltflaechenStart();
            $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
            if (($Recht9100 & 4) == 4)
            {
                $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
            }

            $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
            $Form->Schaltflaeche('image', 'cmdDSWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');

            $Form->SchaltflaechenEnde();

            $Form->SchreibeHTMLCode('</form>');
        }
    }
    elseif ($flagKassendatenSuchen != true)
    {
        $SQL = "Select * from FGKOPFDATEN_HIST WHERE FKH_KEY=" . $DB->FeldInhaltFormat('NO', $AWIS_HISTKEY);

        $rsFKH = $DB->RecordSetOeffnen($SQL);

        $Form->SchreibeHTMLCode('<form name=frmFirstGlasDetails action=./autoglas_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./autoglas_Main.php?cmdAktion=Details&FGKListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsFKH->FeldInhalt('FGK_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', $rsFKH->FeldInhalt('FGK_USERDAT')));
        $Form->InfoZeile($Felder, '');

        $EditRecht = (($Recht9100 & 2) != 0);

        if ($AWIS_KEY1 >= 0)
        {
            $EditRecht = true;

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KEY'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_KEY', $rsFKH->FeldInhalt('FKH_KEY'), 20, 50, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('L');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KENNUNG'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_KENNUNG', $rsFKH->FeldInhalt('FKH_KENNUNG'), 10, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_FILID'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_FILID', $rsFKH->FeldInhalt('FKH_FILID'), 5, 100, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_VORGANGNR'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_VORGANGNR', $rsFKH->FeldInhalt('FKH_VORGANGNR'), 50, 50, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KFZKENNZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_KFZKENNZ', $rsFKH->FeldInhalt('FKH_KFZKENNZ'), 10, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KBANR'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_KBANR', $rsFKH->FeldInhalt('FKH_KBANR'), 7, 100, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_FAHRGESTELLNR'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_FAHRGESTELLNR', $rsFKH->FeldInhalt('FKH_FAHRGESTELLNR'), 30, 100, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KFZBEZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_KFZBEZ', $rsFKH->FeldInhalt('FKH_KFZBEZ'), 150, 600, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_VERSICHERUNG'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_VERSICHERUNG', $rsFKH->FeldInhalt('FKH_VERSICHERUNG'), 150, 600, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_VERSSCHEINNR'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_VERSSCHEINNR', $rsFKH->FeldInhalt('FKH_VERSSCHEINNR'), 20, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_SB'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_SB', $rsFKH->FeldInhalt('FKH_SB'), 7, 100, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_MONTAGEDATUM'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_MONTAGEDATUM', $rsFKH->FeldInhalt('FKH_MONTAGEDATUM'), 30, 100, false);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_ZEITSTEMPEL'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_ZEITSTEMPEL', $rsFKH->FeldInhalt('FKH_ZEITSTEMPEL'), 20, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_STEUERSATZ'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_STEUERSATZ', $rsFKH->FeldInhalt('FKH_STEUERSATZ'), 7, 100, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_KUNDENNAME'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_KUNDENNAME', $rsFKH->FeldInhalt('FKH_KUNDENNAME'), 30, 200, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_ZEITSTEMPEL_KASSIERT'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_ZEITSTEMPEL_KASSIERT', $rsFKH->FeldInhalt('FKH_ZEITSTEMPEL_KASSIERT'), 20, 200, false);
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_FREIGABEGRUND'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_FREIGABEGRUND', $rsFKH->FeldInhalt('FKH_FREIGABEGRUND'), 92, 100, $EditRecht);
            $Form->ZeileEnde();

            $Form->ZeileStart();
            $Form->Erstelle_TextLabel($AWISSprachKonserven['FKH']['FKH_DATUMZUGANG'] . ':', 150);
            $Form->Erstelle_TextFeld('FKH_DATUMZUGANG', $rsFKH->FeldInhalt('FKH_DATUMZUGANG'), 20, 50, false);
            $Form->ZeileEnde();

            $Form->Trennzeile('O');
            //Status anzeigen
        }

        //Erstelle Unterregister
        if ($AWIS_HISTKEY != 0)
        {
            $RegisterSeite = (isset($_GET['Seite']) ? $_GET['Seite'] : (isset($_POST['Seite']) ? $_POST['Seite'] : ''));
            $SubReg = new awisRegister(9101);
            $SubReg->ZeichneRegister((isset($_GET['Seite']) ? $_GET['Seite'] : ''));
        }
        $Form->SchaltflaechenStart();
        $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');

        if (($Recht9100 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))
        {
            
        }
        $Form->SchaltflaechenEnde();

        $Form->SchreibeHTMLCode('</form>');
    }
    elseif ($flagKassendatenSuchen == true)
    {
        if (isset($_POST['cmdSuche_x']))
        {
            $Param['FGK_VORGANGNR'] = $_POST['sucFGK_VORGANGNR'];
            $Param['FGK_FILID'] = $_POST['sucFGK_FILID'];
            $Param['FGK_WANR'] = $_POST['sucFKA_WANR'];
            $Param['FGK_DATUMVON'] = $_POST['sucFKA_DATUMVON'];
            $Param['FGK_DATUMBIS'] = $_POST['sucFKA_DATUMBIS'];
            $Param['FGK_KFZKENNZ'] = $_POST['sucFGK_KFZKENNZ'];

            $Param['ORDERBY'] = '';
            $Param['FKABLOCK'] = '';
            $Param['KEY'] = 0;

            $AWISBenutzer->ParameterSchreiben("FKASuche", serialize($Param));

            $AWISBenutzer->ParameterSchreiben("AktuellerFKA", '');
        }
        elseif (isset($_POST['cmdSpeichernKassendaten_x']))
        {
            include './firstglas_kassendaten_speichern.php';

            $TextKonserven = array();
            $TextKonserven[] = array('FGK', '*');
            $TextKonserven[] = array('FKH', '*');
            $TextKonserven[] = array('FKA', '*');
            $TextKonserven[] = array('Wort', 'Seite');
            $TextKonserven[] = array('Wort', 'lbl_suche');
            $TextKonserven[] = array('Wort', 'lbl_weiter');
            $TextKonserven[] = array('Wort', 'lbl_speichern');
            $TextKonserven[] = array('Wort', 'lbl_trefferliste');
            $TextKonserven[] = array('Wort', 'lbl_aendern');
            $TextKonserven[] = array('Wort', 'lbl_hilfe');
            $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
            $TextKonserven[] = array('Wort', 'lbl_loeschen');
            $TextKonserven[] = array('Wort', 'lbl_zurueck');
            $TextKonserven[] = array('Wort', 'lbl_DSZurueck');
            $TextKonserven[] = array('Wort', 'lbl_DSWeiter');
            $TextKonserven[] = array('Wort', 'lbl_drucken');
            $TextKonserven[] = array('Wort', 'lbl_Hilfe');
            $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
            $TextKonserven[] = array('Liste', 'lst_JaNein');
            $TextKonserven[] = array('Wort', 'lbl_AEMNachdruck');

            $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
        }
        elseif (isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
        {
            include './firstglas_loeschen.php';
        }
        elseif (isset($_POST['cmdLoeschenAbbrechen']))
        {
            $AWIS_KEY1 = $_POST['txtFKAKey'];
        }
        elseif (isset($_GET['Key']))
        {
            $AWIS_KEY1 = floatval($_GET['Key']);
        }
        elseif (isset($_GET['FKA_KEY']))
        {
            $AWIS_KEY1 = floatval($_GET['FKA_KEY']);
        }
        if (isset($_POST['cmdDSKasseZurueck_x']))
        { // vorheriger DS
            $Param = unserialize($AWISBenutzer->ParameterLesen('FKASuche'));
            $Bedingung = _BedingungErstellen($Param);

            $Bedingung .= ' AND FKA_KEY < ' . floatval($_POST['txtFKA_KEY']);

            $SQL = 'SELECT * FROM (SELECT FKA_KEY FROM FKASSENDATEN  ';
            $SQL .= 'WHERE ' . substr($Bedingung, 4) . ' ';
            $SQL .= 'ORDER BY FKA_KEY DESC) WHERE ROWNUM = 1';

            $rsFKA = $DB->RecordSetOeffnen($SQL);
            if ($rsFKA->FeldInhalt('FKA_KEY') != '')
            {
                $AWIS_KEY1 = $rsFKA->FeldInhalt('FKA_KEY');
            }
            else
            {
                $AWIS_KEY1 = $_POST['txtFKA_KEY'];
            }
        }
        elseif (isset($_POST['cmdDSKasseWeiter_x']))
        { // n�chster DS
            $Param = unserialize($AWISBenutzer->ParameterLesen('FKASuche'));
            $Bedingung = _BedingungErstellen($Param);

            $Bedingung .= ' AND FKA_KEY > ' . floatval($_POST['txtFKA_KEY']);

            $SQL = 'SELECT * FROM (SELECT FKA_KEY FROM FKASSENDATEN  ';
            $SQL .= 'WHERE ' . substr($Bedingung, 4) . ' ';
            $SQL .= 'ORDER BY FKA_KEY) WHERE ROWNUM = 1';

            $rsFKA = $DB->RecordSetOeffnen($SQL);
            if ($rsFKA->FeldInhalt('FKA_KEY') != '')
            {
                $AWIS_KEY1 = $rsFKA->FeldInhalt('FKA_KEY');
            }
            else
            {
                $AWIS_KEY1 = $_POST['txtFKA_KEY'];
            }
        }

        if (isset($_GET['FKAListe']))
        {
            $AWIS_KEY1 = '';
            $AWIS_KEY2 = '';
            $AWIS_HISTKEY = '';
        }

        $Param = unserialize($AWISBenutzer->ParameterLesen("FKASuche"));

        $Bedingung = _BedingungErstellen($Param);

        if (!isset($_GET['Sort']))
        {
            if (isset($Param['ORDERBY']) AND $Param['ORDERBY'] != '')
            {
                $ORDERBY = $Param['ORDERBY'];
            }
            else
            {
                $ORDERBY = ' FKA_KEY';
            }
        }
        else
        {
            $ORDERBY = ' ' . str_replace('~', ' DESC ', $_GET['Sort']);
        }

        $SQL = 'SELECT FKASSENDATEN.* ';
        if ($AWIS_KEY1 <= 0)
        {
            $SQL .= ',row_number() over (order by ' . $ORDERBY . ') AS ZeilenNr ';
        }
        $SQL .= ' FROM FKASSENDATEN ';
        if ($Bedingung != '')
        {
            $SQL .= ' WHERE' . substr($Bedingung, 4);
        }
        $SQL .= ' ORDER BY ' . $ORDERBY;

        $Block = 1;
        if (isset($_REQUEST['FKABlock']))
        {
            $Block = $Form->Format('N0', $_REQUEST['FKABlock'], false);
        }
        elseif (isset($Param['FKABLOCK']) AND $Param['FKABLOCK'] != '')
        {
            $Block = intval($Param['FKABLOCK']);
        }

        $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
        $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
        $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);

        if ($AWIS_KEY1 <= 0)
        {
            $SQL = 'SELECT * FROM (' . $SQL . ')DATEN WHERE ZeilenNr>=' . $StartZeile . ' AND  ZeilenNr<' . ($StartZeile + $ZeilenProSeite);
        }

        $rsFGK = $DB->RecordSetOeffnen($SQL);

        $Param['ORDERBY'] = $ORDERBY;
        $Param['KEY'] = $AWIS_KEY1;
        $Param['FKABLOCK'] = $Block;
        $AWISBenutzer->ParameterSchreiben("FKASuche", serialize($Param));

        if ($rsFGK->AnzahlDatensaetze() > 1 AND $AWIS_KEY1 == 0)
        {      // Liste anzeigen
            $Form->SchreibeHTMLCode('<form name="frmFirstglasDetails" action="./autoglas_Main.php?cmdAktion=Details" method="POST"  enctype="multipart/form-data">');

            $Param['FKABLOCK'] = $Block;
            $Param['KEY'] = '';

            $AWISBenutzer->ParameterSchreiben('FirstglasDetails', $Param);

            $Form->ZeileStart();
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_KEY' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_KEY')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KEY'], 60, '', $Link);
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_FILID'], 60, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_FILID' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_FILID')) ? '~' : '');
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_AEMNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_AEMNR')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_AEMNR'], 150, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_AEMNR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_AEMNR')) ? '~' : '');
            $Link .= '&Sort=FKA_DATUM' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_DATUM')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_DATUM'], 160, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_UHRZEIT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_UHRZEIT')) ? '~' : '');
            $Link .= '&Sort=FKA_UHRZEIT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_UHRZEIT')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_UHRZEIT'], 100, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_UHRZEIT' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_UHRZEIT')) ? '~' : '');
            $Link .= '&Sort=FKA_WANR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_WANR')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_WANR'], 100, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_WANR' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_WANR')) ? '~' : '');
            $Link .= '&Sort=FKA_MENGE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_MENGE')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_MENGE'], 70, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_MENGE' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_MENGE')) ? '~' : '');
            $Link .= '&Sort=FKA_BETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_BETRAG')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_BETRAG'], 100, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_BETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_BETRAG')) ? '~' : '');
            $Link .= '&Sort=FKA_BETRAG' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_BETRAG')) ? '~' : '');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'], 160, '', $Link);
            $Link = './autoglas_Main.php?cmdAktion=Details' . (isset($_GET['FKABlock']) ? '&FKABlock=' . intval($_GET['FKABlock']) : '');
            $Link .= '&Sort=FKA_KFZKENNZ' . ((isset($_GET['Sort']) AND ($_GET['Sort'] == 'FKA_KFZKENNZ')) ? '~' : '');

            $Form->ZeileEnde();

            $FGKZeile = 0;
            while (!$rsFGK->EOF())
            {
                $Form->ZeileStart();
                $Link = './autoglas_Main.php?cmdAktion=Details&FKA_KEY=' . $rsFGK->FeldInhalt('FKA_KEY') . '';
                $Form->Erstelle_ListenFeld('FKA_KEY', $rsFGK->FeldInhalt('FKA_KEY'), 0, 60, false, ($FGKZeile % 2), '', $Link, 'T');
                $Form->Erstelle_ListenFeld('FKA_FILID', $rsFGK->FeldInhalt('FKA_FILID'), 0, 60, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FKA_AEMNR', $rsFGK->FeldInhalt('FKA_AEMNR'), 0, 150, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FKA_DATUM', $rsFGK->FeldInhalt('FKA_DATUM'), 0, 160, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FKA_UHRZEIT', $rsFGK->FeldInhalt('FKA_UHRZEIT'), 0, 100, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FKA_WANR', $rsFGK->FeldInhalt('FKA_WANR'), 0, 100, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FKA_MENGE', $rsFGK->FeldInhalt('FKA_MENGE'), 0, 70, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FKA_BETRAG', $rsFGK->FeldInhalt('FKA_BETRAG'), 0, 100, false, ($FGKZeile % 2), '', '');
                $Form->Erstelle_ListenFeld('FKA_KFZKENNZ', $rsFGK->FeldInhalt('FKA_KFZKENNZ'), 0, 160, false, ($FGKZeile % 2), '', '');

                $Form->ZeileEnde();

                $rsFGK->DSWeiter();
                $FGKZeile++;
            }

            $Link = './autoglas_Main.php?cmdAktion=Details&FKABLOCK&FKAListe=True' . (isset($_GET['Seite']) ? '&Seite=' . $_GET['Seite'] : '');

            $Param['FKAListe'] = 1;
            $Param['CheckSucKassendaten'] = True;
            //Parameter f�r FKAListe schreiben
            $AWISBenutzer->ParameterSchreiben('FKASuche', serialize($Param));

            $Form->SchreibeHTMLCode("<input type='hidden' name='FKABLOCK'>");
            BlaetternKassenDatenZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');

            $Form->SchaltflaechenStart();
            $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
            if (($Recht9100 & 4) == 4 AND !isset($_POST['cmdDSNeu_x']))
            {  // Hinzuf�gen erlaubt?
            }
            $Form->SchaltflaechenEnde();

            $Form->SchreibeHTMLCode('</form>');
        }
        else
        {
            $Form->SchreibeHTMLCode('<form name=frmFirstGlasDetails action=./autoglas_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">');

            if ($AWIS_KEY1 != -1)
            {
                $Param['KEY'] = $rsFGK->FeldInhalt('FKA_KEY');
                $AWISBenutzer->ParameterSchreiben('FirstglasDetails', serialize($Param));
                $Form->Erstelle_HiddenFeld('FKA_KEY', $rsFGK->FeldInhalt('FKA_KEY'));
                $OptionBitteWaehlen = '-1~' . $AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
            }
            else
            {
                $AWIS_KEY1 = -1;
                $AWIS_KEY2 = -1;
            }
            $Felder = array();
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a class=BilderLink href=./autoglas_Main.php?cmdAktion=Details&FKAListe=1 accesskey=T title='" . $AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsFGK->FeldInhalt('FGK_USER'));
            $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $Form->Format('DU', $rsFGK->FeldInhalt('FGK_USERDAT')));
            $Form->InfoZeile($Felder, '');

            $EditRecht = (($Recht9100 & 2) != 0);

            if ($AWIS_KEY1 >= 0)
            {
                $EditRecht = true;

                $rsCheckBoxen = $DB->RecordSetOeffnen($SQL);

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KEY'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_KEY', $rsFGK->FeldInhalt('FKA_KEY'), 20, 50, false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KENNUNG'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_KENNUNG', $rsFGK->FeldInhalt('FKA_KENNUNG'), 10, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_FILID'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_FILID', $rsFGK->FeldInhalt('FKA_FILID'), 5, 100, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_AEMNR'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_AEMNR', $rsFGK->FeldInhalt('FKA_AEMNR'), 50, 50, false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_DATUM'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_DATUM', $rsFGK->FeldInhalt('FKA_DATUM'), 10, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_UHRZEIT'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_UHRZEIT', $rsFGK->FeldInhalt('FKA_UHRZEIT'), 7, 100, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BSA'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_BSA', $rsFGK->FeldInhalt('FKA_BSA'), 30, 100, false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_WANR'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_WANR', $rsFGK->FeldInhalt('FKA_WANR'), 150, 600, false);
                $Form->ZeileEnde();

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_AEMNR'] . ':', 150);

                $Form->Erstelle_HiddenFeld('FKA_OLDAEMNR', $rsFGK->FeldInhalt('FKA_AEMNR'));
                $Form->Erstelle_TextFeld('FKA_AEMNR', $rsFGK->FeldInhalt('FKA_AEMNR'), 13, 600, True);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_ATUNR'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_ATUNR', $rsFGK->FeldInhalt('FKA_ATUNR'), 20, 200, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_MENGE'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_MENGE', $rsFGK->FeldInhalt('FKA_MENGE'), 7, 100, false);
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_BETRAG'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_BETRAG', $rsFGK->FeldInhalt('FKA_BETRAG'), 30, 200, false);
                $Form->ZeileEnde();

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_KFZKENNZ'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_KFZKENNZ', $rsFGK->FeldInhalt('FKA_KFZKENNZ'), 20, 200, false);

                $Form->Trennzeile('O');

                $Form->ZeileStart();
                $Form->Erstelle_TextLabel($AWISSprachKonserven['FKA']['FKA_STATUS'] . ':', 150);
                $Form->Erstelle_TextFeld('FKA_STATUS', $rsFGK->FeldInhalt('FKA_STATUS'), 20, 200, false);
                $Form->ZeileEnde();

                $Form->SchaltflaechenStart();
                $Form->Schaltflaeche('href', 'cmd_zurueck', '../index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
                if (($Recht9100 & 4) == 4)
                {
                    $Form->Schaltflaeche('image', 'cmdSpeichernKassendaten', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
                }
                $Form->Schaltflaeche('image', 'cmdDSKasseZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_DSZurueck'], 'Y');
                $Form->Schaltflaeche('image', 'cmdDSKasseWeiter', '', '/bilder/cmd_dsweiter.png', $AWISSprachKonserven['Wort']['lbl_DSWeiter'], 'X');

                $Form->SchaltflaechenEnde();

                $Form->SchreibeHTMLCode('</form>');
            }
        }
    }

    $Form->Formular_Ende();
}
catch (Exception $ex)
{
    if ($Form instanceof awisFormular)
    {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200809161605");
    }
    else
    {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}

function _BedingungErstellen($ArrParam)
{
    global $AWIS_KEY1;
    global $AWISBenutzer;
    global $flagKassendatenSuchen;
    global $DB;

    $Bedingung = '';

    if ($flagKassendatenSuchen == false)
    {
        if (floatval($AWIS_KEY1) != 0)
        {
            $Bedingung.= ' AND FGK_KEY = ' . $AWIS_KEY1;
            return $Bedingung;
        }

        if (isset($ArrParam['FGK_VORGANGNR']) AND $ArrParam['FGK_VORGANGNR'] != '')
        {
            $Bedingung = " AND FGK_VORGANGNR = " . $DB->FeldInhaltFormat('T', $ArrParam['FGK_VORGANGNR'], true);
        }
        if (isset($ArrParam['FGK_FILID']) AND $ArrParam['FGK_FILID'] != '')
        {
            $Bedingung .= " AND FGK_FILID = " . $DB->FeldInhaltFormat('N0', $ArrParam['FGK_FILID'], true);
        }
        if (isset($ArrParam['FGK_KFZKENNZ']) AND $ArrParam['FGK_KFZKENNZ'] != '')
        {
            $Bedingung .= " AND FGK_KFZKENNZ = " . $DB->FeldInhaltFormat('T', $ArrParam['FGK_KFZKENNZ'], true) . " ";
        }
    }

    if ($flagKassendatenSuchen == true)
    {
        if (floatval($AWIS_KEY1) != 0)
        {
            $Bedingung.= ' AND FKA_KEY = ' . $AWIS_KEY1;
            return $Bedingung;
        }

        if (isset($ArrParam['FGK_VORGANGNR']) AND $ArrParam['FGK_VORGANGNR'] != '')
        {
            $Bedingung = " AND FKA_AEMNR = " . $DB->FeldInhaltFormat('T', $ArrParam['FGK_VORGANGNR'], true);
        }
        if (isset($ArrParam['FGK_FILID']) AND $ArrParam['FGK_FILID'] != '')
        {
            $Bedingung .= " AND FKA_FILID = " . $DB->FeldInhaltFormat('N0', $ArrParam['FGK_FILID'], true);
        }
        if (isset($ArrParam['FGK_KFZKENNZ']) AND $ArrParam['FGK_KFZKENNZ'] != '')
        {
            $Bedingung .= " AND FKA_KFZKENNZ = " . $DB->FeldInhaltFormat('T', $ArrParam['FGK_KFZKENNZ'], true) . " ";
        }

        if (isset($ArrParam['FGK_WANR']) AND $ArrParam['FGK_WANR'] != '')
        {
            $Bedingung .= " AND FKA_WANR =  " . $DB->FeldInhaltFormat('T', $ArrParam['FGK_WANR'], true) . " ";
        }

        if ($ArrParam['FGK_DATUMVON'] != "" && $ArrParam['FGK_DATUMBIS'] != "")
        {
            $Bedingung .= " AND FKA_DATUM >=  " . $DB->FeldInhaltFormat('DU', $ArrParam['FGK_DATUMVON'], true) . "AND FKA_DATUM <= " . $DB->FeldInhaltFormat('DU', $ArrParam['FGK_DATUMBIS'], true) . " ";
        }
    }
    return $Bedingung;
}

function BlaetternKassenDatenZeile($AnzDS, $ZeilenProSeite, $Link, $Block, $ToolTippText='')
{
    if ($AnzDS <= $ZeilenProSeite)
    {
        return;
    }

    $LinkW = $LinkZ = $Link;

    if ($Block > 1)
    {
        $LinkZ .= ('&FKABlock=' . ($Block - 1));
        echo '<a href="' . $LinkZ . '">&lt;&lt;</a>';
    }

    // Auswahlfeld
    echo '<select name="FKABlock" ';
    if ($ToolTippText != '')
    {
        echo ' title="' . $ToolTippText . '"';
    }
    echo ' onchange="submit();" >';
    for ($i = 1; $i < (($AnzDS / $ZeilenProSeite) + 1); $i++)
    {
        echo '<option ' . ($Block == $i ? 'selected' : '') . ' value="' . $i . '">' . $i . '</option>';
    }
    echo '</select>';


    if ($AnzDS > ($Block * $ZeilenProSeite))
    {
        // Weiter
        if ($Block == 0)
        {
            $LinkW .= '&FKABlock=2';
        }
        else
        {
            $LinkW .= ('&FKABlock=' . ($Block + 1));
        }
        echo '<a href="' . $LinkW . '">&gt;&gt;</a>';
    }
}

?>