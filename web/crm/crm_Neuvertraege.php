<?php
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $awisRSZeilen;

$TextKonserven = array();
$TextKonserven[]=array('CGK','%');
$TextKonserven[]=array('Liste','lst_JaNein');
$TextKonserven[]=array('Fehler','err_keineDatenbank');
$TextKonserven[]=array('Fehler','err_keineRechte');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('CWV','CWV_KON_KEY');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('FIL','FIL_ID');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_speichern');


$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3712 = awisBenutzerRecht($con,3712,$AWISBenutzer->BenutzerName());
if($Recht3712==0)
{
    awisEreignis(3,1000,'CRM-Neuvertraege',$AWISBenutzer->BenutzerName(),'','','');
    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}


awis_FORM_FormularStart();
echo '<form name=frmNeuvertraege method=post action=./crm_Main.php?cmdAktion=Neuvertraege>';

$EditRecht=(($Recht3712&6)!=0);

$Param = explode(';',awis_BenutzerParameter($con, 'SucheNeueVertraege', $AWISBenutzer->BenutzerName()));
//$Param = array();

if(isset($_POST['cmdSuche_x']))
{
	$Param = $_POST['sucSucheBetreuer'].';'.$_POST['sucFIL_ID'];
	$Param = explode(';',$Param);
}
elseif(isset($_GET['CGK_BETREUER_ID']))
{
	$Param[0]=$_GET['CGK_BETREUER_ID'];
}

if(isset($_GET['CGKListe']))
{
	$Param = explode(';',awis_BenutzerParameter($con,'SucheNeueVertraege',$AWISBenutzer->BenutzerName()));
}

awis_Debug(1,$Param);
awis_FORM_ZeileStart();

if(($Recht3712&2)==2)	// auch fremde anzeigen
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CWV']['CWV_KON_KEY'].'',150);

	$SQL = 'SELECT DISTINCT COALESCE(CGK_BETREUER_ID,-1) AS ID , COALESCE(CGK_BETREUER,\'::ohne Betreuer::\') AS CGK_BETREUER';
	$SQL .= ' FROM CRMGewerbekunden ';
	$SQL .= ' LEFT OUTER JOIN CRMAkquiseStand ON TO_NUMBER(CAQ_VERTRAGSNUMMER) = TO_NUMBER(CGK_ACCOUNT_ID)';
	$SQL .= ' WHERE CAQ_KEY IS NULL';
	$SQL .= ' AND CGK_DBM_KARTENART = \'HK\'';
	$SQL .= ' AND CGK_ACCOUNT_STATUS <> 2';				// Nicht die gekündigten
	$SQL .= ' AND cgk_betreuerfilter<>1';				// Nicht die gefilterten Betreuer
/*	$SQL .= ' AND CGK_BETREUER_ID <> 300789';			// Frau Bock nicht anzeigen!!
	$SQL .= ' AND CGK_BETREUER NOT LIKE (\'%(301656)%\')';			// Hr. Handschuh nicht anzeigen!!
	$SQL .= ' AND CGK_BETREUER NOT LIKE (\'%(301726)%\')';			// Hr. Bensch nicht anzeigen!!	
	$SQL .= ' AND CGK_BETREUER NOT LIKE (\'%(302008)%\')';			// Hr. Carsten Winkler nicht anzeigen!!	
*/	$SQL .= ' ORDER BY CGK_BETREUER';
	
	awis_FORM_Erstelle_SelectFeld('*SucheBetreuer',(isset($Param[0])?$Param[0]:''),200,true,$con,$SQL,'~'.$AWISSprachKonserven['Wort']['Auswahl_ALLE']);
	awis_FORM_ZeileEnde();
}
if(($Recht3712&2)==2)	// auch fremde anzeigen
{
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_ID'].'',150);

	awis_FORM_Erstelle_TextFeld('*FIL_ID','',5,100,true,'','','','N');
	awis_FORM_ZeileEnde();
}


awis_FORM_SchaltflaechenStart();
awis_FORM_Schaltflaeche('image', 'cmdSuche', '', '/bilder/eingabe_ok.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
awis_FORM_SchaltflaechenEnde();

echo '</form>';

awis_FORM_Trennzeile();

//***********************************
//Kontakte anzeigen
//***********************************
$CGKBetreuerID = '';
$MaxDSAnzahl = awis_BenutzerParameter($con,'AnzahlDatensaetzeProListe',$AWISBenutzer->BenutzerName());

/*
$SQL = 'SELECT CRMGewerbekunden.*';
$SQL .= ' FROM CRMGewerbekunden ';
$SQL .= ' LEFT OUTER JOIN CRMAkquiseStand ON CAQ_VERTRAGSNUMMER = CGK_ACCOUNT_ID';
$SQL .= ' WHERE CAQ_KEY IS NULL';
$SQL .= ' AND CGK_DBM_KARTENART = \'HK\'';
$SQL .= ' AND CGK_ACCOUNT_STATUS <> 2';				// Nicht die gekündigten
$SQL .= ' AND cgk_betreuerfilter<>1';				// Nicht die gefilterten Betreuer
/*$SQL .= ' AND CGK_BETREUER_ID <> 300789';			// Frau Bock nicht anzeigen!!
$SQL .= ' AND CGK_BETREUER NOT LIKE (\'%(301656)%\')';			// Hr. Handschuh nicht anzeigen!!
$SQL .= ' AND CGK_BETREUER NOT LIKE (\'%(301726)%\')';			// Hr. Bensch nicht anzeigen!!
$SQL .= ' AND CGK_BETREUER NOT LIKE (\'%(302008)%\')';			// Hr. Carsten Winkler nicht anzeigen!!
*/

// Neue Abfrage, um die Performance zu optimieren
// 26.7.2011, SK
$SQL = "SELECT * FROM (
SELECT CGK_KEY,CGK_ACCOUNT_ID
FROM CRMGewerbekunden 
WHERE 
CGK_DBM_KARTENART = 'HK' 
AND CGK_ACCOUNT_STATUS <> 2 
AND cgk_betreuerfilter<>1";
 


/*if(isset($_POST['sucSucheBetreuer']))
{
	$SQL .= ' AND CGK_BETREUER_ID = 0'.$_POST['sucSucheBetreuer'];
	$CGKBetreuerID = $_POST['sucSucheBetreuer'];
}*/
if(isset($Param[0]) AND $Param[0]=='-1')
{
	$SQL .= ' AND CGK_BETREUER_ID IS NULL';
	$CGKBetreuerID = $Param[0];
}
elseif(isset($Param[0]) AND $Param[0]!='')
{
	$SQL .= ' AND CGK_BETREUER_ID = 0'.$Param[0];
	$CGKBetreuerID = $Param[0];
}
else
{
	$SQL .= ' AND CGK_BETREUER_ID is not null';
}
/*
if(isset($_GET['CGK_BETREUER_ID']))
{
	$SQL .= ' AND CGK_BETREUER_ID = 0'.intval($_GET['CGK_BETREUER_ID']);
	$CGKBetreuerID = intval($_GET['CGK_BETREUER_ID']);
}
*/

if(isset($_GET['CGKKEY']))
{
	$SQL .= ' AND CGK_KEY = 0'.intval($_GET['CGKKEY']);
}

$SQL .= ") DATEN
LEFT OUTER JOIN CRMAkquiseStand ON CAQ_VERTRAGSNUMMER = CGK_ACCOUNT_ID 
LEFT OUTER JOIN CRMGewerbekunden ON DATEN.CGK_KEY = CRMGewerbekunden.CGK_KEY
WHERE CAQ_KEY IS NULL
";



if(!isset($_GET['CGKSort']))
{
	if(isset($Param[2]) AND $Param[2]!='')
	{
		$SQL .= ' ORDER BY '.$Param[2];
	}
	else
	{
		$SQL .= ' ORDER BY CGK_CUST_ID DESC';
		$Param[2]='CGK_CUST_ID';
	}
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['CGKSort']);
	$Param[2]=str_replace('~',' DESC ',$_GET['CGKSort']);
}

awis_Debug(1,$SQL);
$rsCGK = awisOpenRecordset($con,$SQL);
$rsCGKZeilen = $awisRSZeilen;

if(!isset($_GET['CGKKEY']) OR isset($_GET['CGKListe']))					// Liste anzeigen
{
	awis_FORM_ZeileStart();

	$Link = './crm_Main.php?cmdAktion=Neuvertraege';
	$Link .= '&CGKSort=CGK_ACCOUNT_ID'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_ACCOUNT_ID'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_ACCOUNT_ID'],240,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Neuvertraege';
	$Link .= '&CGKSort=CGK_ASSOCIATION'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_ASSOCIATION'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_ASSOCIATION'],120,'',$Link,$AWISSprachKonserven['CGK']['CGK_ASSOCIATION']);
	$Link = './crm_Main.php?cmdAktion=Neuvertraege';
	$Link .= '&CGKSort=CGK_OPAL_KARTENART'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_OPAL_KARTENART'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_OPAL_KARTENART'],30,'',$Link,$AWISSprachKonserven['CGK']['CGK_OPAL_KARTENART']);
	$Link = './crm_Main.php?cmdAktion=Neuvertraege';
	$Link .= '&CGKSort=CGK_NACHNAME'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_NACHNAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_NACHNAME'],220,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Neuvertraege';
	$Link .= '&CGKSort=CGK_FIRMA1'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_FIRMA1'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_FIRMA1'],300,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Neuvertraege';
	$Link .= '&CGKSort=CGK_EMBOSSINGNAME'.((isset($_GET['CGKSort']) AND ($_GET['CGKSort']=='CGK_EMBOSSINGNAME'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CGK']['CGK_EMBOSSINGNAME'],200,'',$Link);

	awis_FORM_ZeileEnde();

		// Blockweise
	$StartZeile=0;
	if(isset($_GET['Block']))
	{
		$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
	}

		// Seitenweises blaettern
	if($rsCGKZeilen>$MaxDSAnzahl)
	{
		awis_FORM_ZeileStart();
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['Wort']['Seite'],50,'');

		for($i=0;$i<($rsCGKZeilen/$MaxDSAnzahl);$i++)
		{
			if($i!=($StartZeile/$MaxDSAnzahl))
			{
				$Text = '&nbsp;<a href=./crm_Main.php?cmdAktion=Neuvertraege'.($CGKBetreuerID !=''?'&CGK_BETREUER_ID='.$CGKBetreuerID:'').'&Block='.$i.(isset($_GET['Sort'])?'&Sort='.$_GET['Sort']:'').'>'.($i+1).'</a>';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
			else
			{
				$Text = '&nbsp;'.($i+1).'';
				awis_FORM_Erstelle_TextLabel($Text,30,'');
			}
		}
		awis_FORM_ZeileEnde();
	}

	for($CGKZeile=$StartZeile;$CGKZeile<$rsCGKZeilen and $CGKZeile<$StartZeile+$MaxDSAnzahl;$CGKZeile++)
	{
		awis_FORM_ZeileStart();
		$Icons = array();
		$Icons[] = array('edit','./crm_Main.php?cmdAktion=Neuvertraege&CGKKEY='.$rsCGK['CGK_KEY'][$CGKZeile]);
		awis_FORM_Erstelle_ListeIcons($Icons,20,($CGKZeile%2));

		$ToolTipp='';
		$ToolTipp=(isset($rsCGK['CGK_LOCK_REASON'][0])?$AWISSprachKonserven['CGK']['CGK_CARDLOCKREASON_TEXT'].': '.$rsCGK['CGK_CARDLOCKREASON_TEXT'][0]:'');
		$ToolTipp.=($rsCGK['CGK_CARDBLOCKSTATUS'][$CGKZeile]>1?($ToolTipp==''?'':'/').$AWISSprachKonserven['CGK']['CGK_CARDBLOCKSTATUS_TEXT'].': '.$rsCGK['CGK_CARDBLOCKSTATUS_TEXT'][$CGKZeile]:'');
		awis_FORM_Erstelle_ListenFeld('#CGK_ACCOUNT_ID',$rsCGK['CGK_ACCOUNT_ID'][$CGKZeile],20,220,false,($CGKZeile%2),($ToolTipp==''?'':'color:red;'),'','T','',$ToolTipp);
		$Link = ($rsCGK['CGK_ASSOCIATIONKEY'][$CGKZeile]!=''?'./crm_Main.php?cmdAktion=Details&CAQ_VERTRAGSNUMMER='.$rsCGK['CGK_ASSOCIATIONKEY'][$CGKZeile]:'');
		awis_FORM_Erstelle_ListenFeld('#CGK_ASSOCIATIONKEY',$rsCGK['CGK_ASSOCIATIONKEY'][$CGKZeile],20,120,false,($CGKZeile%2),($ToolTipp==''?'':'color:red;'),$Link,'T','',$ToolTipp);
		awis_FORM_Erstelle_ListenFeld('#CGK_OPAL_KARTENART',$rsCGK['CGK_OPAL_KARTENART'][$CGKZeile],20,30,false,($CGKZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CGK_NACHNAME',$rsCGK['CGK_NACHNAME'][$CGKZeile],20,220,false,($CGKZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CGK_FIRMA1',$rsCGK['CGK_FIRMA1'][$CGKZeile],20,300,false,($CGKZeile%2),'','','T');
		awis_FORM_Erstelle_ListenFeld('#CGK_EMBOSSINGNAME',$rsCGK['CGK_EMBOSSINGNAME'][$CGKZeile],20,200,false,($CGKZeile%2),'','','T');
		awis_FORM_ZeileEnde();
	}
	awis_FORM_FormularEnde();
}
else 		// Einer oder keiner
{
	echo '<form name=frmCRMAdressen action=./crm_Main.php?cmdAktion=Details method=POST  enctype="multipart/form-data">';
	
	awis_FORM_FormularStart();

	echo '<input name=txtCGK_KEY type=hidden value=0'.(isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_KEY'][0]:'').'>';
	echo '<input name=txtCGK_CGK_KEY type=hidden value=0'.$AWIS_KEY1.'>';

	// Gleich den Betreuer mit eintragen
	echo '<input name=txtBETREUER_ID type=hidden value=\''.$Param[0].'\'>';
	
	
	$AWIS_KEY2 = (isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_KEY'][0]:'0');
		// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Neuvertraege&CGKListe=1 accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY2===0?'':(isset($rsCGK['CGK_KEY'][0])?$rsCGK['CGK_AC_DBM_UPDATE_DT'][0]:'')));
	awis_FORM_InfoZeile($Felder,'');

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_BETREUER'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_BETREUER',(isset($rsCGK['CGK_BETREUER'][0])?$rsCGK['CGK_BETREUER'][0]:''),10,500,false,'','','','T');
	if($rsCGK['CGK_ACCOUNT_TY'][0] == 20)
	{
		awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['txt_EUROSHELL'],160);
	}
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ACCOUNT_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_ACCOUNT_ID',(isset($rsCGK['CGK_ACCOUNT_ID'][0])?$rsCGK['CGK_ACCOUNT_ID'][0]:''),10,200,false,'','','','T');
	echo '<input name=txtCGK_ACCOUNT_ID type=hidden value=\''.$rsCGK['CGK_ACCOUNT_ID'][0].'\'>';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CUST_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CUST_ID',(isset($rsCGK['CGK_CUST_ID'][0])?$rsCGK['CGK_CUST_ID'][0]:''),10,200,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ASSOCIATIONKEY'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_ASSOCIATIONKEY',(isset($rsCGK['CGK_ASSOCIATIONKEY'][0])?$rsCGK['CGK_ASSOCIATIONKEY'][0]:''),10,200,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ASSOCIATION'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_ASSOCIATION',(isset($rsCGK['CGK_ASSOCIATION'][0])?$rsCGK['CGK_ASSOCIATION'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARD_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CARD_ID',(isset($rsCGK['CGK_CARD_ID'][0])?$rsCGK['CGK_CARD_ID'][0]:''),10,200,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_DBM_KARTENART'].':',100);
	awis_FORM_Erstelle_TextFeld('*CGK_DBM_KARTENART',(isset($rsCGK['CGK_DBM_KARTENART'][0])?$rsCGK['CGK_DBM_KARTENART'][0]:''),10,200,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_NACHNAME'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_NACHNAME',(isset($rsCGK['CGK_NACHNAME'][0])?$rsCGK['CGK_NACHNAME'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_NACHNAME type=hidden value=\''.$rsCGK['CGK_NACHNAME'][0].'\'>';
	awis_FORM_Erstelle_Checkbox('alsap', 'off', 20, true, 'on');
	awis_FORM_Erstelle_TextLabel('als Ansprechpartner anlegen',300);
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_VORNAME'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_VORNAME',(isset($rsCGK['CGK_VORNAME'][0])?$rsCGK['CGK_VORNAME'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_VORNAME type=hidden value=\''.$rsCGK['CGK_VORNAME'][0].'\'>';
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_BIRTH_DT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_BIRTH_DT',(isset($rsCGK['CGK_BIRTH_DT'][0])?$rsCGK['CGK_BIRTH_DT'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_BIRTH_DT type=hidden value=\''.$rsCGK['CGK_BIRTH_DT'][0].'\'>';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRMA1'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_FIRMA1',(isset($rsCGK['CGK_FIRMA1'][0])?$rsCGK['CGK_FIRMA1'][0]:''),10,400,false,'','','','T');		
	echo '<input name=txtCGK_FIRMA1 type=hidden value=\''.$rsCGK['CGK_FIRMA1'][0].'\'>';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRMA2'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_FIRMA2',(isset($rsCGK['CGK_FIRMA2'][0])?$rsCGK['CGK_FIRMA2'][0]:''),10,400,false,'','','','T');		
	echo '<input name=txtCGK_FIRMA2 type=hidden value=\''.$rsCGK['CGK_FIRMA2'][0].'\'>';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRMA3'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_FIRMA3',(isset($rsCGK['CGK_FIRMA3'][0])?$rsCGK['CGK_FIRMA3'][0]:''),10,400,false,'','','','T');		
	echo '<input name=txtCGK_FIRMA3 type=hidden value=\''.$rsCGK['CGK_FIRMA3'][0].'\'>';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CUSTOMERGROUP'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CUSTOMERGROUP',(isset($rsCGK['CGK_CUSTOMERGROUP'][0])?$rsCGK['CGK_CUSTOMERGROUP'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_CUSTOMERGROUP type=hidden value=\''.$rsCGK['CGK_CUSTOMERGROUP'][0].'\'>';
	awis_FORM_Erstelle_Checkbox('inverbinfo', 'off', 20, true, 'on');
	awis_FORM_Erstelle_TextLabel('in Verbandsinfo &uuml;bernehmen',300);
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_STRASSE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_STRASSE',(isset($rsCGK['CGK_STRASSE'][0])?$rsCGK['CGK_STRASSE'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_STRASSE type=hidden value=\''.$rsCGK['CGK_STRASSE'][0].'\'>';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_HAUSNUMMER'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_HAUSNR',(isset($rsCGK['CGK_HAUSNR'][0])?$rsCGK['CGK_HAUSNR'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_HAUSNR type=hidden value=\''.$rsCGK['CGK_HAUSNR'][0].'\'>';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PLZ'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PLZ',(isset($rsCGK['CGK_PLZ'][0])?$rsCGK['CGK_PLZ'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_PLZ type=hidden value=\''.$rsCGK['CGK_PLZ'][0].'\'>';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_ORT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_ORT',(isset($rsCGK['CGK_ORT'][0])?$rsCGK['CGK_ORT'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_ORT type=hidden value=\''.$rsCGK['CGK_ORT'][0].'\'>';
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PHONE_H'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PHONE_H',(isset($rsCGK['CGK_PHONE_H'][0])?$rsCGK['CGK_PHONE_H'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_PHONE_H type=hidden value=\''.$rsCGK['CGK_PHONE_H'][0].'\'>';
	awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FAX'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_FAX',(isset($rsCGK['CGK_FAX'][0])?$rsCGK['CGK_FAX'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_FAX type=hidden value=\''.$rsCGK['CGK_FAX'][0].'\'>';
	awis_FORM_ZeileEnde();

    awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_EMAIL'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_EMAIL',(isset($rsCGK['CGK_EMAIL'][0])?$rsCGK['CGK_EMAIL'][0]:''),10,400,false,'','','','T');
	echo '<input name=txtCGK_EMAIL type=hidden value=\''.$rsCGK['CGK_EMAIL'][0].'\'>';
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_PLATE_NUMBER'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_PLATE_NUMBER',(isset($rsCGK['CGK_PLATE_NUMBER'][0])?$rsCGK['CGK_PLATE_NUMBER'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_EMBOSSINGNAME'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_EMBOSSINGNAME',(isset($rsCGK['CGK_EMBOSSINGNAME'][0])?$rsCGK['CGK_EMBOSSINGNAME'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CATEGORY'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CATEGORY',(isset($rsCGK['CGK_CATEGORY'][0])?$rsCGK['CGK_CATEGORY'][0]:''),10,400,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_FIRSTTRANSACTIONDATE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_FIRSTTRANSACTIONDATE',(isset($rsCGK['CGK_FIRSTTRANSACTIONDATE'][0])?$rsCGK['CGK_FIRSTTRANSACTIONDATE'][0]:''),10,120,false,'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LASTTRANSACTIONDATE'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_LASTTRANSACTIONDATE',(isset($rsCGK['CGK_LASTTRANSACTIONDATE'][0])?$rsCGK['CGK_LASTTRANSACTIONDATE'][0]:''),10,120,false,'','','','D');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARDLIMIT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CARDLIMIT',(isset($rsCGK['CGK_CARDLIMIT'][0])?$rsCGK['CGK_CARDLIMIT'][0]:''),10,120,false,'','','','N0');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_SALES_STORE_ID'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_SALES_STORE_ID',(isset($rsCGK['CGK_SALES_STORE_ID'][0])?$rsCGK['CGK_SALES_STORE_ID'][0]:''),10,120,false,'','','','T');
	$BindeVariablen=array();
	$BindeVariablen['var_N0_fil_id']=intval('0'.(isset($rsCGK['CGK_SALES_STORE_ID'][0])?$rsCGK['CGK_SALES_STORE_ID'][0]:''));
	$SQL = 'SELECT FIL_BEZ FROM FILIALEN WHERE FIL_ID=:var_N0_fil_id';
	$rsFIL = awisOpenRecordset($con, $SQL, true, false, $BindeVariablen);
	awis_FORM_Erstelle_TextFeld('*FIL_BEZ',(isset($rsFIL['FIL_BEZ'][0])?$rsFIL['FIL_BEZ'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();

	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LOCK_DT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_LOCK_DT',(isset($rsCGK['CGK_LOCK_DT'][0])?$rsCGK['CGK_LOCK_DT'][0]:''),10,120,false,'','','','D');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_LOCK_REASON'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_LOCK_REASON',(isset($rsCGK['CGK_LOCK_REASON'][0])?$rsCGK['CGK_LOCK_REASON'][0]:''),10,120,false,'','','','T');
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CGK']['CGK_CARDLOCKREASON_TEXT'].':',160);
	awis_FORM_Erstelle_TextFeld('*CGK_CARDLOCKREASON_TEXT',(isset($rsCGK['CGK_CARDLOCKREASON_TEXT'][0])?$rsCGK['CGK_CARDLOCKREASON_TEXT'][0]:''),10,300,false,'','','','T');
	awis_FORM_ZeileEnde();

    echo '<input name=txtINFO_Vertragsstatus type=hidden value=\''.$rsCGK['CGK_ACCOUNTSTATUS_TEXT'][0].'\'>';
    echo '<input name=txtINFO_Kartensperren type=hidden value=\''.$rsCGK['CGK_ACCTBLOCKST_TEXT'][0].'\'>';
    echo '<input name=txtINFO_Poststatus type=hidden value=\''.$rsCGK['CGK_POST_STATUS'][0].'\'>';
    echo '<input name=txtINFO_Vertragssperren type=hidden value=\''.$rsCGK['CGK_CARDBLOCKSTATUS_TEXT'][0].'\'>';
    
    // bei EuroShell-Kunden soll beim speichern automatisch das Ranking EuroShell mit gespeichert werden
    // daher wird das Feld cgk_account_ty (20 = EuroShell) gebraucht
    echo '<input name=txtCGK_ACCOUNT_TY type=hidden value=\'' . $rsCGK['CGK_ACCOUNT_TY'][0].'\'>';

	awis_FORM_FormularEnde();
	
	
	//***************************************
	// Schaltflaechen fuer dieses Register
	//***************************************
	if (($Recht3712&8)==8)
	{
		awis_FORM_SchaltflaechenStart();	
		awis_FORM_Schaltflaeche('image', 'cmdSpeichernV', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		awis_FORM_SchaltflaechenEnde();
	}		

	echo '</form>';
}
awis_BenutzerParameterSpeichern($con,'SucheNeueVertraege',$AWISBenutzer->BenutzerName(),implode(';',$Param));

?>