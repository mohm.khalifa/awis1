<?php
// Variablen
global $awisDBFehler;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 501);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
   die("Keine ausreichenden Rechte!");
}

if(isset($_POST['cmdSuche_x']) AND $_POST['cmdSuche_x']!='') 		// Neue suche
{
	$Param = $_POST['txtOEN_NUMMER'];		// 0
	$Param .= ';' . $_POST['txtKBA_NR'];	// 1
	$Param .= ';' . $_POST['txtFIL_ID'];	// 2
	$Param .= ';' . $_POST['txtLFDNR'];	// 3
	$Param .= ';' . $_POST['txtFormat'];	// 4
	$Param .= ';' . $_POST['txtDatumVom'];	// 5
	$Param .= ';' . $_POST['txtDatumBis'];	// 6
	$Param .= ';' . $_POST['txtFIL_ID_Bis'];	// 7
	$Param .= ';' . $_POST['txtBearbeitungsgruppe'];	// 8

	awis_BenutzerParameterSpeichern($con,'AktuelleFremdkaufSuche' , $AWISBenutzer->BenutzerName(), $Param);
	$Param = explode(';',$Param);
}
elseif(isset($_GET['OEN_NUMMER']))	// OEN_Nummer wurde �bergeben
{
	$Param = array($_GET['OEN_NUMMER'],'','','','');
}
else	// Letzte gespeicherte Suche
{
	$Param = explode(';',awis_BenutzerParameter($con,'AktuelleFremdkaufSuche' , $AWISBenutzer->BenutzerName()));
}

$SQL = 'SELECT DISTINCT V_FREMDKAEUFE_MIT_KFZ_STL_AWST.* ';
If($Param[8]!='')		// Bearbeitungsgruppe
{
	$SQL .= ',KFZ_HERSTID, HER_BEARBEITUNGSGRUPPE';
}

$SQL .= ' FROM V_FREMDKAEUFE_MIT_KFZ_STL_AWST ';
If($Param[8]!='')		// Bearbeitungsgruppe
{
	$SQL .= ' LEFT OUTER JOIN Hersteller ON HER_ID = KFZ_HERSTID';
}
$Bedingung = '';

If($Param[0]!='')		// OEN-Nummer
{
	$Bedingung .= 'AND FRK_OEN_SUCHNUMMER ' . awisLIKEoderIST($Param[0],true,false,false,2);
}

If($Param[1]!='')		// KBA-Nummer
{
	$Bedingung .= "AND FRK_KBA = '" . $Param[1] . "'";
}

If($Param[2]!='' OR $Param[7]!='')		// Filialen
{
	if($Param[2]!='' AND $Param[7]=='')
	{
		$Bedingung .= "AND FRK_FIL_ID = '" . $Param[2] . "'";
	}
	else
	{
		if($Param[2]!='')
		{
			$Bedingung .= " AND FRK_FIL_ID >= '" . $Param[2] . "'";
		}
		if($Param[7]!='')
		{
			$Bedingung .= " AND FRK_FIL_ID <= '" . $Param[7] . "'";
		}
	}
}

If($Param[3]!='')		// Lfd-Nr
{
	$Bedingung .= "AND FRK_LFDNR = '" . $Param[3] . "'";
}
If($Param[5]!='')		// DatumVom
{
	If($Param[6]!='')		// DatumBis
	{
		$Bedingung .= "AND (TO_DATE(FRK_DATUM,'dd.mm.yy') >= '" . awis_format($Param[5], 'OracleDatum') . "'";
		$Bedingung .= "AND TO_DATE(FRK_DATUM,'dd.mm.yy') <= '" . awis_format($Param[6], 'OracleDatum') . "')";
	}
	else
	{
		$Bedingung .= "AND (TO_DATE(FRK_DATUM,'dd.mm.yy') = '" . awis_format($Param[5], 'OracleDatum') . "')";
	}
}

if(isset($_GET['Filter']) AND $_GET['Filter']=='Anwend')
{
	$Bedingung .= " AND (AWSTNR IS NULL AND ST_AWSTNR IS NULL)";
}
If($Param[8]!='')		// Bearbeitungsgruppe
{
	$Bedingung .= "AND HER_BEARBEITUNGSGRUPPE = '" . $Param[8] . "'";
}

if($_REQUEST['cmdAktion']=='EKAT')
{
	// lt. Schmidtberger, 12.11.2009
	$Bedingung .= ' AND (FRK_KBA <> \'9999999\'AND FRK_KBA <> \'0\')';
	
	// lt. Schmidtberger, (e-mail vom 12.03.2010) entfernt
	//$Bedingung .= ' AND AWSTNR IS NULL';
}

if($Bedingung=='')		// Keine Kriterien -> w�rde alle Daten liefern-> ABBRUCH
{
	echo '<span class=HinweisText>Sie haben keine Kriterien angegeben.</span>';
	awisLogoff($con);
	die();
}

$SQL .= ' WHERE ' . substr($Bedingung,4);
//$SQL .= $Bedingung;

if(isset($_GET['Sort']) AND $_GET['Sort']!='')
{
	$SQL .= ' ORDER BY ' . $_GET['Sort'];
}
else
{
	$SQL .= ' ORDER BY FRK_DATUM, FRK_FIL_ID, FRK_LFDNR DESC';
}

//
awis_Debug(1,$SQL);
$rsOEP = awisOpenRecordset($con, $SQL);
if($rsOEP==FALSE)
{
	awisErrorMailLink("artikel_Fremdkauf.php", 2, $awisDBFehler['message'],'SQL:' . $SQL);
}
$rsOEPZeilen=$awisRSZeilen;

$AnzFK=0;
$Doppelte = array();

If($Param[0]!='')		// OEN-Nummer
{
	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		$SuchKey = $rsOEP['FRK_FIL_ID'][$OEPZeile] . '-' . $rsOEP['FRK_LFDNR'][$OEPZeile];
		if(array_search($SuchKey, $Doppelte)===FALSE)
		{
			$Dat = mktime(0,0,0,substr($rsOEP['FRK_DATUM'][$OEPZeile],3,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],0,2),substr($rsOEP['FRK_DATUM'][$OEPZeile],6)+1);
			if($Dat > time())
			{
				$AnzFK+=$rsOEP['FRK_MENGE'][$OEPZeile];
			}
			$AnzFKGesamt+=$rsOEP['FRK_MENGE'][$OEPZeile];
			array_push($Doppelte, $SuchKey);
		}
	}
	echo 'Fremdk�ufe gesamt: ' . $AnzFKGesamt . '. Davon in den letzten 12 Monaten: ' . $AnzFK;
}
else	// Nur Zeilenanzahl, wenn keine OENummer gesucht wurde
{
	echo 'Es wurden ' . $rsOEPZeilen . ' Zeilen gefunden';
}

if((isset($_POST['txtFormat']) AND $_POST['txtFormat']=='liste')
	OR (!isset($_POST['txtFormat']) or $_POST['txtFormat']==''))		// Liste auf dem Bildschirm
{

	print "<table id=DatenTabelle width=100% border=1>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_FIL_ID" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_FIL_ID'?'%20DESC':'') . ">Filiale</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_DATUM" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_DATUM'?'%20DESC':'') . ">Datum</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_LFDNR" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_LFDNR'?'%20DESC':'') . ">Lfd.Nr</a></td>";
	print "<td id=FeldBez>WstNr</td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_KBA" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_KBA'?'%20DESC':'') . ">KBA-Nr</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_MENGE" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_MENGE'?'%20DESC':'') . ">Menge</a></td>";
	print "<td id=FeldBez>Grund</td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_OEN_NUMMER" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_OEN_NUMMER'?'%20DESC':'') . ">OE-Nr</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_ARTBEZ" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_ARTBEZ'?'%20DESC':'') . ">Artikel</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=ATUNR_SUCH" . (isset($_GET['Sort']) && $_GET['Sort']=='ATUNR_SUCH'?'%20DESC':'') . ">ATU-Nr</a></td>";
	print "<td id=FeldBez><a href=./fremdkauf_Main.php?cmdAktion=EKAT".(isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'&Filter=Anwend':'')."&Sort=FRK_PREIS" . (isset($_GET['Sort']) && $_GET['Sort']=='FRK_PREIS'?'%20DESC':'') . ">Preis</a></td>";
	print "<td id=FeldBez>Rabatt</td>";

	if(isset($_GET['Filter']) AND $_GET['Filter']=='Anwend')
	{
		print "<td id=FeldBez>Anwendungstellen (<a href=./fremdkauf_Main.php?cmdAktion=EKAT>alle anzeigen</a>)</td>";
	}
	else
	{
		print "<td id=FeldBez>Anwendungstellen (<a href=./fremdkauf_Main.php?cmdAktion=EKAT&Filter=Anwend" . (isset($_GET['Filter']) && $_GET['Filter']=='Anwend'?'%20DESC':'') . ">leere anzeigen</a>)</td>";
	}
	print "</tr>";

	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{

		echo '<tr>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&FIL_ID=' .$rsOEP['FRK_FIL_ID'][$OEPZeile].'>'	. $rsOEP['FRK_FIL_ID'][$OEPZeile] . '</a></td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_DATUM'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&LFDNR=' . $rsOEP['FRK_LFDNR'][$OEPZeile] . '&FIL_ID=' . $rsOEP['FRK_FIL_ID'][$OEPZeile] . '>' . $rsOEP['FRK_LFDNR'][$OEPZeile] . '</a></td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_WSTNR'][$OEPZeile] . '</td>';

		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'><a href=../auswertungen/fremdkauf_Main.php?cmdAktion=Suche&KBA_NR=' . $rsOEP['FRK_KBA'][$OEPZeile] . '>' . $rsOEP['FRK_KBA'][$OEPZeile] . '</a>';
		echo '<br>' . $rsOEP['KFZ_HERSTELLER'][$OEPZeile] . ', ' . $rsOEP['KFZ_MODELL'][$OEPZeile];
		echo '</td>';

		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_MENGE'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_GRUND'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. $rsOEP['FRK_OEN_NUMMER'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>'	. htmlspecialchars($rsOEP['FRK_ARTBEZ'][$OEPZeile]) . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .'>' . $rsOEP['ATUNR_SUCH'][$OEPZeile] . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .' align=right>'	. awis_format($rsOEP['FRK_PREIS'][$OEPZeile],'Currency') . '</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .' align=right>'	. $rsOEP['FRK_RABATT'][$OEPZeile] . '%</td>';
		echo '<td ' . (($OEPZeile%2)==0?'id=TabellenZeileGrau':'id=TabellenZeileWeiss') .' align=left>';

		if($rsOEP['BEZ'][$OEPZeile]!='' AND $rsOEP['AWBEZ'][$OEPZeile]!='')
		{
			echo '<br>' . $rsOEP['AWSTNR'][$OEPZeile] . ' (' . $rsOEP['AWBEZ'][$OEPZeile] . ')';
		}
		elseif($rsOEP['BEZ'][$OEPZeile]!='')
		{
			echo $rsOEP['ST_AWSTNR'][$OEPZeile] . '(' . $rsOEP['BEZ'][$OEPZeile] . ')';
		}
		elseif($rsOEP['AWBEZ'][$OEPZeile]!='')
		{
			echo $rsOEP['AWSTNR'][$OEPZeile] . ' (' . $rsOEP['AWBEZ'][$OEPZeile] . ')';
		}
		else
		{
			$rsFRK = awisOpenRecordset($con, "SELECT * FROM v_FREMDKAEUFE_GES_PRO_OENUMMER WHERE FRK_OEN_SUCHNUMMER " . awisLIKEoderIST($rsOEP['FRK_OEN_NUMMER'][$OEPZeile],false,false,false,2) . ' ORDER BY Jahre');
			$rsFRKZeilen = $awisRSZeilen;

			$Insg = 0;
			$AktJahr = 0;
			$VorJahr = 0;
			For($i=0;$i<$rsFRKZeilen;$i++)
			{
				if($rsFRK['JAHRE'][$i]==0)
				{
					$AktJahr = $rsFRK['MENGE'][$i];
				}
				if($rsFRK['JAHRE'][$i]==1)
				{
					$VorJahr = $rsFRK['MENGE'][$i];
				}
				$Insg += $rsFRK['MENGE'][$i];
			}

			echo 'Fremdk�ufe: ' . $Insg . '/' . $AktJahr;
		}
		echo '</td>';

		echo '</tr>';
	}

	print "</table>";
}
elseif($_POST['txtFormat']=='csv')		// Liste als CSV Datei
{
	$SQL = 'SELECT HER_ID,HER_BEZEICHNUNG,HER_BEARBEITUNGSGRUPPE ';
	$SQL .= ' FROM HERSTELLER ORDER BY HER_BEZEICHNUNG';
	$rsHER = awisOpenRecordset($con,$SQL);
	$rsHER_Zeilen = $awisRSZeilen;
//var_dump($rsOEP);
		// Dateinamen generieren
	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );

	fputs($fd, "Filiale;Datum;LfdNr;WstNr;Gruppe;Modell;KBA-Nr;Menge;Grund;OE-Nr;Artikel;Cross;CrossAlle;Filiale;AWST;Anwendungstellen;FK_Ges;FK_12;Bearbeitungsgruppe\n");

	for($OEPZeile=0;$OEPZeile<$rsOEPZeilen;$OEPZeile++)
	{
		fputs($fd, '' . $rsOEP['FRK_FIL_ID'][$OEPZeile]);
		fputs($fd, ';' . $rsOEP['FRK_DATUM'][$OEPZeile]);
		fputs($fd, ';' . $rsOEP['FRK_LFDNR'][$OEPZeile]);
		fputs($fd, ';' . $rsOEP['FRK_WSTNR'][$OEPZeile]);
/*
		// Herstellergruppe oder Hersteller
		for($herid=0;$herid<$rsHER_Zeilen;$herid++)
		{
			if($rsOEP['KFZ_HERSTID'][$OEPZeile] == $rsHER['HER_ID'][$herid])
			{
				fputs($fd, ';' . str_replace(';',',' ,$rsHER['HER_BEARBEITUNGSGRUPPE'][$herid]));
				break;
			}
		}
		if($herid>=$rsHER_Zeilen)		// EKAT Hersteller ist nicht in Tabelle Hersteller
		{
			fputs($fd, ';<<Kein Hersteller>>');
		}
*/
		if($rsOEP['HER_BEARBEITUNGSGRUPPE'][$OEPZeile]=='')
		{
			fputs($fd, ';<<Kein Hersteller>>');
		}
		else
		{
			fputs($fd, ';' . $rsOEP['HER_BEARBEITUNGSGRUPPE'][$OEPZeile]);			
		}

		fputs($fd, ';' . str_replace(';',',' ,$rsOEP['KFZ_MODELL'][$OEPZeile]));
		fputs($fd, ';' . $rsOEP['FRK_KBA'][$OEPZeile]);
		fputs($fd, ';' . $rsOEP['FRK_MENGE'][$OEPZeile]);
		fputs($fd, ';' . str_replace(';',',',$rsOEP['FRK_GRUND'][$OEPZeile]));
		fputs($fd, ";" . $rsOEP['FRK_OEN_NUMMER'][$OEPZeile]) . "";
		fputs($fd, ';' . str_replace(';',',',$rsOEP['FRK_ARTBEZ'][$OEPZeile]));

				// ATU-Nr aus den Teileinfos

		$SQL = "SELECT TEI_SUCH1 from TeileInfos WHERE TeileInfos.TEI_ITY_ID1 = 'AST' AND TeileInfos.TEI_ITY_ID2 IN ('OEN','LAR') AND TeileInfos.TEI_SUCH2 " . awisLIKEoderIST($rsOEP['FRK_OEN_NUMMER'][$OEPZeile],FALSE,FALSE,FALSE,2) . "";
		$rsATUNR = awisOpenRecordset($con, $SQL);
		fputs($fd, ';' . $rsOEP['ATUNR_SUCH'][$OEPZeile]);		// Den ersten als separate Spalte
		$Ausgabe = '';

		for($i=0;$i<$awisRSZeilen;$i++)
		{
			if(strstr($Ausgabe,$rsATUNR['TEI_SUCH1'][$i])==FALSE)
			{
				$Ausgabe .= ',' . $rsATUNR['TEI_SUCH1'][$i];
			}
		}
		unset($rsATUNR);

		fputs($fd, ';' . substr($Ausgabe,1));


		fputs($fd, ';' . $rsOEP['ATUNR_FILIALE'][$OEPZeile]);

		if($rsOEP['BEZ'][$OEPZeile]!='' AND $rsOEP['AWBEZ'][$OEPZeile]!='')
		{
			fputs($fd, ';' . $rsOEP['AWSTNR'][$OEPZeile] . ';' . $rsOEP['AWBEZ'][$OEPZeile] . ';;');
		}
		elseif($rsOEP['BEZ'][$OEPZeile]!='')
		{
			fputs($fd, ';' . $rsOEP['ST_AWSTNR'][$OEPZeile] . ';' . $rsOEP['BEZ'][$OEPZeile] . ';;');
		}
		elseif($rsOEP['AWBEZ'][$OEPZeile]!='')
		{
			fputs($fd, ';' . $rsOEP['AWSTNR'][$OEPZeile] . ' ;' . $rsOEP['AWBEZ'][$OEPZeile] . ';;');
		}
		else
		{
			fputs($fd, ';;');		// Keine Anwendungsstellen -> leer lassen

			$rsFRK = awisOpenRecordset($con, "SELECT * FROM v_FREMDKAEUFE_GES_PRO_OENUMMER WHERE FRK_OEN_SUCHNUMMER " . awisLIKEoderIST($rsOEP['FRK_OEN_NUMMER'][$OEPZeile],false,false,false,2) . ' ORDER BY Jahre');
			$rsFRKZeilen = $awisRSZeilen;

			$Insg = 0;
			$AktJahr = 0;
			$VorJahr = 0;
			For($i=0;$i<$rsFRKZeilen;$i++)
			{
				if($rsFRK['JAHRE'][$i]==0)
				{
					$AktJahr = $rsFRK['MENGE'][$i];
				}
				if($rsFRK['JAHRE'][$i]==1)
				{
					$VorJahr = $rsFRK['MENGE'][$i];
				}
				$Insg += $rsFRK['MENGE'][$i];
			}
			fputs($fd, ';' . ($VorJahr+$AktJahr));
			fputs($fd, ';' . $AktJahr);
		}
		fputs($fd, ';' . $rsOEP['HER_BEARBEITUNGSGRUPPE'][$OEPZeile]);
		fputs($fd, "\n");
	}

			// Fertig, Datei schlie�en
	fclose($fd);
	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';
}

unset($rsOEP);

?>

