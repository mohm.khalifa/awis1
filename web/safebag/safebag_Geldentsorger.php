<?php
require_once('safebag_funktionen.inc');

global $AWISCursorPosition;
global $AWISBenutzer;

try
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();

	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SBI','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[]=array('Wort','PDFErzeugen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);


	$Recht32000 = $AWISBenutzer->HatDasRecht(32000);
	if($Recht32000 == 0)
	{
	    $Form->Fehler_KeineRechte();
	}
    
    $AWIS_KEYs1 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TGD_KEY)
    $AWIS_KEYs2 = array();                                              // ein Array, da u.U. mehr als ein Key kommen k�nnen (= TKD_KEY)    
    $AWIS_KEYs1[0] = (isset($_GET['SBI_KEY'])? $_GET['SBI_KEY']: 0);    // wenn �ber GET (direkter Klick vom Benutzer auf href)
    $AWIS_KEYs2[0] = 0;
    if (isset($_GET['SBI_KEYs']))                                       // wenn �ber GET ein ganzes quasi-Array mitkommt (z.B. beim umsortieren der Zuordnungen)
    {
        $AWIS_KEYs1 = explode(awis_abgleich_funktionen::_UniqueDelimiter, $_GET['SBI_KEYs']);
    }
    
    if ($AWIS_KEYs1[0] <= 0)                                            // wenn 0-Index leer,
    {
    	foreach ($_POST as $key => $value)                              // dann alle POST-�bergaben durchlaufen
    	{
    		if (strpos($key, 'txtSBI_CHK_') === 0)                      // wenn an 1. Position (0. Index), 'txtTGD_CHK_' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merken
    		{
    			if ($AWIS_KEYs1[0] <= 0)                                // wenn 0. Index vom Array noch unbelegt, dann hier hin schreiben
    			{
    				$AWIS_KEYs1[0] = $value;
    			}
    			else                                                    // ansonsten hinzuf�gen
    			{
    				$AWIS_KEYs1[] = $value;
    			}
    		}
    	}
    }
    
    foreach ($_POST as $key => $value)                              // alle POST-�bergaben durchlaufen
    {
        if (strpos($key, 'txtSBK_CHK_') === 0)                      // wenn an 1. Position (0. Index), 'txtTKD_CHK_' gefunden wird (Checkbox war angehakelt), dann ist im Wert der Key => merken
        {
            if ($AWIS_KEYs2[0] <= 0)                                // wenn 0. Index vom Array noch unbelegt, dann hier hin schreiben
            {
                $AWIS_KEYs2[0] = $value;
            }
            else                                                    // ansonsten hinzuf�gen
            {
                $AWIS_KEYs2[] = $value;
            }            
        }
    }
        
    $SBA = new awis_abgleich_funktionen();
    
    $AnsichtSuche = false;
    $AnsichtAuswahl = false;
    $AnsichtZuordnung = false;
    
    $Gespeichert = false;                   // Flag, ob gespeichert wurde (=> wichtig, damit die Parameter behalten werden => siehe $SuchParam['SPEICHERN'])
    //$MailSenden = false;                    // Flag, ob Mail gesendet werden soll
    
    if (isset($_POST['cmdSuche_x']))
    {
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdDSWeiter_x']) and $AWIS_KEYs1[0] > 0)
    {
        $AnsichtZuordnung = true;
    }
    elseif (isset($_POST['cmdDSZurueck_x']))
    {
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdMail_x']))
    {
        $MailSenden = true;
        $AnsichtAuswahl = true;
    }
    elseif (isset($_POST['cmdSpeichern_SA'])
         or isset($_POST['cmdSpeichern_SB'])
    	 or isset($_POST['cmdSpeichern_AF'])
    	 or isset($_POST['cmdSpeichern_SF'])
    	 or isset($_POST['cmdSpeichern_B']))
    {
        $AbgleichArt = 'XXX';       // vorbelegen mit ung�ltig => nicht speichern
        $Freigabe = 0;
        $Bemerkung = '';
        if (isset($_POST['cmdSpeichern_SB']) 
        and ($AWIS_KEYs2[0] != 0)                           // wenn 1 Kassendatensatz (laut AWIS_KEY2) verf�gbar ist, gegen nicht 0 pr�fen, da das erste Arrayelement so vorbelegt wird
        and (count($AWIS_KEYs2) == 1)
        and (!empty($_POST['txtSBA_BEMERKUNG_UNTEN']))
    	)                      // nur ein Kassendatensatz
        {
            $AbgleichArt = 'S';
            $Freigabe = 1;
            $Bemerkung = $_POST['txtSBA_BEMERKUNG_UNTEN'];
        }  
        if (isset($_POST['cmdSpeichern_SF'])
        		and ($AWIS_KEYs2[0] != 0)                           // wenn 1 Kassendatensatz (laut AWIS_KEY2) verf�gbar ist, gegen nicht 0 pr�fen, da das erste Arrayelement so vorbelegt wird
        		and (count($AWIS_KEYs2) == 1))                      // nur ein Kassendatensatz
        {
        	$AbgleichArt = 'SF';
        	$Freigabe = 1;
        	$Bemerkung = $_POST['txtSBA_BEMERKUNG_UNTEN'];
        }
        
        if (isset($_POST['cmdSpeichern_B'])
        		and ($AWIS_KEYs2[0] != 0)                           // wenn 1 Kassendatensatz (laut AWIS_KEY2) verf�gbar ist, gegen nicht 0 pr�fen, da das erste Arrayelement so vorbelegt wird
        		and (count($AWIS_KEYs2) == 1))                      // nur ein Kassendatensatz
        {
        	$AbgleichArt = 'B';
        	$Bemerkung = $_POST['txtSBA_BEMERKUNG_UNTEN'];
        	//$Form->Hinweistext($AWISSprachKonserven['SBI']['MailGesendet'], 1);
        	$SBK_KEY =	$AWIS_KEYs2[0];
        	$SBI_KEY = $AWIS_KEYs1[0];
        	//$SBA->MailSenden($SBK_KEY,$SBI_KEY,$AbgleichArt,$AWISBenutzer);
        }
        
        if (isset($_POST['cmdSpeichern_SA']) and ($AWIS_KEYs2[0] == 0) and (!empty($_POST['txtSBA_BEMERKUNG_OBEN'])))      // keine leere Begr�ndung
        {
            $AbgleichArt = 'S';
            $Freigabe = 1;
            $Bemerkung = $_POST['txtSBA_BEMERKUNG_OBEN'];
        }
        if (isset($_POST['cmdSpeichern_AF']) and ($AWIS_KEYs2[0] == 0))      // keine leere Begr�ndung
        {
            $AbgleichArt = 'AF';
            $Bemerkung = $_POST['txtSBA_BEMERKUNG_OBEN'];
            //$Form->Hinweistext($AWISSprachKonserven['SBI']['MailGesendet'], 1);
            $SBK_KEY =	'';
            $SBI_KEY = $AWIS_KEYs1[0];
            //$SBA->MailSenden($SBK_KEY,$SBI_KEY,$AbgleichArt,$AWISBenutzer);
        }
        if ($AbgleichArt != 'XXX')          // auf ungleich 'XXX' pr�fen, da $AbgleichArt so vorbelegt wurde. 
        {
            foreach ($AWIS_KEYs1 as $key => $SBIvalue)
            {
                foreach ($AWIS_KEYs2 as $key => $SBKvalue)
                {
                    $SBA->AbgleichSpeichern($SBIvalue, $SBKvalue, $AbgleichArt, $Bemerkung,$Freigabe, $AWISBenutzer->BenutzerName(1));
                }
            }
            $Form->Hinweistext($AWISSprachKonserven['SBI']['DatenGespeichert'], 1);
            //$Form->ZeileStart();
            //$Form->Erstelle_TextLabel($AWISSprachKonserven['TAG']['ZurueckSuche'], 800, '', './abgleich_Main.php?cmdAktion=Tuev'); 
            //$Form->ZeileEnde();
            $SQL =  'select * from sbgeldentsorger ' .
            		//'where tgd_fil_id = :var_N0_fil_id ' .
            		'where  sbi_abgeglichen = 0';
            
            if($_POST['txtSFilId'] != '' and $_POST['txtSFilId'] != '::bitte w�hlen::')
            {
            	$SQL .=' and sbi_fil_id = :var_N0_fil_id';
            	$DB->SetzeBindevariable('SBI', 'var_N0_fil_id', $_POST['txtSFilId'] , awisDatenbank::VAR_TYP_GANZEZAHL);
            }
            if (isset($_POST['txtDatumVon']) AND (!empty($_POST['txtDatumVon'])))
            {
            	$SQL .=' AND sbi_umsatztag >= :var_D_datum_von';
            	$DB->SetzeBindevariable('SBI', 'var_D_datum_von', $_POST['txtDatumVon'] , awisDatenbank::VAR_TYP_DATUM);
            } 
            if (isset($_POST['txtDatumBis']) AND (!empty($_POST['txtDatumBis'])))
            {
            
            	$SQL .=' AND sbi_umsatztag <= :var_D_datum_bis';
            	$DB->SetzeBindevariable('SBI', 'var_D_datum_bis', $_POST['txtDatumBis'] , awisDatenbank::VAR_TYP_DATUM);
            }         		
            $rsSuche = $DB->RecordSetOeffnen($SQL,$DB->Bindevariablen('SBI'));
            if ($rsSuche->AnzahlDatensaetze() > 0)
            {
            	$AnsichtAuswahl = true;
            }
            else 
            {
            	$AnsichtSuche = true;
            }
            $Gespeichert = true;
        }
        else        
        {
        	if (isset($_POST['cmdSpeichern_SA']))
        	{
        		$Form->Hinweistext($AWISSprachKonserven['SBI']['KeineBemerkung'], 1);
        	}
         	else if (isset($_POST['cmdSpeichern_AF']) or isset($_POST['cmdSpeichern_SF']) or isset($_POST['cmdSpeichern_B']))
         	{
         		$Form->Hinweistext($AWISSprachKonserven['SBI']['FalscheAnzahlDaten'], 1);
         	} 
         	else
         	{
         		$Form->Hinweistext($AWISSprachKonserven['SBI']['FalscheAnzahlDaten'].' '.$AWISSprachKonserven['SBI']['oder'].' '.$AWISSprachKonserven['SBI']['KeineBemerkung'], 1);
         	}
        	$AnsichtZuordnung = true;
        }
    }
    else
    {
        if (isset($_REQUEST['txtANSICHT']))        // wegen Block
        {
        	switch ($_REQUEST['txtANSICHT'])
            {
                case 'Auswahl':     $AnsichtAuswahl = true;     break;
                case 'Zuordnung':   $AnsichtZuordnung = true;   break;
                default: break;
            }
        }
        elseif ($AWIS_KEYs1[0] > 0)
        {
        	$AnsichtZuordnung = true;
        }
        else
        {
        	$AnsichtSuche = true;
        }
    }
    
$Form->DebugAusgabe(1, 'AnsichtSuche: ' . $AnsichtSuche, 'AnsichtAuswahl: ' . $AnsichtAuswahl, 'AnsichtZuordnung: ' . $AnsichtZuordnung);        
$Form->DebugAusgabe(1, $AWIS_KEYs1, $AWIS_KEYs2);
$Form->DebugAusgabe(1, $_REQUEST);    

	/**********************************************
	* Benutzerparameter
	***********************************************/
    $SuchParam = array();
	if ((strlen($AWISBenutzer->ParameterLesen('Formular_SBI')) < 8))
	{
        $SuchParam['DatumVon'] = '';
        $SuchParam['DatumBis'] = '';
        $SuchParam['FilId'] = '';
        $SuchParam['SPEICHERN']= 'off';
	    $AWISBenutzer->ParameterSchreiben("Formular_SBI", serialize($SuchParam));
	}
    $SuchParam = unserialize($AWISBenutzer->ParameterLesen('Formular_SBI'));
	if ($AnsichtAuswahl and !isset($_GET['SBISort']))
	{
		
        $SuchParam['DatumVon'] = (isset($_POST['sucSBA_DATUMVON'])? $_POST['sucSBA_DATUMVON']: $SuchParam['DatumVon']);
        $SuchParam['DatumBis'] = (isset($_POST['sucSBA_DATUMBIS'])? $_POST['sucSBA_DATUMBIS']: $SuchParam['DatumBis']);
        $SuchParam['FilId'] = (isset($_POST['sucSBA_FILNR'])? $_POST['sucSBA_FILNR']: $SuchParam['FilId']);
        If(!isset($_POST['cmdDSZurueck_x']))
        {
        	$SuchParam['SPEICHERN'] = ((isset($_POST['sucSBA_AUSWAHLSPEICHERN']) or ($Gespeichert == true))? 'on': 'off');
        }
        $AWISBenutzer->ParameterSchreiben("Formular_SBI", serialize($SuchParam));	        
	}    
    /*
    if ($MailSenden)
    {
        $TAG->MailSenden($SuchParam, $AWISBenutzer);
    }*/
    
	/**********************************************
	* Formular
	***********************************************/    
    $Form->SchreibeHTMLCode('<form name=frmGeldentsorger action=./safebag_Main.php?cmdAktion=Geldentsorger method=POST enctype="multipart/form-data">');
	$Form->Formular_Start();
$Form->DebugAusgabe(1, $SuchParam);

    if ($AnsichtSuche or $AnsichtAuswahl)
    {
        $SBA->Label_Filter($AnsichtSuche);
        $SBA->Feld_UmsatzTagDatumVonBis($SuchParam, $AnsichtSuche);
        $SBA->Feld_SelectFilId($SuchParam, $AnsichtSuche, 1);
        $SBA->Feld_AuswahlSpeichern($SuchParam, $AnsichtSuche);
    }
    
    if ($AnsichtAuswahl or $AnsichtZuordnung)
    {
        $Form->Trennzeile('L');
        $Sort= '';
        if(isset($_GET['SBISort']))
        {
        	$Sort = $_GET['SBISort'];
        }
        elseif(isset($_REQUEST['txtSBISort']))
        {
        	$Sort = $_REQUEST['txtSBISort'];
        }
		// Infozeile zusammenbauen
		$Felder = array();
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./safebag_Main.php?cmdAktion=Geldentsorger accesskey=S title=' . $AWISSprachKonserven['SBI']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
        $Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['SBI']['ZurueckSuche']);
		$Form->InfoZeile($Felder, '');
        
        if ($AnsichtAuswahl)
        {
            $SQL = 'select sbi.*, '.
                   'row_number() OVER (';            
            if ($Sort != '')
            {		// wenn GET-Sort, dann nach diesen Feld sortieren
                $SQL .= ' order by ' . str_replace('~',' DESC ', $Sort);
            }
            else 							
            {		// sonst pauschal nach Pr�fdatum
                $SQL .= 'order by sbi_umsatztag desc';
            }
            $SQL .= ') AS ZeilenNr ' . 
                    'from sbgeldentsorger sbi ';
            //$Bedingung .= $TAG->ErstelleTGDBedingung($SuchParam);       // Bedingung erstellen 2x => f�r den ersten Teile ODER und f�r den zweiten Teil der Gesamtbedingung
            $Bedingung = ' and sbi_abgeglichen = 0';                  // dann verODERn, geklammert 
            $Bedingung .= $SBA->ErstelleSBIBedingung($SuchParam);       // mit dem Rest
            if ($Bedingung != '')
            {
                $SQL .= ' WHERE ' . substr($Bedingung, 4);
            }

            $MaxDS = 1;
            $ZeilenProSeite=1;
            $Block = 1;
            // Zum Bl�ttern in den Daten
            if (isset($_REQUEST['Block']))
            {
                if (! isset($_POST['cmdDSWeiter_x']) and !isset($_POST['cmdDSZurueck_x']))
                {
                    $Block = $Form->Format('N0', $_REQUEST['Block'], false);
                }
            }
            $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
            $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
            $MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
            $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
$Form->DebugAusgabe(1, $SQL);
            $rsSBI = $DB->RecordSetOeffnen($SQL);
            
            // Spaltenbreiten f�r Listenansicht
            $FeldBreiten = array();
            //$FeldBreiten['ChkBox'] = 20;
            $FeldBreiten['Safebagnr'] = 100;
            $FeldBreiten['UmsatzTag'] = 120;
            $FeldBreiten['Betrag'] = 120;
            $FeldBreiten['FilId'] = 80;
            $Gesamtbreite = 0;
            foreach ($FeldBreiten as $value)
            {
                $Gesamtbreite += $value +2.9;
            }
            
            $Form->ZeileStart();
            // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
            //$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
            // �berschrift der Listenansicht mit Sortierungslink: Filiale
            $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Auswahl&SBISort=SBI_FIL_ID'.((isset($_GET['SBISort']) AND ($_GET['SBISort']=='SBI_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_FIL_ID'], $FeldBreiten['FilId'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
            $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Auswahl&SBISort=SBI_UMSATZTAG'.((isset($_GET['SBISort']) AND ($_GET['SBISort']=='SBI_UMSATZTAG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_UMSATZTAG'], $FeldBreiten['UmsatzTag'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
            $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Auswahl&SBISort=SBI_SAFEBAGNR'.((isset($_GET['SBISort']) AND ($_GET['SBISort']=='SBI_SAFEBAGNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_SAFEBAGNR'], $FeldBreiten['Safebagnr'], '', $Link);
            // �berschrift der Listenansicht mit Sortierungslink: Nachname
            $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Auswahl&SBISort=SBI_BETRAG'.((isset($_GET['SBISort']) AND ($_GET['SBISort']=='SBI_BETRAG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_BETRAG'], $FeldBreiten['Betrag'], '', $Link,'','R');
            $Form->ZeileEnde();
            if($rsSBI->AnzahlDatensaetze() > 12)
            {
            	$Form->SchreibeHTMLCode('<div id=scrSafebagOffen style="width: 100%; height: 250px;  overflow-y: scroll;">');
            }
            $DS = 0;	// f�r Hintergrundfarbumschaltung
            while(! $rsSBI->EOF())
            {
            	$Form->ZeileStart();
                $Link = './safebag_Main.php?cmdAktion=Geldentsorger&SBI_KEY=0'.$rsSBI->FeldInhalt('SBI_KEY') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
                //$Form->Erstelle_Checkbox('SBI_CHK_' . $rsSBI->FeldInhalt('SBI_KEY'), '', $FeldBreiten['ChkBox'], true, $rsSBI->FeldInhalt('SBI_KEY'));                
                $TTT = $rsSBI->FeldInhalt('SBI_FIL_ID');
                $Form->Erstelle_ListenFeld('SBI_FIL_ID', $rsSBI->FeldInhalt('SBI_FIL_ID'), 0, $FeldBreiten['FilId'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                $TTT = $Form->Format('D', $rsSBI->FeldInhalt('SBI_UMSATZTAG'));
                $Form->Erstelle_ListenFeld('SBI_UMSATZTAG', $Form->Format('D',$rsSBI->FeldInhalt('SBI_UMSATZTAG')), 0, $FeldBreiten['UmsatzTag'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                //$TTT = $rsTGD->FeldInhalt('TGD_NACHNAME') . ' (' . $rsTGD->FeldInhalt('TGD_KFZKENNZ') . ')';
                //$Form->Erstelle_ListenFeld('TGD_NACHNAME', $rsTGD->FeldInhalt('TGD_NACHNAME'), 0, $FeldBreiten['Name'], false, ($DS%2), '', $Link, 'T', 'L', $TTT)
                $TTT = $rsSBI->FeldInhalt('SBI_SAFEBAGNR');
                $Form->Erstelle_ListenFeld('SBI_SAFEBAGNR', $rsSBI->FeldInhalt('SBI_SAFEBAGNR'), 0, $FeldBreiten['Safebagnr'], false, ($DS%2),'', $Link, 'T', 'L', $TTT);
                $TTT = $Form->Format('N2', $rsSBI->FeldInhalt('SBI_BETRAG'));
                $Form->Erstelle_ListenFeld('SBI_BETRAG', $Form->Format('N2', $rsSBI->FeldInhalt('SBI_BETRAG')), 0, $FeldBreiten['Betrag'], false, ($DS%2), '', $Link, 'T', 'R', $TTT);
                //$TTT = $rsTGD->FeldInhalt('TGD_TUEVNR') . ' - ' . $rsTGD->FeldInhalt('TUE_BEZ');
                //$Form->Erstelle_ListenFeld('TGD_TUEVNR', $rsTGD->FeldInhalt('TGD_TUEVNR'), 0, $FeldBreiten['TuevNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);                             
                //$TTT = $Form->Format('D', $rsTGD->FeldInhalt('TGD_BELEGDATUM')) . ' (' . $rsTGD->FeldInhalt('TGD_BELEGNR') . ')';
                //$Form->Erstelle_ListenFeld('TGD_BELEGDATUM', $Form->Format('D', $rsTGD->FeldInhalt('TGD_BELEGDATUM')), 0, $FeldBreiten['BelegDatum'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                //$TTT = $rsTGD->FeldInhalt('TUA_BEZ') . ' (' . $rsTGD->FeldInhalt('TUA_BEZ') . ')';
                //$Form->Erstelle_ListenFeld('TGD_TUEVART', $rsTGD->FeldInhalt('TUA_BEZ'), 0, $FeldBreiten['TuevArt'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                //$TTT = $rsTGD->FeldInhalt('TGD_FIL_ID') . ' - ' . $rsTGD->FeldInhalt('FIL_BEZ');
                //$Form->Erstelle_ListenFeld('TGD_FIL_ID', $rsTGD->FeldInhalt('TGD_FIL_ID'), 0, $FeldBreiten['FilNr'], false, ($DS%2), '', $Link, 'T', 'L', $TTT);
                //$TTT = $AWISSprachKonserven['TAG']['Nachverfolgung'];
                //$Form->Erstelle_ListenFeld('TGD_DUPLIKAT', $rsTGD->FeldInhalt('DUPLIKAT'), 0, $FeldBreiten['Nachverfolgung']-4, false, ($DS%2), '', '', 'T', 'L', $TTT);
                $Form->ZeileEnde();
                $DS++;
                $rsSBI->DSWeiter();                
            }
            if($rsSBI->AnzahlDatensaetze() > 12)
            {
            	$Form->SchreibeHTMLCode('</div>') ;
            }
            $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	    
            $Form->ZeileStart();
            $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['SBI']['SummeGesamtDS'].': '.$Form->Format('N0',$rsSBI->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
            //$Form->Erstelle_Liste_Ueberschrift($Form->Format('N0',$rsSBI->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), 107, 'font-weight:bolder;','','','R');
            $Form->ZeileEnde();
            $Form->Erstelle_HiddenFeld('ANSICHT', 'Auswahl');                       // f�r direkte Auswahl der Seite
            $Form->Erstelle_HiddenFeld('SBISort', $Sort);        
        }
        elseif ($AnsichtZuordnung)
        {
        	
        	$SQL = 'select sbi.*,fil_bez '.
        			'from sbgeldentsorger sbi '.
        		    'inner join filialen on sbi_fil_id = fil_id ';              
			if 	(count($AWIS_KEYs1)==1)
			{
				$DB->SetzeBindevariable('SBI', 'var_N0_sbi_key', $AWIS_KEYs1[0], awisDatenbank::VAR_TYP_GANZEZAHL);
				$SQL.='WHERE sbi.sbi_key = :var_N0_sbi_key';
			}
        	else 
        	{
		        $SQL.='WHERE sbi.sbi_key in (';
        		$i=0;
	           	foreach ($AWIS_KEYs1 as $key => $value)
		        {
	                ++$i;
	            	$DB->SetzeBindevariable('SBI', 'var_N0_sbi_key_'.$i, $value, awisDatenbank::VAR_TYP_GANZEZAHL);
	            	$SQL .=':var_N0_sbi_key_'.$i.', ';                	        	
	            }           
	            $SQL = substr($SQL, 0, -2);
	            $SQL .= ')';
        	}
        	$Form->DebugAusgabe(1, $SQL);        
            $rsSBI = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SBI'));
            $AnzahlGewaehlteDatensaetze = $rsSBI->AnzahlDatensaetze();
            $SBA->Label_Abgleich();
            
            $TopErgebnissFilId = array();
            $TGDMerker = 0;
            
            while(! $rsSBI->EOF())
            {
                $SBA->Felder_AbgleichSBI($rsSBI);
                $TopErgebnissFilId[] = $rsSBI->FeldInhalt('SBI_FIL_ID');      
                $rsSBI->DSWeiter();
                
                if (! $rsSBI->EOF())
                {
                    $Form->Trennzeile('O');
                }
            }                

            if ($AnzahlGewaehlteDatensaetze == 1)       // nur wenn 1 T�V-Datensatz gew�hlt wurde, k�nnen folgende drei Abgleicharten gew�hlt werden
            {
                $Form->Trennzeile('O');
                $SBA->Feld_Bemerkung(1, $TGDMerker);
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('', awis_abgleich_funktionen::_LabelBreite);
                $Form->Schaltflaeche('submit', 'cmdSpeichern_AF', '', '', $AWISSprachKonserven['SBI']['MAAusgabeFehlt'], '');
                $Form->Schaltflaeche('submit', 'cmdSpeichern_SA', '', '', $AWISSprachKonserven['SBI']['MASonstiges'], '');
                $Form->ZeileEnde();
                
            }
            
            if ($AnzahlGewaehlteDatensaetze > 2)    // zu viele T�V-Datens�tze f�r Abgleich ausgew�hlt
            {
                $Form->Hinweistext($AWISSprachKonserven['TAG']['ZuVieleDaten'], 1);                
            }
            else
            {
            	$Sort= '';
            	if(isset($_GET['SBKSort']))
            	{
            		$Sort = $_GET['SBKSort'];
            	}
            	elseif(isset($_REQUEST['txtSBKSort']))
            	{
            		$Sort = $_REQUEST['txtSBKSort'];
            	}
                
                
                //$DB->SetzeBindevariable('TKD', 'var_tkd_datumzeit', $SucDatum, awisDatenbank::VAR_TYP_DATUM);
                
            	$Form->Trennzeile('L');
                $SQL2 = 'select * from sbkassendaten ';
                   
                if(!empty($TopErgebnissFilId))
                {
                	$SQL2 .= ' where sbk_fil_id in(';
                	$SucFilID='';
                	$i=0;
                	foreach ($TopErgebnissFilId as $wert)
                	{
						++$i;
		            	$DB->SetzeBindevariable('SBK', 'var_N0_fil_id_'.$i, $wert, awisDatenbank::VAR_TYP_GANZEZAHL );
		            	$SQL2 .=':var_N0_fil_id_'.$i.', ';                	        	
                		$SucFilID .= ',' . $wert;
                	}
                
                	$SucFilID = substr($SucFilID,1);
                	
		            $SQL2 = substr($SQL2, 0, -2);
		            $SQL2 .= ')';
                }
                else
                {
                	$SucFilID = $_REQUEST['txtFilId'];
                	$DB->SetzeBindevariable('SBK', 'var_N0_fil_id', $SucFilID, awisDatenbank::VAR_TYP_GANZEZAHL);
                	$SQL2 .= ' where sbk_fil_id = :var_N0_fil_id';
                }
                
                //$SQL2 .= ' and tkd_datumzeit + 30 >= :var_tkd_datumzeit';
                $SQL2 .= ' and sbk_abgeglichen = 0';
                $SQL =  'select sub.*,row_number() OVER (';            
                if ($Sort != '')
                {		// wenn GET-Sort, dann nach diesen Feld sortieren
                    $SQL .= 'order by ' . str_replace('~',' DESC ', $Sort);
                }
                else 							
                {		// sonst pauschal nach erster Spalte => ergibt die richtige Treffersortierung (generiert in ErstelleTopErgebnissSQL())
                    $SQL .= 'order by sbk_datum desc';
                }
                $SQL .= ') AS ZeilenNr from (';
                $SQL .= $SQL2;
                $SQL .= ')sub ';
                $MaxDS = 1;
                $ZeilenProSeite=1;
                $Block = 1;
                // Zum Bl�ttern in den Daten
                if (isset($_REQUEST['Block']))
                {
                    $Block = $Form->Format('N0', $_REQUEST['Block'], false);
                }
                $ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
                $StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
                $MaxDS = $DB->ErmittleZeilenAnzahl($SQL, $DB->Bindevariablen('SBK', false));
                $SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
                $rsSBK = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('SBK'));            
                $Form->DebugAusgabe(1, $DB->LetzterSQL());
                $Form->DebugAusgabe(1, $DB->Bindevariablen('SBK', false));
                // Spaltenbreiten f�r Listenansicht
                $FeldBreiten = array();
                $FeldBreiten['ChkBox'] = 20;
                $FeldBreiten['Filiale'] = 50;
                $FeldBreiten['Umsatztag'] = 120;
                $FeldBreiten['Safebagnr'] = 120;
                $FeldBreiten['Betrag'] = 100;
                $Gesamtbreite = 0;
                foreach ($FeldBreiten as $value)
                {
                    $Gesamtbreite += $value+2.8;
                }            
                $AWIS_KEYs1String = implode(awis_abgleich_funktionen::_UniqueDelimiter, $AWIS_KEYs1);       // alle Anzeigen verpacken und an Link mit dran h�ngen
                $Form->ZeileStart();
                // �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
                $Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
                // �berschrift der Listenansicht mit Sortierungslink: Kassendatum
                $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Zuordnung&SBKSort=SBK_FIL_ID'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&SBI_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_FIL_ID'], $FeldBreiten['Filiale'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
                $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Zuordnung&SBKSort=SBK_DATUM'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_DATUM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&SBI_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_UMSATZTAG'], $FeldBreiten['Umsatztag'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: Name
                $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Zuordnung&SBKSort=SBK_SAFEBAGNR'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_SAFEBAGNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&SBI_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_SAFEBAGNR'], $FeldBreiten['Safebagnr'], '', $Link);
                // �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
                $Link = './safebag_Main.php?cmdAktion=Geldentsorger&txtANSICHT=Zuordnung&SBKSort=SBK_BETRAG'.((isset($_GET['SBKSort']) AND ($_GET['SBKSort']=='SBK_BETRAG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&SBI_KEYs='.$AWIS_KEYs1String.'&txtFilId='.$SucFilID;
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SBI_SOLLBETRAG'], $FeldBreiten['Betrag'], '', $Link);
                $Form->ZeileEnde();
                // �berschrift der Listenansicht mit Sortierungslink: T�V-Nr
                //$Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&TKDSort=TKD_TUEVNR'.((isset($_GET['TKDSort']) AND ($_GET['TKDSort']=='TKD_TUEVNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'').'&TGD_KEYs='.$AWIS_KEYs1String;
                //$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['TAG']['TuevNr'], $FeldBreiten['TuevNr'], '', $Link);            
                if($rsSBK->AnzahlDatensaetze() > 12)
                {
                	$Form->SchreibeHTMLCode('<div id=scrKasse style="width: 100%; height: 250px;  overflow-y: scroll;">');
                }
                $DS = 0;	// f�r Hintergrundfarbumschaltung
                while(! $rsSBK->EOF())
                {
                    $Form->ZeileStart();

                    $Form->Erstelle_Checkbox('SBK_CHK_' . $rsSBK->FeldInhalt('SBK_KEY'), '', $FeldBreiten['ChkBox'], true, $rsSBK->FeldInhalt('SBK_KEY'));
                    $TTT = $rsSBK->FeldInhalt('SBK_FIL_ID');
                    $Form->Erstelle_ListenFeld('SBK_FILID', $rsSBK->FeldInhalt('SBK_FIL_ID'), 0, $FeldBreiten['Filiale'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $Form->Format('D', $rsSBK->FeldInhalt('SBK_DATUM'));
                    $Form->Erstelle_ListenFeld('SBK_DATUM', $Form->Format('D', $rsSBK->FeldInhalt('SBK_DATUM')), 0, $FeldBreiten['Umsatztag'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $rsSBK->FeldInhalt('SBK_SAFEBAGNR');
                    $Form->Erstelle_ListenFeld('SBK_SAFEBAGNR', $rsSBK->FeldInhalt('SBK_SAFEBAGNR'), 0, $FeldBreiten['Safebagnr'], false, ($DS%2), '', '', 'T', 'L', $TTT);
                    $TTT = $rsSBK->FeldInhalt('SBK_BETRAG');
                    $Form->Erstelle_ListenFeld('SBK_BETRAG', $rsSBK->FeldInhalt('SBK_BETRAG'), 0, $FeldBreiten['Betrag'], false, ($DS%2), '', '', 'T', 'R', $TTT);
                    $Form->ZeileEnde();
                    $DS++;
                    $rsSBK->DSWeiter();                
                }
                if($rsSBK->AnzahlDatensaetze() > 12)
                {
                	$Form->SchreibeHTMLCode('</div>') ;
                }
                $DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
                $Form->ZeileStart();
                $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['SBI']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['SBI']['SummeGesamtDS'].': '.$Form->Format('N0',$rsSBK->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS),$Gesamtbreite, 'font-weight:bolder;');
                $Form->ZeileEnde();           
                foreach ($AWIS_KEYs1 as $key => $value)
                {
                    $Form->Erstelle_HiddenFeld('SBI_CHK_' . $value, $value);    // AWIS_KEYs1 f�r n�chste Seite merken
                } 
          
                $Link = './abgleich_Main.php?cmdAktion=Tuev&txtANSICHT=Zuordnung&txtFilId='.$SucFilID.'&TKDSort='.$Sort;      // f�r N�chste/Vorherige Seite schalten (href im Framwork) (=> mit txt damit immer die gleiche REQUEST-Var ankommt, wegen HiddenFeld)
                $Form->Erstelle_HiddenFeld('ANSICHT', 'Zuordnung');
                $Form->Erstelle_HiddenFeld('FilId', $SucFilID);                       // f�r direkte Auswahl der Seite
                //$Form->Erstelle_HiddenFeld('BelegNr', $SuchParam['BelegNr']);
                $Form->Erstelle_HiddenFeld('SFilId', $SuchParam['FilId']);
                $Form->Erstelle_HiddenFeld('DatumVon', $SuchParam['DatumVon']);
                $Form->Erstelle_HiddenFeld('DatumBis', $SuchParam['DatumBis']);
                $Form->Erstelle_HiddenFeld('TKDSort', $Sort);
                //$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');            

                $Form->Trennzeile('L');

                //$TAG->Feld_SelectAbgleicharten();
                $SBA->Feld_Bemerkung();
                $Form->ZeileStart();
                $Form->Erstelle_TextLabel('', awis_abgleich_funktionen::_LabelBreite);            
                $Form->Schaltflaeche('submit', 'cmdSpeichern_SB', '', '', $AWISSprachKonserven['SBI']['MASonstiges'], '');
                $Form->Schaltflaeche('submit', 'cmdSpeichern_B', '', '', $AWISSprachKonserven['SBI']['MABetragsdifferenz'], '');
                $Form->Schaltflaeche('submit', 'cmdSpeichern_SF', '', '', $AWISSprachKonserven['SBI']['MASafebagFalsch'], '');
                $Form->ZeileEnde();
            }
        }
    }

	$Form->Formular_Ende();	    
    
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if ((($Recht32000&1) == 1)
    and ($AnsichtSuche))
	{	
	    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	}    
    if ($AnsichtZuordnung)
	{	        
        $Form->Schaltflaeche('image', 'cmdDSZurueck', '', '/bilder/cmd_dszurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'N');
    }
    
    if ($AnsichtAuswahl)
    {
    	//var_dump($SuchParam['DatumVon']);
    	//var_dump($SuchParam['DatumBis']);
    	$Param = '&Filiale='.$SuchParam['FilId'];
    	$Param .= '&UmsVon='.$SuchParam['DatumVon'];
    	$Param .= '&UmsBis='.$SuchParam['DatumBis'];
    	//$SuchParam['SuchenInKey'] = awis_abgleich_funktionen::_SuchenInTGD;             // Schalter auf TGD-Tabelle setzen
    	$LinkPDF = '/berichte/drucken.php?XRE=48&ID='.base64_encode($Param);
    	$Form->Schaltflaeche('href', 'cmdPdf', $LinkPDF, '/bilder/cmd_pdf.png', $AWISSprachKonserven['Wort']['PDFErzeugen']);
    }
    
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
    $Form->SetzeCursor($AWISCursorPosition);
    
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>