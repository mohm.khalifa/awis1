<?php

//Blanko -> Formular VisitCheck
require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('IBG','%');
$TextKonserven[]=array('CV','%');

$AWISBenutzer = awisBenutzer::Init();

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Ausdruck = new awisAusdruck('P','A4','');
$Ausdruck->NeueSeite(0,1);

if ($AWISBenutzer->BenutzerSprache()=='CZ')
{
	$Ausdruck->_pdf->ZeichenKodierung('CP1250');
	$Schriftart = 'arialcz';
}
else 
{
	$Schriftart = 'arial';
}

$Ausdruck->_pdf->AddFont($Schriftart,'','');

function Auswertung_Felder($Ausdruck,$ID,$x1,$y1,$x1_2,$y1_2,$FeldBreite = 5)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	global $Schriftart;
	
	$Schritt = 3;
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;
	
	$rsAuswertung = $DB->RecordSetOeffnen($SQL);
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1,$y1,$x2 = ($x1  + $Schritt),$y2 = ($y1 + $Schritt));
	  $Ausdruck->_pdf->Line($x2, $y1, $x1, $y2);
          $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	}
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1_2,$y1_2,$x2 = ($x1_2  + $Schritt),$y2 = ($y1_2 + $Schritt));
      $Ausdruck->_pdf->Line($x2, $y1_2, $x1_2, $y2);
      $Ausdruck->_pdf->SetLineWidth(0); 	
	}
	else
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	}
	
	if($rsAuswertung->FeldInhalt('PBF_BEMERKUNG') == '')
	{
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6);
	  $Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,35)."\n".substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),35,35);
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$Text,'LTRB',0,'L',0);
	}
	else 
	{
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6); 	
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$rsAuswertung->FeldInhalt('PBF_BEMERKUNG').':'.$rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),'LTRB','L',1);  	
	}
	
	
}

$Spalte = 15;
$Zeile = 5;

if($_GET['PEI_KEY'] == 0)
{
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch GBL: Typ C','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 23.10.2012','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.0','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.1','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['IBG']['IBG_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['IBG']['IBG_Datum'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,'','B',0,'C',0);
$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,'A. '.$AWISSprachKonserven['IBG']['IBG_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_A1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 62, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 62, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['IBG']['IBG_A2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 69, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 69, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',1);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_A3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 75, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 75, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_A4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 80, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 80, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'B. '.$AWISSprachKonserven['IBG']['IBG_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'1. '.$AWISSprachKonserven['IBG']['IBG_B1'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 91.5, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 91.5, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_B2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 98, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 98, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'3. '.$AWISSprachKonserven['IBG']['IBG_B3'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 104.5, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 104.5, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'4. '.$AWISSprachKonserven['IBG']['IBG_B4'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 112.5, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 112.5, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'5. '.$AWISSprachKonserven['IBG']['IBG_B5'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 120.5, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 120.5, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_B6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 127, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 127, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_B7'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 132, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 132, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'8. '.$AWISSprachKonserven['IBG']['IBG_B8'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 137, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 137, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'9. '.$AWISSprachKonserven['IBG']['IBG_B9'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 143.5, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 143.5, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'10. '.$AWISSprachKonserven['IBG']['IBG_B10'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 150, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 150, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'C. '.$AWISSprachKonserven['IBG']['IBG_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_C1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 160, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 160, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_C2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 165, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 165, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_C3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 170, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 170, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_C4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 175, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 175, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_C5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 180, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 180, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'D. '.$AWISSprachKonserven['IBG']['IBG_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_D1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 190, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 190, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_D2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 195, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 195, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_D3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 200, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 200, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(180,5,'Bemerkung:','LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(180,5,'','LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(180,5,'','LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(180,5,'','LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'E. '.$AWISSprachKonserven['IBG']['IBG_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_E1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 230, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 230, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_E2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 235, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 235, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_E3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 240, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 240, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['IBG']['IBG_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_F1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 250, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 250, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_F2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 255, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 255, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_F3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 260, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 260, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_F4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 265, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 265, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_F5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 270, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 270, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_F6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 275, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 275, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->NeueSeite(0,1);

$Spalte = 15;
$Zeile = 5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch GBL: Typ C','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 2 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 23.10.2012','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.0','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.1','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['IBG']['IBG_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_G1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 47, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 47, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'8. '.$AWISSprachKonserven['IBG']['IBG_G2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 52, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 52, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'9. '.$AWISSprachKonserven['IBG']['IBG_G3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 57, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 57, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'H. '.$AWISSprachKonserven['IBG']['IBG_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_H1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 67, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 67, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_H2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 72, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 72, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_H3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 77, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 77, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_H4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 82, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 82, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_H5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 87, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 87, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_H6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 92, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 92, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_H7'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 97, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 97, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'8. '.$AWISSprachKonserven['IBG']['IBG_H8'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 102, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 102, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'9. '.$AWISSprachKonserven['IBG']['IBG_H9'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 107, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 107, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'10. '.$AWISSprachKonserven['IBG']['IBG_H10'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 112, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 112, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'11. '.$AWISSprachKonserven['IBG']['IBG_H11'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 117, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 117, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'12. '.$AWISSprachKonserven['IBG']['IBG_H12'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 122, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 122, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'13. '.$AWISSprachKonserven['IBG']['IBG_H13'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 128.5, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 128.5, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'I. '.$AWISSprachKonserven['IBG']['IBG_I'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_I1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 140, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 140, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_I2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 145, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 145, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_I3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 150, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 150, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_I4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 155, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 155, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_I5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 160, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 160, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_I6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 165, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 165, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_I7'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 170, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 170, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->MultiCell(110,5,'J. '.$AWISSprachKonserven['IBG']['IBG_J'],'LTRB','L',1);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,10,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,10,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,10,'','LTRB','L',1);
$Spalte-=110;

$Zeile+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_J1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 185, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 185, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_J2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 190, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 190, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_J3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 195, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 195, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_J4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 200, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 200, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_J5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 205, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 205, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_J6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 210, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 210, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'K. '.$AWISSprachKonserven['IBG']['IBG_K'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_K1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 220, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 220, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=70;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Datum:','T',0,'C',0);

$Spalte+=40;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift WL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL:','T',0,'C',0);



}
elseif($_GET['PEI_KEY'] != 0)
{

$SQL = 	'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS '; 
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= 'WHERE PEZ_PEI_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEI_KEY']);
	
$rsBericht = $DB->RecordSetOeffnen($SQL);	

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch GBL: Typ C','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 23.10.2012','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.0','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.1','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['IBG']['IBG_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,$rsBericht->FeldInhalt('PEI_FIL_ID'),'B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITVON'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITBIS'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['IBG']['IBG_Datum'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,substr($rsBericht->FeldInhalt('PEI_DATUM'),0,10),'B',0,'C',0);
$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,'A. '.$AWISSprachKonserven['IBG']['IBG_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_A1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,301, 128.5, 62,143, 62);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'2. '.$AWISSprachKonserven['IBG']['IBG_A2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,302, 128.5, 69,143, 69,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_A3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,303, 128.5, 75,143,75);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_A4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,304, 128.5, 80,143,80);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'B. '.$AWISSprachKonserven['IBG']['IBG_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'1. '.$AWISSprachKonserven['IBG']['IBG_B1'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,306, 128.5, 91.5,143,91.5,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_B2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,307, 128.5, 98,143,98);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'3. '.$AWISSprachKonserven['IBG']['IBG_B3'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,308, 128.5, 104.5,143,104.5,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'4. '.$AWISSprachKonserven['IBG']['IBG_B4'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,309, 128.5, 112.5,143,112.5,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'5. '.$AWISSprachKonserven['IBG']['IBG_B5'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,310, 128.5, 120.5,143,120.5,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_B6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,311, 128.5, 127,143,127);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_B7'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,312, 128.5, 132,143,132);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'8. '.$AWISSprachKonserven['IBG']['IBG_B8'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,313, 128.5, 137,143,137);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'9. '.$AWISSprachKonserven['IBG']['IBG_B9'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,314, 128.5, 143.5,143,143.5,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'10. '.$AWISSprachKonserven['IBG']['IBG_B10'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,315, 128.5, 150,143,150);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'C. '.$AWISSprachKonserven['IBG']['IBG_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_C1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,317, 128.5, 160,143,160);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_C2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,318, 128.5, 165,143,165);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_C3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,319, 128.5, 170,143,170);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_C4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,320, 128.5, 175,143,175);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_C5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,321, 128.5, 180,143,180);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'D. '.$AWISSprachKonserven['IBG']['IBG_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_D1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,323, 128.5, 190,143,190);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_D2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,324, 128.5, 195,143,195);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_D3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,325, 128.5, 200,143,200);

$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
$SQL .= ' AND PEZ_PBF_ID=326';

$rsBemerkung = $DB->RecordSetOeffnen($SQL);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(180,5,'Bemerkung:'.$rsBemerkung->FeldInhalt('PEZ_BEMERKUNG'),'LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(180,5,'','LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(180,5,'','LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(180,5,'','LTRB',0,'L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'E. '.$AWISSprachKonserven['IBG']['IBG_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_E1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,328, 128.5, 230,143,230);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_E2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,329, 128.5, 235,143,235);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_E3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,330, 128.5, 240,143,240);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['IBG']['IBG_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_F1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,332, 128.5, 250,143,250);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_F2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,333, 128.5, 255,143,255);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_F3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,334, 128.5, 260,143,260);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_F4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,335, 128.5, 265,143,265);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_F5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,336, 128.5, 270,143,270);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_F6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,337, 128.5, 275,143,275);

$Ausdruck->NeueSeite(0,1);

$Spalte = 15;
$Zeile = 5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/atulogo_neu_klein.jpg', 19, 15);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Intensiv - Besuch GBL: Typ C','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/NL/CH/CZ/IT','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'Vertrieb: TKD','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 2 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 23.10.2012','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'Ersetzt Version 1.0','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.1','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'F. '.$AWISSprachKonserven['IBG']['IBG_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_G1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,339, 128.5, 47,143,47);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'8. '.$AWISSprachKonserven['IBG']['IBG_G2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,340, 128.5, 52,143,52);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'9. '.$AWISSprachKonserven['IBG']['IBG_G3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,341, 128.5, 57,143,57);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'H. '.$AWISSprachKonserven['IBG']['IBG_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_H1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,344, 128.5, 67,143,67);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_H2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,345, 128.5, 72,143,72);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_H3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,346, 128.5, 77,143,77);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_H4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,347, 128.5, 82,143,82);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_H5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,348, 128.5, 87,143,87);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_H6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,349, 128.5, 92,143,92);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_H7'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,350, 128.5, 97,143,97);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'8. '.$AWISSprachKonserven['IBG']['IBG_H8'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,351, 128.5, 102,143,102);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'9. '.$AWISSprachKonserven['IBG']['IBG_H9'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,352, 128.5, 107,143,107);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'10. '.$AWISSprachKonserven['IBG']['IBG_H10'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,353, 128.5, 112,143,112);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'11. '.$AWISSprachKonserven['IBG']['IBG_H11'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,354, 128.5, 117,143,117);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'12. '.$AWISSprachKonserven['IBG']['IBG_H12'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,355, 128.5, 122,143,122);

$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,'13. '.$AWISSprachKonserven['IBG']['IBG_H13'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,356, 128.5, 128.5,143,128.5,8);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'I. '.$AWISSprachKonserven['IBG']['IBG_I'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_I1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,358, 128.5, 140,143,140);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_I2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,359, 128.5, 145,143,145);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_I3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,360, 128.5, 150,143,150);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_I4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,361, 128.5, 155,143,155);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_I5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,362, 128.5, 160,143,160);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_I6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,363, 128.5, 165,143,165);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'7. '.$AWISSprachKonserven['IBG']['IBG_I7'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,364, 128.5, 170,143,170);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->MultiCell(110,5,'J. '.$AWISSprachKonserven['IBG']['IBG_J'],'LTRB','L',1);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,10,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,10,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,10,'','LTRB','L',1);
$Spalte-=110;

$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_J1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,366, 128.5, 185,143,185);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'2. '.$AWISSprachKonserven['IBG']['IBG_J2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,367, 128.5, 190,143,190);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'3. '.$AWISSprachKonserven['IBG']['IBG_J3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,368, 128.5, 195,143,195);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'4. '.$AWISSprachKonserven['IBG']['IBG_J4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,369, 128.5, 200,143,200);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'5. '.$AWISSprachKonserven['IBG']['IBG_J5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,370, 128.5, 205,143,205);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'6. '.$AWISSprachKonserven['IBG']['IBG_J6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,371, 128.5, 210,143,210);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'K. '.$AWISSprachKonserven['IBG']['IBG_K'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,'1. '.$AWISSprachKonserven['IBG']['IBG_K1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,373, 128.5, 220,143,220);

$Zeile+=53;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Datum:','T',0,'C',0);

$Spalte+=40;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift WL:','T',0,'C',0);

$Spalte+=50;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL:','T',0,'C',0);


$Zeile+=5;
$Spalte-=140;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";

$rsGesamt = $DB->RecordSetOeffnen($SQL);

$Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');

echo "Gesamtanzahl:".$Gesamtanzahl;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= "AND PEZ_WERT = '0'";

$GesamtFalsch = $DB->RecordSetOeffnen($SQL);

$Falsch = $GesamtFalsch->FeldInhalt('ANZ');

echo "Falsch".$Falsch;

$Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) *100); 

if($Prozentsatz >= 90 && $Prozentsatz <= 100)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',87.5),'',0,'C',0);
	$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GR�N)','',0,'R',0);
	
}
elseif($Prozentsatz >= 80 && $Prozentsatz <= 90)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',87.5),'',0,'C',0);
	$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(97.5,5,'(GELB)','',0,'R',0);
	
}
elseif($Prozentsatz < 80)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	//$Ausdruck->_pdf->cell2(180,4,'Hallo','LTRB',0,'L',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',$Spalte+72.5,$Zeile,'','');
	
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=15;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(180,5,'(ROT)','',0,'C',0);
	//$Ausdruck->_pdf->Image($file, $x, $y,$w,$h);
}
$Zeile+=4;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(180,5,'Erf�llungsgrad: min. 75 % der Felder, Erf�llungsrating: 100% - 90% = Gr�n, 89% - 80% = Gelb, < 80% = Rot   ','',0,'C',0);
	


}


$Ausdruck->Anzeigen();


?>