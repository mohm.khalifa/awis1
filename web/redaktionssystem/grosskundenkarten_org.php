<?php
require_once("register.inc.php");

require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($PHP_AUTH_USER) .">";

// Variablen
global $_POST;
global $_GET;
global $GKK_Wert;
global $gkk_date;

include ("ATU_Header.php");	// Kopfzeile

clearstatcache();

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m&ouml;glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

$RechteStufe = awisBenutzerRecht($con, 805);
if($RechteStufe==0)
{
   awisEreignis(3,1000,'Redaktionssystem: Grosskundenkarten',$_SERVER['PHP_AUTH_USER'],'','','');
   die("Keine ausreichenden Rechte!");
}

echo "<form name=frmGrosskundenkarte method=post action=./grosskundenkarten.php>";

echo '<tr><td width=100%>Karten erzeugen: </td>';
echo '<td><select name=txt_gkk>';

$rsGKK = awisOpenRecordset($con, "SELECT ROWNUM AS NR, COLUMN_VALUE AS GKK_STR FROM table(gkk_pivot)");
$rsGKKZeilen = $awisRSZeilen;

echo "<option value=0>::Neue erstellen::</option>";
for($GKKZeile=0;$GKKZeile<$rsGKKZeilen;$GKKZeile++)
{
	echo "<option value=" . $rsGKK['NR'][$GKKZeile] . ">" . $rsGKK['GKK_STR'][$GKKZeile] . "</option>";
	$GKK_Wert[$rsGKK['NR'][$GKKZeile]]=$rsGKK['GKK_STR'][$GKKZeile];
}

echo "</select></td></tr>";

if($_POST['cmdEingabe_x']!='')
{ 
	if($_POST['txt_gkk']==0) 
	{
		$SQL = "SELECT  st.name1 || ';' || st.name2 || ';' || st.name3 || ';' || ";
		$SQL .= "k.kunden_nr || ';' || k.gkb_staat || ';' || k.karten_nr ";
		$SQL .= "|| ';' || TO_CHAR(gueltigkeit, 'MMYY') || ';' || ";
		$SQL .= "LPAD(TO_CHAR(k.kunden_nr), 6, '0') || LPAD(TO_CHAR(k.gkb_staat), 2, '0') ";
		$SQL .= "|| LPAD(TO_CHAR(k.karten_nr), 4, '0') || TO_CHAR (k.pruefziffer) ";
		$SQL .= "|| ';' || ";
		$SQL .= "filsys.p_barcode.ean13@atu_wen1.atu.de( ";
		$SQL .= "LPAD(TO_CHAR(k.kunden_nr), 6, '0') || LPAD(TO_CHAR(k.gkb_staat), 2, '0') ";
		$SQL .= "|| LPAD(TO_CHAR(k.karten_nr), 4, '0') || TO_CHAR (k.pruefziffer)) GKK ";
		$SQL .= "from filsys.grosskundenkarte@atu_wen1.atu.de k, filsys.grosskundest@atu_wen1.atu.de st ";
		$SQL .= "WHERE k.kunden_nr = st.kunden_nr and k.gkb_staat = st.gkb_staat and k.letzter_druck IS NULL ";
		$SQL .= "ORDER by k.kunden_nr, k.karten_nr";
		
		$rsGrosskundenkarten = awisOpenRecordset($con, $SQL);
		$rsGrosskundenkartenZeilen = $awisRSZeilen;
		$DateiName = awis_UserExportDateiName('.' . $_POST['txtFormat']);
		$DateiName .= 'txt';
		$DateiNameLink = pathinfo($DateiName);
		$DateiNameLink = '/export/' . $DateiNameLink['basename'];
			
		$fd = fopen($DateiName,'w');
		if($fd==FALSE)
		{
			die($DateiName);
		}
		
		for($DSNr=0;$DSNr<$rsGrosskundenkartenZeilen;$DSNr++)
		{
			fputs($fd, $rsGrosskundenkarten['GKK'][$DSNr] . "\r\n");
		}
		fclose($fd);
		
		echo "<br>Es wurden " . $rsGrosskundenkartenZeilen  . " Zeilen ausgegeben!<br>";
		
		echo "<br><a href=$DateiNameLink>Datei &ouml;ffnen</a><br>";
		
		$SQL = "UPDATE filsys.grosskundenkarte@atu_wen1.atu.de ";
		$SQL .= "SET filsys.grosskundenkarte.letzter_druck@atu_wen1.atu.de=SYSDATE ";
		$SQL .= "WHERE filsys.grosskundenkarte.letzter_druck@atu_wen1.atu.de IS NULL";	
		
		//$Erg=awisExecute($con, $SQL);
		
		if($Erg==FALSE)
		{
			awisErrorMailLink("grosskundenkarten.php",2,$awisDBFehler['message']);
		}
	}
	
	if($_POST['txt_gkk']>0) 
	{
	
		$gkk_date=trim(substr($GKK_Wert[intval($_POST['txt_gkk'])],0,strpos($GKK_Wert[$_POST['txt_gkk']],'|')-1));
		
		$SQL = "SELECT  st.name1 || ';' || st.name2 || ';' || st.name3 || ';' || ";
		$SQL .= "k.kunden_nr || ';' || k.gkb_staat || ';' || k.karten_nr ";
		$SQL .= "|| ';' || TO_CHAR(gueltigkeit, 'MMYY') || ';' || ";
		$SQL .= "LPAD(TO_CHAR(k.kunden_nr), 6, '0') || LPAD(TO_CHAR(k.gkb_staat), 2, '0') ";
		$SQL .= "|| LPAD(TO_CHAR(k.karten_nr), 4, '0') || TO_CHAR (k.pruefziffer) ";
		$SQL .= "|| ';' || ";
		$SQL .= "filsys.p_barcode.ean13@atu_wen1.atu.de( ";
		$SQL .= "LPAD(TO_CHAR(k.kunden_nr), 6, '0') || LPAD(TO_CHAR(k.gkb_staat), 2, '0') ";
		$SQL .= "|| LPAD(TO_CHAR(k.karten_nr), 4, '0') || TO_CHAR (k.pruefziffer)) GKK ";
		$SQL .= "from filsys.grosskundenkarte@atu_wen1.atu.de k, filsys.grosskundest@atu_wen1.atu.de st ";
		$SQL .= "WHERE k.kunden_nr = st.kunden_nr and k.gkb_staat = st.gkb_staat ";
		$SQL .= "AND k.letzter_druck=to_date('" . $gkk_date . "','DD.MM.RR HH24:MI:SS') ORDER by k.kunden_nr, k.karten_nr";
		
		$rsGrosskundenkarten = awisOpenRecordset($con, $SQL);
		$rsGrosskundenkartenZeilen = $awisRSZeilen;
		$DateiName = awis_UserExportDateiName('.' . $_POST['txtFormat']);
		$DateiName .= 'txt';
		$DateiNameLink = pathinfo($DateiName);
		$DateiNameLink = '/export/' . $DateiNameLink['basename'];
			
		$fd = fopen($DateiName,'w');
		if($fd==FALSE)
		{
			die($DateiName);
		}
		
		for($DSNr=0;$DSNr<$rsGrosskundenkartenZeilen;$DSNr++)
		{
			fputs($fd, $rsGrosskundenkarten['GKK'][$DSNr] . "\r\n");
		}
		fclose($fd);
		
		echo "<br>Es wurden " . $rsGrosskundenkartenZeilen  . " Zeilen ausgegeben!<br>";
		
		echo "<br><a href=$DateiNameLink>Datei &ouml;ffnen</a><br>";
		
		$SQL = "UPDATE filsys.grosskundenkarte@atu_wen1.atu.de ";
		$SQL .= "SET filsys.grosskundenkarte.letzter_druck@atu_wen1.atu.de=SYSDATE ";
		$SQL .= "WHERE filsys.grosskundenkarte.letzter_druck@atu_wen1.atu.de=to_date('" . $gkk_date . "','DD.MM.RR HH24:MI:SS')";	
		
		$Erg=awisExecute($con, $SQL);
		
		if($Erg==FALSE)
		{
			awisErrorMailLink("grosskundenkarten.php",2,$awisDBFehler['message']);
		}
	}
}
	
	awislogoff($con);
	
	echo "<br><input type=image title=Ausf&uuml;hren src=/bilder/eingabe_ok.png name=cmdEingabe onclick=location.href='./grosskundenkarten.php'>";

echo '</form>';
	
echo "<br><input type=image title=Zur&uuml;ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='./redaktionssystem_Main.php'>";
?>
