<html>
<head>

<title>Awis - ATU webbasierendes Informationssystem</title>

<?
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->BenutzerName()) .">";
?>
</head>

<body>
<?
global $awisRSZeilen;
global $PER_NR;
//global $HTTP_REFERER;
//global $AWISBenutzer;
//awis_Debug(1, $AWISBenutzer->BenutzerName());
//die();

include ("ATU_Header.php");	// Kopfzeile

$con = awislogon();
if($con==FALSE)
{
	die("<h2><font color=#FF0000>Keine Datenbankverbindung m�glich. Bitte kontaktieren Sie den Administrator.</font></h2>");
}

if(awisBenutzerRecht($con,110)==0)
{
    awisEreignis(3,1000,'Schichtplan',$AWISBenutzer->BenutzerName(),'','','');
    die("Keine ausreichenden Rechte!");
}

$PER_NR=$_REQUEST['PER_NR'];

$AnzeigeRechteStufe = awisBenutzerRecht($con, 115);

$rsPersKuerzel='';

$SQL="SELECT * FROM PKUERZEL WHERE PEK_USER=(SELECT DISTINCT PER_USER FROM PERSONAL WHERE PER_NR='".$PER_NR."') ORDER BY PEK_ID";

$rsTaetigkeit = awisOpenRecordset($con,$SQL);
$rsTaetigkeitZeilen = $awisRSZeilen;

for($i=0;$i<$rsTaetigkeitZeilen;$i++)
{
	$rsPersKuerzel = array_merge($rsPersKuerzel, array($rsTaetigkeit["PEK_ID"][$i] => $rsTaetigkeit["PEK_ANZEIGESTUFE"][$i]));
}

$SQL = "SELECT PER_Nachname, PER_Vorname, PER_EMail, PER_MOBILTELEFON, PERSONALEINSATZPLANUNG.* FROM PERSONALEINSATZPLANUNG, PERSONAL ";
$SQL .= " WHERE PER_NR='" . $PER_NR . "'";
$SQL .= " AND PEP_PER_NR(+) = PER_NR";
$SQL .= " ORDER BY PEP_JAHR DESC, PEP_KALENDERWOCHE DESC";

$rsSchichtPlan = awisOpenRecordset($con, $SQL);
$rsSchichtPlanZeilen = $awisRSZeilen;

awis_Debug(1,$SQL);

print "<table width=100% border=0><tr><td align=left><span class=DatenFeldGross>Schichtplan f�r " . $rsSchichtPlan["PER_NACHNAME"][0] . ", " . $rsSchichtPlan["PER_VORNAME"][0] . "</span></td>";
if (isset($_REQUEST['FILID']))
{
	print "<td align=right><input align=right type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='".$_SERVER["HTTP_REFERER"].(strpos($_SERVER["HTTP_REFERER"],'cmdAktion=Filialinfos')===false?"?cmdAktion=Filialinfos&":'').(strpos($_SERVER["HTTP_REFERER"],'FILID')===false?"&FILID=".$_REQUEST['FILID']:'') ."'></td></tr></table>";
}else {
	print "<td align=right><input align=right type=image alt=Zur�ck src=/bilder/zurueck.png name=cmdZurueck onclick=location.href='".$_SERVER["HTTP_REFERER"].(strpos($_SERVER["HTTP_REFERER"],'cmdAktion=Filialinfos')===false?"?cmdAktion=Filialinfos&":'')."'></td></tr></table>";
}
print "<br>Mobilfunk: " . $rsSchichtPlan['PER_MOBILTELEFON'][0] . "";
print "<br>E-Mail: <a href=mailto:" . $rsSchichtPlan["PER_EMAIL"][0] . ">" . $rsSchichtPlan["PER_EMAIL"][0] . "</a>";
print "<hr>";


print "<table width=100% border=0>";
print "<tr><td id=Ueberschrift width=100>Woche</td><td width=50 id=Ueberschrift>Mo</td><td width=50 id=Ueberschrift>Di</td><td width=50 id=Ueberschrift>Mi</td><td width=50 id=Ueberschrift>Do</td><td width=50 id=Ueberschrift>Fr</td><td width=50 id=Ueberschrift>Sa</td><td width=50 id=Ueberschrift>So</td><td id=Ueberschrift>Bemerkung</td></tr>";

$MaxZeilen=intval(awis_BenutzerParameter($con,'Filialen:Anzahl Wochen Schichtplan',$AWISBenutzer->BenutzerName()));
for($i=0;$i<$rsSchichtPlanZeilen;$i++)
{
	if(strftime("%V%Y",time()) == $rsSchichtPlan["PEP_KALENDERWOCHE"][$i].$rsSchichtPlan["PEP_JAHR"][$i])
	{
		$format = "DatenFeldNormalFett";
		$FeldFiller="--";
		
		for($j=$i-1;$j<=$i+$MaxZeilen;$j++)
		{
			if(strftime("%V%Y",time()) == $rsSchichtPlan["PEP_KALENDERWOCHE"][$j].$rsSchichtPlan["PEP_JAHR"][$j])
			{
				$format = "DatenFeldNormalFett";
			}
			else
			{
				$format = "DatenFeldNormal";
			}
			print "<tr><td><span class=$format>" . $rsSchichtPlan["PEP_KALENDERWOCHE"][$j] . "/" . $rsSchichtPlan["PEP_JAHR"][$j] . "</span></td>";

				//Wochentage
			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_PEK_ID_MONTAG"][$j]) && ($rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_MONTAG"][$j]] & $AnzeigeRechteStufe)==$rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_MONTAG"][$j]]?$rsSchichtPlan["PEP_PEK_ID_MONTAG"][$j]:$FeldFiller) . "</span></td>";
			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_PEK_ID_DIENSTAG"][$j]) && ($rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_DIENSTAG"][$j]] & $AnzeigeRechteStufe)==$rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_DIENSTAG"][$j]]?$rsSchichtPlan["PEP_PEK_ID_DIENSTAG"][$j]:$FeldFiller) . "</span></td>";
			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_PEK_ID_MITTWOCH"][$j]) && ($rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_MITTWOCH"][$j]] & $AnzeigeRechteStufe)==$rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_MITTWOCH"][$j]]?$rsSchichtPlan["PEP_PEK_ID_MITTWOCH"][$j]:$FeldFiller) . "</span></td>";
			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_PEK_ID_DONNERSTAG"][$j]) && ($rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_DONNERSTAG"][$j]] & $AnzeigeRechteStufe)==$rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_DONNERSTAG"][$j]]?$rsSchichtPlan["PEP_PEK_ID_DONNERSTAG"][$j]:$FeldFiller) . "</span></td>";
			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_PEK_ID_FREITAG"][$j]) && ($rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_FREITAG"][$j]] & $AnzeigeRechteStufe)==$rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_FREITAG"][$j]]?$rsSchichtPlan["PEP_PEK_ID_FREITAG"][$j]:$FeldFiller) . "</span></td>";
			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_PEK_ID_SAMSTAG"][$j]) && ($rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_SAMSTAG"][$j]] & $AnzeigeRechteStufe)==$rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_SAMSTAG"][$j]]?$rsSchichtPlan["PEP_PEK_ID_SAMSTAG"][$j]:$FeldFiller) . "</span></td>";
			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_PEK_ID_SONNTAG"][$j]) && ($rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_SONNTAG"][$j]] & $AnzeigeRechteStufe)==$rsPersKuerzel[$rsSchichtPlan["PEP_PEK_ID_SONNTAG"][$j]]?$rsSchichtPlan["PEP_PEK_ID_SONNTAG"][$j]:$FeldFiller) . "</span></td>";

			print "<td><span class=$format>" . (isset($rsSchichtPlan["PEP_BEMERKUNG"][$j])?$rsSchichtPlan["PEP_BEMERKUNG"][$j]:'') . "</span></td>";

		}
		 	break;
	}
	else
	{
	}


	print "</tr>";
}

print "</table>";


/*********************************
* Legende ausgeben
* *******************************/

print "<hr><br><span class=DatenFeldGross>Legende</span><br>";

for($j=0;$j<$rsTaetigkeitZeilen;$j++)
{
	if($rsTaetigkeit["PEK_ANZEIGESTUFE"][$j] <= $AnzeigeRechteStufe)
	{
		print "<br><font size=1>" . $rsTaetigkeit["PEK_ID"][$j] . " = " . $rsTaetigkeit["PEK_TEXT"][$j] . "</font>";
	}
}

awislogoff($con);

?>
</body>
</html>
