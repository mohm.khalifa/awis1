<?php


try {
    global $AWISBenutzer;
    global $AWIS_KEY1;
    global $AWISCursorPosition;

    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[] = array('LIE', '*');
    $TextKonserven[] = array('Wort', 'lbl_weiter');
    $TextKonserven[] = array('Wort', 'lbl_speichern');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_hilfe');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Wort', 'lbl_trefferliste');
    $TextKonserven[] = array('Wort', 'DateiOeffnen');
    $TextKonserven[] = array('Fehler', 'err_keineDaten');
    $TextKonserven[] = array('Fehler', 'err_keineDatenbank');
    $TextKonserven[] = array('FEHLER', 'err_UngueltigesUploadDateiFormat');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht612 = $AWISBenutzer->HatDasRecht(612);
    if ($Recht612 == 0) {
        $Form->Fehler_KeineRechte();
    }

    $Form->Formular_Start();

    $SQL = 'SELECT * ';
    $SQL .= 'FROM informationstypen ';
    $SQL .= 'LEFT OUTER JOIN lieferanteninfos ';
    $SQL .= 'ON ity_key = lin_ity_key ';
    $SQL .= 'AND lin_lie_nr = :var_lie_nr ';
    $SQL .= 'WHERE ity_status = \'A\' ';
    $SQL .= 'AND ity_xtn_kuerzel = \'LIE\' ';
    $SQL .= 'AND ity_bereich = :var_ity_bereich ';
    $SQL .= 'ORDER BY ity_sortierung';
    $Form->DebugAusgabe(1, $SQL, $AWIS_KEY1);
    $BindeVariablen = array();
    $BindeVariablen['var_lie_nr'] = $AWIS_KEY1;
    $BindeVariablen['var_ity_bereich'] = 'Listendruck';
    $rsItyListendruck = $DB->RecordsetOeffnen($SQL, $BindeVariablen);

    $Form->Trennzeile();

    $Form->ZeileStart();
    $Form->Erstelle_TextFeld('Ueberschrift' . 'Listendruck', $AWISSprachKonserven['LIE']['ITY_Listendruck'], 100, 600, false);
    $Form->ZeileEnde();

    while (!$rsItyListendruck->EOF()) {
        $ItyName = 'LIN_WERT' . '_' . $rsItyListendruck->FeldInhalt('ITY_KEY') . '_' . $rsItyListendruck->FeldInhalt('LIN_KEY') . '_' . $rsItyListendruck->FeldInhalt('ITY_FORMAT');

        $ItyZeichen = $rsItyListendruck->FeldInhalt('ITY_ZEICHEN');
        $ItyBreite = $rsItyListendruck->FeldInhalt('ITY_BREITE');
        $ItyFormat = $rsItyListendruck->FeldInhalt('ITY_FORMAT');
        $AendernRecht = !$rsItyListendruck->FeldInhalt('ITY_READONLY');
        $LinWert = $rsItyListendruck->FeldInhalt('LIN_WERT');

        if (!empty($AWISSprachKonserven['LIE']['ITY_' . $rsItyListendruck->FeldInhalt('ITY_INFORMATION')])) {
            $Form->ZeileStart();
            $ZeileGestartet = true;
            $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['ITY_' . $rsItyListendruck->FeldInhalt('ITY_INFORMATION')] . ':', 300);
        }

        switch (substr($rsItyListendruck->FeldInhalt('ITY_DATENQUELLE'), 0, 3)) {
            case 'TXT':
                if (!empty($LinWert)) {
                    if ($ItyFormat == 'D') {
                        $ItyFormat = 'T';
                        $LinWert = substr($LinWert, 4, 2) . '.' . substr($LinWert, 2, 2) . '.20' . substr($LinWert, 0, 2);
                    }
                    $Form->Erstelle_TextFeld($ItyName, $LinWert, $ItyZeichen, $ItyBreite, $AendernRecht, '', '', '', $ItyFormat);
                    if ($ItyFormat == 'N2') {
                        $Form->Erstelle_TextLabel('EUR', 300, 'font-weight:bold');
                    }
                }
                break;
            case 'CHK':
                $Form->Erstelle_HiddenFeld($ItyName, 'off');
                $Form->Erstelle_Checkbox($ItyName, $LinWert, $ItyBreite, $AendernRecht, 'on');
                break;
            default:
                $Form->Erstelle_TextFeld($ItyName, $LinWert, $ItyZeichen, $ItyBreite, $AendernRecht, '', '', '', $ItyFormat);
                break;
        }
        if ($ZeileGestartet) {
            $Form->ZeileEnde();
        }
        $ZeileGestartet = false;
        $rsItyListendruck->DSWeiter();
    }

    $ItyBereiche = array(
        1 => 'Sonstiges',
        2 => 'ReklPausch',
        3 => 'Warenrueck',
        4 => 'Avisierung',
        5 => 'Unterlagen'
    );

    $FirstRun = true;

    foreach ($ItyBereiche AS $ItyWert => $ItyBereich) {
        /*
        $SQL = 'Select * FROM informationstypen ';
        $SQL .= 'WHERE ity_xtn_kuerzel = \'LIE\' ';
        $SQL .= 'AND ity_status = \'A\' ';
        $SQL .= 'AND ity_bereich = \'' . $ItyBereich . '\' ';
        $SQL .= 'ORDER BY ity_sortierung';
        */

        $SQL = 'SELECT * ';
        $SQL .= 'FROM informationstypen ';
        $SQL .= 'LEFT OUTER JOIN lieferanteninfos ';
        $SQL .= 'ON ity_key = lin_ity_key ';
        $SQL .= 'AND lin_lie_nr = :var_lie_nr ';
        $SQL .= 'WHERE ity_status = \'A\' ';
        $SQL .= 'AND ity_xtn_kuerzel = \'LIE\' ';
        $SQL .= 'AND ity_bereich = :var_ity_bereich ';
        $SQL .= 'ORDER BY ity_sortierung';

        $BindeVariablen = array();
        $BindeVariablen['var_lie_nr'] = $AWIS_KEY1;
        $BindeVariablen['var_ity_bereich'] = $ItyBereich;
        $rsIty = $DB->RecordsetOeffnen($SQL, $BindeVariablen);

        $Form->Trennzeile();

        $Form->ZeileStart();
        $Form->Erstelle_TextFeld('Ueberschrift' . $ItyBereiche[$ItyWert], $AWISSprachKonserven['LIE']['ITY_' . $ItyBereiche[$ItyWert]], 100, 600, false);
        $Form->ZeileEnde();

        $ChkZ�hler = 0;
        while (!$rsIty->EOF()) {
            $ItyName = 'LIN_WERT' . '_' . $rsIty->FeldInhalt('ITY_KEY') . '_' . $rsIty->FeldInhalt('LIN_KEY') . '_' . $rsIty->FeldInhalt('ITY_FORMAT');

            $ItyZeichen = $rsIty->FeldInhalt('ITY_ZEICHEN');
            $ItyBreite = $rsIty->FeldInhalt('ITY_BREITE');
            $ItyFormat = $rsIty->FeldInhalt('ITY_FORMAT');

            $AendernRecht = ((($Recht612 & $rsIty->FeldInhalt('ITY_RECHTESTUFEBEARBEITEN')) != 0) AND ($rsIty->FeldInhalt('ITY_READONLY') == 0));
            $LinWert = $rsIty->FeldInhalt('LIN_WERT');

            if ($ChkZ�hler == 0 OR substr($rsIty->FeldInhalt('ITY_DATENQUELLE'), 0, 3) != 'CHK') {
                $Form->ZeileStart();
            }
            if (substr($rsIty->FeldInhalt('ITY_DATENQUELLE'), 0, 3) != 'CHK') {
                if ($rsIty->FeldInhalt('ITY_INFORMATION') == 'Leerzeile') {
                    $Form->Trennzeile('O');
                } else {
                    $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['ITY_' . $rsIty->FeldInhalt('ITY_INFORMATION')] . ':', 300);
                }
            }
            switch (substr($rsIty->FeldInhalt('ITY_DATENQUELLE'), 0, 3)) {
                case 'TXT':
                    $ChkZ�hler = 0;
                    $Form->Erstelle_TextFeld($ItyName, $LinWert, $ItyZeichen, $ItyBreite, $AendernRecht, '', '', '', $ItyFormat);

                    if ($FirstRun) {
                        $AWISCursorPosition = 'txt' . $ItyName;
                        $FirstRun = false;
                    }
                    if ($ItyFormat == 'N2') {
                        $Form->Erstelle_TextLabel('%', 300);
                    }
                    break;
                case 'CHK':
                    $Form->Erstelle_HiddenFeld($ItyName, 'off');
                    $Form->Erstelle_Checkbox($ItyName, $LinWert, $ItyBreite, $AendernRecht, 'on');
                    if ($ChkZ�hler == 0) {
                        $ChkZ�hler = 1;
                    } else {
                        $ChkZ�hler = 0;
                    }
                    break;
                case 'LST':
                    $Felder = explode(':', $rsIty->FeldInhalt('ITY_DATENQUELLE'));
                    $Daten = $Form->LadeTexte(array(array($Felder[1], $Felder[2])));
                    $Daten = explode('|', $Daten[$Felder[1]][$Felder[2]]);
                    $Form->Erstelle_SelectFeld($ItyName, $LinWert, $ItyBreite, $AendernRecht, '', $AWISSprachKonserven['Wort']['txt_BitteWaehlen'], '', '', '', $Daten);
                    break;
                case 'MEM':
                    $Felder = explode(':', $ItyZeichen);
                    $Spalten = $Felder[0];
                    $Zeilen = $Felder[1];
                    $Form->Erstelle_Textarea($ItyName, $LinWert, $ItyBreite, $Spalten, $Zeilen, $AendernRecht);
                    break;
                default:
                    $Form->Erstelle_TextFeld($ItyName, $LinWert, $ItyZeichen, $ItyBreite, $AendernRecht, '', '', '', $ItyFormat);
                    break;
            }
            if (substr($rsIty->FeldInhalt('ITY_DATENQUELLE'), 0, 3) == 'CHK') {
                $Form->Erstelle_TextLabel($AWISSprachKonserven['LIE']['ITY_' . $rsIty->FeldInhalt('ITY_INFORMATION')], 276);
            }
            if (substr($rsIty->FeldInhalt('ITY_DATENQUELLE'), 0, 3) != 'CHK') {
                $Form->ZeileEnde();
            } elseif ($ChkZ�hler == 0 and substr($rsIty->FeldInhalt('ITY_DATENQUELLE'), 0, 3) == 'CHK') {
                $Form->ZeileEnde();
            }
            $rsIty->DSWeiter();
        }
    }
    $Form->Formular_Ende();

    $Form->SetzeCursor($AWISCursorPosition);

    unset($rsIty);
    unset($ItyBereiche);
} catch (Exception $ex) {
}

?>