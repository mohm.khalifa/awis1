<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('FIL','%');
	$TextKonserven[]=array('MBW','%');
	$TextKonserven[]=array('MFI','%');
	$TextKonserven[]=array('MFR','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','INET_AdressenSuche');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht3022= $AWISBenutzer->HatDasRecht(3022);		// Mitbewerber in Filialinfo
	if(($Recht3022&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_MBWMFI'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$FilZugriff=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);
	$FilZugriffListe=explode(',',$FilZugriff);

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['SSort']))
	{
		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY MFI_ENTFERNUNG';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
		$Param['ORDER']=$ORDERBY;
	}

	if(isset($_GET['Edit']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['Edit']);
	}

	$SQL = 'SELECT MitbewerberFilialen.*, FIL_BEZ, FIL_ORT';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM MitbewerberFilialen ';
	$SQL .= ' INNER JOIN Filialen ON MFI_FIL_ID = FIL_ID';
	$SQL .= ' WHERE MFI_MBW_KEY ='.$AWIS_KEY1;
	if($AWIS_KEY2!=0)
	{
		$SQL .= ' AND MFI_KEY = '.$AWIS_KEY2;
	}

//$Form->DebugAusgabe(1,$SQL);

		// Aktuellen Block NICHT speichern-> gibt Probleme beim Hin-und-Her wechseln!!
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			//$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_MBWMFI',serialize($Param));
		}
		elseif(isset($Param['BLOCK']) AND $Param['FIL_ID']==$AWIS_KEY1)
		{
			//$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		if($Block==0)
		{
			$Block=1;
		}
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else

	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
		$SQL .= ' AND MFI_KEY = '.$AWIS_KEY2;
	}

	$SQL .= $ORDERBY;

	$rsMFI = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1,$SQL);
	$Param['FIL_ID']=$AWIS_KEY1;
	$AWISBenutzer->ParameterSchreiben('Formular_MBWMFI',serialize($Param));

	if($rsMFI->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0 OR isset($_GET['MFIListe']))						// Liste anzeigen
	{
		$DetailAnsicht = false;

		$BildschirmBreite = $AWISBenutzer->ParameterLesen('BildschirmBreite');
		if($BildschirmBreite<1024)
		{
			$Breiten['FIL_BEZ']=200;
		}
		elseif($BildschirmBreite<1280)
		{
			$Breiten['FIL_BEZ']=250;
		}
		else
		{
			$Breiten['FIL_BEZ']=250;
		}

		$Form->Formular_Start();

		$Form->ZeileStart();
		$Icons = array();
		if(($Recht3022&2)==2)
		{

			$NeuAnzeigen = true;
			while(!$rsMFI->EOF())
			{
				if(array_search($rsMFI->FeldInhalt('MFI_FIL_ID'),$FilZugriffListe)!==false)
				{
					$NeuAnzeigen=false;
					break;
				}
				$rsMFI->DSWeiter();
			}
			$rsMFI->DSErster();
			if($NeuAnzeigen)
			{
				$Icons[] = array('new','/mitbewerb/mitbewerb_Main.php?cmdAktion=Details&'.(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'').'&MBW_KEY='.$AWIS_KEY1.'&Edit=-1','g');
			}
		}
		$Form->Erstelle_ListeIcons($Icons,38,0);

		$Link = './mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=FIL_BEZ'.((isset($_GET['SSort']) AND ($_GET['SSort']=='FIL_BEZ'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['FIL']['FIL_BEZ'],$Breiten['FIL_BEZ'],'',$Link);
		$Link = './mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=MFI_ENTFERNUNG'.((isset($_GET['SSort']) AND ($_GET['SSort']=='MFI_ENTFERNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['MFI']['MFI_ENTFERNUNG'],100,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsMFI->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Icons = array();
			if(($Recht3022&4)==4)
			{
				$Icons[] = array('edit','../mitbewerb/mitbewerb_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&MBW_KEY='.$rsMFI->FeldInhalt('MFI_MBW_KEY').'&Edit='.$rsMFI->FeldInhalt('MFI_KEY'));
			}
			if(($Recht3022&8)==8)
			{
				if($FilZugriff!='' AND $rsMFI->FeldInhalt('MFI_KEY')!='')
				{
					if(array_search($rsMFI->FeldInhalt('MFI_FIL_ID'),$FilZugriffListe)!==false)
					{
						$Icons[] = array('delete','./mitbewerb_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Del='.$rsMFI->FeldInhalt('MFI_KEY'));
					}
				}
				else
				{
					$Icons[] = array('delete','./mitbewerb_Main.php?cmdAktion=Details&Seite=Filialzuordnungen&Del='.$rsMFI->FeldInhalt('MFI_KEY'));
				}
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Text = (strlen($rsMFI->FeldInhalt('FIL_BEZ'))>(int)($Breiten['FIL_BEZ']/$ListenSchriftFaktor)?substr($rsMFI->FeldInhalt('FIL_BEZ'),0,(int)($Breiten['FIL_BEZ']/$ListenSchriftFaktor)-2).'..':$rsMFI->FeldInhalt('FIL_BEZ'));
			$Form->Erstelle_ListenFeld('FIL_BEZ',$Text,0,$Breiten['FIL_BEZ'],false,($DS%2),'','','T','L',($Text!=$rsMFI->FeldInhalt('FIL_BEZ')?$rsMFI->FeldInhalt('FIL_BEZ'):''));
			$Form->Erstelle_ListenFeld('MFI_ENTFERNUNG',$rsMFI->FeldInhalt('MFI_ENTFERNUNG'),0,100,false,($DS%2),'','','N2','R','');
			$Form->ZeileEnde();

			$rsMFI->DSWeiter();
			$DS++;
		}

		$Link = './mitbewerb_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	else			// Deails zu einer Filiale
	{
		$Form->Formular_Start();

		$EditModus = ($Recht3022&6);
		if($FilZugriff!='' AND $rsMFI->FeldInhalt('MFI_KEY')!='')
		{
			if(array_search($rsMFI->FeldInhalt('MFI_FIL_ID'),$FilZugriffListe)===false)
			{
				$EditModus=false;
			}
		}

		$Form->Erstelle_HiddenFeld('MFI_KEY',$rsMFI->FeldInhalt('MFI_KEY'));
		$Form->Erstelle_HiddenFeld('MFI_MBW_KEY',$AWIS_KEY1);
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./mitbewerb_Main.php?cmdAktion=Details&MFIListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsMFI->FeldInhalt('MFI_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsMFI->FeldInhalt('MFI_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MFI']['MFI_FIL_ID'].':',150,'');

		if(count($FilZugriffListe)>1)
		{
			$SQL = 'SELECT FIL_ID, FIL_BEZ || \' (\'||FIL_ID||\')\' AS FilBez';
			$SQL .= ' FROM Filialen ';
			$SQL .= ' WHERE FIL_ID IN ('.$FilZugriff.')';
			$SQL .= ' ORDER BY FIL_BEZ';
			$Form->Erstelle_SelectFeld('MFI_FIL_ID',$rsMFI->FeldInhalt('MFI_FIL_ID'),150,$EditModus,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		}
		elseif($FilZugriff!='')
		{
			$FIL_ID = ($rsMFI->FeldInhalt('MFI_FIL_ID')==''?$FilZugriff:$rsMFI->FeldInhalt('MFI_FIL_ID'));
			$Form->Erstelle_HiddenFeld('MFI_FIL_ID',$FIL_ID);
			$Form->Erstelle_TextFeld('*MFI_FIL_ID',$FIL_ID,4,50,false,'','','','T','L','','',10);
			$rsMBW = $DB->RecordSetOeffnen('SELECT * FROM Mitbewerber WHERE MBW_KEY = 0'.$AWIS_KEY1);
			$INET_LINK_Route = $AWISBenutzer->ParameterLesen('INET_SUCHE_MitbewerbRoute');
			require_once('awisFilialen.inc');
			$PEIKey = new awisFilialen($FIL_ID);
			$GeoLaenge = $PEIKey->FilialInfo(130,4);
			$GeoBreite = $PEIKey->FilialInfo(131,4);
			$INET_LINK_Route = str_replace('$FIL_GEOCODE',$DB->FeldInhaltFormat('N8',$GeoBreite).','.$DB->FeldInhaltFormat('N8',$GeoLaenge),$INET_LINK_Route);
			$INET_LINK_Route = str_replace('$STRASSE',$rsMBW->FeldInhalt('MBW_STRASSE'),$INET_LINK_Route);
			$INET_LINK_Route = str_replace('$HAUSNUMMER',$rsMBW->FeldInhalt('MBW_HAUSNUMMER'),$INET_LINK_Route);
			$INET_LINK_Route = str_replace('$LAN_CODE',$rsMBW->FeldInhalt('MBW_LAN_CODE'),$INET_LINK_Route);
			$INET_LINK_Route = str_replace('$PLZ',$rsMBW->FeldInhalt('MBW_PLZ'),$INET_LINK_Route);
			$INET_LINK_Route = str_replace('$ORT',$rsMBW->FeldInhalt('MBW_ORT'),$INET_LINK_Route);
			$Form->Erstelle_Bild('href','MAPS',$INET_LINK_Route,'/bilder/icon_globus.png',$AWISSprachKonserven['Wort']['INET_AdressenSuche']);
		}
		else
		{
			$Form->Erstelle_TextFeld('MFI_FIL_ID',$rsMFI->FeldInhalt('MFI_FIL_ID'),4,180,$EditModus,'','','','T','L','','',10);
		}
		$AWISCursorPosition=($EditModus?'txtFIL_ID':$AWISCursorPosition);
		$Form->Erstelle_TextFeld('FIL_BEZ',$rsMFI->FeldInhalt('FIL_BEZ'),5,500,false,'','','');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MFI']['MFI_ENTFERNUNG'].':',150,'');
		$Form->Erstelle_TextFeld('MFI_ENTFERNUNG',$rsMFI->FeldInhalt('MFI_ENTFERNUNG'),7,60,$EditModus,'','','','N2','L','','',10);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MFI']['MFI_SICHTBAR'].':',150,'');
		$Liste = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);
		$Form->Erstelle_SelectFeld('MFI_SICHTBAR',$rsMFI->FeldInhalt('MFI_SICHTBAR'),100,$EditModus,'','','','','',$Liste);
		$Form->ZeileEnde();

		$Form->Trennzeile();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['MFR']['Ueberschrift'],750,'Ueberschrift');
		$Form->ZeileEnde();

		$SQL = 'SELECT MFR_KEY,MFR_MFI_KEY,MFR_MRG_KEY,MRG_KEY,MFR_RANKING,MFR_BEMERKUNG,MFR_USER,MFR_USERDAT';
		$SQL .= ', MRG_KEY, MRG_BEZEICHNUNG';
		$SQL .= ' FROM MITBEWERBERRANKINGGRUPPEN';
		$SQL .= ' LEFT OUTER JOIN MITBEWERBERFILIALENRANKING ON MFR_MRG_KEY = MRG_KEY AND MFR_MFI_KEY = '.$DB->FeldInhaltFormat('N0',$rsMFI->FeldInhalt('MFI_KEY'),false);
		$SQL .= ' WHERE MRG_STATUS = \'A\'';
		$SQL .= ' ORDER BY  MRG_SORTIERUNG';
		$rsMFR = $DB->RecordSetOeffnen($SQL);

		while(!$rsMFR->EOF())
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($rsMFR->FeldInhalt('MRG_BEZEICHNUNG').':',150,'');

			$Liste = explode("|",$AWISSprachKonserven['MFR']['lst_MFR_RANKING']);

			$Name = 'MFR_RANKING_'.$rsMFR->FeldInhalt('MFR_KEY').'_'.$rsMFR->FeldInhalt('MRG_KEY');
			$Form->Erstelle_SelectFeld($Name,$rsMFR->FeldInhalt('MFR_RANKING'),200,$EditModus,'',($rsMFR->FeldInhalt('MFR_KEY')!=''?'':'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']),'','','',$Liste);

			$Form->Erstelle_TextLabel($AWISSprachKonserven['MFR']['MFR_BEMERKUNG'].':',120,'');
			$Name = 'MFR_BEMERKUNG_'.$rsMFR->FeldInhalt('MFR_KEY').'_'.$rsMFR->FeldInhalt('MRG_KEY');
			$Form->Erstelle_TextFeld($Name,$rsMFR->FeldInhalt('MFR_BEMERKUNG'),30,200,$EditModus,'','','','T','L','','',100);
			$Form->ZeileEnde();

			$rsMFR->DSWeiter();
		}

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>