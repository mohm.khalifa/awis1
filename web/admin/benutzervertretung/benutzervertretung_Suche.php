<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=WIN1252">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="author" content="ATU">
<?php
global $AWISBenutzer;
global $EingabeFeld;
global $AWISCursorPosition;

try 
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form = new awisFormular();
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('XBV','XBV_%');
	$TextKonserven[]=array('Wort','Auswahl_ALLE');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort', 'txt_BitteWaehlen');
	$TextKonserven[]=array('Wort', 'AuswahlSpeichern');
	$TextKonserven[]=array('Wort', 'ttt_AuswahlSpeichern');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht19500 = $AWISBenutzer->HatDasRecht(19500);
	
	if($Recht19500 == 0)
	{
	    $Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineRechte']);
		die();
	}
	
	$Form->SchreibeHTMLCode("<form name=frmSuche method=post action=./benutzervertretung_Main.php?cmdAktion=Details>");
	
	/**********************************************
	 * * Eingabemaske
	***********************************************/
	$Param = unserialize($AWISBenutzer->ParameterLesen('AdressenSuche'));
	
	$Form->Formular_Start();

	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['XBV']['XBV_XBN_KEY'].':',190);
	$Form->Erstelle_TextFeld('*SuchName',($Param['SPEICHERN']=='on'?$Param['SuchName']:''),20,200,true);
	$AWISCursorPosition='sucSuchName';
	$Form->ZeileEnde();
    
	$Form->ZeileStart();
	$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',190);
	$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),30,true,'on','',$AWISSprachKonserven['Wort']['ttt_AuswahlSpeichern']);
	$Form->ZeileEnde();
	
	$Form->Formular_Ende();
	
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'F');
	
	if (($Recht19500 & 4) != 0)
	{	// wenn Recht zum Anlegen
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN', $ex->getMessage());
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage());
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>