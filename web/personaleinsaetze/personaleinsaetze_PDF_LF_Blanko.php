<?php

//Blanko -> Formular VisitCheck
require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[]=array('IBG','%');
$TextKonserven[]=array('CV','%');
$TextKonserven[]=array('LF','%');

$AWISBenutzer = awisBenutzer::Init();

$Form = new AWISFormular(); $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Ausdruck = new awisAusdruck('P','A4','');
$Ausdruck->NeueSeite(0,1);

if ($AWISBenutzer->BenutzerSprache()=='CZ')
{
	$Ausdruck->_pdf->ZeichenKodierung('CP1250');
	$Schriftart = 'arialcz';
}
else 
{
	$Schriftart = 'arial';
}

$Ausdruck->_pdf->AddFont($Schriftart,'','');

function Auswertung_Felder($Ausdruck,$ID,$x1,$y1,$x1_2,$y1_2,$FeldBreite = 5)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	global $Schriftart;
	
	$Schritt = 3;
	
	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;
	
	$rsAuswertung = $DB->RecordSetOeffnen($SQL);
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '1')
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1,$y1,$x2 = ($x1  + $Schritt),$y2 = ($y1 + $Schritt));
	  $Ausdruck->_pdf->Line($x2, $y1, $x1, $y2);
          $Ausdruck->_pdf->SetLineWidth(0); 
	}
	else
	{
	  $Ausdruck->_pdf->cell2(10,$FeldBreite,$Ausdruck->_pdf->Rect($x1,$y1,$Schritt,$Schritt),'LTRB','C',1);
	}
	
	if($rsAuswertung->FeldInhalt('PEZ_WERT') == '0')
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	  $Ausdruck->_pdf->SetLineWidth(1); 
  	  $Ausdruck->_pdf->Line($x1_2,$y1_2,$x2 = ($x1_2  + $Schritt),$y2 = ($y1_2 + $Schritt));
      $Ausdruck->_pdf->Line($x2, $y1_2, $x1_2, $y2);
      $Ausdruck->_pdf->SetLineWidth(0); 	
	}
	else
	{
	  $Ausdruck->_pdf->cell2(20,$FeldBreite,$Ausdruck->_pdf->Rect($x1_2,$y1_2,$Schritt,$Schritt),'LTRB','C',1);
	}
	$SchriftartTemp = $Schriftart;
	if($rsAuswertung->FeldInhalt('PBF_BEMERKUNG') == '')
	{
	  $Schriftart = 'courier';
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6);
	  //$Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,35)."\n".substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),35,33).($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG') != '' ?'...':'');
	  $Text = '';
	  $DeadEnd = '';
	  if($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG') == '')
	  {
	  	$Ausdruck->_pdf->SetFillColor(255,255,255);
	  	$Spalte = $Ausdruck->_pdf->getX();
	  	$Zeile = $Ausdruck->_pdf->getY();
	  	$Ausdruck->_pdf->cell2(40,$FeldBreite,'','LTRB','L',1,1);
	  	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	  	$Ausdruck->_pdf->cell2(40,$FeldBreite,$Text,'LTRB',0,'L',0);
	  }
	  else 
	  {
	  	$Spalte = $Ausdruck->_pdf->getX();
	  	$Ausdruck->_pdf->SetFillColor(255,255,255);
	  	$Zeile = $Ausdruck->_pdf->getY();
	  	$Ausdruck->_pdf->cell2(40,$FeldBreite,'','LTRB','L',1,1);
	  	switch($FeldBreite)
	  	{
	  		case '5':			// PDF im extra Fenster -> Download
	  			$Hoehe = 2.5;
	  			$Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,60);
	  			break;
	  		case '8':			// Standard-Stream
	  			$Hoehe = 1.9;
	  			$Text = substr($rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),0,110);
	  			break;
	  		
	  	}
	  		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	  		$Spalte = $Ausdruck->_pdf->getX();
	  		$Zeile = $Ausdruck->_pdf->getY();
	  		$Ausdruck->_pdf->MultiCell(40,$Hoehe, $Text,0,'N');	
	  		$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	  		$Ausdruck->_pdf->cell2(40,$FeldBreite,'','LTRB','L',1);
	  }
	  
	}
	else 
	{
	  
	  $Ausdruck->_pdf->SetFont($Schriftart,'',6); 	
	  $Ausdruck->_pdf->cell2(40,$FeldBreite,$rsAuswertung->FeldInhalt('PBF_BEMERKUNG').':'.$rsAuswertung->FeldInhalt('PEZ_BEMERKUNG'),'LTRB','L',1);  	
	}
	
	$Schriftart = $SchriftartTemp;
	
}

function FuelleFelder($Ausdruck,$ID)
{
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	global $Schriftart;

	$Schritt = 3;

	$SQL =  'Select * from Personaleinsberichtszuord where PEZ_PEI_KEY='.$_GET['PEI_KEY'];
	$SQL .= ' AND PEZ_PBF_ID='.$ID;

	$rsAuswertung = $DB->RecordSetOeffnen($SQL);
	
	$Ausdruck->_pdf->cell2(40,4,$rsAuswertung->FeldInhalt('PEZ_WERT'),'B',0,'L',0);
}


$Spalte = 15;
$Zeile = 5;

if($_GET['PEI_KEY'] == 0)
{
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Filialbesuch GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Filialbericht GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 29.01.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);
	
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['IBG']['IBG_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,'','B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['IBG']['IBG_Datum'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,'','B',0,'C',0);
$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['LF']['LF_GBL'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_GL'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_WL'].':','',0,'L',0);
$Ausdruck->_pdf->cell2(40,4,'','B',0,'L',0);


$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 72, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 72, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_A2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 77, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 77, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 85, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 85, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 90, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 90, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 95, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 95, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 100, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 100, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B1'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 110, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 110, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 118, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 118, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 123, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 123, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B4'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 128, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 128, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B5'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 136, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 136, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B6'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(10,8,$Ausdruck->_pdf->Rect(128.5, 144, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,8,$Ausdruck->_pdf->Rect(143, 144, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,8,'','LTRB','L',0);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B7'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 152, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 152, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B8'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 157, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 157, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B9'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 162, 3, 3),'LTRB','C',0);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 162, 3, 3),'LTRB','L',0);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B10'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 167, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 167, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B11'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 172, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 172, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B12'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 177, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 177, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B13'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 182, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 182, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 192, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 192, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 197, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 197, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 202, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 202, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 212, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 212, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 217, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 217, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 222, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 222, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 227, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 227, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D5'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 232, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 232, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D6'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 237, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 237, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D7'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 242, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 242, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D8'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 247, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 247, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D9'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 252, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 252, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D10'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 257, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 257, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D11'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 262, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 262, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D12'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 267, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 267, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D13'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 272, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 272, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);


$Zeile+=15;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Liste Filialbesuch GBL '.date('m-Y'),'T',0,'L',0);


$Ausdruck->NeueSeite(0,1);

$Spalte = 15;
$Zeile = 5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Filialbesuch GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Filialbericht GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 2 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 29.01.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);

$Zeile+=15;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);
$Zeile+=5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D14'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 47, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 47, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D15'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 52, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 52, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D16'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 57, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 57, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D17'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 62, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 62, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D18'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 67, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 67, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D19'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 72, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 72, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 82, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 82, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 87, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 87, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 92, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 92, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 97, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 97, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 107, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 107, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 112, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 112, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 117, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 117, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 122, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 122, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_G1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 132, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 132, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_G2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 137, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 137, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H1'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 147, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 147, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H2'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 152, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 152, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H3'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 157, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 157, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H4'],'LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(10,5,$Ausdruck->_pdf->Rect(128.5, 162, 3, 3),'LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,$Ausdruck->_pdf->Rect(143, 162, 3, 3),'LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=25;
$Spalte+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Unterschrift GL/WL','T',0,'C',0);

$Spalte+=120;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL','T',0,'C',0);

$Zeile+=100;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Liste Filialbesuch GBL '.date('m-Y'),'T',0,'L',0);





}
elseif($_GET['PEI_KEY'] != 0)
{

$SQL = 	'Select PEZ_KEY,PEZ_PEI_KEY,PEZ_PBF_ID,PEZ_WERT,PEZ_BEMERKUNG,PEZ_USER,PEZ_USERDAT,PEI_KEY,PEI_DATUM,PEI_XBN_KEY,PEI_VONXTZ_ID,PEI_BISXTZ_ID,PEI_PEA_KEY,PEI_FIL_ID,PEI_BEMERKUNG,PEI_PEB_KEY,PEI_USER,PEI_USERDAT,PEI_PBE_KEY,PEI_PEZ_STATUS,aa.XTZ_TAGESZEIT AS ZEITVON,bb.XTZ_TAGESZEIT AS ZEITBIS '; 
$SQL .= 'From Personaleinsberichtszuord Inner Join Personaleinsaetze On Pei_Key = Pez_Pei_Key ';
$SQL .= 'inner join TAGESZEITEN aa ON PEI_VONXTZ_ID = aa.XTZ_ID   inner join TAGESZEITEN bb ON PEI_BISXTZ_ID = bb.XTZ_ID ';
$SQL .= 'WHERE PEZ_PEI_KEY='.$DB->FeldInhaltFormat('NO',$_GET['PEI_KEY']);
	
$rsBericht = $DB->RecordSetOeffnen($SQL);	

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Filialbesuch GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Filialbericht GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 1 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 29.01.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);
$Zeile+=10;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(12,4,$AWISSprachKonserven['IBG']['IBG_Filiale'].':','',0,'L',0);
//$Spalte=+5;
$Ausdruck->_pdf->cell2(40,4,$rsBericht->FeldInhalt('PEI_FIL_ID'),'B',0,'L',0);
$Ausdruck->_pdf->cell2(30,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitVon'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITVON'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['IBG']['IBG_BesuchszeitBis'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(25,4,$rsBericht->FeldInhalt('ZEITBIS'),'B',0,'C',0);
$Ausdruck->_pdf->cell2(15,4,$AWISSprachKonserven['IBG']['IBG_Datum'].':','',0,'C',0);
$Ausdruck->_pdf->cell2(22,4,substr($rsBericht->FeldInhalt('PEI_DATUM'),0,10),'B',0,'C',0);
$Zeile+=10;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(10,4,$AWISSprachKonserven['LF']['LF_GBL'].':','',0,'L',0);
FuelleFelder($Ausdruck,407);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_GL'].':','',0,'L',0);
FuelleFelder($Ausdruck,408);
$Ausdruck->_pdf->cell2(8,4,$AWISSprachKonserven['LF']['LF_WL'].':','',0,'L',0);
FuelleFelder($Ausdruck,409);

$Zeile+=15;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,411, 128.5, 72,143,72);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_A2'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,471, 128.5, 77,143,77,8);
$Spalte-=110;


$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,472, 128.5, 85,143,85);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,412, 128.5, 90,143,90);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,413, 128.5, 95,143,95);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_A6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,414, 128.5, 100,143,100);

$Zeile+=5;
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B1'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,416, 128.5, 110,143,110,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,417, 128.5, 118,143,118);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,418, 128.5, 123,143,123);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B4'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,419, 128.5, 128,143,128,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B5'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,420, 128.5, 136,143,136,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->MultiCell(110,4,$AWISSprachKonserven['LF']['LF_B6'],'LTRB','L',0);
$Spalte+=110;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
Auswertung_Felder($Ausdruck,421, 128.5, 144,143,144,8);
$Spalte-=110;

$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B7'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,422, 128.5, 152,143,152);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B8'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,423, 128.5, 157,143,157);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B9'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,424, 128.5, 162,143,162);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B10'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,425, 128.5, 167,143,167);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B11'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,426, 128.5, 172,143,172);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B12'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,427, 128.5, 177,143,177);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_B13'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,428, 128.5, 182,143,182);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',1,1);
$Spalte-=140;

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,430, 128.5, 192,143,192);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,431, 128.5, 197,143,197);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_C3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,432, 128.5, 202,143,202);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;


$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,434, 128.5, 212,143,212);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,435, 128.5, 217,143,217);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,436, 128.5, 222,143,222);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,437, 128.5, 227,143,227);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D5'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,438, 128.5, 232,143,232);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D6'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,439, 128.5, 237,143,237);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D7'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,440, 128.5, 242,143,242);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D8'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,441, 128.5, 247,143,247);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D9'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,442, 128.5, 252,143,252);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D10'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,443, 128.5, 257,143,257);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D11'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,444, 128.5, 262,143,262);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D12'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,445, 128.5, 267,143,267);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D13'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,446, 128.5, 272,143,272);

$Zeile+=5;
$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','T','L',1,1);
$Spalte-=140;
$Ausdruck->_pdf->SetFillColor(190,190,190);


$Zeile+=10;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Liste Filialbesuch GBL '.date('m-Y'),'T',0,'L',0);


$Ausdruck->NeueSeite(0,1);

$Spalte = 15;
$Zeile = 5;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),16);
$Ausdruck->_pdf->Image('../bilder/ATU_Deutschland_farbe.PNG', 16.5,12.5, 591/22.5,276/22.5);
$Ausdruck->_pdf->cell2(30,26,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(120,8,'Filialbesuch GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
//$Ausdruck->_pdf->cell2(30,8,'g�ltig in','LTR',0,'C',0);
$Ausdruck->_pdf->cell2(30,8,'g�ltig in ','LRT',0,'C',0);
$Zeile+=8;
$Spalte+=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',18);
$Ausdruck->_pdf->cell2(120,8,'Filialbericht GBL','LTRB',0,'C',0);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(30,8,'DE/AT/CH','LRB',0,'C',0);
$Zeile+=8;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',8);
$Ausdruck->_pdf->cell2(120,5,'COO','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Seite 2 von 2','LBTR',0,'C',0);
$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(60,5,'Herausgabedatum: 29.01.2015','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(60,5,'','LTRB',0,'L',0);
$Ausdruck->_pdf->cell2(30,5,'Version 1.0','LBTR',0,'C',0);

$Zeile+=15;
$Spalte-=30;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,'','','L',0);
$Ausdruck->_pdf->cell2(10,5,$AWISSprachKonserven['CV']['CV_OK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(20,5,$AWISSprachKonserven['CV']['CV_NICHTOK'],'LTRB',0,'C',0);
$Ausdruck->_pdf->cell2(40,5,$AWISSprachKonserven['CV']['CV_BEMERKUNG'],'LTRB',0,'C',0);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D14'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,447, 128.5, 47,143,47);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D15'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,448, 128.5, 52,143,52);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D16'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,449, 128.5,57,143,57);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D17'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,450, 128.5,62,143,62);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D18'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,451, 128.5,67,143,67);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_D19'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,452, 128.5,72,143,72);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,454, 128.5,82,143,82);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,455, 128.5,87,143,87);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,456, 128.5,92,143,92);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_E4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,457, 128.5,97,143,97);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,459, 128.5,107,143,107);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,460, 128.5,112,143,112);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,461, 128.5,117,143,117);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_F4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,462, 128.5,122,143,122);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_G'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);

$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_G1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,464, 128.5,132,143,132);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_G2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,465, 128.5,137,143,137);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFillColor(190,190,190);
$Ausdruck->_pdf->SetFont($Schriftart,($AWISBenutzer->BenutzerSprache()!='CZ'?'B':''),10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H'],'LTRB',0,'L',1);
$Ausdruck->_pdf->cell2(10,5,'','LTRB','C',1);
$Ausdruck->_pdf->cell2(20,5,'','LTRB','L',1);
$Ausdruck->_pdf->cell2(40,5,'','LTRB','L',1);


$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','LTR','L',0,1);
$Spalte-=140;

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H1'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,467, 128.5,147,143,147);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H2'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,468, 128.5,152,143,152);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H3'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,469, 128.5,157,143,157);

$Zeile+=5;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(110,5,$AWISSprachKonserven['LF']['LF_H4'],'LTRB',0,'L',0);
Auswertung_Felder($Ausdruck,470, 128.5,162,143,162);

$Zeile+=5;
$Ausdruck->_pdf->SetFillColor(255,255,255);
$Spalte+=140;
$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell2(40,5,'','T','L',0,1);
$Spalte-=140;
$Ausdruck->_pdf->SetFillColor(190,190,190);


$Zeile+=20;
$Spalte+=10;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(30,5,'Unterschrift GL/WL','T',0,'C',0);

$Spalte+=120;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont($Schriftart,'',10);
$Ausdruck->_pdf->cell2(40,5,'Unterschrift GBL','T',0,'C',0);

//$Zeile+=110;
//$Spalte=15;


//$Ausdruck->_pdf->SetFont('Arial','',6);

//$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
//$Ausdruck->_pdf->Cell2(180,5,'Liste Filialbesuch GBL '.date('m-Y'),'T',0,'L',0);


$Zeile+=5;
$Spalte-=136;

$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= " AND (PEZ_WERT = '0' OR PEZ_WERT = '1')";

$rsGesamt = $DB->RecordSetOeffnen($SQL);

$Gesamtanzahl = $rsGesamt->FeldInhalt('ANZ');


$SQL = "Select count(*) AS Anz from PERSONALEINSBERICHTSZUORD WHERE PEZ_PEI_KEY=".$_GET['PEI_KEY'];
$SQL .= "AND PEZ_WERT = '0'";

$GesamtFalsch = $DB->RecordSetOeffnen($SQL);



$Falsch = $GesamtFalsch->FeldInhalt('ANZ');



$Prozentsatz = 100 - (($Falsch / $Gesamtanzahl) *100); 


//echo 'Gesamt:'.$Gesamtanzahl;
//echo 'Falsch:'.$Falsch;
//echo 'Prozent:'.$Prozentsatz;
//die;

if($Prozentsatz >= 89 && $Prozentsatz <= 100)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_quer.jpg',87.5),'',0,'C',0);
	$Ausdruck->_pdf->Image('../bilder/ampel_gruen_neu_hoch.jpg',$Spalte+85,$Zeile-20,'','');
	$Zeile+=6;
	$Spalte=$Spalte+10.5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(160,5,'(GR�N)','',0,'C',0);
	$Spalte=$Spalte-10.5;
	
}
elseif($Prozentsatz >= 79 && $Prozentsatz <= 89)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_quer.jpg',87.5),'',0,'C',0);
	$Ausdruck->_pdf->Image('../bilder/ampel_gelb_neu_hoch.jpg',$Spalte+85,$Zeile-20,'','');
	$Zeile+=6;
	$Spalte=$Spalte+10.5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(160,5,'(GELB)','',0,'C',0);
	$Spalte=$Spalte-10.5;
}
elseif($Prozentsatz < 79)
{
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	//$Ausdruck->_pdf->cell2(180,4,'Hallo','LTRB',0,'L',0);
	$Ausdruck->_pdf->SetFont($Schriftart,'',10);
	$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_hoch.jpg',$Spalte+85,$Zeile-20,'','');
	
	//$Ausdruck->_pdf->cell2(180,20,$Ausdruck->_pdf->Image('../bilder/ampel_rot_neu_quer.jpg',87.5),'',0,'C',0);
	$Zeile+=6;
	$Spalte=$Spalte+10.5;
	$Ausdruck->_pdf->setXY($Spalte, $Zeile);
	$Ausdruck->_pdf->cell2(159.75,5,'(ROT)','',0,'C',0);
	$Spalte=$Spalte-10.5;
}
$Zeile+=4;
//$Ausdruck->_pdf->setXY($Spalte, $Zeile);
//$Ausdruck->_pdf->cell2(190,5,'Erf�llungsgrad: min. 75 % der Felder, Erf�llungsrating: 100% - 90% = Gr�n, 89% - 80% = Gelb, < 80% = Rot   ','',0,'C',0);
	


}

$Zeile+=85;
$Spalte=15;


$Ausdruck->_pdf->SetFont('Arial','',6);

$Ausdruck->_pdf->SetXY($Spalte,$Zeile);
$Ausdruck->_pdf->Cell2(180,5,'Liste Filialbesuch GBL '.date('m-Y'),'T',0,'L',0);

$Ausdruck->Anzeigen();


?>