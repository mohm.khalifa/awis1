<?php
//************************************************************************************************************
// Datenschnittstelle: TWS
//************************************************************************************************************
//
// Aufraggeber: Sven Conrad
// Auftrag:     Juni 2007
//************************************************************************************************************
// Parameter:	FIL_ID		Filiale
//              GBL_PERS_NR Gebietsleiter
//              VKL_PERS_NR Verkaufsleiter
//************************************************************************************************************
// Datenformat: ASCII, Trennzeichen angebbar
//************************************************************************************************************
// Fehlercodes: ###RECHTE###       Kein ausreichendes Recht f�r diese Aktion
//************************************************************************************************************
require_once('db.inc.php');
require_once("sicherheit.inc.php");
global $awisRSZeilen;
global $awisRSInfo;
$con = awisLogon();

$Recht4000 = awisBenutzerRecht($con,4000);
if($Recht4000==0)
{
    awisEreignis(3,1000,'TWS-Export',$_SERVER['PHP_AUTH_USER'],'','','');
    echo '###RECHTE###';
	die();
}

// Optionale Zusatzparameter
if(isset($_GET['TRENNER']))
{
	$Trenner=$_GET['TRENNER'];
}
else
{
	$Trenner=';';
}

if(isset($_GET['ZEILENTRENNER']))
{
	$ZeilenTrenner=$_GET['ZEILENTRENNER'];
}
else
{
	$ZeilenTrenner="\r\n";
}


$SQL = 'select FIL_ID, VKLName, VKLPersNr, GBLName, GBLPersNr, GEBIET, RZID';
$SQL .= ' FROM';
$SQL .= ' (';
$SQL .= ' select FIL_ID,MIT_BEZEICHNUNG AS VKLName, MIT_PER_NR AS VKLPersNr, fif_wert AS RZID';
$SQL .= ' from filialen';
$SQL .= ' inner join filialinfos on fil_id = fif_fil_id AND fif_fit_id = 26';
$SQL .= ' inner join mitarbeiter on MIT_REZ_ID = fif_wert AND MIT_STATUS=\'A\'';
$SQL .= ' INNER JOIN MitarbeiterTaetigkeiten ON MIT_MTA_ID = MTA_ID AND MIT_MTA_ID IN (1,110)';
$SQL .= ' ) VKL';
$SQL .= ' INNER JOIN';
$SQL .= ' (';
$SQL .= ' SELECT VKG_ID AS GEBIET, MIT_Bezeichnung AS GBLName,PER_NR AS GBLPersNr, fif_fil_id';
$SQL .= ' FROM Verkaufsgebiete';
$SQL .= ' INNER JOIN Mitarbeiter ON VKG_MIT_KEY = MIT_KEY';
$SQL .= ' LEFT OUTER JOIN Personal ON MIT_PER_NR = PER_NR';
$SQL .= ' inner join filialinfos on vkg_id = fif_wert and fif_fit_id=21';
$SQL .= ' ) GBL ON FIL_ID = FIF_FIL_ID';

if(isset($_GET['FIL_ID']))
{
	$SQL .= ' WHERE FIL_ID='.intval($_GET['FIL_ID']);
}
$SQL .= ' order by fil_id';


$rsDaten = awisOpenRecordset($con,$SQL);
$rsDatenZeilen = $awisRSZeilen;

$Zeile='';
foreach($awisRSInfo AS $Feld)
{
	$Zeile .= $Trenner.$Feld['Name'];
}

//�berschrift ausgeben
echo substr($Zeile,strlen($Trenner)).$ZeilenTrenner;

for($DatenZeile=0;$DatenZeile<$rsDatenZeilen;$DatenZeile++)
{
	$Zeile='';
	foreach($awisRSInfo AS $Feld)
	{
		$Zeile .= $Trenner.$rsDaten[$Feld['Name']][$DatenZeile];
	}
	echo substr($Zeile,strlen($Trenner)).$ZeilenTrenner;
}	

?>