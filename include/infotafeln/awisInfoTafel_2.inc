<?php
try
{
    // Textkonserven laden
    $TextKonserven = array();
    $TextKonserven[]=array('ZUB','ttt_ZUBSTATUS');
    $TextKonserven[]=array('ZUB','ZUB_WANR');
    $TextKonserven[]=array('ZUB','LetzterDatensatz');
    $TextKonserven[]=array('ZIL','%');
    $TextKonserven[]=array('Wort','lbl_weiter');
    $TextKonserven[]=array('Wort','lbl_speichern');
    $TextKonserven[]=array('Wort','lbl_suche');
    $TextKonserven[]=array('Wort','lbl_zurueck');
    $TextKonserven[]=array('Wort','lbl_hilfe');
    $TextKonserven[]=array('Wort','Seite');
    $TextKonserven[]=array('Wort','txt_BitteWaehlen');
    $TextKonserven[]=array('Liste','lst_ZIL_TYP');
    $TextKonserven[]=array('Fehler','err_keineDaten');
    $TextKonserven[]=array('Fehler','err_keineDatenbank');
    $TextKonserven[]=array('XJO','XJO_%');
    $TextKonserven[]=array('Wort','DatumVom');
    $TextKonserven[]=array('Wort','DatumBis');

    $Form = new awisFormular();
    $AWISBenutzer = awisBenutzer::Init();
    $DB = awisDatenbank::NeueVerbindung('AWIS');
    $DB->Oeffnen();
    $AWISWerkzeug = new awisWerkzeuge();

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
    $Recht10007 = $AWISBenutzer->HatDasRecht(10007);
    if($Recht10007==0)
    {
        awisEreignis(3,1000,'Zukauf',$AWISBenutzer->BenutzerName(),'','','');
        echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
        echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
        die();
    }

    $SQL = 'SELECT *';
    $SQL .= ' FROM jobliste';
    if($AWISWerkzeug->awisLevel()!=awisWerkzeuge::AWIS_LEVEL_ENTWICK)
    {
        $SQL .= ' WHERE xjo_gruppe like \'ZUKAUF%\' ';
    }
    $SQL .= ' ORDER BY XJO_KEY';

    $rsXJO = $DB->RecordSetOeffnen($SQL);

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
    $Link .= '&Sort=XJO_JOBBEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_JOBBEZEICHNUNG'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_JOBBEZEICHNUNG'],300,'',$Link);

    $Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
    $Link .= '&Sort=XJO_NAECHSTERSTART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_NAECHSTERSTART'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_NAECHSTERSTART'],160,'',$Link);

    $Link = './zukaufbestellungen_Main.php?cmdAktion=Importinfo'.(isset($_GET['Seite'])?'&Seite='.intval($_GET['Seite']):'');
    $Link .= '&Sort=XJO_LETZTERSTART'.((isset($_GET['Sort']) AND ($_GET['Sort']=='XJO_LETZTERSTART'))?'~':'');
    $Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['XJO']['XJO_LETZTERSTART'],160,'',$Link);

    $Form->ZeileEnde();

    //for($DS=0;$DS<$rsXJOZeilen;$DS++)
    $DS=0;
    while(!$rsXJO->EOF())
    {
        $Status = '';
        $Form->ZeileStart();

        $Link = '';
        $Form->Erstelle_ListenFeld('XJO_JOBBEZEICHNUNG',$rsXJO->FeldInhalt('XJO_JOBBEZEICHNUNG'),0,300,false,($DS%2),'',$Link,'T');
        $Form->Erstelle_ListenFeld('XJO_NAECHSTERSTART',$rsXJO->FeldInhalt('XJO_NAECHSTERSTART'),0,160,false,($DS%2),'',$Link,'DU');
        $Form->Erstelle_ListenFeld('XJO_LETZTERSTART',$rsXJO->FeldInhalt('XJO_LETZTERSTART'),0,160,false,($DS%2),'',$Link,'DU');

        $Form->ZeileEnde();
        $DS++;
        $rsXJO->DSWeiter();
    }
    
    $Form->Formular_Ende();
}
catch(Exception $ex)
{
    var_dump($ex);
}
    //$Form->PruefeDatum($DatumsText);
?>