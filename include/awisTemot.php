<?php
require_once('db.inc.php');
require_once('register.inc.php');
require_once('awis_forms.inc.php');
require_once('awisINFO.php');

/**
 * Klasse fuer die Abwicklung des Datenaustausches mit TEMOT
 *
 * @author  Sacha Kerres
 * @version 20070731
 * @copyright ATU Auto Teile Unger
 * @package AWIS
 * @name awisTEMOT
 * @uses soapClient
 * 
 */
class awisTemot
{
	/**
	 * Backupserver f�r Notfalldaten
	 *
	 */
	const BACKUP_SERVER = 'AWIS3.ATU.DE';
	/**
	 * WSDL Datei fuer den Abfruf der Daten
	 *
	 */
	//const TEMOT_WSDL_PV = 'https://atu.pvservice.de/atu/service.asmx?wsdl';
	const TEMOT_WSDL_PV = 'https://62.153.117.211/atu/service.asmx?wsdl';
	//const TEMOT_WSDL_TROST = 'https://www.trost.de/lsservice/service.asmx?wsdl';
	//const TEMOT_WSDL_TROST_TEST = 'https://www.trost.de/lsservicetest/service.asmx?wsdl';
	
	const TEMOT_WSDL_TROST = 'https://webservices.trost.de/LSService/service.asmx?wsdl';
	const TEMOT_WSDL_TROST_TEST = 'https://www.webservices.trost.de/LSServiceTest/service.asmx?wsdl';
	
	/**
	 * Aktuelle MwSt Satz
	 *
	 */
	const MWST=1.19;
	
	/**
	 * Sortiment, wenn keine Warengruppe ermittelt werden kann
	 *
	 */
	const TEMOT_DEFAULT_SORTIMENT='ZUKNEU';
	
	/**
	 * Zukauflieferanten-ID von TEMOT
	 *
	 */
	const TEMOT_ZLI_KEY=1;
	/**
	 * Debuganzeige
	 * 
	 * @var int
	 */
	private $_Debug = 0;
	/**
	 * SOAP Client
	 *
	 * @var SOAPClient
	 */
	private $_SoapClient = null;
	
	/**
	 * Session-Kennung f�r die Authentifizierung bei TEMOT
	 *
	 * @var string
	 */
	private $_SessionID = '';

	/**
	 * Kundenkennung f�r die Authentifizierung bei TEMOT
	 *
	 * @var unknown_type
	 */
	private $_KundenIdent='';
	
	/**
	 * Datenbankverbindung
	 *
	 * @var oci_connection
	 */
	private $_con = null;
	
	/**
	 * Infoobjekt zum versenden von E-Mails und auslesen von Parametern
	 *
	 * @var awisINFO
	 */
	private $_AWISInfo;
	/**
	 * E-Mail Adressen f�r Fehler
	 *
	 * @var string
	 */
	private $_FehlerMail_Empfaenger='';
	
	/**
	 * Zu importierende Firma
	 *
	 * @var unknown_type
	 */
	private $_Firma = '';
	/**
	 * Erzeugung des Objekts
	 *
	 * @param int $Debug
	 * @param strint $Firma (PV / TROST)
	 */
	public function __construct($Debug=0, $Firma='PV')
	{
		ini_set('soap.wsdl_cache_enabled',0);
		
		$this->_Debug = intval($Debug);
		
		$Params = array('trace'=>1,						// Protokoll
			'proxy_host' => 'webproxy.ads.atu.de',		// Proxy-Server
			//'proxy_host' => '10.100.129.10',		// Proxy-Server
			'proxy_port' => 8080,						// Port fuer den Proxy
			//'proxy_login'=> '',							// Benutzername
			//'proxy_password'=>''						// Passwort
			);

		// SOAP Client starten
		try 
		{
			$this->_Firma = $Firma;				// Firma speicher f�r LOG
			
			if($Firma=='PV')
			{
				$this->_SoapClient = new SoapClient(self::TEMOT_WSDL_PV,$Params);
	
				$this->_SessionID='Abc123';		// Fest f�r die Testumgebung
				$this->_KundenIdent='ATU';		// Fest
				
				/*
				$Erg = $this->_SoapClient->Test('ATU');
				if(!isset($Erg->TestResult))
				{
					throw new Exception('Fehler beim Initialisieren des SOAP Clients: Fehler beim Test',1000);	
				}
				elseif($this->_Debug)
				{
					echo 'Test der Schnittstelle:'.$Erg->TestResult.PHP_EOL;
					//var_dump($this->_SoapClient->__getfunctions());
				}
*/
			}
			elseif($Firma=='TROST_TEST')
			{
				$this->_SoapClient = new SoapClient(self::TEMOT_WSDL_TROST_TEST,$Params);
				$this->_SessionID='Abc123';		// Fest f�r die Testumgebung
				$this->_KundenIdent='ATU';		// Fest
				var_dump($this->_SoapClient->__getfunctions());	
			}
			else 		// TROST
			{
				$this->_SoapClient = new SoapClient(self::TEMOT_WSDL_TROST,$Params);
	
				$this->_SessionID='Abc123';		// Fest f�r die Testumgebung
				$this->_KundenIdent='ATU';		// Fest
				//var_dump($this->_SoapClient->__getfunctions());	
			}
			
			$this->_con = awisLogon();
			$this->_AWISInfo = new awisINFO();
			
			$this->_FehlerMail_Empfaenger = explode(';',$this->_AWISInfo->LeseAWISParameter('awiszukauf.conf','MAIL_ADRESSEN_FEHLER'));
		}
		catch (Exception $ex)
		{
			throw new Exception('Fehler beim Initialisieren des SOAP Clients:'.$ex->getMessage(),1000);
		}
		
		
		// Meldung im Debug Modus augeben
		if($this->_Debug==1)
		{
			echo 'Init: OK'.PHP_EOL;
		}
	}
	
	function __destruct()
	{
		if(!is_null($this->_con))
		{
			awisLogoff($this->_con);
		}
	}
	
	/**
	 * Bestellungen von TEMOT lesen
	 *
	 * werden keine Parameter angegeben, werden alle Daten ausgelesen
	 * 
	 * @param string $Filiale
	 * @param string $WANummer
	 * @param string $BestellDatumVom (Format: YYYYMMTT)
	 * @param string $BestellDatumBis (Format: YYYYMMTT)
	 * @param string $Datei (Import aus der Datei, nicht SOAP)
	 * @param string $Status (A=Alle, auch schon gelesene)
	 * @param boolean $NurLeere (Korrigiert nur die leeren, d.h. ohne ExterneID)
	 */
	public function LeseBestellungen($Filiale='',$WANummer='',$BestellDatumVom='',$BestellDatumBis='', $Datei='', $Status='',$NurLeere=false)
	{	
		global $awisRSZeilen;
		
		try 
		{
			if($Datei=='')
			{
				$Params = array("sessionID"=>'Abc123',
								"status"=>$Status,
								"werkstattauftragsnummer"=>$WANummer,
								"bestelldatum"=>$BestellDatumVom,
								"bestelldatumbis"=>$BestellDatumBis,
								"filialnummer"=>$Filiale,
								"kundenident"=>'ATU');				
				$Erg = $this->_SoapClient->OrderConfirmation($Params);
				//if(strlen(serialize($Erg))>600)
				//{
					//file_put_contents('/tmp/temot'.$this->_Firma.'_'.date('Y-m-d-H-i-s').'.obj',serialize($Erg));
				//}		
				if($this->_Debug)
				{
					var_dump($Erg);
				}
			}
			else 
			{		
				$Erg = unserialize(file_get_contents($Datei));
				//$Erg = simplexml_load_string($Daten);

				if($this->_Debug)
				{
					echo 'Bestelldaten:'.PHP_EOL;
				}
			}
			
			$Erg = $Erg->OrderConfirmationResult;
			if(substr($Erg,0,1)!='<')
			{
				if($this->_Debug)
				{
					echo 'Keine Daten erhalten.'.PHP_EOL;
				}
				return false;
			}
			$Erg = str_replace('&','_',$Erg);
			$Obj = simplexml_load_string($Erg);
			if($Obj===false)
			{
				if($this->_Debug)
				{
					echo 'FEHLER: Kann Rueckgabe nicht als XML parsen.'.PHP_EOL;
					//throw new Exception('Kann Ergebnis nicht als XML parsen.',200711142152);
					$this->_AWISInfo->EMail($this->_FehlerMail_Empfaenger,'ZUKAUF-Fehler','Kann Rueckgabe nicht als XML parsen',2);
										
					return false;	
				}
			}

			if($this->_Debug)
			{
				echo 'Datum:'.$Obj->Confirmationheader->Datum.PHP_EOL;
				echo 'Bestelldatum vom:'.$Obj->Confirmationheader->Datum.PHP_EOL;
				echo 'Bestelldatum bis:'.$Obj->Confirmationheader->Datum.PHP_EOL;
			}
			
			foreach($Obj->Confirmationsdetails AS $Bestellung)
			{
				if($this->_Debug)
				{
					echo 'ID:'.$Bestellung->Identifikation.', vom '.(awisFeldFormat('DU',$Bestellung->Auftragserfassungsdatum . ' ' . substr($Bestellung->Auftragserfassungzeit,-6,6))).PHP_EOL;
				}
				

				if($this->_Debug)
				{
					echo '   -Suche Hersteller zu '.$Bestellung->HLK.PHP_EOL;
				}

					//****************************************
					// Hersteller ermitteln
					//****************************************
				$SQL = 'SELECT ZLH_KEY FROM ZukaufLieferantenHersteller';
				$SQL .= ' WHERE ZLH_ZLI_Key = 1 AND ZLH_Kuerzel='.awisFeldFormat('T',trim($Bestellung->HLK));
				$rsZLH = awisOpenRecordset($this->_con,$SQL);
				if($awisRSZeilen==0)
				{
					$SQL = 'INSERT INTO ZukaufLieferantenHersteller';
					$SQL .= '(ZLH_ZLI_Key, ZLH_Kuerzel, ZLH_Bezeichnung, ZLH_User, ZLH_UserDat)';
					$SQL .= ' VALUES (';
					$SQL .= ' '.self::TEMOT_ZLI_KEY;
					$SQL .= ' ,'.awisFeldFormat('T',trim($Bestellung->HLK));
					$SQL .= ' ,\'::unbekannt::\'';
					$SQL .= ' ,\'Bestellimport\'';
					$SQL .= ' ,SYSDATE';
					$SQL .= ' )';
					if(awisExecute($this->_con, $SQL)===false)
					{
						throw new Exception('Fehler beim Speichern eines Herstellers:'.$SQL, 200709181331);
					}
					$SQL = 'SELECT seq_ZLH_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = awisOpenRecordset($this->_con,$SQL);
					$ZLHKEY=$rsKey['KEY'][0];
					
					self::_LogEintrag('ZLH','Hersteller '.trim($Bestellung->HLK).' wurde nicht gefunden. Neu angelegt.',$Bestellung->Identifikation);
				}
				else 
				{
					$ZLHKEY=$rsZLH['ZLH_KEY'][0];
				}
				
				
				if($this->_Debug)
				{
					echo '   -Suche Artikel fuer '.trim($Bestellung->Artikelbezeichnung).PHP_EOL;
				}
					//****************************************
					// Artikel
					//****************************************
				$SQL = ' SELECT ZLA_Key, ZLA_WG1, ZLA_WG2, ZLA_WG3, ZLA_EK, ZLA_RABATT1, ZLA_RABATT2, ZLA_TAUSCHTEIL, ZLA_BEZEICHNUNG FROM ZukaufLieferantenArtikel WHERE ZLA_ZLH_Key='.awisFeldFormat('T',$ZLHKEY);
				$SQL .= ' AND ZLA_Artikelnummer = '.awisFeldFormat('T',trim($Bestellung->Artikelnummer)).'';
				$SQL .= ' AND ZLA_ZLI_Key='.self::TEMOT_ZLI_KEY;		// Temot
				$rsZLA = awisOpenRecordset($this->_con,$SQL);
				if($awisRSZeilen==0)
				{
					$SQL = 'INSERT INTO ZukaufArtikel';
					$SQL .= '(';
					$SQL .= 'ZLA_Artikelnummer, ZLA_ZLI_Key, ZLA_ZLH_Key,ZLA_Bezeichnung';
					$SQL .= ',ZLA_MengenEinheit,ZLA_Waehrung,ZLA_VK,ZLA_EinheitenProVK';
					$SQL .= ', ZLA_WG1, ZLA_WG2, ZLA_WG3, ZLA_RABATT1, ZLA_RABATT2, ZLA_EK';
					$SQL .= ',ZLA_TAUSCHTEIL, ZLA_Status,ZLA_User,ZLA_UserDat';
					$SQL .= ')VALUES(';
					$SQL .= ' '.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
					$SQL .= ','.self::TEMOT_ZLI_KEY;
					$SQL .= ','.$ZLHKEY;
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Artikelbezeichnung));
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Mengenheit));
					$SQL .= ','.awisFeldFormat('T',($Bestellung->Waerungsschluessel=='978'?'EUR':$Bestellung->Waerungsschluessel));
					$SQL .= ','.awisFeldFormat('N2',intval($Bestellung->Bruttopreis)/1000);
					$SQL .= ','.awisFeldFormat('Z',$Bestellung->Preiseinheit);
					$SQL .= ', '.awis_FeldInhaltFormat('T',trim((isset($Bestellung->Hauptwarengruppe)?$Bestellung->Hauptwarengruppe:'')),true);
					$SQL .= ', '.awis_FeldInhaltFormat('T',trim((isset($Bestellung->Warengruppe)?$Bestellung->Warengruppe:'')),true);
					$SQL .= ', '.awis_FeldInhaltFormat('T',trim((isset($Bestellung->Unterwarengruppe)?$Bestellung->Unterwarengruppe:'')),true);
					$Betrag = intval(trim((isset($Bestellung->Rabatt1)?$Bestellung->Rabatt1:'0')))/100;
					$SQL .= ', '.awis_FeldInhaltFormat('N4',$Betrag,false);
					$Betrag = intval(trim((isset($Bestellung->Rabatt2)?$Bestellung->Rabatt2:'0')))/100;
					$SQL .= ', '.awis_FeldInhaltFormat('N4',$Betrag,false);
					$Betrag = intval(trim((isset($Bestellung->Preis)?$Bestellung->Preis:'0')))/1000;
					$SQL .= ', '.awis_FeldInhaltFormat('N4',$Betrag,false);
					$SQL .= ', '.awisFeldFormat('N0',($Bestellung->Tauschteilkennzeichen=='N'?0:1));
					$SQL .= ',\'U\'';		// Status unbekannt
					$SQL .= ' ,\'Bestellimport\'';
					$SQL .= ' ,SYSDATE';
					$SQL .= ')';
					if(awisExecute($this->_con, $SQL)===false)
					{
						throw new Exception('Fehler beim Speichern eines Artikels:'.$SQL, 200709181345);
					}
					$SQL = 'SELECT seq_ZLA_KEY.CurrVal AS KEY FROM DUAL';
					$rsKey = awisOpenRecordset($this->_con,$SQL);
					$ZLAKEY=$rsKey['KEY'][0];
					
					$ATUSORTIMENT=self::TEMOT_DEFAULT_SORTIMENT;
					self::_LogEintrag('ZLA','Artikel '.trim($Bestellung->Artikelnummer).'/'.trim($Bestellung->HLK).' wurde nicht gefunden. Neu angelegt.',$Bestellung->Identifikation);
				
					$SQL = ' SELECT ZLA_Key, ZLA_WG1, ZLA_WG2, ZLA_WG3, ZLA_EK, ZLA_RABATT1, ZLA_RABATT2, ZLA_TAUSCHTEIL, ZLA_BEZEICHNUNG FROM ZukaufLieferantenArtikel WHERE ZLA_ZLH_Key='.awisFeldFormat('T',$ZLHKEY);
					$SQL .= ' AND ZLA_Artikelnummer = '.awisFeldFormat('T',trim($Bestellung->Artikelnummer)).'';
					$SQL .= ' AND ZLA_ZLI_Key='.self::TEMOT_ZLI_KEY;		// Temot
					$rsZLA = awisOpenRecordset($this->_con,$SQL);
					if(!isset($rsZLA['ZLA_WG1'][0]))
					{
						self::_LogEintrag('ZLA','Artikel nach einem INSERT nicht mehr gefunden. Bitte Entwickler benachrichtigen.',$Bestellung->Identifikation);
					}
				}
				else 
				{
					if(substr(strtoupper(trim($Bestellung->Artikelbezeichnung)),0,strlen('ALTTEILWERT'))!='ALTTEILWERT')
					{
						
							//**********************************************************************
							// Preisaktualisierung bei neuem Preis �ber die Schnittstelle
							//**********************************************************************
							
						$UpdateSQL = '';
						
						if(($rsZLA['ZLA_WG1'][0]=='' AND((trim($Bestellung->Hauptwarengruppe)!=$rsZLA['ZLA_WG1'][0])
						   or (trim($Bestellung->Warengruppe)!=$rsZLA['ZLA_WG2'][0])
						   or (trim($Bestellung->Unterwarengruppe)!=$rsZLA['ZLA_WG3'][0])))
						   AND $Bestellung->Artikelbezeichnung == $rsZLA['ZLA_BEZEICHNUNG'][0])
						{
							$UpdateSQL .= ', ZLA_WG1 = '.awis_FeldInhaltFormat('T',trim($Bestellung->Hauptwarengruppe),true);
							$UpdateSQL .= ', ZLA_WG2 =  '.awis_FeldInhaltFormat('T',trim((isset($Bestellung->Warengruppe)?$Bestellung->Warengruppe:'')),true);
							$UpdateSQL .= ', ZLA_WG3 =  '.awis_FeldInhaltFormat('T',trim((isset($Bestellung->Unterwarengruppe)?$Bestellung->Unterwarengruppe:'')),true);
						}
						else
						{
							echo "WG von anderer Firma: " . $rsZLA['ZLA_BEZEICHNUNG'][0] . "\n";
						}
						if($rsZLA['ZLA_TAUSCHTEIL'][0]!=($Bestellung->Tauschteilkennzeichen=='N'?0:1))
						{
							$UpdateSQL .= ', ZLA_TAUSCHTEIL =  '.awisFeldFormat('N0',($Bestellung->Tauschteilkennzeichen=='N'?0:1));
						}


						$PreisAlt = awis_FeldInhaltFormat('N4',$rsZLA['ZLA_EK'][0])*($rsZLA['ZLA_RABATT1'][0]==0?1:(1-($rsZLA['ZLA_RABATT1'][0]/100)))*($rsZLA['ZLA_RABATT2'][0]==0?1:(1-($rsZLA['ZLA_RABATT2'][0]/100)));
						$Rabatt1Neu = (intval(trim((isset($Bestellung->Rabatt1)?$Bestellung->Rabatt1:'0')))/100);
						$Rabatt2Neu = (intval(trim((isset($Bestellung->Rabatt2)?$Bestellung->Rabatt2:'0')))/100);
						$PreisNeu = awis_FeldInhaltFormat('N4',(intval(trim((isset($Bestellung->Preis)?$Bestellung->Preis:'0')))/1000)*($Rabatt1Neu==0?1:1-($Rabatt1Neu/100))*($Rabatt2Neu==0?1:(1-($Rabatt2Neu/100))));
	
						if($PreisAlt > $PreisNeu OR $PreisAlt==0)
	//					if(0)
						{
							$SQL = 'Update Zukauflieferantenartikel';
							$SQL .= ' SET ZLA_User = \'Bestellimport\', ZLA_USERDAT=SYSDATE';
							$SQL .= ', ZLA_EK = '.awisFeldFormat('N4',(intval(trim((isset($Bestellung->Preis)?$Bestellung->Preis:'0')))/1000),true);
							$SQL .= ', ZLA_RABATT1 = '.awisFeldFormat('N2',$Rabatt1Neu,true);
							$SQL .= ', ZLA_RABATT2 = '.awisFeldFormat('N2',$Rabatt2Neu,true);
							$SQL .= ' WHERE ZLA_KEY = 0'.$rsZLA['ZLA_KEY'][0];
							$SQL .= "\n";
							
							// TODO: Tabelle
							$fp = fopen('/daten/jobs/log/temot_preiserhoehung.log','a+');
							fputs($fp,"--Update wurde ausgefuehert:\n");
							fputs($fp,$SQL);
							fputs($fp,"\n/*\n".serialize($Bestellung)."\n*/\n");
							fclose($fp);
							$SQL = '';
						}
						elseif($PreisAlt < $PreisNeu)
						{
							// Update in eigene Tabelle speichern!!
							$UpdateSoll = '-- Hersteller: '.$ZLHKEY."\n";
							$UpdateSoll .= '-- Artikel:    '.awisFeldFormat('T',trim($Bestellung->Artikelbezeichnung))."\n";
							$UpdateSoll .= '-- Preis neu:  '.$PreisNeu."\n";
							$UpdateSoll .= '-- Preis alt:  '.$PreisAlt."\n";
							$UpdateSoll .= '-- EK alt:  '.awisFeldFormat('N4',$rsZLA['ZLA_EK'][0])."\n";
							$UpdateSoll .= '-- R1 alt:  '.awisFeldFormat('N4',($rsZLA['ZLA_RABATT1'][0]==0?1:(1-($rsZLA['ZLA_RABATT1'][0]/100))))."\n";
							$UpdateSoll .= '-- R2 alt:  '.awisFeldFormat('N4',($rsZLA['ZLA_RABATT2'][0]==0?1:(1-($rsZLA['ZLA_RABATT2'][0]/100))))."\n";
							$UpdateSoll .= 'Update Zukauflieferantenartikel';
							$UpdateSoll .= ' SET ZLA_User = \'Bestellimport\', ZLA_USERDAT=SYSDATE';
							$UpdateSoll .= ', ZLA_EK = '.awisFeldFormat('N4',(intval(trim((isset($Bestellung->Preis)?$Bestellung->Preis:'0')))/1000),true);
							$UpdateSoll .= ', ZLA_RABATT1 = '.awisFeldFormat('N2',$Rabatt1Neu,true);
							$UpdateSoll .= ', ZLA_RABATT2 = '.awisFeldFormat('N2',$Rabatt2Neu,true);
							$UpdateSoll .= ' WHERE ZLA_KEY = 0'.$rsZLA['ZLA_KEY'][0];
							$UpdateSoll .= "\n";
							
							// Daten schreiben zur manuellen Auswertung
							// TODO: Tabelle
							$fp = fopen('/daten/jobs/log/temot_preiserhoehung.sql','a+');
							fputs($fp,$UpdateSoll);
							fclose($fp);
						}
					
						if($UpdateSQL)
						{
							$SQL = 'Update Zukauflieferantenartikel';
							$SQL .= ' SET ZLA_User = \'Bestellimport\', ZLA_USERDAT=SYSDATE';
							$SQL .= $UpdateSQL;
							$SQL .= ' WHERE ZLA_KEY = 0'.$rsZLA['ZLA_KEY'][0];
	
							$fp = fopen('/daten/jobs/log/temot_warengruppen.log','a+');
							fputs($fp,"--Update wurde ausgefuehert:\n");
							fputs($fp,$SQL."\n");
							fclose($fp);
							//$SQL = '';
							
							if(awisExecute($this->_con, $SQL)==false)
							{
								echo 'Fehler bei ' . $SQL;
							}
							else
							{
								$SQL = ' SELECT ZLA_Key, ZLA_WG1, ZLA_WG2, ZLA_WG3, ZLA_EK, ZLA_RABATT1, ZLA_RABATT2 FROM ZukaufLieferantenArtikel WHERE ZLA_ZLH_Key='.awisFeldFormat('T',$ZLHKEY);
								$SQL .= ' AND ZLA_Artikelnummer = '.awisFeldFormat('T',trim($Bestellung->Artikelnummer)).'';
								$SQL .= ' AND ZLA_ZLI_Key='.self::TEMOT_ZLI_KEY;		// Temot
								$rsZLA = awisOpenRecordset($this->_con,$SQL);
							}			
						}					
					}
				}
				
					//***************************************************
					// Sortiment suchen
					//***************************************************
				$ZLAKEY = $rsZLA['ZLA_KEY'][0];
				$SQL = 'SELECT ZSZ_SORTIMENT FROM ZUKAUFSORTIMENTZUORDNUNGEN ';
				$SQL .= ' WHERE ZSZ_WG1='.awisFeldFormat('T',$rsZLA['ZLA_WG1'][0]);
				$SQL .= ' AND ZSZ_WG2='.awisFeldFormat('T',$rsZLA['ZLA_WG2'][0]);
				$SQL .= ' AND ZSZ_WG3='.awisFeldFormat('T',$rsZLA['ZLA_WG3'][0]);
				$SQL .= ' AND ZSZ_ZLI_KEY='.self::TEMOT_ZLI_KEY;
				$rsZSZ = awisOpenRecordset($this->_con,$SQL);
				if($awisRSZeilen==0)
				{
					$ATUSORTIMENT=self::TEMOT_DEFAULT_SORTIMENT;
					self::_LogEintrag('ZSZ','Sortiment fuer Artikel-KEY '.$rsZLA['ZLA_KEY'][0].', Warengruppen '.$rsZLA['ZLA_WG1'][0].'/'.$rsZLA['ZLA_WG2'][0].'/'.$rsZLA['ZLA_WG3'][0].' wurde nicht gefunden. Standard ('.self::TEMOT_DEFAULT_SORTIMENT .') verwendet.',$Bestellung->Identifikation);
				}
				else 
				{
					$ATUSORTIMENT=$rsZSZ['ZSZ_SORTIMENT'][0];
				}
			
				if($this->_Debug)
				{
					if($Bestellung->Faktorwertstellung!=1)
					{
						echo '  --> Rueckgabe'.PHP_EOL;				
					}
					
					if(intval($Bestellung->Altteilwert)>0)
					{
						echo '    --> Altteilwert: '.((intval($Bestellung->Altteilwert)/1000)).PHP_EOL;
					}
				}
						//**************************************************
						// Infos zur Bestellung suchen und Daten korrigieren
						//**************************************************				
				$WANr=trim($Bestellung->Kundenbestellnummer);
				
				$Infos = '';
				if(!is_numeric($WANr))
				{
					$WANr = '0';
					$Infos = $Bestellung->Kundenbestellnummer;
				}
				
/*				if(strlen($WANr)<6 OR substr(strtoupper(trim($Bestellung->Artikelbezeichnung)),0,strlen('ALTTEILWERT'))=='ALTTEILWERT')
				{						
					if($this->_Debug)
					{
						echo '  -->Suche Lieferung fuer Altteilrueckgabe:'.trim($Bestellung->Identifikation).PHP_EOL;
					}

					$SQL = 'SELECT * ';
					$SQL .= ' FROM ZukaufBestellungen';
					$SQL .= ' WHERE ZUB_Infos IS NULL';
					$SQL .= ' AND ZUB_ZLI_KEY=0'.self::TEMOT_ZLI_KEY ;
					$SQL .= ' AND ZUB_FIL_ID=0'.trim($Bestellung->FilialnummerATU);
					$SQL .= ' AND ZUB_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
					$SQL .= ' AND ZUB_ArtikelBezeichnung<>\'Altteilwert\'';
					$SQL .= ' AND ZUB_Hersteller='.awisFeldFormat('T',trim($Bestellung->HLK));
					$SQL .= ' ORDER BY ZUB_BestellDatum ASC';

					$rsINF = awisOpenRecordset($this->_con,$SQL);
					if($awisRSZeilen>0)
					{
						$WANr = $rsINF['ZUB_WANR'][0];
						$SQL = 'UPDATE ZukaufBestellungen';
						$SQL .= ' SET ZUB_Infos = '.awisFeldFormat('T','ALTTEIL-RUECKGABE:'.trim($Bestellung->Identifikation));
						$SQL .= ' WHERE ZUB_KEY = 0'.$rsINF['ZUB_KEY'][0];
						if(awisExecute($this->_con,$SQL)===false)
						{
							throw new Exception('Kann Altteilr�ckgabe nicht schreiben!',200709101353);
						}
						if($this->_Debug)
						{
							echo '    --> Habe Altteilrueckgabe fuer '.$rsINF['ZUB_KEY'][0].' gefunden.'.PHP_EOL;
						}
						$Infos .= 'ALTTEIL-FUER:'.trim($Bestellung->Identifikation);
					}
					elseif(substr(strtoupper(trim($Bestellung->Artikelbezeichnung)),0,strlen('ALTTEILWERT'))=='ALTTEILWERT') 
					{
						$Infos .= 'ALTTEIL-FUER:0';
					}
				}
*/

				//*****************************************************
				// R�ckgabe einess Teils oder eine Altteilwerterstattung
				//*****************************************************
				$ZUBKEY_Uebergeordnet = '';
				if(intval($Bestellung->Faktorwertstellung)<0)
				{						
					if($this->_Debug)
					{
						echo '  -->Suche Lieferung fuer Teilerueckgabe:'.trim($Bestellung->Identifikation).PHP_EOL;
					}

					$SQL = 'SELECT Z1.*';
					$SQL .= ' FROM ZukaufBestellungen Z1';
					$SQL .= ' WHERE ZUB_ZLI_KEY=0'.self::TEMOT_ZLI_KEY ;
					$SQL .= ' AND ZUB_FIL_ID=0'.trim($Bestellung->FilialnummerATU);
					$SQL .= ' AND ZUB_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
					$SQL .= ' AND ZUB_ArtikelBezeichnung<>\'Altteilwert\'';
					$SQL .= ' AND ZUB_Hersteller='.awisFeldFormat('T',trim($Bestellung->HLK));
					$SQL .= ' AND ZUB_Bestellmenge > 0';
					$SQL .= ' AND NVL((SELECT SUM(ZUB_Bestellmenge) AS Menge';
					$SQL .= ' FROM ZukaufBestellungen Z2';
					$SQL .= ' WHERE Z2.ZUB_ZUB_KEY = Z1.ZUB_KEY),0) < ZUB_Bestellmenge';
					$SQL .= ' AND NOT EXISTS(SELECT *';
					$SQL .= ' FROM ZukaufWerkstattauftraege';
					$SQL .= ' WHERE ZWA_ZUB_KEY= Z1.ZUB_KEY)';
					$SQL .= ' AND ZUB_Abschlussart = 1';
					$SQL .= ' ORDER BY ZUB_BestellDatum ASC';
					
					$rsINF = awisOpenRecordset($this->_con,$SQL);
					if($awisRSZeilen>0)
					{
						$ZUBKEY_Uebergeordnet = $rsINF['ZUB_KEY'][0];
						
						$WANr = $rsINF['ZUB_WANR'][0];
						$SQL = 'UPDATE ZukaufBestellungen';
						$SQL .= ' SET ZUB_Infos = '.awisFeldFormat('T',substr($rsINF['ZUB_INFOS'][0].' '.'RUECKGABE:'.trim($Bestellung->Identifikation),0,1000));
						$SQL .= ' WHERE ZUB_KEY = 0'.$rsINF['ZUB_KEY'][0];
						if(awisExecute($this->_con,$SQL)===false)
						{
							throw new Exception('Kann R�ckgabe nicht schreiben!',200709101353);
						}
						if($this->_Debug)
						{
							echo '    --> Habe Rueckgabe fuer '.$rsINF['ZUB_KEY'][0].' gefunden.'.PHP_EOL;
						}
						$Infos .= 'RUECKGABE-FUER:'.trim($Bestellung->Identifikation);
					}
				}
				elseif(substr(strtoupper(trim($Bestellung->Artikelbezeichnung)),0,strlen('ALTTEILWERT'))=='ALTTEILWERT')
				{
					if($this->_Debug)
					{
						echo '  -->Suche Lieferung fuer Altteilrueckgabe:'.trim($Bestellung->Identifikation).PHP_EOL;
					}

					$SQL = 'SELECT *';
					$SQL = ' FROM ZukaufBestellungen Z1';
					$SQL .= ' WHERE ZUB_ZLI_KEY=0'.self::TEMOT_ZLI_KEY ;
					$SQL .= ' AND ZUB_FIL_ID=0'.trim($Bestellung->FilialnummerATU);
					$SQL .= ' AND ZUB_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
					$SQL .= ' AND ZUB_ArtikelBezeichnung<>\'Altteilwert\'';
					$SQL .= ' AND ZUB_Hersteller='.awisFeldFormat('T',trim($Bestellung->HLK));
					$SQL .= ' AND ZUB_Bestellmenge > 0';
					$SQL .= ' AND (EXISTS(SELECT *';
					$SQL .= ' FROM ZukaufWerkstattauftraege';
					$SQL .= ' WHERE ZWA_ZUB_KEY= Z1.ZUB_KEY) OR ZUB_Abschlussart = 2)';
					$SQL .= ' AND ABS(NVL((SELECT SUM(ZUB_BESTELLMENGE) AS MENGE';
					$SQL .= ' FROM Zukaufbestellungen Z2';
					$SQL .= ' WHERE ZUB_ZUB_KEY = Z1.ZUB_KEY';
					$SQL .= ' AND UPPER(ZUB_Artikelbezeichnung) = \'ALTTEILWERT\')';
					$SQL .= ' ,0)) < ZUB_Bestellmenge';
					$SQL .= ' ORDER BY ZUB_BestellDatum ASC';
					
					$rsINF = awisOpenRecordset($this->_con,$SQL);
					if($awisRSZeilen>0)
					{
						$ZUBKEY_Uebergeordnet = $rsINF['ZUB_KEY'][0];
						
						$WANr = $rsINF['ZUB_WANR'][0];
						$SQL = 'UPDATE ZukaufBestellungen';
						$SQL .= ' SET ZUB_Infos = '.awisFeldFormat('T',$rsINF['ZUB_INFOS'][0].' '.'ALTTEIL:'.trim($Bestellung->Identifikation));
						$SQL .= ' WHERE ZUB_KEY = 0'.$rsINF['ZUB_KEY'][0];
						if(awisExecute($this->_con,$SQL)===false)
						{
							throw new Exception('Kann R�ckgabe nicht schreiben!',200709101353);
						}
						if($this->_Debug)
						{
							echo '    --> Habe Rueckgabe fuer '.$rsINF['ZUB_KEY'][0].' gefunden.'.PHP_EOL;
						}
						$Infos .= 'ALTTEIL-FUER:'.trim($Bestellung->Identifikation);
					}
					elseif(substr(strtoupper(trim($Bestellung->Artikelbezeichnung)),0,strlen('ALTTEILWERT'))=='ALTTEILWERT') 
					{
						$Infos .= 'ALTTEIL-FUER:0';
					}
				}

				if(strlen($WANr)==6)	// Alte Nummer
				{
					$WANrMit = " ((trim(to_char(".trim($Bestellung->FilialnummerATU).",'0999')) ";
					$WANrMit  .= "  || trim(to_char(".$WANr.",'099999')) ";
					$WANrMit  .= " || pruefziffer_2of5il(trim(to_char(".trim($Bestellung->FilialnummerATU).",'0999')) ";
					$WANrMit  .= " || trim(to_char(".$WANr.",'099999'))))) ";
					$WANr = $WANrMit;
				}
				elseif(strlen($WANr)>6) 						// Neue Nummer mit Pr�fziffer
				{
					if(strlen($WANr)>=9)
					{
						$WANr=substr('0000'.$WANr,-11);
					}
				}
				
				// Alte Bestellung suchen -> nach ID und Lieferant
				$NeueBestellung = false;
				$SQL = ' SELECT *';
				$SQL .= ' FROM Zukaufbestellungen';
				$SQL .= ' WHERE ZUB_ExterneID='.awisFeldFormat('T',$Bestellung->Identifikation);
				$SQL .= ' AND ZUB_ZLI_KEY=0'.self::TEMOT_ZLI_KEY;
				$rsZUB = awisOpenRecordset($this->_con,$SQL);
				
				if($awisRSZeilen==0)		// Nichts gefunden -> nach Angaben in der Bestellung suchen
				{
					$SQL = 'SELECT *';
					$SQL .= ' FROM ZukaufBestellungen';
					$SQL .= ' WHERE '; 
					$SQL .= ' ZUB_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
					$SQL .= ' AND ZUB_Hersteller='.awisFeldFormat('T',trim($Bestellung->HLK));
					$SQL .= ' AND ZUB_WANr='.$WANr;
					$SQL .= ' AND ZUB_ZLI_KEY=0'.self::TEMOT_ZLI_KEY;
					$SQL .= ' AND ZUB_FIL_ID='.awisFeldFormat('Z',trim($Bestellung->FilialnummerATU));
					$SQL .= ' AND (ZUB_EXTERNEID IS NULL OR ZUB_EXTERNEID='.awisFeldFormat('T',trim($Bestellung->Identifikation)).')';
	
					$rsZUB = awisOpenRecordset($this->_con,$SQL);
					if($awisRSZeilen==0)
					{
						$NeueBestellung=true;
					}
				}
				
				//****************************************
				// Preis bestimmen
				//****************************************
				
					// Faktor f�r den angeleiferten Preis
					//	damit wird immer auf 1 Einheit runtergerechnet
				$PreisFaktor=self::_PreisEinheit(trim($Bestellung->Preiseinheit));
				if($PreisFaktor=='##' OR $PreisFaktor==0)
				{
					throw new Exception('Unbekannte Preiseinheit:'.trim($Bestellung->Preiseinheit),200710161740);
				} 
				
				// Daten aus dem aktuellen System holen
				
				$SQL = 'SELECT AST_VK, AST_ATUNR';
				$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS VP';
				$SQL .= ', (SELECT ATW_BETRAG FROM ArtikelStammInfos ';
				$SQL .= '  	INNER JOIN ALTTEILWERTE ON ATW_LAN_CODE = \'DE\' AND ATW_KENNUNG = ASI_WERT';
				$SQL .= '     WHERE ASI_AIT_ID=190 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS ATW_BETRAG';
				$SQL .= ' FROM ZukaufLieferantenArtikel ';
				$SQL .= ' INNER JOIN Artikelstamm ON ZLA_AST_ATUNR = AST_ATUNR';
				$SQL .= ' INNER JOIN ZukaufLieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
				$SQL .= ' WHERE ZLA_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
				$SQL .= ' AND ZLH_Kuerzel='.awisFeldFormat('T',trim($Bestellung->HLK));
				$rsAST = awisOpenRecordset($this->_con,$SQL);
				
				
				// Notfall-Konzept
				/*
						$SQL = 'SELECT AST_VK, AST_ATUNR';
						$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos@'.self::BACKUP_SERVER.' WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS VP';
						$SQL .= ' FROM ZukaufLieferantenArtikel ';
						$SQL .= ' INNER JOIN Artikelstamm@'.self::BACKUP_SERVER.' ON ZLA_AST_ATUNR = AST_ATUNR';
						$SQL .= ' INNER JOIN ZukaufLieferantenHersteller ON ZLA_ZLH_KEY = ZLH_KEY';
						$SQL .= ' WHERE ZLA_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
						$SQL .= ' AND ZLH_Kuerzel='.awisFeldFormat('T',trim($Bestellung->HLK));
						$rsAST = awisOpenRecordset($this->_con,$SQL);
				*/
				if($awisRSZeilen>0)			// Wir kennen eine ATU Nummer -> diesen Preis verwenden
				{
					$VP = $rsAST['VP'][0];
					if($VP=='' OR $VP==0)
					{
						$VP = 1;
					}
					$VKPreis = awisFeldFormat('N2',$rsAST['AST_VK'][0])/$VP;
					
					if($rsAST['ATW_BETRAG'][0]!='')
					{
						$VKPreis -= awisFeldFormat('N2',$rsAST['ATW_BETRAG'][0]);
						if($this->_Debug)
						{
							echo '    -> Ziehe Altteilwert von Hoehe von '.$rsAST['ATW_BETRAG'][0].' EUR ab.'.PHP_EOL;
						}
					}
					if($VKPreis==0)
					{
						self::_LogEintrag('ZUB','Keine Artikeldaten  im Produktiv-Server. Nehme AWIS3!! KEY '.$rsZLA['ZLA_KEY'][0].'',$Bestellung->Identifikation);
						
						$SQL = 'SELECT AST_VK, AST_ATUNR';
						$SQL .= ', (SELECT ASI_WERT FROM ArtikelStammInfos@'.self::BACKUP_SERVER.' WHERE ASI_AIT_ID=191 AND ASI_AST_ATUNR=AST_ATUNR AND ROWNUM = 1) AS VP';
						$SQL .= ' FROM ZukaufLieferantenArtikel@'.self::BACKUP_SERVER.' ';
						$SQL .= ' INNER JOIN Artikelstamm@'.self::BACKUP_SERVER.' ON ZLA_AST_ATUNR = AST_ATUNR';
						$SQL .= ' INNER JOIN ZukaufLieferantenHersteller@'.self::BACKUP_SERVER.' ON ZLA_ZLH_KEY = ZLH_KEY';
						$SQL .= ' WHERE ZLA_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));
						$SQL .= ' AND ZLH_Kuerzel='.awisFeldFormat('T',trim($Bestellung->HLK));
						$rsAST = awisOpenRecordset($this->_con,$SQL);
						$VP = $rsAST['VP'][0];
						if($VP=='' OR $VP==0)
						{
							$VP = 1;
						}
						$VKPreis = awisFeldFormat('N2',$rsAST['AST_VK'][0])/$VP;
						if($VKPreis==0)
						{
							self::_LogEintrag('ZUB','Auch keine Artikeldaten im Entwicklungs-Server. Nehme Temot-Preis. KEY '.$rsZLA['ZLA_KEY'][0].'',$Bestellung->Identifikation);
							$VKPreis = ((intval($Bestellung->Bruttopreis)/1000/$PreisFaktor)*self::MWST);
						}
					}
				}
				else 
				{
					// Temot Preis kommt ohne MwST. Da wir keine anderen Infos haben-> 19% nehmen
					$VKPreis = ((intval($Bestellung->Bruttopreis)/1000/$PreisFaktor)*self::MWST);
				}
				
				if($NeueBestellung)		// Neuer Datensatz
				{
					if($this->_Debug)
					{
						echo ' (neu)'.PHP_EOL;
					}
					
					$SQL = 'INSERT INTO ZukaufBestellungen ';
					$SQL .= '(ZUB_ExterneID, ZUB_ZLI_KEY, ZUB_Unterlieferant, ZUB_BestellDatum, ZUB_FIL_ID, ZUB_WANR ';
					$SQL .= ', ZUB_ArtikelNummer, ZUB_ArtikelBezeichnung, ZUB_Hersteller, ZUB_VersandArtBez';
					$SQL .= ', ZUB_AuftragsNrExtern, ZUB_AuftragsPosExtern, ZUB_AuftragsUPosExtern';
					$SQL .= ', ZUB_BestellMenge, ZUB_MengenEinheiten, ZUB_TechnikEinheiten, ZUB_Tauschteil';
					$SQL .= ', ZUB_PreisBrutto, ZUB_PreisAngeliefert, ZUB_EinheitenProPreis, ZUB_Waehrung, ZUB_ZLA_Key, ZUB_Sortiment';
					if(isset($Bestellung->Altteilwert))
					{
						$SQL .= ', ZUB_ALTTEILWERT';
					}
					if($ZUBKEY_Uebergeordnet!='')
					{
						$SQL .= ', ZUB_ZUB_KEY';
					}
					$SQL .= ', ZUB_EK, ZUB_Infos, ZUB_USER, ZUB_UserDat )VALUES(';
					$SQL .= ' '.awisFeldFormat('T',$Bestellung->Identifikation);
					$SQL .= ','.self::TEMOT_ZLI_KEY;		// TEMOT-Kennung
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Firma).'-'.trim($Bestellung->Standort));
					$SQL .= ','.awisFeldFormat('DU',$Bestellung->Auftragserfassungsdatum . ' ' . substr($Bestellung->Auftragserfassungzeit,-6,6));
					$SQL .= ','.awisFeldFormat('Z',trim($Bestellung->FilialnummerATU));				// Filiale
					if(strlen($WANr)>20)	// Formel, keine Zahl!
					{
						$SQL .= ', '.$WANr;	
					}
					else
					{
						$SQL .= ','.awisFeldFormat('T',$WANr);			// Werkstattauftrag
					}
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Artikelnummer));					// Lieferantenartikelnummer
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Artikelbezeichnung));
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->HLK));
					$SQL .= ','.awisFeldFormat('T',self::_VersandArt(trim($Bestellung->Versandart)));
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Auftragsnummer));
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Auftragspositionsnummer));
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Unterpositionsnummer));
					// �nderung im Produktivsystem ab dem 7.11.2007
					// TODO: isset kann raus, wenn die Daten neu kommen!
					if(isset($Bestellung->Faktorwertstellung) AND $Bestellung->Faktorwertstellung==-1)
					{
						 $SQL .= ','.awisFeldFormat('Z',(intval($Bestellung->Bestellmenge)/1000)*-1);
					}
					else
					{
						$SQL .= ','.awisFeldFormat('Z',(intval($Bestellung->Bestellmenge)/1000));
					}
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->Mengenheit));
					$SQL .= ','.awisFeldFormat('T',trim($Bestellung->TechnischeMengenheit));
					$SQL .= ','.awisFeldFormat('T',($Bestellung->Tauschteilkennzeichen=='N'?0:1));
					$SQL .= ','.awisFeldFormat('N3',$VKPreis);
					$SQL .= ','.awisFeldFormat('N3',((intval($Bestellung->Bruttopreis)/1000/$PreisFaktor)*self::MWST));
					$SQL .= ','.awisFeldFormat('T',$PreisFaktor);
					$SQL .= ','.awisFeldFormat('T',($Bestellung->Waerungsschluessel=='978'?'EUR':$Bestellung->Waerungsschluessel));
					$SQL .= ','.awisFeldFormat('Z',$ZLAKEY);
					$SQL .= ','.awisFeldFormat('T',$ATUSORTIMENT);
					if(isset($Bestellung->Altteilwert))
					{
						$SQL .= ','.awisFeldFormat('N3',((intval($Bestellung->Altteilwert)/1000)));
					}
					if($ZUBKEY_Uebergeordnet!='')
					{
						$SQL .= ','.awisFeldFormat('Z',$ZUBKEY_Uebergeordnet);
					}
					
					$Rabatt1Neu = (intval(trim((isset($Bestellung->Rabatt1)?$Bestellung->Rabatt1:'0')))/100);
					$Rabatt2Neu = (intval(trim((isset($Bestellung->Rabatt2)?$Bestellung->Rabatt2:'0')))/100);
					$EK = awis_FeldInhaltFormat('N4',(intval(trim((isset($Bestellung->Preis)?$Bestellung->Preis:'0')))/1000)*($Rabatt1Neu==0?1:1-($Rabatt1Neu/100))*($Rabatt2Neu==0?1:(1-($Rabatt2Neu/100))));
					$SQL .= ','.awisFeldFormat('N2',$EK);
					$SQL .= ','.awisFeldFormat('T',$Infos);
					$SQL .= ','.awisFeldFormat('T',getenv('HOST'));
					$SQL .= ',SYSDATE';
					$SQL .= ')';

					if(awisExecute($this->_con, $SQL)==false)
					{
						echo 'Fehler : '.$SQL.PHP_EOL;
						$this->_AWISInfo->EMail($this->_FehlerMail_Empfaenger,'ZUKAUF-Fehler','Fehler beim Speichern einer neuen Bestellung:'.$SQL,2);
						throw new Exception('Fehler beim Speichern einer neuen Bestellung:'.$SQL,200709131937);
					}
				}
				else 						// schon vorhanden
				{
					if($rsZUB['ZUB_EXTERNEID'][0]=='')		// Wurde schon mal manuell eingetragen
					{
						if($this->_Debug)
						{
							echo ' (manuell vorhanden)'.PHP_EOL;
						}
						self::_LogEintrag('ZUB','Manuell eingef�gte Bestellung wurde ueberschrieben: '.$Bestellung->Identifikation);
						$this->_AWISInfo->EMail($this->_FehlerMail_Empfaenger,'ZUKAUF-Info','Manuell eingef�gte Bestellung wurde ueberschrieben: '.$Bestellung->Identifikation,2);
					}
					else									// Wurde schon einmal gelesen
					{
						if($this->_Debug)
						{
							echo ' (vorhanden)'.PHP_EOL;
						}
						
						if($NurLeere)
						{
							if($this->_Debug)
							{
								echo '  --> wird NICHT mehr geaendert.',PHP_EOL;
							}
							continue;		// Naechsten Eintrag
						}
					}
					
					$SQL = 'UPDATE ZukaufBestellungen SET';
					$SQL .= ' ZUB_ExterneID='.awisFeldFormat('T',$Bestellung->Identifikation);
					$SQL .= ',ZUB_ZLI_KEY=1';		// TEMOT-Kennung
					$SQL .= ',ZUB_Unterlieferant='.awisFeldFormat('T',$Bestellung->Firma.'-'.$Bestellung->Standort);
					$SQL .= ',ZUB_BestellDatum='.awisFeldFormat('DU',$Bestellung->Auftragserfassungsdatum . ' ' . substr($Bestellung->Auftragserfassungzeit,-6,6));
					$SQL .= ',ZUB_FIL_ID='.awisFeldFormat('Z',trim($Bestellung->FilialnummerATU));				// Filiale
					$SQL .= ',ZUB_WANR='.awisFeldFormat('T',$WANr);			// Werkstattauftrag
					$SQL .= ',ZUB_ArtikelNummer='.awisFeldFormat('T',trim($Bestellung->Artikelnummer));					// Lieferantenartikelnummer
					$SQL .= ',ZUB_ArtikelBezeichnung='.awisFeldFormat('T',trim($Bestellung->Artikelbezeichnung));
					$SQL .= ',ZUB_Hersteller='.awisFeldFormat('T',trim($Bestellung->HLK));
					$SQL .= ',ZUB_VersandArtBez='.awisFeldFormat('T',self::_VersandArt(trim($Bestellung->Versandart)));
					$SQL .= ',ZUB_AuftragsNrExtern='.awisFeldFormat('T',trim($Bestellung->Auftragsnummer));
					$SQL .= ',ZUB_AuftragsPosExtern='.awisFeldFormat('T',trim($Bestellung->Auftragspositionsnummer));
					$SQL .= ',ZUB_AuftragsUPosExtern='.awisFeldFormat('T',trim($Bestellung->Unterpositionsnummer));
					// �nderung im Produktivsystem ab dem 7.11.2007
					// TODO: isset kann raus, wenn die Daten neu kommen!
					if(isset($Bestellung->Faktorwertstellung) AND $Bestellung->Faktorwertstellung==-1)
					{
						 $SQL .= ',ZUB_BestellMenge='.awisFeldFormat('N3',(intval($Bestellung->Bestellmenge)/-1000));
					}
					else
					{
						$SQL .= ',ZUB_BestellMenge='.awisFeldFormat('N3',(intval($Bestellung->Bestellmenge)/1000));
					}
					$SQL .= ',ZUB_MengenEinheiten='.awisFeldFormat('T',trim($Bestellung->Mengenheit));
					$SQL .= ',ZUB_TechnikEinheiten='.awisFeldFormat('T',trim($Bestellung->TechnischeMengenheit));
					$SQL .= ',ZUB_Tauschteil='.awisFeldFormat('T',($Bestellung->Tauschteilkennzeichen=='N'?0:1));
					$SQL .= ',ZUB_PreisBrutto='.awisFeldFormat('N3',$VKPreis);
					$SQL .= ',ZUB_PreisAngeliefert='.awisFeldFormat('N3',((intval($Bestellung->Bruttopreis)/1000/$PreisFaktor)*self::MWST));
					$SQL .= ',ZUB_EinheitenProPreis='.awisFeldFormat('T',$PreisFaktor);
					$SQL .= ',ZUB_Waehrung='.awisFeldFormat('T',($Bestellung->Waerungsschluessel=='978'?'EUR':$Bestellung->Waerungsschluessel));
					$SQL .= ',ZUB_ZLA_Key='.awisFeldFormat('Z',$ZLAKEY);
					$SQL .= ',ZUB_Sortiment='.awisFeldFormat('T',$ATUSORTIMENT);
					$SQL .= ',ZUB_Infos='.awisFeldFormat('T',$Infos);
					if(isset($Bestellung->Altteilwert))
					{
						$SQL .= ',ZUB_ALTTEILWERT='.awisFeldFormat('N3',(intval($Bestellung->Altteilwert)/1000));
					}
					$SQL .= ', ZUB_USER = '.awisFeldFormat('T',getenv('HOST'));
					$SQL .= ', ZUB_UserDat=SYSDATE';
					$SQL .= ' WHERE ZUB_KEY=0'.$rsZUB['ZUB_KEY'][0];
						// nicht die manuell geaenderten !!
					$SQL .= ' AND (ZUB_ExterneID is null or (ZUB_USER = \'Temot\' OR ZUB_USER=\'Bestellimport\'))';
					if(awisExecute($this->_con, $SQL)===false)
					{
						echo 'Fehler : '.$SQL.PHP_EOL;
						throw new Exception('Fehler beim Aendern einer vorhanden ('.$rsZUB['ZUB_KEY'][0].') Bestellung:'.$SQL,200709131938);
					}
				}
				//break;
				
				$Erg = self::BestaetigeBestellung($Bestellung->Identifikation);
				if($Erg != 2)
				{
					$this->_AWISInfo->SyslogEintrag(1,'Fehler beim Bestellungen bestaetigen.');
				}
				elseif($this->_Debug)
				{
					echo 'Bestellung best�tigt.'.PHP_EOL;
					var_dump($Erg);
				}

			}	// Jede einzelne Position
		}
		catch (Exception $ex)
		{
			throw new Exception('Fehler beim Bestellung lesen:'.$ex->getMessage(),1000);
		}
		
		return true;
	}
	
	/**
	 * Bestaetigt den Download einer Bestellung
	 *
	 * @param string $ID
	 * @author Sacha Kerres
	 * @version 200708091925
	 */
	private function BestaetigeBestellung($ID)
	{
		$Params = array("sessionID"=>'Abc123',
						"identID"=>$ID);				
		$Erg = $this->_SoapClient->SetOrderConfirmation($Params);
		if($this->_Debug)
		{
			var_dump($Erg);
		}

		$Obj = simplexml_load_string($Erg->SetOrderConfirmationResult);
		$Erg = $Obj->SetConfirmation;
		return $Erg;	
	}
	
	/**
	 * Testbestellungen lesen
	 *
	 * @param string $Filiale
	 * @param string $WANummer
	 * @param string $BestellDatumVom
	 * @param string $BestellDatumBis
	 * @param string $Datei
	 * @param string $Status
	 */
	public function _TEST_DatenLesen($Filiale='',$WANummer='',$BestellDatumVom='',$BestellDatumBis='', $Datei='', $Status='')
	{
		$Params = array("sessionID"=>'Abc123',
						"status"=>$Status,
						"werkstattauftragsnummer"=>$WANummer,
						"bestelldatum"=>$BestellDatumVom,
						"bestelldatumbis"=>$BestellDatumBis,
						"filialnummer"=>$Filiale,
						"kundenident"=>'ATU');
		$Erg = $this->_SoapClient->OrderConfirmation($Params);
		file_put_contents('/tmp/TEST_temot'.$BestellDatumBis.'.xml',serialize($Erg));
		echo 'Datei /tmp/TEST_temot'.$BestellDatumBis.'.xml wurde geschrieben';

		$Erg = $Erg->OrderConfirmationResult;
		if(substr($Erg,0,1)!='<')
		{
			if($this->_Debug)
			{
				echo 'Keine Daten erhalten.'.PHP_EOL;
			}
			return false;
		}
		
		$Erg = str_replace('&','_',$Erg);
		$Obj = simplexml_load_string($Erg);
		
		foreach($Obj->Confirmationsdetails AS $Bestellung)
		{
			$Erg = self::BestaetigeBestellung($Bestellung->Identifikation);
			if($this->_Debug)
			{
				echo 'ID:'.$Bestellung->Identifikation.', vom '.awis_PruefeDatum($Bestellung->Auftragserfassungsdatum . ' ' . substr($Bestellung->Auftragserfassungzeit,-6,6),true).PHP_EOL;
				echo '  Erg:'.$Erg.PHP_EOL;
			}
		}
		
		return true;	
	}
	
	/**
	 * Protokolleintrag des Imports schreiben
	 *
	 * @param string $Typ
	 * @param string $Meldung
	 * @param string $ExterneID
	 * @author Sacha Kerres
	 * @version 200709181643
	 * 
	 */
	private function _LogEintrag($Typ, $Meldung, $ExterneID='')
	{
		$SQL = 'INSERT INTO ZukaufImportLog';
		$SQL .= '(ZIL_ZEITPUNKT,ZIL_TYP,ZIL_MELDUNG,ZIL_EXTERNEID,ZIL_ZLI_KEY';
		$SQL .= ',ZIL_USER,ZIL_USERDAT)';
		$SQL .= 'VALUES(';
		$SQL .= 'SYSDATE ';
		$SQL .= ', '.awisFeldFormat('T',$Typ);
		$SQL .= ', '.awisFeldFormat('T',$Meldung);
		$SQL .= ', '.awisFeldFormat('T',$ExterneID);
		$SQL .= ', '.self::TEMOT_ZLI_KEY;
		$SQL .= ', '.awisFeldFormat('T','Import');
		$SQL .= ',SYSDATE';
		$SQL .= ')';

		if(awisExecute($this->_con, $SQL)==false)
		{
			echo 'Fehler : '.$SQL.PHP_EOL;
			throw new Exception('Fehler beim Schreiben eines Protokolleintrags.',200709131938);
		}
	}
	
	
	/**
	 * Versandarten
	 *
	 * @param int $ID
	 * @return string
	 */
	private function _VersandArt($ID)
	{
		$Daten = file('/daten/daten/temot/temot_versandarten.csv');
		foreach($Daten AS $Zeile)
		{
			$Zeile = explode(';',$Zeile);
			
			if($Zeile[0]==$ID)
			{
				return ($Zeile[1]);
			}
		}
		return '::unbekannt::'.$ID;		
	}
	
	/**
	 * Peiseinheiten
	 *
	 * @param int $ID
	 * @return string
	 */
	private function _PreisEinheit($ID)
	{
		$Daten = file('/daten/daten/temot/temot_preiseinheiten.csv');
		foreach($Daten AS $Zeile)
		{
			$Zeile = explode(';',$Zeile);
			if(intval($Zeile[0])==intval($ID))
			{
				return (intval($Zeile[1]));
			}
		}
		return '##';		
	}
}



// Testroutine
global $argv;
global $argc;

if(isset($argv[1]))
{
	$TestAktiv = false;
	$DatumVom = ''; //date('Ymd');
	$DatumBis = ''; //date('Ymd');
	$WANr = '';
	$AlleLesen='';
	$Filiale='';
	$Simulation = false;
	$Debug=false;
	$Firma='';
	
	for($i=1;$i<$argc;$i++)
	{
		switch ($argv[$i]) {
			case '--help':
			case '-h':
				echo 'Syntax:'.PHP_EOL;
				echo ''.$argv[0].' [OPTIONEN]'.PHP_EOL.PHP_EOL;
				echo 'OPTIONEN:'.PHP_EOL;
				echo '-h, --help                        Diese Hilfe.'.PHP_EOL;
				echo '-t, --test                        Testroutine aktivieren.'.PHP_EOL;
				echo '-dv, --datumvom <JJJJMMDD>        Startdatum. Standard: Heute.'.PHP_EOL;
				echo '-db, --datumbis <JJJJMMDD>        Enddatum. Standard: Heute.'.PHP_EOL;
				echo '-wa, --auftrag <11-stellige Nr>   Werkstattauftragsnummer. Standard: alle'.PHP_EOL;
				echo '-f, --filiale <NR>                Filialnummer. Standard: alle'.PHP_EOL;
				echo '-a, -alle                         Holt alle Auftraege ab, egal ob sie schon gelesen wurden.'.PHP_EOL;
				echo '-s, --simulation                  Schreibt keine Daten weg, sondern zeigt sie am stdout an.'.PHP_EOL;
				echo '-l, --lieferant [PV|TROST]        Lieferant.'.PHP_EOL;
				echo '-d, --debug                       Debug Ausgaben.'.PHP_EOL;
				break;
			case '-t':
			case '--test':
				$TestAktiv = True;
				break;
			case '-dv':
			case '--datumvom':
				$DatumVom = $argv[++$i];
				break;
			case '-db':
			case '--datumbis':
				$DatumBis = $argv[++$i];
				break;
			case '-wa':
			case '--auftrag':
				$WANr = $argv[++$i];
				break;
			case '-a':
			case '--alle':
				$AlleLesen = 'A';
				break;
			case '-f':
			case '--filiale':
				$Filiale = $argv[++$i];
				break;
			case '-l':
			case '--lieferant':
				$Firma = $argv[++$i];
				if($Firma!='PV' and $Firma!='TROST' and $Firma!='TROST_TEST')
				{
					die('Falschen LLieferant angegeben!');
				}
				break;
			case '-d':
			case '--debug':
				$Debug=true;
				break;
			case '-s':
			case '--simulation':
				$Simulation=true;
				break;
			default:
				break;
		}
	}

	if($TestAktiv)
	{
		echo PHP_EOL.'======================================='.PHP_EOL;
		echo 'TEST Routine fuer awisTemot Klasse.'.PHP_EOL;
		echo '======================================='.PHP_EOL;
		
		$Temot = new awisTemot($Debug,$Firma);
		if($Simulation)
		{
			$Erg = $Temot->_TEST_DatenLesen($Filiale,$WANr,$DatumVom,$DatumBis,'',$AlleLesen);
		}
		else 
		{
			$Erg = $Temot->LeseBestellungen($Filiale,$WANr,$DatumVom,$DatumBis,'',$AlleLesen);
		}
	
		echo '======================================='.PHP_EOL;
		echo 'TEST Routine abgeschlossen.'.PHP_EOL;
		echo '======================================='.PHP_EOL;
	}
}	
?>
