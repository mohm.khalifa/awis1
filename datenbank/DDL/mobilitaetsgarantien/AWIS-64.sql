--------------------------------------------------------
--  Tabelle MOBILITAETSGARANTIEN anpassen
--------------------------------------------------------
ALTER TABLE MOBILITAETSGARANTIEN
  MODIFY (MOB_KFZ_KENNZEICHEN NOT NULL);

--------------------------------------------------------
--  Textkonserve hinzufügen
--------------------------------------------------------
INSERT
INTO
  TEXTKONSERVEN
  (
    XTX_KENNUNG,
    XTX_BEREICH,
    XTX_BEMERKUNG,
    XTX_TEXT_DE,
    XTX_TEXT_CZ,
    XTX_TEXT_NL,
    XTX_TEXT_IT,
    XTX_TEXT_FR,
    XTX_TEXT_CH,
    XTX_TEXT_AT,
    XTX_USERDAT
  )
VALUES
  (
    'MOB_ERR_LEERKENNZ',
    'MOB',
    'Leeres Kennzeichen entdeckt.',
    'Leeres Kennzeichen entdeckt.',
    'Leeres Kennzeichen entdeckt.',
    'Leeres Kennzeichen entdeckt.',
    'Leeres Kennzeichen entdeckt.',
    'Leeres Kennzeichen entdeckt.',
    'Leeres Kennzeichen entdeckt.',
    'Leeres Kennzeichen entdeckt.',
    sysdate
  );