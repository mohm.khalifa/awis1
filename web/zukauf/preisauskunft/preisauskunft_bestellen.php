<?php
global $AWISBenutzer;
try
{
	ini_set('soap.wsdl_cache_enabled',0);
	$Bestellen = true;

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ZPA'));

	
	$AWISWerkzeug = new awisWerkzeuge();
	if($AWISWerkzeug->awisLevel()==awisWerkzeuge::AWIS_LEVEL_ENTWICK OR $AWISWerkzeug->awisLevel()==awisWerkzeuge::AWIS_LEVEL_STAGING)
	{
		echo 'TEST-WA:'.$AWISWerkzeug->BerechnePruefziffer($Param['FIL_ID'].rand(100000, 999999),awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_ERMITTELN);
	}
	
	$SQL = 'SELECT  ZukaufPreisauskunft.*, LIE_NAME1';
	$SQL .= ' FROM ZukaufPreisauskunft';
	$SQL .= ' INNER JOIN Lieferanten ON ZPA_LIE_NR = LIE_NR';
	$SQL .= ' WHERE ZPA_WARENKORB = 1';
	$SQL .= ' AND ZPA_XBN_KEY = 0'.$AWISBenutzer->BenutzerID();
	$SQL .= ' ORDER BY ZPA_LIE_Nr';

	$rsZPA = $DB->RecordSetOeffnen($SQL);
	if($rsZPA->AnzahlDatensaetze()==0)
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['ZPA']['err_KeinWarenkorb'],1,'');
		$Form->ZeileEnde();
		$Bestellen = false;
	}
	else
	{
		$WAOK = true;
		if(!isset($_POST['sucZPA_WANUMMER']) OR $_POST['sucZPA_WANUMMER']=='' OR $AWISWerkzeug->BerechnePruefziffer($_POST['sucZPA_WANUMMER'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG ,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN )===false)
		{
			$WAOK = false;
		}

		if(!isset($_POST['cmdBestellenOK_x']) OR $WAOK===false)
		{
			$Form->Formular_Start();
			
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel(utf8_encode($AWISSprachKonserven['ZPA']['txt_WirklichBestellen']),500,'font-size:larger;color:red');
			$Form->ZeileEnde();

			$Form->Trennzeile('O');
			if(isset($_POST['sucZPA_WANUMMER']) AND $WAOK===false)
			{
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['ZPA']['txtPruefzifferWAFalsch'].': '.$_POST['sucZPA_WANUMMER'],1);
				$Form->ZeileEnde();

				$Form->Trennzeile('O');
			}

			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['ZPA']['ZPA_ZUKAUFBESTELLNUMMER'].':',200,'','',$AWISSprachKonserven['ZPA']['ttt_ZPA_ZUKAUFBESTELLNUMMER']);
			$Form->Erstelle_TextFeld('*ZPA_WANUMMER',(isset($_POST['ZPA_WANUMMER'])?$_POST['ZPA_WANUMMER']:''),15,190,true,'','','','T','L','','',11);
			$AWISCursorPosition='sucZPA_WANUMMER';
			$Form->ZeileEnde();

			$Form->Trennzeile('O');

			$Form->Schaltflaeche('image', 'cmdBestellenOK', '', '/bilder/cmd_daumenhoch.png', $AWISSprachKonserven['ZPA']['lbl_Bestellen'], 'J');
			$Form->Schaltflaeche('image', 'cmdNichtBestellen', '', '/bilder/cmd_daumenrunter.png', '', 'N');

			$Form->Trennzeile();
			$Form->Formular_Ende();
		}
		else
		{
			// Pr�fungen abgeschlossen
			// Kann bestellt werden?
			if($Bestellen)
			{
				$BestellListe = new awisSimpleXMLElement('<Bestellung/>');
				$BestellListe->addChild('Benutzer',$AWISBenutzer->BenutzerName());
				$BestellListe->addChild('Zeitpunkt',date('c'));
				$BestellListe->addChild('Werkstattauftrag',$_POST['sucZPA_WANUMMER']);
				$BestellListe->addChild('FIL_ID',$Param['FIL_ID']);
				$Lieferanten=$BestellListe->addChild('Lieferanten');

				$LetzterLieferant = 0;
				while(!$rsZPA->EOF())
				{
					if($LetzterLieferant != $rsZPA->FeldInhalt('ZPA_LIE_NR'))
					{
						$Lieferant = $Lieferanten->addChild('Lieferant');

						$Lieferant->addChild('LIE_NR',$rsZPA->FeldInhalt('ZPA_LIE_NR'));
						$Lieferant->addCData('Lieferant',$rsZPA->FeldInhalt('LIE_NAME1'));
						$Lieferant->addChild('Versandart',isset($Param['txtZUSTELLUNG_'.$rsZPA->FeldInhalt('ZPA_LIE_NR')])?$Param['txtZUSTELLUNG_'.$rsZPA->FeldInhalt('ZPA_LIE_NR')]:2);

						$Artikelliste = $Lieferant->addChild('Artikelliste');

						$LetzterLieferant = $rsZPA->FeldInhalt('ZPA_LIE_NR');
					}

					$Artikel = $Artikelliste->addChild('Artikel');
					$Artikel->addChild('Bestellnummer',$rsZPA->FeldInhalt('ZPA_BESTELLNUMMER'));
					$Artikel->addChild('BestellIdLieferant',$rsZPA->FeldInhalt('ZPA_BESTELLIDLIEFERANT'));
					$Artikel->addChild('Hlka',$rsZPA->FeldInhalt('ZPA_HERSTELLERCODE'));
					$Artikel->addCData('Artikelbezeichnung',$rsZPA->FeldInhalt('ZPA_ARTIKELBEZEICHNUNG'));
					$Artikel->addChild('Menge',$rsZPA->FeldInhalt('ZPA_MENGE'));
					$Artikel->addChild('Key',$rsZPA->FeldInhalt('ZPA_KEY'));
					// M�gliche Lieferarten f�r den jeweiligen Lieferanten (v.a. TROST)
					$Artikel->addChild('Lieferarten',$rsZPA->FeldInhalt('ZPA_LIEFERART'));

					$rsZPA->DSWeiter();
				}

				$Form->Formular_Start();

				ini_set('max_execution_time', 60);
				
				$Params = array('trace'=>1,'soap_version'=>SOAP_1_2
					, 'exceptions'=>1
					//, 'encoding'=>'ISO-8859-15'
					, 'encoding'=>'utf-8'
                    , 'timeout' => 120
					, 'location'=>'http://'.$ATU_SOAP_SERVER.'/soap/awisSOAP_Zukauf.php'
					);

				$SC = new SoapClient('http://'.$ATU_SOAP_SERVER.'/soap/awisSOAP_Zukauf.php?wsdl',$Params);

                try{
                    $Erg = ($SC->Bestellen($BestellListe->asXML(),$AWISBenutzer->BenutzerName()));

                }catch(SoapFault $e){
                    $Form->Fehler_Anzeigen('SoapServer',(isset($SC->__last_response)?$SC->__last_response:''),'SpaeterWiederholen',-4,11122107);
                    $Form->DebugAusgabe(1,$SC,$Erg);
                    $Mailtext = 'Fehler bei der Bestellung:'.(isset($SC->__last_response)?$SC->__getLastResponse():'')."\nAnfrage lautet:".serialize($Artikel) .'<br><br>';
                    $Mailtext .= 'Meldung: '.$e->getMessage().'<br>';
                    $Mailtext .= 'Datei: '.$e->getFile().'('.$e->getLine().')<br>';
                    $Mailtext .= 'DebugDetails: <br>';
                    $Mailtext .= var_export($SC,true);
                    $Mailtext .= var_export($Erg,true);
                    $Mailtext .= '<br>';
                    $Mailtext .= 'Stacktrace: ';
                    $Mailtext .= $e->getTraceAsString();

                    $AWISWerkzeug->EMail(array('shuttle@de.atu.eu'),'SOAP-Client Fehler AWIS (Client) - Zukauf - Filiale '.$Param['FIL_ID'],$Mailtext,3,'shuttle@de.atu.eu','awis@de.awis.eu');


                }
				$Bestellprotokoll = simplexml_load_string($Erg);
				if($Bestellprotokoll === False)
				{
					$Form->ZeileStart();
					$Form->Hinweistext($AWISSprachKonserven['ZPA']['err_FehlerBeimBestellen'],1);
					$Form->ZeileEnde();
					$Form->DebugAusgabe(1,htmlentities($Erg));
				}
				else
				{
					// Bestell�bersicht anzeigen
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['ZPA']['txt_ArtikelBestellt'],500,'font-size:larger');
					$Form->ZeileEnde();

					$Form->Trennzeile();

					foreach($Bestellprotokoll->Lieferanten->Lieferant AS $xmlLieferant)
					{
						$Form->ZeileStart();
						$Form->Erstelle_ListenBild('href','cmdProtokoll','./preisauskunft_Drucken.php?ID='.base64_encode($xmlLieferant->LIE_NR.'~'.$_POST['sucZPA_WANUMMER']),'/bilder/cmd_pdf.png','',0,'',16,16,20);
						$Form->Erstelle_Liste_Ueberschrift($xmlLieferant->Lieferant,($BildschirmBreite<1024?708:932),'');
						$Form->ZeileEnde();
						$Form->ZeileStart();
						switch ((string)$xmlLieferant->Bestellstatus)
						{
							case '##1##':			// Bestellt
								$Text = $AWISSprachKonserven['ZPA']['txt_AlleArtikelWurdenBestellt'];
								$TextFarbe = 'color:green;text-weight:bold;';
								break;
							case '##2##':
								$Text = $AWISSprachKonserven['ZPA']['txt_ArtikelWurdenTeilweiseBestellt'];
								$TextFarbe = 'color:red;text-weight:bold;background-color:yellow';
								break;
							case '##3##':
							case '##4##':
								$Text = $AWISSprachKonserven['ZPA']['txt_ArtikelWurdenNichtBestellt'];
								$TextFarbe = 'color:red;background-color:yellow;';
								break;
							default:
								$Text = utf8_decode($xmlLieferant->Bestellstatus);
								$TextFarbe = 'color:red;background-color:yellow;';
						}
						$Form->Erstelle_Liste_Ueberschrift($Text,($BildschirmBreite<1024?744:956),$TextFarbe);
						$Form->ZeileEnde();

						$Form->ZeileStart();
						$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['ZPA_BESTELLNUMMER'],180,'');
						$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['ZPA_ARTIKELBEZEICHNUNG'],390,'');
						$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['ZPA_MENGE'],110,'');
						if($BildschirmBreite<1024)
						{
							$Form->ZeileEnde();
							$Form->ZeileStart();
							$Form->Erstelle_Liste_Ueberschrift('',180,'');
						}
						$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['txtLiefermenge'],110,'');
						$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZPA']['txtRueckstand'],150,'');
						$Form->ZeileEnde();
						$DS=0;
						if(isset($xmlLieferant->Positionen->Artikel))
						{
							foreach($xmlLieferant->Positionen->Artikel AS $xmlArtikel)
							{
								$SQL = 'SELECT *';

											$SQL .= ' FROM ZUKAUFPREISAUSKUNFT WHERE ZPA_KEY = 0'.$xmlArtikel->Key.'';
								$rsZPA = $DB->RecordSetOeffnen($SQL);
								
								$DB->TransaktionBegin();
								
								$SQL = 'INSERT INTO ZUKAUFLIEFERANTENBESTELLUNGEN(';
			  					$SQL .= ' ZLB_FIL_ID, ZLB_WANR, ZLB_LIE_NR, ZLB_ZLA_KEY, ZLB_BESTELLNUMMER, ZLB_ARTIKELBEZEICHNUNG, ZLB_HERSTELLER, ZLB_HERSTELLERCODE,';
			  					$SQL .= ' ZLB_BESTELLMENGE, ZLB_LIEFERMENGE, ZLB_RUECKSTAND, ZLB_STATUS, ZLB_STATUSTEXT,';
			  					$SQL .= ' ZLB_BESTANDEIGEN, ZLB_BESTANDNACHBAR, ZLB_BESTANDFILIALE, ';
			  					$SQL .= ' ZLB_AUFTRAGSNR, ZLB_AUFTRAGSURL, ZLB_VERSANDART, ';
			  					$SQL .= ' ZLB_USER, ZLB_USERDAT';
			  					$SQL .= ') VALUES (';
								$SQL .= ' '.$Param['FIL_ID'];
								$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$_POST['sucZPA_WANUMMER']);
								$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$xmlLieferant->LIE_NR);
								$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsZPA->FeldInhalt('ZPA_ZLA_KEY'),true);
								$SQL .= ' , '.$DB->FeldInhaltFormat('T',(string)$xmlArtikel->Bestellnummer);
								$SQL .= ' , '.$DB->FeldInhaltFormat('T',utf8_decode((string)$xmlArtikel->Artikelbezeichnung));
								$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsZPA->FeldInhalt('ZPA_HERSTELLER'),true);
								$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$rsZPA->FeldInhalt('ZPA_HERSTELLERCODE'),true);
								$SQL .= ', '.$DB->FeldInhaltFormat('N0',$xmlArtikel->Menge,false);
								$SQL .= ', '.$DB->FeldInhaltFormat('N0',$xmlArtikel->Liefermenge,false);
								$SQL .= ', '.$DB->FeldInhaltFormat('N0',$xmlArtikel->Rueckstand,false);
								$SQL .= ', '.$DB->FeldInhaltFormat('N0',$xmlArtikel->Status,false);
								$SQL .= ', '.$DB->FeldInhaltFormat('T',$xmlArtikel->Statustext,false);
								$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$rsZPA->FeldInhalt('ZPA_BESTANDEIGEN'),false);
								$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$rsZPA->FeldInhalt('ZPA_BESTANDNACHBAR'),false);
								$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$rsZPA->FeldInhalt('ZPA_FIL_ID'),false);
								$SQL .= ', '.$DB->FeldInhaltFormat('T',(isset($xmlLieferant->Auftragsnummer)?$xmlLieferant->Auftragsnummer:''),false);
								$SQL .= ', '.$DB->FeldInhaltFormat('T',(isset($xmlLieferant->Auftragsurl)?$xmlLieferant->Auftragsurl:''),false);
								$SQL .= ', '.$DB->FeldInhaltFormat('T',$xmlLieferant->Versandart);
								$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName(),false);
								$SQL .= ', SYSDATE';
			  					$SQL .= ')';

			  					$DB->Ausfuehren($SQL);
			  					$SQL = 'DELETE FROM ZUKAUFPREISAUSKUNFT WHERE ZPA_KEY = 0'.$xmlArtikel->Key.'';
			  					$DB->Ausfuehren($SQL);

			  					$DB->TransaktionCommit();

			  					
								$Form->ZeileStart();
								$Form->Erstelle_ListenFeld('*f1',(string)$xmlArtikel->Bestellnummer,0,180,false,($DS%2),'','','T');
								$Form->Erstelle_ListenFeld('*f1',utf8_decode((string)$xmlArtikel->Artikelbezeichnung),0,390,false,($DS%2),'','','T');
								$Form->Erstelle_ListenFeld('*f1',(string)$xmlArtikel->Menge,0,110,false,($DS%2),'','','N0T');
								if($BildschirmBreite<1024)
								{
									$Form->ZeileEnde();
									$Form->ZeileStart();
									$Form->Erstelle_TextLabel('',180);
								}

								$Form->Erstelle_ListenFeld('*f1',(string)$xmlArtikel->Liefermenge,0,110,false,($DS%2),'','','N0T');
								$Form->Erstelle_ListenFeld('*f1',(string)$xmlArtikel->Rueckstand,0,130,false,($DS%2),'','','N0T');
								switch ((int)$xmlArtikel->Status)
								{
									case 0:
										$Text = $AWISSprachKonserven['ZPA']['txtPositionWurdeMitWarnungBestellt'].': '.utf8_decode((string)$xmlArtikel->Statustext);
										$Form->Erstelle_ListenBild('image','hinweis','','/bilder/icon_warnung.png',$Text,($DS%2),'',16);
										break;
									case 1:
										$Text = $AWISSprachKonserven['ZPA']['txtPositionWurdeBestellt'];
										$Form->Erstelle_ListenBild('image','ok','','/bilder/icon_ok.png',$Text,($DS%2),'',16);
										break;
									case 2:
										$Text = $AWISSprachKonserven['ZPA']['txtPositionWurdeNichtBestellt'].': '.utf8_decode((string)$xmlArtikel->Statustext);
										$Form->Erstelle_ListenBild('image','fehler','','/bilder/icon_fehler.png',$Text,($DS%2),'',16);
										break;
/* Noch nicht in Pruduktivsystem
									case 3:
										$Text = $AWISSprachKonserven['ZPA']['txtPositionMehrBestellt'].': '.utf8_decode((string)$xmlArtikel->Statustext);
										$Form->Erstelle_ListenBild('image','hinweis','','/bilder/icon_warnung.png',$Text,($DS%2),'',16);
										break;
*/
								}
								$Form->ZeileEnde();
								$DS++;
							}
						}
						else
						{
							$Form->ZeileStart();
							$Form->Hinweistext($xmlLieferant->Fehler->Text);
							$Form->ZeileEnde();
						}
					}
				}

				$Form->Trennzeile();
				$Form->Formular_Ende();

				//
				// Formular muss geleert werden
				//
				$_POST['sucZLA_ARTIKELNUMMER']='';
				$_POST['sucZPA_MENGE']='1';
				$_POST['sucZPA_WANUMMER']='';
				$Param = array();
				$AWISBenutzer->ParameterSchreiben('Formular_ZPA',serialize($Param));
			}
		}
	}
}
catch(Exception $ex)
{
	$Form->ZeileStart();
	$Form->Hinweistext('Es ist ein Problem aufgetreten:'.$ex->getCode().'/'.$ex->getMessage());
	$Form->ZeileEnde();
}
?>