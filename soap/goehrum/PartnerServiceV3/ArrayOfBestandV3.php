<?php

class ArrayOfBestandV3 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BestandV3[] $BestandV3
     */
    protected $BestandV3 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BestandV3[]
     */
    public function getBestandV3()
    {
      return $this->BestandV3;
    }

    /**
     * @param BestandV3[] $BestandV3
     * @return ArrayOfBestandV3
     */
    public function setBestandV3(array $BestandV3 = null)
    {
      $this->BestandV3 = $BestandV3;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BestandV3[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BestandV3
     */
    public function offsetGet($offset)
    {
      return $this->BestandV3[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BestandV3 $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BestandV3[] = $value;
      } else {
        $this->BestandV3[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BestandV3[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BestandV3 Return the current element
     */
    public function current()
    {
      return current($this->BestandV3);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BestandV3);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BestandV3);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BestandV3);
    }

    /**
     * Countable implementation
     *
     * @return BestandV3 Return count of elements
     */
    public function count()
    {
      return count($this->BestandV3);
    }

}
