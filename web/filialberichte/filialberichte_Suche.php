<?php
global $AWISCursorPosition;
global $AWISBenutzer;

$Form = new awisFormular();

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('FLB','%');
$TextKonserven[]=array('Wort','AuswahlSpeichern');
$TextKonserven[]=array('Wort','Auswahl_ALLE');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');
$TextKonserven[]=array('Wort','txt_OhneZuordnung');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_suche');
$TextKonserven[]=array('Wort','lbl_zurueck');
$TextKonserven[]=array('Wort','lbl_hilfe');
$TextKonserven[]=array('Wort','Status');
$TextKonserven[]=array('RFS','lst_RFS_STATUS');

$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

$Recht16000=$AWISBenutzer->HatDasRecht(16000);

echo "<form name=frmSuche method=post action=./filialberichte_Main.php?cmdAktion=Details>";

/**********************************************
* * Eingabemaske
***********************************************/
$Param = unserialize($AWISBenutzer->ParameterLesen('FLBSuche'));

$Form->Formular_Start();

//Pr�fen, ob der angemeldete Benutzer eine Filiale ist		
$UserFilialen=$AWISBenutzer->FilialZugriff(0,awisBenutzer::FILIALZUGRIFF_STRING);

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_FIL_ID'].':',100);
if($UserFilialen!='')
{	
	$Form->Erstelle_TextFeld('*FLB_FIL_ID',$UserFilialen,20,200,false,'','','','T');
	echo '<input type=hidden name="sucFLB_FIL_ID" value="'.$UserFilialen.'">';
}
else 
{
	$Form->Erstelle_TextFeld('*FLB_FIL_ID','',20,200,true);	
	$AWISCursorPosition='sucFLB_FIL_ID';
}

$Form->ZeileEnde();

$Form->ZeileStart();
$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_DATUMVON'].':',100);
$Form->Erstelle_TextFeld('*FLB_DATUM_VON','',20,200,true,'','','','D');
$Form->Erstelle_TextLabel($AWISSprachKonserven['FLB']['FLB_DATUMBIS'].':',100);
$Form->Erstelle_TextFeld('*FLB_DATUM_BIS','',20,200,true,'','','','D');
$Form->ZeileEnde();

$Form->Formular_Ende();

$Form->SchaltflaechenStart();

$Form->Schaltflaeche('href','cmd_zurueck','/filialtaetigkeiten/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');

if(($Recht16000&4) == 4)		
{
	$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
}

$Form->SchaltflaechenEnde();

if($AWISCursorPosition!='')
{
	echo '<Script Language=JavaScript>';
	echo "document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();";
	echo '</Script>';
}
?>
