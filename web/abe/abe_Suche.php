<?php
global $AWISBenutzer;
global $AWISCursorPosition;

require_once('awisFilialen.inc');

try {
    $TextKonserven = array();
    $TextKonserven[] = array('ABF', '*');
    $TextKonserven[] = array('FIL', 'FIL_GEBIET');
    $TextKonserven[] = array('Wort', 'wrd_Filiale');
    $TextKonserven[] = array('Wort', 'lbl_suche');
    $TextKonserven[] = array('Wort', 'lbl_zurueck');
    $TextKonserven[] = array('Wort', 'lbl_drucken');
    $TextKonserven[] = array('Wort', 'txt_BitteWaehlen');
    $TextKonserven[] = array('Liste', 'lst_ALLE_0');
    $TextKonserven[] = array('Wort', 'AuswahlSpeichern');
    $TextKonserven[] = array('Wort', 'lbl_hinzufuegen');
    $TextKonserven[] = array('Wort', 'AlleAnzeigen');

    $AWISBenutzer = awisBenutzer::Init();
    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

    $Recht51000 = $AWISBenutzer->HatDasRecht(51000);

    if ($Recht51000 == 0) {
        $Form->Formular_Start();
        $Form->Fehler_KeineRechte();
        $Form->Formular_Ende();
        die();
    }

    $Form->SchreibeHTMLCode('<form name="frmABESuche" action="./abe_Main.php?cmdAktion=Details" method="POST"  >');
    $Param = unserialize($AWISBenutzer->ParameterLesen('Formular_ABE'));

    $Form->Formular_Start();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['ABF']['ABF_FAHRGESTELLNR'] . ':', 200);
    $Form->Erstelle_TextFeld('*FAHRGESTELLNR', ($Param['SPEICHERN'] == 'on'?$Param['ABF_FAHRGESTELLNR']:''), 20, 200, true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['ABF']['ABF_MOTORNR'] . ':', 200);
    $Form->Erstelle_TextFeld('*MOTORNR', ($Param['SPEICHERN'] == 'on'?$Param['ABF_MOTORNR']:''), 20, 200, true);
    $Form->ZeileEnde();

    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['ABF']['ABF_ATU_NR'] . ':', 200);
    $Form->Erstelle_TextFeld('*ATU_NR', ($Param['SPEICHERN'] == 'on'?$Param['ABF_ATU_NR']:''), 20, 200, true);
    $Form->ZeileEnde();


    // Auswahl kann gespeichert werden
    $Form->ZeileStart();
    $Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Param['SPEICHERN'] == 'on'?'on':''), 20, true, 'on');
    $Form->ZeileEnde();

    $Form->Formular_Ende();

    $Form->SchaltflaechenStart();
    $Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    $Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    $Form->SchaltflaechenEnde();

    $Form->SetzeCursor($AWISCursorPosition);
    $Form->SchreibeHTMLCode('</form>');
} catch (Exception $ex) {
    if ($Form instanceof awisFormular) {
        $Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200906241613");
    } else {
        echo 'allg. Fehler:' . $ex->getMessage();
    }
}
?>