<?php

    require_once("/daten/include/versicherungen_Shuttle.php");
    require_once("/daten/include/awisJobVerwaltung.inc");

    try
    {
        $Job = new awisJobProtokoll(7, 'Starte Versicherungsverarbeitung');
        $Shuttle = new VersicherungenShuttle();

        $Shuttle->Shuttle();
    }
    catch (Exception $ex)
    {
       echo $ex->getMessage().PHP_EOL;
       if(is_object($Job))
       {
           $Job->SchreibeLog(3,9,'Fehler bei der Ausführung:'.$ex->getMessage());
       }
    }
    catch (awisException $e)
    {
		echo $e->getMessage().'|'.$e->getSQL();
	}

?>
