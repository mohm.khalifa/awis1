<?php
try
{
	ini_set('include_path', ini_get('include_path').':/Daten/web/webdaten/PHPExcel:/Daten/web/webdaten/PHPExcel/Shared');
	ini_set('max_execution_time', 6000);
	require_once('PHPExcel.php');
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$Form->DebugAusgabe(1,$_POST);
	$Form->DebugAusgabe(1,$_GET);
	$Form->DebugAusgabe(1,$_FILES);
	
	$AWISWerkzeuge = new awisWerkzeuge();
	
	$StatistikRST['neu']=0;
	$StatistikRST['geaendert']=0;
	$StatistikRST['fehlerhaft']=0;
	$StatistikRRP['neu']=0;
	$StatistikRRP['geaendert']=0;
	$StatistikRRP['fehlerhaft']=0;
	$StatistikWVL['neu']=0;
	$StatistikRST['RSTVorhanden']=0;
	
	// Pr�fsumme ermitten, um zu pr�fen, ob die Datei schon mal importiert wurde
	$MD5 = md5_file($_FILES['IMPORT']['tmp_name']);
	$SQL = 'SELECT *';
	$SQL .= ' FROM IMPORTPROTOKOLL';
	$SQL .= ' WHERE XDI_BEREICH = \'RST\'';
	$SQL .= ' AND XDI_MD5 = '.$DB->FeldInhaltFormat('T',$MD5);
	$rsXDI = $DB->RecordSetOeffnen($SQL);
	$Form->DebugAusgabe(1,$DB->LetzterSQL());
	
	if(!$rsXDI->EOF())
	{
		// Dateien k�nnen immer nur einmal eingelesen werden
		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datei wurde bereits importiert.', 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Benutzer:'.$rsXDI->FeldInhalt('XDI_USER'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Datum:'.$rsXDI->FeldInhalt('XDI_DATUM'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel('Dateiname:'.$rsXDI->FeldInhalt('XDI_DATEINAME'), 800,'Hinweis');
		$Form->ZeileEnde();
		$Form->Trennzeile();
		$Form->Formular_Ende();
	}
	else
	{
		$DB->TransaktionBegin();
		
		$Datei = $_FILES['IMPORT']['tmp_name'];

		// Datei �ffnen
		$XLSXObj = PHPExcel_IOFactory::load($Datei);

		// Blatt untersuchen
		$BlattObj = $XLSXObj->getActiveSheet();
		
		// Kenner lesen
		$Kenner = $BlattObj->getCell('A1')->getValue();
		
		if($Kenner != 'AWIS')
		{
			$Form->Erstelle_TextLabel('Falsche Importdatei.'.$Kenner, 800);
		}
		else
		{
			$Form->Erstelle_TextLabel('Daten werden importiert...', 800);

			$SQL = 'INSERT INTO IMPORTPROTOKOLL(';
			$SQL .= 'XDI_BEREICH, XDI_DATEINAME, XDI_DATUM, XDI_MD5';
			$SQL .= ', XDI_USER,XDI_USERDAT)VALUES (';
			$SQL .= ' \'RST\'';
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$_FILES['IMPORT']['name']);
			$SQL .= ', SYSDATE';
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$MD5);
			$SQL .= ', '.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
			$SQL .= ', SYSDATE)';
			$DB->Ausfuehren($SQL);
			$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
			$SQL = 'SELECT seq_XDI_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$XDI_KEY=$rsKey->FeldInhalt('KEY');
			
			// �berschriften lesen
			$Ueberschrift=array();
			$Zeile = 2;
			$Spalte = 'A';
			$Zelle = $Spalte.$Zeile;
			while($BlattObj->getCell($Zelle)->getValue()!='')
			{
				$Ueberschrift[$BlattObj->getCell($Zelle)->getValue()]=$Spalte++;
				$Zelle = $Spalte.$Zeile;
			}
			$Form->DebugAusgabe(1,$Ueberschrift);

			$FEHLER = false;

			// Jetzt die Daten
			$Zeile=3;
			while ($BlattObj->getCell($Ueberschrift['HERSTELLER'].$Zeile)->getValue()!='')
			{
				//Daten trimmen und ggf umkonvertieren
				$STATUS=strtoupper(trim(KodiereText($BlattObj->getCell($Ueberschrift['STATUS'].$Zeile)->getValue())));
				$ATUNR=strtoupper(trim(KodiereText($BlattObj->getCell($Ueberschrift['ATUNR'].$Zeile)->getValue())));
				$WG=str_pad(trim(KodiereText($BlattObj->getCell($Ueberschrift['WG'].$Zeile)->getValue())),2,'0',STR_PAD_LEFT);
				$SORT=str_pad(trim(KodiereText($BlattObj->getCell($Ueberschrift['SORT'].$Zeile)->getValue())),3,'0',STR_PAD_LEFT);
				
				$GUELTIGAB=trim(KodiereText($BlattObj->getCell($Ueberschrift['GUELTIGAB'].$Zeile)->getValue()));
				$GUELTIGBIS=trim(KodiereText($BlattObj->getCell($Ueberschrift['GUELTIGBIS'].$Zeile)->getValue()));
				
				// Zeit (also Nachkommastellen) weg da diese nicht gebraucht werden
				$GUELTIGAB = (strpos($GUELTIGAB, '.') ? substr($GUELTIGAB, 0,strpos($GUELTIGAB, '.')) :  substr($GUELTIGAB, 0));
				$GUELTIGBIS = (strpos($GUELTIGBIS, '.') ? substr($GUELTIGBIS, 0,strpos($GUELTIGBIS, '.')) :  substr($GUELTIGBIS, 0));
				
				if($GUELTIGAB!='')
				{
					if(is_numeric($GUELTIGAB) AND $GUELTIGAB >30000 AND $GUELTIGAB <99999)
					{
						$GUELTIGAB = date('d.m.Y',strtotime('01-01-1900 + '.$GUELTIGAB .' days - 2 days'));
					}
					else
					{
						$GUELTIGAB = $Form->PruefeDatum($GUELTIGAB,false,false,false);
					}
				}
				
				if($GUELTIGBIS!='')
				{
					if(is_numeric($GUELTIGBIS) AND $GUELTIGBIS >30000 AND $GUELTIGBIS <99999)
					{
						$GUELTIGBIS = date('d.m.Y',strtotime('01-01-1900 + '.$GUELTIGBIS .' days - 2 days'));
					}
					else
					{
						$GUELTIGBIS = $Form->PruefeDatum($GUELTIGBIS,false,false,false);
					}
				}
				
				$EAN=trim(KodiereText($BlattObj->getCell($Ueberschrift['EAN'].$Zeile)->getValue()));
				$HERSTELLER=strtoupper(trim(KodiereText($BlattObj->getCell($Ueberschrift['HERSTELLER'].$Zeile)->getValue())));
				$BEZEICHNUNG=trim(KodiereText($BlattObj->getCell($Ueberschrift['BEZEICHNUNG'].$Zeile)->getValue()));
				$LIE_NR=trim(KodiereText($BlattObj->getCell($Ueberschrift['LIE_NR'].$Zeile)->getValue()));
				$LARTNR=trim(KodiereText($BlattObj->getCell($Ueberschrift['LARTNR'].$Zeile)->getValue()));
				$BRUTTO=trim(KodiereText($BlattObj->getCell($Ueberschrift['BRUTTO'].$Zeile)->getValue()));
				$BREITE=trim(KodiereText($BlattObj->getCell($Ueberschrift['BREITE'].$Zeile)->getValue()));
				$QUERSCHNITT=trim(KodiereText($BlattObj->getCell($Ueberschrift['QUERSCHNITT'].$Zeile)->getValue()));
				$BAUART=trim(KodiereText($BlattObj->getCell($Ueberschrift['BAUART'].$Zeile)->getValue()));
				$INNENDURCHMESSER=trim(KodiereText($BlattObj->getCell($Ueberschrift['INNENDURCHMESSER'].$Zeile)->getValue()));
				$FELGENRIPPE=trim(KodiereText($BlattObj->getCell($Ueberschrift['FELGENRIPPE'].$Zeile)->getValue()));
				$LLKW=trim(KodiereText($BlattObj->getCell($Ueberschrift['LLKW'].$Zeile)->getValue()));
				$LOADINDEX=trim(KodiereText($BlattObj->getCell($Ueberschrift['LOADINDEX'].$Zeile)->getValue()));
				$LOADINDEX2=trim(KodiereText($BlattObj->getCell($Ueberschrift['LOADINDEX2'].$Zeile)->getValue()));
				$LOADINDEX3=trim(KodiereText($BlattObj->getCell($Ueberschrift['LOADINDEX3'].$Zeile)->getValue()));
				$RF=trim(KodiereText($BlattObj->getCell($Ueberschrift['RF'].$Zeile)->getValue()));
				$SPEEDINDEX=trim(KodiereText($BlattObj->getCell($Ueberschrift['SPEEDINDEX'].$Zeile)->getValue()));
				$SPEEDINDEX2=trim(KodiereText($BlattObj->getCell($Ueberschrift['SPEEDINDEX2'].$Zeile)->getValue()));
				$ZUSATZBEMERKUNG=trim(KodiereText($BlattObj->getCell($Ueberschrift['ZUSATZBEMERKUNG'].$Zeile)->getValue()));
				$ZUSATZBEMERKUNG2=trim(KodiereText($BlattObj->getCell($Ueberschrift['ZUSATZBEMERKUNG2'].$Zeile)->getValue()));
				$ZUSATZBEMERKUNG3=trim(KodiereText($BlattObj->getCell($Ueberschrift['ZUSATZBEMERKUNG3'].$Zeile)->getValue()));
				$TYP=trim(KodiereText($BlattObj->getCell($Ueberschrift['TYP'].$Zeile)->getValue()));
				$TYP2=trim(KodiereText($BlattObj->getCell($Ueberschrift['TYP2'].$Zeile)->getValue()));
				$TYP3=trim(KodiereText($BlattObj->getCell($Ueberschrift['TYP3'].$Zeile)->getValue()));
				$ROLLRES=trim(KodiereText($BlattObj->getCell($Ueberschrift['ROLLRES'].$Zeile)->getValue()));
				$WETGRIP=trim(KodiereText($BlattObj->getCell($Ueberschrift['WETGRIP'].$Zeile)->getValue()));
				$NOISEPE=trim(KodiereText($BlattObj->getCell($Ueberschrift['NOISEPE'].$Zeile)->getValue()));
				$NOISECL=trim(KodiereText($BlattObj->getCell($Ueberschrift['NOISECL'].$Zeile)->getValue()));
				$BEMERKUNG=trim(KodiereText($BlattObj->getCell($Ueberschrift['BEMERKUNG'].$Zeile)->getValue()));
				$WINTERTAUGLICHKEIT=strtoupper(trim(KodiereText($BlattObj->getCell($Ueberschrift['WINTERTAUGLICHKEIT'].$Zeile)->getValue())));
				$WINTERTAUGLICHKEIT=($WINTERTAUGLICHKEIT=="X")?$WINTERTAUGLICHKEIT:"";
                $EISGRIFF=trim(KodiereText($BlattObj->getCell($Ueberschrift['EISGRIFF'].$Zeile)->getValue()));
                $EISGRIFF=($EISGRIFF=="X")?$EISGRIFF:"";
                $INFO=trim(KodiereText($BlattObj->getCell($Ueberschrift['INFO'].$Zeile)->getValue()));
                $URL4=trim(KodiereText($BlattObj->getCell($Ueberschrift['URL4'].$Zeile)->getValue()));
                $URL5=trim(KodiereText($BlattObj->getCell($Ueberschrift['URL5'].$Zeile)->getValue()));
				$RRP_KEY=trim(KodiereText($BlattObj->getCell($Ueberschrift['RRP_KEY'].$Zeile)->getValue()));
				$RST_KEY=trim(KodiereText($BlattObj->getCell($Ueberschrift['RST_KEY'].$Zeile)->getValue()));

				
				//*********************************************************************************
				// Daten importieren
				//*********************************************************************************
				$SQL = 'SELECT RST_KEY, RST_BEMERKUNG';
				$SQL .= ' FROM REIFENSTAMM';
				if ($RST_KEY!='0')
				{
					$SQL .= ' WHERE RST_KEY = :var_N0_RST_KEY';
					$DB->SetzeBindevariable('RST', 'var_N0_RST_KEY', $RST_KEY, awisDatenbank::VAR_TYP_GANZEZAHL);
				}
				else
				{
					$SQL .= ' WHERE (RST_LIE_NR = :var_T_RST_LIE_NR ';
					$SQL .= ' AND RST_LARTNR = :var_T_RST_LARTNR) ';
					$DB->SetzeBindevariable('RST', 'var_T_RST_LIE_NR', $LIE_NR, awisDatenbank::VAR_TYP_TEXT,'TU');
					$DB->SetzeBindevariable('RST', 'var_T_RST_LARTNR', $LARTNR, awisDatenbank::VAR_TYP_TEXT,'TU');
				}
				$rsRST = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('RST'));
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
				$Form->DebugAusgabe(1,$RST_KEY);
				
				if ($RST_KEY!='0') 
				{
					if ($rsRST->EOF())
					{
						$speichern=false;
						echo '<br>(F) Zeile '.$Zeile.': Kein Update da Reifenstammsatz nicht mehr vorhanden! Lieferantennummer: ' . $LIE_NR . " LieferantenArtikelnummer: " . $LARTNR ;
						$StatistikRST['fehlerhaft']++;
						$Zeile++;
						continue;
					}
				}
				else
				{
					if (!$rsRST->EOF())
					{
						echo '<br>(F) Zeile '.$Zeile.': Keine Neuanlage da Reifenstammsatz bereits vorhanden!  Lieferantennummer: ' . $LIE_NR . " LieferantenArtikelnummer: " . $LARTNR ;
						$StatistikRST['RSTVorhanden']++;
						$Zeile++;
						continue;
					}
				}
				
				if(!isset($HerstellerCache[$HERSTELLER]))
				{
					$SQL = 'SELECT REH_KEY';
					$SQL .= ' FROM REIFENHERSTELLER';
					$SQL .= ' WHERE TRIM(UPPER(REH_BEZEICHNUNG)) = TRIM(UPPER(:var_T_REH_BEZEICHNUNG)) ';
					$DB->SetzeBindevariable('REH', 'var_T_REH_BEZEICHNUNG', $HERSTELLER, awisDatenbank::VAR_TYP_TEXT,'TU');
					$rsREH = $DB->RecordSetOeffnen($SQL, $DB->Bindevariablen('REH'));
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
					if($rsREH->EOF())
					{
						$REH_KEY=0;
						//weiter unten wird eine Wiedervorlage erzeugt
					
					}
					else 
					{
						$HerstellerCache[$HERSTELLER] = $REH_KEY = $rsREH->FeldInhalt('REH_KEY');					
					}
				}
				else
				{
					$REH_KEY = $HerstellerCache[$HERSTELLER];
				}				
				
				//Falls ATUNR mit in der Datei angegeben ist:
				unset($rsAST);
				unset($rsSON);
				if ($ATUNR!='')
				{
					// AST vorhanden?
					$SQL = 'SELECT *';
					$SQL .= ' FROM v_reifen_ast';
					$SQL .= ' WHERE AST_ATUNR = :var_T_ATUNR';
					
					$BindeVariablen = array();
					$BindeVariablen['var_T_ATUNR']= $ATUNR;
					$rsAST = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
					if($rsAST->EOF())
					{
						unset($rsAST);
						
						//Pr�fen, ob es sich um einen Sonderbestellartikel handelt
						$SQL  = 'SELECT reh_key ';
						$SQL .= ' FROM V_REIFENHERSTELLER';
						$SQL .= ' WHERE REH_SONDERBESTNR = :var_T_ATUNR';
						$BindeVariablen['var_T_ATUNR']= $ATUNR;
						$rsSON = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
						
						if ($rsSON->EOF())
						{
							unset($rsSON);
						}
					}
				}
				
				if($rsRST->EOF())
				{
					// Jetzt den neuen anlegen
					$SQL  = 'INSERT';
					$SQL .= ' INTO REIFENSTAMM (';
					$SQL .= ' RST_BEZEICHNUNG,';
					$SQL .= ' RST_REH_KEY,';
					$SQL .= ' RST_LIE_NR,';
					$SQL .= ' RST_LARTNR,';
					$SQL .= ' RST_BREITE,';
					$SQL .= ' RST_QUERSCHNITT,';
					$SQL .= ' RST_BAUART,';
					$SQL .= ' RST_INNENDURCHMESSER,';
					$SQL .= ' RST_LLKW,';
					$SQL .= ' RST_LOADINDEX,';
					$SQL .= ' RST_LOADINDEX2,';
					$SQL .= ' RST_LOADINDEX3,';
					$SQL .= ' RST_RF,';
					$SQL .= ' RST_SPEEDINDEX,';
					$SQL .= ' RST_SPEEDINDEX2,';
					$SQL .= ' RST_ZUSATZBEMERKUNG,';
					$SQL .= ' RST_ZUSATZBEMERKUNG2,';
					$SQL .= ' RST_ZUSATZBEMERKUNG3,';
					$SQL .= ' RST_TYP,';
					$SQL .= ' RST_TYP2,';
					$SQL .= ' RST_TYP3,';
					$SQL .= ' RST_ROLLRES,';
					$SQL .= ' RST_WETGRIP,';
					$SQL .= ' RST_NOISEPE,';
					$SQL .= ' RST_NOISECL,';
					$SQL .= ' RST_KBPREIS,';
					$SQL .= ' RST_EAN,';
					$SQL .= ' RST_FELGENRIPPE,';
					$SQL .= ' RST_XDI_KEY,';
					$SQL .= ' RST_CREADAT,';
					$SQL .= ' RST_IMPORTSTATUS,';
					$SQL .= ' RST_STATUS,';
					$SQL .= ' RST_BEMERKUNG,';
					$SQL .= ' RST_WINTERTAUGLICHKEIT,';
					$SQL .= ' RST_EISGRIFF,';
					$SQL .= ' RST_INFO,';
					$SQL .= ' RST_URL4,';
					$SQL .= ' RST_URL5,';
					$SQL .= ' RST_USER,';
					$SQL .= ' RST_USERDAT';
					$SQL .= ' ) VALUES (';
					$SQL .= ' '.$DB->FeldInhaltFormat('T',$BEZEICHNUNG);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$REH_KEY);
					$SQL .= ','.$DB->FeldInhaltFormat('T',!isset($rsAST)?$LIE_NR:$rsAST->FeldInhalt('AST_LIEFNR'));
					$SQL .= ','.$DB->FeldInhaltFormat('T',!isset($rsAST)?$LARTNR:$rsAST->FeldInhalt('AST_LIEFARTNR'));
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$BREITE);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$QUERSCHNITT);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$BAUART);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$INNENDURCHMESSER);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$LLKW);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$LOADINDEX);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$LOADINDEX2);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$LOADINDEX3);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$RF);
					$SQL .= ','.$DB->FeldInhaltFormat('TU',$SPEEDINDEX);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$SPEEDINDEX2);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$ZUSATZBEMERKUNG);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$ZUSATZBEMERKUNG2);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$ZUSATZBEMERKUNG3);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$TYP);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$TYP2);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$TYP3);
					$SQL .= ','.$DB->FeldInhaltFormat('TU',!isset($rsAST)?$ROLLRES:$rsAST->FeldInhalt('RLA_ROLLRES'));
					$SQL .= ','.$DB->FeldInhaltFormat('TU',!isset($rsAST)?$WETGRIP:$rsAST->FeldInhalt('RLA_WETGRIP'));
					$SQL .= ','.$DB->FeldInhaltFormat('TU',!isset($rsAST)?$NOISEPE:$rsAST->FeldInhalt('RLA_NOISEPE'));
					$SQL .= ','.$DB->FeldInhaltFormat('TU',!isset($rsAST)?$NOISECL:$rsAST->FeldInhalt('RLA_NOISECL'));
					$SQL .= ','.$DB->FeldInhaltFormat('N2',!isset($rsAST)?$BRUTTO:$rsAST->FeldInhalt('AST_BRUTTO'));
					$SQL .= ','.$DB->FeldInhaltFormat('T',!isset($rsAST)?$EAN:$rsAST->FeldInhalt('AST_HAUPTEAN'));
					$SQL .= ','.$DB->FeldInhaltFormat('TU',$FELGENRIPPE);
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$XDI_KEY);
					$SQL .= ',SYSDATE';
					$SQL .= ','.$DB->FeldInhaltFormat('N0',$STATUS); //TODO: Nachfragen welcher default Status
					$SQL .= ',10';
					$SQL .= ','.$DB->FeldInhaltFormat('T',$BEMERKUNG);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$WINTERTAUGLICHKEIT);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$EISGRIFF);
					$SQL .= ','.$DB->FeldInhaltFormat('T',substr($INFO,0,15));
                    $SQL .= ','.$DB->FeldInhaltFormat('T',$URL4);
                    $SQL .= ','.$DB->FeldInhaltFormat('T',$URL5);
					$SQL .= ','.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ', SYSDATE)';
					
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
						
					$SQL = 'SELECT seq_RST_key.CurrVal AS KEY FROM dual';
					$rsKEY = $DB->RecordSetOeffnen($SQL);
					$RST_key = $rsKEY->FeldInhalt('KEY');

					$StatistikRST['neu']++;
				}
				else
				{
					$SQL = 'UPDATE REIFENSTAMM';
					$SQL .= ' SET RST_BEZEICHNUNG = '.$DB->FeldInhaltFormat('T',$BEZEICHNUNG);
					$SQL .= ', RST_REH_KEY = '.$DB->FeldInhaltFormat('N0',$REH_KEY);
					$SQL .= ', RST_LIE_NR = LPAD('.$DB->FeldInhaltFormat('T',!isset($rsAST)?$LIE_NR:$rsAST->FeldInhalt('AST_LIEFNR')).',4,\'0\')';
					$SQL .= ', RST_LARTNR = '.$DB->FeldInhaltFormat('T',!isset($rsAST)?$LARTNR:$rsAST->FeldInhalt('AST_LIEFARTNR'));
					$SQL .= ', RST_BREITE = '.$DB->FeldInhaltFormat('N0',$BREITE);
					$SQL .= ', RST_QUERSCHNITT = '.$DB->FeldInhaltFormat('T',$QUERSCHNITT);
					$SQL .= ', RST_BAUART = '.$DB->FeldInhaltFormat('T',$BAUART);
					$SQL .= ', RST_INNENDURCHMESSER = '.$DB->FeldInhaltFormat('N0',$INNENDURCHMESSER);
					$SQL .= ', RST_LLKW = '.$DB->FeldInhaltFormat('T',$LLKW);
					$SQL .= ', RST_LOADINDEX = '.$DB->FeldInhaltFormat('T',$LOADINDEX);
					$SQL .= ', RST_LOADINDEX2 = '.$DB->FeldInhaltFormat('N0',$LOADINDEX2);
					$SQL .= ', RST_LOADINDEX3 = '.$DB->FeldInhaltFormat('N0',$LOADINDEX3);
					$SQL .= ', RST_RF = '.$DB->FeldInhaltFormat('T',$RF);
					$SQL .= ', RST_SPEEDINDEX = '.$DB->FeldInhaltFormat('T',$SPEEDINDEX);
					$SQL .= ', RST_SPEEDINDEX2 = '.$DB->FeldInhaltFormat('T',$SPEEDINDEX2);
					$SQL .= ', RST_ZUSATZBEMERKUNG = '.$DB->FeldInhaltFormat('T',$ZUSATZBEMERKUNG);
					$SQL .= ', RST_ZUSATZBEMERKUNG2 = '.$DB->FeldInhaltFormat('T',$ZUSATZBEMERKUNG2);
					$SQL .= ', RST_ZUSATZBEMERKUNG3 = '.$DB->FeldInhaltFormat('T',$ZUSATZBEMERKUNG3);
					$SQL .= ', RST_TYP = '.$DB->FeldInhaltFormat('N0',$TYP);
					$SQL .= ', RST_TYP2 = '.$DB->FeldInhaltFormat('N0',$TYP2);
					$SQL .= ', RST_TYP3 = '.$DB->FeldInhaltFormat('N0',$TYP3);
					$SQL .= ', RST_ROLLRES = '.$DB->FeldInhaltFormat('T',!isset($rsAST)?$ROLLRES:$rsAST->FeldInhalt('RLA_ROLLRES'));
					$SQL .= ', RST_WETGRIP = '.$DB->FeldInhaltFormat('T',!isset($rsAST)?$WETGRIP:$rsAST->FeldInhalt('RLA_WETGRIP'));
					$SQL .= ', RST_NOISEPE = '.$DB->FeldInhaltFormat('T',!isset($rsAST)?$NOISEPE:$rsAST->FeldInhalt('RLA_NOISEPE'));
					$SQL .= ', RST_NOISECL = '.$DB->FeldInhaltFormat('T',!isset($rsAST)?$NOISECL:$rsAST->FeldInhalt('RLA_NOISECL'));
					$SQL .= ', RST_KBPREIS = '.$DB->FeldInhaltFormat('N2',!isset($rsAST)?$BRUTTO:$rsAST->FeldInhalt('AST_BRUTTO'));
					$SQL .= ', RST_EAN = '.$DB->FeldInhaltFormat('T',!isset($rsAST)?$EAN:$rsAST->FeldInhalt('AST_HAUPTEAN'));
					$SQL .= ', RST_FELGENRIPPE = '.$DB->FeldInhaltFormat('T',$FELGENRIPPE);
					$SQL .= ', RST_XDI_KEY='.$DB->FeldInhaltFormat('N0',$XDI_KEY);
					$SQL .= ', RST_IMPORTSTATUS = 30';
					$SQL .= ', RST_STATUS = '.$DB->FeldInhaltFormat('N0',$STATUS);

					if ($rsRST->FeldInhalt('RST_BEMERKUNG')!='' and $BEMERKUNG!='')
					{
						$SQL .= ', RST_BEMERKUNG = RST_BEMERKUNG || CHR(13) || CHR(10) || '.$DB->FeldInhaltFormat('T',$BEMERKUNG);
					}
					elseif ($BEMERKUNG!='')
					{
						$SQL .= ', RST_BEMERKUNG = '.$DB->FeldInhaltFormat('T',$BEMERKUNG);
					}

                    $SQL .= ', RST_WINTERTAUGLICHKEIT = '.$DB->FeldInhaltFormat('T',$WINTERTAUGLICHKEIT);
                    $SQL .= ', RST_EISGRIFF = '.$DB->FeldInhaltFormat('T',$EISGRIFF);
                    $SQL .= ', RST_INFO = '.$DB->FeldInhaltFormat('T',substr($INFO,0,15));
                    $SQL .= ', RST_URL4 = '.$DB->FeldInhaltFormat('T',$URL4);
                    $SQL .= ', RST_URL5 = '.$DB->FeldInhaltFormat('T',$URL5);
					$SQL .= ', RST_USER=\''.$AWISBenutzer->BenutzerName().'\'';
					$SQL .= ', RST_USERDAT=SYSDATE';
					$SQL .= ' WHERE RST_key=0' . $rsRST->FeldInhalt('RST_KEY') . '';

					$RST_key = $rsRST->FeldInhalt('RST_KEY');
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
						
					$StatistikRST['geaendert']++;
				}

				//falls vorher keinen Hersteller gefunden eine Wiedervorlage generieren
				if ($REH_KEY==0) 
				{
					$SQL = ' INSERT INTO WIEDERVORLAGEN';
					$SQL .= ' (WVL_XTN_KUERZEL,WVL_XXX_KEY,WVL_DATUM,WVL_TEXT,WVL_BEARBEITER,WVL_TYP';
					$SQL .= ' ,WVL_KENNUNG,WVL_STATUS,WVL_USER,WVL_USERDAT';
					$SQL .= ' ) VALUES (';
					$SQL .= ' \'RST\'';
					$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$RST_key);
					$SQL .= ' ,TRUNC(SYSDATE)';
					$SQL .= ' ,\'Hersteller in Datei <'.$_FILES['IMPORT']['name'].'> Zeile: '.$Zeile.' bei Import nicht gefunden. Daten m�ssen korrigiert werden!\'';
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ' ,\'A\'';
					$SQL .= ' ,\'REHFEHLT\'';
					$SQL .= ' ,1';
					$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
					$SQL .= ' ,SYSDATE';
					$SQL .= ' )';
					
					$DB->Ausfuehren($SQL,'',true);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
				}
				
				//Falls die ATUNR in der Datei angegeben ist:
				if (KodiereText($BlattObj->getCell($Ueberschrift['ATUNR'].$Zeile)->getValue())!='')
				{
					// Pr�fen, ob ein Reifenpflegesatz f�r diesen Reifenstammsatz existiert.
					// Wenn ja, ATUNR aus Datei ignorieren und nichts machen da 
					// Reifenstammsatz bereits einer ATUNR zugeordnet
					$SQL = 'SELECT *';
					$SQL .= ' FROM reifenpflege';
					$SQL .= ' WHERE RRP_RST_KEY = :var_N0_RST_KEY';
					$BindeVariablen = array();
					$BindeVariablen['var_N0_RST_KEY']= $RST_key;
					$rsFelder = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
					$Form->DebugAusgabe(1,$DB->LetzterSQL());
					
					if ($rsFelder->EOF())
					{
						
						if (!isset($rsSON))
						{
							// ATUNR ist keine Sonderbestellnummer:
							// Pr�fen, ob bereits ein Reifenpflegesatz f�r die angegebene ATUNR existiert
							// Wenn nein, Anlage eines Reifenpflegedatensatzes und verkn�pfen
							// zum Reifenstammsatz
							$SQL = 'SELECT *';
							$SQL .= ' FROM reifenpflege';
							$SQL .= ' WHERE RRP_ATUNR = :var_T_RRP_ATUNR';
							$BindeVariablen = array();
							$BindeVariablen['var_T_RRP_ATUNR']= $ATUNR;
							$rsFelder = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
							$Form->DebugAusgabe(1,$DB->LetzterSQL());
						}
						else
						{
							// ATUNR ist eine Sonderbestellnummer:
							// Pr�fen, ob ein "freier" Reifenpflegesatz f�r die angegebene ATUNR existiert
							// d.h. zu dieser ATUNR soll kein Reifenstammsatz verkn�ft sein.
							// Wenn vorhanden, Anlage eines Reifenpflegedatensatzes und verkn�pfen
							// zum Reifenstammsatz
							$SQL = 'SELECT *';
							$SQL .= ' FROM reifenpflege';
							$SQL .= ' WHERE RRP_ATUNR = :var_T_RRP_ATUNR';
							$SQL .= ' AND RRP_RST_KEY IS NULL';
							$BindeVariablen = array();
							$BindeVariablen['var_T_RRP_ATUNR']= $ATUNR;
							$rsFelder = $DB->RecordSetOeffnen($SQL, $BindeVariablen);
							$Form->DebugAusgabe(1,$DB->LetzterSQL());
						}

						if ($rsFelder->EOF()) 
						{
							$SQL  = 'INSERT';
							$SQL .= ' INTO REIFENPFLEGE (';
							$SQL .= '  RRP_RST_KEY';
							$SQL .= ', RRP_AST_KEY';
							$SQL .= ', RRP_ATUNR';
							$SQL .= ', RRP_BEZEICHNUNGWW';
							$SQL .= ', RRP_WGR_ID';
							$SQL .= ', RRP_WUG_ID';
							$SQL .= ', RRP_KENN1';
							$SQL .= ', RRP_KENN2';
							$SQL .= ', RRP_KENN_L2';
							$SQL .= ', RRP_SUCH1';
							$SQL .= ', RRP_SUCH2';
							$SQL .= ', RRP_GUELTIGAB';
							$SQL .= ', RRP_GUELTIGBIS';
							$SQL .= ', RRP_BEMERKUNG';
							$SQL .= ', RRP_USER';
							$SQL .= ', RRP_USERDAT';
							$SQL .= ' ) VALUES (';
							$SQL .= '  '.$DB->FeldInhaltFormat('N0',$RST_key);
							$SQL .= ' ,'.(isset($rsAST)?$DB->FeldInhaltFormat('N0',$rsAST->FeldInhalt('AST_KEY')):'null');
							$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_ATUNR'):$ATUNR);
							$SQL .= ' ,'.(isset($rsAST)?$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_BEZEICHNUNGWW')):'null');
							// WUG / WGR aus der DB, wenn vorhanden
							$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('WUG_WGR_ID'):$WG);
							$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('WUG_ID'):$SORT);
							$SQL .= ' ,'.$DB->FeldInhaltFormat('T',isset($rsAST)?$rsAST->FeldInhalt('AST_KENNUNG'):'S');
							$SQL .= ' ,'.(isset($rsAST)?$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_KENN2')):'null');
							$SQL .= ' ,'.(isset($rsAST)?$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_KENN_L2')):'null');
							$SQL .= ' ,'.(isset($rsAST)?$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_SUCH1')):'null');
							$SQL .= ' ,'.(isset($rsAST)?$DB->FeldInhaltFormat('T',$rsAST->FeldInhalt('AST_SUCH2')):'null');
							// Sofort g�ltig, wenn der Artikel in der VAX angelegt ist.
							$SQL .= ' ,'.(isset($rsAST)?'TRUNC(SYSDATE)': '\'' . $GUELTIGAB . '\'');
							$SQL .= ' ,'.(isset($rsAST)?$DB->FeldInhaltFormat('D','31.12.2030') : '\'' . $GUELTIGBIS . '\'');
							$SQL .= ' ,\'Insert durch Reifenstammimport <'.$_FILES['IMPORT']['name'].'> am '.date('d.m.Y H:i:s').'\' || CHR(13) || CHR(10) ';
							$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
							$SQL .= ' , SYSDATE)';
							
							$DB->Ausfuehren($SQL,'',true);
							$Form->DebugAusgabe(1,$DB->LetzterSQL());
							$StatistikRRP['neu']++;
							
							// Neuen Artikel angelegt, der nicht in der VAX ist
							// und keine Sonderbestellnummer ->Wiedervorlage generieren
							if(!isset($rsAST) AND !isset($rsSON))
							{
								$SQL = 'SELECT seq_RRP_key.CurrVal AS KEY FROM dual';
								$rsKEY = $DB->RecordSetOeffnen($SQL);
								$RRP_key = $rsKEY->FeldInhalt('KEY');
							
								//Wiedervorlage generieren
								$SQL = ' INSERT INTO WIEDERVORLAGEN';
								$SQL .= ' (WVL_XTN_KUERZEL,WVL_XXX_KEY,WVL_DATUM,WVL_TEXT,WVL_BEARBEITER';
								$SQL .= ' ,WVL_TYP,WVL_KENNUNG,WVL_STATUS,WVL_USER,WVL_USERDAT';
								$SQL .= ' ) VALUES (';
								$SQL .= ' \'RRP\'';
								$SQL .= ' ,'.$DB->FeldInhaltFormat('N0',$RRP_key);
								$SQL .= ' ,TRUNC(SYSDATE)';
								$SQL .= ' ,\'Artikel aus <'.$_FILES['IMPORT']['name'].'> Zeile: '.$Zeile.' bei Import neu angelegt. Daten m�ssen komplettiert werden!\'';
								$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
								$SQL .= ' ,\'A\'';
								$SQL .= ' ,\'RRPNICHTKOMPLETT\'';
								$SQL .= ' ,1';
								$SQL .= ' ,'.$DB->FeldInhaltFormat('T',$AWISBenutzer->BenutzerName());
								$SQL .= ' ,SYSDATE';
								$SQL .= ' )';
						
								$DB->Ausfuehren($SQL,'',true);
							}
						}
						elseif ($rsFelder->FeldInhalt('RRP_RST_KEY')=='')
						{
							//es existiert ein Reifenpflegesatz, aber ohne Verkn�pfung zum Reifenstammsatz
							//-> jetzt Reifenpflegesatz mit Reifenstammsatz verkn�pfen
							//au�erdem GueltigAb- und GueltigBis-Datum updaten 
							$SQL = 'UPDATE REIFENPFLEGE';
							$SQL .= ' SET RRP_RST_KEY = '.$DB->FeldInhaltFormat('N0',$RST_key);
							
							if ($rsFelder->FeldInhalt('RRP_BEMERKUNG')!='')
							{
								$SQL .= ', RRP_BEMERKUNG = RRP_BEMERKUNG || CHR(13) || CHR(10) || \'Update durch Reifenstammimport <'.$_FILES['IMPORT']['name'].'> am '.date('d.m.Y H:i:s').'\'';
							}
							else
							{
								$SQL .= ', RRP_BEMERKUNG = \'Update durch Reifenstammimport <'.$_FILES['IMPORT']['name'].'> am '.date('d.m.Y H:i:s').'\'';
							}
								
							$SQL .= ', RRP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
							$SQL .= ', RRP_USERDAT=SYSDATE';
							$SQL .= ", RRP_GUELTIGAB = '" . $GUELTIGAB . "'";
							$SQL .= ", RRP_GUELTIGBIS = '" . $GUELTIGBIS . "'";
							$SQL .= ' WHERE RRP_key=0' . $rsFelder->FeldInhalt('RRP_KEY') . '';
							
							$DB->Ausfuehren($SQL,'',true);
							$Form->DebugAusgabe(1,$DB->LetzterSQL());
							
							$StatistikRRP['geaendert']++;
						}
					}
					elseif(!$rsFelder->EOF() && isset($rsSON))
					{
						//Reifen ist Sonderbestellung + es gibt bereits eine Zordnung
						//von Stammsatz zu Pflegesatz. Updaten damit FA trotzdem z.B.
						//die Gueltigkeit festlegen kann.
						$SQL = 'UPDATE REIFENPFLEGE SET';							
						if ($rsFelder->FeldInhalt('RRP_BEMERKUNG')!='')
						{
							$SQL .= ' RRP_BEMERKUNG = RRP_BEMERKUNG || CHR(13) || CHR(10) || \'Update durch Reifenstammimport <'.$_FILES['IMPORT']['name'].'> am '.date('d.m.Y H:i:s').'\'';
						}
						else
						{
							$SQL .= ' RRP_BEMERKUNG = \'Update durch Reifenstammimport <'.$_FILES['IMPORT']['name'].'> am '.date('d.m.Y H:i:s').'\'';
						}
						
						$SQL .= ', RRP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', RRP_USERDAT=SYSDATE';
						$SQL .= ", RRP_GUELTIGAB = '" . $GUELTIGAB . "'";
						$SQL .= ", RRP_GUELTIGBIS = '" . $GUELTIGBIS . "'";
						$SQL .= ' WHERE RRP_key=0' . $rsFelder->FeldInhalt('RRP_KEY') . '';
							
						$DB->Ausfuehren($SQL,'',true);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());
							
						$StatistikRRP['geaendert']++;
					}
					elseif(!$rsFelder->EOF())
					{
						// Reifen ist keine Sonderbestellung + es gibt bereits eine Zuordnung
						// G�ltigkeitsdatum wird hier geupdatet
						$SQL = 'UPDATE REIFENPFLEGE SET';
						if ($rsFelder->FeldInhalt('RRP_BEMERKUNG')!='')
						{
							$SQL .= ' RRP_BEMERKUNG = RRP_BEMERKUNG || CHR(13) || CHR(10) || \'Update durch Reifenstammimport <'.$_FILES['IMPORT']['name'].'> am '.date('d.m.Y H:i:s').'\'';
						}
						else
						{
							$SQL .= ' RRP_BEMERKUNG = \'Update durch Reifenstammimport <'.$_FILES['IMPORT']['name'].'> am '.date('d.m.Y H:i:s').'\'';
						}
						
						$SQL .= ', RRP_USER=\''.$AWISBenutzer->BenutzerName().'\'';
						$SQL .= ', RRP_USERDAT=SYSDATE';
						$SQL .= ", RRP_GUELTIGAB = '" . $GUELTIGAB . "'";
						$SQL .= ", RRP_GUELTIGBIS = '" . $GUELTIGBIS . "'";
						$SQL .= ' WHERE RRP_key=0' . $rsFelder->FeldInhalt('RRP_KEY') . '';
							
						$DB->Ausfuehren($SQL,'',true);
						$Form->DebugAusgabe(1,$DB->LetzterSQL());
							
						$StatistikRRP['geaendert']++;
					}
				}	

				$Zeile++;
			}
		}

		$DB->TransaktionCommit();
		
		$Form->Erstelle_TextLabel('-- Reifenstamm neue Daten: '.$StatistikRST['neu'], 800);
		$Form->Erstelle_TextLabel('-- Reifenpflege ge&auml;nderte Daten: '.$StatistikRRP['geaendert'], 800);		
		$Form->Erstelle_TextLabel('-- Reifenstamm fehlerhafte Daten: '.$StatistikRST['fehlerhaft'], 800);
		$Form->Erstelle_TextLabel('-- Reifenpflege neue Daten: '.$StatistikRRP['neu'], 800);
		$Form->Erstelle_TextLabel('-- Reifenstamm ge&auml;nderte Daten: '.$StatistikRST['geaendert'], 800);
		$Form->Erstelle_TextLabel('-- Reifenpflege Wiedervorlagen: '.$StatistikWVL['neu'], 800);
		$Form->Erstelle_TextLabel('-- Reifenstamm bereits vorhanden: '.$StatistikRST['RSTVorhanden'], 800);
		
	}
}
catch (awisException $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL(),$ex->getLine());
}
catch (Exception $ex)
{
	if($DB->TransaktionAktiv())
	{
		$DB->TransaktionRollback();
	}
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

/**
 * Kodierung des Textes, damit Umlaute richtig gespeichert werden
 * @param string $Text
 */
function KodiereText($Text)
{
    $Erg = iconv('UTF-8','WINDOWS-1252',$Text);
    return ($Erg);
}
?>