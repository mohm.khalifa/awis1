<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('SBI','%');
	$TextKonserven[]=array('MBW','%');
	$TextKonserven[]=array('MFI','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','Link_Route');
	$TextKonserven[]=array('Wort','Link_Maps');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht5001= $AWISBenutzer->HatDasRecht(5001);		// Register in Filialen
	$Recht37000= $AWISBenutzer->HatDasRecht(32010);		// Geldentsorger in Filialinfo
	if(($Recht5001&11)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$SQL = "SELECT SBT_KEY,SBT_MO,SBT_DI,SBT_MI,SBT_DO,SBT_FR,SBT_SA,SBT_SBN_KEY,SBT_USER,to_char(SBT_USERDAT,'DD.MM.YYYY HH24:mi:ss')as SBT_USERDAT";
	$SQL .= ' FROM SBABHOLTAGE';
	$SQL .= ' WHERE SBT_FIL_ID ='.$AWIS_KEY1;
	
	$rsTerminals = $DB->RecordSetOeffnen($SQL);
	
	
	$Form->Formular_Start();
	// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsTerminals->FeldInhalt('SBT_USER'));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsTerminals->FeldInhalt('SBT_USERDAT'));
	$Form->InfoZeile($Felder, '');
	
	$SQL = 'SELECT SBN_KEY,SBN_ENTSORGERNAME';
	$SQL .= ' FROM SBENTSORGER';
	$SQL .= ' WHERE SBN_AKTIV = 1';
	
	$rsENT = $DB->RecordSetOeffnen($SQL);
	
	if(($Recht37000&1)==1)
	{
	
		if(($Recht37000&2)==2)
		{
			$Edit = true;
		}
		else
		{
			$Edit = false;
		}
		
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['EntsorgerTage'],100, 'font-weight:bolder');
        $Form->Trennzeile('O');
        $Form->ZeileEnde();
        $Form->ZeileStart();
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['Montag']. ': ',70);
        $Form->Erstelle_Checkbox('CHKSBI_MO',$rsTerminals->FeldInhalt('SBT_MO'),50,$Edit,1);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['Dienstag']. ': ',70);
        $Form->Erstelle_Checkbox('CHKSBI_DI',$rsTerminals->FeldInhalt('SBT_DI'),50,$Edit,1);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['Mittwoch']. ': ',70);
        $Form->Erstelle_Checkbox('CHKSBI_MI',$rsTerminals->FeldInhalt('SBT_MI'),50,$Edit,1);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['Donnerstag']. ': ',70);
        $Form->Erstelle_Checkbox('CHKSBI_DO',$rsTerminals->FeldInhalt('SBT_DO'),50,$Edit,1);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['Freitag']. ': ',70);
        $Form->Erstelle_Checkbox('CHKSBI_FR',$rsTerminals->FeldInhalt('SBT_FR'),50,$Edit,1);
        $Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['Samstag']. ': ',70);
        $Form->Erstelle_Checkbox('CHKSBI_SA',$rsTerminals->FeldInhalt('SBT_SA'),50,$Edit,1);
        $Form->ZeileEnde();

        $Form->Trennzeile('L');
	}
	
	
	
	if(($Recht37000&4)==4)
	{
		if(($Recht37000&8)==8)
		{
			$Edit = true;
		}
		else 
		{
			$Edit = false;
		}


        $Form->Trennzeile('O');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['SBI']['Entsorger'] . ':',100, 'font-weight:bolder');
		$Form->Erstelle_SelectFeld('SBI_Entsorger',$rsTerminals->FeldInhalt('SBT_SBN_KEY'),'',$Edit,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','');
		$Form->Erstelle_HiddenFeld('SBT_KEY',$rsTerminals->FeldInhalt('SBT_KEY'));
		$Form->ZeileEnde();
	}

	$Form->Formular_Ende();

}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}


?>