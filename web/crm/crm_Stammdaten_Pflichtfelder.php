<?php
global $con;
global $Recht3708;
global $AWISSprache;
global $awisRSZeilen;
global $awisRSInfoName;
global $CursorFeld;		// Zum Cursor setzen
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;

// Textkonserven laden
$TextKonserven = array();
$TextKonserven[]=array('CPF','%');
$TextKonserven[]=array('CAD','%');
$TextKonserven[]=array('Wort','lbl_weiter');
$TextKonserven[]=array('Wort','lbl_speichern');
$TextKonserven[]=array('Wort','lbl_trefferliste');
$TextKonserven[]=array('Wort','lbl_aendern');
$TextKonserven[]=array('Wort','lbl_hinzufuegen');
$TextKonserven[]=array('Wort','lbl_loeschen');
$TextKonserven[]=array('Wort','lbl_DSZurueck');
$TextKonserven[]=array('Wort','lbl_DSWeiter');
$TextKonserven[]=array('Wort','Seite');
$TextKonserven[]=array('Wort','txt_BitteWaehlen');

$AWISSprachKonserven = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
$Recht3708 = awisBenutzerRecht($con,3708,$AWISBenutzer->BenutzerName());
if($Recht3708==0)
{
	awisEreignis(3,1000,'CRM',$AWISBenutzer->BenutzerName(),'','','');
	echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
	echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
	die();
}

$Bedingung = '';

//********************************************************
// Parameter ?
//********************************************************

if(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
{
	include('./crm_loeschen.php');
}
elseif(isset($_POST['cmdSpeichern_x']))
{
	include('./crm_speichern_stammdaten.php');
}
elseif(isset($_GET['CPF_KEY']))
{
	$Bedingung .= ' AND CPF_KEY='.awis_FeldInhaltFormat('Z',$_GET['CPF_KEY']).'';
}
elseif(isset($_POST['txtCPF_KEY']))
{
	$Bedingung .= ' AND CPF_KEY='.awis_FeldInhaltFormat('Z',$_POST['txtCPF_KEY']).'';
}

if($AWIS_KEY2>0)
{
	$Bedingung .= ' AND CPF_KEY='.awis_FeldInhaltFormat('Z',$AWIS_KEY2).'';
}

//********************************************************
// Daten suchen
//********************************************************

$SQL = 'SELECT *';
$SQL .= ' FROM CRMPflichtfelder';

if($Bedingung!='')
{
	$SQL .= ' WHERE ' . substr($Bedingung,4);
}

if(!isset($_GET['Sort']))
{
	$SQL .= ' ORDER BY CPF_PFLICHTFELD';
}
else
{
	$SQL .= ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
}
$rsCPF = awisOpenRecordset($con, $SQL);
$rsCPFZeilen = $awisRSZeilen;
$rankingString = $AWISSprachKonserven['CAD']['lst_CAD_RANKING'];
$pflichtfeldString = $AWISSprachKonserven['CPF']['lst_CPF_PFLICHTFELDER'];

//********************************************************
// Daten anzeigen
//********************************************************
if($rsCPFZeilen==0 AND !isset($_GET['CPF_KEY']))		// Keine Meldung bei neuen Datensätzen!
{
	if(($Recht3708&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder&CPF_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}
	echo '<span class=HinweisText>Es wurden keine CRMPflichtfelder gefunden.</span>';
}
elseif($rsCPFZeilen>1 || isset($_GET['Liste']))						// Liste anzeigen
{
	awis_FORM_FormularStart();

	awis_FORM_ZeileStart();

	if(($Recht3708&6)>0)
	{
		$Icons[] = array('new','./crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder&CPF_KEY=0');
		awis_FORM_Erstelle_ListeIcons($Icons,38,-1);
	}
	
	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CPF_PFLICHTFELD'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CPF_PFLICHTFELD'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPF']['CPF_PFLICHTFELD'],250,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CPF_RANKING'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CPF_RANKING'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPF']['CPF_RANKING'],250,'',$Link);
	$Link = './crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'');
	$Link .= '&Sort=CPF_KON_KEY'.((isset($_GET['Sort']) AND ($_GET['Sort']=='CPF_KON_KEY'))?'~':'');
	awis_FORM_Erstelle_Liste_Ueberschrift($AWISSprachKonserven['CPF']['CPF_KON_KEY'],250,'',$Link);
	awis_FORM_ZeileEnde();
	
	for($CPFZeile=0;$CPFZeile<$rsCPFZeilen;$CPFZeile++)
	{
		awis_FORM_ZeileStart();

		$Icons = array();
		if(($Recht3708&6)>0)	// Ändernrecht
		{
			$Icons[] = array('edit','./crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder&CPF_KEY='.$rsCPF['CPF_KEY'][$CPFZeile]);
			$Icons[] = array('delete','./crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder&Del='.$rsCPF['CPF_KEY'][$CPFZeile]);
		}
		awis_FORM_Erstelle_ListeIcons($Icons,38,($CPFZeile%2));
		
		$posStart = strpos($pflichtfeldString, $rsCPF['CPF_PFLICHTFELD'][$CPFZeile].'~');
		$posEnde = strpos(substr($pflichtfeldString,$posStart + strlen($rsCPF['CPF_PFLICHTFELD'][$CPFZeile].'~')), '|');
		$posEnde === false ? $posEnde = 100 : $posEnde;
		$pflichtfeld = substr($AWISSprachKonserven['CPF']['lst_CPF_PFLICHTFELDER'], $posStart + strlen($rsCPF['CPF_PFLICHTFELD'][$CPFZeile].'~'), $posEnde);
		
		awis_FORM_Erstelle_ListenFeld('CPF_PFLICHTFELD',$pflichtfeld,0,250,false,($CPFZeile%2));
		
		if($rsCPF['CPF_RANKING'][$CPFZeile] > 0 && is_null($rsCPF['CPF_RANKING'][$CPFZeile]) === false)
		{
			$posStart = strpos($rankingString, $rsCPF['CPF_RANKING'][$CPFZeile].'~');
			$posEnde = strpos(substr($rankingString,$posStart + strlen($rsCPF['CPF_RANKING'][$CPFZeile].'~')), '|');
			$posEnde === false ? $posEnde = 100 : $posEnde;
			$ranking = substr($AWISSprachKonserven['CAD']['lst_CAD_RANKING'], $posStart + strlen($rsCPF['CPF_RANKING'][$CPFZeile].'~'), $posEnde);
		}
		else
		{
			$ranking = '';
		}
				
		awis_FORM_Erstelle_ListenFeld('CPF_RANKING',$ranking,0,250,false,($CPFZeile%2));
		
		if($rsCPF['CPF_KON_KEY'][$CPFZeile] > 0 && is_null($rsCPF['CPF_KON_KEY'][$CPFZeile]) === false)
		{
			$SQL = "SELECT kon_name1 || ', ' || kon_name2 as betreuer ";
			$SQL .= "FROM kontakte WHERE kon_key = " . $rsCPF['CPF_KON_KEY'][$CPFZeile];
			$rsKON = awisOpenRecordset($con, $SQL);
			$betreuer = $rsKON['BETREUER'][0];
		}
		else
		{
			$betreuer = '';
		}
		
		awis_FORM_Erstelle_ListenFeld('CPF_KON_KEY',$betreuer,0,250,false,($CPFZeile%2));
		awis_FORM_ZeileEnde();
	}
	
	awis_FORM_FormularEnde();
}
else 
{
	echo '<form name=frmCRMPflichtfelder action=./crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder method=POST  enctype="multipart/form-data">';
	
	$AWIS_KEY1 = (isset($rsCPF['CPF_KEY'][0])?$rsCPF['CPF_KEY'][0]:0);
	
	echo '<input type=hidden name=txtCPF_KEY value='.$AWIS_KEY1. '>';
	
	awis_FORM_FormularStart();
	$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
	
	// Infozeile zusammenbauen
	$Felder = array();
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./crm_Main.php?cmdAktion=Stammdaten&Seite=Pflichtfelder&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/NeueListe.png></a>");
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCPF['CPF_USER'][0]));
	$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>($AWIS_KEY1===0?'':$rsCPF['CPF_USERDAT'][0]));
	awis_FORM_InfoZeile($Felder,'');
	
	$TabellenKuerzel='CPF';
	$EditRecht=(($Recht3708&2)!=0);
	
	if($AWIS_KEY1==0)
	{
		$EditRecht=true;
	}
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CPF']['CPF_PFLICHTFELD'] . ':', 120);
	$StatusText = explode("|", $AWISSprachKonserven['CPF']['lst_CPF_PFLICHTFELDER']);
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_PFLICHTFELD', ($AWIS_KEY1 === 0 ? '' : $rsCPF['CPF_PFLICHTFELD'][0]), 200, $EditRecht, $con, '', $OptionBitteWaehlen, '', '', '', $StatusText, '');
	awis_FORM_ZeileEnde();
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_RANKING'] . ':', 120);
	$StatusText = explode("|", $AWISSprachKonserven['CAD']['lst_CAD_RANKING']);
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_RANKING', ($AWIS_KEY1 === 0 ? '' : $rsCPF['CPF_RANKING'][0]), 200, $EditRecht, $con, '', $OptionBitteWaehlen, '', '', '', $StatusText, '');
	awis_FORM_ZeileEnde();
	
	$SQL = 'SELECT DISTINCT *';
	$SQL .= " FROM (SELECT kon.kon_key, kon.kon_name1||', '|| COALESCE(kon.kon_name2,'') AS kontaktname";
	$SQL .= ' FROM kontakte kon INNER JOIN v_accountrechte b';
	$SQL .= ' ON kon_key = b.xbn_kon_key';
	if(!isset($rsCPF['CPF_KON_KEY'][0]))
	{
		$SQL .= " WHERE (b.xrc_id = 3704 AND BITAND (b.xba_stufe, 16) = 16 AND kon.kon_status='A'))";
	}
	else
	{
		$SQL .= " WHERE ((b.xrc_id = 3704 AND BITAND (b.xba_stufe, 16) = 16 AND kon.kon_status='A')";
		$SQL .= ' OR b.xbn_key = 0' . $rsCPF['CPF_KON_KEY'][0] . '))';
	}
	$SQL .= ' ORDER BY kontaktName';
	
	awis_FORM_ZeileStart();
	awis_FORM_Erstelle_TextLabel($AWISSprachKonserven['CAD']['CAD_KON_KEY'] . ':', 120);
	awis_FORM_Erstelle_SelectFeld($TabellenKuerzel . '_KON_KEY', ($AWIS_KEY1 === 0 ? '' : $rsCPF['CPF_KON_KEY'][0]), 300, $EditRecht, $con, $SQL, $OptionBitteWaehlen, '', '', '', '', '');
	awis_FORM_ZeileEnde();
	
	awis_FORM_FormularEnde();
	
	//***************************************
	// Schaltflächen für dieses Register
	//***************************************
	awis_FORM_SchaltflaechenStart();
	if(($Recht3708&(2+4+256))!==0)		//
	{
		awis_FORM_Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/diskette.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	awis_FORM_SchaltflaechenEnde();
	
	echo '</form>';
}