<?php
/**
 * Details zu den Personaleinsaetzen
 *
 * @author Sacha Kerres
 * @copyright ATU Auto Teile Unger
 * @version 200810090927
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $SpeichernOK;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('BES','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Liste','lst_MassErfuellt');
	$TextKonserven[]=array('Wort','PDFErzeugen');

	
	$AWISBenutzer = awisBenutzer::Init();

	$Form = new awisFormular();
	$DBSCHAD = awisDatenbank::NeueVerbindung('SCHAD');
	$DBSCHAD->Oeffnen();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$Recht40014 = $AWISBenutzer->HatDasRecht(40014);
	$EditRecht = ($Recht40014&2) == 2;
	$HinzuRecht = ($Recht40014&4) == 4;
	$Param = array();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SCHAD_SAA'));
	
	$Fehler = 0;
	$Save = 0;
	$FehlerFest = false;
	$NeuAnlage = 0;	
	
	$FelderNew = $Form->NameInArray($_POST, 'ico_add',1,1);
	$FelderDel = $Form->NameInArray($_POST, 'ico_delete',1,1);
	

	
    if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
    {
		$Param['ORDER'] = 'AUFTRAGSART_ATU';
 	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
	}

	if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
	{
		$Param['BLOCK'] = $_REQUEST['Block'];
	}
	
	if($Recht40014==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	
	if(isset($_GET['ART_ID']))
	{
		$AWIS_KEY1 = $_GET['ART_ID'];
	}
	else 
	{
	    $AWIS_KEY1='';
	}
	
	
	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '-1';
	}
	
	elseif(isset($_POST['cmdSuche_x']))
	{	
		$Param['BES_AUFTRAGSART'] = 	$Form->Format('T',$_POST['sucBES_AUFTRAGSART'],true);
		$Param['BES_VERALTET']= $Form->Format('N0',$_POST['sucBES_VERALTET'],true);
		$Param['BES_SICHTBAR_FILIALE'] = $Form->Format('N0',$_POST['sucBES_SICHTBAR_FILIALEN'],true);
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
		$Param['BLOCK']=1;
	}
		
	//Speichernbutton oder Klick aufs F�hnchen
	if(isset($_POST['cmdSpeichern_x']) or isset($_GET['button']))
	{
		include('./auftragsarten_speichern.php');
	}

	/* Daten beschaffen */
    $SQL  ='SELECT';
    $SQL .='   Daten.*,';
    $SQL .='   row_number() OVER (';
    $SQL .= ' order by ' . $Param['ORDER'] .' )  AS ZeilenNr';
    $SQL .=' FROM';
    $SQL .='   (';
    $SQL .='     SELECT';
    $SQL .='       ART_ID,';
    $SQL .='       AUFTRAGSART_ATU,';
    $SQL .='       VERALTET,';
    $SQL .='       NVL(SICHTBAR_FILIALEN,\'0\') AS SICHTBAR_FILIALEN,';
    $SQL .='       BES_USER,';
    $SQL .='       BES_USERDAT';
    $SQL .='     FROM';
    $SQL .='       auftragsarten';
    $SQL .='   )daten';
    	
	
	if (isset($_POST['cmdSuche_x']) or $AWIS_KEY1 == '' or $SpeichernOK)
	{
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if($Bedingung != '')
		//if ($Bedingung != '' and (!isset($_POST['cmdHinzufuegen_x'])) and (!isset($_POST['cmdDSNeu_x'])) and !isset($_GET['HRT_KEY']) and $NeuAnlage != 1 and !isset($_POST['txtHRT_KEY']))
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
	}

	if($AWIS_KEY1 != '' and !$SpeichernOK)
	{
		if($AWIS_KEY1 <> '')
		{
			$SQL .= ' WHERE ART_ID='.$AWIS_KEY1;
		}
		else 
		{
			$SQL .= ' AND ART_ID='.$AWIS_KEY1;
		}
		
	}
	$MaxDS = 1;
	$ZeilenProSeite=1;
    $Block = 1;
	// Zum Bl�ttern in den Daten
	if (isset($_REQUEST['Block']))
	{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
	}
	else
	{
		$Block = $Param['BLOCK'];
	}
	$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
	
	$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
	$MaxDS = $DBSCHAD->ErmittleZeilenAnzahl($SQL);
	
	//AWIS_KEY1 ist leer, wenn man �ber die Suche kommt. Wenn gespeichert wird(AWIS_KEY1 ist dann nicht leer), kommt man wieder in die Liste, deswegen auch die Blockparameter hernehmen.  
	if($AWIS_KEY1 == '' or (isset($_POST['cmdSpeichern_x']) and $SpeichernOK))
	{
		$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
	}
	$Form->DebugAusgabe(1, $SQL);
	$rsSBE = $DBSCHAD->RecordSetOeffnen($SQL);
	// Spaltenbreiten f�r Listenansicht
	$FeldBreiten = array();
	$FeldBreiten['BES_AUFTRAGSART'] = 500;
	
	$FeldBreiten['BES_VERALTET'] = 20;
	
	
	$count = 0;
	$Gesamtbreite = '';
	foreach ($FeldBreiten as $value)
	{
		$Gesamtbreite += $value;
		$count++;
	}
	if(stripos($_SERVER['HTTP_USER_AGENT'],'MSIE')===false)
	{
		$Gesamtbreite += ($count * 4);
	}
	
	$Gesamtbreite += 20;
	
	$Form->SchreibeHTMLCode('<form name="frmSKEDetails" action="./auftragsarten_Main.php?cmdAktion=Details'.(($AWIS_KEY1!='')?'&ART_ID=':'') .$AWIS_KEY1.' " method=POST  enctype="multipart/form-data">');
	
	$Form->Formular_Start();
	
	if (($rsSBE->AnzahlDatensaetze() > 1))
	{
		$Form->ZeileStart();
		// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
	    $Link = './auftragsarten_Main.php?cmdAktion=Details&Sort=AUFTRAGSART_ATU'.((isset($_GET['Sort']) AND ($_GET['Sort']=='AUFTRAGSART_ATU'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');

		
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['BES_AUFTRAGSART'], $FeldBreiten['BES_AUFTRAGSART'], '', $Link);
		
		
		if (($Recht40014&2) == 2)
		{
			$Link = './auftragsarten_Main.php?cmdAktion=Details&Sort=VERALTET'.((isset($_GET['Sort']) AND ($_GET['Sort']=='VERALTET'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBAKTIV'];
			$Form->Erstelle_Liste_Ueberschrift('A', $FeldBreiten['BES_VERALTET'], '',$Link,$TTT);
		
			$Link = './auftragsarten_Main.php?cmdAktion=Details&Sort=SICHTBAR_FILIALEN'.((isset($_GET['Sort']) AND ($_GET['Sort']=='SICHTBAR_FILIALEN'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$TTT = $AWISSprachKonserven['BES']['ttt_UEBFILIALEN'];
			$Form->Erstelle_Liste_Ueberschrift('S', $FeldBreiten['BES_VERALTET'], '',$Link,$TTT);
			
		}
	
		$Form->ZeileEnde();
		$DS = 0;	// f�r Hintergrundfarbumschaltung
		while(! $rsSBE->EOF())
		{
			$Form->ZeileStart();
			if ($EditRecht)
			{
			    //Link zum Datensatz
			    $Link = './auftragsarten_Main.php?cmdAktion=Details&ART_ID='.$rsSBE->FeldInhalt('ART_ID') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			}
			else 
			{
				$Link = '';
			}
			$TTT = $rsSBE->FeldInhalt('AUFTRAGSART_ATU');
			
			$Form->Erstelle_ListenFeld('BES_AUFTRAGSART', $rsSBE->FeldInhalt('AUFTRAGSART_ATU'), 0, $FeldBreiten['BES_AUFTRAGSART'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
			$Link = '';
				
			
			$LinkAktiv = '';
			if (($Recht40014&2) == 2)
			{
			    $IconsArray = '';
			    $LinkAktiv .=  './auftragsarten_Main.php?cmdAktion=Details&button='.$rsSBE->FeldInhalt('ART_ID').'&VERALTET='.$rsSBE->FeldInhalt('VERALTET');
				if ($rsSBE->FeldInhalt('VERALTET') == '1')
				{
					$IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Inaktiv']);
				}
				else
				{
					$IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Aktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['BES_VERALTET'] , ($DS%2));
				$IconsArray = '';
				
				$LinkAktiv =  './auftragsarten_Main.php?cmdAktion=Details&button='.$rsSBE->FeldInhalt('ART_ID').'&Sichtbar='.$rsSBE->FeldInhalt('SICHTBAR_FILIALEN');
				if ($rsSBE->FeldInhalt('SICHTBAR_FILIALEN') == '1')
				{
				    $IconsArray[] = array("flagge_gruen", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Aktiv']);
				}
				else
				{
				    $IconsArray[] = array("flagge_rot", $LinkAktiv, "", $AWISSprachKonserven['BES']['ttt_Inaktiv']);
				}
				$Form->Erstelle_ListeIcons($IconsArray,$FeldBreiten['BES_VERALTET'] , ($DS%2));
			
			}
			
			$Form->ZeileEnde();
			$DS++;
			$rsSBE->DSWeiter();
		}
		
		
		$DSGesvorAkt = ($Block-1)* $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
			
		$Form->ZeileStart();
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['BES']['SummeAnzeigeDS'].' / '.$AWISSprachKonserven['BES']['SummeGesamtDS'].': '.$Form->Format('N0',$rsSBE->AnzahlDatensaetze() == 0 ? $DSGesvorAkt : $DSGesvorAkt+1).' - '.$Form->Format('N0',$DSGesvorAkt + $DS).' / '.$Form->Format('N0',$MaxDS), $Gesamtbreite, 'font-weight:bolder;');
		$Link = './auftragsarten_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
		$Form->ZeileEnde();
		
		//$Link = './auftragsarten_Main.php?cmdAktion=Details';
		//$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	}
	elseif($rsSBE->AnzahlDatensaetze() == 1 or $AWIS_KEY1 == '-1')
	{
   
		// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./auftragsarten_Main.php?cmdAktion=Details accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBE->FeldInhalt('KENN_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsSBE->FeldInhalt('KENN_USERDAT'));
		$Form->InfoZeile($Felder,'');

        
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_AUFTRAGSART'] . ':',140);
		$AWISCursorPosition= 'txtBES_AUFTRAGSART';
		$Form->Erstelle_TextFeld('!BES_AUFTRAGSART', isset($_POST['txtBES_AUFTRAGSART'])?$_POST['txtBES_AUFTRAGSART']:$rsSBE->FeldInhalt('AUFTRAGSART_ATU'), 90, 90,$EditRecht or $NeuAnlage);
		$Form->ZeileEnde();
		
		if (isset($_POST['cmdSpeichern_x']))
		{
		  
		    if (isset($_POST['txtCHK_SICHTBAR_FILIALEN']))
		    {
		        $CHK_FIL = true;
		    }
		    else
		    {
		        $CHK_FIL = false;
		    }
		
		    if (isset($_POST['txtCHK_AKTIV']))
		    {
		        $CHK_AKT = true;
		    }
		    else
		    {		        
		        $CHK_AKT = false;
		    }
		      
		}
		elseif($AWIS_KEY1=='-1')
		{
		    $CHK_FIL = true;
		    $CHK_AKT = true;
		}
		else
		{
		    $CHK_FIL = $rsSBE->FeldInhalt('SICHTBAR_FILIALEN');
		    $CHK_AKT =$rsSBE->FeldInhalt('VERALTET')==1?false:true;
		}
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['BES_SICHTBAR_FILIALE'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_SICHTBAR_FILIALEN',$CHK_FIL,40,true,true);
		$Form->ZeileEnde();
	
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['BES']['SBA_AKTIVSUCH'] . ':',140);
		$Form->Erstelle_Checkbox('CHK_AKTIV',$CHK_AKT,40,true,true);
		$Form->ZeileEnde();	
	}
	else
	{
		$Form->ZeileStart();
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
		$Form->ZeileEnde();
	}
	
    $Form->Formular_Ende();
	
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href', 'cmd_zurueck', '/schaden_neu/index.php', '/bilder/cmd_zurueck.png', $AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
	
	if(($Recht40014&2)==2 and ($rsSBE->AnzahlDatensaetze() == 1 or $AWIS_KEY1 == '-1'))//Speichern wenn Recht und 1(Detailansicht) oder kein (Neuanlage) Datensatz gefunden wurden
	{
	    $Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	elseif(($Recht40014&4)==4)//Neuanlage wenn Recht und wenn kein Speichernbutton da ist
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}
	
	
	$Form->SchaltflaechenEnde();
	$Form->SchreibeHTMLCode('</form>');
	
	$AWISBenutzer->ParameterSchreiben('Formular_SCHAD_SAA',serialize($Param));
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung zusammenbauen
 *
 * @param string $Param
 * @return string
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);

	$Bedingung = '';

	if(isset($Param['BES_AUFTRAGSART']) AND $Param['BES_AUFTRAGSART']!='')
	{
		$Bedingung .= 'AND UPPER(AUFTRAGSART_ATU) ' . $DB->LikeOderIst($Param['BES_AUFTRAGSART'], awisDatenbank::AWIS_LIKE_UPPER);
	}
	if(isset($Param['BES_VERALTET']) AND $Param['BES_VERALTET']!='')
	{
	    //Umdrehen-.-
	    $Wert=($Param['BES_VERALTET']==1?0:1);
	    
		$Bedingung .= 'AND VERALTET ' . $DB->LikeOderIst($Wert) . ' ';
	}
	
	
	if($Param['BES_SICHTBAR_FILIALE'] == '0')
	{
	    $Bedingung .= 'AND (SICHTBAR_FILIALEN ' . $DB->LikeOderIst($Param['BES_SICHTBAR_FILIALE']) . ' or SICHTBAR_FILIALEN is null) ';
	}
	elseif($Param['BES_SICHTBAR_FILIALE'] == '1')
	{
	    $Bedingung .= 'AND SICHTBAR_FILIALEN ' . $DB->LikeOderIst($Param['BES_SICHTBAR_FILIALE']) . ' ';
	}
	
	

	
	return $Bedingung;
}

?>