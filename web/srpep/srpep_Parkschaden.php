<?php
/**
 * Parksch�denerfassung
 *
 * @author Tobias Sch�ffler
 * @copyright ATU Auto Teile Unger
 * @version 201507031016
 * @todo
 */
global $AWISCursorPosition;
global $AWISBenutzer;
global $AWIS_KEY1;
global $AWIS_KEY2;
global $Fehler;
global $rsPKS;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('PKS','%');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_suche');
	$TextKonserven[]=array('Wort','AuswahlSpeichern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','wrd_Filiale');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Liste','lst_OffenMass');
	$TextKonserven[]=array('Wort','PDFErzeugen');
	$TextKonserven[]=array('Wort','DatumVom');
	$TextKonserven[]=array('Wort','Datum');
	$TextKonserven[]=array('FIL','FIL_GEBIET');
	$TextKonserven[]=array('Liste','lst_ALLE_0');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	
	$AWIS_KEY1 = '';
	$AWISBenutzer = awisBenutzer::Init();
	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_SRP'));
	$Form = new awisFormular();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();	
	
	$FalscherDS = FALSE;
	$Seite = 'Suche';
	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	
	$script = "<script type='text/javascript'>
						
			function blendeEinStorno(obj) { 
				var checkWert = obj.options[obj.selectedIndex].value;
				
				if(checkWert == 3) // wenn Storno Stornogrund mit �berschrift einbleden
	    	    { 
	    	         document.getElementById('STORNOLEER').style.display = 'block';
					 document.getElementById('GRUNDSTORNO').style.display = 'block';	  
	    	         document.getElementById('UEBGRUNDSTORNO').style.display = 'block';	               
		        }
				else // ansosten Feld und �berschrift wieder ausblenden und Inhalt leeren
				{
					 document.getElementById('STORNOLEER').style.display = 'none';
					 document.getElementById('GRUNDSTORNO').style.display = 'none';	  
	    	         document.getElementById('UEBGRUNDSTORNO').style.display = 'none';	    
					 document.getElementsByName('txtPKS_GRUNDSTORNO')[0].value = ''          
				}
				
			}
				
				
			function blendeEinIststand(obj) {	           
	    	       var checkWert = obj.options[obj.selectedIndex].value;
	    	      
	    	       if(checkWert == 1) // Wenn ja
	    	       { 
	    	           document.getElementById('DATUMBES').style.display = 'block';	 // Besuchertermin einblenden
	    	           document.getElementById('GRUNDABL').style.display = 'none'; // evtl. vorher angezeigten Ablehnungsgrund ausblenden
					   document.getElementById('UEBGRUNDABL').style.display = 'none'; // �berschrift Ablehnung ausblenden
					   document.getElementById('UEBDATUMBES').style.display = 'none'; // Leerzeile Ablehnung ausblenden
					   document.getElementsByName('txtPKS_GRUNDABL')[0].value = ''  // Ablehnungsgrund leeren
					   document.getElementsByName('txtPKS_GRUNDABL')[0].style.backgroundColor = '#F78181';	// und Hintergrundfarbe wieder auf rot            
		           }
	    	       else if(checkWert == 0) //nein
	    	       {
	    	            document.getElementById('UEBGRUNDABL').style.display = 'block'; // �berschrift Ablehnung einblenden
						document.getElementById('GRUNDABL').style.display = 'block'; // Ablehnungsgrund einblenden
						document.getElementById('EMPTY').style.display = 'block'; // Leerzeile unter Ablehnungsgrund einblenden
						document.getElementById('UEBDATUMBES').style.display = 'block';	// Leerzeile zwischen Iststand und Ablehnungsgrund einblenden
						document.getElementById('DATUMBES').style.display = 'none';	// Feld Datumbesichtigung falls angezeigt ausblenden
						document.getElementById('txtPKS_DATUMBES').value = ''; // Besichtigungstermin leeren
						document.getElementById('txtPKS_DATUMBES').style.backgroundColor = '#F78181'; // und Hintergrundfarbe wieder auf rot
	    	       }
	    	       else //bitte w�hlen
	    	       {
	    	           	document.getElementById('UEBGRUNDABL').style.display = 'none'; // �berschrift Ablehnungsgrund ausblenden
						document.getElementById('GRUNDABL').style.display = 'none';	 // Ablehnungsgrund ausblenden
						document.getElementsByName('txtPKS_GRUNDABL')[0].style.backgroundColor = '#F78181'; // Hintergrund Ablehnungsgrund auf rot
						document.getElementById('DATUMBES').style.display = 'none'; // Besichtigungstermin ausblenden
						document.getElementById('txtPKS_DATUMBES').style.backgroundColor = '#F78181'; // Hintergrund Besichtigungstermin auf rot
						document.getElementById('UEBDATUMBES').style.display = 'none'; // �berschrift Besichtigungstermin ausblenden	  
						document.getElementById('txtPKS_DATUMBES').value = ''; // Besichtigungstermin leeren
						document.getElementsByName('txtPKS_GRUNDABL')[0].value = '' // Ablehnungsgrund leeren
	    	       }
	    	}   	
					
	      </script>";
	
		
	
		echo $script;
	
	$Recht11005 = $AWISBenutzer->HatDasRecht(11005);
	
	$Form->SchreibeHTMLCode('<form name="frmPKS" action="./srpep_Main.php?cmdAktion=Parkschaden" method="POST" enctype="multipart/form-data">');
	
	if(!isset($Param['ORDER']) or $Param['ORDER'] == '')
	{
		$Param['ORDER'] = 'pks_fil_id';
		$AWISBenutzer->ParameterSchreiben('Formular_SRP',serialize($Param));
	}
	
	if (isset($_GET['Sort']) and $_GET['Sort'] != '')
	{		// wenn GET-Sort, dann nach diesen Feld sortieren
		$Param['ORDER'] = str_replace('~',' DESC ', $_GET['Sort']);
		$AWISBenutzer->ParameterSchreiben('Formular_SRP',serialize($Param));
	}
	
	if(isset($_GET['PKS_KEY']))
	{
		$AWIS_KEY1 = $_GET['PKS_KEY'];
	}
	

	if(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1 = '0';
	}

	
	
	if(isset($_POST['cmdSpeichern_x']))
	{		
		include('./srpep_speichern.php');
	
		if($SpeicherFehler) //Wenn Speicherfehler, Detaildatensatz offen lassen. 
		{
			$AWIS_KEY1 = $_POST['txtPKS_KEY'];
		    $Seite = 'Details';
		}
		elseif($_POST['txtPKS_KEY'] == '0') //Wenn ein neuer angelegt wurde, dann in die Suche zur�ck. 
		{
		    $Seite = 'Suche';
		}
		else //Wenn einer Editiert wurde, dann in die Detailliste
		{
			$Seite = 'Details'; 
			unset($_POST);
		} 
		
	}
	
	if($Recht11005==0)
	{
		$Form->Fehler_KeineRechte();
	}
	
	if(isset($_POST['cmdSuche_x']) or isset($_REQUEST['Block']) or $AWIS_KEY1 != '' or isset($_REQUEST['Sort']) or $Seite == 'Details')
	{
		if(isset($_POST['cmdSuche_x']))
		{
		    $Param['PKS_ERFDATUM']=$Form->Format('T',$_POST['sucPKS_ERFDATUM'],true);
			$Param['PKS_FIL_ID']=$Form->Format('T',$_POST['sucPKS_FIL_ID_SUCH'],true);
			$Param['FIL_GEBIET']=$Form->Format('T',$_POST['sucPKS_GEBIET'],true);
			$Param['PKS_NACHNAME']=$Form->Format('T',$_POST['sucPKS_NACHNAME'],true);
			$Param['PKS_VERSSCHNR']=$Form->Format('T',$_POST['sucPKS_VERSSCHNR'],true);
			$Param['PKS_VERSNR']=$Form->Format('T',$_POST['sucPKS_VERSNR'],true);
			$Param['PKS_SCHADENART']=$Form->Format('T',$_POST['sucPKS_SCHADENART'],true);
			$Param['PKS_POSITION']=$Form->Format('T',$_POST['sucPKS_POSITION'],true);
			$Param['PKS_STATUS']=$Form->Format('T',$_POST['sucPKS_STATUS'],true);
			if(isset($_POST['sucPKS_ENTSCHEIDUNG']))
			{
				$Param['PKS_ENTSCHEIDUNG']=$Form->Format('T',$_POST['sucPKS_ENTSCHEIDUNG'],true);
			}
			$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';
			$Param['BLOCK']=1;
			$AWISBenutzer->ParameterSchreiben('Formular_SRP',serialize($Param));
		}
		if(isset($_GET['Mail']) or isset($_POST['txtMail']))
		{
			$Param['PKS_ERFDATUM']='';
			$Param['PKS_FIL_ID']='';
			$Param['FIL_GEBIET']='';
			$Param['PKS_NACHNAME']='';
			$Param['PKS_VERSSCHNR']='';
			$Param['PKS_VERSNR']='::bitte w�hlen::';
			$Param['PKS_SCHADENART']='::bitte w�hlen::';
			$Param['PKS_POSITION']='::bitte w�hlen::';
			$Param['PKS_STATUS']='::bitte w�hlen::';
			$Form->Erstelle_HiddenFeld('Mail',true);
		}
		
		
		if(isset($_REQUEST['Block']) and $_REQUEST['Block'] != '')
		{
			$Param['BLOCK'] = $_REQUEST['Block'];
			$AWISBenutzer->ParameterSchreiben('Formular_SRP',serialize($Param));
		}
		
		
		
		/** Test-SQL Eskalationen **/	

		$Konserve = '';
		if(($Recht11005&2)==2)
		{
			$Konserve = 'PKS_LST_STATUS_SVT_LIST';
		}
		elseif(($Recht11005&4)==4)
		{
			$Konserve = 'PKS_LST_STATUS_LL';
		}
		else 
		{
			$Konserve = 'PKS_LST_STATUS_ADMIN';
		}
		
		$Form->Formular_Start();
		
		
		$SQL  ='Select PKS_KEY,PKS_USER,PKS_USERDAT,PKS_FIL_ID,PKS_ERFDATUM,PKS_NACHNAME,PKS_VORNAME,PKS_VERSSCHEINNR,PKS_SCHADENART,PKS_VERSNR,PKS_SCHADENPOS,PKS_GROESSE,PKS_FARBE,PKS_STRASSE,PKS_ORT,PKS_PLZ,PKS_TELEFON,PKS_MOBIL,HERST,MOD,TYP,PKS_KHERNR,PKS_KMODNR,PKS_KTYPNR,PKS_KFZKENN,func_textkonserve(\''.$Konserve.'\',\'PKS\',\'DE\',PKS_STATUS) as PKS_STATUS_WORT,PKS_STATUS,PKS_NOTIZ,PKS_DATUMRR,PKS_STAND,PKS_DATUMBES,PKS_GRUNDABL,PKS_GRUNDSTORNO ';
		if(!isset($_GET['Mail']) and !isset($_POST['txtMail']))
		{
			$SQL .=',row_number() OVER (';
			$SQL .= ' order by ' . $Param['ORDER'];
			$SQL .= ') AS ZeilenNr ';
		}
		$SQL .=	' from( ' ;
		$SQL .= 'Select PKS_KEY,PKS_USER,PKS_USERDAT,PKS_FIL_ID,PKS_ERFDATUM,PKS_NACHNAME,PKS_VORNAME,PKS_VERSSCHEINNR,PKS_SCHADENART,PKS_VERSNR,PKS_SCHADENPOS,PKS_GROESSE,PKS_FARBE,PKS_STRASSE,PKS_ORT,PKS_PLZ,PKS_TELEFON,PKS_MOBIL,a.BEZ as HERST,b.BEZ as MOD,c.BEZ as TYP,PKS_KHERNR,PKS_KMODNR,PKS_KTYPNR,PKS_KFZKENN,PKS_STATUS,PKS_NOTIZ,PKS_DATUMRR,PKS_STAND,PKS_DATUMBES,PKS_GRUNDABL,PKS_GRUNDSTORNO from Parkschaeden';
		$SQL .= ' left join kfz_herst a on PKS_KHERNR = KHERNR ';
		$SQL .= ' left join kfz_modell b on PKS_KMODNR = KMODNR ';
		$SQL .= ' left join kfz_typ c on PKS_KTYPNR = KTYPNR ';
		$SQL .= ')';
		
		
		
		$Bedingung = '';
		$Bedingung .= _BedingungErstellen($Param);       // mit dem Rest
		if (isset($_REQUEST['Block']))
		{
			$Block = $Form->Format('N0', $_REQUEST['Block'], false);
		
		}
		else
		{
			$Block = $Param['BLOCK'];
		}
		
		
		if ($Bedingung != '')
		{
			$SQL .= ' WHERE ' . substr($Bedingung, 4);
		}
		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');
		$StartZeile = (($Block - 1) * $ZeilenProSeite) + 1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		if(!isset($_GET['Mail']) and !isset($_POST['txtMail']))
		{
			$SQL = "SELECT * FROM ($SQL) DATEN WHERE ZeilenNr >= $StartZeile AND ZeilenNr < " . ($StartZeile + $ZeilenProSeite);
		}
		if($AWIS_KEY1 != '')
		{
			if((($Recht11005&8)==8 or ($Recht11005&2)==2) and (isset($_GET['Mail']) or isset($_POST['txtMail'])))
			{
				$SQL .= ' WHERE pks_key='.$AWIS_KEY1;
			}
			else 
			{
				$SQL .= ' AND pks_key='.$AWIS_KEY1;
			}
			
		}
		
		if(($Recht11005&4)==4)
		{
			$SQL .= ' AND pks_status > 0 and pks_status < 3';
		}
	
		$rsPKS = $DB->RecordSetOeffnen($SQL);
		
		if($AWIS_KEY1 == '' and $rsPKS->AnzahlDatensaetze() == 1)
		{
			$AWIS_KEY1 = $rsPKS->FeldInhalt('PKS_KEY');
		}
		
		$FeldBreiten = array();
		$FeldBreiten['Datum'] = 100;
		$FeldBreiten['FIL_ID'] = 60;
		$FeldBreiten['NNAME'] = 150;
		$FeldBreiten['VORNAME'] = 150;
		$FeldBreiten['VERSSCHNR'] = 140;
		$FeldBreiten['STATUS'] = 170;
	   
		
		
		
		if (($rsPKS->AnzahlDatensaetze() > 1) or ($rsPKS->AnzahlDatensaetze() == 1 and (isset($_REQUEST['Block']) or isset($_REQUEST['Sort']))))
		{
			// Infozeile zusammenbauen
			$Felder = array();
			//$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
			$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href="./srpep_Main.php?cmdAktion=Parkschaden" accesskey=S title=' . $AWISSprachKonserven['PKS']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
			$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['PKS']['ZurueckSuche']);
			$Form->InfoZeile($Felder, '');
		
			$Form->ZeileStart();
			// �berschrift der Listenansicht mit Sortierungslink: Platzhalter f�r Checkboxen
			//$Form->Erstelle_Liste_Ueberschrift('', $FeldBreiten['ChkBox']-4);
			// �berschrift der Listenansicht mit Sortierungslink: Filiale
			$Link = './srpep_Main.php?cmdAktion=Parkschaden&Sort=PKS_ERFDATUM'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PKS_ERFDATUM'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['DatumVom'], $FeldBreiten['Datum'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Kfz-Kennzeichen
			$Link = './srpep_Main.php?cmdAktion=Parkschaden&Sort=PKS_FIL_ID'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PKS_FIL_ID'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['Wort']['wrd_Filiale'], $FeldBreiten['FIL_ID'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Betrag/Preis
			$Link = './srpep_Main.php?cmdAktion=Parkschaden&Sort=PKS_NACHNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PKS_NACHNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PKS']['PKS_NACHNAME'], $FeldBreiten['NNAME'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './srpep_Main.php?cmdAktion=Parkschaden&Sort=PKS_VORNAME'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PKS_VORNAME'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PKS']['PKS_VORNAME'], $FeldBreiten['VORNAME'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './srpep_Main.php?cmdAktion=Parkschaden&Sort=PKS_VERSSCHEINNR'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PKS_VERSSCHEINNR'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PKS']['PKS_VERSSCHNR'], $FeldBreiten['VERSSCHNR'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			$Link = './srpep_Main.php?cmdAktion=Parkschaden&Sort=PKS_STATUS'.((isset($_GET['Sort']) AND ($_GET['Sort']=='PKS_STATUS'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['PKS']['PKS_STATUS'], $FeldBreiten['STATUS'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=QMS_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='QMS_BEZEICHNUNG'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_Status'], $FeldBreiten['Status'], '', $Link);
			// �berschrift der Listenansicht mit Sortierungslink: Nachname
			//$Link = './qualitaetsmaengel_Main.php?cmdAktion=Details&Sort=ANZ'.((isset($_GET['Sort']) AND ($_GET['Sort']=='ANZ'))?'~':'') . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'');
			//$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['QMM']['QMM_ANZ'], $FeldBreiten['Anz'], '', $Link);
			$Form->ZeileEnde();
			$DS = 0;	// f�r Hintergrundfarbumschaltung
			while(! $rsPKS->EOF())
			{
				$Form->ZeileStart();
				$Link = './srpep_Main.php?cmdAktion=Parkschaden&PKS_KEY='.$rsPKS->FeldInhalt('PKS_KEY');
				$TTT = $rsPKS->FeldInhalt('PKS_ERFDATUM');
				$Form->Erstelle_ListenFeld('PKS_ERFDATUM', $Form->Format('D',$rsPKS->FeldInhalt('PKS_ERFDATUM')), 0, $FeldBreiten['Datum'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT =  $rsPKS->FeldInhalt('PKS_FIL_ID');
				$Form->Erstelle_ListenFeld('PKS_FIL_ID',$rsPKS->FeldInhalt('PKS_FIL_ID'), 0, $FeldBreiten['FIL_ID'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT = $rsPKS->FeldInhalt('PKS_NACHNAME');
				$Form->Erstelle_ListenFeld('PKS_NACHNAME', $rsPKS->FeldInhalt('PKS_NACHNAME'), 0, $FeldBreiten['NNAME'], false, ($DS%2),'',$Link, 'T', 'L', $TTT);
				$TTT = $rsPKS->FeldInhalt('PKS_VORNAME');
				$Form->Erstelle_ListenFeld('PKS_VORNAME',$rsPKS->FeldInhalt('PKS_VORNAME'), 0, $FeldBreiten['VORNAME'], false, ($DS%2), '',$Link, 'T', 'L', $TTT);
				$TTT = $rsPKS->FeldInhalt('PKS_VERSSCHEINNR');
				$Form->Erstelle_ListenFeld('PKS_VERSCHNR',$rsPKS->FeldInhalt('PKS_VERSSCHEINNR'), 0, $FeldBreiten['VERSSCHNR'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$TTT = $rsPKS->FeldInhalt('PKS_STATUS_WORT');
				$Form->Erstelle_ListenFeld('PKS_STATUS',$rsPKS->FeldInhalt('PKS_STATUS_WORT'), 0, $FeldBreiten['STATUS'], false, ($DS%2), '','', 'T', 'L', $TTT);
				$Form->ZeileEnde();
				$DS++;
				$rsPKS->DSWeiter();
			}
			$Link = './srpep_Main.php?cmdAktion=Parkschaden';
			$Form->BlaetternZeile($MaxDS, $ZeilenProSeite, $Link, $Block, '');
	
		}
		else if ($AWIS_KEY1 != '' or $rsPKS->AnzahlDatensaetze() == 1)
		{	
			if ($AWIS_KEY1 == '0')
			{
				// Infozeile zusammenbauen
				$Felder = array();
				//$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
				$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href="./srpep_Main.php?cmdAktion=Parkschaden" accesskey="S" title=' . $AWISSprachKonserven['PKS']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
				$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['PKS']['ZurueckSuche']);
				$Form->InfoZeile($Felder, '');
			}
			else if($AWIS_KEY1 != '' and $rsPKS->AnzahlDatensaetze() == 1)
			{
				$SQL = 'select * from parkschaeden';
				if ($Bedingung != '')
				{
					$SQL .= ' WHERE ' . substr($Bedingung, 4);
				}
				$rsPKSCheck = $DB->RecordSetOeffnen($SQL);
				
				if($rsPKSCheck->AnzahlDatensaetze() > 1)
				{
					// Infozeile zusammenbauen
					$Felder = array();
					$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./srpep_Main.php?cmdAktion=Parkschaden&Block=".$Param['BLOCK']. " accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
					$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPKS->FeldInhalt('PKS_USER'));
					$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPKS->FeldInhalt('PKS_USERDAT'));
					$Form->InfoZeile($Felder,'');		
				}
				else
				{
					// Infozeile zusammenbauen
					$Felder = array();
					//$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
					$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href="./srpep_Main.php?cmdAktion=Parkschaden" accesskey=S title=' . $AWISSprachKonserven['PKS']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
					$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['PKS']['ZurueckSuche']);
					$Form->InfoZeile($Felder, '');
				}
			}
			else if($AWIS_KEY1 == '' and $rsPKS->AnzahlDatensaetze() == 1)
			{
				// Infozeile zusammenbauen
				$Felder = array();
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=\"./srpep_Main.php?cmdAktion=Parkschaden\" accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPKS->FeldInhalt('PKS_USER'));
				$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsPKS->FeldInhalt('PKS_USERDAT'));
				$Form->InfoZeile($Felder,'');
			}
			else 
			{
				// Infozeile zusammenbauen
				$Felder = array();
				//$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
				$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href="./srpep_Main.php?cmdAktion=Parkschaden" accesskey=S title=' . $AWISSprachKonserven['PKS']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
				$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['PKS']['ZurueckSuche']);
				$Form->InfoZeile($Felder, '');
				
				$Form->ZeileStart();
				$Form->Hinweistext($AWISSprachKonserven['PKS']['MSG_FALSCHERDS']);
				$Form->ZeileEnde();
				
				$FalscherDS = TRUE;
				

			}
			
			$Form->Erstelle_HiddenFeld('PKS_KEY', $AWIS_KEY1);
			
			if(!$FalscherDS)
			{
				$aendern = true;
				if($rsPKS->FeldInhalt('PKS_STATUS') > 0)
				{
					$aendern = false;
				}
				else
				{
					$aendern = true;
				}
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['UEBERSCHRIFT_SCHADERF'], 800, 'font-weight:bolder;font-size: 21px');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				// Schadenserfassung
				
				/**
				 *  Schadensart
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_SCHADENART'] . ':',200);
				$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_SCHADENART']);
				$Form->Erstelle_SelectFeld('PKS_SCHADENART',isset($_POST['txtPKS_SCHADENART'])?$_POST['txtPKS_SCHADENART']:$rsPKS->FeldInhalt('PKS_SCHADENART'),150,$aendern,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				$Form->ZeileEnde();
				
				/**
				 *  Schadensposition
				*/
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_POS'] . ':',200);
				$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_POS']);
				$Form->Erstelle_SelectFeld('PKS_SCHADENPOS',isset($_POST['txtPKS_SCHADENPOS'])?$_POST['txtPKS_SCHADENPOS']:$rsPKS->FeldInhalt('PKS_SCHADENPOS'),150,$aendern,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				$Form->ZeileEnde();
				
				/**
				 *  Schadensgr�sse
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_GROESSE'] . ':',200);
				$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_GROESSE']);
				$Form->Erstelle_SelectFeld('PKS_GROESSE',isset($_POST['txtPKS_GROESSE'])?$_POST['txtPKS_GROESSE']:$rsPKS->FeldInhalt('PKS_GROESSE'),150,$aendern,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'','',array(),SetzeFarbschema('PKS_GROESSE','P'));
				$Form->ZeileEnde();
				
				/**
				 * Farbton
				 */
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_LACK'] . ':',200);
				$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_LACK']);
				$Form->Erstelle_SelectFeld('PKS_FARBE',isset($_POST['txtPKS_FARBE'])?$_POST['txtPKS_FARBE']:$rsPKS->FeldInhalt('PKS_FARBE'),150,$aendern,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_NOTIZ'] . ':',200);
				$Form->ZeileEnde();
				$Form->ZeileStart();
				$Form->Erstelle_Textarea('PKS_NOTIZ',isset($_POST['txtPKS_NOTIZ'])?$_POST['txtPKS_NOTIZ']:$rsPKS->FeldInhalt('PKS_NOTIZ'),500,80,3,$aendern,'font: bolder');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
					
				
				$Form->ZeileStart();
				$Form->Trennzeile('T');
				$Form->ZeileEnde();
			
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				// Eingabe Kundendaten
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['UEBERSCHRIFT_KUNDENDATEN'], 800, 'font-weight:bolder;;font-size: 21px');
				$Form->ZeileEnde();
					
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				$colorRed='#F78181';
				$colorOrange='#F7BE81';
				
				/**
				 * Erfassungsdatum
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['Datum'].':',200);
				$Form->Erstelle_TextFeld('PKS_ERFDATUM',(isset($_POST['txtPKS_ERFDATUM']))? $_POST['txtPKS_ERFDATUM']: ($rsPKS->FeldInhalt('PKS_ERFDATUM')=='')?date('d.m.Y',time()):$rsPKS->FeldInhalt('PKS_ERFDATUM'),7,150,$aendern,'','','','D','L','','',10);
				$Form->ZeileEnde();
				
				
				/**
				 * Kundennachname
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_NACHNAME'].':',200);
				$Form->Erstelle_TextFeld('PKS_NACHNAME',isset($_POST['txtPKS_NACHNAME'])?$_POST['txtPKS_NACHNAME']:$rsPKS->FeldInhalt('PKS_NACHNAME'), 25,200, $aendern,'',SetzeFarbschema('PKS_NACHNAME','P'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
				
				/**
				 * Kundenvorname
				 */
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_VORNAME'].':',200);
				$Form->Erstelle_TextFeld('PKS_VORNAME',isset($_POST['txtPKS_VORNAME'])?$_POST['txtPKS_VORNAME']:$rsPKS->FeldInhalt('PKS_VORNAME'), 25,50, $aendern, '',SetzeFarbschema('PKS_VORNAME','P'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['UEBERSCHRIFT_ANSCHRIFT']. ' :', 800, 'font-weight:bolder');
				$Form->ZeileEnde();
				
	
				/**
				 * Strasse/Hausnummer:
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_STRASSE'].':',200);
				$Form->Erstelle_TextFeld('PKS_STRASSE',isset($_POST['txtPKS_STRASSE'])?$_POST['txtPKS_STRASSE']:$rsPKS->FeldInhalt('PKS_STRASSE'), 25,200, $aendern, '',SetzeFarbschema('PKS_STRASSE','P'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
				
				/**
				* Postleitzahl:
				*/
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_PLZ'].':',200);
				$Form->Erstelle_TextFeld('PKS_PLZ',isset($_POST['txtPKS_PLZ'])?$_POST['txtPKS_PLZ']:$rsPKS->FeldInhalt('PKS_PLZ'), 25,50, $aendern, '',SetzeFarbschema('PKS_PLZ','P'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
	
				/**
				 * Ort:
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_ORT'].':',200);
				$Form->Erstelle_TextFeld('PKS_ORT',isset($_POST['txtPKS_ORT'])?$_POST['txtPKS_ORT']:$rsPKS->FeldInhalt('PKS_ORT'), 25,50, $aendern, '',SetzeFarbschema('PKS_ORT','P'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
				
	
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['UEBERSCHRIFT_TELEFON']. ' :', 800, 'font-weight:bolder');
				$Form->ZeileEnde();
					
				
				/**
				 * Festnetz:
				*/
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_TELEFON'].':',200);
				$Form->Erstelle_TextFeld('PKS_TELEFON',isset($_POST['txtPKS_TELEFON'])?$_POST['txtPKS_TELEFON']:$rsPKS->FeldInhalt('PKS_TELEFON'), 25,200, $aendern, '',SetzeFarbschema('PKS_TELEFON;PKS_MOBIL','M'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['oder'],200);
				$Form->ZeileEnde();
				
				/**
				 * Handy:
				*/
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_MOBIL'].':',200);
				$Form->Erstelle_TextFeld('PKS_MOBIL',isset($_POST['txtPKS_MOBIL'])?$_POST['txtPKS_MOBIL']:$rsPKS->FeldInhalt('PKS_MOBIL'), 25,200, $aendern, '',SetzeFarbschema('PKS_TELEFON;PKS_MOBIL','M'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
					
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['UEBERSCHRIFT_FAHRZEUG']. ' :', 800, 'font-weight:bolder');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				/*
				$box = '';
				$box = 'box_Hersteller';
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_HERSTELLER'] . ':', 200);
				$Form->Erstelle_TextFeld('PKS_HERSTELLER',$rsPKS->FeldInhalt('HERST'), 25,178, $aendern,'', '', '', 'T', 'L','','',50,'onkeyup="ladeDiv(this,\''.$box.'\');" onfocus="ladeDiv(this,\''.$box.'\');" onblur="closeDiv(\''.$box.'\');"');
				echo '<img id=ajax style=display:none border=0 src=/bilder/ajax-loader.gif>';
				$Form->AuswahlBoxHuelle($box,'AuswahlListe','display:none; position:absolute; top:695px; left:166px;z-index:1;');
				$Form->ZeileEnde();
				
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart('font-size:8pt');
					$Form->Erstelle_TextLabel('&nbsp',200);
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['ttt_Herstellersuche'],600);
					$Form->ZeileEnde();
				
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
				}
			
				
				$Form->Erstelle_HiddenFeld('PKS_KHERNR',$rsPKS->FeldInhalt('PKS_KHERNR'));
				
				$box = 'box_Modell';
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_MODELL'] . ':', 200);
				$Form->Erstelle_TextFeld('PKS_MODELL',$rsPKS->FeldInhalt('MOD'), 25,200, $aendern,'', '', '', 'T', 'L','','',50,'onkeyup="ladeDiv(this,\''.$box.'\');" onfocus="ladeDiv(this,\''.$box.'\');" onblur="closeDiv(\''.$box.'\');"');
				$Form->AuswahlBoxHuelle($box,'AuswahlListe','display:none; position:absolute; top:750px; left:166px;z-index:1;');
				$Form->ZeileEnde();
					
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart('font-size:8pt');
					$Form->Erstelle_TextLabel('&nbsp',200);
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['ttt_Modellsuche'],600);
					$Form->ZeileEnde();
				
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
				}	
					
				$Form->Erstelle_HiddenFeld('PKS_KMODNR','');
				
				$box = 'box_Typ';
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_TYP'] . ':', 200);
				$Form->Erstelle_TextFeld('PKS_TYP',$rsPKS->FeldInhalt('TYP'), 25,200, $aendern,'', '', '', 'T', 'L','','',50,'onkeyup="ladeDiv(this,\''.$box.'\');" onfocus="ladeDiv(this,\''.$box.'\');" onblur="closeDiv(\''.$box.'\');"');
				$Form->AuswahlBoxHuelle($box,'AuswahlListe','display:none; position:absolute; top:810px; left:166px;z-index:1;');
				$Form->ZeileEnde();
				
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart('font-size:8pt');
					$Form->Erstelle_TextLabel('&nbsp',200);
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['ttt_Typsuche'],600);
					$Form->ZeileEnde();
				
				
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
				}
				
				$Form->Erstelle_HiddenFeld('PKS_KTYPNR','');
				
				*/
				
				/**
				 * Hersteller:
				 */
				
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_HERSTELLERSUCHE'] . ':', 200);
				
					$Form->Erstelle_TextFeld('*PKS_HERSTELLER','',25, 300, $aendern, '','background-color:#22FF22;','', 'T', 'L');
					$Form->ZeileEnde();
					$Form->ZeileStart('font-size:8pt');
					$Form->Erstelle_TextLabel('&nbsp',200);
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['ttt_Herstellersuche'],600);
					$Form->ZeileEnde();
				}
					
			
				$filter='*F*PKS_HERSTELLER;sucPKS_HERSTELLER;PKS_KMODNR=*txtPKS_KMODNR&PKS_KTYPNR=*txtPKS_KTYPNR&ZUSATZ=B~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'].';;';
	
				
				if (($rsPKS->FeldInhalt('PKS_KHERNR') != '') or (isset($_POST['txtPKS_KHERNR']) and $_POST['txtPKS_KHERNR'] != ''))       // nur wenn 1 T�V-Datensatz gew�hlt wurde, k�nnen folgende drei Abgleicharten gew�hlt werden
				{
					$SQL = 'Select * from kfz_herst ';
					if(isset($_POST['txtPKS_KHERNR']))
					{
						$SQL .=	'where KHERNR ='. $_POST['txtPKS_KHERNR'];
					}
					else if($rsPKS->FeldInhalt('PKS_KHERNR') != '')
					{
						$SQL .=	'where KHERNR ='. $rsPKS->FeldInhalt('PKS_KHERNR');
					}
					
					$rsDefault = $DB->RecordSetOeffnen($SQL);
					$DefaultWert = $rsDefault->FeldInhalt('KHERNR').'~'.$rsDefault->FeldInhalt('BEZ');
				}
				else
				{
					$DefaultWert = 'B~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
				}
				$Form->ZeileStart();
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_HERSTELLER'] . ':', 180);
					$Form->Erstelle_SelectFeld('PKS_KHERNR','', 700, $aendern,$filter,$DefaultWert,'', '','','','','',array(),'');
				}
				else
				{
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_HERSTELLER'] . ':', 200);
					$Form->Erstelle_TextFeld('PKS_KHERNR',$rsPKS->FeldInhalt('HERST'),25, 300, $aendern, '','','', 'T', 'L');
				}
				$Form->ZeileEnde();
				
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
				}
	
				/**
				 * Modell:
				 */
					
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_MODELLSUCHE'] . ':', 200);
						
					$Form->Erstelle_TextFeld('*PKS_MODELL','',25, 300, $aendern, '','background-color:#22FF22;','', 'T', 'L');
					$Form->ZeileEnde();
					$Form->ZeileStart('font-size:8pt');
					$Form->Erstelle_TextLabel('&nbsp',200);
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['ttt_Modellsuche'],600);
					$Form->ZeileEnde();
				}
				
				
				$filter='*F*PKS_MODELL;sucPKS_MODELL;PKS_KHERNR=*txtPKS_KHERNR&PKS_KTYPNR=*txtPKS_KTYPNR&ZUSATZ=B~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'].';;';
				
				if (($rsPKS->FeldInhalt('PKS_KMODNR') != '') or (isset($_POST['txtPKS_KMODNR']) and $_POST['txtPKS_KMODNR'] != ''))       // nur wenn 1 T�V-Datensatz gew�hlt wurde, k�nnen folgende drei Abgleicharten gew�hlt werden
				{
					$SQL = 'Select * from kfz_modell ';
					if(isset($_POST['txtPKS_KMODNR']))
					{
						$SQL .=	'where KMODNR ='. $_POST['txtPKS_KMODNR'];
					}
					else if($rsPKS->FeldInhalt('PKS_KMODNR') != '')
					{
						$SQL .=	'where KMODNR ='. $rsPKS->FeldInhalt('PKS_KMODNR');
					}
					
					$rsDefault = $DB->RecordSetOeffnen($SQL);
					
					$DefaultWert = $rsDefault->FeldInhalt('KMODNR').'~'.$rsDefault->FeldInhalt('BEZ');
				}
				else
				{
					$DefaultWert = 'B~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
				}
				$Form->ZeileStart();
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_MODELL'] . ':', 180);
					$Form->Erstelle_SelectFeld('PKS_KMODNR','', 700, $aendern,$filter,$DefaultWert,'', '','','','','',array(),'');
				}
				else
				{
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_MODELL'] . ':', 200);
					$Form->Erstelle_TextFeld('PKS_KMODNR',$rsPKS->FeldInhalt('MOD'),25, 300, $aendern, '','','', 'T', 'L');
				}
				$Form->ZeileEnde();
				
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
				}
				
				
				/**
				 * TYP:
				 */
				
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_TYPSUCHE'] . ':', 200);
					
					$Form->Erstelle_TextFeld('*PKS_TYPNR','',25, 300, $aendern, '','background-color:#22FF22;','', 'T', 'L');
					$Form->ZeileEnde();
					$Form->ZeileStart('font-size:8pt');
					$Form->Erstelle_TextLabel('&nbsp',200);
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['ttt_Typsuche'],600);
					$Form->ZeileEnde();
				}
					
					
				$filter='*F*PKS_TYP;sucPKS_TYPNR;PKS_KMODNR=*txtPKS_KMODNR&PKS_KHERNR=*txtPKS_KHERNR&ZUSATZ=B~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'].';;';
	
				if (($rsPKS->FeldInhalt('PKS_KTYPNR') != '') or (isset($_POST['txtPKS_KTYPNR']) and $_POST['txtPKS_KTYPNR'] != ''))       // nur wenn 1 T�V-Datensatz gew�hlt wurde, k�nnen folgende drei Abgleicharten gew�hlt werden
				{
					$SQL = 'Select * from kfz_typ ';
					if(isset($_POST['txtPKS_KTYPNR']))
					{
						$SQL .=	'where KTYPNR ='. $_POST['txtPKS_KTYPNR'];
					}
					else if($rsPKS->FeldInhalt('PKS_KTYPNR') != '')
					{
						$SQL .=	'where KTYPNR ='. $rsPKS->FeldInhalt('PKS_KTYPNR');
					}
				
					$rsDefault = $DB->RecordSetOeffnen($SQL);
				
					$DefaultWert = $rsDefault->FeldInhalt('KTYPNR').'~'.$rsDefault->FeldInhalt('BEZ');
				}
				else
				{
					$DefaultWert = 'B~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];
				}
				$Form->ZeileStart();
				if($rsPKS->FeldInhalt('PKS_STATUS') == 0)
				{
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_TYP'] . ':', 180);
					$Form->Erstelle_SelectFeld('PKS_KTYPNR','', 700, $aendern,$filter,$DefaultWert,'', '','','','','',array(),'');
				}
				else
				{
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_TYP'] . ':', 200);
					$Form->Erstelle_TextFeld('PKS_KTYPNR',$rsPKS->FeldInhalt('TYP'),15, 300, $aendern, '','','', 'T', 'L');
				}
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				/**
				 * KFZ:
				 */
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_KFZKENN'].':',200);
				$Form->Erstelle_TextFeld('PKS_KFZKENN',isset($_POST['txtPKS_KFZKENN'])?$_POST['txtPKS_KFZKENN']:$rsPKS->FeldInhalt('PKS_KFZKENN'), 25,200, $aendern, '', '', '', 'T', 'L','','',50);
				$Form->ZeileEnde();
				
				/**
				 * Versicherung
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_VERSNR'].':',200);
					
				$SQL = 'SELECT VVE_VERSNR,VVE_BEZEICHNUNG'; // Alle Aktiven Versicherungen f�r Parksch�denerfassung holen
				$SQL .= ' FROM VERSVERSICHERUNGEN';
				$SQL .= ' WHERE VVE_PKS_VERSAKTIV = 1';
				$SQL .= ' ORDER BY VVE_BEZEICHNUNG';
			
				$Form->Erstelle_SelectFeld('PKS_VERSNR',isset($_POST['txtPKS_VERSNR'])?$_POST['txtPKS_VERSNR']:$rsPKS->FeldInhalt('PKS_VERSNR'),400,$aendern,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','','','','',array(),SetzeFarbschema('PKS_VERSNR','P'));
				$Form->ZeileEnde();
				
				/**
				 * VERSCHEINNR:
				 */
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_VERSSCHNR'].':',200);
				$Form->Erstelle_TextFeld('PKS_VERSSCHEINNR',isset($_POST['txtPKS_VERSSCHEINNR'])?$_POST['txtPKS_VERSSCHEINNR']:$rsPKS->FeldInhalt('PKS_VERSSCHEINNR'), 25,50, $aendern, '',SetzeFarbschema('PKS_VERSSCHEINNR','P'), '', 'T', 'L','','',50);
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('T');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				// Eingabe Kundendaten
					
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['UEBERSCHRIFT_FILIALBESTIMMUNG'], 800, 'font-weight:bolder;;font-size: 21px');
				$Form->ZeileEnde();
				
				$Form->ZeileStart();
				$Form->Trennzeile('O');
				$Form->ZeileEnde();
				
				/**
				 * Filialnr:
				 */
				
				
				
				$Form->ZeileStart();
				$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'] . ':', 200);
				$Form->AuswahlBox('PKS_FIL_ID', 'box1','', 'PKS_FIL_ID', '', 25,10,isset($_POST['sucPKS_FIL_ID'])?$_POST['sucPKS_FIL_ID']:$rsPKS->FeldInhalt('PKS_FIL_ID'),'T',$aendern,'','','',50,'','','',0,SetzeFarbschema('PKS_FIL_ID','P','suc'));
				$Form->ZeileEnde();
						
	    		$Inhalt = '';
	    		$Fehler = '';
	    		$FIL_ID = '';
	    		ob_start();
		    		
	   
		    		if($rsPKS->AnzahlDatensaetze() == 0 and (!isset($_POST['sucPKS_FIL_ID']) or $_POST['sucPKS_FIL_ID'] == ''))
		    		{
						
		    			$FIL_ID = '0';
		    			$Fehler = $AWISSprachKonserven['PKS']['MSG_FILIALEINGABE'];
		    		}
		    		else
		    		{
		    			if(isset($_POST['sucPKS_FIL_ID']) and $_POST['sucPKS_FIL_ID']!= '')
		    			{
		    				$FIL_ID= intval($_POST['sucPKS_FIL_ID']);
		    				if($_POST['sucPKS_FIL_ID'] == '')
		    				{
		    					$Fehler = $AWISSprachKonserven['PKS']['MSG_FILIALEINGABE'];
		    				}
		    			}
		    			else 
		    			{
		    				if(isset($_POST['sucPKS_FIL_ID']) and $_POST['sucPKS_FIL_ID'] == '')
		    				{
		    					$FIL_ID = '0';
		    					$Fehler = $AWISSprachKonserven['PKS']['MSG_FILIALEINGABE'];
		    				}
		    				else
		    				{
		    					$FIL_ID = intval($rsPKS->FeldInhalt('PKS_FIL_ID'));
		    				}
		    			}
		    			
		    			if($FIL_ID == '0' and $Fehler == '')
		    			{
		    				$Fehler == 'keine g�ltige Fililale';
		    			}
		    		}
		    		
		    			$SQL  = ' select FEB_BEZEICHNUNG,(select FIL_BEZ from Filialen where FIL_ID = '.$FIL_ID.') as FIL_BEZ from filialebenen where feb_key = ';
		    			$SQL .= ' (select distinct feb_feb_key from v_filialebenenrollen_aktuell  ';
		    			$SQL .= ' inner join Filialebenen on xx1_feb_key = feb_key ';
		    			$SQL .= ' where xx1_fil_id ='.$FIL_ID.')';
		    				
		    			$rsFIL = $DB->RecordSetOeffnen($SQL);
		    			
		    			if($rsFIL->AnzahlDatensaetze() == 0 and $Fehler == '')
		    			{
		    				$Fehler = 'keine g�ltige Fililale'; 
		    			}
		    		
	    			$Form->ZeileStart();
	    			$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_FILNAME'].':',200);
	    			$Form->Erstelle_TextFeld('PKS_FIL_NAME',$Fehler == '' ? $rsFIL->FeldInhalt('FIL_BEZ'):$Fehler, 40,300, false, '', '', '', 'T', 'L','','',50);
	    			$Form->ZeileEnde();
	    			/**
	    			 * Region nur lesen!
	    			 */
	    			$Form->ZeileStart();
	    			$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_REGION'].':',200);
	    			$Form->Erstelle_TextFeld('PKS_REGION',$Fehler == '' ? $rsFIL->FeldInhalt('FEB_BEZEICHNUNG'):$Fehler, 40,300, false, '', '', '', 'T', 'L','','',50);
	    			$Form->ZeileEnde();
	    			
	    			$Form->ZeileStart();
	    			$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_LL'].':',200);
	    			
	    			
	    			if($FIL_ID != '0')
	    			{
	    				$SQL  = 'select ';
	    				$SQL .= '(select KON_NAME1||\',\'||KON_NAME2||\' \'||\'(GBL)\' as KON_NAME from v_filialebenenrollen_aktuell ';
	    				$SQL .= ' inner join kontakte on xx1_kon_key = kon_key';
	    				$SQL .= ' where xx1_fil_id ='.$FIL_ID;
	    				$SQL .= ' and xx1_fer_key = 25 and rownum = 1) GBL';
	    				//$SQL .= '(select KON_NAME1||\',\'||KON_NAME2||\' \'||\'(LEITER LACK)\' as KON_NAME from v_filialebenenrollen_aktuell';
	    				//$SQL .= ' inner join kontakte on xx1_kon_key = kon_key';
	    				//$SQL .= ' where xx1_fil_id ='.$FIL_ID;
	    				//$SQL .= ' and xx1_fer_key = 47';
	    				//$SQL .= ') LL from dual';
	    				$SQL .= ' from dual';
	    				
	    				$rsZU = $DB->RecordSetOeffnen($SQL);
	    			
	    				/**
	    				 * Zust�ndiger nur lesen!
	    				*/
	    			
	    				$Zustaendiger='';
	    				if($rsZU->FeldInhalt('LL') =='' and $rsZU->FeldInhalt('GBL') != '')
	    				{
	    					$Zustaendiger = $rsZU->FeldInhalt('GBL');
	    				}
	    				else if($rsZU->FeldInhalt('LL') !='')
	    				{
	    					$Zustaendiger = $rsZU->FeldInhalt('LL');
	    				}
	    				else 
	    				{
	    					$Zustaendiger = 'Kein Zust�ndiger gefunden';
	    				}
	    			}
	    			$Form->Erstelle_TextFeld('PKS_ZUSTAENDIG',$Fehler == '' ? $Zustaendiger : $Fehler, 40,300, false, '', '', '', 'T', 'L','','',50);
	    			$Form->ZeileEnde();
	    			
	    			
	    			$Inhalt = ob_get_contents();
	    		ob_end_clean();
	
	    		
				$Form->AuswahlBoxHuelle('box1','AuswahlListe','',$Inhalt);
		
				
				if(($Recht11005&2)==2 or (($Recht11005&8)==8 and ($rsPKS->FeldInhalt('PKS_STATUS') == '0' or $AWIS_KEY1 == '0' or $rsPKS->FeldInhalt('PKS_STATUS') == '3')))
				{
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('T');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
				
					/**
					 *  Status
					 */
					
					
					$Form->ZeileStart();
					if($aendern == false)
					{
						$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_STATUS_SVT_LIST']);
					}
					else
					{
						if($AWIS_KEY1 == '0')
						{
							$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_STATUS_SVT_OSTORNO']);
						}
						else 
						{
							$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_STATUS_SVT']);
						}
					}
					
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_STATUS'] . ':',200);
					$Form->Erstelle_SelectFeld('PKS_STATUS',isset($_POST['txtPKS_STATUS'])?$_POST['txtPKS_STATUS']:$rsPKS->FeldInhalt('PKS_STATUS'),250,$aendern,'','','','','',$Daten,'onchange="blendeEinStorno(this);"');
					$Form->ZeileEnde();
					
					if((isset($_POST['txtPKS_STATUS']) and $_POST['txtPKS_STATUS'] == '3') or ($rsPKS->FeldInhalt('PKS_STATUS') == '3'))
					{
						$Form->ZeileStart('display:block','GSEM"ID="STORNOLEER');
					}
				    else 
				    {
						$Form->ZeileStart('display:none','GSEM"ID="STORNOLEER');
				    }
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					if((isset($_POST['txtPKS_STATUS']) and $_POST['txtPKS_STATUS'] == '3') or ($rsPKS->FeldInhalt('PKS_STATUS') == '3'))
					{
						$Form->ZeileStart('display:bock','GSUEB"ID="UEBGRUNDSTORNO');
					}
					else
					{
						$Form->ZeileStart('display:none','GSUEB"ID="UEBGRUNDSTORNO');
					}
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_GRUNDSTORNO'] . ':',200);
					$Form->ZeileEnde();
					
					if((isset($_POST['txtPKS_STATUS']) and $_POST['txtPKS_STATUS'] == '3') or ($rsPKS->FeldInhalt('PKS_STATUS') == '3'))
					{
						$Form->ZeileStart('display:block','GS"ID="GRUNDSTORNO');
					}
					else 
					{
						$Form->ZeileStart('display:none','GS"ID="GRUNDSTORNO');
					}
					$Form->Erstelle_Textarea('PKS_GRUNDSTORNO',isset($_POST['txtPKS_GRUNDSTORNO'])?$_POST['txtPKS_GRUNDSTORNO']:$rsPKS->FeldInhalt('PKS_GRUNDSTORNO'),500,80,3,$aendern,'font: bolder',SetzeFarbschema('PKS_GRUNDSTORNO','P'));
					$Form->ZeileEnde();
				}
				if(($rsPKS->FeldInhalt('PKS_STATUS') > 0 and $rsPKS->FeldInhalt('PKS_STATUS') != 3) and (($Recht11005&4)==4 or ($Recht11005&8)==8))
				{
					$aendernLL = true;
					if($rsPKS->FeldInhalt('PKS_STATUS') > 1)
					{
						$aendernLL = false;
					}
					else
					{
						$aendernLL = true;
					}
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('T');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					// Kundengespr�ch LL
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['UEBERSCHRIFT_KDGESPRAECH'], 800, 'font-weight:bolder;;font-size: 21px');
					$Form->ZeileEnde();
					
					$Form->ZeileStart();
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
					
					/**
					 * Kundenr�ckruf
					 */
						
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_DATUMRR'].':',200);
					$Form->Erstelle_TextFeld('PKS_DATUMRR',isset($_POST['txtPKS_DATUMRR'])?$_POST['txtPKS_DATUMRR']:$rsPKS->FeldInhalt('PKS_DATUMRR'),7,150,$aendernLL,'',SetzeFarbschema('PKS_DATUMRR','P'),'','D','L','','',10);
					$Form->ZeileEnde();
					
					/**
					 *  Parkschaden Iststand
					 */
					
					$Form->ZeileStart();
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_STAND'] . ':',200);
					$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_JANEIN']);
					$Form->Erstelle_SelectFeld('PKS_STAND',isset($_POST['txtPKS_STAND'])?$_POST['txtPKS_STAND']:$rsPKS->FeldInhalt('PKS_STAND'),150,$aendernLL,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten,'onchange="blendeEinIststand(this);"','',array(),SetzeFarbschema('PKS_STAND','P'));
					$Form->ZeileEnde();
					
					/**
					 * Besichtigungstermin
					 */
					if(($aendernLL == false and $rsPKS->FeldInhalt('PKS_DATUMBES') != '') or ($rsPKS->FeldInhalt('PKS_STAND') == '1' or (isset($_POST['txtPKS_STAND']) and $_POST['txtPKS_STAND'] == '1')))
					{
						if((isset($_POST['txtPKS_STAND']) and $_POST['txtPKS_STAND'] == '1'))
						{
							$Form->ZeileStart('display:block','DB"ID="DATUMBES');
						}
						else if(isset($_POST['txtPKS_STAND']) and ($_POST['txtPKS_STAND'] == '::bitte w�hlen::' or $_POST['txtPKS_STAND'] != '1'))
						{
							$Form->ZeileStart('display:none','DB"ID="DATUMBES');
						}
						else if($rsPKS->FeldInhalt('PKS_STAND') == '1')
						{
							$Form->ZeileStart('display:block','DB"ID="DATUMBES');
						}
						else
						{
							$Form->ZeileStart('display:none','DB"ID="DATUMBES');
						}
					}
					else
					{
						$Form->ZeileStart('display:none','DB"ID="DATUMBES');
					}
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_DATUMBES'].':',200);
					$Form->Erstelle_TextFeld('PKS_DATUMBES',isset($_POST['txtPKS_DATUMBES'])?$_POST['txtPKS_DATUMBES']:$rsPKS->FeldInhalt('PKS_DATUMBES'),7,150,$aendernLL,'',SetzeFarbschema('PKS_DATUMBES','P'),'','D','L','','',10);
					$Form->ZeileEnde();
					
					//var_dump($_POST['txtPKS_STAND']);
					if($rsPKS->FeldInhalt('PKS_STAND') == '0' or (isset($_POST['txtPKS_STAND'])))
					{
						if(isset($_POST['txtPKS_STAND']) and ($_POST['txtPKS_STAND'] == '::bitte w�hlen::' or $_POST['txtPKS_STAND'] == '1'))
						{
							$Form->ZeileStart('display:none','DB"ID="UEBDATUMBES');
						}
						else
						{
							
							$Form->ZeileStart('display:block','UDB"ID="UEBDATUMBES');
						}
					}
					else
					{
						$Form->ZeileStart('display:none','DB"ID="UEBDATUMBES');
					}
					$Form->Trennzeile('O');
					$Form->ZeileEnde();
	
					
					/**
					 * Ablehnungsgrund
					 */
					
					if(($aendernLL == false and $rsPKS->FeldInhalt('PKS_GRUNDABL') != '') or ($rsPKS->FeldInhalt('PKS_STAND') == '0' or (isset($_POST['txtPKS_STAND']) and $_POST['txtPKS_STAND'] == '0')))
					{
						if((isset($_POST['txtPKS_STAND']) and $_POST['txtPKS_STAND'] == '0'))
						{
							$Form->ZeileStart('display:block;','GA" ID="UEBGRUNDABL');
						}
						else if(isset($_POST['txtPKS_STAND']) and ($_POST['txtPKS_STAND'] == '::bitte w�hlen::' or $_POST['txtPKS_STAND'] != '0'))
						{
							$Form->ZeileStart('display:none;','GAU" ID="UEBGRUNDABL');
						}
						else if($rsPKS->FeldInhalt('PKS_STAND') == '0')
						{
							$Form->ZeileStart('display:block;','GAU" ID="UEBGRUNDABL');
						}
						else
						{
							$Form->ZeileStart('display:none;','GAU" ID="UEBGRUNDABL');
						}
					}
					else
					{
						$Form->ZeileStart('display:none','GAU" ID="UEBGRUNDABL');
					}
					$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_GRUNDABL'] . ':',200);
					$Form->ZeileEnde();
					
					if(($aendernLL == false and $rsPKS->FeldInhalt('PKS_GRUNDABL') != '') or ($rsPKS->FeldInhalt('PKS_STAND') == '0' or (isset($_POST['txtPKS_STAND']) and $_POST['txtPKS_STAND'] == '0')))
					{
						if((isset($_POST['txtPKS_STAND']) and $_POST['txtPKS_STAND'] == '0'))
						{
							$Form->ZeileStart('display:block','GA"ID="GRUNDABL');
						}
						else if(isset($_POST['txtPKS_STAND']) and ($_POST['txtPKS_STAND'] == '::bitte w�hlen::' or $_POST['txtPKS_STAND'] != '0'))
						{
							$Form->ZeileStart('display:none','GA"ID="GRUNDABL');
						}
						else if($rsPKS->FeldInhalt('PKS_STAND') == '0')
						{
							$Form->ZeileStart('display:block','GA"ID="GRUNDABL');
						}
						else 
						{
							$Form->ZeileStart('display:none','GA"ID="GRUNDABL');
						}
					}
					else
					{
						$Form->ZeileStart('display:none','GA"ID="GRUNDABL');
					}
					$Form->Erstelle_Textarea('PKS_GRUNDABL',isset($_POST['txtPKS_GRUNDABL'])?$_POST['txtPKS_GRUNDABL']:$rsPKS->FeldInhalt('PKS_GRUNDABL'),500,80,3,$aendernLL,'font: bolder',SetzeFarbschema('PKS_GRUNDABL','P'));
					$Form->ZeileEnde();
					
					if($rsPKS->FeldInhalt('PKS_STATUS') == 1)
					{
					
					
						$Form->ZeileStart('display:block','EMPTY"ID="EMPTY');
						$Form->Trennzeile('O');
						$Form->ZeileEnde();
						
						$Form->ZeileStart();
						$Form->Trennzeile('T');
						$Form->ZeileEnde();
						
						$Form->ZeileStart();
						$Form->Trennzeile('O');
						$Form->ZeileEnde();
					
					
					
						/**
						 *  Status
						 */
						
			
						$Form->ZeileStart();
						$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_STATUS'] . ':',200);
						$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_MASKE_LL']);
						$Form->Erstelle_SelectFeld('PKS_STATUS',isset($_POST['txtPKS_STATUS'])?$_POST['txtPKS_STATUS']:$rsPKS->FeldInhalt('PKS_STATUS'),150,true,'','','','','',$Daten);
						$Form->ZeileEnde();
					}
					
					
				}
			}
			$Form->ZeileStart();
			$Form->Trennzeile('O');
			$Form->ZeileEnde();
			
			
		}
		else
		{
			// Infozeile zusammenbauen
			$Felder = array();
			//$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href=./abgleich_Main.php?cmdAktion=Kasse' . (isset($_GET['Block'])?'&Block='.$_GET['Block']:'') . ' accesskey=S title=' . $AWISSprachKonserven['TAG']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
			$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => '<a href="./srpep_Main.php?cmdAktion=Parkschaden" accesskey=S title=' . $AWISSprachKonserven['PKS']['ZurueckSuche'] . '><img border=0 src=/bilder/cmd_trefferliste.png></a>');
			$Felder[] = array('Style'=>'font-size:smaller;', 'Inhalt' => $AWISSprachKonserven['PKS']['ZurueckSuche']);
			$Form->InfoZeile($Felder, '');
		
		
			$Form->ZeileStart();
			$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
			$Form->ZeileEnde();
		}
		
		$Form->Formular_Ende();
	}
	else 
	{
		
		$Form->Formular_Start();
		/**
		 * Erfassungsdatum
		 */
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['DatumVom'].':',200);
		$Form->Erstelle_TextFeld('*PKS_ERFDATUM',($Param['SPEICHERN']=='on'?$Param['PKS_ERFDATUM']:''),7,150,true,'','','','D','L','',($Param['SPEICHERN']=='on'?'':date('d.m.Y',time())),10);
		$Form->ZeileEnde();
		$AWISCursorPosition = 'sucPKS_ERFDATUM';
		
		$SQL ='select feb_key,feb_bezeichnung from v_filialebenenrollen_aktuell';
		$SQL .=' inner join Filialebenen on xx1_feb_key = feb_key';
		
		
		
		/**
		 * Gebiete
		 */
		
		if(($Recht11005&4)==4)
		{
			//Nur f�r GBL/RL/RGZ und ihre Vertreter 14.01.2015 ST
			$SQL .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
			$SQL .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'and xx1_fer_key in (25))';
			$SQL .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
			$SQL .=' and trunc(frv_datumvon) <= trunc(sysdate)';
			$SQL .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (25))';
		}
		
		$SQL .=' group by feb_key,feb_bezeichnung';
		$SQL .=' order by feb_bezeichnung';
		
		//Gebiete TODO f�r Lackmeister/GBL einschr�nken
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['FIL']['FIL_GEBIET'].':',200);
		$Form->Erstelle_SelectFeld('*PKS_GEBIET',($Param['SPEICHERN']=='on'?$Param['FIL_GEBIET']:''),400,true,$SQL,$AWISSprachKonserven['Liste']['lst_ALLE_0']);
		$Form->ZeileEnde();
		
		
		/**
		 * Filiale
		 */
		if(($Recht11005&4)==4)
		{
			//Filiale TODO f�r Lackmeister/GBL einschr�nken
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',180);
			$Form->Erstelle_SelectFeld('*PKS_FIL_ID_SUCH',($Param['SPEICHERN']=='on'?$Param['PKS_FIL_ID']:''),400, true, '***PKS_Daten;sucPKS_FIL_ID_SUCH;Gebiet=*sucPKS_GEBIET&ZUSATZ='.$AWISSprachKonserven['Liste']['lst_ALLE_0'].'&KON='.$AWISBenutzer->BenutzerKontaktKEY(),(($Param['PKS_FIL_ID'] == '0' or $Param['SPEICHERN']=='')?$AWISSprachKonserven['Liste']['lst_ALLE_0']: $Param['PKS_FIL_ID'].'~'.$Param['PKS_FIL_ID']),'', '', '');
			$Form->ZeileEnde();
		}
		else 
		{
			//Filiale TODO f�r Lackmeister/GBL einschr�nken
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['wrd_Filiale'].':',200);
			$Form->Erstelle_TextFeld('*PKS_FIL_ID_SUCH',($Param['SPEICHERN']=='on'?$Param['PKS_FIL_ID']:''), 1,5, true, '', '', '', 'T', 'L','','',4);
			$Form->ZeileEnde();
		}

	
		/**
		 * Kundennachname
		 */
		
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_NACHNAME'].':',200);
		$Form->Erstelle_TextFeld('*PKS_NACHNAME',($Param['SPEICHERN']=='on'?$Param['PKS_NACHNAME']:''), 25,50, true, '', '', '', 'T', 'L','','',50);
		$Form->ZeileEnde();
		
		/**
		 * Versicherungsscheinnummer
		 */
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_VERSSCHNR'].':',200);
		$Form->Erstelle_TextFeld('*PKS_VERSSCHNR',($Param['SPEICHERN']=='on'?$Param['PKS_VERSSCHNR']:''), 20,50, true, '', '', '', 'T', 'L','','',20);
		$Form->ZeileEnde();
		
		
		/**
		 * Versicherung
		 */
	
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_VERSNR'].':',200);
			
		$SQL = 'SELECT VVE_VERSNR,VVE_BEZEICHNUNG'; // Alle Aktiven Versicherungen f�r Parksch�denerfassung holen
		$SQL .= ' FROM VERSVERSICHERUNGEN';
		$SQL .= ' WHERE VVE_PKS_VERSAKTIV = 1';
		$SQL .= ' ORDER BY VVE_BEZEICHNUNG';
	
		$Form->Erstelle_SelectFeld('*PKS_VERSNR',($Param['SPEICHERN']=='on'?$Param['PKS_VERSNR']:''),400,true,$SQL,$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$Form->ZeileEnde();
				
		/**
		 *  Schadensart
		*/
				
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_SCHADENART'] . ':',200);
		$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_SCHADENART']);
		$Form->Erstelle_SelectFeld('*PKS_SCHADENART',($Param['SPEICHERN']=='on'?$Param['PKS_SCHADENART']:''),150,true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
	
		/**
		 *  Schadensposition
		 */
			
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_POS'] . ':',200);
		$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_POS']);
		$Form->Erstelle_SelectFeld('*PKS_POSITION',($Param['SPEICHERN']=='on'?$Param['PKS_POSITION']:''),150,true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
	
		/**
		 *  Status
		 */


		 $Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_STATUS'] . ':',200);
		if(($Recht11005&2)==2)
		{
			$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_STATUS_SVT']);
		}
		elseif(($Recht11005&4)==4)
		{
			$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_STATUS_LL']);
		}
		else 
		{
			$Daten = explode('|',$AWISSprachKonserven['PKS']['PKS_LST_STATUS_ADMIN']);
		}
		$Form->Erstelle_SelectFeld('*PKS_STATUS',($Param['SPEICHERN']=='on'?$Param['PKS_STATUS']:''),150,true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
		
		
		/**
		 *  Entscheidung Ja/nein
		 */
		// TODO Nur f�r Leiter Lack
		/*
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['PKS']['PKS_ENTSCHEIDUNG'] . ':',200);
		$Daten = explode('|',$AWISSprachKonserven['Liste']['lst_JaNein']);
		$Form->Erstelle_SelectFeld('*PKS_ENTSCHEIDUNG',($Param['SPEICHERN']=='on'?$Param['PKS_ENTSCHEIDUNG']:''),150,true,'',$AWISSprachKonserven['Wort']['txt_BitteWaehlen'],'','','',$Daten);
		$Form->ZeileEnde();
		*/	
		
		// Auswahl kann gespeichert werden
		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['Wort']['AuswahlSpeichern'].':',200);
		$Form->Erstelle_Checkbox('*AuswahlSpeichern',($Param['SPEICHERN']=='on'?'on':''),20,true,'on');
		$Form->ZeileEnde();
		
		//$Form->SchreibeHTMLCode('</form>');
		$Form->Formular_Ende();
	}
	//************************************************************
	//* Schaltfl�chen
	//************************************************************
	$Form->SchaltflaechenStart();
	// Zur�ck zum Men�
	$Form->Schaltflaeche('href','cmd_zurueck','/index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	if((!isset($_POST['cmdSuche_x']) and !isset($_POST['cmdDSNeu_x'])) and !isset($_REQUEST['Block']) and !isset($_REQUEST['Sort']) and $AWIS_KEY1 == '' and $Seite == 'Suche')
	{
		$Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $AWISSprachKonserven['Wort']['lbl_suche'], 'W');
	}
	if(($AWIS_KEY1 != '' or isset($rsPKS)) and (((($Recht11005&2)==2 or ($Recht11005&8)==8) and $rsPKS->FeldInhalt('PKS_STATUS')==0) or ((($Recht11005&4)==4 or ($Recht11005&8)==8) and $rsPKS->FeldInhalt('PKS_STATUS')==1)))
	{
		if($rsPKS->AnzahlDatensaetze() == 1 or ((($Recht11005&2)==2 or ($Recht11005&8)==8) and $AWIS_KEY1 != '') and !$FalscherDS)
		{
			$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
		}
		else 
		{
			if(($Recht11005&2)==2 or ($Recht11005&8)==8)
			{
				if(isset($rsPKS))
				{
					if($rsPKS->FeldInhalt('PKS_STATUS') == '0' or $rsPKS->AnzahlDatensaetze() > 1 or $Seite == 'Suche' and !$FalscherDS)
					{
						$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
					}
						
				}
				else 
				{
					$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
				}
			}
		}
	}
	else
	{
		if(($Recht11005&2)==2 or ($Recht11005&8)==8)
		{
			if(isset($rsPKS))
			{
				if($rsPKS->FeldInhalt('PKS_STATUS') == '0' or $rsPKS->AnzahlDatensaetze() > 1)
				{
					$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
				}
			}
			else
			{
				$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
			}
		}
	}
	$Form->SchaltflaechenEnde();
	
	$Form->SchreibeHTMLCode('</form>');
	$Form->SetzeCursor($AWISCursorPosition);

}
catch (awisException $ex)
{
	
	if($Form instanceof awisFormular)
	{
			$Form->DebugAusgabe(1, $ex->getSQL());
			$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"201211161605");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

function _BedingungErstellen($Param)
{
	global $AWISBenutzer;
	global $DB;
	global $Form;
	
	$Recht11005 = $AWISBenutzer->HatDasRecht(11005);
	
	$Bedingung = '';
	
	if(($Recht11005&4)==4)
	{

		//SQL einschr�nken f�r GBL,LL
		$Bedingung .= 'AND PKS_FIL_ID in( select xx1_fil_id from v_filialebenenrollen_aktuell';
		$Bedingung .=' inner join Filialebenen on xx1_feb_key = feb_key';
		$Bedingung .=' left join filialebenenrollenvertreter on xx1_frz_key = frv_frz_key';
		$Bedingung .=' where (xx1_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'and xx1_fer_key in (25))';
		$Bedingung .=' or (frv_kon_key ='.$AWISBenutzer->BenutzerKontaktKEY().'';
		$Bedingung .=' and trunc(frv_datumvon) <= trunc(sysdate)';
		$Bedingung .=' and trunc(frv_datumbis) >= trunc(sysdate) and xx1_fer_key in (25))';
		$Bedingung .=' group by xx1_fil_id) ';
	
	}
	
	
	if (isset($Param['PKS_ERFDATUM']) AND (!empty($Param['PKS_ERFDATUM'])))                                               // "ID" ignorieren, kommt wenn �berschriftenzeile gew�hlt w�rde (vgl. Funktion "Feld_BelegNr" << '\'ID\', ' .        // 1. Spalte (siehe Kommentar f�r 1. Spalte nach UNION) >>)
	{
		$Bedingung .= ' AND PKS_ERFDATUM = ' . $DB->FeldInhaltFormat('D', $Param['PKS_ERFDATUM']);
	}
	if(isset($Param['FIL_GEBIET']) AND $Param['FIL_GEBIET'] !='' AND $Param['FIL_GEBIET'] !='0')
	{
		//Fehler behoben G�ltigkeiten hinzugef�gt 14.01.2015 ST
		$Bedingung .= 'AND PKS_FIL_ID in( select fez_fil_id from filialebenenzuordnungen ';
		$Bedingung .=' where fez_feb_key ='.$Param['FIL_GEBIET'];
		$Bedingung .=' and trunc(fez_gueltigab) <= trunc(sysdate)';
		$Bedingung .=' and trunc(fez_gueltigbis) >= trunc(sysdate))';
	}
	
	if(isset($Param['PKS_FIL_ID']) AND $Param['PKS_FIL_ID']!='' AND $Param['PKS_FIL_ID']!=0)
	{
		$Bedingung .= 'AND PKS_FIL_ID =' . $Param['PKS_FIL_ID'];
	}
	
	if (isset($Param['PKS_NACHNAME']) AND (! empty($Param['PKS_NACHNAME'])))
	{
		$Bedingung .= " AND (UPPER(PKS_NACHNAME) " . $DB->LIKEoderIST('%' . $Param['PKS_NACHNAME'] . '%', awisDatenbank::AWIS_LIKE_UPPER) . ")";
	}
	
	if (isset($Param['PKS_VERSSCHNR']) AND (! empty($Param['PKS_VERSSCHNR'])))
	{
		$Bedingung .= " AND (UPPER(PKS_VERSSCHEINNR) " . $DB->LIKEoderIST('%' . $Param['PKS_VERSSCHNR'] . '%', awisDatenbank::AWIS_LIKE_UPPER) . ")";
	}
	
	if(isset($Param['PKS_VERSNR']) AND $Param['PKS_VERSNR']!='::bitte w�hlen::')
	{
		$Bedingung .= ' AND PKS_VERSNR =' . $Param['PKS_VERSNR'];
	}
	
	if(isset($Param['PKS_SCHADENART']) AND $Param['PKS_SCHADENART']!='::bitte w�hlen::')
	{
		$Bedingung .= ' AND PKS_SCHADENART =' . $Param['PKS_SCHADENART'];
	}
	
	if(isset($Param['PKS_SCHADENART']) AND $Param['PKS_SCHADENART']!='::bitte w�hlen::')
	{
		$Bedingung .= ' AND PKS_SCHADENART =' . $Param['PKS_SCHADENART'];
	}
	
	if(isset($Param['PKS_POSITION']) AND $Param['PKS_POSITION']!='::bitte w�hlen::')
	{
		$Bedingung .= ' AND PKS_SCHADENPOS =' . $Param['PKS_POSITION'];
	}
	
	if(isset($Param['PKS_STATUS']) AND $Param['PKS_STATUS']!='::bitte w�hlen::')
	{
		if(($Recht11005&2)==2)
		{
			if($Param['PKS_STATUS'] == '1')
			{
				$Bedingung .= ' AND PKS_STATUS in(1,2)';
			}
			else 
			{
				$Bedingung .= ' AND PKS_STATUS =' . $Param['PKS_STATUS'];
			}
		}
		else 
		{
			$Bedingung .= ' AND PKS_STATUS =' . $Param['PKS_STATUS'];
		}
	}
	

	
	return $Bedingung;
}

function SetzeFarbschema($Felder,$Definition,$Praefix='txt')
{
	global $rsPKS;
	$FeldListe = array();
	$FeldListe = explode(';',$Felder);
	if($Definition == 'P')
	{
		$InputStyle = '';
	}
	else 
	{
		$InputStyle = 'background-color:#F7BE81';
	}
	$count = 0;
	foreach($FeldListe as $Feld)
	{
		//$Praefix = (isset($_POST['txt'.$Feld])?'txt':'suc');
		//$Praefix = 'txt';
		if(isset($_POST[$Praefix.$Feld]) and $_POST[$Praefix.$Feld] != '::bitte w�hlen::')
		{
			$FeldListe[$count] = $_POST[$Praefix.$Feld];
		}
		else if(isset($_POST[$Praefix.$Feld]) and $_POST[$Praefix.$Feld] == '::bitte w�hlen::')
		{
			$FeldListe[$count] = '';
		}
		else
		{
			$FeldListe[$count] = $rsPKS->FeldInhalt($Feld);
		}
		if($Definition == 'P')
		{
			if($FeldListe[$count] == '')
			{
				$InputStyle = 'background-color:#F78181';
			}
		}	
		else if($Definition == 'M')
		{
			if($FeldListe[$count] != '')
			{
				$InputStyle = '';
			}
		}
		$count++;
    }	
	return $InputStyle;
}

?>