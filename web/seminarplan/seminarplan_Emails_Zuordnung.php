<?php
global $SEP;
require_once 'seminarplan_Emails_funktionen.inc';
try {
    $SEP = new seminarplan_Emails_funktionen();

    $SQL = 'select ';
    $SQL .= ' SEM_KEY ,';
    $SQL .= ' SEM_SEP_SEMINAR ,';
    $SQL .= ' SEM_MVT_BEREICH ,';
    $SQL .= ' SEM_ANHAENGE ,';
    $SQL .= ' SEM_ABSENDER ,';
    $SQL .= ' SEM_EMPFAENGER ,';
    $SQL .= ' SEM_EMPFAENGERCC ,';
    $SQL .= ' SEM_EMPFAENGERBCC ,';
    $SQL .= ' SEM_USER ,';
    $SQL .= ' SEM_USERDAT  ';
    $SQL .= ' ';
    $SQL .= ' from SEMINAREMAILS';

    if (isset($_GET['SEM_KEY'])) {
        $SQL .= ' WHERE SEM_KEY = ' . $SEP->DB->WertSetzen('MVT', 'T', $_GET['SEM_KEY']);
        $SEP->AWIS_KEY1 = $_GET['SEM_KEY'];
    }

    $rsMVT = $SEP->DB->RecordSetOeffnen($SQL, $SEP->DB->Bindevariablen('MVT'));

    if ($rsMVT->AnzahlDatensaetze() > 0 and $SEP->AWIS_KEY1 == '') {//Liste

        $Feldbreiten = array();
        $Feldbreiten['Icons'] = 20;
        $Feldbreiten['SEM_MVT_BEREICH'] = 200;
        $Feldbreiten['SEM_SEP_SEMINAR'] = 800;

        $SEP->Form->ZeileStart();
        $Icons[] = array('new', './seminarplan_Main.php?cmdAktion=Emails&Seite=Zuordnung&SEM_KEY=-1');
        $SEP->Form->Erstelle_ListeIcons($Icons, $Feldbreiten['Icons'], -2, $SEP->AWISSprachKonserven['Wort']['ttt_PositionHinzufuegen']);
        $SEP->Form->Erstelle_Liste_Ueberschrift('', $Feldbreiten['Icons']);
        $SEP->Form->Erstelle_Liste_Ueberschrift($SEP->AWISSprachKonserven['SEP']['SEM_MVT_BEREICH'], $Feldbreiten['SEM_MVT_BEREICH']);
        $SEP->Form->Erstelle_Liste_Ueberschrift($SEP->AWISSprachKonserven['SEP']['SEM_SEP_SEMINAR'], $Feldbreiten['SEM_SEP_SEMINAR']);
        $SEP->Form->ZeileEnde();

        $HG = 0;
        while (!$rsMVT->EOF()) {
            $HG = ($rsMVT->DSNummer() % 2);
            $SEP->Form->ZeileStart();
            $Icons = array();
            if ($rsMVT->FeldInhalt('SEM_USER') != 'AWIS') { //Es k�nnen nur Zuordnungen ge�ndert werden, die durch User entstanden sind.
                $Icons[] = array('edit', './seminarplan_Main.php?cmdAktion=Emails&Seite=Zuordnung&SEM_KEY=' . $rsMVT->FeldInhalt('SEM_KEY'));
                $SEP->Form->Erstelle_ListeIcons($Icons, $Feldbreiten['Icons'], $HG, $SEP->AWISSprachKonserven['Wort']['ttt_PositionHinzufuegen']);
                $SEP->Form->Erstelle_ListenLoeschPopUp('./seminarplan_Main.php?cmdAktion=Emails&Seite=Zuordnung&DEL=' . $rsMVT->FeldInhalt('SEM_KEY'), $rsMVT->FeldInhalt('SEM_KEY'), $Feldbreiten['Icons'], '', '', $HG);

            } else {
                $SEP->Form->Erstelle_ListenFeld('', '', $Feldbreiten['Icons'] / 10 * 2, $Feldbreiten['Icons'] * 2, false, $HG);
            }

            $SEP->Form->Erstelle_ListenFeld('', $rsMVT->FeldInhalt('SEM_MVT_BEREICH'), $Feldbreiten['SEM_MVT_BEREICH'] / 10, $Feldbreiten['SEM_MVT_BEREICH'], false, $HG);
            $SEP->Form->Erstelle_ListenFeld('', $rsMVT->FeldInhalt('SEM_SEP_SEMINAR'), $Feldbreiten['SEM_SEP_SEMINAR'] / 10, $Feldbreiten['SEM_SEP_SEMINAR'], false, $HG);

            $SEP->Form->ZeileEnde();
            $rsMVT->DSWeiter();
        }


    } else {

        // Infozeile zusammenbauen
        $Felder = array();
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => "<a href=./seminarplan_Main.php?cmdAktion=Emails&Seite=Zuordnung&Liste=True accesskey=T title='" . $SEP->AWISSprachKonserven['Wort']['lbl_trefferliste'] . "'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMVT->FeldInhalt('SEM_USER'));
        $Felder[] = array('Style' => 'font-size:smaller;', 'Inhalt' => $rsMVT->FeldInhalt('SEM_USERDAT'));
        $SEP->Form->InfoZeile($Felder, '');

        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['SEP']['SEM_SEP_SEMINAR'], 200);
        $SEP->Form->Erstelle_TextFeld('!SEM_SEP_SEMINAR', $rsMVT->FeldOderPOST('SEM_SEP_SEMINAR'), 18, 180, $SEP->EditRecht);
        $SEP->Form->ZeileEnde();


        echo '<script>' . PHP_EOL;
        echo '$(document).ready(function(){ ' . PHP_EOL;
        echo ' lst_SEM_Vorschau(this); ' . PHP_EOL;
        echo '});' . PHP_EOL;
        echo '</script> ' . PHP_EOL;
        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['SEP']['SEM_MVT_BEREICH'], 200);
        $SQL = 'select MVT_BEREICH, MVT_BEREICH from mailversandtexte where mvt_bereich like \'SET_EINLADUNG_%\'';
        $SEP->Form->AuswahlSelectFeld('!SEM_MVT_BEREICH', $rsMVT->FeldOderPOST('SEM_MVT_BEREICH'), 630, $SEP->EditRecht, $SQL, 'lst_SEM_Vorschau', 'Box1', array(), $SEP->OptionBitteWaehlen);
        $SEP->Form->ZeileEnde();

        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['SEP']['SEM_ANHAENGE'], 200);
        $Daten = explode('|', $SEP->AWISSprachKonserven['SEP']['lst_SEM_ANHAENGE']);
        if (isset($_POST['txtSEM_MVT_BEREICH[]'])) {
            $Wert = $_POST['txtSEM_MVT_BEREICH[]'];
        } else {
            $Wert = unserialize($rsMVT->FeldInhalt('SEM_ANHAENGE'));
            if (!is_array($Wert)) {
                $Wert = array($Wert);
            }
        }
        $SEP->Form->Erstelle_MehrfachSelectFeld('SEM_ANHAENGE', $Wert, '500:500', $SEP->EditRecht, '', '', '', '', '', $Daten);
        $SEP->Form->ZeileEnde();

        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['SEP']['SEM_ABSENDER'], 200);
        $SEP->Form->Erstelle_TextFeld('!SEM_ABSENDER', $rsMVT->FeldOderPOST('SEM_ABSENDER'), 70, 700, $SEP->EditRecht);
        $SEP->Form->ZeileEnde();

        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['SEP']['SEM_EMPFAENGER'], 200);
        $Daten = explode('|', $SEP->AWISSprachKonserven['SEP']['lst_SEM_EMPFAENGER']);
        if (isset($_POST['txtSEM_EMPFAENGER[]'])) {
            $Wert = $_POST['txtSEM_EMPFAENGER[]'];
        } else {
            $Wert = unserialize($rsMVT->FeldInhalt('SEM_EMPFAENGER'));
            if (!is_array($Wert)) {
                $Wert = array($Wert);
            }
        }
        $SEP->Form->Erstelle_MehrfachSelectFeld('!SEM_EMPFAENGER', $Wert, '500:500', $SEP->EditRecht, '', '', '', '', '', $Daten);
        $SEP->Form->ZeileEnde();


        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['SEP']['SEM_EMPFAENGERCC'], 200);
        $Daten = explode('|', $SEP->AWISSprachKonserven['SEP']['lst_SEM_EMPFAENGER']);
        if (isset($_POST['txtSEM_EMPFAENGERCC[]'])) {
            $Wert = $_POST['txtSEM_EMPFAENGERCC[]'];
        } else {
            $Wert = unserialize($rsMVT->FeldInhalt('SEM_EMPFAENGERCC'));
            if (!is_array($Wert)) {
                $Wert = array($Wert);
            }
        }
        $SEP->Form->Erstelle_MehrfachSelectFeld('SEM_EMPFAENGERCC', $Wert, '500:500', $SEP->EditRecht, '', '', '', '', '', $Daten);
        $SEP->Form->ZeileEnde();


        $SEP->Form->ZeileStart();
        $SEP->Form->Erstelle_TextLabel($SEP->AWISSprachKonserven['SEP']['SEM_EMPFAENGERBCC'], 200);
        $Daten = explode('|', $SEP->AWISSprachKonserven['SEP']['lst_SEM_EMPFAENGER']);
        if (isset($_POST['txtSEM_EMPFAENGERBCC[]'])) {
            $Wert = $_POST['txtSEM_EMPFAENGERBCC[]'];
        } else {
            $Wert = unserialize($rsMVT->FeldInhalt('SEM_EMPFAENGERBCC'));
            if (!is_array($Wert)) {
                $Wert = array($Wert);
            }
        }
        $SEP->Form->Erstelle_MehrfachSelectFeld('SEM_EMPFAENGERBCC', $Wert, '500:500', $SEP->EditRecht, '', '', '', '', '', $Daten);
        $SEP->Form->ZeileEnde();

        $SEP->Form->AuswahlBoxHuelle('Box1');
    }

    $SEP->Form->Formular_Ende();

    $SEP->Form->SchaltflaechenStart();
    $SEP->Form->Schaltflaeche('href', 'cmd_zurueck', '/index.php', '/bilder/cmd_zurueck.png', $SEP->AWISSprachKonserven['Wort']['lbl_zurueck'], 'Z');
    if ($SEP->AWIS_KEY1 != '') {
        $SEP->Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $SEP->AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
    }

    $SEP->Form->SchaltflaechenEnde();


} catch (awisException $ex) {
    if ($SEP->Form instanceof awisFormular) {
        $SEP->Form->DebugAusgabe(1, $ex->getSQL());
        $SEP->Form->Fehler_Anzeigen('INTERN', $ex->getMessage(), 'MELDEN', 6, "200812180928");
    } else {
        echo 'AWIS-Fehler:' . $ex->getMessage();
    }
}

?>