<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;

require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KEZ','%');
	$TextKonserven[]=array('KEW','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht9200= $AWISBenutzer->HatDasRecht(9200);		// kennwoerterer in Filialinfo
	if(($Recht9200&1)==0)
	{
		$Form->Fehler_KeineRechte();
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KEWKEZ'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);
	$JaNeinListe = explode("|",$AWISSprachKonserven['Liste']['lst_JaNein']);

	//*********************************************************
	//* Sortierung
	//*********************************************************
	if(!isset($_GET['SSort']))
	{
		if(isset($Param['ORDER']) AND $Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY XXX_TYP, XXX_NAME';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['SSort']);
		$Param['ORDER']=$ORDERBY;
	}

	if(isset($_GET['Edit']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['Edit']);
	}

	$SQL =' SELECT Daten.* ';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM (';
	$SQL .= ' 	SELECT KennwoerterZugriffe.*, COALESCE(XBN_NAME, KZG_BEZEICHNUNG) AS XXX_NAME, CASE WHEN XBN_NAME IS NULL THEN \'G\' ELSE \'B\' END AS XXX_TYP';
	$SQL .= ' FROM KennwoerterZugriffe ';
	$SQL .= ' LEFT OUTER JOIN Benutzer ON KEZ_XXX_KEY = XBN_KEY';
	$SQL .= ' LEFT OUTER JOIN KennwoerterGruppen ON KEZ_XXX_KEY = KZG_KEY';
	$SQL .= ' WHERE KEZ_KEW_KEY ='.$AWIS_KEY1;
	if($AWIS_KEY2!=0)
	{
		$SQL .= ' AND KEZ_KEY = '.$AWIS_KEY2;
	}
	$SQL .= ') DATEN';

//$Form->DebugAusgabe(1,$SQL);

		// Aktuellen Block NICHT speichern-> gibt Probleme beim Hin-und-Her wechseln!!
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			//$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_KEWKEZ',serialize($Param));
		}
		elseif(isset($Param['BLOCK']) AND $Param['XXX_KEY']==$AWIS_KEY1)
		{
			//$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		if($Block==0)
		{
			$Block=1;
		}
		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else

	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsKEZ = $DB->RecordSetOeffnen($SQL);
//$Form->DebugAusgabe(1,$SQL);
	$Param['XXX_KEY']=$AWIS_KEY1;
	$AWISBenutzer->ParameterSchreiben('Formular_KEWKEZ',serialize($Param));

	if($rsKEZ->AnzahlDatensaetze()>1 OR $AWIS_KEY2==0 OR isset($_GET['KEZListe']))						// Liste anzeigen
	{
		// Liste aufl�sen, um Zugriffe zu vermeiden
		foreach($JaNeinListe AS $Text)
		{
			$Text = explode('~',$Text);
			$JaNeinListeArr[$Text[0]]=$Text[1];
		}

		$DetailAnsicht = false;

		$Form->Formular_Start();

		$Form->ZeileStart();
		$Icons = array();
		if(($Recht9200&2)==2)
		{
			$Icons[] = array('new','/kennwoerter/kennwoerter_Main.php?cmdAktion=Details&'.(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'').'&KEW_KEY='.$AWIS_KEY1.'&Edit=-1','g');
		}
		$Form->Erstelle_ListeIcons($Icons,38,0);

		$Link = './kennwoerter_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=XXX_NAME'.((isset($_GET['SSort']) AND ($_GET['SSort']=='XXX_NAME'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KEZ']['KEZ_XXX_KEY'],324,'',$Link);

		$Link = './kennwoerter_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&SSort=KEZ_AENDERN'.((isset($_GET['SSort']) AND ($_GET['SSort']=='KEZ_AENDERN'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KEZ']['KEZ_AENDERN'],100,'',$Link);
		$Form->ZeileEnde();

		$DS=0;
		while(!$rsKEZ->EOF())
		{
			$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');

			$Icons = array();
			if(($Recht9200&4)==4)
			{
				$Icons[] = array('edit','../kennwoerter/kennwoerter_Main.php?cmdAktion=Details&Seite=Zugriffe&KEW_KEY='.$rsKEZ->FeldInhalt('KEZ_KEW_KEY').'&Edit='.$rsKEZ->FeldInhalt('KEZ_KEY'));
			}
			if(($Recht9200&8)==8)
			{
				$Icons[] = array('delete','./kennwoerter_Main.php?cmdAktion=Details&Seite=Zugriffe&Del='.$rsKEZ->FeldInhalt('KEZ_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenBild(awisFormular::BILD_TYP_OHNEAKTION,'Typ','',($rsKEZ->FeldInhalt('XXX_TYP')=='B'?'/bilder/icon_mensch.png':'/bilder/icon_gruppe.png'),'',($DS%2),'',16,16,20);
			$Form->Erstelle_ListenFeld('XXX_NAME',$rsKEZ->FeldInhalt('XXX_NAME'),0,300,false,($DS%2),'','','T','L');
			$Form->Erstelle_ListenFeld('KEZ_AENDERN',$JaNeinListeArr[$rsKEZ->FeldInhalt('KEZ_AENDERN')],0,100,false,($DS%2),'','','T','L','');
			$Form->ZeileEnde();

			$rsKEZ->DSWeiter();
			$DS++;
		}

		$Link = './kennwoerter_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}
	else			// Deails zu einer Filiale
	{
		$Form->Formular_Start();

		$EditModus = ($Recht9200&6);

		$Form->Erstelle_HiddenFeld('KEZ_KEY',$rsKEZ->FeldInhalt('KEZ_KEY'));
		$Form->Erstelle_HiddenFeld('KEZ_KEW_KEY',$AWIS_KEY1);
			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./kennwoerter_Main.php?cmdAktion=Details&KEZListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKEZ->FeldInhalt('KEZ_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKEZ->FeldInhalt('KEZ_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEZ']['KEZ_XXX_KEY'].':',150,'');
		$SQL = 'SELECT XBN_KEY, XBN_NAME, \'Benutzer\'';
		$SQL .= ' FROM Benutzer ';
		$SQL .= ' WHERE XBN_FILIALEN IS NULL';
		$SQL .= ' UNION ';
		$SQL .= ' SELECT KZG_KEY, \' \' || KZG_BEZEICHNUNG, \'Gruppe\'';		// Leerzeichen, damit oben in der Liste!
		$SQL .= ' FROM KennwoerterGruppen ';
		$SQL .= ' WHERE KZG_XBN_KEY = '.$AWISBenutzer->BenutzerID();
		$SQL .= ' ORDER BY XBN_NAME';
		$Form->Erstelle_SelectFeld('KEZ_XXX_KEY',$rsKEZ->FeldInhalt('KEZ_XXX_KEY'),150,$EditModus,$SQL,'~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen']);
		$AWISCursorPosition='txtKEZ_XXX_KEY';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEZ']['KEZ_AENDERN'].':',150,'');
		$Form->Erstelle_SelectFeld('KEZ_AENDERN',$rsKEZ->FeldInhalt('KEZ_AENDERN'),100,$EditModus,'','','','','',$JaNeinListe);
		$Form->ZeileEnde();

		$Form->Formular_Ende();
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>