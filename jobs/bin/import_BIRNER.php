<?php
require_once('/daten/include/awisZukauf_BIRNER.inc');
require_once('/daten/include/awisJobVerwaltung.inc');

try
{
    $Benutzer = '';
    $DebugLevel = 0;
    $JobId = 0;
    $StartTag = '';
    $EndTag = '';
    $WANummer = '';
    $Abfrage = 'bestellung';
    $Bestaetigen = true;
    $DebugModus = false;


    for($i=1;$i<$argc;$i++)
    {
            switch (strtolower($argv[$i]))
            {
                    case '--benutzer':
                            $Benutzer = $argv[++$i];
                            break;
                    case '--debuglevel':
                            $DebugLevel = $argv[++$i];
                            break;
                    case '--wanummer':
                            $WANummer = $argv[++$i];
                            break;
                    case '--jobid':
                            $JobId = $argv[++$i];
                            break;
                    case '--starttag':
                            $StartTag = $argv[++$i];
                            break;
                    case '--endtag':
                            $EndTag = $argv[++$i];
                            break;
                    case '--abfrage':
                            $Abfrage = strtolower($argv[++$i]);
                            break;
                    case '--bestaetige':
                        $Bestaetige = strtolower($argv[++$i]);
                        $Abfrage = 'bestaetige';
                        $BestaetigeExtId = $argv[++$i];
                        break;
                    case '--bestaetigen':
                        $Bestaetigen = strtolower($argv[++$i]);
                        $BestaetigeExtId = $argv[++$i];
                        break;
                    case '--debugmodus':
                        $DebugModus = true;
                        break;
                    default:
                            echo 'Parameter '.$argv[$i].' ist unbekannt.';
                    case '--help':
                            echo $argv[0].PHP_EOL;
                            echo 'Optionen:'.PHP_EOL;
                            echo '  --benutzer <Name>                  zu verwendender Benutzer fuer den Import.'.PHP_EOL;
                            echo '  --debuglevel <Nr>                  Debuglevel (0-10).'.PHP_EOL;
                            echo '  --jobid  <Nr>                      JobId fuer die Protokollierung.'.PHP_EOL;
                            echo '  --wanummer  <Nr>                   WANummer fuer die Abfrage.'.PHP_EOL;
                            echo '  --starttag  <TT.MM.YY>             Beginn des ersten Abrufs.'.PHP_EOL;
                            echo '  --endtag  <TT.MM.YY>               Ende des Abrufs.'.PHP_EOL;
                            echo '  --abfrage  [Abfragetyp]            Art des Abrufs, Werte: Bestellung, Lieferschein oder Rechnung.'.PHP_EOL;
                            echo '  --bestaetigen  [1,0]               Sollen die abgeholten Datens�tze best�tigt werden?'.PHP_EOL;
                            echo '  --bestaetige [Bestellung, Lieferschein, Rechnung] <ExterneId> Best�tigt die ExterneId';
                            echo PHP_EOL;
                            die();
            }
    }


    $AWISBenutzer = awisBenutzer::Init($Benutzer);

    $Form = new awisFormular();
    $DB = awisDatenbank::NeueVerbindung('AWIS');

    $ZUK = new awisZukauf_BIRNER($DebugLevel, $Bestaetigen);

    if($DebugModus){
        $ZUK->DebugModus();
    }

    switch($Abfrage)
    {
        case 'bestellunglieferschein':
            if($ZUK->LeseBestellungen($WANummer,'','',($WANummer!=''?'A':''))>0)
            {
                $ZUK->SpeichereBestellungen();
            }
            if($ZUK->LeseLieferscheine($WANummer,'','',($WANummer!=''?'A':''))>0)
            {
                $ZUK->SpeichereLieferscheine();
            }
            break;
        case 'bestellung':
            if($ZUK->LeseBestellungen($WANummer,'','',($WANummer!=''?'A':''))>0)
            {
                $ZUK->SpeichereBestellungen();
            }
            break;
        case 'lieferschein':
            if($ZUK->LeseLieferscheine($WANummer,'','',($WANummer!=''?'A':''))>0)
            {
                $ZUK->SpeichereLieferscheine();
            }
            break;
        case 'rechnung':
            $Weiterlesen = true;
            $MaxDurchlaeufe = 20; //Maximal 20 mal laufen

            while($Weiterlesen and ($MaxDurchlaeufe-- > 0)){
                if($ZUK->LeseRechnungen($WANummer,$StartTag,$EndTag,($WANummer!=''?'A':''),'')>0)
                {
                    $ZUK->SpeichereRechnungen();
                    if($ZUK->AnzRechnungZumBestaetigen() > 0){
                        $Erg = $ZUK->BestaetigeAbholung();
                    }else{
                        $Weiterlesen = false;
                    }
                }
            }
            break;
        case 'bestaetige':
            switch ($Bestaetige){
                case 'bestellung':
                    $ZUK->BestaetigeBestellung($BestaetigeExtId);
                    break;
                case 'lieferschein':
                    $ZUK->BestaetigeLieferschein($BestaetigeExtId);
                    break;
                case 'rechnung':
                    $ZUK->BestaetigeRechnung($BestaetigeExtId);
                    break;
            }
            break;
    }


    if($Bestaetigen){
        $ZUK->BestaetigeAbholung();
    }

}
catch (awisException $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'SQL: '.$ex->getSQL().PHP_EOL;
}
catch (Exception $ex)
{
    echo 'FEHLER: '.$ex->getMessage().PHP_EOL;
    echo 'FEHLERZeile: '.$ex->getLine().PHP_EOL;
}
?>

