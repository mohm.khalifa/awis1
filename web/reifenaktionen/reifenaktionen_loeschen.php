<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try 
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();
	$Form->DebugAusgabe(1,'l�schen:');
	$Form->DebugAusgabe(1,$_POST);
	
	$Tabelle= '';
	
	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtRRA_KEY']))
		{
			$Tabelle = 'RRA';
			$Key=$_POST['txtRRA_KEY'];
	
			$SQL = 'SELECT RRA_KEY, RRA_AKTIONSNAME';
			$SQL .= ' FROM REIFENAKTIONEN ';
			$SQL .= ' WHERE RRA_KEY=0'.$Key;
			
			$rsDaten = $DB->RecordsetOeffnen($SQL);
			$Form->DebugAusgabe(1,$DB->LetzterSQL());
				
			$HauptKey = $rsDaten->FeldInhalt('RRA_KEY');
				
			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('RRA','RRA_AKTIONSNAME'),$rsDaten->FeldInhalt('RRA_AKTIONSNAME'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		switch($_GET['Seite'])
		{
			case 'Reifenaktionen':
				$Tabelle = 'RRA';
				$Key=$_GET['Del'];
	
				$SQL = 'SELECT RRA_KEY, RRA_AKTIONSNAME';
				$SQL .= ' FROM V_REIFENPFLEGE ';
				$SQL .= ' WHERE RRA_KEY=0'.$Key;
												
				$rsDaten = $DB->RecordsetOeffnen($SQL);
				$HauptKey = $rsDaten->FeldInhalt('RRA_KEY');
				
				$Felder=array();
				$Felder[]=array($Form->LadeTextBaustein('RRA','RRA_AKTIONSNAME'),$rsDaten->FeldInhalt('RRA_AKTIONSNAME'));
				break;
			default:
				break;
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchf�hren
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'RRA':
				$SQL = 'DELETE FROM REIFENAKTIONEN ';
				$SQL .= ' WHERE RRA_KEY=0'.$_POST['txtKey'];
				$DB->Ausfuehren($SQL,'',true);
				$Form->DebugAusgabe(1,$DB->LetzterSQL());
				$AWIS_KEY1='';
				break;		
			default:
				break;
		}
	}
	
	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);
	
		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./reifenaktionen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').' method=post>');
	
		$Form->Formular_Start();
		
		$Form->ZeileStart();		
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);	
		$Form->ZeileEnde();
	
		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',200);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}
	
		$Form->Erstelle_HiddenFeld('HauptKey',$HauptKey);
/*
		if(isset($SubKey))
		{
			$Form->Erstelle_HiddenFeld('SubKey',$SubKey);			// Bei den Unterseiten
		}
*/
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);
		
		$Form->Trennzeile();
	
		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();
	
		$Form->SchreibeHTMLCode('</form>');
	
		$Form->Formular_Ende();
	
		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>