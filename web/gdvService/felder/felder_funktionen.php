<?php

/**
 * Created by PhpStorm.
 * User: gebhardt_p
 * Date: 18.07.2016
 * Time: 08:17
 */
class felder_funktionen
{

    private $_DB;

    function __construct()
    {
        $this->_DB = awisDatenbank::NeueVerbindung('AWIS');
        $this->_DB->Oeffnen();
    }

    /**
     * Diese Funktion geht leider ein wenig am AWIS Standard vorbei, macht jedoch Sinn, da sonst
     * immer und immer wieder der "selbe" SQL auftauchen w�rde.
     *
     * returns awisRecordset||SQL
     */
    const RETURNTYP_RECORDSET = 1;
    const RETURNTYP_SQL = 2;
    const TYP_NACHRICHTENTYP = 1;
    const TYP_SATZARTEN = 2;
    const TYP_FELDER = 3;
    //Konstanten f�r Bedingungen um Schreibfehler zu vermeiden...
    const Bedingung_GDN_NUMMER = 'GDN_NUMMER';
    const Bedingung_VVE_VERSNR = 'VVE_VERSNR';
    const Bedingung_GDF_KEY = 'GDF_KEY';
    const Bedingung_GDS_NUMMER = 'GDS_NUMMER';

    public function holeFelderDetailDaten($Typ = '', $ReturnTyp, $Bedingung = array())
    {
        global $AWIS_KEY1;
        global $AWIS_KEY2;

        $SQL = 'SELECT a.VVE_VERSNR,';
        $SQL .= '   B.VVW_RELEASE,';
        $SQL .= '   C.VNT_GDN_NUMMER,';
        $SQL .= '   D.VSA_GDS_NUMMER,';
        $SQL .= '   E.GDR_KEY,';
        $SQL .= '   F.GDS_KEY,';
        $SQL .= '   F.GDS_NUMMER,';
        $SQL .= '   F.GDS_BEZEICHNUNG,';
        $SQL .= '   G.GDF_KEY,';
        $SQL .= '   G.GDF_BEZEICHNUNG,';
        $SQL .= '   G.GDF_TYP,';
        $SQL .= '   G.GDF_XMLTYP,';
        $SQL .= '   G.GDF_PFLICHT,';
        $SQL .= '   I.GDN_BEZEICHNUNG';
        $SQL .= ' FROM VERSVERSICHERUNGEN a';
        $SQL .= ' INNER JOIN VERSVERSANDWEG B';
        $SQL .= ' ON a.VVE_VERSNR = B.VVW_VERSNR';
        $SQL .= ' INNER JOIN VERSNACHRICHTENTYPEN C';
        $SQL .= ' ON a.VVE_VERSNR = C.VNT_VERSNR';
        $SQL .= ' INNER JOIN GDVRELEASE E';
        $SQL .= ' ON E.GDR_RELEASE = B.VVW_RELEASE';
        $SQL .= ' INNER JOIN GDVSATZARTEN F';
        $SQL .= ' ON F.GDS_GDR_KEY = E.GDR_KEY';
        $SQL .= ' INNER JOIN VERSSATZARTEN D';
        $SQL .= ' ON a.VVE_VERSNR      = D.VSA_VERSNR';
        $SQL .= ' AND C.VNT_GDN_NUMMER = D.VSA_GDN_NUMMER';
        $SQL .= ' AND D.VSA_GDS_NUMMER = F.GDS_NUMMER';
        $SQL .= ' INNER JOIN GDVFELDER G';
        $SQL .= ' ON G.GDF_GDS_KEY = F.GDS_KEY';
        $SQL .= ' inner join GDVNACHRICHTENTYPEN i';
        $SQL .= ' on I.GDN_NUMMER = VSA_GDN_NUMMER ';

        $SQL .= ' WHERE 1=1';

        if ($AWIS_KEY1 > 0) {
            $SQL .= ' AND VVE_VERSNR = ';
            if ($ReturnTyp = $this::RETURNTYP_SQL) { //Keine BV benutzen, wenn Return ein SQL sein soll
                $SQL .= $AWIS_KEY1;
            } else {
                $this->_DB->WertSetzen('HDF', 'N0', $AWIS_KEY1);
            }
        }

        if ($AWIS_KEY2 > 0) {
            $SQL .= ' AND GDF_KEY = ';
            if ($ReturnTyp = $this::RETURNTYP_SQL) { //Keine BV benutzen, wenn Return ein SQL sein soll
                $SQL .= $AWIS_KEY2;
            } else {
                $this->_DB->WertSetzen('HDF', 'N0', $AWIS_KEY1);
            };
        }

        //Ggf. Subselect herumbauen.
        switch ($Typ) {
            case '':
                break;
            case $this::TYP_NACHRICHTENTYP:
                $SQL = 'Select distinct VNT_GDN_NUMMER, VNT_GDN_NUMMER||\'~\'||GDN_BEZEICHNUNG from (' . $SQL;
                $SQL .= ')';
                $SQL .= ' order by VNT_GDN_NUMMER';
                break;
            case $this::TYP_SATZARTEN:
                $SQL = 'Select distinct GDS_NUMMER, GDS_BEZEICHNUNG from (' . $SQL;
                $SQL .= $this->_FelderBedingungen($Bedingung, $ReturnTyp);
                $SQL .= ')';
                $SQL .= ' order by GDS_NUMMER';
                break;
            case $this::TYP_FELDER:
                $SQL = 'Select distinct GDF_KEY, GDF_BEZEICHNUNG from (' . $SQL;
                $SQL .= $this->_FelderBedingungen($Bedingung, $ReturnTyp);
                $SQL .= ')';
                $SQL .= ' order by GDF_KEY';
                break;
        }

        switch ($ReturnTyp) {
            case $this::RETURNTYP_RECORDSET:
                return $this->_DB->RecordSetOeffnen($SQL);
                break;
            case $this::RETURNTYP_SQL;
                return $SQL;
        }
    }

    private function _FelderBedingungen($Bedingungen = array(), $ReturnTyp)
    {
        $Return = '';

        foreach ($Bedingungen as $TabellenFeld => $Bedingung) {
            $Return .= ' AND ' . $TabellenFeld . ' = ';
            if ($ReturnTyp == $this::RETURNTYP_SQL) {
                $Return .= $this->_DB->FeldInhaltFormat('T', $Bedingung);
            } else {
                $Return .= $this->_DB->WertSetzen('HDF', 'T', $Bedingung);
            }
        }

        return $Return;
    }

    public function setzeDisplay($Wert)
    {
        if ($Wert == '') {
            return 'display: none';
        } else {
            return;
        }
    }

}