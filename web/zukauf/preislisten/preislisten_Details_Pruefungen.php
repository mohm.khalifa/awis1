<?php
global $AWIS_KEY2;
global $AWIS_KEY2;
/**
 * @class preislisten_funktionen
 */
global $ZPL;
$DetailAnsicht = false;
$Speichern = false;
//********************************************************
// AWIS_KEY2 setzen
//********************************************************
$AWIS_KEY2 = '';
if (isset($_GET['ZPP_KEY'])) {
    $AWIS_KEY2 = $_GET['ZPP_KEY'];
} elseif (isset($_POST['txtZPP_KEY'])) {
    $AWIS_KEY2 = $_POST['txtZPP_KEY'];
}

if (isset($_POST['cmdDSNeu_x'])) { //Neuanlage?
    $AWIS_KEY2 = -1;
    $_POST = array(); //Falls in nem bestehenden Datensatz auf "Neu" geklickt wurde, muss der POST geleert werden, da diese dann in die Felder geschrieben werden
}
if($AWIS_KEY2!=''){
    $ZPL->Param['KEY2'] = $AWIS_KEY2; //Key als Parameter wegschreiben, f�r den Fall, dass der User den Reiter wechselt..
}



//********************************************************
// SQL der Seite
//********************************************************
$SQL  ='SELECT ';
$SQL .='  ZPP_KEY,';
$SQL .='   ZPP_MASKE,';
$SQL .='   ZPP_PROGRAMM,';
$SQL .='   ZPP_BEZEICHNUNG,';
$SQL .='   ZPP_BESCHREIBUNG,';
$SQL .='   ZPP_STATUS,';
$SQL .='   ZPP_USER,';
$SQL .='   ZPP_USERDAT';
$SQL .=' FROM ZUKAUFPREISLISTENPRUEFPROGS ';
$SQL .=' WHERE ZPP_STATUS = 1 ';

if ($AWIS_KEY2 != '') {
    $SQL .= ' AND ZPP_KEY = ' . $ZPL->DBi->WertSetzen('ZPP','Z',$AWIS_KEY2);
}
$SQL .= ' ORDER BY ZPP_SORT asc';

//********************************************************
// Fertigen SQL ausf�hren
//********************************************************
$rsZPP = $ZPL->DBi->RecordsetOeffnen($SQL, $ZPL->DBi->Bindevariablen('ZPP', true));
$ZPL->Form->DebugAusgabe(1, $ZPL->DB->LetzterSQL());

//********************************************************
// Anzeige Start
//********************************************************

if($AWIS_KEY2!=''){
    if ($rsZPP->FeldInhalt('ZPP_KEY') != '') {
        $AWIS_KEY2 = $rsZPP->FeldInhalt('ZPP_KEY');
    }

    $Speichern = true;
    $ZPL->Form->Erstelle_HiddenFeld('ZPP_KEY', $AWIS_KEY2);

    $Prog = explode(':',$rsZPP->FeldInhalt('ZPP_PROGRAMM'));
    $Typ = $Prog[0];
    $Programm = $Prog[1];
    $ZPL->Form->ZeileStart();
    $ZPL->Form->Erstelle_TextLabel($rsZPP->FeldInhalt('ZPP_BEZEICHNUNG'),300,'text-align:center;width: 100%;font-weight: bold;font-size: 12pt;');
    $ZPL->Form->ZeileEnde();
    $Felder = array();
    $Felder[] = array(
        'Style' => 'font-size:smaller;',
        'Inhalt' => "<a href=./preislisten_Main.php?cmdAktion=Details&Seite=Pruefungen accesskey=T title='Zur�ck zu den Pr�fprogrammen'><img border=0 src=/bilder/cmd_trefferliste.png></a>"
    );
    $ZPL->Form->InfoZeile($Felder, '');
    switch ($Typ){
        case 'SQL':
            require_once 'preislisten_SQLProgramm.php';
            break;
        case 'PHP':
           if(is_file($Programm)){
               // Infozeile zusammenbauen


               include $Programm;
           }else{
               $ZPL->Form->Hinweistext('Pr�fprogramm nicht gefunden!',awisFormular::HINWEISTEXT_FEHLER);
           }
            break;
    }

    $EditRecht = (($ZPL->Recht10071 & 2) != 0);
    
    
}else{
    $FeldBreiten = array();
    $FeldBreiten['ZPP_BEZEICHNUNG'] = 200;
    $FeldBreiten['ZPP_BESCHREIBUNG'] = 500;
    $FeldBreiten['ZPP_AKTIONEN'] = 150;

    $ZPL->Form->ZeileStart();
    $Link = '';
    $ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPP']['ZPP_BEZEICHNUNG'], $FeldBreiten['ZPP_BEZEICHNUNG'], '', $Link);
    $ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPP']['ZPP_BESCHREIBUNG'], $FeldBreiten['ZPP_BESCHREIBUNG'], '', $Link);
    $ZPL->Form->Erstelle_Liste_Ueberschrift($ZPL->AWISSprachKonserven['ZPP']['ZPP_AKTIONEN'], $FeldBreiten['ZPP_AKTIONEN'], '', $Link);
    $ZPL->Form->ZeileEnde();

    $DS = 0;
    while (!$rsZPP->EOF()) {
        $HG = ($DS % 2);
        $ZPL->Form->ZeileStart();
        $Link = './preislisten_Main.php?cmdAktion=Details&ZPP_KEY=0' . $rsZPP->FeldInhalt('ZPP_KEY') . (isset($_GET['BLOCK2'])?'&Block=' . intval($_GET['BLOCK2']):'') . (isset($_GET['Seite'])?'&Seite=' . ($_GET['Seite']):'');
        $ZPL->Form->Erstelle_ListenFeld('ZPP_BEZEICHNUNG', $rsZPP->FeldInhalt('ZPP_BEZEICHNUNG'), 0, $FeldBreiten['ZPP_BEZEICHNUNG'], false, $HG, '', $Link, 'T', 'L',$rsZPP->FeldInhalt('ZPP_BEZEICHNUNG'));
        $ZPL->Form->Erstelle_ListenFeld('ZPP_BESCHREIBUNG', $rsZPP->FeldInhalt('ZPP_BESCHREIBUNG'), 0, $FeldBreiten['ZPP_BESCHREIBUNG'], false, $HG, '', '', 'T', 'L',$rsZPP->FeldInhalt('ZPP_BESCHREIBUNG'));
        $ZPL->Form->Erstelle_ListenFeld('ZPP_STATUS', $rsZPP->FeldInhalt('ZPP_STATUS'), 0, $FeldBreiten['ZPP_AKTIONEN'], false, $HG, '', '', 'T', 'L',$rsZPP->FeldInhalt('ZPP_STATUS'));

        $ZPL->Form->ZeileEnde();

        $rsZPP->DSWeiter();
        $DS++;
    }

}

?>