<?php
require_once "awisVCalendar.inc";
require_once "awisSeminarplanEmail.php";
try {
    $AWISBenutzer = awisBenutzer::Init();
    $AWISDB = awisDatenbank::NeueVerbindung('AWIS');
    $AWISDB->Oeffnen();
    $Form = new awisFormular();

    $awisVCalendar = new awisVCalendar();

    $AWIS_KEY1 = $_POST["txtSEP_KEY"];

    $SQL="";
    $SQL.="Select a.*, b.*,c.* from ";
    $SQL.=" SEMINARTEILNEHMER a";
    $SQL.=" LEFT JOIN SEMINARPLAN b";
    $SQL.=" ON a.SET_SEP_KEY = b.SEP_KEY";
    $SQL.=" LEFT JOIN SEMINARRAEUME c";
    $SQL.=" ON b.SEP_SER_KEY = c.SER_KEY";
    $SQL.=" WHERE";
    $SQL.=" a.SET_SEP_KEY = ".$AWISDB->WertSetzen('SEK','Z',$AWIS_KEY1);
    $SQL.=" AND a.SET_STATUSTEILNEHMER != 3";

    $rsSET = $AWISDB->RecordSetOeffnen($SQL,$AWISDB->Bindevariablen('SEK'));

    $awisSeminarplanEmail = new awisSeminarplanEmail($rsSET->FeldInhalt('SET_KEY')); //Wird nur zur Platzhalterersetzung verwendet
    $SQL = "SELECT * FROM MAILVERSANDTEXTE";
    $SQL .= " WHERE MVT_BEREICH = 'SEK_KALENDER_EINLADUNG'";
    $rsText = $AWISDB->RecordSetOeffnen($SQL);
    $Text = $awisSeminarplanEmail->ErsetzePlatzhalter($rsText->FeldInhalt('MVT_TEXT'));
    $Betreff = $awisSeminarplanEmail->ErsetzePlatzhalter($rsText->FeldInhalt('MVT_BETREFF'));
    $Terminvon = $rsSET->FeldInhalt('SEP_VON');
    $Terminbis = $rsSET->FeldInhalt('SEP_BIS');
    $Ort = ladeOrt($AWISDB,$rsSET->FeldInhalt('SEP_KEY'));
    $Teilnehmerliste = ladeTeilnehmerListe($_POST['txtMailEmpfaenger']);

    //Kalender-Objekt erstellen und anschliessend Downloaden
    $awisVCalendar->erstelleVCalendarObjekt($AWISBenutzer->EMailAdresse(),$Teilnehmerliste,'',$Terminvon,$Terminbis,$Betreff,$Text,2,$Ort);
    $rsSET->DSErster(); //Wieder ersten Datensatz nehmen sonst gibt es Probleme
    $awisVCalendar->downloadVCalendar("TerminSEP_".strval($rsSET->FeldInhalt('SEP_KEY')).".ics");
}
catch (awisException $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
catch (Exception $ex)
{
    $Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}

/**
 * @param String $Empfaenger Empfaengerliste aus Post
 * @return array
 */
function ladeTeilnehmerListe($Empfaenger) {

    $Teilnehmerliste = array();
    $PostListe = explode(';',$Empfaenger);

    foreach ($PostListe as $Teilnehmer) {
        $Teilnehmer = trim($Teilnehmer);
        if(preg_match('/.*@.*\..*/',$Teilnehmer)){
            $Teilnehmerliste[] = $Teilnehmer;
        }
    }

    return array_unique($Teilnehmerliste);
}

function ladeOrt($AWISDB,$SEP_KEY){

    $SQL = "";
    $SQL .= " SELECT spa_lokationsname || ' | ' || spa_strasse || ' | ' || spa_postleitzahl || ' ' || spa_ort as LOKATION ";
    $SQL .= " FROM AWIS.SEMINARPLAN";
    $SQL .= " INNER JOIN ( ";
    $SQL .= "   select sep_key as spa_sep_key, ";
    $SQL .= "   CASE";
    $SQL .= "       WHEN sep_srg_key = 51 and spa_lokationsname is null THEN 'ATU-Zentrale'";
    $SQL .= "       ELSE spa_lokationsname";
    $SQL .= "   END as spa_lokationsname,";
    $SQL .= "   CASE";
    $SQL .= "       WHEN sep_srg_key = 51 and spa_postleitzahl is null THEN '92637'";
    $SQL .= "       ELSE spa_postleitzahl";
    $SQL .= "   END as spa_postleitzahl,";
    $SQL .= "   CASE";
    $SQL .= "       WHEN sep_srg_key = 51 and spa_ort is null THEN 'Weiden'";
    $SQL .= "       ELSE spa_ort";
    $SQL .= "   END as spa_ort,";
    $SQL .= "   CASE";
    $SQL .= "       WHEN sep_srg_key = 51 and spa_strasse is null THEN 'Dr. Kilian Strasse 11'";
    $SQL .= "       ELSE spa_strasse";
    $SQL .= "   END as spa_strasse";
    $SQL .= "   from seminarraumadressen";
    $SQL .= "   right join seminarraeume";
    $SQL .= "       on ser_raumnr = spa_key";
    $SQL .= "   right join seminarplan";
    $SQL .= "       on sep_ser_key = ser_key";
    $SQL .= " union ";
    $SQL .= "   select sep_key as spa_sep_key, ";
    $SQL .= "   (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = 600";
    $SQL .= "       and SEI_XTN_KUERZEL = 'SEP' and SEI_XXX_KEY = SEP_KEY) as spa_lokationsname,";
    $SQL .= "   (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = 601 ";
    $SQL .= "       and SEI_XTN_KUERZEL = 'SEP' and SEI_XXX_KEY = SEP_KEY) as spa_postleitzahl, ";
    $SQL .= "   (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = 602 ";
    $SQL .= "       and SEI_XTN_KUERZEL = 'SEP' and SEI_XXX_KEY = SEP_KEY) as spa_ort, ";
    $SQL .= "   (select SEI_WERT from SEMINARPLANINFOS where SEI_ITY_KEY = 603 ";
    $SQL .= "       and SEI_XTN_KUERZEL = 'SEP' and SEI_XXX_KEY = SEP_KEY) as spa_strasse ";
    $SQL .= "   from seminarplan ";
    $SQL .= "   left join seminarteilnehmer ";
    $SQL .= "       on set_sep_key = sep_key ";
    $SQL .= "   where sep_key = ".$AWISDB->WertSetzen('SPA', 'Z', $SEP_KEY);
    $SQL .= "   and sep_srg_key = -99";
    $SQL .= " ) ";
    $SQL .= " ON SEP_KEY = SPA_SEP_KEY ";
    $SQL .= " WHERE";
    $SQL .= " sep_key =".$AWISDB->WertSetzen('SPA', 'Z', $SEP_KEY);

    $rsOrt = $AWISDB->RecordSetOeffnen($SQL, $AWISDB->Bindevariablen('SPA'));

    return $rsOrt->FeldInhalt('LOKATION');
}