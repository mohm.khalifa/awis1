<?php
require_once("card/card_db.inc.php");			// DB
require_once("card/card_rechte.inc.php");		// Rechte
require_once("card/card_allgparam.inc.php");	// allg. Parameter
require_once("card/card_zeitanfang.inc.php");	// Zeitmessung
require_once("card/card_fehler.inc.php");		// Funktionen Fehlerhandling
require_once("card/card_funktionen.inc.php");	// versch. Funktionen

?>
<script language="JavaScript">
<?php
if($intZeitmessung)
{
?>
	top.dialogArguments.parent.card_ladezeit.startzeit();	//Zeitmessung
<?php } ?>

</script>
<html>
<head>
<title>Doppelte Rechnungsnummern</title>
<?php include("card/css/css_card_master.php");	//Style-Sheets ?>

<script src="../javascript/card_allgemeinefunktionen.js" type="text/javascript">
</script>


<?php 

//Rechnungsnummern ermitteln

$strDb = "ora";

$strSql="select rec_rnr, nvl(firma,name || ' ' || vorname) name, plz, ort
		from rechnungen a, adressen_snap b
		where a.rec_kunden_nr = b.kunden_nr and 
		substr(rec_rnr,1,6)='$strRec_rnr' 
		order by rec_rnr";

$strQueryString = "strRec_rnr=".$strRec_rnr;

include("card/card_blaettern_haupt.inc.php");	//Bl�ttern 

?>
<script language="JavaScript">
function init()
{
	<?php
	if($intZeitmessung)
	{
	?>
		top.dialogArguments.parent.card_ladezeit.endezeit();	//Zeitmessung
	<?php } ?>
	
	document.ondblclick = zoom;		//Zoomfenster
}

//Wird bei Doppelklick auf das Feld mit der Rechnungsnr ge�ffnet, d.h. �bermittelt die Rechnungsnr nach card_haupt.php

function auswahlrgnr(strRechnungsnr)
{
	<?php
	if($intZeitmessung)
	{
	?>
		top.dialogArguments.parent.card_ladezeit.sendezeit("Auswahl Liste doppelte Rechnungsnummern");	//Zeitmessung
	<?php } ?>
		
	parent.window.returnValue = strRechnungsnr;
	parent.self.close();
}
</script>

</head>
<body onload="init();">
<table cellspacing="0" cellpadding="0" width="100%" height="100%">
<tr><td align="center" valign="top">
<br>
Doppelte Rechnungsnr: <span class="titel"><?php echo $strRec_rnr; ?></span>&nbsp;
<br>
Datens&auml;tze: <span class="titel"><?php echo $strPosition; ?></span><br><br>
<?php 
$arrRechnungen = abfrage("ora",$strSqlScroll); 

if($arrRechnungen["trefferarray"])
{
?>
	<table cellspacing="0" cellpadding="0">
	<tr>
	<td>&nbsp;</td><td class="titel">Rechnungsnr:</td><td class="titel">Name:</td><td class="titel">PLZ:</td><td class="titel">Ort:</td>
	</tr>
	<?php
		$arrHilf = $arrRechnungen["trefferarray"];
		
		for($i=0;$i<$arrRechnungen["zeilen"];$i++)
		{
			echo "<tr>";
			echo "<td><img src=\"/bilder/eingabe_ok.png\" class=\"quadrat\" alt=\"Datensatz ausw&auml;hlen\" onclick=\"auswahlrgnr('".$arrHilf["REC_RNR"][$i]."');\"></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"rgnr\" value=\"".$arrHilf["REC_RNR"][$i]."\" readonly></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"name\" value=\"".$arrHilf["NAME"][$i]."\" readonly></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"plz\" value=\"".$arrHilf["PLZ"][$i]."\" readonly></td>";
			echo "<td class=\"text\"><input type=\"text\" class=\"ort\" value=\"".$arrHilf["ORT"][$i]."\" readonly></td>";
			echo "</tr>";
		}
	?>
	</table>
<?php
}
else
{
	echo "<span class=\"titel\">keine Datens&auml;tze gefunden!</span>";
}
?>
</td></tr></table>
</body>
</html>
<?php 
$intDialog = 1;
include("card/card_zeitende.inc.php");			// Zeitmessung	
?>