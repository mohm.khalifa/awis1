<?php
global $AWIS_KEY1;
global $AWIS_KEY2;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

try
{
	$AWISBenutzer = awisBenutzer::Init();
	$AWISDB = awisDatenbank::NeueVerbindung('AWIS');
	$AWISDB->Oeffnen();
	$Form = new awisFormular();

	//***********************************************************************************
	//** Lieferschein �ndern / anlegen
	//***********************************************************************************
	$Felder = explode(';',$Form->NameInArray($_POST, 'txtZUL_',1,1));
	if(count($Felder)>0 AND $Felder[0]!='')
	{
		$TextKonserven[]=array('ZUL','ZUL_%');
		$TextKonserven[]=array('Fehler','err_KeinWert');
		$TextKonserven[]=array('Fehler','err_PruefzifferFalsch');
		$TXT_Speichern = $Form->LadeTexte($TextKonserven);
		$AWIS_KEY1 = $_POST['txtZUL_KEY'];

		$rsZUL = $DB->RecordSetOeffnen('SELECT * FROM Zukauflieferscheine WHERE ZUL_KEY = '.$DB->FeldInhaltFormat('T',$AWIS_KEY1));

		if($rsZUL->EOF())
		{
die('Keine NEUEN');
			// Artikel suchen
			$SQL = 'SELECT ZLA_KEY, ZAL_KEY, ZLA_BEZEICHNUNG';
			$SQL .= ' FROM Zukaufartikel';
			$SQL .= ' INNER JOIN Zukaufartikellieferanten ON ZLA_KEY = ZAL_ZLA_KEY AND ZAL_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUL_LIE_NR'],true);
			$SQL .= ' INNER JOIN ZUKAUFHERSTELLERCODES ON ZLA_ZLH_KEY = ZHK_ZLH_KEY AND ZHK_LIE_NR = '.$DB->FeldInhaltFormat('T',$_POST['txtZUL_LIE_NR'],false);
			$SQL .= ' WHERE ZLA_ARTIKELNUMMER = '.$DB->FeldInhaltFormat('TU',$_POST['txtZUL_ARTIKELNUMMER'],false);
			$SQL .= ' AND ZHK_CODE = '.$DB->FeldInhaltFormat('TU',$_POST['txtZUL_HERSTELLER'],false);
			$rsZLA = $DB->RecordSetOeffnen($SQL);

			// Wenn nicht, rausl�schen, dann l�uft er in die Fehler
			if($rsZLA->EOF())
			{
				$_POST['txtZUL_ARTIKELNUMMER'] = '';
			}

			// Bezeichnung aus dem Artikelstamm, wenn sie angegeben wurde
			$_POST['txtZUL_ARTIKELBEZEICHNUNG'] = ($_POST['txtZUL_ARTIKELBEZEICHNUNG']==''?$rsZLA->FeldInhalt('ZLA_BEZEICHNUNG'):$_POST['txtZUL_ARTIKELBEZEICHNUNG']);


			$Fehler = '';
			$Pflichtfelder = array('ZUL_FIL_ID','ZUL_LIE_NR','ZUL_HERSTELLER','ZUL_BESTELLDATUM','ZUL_ARTIKELNUMMER','ZUL_ARTIKELBEZEICHNUNG','ZUL_WANRKORR','ZUL_BESTELLMENGE','ZUL_PREISBRUTTO');
			foreach($Pflichtfelder AS $Pflichtfeld)
			{
				if($_POST['txt'.$Pflichtfeld]=='')	// Name muss angegeben werden
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_KeinWert'].' '.$TXT_Speichern['ZUL'][$Pflichtfeld].'<br>';
				}
			}
			if($_POST['txtZUL_WANRKORR']!='')
			{
				if(!awisWerkzeuge::BerechnePruefziffer($_POST['txtZUL_WANRKORR'],awisWerkzeuge::PRUEFZIIFER_TYP_WERKSTATTAUFTRAG,awisWerkzeuge::PRUEFZIIFER_ERG_PRUEFEN))
				{
					$Fehler .= $TXT_Speichern['Fehler']['err_PruefzifferFalsch'].' '.$TXT_Speichern['ZUL']['ZUL_WANRKORR'].'<br>';
				}
			}

				// Wurden Fehler entdeckt? => Speichern abbrechen
			if($Fehler!='')
			{
				die('<span class=HinweisText>'.$Fehler.'</span>');
			}


			$SQL = 'INSERT INTO Zukauflieferscheine';
			$SQL .= '(ZUL_BESTELLDATUM,ZUL_FIL_ID,ZUL_LIE_NR,ZUL_WANR,ZUL_ZAL_KEY,ZUL_ZLA_KEY,';
			$SQL .= 'ZUL_ARTIKELNUMMER,ZUL_ARTIKELBEZEICHNUNG,ZUL_HERSTELLER,ZUL_VERSANDARTBEZ,';
			$SQL .= 'ZUL_SORTIMENT,ZUL_BESTELLMENGE,ZUL_MENGENEINHEITEN,ZUL_TAUSCHTEIL,ZUL_PREISBRUTTO,';
			$SQL .= 'ZUL_PREISANGELIEFERT,ZUL_ALTTEILWERT,ZUL_EINHEITENPROPREIS,ZUL_WAEHRUNG,';
			$SQL .= 'ZUL_ABSCHLUSSART,ZUL_ABSCHLUSSGRUND,';
			$SQL .= 'ZUL_USER,ZUL_USERDAT';
			$SQL .= ')VALUES(';
			$SQL .= ' ' . $DB->FeldInhaltFormat('DU',$_POST['txtZUL_BESTELLDATUM'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUL_FIL_ID'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_LIE_NR'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_WANRKORR'],true);		// in das richtige Feld speichern!!
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsZLA->FeldInhalt('ZAL_KEY'),true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N0',$rsZLA->FeldInhalt('ZLA_KEY'),true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_ARTIKELNUMMER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_ARTIKELBEZEICHNUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_HERSTELLER'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_VERSANDARTBEZ'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_SORTIMENT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUL_BESTELLMENGE'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_MENGENEINHEITEN'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUL_TAUSCHTEIL'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUL_PREISBRUTTO'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUL_PREISANGELIEFERT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('N3',$_POST['txtZUL_ALTTEILWERT'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUL_EINHEITENPROPREIS'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_WAEHRUNG'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('Z',$_POST['txtZUL_ABSCHLUSSART'],true);
			$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZUL_ABSCHLUSSGRUND'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if($DB->Ausfuehren($SQL)===false)
			{
				self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Speichern einer Bestellung von '.$this->_LIE_NR.'.',$_POST['EXTERNEID']);
				throw new Exception('Fehler beim Speichern einer Bestellung:'.$SQL, 200902101017);
			}
			$SQL = 'SELECT seq_ZUL_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = $DB->RecordSetOeffnen($SQL);
			$ZUL_KEY=$rsKey->Feldinhalt('KEY');
			$AWIS_KEY1=$ZUL_KEY;

			if(isset($ZULZULKEY) AND $ZULZULKEY!=='')
			{
				$SQL = 'UPDATE Zukauflieferscheine SET ZUL_ZUL_KEY = '.$ZUL_KEY;
				$SQL .= ' WHERE ZUL_KEY = '.$ZULZULKEY;

				if($DB->Ausfuehren($SQL,'',true)===false)
				{
					self::_LogEintrag(self::LOGTYP_FEHLER,'Fehler beim Aktualisieren einer verkn�ften Bestellung von '.$this->_LIE_NR.'. ZULKEY:'.$ZULZULKEY.' sollte auf '.$ZUL_KEY.' verknpuepft werden.',$_POST['EXTERNEID']);
				}
			}
		}
		else
		{
			$FehlerListe = array();
			$UpdateFelder = '';
			$Fehler='';

			$rsZUL = $DB->RecordSetOeffnen('SELECT * FROM Zukauflieferscheine WHERE ZUL_key=' . $_POST['txtZUL_KEY'] . '');
			$FeldListe = '';
			foreach($Felder AS $Feld)
			{
				$FeldName = substr($Feld,3);
				if(isset($_POST['old'.$FeldName]))
				{
					// Alten und neuen Wert umformatieren!!
					$WertNeu=$DB->FeldInhaltFormat($rsZUL->FeldInfo($FeldName,'TypKZ'),$_POST[$Feld],true);
					$WertAlt=$DB->FeldInhaltFormat($rsZUL->FeldInfo($FeldName,'TypKZ'),$_POST['old'.$FeldName],true);
					$WertDB=$DB->FeldInhaltFormat($rsZUL->FeldInfo($FeldName,'TypKZ'),$rsZUL->FeldInhalt($FeldName),true);
			//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
					if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
					{
						if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
						{
							$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
						}
						else
						{
							$FeldListe .= ', '.$FeldName.'=';

							if($_POST[$Feld]=='')	// Leere Felder immer als NULL
							{
								$FeldListe.=' null';
							}
							else
							{
								$FeldListe.=$WertNeu;
							}
						}
					}
				}
			}

			if(count($FehlerListe)>0)
			{
				$Meldung = str_replace('%1',$rsZUL->FeldInhalt('ZUL_USER'),$TXT_Speichern['Meldung']['DSVeraendert']);
				foreach($FehlerListe AS $Fehler)
				{
					$FeldName = $Form->LadeTextBaustein(substr($Fehler[0],0,3),$Fehler[0]);
					$Meldung .= '<br>&nbsp;'.$FeldName.': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
				}
				$Form->Fehler_Anzeigen('DSVeraendert',$Meldung,'EingabeWiederholen',-1);
			}
			elseif($FeldListe!='')
			{
				$SQL = 'UPDATE Zukauflieferscheine SET';
				$SQL .= substr($FeldListe,1);
				$SQL .= ', ZUL_user=\''.$AWISBenutzer->BenutzerName().'\'';
				$SQL .= ', ZUL_userdat=sysdate';
				$SQL .= ' WHERE ZUL_key=0' . $_POST['txtZUL_KEY'] . '';

				if($DB->Ausfuehren($SQL)===false)
				{
					throw new awisException('Fehler beim Speichern',807242032,$SQL,2);
				}
			}
		}
	}// Zukaufbestellung




	//*******************************************************
	// Sonderanfragen
	//*******************************************************
	if(isset($_POST['txtZSA_WANR']))
	{
		$SQL = 'INSERT INTO ZukaufSonderanfragen';
		$SQL .= '(ZSA_WANR,ZSA_FIRMA, ZSA_USER,ZSA_USERDAT';
		$SQL .= ')VALUES(';
		$SQL .= ' ' . $DB->FeldInhaltFormat('T',$_POST['txtZSA_WANR'],true);
		$SQL .= ',' . $DB->FeldInhaltFormat('T',$_POST['txtZSA_FIRMA'],true);
		$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
		$SQL .= ',SYSDATE';
		$SQL .= ')';
		if($DB->Ausfuehren($SQL)===false)
		{
			throw new awisException('Fehler beim Speichern',802261339,$SQL,2);
		}
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>