<?php
require_once 'awisBenutzer.inc';
/**
 * Filialberichte_job.php
 *
 * Mit diesem Job werden die EMails bei zu wenig ausgefüllten Filialberichten verschickt
 *
 * @author    Thomas Riedl
 * @copyright ATU Auto-Teile-Unger
 * @version   20110408
 */
$AWISBenutzer = awisBenutzer::Init('gebhardt_p');
require_once('filialberichte_CMail.php');
//require_once 'awisJobVerwaltung.inc';
require_once('awisDatenbank.inc');

try {
    /*
    $Job = new awisJobProtokoll(25,'Starte EMail-Versand.');

    $Job->SchreibeLog(awisJobProtokoll::TYP_START,1,'EMail-Versand beginnt.');
    */
    $CMail = new CMail();
    $CMail->import();
    //$Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,1,'EMail-Versand abgeschlossen.');

} catch (Exception $ex) {
    #	echo PHP_EOL.'Seminarplanimport: Fehler: '.$ex->getMessage().PHP_EOL;

    awisWerkzeuge::EMail(array('shuttle@de.atu.eu'), 'ERROR FILIALBERICHTE', '', 3, '', 'awis@de.atu.eu');
    /*
    if(isset($Job))
    {
        $Job->SchreibeLog(awisJobProtokoll::TYP_ENDE,3,'Fehler:'.$ex->getMessage());
    }
    */
}
?>
