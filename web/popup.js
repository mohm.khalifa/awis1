/* Wir machen verschiedene Ids f�r DialogPopup */
	function Neuids() {
			var el;
			var i;
			var overlay;
			var fenster;
		
				el = document.querySelectorAll('#overlay').length;
	
				for(i = 0; i < el;i++)
				{
					overlay = document.getElementById("overlay");
					overlay.setAttribute( 'id', 'overlay_' + i );
					
					fenster = document.getElementById("fenster");
					fenster.setAttribute( 'id', 'fenster_' + i );
				}
			
     }

	/* popupDialog */
    function DialogPopup(Number) {
		var el;
		
		el = document.getElementById("overlay_" + Number);
			
			//Wenn Popup unsichtbar ist, dann hinzuf�gen wir die Animation
			if(el.style.visibility == 'hidden'){
				var element = document.getElementById("fenster_" + Number);
				element.className = '';
				element.classList.add("animated_in");
				element.classList.add("fensterKlasse");
			}else{
				var element = document.getElementById("fenster_" + Number);
				element.className = '';
				var height = $(document).scrollTop();
				
				el.style.marginTop = height;
				element.classList.add("animated_out");
				element.classList.add("fensterKlasse");
			}
			
	    
			var element = document.getElementById("fenster_" + Number);
			element.classList.add("wiggle");

			
			
			
		
			
			
			//Wenn wir nicht auf Popup klicken, dann Animation hinzuf�gen
			$('html').click(function(e){
				 var ids = "fenster_" + Number;
				if(e.target.id == ids){
					e.preventDefault();
				}else{
					 element.classList.add("shake");
					  setTimeout(function(){
						  element.classList.remove("shake");
						  element.classList.remove("wiggle");
							},1500);
				}
			});
	
			
			
			el.style.visibility = (el.style.visibility == "visible") ? "visible" : "visible";
		
		
    }

/* MasterPopUpOeffnen */
function MasterPopUpOeffnen(Number) {
    var el;

    //Text und Inhalt setzen
    $("#fenster_MasterPopUp .fensterHeaderText").html(eval("MP_Ueberschrift"+Number));
    $("#fenster_MasterPopUp .fensterInhalt").html(eval("MP_Inhalt"+Number));

    //Den Header die richtig Klasse geben
    $("#fenster_MasterPopUp .fensterHeader").removeAttr('class').addClass('fensterHeader');
    $("#fenster_MasterPopUp .fensterHeader").addClass("fensterHeader_"+eval("MP_Klasse"+Number));

    $("#fenster_MasterPopUp .fensterSchaltflaeche").html('');
    var Schalter = '<div class"fensterSchaltflaeche" >';
	$.each(eval("MP_Schaltflaechen"+Number), function(index, value){
        if(value['Typ'] == 'href'){
        	Schalter += '<a class="" href="'  + value['Zusatz'] + '">';
            Schalter += '<div style="vertical-align: baseline;" class=" fensterSchaltflaeche_button FensterButton FensterButton_' + eval("MP_Klasse"+Number) + '" href="'+value['Zusatz']+'" ' + ' accesskey="'  + value['accesskey']  + '" >' + value['Text']  + ' </div>';
            Schalter += '</a>'
		}else if(value['Typ'] == 'post'){//ungetestet!!!!
            Schalter +=  '<input type="image" name="' + value['id'] + '" id="' + value['id'] + '" class="fensterSchaltflaeche_button" src="' + value[0] + '" border="0" alt="' + value[3] + '" />';
        }else if(value['Typ'] == 'close'){
           Schalter += '<div  id="cmdClose" style="vertical-align: baseline;" class="fensterSchaltflaeche_button FensterButton FensterButton_'+ eval("MP_Klasse"+Number)+'" src="' + value[0]  + '" accesskey="'  + value['accesskey']  + '" border="0" alt="' + value[3] + '"onclick="DialogPopup_schliessen(\'MasterPopUp\');" >' + value['Text']+'</div>';
        }else if(value['Typ'] == 'script'){
        Schalter += '<div name="' + value['Id'] + '" id="' + value['Id'] + '" style="vertical-align: baseline;" class="fensterSchaltflaeche_button FensterButton FensterButton_'+ eval("MP_Klasse"+Number)+'" src="' + value[0]  + '" accesskey="'  + value['accesskey']  + '" border="0" onClick=' + value['Zusatz'] + '; " >' + value['Text']+'</div>';
     }
	});
    Schalter += '</div';
    $("#fenster_MasterPopUp .fensterSchaltflaeche").append(Schalter);
    //Den Buttons die richtige Klasse geben
    $("#fenster_MasterPopUp .fensterButton").removeAttr('class').addClass('fensterButton');
    $("#fenster_MasterPopUp .fensterButton").addClass("fensterButton_"+eval("MP_Klasse"+Number));



    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape Taste
            DialogPopup_schliessen('MasterPopUp');
        }
    });


    el = document.getElementById("overlay_MasterPopUp");

    //Wenn Popup unsichtbar ist, dann hinzuf�gen wir die Animation
    if(el.style.visibility == 'hidden'){
        var element = document.getElementById("fenster_MasterPopUp" );
        element.className = '';
        element.classList.add("animated_in");
        element.classList.add("fensterKlasse");
    }else{
        var element = document.getElementById("fenster_MasterPopUp" );
        element.className = '';
        var height = $(document).scrollTop();

        el.style.marginTop = height;
        element.classList.add("animated_out");
        element.classList.add("fensterKlasse");
    }


    var element = document.getElementById("fenster_MasterPopUp" );
    element.classList.add("wiggle");




    //Wenn wir nicht auf Popup klicken, dann Animation hinzuf�gen
    $('html').click(function(e){
        var ids = "fenster_" + Number;
        if(e.target.id == ids){
            e.preventDefault();
        }else{
            element.classList.add("shake");
            setTimeout(function(){
                element.classList.remove("shake");
                element.classList.remove("wiggle");
            },1500);
        }
    });



    el.style.visibility = (el.style.visibility == "visible") ? "visible" : "visible";


}


    //Popup Schlie�enFunktion, alles gleich ist wie bei Anrufen, nur verstecken wir hier
    function DialogPopup_schliessen(Number) {
    	el = document.getElementById("overlay_" + Number);
		if(el.style.visibility == 'visible'){
			var element = document.getElementById("fenster_" + Number);
			element.className = '';
			element.classList.add("animated_out");
			element.classList.add("fensterKlasse");
		}
    	
    	var element = document.getElementById("fenster_" + Number);
    	element.classList.add("bounceOut");
    	element.classList.add("fensterKlasse");
		
		
    	
		setTimeout(function(){
		el.style.visibility = (el.style.visibility == "hidden") ? "visible" : "hidden";
		},1000);
    }
	
    var IEversion = detectIE();

    function detectIE() {
    	  var ua = window.navigator.userAgent;

    	  // Test values; Uncomment to check result …

    	  // IE 10
    	  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
    	  
    	  // IE 11
    	  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
    	  
    	  // IE 12 / Spartan
    	  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
    	  
    	  // Edge (IE 12+)
    	  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    	  var msie = ua.indexOf('MSIE ');
    	  if (msie > 0) {
    	    // IE 10 or older => return version number
    	    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    	  }

    	  var trident = ua.indexOf('Trident/');
    	  if (trident > 0) {
    	    // IE 11 => return version number
    	    var rv = ua.indexOf('rv:');
    	    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    	  }

    	  var edge = ua.indexOf('Edge/');
    	  if (edge > 0) {
    	    // Edge (IE 12+) => return version number
    	    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    	  }

    	  // other browser
    	  return false;
    	}    
    