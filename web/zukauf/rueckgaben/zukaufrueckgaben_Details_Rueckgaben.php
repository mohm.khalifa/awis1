<?php
global $AWISCursorPosition;
global $AWIS_KEY1;
global $AWIS_KEY2;

try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('ZRU','%');
	$TextKonserven[]=array('Liste','ZRU_GRUND');
	$TextKonserven[]=array('Liste','ZRU_STATUS');
	$TextKonserven[]=array('ZAP','ZAP_GUELTIGAB');
	$TextKonserven[]=array('ZAP','ZAP_VKPR');
	$TextKonserven[]=array('ZAP','ZAP_EK');
	$TextKonserven[]=array('LIE','ZRU_MENGE');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','Altteilwert');
	$TextKonserven[]=array('Wort','AktuellesSortiment');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Wort','KeineDatenVorhanden');
	$TextKonserven[]=array('Wort','KeineZuordnungGefunden');
	$TextKonserven[]=array('Liste','lst_AktivInaktiv');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineRechte');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_senden');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();


	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht10050 = $AWISBenutzer->HatDasRecht(10050);
	if(($Recht10050)==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		die();
	}

	$MaxDSAnzahl = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

	if(!isset($_GET['SSort']))
	{
		$ORDERBY = ' ORDER BY ZRU_DATUM DESC';
	}
	else
	{
		$SortFelder = explode(';',$_GET['SSort']);
		$OrderBy = '';
		foreach($SortFelder AS $SortFeld)
		{
			$OrderBy .= ' '.str_replace('~',' DESC ',$_GET['SSort']);
		}
		$ORDERBY = ($OrderBy==''?'':' ORDER BY '.$OrderBy);
	}
	
	$SQL = 'SELECT Zukaufrueckgaben.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM Zukaufrueckgaben ';
	$SQL .= ' WHERE ZRU_ZUL_KEY=0'.$AWIS_KEY1;

	if(isset($_GET['Edit']))
	{
		$AWIS_KEY2 = $DB->FeldInhaltFormat('N0',$_GET['Edit']);
	}
	
	if($AWIS_KEY2>0)
	{
		$SQL .= ' AND ZRU_KEY = 0'.$AWIS_KEY2;
		$_GET['Edit']=$AWIS_KEY2;		// Um sicherzustellen, dass nach dem Anlegen wieder ins Detail gesprungen wird
	}


	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY2<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			//$AWISBenutzer->ParameterSchreiben('Formular_ZUKHIST',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	//$Form->DebugAusgabe(1,$SQL,$MaxDS,$ZeilenProSeite,$Block);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsZRU = $DB->RecordsetOeffnen($SQL);

	$SQL = 'SELECT ZUL_LIEFERMENGE';
	$SQL .= ', (SELECT SUM(ZWA_MENGE) FROM ZUKAUFWERKSTATTAUFTRAEGE WHERE ZWA_ZUB_KEY = ZUL_ZUB_KEY) AS VERKAUFT';
	$SQL .= ', (SELECT SUM(ZRU_MENGE) FROM ZUKAUFRUECKGABEN WHERE ZRU_ZUL_KEY = ZUL_KEY) AS RUECKGABE';
	$SQL .= ' FROM ZUKAUFLIEFERSCHEINE';
	$SQL .= ' WHERE ZUL_KEY = '.$AWIS_KEY1;
    $rsMenge = $DB->RecordSetOeffnen($SQL);
    $OffeneMenge = $rsMenge->FeldInhalt('ZUL_LIEFERMENGE')-$rsMenge->FeldInhalt('VERKAUFT')-$rsMenge->FeldInhalt('RUECKGABE');

    $Form->Formular_Start();

	if($rsZRU->AnzahlDatensaetze()>1 OR isset($_GET['SListe']) OR !isset($_GET['Edit']))
	{
		$Form->ZeileStart();
		$Icons=array();
		if((intval($Recht10050)&6)!=0 AND $OffeneMenge>0)
		{
			$Icons[] = array('new','./zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].'&Seite=Rueckgaben&ZUL_KEY='.$AWIS_KEY1.'&Edit=-1');
  		}

  		$Form->Erstelle_ListeIcons($Icons,38,-1);

  		$Link = './zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].''.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZRU_DATUM'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZRU_DATUM'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZRU']['ZRU_DATUM'],100,'',$Link);
  		$Link = './zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].''.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZRU_MENGE'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZRU_MENGE'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZRU']['ZRU_MENGE'],100,'',$Link);
  		$Link = './zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].''.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'');
		$Link .= '&SSort=ZRU_BESTELLNUMMER'.((isset($_GET['SSort']) AND ($_GET['SSort']=='ZRU_BESTELLNUMMER'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['ZRU']['ZRU_GRUND'],400,'',$Link);
		$Form->ZeileEnde();

			// Blockweise
		$StartZeile=0;
		if(isset($_GET['Block']))
		{
			$StartZeile = intval($_GET['Block'])*$MaxDSAnzahl;
		}

		$Menge = 0;
		$Vorgaenge = 0;

		$DS=0;
		while(!$rsZRU->EOF())
		{
			$Form->ZeileStart();

			$Icons = array();
			if(intval($Recht10050&1)>0)	// Leserecht
			{
				$Icons[] = array('edit','./zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].'&Seite=Rueckgaben&ZUR_KEY='.$AWIS_KEY1.'&Edit='.$rsZRU->FeldInhalt('ZRU_KEY'));
			}
			if(intval($Recht10050&4)>0)	// L�schrecht
			{
				$Icons[] = array('delete','./zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].'&Seite=Rueckgaben&ZUR_KEY='.$AWIS_KEY1.'&Del='.$rsZRU->FeldInhalt('ZRU_KEY'));
			}
			$Form->Erstelle_ListeIcons($Icons,38,($DS%2));

			$Form->Erstelle_ListenFeld('*ZRU_DATUM',$rsZRU->FeldInhalt('ZRU_DATUM'),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZRU_MENGE',substr($rsZRU->FeldInhalt('ZRU_MENGE'),0,20),20,100,false,($DS%2),'','','T');
			$Form->Erstelle_ListenFeld('*ZUR_GRUND',$Form->WerteListe($AWISSprachKonserven['Liste']['ZRU_GRUND'],$rsZRU->FeldInhalt('ZRU_GRUND')),20,400,false,($DS%2),'','','T');
            if($rsZRU->FeldInhalt('ZRU_BEMERKUNG')!='')
            {
                $Form->Erstelle_HinweisIcon('info', 20,($DS%2),$rsZRU->FeldInhalt('ZRU_BEMERKUNG'));
            }
			$Form->ZeileEnde();

			$rsZRU->DSWeiter();
			$DS++;
		}

		$Link = './zukaufrueckgaben_Main.php?cmdAktion='.$_GET['cmdAktion'].'&Seite=Lieferanten';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');
		$Form->Formular_Ende();
		$DetailAnsicht = false;
	}
	elseif($rsZRU->AnzahlDatensaetze()<=1 OR $AWIS_KEY2==-1)
	{
		$EditModus = ($Recht10050&6);
		$AWIS_KEY2 = $rsZRU->FeldInhalt('ZRU_KEY');
		$DetailAnsicht = true;
		
		$Form->Erstelle_HiddenFeld('ZRU_KEY',$AWIS_KEY2);
		$Form->Erstelle_HiddenFeld('ZRU_ZUL_KEY',$AWIS_KEY1);

			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./zukaufrueckgaben_Main.php?cmdAktion=".$_GET['cmdAktion']."&Seite=Lieferanten&SListe=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZRU->FeldInhalt('ZRU_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsZRU->FeldInhalt('ZRU_USERDAT'));
		$Form->InfoZeile($Felder,'');


		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZRU']['ZRU_DATUM'].':',200);
		$Form->Erstelle_TextFeld('!ZRU_DATUM',$rsZRU->FeldInhalt('ZRU_DATUM'),10,200,$EditModus,'','','','D','','',date('d.m.Y'));
		if($EditModus)
		{
			$AWISCursorPosition='txtZRU_DATUM';
		}
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZRU']['ZRU_STATUS'].':',200);
		$Status = explode("|",$AWISSprachKonserven['Liste']['ZRU_STATUS']);
		$Form->Erstelle_SelectFeld('ZRU_STATUS',$rsZRU->FeldInhalt('ZRU_STATUS'),"200:190",false,'','','','','',$Status);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZRU']['ZRU_MENGE'].':',200);
        if($AWIS_KEY2>0)
        {
            $Form->Erstelle_TextFeld('ZRU_MENGE',$rsZRU->FeldInhalt('ZRU_MENGE'),10,100,false,'','','','N0');
        }
        else
        {
    		$Mengen=array();
            // Mengen als Auswahlliste, damit keine Probleme mit Fehleingaben auftreten k�nnen
    		for($i=1;$i<=$OffeneMenge;$i++)
    		{
    		    $Mengen[] = $i.'~'.$i;
    		}
    		$Form->Erstelle_SelectFeld('!ZRU_MENGE',$rsZRU->FeldInhalt('ZRU_MENGE'),100,$EditModus,'','',$OffeneMenge,'','',$Mengen);
        }
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZRU']['ZRU_GRUND'].':',200);
		$JaNein = explode("|",$AWISSprachKonserven['Liste']['ZRU_GRUND']);
		$Form->Erstelle_SelectFeld('!ZRU_GRUND',$rsZRU->FeldInhalt('ZRU_GRUND'),100,$EditModus,'','','','','',$JaNein);
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['ZRU']['ZRU_BEMERKUNG'].':',200);
		$Form->Erstelle_TextArea('!ZRU_BEMERKUNG',$rsZRU->FeldInhalt('ZRU_BEMERKUNG'),600,80,5,$EditModus);
		$Form->ZeileEnde();

		$Form->Formular_Ende();
		
	}
	else
	{
		$Form->Erstelle_TextFeld('#Hinweis',$AWISSprachKonserven['Wort']['KeineDatenVorhanden'],20,500);
	}
	
	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();
	
	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
	
	if(($Recht10050&6)!=0 AND $DetailAnsicht)
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}
	
	if(($Recht10050&6)!=0 AND $DetailAnsicht AND $rsZRU->FeldInhalt('ZRU_STATUS')==1)
	{
		$Form->Schaltflaeche('href', 'cmdSenden', './zukaufrueckgaben_senden.php?ZRU_KEY='.$AWIS_KEY2, '/bilder/cmd_antenne.png', $AWISSprachKonserven['Wort']['lbl_senden'], 'M');
	}
	
	$Form->SchaltflaechenEnde();
	
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181640");
	}
	else
	{
		echo 'AWIS-Fehler:'.$ex->getMessage();
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812181642");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}
?>