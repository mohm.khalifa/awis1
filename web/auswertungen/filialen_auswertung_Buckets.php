<?php
// Variablen
global $_POST;
global $con;
global $AWISBenutzer;
global $awisRSZeilen;

$RechteStufe = awisBenutzerRecht($con, 500);
if(($RechteStufe&2)==0)
{
   awisEreignis(3,1000,'Auswertungen: Filialen',$AWISBenutzer->BenutzerName(),'','','');
//   die("Keine ausreichenden Rechte!");
   die("");
}

// lokale Variablen
// Ende Variablen

//var_dump($_POST);

print "<form name=frmSuche method=post action=./filialen_auswertung_Main.php?cmdAktion=Buckets>";

if(isset($_POST['txtAnzahl']) AND $_POST['txtAnzahl']!='')
{
	if(isset($_POST['txtOffset']) AND $_POST['txtOffset']!='')
	{	
		$offset = '+ ' . $_POST['txtOffset'];
	}

	$SQL = 'select fil_id, fil_bez, fil_plz, mod(row_number () over (order by fil_plz), ';
	$SQL .= $_POST['txtAnzahl'] . ') bucket, ii.FIF_WERT edatum ';
	$SQL .= ' from awis.filialen ff, (select * from awis.filialinfos ii where ii.FIF_FIT_ID = 34 and ii.FIF_IMQ_ID = 32) ii';
	$SQL .= " where fil_lan_wwskenn = 'BRD' and fil_plz is not null and ff.FIL_ID = ii.FIF_FIL_ID(+)";	
	$SQL .= " and to_date(ii.FIF_WERT, 'DD.MM.RR') < sysdate " . (isset($offset)?$offset:"");
	$SQL .= " order by fil_plz";

	$rsBuckets = awisOpenRecordset($con, $SQL);
	$rsBucketsZeilen = $awisRSZeilen;

	$DateiName = awis_UserExportDateiName('.csv');
	$fd = fopen($DateiName,'w' );

	fputs($fd, "Filiale;Bezeichnung;PLZ;Bucket;Eroeffnungsdatum\n");
	for($DSNr=0;$DSNr<$rsBucketsZeilen;$DSNr++)
	{
		fputs($fd, $rsBuckets['FIL_ID'][$DSNr] . ';' . $rsBuckets['FIL_BEZ'][$DSNr] . ';' . $rsBuckets['FIL_PLZ'][$DSNr]  . ';' . $rsBuckets['BUCKET'][$DSNr] . ';' . $rsBuckets['EDATUM'][$DSNr]. "\n");
	}
	fclose($fd);

	$DateiName = pathinfo($DateiName);
	echo '<br><a href=/export/' . $DateiName["basename"] . '>Datei �ffnen</a>';

}
else
{
	echo '<br>';
	echo '<b>Folgende Daten werden generiert:</b>';
	echo '<ul style="list-style-type:none;"><li>Einteilung der Filialen (nur BRD) in Buckets (Sortierung nach PLZ).<br>&Uuml;ber das Offset (Anzahl Tage) kann noch eingestellt werden, bis zu welchem Er&ouml;ffnungsdatum die Filialen enthalten sein sollen.</li>';
	echo "<br><li>";
	echo "<table><tr><td>Anzahl</td><td><input name=txtAnzahl></td></tr>";
	echo "<tr><td>Offset</td><td><input name=txtOffset></td></tr></table></li></ul>";
	print "<br>&nbsp;<input tabindex=98 type=image src=/bilder/eingabe_ok.png alt='Suche starten' name=cmdSuche value=\"Aktualisieren\">";
}

//print "<input type=hidden name=cmdAktion value=Austattung>";
print "</form>";

?>

