<?php
global $VBA;
try
{
    $Speichern = isset($VBA->Param['SPEICHERN']) && $VBA->Param['SPEICHERN'] == 'on'?true:false;

    $VBA->RechteMeldung(0);//Anzeigen Recht
    echo "<form name=frmSuche method=post action=./versvermittlungen_Main.php?cmdAktion=Details>";

    $VBA->Form->Formular_Start();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VERSBEZ'] . ':', 200);

    $SQL = 'SELECT DISTINCT VVE_KUERZEL, VVE_KUERZEL';
    $SQL .= ' FROM VERSDIREKTVERMITTLUNGREF VDV';
    $SQL .= ' INNER JOIN VERSVERSICHERUNGEN VVE';
    $SQL .= ' ON VVE.VVE_VERSNR = VDV.VDV_VERS_NR';

    $VBA->Form->Erstelle_SelectFeld('*VVE_KUERZEL', ($Speichern?$VBA->Param['VVE_KUERZEL']:''), 400, true, $SQL, $VBA->OptionBitteWaehlen);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_KFZKZ'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('*VBA_KFZKZ', ($Speichern?$VBA->Param['VBA_KFZKZ']:''), 25, 250, true);
    $VBA->AWISCursorPosition='sucVBA_KFZKZ';
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_VSNR'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('*VBA_VSNR', ($Speichern?$VBA->Param['VBA_VSNR']:''), 25, 250, true);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_SNR'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('*VBA_SNR', ($Speichern?$VBA->Param['VBA_SNR']:''), 25, 250, true);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_ORDNUNGSNR'] . ':', 200);
    $VBA->Form->Erstelle_TextFeld('*VBA_ORDNUNGSNR', ($Speichern?$VBA->Param['VBA_ORDNUNGSNR']:''), 25, 250, true);
    $VBA->Form->ZeileEnde();

    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['VBA']['VBA_AXZUORDNUNG'] . ':', 200);
    $AXZuordnung = explode("|",$VBA->AWISSprachKonserven['VBA']['VBA_LST_AXZUORDNUNG']);
    $VBA->Form->Erstelle_SelectFeld('*VBA_AXZ', ($Speichern?$VBA->Param['VBA_AXZ']:''), 400, true, '', '', '', '', '', $AXZuordnung);
    $VBA->Form->ZeileEnde();

    // Auswahl kann gespeichert werden
    $VBA->Form->ZeileStart();
    $VBA->Form->Erstelle_TextLabel($VBA->AWISSprachKonserven['Wort']['AuswahlSpeichern'] . ':', 200);
    $VBA->Form->Erstelle_Checkbox('*AuswahlSpeichern', ($Speichern?'on':''), 20, true, 'on');
    $VBA->Form->ZeileEnde();

    $VBA->Form->Formular_Ende();

    $VBA->Form->SchaltflaechenStart();
    $VBA->Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$VBA->AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');
    $VBA->Form->Schaltflaeche('image', 'cmdSuche', '', '/bilder/cmd_weiter.png', $VBA->AWISSprachKonserven['Wort']['lbl_suche'], 'W');
    if(($VBA->Recht9800&4) == 4){
        $VBA->Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $VBA->AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
    }
    $VBA->Form->SchaltflaechenEnde();

    $VBA->Form->SchreibeHTMLCode('</form>');
}
catch (awisException $ex)
{
    if($VBA->Form instanceof awisFormular)
    {
        $VBA->Form->DebugAusgabe(1, $ex->getSQL());
        $VBA->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180928");
    }
    else
    {
        $VBA->Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
    }
}
catch (Exception $ex)
{
    if($VBA->Form instanceof awisFormular)
    {
        $VBA->Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200812180922");
    }
    else
    {
        echo 'allg. Fehler:'.$ex->getMessage();
    }
}
?>
