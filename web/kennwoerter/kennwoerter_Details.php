<?php
global $AWISCursorPosition;		// Zum Cursor setzen
global $AWIS_KEY1;
global $AWIS_KEY2;
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');
try
{
	// Textkonserven laden
	$TextKonserven = array();
	$TextKonserven[]=array('KEW','%');
	$TextKonserven[]=array('Wort','lbl_weiter');
	$TextKonserven[]=array('Wort','lbl_speichern');
	$TextKonserven[]=array('Wort','lbl_zurueck');
	$TextKonserven[]=array('Wort','lbl_hilfe');
	$TextKonserven[]=array('Wort','lbl_trefferliste');
	$TextKonserven[]=array('Wort','lbl_aendern');
	$TextKonserven[]=array('Wort','lbl_hinzufuegen');
	$TextKonserven[]=array('Wort','lbl_loeschen');
	$TextKonserven[]=array('Wort','Seite');
	$TextKonserven[]=array('Wort','INET_AdressenSuche');
	$TextKonserven[]=array('Wort','txt_BitteWaehlen');
	$TextKonserven[]=array('Liste','lst_JaNein');
	$TextKonserven[]=array('Fehler','err_keineDaten');
	$TextKonserven[]=array('Fehler','err_keineDatenbank');

	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$AWISSprachKonserven = $Form->LadeTexte($TextKonserven);
	$Recht9200 = $AWISBenutzer->HatDasRecht(9200);
	if($Recht9200==0)
	{
	    echo "<span class=HinweisText>".$AWISSprachKonserven['Fehler']['err_keineRechte']."</span>";
		echo "<br><br><input type=image title='".$AWISSprachKonserven['Wort']['lbl_zurueck']."' accesskey=z src=/bilder/zurueck.png name=cmdZurueck onclick=top.location.href='/index.php';>";
		die();
	}

	$Param = unserialize($AWISBenutzer->ParameterLesen('Formular_KEW'));
	$ListenSchriftGroesse = $AWISBenutzer->ParameterLesen('Schriftgroesse:Listen');
	$ListenSchriftFaktor = (int)((($ListenSchriftGroesse==0?12:$ListenSchriftGroesse)/12)*9);


	$DetailAnsicht=false;
	//********************************************************
	// Parameter ?
	//********************************************************
	if(isset($_POST['cmdSuche_x']))
	{
		$Param = array();
		$Param['KEW_BEZEICHNUNG'] = $_POST['sucKEW_BEZEICHNUNG'];
		$Param['KEW_BENUTZERNAME'] = $_POST['sucKEW_BENUTZERNAME'];
		$Param['KEW_BEREICH'] = $_POST['sucKEW_BEREICH'];
		$Param['KEW_BEMERKUNG'] = $_POST['sucKEW_BEMERKUNG'];
		$Param['KEW_KUNDENNUMMER'] = $_POST['sucKEW_KUNDENNUMMER'];

		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$Param['SPEICHERN']=isset($_POST['sucAuswahlSpeichern'])?'on':'';

		$AWISBenutzer->ParameterSchreiben("Formular_KEW",serialize($Param));
	}
	elseif(isset($_POST['cmdLoeschen_x']) or isset($_GET['Del']) or isset($_POST['cmdLoeschenOK']))
	{
		include('./kennwoerter_loeschen.php');
	}
	elseif(isset($_POST['cmdSpeichern_x']))
	{
		include('./kennwoerter_speichern.php');
	}
	elseif(isset($_POST['cmdDSNeu_x']))
	{
		$AWIS_KEY1=-1;
		$Param['KEY']='';
		$Param['WHERE']='';
		$Param['ORDER']='';
		$AWISBenutzer->ParameterSchreiben('Formular_KEW',serialize($Param));
	}
	elseif(isset($_GET['KEW_KEY']))
	{
		$AWIS_KEY1 = $Form->Format('N0',$_GET['KEW_KEY']);
		$Param['KEY'] = ($AWIS_KEY1<0?'':$AWIS_KEY1);
		$Param['WHERE']='';
		$Param['ORDER']='';
		$AWISBenutzer->ParameterSchreiben('Formular_KEW',serialize($Param));
	}
	else 		// Nicht �ber die Suche gekommen, letzten Key abfragen
	{
		if(!isset($Param['KEY']))
		{
			$Param['KEY']='';
			$Param['WHERE']='';
			$Param['ORDER']='';
			$AWISBenutzer->ParameterSchreiben('Formular_KEW',serialize($Param));
		}

		if(isset($_GET['Liste']) OR (isset($_REQUEST['Block']) AND !isset($_REQUEST['Seite'])))
		{
			$Param['KEY']=0;
		}
		$AWIS_KEY1=$Param['KEY'];
	}

	//*********************************************************
	//* Sortierung
	//*********************************************************

	if(!isset($_GET['Sort']))
	{
		if($Param['ORDER']!='')
		{
			$ORDERBY = $Param['ORDER'];
		}
		else
		{
			$ORDERBY = ' ORDER BY KEW_BEZEICHNUNG';
		}
	}
	else
	{
		$ORDERBY = ' ORDER BY '.str_replace('~',' DESC ',$_GET['Sort']);
	}
	$Param['ORDER']=$ORDERBY;

	//********************************************************
	// Daten suchen
	//********************************************************
	$Bedingung = _BedingungErstellen($Param);

	$SQL = 'SELECT kennwoerter.*';
	$SQL .= ', row_number() over ('.$ORDERBY.') AS ZeilenNr';
	$SQL .= ' FROM kennwoerter';

	if($Bedingung!='')
	{
		$SQL .= ' WHERE ' . substr($Bedingung,4);
	}

	// Wenn ein DS ausgew�hlt wurde, muss nicht gebl�ttert werden
	if($AWIS_KEY1<=0)
	{
		// Zum Bl�ttern in den Daten
		$Block = 1;
		if(isset($_REQUEST['Block']))
		{
			$Block=$Form->Format('N0',$_REQUEST['Block'],false);
			$Param['BLOCK']=$Block;
			$AWISBenutzer->ParameterSchreiben('Formular_KEW',serialize($Param));
		}
		elseif(isset($Param['BLOCK']))
		{
			$Block=$Param['BLOCK'];
		}

		$ZeilenProSeite = $AWISBenutzer->ParameterLesen('AnzahlDatensaetzeProListe');

		$StartZeile = (($Block-1)*$ZeilenProSeite)+1;
		$MaxDS = $DB->ErmittleZeilenAnzahl($SQL);
		$SQL = 'SELECT * FROM ('.$SQL.')DATEN WHERE ZeilenNr>='.$StartZeile.' AND  ZeilenNr<'.($StartZeile+$ZeilenProSeite);
	}
	else
	{
		$MaxDS = 1;
		$ZeilenProSeite=1;
		$Block = 1;
	}

	$SQL .= $ORDERBY;

	$rsKEW = $DB->RecordsetOeffnen($SQL);
	$AWISBenutzer->ParameterSchreiben('Formular_KEW',serialize($Param));

	//********************************************************
	// Daten anzeigen
	//********************************************************
	echo '<form name=frmkennwoerter action=./kennwoerter_Main.php?cmdAktion=Details'.(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').''.(isset($_GET['Block'])?'&Block='.$_GET['Block']:'').' method=POST>';

	if($rsKEW->EOF() AND !isset($_POST['cmdDSNeu_x']) AND $AWIS_KEY1!=-1)		// Keine Meldung bei neuen Datens�tzen!
	{
		$Form->Hinweistext($AWISSprachKonserven['Fehler']['err_keineDaten']);
	}
	elseif($rsKEW->AnzahlDatensaetze()>1 or isset($_GET['Liste']))						// Liste anzeigen
	{
		$DetailAnsicht = false;
		$Form->Formular_Start();

		$Form->ZeileStart($ListenSchriftGroesse==0?'':'font-size:'.intval($ListenSchriftGroesse).'pt');
		$Link = './kennwoerter_Main.php?cmdAktion=Details'.(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
		$Link .= '&Sort=KEW_BEZEICHNUNG'.((isset($_GET['Sort']) AND ($_GET['Sort']=='KEW_BEZEICHNUNG'))?'~':'');
		$Form->Erstelle_Liste_Ueberschrift($AWISSprachKonserven['KEW']['KEW_BEZEICHNUNG'],200,'',$Link);


		$Form->ZeileEnde();

		$DS=0;
		while(!$rsKEW->EOF())
		{
			$Form->ZeileStart();
			$Link = './kennwoerter_Main.php?cmdAktion=Details&KEW_KEY=0'.$rsKEW->FeldInhalt('KEW_KEY').(isset($_GET['Block'])?'&Block='.intval($_GET['Block']):'').(isset($_GET['Seite'])?'&Seite='.($_GET['Seite']):'');
			$Form->Erstelle_ListenFeld('KEW_BEZEICHNUNG',$rsKEW->FeldInhalt('KEW_BEZEICHNUNG'),0,200,false,($DS%2),'',$Link,'T','L',$rsKEW->FeldInhalt('KEW_BEZEICHNUNG'));


			//TODO: Felder eintragen


			$Form->ZeileEnde();

			$rsKEW->DSWeiter();
			$DS++;
		}

		$Link = './kennwoerter_Main.php?cmdAktion=Details';
		$Form->BlaetternZeile($MaxDS,$ZeilenProSeite,$Link,$Block,'');

		$Form->Formular_Ende();
	}			// Eine einzelne Adresse
	else										// Eine einzelne oder neue Adresse
	{
		$DetailAnsicht = true;
		$AWIS_KEY1 = $rsKEW->FeldInhalt('KEW_KEY');

		$Param['KEY']=$AWIS_KEY1;
		$AWISBenutzer->ParameterSchreiben('Formular_KEW',serialize($Param));

		echo '<input type=hidden name=txtKEW_KEY value='.$AWIS_KEY1. '>';

		$Form->Formular_Start();
		$OptionBitteWaehlen = '-1~'.$AWISSprachKonserven['Wort']['txt_BitteWaehlen'];


			// Infozeile zusammenbauen
		$Felder = array();
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>"<a href=./kennwoerter_Main.php?cmdAktion=Details&Liste=True accesskey=T title='".$AWISSprachKonserven['Wort']['lbl_trefferliste']."'><img border=0 src=/bilder/cmd_trefferliste.png></a>");
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKEW->FeldInhalt('KEW_USER'));
		$Felder[] = array('Style'=>'font-size:smaller;','Inhalt'=>$rsKEW->FeldInhalt('KEW_USERDAT'));
		$Form->InfoZeile($Felder,'');

		$EditRecht=(($Recht9200&6)!=0) or isset($_POST['cmdDSNeu_x']);
		// Eigenes Kennwort?
		if($AWISBenutzer->BenutzerID()!=$rsKEW->FeldInhalt('KEW_XBN_KEY') AND !$_POST['cmdDSNeu_x'])
		{
			$SQL = 'SELECT COALESCE(max(kez_aendern),0) as kez_aendern';
			$SQL .= ' FROM Kennwoerterzugriffe';
			$SQL .= ' INNER JOIN KennwoerterGruppenMitglieder ON KEZ_XXX_KEY=KZM_KZG_KEY';

			$rsZugriff = $DB->RecordSetOeffnen($SQL);
			if($rsZugriff->FeldInhalt('kez_aendern')==0)
			{
				$EditRecht=0;
			}
		}

		// �berschrift
		$Form->Erstelle_HiddenFeld('KEW_KEY',$rsKEW->FeldInhalt('KEW_KEY'));

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BEZEICHNUNG'].':',150);
		$Form->Erstelle_TextFeld('KEW_BEZEICHNUNG',$rsKEW->FeldInhalt('KEW_BEZEICHNUNG'),50,500,$EditRecht,'','','','T');
		$AWISCursorPosition = 'txtKEW_BEZEICHNUNG';
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BEREICH'].':',150);
		$Form->Erstelle_TextFeld('KEW_BEREICH',$rsKEW->FeldInhalt('KEW_BEREICH'),50,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BENUTZERNAME'].':',150);
		$Form->Erstelle_TextFeld('KEW_BENUTZERNAME',$rsKEW->FeldInhalt('KEW_BENUTZERNAME'),50,500,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_KUNDENNUMMER'].':',150);
		$Form->Erstelle_TextFeld('KEW_KUNDENNUMMER',$rsKEW->FeldInhalt('KEW_KUNDENNUMMER'),10,100,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BENUTZERKENNWORT'].':',134);
		$Form->Erstelle_Bild('script','cmd_Anzeigen','onclick=alert(\''.$rsKEW->FeldInhalt('KEW_BENUTZERKENNWORT').'\',\'XX\');','/bilder/icon_lupe.png','');
		$Form->Erstelle_TextFeld('KEWBENUTZERKENNWORT1','********',20,200,$EditRecht,'','','','P');
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KennwortWiederholen'].':',200);
		$Form->Erstelle_TextFeld('KEWBENUTZERKENNWORT2','********',20,200,$EditRecht,'','','','P');
		$Form->ZeileEnde();

		$Form->ZeileStart();
		$Form->Erstelle_TextLabel($AWISSprachKonserven['KEW']['KEW_BEMERKUNG'].':',150);
		$Form->Erstelle_Textarea('KEW_BEMERKUNG',$rsKEW->FeldInhalt('KEW_BEMERKUNG'),500,70,3,$EditRecht,'','','','T');
		$Form->ZeileEnde();

		$Form->Formular_Ende();


		if($rsKEW->FeldInhalt('KEW_KEY')!='')
		{
			// Zugriffe nur f�r die eigenen Kennw�rter erlauben
			if($AWISBenutzer->BenutzerID()==$rsKEW->FeldInhalt('KEW_XBN_KEY'))
			{
				$Reg = new awisRegister(9201);
				$Reg->ZeichneRegister((isset($_GET['Seite'])?$_GET['Seite']:''));
			}
		}

	}

	//***************************************
	// Schaltfl�chen f�r dieses Register
	//***************************************
	$Form->SchaltflaechenStart();

	$Form->Schaltflaeche('href','cmd_zurueck','../index.php','/bilder/cmd_zurueck.png',$AWISSprachKonserven['Wort']['lbl_zurueck'],'Z');

	if((($Recht9200&6)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdSpeichern', '', '/bilder/cmd_speichern.png', $AWISSprachKonserven['Wort']['lbl_speichern'], 'S');
	}

	if((($Recht9200&4)!=0))
	{
		$Form->Schaltflaeche('image', 'cmdDSNeu', '', '/bilder/cmd_neu.png', $AWISSprachKonserven['Wort']['lbl_hinzufuegen'], 'N');
	}

	if((($Recht9200&8)!=0  AND $DetailAnsicht))
	{
		$Form->Schaltflaeche('image', 'cmdLoeschen', '', '/bilder/cmd_loeschen.png', $AWISSprachKonserven['Wort']['lbl_loeschen'], '');
	}

	$Form->SchaltflaechenEnde();

	$Form->SchreibeHTMLCode('</form>');

	if($AWISCursorPosition!='')
	{
		$Form->SchreibeHTMLCode('<Script Language=JavaScript>');
		$Form->SchreibeHTMLCode("document.getElementsByName(\"".$AWISCursorPosition."\")[0].focus();");
		$Form->SchreibeHTMLCode('</Script>');
	}
}
catch (awisException $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->DebugAusgabe(1, $ex->getSQL());
		@file_put_contents('/tmp/kennwoerter_Details.err',$AWISBenutzer->BenutzerName()."\n".serialize($SQL)."\n\n",FILE_APPEND);
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151350");
	}
	else
	{
		$Form->SchreibeHTMLCode('AWIS-Fehler:'.$ex->getMessage());
	}
}
catch (Exception $ex)
{
	if($Form instanceof awisFormular)
	{
		$Form->Fehler_Anzeigen('INTERN',$ex->getMessage(),'MELDEN',6,"200909151351");
	}
	else
	{
		echo 'allg. Fehler:'.$ex->getMessage();
	}
}

/**
 * Bedingung f�r die Abfrage erstellen
 *
 * @param unknown_type $Param
 */
function _BedingungErstellen($Param)
{
	global $AWIS_KEY1;
	global $DB;
	global $AWISBenutzer;

	$Bedingung = '';

	if($AWIS_KEY1!=0)
	{
		$Bedingung.= ' AND KEW_KEY = '.floatval($AWIS_KEY1);
		return $Bedingung;
	}

	if(isset($Param['KEW_BEZEICHNUNG']) AND $Param['KEW_BEZEICHNUNG']!='')
	{
		$Bedingung .= ' AND (UPPER(KEW_BEZEICHNUNG) ' . $DB->LIKEoderIST('*'.$Param['KEW_BEZEICHNUNG'].'*',awisDatenbank::AWIS_LIKE_UPPER) . ')';
	}

	if(isset($Param['KEW_BEMERKUNG']) AND $Param['KEW_BEMERKUNG']!='')
	{
		$Bedingung .= ' AND UPPER(KEW_BEMERKUNG) ' . $DB->LIKEoderIST($Param['KEW_BEMERKUNG'],awisDatenbank::AWIS_LIKE_UPPER) . '';
	}

	if(isset($Param['KEW_BEREICH']) AND $Param['KEW_BEREICH']!='')
	{
		$Bedingung .= ' AND KEW_BEREICH ' . $DB->LIKEoderIST($Param['KEW_BEREICH'],awisDatenbank::AWIS_LIKE_UPPER) . '';
	}

	if(isset($Param['KEW_KUNDENNUMMER']) AND $Param['KEW_KUNDENNUMMER']!='')
	{
		$Bedingung .= ' AND EXISTS(SELECT * FROM MitbewerberFilialen WHERE MFI_KEW_KEY = KEW_KEY AND KEW_KUNDENNUMMER <= '.$DB->FeldInhaltFormat('N2',$Param['KEW_KUNDENNUMMER']).')';
	}

	$Bedingung .= ' AND (EXISTS(SELECT * FROM Kennwoerterzugriffe ';
	$Bedingung .= '             LEFT OUTER JOIN KennwoerterGruppenMitglieder ON KEZ_XXX_KEY=KZM_KZG_KEY';
	$Bedingung .= ' 			WHERE KEW_KEY = KEZ_KEW_KEY ';
	$Bedingung .= '             AND (KEZ_XXX_KEY = '.$AWISBenutzer->BenutzerID().' OR ';
	$Bedingung .= '                  KZM_XBN_KEY = '.$AWISBenutzer->BenutzerID().') ';
	$Bedingung .= ' ) OR ';
	$Bedingung .= ' KEW_XBN_KEY = '.$AWISBenutzer->BenutzerID().')';

	return $Bedingung;
}
?>