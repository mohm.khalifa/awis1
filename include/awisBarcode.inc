<?php
class awisBarcode
{
	/**
	 * Referenz auf den Bericht
	 * @var awisBericht
	 */
	private $_PDF;

	/**
	 * Codes f�r den CODE128 Barcode
	 * @var unknown_type
	 */
	private $_T128 = array();

	private $ABCset="";                                        // jeu des caract�res �ligibles au C128
	private $Aset="";                                          // Set A du jeu des caract�res �ligibles
	private $Bset="";                                          // Set B du jeu des caract�res �ligibles
	private $Cset="";                                          // Set C du jeu des caract�res �ligibles
	private $SetFrom;                                          // Convertisseur source des jeux vers le tableau
	private $SetTo;                                            // Convertisseur destination des jeux vers le tableau
	private $JStart = array("A"=>103, "B"=>104, "C"=>105);     // Caract�res de s�lection de jeu au d�but du C128
	private $JSwap = array("A"=>101, "B"=>100, "C"=>99);       // Caract�res de changement de jeu

	public function __construct($PDF)
	{
		$this->_PDF = $PDF;
	}

	function EAN13($x,$y,$barcode,$h=16,$w=.35,$bottomnumbers=true)
	{
		$this->Barcode($x,$y,$barcode,$h,$w,13,false,$bottomnumbers);
	}

	function UPC_A($x,$y,$barcode,$h=16,$w=.35,$bottomnumbers=true)
	{
		$this->Barcode($x,$y,$barcode,$h,$w,12,false,$bottomnumbers);
	}

	function GetCheckDigit($barcode)
	{
		//Compute the check digit
		$sum=0;
		for($i=1;$i<=11;$i+=2)
			$sum+=3*$barcode{$i};
		for($i=0;$i<=10;$i+=2)
			$sum+=$barcode{$i};
		$r=$sum%10;
		if($r>0)
			$r=10-$r;
		return $r;
	}

	function TestCheckDigit($barcode)
	{
		//Test validity of check digit
		$sum=0;
		for($i=1;$i<=11;$i+=2)
			$sum+=3*$barcode{$i};
		for($i=0;$i<=10;$i+=2)
			$sum+=$barcode{$i};
		return ($sum+$barcode{12})%10==0;
	}

	/**
	 * Berechne und Drucke einen Barcode
	 * @param int $x
	 * @param int $y
	 * @param string $barcode
	 * @param int $h
	 * @param int $w
	 * @param int $len
	 * @param boolean $bottomnumbers Sollen die Zahlen unter dem Barcode angezeigt
	 */
	function Barcode($x,$y,$barcode,$h,$w,$len,$OhnePruefziffer=false,$bottomnumbers=true)
	{
		//Padding
		if($OhnePruefziffer===false)
		{
			$barcode=str_pad($barcode,$len-1,'0',STR_PAD_LEFT);
			if($len==12)
				$barcode='0'.$barcode;
			//Add or control the check digit
			if(strlen($barcode)==12)
				$barcode.=$this->GetCheckDigit($barcode);
			elseif(!$this->TestCheckDigit($barcode))
				die('Incorrect check digit');
		}
		
			//Convert digits to bars
		$codes=array(
			'A'=>array(
				'0'=>'0001101','1'=>'0011001','2'=>'0010011','3'=>'0111101','4'=>'0100011',
				'5'=>'0110001','6'=>'0101111','7'=>'0111011','8'=>'0110111','9'=>'0001011'),
			'B'=>array(
				'0'=>'0100111','1'=>'0110011','2'=>'0011011','3'=>'0100001','4'=>'0011101',
				'5'=>'0111001','6'=>'0000101','7'=>'0010001','8'=>'0001001','9'=>'0010111'),
			'C'=>array(
				'0'=>'1110010','1'=>'1100110','2'=>'1101100','3'=>'1000010','4'=>'1011100',
				'5'=>'1001110','6'=>'1010000','7'=>'1000100','8'=>'1001000','9'=>'1110100')
			);
		$parities=array(
			'0'=>array('A','A','A','A','A','A'),
			'1'=>array('A','A','B','A','B','B'),
			'2'=>array('A','A','B','B','A','B'),
			'3'=>array('A','A','B','B','B','A'),
			'4'=>array('A','B','A','A','B','B'),
			'5'=>array('A','B','B','A','A','B'),
			'6'=>array('A','B','B','B','A','A'),
			'7'=>array('A','B','A','B','A','B'),
			'8'=>array('A','B','A','B','B','A'),
			'9'=>array('A','B','B','A','B','A')
			);
		$code='101';
		$p=$parities[$barcode{0}];
		for($i=1;$i<=6;$i++)
		{
			$code.=$codes[$p[$i-1]][$barcode{$i}];
		}
		$code.='01010';
		for($i=7;$i<=12;$i++)
			$code.=$codes['C'][$barcode{$i}];
		$code.='101';
		//Draw bars

		for($i=0;$i<strlen($code);$i++)
		{
			if($code{$i}=='1')
				$this->_PDF->Rect($x+($i*$w),$y,$w,$h,'F');
		}
        
		if($bottomnumbers === true) {

            //Print text under barcode
            $this->_PDF->SetFont('Arial', '', 8);
            //$this->_PDF->Text($x,$y+$h+11/$this->_PDF->k,substr($barcode,-$len));
            $this->_PDF->SetXY($x, $y + $h);
            $this->_PDF->Cell($w, $h, substr($barcode, -$len), 0, 0, 'L');            // Barcode zentriert drunter
        }
	}



	/**
	 * CODE128 Barcode drucken
	 * @param int $x		X-Position
	 * @param int $y		Y-Position
	 * @param string $code	Barcode
	 * @param int $w		Breite in Pixeln
	 * @param int $h		Hoehe in Pixeln
	 */
	function Code128($x, $y, $code, $w, $h)
	{
	    $this->_T128[] = array(2, 1, 2, 2, 2, 2);           //0 : [ ]               // composition des caract�res
	    $this->_T128[] = array(2, 2, 2, 1, 2, 2);           //1 : [!]
	    $this->_T128[] = array(2, 2, 2, 2, 2, 1);           //2 : ["]
	    $this->_T128[] = array(1, 2, 1, 2, 2, 3);           //3 : [#]
	    $this->_T128[] = array(1, 2, 1, 3, 2, 2);           //4 : [$]
	    $this->_T128[] = array(1, 3, 1, 2, 2, 2);           //5 : [%]
	    $this->_T128[] = array(1, 2, 2, 2, 1, 3);           //6 : [&]
	    $this->_T128[] = array(1, 2, 2, 3, 1, 2);           //7 : [']
	    $this->_T128[] = array(1, 3, 2, 2, 1, 2);           //8 : [(]
	    $this->_T128[] = array(2, 2, 1, 2, 1, 3);           //9 : [)]
	    $this->_T128[] = array(2, 2, 1, 3, 1, 2);           //10 : [*]
	    $this->_T128[] = array(2, 3, 1, 2, 1, 2);           //11 : [+]
	    $this->_T128[] = array(1, 1, 2, 2, 3, 2);           //12 : [,]
	    $this->_T128[] = array(1, 2, 2, 1, 3, 2);           //13 : [-]
	    $this->_T128[] = array(1, 2, 2, 2, 3, 1);           //14 : [.]
	    $this->_T128[] = array(1, 1, 3, 2, 2, 2);           //15 : [/]
	    $this->_T128[] = array(1, 2, 3, 1, 2, 2);           //16 : [0]
	    $this->_T128[] = array(1, 2, 3, 2, 2, 1);           //17 : [1]
	    $this->_T128[] = array(2, 2, 3, 2, 1, 1);           //18 : [2]
	    $this->_T128[] = array(2, 2, 1, 1, 3, 2);           //19 : [3]
	    $this->_T128[] = array(2, 2, 1, 2, 3, 1);           //20 : [4]
	    $this->_T128[] = array(2, 1, 3, 2, 1, 2);           //21 : [5]
	    $this->_T128[] = array(2, 2, 3, 1, 1, 2);           //22 : [6]
	    $this->_T128[] = array(3, 1, 2, 1, 3, 1);           //23 : [7]
	    $this->_T128[] = array(3, 1, 1, 2, 2, 2);           //24 : [8]
	    $this->_T128[] = array(3, 2, 1, 1, 2, 2);           //25 : [9]
	    $this->_T128[] = array(3, 2, 1, 2, 2, 1);           //26 : [:]
	    $this->_T128[] = array(3, 1, 2, 2, 1, 2);           //27 : [;]
	    $this->_T128[] = array(3, 2, 2, 1, 1, 2);           //28 : [<]
	    $this->_T128[] = array(3, 2, 2, 2, 1, 1);           //29 : [=]
	    $this->_T128[] = array(2, 1, 2, 1, 2, 3);           //30 : [>]
	    $this->_T128[] = array(2, 1, 2, 3, 2, 1);           //31 : [?]
	    $this->_T128[] = array(2, 3, 2, 1, 2, 1);           //32 : [@]
	    $this->_T128[] = array(1, 1, 1, 3, 2, 3);           //33 : [A]
	    $this->_T128[] = array(1, 3, 1, 1, 2, 3);           //34 : [B]
	    $this->_T128[] = array(1, 3, 1, 3, 2, 1);           //35 : [C]
	    $this->_T128[] = array(1, 1, 2, 3, 1, 3);           //36 : [D]
	    $this->_T128[] = array(1, 3, 2, 1, 1, 3);           //37 : [E]
	    $this->_T128[] = array(1, 3, 2, 3, 1, 1);           //38 : [F]
	    $this->_T128[] = array(2, 1, 1, 3, 1, 3);           //39 : [G]
	    $this->_T128[] = array(2, 3, 1, 1, 1, 3);           //40 : [H]
	    $this->_T128[] = array(2, 3, 1, 3, 1, 1);           //41 : [I]
	    $this->_T128[] = array(1, 1, 2, 1, 3, 3);           //42 : [J]
	    $this->_T128[] = array(1, 1, 2, 3, 3, 1);           //43 : [K]
	    $this->_T128[] = array(1, 3, 2, 1, 3, 1);           //44 : [L]
	    $this->_T128[] = array(1, 1, 3, 1, 2, 3);           //45 : [M]
	    $this->_T128[] = array(1, 1, 3, 3, 2, 1);           //46 : [N]
	    $this->_T128[] = array(1, 3, 3, 1, 2, 1);           //47 : [O]
	    $this->_T128[] = array(3, 1, 3, 1, 2, 1);           //48 : [P]
	    $this->_T128[] = array(2, 1, 1, 3, 3, 1);           //49 : [Q]
	    $this->_T128[] = array(2, 3, 1, 1, 3, 1);           //50 : [R]
	    $this->_T128[] = array(2, 1, 3, 1, 1, 3);           //51 : [S]
	    $this->_T128[] = array(2, 1, 3, 3, 1, 1);           //52 : [T]
	    $this->_T128[] = array(2, 1, 3, 1, 3, 1);           //53 : [U]
	    $this->_T128[] = array(3, 1, 1, 1, 2, 3);           //54 : [V]
	    $this->_T128[] = array(3, 1, 1, 3, 2, 1);           //55 : [W]
	    $this->_T128[] = array(3, 3, 1, 1, 2, 1);           //56 : [X]
	    $this->_T128[] = array(3, 1, 2, 1, 1, 3);           //57 : [Y]
	    $this->_T128[] = array(3, 1, 2, 3, 1, 1);           //58 : [Z]
	    $this->_T128[] = array(3, 3, 2, 1, 1, 1);           //59 : [[]
	    $this->_T128[] = array(3, 1, 4, 1, 1, 1);           //60 : [\]
	    $this->_T128[] = array(2, 2, 1, 4, 1, 1);           //61 : []]
	    $this->_T128[] = array(4, 3, 1, 1, 1, 1);           //62 : [^]
	    $this->_T128[] = array(1, 1, 1, 2, 2, 4);           //63 : [_]
	    $this->_T128[] = array(1, 1, 1, 4, 2, 2);           //64 : [`]
	    $this->_T128[] = array(1, 2, 1, 1, 2, 4);           //65 : [a]
	    $this->_T128[] = array(1, 2, 1, 4, 2, 1);           //66 : [b]
	    $this->_T128[] = array(1, 4, 1, 1, 2, 2);           //67 : [c]
	    $this->_T128[] = array(1, 4, 1, 2, 2, 1);           //68 : [d]
	    $this->_T128[] = array(1, 1, 2, 2, 1, 4);           //69 : [e]
	    $this->_T128[] = array(1, 1, 2, 4, 1, 2);           //70 : [f]
	    $this->_T128[] = array(1, 2, 2, 1, 1, 4);           //71 : [g]
	    $this->_T128[] = array(1, 2, 2, 4, 1, 1);           //72 : [h]
	    $this->_T128[] = array(1, 4, 2, 1, 1, 2);           //73 : [i]
	    $this->_T128[] = array(1, 4, 2, 2, 1, 1);           //74 : [j]
	    $this->_T128[] = array(2, 4, 1, 2, 1, 1);           //75 : [k]
	    $this->_T128[] = array(2, 2, 1, 1, 1, 4);           //76 : [l]
	    $this->_T128[] = array(4, 1, 3, 1, 1, 1);           //77 : [m]
	    $this->_T128[] = array(2, 4, 1, 1, 1, 2);           //78 : [n]
	    $this->_T128[] = array(1, 3, 4, 1, 1, 1);           //79 : [o]
	    $this->_T128[] = array(1, 1, 1, 2, 4, 2);           //80 : [p]
	    $this->_T128[] = array(1, 2, 1, 1, 4, 2);           //81 : [q]
	    $this->_T128[] = array(1, 2, 1, 2, 4, 1);           //82 : [r]
	    $this->_T128[] = array(1, 1, 4, 2, 1, 2);           //83 : [s]
	    $this->_T128[] = array(1, 2, 4, 1, 1, 2);           //84 : [t]
	    $this->_T128[] = array(1, 2, 4, 2, 1, 1);           //85 : [u]
	    $this->_T128[] = array(4, 1, 1, 2, 1, 2);           //86 : [v]
	    $this->_T128[] = array(4, 2, 1, 1, 1, 2);           //87 : [w]
	    $this->_T128[] = array(4, 2, 1, 2, 1, 1);           //88 : [x]
	    $this->_T128[] = array(2, 1, 2, 1, 4, 1);           //89 : [y]
	    $this->_T128[] = array(2, 1, 4, 1, 2, 1);           //90 : [z]
	    $this->_T128[] = array(4, 1, 2, 1, 2, 1);           //91 : [{]
	    $this->_T128[] = array(1, 1, 1, 1, 4, 3);           //92 : [|]
	    $this->_T128[] = array(1, 1, 1, 3, 4, 1);           //93 : [}]
	    $this->_T128[] = array(1, 3, 1, 1, 4, 1);           //94 : [~]
	    $this->_T128[] = array(1, 1, 4, 1, 1, 3);           //95 : [DEL]
	    $this->_T128[] = array(1, 1, 4, 3, 1, 1);           //96 : [FNC3]
	    $this->_T128[] = array(4, 1, 1, 1, 1, 3);           //97 : [FNC2]
	    $this->_T128[] = array(4, 1, 1, 3, 1, 1);           //98 : [SHIFT]
	    $this->_T128[] = array(1, 1, 3, 1, 4, 1);           //99 : [Cswap]
	    $this->_T128[] = array(1, 1, 4, 1, 3, 1);           //100 : [Bswap]
	    $this->_T128[] = array(3, 1, 1, 1, 4, 1);           //101 : [Aswap]
	    $this->_T128[] = array(4, 1, 1, 1, 3, 1);           //102 : [FNC1]
	    $this->_T128[] = array(2, 1, 1, 4, 1, 2);           //103 : [Astart]
	    $this->_T128[] = array(2, 1, 1, 2, 1, 4);           //104 : [Bstart]
	    $this->_T128[] = array(2, 1, 1, 2, 3, 2);           //105 : [Cstart]
	    $this->_T128[] = array(2, 3, 3, 1, 1, 1);           //106 : [STOP]
	    $this->_T128[] = array(2, 1);                       //107 : [END BAR]

	    for ($i = 32; $i <= 95; $i++) {                                            // jeux de caract�res
	        $this->ABCset .= chr($i);
	    }
	    $this->Aset = $this->ABCset;
	    $this->Bset = $this->ABCset;
	    for ($i = 0; $i <= 31; $i++) {
	        $this->ABCset .= chr($i);
	        $this->Aset .= chr($i);
	    }
	    for ($i = 96; $i <= 126; $i++) {
	        $this->ABCset .= chr($i);
	        $this->Bset .= chr($i);
	    }
	    $this->Cset="0123456789";

	    for ($i=0; $i<96; $i++) {                                                  // convertisseurs des jeux A & B
	        @$this->SetFrom["A"] .= chr($i);
	        @$this->SetFrom["B"] .= chr($i + 32);
	        @$this->SetTo["A"] .= chr(($i < 32) ? $i+64 : $i-32);
	        @$this->SetTo["B"] .= chr($i);
	    }


	    $Aguid = "";                                                                      // Cr�ation des guides de choix ABC
	    $Bguid = "";
	    $Cguid = "";
	    for ($i=0; $i < strlen($code); $i++) {
	        $needle = substr($code,$i,1);
	        $Aguid .= ((strpos($this->Aset,$needle)===false) ? "N" : "O");
	        $Bguid .= ((strpos($this->Bset,$needle)===false) ? "N" : "O");
	        $Cguid .= ((strpos($this->Cset,$needle)===false) ? "N" : "O");
	    }

	    $SminiC = "OOOO";
	    $IminiC = 4;

	    $crypt = "";
	    while ($code > "") {
	                                                                                    // BOUCLE PRINCIPALE DE CODAGE
	        $i = strpos($Cguid,$SminiC);                                                // for�age du jeu C, si possible
	        if ($i!==false) {
	            $Aguid [$i] = "N";
	            $Bguid [$i] = "N";
	        }

	        if (substr($Cguid,0,$IminiC) == $SminiC) {                                  // jeu C
	            $crypt .= chr(($crypt > "") ? $this->JSwap["C"] : $this->JStart["C"]);  // d�but Cstart, sinon Cswap
	            $made = strpos($Cguid,"N");                                             // �tendu du set C
	            if ($made === false) {
	                $made = strlen($Cguid);
	            }
	            if (fmod($made,2)==1) {
	                $made--;                                                            // seulement un nombre pair
	            }
	            for ($i=0; $i < $made; $i += 2) {
	                $crypt .= chr(strval(substr($code,$i,2)));                          // conversion 2 par 2
	            }
	            $jeu = "C";
	        } else {
	            $madeA = strpos($Aguid,"N");                                            // �tendu du set A
	            if ($madeA === false) {
	                $madeA = strlen($Aguid);
	            }
	            $madeB = strpos($Bguid,"N");                                            // �tendu du set B
	            if ($madeB === false) {
	                $madeB = strlen($Bguid);
	            }
	            $made = (($madeA < $madeB) ? $madeB : $madeA );                         // �tendu trait�e
	            $jeu = (($madeA < $madeB) ? "B" : "A" );                                // Jeu en cours

	            $crypt .= chr(($crypt > "") ? $this->JSwap[$jeu] : $this->JStart[$jeu]); // d�but start, sinon swap

	            $crypt .= strtr(substr($code, 0,$made), $this->SetFrom[$jeu], $this->SetTo[$jeu]); // conversion selon jeu

	        }
	        $code = substr($code,$made);                                           // raccourcir l�gende et guides de la zone trait�e
	        $Aguid = substr($Aguid,$made);
	        $Bguid = substr($Bguid,$made);
	        $Cguid = substr($Cguid,$made);
	    }                                                                          // FIN BOUCLE PRINCIPALE

	    $check = $crypt==''?0:ord($crypt[0]);                                                   // calcul de la somme de contr�le
	    for ($i=0; $i<strlen($crypt); $i++) {
	        $check += (ord($crypt[$i]) * $i);
	    }
	    $check %= 103;

	    $crypt .= chr($check) . chr(106) . chr(107);                               // Chaine Crypt�e compl�te

	    $i = (strlen($crypt) * 11) - 8;                                            // calcul de la largeur du module
	    $modul = $w/$i;

	    $this->_PDF->SetFillColor(0,0,0);											// Immer schwarz

	    for ($i=0; $i<strlen($crypt); $i++) {                                      // BOUCLE D'IMPRESSION
	        $c = $this->_T128[ord($crypt[$i])];
	        for ($j=0; $j<count($c); $j++) {
	            $this->_PDF->Rect($x,$y,$c[$j]*$modul,$h,"F");
	            $x += ($c[$j++]+$c[$j])*$modul;
	        }
	    }
	}
}                                                                              // FIN DE CLASSE
?>