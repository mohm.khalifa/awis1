<?php
global $AWIS_KEY1;
global $awisRSInfo;
global $awisRSInfoName;
global $AWISBenutzer;

$TextKonserven=array();
$TextKonserven[]=array('Fehler','err_KeinWert');
$TextKonserven[]=array('Fehler','err_FelderVeraendert');
$TextKonserven[]=array('Wort','geaendert_von');
$TextKonserven[]=array('Wort','geaendert_auf');
$TextKonserven[]=array('Meldung','DSVeraendert');
$TextKonserven[]=array('Meldung','EingabeWiederholen');

$AWISSprache = awis_BenutzerParameter($con, 'AnzeigeSprache',$AWISBenutzer->BenutzerName());

//***********************************************
// Filialebenen
//***********************************************
awis_Debug(1,$_POST);

$AWIS_KEY1=$_POST['txtFEB_KEY'];
//echo $AWIS_KEY1;
//die();
$Felder = awis_NameInArray($_POST, 'txtFEB_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('FEB','FEB_%');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';
	
	if($_POST['txtFEB_KEY']=='0')
	{
		$Speichern = true;
		
		if($Speichern)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO filialebenen';
			$SQL .= '(FEB_BEZEICHNUNG,FEB_BEMERKUNG,FEB_EBENE,FEB_SORTIERUNG,FEB_GUELTIGAB,FEB_GUELTIGBIS,FEB_FEB_KEY';
			$SQL .= ',FEB_USER,FEB_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('T',$_POST['txtFEB_BEZEICHNUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtFEB_BEMERKUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFEB_EBENE'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFEB_SORTIERUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFEB_GUELTIGAB'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFEB_GUELTIGBIS'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFEB_FEB_KEY'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';
			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('filialebenen',1,$awisDBError['message'],'200804011001');
				die();
			}
			$SQL = 'SELECT seq_FEB_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = awisOpenRecordset($con,$SQL);
			$AWIS_KEY1=$rsKey['KEY'][0];
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsFEB = awisOpenRecordset($con,'SELECT * FROM filialebenen WHERE FEB_key=' . $_POST['txtFEB_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsFEB[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsFEB['FEB_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE filialebenen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', FEB_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', FEB_userdat=sysdate';
			$SQL .= ' WHERE FEB_key=0' . $_POST['txtFEB_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('filialebenenBereiche',1,'Fehler beim Speichern',$SQL);
			}
		}
	}
}


//***********************************************
// Filialebenenrollenzuordnungen
//***********************************************

$Felder = awis_NameInArray($_POST, 'txtFRZ_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('FEB','FRZ_%');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';
	
	if($_POST['txtFRZ_KEY']=='0')
	{
		$Speichern = true;
		
		if($Speichern)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO filialebenenrollenzuordnungen';
			$SQL .= '(FRZ_XXX_KEY,FRZ_XTN_KUERZEL,FRZ_BEMERKUNG,FRZ_GUELTIGAB,FRZ_GUELTIGBIS,FRZ_FER_KEY,FRZ_KON_KEY';
			$SQL .= ',FRZ_USER,FRZ_USERDAT';
			$SQL .= ')VALUES (';
			if ($_POST['txtFRZ_XTN_KUERZEL']=='FIL')
			{
				$SQL .= ' ' . awis_FeldInhaltFormat('N0',$_POST['txtFRZ_XXX_KEY'],true);
			}
			else 
			{
				$SQL .= ' ' . awis_FeldInhaltFormat('N0',$AWIS_KEY1,true);
			}
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtFRZ_XTN_KUERZEL'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtFRZ_BEMERKUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFRZ_GUELTIGAB'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFRZ_GUELTIGBIS'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFRZ_FER_KEY'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFRZ_KON_KEY'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('filialebenenrollenzuordnungen',1,$awisDBError['message'],'200804011001');
				die();
			}
			$SQL = 'SELECT seq_FRZ_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = awisOpenRecordset($con,$SQL);
			$AWIS_KEY2=$rsKey['KEY'][0];
			//echo $AWIS_KEY2;
			//die();
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsFEB = awisOpenRecordset($con,'SELECT * FROM filialebenenrollenzuordnungen WHERE FRZ_key=' . $_POST['txtFRZ_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsFEB[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsFEB['FRZ_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE filialebenenrollenzuordnungen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', FRZ_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', FRZ_userdat=sysdate';
			$SQL .= ' WHERE FRZ_key=0' . $_POST['txtFRZ_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('filialebenenrollenzuordnungen',1,'Fehler beim Speichern',$SQL);
			}
		}
	}
}


//***********************************************
// filialebenenzuordnungen
//***********************************************

$Felder = awis_NameInArray($_POST, 'txtFEZ_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('FEB','FEZ_%');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';
	
	if($_POST['txtFEZ_KEY']=='0')
	{
		$Speichern = true;
		
		if($Speichern)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO filialebenenzuordnungen';
			$SQL .= '(FEZ_FEB_KEY,FEZ_BEMERKUNG,FEZ_GUELTIGAB,FEZ_GUELTIGBIS,FEZ_FIL_ID';
			$SQL .= ',FEZ_USER,FEZ_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('N0',$AWIS_KEY1,true);
			$SQL .= ',' . awis_FeldInhaltFormat('T',$_POST['txtFEZ_BEMERKUNG'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFEZ_GUELTIGAB'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFEZ_GUELTIGBIS'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFEZ_FIL_ID'],true);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('filialebenenzuordnungen',1,$awisDBError['message'],'200804011001');
				die();
			}
			$SQL = 'SELECT seq_FEZ_KEY.CurrVal AS KEY FROM DUAL';
			$rsKey = awisOpenRecordset($con,$SQL);
			$AWIS_KEY2=$rsKey['KEY'][0];
		}
	}
	else 					// ge�nderte Zuordnung
	{
		$FehlerListe = array();
		$UpdateFelder = '';

		$rsFEB = awisOpenRecordset($con,'SELECT * FROM filialebenenzuordnungen WHERE FEZ_key=' . $_POST['txtFEZ_KEY'] . '');
		$FeldListe = '';
		foreach($Felder AS $Feld)
		{
			$FeldName = substr($Feld,3);
			if(isset($_POST['old'.$FeldName]))
			{
		// Alten und neuen Wert umformatieren!!
				$WertNeu=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST[$Feld],true);
				$WertAlt=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$_POST['old'.$FeldName],true);
				$WertDB=awis_FeldInhaltFormat($awisRSInfoName[$FeldName]['TypKZ'],$rsFEB[$FeldName][0],true);
		//echo '<br>.'.$Feld.' 1='.$WertNeu.' 2='.$WertAlt.' 3='.$WertDB;
				if(isset($_POST['old'.$FeldName]) AND ($WertDB=='null' OR $WertAlt!=$WertNeu) AND !(strlen($FeldName)==7 AND substr($FeldName,-4,4)=='_KEY'))
				{
					if($WertAlt != $WertDB AND $WertAlt != 'null' AND $WertDB!='null')
					{
						$FehlerListe[] = array($FeldName,$WertAlt,$WertDB);
					}
					else
					{
						$FeldListe .= ', '.$FeldName.'=';

						if($_POST[$Feld]=='')	// Leere Felder immer als NULL
						{
							$FeldListe.=' null';
						}
						else
						{
							$FeldListe.=$WertNeu;
						}
					}
				}
			}
		}

		if(count($FehlerListe)>0)
		{
			$Meldung = str_replace('%1',$rsFEB['FEZ_USER'][0],$TXT_Speichern['Meldung']['DSVeraendert']);
			foreach($FehlerListe AS $Fehler)
			{
				$Meldung .= '<br>&nbsp;'.$Fehler[0].': \''.$Fehler[1].'\' ==> \''.$Fehler[2].'\'';
			}
			awisFORM_Meldung(1, $Meldung, 30001, $TXT_Speichern['Meldung']['EingabeWiederholen']);
		}
		elseif($FeldListe!='')
		{
			$SQL = 'UPDATE filialebenenzuordnungen SET';
			$SQL .= substr($FeldListe,1);
			$SQL .= ', FEZ_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', FEZ_userdat=sysdate';
			$SQL .= ' WHERE FEZ_key=0' . $_POST['txtFEZ_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('filialebenenzuordnungen',1,'Fehler beim Speichern',$SQL);
			}
		}
	}
}

//***********************************************
// Rollenvertreter
//***********************************************

$Felder = awis_NameInArray($_POST, 'txtFRV_',1,1);
if($Felder!='')
{
	$Felder = explode(';',$Felder);
	$TextKonserven[]=array('FEB','FEZ_%');
	$TXT_Speichern = awis_LadeTextKonserven($con, $TextKonserven, $AWISSprache);
	$FeldListe='';
	$SQL = '';
	$Fehler = 0;
	
	if($_POST['txtFRV_KON_KEY'] == '::bitte w�hlen::')
	{
		awis_FORM_Hinweistext('Bitte Kontakt ausw�hlen.', $Modus=1, $Style='');
		$Fehler++;
		$_GET['FRV_KEY'] = $_POST['txtFRV_KEY'];
	}
	if($_POST['txtFRV_GUELTIGAB'] == '')
	{
		awis_FORM_Hinweistext('G�ltig ab muss gef�llt sein(Default zur�ckgesetzt).', $Modus=1, $Style='');
		$Fehler++;
		$_GET['FRV_KEY'] = $_POST['txtFRV_KEY'];
	}
	if($_POST['txtFRV_GUELTIGBIS'] == '')
	{
		awis_FORM_Hinweistext('G�ltig bis muss gef�llt sein(Default zur�ckgesetzt).', $Modus=1, $Style='');
		$Fehler++;
		$_GET['FRV_KEY'] = $_POST['txtFRV_KEY'];
	}
	if($_POST['txtFRV_GUELTIGBIS'] != '' and strtotime($_POST['txtFRV_GUELTIGBIS']) < strtotime(date('d.m.Y',time())))
	{
		awis_FORM_Hinweistext('G�ltig bis darf nicht vergangen sein(Default zur�ckgesetzt).', $Modus=1, $Style='');
		$Fehler++;
		$_GET['FRV_KEY'] = $_POST['txtFRV_KEY'];
	}
	if(($_POST['txtFRV_GUELTIGBIS'] != '' and $_POST['txtFRV_GUELTIGAB'] != '') and (strtotime($_POST['txtFRV_GUELTIGBIS']) < strtotime($_POST['txtFRV_GUELTIGAB'])))
	{
		awis_FORM_Hinweistext('G�ltig bis darf nicht kleiner sein als G�ltig ab(Default zur�ckgesetzt).', $Modus=1, $Style='');
		$Fehler++;
		$_GET['FRV_KEY'] = $_POST['txtFRV_KEY'];
	}
	
	if($Fehler > 0)
	{
		$Speichern = false;
	}
	else
	{
		$Speichern = true;
	}
	
	if($_POST['txtFRV_KEY']=='0')
	{	
		if($Speichern)
		{
			$Fehler = '';
			$SQL = 'INSERT INTO filialebenenrollenvertreter ';
			$SQL .= '(FRV_FRZ_KEY,FRV_KON_KEY,FRV_DATUMVON,FRV_DATUMBIS';
			$SQL .= ',FRV_USER,FRV_USERDAT';
			$SQL .= ')VALUES (';
			$SQL .= ' ' . awis_FeldInhaltFormat('N0',$_POST['txtFRZ_KEY'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('N0',$_POST['txtFRV_KON_KEY'],true);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFRV_GUELTIGAB'],false);
			$SQL .= ',' . awis_FeldInhaltFormat('D',$_POST['txtFRV_GUELTIGBIS'],false);
			$SQL .= ',\'' . $AWISBenutzer->BenutzerName() . '\'';
			$SQL .= ',SYSDATE';
			$SQL .= ')';

			if(!awisExecute($con,$SQL))
			{
				awisErrorMailLink('Rollenvertreter',1,$awisDBError['message'],'200804011001');
				die();
			}
		}
	}
	else 					// ge�nderte Zuordnung
	{
		if($Speichern)
		{
			$SQL = 'UPDATE filialebenenrollenvertreter SET';
			$SQL .= ' FRV_KON_KEY ='. awis_FeldInhaltFormat('N0',$_POST['txtFRV_KON_KEY'],true);
			$SQL .= ', FRV_DATUMVON ='. awis_FeldInhaltFormat('D',$_POST['txtFRV_GUELTIGAB'],false);
			$SQL .= ', FRV_DATUMBIS ='. awis_FeldInhaltFormat('D',$_POST['txtFRV_GUELTIGBIS'],false);
			$SQL .= ', FRV_user=\''.$AWISBenutzer->BenutzerName().'\'';
			$SQL .= ', FRV_userdat=sysdate';
			$SQL .= ' WHERE FRV_KEY=0' . $_POST['txtFRV_KEY'] . '';
			if(awisExecute($con, $SQL)===false)
			{
				awisErrorMailLink('filialebenenzuordnungen',1,'Fehler beim Speichern',$SQL);
			}
		}
	}
	

}
?>