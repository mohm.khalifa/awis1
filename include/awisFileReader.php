<?php

class awisFileReader
{

    /**
     * @var SplFileObject
     */
    private $_Datei;

    /**
     * @var string
     */
    private $_Trennzeichen = ';';

    /**
     * @var string
     */
    private $_Textbegrenzungszeichen = '';

    /**
     * @var string
     */
    private $_EscapeZeichen = '';

    /**
     * @var array
     */
    private $_Ueberschriften;

    /**
     * Ist eine Ueberschriftenzeile vorhande?
     * @var bool
     */
    private $_UeberschriftenZeile;

    /**
     * @var array
     */
    private $_AktuellerDS = [];

    /**
     * @var int
     */
    private $_DSNummer = -1;

    /**
     * @var int
     */
    private $_ZeilenAnzahl = 0;

    /**
     * awisFileReader constructor.
     *
     * @param        $Pfad
     * @param bool   $UeberschriftenZeile
     * @param string $Trenzeichen
     * @param string $Textbegrenzungszeichen
     * @param string $EscapeZeichen
     *
     * @throws Exception
     */
    public function __construct($Pfad, $UeberschriftenZeile = true, $Trenzeichen = ';', $Textbegrenzungszeichen = '"', $EscapeZeichen = '\\')
    {
        $this->_OeffneImportdatei($Pfad);

        $this->setzeTrennzeichen($Trenzeichen,$Textbegrenzungszeichen,$EscapeZeichen);

        $this->_UeberschriftenZeile = $UeberschriftenZeile;
        if($this->_UeberschriftenZeile ){
            $this->parseUeberschriften();
        }

    }

    /**
     * @param $Pfad
     *
     * @throws Exception
     */
    private function _OeffneImportdatei($Pfad){
        $this->_Datei = new SplFileObject($Pfad,'r');

        if(!$this->_Datei){
            throw new Exception('Konnte Datei nicht �ffnen','201902041452');
        }

        //Letzte Zeile ermitteln
        $this->_Datei->seek(PHP_INT_MAX);
        $this->_ZeilenAnzahl = $this->_Datei->key() + 1;
        $this->_Datei->rewind();
    }

    /**
     * Parst die �berschriften
     *
     * @throws Exception
     */
    private function parseUeberschriften(){
        $this->DSWeiter();
        $this->_Ueberschriften = array_flip($this->_AktuellerDS);
        $this->DSWeiter();
    }

    /**
     * Pr�ft ob alle erwarteten Ueberschriften in der Datei sind
     * @param array $ErwarteteUeberschriften
     *
     * @throws Exception
     */
    public function pruefeUeberschriften(array $ErwarteteUeberschriften){
        if(!in_array($ErwarteteUeberschriften,$this->_Ueberschriften)){
            throw new Exception('Nicht alle erwarteten Ueberschriften gefunden','201902041455');
        }
    }

    /**
     * Parst die Zeile ins Array
     * @throws Exception
     */
    private function parseLine(){
        $this->_AktuellerDS = $this->_Datei->current();

        if(!$this->_AktuellerDS){
            throw new Exception('Fehler beim parsen der Zeile: ' . $this->_DSNummer,'201902041453');
        }
    }

    /**
     * Setzt den Cursor eine Zeile weiter
     * @throws Exception
     */
    public function DSWeiter(){
        $DSNr = $this->_DSNummer + 1;
        $this->geheZuZeile($DSNr);
    }

    /**
     * Setzt den Cursor in der Datei
     * @param $ZeilenNr
     *
     * @throws Exception
     */
    public function geheZuZeile($ZeilenNr){
        $this->_DSNummer = $ZeilenNr;
        $this->_Datei->seek($ZeilenNr);
        $this->parseLine();
    }


    /**
     * @param string $Feld Name des Feldes aus Ueberschrift, oder Spaltennummer
     *
     * @return string
     */
    public function FeldInhalt($Feld){
        if($this->_Ueberschriften and !is_numeric($Feld)){
            return @$this->_AktuellerDS[$this->_Ueberschriften[$Feld]];
        }else{
            return @$this->_AktuellerDS[$Feld];
        }
    }

    /**
     * Returnt true, wenn das Ende der Datei erreicht ist
     * @return bool
     */
    public function EOF(){

        return $this->_DSNummer>=$this->_ZeilenAnzahl;
    }

    /**
     * Definiert Trennzeichen usw.
     *
     * @param        $Trennzeichen
     * @param string $Textbegrenzungszeichen
     * @param string $EscapeZeichen
     */
    private function setzeTrennzeichen($Trennzeichen, $Textbegrenzungszeichen='"', $EscapeZeichen = '\\'){
        $this->_Datei->setCsvControl($Trennzeichen, $Textbegrenzungszeichen,$EscapeZeichen);
        $this->_Datei->setFlags(SplFileObject::READ_CSV);
        $this->_Trennzeichen = $Trennzeichen;
        $this->_Textbegrenzungszeichen = $Textbegrenzungszeichen;
        $this->_EscapeZeichen = $EscapeZeichen;
    }

    /**
     * Liefert die Ueberschriften der Datei
     * @return array|null
     */
    public function Ueberschriften(){
        return array_flip($this->_Ueberschriften);
    }

    /**
     * Liefert den aktuellen Datensatz als Array
     * @return array
     */
    public function AktuellerDS(){
        return $this->_AktuellerDS;
    }
}