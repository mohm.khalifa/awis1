<?php
require_once '../asesplandaten/asesplandaten_funktionen.php';

$APD = new asesplandaten_funktionen();
$Speichern = isset($APD->Param['SPEICHERN']) && $APD->Param['SPEICHERN'] == 'on'?true:false;

$APD->Form->ZeileStart();
$APD->Form->Erstelle_TextLabel($APD->AWISSprachKonserven['FIL']['FIL_ID'].':',190);
$SQL = 'select FIL_ID, FIL_ID || \' \' || FIL_BEZ from V_FILIALEN_AKTUELL a ' ;

if(isset($_REQUEST['txtAPD_CLIENT_NO'])){
    $SQL .= ' inner join LAENDER b on a.LAN_CODE = b.LAN_CODE ';
    $SQL .= ' where LAN_ASES_CLIENTNO = ' . $APD->DB->WertSetzen('APD','T',$_REQUEST['txtAPD_CLIENT_NO']);
}

$SQL .= ' order by FIL_ID asc';

$APD->Form->Erstelle_MehrfachSelectFeld('APD_FIL_ID',($Speichern?$APD->Param['APD_FIL_ID']:[]),'1000:1000', true,$SQL,'','','','','','','',$APD->DB->Bindevariablen('APD'),'','AWIS','','',20);
$APD->AWISCursorPosition='txtAPD_FIL_ID';
$APD->Form->ZeileEnde();


$APD->Form->ZeileStart('display: none', 'FORMAT_PERSNR');
$APD->Form->Hinweistext($APD->AWISSprachKonserven['APD']['APD_ERR_FORMAT_PERSNR'],awisFormular::HINWEISTEXT_FEHLER);
$APD->Form->ZeileEnde();

$APD->Form->ZeileStart();
$APD->Form->Erstelle_TextLabel($APD->AWISSprachKonserven['PER']['PER_NR'].':',190);
$APD->Form->Erstelle_TextFeld('APD_PERS_NR',($Speichern?$APD->Param['APD_PERS_NR']:''),150,1000,true,'','','','T','L','','',0,'onChange="pruefeAPD_PERS_NR();"');
$APD->Form->ZeileEnde();


$APD->Form->ZeileStart('display: none', 'FORMAT_DATUM');
$APD->Form->Hinweistext($APD->AWISSprachKonserven['APD']['APD_ERR_FORMAT_DATUM'],awisFormular::HINWEISTEXT_FEHLER);
$APD->Form->ZeileEnde();

$APD->Form->ZeileStart();
$APD->Form->Erstelle_TextLabel($APD->AWISSprachKonserven['Wort']['DatumVom'].':',190);
$APD->Form->Erstelle_TextFeld('APD_DATUM_VON',($Speichern?$APD->Param['APD_DATUM_VON']:date('c')),15,150,true,'','','','D','L','','',0,'onChange="pruefeDatum();"');
$APD->Form->ZeileEnde();

$APD->Form->ZeileStart();
$APD->Form->Erstelle_TextLabel($APD->AWISSprachKonserven['Wort']['DatumBis'].':',190);
$APD->Form->Erstelle_TextFeld('APD_DATUM_BIS',($Speichern?$APD->Param['APD_DATUM_BIS']:date('c')),15,150,true,'','','','D','L','','',0,'onChange="pruefeDatum();"');
$APD->Form->ZeileEnde();