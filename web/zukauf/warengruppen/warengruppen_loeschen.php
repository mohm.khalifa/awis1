<?php
global $AWIS_KEY1;

$TextKonserven=array();
$TextKonserven[]=array('Wort','WirklichLoeschen');
$TextKonserven[]=array('Wort','Ja');
$TextKonserven[]=array('Wort','Nein');

try
{
	$Form = new awisFormular();
	$AWISBenutzer = awisBenutzer::Init();
	$DB = awisDatenbank::NeueVerbindung('AWIS');
	$DB->Oeffnen();

	$Tabelle= '';

	if(!isset($_GET['Del']) AND isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_POST['txtZSZ_KEY']))
		{
			$Tabelle = 'ZSZ';
			$Key=$_POST['txtZSZ_KEY'];

			$SQL = 'SELECT ZSZ_WG1,ZSZ_SORTIMENT,ZSZ_KEY';
			$SQL .= ' FROM ZUKAUFSORTIMENTZUORDNUNGEN ';
			$SQL .= ' WHERE ZSZ_KEY=0'.$Key;

			$rsDaten = $DB->RecordsetOeffnen($SQL);

			$ZSZKey = $rsDaten->FeldInhalt('ZSZ_KEY');

			$Felder=array();
			$Felder[]=array($Form->LadeTextBaustein('ZSZ','ZSZ_WG1'),$rsDaten->FeldInhalt('ZSZ_WG1'));
			$Felder[]=array($Form->LadeTextBaustein('ZSZ','ZSZ_WG2'),$rsDaten->FeldInhalt('ZSZ_WG2'));
			$Felder[]=array($Form->LadeTextBaustein('ZSZ','ZSZ_WG3'),$rsDaten->FeldInhalt('ZSZ_WG3'));
			$Felder[]=array($Form->LadeTextBaustein('ZSZ','ZSZ_SORTIMENT'),$rsDaten->FeldInhalt('ZSZ_SORTIMENT'));
		}
	}
	elseif(isset($_GET['Del']) AND !isset($_POST['cmdLoeschen_x']))
	{
		if(isset($_GET['Unterseite']))
		{
			switch($_GET['Unterseite'])
			{
				default:
					break;
			}
		}
		else
		{
			switch ($_GET['Seite'])
			{
				case '':
					$Tabelle = '';
					$Key=$_GET['Del'];
					$ZSZKey=$_GET['Key'];

					$SQL = 'SELECT ';
					$SQL .= ' FROM XXXX ';
					$SQL .= ' WHERE _KEY=0'.$Key . '';
					$rsDaten = $DB->RecordsetOeffnen($SQL);

					$Felder=array();

					$Felder[]=array($Form->LadeTextBaustein('',''),$rsDaten->FeldInhalt(''));
					break;
				default:
					break;
			}
		}
	}
	elseif(isset($_POST['cmdLoeschenOK']))	// Loeschen durchführen
	{
		$SQL = '';
		switch ($_POST['txtTabelle'])
		{
			case 'ZSZ':
				$SQL = 'DELETE FROM ZUKAUFSORTIMENTZUORDNUNGEN WHERE ZSZ_key=0'.$_POST['txtKey'];
				$AWIS_KEY1=-1;
				break;
			default:
				break;
		}

		if($SQL !='')
		{
			if($DB->Ausfuehren($SQL)===false)
			{
				awisErrorMailLink('warengruppen_loeschen_1',1,$awisDBError['messages'],'');
			}
		}
	}

	if($Tabelle!='')
	{
		$TXT_AdrLoeschen = $Form->LadeTexte($TextKonserven);

		$Form->SchreibeHTMLCode('<form name=frmLoeschen action=./warengruppen_Main.php?cmdAktion='.$_GET['cmdAktion'].(isset($_GET['Seite'])?'&Seite='.$_GET['Seite']:'').(isset($_GET['Unterseite'])?'&Unterseite='.$_GET['Unterseite']:'').' method=post>');

		$Form->Formular_Start();
		$Form->ZeileStart();
		$Form->Hinweistext($TXT_AdrLoeschen['Wort']['WirklichLoeschen']);
		$Form->ZeileEnde();

		foreach($Felder AS $Feld)
		{
			$Form->ZeileStart();
			$Form->Erstelle_TextLabel($Feld[0].':',150);
			$Form->Erstelle_TextFeld('Feld',$Feld[1],100,500,false);
			$Form->ZeileEnde();
		}

		$Form->Erstelle_HiddenFeld('ZSZKey',$ZSZKey);
		$Form->Erstelle_HiddenFeld('Tabelle',$Tabelle);
		$Form->Erstelle_HiddenFeld('Key',$Key);

		$Form->Trennzeile();

		$Form->ZeileStart();
		$Form->Schaltflaeche('submit','cmdLoeschenOK','','',$TXT_AdrLoeschen['Wort']['Ja'],'');
		$Form->Schaltflaeche('submit','cmdLoeschenAbbrechen','','',$TXT_AdrLoeschen['Wort']['Nein'],'');
		$Form->ZeileEnde();

		$Form->SchreibeHTMLCode('</form>');

		$Form->Formular_Ende();

		die();
	}
}
catch (awisException $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
	$Form->DebugAusgabe(1,$ex->getSQL());
}
catch (Exception $ex)
{
	$Form->Fehler_Anzeigen('SpeicherFehler',$ex->getMessage(),'HotlineBenachrichtigen',-2);
}
?>