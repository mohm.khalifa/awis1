<html>
<head>
<meta name="author" content="Sacha Kerres">
<meta http-equiv="expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<CONTENT="text/html; charset=ISO-8859-15">
<title>Awis - ATU webbasierendes Informationssystem</title>
<?php
require_once("register.inc.php");
require_once("db.inc.php");		// DB-Befehle
require_once("sicherheit.inc.php");
global $AWISBenutzer;
global $_GET;
global $_POST;
global $awisDBFehler;			// Fehler-Objekt bei DB-Zugriff
global $awisRSZeilen;
global $con;

print "<link rel=stylesheet type=text/css href=" . awis_CSSDatei($AWISBenutzer->Benutzername()) .">";
?>
</head>

<?php
/****************************************************************************************************
*
* 	hersteller
*
* 	Pflege der Hersteller
*
* 	Autor: 	Sacha Kerres
* 	Datum:	Dez. 2003
*
****************************************************************************************************/
// Variablen

include ("ATU_Header.php");	// Kopfzeile

$con = awisLogon();
$Rechtestufe = awisBenutzerRecht($con, 602);
		// 	1=Einsehen
		//	2=Bearbeiten
		//	4=Hinzuf�gen

if($Rechtestufe==0)
{
    awisEreignis(3,1000,'Hersteller',$AWISBenutzer->Benutzername(),'','','');
    die("<span class=HinweisText>Keine ausreichenden Rechte!</span>");
}

echo '<form name=frmHersteller method=post action=./hersteller.php>';

if(isset($_POST['cmdSpeichern_x']) AND $_POST['cmdSpeichern_x']!='')
{
	$Aenderungen = '';
	If($_POST['txtHER_ID'] != $_POST['txtHER_ID_old'])
	{
		$Aenderungen = ', HER_ID = ' . $_POST['txtHER_ID'];
	}
	If($_POST['txtHER_BEZEICHNUNG'] != $_POST['txtHER_BEZEICHNUNG_old'])
	{
		$Aenderungen .= ", HER_BEZEICHNUNG = '" . $_POST['txtHER_BEZEICHNUNG'] . "'";
	}
	If($_POST['txtHER_KONZERN'] != $_POST['txtHER_KONZERN_old'])
	{
		$Aenderungen .= ", HER_KONZERN='" . $_POST['txtHER_KONZERN'] . "'";
	}

	If($_POST['txtHER_TYP'] != $_POST['txtHER_TYP_old'])
	{
		$Aenderungen .= ", HER_TYP=" . $_POST['txtHER_TYP'] . "";
	}

	If($_POST['txtHER_BEARBEITUNGSGRUPPE'] != $_POST['txtHER_BEARBEITUNGSGRUPPE_old'])
	{
		$Aenderungen .= ", HER_BEARBEITUNGSGRUPPE='" . $_POST['txtHER_BEARBEITUNGSGRUPPE'] . "'";
	}

	If($Aenderungen!='')
	{
		$SQL = "UPDATE HERSTELLER SET " . substr($Aenderungen,2);
		$SQL .= ", HER_USER='" . $AWISBenutzer->Benutzername() . "', HER_USERDAT=SYSDATE";
		$SQL .= " WHERE HER_ID=0" . $_POST['txtHER_ID_old'];
		$Erg = awisExecute($con, $SQL);
		if($Erg==FALSE)
		{
			awisErrorMailLink("hersteller.php", 2, $awisDBFehler);
			awisLogoff($con);
			die();
		}
	}
}

$ZeitVerbrauch = time();		// F�r die Zeitanzeige
$SQL = '';

	//**************************************************
	// Kein spezieller angegeben -> suchen
	//**************************************************

if((!isset($_GET['HERID']) and !isset($_POST['txtHER_ID'])) or isset($_POST['cmdTrefferListe_x']))
{
	$SQL = "SELECT * FROM HERSTELLER, IMPORTQUELLEN WHERE HER_IMQ_ID = IMQ_ID AND HER_ID > 0";
}
else
{
	$SQL = "SELECT * FROM HERSTELLER, IMPORTQUELLEN WHERE HER_IMQ_ID = IMQ_ID AND HER_ID='". $_GET['HERID'] . $_POST['txtHER_ID'] ."'";
}

if(isset($_GET['Sort']))
{
	$SQL .= " ORDER BY " . $_GET['Sort'];
}
else
{
	$SQL .= " ORDER BY HER_ID";
}


$rsHER = awisOpenRecordset($con, $SQL);
$rsHERZeilen = $awisRSZeilen;


	//**************************************************
	// Daten ausgeben
	//**************************************************

if($rsHERZeilen > 1)	// Liste anzeigen
{
	echo '<table id=Farbig border=1 width=100% id=DatenTabelle>';
	echo '<tr><td id=FeldBez><a href=./hersteller.php?Sort=HER_ID>Nummer</a></td>';
	echo '<td id=FeldBez><a href=./hersteller.php?Sort=HER_BEZEICHNUNG>Bezeichnung</a></td>';
	echo '<td id=FeldBez><a href=./hersteller.php?Sort=HER_KONZERN>Konzern</a></td>';
	echo '</tr>';

	for($Zeile=0;$Zeile<$rsHERZeilen;$Zeile++)
	{
		echo "<tr><td id=DatenFeld><a href=./hersteller.php?HERID=" . $rsHER['HER_ID'][$Zeile] . ">" . $rsHER['HER_ID'][$Zeile] . "</a></td>";
		echo "<td id=DatenFeld>" . $rsHER['HER_BEZEICHNUNG'][$Zeile] . "</td>";
		echo "<td id=DatenFeld>" . $rsHER['HER_KONZERN'][$Zeile] . "</td>";

		echo '</tr>';
	}

	echo '</table>';
}
else					// einzelnen hersteller anzeigen
{
	if(($Rechtestufe & 2)==2 AND ($rsHER['HER_IMQ_ID'][0]==1 OR $rsHER['HER_IMQ_ID'][0]==4))
	{
		$AendernErlaubt = True;
	}
	else
	{
		$AendernErlaubt = False;
	}


	//Zeile 1 - �berschift
	echo '<table id=Farbig width=100% border=0><tr><td width=80%><h3>Herstellerstamm</h3></td>';
	echo '<td><font size=1>' . $rsHER['HER_USER'][0] . '<br>' . $rsHER['HER_USERDAT'][0] . '</font></td>';

    echo "<td width=40 backcolor=#000000 align=right><input name=cmdTrefferListe type=image src=/bilder/NeueListe.png accesskey='t' alt='Trefferliste (Alt+T)' onclick=location.href='./hersteller.php?Liste=True';></td>";
	echo '</tr></table>';

	echo '<table id=Farbig border=1 width=100%>';
	echo '<colgroup><col width=170><col width=400><col width=100><col width=*></colgroup>';
	echo '<tr>';
	echo '<td id=FeldBez>Hersteller-Nr</td>';
	if($AendernErlaubt)
	{
		echo "<td><input name=txtHER_ID size=3 value='" . $rsHER['HER_ID'][0] . "'>";
		echo "<input type=hidden name=txtHER_ID_old value='" . $rsHER['HER_ID'][0] . "'></td>";
	}
	else
	{
		echo '<td>' . $rsHER['HER_ID'][0] . '</td>';
	}
	echo '<td></td>';
	echo '</tr>';

			// Zeile 2
	echo '<tr>';
	echo '<td id=FeldBez>Name</td>';
	if($AendernErlaubt)
	{
		echo "<td><input name=txtHER_BEZEICHNUNG size=40 value='" . $rsHER['HER_BEZEICHNUNG'][0] . "'>";
		echo "<input type=hidden name=txtHER_BEZEICHNUNG_old value='" . $rsHER['HER_BEZEICHNUNG'][0] . "'></td>";
		echo '<td id=FeldBez>Konzern</td>';
		echo "<td><input name=txtHER_KONZERN size=40 value='" . $rsHER['HER_KONZERN'][0] . "'>";
		echo "<input type=hidden name=txtHER_KONZERN_old value='" . $rsHER['HER_KONZERN'][0] . "'></td>";
	}
	else
	{
		echo '<td>' . $rsHER['HER_BEZEICHNUNG'][0] . '</td>';
		echo '<td id=FeldBez>Konzern</td>';
		echo '<td>' . $rsHER['HER_KONZERN'][0] . '</td>';
	}
	echo '</tr>';

			// Zeile 3
	echo '<tr>';
	echo '<td id=FeldBez>Typ</td>';
	echo '<td>';
	if($AendernErlaubt)
	{
		echo '<select name=txtHER_TYP>';
		echo '<option value=1 ' . ($rsHER['HER_TYP'][0]==1?'selected':'') . '>Fahrzeughersteller</option>';
		echo '<option value=2 ' . ($rsHER['HER_TYP'][0]==2?'selected':'') . '>Teilehersteller</option>';
		echo '</select>';
		echo "<input type=hidden name=txtHER_TYP_old value='" . $rsHER['HER_TYP'][0] . "'></td>";
	}
	else
	{
		echo ($rsHER['HER_TYP'][0]==1?'Fahrzeughersteller':'Teilehersteller');
	}
	echo '</td>';

	echo '<td id=FeldBez>Quelle</td>';
	echo '<td>';
	echo $rsHER['IMQ_IMPORTQUELLE'][0];
	echo '</td>';

	echo '</tr>';

			// Zeile 4

	echo '<tr>';
	echo '<td id=FeldBez>Bearbeitungsgruppe</td>';

	echo "<td><input name=txtHER_BEARBEITUNGSGRUPPE size=40 value='" . $rsHER['HER_BEARBEITUNGSGRUPPE'][0] . "'>";
	echo "<input type=hidden name=txtHER_BEARBEITUNGSGRUPPE_old size=40 value='" . $rsHER['HER_BEARBEITUNGSGRUPPE'][0] . "'></td>";

	echo '<td></td>';
	echo '</tr>';


	if($AendernErlaubt)
	{
		print "<tr><td colspan=9><input type=hidden name=Speichern Value=True>";
		print " <input type=image accesskey=S alt='Speichern (Alt+S)' src=/bilder/diskette.png name=cmdSpeichern onclick=location.href='./hersteller.php?HERID=" . $rsHER["HER_ID"][0] . "&Speichern=True'>";
		print "</td></tr>";
	}

	echo '</table>';

				// Cursor in das erste Feld setzen
	print "<Script Language=JavaScript>";
	print "document.getElementsByName(\"txtHER_ID\")[0].focus();";
	print "</Script>";

}

/*
* Zur�ck - Link
*
* */

if(isset($_POST['txtZurueck']) AND $_POST['txtZurueck']!='')
{
	$ZurueckLink = $_POST['txtZurueck'];
}
else
{
	if($_GET['Zurueck']=='artikel_LIEF')
	{
		$ZurueckLink = '/ATUArtikel/artikel_Main.php?Key=' . $_GET['ASTKEY'] . '&cmdAktion=ArtikelInfos';
	}
	elseif($_GET['Zurueck']=='')
	{
		$ZurueckLink = './stammdaten_Main.php';
	}
	else
	{
		$ZurueckLink = str_replace('~~9~~', '&', $_GET['Zurueck']);
	}
}

echo '<input type=hidden name=txtZurueck value=' . $ZurueckLink . '>';

echo '</form>';

print "<br><hr><input type=image alt='Zur�ck (Alt+z)' src=/bilder/zurueck.png name=cmdZurueck accesskey=z onclick=location.href='$ZurueckLink';>";
print "&nbsp;<input type=image alt='Hilfe (Alt+h)' src=/bilder/hilfe.png name=cmdHilfe accesskey=h onclick=window.open('/hilfe/hilfe_Main.php?HilfeThema=hersteller','Hilfe','toolbar=no,menubar=no,dependent=yes,status=no');>";

awisLogoff($con);

?>