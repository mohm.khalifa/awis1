<?php

/**
 * Funktions-/Scriptbeschreibung:
 * Script erzeugt eine PDF-Datei mit einer Liste über alle aktuell gesperrten AFG-Vorgänge
 *
 * Parameter:
 * -- keine --
 *
 * Erstellt:
 * 01.09.2009 CB
 *
 * Änderungen:
 * 15.06.2011 OP : Liste neu aufgebaut. Es werden jetzt neben Daten aus der Kopftabelle auch
 *                 Daten aus der Kassentabelle angezeigt. Außerdem wird die Betragsdifferenz
 *                 berechnet und ebenfalls mit angezeigt.
 */
require_once('awisAusdruck.php');
require_once('awisDatenbank.inc');
require_once('awisFormular.inc');

ini_set('max_execution_time', 300);

$DB = awisDatenbank::NeueVerbindung('AWIS');
$DB->Oeffnen();
$Form = new awisFormular();

$TextKonserven[] = array('FGK', '%');
$TextKonserven[] = array('FKH', '%');
$TextKonserven[] = array('FKA', '%');
$TextKonserven[] = array('FGP', '%');
$TextKonserven[] = array('FPH', '%');

$AWISSprachKonserven = awisFormular::LadeTexte($TextKonserven);

//-----------------------------------------------------------------------------------------------------------
// Seitenparameter festlegen und Listenüberschrift erstellen

$Spalte = 15;
$Zeile = 20;

$Ausdruck = new awisAusdruck('L', 'A4', array('BriefpapierATU_DE_Seite_2_quer.pdf'));
$Ausdruck->NeueSeite(0, 1);

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->SetFont('Arial', 'B', 10);

$Zeile = 30;
$count = 0;

$Ausdruck->_pdf->setXY($Spalte, $Zeile);
$Ausdruck->_pdf->cell(35, 5, $AWISSprachKonserven['FGK']['FGK_VORGANGNR'], 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(15, 5, 'Kasse', 'LTRB', 0, 'R', 0);
$Ausdruck->_pdf->cell(15, 5, 'WANR', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(15, 5, 'FG', 'LTRB', 0, 'R', 0);
$Ausdruck->_pdf->cell(15, 5, 'Diff.', 'LTRB', 0, 'R', 0);
$Ausdruck->_pdf->cell(45, 5, 'Versicherung', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(25, 5, 'Datum(Kasse)', 'LTRB', 0, 'L', 0);
$Ausdruck->_pdf->cell(105, 5, 'Bemerkung', 'LTRB', 0, 'L', 0);
//-----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
// Daten für Liste selektieren und Berechnen und Listenzeilen erstellen
//$SQL  = 'Select * from FGKOPFDATEN WHERE FGK_FGN_KEY=6 ORDER BY FGK_VORGANGNR ASC';
//$SQL = "SELECT * FROM fgkopfdaten a INNER JOIN ";
//$SQL .= "(select fgk_vorgangnr ";
//$SQL .= "from ";
//$SQL .= "(select distinct fgk_kennung, fgk_vorgangnr ";
//$SQL .= "from fgkopfdaten a inner join fkassendaten b on a.fgk_vorgangnr = b.fka_aemnr ";
//$SQL .= "where fgk_fgn_key = 6)) b ON a.fgk_vorgangnr = b.fgk_vorgangnr ";
//$SQL .= "order by fgk_montagedatum"; //<- Sortierung nach Kassendatum!!!!!

$SQL = "SELECT * FROM fgkopfdaten a INNER JOIN ";
$SQL .= "(select fgk_vorgangnr, kassedatum ";
$SQL .= "from ";
$SQL .= "(select distinct fgk_kennung, fgk_vorgangnr, min(fka_datum) as kassedatum ";
$SQL .= "from fgkopfdaten a inner join fkassendaten b on a.fgk_vorgangnr = b.fka_aemnr ";
$SQL .= "where fgk_fgn_key = 6 group by fgk_kennung, fgk_vorgangnr order by kassedatum)) b ON a.fgk_vorgangnr = b.fgk_vorgangnr ";
$SQL .= "order by kassedatum";

$rsKopfdaten = $DB->RecordSetOeffnen($SQL);

while (!$rsKopfdaten->EOF())
{
    // Positionsdaten aufbereiten (-> Summe bilden + Steuer hinzuaddieren)
    $SQL = 'SELECT * FROM fgpositionsdaten WHERE fgp_fgk_key = ' . $rsKopfdaten->FeldInhalt('FGK_KEY');

    $rsPosDaten = $DB->RecordSetOeffnen($SQL);

    $zwischenSumPos = 0.00;
    $endSumPos = 0.00;

    while (!$rsPosDaten->EOF())
    {
        $zwischenSumPos = $zwischenSumPos + (str_replace(',', '.', $rsPosDaten->FeldInhalt('FGP_ANZAHL')) * str_replace(',', '.', $rsPosDaten->FeldInhalt('FGP_OEMPREIS')));

        $rsPosDaten->DSWeiter();
    }

    $endSumPos = round($zwischenSumPos * (1 + ($rsKopfdaten->FeldInhalt('FGK_STEUERSATZ') / 100)), 2);

    // Kassendaten aufbereiten (-> Summe bilden wenn bereits Umbuchen stattfanden und daher zu einem Vorgang mehrere KassenDS vorhanden sind
    $SQL = 'SELECT * FROM fkassendaten WHERE fka_aemnr = \'' . $rsKopfdaten->FeldInhalt('FGK_VORGANGNR') . '\'';

    $rsKassenDaten = $DB->RecordSetOeffnen($SQL);

    $zwischenSumKasse = 0.00;
    $endSumKasse = 0.00;
    $wanr = '';
    $datumKasse = '';

    while (!$rsKassenDaten->EOF())
    {
        $arrSearch = array(',', '-');
        $arrReplace = array('.', '');

        $zwischenSumKasse = $zwischenSumKasse + (str_replace($arrSearch, $arrReplace, $rsKassenDaten->FeldInhalt('FKA_MENGE')) * str_replace(',', '.', $rsKassenDaten->FeldInhalt('FKA_BETRAG')));
        $wanr = $rsKassenDaten->FeldInhalt('FKA_WANR');
        $datumKasse = $rsKassenDaten->FeldInhalt('FKA_DATUM');

        $rsKassenDaten->DSWeiter();
    }
    $datumK = substr($datumKasse, 0, -8);
    $endSumKasse = round($zwischenSumKasse, 2);
    $diffBetrag = round($endSumKasse - $endSumPos, 2);

    //---------------------------------------------------------------------------
    // Listenzeile erstellen
    // $Zeile += 5;

    $zeichenlaenge = strlen($rsKopfdaten->FeldInhalt('FGK_NOTIZ'));
    //Anzahl Zeilenumbrüche berechnen
    $berechAnz = $zeichenlaenge / 70;
    $zeilenHoehe = 5;

    if ($berechAnz > 1)
    {
        //auf ganze Zeile aufrunden
        $anzZeilen = ceil($berechAnz);
        //1 Zeile = 5
        $zeilenHoehe = 5 * $anzZeilen;
    }
    if ($count == 0)
    {
        $Zeile += 5;
    }

    if ($count < 20)
    {
        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        //AFG-Nummer
        $Ausdruck->_pdf->cell(35, $zeilenHoehe, $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), 'LTRB', 0, 'L', 0);
        //Betrag (Kasse)
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $endSumKasse, 'LTRB', 0, 'R', 0);
        //WANR
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $wanr, 'LTRB', 0, 'L', 0);
        //Betrag(FG)
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $endSumPos, 'LTRB', 0, 'R', 0);
        //Differenz
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $diffBetrag, 'LTRB', 0, 'R', 0);
        //Versicherung
        $Ausdruck->_pdf->cell(45, $zeilenHoehe, substr($rsKopfdaten->FeldInhalt('FGK_VERSICHERUNG'), 0, 20), 'LTRB', 0, 'L', 0);
        //Datum(Kasse)
        $Ausdruck->_pdf->cell(25, $zeilenHoehe, $datumK, 'LTRB', 0, 'C', 0);
        //Bemerkung (leeres Feld)

        $Ausdruck->_pdf->SetFont('Courier', 'B', 7);
        $Ausdruck->_pdf->Multicell(105, 5, $rsKopfdaten->FeldInhalt('FGK_NOTIZ'), 'LTRB', 'L', 0);
        $Ausdruck->_pdf->SetFont('Arial', 'B', 10);
        $Zeile += $zeilenHoehe;
    }
    else
    {
        if ($berechAnz > 1)
        {
            //auf ganze Zeile aufrunden
            $anzZeilen = ceil($berechAnz);
            //1 Zeile = 5
            $zeilenHoehe = 5 * $anzZeilen;
        }
        // neue Seite beginnt (count zurücksetzen und Listenüberschrift ausgeben)
        $Zeile = 30;
        $count = 0;
        $Ausdruck->_pdf->AddPage();

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        $Ausdruck->_pdf->cell(35, 5, $AWISSprachKonserven['FGK']['FGK_VORGANGNR'], 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 5, 'Kasse', 'LTRB', 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, 'WANR', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(15, 5, 'FG', 'LTRB', 0, 'R', 0);
        $Ausdruck->_pdf->cell(15, 5, 'Diff.', 'LTRB', 0, 'R', 0);
        $Ausdruck->_pdf->cell(45, 5, 'Versicherung', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(25, 5, 'Datum(Kasse)', 'LTRB', 0, 'L', 0);
        $Ausdruck->_pdf->cell(105, 5, 'Bemerkung', 'LTRB', 0, 'L', 0);

        if ($count == 0)
        {
            $Zeile += 5;
        }

        $Ausdruck->_pdf->setXY($Spalte, $Zeile);
        //AFG-Nummer
        $Ausdruck->_pdf->cell(35, $zeilenHoehe, $rsKopfdaten->FeldInhalt('FGK_VORGANGNR'), 'LTRB', 0, 'L', 0);
        //Betrag (Kasse)
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $endSumKasse, 'LTRB', 0, 'R', 0);
        //WANR
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $wanr, 'LTRB', 0, 'L', 0);
        //Betrag(FG)
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $endSumPos, 'LTRB', 0, 'R', 0);
        //Differenz
        $Ausdruck->_pdf->cell(15, $zeilenHoehe, $diffBetrag, 'LTRB', 0, 'R', 0);
        //Versicherung
        $Ausdruck->_pdf->cell(45, $zeilenHoehe, substr($rsKopfdaten->FeldInhalt('FGK_VERSICHERUNG'), 0, 20), 'LTRB', 0, 'L', 0);
        //Datum(Kasse)
        $Ausdruck->_pdf->cell(25, $zeilenHoehe, $datumK, 'LTRB', 0, 'C', 0);
        //Bemerkung (leeres Feld)
        $Ausdruck->_pdf->SetFont('Courier', 'B', 7);
        $Ausdruck->_pdf->Multicell(105, 5, $rsKopfdaten->FeldInhalt('FGK_NOTIZ'), 'LTRB', 'L', 0);
        $Ausdruck->_pdf->SetFont('Arial', 'B', 10);
        $Zeile += $zeilenHoehe;
    }


    //die();
    $count++;
    $rsKopfdaten->DSWeiter();

    //---------------------------------------------------------------------------
}

$Ausdruck->Anzeigen();



//
//----------------------------------------------------------------------------------------------------------
?>